#!/bin/bash

# set os type
linux=false
sunos=false
case "`uname`" in
Linux*) linux=true;;
SunOS*) sunos=true;;
esac

# properties
BIN=`echo $0 | awk '{ print substr( $0, 0, length($0)-6 ) }'`

cd ${BIN}/../..
DIST=`pwd`
. ${DIST}/elasticsearch/bin/rssearch-env.sh
export JAVA_HOME=${RSSEARCHJDK}

if [ -d ${JAVA_HOME} ];
	then 
	JAVA_RESOLVEMAJOR=`${DIST}/jdk/bin/java -version 2>&1 | awk 'NR==1{split($0,a,"."); print a[2] }'`
	JAVA_RESOLVELESSER=`${DIST}/jdk/bin/java -version 2>&1 | awk  'NR==1{gsub(/"/,"");split($0,a,"_"); print a[2] }'`
	JAVA_BLUEPRINTMAJOR=`${JAVA_HOME}/bin/java -version 2>&1 | awk 'NR==1{split($0,a,"."); print a[2] }'`
	JAVA_BLUEPRINTLESSER=`${JAVA_HOME}/bin/java -version 2>&1 | awk  'NR==1{gsub(/"/,"");split($0,a,"_"); print a[2] }'`
	if [ ${JAVA_RESOLVEMAJOR} -ne ${JAVA_BLUEPRINTMAJOR} ]; then
			echo "supplied java version is different than the required version, using default resolve java"
			export JAVA_HOME=${DIST}/jdk
	fi

	if [ ${JAVA_RESOLVELESSER} -gt ${JAVA_BLUEPRINTLESSER} ]; then
			echo "supplied java version is less than required, using  default resolve java"
			export JAVA_HOME=${DIST}/jdk
	fi
else
	echo given java_home folder does not exist using default resolve java
	export JAVA_HOME=${DIST}/jdk
fi
export ELASTICSEARCH_PID_FILE=${DIST}/elasticsearch/bin/lock

if [ -f ${ELASTICSEARCH_PID_FILE} ]; then
    if [ $# -eq 0 ] || [ "$1" != "1" ]; then
        echo "Lock File Detected"
        echo "Cancelling ElasticSearch Startup"
        exit 1
    fi
fi

if $sunos; then
    export JAVA_OPTS="$JAVA_OPTS -d64"
fi

echo "Starting ElasticSearch"
elasticsearch/bin/elasticsearch -p $ELASTICSEARCH_PID_FILE -d

for i in 1 2 3 4 5
do
  if [ -f $ELASTICSEARCH_PID_FILE ]; then
    break
  else
    sleep 1
  fi
done

PID=`cat $ELASTICSEARCH_PID_FILE 2>/dev/null`

echo "Started ElasticSearch pid: ${PID}"
