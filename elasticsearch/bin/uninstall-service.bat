@echo off

REM ##############################################
REM ## call this file to set environment variables
REM ##############################################
set SBIN=%~dp0
set DIST=%SBIN%\..\..
set JAVA_HOME=%DIST%\jdk

CALL "%SBIN%\service.bat" remove > uninstallLog.log
echo ======================= >> uninstallLog.log

echo The 'rssearch' service has been uninstalled.

set SBIN=
set DIST=
set JAVA_HOME=
