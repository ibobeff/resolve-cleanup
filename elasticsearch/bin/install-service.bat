@echo off

REM ##############################################
REM ## call this file to set environment variables
REM ##############################################
set SBIN=%~dp0
pushd %~dp0..\..
set DIST=%CD%
popd
call "%DIST%\elasticsearch\bin\rssearch-env.bat"
set JDK=%RSSEARCHJDK%
set JAVA_HOME=%JDK%

if exist "%JAVA_HOME%" (
	goto jre_exists
) else (
	echo given java_home folder does not exist using default resolve java
	set JDK=%DIST%\jdk
	set JAVA_HOME=%JDK%\jre
	goto finish_run
)
:jre_exists
	for /f "tokens=2-5 delims=.-_" %%j in ('"%JAVA_HOME%\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSIONNEW=%%j 
			set LESSERVERSIONNEW=%%l
		)
	for /f "tokens=2-5 delims=.-_" %%j in ('"%DIST%\jdk\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSION=%%j 
			set LESSERVERSION=%%l
		)
	if %MAJORVERSIONNEW% NEQ %MAJORVERSION% (
		echo given java version is java version 8 using default resolve java
		set JAVA_HOME=%DIST%\jdk\jre
	)
	if %LESSERVERSIONNEW% LSS %LESSERVERSION% (
		echo given java version is less than resolve's installed java version using default resolve java
		set JAVA_HOME=%DIST%\jdk\jre
	)
	goto finish_run
:finish_run

set ES_MIN_MEM=256M
set ES_MAX_MEM=512M

CALL "%SBIN%\service.bat" install > installLog.log
echo ======================= >> installLog.log

echo The 'rssearch' service has been installed.

set SBIN=
set DIST=
set JAVA_HOME=
set ES_MIN_MEM=
set ES_MAX_MEM=
