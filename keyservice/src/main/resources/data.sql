insert into keys(key_id, creation_ts, expiration_ts, key_type, key_value) values(1, NOW(), NULL, 'LICENSE', 'This is a fairly long phrase used to encrypt');
insert into keys(key_id, creation_ts, expiration_ts, key_type, key_value) values(2, NOW(), NULL, 'DES', 'this is a fairly long phrase - no problems only solutions');
insert into keys(key_id, creation_ts, expiration_ts, key_type, key_value) values(3, NOW(), NULL, 'AES', 'ENC:XgAkfIoSYCVNis5EwVkQCakKjRroa8Gz6qZ6NTfxm4o=');

insert into users(id, username, password) values(1, 'admin', '$2a$10$ng2ch3YOe48qGxL9Eus35uedBraq84O6SCDzfOHQXLgF.Ag3HDJWO');