CREATE TABLE IF NOT EXISTS keys (
    key_id integer not null auto_increment,
    creation_ts datetime not null,
    expiration_ts datetime,
    key_type varchar(255) not null,
    key_value varchar(255) not null,
    primary key (key_id)
);

CREATE TABLE IF NOT EXISTS users (
    id integer not null auto_increment,
    username varchar(255) not null,
    password varchar(255) not null,
    primary key (id)
);
