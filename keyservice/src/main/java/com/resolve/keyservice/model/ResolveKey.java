package com.resolve.keyservice.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "keys")
public class ResolveKey {
	public enum KeyType {
	    DES,
	    AES,
	    LICENSE
	}
	
	@Id
    @Column(name = "key_id")
    @GeneratedValue
	private int id;
	
	@Column(name = "creation_ts", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	
	@Column(name = "expiration_ts")
	@Temporal(TemporalType.TIMESTAMP)
	private Date expirationDate;

	@Enumerated(EnumType.STRING)
	@Column(name = "key_type", nullable = false)
	private KeyType keyType;

	@Column(name = "key_value", nullable = false)
	private String keyValue;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public KeyType getKeyType() {
		return keyType;
	}

	public void setKeyType(KeyType keyType) {
		this.keyType = keyType;
	}

	public String getKeyValue() {
		return keyValue;
	}

	public void setKeyValue(String keyValue) {
		this.keyValue = keyValue;
	}
}
