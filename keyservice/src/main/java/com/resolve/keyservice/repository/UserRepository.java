package com.resolve.keyservice.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.resolve.keyservice.model.User;

@Repository
@RestResource(exported = false)
public interface UserRepository extends CrudRepository<User, Integer> {
	User findByUsername(String username);
}
