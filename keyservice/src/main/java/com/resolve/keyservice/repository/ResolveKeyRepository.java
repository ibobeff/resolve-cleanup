package com.resolve.keyservice.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.resolve.keyservice.model.ResolveKey;
import com.resolve.keyservice.model.ResolveKey.KeyType;

@RepositoryRestResource(path = "keys", itemResourceRel = "keys", collectionResourceRel = "keys")
public interface ResolveKeyRepository extends CrudRepository<ResolveKey, Integer> {
	@RestResource(path = "key", rel = "key")
	ResolveKey findTopByKeyTypeAndExpirationDateIsNullOrderByCreationDateDesc(@Param("keyType") KeyType keyType);
}
