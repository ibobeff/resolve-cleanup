package com.resolve.keyservice;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

import com.resolve.keyservice.model.ResolveKey.KeyType;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ApplicationTest {
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testUnAuthorizedAccess() throws Exception {
		mockMvc.perform(get("/")).andExpect(status().isUnauthorized());
	}

	@Test
	public void testAuthorizedAccess() throws Exception {
		mockMvc.perform(get("/").with(getUser())).andExpect(status().isOk())
				.andExpect(jsonPath("$._links.keys").exists());
	}

	@Test
	public void testGetKeys() throws Exception {
		for (KeyType keyType : KeyType.values()) {
			mockMvc.perform(get(String.format("/keys/search/key?keyType=%s", keyType.name())).with(getUser()))
					.andExpect(status().isOk()).andExpect(jsonPath("$.keyValue").exists());
		}
	}

	private RequestPostProcessor getUser() {
		return user("admin").password("pass").roles("USER", "ADMIN");
	}
}
