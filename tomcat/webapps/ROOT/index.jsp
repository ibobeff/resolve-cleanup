<!DOCTYPE html>
<%@page import="java.util.*,java.net.*" %>
<% 
	Enumeration<String> hdrNames = request.getHeaderNames();
    StringBuilder sb = new StringBuilder("");
	
	while(hdrNames.hasMoreElements())
    {
		if (sb.length() == 0)
		{
			sb.append("?");
		}
		
		String hdrName = hdrNames.nextElement();
		Enumeration<String> hdrValues = request.getHeaders(hdrName);
		
		while(hdrValues.hasMoreElements())
		{
			String hdrValue = hdrValues.nextElement();
//			System.out.println("Adding Parameter " + hdrName + "=" + hdrValue + " to hdrNameValMap.");
			sb.append(hdrName).append("=").append(URLEncoder.encode(hdrValue, "UTF-8")).append("&");
		}
    }
	
    String inputParams = (sb.deleteCharAt(sb.length() - 1)).toString();
//	System.out.println("inputParams=[" + inputParams + "]");%>
<html>
<head>
  <script type="text/javascript">
	document.write('<meta http-equiv="refresh" content="0; url=/resolve' + '<%=inputParams%>' + '">');
  </script>
</head>
</html>
