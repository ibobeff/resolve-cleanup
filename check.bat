findstr /s /i "org.apache.cxf.common.util.StringUtils edu.emory.mathcs.backport.java.util.Arrays edu.emory.mathcs.backport.java.util.Collections" src\*.java
@set ERRLVL=%ERRORLEVEL%
@IF %ERRLVL%==0 EXIT /B 1

findstr /s /r "^[^\"]*\"[^\"]*$" config\*.bat
@set ERRLVL=%ERRORLEVEL%
@IF %ERRLVL%==0 EXIT /B 1

rem findstr /s /i "antlr.StringUtils" src\*.java
rem @set ERRLVL=%ERRORLEVEL%
rem @IF %ERRLVL%==0 EXIT /B 1

rem findstr /s /i "org.gargoylesoftware.htmlunit.util.StringUtils" src\*.java
rem @set ERRLVL=%ERRORLEVEL%
rem @IF %ERRLVL%==0 EXIT /B 1

rem findstr /s /i "com.ibm.wsdl.util.StringUtils" src\*.java
rem @set ERRLVL=%ERRORLEVEL%
rem @IF %ERRLVL%==0 EXIT /B 1

rem findstr /s /i "com.icominfo.Utils.StringUtils" src\*.java
rem @set ERRLVL=%ERRORLEVEL%
rem @IF %ERRLVL%==0 EXIT /B 1

rem findstr /s /i "com.intelliden.util.StringUtils" src\*.java
rem @set ERRLVL=%ERRORLEVEL%
rem @IF %ERRLVL%==0 EXIT /B 1

rem findstr /s /i "com.mysql.jdbc.StringUtils" src\*.java
rem @set ERRLVL=%ERRORLEVEL%
rem @IF %ERRLVL%==0 EXIT /B 1

rem findstr /s /i "com.sun.xml.internal.ws.util.StringUtils" src\*.java
rem @set ERRLVL=%ERRORLEVEL%
rem @IF %ERRLVL%==0 EXIT /B 1

rem findstr /s /i "org.apache.commons.codec.binary.StringUtils" src\*.java
rem @set ERRLVL=%ERRORLEVEL%
rem @IF %ERRLVL%==0 EXIT /B 1

rem findstr /s /i "org.apache.tools.ant.util.StringUtils" src\*.java
rem @set ERRLVL=%ERRORLEVEL%
rem @IF %ERRLVL%==0 EXIT /B 1

rem findstr /s /i "org.apache.velocity.util.StringUtils" src\*.java
rem @set ERRLVL=%ERRORLEVEL%
rem @IF %ERRLVL%==0 EXIT /B 1

rem findstr /s /i "org.apache.woden.internal.util.StringUtils" src\*.java
rem @set ERRLVL=%ERRORLEVEL%
rem @IF %ERRLVL%==0 EXIT /B 1

rem findstr /s /i "org.springframework.util.StringUtils" src\*.java
rem @set ERRLVL=%ERRORLEVEL%
rem @IF %ERRLVL%==0 EXIT /B 1

@IF %ERRLVL%==1 EXIT /B 0
