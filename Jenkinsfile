#!/usr/bin/env groovy

pipeline {
    agent { 
        label 'rsjenkins-slave' 
    }
    options { 
        timestamps()
        disableConcurrentBuilds()
    }
    parameters {
        string(name: 'VERSION', defaultValue: 'trunk', description: 'Version to build? (f.e. 6.5.0.0). Default: trunk')
        choice(name: 'DEV_MODE', choices:"develop\nproduction", description: "Choose development mode (wether the GIT commit hash should be appended to the Resolve version? Default: DEVELOPMENT" )
        gitParameter branchFilter: 'origin/(.*)', defaultValue: '', name: 'BRANCH', sortMode: 'ASCENDING_SMART', type: 'PT_BRANCH_TAG', quickFilterEnabled: true, useRepository: 'https://bitbucket.org/resolvesystems/resolve.git', description: 'Choose branch to build from. Default: develop'
        choice(name: 'TARGET', choices:"COMPLETE\nCOMPLETE-RSSDKDEVELOPEDGATEWAYS\nRELEASE\nBUILD-UPDATE\nBUILD-UPGRADE", description: "Choose Ant target (f.e. COMPLETE). Default: COMPLETE" )
        choice(name: 'BUILD_REPO', choices:"NEXUS\nS3\nBOTH\nSKIP", description: "Location to upload build artifacts. Default: Nexus")
        booleanParam(name: 'CLEAN_WORKSPACE', defaultValue: false, description: 'Clean up workspace on success. Default: false') 
        booleanParam(name: 'BUILD_RC', defaultValue: false, description: 'Build release candidate? Default: false')
    }
    stages {
        stage("User checkout") {
            when {
                triggeredBy cause: "UserIdCause"
            }
            steps {
                git branch: "${params.BRANCH}", url: 'https://bitbucket.org/resolvesystems/resolve.git', credentialsId: '812276fc-0dcc-4944-8d62-25ad4348b556'
            }
        }
        
        stage("Default checkout") {
            when {
                anyOf {
                  triggeredBy 'SCMTrigger';
                  triggeredBy 'TimerTrigger';
                  triggeredBy 'UpstreamCause';
                }
            }
            steps {
                checkout([$class: 'GitSCM', 
                    branches: [[name: '**']], 
                    doGenerateSubmoduleConfigurations: false, 
                    extensions: [[$class: 'CheckoutOption'], 
                    [$class: 'SubmoduleOption', disableSubmodules: false, parentCredentials: true, recursiveSubmodules: true, reference: '', trackingSubmodules: true],
                    [$class: 'PruneStaleBranch'], 
                    [$class: 'CleanCheckout'], 
                    [$class: 'CleanBeforeCheckout']], 
                    submoduleCfg: [], 
                    userRemoteConfigs: [[]]])
            }
        }
        stage("Prepare") {
            steps {
                echo "I'm a placeholder"
            }
        }
        stage("Build") {
            steps {
                buildResolve("${params.VERSION}","${params.DEV_MODE}", "${params.BUILD_RC}", "${params.TARGET}")
            }
        }
        stage("Upload artifacts to Nexus") {
            when {
                expression {
                    return params.BUILD_REPO ==~ /(?i)(Nexus|BOTH)/
                }
            }
            steps {
                uploadNexus("${params.BUILD_RC}")
            }
        }
        stage("Upload artifacts to S3 bucket") {
            when {
                expression {
                    return params.BUILD_REPO ==~ /(?i)(S3|BOTH)/
                }
            }
            steps {
                S3Upload()
            }
        }
        stage("Cleanup") {
            when { 
                expression { return params.CLEAN_WORKSPACE } 
            }
            steps {
                cleanWs()
            }
        }
        stage("Deploy") {
            when { 
                expression { return params.BRANCH ==~ /(?i)develop/ } 
            }
            steps {
                build job: 'Deploy Resolve', 
                propagate: false,
                parameters: [
                    extendedChoice(name: 'JOB_TYPE', value: 'Resolve-update'), 
                    string(name: 'ACTION', value: 'run'), 
                    extendedChoice(name: 'BRANCH', value: 'develop'), 
                    extendedChoice(name: 'TARGET', value: 'dev-build01_10.50.0.206'), 
                    booleanParam(name: 'RUN_TESTS', value: 'true'), 
                    string(name: 'UI_TESTS', value: 'Smoke')
                ]
            }
        }
    }
    post {
        success {
            notify("SUCCESSFUL")
            cleanWorkspace()
        }
        
        failure {
            notify("FAILED")
        }
    }
}
