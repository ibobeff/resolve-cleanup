import com.jagacy.Session3270;
import com.jagacy.util.JagacyException;
import com.jagacy.util.JagacyProperties;

/**
 * This class is unique. Each method is a simple state machine totally driven by
 * the properties file. It never has to be changed or recompiled! Its better
 * than a macro language.
 * 
 * @author Robert M. Preston
 * 
 */
public class Example4 extends Session3270 {

    private JagacyProperties props;

    private Example4() throws JagacyException {
        super("example4");
        props = getProperties();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.Session3270#logon()
     */
    protected boolean logon() throws JagacyException {
        // Notice that you don't have to prefix each property with 'example4'.
        // Jagacy will do this for you.
        boolean isWait = true;
        for (String state = "Begin"; true;) {
            if (isWait) {
                waitForChange("logon.timeout." + state + ".seconds");
            }
            if (!waitForPosition("logon.position." + state, 0)) {
                state = props.get("logon.nextState.alt." + state);
                continue;
            }
            if (props.getBoolean("logon.done." + state, false)) {
                break;
            }
            if (props.getBoolean("logon.entry." + state, true)) {
                writePosition("logon.entry." + state);
            }

            isWait = writeKey("logon.key." + state).isWaitKey();
            state = props.get("logon.nextState." + state);
        }

        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.Session3270#logoff()
     */
    protected void logoff() throws JagacyException {
        boolean isWait = false;
        for (String state = "Begin"; true;) {
            if (props.getBoolean("logoff.done." + state, false)) {
                break;
            }
            if (isWait) {
                waitForChange("logoff.timeout." + state + ".seconds");
            }
            if (!waitForPosition("logoff.position." + state, 0)) {
                state = props.get("logoff.nextState.alt." + state);
                continue;
            }
            if (props.getBoolean("logoff.entry." + state, true)) {
                writePosition("logoff.entry." + state);
            }
            isWait = writeKey("logoff.key." + state).isWaitKey();
            state = props.get("logoff.nextState." + state);
        }
    }

    /**
     * Retrieves the first 5 patents that begin with Java and writes them to
     * System.out.
     * 
     * @throws JagacyException If an error occurs.
     */
    private void getJavaPatents() throws JagacyException {
        boolean isWait = false;
        for (String state = "Begin"; true;) {
            if (isWait) {
                waitForChange("data.timeout." + state + ".seconds");
            }
            if (!waitForPosition("data.position." + state, 0)) {
                state = props.get("data.nextState.alt." + state);
                continue;
            }
            if (props.getBoolean("data.done." + state, false)) {
                break;
            }
            if (props.getBoolean("data.entry." + state, true)) {
                writePosition("data.entry." + state);
            }
            isWait = writeKey("data.key." + state).isWaitKey();
            state = props.get("data.nextState." + state);
        }

        int startColumn = props.getCardinal("data.startColumn");
        for (int i = props.getCardinal("data.beginRow"), length = props
            .getCardinal("data.endRow"); i < length; i++) {

            System.out.println(readPosition(i, startColumn, getWidth()
                - startColumn));
        }
    }

    /**
     * Print exception(s) to System.err.
     * 
     * @param e JagacyException
     */
    private static void printExceptions(JagacyException e) {
        System.err.println(e);
        if (e.hasException()) {
            System.err.println(e.getException());
        }
    }

    public static void main(String[] args) {
        Example4 example = null;
        try {
            example = new Example4();
            example.open();
            example.getJavaPatents();
            example.close();
        } catch (JagacyException e) {
            printExceptions(e);
            if (example != null) {
                example.abort();
            }
        }

        if ((example != null) && example.props.getBoolean("window", false)) {
            // Swing requires this if window is enabled.
            System.exit(0);
        }
    }
}