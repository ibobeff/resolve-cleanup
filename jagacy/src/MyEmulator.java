import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Iterator;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import com.jagacy.AbstractSession;
import com.jagacy.Key;
import com.jagacy.Seconds;
import com.jagacy.Session3270;
import com.jagacy.ui.AbstractPanel;
import com.jagacy.ui.AbstractSwing;
import com.jagacy.ui.Panel3270;
import com.jagacy.ui.Swing3270;
import com.jagacy.ui.UserInterface;
import com.jagacy.util.JagacyException;
import com.jagacy.util.LinkedHashMap;

/**
 * Implements a Patent session.
 * 
 * @author Robert M. Preston
 * 
 */
class PatentSession extends Session3270 implements Seconds {
    private UserInterface myUi;

    /**
     * Creates a Patent session.
     * 
     * @param name The session name (also the name of the .properties file).
     * @param ui The UI used to display the session.
     * @throws JagacyException If an error occurs.
     */
    PatentSession(String name, UserInterface ui) throws JagacyException {
        super(name, "pericles.ipaustralia.gov.au", "IBM-3278-2");
        myUi = ui;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.Session3270#createUi()
     */
    protected UserInterface createUi() throws JagacyException {
        return myUi;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.AbstractSession#logon()
     */
    protected boolean logon() throws JagacyException {
        if (!waitForPosition(10, 20, "A U S T R A L I A", THIRTY_SECONDS)) {
            return false;
        }

        writeKey(Key.ENTER);

        if (!waitForPosition(5, 1, "DISCLAIMER", FIVE_SECONDS)) {
            return false;
        }

        writeKey(Key.ENTER);

        if (!waitForPosition(2, 72, "ZMENUM2", FIVE_SECONDS)) {
            return false;
        }

        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.AbstractSession#logoff()
     */
    protected void logoff() throws JagacyException {
        while (true) {
            writeKey(Key.PF3);
            waitForChange(TEN_SECONDS);

            if (waitForPosition(2, 72, "ZMENUM2", 0)) {
                break;
            }
        }
        writeKey(Key.PF3);
    }
}

/**
 * Implements a specialized Swing GUI.
 * 
 * @author Robert M. Preston
 * 
 */
public class MyEmulator extends AbstractSwing {

    private static final long serialVersionUID = 8831901593363366980L;

    private static final ImageIcon ICON = new ImageIcon(MyEmulator.class
        .getResource("images/book.jpg"));

    private LinkedHashMap myLookAndFeelMap = new LinkedHashMap();

    private boolean myIsCancelled;

    /**
     * Creates the GUI.
     * 
     * @throws JagacyException If an error occurs.
     */
    private MyEmulator() throws JagacyException {
        super("Patents");

        myLookAndFeelMap
            .put("Metal", "javax.swing.plaf.metal.MetalLookAndFeel");
        myLookAndFeelMap.put("Motif",
            "com.sun.java.swing.plaf.motif.MotifLookAndFeel");
        myLookAndFeelMap.put("Windows",
            "com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        myLookAndFeelMap.put("Mac", "javax.swing.plaf.mac.MacLookAndFeel");

        // Buggy.
        // myLookAndFeelMap.put("GTK",
        // "com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.ui.AbstractSwing#createSession(java.lang.String)
     */
    protected AbstractSession createSession(String name) throws JagacyException {
        return new PatentSession(name, this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.ui.AbstractSwing#createPanel()
     */
    protected AbstractPanel createPanel() throws JagacyException {
        return new Panel3270(this) {
            private static final long serialVersionUID = 299834078527771447L;

            /*
             * (non-Javadoc)
             * 
             * @see com.jagacy.ui.AbstractPanel#processRightClick(int, int)
             */
            protected void processRightClick(int row, int column) {
            }

            /*
             * (non-Javadoc)
             * 
             * @see com.jagacy.ui.AbstractPanel#processKey(java.awt.event.KeyEvent)
             */
            public void processKey(KeyEvent event) {
                super.processKey(event);
            }
        };
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.ui.AbstractSwing#getProductName()
     */
    protected String getProductName() {
        return "MyEmulator";
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.ui.AbstractSwing#getIcon()
     */
    protected ImageIcon getIcon() {
        return ICON;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.ui.AbstractSwing#connect()
     */
    protected boolean connect() {
        boolean connected = false;
        try {
            getSession().open();
            connected = true;
        } catch (JagacyException e) {
            AbstractSwing.printExceptions(e);
        }

        return connected;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.ui.AbstractSwing#addMenu(javax.swing.JMenu)
     */
    protected JMenu addMenu(JMenu menu) {
        JMenu newMenu = null;
        if (menu.getActionCommand().equals("Edit")) {
            newMenu = createMenu("Options", KeyEvent.VK_P);
            newMenu.add(createMenuItem("Look and Feel", KeyEvent.VK_N));
        }
        return newMenu;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.ui.AbstractSwing#addMenuItem(javax.swing.JMenuItem)
     */
    protected JMenuItem addMenuItem(JMenuItem menuItem) {
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.ui.AbstractSwing#addUserComponent(int)
     */
    protected JComponent addUserComponent(int index) {
        switch (index) {
            case 0:
                return getCursorComponent();
            case 1:
                return createEmptyComponent();
                // return getTimeComponent();
            case 2:
                return createEmptyComponent();
        }

        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.ui.AbstractSwing#addButton(javax.swing.JButton)
     */
    protected JButton addButton(JButton button) {
        JButton newButton = null;
        if (button.getActionCommand().equals("Paste")) {
            newButton = createButton(MyEmulator.class, "images/duke.gif",
                "Look and Feel", "Defines the look and feel of the program");
        }
        return newButton;
    }

    /**
     * Implements the Look and Feel dialog box.
     */
    private void changeLookAndFeel() {
        myIsCancelled = false;
        final JDialog dialog = new JDialog(this, "Look and Feel", true);
        dialog.getContentPane().setLayout(new BorderLayout());
        dialog.setResizable(false);
        KeyAdapter finished = new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    dialog.setVisible(false);
                }
            }
        };
        JButton ok = new JButton("OK");
        ok.setFont(CONTROL_FONT);
        ok.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                dialog.setVisible(false);
            }
        });
        ok.addKeyListener(finished);

        JButton cancel = new JButton("Cancel");
        cancel.setFont(CONTROL_FONT);
        cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                dialog.setVisible(false);
                myIsCancelled = true;
            }
        });

        JPanel panel = new JPanel();
        panel.add(Box.createHorizontalGlue());
        panel.add(ok);
        panel.add(cancel);
        panel.add(Box.createHorizontalGlue());
        dialog.getContentPane().add(panel, BorderLayout.SOUTH);

        panel = new JPanel();
        final JComboBox combo = new JComboBox();
        combo.setFont(CONTROL_FONT);
        for (Iterator i = myLookAndFeelMap.keySet().iterator(); i.hasNext();) {
            String key = (String)i.next();
            try {
                Class.forName((String)myLookAndFeelMap.get(key));
                combo.addItem(key);
            } catch (ClassNotFoundException e) {
            }
        }
        combo.setSelectedIndex(0);
        panel.add(combo);
        dialog.getContentPane().add(panel, BorderLayout.CENTER);

        ok.grabFocus();

        dialog.pack();
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);

        if (myIsCancelled) {
            return;
        }

        try {
            UIManager.setLookAndFeel((String)myLookAndFeelMap.get(combo
                .getSelectedItem()));
            SwingUtilities.updateComponentTreeUI(this);
            pack();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.ui.AbstractSwing#processAction(java.awt.event.ActionEvent)
     */
    protected boolean processAction(ActionEvent event) {
        boolean processed = false;
        String command = event.getActionCommand();
        if (command.startsWith("About ")) {
            JOptionPane.showMessageDialog(this, getProductName() + "\n"
                + "Copyright " + COPYRIGHT_SIGN_SYMBOL
                + " 2006-2008 My Software.\nAll Rights Reserved.\n", "About "
                + getProductName(), JOptionPane.INFORMATION_MESSAGE, ICON);
            processed = true;
        } else if (command.equals("Look and Feel")) {
            changeLookAndFeel();
            processed = true;
        }
        return processed;
    }

    /**
     * Creates a specialized Swing GUI.
     * 
     * @param args Command line parameters.
     */
    public static void main(String[] args) {
        try {
            new MyEmulator();
        } catch (JagacyException e) {
            AbstractSwing.notify(new Swing3270(), ERROR_LEVEL,
                "MyEmulator Error", e.getMessage() + "\n");
            AbstractSwing.printExceptions(e);
            System.exit(1);
        }
    }
}
