import com.jagacy.Key;
import com.jagacy.Session3270;
import com.jagacy.util.JagacyException;
import com.jagacy.util.JagacyProperties;
import com.jagacy.util.Loggable;

/**
 * This class doesn't use fields or positions to retrieve the first 5 patents
 * that begin with java. Not as robust as the other examples.
 * 
 * @author Robert M. Preston
 * 
 */
public class Example1 extends Session3270 {

    private JagacyProperties props;

    private Loggable logger;

    private Example1() throws JagacyException {
        super("example1");
        props = getProperties();
        logger = getLoggable();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.Session3270#logon()
     */
    protected boolean logon() throws JagacyException {
        // Notice that you don't have to prefix each property with 'example1'.
        // Jagacy will do this for you.

        // Intermediate screen.
        waitForChange("logon.timeout.seconds");
        if (readRow(10).indexOf(props.get("logon.id")) == -1) {
            logger.fatal("Not logon screen");
            return false;
        }

        writeKey(Key.ENTER);

        waitForChange("disclaimer.timeout.seconds");
        if (readRow(5).indexOf(props.get("disclaimer.id")) == -1) {
            logger.fatal("Not disclaimer screen");
            return false;
        }

        writeKey(Key.ENTER);

        // Intermediate screens.
        do {
            waitForChange("menu.timeout.seconds");
            if (readRow(2).indexOf(props.get("menu.id")) != -1) {
                break;
            }
        } while (true);

        writeAfterLabel("menu");
        writeKey(Key.ENTER);

        waitForChange("admin.timeout.seconds");
        if (readRow(2).indexOf(props.get("admin.id")) == -1) {
            logger.fatal("Not admin screen");
            return false;
        }

        writeAfterLabel("admin");
        writeKey(Key.ENTER);

        waitForChange("patadmin.timeout.seconds");
        if (readRow(23).indexOf(props.get("patadmin.id")) == -1) {
            logger.fatal("Not patadmin screen");
            return false;
        }

        writeKey(Key.ENTER);

        waitForChange("patent.timeout.seconds");
        if (readRow(2).indexOf(props.get("patent.id")) == -1) {
            logger.fatal("Not patent screen");
            return false;
        }

        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.Session3270#logoff()
     */
    protected void logoff() throws JagacyException {
        while (readRow(2).indexOf(props.get("logoff.id")) == -1) {
            writeKey(Key.PF3);
            waitForChange("logoff.timeout.seconds");
        }
        writeKey(Key.PF3);
    }

    /**
     * Retrieves the first 5 patents that begin with java and writes them to
     * System.out.
     * 
     * @throws JagacyException If an error occurs.
     */
    private void getJavaPatents() throws JagacyException {
        if (readRow(2).indexOf(props.get("patent.id")) == -1) {
            logger.fatal("Not patent screen");
            return;
        }

        writeString("java");
        writeKey(Key.ENTER);

        waitForChange("data.timeout.seconds");
        if (readRow(2).indexOf(props.get("data.id")) == -1) {
            logger.fatal("Not data screen");
            return;
        }

        int startColumn = props.getCardinal("data.startColumn");
        for (int i = props.getCardinal("data.beginRow"), length = props
            .getCardinal("data.endRow"); i < length; i++) {

            System.out.println(readPosition(i, startColumn, getWidth()
                - startColumn));
        }
    }

    /**
     * Print exception(s) to System.err.
     * 
     * @param e JagacyException
     */
    private static void printExceptions(JagacyException e) {
        System.err.println(e);
        if (e.hasException()) {
            System.err.println(e.getException());
        }
    }

    public static void main(String[] args) {
        Example1 example = null;
        try {
            example = new Example1();
            example.open();
            example.getJavaPatents();
            example.close();
        } catch (JagacyException e) {
            printExceptions(e);
            if (example != null) {
                example.abort();
            }
        }

        if ((example != null) && example.props.getBoolean("window", false)) {
            // Swing requires this if window is enabled.
            System.exit(0);
        }
    }
}