import com.jagacy.Key;
import com.jagacy.Session3270;
import com.jagacy.util.JagacyException;
import com.jagacy.util.JagacyProperties;
import com.jagacy.util.Loggable;

/**
 * This class uses positions to retrieve the first 5 patents that begin with
 * java.
 * 
 * @author Robert M. Preston
 * 
 */
public class Example2 extends Session3270 {

    private JagacyProperties props;

    private Loggable logger;

    private Example2() throws JagacyException {
        super("example2");
        props = getProperties();
        logger = getLoggable();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.Session3270#logon()
     */
    protected boolean logon() throws JagacyException {
        // Notice that you don't have to prefix each property with 'example2'.
        // Jagacy will do this for you.
        if (!waitForPosition("logon.wait", "logon.timeout.seconds")) {
            logger.fatal("Not logon screen");
            return false;
        }

        writeKey(Key.ENTER);

        if (!waitForPosition("disclaimer.wait", "disclaimer.timeout.seconds")) {
            logger.fatal("Not disclaimer screen");
            return false;
        }

        writeKey(Key.ENTER);

        if (!waitForPosition("menu.wait", "menu.timeout.seconds")) {
            logger.fatal("Not menu screen");
            return false;
        }

        writePosition("menu.entry");
        writeKey(Key.ENTER);

        if (!waitForPosition("admin.wait", "admin.timeout.seconds")) {
            logger.fatal("Not admin screen");
            return false;
        }

        writePosition("admin.entry");
        writeKey(Key.ENTER);

        if (!waitForPosition("patadmin.wait", "patadmin.timeout.seconds")) {
            logger.fatal("Not patadmin screen");
            return false;
        }

        writeKey(Key.ENTER);

        if (!waitForPosition("patent.wait", "patent.timeout.seconds")) {
            logger.fatal("Not patent screen");
            return false;
        }

        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.Session3270#logoff()
     */
    protected void logoff() throws JagacyException {

        while (true) {
            writeKey(Key.PF3);
            waitForChange("logoff.timeout.seconds");

            if (waitForPosition("logoff.wait", 0)) {
                break;
            }
        }
        writeKey(Key.PF3);
    }

    /**
     * Retrieves the first 5 patents that begin with Java and writes them to
     * System.out.
     * 
     * @throws JagacyException If an error occurs.
     */
    private void getJavaPatents() throws JagacyException {
        if (!waitForPosition("patent.wait", "patent.timeout.seconds")) {
            logger.fatal("Not patent screen");
            return;
        }

        writePosition("patent.entry", "java");
        writeKey(Key.ENTER);

        if (!waitForPosition("data.wait", "data.timeout.seconds")) {
            logger.fatal("Not data screen");
            return;
        }

        int startColumn = props.getCardinal("data.startColumn");
        for (int i = props.getCardinal("data.beginRow"), length = props
            .getCardinal("data.endRow"); i < length; i++) {

            System.out.println(readPosition(i, startColumn, getWidth()
                - startColumn));
        }
    }

    /**
     * Print exception(s) to System.err.
     * 
     * @param e JagacyException
     */
    private static void printExceptions(JagacyException e) {
        System.err.println(e);
        if (e.hasException()) {
            System.err.println(e.getException());
        }
    }

    public static void main(String[] args) {
        Example2 example = null;
        try {
            example = new Example2();
            example.open();
            example.getJavaPatents();
            example.close();
        } catch (JagacyException e) {
            printExceptions(e);
            if (example != null) {
                example.abort();
            }
        }

        if ((example != null) && example.props.getBoolean("window", false)) {
            // Swing requires this if window is enabled.
            System.exit(0);
        }
    }
}