import com.jagacy.Key;
import com.jagacy.Session3270;
import com.jagacy.util.JagacyException;
import com.jagacy.util.JagacyProperties;
import com.jagacy.util.Loggable;

/**
 * This class uses fields to retrieve the first 5 patents that begin with java.
 * 
 * @author Robert M. Preston
 * 
 */
public class Example3 extends Session3270 {

    private JagacyProperties props;

    private Loggable logger;

    private Example3() throws JagacyException {
        super("example3");
        props = getProperties();
        logger = getLoggable();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.Session3270#logon()
     */
    protected boolean logon() throws JagacyException {
        // Notice that you don't have to prefix each property with 'example3'.
        // Jagacy will do this for you.
        if (!waitForField("logon.wait", "logon.timeout.seconds")) {
            logger.fatal("Not logon screen");
            return false;
        }

        writeKey(Key.ENTER);

        if (!waitForField("disclaimer.wait", "disclaimer.timeout.seconds")) {
            logger.fatal("Not disclaimer screen");
            return false;
        }

        writeKey(Key.ENTER);

        if (!waitForField("menu.wait", "menu.timeout.seconds")) {
            logger.fatal("Not menu screen");
            return false;
        }

        writeField("menu.entry");
        writeKey(Key.ENTER);

        if (!waitForField("admin.wait", "admin.timeout.seconds")) {
            logger.fatal("Not admin screen");
            return false;
        }

        writeField("admin.entry");
        writeKey(Key.ENTER);

        if (!waitForField("patadmin.wait", "patadmin.timeout.seconds")) {
            logger.fatal("Not patadmin screen");
            return false;
        }

        writeKey(Key.ENTER);

        if (!waitForField("patent.wait", "patent.timeout.seconds")) {
            logger.fatal("Not patent screen");
            return false;
        }

        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.jagacy.Session3270#logoff()
     */
    protected void logoff() throws JagacyException {
        while (true) {
            writeKey(Key.PF3);
            waitForChange("logoff.timeout.seconds");

            if (waitForField("logoff.wait", 0)) {
                break;
            }
        }
        writeKey(Key.PF3);
    }

    /**
     * Retrieves the first 5 patents that begin with java and writes them to
     * System.out.
     * 
     * @throws JagacyException If an error occurs.
     */
    private void getJavaPatents() throws JagacyException {
        if (!waitForField("patent.wait", "patent.timeout.seconds")) {
            logger.fatal("Not patent screen");
            return;
        }

        writeField("patent.entry", "java");
        writeKey(Key.ENTER);

        if (!waitForField("data.wait", "data.timeout.seconds")) {
            logger.fatal("Not data screen");
            return;
        }

        int startColumn = props.getCardinal("data.startColumn");
        for (int i = props.getCardinal("data.beginRow"), length = props
            .getCardinal("data.endRow"); i < length; i++) {

            System.out.println(readPosition(i, startColumn, getWidth()
                - startColumn));
        }
    }

    /**
     * Print exception(s) to System.err.
     * 
     * @param e JagacyException
     */
    private static void printExceptions(JagacyException e) {
        System.err.println(e);
        if (e.hasException()) {
            System.err.println(e.getException());
        }
    }

    public static void main(String[] args) {
        Example3 example = null;
        try {
            example = new Example3();
            example.open();
            example.getJavaPatents();
            example.close();
        } catch (JagacyException e) {
            printExceptions(e);
            if (example != null) {
                example.abort();
            }
        }

        if ((example != null) && example.props.getBoolean("window", false)) {
            // Swing requires this if window is enabled.
            System.exit(0);
        }
    }
}