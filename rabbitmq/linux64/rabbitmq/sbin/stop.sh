#!/bin/bash

# properties
BIN=`echo $0 | awk '{ print substr( $0, 0, length($0)-7 ) }'`

cd ${BIN}
DIST=`pwd`

LOCKFILE=${DIST}/lock

if [ ! -f lock ]; then
    echo "Lock File Not Found...Exiting"
    exit 1
fi

LOCK=`cat lock`

#kill ${LOCK}
echo "Stopping RSMQ PID: ${LOCK}"

./rabbitmqctl stop &

rm lock

echo "RSMQ stopped"
