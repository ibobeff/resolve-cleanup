#!/bin/bash

# properties
BIN=`echo $0 | awk '{ print substr( $0, 0, length($0)-6 ) }'`

cd ${BIN}
DIST=`pwd`
PRIMARY=TRUE

export RABBITMQ_PID_FILE=${DIST}/lock

if [ -f ${RABBITMQ_PID_FILE} ]; then
    if [ $# -eq 0 ] || [ "$1" != "1" ]; then
        echo "Lock File Detected"
        echo "Cancelling RSMQ Startup"
        exit 1
    fi
fi

rm -rf ${DIST}/../../../db
echo "Starting RSMQ"
./rabbitmq-server &
./rabbitmqctl wait $RABBITMQ_PID_FILE
`ps -ef | grep "${ERLANG_HOME}/erts-7.3/bin/beam" | awk '/beam/ && !/awk/ && !/grep/ {print $2}' | tr '\n' ' ' > $RABBITMQ_PID_FILE`
if [ $? -eq 0 ] ; then
    ./setup_user.sh
else
    if [ "${PRIMARY}" = "FALSE" ]
    then
        sleep 5
    fi
    ./rabbitmq-server &
    ./rabbitmqctl wait $RABBITMQ_PID_FILE
	`ps -ef | grep "${ERLANG_HOME}/erts-7.3/bin/beam" | awk '/beam/ && !/awk/ && !/grep/ {print $2}' | tr '\n' ' ' > $RABBITMQ_PID_FILE`
    if [ $? -eq 0 ] ; then
        ./setup_user.sh
    fi
fi

PID=`cat $RABBITMQ_PID_FILE`
echo "Started RSMQ pid: ${PID}"
