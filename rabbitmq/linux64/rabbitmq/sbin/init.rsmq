#!/bin/bash
#
# init.rsmq
#
# Copyright (c) 2008 Resolve Systems
# All rights reserved.
#
### RedHat Service Information follows:
#
# By default, install service for runlevels 3 and 5, with
# start priority of 91 and stop priority of 09.
#
# chkconfig: 35 91 09
# description: Resolve RSMQ
#

R_USER=resolve
DIST=INSTALLDIR
LOCKFILE=$DIST/rabbitmq/linux64/rabbitmq/sbin/lock
TERMINATETIMEOUT=30
RETVAL=0

success()
{
    printf "OK"
}

failure()
{
    printf "FAILED"
}

status()
{
    if [ -f ${LOCKFILE} ] ; then
        pid=`cat ${LOCKFILE} | tr '\n' ' '`
        ps -p ${pid} > /dev/null 2>&1
        if [ $? -ne 0 ] ; then
           if [ -z "$pid" ]; then
               printf "DOWN"
           else
               printf "DOWN PID: "$pid""
           fi
            rm -f ${LOCKFILE}
        else
            printf "UP PID: "$pid""
        fi
    else
        pid=""
        printf "DOWN"
    fi

    linux=false
    sunos=false
    case "`uname`" in
    Linux*) linux=true;;
    SunOS*) sunos=true;;
    esac

    if $sunos; then
        PROCESS=`/usr/ucb/ps -augxwww | grep "${DIST}/rabbitmq/linux64/erlang/erts-7.3/bin/beam" | nawk '/beam/ && !/awk/ && !/grep/ {print $2}' | tr '\n' ' '`
    else
        PROCESS=`ps -ef | grep "${DIST}/rabbitmq/linux64/erlang/erts-7.3/bin/beam" | awk '/beam/ && !/awk/ && !/grep/ {print $2}' | tr '\n' ' '`
    fi

    if [ "$PROCESS" != "${pid}" ]; then
        printf "\nHANGING $PROCESS"
    fi
}

util_inc ()
{
    cntvar=$1
    val="`eval echo \\$${cntvar}`"
    eval ${cntvar}=`expr ${val} + 1`
}

case "$1" in
start_msg)
    echo "Starting RSMQ"
    ;;

stop_msg)
    echo "Stopping RSMQ"
    ;;

status)
    printf "RSMQ: "
    status
    printf "\n"
    ;;

start)
    ID=`id | sed -e "s/^uid=[0-9]*(\(.*\)) gid.*/\1/"`
    cd $DIST/rabbitmq/linux64/rabbitmq/sbin
    printf "Starting RSMQ "
    if [ "${ID}x" = "${R_USER}x" ]; then
        "./run.sh" 1 > "$DIST/rabbitmq/linux64/rabbitmq/sbin/stdout" 2>&1
    else
        /bin/su $R_USER "./run.sh" 1 > "$DIST/rabbitmq/linux64/rabbitmq/sbin/stdout" 2>&1
    fi

    if [ $? -eq 0 ]; then
        success
    else
        failure
    fi
    printf "\n"
    if [ $? -ne 0 ] ; then
        exit 1
    fi
    ;;

start_safe)
    ID=`id | sed -e "s/^uid=[0-9]*(\(.*\)) gid.*/\1/"`
    cd $DIST/rabbitmq/linux64/rabbitmq/sbin
    echo "Starting RSMQ "
    if [ "${ID}x" = "${R_USER}x" ]; then
        "./run.sh"
    else
        /bin/su $R_USER "./run.sh"
    fi

    if [ $? -ne 0 ] ; then
        exit 1
    fi
    ;;

stop)
    if [ -f ${LOCKFILE} ] ; then
        printf "Stopping RSMQ: "
        RETVAL=1

        pid=`cat ${LOCKFILE}`
        [ "$pid" != "" ] && kill -TERM ${pid}

        if [ $? -eq 0 ] ; then
            cnt=0
            while [ ${cnt} -lt ${TERMINATETIMEOUT} ] ; do
                printf "."
                sleep 1
                ps -p ${pid} > /dev/null 2>&1
                if [ $? -ne 0 ] ; then
                    break
                fi
                util_inc cnt
            done
        else
            rm -f "${LOCKFILE}"
        fi

        ps -p ${pid} > /dev/null 2>&1
        if [ $? -eq 0 ] ; then
            printf "Timeout\n"
            printf "Cannot stop RSMQ - killing it..."
            # Signal USR1 (16) will dump debug information to the agent log
            kill -16 ${pid}
            sleep 1
            kill -9 ${pid}
            sleep 2
            ps -p ${pid} > /dev/null 2>&1
            if [ $? -eq 0 ] ; then
                printf "Cannot kill RSMQ - giving up.\n"
                RETVAL = 0
            fi
        fi
		${DIST}/rabbitmq/linux64/erlang/erts-7.3/bin/epmd -kill
        rm -f ${LOCKFILE}

        if [ $RETVAL -eq 1 ] ; then
            success
            RETVAL=0
        else
            failure
            RETVAL=1
        fi
        printf "\n"
    fi
    ;;

restart)
    $0 stop
    $0 start
    RETVAL=$?
    ;;

*)
    echo "Usage: $0 {start|start_safe|stop|restart|status}"
    exit 1
    ;;
esac
exit $RETVAL
