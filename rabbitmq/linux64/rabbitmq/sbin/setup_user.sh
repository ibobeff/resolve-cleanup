#!/bin/bash

BIN=`echo $0 | awk '{ print substr( $0, 0, length($0)-13 ) }'`

cd ${BIN}
DIST=`pwd`

cd $DIST

for user in `./rabbitmqctl list_users -q | awk '{print $1}'`; do
    ./rabbitmqctl delete_user $user
done
./rabbitmqctl add_user admin resolve
./rabbitmqctl set_user_tags admin administrator
./rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"
./rabbitmqctl set_policy federate-me "." '{"ha-mode":"all"}'
