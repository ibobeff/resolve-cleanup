@echo off

set SBIN=%~dp0

REM ##############################################
REM ## call this file to set environment variables
REM ##############################################

CALL "%SBIN%\rabbitmq-service.bat" remove

CALL "%SBIN%\..\..\erlang\erts-7.3\bin\epmd.exe" -kill

reg delete "HKLM\SOFTWARE\Ericsson\Erlang\ErlSrv\1.1\RSMQ" /f 2>nul

del /f "%WINDIR%\.erlang.cookie"

set SBIN=
