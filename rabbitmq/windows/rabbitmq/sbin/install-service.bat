@echo off

set SBIN=%~dp0

REM ##############################################
REM ## call this file to set environment variables
REM ##############################################

REM ## need to copy .erlang.cookie file to the %WINDIR% or cluster will have problem
copy "%SBIN%\..\..\..\config\.erlang.cookie" "%WINDIR%"

CALL "%SBIN%\rabbitmq-service.bat" install > "%SBIN%\installLog.log"
echo ======================= >> "%SBIN%\installLog.log"
CALL "%SBIN%\rabbitmq-service.bat" list >> "%SBIN%\installLog.log"
