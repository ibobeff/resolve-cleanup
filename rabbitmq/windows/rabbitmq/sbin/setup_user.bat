@echo off

pushd %~dp0

for /f %%a in (
'rabbitmqctl.bat list_users -q'
) do (
  if not "%%a" == "RabbitMQ" (
      call rabbitmqctl.bat delete_user %%a
  )
) 
call rabbitmqctl.bat add_user admin resolve
call rabbitmqctl.bat set_user_tags admin administrator
call rabbitmqctl.bat set_permissions -p / admin ".*" ".*" ".*"
call rabbitmqctl.bat set_policy federate-me "." "{""ha-mode"":""all""}"
popd
