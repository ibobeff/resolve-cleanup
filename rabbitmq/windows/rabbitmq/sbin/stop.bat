@echo off

set SBIN=%~dp0

REM ##############################################
REM ## call this file to set environment variables
REM ##############################################
CALL "%SBIN%\rabbitmq-env.bat"

CALL "%SBIN%\rabbitmqctl.bat" stop

CALL "%SBIN%\..\..\erlang\erts-5.10.1\bin\epmd.exe" -kill

set SBIN=
