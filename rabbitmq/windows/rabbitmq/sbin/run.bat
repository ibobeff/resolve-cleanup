@echo off

set SBIN=%~dp0

REM ##############################################
REM ## call this file to set environment variables
REM ##############################################

del /F /Q %SBIN%\..\..\..\db

CALL "%SBIN%\rabbitmq-env.bat"

CALL "%SBIN%\rabbitmq-server.bat" -detached

CALL "%SBIN%\setup_user.bat"

set SBIN=
