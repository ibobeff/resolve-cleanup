#!/bin/bash

BIN=`echo $0 | awk '{ print substr( $0, 0, length($0)-13 ) }'`

cd ${BIN}
DIST=`pwd`

cd $DIST

./rabbitmqctl delete_user guest
./rabbitmqctl add_user admin resolve
./rabbitmqctl set_user_tags admin administrator
./rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"
./rabbitmqctl set_policy federate-me "." '{"ha-mode":"all"}'
