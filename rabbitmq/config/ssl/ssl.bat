set HOSTNAME=%COMPUTERNAME%
REM set HOSTNAME=DGTBYR1.generationetech.com
REM set HOSTNAME=localhost
echo %HOSTNAME%

mkdir resolveca
mkdir server
mkdir client

copy openssl.cnf resolveca
cd resolveca
mkdir certs private
echo 01 > serial
type NUL > index.txt
openssl req -x509 -config openssl.cnf -newkey rsa:2048 -days 3650 -out cacert.pem -outform PEM -subj /CN=ResolveCA/ -nodes
openssl x509 -in cacert.pem -out cacert.cer -outform DER

cd ..\server
openssl genrsa -out key.pem 2048
openssl req -new -key key.pem -out req.pem -outform PEM -subj /CN=%HOSTNAME%/O=server/ -nodes

cd ..\resolveca
openssl ca -config openssl.cnf -in ..\server\req.pem -out ..\server\cert.pem -notext -batch -extensions server_ca_extensions

cd ..\server
openssl pkcs12 -export -out keycert.p12 -in cert.pem -inkey key.pem -passout pass:resolve

cd ..\client
openssl genrsa -out key.pem 2048
openssl req -new -key key.pem -out req.pem -outform PEM -subj /CN=%HOSTNAME%/O=client/ -nodes

cd ..\resolveca
openssl ca -config openssl.cnf -in ..\\client\\req.pem -out ..\\client\\cert.pem -notext -batch -extensions client_ca_extensions

cd ..\client
openssl pkcs12 -export -out keycert.p12 -in cert.pem -inkey key.pem -passout pass:resolve

cd ..\