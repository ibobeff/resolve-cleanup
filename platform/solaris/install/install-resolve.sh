#!/bin/bash

if [ $# -lt 1 ]; then
   echo ""
   echo "Usage: <BLUEPRINT> [<OPTIONS>] [--force]"
   echo ""
   echo "BLUEPRINT - Location of Blueprint file with Resolve configuration"
   echo "            DEFAULT for default install"
   echo ""
   echo "OPTION     - Options to be passed into the RSMgmt run script"
   echo ""
   echo "  -lic      License File for Resolve Installation"
   echo ""
   echo "  -dns      DNS Host Name to set into rsview properties for system url"
   echo ""
   echo "  -port     Host Port to set into rsview properties for system url"
   echo ""
   echo "  -u        MySQL Root Username used to create the Resolve database specified in the blueprint"
   echo ""
   echo "  -p        MySQL Root Password"
   echo ""
   echo "  -cu       RSConsole username to run default imports"
   echo "            only needed if non-default RSConsole username is used in the blueprint"
   echo ""
   echo "  -cp       RSConsole password to run default imports"
   echo "            only needed if non-default RSConsole password is used in the blueprint"
   echo ""
   echo " --force   Continue installation even if validations that check if Resolve is already"
   echo "           installed or running on the system fail"
   echo ""
   echo " --continue Resume a failed installation, skipping some duplicate steps such as unpacking the"
   echo "            component tar.gz files"
   echo ""
   echo "This script will install the Resolve components in the current directory and start Resolve."
   echo "The Blueprint file specified will be used to configure the Resolve installation"
   echo ""
   echo "If a default installation is desired simply provide DEFAULT for the <BLUEPRINT>. If the"
   echo "blueprint.properites file was updated, please enter blueprint.properties filename for"
   echo "<BLUEPRINT>."
   echo ""
   exit 1
fi


# set os type
linux=false
solaris=false
case "`uname`" in
Linux*) linux=true;;
SunOS*) solaris=true;;
esac

ORIGDIR=INSTALLDIR
DIST=`echo $0 | awk '{ print substr( $0, 0, length($0)-18 ) }'`
cd $DIST
INSTALLDIR=`pwd`
BLUEPRINT=blueprint.properties
LICENSE=
FORCE=FALSE
ANSWER=
CONTINUE=FALSE

if [ $1 != "DEFAULT" ]; then
    if [ ! -f $1 ]; then
        echo "Provided Blueprint File $1 Cannot Be Found"
        echo "Cancelling Installation"
        exit 1
    fi
    BLUEPRINT=$1
fi

DB_TYPE=`grep "DB_TYPE=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [A-Z] [a-z]`

OPTIONS=""
shift
while [ $# -gt 0 ]
do
    if [ $1 == "-u" ]; then
        shift
        if [ $# -gt 0 ]; then
            OPTIONS="$OPTIONS -BDMYSQL_ROOT=$1"
        fi
    elif [ $1 == "-p" ]; then
        shift
        if [ $# -gt 0 ]; then
            OPTIONS="$OPTIONS -BDMYSQL_ROOT_PASSWORD=$1"
        fi
    elif [ $1 == "-cu" ]; then
        shift
        if [ $# -gt 0 ]; then
            OPTIONS="$OPTIONS -BDRSCONSOLE_USERNAME=$1"
        fi
    elif [ $1 == "-cp" ]; then
        shift
        if [ $# -gt 0 ]; then
            OPTIONS="$OPTIONS -BDRSCONSOLE_PASSWORD=$1"
        fi
    elif [ $1 == "-dns" ]; then
        shift
        if [ $# -gt 0 ]; then
            OPTIONS="$OPTIONS -BDDNS_HOST=$1"
        fi
    elif [ $1 == "-port" ]; then
        shift
        if [ $# -gt 0 ]; then
            OPTIONS="$OPTIONS -BDDNS_PORT=$1"
        fi
    elif [ $1 == "-lic" ]; then
        shift
        if [ $# -gt 0 ]; then
            LICENSE=$1
        fi
    elif [ $1 == "--force" ]; then
        FORCE=TRUE
    elif [ $1 == "--continue" ]; then
        CONTINUE=TRUE
    elif [ $1 == "--auto-accept" ]; then
        ANSWER=AUTO
    else
        OPTIONS="$OPTIONS $1"
    fi
    shift
done

if $solaris; then
    LIBC=`pvs /usr/lib/libc.so.1 | grep SUNW_1.22.2`
    if [ "${LIBC}x" == "x" ]; then
        echo "WARNING!!! Required libc.so.1 version SUNW_1.22.2 Not Found"
        if [ "${FORCE}" == "FALSE" ]; then
            echo "Cancelling Installation"
            exit 1
        fi
    fi
fi


OPTIONS="$OPTIONS -BDINSTALL=TRUE"

R_USER=`grep "resolve.user=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [A-Z] [a-z]`
ID=`id | sed -e "s/^uid=[0-9]*(\(.*\)) gid.*/\1/"`
if [ "${ID}x" != "${R_USER}x" ]; then
    echo "WARNING!!! Current User ID (${ID}) does not match the resolve.user property specified in the blueprint.properties"
    if [ "${FORCE}" == "FALSE" ]; then
        echo "The Resolve installation must be run as the User ID specified by resolve.user in the blueprint.properties."
        echo "Please either continue the installation as the User specified by resolve.user (${R_USER}), change"
        echo "the resolve.user property to match the User ID that will install and own the Resolve installation,"
        echo "or use the --force option if you wish to continue the installation as is."
        echo "Cancelling Installation"
        exit 1
    fi
fi

INSTALLED=`find . -name "resolve*.jar" | grep -v rsexpert 2>/dev/null`
if [ "${INSTALLED}x" != "x" ]; then
    echo "WARNING!!! Resolve components are already installed in this directory"
    if [ "${FORCE}" == "FALSE" ]; then
        echo "Please use the --force option if you wish to continue the installation."
        echo "Cancelling Installation"
        exit 1
    fi
else
    INSTALLED=`find . -name "rsview.jar" 2>/dev/null`
    if [ "${INSTALLED}x" != "x" ]; then
        echo "WARNING!!! RSView is already installed in this directory"
        if [ "${FORCE}" == "FALSE" ]; then
            echo "Please use the --force option if you wish to continue the installation."
            echo "Cancelling Installation"
            exit 1
        fi
    fi
fi

if $solaris; then
    RUNNING=`/usr/ucb/ps -augxwww | grep resolve-[a-zA-Z]*\.jar | grep -v grep`
else
    RUNNING=`ps -ef | grep -e resolve-[a-zA-Z]*\.jar | grep -v grep`
fi
if [ "${RUNNING}x" != "x" ]; then
    echo "WARNING!!!  Resolve components are already running on this system"
    if [ "${FORCE}" == "FALSE" ]; then
        echo "Please remove the existing components or validate they will not interfere with the new installation"
        echo "then use the --force option to continue the installation."
        echo "Cancelling Installation"
        exit 1
    fi
fi

if $solaris; then
    RUNNING=`/usr/ucb/ps -augxwww | grep Bootstrap | grep -v grep`
else
    RUNNING=`ps -ef | grep Bootstrap | grep -v grep`
fi
if [ "${RUNNING}x" != "x" ]; then
    echo "WARNING!!!  A Tomcat server is already running on this system"
    if [ "${FORCE}" == "FALSE" ]; then
        echo "Please validate that the new RSView tomcat won't interfere with the existing Tomcat server"
        echo "then use the --force option to continue the installation."
        echo "Cancelling Installation"
        exit 1
    fi
fi

if $solaris; then
    RUNNING=`/usr/ucb/ps -augxwww | grep routerconfig.xml | grep -v grep`
else
    RUNNING=`ps -ef | grep routerconfig.xml | grep -v grep`
fi
if [ "${RUNNING}x" != "x" ]; then
    echo "WARNING!!!  An RSMQ component is already running on this system"
    if [ "${FORCE}" == "FALSE" ]; then
        echo "Please remove this component to prevent it from interfering with the new installation"
        echo "then use the --force option to continue the installation."
        echo "Cancelling Installation"
        exit 1
    fi
fi

if $solaris; then
    TAR=$INSTALLDIR/gtar
else
    TAR=tar
fi

if [ "$ANSWER" != "AUTO" ]; then
# read and accept license file
    while read inputline
    do
        echo $inputline
    done < license.txt

    printf "Do You Accept The Above? (Y/N) "
    ANSWER=

    while [ "${ANSWER}x" == "x" ]
    do
        read inputline
        ANSWER=`echo $inputline | tr [A-Z] [a-z]`
        if [ "${ANSWER}" == "y" ] || [ "${ANSWER}" == "yes" ]; then
            ANSWER="Y"
        elif [ "${ANSWER}" == "n" ] || [ "${ANSWER}" == "no" ]; then
            ANSWER="N"
        else
            ANSWER=
            printf "Please Enter Y or N "
        fi
    done

    if [ "${ANSWER}" == "N" ]; then
        echo "Cancelling Installation"
        exit 1
    fi
fi

if [ "${CONTINUE}" != "TRUE" ]; then
# uncompressing common libs
    gunzip < rs-common.tar.gz | $TAR xf -

# uncompressing thirdparty libs
    gunzip < rs-thirdparty.tar.gz | $TAR xf -

    gunzip < rsmq.tar.gz | $TAR xf -
    gunzip < rscontrol-shared.tar.gz | $TAR xf -
    gunzip < rsremote-shared.tar.gz | $TAR xf -
    gunzip < rsmgmt-shared.tar.gz | $TAR xf -
    gunzip < rsview-shared.tar.gz | $TAR xf -
    gunzip < rsconsole-shared.tar.gz | $TAR xf -
    gunzip < mysqlconf.tar.gz | $TAR xf -
    gunzip < oracleconf.tar.gz | $TAR xf -
    gunzip < db2conf.tar.gz | $TAR xf -
    gunzip < elasticsearch.tar.gz | $TAR xf -
gunzip < rssync-shared.tar.gz | $TAR xf -

    if $solaris; then
    chmod 755 rabbitmq/solaris/rabbitmq/sbin/*
    chmod 755 rabbitmq/solaris/erlang/bin/*
    chmod 755 rabbitmq/solaris/erlang/erts-7.3/bin/*
    fi
    if $linux; then
    chmod 755 rabbitmq/linux64/rabbitmq/sbin/*
    chmod 755 rabbitmq/linux64/erlang/bin/*
    chmod 755 rabbitmq/linux64/erlang/erts-7.3/bin/*
    fi
fi

# replace default ${ORIGDIR} with installdir in rsmgmt
/bin/sed -e "s|${ORIGDIR}|${INSTALLDIR}|g" rsmgmt/config/config.xml > sed.tmp
mv -f sed.tmp rsmgmt/config/config.xml

# chmod 755 bin directories
chmod -R 755 jdk/bin
chmod 755 rsconsole/bin/*.sh
chmod 755 rscontrol/bin/*.sh
chmod 755 rscontrol/bin/init.*
chmod 755 rsremote/bin/*.sh
chmod 755 rsremote/bin/init.*
chmod 755 rsmgmt/bin/*.sh
chmod 755 rsmgmt/bin/init.*
chmod 755 rssync/bin/*.sh
chmod 755 rssync/bin/init.*
chmod 755 elasticsearch/bin/*.sh
chmod 755 elasticsearch/bin/init.*
mkdir ${INSTALLDIR}/elasticsearch/plugins
mkdir ${INSTALLDIR}/elasticsearch/logs
touch ${INSTALLDIR}/elasticsearch/logs/stdout
chmod 755 elasticsearch/bin/elasticsearch
chmod 754 tomcat/bin/*.sh
chmod 754 tomcat/bin/init.*
mkdir ${INSTALLDIR}/tomcat/logs
chmod 755 bin/*.sh

ln -s $INSTALLDIR/jdk jdk/jre

cp $BLUEPRINT rsmgmt/config/blueprint.properties

RSVIEW=`grep "resolve.rsview=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [A-Z] [a-z]`
RSCONTROL=`grep "resolve.rscontrol=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [A-Z] [a-z]`
RSMGMT=`grep "rsmgmt.sql.active=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [A-Z] [a-z]`

if [ -f bin/stop.sh ]; then
    bin/stop.sh all &> /dev/null
fi

if [ "${RSVIEW}" == "true" ] || [ "${RSCONTROL}" == "true" ] || [ "${RSMGMT}" == "true" ]; then
    rsmgmt/bin/run.sh -u admin -p resolve -f install/resolve-install-pre $OPTIONS
    rsmgmt/bin/run.sh -u admin -p resolve -f install/resolve-configure $OPTIONS -s
	if [ "${FORCE}" == "FALSE" ] && [ "$?" == "-1" ]; then
        exit
    fi
    rsmgmt/bin/run.sh -u admin -p resolve -f install/resolve-install-db $OPTIONS || exit
else
    rsmgmt/bin/run.sh -u admin -p resolve -f install/resolve-configure $OPTIONS -s
fi

if [ "${RSVIEW}" == "true" ]; then
    rsmgmt/bin/run.sh -u admin -p resolve -f install/resolve-install-setup $OPTIONS
fi

if [ "${LICENSE}x" != "x" ]; then 
    if [ -f $LICENSE ]; then
        rsmgmt/bin/run.sh -u admin -p resolve -f install/resolve-install-license "-FD$LICENSE" $OPTIONS
    else
        echo "WARNING!!! License File $LICENSE Could Not Be Found"
    fi
fi

rsmgmt/bin/run.sh -u admin -p resolve -f install/resolve-install-post $OPTIONS
rsmgmt/bin/run.sh -u admin -p resolve -f install/resolve-install-cleanup $OPTIONS

echo ""
echo "Resolve Installation Finished in $SECONDS seconds"
rsmgmt/bin/run.sh -f mgmt/Log "-FDResolve;Installation;Finished;in;$SECONDS;seconds" 1>/dev/null
