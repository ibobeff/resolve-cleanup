#!/bin/bash

if [ "$1" == "--help" ]; then
   echo ""
   echo "Usage: [--force]"
   echo ""
   echo ""
   echo " --force   Continue installation even if validations that check if RSProxy is already"
   echo "           installed or running on the system fail"
   echo ""
   echo "This script will install RSProxy. After installation you will have to modify the config.xml found in config folder with the listener port configuration."
   echo ""
   exit 1
fi

# set os type
linux=false
solaris=false
case "`uname`" in
Linux*) linux=true;;
SunOS*) solaris=true;;
esac

ORIGDIR=INSTALLDIR
DIST=`echo $0 | awk '{ print substr( $0, 0, length($0)-18 ) }'`
cd $DIST
INSTALLDIR=`pwd`
FORCE=FALSE

if [ "$1" = "--force" ]; then
    FORCE=TRUE
else
    FORCE=FALSE
fi

INSTALLED=`find . -name "resolve-proxy.jar" 2>/dev/null`
if [ "${INSTALLED}x" != "x" ]; then
    echo "WARNING!!! RSProxy is already installed in this directory"
    if [ "${FORCE}" == "FALSE" ]; then
        echo "Please use the --force option if you wish to continue the installation"
        echo "Cancelling Installation"
        exit 1
    fi
fi

if $solaris; then
    RUNNING=`/usr/ucb/ps -augxwww | grep resolve-proxy\.jar | grep -v grep`
else
    RUNNING=`ps -ef | grep -e resolve-proxy\.jar | grep -v grep`
fi
if [ "${RUNNING}x" != "x" ]; then
    echo "WARNING!!!  Resolve components are already running on this system"
    if [ "${FORCE}" == "FALSE" ]; then
        echo "Please remove the existing components or validate they will not interfere with the new installation"
        echo "then use the --force option to continue the installation."
        echo "Cancelling Installation"
        exit 1
    fi
fi

if $solaris; then
    TAR=$INSTALLDIR/gtar
else
    TAR=tar
fi

# uncompressing thirdparty libs
gunzip < rs-thirdparty.tar.gz | $TAR xf -

gunzip < rsproxy-shared.tar.gz | $TAR xf -

# replace default ${ORIGDIR} with installdir in rsproxy
/bin/sed -e "s|${ORIGDIR}|${INSTALLDIR}|g" rsproxy/config/config.xml > sed.tmp
mv -f sed.tmp rsproxy/config/config.xml

# chmod 755 bin directories
chmod -R 755 jdk/bin
chmod 755 rsproxy/bin/*.sh
chmod 755 rsproxy/bin/init.*

ln -s $INSTALLDIR/jdk jdk/jre

echo "RSProxy installed, now modify the config/config.xml and add listener configurations"
echo "After config.xml is changed execute run.sh to start RSProxy"
