#!/bin/sh

if [ $# -lt 1 ]; then
   echo ""
   echo "Usage: <BLUEPRINT> [<OPTIONS>] [-u <MySQL root username>] [-p <MySQL root password>]"
   echo ""
   echo "BLUEPRINT - location of the blueprint.properties file with Resolve configuration."
   echo "            Specify DEFAULT for default installation."
   echo ""
   echo "OPTION    - Options to be passed into the RSConsole run script"
   echo "            Example: -BDMYSQL_ROOT_PASSWORD=password will add the property"
   echo "            \"MYSQL_ROOT_PASSWORD\" in the blueprint configuration of RSConsole"
   echo ""
   echo "This script will install Resolve components in the current directory and start them up."
   echo "The blueprint.properties file specified will be used to configure the Resolve installation."
   echo ""
   echo "If a default installation is desired simply provide DEFAULT for the blueprint file."
   echo ""
   echo "To set the DB root username and password for installation add"
   echo "-u username -p password"
   echo ""
   exit 1
fi

# set os type
linux=false
solaris=false
case "`uname`" in
Linux*) linux=true;;
SunOS*) solaris=true;;
esac

ORIGDIR=INSTALLDIR
INSTALLDIR=`pwd`
BLUEPRINT=rsconsole/config/blueprint.properties

if [ $1 != "DEFAULT" ] && [ ! -f $1 ]; then
    echo "Provided blueprint.properties file: $1 cannot be found."
    exit 1
fi

if $solaris; then
    TAR=$INSTALLDIR/gtar
else
    TAR=tar
fi

# uncompressing common libs
gunzip < rs-common.tar.gz | $TAR xvf -

# uncompressing thirdparty libs
gunzip < rs-thirdparty.tar.gz | $TAR xvf -

gunzip < rsmq.tar.gz | $TAR xvf -
gunzip < rszk.tar.gz | $TAR xvf -
gunzip < rscontrol-shared.tar.gz | $TAR xvf -
gunzip < rsserver-shared.tar.gz | $TAR xvf -
gunzip < rsremote-shared.tar.gz | $TAR xvf -
gunzip < rsmgmt-shared.tar.gz | $TAR xvf -
gunzip < rsview-shared.tar.gz | $TAR xvf -
gunzip < rsconsole-shared.tar.gz | $TAR xvf -
gunzip < mysqlconf.tar.gz | $TAR xvf -
gunzip < oracleconf.tar.gz | $TAR xvf -

if $linux; then
    chmod -R 755 swiftmq/scripts/linux
elif $solaris; then
    chmod -R 755 swiftmq/scripts/solaris
fi


# replace default ${ORIGDIR} with installdir in rsconsole
/bin/sed -e "s|${ORIGDIR}|${INSTALLDIR}|g" rsconsole/config/config.xml > sed.tmp
mv -f sed.tmp rsconsole/config/config.xml

# chmod 755 bin directories
chmod -R 755 jdk/bin
chmod -R 755 rsconsole/bin
chmod -R 755 rscontrol/bin
chmod -R 755 rsremote/bin
chmod -R 755 rsmgmt/bin
chmod -R 754 tomcat/bin
chmod -R 755 bin

if [ $1 != "DEFAULT" ]; then
    cp $1 $BLUEPRINT
fi

OPTIONS=""
shift
while [ $# -gt 0 ]
do
    if [ $1 == "-u" ]; then
        shift
        if [ $# -gt 0 ]; then
            OPTIONS="$OPTIONS -BDMYSQL_ROOT=$1"
        fi
    elif [ $1 == "-p" ]; then
        shift
        if [ $# -gt 0 ]; then
            OPTIONS="$OPTIONS -BDMYSQL_ROOT_PASSWORD=$1"
        fi
    else
        OPTIONS="$OPTIONS $1"
    fi
    shift
done

rsconsole/bin/run.sh -u admin -p resolve -f install/resolve-configure $OPTIONS
