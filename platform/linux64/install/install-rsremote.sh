#!/bin/bash

if [ $# -lt 1 ]; then
   echo ""
   echo "Usage: <BLUEPRINT> [<OPTIONS>] [--force]"
   echo ""
   echo "BLUEPRINT - Location of blueprint file with RSRemote standalone configuration."
   echo ""
   echo "OPTIONS   - Options to be passed into the RSMgmt run script:"
   echo ""
   echo "  -lic     License file for resolve installation."
   echo ""
   echo " --force   Continue installation even if validations that check if Resolve is already"
   echo "           installed or running on the system fail."
   echo ""
   echo "This script will install, configure, and start a standalone RSRemote instance."
   echo "A RSMgmt component is included for local monitoring and configuration of the RSRemote component."
   echo "The blueprint file specified will be used to configure the RSRemote and RSMgmt."
   echo ""
   echo "The blueprint-rsremote.properties must be configured with the RSMQ(s) of the installation it"
   echo "will connect to.  This blueprint will be renamed to blueprint.properties by the installation."
   echo "The NODE_LIST in the blueprint must be identical to the NODE_LIST in the primary Resolve servers."
   echo ""
   exit 1
fi


# set os type
linux=false
solaris=false
case "`uname`" in
Linux*) linux=true;;
SunOS*) solaris=true;;
esac

ORIGDIR=INSTALLDIR
DIST=`echo $0 | awk '{ print substr( $0, 0, length($0)-19 ) }'`
cd $DIST
INSTALLDIR=`pwd`
BLUEPRINT=blueprint-rsremote.properties
LICENSE=
FORCE=FALSE
ANSWER=

if [ $1 != "DEFAULT" ]; then
    if [ ! -f $1 ]; then
        echo "Provided blueprint.properties file $1 cannot be found."
        echo "Canceling installation."
        exit 1
    fi
    BLUEPRINT=$1
fi

OPTIONS=""
shift
while [ $# -gt 0 ]
do
    if [ $1 == "--force" ]; then
        FORCE=TRUE
    elif [ $1 == "-lic" ]; then
        shift
        if [ $# -gt 0 ]; then
            LICENSE=$1
        fi
    elif [ $1 == "--auto-accept" ]; then
        ANSWER=AUTO
    else
        OPTIONS="$OPTIONS $1"
    fi
    shift
done

R_USER=`grep "resolve.user=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [A-Z] [a-z]`
ID=`id | sed -e "s/^uid=[0-9]*(\(.*\)) gid.*/\1/"`
if [ "${ID}x" != "${R_USER}x" ]; then
    echo "WARNING !!! Current user ID (${ID}) does not match the resolve.user property specified in the blueprint.properties file."
    if [ "${FORCE}" == "FALSE" ]; then
        echo "The Resolve installation must be run as the user ID specified by resolve.user in the blueprint.properties."
        echo "Please either continue the installation as the User specified by resolve.user (${R_USER}), change"
        echo "the resolve.user property to match the user ID that will install and own the Resolve installation,"
        echo "or use the --force option if you wish to continue the installation as is."
        echo "Canceling installation."
        exit 1
    fi
fi

INSTALLED=`find . -name "resolve-remote.jar" 2>/dev/null`
if [ "${INSTALLED}x" != "x" ]; then
    echo "WARNING !!! RSRemote is already installed in this directory."
    if [ "${FORCE}" == "FALSE" ]; then
        echo "Please use the --force option if you wish to continue the installation."
        echo "Canceling installation."
        exit 1
    fi
fi

INSTALLED=`find . -name "resolve-mgmt.jar" 2>/dev/null`
if [ "${INSTALLED}x" != "x" ]; then
    echo "WARNING !!! RSMgmt is already installed in this directory."
    if [ "${FORCE}" == "FALSE" ]; then
        echo "Please use the --force option if you wish to continue the installation."
        echo "Canceling installation"
        exit 1
    fi
fi

if $solaris; then
    RUNNING=`/usr/ucb/ps -augxwww | grep resolve-[a-zA-Z]*\.jar | grep -v grep`
else
    RUNNING=`ps -ef | grep -e resolve-[a-zA-Z]*\.jar | grep -v grep`
fi
if [ "${RUNNING}x" != "x" ]; then
    echo "WARNING !!! Resolve components are already running on this system."
    if [ "${FORCE}" == "FALSE" ]; then
        echo "Please remove the existing components or validate they will not interfere with the new installation."
        echo "then use the --force option to continue the installation."
        echo "Canceling installation."
        exit 1
    fi
fi

if $solaris; then
    TAR=$INSTALLDIR/gtar
else
    TAR=tar
fi

if [ "$ANSWER" != "AUTO" ]; then
# read and accept license file
    while read inputline
    do
        echo $inputline
    done < license.txt

    printf "Do You Accept The Above? (Y/N)"
    ANSWER=

    while [ "${ANSWER}x" == "x" ]
    do
        read inputline
        ANSWER=`echo $inputline | tr [A-Z] [a-z]`
        if [ "${ANSWER}" == "y" ] || [ "${ANSWER}" == "yes" ]; then
            ANSWER="Y"
        elif [ "${ANSWER}" == "n" ] || [ "${ANSWER}" == "no" ]; then
            ANSWER="N"
        else
            ANSWER=
            printf "Please Enter Y or N."
        fi
    done

    if [ "${ANSWER}" == "N" ]; then
        echo "Canceling installation."
        exit 1
    fi
fi

# uncompressing common libs
gunzip < rs-common.tar.gz | $TAR xf -

# uncompressing thirdparty libs
gunzip < rs-thirdparty.tar.gz | $TAR xf -

gunzip < rsremote-shared.tar.gz | $TAR xf -
gunzip < rsmgmt-shared.tar.gz | $TAR xf -

# replace default ${ORIGDIR} with installdir in rsmgmt
/bin/sed -e "s|${ORIGDIR}|${INSTALLDIR}|g" rsmgmt/config/config.xml > sed.tmp
mv -f sed.tmp rsmgmt/config/config.xml

# chmod 755 bin directories
chmod -R 755 jdk/bin
chmod 755 rsremote/bin/*.sh
chmod 755 rsremote/bin/init.*
chmod 755 rsmgmt/bin/*.sh
chmod 755 rsmgmt/bin/init.*
chmod 755 bin/*.sh

ln -s $INSTALLDIR/jdk jdk/jre

cp $BLUEPRINT rsmgmt/config/blueprint.properties

if [ "${LICENSE}x" != "x" ]; then 
    if [ -f $LICENSE ]; then
        rsmgmt/bin/run.sh -u admin -p resolve -f install/resolve-install-license "-FD$LICENSE" $OPTIONS
    else
        echo "WARNING !!! License file $LICENSE could not be found."
    fi
fi

rsmgmt/bin/run.sh -u admin -p resolve -f install/resolve-configure -s $OPTIONS
rsmgmt/bin/run.sh -u admin -p resolve -f install/resolve-install-cleanup $OPTIONS
bin/run.sh ALL
