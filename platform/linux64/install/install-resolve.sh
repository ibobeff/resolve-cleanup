#!/bin/bash

if [ $# -lt 1 ]; then
   echo ""
   echo "Usage: <BLUEPRINT> [<OPTIONS>] [--force]"
   echo ""
   echo "BLUEPRINT - Location of blueprint.properties file with Resolve configuration."
   echo "            Specify DEFAULT for default installation."
   echo ""
   echo "OPTIONS     - Options to be passed into the RSMgmt run script:"
   echo ""
   echo "  -lic           License File for Resolve Installation."
   echo ""
   echo "  -dns           DNS host name to set into RSView properties for system URL."
   echo ""
   echo "  -port          Host port to set into RSView properties for system URL."
   echo ""
   echo "  -u             MySQL root username used to create the Resolve database specified in the blueprint configuration."
   echo ""
   echo "  -p             MySQL root password."
   echo ""
   echo "  -cu            RSConsole username to run default imports."
   echo "                 Only needed if non-default RSConsole username is used in the blueprint configuration."
   echo ""
   echo "  -cp            RSConsole password to run default imports."
   echo "                 Only needed if non-default RSConsole password is used in the blueprint configuration."
   echo ""
   echo " -keystoreFile   Keystore file path with private key and public certificates for RSView HTTPS configuration."
   echo "                 If not provided a new keystore will be generated during installation with a self-signed certificate."
   echo ""
   echo " -keystorePass   Keystore password for keystore file. If not provided a default value will be used."
   echo ""
   echo " --force         Continue installation even if validations that check if Resolve is already"
   echo "                 installed or running on the system fail."
   echo ""
   echo " --continue      Resume a failed installation, skipping some duplicate steps such as unpacking the"
   echo "                 component tar.gz files."
   echo ""
   echo " --http          Install RSView using HTTP instead of HTTPS (default option)."
   echo ""
   echo "This script will install the Resolve components in the current directory and start Resolve."
   echo "The blueprint configuration file specified will be used to configure the Resolve components during installation."
   echo ""
   echo "If a default installation is desired simply provide DEFAULT for the <BLUEPRINT>."
   echo "If the blueprint.properites file was updated,"
   echo "please enter blueprint.properties filename for <BLUEPRINT>."
   echo ""
   exit 1
fi

# set os type
linux=false
solaris=false
case "`uname`" in
Linux*) linux=true;;
SunOS*) solaris=true;;
esac

ORIGDIR=INSTALLDIR
DIST=`echo $0 | awk '{ print substr( $0, 0, length($0)-18 ) }'`
cd $DIST
INSTALLDIR=`pwd`
BLUEPRINT=blueprint.properties
LICENSE=
FORCE=FALSE
ANSWER=
CONTINUE=FALSE
HTTPS=TRUE

if [ $1 != "DEFAULT" ]; then
    if [ ! -f $1 ]; then
        echo "Provided blueprint.properties file $1 cannot be found."
        echo "Canceling installation."
        exit 1
    fi
    BLUEPRINT=$1
fi

DB_TYPE=`grep "DB_TYPE=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [A-Z] [a-z]`

OPTIONS=""
shift
while [ $# -gt 0 ]
do
    if [ $1 == "-u" ]; then
        shift
        if [ $# -gt 0 ]; then
            OPTIONS="$OPTIONS -BDMYSQL_ROOT=$1"
        fi
    elif [ $1 == "-p" ]; then
        shift
        if [ $# -gt 0 ]; then
            OPTIONS="$OPTIONS -BDMYSQL_ROOT_PASSWORD=$1"
        fi
    elif [ $1 == "-cu" ]; then
        shift
        if [ $# -gt 0 ]; then
            OPTIONS="$OPTIONS -BDRSCONSOLE_USERNAME=$1"
        fi
    elif [ $1 == "-cp" ]; then
        shift
        if [ $# -gt 0 ]; then
            OPTIONS="$OPTIONS -BDRSCONSOLE_PASSWORD=$1"
        fi
    elif [ $1 == "-dns" ]; then
        shift
        if [ $# -gt 0 ]; then
            OPTIONS="$OPTIONS -BDDNS_HOST=$1"
        fi
    elif [ $1 == "-port" ]; then
        shift
        if [ $# -gt 0 ]; then
            OPTIONS="$OPTIONS -BDDNS_PORT=$1"
        fi
	elif [ $1 == "-keystoreFile" ]; then
		shift
		if [ $# -gt 0 ]; then
			OPTIONS="$OPTIONS -BDrsview.tomcat.connector.https.keystoreFile=$1"
		fi
	elif [ $1 == "-keystorePass" ]; then
		shift
		if [ $# -gt 0 ]; then
			OPTIONS="$OPTIONS -BDrsview.tomcat.connector.https.keystorePass=$1"
		fi
    elif [ $1 == "-lic" ]; then
        shift
        if [ $# -gt 0 ]; then
            LICENSE=$1
        fi
    elif [ $1 == "--force" ]; then
        FORCE=TRUE
    elif [ $1 == "--continue" ]; then
        CONTINUE=TRUE
    elif [ $1 == "--auto-accept" ]; then
        ANSWER=AUTO
    elif [ $1 == "--http" ]; then
        HTTPS=FALSE
    else
        OPTIONS="$OPTIONS $1"
    fi
    shift
done

OPTIONS="$OPTIONS -BDINSTALL=TRUE"

if [ "${HTTPS}" == "TRUE" ]; then
    OPTIONS="$OPTIONS -BDrsview.tomcat.ssl.config=true"
    OPTIONS="$OPTIONS -BDrsview.tomcat.https=true"
fi

R_USER=`grep "resolve.user=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [A-Z] [a-z]`
ID=`id | sed -e "s/^uid=[0-9]*(\(.*\)) gid.*/\1/"`
if [ "${ID}x" != "${R_USER}x" ]; then
    echo "WARNING !!! Current user ID (${ID}) does not match the resolve.user property specified in the blueprint.properties file."
    if [ "${FORCE}" == "FALSE" ]; then
        echo "The Resolve installation must be run as the user ID specified by the resolve.user property in the blueprint.properties file."
        echo "Please either continue the installation as the user specified by the resolve.user property (${R_USER}),"
        echo "change the resolve.user property to match the user ID that will install and own the Resolve installation,"
        echo "or use the --force option if you wish to continue the installation as is."
        echo "Canceling Installation."
        exit 1
    fi
fi

INSTALLED=`find . -name "resolve*.jar" | grep -v rsexpert 2>/dev/null`
if [ "${INSTALLED}x" != "x" ]; then
    echo "WARNING !!! Resolve components are already installed in this directory."
    if [ "${FORCE}" == "FALSE" ]; then
        echo "Please use the --force option if you wish to continue the installation."
        echo "Canceling installation."
        exit 1
    fi
else
    INSTALLED=`find . -name "rsview.jar" 2>/dev/null`
    if [ "${INSTALLED}x" != "x" ]; then
        echo "WARNING !!! RSView is already installed in this directory."
        if [ "${FORCE}" == "FALSE" ]; then
            echo "Please use the --force option if you wish to continue the installation."
            echo "Canceling installation."
            exit 1
        fi
    fi
fi

if $solaris; then
    RUNNING=`/usr/ucb/ps -augxwww | grep resolve-[a-zA-Z]*\.jar | grep -v grep`
else
    RUNNING=`ps -ef | grep -e resolve-[a-zA-Z]*\.jar | grep -v grep`
fi
if [ "${RUNNING}x" != "x" ]; then
    echo "WARNING !!!  Resolve components are already running on this system."
    if [ "${FORCE}" == "FALSE" ]; then
        echo "Please remove the existing components or validate they will not interfere with the new installation or"
        echo "use the --force option to continue the installation."
        echo "Canceling installation."
        exit 1
    fi
fi

if $solaris; then
    RUNNING=`/usr/ucb/ps -augxwww | grep Bootstrap | grep -v grep`
else
    RUNNING=`ps -ef | grep Bootstrap | grep -v grep`
fi
if [ "${RUNNING}x" != "x" ]; then
    echo "WARNING !!! A Tomcat server is already running on this system."
    if [ "${FORCE}" == "FALSE" ]; then
        echo "Please validate that the new RSView Tomcat instance won't interfere with the existing one or"
        echo "then use the --force option to continue the installation."
        echo "Canceling installation."
        exit 1
    fi
fi

if $solaris; then
    RUNNING=`/usr/ucb/ps -augxwww | grep routerconfig.xml | grep -v grep`
else
    RUNNING=`ps -ef | grep routerconfig.xml | grep -v grep`
fi
if [ "${RUNNING}x" != "x" ]; then
    echo "WARNING !!! An RSMQ component is already running on this system."
    if [ "${FORCE}" == "FALSE" ]; then
        echo "Please remove this component to prevent it from interfering with the new installation or"
        echo "then use the --force option to continue the installation."
        echo "Canceling installation."
        exit 1
    fi
fi

if $solaris; then
    TAR=$INSTALLDIR/gtar
else
    TAR=tar
fi

if [ "$ANSWER" != "AUTO" ]; then
# read and accept license file
    while read inputline
    do
        echo $inputline
    done < license.txt

    printf "Do You Accept The Above? (Y/N) "
    ANSWER=

    while [ "${ANSWER}x" == "x" ]
    do
        read inputline
        ANSWER=`echo $inputline | tr [A-Z] [a-z]`
        if [ "${ANSWER}" == "y" ] || [ "${ANSWER}" == "yes" ]; then
            ANSWER="Y"
        elif [ "${ANSWER}" == "n" ] || [ "${ANSWER}" == "no" ]; then
            ANSWER="N"
        else
            ANSWER=
            printf "Please Enter Y or N."
        fi
    done

    if [ "${ANSWER}" == "N" ]; then
        echo "Canceling installation."
        exit 1
    fi
fi

DCS=`grep "^resolve.dcs=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [A-Z] [a-z]`

if [ "${CONTINUE}" != "TRUE" ]; then
# uncompressing common libs
    gunzip < rs-common.tar.gz | $TAR xf -

# uncompressing thirdparty libs
    gunzip < rs-thirdparty.tar.gz | $TAR xf -

    gunzip < rsmq.tar.gz | $TAR xf -
    gunzip < rscontrol-shared.tar.gz | $TAR xf -
    gunzip < rsremote-shared.tar.gz | $TAR xf -
    gunzip < rsmgmt-shared.tar.gz | $TAR xf -
    gunzip < rsview-shared.tar.gz | $TAR xf -
    gunzip < rsconsole-shared.tar.gz | $TAR xf -
    gunzip < rsarchive-shared.tar.gz | $TAR xf -
    gunzip < mysqlconf.tar.gz | $TAR xf -
    gunzip < oracleconf.tar.gz | $TAR xf -
    gunzip < db2conf.tar.gz | $TAR xf -
    gunzip < elasticsearch.tar.gz | $TAR xf -
    gunzip < logstash.tar.gz | $TAR xf -
	gunzip < rssync-shared.tar.gz | $TAR xf -
	gunzip < rslog-shared.tar.gz | $TAR xf -

	if [ "${DCS}" == "true" ]; then
 	   gunzip < dcs.tar.gz | $TAR xf -
	fi

    if $solaris; then
        chmod 755 rabbitmq/solaris/rabbitmq/sbin/*
        chmod 755 rabbitmq/solaris/erlang/bin/*
        chmod 755 rabbitmq/solaris/erlang/erts-7.3/bin/*
    fi
    if $linux; then
        chmod 755 rabbitmq/linux64/rabbitmq/sbin/*
        chmod 755 rabbitmq/linux64/erlang/bin/*
        chmod 755 rabbitmq/linux64/erlang/erts-7.3/bin/*
    fi
fi

#Validate System Limits are Set Correctly
RSSEARCH=`grep "^resolve.rssearch=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [A-Z] [a-z]`
if [ "${RSSEARCH}" == "true" ]; then
    MAPCOUNT=`sysctl vm.max_map_count | awk 'BEGIN{FS="="}{print $2}'`
    if [ ${MAPCOUNT} -lt 262144 ]; then
        echo "Current vm.max_map_count (${MAPCOUNT}) is too low for the installation to run successfully."
        echo "Please set it to at least 262144 before continuing with the installation. This can be done temporarily by running sysctl -w vm.max_map_count=262144 as root, or permanently by configuring vm.max_map_count in /etc/sysctl.conf or by running the script setup_sysctl.sh."
        exit 1
    fi
    OPENFILES=`ulimit -a | grep "open files" | awk '{print $NF}'`
    if [ ${OPENFILES} -lt 2048 ]; then
        echo "Current max file descriptors (${OPENFILES}) is too low for the install to run successfully."
        echo "Please set it to at least 2048 (recommended is 131072) before continuing with the installation. This can be done temporarily by running ulimit -n 131072 as root, or permanently by configuring nofile in /etc/security/limits.conf or by running the script setup_limits.sh."
        exit 1
    fi
    MAXPROCESS=`ulimit -a | grep "max user processes" | awk '{print $NF}'`
    if [ ${MAXPROCESS} -lt 2048 ]; then
        echo "Current max user processes (${MAXPROCESS}) is too low for the install to run successfully."
        echo "Please set it to at least 2048 (recommended is 10240) before continuing with the installation. This can be done temporarily by running ulimit -u 10240 as root, or permanently by configuring nproc in /etc/security/limits.conf or by running the script setup_limits.sh."
        exit 1
    fi
    MAXLOCKEDMEMORY=`ulimit -a | grep "max locked memory" | awk '{print $NF}'`
    if [ "${MAXLOCKEDMEMORY}" != "unlimited" ]; then
        echo "Current max locked memory (${MAXLOCKEDMEMORY}) is too low for the install to run successfully."
        echo "Please set it to unlimited before continuing with the installation. This can be done by configuring memlock and as in /etc/security/limits.conf or by running the script setup_limits.sh."
        exit 1
    fi
fi

# replace default ${ORIGDIR} with installdir in rsmgmt
/bin/sed -e "s|${ORIGDIR}|${INSTALLDIR}|g" rsmgmt/config/config.xml > sed.tmp
mv -f sed.tmp rsmgmt/config/config.xml

# chmod 755 bin directories
chmod -R 755 jdk/bin
chmod 755 rsconsole/bin/*.sh
chmod 755 rscontrol/bin/*.sh
chmod 755 rscontrol/bin/init.*
chmod 755 rsremote/bin/*.sh
chmod 755 rsremote/bin/init.*
chmod 755 rsmgmt/bin/*.sh
chmod 755 rsmgmt/bin/init.*
chmod 755 rssync/bin/*.sh
chmod 755 rssync/bin/init.*
chmod 755 rsarchive/bin/*.sh
chmod 755 rsarchive/bin/init.*
chmod 755 elasticsearch/bin/*.sh
chmod 755 elasticsearch/bin/init.*
mkdir ${INSTALLDIR}/elasticsearch/plugins
mkdir ${INSTALLDIR}/elasticsearch/logs
touch ${INSTALLDIR}/elasticsearch/logs/stdout
chmod 755 elasticsearch/bin/elasticsearch
chmod 755 logstash/bin/*.sh
chmod 755 logstash/bin/init.*
mkdir ${INSTALLDIR}/logstash/logs
touch ${INSTALLDIR}/logstash/logs/stdout
chmod 755 logstash/bin/logstash
chmod 755 logstash/bin/logstash-keystore
chmod 755 logstash/bin/logstash-plugin
chmod 755 logstash/bin/cpdump
chmod 755 logstash/bin/pqcheck
chmod 755 logstash/bin/ruby
chmod 755 logstash/bin/system-install
chmod 755 logstash/vendor/jruby/bin/*
chmod 755 logstash/vendor/bundle/jruby/2.3.0/bin/*
chmod 754 tomcat/bin/*.sh
chmod 754 tomcat/bin/init.*
mkdir ${INSTALLDIR}/tomcat/logs
chmod 755 bin/*.sh
chmod 755 rslog/bin/*.sh
chmod 755 rslog/bin/init.*

if [ "${DCS}" == "true" ]; then
   chmod -R 755 dcs
fi

ln -s $INSTALLDIR/jdk jdk/jre

cp $BLUEPRINT rsmgmt/config/blueprint.properties

RSVIEW=`grep "^resolve.rsview=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [A-Z] [a-z]`
RSCONTROL=`grep "^resolve.rscontrol=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [A-Z] [a-z]`
RSMGMT=`grep "^rsmgmt.sql.active=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [A-Z] [a-z]`

if [ -f bin/stop.sh ]; then
    bin/stop.sh all &> /dev/null
fi

if [ "${RSVIEW}" == "true" ] || [ "${RSCONTROL}" == "true" ] || [ "${RSMGMT}" == "true" ]; then
    rsmgmt/bin/run.sh -u admin -p resolve -f install/resolve-install-pre $OPTIONS
    rsmgmt/bin/run.sh -u admin -p resolve -f install/resolve-configure $OPTIONS -s
	if [ "$?" == "255" ] && [ "${FORCE}" == "FALSE" ]; then
		echo "WARNING !!! Insufficient disk space. Canceling installation."
        exit 1
    fi
    rsmgmt/bin/run.sh -u admin -p resolve -f install/resolve-install-db $OPTIONS || exit
else
    rsmgmt/bin/run.sh -u admin -p resolve -f install/resolve-configure $OPTIONS -s
	if [ "$?" == "255" ] && [ "${FORCE}" == "FALSE" ]; then
		echo "WARNING !!! Insufficient disk space. Canceling installation."
        exit 1
    fi
fi


if [ "${RSVIEW}" == "true" ]; then
    rsmgmt/bin/run.sh -u admin -p resolve -f install/resolve-install-setup $OPTIONS
fi

if [ "${LICENSE}x" != "x" ]; then 
    if [ -f $LICENSE ]; then
        rsmgmt/bin/run.sh -u admin -p resolve -f install/resolve-install-license "-FD$LICENSE" $OPTIONS
    else
        echo "WARNING !!! License file $LICENSE could not be found."
    fi
fi

# install DCS dependencies

DCS_RSDATALOADER=`grep "^resolve.dcs.rsdataloader=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [A-Z] [a-z]`
DCS_RSREPORTING=`grep "^resolve.dcs.rsreporting=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [A-Z] [a-z]`

# RSDataLoader
if [ "${DCS_RSDATALOADER}" == "true" ]; then
 	echo;
fi

# RSDataReporting
if [ "${DCS_RSREPORTING}" == "true" ]; then
 	echo;
fi

rsmgmt/bin/run.sh -u admin -p resolve -f install/resolve-install-post $OPTIONS
rsmgmt/bin/run.sh -u admin -p resolve -f install/resolve-install-cleanup $OPTIONS

chmod 400 `find tomcat/conf -type f`
echo ""
echo "Resolve installation finished in $SECONDS seconds."
rsmgmt/bin/run.sh -f mgmt/Log "-FDResolve;Installation;Finished;in;$SECONDS;seconds" 1>/dev/null
