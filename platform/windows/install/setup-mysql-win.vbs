
set args = WScript.arguments.Named

' initialize defaults
if args.exists("help") then
    WScript.echo "*** MYSQL SETUP ***"
    WScript.echo ""
    WScript.echo "  Usage: /option:<value> ..."
    WScript.echo ""
    WScript.echo "OPTIONAL PARAMETERS:"
    WScript.echo ""
    WScript.echo "    /dir:<installdir>     - Installation directory path (absolute)"
    WScript.echo "    /mysqldir:<mysqldir>  - MySQL Installation directory path (absolute)"
    WScript.echo "    /mysqld:<exename>     - The executable name of the mysqld (mysqldt.exe)"
    WScript.echo "    /noservice            - Do not install windows service"
    WScript.echo "    /waittime:<secs>      - Number of seconds to wait for DB init (default 10)"
    WScript.echo ""
    WScript.quit
end if

' init objects
set fso = CreateObject("Scripting.FileSystemObject")
set objshell = CreateObject("WScript.Shell")

' get systemroot
systemroot = objshell.ExpandEnvironmentStrings("%systemroot%")

' installdir
installdir = objshell.CurrentDirectory
if args.exists("dir") then
    installdir = args.item("dir")
end if

' mysqldir
mysqldir = installdir & "/mysql-5.1.42-win32"
if args.exists("mysqldir") then
    mysqldir = args.item("mysqldir")
end if

' mysqld
mysqld = "mysqld.exe"
if args.exists("mysqld") then
    mysqld = args.item("mysqld")
end if

' waittime
waittime = 10000
if args.exists("waittime") then
    waittime = args.item("waittime") * 1000
end if

' noservice
noservice = "false"
if args.exists("noservice") then
    noservice = args.item("noservice")
end if

' copy my.cnf to windows
srcfile = installdir & "/mysql/my.cnf"
destfile = systemroot & "/my.cnf"
fso.copyfile srcfile, destfile

' append mysql install path
const APPENDING = 8
set file = fso.OpenTextFile(destfile, APPENDING)
file.writeline "[mysqld]"
file.writeline "basedir=" & mysqldir
file.writeline "datadir=" & mysqldir & "/data"
file.close

' install windows service
if noservice <> "" then
WScript.echo "Installing Windows service"
    'WScript.echo """" & mysqldir & "\bin\" & mysqld & """" & " --install"
    objshell.run """" & mysqldir & "\bin\" & mysqld & """" & " --install", 0
end if
WScript.sleep(5000)

' start mysql service for initialization
set wmi = GetObject("winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
set services = wmi.execQuery("SELECT * FROM Win32_Service WHERE name='mysql'")
for each service in services
    service.startService()
    WScript.echo "Started service " & service.displayName
next
WScript.sleep(15000)

' initialize database
WScript.echo "Initializing MYSQL"
WScript.sleep(waittime)

' copy create.sql to mysql/bin
srcfile = installdir & "/mysql/create.sql"
destfile = mysqldir & "/bin/create.sql"
fso.copyfile srcfile, destfile

' copy init.sql to mysql/bin
srcfile = installdir & "/mysql/init.sql"
destfile = mysqldir & "/bin/init.sql"
fso.copyfile srcfile, destfile

' init sql
objshell.CurrentDirectory = mysqldir & "/bin"
objshell.run "mysql.exe --user root --database mysql -e ""source create.sql""", 0, TRUE
WScript.echo "Created user and database"
WScript.sleep(2000)

' init sql
objshell.CurrentDirectory = mysqldir & "/bin"
objshell.run "mysql.exe --user root --database mysql -e ""source init.sql""", 0, TRUE
WScript.echo "Init SQL table"
WScript.sleep(2000)

WScript.echo "Initialization completed"
objshell.CurrentDirectory = installdir

