
set args = WScript.arguments.Named

' initialize defaults
if args.exists("help") then
    ' usage help
    WScript.echo "*** RSVIEW SETUP ***"
    WScript.echo ""
    WScript.echo "  Usage: /option:<value> ..."
    WScript.echo ""
    WScript.echo "OPTIONAL PARAMETERS:"
    WScript.echo ""
    WScript.echo "    /dir:<installdir>     - Installation directory path (absolute)"
    WScript.echo "    /mysqldir:<mysqldir>  - MySQL Installation directory path (absolute)"
    WScript.echo ""
    WScript.quit
end if

set objshell = CreateObject("WScript.Shell")
set fso = CreateObject("Scripting.FileSystemObject")

' installdir
installdir = objshell.CurrentDirectory
if args.exists("dir") then
    installdir = args.item("dir")
end if

' mysqldir
mysqldir = installdir & "/mysql-5.1.42-win32"
if args.exists("mysqldir") then
    mysqldir = args.item("mysqldir")
end if

' print xwiki.xml context file
WScript.echo "Updating WEB-INF/log.cfg file"
logfile = installdir & "/tomcat/logs/rsview.log"
logfile = replace(logfile, "\", "/")
logfile = right(logfile, len(logfile) - 2)

catalinafile = installdir & "/tomcat/logs/stdout.log"
catalinafile = replace(catalinafile, "\", "/")
catalinafile = right(catalinafile, len(catalinafile) - 2)

CR = VBCR & VBLF
logcfg = "# log filename" & CR & "log4j.appender.RSVIEW.File=" & logfile
catalinacfg = "# log filename" & CR & "log4j.appender.TOMCAT.File=" & catalinafile

const APPENDING = 8
set file = fso.OpenTextFile(installdir & "/tomcat/webapps/resolve/WEB-INF/log.cfg", APPENDING)
file.writeline logcfg
file.writeline catalinacfg
file.close


' install windows service
WScript.echo "Installing TOMCAT Windows service"

objshell.CurrentDirectory = installdir & "\tomcat\bin"
objshell.run "install-service-rsview.bat", 0, TRUE

' start rsview tomcat service for initialization
'set wmi = GetObject("winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
'set services = wmi.execQuery("SELECT * FROM Win32_Service WHERE name='rsview'")
'for each service in services
'    service.startService()
'    WScript.echo "Started service " & service.displayName
'next

' wait until glide.resolve_registration table is created before completion and starting rscontrol installation
'objshell.CurrentDirectory = mysqldir & "\bin"
'finish = false
'WScript.echo "Initializing Database"
'do until finish
'    WScript.stdout.write "."
'    Set execobj = objshell.Exec("mysql --user=root -e ""desc resolve_registration"" resolve")
'    do until execobj.stdout.atEndOfStream
'        execout = execobj.stdout.readline()
'        matchstr = instr(execout, "ERROR")
'        if matchstr <> 0 then
'            finish = false
'        else
'            finish = true
'        end if
'    loop
'    WScript.sleep(5000)
'loop
WScript.echo ""
WScript.echo "Completed initialization"
objshell.CurrentDirectory = installdir

