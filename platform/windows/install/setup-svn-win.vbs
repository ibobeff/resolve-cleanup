
set args = WScript.arguments.Named

' initialize defaults
if args.exists("help") then
    ' usage help
    WScript.echo "*** SVN SETUP ***"
    WScript.echo ""
    WScript.echo "  Usage: /option:<value> ..."
    WScript.echo ""
    WScript.echo "OPTIONAL PARAMETERS:"
    WScript.echo ""
    WScript.echo "    /dir:<installdir>     - Installation directory path (absolute)"
    WScript.echo ""
    WScript.quit
end if

set objshell = CreateObject("WScript.Shell")
set fso = CreateObject("Scripting.FileSystemObject")

' installdir
installdir = objshell.CurrentDirectory
if args.exists("dir") then
    installdir = args.item("dir")
end if
objshell.CurrentDirectory = installdir

WScript.sleep(15000)


' add core lib files
objshell.CurrentDirectory = installdir & "/src"
WScript.echo ""
WScript.echo "Installing Core Repository Libraries"
objshell.run "..\svn\bin\svn.exe update", 0, TRUE
objshell.run "..\svn\bin\svn.exe add --force *", 0, TRUE
objshell.run "..\svn\bin\svn.exe commit -m core", 0, TRUE
objshell.CurrentDirectory = installdir
WScript.sleep(5000)
WScript.echo "Added core repository files"

WScript.echo ""
WScript.echo "Created SVN Version Control"
WScript.sleep(3000)
