;--------------------------------
; Uses NSIS Modern UI

!define RESOLVE3DIR "..\..\.."

; include Modern UI
!include "MUI.nsh"

; Define variable macros
!define TEMP $R0

; The name of the installer
Name "RSRemote"

; The file to write
OutFile "rsremote64.exe"

; The default installation directory
InstallDir "$PROGRAMFILES64\Resolve Systems"

;Language strings
LangString PRODUCT_NAME_RESOLVE ${LANG_ENGLISH} "Resolve RBA - RSRemote"
LangString DESC_RESOLVE ${LANG_ENGLISH} "Resolve RSRemote components - includes: RSRemote, RSMgmt"
LangString TEXT_IO_TITLE ${LANG_ENGLISH} "Resolve Configuration Information"
LangString TEXT_IO_SUBTITLE ${LANG_ENGLISH} "Enter configuration settings for Resolve"

;--------------------------------
; Modern Interface Settings

!define MUI_ABORTWARNING
!define MUI_ICON "${RESOLVE3DIR}\platform\windows\install\resolve.ico"
!define MUI_UNICON "${RESOLVE3DIR}\platform\windows\install\resolve.ico"
!define MUI_COMPONENTSPAGE_SMALLDESC
!define MUI_WELCOMEFINISHPAGE_BITMAP "welcome.bmp"

;--------------------------------
; Files to be extracted on startup
ReserveFile "${NSISDIR}\Plugins\InstallOptions.dll"
ReserveFile "rsremote.ini"

; init variables
var /GLOBAL productname
Var /GLOBAL blueprint
Var /GLOBAL mysqlconnector
Var /GLOBAL has_mysql

;--------------------------------
; Pages
;--------------------------------


!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "${RESOLVE3DIR}\license\license.txt"
!insertmacro MUI_PAGE_COMPONENTS
Page custom Settings ValidateSettings ": Configuration Settings"
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------
; Languages

!insertmacro MUI_LANGUAGE "English"

;--------------------------------
; Resolve Section
;--------------------------------

Section "RSRemote (required)" SECTION_RESOLVE

  SectionIn RO
  
  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  StrCpy $productname "$(PRODUCT_NAME_RESOLVE)"

  ; get InstallOptions dialog user input
  ReadINIStr ${TEMP} "$PLUGINSDIR\rsremote.ini" "Field 3" "State"
  StrCpy $blueprint ${TEMP}
  DetailPrint "Resolve Blueprint File=$blueprint"

  ; Put file there
  File "${RESOLVE3DIR}\platform\windows\install\unzip.exe"
  File "${RESOLVE3DIR}\platform\windows\install\ln.exe"
  File "${RESOLVE3DIR}\platform\windows\install\sed.exe"
  File "${RESOLVE3DIR}\builds\rs-common.zip"
  File "${RESOLVE3DIR}\builds\rs-thirdparty64.zip"
  File "${RESOLVE3DIR}\builds\rsmgmt64.zip"
  File "${RESOLVE3DIR}\builds\rsremote64.zip"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Resolve" "DisplayName" "$productname"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Resolve" "UninstallString" '"$INSTDIR\uninstall_resolve.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Resolve" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Resolve" "NoRepair" 1
  WriteUninstaller "uninstall_resolve.exe"

  ; unzip file
  nsExec::Exec '"$INSTDIR\unzip.exe" -q -o "$INSTDIR\rs-common.zip" -d "$INSTDIR"'
  nsExec::Exec '"$INSTDIR\unzip.exe" -q -o "$INSTDIR\rs-thirdparty64.zip" -d "$INSTDIR"'
  nsExec::Exec '"$INSTDIR\unzip.exe" -q -o "$INSTDIR\rsmgmt64.zip" -d "$INSTDIR"'
  nsExec::Exec '"$INSTDIR\unzip.exe" -q -o "$INSTDIR\rsremote64.zip" -d "$INSTDIR"'

  ; copy mysql-connector.jar
  StrCmp $has_mysql "TRUE" 0 end_copy_mysqlconnector
      ; Delete "$INSTDIR\rs-thirdparty.zip"
      CopyFiles $mysqlconnector $INSTDIR\lib\mysql-connector.jar
      CopyFiles $mysqlconnector $INSTDIR\tomcat\webapps\resolve\WEB-INF\lib\mysql-connector.jar
  end_copy_mysqlconnector:

  ; copy blueprint.properties
  CopyFiles $blueprint $INSTDIR\rsmgmt\config\blueprint.properties


  ; create softlink to jre
  ExecWait '"$INSTDIR\ln.exe" "$INSTDIR\jre7\jre" "$INSTDIR\jre7"'

  ; setup configuration
  ExecWait 'cscript.exe "$INSTDIR\setup-rsmgmt-win.vbs"'
  sleep 5000
  ExecWait 'cscript.exe "$INSTDIR\setup-rsremote-win.vbs"'
  sleep 5000

  ; execute rsconsole
  
  ExecWait '"$INSTDIR\rsmgmt\bin\run.bat" -u admin -p resolve -f install/resolve-configure "-BDrsremote.esb.queue.name.2=WINDOWS" -s'
  sleep 5000
  ExecWait '"$INSTDIR\rsmgmt\bin\run.bat" -u admin -p resolve -f install/resolve-install-post'
  sleep 5000

  ; delete zip file
  Delete "$INSTDIR\rs-common.zip"
  Delete "$INSTDIR\rs-thirdparty64.zip"
  Delete "$INSTDIR\rsmgmt64.zip"
  Delete "$INSTDIR\rsremote64.zip"
  
SectionEnd

;--------------------------------
; Components Descriptions
;--------------------------------

;Assign language strings to sections
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
!insertmacro MUI_DESCRIPTION_TEXT ${SECTION_RESOLVE} $(DESC_RESOLVE)
!insertmacro MUI_FUNCTION_DESCRIPTION_END


;--------------------------------
; Uninstaller
;--------------------------------

Section "Uninstall"
  
  ; stop services
  ExecWait 'sc.exe stop rsmgmt'
  sleep 5000
  ExecWait 'sc.exe stop rsremote'
  sleep 5000

  ; delete services
  ExecWait 'sc.exe delete rsmgmt'
  sleep 5000
  ExecWait 'sc.exe delete rsremote'
  sleep 5000

  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Resolve"

  ; Remove files and uninstaller
  Delete $INSTDIR\.nsi
  Delete $INSTDIR\uninstall_resolve.exe

  ; Remove directories used
  RMDir /r "$INSTDIR"

SectionEnd


;--------------------------------
; Resolve Functions
;--------------------------------

Function .onInit

  ; Extract InstallOptions Plugin files
  InitPluginsDir
  File /oname=$PLUGINSDIR\rsremote.ini "rsremote.ini"

FunctionEnd


Function Settings

  ; Display InstallOptions Dialog

  !insertmacro MUI_HEADER_TEXT "$(TEXT_IO_TITLE)" "$(TEXT_IO_SUBTITLE)"
  !insertmacro MUI_INSTALLOPTIONS_DISPLAY "ioFile.ini"

  Push ${TEMP}
  InstallOptions::dialog "$PLUGINSDIR\rsremote.ini"
  Pop ${TEMP}

  Pop ${TEMP}

FunctionEnd


Function ValidateSettings

  ; Check if blueprint file exists
  ReadINIStr ${TEMP} "$PLUGINSDIR\rsremote.ini" "Field 3" "State"
  IfFileExists ${TEMP} true_blueprint false_blueprint
  true_blueprint:
      goto end_check_blueprint
  false_blueprint:
      MessageBox MB_ICONEXCLAMATION|MB_OK "Please ensure that the blueprint.properties file exists."
      Abort
      goto end_check_blueprint
  end_check_blueprint:

FunctionEnd


;--------------------------------
; Third-Party Functions
;--------------------------------

; Function GetParameters
;
; input, none
; output, top of stack (replaces, with e.g. whatever)
; modifies no other variables.
 
Function GetParameters
 
  Push $R0
  Push $R1
  Push $R2
  Push $R3
  
  StrCpy $R2 1
  StrLen $R3 $CMDLINE
  
  ;Check for quote or space
  StrCpy $R0 $CMDLINE $R2
  StrCmp $R0 '"' 0 +3
    StrCpy $R1 '"'
    Goto loop
  StrCpy $R1 " "
  
  loop:
    IntOp $R2 $R2 + 1
    StrCpy $R0 $CMDLINE 1 $R2
    StrCmp $R0 $R1 get
    StrCmp $R2 $R3 get
    Goto loop
  
  get:
    IntOp $R2 $R2 + 1
    StrCpy $R0 $CMDLINE 1 $R2
    StrCmp $R0 " " get
    StrCpy $R0 $CMDLINE "" $R2
  
  Pop $R3
  Pop $R2
  Pop $R1
  Exch $R0
 
FunctionEnd

; Function GetParameterValue
;
; Chris Morgan<cmorgan@alum.wpi.edu> 5/10/2004
; -Updated 4/7/2005 to add support for retrieving a command line switch
;  and additional documentation
;
; Searches the command line input, retrieved using GetParameters, for the
; value of an option given the option name.  If no option is found the
; default value is placed on the top of the stack upon function return.
;
; This function can also be used to detect the existence of just a
; command line switch like /OUTPUT  Pass the default and "OUTPUT"
; on the stack like normal.  An empty return string "" will indicate
; that the switch was found, the default value indicates that
; neither a parameter or switch was found.
;
; Inputs - Top of stack is default if parameter isn't found,
;  second in stack is parameter to search for, ex. "OUTPUT"
; Outputs - Top of the stack contains the value of this parameter
;  So if the command line contained /OUTPUT=somedirectory, "somedirectory"
;  will be on the top of the stack when this function returns
;
; Register usage
;$R0 - default return value if the parameter isn't found
;$R1 - input parameter, for example OUTPUT from the above example
;$R2 - the length of the search, this is the search parameter+2
;      as we have '/OUTPUT='
;$R3 - the command line string
;$R4 - result from StrStr calls
;$R5 - search for ' ' or '"'
 
Function GetParameterValue
  Exch $R0  ; get the top of the stack(default parameter) into R0
  Exch      ; exchange the top of the stack(default) with
            ; the second in the stack(parameter to search for)
  Exch $R1  ; get the top of the stack(search parameter) into $R1
 
  ;Preserve on the stack the registers used in this function
  Push $R2
  Push $R3
  Push $R4
  Push $R5
 
  Strlen $R2 $R1+2    ; store the length of the search string into R2
 
  Call GetParameters  ; get the command line parameters
  Pop $R3             ; store the command line string in R3
 
  # search for quoted search string
  StrCpy $R5 '"'      ; later on we want to search for a open quote
  Push $R3            ; push the 'search in' string onto the stack
  Push '"/$R1='       ; push the 'search for'
  Call StrStr         ; search for the quoted parameter value
  Pop $R4
  StrCpy $R4 $R4 "" 1   ; skip over open quote character, "" means no maxlen
  StrCmp $R4 "" "" next ; if we didn't find an empty string go to next
 
  # search for non-quoted search string
  StrCpy $R5 ' '      ; later on we want to search for a space since we
                      ; didn't start with an open quote '"' we shouldn't
                      ; look for a close quote '"'
  Push $R3            ; push the command line back on the stack for searching
  Push '/$R1='        ; search for the non-quoted search string
  Call StrStr
  Pop $R4
 
  ; $R4 now contains the parameter string starting at the search string,
  ; if it was found
next:
  StrCmp $R4 "" check_for_switch ; if we didn't find anything then look for
                                 ; usage as a command line switch
  # copy the value after /$R1= by using StrCpy with an offset of $R2,
  # the length of '/OUTPUT='
  StrCpy $R0 $R4 "" $R2  ; copy commandline text beyond parameter into $R0
  # search for the next parameter so we can trim this extra text off
  Push $R0
  Push $R5            ; search for either the first space ' ', or the first
                      ; quote '"'
                      ; if we found '"/output' then we want to find the
                      ; ending ", as in '"/output=somevalue"'
                      ; if we found '/output' then we want to find the first
                      ; space after '/output=somevalue'
  Call StrStr         ; search for the next parameter
  Pop $R4
  StrCmp $R4 "" done  ; if 'somevalue' is missing, we are done
  StrLen $R4 $R4      ; get the length of 'somevalue' so we can copy this
                      ; text into our output buffer
  StrCpy $R0 $R0 -$R4 ; using the length of the string beyond the value,
                      ; copy only the value into $R0
  goto done           ; if we are in the parameter retrieval path skip over
                      ; the check for a command line switch
 
; See if the parameter was specified as a command line switch, like '/output'
check_for_switch:
  Push $R3            ; push the command line back on the stack for searching
  Push '/$R1'         ; search for the non-quoted search string
  Call StrStr
  Pop $R4
  StrCmp $R4 "" done  ; if we didn't find anything then use the default
  StrCpy $R0 ""       ; otherwise copy in an empty string since we found the
                      ; parameter, just didn't find a value
 
done:
  Pop $R5
  Pop $R4
  Pop $R3
  Pop $R2
  Pop $R1
  Exch $R0 ; put the value in $R0 at the top of the stack
FunctionEnd

; Function StrStr
 ; input, top of stack = string to search for
 ;        top of stack-1 = string to search in
 ; output, top of stack (replaces with the portion of the string remaining)
 ; modifies no other variables.
 ;
 ; Usage:
 ;   Push "this is a long ass string"
 ;   Push "ass"
 ;   Call StrStr
 ;   Pop $R0
 ;  ($R0 at this point is "ass string")

Function StrStr
  Exch $R1 ; st=haystack,old$R1, $R1=needle
  Exch    ; st=old$R1,haystack
  Exch $R2 ; st=old$R1,old$R2, $R2=haystack
  Push $R3
  Push $R4
  Push $R5
  StrLen $R3 $R1
  StrCpy $R4 0
  ; $R1=needle
  ; $R2=haystack
  ; $R3=len(needle)
  ; $R4=cnt
  ; $R5=tmp
  loop:
    StrCpy $R5 $R2 $R3 $R4
    StrCmp $R5 $R1 done
    StrCmp $R5 "" done
    IntOp $R4 $R4 + 1
    Goto loop
done:
  StrCpy $R1 $R2 "" $R4
  Pop $R5
  Pop $R4
  Pop $R3
  Pop $R2
  Exch $R1
FunctionEnd
