set args = WScript.arguments.Named

set objshell = CreateObject("WScript.Shell")

' installdir
installdir = objshell.CurrentDirectory
if args.exists("dir") then
    installdir = args.item("dir")
end if

' ipaddr
ipaddr = "localhost"
if args.exists("ipaddr") then
    ipaddr = args.item("ipaddr")
end if

noservice = "false"
listenerport = "8000"
remotehost = "remotehost"
remoteport = "4004"
svnaddr = ipaddr & ":12438"

' listenerport
if args.exists("listenerport") then
    listenerport = args.item("listenerport")
end if

' remotehost
if args.exists("remotehost") then
    remotehost = args.item("remotehost")
end if

' remoteport
if args.exists("remoteport") then
    remoteport = args.item("remoteport")
end if

' noservice
if args.exists("noservice") then
    noservice = args.item("noservice")
end if

' print config file
WScript.echo "Generating RSPROXY config.xml file"
CR = VBCR & VBLF
config = "<?xml version=""1.0"" encoding=""UTF-8""?>" & CR _
    & "<CONFIGURATION> " & CR _
    & "    <GENERAL HOME=""" & installdir & """ />" & CR _ 
	& "    <PROXY>" & CR_
    & "        <LISTENER HOST="""& ipaddr &""" LISTENER_PORT="""& listenerport &""" REMOTE_HOST="""& remotehost &""" REMOTE_PORT="""& remoteport &"""/>" & CR_
    & "    </PROXY>" & CR _
    & "</CONFIGURATION>"
'WScript.echo config
fileName = installdir & "\rsproxy\config\config.xml"
WScript.echo "Config file name: " & fileName

const OVERWRITE = 2
set fso = CreateObject("Scripting.FileSystemObject")

set file = fso.OpenTextFile(fileName, 1)
strText = file.ReadAll
file.Close

strNewText = Replace(strText, "INSTALLDIR", installdir)

set file = fso.OpenTextFile(fileName, 2)
file.WriteLine strNewText
' file.write config
file.close

' install windows service
if noservice <> "" then

    ' go to bin directory
    objshell.CurrentDirectory = "rsproxy/bin"

    WScript.echo "Installing Windows service"
    objshell.run "install-service.bat", 0, TRUE

    ' start service
'        set wmi = GetObject("winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
'        set services = wmi.execQuery("SELECT * FROM Win32_Service WHERE name='rsproxy'")
'        for each service in services
'            err = service.Change ( , , , , , True, , "")  
'            if err = 0 then
'                service.startService()
'                WScript.echo "Started service " & service.displayName
'            else
'                WScript.echo "FAIL: install service or set desktop interaction mode"
'            end if
'        next
end if

