set args = WScript.arguments.Named

set objshell = CreateObject("WScript.Shell")

' installdir
installdir = objshell.CurrentDirectory
if args.exists("dir") then
    installdir = args.item("dir")
end if

' ipaddr
ipaddr = "localhost"
if args.exists("ipaddr") then
    ipaddr = args.item("ipaddr")
end if

esbaddr = ipaddr & ":4004"
esbuser = "admin"
esbpass = "resolve"
svnaddr = ipaddr & ":12438"

' esbaddr
if args.exists("esbaddr") then
    esbaddr = args.item("esbaddr")
end if

' esbuser
if args.exists("esbuser") then
    esbuser = args.item("esbuser")
end if

' esbpass
if args.exists("esbpass") then
    esbpass = args.item("esbpass")
end if

' svnaddr
if args.exists("svnaddr") then
    esbpass = args.item("svnaddr")
end if

' print config file
WScript.echo "Generating RSCONSOLE config.xml file"
CR = VBCR & VBLF
config = "<?xml version=""1.0"" encoding=""UTF-8""?>" & CR _
    & "<CONFIGURATION> " & CR _
    & "    <GENERAL HOME=""" & installdir & """ />" & CR _ 
    & "    <ESB PRODUCT=""SWIFTMQ"" BROKERADDR="""& esbaddr &""" USERNAME="""& esbuser &""" PASSWORD="""& esbpass &""" DOMAIN=""domain1"" BROKERNAME=""router1"" MGMTADDR="""" />" & CR _
    & "    <USERS>" & CR _
    & "        <USER USERNAME=""admin"" PASSWORD=""resolve"" />" & CR _
    & "    </USERS>" & CR _
    & "</CONFIGURATION>"
'WScript.echo config

const OVERWRITE = 2
set fso = CreateObject("Scripting.FileSystemObject")
set file = fso.CreateTextFile("rsconsole/config/config.xml")
file.write config
file.close
