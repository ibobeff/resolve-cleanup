#!/bin/bash

if [ $# -lt 2 ]; then
    echo "Usage: -f <filename> [OPTIONS]"
    echo ""
    echo "This script will upgrade Resolve components."
    echo "The upgrade zip file should be placed in the install directory."
    echo "The Name of the upgrade zip is provided with -f."
    echo ""
    echo "Options:"
    echo "    -u <username> Username for RSConsole (used to Import upgraded"
    echo "                  libraries)"
    echo "    -p <password> Password for RSConsole (used to Import upgraded"
    echo "                  libraries)"
    echo "    --force       Continue Upgrade Even if Backup of Same ID"
    echo "                  is detected"
    echo "    --no-config   Do not run configuration off of blueprint"
    echo "                  after updating"
    echo "    --no-start    Do not start all components after upgrade is"
    echo "                  complete"
    echo "    --no-stop     Do not stop all components before applying"
    echo "                  upgrade"
    echo "    --no-restart  Do not stop all components before applying"
    echo "                  upgrade and do not start all components after"
    echo "                  upgrade is complete"
    echo "    --no-import   Do not import any Upgraded Modules"
    echo "    --purge-index Purge and re-index all components for search"
    echo "    --skip-sql    Do not apply any SQL upgrades included in the"
    echo "                  upgrade."
    echo "    --skip-cli    Do not apply any CLI upgrades included in the"
    echo "                  upgrade."
    echo "    -dns          DNS Host Name to set into rsview properties"
    echo "                  for system url (3.4 upgrade only)"
    echo "    -port         Host Port to set into rsview properties for"
    echo "                  system url (3.4 upgrade only)"
    echo ""
    exit 1
fi

DIST=`echo $0 | awk '{ print substr( $0, 0, length($0)-10 ) }'`/..
OPTIONS="--backup-index"
BLUEPRINT_OPTIONS=""
FORCE=FALSE
BLUEPRINT=${DIST}/bin/blueprint.properties
ANSWER=
STOP=TRUE
SUCCESS=FALSE
LICENSE=

# set os type
linux=false
solaris=false
case "`uname`" in
Linux*) linux=true;;
SunOS*) solaris=true;;
esac

while [ $# -gt 0 ]
do
    if [ $1 == "-u" ]; then
        shift
        if [ $# -gt 0 ]; then
            USERNAME=$1
        else
            USERNAME=admin
        fi
    elif [ $1 == "-p" ]; then
        shift
        if [ $# -gt 0 ]; then
            PASSWORD=$1
        else
            PASSWORD=resolve
        fi
    elif [ $1 == "-f" ]; then
        shift
        if [ $# -gt 0 ]; then
            FILENAME=$1
        fi
    elif [ $1 == "-lic" ]; then
        shift
        if [ $# -gt 0 ]; then
            LICENSE=$1
        fi
    elif [ $1 == "-dns" ]; then
        shift
        if [ $# -gt 0 ]; then
            if [ "${BLUEPRINT_OPTIONS}x" == "x" ]; then
                BLUEPRINT_OPTIONS="-BDDNS_HOST=$1"
            else
                BLUEPRINT_OPTIONS="$BLUEPRINT_OPTIONS;DNS_HOST=$1"
            fi
        fi
    elif [ $1 == "-port" ]; then
        shift
        if [ $# -gt 0 ]; then
            if [ "${BLUEPRINT_OPTIONS}x" == "x" ]; then
                BLUEPRINT_OPTIONS="-BDDNS_PORT=$1"
            else
                BLUEPRINT_OPTIONS="$BLUEPRINT_OPTIONS;DNS_PORT=$1"
            fi
        fi
    else
        if [ $1 == "--force" ]; then
            FORCE=TRUE
        elif [ $1 == "--auto-accept" ]; then
            ANSWER="AUTO"
        elif [ $1 == "--no-stop" ] || [ $1 == "--no-restart" ]; then
            STOP=FALSE
        fi
        if [ "${OPTIONS}x" == "x" ]; then
            OPTIONS=$1
        else
            OPTIONS="$OPTIONS;$1"
        fi
    fi
    shift
done

if [ "${FORCE}" == "FALSE" ]; then
    LIC_TEST=`ls *.lic 2>/dev/null`
    if [ "${LIC_TEST}x" == "x" ]; then
        if [ -f $DIST/bin/init.rsview ]; then
            RSVIEW_MEM=`grep "rsview.run.Xmx=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}'`
            if [[ $RSVIEW_MEM =~ "^[0-9]+$" ]]; then
                if [ $RSVIEW_MEM -gt 512 ]; then
                    echo "WARNING!!! No License File Found.  The RSView Memory (${RSVIEW_MEM}) is"
                    echo "restricted to 512 without a License.  Please either reduce the RSView"
                    echo "allocated memory or contact support to receive a license"
                    echo "Cancelling Upgrade"
                    exit 1
                fi
            fi
        fi
        if [ -f $DIST/bin/init.rscontrol ]; then
            RSCONTROL_MEM=`grep "rscontrol.run.Xmx=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}'`
            if [[ $RSCONTROL_MEM =~ "^[0-9]+$" ]]; then
                if [ $RSCONTROL_MEM -gt 512 ]; then
                    echo "WARNING!!! No License File Found.  The RSControl Memory (${RSCONTROL_MEM}) is"
                    echo "restricted to 512 without a License.  Please either reduce the RSControl"
                    echo "allocated memory or contact support to receive a license"
                    echo "Cancelling Upgrade"
                    exit 1
                fi
            fi
        fi
    fi
fi

R_USER=`grep "resolve.user=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [A-Z] [a-z]`
ID=`id | sed -e "s/^uid=[0-9]*(\(.*\)) gid.*/\1/"`
if [ "${ID}x" != "${R_USER}x" ]; then
    echo "WARNING!!! Current User ID (${ID}) does not match the resolve.user property specified in the blueprint.properties"
    if [ "${FORCE}" == "FALSE" ]; then
        echo "The Resolve upgrade must be run as the User ID specified by resolve.user in the blueprint.properties."
        echo "Please either continue the upgrade as the User specified by resolve.user (${R_USER}),"
        echo "or use the --force option if you wish to continue the upgrade as is."
        echo "Cancelling Upgrade"
        exit 1
    fi
fi

if [ "${FILENAME}x" == "x" ]; then
    echo "Missing Upgrade File Name"
    echo "Cancelling Upgrade"
    exit 1
elif [ ! -f $FILENAME ]; then
    echo "Upgrade File $FILENAME Does Not Exist"
    echo "Cancelling Upgrade"
    exit 1
else
    TMPFILENAME=`echo $FILENAME | awk 'BEGIN{FS="/"}{print $NF}' | awk 'BEGIN{FS="-"}{print$1}'`
    if [ "${TMPFILENAME}" != "upgrade" ]; then
        echo "WARNING!!! File $FILENAME does not appear to be an Upgrade file"
        if [ "${FORCE}" == "FALSE" ]; then
            echo "Please make sure the file specified is an upgrade and has not been renamed"
            exit 1
        fi
    fi
    FILEDATE=`echo $FILENAME | awk 'BEGIN{FS="/"}{print $NF}' | awk 'BEGIN{FS="-"}{print$NF}' | awk 'BEGIN{FS="."}{print$1}'`
    LOCKFILE=${DIST}/file/lock-${FILEDATE}
    if [ ! -f $LOCKFILE ]; then
        echo "Lock File not detected.  Please unzip the Upgrade file in the installation directory before starting the upgrade"
        exit 1
    fi

    if [ "${PASSWORD}x" == "x" ]; then
        if [ "${ANSWER}" == "AUTO" ]; then
            PASSWORD=resolve
        else
            printf "Password: "
            read -s PASSWORD
            echo ""
        fi
    fi

    if [ "${USERNAME}x" == "x" ]; then
        USERNAME=admin
    fi

    LICENSETXT=`unzip -l $FILENAME | grep " file/license/license.txt" 2>/dev/null`
    if [ "${LICENSETXT}x" != "x" ] && [ "${ANSWER}x" == "x" ] ; then
        unzip -p $FILENAME file/license/license.txt

        printf "Do You Accept The Above? (Y/N) "

        while [ "${ANSWER}x" == "x" ]
        do
            read inputline
            ANSWER=`echo $inputline | tr [A-Z] [a-z]`
            if [ "${ANSWER}" == "y" ] || [ "${ANSWER}" == "yes" ]; then
                ANSWER="Y"
            elif [ "${ANSWER}" == "n" ] || [ "${ANSWER}" == "no" ]; then
                ANSWER="N"
            else
                ANSWER=
                printf "Please Enter Y or N "
            fi
        done

        if [ "${ANSWER}" == "N" ]; then
            echo "Cancelling Upgrade"
            exit 1
        fi
    fi

    if [ "${ANSWER}" != "AUTO" ]; then
        echo ""
        echo "Please backup your resolve servers and database before proceeding"
        echo "with the upgrade. For 5.1 upgrade, all servers must be upgraded"
        echo "simultaneously"
        echo "Ctrl + C can be used to terminate the upgrade process at any time"

        printf "Do You Wish to Continue? (Y/N) "
        ANSWER=

        while [ "${ANSWER}x" == "x" ]
        do
            read inputline
            ANSWER=`echo $inputline | tr [A-Z] [a-z]`
            if [ "${ANSWER}" == "y" ] || [ "${ANSWER}" == "yes" ]; then
                ANSWER="Y"
            elif [ "${ANSWER}" == "n" ] || [ "${ANSWER}" == "no" ]; then
                ANSWER="N"
            else
                ANSWER=
                printf "Please Enter Y or N "
            fi
        done

        if [ "${ANSWER}" == "N" ]; then
            echo "Cancelling Upgrade"
            exit 1
        fi
    fi

    chmod 755 ${DIST}/rsmgmt/bin/*.sh 2>/dev/null
    ${DIST}/rsmgmt/bin/run.sh -f install/resolve-update-cleanup "-FD$OPTIONS" -u
    cp $FILENAME $DIST 2>/dev/null
    FILENAME=`echo $FILENAME | awk 'BEGIN{FS="/"}{print $NF}'`
    unzip -oq $DIST/$FILENAME -d $DIST

    # replace default ${ORIGDIR} with installdir in rsmgmt
    cd ${DIST}
    ORIGDIR=INSTALLDIR
    INSTALLDIR=`pwd`

    if $linux; then
        chmod 755 ${INSTALLDIR}/file/swiftmq/scripts/linux/* 2>/dev/null
        chmod 755 ${INSTALLDIR}/file/rabbitmq/linux64/rabbitmq/sbin/* 2>/dev/null
        chmod 755 ${INSTALLDIR}/file/rabbitmq/linux64/erlang/bin/* 2>/dev/null
        chmod 755 ${INSTALLDIR}/file/rabbitmq/linux64/erlang/erts-7.3/bin/* 2>/dev/null
    elif $solaris; then
        chmod 755 ${INSTALLDIR}/file/swiftmq/scripts/solaris/* 2>/dev/null
        chmod 755 ${INSTALLDIR}/file/rabbitmq/solaris/rabbitmq/sbin/* 2>/dev/null
        chmod 755 ${INSTALLDIR}/file/rabbitmq/solaris/erlang/bin/* 2>/dev/null
        chmod 755 ${INSTALLDIR}/file/rabbitmq/solaris/erlang/erts-7.3/bin/* 2>/dev/null
    fi
    chmod 755 ${INSTALLDIR}/file/rsconsole/bin/*.sh 2>/dev/null
    chmod 755 ${INSTALLDIR}/file/rsconsole/bin/init.* 2>/dev/null
    chmod 755 ${INSTALLDIR}/file/rsmgmt/bin/*.sh 2>/dev/null
    chmod 755 ${INSTALLDIR}/file/rsmgmt/bin/init.* 2>/dev/null
    chmod 755 ${INSTALLDIR}/file/rscontrol/bin/*.sh 2>/dev/null
    chmod 755 ${INSTALLDIR}/file/rscontrol/bin/init.* 2>/dev/null
    chmod 755 ${INSTALLDIR}/file/rsremote/bin/*.sh 2>/dev/null
    chmod 755 ${INSTALLDIR}/file/rsremote/bin/init.* 2>/dev/null
    chmod 754 ${INSTALLDIR}/file/tomcat/bin/*.sh 2>/dev/null
    chmod 754 ${INSTALLDIR}/file/tomcat/bin/init.* 2>/dev/null
    chmod 755 ${INSTALLDIR}/file/cassandra/bin/* 2>/dev/null
    chmod 755 ${INSTALLDIR}/file/cassandra/conf/*.sh 2>/dev/null
    chmod 755 ${INSTALLDIR}/file/elasticsearch/bin/*.sh 2>/dev/null
    chmod 755 ${INSTALLDIR}/file/elasticsearch/bin/init.* 2>/dev/null
    chmod 755 ${INSTALLDIR}/file/elasticsearch/bin/elasticsearch 2>/dev/null
    chmod 755 ${INSTALLDIR}/file/bin/*.sh 2>/dev/null

    if $linux; then
        chmod 755 ${INSTALLDIR}/rabbitmq/linux64/rabbitmq/sbin/* 2>/dev/null
        chmod 755 ${INSTALLDIR}/rabbitmq/linux64/erlang/bin/* 2>/dev/null
        chmod 755 ${INSTALLDIR}/rabbitmq/linux64/erlang/erts-7.3/bin/* 2>/dev/null
    elif $solaris; then
        chmod 755 ${INSTALLDIR}/rabbitmq/solaris/rabbitmq/sbin/* 2>/dev/null
        chmod 755 ${INSTALLDIR}/rabbitmq/solaris/erlang/bin/* 2>/dev/null
        chmod 755 ${INSTALLDIR}/rabbitmq/solaris/erlang/erts-7.3/bin/* 2>/dev/null
    fi

    chmod 755 ${INSTALLDIR}/rsmgmt/bin/*.sh 2>/dev/null
    chmod 755 ${INSTALLDIR}/rsmgmt/bin/init.* 2>/dev/null
    chmod 755 ${INSTALLDIR}/elasticsearch/bin/*.sh 2>/dev/null
    chmod 755 ${INSTALLDIR}/elasticsearch/bin/init.* 2>/dev/null
    chmod 755 ${INSTALLDIR}/elasticsearch/bin/elasticsearch 2>/dev/null
    chmod 755 ${INSTALLDIR}/bin/*.sh 2>/dev/null
fi

if [ "${STOP}" == "TRUE" ]; then
    ######################MCP Monitored Statement
    echo "Stopping Resolve Components"
    ######################MCP Monitored Statement
    ${INSTALLDIR}/bin/stop.sh ALL
fi

CP=
for FILE in `ls $INSTALLDIR/file/tmp/`
do
    case $FILE in
    *.jar)
        if [ "${CP}x" == "x" ]; then
            CP="${INSTALLDIR}/file/tmp/$FILE"
        else
            CP="${CP}:${INSTALLDIR}/file/tmp/$FILE"
        fi
    ;;
    esac
done

"${INSTALLDIR}/jdk/bin/java" -cp "$CP" -Dorg.owasp.esapi.resources="${INSTALLDIR}/file/tmp" com.resolve.update.Main -f file/install-update.groovy "-FD$FILENAME;$USERNAME;$PASSWORD;$OPTIONS" -d "${INSTALLDIR}"
if [ $? -eq 0 ] ; then
    /bin/sed -e "s|${ORIGDIR}|${INSTALLDIR}|g" rsmgmt/config/config.xml > sed.tmp
    mv -f sed.tmp rsmgmt/config/config.xml

    "${INSTALLDIR}/bin/config.sh" --skip-dashboard

    if [ "${LICENSE}x" != "x" ]; then 
        if [ -f $LICENSE ]; then
            rsmgmt/bin/run.sh -u admin -p resolve -f install/resolve-install-license "-FD$LICENSE" $OPTIONS -u
        else
            echo "WARNING!!! License File $LICENSE Could Not Be Found"
        fi
    fi

    "${INSTALLDIR}/rsmgmt/bin/run.sh" -f install/resolve-update "-FD$FILENAME;$USERNAME;$PASSWORD;$OPTIONS" -u

    if [ $? -eq 0 ] ; then
        SUCCESS=TRUE
        rm $FILENAME 2>/dev/null
    fi

    if [ -f $INSTALLDIR/bin/init.rsview ]; then
        ${INSTALLDIR}/bin/dashboard.sh UPDATE
    fi
fi

if [ "${SUCCESS}" = "TRUE" ]; then
    ######################MCP Monitored Statement
    echo "Resolve Upgrade Finished in $SECONDS seconds"
    ######################MCP Monitored Statement
    rsmgmt/bin/run.sh -f mgmt/Log "-FDResolve;Upgrade;Finished;in;$SECONDS;seconds" -u 1>/dev/null
else
    ######################MCP Monitored Statement
    echo "Resolve Upgrade Failed in $SECONDS seconds"
    ######################MCP Monitored Statement
    rsmgmt/bin/run.sh -f mgmt/Log "-FDResolve;Upgrade;Failed;in;$SECONDS;seconds" -u 1>/dev/null
fi
