#!/bin/bash

if [ $# -lt 1 ] || [ "$1" != "--run" ]; then
    echo ""
    echo "Usage: <--run>"
    echo "This script will run each of the setup scripts with default configurations. Each"
    echo "script must be run as root.  To see details of each script run them individually"
    echo "with the --help option"
    exit 0
fi

ID=`id | sed -e "s/^uid=[0-9]*(\(.*\)) gid.*/\1/"`

if [ "${ID}x" != "rootx" ]; then
    echo "The Server Setup Must be run as the Root User"
    exit 0
fi

DIST=`echo $0 | awk '{ print substr( $0, 0, length($0)-15 ) }'`/..

$DIST/bin/setup_services.sh all
$DIST/bin/setup_limits.sh --default --default --default --default
$DIST/bin/setup_sysctl.sh --default
