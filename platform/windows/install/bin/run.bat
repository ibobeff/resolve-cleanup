@echo off

if not "%1" == "" GOTO :START
    echo.
    echo "Usage: <Resolve Component...>"
    echo "This script will start up the specified Resolve Components"
    echo "ALL can be used to start up all components (except RSCONSOLE) installed on this system"
    GOTO :END

:START
pushd %~dp0..
set DIST=%CD%
popd
:LOOP
if "%1" == "" GOTO :END_LOOP
    if /i "%1" == "ALL" GOTO :ALL
		if /i "%1" == "RSREMOTE" call %DIST%\rsremote\bin\rsremote-updateService.bat
        sc start %1
    GOTO :SHIFT
        :ALL
            sc start rsmq
            sc start rsmgmt
            sc start rssearch
            :SLEEP_LOOP
                echo|set /p=.
                ping 123.45.67.89 -n 1 -w 5000 > nul
                set /a COUNT += 1
                if %COUNT% LSS 12 GOTO :SLEEP_LOOP
            sc start rsview
            sc start rscontrol
			call %DIST%\rsremote\bin\rsremote-updateService.bat
            sc start rsremote
    :SHIFT
    shift
GOTO :LOOP
:END_LOOP

:END
set COUNT=
