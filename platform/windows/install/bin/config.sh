#!/bin/bash

if [ "$1" == "--help" ]; then
   echo "This script will configure Resolve components in the current directory."
   echo "The rsmgmt/configure/blueprint.properties file will be used to configure the"
   echo "Resolve installation"
   echo ""
   exit 1
fi

DIST=`echo $0 | awk '{ print substr( $0, 0, length($0)-9 ) }'`/..

OPTIONS=""
FLAGS=""

if [ -d "${DIST}/tomcat/conf" ]; then
 chmod 600 `find ${DIST}/tomcat/conf -type f`
fi
while [ $# -gt 0 ]
do
    if [[ $1 == -* ]]; then
        if [ "${FLAGS}x" == "x" ]; then
            FLAGS=$1
        else
            FLAGS="$FLAGS $1"
        fi
    elif [ "${OPTIONS}x" == "x" ]; then
        OPTIONS=$1
    else
        OPTIONS="$OPTIONS;$1"
    fi
    shift
done

${DIST}/rsmgmt/bin/run.sh $FLAGS -f install/resolve-configure -s "-FD$OPTIONS"

if [ "$?" == "255" ]; then
  echo "WARNING!!! Insufficient diskspace."
  exit 1
fi

if [ -d "${DIST}/tomcat/conf" ]; then
 chmod 400 `find ${DIST}/tomcat/conf -type f`
fi
