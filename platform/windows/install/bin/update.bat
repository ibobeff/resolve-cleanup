@echo off
setlocal EnableDelayedExpansion

IF NOT "%1" == "" GOTO :START
    :HELP
    echo Usage: -f ^<filename^> [OPTIONS]
    echo.
    echo This script will update Resolve components.
    echo The update zip file should be placed in the install directory.
    echo The Name of the update zip is provided with -f.
    echo.
    echo Options:
    echo     -u            RSConsole username, used to import any modules
    echo                   included in the update. Default will be used
    echo                   if this option is not used
    echo     -p            RSConsole password, used to import any modules
    echo                   included in the update. Default will be used
    echo                   if this option or -pw option is not used
    echo     -pw           Prompt user for RSConsole password. Alternative
    echo                   to providing it with -p option
    echo     --force          Continue Update Even if Backup of Same ID
    echo                      is detected
    echo     --no-config      Do not run configuration off of blueprint
    echo                      after updating
    echo     --no-start       Do not start all components after update is
    echo                      complete
    echo     --no-stop        Do not stop all components before applying
    echo                      update
    echo     --no-restart     Do not stop all components before applying
    echo                      update and do not start all components after
    echo                      update is complete
    echo     --no-import      Do not import any Updated Modules
    echo     --reset-services Uninstall and Reinstall all Services
    echo     --skip-sql       Do not apply any SQL updates included in the
    echo                      update.
    echo.
    GOTO :END

:START

set DIST=%~dp0..
set UPDATE_OPTIONS=
set USERNAME=admin
set PASSWORD=resolve
set ANSWER=
set SUCCESS=FALSE

:LOOP
set ARG=%1
IF "%ARG%" == "" GOTO :END_LOOP
    IF NOT "%ARG%" == "-u" GOTO :PASS
        shift
        set ARG=%1
        IF "%ARG%" == "" goto :NO_USERNAME
        IF "%ARG:~0,1%" == "-" goto :NO_USERNAME
            set USERNAME="%ARG%"
            GOTO :SHIFT
        :NO_USERNAME
            echo Must Supply Username With -u
            GOTO :END
    :PASS
    IF NOT "%ARG%" == "-p" GOTO :FILE
        shift
        set ARG=%1
        IF "%ARG%" == "" GOTO :NO_PASSWORD
        IF "%ARG:~0,1%" == "-" goto :NO_PASSWORD
            set PASSWORD="%ARG%"
            GOTO :SHIFT
        :NO_PASSWORD
            echo Must Supply Password With -p
            GOTO :END
    :FILE
    IF NOT "%ARG%" == "-f" GOTO :OPTIONS
        shift
        set ARG=%1
        IF "%ARG%" == "" GOTO :HELP
            set FILENAME=%ARG%
            set ZIPFILENAME=%~nx1
            GOTO :SHIFT
    :OPTIONS
    IF "%ARG%" == "--auto-accept" (SET ANSWER=AUTO)
    IF NOT "%UPDATE_OPTIONS%" == "" GOTO :ADDOPTION
        set UPDATE_OPTIONS=%ARG%
        GOTO :SHIFT
    :ADDOPTION
        set UPDATE_OPTIONS=%UPDATE_OPTIONS%;%ARG%
        GOTO :SHIFT
    :SHIFT
    shift
    set ARG=%1
GOTO :LOOP
:END_LOOP

IF EXIST "%FILENAME%" GOTO :UPDATE
    echo Update File %FILENAME% Does Not Exist
    GOTO :END

:UPDATE
rem echo Dist: %DIST%
REM set DIST1=%DIST%

copy /V /Y /B "%FILENAME%" "%DIST%"
rem echo \tprevious to unzip
"%DIST%\unzip" -oq "%DIST%\%ZIPFILENAME%" -d "%DIST%"
rem echo \tafter unzip
pushd %DIST%
set TMPDIST=%CD%
popd

FOR %%c in ("%DIST%\file\tmp\*.jar") DO (
    set CP=!CP!;%DIST%/file/tmp/%%~nc%%~xc
    rem END CP_LOOP
)
call "%DIST%\file\tmp\windows_update.bat"
call "%DIST%\jdk\bin\java" -cp "%CP%" -Dorg.owasp.esapi.resources="%DIST%\file\tmp" com.resolve.update.Main "-FD%UPDATE_OPTIONS%" -d "%TMPDIST%" -f "%DIST%\file\tmp\update.xml" -u %USERNAME% -p %PASSWORD% -z %ZIPFILENAME%

IF ERRORLEVEL 1 GOTO :MCP
    set SUCCESS=TRUE
    del /F /Q %ZIPFILENAME%

:MCP
IF %SUCCESS%==FALSE GOTO :FAIL_MESSAGE
    rem ######################MCP Monitored Statement
    echo Resolve Update Finished
    rem ######################MCP Monitored Statement
    call rsmgmt\bin\run.bat -f mgmt/Log "-FDResolve;Update;Finished" -u >nul 2>&1
    GOTO :END
:FAIL_MESSAGE
    rem ######################MCP Monitored Statement
    echo Resolve Update Failed
    rem ######################MCP Monitored Statement
    call rsmgmt\bin\run.bat -f mgmt/Log "-FDResolve;Update;Failed" -u >nul 2>&1

:END
set DIST=
set TMPDIST=
set FILENAME=
set ZIPFILENAME=
set USERNAME=
set PASSWORD=
set ARG=
set LICENSE=
set ANSWER=
set TMPFILENAME=
set CP=
set UPDATE_OPTIONS=
set SUCCESS=
