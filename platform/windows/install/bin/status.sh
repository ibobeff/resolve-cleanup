#!/bin/bash

if [ "$1" == "--help" ]; then
    echo ""
    echo "Usage: <Resolve Component...>"
    echo "This script will check the if the specified Resolve Components are running"
    echo "ALL can be used to check all components (except RSCONSOLE) installed on this system"
    exit 0
fi

# properties
DIST=`echo $0 | awk '{ print substr( $0, 0, length($0)-9 ) }'`/..

COMPONENTS=("$@")
if [ "${COMPONENTS}x" == "x" ]; then
    COMPONENTS=('all')
fi

ARGS=${#COMPONENTS[@]}
for (( i=0;i<$ARGS;i++));
do
    COMPONENT=${COMPONENTS[${i}]}
    if [ $COMPONENT = "tomcat" ]; then
        COMPONENT="rsview"
    elif [ $COMPONENT == "elasticsearch" ]; then
        COMPONENT=rssearch
    elif [ $COMPONENT == "rabbitmq" ]; then
        COMPONENT=rsmq
    fi
    if [ $COMPONENT = "all" ]; then
        for FILE in `ls $DIST/bin`
        do
            case $FILE in
            init.*)
                if [ -f $DIST/bin/$FILE ]; then
                    $DIST/bin/$FILE status
                fi
            ;;
            esac
        done
    else
        INITFILE="${DIST}/bin/init.${COMPONENT}"
        if [ -f $INITFILE ]; then
            $INITFILE status
        else
            echo "Invalid Component: ${COMPONENT}"
        fi
    fi
done
