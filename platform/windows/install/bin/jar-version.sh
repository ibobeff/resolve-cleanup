#!/bin/bash

echo ""

if [ $# -lt 1 ]; then
   echo "Usage: <Jar File>"
   echo ""
   echo "Jar File - Path of Jar file to Check Version"
   echo ""
   echo "This script will check the version information of a Resolve Jar file"
   echo ""
   exit 1
fi

VERSION=`unzip -l $1 | grep META-INF/version`

if [ "$VERSION" = "" ]; then
  echo "No Version Information"
fi

echo $VERSION | awk '{ print substr( $4, 10) }'
