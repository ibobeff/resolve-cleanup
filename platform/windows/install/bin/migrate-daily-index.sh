#!/bin/bash

if [ $# -lt 4 ] || [ "$1" == "--help" ]; then
    echo "Usage: [-ts <ToServerList>] [-tp <ToServerPort>] [-c <ToServerClustername>] [-fs <FromServer>] [-fp <FromServerPort>] -s <StartDate> -e <EndDate> [-sc <ScrollSize>] [--no-config]"
    echo ""
    echo "This script will migrate data from one ES cluster to another.  This is meant for migrating data from older ES"
    echo "instances to a newer ES cluster.  For Server Settings if not specified they will be pulled from the local blueprint"
    echo "For Start and End Dates Using 0 can be used to specify no exact time (i.e 0 and 0 will copy all data)"
    echo "Inputs:"
    echo "    -ts <ToServerList>         Comma separated list of all servers (IP/Host) in the new cluster"
    echo "    -tp <ToServerPort>         Port to connect to new server on, this should be the TCP port (i.e. 9300 or equivalent)"
    echo "    -c  <ToServerClusterName>  Name of the ES Cluster being conected to (e.g. RESOLVE)"
    echo "    -fs <FromServer>           IP/Host of the server to copy data from, only one server in this cluster should be listed"
    echo "    -fp <FromServerPort>       Port to connect to on old server cluster, this should be the HTTP port (i.e. 9200 or equivalent)"
    echo "    -s  <StartDate>            First Day to copy data from, needs to be in YYYYMMDD format or be 0"
    echo "    -e  <EndDate>              Last Day to copy data from, needs to be in YYYYMMDD format or be 0"
    echo "    -sc <ScrollSize>           Amount of records to copy at a time to ES Cluster being connected to, default 50"
    echo "                               Can be lowered if migration is running into buffer size errors"
    echo "    --no-config                Skips Automatic configuration of new ES instance"
    exit 1
fi

DIST=`echo $0 | awk '{ print substr( $0, 0, length($0)-22 ) }'`/..
BLUEPRINT=${DIST}/bin/blueprint.properties

TOSERVERLIST=`grep "RSSEARCH_NODES=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}'`
TOSERVERPORT=`grep "rssearch.yml.transport.tcp.port=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}'`
TOSERVERPORT=$(expr ${TOSERVERPORT} + 400)
CLUSTERNAME=`grep "CLUSTERNAME=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}'`
FROMSERVER=`grep "LOCALHOST=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}'`
FROMSERVERPORT=`grep "rssearch.yml.http.port=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}'`
STARTTIME=-1
ENDTIME=-1
CONFIG=TRUE
PRIMARY=`grep "PRIMARY=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [a-z] [A-Z]`
SCROLLSIZE=50


while [ $# -gt 0 ]
do
    if [ $1 == "-ts" ]; then
        shift
        if [ $# -gt 0 ]; then
            TOSERVERLIST=$1
        fi
    elif [ $1 == "-tp" ]; then
        shift
        if [ $# -gt 0 ]; then
            TOSERVERPORT=$1
        fi
    elif [ $1 == "-c" ]; then
        shift
        if [ $# -gt 0 ]; then
            CLUSTERNAME=$1
        fi
    elif [ $1 == "-fs" ]; then
        shift
        if [ $# -gt 0 ]; then
            FROMSERVER=$1
        fi
    elif [ $1 == "-fp" ]; then
        shift
        if [ $# -gt 0 ]; then
            FROMSERVERPORT=$1
        fi
    elif [ $1 == "-s" ]; then
        shift
        if [ $# -gt 0 ]; then
            STARTTIME=$1
        fi
    elif [ $1 == "-e" ]; then
        shift
        if [ $# -gt 0 ]; then
            ENDTIME=$1
        fi
    elif [ $1 == "-sc" ]; then
        shift
        if [ $# -gt 0 ]; then
            SCROLLSIZE=$1
        fi
    elif [ $1 == "--no-config" ]; then
        CONFIG=FALSE
    fi
    shift
done

if [[ $STARTTIME =~ ^[0-9]{4}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])$ ]]; then
    echo "Migrating Data From: $STARTTIME"
elif [ $STARTTIME = 0 ]; then
    echo "Migrating Data from Earliest Records"
else
    echo "Invalid Start Date Specified: $STARTTIME"
    exit 1
fi

if [[ $ENDTIME =~ ^[0-9]{4}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])$ ]]; then
    echo "Migrating Data To: $ENDTIME"
elif [ $ENDTIME = 0 ]; then
    echo "Migrating Data to Latest Records"
else
    echo "Invalid End Date Specified: $ENDTIME"
    exit 1
fi

cd ${DIST}

DIST=`pwd`

if [ ! -d elasticsearch.new ]; then
    echo "New Elasticsearch Directory Not Found, Must be Started Manually"
else
    #Validate System Limits are Set Correctly
    MAPCOUNT=`sysctl vm.max_map_count | awk 'BEGIN{FS="="}{print $2}'`
    if [ ${MAPCOUNT} -lt 262144 ]; then
        echo "Current vm.max_map_count (${MAPCOUNT}) is too low for the updated Elasticsearch to run successfully."
        echo "Please set it to at least 262144 before continuing with the data migration. This can be done temporarily by running sysctl -w vm.max_map_count=262144 as root, or permanently by configuring vm.max_map_count in /etc/sysctl.conf or by running the script setup_sysctl.sh"
        exit 1
    fi
    OPENFILES=`ulimit -a | grep "open files" | awk '{print $NF}'`
    if [ ${OPENFILES} -lt 2048 ]; then
        echo "Current max file descriptors (${OPENFILES}) is too low for the updated Elasticsearch to run successfully."
        echo "Please set it to at least 2048 (recommended is 131072) before continuing with the data migration. This can be done temporarily by running ulimit -n 131072 as root, or permanently by configuring nofile in /etc/security/limits.conf or by running the script setup_limits.sh"
        exit 1
    fi
    MAXPROCESS=`ulimit -a | grep "max user processes" | awk '{print $NF}'`
    if [ ${MAXPROCESS} -lt 2048 ]; then
        echo "Current max user processes (${MAXPROCESS}) is too low for the updated Elasticsearch to run successfully."
        echo "Please set it to at least 2048 (recommended is 10240) before continuing with the data migration. This can be done temporarily by running ulimit -u 10240 as root, or permanently by configuring nproc in /etc/security/limits.conf or by running the script setup_limits.sh"
        exit 1
    fi
    MAXLOCKEDMEMORY=`ulimit -a | grep "max locked memory" | awk '{print $NF}'`
    if [ "${MAXLOCKEDMEMORY}" != "unlimited" ]; then
        echo "Current max locked memory (${MAXLOCKEDMEMORY}) is too low for the updated Elasticsearch to run successfully."
        echo "Please set it to unlimited before continuing with the data migration. This can be done by configuring memlock and as in /etc/security/limits.conf or by running the script setup_limits.sh"
        exit 1
    fi

    if [ ! -f ${DIST}/elasticsearch.new/bin/lock ]; then
        if [ "${CONFIG}" = "TRUE" ]; then
            echo "Configuring New Elasticsearch Instance"
            rsmgmt/bin/run.sh -f ${DIST}/file/tmp/rssearchnew-run-configure.groovy "-FDnew"
            rsmgmt/bin/run.sh -f ${DIST}/file/tmp/rssearchnew-yml-configure.groovy "-FDnew"
        fi

        chmod 755 elasticsearch.new/bin/*.sh
        chmod 755 elasticsearch.new/bin/init.*
        chmod 755 elasticsearch.new/bin/elasticsearch
        chmod -R 755 jdktmp/bin

        echo "Starting New Elasticsearch Instance"
        ${DIST}/elasticsearch.new/bin/runtmp.sh
    else
        echo "New Elasticsearch Instance Already Starting"
    fi
fi

function Elastic_Search_Status(){
    local elastic_rp=$(curl -silent http://$1:$2/_cluster/health?wait_for_status=green&timeout=40s)
    local status=`echo "$elastic_rp"  | grep -o 'green'`
    echo $status
}

ELASTICSEARCHYML=${DIST}/elasticsearch.new/config/elasticsearch.yml

ES_PORT=`grep "http.port:" $ELASTICSEARCHYML | awk 'BEGIN{FS=":"}{print $NF}' | tr -d '[:space:]'`
ES_URI=`grep "network.host:" $ELASTICSEARCHYML | awk 'BEGIN{FS=":"}{print $NF}' | tr -d '[:space:]'`

echo "Waiting Indefinitely for New ES Cluster to have 'green' status"
esready=$(Elastic_Search_Status  $ES_URI $ES_PORT)
#check the reuslt 
while [ -z "$esready" ]
do 
    printf "."
    sleep 5
    esready=$(Elastic_Search_Status  $ES_URI $ES_PORT)
done
echo ""

ELASTICSEARCHYML=${DIST}/elasticsearch/config/elasticsearch.yml

ES_PORT=`grep "http.port:" $ELASTICSEARCHYML | awk 'BEGIN{FS=":"}{print $NF}' | tr -d '[:space:]'`
ES_URI=`grep "network.host:" $ELASTICSEARCHYML | awk 'BEGIN{FS=":"}{print $NF}' | tr -d '[:space:]'`

echo "Waiting Indefinitely for Old ES Cluster to have 'green' status"
esready=$(Elastic_Search_Status  $ES_URI $ES_PORT)
#check the reuslt 
while [ -z "$esready" ]
do 
    printf "."
    sleep 5
    esready=$(Elastic_Search_Status  $ES_URI $ES_PORT)
done
echo ""


if [ "${PRIMARY}" = "TRUE" ]; then
    echo "Starting Data Migration"
# default classes
    CP="${DIST}/rsmgmt/service/proxy"
    CP="${CP}:${DIST}/rsmgmt/lib/resolve-mgmt.jar"

    CP="${CP}:${DIST}/elasticsearch.new/lib/elasticsearch-5.4.1.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/jna-4.4.0.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/jts-1.13.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/lucene-analyzers-common-6.5.1.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/lucene-core-6.5.1.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/lucene-grouping-6.5.1.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/lucene-highlighter-6.5.1.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/lucene-join-6.5.1.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/lucene-memory-6.5.1.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/lucene-misc-6.5.1.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/lucene-queries-6.5.1.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/lucene-queryparser-6.5.1.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/lucene-sandbox-6.5.1.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/lucene-spatial-6.5.1.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/lucene-suggest-6.5.1.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/spatial4j-0.6.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/HdrHistogram-2.1.9.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/hppc-0.7.1.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/t-digest-3.0.jar"
    CP="${CP}:${DIST}/elasticsearch.new/modules/reindex/commons-logging-1.1.3.jar"
    CP="${CP}:${DIST}/elasticsearch.new/modules/reindex/commons-codec-1.10.jar"
    CP="${CP}:${DIST}/elasticsearch.new/modules/reindex/reindex-5.4.1.jar"
    CP="${CP}:${DIST}/elasticsearch.new/modules/reindex/httpcore-4.4.5.jar"
    CP="${CP}:${DIST}/elasticsearch.new/modules/reindex/httpcore-nio-4.4.5.jar"
    CP="${CP}:${DIST}/elasticsearch.new/modules/reindex/rest-5.4.1.jar"
    CP="${CP}:${DIST}/elasticsearch.new/modules/reindex/httpasyncclient-4.1.2.jar"
    CP="${CP}:${DIST}/elasticsearch.new/modules/reindex/httpclient-4.5.2.jar"
    CP="${CP}:${DIST}/elasticsearch.new/modules/transport-netty3/transport-netty3-5.4.1.jar"
    CP="${CP}:${DIST}/elasticsearch.new/modules/transport-netty3/netty-3.10.6.Final.jar"
    CP="${CP}:${DIST}/elasticsearch.new/modules/transport-netty4/transport-netty4-5.4.1.jar"
    CP="${CP}:${DIST}/elasticsearch.new/modules/transport-netty4/netty-buffer-4.1.11.Final.jar"
    CP="${CP}:${DIST}/elasticsearch.new/modules/transport-netty4/netty-codec-4.1.11.Final.jar"
    CP="${CP}:${DIST}/elasticsearch.new/modules/transport-netty4/netty-codec-http-4.1.11.Final.jar"
    CP="${CP}:${DIST}/elasticsearch.new/modules/transport-netty4/netty-common-4.1.11.Final.jar"
    CP="${CP}:${DIST}/elasticsearch.new/modules/transport-netty4/netty-handler-4.1.11.Final.jar"
    CP="${CP}:${DIST}/elasticsearch.new/modules/transport-netty4/netty-resolver-4.1.11.Final.jar"
    CP="${CP}:${DIST}/elasticsearch.new/modules/transport-netty4/netty-transport-4.1.11.Final.jar"
    CP="${CP}:${DIST}/elasticsearch.new/modules/percolator/percolator-5.4.1.jar"
    CP="${CP}:${DIST}/elasticsearch.new/modules/lang-mustache/lang-mustache-5.4.1.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/jackson-core-2.8.6.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/jackson-dataformat-cbor-2.8.6.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/jackson-dataformat-smile-2.8.6.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/jackson-dataformat-yaml-2.8.6.jar"

    CP="${CP}:${DIST}/lib/commons-io.jar"
    CP="${CP}:${DIST}/lib/commons-lang.jar"
    CP="${CP}:${DIST}/lib/commons-lang3.jar"
    CP="${CP}:${DIST}/lib/commons-collections.jar"
    CP="${CP}:${DIST}/lib/commons-dbcp.jar"
    CP="${CP}:${DIST}/lib/commons-pool.jar"
    CP="${CP}:${DIST}/lib/commons-beanutils.jar"
    CP="${CP}:${DIST}/lib/log4j.jar"
    CP="${CP}:${DIST}/lib/dom4j.jar"
    CP="${CP}:${DIST}/lib/jaxen.jar"
    CP="${CP}:${DIST}/lib/jms.jar"
    CP="${CP}:${DIST}/lib/groovy.jar"
    CP="${CP}:${DIST}/lib/concurrent.jar"
    CP="${CP}:${DIST}/lib/options.jar"
    CP="${CP}:${DIST}/lib/snmp4j.jar"
    CP="${CP}:${DIST}/lib/ezmorph.jar"
    CP="${CP}:${DIST}/lib/json-lib.jar"
    CP="${CP}:${DIST}/lib/zookeeper.jar"
    CP="${CP}:${DIST}/lib/opencsv.jar"
    CP="${CP}:${DIST}/lib/poi.jar"
    CP="${CP}:${DIST}/lib/poi-ooxml.jar"
    CP="${CP}:${DIST}/lib/esapi.jar"
    CP="${CP}:${DIST}/lib/joda-time.jar"
    CP="${CP}:${DIST}/lib/guava.jar"
    CP="${CP}:${DIST}/lib/rabbitmq-client.jar"
    CP="${CP}:${DIST}/lib/antlr-runtime.jar"
    CP="${CP}:${DIST}/lib/apache-log4j-extras.jar"
    CP="${CP}:${DIST}/lib/asm.jar"
    CP="${CP}:${DIST}/lib/asm-commons.jar"
    CP="${CP}:${DIST}/lib/tika.jar"
    CP="${CP}:${DIST}/lib/jsoup.jar"
    CP="${CP}:${DIST}/lib/netty.jar"
    CP="${CP}:${DIST}/lib/transport.jar"
    CP="${CP}:${DIST}/lib/lang-mustache-client.jar"
    CP="${CP}:${DIST}/lib/percolator-client.jar"
    CP="${CP}:${DIST}/lib/transport-netty3-client.jar"
    CP="${CP}:${DIST}/lib/transport-netty4-client.jar"
    CP="${CP}:${DIST}/lib/lucene-backward-codecs.jar"
    CP="${CP}:${DIST}/lib/lucene-spatial3d.jar"
    CP="${CP}:${DIST}/lib/lucene-spatial-extras.jar"
    CP="${CP}:${DIST}/lib/netty-buffer.jar"
    CP="${CP}:${DIST}/lib/netty-codec.jar"
    CP="${CP}:${DIST}/lib/netty-codec-http.jar"
    CP="${CP}:${DIST}/lib/netty-common.jar"
    CP="${CP}:${DIST}/lib/netty-handler.jar"
    CP="${CP}:${DIST}/lib/netty-resolver.jar"
    CP="${CP}:${DIST}/lib/netty-transport.jar"
    CP="${CP}:${DIST}/lib/t-digest.jar"
    CP="${CP}:${DIST}/lib/log4j-api.jar"
    CP="${CP}:${DIST}/lib/log4j-core.jar"
    CP="${CP}:${DIST}/lib/slf4j-api.jar"
    CP="${CP}:${DIST}/lib/ganymed-ssh2.jar"
    CP="${CP}:${DIST}/lib/commons-httpclient.jar"
    CP="${CP}:${DIST}/lib/httpmime.jar"
    CP="${CP}:${DIST}/lib/httpclient-win.jar"
    CP="${CP}:${DIST}/lib/fluent-hc.jar"
    CP="${CP}:${DIST}/lib/jna.jar"
    CP="${CP}:${DIST}/lib/jna-platform.jar"
    CP="${CP}:${DIST}/lib/httpclient-cache.jar"
    CP="${CP}:${DIST}/lib/commons-codec.jar"
    CP="${CP}:${DIST}/lib/commons-net.jar"
    CP="${CP}:${DIST}/lib/mariadb-java-client.jar"
    CP="${CP}:${DIST}/lib/ojdbc6.jar"
    CP="${CP}:${DIST}/lib/xdb6.jar"
    CP="${CP}:${DIST}/lib/mail.jar"
    CP="${CP}:${DIST}/lib/activation.jar"
    CP="${CP}:${DIST}/lib/org.restlet.ext.fileupload.jar"
    CP="${CP}:${DIST}/lib/org.restlet.ext.jetty.jar"
    CP="${CP}:${DIST}/lib/org.restlet.ext.ssl.jar"
    CP="${CP}:${DIST}/lib/org.restlet.jar"
    CP="${CP}:${DIST}/lib/org.jsslutils.jar"
    CP="${CP}:${DIST}/lib/jetty-ajp.jar"
    CP="${CP}:${DIST}/lib/jetty-continuation.jar"
    CP="${CP}:${DIST}/lib/jetty-http.jar"
    CP="${CP}:${DIST}/lib/jetty-io.jar"
    CP="${CP}:${DIST}/lib/jetty-security.jar"
    CP="${CP}:${DIST}/lib/jetty-server.jar"
    CP="${CP}:${DIST}/lib/jetty-servlet.jar"
    CP="${CP}:${DIST}/lib/jetty-servlets.jar"
    CP="${CP}:${DIST}/lib/jetty-util.jar"
    CP="${CP}:${DIST}/lib/jetty-webapp.jar"
    CP="${CP}:${DIST}/lib/org.apache.commons.fileupload.jar"
    CP="${CP}:${DIST}/lib/servlet-api.jar"
    CP="${CP}:${DIST}/lib/jsqlparser.jar"

    CP="${CP}:${DIST}/elasticsearch.new/lib/log4j-1.2-api-2.8.2.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/log4j-api-2.8.2.jar"
    CP="${CP}:${DIST}/elasticsearch.new/lib/log4j-core-2.8.2.jar"

    PATH=${JDK}/bin:${PATH} export PATH

    JAVA_EXEC=${DIST}/jdktmp/bin/java

    "${JAVA_EXEC}" -Dorg.owasp.esapi.resources=${DIST}/rsmgmt/config -Djava.security.egd=file:///dev/urandom -Djava.library.path="${DIST}/lib" -Dorg.apache.cxf.Logger=org.apache.cxf.common.logging.Log4jLogger -cp "${CP}" com.resolve.rsmgmt.Main rsmgmt -f mgmt/PreMigrateDailyIndex.groovy "-FD${TOSERVERLIST};${TOSERVERPORT};${CLUSTERNAME};${FROMSERVER};${FROMSERVERPORT};${STARTTIME};${ENDTIME};${SCROLLSIZE}"
else
    echo "ES Started, Migration will run on instance designated as PRIMARY by the blueprint.properties"
fi
