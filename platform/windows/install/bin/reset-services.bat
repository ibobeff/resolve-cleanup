@echo off

if not "%1" == "" GOTO :START
    echo.
    echo "Usage: <Resolve Component...>"
    echo "This script will uninstall and reinstall the specified Resolve Components"
    echo "ALL can be used to uninstall and reinstall all components installed on this system"
    echo "Components Should be Stopped Before Running This Script"
    GOTO :END

:START

set DIST=%~dp0\..
set CURRDIR=%CD%

:LOOP
if "%1" == "" GOTO :END_LOOP
    if /i "%1" == "ALL" GOTO :ALL
    if /i "%1" == "TOMCAT" GOTO :TOMCAT
    if /i "%1" == "RSVIEW" GOTO :TOMCAT
    if /i "%1" == "RSREMOTE" GOTO :REMOTE
    if /i "%1" == "RSCONTROL" GOTO :CONTROL
    if /i "%1" == "RSMQ" GOTO :RSMQ
    if /i "%1" == "RSMGMT" GOTO :MGMT
    if /i "%1" == "RSSEARCH" GOTO :RSSEARCH
        :TOMCAT
            cd %DIST%/tomcat/bin
            call uninstall-service.bat
            call install-service-rsview.bat
            GOTO :SHIFT
        :REMOTE
            cd %DIST%/rsremote/bin
            call uninstall-service.bat
            call install-service.bat
            GOTO :SHIFT
        :CONTROL
            cd %DIST%/rscontrol/bin
            call uninstall-service.bat
            call install-service.bat
            GOTO :SHIFT
        :RSMQ
            cd %DIST%/rabbitmq/windows/rabbitmq/sbin
            call uninstall-service.bat
            call install-service.bat
            GOTO :SHIFT
        :MGMT
            cd %DIST%/rsmgmt/bin
            call uninstall-service.bat
            call install-service.bat
            GOTO :SHIFT
        :RSSEARCH
            cd %DIST%/elasticsearch/bin
            call uninstall-service.bat
            call install-service.bat
            GOTO :SHIFT
    GOTO :SHIFT
        :ALL
            cd %DIST%/tomcat/bin
            call uninstall-service.bat
            call install-service-rsview.bat

            cd %DIST%/rsremote/bin
            call uninstall-service.bat
            call install-service.bat

            cd %DIST%/rscontrol/bin
            call uninstall-service.bat
            call install-service.bat

            cd %DIST%/rsmgmt/bin
            call uninstall-service.bat
            call install-service.bat

            cd %DIST%/rabbitmq/windows/rabbitmq/sbin
            call uninstall-service.bat
            call install-service.bat

            cd %DIST%/elasticsearch/bin
            call uninstall-service.bat
            call install-service.bat
    :SHIFT
    shift
GOTO :LOOP
:END_LOOP

cd %CURRDIR%
:END
set DIST=
set CURRDIR=
