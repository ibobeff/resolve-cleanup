@echo off

if not "%1" == "" GOTO :START
    echo.
    echo "Usage: <Resolve Component...>"
    echo "This script will restart the specified Resolve Components"
    echo "ALL can be used to restart all components (except RSCONSOLE) installed on this system"
    GOTO :END

:START

set DIST=%~dp0..

echo "Stopping %*"
call "%DIST%\bin\stop.bat" %*
ping 123.45.67.89 -n 1 -w 10000 > nul
set DIST=%~dp0..
echo "Starting %*"
call "%DIST%\bin\run.bat" %*

:END
set DIST=
