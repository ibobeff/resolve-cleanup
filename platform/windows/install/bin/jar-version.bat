@echo off

echo.
if exist %1 goto getversion
   echo Usage: ^<Jar File^>
   echo.
   echo Jar File - Path of Jar file to Check Version
   echo.
   echo This script will check the version information of a Resolve Jar file
   echo.
goto end

:getversion
pushd=%~dp0\..
for /f "tokens=4 delims= " %%a in ('UNZIP.exe -l %1 ^| findstr "META-INF/version"') do @set version=%%a

if "%version%" == "" (
    echo No Version Information
) else (
    echo %version:~9%
)
popd
set version=
:end
