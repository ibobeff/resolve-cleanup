@echo off
setlocal EnableDelayedExpansion

IF NOT "%1" == "" GOTO :START
    :HELP
    echo Usage: -f ^<filename^> [OPTIONS]
    echo.
    echo This script will update Resolve components.
    echo The upgrade zip file should be placed in the install directory.
    echo The Name of the upgrade zip is provided with -f.
    echo.
    echo Options:
    echo     -u ^<username^>  Username for RSConsole (used to Import updated
    echo                   libraries)
    echo     -p ^<password^>  Password for RSConsole (used to Import updated
    echo                      libraries)
    echo     --force          Continue Update Even if Backup of Same ID
    echo                      is detected
    echo     --no-config      Do not run configuration off of blueprint
    echo                      after updating
    echo     --no-start       Do not start all components after update is
    echo                      complete
    echo     --no-stop        Do not stop all components before applying
    echo                      update
    echo     --no-restart     Do not stop all components before applying
    echo                      update and do not start all components after
    echo                      update is complete
    echo     --no-import      Do not import any Updated Modules
    echo     --purge-index    Purge and re-index all components for search
    echo     --reset-services Uninstall and Reinstall all Services
    echo     --skip-sql       Do not apply any SQL updates included in the
    echo                      update.
    echo     --skip-cli       Do not apply any CLI updates included in the
    echo                      update.
    echo      -dns            DNS Host Name to set into rsview properties
    echo                      for system url (3.4 upgrade only)
    echo      -port           Host Port to set into rsview properties for
    echo                      system url (3.4 upgrade only)
    echo.
    GOTO :END

:START

set DIST=%~dp0..
set UPGRADE_OPTIONS=--backup-index
set BLUEPRINT_OPTIONS=
set USERNAME=admin
set PASSWORD=
set FORCE=FALSE
set ANSWER=
set STOP=TRUE
set SUCCESS=FALSE
set LICENSE=

:LOOP
set ARG=%1
IF "%ARG%" == "" GOTO :END_LOOP
    IF NOT "%ARG%" == "-u" GOTO :PASS
        shift
        set ARG=%1
        IF "%ARG%" == "" goto :NO_USERNAME
        IF "%ARG:~0,1%" == "-" goto :NO_USERNAME
            set USERNAME="%ARG%"
            GOTO :SHIFT
        :NO_USERNAME
            echo Must Supply Username With -u
            GOTO :END
    :PASS
    IF NOT "%ARG%" == "-p" GOTO :LICENSE
        shift
        set ARG=%1
        IF "%ARG%" == "" GOTO :NO_PASSWORD
        IF "%ARG:~0,1%" == "-" goto :NO_PASSWORD
            set PASSWORD="%ARG%"
            GOTO :SHIFT
        :NO_PASSWORD
            echo Must Supply Password With -p
            GOTO :END
    :LICENSE
    IF NOT "%ARG%" == "-lic" GOTO :FILE
        shift
        set ARG=%1
        IF "%ARG%" == "" GOTO :NO_LICENSE
        IF "%ARG:~0,1%" == "-" goto :NO_LICENSE
            set LICENSE="%ARG%"
            GOTO :SHIFT
        :NO_LICENSE
            echo Must Supply License With -lic
            GOTO :END
    :FILE
    IF NOT "%ARG%" == "-f" GOTO :DNS_HOST
        shift
        set ARG=%1
        IF "%ARG%" == "" GOTO :HELP
            set FILENAME=%ARG%
            set ZIPFILENAME=%~nx1
            GOTO :SHIFT
    :DNS_HOST
    IF NOT "%ARG%" == "-dns" GOTO :DNS_PORT
        shift
        set ARG=%1
        IF "%ARG%" == "" GOTO :END_LOOP
            IF NOT "%BLUEPRINT_OPTIONS%" == "" GOTO :ADDBLUEPRINTOPTION
                set BLUEPRINT_OPTIONS=-BDDNS_HOST=%ARG%
                GOTO :SHIFT
            :ADDBLUEPRINTOPTION
                set BLUEPRINT_OPTIONS=%BLUEPRINT_OPTIONS%;DNS_HOST=%ARG%
                GOTO :SHIFT
            GOTO :SHIFT
    :DNS_PORT
    IF NOT "%ARG%" == "-port" GOTO :OPTIONS
        shift
        set ARG=%1
        IF "%ARG%" == "" GOTO :END_LOOP
            IF NOT "%BLUEPRINT_OPTIONS%" == "" GOTO :ADDBLUEPRINTOPTION
                set BLUEPRINT_OPTIONS=-BDDNS_PORT=%ARG%
                GOTO :SHIFT
            :ADDBLUEPRINTOPTION
                set BLUEPRINT_OPTIONS=%BLUEPRINT_OPTIONS%;DNS_PORT=%ARG%
                GOTO :SHIFT
            GOTO :SHIFT
    :OPTIONS
    IF "%ARG%" == "--no-stop" (SET STOP=FALSE)
    IF "%ARG%" == "--no-restart" (SET STOP=FALSE)
    IF "%ARG%" == "--force" (SET FORCE=TRUE)
    IF "%ARG%" == "--auto-accept" (SET ANSWER=AUTO)
    IF NOT "%UPGRADE_OPTIONS%" == "" GOTO :ADDOPTION
        set UPGRADE_OPTIONS=%ARG%
        GOTO :SHIFT
    :ADDOPTION
        set UPGRADE_OPTIONS=%UPGRADE_OPTIONS%;%ARG%
        GOTO :SHIFT
    :SHIFT
    shift
    set ARG=%1
GOTO :LOOP
:END_LOOP

IF EXIST "%FILENAME%" GOTO :COMMAND
    echo Upgrade File %FILENAME% Does Not Exist
    GOTO :END

:COMMAND

set TMPFILENAME=%ZIPFILENAME:~0,7%

IF /i %TMPFILENAME% EQU UPGRADE (
    GOTO :LOCKCHECK
)

echo WARNING!!! File %FILENAME% does not appear to be an Upgrade file
IF "%FORCE%" EQU "FALSE" ( 
    echo Please make sure the file specified is an upgrade and has not been renamed
    GOTO :END
)

:LOCKCHECK
set FILEDATE=%ZIPFILENAME:~-12%
set FILEDATE=%FILEDATE:~0,-4%
set LOCKFILE=%DIST%\file\lock-%FILEDATE%

IF EXIST "%LOCKFILE%" GOTO :LICENSE
    echo Lock File not detected.  Please unzip the Upgrade file in the installation directory before starting the upgrade
    GOTO :END

:LICENSE

IF "%FORCE%" EQU "TRUE" GOTO :AGREE_LICENSE
    IF EXIST "*.lic" GOTO :AGREE_LICENSE
        sc query "rsview" > NUL
        IF ERRORLEVEL 1060 GOTO :RSCONTROL
            set RSVIEW_MEM=-1
            FOR /F "delims== tokens=1-2" %%a in ('findstr rsview.run.Xmx blueprint.properties') do (
                set RSVIEW_MEM=%%b
            )
            IF %RSVIEW_MEM% leq 512 GOTO :RSCONTROL
                echo WARNING!!! No License File Found.  The RSView Memory (%RSVIEW_MEM%) is
                echo restricted to 512 without a License.  Please either reduce the RSView
                echo allocated memory or contact support to receive a license
                echo Cancelling Upgrade
                GOTO :END

        :RSCONTROL
        sc query "rscontrol" > NUL
        IF ERRORLEVEL 1060 GOTO :AGREE_LICENSE
            set RSCONTROL_MEM=-1
            FOR /F "delims== tokens=1-2" %%a in ('findstr rscontrol.run.Xmx blueprint.properties') do (
                set RSCONTROL_MEM=%%b
            )
            IF %RSCONTROL_MEM% leq 512 GOTO :AGREE_LICENSE
                    echo WARNING!!! No License File Found.  The RSControl Memory (%RSCONTROL_MEM%) is
                    echo restricted to 512 without a License.  Please either reduce the RSControl
                    echo allocated memory or contact support to receive a license
                    echo Cancelling Upgrade

:AGREE_LICENSE

for /f "tokens=*" %%a in (
"'%DIST%\unzip' -l %FILENAME% 2^>null ^| findstr /C:' file/license/license.txt'"
) do (
set LICENSE="%%a"
) 

IF "%ANSWER%" EQU "AUTO" (
    IF [%PASSWORD%] == [] (
        set PASSWORD=resolve
    )
    GOTO :STOP_COMPONENTS
)

IF NOT [%PASSWORD%] == [] (
    GOTO :LICENSECHECK
)

set /P PASSWORD="Password: "

:LICENSECHECK

IF [%LICENSE%] == [] (
    GOTO :MESSAGE
)
"%DIST%\unzip" -p %FILENAME% file/license/license.txt
set /P ANSWER="Do You Accept The Above? (Y/N) "

:RESPONSE
IF /i %ANSWER% EQU Y (
    GOTO :MESSAGE
)
IF /i %ANSWER% EQU YES (
    GOTO :MESSAGE
)
IF /i %ANSWER% EQU N (
    GOTO :END
)
IF /i %ANSWER% EQU NO (
    GOTO :END
)
set /P ANSWER="Please Enter Y or N "
GOTO :RESPONSE

:MESSAGE
echo.
echo Please backup your resolve servers and database before proceeding
echo with the upgrade. For 5.1 upgrade, all servers must be upgraded
echo simultaneously
echo Ctrl + C can be used to terminate the upgrade process at any time

set /P ANSWER="Do You Wish to Continue? (Y/N) "

:MESSAGE_RESPONSE
IF /i %ANSWER% EQU Y (
    GOTO :STOP_COMPONENTS
)
IF /i %ANSWER% EQU YES (
    GOTO :STOP_COMPONENTS
)
IF /i %ANSWER% EQU N (
    GOTO :END
)
IF /i %ANSWER% EQU NO (
    GOTO :END
)
set /P ANSWER="Please Enter Y or N "
GOTO :MESSAGE_RESPONSE

:STOP_COMPONENTS
rem Stop All Components
IF %STOP%==FALSE GOTO :UPDATE
    rem ######################MCP Monitored Statement
    echo Stopping Resolve Components
    rem ######################MCP Monitored Statement
    call "%DIST%\bin\stop.bat" ALL

:UPDATE
rem echo Dist: %DIST%
REM set DIST1=%DIST%
set TMPDIST=%DIST%
call "%DIST%\rsmgmt\bin\run.bat" -f install/resolve-update-cleanup "-FD%UPGRADE_OPTIONS%" -u

set DIST=%TMPDIST%

copy /V /Y /B "%FILENAME%" "%DIST%"
rem echo \tprevious to unzip
"%DIST%\unzip" -oq "%DIST%\%ZIPFILENAME%" -d "%DIST%"
rem echo \tafter unzip
pushd %DIST%
set ESCDIST=%CD:\=\\%
popd

set CP=%DIST%\file\tmp\resolve-update.jar
FOR %%c in ("%DIST%\file\tmp\*.jar") DO (
    IF %%c==%DIST%\file\tmp\resolve-update.jar GOTO :CP_LOOP
        set CP=!CP!;%DIST%/file/tmp/%%~nc%%~xc
    :CP_LOOP
    rem END CP_LOOP
)

call "%DIST%\jdk\bin\java" -cp "%CP%" -Dorg.owasp.esapi.resources="%DIST%\file\tmp" com.resolve.update.Main -f file\install-update.groovy "-FD%ZIPFILENAME%;%USERNAME%;%PASSWORD%;%UPGRADE_OPTIONS%" -d "%DIST%"

IF ERRORLEVEL 1 GOTO :END
    rem make sure rsmgmt config is set up correctly with INSTALLDIR
    "%DIST%\sed" -e "s|INSTALLDIR|%ESCDIST%|g" "%DIST%\rsmgmt\config\config.xml" > sed.tmp
    move /Y sed.tmp "%DIST%\rsmgmt\config\config.xml"

    call "%DIST%\bin\config.bat" --skip-dashboard
    set DIST=%TMPDIST%

    IF [%LICENSE%] == [] GOTO :RESOLVE_UPDATE
        IF NOT EXIST "%LICENSE%" GOTO :LICENSE_NOT_FOUND
            call "%DIST%\rsmgmt\bin\run.bat" -u admin -p resolve -f install/resolve-install-license "-FD%LICENSE%" %UPGRADE_OPTIONS% -u
            GOTO :RESOLVE_UPDATE
        :LICENSE_NOT_FOUND
            echo WARNING!!! License File %LICENSE% Could Not Be Found

    :RESOLVE_UPDATE
    set DIST=%TMPDIST%
    call "%DIST%\rsmgmt\bin\run.bat" -f install/resolve-update "-FD%ZIPFILENAME%;%USERNAME%;%PASSWORD%;%UPGRADE_OPTIONS%" "%BLUEPRINT_OPTIONS%" -u
    if ERRORLEVEL 1 GOTO :DASHBOARD
        set SUCCESS=TRUE
        del /F /Q %ZIPFILENAME%

    :DASHBOARD
    set USERNAME=
    set PASSWORD=
    set DIST=%TMPDIST%
    sc query "rsview" > NUL
    IF ERRORLEVEL 1060 GOTO :END
        call "%DIST%\bin\dashboard.bat" UPDATE

IF %SUCCESS%==FALSE GOTO :FAIL_MESSAGE
    rem ######################MCP Monitored Statement
    echo Resolve Upgrade Finished
    rem ######################MCP Monitored Statement
    call rsmgmt\bin\run.bat -f mgmt/Log "-FDResolve;Upgrade;Finished" -u >nul 2>&1
    GOTO :END
:FAIL_MESSAGE
    rem ######################MCP Monitored Statement
    echo Resolve Upgrade Failed
    rem ######################MCP Monitored Statement
    call rsmgmt\bin\run.bat -f mgmt/Log "-FDResolve;Upgrade;Failed" -u >nul 2>&1

:END
set DIST=
set TMPDIST=
set ESCDIST=
set FILENAME=
set ZIPFILENAME=
set USERNAME=
set PASSWORD=
set ARG=
set LICENSE=
set ANSWER=
set TMPFILENAME=
set FILEDATE=
set LOCKFILE=
set UPGRADE_OPTIONS=
set BLUEPRINT_OPTIONS=
set STOP=
set CP=
set SUCCESS=
set LICENSE=
