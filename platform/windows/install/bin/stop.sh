#!/bin/bash

if [ $# -lt 1 ]; then
  echo ""
  echo "Usage: <Resolve Component...>"
  echo "This script will stop the specified Resolve Components"
  echo "ALL can be used to stop all components (except RSCONSOLE) installed on this system"
  exit 1
fi

# properties
DIST=`echo $0 | awk '{ print substr( $0, 0, length($0)-7 ) }'`/..

FORCE=false
eval LASTARG=\${$#}

if [ $LASTARG = "-k" ]; then
    FORCE=true
fi

while [ $# -gt 0 ]
do
    COMPONENT=`echo $1 | tr [A-Z] [a-z]`
    if [ $COMPONENT = "all" ]; then
        for FILE in `ls $DIST/bin`
        do
            case $FILE in
            init.*)
                if [ $FILE != "init.rsmq" ] && [ $FILE != "init.rssearch" ] && [ -f $DIST/bin/$FILE ]; then
                    if $FORCE; then
                        $DIST/bin/$FILE stop 10
                    else
                        $DIST/bin/$FILE stop
                    fi
                fi
            ;;
            esac
        done
        if [ -f $DIST/bin/init.rssearch ]; then
            if $FORCE; then
                $DIST/bin/init.rssearch stop 10
            else
                $DIST/bin/init.rssearch stop
            fi
        fi
        if [ -f $DIST/bin/init.rsmq ]; then
            if $FORCE; then
                $DIST/bin/init.rsmq stop 10
            else
                $DIST/bin/init.rsmq stop
            fi
        fi
    elif [ $COMPONENT != "-k" ]; then
        if [ $COMPONENT == "tomcat" ]; then
            COMPONENT=rsview
        elif [ $COMPONENT == "elasticsearch" ]; then
            COMPONENT=rssearch
        elif [ $COMPONENT == "rabbitmq" ]; then
            COMPONENT=rsmq
        fi
        FILE="${DIST}/bin/init.${COMPONENT}"
        if [ -f $FILE ]; then
            if $FORCE; then
                $FILE stop 10
            else
                $FILE stop
            fi
        else
            echo "Invalid Component: ${COMPONENT}"
        fi
    fi
    shift
done
