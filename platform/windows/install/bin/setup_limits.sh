#!/bin/bash

if [ $# -lt 4 ] || [ "$1" == "--help" ]; then
    echo ""
    echo "Usage: <FILE LIMIT> <MEMORY LIMIT> <ADDRESS LIMIT> <PROCESS LIMIT>"
    echo "This setup script will add a File Limit (default 131072), Locked-In Memory Limit (default"
    echo "unlimited), Address Space Limit (default unlimited), and Process Limit (default 10240) for"
    echo "the Resolve installation user on this system (/etc/security/limits.conf).  This script is"
    echo "only for initial setup on Linux systems."
    echo ""
    echo "FILE LIMIT    - File Limit to set on they system for the Resolve user"
    echo "                Can use --default to setup with default file limit (131072)"
    echo "                or --skip-file to skip file limit setup"
    echo "MEMORY LIMIT  - Locked-In Memory Limit to set on they system for the Resolve user"
    echo "                Can use --default to setup with default locked-in memory limit (unlimited)"
    echo "                or --skip-memory to skip locked-in memory limit setup"
    echo "ADDRESS LIMIT - Address Space Limit to set on they system for the Resolve user"
    echo "                Can use --default to setup with default address space limit (unlimited)"
    echo "                or --skip-address to skip the address space limit setup"
    echo "PROCESS LIMIT - Process Limit to set on they system for the Resolve user"
    echo "                Can use --default to setup with default process limit (10240)"
    echo "                or --skip-process to skip the process limit setup"
    echo ""
    echo "Examples:"
    echo "    ./setup_limits.sh  --default --default --default --default"
    echo "                Setup limits.conf with all default values"
    echo "    ./setup_limits.sh  40000 --skip-memory --skip-address --skip-process"
    echo "                Setup 40000 as the file limit (nofile), and do not set up values for"
    echo "                the other three limit settings"
    echo "    ./setup_limits.sh  --skip-file unlimited 10000 5000"
    echo "                Setup unlimited as the Memory (memlock) limit, 10000  as the Address Space (as)"
    echo "                limit and 5000 as the Process (nproc) limit, and skip setting the file limit"
    exit 0
fi

linux=false
sunos=false
case "`uname`" in
Linux*) linux=true;;
SunOS*) sunos=true;;
esac

if $sunos; then
    echo "SunOS server detected, skipping limit setup"
    exit 0
fi

ID=`id | sed -e "s/^uid=[0-9]*(\(.*\)) gid.*/\1/"`

if [ "${ID}x" != "rootx" ]; then
    echo "The Service Setup Must be run as the Root User"
    exit 0
fi

DIST=`echo $0 | awk '{ print substr( $0, 0, length($0)-15 ) }'`/..
BLUEPRINT=$DIST/rsmgmt/config/blueprint.properties

R_USER=`grep "resolve.user=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [A-Z] [a-z]`

if [ "${R_USER}x" == "x" ]; then
    echo "Unable to Determine User ID for Resolve, assuming 'resolve'"
    R_USER=resolve
fi

REGEX="^[0-9]+$"

#File Limit
if [ "$1" == "--default" ]; then
    FILE_LIMIT=131072
elif [ "$1" == "--skip-file" ]; then
    FILE_LIMIT="-1"
elif [[ $1 =~ $REGEX ]]; then
    FILE_LIMIT=$1
else
    echo "Invalid Argument, FILE_LIMIT must be either --default or a number for the file limit"
    exit 0
fi

#Memory Limit
if [ "$2" == "--default" ]; then
    MEMORY_LIMIT=unlimited
elif [ "$2" == "--skip-memory" ]; then
    MEMORY_LIMIT="-1"
elif [[ $2 =~ $REGEX ]]; then
    MEMORY_LIMIT=$2
else
    echo "Invalid Argument, MEMORY_LIMIT must be either --default or a number for the locked-in memory limit"
    exit 0
fi

#Address Limit
if [ "$3" == "--default" ]; then
    ADDRESS_LIMIT=unlimited
elif [ "$3" == "--skip-address" ]; then
    ADDRESS_LIMIT="-1"
elif [[ $3 =~ $REGEX ]]; then
    ADDRESS_LIMIT=$3
else
    echo "Invalid Argument, ADDRESS_LIMIT must be either --default or a number for the locked-in address limit"
    exit 0
fi

#Process Limit
if [ "$4" == "--default" ]; then
    PROCESS_LIMIT=10240
elif [ "$4" == "--skip-process" ]; then
    PROCESS_LIMIT="-1"
elif [ "$4" == "unlimited" ]; then
    PROCESS_LIMIT="unlimited"
elif [[ $4 =~ $REGEX ]]; then
    PROCESS_LIMIT=$4
else
    echo "Invalid Argument, PROCESS_LIMIT must be either --default, a number, or unlimited for the process limit"
    exit 0
fi

echo "Backing Up Existing limits.conf"
if [ -f /etc/security/limits.conf.resolvebackup ]; then
    echo "Backup file /etc/security/limits.conf.resolvebackup already exists"
    printf "Do You Wish to Continue and Overwrite the Backup? (Y/N) "
    ANSWER=

    while [ "${ANSWER}x" == "x" ]
    do
        read inputline
        ANSWER=`echo $inputline | tr [A-Z] [a-z]`
        if [ "${ANSWER}" == "y" ] || [ "${ANSWER}" == "yes" ]; then
            ANSWER="Y"
        elif [ "${ANSWER}" == "n" ] || [ "${ANSWER}" == "no" ]; then
            ANSWER="N"
        else
            ANSWER=
            printf "Please Enter Y or N "
        fi
    done

    if [ "${ANSWER}" == "N" ]; then
        echo "Cancelling Limits Setup"
        exit 1
    fi
fi

cp -p /etc/security/limits.conf /etc/security/limits.conf.resolvebackup

if [ "$FILE_LIMIT" == "unlimited" ] || [ $FILE_LIMIT -gt 0 ]; then
    echo "* soft nofile $FILE_LIMIT" | tee -a /etc/security/limits.conf
    echo "* hard nofile $FILE_LIMIT" | tee -a /etc/security/limits.conf
    echo "$R_USER soft nofile $FILE_LIMIT" | tee -a /etc/security/limits.conf
    echo "$R_USER hard nofile $FILE_LIMIT" | tee -a /etc/security/limits.conf
fi
if [ "$MEMORY_LIMIT" == "unlimited" ] || [ $MEMORY_LIMIT -gt 0 ]; then
    echo "* soft memlock $MEMORY_LIMIT" | tee -a /etc/security/limits.conf
    echo "* hard memlock $MEMORY_LIMIT" | tee -a /etc/security/limits.conf
    echo "$R_USER soft memlock $MEMORY_LIMIT" | tee -a /etc/security/limits.conf
    echo "$R_USER hard memlock $MEMORY_LIMIT" | tee -a /etc/security/limits.conf
fi
if [ "$ADDRESS_LIMIT" == "unlimited" ] || [ $ADDRESS_LIMIT -gt 0 ]; then
    echo "* soft as $ADDRESS_LIMIT" | tee -a /etc/security/limits.conf
    echo "* hard as $ADDRESS_LIMIT" | tee -a /etc/security/limits.conf
    echo "$R_USER soft as $ADDRESS_LIMIT" | tee -a /etc/security/limits.conf
    echo "$R_USER hard as $ADDRESS_LIMIT" | tee -a /etc/security/limits.conf
fi
if [ "$PROCESS_LIMIT" == "unlimited" ] || [ $PROCESS_LIMIT -gt 0 ]; then
    echo "* soft nproc $PROCESS_LIMIT" | tee -a /etc/security/limits.conf
    echo "* hard nproc $PROCESS_LIMIT" | tee -a /etc/security/limits.conf
    echo "$R_USER soft nproc $PROCESS_LIMIT" | tee -a /etc/security/limits.conf
    echo "$R_USER hard nproc $PROCESS_LIMIT" | tee -a /etc/security/limits.conf
fi

echo "Limits configuration has been set up.  Please restart your session for these changes to take effect."
