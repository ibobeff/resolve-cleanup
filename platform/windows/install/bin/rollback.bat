@echo off

if not "%1" == "" GOTO :START
    :HELP
    echo Usage: -i ^<Version^>
    echo.
    echo This script will roll back Resolve components.
    echo The update to roll back will be provided by -i.
    echo To determine which updates are on a system check
    echo the file folder for checksum files like
    echo update-<Version>.md5
    echo.
    GOTO :END

:START

set DIST=%~dp0..
set OPTIONS=
set UPDATE=
set AUTOANSWER=

:LOOP
set ARG=%1
if "%ARG%" == "" GOTO :END_LOOP
    if not "%ARG%" == "-i" GOTO :OPTIONS
        shift
        set ARG=%1
        if "%ARG%" == "" goto :NO_ID
            set UPDATE=%ARG%
            GOTO :SHIFT
        :NO_ID
            echo Must Supply Update ID To Roll Back with -i
            GOTO :END
    :OPTIONS
    IF "%ARG%" == "--auto-accept" (SET AUTOANSWER=AUTO)
    if not "%OPTIONS%" == "" GOTO :ADDOPTION
        set OPTIONS=%ARG%
        GOTO :SHIFT
    :ADDOPTION
        set OPTIONS=%OPTIONS%;%ARG%
        GOTO :SHIFT
    :SHIFT
    shift
    set ARG=%1
GOTO :LOOP
:END_LOOP

if not "%UPDATE%" == "" GOTO :COMMAND
    echo Missing Update ID to Roll Back
    GOTO :END

:COMMAND

IF "%AUTOANSWER%" EQU "AUTO" (
    GOTO :ROLLBACK
)

echo You are about to roll back the changed applied by the %UPDATE% update on this server.
echo This action cannot be undone, are you sure you want to continue?

set /P ANSWER="Execute Rollback? (Y/N) "

:RESPONSE
IF /i %ANSWER% EQU Y (
    GOTO :ROLLBACK
)
IF /i %ANSWER% EQU YES (
    GOTO :ROLLBACK
)
IF /i %ANSWER% EQU N (
    echo Cancelling Rollback
    GOTO :END
)
IF /i %ANSWER% EQU NO (
    echo Cancelling Rollback
    GOTO :END
)
set /P ANSWER="Please Enter Y or N "
GOTO :RESPONSE

:ROLLBACK
SET UPDATE=%UPDATE:.=_%

if EXIST "%DIST%\rsmgmt\lib\resolve-mgmt.jar.%UPDATE%" (
    call "%DIST%\bin\stop.bat" rsmgmt
    move /Y "%DIST%\rsmgmt\lib\resolve-mgmt.jar.%UPDATE%" "%DIST%\rsmgmt\lib\resolve-mgmt.jar"
)

call "%DIST%\rsmgmt\bin\run.bat" -f install/resolve-rollback "-FD%UPDATE%;%OPTIONS%"
:END
set DIST=
set OPTIONS=
set UPDATE=
set ANSWER=
set AUTOANSWER=
set ARG=
