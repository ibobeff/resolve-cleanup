@echo on

set CURRENT_DIR=%~dp0
set DIST=%CURRENT_DIR%/../../dist
set JRE=%DIST%/jdk
set JAVA_LIB_PATH=%DIST%/lib
REM OWASP ESAPI
REM RSMGMT shall be on every server running resolve
set JAVA_OPTS=%JAVA_OPTS% -Dorg.owasp.esapi.resources="%DIST%/rsmgmt/config"

"%JRE%\bin\java" %JAVA_OPTS% -cp "%JAVA_LIB_PATH%/rsexec.jar;%JAVA_LIB_PATH%/json-lib.jar;%JAVA_LIB_PATH%/commons-lang3.jar;%JAVA_LIB_PATH%/log4j.jar;%JAVA_LIB_PATH%/esapi.jar" com.resolve.util.client.RSClientExecute %*
