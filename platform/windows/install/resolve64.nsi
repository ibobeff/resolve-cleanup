;--------------------------------
; Uses NSIS Modern UI

!define RESOLVE3DIR "..\..\.."

; include Modern UI
!include "MUI.nsh"

; Define variable macros
!define TEMP $R0

; The name of the installer
Name "Resolve"

; The file to write
OutFile "resolve64.exe"

; The default installation directory
InstallDir "$PROGRAMFILES64\Resolve Systems"

;Language strings
LangString PRODUCT_NAME_RESOLVE ${LANG_ENGLISH} "Resolve RBA - Platform"
LangString DESC_RESOLVE ${LANG_ENGLISH} "Resolve core components - includes: RSView, RSControl, RSRemote, RSMgmt, RSMQ, RSSearch"
LangString TEXT_IO_TITLE ${LANG_ENGLISH} "Resolve Configuration Information"
LangString TEXT_IO_TITLE_OPTIONAL ${LANG_ENGLISH} "Resolve Configuration Information (Optional)"
LangString TEXT_IO_SUBTITLE ${LANG_ENGLISH} "Enter configuration settings for Resolve"

;--------------------------------
; Modern Interface Settings

!define MUI_ABORTWARNING
!define MUI_ICON "${RESOLVE3DIR}\platform\windows\install\resolve.ico"
!define MUI_UNICON "${RESOLVE3DIR}\platform\windows\install\resolve.ico"
!define MUI_COMPONENTSPAGE_SMALLDESC
!define MUI_WELCOMEFINISHPAGE_BITMAP "welcome.bmp"

;--------------------------------
; Files to be extracted on startup
ReserveFile "${NSISDIR}\Plugins\InstallOptions.dll"
ReserveFile "resolve.ini"
ReserveFile "resolveOptional.ini"

; init variables
var /GLOBAL productname
Var /GLOBAL blueprint
Var /GLOBAL mysql_root
Var /GLOBAL mysql_pass
Var /GLOBAL has_mysql
Var /GLOBAL dns
Var /GLOBAL port
Var /GLOBAL license
Var /GLOBAL error

; StrContains
; This function does a case sensitive searches for an occurrence of a substring in a string. 
; It returns the substring if it is found. 
; Otherwise it returns null(""). 
; Written by kenglish_hi
; Adapted from StrReplace written by dandaman32
 
 
Var STR_HAYSTACK
Var STR_NEEDLE
Var STR_CONTAINS_VAR_1
Var STR_CONTAINS_VAR_2
Var STR_CONTAINS_VAR_3
Var STR_CONTAINS_VAR_4
Var STR_RETURN_VAR
 
Function StrContains
  Exch $STR_NEEDLE
  Exch 1
  Exch $STR_HAYSTACK
  ; Uncomment to debug
  ;MessageBox MB_OK 'STR_NEEDLE = $STR_NEEDLE STR_HAYSTACK = $STR_HAYSTACK '
    StrCpy $STR_RETURN_VAR ""
    StrCpy $STR_CONTAINS_VAR_1 -1
    StrLen $STR_CONTAINS_VAR_2 $STR_NEEDLE
    StrLen $STR_CONTAINS_VAR_4 $STR_HAYSTACK
    loop:
      IntOp $STR_CONTAINS_VAR_1 $STR_CONTAINS_VAR_1 + 1
      StrCpy $STR_CONTAINS_VAR_3 $STR_HAYSTACK $STR_CONTAINS_VAR_2 $STR_CONTAINS_VAR_1
      StrCmp $STR_CONTAINS_VAR_3 $STR_NEEDLE found
      StrCmp $STR_CONTAINS_VAR_1 $STR_CONTAINS_VAR_4 done
      Goto loop
    found:
      StrCpy $STR_RETURN_VAR $STR_NEEDLE
      Goto done
    done:
   Pop $STR_NEEDLE ;Prevent "invalid opcode" errors and keep the
   Exch $STR_RETURN_VAR  
FunctionEnd
 
!macro _StrContainsConstructor OUT NEEDLE HAYSTACK
  Push `${HAYSTACK}`
  Push `${NEEDLE}`
  Call StrContains
  Pop `${OUT}`
!macroend
 
!define StrContains '!insertmacro "_StrContainsConstructor"'

;--------------------------------
; Pages
;--------------------------------


!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "${RESOLVE3DIR}\license\license.txt"
!insertmacro MUI_PAGE_COMPONENTS
Page custom Settings ValidateSettings ": Configuration Settings"
Page custom OptionalSettings ValidateAll ": Configuration Settings (Optional)"
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------
; Languages

!insertmacro MUI_LANGUAGE "English"

;--------------------------------
; Resolve Section
;--------------------------------

Section "Resolve (required)" SECTION_RESOLVE
  ClearErrors
  SectionIn RO
  
  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  StrCpy $productname "$(PRODUCT_NAME_RESOLVE)"

  ; get InstallOptions dialog user input
  ReadINIStr ${TEMP} "$PLUGINSDIR\resolve.ini" "Field 3" "State"
  StrCpy $blueprint ${TEMP}
  DetailPrint "Resolve Blueprint File=$blueprint"

  ReadINIStr ${TEMP} "$PLUGINSDIR\resolve.ini" "Field 6" "State"
  StrCpy $license ${TEMP}
  DetailPrint "Resolve License File=$license"

  ReadINIStr ${TEMP} "$PLUGINSDIR\resolveOptional.ini" "Field 3" "State"
  StrCpy $mysql_root ${TEMP}
  DetailPrint "MySQL Root=$mysql_root"

  ReadINIStr ${TEMP} "$PLUGINSDIR\resolveOptional.ini" "Field 5" "State"
  StrCpy $mysql_pass ${TEMP}
  DetailPrint "MySQL Password = *****"

  ReadINIStr ${TEMP} "$PLUGINSDIR\resolveOptional.ini" "Field 8" "State"
  StrCpy $dns ${TEMP}
  DetailPrint "Resolve URL Host Name=$dns"

  ReadINIStr ${TEMP} "$PLUGINSDIR\resolveOptional.ini" "Field 10" "State"
  StrCpy $port ${TEMP}
  DetailPrint "Resolve URL Port=$port"

  ; Put file there
  File "${RESOLVE3DIR}\platform\windows\install\unzip.exe"
  File "${RESOLVE3DIR}\platform\windows\install\zip.exe"
  File "${RESOLVE3DIR}\platform\windows\install\ln.exe"
  File "${RESOLVE3DIR}\platform\windows\install\sed.exe"
  File "${RESOLVE3DIR}\builds\rs-common.zip"
  File "${RESOLVE3DIR}\builds\rs-thirdparty64.zip"
  File "${RESOLVE3DIR}\builds\rsmq64.zip"
  File "${RESOLVE3DIR}\builds\elasticsearch.zip"
  File "${RESOLVE3DIR}\builds\mysqlconf.zip"
  File "${RESOLVE3DIR}\builds\oracleconf.zip"
  File "${RESOLVE3DIR}\builds\db2conf.zip"
  File "${RESOLVE3DIR}\builds\resolve.zip"
  File "${RESOLVE3DIR}\builds\rsview64.zip"
  File "${RESOLVE3DIR}\builds\rsconsole.zip"
  File "${RESOLVE3DIR}\builds\rscontrol64.zip"
  File "${RESOLVE3DIR}\builds\rsmgmt64.zip"
  File "${RESOLVE3DIR}\builds\rsremote64.zip"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Resolve" "DisplayName" "$productname"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Resolve" "UninstallString" '"$INSTDIR\uninstall_resolve.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Resolve" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Resolve" "NoRepair" 1
  WriteUninstaller "uninstall_resolve.exe"

  ; unzip file
  nsExec::Exec '"$INSTDIR\unzip.exe" -q -o "$INSTDIR\rs-common.zip" -d "$INSTDIR"'
  nsExec::Exec '"$INSTDIR\unzip.exe" -q -o "$INSTDIR\rs-thirdparty64.zip" -d "$INSTDIR"'
  nsExec::Exec '"$INSTDIR\unzip.exe" -q -o "$INSTDIR\rsmq64.zip" -d "$INSTDIR"'
  nsExec::Exec '"$INSTDIR\unzip.exe" -q -o "$INSTDIR\elasticsearch.zip" -d "$INSTDIR"'
  nsExec::Exec '"$INSTDIR\unzip.exe" -q -o "$INSTDIR\mysqlconf.zip" -d "$INSTDIR"'
  nsExec::Exec '"$INSTDIR\unzip.exe" -q -o "$INSTDIR\oracleconf.zip" -d "$INSTDIR"'
  nsExec::Exec '"$INSTDIR\unzip.exe" -q -o "$INSTDIR\db2conf.zip" -d "$INSTDIR"'
  nsExec::Exec '"$INSTDIR\unzip.exe" -q -o "$INSTDIR\resolve.zip" -d "$INSTDIR"'
  nsExec::Exec '"$INSTDIR\unzip.exe" -q -o "$INSTDIR\rsview64.zip" -d "$INSTDIR"'
  nsExec::Exec '"$INSTDIR\unzip.exe" -q -o "$INSTDIR\rscontrol64.zip" -d "$INSTDIR"'
  nsExec::Exec '"$INSTDIR\unzip.exe" -q -o "$INSTDIR\rsmgmt64.zip" -d "$INSTDIR"'
  nsExec::Exec '"$INSTDIR\unzip.exe" -q -o "$INSTDIR\rsremote64.zip" -d "$INSTDIR"'
  nsExec::Exec '"$INSTDIR\unzip.exe" -q -o "$INSTDIR\rsconsole.zip" -d "$INSTDIR"'

  ; copy blueprint.properties
  CopyFiles $blueprint $INSTDIR\rsmgmt\config\blueprint.properties


  ; create softlink to jre
  ExecWait '"$INSTDIR\ln.exe" "$INSTDIR\jdk\jre" "$INSTDIR\jdk"'

  ExecWait 'cscript.exe "$INSTDIR\setup-rsmgmt-win.vbs"'
  sleep 5000

  ; execute rsconsole
  StrCmp $mysql_root "" 0 false_mysql_install_pre
      StrCmp $mysql_pass "" 0 false_mysql_only_pass
          ExecWait '"$INSTDIR\rsmgmt\bin\run.bat" -u admin -p resolve -f install/resolve-install-pre'
          goto end_mysql_install_pre
      false_mysql_only_pass:
          ExecWait '"$INSTDIR\rsmgmt\bin\run.bat" -u admin -p resolve -f install/resolve-install-pre "-BDMYSQL_ROOT_PASSWORD=$mysql_pass"'
          goto end_mysql_install_pre

  false_mysql_install_pre:
      StrCmp $mysql_pass "" 0 false_mysql_pass
          ExecWait '"$INSTDIR\rsmgmt\bin\run.bat" -u admin -p resolve -f install/resolve-install-pre "-BDMYSQL_ROOT=$mysql_root"'
          goto end_mysql_install_pre
      false_mysql_pass:
          ExecWait '"$INSTDIR\rsmgmt\bin\run.bat" -u admin -p resolve -f install/resolve-install-pre "-BDMYSQL_ROOT=$mysql_root;MYSQL_ROOT_PASSWORD=$mysql_pass"'
          goto end_mysql_install_pre

  end_mysql_install_pre:

  sleep 5000
  ExecWait '"$INSTDIR\rsmgmt\bin\run.bat" -u admin -p resolve -f install/resolve-install-setup "-BDDNS_HOST=$dns;DNS_PORT=$port"'
  sleep 5000
  ExecWait '"$INSTDIR\rsmgmt\bin\run.bat" -u admin -p resolve -f install/resolve-configure "-BDINSTALL=TRUE;rsremote.esb.queue.name.2=WINDOWS" -s'
  sleep 5000
  Call readErrorFile
  ${StrContains} $1 "error" $1
  StrCpy $error "Diskspace error"
  StrCmp $1 "error" AbortInstallation resolveInstall
  resolveInstall:
  ; create and run install script setup
  ExecWait '"$INSTDIR\rsmgmt\bin\run.bat" -u admin -p resolve -f install/resolve-install-win'
  sleep 5000
  ExecWait '"$INSTDIR\resolve-setup.bat" "$INSTDIR"'
  sleep 5000

  StrCmp $license "" 0 setup_license
      goto end_setup_license
  setup_license:
      ExecWait '"$INSTDIR\rsmgmt\bin\run.bat" -u admin -p resolve -f install/resolve-install-license "-FD$license"'
      goto end_setup_license

  end_setup_license:

  ExecWait '"$INSTDIR\rsmgmt\bin\run.bat" -u admin -p resolve -f install/resolve-install-db'
  sleep 5000
  Call readErrorFile
  ${StrContains} $1 "error" $1
  StrCpy $error "Database installation error"
  StrCmp $1 "error" AbortInstallation resolvePostInstall
  AbortInstallation:
	MessageBox MB_ICONEXCLAMATION|MB_OK "$error. Installation aborted. Please check the log file for more details. Click Cancel to exit installation."
	ClearErrors
	Abort
  resolvePostInstall:
  ExecWait '"$INSTDIR\rsmgmt\bin\run.bat" -u admin -p resolve -f install/resolve-install-post'
  sleep 5000

  ; delete zip file
  Delete "$INSTDIR\rs-common.zip"
  Delete "$INSTDIR\rs-thirdparty64.zip"
  Delete "$INSTDIR\rsmq64.zip"
  Delete "$INSTDIR\elasticsearch.zip"
  Delete "$INSTDIR\mysqlconf.zip"
  Delete "$INSTDIR\oracleconf.zip"
  Delete "$INSTDIR\db2conf.zip"
  Delete "$INSTDIR\resolve.zip"
  Delete "$INSTDIR\rsview64.zip"
  Delete "$INSTDIR\rscontrol64.zip"
  Delete "$INSTDIR\rsmgmt64.zip"
  Delete "$INSTDIR\rsremote64.zip"
  Delete "$INSTDIR\rsconsole.zip"
  Delete "$INSTDIR\resolve-setup.bat"
  ClearErrors
SectionEnd

;--------------------------------
; Components Descriptions
;--------------------------------

;Assign language strings to sections
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
!insertmacro MUI_DESCRIPTION_TEXT ${SECTION_RESOLVE} $(DESC_RESOLVE)
!insertmacro MUI_FUNCTION_DESCRIPTION_END


;--------------------------------
; Uninstaller
;--------------------------------

Section "Uninstall"
  
  ; stop services
  ExecWait 'sc.exe stop rsview'
  sleep 5000
  ExecWait 'sc.exe stop rscontrol'
  sleep 5000
  ExecWait 'sc.exe stop rsmgmt'
  sleep 5000
  ExecWait 'sc.exe stop rsremote'
  sleep 5000
  ExecWait 'sc.exe stop rssearch'
  sleep 5000
  ExecWait 'sc.exe stop rsmq'
  sleep 5000

  ; delete services
  ExecWait 'sc.exe delete rsview'
  sleep 5000
  ExecWait 'sc.exe delete rscontrol'
  sleep 5000
  ExecWait 'sc.exe delete rsmgmt'
  sleep 5000
  ExecWait 'sc.exe delete rsremote'
  sleep 5000
  ExecWait '$INSTDIR\elasticsearch\bin\uninstall-service.bat'
  sleep 5000
  ExecWait '$INSTDIR\rabbitmq\windows\rabbitmq\sbin\uninstall-service.bat'
  sleep 5000

  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Resolve"

  ; Remove files and uninstaller
  Delete $INSTDIR\.nsi
  Delete $INSTDIR\uninstall_resolve.exe

  ; Remove directories used
  RMDir /r "$INSTDIR"

SectionEnd

Function readErrorEnv
	ClearErrors
	ReadEnvStr $0 INSTALLATION_ERROR
FunctionEnd

Function readErrorFile 
	ClearErrors
	FileOpen $0 $TEMP\error.txt r
	IfErrors done
	FileRead $0 $1
	FileClose $0
	done:
FunctionEnd


;--------------------------------
; Resolve Functions
;--------------------------------

Function .onInit

  ; Extract InstallOptions Plugin files
  InitPluginsDir
  File /oname=$PLUGINSDIR\resolve.ini "resolve.ini"
  File /oname=$PLUGINSDIR\resolveOptional.ini "resolveOptional.ini"

FunctionEnd


Function Settings

  ; Display InstallOptions Dialog

  !insertmacro MUI_HEADER_TEXT "$(TEXT_IO_TITLE)" "$(TEXT_IO_SUBTITLE)"
  !insertmacro MUI_INSTALLOPTIONS_DISPLAY "ioFile.ini"

  Push ${TEMP}
  InstallOptions::dialog "$PLUGINSDIR\resolve.ini"
  Pop ${TEMP}

  Pop ${TEMP}

FunctionEnd

Function OptionalSettings

  ; Display InstallOptions Dialog

  !insertmacro MUI_HEADER_TEXT "$(TEXT_IO_TITLE_OPTIONAL)" "$(TEXT_IO_SUBTITLE)"
  !insertmacro MUI_INSTALLOPTIONS_DISPLAY "ioFile.ini"

  Push ${TEMP}
  InstallOptions::dialog "$PLUGINSDIR\resolveOptional.ini"
  Pop ${TEMP}

  Pop ${TEMP}

FunctionEnd

Function ValidateSettings

  ; Check if blueprint file exists
  ReadINIStr ${TEMP} "$PLUGINSDIR\resolve.ini" "Field 3" "State"
  IfFileExists ${TEMP} true_blueprint false_blueprint
  true_blueprint:
      goto end_check_blueprint
  false_blueprint:
      MessageBox MB_ICONEXCLAMATION|MB_OK "Please ensure that the blueprint.properties file exists."
      Abort
      goto end_check_blueprint
  end_check_blueprint:

  ; Check if license file exists
  ReadINIStr ${TEMP} "$PLUGINSDIR\resolveOptional.ini" "Field 6" "State"
  StrCmp ${TEMP} "" 0 check_license
      goto end_check_license
  check_license:
      IfFileExists ${TEMP} true_license false_license
      true_license:
          goto end_check_license
      false_license:
          MessageBox MB_ICONEXCLAMATION|MB_OK "Please ensure that the license file exists."
          Abort
          goto end_check_license
  end_check_license:

FunctionEnd

Function ValidateAll

FunctionEnd

;--------------------------------
; Third-Party Functions
;--------------------------------

; Function GetParameters
;
; input, none
; output, top of stack (replaces, with e.g. whatever)
; modifies no other variables.
 
Function GetParameters
 
  Push $R0
  Push $R1
  Push $R2
  Push $R3
  
  StrCpy $R2 1
  StrLen $R3 $CMDLINE
  
  ;Check for quote or space
  StrCpy $R0 $CMDLINE $R2
  StrCmp $R0 '"' 0 +3
    StrCpy $R1 '"'
    Goto loop
  StrCpy $R1 " "
  
  loop:
    IntOp $R2 $R2 + 1
    StrCpy $R0 $CMDLINE 1 $R2
    StrCmp $R0 $R1 get
    StrCmp $R2 $R3 get
    Goto loop
  
  get:
    IntOp $R2 $R2 + 1
    StrCpy $R0 $CMDLINE 1 $R2
    StrCmp $R0 " " get
    StrCpy $R0 $CMDLINE "" $R2
  
  Pop $R3
  Pop $R2
  Pop $R1
  Exch $R0
 
FunctionEnd

; Function GetParameterValue
;
; Chris Morgan<cmorgan@alum.wpi.edu> 5/10/2004
; -Updated 4/7/2005 to add support for retrieving a command line switch
;  and additional documentation
;
; Searches the command line input, retrieved using GetParameters, for the
; value of an option given the option name.  If no option is found the
; default value is placed on the top of the stack upon function return.
;
; This function can also be used to detect the existence of just a
; command line switch like /OUTPUT  Pass the default and "OUTPUT"
; on the stack like normal.  An empty return string "" will indicate
; that the switch was found, the default value indicates that
; neither a parameter or switch was found.
;
; Inputs - Top of stack is default if parameter isn't found,
;  second in stack is parameter to search for, ex. "OUTPUT"
; Outputs - Top of the stack contains the value of this parameter
;  So if the command line contained /OUTPUT=somedirectory, "somedirectory"
;  will be on the top of the stack when this function returns
;
; Register usage
;$R0 - default return value if the parameter isn't found
;$R1 - input parameter, for example OUTPUT from the above example
;$R2 - the length of the search, this is the search parameter+2
;      as we have '/OUTPUT='
;$R3 - the command line string
;$R4 - result from StrStr calls
;$R5 - search for ' ' or '"'
 
Function GetParameterValue
  Exch $R0  ; get the top of the stack(default parameter) into R0
  Exch      ; exchange the top of the stack(default) with
            ; the second in the stack(parameter to search for)
  Exch $R1  ; get the top of the stack(search parameter) into $R1
 
  ;Preserve on the stack the registers used in this function
  Push $R2
  Push $R3
  Push $R4
  Push $R5
 
  Strlen $R2 $R1+2    ; store the length of the search string into R2
 
  Call GetParameters  ; get the command line parameters
  Pop $R3             ; store the command line string in R3
 
  # search for quoted search string
  StrCpy $R5 '"'      ; later on we want to search for a open quote
  Push $R3            ; push the 'search in' string onto the stack
  Push '"/$R1='       ; push the 'search for'
  Call StrStr         ; search for the quoted parameter value
  Pop $R4
  StrCpy $R4 $R4 "" 1   ; skip over open quote character, "" means no maxlen
  StrCmp $R4 "" "" next ; if we didn't find an empty string go to next
 
  # search for non-quoted search string
  StrCpy $R5 ' '      ; later on we want to search for a space since we
                      ; didn't start with an open quote '"' we shouldn't
                      ; look for a close quote '"'
  Push $R3            ; push the command line back on the stack for searching
  Push '/$R1='        ; search for the non-quoted search string
  Call StrStr
  Pop $R4
 
  ; $R4 now contains the parameter string starting at the search string,
  ; if it was found
next:
  StrCmp $R4 "" check_for_switch ; if we didn't find anything then look for
                                 ; usage as a command line switch
  # copy the value after /$R1= by using StrCpy with an offset of $R2,
  # the length of '/OUTPUT='
  StrCpy $R0 $R4 "" $R2  ; copy commandline text beyond parameter into $R0
  # search for the next parameter so we can trim this extra text off
  Push $R0
  Push $R5            ; search for either the first space ' ', or the first
                      ; quote '"'
                      ; if we found '"/output' then we want to find the
                      ; ending ", as in '"/output=somevalue"'
                      ; if we found '/output' then we want to find the first
                      ; space after '/output=somevalue'
  Call StrStr         ; search for the next parameter
  Pop $R4
  StrCmp $R4 "" done  ; if 'somevalue' is missing, we are done
  StrLen $R4 $R4      ; get the length of 'somevalue' so we can copy this
                      ; text into our output buffer
  StrCpy $R0 $R0 -$R4 ; using the length of the string beyond the value,
                      ; copy only the value into $R0
  goto done           ; if we are in the parameter retrieval path skip over
                      ; the check for a command line switch
 
; See if the parameter was specified as a command line switch, like '/output'
check_for_switch:
  Push $R3            ; push the command line back on the stack for searching
  Push '/$R1'         ; search for the non-quoted search string
  Call StrStr
  Pop $R4
  StrCmp $R4 "" done  ; if we didn't find anything then use the default
  StrCpy $R0 ""       ; otherwise copy in an empty string since we found the
                      ; parameter, just didn't find a value
 
done:
  Pop $R5
  Pop $R4
  Pop $R3
  Pop $R2
  Pop $R1
  Exch $R0 ; put the value in $R0 at the top of the stack
FunctionEnd

; Function StrStr
 ; input, top of stack = string to search for
 ;        top of stack-1 = string to search in
 ; output, top of stack (replaces with the portion of the string remaining)
 ; modifies no other variables.
 ;
 ; Usage:
 ;   Push "this is a long ass string"
 ;   Push "ass"
 ;   Call StrStr
 ;   Pop $R0
 ;  ($R0 at this point is "ass string")

Function StrStr
  Exch $R1 ; st=haystack,old$R1, $R1=needle
  Exch    ; st=old$R1,haystack
  Exch $R2 ; st=old$R1,old$R2, $R2=haystack
  Push $R3
  Push $R4
  Push $R5
  StrLen $R3 $R1
  StrCpy $R4 0
  ; $R1=needle
  ; $R2=haystack
  ; $R3=len(needle)
  ; $R4=cnt
  ; $R5=tmp
  loop:
    StrCpy $R5 $R2 $R3 $R4
    StrCmp $R5 $R1 done
    StrCmp $R5 "" done
    IntOp $R4 $R4 + 1
    Goto loop
done:
  StrCpy $R1 $R2 "" $R4
  Pop $R5
  Pop $R4
  Pop $R3
  Pop $R2
  Exch $R1
FunctionEnd


