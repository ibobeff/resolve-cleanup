set args = WScript.arguments.Named

set objshell = CreateObject("WScript.Shell")

' installdir
installdir = objshell.CurrentDirectory
if args.exists("dir") then
    installdir = args.item("dir")
end if

' ipaddr
ipaddr = "localhost"
if args.exists("ipaddr") then
    ipaddr = args.item("ipaddr")
end if

noservice = "false"

' noservice
if args.exists("noservice") then
    noservice = args.item("noservice")
end if

' install windows service
if noservice <> "" then

    ' go to bin directory
    objshell.CurrentDirectory = "rsremote/bin"

    WScript.echo "Installing Windows service"
    objshell.run "install-service.bat", 0, TRUE

    ' start service
'        set wmi = GetObject("winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
'        set services = wmi.execQuery("SELECT * FROM Win32_Service WHERE name='rsremote'")
'        for each service in services
'            err = service.Change ( , , , , , True, , "")  
'            if err = 0 then
'                service.startService()
'                WScript.echo "Started service " & service.displayName
'            else
'                WScript.echo "FAIL: install service or set desktop interaction mode"
'            end if
'        next
end if
