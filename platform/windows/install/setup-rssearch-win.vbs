' initialize defaults
set args = WScript.arguments.Named

set objshell = CreateObject("WScript.Shell")

' usage help
if args.exists("help") then
    WScript.echo "*** RSSEARCH SETUP ***"
    WScript.echo ""
    WScript.echo "  Usage: /option:<value> ..."
    WScript.echo ""
    WScript.echo "OPTIONAL PARAMETERS:"
    WScript.echo ""
    WScript.echo "    /dir:<installdir>        - Installation directory path (absolute)"
    WScript.echo "    /noservice               - Do not install rsagent windows service"
    WScript.echo ""
    WScript.quit
end if

' installdir
installdir = objshell.CurrentDirectory
if args.exists("dir") then
    installdir = args.item("dir")
end if

' noservice
noservice = "false"
if args.exists("noservice") then
    noservice = args.item("noservice")
end if


' install windows service
if noservice <> "" then
    ' go to rsagent/bin directory
    objshell.CurrentDirectory = "elasticsearch/bin"

    WScript.echo "Installing RSSEARCH Windows service"
    objshell.run "install-service.bat", 0, TRUE

    ' start service
'    set wmi = GetObject("winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
'    set services = wmi.execQuery("SELECT * FROM Win32_Service WHERE name='rsmq'")
'    for each service in services
'        service.startService()
'        WScript.echo "Started service " & service.displayName
'    next
end if


