
' initialize defaults
set args = WScript.arguments.Named

' usage help
if args.exists("help") then
    WScript.echo "*** RSCONTROL SETUP ***"
    WScript.echo ""
    WScript.echo "  Usage: /option:<value> ..."
    WScript.echo ""
    WScript.echo "OPTIONAL PARAMETERS:"
    WScript.echo ""
    WScript.echo "    /dir:<installdir>        - Installation directory path (absolute)"
    WScript.echo "    /noservice               - Do not install windows service"
    WScript.echo ""
    WScript.quit
end if 

set objshell = CreateObject("WScript.Shell")

' installdir
installdir = objshell.CurrentDirectory
if args.exists("dir") then
    installdir = args.item("dir")
end if

noservice = "false"

' noservice
if args.exists("noservice") then
    noservice = args.item("noservice")
end if


' install windows service
if noservice <> "" then
    ' go to bin directory
    objshell.CurrentDirectory = "rscontrol/bin"

    WScript.echo "Installing Windows service"
    objshell.run "install-service.bat", 0, TRUE

    ' start service
'    set wmi = GetObject("winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
'    set services = wmi.execQuery("SELECT * FROM Win32_Service WHERE name='rscontrol'")
'    for each service in services
'        service.startService()
'        WScript.echo "Started service " & service.displayName
'    next
end if
