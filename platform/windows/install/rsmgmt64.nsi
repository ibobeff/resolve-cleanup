;--------------------------------
; Uses NSIS Modern UI

!define RESOLVE3DIR "..\..\.."

; include Modern UI
!include "MUI.nsh"

; Define Constants
LangString PRODUCT_NAME ${LANG_ENGLISH} "Resolve Systems - RSMgmt"
LangString TEXT_IO_TITLE ${LANG_ENGLISH} "Resolve Configuration Information"
LangString TEXT_IO_SUBTITLE ${LANG_ENGLISH} "Enter configuration settings to connect with Resolve"

; Define variable macros
!define TEMP $R0

; The name of the installer
Name "RSMgmt"

; The file to write
OutFile "rsmgmt64.exe"

; The default installation directory
InstallDir "$PROGRAMFILES64\Resolve Systems"


;--------------------------------
; Modern Interface Settings

!define MUI_ABORTWARNING
!define MUI_ICON "${RESOLVE3DIR}\platform\windows\install\resolve.ico"
!define MUI_UNICON "${RESOLVE3DIR}\platform\windows\install\resolve.ico"


;--------------------------------

; Files to be extracted on startup
ReserveFile "${NSISDIR}\Plugins\InstallOptions.dll"
ReserveFile "rsmgmt.ini"

; define variables
Var /GLOBAL help
Var /GLOBAL ipaddr
Var /GLOBAL username
Var /GLOBAL password
Var /GLOBAL common_installed

;--------------------------------
; Pages
;--------------------------------

!insertmacro MUI_PAGE_DIRECTORY
Page custom Settings ValidateSettings ": Configuration Settings"
!insertmacro MUI_PAGE_INSTFILES

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------
; Languages

!insertmacro MUI_LANGUAGE "English"

;--------------------------------
; Installer
;--------------------------------

Section "Install"

  SectionIn RO

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR

  ; set product name
  var /GLOBAL productname
  StrCpy $productname "$(PRODUCT_NAME)"

  ; get InstallOptions dialog user input
  IfSilent IfSilentEnd
    ReadINIStr ${TEMP} "$PLUGINSDIR\rsmgmt.ini" "Field 3" "State"
    StrCpy $ipaddr ${TEMP}
    DetailPrint "Resolve IP Address=$ipaddr"

    ReadINIStr ${TEMP} "$PLUGINSDIR\rsmgmt.ini" "Field 5" "State"
    StrCpy $username ${TEMP}
    DetailPrint "Resolve ESB Username=$username"

    ReadINIStr ${TEMP} "$PLUGINSDIR\rsmgmt.ini" "Field 7" "State"
    StrCpy $password ${TEMP}
    DetailPrint "Resolve ESB Password=$password"
  IfSilentEnd:
  
  ; check if ipaddr defined
  StrCmp $ipaddr "" 0 EndCheckIPAddr
      MessageBox MB_ICONEXCLAMATION|MB_OK "Missing IP Address of Resolve"
      Quit
  EndCheckIPAddr:

  ; Check whether other Resolve components already installed
  IfFileExists $INSTDIR\jre7 true_check_installed false_check_installed
  true_check_installed:
      StrCpy $common_installed "TRUE"
      goto end_check_installed
  false_check_installed:
      StrCpy $common_installed "FALSE"
      goto end_check_installed
  end_check_installed:


  ; Install RSMgmt files
  File "${RESOLVE3DIR}\platform\windows\install\unzip.exe"
  File "${RESOLVE3DIR}\builds\rsmgmt64.zip"

  ; Install common files
  StrCmp $common_installed "FALSE" 0 end_install_common
      File "${RESOLVE3DIR}\builds\rs-common.zip"
      File "${RESOLVE3DIR}\builds\rs-thirdparty64.zip"
  end_install_common:
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\RSMgmt" "DisplayName" "$productname"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\RSMgmt" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\RSMgmt" "NoRepair" 1
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\RSMgmt" "UninstallString" '"$INSTDIR\uninstall_rsmgmt.exe"'
  WriteUninstaller "uninstall_rsmgmt.exe"

  ; unzip rsmgmt
  nsExec::Exec '"$INSTDIR\unzip.exe" -q -o "$INSTDIR\rsmgmt64.zip" -d "$INSTDIR"'

  ; unzip common files
  StrCmp $common_installed "FALSE" 0 end_unzip_common
      nsExec::Exec '"$INSTDIR\unzip.exe" -q -o "$INSTDIR\rs-common.zip" -d "$INSTDIR"'
      nsExec::Exec '"$INSTDIR\unzip.exe" -q -o "$INSTDIR\rs-thirdparty64.zip" -d "$INSTDIR"'
  end_unzip_common:

  ; setup configuration
  ExecWait 'cscript.exe "$INSTDIR\setup-rsmgmt-win.vbs" /ipaddr:$ipaddr /esbuser:$username /esbpass:$password /standalone:true'

  ; delete zip file
  Delete "$INSTDIR\unzip.exe"
  Delete "$INSTDIR\rsmgmt64.zip"
  StrCmp $common_installed "FALSE" 0 end_delete_common_zip
      Delete "$INSTDIR\rs-common.zip"
      Delete "$INSTDIR\rs-thirdparty64.zip"
  end_delete_common_zip:
  
SectionEnd


;--------------------------------
; Uninstaller
;--------------------------------

Section "Uninstall"
  
  ; stop service
  ExecWait 'sc stop rsmgmt'

  ; wait for rsmgmt to stop
  sleep 15000

  ; delete service
  ExecWait '"$INSTDIR\rsmgmt\bin\rsmgmt.exe" -uninstall rsmgmt'

  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\RSMgmt"

  ; Remove uninstaller
  Delete $INSTDIR\.nsi
  Delete $INSTDIR\uninstall_rsmgmt.exe

  ; Check whether other Resolve components installed
  IfFileExists $INSTDIR\rsview end_dir_remove dir_remove
  IfFileExists $INSTDIR\rsconsole end_dir_remove dir_remove
  dir_remove:
      RMDir /r "$INSTDIR"
  end_dir_remove:

SectionEnd


;--------------------------------
; Resolve Functions
;--------------------------------

Function .onInit

  ; Display Usage
  Call CheckUsage

  ; Get command line options
  Call GetParameters

  ; IPADDR option
  Push "IPADDR"
  Push ""
  Call GetParameterValue
  Pop $2
  StrCpy $ipaddr $2

  ; USERNAME option
  Push "USER"
  Push "admin"
  Call GetParameterValue
  Pop $2
  StrCpy $username $2

  ; PASSWORD option
  Push "PASS"
  Push "resolve"
  Call GetParameterValue
  Pop $2
  StrCpy $password $2

  ; Display Usage if silent and missing $ipaddr
  IfSilent 0 EndCheckIPAddr
  StrCmp $ipaddr "" 0 EndCheckIPAddr
    Call Usage
  EndCheckIPAddr:

  ; Extract InstallOptions Plugin files
  InitPluginsDir
  File /oname=$PLUGINSDIR\rsmgmt.ini "rsmgmt.ini"

FunctionEnd


Function CheckUsage

  Push $CMDLINE
  Push "/?"
  Call StrStr
  Pop $help
  StrCmp $help "" UsageEnd1
    Call Usage
  UsageEnd1:

  Push $CMDLINE
  Push "/h"
  Call StrStr
  Pop $help
  StrCmp $help "" UsageEnd2
    Call Usage
  UsageEnd2:

  Push $CMDLINE
  Push "-h"
  Call StrStr
  Pop $help
  StrCmp $help "" UsageEnd3
    Call Usage
  UsageEnd3:

  Push $CMDLINE
  Push "/help"
  Call StrStr
  Pop $help
  StrCmp $help "" UsageEnd4
    Call Usage
  UsageEnd4:

FunctionEnd


Function Usage

  MessageBox MB_ICONEXCLAMATION|MB_OK "Silent Install Usage: rsmgmt.exe /S /IPADDR=resolve-ipaddr [/USER=esb-username /PASS=esb-password]"
  Quit

FunctionEnd

Function Settings

  ; Display InstallOptions Dialog

  !insertmacro MUI_HEADER_TEXT "$(TEXT_IO_TITLE)" "$(TEXT_IO_SUBTITLE)"
  !insertmacro MUI_INSTALLOPTIONS_DISPLAY "ioFile.ini"

  Push ${TEMP}
  InstallOptions::dialog "$PLUGINSDIR\rsmgmt.ini"
  Pop ${TEMP}

  Pop ${TEMP}

FunctionEnd

Function ValidateSettings

  ReadINIStr ${TEMP} "$PLUGINSDIR\rsmgmt.ini" "Field 3" "State"
  StrCmp ${TEMP} "" 0 StrCmpEnd
      MessageBox MB_ICONEXCLAMATION|MB_OK "Please enter the IP Address of Resolve."
      Abort
  StrCmpEnd:

FunctionEnd


;--------------------------------
; Third-Party Functions
;--------------------------------

; Function GetParameters
;
; input, none
; output, top of stack (replaces, with e.g. whatever)
; modifies no other variables.
 
Function GetParameters
 
  Push $R0
  Push $R1
  Push $R2
  Push $R3
  
  StrCpy $R2 1
  StrLen $R3 $CMDLINE
  
  ;Check for quote or space
  StrCpy $R0 $CMDLINE $R2
  StrCmp $R0 '"' 0 +3
    StrCpy $R1 '"'
    Goto loop
  StrCpy $R1 " "
  
  loop:
    IntOp $R2 $R2 + 1
    StrCpy $R0 $CMDLINE 1 $R2
    StrCmp $R0 $R1 get
    StrCmp $R2 $R3 get
    Goto loop
  
  get:
    IntOp $R2 $R2 + 1
    StrCpy $R0 $CMDLINE 1 $R2
    StrCmp $R0 " " get
    StrCpy $R0 $CMDLINE "" $R2
  
  Pop $R3
  Pop $R2
  Pop $R1
  Exch $R0
 
FunctionEnd

; Function GetParameterValue
;
; Chris Morgan<cmorgan@alum.wpi.edu> 5/10/2004
; -Updated 4/7/2005 to add support for retrieving a command line switch
;  and additional documentation
;
; Searches the command line input, retrieved using GetParameters, for the
; value of an option given the option name.  If no option is found the
; default value is placed on the top of the stack upon function return.
;
; This function can also be used to detect the existence of just a
; command line switch like /OUTPUT  Pass the default and "OUTPUT"
; on the stack like normal.  An empty return string "" will indicate
; that the switch was found, the default value indicates that
; neither a parameter or switch was found.
;
; Inputs - Top of stack is default if parameter isn't found,
;  second in stack is parameter to search for, ex. "OUTPUT"
; Outputs - Top of the stack contains the value of this parameter
;  So if the command line contained /OUTPUT=somedirectory, "somedirectory"
;  will be on the top of the stack when this function returns
;
; Register usage
;$R0 - default return value if the parameter isn't found
;$R1 - input parameter, for example OUTPUT from the above example
;$R2 - the length of the search, this is the search parameter+2
;      as we have '/OUTPUT='
;$R3 - the command line string
;$R4 - result from StrStr calls
;$R5 - search for ' ' or '"'
 
Function GetParameterValue
  Exch $R0  ; get the top of the stack(default parameter) into R0
  Exch      ; exchange the top of the stack(default) with
            ; the second in the stack(parameter to search for)
  Exch $R1  ; get the top of the stack(search parameter) into $R1
 
  ;Preserve on the stack the registers used in this function
  Push $R2
  Push $R3
  Push $R4
  Push $R5
 
  Strlen $R2 $R1+2    ; store the length of the search string into R2
 
  Call GetParameters  ; get the command line parameters
  Pop $R3             ; store the command line string in R3
 
  # search for quoted search string
  StrCpy $R5 '"'      ; later on we want to search for a open quote
  Push $R3            ; push the 'search in' string onto the stack
  Push '"/$R1='       ; push the 'search for'
  Call StrStr         ; search for the quoted parameter value
  Pop $R4
  StrCpy $R4 $R4 "" 1   ; skip over open quote character, "" means no maxlen
  StrCmp $R4 "" "" next ; if we didn't find an empty string go to next
 
  # search for non-quoted search string
  StrCpy $R5 ' '      ; later on we want to search for a space since we
                      ; didn't start with an open quote '"' we shouldn't
                      ; look for a close quote '"'
  Push $R3            ; push the command line back on the stack for searching
  Push '/$R1='        ; search for the non-quoted search string
  Call StrStr
  Pop $R4
 
  ; $R4 now contains the parameter string starting at the search string,
  ; if it was found
next:
  StrCmp $R4 "" check_for_switch ; if we didn't find anything then look for
                                 ; usage as a command line switch
  # copy the value after /$R1= by using StrCpy with an offset of $R2,
  # the length of '/OUTPUT='
  StrCpy $R0 $R4 "" $R2  ; copy commandline text beyond parameter into $R0
  # search for the next parameter so we can trim this extra text off
  Push $R0
  Push $R5            ; search for either the first space ' ', or the first
                      ; quote '"'
                      ; if we found '"/output' then we want to find the
                      ; ending ", as in '"/output=somevalue"'
                      ; if we found '/output' then we want to find the first
                      ; space after '/output=somevalue'
  Call StrStr         ; search for the next parameter
  Pop $R4
  StrCmp $R4 "" done  ; if 'somevalue' is missing, we are done
  StrLen $R4 $R4      ; get the length of 'somevalue' so we can copy this
                      ; text into our output buffer
  StrCpy $R0 $R0 -$R4 ; using the length of the string beyond the value,
                      ; copy only the value into $R0
  goto done           ; if we are in the parameter retrieval path skip over
                      ; the check for a command line switch
 
; See if the parameter was specified as a command line switch, like '/output'
check_for_switch:
  Push $R3            ; push the command line back on the stack for searching
  Push '/$R1'         ; search for the non-quoted search string
  Call StrStr
  Pop $R4
  StrCmp $R4 "" done  ; if we didn't find anything then use the default
  StrCpy $R0 ""       ; otherwise copy in an empty string since we found the
                      ; parameter, just didn't find a value
 
done:
  Pop $R5
  Pop $R4
  Pop $R3
  Pop $R2
  Pop $R1
  Exch $R0 ; put the value in $R0 at the top of the stack
FunctionEnd

; Function StrStr
 ; input, top of stack = string to search for
 ;        top of stack-1 = string to search in
 ; output, top of stack (replaces with the portion of the string remaining)
 ; modifies no other variables.
 ;
 ; Usage:
 ;   Push "this is a long ass string"
 ;   Push "ass"
 ;   Call StrStr
 ;   Pop $R0
 ;  ($R0 at this point is "ass string")

Function StrStr
  Exch $R1 ; st=haystack,old$R1, $R1=needle
  Exch    ; st=old$R1,haystack
  Exch $R2 ; st=old$R1,old$R2, $R2=haystack
  Push $R3
  Push $R4
  Push $R5
  StrLen $R3 $R1
  StrCpy $R4 0
  ; $R1=needle
  ; $R2=haystack
  ; $R3=len(needle)
  ; $R4=cnt
  ; $R5=tmp
  loop:
    StrCpy $R5 $R2 $R3 $R4
    StrCmp $R5 $R1 done
    StrCmp $R5 "" done
    IntOp $R4 $R4 + 1
    Goto loop
done:
  StrCpy $R1 $R2 "" $R4
  Pop $R5
  Pop $R4
  Pop $R3
  Pop $R2
  Exch $R1
FunctionEnd


