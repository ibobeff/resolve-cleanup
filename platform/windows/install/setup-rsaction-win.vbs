set args = WScript.arguments.Named

set objshell = CreateObject("WScript.Shell")

' installdir
installdir = objshell.CurrentDirectory
if args.exists("dir") then
    installdir = args.item("dir")
end if

' ipaddr
ipaddr = "localhost"
if args.exists("ipaddr") then
    ipaddr = args.item("ipaddr")
end if

noservice = "false"
esbaddr = ipaddr & ":4004"
esbuser = "admin"
esbpass = "resolve"
svnaddr = ipaddr & ":12438"

' esbaddr
if args.exists("esbaddr") then
    esbaddr = args.item("esbaddr")
end if

' esbuser
if args.exists("esbuser") then
    esbuser = args.item("esbuser")
end if

' esbpass
if args.exists("esbpass") then
    esbpass = args.item("esbpass")
end if

' svnaddr
if args.exists("svnaddr") then
    esbpass = args.item("svnaddr")
end if

' noservice
if args.exists("noservice") then
    noservice = args.item("noservice")
end if

' print config file
WScript.echo "Generating RSACTION config.xml file"
CR = VBCR & VBLF
config = "<?xml version=""1.0"" encoding=""UTF-8""?>" & CR _
    & "<CONFIGURATION> " & CR _
    & "    <GENERAL HOME=""" & installdir & """ />" & CR _ 
    & "    <ESB PRODUCT=""SWIFTMQ"" BROKERADDR="""& esbaddr &""" USERNAME="""& esbuser &""" PASSWORD="""& esbpass &""" DOMAIN=""domain1"" BROKERNAME=""router1"" MGMTADDR="""" />" & CR _
    & "    <SVN HOST=""svn://" & svnaddr & """ />" & CR _  
    & "</CONFIGURATION>"
'WScript.echo config

const OVERWRITE = 2
set fso = CreateObject("Scripting.FileSystemObject")
set file = fso.CreateTextFile("rsaction/config/config.xml")
file.write config
file.close

' create properties.txt files
set fso = CreateObject("Scripting.FileSystemObject")
set file = fso.CreateTextFile("rsaction/config/properties.txt")
file.close

' install windows service
if noservice <> "" then

    ' go to bin directory
    objshell.CurrentDirectory = "rsaction/bin"

    WScript.echo "Installing Windows service"
    objshell.run "install-service.bat", 0, TRUE

    ' start service
'        set wmi = GetObject("winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
'        set services = wmi.execQuery("SELECT * FROM Win32_Service WHERE name='rsaction'")
'        for each service in services
'            err = service.Change ( , , , , , True, , "")  
'            if err = 0 then
'                service.startService()
'                WScript.echo "Started service " & service.displayName
'            else
'                WScript.echo "FAIL: install service or set desktop interaction mode"
'            end if
    next
end if

