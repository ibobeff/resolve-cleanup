#!/usr/bin/perl
# This is a simple CGI script for Webtop that will launch a web application
# using values passed from Webtop, such as event fields and values
# (i.e., Node, Summary, etc.)
#
# CHANGE HISTORY

#
# HTML VARIABLES
#
# Affects the look & feel of the initial banner / form that is displayed
# Make sure that values are valid HTML (i.e, "blue", "#ccccff", etc.)
#
$Background_color = "#c2ccc8";
$Text_color = "black";

#############################
# Should not need to set anything below here ...
#############################

#
# Main
#
select((select(STDOUT), $| = 1)[0]);      # Force non-buffered I/O

($Prog = $0) =~ s%.*/%%;
my $error = "";
my $junk;

#
# Get the input variables that MAY have been posted to us
#
$buffer = $ENV{'QUERY_STRING'};

#
# We should now have the raw posted data
# Decode, un-webify and assign values into a hash
#
if ($buffer) {
  $buffer =~ s/\s$//;                           # kill trailing spaces
  @pairs = split(/&/, $buffer);
  foreach $pair (@pairs) {
    ($field, $value) = split(/=/, $pair);
    # Un-Webify plus signs and %-encoding
    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
    $field =~ tr/+/ /;
    $field =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
    # strip out prepended '$selected_rows.' that webtop adds
    $field =~ s/\$selected_rows\.//g;
    $Value{$field} = $value;
  }
} else {
  #
  # no data passed to us-- send an error message
  #
  &croak ("The web server did NOT post any data");
}




#
# Build a URL using passed values if possible
#
$alertid =  $Value{'ServerName'}.":".$Value{'ServerSerial'};

$Location = "http://RESOLVESERVER:8080/resolve/jsp/rsclient.jsp?lookup=".$Value{'EventId'}."&ALERTID=%27".$alertid."%27";
#
# Display an HTML page to the browser, redirecting them to new URL
#
print STDOUT "Content-type: text/html\n\n";
print STDOUT "<html>\n";
print STDOUT "<head>\n";
print STDOUT "<meta http-equiv=\"Refresh\" \n";
print STDOUT "content=\"0;url=$Location\">\n";
print STDOUT "<title>Preparing Pager List...</title>\n";
print STDOUT "</head>\n";
print STDOUT "<body style=\"background-color: $Background_color; ";
print STDOUT "color: $Text_color\">\n";

#########################
# Subroutine:  croak
# Description: Generate an error and exit
# Parameters:  (input)  message
#              (output) None
#
sub croak {
  my($msg) = @_;
  if ($LogErrors) {
    open (LOGFILE, ">>$LogFile");
    print LOGFILE "${Date}: Error: $msg\n";
    close LOGFILE;
    chmod 0777, $LogFile;
  }
  print STDOUT "<html>\n";
  print STDOUT "<head>\n";
  print STDOUT "<title>Error</title>\n";
  print STDOUT "</head>\n";
  print STDOUT "<body style=\"background-color: $Background_color; ";
  print STDOUT "color: $Text_color\">\n";
  print STDOUT "<center>\n";
  print STDOUT "<h2>Error</h2>\n";
  print STDOUT "<br><br>\n";
  print STDOUT "$msg\n";
  print STDOUT "<br>\n";
  print STDOUT "</center>\n";
  print STDOUT "</body></html>\n";
  exit 1;
}
