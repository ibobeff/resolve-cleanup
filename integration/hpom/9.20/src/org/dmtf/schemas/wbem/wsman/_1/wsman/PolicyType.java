
package org.dmtf.schemas.wbem.wsman._1.wsman;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PolicyType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PolicyType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CancelSubscription"/>
 *     &lt;enumeration value="Skip"/>
 *     &lt;enumeration value="Notify"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PolicyType")
@XmlEnum
public enum PolicyType {

    @XmlEnumValue("CancelSubscription")
    CANCEL_SUBSCRIPTION("CancelSubscription"),
    @XmlEnumValue("Skip")
    SKIP("Skip"),
    @XmlEnumValue("Notify")
    NOTIFY("Notify");
    private final String value;

    PolicyType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PolicyType fromValue(String v) {
        for (PolicyType c: PolicyType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
