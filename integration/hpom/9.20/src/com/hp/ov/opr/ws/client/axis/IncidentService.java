/***************************************************************
 *  Copyright 2008 Hewlett-Packard Development Company, L.P.
 ***************************************************************/
/**
 * IncidentService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

package com.hp.ov.opr.ws.client.axis;

/*
 *  Interface that contains all operations supported by the OM Incident WebService
 */
public interface IncidentService {
  /**
   * DisownMany operation
   * 
   * @param incidentIDs List of incident IDs to be disowned
   * @param resourceURI the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param operationTimeout the operation timeout
   */
  public void DisownMany(
      com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.IncidentIDs incidentIDs,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout)
  throws java.rmi.RemoteException
  ;

  /**
   * StopAction operation
   * 
   * @param actionType the type of action that will be stopped (automatic or operator-initiated)
   * @param resourceURI1 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet the selector set to identify the incident
   * @param operationTimeout2 the operation timeout
   */
  public void StopAction(
      com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.ActionType actionType,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI1,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout2)
  throws java.rmi.RemoteException
  ;

  /**
   * PullOp operation
   * 
   * @param pull Pull soap body block
   * @param resourceURI4 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param operationTimeout5 the operation timeout
   */
  public org.xmlsoap.schemas.ws._2004._09.enumeration.PullResponse PullOp(
      org.xmlsoap.schemas.ws._2004._09.enumeration.Pull pull,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI4,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout5)
  throws java.rmi.RemoteException
  ;

  /**
   * EnumerateOp operation
   * 
   * @param enumerate Enumerate soap body block
   * @param resourceURI6 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet7 the selector set to identify the incident
   * @param operationTimeout8 the operation timeout
   */
  public org.xmlsoap.schemas.ws._2004._09.enumeration.EnumerateResponse EnumerateOp(
      org.xmlsoap.schemas.ws._2004._09.enumeration.Enumerate enumerate,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI6,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet7,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout8)
  throws java.rmi.RemoteException
  ;

  /**
   * Create operation
   * 
   * @param incident Incident to be created
   * @param resourceURI9 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param operationTimeout10 the operation timeout
   */
  public org.xmlsoap.schemas.ws._2004._08.addressing.EndpointReferenceType Create(
      com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident incident,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI9,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout10)
  throws java.rmi.RemoteException
  ;

  /**
   * SetCustomAttribute operation
   * 
   * @param customAttribute CustomAttribute that should be set (key and text)
   * @param resourceURI11 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet12 the selector set to identify the incident
   * @param operationTimeout13 the operation timeout
   */
  public void SetCustomAttribute(
      com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute customAttribute,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI11,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet12,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout13)
  throws java.rmi.RemoteException
  ;

  /**
   * GetAnnotations operation
   * 
   * @param resourceURI16 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet17 the selector set to identify the incident
   * @param operationTimeout18 the operation timeout
   */
  public com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.Annotations GetAnnotations(
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI16,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet17,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout18)
  throws java.rmi.RemoteException
  ;

  /**
   * RenewOp operation
   * 
   * @param renew Renew soap body block
   * @param identifier Subscription identifier
   * @param resourceURI19 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param operationTimeout20 the operation timeout
   */
  public org.xmlsoap.schemas.ws._2004._08.eventing.RenewResponse RenewOp(
      org.xmlsoap.schemas.ws._2004._08.eventing.Renew renew,
      java.lang.String identifier,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI19,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout20)
  throws java.rmi.RemoteException
  ;

  /**
   * UnsubscribeOp operation
   * 
   * @param unsubscribe Unsubscribe soap body block
   * @param identifier21 Subscription identifier
   * @param resourceURI22 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param operationTimeout23 the operation timeout
   */
  public void UnsubscribeOp(
      org.xmlsoap.schemas.ws._2004._08.eventing.Unsubscribe unsubscribe,
      java.lang.String identifier21,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI22,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout23)
  throws java.rmi.RemoteException
  ;

  /**
   * DeleteCustomAttribute operation
   * 
   * @param customAttributeKey Key of CustomAttribute that should be deleted
   * @param resourceURI25 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet26 the selector set to identify the incident
   * @param operationTimeout27 the operation timeout
   */
  public void DeleteCustomAttribute(
      java.lang.String customAttributeKey,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI25,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet26,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout27)
  throws java.rmi.RemoteException
  ;

  /**
   * SubscribeOp operation
   * 
   * @param subscribe Subscribe soap body block
   * @param resourceURI29 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param operationTimeout30 the operation timeout
   */
  public org.xmlsoap.schemas.ws._2004._08.eventing.SubscribeResponse SubscribeOp(
      org.xmlsoap.schemas.ws._2004._08.eventing.Subscribe subscribe,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI29,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout30)
  throws java.rmi.RemoteException
  ;

  /**
   * Put operation
   * 
   * @param incident31 Incident to be updated (new values)
   * @param resourceURI32 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet33 the selector set to identify the incident
   * @param operationTimeout34 the operation timeout
   */
  public com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident Put(
      com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident incident31,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI32,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet33,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout34)
  throws java.rmi.RemoteException
  ;

  /**
   * ReleaseOp operation
   * 
   * @param release Release soap body block
   * @param resourceURI36 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param operationTimeout37 the operation timeout
   */
  public void ReleaseOp(
      org.xmlsoap.schemas.ws._2004._09.enumeration.Release release,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI36,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout37)
  throws java.rmi.RemoteException
  ;

  /**
   * Close operation
   * 
   * @param resourceURI40 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet41 the selector set to identify the incident
   * @param operationTimeout42 the operation timeout
   */
  public void Close(
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI40,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet41,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout42)
  throws java.rmi.RemoteException
  ;

  /**
   * GetInstructionText operation
   * 
   * @param resourceURI45 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet46 the selector set to identify the incident
   * @param operationTimeout47 the operation timeout
   */
  public java.lang.String GetInstructionText(
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI45,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet46,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout47)
  throws java.rmi.RemoteException
  ;
  
  /**
   * OwnMany operation
   * 
   * @param incidentIDs48 List of incident IDs to be owned
   * @param resourceURI49 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param operationTimeout50 the operation timeout
   */
  public void OwnMany(
      com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.IncidentIDs incidentIDs48,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI49,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout50)
  throws java.rmi.RemoteException
  ;

  /**
   * Get operation
   * 
   * @param resourceURI53 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet54 the selector set to identify the incident
   * @param operationTimeout55 the operation timeout
   */
  public com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident Get(
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI53,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet54,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout55)
  throws java.rmi.RemoteException
  ;

  /**
   * StartAction operation
   * 
   * @param actionType57 the type of action that will be started (automatic or operator-initiated)
   * @param resourceURI58 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet59 the selector set to identify the incident
   * @param operationTimeout60 the operation timeout
   */
  public void StartAction(
      com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.ActionType actionType57,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI58,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet59,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout60)
  throws java.rmi.RemoteException
  ;

  /**
   * UpdateAnnotation operation
   * 
   * @param updateAnnotation Text and ID of annotation that should be updated (in UpdateAnnotation structure)
   * @param resourceURI62 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet63 the selector set to identify the incident
   * @param operationTimeout64 the operation timeout
   */
  public void UpdateAnnotation(
      com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.UpdateAnnotation updateAnnotation,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI62,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet63,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout64)
  throws java.rmi.RemoteException
  ;

  /**
   * AddAnnotation operation
   * 
   * @param annotationText Text of new annotation
   * @param resourceURI66 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet67 the selector set to identify the incident
   * @param operationTimeout68 the operation timeout
   */
  public java.lang.String AddAnnotation(
      java.lang.String annotationText,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI66,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet67,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout68)
  throws java.rmi.RemoteException
  ;

  /**
   * DeleteAnnotation operation
   * 
   * @param annotationId69 ID of annotation that should be deleted
   * @param resourceURI70 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet71 the selector set to identify the incident
   * @param operationTimeout72 the operation timeout
   */
  public void DeleteAnnotation(
      java.lang.String annotationId69,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI70,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet71,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout72)
  throws java.rmi.RemoteException
  ;

  /**
   * ReopenMany operation
   * 
   * @param incidentIDs74 List of incident IDs to be reopened
   * @param resourceURI75 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param operationTimeout76 the operation timeout
   */
  public void ReopenMany(
      com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.IncidentIDs incidentIDs74,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI75,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout76)
  throws java.rmi.RemoteException
  ;

  /**
   * Delete operation
   * 
   * @param resourceURI79 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet80 the selector set to identify the incident
   * @param operationTimeout81 the operation timeout
   */
  public void Delete(
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI79,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet80,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout81)
  throws java.rmi.RemoteException
  ;

  /**
   * Reopen operation
   * 
   * @param resourceURI84 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet85 the selector set to identify the incident
   * @param operationTimeout86 the operation timeout
   */
  public void Reopen(
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI84,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet85,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout86)
  throws java.rmi.RemoteException
  ;

  /**
   * CloseMany operation
   * 
   * @param incidentIDs88 List of incident IDs to be closed
   * @param resourceURI89 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param operationTimeout90 the operation timeout
   */
  public void CloseMany(
      com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.IncidentIDs incidentIDs88,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI89,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout90)
  throws java.rmi.RemoteException
  ;
}
