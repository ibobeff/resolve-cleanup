/***************************************************************
 *  Copyright 2008 Hewlett-Packard Development Company, L.P.
 ***************************************************************/
/**
 * IncidentServiceStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 *
 *
 * Manually Modified so the client actually works
 *
 * Client stub class that contains all operations supported by the OM Incident WebService
 *
 * Modified parts are marked by a leading comment
 * "//MODIFIED"
 *
*/
package com.hp.ov.opr.ws.client.axis;

import com.resolve.util.Log;

/*
 *  IncidentServiceStub java implementation
 */

public class IncidentServiceStub extends org.apache.axis2.client.Stub
implements IncidentService{
  protected org.apache.axis2.description.AxisOperation[] _operations;

  //hashmaps to keep the fault mapping
  private java.util.HashMap faultExceptionNameMap = new java.util.HashMap();
  private java.util.HashMap faultExceptionClassNameMap = new java.util.HashMap();
  private java.util.HashMap faultMessageMap = new java.util.HashMap();

  private static int counter = 0;

  private static synchronized String getUniqueSuffix(){
    // reset the counter if it is greater than 99999
    if (counter > 99999){
      counter = 0;
    }
    counter = counter + 1;
    return Long.toString(System.currentTimeMillis()) + "_" + counter;
  }


  private void populateAxisService() throws org.apache.axis2.AxisFault {

    //creating the Service with a unique name
    _service = new org.apache.axis2.description.AxisService("IncidentService" + getUniqueSuffix());
    addAnonymousOperations();

    //creating the operations

    org.apache.axis2.description.AxisOperation __operation;
    _operations = new org.apache.axis2.description.AxisOperation[26];

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "DisownMany"));
    _service.addOperation(__operation);
    _operations[0]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "StopAction"));
    _service.addOperation(__operation);
    _operations[1]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "PullOp"));
    _service.addOperation(__operation);
    _operations[2]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "SubscriptionEndOp"));
    _service.addOperation(__operation);
    _operations[3]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "EnumerateOp"));
    _service.addOperation(__operation);
    _operations[4]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Create"));
    _service.addOperation(__operation);
    _operations[5]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "SetCustomAttribute"));
    _service.addOperation(__operation);
    _operations[6]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "GetAnnotations"));
    _service.addOperation(__operation);
    _operations[7]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "RenewOp"));
    _service.addOperation(__operation);
    _operations[8]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "UnsubscribeOp"));
    _service.addOperation(__operation);
    _operations[9]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "DeleteCustomAttribute"));
    _service.addOperation(__operation);
    _operations[10]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "SubscribeOp"));
    _service.addOperation(__operation);
    _operations[11]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Put"));
    _service.addOperation(__operation);
    _operations[12]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "ReleaseOp"));
    _service.addOperation(__operation);
    _operations[13]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Close"));
    _service.addOperation(__operation);
    _operations[14]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "GetInstructionText"));
    _service.addOperation(__operation);
    _operations[15]=__operation;
    
    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "OwnMany"));
    _service.addOperation(__operation);
    _operations[16]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Get"));
    _service.addOperation(__operation);
    _operations[17]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "StartAction"));
    _service.addOperation(__operation);
    _operations[18]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "UpdateAnnotation"));
    _service.addOperation(__operation);
    _operations[19]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "AddAnnotation"));
    _service.addOperation(__operation);
    _operations[20]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "DeleteAnnotation"));
    _service.addOperation(__operation);
    _operations[21]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "ReopenMany"));
    _service.addOperation(__operation);
    _operations[22]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Delete"));
    _service.addOperation(__operation);
    _operations[23]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Reopen"));
    _service.addOperation(__operation);
    _operations[24]=__operation;

    __operation = new org.apache.axis2.description.OutInAxisOperation();
    __operation.setName(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "CloseMany"));
    _service.addOperation(__operation);
    _operations[25]=__operation;
  }

  //populates the faults
  private void populateFaults(){
  }

  /**
   *Constructor that takes in a configContext
   */

  public IncidentServiceStub(org.apache.axis2.context.ConfigurationContext configurationContext,
      java.lang.String targetEndpoint)
  throws org.apache.axis2.AxisFault {
    this(configurationContext,targetEndpoint,false);
  }

  /**
   * Constructor that takes in a configContext and uses a separate listener
   */
  public IncidentServiceStub(org.apache.axis2.context.ConfigurationContext configurationContext,
      java.lang.String targetEndpoint, boolean useSeparateListener)
  throws org.apache.axis2.AxisFault {
    //To populate AxisService
    populateAxisService();
    populateFaults();

    _serviceClient = new org.apache.axis2.client.ServiceClient(configurationContext,_service);
    configurationContext = _serviceClient.getServiceContext().getConfigurationContext();
    _serviceClient.getOptions().setTo(new org.apache.axis2.addressing.EndpointReference(
        targetEndpoint));
    _serviceClient.getOptions().setUseSeparateListener(useSeparateListener);

    //Set the soap version
    _serviceClient.getOptions().setSoapVersionURI(org.apache.axiom.soap.SOAP12Constants.SOAP_ENVELOPE_NAMESPACE_URI);

    // MODIFIED - add WS-Addressing support
    _serviceClient.getOptions().setProperty(org.apache.axis2.addressing.AddressingConstants.ADD_MUST_UNDERSTAND_TO_ADDRESSING_HEADERS, Boolean.TRUE);
    _serviceClient.getOptions().setProperty(org.apache.axis2.addressing.AddressingConstants.WS_ADDRESSING_VERSION, org.apache.axis2.addressing.AddressingConstants.Submission.WSA_NAMESPACE);
    _serviceClient.engageModule(org.apache.axis2.Constants.MODULE_ADDRESSING);

    // MODIFIED - add SSL support (use EasySSLProtocolSocketFactory to accept all certificates)
    org.apache.commons.httpclient.protocol.Protocol.registerProtocol("https",
        new org.apache.commons.httpclient.protocol.Protocol("https",
                new org.apache.commons.httpclient.contrib.ssl.EasySSLProtocolSocketFactory(), 443));
  }

  /**
   * Default Constructor
   */
  public IncidentServiceStub(org.apache.axis2.context.ConfigurationContext configurationContext)
  throws org.apache.axis2.AxisFault {
    this(configurationContext,"$$SERVICE_URL/" );
  }

  /**
   * Default Constructor
   */
  public IncidentServiceStub()
  throws org.apache.axis2.AxisFault {
    this("$$SERVICE_URL/" );
  }

  /**
   * Constructor taking the target endpoint
   */
  public IncidentServiceStub(java.lang.String targetEndpoint)
  throws org.apache.axis2.AxisFault {
    this(null,targetEndpoint);
  }

  // MODIFIED - username and password used for WS requests
  private String m_user;
  private String m_password;

  public java.util.HashMap<Class, javax.xml.bind.JAXBContext> getContextMap() {
    return classContextMap;
  }

  // MODIFIED - added constructor that accepts username and password used for WS requests
  /**
   * Constructor taking the target endpoint and user/password (recommended to use)
   */
  public IncidentServiceStub(java.lang.String targetEndpoint, String user, String password)
  throws org.apache.axis2.AxisFault {
      this(null, targetEndpoint);
      m_user = user;
      m_password = password;
  }


  /**
   * DisownMany method
   *
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#DisownMany
   * @param incidentIDs92 List of incident IDs to be disowned
   * @param resourceURI93 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param operationTimeout94 the operation timeout
   */
  public  void DisownMany(
      com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.IncidentIDs incidentIDs92,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI93,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout94)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
      _operationClient.getOptions().setAction("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident/DisownMany");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          incidentIDs92,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "DisownMany")));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI93!=null){
        org.apache.axiom.om.OMElement omElementresourceURI89 = toOM(resourceURI93, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "DisownMany")));
        addHeader(omElementresourceURI89,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout94!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout90 = toOM(operationTimeout94, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "DisownMany")));
        addHeader(omElementoperationTimeout90,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);
      return;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * StopAction method
   *
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#StopAction
   * @param actionType96 the type of action that will be stopped (automatic or operator-initiated)
   * @param resourceURI97 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet98 the selector set to identify the incident
   * @param operationTimeout99 the operation timeout
   */
  public  void StopAction(
      com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.ActionType actionType96,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI97,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet98,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout99)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[1].getName());
      _operationClient.getOptions().setAction("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident/StopAction");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          actionType96,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "StopAction")));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI97!=null){
        org.apache.axiom.om.OMElement omElementresourceURI93 = toOM(resourceURI97, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "StopAction")));
        addHeader(omElementresourceURI93,env);
      }

      // add the children only if the parameter is not null
      if (selectorSet98!=null){
        org.apache.axiom.om.OMElement omElementselectorSet94 = toOM(selectorSet98, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "StopAction")));
        addHeader(omElementselectorSet94,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout99!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout95 = toOM(operationTimeout99, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "StopAction")));
        addHeader(omElementoperationTimeout95,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);
      return;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * PullOp method
   *
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#PullOp
   * @param pull101 Pull soap body block
   * @param resourceURI102 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param operationTimeout103 the operation timeout
   */
  public  org.xmlsoap.schemas.ws._2004._09.enumeration.PullResponse PullOp(
      org.xmlsoap.schemas.ws._2004._09.enumeration.Pull pull101,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI102,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout103)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[2].getName());
      _operationClient.getOptions().setAction("http://schemas.xmlsoap.org/ws/2004/09/enumeration/Pull");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          pull101,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "PullOp")));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI102!=null){
        org.apache.axiom.om.OMElement omElementresourceURI98 = toOM(resourceURI102, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "PullOp")));
        addHeader(omElementresourceURI98,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout103!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout99 = toOM(operationTimeout103, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "PullOp")));
        addHeader(omElementoperationTimeout99,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);
      org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
          org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
      org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();

      java.lang.Object object = fromOM(
          _returnEnv.getBody().getFirstElement() ,
          org.xmlsoap.schemas.ws._2004._09.enumeration.PullResponse.class,
          getEnvelopeNamespaces(_returnEnv));

      return (org.xmlsoap.schemas.ws._2004._09.enumeration.PullResponse)object;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * EnumerateOp method
   *
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#EnumerateOp
   * @param enumerate106 Enumerate soap body block
   * @param resourceURI107 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet108 the selector set to identify the incident
   * @param operationTimeout109 the operation timeout
   */

  public  org.xmlsoap.schemas.ws._2004._09.enumeration.EnumerateResponse EnumerateOp(
      org.xmlsoap.schemas.ws._2004._09.enumeration.Enumerate enumerate106,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI107,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet108,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout109)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[4].getName());
      _operationClient.getOptions().setAction("http://schemas.xmlsoap.org/ws/2004/09/enumeration/Enumerate");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          enumerate106,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "EnumerateOp")));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI107!=null){
        org.apache.axiom.om.OMElement omElementresourceURI103 = toOM(resourceURI107, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "EnumerateOp")));
        addHeader(omElementresourceURI103,env);
      }

      // add the children only if the parameter is not null
      if (selectorSet108!=null){
        org.apache.axiom.om.OMElement omElementselectorSet104 = toOM(selectorSet108, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "EnumerateOp")));
        addHeader(omElementselectorSet104,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout109!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout105 = toOM(operationTimeout109, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "EnumerateOp")));
        addHeader(omElementoperationTimeout105,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);

      org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
          org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
      org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();

      java.lang.Object object = fromOM(
          _returnEnv.getBody().getFirstElement() ,
          org.xmlsoap.schemas.ws._2004._09.enumeration.EnumerateResponse.class,
          getEnvelopeNamespaces(_returnEnv));

      return (org.xmlsoap.schemas.ws._2004._09.enumeration.EnumerateResponse)object;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * Create method
   *
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#Create
   * @param incident111 Incident to be created
   * @param resourceURI112 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param operationTimeout113 the operation timeout
   */
  public  org.xmlsoap.schemas.ws._2004._08.addressing.EndpointReferenceType Create(
      com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident incident111,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI112,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout113)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[5].getName());
      _operationClient.getOptions().setAction("http://schemas.xmlsoap.org/ws/2004/09/transfer/Create");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          incident111,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "Create")));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI112!=null){
        org.apache.axiom.om.OMElement omElementresourceURI108 = toOM(resourceURI112, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Create")));
        addHeader(omElementresourceURI108,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout113!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout109 = toOM(operationTimeout113, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Create")));
        addHeader(omElementoperationTimeout109,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);            
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);

      org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
          org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
      org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();

      java.lang.Object object = fromOM(
          _returnEnv.getBody().getFirstElement() ,
          org.xmlsoap.schemas.ws._2004._08.addressing.EndpointReferenceType.class,
          getEnvelopeNamespaces(_returnEnv));

      return (org.xmlsoap.schemas.ws._2004._08.addressing.EndpointReferenceType)object;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * SetCustomAttribute method
   * 
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#SetCustomAttribute
   * @param customAttribute115 CustomAttribute that should be set (key and text)
   * @param resourceURI116 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet117 the selector set to identify the incident
   * @param operationTimeout118 the operation timeout
   */

  public  void SetCustomAttribute(
      com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute customAttribute115,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI116,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet117,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout118)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[6].getName());
      _operationClient.getOptions().setAction("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident/SetCustomAttribute");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          customAttribute115,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "SetCustomAttribute")));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI116!=null){
        org.apache.axiom.om.OMElement omElementresourceURI112 = toOM(resourceURI116, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "SetCustomAttribute")));
        addHeader(omElementresourceURI112,env);
      }

      // add the children only if the parameter is not null
      if (selectorSet117!=null){
        org.apache.axiom.om.OMElement omElementselectorSet113 = toOM(selectorSet117, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "SetCustomAttribute")));
        addHeader(omElementselectorSet113,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout118!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout114 = toOM(operationTimeout118, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "SetCustomAttribute")));
        addHeader(omElementoperationTimeout114,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);
      return;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * GetAnnotations method
   *
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#GetAnnotations
   * @param resourceURI121 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet122 the selector set to identify the incident
   * @param operationTimeout123 the operation timeout
   */
  public  com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.Annotations GetAnnotations(
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI121,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet122,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout123)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[7].getName());
      _operationClient.getOptions().setAction("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident/GetAnnotations");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI121!=null){
        org.apache.axiom.om.OMElement omElementresourceURI117 = toOM(resourceURI121, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "GetAnnotations")));
        addHeader(omElementresourceURI117,env);
      }

      // add the children only if the parameter is not null
      if (selectorSet122!=null){
        org.apache.axiom.om.OMElement omElementselectorSet118 = toOM(selectorSet122, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "GetAnnotations")));
        addHeader(omElementselectorSet118,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout123!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout119 = toOM(operationTimeout123, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "GetAnnotations")));
        addHeader(omElementoperationTimeout119,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);

      org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
          org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
      org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();

      java.lang.Object object = fromOM(
          _returnEnv.getBody().getFirstElement() ,
          com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.Annotations.class,
          getEnvelopeNamespaces(_returnEnv));

      return (com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.Annotations)object;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * RenewOp method
   * 
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#RenewOp
   * @param renew125 Renew soap body block
   * @param identifier126 Subscription identifier
   * @param resourceURI127 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param operationTimeout128 the operation timeout
   */
  public  org.xmlsoap.schemas.ws._2004._08.eventing.RenewResponse RenewOp(
      org.xmlsoap.schemas.ws._2004._08.eventing.Renew renew125,
      java.lang.String identifier126,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI127,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout128)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[8].getName());
      _operationClient.getOptions().setAction("http://schemas.xmlsoap.org/ws/2004/08/eventing/Renew");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          renew125,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "RenewOp")));
      env.build();

      // add the children only if the parameter is not null
      if (identifier126!=null){
        org.apache.axiom.om.OMElement omElementidentifier122 = toOM(identifier126, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "RenewOp")));
        addHeader(omElementidentifier122,env);
      }

      // add the children only if the parameter is not null
      if (resourceURI127!=null){
        org.apache.axiom.om.OMElement omElementresourceURI123 = toOM(resourceURI127, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "RenewOp")));
        addHeader(omElementresourceURI123,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout128!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout124 = toOM(operationTimeout128, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "RenewOp")));
        addHeader(omElementoperationTimeout124,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);

      org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
          org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
      org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();

      java.lang.Object object = fromOM(
          _returnEnv.getBody().getFirstElement() ,
          org.xmlsoap.schemas.ws._2004._08.eventing.RenewResponse.class,
          getEnvelopeNamespaces(_returnEnv));

      return (org.xmlsoap.schemas.ws._2004._08.eventing.RenewResponse)object;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * UnsubscribeOp method
   *
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#UnsubscribeOp
   * @param unsubscribe130 Unsubscribe soap body block
   * @param identifier131 Subscription identifier
   * @param resourceURI132 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param operationTimeout133 the operation timeout
   */
  public  void UnsubscribeOp(
      org.xmlsoap.schemas.ws._2004._08.eventing.Unsubscribe unsubscribe130,
      java.lang.String identifier131,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI132,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout133)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[9].getName());
      _operationClient.getOptions().setAction("http://schemas.xmlsoap.org/ws/2004/08/eventing/Unsubscribe");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          unsubscribe130,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "UnsubscribeOp")));
      env.build();

      // add the children only if the parameter is not null
      if (identifier131!=null){
        org.apache.axiom.om.OMElement omElementidentifier127 = toOM(identifier131, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "UnsubscribeOp")));
        addHeader(omElementidentifier127,env);
      }

      // add the children only if the parameter is not null
      if (resourceURI132!=null){
        org.apache.axiom.om.OMElement omElementresourceURI128 = toOM(resourceURI132, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "UnsubscribeOp")));
        addHeader(omElementresourceURI128,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout133!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout129 = toOM(operationTimeout133, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "UnsubscribeOp")));
        addHeader(omElementoperationTimeout129,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);            
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);
      return;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * DeleteCustomAttribute method signature
   *
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#DeleteCustomAttribute
   * @param customAttributeKey135 Key of CustomAttribute that should be deleted
   * @param resourceURI136 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet137 the selector set to identify the incident
   * @param operationTimeout138 the operation timeout
   */
  public  void DeleteCustomAttribute(
      java.lang.String customAttributeKey135,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI136,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet137,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout138)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[10].getName());
      _operationClient.getOptions().setAction("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident/DeleteCustomAttribute");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          customAttributeKey135,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "DeleteCustomAttribute")));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI136!=null){
        org.apache.axiom.om.OMElement omElementresourceURI132 = toOM(resourceURI136, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "DeleteCustomAttribute")));
        addHeader(omElementresourceURI132,env);
      }

      // add the children only if the parameter is not null
      if (selectorSet137!=null){
        org.apache.axiom.om.OMElement omElementselectorSet133 = toOM(selectorSet137, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "DeleteCustomAttribute")));
        addHeader(omElementselectorSet133,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout138!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout134 = toOM(operationTimeout138, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "DeleteCustomAttribute")));
        addHeader(omElementoperationTimeout134,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);
      return;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * SubscribeOp method
   * 
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#SubscribeOp
   * @param subscribe140 Subscribe soap body block
   * @param resourceURI141 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param operationTimeout142 the operation timeout
   */
  public  org.xmlsoap.schemas.ws._2004._08.eventing.SubscribeResponse SubscribeOp(
      org.xmlsoap.schemas.ws._2004._08.eventing.Subscribe subscribe140,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI141,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout142)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[11].getName());
      _operationClient.getOptions().setAction("http://schemas.xmlsoap.org/ws/2004/08/eventing/Subscribe");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          subscribe140,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "SubscribeOp")));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI141!=null){
        org.apache.axiom.om.OMElement omElementresourceURI137 = toOM(resourceURI141, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "SubscribeOp")));
        addHeader(omElementresourceURI137,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout142!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout138 = toOM(operationTimeout142, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "SubscribeOp")));
        addHeader(omElementoperationTimeout138,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);

      org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
          org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
      org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();

      java.lang.Object object = fromOM(
          _returnEnv.getBody().getFirstElement() ,
          org.xmlsoap.schemas.ws._2004._08.eventing.SubscribeResponse.class,
          getEnvelopeNamespaces(_returnEnv));

      return (org.xmlsoap.schemas.ws._2004._08.eventing.SubscribeResponse)object;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * Put method
   * 
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#Put
   * @param incident144 Incident to be updated (new values)
   * @param resourceURI145 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet146 the selector set to identify the incident
   * @param operationTimeout147 the operation timeout
   */
  public  com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident Put(
      com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident incident144,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI145,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet146,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout147)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[12].getName());
      _operationClient.getOptions().setAction("http://schemas.xmlsoap.org/ws/2004/09/transfer/Put");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          incident144,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "Put")));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI145!=null){
        org.apache.axiom.om.OMElement omElementresourceURI141 = toOM(resourceURI145, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Put")));
        addHeader(omElementresourceURI141,env);
      }

      // add the children only if the parameter is not null
      if (selectorSet146!=null){
        org.apache.axiom.om.OMElement omElementselectorSet142 = toOM(selectorSet146, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Put")));
        addHeader(omElementselectorSet142,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout147!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout143 = toOM(operationTimeout147, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Put")));
        addHeader(omElementoperationTimeout143,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      Log.log.trace("Put SOAP Envelope : " + _messageContext.getEnvelope().toString());
      
      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);            
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);

      org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
          org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
      org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();

      java.lang.Object object = fromOM(
          _returnEnv.getBody().getFirstElement() ,
          com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident.class,
          getEnvelopeNamespaces(_returnEnv));

      return (com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident)object;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * ReleaseOp method
   * 
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#ReleaseOp
   * @param release149 Release soap body block
   * @param resourceURI150 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param operationTimeout151 the operation timeout
   */
  public  void ReleaseOp(
      org.xmlsoap.schemas.ws._2004._09.enumeration.Release release149,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI150,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout151)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[13].getName());
      _operationClient.getOptions().setAction("http://schemas.xmlsoap.org/ws/2004/09/enumeration/Release");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          release149,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "ReleaseOp")));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI150!=null){
        org.apache.axiom.om.OMElement omElementresourceURI146 = toOM(resourceURI150, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "ReleaseOp")));
        addHeader(omElementresourceURI146,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout151!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout147 = toOM(operationTimeout151, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "ReleaseOp")));
        addHeader(omElementoperationTimeout147,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);
      return;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * Close method
   *
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#Close
   * @param resourceURI154 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet155 the selector set to identify the incident
   * @param operationTimeout156 the operation timeout
   */
  public  void Close(
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI154,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet155,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout156)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[14].getName());
      _operationClient.getOptions().setAction("http://schemas.hp.com/ism/ServiceOperation/Common/1/Close");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI154!=null){
        org.apache.axiom.om.OMElement omElementresourceURI150 = toOM(resourceURI154, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Close")));
        addHeader(omElementresourceURI150,env);
      }

      // add the children only if the parameter is not null
      if (selectorSet155!=null){
        org.apache.axiom.om.OMElement omElementselectorSet151 = toOM(selectorSet155, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Close")));
        addHeader(omElementselectorSet151,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout156!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout152 = toOM(operationTimeout156, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Close")));
        addHeader(omElementoperationTimeout152,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);
      return;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * GetInstructionText method
   *
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#GetInstructionText
   * @param resourceURI159 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet160 the selector set to identify the incident
   * @param operationTimeout161 the operation timeout
   */
  public  java.lang.String GetInstructionText(
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI159,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet160,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout161)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[15].getName());
      _operationClient.getOptions().setAction("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident/GetInstructionText");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI159!=null){
        org.apache.axiom.om.OMElement omElementresourceURI159 = toOM(resourceURI159, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "GetInstructionText")));
        addHeader(omElementresourceURI159,env);
      }

      // add the children only if the parameter is not null
      if (selectorSet160!=null){
        org.apache.axiom.om.OMElement omElementselectorSet160 = toOM(selectorSet160, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "GetInstructionText")));
        addHeader(omElementselectorSet160,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout161!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout161 = toOM(operationTimeout161, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "GetInstructionText")));
        addHeader(omElementoperationTimeout161,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);            
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);

      org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
          org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
      org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();

      java.lang.Object object = fromOM(
          _returnEnv.getBody().getFirstElement() ,
          java.lang.String.class,
          getEnvelopeNamespaces(_returnEnv));

      return (java.lang.String)object;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }
  
  /**
   * OwnMany method
   *
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#OwnMany
   * @param incidentIDs163 List of incident IDs to be owned
   * @param resourceURI164 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param operationTimeout165 the operation timeout
   */
  public  void OwnMany(
      com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.IncidentIDs incidentIDs163,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI164,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout165)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[15].getName());
      _operationClient.getOptions().setAction("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident/OwnMany");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          incidentIDs163,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "OwnMany")));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI164!=null){
        org.apache.axiom.om.OMElement omElementresourceURI155 = toOM(resourceURI164, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "OwnMany")));
        addHeader(omElementresourceURI155,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout165!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout156 = toOM(operationTimeout165, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "OwnMany")));
        addHeader(omElementoperationTimeout156,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);            
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);
      return;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * Get method
   * 
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#Get
   * @param resourceURI168 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet169 the selector set to identify the incident
   * @param operationTimeout170 the operation timeout
   */
  public  com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident Get(
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI168,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet169,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout170)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[16].getName());
      _operationClient.getOptions().setAction("http://schemas.xmlsoap.org/ws/2004/09/transfer/Get");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI168!=null){
        org.apache.axiom.om.OMElement omElementresourceURI159 = toOM(resourceURI168, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Get")));
        addHeader(omElementresourceURI159,env);
      }

      // add the children only if the parameter is not null
      if (selectorSet169!=null){
        org.apache.axiom.om.OMElement omElementselectorSet160 = toOM(selectorSet169, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Get")));
        addHeader(omElementselectorSet160,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout170!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout161 = toOM(operationTimeout170, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Get")));
        addHeader(omElementoperationTimeout161,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);

      org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
          org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
      org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();

      java.lang.Object object = fromOM(
          _returnEnv.getBody().getFirstElement() ,
          com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident.class,
          getEnvelopeNamespaces(_returnEnv));

      return (com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident)object;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * StartAction method
   *
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#StartAction
   * @param actionType172 the type of action that will be started (automatic or operator-initiated)
   * @param resourceURI173 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet174 the selector set to identify the incident
   * @param operationTimeout175 the operation timeout
   */
  public  void StartAction(
      com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.ActionType actionType172,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI173,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet174,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout175)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[17].getName());
      _operationClient.getOptions().setAction("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident/StartAction");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          actionType172,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "StartAction")));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI173!=null){
        org.apache.axiom.om.OMElement omElementresourceURI164 = toOM(resourceURI173, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "StartAction")));
        addHeader(omElementresourceURI164,env);
      }

      // add the children only if the parameter is not null
      if (selectorSet174!=null){
        org.apache.axiom.om.OMElement omElementselectorSet165 = toOM(selectorSet174, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "StartAction")));
        addHeader(omElementselectorSet165,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout175!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout166 = toOM(operationTimeout175, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "StartAction")));
        addHeader(omElementoperationTimeout166,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);
      return;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * UpdateAnnotation method
   * 
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#UpdateAnnotation
   * @param updateAnnotation177 Text and ID of annotation that should be updated (in UpdateAnnotation structure)
   * @param resourceURI178 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet179 the selector set to identify the incident
   * @param operationTimeout180 the operation timeout
   */
  public  void UpdateAnnotation(
      com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.UpdateAnnotation updateAnnotation177,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI178,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet179,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout180)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[18].getName());
      _operationClient.getOptions().setAction("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident/UpdateAnnotation");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          updateAnnotation177,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "UpdateAnnotation")));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI178!=null){
        org.apache.axiom.om.OMElement omElementresourceURI169 = toOM(resourceURI178, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "UpdateAnnotation")));
        addHeader(omElementresourceURI169,env);
      }

      // add the children only if the parameter is not null
      if (selectorSet179!=null){
        org.apache.axiom.om.OMElement omElementselectorSet170 = toOM(selectorSet179, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "UpdateAnnotation")));
        addHeader(omElementselectorSet170,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout180!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout171 = toOM(operationTimeout180, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "UpdateAnnotation")));
        addHeader(omElementoperationTimeout171,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);            
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);
      return;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * AddAnnotation method
   * 
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#AddAnnotation
   * @param annotationText182 Text of new annotation
   * @param resourceURI183 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet184 the selector set to identify the incident
   * @param operationTimeout185 the operation timeout
   */
  public  java.lang.String AddAnnotation(
      java.lang.String annotationText182,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI183,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet184,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout185)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[19].getName());
      _operationClient.getOptions().setAction("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident/AddAnnotation");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      /*
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          annotationText182,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "AddAnnotation")));
          */
      env = toEnvelopeAddAnnotation(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          annotationText182,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "AddAnnotation")));
      env.build();
      
      // add the children only if the parameter is not null
      if (resourceURI183!=null){
        org.apache.axiom.om.OMElement omElementresourceURI174 = toOM(resourceURI183, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "AddAnnotation")));
        addHeader(omElementresourceURI174,env);
      }

      // add the children only if the parameter is not null
      if (selectorSet184!=null){
        org.apache.axiom.om.OMElement omElementselectorSet175 = toOM(selectorSet184, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "AddAnnotation")));
        addHeader(omElementselectorSet175,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout185!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout176 = toOM(operationTimeout185, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "AddAnnotation")));
        addHeader(omElementoperationTimeout176,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);

      org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
          org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
      org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();

      java.lang.Object object = fromOM(
          _returnEnv.getBody().getFirstElement() ,
          java.lang.String.class,
          getEnvelopeNamespaces(_returnEnv));

      return (java.lang.String)object;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * DeleteAnnotation method
   * 
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#DeleteAnnotation
   * @param annotationId187 ID of annotation that should be deleted
   * @param resourceURI188 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet189 the selector set to identify the incident
   * @param operationTimeout190 the operation timeout
   */
  public  void DeleteAnnotation(
      java.lang.String annotationId187,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI188,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet189,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout190)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[20].getName());
      _operationClient.getOptions().setAction("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident/DeleteAnnotation");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      /*
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          annotationId187,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "DeleteAnnotation")));
       */
      env = toEnvelopeDeleteAnnotation(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          annotationId187,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "DeleteAnnotation")));

      env.build();

      // add the children only if the parameter is not null
      if (resourceURI188!=null){
        org.apache.axiom.om.OMElement omElementresourceURI179 = toOM(resourceURI188, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "DeleteAnnotation")));
        addHeader(omElementresourceURI179,env);
      }

      // add the children only if the parameter is not null
      if (selectorSet189!=null){
        org.apache.axiom.om.OMElement omElementselectorSet180 = toOM(selectorSet189, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "DeleteAnnotation")));
        addHeader(omElementselectorSet180,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout190!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout181 = toOM(operationTimeout190, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "DeleteAnnotation")));
        addHeader(omElementoperationTimeout181,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);            
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);
      return;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * ReopenMany method signature
   *
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#ReopenMany
   * @param incidentIDs192 List of incident IDs to be reopened
   * @param resourceURI193 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param operationTimeout194 the operation timeout
   */
  public  void ReopenMany(
      com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.IncidentIDs incidentIDs192,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI193,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout194)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[21].getName());
      _operationClient.getOptions().setAction("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident/ReopenMany");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          incidentIDs192,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "ReopenMany")));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI193!=null){
        org.apache.axiom.om.OMElement omElementresourceURI184 = toOM(resourceURI193, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "ReopenMany")));
        addHeader(omElementresourceURI184,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout194!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout185 = toOM(operationTimeout194, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "ReopenMany")));
        addHeader(omElementoperationTimeout185,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);
      return;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * Delete method signature
   * 
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#Delete
   * @param resourceURI197 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet198 the selector set to identify the incident
   * @param operationTimeout199 the operation timeout
   */
  public  void Delete(
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI197,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet198,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout199)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[22].getName());
      _operationClient.getOptions().setAction("http://schemas.xmlsoap.org/ws/2004/09/transfer/Delete");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI197!=null){
        org.apache.axiom.om.OMElement omElementresourceURI188 = toOM(resourceURI197, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Delete")));
        addHeader(omElementresourceURI188,env);
      }

      // add the children only if the parameter is not null
      if (selectorSet198!=null){
        org.apache.axiom.om.OMElement omElementselectorSet189 = toOM(selectorSet198, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Delete")));
        addHeader(omElementselectorSet189,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout199!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout190 = toOM(operationTimeout199, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Delete")));
        addHeader(omElementoperationTimeout190,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);
      return;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * Reopen method
   * 
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#Reopen
   * @param resourceURI202 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param selectorSet203 the selector set to identify the incident
   * @param operationTimeout204 the operation timeout
   */
  public  void Reopen(
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI202,
      org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType selectorSet203,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout204)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[23].getName());
      _operationClient.getOptions().setAction("http://schemas.hp.com/ism/ServiceOperation/Common/1/Reopen");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI202!=null){
        org.apache.axiom.om.OMElement omElementresourceURI193 = toOM(resourceURI202, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Reopen")));
        addHeader(omElementresourceURI193,env);
      }

      // add the children only if the parameter is not null
      if (selectorSet203!=null){
        org.apache.axiom.om.OMElement omElementselectorSet194 = toOM(selectorSet203, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Reopen")));
        addHeader(omElementselectorSet194,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout204!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout195 = toOM(operationTimeout204, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Reopen")));
        addHeader(omElementoperationTimeout195,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);
      return;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }

  /**
   * CloseMany method
   *
   * @see com.hp.ov.opr.ws.client.axis.IncidentService#CloseMany
   * @param incidentIDs206 List of incident IDs to be closed
   * @param resourceURI207 the Incident resource URI ("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident")
   * @param operationTimeout208 the operation timeout
   */
  public  void CloseMany(
      com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.IncidentIDs incidentIDs206,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI resourceURI207,
      org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration operationTimeout208)
  throws java.rmi.RemoteException
  {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try{
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[24].getName());
      _operationClient.getOptions().setAction("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident/CloseMany");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");

      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();

      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          incidentIDs206,
          optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "CloseMany")));
      env.build();

      // add the children only if the parameter is not null
      if (resourceURI207!=null){
        org.apache.axiom.om.OMElement omElementresourceURI198 = toOM(resourceURI207, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "CloseMany")));
        addHeader(omElementresourceURI198,env);
      }

      // add the children only if the parameter is not null
      if (operationTimeout208!=null){
        org.apache.axiom.om.OMElement omElementoperationTimeout199 = toOM(operationTimeout208, optimizeContent(new javax.xml.namespace.QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "CloseMany")));
        addHeader(omElementoperationTimeout199,env);
      }

      //adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);

      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);

      // add the message context to the operation client
      _operationClient.addMessageContext(_messageContext);

      // MODIFIED - add username and password to http request
      org.apache.axis2.client.Options options = _operationClient.getOptions();
      options.setUserName(m_user);
      options.setPassword(m_password);
      org.apache.axis2.transport.http.HttpTransportProperties.Authenticator auth
        = new org.apache.axis2.transport.http.HttpTransportProperties.Authenticator();
      auth.setUsername(m_user);
      auth.setPassword(m_password);
      options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
      _operationClient.setOptions(options);

      //execute the operation client
      _operationClient.execute(true);
      return;
    }catch(org.apache.axis2.AxisFault f){
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt!=null){
        if (faultExceptionNameMap.containsKey(faultElt.getQName())){
          //make the fault by reflection
          try{
            java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.Exception ex=
              (java.lang.Exception) exceptionClass.newInstance();
            //message class
            java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[]{messageClass});
            m.invoke(ex,new java.lang.Object[]{messageObject});
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }catch(java.lang.ClassCastException e){
            // we cannot instantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.ClassNotFoundException e) {
            // we cannot instantiate the class - throw the original Axis fault
            throw f;
          }catch (java.lang.NoSuchMethodException e) {
            // we cannot instantiate the class - throw the original Axis fault
            throw f;
          } catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot instantiate the class - throw the original Axis fault
            throw f;
          }  catch (java.lang.IllegalAccessException e) {
            // we cannot instantiate the class - throw the original Axis fault
            throw f;
          }   catch (java.lang.InstantiationException e) {
            // we cannot instantiate the class - throw the original Axis fault
            throw f;
          }
        }else{
          throw f;
        }
      }else{
        throw f;
      }
    } finally {
      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
    }
  }


  /**
   *  A utility method that copies the namepaces from the SOAPEnvelope
   */
  private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
    java.util.Map returnMap = new java.util.HashMap();
    java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
    while (namespaceIterator.hasNext()) {
      org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
      returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
    }
    return returnMap;
  }

  private javax.xml.namespace.QName[] opNameArray = null;
  private boolean optimizeContent(javax.xml.namespace.QName opName) {
    if (opNameArray == null) {
      return false;
    }
    for (int i = 0; i < opNameArray.length; i++) {
      if (opName.equals(opNameArray[i])) {
        return true;
      }
    }
    return false;
  }


  //$$SERVICE_URL/
  private static javax.xml.bind.JAXBContext _com_hp_schemas_opr_ws_serviceoperation_incidentmanagement__1_incident_IncidentIDs;
  private static javax.xml.bind.JAXBContext _org_dmtf_schemas_wbem_wsman__1_wsman_AttributableURI;
  private static javax.xml.bind.JAXBContext _org_dmtf_schemas_wbem_wsman__1_wsman_AttributableDuration;
  private static javax.xml.bind.JAXBContext _com_hp_schemas_opr_ws_serviceoperation_incidentmanagement__1_incident_ActionType;
  private static javax.xml.bind.JAXBContext _org_dmtf_schemas_wbem_wsman__1_wsman_SelectorSetType;
  private static javax.xml.bind.JAXBContext _org_xmlsoap_schemas_ws__2004__09_enumeration_Pull;
  private static javax.xml.bind.JAXBContext _org_xmlsoap_schemas_ws__2004__09_enumeration_PullResponse;
  private static javax.xml.bind.JAXBContext _org_xmlsoap_schemas_ws__2004__08_eventing_SubscriptionEnd;
  private static javax.xml.bind.JAXBContext _org_xmlsoap_schemas_ws__2004__09_enumeration_Enumerate;
  private static javax.xml.bind.JAXBContext _org_xmlsoap_schemas_ws__2004__09_enumeration_EnumerateResponse;
  private static javax.xml.bind.JAXBContext _com_hp_schemas_ism_serviceoperation_incidentmanagement__1_incident_Incident;
  private static javax.xml.bind.JAXBContext _org_xmlsoap_schemas_ws__2004__08_addressing_EndpointReferenceType;
  private static javax.xml.bind.JAXBContext _com_hp_schemas_opr_ws_serviceoperation_incidentmanagement__1_incident_CustomAttribute;
  private static javax.xml.bind.JAXBContext _com_hp_schemas_opr_ws_serviceoperation_incidentmanagement__1_incident_Annotations;
  private static javax.xml.bind.JAXBContext _org_xmlsoap_schemas_ws__2004__08_eventing_Renew;
  private static javax.xml.bind.JAXBContext _org_xmlsoap_schemas_ws__2004__08_eventing_RenewResponse;
  private static javax.xml.bind.JAXBContext _java_lang_String;
  private static javax.xml.bind.JAXBContext _org_xmlsoap_schemas_ws__2004__08_eventing_Unsubscribe;
  private static javax.xml.bind.JAXBContext _org_xmlsoap_schemas_ws__2004__08_eventing_Subscribe;
  private static javax.xml.bind.JAXBContext _org_xmlsoap_schemas_ws__2004__08_eventing_SubscribeResponse;
  private static javax.xml.bind.JAXBContext _org_xmlsoap_schemas_ws__2004__09_enumeration_Release;
  private static javax.xml.bind.JAXBContext _com_hp_schemas_opr_ws_serviceoperation_incidentmanagement__1_incident_UpdateAnnotation;
  
  private static final java.util.HashMap<Class,javax.xml.bind.JAXBContext> classContextMap = new java.util.HashMap<Class,javax.xml.bind.JAXBContext>();

  static {
    javax.xml.bind.JAXBContext jc;
    jc = null;
    try {
      jc = javax.xml.bind.JAXBContext.newInstance(com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.IncidentIDs.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.IncidentIDs");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _com_hp_schemas_opr_ws_serviceoperation_incidentmanagement__1_incident_IncidentIDs = jc;
      classContextMap.put(com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.IncidentIDs.class, jc);
    }

    jc = null;
    try {
      jc = javax.xml.bind.JAXBContext.newInstance(org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _org_dmtf_schemas_wbem_wsman__1_wsman_AttributableURI = jc;
      classContextMap.put(org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI.class, jc);
    }

    jc = null;
    try {
      jc = javax.xml.bind.JAXBContext.newInstance(org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _org_dmtf_schemas_wbem_wsman__1_wsman_AttributableDuration = jc;
      classContextMap.put(org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration.class, jc);
    }

    jc = null;
    try {
      jc = javax.xml.bind.JAXBContext.newInstance(com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.ActionType.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.ActionType");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _com_hp_schemas_opr_ws_serviceoperation_incidentmanagement__1_incident_ActionType = jc;
      classContextMap.put(com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.ActionType.class, jc);
    }

    jc = null;
    try {
      jc = javax.xml.bind.JAXBContext.newInstance(org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _org_dmtf_schemas_wbem_wsman__1_wsman_SelectorSetType = jc;
      classContextMap.put(org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType.class, jc);
    }

    jc = null;
    try {
      jc = javax.xml.bind.JAXBContext.newInstance(org.xmlsoap.schemas.ws._2004._09.enumeration.Pull.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: org.xmlsoap.schemas.ws._2004._09.enumeration.Pull");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _org_xmlsoap_schemas_ws__2004__09_enumeration_Pull = jc;
      classContextMap.put(org.xmlsoap.schemas.ws._2004._09.enumeration.Pull.class, jc);
    }

    jc = null;
    try {
      jc = javax.xml.bind.JAXBContext.newInstance(org.xmlsoap.schemas.ws._2004._09.enumeration.PullResponse.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: org.xmlsoap.schemas.ws._2004._09.enumeration.PullResponse");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _org_xmlsoap_schemas_ws__2004__09_enumeration_PullResponse = jc;
      classContextMap.put(org.xmlsoap.schemas.ws._2004._09.enumeration.PullResponse.class, jc);
    }

    jc = null;
    try {
      jc = javax.xml.bind.JAXBContext.newInstance(org.xmlsoap.schemas.ws._2004._08.eventing.SubscriptionEnd.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: org.xmlsoap.schemas.ws._2004._08.eventing.SubscriptionEnd");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _org_xmlsoap_schemas_ws__2004__08_eventing_SubscriptionEnd = jc;
      classContextMap.put(org.xmlsoap.schemas.ws._2004._08.eventing.SubscriptionEnd.class, jc);
    }

    jc = null;
    try {
      // MODIFIED - add IncidentEnumerationFilter
      jc = javax.xml.bind.JAXBContext.newInstance(org.xmlsoap.schemas.ws._2004._09.enumeration.Enumerate.class,
           com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.IncidentEnumerationFilter.class);
    	
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: org.xmlsoap.schemas.ws._2004._09.enumeration.Enumerate");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _org_xmlsoap_schemas_ws__2004__09_enumeration_Enumerate = jc;
      classContextMap.put(org.xmlsoap.schemas.ws._2004._09.enumeration.Enumerate.class, jc);
    }

    jc = null;
    try {
      jc = javax.xml.bind.JAXBContext.newInstance(org.xmlsoap.schemas.ws._2004._09.enumeration.EnumerateResponse.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: org.xmlsoap.schemas.ws._2004._09.enumeration.EnumerateResponse");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _org_xmlsoap_schemas_ws__2004__09_enumeration_EnumerateResponse = jc;
      classContextMap.put(org.xmlsoap.schemas.ws._2004._09.enumeration.EnumerateResponse.class, jc);
    }

    jc = null;
    try {
      jc = javax.xml.bind.JAXBContext.newInstance(com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _com_hp_schemas_ism_serviceoperation_incidentmanagement__1_incident_Incident = jc;
      classContextMap.put(com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident.class, jc);
    }

    jc = null;
    try {
      jc = javax.xml.bind.JAXBContext.newInstance(org.xmlsoap.schemas.ws._2004._08.addressing.EndpointReferenceType.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: org.xmlsoap.schemas.ws._2004._08.addressing.EndpointReferenceType");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _org_xmlsoap_schemas_ws__2004__08_addressing_EndpointReferenceType = jc;
      classContextMap.put(org.xmlsoap.schemas.ws._2004._08.addressing.EndpointReferenceType.class, jc);
    }

    jc = null;
    try {
      jc = javax.xml.bind.JAXBContext.newInstance(com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _com_hp_schemas_opr_ws_serviceoperation_incidentmanagement__1_incident_CustomAttribute = jc;
      classContextMap.put(com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute.class, jc);
    }

    jc = null;
    try {
      jc = javax.xml.bind.JAXBContext.newInstance(com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.Annotations.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.Annotations");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _com_hp_schemas_opr_ws_serviceoperation_incidentmanagement__1_incident_Annotations = jc;
      classContextMap.put(com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.Annotations.class, jc);
    }

    jc = null;
    try {
      jc = javax.xml.bind.JAXBContext.newInstance(org.xmlsoap.schemas.ws._2004._08.eventing.Renew.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: org.xmlsoap.schemas.ws._2004._08.eventing.Renew");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _org_xmlsoap_schemas_ws__2004__08_eventing_Renew = jc;
      classContextMap.put(org.xmlsoap.schemas.ws._2004._08.eventing.Renew.class, jc);
    }

    jc = null;
    try {
      jc = javax.xml.bind.JAXBContext.newInstance(org.xmlsoap.schemas.ws._2004._08.eventing.RenewResponse.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: org.xmlsoap.schemas.ws._2004._08.eventing.RenewResponse");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _org_xmlsoap_schemas_ws__2004__08_eventing_RenewResponse = jc;
      classContextMap.put(org.xmlsoap.schemas.ws._2004._08.eventing.RenewResponse.class, jc);
    }

    jc = null;
    try {
      jc = javax.xml.bind.JAXBContext.newInstance(java.lang.String.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: java.lang.String");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _java_lang_String = jc;
      classContextMap.put(java.lang.String.class, jc);
    }

    jc = null;
    try {
      jc = javax.xml.bind.JAXBContext.newInstance(org.xmlsoap.schemas.ws._2004._08.eventing.Unsubscribe.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: org.xmlsoap.schemas.ws._2004._08.eventing.Unsubscribe");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _org_xmlsoap_schemas_ws__2004__08_eventing_Unsubscribe = jc;
      classContextMap.put(org.xmlsoap.schemas.ws._2004._08.eventing.Unsubscribe.class, jc);
    }

    jc = null;
    try {
      // MODIFIED - add IncidentEventingFilter
      jc = javax.xml.bind.JAXBContext.newInstance(org.xmlsoap.schemas.ws._2004._08.eventing.Subscribe.class,
          com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.IncidentEventingFilter.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: org.xmlsoap.schemas.ws._2004._08.eventing.Subscribe");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _org_xmlsoap_schemas_ws__2004__08_eventing_Subscribe = jc;
      classContextMap.put(org.xmlsoap.schemas.ws._2004._08.eventing.Subscribe.class, jc);
    }

    jc = null;
    try {
      jc = javax.xml.bind.JAXBContext.newInstance(org.xmlsoap.schemas.ws._2004._08.eventing.SubscribeResponse.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: org.xmlsoap.schemas.ws._2004._08.eventing.SubscribeResponse");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _org_xmlsoap_schemas_ws__2004__08_eventing_SubscribeResponse = jc;
      classContextMap.put(org.xmlsoap.schemas.ws._2004._08.eventing.SubscribeResponse.class, jc);
    }

    jc = null;
    try {
      jc = javax.xml.bind.JAXBContext.newInstance(org.xmlsoap.schemas.ws._2004._09.enumeration.Release.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: org.xmlsoap.schemas.ws._2004._09.enumeration.Release");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _org_xmlsoap_schemas_ws__2004__09_enumeration_Release = jc;
      classContextMap.put(org.xmlsoap.schemas.ws._2004._09.enumeration.Release.class, jc);
    }

    jc = null;
    try {
      jc = javax.xml.bind.JAXBContext.newInstance(com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.UpdateAnnotation.class);
    }
    catch ( javax.xml.bind.JAXBException ex ) {
      System.err.println("Unable to create JAXBContext for class: com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.UpdateAnnotation");
      Runtime.getRuntime().exit(-1);
    }
    finally {
      _com_hp_schemas_opr_ws_serviceoperation_incidentmanagement__1_incident_UpdateAnnotation = jc;
      classContextMap.put(com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.UpdateAnnotation.class, jc);
    }
  }


  private org.apache.axiom.om.OMElement toOM(com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.IncidentIDs param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _com_hp_schemas_opr_ws_serviceoperation_incidentmanagement__1_incident_IncidentIDs;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.IncidentIDs.class,
          param,
          marshaller,
          "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident",
          "IncidentIDs");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident",
          null);
      return factory.createOMElement(source, "IncidentIDs", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.IncidentIDs param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  private org.apache.axiom.om.OMElement toOM(org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _org_dmtf_schemas_wbem_wsman__1_wsman_AttributableURI;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI.class,
          param,
          marshaller,
          "http://schemas.dmtf.org/wbem/wsman/1/wsman.xsd",
          "ResourceURI");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.dmtf.org/wbem/wsman/1/wsman.xsd",
          null);
      return factory.createOMElement(source, "ResourceURI", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  private org.apache.axiom.om.OMElement toOM(org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _org_dmtf_schemas_wbem_wsman__1_wsman_AttributableDuration;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration.class,
          param,
          marshaller,
          "http://schemas.dmtf.org/wbem/wsman/1/wsman.xsd",
          "OperationTimeout");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.dmtf.org/wbem/wsman/1/wsman.xsd",
          null);
      return factory.createOMElement(source, "OperationTimeout", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  private org.apache.axiom.om.OMElement toOM(com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.ActionType param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _com_hp_schemas_opr_ws_serviceoperation_incidentmanagement__1_incident_ActionType;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.ActionType.class,
          param,
          marshaller,
          "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident",
          "ActionType");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident",
          null);
      return factory.createOMElement(source, "ActionType", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.ActionType param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  private org.apache.axiom.om.OMElement toOM(org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _org_dmtf_schemas_wbem_wsman__1_wsman_SelectorSetType;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType.class,
          param,
          marshaller,
          "http://schemas.dmtf.org/wbem/wsman/1/wsman.xsd",
          "SelectorSet");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.dmtf.org/wbem/wsman/1/wsman.xsd",
          null);
      return factory.createOMElement(source, "SelectorSet", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  private org.apache.axiom.om.OMElement toOM(org.xmlsoap.schemas.ws._2004._09.enumeration.Pull param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _org_xmlsoap_schemas_ws__2004__09_enumeration_Pull;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( org.xmlsoap.schemas.ws._2004._09.enumeration.Pull.class,
          param,
          marshaller,
          "http://schemas.xmlsoap.org/ws/2004/09/enumeration",
          "Pull");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.xmlsoap.org/ws/2004/09/enumeration",
          null);
      return factory.createOMElement(source, "Pull", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.xmlsoap.schemas.ws._2004._09.enumeration.Pull param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  private org.apache.axiom.om.OMElement toOM(org.xmlsoap.schemas.ws._2004._09.enumeration.PullResponse param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _org_xmlsoap_schemas_ws__2004__09_enumeration_PullResponse;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( org.xmlsoap.schemas.ws._2004._09.enumeration.PullResponse.class,
          param,
          marshaller,
          "http://schemas.xmlsoap.org/ws/2004/09/enumeration",
          "PullResponse");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.xmlsoap.org/ws/2004/09/enumeration",
          null);
      return factory.createOMElement(source, "PullResponse", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.xmlsoap.schemas.ws._2004._09.enumeration.PullResponse param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  private org.apache.axiom.om.OMElement toOM(org.xmlsoap.schemas.ws._2004._08.eventing.SubscriptionEnd param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _org_xmlsoap_schemas_ws__2004__08_eventing_SubscriptionEnd;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( org.xmlsoap.schemas.ws._2004._08.eventing.SubscriptionEnd.class,
          param,
          marshaller,
          "http://schemas.xmlsoap.org/ws/2004/08/eventing",
          "SubscriptionEnd");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.xmlsoap.org/ws/2004/08/eventing",
          null);
      return factory.createOMElement(source, "SubscriptionEnd", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.xmlsoap.schemas.ws._2004._08.eventing.SubscriptionEnd param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  private org.apache.axiom.om.OMElement toOM(org.xmlsoap.schemas.ws._2004._09.enumeration.Enumerate param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _org_xmlsoap_schemas_ws__2004__09_enumeration_Enumerate;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( org.xmlsoap.schemas.ws._2004._09.enumeration.Enumerate.class,
          param,
          marshaller,
          "http://schemas.xmlsoap.org/ws/2004/09/enumeration",
          "Enumerate");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.xmlsoap.org/ws/2004/09/enumeration",
          null);
      return factory.createOMElement(source, "Enumerate", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.xmlsoap.schemas.ws._2004._09.enumeration.Enumerate param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  private org.apache.axiom.om.OMElement toOM(org.xmlsoap.schemas.ws._2004._09.enumeration.EnumerateResponse param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _org_xmlsoap_schemas_ws__2004__09_enumeration_EnumerateResponse;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( org.xmlsoap.schemas.ws._2004._09.enumeration.EnumerateResponse.class,
          param,
          marshaller,
          "http://schemas.xmlsoap.org/ws/2004/09/enumeration",
          "EnumerateResponse");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.xmlsoap.org/ws/2004/09/enumeration",
          null);
      return factory.createOMElement(source, "EnumerateResponse", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.xmlsoap.schemas.ws._2004._09.enumeration.EnumerateResponse param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  private org.apache.axiom.om.OMElement toOM(com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _com_hp_schemas_ism_serviceoperation_incidentmanagement__1_incident_Incident;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident.class,
          param,
          marshaller,
          "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          "Incident");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident",
          null);
      return factory.createOMElement(source, "Incident", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  private org.apache.axiom.om.OMElement toOM(org.xmlsoap.schemas.ws._2004._08.addressing.EndpointReferenceType param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _org_xmlsoap_schemas_ws__2004__08_addressing_EndpointReferenceType;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( org.xmlsoap.schemas.ws._2004._08.addressing.EndpointReferenceType.class,
          param,
          marshaller,
          "http://schemas.xmlsoap.org/ws/2004/09/transfer",
          "ResourceCreated");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.xmlsoap.org/ws/2004/09/transfer",
          null);
      return factory.createOMElement(source, "ResourceCreated", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.xmlsoap.schemas.ws._2004._08.addressing.EndpointReferenceType param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  private org.apache.axiom.om.OMElement toOM(com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _com_hp_schemas_opr_ws_serviceoperation_incidentmanagement__1_incident_CustomAttribute;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute.class,
          param,
          marshaller,
          "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident",
          "CustomAttribute");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident",
          null);
      return factory.createOMElement(source, "CustomAttribute", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  private org.apache.axiom.om.OMElement toOM(com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.Annotations param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _com_hp_schemas_opr_ws_serviceoperation_incidentmanagement__1_incident_Annotations;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.Annotations.class,
          param,
          marshaller,
          "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident",
          "Annotations");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident",
          null);
      return factory.createOMElement(source, "Annotations", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.Annotations param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  private org.apache.axiom.om.OMElement toOM(org.xmlsoap.schemas.ws._2004._08.eventing.Renew param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _org_xmlsoap_schemas_ws__2004__08_eventing_Renew;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( org.xmlsoap.schemas.ws._2004._08.eventing.Renew.class,
          param,
          marshaller,
          "http://schemas.xmlsoap.org/ws/2004/08/eventing",
          "Renew");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.xmlsoap.org/ws/2004/08/eventing",
          null);
      return factory.createOMElement(source, "Renew", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.xmlsoap.schemas.ws._2004._08.eventing.Renew param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  private org.apache.axiom.om.OMElement toOM(org.xmlsoap.schemas.ws._2004._08.eventing.RenewResponse param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _org_xmlsoap_schemas_ws__2004__08_eventing_RenewResponse;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( org.xmlsoap.schemas.ws._2004._08.eventing.RenewResponse.class,
          param,
          marshaller,
          "http://schemas.xmlsoap.org/ws/2004/08/eventing",
          "RenewResponse");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.xmlsoap.org/ws/2004/08/eventing",
          null);
      return factory.createOMElement(source, "RenewResponse", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.xmlsoap.schemas.ws._2004._08.eventing.RenewResponse param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  private org.apache.axiom.om.OMElement toOM(java.lang.String param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _java_lang_String;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( java.lang.String.class,
          param,
          marshaller,
          "http://schemas.xmlsoap.org/ws/2004/08/eventing",
          "Identifier");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.xmlsoap.org/ws/2004/08/eventing",
          null);
      return factory.createOMElement(source, "Identifier", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, java.lang.String param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }

  private org.apache.axiom.om.OMElement toOMAddAnnotation(java.lang.String param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _java_lang_String;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( java.lang.String.class,
          param,
          marshaller,
          "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident",
          "AnnotationText");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident",
          null);
      return factory.createOMElement(source, "AnnotationText", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelopeAddAnnotation(org.apache.axiom.soap.SOAPFactory factory, java.lang.String param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOMAddAnnotation(param, optimizeContent));
    return envelope;
  }
  
  private org.apache.axiom.om.OMElement toOMDeleteAnnotation(java.lang.String param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _java_lang_String;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( java.lang.String.class,
          param,
          marshaller,
          "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident",
          "AnnotationId");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident",
          null);
      return factory.createOMElement(source, "AnnotationId", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelopeDeleteAnnotation(org.apache.axiom.soap.SOAPFactory factory, java.lang.String param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOMDeleteAnnotation(param, optimizeContent));
    return envelope;
  }

  private org.apache.axiom.om.OMElement toOM(org.xmlsoap.schemas.ws._2004._08.eventing.Unsubscribe param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _org_xmlsoap_schemas_ws__2004__08_eventing_Unsubscribe;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( org.xmlsoap.schemas.ws._2004._08.eventing.Unsubscribe.class,
          param,
          marshaller,
          "http://schemas.xmlsoap.org/ws/2004/08/eventing",
          "Unsubscribe");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.xmlsoap.org/ws/2004/08/eventing",
          null);
      return factory.createOMElement(source, "Unsubscribe", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.xmlsoap.schemas.ws._2004._08.eventing.Unsubscribe param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  private org.apache.axiom.om.OMElement toOM(org.xmlsoap.schemas.ws._2004._08.eventing.Subscribe param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _org_xmlsoap_schemas_ws__2004__08_eventing_Subscribe;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( org.xmlsoap.schemas.ws._2004._08.eventing.Subscribe.class,
          param,
          marshaller,
          "http://schemas.xmlsoap.org/ws/2004/08/eventing",
          "Subscribe");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.xmlsoap.org/ws/2004/08/eventing",
          null);
      return factory.createOMElement(source, "Subscribe", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.xmlsoap.schemas.ws._2004._08.eventing.Subscribe param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  private org.apache.axiom.om.OMElement toOM(org.xmlsoap.schemas.ws._2004._08.eventing.SubscribeResponse param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _org_xmlsoap_schemas_ws__2004__08_eventing_SubscribeResponse;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( org.xmlsoap.schemas.ws._2004._08.eventing.SubscribeResponse.class,
          param,
          marshaller,
          "http://schemas.xmlsoap.org/ws/2004/08/eventing",
          "SubscribeResponse");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.xmlsoap.org/ws/2004/08/eventing",
          null);
      return factory.createOMElement(source, "SubscribeResponse", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.xmlsoap.schemas.ws._2004._08.eventing.SubscribeResponse param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  private org.apache.axiom.om.OMElement toOM(org.xmlsoap.schemas.ws._2004._09.enumeration.Release param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _org_xmlsoap_schemas_ws__2004__09_enumeration_Release;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( org.xmlsoap.schemas.ws._2004._09.enumeration.Release.class,
          param,
          marshaller,
          "http://schemas.xmlsoap.org/ws/2004/09/enumeration",
          "Release");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.xmlsoap.org/ws/2004/09/enumeration",
          null);
      return factory.createOMElement(source, "Release", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.xmlsoap.schemas.ws._2004._09.enumeration.Release param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  private org.apache.axiom.om.OMElement toOM(com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.UpdateAnnotation param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    try {
      javax.xml.bind.JAXBContext context = _com_hp_schemas_opr_ws_serviceoperation_incidentmanagement__1_incident_UpdateAnnotation;
      javax.xml.bind.Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

      JaxbRIDataSource source = new JaxbRIDataSource( com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.UpdateAnnotation.class,
          param,
          marshaller,
          "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident",
          "UpdateAnnotation");
      org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident",
          null);
      return factory.createOMElement(source, "UpdateAnnotation", namespace);
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.UpdateAnnotation param, boolean optimizeContent)
  throws org.apache.axis2.AxisFault {
    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
    envelope.getBody().addChild(toOM(param, optimizeContent));
    return envelope;
  }


  /**
   *  get the default envelope
   */
  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory) {
    return factory.getDefaultEnvelope();
  }

  private java.lang.Object fromOM (
      org.apache.axiom.om.OMElement param,
      java.lang.Class type,
      java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{
    try {
      javax.xml.bind.JAXBContext context = classContextMap.get(type);
      javax.xml.bind.Unmarshaller unmarshaller = context.createUnmarshaller();
      return unmarshaller.unmarshal(param.getXMLStreamReaderWithoutCaching(), type).getValue();
    } catch (javax.xml.bind.JAXBException bex){
      throw org.apache.axis2.AxisFault.makeFault(bex);
    }
  }

  class JaxbRIDataSource implements org.apache.axiom.om.OMDataSource {
    /**
     * Bound object for output.
     */
     private final Object outObject;

    /**
     * Bound class for output.
     */
     private final Class outClazz;

     /**
      * Marshaller.
      */
     private final javax.xml.bind.Marshaller marshaller;

     /**
      * Namespace
      */
     private String nsuri;

     /**
      * Local name
      */
     private String name;

     /**
      * Constructor from object and marshaller.
      *
      * @param obj
      * @param marshaller
      */
     public JaxbRIDataSource(Class clazz, Object obj, javax.xml.bind.Marshaller marshaller, String nsuri, String name) {
       this.outClazz = clazz;
       this.outObject = obj;
       this.marshaller = marshaller;
       this.nsuri = nsuri;
       this.name = name;
     }

     public void serialize(java.io.OutputStream output, org.apache.axiom.om.OMOutputFormat format) throws javax.xml.stream.XMLStreamException {
       try {
         marshaller.marshal(new javax.xml.bind.JAXBElement(
             new javax.xml.namespace.QName(nsuri, name), outObject.getClass(), outObject), output);
       } catch (javax.xml.bind.JAXBException e) {
         throw new javax.xml.stream.XMLStreamException("Error in JAXB marshalling", e);
       }
     }

     public void serialize(java.io.Writer writer, org.apache.axiom.om.OMOutputFormat format) throws javax.xml.stream.XMLStreamException {
       try {
         marshaller.marshal(new javax.xml.bind.JAXBElement(
             new javax.xml.namespace.QName(nsuri, name), outObject.getClass(), outObject), writer);
       } catch (javax.xml.bind.JAXBException e) {
         throw new javax.xml.stream.XMLStreamException("Error in JAXB marshalling", e);
       }
     }

     public void serialize(javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
       try {
         marshaller.marshal(new javax.xml.bind.JAXBElement(
             new javax.xml.namespace.QName(nsuri, name), outObject.getClass(), outObject), xmlWriter);
       } catch (javax.xml.bind.JAXBException e) {
         throw new javax.xml.stream.XMLStreamException("Error in JAXB marshalling", e);
       }
     }

     public javax.xml.stream.XMLStreamReader getReader() throws javax.xml.stream.XMLStreamException {
       try {
         javax.xml.bind.JAXBContext context = classContextMap.get(outClazz);
         org.apache.axiom.om.impl.builder.SAXOMBuilder builder = new org.apache.axiom.om.impl.builder.SAXOMBuilder();
         javax.xml.bind.Marshaller marshaller = context.createMarshaller();
         marshaller.marshal(new javax.xml.bind.JAXBElement(
             new javax.xml.namespace.QName(nsuri, name), outObject.getClass(), outObject), builder);

         return builder.getRootElement().getXMLStreamReader();
       } catch (javax.xml.bind.JAXBException e) {
         throw new javax.xml.stream.XMLStreamException("Error in JAXB marshalling", e);
       }
     }
  }
}
