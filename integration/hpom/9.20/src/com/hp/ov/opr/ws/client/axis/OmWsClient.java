/***************************************************************
 *  Copyright 2007 Hewlett-Packard Development Company, L.P.
 ***************************************************************/

package com.hp.ov.opr.ws.client.axis;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;

import org.apache.axis2.AxisFault;
import org.apache.xerces.dom.ElementNSImpl;
import org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration;
import org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI;
import org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType;
import org.dmtf.schemas.wbem.wsman._1.wsman.SelectorType;
import org.xmlsoap.schemas.ws._2004._08.addressing.EndpointReferenceType;
import org.xmlsoap.schemas.ws._2004._08.eventing.DeliveryType;
import org.xmlsoap.schemas.ws._2004._08.eventing.FilterType;
import org.xmlsoap.schemas.ws._2004._08.eventing.Renew;
import org.xmlsoap.schemas.ws._2004._08.eventing.Subscribe;
import org.xmlsoap.schemas.ws._2004._08.eventing.SubscribeResponse;
import org.xmlsoap.schemas.ws._2004._08.eventing.Unsubscribe;
import org.xmlsoap.schemas.ws._2004._09.enumeration.Enumerate;
import org.xmlsoap.schemas.ws._2004._09.enumeration.EnumerateResponse;
import org.xmlsoap.schemas.ws._2004._09.enumeration.EnumerationContextType;
import org.xmlsoap.schemas.ws._2004._09.enumeration.ItemListType;
import org.xmlsoap.schemas.ws._2004._09.enumeration.Pull;
import org.xmlsoap.schemas.ws._2004._09.enumeration.PullResponse;
import org.xmlsoap.schemas.ws._2004._09.enumeration.Release;

import com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident;
import com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incidentextensions.IncidentExtensionsType;
import com.hp.schemas.ism.servicetransition.configurationmanagement._1.node.NodePropertiesType;
import com.hp.schemas.ism.servicetransition.configurationmanagement._1.node.NodeReferenceType;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.ActionStatus;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.ActionType;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.Annotations;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttributes;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.IncidentAction;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.IncidentIDs;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.OperationsExtension;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.UpdateAnnotation;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.IncidentEnumerationFilter;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.IncidentEventingFilter;

/**
 *
 * Actual client class, which calls the stub methods from the IncidentServiceStub class <br>
 * <br>
 * How to set up an eclipse project:<br>
 *
 * <li>Make sure you have axis2-1.4.1 installed. Other version might also work, but have not been verified</li>
 * <li>Open eclipse, create a new Java Project from existing source</li>
 * <li>Select the directory, this file is located in</li>
 * <li>Make sure the source directory setting for the new project is correct</li>
 * <li>Add the following jar files from the axis installation to your Classpath:
 *   axiom-api-1.2.7.jar axis2-kernel-1.4.1.jar axis2-saaj-api-1.4.1.jar
 *   commons-httpclient-3.1.jar commons-logging-1.1.1.jar
 *   jaxb-api-2.1.jar xercesImpl-2.8.1.jar
 * <li>Compilation should now succeed without failing</li>
 * <li>To actually run the client, you must add a main method, that calls any of the operations
 *   in this file</li>
 * <li>Also add all axis jars to the classpath, when the client should execute</li>
 * 
 */
public class OmWsClient
{

  // the instance of the Service Stub that does the actual SOAP requests
  private IncidentServiceStub m_proxy = null;

  // the target URI of the WebService. For the OM Incident WS, 
  // this is <hostname>:<port>/opr-webservice/Incident.svc
  private String m_targetURI = null;

  // An incident can be uniquely identified via its ID
  private static final String INCIDENT_ID_SELECTOR_NAME = "IncidentID";
  private static final String FILTER_DIALECT_URI = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/IncidentFilter";
  private static final String INCIDENT_RESOURCE_URI = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident";
  
  public OmWsClient (String targetURI)
  {
    try
    {
      m_proxy = new IncidentServiceStub(targetURI);
      m_targetURI = targetURI;
    }
    catch (AxisFault e)
    {
      e.printStackTrace();
    }
  }
  
  public OmWsClient (String host, String port, String user, String password)
  {
    try
    {
      m_targetURI = "https://" + host + ":" + port + "/opr-webservice/Incident.svc/";
      m_proxy = new IncidentServiceStub(m_targetURI, user, password);
    }
    catch (AxisFault e)
    {
      e.printStackTrace();
    }
  }
    
  public static void main(String[] args)
  {
    
    if (args == null || args.length < 4)
    {
      usage();
      System.exit(-1);
    }
    else
    {
      OmWsClient myClient = new OmWsClient(args[0], args[1], args[2], args[3]);
      
      // The following is only an example what can be done and how to use the API!!!
      
      try
      {
        // First, do an enumerate with a following pull
        Incident incidentWithOperAction = null;
        
        IncidentEnumerationFilter enumFilter = new IncidentEnumerationFilter();
        enumFilter.getSeverity().add("critical");
        String enumerationContext = myClient.enumerateWithFilterOp(enumFilter);
        System.out.println("Enumeration context = " + enumerationContext);
        
        List<Incident> pulledIncidents = myClient.pullOp(enumerationContext, BigInteger.valueOf(100));
        if (pulledIncidents.size() == 0)
            System.out.println("Enumerated no Incidents");
        else
            for (Incident inc : pulledIncidents)
            {
              String actionString = "";
              IncidentAction autoAction = inc.getExtensions().getOperationsExtension().getAutomaticAction();
              if ((autoAction != null) && (autoAction.getActionStatus() != ActionStatus.NOT_AVAILABLE))
              {
                actionString += "; has automatic action";
              }
              
              IncidentAction oprAction = inc.getExtensions().getOperationsExtension().getOperatorAction();
              if ((oprAction != null) && (oprAction.getActionStatus() != ActionStatus.NOT_AVAILABLE))
              {
                actionString += "; has operator-initiated action";
                incidentWithOperAction = inc;
              }
              
              System.out.println("Enumerated Incident " + inc.getIncidentID() + ": " + inc.getSeverity() + "; " + inc.getTitle() + "; " + actionString + "; " + myClient.getInstructionTextOp(inc.getIncidentID()));
            }
        
        // Then, start operator-initiated action
        if (incidentWithOperAction != null)
          myClient.startActionOp(incidentWithOperAction.getIncidentID(), ActionType.OPERATOR_ACTION);
        
        // Then, create a new incident with a custom attribute
        Incident myIncident = generateNewIncidentWithCustomAttribute(args);
        EndpointReferenceType incidentEPR = myClient.createOp(myIncident);
        String incidentID = extractIncidentIdFromEPR(incidentEPR);
        System.out.println("Created Incident " + incidentID);
        
        // get the newly created incident
        myIncident = myClient.getOp(incidentID);
        System.out.println("Get Incident " + incidentID);
        
        // change its severity
        myIncident.setSeverity("Critical");
        myClient.putOp(myIncident);
        System.out.println("Changed severity on Incident " + incidentID);
          
        // and acknowledge it
        myClient.closeOp(incidentID);
        System.out.println("Acknowledged Incident " + incidentID);
      }
      catch (RemoteException e)
      {
        e.printStackTrace();
      }
    }
  }

  private static String extractIncidentIdFromEPR(EndpointReferenceType incidentEPR)
  {
    String incidentID = null;
    List<Object> anyList = incidentEPR.getReferenceParameters().getAny();
    for (Object obj : anyList)
    {
      if (obj instanceof ElementNSImpl)
      {
        ElementNSImpl objElem = (ElementNSImpl) obj;
        if (objElem.getLocalName().equals("SelectorSet"))
        {
          incidentID = objElem.getFirstChild().getTextContent();
        }
      }
      else if (obj instanceof org.apache.xerces.dom.ElementNSImpl)
      {
        org.apache.xerces.dom.ElementNSImpl objElem = (org.apache.xerces.dom.ElementNSImpl) obj;
        if (objElem.getLocalName().equals("SelectorSet"))
        {
          incidentID = objElem.getFirstChild().getTextContent();
        }
      }
    }
    return incidentID;
  }

  private static Incident generateNewIncidentWithCustomAttribute(String[] args)
  {
    Incident myIncident = new Incident();
    
    myIncident.setTitle("TestMessage2");
    
    NodeReferenceType nodeRef = new NodeReferenceType();
    NodePropertiesType nodeProperties = new NodePropertiesType();
    nodeProperties.setDnsName(args[0]);
    nodeRef.setNodeProperties(nodeProperties);
    myIncident.setEmittingNode(nodeRef);
    
    myIncident.setSeverity("Minor");

    IncidentExtensionsType incidentExtensions = new IncidentExtensionsType();
    OperationsExtension oprExt = new OperationsExtension();

    CustomAttribute myCA = new CustomAttribute();
    myCA.setKey("TestCA");
    myCA.setText("TestCAValue");
    CustomAttributes cas = new CustomAttributes();
    cas.getCustomAttribute().add(myCA);
    
    oprExt.setCustomAttributes(cas);
    incidentExtensions.setOperationsExtension(oprExt);
    myIncident.setExtensions(incidentExtensions);
    
    return myIncident;
  }
  
  private static void usage()
  {
    System.out.println("Usage: ");
    System.out.println("<OmWsClient> <host> <port> <username> <password>");
  }
  
  /**
   * Method to call the release operation on a WebService server
   * @param context The enumeration context (GUID) to be released
   * @throws RemoteException
   */
  private void releaseOp(String context) throws RemoteException
  {
    // Creating release structure, containing the EnumerationContext to be released
    Release release244 = new Release();
    EnumerationContextType enumContext = new EnumerationContextType();
    enumContext.getContent().add(context);
    release244.setEnumerationContext(enumContext);

    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();
    m_proxy.ReleaseOp(release244, resourceUri, operationTimeout);
  }

  /**
   * Method to call the getAnnotations custom operation on a WebService server
   * @param incidentID the ID of an incident, for which Annotations should be retrieved
   * @return a list of annotations
   * @throws RemoteException
   */
  private Annotations getAnnotationsOp(String incidentID) throws RemoteException
  {
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    // SelectorSetType
    SelectorSetType selectorSet = new SelectorSetType();
    SelectorType incidentIdSelector = new SelectorType();
    incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
    incidentIdSelector.getContent().add(incidentID);
    selectorSet.getSelector().add(incidentIdSelector);

    Annotations annos = m_proxy.GetAnnotations(resourceUri, selectorSet, operationTimeout);
    return annos;
  }

  /**
   * Method to call the deleteAnnotation custom operation on a WebService server
   * @param incidentID ID of an incident, for which an annotation should be deleted
   * @param annotationID ID of the annotation to be deleted
   * @throws RemoteException
   */
  private void deleteAnnotationOp(String incidentID, String annotationID) throws RemoteException
  {
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    // SelectorSetType
    SelectorSetType selectorSet = new SelectorSetType();
    SelectorType incidentIdSelector = new SelectorType();
    incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
    incidentIdSelector.getContent().add(incidentID);
    selectorSet.getSelector().add(incidentIdSelector);
   
    m_proxy.DeleteAnnotation(annotationID, resourceUri, selectorSet, operationTimeout);
  }

  /**
   * Method to call the updateAnnotation custom operation on a WebService server
   * @param incidentID ID of an incident, for which an annotation should be updated
   * @param annotationID ID of the annotation that should be updated 
   * @param annotationText The new text of the updated annotation
   * @throws RemoteException
   */
  private void updateAnnotationOp(String incidentID, 
                                  String annotationID, 
                                  String annotationText) throws RemoteException
  {
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    // SelectorSetType
    SelectorSetType selectorSet = new SelectorSetType();
    SelectorType incidentIdSelector = new SelectorType();
    incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
    incidentIdSelector.getContent().add(incidentID);
    selectorSet.getSelector().add(incidentIdSelector);
    
    UpdateAnnotation updateAnnotation = new UpdateAnnotation();
    updateAnnotation.setAnnotationId(annotationID);
    updateAnnotation.setAnnotationText(annotationText);

    m_proxy.UpdateAnnotation(updateAnnotation, resourceUri, selectorSet, operationTimeout);
  }

  /**
   * Method to call the addAnnotation custom operation on a WebService server
   * @param incidentID ID of an incident, for which an annotation should be added
   * @param annotationText The text of the new annotation
   * @return the ID of the new Annotation
   * @throws RemoteException
   */
  private String addAnnotationOp(String incidentID, String annotationText) throws RemoteException
  {
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    // SelectorSetType
    SelectorSetType selectorSet = new SelectorSetType();
    SelectorType incidentIdSelector = new SelectorType();
    incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
    incidentIdSelector.getContent().add(incidentID);
    selectorSet.getSelector().add(incidentIdSelector);
    
    return m_proxy.AddAnnotation(
    		annotationText, 
    		//new com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.ObjectFactory().createAnnotationText(annotationText),
    		resourceUri, selectorSet, operationTimeout);
  }

  /**
   * create an OperationTimeout of 10 minutes (used by each operation method)
   * @return an OperationTimeout
   */
  private AttributableDuration setOperationTimeout()
  {
    AttributableDuration operationTimeout = new AttributableDuration();
    Duration timeoutDuration;
    try
    {
      timeoutDuration = DatatypeFactory.newInstance().newDurationDayTime(true, 0, 0, 10, 0);
      operationTimeout.setValue(timeoutDuration);
    } 
    catch (DatatypeConfigurationException e) 
    {
      e.printStackTrace();
    }
    return operationTimeout;
  }

  /**
   * create a ResourceURI (used by each operation method)
   * @return a ResourceURI
   */
  private AttributableURI setResourceUri()
  {
    AttributableURI resourceUri = new AttributableURI();
    resourceUri.setValue(INCIDENT_RESOURCE_URI);
    return resourceUri;
  }

  /**
   * Method to call the deleteCustomAttribute custom operation on a WebService server
   * @param incidentID ID of an incident, for which a CA should be deleted
   * @param caKey the key of a Custom Attribute
   * @throws RemoteException
   */
  private void deleteCustomAttributeOp(String incidentID, String caKey) throws RemoteException
  {
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    // SelectorSetType
    SelectorSetType selectorSet = new SelectorSetType();
    SelectorType incidentIdSelector = new SelectorType();
    incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
    incidentIdSelector.getContent().add(incidentID);
    selectorSet.getSelector().add(incidentIdSelector);
    
    m_proxy.DeleteCustomAttribute(caKey, resourceUri, selectorSet, operationTimeout);
  }

  /**
   * Method to call the setCustomAttribute custom operation on a WebService server
   * @param incidentID ID of an incident, for which a CA should be set
   * @param cma the Custom Attribute that should be set
   * @throws RemoteException
   */
  private void setCustomAttributeOp(String incidentID, CustomAttribute cma) throws RemoteException
  {
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    // SelectorSetType
    SelectorSetType selectorSet = new SelectorSetType();
    SelectorType incidentIdSelector = new SelectorType();
    incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
    incidentIdSelector.getContent().add(incidentID);
    selectorSet.getSelector().add(incidentIdSelector);
    
    m_proxy.SetCustomAttribute(cma, resourceUri, selectorSet, operationTimeout);
  }

  /**
   * Method to call the StartAction custom operation on a WebService server
   * @param incidentID ID of an incident, for which an action should be started
   * @param actionType the type of action that should be started; can be ActionType.OPERATOR_ACTION or ActionType.AUTOMATIC_ACTION
   * @throws RemoteException
   */
  private void startActionOp(String incidentID, ActionType actionType) throws RemoteException
  {
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    // SelectorSetType
    SelectorSetType selectorSet = new SelectorSetType();
    SelectorType incidentIdSelector = new SelectorType();
    incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
    incidentIdSelector.getContent().add(incidentID);
    selectorSet.getSelector().add(incidentIdSelector);
    
    m_proxy.StartAction(actionType, resourceUri, selectorSet, operationTimeout);
  }
  
  /**
   * Method to call the StopAction custom operation on a WebService server
   * @param incidentID ID of an incident, for which an action should be stopped
   * @param actionType the type of action that should be stopped; can be ActionType.OPERATOR_ACTION or ActionType.AUTOMATIC_ACTION
   * @throws RemoteException
   */
  private void stopActionOp(String incidentID, ActionType actionType) throws RemoteException
  {
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    // SelectorSetType
    SelectorSetType selectorSet = new SelectorSetType();
    SelectorType incidentIdSelector = new SelectorType();
    incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
    incidentIdSelector.getContent().add(incidentID);
    selectorSet.getSelector().add(incidentIdSelector);
    
    m_proxy.StopAction(actionType, resourceUri, selectorSet, operationTimeout);
  }
  
  /**
   * Method to get the InstructionText of an incident from a WebService server
   * @param incidentID ID of an incident, for which the InstructionText should be retrieved
   * @return the InstructionText that was sent back from the server
   * @throws RemoteException
   */
  private String getInstructionTextOp(String incidentID) throws RemoteException
  {
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    // SelectorSetType
    SelectorSetType selectorSet = new SelectorSetType();
    SelectorType incidentIdSelector = new SelectorType();
    incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
    incidentIdSelector.getContent().add(incidentID);
    selectorSet.getSelector().add(incidentIdSelector);
    
    String instructionText = m_proxy.GetInstructionText(resourceUri, selectorSet, operationTimeout);
    return instructionText;
  }
  
  /**
   * Method to call the own custom operation on a WebService server
   * @param incidentIDList List of IDs of incidents, that should be owned
   * @throws RemoteException
   */
  private void ownOp(List<String> incidentIDList) throws RemoteException
  {
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    IncidentIDs incidentIDs = new IncidentIDs();
    incidentIDs.getId().addAll(incidentIDList);
    
    m_proxy.OwnMany(incidentIDs, resourceUri, operationTimeout);
  }
  
  /**
   * Method to call the disown custom operation on a WebService server
   * @param incidentIDList List of IDs of incidents, that should be disowned
   * @throws RemoteException
   */
  private void disownOp(List<String> incidentIDList) throws RemoteException
  {
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    IncidentIDs incidentIDs = new IncidentIDs();
    incidentIDs.getId().addAll(incidentIDList);
    
    m_proxy.DisownMany(incidentIDs, resourceUri, operationTimeout);
  }

  /**
   * Method to call the closeMany custom operation on a WebService server
   * @param incidentIDList List of IDs of incidents, that should be closed
   * @throws RemoteException
   */
  private void closeManyOp(List<String> incidentIDList) throws RemoteException
  {
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    IncidentIDs incidentIDs = new IncidentIDs();
    incidentIDs.getId().addAll(incidentIDList);
    
    m_proxy.CloseMany(incidentIDs, resourceUri, operationTimeout);
  }
  
  /**
   * Method to call the close custom operation on a WebService server
   * @param incidentID ID of  an incident, that should be closed
   * @throws RemoteException
   */
  private void closeOp(String incidentID) throws RemoteException
  {
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    // SelectorSetType
    SelectorSetType selectorSet = new SelectorSetType();
    SelectorType incidentIdSelector = new SelectorType();
    incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
    incidentIdSelector.getContent().add(incidentID);
    selectorSet.getSelector().add(incidentIdSelector);
    
    m_proxy.Close(resourceUri, selectorSet, operationTimeout);
  }
  
  /**
   * Method to call the reopenMany custom operation on a WebService server
   * @param incidentIDList List of IDs of incidents, that should be reopened
   * @throws RemoteException
   */
  private void reopenManyOp(List<String> incidentIDList) throws RemoteException
  {
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    IncidentIDs incidentIDs = new IncidentIDs();
    incidentIDs.getId().addAll(incidentIDList);
    
    m_proxy.ReopenMany(incidentIDs, resourceUri, operationTimeout);
  }
  
  /**
   * Method to call the reopen custom operation on a WebService server
   * @param incidentID ID of an incident, that should be reopened
   * @throws RemoteException
   */
  private void reopenOp(String incidentID) throws RemoteException
  {
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    // SelectorSetType
    SelectorSetType selectorSet = new SelectorSetType();
    SelectorType incidentIdSelector = new SelectorType();
    incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
    incidentIdSelector.getContent().add(incidentID);
    selectorSet.getSelector().add(incidentIdSelector);
    
    m_proxy.Reopen(resourceUri, selectorSet, operationTimeout);
  }

  /**
   * Method to call the get operation on a WebService server
   * @param incidentID ID of an incident, that should be retrieved
   * @return the Incident that was sent back from the server
   * @throws RemoteException
   */
  private Incident getOp(String incidentID) throws RemoteException
  {
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    // SelectorSetType
    SelectorSetType selectorSet = new SelectorSetType();
    SelectorType incidentIdSelector = new SelectorType();
    incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
    incidentIdSelector.getContent().add(incidentID);
    selectorSet.getSelector().add(incidentIdSelector);

    Incident incident = m_proxy.Get(resourceUri, selectorSet, operationTimeout);
    return incident;
  }
  
  /**
   * Method to call the pull operation on a WebService server (used by enumeration and eventing)
   * @param enumerationContext Context of the enumeration iterator
   * @param maxElements maximum number of elements that should be sent back
   * @return List of incidents 
   * @throws RemoteException
   */
  private List<Incident> pullOp(String enumerationContext, BigInteger maxElements) throws RemoteException
  {
    List<Incident> incidents = new ArrayList<Incident>();

    // Pull
    Pull pull = new Pull();
    EnumerationContextType enumerationContextType = new EnumerationContextType();
    enumerationContextType.getContent().add(enumerationContext);

    Duration maxTimeDuration = null;
    try
    {
      maxTimeDuration = DatatypeFactory.newInstance().newDurationDayTime(true, 0, 0, 50, 0);
    }
    catch (DatatypeConfigurationException e)
    {
      e.printStackTrace();
    }

    pull.setEnumerationContext(enumerationContextType);
    pull.setMaxTime(maxTimeDuration);

    if (maxElements != null)
    {
      pull.setMaxElements(maxElements);
    }
    else
    {
      pull.setMaxElements(BigInteger.valueOf(100));
    }

    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    PullResponse pullResponse = m_proxy.PullOp(pull, resourceUri, operationTimeout);
    ItemListType itemListType = pullResponse.getItems();
    List<Object> itemList = itemListType.getAny();

    // after the pull, convert the returned items to Incidents
    for (Object itemObject : itemList)
    {
      try
      {
        javax.xml.bind.JAXBContext context = m_proxy.getContextMap().get(Incident.class);
        javax.xml.bind.Unmarshaller unmarshaller = context.createUnmarshaller();
        incidents.add(unmarshaller.unmarshal((ElementNSImpl)itemObject, Incident.class).getValue());
      }
      catch (javax.xml.bind.JAXBException bex)
      {
        throw org.apache.axis2.AxisFault.makeFault(bex);
      }
    }

    return incidents;
  }

  /**
   * Method to call the create operation on a WebService server
   * @param incident Incident that should be created on the server
   * @return EndpointReference to the newly created incident
   * @throws RemoteException
   */
  private EndpointReferenceType createOp(Incident incident) throws RemoteException
  {
    if (m_proxy == null || m_targetURI == null)
    {
      return null;
    }
    
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();
    EndpointReferenceType epr = m_proxy.Create(incident, resourceUri, operationTimeout);
    return epr;
  }

  /**
   * Method to call the put operation on a WebService server (i.e. update an existing incident)
   * @param newIncident incident that should be updated (with new values)
   * @return incident that has been updated
   * @throws RemoteException
   */
  private Incident putOp(Incident newIncident) throws RemoteException
  {
    if (m_proxy == null || m_targetURI == null)
    {
      return null;
    }
    
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    // SelectorSetType
    SelectorSetType selectorSet = new SelectorSetType();
    SelectorType incidentIdSelector = new SelectorType();
    incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
    incidentIdSelector.getContent().add(newIncident.getIncidentID());
    selectorSet.getSelector().add(incidentIdSelector);

    Incident inc = m_proxy.Put(newIncident, resourceUri, selectorSet, operationTimeout);
    return inc;
  }
  
  /**
   * Method to call the enumerate operation on a WebService server
   * @return enumeration context ID, that can be used in subsequent pull operations
   * @throws RemoteException
   */
  private String enumerateOp() throws RemoteException
  {
    if (m_proxy == null || m_targetURI == null)
    {
      return null;
    }
    
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    // Enumerate
    Enumerate enumerate = new Enumerate();

    // SelectorSetType
    SelectorSetType selectorSet = null;

    EnumerateResponse enumerateResponse = m_proxy.EnumerateOp(enumerate, resourceUri, selectorSet, operationTimeout);
    String enumerationContext = (String) enumerateResponse.getEnumerationContext().getContent().get(0);
    return enumerationContext; 
  }
  
  /**
   * Method to call the enumerate operation on a WebService server with a filter
   * @param filter IncidentEnumerationFilter to filter for specific incidents
   * @return enumeration context ID, that can be used in subsequent pull operations
   * @throws RemoteException
   */
  private String enumerateWithFilterOp(IncidentEnumerationFilter filter) throws RemoteException
  {
    if (m_proxy == null || m_targetURI == null)
    {
      return null;
    }
    
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    // Enumerate
    Enumerate enumerate = new Enumerate();
    org.xmlsoap.schemas.ws._2004._09.enumeration.FilterType enumerationFilter = new org.xmlsoap.schemas.ws._2004._09.enumeration.FilterType();
    enumerationFilter.setDialect(FILTER_DIALECT_URI);
   // enumerationFilter.getContent().add(filter);
    enumerationFilter.getContent().add(new com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.ObjectFactory().createIncidentEnumerationFilter(filter));
    enumerate.setFilter(enumerationFilter);

    // SelectorSetType
    SelectorSetType selectorSet = null;

    EnumerateResponse enumerateResponse = m_proxy.EnumerateOp(enumerate, resourceUri, selectorSet, operationTimeout);
    String enumerationContext = (String) enumerateResponse.getEnumerationContext().getContent().get(0);
    return enumerationContext; 
  }

  /**
   * Method to call the subscribe operation (in pull mode) on a WebService server
   * @return enumeration context ID for the pull eventing
   * @throws RemoteException
   */
  private String subscribeOp() throws RemoteException
  {
    if (m_proxy == null || m_targetURI == null)
    {
      return null;
    }
    
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    // Subscribe
    Subscribe subscribe = new Subscribe();
    DeliveryType delivery = new DeliveryType();
    delivery.setMode("http://schemas.dmtf.org/wbem/wsman/1/wsman/Pull");
    subscribe.setDelivery(delivery);
    subscribe.setExpires("P1DT0H0M0.000S");

    SubscribeResponse response = m_proxy.SubscribeOp(subscribe, resourceUri, operationTimeout);
    List<Object> elements = response.getAny();
    for (Object o : elements)
    {
      if (o instanceof ElementNSImpl)
      {
        ElementNSImpl elem = (ElementNSImpl) o;
        if (elem.getLocalName().equals("EnumerationContext"))
        {
          return elem.getTextContent();
        }
        
      }
    }
    return null;
  }
  
  /**
   * Method to call the subscribe operation (in pull mode) on a WebService server
   * @param filter
   * @return enumeration context ID for the pull eventing
   * @throws RemoteException
   */
  private String subscribeWithFilterOp(IncidentEventingFilter filter) throws RemoteException
  {
    if (m_proxy == null || m_targetURI == null)
    {
      return null;
    }
    
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    // Subscribe
    Subscribe subscribe = new Subscribe();

    DeliveryType delivery = new DeliveryType();
    delivery.setMode("http://schemas.dmtf.org/wbem/wsman/1/wsman/Pull");
    subscribe.setDelivery(delivery);
    
    FilterType eventingFilter = new FilterType();
    eventingFilter.setDialect(FILTER_DIALECT_URI);
    eventingFilter.getContent().add(new com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.ObjectFactory().createIncidentEventingFilter(filter));

    subscribe.setFilter(eventingFilter);
    subscribe.setExpires("P1DT0H0M0.000S");

    SubscribeResponse response = m_proxy.SubscribeOp(subscribe, resourceUri, operationTimeout);
    List<Object> elements = response.getAny();
    for (Object o : elements)
    {
      if (o instanceof ElementNSImpl)
      {
        ElementNSImpl elem = (ElementNSImpl) o;
        if (elem.getLocalName().equals("EnumerationContext"))
        {
          return elem.getTextContent();
        }
        
      }
    }
    return null;
  }

  /**
   * Method to call the unsubscribe operation on a WebService server
   * @param eventingContext Context of the Eventing iterator that should be unsubscribed
   * @throws RemoteException
   */
  private void unsubscribeOp(String eventingContext) throws RemoteException
  {
    if (m_proxy == null || m_targetURI == null)
    {
      return;
    }
    
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    // UnSubscribe
    Unsubscribe unsubscribe = new Unsubscribe();

    m_proxy.UnsubscribeOp(unsubscribe, eventingContext, resourceUri, operationTimeout);
  }

  /**
   * Method to call the renew operation on a WebService server
   * @param eventingContext Context of the Eventing iterator that should be renewed
   * @param duration the new duration until the subscription expires
   * @throws RemoteException
   */
  private void renewOp(String eventingContext, Duration duration) throws RemoteException
  {
    if (m_proxy == null || m_targetURI == null)
    {
      return;
    }
    
    AttributableURI resourceUri = setResourceUri();
    AttributableDuration operationTimeout = setOperationTimeout();

    // Renew
    Renew renew = new Renew();
    renew.setExpires(duration.toString());

    m_proxy.RenewOp(renew, eventingContext, resourceUri, operationTimeout);
  }
}
