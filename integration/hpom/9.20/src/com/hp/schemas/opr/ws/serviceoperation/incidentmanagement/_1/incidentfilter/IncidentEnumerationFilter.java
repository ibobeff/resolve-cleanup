
package com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IncidentEnumerationFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IncidentEnumerationFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Severity" type="{http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident}Severity_OpenType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="EmittingNode" type="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/IncidentFilter}EmittingNode" minOccurs="0"/>
 *         &lt;element name="Category" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Application" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Object" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmittingCI" type="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/IncidentFilter}EmittingCI" minOccurs="0"/>
 *         &lt;element name="CorrelationKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EscalationStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ConditionMatched" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ReceivedTime" type="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/IncidentFilter}TimeFilter" minOccurs="0"/>
 *         &lt;element name="Title" type="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/IncidentFilter}KeywordFilter" minOccurs="0"/>
 *         &lt;element name="CustomAttributes" type="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/IncidentFilter}CustomAttributes" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IncidentEnumerationFilter", propOrder = {
    "severity",
    "emittingNode",
    "category",
    "application",
    "object",
    "emittingCI",
    "correlationKey",
    "escalationStatus",
    "conditionMatched",
    "receivedTime",
    "title",
    "customAttributes"
})
public class IncidentEnumerationFilter {

    @XmlElement(name = "Severity")
    protected List<String> severity;
    @XmlElement(name = "EmittingNode")
    protected EmittingNode emittingNode;
    @XmlElement(name = "Category")
    protected String category;
    @XmlElement(name = "Application")
    protected String application;
    @XmlElement(name = "Object")
    protected String object;
    @XmlElement(name = "EmittingCI")
    protected EmittingCI emittingCI;
    @XmlElement(name = "CorrelationKey")
    protected String correlationKey;
    @XmlElement(name = "EscalationStatus")
    protected String escalationStatus;
    @XmlElement(name = "ConditionMatched")
    protected Boolean conditionMatched;
    @XmlElement(name = "ReceivedTime")
    protected TimeFilter receivedTime;
    @XmlElement(name = "Title")
    protected KeywordFilter title;
    @XmlElement(name = "CustomAttributes")
    protected CustomAttributes customAttributes;

    /**
     * Gets the value of the severity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the severity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSeverity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getSeverity() {
        if (severity == null) {
            severity = new ArrayList<String>();
        }
        return this.severity;
    }

    /**
     * Gets the value of the emittingNode property.
     * 
     * @return
     *     possible object is
     *     {@link EmittingNode }
     *     
     */
    public EmittingNode getEmittingNode() {
        return emittingNode;
    }

    /**
     * Sets the value of the emittingNode property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmittingNode }
     *     
     */
    public void setEmittingNode(EmittingNode value) {
        this.emittingNode = value;
    }

    /**
     * Gets the value of the category property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategory() {
        return category;
    }

    /**
     * Sets the value of the category property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategory(String value) {
        this.category = value;
    }

    /**
     * Gets the value of the application property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplication() {
        return application;
    }

    /**
     * Sets the value of the application property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplication(String value) {
        this.application = value;
    }

    /**
     * Gets the value of the object property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObject() {
        return object;
    }

    /**
     * Sets the value of the object property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObject(String value) {
        this.object = value;
    }

    /**
     * Gets the value of the emittingCI property.
     * 
     * @return
     *     possible object is
     *     {@link EmittingCI }
     *     
     */
    public EmittingCI getEmittingCI() {
        return emittingCI;
    }

    /**
     * Sets the value of the emittingCI property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmittingCI }
     *     
     */
    public void setEmittingCI(EmittingCI value) {
        this.emittingCI = value;
    }

    /**
     * Gets the value of the correlationKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationKey() {
        return correlationKey;
    }

    /**
     * Sets the value of the correlationKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationKey(String value) {
        this.correlationKey = value;
    }

    /**
     * Gets the value of the escalationStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEscalationStatus() {
        return escalationStatus;
    }

    /**
     * Sets the value of the escalationStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEscalationStatus(String value) {
        this.escalationStatus = value;
    }

    /**
     * Gets the value of the conditionMatched property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isConditionMatched() {
        return conditionMatched;
    }

    /**
     * Sets the value of the conditionMatched property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setConditionMatched(Boolean value) {
        this.conditionMatched = value;
    }

    /**
     * Gets the value of the receivedTime property.
     * 
     * @return
     *     possible object is
     *     {@link TimeFilter }
     *     
     */
    public TimeFilter getReceivedTime() {
        return receivedTime;
    }

    /**
     * Sets the value of the receivedTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeFilter }
     *     
     */
    public void setReceivedTime(TimeFilter value) {
        this.receivedTime = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link KeywordFilter }
     *     
     */
    public KeywordFilter getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link KeywordFilter }
     *     
     */
    public void setTitle(KeywordFilter value) {
        this.title = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link CustomAttributes }
     *     
     */
    public CustomAttributes getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomAttributes }
     *     
     */
    public void setCustomAttributes(CustomAttributes value) {
        this.customAttributes = value;
    }

}
