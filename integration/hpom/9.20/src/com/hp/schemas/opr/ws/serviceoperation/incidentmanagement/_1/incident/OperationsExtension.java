
package com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for OperationsExtension complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OperationsExtension">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}Application" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}Object" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}StateChangeTime" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}CreationTime" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}ReceivedTime" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}NumberOfDuplicates" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}CorrelationKey" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}AcknowledgeCorrelationKey" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}ConditionMatched" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}AutomaticAction" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}OperatorAction" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}EscalationStatus" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}Source" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}NumberOfAnnotations" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}OriginalEvent" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}CustomAttributes" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}AutomaticActionStatus" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}OperatorActionStatus" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OperationsExtension", propOrder = {
    "application",
    "object",
    "stateChangeTime",
    "creationTime",
    "receivedTime",
    "numberOfDuplicates",
    "correlationKey",
    "acknowledgeCorrelationKey",
    "conditionMatched",
    "automaticAction",
    "operatorAction",
    "escalationStatus",
    "source",
    "numberOfAnnotations",
    "originalEvent",
    "customAttributes",
    "automaticActionStatus",
    "operatorActionStatus"
})
public class OperationsExtension {

    @XmlElement(name = "Application")
    protected String application;
    @XmlElement(name = "Object")
    protected String object;
    @XmlElement(name = "StateChangeTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar stateChangeTime;
    @XmlElement(name = "CreationTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creationTime;
    @XmlElement(name = "ReceivedTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar receivedTime;
    @XmlElement(name = "NumberOfDuplicates")
    protected Long numberOfDuplicates;
    @XmlElement(name = "CorrelationKey")
    protected String correlationKey;
    @XmlElement(name = "AcknowledgeCorrelationKey")
    protected String acknowledgeCorrelationKey;
    @XmlElement(name = "ConditionMatched")
    protected Boolean conditionMatched;
    @XmlElement(name = "AutomaticAction")
    protected IncidentAction automaticAction;
    @XmlElement(name = "OperatorAction")
    protected IncidentAction operatorAction;
    @XmlElement(name = "EscalationStatus")
    protected EscalationStatus escalationStatus;
    @XmlElement(name = "Source")
    protected String source;
    @XmlElement(name = "NumberOfAnnotations")
    protected Long numberOfAnnotations;
    @XmlElement(name = "OriginalEvent")
    protected String originalEvent;
    @XmlElement(name = "CustomAttributes")
    protected CustomAttributes customAttributes;
    @XmlElement(name = "AutomaticActionStatus")
    protected ActionStatus automaticActionStatus;
    @XmlElement(name = "OperatorActionStatus")
    protected ActionStatus operatorActionStatus;

    /**
     * Gets the value of the application property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplication() {
        return application;
    }

    /**
     * Sets the value of the application property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplication(String value) {
        this.application = value;
    }

    /**
     * Gets the value of the object property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObject() {
        return object;
    }

    /**
     * Sets the value of the object property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObject(String value) {
        this.object = value;
    }

    /**
     * Gets the value of the stateChangeTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStateChangeTime() {
        return stateChangeTime;
    }

    /**
     * Sets the value of the stateChangeTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStateChangeTime(XMLGregorianCalendar value) {
        this.stateChangeTime = value;
    }

    /**
     * Gets the value of the creationTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationTime() {
        return creationTime;
    }

    /**
     * Sets the value of the creationTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationTime(XMLGregorianCalendar value) {
        this.creationTime = value;
    }

    /**
     * Gets the value of the receivedTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReceivedTime() {
        return receivedTime;
    }

    /**
     * Sets the value of the receivedTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReceivedTime(XMLGregorianCalendar value) {
        this.receivedTime = value;
    }

    /**
     * Gets the value of the numberOfDuplicates property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumberOfDuplicates() {
        return numberOfDuplicates;
    }

    /**
     * Sets the value of the numberOfDuplicates property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumberOfDuplicates(Long value) {
        this.numberOfDuplicates = value;
    }

    /**
     * Gets the value of the correlationKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationKey() {
        return correlationKey;
    }

    /**
     * Sets the value of the correlationKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationKey(String value) {
        this.correlationKey = value;
    }

    /**
     * Gets the value of the acknowledgeCorrelationKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcknowledgeCorrelationKey() {
        return acknowledgeCorrelationKey;
    }

    /**
     * Sets the value of the acknowledgeCorrelationKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcknowledgeCorrelationKey(String value) {
        this.acknowledgeCorrelationKey = value;
    }

    /**
     * Gets the value of the conditionMatched property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isConditionMatched() {
        return conditionMatched;
    }

    /**
     * Sets the value of the conditionMatched property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setConditionMatched(Boolean value) {
        this.conditionMatched = value;
    }

    /**
     * Gets the value of the automaticAction property.
     * 
     * @return
     *     possible object is
     *     {@link IncidentAction }
     *     
     */
    public IncidentAction getAutomaticAction() {
        return automaticAction;
    }

    /**
     * Sets the value of the automaticAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link IncidentAction }
     *     
     */
    public void setAutomaticAction(IncidentAction value) {
        this.automaticAction = value;
    }

    /**
     * Gets the value of the operatorAction property.
     * 
     * @return
     *     possible object is
     *     {@link IncidentAction }
     *     
     */
    public IncidentAction getOperatorAction() {
        return operatorAction;
    }

    /**
     * Sets the value of the operatorAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link IncidentAction }
     *     
     */
    public void setOperatorAction(IncidentAction value) {
        this.operatorAction = value;
    }

    /**
     * Gets the value of the escalationStatus property.
     * 
     * @return
     *     possible object is
     *     {@link EscalationStatus }
     *     
     */
    public EscalationStatus getEscalationStatus() {
        return escalationStatus;
    }

    /**
     * Sets the value of the escalationStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link EscalationStatus }
     *     
     */
    public void setEscalationStatus(EscalationStatus value) {
        this.escalationStatus = value;
    }

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSource(String value) {
        this.source = value;
    }

    /**
     * Gets the value of the numberOfAnnotations property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumberOfAnnotations() {
        return numberOfAnnotations;
    }

    /**
     * Sets the value of the numberOfAnnotations property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumberOfAnnotations(Long value) {
        this.numberOfAnnotations = value;
    }

    /**
     * Gets the value of the originalEvent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalEvent() {
        return originalEvent;
    }

    /**
     * Sets the value of the originalEvent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalEvent(String value) {
        this.originalEvent = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link CustomAttributes }
     *     
     */
    public CustomAttributes getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomAttributes }
     *     
     */
    public void setCustomAttributes(CustomAttributes value) {
        this.customAttributes = value;
    }

    /**
     * Gets the value of the automaticActionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ActionStatus }
     *     
     */
    public ActionStatus getAutomaticActionStatus() {
        return automaticActionStatus;
    }

    /**
     * Sets the value of the automaticActionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionStatus }
     *     
     */
    public void setAutomaticActionStatus(ActionStatus value) {
        this.automaticActionStatus = value;
    }

    /**
     * Gets the value of the operatorActionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ActionStatus }
     *     
     */
    public ActionStatus getOperatorActionStatus() {
        return operatorActionStatus;
    }

    /**
     * Sets the value of the operatorActionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionStatus }
     *     
     */
    public void setOperatorActionStatus(ActionStatus value) {
        this.operatorActionStatus = value;
    }

}
