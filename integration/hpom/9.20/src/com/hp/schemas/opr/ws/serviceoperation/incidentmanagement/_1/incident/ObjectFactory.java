
package com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

  private final static QName _InstructionText_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "InstructionText");
  private final static QName _AcknowledgeCorrelationKey_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "AcknowledgeCorrelationKey");
  private final static QName _Annotation_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "Annotation");
  private final static QName _NumberOfDuplicates_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "NumberOfDuplicates");
  private final static QName _AnnotationText_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "AnnotationText");
  private final static QName _CustomAttribute_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "CustomAttribute");
  private final static QName _UpdateAnnotation_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "UpdateAnnotation");
  private final static QName _Object_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "Object");
  private final static QName _Annotations_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "Annotations");
  private final static QName _OperatorAction_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "OperatorAction");
  private final static QName _ActionStatus_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "ActionStatus");
  private final static QName _OperatorActionStatus_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "OperatorActionStatus");
  private final static QName _EscalationStatus_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "EscalationStatus");
  private final static QName _StateChangeTime_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "StateChangeTime");
  private final static QName _OriginalEvent_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "OriginalEvent");
  private final static QName _IncidentIDs_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "IncidentIDs");
  private final static QName _Application_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "Application");
  private final static QName _ActionType_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "ActionType");
  private final static QName _OperationsExtension_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "OperationsExtension");
  private final static QName _NumberOfAnnotations_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "NumberOfAnnotations");
  private final static QName _CreationTime_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "CreationTime");
  private final static QName _Source_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "Source");
  private final static QName _CorrelationKey_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "CorrelationKey");
  private final static QName _CustomAttributes_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "CustomAttributes");
  private final static QName _AnnotationId_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "AnnotationId");
  private final static QName _AutomaticActionStatus_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "AutomaticActionStatus");
  private final static QName _ReceivedTime_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "ReceivedTime");
  private final static QName _CustomAttributeKey_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "CustomAttributeKey");
  private final static QName _AutomaticAction_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "AutomaticAction");
  private final static QName _ActionCommand_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "ActionCommand");
  private final static QName _Group_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "Group");
  private final static QName _ConditionMatched_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", "ConditionMatched");

  /**
   * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident
   * 
   */
  public ObjectFactory() {
  }

  /**
   * Create an instance of {@link CustomAttribute }
   * 
   */
  public CustomAttribute createCustomAttribute() {
      return new CustomAttribute();
  }

  /**
   * Create an instance of {@link CustomAttributes }
   * 
   */
  public CustomAttributes createCustomAttributes() {
      return new CustomAttributes();
  }

  /**
   * Create an instance of {@link Annotation }
   * 
   */
  public Annotation createAnnotation() {
      return new Annotation();
  }

  /**
   * Create an instance of {@link Annotations }
   * 
   */
  public Annotations createAnnotations() {
      return new Annotations();
  }

  /**
   * Create an instance of {@link OperationsExtension }
   * 
   */
  public OperationsExtension createOperationsExtension() {
      return new OperationsExtension();
  }

  /**
   * Create an instance of {@link IncidentAction }
   * 
   */
  public IncidentAction createIncidentAction() {
      return new IncidentAction();
  }

  /**
   * Create an instance of {@link IncidentIDs }
   * 
   */
  public IncidentIDs createIncidentIDs() {
      return new IncidentIDs();
  }

  /**
   * Create an instance of {@link UpdateAnnotation }
   * 
   */
  public UpdateAnnotation createUpdateAnnotation() {
      return new UpdateAnnotation();
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "InstructionText")
  public JAXBElement<String> createInstructionText(String value) {
      return new JAXBElement<String>(_InstructionText_QNAME, String.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "AcknowledgeCorrelationKey")
  public JAXBElement<String> createAcknowledgeCorrelationKey(String value) {
      return new JAXBElement<String>(_AcknowledgeCorrelationKey_QNAME, String.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link Annotation }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "Annotation")
  public JAXBElement<Annotation> createAnnotation(Annotation value) {
      return new JAXBElement<Annotation>(_Annotation_QNAME, Annotation.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "NumberOfDuplicates")
  public JAXBElement<Long> createNumberOfDuplicates(Long value) {
      return new JAXBElement<Long>(_NumberOfDuplicates_QNAME, Long.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "AnnotationText")
  public JAXBElement<String> createAnnotationText(String value) {
      return new JAXBElement<String>(_AnnotationText_QNAME, String.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link CustomAttribute }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "CustomAttribute")
  public JAXBElement<CustomAttribute> createCustomAttribute(CustomAttribute value) {
      return new JAXBElement<CustomAttribute>(_CustomAttribute_QNAME, CustomAttribute.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link UpdateAnnotation }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "UpdateAnnotation")
  public JAXBElement<UpdateAnnotation> createUpdateAnnotation(UpdateAnnotation value) {
      return new JAXBElement<UpdateAnnotation>(_UpdateAnnotation_QNAME, UpdateAnnotation.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "Object")
  public JAXBElement<String> createObject(String value) {
      return new JAXBElement<String>(_Object_QNAME, String.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link Annotations }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "Annotations")
  public JAXBElement<Annotations> createAnnotations(Annotations value) {
      return new JAXBElement<Annotations>(_Annotations_QNAME, Annotations.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link IncidentAction }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "OperatorAction")
  public JAXBElement<IncidentAction> createOperatorAction(IncidentAction value) {
      return new JAXBElement<IncidentAction>(_OperatorAction_QNAME, IncidentAction.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link ActionStatus }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "ActionStatus")
  public JAXBElement<ActionStatus> createActionStatus(ActionStatus value) {
      return new JAXBElement<ActionStatus>(_ActionStatus_QNAME, ActionStatus.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link ActionStatus }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "OperatorActionStatus")
  public JAXBElement<ActionStatus> createOperatorActionStatus(ActionStatus value) {
      return new JAXBElement<ActionStatus>(_OperatorActionStatus_QNAME, ActionStatus.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link EscalationStatus }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "EscalationStatus")
  public JAXBElement<EscalationStatus> createEscalationStatus(EscalationStatus value) {
      return new JAXBElement<EscalationStatus>(_EscalationStatus_QNAME, EscalationStatus.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "StateChangeTime")
  public JAXBElement<XMLGregorianCalendar> createStateChangeTime(XMLGregorianCalendar value) {
      return new JAXBElement<XMLGregorianCalendar>(_StateChangeTime_QNAME, XMLGregorianCalendar.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "OriginalEvent")
  public JAXBElement<String> createOriginalEvent(String value) {
      return new JAXBElement<String>(_OriginalEvent_QNAME, String.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link IncidentIDs }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "IncidentIDs")
  public JAXBElement<IncidentIDs> createIncidentIDs(IncidentIDs value) {
      return new JAXBElement<IncidentIDs>(_IncidentIDs_QNAME, IncidentIDs.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "Application")
  public JAXBElement<String> createApplication(String value) {
      return new JAXBElement<String>(_Application_QNAME, String.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link ActionType }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "ActionType")
  public JAXBElement<ActionType> createActionType(ActionType value) {
      return new JAXBElement<ActionType>(_ActionType_QNAME, ActionType.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link OperationsExtension }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "OperationsExtension")
  public JAXBElement<OperationsExtension> createOperationsExtension(OperationsExtension value) {
      return new JAXBElement<OperationsExtension>(_OperationsExtension_QNAME, OperationsExtension.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "NumberOfAnnotations")
  public JAXBElement<Long> createNumberOfAnnotations(Long value) {
      return new JAXBElement<Long>(_NumberOfAnnotations_QNAME, Long.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "CreationTime")
  public JAXBElement<XMLGregorianCalendar> createCreationTime(XMLGregorianCalendar value) {
      return new JAXBElement<XMLGregorianCalendar>(_CreationTime_QNAME, XMLGregorianCalendar.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "Source")
  public JAXBElement<String> createSource(String value) {
      return new JAXBElement<String>(_Source_QNAME, String.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "CorrelationKey")
  public JAXBElement<String> createCorrelationKey(String value) {
      return new JAXBElement<String>(_CorrelationKey_QNAME, String.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link CustomAttributes }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "CustomAttributes")
  public JAXBElement<CustomAttributes> createCustomAttributes(CustomAttributes value) {
      return new JAXBElement<CustomAttributes>(_CustomAttributes_QNAME, CustomAttributes.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "AnnotationId")
  public JAXBElement<String> createAnnotationId(String value) {
      return new JAXBElement<String>(_AnnotationId_QNAME, String.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link ActionStatus }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "AutomaticActionStatus")
  public JAXBElement<ActionStatus> createAutomaticActionStatus(ActionStatus value) {
      return new JAXBElement<ActionStatus>(_AutomaticActionStatus_QNAME, ActionStatus.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "ReceivedTime")
  public JAXBElement<XMLGregorianCalendar> createReceivedTime(XMLGregorianCalendar value) {
      return new JAXBElement<XMLGregorianCalendar>(_ReceivedTime_QNAME, XMLGregorianCalendar.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "CustomAttributeKey")
  public JAXBElement<String> createCustomAttributeKey(String value) {
      return new JAXBElement<String>(_CustomAttributeKey_QNAME, String.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link IncidentAction }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "AutomaticAction")
  public JAXBElement<IncidentAction> createAutomaticAction(IncidentAction value) {
      return new JAXBElement<IncidentAction>(_AutomaticAction_QNAME, IncidentAction.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "ActionCommand")
  public JAXBElement<String> createActionCommand(String value) {
      return new JAXBElement<String>(_ActionCommand_QNAME, String.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "Group")
  public JAXBElement<String> createGroup(String value) {
      return new JAXBElement<String>(_Group_QNAME, String.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident", name = "ConditionMatched")
  public JAXBElement<Boolean> createConditionMatched(Boolean value) {
      return new JAXBElement<Boolean>(_ConditionMatched_QNAME, Boolean.class, null, value);
  }
}
