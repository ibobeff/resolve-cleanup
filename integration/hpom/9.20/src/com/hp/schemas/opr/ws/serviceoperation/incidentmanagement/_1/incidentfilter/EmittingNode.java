
package com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmittingNode complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmittingNode">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NodeProperties" type="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/IncidentFilter}NodeProperties"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmittingNode", propOrder = {
    "nodeProperties"
})
public class EmittingNode {

    @XmlElement(name = "NodeProperties", required = true)
    protected NodeProperties nodeProperties;

    /**
     * Gets the value of the nodeProperties property.
     * 
     * @return
     *     possible object is
     *     {@link NodeProperties }
     *     
     */
    public NodeProperties getNodeProperties() {
        return nodeProperties;
    }

    /**
     * Sets the value of the nodeProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link NodeProperties }
     *     
     */
    public void setNodeProperties(NodeProperties value) {
        this.nodeProperties = value;
    }

}
