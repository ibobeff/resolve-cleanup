
package com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmittingCI complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmittingCI">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConfigurationItemProperties" type="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/IncidentFilter}ConfigurationItemProperties"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmittingCI", propOrder = {
    "configurationItemProperties"
})
public class EmittingCI {

    @XmlElement(name = "ConfigurationItemProperties", required = true)
    protected ConfigurationItemProperties configurationItemProperties;

    /**
     * Gets the value of the configurationItemProperties property.
     * 
     * @return
     *     possible object is
     *     {@link ConfigurationItemProperties }
     *     
     */
    public ConfigurationItemProperties getConfigurationItemProperties() {
        return configurationItemProperties;
    }

    /**
     * Sets the value of the configurationItemProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfigurationItemProperties }
     *     
     */
    public void setConfigurationItemProperties(ConfigurationItemProperties value) {
        this.configurationItemProperties = value;
    }

}
