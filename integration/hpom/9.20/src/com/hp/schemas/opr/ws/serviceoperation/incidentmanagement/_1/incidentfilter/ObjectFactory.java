
package com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _IncidentEventingFilter_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/IncidentFilter", "IncidentEventingFilter");
    private final static QName _IncidentEnumerationFilter_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/IncidentFilter", "IncidentEnumerationFilter");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IncidentEventingFilter }
     * 
     */
    public IncidentEventingFilter createIncidentEventingFilter() {
        return new IncidentEventingFilter();
    }

    /**
     * Create an instance of {@link CustomAttributes }
     * 
     */
    public CustomAttributes createCustomAttributes() {
        return new CustomAttributes();
    }

    /**
     * Create an instance of {@link CustomAttribute }
     * 
     */
    public CustomAttribute createCustomAttribute() {
        return new CustomAttribute();
    }

    /**
     * Create an instance of {@link EmittingNode }
     * 
     */
    public EmittingNode createEmittingNode() {
        return new EmittingNode();
    }

    /**
     * Create an instance of {@link NodeProperties }
     * 
     */
    public NodeProperties createNodeProperties() {
        return new NodeProperties();
    }

    /**
     * Create an instance of {@link TimeFilter }
     * 
     */
    public TimeFilter createTimeFilter() {
        return new TimeFilter();
    }

    /**
     * Create an instance of {@link EmittingCI }
     * 
     */
    public EmittingCI createEmittingCI() {
        return new EmittingCI();
    }

    /**
     * Create an instance of {@link ConfigurationItemProperties }
     * 
     */
    public ConfigurationItemProperties createConfigurationItemProperties() {
        return new ConfigurationItemProperties();
    }

    /**
     * Create an instance of {@link KeywordFilter }
     * 
     */
    public KeywordFilter createKeywordFilter() {
        return new KeywordFilter();
    }

    /**
     * Create an instance of {@link IncidentEnumerationFilter }
     * 
     */
    public IncidentEnumerationFilter createIncidentEnumerationFilter() {
        return new IncidentEnumerationFilter();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IncidentEventingFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/IncidentFilter", name = "IncidentEventingFilter")
    public JAXBElement<IncidentEventingFilter> createIncidentEventingFilter(IncidentEventingFilter value) {
        return new JAXBElement<IncidentEventingFilter>(_IncidentEventingFilter_QNAME, IncidentEventingFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IncidentEnumerationFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/IncidentFilter", name = "IncidentEnumerationFilter")
    public JAXBElement<IncidentEnumerationFilter> createIncidentEnumerationFilter(IncidentEnumerationFilter value) {
        return new JAXBElement<IncidentEnumerationFilter>(_IncidentEnumerationFilter_QNAME, IncidentEnumerationFilter.class, null, value);
    }

}
