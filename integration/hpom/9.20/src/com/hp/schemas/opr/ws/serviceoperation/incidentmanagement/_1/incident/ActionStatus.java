
package com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActionStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ActionStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="unknown"/>
 *     &lt;enumeration value="notAvailable"/>
 *     &lt;enumeration value="available"/>
 *     &lt;enumeration value="running"/>
 *     &lt;enumeration value="succeeded"/>
 *     &lt;enumeration value="failed"/>
 *     &lt;enumeration value="discarded"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ActionStatus")
@XmlEnum
public enum ActionStatus {

    @XmlEnumValue("unknown")
    UNKNOWN("unknown"),
    @XmlEnumValue("notAvailable")
    NOT_AVAILABLE("notAvailable"),
    @XmlEnumValue("available")
    AVAILABLE("available"),
    @XmlEnumValue("running")
    RUNNING("running"),
    @XmlEnumValue("succeeded")
    SUCCEEDED("succeeded"),
    @XmlEnumValue("failed")
    FAILED("failed"),
    @XmlEnumValue("discarded")
    DISCARDED("discarded");
    private final String value;

    ActionStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ActionStatus fromValue(String v) {
        for (ActionStatus c: ActionStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
