
package com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IncidentAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IncidentAction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}ActionStatus" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}ActionCommand"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceTransition/ConfigurationManagement/1/Node}DnsName" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}ActionType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IncidentAction", propOrder = {
    "actionStatus",
    "actionCommand",
    "dnsName",
    "actionType"
})
public class IncidentAction {

    @XmlElement(name = "ActionStatus")
    protected ActionStatus actionStatus;
    @XmlElement(name = "ActionCommand", required = true)
    protected String actionCommand;
    @XmlElement(name = "DnsName", namespace = "http://schemas.hp.com/ism/ServiceTransition/ConfigurationManagement/1/Node")
    protected String dnsName;
    @XmlElement(name = "ActionType")
    protected ActionType actionType;

    /**
     * Gets the value of the actionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ActionStatus }
     *     
     */
    public ActionStatus getActionStatus() {
        return actionStatus;
    }

    /**
     * Sets the value of the actionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionStatus }
     *     
     */
    public void setActionStatus(ActionStatus value) {
        this.actionStatus = value;
    }

    /**
     * Gets the value of the actionCommand property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCommand() {
        return actionCommand;
    }

    /**
     * Sets the value of the actionCommand property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCommand(String value) {
        this.actionCommand = value;
    }

    /**
     * Gets the value of the dnsName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDnsName() {
        return dnsName;
    }

    /**
     * Sets the value of the dnsName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDnsName(String value) {
        this.dnsName = value;
    }

    /**
     * Gets the value of the actionType property.
     * 
     * @return
     *     possible object is
     *     {@link ActionType }
     *     
     */
    public ActionType getActionType() {
        return actionType;
    }

    /**
     * Sets the value of the actionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionType }
     *     
     */
    public void setActionType(ActionType value) {
        this.actionType = value;
    }

}
