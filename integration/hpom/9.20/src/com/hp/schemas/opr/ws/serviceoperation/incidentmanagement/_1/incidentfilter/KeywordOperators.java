
package com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for KeywordOperators.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="KeywordOperators">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="exactly"/>
 *     &lt;enumeration value="contains"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "KeywordOperators")
@XmlEnum
public enum KeywordOperators {

    @XmlEnumValue("exactly")
    EXACTLY("exactly"),
    @XmlEnumValue("contains")
    CONTAINS("contains");
    private final String value;

    KeywordOperators(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static KeywordOperators fromValue(String v) {
        for (KeywordOperators c: KeywordOperators.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
