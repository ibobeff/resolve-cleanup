
package com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.change;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.change package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ChangeType_QNAME = new QName("http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident/Change", "ChangeType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.change
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident/Change", name = "ChangeType")
    public JAXBElement<String> createChangeType(String value) {
        return new JAXBElement<String>(_ChangeType_QNAME, String.class, null, value);
    }

}
