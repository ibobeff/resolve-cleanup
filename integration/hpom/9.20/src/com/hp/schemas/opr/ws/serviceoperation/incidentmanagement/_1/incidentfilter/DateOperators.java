
package com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DateOperators.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DateOperators">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="to"/>
 *     &lt;enumeration value="from"/>
 *     &lt;enumeration value="at"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DateOperators")
@XmlEnum
public enum DateOperators {

    @XmlEnumValue("to")
    TO("to"),
    @XmlEnumValue("from")
    FROM("from"),
    @XmlEnumValue("at")
    AT("at");
    private final String value;

    DateOperators(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DateOperators fromValue(String v) {
        for (DateOperators c: DateOperators.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
