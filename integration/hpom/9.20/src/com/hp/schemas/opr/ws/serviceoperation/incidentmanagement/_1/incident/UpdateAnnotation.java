
package com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateAnnotation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateAnnotation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}AnnotationId"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}AnnotationText"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateAnnotation", propOrder = {
    "annotationId",
    "annotationText"
})
public class UpdateAnnotation {

    @XmlElement(name = "AnnotationId", required = true)
    protected String annotationId;
    @XmlElement(name = "AnnotationText", required = true)
    protected String annotationText;

    /**
     * Gets the value of the annotationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnotationId() {
        return annotationId;
    }

    /**
     * Sets the value of the annotationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnotationId(String value) {
        this.annotationId = value;
    }

    /**
     * Gets the value of the annotationText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnotationText() {
        return annotationText;
    }

    /**
     * Sets the value of the annotationText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnotationText(String value) {
        this.annotationText = value;
    }

}
