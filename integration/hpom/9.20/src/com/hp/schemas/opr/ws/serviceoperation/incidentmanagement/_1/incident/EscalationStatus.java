
package com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EscalationStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EscalationStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="notEscalated"/>
 *     &lt;enumeration value="escalationReceived"/>
 *     &lt;enumeration value="escalationSent"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EscalationStatus")
@XmlEnum
public enum EscalationStatus {

    @XmlEnumValue("notEscalated")
    NOT_ESCALATED("notEscalated"),
    @XmlEnumValue("escalationReceived")
    ESCALATION_RECEIVED("escalationReceived"),
    @XmlEnumValue("escalationSent")
    ESCALATION_SENT("escalationSent");
    private final String value;

    EscalationStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EscalationStatus fromValue(String v) {
        for (EscalationStatus c: EscalationStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
