
package com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incidentextensions.IncidentExtensionsType;


/**
 * <p>Java class for Incident complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Incident">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident}IncidentBase">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident}Extensions" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Incident", propOrder = {
    "extensions"
})
public class Incident
    extends IncidentBase
{

    @XmlElement(name = "Extensions")
    protected IncidentExtensionsType extensions;

    /**
     * Gets the value of the extensions property.
     * 
     * @return
     *     possible object is
     *     {@link IncidentExtensionsType }
     *     
     */
    public IncidentExtensionsType getExtensions() {
        return extensions;
    }

    /**
     * Sets the value of the extensions property.
     * 
     * @param value
     *     allowed object is
     *     {@link IncidentExtensionsType }
     *     
     */
    public void setExtensions(IncidentExtensionsType value) {
        this.extensions = value;
    }

}
