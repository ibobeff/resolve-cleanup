
package com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProblemType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ProblemType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="availability"/>
 *     &lt;enumeration value="performance"/>
 *     &lt;enumeration value="other"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ProblemType")
@XmlEnum
public enum ProblemType {

    @XmlEnumValue("availability")
    AVAILABILITY("availability"),
    @XmlEnumValue("performance")
    PERFORMANCE("performance"),
    @XmlEnumValue("other")
    OTHER("other");
    private final String value;

    ProblemType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ProblemType fromValue(String v) {
        for (ProblemType c: ProblemType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
