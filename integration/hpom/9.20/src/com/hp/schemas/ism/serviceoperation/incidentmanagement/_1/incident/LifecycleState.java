
package com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LifecycleState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LifecycleState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="work in progress"/>
 *     &lt;enumeration value="open"/>
 *     &lt;enumeration value="closed"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LifecycleState")
@XmlEnum
public enum LifecycleState {

    @XmlEnumValue("work in progress")
    WORK_IN_PROGRESS("work in progress"),
    @XmlEnumValue("open")
    OPEN("open"),
    @XmlEnumValue("closed")
    CLOSED("closed");
    private final String value;

    LifecycleState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LifecycleState fromValue(String v) {
        for (LifecycleState c: LifecycleState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
