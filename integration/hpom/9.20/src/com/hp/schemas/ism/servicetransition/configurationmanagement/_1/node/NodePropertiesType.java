
package com.hp.schemas.ism.servicetransition.configurationmanagement._1.node;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.hp.schemas.ism.servicetransition.configurationmanagement._1.configurationitem.ConfigurationItemPropertiesType;
import org.w3c.dom.Element;


/**
 * <p>Java class for NodePropertiesType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NodePropertiesType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.hp.com/ism/ServiceTransition/ConfigurationManagement/1/ConfigurationItem}ConfigurationItemPropertiesType">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceTransition/ConfigurationManagement/1/Node}ShortHostname" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceTransition/ConfigurationManagement/1/Node}PrimaryDnsDomainName" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceTransition/ConfigurationManagement/1/Node}NetworkAddress" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceTransition/ConfigurationManagement/1/Node}DnsName" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceTransition/ConfigurationManagement/1/Node}CoreId" minOccurs="0"/>
 *         &lt;element name="ExtensionsSeparator" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;any/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NodePropertiesType", propOrder = {
    "shortHostname",
    "primaryDnsDomainName",
    "networkAddress",
    "dnsName",
    "coreId",
    "extensionsSeparator",
    "any"
})
public class NodePropertiesType
    extends ConfigurationItemPropertiesType
{

    @XmlElement(name = "ShortHostname")
    protected String shortHostname;
    @XmlElement(name = "PrimaryDnsDomainName")
    protected String primaryDnsDomainName;
    @XmlElement(name = "NetworkAddress")
    protected String networkAddress;
    @XmlElement(name = "DnsName")
    protected String dnsName;
    @XmlElement(name = "CoreId")
    protected String coreId;
    @XmlElement(name = "ExtensionsSeparator", required = true)
    protected Object extensionsSeparator;
    @XmlAnyElement(lax = true)
    protected Object any;

    /**
     * Gets the value of the shortHostname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShortHostname() {
        return shortHostname;
    }

    /**
     * Sets the value of the shortHostname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShortHostname(String value) {
        this.shortHostname = value;
    }

    /**
     * Gets the value of the primaryDnsDomainName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryDnsDomainName() {
        return primaryDnsDomainName;
    }

    /**
     * Sets the value of the primaryDnsDomainName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryDnsDomainName(String value) {
        this.primaryDnsDomainName = value;
    }

    /**
     * Gets the value of the networkAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkAddress() {
        return networkAddress;
    }

    /**
     * Sets the value of the networkAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkAddress(String value) {
        this.networkAddress = value;
    }

    /**
     * Gets the value of the dnsName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDnsName() {
        return dnsName;
    }

    /**
     * Sets the value of the dnsName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDnsName(String value) {
        this.dnsName = value;
    }

    /**
     * Gets the value of the coreId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoreId() {
        return coreId;
    }

    /**
     * Sets the value of the coreId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoreId(String value) {
        this.coreId = value;
    }

    /**
     * Gets the value of the extensionsSeparator property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getExtensionsSeparator() {
        return extensionsSeparator;
    }

    /**
     * Sets the value of the extensionsSeparator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setExtensionsSeparator(Object value) {
        this.extensionsSeparator = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * @return
     *     possible object is
     *     {@link Element }
     *     {@link Object }
     *     
     */
    public Object getAny() {
        return any;
    }

    /**
     * Sets the value of the any property.
     * 
     * @param value
     *     allowed object is
     *     {@link Element }
     *     {@link Object }
     *     
     */
    public void setAny(Object value) {
        this.any = value;
    }

}
