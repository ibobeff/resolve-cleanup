
package com.hp.schemas.ism.serviceoperation.common._1.workitem;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.hp.schemas.ism.common._1.common.Extensions;


/**
 * <p>Java class for WorkItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WorkItem">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.hp.com/ism/ServiceOperation/Common/1/WorkItem}WorkItemBase">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.hp.com/ism/Common/1/Common}Extensions"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkItem", propOrder = {
    "extensions"
})
public abstract class WorkItem
    extends WorkItemBase
{

    @XmlElement(name = "Extensions", namespace = "http://schemas.hp.com/ism/Common/1/Common", required = true)
    protected Extensions extensions;

    /**
     * Gets the value of the extensions property.
     * 
     * @return
     *     possible object is
     *     {@link Extensions }
     *     
     */
    public Extensions getExtensions() {
        return extensions;
    }

    /**
     * Sets the value of the extensions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Extensions }
     *     
     */
    public void setExtensions(Extensions value) {
        this.extensions = value;
    }

}
