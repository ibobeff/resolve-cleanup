
package com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incidentextensions;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.OperationsExtension;


/**
 * <p>Java class for IncidentExtensionsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IncidentExtensionsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident}OperationsExtension" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident/Change}ChangeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IncidentExtensionsType", propOrder = {
    "operationsExtension",
    "changeType"
})
public class IncidentExtensionsType {

    @XmlElement(name = "OperationsExtension", namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident")
    protected OperationsExtension operationsExtension;
    @XmlElement(name = "ChangeType", namespace = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/Incident/Change")
    protected String changeType;
    
    /**
     * Gets the value of the operationsExtension property.
     * 
     * @return
     *     possible object is
     *     {@link OperationsExtension }
     *     
     */
    public OperationsExtension getOperationsExtension() {
        return operationsExtension;
    }

    /**
     * Sets the value of the operationsExtension property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationsExtension }
     *     
     */
    public void setOperationsExtension(OperationsExtension value) {
        this.operationsExtension = value;
    }

    /**
     * Gets the value of the changeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeType() {
        return changeType;
    }

    /**
     * Sets the value of the changeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeType(String value) {
        this.changeType = value;
    }
}
