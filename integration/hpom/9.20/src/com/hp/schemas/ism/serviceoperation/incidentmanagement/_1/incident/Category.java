
package com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Category.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Category">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="network"/>
 *     &lt;enumeration value="shared"/>
 *     &lt;enumeration value="security"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Category")
@XmlEnum
public enum Category {

    @XmlEnumValue("network")
    NETWORK("network"),
    @XmlEnumValue("shared")
    SHARED("shared"),
    @XmlEnumValue("security")
    SECURITY("security");
    private final String value;

    Category(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Category fromValue(String v) {
        for (Category c: Category.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
