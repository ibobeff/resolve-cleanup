
package com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CollaborationMode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CollaborationMode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="fyi"/>
 *     &lt;enumeration value="peer"/>
 *     &lt;enumeration value="incident_exchange"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CollaborationMode")
@XmlEnum
public enum CollaborationMode {

    @XmlEnumValue("fyi")
    FYI("fyi"),
    @XmlEnumValue("peer")
    PEER("peer"),
    @XmlEnumValue("incident_exchange")
    INCIDENT_EXCHANGE("incident_exchange");
    private final String value;

    CollaborationMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CollaborationMode fromValue(String v) {
        for (CollaborationMode c: CollaborationMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
