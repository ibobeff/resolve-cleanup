
package com.hp.schemas.ism.servicetransition.configurationmanagement._1.configurationitem;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.xmlsoap.schemas.ws._2004._08.addressing.EndpointReferenceType;


/**
 * <p>Java class for ConfigurationItemReferenceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConfigurationItemReferenceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.xmlsoap.org/ws/2004/08/addressing}EndpointReference" minOccurs="0"/>
 *         &lt;element name="ConfigurationItemProperties" type="{http://schemas.hp.com/ism/ServiceTransition/ConfigurationManagement/1/ConfigurationItem}ConfigurationItemPropertiesType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConfigurationItemReferenceType", propOrder = {
    "endpointReference",
    "configurationItemProperties"
})
public class ConfigurationItemReferenceType {

    @XmlElement(name = "EndpointReference", namespace = "http://schemas.xmlsoap.org/ws/2004/08/addressing")
    protected EndpointReferenceType endpointReference;
    @XmlElement(name = "ConfigurationItemProperties")
    protected ConfigurationItemPropertiesType configurationItemProperties;

    /**
     * Gets the value of the endpointReference property.
     * 
     * @return
     *     possible object is
     *     {@link EndpointReferenceType }
     *     
     */
    public EndpointReferenceType getEndpointReference() {
        return endpointReference;
    }

    /**
     * Sets the value of the endpointReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link EndpointReferenceType }
     *     
     */
    public void setEndpointReference(EndpointReferenceType value) {
        this.endpointReference = value;
    }

    /**
     * Gets the value of the configurationItemProperties property.
     * 
     * @return
     *     possible object is
     *     {@link ConfigurationItemPropertiesType }
     *     
     */
    public ConfigurationItemPropertiesType getConfigurationItemProperties() {
        return configurationItemProperties;
    }

    /**
     * Sets the value of the configurationItemProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfigurationItemPropertiesType }
     *     
     */
    public void setConfigurationItemProperties(ConfigurationItemPropertiesType value) {
        this.configurationItemProperties = value;
    }

}
