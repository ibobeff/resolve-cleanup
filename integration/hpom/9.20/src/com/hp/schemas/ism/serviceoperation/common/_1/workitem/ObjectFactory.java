
package com.hp.schemas.ism.serviceoperation.common._1.workitem;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.hp.schemas.ism.serviceoperation.common._1.workitem package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AssignedOperator_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/Common/1/WorkItem", "AssignedOperator");
    private final static QName _PrimaryAssignmentGroup_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/Common/1/WorkItem", "PrimaryAssignmentGroup");
    private final static QName _WorkItem_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/Common/1/WorkItem", "WorkItem");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.hp.schemas.ism.serviceoperation.common._1.workitem
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AssignedOperator.PersonAttributes }
     * 
     */
    public AssignedOperator.PersonAttributes createAssignedOperatorPersonAttributes() {
        return new AssignedOperator.PersonAttributes();
    }

    /**
     * Create an instance of {@link AssignedOperator }
     * 
     */
    public AssignedOperator createAssignedOperator() {
        return new AssignedOperator();
    }

    /**
     * Create an instance of {@link PrimaryAssignmentGroup }
     * 
     */
    public PrimaryAssignmentGroup createPrimaryAssignmentGroup() {
        return new PrimaryAssignmentGroup();
    }

    /**
     * Create an instance of {@link PrimaryAssignmentGroup.SupportGroupAttributes }
     * 
     */
    public PrimaryAssignmentGroup.SupportGroupAttributes createPrimaryAssignmentGroupSupportGroupAttributes() {
        return new PrimaryAssignmentGroup.SupportGroupAttributes();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AssignedOperator }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/Common/1/WorkItem", name = "AssignedOperator")
    public JAXBElement<AssignedOperator> createAssignedOperator(AssignedOperator value) {
        return new JAXBElement<AssignedOperator>(_AssignedOperator_QNAME, AssignedOperator.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PrimaryAssignmentGroup }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/Common/1/WorkItem", name = "PrimaryAssignmentGroup")
    public JAXBElement<PrimaryAssignmentGroup> createPrimaryAssignmentGroup(PrimaryAssignmentGroup value) {
        return new JAXBElement<PrimaryAssignmentGroup>(_PrimaryAssignmentGroup_QNAME, PrimaryAssignmentGroup.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WorkItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/Common/1/WorkItem", name = "WorkItem")
    public JAXBElement<WorkItem> createWorkItem(WorkItem value) {
        return new JAXBElement<WorkItem>(_WorkItem_QNAME, WorkItem.class, null, value);
    }

}
