
package com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.hp.schemas.ism.serviceoperation.common._1.workitem.WorkItemBase;
import com.hp.schemas.ism.servicetransition.configurationmanagement._1.configurationitem.ConfigurationItemReferenceType;
import com.hp.schemas.ism.servicetransition.configurationmanagement._1.node.NodeReferenceType;


/**
 * <p>Java class for IncidentBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IncidentBase">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.hp.com/ism/ServiceOperation/Common/1/WorkItem}WorkItemBase">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident}IncidentID" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident}Title" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident}Description" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident}LifecycleState" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident}Severity" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident}Solution" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident}Category" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident}SubCategory" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident}ProductType" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident}ProblemType" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident}CollaborationMode" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident}AffectedCI" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident}EmittingCI" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident}EmittingNode" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident}RequesterReference" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident}Type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IncidentBase", propOrder = {
    "incidentID",
    "title",
    "description",
    "lifecycleState",
    "severity",
    "solution",
    "category",
    "subCategory",
    "productType",
    "problemType",
    "collaborationMode",
    "affectedCI",
    "emittingCI",
    "emittingNode",
    "requesterReference",
    "type"
})
@XmlSeeAlso({
    Incident.class
})
public class IncidentBase
    extends WorkItemBase
{

    @XmlElement(name = "IncidentID")
    protected String incidentID;
    @XmlElement(name = "Title")
    protected String title;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "LifecycleState")
    protected String lifecycleState;
    @XmlElement(name = "Severity")
    protected String severity;
    @XmlElement(name = "Solution")
    protected String solution;
    @XmlElement(name = "Category")
    protected String category;
    @XmlElement(name = "SubCategory")
    protected String subCategory;
    @XmlElement(name = "ProductType")
    protected String productType;
    @XmlElement(name = "ProblemType")
    protected String problemType;
    @XmlElement(name = "CollaborationMode")
    protected String collaborationMode;
    @XmlElement(name = "AffectedCI")
    protected ConfigurationItemReferenceType affectedCI;
    @XmlElement(name = "EmittingCI")
    protected ConfigurationItemReferenceType emittingCI;
    @XmlElement(name = "EmittingNode")
    protected NodeReferenceType emittingNode;
    @XmlElement(name = "RequesterReference")
    protected RequesterReference requesterReference;
    @XmlElement(name = "Type")
    protected String type;

    /**
     * Gets the value of the incidentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncidentID() {
        return incidentID;
    }

    /**
     * Sets the value of the incidentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncidentID(String value) {
        this.incidentID = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the lifecycleState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLifecycleState() {
        return lifecycleState;
    }

    /**
     * Sets the value of the lifecycleState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLifecycleState(String value) {
        this.lifecycleState = value;
    }

    /**
     * Gets the value of the severity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeverity() {
        return severity;
    }

    /**
     * Sets the value of the severity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeverity(String value) {
        this.severity = value;
    }

    /**
     * Gets the value of the solution property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSolution() {
        return solution;
    }

    /**
     * Sets the value of the solution property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSolution(String value) {
        this.solution = value;
    }

    /**
     * Gets the value of the category property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategory() {
        return category;
    }

    /**
     * Sets the value of the category property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategory(String value) {
        this.category = value;
    }

    /**
     * Gets the value of the subCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubCategory() {
        return subCategory;
    }

    /**
     * Sets the value of the subCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubCategory(String value) {
        this.subCategory = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Gets the value of the problemType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProblemType() {
        return problemType;
    }

    /**
     * Sets the value of the problemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProblemType(String value) {
        this.problemType = value;
    }

    /**
     * Gets the value of the collaborationMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollaborationMode() {
        return collaborationMode;
    }

    /**
     * Sets the value of the collaborationMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollaborationMode(String value) {
        this.collaborationMode = value;
    }

    /**
     * Gets the value of the affectedCI property.
     * 
     * @return
     *     possible object is
     *     {@link ConfigurationItemReferenceType }
     *     
     */
    public ConfigurationItemReferenceType getAffectedCI() {
        return affectedCI;
    }

    /**
     * Sets the value of the affectedCI property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfigurationItemReferenceType }
     *     
     */
    public void setAffectedCI(ConfigurationItemReferenceType value) {
        this.affectedCI = value;
    }

    /**
     * Gets the value of the emittingCI property.
     * 
     * @return
     *     possible object is
     *     {@link ConfigurationItemReferenceType }
     *     
     */
    public ConfigurationItemReferenceType getEmittingCI() {
        return emittingCI;
    }

    /**
     * Sets the value of the emittingCI property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfigurationItemReferenceType }
     *     
     */
    public void setEmittingCI(ConfigurationItemReferenceType value) {
        this.emittingCI = value;
    }

    /**
     * Gets the value of the emittingNode property.
     * 
     * @return
     *     possible object is
     *     {@link NodeReferenceType }
     *     
     */
    public NodeReferenceType getEmittingNode() {
        return emittingNode;
    }

    /**
     * Sets the value of the emittingNode property.
     * 
     * @param value
     *     allowed object is
     *     {@link NodeReferenceType }
     *     
     */
    public void setEmittingNode(NodeReferenceType value) {
        this.emittingNode = value;
    }

    /**
     * Gets the value of the requesterReference property.
     * 
     * @return
     *     possible object is
     *     {@link RequesterReference }
     *     
     */
    public RequesterReference getRequesterReference() {
        return requesterReference;
    }

    /**
     * Sets the value of the requesterReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequesterReference }
     *     
     */
    public void setRequesterReference(RequesterReference value) {
        this.requesterReference = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

}
