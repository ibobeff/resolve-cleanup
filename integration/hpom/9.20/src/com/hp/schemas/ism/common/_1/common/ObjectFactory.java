
package com.hp.schemas.ism.common._1.common;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.hp.schemas.ism.common._1.common package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Extensions_QNAME = new QName("http://schemas.hp.com/ism/Common/1/Common", "Extensions");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.hp.schemas.ism.common._1.common
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Extensions }
     * 
     */
    public Extensions createExtensions() {
        return new Extensions();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Extensions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/Common/1/Common", name = "Extensions")
    public JAXBElement<Extensions> createExtensions(Extensions value) {
        return new JAXBElement<Extensions>(_Extensions_QNAME, Extensions.class, null, value);
    }

}
