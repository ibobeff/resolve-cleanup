
package com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incidentextensions.IncidentExtensionsType;
import com.hp.schemas.ism.servicetransition.configurationmanagement._1.configurationitem.ConfigurationItemReferenceType;
import com.hp.schemas.ism.servicetransition.configurationmanagement._1.node.NodeReferenceType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AffectedCI_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "AffectedCI");
    private final static QName _Incident_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Incident");
    private final static QName _IncidentID_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "IncidentID");
    private final static QName _Severity_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Severity");
    private final static QName _Extensions_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Extensions");
    private final static QName _Type_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Type");
    private final static QName _SubCategory_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "SubCategory");
    private final static QName _RequesterReference_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "RequesterReference");
    private final static QName _LifecycleState_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "LifecycleState");
    private final static QName _EmittingCI_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "EmittingCI");
    private final static QName _ProductType_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "ProductType");
    private final static QName _Title_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Title");
    private final static QName _Category_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Category");
    private final static QName _Solution_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Solution");
    private final static QName _Description_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "Description");
    private final static QName _CollaborationMode_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "CollaborationMode");
    private final static QName _ResourceID_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "ResourceID");
    private final static QName _ProblemType_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "ProblemType");
    private final static QName _EmittingNode_QNAME = new QName("http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", "EmittingNode");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IncidentBase }
     * 
     */
    public IncidentBase createIncidentBase() {
        return new IncidentBase();
    }

    /**
     * Create an instance of {@link Incident }
     * 
     */
    public Incident createIncident() {
        return new Incident();
    }

    /**
     * Create an instance of {@link RequesterReference }
     * 
     */
    public RequesterReference createRequesterReference() {
        return new RequesterReference();
    }

    /**
     * Create an instance of {@link RequesterReference.IncidentAttributes }
     * 
     */
    public RequesterReference.IncidentAttributes createRequesterReferenceIncidentAttributes() {
        return new RequesterReference.IncidentAttributes();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfigurationItemReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", name = "AffectedCI")
    public JAXBElement<ConfigurationItemReferenceType> createAffectedCI(ConfigurationItemReferenceType value) {
        return new JAXBElement<ConfigurationItemReferenceType>(_AffectedCI_QNAME, ConfigurationItemReferenceType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Incident }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", name = "Incident")
    public JAXBElement<Incident> createIncident(Incident value) {
        return new JAXBElement<Incident>(_Incident_QNAME, Incident.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", name = "IncidentID")
    public JAXBElement<String> createIncidentID(String value) {
        return new JAXBElement<String>(_IncidentID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", name = "Severity")
    public JAXBElement<String> createSeverity(String value) {
        return new JAXBElement<String>(_Severity_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IncidentExtensionsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", name = "Extensions")
    public JAXBElement<IncidentExtensionsType> createExtensions(IncidentExtensionsType value) {
        return new JAXBElement<IncidentExtensionsType>(_Extensions_QNAME, IncidentExtensionsType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", name = "Type")
    public JAXBElement<String> createType(String value) {
        return new JAXBElement<String>(_Type_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", name = "SubCategory")
    public JAXBElement<String> createSubCategory(String value) {
        return new JAXBElement<String>(_SubCategory_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequesterReference }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", name = "RequesterReference")
    public JAXBElement<RequesterReference> createRequesterReference(RequesterReference value) {
        return new JAXBElement<RequesterReference>(_RequesterReference_QNAME, RequesterReference.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", name = "LifecycleState")
    public JAXBElement<String> createLifecycleState(String value) {
        return new JAXBElement<String>(_LifecycleState_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfigurationItemReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", name = "EmittingCI")
    public JAXBElement<ConfigurationItemReferenceType> createEmittingCI(ConfigurationItemReferenceType value) {
        return new JAXBElement<ConfigurationItemReferenceType>(_EmittingCI_QNAME, ConfigurationItemReferenceType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", name = "ProductType")
    public JAXBElement<String> createProductType(String value) {
        return new JAXBElement<String>(_ProductType_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", name = "Title")
    public JAXBElement<String> createTitle(String value) {
        return new JAXBElement<String>(_Title_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", name = "Category")
    public JAXBElement<String> createCategory(String value) {
        return new JAXBElement<String>(_Category_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", name = "Solution")
    public JAXBElement<String> createSolution(String value) {
        return new JAXBElement<String>(_Solution_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", name = "Description")
    public JAXBElement<String> createDescription(String value) {
        return new JAXBElement<String>(_Description_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", name = "CollaborationMode")
    public JAXBElement<String> createCollaborationMode(String value) {
        return new JAXBElement<String>(_CollaborationMode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", name = "ResourceID")
    public JAXBElement<String> createResourceID(String value) {
        return new JAXBElement<String>(_ResourceID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", name = "ProblemType")
    public JAXBElement<String> createProblemType(String value) {
        return new JAXBElement<String>(_ProblemType_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NodeReferenceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident", name = "EmittingNode")
    public JAXBElement<NodeReferenceType> createEmittingNode(NodeReferenceType value) {
        return new JAXBElement<NodeReferenceType>(_EmittingNode_QNAME, NodeReferenceType.class, null, value);
    }

}
