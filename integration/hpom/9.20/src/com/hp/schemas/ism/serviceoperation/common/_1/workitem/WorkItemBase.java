
package com.hp.schemas.ism.serviceoperation.common._1.workitem;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.IncidentBase;


/**
 * <p>Java class for WorkItemBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WorkItemBase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceOperation/Common/1/WorkItem}AssignedOperator" minOccurs="0"/>
 *         &lt;element ref="{http://schemas.hp.com/ism/ServiceOperation/Common/1/WorkItem}PrimaryAssignmentGroup" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkItemBase", propOrder = {
    "assignedOperator",
    "primaryAssignmentGroup"
})
@XmlSeeAlso({
    IncidentBase.class,
    WorkItem.class
})
public abstract class WorkItemBase {

    @XmlElement(name = "AssignedOperator")
    protected AssignedOperator assignedOperator;
    @XmlElement(name = "PrimaryAssignmentGroup")
    protected PrimaryAssignmentGroup primaryAssignmentGroup;

    /**
     * Gets the value of the assignedOperator property.
     * 
     * @return
     *     possible object is
     *     {@link AssignedOperator }
     *     
     */
    public AssignedOperator getAssignedOperator() {
        return assignedOperator;
    }

    /**
     * Sets the value of the assignedOperator property.
     * 
     * @param value
     *     allowed object is
     *     {@link AssignedOperator }
     *     
     */
    public void setAssignedOperator(AssignedOperator value) {
        this.assignedOperator = value;
    }

    /**
     * Gets the value of the primaryAssignmentGroup property.
     * 
     * @return
     *     possible object is
     *     {@link PrimaryAssignmentGroup }
     *     
     */
    public PrimaryAssignmentGroup getPrimaryAssignmentGroup() {
        return primaryAssignmentGroup;
    }

    /**
     * Sets the value of the primaryAssignmentGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrimaryAssignmentGroup }
     *     
     */
    public void setPrimaryAssignmentGroup(PrimaryAssignmentGroup value) {
        this.primaryAssignmentGroup = value;
    }

}
