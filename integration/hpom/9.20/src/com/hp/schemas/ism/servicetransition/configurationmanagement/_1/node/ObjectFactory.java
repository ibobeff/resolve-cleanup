
package com.hp.schemas.ism.servicetransition.configurationmanagement._1.node;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.hp.schemas.ism.servicetransition.configurationmanagement._1.node package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _NetworkAddress_QNAME = new QName("http://schemas.hp.com/ism/ServiceTransition/ConfigurationManagement/1/Node", "NetworkAddress");
    private final static QName _DnsName_QNAME = new QName("http://schemas.hp.com/ism/ServiceTransition/ConfigurationManagement/1/Node", "DnsName");
    private final static QName _PrimaryDnsDomainName_QNAME = new QName("http://schemas.hp.com/ism/ServiceTransition/ConfigurationManagement/1/Node", "PrimaryDnsDomainName");
    private final static QName _CoreId_QNAME = new QName("http://schemas.hp.com/ism/ServiceTransition/ConfigurationManagement/1/Node", "CoreId");
    private final static QName _ShortHostname_QNAME = new QName("http://schemas.hp.com/ism/ServiceTransition/ConfigurationManagement/1/Node", "ShortHostname");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.hp.schemas.ism.servicetransition.configurationmanagement._1.node
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link NodeReferenceType }
     * 
     */
    public NodeReferenceType createNodeReferenceType() {
        return new NodeReferenceType();
    }

    /**
     * Create an instance of {@link NodePropertiesType }
     * 
     */
    public NodePropertiesType createNodePropertiesType() {
        return new NodePropertiesType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceTransition/ConfigurationManagement/1/Node", name = "NetworkAddress")
    public JAXBElement<String> createNetworkAddress(String value) {
        return new JAXBElement<String>(_NetworkAddress_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceTransition/ConfigurationManagement/1/Node", name = "DnsName")
    public JAXBElement<String> createDnsName(String value) {
        return new JAXBElement<String>(_DnsName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceTransition/ConfigurationManagement/1/Node", name = "PrimaryDnsDomainName")
    public JAXBElement<String> createPrimaryDnsDomainName(String value) {
        return new JAXBElement<String>(_PrimaryDnsDomainName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceTransition/ConfigurationManagement/1/Node", name = "CoreId")
    public JAXBElement<String> createCoreId(String value) {
        return new JAXBElement<String>(_CoreId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ism/ServiceTransition/ConfigurationManagement/1/Node", name = "ShortHostname")
    public JAXBElement<String> createShortHostname(String value) {
        return new JAXBElement<String>(_ShortHostname_QNAME, String.class, null, value);
    }

}
