insert into link_definitions values ('ResolveResults','$custom_info.Resolve.actionResults','Resolve Results');
update alert_column_names set representation = "link.ResolveResults" where internal_name = "custom_info.Resolve.actionResults";
update situation_column_names set representation = "link.ResolveResults" where internal_name = "custom_info.Resolve.actionResults";

insert into sitroom_plugins values("Resolve","custom_info.Resolve.actionResults","ResolveResults","/images/resolve_small.png");
