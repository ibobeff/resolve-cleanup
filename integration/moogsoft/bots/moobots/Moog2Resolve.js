
/************************************************************
 *              Copyright (c) Moogsoft Inc 2015             *
 *                                                          *
 *----------------------------------------------------------*
 *                                                          *
 * The contents of this configuration file may be copied,   *
 * amended and used to create derivative works.             *
 *                                                          *
 ************************************************************/

//---------------------------------------------------------------
//
// Empty moobot implementation to allow alerts and situations
// to be sento to ReolveSystems Resolve  via RESTful POST
//
//---------------------------------------------------------------

//
// Load the minimum required modules
//

var proc        = MooBot.loadModule('Process');
var events      = MooBot.loadModule('Events');
var logger      = MooBot.loadModule('Logger');
var constants   = MooBot.loadModule('Constants');
var moogdb      = MooBot.loadModule('MoogDb.V2');
var rest        = MooBot.loadModule('REST.V2');


// Accept:
// 1. New and updated alerts (to allow for upstream moobot changes).
// 2. New and updated situations (to allow for upstream moobot changes).
//

// Situations.
events.onEvent("resolveSituation",constants.eventType("Sig")).listen();
events.onEvent("resolveSituation",constants.eventType("SigUpdate")).listen();


// Alerts
events.onEvent("resolveAlert",constants.eventType("Alert")).listen();
events.onEvent("resolveAlert",constants.eventType("AlertUpdate")).listen();

//
// Global variables
//

// var resolveURL="http://demo.resolvesys.com:9000/moog";

var resolveURI = "https://<host>:<port>/<filter_id>";
var MOOBOT_NAME="Moog2Resolve::";
var requestTimeout = 10;

// Set forward = true to forward the passed object after sending to Resolve
// or encountering an error.
// If any other moolet has a process_output_of including this moolet (Moog2Reolve) then
// set to true.

// If forwarding to a specific moolet, then modify the call to alert.forward(this) to specify
// the onward moolet name.

var forwardAlerts = false;
var forwardSituations = false;

// ----------------------------------------------------------------------
// Moobot functions
// - resolveAlert(alert)
// - resolveSituation(situaiton)
// - createResolvePayload(obj,value list)
// ----------------------------------------------------------------------

//
// Alert -> Resolve
//

function resolveAlert(alert)  {

	var NAME = MOOBOT_NAME + "resolveAlert: ";

	var currentCustomInfo = alert.getCustomInfo();

	// No custom info - set to empty and update the alert.
	if ( !currentCustomInfo ) {
		currentCustomInfo = {};
		alert.setCustomInfo(currentCustomInfo);
		moogdb.setAlertCustomInfo(alert.value("alert_id"),{},false);
	}

	//
	// Blank Resolve Custom Info object
	//

	var resolveCustomInfo =  {
		Resolve : {
			actionStatus : false,
			actionResults : "",
			actionSummary : ""
		}
	};

	// If there is no current Resolve object, add the default blank one.

	if ( !currentCustomInfo.Resolve ) {
		alert.setCustomInfoValue("Resolve",resolveCustomInfo.Resolve);
	}

	// Update the alert and the database object, but only if the current value is false / null
	// Use a db custom info merge to prevent updating the entire custom_info object
	// This pattern minimises the potential race conditions when setting custom_info.

	// MOOG-5889 Workaround
	alert.setCustomInfo(alert.getCustomInfo());
	currentCustomInfo = alert.getCustomInfo();

	// By default only forward an alert to Resolve on it's first occurrence,

	if ( !currentCustomInfo.Resolve.actionStatus ) {

		// No current status, eligible to send to Resolve.

		// Build an alert object to send.
		// Use an array of values, custom_info values can be included, e.g.
		// var alertParams=[ "alert_id", "source","class","type","description","custom_info.somekey" ];

		var alertParams=[ "alert_id", "source","class","type","description","severity" ] ;
		var resolvePayload = createResolvePayload(alert,alertParams);

		// Add additional logic here to modify the data sent to Resolve
		// e.g.
		// if ( alert.value("severity") === 5 )  {
		//		resolvePayload.priority = 5;
		// }

		if ( !resolvePayload ) {
			logger.warning(NAME + "Failed to generate a suitable object to pass to Resolve");
			if ( forwardAlerts ) {
				alert.forward(this);
				return;
			}
		}

		// Async request with callbacks
		// return and update the object accordingly.

		var resolveURL = resolveURI + "?alert_id=" + alert.value("alert_id");

		var resolvePostParams={
			"url" : resolveURL,
			"body" : resolvePayload,
			"disable_certificate_validation" : true,
			"content_type" : "application/json",
			"timeout" : requestTimeout,
			"success" : function(req,res) {

				// Update the alert with a pending resolve status
				// Pre-define where results can be found

				alert.setCustomInfoValue("Resolve.actionStatus","pending");
				alert.setCustomInfoValue("Resolve.actionResults",resolveURL);

				resolveCustomInfo.Resolve.actionStatus = "pending";
				resolveCustomInfo.Resolve.actionResults = resolveURL;

				// Update the Resolve key in custom_info.
				moogdb.setAlertCustomInfo(alert.value("alert_id"), resolveCustomInfo ,true);

				logger.info(NAME + "Alert: " + alert.value("alert_id") + " : Successfully sent alert to Resolve");

				// Forward if needed.
				if ( forwardAlerts ) {
					alert.forward(this);
					return;
				}

			},
			"failure" : function(req,res) {

				// We failed - 400, 404, 408, 500 etc.
				// update the alert and the return code and msg if they
				// are provided.

				alert.setCustomInfoValue("Resolve.actionStatus","Request failed");
				resolveCustomInfo.Resolve.actionStatus = "Request failed";
				moogdb.setAlertCustomInfo(alert.value("alert_id"), resolveCustomInfo ,true);

				var logMsg = NAME + "Alert: " + alert.value("alert_id") + " : Failed to send alert to Resolve";
				logMsg += res.status_code ? " HTTP Status: " + res.status_code : "";
				logMsg += res.status_msg ? " Status msg: " + res.status_msg : "";
				logger.warning(logMsg);

				// Forward if needed.
				if ( forwardAlerts ) {
					alert.forward(this);
					return;
				}
			}
		};

		// Send the async request.
		var resolveRequest=rest.sendPost(resolvePostParams);
		return;
	}
	else {

		// The alert has a current actionStatus - take no action.
		if ( forwardAlerts ) {
			alert.forward(this);
			return;
		}
	}
}

//
// Situation -> Resolve
//

function resolveSituation(situation)  {

	var NAME = MOOBOT_NAME + "resolveSituation: ";
	logger.warning("HERE --------------------------------------- ");

	var currentCustomInfo = situation.getCustomInfo();

	// No custom info - set to blank and update the situation
	if ( !currentCustomInfo ) {
		currentCustomInfo = {};
		situation.setCustomInfo(currentCustomInfo);
		moogdb.setSigCustomInfo(situation.value("sig_id"),{},false);
	}

	//
	// Blank Resolve Custom Info object
	//

	var resolveCustomInfo =  {
		Resolve : {
			actionStatus : false,
			actionResults : "",
			actionSummary : ""
		}
	};

	// If there is no current Resolve object, add the default blank one.

	if ( !currentCustomInfo.Resolve ) {
		situation.setCustomInfoValue("Resolve",resolveCustomInfo.Resolve);
	}

	// Update the situation and the database object, but only if the current value is false / null
	// Use a db custom info merge to prevent updating the entire custom_info object
	// This pattern minimises the potential race conditions when setting custom_info.

	// MOOG-5889 Workaround
	situation.setCustomInfo(situation.getCustomInfo());
	currentCustomInfo = situation.getCustomInfo();

	// By default only forward an situation to Resolve on it's first occurrence,

	if ( !currentCustomInfo.Resolve.actionStatus ) {

		// No current status, eligible to send to Resolve.

		// Build an situation object to send.
		// Use an array of values, custom_info values can be included, e.g.
		// var situationParams=[ "sig_id" , "description" ];

		var situationParams=[ "sig_id","description","internal_priority" ];
		var resolvePayload = createResolvePayload(situation,situationParams);

		// Add additonal logic here to modify the data sent to Resolve
		// e.g.
		// if ( situation.value("internal_priority") === 5 ) {
		//		resolvePayload.priority = 1;
		//	}

		if ( !resolvePayload ) {
			logger.warning(NAME + "Failed to generate a suitable object to pass to Resolve");
			if ( forwardAlerts ) {
				situation.forward(this);
				return;
			}
		}

		// Async request with callbacks
		// return and update the object accordingly.

		var resolveURL = resolveURI + "?situation_id=" + situation.value("situation_id");

		var resolvePostParams={
			"url" : resolveURL,
			"body" : resolvePayload,
			"disable_certificate_validation" : true,
			"content_type" : "application/x-www-form-urlencoded",
			"timeout" : requestTimeout,
			"success" : function(req,res) {

				// Update the situation with a pending resolve status
				// Pre-define where results can be found

				situation.setCustomInfoValue("Resolve.actionStatus","pending");
				situation.setCustomInfoValue("Resolve.actionResults",resolveURL);

				resolveCustomInfo.Resolve.actionStatus = "pending";
				resolveCustomInfo.Resolve.actionResults = resolveURL;

				// Update the Resolve key in custom_info.
				moogdb.setSigCustomInfo(situation.value("sig_id"), resolveCustomInfo ,true);

				logger.info(NAME + "Situation: " + situation.value("sig_id") + " : Successfully sent situation to Resolve");

				// Forward if needed.
				if ( forwardSituations ) {
					situation.forward(this);
					return;
				}

			},
			"failure" : function(req,res) {

				// We failed - 400, 404, 408, 500 etc.
				// update the situation and the return code and msg if they
				// are provided.

				situation.setCustomInfoValue("Resolve.actionStatus","Request failed");
				resolveCustomInfo.Resolve.actionStatus = "Request failed";
				moogdb.setSigCustomInfo(situation.value("sig_id"), resolveCustomInfo ,true);

				var logMsg = NAME + "Situation: " + situation.value("sig_id") + " : Failed to send situation to Resolve";
				logMsg += res.status_code ? " HTTP Status: " + res.status_code : "";
				logMsg += res.status_msg ? " Status msg: " + res.status_msg : "";
				logger.warning(logMsg);

				// Forward if needed.
				if ( forwardSituations ) {
					situation.forward(this);
					return;
				}
			}
		};

		// Send the async request.
		var resolveRequest=rest.sendPost(resolvePostParams);
		return;
	}
	else {

		// The situation has a current actionStatus - take no action.
		if ( forwardSituations ) {
			situation.forward(this);
			return;
		}
	}
	logger.warning("THERE --------------------------------------- ");
}

function createResolvePayload(obj,objKeys) {

	var NAME = MOOBOT_NAME + "createResolvePayload: ";

	// Returns a populated object from the "objKeys" of "obj" (including custom_info values)

	var objData={};

	if ( !isArray(objKeys) ) {
		logger.warning(NAME + "Expected an array of object keys");
		return false;
	}

	for ( var objIdx = 0 ; objIdx < objKeys.length ; objIdx++ ) {

		var objName = objKeys[objIdx];
		var value = null;

		var ciRe = /^custom_info\.(.*)$/i.exec(objName);
		if ( ciRe && ciRe.length === 2 ) {
			var ciObj = ciRe[1];
			value = getCustomInfoValue(obj.getCustomInfo(),ciObj);
		}
		else {
			value = obj.value(objName);
		}
		objData[objName] = value;
	}
	return objData;

}

// ---------------------------------
// Utility functions - do not edit
// ---------------------------------

function getCustomInfoValue(sourceObj,targetObj) {
	if (!sourceObj || !targetObj ) {
		return null;
	}
	var targetObjKeys=targetObj.split(".");
	var objToCheck=sourceObj;
	for ( var kIdx = 0; kIdx < targetObjKeys.length; kIdx++ ) {
		objToCheck=objToCheck[targetObjKeys[kIdx]];
		if ( typeof objToCheck === 'undefined' ) {
			return null;
		}
	}
	return objToCheck;
}

function isArray(o) {
	return Object.prototype.toString.call(o) === '[object Array]';
}

// ---------------------------------


