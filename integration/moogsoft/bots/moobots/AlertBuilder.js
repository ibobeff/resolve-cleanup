/************************************************************
 *              Copyright (c) Moogsoft Inc 2015             *
 *                                                          *
 *----------------------------------------------------------*
 *                                                          *
 * The contents of this configuration file may be copied,   *
 * amended and used to create derivative works.             *
 *                                                          *
 ************************************************************/
 
//
// *****************************************************************************
//
// IMPORTANT:
//
// Prior to Incident.MOOG v4.1.14 AlertBuilder.js was a version of exemplary
// Alert Builder moobot that utilised v1 of the MoogDB module, with an
// alternative (AlertBuilderV2.js) that was written using v2 of the MoogDB
// module (MoogDb.V2)
//
// Since Incident.MOOG v4.1.14, v1 of MoogDB is no longer supported and has been
// removed. The only supported version is now MoogDB.V2. Consequently there is
// only a single version of each moobot each of which now only works with
// MoogDb.V2. For Alert Builder the current exemplar is AlertBuilder.js.
//
// *****************************************************************************
//

//
// load the required modules
//
var proc        =MooBot.loadModule('Process');
var events      =MooBot.loadModule('Events');
var moogdb      =MooBot.loadModule('MoogDb.V2');
var logger      =MooBot.loadModule('Logger');
var constants   =MooBot.loadModule('Constants');

//
// This function is called by the registered callback for eventType "Event"
//
function newEvent(event) 
{ 
    //---------------------------------------------
    // Here we show how the source event for alert creation can be modified
    // prior to the alert being created
    //---------------------------------------------
    /*******************************************************
    if(event.contains("description") === true)
    {
        var rc=event.set("description","test set thing");
        if(rc === true)
        {
            logger.info("Set the description");
        }
    }

    //
    // Using the previous example scratch counter
    // we calculate and output the alert count
    //
    var count_val=constants.get("counter");
    count_val++;
    constants.put("counter",count_val);

    logger.info("Alert count: " + count_val);

    *******************************************************/

    //
    // Create an alert for this event
    //
    // To merge custom_info for de-duplicated alerts, rather than leaving the
    // field unchanged after initial alert creation, replace the following line
    // of code with:
    //
    // var alert=moogdb.createAlert(event, true);
    //
    // NOTE: At higher rates of events/second this will have some impact on performance.
    //
    var alert=moogdb.createAlert(event);
    if(alert)
    {
        //
        // Log the alert count
        //
        logger.info("New Alert Id: " + alert.value("alert_id"));

        //  Example code for modifying the custom_info field in an alert:
        //
        //  var customInfo = alert.getCustomInfo();
        //
        //  if (customInfo)
        //  {
        //      logger.warning( "Custom info already exists" );
        //
        //      if (customInfo.cake_count)
        //      {
        //          logger.warning( "Incrementing cake count" );
        //          var count = customInfo.cake_count;
        //          count++;
        //          customInfo.cake_count = count;
        //      }
        //
        //      // Adds a new custom information field
        //      // customInfo.doughnuts = "Jam or ring?";
        //
        //      // Set a field to null
        //      // customInfo.cookies = null;
        //
        //      // Remove a field
        //      // delete customInfo.doughnuts;
        //
        //      // To set all the custom info to null:
        //      // customInfo = null;
        //  }
        //  else
        //  {
        //      logger.warning( "Custom info is null, creating" );
        //
        //      customInfo = {
        //                    "to_do_list" : ["Write list", "Lose list"],
        //                    "cake_count" : "0"
        //                   }
        //  }
        //
        //  alert.setCustomInfo(customInfo);
        //  moogdb.setAlertCustomInfo(alert);

        //
        // Now, send the alert onto whichever moolets are listening for alerts via their
        // process_output_of moog_farmd.conf setting
        //
        // NOTE: Forwarding an alert should always be the final operation of newEvent()!
        //
        alert.forward(this);
        alert.forward("Moog2Resolve");
        //
        // To forward the alert onto a moolet which does not use process_output_of,
        // uncomment the following line and add change it to reflect the name of the
        // moolet you wish to send the alert to. The alert can be forwarded to multiple
        // moolets by having multiple configured copied of this line:
        //
        // alert.forward( "My_AlertRulesEngine_Moolet" );
        //
    }
    else
    {
        logger.warning("Alert not created successfully");
    }
}

//
// Register the function newEvent() as the callback for new events
//
events.onEvent("newEvent",constants.eventType("Event")).listen();
