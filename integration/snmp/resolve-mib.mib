RESOLVE-MIB DEFINITIONS ::= BEGIN
IMPORTS
    enterprises, OBJECT-TYPE, MODULE-IDENTITY, NOTIFICATION-TYPE,
    IpAddress, Integer32, Counter32, TimeTicks
        FROM SNMPv2-SMI

    DisplayString
        FROM SNMPv2-TC;

resolve MODULE-IDENTITY
    LAST-UPDATED "1106030000Z"   -- 3 June 2011
    ORGANIZATION "Resolve Systems"
    CONTACT-INFO
        "http://www.resolve-systems.com/"
    DESCRIPTION
        "Enterprise node for Resolve Systems"
    ::= { enterprises 29243 }



resolveEventsV2 OBJECT-IDENTITY
    STATUS      current
    DESCRIPTION
        "Definition point for resolve enterprise notifications."
    ::= { resolve 0 }

    alertInfo OBJECT IDENTIFIER             ::= { resolve 1 }

--
-- OBJECT VALUES
--

alertSeverity OBJECT-TYPE
    SYNTAX      DisplayString
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
        "Alert severity - critical, severity, warning, info."
    ::= { alertInfo 1 }

alertType OBJECT-TYPE
    SYNTAX      DisplayString
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
        "Alert type code. See JAVADOC API for code values and action to be taken."
    ::= { alertInfo 2 }

alertSource OBJECT-TYPE
    SYNTAX      DisplayString
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
        "Agent source"
    ::= { alertInfo 3 }

alertComponent OBJECT-TYPE
    SYNTAX      DisplayString
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
        "Agent component"
    ::= { alertInfo 4 }

alertMessage OBJECT-TYPE
    SYNTAX      OCTET STRING
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
        "Agent description."
    ::= { alertInfo 5 }

--
-- NOTIFICATION TYPES
--

fatalLog NOTIFICATION-TYPE
    OBJECTS     { alertSeverity, alertType, alertSource, alertComponent, alertMessage }
    STATUS      current
    DESCRIPTION
        "SNMP trap generated for a fatal log message"
    ::= { resolveEventsV2 1 }

END
