import sys
import cherrypy
import logging
import logging.handlers
import splunk
import splunk.rest

import splunk.appserver.mrsparkle.controllers as controllers
from splunk.appserver.mrsparkle.lib.decorators import expose_page
from splunk.appserver.mrsparkle.lib.routes import route

import splunk.bundle as bundle

from splunk.appserver.mrsparkle.lib.util import make_splunkhome_path


def setup_logger(level):
    logger = logging.getLogger('splunk.appserver.ResolveUI.controllers.RsRestApi')
    logger.propagate = False # Prevent the log messages from being duplicated in the python.log file.
    logger.setLevel(level)

    file_handler = logging.handlers.RotatingFileHandler(make_splunkhome_path(['var', 'log', 'splunk', 'rs_rest_api.log']), maxBytes=25000000)

    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    return logger

logger = setup_logger(logging.INFO)

class RsRestApi(controllers.BaseController):

    CONF_FILE = 'rsapp'

    @route('/:var=get_proxy_setting')
    @expose_page(must_login=True, methods=['GET', 'POST'])
    def get_proxy_setting(self, **kwargs):
        try:
            sessionKey = cherrypy.session.get('sessionKey')
            request = cherrypy.request
            user = request.headers.get('Remote-User')
            # Read the current settings from the conf file
            configs = bundle.getConf(self.CONF_FILE, sessionKey=sessionKey, namespace='ResolveUI', owner=user)
            confDict = {}
            for s in configs:
                confDict[s] = {}
                confDict[s].update(configs[s].items())

            # Get the settings
            content = {}
            content['sessionId']    = cherrypy.session.id
            content['sessionKey']   = sessionKey
            content['userId']       = user
            if None != confDict:
                for stanza, settings in confDict.items():
                    for key, val in settings.items():
                        content[key] = val
            return self.render_json(content)

        except:
            logger.exception("Unable to process the request get_proxy_setting")

