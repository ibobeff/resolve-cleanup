
define(function(require, exports, module){
    // Base class for custom views
    var SimpleSplunkView = require('splunkjs/mvc/simplesplunkview');

    // Define the custom view class
    var RSCustomView = SimpleSplunkView.extend({
        className: "RSCustomView",

        render: function() {
            var me = this;
            $.ajax(
                {
                    url:  Splunk.util.make_url('/custom/ResolveUI/rs_rest_api/get_proxy_setting'),
                    type: 'POST',
                    data: {},
                    success: function(result, textStatus, jqXHR){

                        if( result.hasOwnProperty("success") && result.success == false ){
                            return;
                        }

                        var proxy_server = result['proxy_server'],
                            proxy_port   = (result['proxy_port'] && result['proxy_port'].trim())? ':' + result['proxy_port']: '' ,
                            proxy_type   = result['proxy_type']? result['proxy_type'] + '://': 'http://',
                            userId       = result['userId'],
                            sessionKey   = result['sessionKey'],
                            viewPage     = me.settings.get('viewPage'),
                            src          = proxy_type + proxy_server + proxy_port + '/resolve/jsp/rsclient.jsp?displayClient=false';

                        src += '&SSOTYPE=splunk&USERNAME='+userId+'&CREDTOKEN='+sessionKey;
                        if (viewPage == 'content_search') {
                            src += '#RS.search.Main/';
                        } else if (viewPage == 'social_collaboration') {
                            src += '#RS.social.Main/'
                        }
                        else if (viewPage == 'worksheets_my_assigned') {
                            src += '#RS.worksheet.Worksheets/assignedTo=admin';
                        }
                        else if (viewPage == 'worksheets_unassigned') {
                            src += '#RS.worksheet.Worksheets/assignedToName=NIL';
                        }
                        else if (viewPage == 'worksheets_all') {
                            src += '#RS.worksheet.Worksheets/noFilter=true';
                        }
                        else if (viewPage == 'worksheet_by_reference') {
                            src += '#RS.worksheet.Worksheets/'
                        + location.search.slice(1);
                        }
                        else if (viewPage == 'worksheet_by_alertid') {
                            src += '#RS.worksheet.Worksheets/'
                        + location.search.slice(1);
                        }
                        else if (viewPage == 'worksheet_by_correlationid') {
                            src += '#RS.worksheet.Worksheets/'
                        + location.search.slice(1);
                        }
                        else if (viewPage == 'contentrequest_new_request') {
                            src += '#RS.formbuilder.Viewer/name=NEW_CONTENT_REQUEST';
                        }
                        else if (viewPage == 'contentrequest_my_request') {
                            src += '#RS.customtable.Table/table=content_request&view=user&u_status=neq~Closed&u_requester=admin';
                        }
                        else if (viewPage == 'contentrequest_my_assigned') {
                            src += '#RS.customtable.Table/table=content_request&view=Default&u_assignee=admin';
                        }
                        else if (viewPage == 'contentrequest_unassigned') {
                            src += '#RS.customtable.Table/table=content_request&view=Default&u_status=neq~Closed&u_status=neq~Rejected&u_assignee=NIL';
                        }
                        else if (viewPage == 'contentrequest_all') {
                            src += '#RS.customtable.Table/table=content_request&view=Default&noFilter=true';
                        }
                        else if (viewPage == 'new_content') {
                            src += '#RS.wiki.Main/newWiki=true';
                        }
                        else if (viewPage == 'automation_my_assigned') {
                            src += '#RS.wiki.resolutionbuilder.Automations/sysUpdatedBy=admin';
                        }
                        else if (viewPage == 'automation_all') {
                            src += '#RS.wiki.resolutionbuilder.Automations/noFilter=true';
                        }
                        else if (viewPage == 'actiontasks_my_assigned') {
                            src += '#RS.actiontask.Main/assignedTo.UUserName=admin';
                        }
                        else if (viewPage == 'actiontasks_all') {
                            src += '#RS.actiontask.Main/noFilter=true';
                        }
                        else if (viewPage == 'actiontasks_resolve_menus') {
                            src += '#RS.client.Menu/menuSet=All';
                        }
                        else if (viewPage == 'resolve_reports') {
                            src += '#RS.client.Menu/menuSet=Report%20Menu';
                        }
                        else if (viewPage == 'wiki_business_reports') {
                            // src += '#RS.wiki.Main/name=Reports.Runbook%20Cost%20Saving%20Details';
                            src += '&interval=240#RS.wiki.Main/name=Reports.Resolve%20Execution%20Dashboard%203';
                        }
                        else if (viewPage == 'resolve_menus') {
                            src += '#RS.client.Menu/menuSet=All';
                        }
                        else if (viewPage == 'resolution_routing') {
                            //src = window.location.protocol + '//' + me.$el[0].getAttribute('hostname')
                            //    + ':' + me.$el[0].getAttribute('port') + '/resolve/jsp/rsclient.jsp?' + location.search.slice(1);
                            src += '&' + location.search.slice(1);
                        }
                        else {
                            alert('Wait a minute, this can\'t be right! You clicked something else that I don\'t recognize');
                        }

                        me.$el.html(
                            "<div class=\"container\">" +
                            "<iframe id=\"RSCustomView\" src=" + src + " height=\"100%\" width=\"100%\" border=\"0\" frameborder=\"0\"></iframe>"
                            + "</div>"
                        );

                        return me;
                    },
                    error: function(jqXHR,textStatus,errorThrown) {
                        console.error('Failed to get Resolve app proxy settings');
                    }
                });
        }
    });
    return RSCustomView;
});
