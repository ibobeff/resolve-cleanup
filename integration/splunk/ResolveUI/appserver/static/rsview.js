require([
    "/static/app/ResolveUI/RSCustomView.js",
    "splunkjs/mvc/simplexml/ready!"
], function(RSCustomView) {

    // Create a custom view
    var viewPage = $("#rsview")[0].getAttribute('view_page')
    var customView = new RSCustomView({
        id: viewPage,
        viewPage: viewPage,
        el: $("#rsview")
    }).render();

});

