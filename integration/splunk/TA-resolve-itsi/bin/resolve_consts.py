app_name = "TA-resolve-itsi"

# Constants for Resolve account
encrypted = "<encrypted>"
userpass_sep = "``"
dummy = "dummy"

# Resolve versions
CORONA = "5.5"

DEFAULT_RECORD_LIMIT = 1000
DEFAULT_DISPLAY_VALUE = "false"
