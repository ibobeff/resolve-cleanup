# Resolve Guided Resolution
action.resolve_guided_resolution.param.event_id = <string>
* Field in the event indicating the ID to process.
* Mandatory - If no value is provided, no event will be processed.
* Set to $result.event_id$

# Resolution Worksheet
action.resolution_worksheet.param.event_id = <string>
* Field in the event indicating the ID to process.
* Mandatory - If no value is provided, no event will be processed.
* Set to $result.event_id$

# Resolve Action No HTML
action.resolve_no_html.param.event_id = <string>
* Field in the event indicating the ID to process.
* Mandatory - If no value is provided, no event will be processed.
* Set to $result.event_id$
