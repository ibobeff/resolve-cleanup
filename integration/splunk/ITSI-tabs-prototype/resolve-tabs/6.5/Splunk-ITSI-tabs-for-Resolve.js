function waitForElementToDisplay(selector, timeToWaitBetweenIterations, countBailOut, functionToExecute) {
	if(document.querySelector(selector) != null) {
		// alert("Selector '" + selector + "' is available...");
		functionToExecute();
		return;
	}
	else {
		setTimeout(function() {
			if (countBailOut > 0) {
				countBailOut--;
				waitForElementToDisplay(selector, timeToWaitBetweenIterations, countBailOut, functionToExecute);
			} else {
				// alert("Bail out on '" + selector + "' selector to show up.");
				return;
			}
		}, timeToWaitBetweenIterations);
	}
}

function appendResolveRRTab() {
	if (document.querySelector("div.tab-layout-container ul.nav-tabs li#resolve-rr-tab") == null) {
		var otherTabs = document.querySelectorAll("div.event-management-detail-panel-body div.tab-layout-container ul li");
		[].forEach.call(otherTabs, function(li) {
			li.addEventListener("click", function() {
				var resolveContent = document.querySelector("#resolve-rr-content");
				if (resolveContent != null) {
					resolveContent.remove();
				}
			})
		});
		var navTabs = document.querySelector("div.tab-layout-container ul.nav-tabs"),
		    li = document.createElement("LI");
		li.setAttribute('role', 'presentation');
		li.setAttribute('data-tab-id', 'resolve-rr-tab');
		li.setAttribute('id', 'resolve-rr-tab');
		li.innerHTML = '<a role="tab" class="tab-label" style="color:#66CC33; font-weight: bold">Resolve Guided Resolution</a>';
		navTabs.appendChild(li);
		var activeLis = document.querySelectorAll("div.event-management-detail-panel-body div.tab-layout-container ul li");
		li.addEventListener('click', function() {
			var wsIframe = document.querySelector("#resolve-ws-content");
			if (wsIframe != null) {
				wsIframe.remove();
			}
			var queryString = "";
			if (document.querySelector("div.overview-contents div.overview-row") != null) {
				var eventDetailRows = document.querySelectorAll("div.overview-contents div.overview-row");
				[].forEach.call(eventDetailRows, function(eventDetailRow) {
					var key = eventDetailRow.childNodes[0].textContent,
					value = eventDetailRow.childNodes[1].textContent;
					queryString += "&" + key.substr(0,key.indexOf(":")) + "=" + encodeURI(value);
				});
			}
		    var lis = document.querySelectorAll(".nav-tabs li"),
				tabPanes = document.querySelectorAll("div.tab-content-scroll-wrapper div.tab-content div.tab-pane");
			[].forEach.call(lis, function(li) {
				li.className = "";
			});
			[].forEach.call(tabPanes, function(tabPane) {
				tabPane.className = "tab-pane";
			});
			document.querySelector("#resolve-rr-tab").className = "active";
			var tabContent = document.querySelector("div.event-management-detail-panel-body div.tab-layout-container div.tab-content-scroll-wrapper div.tab-content"),
				tab = document.createElement("DIV");
			tab.setAttribute('role', 'tabpanel');
			tab.setAttribute('data-tab-id', 'resolve-rr-tab');
			tab.setAttribute('id', 'resolve-rr-content');
			tab.setAttribute('class', 'tab-pane');
			tabContent.appendChild(tab);
			var eventId = document.querySelector("#app-main-layout > div > div.event-management-layout > div > div.event-management-detail-list > div.event-management-detail-list-body-container > div.event-management-detail-list-body > div.event-management-detail-list-row.active").getAttribute("data-eventid");
			var cacheBuster = Math.round(new Date().getTime() / 1000),
				link = "%%RSVIEW_URL%%/resolve/jsp/rsclient.jsp?SSOTYPE=splunk&displayClient=false&rid=true&autoCreate=true&splunk_itsi=true&notable_event_id=" + eventId + queryString + "&cb=" + cacheBuster,
				height = parseInt(document.querySelector(".event-management-detail-panel-body").clientHeight);
		    	height -= parseInt(document.querySelector("div.event-management-detail-panel-body-header").clientHeight);
				height -= parseInt(document.querySelector("ul.nav-tabs").clientHeight);
		    	height -= 8;
			tab.innerHTML = "<div role='tabpanel' class='tab-pane' id='resolve-rr-iframe'><iframe id='resolve-ext-iframe' src='"+link+"' width='100%' height='"+height+"' style='border: 0px;'></iframe></div>";
			document.querySelector("#resolve-rr-content").className = "active";
		});
	}
	if (document.querySelector("table.table-chrome.table-striped.wrapped-results") == undefined) {
		waitForElementToDisplay("table.table-chrome.table-striped.wrapped-results", 500, 100, appendResolveRRTab);
	} else {
		waitForElementToDisplay("table.table-chrome.table-striped.wrapped-results tbody tr td:first-child", 500, 100, appendResolveWSTab);
	}
}

function appendResolveWSTab() {
	if (document.querySelector("div.tab-layout-container ul.nav-tabs li#resolve-ws-tab") == null) {
		var fullText = document.querySelectorAll("table.table-chrome.table-striped.wrapped-results")[0].textContent;
		if (fullText.indexOf("WorksheetID=") > -1) {
			var
				wsIndexStart = fullText.indexOf("WorksheetID=") + 12,
				wsIndexEnd = wsIndexStart + 32,
				wsId = fullText.substr(wsIndexStart, wsIndexEnd);
				console.log("RWS_ID : " + wsId);
		} else {
			return;
		}
		var otherTabs = document.querySelectorAll("div.event-management-detail-panel-body div.tab-layout-container ul li");
		[].forEach.call(otherTabs, function(li) {
			li.addEventListener("click", function() {
				var resolveContent = document.querySelector("#resolve-ws-content");
				if (resolveContent != null) {
					resolveContent.remove();
				}
			})
		});
		var navTabs = document.querySelector("div.tab-layout-container ul.nav-tabs"),
			li = document.createElement("LI");
		li.setAttribute('role', 'presentation');
		li.setAttribute('data-tab-id', 'resolve-ws-tab');
		li.setAttribute('id', 'resolve-ws-tab');
		li.innerHTML = '<a role="tab" class="tab-label" style="color:#66CC33; font-weight: bold">Resolution Worksheet</a>';
		navTabs.appendChild(li);
		var activeLis = document.querySelectorAll("div.event-management-detail-panel-body div.tab-layout-container ul li");
		li.addEventListener('click', function() {
			var rrIframe = document.querySelector("#resolve-rr-content");
			if (rrIframe != null) {
				rrIframe.remove();
			}
			var lis = document.querySelectorAll(".nav-tabs li"),
				tabPanes = document.querySelectorAll("div.tab-content-scroll-wrapper div.tab-content div.tab-pane");
			[].forEach.call(lis, function(li) {
				li.className = "";
			});
			[].forEach.call(tabPanes, function(tabPane) {
				tabPane.className = "tab-pane";
			});
			document.querySelector("#resolve-ws-tab").className = "active";
			var tabContent = document.querySelector("div.event-management-detail-panel-body div.tab-layout-container div.tab-content-scroll-wrapper div.tab-content"),
				tab = document.createElement("DIV");
			tab.setAttribute('role', 'tabpanel');
			tab.setAttribute('data-tab-id', 'resolve-ws-tab');
			tab.setAttribute('id', 'resolve-ws-content');
			tab.setAttribute('class', 'tab-pane');
			tabContent.appendChild(tab);
			var eventId = document.querySelector("#app-main-layout > div > div.event-management-layout > div > div.event-management-detail-list > div.event-management-detail-list-body-container > div.event-management-detail-list-body > div.event-management-detail-list-row.active").getAttribute("data-eventid");
			var cacheBuster = Math.round(new Date().getTime() / 1000),
				link = "%%RSVIEW_URL%%/resolve/jsp/rsclient.jsp?SSOTYPE=splunk&displayClient=false#RS.worksheet.Worksheet/id="+wsId+"&activeTab=1&cb=" + cacheBuster,
				height = parseInt(document.querySelector(".event-management-detail-panel-body").clientHeight);
				height -= parseInt(document.querySelector("div.event-management-detail-panel-body-header").clientHeight);
				height -= parseInt(document.querySelector("ul.nav-tabs").clientHeight);
				height -= 8;
			tab.innerHTML = "<div role='tabpanel' class='tab-pane' id='resolve-ws-iframe'><iframe id='resolve-ext-iframe' src='"+link+"' width='100%' height='"+height+"' style='border: 0px;'></iframe></div>";
			document.querySelector("#resolve-ws-content").className = "active";
		});
	}
}

/*
function attachClickEventForResolve() {
	var divs = document.querySelectorAll(".event-management-detail-list-row");
	[].forEach.call(divs, function(div) {
		div.addEventListener('click', function() {
			appendResolveRRTab();
		});
	});
}
*/

/* DO NOT ERASE THE FOLLOWING ... IT CONTAINS AN IMPROVEMENT CODE THAT NEEDS TO BE FINISHED */
/*
function isDescendantOfClass(parentClass, child) {
    var node = child.parentNode;
    while (node != null) {
        if (node.className == parentClass) {
            return true;
        }
        node = node.parentNode;
    }
    return false;
}

document.querySelector('body').addEventListener('click', function(e) {
	var parentTargetClass1 = "event-management-overview-row",
        parentTargetClass2 = "event-management-detail-list-row active";
    if (isDescendantOfClass(parentTargetClass1, e.target) || isDescendantOfClass(parentTargetClass2, e.target)) {
			waitForElementToDisplay("div.tab-layout-container ul.nav-tabs", 500, 1000000, appendResolveRRTab);
			// waitForElementToDisplay(".event-management-detail-list-row active", 500, 1000000, attachClickEventForResolve);
    }
	//
	if (event.target.tagName.toLowerCase() === 'a' && event.target.innerHTML === 'Actions<span class="caret"></span>') {
		waitForElementToDisplay("ul.event-management-extensible-actions-menu-list", 100, 100, function(e) {
			var queryString = "";
			if (document.querySelector("div.overview-contents div.overview-row") != null) {
				var eventDetailRows = document.querySelectorAll("div.overview-contents div.overview-row");
				[].forEach.call(eventDetailRows, function(eventDetailRow) {
					var key = eventDetailRow.childNodes[0].textContent,
						value = eventDetailRow.childNodes[1].textContent;
					queryString += "&" + key.substr(0,key.indexOf(":")) + "=" + encodeURI(value.replace('#', ''));
				});
			}
			var ul = document.querySelector("ul.event-management-extensible-actions-menu-list"),
				li = document.createElement("LI");
			li.setAttribute('id', 'resolve-guided-resolution-link');
			li.innerHTML = '<a class="event-management-extensible-action" style="color:#66CC33; font-weight: bold">Resolve Guided Resolution</a>';
			ul.appendChild(li);
			li.addEventListener('click', function() {
				var eventId = document.querySelector("#app-main-layout > div > div.event-management-layout > div > div.event-management-detail-list > div.event-management-detail-list-body-container > div.event-management-detail-list-body > div.event-management-detail-list-row.active").getAttribute("data-eventid"),
					cacheBuster = Math.round(new Date().getTime() / 1000),
					link = "%%RSVIEW_URL%%/resolve/jsp/rsclient.jsp?SSOTYPE=splunk&rid=true&autoCreate=true&splunk_itsi=true&notable_event_id="+eventId+queryString+"&cb="+cacheBuster;
				window.open(link);
			});
			var fullText = document.querySelectorAll("table.table-chrome.table-striped.wrapped-results")[0].textContent;
			if (fullText.indexOf("WorksheetID=") > -1) {
				var wsIndexStart = fullText.indexOf("WorksheetID=") + 12,
					wsIndexEnd = wsIndexStart + 32,
					wsId = fullText.substr(wsIndexStart, wsIndexEnd);
				console.log("RWS_ID : " + wsId);
				var li = document.createElement("LI");
				li.setAttribute('id', 'resolve-worksheet-link');
				li.innerHTML = '<a class="event-management-extensible-action" style="color:#66CC33; font-weight: bold">Resolve Worksheet</a>';
				ul.appendChild(li);
				li.addEventListener('click', function() {
					var cacheBuster = Math.round(new Date().getTime() / 1000),
						link = "%%RSVIEW_URL%%/resolve/jsp/rsclient.jsp?SSOTYPE=splunk&activeTab=1&cb="+cacheBuster+"#RS.worksheet.Worksheet/id="+wsId;
					window.open(link);
				});
			}
		});
	}
});
*/