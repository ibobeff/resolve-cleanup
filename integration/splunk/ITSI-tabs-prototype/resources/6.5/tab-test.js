var ul = $("div.tab-layout-container ul.nav-tabs"),
    li = document.createElement("LI");
li.setAttribute('role', 'presentation');
li.setAttribute('data-tab-id', 'resolve-splunk-app-tab');
li.setAttribute('id', 'resolve-splunk-app-tab');
li.innerHTML = '<a role="tab" class="tab-label" style="color:#66CC33; font-weight: bold">Resolve Guided Resolution</a>';
ul.appendChild(li);
li.addEventListener('click', function() {
    var queryString = "",
    	lis = document.querySelectorAll(".nav-tabs li"),
		divs = document.querySelectorAll("div.tab-content-scroll-wrapper div.tab-content div.tab-pane");
	[].forEach.call(lis, function(li) {
		li.className = "";
	});
	[].forEach.call(lis, function(div) {
		div.className = "";
	});
	document.querySelector("#resolve-splunk-app-tab").className = "active";
	var eventId = document.querySelector("#app-main-layout > div > div.event-management-layout > div > div.event-management-detail-list > div.event-management-detail-list-body-container > div.event-management-detail-list-body > div.event-management-detail-list-row.active").getAttribute("data-eventid");
	var link = "https://localhost.cloud.resolvesys.com:8443/resolve/jsp/rsclient.jsp?rid=true&autoCreate=true&rule_id="+eventId+"&status=1&security_domain=threat&displayClient=false",
		activeTabPane = document.querySelector(".tab-content-scroll-wrapper div.tab-content div.tab-pane.active"),
        height = parseInt(document.querySelector(".event-management-detail-panel-body").clientHeight);
		console.log(height);
    	height -= parseInt(document.querySelector("div.event-management-detail-panel-body-header").clientHeight);
		console.log(height);
		height -= parseInt(document.querySelector("ul.nav-tabs").clientHeight);
		console.log(height);
    	height -= 8;
		console.log(height);

	if (document.querySelector("#resolve-splunk-app-tab") != undefined) {
		var contentDiv = document.querySelector("div.tab-content-scroll-wrapper div.tab-content");
		contentDiv.innerHTML = "<div role='tabpanel' class='tab-pane' id='resolve-splunk-app-tab-pane'><iframe id='resolve-ext-iframe' src='"+link+"' width='100%' height='"+height+"' style='border: 0px;'></iframe></div>";
	}
	document.querySelector("#resolve-splunk-app-tab-pane").className = "active";
});
