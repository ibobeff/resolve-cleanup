function waitForElementToDisplay(selector, timeToWaitBetweenIterations, countBailOut, functionToExecute) {
	if(document.querySelector(selector) != null) {
		// alert("Selector '" + selector + "' is available...");
		functionToExecute();
		return;
	}
	else {
		setTimeout(function() {
			if (countBailOut > 0) {
				countBailOut--;
				waitForElementToDisplay(selector, timeToWaitBetweenIterations, countBailOut, functionToExecute);
			} else {
				// alert("Bail out on '" + selector + "' selector to show up.");
				return;
			}
		}, timeToWaitBetweenIterations);
	}
}

function canAppendResolveTab() {
	if (document.querySelector("div.tab-layout-container ul.nav-tabs li#resolve-splunk-app-tab") == null) {
		var ul = document.querySelector("div.tab-layout-container ul.nav-tabs"),
		    li = document.createElement("LI");
		li.setAttribute('role', 'presentation');
		li.setAttribute('data-tab-id', 'resolve-splunk-app-tab');
		li.setAttribute('id', 'resolve-splunk-app-tab');
		li.innerHTML = '<a role="tab" class="tab-label" style="color:#66CC33; font-weight: bold">Resolve Guided Resolution</a>';
		ul.appendChild(li);
		li.addEventListener('click', function() {
		    var queryString = "",
		    	lis = document.querySelectorAll(".nav-tabs li"),
				divs = document.querySelectorAll("div.tab-content-scroll-wrapper div.tab-content div.tab-pane");
			[].forEach.call(lis, function(li) {
				li.className = "";
			});
			[].forEach.call(lis, function(div) {
				div.className = "";
			});
			document.querySelector("#resolve-splunk-app-tab").className = "active";
			var eventId = document.querySelector("#app-main-layout > div > div.event-management-layout > div > div.event-management-detail-list > div.event-management-detail-list-body-container > div.event-management-detail-list-body > div.event-management-detail-list-row.active").getAttribute("data-eventid");
			var link = "https://localhost.cloud.resolvesys.com:8443/resolve/jsp/rsclient.jsp?displayClient=false&rid=true&autoCreate=true&splunk_itsi=true&rule_id="+eventId,
				activeTabPane = document.querySelector(".tab-content-scroll-wrapper div.tab-content div.tab-pane.active"),
		        height = parseInt(document.querySelector(".event-management-detail-panel-body").clientHeight);
		    	height -= parseInt(document.querySelector("div.event-management-detail-panel-body-header").clientHeight);
				height -= parseInt(document.querySelector("ul.nav-tabs").clientHeight);
		    	height -= 8;
			if (document.querySelector("#resolve-splunk-app-tab") != undefined) {
				var contentDiv = document.querySelector("div.tab-content-scroll-wrapper div.tab-content");
				contentDiv.innerHTML = "<div role='tabpanel' class='tab-pane' id='resolve-splunk-app-tab-pane'><iframe id='resolve-ext-iframe' src='"+link+"' width='100%' height='"+height+"' style='border: 0px;'></iframe></div>";
			}
			document.querySelector("#resolve-splunk-app-tab-pane").className = "active";
		});
	}
	if (document.querySelector("table.table-chrome.table-striped.wrapped-results") == undefined) {
		waitForElementToDisplay("table.table-chrome.table-striped.wrapped-results", 100, 100, canAppendResolveTab);
	} else {
		waitForElementToDisplay("table.table-chrome.table-striped.wrapped-results tbody tr td:first-child", 100, 100, appendResolveTab);
	}
}

function appendResolveTab() {
	var fullText = document.querySelectorAll("table.table-chrome.table-striped.wrapped-results")[0].textContent;
	if (fullText.indexOf("WorksheetID=") > -1) {
		var
			wsIndexStart = fullText.indexOf("WorksheetID=") + 12,
			wsIndexEnd = wsIndexStart + 32,
			wsId = fullText.substr(wsIndexStart, wsIndexEnd);
			console.log("RWS_ID : " + wsId);
	} else {
		return;
	}
	if (document.querySelector("#resolve-ext-tab") != null) {
		document.querySelector("#resolve-ext-tab").remove();
	}
	var li = document.createElement("LI");
	li.setAttribute('role', 'presentation');
	li.setAttribute('data-tab-id', 'resolve-ext-tab');
	li.setAttribute('id', 'resolve-ext-tab');
	li.innerHTML = '<a role="tab" class="tab-label" style="color:#66CC33; font-weight: bold">Resolution Worksheet</a>';
	document.querySelector("div.tab-layout-container ul.nav-tabs").appendChild(li);
	li.addEventListener('click', function() {
		var queryString="",
			lis = document.querySelectorAll(".nav-tabs li"),
			divs = document.querySelectorAll("div.tab-content-scroll-wrapper div.tab-content div.tab-pane");
		[].forEach.call(lis, function(li) {
			li.className = "";
		});
		[].forEach.call(lis, function(div) {
			div.className = "";
		});
		document.querySelector("#resolve-ext-tab").className = "active";
		var link = "https://localhost.cloud.resolvesys.com:8443/resolve/jsp/rsclient.jsp?displayClient=false#RS.worksheet.Worksheet/id="+wsId+"&activeTab=1";
		var activeTabPane=document.querySelector(".tab-content-scroll-wrapper div.tab-content div.tab-pane.active"),
			height = parseInt(document.querySelector(".event-management-detail-panel-body").clientHeight);
			height -= parseInt(document.querySelector("div.event-management-detail-panel-body-header").clientHeight);
			height -= parseInt(document.querySelector("ul.nav-tabs").clientHeight);
			height -= 8;
		if (document.querySelector("#resolve-ext-iframe") == null) {
			document.querySelector("div.tab-content-scroll-wrapper div.tab-content").innerHTML = "<div role='tabpanel' class='tab-pane' id='resolve-ext-tab-pane'><iframe id='resolve-ext-iframe' src='"+link+"' width='100%' height='"+height+"' style='border: 0px;'></iframe></div>";
		}
		document.querySelector("#resolve-ext-tab-pane").className = "active";
	});
}

waitForElementToDisplay("div.tab-layout-container ul.nav-tabs", 100, 1000000, canAppendResolveTab);
waitForElementToDisplay(".event-management-detail-list-row", 100, 1000000, attachClickEventForResolve);

function attachClickEventForResolve() {
	var divs = document.querySelectorAll(".event-management-detail-list-row");
	[].forEach.call(divs, function(div) {
		div.addEventListener('click', function() {
			canAppendResolveTab();
		});
	});
}
