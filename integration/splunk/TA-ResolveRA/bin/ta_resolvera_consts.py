globalsetting_key = "global_settings"

name = "name"
value = "value"

password = "password"
proxy_password = "proxy_password"
removed = "removed"

all_settings = "all_settings"
myta_settings = "TA-ResolveRA_settings"
global_settings = "global_settings"
proxy_settings = "proxy_settings"
myta_credential_settings = "credential_settings"
myta_customized_settings = "customized_settings"

type_bool = "type_bool"
type_text = "type_text"
type_password = "type_password"

myta_conf = "TA-ResolveRA"
myta_credential_conf = "TA-ResolveRA_credential"
myta_customized_conf = "TA-ResolveRA_customized"

ignore_backend_req = "ignore_backend_req"