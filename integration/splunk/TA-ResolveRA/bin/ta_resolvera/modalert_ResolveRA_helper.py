# encoding = utf-8

import httplib2
import urllib
import json

def process_event(helper, *args, **kwargs):
    """
    # IMPORTANT
    # Do not remove the anchor macro:start and macro:end lines.
    # These lines are used to generate sample code. If they are
    # removed, the sample code will not be updated when configurations
    # are updated.

    # The following example adds two sample events ("hello", "world")
    # and writes them to Splunk
    # NOTE: Call helper.writeevents() only once after all events
    # have been added
    helper.addevent("hello", sourcetype="resolve")
    helper.addevent("world", sourcetype="resolve")
    helper.writeevents(index="summary", host="localhost", source="localhost")

    # The following example gets the events that trigger the alert
    events = helper.get_events()
    for event in events:
        helper.log_info("event={}".format(event))

    # helper.settings is a dict that includes environment configuration
    # Example usage: helper.settings["server_uri"]
    helper.log_info("server_uri={}".format(helper.settings["server_uri"]))
    """

    helper.log_info("Alert action incident_resolution started.")

    resolve_http_gw_uri = helper.get_global_setting("resolve_http_gw_uri")
    helper.log_info("resolve_http_gw_uri={}".format(resolve_http_gw_uri))

    # The following example gets the events that trigger the alert
    events = helper.get_events()
    for event in events:
        description = urllib.quote(event["rule_description"])
        query_string = "?sid={}&correlationid={}&reference={}&description={}".format(event["orig_sid"], event["orig_sid"], event["rule_id"], description)
        for key in event:
            if key[:1] != "_" and key != "rid":
                query_string += "&{}={}".format(key, urllib.quote(event[key]))
        # helper.log_info("Event Description: {}".format(description))
        # url = "{}?rule_id={}&sid={}&security_domain={}&description={}".format(resolve_http_gw_uri, event["event_id"], event["orig_sid"], event["security_domain"], description)
        # helper.log_info(query_string)
        url = "{}{}".format(resolve_http_gw_uri, query_string)
        headers = {'Content-Type': 'application/json'}
        method = 'GET'

        if url.startswith("http:"):
            helper.log_error("Cannot make request to Resolve.  Provided URL uses insecure scheme (http). ")
            return 1

        resp, content = httplib2.Http().request(url, method, headers=headers)
        helper.log_info("Response: {}".format(resp))
        helper.log_info("Content: {}".format(content))
        message = json.loads(content)
        # worksheet_id = message["Message"]["worksheetId"]
        # helper.log_info("Generated Resolve worksheetId: %s" % worksheet_id)

    return 0
