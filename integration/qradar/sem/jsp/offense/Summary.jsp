<%@ page import="java.util.Locale"%>
<%@ page import="java.util.Arrays"%>
<%@ page import="com.q1labs.console.util.ConsoleUtils"%>
<%@ page import="com.q1labs.uiframeworks.util.RequestUtils"%>
<%@ page import="java.util.List"%>
<%@ page extends="com.q1labs.uiframeworks.jsp.HttpJspBase"
	import="com.q1labs.uiframeworks.util.RequestUtils"
	import="com.q1labs.uiframeworks.guiactions.Toolbar"
	import="com.q1labs.uiframeworks.guiactions.GUIAction"
	import="com.q1labs.uiframeworks.guiactions.GUIActionFactory"
	import="com.q1labs.uiframeworks.guiactions.GUIActionGroup"
	import="com.q1labs.console.util.ConsoleUtils"
	import="com.q1labs.core.dao.sem.views.NetworkDetailsView"
	import="com.q1labs.core.shared.util.UserUtils"
    	import="com.q1labs.sem.ui.semservices.OffenseSummaryForm"
    	import="com.q1labs.sem.ui.semservices.ForensicsForm"
    	import="com.q1labs.sem.ui.semservices.OffenseSourceForm"
	import="com.q1labs.core.types.networkevent.NetworkEventType"
	import="com.q1labs.sem.ui.semservices.OffenseSourceForm"
	import="com.q1labs.sem.ui.util.SemUtils"
	import="com.q1labs.sem.ui.semservices.AttackerForm"
	import="com.q1labs.events.ui.EventViewerApplication"
	import="com.q1labs.console.Globals"
	import="org.apache.struts.action.ActionErrors"
	import="com.q1labs.core.ui.paging.PagingInfo" 
	import="java.util.List"
	import="java.util.Map"
	import="java.util.HashMap"
	import="java.util.Iterator"
	import="java.util.Properties"
	import="java.util.regex.Matcher"
    	import=" java.util.regex.Pattern"
	import="java.net.InetAddress"
	import="java.net.URLEncoder"
	import="com.q1labs.core.ui.util.BreadcrumbTrailItem"
	import="com.q1labs.core.ui.util.BreadcrumbTrailProcessor"
	import="com.q1labs.core.ui.coreservices.UIUserManagementServices"
	import="com.q1labs.frameworks.logging.LoggerFactory"
	import="com.q1labs.frameworks.logging.ILogger"
	import="com.q1labs.core.dao.sem.ISEMDAOConstants"
	import="com.q1labs.uiframeworks.util.HTMLUtils"
	import="com.q1labs.qradar.ui.bean.UserForm"
	import="com.q1labs.qradar.ui.bean.RoleForm"
	import="com.q1labs.frameworks.session.ISessionContext"
	import="com.q1labs.sem.ui.guiactions.ViewOffenseEventsAction"
	import="com.q1labs.sem.ui.SemApplication"
	import="org.apache.commons.lang3.StringEscapeUtils"
	import="com.google.gson.Gson"
        import="com.resolve.gateway.qradar.QRadarAPI"
        import="com.resolve.gateway.qradar.OffenseServlet"
	%>
<%@ taglib uri="/tags/struts-bean" prefix="bean"%>
<%@ taglib uri="/tags/struts-html" prefix="html"%>
<%@ taglib uri="/tags/struts-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/q1labs-console.tld" prefix="console"%>
<%
	//Fetch Data
	OffenseSummaryForm bean = (OffenseSummaryForm) RequestUtils.getFormBean(pageContext);
	
	AttackerForm attacker = bean.getAttackerForm();

	String appName = "Sem";
	String pageId = "OffenseSummary";
	
	//We take the current page from the offenseSummaryForm
	String currentPageNumberString = bean.getCurrentPage();
	int currentPageNumber = Integer.parseInt(currentPageNumberString);
	
	String summaryToolbarName = ConsoleUtils.getJspProperty(appName, pageId, "summaryToolbar");
	String summaryId = HTMLUtils.escapeHTMLAttr(StringEscapeUtils.escapeEcmaScript(request.getParameter("summaryId")));
	boolean trailMaxSize = request.getAttribute(Globals.BREADCRUMB_TRAIL_MAX_SIZE_PARAM) != null;
	ILogger log = LoggerFactory.getLogger("Summary.jsp");

	List trail = BreadcrumbTrailProcessor.getBreadcrumbTrail(request);
	GUIAction eventLinkButton = GUIActionFactory.getInstance().getAction("viewOffenseEvents");
	eventLinkButton.init(pageContext, bean);
	
	GUIAction flowLinkButton = GUIActionFactory.getInstance().getAction("datamineOffense");
	flowLinkButton.init(pageContext, bean);
	
	String sessionToken = (String) request.getSession().getAttribute("SEC");
	
	//System.out.println("here: " + link);
	
	boolean userHasEventViewer = UserUtils.userHasCapabilities(RequestUtils.getUserName(pageContext), EventViewerApplication.CAPABILITY_EVENT_VIEWER);
	boolean hasManageOffenseCloseReason = UserUtils.userHasCapabilities(RequestUtils.getUserName(pageContext), SemApplication.CAPABILITY_SEMMANAGECLOSINGREASONS);
	boolean userHasForensics = UserUtils.userHasCapabilities(RequestUtils.getUserName(pageContext), "FORENSICS");
	request.setAttribute(com.q1labs.uiframeworks.Globals.SORTING_ENABLED_KEY, "false");

	// Add Resolve Action Buttons
        ServletContext context = getServletContext();

        if((String)context.getAttribute("host") == null)
            OffenseServlet.loadResolveProperties(config);

        String host = (String)context.getAttribute("resolve_host");
        String port = (String)context.getAttribute("resolve_port");
        String ssl =  (String)context.getAttribute("resolve_ssl");
        String keys =  (String)context.getAttribute("resolve_keys");
        String qradarSEC = (String)context.getAttribute("SEC");
	String qradarUserRole = (String)context.getAttribute("ROLE");
System.out.println("ssl = " + ssl);
System.out.println("keys = " + keys);
System.out.println("qradarUserRole = " + qradarUserRole);

        Boolean sslEnabled = (ssl == null || ssl.length() == 0 || ssl.equals("false"))?false:true;

        // verify user role against the user role loaded from the properties file
        String username = RequestUtils.getUserName(pageContext);
System.out.println("!!username = " + username);
	ISessionContext sessionContext = RequestUtils.getSessionContext((HttpServletRequest) pageContext.getRequest());
	UIUserManagementServices uiUserMgmtSvcs = (UIUserManagementServices) sessionContext.createObject(UIUserManagementServices.NAME);
	UserForm loggedInUser = uiUserMgmtSvcs.getDeployedUserByUsername(username);
	RoleForm r = loggedInUser.getRole();
	String roleName = r.getName();
System.out.println("ServletContext: username = " + username);
System.out.println("UIUserManagementServices: role = " + roleName);

	boolean isValidUser = false;
	if(qradarUserRole != null && qradarUserRole.equals(roleName))
		isValidUser= true;

        String params = "";
        String worksheetId = null;
        String protocol = (!sslEnabled)?"http://":"https://";

	if(isValidUser) {
	try {
                // Get offense detail
                String localHostAddress = InetAddress.getLocalHost().getHostAddress();
                QRadarAPI api = new QRadarAPI(localHostAddress, "", "/api/siem/", qradarSEC);
                String offenseDetail = api.getOffense(summaryId, null);
                StringBuilder sb = new StringBuilder();

                if(offenseDetail != null) {
                    Map<String, Object> attr = QRadarAPI.getJsonMap(offenseDetail);

                        if(attr != null) {
				if(keys != null && keys.trim().length() != 0) {
					String[] keyList = keys.split(",");
					for(int i=0; i<keyList.length; i++) {
						String key = keyList[i].trim();
						if(key.length() == 0)
							continue;
	                                        Object value = attr.get(key);
System.out.println("key = " + key);
System.out.println("value = " + value);
						if(value == null)
							continue;
       		                                String encoded = URLEncoder.encode(value.toString(), "UTF-8");
       		                                sb.append("&").append(key.toUpperCase()).append("=").append(encoded);
					}
                                }

                                params = sb.toString();
                        }
                }

		if(params.length() == 0)
			params = new String("ID=") + summaryId;
System.out.println("params = " + params);
                // Get latest worksheet id for this offense
                String resp = api.getOffenseNotes(summaryId, null, null, null, null);
System.out.println("resp = " + resp);
                if(!resp.contains("error:")) {
                        int currentId = 0;
                        long lastNoteTime = 0;
                        String lastNote = "";
                        List<Map<String, Object>> list = QRadarAPI.getJsonArrayMap(resp);

                        for(int i=0; i<list.size(); i++) {
                                Map<String, Object> offense = list.get(i);
                                Long createTime = (Long)offense.get("create_time");

                                if(createTime == null)
                                        continue;
                                if(createTime.longValue() > lastNoteTime) {
                                        String note = (String)offense.get("note_text");
                                        if(note != null && note.contains("Worksheet id is: ")) {
                                                Integer id = (Integer)offense.get("id");
                                                currentId = id.intValue();
                                                lastNoteTime = createTime.longValue();
                                                lastNote = note;
System.out.println("note = " + note);
                                        }
                                }
                        }
System.out.println("currentId = " + currentId);
                        if(currentId != 0 && lastNote.length() > 17 ) {
				int index = lastNote.indexOf("Worksheet id is: ");
				if(index != -1)
                                        worksheetId = lastNote.substring(index+17);
System.out.println("worksheetId = " + worksheetId);
                        }
                }
	} catch(Exception e) {
		e.printStackTrace();
	}	
	}
%>
<console:page pageId="<%=pageId%>" appName="Sem"
	disableRightClick="false" enableBreadcrumbTrail="true" helpNodeId="toolbarHelp">
	<body
		onmousedown="if(typeof(top.hideMenu)!='undefined')top.hideMenu(true)">
		<script type="text/javascript">
    	domapi.loadUnit("popupmenu");
		domapi.loadUnit("speedbutton");
		domapi.loadUnit("flyover");

		var userHasForensics = <%=userHasForensics%>;
		var targetTable = null;
		var categoriesTable = null;
		var annotationsTable = null;
		var eventsTable = null;
		var flowsTable = null;
		var summaryId = <%=summaryId%>;
		var tokenString = '<%=bean.getTokensAsString()%>';
		var tokens = tokenString.split('<%=ISEMDAOConstants.TOKEN_SEPARATOR%>'); 
		var dismissedCode = <%=bean.getDismissedCode()%>;
		var appName = '<%=appName%>';
		//bug 31553 take the current page and set it in the javascript for the offenseSummaryList.js
		var currentPageNumber = "<%=currentPageNumber%>";
		
		var hasManageOffenseCloseReason = <%=hasManageOffenseCloseReason%>;

		function getEventLink()
		{
			<%=eventLinkButton.getJavaScript()%>
		}

		function getFlowLink()
		{
			<%=flowLinkButton.getJavaScript()%>
		}
		
		function setActionsEnabled ()
		{
			
		}

		function offense_source_navigateToItem()
		{
			alert("not implemented");
		}

		function updateEventsTable( data )
		{
			var eventDiv = document && document.getElementById("events_div");
			
			if (eventDiv)
			{
				if (data.isError)
				{
					eventDiv.innerHTML = "<br/>" + data.errors[0].message;
				}
				else if( data.isException )
				{
					eventDiv.innerHTML = i18n("sem.jsp.summary.event.error");
				}
				else
				{
					eventDiv.innerHTML = data;
					try
					{
						eventsTable = new tableGrid("events_table", true);
					}
					catch (e)
					{
						eventsTable = new tableGrid("events_table");
					}
					eventsTable.navigateFunction = events_launchItem;
					eventsTable.launchFunction = events_launchItem;
					execScriptInHTML(data);
				}
			}
			 
		}

		function updateFlowsTable(data)
		{
			var flowDiv = document && document.getElementById("flows_div");
			
			if (flowDiv)
			{
				if (data.isError)
				{
					flowDiv.innerHTML = "<br/>" + data.errors[0].message;
				}
				else if( data.isException )
				{
					flowDiv.innerHTML = i18n("sem.jsp.summary.flow.error");
				}
				else
				{
					flowDiv.innerHTML = data;
					try
					{
						flowsTable = new tableGrid("flows_table", true);
					}
					catch (e)
					{
						flowsTable = new tableGrid("flows_table");
					}
					flowsTable.navigateFunction = flows_launchItem;
					flowsTable.launchFunction = flows_launchItem;
					execScriptInHTML(data);
				}
			}
		}
		
		function eventsTableTimeout()
		{
			var eventDiv = document.getElementById("events_div");
			
			if (eventDiv)
			{
				eventDiv.innerHTML = i18n("sem.jsp.summary.event.timoutError");
			}
		
		}
		
		function flowsTableTimeout()
		{
			var eventDiv = document.getElementById("flows_div");
			
			if (eventDiv)
			{
				eventDiv.innerHTML = i18n("sem.jsp.summary.flow.timoutError");
			}
		
		}
		
		function getRightClickedRow()
		{
			return [<%=summaryId%>,lastClicked.id];
		}

		function target_navigateToItem( targetId )
		{
			bTrailNavigateTo('do/core/summarylist?appName=Sem&pageId=TargetOffenseList&summaryId=' + targetId);
		}
		
		function target_launchItem( targetId )
		{
			var strName = "Details"; 
			var win = openWindow('do/core/summarylist?appName=Sem&pageId=TargetOffenseList&summaryId=' + targetId,strName,900,750);
			win.focus();
		}

		function attacker_navigateToItem( attackerId )
		{
			bTrailNavigateTo('do/core/summarylist?appName=Sem&pageId=AttackerOffenseList&summaryId=' + attackerId);
		}
		
		function category_launchItem( categoryId )
		{
		}
		
		function flow_category_launchItem( categoryId )
		{
			flow_event_category_launchItem( categoryId, false );
		}

		function event_category_launchItem( categoryId )
		{
			flow_event_category_launchItem( categoryId, true );
		}
		
		function flow_event_category_launchItem( categoryId, events )
		{

			var appName = "EventViewer";
			var pageId = "EventList";
			
			if (!events)
			{
				appName = "Surveillance";
				pageId = "FlowList";
			}

			var lastClicked = categoriesTable.getRowById(categoryId);
			var url = "do/ariel/arielSearch?appName=" + appName + "&pageId=" + pageId + "&dispatch=performSearch&newSearch=true&value(timeRangeType)=absolute&value(sortBy)=StartTimeKey&value(offenseId)=EQ%20" + 
			          "<%=bean.getId()%>" + 
			          "&value(startTimeAbs)=" + (((parseInt((lastClicked.getAttribute('startTime')) / 60000)) - 2) * 60000) + "&value(endTimeAbs)=" + (((parseInt((lastClicked.getAttribute('endTime')) / 60000)) + 2) * 60000) +  
			          "&value(category)=EQ%20" + lastClicked.getAttribute('type');
			          
			// A unique id is added to the window name in order to make it unique, allowing more then one window to be open at once, 
			// unless both buttons are clicked within one millisecond of each other.          
			openWindow("qradar/jsp/ArielSearchWrapper.jsp?url=" + escape(url),"catSearch" +categoryId + summaryId + domapi.guid(),1020,750, true, true);
		}
		
		function events_launchItem( eventId )
		{
			var handleId = eventsTable.getRowById(eventId).getAttribute("handleId");
			var recordIndex = eventsTable.getRowById(eventId).getAttribute("recordIndex");
			var url = "do/ariel/arielDetails?dispatch=viewDetails&recordIndex=" + recordIndex + "&handleId=" + handleId;
			openWindow(url,"eventDetails" + eventId.substr( eventId.lastIndexOf('-') + 1 ),1020,750, true, true);
		}	

		function flows_launchItem( eventId )
		{
			var handleId = flowsTable.getRowById(eventId).getAttribute("handleId");
			var recordIndex = flowsTable.getRowById(eventId).getAttribute("recordIndex");
			var url = "do/ariel/arielDetails?dispatch=viewDetails&recordIndex=" + recordIndex + "&handleId=" + handleId;
			openWindow(url,"flowDetails" + eventId.substr( eventId.lastIndexOf('-') + 1 ),1020,750, true, true);
		}
		
		function category_navigateToItem( categoryId )
		{
			category_launchItem(categoryId);
		}
		
		
		function getCatRightClickParams()
		{
			return [<%=summaryId%>, categoriesTable.lastClicked.id];
		}
		
		function getTargetRightClickParams()
		{
			return [<%=summaryId%>, targetTable.lastClicked.id];
		}

		function getAttackerRightClickParams()
		{
			return [<%=summaryId%>, attackerTable.lastClicked.id];
		}

		domapi.unloadHooks.push( function() 
		{
			if (targetTable && targetTable.free)
				targetTable.free();
			if (categoriesTable && categoriesTable.free)
				categoriesTable.free();
			if (eventsTable && eventsTable.free)
				eventsTable.free();
		});
		
   		domapi.addEvent( window, "load", function()
    	{
  

			<%
			if (summaryToolbarName!=null)
			{
				Toolbar summaryToolbar = new Toolbar(summaryToolbarName);
				if( summaryToolbar != null )
				{
					%>
					var summaryToolbarDiv = domapi.getElm('summaryToolbarSection');
<% if(isValidUser) { %>
var N594297126799587420Item = domapi.Speedbutton({
	parent:summaryToolbarDiv,
	hint:"Resolve Guided Solution",
	text:"Send to Resolve RR",
	rawText:"Resolve Guided Solution",
	src:"sem/img/icons/resolve.gif",
	enabled:true,
	onclick:function (m,i){ 
		var param = "<%= params%>";
		if(param.length != 0) {
			var url = "<%= protocol%><%= host%>:<%= port%>/resolve/jsp/rsclient.jsp?rid=true&autoCreate=true&REFERENCE=<%= summaryId%>&user=<%= username%><%= params%>";
// alert(url);
			window.open(url, '');
		}

		else
			alert("Sorry, failed to retrieve the offense detail. Please try again.");
	}
});

// registerActionReference("viewOffenseSummaryButton", N594297126799587420Item);

var N594297126799587421Item = domapi.Speedbutton({
	parent:summaryToolbarDiv,
	hint:"Resolve Worksheet",
	text:"Show Resolve Worksheet",
	rawText:"Show Resolve Result",
	src:"sem/img/icons/resolve.gif",
	enabled:true,
	onclick:function (m,i){ 
		console.log();
//	alert(summaryId);
// alert('worksheet id = <%= worksheetId %>');
	<% if(worksheetId != null && worksheetId.length() != 0) { %>
		var url = "<%= protocol%><%= host%>:<%= port%>/resolve/jsp/rsclient.jsp#RS.worksheet.Worksheet/id=<%= worksheetId %>&setActive=true";
		var targetWin = window.open(url, '');
// alert(url);
//			targetWin.focus();
	<% }
	else { %>
		alert("No worksheet available at this moment.");
	<% } %>
	}
});
<% } %>
					<%=summaryToolbar.getJavaScript(pageContext, "summaryToolbarDiv", bean.getId())%>
					<%
				}
				else
				{
					log.warn("Could not find a toolbar named " + summaryToolbar);
				}
			}
			%>

			var enableResize = true;
			 
			var targetsTableHeader = document.getElementById("targets_table_header");
			var targetsTableButtons = document.getElementById("targets_table_buttons");
			if( targetsTableHeader )
			{
				targetTable = new tableGrid("targets_table", enableResize); //hook up all the clicking & selection code.
				targetTable.launchFunction = function(e) { target_launchItem(e) };
				targetTable.navigateFunction = function(e) { target_navigateToItem(e) };
				dynamicPopupMenu( domapi.Elm({id:"targets_table"}), 'TargetList', getTargetRightClickParams  );
			}

			var attackersTableHeader = document.getElementById("attackers_table_header");
			var attackersTableButtons = document.getElementById("attackers_table_buttons");
			if( attackersTableHeader )
			{
				attackerTable = new tableGrid("attackers_table", enableResize); //hook up all the clicking & selection code.
				attackerTable.launchFunction = function(e) { attacker_launchItem(e) };
				attackerTable.navigateFunction = function(e) { attacker_navigateToItem(e) };
				dynamicPopupMenu( domapi.Elm({id:"attackers_table"}), 'AttackerList', getAttackerRightClickParams  );
			}
			
			var notesTableHeader = document.getElementById("notes_table_header");
			var notesTableButtons = document.getElementById("notes_table_buttons");
			if( notesTableHeader )
			{
				notesTable = new tableGrid("notes_table", enableResize); //hook up all the clicking & selection code.
				//notesTable.launchFunction = function(e) { notes_launchItem(e) };
				//notesTable.navigateFunction = function(e) { notes_navigateToItem(e) };
				//dynamicPopupMenu( domapi.Elm({id:"notes_table"}), 'NotesList', getNotesRightClickParams  );
			}

			var offenseSearchResultsTableHeader = document.getElementById("offense_search_results_table_header");
			var offenseSearchResultsTableButtons = document.getElementById("offense_search_results_table_buttons");
			if ( offenseSearchResultsTableHeader ) {
				searchResultTable = new tableGrid("offense_search_results_table", enableResize);
				searchResultTable.launchFunction = function(e) { searchResult_launchItem(e) };
				searchResultTable.navigateFunction = function(e) { searchResult_launchItem(e) };
			}

			var userTableHeader = document.getElementById("user_table_header");
			var userTableButtons = document.getElementById("user_table_buttons");
			if( userTableHeader )
			{
				userTable = new tableGrid("user_table", enableResize); //hook up all the clicking & selection code.
				//userTable.launchFunction = function(e) { users_launchItem(e) };
				//userTable.navigateFunction = function(e) { users_navigateToItem(e) };
				//dynamicPopupMenu( domapi.Elm({id:"user_table"}), 'UserList', getUserRightClickParams  );
			}

			var deviceTableHeader = document.getElementById("device_table_header");
			var deviceTableButtons = document.getElementById("device_table_buttons");
			if( deviceTableHeader )
			{
				deviceTable = new tableGrid("device_table", enableResize); //hook up all the clicking & selection code.
				//deviceTable.launchFunction = function(e) { device_launchItem(e) };
				//deviceTable.navigateFunction = function(e) { device_navigateToItem(e) };
				//dynamicPopupMenu( domapi.Elm({id:"device_table"}), 'DeviceList', getDeviceRightClickParams  );
			}
			
			
			var categoriesTableHeader = document.getElementById("categories_table_header");
			var categoriesTableButtons = document.getElementById("categories_table_buttons");
			if( categoriesTableHeader )
			{
				categoriesTable = new tableGrid("categories_table", enableResize); //hook up all the clicking & selection code.

				if (<%=userHasEventViewer%>)
				{
					categoriesTable.launchFunction = function(e) { category_launchItem(e) };
					categoriesTable.navigateFunction = function(e) { category_navigateToItem(e) };
				}
				
				dynamicPopupMenu( domapi.Elm({id:"categories_table"}), 'CategoryList', getCatRightClickParams  );
			}

			var annotationsTableHeader = document.getElementById("annotations_table_header");
			var annotationsTableButtons = document.getElementById("annotations_table_buttons");
			if( annotationsTableHeader )
			{
				annotationsTable = new tableGrid("annotations_table", enableResize); //hook up all the clicking & selection code.
				annotationsTable.launchFunction = function(e) { return; };
				annotationsTable.navigateFunction = function(e) { return; };
			}
			
			var eventsTableHeader = document.getElementById("events_div_header");
			var eventsTableButtons = document.getElementById("events_table_buttons");
			var flowsTableButtons = document.getElementById("flows_table_buttons");
			<%
				String toolbarName = null;
				Toolbar tableToolbar = null;

				if( bean.getTopNotesToShow() > 0 )
				{
					toolbarName = "ViewNotesToolbar";
					tableToolbar = new Toolbar( toolbarName );
					out.print(tableToolbar.getJavaScript( pageContext, "notesTableButtons", bean.getId()));
					
				}
				
				if( bean.getTopOffenseSearchResultsToShow() > 0 ) {
					toolbarName = "ViewOffenseSearchResultsToolbar";
					tableToolbar = new Toolbar( toolbarName );
					out.print(tableToolbar.getJavaScript( pageContext, "offenseSearchResultsTableButtons", bean.getId()));
				}

				if( bean.getTopCategoriesToShow() > 0 )
				{
					toolbarName = "ViewOffenseCategoryListToolbar";
					tableToolbar = new Toolbar( toolbarName );
					out.print(tableToolbar.getJavaScript(pageContext, "categoriesTableButtons", bean.getId()));
				}

				if( bean.getTopTargetsToShow() > 0 && bean.getTopTargetsSize()>0 )
				{
					toolbarName = "ViewOffenseTargetListToolbar";
					tableToolbar = new Toolbar( toolbarName );
					out.print(tableToolbar.getJavaScript(pageContext, "targetsTableButtons", bean.getId()));
				}

				if( bean.getTopAttackersToShow() > 0 )
				{
					toolbarName = "ViewOffenseAttackerListToolbar";
					tableToolbar = new Toolbar( toolbarName );
					out.print(tableToolbar.getJavaScript(pageContext, "attackersTableButtons", bean.getId()));
				}

				if( bean.getTopAnnotationsToShow() > 0 )
				{
					toolbarName = "ViewOffenseAnnotationListToolbar";
					tableToolbar = new Toolbar( toolbarName );
					out.print(tableToolbar.getJavaScript(pageContext, "annotationsTableButtons", bean.getId()));
				}

				if( bean.getTopEventsToShow() > 0 )
				{
					toolbarName = "ViewOffenseEventsToolbar";
					tableToolbar = new Toolbar( toolbarName );
					out.print(tableToolbar.getJavaScript(pageContext, "eventsTableButtons", bean.getId()));
				}
				
				if( bean.getTopFlowsToShow() > 0 )
				{
					toolbarName = "ViewOffenseFlowsToolbar";
					tableToolbar = new Toolbar( toolbarName );
					out.print(tableToolbar.getJavaScript(pageContext, "flowsTableButtons", bean.getId()));
				}
				
				if( bean.getTopDevicesToShow() > 0 )
				{
					toolbarName = "ViewOffenseDevicesToolbar";
					tableToolbar = new Toolbar( toolbarName );
					out.print(tableToolbar.getJavaScript(pageContext, "deviceTableButtons", bean.getId()));
				}
				
				if( bean.getTopUsersToShow() > 0 )
				{
					toolbarName = "ViewoffenseusersToolbar";
					tableToolbar = new Toolbar( toolbarName );
					out.print(tableToolbar.getJavaScript(pageContext, "userTableButtons", bean.getId()));
				}
			%>
			
			//var attackerHeader = domapi.getElm('attacker_header');
			//var attackerDetailsButton = domapi.Speedbutton({parent:attackerHeader,hint:"View all details of this attacker",text:"Details",src:"sem/img/icons/attacker.gif",enabled:true,onclick:attacker_navigateToItem});
	
//			var notesHeader = domapi.getElm('notes_table_header');
	//		var notesDetailsButton = domapi.Speedbutton({parent:notesHeader,hint:"Add additional note",text:"Add note",src:"core/img/icons/edit.gif",enabled:true,onclick:function(){notes(summaryId)} });
			
			//getEventsForParams( PageContext pageContext, long token, long startTime, long endTime, int numberOfEvents ) throws Exception
			//getEventsForParams
			
			<%
			long oneHour = 3600000;
			long startTime = bean.getEndTimeAbs() - oneHour;
			long endTime = bean.getEndTimeAbs();

			//Don't go outside valid range
			if( startTime < bean.getStartTimeAbs() )
				startTime = bean.getStartTimeAbs();
				
			//Make sure they are not the same, BUG: 10621
			if ( startTime == endTime )
				endTime += 500;
			if (bean.getTopEventsToShow()>0)
			{
				
			%>
			applicationMethod("EventViewer","getTopEventsTable","{offenseId:'" + summaryId + "',startTime:'" + <%=startTime%> + "',endTime:'" + <%=endTime%> + "',numberOfEvents:'<%=bean.getTopEventsToShow()%>'}", "EventList", updateEventsTable, eventsTableTimeout);
			<%
			}
			
			if (bean.getTopFlowsToShow()>0)
			{
				
			%>
			applicationMethod("Surveillance","getTopFlowsTable","{offenseId:'" + summaryId + "',startTime:'" + <%=startTime%> + "',endTime:'" + <%=endTime%> + "',numberOfFlows:'<%=bean.getTopFlowsToShow()%>'}", "FlowList", updateFlowsTable, flowsTableTimeout);
			<%
			}
			%>
      	} );
   		sessionToken = null;
   		require(["dojo/cookie","dojo/json","dojo/fx/Toggler","dojo/fx","dojo/dom-construct","dojo/dom","dojo/ready", "dojo/on"], function(cookie,json,toggler,coreFx,domConstruct,dom,ready, on){
   			ready(function(){
   				var container = new toggler({
   				    node: "forensicsContainer"
   				  });
   				
				container.hide();

				if (userHasForensics)
				{
					sessionToken = "<%=sessionToken%>";
					
					var forensicsID = summaryId + "$F0r3nsic5$"
	  
					var xhrArgs = {
   						    url: "/forensics/workFlowActions.php?session="+sessionToken+"&action=incidentinfo&tagvalue="+forensicsID,
   						    handleAs: "json",
   						    load: function(data){
   						   		loadForensicsTable(data);
   						    },
   						    error: function(error){
   						    	// Do nothing
   						    }
   						  }
   						
					// Call the asynchronous xhrGet
					var deferred = dojo.xhrPost(xhrArgs);
   				
					function loadForensicsTable(data)
					{
						
						if (data.length > 0)
						{
							data.forEach(function(ip)
							{
								if (ip.stime.length > 14 && ip.etime.length > 14){
									var emonth = null;
									var smonth = null;
									if ((ip.stime.substring(0,2) - 1) < 0)
										smonth = 12;
									else
										smonth = ip.stime.substring(0,2) - 1;
									if ((ip.etime.substring(0,2) - 1) < 0)
										emonth = 12;
									else
										emonth = ip.etime.substring(0,2) - 1;
									
									var start = new Date(ip.stime.substring(4,8),smonth,ip.stime.substring(2,4),ip.stime.substring(9,11),ip.stime.substring(12,14));
									var end = new Date(ip.etime.substring(4,8),emonth,ip.etime.substring(2,4),ip.etime.substring(9,11),ip.etime.substring(12,14));
								}
								else
								{
									var start = "";
									var end = "";
								}
								var uuid = (S4() + S4() + "-" + S4() + "-4" + S4().substr(0,3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
								var node = "<tr ondblclick=\"javascript:parent.top.setActiveTab('Forensics','/forensics/index.php?session='+sessionToken+'&q=IPAddress:" +ip.ip+"');\"><td>"+ ip.casename + "</td><td>"+ ip.collection +"</td><td><nobr><span id=\""+uuid+"\" value=" + ip.ip +" title=\"\"><span>"+ip.ip+"</span></span></nobr></td><td>"+ start.toLocaleString() +"</td><td>"+ end.toLocaleString() +"</td><td>"+ ip.status +"</td></tr>";
								var refNode = dom.byId("forensicsTableBody");
								domConstruct.place(node, refNode, 'last');
								dynamicPopupMenu(domapi.getElm(uuid),"ipPopup",ip.ip,"ip");
							});
							
							container.show();
							
							dojo.query('#forensicsIpsTable tr').on('click', function(evt) {        
								   dojo.toggleClass(this, 'datarowselected'); 
							});
						}
					}
				}
   			});
   		});	
   		
   		function S4() {
   		    return (((1+Math.random())*0x10000)|0).toString(16).substring(1); 
   		}

   		
      		
		</script>
<% 
	 Locale locale = RequestUtils.getLocale((HttpServletRequest)pageContext.getRequest());
	 List<String> fileNames= Arrays.asList("/console/nls/summary.js");
	 List<String> jsFiles=ConsoleUtils.getLocaleJsFiles(fileNames, locale);
	 for(int i = 0; i < jsFiles.size(); i++) { %>
	 <script language="javascript" src="<%=jsFiles.get(i) %>"></script>
<% } %>

		<div class="toolbar">
			<!-- Displays toolbar background -->
			<div class="shade">
				<div class="breadcrumb">
				<%
				if (trail != null && trail.size() > 0)
				{
					for (int i = 0; i < trail.size(); ++i)
					{
						BreadcrumbTrailItem item = (BreadcrumbTrailItem) trail.get(i);
						String url = item.getUrl();
				
						//bug 31553
						if(url != null){
							if(url.contains("pageNumber=") ){
								if(currentPageNumberString != null && !currentPageNumberString.equals("")){
									String regex = "^(.*[\\?|&])pageNumber=[0-9]*(.*)$";
								 	Pattern pattern = Pattern.compile(regex);			
									Matcher matcher = pattern.matcher(url);
									matcher.find();
									url = matcher.replaceAll(matcher.group(1) + "pageNumber=" + currentPageNumberString + matcher.group(2));
								}else{
									String regex = "^(.*)[\\?|&]pageNumber=[0-9]*(.*)$";
								 	Pattern pattern = Pattern.compile(regex);			
									Matcher matcher = pattern.matcher(url);
									matcher.find();
									url = matcher.replaceAll(matcher.group(1) + matcher.group(2));
								}
							}else{	
								if(currentPageNumberString != null && !currentPageNumberString.equals(""))
									url = url + "&pageNumber=" + currentPageNumberString;
							}
						}
								
						if (i == (trail.size() - 1))
						{
							%><%=HTMLUtils.escapeURL(item.getLabel())%><%
						}
						else
						{
							//For the first item (go back to the main list),
							//we use the regular "navigateTo" to avoid the
							//post dialog box in the browser. This url
							//should not be very big anyway.
							
							if (i==0)
							{
								%><a href="<%=HTMLUtils.escapeURL(url)%>"><%=HTMLUtils.escapeURL(item.getLabel())%></a><%			
							}
							else
							{
 								url = url + "&trailPos=" + i;							
								%><a href="javascript:bTrailNavigateTo('<%=HTMLUtils.escapeURL(url)%>');"><%=HTMLUtils.escapeURL(item.getLabel())%></a><%			
							}
						
							if (trailMaxSize && i==0)
							{
								out.write(RequestUtils.getLocalizedText(pageContext, "sem.breadcrumb.label.trailMaxSize"));
							}
							else
							{
								out.write(RequestUtils.getLocalizedText(pageContext, "sem.breadcrumb.label.gt"));			
							}		
						}
					}
				}
				%>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div style="clear: both;">
			<div id="scrollingDiv" style="height:0px" style="clear:both;">
			<logic:messagesPresent message="false">
	    		<div class="errorMessageSection">
	    			<html:errors/>
	    		</div>
			</logic:messagesPresent>
				<jsp:include page="../summaryList/OffenseSummary.jsp" flush="true" />
				
				<console:summaryListTable headerKey="sem.offenseSummary.notesList.headers" numToShow="<%=bean.getTopNotesToShow() %>" 
						appName="Sem" pageId="OffenseSummary" context="NotesLess" tableId="notes_table" data="<%=bean.getTopNotes()%>" 
						displayType="sem.jsp.summary.displayType.notes"></console:summaryListTable>
				
				<console:summaryListTable headerKey="sem.offenseSummary.offenseSearchResultList.headers" numToShow="<%=bean.getTopOffenseSearchResultsToShow() %>" 
						appName="Sem" pageId="OffenseSummary" context="OffenseSearchResultsList" tableId="offense_search_results_table" data="<%=bean.getTopOffenseSearchResults()%>" 
						displayType="sem.jsp.summary.displayType.offenseSearchResults"></console:summaryListTable>
				
				<br/>
				<div id="forensicsContainer" style="clear:both;display:block;"><!-- //TODO: change to default none -->
				<div class="gridHeader" id="forensicIps_header" style="clear:both;"> 
					<div class="heading" id="forensicIps_header_title">
						<%=RequestUtils.getLocalizedText(pageContext, "sem.jsp.summary.forensics.header") %>
					</div>
				</div>
				<table class="grid dashboard-grid resizeableGrid" width="100%" id="forensicsIpsTable" summary style="cursor: default;">
					<thead>
						<tr>
							<th><%=RequestUtils.getLocalizedText(pageContext, "sem.jsp.summary.forensics.case") %></th>
							<th><%=RequestUtils.getLocalizedText(pageContext, "sem.jsp.summary.forensics.collection") %></th>
							<th><%=RequestUtils.getLocalizedText(pageContext, "sem.jsp.summary.forensics.ip") %></th>
							<th><%=RequestUtils.getLocalizedText(pageContext, "sem.jsp.summary.forensics.start") %></th>
							<th><%=RequestUtils.getLocalizedText(pageContext, "sem.jsp.summary.forensics.end") %></th>
							<th><%=RequestUtils.getLocalizedText(pageContext, "sem.jsp.summary.forensics.status") %></th>
						</tr>
					</thead>
					<tbody id="forensicsTableBody">
					
					
					</tbody>
				</table>
				</div>
				<br/>
				
				<console:summaryListTable headerKey="sem.offenseSummary.attackerList.headers" numToShow="<%=bean.getTopAttackersToShow() %>" 
						appName="Sem" pageId="OffenseSummary" context="OffenseTopAttackersList" tableId="attackers_table" data="<%=bean.getTopAttackers()%>" 
						displayType="sem.jsp.summary.displayType.source"></console:summaryListTable>
				
				<%-- BUG 32132 - Do not display top targets if the offense has no local targets --%> 
				<%
				if (bean.getTopTargetsSize()>0)
				{
				%>
				<console:summaryListTable headerKey="sem.offenseSummary.targetList.headers" numToShow="<%=bean.getTopTargetsToShow() %>" 
						appName="Sem" pageId="OffenseSummary" context="OffenseTopTargetsList" tableId="targets_table" data="<%=bean.getTopTargets()%>" 
						displayType="sem.jsp.summary.displayType.destination"></console:summaryListTable>
				
				<%
				}
				%>
				<console:summaryListTable headerKey="sem.offenseSummary.deviceList.headers" numToShow="<%=bean.getTopDevicesToShow() %>" 
						appName="Sem" pageId="OffenseSummary" context="OffenseDeviceList" tableId="device_table" data="<%=bean.getTopDevices()%>" 
						displayType="sem.jsp.summary.displayType.logSource"></console:summaryListTable>
				
				<console:summaryListTable headerKey="sem.offenseSummary.userList.headers" numToShow="<%=bean.getTopUsersToShow() %>" 
						appName="Sem" pageId="OffenseSummary" context="OffenseUserList" tableId="user_table" data="<%=bean.getTopUsers()%>" 
						displayType="sem.jsp.summary.displayType.users"></console:summaryListTable>
		
				<console:summaryListTable headerKey="jsp.Sem.TopCategoryList.headers" numToShow="<%=bean.getTopCategoriesToShow() %>" 
						appName="Sem" pageId="OffenseSummary" context="TopCategoryList" tableId="categories_table" data="<%=bean.getTopCategories()%>" 
						displayType="sem.jsp.summary.displayType.categories"></console:summaryListTable>
										
				<%
				if (bean.getTopEventsToShow() > 0)
				{
				%>
				<div class="gridHeader" id="events_div_header">
					<div id="events_table_title" class="heading">
						<bean:message key="sem.offenseSummary.lastEvents" arg0="<%=bean.getTopEventsToShow()+\"\"%>" />
					</div>
					<div id="events_table_buttons" class="buttons">
					</div>
				</div>
				<div id="events_div">
						<div align="center"><br/><%=RequestUtils.getLocalizedText(pageContext, "sem.jsp.summary.events.fetch") %></div>
				</div>
				<br />
				<%
				}
				%>
				
				<%
				if (bean.getTopFlowsToShow() > 0)
				{
				%>
				<div class="gridHeader" id="flows_div_header">
					<div id="flows_table_title" class="heading">
						<bean:message key="sem.offenseSummary.lastFlows" arg0="<%=bean.getTopFlowsToShow()+\"\"%>" />
					</div>
					<div id="flows_table_buttons" class="buttons">
					</div>
				</div>
				<div id="flows_div">
						<div align="center"><br/><%=RequestUtils.getLocalizedText(pageContext, "sem.jsp.summary.flows.fetch") %></div>
				</div>
				<br />
				<%
				}
				%>

				<console:summaryListTable headerKey="jsp.Sem.AnnotationList.headers" numToShow="<%=bean.getTopAnnotationsToShow() %>" 
						appName="Sem" pageId="OffenseSummary" context="AnnotationList" tableId="annotations_table" data="<%=bean.getTopAnnotations()%>" 
						displayType="sem.jsp.summary.displayType.annotations"></console:summaryListTable>
			</div>
		</div>
		<!-- Display footer -->
		<console:Footer displayLogo="false"> 
			<div id="pageInfo"><%=RequestUtils.getLocalizedText(pageContext, "sem.jsp.summary.footer.elapsed", bean.getRequestElapsedTime()) %></div> 
		</console:Footer> 
	</body>
</console:page>
