/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.qradar;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.lang3.RandomStringUtils;


public class CryptUtils
{
    // default prefix
    public static String prefixLocal = "ENC:";
    public static String prefix128Local = "ENC1:";

    // crypto
    final static String CRYPTKEY = "a short phrase for qradar";
    static Crypt crypt = null;
    static Crypt cryptAES = null;
    static String encData;
    
    public static boolean isEncrypted(String value)
    {
        boolean result = false;
        
        if (value != null && value.startsWith(prefixLocal))
        {
            result = true;
        }
        else if (value != null && value.startsWith(prefix128Local))
        {
            result = true;
        }
        
        return result;
    } // isEncrypted

    public static String encrypt(String value) throws Exception
    {
        // by default the encryption is AES128
        return encryptAES(value, prefix128Local);
    } // encrypt

    public static String encryptAES(String value, String prefix) throws Exception
    {
        String result = value;

        if (cryptAES == null)
        {
            // crypt = new Crypt(Crypt.ENCRYPTION_SCHEME_DESEDE, CRYPTKEY);
            cryptAES = new Crypt(Crypt.ENCRYPTION_SCHEME_AES128, CRYPTKEY);
        }

        // add prefix if defined
        if (prefix != null && value != null && !value.startsWith(prefix))
        {
            result = prefix + cryptAES.encryptAES(value);
        }

        return result;
    } // encrypt

    public static String encryptDES(String value, String prefix) throws Exception
    {
        String result = value;

        if (crypt == null)
        {
            crypt = new Crypt(Crypt.ENCRYPTION_SCHEME_DESEDE, CRYPTKEY);
        }

        // add prefix if defined
        if (prefix != null && value != null && !value.startsWith(prefix) && prefix != null && prefix.equals(prefixLocal))
        {
            result = prefix + crypt.encrypt(value);
        }

        return result;
    } // encrypt

    public static String decryptUTF8(String value) throws Exception
    {
        String decryptString = value;

        if (value != null && value.startsWith(prefixLocal))
        {
            decryptString = decryptUTF8(value, prefixLocal);
        }
        else if (value != null && value.startsWith(prefix128Local))
        {
            decryptString = decryptAES128UTF8(value, prefix128Local);
        }

        return decryptString;
    } // decrypt

    public static String decrypt(String value) throws Exception
    {
        String decryptString = value;

        if (value != null && value.startsWith(prefixLocal))
        {
            decryptString = decrypt(value, prefixLocal);
        }
        else if (value != null && value.startsWith(prefix128Local))
        {
            decryptString = decryptAES128(value, prefix128Local);
        }

        return decryptString;
    } // decrypt

    public static String decryptAES128(String value, String prefix) throws Exception
    {
        String result = value;

        if (cryptAES == null)
        {
            cryptAES = new Crypt(Crypt.ENCRYPTION_SCHEME_AES128, CRYPTKEY);
        }

        // remove prefix if defined
        if (prefix != null && value != null && value.startsWith(prefix) && prefix.equals(prefix128Local))
        {
            value = value.substring(prefix.length());
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    result = cryptAES.decryptAES128(value);
                    break;
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    try
                    {
                        if (i < 2)
                        {
                            Thread.sleep(2000);
                        }
                    }
                    catch (InterruptedException ie)
                    {
                        ie.printStackTrace();
                    }
                }
            }
        }

        return result;

    } // decrypt

    public static String decryptAES128UTF8(String value, String prefix) throws Exception
    {
        String result = value;

        if (cryptAES == null)
        {
            cryptAES = new Crypt(Crypt.ENCRYPTION_SCHEME_AES128, CRYPTKEY);
        }

        // remove prefix if defined
        if (prefix != null && value != null && value.startsWith(prefix) && prefix.equals(prefix128Local))
        {
            value = value.substring(prefix.length());
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    result = cryptAES.decryptAES128UTF8(value);
                    break;
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    try
                    {
                        if (i < 2)
                        {
                            Thread.sleep(2000);
                        }
                    }
                    catch (InterruptedException ie)
                    {
                        ie.printStackTrace();
                    }
                }
            }
        }

        return result;

    } // decrypt

    public static String decryptUTF8(String value, String prefix) throws Exception
    {
        String result = value;

        if (crypt == null)
        {
            crypt = new Crypt(Crypt.ENCRYPTION_SCHEME_DESEDE, CRYPTKEY);
        }

        // remove prefix if defined
        if (prefix != null && value != null && value.startsWith(prefix) && prefix != null && prefix.equals(prefixLocal))
        {
            value = value.substring(prefix.length());
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    result = crypt.decryptUTF8(value);
                    break;
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    try
                    {
                        if (i < 2)
                        {
                            Thread.sleep(2000);
                        }
                    }
                    catch (InterruptedException ie)
                    {
                        ie.printStackTrace();
                    }
                }
            }
        }

        return result;

    } // decrypt

    public static String decrypt(String value, String prefix) throws Exception
    {
        String result = value;

        if (crypt == null)
        {
            crypt = new Crypt(Crypt.ENCRYPTION_SCHEME_DESEDE, CRYPTKEY);
        }

        // remove prefix if defined
        if (prefix != null && value != null && value.startsWith(prefix) && prefix != null && prefix.equals(prefixLocal))
        {
            value = value.substring(prefix.length());
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    result = crypt.decrypt(value);
                    break;
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    try
                    {
                        if (i < 2)
                        {
                            Thread.sleep(2000);
                        }
                    }
                    catch (InterruptedException ie)
                    {
                        ie.printStackTrace();
                    }
                }
            }
        }

        return result;

    } // decrypt

    public static String encryptUnix(String value, String salt) throws Exception
    {
        return encryptUnix(value, prefixLocal);
    } // encryptUnix

    public static String encryptMD5(String value)
    {
        // return CryptMD5.encrypt(value);

        String result = value;
        try
        {
        	result = DigestUtils.md5Hex(value);
//            MD5 md5 = new MD5();
//            md5.update(value, null);
//            result = md5.asHex();
        }
        catch (Exception e)
        {
        }

        return result;
    } // encryptMD5

    public static String getENCData()
    {
        return Crypt.getENCData();
    } // getENCDaata

    public static void setENCData(String encData) throws Exception
    {
        Crypt.setENDData(encData);
    } // setENCData
    
    public static String generatePassword()
    {
        return RandomStringUtils.randomAlphanumeric(RandomUtils.nextInt(13) + 8); 
    } // generatePassword
  
} // CryptUtils
