/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.qradar;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

import com.resolve.gateway.qradar.Crypt;
import com.resolve.gateway.qradar.CryptUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

public class QRadarAPI
{
    private String host;
    private String port;
    private String url;
    private String sec;
    private boolean selfSigned;
    
    public QRadarAPI(String host, String port, String uri, String sec) throws Exception {
        
        if(host == null || host.length() == 0 || sec == null || sec.length() == 0)
            throw new Exception("No host connection information is provided.");
        
        this.host = host;
        this.port = port;
        this.url = getServerUrl(uri);
        this.sec = sec;
        this.selfSigned = false;
    }
    
    public QRadarAPI(String host, String port, String uri, String sec, boolean selfSigned) throws Exception {
        
        if(host == null || host.length() == 0 || sec == null || sec.length() == 0)
            throw new Exception("No host connection information is provided.");
        
        this.host = host;
        this.port = port;
        this.url = getServerUrl(uri);
        this.sec = sec;
        this.selfSigned = selfSigned;
    }
    
    private String getServerUrl(String uri) {
        
        StringBuilder sb = new StringBuilder();
        sb.append("https://").append(host);
        if(port != null && port.length() != 0)
            sb.append(":").append(port);
        
        sb.append(uri);

        return sb.toString();
    }
    
    public String getOffense(String offenseId, String fields) throws Exception {
        
        if(offenseId == null || offenseId.length() == 0)
            throw new Exception("No offense id is provided.");
        
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("sec", sec);
        
        String requestUrl = url + "offenses/" + offenseId;
        
        if(fields != null && fields.length() != 0)
            requestUrl = requestUrl + "?fields=" + URLEncoder.encode(fields, "UTF-8");
// System.out.println(requestUrl);
        return getHTTPSURLResponse(requestUrl, "GET", headers, null, selfSigned);
//        RestClient client = new RestClient("https://" + host + ":" + port, "", "", false, false, true, true);
//        String response = client.callGet(URI + "/Query", null, queryRequest.toString(), "application/json", "application/json");

    }
    
    public String getOffenses(Integer start, Integer end, String limit, String offset, String fields, String filter) throws Exception {

        String requestUrl = url + "offenses";
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("sec", sec);
        
        if(start != null && end != null) {
            String range = "items=" + start.toString() + "-" + end.toString();
            headers.put("range", range);
        }
        
        if(limit != null && limit.length() != 0) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "limit=" + limit;
        }
        
        if(offset != null && offset.length() != 0) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "offset=" + offset;
        }
        
        if(fields != null && fields.length() != 0) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "fields=" + URLEncoder.encode(fields, "UTF-8");
        }
        
        if(filter != null && filter.length() != 0) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "filter=" + URLEncoder.encode(filter, "UTF-8");
        }
// System.out.println(requestUrl);   
        return getHTTPSURLResponse(requestUrl, "GET", headers, null, selfSigned);
    }
    
    public String postOffense(String offenseId, String status, String assignedTo, String fields, Boolean isProtected, Boolean followUp, Integer closingReasonId) throws Exception {

        if(offenseId == null || offenseId.length() == 0)
            throw new Exception("No offense id is provided.");

        Map<String, String> headers = new HashMap<String, String>();
        headers.put("sec", sec);
        
        String requestUrl = url + "offenses/" + offenseId;
        
        if(status != null && status.length() != 0) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "status=" + status;
        }
        
        if(assignedTo != null && assignedTo.length() != 0) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "assignedTo=" + URLEncoder.encode(assignedTo, "UTF-8");
        }
        
        if(fields != null && fields.length() != 0) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "fields=" + URLEncoder.encode(fields, "UTF-8");
        }
        
        if(isProtected != null) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "protected=" + isProtected;
        }
        
        if(followUp != null) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "follow_up=" + followUp;
        }
        
        if(closingReasonId != null) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "closing_reason_id=" + closingReasonId;
        }
// System.out.println(requestUrl);   
        return getHTTPSURLResponse(requestUrl, "POST", headers, null, selfSigned);
    }
    
    public String getOffenseNote(String offenseId, String noteId, String fields) throws Exception {
        
        if(offenseId == null || offenseId.length() == 0 || noteId == null || noteId.length() == 0)
            throw new Exception("No offense id or note id is provided.");
        
        try {
            new Integer(offenseId);
            new Integer(noteId);
        } catch(Exception e) {
            throw new Exception("Offense id or note id must be a number.");
        }
        
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("sec", sec);
        
        String requestUrl = url + "offenses/" + offenseId + "/notes/" + noteId;

        if(fields != null && fields.length() != 0)
            requestUrl = requestUrl + "?fields=" + URLEncoder.encode(fields, "UTF-8");
// System.out.println(requestUrl);       
        return getHTTPSURLResponse(requestUrl, "GET", headers, null, selfSigned);
    }
    
    public String getOffenseNotes(String offenseId, String fields, String filter, Integer start, Integer end) throws Exception {
        
        if(offenseId == null || offenseId.length() == 0)
            throw new Exception("No offense id is provided.");
        
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("sec", sec);
        
        String requestUrl = url + "offenses/" + offenseId + "/notes";
        
        if(fields != null && fields.length() != 0) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "?" + URLEncoder.encode(fields, "UTF-8");
        }
        
        if(filter != null && filter.length() != 0) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "filter=" + URLEncoder.encode(filter, "UTF-8");
        }
        
        if(start != null && end != null) {
            try {
                String range = "items=" + start.toString() + "-" + end.toString();
                headers.put("range", range);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
// System.out.println(requestUrl);
        return getHTTPSURLResponse(requestUrl, "GET", headers, null, selfSigned);
    }
    
    public String postOffenseNote(String offenseId, String note, String fields) throws Exception {

        if(offenseId == null || offenseId.length() == 0 || note == null || note.length() == 0)
            throw new Exception("No offense id or note is provided.");

        Map<String, String> headers = new HashMap<String, String>();
        headers.put("sec", sec);
        
        String requestUrl = url + "offenses/" + offenseId + "/notes?note_text=" + URLEncoder.encode(note, "UTF-8");
        
        if(fields != null && fields.length() != 0)
            requestUrl = requestUrl + "&fields=" + URLEncoder.encode(fields, "UTF-8");
// System.out.println(requestUrl);   
        return getHTTPSURLResponse(requestUrl, "POST", headers, null, selfSigned);
    }
    
    public static Map<String, String> parseOffenseNotes(String notes) throws Exception {
        
        if(notes == null || notes.length() == 0)
            throw new Exception("note is not available.");
        
        Map<String, String> noteList = new HashMap<String, String>();
        
        try {
            JSONArray array = JSONArray.fromObject(notes);
            
            for(int i=0; i<array.size(); i++) {
                JSONObject note = array.getJSONObject(i);
                if(note == null)
                    continue;
                
                String id = note.getString("id");
                String text = note.getString("note_text");
                
                if(id == null || id.length() == 0 || text == null || text.length() == 0)
                    continue;

                noteList.put(id,  text);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

        return noteList;
    }
    
    public static Map<String, Object> getJsonMap(String input) {
        
        Map<String, Object> map = new HashMap<String, Object>();
        
        try {
            JSONObject attr = JSONObject.fromObject(input);
            for(Iterator<String> it = attr.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                Object value = attr.get(key);
// System.out.println(key + " = " + value);
                   map.put(key, value);
            }
        } catch(Exception e) {
            System.err.println(input);
            e.printStackTrace();
        }

        return map;
    }
    
    public static List<Map<String, Object>> getJsonArrayMap(String input) {
        
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        
        try {
            JSONArray jsonArray = (JSONArray)JSONSerializer.toJSON(input);
            for(int i=0; i<jsonArray.size(); i++) {
                Map<String, Object> attr = (Map<String, Object>)jsonArray.get(i);
                Set<String> keySet = attr.keySet();
                
                Map<String, Object> offense = new HashMap<String, Object>();
                for(Iterator<String> it = keySet.iterator(); it.hasNext();) {
                    String key = it.next();
                    Object value = attr.get(key);
// System.out.println(key + " = " + value);
                    offense.put(key, value);
                }
                
                list.add(offense);
            }
        } catch(Exception e) {
            System.err.println(input);
            e.printStackTrace();
        }

        return list;
    }
   
    public static String getHTTPURLResponse(String urlStr, String method, Map<String, String> httpHeader, String body) throws Exception {
        
        return getHTTPSURLResponse(urlStr, method, httpHeader, body, false);
    }
    
    public static String getHTTPSURLResponse(String urlStr, String method, Map<String, String> httpHeader, String body, boolean selfSigned) throws Exception {
        
        HttpURLConnection conn = null;
        OutputStream output = null;
        BufferedReader bufferedReader = null;
        StringBuilder response = new StringBuilder();
        
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        CookieStore cookieStore = cookieManager.getCookieStore();
        CookieHandler.setDefault(cookieManager);
        
        try {       
            URL url = new URL(urlStr);
//            Log.log.info("url = " + urlStr);
            
            String protocol = url.getProtocol();
            if(protocol != null && protocol.length() != 0 && protocol.equalsIgnoreCase("https")) {
                if(selfSigned) {
                    HttpsURLConnection.setDefaultHostnameVerifier(new javax.net.ssl.HostnameVerifier() {
                        public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
                            return true;
                        }
                    });
                }
                
                conn = (HttpsURLConnection)url.openConnection();
//                conn.setRequestProperty("User-Agent", "Mozilla/5.0");
                conn.setDoInput(true);
            } // https
            
            else
                conn = (HttpURLConnection) url.openConnection();
            
            conn.setRequestMethod(method);
            
            if(httpHeader != null) {
                Set<String> keys = httpHeader.keySet();
                for(Iterator it = keys.iterator(); it.hasNext();) {
                    String key = (String)it.next();
                    String value = httpHeader.get(key);
                    
                    conn.addRequestProperty(key,  value);
                }
            }
            
            List<HttpCookie> cookies = cookieStore.getCookies();
            
            if (cookies != null) {
                StringBuilder sb = new StringBuilder();
                
                for (HttpCookie cookie : cookies) {
                    String cookieName = cookie.getName();
                    String cookieValue = cookie.getValue();
                    
                    sb.append(cookieName).append("=").append(cookieValue).append(";");
                }
                
//                Log.log.info("cookies = " + sb.toString());
                conn.addRequestProperty("Cookie", sb.toString());
            }
            
            if(body != null) {
                byte[] outputInBytes = body.getBytes("UTF-8");
                conn.setDoOutput(true);
                output = conn.getOutputStream();
                output.write(outputInBytes);
            }
System.out.println("Before reading in ...");
            bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
System.out.println("After reading in ...");
            String line = null;
            while ((line = bufferedReader.readLine()) != null)
                response.append(line);

            conn.getContent();
        } catch(Exception e) {
//            e.printStackTrace();
            response.append("error: " + e.getMessage());
            throw e;
        } finally {
            try {
                if(output != null)
                    output.close();
                if(bufferedReader != null)
                    bufferedReader.close();
                if(conn != null)
                    conn.disconnect();
            } catch(Exception ee) {}
        }
        
        return response.toString();
    } // getResponse()
      
    public static void main(String[] args) {
              
        if(args.length > 0 && args[0] != null) {
            String encrypted = encryptKey(args[0]);
            
            if(encrypted.length() != 0)
                decryptKey(encrypted);
        }
        
        else
            System.out.println("Please enter the original key.");
    }
    
    public static String encryptKey(String input) {
        
        String output = "";
        
        if(input == null || input.length() == 0)
            return "";
        
        try {
/*          String key = "resolve qradar integration";
            Crypt crypt = new Crypt(Crypt.ENCRYPTION_SCHEME_DESEDE, CRYPTKEY);
            String encryptedKey = crypt.encrypt(key);
            System.out.println("encryptedKey = " + encryptedKey); */

            String encData = CryptUtils.decrypt(Crypt.getEncDataLocal(), CryptUtils.prefixLocal);
            CryptUtils.setENCData(encData);
            
            output = CryptUtils.encrypt(input);
        } catch(Exception e) {
            e.printStackTrace();
        }
System.out.println("encrypted = " + output);
        return output;
    }
    
    public static String decryptKey(String input) {
        
        String output = "";
        
        if(input == null || input.length() == 0)
            return "";
        
        try {
            String encData = CryptUtils.decrypt(Crypt.getEncDataLocal(), CryptUtils.prefixLocal);
            CryptUtils.setENCData(encData);
            
            output = CryptUtils.decrypt(input);
        } catch(Exception e) {
            e.printStackTrace();
        }
System.out.println("decrypted = " + output);
        return output;
    }
  
    private static void test() {
        
        String sec = "7c62cba8-bda7-4e4e-99f1-42e052fc32ca";
        
        List<Map<String, String>> offenseList = new ArrayList<Map<String, String>>();
        
        try {
            QRadarAPI api = new QRadarAPI("10.50.1.66", "", "/api/siem/", sec, true);

            // 1. Get offense with id
            String response = api.getOffense("1", null);
            System.out.println("1. Get offense with id ... ");
            System.out.println(response);

            Map<String, Object> attr = QRadarAPI.getJsonMap(response);
            StringBuilder sb = new StringBuilder();
            for(Iterator<String> it=attr.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                Object value = attr.get(key);
                sb.append("&").append(key).append("=").append(value.toString());
            }
            System.out.println(sb.toString());

            // 2. Get offenses
            response = api.getOffenses(0, 2, null, null, null, null);
            System.out.println("2. Get offenses ...");
            System.out.println(response);
            getJsonArrayMap(response);

            // 3. Post offense status
            response = api.postOffense("1", "OPEN", null, null, null, null, null);
            System.out.println("3. Post offense ... ");
            System.out.println(response);

            // 4. Get offense note with id
            response = api.getOffenseNote("1", "101", null);
            System.out.println("4. Get offense note with id ... ");
            System.out.println(response);

            // 5. Get offense notes
            response = api.getOffenseNotes("1", null, null,  0, 2);
            System.out.println("5. Get offense notes ... ");
            System.out.println(response);
            // 6. Post offense note
            response = api.postOffenseNote("1", "test note", null);
            System.out.println("6. Post offense note ... ");
            System.out.println(response);
        } catch(Exception e) {
            e.printStackTrace();
        }
        
    } // main()

} // QRadarAPI
