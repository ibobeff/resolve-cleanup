/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.qradar;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/resolve")
public class OffenseServlet extends HttpServlet
{
    private static final long serialVersionUID = -1L;
    
    private static final String FILENAME = "resolve.properties";

    private static String host = "";
    private static String port = "";
    private static String ssl = "";
    
    private static String filterHost = "";
    private static String filterPort = "";
    private static String filterUri = "";
    private static String filterSsl = "";
    
    private static String role = "";
    private static String sec = "";
    
    @Override
    public void init(ServletConfig config) throws ServletException {
       
        ServletContext context = config.getServletContext();
        
        if((String)context.getAttribute("host") == null)
            OffenseServlet.loadResolveProperties(config);

        this.host = (String)context.getAttribute("resolve_host");
        this.port = (String)context.getAttribute("resolve_port");
        this.ssl = (String)context.getAttribute("resolve_ssl");
        
        this.filterHost = (String)context.getAttribute("resolve_filter_host");
        this.filterPort = (String)context.getAttribute("resolve_filter_port");
        this.filterUri = (String)context.getAttribute("resolve_filter_uri");
        this.filterSsl = (String)context.getAttribute("resolve_filter_ssl");

        this.role = (String)context.getAttribute("ROLE");
        this.sec = (String)context.getAttribute("SEC");
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String token = (String)request.getParameter("token");
System.out.println("token = " + token);
        if(token == null || token.length() == 0) {
            response.getWriter().write("Error: no token is included.");
            response.flushBuffer();
            return;
        }
        
        if(host == null || host.length() == 0 || port == null || port.length() == 0 ||
           filterPort == null || filterPort.length() == 0 || filterUri == null || filterUri.length() == 0) {
            response.getWriter().write("Error: configuration is missing.");
            response.flushBuffer();
            return;
        }
        
        Map<String, String[]> requestParameters = request.getParameterMap();
        Map<String, String> params = parseParams(requestParameters);

        StringBuilder sb = new StringBuilder();
        String protocol = (ssl == null || ssl.length() == 0 || ssl.equals("false"))?"http://":"https://";
        sb.append(protocol).append(host).append(":").append(port).append("/resolve/rest/invokeHTTPFilter?token=").append(token);
        sb.append("&filter_host=").append(filterHost).append("&filter_port=").append(filterPort).append("&filter_uri=").append(filterUri);
        if(filterSsl != null && filterSsl.length() != 0)
            sb.append("&filter_ssl=").append(filterSsl);
        
        for(Iterator<String> it=params.keySet().iterator(); it.hasNext();) {
            String key = it.next();
            if(key != null && (key.equals("token") || key.equals("filter_host") || key.equals("filter_port") || key.equals("filter_uri") || key.equals("filter_ssl")))
                continue;
            String value = params.get(key);
            sb.append("&").append(key).append("=").append(URLEncoder.encode(value, "UTF-8"));
        }
        
        // Invoke Resolve HTTPFilter and return worksheet id
        String filterUrl = sb.toString();
System.out.println(filterUrl);

        String result = "";
        
        try {
            String resp = QRadarAPI.getHTTPURLResponse(filterUrl, "GET", null, null);
System.out.println(resp);
            result = resp;
            if(resp != null && resp.length() != 0) {
                int index = resp.indexOf("worksheetId\":");
                if(index != -1) {
                    result = resp.substring(index+13);
                    result = result.substring(1, 33);
                    if(result != null && result.length() != 0)
                        result = "Worksheet id is: " + result;
                }
System.out.println("worksheet id = " + result);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        
        response.getWriter().write(result);
    } // doPost()
    
    public static Properties readFileToProperties(String path, String fileName)
    {
        Properties properties = null;
        
        if(path == null) {
            System.err.println("File path not found.");
            return properties;
        }
            

        try
        {
            String filePath = path + "/WEB-INF/";
            InputStream is = new FileInputStream(filePath + fileName);
            properties = new java.util.Properties();
            properties.load(is);
            is.close();
        }
        catch (Exception e)
        {
            System.err.println("Failed to load properties.");
            e.printStackTrace();
        }

        return properties;
    }
    
    private Map<String, String> parseParams(Map<String, String[]> input) {
        
        Map<String, String> params = new HashMap<String, String>();
        
        if(input != null && input.size() != 0) {
            for(Iterator<String> it=input.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                String[] value = input.get(key);
                if(value != null && value.length == 1) {
                    params.put(key, value[0]);
                }
            }
        }
        
        return params;
    }
    
    public static void loadResolveProperties(ServletConfig config) throws ServletException {
        
        ServletContext context = config.getServletContext();
        String path = context.getRealPath("/");
        Properties prop = readFileToProperties(path, FILENAME);
        
        if(prop == null)
            throw new ServletException("Cannot load properties file.");
        
        String host = prop.getProperty("resolve.host");
        String port = prop.getProperty("resolve.port");
        String ssl = prop.getProperty("resolve.ssl");
        String keys = prop.getProperty("resolve.keys");

        String filterHost = prop.getProperty("resolve.filter.host");
        String filterPort = prop.getProperty("resolve.filter.port");
        String filterUri = prop.getProperty("resolve.filter.uri");
        String filterSsl = prop.getProperty("resolve.filter.ssl");

        String role = prop.getProperty("qradar.user.role");
        String sec = prop.getProperty("qradar.sec");
        
        context.setAttribute("resolve_host", host);
        context.setAttribute("resolve_port", port);
        context.setAttribute("resolve_ssl", ssl);
        context.setAttribute("resolve_keys", keys);
        
        context.setAttribute("resolve_filter_host", filterHost);
        context.setAttribute("resolve_filter_port", filterPort);
        context.setAttribute("resolve_filter_uri", filterUri);
        context.setAttribute("resolve_filter_ssl", filterSsl);
        
        context.setAttribute("ROLE", role);
        context.setAttribute("SEC", sec);
    }
    
} // OffenseServlet