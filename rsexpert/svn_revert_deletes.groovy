/*
 * This script will check all the missing files to see if they needed to be reverted. A missing file is reverted if the action="DELETE"
 */
def out = "svn status".execute().text

out.eachLine { line ->
    if (line.startsWith("!"))
    {
        def file = line.substring(1,line.length()).trim()

        // look for missing files
        println "Checking missing file: ${file}"
        def cat =  "svn cat ${file}".execute().text

        // check if missing files have action="DELETE"
        if (cat.contains('action="DELETE"'))
        {
            println "Reverting file: ${file}"
            def revert =  "svn revert ${file}".execute().text
            println "${revert}"
        }
    }
}

