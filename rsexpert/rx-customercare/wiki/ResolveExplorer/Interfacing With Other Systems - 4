<?xml version="1.0" encoding="UTF-8"?>

<xwikidoc>
<web>ResolveExplorer</web>
<sys_id>8a94829c4108c1c4014118a0281d0257</sys_id>
<name>Interfacing With Other Systems - 4</name>
<summary>1 Persisting Connections with Sessions</summary>
<title>ResolveExplorer.Interfacing With Other Systems - 4</title>
<language></language>
<defaultLanguage></defaultLanguage>
<translation></translation>
<parent></parent>
<creator></creator>
<author>admin</author>
<contentAuthor></contentAuthor>
<creationDate>1379122851000</creationDate>
<contentUpdateDate>1384247738000</contentUpdateDate>
<catalogs></catalogs>
<template></template>
<model></model>
<abort></abort>
<final></final>
<content>{section:type=SOURCE|title=main|id=rssection_main|height=300}
1 Persisting Connections with Sessions

During runbook execution, when a task finishes, Resolve will serialize any appropriate data (such as FLOWS or OUTPUTS) and pass it to the next task. Some objects, such as connection objects, cannot be serialized.
&lt;br/&gt;&lt;br/&gt;
In our example, we need to open a connection to a Cisco router and have that connection available later. Resolve provides an object called SESSIONS to allow for such objects to be persisted between tasks.
&lt;br/&gt;&lt;br/&gt;
Imagine we have a connection object defined as 'conn'. We open the connection, and at the end of the content script put it into SESSIONS like this:
{code}
def conn;
//create connection

SESSIONS.put(conn);
{code}
&lt;br/&gt;
At the beginning of the next task, the one that will run a command, we retrieve the connection using:
{code}
def conn = SESSIONS.get();

//execute commands
{code}
&lt;br/&gt;
Finally, in our close connection task we do:
{code}
def conn = SESSIONS.get();

//send any necessary logout commands

conn.close();
SESSIONS.remove();
{code}
&lt;br/&gt;
If the connection has been established, it is important to always direct the logic to the disconnect task to ensure that the session can be cleaned up before continuing.

{section}</content>
<defaultRole>true</defaultRole>
<readRole>admin,resolve_dev,resolve_process,resolve_user</readRole>
<writeRole>admin,resolve_dev,resolve_process,resolve_user</writeRole>
<adminRole>admin,resolve_dev,resolve_process,resolve_user</adminRole>
<executeRole>admin,resolve_dev,resolve_process,resolve_user</executeRole>
<decisionTree></decisionTree>
<weight>0.0</weight>
</xwikidoc>
