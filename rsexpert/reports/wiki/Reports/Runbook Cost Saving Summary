<?xml version="1.0" encoding="UTF-8"?>

<xwikidoc>
<web>Reports</web>
<sys_id>8a94811b4d596fa0014d5a76122b0b11</sys_id>
<name>Runbook Cost Saving Summary</name>
<summary>Reports.Cost Saving Summary</summary>
<title></title>
<language></language>
<defaultLanguage></defaultLanguage>
<translation></translation>
<parent></parent>
<creator></creator>
<author>admin</author>
<contentAuthor></contentAuthor>
<creationDate>1431766996000</creationDate>
<contentUpdateDate>1502321064000</contentUpdateDate>
<tags></tags>
<template></template>
<model><![CDATA[]]></model>
<abort><![CDATA[]]></abort>
<final><![CDATA[]]></final>
<content><![CDATA[{section:type=source|title=Untitled Section}
<style>
    #Wrapper{
        height : 90%;
        padding: 2px 0px 2px 16px;
    }
    .chartWrapper{
        height : 100%;
        width: 100%;
    }
    #chartId {
        width: 55%;
        height : 100%;
        display: inline-block;
        vertical-align: middle;
    }
    #lengendId {
        width: 40%;
        height: 100% ;
        position : relative;
        display: inline-block;
        vertical-align: middle;
    }
    
</style>
<div id="Wrapper">
    <div id='zoomBar'></div>
    <div class= 'chartWrapper'>
        <div id='chartId'  class='chart'></div>
        <div id="lengendId" ></div>
    </div>
</div>
{pre}
<script type="text/javascript">

var DEFAULT_METRIC = 'Month'; //Minute, Hour, Day

//DEFAULT FOR EACH UNIT
var DEFAULTS = {
    QUARTER_RANGE : [2, 'day'],
    MONTH_RANGE : [1, 'month'],
    WEEK_RANGE : [1.5, 'year'],
    DAY_RANGE : [1.5, 'year'],
    HR_RANGE : [1.5, 'year']
}

//INITIALIZATION
Ext.onReady(function() {

    var mode = getParameterByName('mode');

    if(mode == 'simple'){
        //Add cls simple contain wrapper.
        var wrapper = document.getElementById('Wrapper');
        wrapper.className += ' simple';

        simpleModeReport('chartId', 'lengendId', 'Runbook Cost Saving Summary', DEFAULTS, DEFAULT_METRIC);
    }
    else{
        detailModeReport('chartId', 'lengendId', 'Runbook Cost Saving Summary', DEFAULTS, DEFAULT_METRIC, 'selectionCom', 'zoomBar');
    }
});
</script>
<script src="/resolve/js/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="/resolve/js/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="/resolve/js/timezonedetect.js" type="text/javascript"></script>
<script type="text/javascript">

//ULTILITY FUNCTIONS
//Get the value of URL's parameter
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

//Convert time quantity to miliseconds
function convertToMillis(quantity, period)
{
    var offsetValues = {};
    offsetValues.second = 1000;
    offsetValues.minute = 60000;
    offsetValues.hour = 3600000;
    offsetValues.day = 86400000;
    offsetValues.week = 604800000;
    offsetValues.month = 2592000000;
    offsetValues.year = 31536000000;
    
    return (quantity * offsetValues[period]);
}

//Generate simple mode report with passed in paramenters
//For parameters explanation, check below.
function simpleModeReport(chartId, legendId, chartTitle, defaults, defaultMetric){
    //Set up chart.
     var chart = createChart(chartId, legendId, chartTitle, true);

    //Create datastore for this chart
    var dataStore = createDataStore(chart, defaults);


    //User defined data (Runbook Properties)
    var userDefinedDataStore = createUserDefinedDataStore(dataStore);
    userDefinedDataStore.load();
}

//Generate Detail mode report
// chartId (String) : id of chart.
// valueAxesTitle (String) : title of value of axis.
// chartTitle (String) : title of chart

// dataStoreModel (String) : model use in datastore (must match model defined in interface.)
// dataFieldName (String) : field name will be use in aggregate function.
// aggregateType (String) : aggregate function type. Must match : sum , avg , MB to GB (for Database Size)
// defaults (Object) : contain table use for dataloading as well as data range for each metric. Must match following format:
//   {  MIN_TABLE : table_name ,
//      MIN_RANGE : [quantity (Number), period(String)] e.g [1 , 'month'] ,
//      HR_TABLE : table_name,
//      HR_RANGE : [quantity (Number), period(String)],
//      DAY_TABLE : table_name,
//      DAY_RANGE : [quantity (Number), period(String)] }
// defaultMetric (String) : default unit use for initial load (Minute, Hour, Day).

// selectionComponentId (String) : id of selection component.
// toolbarId (String) : id of metric toolbar.
function detailModeReport(chartId, legendId, chartTitle, defaults, defaultMetric, selectionComponentId, toolbarId){
    //Set up chart.
     var chart = createChart(chartId, legendId, chartTitle, false);

    //Create datastore for this chart
    var dataStore = createDataStore(chart, defaults);

    //User defined data (Runbook Properties)
    var userDefinedDataStore = createUserDefinedDataStore(dataStore);
    userDefinedDataStore.load();

    //Create Zoombar
    var zoomBar = createZoomToolbar(chart, toolbarId);
}

//AMCHART RELATED FUNCTIONS
//Create chart with default config
function createChart(chartId, legendId , chartTitle, hasTitle){
    var chart = AmCharts.makeChart(chartId, {
        type: 'pie',
        pathToImages: '/resolve/js/amcharts/amcharts/images/',
        dataProvider: [],
        graph : [],
        colors : [ "#FF6600", "#FF9E01", "#FCD202", "#F8FF01", "#B0DE09", "#04D215", "#0D8ECF", "#0D52D1", "#2A0CD0", "#8A0CCF", "#CD0D74", "#754DEB", "#DDDDDD", "#999999", "#333333", "#000000", "#57032A", "#CA9726", "#990000", "#4B0C25", "#FF0F00"],
        valueField : "value",
        titleField : "name",
        labelsEnabled: false,
        autoMargins: false,
        marginTop: 50,
        marginBottom: 50,
        marginLeft: 0,
        marginRight: 0,
        pullOutRadius: 0,
        innerRadius : '50%',
        balloonText :   "<b>Source:</b> [[title]]<br>" +
                        "<b>Saved:</b> <b>$</b> [[value]] ([[percents]]%)<br>" +
                        "<b>Executed:</b> [[count]] <b>times</b>" +
                        "<b></b><br><b>From:</b> [[from]] <br>" +
                        "<b>To:</b> [[to]]",
        balloon : {
            textAlign: "left"
        }
    });

    var legend = new AmCharts.AmLegend();
    legend.position = 'right';
    legend.autoMargins = false;
    legend.maxColumns = 1;
    legend.valueText = "";
    legend.marginTop = 20;
    chart.addLegend(legend, legendId);

    //this will be use to hold dataProvider for each zoom
    chart.dataSource = {};

    if(hasTitle == true ){
        chart.addTitle(chartTitle);
        chart.validateNow();
    }
    return chart;
}

//Update chart with new data
function updateChart(chart, zoom){
    if(chart){
        chart.clearLabels();
        var dataProvider = chart.dataSource[zoom];
        if(dataProvider && dataProvider.length > 0)
            chart.dataProvider = dataProvider;
        else{
            chart.dataProvider = [];
            chart.addLabel(0, '50%', 'The chart contains no data', 'center', 15);
        }
        chart.validateData();
    }
}

//DATASTORE FUNCTIONS
// chart (AmChart Object) : chart use with this datastore.

function createDataStore(chart, defaults){
    var dataUnit = "hour"; // the loweest interval .
    var zoomRange = [
        [12, 'hour'],[1, 'day'],[1, 'week'],[2, 'week'],[1, 'month'],[3, 'month'],[6, 'month'],[1, 'year']
    ];
    var dataRange = [1,  'year'];
    var runbookData;

    function setRunbookData(records){  

        runbookData = {};
        //Compute graphs for each runbook name.
        Ext.Array.each(records, function(record){
          
            //Get manual runtime for each runbook
            var runbookId = record.data.runbook_name
            runbookData[runbookId ] = {};
            runbookData[runbookId ].display_name = record.data.display_name || record.data.runbook_name;
            runbookData[runbookId ].manual_runtime = record.data.manual_runtime;
            runbookData[runbookId ].hourly_cost = record.data.hourly_cost;
        });
    }   

    var dataStore = Ext.create('Ext.data.Store', {
        fields:['thisIsNotUsed'],
        proxy:{
            type:'ajax',
            url:'/resolve/service/report/runbookAnalysis',
            reader:{
                root:'data.data'
            }
        },
        listeners: {
            beforeload : function(store, operation, eOpts){
                Ext.getBody().mask("Updating Chart");
                operation.params = operation.params || {}
                var filter = [];
                var end = new Date();
                var start =  new Date(end.getTime() - convertToMillis(dataRange[0], dataRange[1]));

                //Get current timezone string ID
                var timezone = jstz.determine().name();

                //Get runbook name for query
                var wikiName = [];
                for(var k in runbookData){
                    wikiName.push(k);
                }
                if(wikiName.length == 0)
                      wikiName = [ "Not a valid name" ]; //prevent ES to query all runbook when no runbook defined in properties runbook.
                filter.push({
                    field : "wiki.raw",
                    type : 'terms',
                    value: wikiName.join(),
                    caseSensitive : true
                });
                filter.push({
                    field : "taskName",
                    type : "term",
                    condition : "equals",
                    value: "end"
                });
                filter.push({
                    field : "timestamp",
                    type: "date",
                    condition : "between",
                    startValue : start,
                    endValue : end
                });
                Ext.apply(operation.params,{
                    interval: dataUnit,
                    lowerBound : start.getTime(),
                    upperBound : end.getTime(),
                    aggregationType : 'duration',
                    timezone : timezone,
                    filter:Ext.encode(filter)
                })
            },
            load : function(store, records){
                var analyzedData = [];
                if(records && records.length > 0){
                    Ext.Array.each(records, function(record){
                        var item = record.raw;
                        var dataItem = { timestamp: item.timestamp };
                        if(item.runbook_name){
                            //calculate total manual runtime for this runbook
                            var runbookProperty = runbookData[item.runbook_name];
                            if(runbookProperty){
                                var tot_manual_runtime = item.count * runbookProperty.manual_runtime;
                                var tot_runtime_saved = (tot_manual_runtime - item.duration) / 3600;
                                if(tot_runtime_saved < 0)
                                    tot_runtime_saved = 0; //Ignore negative value.
                                var tot_cost_saved = (runbookProperty.hourly_cost * tot_runtime_saved);
                                
                                //Use display name for runbook instead of runbook name
                                var displayName = runbookProperty.display_name;
                                dataItem['name'] = displayName;
                                dataItem['value_field'] = tot_cost_saved;
                                dataItem['count'] = item.count;
                            }
                        }
                        analyzedData.push(dataItem);
                    });
                    analyzedData = Ext.Array.sort(analyzedData, function(a, b) {
                        return (a.timestamp - b.timestamp);
                    });
                }
                generateDataForChart(analyzedData);
                updateChart(chart, '1_month'); //default zoom
                Ext.getBody().unmask();
            }   
        }
    });

    function generateDataForChart(data){
        var dataSource = {};
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        //get data for time range
        function getDataForTimeRange(startDate, endDate, inputData){
            var result = [];
            for(var i = 0; i < inputData.length; i++){
                var timestamp = inputData[i].timestamp;
                if(timestamp > startDate && timestamp < endDate)
                    result.push(inputData[i]);
            }
            return result;
        }

       
        //get top 9 runbook
        function pickTop10Runbook(saveObject, executionObject){
            var data = [];
            var executionList = {};
            var savedList = {};
            for(var key in saveObject){
                data.push({
                    name : key,
                    value : saveObject[key]
                });
            }
            data = Ext.Array.sort(data, function(a, b){
                return (b.value - a.value);
            });
            
            //get data for top 9
            for(var i = 0; i < data.length && i < 9; i++){
                var dataItem = data[i];
                savedList[dataItem.name] = dataItem.value;
                executionList[dataItem.name] = executionObject[dataItem.name];
            }

            //Put rest of the data into others.
            var otherSavedTotal = 0;
            var otherExecutionTotal = 0;
            for(var i = 9; i < data.length; i++){
                var dataItem = data[i];
                otherSavedTotal += dataItem.value;
                otherExecutionTotal += executionObject[dataItem.name];
            }
            if(otherSavedTotal > 0){
                savedList['Others'] = otherSavedTotal;
                executionList['Others'] = otherExecutionTotal;
            }
            
            function getExecutionData(){
                return executionList;
            }
            function getSavedData(){
                return savedList;
            }
            return {
                getExecutionData : getExecutionData(),
                getSavedData : getSavedData()
            };
        }
        //Compute data for each zoom range.
        for(var i = 0; i < zoomRange.length; i++){
            var currentZoom = zoomRange[i];
            var endDate = new Date();
            var endDateTs = endDate.getTime();
            var startDateTs = endDateTs - convertToMillis(currentZoom[0], currentZoom[1]);
            var startDate = new Date(startDateTs);


            var dataForZoom = getDataForTimeRange(startDate, endDate, data);
       

            //Get total time saved for each runbook in this zoom
            var totalSavedForEachRB = {};
            var totalExecutionForEachRB = {};
             for(var t = 0; t < dataForZoom.length; t++){
            
                var record = dataForZoom[t];
                var recordName = record['name'];
                if(recordName){
                    //Aggregate total time/cost 
                    if(!totalSavedForEachRB[recordName])
                        totalSavedForEachRB[recordName] = 0;
                    totalSavedForEachRB[recordName] += record['value_field'];

                    //Aggregate total execution 
                    if(!totalExecutionForEachRB[recordName])
                        totalExecutionForEachRB[recordName] = 0;
                    totalExecutionForEachRB[recordName] += record['count'];
                }
            }
           //Filter out data to pick only top 9 runbook and put other runbook in to 'others'
            var top10Data = pickTop10Runbook(totalSavedForEachRB, totalExecutionForEachRB);
            totalExecutionForEachRB = top10Data['getExecutionData'];
            totalSavedForEachRB = top10Data['getSavedData'];

            var dataForEachZoom = [];
            for(var key in totalSavedForEachRB){
                var record = {};
                record.name = key;
                record.value = parseFloat((totalSavedForEachRB[key]).toFixed(2)); // Only keep 2 decimal number.
                record.count = totalExecutionForEachRB[key];
                record.from = monthNames[startDate.getMonth()] + " " + startDate.getDate() + " " + startDate.getFullYear();
                record.to = monthNames[endDate.getMonth()] + " " + endDate.getDate() + " " + endDate.getFullYear();
                dataForEachZoom.push(record);
            }
            var dataAttr = currentZoom[0] + "_" + currentZoom[1];
            dataSource[dataAttr] = dataForEachZoom;
        }

        chart.dataSource = dataSource;
    }
    var store = {
        store : dataStore,
        setRunbookData : setRunbookData
    };

    return store;
}

function createUserDefinedDataStore(dataStore){
    var store = Ext.create('Ext.data.JsonStore', {
        fields : [
            {
                name : 'runbook_name',
                mapping : 'u_name'
            },
            {
                name : 'display_name',
                mapping : 'u_display_name'
            },
            {
                name : 'manual_runtime',
                mapping : 'u_manual_duration',
                type : 'int',
                convert : function(value, model){
                    return value * 60;
                }
            },
            {
                name : 'hourly_cost',
                mapping : 'u_hourly_cost'
            }
        ],
        proxy: {
            type: 'ajax',
            method: 'POST',
            url: '/resolve/service/report/businessmetadata',
            extraParams: {
               reportType : 'runbook'
            },
            reader: {
                type: 'json',
                root: 'records'
            }
        },
        sorters: [{
            property: 'u_name',
            direction: 'ASC'
        }],
        autoLoad: false,
        remoteSort: true,
        remoteFilter: false,
        listeners: {
            load : function(store,records){
                
                //Load the data store
                dataStore.setRunbookData(records);
                dataStore.store.load();
            }   
        }
    });
    return store;
};
/*
ZOOMTOOLBAR
chart : AMchart object
datastore : datastore object
selectionComponent : 
id : string id where the toolbar will be rendered
defaultMetric : string default load metric.
*/
function createZoomToolbar(chart, id){
   
    var zoomToolbar = Ext.create("Ext.toolbar.Toolbar",{ 
        cls : 'zoom-tb' 
    });
    
    var zoomConfig = [
        [12, 'hour', '12 hours'],
        [1 , 'day', '1 day'],
        [1, 'week', '1 week'],
        [2, 'week', '2 weeks'],
        [1, 'month', '1 month'],
        [3, 'month', '3 months'],
        [6, 'month', '6 months'],
        [1, 'year', '1 year']
    ];

    Ext.Array.each(zoomConfig, function(config){
        var button = Ext.create('Ext.button.Button',{
            text : config[2],
            quantity : config[0],
            period : config[1],
            handler : function(){
                //Toggle this button and de-toggle all other button.
                var me = this;
                var parent = this.up('toolbar');
                 Ext.Array.each(parent.items.items, function(button){
                    button.toggle(button == me);
                });
                
                var dataAttr = this.quantity + '_' + this.period;
                updateChart(chart, dataAttr);
            }
        });
        button.toggle(config[2] == '1 month');

        zoomToolbar.add(button);
    }); 

    var toolbar = Ext.create("Ext.panel.Panel",{
        layout : {
            type : 'hbox',
            align : 'middle'
        },
        items : [ { html : '<b >Zoom:</b>' } , zoomToolbar ],
        renderTo : id
    });

    return toolbar;
}
</script>
{pre}
{section}]]></content>
<defaultRole>true</defaultRole>
<readRole>admin,resolve_dev,resolve_process,resolve_user</readRole>
<writeRole>admin,resolve_dev,resolve_process,resolve_user</writeRole>
<adminRole>admin,resolve_dev,resolve_process,resolve_user</adminRole>
<executeRole>admin,resolve_dev,resolve_process,resolve_user</executeRole>
<decisionTree><![CDATA[]]></decisionTree>
<weight>0.0</weight>
<displayMode>wiki</displayMode>
<catalogId></catalogId>
<wikiParameters>[]</wikiParameters>
<isRequestSubmission>false</isRequestSubmission>
<reqestSubmissionOn></reqestSubmissionOn>
<lastReviewedOn></lastReviewedOn>
<lastReviewedBy></lastReviewedBy>
<expireOn></expireOn>
<dtAbortTime>0</dtAbortTime>
<updatedBy>admin</updatedBy>
<updatedOn>1502321064000</updatedOn>
<resolutionBuilderId>ff8080814d90bbfd014d914623430011</resolutionBuilderId>
<isDeleted>false</isDeleted>
<decisionTreeOptions><![CDATA[{"dtLayout":{"questionFontColor":"000000","questionFont":"Verdana","questionFontSize":"13","answerFontColor":"000000","answerFont":"Verdana","answerFontSize":"10","recommendationFontColor":"41a510","recommendationFont":"Verdana","recommendationFontSize":"10","confirmationFontColor":"FFFFFF","confirmationBGColor":"41a510","confirmationFont":"Verdana","confirmationFontSize":"10","navigationFontColor":"FFFFFF","navigationBGColor":"41a510","navigationFont":"Verdana","navigationFontSize":"13","autoConfirm":false,"buttonText":"Next","questionOnTop":true,"navOnLeft":true,"verticalAnswerLayout":true},"dtVariables":[]}]]></decisionTreeOptions>
<isTemplate>false</isTemplate>
</xwikidoc>
