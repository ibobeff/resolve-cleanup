/*
 * This script will check all the modified files. If only the dates and owner is changed, it will revert the file
 */
def out = "svn status".execute().text

out.eachLine { line ->
    if (line.startsWith("M"))
    {
        def file = line.substring(1,line.length()).trim()

        // look for missing files
        println "Checking modified file: ${file}"
        def cat =  "svn diff \"${file}\"".execute().text

        boolean modified = false;
        cat.eachLine { modLine ->
            //println "Checking modified: "+modified+" modLine: "+modLine
            if (!modified)
            {
                if (modLine.startsWith("+") && !modLine.startsWith("+++"))
                {
                    // check XML files
                    if (modLine.startsWith("+    <sys_updated"))
                    {
                        println "Skipping line: "+modLine
                    }

                    // package.xml file
                    else if (modLine.startsWith("+<description"))
                    {
                        println "Skipping line: "+modLine
                    }

                    // wiki file
                    else if (modLine.startsWith("+<contentUpdateDate"))
                    {
                        println "Skipping line: "+modLine
                    }

                    // wiki file
                    else if (modLine.startsWith("+<creationDate"))
                    {
                        println "Skipping line: "+modLine
                    }

                    // modified
                    else
                    {
                        println "Modified line: "+modLine
                        println ""
                        modified = true;
                    }
                }
            }
        }

        if (!modified)
        {
            println "Reverting file: ${file}"
            def revert =  "svn revert \"${file}\"".execute().text
            println "${revert}"
        }
    }
}

