/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.persistence.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolveNodePropertiesVO;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "resolve_node_properties", indexes = {@Index(columnList = "u_node_sys_id", name = "rnp_u_node_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveNodeProperties extends BaseModel<ResolveNodePropertiesVO>
{
    private static final long serialVersionUID = -6196076587934539200L;
    
    private String UKey;
    private String UValue;
    
    private ResolveNode node;

    public ResolveNodeProperties()
    {
    }
    
    public ResolveNodeProperties(String sysId)
    {
        this.setSys_id(sysId);
    }

    public ResolveNodeProperties(ResolveNodePropertiesVO vo)
    {
        applyVOToModel(vo);
    }


    @Column(name = "u_key", length = 300)
    public String getUKey()
    {
        return UKey;
    }

    public void setUKey(String key)
    {
        this.UKey = key;
    }

    @Column(name = "u_value", length = 4000)
    public String getUValue()
    {
        return UValue;
    }

    public void setUValue(String value)
    {
        this.UValue = value;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_node_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveNode getNode()
    {
        return node;
    }

    public void setNode(ResolveNode node)
    {
        this.node = node;
        if (node != null)
        {
            Set<ResolveNodeProperties> coll = node.getProperties();
            if (coll == null)
            {
                coll = new HashSet<ResolveNodeProperties>();
                coll.add(this);
                
                node.setProperties(coll);
            }
        }
    }

    @Override
    public ResolveNodePropertiesVO doGetVO()
    {
        ResolveNodePropertiesVO vo = new ResolveNodePropertiesVO();
        super.doGetBaseVO(vo);
        
        vo.setUKey(getUKey());
        vo.setUValue(getUValue());
        
        vo.setNode(Hibernate.isInitialized(this.node) && getNode() != null ? new ResolveNodeVO(this.getNode().getSys_id()): null);
        
        return vo;
    }

    @Override
    public void applyVOToModel(ResolveNodePropertiesVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            
            //this is ignored in the base class , so doing it here.
            this.setSysCreatedBy(StringUtils.isNotBlank(vo.getSysCreatedBy()) && vo.getSysCreatedBy().equals(VO.STRING_DEFAULT) ? getSysCreatedBy() : vo.getSysCreatedBy());
            this.setSysCreatedOn(vo.getSysCreatedOn()!= null && vo.getSysCreatedOn().equals(VO.DATE_DEFAULT) ? getSysCreatedOn() : vo.getSysCreatedOn());

            
            this.setUKey(StringUtils.isNotBlank(vo.getUKey()) && vo.getUKey().equals(VO.STRING_DEFAULT) ? getUKey() : vo.getUKey());
            this.setUValue(StringUtils.isNotBlank(vo.getUValue()) && vo.getUValue().equals(VO.STRING_DEFAULT) ? getUValue() : vo.getUValue());
            
            //references
            this.setNode(vo.getNode() != null ? new ResolveNode(vo.getNode().getSys_id()): getNode());
            

        }
    }

}
