package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.resolve.services.hibernate.vo.CEFDictionaryItemVO;

@Entity
@Table(name = "cef_dictionary")
public class CEFDictionaryItem extends BaseModel<CEFDictionaryItemVO> {

	private static final long serialVersionUID = 6975215602668781463L;

	private String UShortName;
	private String UFullName;
	private String UDataType;
	private Integer ULength;
	private String UDescription;
	private ArtifactType artifactType;
	
	@Column(name = "u_cef_short_name", unique = true, nullable = false)
	public String getUShortName() {
		return UShortName;
	}

	public void setUShortName(String uShortName) {
		UShortName = uShortName;
	}

	@Column(name = "u_cef_full_name", unique = true, nullable = false)
	public String getUFullName() {
		return UFullName;
	}

	public void setUFullName(String uFullName) {
		UFullName = uFullName;
	}

	@Column(name = "u_cef_data_type", nullable = false)
	public String getUDataType() {
		return UDataType;
	}

	public void setUDataType(String uDataType) {
		UDataType = uDataType;
	}

	@Column(name = "u_cef_length")
	public Integer getULength() {
		return ULength;
	}

	public void setULength(Integer uLength) {
		ULength = uLength;
	}

	@Column(name = "u_cef_description", length = 1000)
	public String getUDescription() {
		return UDescription;
	}

	public void setUDescription(String uDescription) {
		UDescription = uDescription;
	}
	
	@ManyToOne
	@JoinColumn(name = "artifact_type_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
	public ArtifactType getArtifactType() {
		return artifactType;
	}

	public void setArtifactType(ArtifactType artifactType) {
		this.artifactType = artifactType;
	}

	@Override
	public CEFDictionaryItemVO doGetVO() {
		CEFDictionaryItemVO vo = new CEFDictionaryItemVO();
        super.doGetBaseVO(vo);
        
        vo.setUShortName(UShortName);
        vo.setUFullName(UFullName);
        vo.setUDataType(UDataType);
        vo.setULength(ULength);
        vo.setUDescription(UDescription);

        return vo;
	}
}
