/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.resolve.services.hibernate.vo.RBLoopVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
@Entity
@Table(name = "rb_loop")
public class RBLoop extends BaseModel<RBLoopVO>
{
    private String description;
    private Double order;
    private Integer count; //if count is provided it loops that many number
    private String itemSource; //WSDATA, PARAMS etc.
    private String itemSourceName; //name of the WSDATA or PARAM
    private String itemDelimiter; //comma or space
    private String parentId;
    private String parentType; //e.g., "builder"

    public RBLoop()
    {
    }

    public RBLoop(RBLoopVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_description", length = 256)
    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @Column(name = "u_order", precision=10, scale=2)
    public Double getOrder()
    {
        return order;
    }

    public void setOrder(Double order)
    {
        this.order = order;
    }

    @Column(name = "u_count")
    public Integer getCount()
    {
        return count;
    }

    public void setCount(Integer count)
    {
        this.count = count;
    }

    @Column(name = "u_item_src", length = 40)
    public String getItemSource()
    {
        return itemSource;
    }

    public void setItemSource(String itemSource)
    {
        this.itemSource = itemSource;
    }

    @Column(name = "u_item_src_name", length = 100)
    public String getItemSourceName()
    {
        return itemSourceName;
    }

    public void setItemSourceName(String itemSourceName)
    {
        this.itemSourceName = itemSourceName;
    }

    @Column(name = "u_item_delimiter", length = 40)
    public String getItemDelimiter()
    {
        return itemDelimiter;
    }
    public void setItemDelimiter(String itemDelimiter)
    {
        this.itemDelimiter = itemDelimiter;
    }

    @Column(name = "u_parent_id", length = 32)
    public String getParentId()
    {
        return parentId;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }
    
    @Column(name = "u_parent_type", length = 40)
    public String getParentType()
    {
        return parentType;
    }
    public void setParentType(String parentType)
    {
        this.parentType = parentType;
    }

    @Override
    public RBLoopVO doGetVO()
    {
        RBLoopVO vo = new RBLoopVO();
        super.doGetBaseVO(vo);

        vo.setDescription(getDescription());
        vo.setOrder(getOrder());
        vo.setCount(getCount());
        vo.setItemSource(getItemSource());
        vo.setItemSourceName(getItemSourceName());
        vo.setItemDelimiter(getItemDelimiter());
        vo.setParentId(getParentId());
        vo.setParentType(getParentType());
        
        return vo;
    }

    @Override
    public void applyVOToModel(RBLoopVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setDescription(StringUtils.isNotBlank(vo.getDescription()) && vo.getDescription().equals(VO.STRING_DEFAULT) ? getDescription() : vo.getDescription());
            this.setOrder(vo.getOrder() != null && vo.getOrder().equals(VO.DOUBLE_DEFAULT) ? getOrder() : vo.getOrder());
            this.setCount(vo.getCount() != null && vo.getParentType().equals(VO.INTEGER_DEFAULT) ? getCount() : vo.getCount());
            this.setItemSource(StringUtils.isNotBlank(vo.getItemSource()) && vo.getItemSource().equals(VO.STRING_DEFAULT) ? getItemSource() : vo.getItemSource());
            this.setItemSourceName(StringUtils.isNotBlank(vo.getItemSourceName()) && vo.getItemSourceName().equals(VO.STRING_DEFAULT) ? getItemSourceName() : vo.getItemSourceName());
            this.setItemDelimiter(StringUtils.isNotBlank(vo.getItemDelimiter()) && vo.getItemDelimiter().equals(VO.STRING_DEFAULT) ? getItemDelimiter() : vo.getItemDelimiter());
            this.setParentId(StringUtils.isNotBlank(vo.getParentId()) && vo.getParentId().equals(VO.STRING_DEFAULT) ? getParentId() : vo.getParentId());
            this.setParentType(StringUtils.isNotBlank(vo.getParentType()) && vo.getParentType().equals(VO.STRING_DEFAULT) ? getParentType() : vo.getParentType());
        }
    }
}
