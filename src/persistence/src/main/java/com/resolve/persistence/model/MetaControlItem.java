/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied,
 * modified, or distributed without the express written permission of
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;

import com.resolve.services.hibernate.vo.MetaControlItemVO;
import com.resolve.services.hibernate.vo.MetaFormActionVO;
import com.resolve.services.hibernate.vo.MetaxFieldDependencyVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "meta_control_item", indexes = {@Index(columnList = "u_meta_control_sys_id", name = "u_meta_control_sys_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaControlItem extends BaseModel<MetaControlItemVO>
{

    private static final long serialVersionUID = 5576541996022062885L;

    private String UName;
    private String UDisplayName;
    private String UGroupName;
    private String UGroupDisplayName;
    private String UType;
    private String UParam;
    private Integer USequence;// sequence #

    private Boolean UCustomTableDisplay;

    // object references
    private MetaControl metaControl;

    private Collection<MetaFormAction> metaFormActions;
    private Collection<MetaxFieldDependency> metaxFieldDependencys;

    // RBA-13999 : adding extra properties, like font, color, etc. - JSON text field
    private String UPropertiesJSON;

    public MetaControlItem()
    {
    }

    public MetaControlItem(MetaControlItemVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_name", length = 100)
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }

    @Column(name = "u_display_name", length = 100)
    public String getUDisplayName()
    {
        return this.UDisplayName;
    }

    public void setUDisplayName(String UDisplayName)
    {
        this.UDisplayName = UDisplayName;
    }

    @Column(name = "u_group_name", length = 100)
    public String getUGroupName()
    {
        return this.UGroupName;
    }

    public void setUGroupName(String UGroupName)
    {
        this.UGroupName = UGroupName;
    }

    @Column(name = "u_group_display_name", length = 100)
    public String getUGroupDisplayName()
    {
        return this.UGroupDisplayName;
    }

    public void setUGroupDisplayName(String UGroupDisplayName)
    {
        this.UGroupDisplayName = UGroupDisplayName;
    }

    @Column(name = "u_type", length = 100)
    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }

    @Column(name = "u_param", length = 4000)
    public String getUParam()
    {
        return this.UParam;
    }

    public void setUParam(String UParam)
    {
        this.UParam = UParam;
    }

    @Column(name = "u_sequence")
    public Integer getUSequence()
    {
        return USequence;
    }

    /**
     * @param uSequence
     *            the uSequence to set
     */
    public void setUSequence(Integer uSequence)
    {
        USequence = uSequence;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_meta_control_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaControl getMetaControl()
    {
        return metaControl;
    }

    public void setMetaControl(MetaControl metaControl)
    {
        this.metaControl = metaControl;

        if (metaControl != null)
        {
            Collection<MetaControlItem> coll = metaControl.getMetaControlItems();
            if (coll == null)
            {
                coll = new HashSet<MetaControlItem>();
                coll.add(this);

                metaControl.setMetaControlItems(coll);
            }
        }
    }

    /**
     * @return the metaFormActions
     */
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "metaControlItem")
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<MetaFormAction> getMetaFormActions()
    {
        return metaFormActions;
    }

    /**
     * @param metaFormActions
     *            the metaFormActions to set
     */
    public void setMetaFormActions(Collection<MetaFormAction> metaFormActions)
    {
        this.metaFormActions = metaFormActions;
    }

    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "metaControlItem")
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<MetaxFieldDependency> getMetaxFieldDependencys()
    {
        return this.metaxFieldDependencys;
    }

    public void setMetaxFieldDependencys(Collection<MetaxFieldDependency> metaxFieldDependencys)
    {
        this.metaxFieldDependencys = metaxFieldDependencys;
    }

    @Override
    public MetaControlItemVO doGetVO()
    {
        MetaControlItemVO vo = new MetaControlItemVO();
        super.doGetBaseVO(vo);

        vo.setUDisplayName(getUDisplayName());
        vo.setUGroupDisplayName(getUGroupDisplayName());
        vo.setUGroupName(getUGroupName());
        vo.setUName(getUName());
        vo.setUParam(getUParam());
        vo.setUSequence(getUSequence());
        vo.setUType(getUType());
        vo.setCustomTableDisplay(ugetUCustomTableDisplay());
        vo.setUPropertiesJSON(getUPropertiesJSON());

        // refs - don;t do the parent, only the children
        Collection<MetaFormAction> metaFormActions = Hibernate.isInitialized(this.metaFormActions) ? getMetaFormActions() : null;
        if (metaFormActions != null && metaFormActions.size() > 0)
        {
            Collection<MetaFormActionVO> vos = new ArrayList<MetaFormActionVO>();
            for (MetaFormAction model : metaFormActions)
            {
                vos.add(model.doGetVO());
            }
            vo.setMetaFormActions(vos);
        }

        Collection<MetaxFieldDependency> metaxFieldDependencys = Hibernate.isInitialized(this.metaxFieldDependencys) ? getMetaxFieldDependencys() : null;
        if (metaxFieldDependencys != null && metaxFieldDependencys.size() > 0)
        {
            Collection<MetaxFieldDependencyVO> vos = new ArrayList<MetaxFieldDependencyVO>();
            for (MetaxFieldDependency model : metaxFieldDependencys)
            {
                vos.add(model.doGetVO());
            }
            vo.setMetaxFieldDependencys(vos);
        }

        return vo;
    }

    @Override
    public void applyVOToModel(MetaControlItemVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setUDisplayName(StringUtils.isNotBlank(getUDisplayName()) && vo.getUDisplayName().equals(VO.STRING_DEFAULT) ? getUDisplayName() : vo.getUDisplayName());
            this.setUGroupDisplayName(StringUtils.isNotBlank(getUGroupDisplayName()) && vo.getUGroupDisplayName().equals(VO.STRING_DEFAULT) ? getUGroupDisplayName() : vo.getUGroupDisplayName());
            this.setUGroupName(StringUtils.isNotBlank(getUGroupName()) && vo.getUGroupName().equals(VO.STRING_DEFAULT) ? getUGroupName() : vo.getUGroupName());
            this.setUName(StringUtils.isNotBlank(getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUParam(StringUtils.isNotBlank(getUParam()) && vo.getUParam().equals(VO.STRING_DEFAULT) ? getUParam() : vo.getUParam());
            this.setUSequence(vo.getUSequence() != null && vo.getUSequence().equals(VO.INTEGER_DEFAULT) ? getUSequence() : vo.getUSequence());
            this.setUType(StringUtils.isNotBlank(getUType()) && vo.getUType().equals(VO.STRING_DEFAULT) ? getUType() : vo.getUType());
            this.setUCustomTableDisplay(vo.isCustomTableDisplay());
            this.setUPropertiesJSON(vo.getUPropertiesJSON() != null ? getUPropertiesJSON() : vo.getUPropertiesJSON());

            Collection<MetaFormActionVO> metaFormActionsVOs = vo.getMetaFormActions();
            if (metaFormActionsVOs != null && metaFormActionsVOs.size() > 0)
            {
                Collection<MetaFormAction> models = new ArrayList<MetaFormAction>();
                for (MetaFormActionVO refVO : metaFormActionsVOs)
                {
                    models.add(new MetaFormAction(refVO));
                }
                this.setMetaFormActions(models);
            }

            Collection<MetaxFieldDependencyVO> metaxFieldDependencysVOs = vo.getMetaxFieldDependencys();
            if (metaxFieldDependencysVOs != null && metaxFieldDependencysVOs.size() > 0)
            {
                Collection<MetaxFieldDependency> models = new ArrayList<MetaxFieldDependency>();
                for (MetaxFieldDependencyVO refVO : metaxFieldDependencysVOs)
                {
                    models.add(new MetaxFieldDependency(refVO));
                }
                this.setMetaxFieldDependencys(models);
            }

        }

    }

    @Column(name = "u_custom_table_display", length = 1)
    @Type(type = "yes_no")
    public Boolean getUCustomTableDisplay()
    {
        return UCustomTableDisplay;
    }

    public Boolean ugetUCustomTableDisplay()
    {
        if (this.UCustomTableDisplay == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UCustomTableDisplay;
        }
    } // ugetUIsDeleted

    public void setUCustomTableDisplay(Boolean customTableDisplay)
    {
        this.UCustomTableDisplay = customTableDisplay;
    }

    @Column(name = "u_properties_json")
    public String getUPropertiesJSON()
    {
        return UPropertiesJSON;
    }

    public void setUPropertiesJSON(String propertiesJSON)
    {
        this.UPropertiesJSON = propertiesJSON;
    }

}
