package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import com.resolve.services.hibernate.vo.ResolveCompRelVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table (name = "resolve_comp_rel", indexes = {@Index(columnList = "u_parent_id", name = "rcl_parent_id_idx")})
public class ResolveCompRel extends BaseModel<ResolveCompRelVO>
{
	private static final long serialVersionUID = 785468158257073418L;
	
	String parentId;
	String parentType;
	String childId;
	String childType;
	
	@Column(name = "u_parent_id", nullable = false, length = 32)
	public String getParentId()
	{
		return parentId;
	}
	public void setParentId(String parentId)
	{
		this.parentId = parentId;
	}
	
	@Column(name = "u_parent_type", nullable = false, length = 50)
	public String getParentType()
	{
		return parentType;
	}
	public void setParentType(String parentType)
	{
		this.parentType = parentType;
	}
	
	@Column(name = "u_child_id", nullable = false, length = 32)
	public String getChildId()
	{
		return childId;
	}
	public void setChildId(String childId)
	{
		this.childId = childId;
	}
	
	@Column(name = "u_child_type", nullable = false, length = 50)
	public String getChildType()
	{
		return childType;
	}
	public void setChildType(String childType)
	{
		this.childType = childType;
	}
	
	@Override
	public ResolveCompRelVO doGetVO()
	{
		ResolveCompRelVO vo = new ResolveCompRelVO();
		
		super.doGetBaseVO(vo);
		vo.setParentId(this.getParentId());
		vo.setParentType(this.getParentType());
		vo.setChildId(this.getChildId());
		vo.setChildType(this.getChildType());
		
		return vo;
	}
	
	@Override
	public void applyVOToModel(ResolveCompRelVO vo)
	{
		if (vo != null)
		{
			super.applyVOToModel(vo);
			
			this.setParentId(StringUtils.isNotBlank(vo.getParentId()) && vo.getParentId().equals(VO.STRING_DEFAULT) ? getParentId() : vo.getParentId());
			this.setParentType(StringUtils.isNotBlank(vo.getParentType()) && vo.getParentType().equals(VO.STRING_DEFAULT) ? getParentType() : vo.getParentType());
			this.setChildId(StringUtils.isNotBlank(vo.getChildId()) && vo.getChildId().equals(VO.STRING_DEFAULT) ? getChildId() : vo.getChildId());
			this.setChildType(StringUtils.isNotBlank(vo.getChildType()) && vo.getChildType().equals(VO.STRING_DEFAULT) ? getChildType() : vo.getChildType());
		}
	}
}
