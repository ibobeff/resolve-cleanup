/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;


import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;

import com.resolve.services.hibernate.vo.OrganizationVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;


@Entity
@Table(name = "organization")
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Organization extends BaseModel<OrganizationVO> implements java.io.Serializable
{
    private static final long serialVersionUID = -3695199679973274822L;
    
    private String UOrganizationName;
    private String UDescription;
    private Boolean UDisable;

    // object references
    private Collection<ConfigActiveDirectory> configActiveDirectories;
    private Collection<ConfigLDAP> configLdap;
    private Collection<ConfigRADIUS> configRadius;

    // object referenced by

    public Organization()
    {
    }
    
    public Organization(OrganizationVO vo)
    {
        applyVOToModel(vo);
    }


    @Column(name = "u_user_org_name")
    public String getUOrganizationName()
    {
        return UOrganizationName;
    }

    public void setUOrganizationName(String uOrganizationName)
    {
        UOrganizationName = uOrganizationName;
    }

   
    @Column(name = "u_description", length=4000)
    public String getUDescription()
    {
        return UDescription;
    }

    public void setUDescription(String uDescription)
    {
        UDescription = uDescription;
    }
    
    
    @Column(name = "u_disable", length = 1)
    @Type(type = "yes_no")
    public Boolean getUDisable()
    {
        return UDisable;
    }

    public void setUDisable(Boolean uDisable)
    {
        UDisable = uDisable;
    }

    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "belongsToOrganization")
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.DELETE)
    public Collection<ConfigActiveDirectory> getConfigActiveDirectories()
    {
        return configActiveDirectories;
    }

    public void setConfigActiveDirectories(Collection<ConfigActiveDirectory> configActiveDirectories)
    {
        this.configActiveDirectories = configActiveDirectories;
    }
    
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "belongsToOrganization")
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.DELETE)
    public Collection<ConfigLDAP> getConfigLdap()
    {
        return configLdap;
    }

    public void setConfigLdap(Collection<ConfigLDAP> configLdap)
    {
        this.configLdap = configLdap;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "belongsToOrganization")
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.DELETE)
    public Collection<ConfigRADIUS> getConfigRadius()
    {
        return configRadius;
    }

    public void setConfigRadius(Collection<ConfigRADIUS> configRadius)
    {
        this.configRadius = configRadius;
    }
    
    @Override
    public OrganizationVO doGetVO()
    {
        OrganizationVO vo = new OrganizationVO();
        super.doGetBaseVO(vo);
        
        vo.setUOrganizationName(getUOrganizationName());
        vo.setUDescription(getUDescription());
        vo.setUDisable(getUDisable());
                
        //do not add the collection references else will go into infinite loop
      
        return vo;
    }

    @Override
    public void applyVOToModel(OrganizationVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUOrganizationName(StringUtils.isNotBlank(vo.getUOrganizationName()) && vo.getUOrganizationName().equals(VO.STRING_DEFAULT) ? getUOrganizationName() : vo.getUOrganizationName());
            this.setUDescription(StringUtils.isNotBlank(vo.getUDescription()) && vo.getUDescription().equals(VO.STRING_DEFAULT) ? getUDescription() : vo.getUDescription());
            this.setUDisable(vo.getUDisable() != null ? vo.getUDisable() : getUDisable());            
        }
        
    }
}
