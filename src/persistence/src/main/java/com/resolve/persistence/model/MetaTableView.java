/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.MetaTableViewFieldVO;
import com.resolve.services.hibernate.vo.MetaTableViewVO;
import com.resolve.services.interfaces.CdataProperty;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;


@Entity
@Table(name = "meta_table_view", indexes = {
                @Index(columnList = "u_meta_table_sys_id", name = "mtv_u_meta_table_idx"),
                @Index(columnList = "u_meta_control_sys_id", name = "mtv_u_meta_control_idx"),
                @Index(columnList = "u_meta_access_rights_sys_id", name = "mtv_u_meta_access_rights_idx"),
                @Index(columnList = "u_parent_sys_id", name = "mtv_u_parent_sys_id_indx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaTableView extends BaseModel<MetaTableViewVO>  implements CdataProperty
{
    private static final long serialVersionUID = -989533702212788971L;

    public final static String RESOURCE_TYPE = "metatableview";
    
    private String UName;
    private String UDisplayName;
    private String UType;
    private Boolean UIsGlobal;
    private String UUser;
    private String UMetaFormLink;
    private String UMetaNewLink;
    private String UTarget;
    private String UParams;
    //private Boolean UIsDefaultRole;
    
    private String UCreateFormId;
    private String UEditFormId;
    
    // object references
    private MetaTable metaTable;  
    //private MetaStyle metaStyle;
    private MetaControl metaControl;
    private MetaAccessRights metaAccessRights;
    
    private MetaTableView parentMetaTableView;
    private Collection<MetaTableView> childMetaTableViews;
    
    // object referenced by
    private Collection<MetaTableViewField> metaTableViewFields;
    
    public MetaTableView()
    {
    }
    
    public MetaTableView(MetaTableViewVO vo)
    {
        applyVOToModel(vo);
    }
    
    @Column(name = "u_name", nullable = false, length = 100)
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    @Column(name = "u_display_name", length = 100)
    public String getUDisplayName()
    {
        return this.UDisplayName;
    }

    public void setUDisplayName(String UDisplayName)
    {
        this.UDisplayName = UDisplayName;
    }
    
    @Column(name = "u_type", nullable = false, length = 100)
    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }
    
    @Column(name = "u_is_global")
    public Boolean getUIsGlobal()
    {
        return this.UIsGlobal;
    } 

    public Boolean ugetUIsGlobal()
    {
        if (this.UIsGlobal == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UIsGlobal;
        }
    } 
    
    public void setUIsGlobal(Boolean UIsGlobal)
    {
        this.UIsGlobal = UIsGlobal;
    }
    
    @Column(name = "u_user", length = 255)
    public String getUUser()
    {
        return this.UUser;
    }

    public void setUUser(String UUser)
    {
        this.UUser = UUser;
    }
    
    @Column(name = "meta_form_link", length = 1000)
    public String getUMetaFormLink()
    {
        return this.UMetaFormLink;
    }

    public void setUMetaFormLink(String UMetaFormLink)
    {
        this.UMetaFormLink = UMetaFormLink;
    }
    
    @Column(name = "meta_new_link", length = 1000)
    public String getUMetaNewLink()
    {
        return this.UMetaNewLink;
    }

    public void setUMetaNewLink(String UMetaNewLink)
    {
        this.UMetaNewLink = UMetaNewLink;
    }
    
    @Column(name = "u_target", length = 50)
    public String getUTarget()
    {
        return this.UTarget;
    }

    public void setUTarget(String UTarget)
    {
        this.UTarget = UTarget;
    }
    
    @Column(name = "u_params", length = 4000)
    public String getUParams()
    {
        return this.UParams;
    }

    public void setUParams(String UParams)
    {
        this.UParams = UParams;
    }
    
    @Column(name = "u_create_form_id", length = 100)
    public String getUCreateFormId()
    {
        return UCreateFormId;
    }

    public void setUCreateFormId(String createFormId)
    {
        this.UCreateFormId = createFormId;
    }

    @Column(name = "u_edit_form_id", length = 100)
    public String getUEditFormId()
    {
        return UEditFormId;
    }

    public void setUEditFormId(String editFormId)
    {
        this.UEditFormId = editFormId;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_meta_table_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaTable getMetaTable()
    {
        return metaTable;
    }

    public void setMetaTable(MetaTable metaTable)
    {
        this.metaTable = metaTable;

        if (metaTable != null)
        {
            Collection<MetaTableView> coll = metaTable.getMetaTableViews();
            if (coll == null)
            {
                coll = new HashSet<MetaTableView>();
                coll.add(this);
                
                metaTable.setMetaTableViews(coll);
            }
        }
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_meta_control_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaControl getMetaControl()
    {
        return metaControl;
    }

    public void setMetaControl(MetaControl metaControl)
    {
        this.metaControl = metaControl;
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_meta_access_rights_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade({CascadeType.DELETE})
    public MetaAccessRights getMetaAccessRights()
    {
        return metaAccessRights;
    }

    public void setMetaAccessRights(MetaAccessRights metaAccessRights)
    {
        this.metaAccessRights = metaAccessRights;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "metaTableView")
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<MetaTableViewField> getMetaTableViewFields()
    {
        return this.metaTableViewFields;
    }

    public void setMetaTableViewFields(Collection<MetaTableViewField> metaTableViewFields)
    {
        this.metaTableViewFields = metaTableViewFields;
    }
    
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_parent_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaTableView getParentMetaTableView()
    {
        return parentMetaTableView;
    }

    public void setParentMetaTableView(MetaTableView metaTableView)
    {
        this.parentMetaTableView = metaTableView;
        
        if (metaTableView != null)
        {
            Collection<MetaTableView> coll = metaTableView.getMetaTableViews();
            if (coll == null)
            {
                coll = new HashSet<MetaTableView>();
                coll.add(this);
                
                metaTableView.setMetaTableViews(coll);
            }
        }

    }

    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "parentMetaTableView")
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<MetaTableView> getMetaTableViews()
    {
        return this.childMetaTableViews;
    }

    public void setMetaTableViews(Collection<MetaTableView> childMetaTableViews)
    {
        this.childMetaTableViews = childMetaTableViews;
    }

    @Override
    public List<String> ugetCdataProperty()
    {
        List<String> list = new ArrayList<String>();
        list.add("UMetaFormLink");
        list.add("UMetaNewLink");

        return list;
    }

    @Override
    public MetaTableViewVO doGetVO()
    {
        MetaTableViewVO vo = new MetaTableViewVO();
        super.doGetBaseVO(vo);
        
        vo.setUDisplayName(getUDisplayName());
        vo.setUIsGlobal(getUIsGlobal());
        vo.setUMetaFormLink(getUMetaFormLink());
        vo.setUMetaNewLink(getUMetaNewLink());
        vo.setUName(getUName());
        vo.setUParams(getUParams());
        vo.setUTarget(getUTarget());
        vo.setUType(getUType());
        vo.setUUser(getUUser());
        vo.setCreateFormId(getUCreateFormId());
        vo.setEditFormId(getUEditFormId());

        //refs
        vo.setMetaAccessRights(Hibernate.isInitialized(this.metaAccessRights) && getMetaAccessRights() != null ? getMetaAccessRights().doGetVO() : null) ;
        vo.setMetaControl(Hibernate.isInitialized(this.metaControl) && getMetaControl() != null ? getMetaControl().doGetVO() : null) ;
        
        Collection<MetaTableViewField> metaTableViewFields = Hibernate.isInitialized(this.metaTableViewFields) ? getMetaTableViewFields() : null;
        if(metaTableViewFields != null && metaTableViewFields.size() > 0)
        {
            Collection<MetaTableViewFieldVO> vos = new ArrayList<MetaTableViewFieldVO>();
            for(MetaTableViewField model : metaTableViewFields)
            {
                vos.add(model.doGetVO());
            }
            vo.setMetaTableViewFields(vos);
        }

        
        return vo;
    }

    @Override
    public void applyVOToModel(MetaTableViewVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUIsGlobal(vo.getUIsGlobal() != null ? vo.getUIsGlobal() : getUIsGlobal());
            this.setUDisplayName(StringUtils.isNotBlank(vo.getUDisplayName()) && vo.getUDisplayName().equals(VO.STRING_DEFAULT) ? getUDisplayName() : vo.getUDisplayName());
            this.setUMetaFormLink(StringUtils.isNotBlank(vo.getUMetaFormLink()) && vo.getUMetaFormLink().equals(VO.STRING_DEFAULT) ? getUMetaFormLink() : vo.getUMetaFormLink());
            this.setUMetaNewLink(StringUtils.isNotBlank(vo.getUMetaNewLink()) && vo.getUMetaNewLink().equals(VO.STRING_DEFAULT) ? getUMetaNewLink() : vo.getUMetaNewLink());
            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUParams(StringUtils.isNotBlank(vo.getUParams()) && vo.getUParams().equals(VO.STRING_DEFAULT) ? getUParams() : vo.getUParams());
            this.setUTarget(StringUtils.isNotBlank(vo.getUTarget()) && vo.getUTarget().equals(VO.STRING_DEFAULT) ? getUTarget() : vo.getUTarget());
            this.setUType(StringUtils.isNotBlank(vo.getUType()) && vo.getUType().equals(VO.STRING_DEFAULT) ? getUType() : vo.getUType());
            this.setUUser(StringUtils.isNotBlank(vo.getUUser()) && vo.getUUser().equals(VO.STRING_DEFAULT) ? getUUser() : vo.getUUser());
            this.setUCreateFormId(StringUtils.isNotBlank(vo.getCreateFormId()) && vo.getCreateFormId().equals(VO.STRING_DEFAULT) ? getUCreateFormId() : vo.getCreateFormId());
            this.setUEditFormId(StringUtils.isNotBlank(vo.getEditFormId()) && vo.getEditFormId().equals(VO.STRING_DEFAULT) ? getUEditFormId() : vo.getEditFormId());
            
            //references
            this.setMetaAccessRights(vo.getMetaAccessRights() != null ? new MetaAccessRights(vo.getMetaAccessRights()) : getMetaAccessRights());
            
            Collection<MetaTableViewFieldVO> metaTableViewFieldsVOs = vo.getMetaTableViewFields();
            if(metaTableViewFieldsVOs != null && metaTableViewFieldsVOs.size() > 0)
            {
                Collection<MetaTableViewField> models = new ArrayList<MetaTableViewField>();
                for(MetaTableViewFieldVO refVO : metaTableViewFieldsVOs)
                {
                    models.add(new MetaTableViewField(refVO));
                }
                this.setMetaTableViewFields(models);
            }
        }
        
    }
    
//    @Column(name = "u_is_default_role", length = 1)
//    @Type(type = "yes_no")
//    public Boolean getUIsDefaultRole()
//    {
//            return UIsDefaultRole;
//    }
//    
//    public Boolean ugetUIsDefaultRole()
//    {
//        if (this.UIsDefaultRole == null)
//        {
//            return new Boolean(false);
//        }
//        else
//        {
//            return this.UIsDefaultRole;
//        }
//    } // ugetUIsDefaultRole
//
//    public void setUIsDefaultRole(Boolean isDefaultRole)
//    {
//        UIsDefaultRole = isDefaultRole;
//    }
}
