/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

// Generated Jan 15, 2010 12:12:58 PM by Hibernate Tools 3.2.4.GA

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.WikiAttachmentContentVO;
import com.resolve.services.interfaces.VO;

/**
 * This file represents the content of the attachment. I created this to
 * simulate the lazy loading as hibernate has no way to lazy load a column in
 * the same table
 * 
 * @author jeet.marwah
 * 
 */

@Entity
@Table(name = "wikiattachmentcontent", indexes = {@Index(columnList = "u_wikiattch_sys_id", name = "wac_u_wikiattch_idx")})
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class WikiAttachmentContent extends BaseModel<WikiAttachmentContentVO>
{
    private static final long serialVersionUID = 4914199649094002809L;
    
    private byte[] UContent;

    // object references
    private WikiAttachment wikiAttachment;

    // object referenced by

    public WikiAttachmentContent()
    {
    }
    
    public WikiAttachmentContent(WikiAttachmentContentVO vo)
    {
        applyVOToModel(vo);
    }

    @Lob
    @Column(name = "u_content", length = 16777215)
    public byte[] getUContent()
    {
        return UContent;
    }

    public void setUContent(byte[] UContent)
    {
        if (UContent == null)
            this.UContent = new byte[0];
        else
        {
            this.UContent = UContent;
        }
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_wikiattch_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public WikiAttachment getWikiAttachment()
    {
        return wikiAttachment;
    }

    public void setWikiAttachment(WikiAttachment wikiAttachment)
    {
        this.wikiAttachment = wikiAttachment;
        
        // one-way only - no collections on the other side
    }

    @Override
    public WikiAttachmentContentVO doGetVO()
    {
        WikiAttachmentContentVO vo = new WikiAttachmentContentVO();
        super.doGetBaseVO(vo);
        
        vo.setUContent(getUContent());
        
        //references
//        vo.setWikiAttachment(Hibernate.isInitialized(this.wikiAttachment) && getWikiAttachment() != null ? getWikiAttachment().doGetVO() : null);
        
        return vo;
    }

    @Override
    public void applyVOToModel(WikiAttachmentContentVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUContent(vo.getUContent() != null && vo.getUContent().equals(VO.BYTE_ARRAY_DEFAULT) ? getUContent() : vo.getUContent());
            
            //references
//            this.setWikiAttachment(vo.getWikiAttachment() != null ? new WikiAttachment(vo.getWikiAttachment()) : null);
        }
        
    }

}
