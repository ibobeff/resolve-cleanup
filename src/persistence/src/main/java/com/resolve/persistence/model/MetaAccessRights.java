/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.resolve.services.hibernate.vo.MetaAccessRightsVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;


@Entity
@Table(name = "meta_access_rights", indexes = {@Index(columnList = "u_resource_sys_id, u_resource_type, u_admin_access, u_read_access", name = "mar_rsidtyp_rdad_acc_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaAccessRights extends BaseModel<MetaAccessRightsVO>
{
    private static final long serialVersionUID = -6048955032227022528L;
    
    private String UResourceType;
    private String UResourceName;
    private String UResourceId;
    private String UReadAccess;
    private String UWriteAccess;
    private String UAdminAccess;
    private String UExecuteAccess;

    // object references

    // object referenced by

    public MetaAccessRights()
    {
    }
    
    public MetaAccessRights(MetaAccessRightsVO vo)
    {
        applyVOToModel(vo);
    }
    
    @Column(name = "u_resource_type")
    public String getUResourceType()
    {
        return this.UResourceType;
    }

    public void setUResourceType(String UResourceType)
    {
        this.UResourceType = UResourceType;
    }

    @Column(name = "u_resource_name")
    public String getUResourceName()
    {
        return this.UResourceName;
    }

    public void setUResourceName(String UResourceName)
    {
        this.UResourceName = UResourceName;
    }

    @Column(name = "u_resource_sys_id", nullable = false)
    public String getUResourceId()
    {
        return this.UResourceId;
    }

    public void setUResourceId(String UResourceId)
    {
        this.UResourceId = UResourceId;
    }

    @Column(name = "u_read_access", length = 1000)
    public String getUReadAccess()
    {
        return this.UReadAccess;
    }

    public void setUReadAccess(String UReadAccess)
    {
        this.UReadAccess = UReadAccess;
    }

    @Column(name = "u_write_access", length = 1000)
    public String getUWriteAccess()
    {
        return this.UWriteAccess;
    }

    public void setUWriteAccess(String UWriteAccess)
    {
        this.UWriteAccess = UWriteAccess;
    }

    @Column(name = "u_admin_access", length = 1000)
    public String getUAdminAccess()
    {
        return this.UAdminAccess;
    }

    public void setUAdminAccess(String UAdminAccess)
    {
        this.UAdminAccess = UAdminAccess;
    }

    @Column(name = "u_execute_access", length = 1000)
    public String getUExecuteAccess()
    {
        return this.UExecuteAccess;
    }

    public void setUExecuteAccess(String UExecuteAccess)
    {
        this.UExecuteAccess = UExecuteAccess;
    }


    @Override
    public MetaAccessRightsVO doGetVO()
    {
        MetaAccessRightsVO vo = new MetaAccessRightsVO();
        super.doGetBaseVO(vo);
        
        vo.setUAdminAccess(getUAdminAccess());
        vo.setUExecuteAccess(getUExecuteAccess());
        vo.setUReadAccess(getUReadAccess());
        vo.setUResourceId(getUResourceId());
        vo.setUResourceName(getUResourceName());
        vo.setUResourceType(getUResourceType());
        vo.setUWriteAccess(getUWriteAccess());
        
        return vo;
    }

    @Override
    public void applyVOToModel(MetaAccessRightsVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);    
            
            this.setUAdminAccess(StringUtils.isNotBlank(vo.getUAdminAccess()) && vo.getUAdminAccess().equals(VO.STRING_DEFAULT) ? getUAdminAccess() : vo.getUAdminAccess());
            this.setUExecuteAccess(StringUtils.isNotBlank(vo.getUExecuteAccess()) && vo.getUExecuteAccess().equals(VO.STRING_DEFAULT) ? getUExecuteAccess() : vo.getUExecuteAccess());
            this.setUReadAccess(StringUtils.isNotBlank(vo.getUReadAccess()) && vo.getUReadAccess().equals(VO.STRING_DEFAULT) ? getUReadAccess() : vo.getUReadAccess());
            this.setUResourceId(StringUtils.isNotBlank(vo.getUResourceId()) && vo.getUResourceId().equals(VO.STRING_DEFAULT) ? getUResourceId() : vo.getUResourceId());
            this.setUResourceName(StringUtils.isNotBlank(vo.getUResourceName()) && vo.getUResourceName().equals(VO.STRING_DEFAULT) ? getUResourceName() : vo.getUResourceName());
            this.setUResourceType(StringUtils.isNotBlank(vo.getUResourceType()) && vo.getUResourceType().equals(VO.STRING_DEFAULT) ? getUResourceType() : vo.getUResourceType());
            this.setUWriteAccess(StringUtils.isNotBlank(vo.getUWriteAccess()) && vo.getUWriteAccess().equals(VO.STRING_DEFAULT) ? getUWriteAccess() : vo.getUWriteAccess());

        }
        
    }

}
