/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.esb.ESB;
import com.resolve.util.Constants;

/**
 * Utility to index 
 * 
 * @author jeet.marwah
 *
 */
public class IndexingUtil
{
    public static final String MAP_OF_ACTIONTASK = "MAP_OF_ACTIONTASK";
    public static final String DELETE_INDEX = "DELETE_INDEX";
    public static final String SYS_ID = "sys_id";
    public static final String RESOLVEACTIONTASK_UFULLNAME = "UFullName";
    public static final String INDEX_WIKIDOCUMENTS_SYS_ID = "INDEX_WIKIDOCUMENTS_SYS_ID";
    public static final String INDEX_WIKIDOCUMENT_SYS_ID = "INDEX_WIKIDOCUMENT_SYS_ID";
    public static final String INDEX_ATTACHMENTS = "INDEX_ALL_ATTACHMENTS";
    
    public static final String RESOLVEASSESS_MODEL_NAME = "ResolveAssess";
    public static final String RESOLVEPREPROCESS_MODEL_NAME = "ResolvePreprocess";
    public static final String RESOLVEPARSER_MODEL_NAME = "ResolveParser";
    public static final String RESOLVEACTIONINVOCOPTIONS_MODEL_NAME = "ResolveActionInvocOptions";
    public static final String RESOLVETAG_OLD_TAGNAME = "RESOLVETAG_OLD_TAGNAME";
    public static final String RESOLVETAG_NEW_TAGNAME = "RESOLVETAG_NEW_TAGNAME";

    /**
     * this is for indexing more then 1 action task
     * 
     * @param mapActionTask - name-value for the sys_id-UFullname of action task
     * @param shouldDelete
     */
    public static void logActionTaskToIndex(ESB esb, Map<String, String> mapActionTask, Boolean shouldDelete)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(MAP_OF_ACTIONTASK, mapActionTask);
        params.put(DELETE_INDEX, shouldDelete);
        
//        esb.sendMessage(Constants.ESB_NAME_RSVIEWS, "MAction.index", params);
//        esb.sendMessage(Constants.ESB_NAME_RSVIEWS, Constants.SERVICE_QUEUE_WIKI_INDEX, params);
        
    }//logActionTaskToIndex

    /**
     * sending the JMS message to index when an ActionTask or any dependent object is changed 
     * 
     * @param actionTaskId
     * @param actionTaskFullName
     * @param shouldDelete
     */
    public static void logActionTaskToIndex(ESB esb, String actionTaskId, String actionTaskFullName, Boolean shouldDelete)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(SYS_ID, actionTaskId);
        params.put(RESOLVEACTIONTASK_UFULLNAME, actionTaskFullName);
        params.put(DELETE_INDEX, shouldDelete);
        
//        esb.sendMessage(Constants.ESB_NAME_RSVIEWS, "MAction.index", params);
//        esb.sendMessage(Constants.ESB_NAME_RSVIEWS, Constants.SERVICE_QUEUE_WIKI_INDEX, params);
        
    }//logActionTaskToIndex

    /**
     * wikidocs to index
     * 
     * @param esb
     * @param wikidocIds
     * @param shouldDelete
     */
    public static void logWikiDocumentsToIndex(ESB esb, List<String> wikidocIds, Boolean shouldDelete)
    {
        //log it in the index table
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(INDEX_WIKIDOCUMENTS_SYS_ID, wikidocIds);
        params.put(DELETE_INDEX, shouldDelete);

//        esb.sendMessage(Constants.ESB_NAME_RSVIEWS, "MAction.index", params);
//        esb.sendMessage(Constants.ESB_NAME_RSVIEWS, Constants.SERVICE_QUEUE_WIKI_INDEX, params);
    }

    /**
     * wikidoc to index
     * 
     * @param esb
     * @param wikidocId
     * @param shouldDelete
     */
    public static void logWikiDocumentToIndex(ESB esb, String wikidocId, Boolean shouldDelete)
    {
        //log it in the index table
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(INDEX_WIKIDOCUMENT_SYS_ID, wikidocId);
        params.put(DELETE_INDEX, shouldDelete);

//        esb.sendMessage(Constants.ESB_NAME_RSVIEWS, "MAction.index", params);
//        esb.sendMessage(Constants.ESB_NAME_RSVIEWS, Constants.SERVICE_QUEUE_WIKI_INDEX, params);
    }
    
    public static void logAssessorToIndex(ESB esb, String sys_id, Boolean shouldDelete)
    {
        //log it in the index table
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(RESOLVEASSESS_MODEL_NAME, sys_id);
        params.put(DELETE_INDEX, shouldDelete);

//        esb.sendMessage(Constants.ESB_NAME_RSVIEWS, "MAction.index", params);
//        esb.sendMessage(Constants.ESB_NAME_RSVIEWS, Constants.SERVICE_QUEUE_WIKI_INDEX, params);
    }
    
    public static void logPreprocessorToIndex(ESB esb, String sys_id, Boolean shouldDelete)
    {
        //log it in the index table
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(RESOLVEPREPROCESS_MODEL_NAME, sys_id);
        params.put(DELETE_INDEX, shouldDelete);

//        esb.sendMessage(Constants.ESB_NAME_RSVIEWS, "MAction.index", params);
//        esb.sendMessage(Constants.ESB_NAME_RSVIEWS, Constants.SERVICE_QUEUE_WIKI_INDEX, params);
    }
    
    public static void logParserToIndex(ESB esb, String sys_id, Boolean shouldDelete)
    {
        //log it in the index table
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(RESOLVEPARSER_MODEL_NAME, sys_id);
        params.put(DELETE_INDEX, shouldDelete);

//        esb.sendMessage(Constants.ESB_NAME_RSVIEWS, "MAction.index", params);
//        esb.sendMessage(Constants.ESB_NAME_RSVIEWS, Constants.SERVICE_QUEUE_WIKI_INDEX, params);
    }
    
    public static void logOptionsToIndex(ESB esb, String sys_id, Boolean shouldDelete)
    {
        //log it in the index table
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(RESOLVEACTIONINVOCOPTIONS_MODEL_NAME, sys_id);
        params.put(DELETE_INDEX, shouldDelete);

//        esb.sendMessage(Constants.ESB_NAME_RSVIEWS, "MAction.index", params);
//        esb.sendMessage(Constants.ESB_NAME_RSVIEWS, Constants.SERVICE_QUEUE_WIKI_INDEX, params);
    }
    
    public static void archive(ESB esb, Map<String, Object> params)
    {
        esb.sendMessage(Constants.ESB_NAME_RSVIEW, "MAction.archive", params);
    }

}
