/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

// Generated Mar 26, 2010 9:19:26 AM by Hibernate Tools 3.2.4.GA

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.WikidocStatisticsVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

/**
 * WikidocStatistics generated by hbm2java
 */
@Entity
@Table(name = "wikidoc_statistics", indexes = {@Index(columnList = "u_wikidoc_sys_id", name = "ws_u_wikidoc_sys_id_idx")})
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class WikidocStatistics extends BaseModel<WikidocStatisticsVO>
{
    private static final long serialVersionUID = -7151357285868752715L;
    
	private String UContentChecksum;
	private String UModelChecksum;
	private String UExceptionChecksum;
	private String UFinalChecksum;
	private Integer UViewCount;
	private Integer UEditCount;
	private Integer UExecuteCount;
	
	private Integer UReviewCount=0;
	private Integer UUsefulNoCount;
	private Integer UUsefulYesCount;

    // object references
	private WikiDocument wikidoc;
	
    // object referenced by
	
	public WikidocStatistics()
	{
	}

    public WikidocStatistics(WikidocStatisticsVO vo)
    {
        applyVOToModel(vo);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_wikidoc_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public WikiDocument getWikidoc()
    {
        return wikidoc;
    }

    public void setWikidoc(WikiDocument wikidoc)
    {
        this.wikidoc = wikidoc;

        if (wikidoc != null)
        {
            Collection<WikidocStatistics> coll = wikidoc.getWikidocStatistics();
            if (coll == null)
            {
				coll = new HashSet<WikidocStatistics>();
	            coll.add(this);
	            
                wikidoc.setWikidocStatistics(coll);
            }
        }
    }
	@Column(name = "u_content_checksum")
	public String getUContentChecksum()
	{
		return this.UContentChecksum;
	}

	public void setUContentChecksum(String UContentChecksum)
	{
		this.UContentChecksum = UContentChecksum;
	}

	@Column(name = "u_model_checksum")
	public String getUModelChecksum()
	{
		return this.UModelChecksum;
	}

	public void setUModelChecksum(String UModelChecksum)
	{
		this.UModelChecksum = UModelChecksum;
	}

	@Column(name = "u_exception_checksum")
	public String getUExceptionChecksum()
	{
		return this.UExceptionChecksum;
	}

	public void setUExceptionChecksum(String UExceptionChecksum)
	{
		this.UExceptionChecksum = UExceptionChecksum;
	}

	@Column(name = "u_final_checksum")
	public String getUFinalChecksum()
	{
		return this.UFinalChecksum;
	}

	public void setUFinalChecksum(String UFinalChecksum)
	{
		this.UFinalChecksum = UFinalChecksum;
	}

	@Column(name = "u_view_count")
	public Integer getUViewCount()
	{
		return this.UViewCount;
	}

	public void setUViewCount(Integer UViewCount)
	{
		this.UViewCount = UViewCount;
	}

	@Column(name = "u_edit_count")
	public Integer getUEditCount()
	{
		return this.UEditCount;
	}

	public void setUEditCount(Integer UEditCount)
	{
		this.UEditCount = UEditCount;
	}

	@Column(name = "u_execute_count")
	public Integer getUExecuteCount()
	{
		return this.UExecuteCount;
	}

	public void setUExecuteCount(Integer UExecuteCount)
	{
		this.UExecuteCount = UExecuteCount;
	}

    @Column(name = "u_review_count")
    public Integer getUReviewCount()
    {
        return UReviewCount;
    }

    public void setUReviewCount(Integer uReviewCount)
    {
        UReviewCount = uReviewCount;
    }

    @Column(name = "u_useful_no_count")
    public Integer getUUsefulNoCount()
    {
        return UUsefulNoCount;
    }

    public void setUUsefulNoCount(Integer uUsefulNoCount)
    {
        UUsefulNoCount = uUsefulNoCount;
    }

    @Column(name = "u_useful_yes_count")
    public Integer getUUsefulYesCount()
    {
        return UUsefulYesCount;
    }

    public void setUUsefulYesCount(Integer uUsefulYesCount)
    {
        UUsefulYesCount = uUsefulYesCount;
    }

    @Override
    public WikidocStatisticsVO doGetVO()
    {
        WikidocStatisticsVO vo = new WikidocStatisticsVO();
        super.doGetBaseVO(vo);
        
        vo.setUContentChecksum(getUContentChecksum());
        vo.setUEditCount(getUEditCount());
        vo.setUExceptionChecksum(getUExceptionChecksum());
        vo.setUExecuteCount(getUExecuteCount());
        vo.setUFinalChecksum(getUFinalChecksum());
        vo.setUModelChecksum(getUModelChecksum());
        vo.setUReviewCount(getUReviewCount());
        vo.setUUsefulNoCount(getUUsefulNoCount());
        vo.setUUsefulYesCount(getUUsefulYesCount());
        vo.setUViewCount(getUViewCount());
        
        //references - SHOULD NOT DO THIS - YOU CAN COME FROM INVOC TO PARAMS BUT NOT THE OTHER WAY AROUND AS IT WILL BE INFINITE LOOP
//        vo.setWikidoc(Hibernate.isInitialized(this.wikidoc) && getWikidoc() != null ? getWikidoc().doGetVO() : null);
        
        return vo;
    }

    @Override
    public void applyVOToModel(WikidocStatisticsVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUContentChecksum(StringUtils.isNotBlank(vo.getUContentChecksum()) && vo.getUContentChecksum().equals(VO.STRING_DEFAULT) ? getUContentChecksum() : vo.getUContentChecksum());
            this.setUModelChecksum(StringUtils.isNotBlank(vo.getUModelChecksum()) && vo.getUModelChecksum().equals(VO.STRING_DEFAULT) ? getUModelChecksum() : vo.getUModelChecksum());
            this.setUExceptionChecksum(StringUtils.isNotBlank(vo.getUExceptionChecksum()) && vo.getUExceptionChecksum().equals(VO.STRING_DEFAULT) ? getUExceptionChecksum() : vo.getUExceptionChecksum());
            this.setUFinalChecksum(StringUtils.isNotBlank(vo.getUFinalChecksum()) && vo.getUFinalChecksum().equals(VO.STRING_DEFAULT) ? getUFinalChecksum() : vo.getUFinalChecksum());

            this.setUEditCount(vo.getUEditCount() != null && vo.getUEditCount().equals(VO.INTEGER_DEFAULT) ? getUEditCount() : vo.getUEditCount());
            this.setUExecuteCount(vo.getUExecuteCount() != null && vo.getUExecuteCount().equals(VO.INTEGER_DEFAULT) ? getUExecuteCount() : vo.getUExecuteCount());
            this.setUReviewCount(vo.getUReviewCount() != null && vo.getUReviewCount().equals(VO.INTEGER_DEFAULT) ? getUReviewCount() : vo.getUReviewCount());
            this.setUUsefulNoCount(vo.getUUsefulNoCount() != null && vo.getUUsefulNoCount().equals(VO.INTEGER_DEFAULT) ? getUUsefulNoCount() : vo.getUUsefulNoCount());
            this.setUUsefulYesCount(vo.getUUsefulYesCount() != null && vo.getUUsefulYesCount().equals(VO.INTEGER_DEFAULT) ? getUUsefulYesCount() : vo.getUUsefulYesCount());
            this.setUViewCount(vo.getUViewCount() != null && vo.getUViewCount().equals(VO.INTEGER_DEFAULT) ? getUViewCount() : vo.getUViewCount());
            
            //references
//            this.setWikidoc(vo.getWikidoc() != null ? new WikiDocument(vo.getWikidoc()) : null);
        }
        
    }
	
    @Override
	public String toString()
	{
	    return "" + (StringUtils.isNotBlank(getWikidoc().getUFullname()) ? 
	                 getWikidoc().getUFullname() : getWikidoc().getSys_id()) +
	           " Statistics : View Count = " + (UViewCount == null ? 0 : UViewCount) +
	           ", Edit Count = " + (UEditCount == null ? 0 : UEditCount) +
	           ", Execute Count = " + (UExecuteCount == null ? 0 : UExecuteCount) + 
	           ", Review Count = " + (UReviewCount == null ? 0 : UReviewCount) + 
	           ", Useful No Count = " + (UUsefulNoCount == null ? 0 : UUsefulNoCount) +
	           ", Useful Yes Count = " + (UUsefulYesCount == null ? 0 : UUsefulYesCount);
	           
	}

}
