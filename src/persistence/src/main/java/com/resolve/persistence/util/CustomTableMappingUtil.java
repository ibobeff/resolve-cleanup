/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

import org.dom4j.Element;
import org.dom4j.Namespace;
import org.dom4j.Node;
import org.dom4j.tree.DefaultAttribute;

import com.resolve.persistence.dao.CustomTableDAO;
import com.resolve.persistence.model.CustomTable;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.ERR;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class CustomTableMappingUtil
{
    private static final String SYS_ORG = "sys_org";
    private static final String SYS_PERM = "sys_perm";
    private static final String SYS_UPDATED_ON = "sys_updated_on";
    private static final String SYS_UPDATED_BY = "sys_updated_by";
    private static final String SYS_CREATED_ON = "sys_created_on";
    private static final String SYS_CREATED_BY = "sys_created_by";
    
    private static final String ERROR_MAPPING_FILE_IS_NOT_OPEN = "Custom table mapping file is not opened yet.";
    private static final String ERROR_OPENING_MAPPING_FILE = "Failed to open custom table mapping configuration file";
    private static final String ERROR_UNKNOWN_FILE_LOCATION = "Unknown custom table mapping file location!";
    
    public static boolean isHibernateInitialized = false;
    private static ConcurrentHashMap<String, String> class2TableMap = new ConcurrentHashMap<String, String>();
    private static ConcurrentHashMap<String, String> table2ClassMap = new ConcurrentHashMap<String, String>();
    private static ConcurrentHashMap<String, String> tableNameLC2TCMap = new ConcurrentHashMap<String, String>();
    
    private static String customTableMappingFile;
    private static XDoc defaultDoc;
    private static final String HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " + "<!DOCTYPE hibernate-mapping PUBLIC \"-//Hibernate/Hibernate Mapping DTD 3.0//EN\" \"http://www.hibernate.org/dtd/hibernate-mapping-3.0.dtd\">" + "<hibernate-mapping default-cascade=\"none\" default-access=\"property\" default-lazy=\"true\" auto-import=\"true\">";
    private static final String FOOTER = "</hibernate-mapping>";
    
    public static XDoc openMappingFile()
    {
        return getMappingDoc();
    }

    @SuppressWarnings("unchecked")
    public static XDoc reloadMappingFile()
    {
        if (StringUtils.isEmpty(customTableMappingFile))
        {
            throw new RuntimeException(ERROR_UNKNOWN_FILE_LOCATION);
        }
        else
        {
            try
            {
                defaultDoc = getMappingDoc(customTableMappingFile);
                HibernateUtil.setCustomeTableXml(defaultDoc.toPrettyString());
            }
            catch (Exception ex)
            {
                Log.log.error(ERROR_OPENING_MAPPING_FILE, ex);
                throw new RuntimeException(ex);
            }
            
            if (defaultDoc != null)
            {
                List<Element> classNodes = defaultDoc.getNodes("//class");
                
                if (classNodes != null && !classNodes.isEmpty())
                {
                    class2TableMap.clear();
                    table2ClassMap.clear();
                    tableNameLC2TCMap.clear();
                    
                    for (Element node : classNodes)
                    {
                        String className = node.attributeValue("entity-name");
                        String tableName = node.attributeValue("table");
                        
                        if (StringUtils.isNotBlank(className) && StringUtils.isNotBlank(tableName))
                        {
                            class2TableMap.put(className, tableName.toLowerCase());
                            table2ClassMap.put(tableName.toLowerCase(), className);
                            tableNameLC2TCMap.put(tableName.toLowerCase(), tableName);
                        }
                    }
                }
            }
        }
        return defaultDoc;
    }

    public static XDoc getMappingDoc()
    {
        if (defaultDoc != null)
        {
            return defaultDoc;
        }

        if (StringUtils.isEmpty(customTableMappingFile))
        {
            throw new RuntimeException(ERROR_UNKNOWN_FILE_LOCATION);
        }
        else
        {
            try
            {
                defaultDoc = getMappingDoc(customTableMappingFile);
            }
            catch (Exception ex)
            {
                Log.log.error(ERROR_OPENING_MAPPING_FILE, ex);
                throw new RuntimeException(ex);
            }
        }
        return defaultDoc;
    }

    public static void setCustomMappingFileLocation(String location)
    {
    	if (StringUtils.isNotBlank(location)) {
    		customTableMappingFile = location + File.separator + "customtables.hbm.xml";
    	} else {
    		customTableMappingFile = null;
    	}
    }

    public static String getCustomMappingFile()
    {
        return customTableMappingFile;
    }

    private static XDoc getMappingDoc(String location) throws Exception
    {
        File file = FileUtils.getFile(location);
        return getMappingDoc(file);
    }

    @SuppressWarnings("unchecked")
    private static XDoc getMappingDoc(File file) throws Exception
    {
        XDoc doc = null;
        if (!file.exists())
        {
            doc = new XDoc();
            doc.addRoot("hibernate-mapping");

            List<Namespace> nss = doc.getRoot().declaredNamespaces();
            for (Namespace ss : nss)
            {
                if (!ss.equals(Namespace.NO_NAMESPACE))
                {
                    doc.getRoot().remove(ss);
                }
            }
            doc.getDocument().addDocType("hibernate-mapping", "-//Hibernate/Hibernate Mapping DTD 3.0//EN", "http://www.hibernate.org/dtd/hibernate-mapping-3.0.dtd");
        } 
        else
        {
            doc = new XDoc(file, true);

            Element root = doc.getRoot();
            List<Namespace> nss = root.declaredNamespaces();
            for (Namespace ss : nss)
            {
                if (!ss.equals(Namespace.NO_NAMESPACE))
                {
                    doc.getRoot().remove(ss);
                }
            }
        }

        return doc;
    }

    public static boolean hasEntity(String entityName)
    {
        if (getMappingDoc() == null)
        {
            throw new RuntimeException(ERROR_MAPPING_FILE_IS_NOT_OPEN);
        }

        return getEntityElement(getMappingDoc(), entityName) != null;
    }

    public static Node removeEntity(String entityName)
    {
        if (getMappingDoc() == null)
        {
            throw new RuntimeException(ERROR_MAPPING_FILE_IS_NOT_OPEN);
        }

        Node n = getEntityElement(getMappingDoc(), entityName);
        if (n != null)
        {
            n.detach();
        }
        return n;
    }

    private static Node getEntityElement(XDoc doc, String entityName)
    {
        return doc.getNode("/hibernate-mapping/class[@entity-name='" + entityName + "']");
    }

    public static List<String> getAllTableNames()
    {
        return getAllEntityNames();
    }

    @SuppressWarnings("unchecked")
    public static List<String> getAllEntityNames()
    {
        XDoc doc = getMappingDoc();
        List<Element> nodes = doc.getNodes("/hibernate-mapping/class");
        List<String> names = new ArrayList<String>();
        for (Element n : nodes)
        {
            names.add(n.attributeValue("entity-name"));
        }
        return names;
    }

    public static Element updateTable(CustomTableDefinition d)
    {
        if (getMappingDoc() == null)
        {
            throw new RuntimeException(ERROR_MAPPING_FILE_IS_NOT_OPEN);
        }

        return updateTable(getMappingDoc(), d);
    }

    private static Element updateTable(XDoc doc, CustomTableDefinition d)
    {
        String entityName = d.getEntityName() == null ? d.getTableName() : d.getEntityName();
        Node entity = getEntityElement(doc, entityName);

        // Remove entity definition if exists
        if (entity != null)
        {
            entity.detach();
        }

        Element hbnMappingNode = (Element) doc.getNode("/hibernate-mapping");
        Element clsElem = hbnMappingNode.addElement("class");
        clsElem.addAttribute("entity-name", entityName);
        clsElem.addAttribute("table", d.getTableName());

        addColumnsToEntity(clsElem, d);
        return clsElem;
    }

    private static Element addColumnsToEntity(Element entity, CustomTableDefinition d)
    {
        addDefaultColumns(entity);

        List<ColumnDefinition> cols = d.getColumns();
        if (cols != null)
        {
            for (ColumnDefinition c : cols)
            {
                c.addColumn(entity);
            }
        }

        return entity;
    }

    private static Element addDefaultColumns(Element entity)
    {
        IDDefinition id = new IDDefinition();
        id.addColumn(entity);

        PropertyDefinition createdBy = new PropertyDefinition(SYS_CREATED_BY, SYS_CREATED_BY, "string");
        createdBy.addColumn(entity);

        PropertyDefinition createdOn = new PropertyDefinition(SYS_CREATED_ON, SYS_CREATED_ON, "timestamp");
        createdOn.addColumn(entity);

        PropertyDefinition updatedBy = new PropertyDefinition(SYS_UPDATED_BY, SYS_UPDATED_BY, "string");
        updatedBy.addColumn(entity);

        PropertyDefinition updatedOn = new PropertyDefinition(SYS_UPDATED_ON, SYS_UPDATED_ON, "timestamp");
        updatedOn.addColumn(entity);
        
//        PropertyDefinition sysRoles = new PropertyDefinition(SYS_PERM, SYS_PERM, "string");
//        sysRoles.addColumn(entity);
        
        PropertyDefinition sysOrganization  = new PropertyDefinition(SYS_ORG, SYS_ORG, "string");
        sysOrganization.addColumn(entity);

        return entity;
    }

    public static void updateMappingFile(String xml) throws Exception
    {
        if (getMappingDoc() == null)
        {
            throw new RuntimeException(ERROR_MAPPING_FILE_IS_NOT_OPEN);
        }

        if (StringUtils.isBlank(getCustomMappingFile())) {
            /*throw new RuntimeException(ERROR_MAPPING_FILE_IS_NOT_OPEN)*/return;
        }

        outputMappingDoc(getMappingDoc(), getCustomMappingFile());
    }

    @SuppressWarnings("unchecked")
    public static void saveMappingFile()
    {
        if (getMappingDoc() == null)
        {
            throw new RuntimeException(ERROR_MAPPING_FILE_IS_NOT_OPEN);
        }

        if (StringUtils.isBlank(getCustomMappingFile())) {
            /*throw new RuntimeException(ERROR_MAPPING_FILE_IS_NOT_OPEN);*/return;
        }

        try
        {
            List<Element> nodes = getMappingDoc().getNodes("/hibernate-mapping/class");
            Set<String> names = new HashSet<String>();
            
            if (nodes != null && !nodes.isEmpty())
            {
                class2TableMap.clear();
                table2ClassMap.clear();
                tableNameLC2TCMap.clear();
                
                // remove duplicates
                for (Element n : nodes)
                {
                    String name = n.attributeValue("entity-name");
                    if (names.contains(name))
                    {
                        Log.log.debug("found duplicate node: "+name);
                        n.detach();
                    }
                    else
                    {
                        names.add(name);
                        
                        String table = n.attributeValue("table");
                        
                        if (StringUtils.isNotBlank(table))
                        {
                            class2TableMap.put(name, table.toLowerCase());
                            table2ClassMap.put(table.toLowerCase(), name);
                            tableNameLC2TCMap.put(table.toLowerCase(), table);
                        }
                        
                        List<Element> fkElems = n.elements("many-to-one");
                        if (fkElems!=null)
                        {
                        	for (Element fkElem : fkElems)
                        	{
                        		fkElem.addAttribute("foreign-key", "none");
                        	}
                        }
                    }
                }
            }
            
            outputMappingDoc(getMappingDoc(), getCustomMappingFile());

        }
        catch (Exception ex)
        {
            throw new RuntimeException(ex);
        }
    }
    
    public static synchronized void saveMappingFile(String xml) throws Exception
    {
        if (StringUtils.isBlank(getCustomMappingFile())) {
            /*throw new RuntimeException("Custom table mapping file is null.")*/return;
        }

        File file = FileUtils.getFile(getCustomMappingFile());
        Writer output = null;
        output = new BufferedWriter(new FileWriter(file));
        output.write(xml);
        output.close();

        reloadMappingFile();
    }

    public static void outputMappingDoc(XDoc doc, String location) throws Exception
    {
    	if (StringUtils.isNotBlank(location)) {
    		Log.log.info("Saving Custom Tables mapping file: "+location);
    		File file = FileUtils.getFile(location);
    		doc.toFile(file);
    		HibernateUtil.setCustomeTableXml(doc.toPrettyString());
        
    		Log.log.trace("mapping file: "+HibernateUtil.getCustomTableXml());
    	}
    }

    public static void outputMappingDoc(XDoc doc, File file) throws Exception
    {
        doc.toFile(file);
    }

    // Merge with default mapping doc
    public static MappingMergeResult mergeMappingDoc(String srcFile)
    {
        File file = FileUtils.getFile(srcFile);
        XDoc srcDoc = null;
        try
        {
            srcDoc = new XDoc(file, true);
        }
        catch (Exception ex)
        {
            Log.log.error(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
        return mergeMappingDoc(srcDoc);
    }

    // Merge with default mapping doc
    public static synchronized MappingMergeResult mergeMappingDoc(XDoc src)
    {
        if (defaultDoc == null)
        {
            reloadMappingFile();
        }
        MappingMergeResult r = mergeMappingDoc(defaultDoc, src);
        defaultDoc = r.doc;
        saveMappingFile();
        return r;
    }

    @SuppressWarnings("unchecked")
    public static MappingMergeResult mergeMappingDoc(XDoc target, XDoc src)
    {
        XDoc doc = null;
        boolean targetUpdated = false;
        try
        {
            doc = new XDoc(target.toString(), true);
            List<Namespace> nss = doc.getRoot().declaredNamespaces();
            for (Namespace ss : nss)
            {
                if (!ss.equals(Namespace.NO_NAMESPACE))
                {
                    doc.getRoot().remove(ss);
                }
            }

            List<Element> nodes = src.getNodes("/hibernate-mapping/class[@entity-name]");
            for (Element n : nodes)
            {
                Element newTable = n.createCopy();
                String entityName = newTable.attributeValue("entity-name");
                Element existingTable = (Element) doc.getNode("/hibernate-mapping/class[@entity-name='" + entityName + "']");

                if (existingTable == null)
                {
                    // Add new custom table if it doesn't exist
                    Element existingParent = (Element) doc.getNode("/hibernate-mapping");
                    existingParent.add(newTable);
                    targetUpdated = true;
                }
                else
                {
                    // If custom table already exists, then merge properties and
                    // relations
                    List<Element> elems = newTable.elements();
                    List<Element> oldElems = existingTable.elements();
                    Map<String, Element> oldElemMap = new HashMap<String, Element>();
                    for (Element old : oldElems)
                    {
                        String oldName = old.attribute("name").getValue();
                        oldElemMap.put(oldName, old);
                    }

                    for (Element e : elems)
                    {
                        String name = e.attribute("name").getValue();
                        if (oldElemMap.get(name) == null) {
                            Element cp = e.createCopy();
                            existingTable.add(cp);
                            targetUpdated = true;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Log.log.error(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }

        return new MappingMergeResult(doc, targetUpdated);
    }

    @SuppressWarnings("unchecked")
    public static void updateCutomTableSchema() throws Exception
    {   
        List<CustomTable> tables = null;
        try
        {
          HibernateProxy.setCurrentUser("system");
        	tables = (List<CustomTable>) HibernateProxy.execute(() -> {
	            CustomTableDAO dao = HibernateUtil.getDAOFactory().getCustomTableDAO();
	            return dao.findAll();
        	});
        }
        catch (Throwable t)
        {
                      HibernateUtil.rethrowNestedTransaction(t);
            Log.log.error(t);
        }

        StringBuilder buffer = new StringBuilder();
        for (CustomTable table : tables)
        {
            if (table.getUSchemaDefinition() != null)
            {
            	String def = table.getUSchemaDefinition();
//                if (def.matches(".*type=\"clob\".*"))
                if (Pattern.compile(".*type=\"clob\".*", Pattern.DOTALL).matcher(def).matches())
                {	
                	Log.log.warn("Custom table " + table.getUName() + " has invalid type definition: type=\"clob\" in schema defintion: " + def);
                    def = def.replaceAll("type=\"clob\"", "type=\"materialized_clob\"");
                	Log.log.warn("Updated table " + table.getUName() + " schema definition: " + def);
                }
                buffer.append(def);
            }
        }

        String mappingStr = HEADER + buffer.toString() + FOOTER; 

        XDoc doc = new XDoc(mappingStr, true);
        List<Namespace> nss = doc.getRoot().declaredNamespaces();
        for (Namespace ss : nss)
        {
            if (!ss.equals(Namespace.NO_NAMESPACE))
            {
                doc.getRoot().remove(ss);
            }
        }

        List<Element> nodes = doc.getNodes("/hibernate-mapping/class");
        Set<String> names = new HashSet<String>();

        // remove duplicates
        for (Element n : nodes)
        {
            String name = n.attributeValue("entity-name");
            if (names.contains(name))
            {
                n.getParent().detach();
            }
            else
            {
                names.add(name);
            }
        }

        MappingMergeResult r = mergeMappingDoc(doc);

        if (r.docUpdated)
        {
            HibernateUtil.reinit();
        }
    }

    @SuppressWarnings("unchecked")
    public static void syncDBCustomTableSchema()
    {
        boolean success = false;
        Log.log.info("Init Custom Table Mapping file");
        try
        {
            StringBuilder buffer = new StringBuilder();
            
            SQLConnection sqlConn = null;
            int reTry=1;
            final int maxRetries = 6;

            while (reTry < maxRetries && !success)
            {
	            try
	            {
	                sqlConn = SQL.getConnection();
	
	                String sql = "SELECT u_name, u_schema_definition FROM custom_table";
	                ResultSet rs = sqlConn.executeQuery(sql);
	                while (rs.next())
	                {
	                    String name = rs.getString(1);
	                    String def = rs.getString(2);

	                    if (Pattern.compile(".*type=\"clob\".*", Pattern.DOTALL).matcher(def).matches())
//	                    if (def.matches(".*type=\"clob\".*"))
	                    {	
	                    	Log.log.warn("Custom table " + name + " has invalid type definition: type=\"clob\" in schema defintion: " + def);
		                    def = def.replaceAll("type=\"clob\"", "type=\"materialized_clob\"");
	                    	Log.log.warn("Updated table " + name + " schema definition: " + def);
	                    }
	                    
	                    if (StringUtils.isNotEmpty(def))
	                    {
		                    Log.log.debug("Adding Table Name: "+name);
	                        buffer.append(def);
	                    }
	                }
	                success = true;
	            }
	            catch (Throwable e)
	            {
                    if (e.getMessage().contains("doesn't exist") || e.getMessage().contains("does not exist"))
                    {
                        Log.log.warn("Skipping custom tables initialization.\n"+e.getMessage());
                        reTry = maxRetries;
                    }
                    else
                    {
    	                Log.log.error(e.getMessage(), e);
    	                Log.log.error("SQL.getConnection() retry # " + reTry);
    	                reTry++;
                    }
	            }
	            finally
	            {
	                SQL.close(sqlConn);
	            }
            }
            
            if (success)
            {
	            String mappingStr = HEADER + buffer.toString() + FOOTER;
	            Log.log.debug(buffer);
	
	            XDoc doc = new XDoc(mappingStr, true);
	            List<Namespace> nss = doc.getRoot().declaredNamespaces();
	            for (Namespace ss : nss)
	            {
	                if (!ss.equals(Namespace.NO_NAMESPACE))
	                {
	                    doc.getRoot().remove(ss);
	                }
	            }
	            
	            defaultDoc = doc;
	            saveMappingFile();
            }
        }
        catch (Throwable t)
        {
            Log.alert(ERR.E20006, t);
            success=false;
        }
    } // syncDBCustomTableSchema
    
    public static void createEmptyCustomMappingFile()
    {
        Log.log.info("Creating empty custom mapping file: "+customTableMappingFile);
        
        // init content
        String content = HEADER + FOOTER; 
        
        try
        {
	        File file = FileUtils.getFile(customTableMappingFile);
	        FileUtils.writeStringToFile(file, content);
        }
        catch (Exception e)
        {
            String msg = "Failed to create empty customtable mapping file. "+e.getMessage();
            Log.log.error(msg, e);
            throw new RuntimeException(msg);
        }
    } // createEmptyCustomMappingFile
    
    /**
     * api that removes the dependency elements from the cfg files. For eg, if a table is references by another table, it adds the many-to-one element. So when that 
     * dependent table is deleted, we have to clear the dependency of that table also. This api removes that reference
     * 
     * @param refEntityName
     */
    public static void removeEntityDependencies(String refEntityName)
    {
        if(StringUtils.isNotBlank(refEntityName))
        {
            String xpath = "//class[many-to-one[@entity-name=\"" + refEntityName + "\"]]";            
            getMappingDoc().removeElements(xpath);
        }
    }
    
    public static String class2Table(String entityClassName)
    {
        String tableName = null;
        
        if (StringUtils.isBlank(StringUtils.strip(entityClassName)))
        {
            return tableName;
        }
        
        return class2TableMap.get(StringUtils.strip(entityClassName));
    } // class2Table
    
    public static String class2TableTrueCase(String entityClassName)
    {
        String tableNameTC = null;
        
        if (StringUtils.isBlank(StringUtils.strip(entityClassName)))
        {
            return tableNameTC;
        }
        
        String tableNameLC = class2Table(entityClassName);
        
        if (StringUtils.isBlank(tableNameLC) && tableNameLC2TCMap.containsKey(tableNameLC))
        {
            return tableNameTC;
        }
        
        return tableNameLC2TCMap.get(tableNameLC);
    } // class2TableTrueCase
    
    public static String table2Class(String tableName)
    {
        return table2ClassMap.get(StringUtils.strip(tableName).toLowerCase());
    } // table2Class
    
    public static boolean modelExists(Class<?> modelClass)
    {
        boolean modelExists = false;
        
        if (modelClass == null)
        {
            return modelExists;
        }
        
        return modelExists(modelClass.getName());
    }
    
    public static boolean modelExists(String modelClassName)
    {
        boolean modelExists = false;
        
        if (StringUtils.isBlank(StringUtils.strip(modelClassName)))
        {
            return modelExists;
        }
                
        return class2TableMap.containsKey(StringUtils.strip(modelClassName));
    }
    
    public static boolean tableExists(String tableName)
    {
        boolean tableExists = false;
        
        if (StringUtils.isBlank(StringUtils.strip(tableName)))
        {
            return tableExists;
        }
        
        return table2ClassMap.containsKey(StringUtils.strip(tableName).toLowerCase());
    }
    
    public static String getTrueCaseTableName(String tableName)
    {
        if (StringUtils.isBlank(tableName))
        {
            return null;
        }
        
        String className = table2Class(tableName);
        
        if (StringUtils.isBlank(className))
        {
            return null;
        }
        
        return class2TableTrueCase(className);
    }
    
    @SuppressWarnings("rawtypes")
	public static String getColumnType(String entityName, String columnName)
    {
    	String type = null;
    	
    	if (StringUtils.isBlank(entityName) || StringUtils.isBlank(columnName))
    	{
    		Log.log.error("Entity and Column name are mandatory");
    		return type;
    	}
    	
    	XDoc doc = getMappingDoc();
    	Element columnProperty = (Element) doc.getNode("/hibernate-mapping/class[@entity-name='" + entityName + "']/property[@name='" + columnName + "']");
    	if (columnProperty != null)
    	{
    		List attributes = columnProperty.attributes();
    		if (attributes != null)
    		{
    			for (Object obj : attributes)
    			{
    				DefaultAttribute attrib = (DefaultAttribute) obj;
    				if (attrib.getName().equals("type"))
    				{
    					type = attrib.getValue();
    					break;
    				}
    			}
    		}
    	}
    	
    	return type;
    }
}
