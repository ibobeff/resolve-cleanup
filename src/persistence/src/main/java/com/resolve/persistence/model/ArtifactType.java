package com.resolve.persistence.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ArtifactTypeVO;
import com.resolve.services.hibernate.vo.CEFDictionaryItemVO;
import com.resolve.services.hibernate.vo.CustomDictionaryItemVO;

@Entity
@Table(name = "artifact_type")
public class ArtifactType extends BaseModel<ArtifactTypeVO> {

	private static final long serialVersionUID = -5758738468839569715L;

	private String UName;
	private Set<ArtifactConfiguration> artifactConfigurations;
	private Set<CEFDictionaryItem> cefItems;
	private Set<CustomDictionaryItem> customItems;
	private Boolean UStandard;

	@Column(name = "u_type_name", unique = true, nullable = false)
	public String getUName() {
		return UName;
	}

	public void setUName(String uName) {
		UName = uName;
	}

	@OneToMany(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "u_type", foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
	public Set<ArtifactConfiguration> getArtifactConfigurations() {
		return artifactConfigurations;
	}

	public void setArtifactConfigurations(Set<ArtifactConfiguration> artifactConfigurations) {
		this.artifactConfigurations = artifactConfigurations;
	}

	@OneToMany(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "artifact_type_sys_id", foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public Set<CEFDictionaryItem> getCefItems() {
		return cefItems;
	}

	public void setCefItems(Set<CEFDictionaryItem> cefItems) {
		this.cefItems = cefItems;
	}

    @OneToMany(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "artifact_type_sys_id", foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
	public Set<CustomDictionaryItem> getCustomItems() {
		return customItems;
	}

	public void setCustomItems(Set<CustomDictionaryItem> customItems) {
		this.customItems = customItems;
	}

	@Column(name = "u_standard")
	public Boolean getUStandard() {
		return UStandard;
	}

	public void setUStandard(Boolean uStandard) {
		UStandard = uStandard;
	}

	@Override
	public ArtifactTypeVO doGetVO() {
		ArtifactTypeVO vo = new ArtifactTypeVO();
        super.doGetBaseVO(vo);
        
        vo.setUName(UName);
        vo.setUStandard(UStandard);

        if (CollectionUtils.isNotEmpty(cefItems)) {
        	Set<CEFDictionaryItemVO> cefVOs = new HashSet<>();
        	
        	for (CEFDictionaryItem item : cefItems) {
        		cefVOs.add(item.doGetVO());
        	}
        	
        	vo.setCefItems(cefVOs);
        }
        
        if (CollectionUtils.isNotEmpty(customItems)) {
        	Set<CustomDictionaryItemVO> customVOs = new HashSet<>();
        	
        	for (CustomDictionaryItem item : customItems) {
        		customVOs.add(item.doGetVO());
        	}
        	
        	vo.setCustomItems(customVOs);
        }
        
        return vo;
	}
}
