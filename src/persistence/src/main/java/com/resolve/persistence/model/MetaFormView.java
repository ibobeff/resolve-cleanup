/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;

import com.resolve.services.hibernate.vo.MetaFormTabVO;
import com.resolve.services.hibernate.vo.MetaFormViewVO;
import com.resolve.services.hibernate.vo.MetaSourceVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;


@Entity
@Table(name = "meta_form_view", indexes = {
                @Index(columnList = "u_meta_access_rights_sys_id", name = "mfv_u_meta_access_rights_idx"),
                @Index(columnList = "u_meta_control_sys_id", name = "mfv_u_meta_control_idx"),
                @Index(columnList = "u_view_name", name = "mfv_u_view_name_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaFormView extends BaseModel<MetaFormViewVO>
{
    private static final long serialVersionUID = -4279692247495108286L;

    public final static String RESOURCE_TYPE = "meta_form_view";

    private String UFormName;
    private String UViewName;
    private String UDisplayName;
    private String UWikiName;
    private String UTableName;
    private String UTableDisplayName;
    //    private String UExecutionSequence;//sequence of operations - mapped to Operation.java
//    private String USystemScript;//only 1 script per view can be executed
//    private String URunbookName;//name of the runbook to be executed 
    private Boolean UIsHeaderVisible;
    private Boolean UIsViewCollapsible;
    private Boolean UIsWizard;
    
    private String UWindowTitle;
    
    // object referenced by
    private MetaAccessRights metaAccessRights;
    private Collection<MetaFormTab> metaFormTabs;//this is DEPRECATED and used for the older version only - HAVE TO CREATE A TOOL TO CONVERT THIS FROM OLDER TO NEWER USING the MetaxFormViewPanel
    private Collection<MetaSource> metaSources;//all the sources/feilds that are for this specific view
    private MetaControl metaControl;
//    private MetaFormViewProperties metaFormViewProperties;

    private Collection<MetaxFormViewPanel> metaxFormViewPanels;
    private String formJsonTree;

    public MetaFormView()
    {
    }
    
    public MetaFormView(MetaFormViewVO vo)
    {
        applyVOToModel(vo);
    }
    
    @Column(name = "u_form_name", length = 100, nullable = false, unique = true)
    public String getUFormName()
    {
        return UFormName;
    }
    public void setUFormName(String uFormName)
    {
        UFormName = uFormName;
    }


    
    /**
     * @return the UViewName
     */
    @Column(name = "u_view_name", length = 100)
    public String getUViewName()
    {
        return UViewName;
    }
    /**
     * @param uTabName the uTabName to set
     */
    public void setUViewName(String uViewName)
    {
        UViewName = uViewName;
    }
    

    /**
     * @return the uWikiName
     */
    @Column(name = "u_wiki_name", length = 500)
    public String getUWikiName()
    {
        return UWikiName;
    }
    public void setUWikiName(String uWikiName)
    {
        UWikiName = uWikiName;
    }
    
    /**
     * @return the uTableName
     */
    @Column(name = "u_table_name", length = 100)
    public String getUTableName()
    {
        return UTableName;
    }

    /**
     * @param uTableName the uTableName to set
     */
    public void setUTableName(String uTableName)
    {
        UTableName = uTableName;
    }
    
    /**
     * @return the uTableDisplayName
     */
    @Column(name = "u_table_display_name", length = 100)
    public String getUTableDisplayName()
    {
        return UTableDisplayName;
    }

    /**
     * @param uTableDisplayName the uTableDisplayName to set
     */
    public void setUTableDisplayName(String uTableDisplayName)
    {
        UTableDisplayName = uTableDisplayName;
    }
    

    /**
     * @return the uDisplayName
     */
    @Column(name = "u_display_name", length = 500)
    public String getUDisplayName()
    {
        return UDisplayName;
    }

    /**
     * @param uDisplayName the uDisplayName to set
     */
    public void setUDisplayName(String uDisplayName)
    {
        UDisplayName = uDisplayName;
    }
    
    
    @Column(name = "u_window_title", length = 100)
    public String getUWindowTitle()
    {
        return UWindowTitle;
    }

    public void setUWindowTitle(String uWindowTitle)
    {
        UWindowTitle = uWindowTitle;
    }

    /**
     * @return the uIsHeaderVisible
     */
    @Column(name = "u_is_header_visible", length = 1)
    @Type(type = "yes_no")
    public Boolean getUIsHeaderVisible()
    {
        return UIsHeaderVisible;
    }
    public Boolean ugetUIsHeaderVisible()
    {
        if(this.UIsHeaderVisible != null)
        {
            return UIsHeaderVisible;    
        }
        else
        {
            return false;
        }
    }
    public void setUIsHeaderVisible(Boolean uIsHeaderVisible)
    {
        UIsHeaderVisible = uIsHeaderVisible;
    }

    /**
     * @return the uIsViewCollapsible
     */
    @Column(name = "u_is_view_collapsible", length = 1)
    @Type(type = "yes_no")
    public Boolean getUIsViewCollapsible()
    {
        return UIsViewCollapsible;
    }
    public Boolean ugetUIsViewCollapsible()
    {
        if(this.UIsViewCollapsible != null)
        {
            return UIsViewCollapsible;    
        }
        else
        {
            return false;
        }
    }
    public void setUIsViewCollapsible(Boolean uIsViewCollapsible)
    {
        UIsViewCollapsible = uIsViewCollapsible;
    }
    
    @Column(name = "u_is_wizard", length = 1)
    @Type(type = "yes_no")
    public Boolean getUIsWizard()
    {
        return UIsWizard;
    }
    public Boolean ugetUIsWizard()
    {
        if(this.UIsWizard != null)
        {
            return UIsWizard;    
        }
        else
        {
            return false;
        }
    }
    public void setUIsWizard(Boolean uIsWizard)
    {
        UIsWizard = uIsWizard;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_meta_access_rights_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade({CascadeType.DELETE})
    public MetaAccessRights getMetaAccessRights()
    {
        return metaAccessRights;
    }

    public void setMetaAccessRights(MetaAccessRights metaAccessRights)
    {
        this.metaAccessRights = metaAccessRights;
    }

    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "metaFormView")
    @LazyCollection(LazyCollectionOption.FALSE)
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<MetaFormTab> getMetaFormTabs()
    {
        return this.metaFormTabs;
    }

    public void setMetaFormTabs(Collection<MetaFormTab> metaFormTabs)
    {
        this.metaFormTabs = metaFormTabs;
    }
    
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "metaFormView")
    @LazyCollection(LazyCollectionOption.FALSE)
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<MetaSource> getMetaSources()
    {
        return this.metaSources;
    }

    public void setMetaSources(Collection<MetaSource> metaSources)
    {
        this.metaSources = metaSources;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_meta_control_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaControl getMetaControl()
    {
        return metaControl;
    }

    public void setMetaControl(MetaControl metaControl)
    {
        this.metaControl = metaControl;
    }
    
//    @OneToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "u_meta_view_prop_sys_id")
//    @Index(name = "mfv_prop_sys_id_idx")
//    @NotFound(action = NotFoundAction.IGNORE)
//    @Cascade({CascadeType.DELETE})
//    public MetaFormViewProperties getMetaFormViewProperties()
//    {
//        return metaFormViewProperties;
//    }
//    public void setMetaFormViewProperties(MetaFormViewProperties metaFormViewProperties)
//    {
//        this.metaFormViewProperties = metaFormViewProperties;
//    }

    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "metaFormView")
    @LazyCollection(LazyCollectionOption.FALSE)
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<MetaxFormViewPanel> getMetaxFormViewPanels()
    {
        return metaxFormViewPanels;
    }

    public void setMetaxFormViewPanels(Collection<MetaxFormViewPanel> metaxFormViewPanels)
    {
        this.metaxFormViewPanels = metaxFormViewPanels;
    }

    @Lob
    @Column(name = "form_json", length=Integer.MAX_VALUE)
    public String getFormJsonTree()
    {
        return formJsonTree;
    }
    public void setFormJsonTree(String formJsonTree)
    {
        this.formJsonTree = formJsonTree;
    }

    @Override
    public MetaFormViewVO doGetVO()
    {
        MetaFormViewVO vo = new MetaFormViewVO();
        super.doGetBaseVO(vo);
        
        vo.setUDisplayName(getUDisplayName());
        vo.setUFormName(getUFormName());
        vo.setUIsHeaderVisible(getUIsHeaderVisible());
        vo.setUIsViewCollapsible(getUIsViewCollapsible());
        vo.setUIsWizard(getUIsWizard());
        vo.setUTableDisplayName(getUTableDisplayName());
        vo.setUTableName(getUTableName());
        vo.setUViewName(getUViewName());
        vo.setUWikiName(getUWikiName());
        vo.setUWindowTitle(getUWindowTitle());
        
        //references
        vo.setMetaAccessRights(Hibernate.isInitialized(this.metaAccessRights) && getMetaAccessRights() != null ? getMetaAccessRights().doGetVO() : null) ;
        vo.setMetaControl(Hibernate.isInitialized(this.metaControl) && getMetaControl() != null ? getMetaControl().doGetVO() : null) ;
//        vo.setMetaFormViewProperties(Hibernate.isInitialized(this.metaFormViewProperties) && getMetaFormViewProperties() != null ? getMetaFormViewProperties().doGetVO() : null) ;
        
        Collection<MetaFormTab> metaFormTabs = Hibernate.isInitialized(this.metaFormTabs) ? getMetaFormTabs() : null;        
        if(metaFormTabs != null && metaFormTabs.size() > 0)
        {
            Collection<MetaFormTabVO> vos = new ArrayList<MetaFormTabVO>();
            for(MetaFormTab model : metaFormTabs)
            {
                vos.add(model.doGetVO());
            }
            vo.setMetaFormTabs(vos);
        }
        
        Collection<MetaSource> metaSources = Hibernate.isInitialized(this.metaSources) ? getMetaSources() : null;        
        if(metaSources != null && metaSources.size() > 0)
        {
            Collection<MetaSourceVO> vos = new ArrayList<MetaSourceVO>();
            for(MetaSource model : metaSources)
            {
                vos.add(model.doGetVO());
            }
            vo.setMetaSources(vos);
        }
        vo.setFormJsonTree(this.getFormJsonTree());
        
        return vo;
    }

    @Override
    public void applyVOToModel(MetaFormViewVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo); 
            
            this.setUDisplayName(StringUtils.isNotBlank(vo.getUDisplayName()) && vo.getUDisplayName().equals(VO.STRING_DEFAULT) ? getUDisplayName() : vo.getUDisplayName());
            this.setUFormName(StringUtils.isNotBlank(vo.getUFormName()) && vo.getUFormName().equals(VO.STRING_DEFAULT) ? getUFormName() : vo.getUFormName());
            this.setUIsHeaderVisible(vo.getUIsHeaderVisible() != null ? vo.getUIsHeaderVisible() : getUIsHeaderVisible());
            this.setUIsViewCollapsible(vo.getUIsViewCollapsible()!= null ? vo.getUIsViewCollapsible() : getUIsViewCollapsible());
            this.setUIsWizard(vo.getUIsWizard()!= null ? vo.getUIsWizard() : getUIsWizard());
            this.setUTableDisplayName(StringUtils.isNotBlank(vo.getUTableDisplayName()) && vo.getUTableDisplayName().equals(VO.STRING_DEFAULT) ? getUTableDisplayName() : vo.getUTableDisplayName());
            this.setUTableName(StringUtils.isNotBlank(vo.getUTableName()) && vo.getUTableName().equals(VO.STRING_DEFAULT) ? getUTableName() : vo.getUTableName());
            this.setUViewName(StringUtils.isNotBlank(vo.getUViewName()) && vo.getUViewName().equals(VO.STRING_DEFAULT) ? getUViewName() : vo.getUViewName());
            this.setUWikiName(StringUtils.isNotBlank(vo.getUWikiName()) && vo.getUWikiName().equals(VO.STRING_DEFAULT) ? getUWikiName() : vo.getUWikiName());
            this.setUWindowTitle(StringUtils.isNotBlank(vo.getUWindowTitle()) && vo.getUWindowTitle().equals(VO.STRING_DEFAULT) ? getUWindowTitle() : vo.getUWindowTitle());

            //references
            this.setMetaAccessRights(vo.getMetaAccessRights() != null ? new MetaAccessRights(vo.getMetaAccessRights()) : getMetaAccessRights()) ;
            this.setMetaControl(vo.getMetaControl() != null ? new MetaControl(vo.getMetaControl()) : getMetaControl());
//            this.setMetaFormViewProperties(vo.getMetaFormViewProperties() != null ? new MetaFormViewProperties(vo.getMetaFormViewProperties()) : getMetaFormViewProperties());
            
            Collection<MetaFormTabVO> metaFormTabsVOs = vo.getMetaFormTabs();
            if(metaFormTabsVOs != null && metaFormTabsVOs.size() > 0)
            {
                Collection<MetaFormTab> models = new ArrayList<MetaFormTab>();
                for(MetaFormTabVO refVO : metaFormTabsVOs)
                {
                    models.add(new MetaFormTab(refVO));
                }
                this.setMetaFormTabs(models);
            }
            
            Collection<MetaSourceVO> metaSourcesVOs =  vo.getMetaSources();
            if(metaSourcesVOs != null && metaSourcesVOs.size() > 0)
            {
                Collection<MetaSource> models = new ArrayList<MetaSource>();
                for(MetaSourceVO refVO : metaSourcesVOs)
                {
                    models.add(new MetaSource(refVO));
                }
                this.setMetaSources(models);
            }
            this.setFormJsonTree(StringUtils.isNotBlank(vo.getFormJsonTree()) && vo.getFormJsonTree().equals(VO.STRING_DEFAULT) ? getFormJsonTree() : vo.getFormJsonTree());
        }
        
    }

}//MetaFormView
