package com.resolve.persistence.model;

import javax.persistence.Column;
//import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

//import org.hibernate.annotations.Cache;
//import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

//import com.resolve.persistence.util.ResolveSLADurationConverter;
import com.resolve.services.hibernate.vo.SIRActivityMetaDataVO;
import com.resolve.services.interfaces.VO;

import static com.resolve.util.StringUtils.*;

import java.time.Duration;

@Entity
@Table(name = "sir_activity_metadata", 
       uniqueConstraints = {
                       @UniqueConstraint(columnNames = {"sys_org", "u_name"}, name = "samd_u_org_name_uk"),
                       @UniqueConstraint(columnNames = {"sys_org", "u_name", "u_ref_wiki_sys_id", "u_isrequired", 
                                                        "u_sla"},
                                         name = "samd_u_org_name_wiki_req_sla_uk")
       }, 
       indexes = {
                @Index(columnList = "sys_org, u_isrequired", name = "samd_org_required_idx"),
                @Index(columnList = "sys_org, u_legacyname", name = "samd_org_legacyname_idx")
       })
/*
 * Enable Hibernate L2 Cache only if it is distributed 
 * accross all nodes in cluster.
 */
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SIRActivityMetaData extends BaseModel<SIRActivityMetaDataVO> {
    
    public static final Duration DEFAULT_SLA_DURATION = Duration.ZERO;
    public static final Boolean DEFAULT_IS_REQAUIRED = Boolean.TRUE;
    
    private static final long serialVersionUID = 1992146034337324467L;
    
    private String name;                // Name, unique per Org
	private String description; 		// Description
	private Boolean isRequired;         // Activity is Required (true) or Optional (false), default is Required (true)
	private String refWikiSysId;        // Sys id of wiki referenced by activity
	private Boolean isTemplateActivity; // is Template Activity (true) or Custom Activity (false), default is Template Activity (true)
	private String legacyName;          // Legacy Name, SIR 1.0 activity names were not unique globally or per Org
	                                    // Non unique legacy names per Org -> Unique Name per Org
	                                    // <Unique Name per Org> = <Non Unique Name>(<Copy #>)
	                                    // Copy # starts from 1 e.g. MyActivity, MyActivity (1), ......
	                                    // For new activities Legacy Name will be same as Name which is unique per Org
	                                    // SLA (service level agreement) duration (with minute granularity) to complete activity
	private Duration sLA;               // SLA duration
	                                    // ZERO Duration represents No SLA for activity
	                                    // Default SLA for activity is NO SLA
	private String altActivityId;       // Alternate Activity Id
	
	public SIRActivityMetaData() {
	    super();
	    isRequired = DEFAULT_IS_REQAUIRED;
	    isTemplateActivity = Boolean.TRUE;
	    sLA = DEFAULT_SLA_DURATION;
	}
	
	public SIRActivityMetaData(SIRActivityMetaDataVO vo) {
        applyVOToModel(vo);
    }
	
	@Column(name = "u_name", length = 200, nullable = false, updatable = false)
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Lob
	@Column(name = "u_description")
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name = "u_isrequired", length = 1, nullable = false, updatable = false)
    @Type(type = "yes_no")
	@ColumnDefault(value = "'Y'")
	public Boolean getIsRequired() {
	    return isRequired;
	}
	
	public Boolean ugetIsRequired() {
	    if (getIsRequired() != null) {
            return getIsRequired();
        } else {
            return DEFAULT_IS_REQAUIRED;
        }
	}
	
	public void setIsRequired(Boolean isRequired) {
	    this.isRequired = isRequired;
	}
	
	@Column(name = "u_ref_wiki_sys_id", length = 32, nullable = false, updatable = false)
	public String getRefWikiSysId() {
	    return refWikiSysId;
	}
	
	public void setRefWikiSysId(String refWikiSysId) {
	    this.refWikiSysId = refWikiSysId;
	}
	
	@Column(name = "u_istemplateactivity", length = 1, nullable = false, updatable = false)
    @Type(type = "yes_no")
    @ColumnDefault(value = "'Y'")
    public Boolean getIsTemplateActivity() {
        return isTemplateActivity;
    }
    
    public Boolean ugetIsTemplateActivity() {
        if (getIsTemplateActivity() != null) {
            return getIsTemplateActivity();
        } else {
            return Boolean.TRUE;
        }
    }
    
    public void setIsTemplateActivity(Boolean isTemplateActivity) {
        this.isTemplateActivity = isTemplateActivity;
    }
    
    @Column(name = "u_legacyname", length = 200, nullable = false, updatable = false)
    public String getLegacyName() {
        return legacyName;
    }
    
    public void setLegacyName(String legacyName) {
        this.legacyName = legacyName;
    }
    
    @Column(name = "u_sla", nullable = false, updatable = false)
    @ColumnDefault(value = "0")
    /*
     * SLA granularity is upto minutes only.
     * SLA is persisted in DB with default java.time.Duration
     * granularity which is nano seconds. 
     * 
     * This gives max SLA duration of 292 years. 
     * 
     * If max SLA duration requirements are insufficient
     * then enable ResolveSLADurationConverter on
     * sLA field in SIRActivityMetaData model.
     * 
     * This will persist the SLA duration in minutes
     * extending max SLA duration to lets just say
     * astronomical units.   
     */
//    @Convert(converter = ResolveSLADurationConverter.class)
    public Duration getSLA() {
//        log.trace("getSLA() returning " + sLA);
        return sLA;
    }
    
    public void setSLA(Duration sLA) {
        if (sLA != null && sLA.toNanos() >= 0) {
            this.sLA = Duration.ofNanos(sLA.toNanos());
        } else {
            sLA = DEFAULT_SLA_DURATION;
        }
//        log.trace("setSLA(" + sLA + ") setting " + this.sLA);
    }
    
    @Column(name = "u_alt_activity_id", length = 32, updatable = false)
    public String getAltActivityId() {
        return altActivityId;
    }
    
    public void setAltActivityId(String altActivityId) {
        this.altActivityId = altActivityId;
    }
    
    @Override
	public SIRActivityMetaDataVO doGetVO() {
		SIRActivityMetaDataVO vo = new SIRActivityMetaDataVO();
		
		super.doGetBaseVO(vo);
				
		vo.setName(getName());
		vo.setDescription(getDescription());
		vo.setIsRequired(getIsRequired());
		vo.setRefWikiSysId(getRefWikiSysId());
		vo.setIsTemplateActivity(getIsTemplateActivity());
		vo.setLegacyName(getLegacyName());
		vo.setSLA(getSLA());
		vo.setAltActivityId(isNotBlank(getAltActivityId()) ? getAltActivityId() : getSys_id());
		
		return vo;
	}
	
	@Override
	public void applyVOToModel(SIRActivityMetaDataVO vo)
	{
		if (vo != null)
		{
			super.applyVOToModel(vo);
			
			setName(isNotBlank(vo.getName()) && vo.getName().equals(VO.STRING_DEFAULT) ? getName() : vo.getName());
			setDescription(isNotBlank(vo.getDescription()) && vo.getDescription().equals(VO.STRING_DEFAULT) ? 
			               getDescription() : vo.getDescription());
			setIsRequired(vo.getIsRequired() != null ? vo.getIsRequired() : ugetIsRequired());
			setRefWikiSysId(isNotBlank(vo.getRefWikiSysId()) && vo.getRefWikiSysId().equals(VO.STRING_DEFAULT) ? 
			                getRefWikiSysId() : vo.getRefWikiSysId());
			setIsTemplateActivity(vo.getIsTemplateActivity() != null ? vo.getIsRequired() : ugetIsRequired());
			setLegacyName(isNotBlank(vo.getLegacyName()) && vo.getLegacyName().equals(VO.STRING_DEFAULT) ? 
			              getLegacyName() : vo.getLegacyName());
			setSLA(vo.getSLA());
			setAltActivityId(isNotBlank(vo.getAltActivityId()) && vo.getAltActivityId().equals(VO.STRING_DEFAULT) ? 
			                 getAltActivityId() : vo.getAltActivityId());
		}
	}
	
	@Override
    public String toString() {
        return "SIR Activity Meta Data [" +
               (isNotBlank(getSys_id()) ? "Sys Id: " + getSys_id() + ", " : "") + 
               "Sys Org: " + (isNotBlank(getSysOrg()) ? getSysOrg() : "") + 
               ", Name: " + name + ", Description: " + 
               (isNotBlank(getDescription()) ? (getDescription().length() > 50 ? getDescription().substring(0, 46) + "..." : getDescription()) : "" ) +
               ", Is Required: " + ugetIsRequired() + ", Referenced Wiki Sys Id: " + getRefWikiSysId() + 
               ", Is Template Activity: " + (getIsTemplateActivity() != null ? getIsTemplateActivity() : Boolean.TRUE) + 
               ", Legacy Name: " + (isNotBlank(getLegacyName()) ? getLegacyName() : "") +
               ", SLA: " + getSLA() +
               ", Alternate Activity Id: " + (isNotBlank(getAltActivityId()) ? getAltActivityId() : "") + 
               "]";
    }
}
