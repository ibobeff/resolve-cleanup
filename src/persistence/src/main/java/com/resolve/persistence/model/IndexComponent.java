/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

/**
 * A generic Interface for all the components that will be indexed
 * 
 * @author jeet.marwah
 *
 */
public interface IndexComponent
{
	public String ugetIndexContent();
}
