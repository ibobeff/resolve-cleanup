/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import org.dom4j.Element;

public class IDDefinition extends ColumnDefinition
{
    IDDefinition()
    {
        name = "sys_id";
        column = "sys_id";
    }
    
    @Override
    public Element addColumn(Element entity)
    {
        Element idElem = entity.addElement("id");
        idElem.addAttribute("name", name);
        idElem.addAttribute("type", "string");
        idElem.addAttribute("column", column);
        idElem.addAttribute("length", "32");

        Element geneElem = idElem.addElement("generator"); 
        geneElem.addAttribute("class", "uuid.hex");
        
        return idElem;
    }

}
