/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import java.util.HashMap;
import java.util.Map;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import com.resolve.esb.ESB;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * Wrapper/Util for the EhCache
 * 
 * @author jeet.marwah
 *
 */
public class CacheUtil
{
    private static volatile CacheManager cacheManager;
    
    //general Resolve cache
    private static String LOCAL_CACHE = "RESOLVE_OBJECTS";
    
    private static String CACHE_OBJ_NAME = "RESOLVE_CACHE_OBJ_NAME";
    public static String CACHE_NAME = "CACHE_NAME";

    public static void voidCacheManager()
    {
        cacheManager = null;
    }

    public static void initLocalCache()
    {
        //NOTE: DO NOT CHECK FOR NULL HERE. THERE ARE PLACES WHERE THERE IS A NEED TO GET A NEW INSTANCE OF CACHE MANAGER
    	cacheManager = CacheManager.newInstance();
        //      testCache();
    }

    private synchronized static void checkCache()
    {
        if (cacheManager==null || (cacheManager.getStatus()!=net.sf.ehcache.Status.STATUS_ALIVE))
        {
            initLocalCache();
        }
    }
    
    // put an object into cache
    public static void putCacheObj(String name, Object obj)
    {
        checkCache();
        if (cacheManager==null || (cacheManager.getStatus()!=net.sf.ehcache.Status.STATUS_ALIVE))
            return;

        Element e = new Element(name, obj);
        cacheManager.getCache(LOCAL_CACHE).put(e);
    }

    // retrieve an object from cache
    public static Object getCacheObj(String name)
    {
        checkCache(); 
        if (cacheManager==null || (cacheManager.getStatus()!=net.sf.ehcache.Status.STATUS_ALIVE))
            return null;
        
        Object result = null;
        Element e = cacheManager.getCache(LOCAL_CACHE).get(name);
        if (e != null)
        {
            result = e.getObjectValue();
        }
        return result;
    }

    // check if an object is in cache or not
    public static boolean isKeyInCache(String name)
    {
        checkCache();
        if (cacheManager==null || (cacheManager.getStatus()!=net.sf.ehcache.Status.STATUS_ALIVE))
            return false;

        return cacheManager.getCache(LOCAL_CACHE).isKeyInCache(name);
    }

    //remove name cache object from local only
    public static void removeCacheObj(String name)
    {
        checkCache();

        //remove the cache locally
        removeCacheObjLocally(name);
        
        //send msg to other rsviews/rscontrol to clear the cache
        removeCacheObjFromCluster(name);
    }
    
    ///////////////// GENERIC APIS ////////////////////
    
    /**
     * Create a user configured cache , eg. MenuCache
     * 
     * @param cache
     */
    public static void createCache(Cache cache)
    {
        checkCache();
        if(cache != null)
        {
            String name = cache.getName();
            if(cacheManager.cacheExists(name))
            {
                //remove the existing cache and use the new one
                cacheManager.removeCache(name);
            }
            
            //add the new cache
            cacheManager.addCache(cache);
        }
    }
    
    public static void invalidateCache(String cacheName)
    {
        checkCache();
        if(StringUtils.isNotBlank(cacheName))
        {
            if(isKeyInCache(cacheName)) // cacheName is cached in Resolve.resolve_shared_object table in DB
            {
                // Please note synchornization to other nodes in cluster is currently responsibility of caller 
                removeCacheObjLocally(cacheName);
            }
            else if (cacheManager.cacheExists(cacheName)) // cacheName is in local cache
            {
                Cache cache = cacheManager.getCache(cacheName);
                if(cache != null)
                {
                    cache.removeAll();
                    
                    //send msgs to other RSVIEWS to remove this cache
                    removeCacheObjFromCluster(cacheName, null);
                }
            }
        }
    }
    
    public static boolean doesCacheExist(String cacheName)
    {
        checkCache();
        boolean exist = false;
        
        if(StringUtils.isNotBlank(cacheName))
        {
            if(cacheManager.getCache(cacheName) != null)
            {
                exist = true;
            }
        }
        
        return exist;
    }
    
    /**
     * check if a key is available in a particular cache
     * 
     * @param cacheName - eg, MenuCache name
     * @param keyName - eg. username in case of MenuCache
     * @return
     */
    public static boolean isKeyInCache(String cacheName, String keyName)
    {
        checkCache();
        boolean keyExist = false;
        
        if(StringUtils.isNotEmpty(cacheName) && StringUtils.isNotEmpty(keyName))
        {
            if(isKeyInCache(cacheName)) // cacheName is cached in Resolve.resolve_shared_object table in DB
            {
                Element e = cacheManager.getCache(LOCAL_CACHE).get(cacheName);
                
                if (e != null)
                {
                    Object objKey = e.getObjectKey();
                    
                    keyExist = objKey instanceof String && ((String)objKey).equals(keyName);
                }
            }
            else if (cacheManager.cacheExists(cacheName)) // cacheName is in local cache)
            {
                Cache cache = cacheManager.getCache(cacheName);
                if(cache != null)
                {
                    keyExist = cache.isKeyInCache(keyName);
                }
            }
        }
                
        return keyExist;
    }
    
    /**
     * get a cache obj from a Cache using a key
     * 
     * @param cacheName - eg, MenuCache name
     * @param keyName - eg. username in case of Menu cache
     * @return
     */
    public static Object getCacheObj(String cacheName, String keyName)
    {
        checkCache();
        if (cacheManager==null || (cacheManager.getStatus() != net.sf.ehcache.Status.STATUS_ALIVE))
            return null;
        
        Object result = null;
        
        if(StringUtils.isNotEmpty(cacheName) && StringUtils.isNotEmpty(keyName))
        {
            Cache cache = cacheManager.getCache(cacheName);
            if(cache != null)
            {
                Element e = cacheManager.getCache(cacheName).get(keyName);
                if (e != null)
                {
                    result = e.getObjectValue();
                }
            }
        }
        
        return result;
    }
    
    /**
     * put an object in a particular cache
     * 
     * @param cacheName
     * @param keyName
     * @param value
     */
    public static void putCacheObj(String cacheName, String keyName, Object value)
    {
        checkCache();
        if (cacheManager==null || (cacheManager.getStatus()!=net.sf.ehcache.Status.STATUS_ALIVE))
            return;

        if(StringUtils.isNotEmpty(cacheName) && StringUtils.isNotEmpty(keyName) && value != null)
        {
            Cache cache = cacheManager.getCache(cacheName);
            if(cache != null)
            {
                Element e = new Element(keyName, value);
                cache.put(e);
            }
        }
    }

    /**
     * remove cache obj for a cache using a key
     * 
     * @param cacheName
     * @param keyName
     */
    public static void removeCacheObj(String cacheName, String keyName)
    {
        checkCache();

        //remove the local cache
        removeCacheObjLocally(cacheName,keyName);
        
        //send msg to other rsviews/rscontrol to clear the cache
        removeCacheObjFromCluster(cacheName, keyName);
    }

    
    /////////////////////////////////////////////////////
    
    /**
     * called from ESB
     * 
     * @param params
     */
    public void clearLocalObjectsCache(Map<String, String> params)
    {
        if (HibernateUtil.isHibernateInitializing2())
        {
            return;
        }
        
        Log.log.info("Received message to clear " + 
                     (params.containsKey(CACHE_OBJ_NAME) ? " cached object " + params.get(CACHE_OBJ_NAME) + " from " : "") + 
                     "local " + params.get(CACHE_NAME) + " cache with sender id " + params.get(Constants.MSG_SOURCE_ID)); 
        
        if(params != null && params.containsKey(Constants.MSG_SOURCE_ID) && params.get(Constants.MSG_SOURCE_ID).equalsIgnoreCase(MainBase.main.configId.getGuid()))
        {
            Log.log.info("***** Ignoring received self generated message to clear " + 
                         (params.containsKey(CACHE_OBJ_NAME) ? " cached object " + params.get(CACHE_OBJ_NAME) + " from " : "") + 
                         "local " + params.get(CACHE_NAME) + " *****");
            return;
        }
        
        checkCache();
        String keyName = (String)params.get(CACHE_OBJ_NAME);
        String cacheName = (String)params.get(CACHE_NAME);

        if (keyName == null && cacheName == null)
        {
            throw new RuntimeException("Received local cache clear message has null key name and cache name");
        }
        
        if(StringUtils.isNotEmpty(keyName))
        {
            removeCacheObjLocally(cacheName, keyName);
        }
        else
        {
            removeCacheObjLocally(cacheName);
        }
    }
    

    //private apis
    /* remove all named cache objects from all cluster members
     * USE WITH EXTREME CAUTION - POTENTIAL FOR INFINITE PING-PONG
     * IF OTHER MESSAGE PROCESSOR METHOD CALLS SAME OPERATION
     */
    
    private static void removeCacheObjFromCluster(String name)
    {
        Map<String, String> msg = new HashMap<String, String>();
        msg.put(CACHE_OBJ_NAME, name);
        removeCacheObjFromCluster(msg);
    }
    
    /* remove all named keys from named cache objects from all cluster members
     * USE WITH EXTREME CAUTION - POTENTIAL FOR INFINITE PING-PONG
     * IF OTHER MESSAGE PROCESSOR METHOD CALLS SAME OPERATION
     */
    private static void removeCacheObjFromCluster(String cacheName, String keyName)
    {
        Map<String, String> msg = new HashMap<String, String>();
        msg.put(CACHE_NAME, cacheName);
        
        if (StringUtils.isNotBlank(keyName))
        {
            msg.put(CACHE_OBJ_NAME, keyName);
        }

        removeCacheObjFromCluster(msg);
    }

    private static void removeCacheObjFromCluster(Map<String, String> msg)
    {
        ESB esb = HibernateUtil.getEsb();
        if (esb == null || msg == null)
        {
            return;
        }

        if (HibernateUtil.isHibernateInitializing2())
        {
            return;
        }
        
        msg.put(Constants.MSG_SOURCE_ID, MainBase.main.configId.getGuid());
        
        Log.log.info("Sending message to other rsviews and rscontrollers in cluster to clear " + 
                     (msg.containsKey(CACHE_OBJ_NAME) ? " cached object " + msg.get(CACHE_OBJ_NAME) + " from " : "") + 
                     "local " + msg.get(CACHE_NAME) + " cache with sender id " + msg.get(Constants.MSG_SOURCE_ID) + "..."); 

        esb.sendInternalMessage(Constants.ESB_NAME_RSVIEWS, "com.resolve.persistence.util.CacheUtil.clearLocalObjectsCache", msg);
        esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROLS, "com.resolve.persistence.util.CacheUtil.clearLocalObjectsCache", msg);
    }
    
    
    private static void removeCacheObjLocally(String cacheName, String keyName)
    {
        if (cacheManager==null || (cacheManager.getStatus()!=net.sf.ehcache.Status.STATUS_ALIVE))
            return;

        if (HibernateUtil.isHibernateInitializing2())
        {
            return;
        }

        if(StringUtils.isNotEmpty(cacheName) && StringUtils.isNotEmpty(keyName))
        {
            Cache cache = cacheManager.getCache(cacheName);
            if(cache != null)
            {
                cache.remove(keyName);
            }
        }
        else if(StringUtils.isNotEmpty(cacheName)) //if there is only cacheName, and no key, that means invalidate the whole cache
        {
            Cache cache = cacheManager.getCache(cacheName);
            if(cache != null)
            {
                cache.removeAll();
            } 
        }
    }
    
    private static void removeCacheObjLocally(String cacheName)
    {
        if (cacheManager==null || (cacheManager.getStatus()!=net.sf.ehcache.Status.STATUS_ALIVE))
            return;

        if (HibernateUtil.isHibernateInitializing2())
        {
            return;
        }

        if (isKeyInCache(cacheName)) // cacheName is cached in Resolve.resolve_shared_object table in DB
        {
            boolean removed = cacheManager.getCache(LOCAL_CACHE).remove(cacheName);
        
            if (!removed)
            {
                Log.log.warn("Failed to remove cache " + cacheName + " from " + LOCAL_CACHE);
            }
        } 
        else if (cacheManager.cacheExists(cacheName)) // cacheName is in local cache
        {
            Cache cache = cacheManager.getCache(cacheName);
            if(cache != null)
            {
                cache.removeAll();
            }
            else
            {
                Log.log.warn("Failed to find local cache " + cacheName);
            }
        }
    }

    public static void testCache()
    {
        String[] arr1 = { "aa", "bb", "cc" };
        putCacheObj("obj1", arr1);
        System.out.println("arr1 = " + arr1.toString());
        removeCacheObj("obj1");
        System.out.println("obj1 in cache : " + isKeyInCache("obj1"));

        String[] arr2 = { "dd", "ee", "ff" };
        putCacheObj("obj2", arr2);
        removeCacheObjFromCluster("obj2");
    }
    
}
