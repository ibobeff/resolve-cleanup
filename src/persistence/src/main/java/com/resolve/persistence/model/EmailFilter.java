/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

//Generated Dec 11, 2009 11:15:20 AM by Hibernate Tools 3.2.4.GA

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.services.hibernate.vo.EmailFilterVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "emailserver_filter",  uniqueConstraints = {@UniqueConstraint(columnNames = {"u_name", "u_queue"}, name = "emailf_u_name_u_queue_uk")})
// @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EmailFilter extends GatewayFilter<EmailFilterVO>
{
    private static final long serialVersionUID = -380580408412474074L;

    private String UQuery;

    public EmailFilter()
    {
    } // EmailFilter

    public EmailFilter(EmailFilterVO vo)
    {
        applyVOToModel(vo);
    } // EmailFilter

    @Column(name = "u_query", length = 4000)
    public String getUQuery()
    {
        return this.UQuery;
    } // getUQuery

    public void setUQuery(String uQuery)
    {
        this.UQuery = uQuery;
    } // setUQuery

    @Override
    public EmailFilterVO doGetVO()
    {
        EmailFilterVO vo = new EmailFilterVO();
        super.doGetBaseVO(vo);

        vo.setUQuery(getUQuery());

        return vo;
    }

    @Override
    public void applyVOToModel(EmailFilterVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);
            this.setUQuery(StringUtils.isNotBlank(vo.getUQuery()) && vo.getUQuery().equals(VO.STRING_DEFAULT) ? getUQuery() : vo.getUQuery());
        }
    }
}

