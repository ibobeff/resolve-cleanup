/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.RBTaskPostParamVO;
import com.resolve.services.interfaces.VO;

@SuppressWarnings("serial")
@Entity
@Table(name = "rb_task_post_param", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_task_id" }, name = "rbtpp_u_name_u_task_id_uk") })
public class RBTaskPostParam extends BaseModel<RBTaskPostParamVO>
{
    private String name;
    private String value;
    
    private RBTask task;
    
    public RBTaskPostParam()
    {
    }

    public RBTaskPostParam(RBTaskPostParamVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_name", length = 40)
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Column(name = "u_value", length = 40)
    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_task_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public RBTask getTask()
    {
        return this.task;
    }

    public void setTask(RBTask task)
    {
        this.task = task;
    }

    @Override
    public RBTaskPostParamVO doGetVO()
    {
        RBTaskPostParamVO vo = new RBTaskPostParamVO();
        super.doGetBaseVO(vo);

        vo.setName(getName());
        vo.setValue(getValue());

        return vo;
    }

    @Override
    public void applyVOToModel(RBTaskPostParamVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setName(vo.getName() != null && vo.getName().equals(VO.STRING_DEFAULT) ? getName() : vo.getName());
            this.setValue(vo.getValue() != null && vo.getValue().equals(VO.STRING_DEFAULT) ? getValue() : vo.getValue());
            
            //references
            this.setTask(vo.getTask() != null ? new RBTask(vo.getTask()) : null);
        }
    }
}
