/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolveAssessVO;
import com.resolve.services.interfaces.CdataProperty;
import com.resolve.services.interfaces.IndexComponent;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

/**
 * ResolveAssess generated by hbm2java
 */
@Entity
@Table(name = "resolve_assess")
public class ResolveAssess extends BaseModel<ResolveAssessVO>  implements IndexComponent, CdataProperty
{
    private static final long serialVersionUID = 8440231853337163224L;

    public final static String RESOURCE_TYPE = "resolveassess";

    private String UName;
    private String UScript;
    private String UDescription;
    private Boolean UParse;
    private Boolean UOnlyCompleted;
    
    /*
     * Expression (RESOLVE_TASK_EXPRESSION) and Output Mapping (RESOLVE_TASK_OUTPUT_MAPPING) 
     * scripts were added as part of Resolve 5.5. These scripts will execute
     * after assessor script.
     */
    private String UExpressionScript;
    private String UOutputMappingScript;
    private String summaryFormat;
    private String detailFormat;
    private String summaryRule;
    private String detailRule;

    private Collection<ResolveActionInvoc> resolveActionInvocs;
    private Collection<ResolveAssessRel> resolveAssessRels;
    
//    private AccessRights accessRights;

    public ResolveAssess()
    {
    }

    public ResolveAssess(ResolveAssessVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_name", length = 300, unique = true, nullable=false)
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }

    @Lob
    @Column(name = "u_script", length = 16777215)
    public String getUScript()
    {
        return this.UScript;
    }

    public void setUScript(String UScript)
    {
        this.UScript = UScript;
    }
    
    @Column(name = "u_description", length = 4000)
    public String getUDescription()
    {
        return this.UDescription;
    }

    public void setUDescription(String UDescription)
    {
        this.UDescription = UDescription;
    }
    
    @Column(name = "u_parse")
    public Boolean getUParse()
    {
        return this.UParse;
    }

    public void setUParse(Boolean UParse)
    {
        this.UParse = UParse;
    }

    @Column(name = "u_only_completed")
    public Boolean getUOnlyCompleted()
    {
        return this.UOnlyCompleted;
    }

    public Boolean ugetUOnlyCompleted()
    {
        if (this.UOnlyCompleted == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UOnlyCompleted;
        }
    } // ugetUOnlyCOmpleted

    public void setUOnlyCompleted(Boolean UOnlyCompleted)
    {
        this.UOnlyCompleted = UOnlyCompleted;
    }

    @Column(name = "u_expression_script", length = 2000)
    public String getUExpressionScript()
	{
		return UExpressionScript;
	}

	public void setUExpressionScript(String uExpressionScript)
	{
		UExpressionScript = uExpressionScript;
	}

	@Column(name = "u_output_mapping_script", length = 2000)
	public String getUOutputMappingScript()
	{
		return UOutputMappingScript;
	}

	public void setUOutputMappingScript(String uOutputMappingScript)
	{
		UOutputMappingScript = uOutputMappingScript;
	}
	
	@Column(name = "u_summary_format", length = 300)
    public String getSummaryFormat()
	{
		return summaryFormat;
	}

	public void setSummaryFormat(String summaryFormat)
	{
		this.summaryFormat = summaryFormat;
	}

	@Column(name = "u_detail_format", length = 300)
	public String getDetailFormat()
	{
		return detailFormat;
	}
	
	public void setDetailFormat(String detailFormat)
	{
		this.detailFormat = detailFormat;
	}
	
	@Column(name = "u_summary_rule", length = 4000)
	public String getSummaryRule()
	{
		return summaryRule;
	}
	public void setSummaryRule(String summaryRule)
	{
		this.summaryRule = summaryRule;
	}

	@Column(name = "u_detail_rule", length = 4000)
	public String getDetailRule()
	{
		return detailRule;
	}
	public void setDetailRule(String detailRule)
	{
		this.detailRule = detailRule;
	}

	// ResolveAssess-ResolveActionInvoc relationship
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "assess")
	@NotFound(action = NotFoundAction.IGNORE)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public Collection<ResolveActionInvoc> getResolveActionInvocs()
    {
        return resolveActionInvocs;
    }

    public void setResolveActionInvocs(Collection<ResolveActionInvoc> resolveActionInvocs)
    {
        this.resolveActionInvocs = resolveActionInvocs;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "assessor")
    @NotFound(action = NotFoundAction.IGNORE)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public Collection<ResolveAssessRel> getResolveAssessRels()
    {
        return resolveAssessRels;
    }

    public void setResolveAssessRels(Collection<ResolveAssessRel> resolveAssessRels)
    {
        this.resolveAssessRels = resolveAssessRels;
    }

    @Override
    public String toString()
    {
        StringBuffer str = new StringBuffer();

        str.append("Assessor Name:").append(UName).append("\n");
        str.append("Assessor Description:").append(UDescription).append("\n");

        return str.toString();
    }
    
    /**
     * method for getting a copy ...similar to clone()
     * 
     * @return
     */
    public ResolveAssess copy()
    {
    	ResolveAssess copy = new ResolveAssess();
    	copy.setSys_id(getSys_id());
    	copy.setSysCreatedBy(getSysCreatedBy());
    	copy.setSysCreatedOn(getSysCreatedOn());
    	copy.setSysUpdatedBy(getSysUpdatedBy());
    	copy.setSysUpdatedOn(getSysUpdatedOn());
    	
    	copy.setUDescription(UDescription);
    	copy.setUName(UName);
    	copy.setUOnlyCompleted(UOnlyCompleted);
    	copy.setUParse(UParse);
    	copy.setUScript(UScript);
    	copy.setUExpressionScript(UExpressionScript);
    	copy.setUOutputMappingScript(UOutputMappingScript);
    	
    	return copy;
    }

    @Override
    public String ugetIndexContent()
    {
        return          UName
                + " " + UScript;
    }

    @Override
    public List<String> ugetCdataProperty()
    {
        List<String> list = new ArrayList<String>();
        list.add("UScript");
        list.add("UExpressionScript");
        list.add("UOutputMappingScript");
        
        return list;
    }//ugetCdataProperty

    @Override
    public ResolveAssessVO doGetVO()
    {
        ResolveAssessVO vo = new ResolveAssessVO();
        super.doGetBaseVO(vo);
        
        vo.setUDescription(getUDescription());
        vo.setUName(getUName());
        vo.setUOnlyCompleted(getUOnlyCompleted());
        vo.setUParse(getUParse());
        vo.setUScript(getUScript());
        vo.setUExpressionScript(getUExpressionScript());
        vo.setUOutputMappingScript(getUOutputMappingScript());
        vo.setDetailFormat(getDetailFormat());
        vo.setSummaryFormat(getSummaryFormat());
        vo.setSummaryRule(this.getSummaryRule());
        vo.setDetailRule(this.getDetailRule());
        
        //get the ref assessors
        Collection<ResolveAssessRel> resolveAssessRels = Hibernate.isInitialized(this.resolveAssessRels) ? getResolveAssessRels() : null;
        if(resolveAssessRels != null && resolveAssessRels.size() > 0)
        {
            Set<String> refAssessors = new HashSet<String>();
            
            for(ResolveAssessRel model : resolveAssessRels)
            {
                refAssessors.add(model.getURefAssessor());
            }
            vo.setRefAssessors(refAssessors);
        }
        
        return vo;
    }

    @Override
    public void applyVOToModel(ResolveAssessVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo); 
            
            this.setUDescription(StringUtils.isNotBlank(vo.getUDescription()) && vo.getUDescription().equals(VO.STRING_DEFAULT) ? getUDescription() : vo.getUDescription());
            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUScript(StringUtils.isNotBlank(vo.getUScript()) && vo.getUScript().equals(VO.STRING_DEFAULT) ? getUScript() : vo.getUScript());

            this.setUOnlyCompleted(vo.getUOnlyCompleted()!= null ? vo.getUOnlyCompleted() : getUOnlyCompleted());
            this.setUParse(vo.getUParse() != null ? vo.getUParse() : getUParse());
            this.setUExpressionScript(StringUtils.isNotBlank(vo.getUExpressionScript()) && vo.getUExpressionScript().equals(VO.STRING_DEFAULT) ? getUExpressionScript() : vo.getUExpressionScript());
            this.setUOutputMappingScript(StringUtils.isNotBlank(vo.getUOutputMappingScript()) && vo.getUOutputMappingScript().equals(VO.STRING_DEFAULT) ? getUOutputMappingScript() : vo.getUOutputMappingScript());
            this.setSummaryFormat(StringUtils.isNotBlank(vo.getSummaryFormat()) && vo.getSummaryFormat().equals(VO.STRING_DEFAULT) ? getSummaryFormat() : vo.getSummaryFormat());
            this.setDetailFormat(StringUtils.isNotBlank(vo.getDetailFormat()) && vo.getDetailFormat().equals(VO.STRING_DEFAULT) ? getDetailFormat() : vo.getDetailFormat());
            this.setSummaryRule(StringUtils.isNotBlank(vo.getSummaryRule()) && vo.getSummaryRule().equals(VO.STRING_DEFAULT) ? getSummaryRule() : vo.getSummaryRule());
            this.setDetailRule(StringUtils.isNotBlank(vo.getDetailRule()) && vo.getDetailRule().equals(VO.STRING_DEFAULT) ? getDetailRule() : vo.getDetailRule());
        }
        
    }
    

}
