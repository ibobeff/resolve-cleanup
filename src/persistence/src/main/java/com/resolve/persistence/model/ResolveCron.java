/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

// Generated Dec 11, 2009 11:15:20 AM by Hibernate Tools 3.2.4.GA

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.CallbackException;
import org.hibernate.type.Type;

import com.resolve.esb.ESB;
import com.resolve.persistence.util.BlockingSIDListener;
import com.resolve.persistence.util.BlockingUListener;
import com.resolve.persistence.util.ResolveInterceptor;
import com.resolve.persistence.util.SIDListener;
import com.resolve.services.hibernate.vo.ResolveCronVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * ResolveCronTask generated by hbm2java
 */
@Entity
@Table(name = "resolve_cron")
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveCron  extends BaseModel<ResolveCronVO>
{
    private static final long serialVersionUID = -4714627623869336019L;
    
	private String UName;
	private String UModule;
	private String URunbook;
	private String UExpression;
	private String UParams;
	private Date UStartTime;
	private Date UEndTime;
	private Boolean UActive;

	public ResolveCron() {
	}
	
	public ResolveCron(ResolveCronVO vo) 
	{
	    applyVOToModel(vo);
    }

	public ResolveCron(String sys_id) 
	{
		this.setSys_id(sys_id);
	}

	@Column(name = "u_name", length = 100, unique = true)
	public String getUName() 
	{
		return this.UName;
	}

	public void setUName(String UName) 
	{
		this.UName = UName;
	}

	@Column(name = "u_module", length = 100)
	public String getUModule() 
	{
		return this.UModule;
	}

	public void setUModule(String UModule) 
	{
		this.UModule = UModule;
	}

	@Column(name = "u_runbook", length = 4000)
	public String getURunbook() 
	{
		return this.URunbook;
	}

	public void setURunbook(String URunbook) 
	{
		this.URunbook = URunbook;
	}

	@Column(name = "u_expression", length = 40)
	public String getUExpression() 
	{
		return this.UExpression;
	}

	public void setUExpression(String UExpression) 
	{
		this.UExpression = UExpression;
	}

	@Column(name = "u_params", length = 4000)
	public String getUParams() 
	{
		return this.UParams;
	}

	public void setUParams(String UParams) 
	{
		this.UParams = UParams;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "u_start_time", length = 19)
	public Date getUStartTime() 
	{
		return this.UStartTime;
	}

	public void setUStartTime(Date UStartTime) 
	{
		this.UStartTime = UStartTime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "u_end_time", length = 19)
	public Date getUEndTime() 
	{
		return this.UEndTime;
	}

	public void setUEndTime(Date UEndTime) 
	{
		this.UEndTime = UEndTime;
	}

    @Column(name = "u_active")
    public Boolean getUActive()
    {
        return this.UActive;
    } // getUActive

    public Boolean ugetUActive()
    {
        // NOTE: null (old record) means active
        if (this.UActive == null)
        {
            return new Boolean(true);
        }
        else
        {
            return this.UActive;
        }
    } // getUActive

    public void setUActive(Boolean UActive)
    {
        this.UActive = UActive;
    }

	public static void initListener(ESB mainESB)
	{
	    final ESB esb = mainESB;
	
	    // insert / update listeners
	    BlockingSIDListener insertListener = new BlockingSIDListener()
        {
            @Override
            public boolean onDataOperation(Object entity, Serializable id, Object[] values, String[] names, Type[] types) throws CallbackException
            {
                Map<String, Object> content = new HashMap<String, Object>();
                content.put(Constants.ESB_PARAM_CRONID, id);
                
                // wait 5secs - to allow commit to complete - NOT REQUIRED AS NOW WE ARE DOING AFTER INSERT
                //ThreadUtils.sleep(5000);
                
                Log.log.debug("Sending the cron loadTask request: "+content);
                esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROLS, "MCron.loadTask", content);
                return true;
            }

        };
        ResolveInterceptor.addInsertAfterListener(ResolveCron.class.getName(), insertListener);

        BlockingUListener updateListener = new BlockingUListener()
        {
            @Override
            public boolean onUpdate(Object entity, Serializable id, Object[] state, Object[] previousState, String[] propertyNames, Type[] types)
                            throws CallbackException
            {
                Map<String, Object> content = new HashMap<String, Object>();
                content.put(Constants.ESB_PARAM_CRONID, id);
                
                // wait 5secs - to allow commit to complete - NOT REQUIRED AS NOW WE ARE DOING AFTER INSERT
                //ThreadUtils.sleep(5000);
                
                Log.log.debug("Sending the cron loadTask request: "+content);
                esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROLS, "MCron.loadTask", content);
                return true;
            }

        };
        ResolveInterceptor.addUpdateAfterListener(ResolveCron.class.getName(), updateListener);
        
        // remove listener
        SIDListener deleteListener = new SIDListener()
        {
            @Override
            public boolean onDataOperation(Object entity, Serializable id, Object[] values, String[] names, Type[] types) throws CallbackException
            {
                boolean result = false;
                
                ResolveCron obj = (ResolveCron)entity;
                
                Map<String, Object> content = new HashMap<String, Object>();
                content.put(Constants.CRON_NAME, obj.getUName());
                
                // wait 5secs - to allow commit to complete
                //ThreadUtils.sleep(5000);
                
                Log.log.debug("Sending the cron removeTask request: "+content);
                esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROLS, "MCron.removeTask", content);
                result = true;
                
                return result;
            }
        };
        ResolveInterceptor.addAsyncDeleteListener(ResolveCron.class.getName(), deleteListener);

        
	} // initListener

    @Override
    public ResolveCronVO doGetVO()
    {
        ResolveCronVO vo = new ResolveCronVO();
        super.doGetBaseVO(vo);
        
        vo.setUActive(getUActive());
        vo.setUEndTime(getUEndTime());
        vo.setUExpression(getUExpression());
        vo.setUModule(getUModule());
        vo.setUName(getUName());
        vo.setUParams(getUParams());
        vo.setURunbook(getURunbook());
        vo.setUStartTime(getUStartTime());
        
        
        return vo;
    }

    @Override
    public void applyVOToModel(ResolveCronVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUActive(vo.getUActive() != null ? vo.getUActive() : getUActive());
            this.setUStartTime(vo.getUStartTime() != null && vo.getUStartTime().equals(VO.DATE_DEFAULT) ? getUStartTime() : vo.getUStartTime());
            this.setUEndTime(vo.getUEndTime()!= null && vo.getUEndTime().equals(VO.DATE_DEFAULT) ? getUEndTime() : vo.getUEndTime());

            this.setUExpression(StringUtils.isNotBlank(vo.getUExpression()) && vo.getUExpression().equals(VO.STRING_DEFAULT) ? getUExpression() : vo.getUExpression());
            this.setUModule(StringUtils.isNotBlank(vo.getUModule()) && vo.getUModule().equals(VO.STRING_DEFAULT) ? getUModule() : vo.getUModule());
            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUParams(StringUtils.isNotBlank(vo.getUParams()) && vo.getUParams().equals(VO.STRING_DEFAULT) ? getUParams() : vo.getUParams());
            this.setURunbook(StringUtils.isNotBlank(vo.getURunbook()) && vo.getURunbook().equals(VO.STRING_DEFAULT) ? getURunbook() : vo.getURunbook());
            
        }
        
    }
	
} // ResolveCron
