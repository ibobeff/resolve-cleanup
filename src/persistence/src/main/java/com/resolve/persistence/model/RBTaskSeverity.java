/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.RBTaskSeverityVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
@Entity
@Table(name = "rb_task_severity")
public class RBTaskSeverity extends BaseModel<RBTaskSeverityVO>
{
    private String severityId; //identifier for GOOD, BAD, WARN, SEVERE, CRITICAL
    private String severity; //GOOD, BAD, WARN, SEVERE, CRITICAL
    private String criteria; //ANY, ALL
    private String variableSource;
    private String variable;
    private String comparison; //EQUALS, >, >= etc.
    private String source;
    private String sourceName;
    private Double order;
    
    private RBTask task;

    public RBTaskSeverity()
    {
    }

    public RBTaskSeverity(RBTaskSeverityVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_severity_id", length = 40)
    public String getSeverityId()
    {
        return severityId;
    }

    public void setSeverityId(String severityId)
    {
        this.severityId = severityId;
    }

    @Column(name = "u_severity", length = 40)
    public String getSeverity()
    {
        return severity;
    }

    public void setSeverity(String severity)
    {
        this.severity = severity;
    }

    @Column(name = "u_criteria", length = 40)
    public String getCriteria()
    {
        return criteria;
    }
   
    public void setCriteria(String criteria)
    {
        this.criteria = criteria;
    }
    
    @Column(name = "u_variable_source", length = 40)
    public String getVariableSource()
    {
        return variableSource;
    }

    public void setVariableSource(String variableSource)
    {
        this.variableSource = variableSource;
    }

    @Column(name = "u_variable", length = 32)
    public String getVariable()
    {
        return variable;
    }

    public void setVariable(String variable)
    {
        this.variable = variable;
    }

    @Column(name = "u_comparison", length = 40)
    public String getComparison()
    {
        return comparison;
    }

    public void setComparison(String comparison)
    {
        this.comparison = comparison;
    }

    @Column(name = "u_source", length = 40)
    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }
    
    @Column(name = "u_source_name", length = 40)
    public String getSourceName()
    {
        return sourceName;
    }

    public void setSourceName(String sourceName)
    {
        this.sourceName = sourceName;
    }
    
    @Column(name = "u_order", precision=10, scale=2)
    public Double getOrder()
    {
        return order;
    }

    public void setOrder(Double order)
    {
        this.order = order;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_task_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public RBTask getTask()
    {
        return this.task;
    }

    public void setTask(RBTask task)
    {
        this.task = task;
    }

    @Override
    public RBTaskSeverityVO doGetVO()
    {
        RBTaskSeverityVO vo = new RBTaskSeverityVO();
        super.doGetBaseVO(vo);

        vo.setSeverityId(getSeverityId());
        vo.setSeverity(getSeverity());
        vo.setCriteria(getCriteria());
        vo.setVariableSource(getVariableSource());
        vo.setVariable(getVariable());
        vo.setComparison(getComparison());
        vo.setSource(getSource());
        vo.setSourceName(getSourceName());
        vo.setOrder(getOrder());

        return vo;
    }

    @Override
    public void applyVOToModel(RBTaskSeverityVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setSeverityId(StringUtils.isNotBlank(vo.getSeverityId()) && vo.getSeverityId().equals(VO.STRING_DEFAULT) ? getSeverityId() : vo.getSeverityId());
            this.setSeverity(StringUtils.isNotBlank(vo.getSeverity()) && vo.getSeverity().equals(VO.STRING_DEFAULT) ? getSeverity() : vo.getSeverity());
            this.setCriteria(StringUtils.isNotBlank(vo.getCriteria()) && vo.getCriteria().equals(VO.STRING_DEFAULT) ? getCriteria() : vo.getCriteria());
            this.setVariableSource(StringUtils.isNotBlank(vo.getVariableSource()) && vo.getVariableSource().equals(VO.STRING_DEFAULT) ? getVariableSource() : vo.getVariableSource());
            this.setVariable(StringUtils.isNotBlank(vo.getVariable()) && vo.getVariable().equals(VO.STRING_DEFAULT) ? getVariable() : vo.getVariable());
            this.setComparison(StringUtils.isNotBlank(vo.getComparison()) && vo.getComparison().equals(VO.STRING_DEFAULT) ? getComparison() : vo.getComparison());
            this.setSource(StringUtils.isNotBlank(vo.getSource()) && vo.getSource().equals(VO.STRING_DEFAULT) ? getSource() : vo.getSource());
            this.setSourceName(StringUtils.isNotBlank(vo.getSourceName()) && vo.getSourceName().equals(VO.STRING_DEFAULT) ? getSourceName() : vo.getSourceName());
            this.setOrder(vo.getOrder() != null && vo.getOrder().equals(VO.INTEGER_DEFAULT) ? getOrder() : vo.getOrder());
            
            //references
            this.setTask(vo.getTask() != null ? new RBTask(vo.getTask()) : null);
        }
    }
}
