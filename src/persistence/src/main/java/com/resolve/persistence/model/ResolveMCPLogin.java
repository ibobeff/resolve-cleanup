package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.resolve.services.hibernate.vo.ResolveMCPLoginVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
//@Table(name = "resolve_mcp_login", uniqueConstraints = {@UniqueConstraint(columnNames = {"u_groupname", "u_clustername"}) })
@Table(name = "resolve_mcp_login")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveMCPLogin extends BaseModel<ResolveMCPLoginVO>
{
    private static final long serialVersionUID = -4765650543436053846L;
    
    private String UGroupName;
    private String UClusterName;
    private String UPWDValue;
    
    public ResolveMCPLogin()
    {
    }
    
    public ResolveMCPLogin(ResolveMCPLoginVO vo)
    {
        applyVOToModel(vo);
    }
    
    @Column(name = "u_groupname", length = 300, nullable=false)
    public String getUGroupName()
    {
        return UGroupName;
    }
    
    public void setUGroupName(String uGroupName)
    {
        UGroupName = uGroupName;
    }
    
    @Column(name = "u_clustername", length = 300, nullable=false)
    public String getUClusterName()
    {
        return UClusterName;
    }
    
    public void setUClusterName(String uClusterName)
    {
        UClusterName = uClusterName;
    }
    
    @Column(name = "u_pwdvalue", length = 300, nullable=false)
    public String getUPWDValue()
    {
        return UPWDValue;
    }
    
    public void setUPWDValue(String value)
    {
        UPWDValue = value;
    }

    @Override
    public ResolveMCPLoginVO doGetVO()
    {
        ResolveMCPLoginVO vo = new ResolveMCPLoginVO();
        super.doGetBaseVO(vo);

        vo.setUGroupName(getUGroupName());
        vo.setUClusterName(getUClusterName());
        vo.setUPWDValue(getUPWDValue());
        return vo;
    }
    
    @Override
    public void applyVOToModel(ResolveMCPLoginVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            this.setUGroupName(StringUtils.isNotBlank(vo.getUGroupName()) && vo.getUGroupName().equals(VO.STRING_DEFAULT) ? getUGroupName() : vo.getUGroupName());
            this.setUClusterName(StringUtils.isNotBlank(vo.getUClusterName()) && vo.getUClusterName().equals(VO.STRING_DEFAULT) ? getUClusterName() : vo.getUClusterName());
            this.setUPWDValue(StringUtils.isNotBlank(vo.getUPWDValue()) && vo.getUPWDValue().equals(VO.STRING_DEFAULT) ? getUPWDValue() : vo.getUPWDValue());
        }
    }
}






