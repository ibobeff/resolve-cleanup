package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;

import com.resolve.services.hibernate.vo.ResolveMCPClusterVO;
import com.resolve.services.hibernate.vo.ResolveMCPHostVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "resolve_mcp_cluster", uniqueConstraints = {@UniqueConstraint(columnNames = {"u_clustername", "u_group_id"}, name = "rmcpcl_clustname_grpid_uk")},
        indexes = {
                @Index(columnList = "u_clustername", name = "rmc_u_clustername_idx"),
                @Index(columnList = "u_group_id", name = "rmc_group_id_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveMCPCluster extends BaseModel<ResolveMCPClusterVO>
{
    private static final long serialVersionUID = -9152550249769984501L;
    
    private String UClusterName;
    private Boolean UIsMonitored;
    private ResolveMCPGroup clusterGroup;
    private String URsmqAddr;
    
    private Collection<ResolveMCPHost> hosts;
    
    public ResolveMCPCluster()
    {
    }
    
    public ResolveMCPCluster(ResolveMCPClusterVO vo)
    {
        applyVOToModel(vo);
    }
    
    @Column(name = "u_rsmqaddr", length = 300, nullable=false)
    public String getURsmqAddr()
    {
        return URsmqAddr;
    }
    public void setURsmqAddr(String addr)
    {
        URsmqAddr = addr;
    }

    @Column(name = "u_clustername", length = 300, nullable=false)
    public String getUClusterName()
    {
        return UClusterName;
    }
    public void setUClusterName(String uClusterName)
    {
        UClusterName = uClusterName;
    }

    @Column(name = "u_is_monitored", length = 1)
    @Type(type = "yes_no")
    public Boolean getUIsMonitored()
	{
		return UIsMonitored;
	}

	public void setUIsMonitored(Boolean uIsMonitored)
	{
		UIsMonitored = uIsMonitored;
	}

	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_group_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveMCPGroup getClusterGroup()
    {
        return clusterGroup;
    }

    public void setClusterGroup(ResolveMCPGroup group)
    {
        this.clusterGroup = group;
    }

    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "cluster", cascade=CascadeType.ALL)
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<ResolveMCPHost> getHosts()
    {
        return hosts;
    }
    public void setHosts(Collection<ResolveMCPHost> hosts)
    {
        this.hosts = hosts;
    }
    
    @Override
    public ResolveMCPClusterVO doGetVO()
    {
        ResolveMCPClusterVO vo = new ResolveMCPClusterVO();
        super.doGetBaseVO(vo);

        vo.setUClusterName(getUClusterName());
        Collection<ResolveMCPHost> mcpHosts = Hibernate.isInitialized(this.getHosts()) ? getHosts() : null;
        if (mcpHosts != null)
        {
            Collection<ResolveMCPHostVO> hostVOs = new ArrayList<ResolveMCPHostVO>();
            for (ResolveMCPHost host : mcpHosts)
            {
                hostVOs.add(host.doGetVO());
            }
            vo.setChildren(hostVOs);
        }

        return vo;
    }
    
    @Override
    public void applyVOToModel(ResolveMCPClusterVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setUClusterName(StringUtils.isNotBlank(vo.getUClusterName()) && vo.getUClusterName().equals(VO.STRING_DEFAULT) ? getUClusterName() : vo.getUClusterName());
        }
    }

}
