/******************************************************************************
* (C) Copyright 2014
import com.resolve.persistence.util.HibernateUtil
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import java.lang.reflect.Field;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.sql.rowset.serial.SerialClob;

import org.apache.commons.lang3.tuple.Pair;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.mapping.Column;
import org.hibernate.mapping.PersistentClass;
import org.hibernate.mapping.Property;
import org.hibernate.proxy.map.MapProxy;
import org.hibernate.query.Query;
import org.hibernate.type.ClobType;
import org.owasp.esapi.ESAPI;

import com.resolve.persistence.dao.OrderbyProperty;
import com.resolve.persistence.model.CustomTable;
import com.resolve.services.hibernate.util.PropertiesUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryParameter;
import com.resolve.services.vo.QuerySort;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.ResolveDefaultValidator;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;

public class EntityUtil
{
    private static final String DOLLAR_TYPE_DOLLAR = "$type$";
    
    @SuppressWarnings("unchecked")
    public static Map<String,Object> findById(String entityName, String id)
    {
        Session session = HibernateUtil.getCurrentSession();
        return (Map<String, Object>)session.get(entityName, id);
    }

    @SuppressWarnings("unchecked")
    public static Map<String,Object> findById(String entityName, String id, boolean lock) throws Exception
    {
    	return (Map<String, Object>) HibernateProxy.execute(() -> {
    		Session session = HibernateUtil.getCurrentSession();
            if (lock)
            {
                return (Map<String, Object>)session.get(entityName, id, LockMode.PESSIMISTIC_WRITE);
            }
            else
            {
                return (Map<String, Object>)session.get(entityName, id);
            }
    	});
        
    }

    @SuppressWarnings("unchecked")
    public static Map<String,Object> loadById(String entityName, String id)
    {
    	try {
			return (Map<String, Object>) HibernateProxy.execute(() -> {
				Session session = HibernateUtil.getCurrentSession();
			    return (Map<String, Object>)session.load(entityName, id);
			});
		} catch (Exception e) {
                             HibernateUtil.rethrowNestedTransaction(e);
			Log.log.error(e.getMessage(), e);
			return null;
		}
        
    }

    @SuppressWarnings("unchecked")
    public static Map<String,Object> loadById(String entityName, String id, boolean lock)
    {
        try {
			return (Map<String, Object>) HibernateProxy.execute(() -> {
				Session session = HibernateUtil.getCurrentSession();
			    if (lock)
			    {
			        return (Map<String, Object>)session.load(entityName, id, LockMode.PESSIMISTIC_WRITE);
			    }
			    else
			    {
			        return (Map<String, Object>)session.load(entityName, id);
			    }
			});
		} catch (Exception e) {
                             HibernateUtil.rethrowNestedTransaction(e);
			Log.log.error(e.getMessage(), e);
			return null;
		}
    }

    public static List<Map<String,Object>> find(String entityName) throws Exception
    {
        return findAll(entityName);
    }

    public static Map<String,Object> findFirst(String entityName, Object example, String... excludeProperty) throws Exception
    {
        List<Map<String,Object>> results = find(entityName, example, excludeProperty);
        if (results.size()==0)
        {
            return null;
        }
        else
        {
            return results.get(0);
        }
    }


    public static List<Map<String,Object>> find(String entityName, int maxResults) throws Exception
    {
        return findAll(entityName, maxResults);
    }

    public static List<Map<String,Object>> find(String entityName, int maxResults, int firstResult) throws Exception
    {
        return findAll(entityName, maxResults, firstResult);
    }

    public static List<Map<String,Object>> find(String entityName, Object exampleInstance, String... excludeProperty) throws Exception
    {
        return find(entityName, exampleInstance, -1, -1, excludeProperty);
    }

    public static List<Map<String,Object>> findAll(String entityName) throws Exception
    {
        return findByCriteria(entityName, -1, -1);
    }

    public static List<Map<String, Object>> findAll(String entityName, int maxResults) throws Exception
    {
        return findByCriteria(entityName, maxResults, -1);
    }

    public static List<Map<String, Object>> findAll(String entityName, int maxResults, int firstResult) throws Exception
    {
        return findByCriteria(entityName, maxResults, firstResult);
    }

    @SuppressWarnings("unchecked")
	public static Map<String,Object> refresh(Map<String,Object> entity)
    {
    	try {
			return (Map<String, Object>) HibernateProxy.execute(() -> {
				HibernateUtil.getCurrentSession().refresh(entity);
			    return entity;
			});
		} catch (Exception e) {
                             HibernateUtil.rethrowNestedTransaction(e);
			Log.log.error(e.getMessage(), e);
			return null;
		}
        
    }

    @SuppressWarnings("unchecked")
	public static Map<String,Object> persist(String entityName, Map<String,Object> entity)
    {
    	try {
			return (Map<String, Object>) HibernateProxy.execute(() -> {
				HibernateUtil.getCurrentSession().saveOrUpdate(entityName, entity);
			    return entity;
			});
		} catch (Exception e) {
                             HibernateUtil.rethrowNestedTransaction(e);
			Log.log.error(e.getMessage(), e);
			return null;
		}
        
    }

    public static void delete(String entityName, Map<String,Object> entity)
    {
        HibernateUtil.getCurrentSession().delete(entityName, entity);
    }

    public static void delete(String entityName, String sys_id )
    {
        Map<String, Object> e = findById(entityName, sys_id);
        if (e!=null)
        {
            delete(entityName, e );
        }
    }

    public static void delete(String entityName, List<String> sys_ids)
    {
        for (String id : sys_ids)
        {
            delete(entityName, id);
        }
    }

    /*
     *  HP TO DO : Determine if type is set to text instead of clob in 
     *  custom table mapping, then is it still required.
     *  
     *  Custom table WYSIWYG and Journal field types set field type as clob 
     *  in Hibernate mapping file. This need to be changed to "text".
     *  
     *  Also need to think how to handle legacy data.
     *  
     *  Convert statically (update) or dynamically at load.
     *  Modify loading of custom tabes to transform clob to text.
     *  
     *  Handles conversion of given Object field type to entity field type for
     *  following given object field types to entity field type
     *  
     *  java.labg.String to jav.sql.Clob
     */
    
    private static Map<String,Object> safelyCastEntityTypes(String entityName, Map<String,Object> entity) throws Exception
    {
        Map<String, Object> converted = new HashMap<String, Object>();
        
        PersistentClass persistClass = HibernateUtil.getClassMapping(entityName);
        
        if (persistClass == null)
        {
            throw new Exception("Failed to get persistent class for entity named " + entityName + ".");
        }
        
        // HP TODO Handle Tx of social entity DTOs to social custom tables
        
        for (String propertyName : entity.keySet())
        {
            if (!propertyName.equals(DOLLAR_TYPE_DOLLAR))
            {
				if (persistClass.hasProperty(propertyName)) {
					Property hibFldProp = persistClass.getProperty(propertyName);

					if (hibFldProp != null && hibFldProp.getType() instanceof ClobType
							&& entity.get(propertyName) instanceof String) {
						converted.put(propertyName, new SerialClob(((String) entity.get(propertyName)).toCharArray()));
					} else {
                    converted.put(propertyName, entity.get(propertyName));
                }
            }
        }
        }
        
        return converted;
    }
    
    @SuppressWarnings("unchecked")
    public  static Map<String,Object> insertOrUpdate(String entityName, Map<String,Object> entity) throws Exception
    {
    	return (Map<String, Object>) HibernateProxy.execute(() -> {
    		HibernateUtil.getCurrentSession().saveOrUpdate(entityName, entity);        
            return entity;
    	});
        
    }

    public static Map<String,Object> save(String entityName, Map<String,Object> entity) throws Exception
    {
        HibernateUtil.getCurrentSession().save(entityName, entity);
        return entity;
    }

    public static Map<String,Object> insert(String entityName, Map<String,Object> entity) throws Exception
    {
    	HibernateProxy.execute(() -> {
    		HibernateUtil.getCurrentSession().save(entityName, entity);
    	});
        
        return entity;
    }

    @SuppressWarnings("unchecked")
	public static Map<String,Object> update(String entityName, Map<String,Object> entity) throws Exception
    {
    	return (Map<String, Object>) HibernateProxy.execute(() -> {
    		HibernateUtil.getCurrentSession().update(entityName, entity);
            return entity;
    	});
        
    }

    public static List<Map<String,Object>> find(String entityName, Object exampleInstance, List<OrderbyProperty> orderbys, String... excludeProperty) throws Exception
    {
        return find(entityName, exampleInstance, -1, -1, orderbys, excludeProperty);
    }


    public static List<Map<String,Object>> find(String entityName, Object exampleInstance, int maxResults, String... excludeProperty) throws Exception
    {
        return find(entityName, exampleInstance, maxResults, -1, excludeProperty);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static Pair<CriteriaBuilder, CriteriaQuery> buildCriteriaQueryByExample(String entityName, Object exampleInstance, String... excludeProperty) throws Exception
    {
        PersistentClass persistClass = HibernateUtil.getClassMapping(entityName);
        
        if (persistClass == null)
        {
            throw new Exception("Failed to get persistent class for entity named " + entityName + ".");
        }
        
        CriteriaBuilder criteriaBldr = HibernateUtil.getCurrentSession().getCriteriaBuilder();
        
        CriteriaQuery criteriaQry = criteriaBldr.createQuery(persistClass.getClass());
        
        Root qryFrom = criteriaQry.from(persistClass.getClass());
        
        criteriaQry.select(qryFrom);
        
        if (exampleInstance instanceof Map)
        {
            Map<String, Object> exmplInstPropValMap = (Map<String, Object>)exampleInstance;
            Set<String> excludedProps = new HashSet<String>(Arrays.asList(excludeProperty));
            ArrayList<Predicate> predicates = new ArrayList<Predicate>();
            
            if (!exmplInstPropValMap.isEmpty())
            {
                for (String prop : exmplInstPropValMap.keySet())
                {
                    if (!excludedProps.contains(prop) && exmplInstPropValMap.get(prop) != null)
                    {
                        Predicate prdCt = criteriaBldr.equal(qryFrom.get(prop), exmplInstPropValMap.get(prop));
                        predicates.add(prdCt);
                    }
                }
            }
            
            if (predicates.size() > 0)
            {
                Predicate andCnjnctPred = criteriaBldr.and(predicates.toArray(new Predicate[predicates.size()]));
                criteriaQry.where(andCnjnctPred);
            }
        }
        
        return Pair.of(criteriaBldr,criteriaQry);
    }
    
    @Deprecated
//    @SuppressWarnings({ "unchecked", "rawtypes" })
    @SuppressWarnings({ "unchecked" })
    public static List<Map<String,Object>> find(String entityName, Object exampleInstance, int maxResults, int firstResult, String... excludeProperty) throws Exception
    {
//        CriteriaQuery criteriaQry = buildCriteriaQueryByExample(entityName, exampleInstance, excludeProperty).getRight();
//        
//        Query qry = HibernateUtil.getCurrentSession().createQuery(criteriaQry);
//        
//        if (maxResults >= 0)
//        {
//            qry.setMaxResults(maxResults);
//        }
//        
//        if (firstResult >= 0)
//        {
//            qry.setFirstResult(firstResult);
//        }
//        
//        return qry.list();
        
        Criteria crit = HibernateUtil.getCurrentSession().createCriteria(entityName);
        Example example = Example.create(exampleInstance);
        for (String exclude : excludeProperty)
        {
            example.excludeProperty(exclude);
        }
        crit.add(example);
        if (maxResults >= 0)
        {
            crit.setMaxResults(maxResults);
        }
        if (firstResult >= 0)
        {
            crit.setFirstResult(firstResult);
        }
        
        return crit.list();
    }

    public static List<Map<String,Object>> find(String entityName, Object exampleInstance, int maxResults, List<OrderbyProperty> orderbys, String... excludeProperty) throws Exception
    {
        return find(entityName, exampleInstance, maxResults, -1, orderbys, excludeProperty);
    }

    public static List<Map<String,Object>> find(String entityName, List<OrderbyProperty> orderbys) throws Exception
    {
        return findAll(entityName, orderbys);
    }

    public static List<Map<String,Object>> find(String entityName, /*Predicate...*/Criterion... restrictions) throws Exception
    {
        return findAll(entityName, restrictions);
    }

    public static List<Map<String,Object>> find(String entityName, int maxResults, /*Predicate...*/Criterion... restrictions) throws Exception
    {
        return findAll(entityName, maxResults, restrictions);
    }

    public static List<Map<String,Object>> find(String entityName, int maxResults, List<OrderbyProperty> orderbys) throws Exception
    {
        return findAll(entityName, maxResults, orderbys);
    }

    public static List<Map<String, Object>> findAll(String entityName, /*Predicate...*/Criterion... restrictions) throws Exception
    {
        return findByCriteria(entityName, -1, -1, restrictions);
    }

    public static List<Map<String, Object>> findAll(String entityName, int maxResults, /*Predicate...*/Criterion... restrictions) throws Exception
    {
        return findByCriteria(entityName, maxResults, -1, restrictions);
    }

    public static List<Map<String,Object>> findAll(String entityName, List<OrderbyProperty> orderbys) throws Exception
    {
        return findByCriteria(entityName, -1, -1, orderbys);
    }

    public static List<Map<String,Object>> findAll(String entityName, int maxResults, List<OrderbyProperty> orderbys) throws Exception
    {
        return findByCriteria(entityName, maxResults, -1, orderbys);
    }

    public static List<Map<String,Object>> find(String entityName, List<OrderbyProperty> orderbys, /*Predicate...*/Criterion restrictions) throws Exception
    {
        return findAll(entityName, orderbys, restrictions);
    }

    public  static List<Map<String,Object>> findAll(String entityName, List<OrderbyProperty>  orderBy, /*Predicate...*/Criterion... restrictions) throws Exception
    {
        return findByCriteria(entityName, -1, -1, orderBy, restrictions);
    }

    @Deprecated
//    @SuppressWarnings({ "unchecked", "rawtypes" })
    @SuppressWarnings({ "unchecked" })
    protected static List<Map<String,Object>> findByCriteria(String entityName, int maxResults, int firstResult, List<OrderbyProperty> props, Criterion... criterion/*Predicate... restrictions*/) throws Exception
    {
        return (List<Map<String, Object>>) HibernateProxy.execute(() -> {
        	List<Map<String,Object>> retList = new ArrayList<Map<String,Object>>();
            
            try
            {
                Criteria crit = HibernateUtil.getCurrentSession().createCriteria(entityName);
                for (Criterion c : criterion)
                {
                    crit.add(c);
                }
        
        //        PersistentClass persistClass = HibernateUtil.getClassMapping(entityName);
        //        
        //        if (persistClass == null)
        //        {
        //            throw new Exception("Failed to get persistent class for entity named " + entityName + ".");
        //        }
        //        
        //        CriteriaBuilder criteriaBldr = HibernateUtil.getCurrentSession().getCriteriaBuilder();
        //        
        //        CriteriaQuery criteriaQry = criteriaBldr.createQuery(persistClass.getEntityPersisterClass());
        //        
        //        Root qryFrom = criteriaQry.from(persistClass.getEntityPersisterClass());
        //        
        //        criteriaQry.select(qryFrom);
        //        
        //        if (restrictions != null && restrictions.length >= 1)
        //        {
        //            Predicate andCnjnctPred = criteriaBldr.and(restrictions);
        //            criteriaQry.where(andCnjnctPred);
        //        }
                
                List<String> dbOrderbyFunctionsBlackList = PropertiesUtil.getPropertyList(Constants.SECURITY_DB_ORDER_BY_FUNCTIONS_BLACKLIST_SYSTEM_PROPERTY);
                
        //        ArrayList<Order> orderBys = new ArrayList<Order>();
                
                for (OrderbyProperty order : props)
                {
                    // Validate order.property against order by functions black list
                    
                    if (dbOrderbyFunctionsBlackList != null && !dbOrderbyFunctionsBlackList.isEmpty() &&
                        dbOrderbyFunctionsBlackList.contains(order.property.toLowerCase()))
                    {
                        throw new RuntimeException("Unsupported function in order by clause.");
                    }
                    
                    crit.addOrder(order.asc ? org.hibernate.criterion.Order.asc(order.property) : org.hibernate.criterion.Order.desc(order.property));
        //            Order orderElm = order.asc ? criteriaBldr.asc(qryFrom.get(order.property)) : 
        //                             criteriaBldr.desc(qryFrom.get(order.property));
        //            
        //            orderBys.add(orderElm);
                }
        
        //        Query qry = HibernateUtil.getCurrentSession().createQuery(criteriaQry.orderBy(orderBys));
                
                if (maxResults >= 0)
                {
                    crit.setMaxResults(maxResults);
        //            qry.setMaxResults(maxResults);
                }
        
                if (firstResult >= 0)
                {
                    crit.setFirstResult(firstResult);
        //            qry.setFirstResult(firstResult);
                }
        
        //        retList = qry.list();
                retList = crit.list();
            }
            catch(Throwable t)
            {
                Log.log.error(t.getMessage(), t);
            }
            
            return retList;
        });
    }

    @Deprecated
//    @SuppressWarnings({ "unchecked", "rawtypes" })
    @SuppressWarnings({ "unchecked" })
    protected static List<Map<String,Object>> findByCriteria(String entityName, int maxResults, int firstResult, Criterion... criterion/*Predicate... restrictions*/) throws Exception
    {
        List<Map<String,Object>> retList = new ArrayList<Map<String,Object>>();
        
        try
        {
        	retList = (List<Map<String, Object>>) HibernateProxy.execute(() -> {
            
	            Criteria crit = HibernateUtil.getCurrentSession().createCriteria(entityName);
	            for (Criterion c : criterion)
	            {
	                crit.add(c);
	            }
	  
	            if (maxResults >= 0)
	            {
	                crit.setMaxResults(maxResults);
	            }
	            
	            if (firstResult >= 0)
	            {
	                crit.setFirstResult(firstResult);
	            }
	    
	            return crit.list();
            
            });
        }
        catch(Throwable t)
        {
                      HibernateUtil.rethrowNestedTransaction(t);
            Log.log.error(t.getMessage(), t);
        }
        
        return retList;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<Map<String, Object>> find(String entityName, Object exampleInstance, int maxResults, int firstResult, List<OrderbyProperty> orderbys, String... excludeProperty) throws Exception
    {
    	return (List<Map<String, Object>>) HibernateProxy.execute(() -> {
    	
	        Pair<CriteriaBuilder, CriteriaQuery> pair = buildCriteriaQueryByExample(entityName, exampleInstance, excludeProperty);
	        
	        List<String> dbOrderbyFunctionsBlackList = PropertiesUtil.getPropertyList(Constants.SECURITY_DB_ORDER_BY_FUNCTIONS_BLACKLIST_SYSTEM_PROPERTY);
	        
	        ArrayList<Order> orderBys = new ArrayList<Order>();
	        
	        Root<?> root = null;        
	        Set<Root<?>> roots = pair.getRight().getRoots();
	        
	        if (roots != null && roots.size() == 1)
	        {
	            root = roots.iterator().next();
	            
	            for (OrderbyProperty order : orderbys)
	            {
	                // Validate order.property against order by functions black list
	                
	                if (dbOrderbyFunctionsBlackList != null && !dbOrderbyFunctionsBlackList.isEmpty() &&
	                    dbOrderbyFunctionsBlackList.contains(order.property.toLowerCase()))
	                {
	                    throw new RuntimeException("Unsupported function in order by clause.");
	                }
	                
	                Order orderElm = order.asc ? pair.getLeft().asc(root.get(order.property)) : 
	                                 pair.getLeft().desc(root.get(order.property));
	    
	                orderBys.add(orderElm);
	            }
	        }

	        Query qry = HibernateUtil.getCurrentSession().createQuery(pair.getRight().orderBy(orderBys));
	        
	        if (maxResults >= 0)
	        {
	            qry.setMaxResults(maxResults);
	        }
	        
	        if (firstResult >= 0)
	        {
	            qry.setFirstResult(firstResult);
	        }
	        
	        return qry.list();
    	});
    }

    public static Clob stringToClob(String str)
    {
        return HibernateUtil.getClobFromString(str);
    } // stringToClob

    public static String clobToString(Clob clob)
    {
        return HibernateUtil.getStringFromClob(clob);
    } // clobToString

    public static List<Map<String, Object>> executeSQLSelect(String sql, int start, int limit) throws Exception
    {
        List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();

        Connection connection = null;
        ResultSet rs = null;
        PreparedStatement statement = null;
        ResultSetMetaData rsmd = null;

        boolean getAllRecords = (start == -1) || limit == -1;// get all recs if
                                                             // any of them is
                                                             // not set
        int recStart = start;
        int recEnd = recStart + limit - 1;

        try
        {
            connection = HibernateUtil.getConnection();
            statement = connection.prepareStatement(
            						ESAPI.validator().getValidInput("Execute SQL Select Statement",
            														SQLUtils.getSafeSQL(sql.trim()), 
            														ResolveDefaultValidator.HQL_SQL_SAFE_STRING_VALIDATOR,
            														ResolveDefaultValidator.MAX_HQL_SQL_SAFE_STRING_LENGTH,
            														false, false));
            rs = statement.executeQuery();
            
            rsmd = rs.getMetaData();

            int numberOfColumns = rsmd.getColumnCount();
            String[] dbColumnNames = new String[numberOfColumns];
            for (int count = 0; count < numberOfColumns; count++)
            {
                // columns start with 1 , so count+1
                dbColumnNames[count] = rsmd.getColumnLabel(count + 1);
            }

            int recCount = 0;

            // while loop to get the data
            while (rs.next())
            {
                if (getAllRecords)
                {
                    Map<String, Object> row = getRowData(rs, dbColumnNames);
                    data.add(row);
                }
                else
                {
                    if (recCount >= recStart && recCount <= recEnd)
                    {
                        Map<String, Object> row = getRowData(rs, dbColumnNames);
                        data.add(row);
                    }
                    else if (recCount > recEnd)
                    {
                        break;
                    }
                }

                recCount++;

            }// end of while loop
        }
        catch (Throwable t)
        {
            Log.log.error("Error in fetching data for :" + sql, t);
            throw new Exception(t.getMessage(), t);
        }
        finally
        {
            try
            {
                if (rs != null)
                {
                    rs.close();
                }

                if (statement != null)
                {
                    statement.close();
                }

                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Throwable t)
            {
            }
        }

        return data;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static List<Map<String, Object>> executeHQLSelect(QueryDTO queryDTO, int start, int limit) throws Exception
    {
        List<Map<String, Object>> resultData = new ArrayList<Map<String, Object>>();

        try
        {
          HibernateProxy.setCurrentUser("admin");
            HibernateProxy.executeNoCache(() -> {
	            if (StringUtils.isEmpty(queryDTO.getModelName()))
	            {
	                String tempTableName = queryDTO.getTableName();
	                CustomTable customTable = new CustomTable();
	                customTable.setUName(tempTableName);
	                CustomTable ct = HibernateUtil.getDAOFactory().getCustomTableDAO().findFirst(customTable);
	                String modelName = ct.getUModelName();
	                queryDTO.setModelName(modelName);
	            }
	            String temp = queryDTO.getSelectHQL();
	            Query query = HibernateUtil.createQuery(temp);
	
	            // Add in all the filtering parameters
	            for (QueryParameter param : queryDTO.getWhereParameters())
	            {
	                query.setParameter(param.getName(), param.getValue());
	            }
	
	            // Add in all sort parameters
	            for (QuerySort qsort : queryDTO.getParsedSorts())
	            {
	                query.setParameter(QuerySort.SORT_PROP_NAME_PREFIX + qsort.getProperty().replaceAll("\\.", "_"),
	                                   qsort.getProperty());
	            }
	            
	            query.setFirstResult(start);
	            if (limit > -1) query.setMaxResults(limit);
	            List<Object> rows = query.list();
	
	            for (Object row : rows)
	            {
	                Map<String, Object> hash;
	                if (row instanceof Map == false)
	                {
	                    hash = new HashMap<String, Object>();
	                    Class c = row.getClass();
	                    for (Field field : c.getDeclaredFields())
	                    {
	                        field.setAccessible(true);
	                        String name = field.getName();
	                        Object r = field.get(row);
	                        String columnName = ((Column) HibernateUtil.getClassMapping(c.getName()).getProperty(name).getColumnIterator().next()).getName();
	                        hash.put(columnName, r);
	                    }
	                }
	                else
	                {
	                    hash = (Map<String, Object>) row;
	                }
	
	                resultData.add(getRecordData(hash));
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Error in executing HQL: " + queryDTO.getSelectHQL(), e);
            throw e;
        }

        return resultData;
    }

    private static Map<String, Object> getRecordData(Map<String, Object> record) throws Exception
    {
        Map<String, Object> temp = new HashMap<String, Object>();

        for (String key : record.keySet())
        {
            temp.put(key, getMassagedData(record.get(key)));
        }

        return temp;
    }

    private static Object getMassagedData(Object data) throws Exception
    {
        if (data == null)
        {
            return "";
        }

        if (data instanceof Timestamp)
        {
            return DateUtils.getDateInUTCFormat((Timestamp) data);
        }
        else if (data instanceof oracle.sql.TIMESTAMP)
        {
            oracle.sql.TIMESTAMP tsOracle = (oracle.sql.TIMESTAMP) data;
            return DateUtils.getDateInUTCFormat(tsOracle.timestampValue());
        }
        else if (data instanceof Clob)
        {
            return HibernateUtil.getStringFromClob(data);
        }
        else if (data instanceof Blob)
        {
            // NOTE : To get the file , use the QueryCustomForm.downloadFile api
            return "";
        }
        else if (data instanceof byte[])
        {
            // NOTE : To get the file , use the QueryCustomForm.downloadFile api
            return "";
        }
        else if (data instanceof MapProxy)
        {
            MapProxy temp = (MapProxy) data;
            return temp.get("sys_id");
        }
        return data;
    }

    private static Map<String, Object> getRowData(ResultSet rs, String[] dbColumnNames) throws Exception
    {
        Map<String, Object> row = new HashMap<String, Object>();

        // add the data to the row
        for (int count = 0; count < dbColumnNames.length; count++)
        {
            String column = dbColumnNames[count];
            Object rawdata = rs.getObject(column);
            Object massagedData = null;

            // oracle.sql.TIMESTAMP
            if (rawdata == null)
            {
                rawdata = "";
            }

            if (rawdata instanceof Timestamp)
            {
                massagedData = DateUtils.getDateInUTCFormat((Timestamp) rawdata);
            }
            else if (rawdata instanceof oracle.sql.TIMESTAMP)
            {
                oracle.sql.TIMESTAMP tsOracle = (oracle.sql.TIMESTAMP) rawdata;
                massagedData = DateUtils.getDateInUTCFormat(tsOracle.timestampValue());
            }
            else if (rawdata instanceof /*oracle.sql.*/Clob)
            {
                massagedData = HibernateUtil.getStringFromClob(rawdata);
            }
            else if (rawdata instanceof /*oracle.sql.*/Blob)
            {
                // NOTE : To get the file , use the QueryCustomForm.downloadFile
                // api
                massagedData = "";
            }
            else if (rawdata instanceof byte[])
            {
                // NOTE : To get the file , use the QueryCustomForm.downloadFile
                // api
                massagedData = "";
            }
            else
            {
                massagedData = rawdata;
            }

            row.put(column.toLowerCase(), massagedData);
        }// end of for loop

        return row;
    }
    
//    public static List<Map<String, Object>> findAll(String entityName, Map<String, Object> restrictions) throws Exception
//    {
//        return findByCriteria(entityName, -1, -1, restrictions);
//    }
//    
//    @SuppressWarnings({ "rawtypes", "unchecked" })
//    protected static List<Map<String,Object>> findByCriteria(String entityName, int maxResults, int firstResult, Map<String, Object> restrictions) throws Exception
//    {
//        PersistentClass persistClass = HibernateUtil.getClassMapping(entityName);
//        
//        if (persistClass == null)
//        {
//            throw new Exception("Failed to get persistent class for entity named " + entityName + ".");
//        }
//        
//        CriteriaBuilder criteriaBldr = HibernateUtil.getCurrentSession().getCriteriaBuilder();
//        
//        CriteriaQuery criteriaQry = criteriaBldr.createQuery(persistClass.getEntityPersisterClass());
//        
//        Root qryFrom = criteriaQry.from(persistClass.getEntityPersisterClass());
//        
//        criteriaQry.select(qryFrom);
//        
//        if (restrictions != null && !restrictions.isEmpty())
//        {
//            ArrayList<Predicate> predicates = new ArrayList<Predicate>();
//            
//            for (String restrictionField : restrictions.keySet())
//            {
//                predicates.add(criteriaBldr.equal(qryFrom.get(restrictionField), restrictions.get(restrictionField)));
//            }
//            
//            Predicate andCnjnctPred = criteriaBldr.and(predicates.toArray(new Predicate[predicates.size()]));
//            criteriaQry.where(andCnjnctPred);
//        }
//        
//        Query qry = HibernateUtil.getCurrentSession().createQuery(criteriaQry);
//        
//        if (maxResults >= 0)
//        {
//            qry.setMaxResults(maxResults);
//        }
//        
//        if (firstResult >= 0)
//        {
//            qry.setFirstResult(firstResult);
//        }
//
//        return qry.list();
//    }
} // EntityUtil
