/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.MetaFieldVO;
import com.resolve.services.interfaces.VO;


@Entity
@Table(name = "meta_field", indexes = {
                @Index(columnList = "u_custom_table_sys_id", name = "mf_u_custom_table_sys_idx"),
                @Index(columnList = "u_meta_access_rights_sys_id", name = "mfield_access_rights_idx"),
                @Index(columnList = "u_meta_field_properties_sys_id", name = "mf_u_meta_field_prop_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaField  extends BaseModel<MetaFieldVO>
{   
    private static final long serialVersionUID = -6696327547871540568L;

    public final static String RESOURCE_TYPE = "metafield";
    
    private String UName;
    private String UTable;
    private String UColumn;
    private String UColumnModelName;
    private String UDisplayName;
    private String UType;
    private String UDbType;
    //private Boolean UIsDefaultRole;
    
    // object referenced
    private CustomTable customTable;
    private MetaAccessRights metaAccessRights;
    private MetaFieldProperties metaFieldProperties;
    
    public MetaField()
    {
    }
    
    public MetaField(MetaFieldVO vo)
    {
        applyVOToModel(vo);
    }
    
    @Column(name = "u_name", nullable = false, length = 100)
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    @Column(name = "u_table", length = 100)
    public String getUTable()
    {
        return this.UTable;
    }

    public void setUTable(String UTable)
    {
        this.UTable = UTable;
    }
    
    @Column(name = "u_column", length = 100)
    public String getUColumn()
    {
        return this.UColumn;
    }

    public void setUColumn(String UColumn)
    {
        this.UColumn = UColumn;
    }
    
    @Column(name = "u_column_model_name", length = 100)
    public String getUColumnModelName()
    {
        return this.UColumnModelName;
    }

    public void setUColumnModelName(String UColumnModelName)
    {
        this.UColumnModelName = UColumnModelName;
    }
    
    @Column(name = "u_display_name", length = 100)
    public String getUDisplayName()
    {
        return this.UDisplayName;
    }

    public void setUDisplayName(String UDisplayName)
    {
        this.UDisplayName = UDisplayName;
    }
    
    @Column(name = "u_type", length = 100)
    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }
    
    @Column(name = "u_dbtype", length = 100)
    public String getUDbType()
    {
        return this.UDbType;
    }

    public void setUDbType(String UDbType)
    {
        this.UDbType = UDbType;
    }
       
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_custom_table_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public CustomTable getCustomTable()
    {
        return customTable;
    }

    public void setCustomTable(CustomTable customTable)
    {
        this.customTable = customTable;

        if (customTable != null)
        {
            Collection<MetaField> coll = customTable.getMetaFields();
            if (coll == null)
            {
                coll = new HashSet<MetaField>();
                coll.add(this);
                
                customTable.setMetaFields(coll);
            }
        }
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_meta_access_rights_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade({CascadeType.DELETE})
    public MetaAccessRights getMetaAccessRights()
    {
        return metaAccessRights;
    }

    public void setMetaAccessRights(MetaAccessRights metaAccessRights)
    {
        this.metaAccessRights = metaAccessRights;
    }

    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_meta_field_properties_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaFieldProperties getMetaFieldProperties()
    {
        return this.metaFieldProperties;
    }

    public void setMetaFieldProperties(MetaFieldProperties metaFieldProperties)
    {
        this.metaFieldProperties = metaFieldProperties;
    }

    @Override
    public MetaFieldVO doGetVO()
    {
        MetaFieldVO vo = new MetaFieldVO();
        super.doGetBaseVO(vo);
        
        vo.setUColumn(getUColumn());
        vo.setUColumnModelName(getUColumnModelName());
        vo.setUDbType(getUDbType());
        vo.setUDisplayName(getUDisplayName());
        vo.setUName(getUName());
        vo.setUTable(getUTable());
        vo.setUType(getUType());
        
        //ignore the references as they go back to parent
//        vo.setCustomTable(Hibernate.isInitialized(this.getCustomTable()) && getCustomTable() != null ? getCustomTable().doGetVO() : null) ;
        vo.setMetaAccessRights(Hibernate.isInitialized(this.metaAccessRights) && getMetaAccessRights() != null ? getMetaAccessRights().doGetVO() : null);
        vo.setMetaFieldProperties(Hibernate.isInitialized(this.metaFieldProperties) && getMetaFieldProperties() != null ? getMetaFieldProperties().doGetVO() : null);
        
        return vo;
    }

    @Override
    public void applyVOToModel(MetaFieldVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUColumn((String) VO.evaluateValue(vo.getUColumn(), getUColumn()));
            this.setUColumnModelName((String) VO.evaluateValue(vo.getUColumnModelName(), getUColumnModelName()));
            this.setUDbType((String) VO.evaluateValue(vo.getUDbType(), getUDbType()));
            this.setUDisplayName((String) VO.evaluateValue(vo.getUDisplayName(), getUDisplayName()));
            this.setUName((String) VO.evaluateValue(vo.getUName(), getUName()));
            this.setUTable((String) VO.evaluateValue(vo.getUTable(), getUTable()));
            this.setUType((String) VO.evaluateValue(vo.getUType(), getUType()));
            
            //ignore the references as they go back to parent
//            this.setMetaTable(vo.getMetaTable() != null ? new MetaTable(vo.getMetaTable()) : null);
            this.setMetaAccessRights(vo.getMetaAccessRights() != null ? new MetaAccessRights(vo.getMetaAccessRights()) : null);
            this.setMetaFieldProperties(vo.getMetaFieldProperties() != null ? new MetaFieldProperties(vo.getMetaFieldProperties()) : null);
            
        }
        
    }
    
//    @Column(name = "u_is_default_role", length = 1)
//    @Type(type = "yes_no")
//    public Boolean getUIsDefaultRole()
//    {
//            return UIsDefaultRole;
//    }
//    
//    public Boolean ugetUIsDefaultRole()
//    {
//        if (this.UIsDefaultRole == null)
//        {
//            return new Boolean(false);
//        }
//        else
//        {
//            return this.UIsDefaultRole;
//        }
//    } // ugetUIsDefaultRole
//
//    public void setUIsDefaultRole(Boolean isDefaultRole)
//    {
//        UIsDefaultRole = isDefaultRole;
//    }
}
