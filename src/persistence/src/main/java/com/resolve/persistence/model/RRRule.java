package com.resolve.persistence.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.RRRuleFieldVO;
import com.resolve.services.hibernate.vo.RRRuleVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
@Entity
@Table(name = "rr_rule", indexes = {@Index(columnList = "u_schema_id", name = "rrr_u_schema_id_idx")})
public class RRRule extends BaseModel<RRRuleVO>
{
    private String rid;
    private List<RRRuleField> ridFields;
    private RRSchema schema;
    private String wikiId;
    private String runbookId;
    private String automationId;
    //for now there is no need to add many to wiki...it just want to refer to that wiki...not to do cascade update/delete
    private String wiki;
    private String runbook;
    private String automation;
    private String module;
    private String eventId;
    private Boolean active;
    private Boolean newWorksheetOnly;
    // RBA-14814 : Implementation - Create and Process Resolution Routing Rule for Security IR
    private Boolean sir;
    private String sirSource;
    private String sirTitle;
    private String sirType;
    private String sirPlaybook;
    private String sirSeverity;
    private String sirOwner;

    @Column(name = "u_rid", length = 4000)
    public String getRid()
    {
        return rid;
    }
    public void setRid(String rid)
    {
        this.rid = rid;
    }
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "rule", orphanRemoval=true, fetch = FetchType.EAGER)
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.ALL)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public List<RRRuleField> getRidFields()
    {
        return ridFields;
    }
    public void setRidFields(List<RRRuleField> ridFields)
    {
        this.ridFields = ridFields;
    }
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_schema_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public RRSchema getSchema()
    {
        return schema;
    }
    public void setSchema(RRSchema schema)
    {
        this.schema = schema;
    }
    @Column(name = "u_wiki_id", length = 32)
    public String getWikiId()
    {
        return wikiId;
    }
    public void setWikiId(String wikiId)
    {
        this.wikiId = wikiId;
    }
    @Column(name = "u_runbook_id", length = 32)
    public String getRunbookId()
    {
        return runbookId;
    }
    public void setRunbookId(String runbookId)
    {
        this.runbookId = runbookId;
    }
    @Column(name="u_automation_id", length=32)
    public String getAutomationId()
    {
        return automationId;
    }
    public void setAutomationId(String automationId)
    {
        this.automationId = automationId;
    }
    @Column(name = "u_new_worksheet", length = 200)
    public Boolean getNewWorksheetOnly()
    {
        return newWorksheetOnly;
    }
    public void setNewWorksheetOnly(Boolean newWorksheetOnly)
    {
        this.newWorksheetOnly = newWorksheetOnly;
    }
    @Column(name = "u_module", length = 300)
    public String getModule()
    {
        return module;
    }
    public void setModule(String module)
    {
        this.module = module;
    }
    @Column(name = "u_active", length = 200)
    public Boolean getActive()
    {
        return active;
    }
    public void setActive(Boolean active)
    {
        this.active = active;
    }
    @Column(name="u_event_id", length=200)
    public String getEventId()
    {
        return eventId;
    }
    public void setEventId(String eventId)
    {
        this.eventId = eventId;
    }
    @Column(name="u_wiki", length=200)
    public String getWiki()
    {
        return wiki;
    }

    public void setWiki(String wiki)
    {
        this.wiki = wiki;
    }
    @Column(name="u_runbook", length=200)
    public String getRunbook()
    {
        return runbook;
    }

    public void setRunbook(String runbook)
    {
        this.runbook = runbook;
    }
    @Column(name="u_automation", length=200)
    public String getAutomation()
    {
        return automation;
    }

    public void setAutomation(String automation)
    {
        this.automation = automation;
    }

    // RBA-14814 : Implementation - Create and Process Resolution Routing Rule for Security IR
    
    @Column(name="u_sir", length=200)
    public Boolean getSir() {
		return sir;
	}

	public void setSir(Boolean sir) {
		this.sir = sir;
	}

	@Column(name="u_sir_source", length=200)
	public String getSirSource() {
		return sirSource;
	}

	public void setSirSource(String sirSource) {
		this.sirSource = sirSource;
	}
	
	@Column(name="u_sir_title", length=200)
	public String getSirTitle() {
		return sirTitle;
	}

	public void setSirTitle(String sirTitle) {
		this.sirTitle = sirTitle;
	}

	@Column(name="u_sir_type", length=200)
	public String getSirType() {
		return sirType;
	}

	public void setSirType(String sirType) {
		this.sirType = sirType;
	}

	@Column(name="u_sir_playbook", length=200)
	public String getSirPlaybook() {
		return sirPlaybook;
	}

	public void setSirPlaybook(String sirPlaybook) {
		this.sirPlaybook = sirPlaybook;
	}

	@Column(name="u_sir_severity", length=200)
	public String getSirSeverity() {
		return sirSeverity;
	}

	public void setSirSeverity(String sirSeverity) {
		this.sirSeverity = sirSeverity;
	}
	
	@Column(name="u_sir_owner", length=200)
	public String getSirOwner() {
		return sirOwner;
	}

	public void setSirOwner(String sirOwner) {
		this.sirOwner = sirOwner;
	}

	@Override
    public RRRuleVO doGetVO()
    {
        RRRuleVO vo = new RRRuleVO();
        super.doGetBaseVO(vo);
        vo.setRid(getRid());
        vo.setActive(getActive());
        vo.setNewWorksheetOnly(getNewWorksheetOnly());
        vo.setModule(getModule());
        vo.setRunbookId(getRunbookId());
        vo.setSchema(this.getSchema()==null?null:this.getSchema().doGetVO());
        vo.setAutomationId(getAutomationId());
        vo.setWikiId(getWikiId());
        vo.setEventId(getEventId());
        vo.setWiki(getWiki());
        vo.setRunbook(getRunbook());
        vo.setAutomation(getAutomation());
        
        // RBA-14814 : Implementation - Create and Process Resolution Routing Rule for Security IR
        vo.setSir(getSir());
        vo.setSirSource(getSirSource());
        vo.setSirTitle(getSirTitle());
        vo.setSirType(getSirType());
        vo.setSirPlaybook(getSirPlaybook());
        vo.setSirSeverity(getSirSeverity());
        vo.setSirOwner(getSirOwner());
        
        if(getRidFields()==null||getRidFields().isEmpty())
            return vo;
        List<RRRuleFieldVO> fields = new ArrayList<RRRuleFieldVO>();
        for(RRRuleField field:getRidFields())
            fields.add(field.doGetVO());
        vo.setRidFields(fields);
        return vo;
    }
    public void applyVOToModelWithFieldOrdered(RRRuleVO vo) throws JsonProcessingException, IOException {
        this.applyVOToModel(vo);
        //this seems more reliable..so that the order is always decided by the schema entity from db;
        if(this.schema==null)
            return;
        ObjectMapper mapper = new ObjectMapper();
        JsonNode fields = mapper.readTree(schema.getJsonFields());
        for(JsonNode field:fields) 
            for(RRRuleField ridField:this.getRidFields())
                if(field.get("name").asText().equals(ridField.getName()))
                    ridField.setOrder(field.get("order").asInt());
    }
    @Override
    public void applyVOToModel(RRRuleVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            this.setRid(StringUtils.isNotBlank(vo.getRid()) && vo.getRid().equals(VO.STRING_DEFAULT) ? getRid() : vo.getRid());
            this.setActive(vo.getActive() != null ? vo.getActive() : getActive());
            this.setNewWorksheetOnly(vo.getNewWorksheetOnly() != null ? vo.getNewWorksheetOnly() : getNewWorksheetOnly());
            this.setModule(StringUtils.isNotBlank(vo.getModule()) && vo.getModule().equals(VO.STRING_DEFAULT) ? getModule() : vo.getModule());
            this.setWikiId(StringUtils.isNotBlank(vo.getWikiId()) && vo.getWikiId().equals(VO.STRING_DEFAULT) ? getWikiId() : vo.getWikiId());
            this.setRunbookId(StringUtils.isNotBlank(vo.getRunbookId()) && vo.getRunbookId().equals(VO.STRING_DEFAULT) ? getRunbookId() : vo.getRunbookId());
            this.setEventId(StringUtils.isNotBlank(vo.getEventId()) && vo.getEventId().equals(VO.STRING_DEFAULT) ? getEventId() : vo.getEventId());
            this.setWiki(StringUtils.isNotBlank(vo.getWiki()) && vo.getWiki().equals(VO.STRING_DEFAULT) ? getWiki() : vo.getWiki());
            this.setRunbook(StringUtils.isNotBlank(vo.getRunbook()) && vo.getRunbook().equals(VO.STRING_DEFAULT) ? getRunbook() : vo.getRunbook());
            this.setAutomation(StringUtils.isNotBlank(vo.getAutomation()) && vo.getAutomation().equals(VO.STRING_DEFAULT) ? getAutomation() : vo.getAutomation());
            
            // RBA-14814 : Implementation - Create and Process Resolution Routing Rule for Security IR
            this.setSir(vo.getSir() != null ? vo.getSir() : getSir());
            this.setSirSource(StringUtils.isNotBlank(vo.getSirSource()) && vo.getSirSource().equals(VO.STRING_DEFAULT) ? getSirSource() : vo.getSirSource());
            this.setSirTitle(StringUtils.isNotBlank(vo.getSirTitle()) && vo.getSirTitle().equals(VO.STRING_DEFAULT) ? getSirTitle() : vo.getSirTitle());
            this.setSirType(StringUtils.isNotBlank(vo.getSirType()) && vo.getSirType().equals(VO.STRING_DEFAULT) ? getSirType() : vo.getSirType());
            this.setSirPlaybook(StringUtils.isNotBlank(vo.getSirPlaybook()) && vo.getSirPlaybook().equals(VO.STRING_DEFAULT) ? getSirPlaybook() : vo.getSirPlaybook());
            this.setSirSeverity(StringUtils.isNotBlank(vo.getSirSeverity()) && vo.getSirSeverity().equals(VO.STRING_DEFAULT) ? getSirSeverity() : vo.getSirSeverity());
            this.setSirOwner(StringUtils.isNotBlank(vo.getSirOwner()) && vo.getSirOwner().equals(VO.STRING_DEFAULT) ? getSirOwner() : vo.getSirOwner());
            
            RRSchema schema = null;
            if(vo.getSchema()!=null) {
                schema = new RRSchema();
                schema.applyVOToModel(vo.getSchema());
            }
            this.setSchema(schema);
            this.setAutomationId(StringUtils.isNotBlank(vo.getAutomationId()) && vo.getAutomationId().equals(VO.STRING_DEFAULT) ? getAutomationId() : vo.getAutomationId());
            if(vo.getRidFields()==null||vo.getRidFields().isEmpty())
                return;
            //this is really annoying...
            //hibernate insert before delete...
            //they say by doing this it avoids violating some foreign key constraint...
            //so u either manually delete the sub entity and flush the session or resuse the persisted entity...
            //so i choose to reuse them...
            List<RRRuleFieldVO> fieldVOs = vo.getRidFields();
            List<RRRuleField> fields = this.getRidFields();
            List<RRRuleField> newFields = new ArrayList<RRRuleField>();
            
            for(RRRuleFieldVO fieldVO:fieldVOs) {
                RRRuleField field = null;
                for(RRRuleField f:fields)
                    if(fieldVO.getName().equals(f.getName())) {
                        field = f;
                        break;
                    }
                if(field!=null) {
                    field.setValue(fieldVO.getValue());
                    newFields.add(field);
                    continue;
                }
                //this will get hit when creating new rule
                newFields.add(new RRRuleField(
                                StringUtils.isEmpty(fieldVO.getId())||fieldVO.getId().equals(VO.STRING_DEFAULT)?null:fieldVO.getId(),
                                fieldVO.getName(),
                                fieldVO.getValue(),
                                fieldVO.getOrder(),this));
            }
            fields.clear();
            fields.addAll(newFields);
        }
    }
}
