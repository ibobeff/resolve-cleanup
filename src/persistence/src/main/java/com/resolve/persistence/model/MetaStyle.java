/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.resolve.services.hibernate.vo.MetaStyleVO;
import com.resolve.services.interfaces.VO;

//THIS IS DEPRECATED AND NOT USED ANYMORE
@Entity
@Table(name = "meta_style")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaStyle extends BaseModel<MetaStyleVO>
{
    private static final long serialVersionUID = 3471683075075115143L;
    
    private String UStyle;
    private String UValue;
    private String UFormValidation;

    public MetaStyle()
    {
    }
    
    public MetaStyle(MetaStyleVO vo)
    {
        applyVOToModel(vo);
    }

    @Lob
    @Column(name = "u_style", length = 16777215)
    public String getUStyle()
    {
        return this.UStyle;
    }

    public void setUStyle(String UStyle)
    {
        this.UStyle = UStyle;
    }
    
    @Lob
    @Column(name = "u_value", length = 16777215)
    public String getUValue()
    {
        return UValue;
    }
    
    public void setUValue(String UValue)
    {
        this.UValue = UValue;
    }
    
    
    @Lob
    @Column(name = "u_form_validation", length = 16777215)
    public String getUFormValidation()
    {
        return UFormValidation;
    }
    public void setUFormValidation(String uFormValidation)
    {
        UFormValidation = uFormValidation;
    }

    
    @Override
    public MetaStyleVO doGetVO()
    {
        MetaStyleVO vo = new MetaStyleVO();
        super.doGetBaseVO(vo);
        
        vo.setUFormValidation(getUFormValidation());
        vo.setUStyle(getUStyle());
        vo.setUValue(getUValue());
        
        return vo;
    }

    @Override
    public void applyVOToModel(MetaStyleVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);   
            
            this.setUFormValidation((String) VO.evaluateValue(vo.getUFormValidation(), getUFormValidation()));
            this.setUStyle((String) VO.evaluateValue(vo.getUStyle(), getUStyle()));
            this.setUValue((String) VO.evaluateValue(vo.getUValue(), getUValue()));
        }
        
    }
}

