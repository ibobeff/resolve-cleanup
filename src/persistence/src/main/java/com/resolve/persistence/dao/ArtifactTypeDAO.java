package com.resolve.persistence.dao;

import com.resolve.persistence.model.ArtifactType;

public interface ArtifactTypeDAO extends GenericDAO<ArtifactType, String> {}
