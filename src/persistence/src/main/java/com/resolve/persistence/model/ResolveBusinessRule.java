/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.resolve.services.hibernate.vo.ResolveBusinessRuleVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "resolve_business_rule")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveBusinessRule extends BaseModel<ResolveBusinessRuleVO>
{
    private static final long serialVersionUID = 7231581331453065519L;
    
    private String UScript;
    private Boolean UAsync; 
    private Boolean UBefore; 
    private Boolean UActive; 
    private String UType;
    private String UName;
    private Integer UOrder;
    private String UTable;
    
	// transient
	
    // object references
	
    // object referenced by
	
    public ResolveBusinessRule()
    {
    }
    
    public ResolveBusinessRule(ResolveBusinessRuleVO vo)
    {
        applyVOToModel(vo);
    }

    @Lob 
    @Column(name = "u_script", length = 16777215)
    public String getUScript()
    {
        return this.UScript;
    }

    public void setUScript(String UScript)
    {
        this.UScript = UScript;
    }

    @Column(name = "u_async")
    public Boolean getUAsync()
    {
        return this.UAsync;
    }
    public void setUAsync(Boolean UAsync)
    {
        this.UAsync = UAsync;
    }
    
    @Column(name = "u_before")
    public Boolean getUBefore()
    {
        return this.UBefore;
    }
    public void setUBefore(Boolean UBefore)
    {
        this.UBefore = UBefore;
    }
    
    @Column(name = "u_active")
    public Boolean getUActive()
    {
        return this.UActive;
    }
    public void setUActive(Boolean UActive)
    {
        this.UActive = UActive;
    }
    
    
    @Column(name = "u_type", length = 1023)
    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }

    @Column(name = "u_name", length = 1023)
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String name)
    {
        this.UName = name;
    }

    @Column(name = "u_order")
    public Integer getUOrder()
    {
        return this.UOrder;
    }

    public void setUOrder(Integer UOrder)
    {
        this.UOrder = UOrder;
    }

    @Column(name = "u_table", length = 1023)
    public String getUTable()
    {
        return this.UTable;
    }

    public void setUTable(String table)
    {
        this.UTable = table;
    }

    @Override
    public ResolveBusinessRuleVO doGetVO()
    {
        ResolveBusinessRuleVO vo = new ResolveBusinessRuleVO();
        super.doGetBaseVO(vo);
        
        vo.setUActive(getUActive());
        vo.setUAsync(getUAsync());
        vo.setUBefore(getUBefore());
        vo.setUName(getUName());
        vo.setUOrder(getUOrder());
        vo.setUScript(getUScript());
        vo.setUTable(getUTable());
        vo.setUType(getUType());
        
        return vo;
    }

    @Override
    public void applyVOToModel(ResolveBusinessRuleVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo); 

            this.setUActive(vo.getUActive() != null ? vo.getUActive() : getUActive());
            this.setUAsync(vo.getUAsync() != null ? vo.getUAsync() : getUAsync());
            this.setUBefore(vo.getUBefore() != null ? vo.getUBefore() : getUBefore());
            this.setUOrder(vo.getUOrder() != null && vo.getUOrder().equals(VO.INTEGER_DEFAULT) ? getUOrder() : vo.getUOrder());

            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUScript(StringUtils.isNotBlank(vo.getUScript()) && vo.getUScript().equals(VO.STRING_DEFAULT) ? getUScript() : vo.getUScript());
            this.setUTable(StringUtils.isNotBlank(vo.getUTable()) && vo.getUTable().equals(VO.STRING_DEFAULT) ? getUTable() : vo.getUTable());
            this.setUType(StringUtils.isNotBlank(vo.getUType()) && vo.getUType().equals(VO.STRING_DEFAULT) ? getUType() : vo.getUType());
        }
        
    }
}
