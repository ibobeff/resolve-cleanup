package com.resolve.persistence.dao;

import com.resolve.persistence.model.ArtifactConfiguration;

public interface ArtifactConfigurationDAO extends GenericDAO<ArtifactConfiguration, String> {}
