/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.List;

/**
 * A generic Interface to get the list of properties that are exported/imported as CDATA in xml files
 * 
 * @author jeet.marwah
 *
 */
public interface CdataProperty
{
	public List<String> ugetCdataProperty();
}
