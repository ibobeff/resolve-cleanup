package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.resolve.services.hibernate.vo.ArchiveSirNoteVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "archive_sir_note")
public class ArchiveSirNote extends BaseModel<ArchiveSirNoteVO>
{
	private static final long serialVersionUID = 3829788109015944715L;
	
	private String note;           // note content
	private String postedBy;       // user who posted the note
	private String activityId;     // id of the Activity which added this note
	private String activityName;   // name of the Activity which added this note
	private String componentId;    // id of the Wiki component which added this note
	private String incidentId;     // id of the incident to which this note belongs
	private String worksheetId;    // Worksheet id belongs to the incident to which this note belongs
	private String source;         // name of a source which created this note.
	private String sourceValue;    // the value of the source.
	private String title;
	private String sourceAndValue; // for internal use only
	
	@Lob
	@Column(name = "u_note", nullable = false, length = 16777215)
	public String getNote()
	{
		return note;
	}
	public void setNote(String note)
	{
		this.note = note;
	}
	
	@Column(name = "u_posted_by", nullable = false, length = 255)
	public String getPostedBy()
	{
		return postedBy;
	}
	public void setPostedBy(String postedBy)
	{
		this.postedBy = postedBy;
	}
	
	@Column(name = "u_activity_id", length = 32)
	public String getActivityId()
    {
        return activityId;
    }
    public void setActivityId(String activityId)
    {
        this.activityId = activityId;
    }
    
    @Column(name = "u_activity_name", length = 255)
    public String getActivityName()
    {
        return activityName;
    }
    public void setActivityName(String activityName)
    {
        this.activityName = activityName;
    }
    
    @Column(name = "u_component_id", length = 40)
    public String getComponentId()
    {
        return componentId;
    }
    public void setComponentId(String componentId)
    {
        this.componentId = componentId;
    }
    
    @Column(name = "u_incident_id", length = 32)
    public String getIncidentId()
    {
        return incidentId;
    }
    public void setIncidentId(String incidentId)
    {
        this.incidentId = incidentId;
    }
    
    @Column(name = "u_worksheet_id", length = 32)
    public String getWorksheetId()
    {
        return worksheetId;
    }
    public void setWorksheetId(String worksheetId)
    {
        this.worksheetId = worksheetId;
    }
	
    @Column(name = "u_source", length = 50)
	public String getSource()
    {
        return source;
    }
    public void setSource(String source)
    {
        this.source = source;
    }
    
    @Column(name = "u_source_value", length = 100)
    public String getSourceValue()
    {
        return sourceValue;
    }
    public void setSourceValue(String sourceValue)
    {
        this.sourceValue = sourceValue;
    }
    
    @Column(name = "u_title", length = 100)
    public String getTitle()
    {
        return title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }
    
    @Column(name = "u_source_and_value")
    public String getSourceAndValue()
    {
        return sourceAndValue;
    }
    public void setSourceAndValue(String sourceAndValue)
    {
        this.sourceAndValue = sourceAndValue;
    }
    
    @Override
	public ArchiveSirNoteVO doGetVO()
	{
		ArchiveSirNoteVO vo = new ArchiveSirNoteVO();
		super.doGetBaseVO(vo);
		
		vo.setNote(this.getNote());
		vo.setPostedBy(this.getPostedBy());
		vo.setActivityId(this.getActivityId());
		vo.setActivityName(this.getActivityName());
		vo.setComponentId(this.getComponentId());
		vo.setIncidentId(this.getIncidentId());
		vo.setWorksheetId(this.getWorksheetId());
		vo.setSource(this.getSource());
		vo.setSourceValue(this.getSourceValue());
		vo.setTitle(this.getTitle());
		vo.setSourceAndValue(this.getSourceAndValue());
		
		return vo;
	}
	
	@Override
	public void applyVOToModel(ArchiveSirNoteVO vo)
	{
		if (vo != null)
		{
			super.applyVOToModel(vo);
			
			this.setNote(StringUtils.isNotBlank(vo.getNote()) && vo.getNote().equals(VO.STRING_DEFAULT) ? getNote() : vo.getNote());
			this.setPostedBy(StringUtils.isNotBlank(vo.getPostedBy()) && vo.getPostedBy().equals(VO.STRING_DEFAULT) ? getPostedBy() : vo.getPostedBy());
			this.setActivityId(StringUtils.isNotBlank(vo.getActivityId()) && vo.getActivityId().equals(VO.STRING_DEFAULT) ? getActivityId() : vo.getActivityId());
			this.setActivityName(StringUtils.isNotBlank(vo.getActivityName()) && vo.getActivityName().equals(VO.STRING_DEFAULT) ? getActivityName() : vo.getActivityName());
			this.setComponentId(StringUtils.isNotBlank(vo.getComponentId()) && vo.getComponentId().equals(VO.STRING_DEFAULT) ? getComponentId() : vo.getComponentId());
			this.setIncidentId(StringUtils.isNotBlank(vo.getIncidentId()) && vo.getIncidentId().equals(VO.STRING_DEFAULT) ? getIncidentId() : vo.getIncidentId());
			this.setWorksheetId(StringUtils.isNotBlank(vo.getWorksheetId()) && vo.getWorksheetId().equals(VO.STRING_DEFAULT) ? getWorksheetId() : vo.getWorksheetId());
			this.setSource(StringUtils.isNotBlank(vo.getSource()) && vo.getSource().equals(VO.STRING_DEFAULT) ? getSource() : vo.getSource());
            this.setSourceValue(StringUtils.isNotBlank(vo.getSourceValue()) && vo.getSourceValue().equals(VO.STRING_DEFAULT) ? getSourceValue() : vo.getSourceValue());
            this.setTitle(StringUtils.isNotBlank(vo.getTitle()) && vo.getTitle().equals(VO.STRING_DEFAULT) ? getTitle() : vo.getTitle());
            this.setSourceAndValue(StringUtils.isNotBlank(vo.getSourceAndValue()) && vo.getSourceAndValue().equals(VO.STRING_DEFAULT) ? getSourceAndValue() : vo.getSourceAndValue());
		}
	}
}
