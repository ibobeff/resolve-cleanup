package com.resolve.persistence.dao;

import com.resolve.persistence.model.ResolveEdge;

public interface ResolveEdgeDAO extends GenericDAO<ResolveEdge, String>
{

}
