package com.resolve.persistence.util;

@FunctionalInterface
public interface HibernateMethod {

	public Object execute() throws Exception;
}
