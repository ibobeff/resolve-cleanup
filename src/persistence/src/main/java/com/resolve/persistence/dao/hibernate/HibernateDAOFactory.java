/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.dao.hibernate;

import java.util.concurrent.ConcurrentHashMap;

import com.resolve.persistence.dao.*;
import com.resolve.persistence.model.*;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class HibernateDAOFactory extends DAOFactory
{
    @SuppressWarnings("rawtypes")
    private ConcurrentHashMap<Class, GenericHibernateDAO> daoInstances = new ConcurrentHashMap<Class, GenericHibernateDAO>();

	@SuppressWarnings("rawtypes")
    private GenericHibernateDAO instantiateDAO(Class daoClass)
	{
		try
		{
			GenericHibernateDAO result = daoInstances.get(daoClass);
			if (result == null)
			{
				Log.log.debug("Instantiating DAO: " + daoClass);
				result = (GenericHibernateDAO) daoClass.newInstance();
				daoInstances.put(daoClass, result);
			}
			return result;
		}
		catch (Exception ex)
		{
			throw new RuntimeException("Can not instantiate DAO: " + daoClass, ex);
		}
	}

	// Inline concrete DAO implementations with no business-related data access
	// methods.
	// If we use public static nested classes, we can centralize all of them in
	// one source file.
	public static class AuditLogDAOHibernate extends GenericHibernateDAO<AuditLog, String> implements AuditLogDAO
	{

	}

	@Override
	public AuditLogDAO getAuditLogDAO()
	{
		return (AuditLogDAO) instantiateDAO(AuditLogDAOHibernate.class);
	}

	public static class AlertLogDAOHibernate extends GenericHibernateDAO<AlertLog, String> implements AlertLogDAO
	{

	}

	@Override
	public AlertLogDAO getAlertLogDAO()
	{
		return (AlertLogDAO) instantiateDAO(AlertLogDAOHibernate.class);
	}

	public static class GatewayLogDAOHibernate extends GenericHibernateDAO<GatewayLog, String> implements GatewayLogDAO
    {

    }

    @Override
    public GatewayLogDAO getGatewayLogDAO()
    {
        return (GatewayLogDAO) instantiateDAO(GatewayLogDAOHibernate.class);
    }

	public static class NetcoolFilterDAOHibernate extends GenericHibernateDAO<NetcoolFilter, String> implements NetcoolFilterDAO
	{

	}

	@Override
    public NetcoolFilterDAO getNetcoolFilterDAO()
	{
		return (NetcoolFilterDAO) instantiateDAO(NetcoolFilterDAOHibernate.class);
	}

	public static class ExchangeserverFilterDAOHibernate extends GenericHibernateDAO<ExchangeserverFilter, String> implements ExchangeserverFilterDAO
    {

    }

    @Override
    public ExchangeserverFilterDAO getExchangeserverFilterDAO()
    {
        return (ExchangeserverFilterDAO) instantiateDAO(ExchangeserverFilterDAOHibernate.class);
    }


	public static class EmailFilterDAOHibernate extends GenericHibernateDAO<EmailFilter, String> implements EmailFilterDAO
    {

    }

    @Override
    public EmailFilterDAO getEmailFilterDAO()
    {
        return (EmailFilterDAO) instantiateDAO(EmailFilterDAOHibernate.class);
    }

    public static class EmailAddressDAOHibernate extends GenericHibernateDAO<EmailAddress, String> implements EmailAddressDAO
    {

    }

    @Override
    public EmailAddressDAO getEmailAddressDAO()
    {
        return (EmailAddressDAO) instantiateDAO(EmailAddressDAOHibernate.class);
    }

    public static class DatabaseFilterDAOHibernate extends GenericHibernateDAO<DatabaseFilter, String> implements DatabaseFilterDAO
    {

    } // DatabaseFilterDAOHibernate

    @Override
    public DatabaseFilterDAO getDatabaseFilterDAO()
    {
        return (DatabaseFilterDAO) instantiateDAO(DatabaseFilterDAOHibernate.class);
    } // getDatabaseFilterDAO

    public static class DatabaseConnectionPoolDAOHibernate extends GenericHibernateDAO<DatabaseConnectionPool, String> implements DatabaseConnectionPoolDAO
    {

    } // DatabaseConnectionPoolDAOHibernate

    @Override
    public DatabaseConnectionPoolDAO getDatabaseConnectionPoolDAO()
    {
        return (DatabaseConnectionPoolDAO) instantiateDAO(DatabaseConnectionPoolDAOHibernate.class);
    } // getDatabaseConnectionPoolDAO

	public static class ResolveActionInvocDAOHibernate extends GenericHibernateDAO<ResolveActionInvoc, String> implements ResolveActionInvocDAO
	{

	}

	@Override
    public ResolveActionInvocDAO getResolveActionInvocDAO()
	{
		return (ResolveActionInvocDAO) instantiateDAO(ResolveActionInvocDAOHibernate.class);
	}

	public static class ResolveActionInvocOptionsDAOHibernate extends GenericHibernateDAO<ResolveActionInvocOptions, String> implements
			ResolveActionInvocOptionsDAO
	{

	}

	@Override
    public ResolveActionInvocOptionsDAO getResolveActionInvocOptionsDAO()
	{
		return (ResolveActionInvocOptionsDAO) instantiateDAO(ResolveActionInvocOptionsDAOHibernate.class);
	}

	public static class ResolveActionParameterDAOHibernate extends GenericHibernateDAO<ResolveActionParameter, String> implements ResolveActionParameterDAO
	{

	}

	@Override
    public ResolveActionParameterDAO getResolveActionParameterDAO()
	{
		return (ResolveActionParameterDAO) instantiateDAO(ResolveActionParameterDAOHibernate.class);
	}

	public static class ResolveTagDAOHibernate extends GenericHibernateDAO<ResolveTag, String> implements ResolveTagDAO
	{

	}

	@Override
    public ResolveTagDAO getResolveTagDAO()
	{
		return (ResolveTagDAO) instantiateDAO(ResolveTagDAOHibernate.class);
	}

	public static class ResolveActionPt2RelationDAOHibernate extends GenericHibernateDAO<ResolveActionPt2Relation, String> implements
			ResolveActionPt2RelationDAO
	{

	}

	@Override
    public ResolveActionPt2RelationDAO getResolveActionPt2RelationDAO()
	{
		return (ResolveActionPt2RelationDAO) instantiateDAO(ResolveActionPt2RelationDAOHibernate.class);
	}

	public static class ResolveActionTaskDAOHibernate extends GenericHibernateDAO<ResolveActionTask, String> implements ResolveActionTaskDAO
	{

	}

	@Override
    public ResolveActionTaskDAO getResolveActionTaskDAO()
	{
		return (ResolveActionTaskDAO) instantiateDAO(ResolveActionTaskDAOHibernate.class);
	}

    public static class ResolveActionTaskArchiveDAOHibernate extends GenericHibernateDAO<ActionTaskArchive, String> implements ResolveActionTaskArchiveDAO
    {

    }

    @Override
    public ResolveActionTaskArchiveDAO getResolveActionTaskArchiveDAO()
    {
        return (ResolveActionTaskArchiveDAO) instantiateDAO(ResolveActionTaskArchiveDAOHibernate.class);
    }
    
	public static class ResolveAssessDAOHibernate extends GenericHibernateDAO<ResolveAssess, String> implements ResolveAssessDAO
	{

	}

	@Override
    public ResolveAssessDAO getResolveAssessDAO()
	{
		return (ResolveAssessDAO) instantiateDAO(ResolveAssessDAOHibernate.class);
	}

	public static class ResolveCronDAOHibernate extends GenericHibernateDAO<ResolveCron, String> implements ResolveCronDAO
	{

	}

	@Override
    public ResolveCronDAO getResolveCronDAO()
	{
		return (ResolveCronDAO) instantiateDAO(ResolveCronDAOHibernate.class);
	}

	public static class ResolveEventDAOHibernate extends GenericHibernateDAO<ResolveEvent, String> implements ResolveEventDAO
	{

	}

	@Override
    public ResolveEventDAO getResolveEventDAO()
	{
		return (ResolveEventDAO) instantiateDAO(ResolveEventDAOHibernate.class);
	}

	public static class ResolveEventHandlerDAOHibernate extends GenericHibernateDAO<ResolveEventHandler, String> implements ResolveEventHandlerDAO
	{

	}

	@Override
    public ResolveEventHandlerDAO getResolveEventHandlerDAO()
	{
		return (ResolveEventHandlerDAO) instantiateDAO(ResolveEventHandlerDAOHibernate.class);
	}

	public static class ResolveExecuteDependencyDAOHibernate extends GenericHibernateDAO<ResolveExecuteDependency, String> implements
			ResolveExecuteDependencyDAO
	{

	}

	@Override
    public ResolveExecuteDependencyDAO getResolveExecuteDependencyDAO()
	{
		return (ResolveExecuteDependencyDAO) instantiateDAO(ResolveExecuteDependencyDAOHibernate.class);
	}

	public static class ResolveExecuteLogDAOHibernate extends GenericHibernateDAO<ResolveExecuteLog, String> implements ResolveExecuteLogDAO
	{

	}

	@Override
    public ResolveExecuteLogDAO getResolveExecuteLogDAO()
	{
		return (ResolveExecuteLogDAO) instantiateDAO(ResolveExecuteLogDAOHibernate.class);
	}

	public static class ResolveImpexGlideDAOHibernate extends GenericHibernateDAO<ResolveImpexGlide, String> implements ResolveImpexGlideDAO
	{

	}

	@Override
    public ResolveImpexGlideDAO getResolveImpexGlideDAO()
	{
		return (ResolveImpexGlideDAO) instantiateDAO(ResolveImpexGlideDAOHibernate.class);
	}

	public static class ResolveImpexModuleDAOHibernate extends GenericHibernateDAO<ResolveImpexModule, String> implements ResolveImpexModuleDAO
	{

	}

	@Override
    public ResolveImpexModuleDAO getResolveImpexModuleDAO()
	{
		return (ResolveImpexModuleDAO) instantiateDAO(ResolveImpexModuleDAOHibernate.class);
	}

	public static class ResolveImpexWikiDAOHibernate extends GenericHibernateDAO<ResolveImpexWiki, String> implements ResolveImpexWikiDAO
	{

	}

	@Override
    public ResolveImpexWikiDAO getResolveImpexWikiDAO()
	{
		return (ResolveImpexWikiDAO) instantiateDAO(ResolveImpexWikiDAOHibernate.class);
	}

	public static class ResolveParserDAOHibernate extends GenericHibernateDAO<ResolveParser, String> implements ResolveParserDAO
	{

	}

	@Override
    public ResolveParserDAO getResolveParserDAO()
	{
		return (ResolveParserDAO) instantiateDAO(ResolveParserDAOHibernate.class);
	}

	public static class ResolveParserTemplateDAOHibernate extends GenericHibernateDAO<ResolveParserTemplate, String> implements ResolveParserTemplateDAO
	{

	}

	@Override
    public ResolveParserTemplateDAO getResolveParserTemplateDAO()
	{
		return (ResolveParserTemplateDAO) instantiateDAO(ResolveParserTemplateDAOHibernate.class);
	}

	public static class ResolvePreprocessDAOHibernate extends GenericHibernateDAO<ResolvePreprocess, String> implements ResolvePreprocessDAO
	{

	}

	@Override
    public ResolvePreprocessDAO getResolvePreprocessDAO()
	{
		return (ResolvePreprocessDAO) instantiateDAO(ResolvePreprocessDAOHibernate.class);
	}

	public static class ResolvePropertiesDAOHibernate extends GenericHibernateDAO<ResolveProperties, String> implements ResolvePropertiesDAO
	{

	}

	@Override
    public ResolvePropertiesDAO getResolvePropertiesDAO()
	{
		return (ResolvePropertiesDAO) instantiateDAO(ResolvePropertiesDAOHibernate.class);
	}

	public static class ResolveRegistrationDAOHibernate extends GenericHibernateDAO<ResolveRegistration, String> implements ResolveRegistrationDAO
	{

	}

	@Override
    public ResolveRegistrationDAO getResolveRegistrationDAO()
	{
		return (ResolveRegistrationDAO) instantiateDAO(ResolveRegistrationDAOHibernate.class);
	}

	public static class ResolveRegistrationPropertyDAOHibernate extends GenericHibernateDAO<ResolveRegistrationProperty, String> implements
			ResolveRegistrationPropertyDAO
	{

	}
	@Override
    public ResolveRegistrationPropertyDAO getResolveRegistrationPropertyDAO()
	{
		return (ResolveRegistrationPropertyDAO) instantiateDAO(ResolveRegistrationPropertyDAOHibernate.class);
	}
	
    // MSG Gateway for SDK 2.0
    public static class MSGGatewayFilterDAOHibernate extends GenericHibernateDAO<MSGGatewayFilter, String> implements MSGGatewayFilterDAO
    {

    }
    @Override
    public MSGGatewayFilterDAO getMSGGatewayFilterDAO()
    {
        return (MSGGatewayFilterDAO) instantiateDAO(MSGGatewayFilterDAOHibernate.class);
    }
    
    public static class MSGGatewayFilterAttrDAOHibernate extends GenericHibernateDAO<MSGGatewayFilterAttr, String> implements
    MSGGatewayFilterAttrDAO
    {

    }
    @Override
    public MSGGatewayFilterAttrDAO getMSGGatewayFilterAttrDAO()
    {
        return (MSGGatewayFilterAttrDAO) instantiateDAO(MSGGatewayFilterAttrDAOHibernate.class);
    }
    
    // PULL Gateway for SDK 2.0
	public static class PullGatewayFilterDAOHibernate extends GenericHibernateDAO<PullGatewayFilter, String> implements
    PullGatewayFilterDAO
    {

    }
	@Override
    public PullGatewayFilterDAO getPullGatewayFilterDAO()
    {
		return (PullGatewayFilterDAO) instantiateDAO(PullGatewayFilterDAOHibernate.class);
    }
	
	public static class PullGatewayFilterAttrDAOHibernate extends GenericHibernateDAO<PullGatewayFilterAttr, String> implements
    PullGatewayFilterAttrDAO
    {

    }
    @Override
    public PullGatewayFilterAttrDAO getPullGatewayFilterAttrDAO()
    {
        return (PullGatewayFilterAttrDAO) instantiateDAO(PullGatewayFilterAttrDAOHibernate.class);
    }
	
	
    //DEC-32 PUSH Gateway for SDK 2.0
  	public static class PushGatewayFilterDAOHibernate extends GenericHibernateDAO<PushGatewayFilter, String> implements
      PushGatewayFilterDAO
      {

      }
  	@Override
      public PushGatewayFilterDAO getPushGatewayFilterDAO()
      {
          return (PushGatewayFilterDAO) instantiateDAO(PushGatewayFilterDAOHibernate.class);
      }
  	
  	public static class PushGatewayFilterAttrDAOHibernate extends GenericHibernateDAO<PushGatewayFilterAttr, String> implements
      PushGatewayFilterAttrDAO
      {

      }
      @Override
      public PushGatewayFilterAttrDAO getPushGatewayFilterAttrDAO()
      {
          return (PushGatewayFilterAttrDAO) instantiateDAO(PushGatewayFilterAttrDAOHibernate.class);
      }
  	
    
	public static class ResolveSessionDAOHibernate extends GenericHibernateDAO<ResolveSession, String> implements
	ResolveSessionDAO
	{

	}
	@Override
    public ResolveSessionDAO getResolveSessionDAO()
	{
		return (ResolveSessionDAO) instantiateDAO(ResolveSessionDAOHibernate.class);
	}

	public static class ResolveSysScriptDAOHibernate extends GenericHibernateDAO<ResolveSysScript, String> implements ResolveSysScriptDAO
	{

	}

	@Override
    public ResolveSysScriptDAO getResolveSysScriptDAO()
	{
		return (ResolveSysScriptDAO) instantiateDAO(ResolveSysScriptDAOHibernate.class);
	}

	public static class ResolveTriggerActionDAOHibernate extends GenericHibernateDAO<ResolveTriggerAction, String> implements ResolveTriggerActionDAO
	{

	}

	@Override
    public ResolveTriggerActionDAO getResolveTriggerActionDAO()
	{
		return (ResolveTriggerActionDAO) instantiateDAO(ResolveTriggerActionDAOHibernate.class);
	}

	public static class SysAppApplicationDAOHibernate extends GenericHibernateDAO<SysAppApplication, String> implements SysAppApplicationDAO
	{

	}

	@Override
    public SysAppApplicationDAO getSysAppApplicationDAO()
	{
		return (SysAppApplicationDAO) instantiateDAO(SysAppApplicationDAOHibernate.class);
	}

	public static class SysAppApplicationrolesDAOHibernate extends GenericHibernateDAO<SysAppApplicationroles, String> implements SysAppApplicationrolesDAO
	{

	}

	@Override
    public SysAppApplicationrolesDAO getSysAppApplicationrolesDAO()
	{
		return (SysAppApplicationrolesDAO) instantiateDAO(SysAppApplicationrolesDAOHibernate.class);
	}

	public static class SysAppModuleDAOHibernate extends GenericHibernateDAO<SysAppModule, String> implements SysAppModuleDAO
	{

	}

	@Override
    public SysAppModuleDAO getSysAppModuleDAO()
	{
		return (SysAppModuleDAO) instantiateDAO(SysAppModuleDAOHibernate.class);
	}

	public static class SysAppModulerolesDAOHibernate extends GenericHibernateDAO<SysAppModuleroles, String> implements SysAppModulerolesDAO
	{

	}

	@Override
    public SysAppModulerolesDAO getSysAppModulerolesDAO()
	{
		return (SysAppModulerolesDAO) instantiateDAO(SysAppModulerolesDAOHibernate.class);
	}

	public static class SysPerspectiveapplicationsDAOHibernate extends GenericHibernateDAO<SysPerspectiveapplications, String> implements SysPerspectiveapplicationsDAO
	{

	}

	@Override
    public SysPerspectiveapplicationsDAO getSysPerspectiveapplicationsDAO()
	{
		return (SysPerspectiveapplicationsDAO) instantiateDAO(SysPerspectiveapplicationsDAOHibernate.class);
	}

	public static class SysPerspectiveDAOHibernate extends GenericHibernateDAO<SysPerspective, String> implements SysPerspectiveDAO
	{

	}

	@Override
    public SysPerspectiveDAO getSysPerspectiveDAO()
	{
		return (SysPerspectiveDAO) instantiateDAO(SysPerspectiveDAOHibernate.class);
	}

	public static class SysPerspectiverolesDAOHibernate extends GenericHibernateDAO<SysPerspectiveroles, String> implements SysPerspectiverolesDAO
	{

	}

	@Override
    public SysPerspectiverolesDAO getSysPerspectiverolesDAO()
	{
		return (SysPerspectiverolesDAO) instantiateDAO(SysPerspectiverolesDAOHibernate.class);
	}

	public static class UsersDAOHibernate extends GenericHibernateDAO<Users, String> implements UsersDAO
	{

	}

	@Override
    public UsersDAO getUsersDAO()
	{
		return (UsersDAO) instantiateDAO(UsersDAOHibernate.class);
	}

	public static class AccessRightsDAOHibernate extends GenericHibernateDAO<AccessRights, String> implements AccessRightsDAO
	{

	}

	@Override
	public AccessRightsDAO getAccessRightsDAO()
	{
		return (AccessRightsDAO) instantiateDAO(AccessRightsDAOHibernate.class);
	}


	public static class GroupRoleRelDAOHibernate extends GenericHibernateDAO<GroupRoleRel, String> implements GroupRoleRelDAO
	{

	}

	@Override
	public GroupRoleRelDAO getGroupRoleRelDAO()
	{
		return (GroupRoleRelDAO) instantiateDAO(GroupRoleRelDAOHibernate.class);
	}

	public static class GroupsDAOHibernate extends GenericHibernateDAO<Groups, String> implements GroupsDAO
	{

	}

	@Override
	public GroupsDAO getGroupsDAO()
	{
		return (GroupsDAO) instantiateDAO(GroupsDAOHibernate.class);
	}

	public static class RolesDAOHibernate extends GenericHibernateDAO<Roles, String> implements RolesDAO
	{

	}

	@Override
	public RolesDAO getRolesDAO()
	{
		return (RolesDAO) instantiateDAO(RolesDAOHibernate.class);
	}

	public static class UserRoleRelDAOHibernate extends GenericHibernateDAO<UserRoleRel, String> implements UserRoleRelDAO
	{

	}

	@Override
	public UserRoleRelDAO getUserRoleRelDAO()
	{
		return (UserRoleRelDAO) instantiateDAO(UserRoleRelDAOHibernate.class);
	}

	public static class UserGroupRelDAOHibernate extends GenericHibernateDAO<UserGroupRel, String> implements UserGroupRelDAO
	{

	}

	@Override
	public UserGroupRelDAO getUserGroupRelDAO()
	{
		return (UserGroupRelDAO) instantiateDAO(UserGroupRelDAOHibernate.class);
	}

	public static class WikiAttachmentDAOHibernate extends GenericHibernateDAO<WikiAttachment, String> implements WikiAttachmentDAO
	{

	}

	@Override
	public WikiAttachmentDAO getWikiAttachmentDAO()
	{
		return (WikiAttachmentDAO) instantiateDAO(WikiAttachmentDAOHibernate.class);
	}

	public static class WikiAttachmentContentDAOHibernate extends GenericHibernateDAO<WikiAttachmentContent, String> implements WikiAttachmentContentDAO
	{

	}

	@Override
	public WikiAttachmentContentDAO getWikiAttachmentContentDAO()
	{
		return (WikiAttachmentContentDAO) instantiateDAO(WikiAttachmentContentDAOHibernate.class);
	}

	public static class WikiDocumentDAOHibernate extends GenericHibernateDAO<WikiDocument, String> implements WikiDocumentDAO
	{
	    // WikiDocument playbook (templates or SIR) once created cannot be deleted or replicated if has at least one reference.
	    
		public void delete(WikiDocument entity) {
			if ((StringUtils.isNotBlank(entity.getUDisplayMode())
					&& entity.getUDisplayMode().equalsIgnoreCase(WikiDocument.DISPLAY_MODE_PLAYBOOK))
					&& entity.ugetUSIRRefCount().longValue() > 0) {
				StringBuilder sb = new StringBuilder();

				sb.append(entity.getUFullname() + " is ").append(WikiDocument.DISPLAY_MODE_PLAYBOOK)
						.append(" template referenced in playbook(s) and hence cannot be deleted.");

				Log.log.info(sb.toString());
				throw new RuntimeException(sb.toString());
			} else {
				super.delete(entity);
			}
		}
	    
	    public void replicate(WikiDocument entity)
	    {
	        if (((StringUtils.isBlank(entity.getUDisplayMode()) ||
	              !entity.getUDisplayMode().equalsIgnoreCase(WikiDocument.DISPLAY_MODE_PLAYBOOK)) &&
                  entity.ugetUSIRRefCount().longValue() == 0) ||
	             (StringUtils.isNotBlank(entity.getUDisplayMode()) &&
	              entity.getUDisplayMode().equalsIgnoreCase(WikiDocument.DISPLAY_MODE_PLAYBOOK) &&
	              StringUtils.isNotBlank(entity.getSys_id()) && StringUtils.isNotBlank(entity.getUImpexSysId()) &&
	              entity.getSys_id().equals(entity.getUImpexSysId())))
            {
                super.replicate(entity);
            }
	        else
            {
                StringBuilder sb = new StringBuilder();
                
                sb.append(entity.getUFullname() + " is ");
                
                if (StringUtils.isBlank(entity.getUDisplayMode()) &&
                    entity.getUDisplayMode().equalsIgnoreCase(WikiDocument.DISPLAY_MODE_PLAYBOOK))
                {
                    sb.append(WikiDocument.DISPLAY_MODE_PLAYBOOK);
                    
                    if (entity.ugetUIsTemplate())
                    {
                        sb.append(" template ");
                    }
                }
                else if (entity.ugetUSIRRefCount().longValue() > 0)
                {
                    sb.append(" referenced in playbook(s) ");
                }
                
                sb.append(" and hence cannot be replicated.");
                
                Log.log.info(sb.toString());
                throw new RuntimeException(sb.toString());
            }
	    }
	}

	@Override
	public WikiDocumentDAO getWikiDocumentDAO()
	{
		return (WikiDocumentDAO) instantiateDAO(WikiDocumentDAOHibernate.class);
	}

	public static class WikiArchiveDAOHibernate extends GenericHibernateDAO<WikiArchive, String> implements WikiArchiveDAO
	{

	}

	@Override
	public WikiArchiveDAO getWikiArchiveDAO()
	{
		return (WikiArchiveDAO) instantiateDAO(WikiArchiveDAOHibernate.class);
	}


	public static class ResolveArchiveDAOHibernate extends GenericHibernateDAO<ResolveArchive, String> implements ResolveArchiveDAO
	{

	}

	@Override
	public ResolveArchiveDAO getResolveArchiveDAO()
	{
		return (ResolveArchiveDAO) instantiateDAO(ResolveArchiveDAOHibernate.class);
	}



	public static class PropertiesDAOHibernate extends GenericHibernateDAO<Properties, String> implements PropertiesDAO
	{

	}

	@Override
	public PropertiesDAO getPropertiesDAO()
	{
		return (PropertiesDAO) instantiateDAO(PropertiesDAOHibernate.class);
	}

	public static class ResolveSequenceGeneratorDAOHibernate extends GenericHibernateDAO<ResolveSequenceGenerator, String> implements ResolveSequenceGeneratorDAO
	{

	}
	@Override
    public ResolveSequenceGeneratorDAO getResolveSequenceGeneratorDAO()
	{
		return (ResolveSequenceGeneratorDAO) instantiateDAO(ResolveSequenceGeneratorDAOHibernate.class);
	}

	public static class ContentWikidocWikidocRelDAOHibernate extends GenericHibernateDAO<ContentWikidocWikidocRel, String> implements ContentWikidocWikidocRelDAO{}

	@Override
	public ContentWikidocWikidocRelDAO getContentWikidocWikidocRelDAO()
	{
		return (ContentWikidocWikidocRelDAO) instantiateDAO(ContentWikidocWikidocRelDAOHibernate.class);
	}

	public static class MainWikidocWikidocRelDAOHibernate extends GenericHibernateDAO<MainWikidocWikidocRel, String> implements MainWikidocWikidocRelDAO{}

	@Override
	public MainWikidocWikidocRelDAO getMainWikidocWikidocRelDAO()
	{
		return (MainWikidocWikidocRelDAO) instantiateDAO(MainWikidocWikidocRelDAOHibernate.class);
	}

	public static class ExceptionWikidocWikidocRelDAOHibernate extends GenericHibernateDAO<ExceptionWikidocWikidocRel, String> implements ExceptionWikidocWikidocRelDAO{}

	@Override
	public ExceptionWikidocWikidocRelDAO getExceptionWikidocWikidocRelDAO()
	{
		return (ExceptionWikidocWikidocRelDAO) instantiateDAO(ExceptionWikidocWikidocRelDAOHibernate.class);
	}

	public static class FinalWikidocWikidocRelDAOHibernate extends GenericHibernateDAO<FinalWikidocWikidocRel, String> implements FinalWikidocWikidocRelDAO{}

	@Override
	public FinalWikidocWikidocRelDAO getFinalWikidocWikidocRelDAO()
	{
		return (FinalWikidocWikidocRelDAO) instantiateDAO(FinalWikidocWikidocRelDAOHibernate.class);
	}

	public static class DTWikidocWikidocRelDAOHibernate extends GenericHibernateDAO<DTWikidocWikidocRel, String> implements DTWikidocWikidocRelDAO{}

    @Override
    public DTWikidocWikidocRelDAO getDTWikidocWikidocRelDAO()
    {
        return (DTWikidocWikidocRelDAO) instantiateDAO(DTWikidocWikidocRelDAOHibernate.class);
    }

	public static class ContentWikidocActiontaskRelDAOHibernate extends GenericHibernateDAO<ContentWikidocActiontaskRel, String> implements ContentWikidocActiontaskRelDAO{}

	@Override
	public ContentWikidocActiontaskRelDAO getContentWikidocActiontaskRelDAO()
	{
		return (ContentWikidocActiontaskRelDAO) instantiateDAO(ContentWikidocActiontaskRelDAOHibernate.class);
	}

	public static class MainWikidocActiontaskRelDAOHibernate extends GenericHibernateDAO<MainWikidocActiontaskRel, String> implements MainWikidocActiontaskRelDAO{}

	@Override
	public MainWikidocActiontaskRelDAO getMainWikidocActiontaskRelDAO()
	{
		return (MainWikidocActiontaskRelDAO) instantiateDAO(MainWikidocActiontaskRelDAOHibernate.class);
	}

	public static class ExceptionWikidocActiontaskRelDAOHibernate extends GenericHibernateDAO<ExceptionWikidocActiontaskRel, String> implements ExceptionWikidocActiontaskRelDAO{}

	@Override
	public ExceptionWikidocActiontaskRelDAO getExceptionWikidocActiontaskRelDAO()
	{
		return (ExceptionWikidocActiontaskRelDAO) instantiateDAO(ExceptionWikidocActiontaskRelDAOHibernate.class);
	}

	public static class FinalWikidocActiontaskRelDAOHibernate extends GenericHibernateDAO<FinalWikidocActiontaskRel, String> implements FinalWikidocActiontaskRelDAO{}

	@Override
	public FinalWikidocActiontaskRelDAO getFinalWikidocActiontaskRelDAO()
	{
		return (FinalWikidocActiontaskRelDAO) instantiateDAO(FinalWikidocActiontaskRelDAOHibernate.class);
	}

	public static class ResolveActionTaskResolveTagDAOHibernate extends GenericHibernateDAO<ResolveActionTaskResolveTagRel, String> implements ResolveActionTaskResolveTagRelDAO{}

	@Override
	public ResolveActionTaskResolveTagRelDAO getResolveActionTaskResolveTagRelDAO()
	{
		return (ResolveActionTaskResolveTagRelDAO) instantiateDAO(ResolveActionTaskResolveTagDAOHibernate.class);
	}

	public static class WikidocResolveTagRelDAOHibernate extends GenericHibernateDAO<WikidocResolveTagRel, String> implements WikidocResolveTagRelDAO{}

	@Override
	public WikidocResolveTagRelDAO getWikidocResolveTagRelDAO()
	{
		return (WikidocResolveTagRelDAO) instantiateDAO(WikidocResolveTagRelDAOHibernate.class);
	}

	public static class WikidocStatisticsDAOHibernate extends GenericHibernateDAO<WikidocStatistics, String> implements WikidocStatisticsDAO{}

	@Override
	public WikidocStatisticsDAO getWikidocStatisticsDAO()
	{
		return (WikidocStatisticsDAO) instantiateDAO(WikidocStatisticsDAOHibernate.class);
	}

	public static class ResolveBusinessRuleDAOHibernate extends GenericHibernateDAO<ResolveBusinessRule, String> implements ResolveBusinessRuleDAO
	{

	}
	@Override
	public ResolveBusinessRuleDAO getResolveBusinessRuleDAO()
	{
		return (ResolveBusinessRuleDAO) instantiateDAO(ResolveBusinessRuleDAOHibernate.class);
	}

	public static class RoleWikidocHomepageRelDAOHibernate extends GenericHibernateDAO<RoleWikidocHomepageRel, String> implements RoleWikidocHomepageRelDAO
	{

	}
	@Override
	public RoleWikidocHomepageRelDAO getRoleWikidocHomepageRelDAO()
	{
		return (RoleWikidocHomepageRelDAO) instantiateDAO(RoleWikidocHomepageRelDAOHibernate.class);
	}


	public static class WikidocAttachmentRelDAOHibernate extends GenericHibernateDAO<WikidocAttachmentRel, String> implements WikidocAttachmentRelDAO
	{

	}
	@Override
	public WikidocAttachmentRelDAO getWikidocAttachmentRelDAO()
	{
		return (WikidocAttachmentRelDAO) instantiateDAO(WikidocAttachmentRelDAOHibernate.class);
	}

	public static class ResolveWikiLookupDAOHibernate extends GenericHibernateDAO<ResolveWikiLookup, String> implements ResolveWikiLookupDAO
	{

	}

    @Override
    public ResolveWikiLookupDAO getResolveWikiLookupDAO()
    {
		return (ResolveWikiLookupDAO) instantiateDAO(ResolveWikiLookupDAOHibernate.class);
    }

    public static class MetaAccessRightsDAOHibernate extends GenericHibernateDAO<MetaAccessRights, String> implements MetaAccessRightsDAO
    {

    }

    @Override
    public MetaAccessRightsDAO getMetaAccessRightsDAO()
    {
        return (MetaAccessRightsDAO) instantiateDAO(MetaAccessRightsDAOHibernate.class);
    }

    public static class MetaStyleDAOHibernate extends GenericHibernateDAO<MetaStyle, String> implements MetaStyleDAO
    {

    }

    @Override
    public MetaStyleDAO getMetaStyleDAO()
    {
        return (MetaStyleDAO) instantiateDAO(MetaStyleDAOHibernate.class);
    }

    public static class CustomTableDAOHibernate extends GenericHibernateDAO<CustomTable, String> implements CustomTableDAO
    {

    }

    @Override
    public CustomTableDAO getCustomTableDAO()
    {
        return (CustomTableDAO) instantiateDAO(CustomTableDAOHibernate.class);
    }

    public static class MetaTableDAOHibernate extends GenericHibernateDAO<MetaTable, String> implements MetaTableDAO
    {

    }

    @Override
    public MetaTableDAO getMetaTableDAO()
    {
        return (MetaTableDAO) instantiateDAO(MetaTableDAOHibernate.class);
    }

    public static class MetaFilterDAOHibernate extends GenericHibernateDAO<MetaFilter, String> implements MetaFilterDAO
    {

    }

    @Override
    public MetaFilterDAO getMetaFilterDAO()
    {
        return (MetaFilterDAO) instantiateDAO(MetaFilterDAOHibernate.class);
    }

    public static class MetaTableViewDAOHibernate extends GenericHibernateDAO<MetaTableView, String> implements MetaTableViewDAO
    {

    }

    @Override
    public MetaTableViewDAO getMetaTableViewDAO()
    {
        return (MetaTableViewDAO) instantiateDAO(MetaTableViewDAOHibernate.class);
    }

    public static class MetaControlDAOHibernate extends GenericHibernateDAO<MetaControl, String> implements MetaControlDAO
    {

    }

    @Override
    public MetaControlDAO getMetaControlDAO()
    {
        return (MetaControlDAO) instantiateDAO(MetaControlDAOHibernate.class);
    }

    public static class MetaControlItemDAOHibernate extends GenericHibernateDAO<MetaControlItem, String> implements MetaControlItemDAO
    {

    }

    @Override
    public MetaControlItemDAO getMetaControlItemDAO()
    {
        return (MetaControlItemDAO) instantiateDAO(MetaControlItemDAOHibernate.class);
    }

    public static class MetaTableViewFieldDAOHibernate extends GenericHibernateDAO<MetaTableViewField, String> implements MetaTableViewFieldDAO
    {

    }

    @Override
    public MetaTableViewFieldDAO getMetaTableViewFieldDAO()
    {
        return (MetaTableViewFieldDAO) instantiateDAO(MetaTableViewFieldDAOHibernate.class);
    }

    public static class MetaViewFieldDAOHibernate extends GenericHibernateDAO<MetaViewField, String> implements MetaViewFieldDAO
    {

    }

    @Override
    public MetaViewFieldDAO getMetaViewFieldDAO()
    {
        return (MetaViewFieldDAO) instantiateDAO(MetaViewFieldDAOHibernate.class);
    }

    public static class MetaFieldDAOHibernate extends GenericHibernateDAO<MetaField, String> implements MetaFieldDAO
    {

    }

    @Override
    public MetaFieldDAO getMetaFieldDAO()
    {
        return (MetaFieldDAO) instantiateDAO(MetaFieldDAOHibernate.class);
    }

    public static class MetaFieldPropertiesDAOHibernate extends GenericHibernateDAO<MetaFieldProperties, String> implements MetaFieldPropertiesDAO
    {

    }

    @Override
    public MetaFieldPropertiesDAO getMetaFieldPropertiesDAO()
    {
        return (MetaFieldPropertiesDAO) instantiateDAO(MetaFieldPropertiesDAOHibernate.class);
    }

    public static class MetaFormViewDAOHibernate extends GenericHibernateDAO<MetaFormView, String> implements MetaFormViewDAO
    {

    }

    @Override
    public MetaFormViewDAO getMetaFormViewDAO()
    {
        return (MetaFormViewDAO) instantiateDAO(MetaFormViewDAOHibernate.class);
    }

    public static class MetaFormActionDAOHibernate extends GenericHibernateDAO<MetaFormAction, String> implements MetaFormActionDAO
    {

    }

    @Override
    public MetaFormViewPropertiesDAO getMetaFormViewPropertiesDAO()
    {
        return (MetaFormViewPropertiesDAO) instantiateDAO(MetaFormViewPropertiesDAOHibernate.class);
    }

    public static class MetaFormViewPropertiesDAOHibernate extends GenericHibernateDAO<MetaFormViewProperties, String> implements MetaFormViewPropertiesDAO
    {

    }

    @Override
    public MetaFormActionDAO getMetaFormActionDAO()
    {
        return (MetaFormActionDAO) instantiateDAO(MetaFormActionDAOHibernate.class);
    }

    public static class MetaFormTabDAOHibernate extends GenericHibernateDAO<MetaFormTab, String> implements MetaFormTabDAO
    {

    }

    @Override
    public MetaFormTabDAO getMetaFormTabDAO()
    {
        return (MetaFormTabDAO) instantiateDAO(MetaFormTabDAOHibernate.class);
    }

    public static class MetaFormTabFieldDAOHibernate extends GenericHibernateDAO<MetaFormTabField, String> implements MetaFormTabFieldDAO
    {

    }

    @Override
    public MetaFormTabFieldDAO getMetaFormTabFieldDAO()
    {
        return (MetaFormTabFieldDAO) instantiateDAO(MetaFormTabFieldDAOHibernate.class);
    }

    public static class MetaViewLookupDAOHibernate extends GenericHibernateDAO<MetaViewLookup, String> implements MetaViewLookupDAO
    {

    }

    @Override
    public MetaViewLookupDAO getMetaViewLookupDAO()
    {
        return (MetaViewLookupDAO) instantiateDAO(MetaViewLookupDAOHibernate.class);
    }



	public static class WikidocRatingResolutionDAOHibernate extends GenericHibernateDAO<WikidocResolutionRating, String> implements WikidocResolutionRatingDAO
	{

	}
    @Override
    public WikidocResolutionRatingDAO getWikidocResolutionRatingDAO()
    {
		return (WikidocResolutionRatingDAO) instantiateDAO(WikidocRatingResolutionDAOHibernate.class);
    }

    public static class WikidocRatingQualityDAOHibernate extends GenericHibernateDAO<WikidocQualityRating, String> implements WikidocQualityRatingDAO
    {

    }
    @Override
    public WikidocQualityRatingDAO getWikidocQualityRatingDAO()
    {
        return (WikidocQualityRatingDAO) instantiateDAO(WikidocRatingQualityDAOHibernate.class);
    }

    public static class ResolveIndexLogDAOHibernate extends GenericHibernateDAO<ResolveIndexLog, String> implements ResolveIndexLogDAO
    {

    }
    @Override
    public ResolveIndexLogDAO getResolveIndexLogDAO()
    {
        return (ResolveIndexLogDAO) instantiateDAO(ResolveIndexLogDAOHibernate.class);
    }

    public static class MetaSourceDAOHibernate extends GenericHibernateDAO<MetaSource, String> implements MetaSourceDAO
    {

    }
    @Override
    public MetaSourceDAO getMetaSourceDAO()
    {
        return (MetaSourceDAO) instantiateDAO(MetaSourceDAOHibernate.class);
    }

    public static class ArchiveActionResultDAOHibernate extends GenericHibernateDAO<ArchiveActionResult, String> implements ArchiveActionResultDAO
    {

    }

    @Override
    public ArchiveActionResultDAO getArchiveActionResultDAO()
    {
        return (ArchiveActionResultDAO) instantiateDAO(ArchiveActionResultDAOHibernate.class);
    }
    
    public static class ArchiveActionResultDAOLobHibernate extends GenericHibernateDAO<ArchiveActionResultLob, String> implements ArchiveActionResultLobDAO
    {

    }
    @Override
    public ArchiveActionResultLobDAO getArchiveActionResultLobDAO()
    {
        return (ArchiveActionResultLobDAO) instantiateDAO(ArchiveActionResultDAOLobHibernate.class);
    }

    public static class ArchiveExecuteDependencyDAOHibernate extends GenericHibernateDAO<ArchiveExecuteDependency, String> implements ArchiveExecuteDependencyDAO
    {

    }

    @Override
    public ArchiveExecuteDependencyDAO getArchiveExecuteDependencyDAO()
    {
        return (ArchiveExecuteDependencyDAO) instantiateDAO(ArchiveExecuteDependencyDAOHibernate.class);
    }

    public static class ArchiveExecuteRequestDAOHibernate extends GenericHibernateDAO<ArchiveExecuteRequest, String> implements ArchiveExecuteRequestDAO
    {

    }

    @Override
    public ArchiveExecuteRequestDAO getArchiveExecuteRequestDAO()
    {
        return (ArchiveExecuteRequestDAO) instantiateDAO(ArchiveExecuteRequestDAOHibernate.class);
    }

    public static class ArchiveExecuteResultDAOHibernate extends GenericHibernateDAO<ArchiveExecuteResult, String> implements ArchiveExecuteResultDAO
    {

    }

    @Override
    public ArchiveExecuteResultDAO getArchiveExecuteResultDAO()
    {
        return (ArchiveExecuteResultDAO) instantiateDAO(ArchiveExecuteResultDAOHibernate.class);
    }

    public static class ArchiveProcessRequestDAOHibernate extends GenericHibernateDAO<ArchiveProcessRequest, String> implements ArchiveProcessRequestDAO
    {

    }

    @Override
    public ArchiveProcessRequestDAO getArchiveProcessRequestDAO()
    {
        return (ArchiveProcessRequestDAO) instantiateDAO(ArchiveProcessRequestDAOHibernate.class);
    }

    public static class ArchiveWorksheetDAOHibernate extends GenericHibernateDAO<ArchiveWorksheet, String> implements ArchiveWorksheetDAO
    {

    }

    @Override
    public ArchiveWorksheetDAO getArchiveWorksheetDAO()
    {
        return (ArchiveWorksheetDAO) instantiateDAO(ArchiveWorksheetDAOHibernate.class);
    }

    public static class ArchiveWorksheetDebugDAOHibernate extends GenericHibernateDAO<ArchiveWorksheetDebug, String> implements ArchiveWorksheetDebugDAO
    {

    }

    @Override
    public ArchiveWorksheetDebugDAO getArchiveWorksheetDebugDAO()
    {
        return (ArchiveWorksheetDebugDAO) instantiateDAO(ArchiveWorksheetDebugDAOHibernate.class);
    }

    public static class custom_table_parent_exampleDAOHibernate extends GenericHibernateDAO<custom_table_parent_example, String> implements custom_table_parent_exampleDAO
    {

    }
    @Override
    public custom_table_parent_exampleDAO getcustom_table_parent_exampleDAO()
    {
        return (custom_table_parent_exampleDAO) instantiateDAO(custom_table_parent_exampleDAOHibernate.class);
    }

    public static class ServiceNowFilterDAOHibernate extends GenericHibernateDAO<ServiceNowFilter, String> implements ServiceNowFilterDAO
    {

    }

    @Override
    public ServiceNowFilterDAO getServiceNowFilterDAO()
    {
        return (ServiceNowFilterDAO) instantiateDAO(ServiceNowFilterDAOHibernate.class);
    }

    public static class TSRMFilterDAOHibernate extends GenericHibernateDAO<TSRMFilter, String> implements TSRMFilterDAO
    {

    }

    @Override
    public TSRMFilterDAO getTSRMFilterDAO()
    {
        return (TSRMFilterDAO) instantiateDAO(TSRMFilterDAOHibernate.class);
    }

    public static class SocialPostAttachmentDAOHibernate extends GenericHibernateDAO<SocialPostAttachment, String> implements SocialPostAttachmentDAO
    {

    }

    @Override
    public SocialPostAttachmentDAO getSocialPostAttachmentDAO()
    {
        return (SocialPostAttachmentDAO) instantiateDAO(SocialPostAttachmentDAOHibernate.class);
    }

    public static class CatalogAttachmentDAOHibernate extends GenericHibernateDAO<CatalogAttachment, String> implements CatalogAttachmentDAO{}

    @Override
    public CatalogAttachmentDAO getCatalogAttachmentDAO()
    {
        return (CatalogAttachmentDAO) instantiateDAO(CatalogAttachmentDAOHibernate.class);
    }

    public static class PostSyncDAOHibernate extends GenericHibernateDAO<PostSync, String> implements PostSyncDAO
    {

    }

    @Override
    public PostSyncDAO getPostSyncDAO()
    {
        return (PostSyncDAO) instantiateDAO(PostSyncDAOHibernate.class);
    }

    public static class ResolveImpexLogDAOHibernate extends GenericHibernateDAO<ResolveImpexLog, String> implements ResolveImpexLogDAO
    {

    }

    @Override
    public ResolveImpexLogDAO getResolveImpexLogDAO()
    {
        return (ResolveImpexLogDAO) instantiateDAO(ResolveImpexLogDAOHibernate.class);
    }
    
    public static class ResolveImpexManifestDAOHibernate extends GenericHibernateDAO<ResolveImpexManifest, String> implements ResolveImpexManifestDAO
    {

    }

    @Override
    public ResolveImpexManifestDAO getResolveImpexManifestDAO()
    {
        return (ResolveImpexManifestDAO) instantiateDAO(ResolveImpexManifestDAOHibernate.class);
    }

    public static class SocialPreferencesDAOHibernate extends GenericHibernateDAO<UserPreferences, String> implements UserPreferencesDAO
    {

    }

    @Override
    public UserPreferencesDAO getSocialPreferencesDAO()
    {
        return (UserPreferencesDAO) instantiateDAO(SocialPreferencesDAOHibernate.class);
    }

    public static class ResolveBlueprintDAOHibernate extends GenericHibernateDAO<ResolveBlueprint, String> implements ResolveBlueprintDAO
    {

    }
    @Override
    public ResolveBlueprintDAO getResolveBlueprintDAO()
    {
        return (ResolveBlueprintDAO) instantiateDAO(ResolveBlueprintDAOHibernate.class);
    }

    public static class MetaxFormViewPanelDAOHibernate extends GenericHibernateDAO<MetaxFormViewPanel, String> implements MetaxFormViewPanelDAO
    {

    }
    @Override
    public MetaxFormViewPanelDAO getMetaxFormViewPanelDAO()
    {
        return (MetaxFormViewPanelDAO) instantiateDAO(MetaxFormViewPanelDAOHibernate.class);
    }

    public static class MetaxFieldDependencyDAOHibernate extends GenericHibernateDAO<MetaxFieldDependency, String> implements MetaxFieldDependencyDAO
    {

    }
    @Override
    public MetaxFieldDependencyDAO getMetaxFieldDependencyDAO()
    {
        return (MetaxFieldDependencyDAO) instantiateDAO(MetaxFieldDependencyDAOHibernate.class);
    }

    public static class XMPPFilterDAOHibernate extends GenericHibernateDAO<XMPPFilter, String> implements XMPPFilterDAO
    {

    }

    @Override
    public XMPPFilterDAO getXMPPFilterDAO()
    {
        return (XMPPFilterDAO) instantiateDAO(XMPPFilterDAOHibernate.class);
    }

    public static class XMPPAddressDAOHibernate extends GenericHibernateDAO<XMPPAddress, String> implements XMPPAddressDAO
    {

    }

    @Override
    public XMPPAddressDAO getXMPPAddressDAO()
    {
        return (XMPPAddressDAO) instantiateDAO(XMPPAddressDAOHibernate.class);
    }

    public static class SNMPFilterDAOHibernate extends GenericHibernateDAO<SNMPFilter, String> implements SNMPFilterDAO
    {

    }

    @Override
    public SNMPFilterDAO getSNMPFilterDAO()
    {
        return (SNMPFilterDAO) instantiateDAO(SNMPFilterDAOHibernate.class);
    }

    public static class SalesforceFilterDAOHibernate extends GenericHibernateDAO<SalesforceFilter, String> implements SalesforceFilterDAO
    {

    }

    @Override
    public SalesforceFilterDAO getSalesforceFilterDAO()
    {
        return (SalesforceFilterDAO) instantiateDAO(SalesforceFilterDAOHibernate.class);
    }

    public static class WikiDocumentMetaFormRelDAOHibernate extends GenericHibernateDAO<WikiDocumentMetaFormRel, String> implements WikiDocumentMetaFormRelDAO
    {

    }

    @Override
    public WikiDocumentMetaFormRelDAO getWikiDocumentMetaFormRelDAO()
    {
        return (WikiDocumentMetaFormRelDAO) instantiateDAO(WikiDocumentMetaFormRelDAOHibernate.class);
    }
    public static class SSHConnectionPoolDAOHibernate extends GenericHibernateDAO<SSHPool, String> implements SSHConnectionPoolDAO
    {

    } // SSHConnectionPoolDAOHibernate

    @Override
    public SSHConnectionPoolDAO getSSHConnectionPoolDAO()
    {
        return (SSHConnectionPoolDAO) instantiateDAO(SSHConnectionPoolDAOHibernate.class);
    } // getSSHConnectionPoolDAO

    public static class RemedyFilterDAOHibernate extends GenericHibernateDAO<RemedyxFilter, String> implements RemedyFilterDAO
    {

    }

    @Override
    public RemedyFilterDAO getRemedyFilterDAO()
    {
        return (RemedyFilterDAO) instantiateDAO(RemedyFilterDAOHibernate.class);
    }

    public static class RemedyFormDAOHibernate extends GenericHibernateDAO<RemedyxForm, String> implements RemedyFormDAO
    {

    }

    @Override
    public RemedyFormDAO getRemedyFormDAO()
    {
        return (RemedyFormDAO) instantiateDAO(RemedyFormDAOHibernate.class);
    }

    public static class HPOMFilterDAOHibernate extends GenericHibernateDAO<HPOMFilter, String> implements HPOMFilterDAO
    {

    }

    @Override
    public HPOMFilterDAO getHPOMFilterDAO()
    {
        return (HPOMFilterDAO) instantiateDAO(HPOMFilterDAOHibernate.class);
    }

    public static class TelnetConnectionPoolDAOHibernate extends GenericHibernateDAO<TelnetPool, String> implements TelnetConnectionPoolDAO
    {

    }

    @Override
    public TelnetConnectionPoolDAO getTelnetConnectionPoolDAO()
    {
        return (TelnetConnectionPoolDAO) instantiateDAO(TelnetConnectionPoolDAOHibernate.class);
    }

    public static class EWSFilterDAOHibernate extends GenericHibernateDAO<EWSFilter, String> implements EWSFilterDAO
    {

    }

    @Override
    public EWSFilterDAO getEWSFilterDAO()
    {
        return (EWSFilterDAO) instantiateDAO(EWSFilterDAOHibernate.class);
    }

    public static class EWSAddressDAOHibernate extends GenericHibernateDAO<EWSAddress, String> implements EWSAddressDAO
    {

    }

    @Override
    public EWSAddressDAO getEWSAddressDAO()
    {
        return (EWSAddressDAO) instantiateDAO(EWSAddressDAOHibernate.class);
    }

    public static class ResolvePermissionDAOHibernate extends GenericHibernateDAO<ResolvePermission, String> implements ResolvePermissionDAO
    {

    }

    @Override
    public ResolvePermissionDAO getResolvePermissionDAO()
    {
        return (ResolvePermissionDAO) instantiateDAO(ResolvePermissionDAOHibernate.class);
    }

    public static class ResolveActionTaskMockDataDAOHibernate extends GenericHibernateDAO<ResolveActionTaskMockData, String> implements ResolveActionTaskMockDataDAO
    {

    }

    @Override
    public ResolveActionTaskMockDataDAO getResolveActionTaskMockDataDAO()
    {
        return (ResolveActionTaskMockDataDAO) instantiateDAO(ResolveActionTaskMockDataDAOHibernate.class);
    }

    public static class MCPBlueprintDAOHibernate extends GenericHibernateDAO<MCPBlueprint, String> implements MCPBlueprintDAO{}

    @Override
    public MCPBlueprintDAO getMCPBlueprintDAO()
    {
        return (MCPBlueprintDAO) instantiateDAO(MCPBlueprintDAOHibernate.class);
    }

    public static class MCPServerDAOHibernate extends GenericHibernateDAO<MCPServer, String> implements MCPServerDAO{}

    @Override
    public MCPServerDAO getMCPServerDAO()
    {
        return (MCPServerDAO) instantiateDAO(MCPServerDAOHibernate.class);
    }

    public static class MCPComponentDAOHibernate extends GenericHibernateDAO<MCPComponent, String> implements MCPComponentDAO{}

    @Override
    public MCPComponentDAO getMCPComponentDAO()
    {
        return (MCPComponentDAO) instantiateDAO(MCPComponentDAOHibernate.class);
    }
    
    public static class MCPFileDAOHibernate extends GenericHibernateDAO<MCPFile, String> implements MCPFileDAO{}

    @Override
    public MCPFileDAO getMCPFileDAO()
    {
        return (MCPFileDAO) instantiateDAO(MCPFileDAOHibernate.class);
    }
    
    public static class MCPFileContentDAOHibernate extends GenericHibernateDAO<MCPFileContent, String> implements MCPFileContentDAO{}

    @Override
    public MCPFileContentDAO getMCPFileContentDAO()
    {
        return (MCPFileContentDAO) instantiateDAO(MCPFileContentDAOHibernate.class);
    }

    // Metric Threshold hibernate table
    public static class MetricThresholdDAOHibernate extends GenericHibernateDAO<MetricThreshold, String> implements MetricThresholdDAO
    {

    }

    @Override
    public MetricThresholdDAO getMetricThresholdDAO()
    {
        return (MetricThresholdDAO) instantiateDAO(MetricThresholdDAOHibernate.class);
    }

    public static class ConfigActiveDirectoryDAOHibernate extends GenericHibernateDAO<ConfigActiveDirectory, String> implements ConfigActiveDirectoryDAO
    {

    }

    @Override
    public ConfigActiveDirectoryDAO getConfigActiveDirectoryDAO()
    {
        return (ConfigActiveDirectoryDAO) instantiateDAO(ConfigActiveDirectoryDAOHibernate.class);
    }

    public static class OrganizationDAOHibernate extends GenericHibernateDAO<Organization, String> implements OrganizationDAO
    {

    }

    @Override
    public OrganizationDAO getOrganizationDAO()
    {
        return (OrganizationDAO) instantiateDAO(OrganizationDAOHibernate.class);
    }

    public static class ConfigLDAPDAOHibernate extends GenericHibernateDAO<ConfigLDAP, String> implements ConfigLDAPDAO
    {

    }

    @Override
    public ConfigLDAPDAO getConfigLDAPDAO()
    {
        return (ConfigLDAPDAO) instantiateDAO(ConfigLDAPDAOHibernate.class);
    }

    public static class ResolveAppsDAOHibernate extends GenericHibernateDAO<ResolveApps, String> implements ResolveAppsDAO
    {

    }

    @Override
    public ResolveAppsDAO getResolveAppsDAO()
    {
        return (ResolveAppsDAO) instantiateDAO(ResolveAppsDAOHibernate.class);
    }

    public static class ResolveControllersDAOHibernate extends GenericHibernateDAO<ResolveControllers, String> implements ResolveControllersDAO
    {

    }

    @Override
    public ResolveControllersDAO getResolveControllersDAO()
    {
        return (ResolveControllersDAO) instantiateDAO(ResolveControllersDAOHibernate.class);
    }

    public static class ResolveAppControllerRelDAOHibernate extends GenericHibernateDAO<ResolveAppControllerRel, String> implements ResolveAppControllerRelDAO
    {

    }

    @Override
    public ResolveAppControllerRelDAO getResolveAppControllerRelDAO()
    {
        return (ResolveAppControllerRelDAO) instantiateDAO(ResolveAppControllerRelDAOHibernate.class);
    }

    public static class ResolveAppRoleRelDAOHibernate extends GenericHibernateDAO<ResolveAppRoleRel, String> implements ResolveAppRoleRelDAO
    {

    }

    @Override
    public ResolveAppRoleRelDAO getResolveAppRoleRelDAO()
    {
        return (ResolveAppRoleRelDAO) instantiateDAO(ResolveAppRoleRelDAOHibernate.class);
    }

    public static class ResolveAppOrganizationRelDAOHibernate extends GenericHibernateDAO<ResolveAppOrganizationRel, String> implements ResolveAppOrganizationRelDAO
    {

    }

    @Override
    public ResolveAppOrganizationRelDAO getResolveAppOrganizationRelDAO()
    {
        return (ResolveAppOrganizationRelDAO) instantiateDAO(ResolveAppOrganizationRelDAOHibernate.class);
    }

    public static class HPSMFilterDAOHibernate extends GenericHibernateDAO<HPSMFilter, String> implements HPSMFilterDAO
    {

    }

    @Override
    public HPSMFilterDAO getHPSMFilterDAO()
    {
        return (HPSMFilterDAO) instantiateDAO(HPSMFilterDAOHibernate.class);
    }

    public static class NamePropertyDAOHibernate extends GenericHibernateDAO<NameProperty, String> implements NamePropertyDAO
    {

    }

    @Override
    public NamePropertyDAO getGatewayNamePropertyDAO()
    {
        return (NamePropertyDAO) instantiateDAO(NamePropertyDAOHibernate.class);
    }
    
    public static class ResolveAssessRelDAOHibernate extends GenericHibernateDAO<ResolveAssessRel, String> implements ResolveAssessRelDAO
    {

    }

    @Override
    public ResolveAssessRelDAO getResolveAssessRelDAO()
    {
        return (ResolveAssessRelDAO) instantiateDAO(ResolveAssessRelDAOHibernate.class);
    }
    
    public static class ResolveParserRelDAOHibernate extends GenericHibernateDAO<ResolveParserRel, String> implements ResolveParserRelDAO
    {

    }

    @Override
    public ResolveParserRelDAO getResolveParserRelDAO()
    {
        return (ResolveParserRelDAO) instantiateDAO(ResolveParserRelDAOHibernate.class);
    }
    
    public static class ResolvePreprocessRelDAOHibernate extends GenericHibernateDAO<ResolvePreprocessRel, String> implements ResolvePreprocessRelDAO
    {

    }

    @Override
    public ResolvePreprocessRelDAO getResolvePreprocessRelDAO()
    {
        return (ResolvePreprocessRelDAO) instantiateDAO(ResolvePreprocessRelDAOHibernate.class);
    }

    public static class AmqpFilterDAOHibernate extends GenericHibernateDAO<AmqpFilter, String> implements AmqpFilterDAO
    {

    }

    @Override
    public AmqpFilterDAO getAmqpFilterDAO()
    {
        return (AmqpFilterDAO) instantiateDAO(AmqpFilterDAOHibernate.class);
    }
    
    public static class TCPFilterDAOHibernate extends GenericHibernateDAO<TCPFilter, String> implements TCPFilterDAO
    {

    }

    @Override
    public TCPFilterDAO getTCPFilterDAO()
    {
        return (TCPFilterDAO) instantiateDAO(TCPFilterDAOHibernate.class);
    }

    public static class RBConnectionDAOHibernate extends GenericHibernateDAO<RBConnection, String> implements RBConnectionDAO
    {

    }

    @Override
    public RBConnectionDAO getRBConnectionDAO()
    {
        return (RBConnectionDAO) instantiateDAO(RBConnectionDAOHibernate.class);
    }

    public static class RBConditionDAOHibernate extends GenericHibernateDAO<RBCondition, String> implements RBConditionDAO
    {

    }

    @Override
    public RBConditionDAO getRBConditionDAO()
    {
        return (RBConditionDAO) instantiateDAO(RBConditionDAOHibernate.class);
    }

    public static class RBCriterionDAOHibernate extends GenericHibernateDAO<RBCriterion, String> implements RBCriterionDAO
    {

    }

    @Override
    public RBCriterionDAO getRBCriterionDAO()
    {
        return (RBCriterionDAO) instantiateDAO(RBCriterionDAOHibernate.class);
    }

    public static class RBTaskDAOHibernate extends GenericHibernateDAO<RBTask, String> implements RBTaskDAO
    {

    }

    @Override
    public RBTaskDAO getRBTaskDAO()
    {
        return (RBTaskDAO) instantiateDAO(RBTaskDAOHibernate.class);
    }

    public static class RBTaskConditionDAOHibernate extends GenericHibernateDAO<RBTaskCondition, String> implements RBTaskConditionDAO
    {

    }

    @Override
    public RBTaskConditionDAO getRBTaskConditionDAO()
    {
        return (RBTaskConditionDAO) instantiateDAO(RBTaskConditionDAOHibernate.class);
    }

    public static class RBTaskSeverityDAOHibernate extends GenericHibernateDAO<RBTaskSeverity, String> implements RBTaskSeverityDAO
    {

    }

    @Override
    public RBTaskSeverityDAO getRBTaskSeverityDAO()
    {
        return (RBTaskSeverityDAO) instantiateDAO(RBTaskSeverityDAOHibernate.class);
    }

    public static class RBTaskVariableDAOHibernate extends GenericHibernateDAO<RBTaskVariable, String> implements RBTaskVariableDAO
    {

    }

    @Override
    public RBTaskVariableDAO getRBTaskVariableDAO()
    {
        return (RBTaskVariableDAO) instantiateDAO(RBTaskVariableDAOHibernate.class);
    }

    public static class RBGeneralDAOHibernate extends GenericHibernateDAO<RBGeneral, String> implements RBGeneralDAO
    {

    }

    @Override
    public RBGeneralDAO getRBGeneralDAO()
    {
        return (RBGeneralDAO) instantiateDAO(RBGeneralDAOHibernate.class);
    }
    
    public static class RBLoopDAOHibernate extends GenericHibernateDAO<RBLoop, String> implements RBLoopDAO
    {

    }

    @Override
    public RBLoopDAO getRBLoopDAO()
    {
        return (RBLoopDAO) instantiateDAO(RBLoopDAOHibernate.class);
    }
    
    public static class RBIfDAOHibernate extends GenericHibernateDAO<RBIf, String> implements RBIfDAO
    {

    }

    @Override
    public RBIfDAO getRBIfDAO()
    {
        return (RBIfDAO) instantiateDAO(RBIfDAOHibernate.class);
    }

    public static class HTTPFilterDAOHibernate extends GenericHibernateDAO<HTTPFilter, String> implements HTTPFilterDAO
    {

    }

    @Override
    public HTTPFilterDAO getHTTPFilterDAO()
    {
        return (HTTPFilterDAO) instantiateDAO(HTTPFilterDAOHibernate.class);
    }

    public static class ResolveCatalogDAOHibernate extends GenericHibernateDAO<ResolveCatalog, String> implements ResolveCatalogDAO
    {

    }

    @Override
    public ResolveCatalogDAO getResolveCatalogDAO()
    {
        return (ResolveCatalogDAO) instantiateDAO(ResolveCatalogDAOHibernate.class);
    }

    public static class ResolveCatalogNodeDAOHibernate extends GenericHibernateDAO<ResolveCatalogNode, String> implements ResolveCatalogNodeDAO
    {

    }

    @Override
    public ResolveCatalogNodeDAO getResolveCatalogNodeDAO()
    {
        return (ResolveCatalogNodeDAO) instantiateDAO(ResolveCatalogNodeDAOHibernate.class);
    }

    public static class ResolveCatalogEdgeDAOHibernate extends GenericHibernateDAO<ResolveCatalogEdge, String> implements ResolveCatalogEdgeDAO
    {

    }

    @Override
    public ResolveCatalogEdgeDAO getResolveCatalogEdgeDAO()
    {
        return (ResolveCatalogEdgeDAO) instantiateDAO(ResolveCatalogEdgeDAOHibernate.class);
    }
    
    public static class ResolveCatalogNodeWikidocRelDAOHibernate extends GenericHibernateDAO<ResolveCatalogNodeWikidocRel, String> implements ResolveCatalogNodeWikidocRelDAO
    {

    }

    @Override
    public ResolveCatalogNodeWikidocRelDAO getResolveCatalogNodeWikidocRelDAO()
    {
        return (ResolveCatalogNodeWikidocRelDAO) instantiateDAO(ResolveCatalogNodeWikidocRelDAOHibernate.class);
    }
    
    public static class ResolveCatalogNodeTagRelDAOHibernate extends GenericHibernateDAO<ResolveCatalogNodeTagRel, String> implements ResolveCatalogNodeTagRelDAO
    {

    }

    @Override
    public ResolveCatalogNodeTagRelDAO getResolveCatalogNodeTagRelDAO()
    {
        return (ResolveCatalogNodeTagRelDAO) instantiateDAO(ResolveCatalogNodeTagRelDAOHibernate.class);
    }
    
    public static class ResolveNodeDAOHibernate extends GenericHibernateDAO<ResolveNode, String> implements ResolveNodeDAO
    {

    }

    @Override
    public ResolveNodeDAO getResolveNodeDAO()
    {
        return (ResolveNodeDAO) instantiateDAO(ResolveNodeDAOHibernate.class);
    }
    
    public static class ResolveEdgeDAOHibernate extends GenericHibernateDAO<ResolveEdge, String> implements ResolveEdgeDAO
    {

    }

    @Override
    public ResolveEdgeDAO getResolveEdgeDAO()
    {
        return (ResolveEdgeDAO) instantiateDAO(ResolveEdgeDAOHibernate.class);
    }
    
    public static class ResolveNodePropertiesDAOHibernate extends GenericHibernateDAO<ResolveNodeProperties, String> implements ResolveNodePropertiesDAO
    {

    }

    @Override
    public ResolveNodePropertiesDAO getResolveNodePropertiesDAO()
    {
        return (ResolveNodePropertiesDAO) instantiateDAO(ResolveNodePropertiesDAOHibernate.class);
    }
    
    public static class ResolveEdgePropertiesDAOHibernate extends GenericHibernateDAO<ResolveEdgeProperties, String> implements ResolveEdgePropertiesDAO
    {

    }

    @Override
    public ResolveEdgePropertiesDAO getResolveEdgePropertiesDAO()
    {
        return (ResolveEdgePropertiesDAO) instantiateDAO(ResolveEdgePropertiesDAOHibernate.class);
    }
    
    
//    public static class SocialNotificationsDAOHibernate extends GenericHibernateDAO<SocialNotifications, String> implements SocialNotificationsDAO
//    {
//
//    }
//
//    @Override
//    public SocialNotificationsDAO getSocialNotificationsDAO()
//    {
//        return (SocialNotificationsDAO) instantiateDAO(SocialNotificationsDAOHibernate.class);
//    }
//    
//    
//    public static class SocialNotificationRuleDAOHibernate extends GenericHibernateDAO<SocialNotificationRule, String> implements SocialNotificationRuleDAO
//    {
//
//    }
//
//    @Override
//    public SocialNotificationRuleDAO getSocialNotificationRuleDAO()
//    {
//        return (SocialNotificationRuleDAO) instantiateDAO(SocialNotificationRuleDAOHibernate.class);
//    }
    
    public static class SocialUserNotificationDAOHibernate extends GenericHibernateDAO<SocialUserNotification, String> implements SocialUserNotificationDAO
    {

    }

    @Override
    public SocialUserNotificationDAO getSocialUserNotificationDAO()
    {
        return (SocialUserNotificationDAO) instantiateDAO(SocialUserNotificationDAOHibernate.class);
    }
    
    public static class TIBCOBespokeFilterDAOHibernate extends GenericHibernateDAO<TIBCOBespokeFilter, String> implements TIBCOBespokeFilterDAO
    {

    }
    
    @Override
    public TIBCOBespokeFilterDAO getTIBCOBespokeFilterDAO()
    {
        return (TIBCOBespokeFilterDAO) instantiateDAO(TIBCOBespokeFilterDAOHibernate.class);
    }
    
    public static class CASpectrumFilterDAOHibernate extends GenericHibernateDAO<CASpectrumFilter, String> implements CASpectrumFilterDAO
    {

    }

    @Override
    public CASpectrumFilterDAO getCASpectrumFilterDAO()
    {
        return (CASpectrumFilterDAO) instantiateDAO(CASpectrumFilterDAOHibernate.class);
    }
    
    
    
    public static class ResolveMCPGroupDAOHibernate extends GenericHibernateDAO<ResolveMCPGroup, String> implements ResolveMCPGroupDAO
    {

    }

    @Override
    public ResolveMCPGroupDAO getResolveMCPGroupDAO()
    {
        return (ResolveMCPGroupDAO) instantiateDAO(ResolveMCPGroupDAOHibernate.class);
    }
    
    
    public static class ResolveMCPClusterDAOHibernate extends GenericHibernateDAO<ResolveMCPCluster, String> implements ResolveMCPClusterDAO
    {

    }

    @Override
    public ResolveMCPClusterDAO getResolveMCPClusterDAO()
    {
        return (ResolveMCPClusterDAO) instantiateDAO(ResolveMCPClusterDAOHibernate.class);
    }
    
    public static class ResolveMCPHostDAOHibernate extends GenericHibernateDAO<ResolveMCPHost, String> implements ResolveMCPHostDAO
    {

    }

    @Override
    public ResolveMCPHostDAO getResolveMCPHostDAO()
    {
        return (ResolveMCPHostDAO) instantiateDAO(ResolveMCPHostDAOHibernate.class);
    }
    
    public static class ResolveMCPBlueprintDAOHibernate extends GenericHibernateDAO<ResolveMCPBlueprint, String> implements ResolveMCPBlueprintDAO
    {

    }

    @Override
    public ResolveMCPBlueprintDAO getResolveMCPBlueprintDAO()
    {
        return (ResolveMCPBlueprintDAO) instantiateDAO(ResolveMCPBlueprintDAOHibernate.class);
    }
    
    public static class ResolveMCPComponentDAOHibernate extends GenericHibernateDAO<ResolveMCPComponent, String> implements ResolveMCPComponentDAO
    {

    }

    @Override
    public ResolveMCPComponentDAO getResolveMCPComponentDAO()
    {
        return (ResolveMCPComponentDAO) instantiateDAO(ResolveMCPComponentDAOHibernate.class);
    }
    
    
    
    public static class ResolveMCPHostStatusDAOHibernate extends GenericHibernateDAO<ResolveMCPHostStatus, String> implements ResolveMCPHostStatusDAO
    {

    }

    @Override
    public ResolveMCPHostStatusDAO getResolveMCPHostStatusDAO()
    {
        return (ResolveMCPHostStatusDAO) instantiateDAO(ResolveMCPHostStatusDAOHibernate.class);
    }
    
    public static class ResolveAssessExpressionDAOHibernate extends GenericHibernateDAO<ResolveTaskExpression, String> implements ResolveTaskExpressionDAO
    {
    	
    }
    
    @Override
    public ResolveTaskExpressionDAO getResolveTaskExpressionDAO()
    {
    	return (ResolveTaskExpressionDAO) instantiateDAO(ResolveAssessExpressionDAOHibernate.class);
    }
    
    public static class ResolveAssessOutputMappingDAOHibernate extends GenericHibernateDAO<ResolveTaskOutputMapping, String> implements ResolveTaskOutputMappingDAO
    {
    	
    }
    
    @Override
    public ResolveTaskOutputMappingDAO getResolveTaskOutputMappingDAO()
    {
    	return (ResolveTaskOutputMappingDAO) instantiateDAO(ResolveAssessOutputMappingDAOHibernate.class);
    }
    
    public static class ConfigRADIUSDAOHibernate extends GenericHibernateDAO<ConfigRADIUS, String> implements ConfigRADIUSDAO
    {

    }

    @Override
    public ConfigRADIUSDAO getConfigRADIUSDAO()
    {
        return (ConfigRADIUSDAO) instantiateDAO(ConfigRADIUSDAOHibernate.class);
    }
    
    public static class ArchiveSirActivityDAOHibernate extends GenericHibernateDAO<ArchiveSirActivity, String> implements ArchiveSirActivityDAO
    {

    }
    @Override
    public ArchiveSirActivityDAO getArchiveSirActivityDAO()
    {
        return (ArchiveSirActivityDAO) instantiateDAO(ArchiveSirActivityDAOHibernate.class);
    }
    
    public static class ArchiveSirNoteDAOHibernate extends GenericHibernateDAO<ArchiveSirNote, String> implements ArchiveSirNoteDAO
    {

    }
    @Override
    public ArchiveSirNoteDAO getArchiveSirNoteDAO()
    {
        return (ArchiveSirNoteDAO) instantiateDAO(ArchiveSirNoteDAOHibernate.class);
    }
    
    public static class ArchiveSirAttachmentDAOHibernate extends GenericHibernateDAO<ArchiveSirAttachment, String> implements ArchiveSirAttachmentDAO
    {

    }
    @Override
    public ArchiveSirAttachmentDAO getArchiveSirAttachmentDAO()
    {
        return (ArchiveSirAttachmentDAO) instantiateDAO(ArchiveSirAttachmentDAOHibernate.class);
    }
    
    public static class ArchiveSirArtifactDAOHibernate extends GenericHibernateDAO<ArchiveSirArtifact, String> implements ArchiveSirArtifactDAO
    {

    }
    @Override
    public ArchiveSirArtifactDAO getArchiveSirArtifactDAO()
    {
        return (ArchiveSirArtifactDAO) instantiateDAO(ArchiveSirArtifactDAOHibernate.class);
    }
    
    public static class ArchiveSirAuditLogDAOHibernate extends GenericHibernateDAO<ArchiveSirAuditLog, String> implements ArchiveSirAuditLogDAO
    {

    }
    @Override
    public ArchiveSirAuditLogDAO getArchiveSirAuditLogDAO()
    {
        return (ArchiveSirAuditLogDAO) instantiateDAO(ArchiveSirAuditLogDAOHibernate.class);
    }
    
    public static class ResolveSecurityIncidentDAOHibernate extends GenericHibernateDAO<ResolveSecurityIncident, String> implements ResolveSecurityIncidentDAO
    {

    }

    @Override
    public ResolveSecurityIncidentDAO getResolveSecurityIncidentDAO()
    {
        return (ResolveSecurityIncidentDAO) instantiateDAO(ResolveSecurityIncidentDAOHibernate.class);
    }
    
    public static class PlaybookActivitiesDAOHibernate extends GenericHibernateDAO<PlaybookActivities, String> implements PlaybookActivitiesDAO
    {

    }

    @Override
    public PlaybookActivitiesDAO getPlaybookActivitiesDAO()
    {
        return (PlaybookActivitiesDAO) instantiateDAO(PlaybookActivitiesDAOHibernate.class);
    }
    
    public static class ResolveCompRelDAOHibernate extends GenericHibernateDAO<ResolveCompRel, String> implements ResolveCompRelDAO
    {

    }
    
    @Override
    public ResolveCompRelDAO getResolveCompRelDAO()
    {
        return (ResolveCompRelDAO) instantiateDAO(ResolveCompRelDAOHibernate.class);
    }
    
    public static class OrgsDAOHibernate extends GenericHibernateDAO<Orgs, String> implements OrgsDAO
    {

    }

    @Override
    public OrgsDAO getOrgsDAO()
    {
        return (OrgsDAO) instantiateDAO(OrgsDAOHibernate.class);
    }
    
    public static class OrgGroupRelDAOHibernate extends GenericHibernateDAO<OrgGroupRel, String> implements OrgGroupRelDAO
    {

    }
    
    @Override
    public OrgGroupRelDAO getOrgGroupRelDAO()
    {
        return (OrgGroupRelDAO) instantiateDAO(OrgGroupRelDAOHibernate.class);
    }

	public static class CustomDictionaryItemDAOHibernate extends GenericHibernateDAO<CustomDictionaryItem, String>
			implements CustomDictionaryItemDAO {
	}

	@Override
	public CustomDictionaryItemDAO getCustomDictionaryDAO() {
		return (CustomDictionaryItemDAO) instantiateDAO(CustomDictionaryItemDAOHibernate.class);
	}

	public static class CEFDictionaryItemDAOHibernate extends GenericHibernateDAO<CEFDictionaryItem, String>
			implements CEFDictionaryItemDAO {}

	@Override
	public CEFDictionaryItemDAO getCEFDictionaryDAO() {
		return (CEFDictionaryItemDAO) instantiateDAO(CEFDictionaryItemDAOHibernate.class);
	}

	public static class ArtifactConfigurationDAOHibernate extends GenericHibernateDAO<ArtifactConfiguration, String>
			implements ArtifactConfigurationDAO {}

	@Override
	public ArtifactConfigurationDAO getArtifactConfigurationDAO() {
		return (ArtifactConfigurationDAO) instantiateDAO(ArtifactConfigurationDAOHibernate.class);
	}

	public static class ArtifactTypeDAOHibernate extends GenericHibernateDAO<ArtifactType, String>
			implements ArtifactTypeDAO {}

	@Override
	public ArtifactTypeDAO getArtifactTypeDAO() {
		return (ArtifactTypeDAO) instantiateDAO(ArtifactTypeDAOHibernate.class);
	}
	
	public static class RRSchemaDAOHibernate extends GenericHibernateDAO<RRSchema, String> implements RRSchemaDAO
    {

    }

    @Override
    public RRSchemaDAO getRRSchemaDAO()
    {
        return (RRSchemaDAO) instantiateDAO(RRSchemaDAOHibernate.class);
    }
    
    public static class RRRuleDAOHibernate extends GenericHibernateDAO<RRRule, String> implements RRRuleDAO
    {

    }

    @Override
    public RRRuleDAO getRRRuleDAO()
    {
        return (RRRuleDAO) instantiateDAO(RRRuleDAOHibernate.class);
    }
    
    public static class CustomDataFormTabDAOHibernate extends GenericHibernateDAO<CustomDataFormTab, String> implements CustomDataFormTabDAO
    {

    }

    @Override
    public CustomDataFormTabDAO getCustomDataFormTabDAO()
    {
        return (CustomDataFormTabDAO) instantiateDAO(CustomDataFormTabDAOHibernate.class);
    }
    
    public static class SIRConfigDAOHibernate extends GenericHibernateDAO<SIRConfig, String> implements SIRConfigDAO
    {

    }

    @Override
    public SIRConfigDAO getSIRConfigDAO()
    {
        return (SIRConfigDAO) instantiateDAO(SIRConfigDAOHibernate.class);
    }
    
    public static class ResolveCaseConfigDAOHibernate extends GenericHibernateDAO<ResolveCaseConfig, String> implements ResolveCaseConfigDAO
    {

    }

    @Override
    public ResolveCaseConfigDAO getResolveCaseConfigDAO()
    {
        return (ResolveCaseConfigDAO) instantiateDAO(ResolveCaseConfigDAOHibernate.class);
    }

	public static class SavedSearchDAOHibernate extends GenericHibernateDAO<SavedSearch, String> implements SavedSearchDAO {}
	
	@Override
	public SavedSearchDAO getSavedSearchDAO() {
		return (SavedSearchDAO) instantiateDAO(SavedSearchDAOHibernate.class);
	}

	public static class SIRPhaseMetaDataDAOHibernate extends GenericHibernateDAO<SIRPhaseMetaData, String> implements SIRPhaseMetaDataDAO {
    }

    @Override
    public SIRPhaseMetaDataDAO getSIRPhaseMetaDataDAO() {
        return (SIRPhaseMetaDataDAO) instantiateDAO(SIRPhaseMetaDataDAOHibernate.class);
    }
    
    public static class SIRActivityMetaDataDAOHibernate extends GenericHibernateDAO<SIRActivityMetaData, String> implements SIRActivityMetaDataDAO {
    }

    @Override
    public SIRActivityMetaDataDAO getSIRActivityMetaDataDAO() {
        return (SIRActivityMetaDataDAO) instantiateDAO(SIRActivityMetaDataDAOHibernate.class);
    }
    
    public static class SIRPhaseRuntimeDataDAOHibernate extends GenericHibernateDAO<SIRPhaseRuntimeData, String> implements SIRPhaseRuntimeDataDAO {
    }

    @Override
    public SIRPhaseRuntimeDataDAO getSIRPhaseRuntimeDataDAO() {
        return (SIRPhaseRuntimeDataDAO) instantiateDAO(SIRPhaseRuntimeDataDAOHibernate.class);
    }
    
    public static class SIRActivityRuntimeDataDAOHibernate extends GenericHibernateDAO<SIRActivityRuntimeData, String> implements SIRActivityRuntimeDataDAO {
    }

    @Override
    public SIRActivityRuntimeDataDAO getSIRActivityRuntimeDataDAO() {
        return (SIRActivityRuntimeDataDAO) instantiateDAO(SIRActivityRuntimeDataDAOHibernate.class);
    }

	public static class CaseAttributeDAOHibernate extends GenericHibernateDAO<CaseAttribute, String> implements CaseAttributeDAO {}

	@Override
	public CaseAttributeDAO getCaseAttributeDAO() {
		return (CaseAttributeDAO) instantiateDAO(CaseAttributeDAOHibernate.class);
	}
	
	public static class CustomDataFormDAOHibernate extends GenericHibernateDAO<CustomDataForm, String> implements CustomDataFormDAO {}

	@Override
	public CustomDataFormDAO getCustomDataFormDAO() {
		return (CustomDataFormDAO) instantiateDAO(CustomDataFormDAOHibernate.class);
	}
	
	public static class RecentItemDAOHibernate extends GenericHibernateDAO<RecentItem, String> implements RecentItemDAO {}

	public RecentItemDAO getRecentItemDAO() {
		return (RecentItemDAO) instantiateDAO(RecentItemDAOHibernate.class);
	}

	public static class RecentSearchDAOHibernate extends GenericHibernateDAO<RecentSearch, String> implements RecentSearchDAO {}

	public RecentSearchDAO getRecentSearchDAO() {
		return (RecentSearchDAO) instantiateDAO(RecentSearchDAOHibernate.class);
	}
	
	public static class AggrExecSummaryDAOHibernate extends GenericHibernateDAO<AggrExecSummary, String> 
													implements AggrExecSummaryDAO {
    }

    @Override
    public AggrExecSummaryDAO getAggrExecSummaryDAO() {
        return (AggrExecSummaryDAO) instantiateDAO(AggrExecSummaryDAOHibernate.class);
    }

	public static class ResolveNamespaceDAOHibernate extends GenericHibernateDAO<ResolveNamespace, String>
													 implements ResolveNamespaceDAO {}

	@Override
	public ResolveNamespaceDAO getResolveNamespaceDAO() {
		return (ResolveNamespaceDAO) instantiateDAO(ResolveNamespaceDAOHibernate.class);
	}
	
	public static class GatewayHealthCheckDAOHibernate extends GenericHibernateDAO<GatewayHealthCheck, String>
													   implements GatewayHealthCheckDAO {}

	@Override
	public GatewayHealthCheckDAO getGatewayHealthCheckDAO() {
		return (GatewayHealthCheckDAO) instantiateDAO(GatewayHealthCheckDAOHibernate.class);
	}
	
	public static class ArchiveWorksheetDataDAOHibernate extends GenericHibernateDAO<ArchiveWorksheetData, String>
	   implements ArchiveWorksheetDataDAO {}

	@Override
	public ArchiveWorksheetDataDAO getArchiveWorksheetDataDAO() {
		return (ArchiveWorksheetDataDAO) instantiateDAO(ArchiveWorksheetDataDAOHibernate.class);
	}
}
