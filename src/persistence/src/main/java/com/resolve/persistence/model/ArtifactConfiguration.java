package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ArtifactConfigurationVO;

@Entity
@Table(name = "artifact_configuration")
public class ArtifactConfiguration extends BaseModel<ArtifactConfigurationVO> {
	private static final long serialVersionUID = -3760139791025486412L;

	private String UName;
	private ArtifactType artifactType;
	private ResolveActionTask task;
	private WikiDocument automation;
	private String param;

    @Column(name = "u_name", unique = true, nullable = false)
	public String getUName() {
		return UName;
	}

	public void setUName(String uName) {
		UName = uName;
	}

	@ManyToOne
    @JoinColumn(name = "u_type", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT) )
    @NotFound(action = NotFoundAction.EXCEPTION)
    public ArtifactType getArtifactType() {
		return artifactType;
	}

	public void setArtifactType(ArtifactType artifactType) {
		this.artifactType = artifactType;
	}

	@ManyToOne
    @JoinColumn(name = "u_actiontask", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
	public ResolveActionTask getTask() {
		return task;
	}

	public void setTask(ResolveActionTask task) {
		this.task = task;
	}

    @ManyToOne
    @JoinColumn(name = "u_automation", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
	public WikiDocument getAutomation() {
		return automation;
	}

	public void setAutomation(WikiDocument automation) {
		this.automation = automation;
	}

    @Column(name = "u_param")
	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	@Override
	public ArtifactConfigurationVO doGetVO() {
		ArtifactConfigurationVO vo = new ArtifactConfigurationVO();
		vo.setUName(UName);
		vo.setParam(param);
		
		if (artifactType != null) {
			vo.setArtifactType(artifactType.doGetVO());
		}
		
		if (automation != null) {
			vo.setAutomation(automation.doGetVO());
		}
		
		if (task != null) {
			vo.setTask(task.doGetVO());
		}
		
		return vo;
	}
	
	@PreUpdate
    @PrePersist
    public void checkOnlyOneCondition() {
        if ((task != null && automation != null)|| (task == null && automation == null)) {
			throw new IllegalArgumentException("Artifact entity can contain only task or automation.");
        }
    }
}
