package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.resolve.services.hibernate.vo.PlaybookActivitiesVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "playbook_activities", indexes = {
                @Index(columnList = "u_fullname", name = "pas_fullname_version_org_idx"),
                @Index(columnList = "u_version", name = "pas_fullname_version_org_idx"),
                @Index(columnList = "sys_org", name = "pas_fullname_version_org_idx"),
                @Index(columnList = "u_sir_sys_id", name = "pas_u_sir_sys_id_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PlaybookActivities extends BaseModel<PlaybookActivitiesVO>
{
    private static final long serialVersionUID = -8939950731330566888L;
    
    private String UFullname;           // Full name (name space.document name) of playbook template, activities
                                        // are part of
                                        //                          OR
                                        // Blank for activities associated with a specific SIR having one or more
                                        // activity added to it and which is/are not part of the playbook template
    private Integer UVersion;           // Version of playbook template, activity was added to first time
                                        //                         OR
                                        // 0 for activities associated with a specific SIR having one or more
                                        // activity added to it and which is/are not part of the playbook template
    private String USirSysId;           // Sys Id of SIR these activities are associated  with                                    
    private String UActivitiesJSON;     // Activities JSON (Hierarchichal structure)
                                        // Array of Phases (in order), Each Phase contianing array of activities
                                        // in order (if not not empty)
    
    public PlaybookActivities()
    {
    }
    
    public PlaybookActivities(PlaybookActivitiesVO vo)
    {
        applyVOToModel(vo);
    }
    
	@Column(name = "u_fullname", length = 300)
    public String getUFullname()
    {
        return this.UFullname;
    }

    public void setUFullname(String UFullname)
    {
        this.UFullname = UFullname;
    }
    
    @Column(name = "u_version")
    public Integer getUVersion()
    {
        return this.UVersion;
    }

    public void setUVersion(Integer UVersion)
    {
        this.UVersion = UVersion;
    }
    
    @Column(name = "u_sir_sys_id", length = 32)
    public String getUSirSysId()
    {
        return this.USirSysId;
    }
    
    public void setUSirSysId(String USirSysId)
    {
        this.USirSysId = USirSysId;
    }
    
    @Lob
    @Column(name = "u_activities_json", length = 16777215)
    public String getUActivitiesJSON()
    {
        return this.UActivitiesJSON;
    }

    public void setUActivitiesJSON(String UActivitiesJSON)
    {
        this.UActivitiesJSON = UActivitiesJSON;
    }
    
	@Override
	public PlaybookActivitiesVO doGetVO()
	{
	    PlaybookActivitiesVO vo = new PlaybookActivitiesVO();
		
		super.doGetBaseVO(vo);
		
		vo.setUFullname(this.getUFullname());
		vo.setUVersion(this.getUVersion());
		vo.setUSirSysId(this.getUSirSysId());
        vo.setUActivitiesJSON(this.getUActivitiesJSON());
				
		return vo;
	}
	
	@Override
	public void applyVOToModel(PlaybookActivitiesVO vo)
	{
		if (vo != null)
		{
			super.applyVOToModel(vo);
			
			this.setUFullname(StringUtils.isNotBlank(vo.getUFullname()) && vo.getUFullname().equals(VO.STRING_DEFAULT) ? getUFullname() : vo.getUFullname());
			this.setUVersion(vo.getUVersion() != null && vo.getUVersion().equals(VO.INTEGER_DEFAULT) ? getUVersion() : vo.getUVersion());
			this.setUSirSysId(StringUtils.isNotBlank(vo.getUSirSysId()) && vo.getUSirSysId().equals(VO.STRING_DEFAULT) ? getUSirSysId() : vo.getUSirSysId());
			this.setUActivitiesJSON(StringUtils.isNotBlank(vo.getUActivitiesJSON()) && vo.getUActivitiesJSON().equals(VO.STRING_DEFAULT) ? getUActivitiesJSON() : vo.getUActivitiesJSON());
		}
	}
}
