/******************************************************************************
* (C) Copyright 2018
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.CustomDataFormTabVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "custom_data_form_tab", uniqueConstraints = { @UniqueConstraint(columnNames = {"u_name", "sys_org"}, name = "cdft_u_name_sys_org_uk")}
       /*, indexes = {
                @Index(columnList = "u_meta_table_view_sys_id", name = "mtvf_u_meta_table_view_idx"),
                @Index(columnList = "u_meta_field_sys_id", name = "mtvf_u_meta_field_idx"),
                @Index(columnList = "u_meta_view_field_sys_id", name = "mtvf_u_meta_view_field_idx"),
                @Index(columnList = "u_meta_field_properties_sys_id", name = "mtvf_u_meta_field_prop_idx")}*/)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CustomDataFormTab  extends BaseModel<CustomDataFormTabVO>
{
    private static final long serialVersionUID = -3728610790088438739L;
    
    private String UName;
    private Boolean UIsActive;
    private Integer UOrder;
    
    // Referenced Objects
    private WikiDocument baseWiki;
    
    // Referencing Objects
    private SIRConfig sirConfig;
    
    public CustomDataFormTab()
    {
    }

    public CustomDataFormTab(CustomDataFormTabVO vo)
    {
        applyVOToModel(vo);
    }
    
    @Column(name = "u_name", length = 200, nullable = false)
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    @Column(name = "u_is_active", length = 1)
    @org.hibernate.annotations.Type(type = "yes_no")
    public Boolean getUIsActive()
    {
        return UIsActive;
    }
    
    public Boolean ugetUIsActive()
    {
        Boolean result = getUIsActive();
        
        if(result == null)
        {
            result = Boolean.FALSE;
        }
        
        return result;
    }
    
    public void setUIsActive(Boolean UIsActive)
    {
        this.UIsActive = UIsActive;
    }
    
    @Column(name = "u_order")
    public Integer getUOrder()
    {
        return UOrder;
    }
    
    public Integer ugetUOrder()
    {
        Integer result = getUOrder();
        
        if (result == null)
        {
            result = VO.NON_NEGATIVE_INTEGER_DEFAULT;
        }
        
        return result;
    }
    
    public void setUOrder(Integer UOrder)
    {
        this.UOrder = UOrder;
    }
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_wikidoc_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public WikiDocument getBaseWiki()
    {
        return baseWiki;
    }

    public void setBaseWiki(WikiDocument baseWiki)
    {
        this.baseWiki = baseWiki;
    }
    
    // objects referenced
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_sir_config_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public SIRConfig getSirConfig()
    {
        return sirConfig;
    }

    public void setSirConfig(SIRConfig sirConfig)
    {
        this.sirConfig = sirConfig;

//        if (sirConfig != null)
//        {
//            setSysOrg(sirConfig.getSysOrg());
//            
//            Collection<CustomDataFormTab> customDataFormTabs = sirConfig.getCustomDataFormTabs();
//            
//            if (customDataFormTabs == null)
//            {
//                customDataFormTabs = new HashSet<CustomDataFormTab>();
//            }
//
//            customDataFormTabs.add(this);
//            sirConfig.setCustomDataFormTabs(customDataFormTabs);
//        }
    }
    
    @Override
    public CustomDataFormTabVO doGetVO()
    {
        CustomDataFormTabVO vo = new CustomDataFormTabVO();
        
        super.doGetBaseVO(vo);
        
        vo.setUName(getUName());
        vo.setUIsActive(getUIsActive());
        vo.setUOrder(getUOrder());
        
        // Referenced Objects
        
        vo.setBaseWiki(Hibernate.isInitialized(this.baseWiki) && getBaseWiki() != null ? getBaseWiki().doGetVO() : null) ;
                
        return vo;
    }

    @Override
    public void applyVOToModel(CustomDataFormTabVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUIsActive(vo.getUIsActive() != null ? vo.getUIsActive() : getUIsActive());
            this.setUOrder(vo.getUOrder() != null ? vo.getUOrder() : getUOrder());
            
            // Referenced Objects
            
            this.setBaseWiki(vo.getBaseWiki() != null ? new WikiDocument(vo.getBaseWiki()) : getBaseWiki());
            
            // Referencing Objects
            
            Set<CustomDataFormTabVO> customDataFormTabVOsInSIRConfig = null;
            
            // Remove CustomDataFormTabVOs from SIRConfigVO to avoid recursion
            
            if (vo.getSirConfig() != null && vo.getSirConfig().getCustomDataFormTabs() != null)
            {
                customDataFormTabVOsInSIRConfig = vo.getSirConfig().getCustomDataFormTabs();
                vo.getSirConfig().setCustomDataFormTabs(null);
            }
            
            this.setSirConfig(vo.getSirConfig() != null ? new SIRConfig(vo.getSirConfig()) : getSirConfig());
            
            // Restore back CustomDataFormTabVOs into SIRConfigVO
            
            if (customDataFormTabVOsInSIRConfig != null)
            {
                vo.getSirConfig().setCustomDataFormTabs(customDataFormTabVOsInSIRConfig);
            }
        }
    }
    
    @Override
    public String toString()
    {
        return "Name: " + getUName() + ", Is Active: " + ugetUIsActive() + ", Base Wiki: " + 
               (getBaseWiki() != null ? getBaseWiki().getUFullname() : "") +
               ", Order: " + ugetUOrder().intValue() +
               ", Org: " + getSysOrg();
    }
    
    @Override
    public boolean equals(Object otherObj)
    {
        boolean isEquals = false;
        
        if (otherObj instanceof CustomDataFormTab)
        {
            CustomDataFormTab otherCustomDataFormTab = (CustomDataFormTab)otherObj;
            
            if (otherCustomDataFormTab != this)
            {
                if (((otherCustomDataFormTab.getSysOrg() == null && this.getSysOrg() == null) ||
                     (otherCustomDataFormTab.getSysOrg() != null && this.getSysOrg() != null &&
                      otherCustomDataFormTab.getSysOrg().equals(this.getSysOrg()))) &&
                    (otherCustomDataFormTab.getUName().equals(this.getUName())))
                {
                    isEquals = true;
                }
            }
            else
                isEquals = true; 
        }
        
        return isEquals;
    }
    
    @Override
    public int hashCode()
    {
        int hash = 1;
        hash = hash * 31 + (this.getSysOrg() == null ? 0 : this.getSysOrg().hashCode());
        hash = hash * 31 + (this.getUName() == null ? 0 : this.getUName().hashCode());
        return hash;
    }
}
