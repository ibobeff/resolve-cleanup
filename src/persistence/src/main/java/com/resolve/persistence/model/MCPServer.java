/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.resolve.services.hibernate.vo.MCPServerVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
@Entity
@Table(name = "mcp_server")
public class MCPServer extends BaseModel<MCPServerVO>
{
    private String serverIp;
    private String name;
    private Boolean active;
    private String description;
    private String tag;
    private String status;
    private String blueprint;
    private String actualBlueprint;
    private String worksheetId;
    private String worksheet;
    private String componentStatus;
    private String version;
    private String build;

    private String resolveHome;
    private Integer sshPort;
    private String remoteUser;
    private String remotePassword;
    private String terminalPrompt;
    private String privateKeyLocation;

    public MCPServer()
    {
    }

    public MCPServer(MCPServerVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_server_ip")
    public String getServerIp()
    {
        return serverIp;
    }

    public void setServerIp(String serverIp)
    {
        this.serverIp = serverIp;
    }

    @Column(name = "u_name")
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Column(name = "u_active")
    public Boolean getActive()
    {
        return this.active;
    }

    public void setActive(Boolean active)
    {
        this.active = active;
    }

    @Column(name = "u_tag")
    public String getTag()
    {
        return tag;
    }

    public void setTag(String tag)
    {
        this.tag = tag;
    }

    @Column(name = "u_description")
    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @Column(name = "u_status")
    public String getStatus()
    {
        return status;
    }

    public void setStatus(String uStatus)
    {
        status = uStatus;
    }

    @Lob
    @Column(name = "u_blueprint", length = 16777215, nullable = false)
    public String getBlueprint()
    {
        return this.blueprint;
    }

    public void setBlueprint(String UBlueprint)
    {
        this.blueprint = UBlueprint;
    }

    @Lob
    @Column(name = "u_act_blueprint", length = 16777215)
    public String getActualBlueprint()
    {
        return actualBlueprint;
    }

    public void setActualBlueprint(String actualBlueprint)
    {
        this.actualBlueprint = actualBlueprint;
    }

    @Column(name = "u_worksheet_id")
    public String getWorksheetId()
    {
        return worksheetId;
    }

    public void setWorksheetId(String worksheetId)
    {
        this.worksheetId = worksheetId;
    }

    @Column(name = "u_worksheet")
    public String getWorksheet()
    {
        return worksheet;
    }

    public void setWorksheet(String worksheet)
    {
        this.worksheet = worksheet;
    }

    @Column(name = "u_comp_status", length = 2000)
    public String getComponentStatus()
    {
        return componentStatus;
    }

    public void setComponentStatus(String componentStatus)
    {
        this.componentStatus = componentStatus;
    }

    @Column(name = "u_version")
    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    @Column(name = "u_build")
    public String getBuild()
    {
        return build;
    }

    public void setBuild(String build)
    {
        this.build = build;
    }

    @Column(name = "u_resolve_home")
    public String getResolveHome()
    {
        return resolveHome;
    }

    public void setResolveHome(String resolveHome)
    {
        this.resolveHome = resolveHome;
    }

    @Column(name = "u_ssh_port")
    public Integer getSshPort()
    {
        return sshPort;
    }

    public void setSshPort(Integer sshPort)
    {
        this.sshPort = sshPort;
    }

    @Column(name = "u_remote_user")
    public String getRemoteUser()
    {
        return remoteUser;
    }

    public void setRemoteUser(String remoteUser)
    {
        this.remoteUser = remoteUser;
    }

    @Column(name = "u_remote_pwd")
    public String getRemotePassword()
    {
        return remotePassword;
    }

    public void setRemotePassword(String remotePassword)
    {
        this.remotePassword = remotePassword;
    }

    @Column(name = "u_terminal_prompt")
    public String getTerminalPrompt()
    {
        return terminalPrompt;
    }

    public void setTerminalPrompt(String terminalPrompt)
    {
        this.terminalPrompt = terminalPrompt;
    }

    @Column(name = "u_private_key_loc")
    public String getPrivateKeyLocation()
    {
        return privateKeyLocation;
    }

    public void setPrivateKeyLocation(String privateKeyLocation)
    {
        this.privateKeyLocation = privateKeyLocation;
    }

    @Override
    public MCPServerVO doGetVO()
    {
        MCPServerVO vo = new MCPServerVO();
        try
        {
            super.doGetBaseVO(vo);

            vo.setServerIp(getServerIp());
            vo.setName(getName());
            vo.setTag(getTag());
            vo.setDescription(getDescription());
            vo.setStatus(getStatus());
            vo.setBlueprint(getBlueprint());
            vo.setActualBlueprint(getActualBlueprint());
            vo.setWorksheetId(getWorksheetId());
            vo.setWorksheet(getWorksheet());
            vo.setComponentStatus(getComponentStatus());
            vo.setVersion(getVersion());
            vo.setBuild(getBuild());

            vo.setResolveHome(getResolveHome());
            vo.setSshPort(getSshPort());
            vo.setRemoteUser(getRemoteUser());
            if (CryptUtils.isEncrypted(getRemotePassword()))
            {
                vo.setRemotePassword(CryptUtils.decrypt(getRemotePassword()));
            }
            vo.setTerminalPrompt(getTerminalPrompt());
            vo.setPrivateKeyLocation(getPrivateKeyLocation());
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return vo;
    }

    @Override
    public void applyVOToModel(MCPServerVO vo)
    {
        if (vo != null)
        {
            try
            {
                super.applyVOToModel(vo);

                this.setName(StringUtils.isNotBlank(vo.getName()) && vo.getName().equals(VO.STRING_DEFAULT) ? getName() : vo.getName());
                this.setTag(StringUtils.isNotBlank(vo.getTag()) && vo.getTag().equals(VO.STRING_DEFAULT) ? getTag() : vo.getTag());
                this.setDescription(StringUtils.isNotBlank(vo.getDescription()) && vo.getDescription().equals(VO.STRING_DEFAULT) ? getDescription() : vo.getDescription());
                this.setStatus(StringUtils.isNotBlank(vo.getStatus()) && vo.getStatus().equals(VO.STRING_DEFAULT) ? getStatus() : vo.getStatus());
                this.setBlueprint(StringUtils.isNotBlank(vo.getBlueprint()) && vo.getBlueprint().equals(VO.STRING_DEFAULT) ? getBlueprint() : vo.getBlueprint());
                this.setActualBlueprint(StringUtils.isNotBlank(vo.getActualBlueprint()) && vo.getActualBlueprint().equals(VO.STRING_DEFAULT) ? getActualBlueprint() : vo.getActualBlueprint());
                this.setWorksheetId(StringUtils.isNotBlank(vo.getWorksheetId()) && vo.getWorksheetId().equals(VO.STRING_DEFAULT) ? getWorksheetId() : vo.getWorksheetId());
                this.setWorksheet(StringUtils.isNotBlank(vo.getWorksheet()) && vo.getWorksheet().equals(VO.STRING_DEFAULT) ? getWorksheet() : vo.getWorksheet());
                this.setComponentStatus(StringUtils.isNotBlank(vo.getComponentStatus()) && vo.getComponentStatus().equals(VO.STRING_DEFAULT) ? getComponentStatus() : vo.getComponentStatus());
                this.setVersion(StringUtils.isNotBlank(vo.getVersion()) && vo.getVersion().equals(VO.STRING_DEFAULT) ? getVersion() : vo.getVersion());
                this.setBuild(StringUtils.isNotBlank(vo.getBuild()) && vo.getBuild().equals(VO.STRING_DEFAULT) ? getBuild() : vo.getBuild());

                this.setResolveHome(StringUtils.isNotBlank(vo.getResolveHome()) && vo.getResolveHome().equals(VO.STRING_DEFAULT) ? getResolveHome() : vo.getResolveHome());
                this.setSshPort(vo.getSshPort() != null && vo.getSshPort().equals(VO.INTEGER_DEFAULT) ? getSshPort() : vo.getSshPort());
                this.setRemoteUser(StringUtils.isNotBlank(vo.getRemoteUser()) && vo.getRemoteUser().equals(VO.STRING_DEFAULT) ? getRemoteUser() : vo.getRemoteUser());
                this.setRemotePassword(StringUtils.isNotBlank(vo.getRemotePassword()) && vo.getRemotePassword().equals(VO.STRING_DEFAULT) ? getRemotePassword() : vo.getRemotePassword());

                String remotePassword = this.getRemotePassword();
                if (!CryptUtils.isEncrypted(remotePassword))
                {
                    remotePassword = CryptUtils.encrypt(remotePassword);
                    this.setRemotePassword(remotePassword);
                }
                this.setTerminalPrompt(StringUtils.isNotBlank(vo.getTerminalPrompt()) && vo.getTerminalPrompt().equals(VO.STRING_DEFAULT) ? getTerminalPrompt() : vo.getTerminalPrompt());
                this.setPrivateKeyLocation(StringUtils.isNotBlank(vo.getPrivateKeyLocation()) && vo.getPrivateKeyLocation().equals(VO.STRING_DEFAULT) ? getPrivateKeyLocation() : vo.getPrivateKeyLocation());

                InputStream inStream = new ByteArrayInputStream(this.getBlueprint().getBytes());
                com.resolve.util.Properties properties = new com.resolve.util.Properties();

                properties.load(inStream);
                if (StringUtils.isBlank(properties.get("LOCALHOST")))
                {
                    throw new RuntimeException("LOCALHOST property in the blueprint cannot be empty.");
                }
                this.setServerIp(properties.get("LOCALHOST"));
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
}
