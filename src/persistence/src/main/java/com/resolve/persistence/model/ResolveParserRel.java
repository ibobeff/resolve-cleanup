/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolveParserRelVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "resolve_parser_rel", indexes = {@Index(columnList = "u_parser_sys_id", name = "u_parser_rel_sys_id_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveParserRel extends BaseModel<ResolveParserRelVO>
{
    private static final long serialVersionUID = -1208952426462587316L;
    
    private ResolveParser parser;
	private String URefParser; 
	
	public ResolveParserRel()
	{
	}

    public ResolveParserRel(ResolveParserRelVO vo)
    {
        applyVOToModel(vo);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_parser_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveParser getParser()
    {
        return parser;
    }

    public void setParser(ResolveParser assessor)
    {
        this.parser = assessor;
    }

    @Column(name = "u_ref_parser", length = 400)
    public String getURefParser()
    {
        return URefParser;
    }

    public void setURefParser(String refAssessor)
    {
        this.URefParser = refAssessor;
    }

    @Override
    public ResolveParserRelVO doGetVO()
    {
        ResolveParserRelVO vo = new ResolveParserRelVO();
        super.doGetBaseVO(vo);
        
        vo.setURefParser(getURefParser());
        
        //references - do not do this to avoid cyclic ref
//        vo.setAssessor(Hibernate.isInitialized(this.assessor) && getAssessor() != null ? getAssessor().doGetVO() : null);
        
        return vo;
    }

    @Override
    public void applyVOToModel(ResolveParserRelVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo); 

            this.setURefParser(StringUtils.isNotBlank(vo.getURefParser()) && vo.getURefParser().equals(VO.STRING_DEFAULT) ? getURefParser() : vo.getURefParser());
        }
        
        
    }

}
