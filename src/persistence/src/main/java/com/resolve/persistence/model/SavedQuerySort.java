package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.resolve.services.hibernate.vo.SavedQuerySortVO;
import com.resolve.services.vo.QuerySort.SortOrder;

@Entity
@Table(name = "saved_sort")
public class SavedQuerySort extends BaseModel<SavedQuerySortVO> {
	private static final long serialVersionUID = 5678050785606750312L;

	private String property;
	private SortOrder direction;
    private SavedSearch savedSearch;
    
	@Column(name = "property", nullable = false)
	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	@Column(name = "direction", nullable = false)
	public SortOrder getDirection() {
		return direction;
	}

	public void setDirection(SortOrder direction) {
		this.direction = direction;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "saved_search_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
	public SavedSearch getSavedSearch() {
		return savedSearch;
	}

	public void setSavedSearch(SavedSearch savedSearch) {
		this.savedSearch = savedSearch;
	}

	@Override
	public SavedQuerySortVO doGetVO() {
		SavedQuerySortVO vo = new SavedQuerySortVO(property, direction); 
        super.doGetBaseVO(vo);
		return vo;
	}
}
