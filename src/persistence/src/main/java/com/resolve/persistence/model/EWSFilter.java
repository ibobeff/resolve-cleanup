/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.services.hibernate.vo.EWSFilterVO;

/**
 * EWSFilter
 */
@Entity
@Table(name = "ews_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }, name = "ewsf_u_name_u_queue_uk") })
// @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EWSFilter extends GatewayFilter<EWSFilterVO>
{
    private static final long serialVersionUID = 1656838028259102245L;

    private String UQuery;
    private Boolean UIncludeAttachment;
    private String UFolderNames;
    private Boolean UMarkAsRead;
	private Boolean UOnlyNew;
    public EWSFilter()
    {
    } // EWSFilter

    public EWSFilter(EWSFilterVO vo)
    {
        applyVOToModel(vo);
    } // EWSFilter

    @Column(name = "u_query", length = 4000)
    public String getUQuery()
    {
        return this.UQuery;
    } // getUQuery

    public void setUQuery(String uQuery)
    {
        this.UQuery = uQuery;
    } // setUQuery

    @Column(name = "u_include_attachment")
    public Boolean getUIncludeAttachment()
    {
        return this.UIncludeAttachment;
    } // getUIncludeAttachment

    public void setUIncludeAttachment(Boolean uIncludeAttachment)
    {
        this.UIncludeAttachment = uIncludeAttachment;
    } // setUIncludeAttachment
    
    public String getUFolderNames()
    {
        return UFolderNames;
    }
    
    public void setUFolderNames(String uFolderNames)
    {
        UFolderNames = uFolderNames;
    }

    @Column(name = "u_only_new")
    public Boolean getUOnlyNew()
    {
        return this.UOnlyNew;
    } // getUOnlyNew

    public void setUOnlyNew(Boolean uOnlyNew)
    {
        this.UOnlyNew = uOnlyNew;
    } // setUOnlyNew

    @Column(name = "u_mark_as_read")
    public Boolean getUMarkAsRead() {
		return UMarkAsRead;
	}

	public void setUMarkAsRead(Boolean uMarkAsRead) {
		UMarkAsRead = uMarkAsRead;
	}

	@Override
    public EWSFilterVO doGetVO()
    {
        EWSFilterVO vo = new EWSFilterVO();
        super.doGetBaseVO(vo);

        vo.setUQuery(getUQuery());
        vo.setUIncludeAttachment(getUIncludeAttachment());
        vo.setUMarkAsRead(getUMarkAsRead());
        vo.setUFolderNames(getUFolderNames());
		vo.setUOnlyNew(getUOnlyNew());
        return vo;
    }

    @Override
    public void applyVOToModel(EWSFilterVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setUQuery(vo.getUQuery() != null ? vo.getUQuery() : getUQuery());
            this.setUIncludeAttachment(vo.getUIncludeAttachment() != null ? vo.getUIncludeAttachment() : getUIncludeAttachment());
            this.setUFolderNames(vo.getUFolderNames() != null ? vo.getUFolderNames() : getUFolderNames());
            this.setUMarkAsRead(vo.getUMarkAsRead() != null ? vo.getUMarkAsRead() : getUMarkAsRead());
			this.setUOnlyNew(vo.getUOnlyNew() != null ? vo.getUOnlyNew() : getUOnlyNew());
        }
    }
}
