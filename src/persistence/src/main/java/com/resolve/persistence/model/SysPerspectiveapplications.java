/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

// Generated Dec 11, 2009 11:15:20 AM by Hibernate Tools 3.2.4.GA

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.SysPerspectiveapplicationsVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

/**
 * SysPerspectiveapplications generated by hbm2java
 */
@Entity
@Table(name = "sys_perspectiveapplications", indexes = {@Index(columnList = "parent_id", name = "spa_parent_id_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SysPerspectiveapplications extends BaseModel<SysPerspectiveapplicationsVO>
{
    private static final long serialVersionUID = -1643704580295972745L;
    
    private String value;
    private Integer sequence;

    // object references

    // object referenced by
    private SysPerspective sysPerspective; // parentId;

    public SysPerspectiveapplications()
    {
    }

    public SysPerspectiveapplications(SysPerspectiveapplicationsVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "value", length = 40)
    public String getValue()
    {
        return this.value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    /**
     * @Column(name = "parent_id", length = 32) public String getParentId() {
     *              return this.parentId; }
     * 
     *              public void setParentId(String parentId) { this.parentId =
     *              parentId; }
     */

    @Column(name = "sequence")
    public Integer getSequence()
    {
        return this.sequence;
    }

    public void setSequence(Integer sequence)
    {
        this.sequence = sequence;
    }

    //this is EAGER on purpose
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "parent_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public SysPerspective getSysPerspective()
    {
        return sysPerspective;
    }

    public void setSysPerspective(SysPerspective sysPerspective)
    {
        this.sysPerspective = sysPerspective;
        if (sysPerspective != null)
        {
            Collection<SysPerspectiveapplications> coll = sysPerspective.getSysPerspectiveapplications();
            if (coll == null)
            {
				coll = new HashSet<SysPerspectiveapplications>();
	            coll.add(this);
	            
                sysPerspective.setSysPerspectiveapplications(coll);
            }
        }
    }

    @Override
    public SysPerspectiveapplicationsVO doGetVO()
    {
        SysPerspectiveapplicationsVO vo = new SysPerspectiveapplicationsVO();
        super.doGetBaseVO(vo);
        
        vo.setSequence(getSequence());
        vo.setValue(getValue());
        
        return vo;
    }

    @Override
    public void applyVOToModel(SysPerspectiveapplicationsVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setSequence(vo.getSequence() != null && vo.getSequence().equals(VO.INTEGER_DEFAULT) ? getSequence() : vo.getSequence());
            this.setValue(StringUtils.isNotBlank(vo.getValue()) && vo.getValue().equals(VO.STRING_DEFAULT) ? getValue() : vo.getValue());
            
        }
        
    }

}
