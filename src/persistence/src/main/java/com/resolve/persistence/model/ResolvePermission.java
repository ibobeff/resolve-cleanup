/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "resolve_permission")
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolvePermission implements java.io.Serializable
{
    private static final long serialVersionUID = 4785648559180261826L;
    
    private String UTableName;//CR or some other app - table name
    private String UPermissionId;//checksum of the URoles - u_perm_id
    private String URole;//admin OR user 
//    private String URoles;// admin,user, this will be duplicated
//    private String URoleType;//read, write, etc

    // sys fields
    private String sys_id;
    private Date sysCreatedOn;
    private String sysCreatedBy;
    private Date sysUpdatedOn;
    private String sysUpdatedBy;
    private Integer sysModCount;
    private String sysPerm;//sysPerm - sys_perm
    private String sysOrg;//sysOrg - sys_org

    // object references

    // object referenced by

    public ResolvePermission()
    {
    }

    @Id
    @Column(name = "sys_id", nullable = false, length = 32)
    @GeneratedValue(generator = "hibernate-uuid")
    public String getSys_id()
    {
        return sys_id;
    }

    public void setSys_id(String sys_id)
    {
        this.sys_id = sys_id;
    }

    @Column(name = "u_role")
    public String getURole()
    {
        return URole;
    }

    public void setURole(String uRole)
    {
        URole = uRole;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sys_created_on", length = 19)
    public Date getSysCreatedOn()
    {
        return this.sysCreatedOn;
    }

    public void setSysCreatedOn(Date sysCreatedOn)
    {
        this.sysCreatedOn = sysCreatedOn;
    }

    @Column(name = "sys_created_by")
    public String getSysCreatedBy()
    {
        return this.sysCreatedBy;
    }

    public void setSysCreatedBy(String sysCreatedBy)
    {
        this.sysCreatedBy = sysCreatedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sys_updated_on", length = 19)
    public Date getSysUpdatedOn()
    {
        return this.sysUpdatedOn;
    }

    public void setSysUpdatedOn(Date sysUpdatedOn)
    {
        this.sysUpdatedOn = sysUpdatedOn;
    }

    @Column(name = "sys_updated_by")
    public String getSysUpdatedBy()
    {
        return this.sysUpdatedBy;
    }

    public void setSysUpdatedBy(String sysUpdatedBy)
    {
        this.sysUpdatedBy = sysUpdatedBy;
    }

    @Column(name = "sys_mod_count")
    public Integer getSysModCount()
    {
        return this.sysModCount;
    }

    public void setSysModCount(Integer sysModCount)
    {
        this.sysModCount = sysModCount;
    }

    @Column(name = "u_table_name")
    public String getUTableName()
    {
        return UTableName;
    }

    public void setUTableName(String uTableName)
    {
        UTableName = uTableName;
    }

    @Column(name = "u_perm_id")
    public String getUPermissionId()
    {
        return UPermissionId;
    }

    public void setUPermissionId(String uPermissionId)
    {
        UPermissionId = uPermissionId;
    }

    @Column(name = "sys_perm")
    public String getSysPerm()
    {
        return sysPerm;
    }

    public void setSysPerm(String sysPerm)
    {
        this.sysPerm = sysPerm;
    }

    @Column(name = "sys_org")
    public String getSysOrg()
    {
        return sysOrg;
    }

    public void setSysOrg(String sysOrg)
    {
        this.sysOrg = sysOrg;
    }
    
    

}
