/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;
import javax.transaction.TransactionManager;
import javax.transaction.UserTransaction;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.CallbackException;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.ReplicationMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.c3p0.internal.C3P0ConnectionProvider;
import org.hibernate.cfg.Configuration;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.mapping.Column;
import org.hibernate.mapping.PersistentClass;
import org.hibernate.mapping.Property;
import org.hibernate.mapping.RootClass;
import org.hibernate.mapping.Table;
import org.hibernate.query.Query;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;
import org.hibernate.tool.schema.TargetType;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;
import com.mchange.v2.c3p0.C3P0Registry;
import com.mchange.v2.c3p0.PoolBackedDataSource;
import com.resolve.esb.ESB;
import com.resolve.persistence.dao.DAOFactory;
import com.resolve.persistence.dao.ResolveSequenceGeneratorDAO;
import com.resolve.persistence.model.BaseModel;
import com.resolve.persistence.model.ResolveSequenceGenerator;
import com.resolve.persistence.model.ResolveSharedObject;
import com.resolve.rsbase.ConfigSQL;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.util.Constants;
import com.resolve.util.DBPoolStatusProvider;
import com.resolve.util.DBPoolUtils;
import com.resolve.util.ERR;
import com.resolve.util.ExecutableObject;
import com.resolve.util.FileUtils;
import com.resolve.util.JavaUtils;
import com.resolve.util.Log;
import com.resolve.util.ResolveDefaultValidator;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;
public class HibernateUtil
{
    public static final String DB_TYPE = "dbtype";
    public static final String DB_NAME = "resolve";
    public static final String DB_USERNAME = "username";
    public static final String DB_P_ASSWORD = "password";
    public static final String DB_URL = "url";
    public static final String DB_HOST = "localhost";
    public static final String DB_POOL_SIZE = "poolsize";
    public static final String DB_TIMEOUT = "timeout";
    public static final String DB_MIN_POOLSIZE = "minPoolSize";
    public static final String DB_MAX_IDLE_TIME = "maxIdleTime";
    public static final String DB_USE_XA_DATASOURCE = "useXADatasource";

    private static final String DB_CACHE_LISTENER_LIST = "_db_cache_listener_list_";
    protected static final String DB_CACHE_REGION = "_db_cache_region_name";
    protected static final String DB_CACHE_KEY = "_db_cache_key";
    protected static final String HB_CACHE_REGION = "_hibernate_cache_region_name";
    protected static final String CACHED_USER_NAME = "_cached_user_name";
    protected static final String SESSION_FACTORY_XPATH = "/hibernate-configuration/session-factory";
    protected static final String MAPPING_XPATH = SESSION_FACTORY_XPATH + "/mapping";
    protected static final String MAPPING_PACKAGE_ATTRIBUTE_XPATH = "mapping/@package";
    protected static final String MAPPING_PACKAGE_ATTRIBUTE_VALUE = "com.resolve.persistence.model";
    protected static final String MAPPING_CLASS_ATTRIBUTE_XPATH = "mapping/@class";
    protected static final String USERS_MAPPING_CLASS_ATTRIBUTE_VALUE = MAPPING_PACKAGE_ATTRIBUTE_VALUE + ".Users";
    
    private static final String PERSISTENCE_MODEL_PACKAGE = BaseModel.class.getPackage().getName();
    
    private static ConcurrentHashMap<String, String> class2TableMap = new ConcurrentHashMap<String, String>();
    private static ConcurrentHashMap<String, String> table2ClassMap = new ConcurrentHashMap<String, String>();
    private static ConcurrentHashMap<String, String> tableNameLC2TCMap = new ConcurrentHashMap<String, String>();
    private static Map<String, Map<String, String>> property2Column = new ConcurrentHashMap<String, Map<String, String>>();
    private static Map<String, Map<String, String>> column2Property = new ConcurrentHashMap<String, Map<String, String>>();
    private static Map<String, Map<String, String>> modelColumnTypeMap = new ConcurrentHashMap<String, Map<String, String>>();
    private static List<String> modelClassNames = new ArrayList<String>();
    private static List<String> entityNames = new ArrayList<String>();
    private static ConcurrentHashMap<DBCacheRegionConstants, ConcurrentHashMap<String, Object>> DBObjectsCache = 
    						new ConcurrentHashMap<DBCacheRegionConstants, ConcurrentHashMap<String, Object>>();
    private static final ConcurrentMap<String, String> gatewayModelFilterMap = new ConcurrentHashMap<String, String>();

    private static Configuration configuration;
    private static SessionFactory sessionFactory;
    private static UserTransaction userTransaction;
    private static TransactionManager transManager;
    private static DAOFactory daoFactory;
    private static String configFile;
    private static AtomicInteger transactionCount = new AtomicInteger(0);
    private static boolean doSchemaUpdate;
    public static ConfigSQL configSQL;
    private static ReadWriteLock initLock = new ReentrantReadWriteLock();
    private static AtomicBoolean cacheNotificationStatus = new AtomicBoolean(true);
    private static boolean debugOutput = false;
    private static boolean stacktraceOutput=false;
    private static AtomicBoolean isInitInProgress = new AtomicBoolean(false);
    public static int refreshInterval = 600;
    private static boolean isOracle;
    private static boolean isMySQL;
    private static Metadata metadata;
    private static StandardServiceRegistry standardRegistry;
    private static Configuration customTablesOnlyConfiguration;
    private static Metadata customTablesOnlyMetadata;
    private static StandardServiceRegistry customTablesOnlyRegistry;
    
    private static DBPoolStatusProvider dbProvider = new DBPoolStatusProvider()
    {
    	@Override
        public int getMaxPoolSize()
        {
    	  return configSQL.getMaxpoolsize();
        }
        
        @Override
        public int getPoolSize()
        {
          try {
            return getCurrentPoolDataSource().getNumConnectionsDefaultUser();
          } catch (SQLException e) {
            Log.log.error(e.getMessage(), e);
            return 0;
          }
        }

		@Override
		public int getAvailableSize() {
          try {
            return getCurrentPoolDataSource().getNumIdleConnectionsDefaultUser();
          } catch (SQLException e) {
            Log.log.error(e.getMessage(), e);
            return 0;
          }
        }

		//Returns # of currently allocated connections
		@Override
		public int getTotalSize() {
          try {
            return getCurrentPoolDataSource().getNumConnectionsDefaultUser();
          } catch (SQLException e) {
            Log.log.error(e.getMessage(), e);
            return 0;
          }
		}
		
        private PoolBackedDataSource getCurrentPoolDataSource() {
          Set allPooledDataSources = C3P0Registry.getPooledDataSources();
          if(allPooledDataSources.isEmpty()) {
            Log.log.error("There is no pooled data source registerd. Check your c3p0 pool configurations.");
          }
          return (PoolBackedDataSource)allPooledDataSources.iterator().next();
        }
    };

    private static ThreadLocal<Integer> nestedTransactionCount = new ThreadLocal<Integer>()
    {
        @Override
        protected synchronized Integer initialValue()
        {
            return 0;
        }
    };
    
    
    private static ThreadLocal<Map<String,Object>> transactionInfo = new ThreadLocal<Map<String,Object>>()
    {
        @Override
        protected synchronized Map<String,Object> initialValue()
        {
            return new HashMap<String,Object>();
        }
    };
    
    private static ThreadLocal<Boolean> rollbackOnly = new ThreadLocal<Boolean>()
    {
        @Override
        protected synchronized Boolean initialValue()
        {
            return false;
        }
    };

    protected static ThreadLocal<User> currentUser = new ThreadLocal<User>()
    {
        @Override
        protected synchronized User initialValue()
        {
            return new User("system"); // default user
        }
    };

    public static User getCurrentUser()
    {
        return currentUser.get();
    }

    public static void setCurrentUser(User u)
    {
        currentUser.set(u);
    }
    
    public static void setCurrentUserByUsername(String username)
    {
        setCurrentUser(new User(username));
    }

    private static String databaseName;

    private static ESB esb;
    private static String guid;
    private static String customTableXml;
//	private static Session session;

    public static ESB getEsb()
    {
        return esb;
    }

    public static void setEsb(ESB e, String id)
    {
        esb = e;
        guid = id;
    }

    public static String getCustomTableXml()
    {
        return customTableXml;
    }

    public static void setCustomeTableXml(String xml)
    {
        customTableXml = xml;
    }

    public static String getGuid()
    {
        return guid;
    }

    @Deprecated
    public static javax.transaction.Transaction getCurrentTransaction()
    {
        Log.log.warn("Transaction Management Deprecated, Current Transaction Is Null");
        javax.transaction.Transaction tr = null;
        return tr;
    }


    public synchronized static void init(String cfg)
    {
        init(cfg, true, false);
    } // init

    public synchronized static void init(String cfg, boolean doUpdate, boolean customTablesOnly)
    {
        configFile = cfg;
        doSchemaUpdate = doUpdate;
        try
        {
            init(doUpdate, true, customTablesOnly);
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);

            if (StringUtils.isNotBlank(CustomTableMappingUtil.getCustomMappingFile())) {
            	// removing custom tables mapping and reinit
            	File file = FileUtils.getFile(CustomTableMappingUtil.getCustomMappingFile());
            	if (file.exists()) {
            		Log.log.warn("Removing custom table mapping file: " + CustomTableMappingUtil.getCustomMappingFile());
            		file.delete();

            		Log.log.warn("Reinitializing Hibernate");
            		init(doUpdate, false, false);
            	}
            }
        }

        initLocals();
        initClassMapping();
        //		initClusterCache();
        //		initLocalCache();

        registerHibernateCacheUpdaters();
    } // init
    
    private synchronized static void initLocals()
    {
        class2TableMap = new ConcurrentHashMap<String, String>();
        table2ClassMap = new ConcurrentHashMap<String, String>();
        tableNameLC2TCMap = new ConcurrentHashMap<String, String>();
        property2Column = new ConcurrentHashMap<String, Map<String, String>>();
        column2Property = new ConcurrentHashMap<String, Map<String, String>>();
        modelClassNames = new ArrayList<String>();
        entityNames = new ArrayList<String>();
    }

    @SuppressWarnings({ "unchecked" })
    private synchronized static void initClassMapping()
    {
        if (configuration == null)
        {
            return;
        }

        Iterator<PersistentClass> cit = getClassMappings().iterator();
        RootClass rc = null;
        String cls = null;
        String table = null;
        String[] names = null;
        Type[] types = null;
        while (cit.hasNext())
        {
            rc = (RootClass) cit.next();
            cls = rc.getClassName();
            if (cls == null)
            {
                cls = rc.getEntityName();
                if (cls == null) continue;
            }
            table = rc.getTable().getName();
            class2TableMap.put(cls, table.toLowerCase());
            table2ClassMap.put(table.toLowerCase(), cls);
            tableNameLC2TCMap.put(table.toLowerCase(), table);
            
            if(isOracle)
            {
                try
                {
                    Iterator<Property> it = getClassMapping(Class.forName(cls).getName()).getPropertyIterator();
                    /*Iterable<Property> iterable = () -> it;
                    
                    names = StreamSupport.stream(iterable.spliterator(), false).map(Property::getName).toArray(String[]::new);
                    types = StreamSupport.stream(iterable.spliterator(), false).map(Property::getType).toArray(Type[]::new);
                    */
                    List<String> namesList = new ArrayList<>();
                    List<Type> typesList = new ArrayList<>();
                    while (it.hasNext()) {
                        Property prop = it.next();
                        namesList.add(prop.getName());
                        typesList.add(prop.getType());
                    }
                    
                    names = namesList.toArray(new String[namesList.size()]);
                    types = typesList.toArray(new Type[typesList.size()]);
                }
                catch (HibernateException e)
                {
                    Log.log.debug("Error while getting metadata for model: " + cls);
                }
                catch (ClassNotFoundException e)
                {
                	Log.log.debug("Found custom table " + cls);
                }
                
                if (names != null && names.length > 0 && types != null && types.length > 0)
                {
                    Map<String,String> map = new HashMap<String, String>();
                    for (int i = 0; i<names.length; i++)
                    {
                        map.put(names[i], types[i].getName());
                    }
                    modelColumnTypeMap.put(cls, map);
                }
            }
            
            // Initialize gateway name to gateway filter model map eagerly
            
            if (cls.startsWith(PERSISTENCE_MODEL_PACKAGE) && 
            	(cls.endsWith("Filter") || cls.endsWith("DatabaseConnectionPool") || cls.endsWith("EmailAddress") || 
                 cls.endsWith("EWSAddress") || cls.endsWith("RemedyxForm") || cls.endsWith("SSHPool") || cls.endsWith("TelnetPool") ||
            	 cls.endsWith("XMPPAddress"))) {            	
            	String filterModel = null;

            	if (cls.indexOf("Filter") > 0) {
            		filterModel = cls.substring(cls.lastIndexOf('.') + 1, cls.indexOf("Filter"));
            	} else {
            		filterModel = cls.substring(cls.lastIndexOf('.') + 1);
        }
            	
            	// Exchange gateway filter model is ExchangeServerFilter
            	switch (filterModel.toLowerCase()) {
            		case "exchangeserver":
            			filterModel = "exchange";
            			break;
            		case "sshpool":
            			filterModel = "ssh";
            			break;
                    case "telnetpool":
                        filterModel = "telnet";
                        break;
            		default:
            			break;
    }
            	
                gatewayModelFilterMap.put(filterModel.toLowerCase(), cls);
                
                // DatabaseFilter model is associated with "db" and "database" gateways
                if (filterModel.toLowerCase().equals("database")) {
                	gatewayModelFilterMap.put("db", cls);
                }
            }
        }
        
        if (Log.log.isDebugEnabled() && MapUtils.isNotEmpty(gatewayModelFilterMap)) {
        	Log.log.debug(String.format("Gateway Model Filter Map Size: %d, Entries:[%s]", 
        								gatewayModelFilterMap.size(), 
        								StringUtils.mapToString(gatewayModelFilterMap, "=", ", ")));
        }
    }

    public static synchronized void closeSessionFactory()
    {
        if (sessionFactory != null && !sessionFactory.isClosed())
        {
            Log.log.info("Close current hibernate SessionFactory...");
            closeC3P0();
            sessionFactory.close();
        }
    }

    private static void closeC3P0() {
      Set<PoolBackedDataSource> allPooledDataSources = C3P0Registry.getPooledDataSources();
      for(PoolBackedDataSource e : allPooledDataSources) {
        e.close();
      }
      if(sessionFactory instanceof SessionFactoryImpl) {
        ConnectionProvider cp = getSessionFactory().getSessionFactoryOptions().getServiceRegistry().getService(ConnectionProvider.class);
        if(cp instanceof C3P0ConnectionProvider) { 
          ((C3P0ConnectionProvider)cp).stop(); 
        }
       }
    }

    //Close preivous session factory and create new one.
    public static synchronized void reinit()
    {
        reinitInternal(false);
    }

    // HP TO DO Why should this be synchronized when it uses locks serializing access to Hibernate re-initialization?
    
    static synchronized void reinitInternal(boolean fromCluster)
    {
    	/*
    	 * initLock protects against two different RSView threads (i.e. users) performing concurrent Hibernate re-initialization
    	 * from same RSView.
    	 * 
    	 * Currently Resolve is not protected against Hibernate re-initialization if invoked concurrently from multiple RSViews
    	 * in cluster. There is a corner case of same table name with same column name from two different user performing
    	 * custom table save connected to different RSViews in cluster.
    	 * 
    	 * When new custom form with file upload field is saved, it first generates file upload table and calls Hibernate 
    	 * re-initialize. Once first calls returns it then again calls Hibernate re-initialize for custom table.
    	 * 
    	 * HP TO DO 
    	 * 
    	 * Identify how it can be speeded up. Takes around 15 to 20 seconds to finish even with update affecting only custom table 
    	 * and not performing any re-index for Resolve tables.
    	 * 
    	 * Using isHibernateInitiazing2() in methods which perform cache clear calls to come out without clearing Hibernate 
    	 * cache if Hibernate re-initialization is in progress.
    	 * Please check clearCachedUser(). This call was introduced for package manager to support packages 
    	 * containing custom tables which result in Hibernate re-initialization(s) while package is being imported.
    	 * During Hibernate re-initialization Mxxx methods accessing DB use the new beginTransaction2, commitTransaction2
    	 * and rollbackTransaction2 calls which serializes access to DB in light of  Hibernate re-initialization.
    	 * For example check ArchiveWiki.addArchive(). 
    	 * 
    	 * isHiberateInitializing2() tries to lock initLock without blocking and if it cannot then it returns 
    	 * Hibernate is initializing. If it can lock the initLock then it returns true state of isInitInProgress flag.
		 * 
    	 */
        Lock lock = initLock.writeLock();
        lock.lock();
        
        try
        {
        	Log.log.info("HibernateUtil.reinit() is called to initialize Hibernate!, obtained write lock!!");
        	
        	int retryCount = 8;
        	
        	while (retryCount > 0 && !isInitInProgress.compareAndSet(false, true)) {
        		try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// Ignore the exception
				}
        		
        		--retryCount;
        	}
        	
        	if (retryCount == 0) {
        		Log.log.warn("Waited for 40 seconds for previous Hibernate re-initialization to complete, " +
        					 "forcing re-intialization!!!");	
        	}
        	
        	if (!isInitInProgress.get()) {
        		isInitInProgress.set(true);
        	}
        	
            HibernateUtil.closeSessionFactory();
            HibernateUtil.init(configFile, doSchemaUpdate, true);

            if (!fromCluster)
            {
                broadcastCustomTableMapping();
            }
            
            isInitInProgress.set(false);
        }
        finally
        {
        	if (lock != null) {
        		lock.unlock();
        		Log.log.info("Hibernate session factory re-initialization is done!");
        	}
        }
    }

    private static synchronized void broadcastCustomTableMapping()
    {
        if (esb == null)
        {
            return;
            //throw new RuntimeException("Error: esb is not initialized");
        }

        if (HibernateUtil.getCustomTableXml() == null)
        {
            throw new RuntimeException("Error: custom table mapping xml is null");
        }

        Map<String, String> msg = new HashMap<String, String>();
        msg.put(Constants.SOURCE_GUID, guid);
        msg.put(Constants.CUSTOM_TABLE_XML, HibernateUtil.getCustomTableXml());

        esb.sendInternalMessage(Constants.ESB_NAME_RSVIEWS, "com.resolve.persistence.util.HibernateUtilHelper.reinitHibernate", msg);
        esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROLS, "com.resolve.persistence.util.HibernateUtilHelper.reinitHibernate", msg);
    }

    private static synchronized Metadata getMetadata() {
        return getMetadata(false);
    }
    
    private static synchronized Metadata getMetadata(boolean reset) {
    	Log.log.trace(String.format("getMetadata(%b), standardRegistry is %snull", reset, 
    							    (standardRegistry != null ? "NOT " : "")));
    	
        if (!reset && metadata != null) {
            return metadata;
        }
        
        if (configuration == null) {
            return null;
        }
        
         MetadataSources sources = new MetadataSources(standardRegistry=(configuration.getStandardServiceRegistryBuilder()
        												                 .applySettings(configuration.getProperties())
        												                 .build()));
        
        if (StringUtils.isNotBlank(CustomTableMappingUtil.getCustomMappingFile())) {
        	sources.addFile(FileUtils.getFile(CustomTableMappingUtil.getCustomMappingFile()));
        }
        metadata = sources.buildMetadata(standardRegistry);
        
        if (StringUtils.isNotBlank(CustomTableMappingUtil.getCustomMappingFile())) {
	        // Build Custom Table Only Metadata
	        
	        MetadataSources customTableSources = new MetadataSources(
	        												customTablesOnlyRegistry = (
	        														customTablesOnlyConfiguration
	        														.getStandardServiceRegistryBuilder()
	        													    .applySettings(customTablesOnlyConfiguration.getProperties())
	        													    .build()));
	        customTableSources.addFile(CustomTableMappingUtil.getCustomMappingFile());
	        customTablesOnlyMetadata = customTableSources.buildMetadata(customTablesOnlyRegistry);
	        
	//        customTablesOnlyRegistry = new StandardServiceRegistryBuilder()
	//        								.applySettings(customTablesOnlyConfiguration.getProperties()).build();
	//        
	//        MetadataSources customTableSources = new MetadataSources(customTablesOnlyRegistry);
	//        customTableSources.addFile(CustomTableMappingUtil.getCustomMappingFile());
	//        customTablesOnlyMetadata = customTableSources.buildMetadata(customTablesOnlyRegistry);
        }
        
        return metadata;
    }
    
    public static Collection<PersistentClass> getClassMappings() {
        if (getMetadata() == null) {
            return Collections.emptyList();
        }
        return getMetadata().getEntityBindings();
    }
    
    public static PersistentClass getClassMapping(String className) {
        if (getMetadata() == null) {
            return null;
        }

        return getMetadata().getEntityBinding(className);
    }

    public static Collection<Table> getTableMappings() { 
        List<Table> tables = new ArrayList<>();
        for (PersistentClass pc : getClassMappings()) {
            tables.add(pc.getTable());
        }
        
        return tables;
    }    
    
    @SuppressWarnings("unchecked")
	private static synchronized void init(boolean doUpdate, boolean throwException, boolean customTablesOnly)
    {
        File cfg = null;
        if (configFile == null || !(cfg = FileUtils.getFile(configFile)).exists())
        {
            throw new RuntimeException(String.format("Fatal error: 'hibernate.cfg.xml' file is misssing - using: %s", 
            										 configFile));
        }

        // Create the initial SessionFactory from the default configuration files
        Log.log.debug(String.format("Initializing Hibernate... Update Schema: %b, Throw Exception: %b, " +
        						    "Update Custom Table Schema Only: %b", doUpdate, throwException, customTablesOnly));

		// Read hibernate.properties, if present
		String url = configSQL.getUrl();
 
		if (StringUtils.isEmpty(url)) {
			String dbType = configSQL.getDbtype();
			
			if (dbType.contains("oracle")) {
				url = getOracleUrl();
			} else if (dbType.equalsIgnoreCase("mysql")) {
				url = getMySQLUrl();
			} else {
				throw new RuntimeException("Unsupported database type: " + dbType);
			}
		}

		configuration = new Configuration().configure(cfg);
		configuration.setProperty("hibernate.connection.url", url);
		configuration.setProperty("hibernate.connection.username", configSQL.getUsername());
		configuration.setProperty("hibernate.connection.password", configSQL.getP_assword());
		
		//c3p0 connection pool configuration
        configuration.setProperty("hibernate.c3p0.min_size", Integer.toString(configSQL.getMinpoolsize()));
        configuration.setProperty("hibernate.c3p0.max_size", Integer.toString(configSQL.getMaxpoolsize()));
        
        configuration.setProperty("hibernate.c3p0.statementCacheNumDeferredCloseThreads", "1");
        
        configuration.setProperty("hibernate.c3p0.timeout", Integer.toString(configSQL.getTimeout()));
        configuration.setProperty("hibernate.c3p0.maxIdleTimeExcessConnections", Integer.toString(configSQL.getMaxIdleTimeExcessConnections()));
	
		/*
         * Loading of custom tables in Hibernate is optional as all components do not use custom tables for example
         * RSArchive.
         * 
         * Components not using custome tables can pass empty or null custom table file location while initializing 
         * Hibernate (persistence).
         *  
         * In such cases skip adding custom tables to Hibernate.
         */
        if (StringUtils.isNotBlank(CustomTableMappingUtil.getCustomMappingFile())) {
        	// create empty custom mapping file if not exists 
        	File file = FileUtils.getFile(CustomTableMappingUtil.getCustomMappingFile());
        	if (!file.exists()) {
        		CustomTableMappingUtil.createEmptyCustomMappingFile();
        	}

        	configuration.addFile(CustomTableMappingUtil.getCustomMappingFile());
        	//configuration.addCacheableFile(CustomTableMappingUtil.getCustomMappingFile());
        }

        configuration.setInterceptor(new ResolveInterceptor());

        if (StringUtils.isNotBlank(CustomTableMappingUtil.getCustomMappingFile())) {
	        /*
	         * Create Custom Table specific only Configuration from hibernate.cfg.xml by removing all 
	         * /hibernate-configuration/session-factory/mapping
	         */
	        
	        File noMappingsCfg = null;
	        String configNoMappingsFile = StringUtils.substringBeforeLast(configFile, ".cfg.xml") + "-nomappings.cfg.xml";
	        
	        /*
	         * Create hibernate-nomappings.cfg.xml from hibernate.cfg.xml if not present or hibernate.cfg.xml last modified 
	         * time is after hibernate-nomappings.cfg.xml last updated time.
	         */
	        
	        
	        if (!(noMappingsCfg = FileUtils.getFile(configNoMappingsFile)).exists() || 
	        	cfg.lastModified() > noMappingsCfg.lastModified()) {
	        	XDoc defaultCfg = null;
	        	
	        	try {
					defaultCfg = new XDoc(cfg, true);
					defaultCfg.removeElements(MAPPING_XPATH);
									
					if (noMappingsCfg != null && noMappingsCfg.exists()) {
						noMappingsCfg.delete();
					}
					
					noMappingsCfg = new File(configNoMappingsFile);
					defaultCfg.toPrettyFile(noMappingsCfg);
				} catch (Exception e) {
					String errMsg = String.format("Error reading XML Document from %s", cfg.getAbsolutePath());
					Log.log.error(errMsg, e);
					throw new RuntimeException(errMsg);
				}        
	        }
	        
	        if (configNoMappingsFile == null || !(noMappingsCfg = FileUtils.getFile(configNoMappingsFile)).exists()) {
	            throw new RuntimeException(String.format("Fatal error: 'hibernate-nomappings.cfg.xml' file is misssing " +
	            										 "- using: %s", configNoMappingsFile));
	        }
	                
	        customTablesOnlyConfiguration = new Configuration().configure(noMappingsCfg);//.configure(customTablesCfg);
	        customTablesOnlyConfiguration.addFile(CustomTableMappingUtil.getCustomMappingFile());
	        
	        customTablesOnlyConfiguration.setProperties(configuration.getProperties());
        }        
        Metadata md = getMetadata(true);
        
        if (doUpdate)
        {
            SchemaUpdate schemaUpdate = new SchemaUpdate();
            
            if (customTablesOnly && customTablesOnlyMetadata != null) {
            	schemaUpdate.setHaltOnError(false);
            	schemaUpdate.execute(EnumSet.of(TargetType.DATABASE, TargetType.STDOUT), customTablesOnlyMetadata);
            	List<? extends Exception> schemaUpdateExceptions = schemaUpdate.getExceptions();
            	
            	if (CollectionUtils.isNotEmpty(schemaUpdateExceptions)) {
            		schemaUpdateExceptions.stream().forEach(ex -> Log.log.warn(ex.getMessage()));
            	}
            } else {
            	schemaUpdate.execute(EnumSet.of(TargetType.DATABASE, TargetType.STDOUT), md);
            }
        }
        
        // Build and store (either in JNDI or static variable)
        try
        {
            rebuildSessionFactory(configuration);
        }
        


        catch (Exception e)
        {
            Log.log.error("Error during the Hibernate.init. This may be due to a corrupt custom table. Please verify the last import operation with custom tables.", e);
            if(throwException)
            {
                throw new RuntimeException(e);
            }
        }

        daoFactory = DAOFactory.instance();
        
        /*
         * Note by HP
         * 
         * Cause:
         * 
         * Current custom tables feature of "referenced tables" allows building relationship between any custom table and 
         * Resolve tables.
         * 
         * Effects:
         * 
         * #1 Prohibits separating Resolve schema and custom tables without breaking custom tables 
         *    "referenced tables" feature. 
         * 
         * #2 Prohibits using Resolve and Custom Tables specific Hibernate Session factories. Limits Resolve to
         *    single Hibernate Session factory serving Resolve & Custom Tables severely affecting security.
         *    
         *    Example : Custom Form (with associated custom table) referencing Resolve's sys_user table which contains
         *              user private info such as answers to forgot password questions etc.
         *
         * #3 Create/Update of custom tables requires re-initialization of Hibernate Session Factory at runtime to allow
         *    use of new/updated custom tables accessible from the application without restarting Resolve application.
         *    
         *    Prohibits use of DB by other asynchronous processes such as scheduled jobs or message processing while
         *    Hibernate is getting re-initialized and new session factory is getting built.
         *    
         * #4 Forces synchronization of Hibernate Session Factory between asynchronous processes and Custom Tables 
         *    create/update forced Hibenrate re-initialization. Without synchronization asynchronous processing 
         *    will fail during Hibernate re-initialization.
         */
        
        Log.log.debug("Hibernate initialized");
    }

    /**
     * This method prepares a hibernate query based on the data passed
     * through the QueryDTO. It also makes sure all the parameters are properly
     * added with their data types.
     *
     * @param query
     * @return
     * @throws Exception 
     * @throws HibernateException 
     */
    
    @SuppressWarnings("rawtypes")
    public static Query createQuery(QueryDTO query) throws Exception
    {
        Query hqlQuery = HibernateUtil.createQuery(query.getSelectHQL());
        hqlQuery.setProperties(query.getParams());
        
        // Add in all sort parameters
        for (QuerySort qsort : query.getParsedSorts()/*getSortItems()*/)
        {
            hqlQuery.setParameter(QuerySort.SORT_PROP_NAME_PREFIX + qsort.getProperty().replaceAll("\\.", "_"),
                                  qsort.getProperty());
        }
        
        return hqlQuery;
    }

    @SuppressWarnings("rawtypes")
    public static Query createQuery(String query) throws Exception
    {       
        return getSessionFactory().getCurrentSession().createQuery(createQueryString(query));
    }
    
    @SuppressWarnings("rawtypes")
    public static String createQueryString(String query) throws Exception
    {
        Log.log.trace("createQuery query : [" + query + "]");
        
        if (StringUtils.isEmpty(query))
        {
            throw new RuntimeException ("Invalid statement");
        }
        
        String strippedQuery = StringUtils.stripToEmpty(query);
        
        if (StringUtils.isEmpty(strippedQuery))
        {
            throw new RuntimeException ("Invalid statement");
        }
        
        String strippedLowerCaseQuery = strippedQuery.toLowerCase();
        
        Log.log.trace("createQuery strippedQuery, strippedLowerCaseQuery : [" + 
                      strippedQuery + "], [" + strippedLowerCaseQuery + "]");
        
        String txtB4ModelName = null;
        String sqlCommand = null;
        
        if (strippedLowerCaseQuery.startsWith("select ") && strippedLowerCaseQuery.length() > 8)
        {
            sqlCommand = strippedQuery.substring(0, 7);
            txtB4ModelName = " from ";
        }
        else if (strippedLowerCaseQuery.startsWith("from ") && strippedLowerCaseQuery.length() > 6)
        {
            sqlCommand = strippedQuery.substring(0, 5);
            //txtB4ModelName = "from ";
        }
        else if (strippedLowerCaseQuery.startsWith("delete ") && strippedLowerCaseQuery.length() >= 8)
        {
            sqlCommand = strippedQuery.substring(0, 7);
            
            if (strippedLowerCaseQuery.startsWith("delete from ") && strippedLowerCaseQuery.length() > 13)
            {
                txtB4ModelName = " from ";
            }
        }
        else if (strippedLowerCaseQuery.startsWith("update "))
        {
            sqlCommand = strippedQuery.substring(0, 7);
        }
        
        if (StringUtils.isBlank(sqlCommand))
        {
            Log.log.error("Unable to identify sql or hql command in the query [" + query + "]");
            throw new RuntimeException ("Invalid statement");
        }
        
        Log.log.trace("createQuery sqlCommand : [" + sqlCommand + "]");
        
        if (StringUtils.isBlank(txtB4ModelName))
        {
            txtB4ModelName = sqlCommand;
        }
        
        Log.log.trace("createQuery txtB4ModelName : [" + txtB4ModelName + "], length = " + txtB4ModelName.length());
        
        int modelNameStartIndx = strippedLowerCaseQuery.indexOf(txtB4ModelName) + txtB4ModelName.length();
        int modelNameEndIndx = strippedQuery.indexOf(" ", modelNameStartIndx + 1);
        
        Log.log.trace("createQuery modelNameStartIndx,  modelNameEndIndx: [" + 
                      modelNameStartIndx + "], [" + modelNameEndIndx + "]");
        
        String nameToCheck = modelNameEndIndx > modelNameStartIndx ? 
                             strippedQuery.substring(modelNameStartIndx, modelNameEndIndx) : 
                             strippedQuery.substring(modelNameStartIndx);
        
        if (StringUtils.isBlank(nameToCheck))
        {
            Log.log.error("Unable to identify table name or model name [" + nameToCheck + "] in the query [" + query + "]");
            throw new RuntimeException ("Invalid statement");
        }
        
        nameToCheck = StringUtils.strip(nameToCheck);
        
        Log.log.trace("createQuery nameToCheck : [" + nameToCheck + "]");
        
        String queryB4ModelName = strippedQuery.substring(0, modelNameStartIndx);
        String queryAfterModelName = StringUtils.substringAfter(strippedQuery.substring(modelNameStartIndx), nameToCheck);
        
        Log.log.trace("createQuery queryB4ModelName, queryAfterModelName : [" + 
                      queryB4ModelName + "], [" + queryAfterModelName + "]");
                
        boolean isModelName = modelExists(nameToCheck);
        boolean isTableName = tableExists(nameToCheck);
        boolean isCustomTableOrModelName = CustomTableMappingUtil.modelExists(nameToCheck) || 
        								   CustomTableMappingUtil.tableExists(nameToCheck);
        
        String tableNameToCheck = isTableName ? nameToCheck : null;
        String modelNameToCheck = isModelName ? nameToCheck : null;
        String customTableOrModelNameToCheck = isCustomTableOrModelName ? nameToCheck : null;
        
        String santizedTableOrModelName = null;
        
        if (isModelName)
        {
            String tableNameKey = class2Table(modelNameToCheck);
            
            if (StringUtils.isEmpty(tableNameKey))
            {
                //Log.log.warn("Could not find table name corresponding to model " + modelName);
                //tableName = modelName;
                Log.log.error("Could not find table name for model name [" + modelNameToCheck + "]");
                throw new RuntimeException ("Invalid statement");
            }
            
            santizedTableOrModelName = table2Class(tableNameKey);
        }
        else if (isTableName)
        {
            String modelNameKey = table2Class(tableNameToCheck);
            
            if (StringUtils.isEmpty(modelNameKey))
            {
                Log.log.error("Could not find model name for table name [" + tableNameToCheck + "]");
                throw new RuntimeException ("Invalid statement");
            }
            
            santizedTableOrModelName = class2TableTrueCase(modelNameKey);
        }
        
        if (isCustomTableOrModelName && StringUtils.isBlank(santizedTableOrModelName))
        {
            String modelNameKey = CustomTableMappingUtil.table2Class(customTableOrModelNameToCheck);
            
            if (StringUtils.isEmpty(modelNameKey))
            {
                Log.log.error("Could not find custom model name for table name [" + customTableOrModelNameToCheck + 
                              ", or " + customTableOrModelNameToCheck.toLowerCase() + "]");
                throw new RuntimeException ("Invalid statement");
            }
            
            santizedTableOrModelName = CustomTableMappingUtil.class2TableTrueCase(modelNameKey);
        }
        
        Log.log.trace("createQuery santizedTableOrModelName : [" + santizedTableOrModelName + "]");
        
        if (StringUtils.isBlank(santizedTableOrModelName))
        {
            Log.log.error("Could not sanitize table or model name [" + nameToCheck + "] in the query [" + query + "]");
            throw new RuntimeException ("Invalid statement");
        }
        
        Log.log.trace("Query with sanitized model or table name: [" + queryB4ModelName + " " + santizedTableOrModelName + " " + queryAfterModelName + "]");
        
        String safeSQLOrHQL;
        
        try {
        	safeSQLOrHQL = SQLUtils.getSafeSQL(ESAPI.validator().getValidInput(
        													"Query before model/table name", queryB4ModelName, 
        													ResolveDefaultValidator.HQL_SQL_SAFE_STRING_VALIDATOR, 
        													(ResolveDefaultValidator.MAX_HQL_SQL_SAFE_STRING_LENGTH / 3), 
        													false) + " " +
        								       ESAPI.validator().getValidInput(
        								    		   					"Sanitized model/table name",
        										  						santizedTableOrModelName, 
        										  						ResolveDefaultValidator.HQL_SQL_ENTITY_VALIDATOR,
        										  						ResolveDefaultValidator.MAX_HQL_SQL_ENTITY_LENGTH,
        										  						false) + " " +
        									   (StringUtils.isNotBlank(queryAfterModelName) ?
        								        ESAPI.validator().getValidInput(
        								        					"Query after model/table name", queryAfterModelName, 
        										  					ResolveDefaultValidator.HQL_SQL_SAFE_STRING_VALIDATOR, 
        										  					ResolveDefaultValidator.MAX_HQL_SQL_SAFE_STRING_LENGTH,
        										  					true) : ""));
        } catch (Exception e) {
        	Log.log.warn("Error " + e.getLocalizedMessage() + " occurred in geting safe SQL/HQL [" +
        				 queryB4ModelName + " " + santizedTableOrModelName + " " + queryAfterModelName + "].", e);
        	throw e;
        }
        
        return safeSQLOrHQL;
    }
    
    @SuppressWarnings("rawtypes")
    public static Query createQuery(String modelName, String paramName, String paramValue)
    {
        String sql = "from " + modelName + " where " + paramName + " = :" + paramName;
        return getCurrentSession().createQuery(sql).
               setParameter(paramName, paramValue);
    }
    
    @SuppressWarnings("rawtypes")
    public static Query createQuery(String modelName, String modelAlias, String inParamName, Collection<String> inParamValues)
    {
        String sql = "from " + modelName + " " + modelAlias + " where " + modelAlias + "." + inParamName + " IN (:" + inParamName + ")";
        return getCurrentSession().createQuery(sql).
               setParameterList(inParamName, inParamValues);
    }
    
    @SuppressWarnings("rawtypes")
    public static Query createSQLQuery(String query) throws Exception
    {
        return getCurrentSession().createNativeQuery(SQLUtils.getSafeSQL(query));
    }
    
    @SuppressWarnings("rawtypes")
    public static Query createCountQuery(QueryDTO query) throws Exception
    {
        Query hqlQuery = HibernateUtil.createQuery(query.getSelectCountHQL());
        hqlQuery.setProperties(query.getParams());
        return hqlQuery;
    }

    @SuppressWarnings("rawtypes")
    public static Query createHQLQuery(String query) throws Exception
    {
        return getCurrentSession().createQuery(getValidQuery(query));

    }
    
    /**
     * Returns the Hibernate configuration that was used to build the
     * SessionFactory.
     *
     * @return Configuration
     */
    public static Configuration getConfiguration()
    {
        return configuration;
    }

    /**
     * Returns the global SessionFactory either from a static variable or a JNDI
     * lookup.
     *
     * @return SessionFactory
     */
    public static SessionFactory getSessionFactory()
    {
        Lock lock = initLock.readLock();
        lock.lock();
        try
        {
            if (sessionFactory == null)
            {
                init(doSchemaUpdate, false, false);
            }
        }
        finally
        {
            lock.unlock();
        }

        return sessionFactory;
    }

    /**
     * Closes the current SessionFactory and releases all resources.
     * <p>
     * The only other method that can be called on HibernateUtil after this one
     * is rebuildSessionFactory(Configuration).
     */
    public static void shutdown()
    {
        Log.log.debug("Shutting down Hibernate");
        // Close caches and connection pools
        getSessionFactory().close();

        // Clear static variables
        sessionFactory = null;
    }

    /**
     * Rebuild the SessionFactory with the static Configuration.
     * <p>
     * Note that this method should only be used with static SessionFactory
     * management, not with JNDI or any other external registry. This method
     * also closes the old static variable SessionFactory before, if it is still
     * open.
     */
//    public static void rebuildSessionFactory()
//    {
//        Log.log.debug("Using current Configuration to rebuild SessionFactory");
//        rebuildSessionFactory(configuration);
//    }

    /**
     * Rebuild the SessionFactory with the given Hibernate Configuration.
     * <p>
     * HibernateUtil does not configure() the given Configuration object, it
     * directly calls buildSessionFactory(). This method also closes the old
     * static variable SessionFactory before, if it is still open.
     *
     * @param cfg
     */
    private static synchronized void rebuildSessionFactory(Configuration cfg) throws Exception
    {
        Log.log.debug("Rebuilding the SessionFactory from given Resolve Configuration");
        
        closeSessionFactory();

        int retry = 3;
        boolean success = false;
        Throwable throwable = null;

        while (!success && retry > 0)
        {
            try
            {
                Map<Thread, Thread> beforeHooks = JavaUtils.getShutdownHooks();
                Log.log.debug("ShutdownHooks before rebuildSessionFactory(): " + beforeHooks);

                sessionFactory = metadata.getSessionFactoryBuilder().applyInterceptor(new ResolveInterceptor()).build();
                
                Map<Thread, Thread> afterHooks = JavaUtils.getShutdownHooks();
                Log.log.debug("ShutdownHooks after rebuildSessionFactory(): " + afterHooks);

                // remove new shutdown hooks
                List<Thread> removeHooks = new ArrayList<Thread>();
                for (Thread hook : afterHooks.values())
                {
                    if (!beforeHooks.containsKey(hook))
                    {
                        removeHooks.add(hook);
                    }
                }
                for (Thread hook : removeHooks)
                {
                    JavaUtils.removeShutdownHook(hook);
                }
                Log.log.debug("ShutdownHooks cleaned up rebuildSessionFactory(): " + afterHooks);

                configuration = cfg;
                success = true;
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                throwable = t;
                try
                {
                    Thread.sleep(10000);
                }
                catch (Throwable t2)
                {
                    // do nothing
                }
            }

            retry--;
        }

        if (!success)
        {
            Log.alert(ERR.E20007);
            throw new Exception(throwable); 
        }
        
        
    } // rebuildSessionFactory

    public static void logTransactionCount()
    {
        Log.log.debug("tranaction count: " + getTransactionCount() + ", nested transaction count: " + getNestedTransactionCount());
    } // logTransactionCount

    public static int getTransactionCount()
    {
        return transactionCount.get();
    } // getTransactionCount

    public static int getNestedTransactionCount()
    {
        return nestedTransactionCount.get();
    } // getNestedTransactionCount


    /*
     * rethrow the nested transaction exceptions so that the session does not get reused
     */
    public static void rethrowNestedTransaction(Throwable ex)
    {
        if (nestedTransactionCount.get() > 0)
        {
            if (Log.log.isTraceEnabled()) {
	            Log.log.trace("rethrowNestedTransaction: tranaction count: " + getTransactionCount() + ", nested transaction count: " + getNestedTransactionCount() + ", rollbackOnly=" + rollbackOnly.get()); 
            }
            throw new RuntimeException("Rethrow nested transaction exception. nested transaction count: " + getNestedTransactionCount() + " transactionCount: " + transactionCount, ex);
        }
    }


    public static SessionImplementor getCurrentSession()
    {

    		return (SessionImplementor) getSessionFactory().getCurrentSession();  	
        
    }

    public static Connection getConnection() throws Exception
    {
        return getSessionFactory().getSessionFactoryOptions().getServiceRegistry().getService(ConnectionProvider.class).getConnection();
    }

    public static DAOFactory getDAOFactory()
    {
        Lock lock = initLock.readLock();
        lock.lock();

        try
        {
            if (daoFactory == null)
            {
                init(doSchemaUpdate, false, false);
            }
        }
        finally
        {
            lock.unlock();
        }

        return daoFactory; // DAOFactory.instance();
    }
    
    public static Session getSession()
    {    	
    	return (SessionImplementor) getSessionFactory().getCurrentSession();	 
    }
    
    public static Transaction beginTransaction(Session session) {

    	
    	
    	Transaction transaction;
		if (nestedTransactionCount.get() == 0 ) {
    		transaction = session.beginTransaction();
    	} else {
    		transaction = session.getTransaction();
    	}
		
		nestedTransactionCount.set(getNestedTransactionCount() + 1);
		
        return transaction;
    	
    }
    
    public static void commitTransaction(Transaction tx) {
    	
    	
    	if (nestedTransactionCount.get() == 1) {
    		tx.commit();
    		getCurrentSession().close();
    	}
    	
    	nestedTransactionCount.set(getNestedTransactionCount() - 1);
    	
    	
    }
    
    public static void rollbackTransaction(Transaction tx) {
    	
    	 if (nestedTransactionCount.get() <= 0)
         {
             // transaction is not started yet, so we don't care about
             // rollback.
             // We're going to reset transaction status.
             nestedTransactionCount.set(0);
             
             return;
         }
    	 
    	 try
         {
    		 tx.rollback();
         }
         catch (Exception ex)
         {
             throw new RuntimeException("Failed to rollback current transaction. nested transaction count: " + getNestedTransactionCount() + " transactionCount: " + transactionCount, ex);
         } finally {
        	 
        	 nestedTransactionCount.set(0);
		}    	
    }

    private static ConcurrentHashMap<String, String> seqNameIdMap = new ConcurrentHashMap<String, String>();

    public static String getNextSequenceString(String sequenceName)
    {
        return sequenceName + getNextSequence(sequenceName);
    } // getNextSequenceString

    public static Integer getNextSequence(String sequenceName)
    {
        return getNextSequence(sequenceName, 0);
    }
    
    // Auto increment the sequence named "sequenceName" and then return its
    // value. New sequence is created if it doesn't exist yet with the given seed value.
    @SuppressWarnings("unchecked")
    public static Integer getNextSequence(String sequenceName, int seedSequence)
    {
        Integer result = -1;

        try
        {
          HibernateProxy.setCurrentUser("system");
        	result = (Integer) HibernateProxy.execute(() -> {
                String id = seqNameIdMap.get(sequenceName);
                if (id != null)
                {
                    ResolveSequenceGenerator rsg = (ResolveSequenceGenerator) getCurrentSession().get(ResolveSequenceGenerator.class, id, LockMode.PESSIMISTIC_WRITE);
                    rsg.setUNumber(rsg.getUNumber() + 1);
                    return rsg.getUNumber();

                }
                else
                {

                    ResolveSequenceGenerator exp = new ResolveSequenceGenerator();
                    List<ResolveSequenceGenerator> lrsg = getCurrentSession().createQuery("from ResolveSequenceGenerator r where r.UName = :name").
                                    setParameter("name", sequenceName).setLockMode("r", LockMode.PESSIMISTIC_WRITE).list();
                    
                    if (lrsg.size() > 0)
                    {
                        exp = lrsg.get(0);
                        exp = (ResolveSequenceGenerator) getCurrentSession().get(ResolveSequenceGenerator.class, exp.getsys_id(), LockMode.PESSIMISTIC_WRITE);
                        seqNameIdMap.put(exp.getUName(), exp.getsys_id());
                        exp.setUNumber(exp.getUNumber() + 1);
                        return  exp.getUNumber();
                    }
                    else
                    {
                        ResolveSequenceGenerator newSeq = new ResolveSequenceGenerator();
                        newSeq.setUName(sequenceName);
                        newSeq.setUNumber(seedSequence);
                        getCurrentSession().save(newSeq);
                        return newSeq.getUNumber();
                    }
                }
        	});

        }
        catch (Throwable t)
        {
          HibernateUtil.rethrowNestedTransaction(t);
        	
        }
        return result;
    } // getNextSequence

    public static String getSequenceString(String sequenceName)
    {
        String result = null;

        Integer value = getSequence(sequenceName);
        if (value != null)
        {
            result = sequenceName + value;
        }

        return result;
    } // getSequenceString

    // Query for the current assigned value for sequence named "sequenceName".
    // Null is returned if sequence doesn't exist.
    public static Integer getSequence(String sequenceName)
    {
        Integer result = null;
        try
        {
          HibernateProxy.setCurrentUser("system");
        	result = (Integer) HibernateProxy.execute(() -> {
                ResolveSequenceGeneratorDAO rsgDao = HibernateUtil.getDAOFactory().getResolveSequenceGeneratorDAO();

                String id = seqNameIdMap.get(sequenceName);
                if (id != null)
                {
                    ResolveSequenceGenerator rsg = HibernateUtil.getDAOFactory().getResolveSequenceGeneratorDAO().findById(id);
                    return rsg.getUNumber();
                }
                else
                {
                    ResolveSequenceGenerator exp = new ResolveSequenceGenerator();
                    exp.setUName(sequenceName);
                    List<ResolveSequenceGenerator> lrsg = rsgDao.find(exp);
                    if (lrsg.size() > 0)
                    {
                        exp = lrsg.get(0);
                        seqNameIdMap.put(exp.getUName(), exp.getsys_id());
                        return exp.getUNumber();
                    }
                }
                
                return null;
        	});
        }
        catch (Throwable t)
        {
          HibernateUtil.rethrowNestedTransaction(t);
        }
        return result;
    }

    public static void evict(String className)
    {
        try
        {
            Class<?> entityClass = Class.forName(className);
            if (entityClass != null)
            {
                getSessionFactory().getCache().evict(entityClass);
            }
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    } // evict

    public static void evict(Class<?> entityClass)
    {
        getSessionFactory().getCache().evict(entityClass);
    } // evict

    public static void evict(String className, String sys_id)
    {
        try
        {
            Class<?> entityClass = Class.forName(className);
            if (entityClass != null)
            {
                getSessionFactory().getCache().evict(entityClass, sys_id);
            }
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    } // evict

    public static void evict(Class<?> entityClass, String sys_id)
    {
        getSessionFactory().getCache().evict(entityClass, sys_id);
    } // evict

    public static String class2Table(String className)
    {
        String tableName = null;
        
        if (StringUtils.isBlank(StringUtils.strip(className)))
        {
            return tableName;
        }
        
        String classNameKey = StringUtils.strip(className).startsWith("com.resolve.persistence.model.") ?
                              StringUtils.strip(className) :
                              "com.resolve.persistence.model." + StringUtils.strip(className);
        
        if (!class2TableMap.containsKey(classNameKey))
        {
            // Custom table class names do not have com.resolve.persistence.model prefix
            classNameKey = StringUtils.strip(className);
        }
        
        return class2TableMap.get(classNameKey);
    } // class2Table
        
    public static String class2TableTrueCase(String className)
    {
        String tableNameTC = null;
        
        if (StringUtils.isBlank(StringUtils.strip(className)))
        {
            return tableNameTC;
        }
        
        String tableNameLC = class2Table(className);
        
        if (StringUtils.isBlank(tableNameLC) || !(tableNameLC2TCMap.containsKey(tableNameLC)))
        {
            return tableNameTC;
        }
        
        return tableNameLC2TCMap.get(tableNameLC);
    } // class2TableTC
    
    public static String class2Table(Class<?> entityClass)
    {
        return class2Table(entityClass.getName());
    } // class2Table
    
    public static String class2TableTrueCase(Class<?> entityClass)
    {
        return class2TableTrueCase(entityClass.getName());
    } // class2Table
    
    public static String table2Class(String tableName)
    {
        String className = null;
        
        if (StringUtils.isBlank(StringUtils.strip(tableName)))
        {
            return className;
        }
        
        return table2ClassMap.get(StringUtils.strip(tableName).toLowerCase());
    } // table2Class
    
    public static boolean modelExists(Class<?> modelClass)
    {
        boolean modelExists = false;
        
        if (modelClass == null)
        {
            return modelExists;
        }
        
        return modelExists(modelClass.getName());
    }
    
    public static boolean modelExists(String modelClassName)
    {
        boolean modelExists = false;
        
        if (StringUtils.isBlank(StringUtils.strip(modelClassName)))
        {
            return modelExists;
        }
        
        String modelClassNameKey  = StringUtils.strip(modelClassName).startsWith("com.resolve.persistence.model.") ? 
                                    StringUtils.strip(modelClassName) : 
                                    "com.resolve.persistence.model." + StringUtils.strip(modelClassName);
        
        if (!class2TableMap.containsKey(modelClassNameKey))
        {
            // Custom table class names do not have com.resolve.persistence.model prefix
            modelClassNameKey = StringUtils.strip(modelClassName);
        }
        
        return class2TableMap.containsKey(modelClassNameKey);
    }
    
    public static boolean tableExists(String tableName)
    {
        boolean tableExists = false;
        
        if (StringUtils.isBlank(StringUtils.strip(tableName)))
        {
            return tableExists;
        }
        
        return table2ClassMap.containsKey(StringUtils.strip(tableName).toLowerCase()) &&
               ((StringUtils.isNotBlank(table2Class(tableName)) &&
                 table2Class(tableName).startsWith("com.resolve.persistence.model")) ||
                StringUtils.strip(tableName).toLowerCase().startsWith("resolve_impex_"));
    }
    
    @SuppressWarnings("unchecked")
    public static void createPropertyColumnMapping(String clsName)
    {
        PersistentClass ps = getClassMapping(clsName);
        Map<String, String> p2c = new ConcurrentHashMap<String, String>();
        Map<String, String> c2p = new ConcurrentHashMap<String, String>();
        Iterator<Property> it = ps.getPropertyIterator();
        
        while (it.hasNext())
        {
            org.hibernate.mapping.Property p = it.next();

            String propName = p.getName();
            Iterator<Column> cit = p.getColumnIterator();
            
            while (cit.hasNext())
            {
                Column c = cit.next();
                String colName = c.getName();
                p2c.put(propName, colName);
                c2p.put(colName, propName);
                break;
            }
        }
        p2c.put("sys_id", "sys_id");
        c2p.put("sys_id", "sys_id");
        property2Column.put(clsName, p2c);
        column2Property.put(clsName, c2p);
    }

    public static String getColumnName(String className, String propertyName)
    {
        if (property2Column.get(className) == null)
        {
            createPropertyColumnMapping(className);
        }
        return property2Column.get(className).get(propertyName);
    }

    public static String getPropertyName(String className, String columnName)
    {
        if (column2Property.get(className) == null)
        {
            createPropertyColumnMapping(className);
        }
        return column2Property.get(className).get(columnName);
    }

    public static Collection<String> getAllPropertiesByTable(String table)
    {
        String clsName = table2Class(table);
        if (clsName != null)
            return getAllProperties(clsName);
        else
            return null;
    }

    public static Collection<String> getAllColumnsByTable(String table)
    {
        String clsName = table2Class(table);
        if (clsName != null)
            return getAllColumns(clsName);
        else
            return null;
    }

    public static Collection<String> getAllProperties(String className)
    {
        if (property2Column.get(className) == null)
        {
            createPropertyColumnMapping(className);
        }
        return property2Column.get(className).keySet();
    }

    public static Collection<String> getAllColumns(String className)
    {
        if (property2Column.get(className) == null)
        {
            createPropertyColumnMapping(className);
        }
        return column2Property.get(className).keySet();
    }

    public static void delete(String modelName, String sys_id) throws Exception
    {
        List<String> ids = new ArrayList<String>();
        ids.add(sys_id);
        delete(modelName, ids);
    }

    @SuppressWarnings("rawtypes")
    public static void delete(String modelName, List<String> sys_ids) throws Exception
    {
        /*
         * Instead of reading entire object in memory and then deleting it,
         * we might just perform a sql delete operation on modelName and sysId(s).
         * Following implementation splits the sys_ids in the batch of 200.
         */
        if (sys_ids != null)
        {
            List<List<String>> batchSysIds = JavaUtils.chopped(sys_ids, 200);

            for(List<String> batchSysId : batchSysIds)
            {
                String sql;
				try {
					sql = "delete from " + ESAPI.validator().getValidInput("Delete model/table name", modelName, 
																		   "HQLSQLEntity", 512, false) + 
						  " where sys_id in (:sqlSysId)";
				} catch (ValidationException | IntrusionException e) {
					Log.log.warn("Error " + e.getLocalizedMessage() + 
								 " occurred in validating specified model/table name " + modelName);
					throw e;
				}
                Query query = HibernateUtil.createQuery(sql);
                query.setParameterList("sqlSysId", batchSysId);
                query.executeUpdate();
            }
        }
    }

    public static void deleteObj(String modelName, Object obj)
    {
        List<Object> list = new ArrayList<Object>();
        list.add(obj);
        
        String tableName = HibernateUtil.class2Table(!modelName.startsWith("com.resolve.persistence.model.") ? 
                                                     "com.resolve.persistence.model." + modelName : modelName);
        
        if (StringUtils.isNotEmpty(tableName))
        {
            deleteObjs(HibernateUtil.table2Class(tableName), list);
        }
    }

    public static void deleteObjs(String modelName, List<Object> objs)
    {
        modelName = "com.resolve.persistence.model." + modelName;
        Session session = HibernateUtil.getCurrentSession();
        for (Object dbObj : objs)
        {
            session.delete(modelName, dbObj);
        }
    }
    
    @SuppressWarnings("unchecked")
    public static void deleteQuery(String modelName, String paramName, String paramValue)
    {
        if (StringUtils.isNotEmpty(paramName) && StringUtils.isNotEmpty(paramValue))
        {
            String query = " from " + modelName + " tblData where tblData." + 
                           (paramName.equalsIgnoreCase("sys_id") || paramName.startsWith("U") ? paramName : paramName + ".sys_id") + " = :" + paramName;
            
            List<Object> objs = HibernateUtil.getCurrentSession().createQuery(query).
                                setParameter(paramName, paramValue).list();
            
            deleteObjs(modelName, objs);
        }
    }

    @SuppressWarnings("unchecked")
    public static void deleteAllQuery(String modelName, String paramName, String paramValue)
    {
        String query = " from " + modelName + " tblData where tblData." + 
                        (paramName.equalsIgnoreCase("sys_id") || paramName.startsWith("U") ? paramName : paramName + ".sys_id") + " != :" + paramName;
        
        List<Object> objs = HibernateUtil.getCurrentSession().createQuery(query).
                            setParameter(paramName, paramValue).list();
        
        deleteObjs(modelName, objs);
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static void deleteQuery(String modelName, String inParamName, Collection<String> inParamValues)
    {
        String query = " from " + modelName + (StringUtils.isNotEmpty(inParamName) ? " tblData where tblData." + 
                       (inParamName.equalsIgnoreCase("sys_id") || inParamName.startsWith("U") ? inParamName : inParamName + ".sys_id") + 
                       " IN (:" + inParamName + ")" : "");
        
        Query hqlQuery = HibernateUtil.getCurrentSession().createQuery(query);
        
        if (StringUtils.isNotEmpty(inParamName))
        {
            hqlQuery.setParameterList(inParamName, inParamValues);
        }
        
        List<Object> objs = hqlQuery.list();
        
        deleteObjs(modelName, objs);
    }
    
    @SuppressWarnings("unchecked")
    public static void deleteQuery(String modelName, String inParamName, Collection<String> inParamValues, String andParamName, String andParamValue)
    {
        String query = " from " + modelName + " tblData where tblData." + 
                       (inParamName.equalsIgnoreCase("sys_id") || inParamName.startsWith("U") ? inParamName : inParamName + ".sys_id") + 
                       " IN (:" + inParamName + ") and tblData." + 
                       (andParamName.equalsIgnoreCase("sys_id") || andParamName.startsWith("U") ? andParamName : andParamName + ".sys_id") + 
                       " = :" + andParamName;
        List<Object> objs = HibernateUtil.getCurrentSession().createQuery(query).
                            setParameterList(inParamName, inParamValues).
                            setParameter(andParamName, andParamValue).list();
        deleteObjs(modelName, objs);
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static void deleteQuery(String modelName, String paramName, String paramValue, String optWhereClause, String optInParamName, Collection<Integer> optInParamValues)
    {
        String query = " from " + modelName + " tblData where tblData." + 
                       (paramName.equalsIgnoreCase("sys_id") || paramName.startsWith("U") ? paramName : paramName + ".sys_id") + " = :" + paramName + 
                       (StringUtils.isNotEmpty(optWhereClause) ? "" + optWhereClause + " and tblData." + 
                        (optInParamName.equalsIgnoreCase("sys_id") || optInParamName.startsWith("U") ? optInParamName : optInParamName + ".sys_id") + 
                        " IN (:" + optInParamName + ")" : "");
        
        Query hqlQuery = HibernateUtil.getCurrentSession().createQuery(query);
        
        hqlQuery.setParameter(paramName, paramValue);
        
        if (StringUtils.isNotEmpty(optWhereClause))
        {
            hqlQuery.setParameterList(optInParamName, optInParamValues);
        }
        
        List<Object> objs = hqlQuery.list();
                        
        deleteObjs(modelName, objs);
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static void deleteQuery(String modelName, String paramName, String paramValue, String optInParamName, Collection<String> optInParamValues)
    {
        String query = " from " + modelName + " tblData where tblData." + 
                       (paramName.equalsIgnoreCase("sys_id") || paramName.startsWith("U") ? paramName : paramName + ".sys_id") + " = :" + paramName + 
                       (StringUtils.isNotEmpty(optInParamName) ? " and tblData." + 
                        (optInParamName.equalsIgnoreCase("sys_id") || optInParamName.startsWith("U") ? optInParamName : optInParamName + ".sys_id") + 
                        " IN (:" + optInParamName + ")" : "");
        
        Query hqlQuery = HibernateUtil.getCurrentSession().createQuery(query);
        
        hqlQuery.setParameter(paramName, paramValue, StringType.INSTANCE);
        
        if (StringUtils.isNotEmpty(optInParamName))
        {
            hqlQuery.setParameterList(optInParamName, optInParamValues);
        }
        
        List<Object> objs = hqlQuery.list();
                        
        deleteObjs(modelName, objs);
    }
    
    public static <T> Object saveObj(String modelName, BaseModel<T> obj)
    {
        Object result = null;
        
        modelName = "com.resolve.persistence.model." + modelName;
        Session session = HibernateUtil.getCurrentSession();
        if(StringUtils.isBlank(obj.getSys_id()))
        {
            session.save(modelName, obj);
        }
        else
        {
            session.update(obj);
        }
        result = obj;
        return result;
    }
    
    public static void saveObjs(String modelName, List<Object> objs)
    {
    	try {
    		
			HibernateProxy.execute(() -> {
				String modelNameFinal = modelName;
				modelNameFinal = "com.resolve.persistence.model." + modelNameFinal;
			        Session session = HibernateUtil.getCurrentSession();
			        for (Object dbObj : objs)
			        {
			            session.saveOrUpdate(modelNameFinal, dbObj);
			        }
			    });
		} catch (Exception e) {
                             HibernateUtil.rethrowNestedTransaction(e);
			Log.log.error(e.getMessage(), e);
		}
    }
      

    public static void updateObj(String modelName, Object obj)
    {
        modelName = "com.resolve.persistence.model." + modelName;
        Session session = HibernateUtil.getCurrentSession();
        session.update(obj);
    }

    public static Object findById(String modelName, String id)
    {
        Session session = HibernateUtil.getCurrentSession();
        return session.get(modelName, id);
    }

    public static class User
    {
        String name;

        public User(String name)
        {
            this.name = name;
        }

        @Override
        public String toString()
        {
            return this.name;
        }
    } // User

    @SuppressWarnings("unchecked")
	public static <V> V invokeInTransaction(Callable<V> call)
    {
        V result = null;

        try
        {
          HibernateProxy.setCurrentUser("system");
        	result = (V) HibernateProxy.execute(() -> {
        		return call.call();
        	});
        }
        catch (Throwable e)
        {
                    HibernateUtil.rethrowNestedTransaction(e);
        		Log.log.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<String> getAllTableNames()
    {
        List<String> tableNames = new ArrayList<String>();
        if (configuration == null)
        {
            return null;
        }

        Iterator<PersistentClass> cit = getClassMappings().iterator();
        RootClass rc = null;
        String table = null;
        while (cit.hasNext())
        {
            rc = (RootClass) cit.next();
            table = rc.getTable().getName();
            tableNames.add(table);
        }
        return tableNames;
    }

    public static String getResolveSchema()
    {
        return getMetadata().getDatabase().getDefaultNamespace().getName().getSchema().getText();
    }

    public static void deleteTableColumn(String table, String column)
    {
        deleteTableColumn(getResolveSchema(), table, column);
    }

    public static void deleteTableColumn(String schema, String table, String column)
    {
        String sql = "alter table " + schema + "." + table + " drop column " + column;
        Connection c = null;
        try
        {
            c = getConnection();
            c.createStatement().executeUpdate(sql);
            Log.log.info("Drop column " + column + " from table " + table + " in schema " + schema);
        }
        catch (Throwable ex)
        {
            Log.log.error("Failed to drop column " + column + " from table " + table, ex);
            throw new RuntimeException(ex);
        }
        finally
        {
            try
            {
                c.close();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
    
    /* Unsafe - SQL Injection weakness
    public static void deleteTable(String table)
    {
        deleteTable(getResolveSchema(), table);
    }
    
    public static void deleteTable(String schema, String table)
    {
        String sql = SQLUtils.getSafeSQL("drop table " + schema + "." + table, configSQL.getDbtype().toUpperCase());

        Connection c = null;
        try
        {
            c = getConnection();
            c.createStatement().executeUpdate(sql);
            Log.log.info("Dropped table " + table + " in schema " + schema);
        }
        catch (Throwable ex)
        {
            Log.log.error("Failed to drop table " + table, ex);
            throw new RuntimeException(ex);
        }
        finally
        {
            try
            {
                c.close();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
    */
    
    public static String getColumnTypeString(String table, String column)
    {
        String result = null;
        Type t = getColumnType(table, column);
        if (t != null)
        {
            result = t.getName();
        }
        return result;
    }

    public static Type getColumnType(String table, String column)
    {
        String cls = table2Class(table);
        if (cls == null)
        {
            throw new RuntimeException("Resolve can't find table " + table);
        }

        if (column2Property.get(cls) == null)
        {
            createPropertyColumnMapping(cls);
        }

        PersistentClass ps = getClassMapping(cls);
        String p = column2Property.get(cls).get(column);
        if (p == null)
        {
            throw new RuntimeException("Resolve can't find column " + column + " in table " + table);
        }
        return ps.getProperty(p).getType();
    }

    public static Property getColumnProperty(String table, String column)
    {
        String cls = table2Class(table);
        if (cls == null)
        {
            throw new RuntimeException("Resolve can't find table " + table);
        }

        if (column2Property.get(cls) == null)
        {
            createPropertyColumnMapping(cls);
        }

        PersistentClass ps = getClassMapping(cls);
        String p = column2Property.get(cls).get(column);
        if (p == null)
        {
            throw new RuntimeException("Resolve can't find column " + column + " in table " + table);
        }
        return ps.getProperty(p);
    }

    public static void businessRuleUpdated()
    {
        if (esb == null)
        {
            return;
        }

        Map<String, String> msg = new HashMap<String, String>();
        msg.put(Constants.SOURCE_GUID, guid);
        msg.put(Constants.BUSINESS_RULE_UPDATE, Boolean.TRUE.toString());

        esb.sendInternalMessage(Constants.ESB_NAME_RSVIEWS, "com.resolve.persistence.util.HibernateUtilHelper.updateBusinessRuleCache", msg);
        esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROLS, "com.resolve.persistence.util.HibernateUtilHelper.updateBusinessRuleCache", msg);
    }

    public static void registerDBPoolStatusProvider()
    {
        DBPoolUtils.registerProvider(dbProvider);
    }

    private static void initEntityMappingCache()
    {
        Iterator<PersistentClass> it = getClassMappings().iterator();
        PersistentClass pc = null;
        while (it.hasNext())
        {
            pc = it.next();
            if (pc.getClassName() != null)
            {
                modelClassNames.add(pc.getClassName());
            }
            else if (pc.getEntityName() != null)
            {
                entityNames.add(pc.getEntityName());
            }
        }
    }

    public static List<String> getAllMappingClassNames()
    {
        if (modelClassNames.isEmpty())
        {
            initEntityMappingCache();
        }
        return modelClassNames;
    }

    public static List<String> getAllMappingEntityNames()
    {
        if (entityNames.isEmpty())
        {
            initEntityMappingCache();
        }
        return entityNames;
    }

    public static String getStringFromClob(Object obj)
    {
        if (obj == null) return null;

        if (!(obj instanceof Clob))
        {
            throw new RuntimeException("Object is not a Clob: " + obj);
        }

        Clob c = (Clob) obj;
        String s = null;
        try
        {
            s = StringUtils.readAll(c.getCharacterStream());
            //            s=c.getSubString(1L, (int)c.length());//**This was throwing exception --> java.lang.RuntimeException: java.lang.UnsupportedOperationException: Blob may not be manipulated from creating session
        }
        catch (Exception ex)
        {
            throw new RuntimeException(ex);
        }
        return s;
    }

    public static Clob getClobFromString(String value)
    {
        Clob newText = null;
        
        try
        {
            
            newText = (Clob) HibernateProxy.execute(() -> {
            	return Hibernate.getLobCreator(getCurrentSession()).createClob(value);
            });
        }
        catch (Throwable t)
        {
                      HibernateUtil.rethrowNestedTransaction(t);
            Log.log.error(t.getMessage(), t);
        }
        
        return newText;
    }

    public static Blob getBlobFromBytes(byte[] bytes)
    {
        Blob blob = null;
    
        try
        {            
            blob = (Blob) HibernateProxy.execute( () -> {
            	return Hibernate.getLobCreator(getCurrentSession()).createBlob(bytes);
            });
        }
        catch (Throwable t)
        {
                      HibernateUtil.rethrowNestedTransaction(t);
            Log.log.error(t.getMessage(), t);
        }
        
        return blob;
    }

    public static byte[] getBytesFromBlob(Object obj)
    {
        if (obj == null) return null;

        if (!(obj instanceof Blob))
        {
            throw new RuntimeException("Object is not a Blob: " + obj);
        }

        Blob b = (Blob) obj;
        byte[] bytes = null;

        try
        {
            bytes = StringUtils.readlInputStream(b.getBinaryStream());
        }
        catch (Exception ex)
        {
            throw new RuntimeException(ex);
        }

        return bytes;
    }

    public static String getStringFromBlob(Object blob)
    {
        byte[] bytes = getBytesFromBlob(blob);
        String s = StringUtils.byteToString(bytes);
        return s;
    }

    public static void setEntityClobProperty(Map<String, Object> map, String key, String value)
    {
        try
        {
            HibernateProxy.execute(() -> {
            	Clob newText = Hibernate.getLobCreator(getCurrentSession()).createClob(value);
                map.put(key, newText);
            });
            
        }
        catch (Throwable t)
        {
                      HibernateUtil.rethrowNestedTransaction(t);
            Log.log.error(t.getMessage(), t);
        }
    }

    public static boolean isNullableTypeColumn(String tableName, String column)
    {
        Property property = getColumnProperty(tableName, column);
        return property.getValue() == null || property.getValue().isNullable();
    }

    //Disable all sys columns updates from current thread
    public static void disableSysColumnsUpdate()
    {
        ExportUtil.isFromImport.set(Boolean.TRUE);
    }

    //Enable or re-enable all sys columns updates from current thread, which is default
    public static void enableSysColumnsUpdate()
    {
        ExportUtil.isFromImport.set(Boolean.FALSE);
    }

    public static Object getCachedDBObject(DBCacheRegionConstants region, String accessId)
    {
        ConcurrentHashMap<?, ?> regionMap = DBObjectsCache.get(region);
        if (regionMap != null)
        {
            return regionMap.get(accessId);
        }
        else
        {
            return null;
        }
    }
    
    public static Object clearCachedDBObject(DBCacheRegionConstants region, String accessId)
    {
        Object cachedObject = null;
        
        ConcurrentHashMap<String, Object> regionMap = DBObjectsCache.get(region);
        
        if (regionMap != null)
        {
            cachedObject = regionMap.get(accessId);
            regionMap.remove(accessId);
        }
        
        return cachedObject;
    }
    
    public static void clearDBCacheRegion(DBCacheRegionConstants region, boolean sendMsg)
    {
        clearDBCacheRegion(region, sendMsg, null);
    }
    
    /**
     *  Invalidates given cache region. 
     *  In case we sendMsg is true -> we invalidate the cache to all rscontrol and rsview instances.
     *  In case we want to invalidate only certain object we should pass its id.
     *  
     * @param region
     * @param sendMsg
     * @param key in case we don't pass the key -> we invalidate the whole region
     */
    public static void clearDBCacheRegion(DBCacheRegionConstants region, boolean sendMsg, String keyToInvalidate) {
        
        if(StringUtils.isEmpty(keyToInvalidate)) {
            ConcurrentHashMap<String, Object> map = DBObjectsCache.remove(region);
            if (map == null) {
                ConcurrentHashMap<String, Object> newMap = new ConcurrentHashMap<String, Object>();
                DBObjectsCache.put(region, newMap);
            }
        } else {
            ConcurrentHashMap<String, Object> map = DBObjectsCache.get(region);
            if(map != null) {
                map.remove(keyToInvalidate);
            }
        }

        
        if (sendMsg)
        {
            Map<String, Object> msg = new HashMap<String, Object>();
            msg.put(Constants.SOURCE_GUID, guid);
            msg.put(DB_CACHE_REGION, region.toString());
            msg.put(DB_CACHE_KEY, keyToInvalidate);
            if (esb != null)
            {
                esb.sendInternalMessage(Constants.ESB_NAME_RSVIEWS, "com.resolve.persistence.util.HibernateUtilHelper.clearDBObjectsCache", msg);
                esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROLS, "com.resolve.persistence.util.HibernateUtilHelper.clearDBObjectsCache", msg);
            }
        }
    }

    public enum DBCacheListenerType
    {
        INSERT, DELETE, UPDATE;
    }

    @SuppressWarnings("unused")
    private static class DBCacheListener
    {
        DBCacheListenerType type;
        Object listener;
        String entity;

        DBCacheListener(DBCacheListenerType t, Object l, String e)
        {
            type = t;
            listener = l;
            entity = e;
        }
    }

    private static void registerDBCacheListeners(final DBCacheRegionConstants region, String[] entities)
    {
        ConcurrentHashMap<String, Object> map = DBObjectsCache.get(region);
        List<DBCacheListener> list = new ArrayList<DBCacheListener>();
        BlockingSIDListener sidListener = new BlockingSIDListener()
        {
            @Override
            public boolean onDataOperation(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) throws CallbackException
            {
                clearDBCacheRegion(region, true);
                return false;
            }

        };
        BlockingUListener uListener = new BlockingUListener()
        {
            @Override
            public boolean onUpdate(Object entity, Serializable id, Object[] state, Object[] previousState, String[] propertyNames, Type[] types) throws CallbackException
            {
                clearDBCacheRegion(region, true);
                return false;
            }

        };

        for (String entity : entities)
        {
            ResolveInterceptor.addInsertListener(entity, sidListener);
            list.add(new DBCacheListener(DBCacheListenerType.INSERT, sidListener, entity));

            ResolveInterceptor.addDeleteListener(entity, sidListener);
            list.add(new DBCacheListener(DBCacheListenerType.DELETE, sidListener, entity));

            ResolveInterceptor.addUpdateListener(entity, uListener);
            list.add(new DBCacheListener(DBCacheListenerType.UPDATE, uListener, entity));
        }
        map.put(DB_CACHE_LISTENER_LIST, list);
    }

    public static synchronized void cacheDBObject(DBCacheRegionConstants region, String[] entities, String acessId, Object value)
    {
        ConcurrentHashMap<String, Object> map = DBObjectsCache.get(region);
        if (map == null)
        {
            DBObjectsCache.putIfAbsent(region, new ConcurrentHashMap<String, Object>());
            registerDBCacheListeners(region, entities);
        }
        DBObjectsCache.get(region).putIfAbsent(acessId, value);
    }

    public static synchronized void cleanupCache()
    {

        DBObjectsCache.clear();
        try
        {
            SessionFactoryImpl sf = (SessionFactoryImpl) HibernateUtil.getSessionFactory();
            Iterator<?> cmIter = getClassMappings().iterator();
            while (cmIter.hasNext())
            {
                org.hibernate.mapping.Bag obj = (org.hibernate.mapping.Bag) cmIter.next();
                String role = null;
                if (obj != null)
                {
                    role = obj.getRole();
                    sf.getCache().evictCollectionRegion(role);
                }
            }

            Iterator<?> cit = getClassMappings().iterator();
            String entity = null;
            RootClass rc = null;
            while (cit.hasNext())
            {
                rc = (RootClass) cit.next();
                entity = rc.getEntityName();
                if (entity != null)
                {
                    sf.getCache().evictEntityRegion(entity);
                }
            }
        }
        catch (Throwable ex)
        {
            Log.log.error("Failed to initialize Resolve clustered cache.", ex);
            throw new RuntimeException(ex);
        }
    }

    private static String[] hibernateCacheUpdateTargets = { 
    				"com.resolve.persistence.model.AccessRights", 
    				"com.resolve.persistence.model.CustomTable", 
    				"com.resolve.persistence.model.Groups", 
    				"com.resolve.persistence.model.MetaAccessRights", 
    				"com.resolve.persistence.model.MetaControl",
                    "com.resolve.persistence.model.MetaControlItem",
                    "com.resolve.persistence.model.MetaField",
                    "com.resolve.persistence.model.MetaFieldProperties",
                    "com.resolve.persistence.model.MetaFilter",
                    "com.resolve.persistence.model.MetaFormAction", 
                    "com.resolve.persistence.model.MetaFormTab", 
                    "com.resolve.persistence.model.MetaFormTabField", 
                    "com.resolve.persistence.model.MetaFormView", 
                    "com.resolve.persistence.model.MetaFormViewProperties", 
                    "com.resolve.persistence.model.MetaSource", 
                    "com.resolve.persistence.model.MetaStyle", 
                    "com.resolve.persistence.model.MetaTable", 
                    "com.resolve.persistence.model.MetaTableView", 
                    "com.resolve.persistence.model.MetaTableViewField",
                    "com.resolve.persistence.model.MetaViewField", 
                    "com.resolve.persistence.model.MetaViewLookup", 
                    "com.resolve.persistence.model.MetaxFieldDependency", 
                    "com.resolve.persistence.model.MetaxFormViewPanel", 
                    "com.resolve.persistence.model.MetricThreshold", 
                    "com.resolve.persistence.model.Properties", 
                    "com.resolve.persistence.model.ResolvePreprocess",
                    "com.resolve.persistence.model.ResolveActionInvoc:com.resolve.persistence.model.ResolvePreprocess.resolveActionInvocs",  
                    "com.resolve.persistence.model.ResolveActionTask:com.resolve.persistence.model.ResolvePreprocess.resolveActionTasks", 
                    "com.resolve.persistence.model.ResolveAssess",
                    "com.resolve.persistence.model.ResolveBusinessRule",
                    "com.resolve.persistence.model.ResolveEventHandler", 
                    "com.resolve.persistence.model.ResolveParser", 
                    "com.resolve.persistence.model.ResolveParserTemplate", 
                    "com.resolve.persistence.model.ResolveProperties",
                    "com.resolve.persistence.model.ResolveBlueprint",
                    "com.resolve.persistence.model.Users",
                    "com.resolve.persistence.model.ResolveTag", 
                    "com.resolve.persistence.model.ResolveTriggerAction", 
                    "com.resolve.persistence.model.ResolveWikiLookup", 
                    "com.resolve.persistence.model.Roles", 
                    "com.resolve.persistence.model.SocialPostAttachment", 
                    "com.resolve.persistence.model.SysAppApplication", 
                    "com.resolve.persistence.model.SysAppApplicationroles", 
                    "com.resolve.persistence.model.SysAppModule",
                    "com.resolve.persistence.model.SysAppModuleroles", 
                    "com.resolve.persistence.model.SysPerspective", 
                    "com.resolve.persistence.model.SysPerspectiveapplications", 
                    "com.resolve.persistence.model.SysPerspectiveroles", 
                    "com.resolve.persistence.model.WikiAttachment", 
                    "com.resolve.persistence.model.WikidocAttachmentRel",
                    "com.resolve.persistence.model.WikidocQualityRating", 
                    "com.resolve.persistence.model.WikidocResolutionRating", 
                    "com.resolve.persistence.model.WikidocResolveTagRel", 
                    "com.resolve.persistence.model.WikiDocument", 
                    "com.resolve.persistence.model.WikiDocumentMetaFormRel",
                    "com.resolve.persistence.model.ResolveActionParameter:com.resolve.persistence.model.ResolveActionInvoc.resolveActionParameters",
                    "com.resolve.persistence.model.ResolveActionInvocOptions:com.resolve.persistence.model.ResolveActionInvoc.resolveActionInvocOptions",
                    "com.resolve.persistence.model.ResolveActionTaskMockData:com.resolve.persistence.model.ResolveActionInvoc.mockDataCollection",
                    "com.resolve.persistence.model.ResolveParserTemplate:com.resolve.persistence.model.ResolveParser.resolveParserTemplates",
                    "com.resolve.persistence.model.ResolveActionInvoc:com.resolve.persistence.model.ResolveParser.resolveActionInvocs",
                    "com.resolve.persistence.model.ResolveParserRel:com.resolve.persistence.model.ResolveParser.resolveParserRels",
                    "com.resolve.persistence.model.ResolveAssessRel:com.resolve.persistence.model.ResolveAssess.resolveAssessRels",
                    "com.resolve.persistence.model.ResolveActionInvoc:com.resolve.persistence.model.ResolveAssess:com.resolve.persistence.model.ResolveAssess.resolveActionInvocs",
                    "com.resolve.persistence.model.UserRoleRel:com.resolve.persistence.model.Users.userRoleRels:com.resolve.persistence.model.Roles.userRoleRels",
                    "com.resolve.persistence.model.UserGroupRel:com.resolve.persistence.model.Users.userGroupRels:com.resolve.persistence.model.Groups.userGroupRels",
                    "com.resolve.persistence.model.UserPreferences:com.resolve.persistence.model.Users.socialPreferences",
                    "com.resolve.persistence.model.GroupRoleRel:com.resolve.persistence.model.Roles.groupRoleRels:com.resolve.persistence.model.Groups.groupRoleRels",
                    "com.resolve.persistence.model.ResolvePreprocessRel:com.resolve.persistence.model.ResolvePreprocess.resolvePreprocessRel",

    };

    static public void evictHibernateRegion(String region)
    {
    	try 
    	{
			HibernateUtil.sessionFactory.getCache().evictEntityRegion(region);
    	}
    	catch (org.hibernate.MappingException ex)
    	{
    		HibernateUtil.sessionFactory.getCache().evictCollectionRegion(region);
    	}
    	catch (java.lang.IllegalStateException iex)
    	{
    		//Log.log.info(iex);  //ignore
    	}
    	catch (Throwable ta)
    	{
    		//ignore
    	}
    }
    
    static void updateHibernateCacheRegion(String entity)
    {
    	int idx = entity.indexOf(":");
    	if (idx>0)
    	{
    		int start = 0;
    		int lastIdx = 0;
    		String ent;
    		while(idx>0)
    		{
    			ent = entity.substring(start, idx);
//    			HibernateUtil.sessionFactory.evictEntity(ent);
        		evictHibernateRegion(ent);
    			start = idx + 1;
    			lastIdx = idx;
    			idx = entity.indexOf(":", start); 
    		}
    		ent = entity.substring(lastIdx+1);
//			HibernateUtil.sessionFactory.evictEntity(ent);
    		evictHibernateRegion(ent);
    	}
    	else
    	{
//    		HibernateUtil.sessionFactory.evictEntity(entity);
    		evictHibernateRegion(entity);
    	}
    }

    private static void notifyHibernateCacheUpdate(String entity)
    {
        if (esb != null && cacheNotificationStatus.get() == true)
        {
            Map<String, String> msg = new HashMap<String, String>();
            msg.put(Constants.SOURCE_GUID, guid);
            msg.put(HB_CACHE_REGION, entity);

            esb.sendInternalMessage(Constants.ESB_NAME_RSVIEWS, "com.resolve.persistence.util.HibernateUtilHelper.clearHBObjectsCache", msg);
            esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROLS, "com.resolve.persistence.util.HibernateUtilHelper.clearHBObjectsCache", msg);
        }
    }

    public static void notifyAllHibernateCacheUpdate()
    {
        for (String target : hibernateCacheUpdateTargets)
        {
            final String targetName = target;

            notifyHibernateCacheUpdate(targetName);
        }
    }

    private static synchronized void registerHibernateCacheUpdaters()
    {
        for (String target : hibernateCacheUpdateTargets)
        {
            final String targetName = target;
            BlockingSIDListener sidListener = new BlockingSIDListener()
            {
                @Override
                public boolean onDataOperation(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) throws CallbackException
                {
                    notifyHibernateCacheUpdate(targetName);
                    return false;
                }
            };
            BlockingUListener uListener = new BlockingUListener()
            {
                @Override
                public boolean onUpdate(Object entity, Serializable id, Object[] state, Object[] previousState, String[] propertyNames, Type[] types) throws CallbackException
                {
                    notifyHibernateCacheUpdate(targetName);
                    return false;
                }
            };

            if (target.contains(":"))
            {
            	//this is case of multiple targets
            	target = target.substring(0, target.indexOf(":"));
            }	
            	
            ResolveInterceptor.addInsertAfterListener(target, sidListener);
            ResolveInterceptor.addDeleteListener(target, sidListener);
            ResolveInterceptor.addUpdateAfterListener(target, uListener);
        }
    }

    public static boolean getCacheNotification()
    {
        return cacheNotificationStatus.get();
    }

    public static void setCacheNotification(boolean status)
    {
        cacheNotificationStatus.set(status);
    }
    
    public static boolean getDebugOutput()
    {
    	return debugOutput;
    }

    public static void setDebugOutput(boolean b)
    {
    	debugOutput=b;
    }
    
    private static void printDBPoolInfo()
    {
    	Log.log.info(String.format("DB Connection Pool Stats: max = %d, allocated = %d, " +
                				   "available (from allocated) = %d, in use (from allocated) = %d, " +
                				   "busy ratio = %f", DBPoolUtils.getMaxPoolSize(), DBPoolUtils.getTotalSize(), 
                				   DBPoolUtils.getAvailableSize(),
                				   (DBPoolUtils.getTotalSize() - DBPoolUtils.getAvailableSize()), 
                				   (((DBPoolUtils.getTotalSize() - DBPoolUtils.getAvailableSize()) * 1.0F) / 
                				    DBPoolUtils.getMaxPoolSize())));
    }
    
    public static boolean getStacktraceOutput()
    {
    	return stacktraceOutput;
    }

    public static void setStacktraceOutput(boolean b)
    {
    	stacktraceOutput=b;
    }
    
    public static boolean doesModelExist(String modelName)
    {
    	return class2TableMap.containsKey(modelName) && (modelName.startsWith("com.resolve.persistence.model"));
    }
    
    public static boolean isHibernateInitializing()
    {
        return isInitInProgress.get();
    }

    public static boolean isHibernateInitializing2()
    {
    	boolean currentIsInitInProgress = true;
    	
    	Lock lock = initLock.readLock();
    	
    	if (lock.tryLock()) {
    		currentIsInitInProgress = isInitInProgress.get();
    		lock.unlock();
    	}
    	
        return currentIsInitInProgress;
    }
    
    public static String getCurrentUsername()
    {
        String result = "system";
        
        HibernateUtil.User contextUser = HibernateUtil.getCurrentUser();
        if (contextUser != null && contextUser.name != null)
        {
            result = contextUser.name;
        }
        return result;
    }
      
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static ResolveSharedObject findLockObject(String lockName) throws Exception {
        ResolveSharedObject rso = null;
        StringBuffer sql = new StringBuffer(" from ResolveSharedObject as rso ").append(" where rso.UName = :lockName");
        Query query = HibernateUtil.createQuery(sql.toString());
        query.setParameter("lockName", lockName, StringType.INSTANCE);
        
        List<Object> lockObjs = query.list();
        
        if (lockObjs != null && lockObjs.size() > 0) {
//            lockId = ((ResolveSharedObject)lockObjs.get(0)).getsys_id();
            rso = (ResolveSharedObject)lockObjs.get(0);
        }
        return rso;
    } 
        
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static void initMasterLockObject()
    {
        try
            {
              HibernateProxy.setCurrentUser("system");
                HibernateProxy.execute(() -> {
                	ResolveSharedObject rso = null;
                	StringBuffer sql = new StringBuffer(" from ResolveSharedObject as rso ").append(" where rso.id = :key");
                    Query query = HibernateUtil.createQuery(sql.toString());
                    query.setParameter("key", Constants.MASTER_LOCK_SYSID , StringType.INSTANCE);
                    List<Object> lockObjs = query.list();
                    if (lockObjs != null && lockObjs.size() > 0)
                    {
                        rso = (ResolveSharedObject)lockObjs.get(0);
                    }
                    else
                    {
                        rso = new ResolveSharedObject();
                        rso.setUName(Constants.ACTIONTASK_MASTER_LOCK);
                        rso.setsys_id(Constants.MASTER_LOCK_SYSID);
                        getCurrentSession().replicate(rso, ReplicationMode.OVERWRITE );
                    }
                    
                });
            }
            catch (Throwable t)
            {
                              HibernateUtil.rethrowNestedTransaction(t);
                Log.log.info(t,t);
            }
        
    }
    
    public static ResolveSharedObject createLockObjectSafe(String lockName) {
        ResolveSharedObject rso = null;
        try {
              HibernateProxy.setCurrentUser("system");
        	rso = (ResolveSharedObject) HibernateProxy.executeNoCacheHibernateInitLocked(() -> {
        		return findLockObject(lockName);
        	});
        } catch (Throwable t) {
        	HibernateUtil.rethrowNestedTransaction(t);
        	Log.log.error(String.format("Error %sin retrieving Resolve shared object named %s, " +
        								"while creating it in safe manner ", 
        								(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
        								lockName),t);
        }
        
        if (rso!=null) {
            return rso;
        } else {
            try {
                  HibernateProxy.setCurrentUser("system");
            	rso = (ResolveSharedObject) HibernateProxy.executeNoCacheHibernateInitLocked(() -> {
	                ResolveSharedObject master = (ResolveSharedObject)getCurrentSession()
	                												  .get(ResolveSharedObject.class, 
	                													   Constants.MASTER_LOCK_SYSID, 
	                													   LockMode.PESSIMISTIC_WRITE);
	                
	                if (master == null) {
	                	String errMsg = String.format("Failed to find master object from database: sysid = %s", 
	                								  Constants.MASTER_LOCK_SYSID);
	                	Log.log.error(errMsg);
	                    throw new RuntimeException(errMsg);
	                }
	                
	                ResolveSharedObject searchedRso = findLockObject(lockName);
	                
	                if (searchedRso == null) {
	                	searchedRso = new ResolveSharedObject();
	                	searchedRso.setUName(lockName);
	                	getCurrentSession().save(searchedRso);
	                	// Persist immediately newly created object without waiting for committing transaction
	                	getCurrentSession().flush();
	                }
	                
	                return searchedRso;
            	});
            } catch (Throwable t) {
            	HibernateUtil.rethrowNestedTransaction(t);
            	Log.log.error(String.format("Error %sin querying Resolve shared object named %s after locking master object", 
											(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
											lockName),t);
            }
        }
        
        return rso;
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static synchronized Object exclusiveClusterExecution(String lockName, ExecutableObject exeObj)
    {
    	/*
    	 * This method needs to be protected against Hibernate factory getting re-initialized 
    	 * due to custom table creation/update and should always use ...NoCache2() methods. 
    	 */
    	
        Object result = null;

        ResolveSharedObject rso = null;
        
        try
        {
          HibernateProxy.setCurrentUser("system");
        	List<Object> lockObjs = (List<Object>) HibernateProxy.executeNoCacheHibernateInitLocked(() -> {
	            StringBuffer sql = new StringBuffer(" from ResolveSharedObject as rso ").append(" where rso.UName = :lockName");
		        Query query = HibernateUtil.createQuery(sql.toString());
		        query.setParameter("lockName", lockName, StringType.INSTANCE);
		        
		        return query.list();
        	});
        	
	        if (lockObjs != null && lockObjs.size() > 0) {
	            rso = (ResolveSharedObject)lockObjs.get(0);
	        }	        
        } catch (Throwable t) {
        	HibernateUtil.rethrowNestedTransaction(t); //
            Log.log.error(String.format("Error %sin retrieving Resolve shared object named %s, creating...", 
            							(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
            							lockName),t);
        }
        
        if (rso == null) {
            rso = createLockObjectSafe(lockName);
        }
        
        int retry = 0;
        while (true) {
            retry++;
            try {
            	String rsoId = rso.getsys_id();
                  HibernateProxy.setCurrentUser("system");
            	rso = (ResolveSharedObject)  HibernateProxy.executeNoCacheHibernateInitLocked(() -> {
            		return getCurrentSession().get(ResolveSharedObject.class, rsoId, LockMode.PESSIMISTIC_WRITE);
            	});
            	
            	if (exeObj != null) {
            		Log.log.info(String.format("start exclusive execution on exeObj = %s", exeObj));
            		result = exeObj.execute();
            		Log.log.info(String.format("end exclusive execution on exeObj = %s", exeObj));
            	}
            	
                break;
            } catch (Throwable t) {
                String rootCause = ExceptionUtils.getRootCauseMessage(t);
                Log.log.info(String.format("Root Cause: %s", (StringUtils.isNotBlank(rootCause) ? rootCause : "unknown")));
                if (StringUtils.isNotBlank(rootCause) && rootCause.contains(Constants.DB_LOCK_ERR_MSG)) {
                    Log.log.warn(String.format("Retry attept %d to aquire Resolve shared object named %s.", retry, lockName));
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        // no need to catch this exception
                    }
                    continue;
                } else {
                    Log.log.error(String.format("Root cause: [%s] is not expected [%s]", rootCause, 
                    							Constants.DB_LOCK_ERR_MSG), t);
                    HibernateUtil.rethrowNestedTransaction(t);
                }
            }
        }
        return result;
    } // execlusiveClusterExecution
    
    private static void logSharedObjects(List<Object> lockObjs, String objName)
    {
        Log.log.warn("Total number of shared object =" + lockObjs.size() + ", name = " + objName);
        for (Object o : lockObjs)
        {
            ResolveSharedObject rsot = (ResolveSharedObject)o;
            
            Object content = null;
            byte[] wsBytes = null;
            if (rsot!=null && rsot.getUContent()!=null)
            {
                wsBytes=rsot.getUContent();
            }

            if (wsBytes!=null&& wsBytes.length>0)
            {
                ByteArrayInputStream bis = new ByteArrayInputStream(wsBytes);
                ObjectInput in = null;
                try {
                  in = new ObjectInputStream(bis);
                  content = in.readObject();
                }
                catch (Exception ex)
                {
                    Log.log.error("Failed to deserialize Java object from database: " + objName, ex);
                    throw new RuntimeException(ex);
                }
                finally {
                    try
                    {
                        bis.close();
                        in.close();
                    }
                    catch (Throwable ex)
                    {
                        //ignore
                    }
                }
            }
            
            Log.log.warn("Shared obj name=" + rsot.getUName() + ", number= " + rsot.getUNumber() + ", sys_id=" + rsot.getsys_id() + ", content=" + content);
        }
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static String saveJavaObject(String objName, Object obj, Integer num)
    {
    	if (obj==null && num==null)
    	{
    		throw new RuntimeException("All data are null. Nothing can be saved to database!");
    	}
        
    	ResolveSharedObject result = null;
        try
        {
          HibernateProxy.setCurrentUser("system");
        	result = (ResolveSharedObject) HibernateProxy.execute(() -> {
	            ResolveSharedObject rso = null;
	            byte[] wsBytes = null;
	        	if (obj!=null)
	        	{
		            ByteArrayOutputStream bos = new ByteArrayOutputStream();
		            ObjectOutput out = null;
		            try {
		              out = new ObjectOutputStream(bos);
		              out.writeObject(obj);
		              wsBytes = bos.toByteArray();
		            }
		            catch (Exception ex)
		            {
		                Log.log.error("Failed to serialize object to database: " + obj, ex);
		                throw new RuntimeException(ex);
		            }
		            finally
		            {
		                try
		                {
		                    out.close();
		                    bos.close();
		                }
		                catch (Exception ex)
		                {
		                    //ignore
		                }
		            }
	        	}
	
	            StringBuffer sql = new StringBuffer(" from ResolveSharedObject as rso ").append(" where rso.UName = :objName");
		        Query query = HibernateUtil.createQuery(sql.toString());
		        query.setParameter("objName", objName, StringType.INSTANCE);
		        List<Object> lockObjs = query.list();
		        boolean isNew = false;
		        if (lockObjs != null && lockObjs.size() > 0)
		        {
		            rso = (ResolveSharedObject)lockObjs.get(0);
		            if (lockObjs.size()>1)
		            {
		                logSharedObjects(lockObjs, objName);
		            }
		        }
		        else
		        {
		        	rso = new ResolveSharedObject();
		        	rso.setUName(objName);
		        	isNew=true;
		        }
	
		        if (wsBytes!=null)
		        {
		        	rso.setUContent(wsBytes);
		        }
	
		        if (num!=null)
		        {
		        	rso.setUNumber(num);
		        }
	
		        if (isNew)
		        {
		        	HibernateUtil.getCurrentSession().save(rso);
		        }
		        return rso;
	        
        	});
        }
        catch (Throwable t)
        {
                   HibernateUtil.rethrowNestedTransaction(t);
        	Log.log.error(t,t);
        }
        return result.getsys_id();
    } // saveJavaObject

    public static String saveJavaObjectOnly(String objName, Object obj)
    {
    	return saveJavaObject(objName, obj, null);
    }

    public static String saveJavaNumberOnly(String objName, Integer number)
    {
    	return saveJavaObject(objName, null, number);
    }
    
    public static Object getJavaObjectOnly(String objName)
    {
    	Map<String,Object> pair = getJavaObject(objName);
    	if (pair==null)
    	{
    		return null;
    	}
    	return pair.get("OBJECT");
    }

    public static Object getJavaObjectOnlyNonBlocking(String objName)
    {
        Map<String,Object> pair = getJavaObjectNonBlocking(objName);
        if (pair==null)
        {
            return null;
        }
        return pair.get("OBJECT");
    }
    
    public static Integer getJavanNumberOnly(String objName)
    {
    	Map<String,Object> pair = getJavaObject(objName);
    	if (pair==null)
    	{
    		return null;
    	}
    	return (Integer)pair.get("NUMBER");
    }
    
    public static Integer getCurrentRBCount(String wiki)
    {
        String mapName = Constants.EXECUTE_PROCESS_CONCURRENT_LIMIT + wiki;
        Integer num =  HibernateUtil.getJavanNumberOnly(mapName);
        return num;
    }
    
    public static void resetRBLimitCounter(String wiki)
    {
        String mapName = Constants.EXECUTE_PROCESS_CONCURRENT_LIMIT + wiki;
        HibernateUtil.saveJavaNumberOnly(mapName, 0);
    }
    
    public static void initRBLimitData(String objName)
    {
        deleteAllRBLimitData();
    }    
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static synchronized void deleteAllRBLimitData()
    {
        
        try
        {
          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.execute(() -> {
        		Session session = getCurrentSession();
	            StringBuilder sql = new StringBuilder(" from ResolveSharedObject as rso where rso.UName like 'PROCESS_CONCURRENT_LIMIT%' ");
	            Query query = HibernateUtil.createQuery(sql.toString());
	            List<Object> lockObjs = query.list();
	            for (Object obj : lockObjs)
	            {
	                session.delete(obj);
	            }
        	});
        }
        catch (Throwable t)
        {
                      HibernateUtil.rethrowNestedTransaction(t);
            Log.log.error(t,t);
        }
    }
    
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static Map<String,Object> getJavaObject(String objName)
    {

        ResolveSharedObject rso = null;
        
        try
        {
          HibernateProxy.setCurrentUser("system");
        	List<Object> lockObjs = (List<Object>) HibernateProxy.execute(() -> {
        		StringBuffer sql = new StringBuffer(" from ResolveSharedObject as rso ").append(" where rso.UName = :objName");
    	        Query query = HibernateUtil.createQuery(sql.toString());
    	        query.setParameter("objName", objName, StringType.INSTANCE);
    	         return query.list();
        	});
            
	        if (lockObjs != null && lockObjs.size() > 0)
	        {
	            rso = (ResolveSharedObject)lockObjs.get(0);
                if (lockObjs.size()>1)
                {
                    logSharedObjects(lockObjs, objName);
                }
	        }
	        else
	        {
	        	rso = null;
	        }
        }
        catch (Throwable t)
        {
                   HibernateUtil.rethrowNestedTransaction(t);
        	Log.log.error(t,t);
        }
        
        if (rso==null)
        {
        	return null;
        }

        try
        {
        	Serializable rsoId = rso.getsys_id();
        	
          HibernateProxy.setCurrentUser("system");
            rso = (ResolveSharedObject) HibernateProxy.execute(() -> {
            	SessionImplementor s = getCurrentSession();            	
				return s.get(ResolveSharedObject.class, rsoId, LockMode.PESSIMISTIC_WRITE);
            });
        }
        catch (Throwable t)
        {
                   HibernateUtil.rethrowNestedTransaction(t);
        	Log.log.error(t,t);
        }
        
        Map<String,Object> result = new HashMap<String, Object>();
        byte[] wsBytes = null;

        if (rso!=null && rso.getUContent()!=null)
        {
            wsBytes=rso.getUContent();
        }

        if (wsBytes!=null&& wsBytes.length>0)
        {
            ByteArrayInputStream bis = new ByteArrayInputStream(wsBytes);
            ObjectInput in = null;
            try {
              in = new ObjectInputStream(bis);
              result.put("OBJECT", in.readObject());
            }
            catch (Exception ex)
            {
                Log.log.error("Failed to deserialize Java object from database: " + objName, ex);
                throw new RuntimeException(ex);
            }
            finally {
                try
                {
                    bis.close();
                    in.close();
                }
                catch (Exception ex)
                {
                    //ignore
                }
            }
        }
        
        if (rso != null && rso.getUNumber() != null)
        {
        	result.put("NUMBER", rso.getUNumber());
        }

        return result;
    } // getJavaObject

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static Map<String,Object> getJavaObjectNonBlocking(String objName)
    {
        ResolveSharedObject rso = null;
        
        try
        {
          HibernateProxy.setCurrentUser("system");
        	rso = (ResolveSharedObject) HibernateProxy.execute(() -> {
        	    StringBuffer sql = new StringBuffer(" from ResolveSharedObject as rso ").append(" where rso.UName = :objName");
                Query query = HibernateUtil.createQuery(sql.toString());
                query.setParameter("objName", objName, StringType.INSTANCE);
                List<Object> lockObjs = query.list();
                if (lockObjs != null && lockObjs.size() > 0)
                {
                    ResolveSharedObject result = (ResolveSharedObject)lockObjs.get(0);
                    if (lockObjs.size()>1)
                    {
                        logSharedObjects(lockObjs, objName);
                    }
                    
                    return result;
                }
                else
                {
                    return null;
                }
                
        	});
        
        }
        catch (Throwable t)
        {
                      HibernateUtil.rethrowNestedTransaction(t);
            Log.log.error(t,t);
        }
        
        if (rso==null)
        {
            return null;
        }
        
        String rsoId = rso.getsys_id();
        
        try
        {
          HibernateProxy.setCurrentUser("system");
        	rso = (ResolveSharedObject) HibernateProxy.execute( ()-> {
                return getCurrentSession().get(ResolveSharedObject.class, rsoId);
        	});
            
        }
        catch (Throwable t)
        {
                      HibernateUtil.rethrowNestedTransaction(t);
            Log.log.error(t,t);
        }
        
        Map<String,Object> result = new HashMap<String, Object>();
        byte[] wsBytes = null;

        if (rso!=null && rso.getUContent()!=null)
        {
            wsBytes=rso.getUContent();
        }

        if (wsBytes!=null&& wsBytes.length>0)
        {
            ByteArrayInputStream bis = new ByteArrayInputStream(wsBytes);
            ObjectInput in = null;
            try {
              in = new ObjectInputStream(bis);
              result.put("OBJECT", in.readObject());
            }
            catch (Exception ex)
            {
                Log.log.error("Failed to deserialize Java object from database: " + objName, ex);
                throw new RuntimeException(ex);
            }
            finally {
                try
                {
                    bis.close();
                    in.close();
                }
                catch (Exception ex)
                {
                    //ignore
                }
            }
        }
        
        if (rso != null && rso.getUNumber() != null)
        {
            result.put("NUMBER", rso.getUNumber());
        }

        return result;
    } // getJavaObjectNonBlocking
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static int deleteJavaObject(String objName)
    {
        try
        {
          HibernateProxy.setCurrentUser("system");
        	return (int) HibernateProxy.execute(() -> {
            	int count = 0;
	        	Session session = getCurrentSession();
	
	        	StringBuffer sql = new StringBuffer(" from ResolveSharedObject as rso ").append(" where rso.UName = :objName");
		        Query query = HibernateUtil.createQuery(sql.toString());
		        query.setParameter("objName", objName, StringType.INSTANCE);
		        List<Object> lockObjs = query.list();
		        if (lockObjs != null && lockObjs.size() > 0)
		        {
	                if (lockObjs.size()>1)
	                {
	                    logSharedObjects(lockObjs, objName);
	                }
		            for (Object o : lockObjs)
		            {
		            	++count;
		            	session.delete(o);
		            }
		        }
		        return count;
            });
        }
        catch (Throwable t)
        {
                   HibernateUtil.rethrowNestedTransaction(t);
        	Log.log.error(t,t);
        }
        
        return 0;
    } // deleteJavaObject
    
    public static int getJdbcBatchSize()
    {
        return getSessionFactory().getSessionFactoryOptions().getJdbcBatchSize();
    }
    
    public static void setTransactionTimeout(int seconds) throws Exception
    {
        userTransaction.setTransactionTimeout(seconds); // 4 min.
    }
    
    public static void refreshPool()
    {
        if(refreshInterval>0)
        {
//            if (ads!=null)
//            {
//                try
//                {
//                    Log.log.info("Start refresh connection pool");
//                    ads.refreshPool();
//                    Log.log.info("End refresh connection pool");
//                }
//                catch(Throwable ex)
//                {
//                    Log.log.error("Failed to refresh connection pool", ex);
//                }
//            }
        }
    }
    
    public static Set<String> getListOfResolveTables()
    {
        return new HashSet<String>(tableNameLC2TCMap.values());
    }
    
    public static DatabaseMetaData getDatabaseMetaData(String dbUrl, String userName, String password)
    {
        DatabaseMetaData dbMetaData = null;
        Connection conn = null;
        
        try
        {
            conn = DriverManager.getConnection(dbUrl, userName, password);
            
            if (conn != null)
            {
                dbMetaData = conn.getMetaData();
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error [" + e.getMessage() + "] occurred while getting database metadata for [" + dbUrl + "].", e);
        }
        finally
        {
            try
            {
                if (conn != null && !conn.isClosed())
                {
                    conn.close();
                }
            }
            catch (SQLException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        return dbMetaData;
    }
    
    public static String getDatabaseVersionByQuery(String dbUrl, String userName, String password)
    {
        String dbVersion = null;
        Connection conn = null;
        Statement stmt = null;
        
        try
        {
            conn = DriverManager.getConnection(dbUrl, userName, password);
            
            if (conn != null)
            {
                stmt = conn.createStatement();
                boolean querySuccess = stmt.execute("select @@version");
                
                if (querySuccess)
                {
                    ResultSet rs = stmt.getResultSet();
                    
                    if (rs.next())
                    {
                        dbVersion = rs.getString(1);
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error [" + e.getMessage() + "] occurred while getting database version by query for [" + dbUrl + "].", e);
        }
        finally
        {
            try
            {
                if (stmt != null && !stmt.isClosed())
                {
                    stmt.close();
                }
                
                if (conn != null && !conn.isClosed())
                {
                    conn.close();
                }
            }
            catch (SQLException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        return dbVersion;
    }
    
    public static boolean isOracle()
    {
        return isOracle;
    }
    
    public static List<QueryFilter> massageFiltersForClobField(String modelName, List<QueryFilter> filters)
    {
        List<QueryFilter> result = new ArrayList<QueryFilter>();
        
        for (QueryFilter filter : filters)
        {
            if (filter.getCondition().equals(QueryFilter.EQUALS) || filter.getCondition().equals(QueryFilter.NOT_EQUALS))
            {
                Map<String, String> columnTypeMap = modelColumnTypeMap.get("com.resolve.persistence.model." + modelName);
                if (columnTypeMap!= null) {
                    String type = getType (filter.getField(), columnTypeMap);
                    if (type != null && type.toLowerCase().contains("clob"))
                    {
                        if (filter.getCondition().equals(QueryFilter.EQUALS))
                        {
                            filter.setCondition(QueryFilter.CONTAINS);
                        }
                        else
                        {
                            filter.setCondition(QueryFilter.NOT_CONTAINS);
                        }
                        filter.setValue("%" + filter.getValue() + "%");
                    }
                }
            }
            result.add(filter);
        }
        
        return result;
    }
    
    private static String getType(String field, Map<String, String> columnTypeMap)
    {
        String type = null;
        
        if (StringUtils.isNotBlank(field))
        {
            String[] fieldArray = field.split("\\.");
            if (fieldArray.length == 1)
            {
                type = columnTypeMap.get(fieldArray[0]);
            }
            else
            {
                for (int i = 0; i<fieldArray.length; i++)
                {
                    String model = columnTypeMap.get(fieldArray[i]);
                    
                    if (StringUtils.isBlank(model))
                    {
                        continue;
                    }
                    if (!model.startsWith("com."))
                    {
                        model = "com.resolve.persistence.model." + model;
                    }
                    Map<String, String> localColTypeMap = modelColumnTypeMap.get(model);
                    if (localColTypeMap != null)
                    {
                        type = localColTypeMap.get(fieldArray[i+1]);
                    }
                    
                    /*
                     * If we have reference field in a model, we need to iterate through
                     * all of them until we reach the field. The check below is precautionary
                     * measure for not getting stepping onto IndexOutOfBoundException.
                     * At every value of i, we should have at least i+1 items in the array. 
                     */
                    if (StringUtils.isBlank(type) && ((fieldArray.length - (i+1)) <= 1))
                    {
                        break;
                    }
                }
            }
        }
        
        return type;
    }
    
    public static boolean isMySQL()
    {
        return isMySQL;
    }
    
    public static String getGatewayModelFilter(String gatewayType) {
        if (gatewayModelFilterMap.isEmpty()) {
            List<String> classNames = HibernateUtil.getAllMappingClassNames();
            if (CollectionUtils.isNotEmpty(classNames)) {
                classNames.stream().filter(className -> className.endsWith("Filter")).forEach(gatewayFilterModelClass -> {
                    String filterModel = gatewayFilterModelClass.substring(gatewayFilterModelClass.lastIndexOf('.') + 1, gatewayFilterModelClass.indexOf("Filter"));
                    gatewayModelFilterMap.put(filterModel.toLowerCase(), gatewayFilterModelClass);
                });
            }
        }
        return gatewayModelFilterMap.get(gatewayType.toLowerCase());
    }
    
    public static void clearCachedUser(String userName, boolean sendMsg) {
    	clearCachedDBObject(DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION, userName);
    	clearCachedDBObject(DBCacheRegionConstants.USER_ROLES_CACHE_REGION, userName);
    	clearCachedDBObject(DBCacheRegionConstants.NEW_HAS_ACCESS_RIGHT_CACHE_REGION, userName);
    	clearCachedDBObject(DBCacheRegionConstants.IS_ORG_ACCESSIBLE_CACHE_REGION, userName);
    	
    	if (sendMsg)
        {
            Map<String, String> msg = new HashMap<String, String>();
            msg.put(Constants.SOURCE_GUID, guid);
            msg.put(CACHED_USER_NAME, userName);
            
            if (esb != null)
            {
                esb.sendInternalMessage(Constants.ESB_NAME_RSVIEWS, "com.resolve.persistence.util.HibernateUtilHelper.clearCachedUser", msg);
                esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROLS, "com.resolve.persistence.util.HibernateUtilHelper.clearCachedUser", msg);
            }
        }
    }
    
    public static void lockHibernateInit() {
    	Lock lock = initLock.readLock();
    	
    	while(true) {
    		if (!isHibernateInitializing2()) {
    			lock.lock();
    			break;
    		}
    		
			try
	        {
	            Thread.sleep(2000);
	        }
	        catch(Exception ex)
	        {
	            //ignore
	        }
    	}
    }
    
    public static void unlockHibernateInit() {
    	Lock lock = initLock.readLock();
    	lock.unlock();
    }

    public static String getValidQuery(String sql) throws Exception {
        return ESAPI.validator().getValidInput(
                    "Validate sql query", 
                    SQLUtils.getSafeSQL(sql),
                    ResolveDefaultValidator.HQL_SQL_SAFE_STRING_VALIDATOR, 
                    ResolveDefaultValidator.MAX_HQL_SQL_SAFE_STRING_LENGTH,
                    false
                );
    }

    public static String getValidMetaData(String tableName) throws Exception {
        return ESAPI.validator().getValidInput(
                    "Validate metadata", 
                    tableName,
                    ResolveDefaultValidator.HQL_SQL_SAFE_TABLE_COL_NAME_STRING_VALIDATOR, 
                    ResolveDefaultValidator.MAX_HQL_SQL_SAFE_TABLE_COL_NAME_STRING_LENGTH,
                    false
                );
    }
    
    /*
     * Execute around method pattern
     * 
     * Caller of this method need not worry about starting, committing or rolling back the transaction,
     * but to concentrate on the actual business action. In other words, the action is automatically wrapped
     * inside database transaction. If anything goes wrong, the transaction is automatically tolled back.
     * The exception is logged and re-thrown so that appropriate action can be taken. 
     */
    public static void action(Consumer<HibernateUtil> block, String username) throws Throwable {
        HibernateUtil util = new HibernateUtil();
        try {
            HibernateProxy.execute(() -> {
            	block.accept(util);
            });            
        } catch (Throwable t) {
            Log.log.error(t.getMessage(), t);
            HibernateUtil.rethrowNestedTransaction(t);
            // Guard against rethrowNestedTransaction, which is NOP if count of nested transaction is 0
            throw t;
        }
    }
    
    /*
     * Execute around method pattern (HibernateInitLocked)
     * 
     * Caller of this method need not worry about starting, committing or rolling back the transaction,
     * but to concentrate on the actual business action. In other words, the action is automatically wrapped
     * inside database transaction. If anything goes wrong, the transaction is automatically tolled back.
     * The exception is logged and re-thrown so that appropriate action can be taken. 
     */
	public static void actionHibernateInitLocked(Consumer<HibernateUtil> block, String username) throws Throwable {
        HibernateUtil util = new HibernateUtil();
        try {
        	HibernateProxy.executeHibernateInitLocked(() -> {
        		block.accept(util);
        	});
        } catch (Throwable t) {
        	Log.log.error(t.getMessage(), t);
        	HibernateUtil.rethrowNestedTransaction(t);
        	// Guard against rethrowNestedTransaction, which is NOP if count of nested transaction is 0
            throw t;
        }
    }
    
    /*
     * Execute around method pattern (HibernateInitLocked)
     * 
     * Caller of this method need not worry about starting, commiting or rollbacking the transaction,
     * but to concentrate on the actual business action. In other words, the action is automatically wrapped
     * inside database transaction. If anything goes wrong, the transaction is automatically tolled back.
     * The exception is logged and re-thrown so that appropriate action can be taken. 
     */
	public static void actionNoCacheHibernateInitLocked(Consumer<HibernateUtil> block, String username) throws Throwable {
        HibernateUtil util = new HibernateUtil();
        try {
        	HibernateProxy.executeNoCacheHibernateInitLocked(() -> {
        		block.accept(util);
        	});
        } catch (Throwable t) {
        	Log.log.error(t.getMessage(), t);
        	HibernateUtil.rethrowNestedTransaction(t);
        	// Guard against rethrowNestedTransaction, which is NOP if count of nested transaction is 0
            throw t;
        }
    }
	
    private static String getOracleUrl()
    {
        String host = configSQL.getHost();
        String dbname = configSQL.getDbname(); 
        
        // update host port if not defined
        if (host.indexOf(':') < 0)
        {
            host += ":1521";
        }

        String dbUrl = "jdbc:oracle:thin:@" + host + ":" + dbname;

        return dbUrl;
    }
    
    private static String getMySQLUrl()
    {
        String host = configSQL.getHost();
        String dbname = configSQL.getDbname(); 
        String username = configSQL.getUsername();
        String pass = configSQL.getP_assword();
        
        if(!host.contains(":")) {
          host += ":3306";
        }
        String dbUrl = "jdbc:mysql://" + host + "/" + dbname;
        
        DatabaseMetaData dbMetaData = getDatabaseMetaData(dbUrl, username, pass);
        String storageEngine = null;
        
        try
        {
            if (dbMetaData != null)
            {
                String dbVersionStr = dbMetaData.getDatabaseProductVersion();
                
                // Handle JDBC driver not retruning product version by making select @@version query
                
                if (StringUtils.isBlank(dbVersionStr))
                {
                    dbVersionStr = getDatabaseVersionByQuery(dbUrl, username, pass);
                }
                
                if (StringUtils.isNotBlank(dbVersionStr))
                {
                    String[] dbVersionSplitStrs = dbVersionStr.split("\\.");
                    
                    if (dbVersionSplitStrs != null && dbVersionSplitStrs.length >= 2)
                    {
                        int version = Integer.parseInt(dbVersionSplitStrs[0]) * 10 + Integer.parseInt(dbVersionSplitStrs[1]);
                        
                        if (version < 57) // 5.6 or before use MyISAM
                        {
                            storageEngine = "&sessionVariables=storage_engine=MyISAM";
                        }
                    }
                }
                else
                {
                    Log.log.error("Unable to identify DB version of MySql for " + dbUrl + ", Unable to set correct Storage Engine for MySql!!!" );
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        // MySql UTF8 support
        //      dbUrl += "?characterEncoding=UTF-8";
        // RBA-13357 : Resolve is not working with MySql 5.7
        // dbUrl += "?characterEncoding=UTF-8&sessionVariables=storage_engine=MyISAM";
        dbUrl += dbUrl.contains("?") ? "&" : "?";
        //pinGlobalTxToPhysicalConnection needs to be a URL parmeter for MariaDB Driver
        dbUrl += "characterEncoding=UTF-8&pinGlobalTxToPhysicalConnection=true";
        
        if (StringUtils.isNotBlank(storageEngine))
        {
            dbUrl += storageEngine; 
        }
        
        Log.log.info("db url: " + dbUrl);

        return dbUrl;
    }
    
} // HibernateUtil
