package com.resolve.persistence.dao;

import com.resolve.persistence.model.PullGatewayFilterAttr;

public interface PullGatewayFilterAttrDAO extends GenericDAO<PullGatewayFilterAttr, String>
{

}
