package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.resolve.services.hibernate.vo.ResolveCaseConfigVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "resolve_case_config")
public class ResolveCaseConfig extends BaseModel<ResolveCaseConfigVO>
{
    private static final long serialVersionUID = -751807208736546550L;
    
    /**
     * configType could be 'CaseType', 'CaseStatus', 'CaseClassification', 'CasePriority', 'IOC' or 'CaseAttributes'
     */
    private String configType;
    
    /**
     * Name of the configuration. <br>E.g., if the configType is CaseType, the configValue could be Phishing or Malware<br>
     * For configType of CaseStatus, configValue could be Open, Assigned, In Progress, Complete or Closed<br>
     * For configType of CasePriority, configValue could be Low, medium, High or critical.<br>
     * For configType of CaseClassification, configValue could be Alert, Incident, Breach.<br>
     * For configType of IOC, configValue will be blank (instead, iocType, iocKey, iocDisplayName, iocSource and iocRegex will be populated.)<br>
     * For configType of CaseAttribute, configValue will hold the name of the custom attribute.
     */
    private String configValue;
    /**
     * For the configType of CasePriority, configValue could be Low, Medium, High or Critical and there will be
     * a corresponding ranking Low - 0, Medium - 1, High - 2, Critical - 3
     */
    private Integer priorityRank;
    
    /**
     * If the configType is CaseAttributes, the configValue will hold the name of the attribute and attribValue will hold it's corresponding value.
     * There could be multiple values associated with a single attribute. In that case, UI will display a drop down list.
     */
    private String attribValue;
    /**
     * iocType could be blank.<br>
     * If it's blank, the iocKey and iocName will be considered as global<br>
     * If it's not blank, it could have any caseType specified above and the corresponding key and displayName will
     * belong to that specific case type.
     */
    private String iocType;
    
    private String iocKey;
    private String iocDisplayName;
    private String iocSource;
    private Boolean iocRegex;
    
    /**
     * active could be true or false depicting whether configuration is active or de-active.
     */
    private Boolean active;
    
    @Column(name = "u_config_type", nullable = false)
    public String getConfigType()
    {
        return configType;
    }
    public void setConfigType(String configType)
    {
        this.configType = configType;
    }
    
    @Column(name = "u_active", length = 1)
    @Type(type = "yes_no")
    public Boolean getActive()
    {
        return active;
    }
    public void setActive(Boolean active)
    {
        this.active = active;
    }
    
    @Column(name = "u_config_value")
    public String getConfigValue()
    {
        return configValue;
    }
    public void setConfigValue(String configValue)
    {
        this.configValue = configValue;
    }
    
    @Column (name = "u_priority_rank")
    public Integer getPriorityRank()
    {
        return priorityRank;
    }
    public void setPriorityRank(Integer priorityRank)
    {
        this.priorityRank = priorityRank;
    }
    
    @Column (name = "u_attrib_value")
    public String getAttribValue()
    {
        return attribValue;
    }
    public void setAttribValue(String attribValue)
    {
        this.attribValue = attribValue;
    }
    
    @Column (name = "u_ioc_type")
    public String getIocType()
    {
        return iocType;
    }
    public void setIocType(String iocType)
    {
        this.iocType = iocType;
    }
    
    @Column (name = "u_ioc_key")
    public String getIocKey()
    {
        return iocKey;
    }
    public void setIocKey(String iocKey)
    {
        this.iocKey = iocKey;
    }
    
    @Column (name = "u_ioc_display_name")
    public String getIocDisplayName()
    {
        return iocDisplayName;
    }
    public void setIocDisplayName(String iocDisplayName)
    {
        this.iocDisplayName = iocDisplayName;
    }
    
    @Column (name = "u_ioc_source")
    public String getIocSource()
    {
        return iocSource;
    }
    public void setIocSource(String iocSource)
    {
        this.iocSource = iocSource;
    }
    
    @Column (name = "u_ioc_regex", length = 1)
    @Type(type = "yes_no")
    public Boolean getIocRegex()
    {
        return iocRegex;
    }
    public void setIocRegex(Boolean iocRegex)
    {
        this.iocRegex = iocRegex;
    }
    
    @Override
    public ResolveCaseConfigVO doGetVO()
    {
        ResolveCaseConfigVO vo = new ResolveCaseConfigVO();
        
        super.doGetBaseVO(vo);
        
        vo.setConfigType(this.getConfigType());
        vo.setConfigValue(this.getConfigValue());
        vo.setPriorityRank(this.getPriorityRank());
        vo.setAttribValue(this.getAttribValue());
        
        vo.setIocKey(this.getIocKey());
        vo.setIocDisplayName(this.getIocDisplayName());
        vo.setIocType(this.getIocType());
        vo.setIocSource(this.getIocSource());
        vo.setIocRegex(this.getIocRegex());
        
        vo.setActive(this.getActive());
        
        
        return vo;
    }
    
    @Override
    public void applyVOToModel(ResolveCaseConfigVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setConfigType(StringUtils.isNotBlank(vo.getConfigType()) && vo.getConfigType().equals(VO.STRING_DEFAULT) ? getConfigType() : vo.getConfigType());
            this.setConfigValue(StringUtils.isNotBlank(vo.getConfigValue()) && vo.getConfigValue().equals(VO.STRING_DEFAULT) ? getConfigValue() : vo.getConfigValue());
            
            this.setPriorityRank(vo.getPriorityRank() != null && vo.getPriorityRank().equals(VO.INTEGER_DEFAULT) ? getPriorityRank() : vo.getPriorityRank());
            this.setAttribValue(StringUtils.isNotBlank(vo.getAttribValue()) && vo.getAttribValue().equals(VO.STRING_DEFAULT) ? getAttribValue() : vo.getAttribValue());
            
            this.setIocType(StringUtils.isNotBlank(vo.getIocType()) && vo.getIocType().equals(VO.STRING_DEFAULT) ? getIocType() : vo.getIocType());
            this.setIocKey(StringUtils.isNotBlank(vo.getIocKey()) && vo.getIocKey().equals(VO.STRING_DEFAULT) ? getIocKey() : vo.getIocKey());
            this.setIocDisplayName(StringUtils.isNotBlank(vo.getIocDisplayName()) && vo.getIocDisplayName().equals(VO.STRING_DEFAULT) ? getIocDisplayName() : vo.getIocDisplayName());
            this.setIocSource(StringUtils.isNotBlank(vo.getIocSource()) && vo.getIocSource().equals(VO.STRING_DEFAULT) ? getIocSource() : vo.getIocSource());
            this.setIocRegex(vo.getIocRegex() != null ? vo.getIocRegex() : getIocRegex());
            
            this.setActive(vo.getActive() != null ? vo.getActive() : getActive());
        }
    }
}
