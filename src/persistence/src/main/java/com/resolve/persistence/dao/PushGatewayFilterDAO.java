package com.resolve.persistence.dao;

import com.resolve.persistence.model.PushGatewayFilter;

public interface PushGatewayFilterDAO extends GenericDAO<PushGatewayFilter, String> {

}
