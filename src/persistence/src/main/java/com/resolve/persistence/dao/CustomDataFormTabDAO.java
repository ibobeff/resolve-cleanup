/******************************************************************************
* (C) Copyright 2018
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.dao;

import com.resolve.persistence.model.CustomDataFormTab;

public interface CustomDataFormTabDAO extends GenericDAO<CustomDataFormTab, String>
{

}
