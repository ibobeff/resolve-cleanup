/******************************************************************************
* (C) Copyright 2019
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Map;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.resolve.services.hibernate.util.OrgsUtil;
import com.resolve.services.hibernate.vo.GatewayHealthCheckVO;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.interfaces.VO;

import static com.resolve.util.Log.*;
import static com.resolve.util.StringUtils.*;

/**
 * Gateway Health Check Result
 */
@Entity
@Table(name = "gateway_health_check",
	   uniqueConstraints = {
			@UniqueConstraint(columnNames = { "orgname", "remotename", "gtwtype", "queuename" }, 
							  name = "ghc_org_remote_type_queue_uk")
	   })
public class GatewayHealthCheck extends BaseModel<GatewayHealthCheckVO> {
	
	private static final long serialVersionUID = 1185953994044253079L;
    
    private String orgName;
    private String remoteName; 	// Container (RSRemote) GUID-NAME-GROUP
    private String gtwType;
    private String queueName;	// Composite queue name suffixed with Org (if set)
    private String report;		// Health Check Report JSON
	
    public GatewayHealthCheck() {
    }

    public GatewayHealthCheck(GatewayHealthCheckVO vo) {
        applyVOToModel(vo);
    }

    @Column(name = "orgname", length = 80, nullable = false)
    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
        
        if (isNotBlank(this.orgName) && !this.orgName.equalsIgnoreCase(OrgsVO.NONE_ORG_NAME) && isBlank(getSysOrg())) {
        	Orgs orgs = null;
        	
        	try {
        		orgs = OrgsUtil.findOrgModelByName(this.orgName);
        	} catch (Throwable t) {
        		log.warn(String.format("Error [%s]in getting Org id for Org name %s, missing Org id in the persisted " +
        							   "Gateway Health Check", (StringUtils.isNotBlank(t.getMessage()) ? 
        									   					"[" + t.getMessage() + "] " : ""), this.orgName));
        	}
        	
        	if (orgs != null) {
        		setSysOrg(orgs.getSys_id());
        	}
        }
    }

    @Column(name = "remotename", length = 80, nullable = false)
    public String getRemoteName() {
        return remoteName;
    }

    public void setRemoteName(String remoteName) {
        this.remoteName = remoteName;
    }

    @Column(name = "gtwtype", length = 40, nullable = false)
    public String getGtwType() {
        return gtwType;
    }

    public void setGtwType(String gtwType) {
        this.gtwType = gtwType;
    }

    @Column(name = "queuename", length = 40, nullable = false)
    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

	@Lob
    @Column(name = "report")
    public String getReport() {
        return report;
    }
	
	@Transient
	public Map<String, String> reportAsMap() {
		Map<String, String> reportAsMap = Collections.emptyMap();
		
		if (isNotBlank(report)) {
			reportAsMap = jsonObjectToMap((stringToJSONObject(report)));
		}
		
		return reportAsMap;
	}
	
    public void setReport(String report)
    {
        this.report = report;
    }
    
    @Transient
    public void mapReport(Map<String, String> reportAsMap) {
    	if (MapUtils.isNotEmpty(reportAsMap)) {
    		setReport(mapToJson(reportAsMap));
    	}
    }
    
    @Override
    public GatewayHealthCheckVO doGetVO() {
    	GatewayHealthCheckVO vo = new GatewayHealthCheckVO();
    	
        super.doGetBaseVO(vo);
        
        vo.setOrgName(getOrgName());
        vo.setRemoteName(getRemoteName());
        vo.setGtwType(getGtwType());
        vo.setQueueName(getQueueName());
        vo.setReport(getReport());
        
        return vo;
    }

    @Override
    public void applyVOToModel(GatewayHealthCheckVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);
            
            setOrgName(isNotBlank(vo.getOrgName()) && vo.getOrgName().equals(VO.STRING_DEFAULT) ? 
            		   getOrgName() : vo.getOrgName());
            setRemoteName(isNotBlank(vo.getRemoteName()) && vo.getRemoteName().equals(VO.STRING_DEFAULT) ? 
            			  getRemoteName() : vo.getRemoteName());
            setGtwType(isNotBlank(vo.getGtwType()) && vo.getGtwType().equals(VO.STRING_DEFAULT) ? getGtwType() : vo.getGtwType());
            setQueueName(isNotBlank(vo.getQueueName()) && vo.getQueueName().equals(VO.STRING_DEFAULT) ? 
            			 getQueueName() : vo.getQueueName());
            setReport(isNotBlank(vo.getReport()) && vo.getReport().equals(VO.STRING_DEFAULT) ? getReport() : vo.getReport());
        }
    }
    
    @Override
    public String toString() {
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
   	 	sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
    	
    	return String.format("[Org Id: %s, Created On: %s, Updated On: %s, Created By: %s, Updated By: %s, " +
				 			 "Modification Count: %d, Org Name: %s, Remote Name: %s, Type: %s, Queue Name: %s," +
				 			 "Report (JSON): %s]", getSysOrg(), sdf.format(getSysCreatedOn()), 
				 			 sdf.format(getSysUpdatedOn()), getSysCreatedBy(),
				 			 getSysUpdatedBy(), (getSysModCount() != null ? getSysModCount().intValue() : 0), getOrgName(), 
				 			 getRemoteName(), getGtwType(), getQueueName(), getReport());
    }

} // ResolveRegistration
