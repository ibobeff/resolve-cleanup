/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.resolve.services.hibernate.vo.MCPBlueprintVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
@Entity
@Table(name = "mcp_blueprint")
public class MCPBlueprint extends BaseModel<MCPBlueprintVO>
{
    private String blueprint;
    private String version;
    private Boolean active;

    public MCPBlueprint() {}

    public MCPBlueprint(MCPBlueprintVO vo)
    {
        applyVOToModel(vo);
    }

    @Lob
    @Column(name = "u_blueprint", length = 16777215, nullable=false)
    public String getBlueprint()
    {
        return this.blueprint;
    }

    public void setBlueprint(String UBlueprint)
    {
        this.blueprint = UBlueprint;
    }

    @Column(name = "u_version")
    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    @Column(name = "u_active")
    public Boolean getActive()
    {
        return this.active;
    }

    public void setActive(Boolean active)
    {
        this.active = active;
    }

    @Override
    public MCPBlueprintVO doGetVO()
    {
        MCPBlueprintVO vo = new MCPBlueprintVO();
        try
        {
            super.doGetBaseVO(vo);

            vo.setBlueprint(getBlueprint());
            vo.setVersion(getVersion());
            vo.setActive(getActive());
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return vo;
    }

    @Override
    public void applyVOToModel(MCPBlueprintVO vo)
    {
        if(vo != null)
        {
            try
            {
                super.applyVOToModel(vo);

                this.setBlueprint(StringUtils.isNotBlank(vo.getBlueprint()) && vo.getBlueprint().equals(VO.STRING_DEFAULT) ? getBlueprint() : vo.getBlueprint());
                this.setVersion(StringUtils.isNotBlank(vo.getVersion()) && vo.getVersion().equals(VO.STRING_DEFAULT) ? getVersion() : vo.getVersion());
                this.setActive(vo.getActive() != null ? vo.getActive() : getActive());
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
}
