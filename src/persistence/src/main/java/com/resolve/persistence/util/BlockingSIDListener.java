/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;


//Internal blocking listener for SQL insert, select, delete operations.
public abstract class BlockingSIDListener implements Ordered, SIDListener, Comparable<BlockingSIDListener>
{
	@Override
	public final int compareTo(BlockingSIDListener t)
	{
		if (t.getOrder()!=this.getOrder())
		{
			return t.getOrder() - getOrder();
		}
		else
		{
			return t.hashCode() - this.hashCode();
		}
	}
	
	@Override
	public int getOrder() { return 0; }

}
