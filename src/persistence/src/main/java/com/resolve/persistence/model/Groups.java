/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

// Generated Jan 15, 2010 12:12:58 PM by Hibernate Tools 3.2.4.GA

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.CallbackException;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.type.Type;

import com.resolve.esb.ESB;
import com.resolve.persistence.util.BlockingSIDListener;
import com.resolve.persistence.util.BlockingUListener;
import com.resolve.persistence.util.DBCacheRegionConstants;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.persistence.util.ResolveInterceptor;
import com.resolve.persistence.util.SIDListener;
import com.resolve.services.hibernate.vo.GroupsVO;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.RolesVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.util.AuditUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * Groups generated by hbm2java
 */
@Entity
@Table(name = "groups")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Groups extends BaseModel<GroupsVO>
{
    private static final long serialVersionUID = -1637852450864228183L;

    private String UName;
    private String UDescription;
    
    private Boolean UIsLinkToTeam;      //if true, than link/sync this group with the same team name
    private Boolean UHasExternalLink;   //if true, it indicates users of this group are using LDAP or AD for authentication
    
    // object references

    // object referenced by
    private Collection<UserGroupRel> userGroupRels;
    private Collection<GroupRoleRel> groupRoleRels;
    private Set<OrgGroupRel> orgGroupRels;

    public Groups()
    {
    }

    public Groups(GroupsVO vo)
    {
        applyVOToModel(vo);
    }

    public String toString()
    {
        return this.UName;
    } // toString

    @Column(name = "u_name", length = 100)
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    
    @Column(name = "u_is_link_to_team", length = 1)
    @org.hibernate.annotations.Type(type = "yes_no")
    public Boolean getUIsLinkToTeam()
    {
        return UIsLinkToTeam;
    }
    
    public Boolean ugetUIsLinkToTeam()
    {
        Boolean result = getUIsLinkToTeam();
        if(result == null)
        {
            result = false;
        }
        
        return result;
    }


    public void setUIsLinkToTeam(Boolean uIsLinkToTeam)
    {
        UIsLinkToTeam = uIsLinkToTeam;
    }
    
    @Column(name = "u_has_external_link", length = 1)
    @org.hibernate.annotations.Type(type = "yes_no")
    public Boolean getUHasExternalLink()
    {
        return UHasExternalLink;
    }

    public Boolean ugetUHasExternalLink()
    {
        Boolean result = getUHasExternalLink();
        if(result == null)
        {
            result = false;
        }
        
        return result;
    }

    
    public void setUHasExternalLink(Boolean uHasExternalLink)
    {
        UHasExternalLink = uHasExternalLink;
    }

    @Column(name = "u_description", length = 1000)
    public String getUDescription()
    {
        return this.UDescription;
    }

    public void setUDescription(String UDescription)
    {
        this.UDescription = UDescription;
    }

    //User-Groups relationship
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "group")
    @NotFound(action = NotFoundAction.IGNORE)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public Collection<UserGroupRel> getUserGroupRels()
    {
        return userGroupRels;
    }

    public void setUserGroupRels(Collection<UserGroupRel> userGroupRels)
    {
        this.userGroupRels = userGroupRels;
    }

    //Group-Role relationship
	// Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "group")
    @NotFound(action = NotFoundAction.IGNORE)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public Collection<GroupRoleRel> getGroupRoleRels()
    {
        return groupRoleRels;
    }

    public void setGroupRoleRels(Collection<GroupRoleRel> groupRoleRels)
    {
        this.groupRoleRels = groupRoleRels;
    }
    
    //Org-Group relationship
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "group")
    @NotFound(action = NotFoundAction.IGNORE)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public Set<OrgGroupRel> getOrgGroupRels()
    {
        return orgGroupRels;
    }
    
    public void setOrgGroupRels(Set<OrgGroupRel> orgGroupRels)
    {
        this.orgGroupRels = orgGroupRels;
    }
    
    public static void initListener(ESB mainESB)
    {
        // insert / update listeners
        BlockingSIDListener insertListener = new BlockingSIDListener()
        {
            @Override
            public boolean onDataOperation(Object entity, Serializable id, Object[] values, String[] names, Type[] types) throws CallbackException
            {
                AuditUtils.insertLog("groups", names, values);
                return true;
            }

        };
        ResolveInterceptor.addAsyncInsertListener(Groups.class.getName(), insertListener);

        BlockingUListener updateListener = new BlockingUListener()
        {
            @Override
            public boolean onUpdate(Object entity, Serializable id, Object[] values, Object[] prevValues, String[] names, Type[] types) throws CallbackException
            {
                AuditUtils.updateLog("groups", names, values, prevValues);
                return true;
            }

        };
        ResolveInterceptor.addAsyncUpdateListener(Groups.class.getName(), updateListener);

        // remove listener
        SIDListener deleteListener = new SIDListener()
        {
            @Override
            public boolean onDataOperation(Object entity, Serializable id, Object[] values, String[] names, Type[] types) throws CallbackException
            {
                AuditUtils.deleteLog("groups", names, values);
                return true;
            }
        };
        ResolveInterceptor.addAsyncDeleteListener(Groups.class.getName(), deleteListener);

    } // initListener	

    @Override
    public GroupsVO doGetVO()
    {
    	return doGetVO(false);
    }
    
    public GroupsVO doGetVO(boolean orgsOnly)
    {
        GroupsVO vo = new GroupsVO();
        super.doGetBaseVO(vo);

        vo.setUDescription(getUDescription());
        vo.setUName(getUName());
        vo.setUIsLinkToTeam(getUIsLinkToTeam());
        vo.setUHasExternalLink(getUHasExternalLink());

        //references 
        //add roles
        Collection<GroupRoleRel> groupRoleRels = Hibernate.isInitialized(this.groupRoleRels) ? getGroupRoleRels() : null;
        if(groupRoleRels != null && !orgsOnly)
        {
            List<RolesVO> roles = new ArrayList<RolesVO>();
            for(GroupRoleRel rel : groupRoleRels)
            {
                Roles role = rel.getRole();
                if(role != null)
                {
                    roles.add(role.doGetVO());
                }
            }
            
            vo.setGroupRoles(roles);
        }
        
        //add users
        if (!orgsOnly)
        {
            Collection<UserGroupRel> userGroupRels = Hibernate.isInitialized(this.userGroupRels) ? getUserGroupRels() : null;
	        if(userGroupRels != null)
	        {
	        	List<UsersVO> users = new ArrayList<UsersVO>();
	            for(UserGroupRel rel : userGroupRels)
	            {
	                Users user = rel.getUser();
	                if (user != null)
	                {
	                    /*
	                     * Removes the Users object from DB user cache if it exists before reseting UserGroupRels to null 
	                     * and then set back original User object back into DB user cache. User object saved in Db user cache
	                     * is shared and hence cannot be impacted by need to avoid cyclic references for populating Users
	                     * objects in Groups.
	                     * 
	                     *  HP TODO
	                     *  
	                     *  Need to think about locking on Hibernate cache used for DB caching.
	                     */
	                    
	                    Object userObjInDBCache = HibernateUtil.getCachedDBObject(
	                    													DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION, 
	                    													user.getUUserName());
	                    
	                    Users userObjInDBCacheWithUserGrpRels = (userObjInDBCache != null && 
	                    										 ((Users)userObjInDBCache).getUserGroupRels() != null && 
	                    										 !((Users)userObjInDBCache).getUserGroupRels().isEmpty()) ? 
	                    										(Users)userObjInDBCache : null;
	                    
	                    if (userObjInDBCacheWithUserGrpRels != null)
	                    {
	                        HibernateUtil.clearCachedDBObject(DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION, 
	                        								  user.getUUserName());
	                        Log.log.trace("Cleared cached DB object for user " + user.getUUserName() + " from " + 
	                        			  DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION.getValue());
	                    }
	                    
	                    //user.setUserGroupRels(null);//this is to avoid the cyclic ref 
	                    users.add(user.doGetVONoUserGroupRels());
	                    
	                    if (userObjInDBCacheWithUserGrpRels != null)
	                    {
	                        HibernateUtil.cacheDBObject(DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION, 
	                                                    new String[] { "com.resolve.persistence.model.Users", 
	                                                                   "com.resolve.persistence.model.UserRoleRel", 
	                                                                   "com.resolve.persistence.model.Roles", 
	                                                                   "com.resolve.persistence.model.UserGroupRel", 
	                                                                   "com.resolve.persistence.model.Groups", 
	                                                                   "com.resolve.persistence.model.GroupRoleRel", 
	                                                                   "com.resolve.persistence.model.UserPreferences", 
	                                                                   "com.resolve.persistence.model.Organization", 
	                                                                   "com.resolve.persistence.model.Orgs", 
	                                                                   "com.resolve.persistence.model.OrgGroupRel" }, 
	                                                    user.getUUserName(), 
	                                                    userObjInDBCacheWithUserGrpRels);
	                        Log.log.trace("Restored back cached DB object for user " + user.getUUserName() + " back into " + 
	                        			  DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION.getValue() + 
	                        			  " with updated UserGroupRels!!!");
	                    }
	                }
	            }
	            
	            vo.setGroupUsers(users);
	        }
        }

        //add orgs
        Collection<OrgGroupRel> orgGroupRels = Hibernate.isInitialized(this.orgGroupRels) ? getOrgGroupRels() : null;
        
        Log.log.trace("# of Org Group Relations for Group named [" + this.getUName() + "] is " + (orgGroupRels != null ? orgGroupRels.size() : "0") );
        
        if(orgGroupRels != null)
        {
            List<OrgsVO> orgs = new ArrayList<OrgsVO>();
            for(OrgGroupRel rel : orgGroupRels)
            {
                Orgs org = rel.getOrg();
                if (org != null)
                {
                    Log.log.trace("Getting Org Group Relations for Org named " + org.getUName() + " with id " + org.getSys_id() + 
                                  " without Org Group Relations and being added to orgs list...");
                    //org.setOrgGroupRels(null);//this is to avoid the cyclic ref 
                    OrgsVO orgsVO = org.doGetVONoOrgGroupRelations()/*doGetVO()*/;
                    orgs.add(orgsVO);
                    Log.log.trace("Added OrgsVO [" + orgsVO + "] to orgs list, count is " + orgs.size());
                }
            }
            
            vo.setGroupOrgs(orgs);
        }

        return vo;
    }

    @Override
    public void applyVOToModel(GroupsVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setUDescription(StringUtils.isNotBlank(vo.getUDescription()) && vo.getUDescription().equals(VO.STRING_DEFAULT) ? getUDescription() : vo.getUDescription());
            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUIsLinkToTeam(vo.getUIsLinkToTeam() != null ? vo.getUIsLinkToTeam() : getUIsLinkToTeam());
            this.setUHasExternalLink(vo.getUHasExternalLink() != null ? vo.getUHasExternalLink() : getUHasExternalLink());
        }
    }

}
