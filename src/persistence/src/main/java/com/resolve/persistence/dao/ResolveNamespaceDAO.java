package com.resolve.persistence.dao;

import com.resolve.persistence.model.ResolveNamespace;

public interface ResolveNamespaceDAO extends GenericDAO<ResolveNamespace, String> {}
