/******************************************************************************
* (C) Copyright 2014
import com.resolve.persistence.util.HibernateUtil
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import java.io.File;
import java.io.FilenameFilter;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Blob;
import java.sql.Clob;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.codehaus.jackson.map.ObjectMapper;
import net.sf.json.JSONObject;
import org.dom4j.Element;
import org.hibernate.CacheMode;
import org.hibernate.CallbackException;
import org.hibernate.ReplicationMode;
import org.hibernate.mapping.Column;
import org.hibernate.mapping.PersistentClass;
import org.hibernate.type.BlobType;
import org.hibernate.type.BooleanType;
import org.hibernate.type.ClobType;
import org.hibernate.type.CollectionType;
import org.hibernate.type.DateType;
import org.hibernate.type.DoubleType;
import org.hibernate.type.EntityType;
import org.hibernate.type.LongType;
import org.hibernate.type.MaterializedBlobType;
import org.hibernate.type.SerializableToBlobType;
import org.hibernate.type.TimestampType;
import org.hibernate.type.Type;
import org.hibernate.type.YesNoType;

import com.resolve.annotations.CData;
import com.resolve.services.interfaces.CdataProperty;
import com.resolve.util.FileUtils;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ExportUtil
{
	private static String defaultOutput = System.getProperty("user.dir");

	public static String EXPORT_CATEGORY = "export_category";
	public static String EXPORT_ENTITY = "export_entity";
	public static String EXPORT_PREVIOUS_STATE = "export_previous_state";
	public static String EXPORT_CURRENT_STATE = "export_current_state";
	public static String EXPORT_ID = "export_id";
	public static String EXPORT_PROPERTY_NAMES = "export_property_names";
	public static String EXPORT_TYPES = "export_types";

	public static String CATEGORY_INSERT = "insert";
	public static String CATEGORY_DELETE = "delete";
	public static String CATEGORY_UPDATE = "update";
	public static String CATEGORY_SELECT = "select";
	
	protected static Map<String, String> cDataTextMap = new HashMap<String, String>();
	protected static List<String> cDataColumnList = new ArrayList<String>();

    public static ThreadLocal<Boolean> isFromImport = new ThreadLocal<Boolean>()
    {
        @Override
        protected synchronized Boolean initialValue()
        {
            return Boolean.FALSE;
        }
    };

	public static String getDefaultOutputDirectory()
	{
		return defaultOutput;
	}

	public static void setDefaultOutputDirectory(String dir)
	{
		defaultOutput = dir;
	}

	static SimpleDateFormat SQLTimestampFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	static SimpleDateFormat SQLDateFormatter = new SimpleDateFormat("yyyy-MM-dd");

	private static UListener updateExportListener;

	public static synchronized void startExportAllUpdates(List<String> tableNames)
	{
		for (String tableName : tableNames)
		{
			startExportUpdate(tableName);
			startExportInsert(tableName);
			startExportDelete(tableName);
		}
	} // startExportAllUpdates

	public static synchronized void stopExportAllUpdates(List<String> tableNames)
	{
		for (String tableName : tableNames)
		{
			stopExportUpdate(tableName);
			stopExportInsert(tableName);
			stopExportDelete(tableName);
		}
	} // stopExportAllUpdates

	public static synchronized void startExportInsertUpdate(List<String> tableNames)
	{
		for (String tableName : tableNames)
		{
			startExportUpdate(tableName);
			startExportInsert(tableName);
		}
	} // startExportInsertUpdate

	public static synchronized void stopExportInsertUpdate(List<String> tableNames)
	{
		for (String tableName : tableNames)
		{
			stopExportUpdate(tableName);
			stopExportInsert(tableName);
		}
	} // stopExportInsertUpdate

	public static synchronized void startExportUpdate(final String tableName)
	{
		String className = HibernateUtil.table2Class(tableName);
		if (className != null)
		{
			if (updateExportListener == null)
			{
				updateExportListener = new UListener()
				{
					@Override
					public synchronized boolean onUpdate(Object entity, Serializable id, Object[] state, Object[] previousState, String[] propertyNames,
							Type[] types) throws CallbackException
					{
						ExportUtil.exportUpdate(entity, id, state, propertyNames, types, defaultOutput, false);
						return false;
					}
				};
			}

			Log.log.info("Adding export update listner: " + className);
			ResolveInterceptor.addAsyncUpdateListener(className, updateExportListener);
		}
	} // startExportUpdate

	public static synchronized void stopExportUpdate(final String tableName)
	{
		String className = HibernateUtil.table2Class(tableName);
		if (className != null)
		{
			if (updateExportListener != null)
			{
				Log.log.info("Removing export update listner: " + className);
				ResolveInterceptor.removeAsyncUpdateListener(className, updateExportListener);
			}
		}
	} // stopExportUpdate

	private static SIDListener insertExportListener;

	public static synchronized void startExportInsert(final String tableName)
	{
		String className = HibernateUtil.table2Class(tableName);
		if (className != null)
		{
			if (insertExportListener == null)
			{
				insertExportListener = new SIDListener()
				{
					@Override
					public synchronized boolean onDataOperation(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types)
							throws CallbackException
					{
						ExportUtil.exportUpdate(entity, id, state, propertyNames, types, defaultOutput, false);
						return false;
					}
				};
			}

			Log.log.info("Adding export insert listner: " + className);
			ResolveInterceptor.addAsyncInsertListener(className, insertExportListener);
		}
	} // startExportInsert

	public static synchronized void stopExportInsert(final String tableName)
	{
		String className = HibernateUtil.table2Class(tableName);
		if (className != null)
		{
			if (insertExportListener != null)
			{
				Log.log.info("Removing export insert listner: " + className);
				ResolveInterceptor.removeAsyncInsertListener(className, insertExportListener);
			}
		}
	} // stopExportInsert

	private static SIDListener deleteExportListener;

	public static synchronized void startExportDelete(final String tableName)
	{
		final String className = HibernateUtil.table2Class(tableName);
		if (className != null)
		{
			if (deleteExportListener == null)
			{
				deleteExportListener = new SIDListener()
				{
					@Override
					public synchronized boolean onDataOperation(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types)
							throws CallbackException
					{
						ExportUtil.exportDelete(className, entity, id, state, propertyNames, types, defaultOutput);
						return false;
					}
				};
			}

			Log.log.info("Adding export delete listner: " + className);
			ResolveInterceptor.addAsyncDeleteListener(className, deleteExportListener);
		}
	} // startExportDelete

	public static synchronized void stopExportDelete(final String tableName)
	{
		String className = HibernateUtil.table2Class(tableName);
		if (className != null)
		{
			if (deleteExportListener != null)
			{
				Log.log.info("Removing export delete listner: " + className);
				ResolveInterceptor.removeAsyncDeleteListener(className, deleteExportListener);
			}
		}
	} // stopExportDelete


    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static Object retrieveEntityProperty(Object entity, String propName) throws Exception
    {
        Class params[] = {};
        Object paramsObj[] = {};

        // get the Class
        Class thisClass = entity.getClass();
        // get the method
        Method thisMethod = null;

        params = new Class[] { Object.class };
        thisMethod = thisClass.getDeclaredMethod("get", params);
        paramsObj = new Object[] { propName };

        // call the method
        return (thisMethod.invoke(entity, paramsObj).toString());
    }

	@SuppressWarnings({ "rawtypes", "unchecked" })
    public static String retrievesys_id(Object entity) throws Exception
	{
		Class params[] = {};
		Object paramsObj[] = {};

		// get the Class
		Class thisClass = entity.getClass();
		// get the method
		Method thisMethod = null;
		try
		{
		    String sysId = BeanUtils.getProperty(entity, "sys_id");
	        return sysId;
		}
		catch (NoSuchMethodException nsm)
		{
		    //this must be an entity
		    params = new Class[] { Object.class };
            thisMethod = thisClass.getDeclaredMethod("get", params);
            paramsObj = new Object[] { "sys_id" };
            // call the method
          return (thisMethod.invoke(entity, paramsObj).toString());
		}
	}

	// //JDBC version of importUpdate
	// public static void importUpdate(String importFile, Connection conn)
	// throws Exception
	// {
	// File file = FileUtils.getFile(importFile);
	// XDoc doc = new XDoc(file);
	// doc.removeResolveNamespaces();
	// String table = doc.getStringValue("/record_update/@table");
	// String clsName = HibernateUtil.table2Class(table);
	// Class cls = Class.forName(clsName);
	// List columns = doc.getListValue("/record_update/" + table + "/*");
	// String[] colNames = new String[columns.size()];
	// String[] colValues = new String[columns.size()];
	//
	// int i = 0;
	// boolean isOracle = false;
	// String sys_id = null;
	//
	// for (Object o : columns)
	// {
	// Element e = (Element) o;
	// colNames[i] = e.getName();
	// colValues[i] = doc.getStringValue(e, "./", null);
	// if (colNames[i].equalsIgnoreCase("sys_id"))
	// {
	// sys_id = colValues[i];
	// }
	// }
	//
	// String colQuote = isOracle ? "\"" : "";
	// boolean isInsert = true;
	// String select = "select * from " + table + " where " + colQuote +
	// "sys_id" + colQuote + "='" + sys_id + "';";
	// Statement stat = conn.createStatement();
	// ResultSet rs = stat.executeQuery(select);
	//
	// if (rs.next())
	// {
	// isInsert = false;
	// }
	// rs.close();
	// stat.close();
	//
	// if (isInsert)
	// {
	// String insert = " insert into " + table + " (";
	// for (int j = 0; j < colNames.length; ++j)
	// {
	// if (j == colNames.length - 1)
	// {
	// insert += colQuote + colNames[j] + colQuote;
	// }
	// else
	// {
	// insert += colNames[j] + ",";
	// }
	// }
	//
	// insert += ") values (";
	//
	// for (int j = 0; j < colValues.length; ++j)
	// {
	// if (j == colValues.length - 1)
	// {
	// insert += colQuote + colNames[j] + colQuote;
	// }
	// else
	// {
	// insert += colNames[j] + ",";
	// }
	// }
	//
	// insert += ");";
	// }
	// else
	// {
	// String sql = "update " + table + " set ";
	//
	// for (int j = 0; j < colNames.length; ++j)
	// {
	// if (colNames[j].equalsIgnoreCase("sys_id"))
	// {
	// sys_id = colValues[j];
	// continue;
	// }
	// }
	// }
	// }

	public static void importDirectory(String dir) throws Exception
	{
		importDirectory(FileUtils.getFile(dir));
	}

	public static void importDirectory(File dir) throws Exception
	{
		FilenameFilter filter = new FilenameFilter()
		{

			@Override
			public boolean accept(File dir, String name)
			{
				if (name != null && name.endsWith(".xml"))
					return true;
				else
					return false;
			}
		};
		File[] files = dir.listFiles(filter);
		for (File f : files)
		{
			if (f.isFile())
			{
				try
				{
					importFile(f);
				}
				catch (Throwable e)
				{
					// ignore failures
					Log.log.error("SKIPPING file: " + f.getName(), e);
				}
			}
		}
	}

	@SuppressWarnings("rawtypes")
    public static void importFile(File importFile) throws Exception
	{
		Log.log.info("Start to process import file: " + importFile);
		//String importStr = utf8Conversion(importFile);
		//XDoc doc = new XDoc(importStr);
		String xml = FileUtils.readFileToString(importFile, "UTF-8");
		
		if (cDataColumnList.isEmpty())
		{
		    cDataColumnList.add("<u_script>");
		    cDataColumnList.add("<u_value>");
		    cDataColumnList.add("<u_sample>");
		}
		cDataTextMap.clear();
		
		for (String column : cDataColumnList)
        {
            // If column is <u_script>, closingElem will be </u_script>
            String closingElem = new StringBuilder(column).insert(1, "/").toString();
            
            String columnText = StringUtils.substringBetween(xml, column, closingElem);
            if (columnText != null)
            {
                if (columnText.contains("CDATA"))
                {
                    int startIndex = xml.indexOf(column);
                    int lastIndex = xml.lastIndexOf(closingElem);
                    columnText = xml.substring(startIndex+column.length(), lastIndex).trim();
                    StringBuilder sb = new StringBuilder(xml);
                    xml = sb.replace(startIndex+column.length(), lastIndex, "").toString();
                    cDataTextMap.put(column, columnText);
                    
                    //xml = StringUtils.remove(xml, scriptText);
                }
            }
        }
		
		XDoc doc = new XDoc(xml);
		doc.removeResolveNamespaces();
		String table = doc.getStringValue("/record_update/@table");
		List unload = doc.getNodes("/unload/*");
		if (!StringUtils.isEmpty(table))
		{
			Element tabElem = (Element) doc.getNode("/record_update/" + table);
			String action = tabElem.attributeValue("action");
			if ("DELETE".equals(action))
			{
				importDelete(importFile);
			}
			else if ("INSERT_OR_UPDATE".equals(action))
			{
				importUpdate(importFile, true);
			}
			else if ("INSERT".equals(action))
			{
				importUpdate(importFile, false);
			}
			else
			{
				throw new Exception("Unsupported import record action: " + action + ". Failed to import file " + importFile);
			}
		}
		else if (unload!=null && unload.size()>0)
		{
			for (Object o : unload)
			{
				Element e = (Element)o;
				importUnloadChild(e, importFile.toString());
			}
		}
		else
		{
			throw new Exception("Unsupported import file format. Failed to import file: " + importFile);
		}
	}

	public static void importUnloadChild(Element child, String importFile) throws Exception
	{
		String action = child.attributeValue("action");
		if ("DELETE".equals(action))
		{
			importUnloadDelete(child);
		}
		else if ("INSERT_OR_UPDATE".equals(action))
		{
			importUnloadUpdate(child, importFile);
		}
		else
		{
			throw new Exception("Unsupported unload import action: " + action + ". Failed to import unload record " + child);
		}
	}

	@SuppressWarnings("rawtypes")
    public static void importUnloadDelete(Element child) throws Exception
	{
		String table = child.getName();
		List columns = child.elements();
		HashMap<String, String> row = new HashMap<String, String>();

		String sys_id = null;
		for (Object o : columns)
		{
			Element e = (Element) o;
			String name = e.getName();
			String value = e.getText();
			row.put(name, value);
			if (name.equalsIgnoreCase("sys_id"))
			{
				sys_id = value;
			}
		}

		if (sys_id == null)
		{
			throw new Exception("No 'sys_id' column is found in record! Failed to process record: " + child);
		}

		String clsName = HibernateUtil.table2Class(table);
        
        try
        {
        	String sys_idFinal = sys_id;
        	
          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.executeNoCache(() -> {
        		Object target = null;
                target = HibernateUtil.getCurrentSession().get(clsName, sys_idFinal);
                if (target!=null)
                {
                    HibernateUtil.getCurrentSession().delete(target);
                    Log.log.info("Deleted record from db: entity=" + clsName + ", sys_id="+sys_idFinal);
                }
                else
                {
                    Log.log.info("Bypass process delete record: no instance is found in database for class = " + clsName + ": sys_id = " + sys_idFinal);
                }
                
        	});
        }
        catch (Exception ex)
        {
            Log.log.error(ex.getMessage(), ex);
            throw ex;
        }
	}

	@SuppressWarnings("rawtypes")
    public static void importUnloadUpdate(Element child, String importFile) throws Exception
	{
		String table = child.getName();
		List columns = child.elements();
		importTableColumns(table, columns, importFile, true);
	}

	@SuppressWarnings("rawtypes")
    public static void importDelete(File importFile) throws Exception
	{
		Log.log.info("Start to process delete record file: " + importFile);
		// XDoc doc = new XDoc(importFile);
		//XDoc doc = new XDoc(utf8Conversion(importFile));
		String xml = FileUtils.readFileToString(importFile, "UTF-8");
		XDoc doc = new XDoc(xml);
		doc.removeResolveNamespaces();
		String table = doc.getStringValue("/record_update/@table");
//		Element tabElem = (Element) doc.getNode("/record_update/" + table);
//		String action = tabElem.attributeValue("action");
		List columns = doc.getNodes("/record_update/" + table + "/*");
		HashMap<String, String> row = new HashMap<String, String>();

		String sys_id = null;
		for (Object o : columns)
		{
			Element e = (Element) o;
			String name = e.getName();
			String value = e.getText();
			row.put(name, value);
			if (name.equalsIgnoreCase("sys_id"))
			{
				sys_id = value;
			}
		}

		if (sys_id == null)
		{
			throw new Exception("No 'sys_id' column is found in record! Failed to process file: " + importFile);
		}


        String clsName = HibernateUtil.table2Class(table);

        
        try
        {
        	String sys_idFinal = sys_id;
        	
          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.executeNoCache(() -> {
        		
        		Object target = null;

                target = HibernateUtil.getCurrentSession().get(clsName, sys_idFinal);
                if (target!=null)
                {
                    HibernateUtil.getCurrentSession().delete(target);
                    Log.log.info("Deleted record from db: entity=" + clsName + ", sys_id="+sys_idFinal);
                }
                else
                {
                    Log.log.info("Bypass process delete record: no instance is found in database for class = " + clsName + ": sys_id = " + sys_idFinal);
                }
        	});
        }
        catch (Exception ex)
        {
            Log.log.error(ex.getMessage(), ex);
            throw ex;
        }

	}

	@SuppressWarnings("rawtypes")
    public static void importUpdate(File importFile, boolean overwrite) throws Exception
	{
		Log.log.info("Start to process an update record file: " + importFile);

		//XDoc doc = new XDoc(utf8Conversion(importFile));
		String xml = FileUtils.readFileToString(importFile, "UTF-8");
		
		for (String column : cDataColumnList)
		{
		    //if (!cDataTextMap.containsKey(column))
		    {
    		    // If column is <u_script>, closingElem will be </u_script>
    		    String closingElem = new StringBuilder(column).insert(1, "/").toString();
    		    
    		    String columnText = StringUtils.substringBetween(xml, column, closingElem);
    		    if (columnText != null)
    	        {
    	            if (columnText.contains("CDATA"))
    	            {
    	                int startIndex = xml.indexOf(column);
    	                int lastIndex = xml.lastIndexOf(closingElem);
    	                columnText = xml.substring(startIndex+column.length(), lastIndex).trim();
    	                StringBuilder sb = new StringBuilder(xml);
    	                xml = sb.replace(startIndex+column.length(), lastIndex, "").toString();
    	                cDataTextMap.put(column, columnText);
    	            }
    	        }
		    }
		}
		
		XDoc doc = new XDoc(xml);

		// XDoc doc = new XDoc(importFile);
		doc.removeResolveNamespaces();
		String table = doc.getStringValue("/record_update/@table");

		if (table == null)
		{
			throw new Exception("Unsupported update format. Failed to process file: " + importFile);
		}

		List columns = doc.getNodes("/record_update/" + table + "/*");
	    importTableColumns(table, columns, importFile.toString(), overwrite);
	}

	@SuppressWarnings({ "unused", "unchecked", "rawtypes", "finally" })
    private static void importTableColumns(String table, List columns, String importFile, boolean overwrite) throws Exception
	{
		if("resolve_cns_name".equalsIgnoreCase(table)) {
			Log.log.warn("Resolve table resolve_cns_name not longer present. Skipping import.");
			return;
		}
		
	    Log.log.debug("Inside ExportUtil.importTableColumns(String table, List columns, String importFile)");

		HashMap<String, String> row = new HashMap<String, String>();
		String sys_id = null;
		for (Object o : columns)
		{
			Element e = (Element) o;
			String name = e.getName().toUpperCase();
			String value = XDoc.getCDataValue(e);//e.getText();
			boolean cdataContent = false;
			
			if (cDataColumnList.contains("<" + name.toLowerCase() + ">"))
			{
			    String columnText = cDataTextMap.get("<" + name.toLowerCase() + ">");
			    if (columnText != null)
			    {
    			    if (columnText.startsWith("<![CDATA["))
                    {
    			    	cdataContent = true;
    			        columnText = columnText.substring(9, columnText.length() - 3).trim();
                    }
                    value = columnText;
			    }
			}
			
			// Only replace the '\r' replacement which export does explicitly. Leave everything as is.
			if (cdataContent)
			{
				value = value.replace("&#13;", "\r");
			}
			else
			{
				value = value.replace("&nbsp;", " ");
				value = StringUtils.unescapeHtml(value);
			}
			cdataContent = false;
			row.put(name, value);
			if (name.equalsIgnoreCase("sys_id"))
			{
				sys_id = value;
			}
		}

		String clsName = HibernateUtil.table2Class(table);
		boolean isEntity = false;
		if (HibernateUtil.getClassMapping(clsName) ==null || HibernateUtil.getClassMapping(clsName).getClassName()==null)
		{
		    isEntity = true;
		}

		Object target = null;

		if (!isEntity)
		{
		    target = Class.forName(clsName).newInstance();
		}
		else
		{
		    target = new HashMap<String, Object>();
		}

        if (!StringUtils.isEmpty(sys_id))
        {
            if (!isEntity)
            {
                PropertyUtils.setProperty(target, "sys_id", sys_id);
            }
            else
            {
                ((HashMap<String,Object>) target).put("sys_id", sys_id);
            }
        }
		PersistentClass ps = HibernateUtil.getClassMapping(clsName);
		
		if (ps == null)
		{
		    throw new Exception("Table: " + table + " does not exist in the system.");
		}

		Iterator it = ps.getPropertyIterator();
		while (it.hasNext())
		{
			org.hibernate.mapping.Property p = (org.hibernate.mapping.Property) it.next();
			String propertyName = p.getName();
			Type typeTest = p.getType();

			if(Log.log.isTraceEnabled())
			{
			    Log.log.trace("property name = " + propertyName + ", property class = " + p.getType().getName() + ", property type = "
					+ p.getType().getClass().getName());
			}

			Iterator cit = p.getColumnIterator();
			while (cit.hasNext())
			{
				Column c = (Column) cit.next();
				String colName = c.getName().toUpperCase();
				String colValue = row.get(colName);

				if (colValue == null)
					break;

				if (p.getType() instanceof CollectionType)
				{
					break;
				}
				else if (p.getType() instanceof EntityType)
				{
				    if (!isEntity)
				    {
				        EntityType enType = (EntityType) p.getType();
				        String enCls = enType.getName();
				        Object entity = Class.forName(enCls).newInstance();
				        PropertyUtils.setProperty(entity, "sys_id", colValue);
				        PropertyUtils.setProperty(target, propertyName, entity);
				    }
				    else
				    {
                        HashMap<String,Object> entity = new HashMap<String,Object>();
                        entity.put( "sys_id", colValue);
                        ((HashMap<String,Object>)target).put(propertyName, entity);

				    }
				}
				else if (p.getType() instanceof ClobType)
				{
				    if(StringUtils.isNotEmpty(colValue))
				    {
				        Clob clob = HibernateUtil.getClobFromString(colValue);
				        PropertyUtils.setProperty(target, propertyName, clob);
				    }
				}
				else if(p.getType() instanceof BlobType)
				{
                    if(StringUtils.isNotEmpty(colValue))
                    {
                        byte[] bytes = StringUtils.stringToBytes(colValue);
                        Blob b = HibernateUtil.getBlobFromBytes(bytes);
                        PropertyUtils.setProperty(target, propertyName, b);
                    }
				}
				else if ((p.getType() instanceof SerializableToBlobType ||
				                p.getType() instanceof MaterializedBlobType) && clsName.endsWith("Roles"))
				{
				    // this is very specific handling for Roles as we introduced a column of type JSONObject.
				    if (StringUtils.isNotBlank(colValue))
				    {
				    	byte[] permissionsByteArray;
				    	if(colValue.startsWith("["))
				    	{
				    		permissionsByteArray = StringUtils.stringToBytes(colValue);
				    	}
				    	else
				    	{
				    		JSONObject obj = new ObjectMapper().readValue(colValue, JSONObject.class);
				    		permissionsByteArray = SerializationUtils.serialize(obj);
				    	}
    				    PropertyUtils.setProperty(target, propertyName, permissionsByteArray);
				    }
				}
				else if(p.getType() instanceof MaterializedBlobType)
				{
				    if (StringUtils.isNotBlank(colValue))
				    {
				        try
				        {
				            byte[] imageByteArray = StringUtils.stringToBytes(colValue);
				            PropertyUtils.setProperty(target, propertyName, imageByteArray);
				        }
				        catch (Exception e)
				        {
				            Log.log.error("Failed to export database update for table: " + table + " and clumn: " + propertyName, e);
				        }
				    }
				}
				else if (p.getType() instanceof TimestampType)
				{
					if (!StringUtils.isEmpty(colValue))
					{
						Date d = null;
						if (propertyName.equals("sysUpdatedOn"))
						{
							d = GMTDate.getDate();
						}
						else
						{
							d = SQLTimestampFormatter.parse(colValue);
						}
						if (!isEntity) 
						{
						    PropertyUtils.setProperty(target, propertyName, d);
						}
						else
						{
			                ((HashMap<String,Object>) target).put(propertyName, d);
						}
					}
				}
				else if (p.getType() instanceof DateType)
				{
					if (!StringUtils.isEmpty(colValue))
					{
						Date d = null;
						if (propertyName.equals("sysUpdatedOn"))
						{
							d = GMTDate.getDate();
						}
						else
						{
							d = SQLDateFormatter.parse(colValue);
						}
						if (!isEntity)
						{
						    PropertyUtils.setProperty(target, propertyName, d);
						}
						else
						{
						    ((HashMap<String,Object>) target).put(propertyName, d);
						}
					}
				}
				else if((p.getType() instanceof YesNoType || p.getType() instanceof BooleanType) && StringUtils.isNotBlank(colValue)) {
				    Boolean boolValue = Boolean.valueOf(colValue);
                    if (!isEntity)
                    {
                        BeanUtils.setProperty(target, propertyName, boolValue);
                    }
                    else
                    {
                        ((HashMap<String,Object>) target).put(propertyName, boolValue);
                    }
				}
				else if (p.getType() instanceof LongType)
				{
				    if (colValue != null)
				    {
				        Long longValue = new Long(colValue);
				        PropertyUtils.setProperty(target, propertyName, longValue);
				    }
				}
				else if (p.getType() instanceof DoubleType)
                {
                    if (colValue != null)
                    {
                        Double doubleValue = new Double(colValue);
                        PropertyUtils.setProperty(target, propertyName, doubleValue);
                    }
                }
				else if (colValue != null)
				{
				    if (!isEntity)
				    {
				        BeanUtils.setProperty(target, propertyName, colValue);
				    }
				    else
				    {
                        ((HashMap<String,Object>) target).put(propertyName, colValue);
				    }
				}

				break;
			}
		}
		
		String sys_id_final = sys_id;
		Object targetFinal = target;

        try
        {
          HibernateProxy.setCurrentUser("system");
            HibernateProxy.executeNoCache(() -> {
            	if (sys_id_final==null)
                {
                    HibernateUtil.getCurrentSession().persist(clsName, targetFinal);
                }
                else
                {
                    if (overwrite)
                    {
                        HibernateUtil.getCurrentSession().replicate(clsName, targetFinal, ReplicationMode.OVERWRITE);
                    }
                    else
                    {
                        HibernateUtil.getCurrentSession().replicate(clsName, targetFinal, ReplicationMode.IGNORE);
                    }
                }
            });
        }
        catch (Throwable ex)
        {
                      HibernateUtil.rethrowNestedTransaction(ex);
            Log.log.error("Failed to process an update instance for class " + clsName + ": sys_id=" + sys_id, ex);
            
        }
		Log.log.info("Successfully processsed an update instance for class = " + clsName + ": sys_id = " + sys_id);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
    public static void exportUpdate(Object entity, Serializable id, Object[] currentState, String[] propertyNames, Type[] types, String outputDirectory, boolean insertOnly)
			throws CallbackException
	{
        String entityName = entity.getClass().getName();
        Blob blob = null;
        String fieldName = null;
        
        Log.log.debug("exportUpdate: Entity Name (" + entityName + ")");
        
        if(entityName.startsWith("java."))
        {
            entityName = ((HashMap<String,String>)entity).get("$type$");
        }
        
        Log.log.debug("exportUpdate: Entity [" + entityName + "]");
        
        boolean isCustomTable = CustomTableMappingUtil.modelExists(entityName) && !(HibernateUtil.modelExists(entityName));
        
        Log.log.debug("exportUpdate: Entity [" + entityName + "], isCustomTable = " + Boolean.toString(isCustomTable));
        
		if (isCustomTable)
		{
		    // Make sure, the custom table data is enclosed in CDATA
		    //isCustomTable = true;
			entityName=((HashMap<String,String>)entity).get("$type$");
		}

		String table = isCustomTable ? CustomTableMappingUtil.class2TableTrueCase(entityName) : HibernateUtil.class2TableTrueCase(entityName);
		
		if (StringUtils.isBlank(table))
        {
            String errMsg = "Failed to get " + (isCustomTable ? "custom " : "") + "table for entity name " + entityName;
            Log.log.error(errMsg);
            throw new CallbackException(errMsg);
        }
        
        Log.log.debug("Entity [" + entityName + "], isCustomTable = " + Boolean.toString(isCustomTable) + ", table = [" + table + "]");
        
		try
		{
			XDoc doc = new XDoc();
			Element root = doc.addRoot("record_update");
			doc.removeResolveNamespaces();

			doc.setStringValue(root, "./@table", table);
			if (table.equalsIgnoreCase("resolve_action_invoc_options"))
			{
			    doc.createPath(root, "./" + table);
			}
			Element update = doc.createPath(root, "./" + table);
			if (insertOnly)
			{
			    doc.setStringValue(update, "./@action", "INSERT");
			}
			else
			{
			    doc.setStringValue(update, "./@action", "INSERT_OR_UPDATE");
			}

			List<String> cdataProperty = getCDATAProperty(entity.getClass()) ;

			PersistentClass ps = HibernateUtil.getClassMapping(entityName);
			
			String[] sortedPropertyNames = Arrays.copyOf(propertyNames, propertyNames.length);
			Arrays.sort(sortedPropertyNames);
			List<String> propertyNamesList = Arrays.asList(propertyNames);
			
			for (int i = 0; i < sortedPropertyNames.length; ++i)
			{
				String propertyName = sortedPropertyNames[i];
				
				int propertyNameIndex = propertyNamesList.indexOf(propertyName);
				
				if (table.equalsIgnoreCase("resolve_impex_module") &&
				                propertyName.equalsIgnoreCase("UZipFileContent"))
				{
				    continue; // we don't want to export ZipFileContent. It will get populated during Import operation.
				}
				
				boolean addCdata = cdataProperty.contains(propertyName);
				org.hibernate.mapping.Property p = ps.getProperty(propertyName);

				Iterator cit = p.getColumnIterator();
				while (cit.hasNext())
				{
					Column c = (Column) cit.next();
					Object state = currentState[propertyNameIndex];
					if (types[propertyNameIndex] instanceof org.hibernate.type.TimestampType)
					{
						Date d = (Date) state;
						if (state != null)
						{
							// Dialect dialect =
							// Dialect.getDialect(HibernateUtil.getConfiguration().getProperties());
							// String objState =
							// ((TimestampType)types[i]).objectToSQLString(state,
							// dialect);
							state = SQLTimestampFormatter.format(d);
						}
					}
					else if (types[propertyNameIndex] instanceof org.hibernate.type.DateType)
					{
						Date d = (Date) state;
						if (state != null)
						{
							// Dialect dialect =
							// Dialect.getDialect(HibernateUtil.getConfiguration().getProperties());
							// String objState =
							// ((TimestampType)types[i]).objectToSQLString(state,
							// dialect);
							state = SQLDateFormatter.format(d);
						}
					}
                    else if (types[propertyNameIndex] instanceof BlobType)
                    {
                        if (state != null)
                        {
                            blob = (Blob) state;
                            state = null;
                            fieldName = propertyName;
                        }
                    }
                    else if (types[propertyNameIndex] instanceof ClobType)
                    {
                        if (state != null)
                        {
                            // convert clob to string
                            state = HibernateUtil.getStringFromClob(state);
                        }
                    }
                    else if (types[propertyNameIndex] instanceof MaterializedBlobType)
                    {
                        if (state != null)
                        {
                            state = StringUtils.byteToString((byte[]) state);
                        }
                    }
					else if (types[propertyNameIndex] instanceof EntityType)
					{
						if (state != null)
						{
							state = retrievesys_id(state);
						}
						else
						{
							break;
						}
					}
					else if (types[propertyNameIndex] instanceof CollectionType)
					{
						break;
					}

					if (state != null)
					{
					    if (state instanceof String)
                        {
                            String s = (String)state;
                            /*
                             * If file has windows newline chars, during import, DocumentHelper.parseText(String string)
                             * replaces '\r\n' with only '\n' making text to appear in a single line in AB definition parser editor.
                             * So, manually replacing '\r' with it's byte value, '&#13;' in the exported xml file preserves it from happening.
                             */
                            state = s.replace("\r", "&#13;");
                        }
					    if(addCdata || isCustomTable)
                        {
                            doc.setCDataValue(update, "./" + c.getName(), "" + state);//state = "<![CDATA[" + state + "]]>";
                        }
                        else
                        {
                            doc.setStringValue(update, "./" + c.getName(), "" + state);
                        }
					}
					Log.log.trace("exportUpdate: column=" + c.getName() + ", state=" + state);
					break;
				}
			}
			doc.setStringValue(update, "sys_id", "" + id);

			File file = null;
			if (!StringUtils.isEmpty(outputDirectory))
			{
				file = FileUtils.getFile(outputDirectory + File.separator + table + "_" + id + ".xml");
			}
			else
			{
				file = FileUtils.getFile(getDefaultOutputDirectory() + File.separator + table + "_" + id + ".xml");
			}
			doc.toFile(file);
			
			if (blob != null)
            {
                String entitySysId = doc.getStringValue("/record_update/" + entityName + "/sys_id");
                File fuFile = FileUtils.getFile(outputDirectory + "data/" + entityName + "," + entitySysId + "," + fieldName);
                byte[] content = blob.getBytes(1, (int)blob.length());
                FileUtils.writeByteArrayToFile(fuFile, content);
                
                blob = null;
            }
		}
		catch (Exception ex)
		{
			Log.log.error("Failed to export database update for table " + table, ex);
			throw new CallbackException(ex);
		}
	}

	@SuppressWarnings("rawtypes")
    public static void exportDelete(String entityName, Object entity, Serializable id, Object[] currentState, String[] propertyNames, Type[] types, String outputDirectory)
			throws CallbackException
	{
	    boolean isCustomModel = CustomTableMappingUtil.modelExists(entityName) && !(HibernateUtil.modelExists(entityName));
        
        String table = isCustomModel ? CustomTableMappingUtil.class2TableTrueCase(entityName) : HibernateUtil.class2TableTrueCase(entityName);
        
        Log.log.debug("exportDelete(" + entityName + ") : isCustomModel = " + Boolean.toString(isCustomModel) + ", table = [" + table + "]");
        
		try
		{
			XDoc doc = new XDoc();
			Element root = doc.addRoot("record_update");
			doc.removeResolveNamespaces();

			doc.setStringValue(root, "./@table", table);
			Element update = doc.createPath(root, "./" + table);
			doc.setStringValue(update, "./@action", "DELETE");

			PersistentClass ps = HibernateUtil.getClassMapping(entityName);
			
			String[] sortedPropertyNames = Arrays.copyOf(propertyNames, propertyNames.length);
            Arrays.sort(sortedPropertyNames);
            List<String> propertyNamesList = Arrays.asList(propertyNames);

			for (int i = 0; i < sortedPropertyNames.length; ++i)
			{
				String propertyName = propertyNames[i];
				int propertyNameIndex = propertyNamesList.indexOf(propertyName);
				
				org.hibernate.mapping.Property p = ps.getProperty(propertyName);
				Iterator cit = p.getColumnIterator();
				while (cit.hasNext())
				{
					Column c = (Column) cit.next();
					Object state = currentState[propertyNameIndex];
					if (types[propertyNameIndex] instanceof org.hibernate.type.TimestampType)
					{
						Date d = (Date) state;
						if (d != null)
						{
							state = SQLTimestampFormatter.format(d);
						}
					}
					else if (types[propertyNameIndex] instanceof org.hibernate.type.DateType)
					{
						Date d = (Date) state;
						if (state != null)
						{
							state = SQLDateFormatter.format(d);
						}
					}
					else if (types[propertyNameIndex] instanceof EntityType)
					{
						state = retrievesys_id(state);
					}
					else if (types[propertyNameIndex] instanceof CollectionType)
					{
						break;
					}

					if (state != null)
					{
						doc.setStringValue(update, "./" + c.getName(), "" + state);
					}

					Log.log.debug("exportDelete: column=" + c.getName() + ", state=" + state);
					break;
				}
			}
			doc.setStringValue(update, "sys_id", "" + id);

			File file = null;
			if (!StringUtils.isEmpty(outputDirectory))
			{
				file = FileUtils.getFile(outputDirectory + File.separator + table + "_" + id + ".xml");
			}
			else
			{
				file = FileUtils.getFile(getDefaultOutputDirectory() + File.separator + table + "_" + id + ".xml");
			}
			doc.toFile(file);
		}
		catch (Exception ex)
		{
			Log.log.error("Failed to export update for table " + table, ex);
			throw new CallbackException(ex);
		}
	}

//	public static void exportListenerRow(Map<String, Object> row)
//	{
//		exportListenerRow(row, null);
//	}
//
//	public static void exportListenerRow(Map<String, Object> row, String outputDirectory)
//	{
//		if (row == null || row.get(EXPORT_CATEGORY) == null)
//		{
//			throw new RuntimeException("Invalid export data row: " + row);
//		}
//		String category = (String) row.get(EXPORT_CATEGORY);
//
//		if (CATEGORY_INSERT.equals(category) || CATEGORY_UPDATE.equals(category) || CATEGORY_SELECT.equals(category))
//		{
//			exportUpdate(row.get(EXPORT_ENTITY), (Serializable) row.get(EXPORT_ID), (Object[]) row.get(EXPORT_CURRENT_STATE), (String[]) row
//					.get(EXPORT_PROPERTY_NAMES), (Type[]) row.get(EXPORT_TYPES), outputDirectory);
//		}
//		else
//		{
//			exportDelete(row.get(EXPORT_ENTITY), (Serializable) row.get(EXPORT_ID), (Object[]) row.get(EXPORT_CURRENT_STATE), (String[]) row
//					.get(EXPORT_PROPERTY_NAMES), (Type[]) row.get(EXPORT_TYPES), outputDirectory);
//		}
//	}

	public static void exportTableRow(String tableName, String sys_id) throws Exception
	{
		List<String> lst = new LinkedList<String>();
		lst.add(sys_id);
		exportTable(tableName, lst, null, false);
	}

	public static String exportTable(String tableName) throws Exception
	{
		return exportTable(tableName, null, null, false);
	}

	public static String exportTable(String tableName, String outputDirectory) throws Exception
	{
		return exportTable(tableName, null, outputDirectory, false);
	}

	public static String exportTable(String tableName, Collection<String> sys_ids) throws Exception
	{
		return exportTable(tableName, sys_ids, null, false);
	}

    @SuppressWarnings({ "unchecked", "finally" })
    public static String exportTable(String tableName, Collection<String> ids, final String outputDirectory, final boolean insertOnly) throws Exception
    {
        Log.log.debug("exportTable(" + tableName + ")");
        
        boolean isCustomTable = CustomTableMappingUtil.tableExists(tableName) && !(HibernateUtil.tableExists(tableName));
        
        String clsName = isCustomTable ? CustomTableMappingUtil.table2Class(tableName) : HibernateUtil.table2Class(tableName);        
        String tabName = isCustomTable ? CustomTableMappingUtil.class2TableTrueCase(clsName) : HibernateUtil.class2TableTrueCase(clsName);
        StringBuffer msgs = new StringBuffer();

        if (StringUtils.isBlank(clsName) || StringUtils.isBlank(tabName))
        {
            String errMsg = "Could not find class mapping for " + (isCustomTable ? "custom " : "") + "table named " + tableName;
            Log.log.error(errMsg);
            throw new Exception(errMsg);
        }
        
        Log.log.debug("exportTable(" + tableName + ") : isCustomTable = " + Boolean.toString(isCustomTable) + ", clsName = [" + clsName + "], tabName = [" + tabName + "]");

        if (ids == null || ids.isEmpty())
        {
            try
            {
              HibernateProxy.setCurrentUser("system");
            	ids  = (Collection<String>) HibernateProxy.executeNoCache(() -> {
                return HibernateUtil.getCurrentSession().createQuery("select r.id from " + clsName + " r where sys_id != :sys_id").
                      setParameter("sys_id", "id_sys").list();
            	});
            }
            catch (Throwable ex)
            {
               Log.log.error("Failed to export table:" + tableName, ex);
               throw ex;
            }
        }

        final Set<String> idSet = new ConcurrentSkipListSet<String>(ids);
        BlockingSIDListener selectExportListener;

        selectExportListener = new BlockingSIDListener()
        {
            @Override
            public int getOrder()
            {
                return -1000000;
            }

            @Override
            public synchronized boolean onDataOperation(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types)
                            throws CallbackException
            {
                if (idSet.contains(id))
                {
                    ExportUtil.exportUpdate(entity, id, state, propertyNames, types, outputDirectory, insertOnly);
                    idSet.remove(id);
                }
                return false;
            }
        };

        ResolveInterceptor.addSelectListener(isCustomTable, clsName, selectExportListener);
        try
        {
            for (String id : ids)
            {
                if (StringUtils.isNotEmpty(id))
                {
                    try
                    {
                      HibernateProxy.setCurrentUser("system");
                    	HibernateProxy.executeNoCache(() -> {
	                        //List list = HibernateUtil.getCurrentSession().createQuery("from " + clsName + " rrrrrr where rrrrrr.id = '" + id + "'").list();
	                        HibernateUtil.getCurrentSession().createQuery("from " + clsName + " rrrrrr where rrrrrr.id = :id").
	                        setParameter("id", id).list();
                    	});
                    }
                    catch (Throwable ex)
                {
                        Log.log.error("Failed to export row of table:" + tableName + ", sys_id: " + id, ex);
                                          HibernateUtil.rethrowNestedTransaction(ex);
                        msgs.append("Failed to export row of table:" + tableName + ", sys_id: " + id).append("\n");
                    }
                }
            }
        }
        finally
        {
            ResolveInterceptor.removeSelectListener(clsName, selectExportListener);
        }

         return msgs.toString();
    }

    @SuppressWarnings("unchecked")
    private static List<String> getCDATAProperty(Class<? extends Object> modelClass)
    {
        List<String> list = new ArrayList<String>();

        try
        {
            boolean hasCdataProperty = false;
            
            if (modelClass.newInstance() instanceof CdataProperty)
            {
                hasCdataProperty = true;
            }
            
            if(hasCdataProperty)
            {
                try
                {
                    // get the method
                    Method method = modelClass.getMethod("ugetCdataProperty");

                    // call the method
                    Object obj = method.invoke(modelClass.newInstance());

                    if(obj != null)
                    {
                        list = (List<String>) obj;
                    }
                }
                catch(Throwable t)
                {
                    Log.log.error("Error getting the cdata info:"+modelClass, t);
                }
            } else {
                List<String> fieldList = new ArrayList<>();
                List<Field> fields = FieldUtils.getFieldsListWithAnnotation(modelClass, CData.class);
                fields.stream().forEach(f -> fieldList.add(f.getName()));
                list.addAll(fieldList);
        }
        }
        catch(Throwable t)
        {
            //do nothing
        }

        return list;
    }//getCDATAProperty
} // ExportUtil
