/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.services.hibernate.vo.RemedyFormVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "remedy_form",  uniqueConstraints = {@UniqueConstraint(columnNames = {"u_name", "u_queue"})}) 
// @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class RemedyForm extends BaseModel<RemedyFormVO>
{
    private static final long serialVersionUID = 2301316570241327825L;
    
    private String UName; 
    private String UQueue;
    private String UFieldList;
   
    public RemedyForm()
    {
    }

    public RemedyForm(RemedyFormVO vo)
    {
        applyVOToModel(vo);
    }

    
    @Column(name = "u_queue", length = 100)
    public String getUQueue()
    {
        return this.UQueue;
    } // getUQueue

    public void setUQueue(String UQueue)
    {
        this.UQueue = UQueue;
    } // setUQueue
    
    @Column(name = "u_name", length = 100)
    public String getUName()
    {
        return UName;
    } // getUName

    public void setUName(String uName)
    {
        this.UName = uName;
    } // setUName

    @Column(name = "u_field_list")
    public String getUFieldList()
    {
        return UFieldList;
    } // getUFieldList

    public void setUFieldList(String uFieldList)
    {
        UFieldList = uFieldList;
    } // setUFieldList

    @Override
    public RemedyFormVO doGetVO()
    {
        RemedyFormVO vo = new RemedyFormVO();
        super.doGetBaseVO(vo);

        vo.setUName(getUName());
        vo.setUQueue(getUQueue());
        vo.setUFieldList(getUFieldList());
        
        return vo;
    }

    @Override
    public void applyVOToModel(RemedyFormVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo); 

            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUQueue(StringUtils.isNotBlank(vo.getUQueue()) && vo.getUQueue().equals(VO.STRING_DEFAULT) ? getUQueue() : vo.getUQueue());
            this.setUFieldList(StringUtils.isNotBlank(vo.getUFieldList()) && vo.getUFieldList().equals(VO.STRING_DEFAULT) ? getUFieldList() : vo.getUFieldList());

        }
        
    }
}

