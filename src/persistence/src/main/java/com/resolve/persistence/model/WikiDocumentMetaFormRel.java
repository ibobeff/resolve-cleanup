/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

// Generated Jan 15, 2010 12:12:58 PM by Hibernate Tools 3.2.4.GA

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;

import com.resolve.services.hibernate.vo.WikiDocumentMetaFormRelVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "wikidoc_metaform_rel", indexes = {
                @Index(columnList = "u_accessrights", name = "wdmf_u_accessrights_idx"),
                @Index(columnList = "u_wikidoc_sys_id", name = "wdmf_u_wikidoc_sys_id_idx"),
                @Index(columnList = "u_metaform_sys_id", name = "wdmf_u_metaform_sys_id_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class WikiDocumentMetaFormRel extends BaseModel<WikiDocumentMetaFormRelVO>
{
    private static final long serialVersionUID = -3727495928936057991L;

    public final static String RESOURCE_TYPE = "wikidoc_metaform_rel";

    private String UType;
    private String UName;//template name
    private String UWikiDocName; //added for convenince and to be able to search
    private String UFormName;//added for convenince and to be able to search
    private String UDescription;
    private Boolean UIsDefaultRole;
    
    // object references
    private AccessRights accessRights;
    private WikiDocument wikidoc;
    private MetaFormView form;

    // object referenced by

    public WikiDocumentMetaFormRel()
    {
    }

    public WikiDocumentMetaFormRel(WikiDocumentMetaFormRelVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_type", length = 40)
    public String getUType()
    {
        return UType;
    }

    public void setUType(String uType)
    {
        UType = uType;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_accessrights", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade({ CascadeType.DELETE })
    public AccessRights getAccessRights()
    {
        return accessRights;
    }

    public void setAccessRights(AccessRights accessRights)
    {
        this.accessRights = accessRights;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_wikidoc_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public WikiDocument getWikidoc()
    {
        return wikidoc;
    }

    public void setWikidoc(WikiDocument wikidoc)
    {
        this.wikidoc = wikidoc;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_metaform_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaFormView getForm()
    {
        return form;
    }

    public void setForm(MetaFormView form)
    {
        this.form = form;
    }

    @Column(name = "u_wikidoc_fullname", length = 300)
    public String getUWikiDocName()
    {
        return UWikiDocName;
    }

    public void setUWikiDocName(String uWikiDocName)
    {
        UWikiDocName = uWikiDocName;
    }

    @Column(name = "u_metaform_name", length = 300)
    public String getUFormName()
    {
        return UFormName;
    }

    public void setUFormName(String uFormName)
    {
        UFormName = uFormName;
    }

    @Lob
    @Column(name = "u_description", length = 16777215)
    public String getUDescription()
    {
        return UDescription;
    }

    public void setUDescription(String uDescription)
    {
        UDescription = uDescription;
    }

    @Column(name = "u_name", length = 300, unique = true)
    public String getUName()
    {
        return UName;
    }

    public void setUName(String uName)
    {
        UName = uName;
    }
    
    @Column(name = "u_is_default_role", length = 1)
    @Type(type = "yes_no")
    public Boolean getUIsDefaultRole()
    {
            return UIsDefaultRole;
    }
    
    public Boolean ugetUIsDefaultRole()
    {
        if (this.UIsDefaultRole == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UIsDefaultRole;
        }
    } // ugetUIsDefaultRole

    public void setUIsDefaultRole(Boolean isDefaultRole)
    {
        UIsDefaultRole = isDefaultRole;
    }
    
    @Override
    public WikiDocumentMetaFormRelVO doGetVO()
    {
        WikiDocumentMetaFormRelVO vo = new WikiDocumentMetaFormRelVO();
        super.doGetBaseVO(vo);

        vo.setUType(getUType());
        vo.setUName(getUName());
        vo.setUWikiDocName(getUWikiDocName());
        vo.setUFormName(getUFormName());
        vo.setUDescription(getUDescription());
        vo.setUIsDefaultRole(ugetUIsDefaultRole());
        
        vo.setAccessRights(Hibernate.isInitialized(this.accessRights) && getAccessRights() != null ? getAccessRights().doGetVO() : null) ;
        vo.setUFullname(getUWikiDocName());
        vo.setUSummary(getUDescription());
//        vo.setWikidoc(Hibernate.isInitialized(this.wikidoc) && getWikidoc() != null ? getWikidoc().doGetVO() : null);
//        vo.setForm(Hibernate.isInitialized(this.form) && getForm() != null ? getForm().doGetVO() : null);
        
        return vo;
    }

    @Override
    public void applyVOToModel(WikiDocumentMetaFormRelVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);  
            
            this.setUType(StringUtils.isNotBlank(vo.getUType()) && vo.getUType().equals(VO.STRING_DEFAULT) ? getUType() : vo.getUType());
            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUWikiDocName(StringUtils.isNotBlank(vo.getUWikiDocName()) && vo.getUWikiDocName().equals(VO.STRING_DEFAULT) ? getUWikiDocName() : vo.getUWikiDocName());
            this.setUFormName(StringUtils.isNotBlank(vo.getUFormName()) && vo.getUFormName().equals(VO.STRING_DEFAULT) ? getUFormName() : vo.getUFormName());
            this.setUDescription(StringUtils.isNotBlank(vo.getUDescription()) && vo.getUDescription().equals(VO.STRING_DEFAULT) ? getUDescription() : vo.getUDescription());
            this.setUIsDefaultRole(vo.getUIsDefaultRole() != null ? vo.getUIsDefaultRole() : getUIsDefaultRole());

            //should not update the wikidoc and form as they are only references
            if(vo.getAccessRights() != null)
            {
                AccessRights ar = this.getAccessRights();
                if(ar == null)
                {
                    ar = new AccessRights();
                }
                ar.applyVOToModel(vo.getAccessRights());
                
                this.setAccessRights(ar);
            }
        }

    }


}
