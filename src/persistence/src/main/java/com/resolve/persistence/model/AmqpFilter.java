/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.services.hibernate.vo.AmqpFilterVO;

/**
 * AmqpFilter
 */
@Entity
@Table(name = "amqp_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }, name = "amqpf_u_name_u_queue_uk") } )
public class AmqpFilter extends GatewayFilter<AmqpFilterVO>
{
    private static final long serialVersionUID = -5569116184953825905L;
    private String UTargetQueue;
    private Boolean UExchange;

    public AmqpFilter()
    {
    } // AmqpFilter

    public AmqpFilter(AmqpFilterVO vo)
    {
        applyVOToModel(vo);
    } // AmqpFilter


    @Column(name = "u_exchange")
    public Boolean getUExchange()
    {
        return this.UExchange;
    } // getUExchange

    public void setUExchange(Boolean uExchange)
    {
        this.UExchange = uExchange;
    } // setUExchange

    @Column(name = "target_queue")
    public String getUTargetQueue()
    {
        return UTargetQueue;
    }

    public void setUTargetQueue(String uTargetQueue)
    {
        UTargetQueue = uTargetQueue;
    }

    @Override
    public AmqpFilterVO doGetVO()
    {
        AmqpFilterVO vo = new AmqpFilterVO();
        super.doGetBaseVO(vo);

        vo.setUTargetQueue(getUTargetQueue());
        vo.setUExchange(getUExchange());

        return vo;
    }

    @Override
    public void applyVOToModel(AmqpFilterVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setUTargetQueue(vo.getUTargetQueue() != null ? vo.getUTargetQueue() : getUTargetQueue());
            this.setUExchange(vo.getUExchange() != null ? vo.getUExchange() : getUExchange());
        }
    }
}
