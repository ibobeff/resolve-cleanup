/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "archive_worksheet_debug", indexes = {@Index(columnList = "sys_created_on", name = "awd_sys_created_on_idx")})
@DynamicUpdate(value = true)
// @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)

public class ArchiveWorksheetDebug implements java.io.Serializable
{
    private static final long serialVersionUID = -4243535785013839290L;

    private String sys_id;

    private String debug;
    private Long timestamp;

    // sys fields
    private String sysUpdatedBy;
    private Date sysUpdatedOn;
    private String sysCreatedBy;
    private Date sysCreatedOn;
    private Integer sysModCount;
	
	// transient
	
    // object references
    //private Worksheet worksheet;                      // UProblem;
	
    // object referenced by
	
    
    @Id
    @Column(name = "sys_id",  nullable = false, length = 32)
    @GeneratedValue(generator = "hibernate-uuid")
    public String getsys_id()
    {
        return this.sys_id;
    }

    public void setsys_id(String sys_id)
    {
        this.sys_id = sys_id;
    }

    @Column(name = "sys_updated_by", length = 40)
    public String getSysUpdatedBy()
    {
        return this.sysUpdatedBy;
    }

    public void setSysUpdatedBy(String sysUpdatedBy)
    {
        this.sysUpdatedBy = sysUpdatedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sys_updated_on", length = 19)
    public Date getSysUpdatedOn()
    {
        return this.sysUpdatedOn;
    }

    public void setSysUpdatedOn(Date sysUpdatedOn)
    {
        this.sysUpdatedOn = sysUpdatedOn;
    }

    @Column(name = "sys_created_by", length = 40)
    public String getSysCreatedBy()
    {
        return this.sysCreatedBy;
    }

    public void setSysCreatedBy(String sysCreatedBy)
    {
        this.sysCreatedBy = sysCreatedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sys_created_on", length = 19)
    public Date getSysCreatedOn()
    {
        return this.sysCreatedOn;
    }

    public void setSysCreatedOn(Date sysCreatedOn)
    {
        this.sysCreatedOn = sysCreatedOn;
    }

    @Column(name = "sys_mod_count")
    public Integer getSysModCount()
    {
        return this.sysModCount;
    }

    public void setSysModCount(Integer sysModCount)
    {
        this.sysModCount = sysModCount;
    }

    /*
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_worksheet")
    @Index(name = "awd_u_worksheet_idx")
    @NotFound(action = NotFoundAction.IGNORE)
    public Worksheet getUWorksheet()
    {
        return worksheet;
    }

    public void setUWorksheet(Worksheet problem)
    {
        this.worksheet = problem;
    }
     */
    
    @Lob
    @Column(name = "u_debug", length = 16777215)
    public String getUDebug()
    {
        return this.debug;
    }

    public void setUDebug(String debug)
    {
        this.debug = debug;
    }
    
    @Column(name = "u_timestamp")
    public Long getUTimestamp()
    {
        return timestamp;
    }

    public void setUTimestamp(Long UTimestamp)
    {
        timestamp = UTimestamp;
    }
    
}
