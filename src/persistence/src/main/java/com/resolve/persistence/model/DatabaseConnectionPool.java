/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

//Generated Dec 11, 2009 11:15:20 AM by Hibernate Tools 3.2.4.GA

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.services.hibernate.vo.DatabaseConnectionPoolVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * DatabaseConnectionPool generated by hbm2java
 */
/**
 * @author alokika.dash
 * 
 */
@Entity
@Table(name = "database_connectionpool",  uniqueConstraints = {@UniqueConstraint(columnNames = {"u_name", "u_queue"}, name = "dbcpool_u_name_u_queue_uk")}) 
// @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class DatabaseConnectionPool extends BaseModel<DatabaseConnectionPoolVO>
{
    private static final long serialVersionUID = 2808226676382975792L;
    
    private String UQueue;
    private String UPoolName; 
    private String UDriverClass;
    private String UDBUsername;
    private String UDBPassword;
    private String UDBConnectURI;
    private Integer UDBMaxConn;
   
    public DatabaseConnectionPool()
    {
    } // DatabaseConnectionPool
    
    public DatabaseConnectionPool(DatabaseConnectionPoolVO vo)
    {
        applyVOToModel(vo);
    } // DatabaseConnectionPool

    @Column(name = "u_queue", length = 100)
    public String getUQueue()
    {
        return this.UQueue;
    } // getUQueue

    public void setUQueue(String UQueue)
    {
        this.UQueue = UQueue;
    } // setUQueue
    
    @Column(name = "u_name", length = 40)
    public String getUPoolName()
    {
        return UPoolName;
    } // getUPoolName

    public void setUPoolName(String uPoolName)
    {
        UPoolName = uPoolName;
    } // setUPoolName

    @Column(name = "u_driverclass")
    public String getUDriverClass()
    {
        return UDriverClass;
    } // getUDriverClass

    public void setUDriverClass(String uDriverClass)
    {
        UDriverClass = uDriverClass;
    } // setUDriverClass

    @Column(name = "u_dbusername", nullable = false, length = 40)
    public String getUDBUsername()
    {
        return UDBUsername;
    } // getUDBUsername

    public void setUDBUsername(String uDBUsername)
    {
        UDBUsername = uDBUsername;
    } // setUDBUsername

    @Column(name = "u_dbconnecturi")
    public String getUDBConnectURI()
    {
        return UDBConnectURI;
    } // getUDBConnectURI

    public void setUDBConnectURI(String uDBConnectURI)
    {
        UDBConnectURI = uDBConnectURI;
    } // setUDBConnectURI

    @Column(name = "u_dbmaxconnect")
    public Integer getUDBMaxConn()
    {
        return UDBMaxConn;
    } // getUDBMaxConn

    public void setUDBMaxConn(Integer uDBMaxConn)
    {
        UDBMaxConn = uDBMaxConn;
    } // setUDBMaxConn

    @Column(name = "u_dbpassword", nullable = false, length = 40)
    public String getUDBPassword()
    {
        return this.UDBPassword;
    } // getUDBPassword

    public void setUDBPassword(String UDBPassword)
    {
        this.UDBPassword = UDBPassword;
    } // setUDBPassword
    
    public void usetPassword(String password)
    {
        if (!StringUtils.isEmpty(password) && password.charAt(password.length()-1) != '=')
        {
            try
            {
                this.UDBPassword = CryptUtils.encrypt(password);
            }
            catch (Exception e)
            {
                Log.log.error("Failed to update Database Connector Password", e);
            }
        }
    } // usetPassord   

    @Override
    public DatabaseConnectionPoolVO doGetVO()
    {
        DatabaseConnectionPoolVO vo = new DatabaseConnectionPoolVO();
        super.doGetBaseVO(vo);
        
        vo.setUDBConnectURI(getUDBConnectURI());
        vo.setUDBMaxConn(getUDBMaxConn());
        vo.setU_db_pas_sword(getUDBPassword());
        vo.setU_db_us_ern_ame(getUDBUsername());
        vo.setUDriverClass(getUDriverClass());
        vo.setUPoolName(getUPoolName());
        vo.setUQueue(getUQueue());
        
        return vo;
    }
    
    @Override
    public DatabaseConnectionPoolVO doGetVO(boolean secured)
    {
        DatabaseConnectionPoolVO vo = new DatabaseConnectionPoolVO(secured);
        super.doGetBaseVO(vo);
        
        vo.setUDBConnectURI(getUDBConnectURI());
        vo.setUDBMaxConn(getUDBMaxConn());
        vo.setU_db_us_ern_ame(getUDBUsername());
        vo.setUDriverClass(getUDriverClass());
        vo.setUPoolName(getUPoolName());
        vo.setUQueue(getUQueue());
        
        return vo;
    }
    
    @Override
    public DatabaseConnectionPoolVO secureGetVO()
    {
        DatabaseConnectionPoolVO vo = this.doGetVO(true);
        return vo;
    }

    @Override
    public void applyVOToModel(DatabaseConnectionPoolVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo); 
            boolean passwordUnchanged = vo.getU_db_pas_sword().equals(VO.STARS);
            boolean nullPassword = StringUtils.isNotBlank(vo.getU_db_pas_sword()) && vo.getU_db_pas_sword().equals(VO.STRING_DEFAULT);
            this.setUDBConnectURI(StringUtils.isNotBlank(vo.getUDBConnectURI()) && vo.getUDBConnectURI().equals(VO.STRING_DEFAULT) ? getUDBConnectURI() : vo.getUDBConnectURI());
            this.setUDBMaxConn(vo.getUDBMaxConn() != null && vo.getUDBMaxConn().equals(VO.INTEGER_DEFAULT) ? getUDBMaxConn() : vo.getUDBMaxConn());
            this.setUDBPassword(passwordUnchanged || nullPassword ? getUDBPassword() : vo.getU_db_pas_sword());
            this.setUDBUsername(StringUtils.isNotBlank(vo.getU_db_us_ern_ame()) && vo.getU_db_us_ern_ame().equals(VO.STRING_DEFAULT) ? getUDBUsername() : vo.getU_db_us_ern_ame());
            this.setUDriverClass(StringUtils.isNotBlank(vo.getUDriverClass()) && vo.getUDriverClass().equals(VO.STRING_DEFAULT) ? getUDriverClass() : vo.getUDriverClass());
            this.setUPoolName(StringUtils.isNotBlank(vo.getUPoolName()) && vo.getUPoolName().equals(VO.STRING_DEFAULT) ? getUPoolName() : vo.getUPoolName());
            this.setUQueue(StringUtils.isNotBlank(vo.getUQueue()) && vo.getUQueue().equals(VO.STRING_DEFAULT) ? getUQueue() : vo.getUQueue());
        }
        
    }
}

