/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.RBConnectionParamVO;
import com.resolve.services.hibernate.vo.RBConnectionVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
@Entity
@Table(name = "rb_connection")
public class RBConnection extends BaseModel<RBConnectionVO>
{
    private String name;
    private String type; //SSH, Telnet
    private String parentId; //Loop Id
    private String parentType; //LOOP or null(for the root builder)
    private Double order;

    private Collection<RBConnectionParam> params;
    
    public RBConnection()
    {
    }

    public RBConnection(RBConnectionVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_name", length = 200)
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Column(name = "u_type", length = 40)
    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    @Column(name = "u_parent_id", length = 32)
    public String getParentId()
    {
        return parentId;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }

    @Column(name = "u_parent_type", length = 40)
    public String getParentType()
    {
        return parentType;
    }

    public void setParentType(String parentType)
    {
        this.parentType = parentType;
    }

    @Column(name = "u_order", precision=10, scale=2)
    public Double getOrder()
    {
        return order;
    }

    public void setOrder(Double order)
    {
        this.order = order;
    }

    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "connection", fetch = FetchType.EAGER)
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.ALL)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public Collection<RBConnectionParam> getParams()
    {
        return params;
    }

    public void setParams(Collection<RBConnectionParam> params)
    {
        this.params = params;
    }

    @Override
    public RBConnectionVO doGetVO()
    {
        RBConnectionVO vo = new RBConnectionVO();
        super.doGetBaseVO(vo);

        vo.setName(getName());
        vo.setType(getType());
        vo.setParentId(getParentId());
        vo.setParentType(getParentType());
        vo.setOrder(getOrder());
        
        //params
        Collection<RBConnectionParam> params = getParams();
        if(params != null)
        {
            List<RBConnectionParamVO> paramVOs = new ArrayList<RBConnectionParamVO>();
            for(RBConnectionParam param : params)
            {
                if (param != null)
                {
                    paramVOs.add(param.doGetVO());
                }
            }
            
            vo.setParams(paramVOs);
        }

        return vo;
    }

    @Override
    public void applyVOToModel(RBConnectionVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setName(StringUtils.isNotBlank(vo.getName()) && vo.getName().equals(VO.STRING_DEFAULT) ? getName() : vo.getName());
            this.setType(StringUtils.isNotBlank(vo.getType()) && vo.getType().equals(VO.STRING_DEFAULT) ? getType() : vo.getType());
            this.setParentId(StringUtils.isNotBlank(vo.getParentId()) && vo.getParentId().equals(VO.STRING_DEFAULT) ? getParentId() : vo.getParentId());
            this.setParentType(StringUtils.isNotBlank(vo.getParentType()) && vo.getParentType().equals(VO.STRING_DEFAULT) ? getParentType() : vo.getParentType());
            this.setOrder(vo.getOrder() != null && vo.getOrder().equals(VO.DOUBLE_DEFAULT) ? getOrder() : vo.getOrder());
            
            Collection<RBConnectionParamVO> paramVOs = vo.getParams();
            if (paramVOs != null && paramVOs.size() > 0)
            {
                List<RBConnectionParam> params = new ArrayList<RBConnectionParam>();
                for (RBConnectionParamVO paramVO : paramVOs)
                {
                    RBConnectionParam param = new RBConnectionParam(paramVO);
                    param.setConnection(this);
                    params.add(param);
                }
                this.setParams(params);
            }
        }
    }
}
