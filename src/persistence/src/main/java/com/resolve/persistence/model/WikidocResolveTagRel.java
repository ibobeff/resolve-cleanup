/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.ConstraintMode;

// Generated Feb 19, 2010 10:57:42 AM by Hibernate Tools 3.2.4.GA

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.vo.WikidocResolveTagRelVO;

/**
 * WikidocResolveTagRel generated by hbm2java
 */
@Entity
@Table(name = "wikidoc_tag_rel", indexes = {
                @Index(columnList = "u_wikidoc_sys_id", name = "u_wikidoc_sys_id_idx"),
                @Index(columnList = "u_tag_sys_id", name = "u_tag_sys_id_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class WikidocResolveTagRel extends BaseModel<WikidocResolveTagRelVO>
{
    private static final long serialVersionUID = 8984427952765345158L;
    
    // object references
    private WikiDocument wikidoc;
    private ResolveTag tag;
	
    public WikidocResolveTagRel()
    {
    }

    public WikidocResolveTagRel(WikidocResolveTagRelVO vo)
    {
        applyVOToModel(vo);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_wikidoc_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public WikiDocument getWikidoc()
    {
        return wikidoc;
    }

    public void setWikidoc(WikiDocument wikidoc)
    {
        this.wikidoc = wikidoc;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_tag_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveTag getTag()
    {
        return tag;
    }

    public void setTag(ResolveTag tag)
    {
        this.tag = tag;
    }

    @Override
    public WikidocResolveTagRelVO doGetVO()
    {
        WikidocResolveTagRelVO vo = new WikidocResolveTagRelVO();
        super.doGetBaseVO(vo);
        
        //only get the required fields to avoid recursiveness and excess data
        if(Hibernate.isInitialized(this.tag) && this.getTag() != null)
        {
            ResolveTagVO tagVO = new ResolveTagVO();
            tagVO.setSys_id(this.getTag().getSys_id());
            tagVO.setUName(this.getTag().getUName());
            
            vo.setTag(tagVO);
        }
        
        if(Hibernate.isInitialized(this.wikidoc) && this.getWikidoc() != null)
        {
            vo.setWikidoc(new WikiDocumentVO(this.wikidoc.getSys_id()));
        }
        
        return vo;
    }


    @Override
    public void applyVOToModel(WikidocResolveTagRelVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);
            
            //references
            this.setTag(vo.getTag() != null ? new ResolveTag(vo.getTag().getSys_id()) : null);
            this.setWikidoc(vo.getWikidoc() != null ? new WikiDocument(vo.getWikidoc().getSys_id()) : null);
        }
        
    }

}
