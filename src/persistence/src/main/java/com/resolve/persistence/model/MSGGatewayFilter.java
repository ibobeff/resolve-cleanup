package com.resolve.persistence.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.resolve.services.interfaces.VO;
import com.resolve.services.hibernate.vo.MSGGatewayFilterVO;

@Entity
@Table(name = "msg_gateway_filter", 
	   uniqueConstraints = { 
			   			       @UniqueConstraint(columnNames = { "u_name", "u_queue" }, name = "msggf_u_name_u_queue_uk") 
			   			   },
	   indexes = {
			         @Index(columnList = "u_gateway_name", name = "msggf_gtw_queue_idx"),
			   	     @Index(columnList = "u_queue", name = "msggf_gtw_queue_idx"),
			   	     @Index(columnList = "u_name", name = "msggf_filter_gtw_queue_idx"),
			   	     @Index(columnList = "u_gateway_name", name = "msggf_filter_gtw_queue_idx"),
			   	     @Index(columnList = "u_queue", name = "msggf_filter_gtw_queue_idx")
			     })
public class MSGGatewayFilter extends GatewayFilter<MSGGatewayFilterVO> {

    private static final long serialVersionUID = 1L;

    public MSGGatewayFilter() {
    }

    public MSGGatewayFilter(MSGGatewayFilterVO vo) {
        applyVOToModel(vo);
    }

    private String UGatewayName;
    
    private Boolean UDeployed;
    
    private Boolean UUpdated;
    
    private String UUsername;

    private String UPassword;

    private String USsl;
    
    @Column(name = "u_gateway_name", length = 40)
    public String getUGatewayName()
    {
        return UGatewayName;
    }

    public void setUGatewayName(String uGatewayName)
    {
        UGatewayName = uGatewayName;
    }
    
    @Column(name = "u_deployed")
    public Boolean getUDeployed()
    {
        return UDeployed;
    }

    public void setUDeployed(Boolean uDeployed)
    {
        UDeployed = uDeployed;
    }

    @Column(name = "u_updated")
    public Boolean getUUpdated()
    {
        return UUpdated;
    }

    public void setUUpdated(Boolean uUpdated)
    {
        UUpdated = uUpdated;
    }
    
    @Column(name = "u_user", length = 40)
    public String getUUsername() {
        return this.UUsername;
    }

    public void setUUsername(String uUsername) {
        this.UUsername = uUsername;
    }
    
    @Column(name = "u_pass", length = 40)
    public String getUPassword() {
        return this.UPassword;
    }

    public void setUPassword(String uPassword) {
        this.UPassword = uPassword;
    }

    @Column(name = "u_ssl", nullable=true)
    public String getUSsl() {
        return this.USsl;
    }

    public void setUSsl(String uSsl) {
        this.USsl = uSsl;
    }

    public MSGGatewayFilterVO doGetVO() {
        
        MSGGatewayFilterVO vo = new MSGGatewayFilterVO();
        
        super.doGetBaseVO(vo);
        
        vo.setUUsername(getUUsername());
        vo.setUPassword(getUPassword());
        vo.setUSsl(getUSsl());
        vo.setUGatewayName(getUGatewayName());
        
        return vo;
    }

    @Override
    public void applyVOToModel(MSGGatewayFilterVO vo) {
        
        if (vo == null) return;
        
        super.applyVOToModel(vo);
        
        if (!VO.STRING_DEFAULT.equals(vo.getUUsername())) this.setUUsername(vo.getUUsername()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUPassword())) this.setUPassword(vo.getUPassword()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUSsl())) this.setUSsl(vo.getUSsl()); else ;
    }

    @Override
    public List<String> ugetCdataProperty() {
        
        List<String> list = super.ugetCdataProperty();
        
        list.add("UUser");
        list.add("UPass");
        list.add("USsl");
        
        return list;
    }
    
}