/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.services.hibernate.vo.RemedyFilterVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "remedy_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
// @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class RemedyFilter extends GatewayFilter<RemedyFilterVO>
{
    private static final long serialVersionUID = 685433036390570108L;

    private String UFormName;
    private String UQuery;

    // transient

    // object references

    // object referenced by
    public RemedyFilter()
    {
    } // TSRMFilter

    public RemedyFilter(RemedyFilterVO vo)
    {
        applyVOToModel(vo);
    } // TSRMFilter

    @Column(name = "u_form_name", length = 100)
    public String getUFormName()
    {
        return UFormName;
    }

    public void setUFormName(String uFormName)
    {
        UFormName = uFormName;
    }

    @Column(name = "u_query", length = 4000)
    public String getUQuery()
    {
        return this.UQuery;
    } // getUQuery

    public void setUQuery(String uQuery)
    {
        this.UQuery = uQuery;
    } // setUQuery

    @Override
    public RemedyFilterVO doGetVO()
    {
        RemedyFilterVO vo = new RemedyFilterVO();
        super.doGetBaseVO(vo);

        vo.setUFormName(getUFormName());
        vo.setUQuery(getUQuery());

        return vo;
    }

    @Override
    public void applyVOToModel(RemedyFilterVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setUFormName(StringUtils.isNotBlank(vo.getUFormName()) && vo.getUFormName().equals(VO.STRING_DEFAULT) ? getUFormName() : vo.getUFormName());
            this.setUQuery(StringUtils.isNotBlank(vo.getUQuery()) && vo.getUQuery().equals(VO.STRING_DEFAULT) ? getUQuery() : vo.getUQuery());
        }
    }
}
