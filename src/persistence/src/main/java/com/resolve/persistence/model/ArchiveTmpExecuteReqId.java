/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;


@Entity
@Table(name = "archive_tmp_execute_req_id", indexes = {@Index(columnList = "u_problem", name = "ater_u_problem")})
// @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ArchiveTmpExecuteReqId implements java.io.Serializable
{
    private static final long serialVersionUID = 8805530448806052037L;
    private String sys_id;
    private String UProblem;

    public ArchiveTmpExecuteReqId()
    {
    }

    @Id
    @Column(name = "sys_id", nullable = false, length = 32)
    public String getsys_id()
    {
        return this.sys_id;
    }

    public void setsys_id(String sys_id)
    {
        this.sys_id = sys_id;
    }
    
    public ArchiveTmpExecuteReqId usetsys_id(String sys_id)
    {
        this.sys_id = sys_id;
        return this;
    }

    @Column(name = "u_problem", length = 32)
    public String getUProblem()
    {
        return this.UProblem;
    }

    public void setUProblem(String UProblem)
    {
        this.UProblem = UProblem;
    }
} // ArchiveTmpExecuteReqId
