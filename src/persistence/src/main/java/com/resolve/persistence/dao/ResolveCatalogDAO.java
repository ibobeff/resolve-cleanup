package com.resolve.persistence.dao;

import com.resolve.persistence.model.ResolveCatalog;

public interface ResolveCatalogDAO extends GenericDAO<ResolveCatalog, String>
{

}
