/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import java.io.Serializable;

import org.hibernate.CallbackException;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.Type;

import com.resolve.util.GMTDate;

public class SystemInterceptor extends EmptyInterceptor
{
    private static final long serialVersionUID = 4522206643998612336L;
    private String RESOLVE = "ResolveInteceptor";

	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) throws CallbackException

	{
		//Log.log.debug("onSave() on entity " + entity.getClass().getName());
		boolean changed = false;
		for (int i = 0; i < state.length; ++i)
		{
			// Log.log.debug(propertyNames[i] + "=" + state[i]);
			if (state[i] != null)
				continue;
			if ("sysUpdatedBy".equals(propertyNames[i]))
			{
				state[i] = RESOLVE;
				changed = true;
			}
			else if ("sysUpdatedOn".equals(propertyNames[i]))
			{
				state[i] = GMTDate.getDate();//new Date();
				changed = true;
			}
			else if ("sysCreatedBy".equals(propertyNames[i]))
			{
				state[i] = RESOLVE;
				changed = true;
			}
			else if ("sysCreatedOn".equals(propertyNames[i]))
			{
				state[i] = GMTDate.getDate(); //new Date();
				changed = true;
			}
		}
		return changed;
	}

	public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState, String[] propertyNames, Type[] types)
			throws CallbackException
	{
		//Log.log.debug("onFlushDirty() on entity " + entity.getClass().getName());

		boolean changed = false;
		for (int i = 0; i < currentState.length; ++i)
		{
			// Log.log.debug(propertyNames[i] + "=" + currentState[i]);
			if ("sysUpdatedBy".equals(propertyNames[i]))
			{
				currentState[i] = RESOLVE;
				changed = true;
			}
			else if ("sysUpdatedOn".equals(propertyNames[i]))
			{
				currentState[i] = GMTDate.getDate(); //new Date();
				changed = true;
			}
		}
		return changed;

	}

	public boolean onLoad(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) throws CallbackException
	{
		//Log.log.debug("onLoad() on entity " + entity.getClass().getName());
		boolean changed = false;
		for (int i = 0; i < state.length; ++i)
		{
			// Log.log.debug(propertyNames[i] + "=" + state[i]);
			if (state[i] != null)
				continue;
			if (types[i] instanceof IntegerType)
			{
				state[i] = new Integer(0);
				changed = true;
			}
			else if (types[i] instanceof LongType)
			{
				state[i] = new Long(0);
				changed = true;
			}
		}
		return changed;
	}
}




