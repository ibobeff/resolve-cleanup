/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

// Generated Dec 11, 2009 11:15:20 AM by Hibernate Tools 3.2.4.GA

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.OrganizationVO;
import com.resolve.services.hibernate.vo.ResolveAppsVO;
import com.resolve.services.hibernate.vo.ResolveControllersVO;
import com.resolve.services.hibernate.vo.RolesVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "resolve_apps",uniqueConstraints = { @UniqueConstraint(columnNames = { "u_app_name"}, name = "rapps_u_app_name_uk") })
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveApps extends BaseModel<ResolveAppsVO>
{
    private static final long serialVersionUID = -5710567772269130179L;

    private String UAppName;
    private String UDescription;

    // object references

    // object referenced by
    private Collection<ResolveAppControllerRel> resolveAppControllerRels;
    private Collection<ResolveAppRoleRel> resolveAppRoleRels;
    private Collection<ResolveAppOrganizationRel> resolveAppOrgRel;
    
    
    public ResolveApps()
    {
    }

    public ResolveApps(ResolveAppsVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_app_name", length = 200)
    public String getUAppName()
    {
        return UAppName;
    }

    public void setUAppName(String uAppName)
    {
        UAppName = uAppName;
    }

    @Column(name = "u_description", length = 3000)
    public String getUDescription()
    {
        return UDescription;
    }

    public void setUDescription(String uDescription)
    {
        UDescription = uDescription;
    }
    
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "resolveApp")
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.DELETE)
    public Collection<ResolveAppControllerRel> getResolveAppControllerRels()
    {
        return resolveAppControllerRels;
    }

    public void setResolveAppControllerRels(Collection<ResolveAppControllerRel> resolveAppControllerRels)
    {
        this.resolveAppControllerRels = resolveAppControllerRels;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "resolveApp")
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.DELETE)
    public Collection<ResolveAppRoleRel> getResolveAppRoleRels()
    {
        return resolveAppRoleRels;
    }

    public void setResolveAppRoleRels(Collection<ResolveAppRoleRel> resolveAppRoleRels)
    {
        this.resolveAppRoleRels = resolveAppRoleRels;
    }    
    
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "resolveApp")
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.DELETE)
    public Collection<ResolveAppOrganizationRel> getResolveAppOrgRel()
    {
        return resolveAppOrgRel;
    }

    public void setResolveAppOrgRel(Collection<ResolveAppOrganizationRel> resolveAppOrgRel)
    {
        this.resolveAppOrgRel = resolveAppOrgRel;
    }

    @Override
    public ResolveAppsVO doGetVO()
    {
        ResolveAppsVO vo = new ResolveAppsVO();
        super.doGetBaseVO(vo);

        vo.setUAppName(getUAppName());
        vo.setUDescription(getUDescription());
        
        //references
        //controllers
        Collection<ResolveAppControllerRel> resolveAppControllerRels = Hibernate.isInitialized(this.resolveAppControllerRels) ? getResolveAppControllerRels() : null;
        if(resolveAppControllerRels != null && resolveAppControllerRels.size() > 0)
        {
            List<ResolveControllersVO> appControllers = new ArrayList<ResolveControllersVO>();
            for(ResolveAppControllerRel model : resolveAppControllerRels)
            {
                ResolveControllers controller = model.getResolveController();
                appControllers.add(controller.doGetVO());
            }
            vo.setAppControllers(appControllers);
        }
        
        //roles
        Collection<ResolveAppRoleRel> resolveAppRoleRels = Hibernate.isInitialized(this.resolveAppRoleRels) ? getResolveAppRoleRels() : null;
        if(resolveAppRoleRels != null && resolveAppRoleRels.size() > 0)
        {
            List<RolesVO> appRoles = new ArrayList<RolesVO>();
            for(ResolveAppRoleRel model : resolveAppRoleRels)
            {
                if(model.getRoles() != null)
                {
                    appRoles.add(model.getRoles().doGetVO());
                }
            }
            vo.setAppRoles(appRoles);
        }
        
        //orgs
        Collection<ResolveAppOrganizationRel> resolveAppOrgRel = Hibernate.isInitialized(this.resolveAppOrgRel) ? getResolveAppOrgRel() : null;
        if(resolveAppOrgRel != null && resolveAppOrgRel.size() > 0)
        {
            List<OrganizationVO> appOrgs = new ArrayList<OrganizationVO>();
            for(ResolveAppOrganizationRel model : resolveAppOrgRel)
            {
                appOrgs.add(model.getOrganization().doGetVO());
            }
            vo.setAppOrgs(appOrgs);
        }

        return vo;
    }

    @Override
    public void applyVOToModel(ResolveAppsVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setUAppName(StringUtils.isNotBlank(vo.getUAppName()) && vo.getUAppName().equals(VO.STRING_DEFAULT) ? getUAppName() : vo.getUAppName());
            this.setUDescription(StringUtils.isNotBlank(vo.getUDescription()) && vo.getUDescription().equals(VO.STRING_DEFAULT) ? getUDescription() : vo.getUDescription());
            
            //references
//            Collection<ResolveAppControllerRelVO> resolveAppControllerRelsVOs = vo.getResolveAppControllerRels();
//            if(resolveAppControllerRelsVOs != null && resolveAppControllerRelsVOs.size() > 0)
//            {
//                Collection<ResolveAppControllerRel> models = new ArrayList<ResolveAppControllerRel>();
//                for(ResolveAppControllerRelVO refVO : resolveAppControllerRelsVOs)
//                {
//                    models.add(new ResolveAppControllerRel(refVO));
//                }
//                this.setResolveAppControllerRels(models);
//            }
//            
//            Collection<ResolveAppRoleRelVO> resolveAppRoleRelsVOs = vo.getResolveAppRoleRels();
//            if(resolveAppRoleRelsVOs != null && resolveAppRoleRelsVOs.size() > 0)
//            {
//                Collection<ResolveAppRoleRel> models = new ArrayList<ResolveAppRoleRel>();
//                for(ResolveAppRoleRelVO refVO : resolveAppRoleRelsVOs)
//                {
//                    models.add(new ResolveAppRoleRel(refVO));
//                }
//                this.setResolveAppRoleRels(models);
//            }
//            
//            Collection<ResolveAppOrganizationRelVO> resolveAppOrgRelVOs = vo.getResolveAppOrgRel();
//            if(resolveAppOrgRelVOs != null && resolveAppOrgRelVOs.size() > 0)
//            {
//                Collection<ResolveAppOrganizationRel> models = new ArrayList<ResolveAppOrganizationRel>();
//                for(ResolveAppOrganizationRelVO refVO : resolveAppOrgRelVOs)
//                {
//                    models.add(new ResolveAppOrganizationRel(refVO));
//                }
//                this.setResolveAppOrgRel(models);
//            }
            
            
        }

    }

} // AlertLog
