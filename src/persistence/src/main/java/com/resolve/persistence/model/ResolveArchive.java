/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

// Generated Jan 15, 2010 2:52:33 PM by Hibernate Tools 3.2.4.GA

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.resolve.services.hibernate.vo.ResolveArchiveVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

/**
 * Wikiarchive generated by hbm2java
 */
@Entity
@Table(name = "resolve_archive")
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveArchive extends BaseModel<ResolveArchiveVO>
{
    private static final long serialVersionUID = -5849325134507912097L;
    
    private String UTableId;
    private String UTableName;
    private String UTableColumn;
    private Integer UVersion;
    private String UPatch;
    private String UComment;
    
    public ResolveArchive()
    {
    }
    
    public ResolveArchive(ResolveArchiveVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_table_sys_id", nullable = false)
    public String getUTableId()
    {
        return this.UTableId;
    }

    public void setUTableId(String UTableId)
    {
        this.UTableId = UTableId;
    }

    @Column(name = "u_table_name")
    public String getUTableName()
    {
        return this.UTableName;
    }

    public void setUTableName(String UTableName)
    {
        this.UTableName = UTableName;
    }

    @Column(name = "u_table_column")
    public String getUTableColumn()
    {
        return this.UTableColumn;
    }

    public void setUTableColumn(String UTableColumn)
    {
        this.UTableColumn = UTableColumn;
    }

    @Column(name = "u_version")
    public Integer getUVersion()
    {
        return this.UVersion;
    }

    public void setUVersion(Integer UVersion)
    {
        this.UVersion = UVersion;
    }

	@Lob
    @Column(name = "u_patch", length = 16777215)
    public String getUPatch()
    {
        return this.UPatch;
    }

    public void setUPatch(String UPatch)
    {
        this.UPatch = UPatch;
    }
    
    @Column(name = "u_comment", length = 4000)
    public String getUComment()
    {
        return this.UComment;
    }

    public void setUComment(String UComment)
    {
        this.UComment = UComment;
    }

    @Override
    public ResolveArchiveVO doGetVO()
    {
        ResolveArchiveVO vo = new ResolveArchiveVO();
        super.doGetBaseVO(vo);
        
        vo.setUComment(getUComment());
        vo.setUPatch(getUPatch());
        vo.setUTableColumn(getUTableColumn());
        vo.setUTableId(getUTableId());
        vo.setUTableName(getUTableName());
        vo.setUVersion(getUVersion());
        
        return vo;
    }

    @Override
    public void applyVOToModel(ResolveArchiveVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo); 
                
            this.setUComment(StringUtils.isNotBlank(vo.getUComment()) && vo.getUComment().equals(VO.STRING_DEFAULT) ? getUComment() : vo.getUComment());
            this.setUPatch(StringUtils.isNotBlank(vo.getUPatch()) && vo.getUPatch().equals(VO.STRING_DEFAULT) ? getUPatch() : vo.getUPatch());
            this.setUTableColumn(StringUtils.isNotBlank(vo.getUTableColumn()) && vo.getUTableColumn().equals(VO.STRING_DEFAULT) ? getUTableColumn() : vo.getUTableColumn());
            this.setUTableId(StringUtils.isNotBlank(vo.getUTableId()) && vo.getUTableId().equals(VO.STRING_DEFAULT) ? getUTableId() : vo.getUTableId());
            this.setUTableName(StringUtils.isNotBlank(vo.getUTableName()) && vo.getUTableName().equals(VO.STRING_DEFAULT) ? getUTableName() : vo.getUTableName());
            this.setUVersion(vo.getUVersion() != null && vo.getUVersion().equals(VO.INTEGER_DEFAULT) ? getUVersion() : vo.getUVersion());
        }
    }

}
