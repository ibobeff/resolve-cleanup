package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.resolve.services.hibernate.vo.CustomDictionaryItemVO;

@Entity
@Table(name = "custom_artifact_key_dictionary")
public class CustomDictionaryItem extends BaseModel<CustomDictionaryItemVO> {
	private static final long serialVersionUID = 7058659541241857766L;

	private String UShortName;
	private String UFullName;
	private String UDescription;
	private ArtifactType artifactType;
	
	@Column(name = "u_short_name", unique = true, nullable = false)
	public String getUShortName() {
		return UShortName;
	}

	public void setUShortName(String uShortName) {
		UShortName = uShortName;
	}

	@Column(name = "u_full_name", unique = true, nullable = false)
	public String getUFullName() {
		return UFullName;
	}

	public void setUFullName(String uFullName) {
		UFullName = uFullName;
	}

	@Column(name = "u_description", length = 1000)
	public String getUDescription() {
		return UDescription;
	}

	public void setUDescription(String uDescription) {
		UDescription = uDescription;
	}

	@ManyToOne
	@JoinColumn(name = "artifact_type_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
	public ArtifactType getArtifactType() {
		return artifactType;
	}

	public void setArtifactType(ArtifactType artifactType) {
		this.artifactType = artifactType;
	}

	@Override
	public CustomDictionaryItemVO doGetVO() {
		CustomDictionaryItemVO vo = new CustomDictionaryItemVO();
        super.doGetBaseVO(vo);
        
        vo.setUShortName(UShortName);
        vo.setUFullName(UFullName);
        vo.setUDescription(UDescription);

        return vo;
	}
}
