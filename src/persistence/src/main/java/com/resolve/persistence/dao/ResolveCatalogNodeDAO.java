package com.resolve.persistence.dao;

import com.resolve.persistence.model.ResolveCatalogNode;

public interface ResolveCatalogNodeDAO extends GenericDAO<ResolveCatalogNode, String>
{

}
