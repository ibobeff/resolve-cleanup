/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.MetaFormActionVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;


@Entity
@Table(name = "meta_form_action", indexes = {@Index(columnList = "u_meta_control_item_sys_id", name = "u_meta_control_item_sys_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaFormAction  extends BaseModel<MetaFormActionVO>
{
    private static final long serialVersionUID = -6292016170447595916L;
    
    private String UOperation;//check the Operation.java for valid types
    private String UCrudAction;//for DB, mapped to CRUDAction.java
    private String URunbookName;//for EXECUTEPROCESS
    private String UScriptName;//for BUSINESSRULE
    private String UActionTaskName;
    private Integer USequence;//sequence # 
    private String UAdditionalParams;
    private String URedirectUrl;
    private String URedirectTarget;
    
    private MetaControlItem metaControlItem;
    
    public MetaFormAction()
    {
    }
       
    public MetaFormAction(MetaFormActionVO vo)
    {
        applyVOToModel(vo);
    }    
    /**
     * @return the uOperation
     */
    @Column(name = "u_operation", length = 32)
    public String getUOperation()
    {
        return UOperation;
    }
    /**
     * @param uOperation the uOperation to set
     */
    public void setUOperation(String uOperation)
    {
        UOperation = uOperation;
    }
    
    /**
     * @return the uCrudAction
     */
    @Column(name = "u_crud_action", length = 32)
    public String getUCrudAction()
    {
        return UCrudAction;
    }
    /**
     * @param uCrudAction the uCrudAction to set
     */
    public void setUCrudAction(String uCrudAction)
    {
        UCrudAction = uCrudAction;
    }


    /**
     * @return the uRunbookName
     */
    @Column(name = "u_runbook_fullname", length = 300)
    public String getURunbookName()
    {
        return URunbookName;
    }
    /**
     * @param uRunbookName the uRunbookName to set
     */
    public void setURunbookName(String uRunbookName)
    {
        URunbookName = uRunbookName;
    }

    /**
     * @return the uScriptName
     */
    @Column(name = "u_script_name", length = 300)
    public String getUScriptName()
    {
        return UScriptName;
    }
    /**
     * @param uScriptName the uScriptName to set
     */
    public void setUScriptName(String uScriptName)
    {
        UScriptName = uScriptName;
    }
    
    
    @Column(name = "u_actiontask_name", length = 300)
    public String getUActionTaskName()
    {
        return UActionTaskName;
    }

    public void setUActionTaskName(String uActionTaskName)
    {
        UActionTaskName = uActionTaskName;
    }

    /**
     * @return the uSequence
     */
    @Column(name = "u_sequence")
    public Integer getUSequence()
    {
        return USequence;
    }
    /**
     * @param uSequence the uSequence to set
     */
    public void setUSequence(Integer uSequence)
    {
        USequence = uSequence;
    }

    @Column(name = "u_add_params")
    public String getUAdditionalParams()
    {
        return UAdditionalParams;
    }


    public void setUAdditionalParams(String uAdditionalParams)
    {
        UAdditionalParams = uAdditionalParams;
    }
    
    @Column(name = "u_redirect_url")
    public String getURedirectUrl()
    {
        return URedirectUrl;
    }

    public void setURedirectUrl(String uRedirectUrl)
    {
        URedirectUrl = uRedirectUrl;
    }


    @Column(name = "u_redirect_target")
    public String getURedirectTarget()
    {
        return URedirectTarget;
    }


    public void setURedirectTarget(String uRedirectTarget)
    {
        URedirectTarget = uRedirectTarget;
    }


    /**
     * @return the metaControlItem
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_meta_control_item_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaControlItem getMetaControlItem()
    {
        return metaControlItem;
    }


    /**
     * @param metaControlItem the metaControlItem to set
     */
    public void setMetaControlItem(MetaControlItem metaControlItem)
    {
        this.metaControlItem = metaControlItem;
        
        if (metaControlItem != null)
        {
            Collection<MetaFormAction> coll = metaControlItem.getMetaFormActions();
            if (coll == null)
            {
                coll = new HashSet<MetaFormAction>();
                coll.add(this);
              
                metaControlItem.setMetaFormActions(coll);
            }
        }
    }


    @Override
    public MetaFormActionVO doGetVO()
    {
        MetaFormActionVO vo = new MetaFormActionVO();
        super.doGetBaseVO(vo);
        
        vo.setUActionTaskName(getUActionTaskName());
        vo.setUAdditionalParams(getUAdditionalParams());
        vo.setUCrudAction(getUCrudAction());
        vo.setUOperation(getUOperation());
        vo.setURedirectTarget(getURedirectTarget());
        vo.setURedirectUrl(getURedirectUrl());
        vo.setURunbookName(getURunbookName());
        vo.setUScriptName(getUScriptName());
        vo.setUSequence(getUSequence());
        
        //ref - no refs to parent
        
        return vo;
    }


    @Override
    public void applyVOToModel(MetaFormActionVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUActionTaskName(StringUtils.isNotBlank(vo.getUActionTaskName()) && vo.getUActionTaskName().equals(VO.STRING_DEFAULT) ? getUActionTaskName() : vo.getUActionTaskName());
            this.setUAdditionalParams(StringUtils.isNotBlank(vo.getUAdditionalParams()) && vo.getUAdditionalParams().equals(VO.STRING_DEFAULT) ? getUAdditionalParams() : vo.getUAdditionalParams());
            this.setUCrudAction(StringUtils.isNotBlank(vo.getUCrudAction()) && vo.getUCrudAction().equals(VO.STRING_DEFAULT) ? getUCrudAction() : vo.getUCrudAction());
            this.setUOperation(StringUtils.isNotBlank(vo.getUOperation()) && vo.getUOperation().equals(VO.STRING_DEFAULT) ? getUOperation() : vo.getUOperation());
            this.setURedirectTarget(StringUtils.isNotBlank(vo.getURedirectTarget()) && vo.getURedirectTarget().equals(VO.STRING_DEFAULT) ? getURedirectTarget() : vo.getURedirectTarget());
            this.setURedirectUrl(StringUtils.isNotBlank(vo.getURedirectUrl()) && vo.getURedirectUrl().equals(VO.STRING_DEFAULT) ? getURedirectUrl() : vo.getURedirectUrl());
            this.setURunbookName(StringUtils.isNotBlank(vo.getURunbookName()) && vo.getURunbookName().equals(VO.STRING_DEFAULT) ? getURunbookName() : vo.getURunbookName());
            this.setUScriptName(StringUtils.isNotBlank(vo.getUScriptName()) && vo.getUScriptName().equals(VO.STRING_DEFAULT) ? getUScriptName() : vo.getUScriptName());
            this.setUSequence(vo.getUSequence() != null && vo.getUSequence().equals(VO.INTEGER_DEFAULT) ? getUSequence() : vo.getUSequence());
            
            //ref - no refs to parent
            
            
        }
        
    }
    
    
    

}
