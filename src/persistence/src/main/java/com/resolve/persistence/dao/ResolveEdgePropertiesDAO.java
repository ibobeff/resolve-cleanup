package com.resolve.persistence.dao;

import com.resolve.persistence.model.ResolveEdgeProperties;

public interface ResolveEdgePropertiesDAO extends GenericDAO<ResolveEdgeProperties, String>
{

}
