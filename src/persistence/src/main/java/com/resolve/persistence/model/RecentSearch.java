/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.resolve.services.hibernate.vo.RecentSearchVO;

@Entity
@Table(name = "recent_search")
public class RecentSearch extends BaseModel<RecentSearchVO>
{
	private static final String ITEM_DELIMITER = ",";
	
	private static final long serialVersionUID = -3711061291480277731L;
	
	private String term;

	private String searchFields;

	private String type = "ALL";

	private String authors;

	private String tags;

	private String createdOn;

	private String updatedOn;

	private String sortBy;

	private int size;
	
	private int from;

	private String namespaces;

	private String menupaths;

	@Column(name = "term", length = 1000)
	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	@Column(name = "search_fields", length = 1000)
	public String getSearchFields() {
		return searchFields;
	}

	public void setSearchFields(String searchFields) {
		this.searchFields = searchFields;
	}

	@Column(name = "type", length = 30)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "authors", length = 500)
	public String getAuthors() {
		return authors;
	}

	public void setAuthors(String authors) {
		this.authors = authors;
	}

	@Column(name = "tags", length = 1000)
	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	@Column(name = "created_on", length = 50)
	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	@Column(name = "updated_on", length = 50)
	public String getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column(name = "sort_by", length = 100)
	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	@Column(name = "size_")
	public int getSize() {
		return size;
	}
	
	public void setSize(int size) {
		this.size = size;
	}

	@Column(name = "col_from")
	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	@Column(name = "namespaces", length = 1000)
	public String getNamespaces() {
		return namespaces;
	}

	public void setNamespaces(String namespaces) {
		this.namespaces = namespaces;
	}

	@Column(name = "menupaths", length = 1000)
	public String getMenupaths() {
		return menupaths;
	}

	public void setMenupaths(String menupaths) {
		this.menupaths = menupaths;
	}

	@Override
	public void applyVOToModel(RecentSearchVO vo) {
		if (vo != null)
        {
            super.applyVOToModel(vo);
            this.setTerm(vo.getTerm());
            this.setSearchFields(vo.getSearchFields() != null ? String.join(ITEM_DELIMITER, vo.getSearchFields()) : null);
            this.setType(vo.getType().name());
            this.setAuthors(vo.getAuthors() != null ? String.join(ITEM_DELIMITER, vo.getAuthors()): null);
            this.setTags(vo.getTags() != null ? String.join(ITEM_DELIMITER, vo.getTags()) : null);
            this.setCreatedOn(vo.getCreatedOn());
            this.setUpdatedOn(vo.getUpdatedOn());
            this.setSortBy(vo.getSortBy());
            this.setSize(vo.getSize());
            this.setFrom(vo.getFrom());
            this.setNamespaces(vo.getNamespaces() != null ? String.join(ITEM_DELIMITER, vo.getNamespaces()) : null);
            this.setMenupaths(vo.getMenupaths() != null ? String.join(ITEM_DELIMITER, vo.getMenupaths()) : null);
        }
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public RecentSearchVO doGetVO() {
		RecentSearchVO vo = new RecentSearchVO();
        super.doGetBaseVO(vo);

        vo.setTerm(this.getTerm());
        vo.setSearchFields(this.getSearchFields() != null ? Arrays.asList(this.getSearchFields().split(ITEM_DELIMITER)) : null);
        vo.setType(this.getType());
        vo.setAuthors(this.getAuthors() != null ? Arrays.asList(this.getAuthors().split(ITEM_DELIMITER)) : null);
        vo.setTags(this.getTags() != null ? Arrays.asList(this.getTags().split(ITEM_DELIMITER)) : null);
        vo.setCreatedOn(this.getCreatedOn());
        vo.setUpdatedOn(this.getUpdatedOn());
        vo.setSortBy(this.getSortBy());
        vo.setSize(String.valueOf(this.getSize()));
        vo.setFrom(String.valueOf(this.getFrom()));
        vo.setNamespaces(this.getNamespaces() != null ? Arrays.asList(this.getNamespaces().split(ITEM_DELIMITER)) : null);
        vo.setMenupaths(this.getMenupaths() != null ? Arrays.asList(this.getMenupaths().split(ITEM_DELIMITER)) : null);
        
        return vo;
	}
}
