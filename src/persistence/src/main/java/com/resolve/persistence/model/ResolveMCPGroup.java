package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolveMCPClusterVO;
import com.resolve.services.hibernate.vo.ResolveMCPGroupVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "resolve_mcp_group", uniqueConstraints = {@UniqueConstraint(columnNames = {"u_groupname"}, name = "rmcpgrp_u_groupname_uk") })
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveMCPGroup extends BaseModel<ResolveMCPGroupVO>
{
    private static final long serialVersionUID = -5121198375770621227L;
    
    private String UGroupName;
    private Collection<ResolveMCPCluster> clusters;
    
    public ResolveMCPGroup()
    {
    }
    
    public ResolveMCPGroup(ResolveMCPGroupVO vo)
    {
        applyVOToModel(vo);
    }
    
    @Column(name = "u_groupname", length = 300, nullable=false)
    public String getUGroupName()
    {
        return UGroupName;
    }
    
    public void setUGroupName(String uGroupName)
    {
        UGroupName = uGroupName;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "clusterGroup")
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<ResolveMCPCluster> getClusters()
    {
        return clusters;
    }

    public void setClusters(Collection<ResolveMCPCluster> clusters)
    {
        this.clusters = clusters;
    }

    @Override
    public ResolveMCPGroupVO doGetVO()
    {
        ResolveMCPGroupVO vo = new ResolveMCPGroupVO();
        super.doGetBaseVO(vo);

        vo.setUGroupName(getUGroupName());
        Collection<ResolveMCPCluster> mcpClusters = Hibernate.isInitialized(this.getClusters()) ? getClusters() : null;
        if (mcpClusters != null)
        {
            Collection<ResolveMCPClusterVO> clusterVOs = new ArrayList<ResolveMCPClusterVO>();
            for (ResolveMCPCluster cluster : mcpClusters)
            {
                clusterVOs.add(cluster.doGetVO());
            }
            vo.setChildren(clusterVOs);
        }

        return vo;
    }
    
    @Override
    public void applyVOToModel(ResolveMCPGroupVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUGroupName(StringUtils.isNotBlank(vo.getUGroupName()) && vo.getUGroupName().equals(VO.STRING_DEFAULT) ? getUGroupName() : vo.getUGroupName());
        }
    }
}
