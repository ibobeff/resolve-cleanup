package com.resolve.persistence.dao;

import com.resolve.persistence.model.PullGatewayFilter;

public interface PullGatewayFilterDAO extends GenericDAO<PullGatewayFilter, String>
{

}
