/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolveCatalogNodeVO;
import com.resolve.services.hibernate.vo.ResolveCatalogVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "catalog")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveCatalog  extends BaseModel<ResolveCatalogVO> 
{
    private static final long serialVersionUID = 7019919780154658101L;

    private String UName;
    private String UType;
    
    // object referenced by
    private Collection<ResolveCatalogNode> catalogNodes;
    
    
    public ResolveCatalog()
    {
    }
    
    public ResolveCatalog(String sysId)
    {
        super.setSys_id(sysId);
    }
    
    public ResolveCatalog(ResolveCatalogVO vo)
    {
        applyVOToModel(vo);
    }
    
    @Column(name = "u_name", length = 300)
    public String getUName()
    {
        return UName;
    }

    public void setUName(String uName)
    {
        UName = uName;
    }
    
    @Column(name = "u_type", length = 50)
    public String getUType()
    {
        return UType;
    }

    public void setUType(String uType)
    {
        UType = uType;
    }

    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "catalog")
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.DELETE)
    public Collection<ResolveCatalogNode> getCatalogNodes()
    {
        return catalogNodes;
    }

    public void setCatalogNodes(Collection<ResolveCatalogNode> catalogNodes)
    {
        this.catalogNodes = catalogNodes;
    }

    @Override
    public ResolveCatalogVO doGetVO()
    {
        ResolveCatalogVO vo = new ResolveCatalogVO();
        super.doGetBaseVO(vo);
        
        vo.setUName(getUName());
        vo.setUType(getUType());
        
        
        Collection<ResolveCatalogNode> catalogNodes = Hibernate.isInitialized(this.catalogNodes) ? getCatalogNodes() : null;
        if (catalogNodes != null && catalogNodes.size() > 0)
        {
            Collection<ResolveCatalogNodeVO> vos = new ArrayList<ResolveCatalogNodeVO>();
            for (ResolveCatalogNode model : catalogNodes)
            {
                vos.add(model.doGetVO());
            }
            vo.setCatalogNodes(vos);
        }
        
        return vo;
    }
    
    @Override
    public void applyVOToModel(ResolveCatalogVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);  
        
            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUType(StringUtils.isNotBlank(vo.getUType()) && vo.getUType().equals(VO.STRING_DEFAULT) ? getUType() : vo.getUType());
            
            Collection<ResolveCatalogNodeVO> catalogNodes = vo.getCatalogNodes();
            if (catalogNodes != null && catalogNodes.size() > 0)
            {
                Collection<ResolveCatalogNode> models = new ArrayList<ResolveCatalogNode>();
                for (ResolveCatalogNodeVO refVO : catalogNodes)
                {
                    models.add(new ResolveCatalogNode(refVO));
                }
                
                this.setCatalogNodes(models);
            }

        }
    }
    

}
