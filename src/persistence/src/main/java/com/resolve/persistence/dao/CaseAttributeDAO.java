package com.resolve.persistence.dao;

import com.resolve.persistence.model.CaseAttribute;

public interface CaseAttributeDAO extends GenericDAO<CaseAttribute, String> {}
