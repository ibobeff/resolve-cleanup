/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.resolve.services.hibernate.vo.CatalogAttachmentVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "catalog_attachment", indexes = {@Index(columnList = "filename", name = "ca_filename_idx", unique = false)})
public class CatalogAttachment extends BaseModel<CatalogAttachmentVO>
{
    private static final long serialVersionUID = -467880949001576532L;
    
    private String fileName;
    private String displayName;
    private String type;
    private String location;
    private Integer size;
    private byte[] content;

    public CatalogAttachment() {}
    
    public CatalogAttachment(CatalogAttachmentVO vo ) 
    {
        applyVOToModel(vo);
    }
   
    @Column(name = "filename", nullable = false, length = 256)
    public String getFileName()
    {
        return fileName;
    }
    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }
    
    @Column(name = "displayname", nullable = false, length = 1000)
    public String getDisplayName()
    {
        return displayName;
    }
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }
    
    @Column(name = "type", length = 4000)
    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }
    
    @Column(name = "location", length = 4000)
    public String getLocation()
    {
        return location;
    }
    public void setLocation(String location)
    {
        this.location = location;
    }
    
    @Column(name = "u_size")
    public Integer getSize()
    {
        return size;
    }
    public void setSize(Integer size)
    {
        this.size = size;
    }
    
    @Lob
    @Column(name = "u_content", length = 16777215)
    public byte[] getContent()
    {
        return content;
    }
    public void setContent(byte[] content)
    {
        this.content = content;
    }
    
    @Override
    public CatalogAttachmentVO doGetVO()
    {
        CatalogAttachmentVO vo = new CatalogAttachmentVO();
        super.doGetBaseVO(vo);
        
        vo.setContent(getContent());
        vo.setDisplayname(getDisplayName());
        vo.setFilename(getFileName());
        vo.setLocation(getLocation());
        vo.setU_size(getSize());
        vo.setType(getType());       
        
        return vo;
    }

    @Override
    public void applyVOToModel(CatalogAttachmentVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo); 
            
            this.setContent(vo.getContent() != null && vo.getContent().equals(VO.BYTE_ARRAY_DEFAULT) ? getContent() : vo.getContent());
            this.setDisplayName(StringUtils.isNotBlank(vo.getDisplayname()) && vo.getDisplayname().equals(VO.STRING_DEFAULT) ? getDisplayName() : vo.getDisplayname());
            this.setFileName(StringUtils.isNotBlank(vo.getFilename()) && vo.getFilename().equals(VO.STRING_DEFAULT) ? getFileName() : vo.getFilename());
            this.setLocation(StringUtils.isNotBlank(vo.getLocation()) && vo.getLocation().equals(VO.STRING_DEFAULT) ? getLocation() : vo.getLocation());
            this.setSize(vo.getU_size() != null && vo.getU_size().equals(VO.INTEGER_DEFAULT) ? getSize() : vo.getU_size());
            this.setType(StringUtils.isNotBlank(vo.getType()) && vo.getType().equals(VO.STRING_DEFAULT) ? getType() : vo.getType());
            
            
        }
        
    }
}
