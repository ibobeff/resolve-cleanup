/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.services.hibernate.vo.SSHPoolVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "ssh_pool",  uniqueConstraints = {@UniqueConstraint(columnNames = {"u_subnetmask", "u_queue"}, name = "sshp_subnetmask_queue_uk")}) 
// @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SSHPool extends BaseModel<SSHPoolVO>
{
    private static final long serialVersionUID = -6764449839432502493L;
    
    private String UQueue;
    private String USubnetMask;
    private Integer UMaxConn;
    private Integer UTimeout;
   

    public SSHPool()
    {
    } // DatabaseConnectionPool

    public SSHPool(SSHPoolVO vo)
    {
        applyVOToModel(vo);
    } // DatabaseConnectionPool

    
    @Column(name = "u_queue", length = 100)
    public String getUQueue()
    {
        return this.UQueue;
    } // getUQueue

    public void setUQueue(String UQueue)
    {
        this.UQueue = UQueue;
    } // setUQueue
    
    @Column(name = "u_subnetmask", length = 40)
    public String getUSubnetMask()
    {
        return USubnetMask;
    }

    public void setUSubnetMask(String uSubnetMask)
    {
        USubnetMask = uSubnetMask;
    }

    @Column(name = "u_maxconn")
    public Integer getUMaxConn()
    {
        return UMaxConn;
    }

    public void setUMaxConn(Integer uMaxConn)
    {
        UMaxConn = uMaxConn;
    }

    @Column(name = "u_timeout")
    public Integer getUTimeout()
    {
        return UTimeout;
    }

    public void setUTimeout(Integer uTimeout)
    {
        UTimeout = uTimeout;
    }

    @Override
    public SSHPoolVO doGetVO()
    {
        SSHPoolVO vo = new SSHPoolVO();
        super.doGetBaseVO(vo);
        
        vo.setUMaxConn(getUMaxConn());
        vo.setUQueue(getUQueue());
        vo.setUSubnetMask(getUSubnetMask());
        vo.setUTimeout(getUTimeout());
        
        return vo;
    }

    @Override
    public void applyVOToModel(SSHPoolVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);

            this.setUMaxConn(vo.getUMaxConn() != null && vo.getUMaxConn().equals(VO.INTEGER_DEFAULT) ? getUMaxConn() : vo.getUMaxConn());
            this.setUTimeout(vo.getUTimeout() != null && vo.getUTimeout().equals(VO.INTEGER_DEFAULT) ? getUTimeout() : vo.getUTimeout());
            
            this.setUQueue(StringUtils.isNotBlank(vo.getUQueue()) && vo.getUQueue().equals(VO.STRING_DEFAULT) ? getUQueue() : vo.getUQueue());
            this.setUSubnetMask(StringUtils.isNotBlank(vo.getUSubnetMask()) && vo.getUSubnetMask().equals(VO.STRING_DEFAULT) ? getUSubnetMask() : vo.getUSubnetMask());
            
        }
        
    }
}

