package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.RRRuleFieldVO;

@SuppressWarnings("serial")
@Entity
@Table(name = "rr_rule_field",uniqueConstraints = { @UniqueConstraint(columnNames = { "u_rule","u_name"}, name = "rrrf_u_rule_u_name_uk") })
public class RRRuleField extends BaseModel<RRRuleFieldVO>
{
    private String name;
    private String value;
    private RRRule rule;
    private Integer order;
    public RRRuleField() {
        super();
    }
    public RRRuleField(String id,String name,String value,Integer order,RRRule rule) {
        super();
        this.setSys_id(id);
        this.name = name;
        this.value = value;
        this.rule = rule;
        this.order = order;
    }
    @Column(name = "u_name", length = 250)
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    @Column(name = "u_value", length = 4000)
    public String getValue()
    {
        return value;
    }
    public void setValue(String value)
    {
        this.value = value;
    }
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_rule", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public RRRule getRule()
    {
        return rule;
    }

    public void setRule(RRRule rule)
    {
        this.rule = rule;
    }
    @Column(name = "u_order")
    public Integer getOrder()
    {
        return order;
    }
    public void setOrder(Integer order)
    {
        this.order = order;
    }
    @Override
    public RRRuleFieldVO doGetVO()
    {
        RRRuleFieldVO vo = new RRRuleFieldVO();
        super.doGetBaseVO(vo);
        vo.setName(getName());
        vo.setValue(getValue());
        vo.setOrder(getOrder());
//        vo.setRule(getRule().doGetVO());
        return vo;
    }
}
