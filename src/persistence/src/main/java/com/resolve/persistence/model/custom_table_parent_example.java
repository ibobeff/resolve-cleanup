/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;


@Entity
@Table(name = "custom_table_parent_example")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class custom_table_parent_example implements java.io.Serializable
{
    private static final long serialVersionUID = 5781121485087032707L;
    private String u_tf_string;
    private String u_ta_string;
    private String u_journal_string;
    private Boolean u_cb_boolean;
    private Long u_tf_number;
    private Double u_tf_decimal;
    private String u_cbox_select;//combo box 
    private String u_rb_choice;//radio button
    private String u_checkbox;//multiple check box
    private Date u_datetime;//date time example
    private Date u_date;//only date and no time
    private String u_list;
    private String u_tf_sequence;
    private Users u_tf_reference;
    private String u_tf_link;
    private Long u_timestamp;
    private String u_tags;


    // sys fields
    private String sys_id;
    private String sysUpdatedBy;
    private Date sysUpdatedOn;
    private String sysCreatedBy;
    private Date sysCreatedOn;
    private Integer sysModCount;

    
    
    
    //////////////////////////////
    
    /**
     * @return the u_tf_string
     */
    @Column(name = "u_tf_string", length = 100)
    public String getU_tf_string()
    {
        return u_tf_string;
    }

    /**
     * @param u_tf_string the u_tf_string to set
     */
    public void setU_tf_string(String u_tf_string)
    {
        this.u_tf_string = u_tf_string;
    }

    /**
     * @return the u_ta_string
     */
    @Column(name = "u_ta_string", length = 4000)
    public String getU_ta_string()
    {
        return u_ta_string;
    }

    /**
     * @param u_ta_string the u_ta_string to set
     */
    public void setU_ta_string(String u_ta_string)
    {
        this.u_ta_string = u_ta_string;
    }

    /**
     * @return the u_journal_string
     */
    @Lob
    @Column(name = "u_journal_string", length = 16777215)
    public String getU_journal_string()
    {
        return u_journal_string;
    }

    /**
     * @param u_journal_string the u_journal_string to set
     */
    public void setU_journal_string(String u_journal_string)
    {
        this.u_journal_string = u_journal_string;
    }

    /**
     * @return the u_cb_boolean
     */
    @Column(name = "u_cb_boolean", length = 1)
    @Type(type = "yes_no")
    public Boolean getU_cb_boolean()
    {
        return u_cb_boolean;
    }

    /**
     * @param u_cb_boolean the u_cb_boolean to set
     */
    public void setU_cb_boolean(Boolean u_cb_boolean)
    {
        this.u_cb_boolean = u_cb_boolean;
    }
    
    /**
     * @return the u_tf_number
     */
    @Column(name = "u_tf_number")
    public Long getU_tf_number()
    {
        return u_tf_number;
    }
    public void setU_tf_number(Long u_tf_number)
    {
        this.u_tf_number = u_tf_number;
    }

    /**
     * @return the u_tf_decimal
     */
    @Column(name = "u_tf_decimal")
    public Double getU_tf_decimal()
    {
        return u_tf_decimal;
    }
    public void setU_tf_decimal(Double u_tf_decimal)
    {
        this.u_tf_decimal = u_tf_decimal;
    }

    
    /**
     * @return the u_cbox_select
     */
    @Column(name = "u_cbox_select")
    public String getU_cbox_select()
    {
        return u_cbox_select;
    }
    public void setU_cbox_select(String u_cbox_select)
    {
        this.u_cbox_select = u_cbox_select;
    }


    /**
     * @return the u_rb_choice
     */
    @Column(name = "u_rb_choice")
    public String getU_rb_choice()
    {
        return u_rb_choice;
    }
    public void setU_rb_choice(String u_rb_choice)
    {
        this.u_rb_choice = u_rb_choice;
    }


    /**
     * @return the u_checkbox
     */
    @Column(name = "u_checkbox")
    public String getU_checkbox()
    {
        return u_checkbox;
    }
    public void setU_checkbox(String u_checkbox)
    {
        this.u_checkbox = u_checkbox;
    }


    /**
     * @return the u_datetime
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "u_datetime")
    public Date getU_datetime()
    {
        return u_datetime;
    }
    public void setU_datetime(Date u_datetime)
    {
        this.u_datetime = u_datetime;
    }
    

    /**
     * @return the u_date
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "u_date")
    public Date getU_date()
    {
        return u_date;
    }
    public void setU_date(Date u_date)
    {
        this.u_date = u_date;
    }


    /**
     * @return the u_list
     */
    @Column(name = "u_list", length = 4000)
    public String getU_list()
    {
        return u_list;
    }
    public void setU_list(String u_list)
    {
        this.u_list = u_list;
    }


    /**
     * @return the u_tf_sequence
     */
    @Column(name = "u_tf_sequence", length = 100)
    public String getU_tf_sequence()
    {
        return u_tf_sequence;
    }
    public void setU_tf_sequence(String u_tf_sequence)
    {
        this.u_tf_sequence = u_tf_sequence;
    }


//    /**
//     * @return the u_tf_reference
//     */
//    @Column(name = "u_tf_reference", length = 100)
//    public String getU_tf_reference()
//    {
//        return u_tf_reference;
//    }
//    public void setU_tf_reference(String u_tf_reference)
//    {
//        this.u_tf_reference = u_tf_reference;
//    }
//    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_tf_reference", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public Users getU_tf_reference()
    {
        return u_tf_reference;
    }

    public void setU_tf_reference(Users u_tf_reference)
    {
        this.u_tf_reference = u_tf_reference;
        
        // one-way only - no collections on the other side
    }

    /**
     * @return the u_tf_link
     */
    @Column(name = "u_tf_link", length = 4000)
    public String getU_tf_link()
    {
        return u_tf_link;
    }
    public void setU_tf_link(String u_tf_link)
    {
        this.u_tf_link = u_tf_link;
    }

    /**
     * @return the u_timestamp
     */
    @Column(name = "u_timestamp")
    public Long getU_timestamp()
    {
        return u_timestamp;
    }
    public void setU_timestamp(Long u_timestamp)
    {
        this.u_timestamp = u_timestamp;
    }


    @Lob
    @Column(name = "u_tags", length = 16777215)
    public String getU_tags()
    {
        return u_tags;
    }
    public void setU_tags(String u_tags)
    {
        this.u_tags = u_tags;
    }

    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // sys cols
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Id
    @Column(name = "sys_id", nullable = false, length = 32)
    @GeneratedValue(generator = "hibernate-uuid")
    public String getsys_id()
    {
        return this.sys_id;
    }

    public void setsys_id(String sys_id)
    {
        this.sys_id = sys_id;
    }
    
    @Column(name = "sys_updated_by", length = 40)
    public String getSysUpdatedBy()
    {
        return this.sysUpdatedBy;
    }

    public void setSysUpdatedBy(String sysUpdatedBy)
    {
        this.sysUpdatedBy = sysUpdatedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sys_updated_on", length = 19)
    public Date getSysUpdatedOn()
    {
        return this.sysUpdatedOn;
    }

    public void setSysUpdatedOn(Date sysUpdatedOn)
    {
        this.sysUpdatedOn = sysUpdatedOn;
    }

    @Column(name = "sys_created_by", length = 40)
    public String getSysCreatedBy()
    {
        return this.sysCreatedBy;
    }

    public void setSysCreatedBy(String sysCreatedBy)
    {
        this.sysCreatedBy = sysCreatedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sys_created_on", length = 19)
    public Date getSysCreatedOn()
    {
        return this.sysCreatedOn;
    }

    public void setSysCreatedOn(Date sysCreatedOn)
    {
        this.sysCreatedOn = sysCreatedOn;
    }

    @Column(name = "sys_mod_count")
    public Integer getSysModCount()
    {
        return this.sysModCount;
    }

    public void setSysModCount(Integer sysModCount)
    {
        this.sysModCount = sysModCount;
    }

}
