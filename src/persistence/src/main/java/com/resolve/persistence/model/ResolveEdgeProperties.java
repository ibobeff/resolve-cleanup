/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolveEdgePropertiesVO;
import com.resolve.services.hibernate.vo.ResolveEdgeVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "resolve_edge_properties", indexes = {@Index(columnList = "u_edge_sys_id", name = "rnp_u_edge_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveEdgeProperties extends BaseModel<ResolveEdgePropertiesVO>
{
    
    private static final long serialVersionUID = 6576074490991613016L;
    
    private String UKey;
    private String UValue;
    
    private ResolveEdge edge;

    public ResolveEdgeProperties()
    {
    }
    
    public ResolveEdgeProperties(String sysId)
    {
        this.setSys_id(sysId);
    }

    public ResolveEdgeProperties(ResolveEdgePropertiesVO vo)
    {
        applyVOToModel(vo);
    }


    @Column(name = "u_key", length = 300)
    public String getUKey()
    {
        return UKey;
    }

    public void setUKey(String key)
    {
        this.UKey = key;
    }

    @Column(name = "u_value", length = 4000)
    public String getUValue()
    {
        return UValue;
    }

    public void setUValue(String value)
    {
        this.UValue = value;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_edge_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveEdge getEdge()
    {
        return edge;
    }

    public void setEdge(ResolveEdge edge)
    {
        this.edge = edge;
    }

    @Override
    public ResolveEdgePropertiesVO doGetVO()
    {
        ResolveEdgePropertiesVO vo = new ResolveEdgePropertiesVO();
        super.doGetBaseVO(vo);
        
        vo.setUKey(getUKey());
        vo.setUValue(getUValue());
        
        vo.setEdge(Hibernate.isInitialized(this.edge) && getEdge() != null ? new ResolveEdgeVO(this.getEdge().getSys_id()): null);
        
        return vo;
    }

    @Override
    public void applyVOToModel(ResolveEdgePropertiesVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUKey(StringUtils.isNotBlank(vo.getUKey()) && vo.getUKey().equals(VO.STRING_DEFAULT) ? getUKey() : vo.getUKey());
            this.setUValue(StringUtils.isNotBlank(vo.getUValue()) && vo.getUValue().equals(VO.STRING_DEFAULT) ? getUValue() : vo.getUValue());
            
            //references
            this.setEdge(vo.getEdge() != null ? new ResolveEdge(vo.getEdge().getSys_id()): getEdge());
            

        }
    }

}
