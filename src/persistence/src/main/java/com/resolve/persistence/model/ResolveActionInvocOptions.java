/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

// Generated Dec 11, 2009 11:15:20 AM by Hibernate Tools 3.2.4.GA

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.CallbackException;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.type.Type;

import com.resolve.esb.ESB;
import com.resolve.persistence.util.BlockingSIDListener;
import com.resolve.persistence.util.BlockingUListener;
import com.resolve.persistence.util.ResolveInterceptor;
import com.resolve.persistence.util.SIDListener;
import com.resolve.services.hibernate.vo.ResolveActionInvocOptionsVO;
import com.resolve.services.interfaces.CdataProperty;
import com.resolve.services.interfaces.IndexComponent;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

/**
 * ResolveActionInvocOptions generated by hbm2java
 */
@Entity
@Table(name = "resolve_action_invoc_options",  uniqueConstraints = {@UniqueConstraint(columnNames = {"u_name", "u_action_invocation"}, name = "rainvopt_u_name_u_act_inv_uk")},
        indexes = {@Index(columnList = "u_action_invocation", name = "rai_u_action_invocation_idx")}) 
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveActionInvocOptions  extends BaseModel<ResolveActionInvocOptionsVO> implements java.io.Serializable, CdataProperty, IndexComponent
{
    
    private static final long serialVersionUID = 184834544986325912L;
    
    private String UName;
    private String UValue;
    private String UDescription;
    private Boolean isAutoGenerated;

    // object references
    private ResolveActionInvoc invocation; // UActionInvocation

    // object referenced by

    public ResolveActionInvocOptions()
    {
    }

    public ResolveActionInvocOptions(ResolveActionInvocOptionsVO vo)
    {
        applyVOToModel(vo);
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_action_invocation", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveActionInvoc getInvocation()
    {
        return this.invocation;
    } // getInvocation

    public void setInvocation(ResolveActionInvoc invocation)
    {
        this.invocation = invocation;

        /*
        if (invocation != null)
        {
            Collection<ResolveActionInvocOptions> coll = invocation.getResolveActionInvocOptions();
            if (coll == null)
            {
				coll = new HashSet<ResolveActionInvocOptions>();
	            coll.add(this);
	            
                invocation.setResolveActionInvocOptions(coll);
            }
        }
        */
    } // setInvocation

    @Column(name = "u_name", length = 100)
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }

    @Lob
    @Column(name = "u_value", length = 16777215)
    public String getUValue()
    {
        return this.UValue;
    }

    public void setUValue(String UValue)
    {
        this.UValue = UValue;
    }

    @Column(name = "u_description", length = 4000)
    public String getUDescription()
    {
        return this.UDescription;
    }

    public void setUDescription(String UDescription)
    {
        this.UDescription = UDescription;
    }
    
    @Column(name="is_auto_generated")
    @org.hibernate.annotations.Type(type = "yes_no")
    public Boolean getIsAutoGenerated()
	{
		return isAutoGenerated;
	}

	public void setIsAutoGenerated(Boolean isAutoGenerated)
	{
		this.isAutoGenerated = isAutoGenerated;
	}

    @Override
    public String toString()
    {
        StringBuffer str = new StringBuffer();

        str.append("Option Name:").append(UName).append("\n");
        str.append("Option Value:").append(UValue).append("\n");
        str.append("Option Description:").append(UDescription).append("\n");

        return str.toString();
    }
    
    /**
     * method for getting a copy ...similar to clone()
     * 
     * @return
     */
    public ResolveActionInvocOptions copy()
    {
    	ResolveActionInvocOptions copy = new ResolveActionInvocOptions();
    	copy.setSys_id(getSys_id());
    	copy.setSysCreatedBy(getSysCreatedBy());
    	copy.setSysCreatedOn(getSysCreatedOn());
    	copy.setSysUpdatedBy(getSysUpdatedBy());
    	copy.setSysUpdatedOn(getSysUpdatedOn());
    	
    	copy.setUDescription(UDescription);
    	copy.setUName(UName);
    	copy.setUValue(UValue);
    	copy.setIsAutoGenerated(isAutoGenerated);

        return copy;
    }
    

    public static void initListener(final ESB mainESB)
    {
        //insert
        BlockingSIDListener insertListener = new BlockingSIDListener()
        {
            @Override
            public boolean onDataOperation(Object entity, Serializable id, Object[] values, String[] names, Type[] types) throws CallbackException
            {
//                System.out.println("Insert listener for ResolveActionInvocOptions called.");
                return true;
            }

        };
        ResolveInterceptor.addInsertAfterListener(ResolveActionInvocOptions.class.getName(), insertListener);
        
        //update listener
        BlockingUListener updateListener = new BlockingUListener()
        {
            /**
             * entity --> New object with the new updated data in it
             * id --> sys_id
             * state --> array of new values
             * previousState --> array of old values
             * propertyNames --> array of attributes
             * types --> array of types like String, Date, etc
             */
            @Override
            public boolean onUpdate(Object entity, Serializable id, Object[] state, Object[] previousState, String[] propertyNames, Type[] types)
                            throws CallbackException
            {
                
//                System.out.println("Update listener for ResolveActionInvocOptions called.");
                //this is for cleanup, so even if something goes wrong, it will not be very critical. Logs should indicate anyway.
                return true;
            }

        };
        ResolveInterceptor.addUpdateAfterListener(ResolveActionInvocOptions.class.getName(), updateListener);
        
        // remove listener
        SIDListener deleteListener = new SIDListener()
        {
            @Override
            public boolean onDataOperation(Object entity, Serializable id, Object[] values, String[] names, Type[] types) throws CallbackException
            {
//                System.out.println("Delete listener for ResolveActionInvocOptions called.");
                //this is for cleanup, so even if something goes wrong, it will not be very critical. Logs should indicate anyway.
                return true;
            }
        };
        ResolveInterceptor.addAsyncDeleteListener(ResolveActionInvocOptions.class.getName(), deleteListener);

        
    } // initListener

    @Override
    public List<String> ugetCdataProperty()
    {
        List<String> list = new ArrayList<String>();
        list.add("UValue");
        
        return list;
    }//ugetCdataProperty
    
    @Override
    public String ugetIndexContent()
    {
        return          UName
                + " " + UValue;
    }//ugetIndexContent

    @Override
    public ResolveActionInvocOptionsVO doGetVO()
    {
        ResolveActionInvocOptionsVO vo = new ResolveActionInvocOptionsVO();
        super.doGetBaseVO(vo);
        
        vo.setUDescription(getUDescription());
        vo.setUName(getUName());
        vo.setUValue(getUValue());
        vo.setIsAutoGenerated(getIsAutoGenerated());
        
        //references - SHOULD NOT DO THIS - YOU CAN COME FROM INVOC TO PARAMS BUT NOT THE OTHER WAY AROUND AS IT WILL BE INFINITE LOOP
//        vo.setInvocation(Hibernate.isInitialized(this.invocation) && getInvocation() != null ? getInvocation().doGetVO() : null);
        
        return vo;
    }

    @Override
    public void applyVOToModel(ResolveActionInvocOptionsVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);   
            
            this.setUDescription(StringUtils.isNotBlank(vo.getUDescription()) && vo.getUDescription().equals(VO.STRING_DEFAULT) ? getUDescription() : vo.getUDescription());
            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUValue(StringUtils.isNotBlank(vo.getUValue()) && vo.getUValue().equals(VO.STRING_DEFAULT) ? getUValue() : vo.getUValue());
            this.setIsAutoGenerated(vo.getIsAutoGenerated() != null ? vo.getIsAutoGenerated() : getIsAutoGenerated());
            
            //references
//            this.setInvocation(vo.getInvocation() != null ? new ResolveActionInvoc(vo.getInvocation()) : null);
            
        }
        
    }
    
	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}

		if (!(obj instanceof ResolveActionInvocOptions)) {
			return false;
		}

		ResolveActionInvocOptions atOpt = (ResolveActionInvocOptions) obj;

		return this.getSys_id().equals(atOpt.getSys_id());
	}
}
