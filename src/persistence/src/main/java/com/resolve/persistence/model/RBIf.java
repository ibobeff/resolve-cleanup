/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.resolve.services.hibernate.vo.RBIfVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
@Entity
@Table(name = "rb_if")
public class RBIf extends BaseModel<RBIfVO>
{
    private String description;
    private Double order;
    private String parentId;
    private String parentType; //e.g., "connection"

    public RBIf()
    {
    }

    public RBIf(RBIfVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_description", length = 256)
    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @Column(name = "u_order", precision=10, scale=2)
    public Double getOrder()
    {
        return order;
    }

    public void setOrder(Double order)
    {
        this.order = order;
    }

    @Column(name = "u_parent_id", length = 32)
    public String getParentId()
    {
        return parentId;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }
    
    @Column(name = "u_parent_type", length = 40)
    public String getParentType()
    {
        return parentType;
    }
    public void setParentType(String parentType)
    {
        this.parentType = parentType;
    }

    @Override
    public RBIfVO doGetVO()
    {
        RBIfVO vo = new RBIfVO();
        super.doGetBaseVO(vo);

        vo.setDescription(getDescription());
        vo.setOrder(getOrder());
        vo.setParentId(getParentId());
        vo.setParentType(getParentType());
        
        return vo;
    }

    @Override
    public void applyVOToModel(RBIfVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setDescription(StringUtils.isNotBlank(vo.getDescription()) && vo.getDescription().equals(VO.STRING_DEFAULT) ? getDescription() : vo.getDescription());
            this.setOrder(vo.getOrder() != null && vo.getOrder().equals(VO.DOUBLE_DEFAULT) ? getOrder() : vo.getOrder());
            this.setParentId(StringUtils.isNotBlank(vo.getParentId()) && vo.getParentId().equals(VO.STRING_DEFAULT) ? getParentId() : vo.getParentId());
            this.setParentType(StringUtils.isNotBlank(vo.getParentType()) && vo.getParentType().equals(VO.STRING_DEFAULT) ? getParentType() : vo.getParentType());
        }
    }
}
