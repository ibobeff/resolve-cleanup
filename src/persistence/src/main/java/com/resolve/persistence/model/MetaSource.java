/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.MetaSourceVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;


@Entity
@Table(name = "meta_source", indexes = {
                @Index(columnList = "u_meta_field_properties_sys_id", name = "ms_u_meta_field_prop_idx"),
                @Index(columnList = "u_meta_form_view_sys_id", name = "ms_u_meta_form_view_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaSource extends BaseModel<MetaSourceVO>
{   
    private static final long serialVersionUID = 7847246563327446688L;
    
    private String UName;
    private String UDisplayName;
    private String USource; 
    private String UType;
    private String UDbType;
    
    // object referenced
    private MetaFormView metaFormView;
//    private MetaAccessRights metaAccessRights;//will be based on view access rights 
    private MetaFieldProperties metaFieldProperties;
    
    public MetaSource()
    {
    }
    
    public MetaSource(MetaSourceVO vo)
    {
        applyVOToModel(vo);
    }

    
    @Column(name = "u_name", nullable = false, length = 100)
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    @Column(name = "u_source", length = 30)
    public String getUSource()
    {
        return this.USource;
    }

    public void setUSource(String USource)
    {
        this.USource = USource;
    }
    
    @Column(name = "u_type", length = 100)
    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }
    
    @Column(name = "u_dbtype", length = 100)
    public String getUDbType()
    {
        return this.UDbType;
    }

    public void setUDbType(String UDbType)
    {
        this.UDbType = UDbType;
    }
    
    @Column(name = "u_display_name", length = 100)
    public String getUDisplayName()
    {
        return this.UDisplayName;
    }

    public void setUDisplayName(String UDisplayName)
    {
        this.UDisplayName = UDisplayName;
    }
    
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_meta_field_properties_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaFieldProperties getMetaFieldProperties()
    {
        return this.metaFieldProperties;
    }

    public void setMetaFieldProperties(MetaFieldProperties metaFieldProperties)
    {
        this.metaFieldProperties = metaFieldProperties;
    }
    
    @ManyToOne(fetch = FetchType.EAGER) //THIS IS DONE ON PURPOSE
    @JoinColumn(name = "u_meta_form_view_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaFormView getMetaFormView()
    {
        return metaFormView;
    }

    public void setMetaFormView(MetaFormView metaFormView)
    {
        this.metaFormView = metaFormView;

        if (metaFormView != null)
        {
            Collection<MetaSource> coll = metaFormView.getMetaSources();
            if (coll == null)
            {
                coll = new HashSet<MetaSource>();
                coll.add(this);
                
                metaFormView.setMetaSources(coll);
            }
        }
    }

    @Override
    public MetaSourceVO doGetVO()
    {
        MetaSourceVO vo = new MetaSourceVO();
        super.doGetBaseVO(vo);
        
        vo.setUDbType(getUDbType());
        vo.setUDisplayName(getUDisplayName());
        vo.setUName(getUName());
        vo.setUSource(getUSource());
        vo.setUType(getUType());
        
        //refs
        vo.setMetaFieldProperties(Hibernate.isInitialized(this.metaFieldProperties) && getMetaFieldProperties() != null ? getMetaFieldProperties().doGetVO() : null) ;
        
        return vo;
    }

    @Override
    public void applyVOToModel(MetaSourceVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);    
            
            this.setUDbType(StringUtils.isNotBlank(vo.getUDbType()) && vo.getUDbType().equals(VO.STRING_DEFAULT) ? getUDbType() : vo.getUDbType());
            this.setUDisplayName(StringUtils.isNotBlank(vo.getUDisplayName())&& vo.getUDisplayName().equals(VO.STRING_DEFAULT) ? getUDisplayName() : vo.getUDisplayName());
            this.setUName(StringUtils.isNotBlank(vo.getUName())&& vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUSource(StringUtils.isNotBlank(vo.getUSource())&& vo.getUSource().equals(VO.STRING_DEFAULT) ? getUSource() : vo.getUSource());
            this.setUType(StringUtils.isNotBlank(vo.getUType())&& vo.getUType().equals(VO.STRING_DEFAULT) ? getUType() : vo.getUType());
            
            //refs
            this.setMetaFieldProperties(vo.getMetaFieldProperties() != null ? new MetaFieldProperties(vo.getMetaFieldProperties()) : getMetaFieldProperties());
            
        }
            
    }
    
    
    
}
