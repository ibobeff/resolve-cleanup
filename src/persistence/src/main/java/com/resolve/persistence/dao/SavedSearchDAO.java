package com.resolve.persistence.dao;

import com.resolve.persistence.model.SavedSearch;

public interface SavedSearchDAO extends GenericDAO<SavedSearch, String> {}
