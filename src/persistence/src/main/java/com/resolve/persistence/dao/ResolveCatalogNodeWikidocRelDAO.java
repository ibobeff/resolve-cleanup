package com.resolve.persistence.dao;

import com.resolve.persistence.model.ResolveCatalogNodeWikidocRel;

public interface ResolveCatalogNodeWikidocRelDAO extends GenericDAO<ResolveCatalogNodeWikidocRel, String>
{

}
