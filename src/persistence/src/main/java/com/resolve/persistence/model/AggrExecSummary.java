package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

//import org.hibernate.annotations.Cache;
//import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.resolve.services.hibernate.vo.AggrExecSummaryVO;
import com.resolve.services.interfaces.VO;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

import static com.resolve.util.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Entity
@Table(name = "aggr_exec_summary", 
       uniqueConstraints = {
                       @UniqueConstraint(columnNames = {"u_start_date", "u_org_id", "u_rbc_source"}, 
										 name = "aes_u_startdt_org_rbcsrc_uk")
       })
/*
 * Enable Hibernate L2 Cache only if it is distributed 
 * across all nodes in cluster.
 */
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
/*
 * Aggregated execution summary for a day.
 */

public class AggrExecSummary extends BaseModel<AggrExecSummaryVO> {
	
	private static final long serialVersionUID = 3657181729172666866L;

	public static final Long NO_USAGE_COUNT = VO.NON_NEGATIVE_LONG_DEFAULT;
	
	private Date startDate;				// start date (UTC)
	private String orgId;				// org id ("None" for No Org)
	private String rbcSource;			// RBC source ("None" for missing RBC source)
	private Long usageCount;			// usage count
	
	public AggrExecSummary() {
	    super();
	}
	
	public AggrExecSummary(AggrExecSummaryVO vo) {
        applyVOToModel(vo);
    }
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "u_start_date")
	public Date getStartDate() {
		return startDate;
	}
	
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	@Column(name = "u_org_id",  nullable = false, length = 32)
	public String getOrgId() {
		return orgId;
	}
	
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
	@Column(name = "u_rbc_source",  nullable = false, length = 128)
	public String getRbcSource() {
		return rbcSource;
	}
	
	public void setRbcSource(String rbcSource) {
		this.rbcSource = rbcSource;
	}
	
	@Column(name="u_usage_count")
	public Long getUsageCount() {
		return usageCount;
	}
	
	public Long ugetUsageCount() {
		if (getUsageCount() != null) {
			return getUsageCount();
		} else {
			return AggrExecSummaryVO.NO_USAGE_COUNT;
		}
	}
	
	public void setUsageCount(Long usageCount) {
		this.usageCount = usageCount;
	}
	
    
    @Override
	public AggrExecSummaryVO doGetVO() {
    	AggrExecSummaryVO vo = new AggrExecSummaryVO();
		
		super.doGetBaseVO(vo);
				
		vo.setStartDate(getStartDate());
		vo.setOrgId(getOrgId());
		vo.setRbcSource(getRbcSource());
		vo.setUsageCount(ugetUsageCount());
		
		return vo;
	}
	
	@Override
	public void applyVOToModel(AggrExecSummaryVO vo)
	{
		if (vo != null)
		{
			super.applyVOToModel(vo);
			
			setStartDate(vo.getStartDate()!= null && vo.getStartDate().equals(VO.DATE_DEFAULT) ? 
						 getStartDate() : vo.getStartDate());
			setOrgId(isNotBlank(vo.getOrgId()) && vo.getOrgId().equals(VO.STRING_DEFAULT) ? getOrgId() : vo.getOrgId());
			setRbcSource(isNotBlank(vo.getRbcSource()) && vo.getRbcSource().equals(VO.STRING_DEFAULT) ? 
						 getRbcSource() : vo.getRbcSource());
			setUsageCount(vo.getUsageCount() != null ? vo.getUsageCount() : ugetUsageCount());
		}
	}
	
	@Override
    public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat(AggrExecSummaryVO.START_DATE_FORMAT);
    	sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
    	
        return "Aggregated Execution Summary [" +
        	   "sysUpdatedOn: " + sdf.format(getSysUpdatedOn()) +
        	   ", Start Date: " + sdf.format(startDate) + 
        	   ", Org: " + (isNotBlank(orgId) ? orgId : "Blank or null") +
        	   ", RBC Source: " + (isNotBlank(rbcSource) ? rbcSource : "Blank or null") + 
        	   ", Usage Count: " + ugetUsageCount() +
               "]";
    }
}
