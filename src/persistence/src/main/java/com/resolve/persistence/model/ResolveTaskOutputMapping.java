/******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolveTaskOutputMappingVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "resolve_task_output_mapping")
public class ResolveTaskOutputMapping extends BaseModel<ResolveTaskOutputMappingVO>
{
	private static final long serialVersionUID = -4896554001475284387L;
	
	private String outputType;
	private String outputName;
	private String sourceType;
	private String sourceName;
	private String script;
	
	private ResolveActionInvoc invocation;
	
	public ResolveTaskOutputMapping()
	{
	}
	
	public ResolveTaskOutputMapping(ResolveTaskOutputMappingVO vo)
	{
		applyVOToModel(vo);
	}
	
	@Column(name = "u_output_type", length = 32)
	public String getOutputType()
	{
		return outputType;
	}

	public void setOutputType(String outputType)
	{
		this.outputType = outputType;
	}

	@Column(name = "u_output_name", length = 32)
	public String getOutputName()
	{
		return outputName;
	}
	public void setOutputName(String outputName)
	{
		this.outputName = outputName;
	}
	
	@Column(name = "u_source_type", length = 32)
	public String getSourceType()
	{
		return sourceType;
	}
	public void setSourceType(String sourceType)
	{
		this.sourceType = sourceType;
	}
	
	@Column(name = "u_source_name", length = 32)
	public String getSourceName()
	{
		return sourceName;
	}
	public void setSourceName(String sourceName)
	{
		this.sourceName = sourceName;
	}
	
	@Lob
	@Column(name = "u_script", length = 16777215)
	public String getScript()
	{
		return script;
	}

	public void setScript(String script)
	{
		this.script = script;
	}

	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_invocation", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
	public ResolveActionInvoc getInvocation()
	{
		return invocation;
	}

	public void setInvocation(ResolveActionInvoc invocation)
	{
		this.invocation = invocation;
	}

	@Override
	public ResolveTaskOutputMappingVO doGetVO()
	{
		ResolveTaskOutputMappingVO vo = new ResolveTaskOutputMappingVO();
		super.doGetBaseVO(vo);
		vo.setOutputType(this.getOutputType());
		vo.setOutputName(this.getOutputName());
		vo.setSourceName(this.getSourceName());
		vo.setSourceType(this.getSourceType());
		vo.setScript(this.getScript());
		
		return vo;
	}
	
	@Override
	public void applyVOToModel(ResolveTaskOutputMappingVO vo)
	{
		if (vo != null)
		{
			super.applyVOToModel(vo);
			this.setOutputType(StringUtils.isNotBlank(vo.getOutputType()) && vo.getOutputType().equals(VO.STRING_DEFAULT) ? getOutputType() : vo.getOutputType());
			this.setOutputName(StringUtils.isNotBlank(vo.getOutputName()) && vo.getOutputName().equals(VO.STRING_DEFAULT) ? getOutputName() : vo.getOutputName());
			this.setSourceType(StringUtils.isNotBlank(vo.getSourceType()) && vo.getSourceType().equals(VO.STRING_DEFAULT) ? getSourceType() : vo.getSourceType());
			this.setSourceName(StringUtils.isNotBlank(vo.getSourceName()) && vo.getSourceName().equals(VO.STRING_DEFAULT) ? getSourceName() : vo.getSourceName());
			this.setScript(StringUtils.isNotBlank(vo.getScript()) && vo.getScript().equals(VO.STRING_DEFAULT) ? getScript() : vo.getScript());
		}
	}
}
