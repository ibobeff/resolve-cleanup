/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import com.resolve.rsbase.MainBase;
import com.resolve.services.hibernate.vo.HTTPFilterVO;
import com.resolve.util.Log;

/**
 * HTTPFilter
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "http_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }, name = "httpf_u_name_u_queue_uk") })
public class HTTPFilter extends GatewayFilter<HTTPFilterVO>
{
    private Integer UPort;
    private String UUri;
    private Boolean USsl;
    private Integer UBlocking;
    private String UAllowedIP;
    private String UBasicAuthPassword;
    private String UBasicAuthUsername;
    //GAT-27 changes to accomodate content type and response filter
    private String UContentType;
    private String UResponsefilter;

    public HTTPFilter()
    {
    }

    public HTTPFilter(HTTPFilterVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_port")
    public Integer getUPort()
    {
        return UPort;
    }

    public void setUPort(Integer uPort)
    {
        UPort = uPort;
    }

    @Column(name = "u_uri")
    public String getUUri()
    {
        return UUri;
    }

    public void setUUri(String uUri)
    {
        UUri = uUri;
    }

    @Column(name = "u_ssl")
    public Boolean getUSsl()
    {
        return USsl;
    }

    public void setUSsl(Boolean uSsl)
    {
        USsl = uSsl;
    }
    
    @Column(name = "u_blocking")
    public Integer getUBlocking()
    {
        return UBlocking;
    }

    public void setUBlocking(Integer uBlocking)
    {
        UBlocking = uBlocking;
    }

    @Column(name = "u_allowed_ip")
    public String getUAllowedIP()
    {
        return UAllowedIP;
    }

    public void setUAllowedIP(String uAllowedIP)
    {
        UAllowedIP = uAllowedIP;
    }
    @Column(name = "u_basic_auth_pw")
    public String getUBasicAuthPassword()
    {
        return UBasicAuthPassword;
    }

    public void setUBasicAuthPassword(String uBasicAuthPassword)
    {
        UBasicAuthPassword = uBasicAuthPassword;
    }
    
    
    @Column(name = "u_basic_auth_un")
    public String getUBasicAuthUsername()
    {
        return UBasicAuthUsername;
    }

    public void setUBasicAuthUsername(String uBasicAuthUsername)
    {
        UBasicAuthUsername = uBasicAuthUsername;
    }
    
    
    @Column(name = "u_content_type")
    public String getUContentType() {
		return UContentType;
	}

	public void setUContentType(String uContentType) {
		UContentType = uContentType;
	}
	 @Column(name = "u_response_filter", length=10000)
	public String getUResponsefilter() {
		return UResponsefilter;
	}

	public void setUResponsefilter(String uResponsefilter) {
		UResponsefilter = uResponsefilter;
	}

	@Transient
    public String getUHttpAuthType()
    {
        
            try
            { 
                String authType =MainBase.main.configDoc.getStringValue("//FILTERAUTHTYPE/HTTP/@type",MainBase.main.configDoc.getStringValue("//HTTP/@HTTPAUTHTYPE")) ;
                
                return authType;
                
                
            }
            catch (Exception e)
            {
                Log.log.error("Could not determine enablement of HTTP BASIC Authentication", e);
               return "";
            }
        
         
    }
    
    @Override
    public HTTPFilterVO doGetVO()
    {
        HTTPFilterVO vo = new HTTPFilterVO();
        super.doGetBaseVO(vo);

        vo.setUPort(getUPort());
        vo.setUUri(getUUri());
        vo.setUSsl(getUSsl());
        vo.setUBlocking(getUBlocking());
        vo.setUAllowedIP(getUAllowedIP());
        vo.setU_bas_icAu_thPas_sword(getUBasicAuthPassword());
        vo.setU_bas_icAu_thUs_ername(getUBasicAuthUsername());
        
        vo.setUContentType(getUContentType());
        vo.setUResponsefilter(getUResponsefilter());
        return vo;
    }

    @Override
    public void applyVOToModel(HTTPFilterVO vo)
    {
        if (vo != null)
        {  
            super.applyVOToModel(vo);
            this.setUPort((vo.getUPort() != null && !HTTPFilterVO.INTEGER_DEFAULT.equals(vo.getUPort()))? vo.getUPort() : getUPort());
            this.setUUri(voGetValue(vo.getUUri(), getUUri()) );
            this.setUSsl(vo.getUSsl() != null ? vo.getUSsl() : getUSsl());
            this.setUBlocking(vo.getUBlocking() != null ? vo.getUBlocking() : getUBlocking());
            this.setUAllowedIP(voGetValue(vo.getUAllowedIP(), getUAllowedIP()));
          
            this.setUBasicAuthPassword(voGetValue(vo.getU_bas_icAu_thPas_sword(),getUBasicAuthPassword()));
            this.setUBasicAuthUsername(voGetValue(vo.getU_bas_icAu_thUs_ername(),getUBasicAuthUsername()));
         
            this.setUContentType(vo.getUContentType());
            this.setUResponsefilter(vo.getUResponsefilter());
        }
    }
    private static String voGetValue(String voValue,  String localValue)
    {
        if(voValue == null)
            return localValue; 
        
        return  voValue.equals(HTTPFilterVO.STRING_DEFAULT) ? localValue : voValue;
    }
    
}
