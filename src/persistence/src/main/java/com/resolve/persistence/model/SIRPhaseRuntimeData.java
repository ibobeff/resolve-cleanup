package com.resolve.persistence.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.Hibernate;
import org.hibernate.annotations.ColumnDefault;

//import org.hibernate.annotations.Cache;
//import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.resolve.services.hibernate.vo.SIRPhaseRuntimeDataVO;

import static com.resolve.services.interfaces.VO.*;

import static com.resolve.util.StringUtils.*;

@Entity
@Table(name = "sir_phase_runtimedata",
       uniqueConstraints = {
                       @UniqueConstraint(columnNames = {"u_resolve_security_incident_id", 
                                                        "u_sir_phase_metadata_id",
                                                        "u_position"}, 
                                         name = "sprd_rsi_phase_position_uk")
       })
/*
 * Enable Hibernate L2 Cache only if it is distributed 
 * accross all nodes in cluster.
 */
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SIRPhaseRuntimeData extends BaseModel<SIRPhaseRuntimeDataVO> {
    
    private static final long serialVersionUID = -3760608965381882503L;
    
    public static final Integer LAST_POSITION = Integer.MAX_VALUE;
    
    private Integer position;                       // Position of this Phase in SIR
    
    // object references
    private SIRPhaseMetaData phaseMetaData;         // SIR Phase Meta Data (fetched lezily)
    private ResolveSecurityIncident referencedRSI;  // Referenced Resolve Security Incident (fetched lazily)
	 
	public SIRPhaseRuntimeData() {
	    super();
	}
	
	public SIRPhaseRuntimeData(SIRPhaseRuntimeDataVO vo) {
        applyVOToModel(vo);
    }
	
	@Column(name = "u_position", nullable=false, updatable=true)
	@ColumnDefault(value = "2147483647")
	public Integer getPosition() {
	    return position;
	}
	
	public Integer ugetPosition() {
	    if (getPosition() != null && getPosition() > NON_NEGATIVE_INTEGER_DEFAULT) {
	        return getPosition();
	    } else {
	        return LAST_POSITION;
	    }
	}
	
	public void setPosition(Integer position) {
	    this.position = position;
	}
	
	@ManyToOne(optional = false, cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
	@JoinColumn(name = "u_sir_phase_metadata_id", referencedColumnName = "sys_id", nullable = false, updatable = true, 
	            foreignKey = @ForeignKey(name = "sprd_sir_phase_metadata_fk", value = ConstraintMode.CONSTRAINT))
	public SIRPhaseMetaData getPhaseMetaData() {
		return phaseMetaData;
	}
	
	public void setPhaseMetaData(SIRPhaseMetaData phaseMetaData) {
		this.phaseMetaData = phaseMetaData;
	}
	
	@ManyToOne(optional = false, cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @JoinColumn(name = "u_resolve_security_incident_id", referencedColumnName = "sys_id", nullable=false, updatable=false, 
                foreignKey = @ForeignKey(name = "sprd_rsi_fk", value = ConstraintMode.CONSTRAINT))
    public ResolveSecurityIncident getReferencedRSI() {
        return referencedRSI;
    }
    
    public void setReferencedRSI(ResolveSecurityIncident referencedRSI) {
        this.referencedRSI = referencedRSI;
    }
    
    @Override
	public SIRPhaseRuntimeDataVO doGetVO() {
		SIRPhaseRuntimeDataVO vo = new SIRPhaseRuntimeDataVO();
		
		super.doGetBaseVO(vo);
		
		vo.setPosition(getPosition());
		
		//references
		vo.setPhaseMetaData(Hibernate.isInitialized(phaseMetaData) && getPhaseMetaData() != null ? 
		                    getPhaseMetaData().doGetVO() : null);
		vo.setReferencedRSI(Hibernate.isInitialized(referencedRSI) && getReferencedRSI() != null ?
		                    getReferencedRSI().doGetVO() : null);
		
		return vo;
	}
	
	@Override
	public void applyVOToModel(SIRPhaseRuntimeDataVO vo)
	{
		if (vo != null && vo.getPhaseMetaData() != null && vo.getReferencedRSI() != null)
		{
			super.applyVOToModel(vo);
			
			setPosition(vo.ugetPosition() != null && vo.ugetPosition() <= NON_NEGATIVE_INTEGER_DEFAULT ? 
			            ugetPosition() : vo.ugetPosition());
			
			//references
			setPhaseMetaData(new SIRPhaseMetaData(vo.getPhaseMetaData()));
			setReferencedRSI(new ResolveSecurityIncident(vo.getReferencedRSI()));
		}
	}
	
	@Override
    public String toString() {
        return "SIR Phase Runtime Data [" +
               (isNotBlank(getSys_id()) ? "Sys Id: " + getSys_id() + ", " : "") + 
               "Sys Org: " + (isNotBlank(getSysOrg()) ? getSysOrg() : "") + 
               ", Position: " + ugetPosition() + 
               ", Phase Meta Data: " + getPhaseMetaData() + 
               ", Referenced Rsolve Security Incident: " + getReferencedRSI() + "]";
    }
}
