/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

// Generated Dec 10, 2009 7:09:50 PM by Hibernate Tools 3.2.4.GA

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.hibernate.CallbackException;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;

import com.resolve.esb.ESB;
import com.resolve.persistence.util.BlockingSIDListener;
import com.resolve.persistence.util.BlockingUListener;
import com.resolve.persistence.util.ResolveInterceptor;
import com.resolve.persistence.util.SIDListener;
import com.resolve.services.hibernate.vo.ResolveActionTaskResolveTagRelVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.interfaces.IndexComponent;
import com.resolve.services.interfaces.UIDisplayAPI;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

/**
 * ResolveActionTask generated by hbm2java
 */
@Entity
@Table(name = "resolve_action_task",  uniqueConstraints = {@UniqueConstraint(columnNames = {"u_name", "u_namespace"}, name = "ratask_u_name_u_namespace_uk")},
        indexes = {
                @Index(columnList = "u_name", name = "rat_u_name_idx"),
                @Index(columnList = "u_namespace", name = "rat_u_namespace_idx"),
                @Index(columnList = "u_fullname", name = "rat_u_fullname_idx"),
                @Index(columnList = "u_preprocess", name = "rat_u_preprocess_idx"),
                @Index(columnList = "u_invocation", name = "rat_u_invocation_idx"),
                @Index(columnList = "u_accessrights", name = "rat_u_accessrights_idx")}) 
@org.hibernate.annotations.Table(appliesTo = "resolve_action_task")
public class ResolveActionTask extends BaseModel<ResolveActionTaskVO> implements UIDisplayAPI, IndexComponent
{
    private static final long serialVersionUID = 503905684957366515L; 

    public final static String RESOURCE_TYPE = "actiontask";

    private String UName;
    private String UNamespace;
    private String UFullName;
 	private String UTypes;
    private Boolean ULogresult;
    private String USummary;
    private String UDescription;
    private String URoles;//this will be same as UExecuteRoles
    private Boolean UActive;
    private String UMenuPath;
    private Boolean UIsDeleted;
    private Boolean UIsDefaultRole;
    private Boolean UIsHidden;
    private String wizardType;

    // transient
    private String UReadRoles;
    private String UWriteRoles;
    private String UAdminRoles;
    private String UExecuteRoles;
    
    // object references
    private ResolvePreprocess preprocess;           // UPreprocess;

    // object referenced by
    private ResolveActionInvoc resolveActionInvoc;  // UInvocation
    
    private Users assignedTo;                       // UAssignedTo;
    private AccessRights accessRights;

    private Collection<ResolveActionTaskResolveTagRel> atTagRels;
    
    public ResolveActionTask()
    {
    }

    public ResolveActionTask(String sysId)
    {
        super.setSys_id(sysId);
    }
    
    public ResolveActionTask(ResolveActionTaskVO vo)
    {
        applyVOToModel(vo);
    }
    
    @Transient
    public String getUReadRoles()
    {
        return UReadRoles;
    }

    public void setUReadRoles(String readRoles)
    {
        UReadRoles = readRoles;
    }

    @Transient
    public String getUWriteRoles()
    {
        return UWriteRoles;
    }

    public void setUWriteRoles(String writeRoles)
    {
        UWriteRoles = writeRoles;
    }

    @Transient
    public String getUAdminRoles()
    {
        return UAdminRoles;
    }

    public void setUAdminRoles(String adminRoles)
    {
        UAdminRoles = adminRoles;
    }

    @Transient
    public String getUExecuteRoles()
    {
        return UExecuteRoles;
    }

    public void setUExecuteRoles(String executeRoles)
    {
        UExecuteRoles = executeRoles; 
    }

    public ResolveActionTask usetsys_id(String sys_id)
    {
        this.setSys_id(sys_id);
        return this;
    }

    @Column(name = "u_name", length = 200)
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }

    @Column(name = "u_namespace", length = 100)
    public String getUNamespace()
    {
        return this.UNamespace;
    }

    public void setUNamespace(String UNamespace)
    {
        this.UNamespace = UNamespace;
    }

    @Column(name = "u_fullname", length = 300)
    public String getUFullName()
	{
		return UFullName;
	}

	public void setUFullName(String fullName)
	{
		this.UFullName = fullName;
	}

    
    @Column(name = "u_types", length = 100)
    public String getUTypes()
    {
        return this.UTypes;
    }

    public void setUTypes(String UTypes)
    {
        this.UTypes = UTypes;
    }

    @Column(name = "u_logresult")
    public Boolean getULogresult()
    {
        return this.ULogresult;
    }

    public Boolean ugetULogresult()
    {
        if (this.ULogresult == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.ULogresult;
        }
    } // ugetULogresult

    public void setULogresult(Boolean ULogresult)
    {
        this.ULogresult = ULogresult;
    }

    @Column(name = "u_summary", length = 300)
    public String getUSummary()
    {
        return this.USummary;
    }

    public void setUSummary(String USummary)
    {
        this.USummary = USummary;
    }

    @Column(name = "u_description", length = 4000)
    public String getUDescription()
    {
        return this.UDescription;
    }

    public void setUDescription(String UDescription)
    {
        String d = UDescription;
        if (UDescription!=null && UDescription.length()>2000)
        {
            d = UDescription.substring(0, 2000-1);
        }
        this.UDescription = d;
    }

    @Column(name = "u_roles", length = 1000)
    public String getURoles()
    {
        return this.URoles;
    }

    public void setURoles(String URoles)
    {
        this.URoles = URoles;
    }

    @Column(name = "u_active")
    public Boolean getUActive()
    {
        return this.UActive;
    } // getUActive

    public Boolean ugetUActive()
    {
        if (this.UActive == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UActive;
        }
    } // getUActive

    public void setUActive(Boolean UActive)
    {
        this.UActive = UActive;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_preprocess", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolvePreprocess getPreprocess()
    {
        return preprocess;
    }

    public void setPreprocess(ResolvePreprocess preprocess)
    {
        this.preprocess = preprocess;

        if (preprocess != null)
        {
            Collection<ResolveActionTask> coll = preprocess.getResolveActionTasks();
            if (coll == null)
            {
				coll = new HashSet<ResolveActionTask>();
	            coll.add(this);
	            
                preprocess.setResolveActionTasks(coll);
            }
        }
    }

    @Column(name = "u_menu_path", length = 100)
    public String getUMenuPath()
    {
        return this.UMenuPath;
    }

    public void setUMenuPath(String UMenuPath)
    {
        this.UMenuPath = UMenuPath;
    }
    
    @Column(name = "u_is_default_role", length = 1)
    @Type(type = "yes_no")
    public Boolean getUIsDefaultRole()
    {
            return UIsDefaultRole;
    }
    
    public Boolean ugetUIsDefaultRole()
    {
        if (this.UIsDefaultRole == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UIsDefaultRole;
        }
    } // ugetUIsDefaultRole

    public void setUIsDefaultRole(Boolean isDefaultRole)
    {
        UIsDefaultRole = isDefaultRole;
    }
    

    @Column(name = "u_is_deleted", length = 1)
    @Type(type = "yes_no")
    public Boolean getUIsDeleted()
    {
            return UIsDeleted;
    }
    
    public Boolean ugetUIsDeleted()
    {
        if (this.UIsDeleted == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UIsDeleted;
        }
    } // ugetUIsDeleted

    public void setUIsDeleted(Boolean isDeleted)
    {
        UIsDeleted = isDeleted;
    }
    
    @Column(name = "u_is_hidden", length = 1)
    @Type(type = "yes_no")
    public Boolean getUIsHidden()
    {
        return UIsHidden;
    }

    public void setUIsHidden(Boolean uIsHidden)
    {
        UIsHidden = uIsHidden;
    }
    
    @Column (name = "u_wizard_type", length = 50)
    public String getWizardType()
	{
		return wizardType;
	}
	public void setWizardType(String wizardType)
	{
		this.wizardType = wizardType;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "u_invocation", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveActionInvoc getResolveActionInvoc()
    {
        return this.resolveActionInvoc;
    } // getResolveActionInvoc

    public void setResolveActionInvoc(ResolveActionInvoc actionInvocation)
    {
        this.resolveActionInvoc = actionInvocation;
    } // setResolveActionInvoc
    
    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "u_assigned_to", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
	public Users getAssignedTo()
	{
		return assignedTo;
	}

	public void setAssignedTo(Users assignedTo)
	{
		this.assignedTo = assignedTo;
		
        // one-way only - no collections on the other side
	}

	@OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_accessrights", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade({CascadeType.DELETE})
    public AccessRights getAccessRights()
    {
        return accessRights;
    }
	
	public void setAccessRights(AccessRights accessRights)
    {
        this.accessRights = accessRights;
        
        // one-way only - no collections on the other side
    }

    // // ResolveActionTask-ResolveExecuteRequest relationship
    // @OneToMany(mappedBy = "task")
    // public Collection<ResolveExecuteRequest> getResolveExecuteRequests()
    // {
    // return resolveExecuteRequests;
    // }
    //
    // public void setResolveExecuteRequests(Collection<ResolveExecuteRequest>
    // resolveExecuteRequests)
    // {
    // this.resolveExecuteRequests = resolveExecuteRequests;
    // }

    // // ResolveActionTask-ResolveActionResult relationship
    // @OneToMany(mappedBy = "task")
    // public Collection<ResolveActionResult> getResolveActionResults()
    // {
    // return resolveActionResults;
    // }
    //
    // public void setResolveActionResults(Collection<ResolveActionResult>
    // resolveActionResults)
    // {
    // this.resolveActionResults = resolveActionResults;
    // }

    // ResolveActionTask-ResolveActionTaskResolveTagRel relationship
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "task")
	@NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.DELETE)
    public Collection<ResolveActionTaskResolveTagRel> getAtTagRels()
    {
        return atTagRels;
    }

    public void setAtTagRels(Collection<ResolveActionTaskResolveTagRel> atTagRels)
    {
        this.atTagRels = atTagRels;
    }

    @Override
    public boolean equals(Object otherObj)
    {
        boolean isEquals = false;
        if (otherObj instanceof ResolveActionTask)
        {
            ResolveActionTask otherTask = (ResolveActionTask) otherObj;
            // if sys_ids are not equal, they are diff
            if (otherTask.getSys_id().equals(this.getSys_id()))
            {
                isEquals = true;
            }
        }
        return isEquals;
    }

    @Override
    public int hashCode()
    {
        int hash = 1;
        hash = hash * 31 + this.UName.hashCode() + this.UNamespace.hashCode();
        hash = hash * 31 + (this.getSys_id() == null ? 0 : this.getSys_id().hashCode());
        return hash;
    }

    @Override
    public String toString()
    {
        StringBuffer str = new StringBuffer();

        str.append("Action Task Namespace:").append(UNamespace).append("\n");
        str.append("Action Task Name:").append(UName).append("\n");
        str.append("Action Task Summary:").append(USummary).append("\n");

        return str.toString();
    }

	@Override
	public String ui_getDisplayName()
	{
		return UNamespace + "." + UName;
	}

	@Override
	public String ui_getDisplayType()
	{
		return "Action Task";
	}

    @Override
    public String ugetIndexContent()
    {
        return      UName
            + " " + UNamespace
            + " " + UFullName
            + " " + UDescription
            + " " + USummary;
    }//ugetIndexContent
    
    public static void initListener(final ESB mainESB)
    {
        //insert
        BlockingSIDListener insertAfterListener = new BlockingSIDListener()
        {
            @Override
            public boolean onDataOperation(Object entity, Serializable id, Object[] state, String[] propertyNames, org.hibernate.type.Type[] types)
                            throws CallbackException
            {
//                System.out.println("Insert listener for ResolveActionTask called.");
                return false;
            }

        };
        ResolveInterceptor.addInsertAfterListener(ResolveActionTask.class.getName(), insertAfterListener);
        
        //update listener
        BlockingUListener updateAfterListener = new BlockingUListener()
        {
            /**
             * entity --> New object with the new updated data in it
             * id --> sys_id
             * state --> array of new values
             * previousState --> array of old values
             * propertyNames --> array of attributes
             * types --> array of types like String, Date, etc
             */
            @Override
            public boolean onUpdate(Object entity, Serializable id, Object[] currentState, Object[] previousState, String[] propertyNames,
                            org.hibernate.type.Type[] types) throws CallbackException
            {
//                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>   Update listener for ResolveActionTask called.");
                //this is for cleanup, so even if something goes wrong, it will not be very critical. Logs should indicate anyway.
                return false;
            }

        };
        ResolveInterceptor.addUpdateAfterListener(ResolveActionTask.class.getName(), updateAfterListener);
        
        // remove listener
        SIDListener deleteListener = new SIDListener()
        {
            @Override
            public boolean onDataOperation(Object entity, Serializable id, Object[] state, String[] propertyNames, org.hibernate.type.Type[] types)
                            throws CallbackException
            {
//                System.out.println("Delete listener for ResolveActionTask called.");
                //this is for cleanup, so even if something goes wrong, it will not be very critical. Logs should indicate anyway.
                return false;
            }
        };
        ResolveInterceptor.addAsyncDeleteListener(ResolveActionTask.class.getName(), deleteListener);

        
    } // initListener

    @Override
    public ResolveActionTaskVO doGetVO()
    {
        ResolveActionTaskVO vo = new ResolveActionTaskVO();
        super.doGetBaseVO(vo);
        
        vo.setUActive(getUActive());
        vo.setUDescription(getUDescription());
        vo.setUFullName(getUFullName());
        vo.setUIsDefaultRole(getUIsDefaultRole());
        vo.setUIsDeleted(getUIsDeleted());
        vo.setUIsHidden(getUIsHidden());
        vo.setULogresult(getULogresult());
        vo.setUMenuPath(getUMenuPath());
        vo.setUName(getUName());
        vo.setUNamespace(getUNamespace());
        vo.setURoles(getURoles());
        vo.setUSummary(getUSummary());
        vo.setUTypes(getUTypes());
        vo.setWizardType(this.getWizardType());
        
        //references
        vo.setAccessRights(Hibernate.isInitialized(this.accessRights) && getAccessRights() != null ? getAccessRights().doGetVO() : null);
        vo.setAssignedTo(Hibernate.isInitialized(this.assignedTo) && getAssignedTo() != null ? getAssignedTo().doGetVO(false) : null);
        if(vo.getAssignedTo() != null)
        {
            vo.getAssignedTo().setUImage(null);
            vo.getAssignedTo().setUTempImage(null);
            vo.getAssignedTo().setUUserP_assword(null);
        }
//        vo.setPreprocess(Hibernate.isInitialized(this.preprocess) && getPreprocess() != null ? getPreprocess().doGetVO() : null);
        vo.setResolveActionInvoc(Hibernate.isInitialized(this.resolveActionInvoc) && getResolveActionInvoc() != null ? getResolveActionInvoc().doGetVO() : null);
        
        //wikidoc-tag rel
        Collection<ResolveActionTaskResolveTagRel> atTagRels = Hibernate.isInitialized(this.atTagRels) ? getAtTagRels() : null;
        if(atTagRels != null && atTagRels.size() > 0)
        {
            Collection<ResolveActionTaskResolveTagRelVO> vos = new ArrayList<ResolveActionTaskResolveTagRelVO>();
            for(ResolveActionTaskResolveTagRel model : atTagRels)
            {
                vos.add(model.doGetVO());
            }
            vo.setAtTagRels(vos);
            
            //update the tags
            Set<String> tags = new HashSet<String>();
            for(ResolveActionTaskResolveTagRelVO relVO : vos)
            {
                tags.add(relVO.getTag().getUName());
            }
            vo.setTags(tags);
        }
        
        return vo;
    }

    @Override
    public void applyVOToModel(ResolveActionTaskVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUDescription(StringUtils.isNotBlank(vo.getUDescription()) && vo.getUDescription().equals(VO.STRING_DEFAULT) ? getUDescription() : vo.getUDescription());
            this.setUFullName(StringUtils.isNotBlank(vo.getUFullName()) && vo.getUFullName().equals(VO.STRING_DEFAULT) ? getUFullName() : vo.getUFullName());
            this.setUMenuPath(StringUtils.isNotBlank(vo.getUMenuPath()) && vo.getUMenuPath().equals(VO.STRING_DEFAULT) ? getUMenuPath() : vo.getUMenuPath());
            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUNamespace(StringUtils.isNotBlank(vo.getUNamespace()) && vo.getUNamespace().equals(VO.STRING_DEFAULT) ? getUNamespace() : vo.getUNamespace());
            this.setURoles(StringUtils.isNotBlank(vo.getURoles()) && vo.getURoles().equals(VO.STRING_DEFAULT) ? getURoles() : vo.getURoles());
            this.setUSummary(StringUtils.isNotBlank(vo.getUSummary()) && vo.getUSummary().equals(VO.STRING_DEFAULT) ? getUSummary() : vo.getUSummary());
            this.setUTypes(StringUtils.isNotBlank(vo.getUTypes()) && vo.getUTypes().equals(VO.STRING_DEFAULT) ? getUTypes() : vo.getUTypes());

            this.setUActive(vo.getUActive() != null ? vo.getUActive() : getUActive());
            this.setULogresult(vo.getULogresult() != null ? vo.getULogresult() : getULogresult());
            this.setUIsDefaultRole(vo.getUIsDefaultRole() != null ? vo.getUIsDefaultRole() : getUIsDefaultRole());
            this.setUIsDeleted(vo.getUIsDeleted() != null ? vo.getUIsDeleted() : getUIsDeleted());
            this.setUIsHidden(vo.getUIsHidden() != null ? vo.getUIsHidden() : getUIsHidden());
            this.setWizardType(StringUtils.isNotBlank(vo.getWizardType()) && vo.getWizardType().equals(VO.STRING_DEFAULT) ? getWizardType() : vo.getWizardType());

            //references
            this.setAccessRights(vo.getAccessRights() != null ? new AccessRights(vo.getAccessRights()) : null);
            this.setAssignedTo(vo.getAssignedTo() != null ? new Users(vo.getAssignedTo()) : null);
//            this.setPreprocess(vo.getPreprocess() != null ? new ResolvePreprocess(vo.getPreprocess()) : null);
            this.setResolveActionInvoc(vo.getResolveActionInvoc() != null ? new ResolveActionInvoc(vo.getResolveActionInvoc()) : null);
            
        }
        
    }

} // ResolveActionTask
