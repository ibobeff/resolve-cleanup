package com.resolve.persistence.model;

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.PushGatewayPropertiesAttrVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "push_gateway_properties_attr", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_push_gateway_properties_sys_id" }, name = "pushgpa_name_val_propid_uk")},
        indexes = {@Index(columnList = "u_push_gateway_properties_sys_id", name = "u_push_gateway_properties_sys_idx")})
public class PushGatewayPropertiesAttr extends BaseModel<PushGatewayPropertiesAttrVO>
{
    private static final long serialVersionUID = 1L;

    private String UName;
    private String UValue;

    // object reference
    private PushGatewayProperties pushGatewayProperties;

    public PushGatewayPropertiesAttr() {
    }

    public PushGatewayPropertiesAttr(PushGatewayPropertiesAttrVO vo) {
        applyVOToModel(vo);
    }

    @Column(name = "u_name", length = 255)
    public String getUName()
    {
        return UName;
    }
    public void setUName(String uName)
    {
        UName = uName;
    }

    @Column(name = "u_value", length = 4000)
    public String getUValue()
    {
        return UValue;
    }
    public void setUValue(String uValue)
    {
        UValue = uValue;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_push_gateway_properties_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.EXCEPTION)
    public PushGatewayProperties getPushGatewayProperties()
    {
        return pushGatewayProperties;
    }

    public void setPushGatewayProperties(PushGatewayProperties pushGatewayProperties) {

        this.pushGatewayProperties = pushGatewayProperties;

        if (pushGatewayProperties != null)
        {
            Collection<PushGatewayPropertiesAttr> coll = pushGatewayProperties.getAttrs();

            if (coll == null) {
                coll = new HashSet<PushGatewayPropertiesAttr>();
                coll.add(this);

                pushGatewayProperties.setAttrs(coll);
            }
        }
    }

    @Override
    public PushGatewayPropertiesAttrVO doGetVO()
    {
        PushGatewayPropertiesAttrVO vo = new PushGatewayPropertiesAttrVO();
        super.doGetBaseVO(vo);

        vo.setUName(getUName());
        vo.setUValue(getUValue());

        return vo;
    }

    @Override
    public void applyVOToModel(PushGatewayPropertiesAttrVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUValue(StringUtils.isNotBlank(vo.getUValue()) && vo.getUValue().equals(VO.STRING_DEFAULT) ? getUValue() : vo.getUValue());
        }
    }

} // class PushGatewayPropoertiesAttr
