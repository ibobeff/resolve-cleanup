/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "resolve_sequence_generator")
public class ResolveSequenceGenerator implements java.io.Serializable
{
    private static final long serialVersionUID = 3709661053422542243L;
    private String sys_id;
    private String UName;
    private Integer UNumber;

    public ResolveSequenceGenerator()
    {
    }

    public ResolveSequenceGenerator(String sys_id)
    {
        this.sys_id = sys_id;
    }

    public ResolveSequenceGenerator(String sys_id, String UName, Integer UNumber)
    {
        this.sys_id = sys_id;
        this.UName = UName;
        this.UNumber = UNumber;
    }

    @Id
    @Column(name = "sys_id",  nullable = false, length = 32)
    @GeneratedValue(generator = "hibernate-uuid")
    public String getsys_id()
    {
        return this.sys_id;
    }

    public void setsys_id(String sys_id)
    {
        this.sys_id = sys_id;
    }

    @Column(name = "u_name", length = 100)
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }

    @Column(name = "u_number")
    public Integer getUNumber()
    {
        return this.UNumber;
    }

    public void setUNumber(Integer number)
    {
        this.UNumber = number;
    }

}
