package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.annotations.CData;
import com.resolve.services.hibernate.vo.PullGatewayFilterAttrVO;
import com.resolve.services.interfaces.VO;

@Entity
@Table(name = "pull_gateway_filter_attr", 
	   uniqueConstraints = {
			   			       @UniqueConstraint(columnNames = { "u_name", "pull_filter_id" }, 
			   			    		   			 name = "pullgfa_name_val_fltrid_uk") 
			   			   },
	   indexes = {
			         @Index(columnList = "pull_filter_id", name = "pullgfa_fltrid_idx")
	   			 })
public class PullGatewayFilterAttr extends BaseModel<PullGatewayFilterAttrVO> {

    private static final long serialVersionUID = 1L;

    public PullGatewayFilterAttr() {
    }

    public PullGatewayFilterAttr(PullGatewayFilterAttrVO vo) {
        applyVOToModel(vo);
    }

    private String UName;
    
    @CData
    private String UValue;
    private String UPullFilterId;

    @Column(name = "u_name", length = 255)
    public String getUName()
    {
        return UName;
    }

    public void setUName(String uName)
    {
        UName = uName;
    }

    @Column(name = "u_value", length = 12000)
    public String getUValue()
    {
        return UValue;
    }

    public void setUValue(String uValue)
    {
        UValue = uValue;
    }

    @Column(name = "pull_filter_id", length = 32)
    public String getUPullFilterId()
    {
        return UPullFilterId;
    }
    
    public void setUPullFilterId(String uPullFilterId) {
        UPullFilterId = uPullFilterId;
    }

    public PullGatewayFilterAttrVO doGetVO() {
        
        PullGatewayFilterAttrVO vo = new PullGatewayFilterAttrVO();
        
        super.doGetBaseVO(vo);

        vo.setUName(getUName());
        vo.setUValue(getUValue());
        vo.setUPullFilterId(getUPullFilterId());
        
        return vo;
    }

    @Override
    public void applyVOToModel(PullGatewayFilterAttrVO vo) {
        
        if (vo == null) return;
        
        super.applyVOToModel(vo);

        if (!VO.STRING_DEFAULT.equals(vo.getUName())) this.setUName(vo.getUName()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUValue())) this.setUValue(vo.getUValue()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUPullFilterId())) this.setUPullFilterId(vo.getUPullFilterId()); else ;
    }
    
} // class PullGatewayFilterAttrVO