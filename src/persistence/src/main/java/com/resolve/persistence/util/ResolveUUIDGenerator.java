/******************************************************************************
* (C) Copyright 2018
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import java.io.Serializable;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.UUIDHexGenerator;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;

/**
 * Resolve Id Generator.
 * 
 * Generator which uses non null id set in the object. 
 * If id set in the object is null then falls back to Hibernate UUID. 
 * 
 * @author hemant.phanasgaonkar
 *
 */

public class ResolveUUIDGenerator extends UUIDHexGenerator {

    private String entityName;
    
    @Override
    public void configure(Type type, Properties params, ServiceRegistry serviceRegistry) throws MappingException {
        entityName = params.getProperty(ENTITY_NAME);
        super.configure(type, params, serviceRegistry);
    }
    
    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {        
        Serializable id = session.getEntityPersister(entityName, object).getIdentifier(object, session);
        
        if (id == null) {
            return super.generate(session, object);
        } else {
            return id;
        }
    }
}