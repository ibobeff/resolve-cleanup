package com.resolve.persistence.dao;

import com.resolve.persistence.model.ResolveCatalogNodeTagRel;

public interface ResolveCatalogNodeTagRelDAO extends GenericDAO<ResolveCatalogNodeTagRel, String>
{

}
