package com.resolve.persistence.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolveNamespaceVO;

@Entity
@Table(name = "namespace")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveNamespace extends BaseModel<ResolveNamespaceVO> {

	private static final long serialVersionUID = -9180983179865503009L;
	
	private String UName;
	private AccessRights accessRights;
	private ResolveNamespace parentNamespace;
	private Set<ResolveNamespace> childNamespaces;
	
    public ResolveNamespace() {
		super();
	}

	public ResolveNamespace(ResolveNamespaceVO vo) {
		applyVOToModel(vo);
	}

	@Column(name = "u_name", nullable = false)
	public String getUName() {
		return UName;
	}

	public void setUName(String uName) {
		UName = uName;
	}

	@OneToOne(fetch = FetchType.EAGER, cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "u_accessrights", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
	public AccessRights getAccessRights() {
		return accessRights;
	}

	public void setAccessRights(AccessRights accessRights) {
		this.accessRights = accessRights;
	}

	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
	@JoinColumn(name = "parent_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
	public ResolveNamespace getParentNamespace() {
		return parentNamespace;
	}

	public void setParentNamespace(ResolveNamespace parentNamespace) {
		this.parentNamespace = parentNamespace;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "parentNamespace")
	public Set<ResolveNamespace> getChildNamespaces() {
		return childNamespaces;
	}

	public void setChildNamespaces(Set<ResolveNamespace> childNamespaces) {
		this.childNamespaces = childNamespaces;
	}

	@Override
	public ResolveNamespaceVO doGetVO() {
		ResolveNamespaceVO vo = new ResolveNamespaceVO();
        super.doGetBaseVO(vo);

        vo.setUName(this.getUName());
        
        if (this.getAccessRights() != null) {
            vo.setAccessRights(this.getAccessRights().doGetVO());
        }
        
        ResolveNamespace parent = this.getParentNamespace();
        ResolveNamespaceVO currentVO = vo;
        // recursively fill the parents up
        while (parent != null) {
        	ResolveNamespaceVO parentVO = new ResolveNamespaceVO();
        	parentVO.setUName(parent.getUName());

        	if (parent.getAccessRights() != null) {
            	parentVO.setAccessRights(parent.getAccessRights().doGetVO());
        	}
        	currentVO.setParentNamespace(parentVO);
        	
        	parent = parent.getParentNamespace();
        	currentVO = parentVO;
        }
        
        Set<ResolveNamespaceVO> namespaces = new HashSet<>();
        if (this.getChildNamespaces() != null) {
            for (ResolveNamespace ns : this.getChildNamespaces()) {
            	namespaces.add(ns.doGetVO());
            }
            vo.setChildNamespaces(namespaces);
        }
        
        return vo;
	}

	@Override
	public void applyVOToModel(ResolveNamespaceVO vo) {
		super.applyVOToModel(vo);
		if (vo.getAccessRights() != null) {
			this.setAccessRights(new AccessRights(vo.getAccessRights()));
		}
		
		this.setUName(vo.getUName());
		ResolveNamespaceVO parentVO = vo.getParentNamespace();
        ResolveNamespace current = this;

		while (parentVO != null) {
        	ResolveNamespace parent = new ResolveNamespace();
        	parent.setUName(parentVO.getUName());

        	if (parentVO.getAccessRights() != null) {
            	parent.setAccessRights(new AccessRights(parentVO.getAccessRights()));
        	}
        	current.setParentNamespace(parent);
        	
        	parentVO = parentVO.getParentNamespace();
        	current = parent;
		}
	}
}
