package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.resolve.services.hibernate.vo.SavedQueryFilterVO;

@Entity
@Table(name = "saved_filter")
public class SavedQueryFilter extends BaseModel<SavedQueryFilterVO> {
	private static final long serialVersionUID = 5641081701189998890L;
    private String type;
    private String value;
    private String startValue;
    private String endValue;
    private String field;
    private String condition;
    private String paramName;
    private Boolean caseSensitive;
    private int startCount;
    private SavedSearch savedSearch;

	@Column(name = "filter_type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "value")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Column(name = "start_value")
	public String getStartValue() {
		return startValue;
	}

	public void setStartValue(String startValue) {
		this.startValue = startValue;
	}

	@Column(name = "end_value")
	public String getEndValue() {
		return endValue;
	}

	public void setEndValue(String endValue) {
		this.endValue = endValue;
	}

	@Column(name = "filter_field")
	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	@Column(name = "filter_condition")
	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	@Column(name = "param_name")
	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	@Column(name = "case_sensitive")
	public Boolean getCaseSensitive() {
		return caseSensitive;
	}

	public void setCaseSensitive(Boolean caseSensitive) {
		this.caseSensitive = caseSensitive;
	}

	@Column(name = "start_count")
	public int getStartCount() {
		return startCount;
	}

	public void setStartCount(int startCount) {
		this.startCount = startCount;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "saved_search_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
	public SavedSearch getSavedSearch() {
		return savedSearch;
	}

	public void setSavedSearch(SavedSearch savedSearch) {
		this.savedSearch = savedSearch;
	}

	@Override
	public SavedQueryFilterVO doGetVO() {
		SavedQueryFilterVO vo = new SavedQueryFilterVO();
        super.doGetBaseVO(vo);

        vo.setCaseSensitive(this.getCaseSensitive());
		vo.setCondition(this.getCondition());
		vo.setEndValue(this.getEndValue());
		vo.setField(this.getField());
		vo.setParamName(this.getParamName());
		vo.setStartCount(this.getStartCount());
		vo.setStartValue(this.getStartValue());
		vo.setType(this.getType());
		vo.setValue(this.getValue());
		
		return vo;
	}
}
