/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolveCatalogEdgeVO;
import com.resolve.services.hibernate.vo.ResolveCatalogNodeVO;
import com.resolve.services.hibernate.vo.ResolveCatalogVO;
import com.resolve.services.interfaces.VO;

@Entity
@Table(name = "catalog_edge", indexes = {
                @Index(columnList = "u_src_sys_id", name = "rce_u_src_node_idx"),
                @Index(columnList = "u_dest_sys_id", name = "rce_u_dest_node_idx"),
                @Index(columnList = "u_catalog_sys_id", name = "rce_u_catalog_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveCatalogEdge extends BaseModel<ResolveCatalogEdgeVO>
{
    private static final long serialVersionUID = -5505273606508652456L;

    private ResolveCatalog catalog;//may have to change to String sysId to avoid the cyclic reference. But test if hibernate creates issue
    private ResolveCatalogNode sourceNode;
    private ResolveCatalogNode destinationNode;
    private Integer UOrder;

    public ResolveCatalogEdge()
    {
    }

    public ResolveCatalogEdge(ResolveCatalogEdgeVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_order")
    public Integer getUOrder()
    {
        return UOrder;
    }

    public void setUOrder(Integer uOrder)
    {
        UOrder = uOrder;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_src_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveCatalogNode getSourceNode()
    {
        return sourceNode;
    }

    public void setSourceNode(ResolveCatalogNode sourceNode)
    {
        this.sourceNode = sourceNode;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_dest_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveCatalogNode getDestinationNode()
    {
        return destinationNode;
    }

    public void setDestinationNode(ResolveCatalogNode destinationNode)
    {
        this.destinationNode = destinationNode;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_catalog_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveCatalog getCatalog()
    {
        return catalog;
    }

    public void setCatalog(ResolveCatalog catalog)
    {
        this.catalog = catalog;
    }

    @Override
    public ResolveCatalogEdgeVO doGetVO()
    {
        ResolveCatalogEdgeVO vo = new ResolveCatalogEdgeVO();
        super.doGetBaseVO(vo);
        
        vo.setUOrder(this.getUOrder());
        
        //populate the sysIds only to avoid cyclic issues
        vo.setCatalog(Hibernate.isInitialized(this.catalog) && getCatalog() != null ? new ResolveCatalogVO(this.getCatalog().getSys_id()) : null);
        vo.setSourceNode(Hibernate.isInitialized(this.sourceNode) && getSourceNode() != null ? new ResolveCatalogNodeVO(this.getSourceNode().getSys_id()): null);
        vo.setDestinationNode(Hibernate.isInitialized(this.destinationNode) && getDestinationNode() != null ? new ResolveCatalogNodeVO(this.getDestinationNode().getSys_id()): null);
        
        return vo;
    }

    @Override
    public void applyVOToModel(ResolveCatalogEdgeVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUOrder(vo.getUOrder() != null && vo.getUOrder().equals(VO.INTEGER_DEFAULT) ? getUOrder() : vo.getUOrder());

            //references
            this.setCatalog(vo.getCatalog() != null ? new ResolveCatalog(vo.getCatalog().getSys_id()) : getCatalog());
            this.setSourceNode(vo.getSourceNode() != null ? new ResolveCatalogNode(vo.getSourceNode().getSys_id()): getSourceNode());
            this.setDestinationNode(vo.getDestinationNode() != null ? new ResolveCatalogNode(vo.getDestinationNode().getSys_id()): getDestinationNode());
            

        }
    }

}
