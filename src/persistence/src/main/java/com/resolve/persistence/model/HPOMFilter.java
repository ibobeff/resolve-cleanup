/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

//July 11, 2012

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.services.hibernate.vo.HPOMFilterVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

/**
 * ServiceNowFilter
 */
@Entity
@Table(name = "hpom_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }, name = "hpomf_u_name_u_queue_uk") })
// @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class HPOMFilter extends GatewayFilter<HPOMFilterVO>
{
    private static final long serialVersionUID = -4044949250944268370L;

    private String UObject;
    private String UQuery;

    // transient

    // object references

    // object referenced by
    public HPOMFilter()
    {
    } // ServiceNowFilter

    public HPOMFilter(HPOMFilterVO vo)
    {
        applyVOToModel(vo);
    } // ServiceNowFilter

    @Column(name = "u_object", length = 40)
    public String getUObject()
    {
        return this.UObject;
    } // getUObject

    public void setUObject(String UObject)
    {
        this.UObject = UObject;
    } // setUObject

    @Column(name = "u_query", length = 4000)
    public String getUQuery()
    {
        return this.UQuery;
    } // getUQuery

    public void setUQuery(String uQuery)
    {
        this.UQuery = uQuery;
    } // setUQuery

    @Override
    public HPOMFilterVO doGetVO()
    {
        HPOMFilterVO vo = new HPOMFilterVO();
        super.doGetBaseVO(vo);

        vo.setUObject(getUObject());
        vo.setUQuery(getUQuery());

        return vo;
    }

    @Override
    public void applyVOToModel(HPOMFilterVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setUObject(StringUtils.isNotBlank(vo.getUObject()) && vo.getUObject().equals(VO.STRING_DEFAULT) ? getUObject() : vo.getUObject());
            this.setUQuery(StringUtils.isNotBlank(vo.getUQuery()) && vo.getUQuery().equals(VO.STRING_DEFAULT) ? getUQuery() : vo.getUQuery());
        }
    }
}
