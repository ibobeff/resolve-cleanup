/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.services.hibernate.vo.RBGeneralVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
@Entity
@Table(name = "rb_general", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_namespace" }, name = "rbgen_u_name_u_namespace_uk") })
public class RBGeneral extends BaseModel<RBGeneralVO>
{
    private String name;
    private String namespace;
    private String summary;
    private Integer noOfColumn; //for the custom form for parameters
    private Boolean newWorksheet;
    private String wikiId;
    private Boolean generated;

    //private Collection<RBParam> params;

    public RBGeneral()
    {
    }

    public RBGeneral(RBGeneralVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_name", length = 200)
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Column(name = "u_namespace", length = 100)
    public String getNamespace()
    {
        return namespace;
    }

    public void setNamespace(String namespace)
    {
        this.namespace = namespace;
    }

    @Column(name = "u_summary", length = 300)
    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    @Column(name = "u_no_of_column")
    public Integer getNoOfColumn()
    {
        return noOfColumn;
    }

    public void setNoOfColumn(Integer noOfColumn)
    {
        this.noOfColumn = noOfColumn;
    }

    @Column(name = "u_new_worksheet")
    public Boolean getNewWorksheet()
    {
        return newWorksheet;
    }

    public void setNewWorksheet(Boolean newWorksheet)
    {
        this.newWorksheet = newWorksheet;
    }

    @Column(name = "wiki_id",  length = 32)
    public String getWikiId()
    {
        return wikiId;
    }

    public void setWikiId(String wikiId)
    {
        this.wikiId = wikiId;
    }
    
    @Column(name = "u_generated")
    public Boolean getGenerated()
    {
        return generated;
    }

    public void setGenerated(Boolean generated)
    {
        this.generated = generated;
    }

    @Override
    public RBGeneralVO doGetVO()
    {
        RBGeneralVO vo = new RBGeneralVO();
        super.doGetBaseVO(vo);

        vo.setName(getName());
        vo.setNamespace(getNamespace());
        vo.setSummary(getSummary());
        vo.setNoOfColumn(getNoOfColumn());
        vo.setNewWorksheet(getNewWorksheet());
        vo.setWikiId(getWikiId());
        vo.setGenerated(getGenerated() != null ? getGenerated() : Boolean.FALSE);
        return vo;
    }

    @Override
    public void applyVOToModel(RBGeneralVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setName(StringUtils.isNotBlank(vo.getName()) && vo.getName().equals(VO.STRING_DEFAULT) ? getName() : vo.getName());
            this.setNamespace(StringUtils.isNotBlank(vo.getNamespace()) && vo.getNamespace().equals(VO.STRING_DEFAULT) ? getNamespace() : vo.getNamespace());
            this.setSummary(StringUtils.isNotBlank(vo.getSummary()) && vo.getSummary().equals(VO.STRING_DEFAULT) ? getSummary() : vo.getSummary());
            this.setNoOfColumn(vo.getNoOfColumn() != null && vo.getNoOfColumn().equals(VO.INTEGER_DEFAULT) ? getNoOfColumn() : vo.getNoOfColumn());
            this.setNewWorksheet(vo.getNewWorksheet() != null ? vo.getNewWorksheet() : getNewWorksheet());
            this.setWikiId(vo.getWikiId() != null && vo.getWikiId().equals(VO.INTEGER_DEFAULT) ? getWikiId() : vo.getWikiId());
            this.setGenerated(vo.getNewWorksheet() != null ? vo.getGenerated() : getGenerated());
        }
    }
}
