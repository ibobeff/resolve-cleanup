package com.resolve.persistence.dao;

import com.resolve.persistence.model.ResolveCatalogEdge;

public interface ResolveCatalogEdgeDAO extends GenericDAO<ResolveCatalogEdge, String>
{

}
