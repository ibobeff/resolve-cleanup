/******************************************************************************
* (C) Copyright 2017
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.CallbackException;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.type.Type;

import com.resolve.esb.ESB;
import com.resolve.persistence.util.BlockingSIDListener;
import com.resolve.persistence.util.BlockingUListener;
import com.resolve.persistence.util.ResolveInterceptor;
import com.resolve.persistence.util.SIDListener;
import com.resolve.services.hibernate.vo.OrgGroupRelVO;
import com.resolve.services.util.AuditUtils;

/**
 * OrgGroupRel.
 */

@Entity
@Table(name = "org_group_rel", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_org_sys_id", "u_group_sys_id" }, name = "orggrprel_org_grp_ids_uk")},
        indexes = {
                @Index(columnList = "u_org_sys_id", name = "ogr_u_org_sys_id_idx"),
                @Index(columnList = "u_group_sys_id", name = "ogr_u_group_sys_id_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class OrgGroupRel extends BaseModel<OrgGroupRelVO>
{
    private static final long serialVersionUID = -41255735754055831L;
    
    // object references
    private Orgs org;
    private Groups group;

    // object referenced by

    public OrgGroupRel()
    {
    }

    public OrgGroupRel(OrgGroupRelVO vo)
    {
        applyVOToModel(vo);
    }

    public String toString()
	{
	    return org + " associated with " + group;
	} // toString

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_org_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public Orgs getOrg()
    {
        return org;
    }

    public void setOrg(Orgs org)
    {
        this.org = org;

        if (org != null)
        {
            Set<OrgGroupRel> orgGrpRelSet = org.getOrgGroupRels();
            if (orgGrpRelSet == null)
            {
                orgGrpRelSet = new HashSet<OrgGroupRel>();
                orgGrpRelSet.add(this);
                
                org.setOrgGroupRels(orgGrpRelSet);
            }
        }
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_group_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public Groups getGroup()
    {
        return group;
    }

    public void setGroup(Groups group)
    {
        this.group = group;

        if (group != null)
        {
            Set<OrgGroupRel> orgGrpRelSet = group.getOrgGroupRels();
            if (orgGrpRelSet == null)
            {
                orgGrpRelSet = new HashSet<OrgGroupRel>();
                orgGrpRelSet.add(this);
	            
                group.setOrgGroupRels(orgGrpRelSet);
            }
        }
    }


	public static void initListener(ESB mainESB)
	{
	    // insert / update listeners
	    BlockingSIDListener insertListener = new BlockingSIDListener()
        {
            @Override
            public boolean onDataOperation(Object entity, Serializable id, Object[] values, String[] names, Type[] types) throws CallbackException
            {
                AuditUtils.insertLog("org_group_rel", names, values);
                return true;
            }

        };
        ResolveInterceptor.addAsyncInsertListener(OrgGroupRel.class.getName(), insertListener);

        BlockingUListener updateListener = new BlockingUListener()
        {
            @Override
            public boolean onUpdate(Object entity, Serializable id, Object[] values, Object[] prevValues, String[] names, Type[] types)
                            throws CallbackException
            {
                AuditUtils.updateLog("org_group_rel", names, values, prevValues);
                return true;
            }

        };
        ResolveInterceptor.addAsyncUpdateListener(OrgGroupRel.class.getName(), updateListener);
        
        // remove listener
        SIDListener deleteListener = new SIDListener()
        {
            @Override
            public boolean onDataOperation(Object entity, Serializable id, Object[] values, String[] names, Type[] types) throws CallbackException
            {
                AuditUtils.deleteLog("org_group_rel", names, values);
                return true;
            }
        };
        ResolveInterceptor.addAsyncDeleteListener(GroupRoleRel.class.getName(), deleteListener);
        
	} // initListener

    @Override
    public OrgGroupRelVO doGetVO()
    {
        OrgGroupRelVO vo = new OrgGroupRelVO();
        super.doGetBaseVO(vo);
        vo.setOrg(Hibernate.isInitialized(this.org) && getOrg() != null ? getOrg().doGetVO() : null);
        vo.setGroup(Hibernate.isInitialized(this.group) && getGroup() != null ? getGroup().doGetVO() : null);
        
        return vo;
    }

    @Override
    public void applyVOToModel(OrgGroupRelVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);  
            
            this.setOrg(vo.getOrg() != null ? new Orgs(vo.getOrg()) : null);
            this.setGroup(vo.getGroup() != null ? new Groups(vo.getGroup()) : null);
        }
    }
    
    @Override
    public boolean equals(Object otherObj)
    {
        boolean isEquals = false;
        if (otherObj instanceof OrgGroupRel)
        {
            OrgGroupRel otherOrgGrpRel = (OrgGroupRel)otherObj;
            
            if (otherOrgGrpRel != this)
            {
                if (otherOrgGrpRel.getOrg().getSys_id().equals(this.getOrg().getSys_id()) &&
                    otherOrgGrpRel.getGroup().getSys_id().equals(this.getGroup().getSys_id()))
                {
                    isEquals = true;
                }
            }
            else
                isEquals = true; 
        }
        
        return isEquals;
    }
    
    @Override
    public int hashCode()
    {
        int hash = 1;
        if (this.getOrg() != null)
        {
            hash = hash * 31 + this.getOrg().getSys_id().hashCode();
        }
        if (this.getGroup() != null)
        {
            hash = hash * 31 + this.getGroup().getSys_id().hashCode();
        }
        if (hash == 1)
        {
            hash = hash * 31 + this.getSys_id().hashCode();
        }
        return hash;
    }
}

