/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.MetaFormTabFieldVO;
import com.resolve.services.interfaces.VO;

@Entity
@Table(name = "meta_form_tab_field", indexes = {
                @Index(columnList = "u_meta_form_tab_sys_id", name = "mftf_u_meta_form_tag_idx"),
                @Index(columnList = "u_meta_source_sys_id", name = "mftf_form_meta_source_sys_idx"),
                @Index(columnList = "u_meta_field_sys_id", name = "mftf_form_meta_field_sys_idx"),
                @Index(columnList = "u_meta_field_properties_sys_id", name = "mftf_u_meta_field_prop_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaFormTabField extends BaseModel<MetaFormTabFieldVO>
{
    private static final long serialVersionUID = 6742740930704987347L;

    // object referenced
    private MetaFormTab metaFormTab;
    private MetaField metaField;//for DB source
    private MetaSource metaSource;//for INPUT,PROPERTIES source type
//    private MetaStyle metaStyle;
    
    //this will be for the DB field - and mapped to form - so 2 forms can have 1 metafield that can have different properties
    //if this is null, then use the metafield properties
    private MetaFieldProperties metaFieldFormProperties;
    
    private Integer UColumnNumber;//will be 1 or 2, to indicate if the field is on the 1st column or the 2nd
    private Integer UOrder;//order # it will rendered
    
    public MetaFormTabField()
    {
    }
    
    public MetaFormTabField(MetaFormTabFieldVO vo)
    {
        applyVOToModel(vo);
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_meta_form_tab_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaFormTab getMetaFormTab()
    {
        return metaFormTab;
    }

    public void setMetaFormTab(MetaFormTab MetaFormTab)
    {
        this.metaFormTab = MetaFormTab;

        if (metaFormTab != null)
        {
            Collection<MetaFormTabField> coll = metaFormTab.getMetaFormTabFields();
            if (coll == null)
            {
                coll = new HashSet<MetaFormTabField>();
                coll.add(this);
                
                metaFormTab.setMetaFormTabFields(coll);
            }
        }
    }
    
    @ManyToOne(fetch = FetchType.EAGER) //THIS IS DONE ON PURPOSE
    @JoinColumn(name = "u_meta_source_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaSource getMetaSource()
    {
        return metaSource;
    }

    public void setMetaSource(MetaSource metaSource)
    {
        this.metaSource = metaSource;
    }
    
    @ManyToOne(fetch = FetchType.EAGER) //THIS IS DONE ON PURPOSE
    @JoinColumn(name = "u_meta_field_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaField getMetaField()
    {
        return metaField;
    }

    public void setMetaField(MetaField metaField)
    {
        this.metaField = metaField;
    }
    
//    @ManyToOne(fetch = FetchType.EAGER) //THIS IS DONE ON PURPOSE
//    @JoinColumn(name = "u_meta_style_sys_id")
//    @Index(name = "mftf_u_meta_style_idx")
//    @NotFound(action = NotFoundAction.IGNORE)
//    public MetaStyle getMetaStyle()
//    {
//        return metaStyle;
//    }
//    
//    public void setMetaStyle(MetaStyle metaStyle)
//    {
//        this.metaStyle = metaStyle;
//    }
    
    @Column(name = "u_order")
    public Integer getUOrder()
    {
        return this.UOrder;
    }

    public void setUOrder(Integer UOrder)
    {
        this.UOrder = UOrder;
    }
    
    @Column(name = "u_column_number")
    public Integer getUColumnNumber()
    {
        return this.UColumnNumber;
    }

    public void setUColumnNumber(Integer UColumnNumber)
    {
        this.UColumnNumber = UColumnNumber;
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_meta_field_properties_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaFieldProperties getMetaFieldFormProperties()
    {
        return metaFieldFormProperties;
    }

    public void setMetaFieldFormProperties(MetaFieldProperties metaFieldFormProperties)
    {
        this.metaFieldFormProperties = metaFieldFormProperties;
    }

    @Override
    public MetaFormTabFieldVO doGetVO()
    {
        MetaFormTabFieldVO vo = new MetaFormTabFieldVO();
        super.doGetBaseVO(vo);
        
        vo.setUColumnNumber(getUColumnNumber());
        vo.setUOrder(getUOrder());
        
        //refs
        vo.setMetaFieldFormProperties(Hibernate.isInitialized(this.metaFieldFormProperties) && getMetaFieldFormProperties() != null ? getMetaFieldFormProperties().doGetVO() : null) ;
        
        return vo;
    }

    @Override
    public void applyVOToModel(MetaFormTabFieldVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo); 
            
            this.setUColumnNumber(vo.getUColumnNumber() != null && vo.getUColumnNumber().equals(VO.INTEGER_DEFAULT) ? getUColumnNumber() : vo.getUColumnNumber());
            this.setUOrder(vo.getUOrder() != null && vo.getUOrder().equals(VO.INTEGER_DEFAULT) ? getUOrder() : vo.getUOrder());
            
            //refs
            this.setMetaFieldFormProperties(vo.getMetaFieldFormProperties() != null ? new MetaFieldProperties(vo.getMetaFieldFormProperties()): getMetaFieldFormProperties());
            
        }
        
    }

   
}//MetaFormTabField
