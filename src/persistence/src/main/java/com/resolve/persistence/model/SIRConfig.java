/******************************************************************************
* (C) Copyright 2018
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.CustomDataFormTabVO;
import com.resolve.services.hibernate.vo.SIRConfigVO;

@Entity
@Table(name = "sir_config"
       /*, indexes = {
                @Index(columnList = "u_meta_table_view_sys_id", name = "mtvf_u_meta_table_view_idx"),
                @Index(columnList = "u_meta_field_sys_id", name = "mtvf_u_meta_field_idx"),
                @Index(columnList = "u_meta_view_field_sys_id", name = "mtvf_u_meta_view_field_idx"),
                @Index(columnList = "u_meta_field_properties_sys_id", name = "mtvf_u_meta_field_prop_idx")}*/)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SIRConfig  extends BaseModel<SIRConfigVO>
{
    private static final long serialVersionUID = 847824975194711289L;
    
    // Referenced Objects
    private Set<CustomDataFormTab> customDataFormTabs;
    
    public SIRConfig()
    {
    }

    public SIRConfig(SIRConfigVO vo)
    {
        applyVOToModel(vo);
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "sirConfig")
//    @LazyCollection(LazyCollectionOption.FALSE)
    @NotFound(action = NotFoundAction.IGNORE)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public Set<CustomDataFormTab> getCustomDataFormTabs()
    {
        return customDataFormTabs;
    }

    public void setCustomDataFormTabs(Set<CustomDataFormTab> customDataFormTabs)
    {
        this.customDataFormTabs = customDataFormTabs;
        
        if (customDataFormTabs != null && !customDataFormTabs.isEmpty())
        {
            for(CustomDataFormTab customDataFormTab : customDataFormTabs)
            {
                customDataFormTab.setSysOrg(getSysOrg());
            }
        }
    }
    
    @Override
    public SIRConfigVO doGetVO()
    {
        SIRConfigVO vo = new SIRConfigVO();
        
        super.doGetBaseVO(vo);
        
        // Referenced Objects
        
        Set<CustomDataFormTab> customDataFormTabs = Hibernate.isInitialized(this.customDataFormTabs) ? getCustomDataFormTabs() : null;
        
        if(customDataFormTabs != null && !customDataFormTabs.isEmpty())
        {
            Set<CustomDataFormTabVO> customDataFormTabVOs = new HashSet<CustomDataFormTabVO>();
            
            for(CustomDataFormTab customDataFormTab : customDataFormTabs)                
            {
                CustomDataFormTabVO customDataFormTabVO = customDataFormTab.doGetVO();
                customDataFormTabVO.setSysOrg(this.getSysOrg());
                customDataFormTabVOs.add(customDataFormTabVO);
            }
            
            vo.setCustomDataFormTabs(customDataFormTabVOs);
        }
                        
        return vo;
    }

    @Override
    public void applyVOToModel(SIRConfigVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);
            
            // Referenced Objects
            
            Set<CustomDataFormTabVO> customDataFormTabVOs  = vo.getCustomDataFormTabs();
            
            if(customDataFormTabVOs != null && !customDataFormTabVOs.isEmpty())
            {
                Set<CustomDataFormTab> customDataFormTabs = new HashSet<CustomDataFormTab>();
                
                for(CustomDataFormTabVO customDataFormTabVO : customDataFormTabVOs)
                {
                    customDataFormTabs.add(new CustomDataFormTab(customDataFormTabVO));
                }
                
                this.setCustomDataFormTabs(customDataFormTabs);
            }
        }
    }
    
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        
        sb.append("Org: ").append(getSysOrg());
        
        if (getCustomDataFormTabs() != null && !getCustomDataFormTabs().isEmpty())
        {            
            sb.append("# of Custom Data Form Tabs: " + getCustomDataFormTabs().size());
            
            sb.append(" [");
            
            for (CustomDataFormTab customDataFormTab : getCustomDataFormTabs())
            {
                sb.append(" ");
                sb.append(customDataFormTab);
            }
            
            sb.append("] ");
        }
        
        return sb.toString();
    }
}
