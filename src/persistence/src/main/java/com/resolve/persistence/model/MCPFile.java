/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.resolve.services.hibernate.vo.MCPFileVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
@Entity
@Table(name = "mcp_file")
public class MCPFile extends BaseModel<MCPFileVO>
{
    private String name;
    private String type;
    private String version;
    private Integer size;
    private Date date; 
    
    private String serverID;
    private String serverName;
    
    private MCPFileContent mcpFileContent;
    
    public MCPFile()
    {
    }
    
    public MCPFile(MCPFileVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_name", length=400)
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Column(name = "u_type", length=40)
    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    @Column(name = "u_version", length=40)
    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    @Column(name = "u_size")
    public Integer getSize()
    {
        return size;
    }

    public void setSize(Integer size)
    {
        this.size = size;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "u_date")
    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }
    
    @Column(name = "u_server", length=32)
    public String getServerID()
    {
        return serverID;
    }
    
    public void setServerID(String serverID)
    {
        this.serverID = serverID;
    }
    
    @Column(name = "u_server_name")
    public String getServerName()
    {
        return serverName;
    }
    
    public void setServerName(String serverName)
    {
        this.serverName = serverName;
    }
    
    @Transient
    public MCPFileContent ugetMCPFileContent()
    {
        return mcpFileContent;
    }

    @Transient
    public void usetMCPFileContent(MCPFileContent mcpFileContent)
    {
        this.mcpFileContent = mcpFileContent;
    }

    @Override
    public MCPFileVO doGetVO()
    {
        MCPFileVO vo = new MCPFileVO();
        super.doGetBaseVO(vo);
        
        vo.setName(getName());
        vo.setType(getType());
        vo.setVersion(getVersion());
        vo.setSize(getSize());
        vo.setDate(getDate());
        vo.setServerID(getServerID());
        vo.setServerName(getServerName());
        
        vo.usetMCPFileContent(ugetMCPFileContent() != null ? ugetMCPFileContent().doGetVO() : null) ;
        
        return vo;
    }

    @Override
    public void applyVOToModel(MCPFileVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setName(StringUtils.isNotBlank(vo.getName()) && vo.getName().equals(VO.STRING_DEFAULT) ? getName() : vo.getName());
            this.setType(StringUtils.isNotBlank(vo.getType()) && vo.getType().equals(VO.STRING_DEFAULT) ? getType() : vo.getType());
            this.setVersion(StringUtils.isNotBlank(vo.getVersion()) && vo.getVersion().equals(VO.STRING_DEFAULT) ? getVersion() : vo.getVersion());
            this.setSize(vo.getSize() != null && vo.getSize().equals(VO.INTEGER_DEFAULT) ? getSize() : vo.getSize());
            this.setDate(vo.getDate() != null && vo.getDate().equals(VO.DATE_DEFAULT) ? getDate() : vo.getDate());
            this.setServerID(vo.getServerID() != null && vo.getServerID().equals(VO.STRING_DEFAULT) ? getServerID() : vo.getServerID());
            this.setServerName(vo.getServerName() != null && vo.getServerName().equals(VO.STRING_DEFAULT) ? getServerName() : vo.getServerName());
            
            this.usetMCPFileContent(vo.ugetMCPFileContent() != null ? new MCPFileContent(vo.ugetMCPFileContent()) : null) ;
        }
    }
}
