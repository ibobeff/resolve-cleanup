/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.persistence.model;

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolveCatalogNodeTagRelVO;
import com.resolve.services.hibernate.vo.ResolveCatalogNodeVO;
import com.resolve.services.hibernate.vo.ResolveCatalogVO;
import com.resolve.services.hibernate.vo.ResolveTagVO;

@Entity
@Table(name = "catalog_node_tag_rel", indexes = {
                @Index(columnList = "u_catalog_sys_id", name = "rcntr_u_catalog_idx"),
                @Index(columnList = "u_catalog_node_sys_id", name = "rcntr_u_cattag_node_idx"),
                @Index(columnList = "u_tag_sys_id", name = "rcntr_u_rstag_node_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveCatalogNodeTagRel extends BaseModel<ResolveCatalogNodeTagRelVO>
{

    private static final long serialVersionUID = -423620937477510756L;
    
    private ResolveCatalog catalog; //belongs to this catalog - added for convenience
    private ResolveCatalogNode catalogNode;
    private ResolveTag tag;
    
    
    public ResolveCatalogNodeTagRel()
    {
    }
    
    public ResolveCatalogNodeTagRel(ResolveCatalogNodeTagRelVO vo )
    {
        applyVOToModel(vo);
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_catalog_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveCatalog getCatalog()
    {
        return catalog;
    }

    public void setCatalog(ResolveCatalog catalog)
    {
        this.catalog = catalog;
    }
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_catalog_node_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveCatalogNode getCatalogNode()
    {
        return catalogNode;
    }

    public void setCatalogNode(ResolveCatalogNode catalogNode)
    {
        this.catalogNode = catalogNode;
        if (catalogNode != null)
        {
            Collection<ResolveCatalogNodeTagRel> coll = catalogNode.getCatalogTagRels();
            if (coll == null)
            {
                coll = new HashSet<ResolveCatalogNodeTagRel>();
                coll.add(this);

                catalogNode.setCatalogTagRels(coll);
            }
        }
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_tag_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveTag getTag()
    {
        return tag;
    }

    public void setTag(ResolveTag tag)
    {
        this.tag = tag;
//        if (tag != null)
//        {
//            Collection<ResolveCatalogNodeTagRel> coll = tag.getCatalogTagRels();
//            if (coll == null)
//            {
//                coll = new HashSet<ResolveCatalogNodeTagRel>();
//                coll.add(this);
//
//                tag.setCatalogTagRels(coll);
//            }
//        }
    }

    @Override
    public ResolveCatalogNodeTagRelVO doGetVO()
    {
        ResolveCatalogNodeTagRelVO vo = new ResolveCatalogNodeTagRelVO();
        super.doGetBaseVO(vo);
        
        //only get the required fields to avoid recursiveness and excess data
        if(Hibernate.isInitialized(this.tag) && this.getTag() != null)
        {
            ResolveTagVO tagVO = new ResolveTagVO();
            tagVO.setSys_id(this.getTag().getSys_id());
            tagVO.setUName(this.getTag().getUName());
            
            vo.setTag(tagVO);
        }

        //catalog node
        if(Hibernate.isInitialized(this.catalogNode) && this.getCatalogNode() != null)
        {
            vo.setCatalogNode(new ResolveCatalogNodeVO(this.catalogNode.getSys_id()));
        }
        
        //catalog 
        if(Hibernate.isInitialized(this.catalog) && this.getCatalog() != null)
        {
            vo.setCatalog(new ResolveCatalogVO(this.catalog.getSys_id()));
        }
        
        return vo;
    }

    @Override
    public void applyVOToModel(ResolveCatalogNodeTagRelVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setTag(vo.getTag() != null ? new ResolveTag(vo.getTag().getSys_id()) : null);
            this.setCatalogNode(vo.getCatalogNode() != null ? new ResolveCatalogNode(vo.getCatalogNode().getSys_id()) : null);
            this.setCatalog(vo.getCatalog() != null ? new ResolveCatalog(vo.getCatalog().getSys_id()) : null);
            
        }
        
    }
    
    
}
