/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

//July 11, 2012

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.services.hibernate.vo.SalesforceFilterVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

/**
 * SalesforceFilter
 */
@Entity
@Table(name = "salesforce_filter",  uniqueConstraints = {@UniqueConstraint(columnNames = {"u_name", "u_queue"}, name = "slsfrc_u_name_u_queue_uk")})
// @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SalesforceFilter extends GatewayFilter<SalesforceFilterVO>
{
    private static final long serialVersionUID = -6830077491035721886L;

    private String UObject;
    private String UQuery;

    // object references

    // object referenced by
    public SalesforceFilter()
    {
    } // SalesforceFilter

    public SalesforceFilter(SalesforceFilterVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_object", length = 40)
    public String getUObject()
    {
        return this.UObject;
    } // getUObject

    public void setUObject(String UObject)
    {
        this.UObject = UObject;
    } // setUObject

    @Column(name = "u_query", length = 4000)
    public String getUQuery()
    {
        return this.UQuery;
    } // getUQuery

    public void setUQuery(String uQuery)
    {
        this.UQuery = uQuery;
    } // setUQuery

    @Override
    public SalesforceFilterVO doGetVO()
    {
        SalesforceFilterVO vo = new SalesforceFilterVO();
        super.doGetBaseVO(vo);

        vo.setUObject(getUObject());
        vo.setUQuery(getUQuery());

        return vo;
    }

    @Override
    public void applyVOToModel(SalesforceFilterVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);

            this.setUQuery(StringUtils.isNotBlank(vo.getUQuery()) && vo.getUQuery().equals(VO.STRING_DEFAULT) ? getUQuery() : vo.getUQuery());
            this.setUObject(StringUtils.isNotBlank(vo.getUObject()) && vo.getUObject().equals(VO.STRING_DEFAULT) ? getUObject() : vo.getUObject());
        }
    }
}

