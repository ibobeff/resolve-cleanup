package com.resolve.persistence.model;

import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolveCatalogNodeWikidocRelVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;

@Entity
@Table(name = "catalog_node_wiki_rel", indexes = {
                @Index(columnList = "u_catalog_sys_id", name = "cnwr_u_catalog_idx"),
                @Index(columnList = "u_wikidoc_sys_id", name = "war_u_cat_wikidoc_idx"),
                @Index(columnList = "u_catalog_node_sys_id", name = "war_u_cat_node_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveCatalogNodeWikidocRel extends BaseModel<ResolveCatalogNodeWikidocRelVO>
{
    private static final long serialVersionUID = -6029986176436569667L;

    // object references
    private ResolveCatalog catalog; //belongs to this catalog - added for convenience
    private ResolveCatalogNode catalogNode;
    private WikiDocument wikidoc;

    public ResolveCatalogNodeWikidocRel()
    {
    }

    public ResolveCatalogNodeWikidocRel(ResolveCatalogNodeWikidocRelVO vo)
    {
        applyVOToModel(vo);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_catalog_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveCatalog getCatalog()
    {
        return catalog;
    }

    public void setCatalog(ResolveCatalog catalog)
    {
        this.catalog = catalog;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_wikidoc_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public WikiDocument getWikidoc()
    {
        return wikidoc;
    }

    public void setWikidoc(WikiDocument wikidoc)
    {
        this.wikidoc = wikidoc;
//        if (wikidoc != null)
//        {
//            Collection<ResolveCatalogNodeWikidocRel> coll = wikidoc.getCatalogNodeWikidocRels();
//            if (coll == null)
//            {
//                coll = new HashSet<ResolveCatalogNodeWikidocRel>();
//                coll.add(this);
//
//                wikidoc.setCatalogNodeWikidocRels(coll);
//            }
//        }
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_catalog_node_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveCatalogNode getCatalogNode()
    {
        return catalogNode;
    }

    public void setCatalogNode(ResolveCatalogNode catalogNode)
    {
        this.catalogNode = catalogNode;
//        if (catalogNode != null)
//        {
//            Collection<ResolveCatalogNodeWikidocRel> coll = catalogNode.getCatalogWikiRels();
//            if (coll == null)
//            {
//                coll = new HashSet<ResolveCatalogNodeWikidocRel>();
//                coll.add(this);
//
//                catalogNode.setCatalogWikiRels(coll);
//            }
//        }
    }

    @Override
    public ResolveCatalogNodeWikidocRelVO doGetVO()
    {
        ResolveCatalogNodeWikidocRelVO vo = new ResolveCatalogNodeWikidocRelVO();
        super.doGetBaseVO(vo);

        //only get the required fields to avoid recursiveness and excess data
        if(Hibernate.isInitialized(this.wikidoc) && this.getWikidoc() != null)
        {
            WikiDocumentVO docVO = new WikiDocumentVO();
            docVO.setSys_id(this.getWikidoc().getSys_id());
            docVO.setId(this.getWikidoc().getSys_id());
            docVO.setUFullname(this.getWikidoc().getUFullname());
            
            vo.setWikidoc(docVO);
        }
        
        //no need to do the catalogNode as its the parent
        

        return vo;
    }

    @Override
    public void applyVOToModel(ResolveCatalogNodeWikidocRelVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setWikidoc(vo.getWikidoc() != null ? new WikiDocument(vo.getWikidoc().getSys_id()) : null);
        }

    }

}
