/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.MetaControlItemVO;
import com.resolve.services.hibernate.vo.MetaControlVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;


@Entity
@Table(name = "meta_control")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaControl  extends BaseModel<MetaControlVO>
{
    private static final long serialVersionUID = -3344011456458728176L;
    
    private String UName;
    private String UDisplayName;
    
    // object references  
    
    // object referenced by
    private Collection<MetaControlItem> metaControlItems;
    
    public MetaControl()
    {
    }
    
    public MetaControl(MetaControlVO vo)
    {
        applyVOToModel(vo);
    }
    
    @Column(name = "u_name", length = 100)
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    @Column(name = "u_display_name", length = 100)
    public String getUDisplayName()
    {
        return this.UDisplayName;
    }

    public void setUDisplayName(String UDisplayName)
    {
        this.UDisplayName = UDisplayName;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "metaControl")
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<MetaControlItem> getMetaControlItems()
    {
        return this.metaControlItems;
    }

    public void setMetaControlItems(Collection<MetaControlItem> metaControlItems)
    {
        this.metaControlItems = metaControlItems;
    }

    @Override
    public MetaControlVO doGetVO()
    {
        MetaControlVO vo = new MetaControlVO();
        super.doGetBaseVO(vo);
        
        vo.setUName(getUName());
        vo.setUDisplayName(getUDisplayName());
        
        //references
        Collection<MetaControlItem> metaControlItems = Hibernate.isInitialized(this.metaControlItems) ? getMetaControlItems() : null;
        if(metaControlItems != null && metaControlItems.size() > 0)
        {
            Collection<MetaControlItemVO> vos = new ArrayList<MetaControlItemVO>();
            for(MetaControlItem model : metaControlItems)
            {
                vos.add(model.doGetVO());
            }
            vo.setMetaControlItems(vos);
        }
        
       
        return vo;
    }

    @Override
    public void applyVOToModel(MetaControlVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo); 
            
            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUDisplayName(StringUtils.isNotBlank(vo.getUDisplayName()) && vo.getUDisplayName().equals(VO.STRING_DEFAULT) ? getUDisplayName() : vo.getUDisplayName());
            
            //references
            Collection<MetaControlItemVO> metaControlItemVOs = vo.getMetaControlItems();
            if(metaControlItemVOs != null && metaControlItemVOs.size() > 0)
            {
                Collection<MetaControlItem> models = new ArrayList<MetaControlItem>();
                for(MetaControlItemVO refVO : metaControlItemVOs)
                {
                    models.add(new MetaControlItem(refVO));
                }
                this.setMetaControlItems(models);
            }
        }
        
    }
}
