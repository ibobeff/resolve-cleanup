/******************************************************************************
* (C) Copyright 2019
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.services.hibernate.vo.ArchiveWorksheetDataVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.Guid;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;

import static com.resolve.util.StringUtils.*;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;

//import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "archive_worksheet_data",
	   uniqueConstraints = {@UniqueConstraint(columnNames = {"wsid", "propname"}, name = "arwsdata_wsid_propname_uk")},
	   indexes = {@Index(columnList = "wsid", name = "arwsdata_wsid_idx")})
//@DynamicUpdate(value = true)
// @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ArchiveWorksheetData extends BaseModel<ArchiveWorksheetDataVO>
{
	private static final long serialVersionUID = 1915518221233657889L;

	public static final int MAX_PROPERTY_NAME_SIZE = 128;
	
    private String worksheetId;
    private String propertyName;
    private byte[] propertyValue;
       
    @Column(name = "wsid", length = 32, nullable = false, updatable = false)
    public String getWorksheetId() {
        return worksheetId;
    }

    public void setWorksheetId(String worksheetId) {
        this.worksheetId = worksheetId;
    }
    
    @Column(name = "propname", length = 128, nullable = false, updatable = false)
    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propname) {
    	String trimmedPropName = propname;
    	
        if (isNotBlank(propname)) {
        	if (propname.trim().length() > MAX_PROPERTY_NAME_SIZE) {
        		trimmedPropName = propname.substring(0, MAX_PROPERTY_NAME_SIZE);
        	}
        }
        
        propertyName = trimmedPropName;
    }
    
    @Lob
    @Column(name = "propvalue", columnDefinition = "BLOB")
    public byte[] getPropertyValue() {
    	return propertyValue;
    }
    
    public void setPropertyValue(byte[] propertyValue) {
    	this.propertyValue = propertyValue;
    }
    
    @Override
	public ArchiveWorksheetDataVO doGetVO() {
		ArchiveWorksheetDataVO vo = new ArchiveWorksheetDataVO();
		
		super.doGetBaseVO(vo);
		
		vo.setId(worksheetId + propertyName); // Overwrite VO's Id to WS Id + Property Name as is case in ES
		vo.setSys_id(vo.getId()); // Overwrite VO's SysId to WS Id + Property Name as is case in ES
		
		byte[] propVal = getPropertyValue();
		
		Object propValObj = propVal != null ? ObjectProperties.deserialize(propVal) : null;
		
		vo.setPropertyValue(propValObj);
		
		return vo;
	}
    
    @Override
    public void applyVOToModel(ArchiveWorksheetDataVO vo) {
    	if (vo != null) {
    		
    		/*
    		 * ES sets sys_id as worksheet id + property name for ease of management.
    		 * DB BaseModel sys_id is restricted to 32 chars only so
    		 * archived worksheet data creates a unique sys_id when it is saved.
    		 *
    		 * Please note as of now no archive data of any type once inserted into DB is
    		 * updated and hence no need to maintain reverse relationship for update scenarios.
    		 * 
    		 * In case archived worksheet data needs to be resurrected back into ES,
    		 * use the worksheet id + property name to set the sys_id of ES document and 
    		 * delete the archived worksheet data record.
    		 * 
    		 * Any archived data once resurrected back into ES should be removed from DB.
    		 * 
    		 * Please note ES worksheet id + property name is maintained as
    		 * composite unique key on archived worksheet data table in DB.
    		 */
    		if (isNotBlank(vo.getSys_id()) && 
    			vo.getSys_id().equals(vo.getWorksheetId() + vo.getPropertyName())) {
    			vo.setSys_id(Guid.getGuid());
    		}
    			
    		super.applyVOToModel(vo);
    		
    		setWorksheetId(isNotBlank(vo.getWorksheetId()) && vo.getWorksheetId().equals(VO.STRING_DEFAULT) ? 
    					   getWorksheetId() : vo.getWorksheetId());
    		setPropertyName(isNotBlank(vo.getPropertyName()) && vo.getPropertyName().equals(VO.STRING_DEFAULT) ?
    						getPropertyName() : vo.getPropertyName());
    		setPropertyValue((vo.getPropertyValue() == null || !(vo.getPropertyValue() instanceof Serializable)) ? 
    						 getPropertyValue() : ObjectProperties.serialize((Serializable)vo.getPropertyValue()));
    	}
    }
    
    @Override
    public String toString() {
    	String iso88591Txt = "";
    	
    	if (propertyValue != null) {
    		ByteArrayOutputStream byteOS = new ByteArrayOutputStream();
    		try {
				byteOS.write(propertyValue);
				iso88591Txt = byteOS.toString(StandardCharsets.ISO_8859_1.name());
			} catch (Exception e) {
				Log.log.warn(String.format("Error %sconverting property value bytes to %s encoded text",
										   (StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
										   StandardCharsets.ISO_8859_1.name()));
			}
    		
    	}
    	
    	return String.format("%s: [ES sys_id: %s, Sys_id: %s, Worksheet Id: %s, Property Name: %s, Property Value%s: %s]",
				 			 ArchiveWorksheetData.class.getSimpleName(), worksheetId + propertyName, getSys_id(), worksheetId, 
				 			 propertyName, 
				 			 (propertyValue != null ? 
				 			  "(Data Type: " + propertyValue.getClass().getSimpleName() + ", Serialized ISO-8859-1: " +
				 			  iso88591Txt + " )" : ""),
				 			 (propertyValue != null ? 
				 			  (ObjectProperties.deserialize(propertyValue)).toString() : "null"));
    }
}
