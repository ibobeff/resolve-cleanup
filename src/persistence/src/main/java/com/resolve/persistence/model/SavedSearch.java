package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.SavedQueryFilterVO;
import com.resolve.services.hibernate.vo.SavedQuerySortVO;
import com.resolve.services.hibernate.vo.SavedSearchVO;

@Entity
@Table(name = "saved_search")
public class SavedSearch extends BaseModel<SavedSearchVO> {
	private static final long serialVersionUID = -912563168700284654L;
	private String name;
	private List<SavedQueryFilter> filters;
	private List<SavedQuerySort> sorts;

	@Column(name = "name", nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToMany(cascade=CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "saved_search_sys_id", foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
	public List<SavedQueryFilter> getFilters() {
		return filters;
	}

	public void setFilters(List<SavedQueryFilter> filters) {
		this.filters = filters;
	}

	@OneToMany(cascade=CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "saved_search_sys_id", foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
	public List<SavedQuerySort> getSorts() {
		return sorts;
	}

	public void setSorts(List<SavedQuerySort> sorts) {
		this.sorts = sorts;
	}

	@Override
	public SavedSearchVO doGetVO() {
		SavedSearchVO vo = new SavedSearchVO();
        super.doGetBaseVO(vo);
        vo.setName(this.getName());
		
		List<SavedQueryFilterVO> filterVOs = new ArrayList<>();
		for (SavedQueryFilter filter : this.getFilters()) {
			filterVOs.add(filter.doGetVO());
		}
		vo.setFilters(filterVOs);
		
		List<SavedQuerySortVO> sortVOs = new ArrayList<>();
		for (SavedQuerySort sort : this.getSorts()) {
			sortVOs.add(sort.doGetVO());
		}
		vo.setSorts(sortVOs);

		return vo;
	}

}
