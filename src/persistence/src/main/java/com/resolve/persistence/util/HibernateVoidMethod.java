package com.resolve.persistence.util;

public interface HibernateVoidMethod {

	public void execute() throws Exception;
}
