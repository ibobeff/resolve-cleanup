/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.dao.hibernate;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.ManagedType;
import javax.persistence.metamodel.SingularAttribute;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.hibernate.LockMode;
import org.hibernate.ReplicationMode;
import org.hibernate.Session;
import org.hibernate.query.Query;

import com.resolve.persistence.dao.GenericDAO;
import com.resolve.persistence.dao.OrderbyProperty;
import com.resolve.persistence.model.BaseModel;
import com.resolve.persistence.model.ModelToVO;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.PropertiesUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.util.Constants;
import com.resolve.util.Log;

public abstract class GenericHibernateDAO<T, ID extends Serializable> implements GenericDAO<T, ID>
{
    private static final String CLASS_KEY = "class";
    private static final String JAVA_ARRAY_CLASS_NAME_PREFIX = int[].class.getName().substring(0, 1);
    
	private Class<T> persistentClass;

	public Long count() 
	{
		Session session = HibernateUtil.getSession();
		try {

			return	(Long) session.createQuery("select count(i) from " + persistentClass.getName() + " i ")
				.uniqueResult();
		} finally {
			session.close();
		}
	}
	
    @SuppressWarnings("unchecked")
    public GenericHibernateDAO()
	{
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	public Class<T> getPersistentClass()
	{
		return persistentClass;
	}
	
	public T findById(ID id)
	{
		return findById(id, false);
	}

	public T findById(ID id, boolean lock)
	{
		T entity;
		if (lock)
			entity = (T) HibernateUtil.getCurrentSession().get(getPersistentClass(), id, LockMode.PESSIMISTIC_WRITE);
		else
			entity = (T) HibernateUtil.getCurrentSession().get(getPersistentClass(), id);

		return entity;
	}

	public T loadById(ID id)
	{
		return loadById(id, false);
	}

	@SuppressWarnings("unchecked")
	public T loadById(ID id, boolean lock)
	{
		try {
			return (T) HibernateProxy.execute(() -> {
				T entity;
				if (lock)
					entity = (T) HibernateUtil.getCurrentSession().load(getPersistentClass(), id, LockMode.PESSIMISTIC_WRITE);
				else
					entity = (T) HibernateUtil.getCurrentSession().load(getPersistentClass(), id);
				return entity;
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
			return null;
		}
	}

	
	public List<T> findAll()
	{
		return findByCriteria(-1, -1);
	}

	public List<T> findAll(Predicate... restrictions)
	{
		return findByCriteria(-1, -1, restrictions);
	}

	public List<T> findAll(OrderbyProperty  orderby)
	{
		ArrayList<OrderbyProperty> orders =  new ArrayList<OrderbyProperty>();
		orders.add(orderby);
		return findByCriteria(-1, -1, orders);
	}
	
	public List<T> findAll(List<OrderbyProperty>  orderbys)
	{
		return findByCriteria(-1, -1, orderbys);
	}

	public 	List<T> findAll(List<OrderbyProperty>  orderBy, Predicate... restrictions)
	{
		return findByCriteria(-1, -1, orderBy, restrictions);
	}
	
	
	public List<T> find()
	{
		return findAll();
	}

	public List<T> find(Predicate... restrictions)
	{
		return findAll(restrictions);
	}
	
	public List<T> find(List<OrderbyProperty> orderbys)
	{
		return findAll(orderbys);
	}

	public List<T> find(List<OrderbyProperty> orderbys, Predicate... restrictions)
	{
		return findAll(orderbys, restrictions);
	}
	
	public List<T> findAll(int maxResults)
	{
		return findByCriteria(maxResults, -1);
	}

	public List<T> findAll(int maxResults, Predicate... restrictions)
	{
		return findByCriteria(maxResults, -1, restrictions);
	}
	
	public List<T> findAll(int maxResults, List<OrderbyProperty> orderbys)
	{
		return findByCriteria(maxResults, -1, orderbys);
	}

	public List<T> findAll(int maxResults, List<OrderbyProperty> orderbys, Predicate... restrictions)
	{
		return findByCriteria(maxResults, -1, orderbys, restrictions);
	}

	public List<T> find(int maxResults)
	{
		return findAll(maxResults);
	}

	public List<T> find(int maxResults, Predicate... criterion)
	{
		return findAll(maxResults, criterion);
	}
	
	public List<T> find(int maxResults, List<OrderbyProperty> orderbys)
	{
		return findAll(maxResults, orderbys);
	}

	public List<T> find(int maxResults, List<OrderbyProperty> orderbys, Predicate... restrictions)
	{
		return findAll(maxResults, orderbys, restrictions);
	}
	
	public List<T> findAll(int maxResults, int firstResult)
	{
		return findByCriteria(maxResults, firstResult);
	}

	public List<T> findAll(int maxResults, int firstResult, List<OrderbyProperty> orderbys, Predicate... restrictions)
	{
		return findByCriteria(maxResults, firstResult, orderbys, restrictions);
	}
		
	public List<T> findAll(int maxResults, int firstResult, OrderbyProperty orderby, Predicate... restrictions)
	{
		ArrayList<OrderbyProperty> orders =  new ArrayList<OrderbyProperty>();
		orders.add(orderby);
		return findByCriteria(maxResults, firstResult, orders, restrictions);
	}

	
	public List<T> findAll(int maxResults, int firstResult, Predicate... restrictions)
	{
		return findByCriteria(maxResults, firstResult, restrictions);
	}
	
	public List<T> find(int maxResults, int firstResult)
	{
		return findAll(maxResults, firstResult);
	}

	public List<T> find(int maxResults, int firstResult, Predicate... restrictions)
	{
		return findAll(maxResults, firstResult, restrictions);
	}
	
	public List<T> find(int maxResults, int firstResult, List<OrderbyProperty> orderbys)
	{
		return findAll(maxResults, firstResult, orderbys);
	}

	public List<T> find(int maxResults, int firstResult, List<OrderbyProperty> orderbys, Predicate... restrictions)
	{
		return findAll(maxResults, firstResult, orderbys, restrictions);
	}
	
	public List<T> find(T exampleInstance, String... excludeProperty)
	{
		return find(exampleInstance, -1, -1, excludeProperty);
	}

	public List<T> find(T exampleInstance, List<OrderbyProperty> orderbys, String... excludeProperty)
	{
		return find(exampleInstance, -1, -1, orderbys, excludeProperty);
	}
	
	
	public List<T> find(T exampleInstance, int maxResults, String... excludeProperty)
	{
		return find(exampleInstance, maxResults, -1, excludeProperty);
	}

	public List<T> find(T exampleInstance, int maxResults, List<OrderbyProperty> orderbys, String... excludeProperty)
	{
		return find(exampleInstance, maxResults, -1, orderbys, excludeProperty);
	}

	@SuppressWarnings("unchecked")
    private Pair<CriteriaBuilder, CriteriaQuery<T>> buildCriteriaQueryByExample(T exampleInstance, String... excludeProperty)
	{
		
	    CriteriaBuilder criteriaBldr = HibernateUtil.getCurrentSession().getCriteriaBuilder();
        
        CriteriaQuery<T> criteriaQry = criteriaBldr.createQuery(getPersistentClass());
        
        Root<T> qryFrom = criteriaQry.from(getPersistentClass());
        
        criteriaQry.select(qryFrom);
        
        if (exampleInstance instanceof ModelToVO)
        {
            ((ModelToVO<T>)exampleInstance).resetFieldsWithDefaultValues();
        }
        
        if (exampleInstance.getClass().equals(getPersistentClass()))
        {
            Map<String, Object> exmplInstPropValMap = new HashMap<String, Object>();
            Set<String> excludedProps = new HashSet<String>(Arrays.asList(excludeProperty));
            ArrayList<Predicate> predicates = new ArrayList<Predicate>();
            
            excludedProps.add(CLASS_KEY);
            
            try
            {
                exmplInstPropValMap = BeanUtilsBean.getInstance().getPropertyUtils().describe(exampleInstance);
            }
            catch (Throwable t)
            {
                Log.log.info("Failed to get properties and associated getter methods for passed in exampleInstance. " + 
                             t.getLocalizedMessage());
            }
            
            if (!exmplInstPropValMap.isEmpty())
            {
                for (String prop : exmplInstPropValMap.keySet())
                {
                    if (!excludedProps.contains(prop) && exmplInstPropValMap.get(prop) != null)
                    {
                        Predicate prdCt = criteriaBldr.equal(qryFrom.get(prop), exmplInstPropValMap.get(prop));
                        predicates.add(prdCt);
                    }
                }
            }
            
            if (predicates.size() > 0)
            {
                Predicate andCnjnctPred = criteriaBldr.and(predicates.toArray(new Predicate[predicates.size()]));
                criteriaQry.where(andCnjnctPred);
            }
        }
        
        return Pair.of(criteriaBldr,criteriaQry);
	}
	
	@SuppressWarnings("unchecked")
    private Pair<CriteriaBuilder, CriteriaQuery<T>> buildCriteriaQueryByExampleNullProps(T exampleInstance, 
    																					 List<String> nullProps, 
    																					 String... excludeProperty) {
		try {
			return (Pair<CriteriaBuilder, CriteriaQuery<T>>) HibernateProxy.execute(() -> {
				if (CollectionUtils.isEmpty(nullProps)) {
					return buildCriteriaQueryByExample(exampleInstance, excludeProperty);
				}
				
			    CriteriaBuilder criteriaBldr = HibernateUtil.getCurrentSession().getCriteriaBuilder();
			    
			    CriteriaQuery<T> criteriaQry = criteriaBldr.createQuery(getPersistentClass());
			    
			    Root<T> qryFrom = criteriaQry.from(getPersistentClass());
			    
			    criteriaQry.select(qryFrom);
			    
			    if (exampleInstance instanceof ModelToVO) {
			        ((ModelToVO<T>)exampleInstance).resetFieldsWithDefaultValues();
			    }
			    
			    if (exampleInstance.getClass().equals(getPersistentClass())) {
			        Map<String, Object> exmplInstPropValMap = new HashMap<String, Object>();
			        Set<String> excludedProps = new HashSet<String>(Arrays.asList(excludeProperty));
			        ArrayList<Predicate> predicates = new ArrayList<Predicate>();
			        
			        excludedProps.add(CLASS_KEY);
			        
			        try {
			            exmplInstPropValMap = BeanUtilsBean.getInstance().getPropertyUtils().describe(exampleInstance);
			        } catch (Throwable t) {
			            Log.log.info(String.format("Failed to get properties and associated getter methods for passed in " +
			            						   "exampleInstance. %s", t.getLocalizedMessage()));
			        }
			        
			        if (!exmplInstPropValMap.isEmpty())
			        {
			            for (String prop : exmplInstPropValMap.keySet())
			            {
			                if (!excludedProps.contains(prop) && (nullProps.contains(prop) ||
			                									  exmplInstPropValMap.get(prop) != null))
			                {
			                    Predicate prdCt = nullProps.contains(prop) ?
			                    				  criteriaBldr.isNull(qryFrom.get(prop)) :
			                    				  criteriaBldr.equal(qryFrom.get(prop), exmplInstPropValMap.get(prop));
			                    predicates.add(prdCt);
			                }
			            }
			        }
			        
			        if (predicates.size() > 0)
			        {
			            Predicate andCnjnctPred = criteriaBldr.and(predicates.toArray(new Predicate[predicates.size()]));
			            criteriaQry.where(andCnjnctPred);
			        }
			    }
			    
			    return Pair.of(criteriaBldr,criteriaQry);
			});
		} catch (Exception e) {
			HibernateUtil.rethrowNestedTransaction(e);
			return null;  
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<T> find(T exampleInstance, int maxResults, int firstResult, String... excludeProperty)
	{
		try {
			return (List<T>) HibernateProxy.execute(() -> {
				
				CriteriaQuery<T> criteriaQry = buildCriteriaQueryByExample(exampleInstance, excludeProperty).getRight();
				        
			    Query<T> qry = HibernateUtil.getCurrentSession().createQuery(criteriaQry);
			    
				if (maxResults >= 0)
				{
				    qry.setMaxResults(maxResults);
				}
				
				if (firstResult >= 0)
				{
				    qry.setFirstResult(firstResult);
				}

				List<T> r = qry.getResultList();
				return r;
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			HibernateUtil.rethrowNestedTransaction(e);
		}
		
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<T> find(T exampleInstance, int maxResults, int firstResult, List<OrderbyProperty> orderbys, String... excludeProperty)
	{
		
		try {
			return (List<T>) HibernateProxy.execute(() -> {
				Pair<CriteriaBuilder, CriteriaQuery<T>> pair = buildCriteriaQueryByExample(exampleInstance, excludeProperty);
				
			    List<String> dbOrderbyFunctionsBlackList = PropertiesUtil.getPropertyList(Constants.SECURITY_DB_ORDER_BY_FUNCTIONS_BLACKLIST_SYSTEM_PROPERTY);
			    
			    ArrayList<Order> orderBys = new ArrayList<Order>();
			    
			    Root<?> root = null;        
			    Set<Root<?>> roots = pair.getRight().getRoots();
			    
			    if (roots != null && roots.size() == 1)
			    {
			        root = roots.iterator().next();
			    
					for (OrderbyProperty order : orderbys)
					{
					    // Validate order.property against order by functions black list
					    
			            if (dbOrderbyFunctionsBlackList != null && !dbOrderbyFunctionsBlackList.isEmpty() &&
			                dbOrderbyFunctionsBlackList.contains(order.property.toLowerCase()))
			            {
			                throw new RuntimeException("Unsupported function in order by clause.");
			            }
			            
			            Order orderElm = order.asc ? pair.getLeft().asc(root.get(order.property)) : 
			                             pair.getLeft().desc(root.get(order.property));
			            
			            orderBys.add(orderElm);
					}
			    }
				
			    Query<T> qry = HibernateUtil.getCurrentSession().createQuery(pair.getRight().orderBy(orderBys));
			    
			    if (maxResults >= 0)
			    {
			        qry.setMaxResults(maxResults);
			    }
			    
			    if (firstResult >= 0)
			    {
			        qry.setFirstResult(firstResult);
			    }

				List<T> r = qry.getResultList();
				return r;
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			HibernateUtil.rethrowNestedTransaction(e);
		}
		
		return null;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<T> findNullProps(T exampleInstance, int maxResults, int firstResult, List<OrderbyProperty> orderbys, 
								 List<String> nullProps, String... excludeProperty) {
		if (CollectionUtils.isEmpty(nullProps)) {
			return find(exampleInstance, maxResults, firstResult, orderbys, excludeProperty);
		}
		try {
			return (List<T>) HibernateProxy.execute(() -> {
				Pair<CriteriaBuilder, CriteriaQuery<T>> pair = buildCriteriaQueryByExampleNullProps(exampleInstance,
																									nullProps, 
																									excludeProperty);
				
		        List<String> dbOrderbyFunctionsBlackList = PropertiesUtil.getPropertyList(Constants.SECURITY_DB_ORDER_BY_FUNCTIONS_BLACKLIST_SYSTEM_PROPERTY);
		        
		        ArrayList<Order> orderBys = new ArrayList<Order>();
		        
		        Root<?> root = null;        
		        Set<Root<?>> roots = pair.getRight().getRoots();
		        
		        if (roots != null && roots.size() == 1)
		        {
		            root = roots.iterator().next();
		        
		    		for (OrderbyProperty order : orderbys)
		    		{
		    		    // Validate order.property against order by functions black list
		    		    
		                if (dbOrderbyFunctionsBlackList != null && !dbOrderbyFunctionsBlackList.isEmpty() &&
		                    dbOrderbyFunctionsBlackList.contains(order.property.toLowerCase()))
		                {
		                    throw new RuntimeException("Unsupported function in order by clause.");
		                }
		                
		                Order orderElm = order.asc ? pair.getLeft().asc(root.get(order.property)) : 
		                                 pair.getLeft().desc(root.get(order.property));
		                
		                orderBys.add(orderElm);
		    		}
		        }
				
		        Query<T> qry = HibernateUtil.getCurrentSession().createQuery(pair.getRight().orderBy(orderBys));
		        
		        if (maxResults >= 0)
		        {
		            qry.setMaxResults(maxResults);
		        }
		        
		        if (firstResult >= 0)
		        {
		            qry.setFirstResult(firstResult);
		        }
		
				List<T> r = qry.getResultList();
				return r;
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			HibernateUtil.rethrowNestedTransaction(e);
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public T findFirst(T example, String... excludeProperty)
	{
	    if (example instanceof ModelToVO)
	    {
	        ((ModelToVO<T>)example).resetFieldsWithDefaultValues();
	    }
	    
		List<T> results = find(example, excludeProperty);
		
		if (results == null || results.size() == 0)
		{
			return null;
		}
		else if (results != null && results.size() > 0)
		{
			return results.get(0);
		}
		
        return null;
	}

	@SuppressWarnings("unchecked")
	public T refresh(T entity)
	{
		try {
			return (T) HibernateProxy.execute(() -> {
				HibernateUtil.getCurrentSession().refresh(entity);
				return entity;
			});
		} catch (Exception e) {

			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
			return null;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public T persist(T entity)
	{
		try {
			return (T) HibernateProxy.execute(() -> {
				HibernateUtil.getCurrentSession().saveOrUpdate(entity);
				return entity;
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
			return null;
		}
		
	}

	public T insertOrUpdate(T entity)
	{
		try {
			HibernateProxy.execute(() -> {
				HibernateUtil.getCurrentSession().saveOrUpdate(entity);
			});
		} catch (Exception e) {
			HibernateUtil.rethrowNestedTransaction(e);
			return null;
		}
		
		return entity;
	}

	@SuppressWarnings("unchecked")
	public T save(T entity)
	{
		try {
			return (T) HibernateProxy.execute(() -> {
				HibernateUtil.getCurrentSession().save(entity);
				return entity;
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
			return null;
		}
		
	}

	public T insert(T entity)
	{
		try {
			HibernateProxy.execute(() -> {
				HibernateUtil.getCurrentSession().save(entity);
			});
		} catch (Exception e) {
			HibernateUtil.rethrowNestedTransaction(e);
			return null;
                          
		}
		
		return entity;
	}

	@SuppressWarnings("unchecked")
	public T update(T entity)
	{
		try {
			return (T) HibernateProxy.execute(() -> {
				HibernateUtil.getCurrentSession().update(entity);
				return entity;
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
			return null;
		}
		
	}

	public List<T> update(List<T> entities)
	{
	    Session session = HibernateUtil.getCurrentSession();
	    entities.stream().forEach(e -> session.update(e));
	    return entities;
	}

	public void delete(T entity)
	{
		HibernateUtil.getCurrentSession().delete(entity);
	}

	public void replicate(T entity)
	{
		HibernateUtil.getCurrentSession().replicate(entity, ReplicationMode.OVERWRITE);
	}
	
	public void flush()
	{
		HibernateUtil.getCurrentSession().flush();
	}

	public void clear()
	{
		HibernateUtil.getCurrentSession().clear();
	}

	/**
	 * Use this inside subclasses as a convenience method.
	 * @throws Exception 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
    protected List<T> findByCriteria(int maxResults, int firstResult, Predicate... restrictions) 
	{
		try {
			return (List<T>) HibernateProxy.execute(() -> {
				CriteriaBuilder criteriaBldr = HibernateUtil.getCurrentSession().getCriteriaBuilder();
			    
			    CriteriaQuery<T> criteriaQry = criteriaBldr.createQuery(getPersistentClass());
			    
			    Root<T> qryFrom = criteriaQry.from(getPersistentClass());
			    
			    criteriaQry.select(qryFrom);
			    
			    if (restrictions != null && restrictions.length >= 1)
			    {
			        Predicate andCnjnctPred = criteriaBldr.and(restrictions);
			        criteriaQry.where(andCnjnctPred);
			    }
			    
			    /*
			     *  If Root/from model contains Blob(byte[])/Clob(char[]) or arrays
			     *  of any other primitive type attribute(s)/column(s)
			     *  then do not set distinct, Oracle cannot perform
			     *  distinct queries on Blob(byte[])/Clob(char[]) type columns though
			     *  MySQL does, but not sure if results are really distinct
			     *  in case of MySQL.
			     */
			    
			    EntityType rootEntityType = (EntityType) qryFrom.getModel();
			    String idName = rootEntityType.getId(rootEntityType.getIdType().getJavaType()).getName();
			    
			    Set<SingularAttribute> attribs = ((ManagedType)rootEntityType).getSingularAttributes();
			    boolean containsArrayType = false;
			    
			    if (attribs != null && !attribs.isEmpty())
			    {
			        for (SingularAttribute attrib : attribs)
			        {
			            if (attrib.getJavaType().getName().equals(int[].class.getName()) ||
			                attrib.getJavaType().getName().equals(char[].class.getName()) ||
			                attrib.getJavaType().getName().startsWith(JAVA_ARRAY_CLASS_NAME_PREFIX))
			            {
			                Log.log.debug(attrib.getName() + " is of array type " + attrib.getJavaType().getClass().getName() +
			                              ", and distinct query on array type is not supported by all databases.");
			                containsArrayType = true;
			                break;
			            }
			        }
			    }
			    
			    Query<T> qry = HibernateUtil.getCurrentSession().createQuery(criteriaQry.distinct(!containsArrayType));
			    
			    if (maxResults >= 0)
			    {
			        qry.setMaxResults(maxResults);
			    }
			    
			    if (firstResult >= 0)
			    {
			        qry.setFirstResult(firstResult);
			    }

			    List<T> r = qry.getResultList();
			    
				// Filter out non-distinct records if model contains Blobarray type column(s)
				
				if (containsArrayType)
				{
				    Map<Serializable, T> resultSet = new HashMap<Serializable, T>();
				    
				    int i = 0;
				    
				    for (T result : r)
				    {
				        Serializable resultIdentifier = null;
				        
				        if (result instanceof BaseModel)
				        {
				            resultIdentifier = ((BaseModel)result).getSys_id();
				        }
				        else
				        {
				            try
			                {
			                    resultIdentifier = (Serializable) BeanUtilsBean.getInstance().getPropertyUtils().describe(result).get(idName);
			                }
			                catch (Exception e)
			                {
			                    Log.log.warn("Error " + e.getLocalizedMessage() + " in getting value of identifier " + idName + 
			                                 " from result for identification of distinct records from result set of type " + result.getClass().getName() +
			                                 ". Returned result set may contain multiple records for single " + idName + " value.");
			                    resultIdentifier = Integer.valueOf(i++);
			                }
				        }
				        
				        resultSet.put(resultIdentifier, result);
				    }
				    
				    r = new ArrayList<T>(resultSet.values());
				}
				
				return r;
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	protected List<T> findByCriteria(int maxResults, int firstResult, List<OrderbyProperty> props, Predicate... restrictions) 
	{
//		Boolean isLocal = startLocalTransaction();
		
		try {
			return (List<T>) HibernateProxy.execute(() -> {
				CriteriaBuilder criteriaBldr = HibernateUtil.getCurrentSession().getCriteriaBuilder();
			    
			    CriteriaQuery<T> criteriaQry = criteriaBldr.createQuery(getPersistentClass());
			    
			    Root<T> qryFrom = criteriaQry.from(getPersistentClass());
			    
			    criteriaQry.select(qryFrom);
			    
			    if (restrictions != null && restrictions.length >= 1)
			    {
			        Predicate andCnjnctPred = criteriaBldr.and(restrictions);
			        criteriaQry.where(andCnjnctPred);
			    }
			    
			    List<String> dbOrderbyFunctionsBlackList = PropertiesUtil.getPropertyList(Constants.SECURITY_DB_ORDER_BY_FUNCTIONS_BLACKLIST_SYSTEM_PROPERTY);
			    
			    ArrayList<Order> orderBys = new ArrayList<Order>();
			    
			    Root<?> root = null;        
			    Set<Root<?>> roots = criteriaQry.getRoots();
			    
			    if (roots != null && roots.size() == 1)
			    {
			        root = roots.iterator().next();
			    
			        for (OrderbyProperty order : props)
			        {
			            // Validate order.property against order by functions black list
			            
			            if (dbOrderbyFunctionsBlackList != null && !dbOrderbyFunctionsBlackList.isEmpty() &&
			                dbOrderbyFunctionsBlackList.contains(order.property.toLowerCase()))
			            {
			                throw new RuntimeException("Unsupported function in order by clause.");
			            }
			            
			            Order orderElm = order.asc ? criteriaBldr.asc(root.get(order.property)) : 
			                             criteriaBldr.desc(root.get(order.property));
			            
			            orderBys.add(orderElm);
			        }
			    }
			    
			    Query<T> qry = HibernateUtil.getCurrentSession().createQuery(criteriaQry.orderBy(orderBys));
			    
			    if (maxResults >= 0)
			    {
			        qry.setMaxResults(maxResults);
			    }
			    
			    if (firstResult >= 0)
			    {
			        qry.setFirstResult(firstResult);
			    }
				
				List<T> r = qry.getResultList();
				
				return r;
				
//			endLocalTransaction(isLocal);

			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
			return null;
		}
		
		
	}

	@SuppressWarnings("unchecked")
    public T merge(T entity)
    {
		try {
			return (T) HibernateProxy.execute(() -> {
				T mergedEntity = (T) HibernateUtil.getCurrentSession().merge(entity);
			    return mergedEntity;
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
			return null;
		}
        
    }
	
	@SuppressWarnings("unchecked")
	public List<T> findLike(T exampleInstance, List<String> likeQueryProperties, String... excludeProperty) {
		try {
			return (List<T>) HibernateProxy.execute(() -> {
				return findLike(exampleInstance, -1, -1, likeQueryProperties, excludeProperty);
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
			return null;
		}
				
	}
	
	public List<T> findLike(T exampleInstance, List<OrderbyProperty> orderbys, List<String> likeQueryProperties, 
						    String... excludeProperty) {
		return findLike(exampleInstance, -1, -1, orderbys, likeQueryProperties, excludeProperty);
	}
	
	@SuppressWarnings("unchecked")
	public List<T> findNullProps(T exampleInstance, List<OrderbyProperty> orderbys, List<String> nullProps, 
		    					 String... excludeProperty) {
		try {
			return (List<T>) HibernateProxy.execute(() -> {
				return findNullProps(exampleInstance, -1, -1, orderbys, nullProps, excludeProperty);
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
			return null;
		}
				
	}
	
	@SuppressWarnings("unchecked")
	public T findFirstLike(T example, List<String> likeQueryProperties, String... excludeProperty) {
	    if (example instanceof ModelToVO){
	        ((ModelToVO<T>)example).resetFieldsWithDefaultValues();
	    }
	    
		List<T> results;
		try {
			results = (List<T>) HibernateProxy.execute(() -> {
				return findLike(example, likeQueryProperties, excludeProperty);
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
			return null;
		}
		
		if (CollectionUtils.isEmpty(results)){
			return null;
		} else {
			return results.get(0);
		}
	}
	
	@SuppressWarnings("unchecked")
	public T findFirstLike(T example, List<OrderbyProperty> orderbys, List<String> likeQueryProperties, 
						   String... excludeProperty) {
	    if (example instanceof ModelToVO) {
	        ((ModelToVO<T>)example).resetFieldsWithDefaultValues();
	    }
	    
		List<T> results = findLike(example, orderbys, likeQueryProperties, excludeProperty);
		
		if (CollectionUtils.isEmpty(results)) {
			return null;
		} else {
			return results.get(0);
		}
	}
	
	public List<T> findLike(T exampleInstance, int maxResults, int firstResult, List<String> likeQueryProperties, 
							String... excludeProperty) {
		CriteriaQuery<T> criteriaQry = buildCriteriaQueryByExample(exampleInstance, likeQueryProperties, excludeProperty)
									   .getRight();
		        
        Query<T> qry = HibernateUtil.getCurrentSession().createQuery(criteriaQry);
        
		if (maxResults >= 0) {
		    qry.setMaxResults(maxResults);
		}
		
		if (firstResult >= 0) {
		    qry.setFirstResult(firstResult);
		}

		List<T> r = qry.getResultList();
		return r;
	}

	@SuppressWarnings("unchecked")
	public List<T> findLike(T exampleInstance, int maxResults, int firstResult, List<OrderbyProperty> orderbys, 
							List<String> likeQueryProperties, String... excludeProperty) {
		try {
			return (List<T>) HibernateProxy.execute(() -> {
				
				Pair<CriteriaBuilder, CriteriaQuery<T>> pair = buildCriteriaQueryByExample(exampleInstance, likeQueryProperties, excludeProperty);
				
			    List<String> dbOrderbyFunctionsBlackList = PropertiesUtil.getPropertyList(Constants.SECURITY_DB_ORDER_BY_FUNCTIONS_BLACKLIST_SYSTEM_PROPERTY);
			    
			    ArrayList<Order> orderBys = new ArrayList<Order>();
			    
			    Root<?> root = null;        
			    Set<Root<?>> roots = pair.getRight().getRoots();
			    
			    if (roots != null && roots.size() == 1) {
			        root = roots.iterator().next();
			    
					for (OrderbyProperty order : orderbys) {
					    // Validate order.property against order by functions black list
					    
			            if (dbOrderbyFunctionsBlackList != null && !dbOrderbyFunctionsBlackList.isEmpty() &&
			                dbOrderbyFunctionsBlackList.contains(order.property.toLowerCase())) {
			                throw new RuntimeException("Unsupported function in order by clause.");
			            }
			            
			            Order orderElm = order.asc ? pair.getLeft().asc(root.get(order.property)) : 
			                             pair.getLeft().desc(root.get(order.property));
			            
			            orderBys.add(orderElm);
					}
			    }
				
			    Query<T> qry = HibernateUtil.getCurrentSession().createQuery(pair.getRight().orderBy(orderBys));
			    
			    if (maxResults >= 0) {
			        qry.setMaxResults(maxResults);
			    }
			    
			    if (firstResult >= 0) {
			        qry.setFirstResult(firstResult);
			    }

				List<T> r = qry.getResultList();
				return r;
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
    private Pair<CriteriaBuilder, CriteriaQuery<T>> buildCriteriaQueryByExample(T exampleInstance,
    																			List<String> likeQueryProperties,
    																			String... excludeProperty) {
		if (CollectionUtils.isEmpty(likeQueryProperties)) {
			return buildCriteriaQueryByExample(exampleInstance, excludeProperty);
		}
		
	    CriteriaBuilder criteriaBldr = HibernateUtil.getCurrentSession().getCriteriaBuilder();
        
        CriteriaQuery<T> criteriaQry = criteriaBldr.createQuery(getPersistentClass());
        
        Root<T> qryFrom = criteriaQry.from(getPersistentClass());
        
        criteriaQry.select(qryFrom);
        
        if (exampleInstance instanceof ModelToVO) {
            ((ModelToVO<T>)exampleInstance).resetFieldsWithDefaultValues();
        }
        
        if (exampleInstance.getClass().equals(getPersistentClass())) {
            Map<String, Object> exmplInstPropValMap = new HashMap<String, Object>();
            Set<String> excludedProps = new HashSet<String>(Arrays.asList(excludeProperty));
            ArrayList<Predicate> predicates = new ArrayList<Predicate>();
            
            excludedProps.add(CLASS_KEY);
            
            try {
                exmplInstPropValMap = BeanUtilsBean.getInstance().getPropertyUtils().describe(exampleInstance);
            } catch (Throwable t) {
                Log.log.info("Failed to get properties and associated getter methods for passed in exampleInstance. " + 
                             t.getLocalizedMessage());
            }
            
            if (!exmplInstPropValMap.isEmpty()) {
                for (String prop : exmplInstPropValMap.keySet()) {
                    if (!excludedProps.contains(prop) && exmplInstPropValMap.get(prop) != null) {
                        Predicate prdCt = likeQueryProperties.contains(prop) ? 
                        				  criteriaBldr.like(qryFrom.get(prop), (String)exmplInstPropValMap.get(prop)) : 
                        				  criteriaBldr.equal(qryFrom.get(prop), exmplInstPropValMap.get(prop));
                        predicates.add(prdCt);
                    }
                }
            }
            
            if (predicates.size() > 0) {
                Predicate andCnjnctPred = criteriaBldr.and(predicates.toArray(new Predicate[predicates.size()]));
                criteriaQry.where(andCnjnctPred);
            }
        }
        
        return Pair.of(criteriaBldr,criteriaQry);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
    public List<T> findAll (QueryDTO queryDTO) {
	    CriteriaBuilder builder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery criteria = builder.createQuery(getPersistentClass());
        Root<Object> root = criteria.from(getPersistentClass());
        List<Predicate> predicates = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(queryDTO.getFilterItems())) {
            queryDTO.getFilters().stream().forEach(filter -> {
                switch (filter.getCondition()) {
                    case QueryFilter.EQUALS : {
                        if (filter.getCaseSensitive() == null || filter.getCaseSensitive()) {
                            predicates.add(builder.equal(root.get(filter.getField()), filter.getValue()));
                        } else {
                            predicates.add(builder.equal(builder.lower(root.get(filter.getField())), filter.getValue()));
                        }
                        break;
                    }
                    case QueryFilter.NOT_EQUALS : {
                        if (filter.getCaseSensitive() == null || filter.getCaseSensitive()) {
                            predicates.add(builder.notEqual(root.get(filter.getField()), filter.getValue()));
                        } else {
                            predicates.add(builder.notEqual(builder.lower(root.get(filter.getField())), filter.getValue()));
                        }
                        break;
                    }
                    case QueryFilter.CONTAINS : {
                            predicates.add(builder.like(builder.lower(root.get(filter.getField())), "%" + filter.getValue().toLowerCase() + "%"));
                        break;
                    }
                }
            });
        }
        List<Order> orderList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(queryDTO.getSortItems())) {
            queryDTO.getSortItems().stream().forEach(sort -> {
                switch (sort.getDirection()) {
                    case ASC : {
                        orderList.add(builder.asc(root.get(sort.getProperty())));
                        break;
                    }
                    case DESC : {
                        orderList.add(builder.desc(root.get(sort.getProperty())));
                        break;
                    }
                }
            });
            if (CollectionUtils.isNotEmpty(orderList))
                criteria.orderBy(orderList);
        }
        
        if (CollectionUtils.isNotEmpty(predicates))
            criteria.where(predicates.stream().toArray(Predicate[]::new));
        int maxResults = queryDTO.getLimit();
        int firstResult = queryDTO.getPage();
        Query query = HibernateUtil.getCurrentSession().createQuery(criteria);
        if (maxResults >= 0) 
            query.setMaxResults(maxResults);
        if (firstResult >= 0)
            query.setFirstResult(firstResult-1);
        
        List<T> result = query.list();
        return result;
	}
	
//	/*
//     * Alternative method implementations without using Hibernate L2 cache
//     */
//	
//	public T findByIdNoCache(ID id)
//    {
//        return findByIdNoCache(id, false);
//    }
//
//    public T findByIdNoCache(ID id, boolean lock)
//    {
//        Boolean isLocal = startLocalTransaction();
//        
//        getSession().setCacheMode(CacheMode.IGNORE);
//        
//        T entity;
//        if (lock)
//            entity = (T) getSession().get(getPersistentClass(), id, LockMode.PESSIMISTIC_WRITE);
//        else
//            entity = (T) getSession().get(getPersistentClass(), id);
//
//        endLocalTransaction(isLocal);
//        return entity;
//    }
//    
//    public List<T> findAllNoCache()
//    {
//        return findByCriteriaNoCache(-1, -1);
//    }
//
//    public List<T> findAllNoCache(Predicate... restrictions)
//    {
//        return findByCriteria(-1, -1, restrictions);
//    }
//
//    public List<T> findAllNoCache(OrderbyProperty  orderby)
//    {
//        ArrayList<OrderbyProperty> orders =  new ArrayList<OrderbyProperty>();
//        orders.add(orderby);
//        return findByCriteria(-1, -1, orders);
//    }
//    
//    public List<T> findAllNoCache(List<OrderbyProperty>  orderbys)
//    {
//        return findByCriteria(-1, -1, orderbys);
//    }
//
//    public  List<T> findAllNoCache(List<OrderbyProperty>  orderBy, Predicate... restrictions)
//    {
//        return findByCriteria(-1, -1, orderBy, restrictions);
//    }
//    
//    @SuppressWarnings({ "unchecked", "rawtypes" })
//    protected List<T> findByCriteriaNoCache(int maxResults, int firstResult, Predicate... restrictions)
//    {
//        Boolean isLocal = startLocalTransaction();
//        
//        getSession().setCacheMode(CacheMode.IGNORE);
//        
//        CriteriaBuilder criteriaBldr = getSession().getCriteriaBuilder();
//        
//        CriteriaQuery<T> criteriaQry = criteriaBldr.createQuery(getPersistentClass());
//        
//        Root<T> qryFrom = criteriaQry.from(getPersistentClass());
//        
//        criteriaQry.select(qryFrom);
//        
//        if (restrictions != null && restrictions.length >= 1)
//        {
//            Predicate andCnjnctPred = criteriaBldr.and(restrictions);
//            criteriaQry.where(andCnjnctPred);
//        }
//        
//        /*
//         *  If Root/from model contains Blob(byte[])/Clob(char[]) or arrays
//         *  of any other primitive type attribute(s)/column(s)
//         *  then do not set distinct, Oracle cannot perform
//         *  distinct queries on Blob(byte[])/Clob(char[]) type columns though
//         *  MySQL does, but not sure if results are really distinct
//         *  in case of MySQL.
//         */
//        
//        EntityType rootEntityType = (EntityType) qryFrom.getModel();
//        String idName = rootEntityType.getId(rootEntityType.getIdType().getJavaType()).getName();
//        
//        Set<SingularAttribute> attribs = ((ManagedType)rootEntityType).getSingularAttributes();
//        boolean containsArrayType = false;
//        
//        if (attribs != null && !attribs.isEmpty())
//        {
//            for (SingularAttribute attrib : attribs)
//            {
//                if (attrib.getJavaType().getName().equals(int[].class.getName()) ||
//                    attrib.getJavaType().getName().equals(char[].class.getName()) ||
//                    attrib.getJavaType().getName().startsWith(JAVA_ARRAY_CLASS_NAME_PREFIX))
//                {
//                    Log.log.debug(attrib.getName() + " is of array type " + attrib.getJavaType().getClass().getName() +
//                                  ", and distinct query on array type is not supported by all databases.");
//                    containsArrayType = true;
//                    break;
//                }
//            }
//        }
//        
//        Query<T> qry = getSession().createQuery(criteriaQry.distinct(!containsArrayType));
//        
//        if (maxResults >= 0)
//        {
//            qry.setMaxResults(maxResults);
//        }
//        
//        if (firstResult >= 0)
//        {
//            qry.setFirstResult(firstResult);
//        }
//
//        List<T> r = qry.getResultList();
//        
//        endLocalTransaction(isLocal);
//        
//        // Filter out non-distinct records if model contains Blobarray type column(s)
//        
//        if (containsArrayType)
//        {
//            Map<Serializable, T> resultSet = new HashMap<Serializable, T>();
//            
//            int i = 0;
//            
//            for (T result : r)
//            {
//                Serializable resultIdentifier = null;
//                
//                if (result instanceof BaseModel)
//                {
//                    resultIdentifier = ((BaseModel)result).getSys_id();
//                }
//                else
//                {
//                    try
//                    {
//                        resultIdentifier = (Serializable) BeanUtilsBean.getInstance().getPropertyUtils().describe(result).get(idName);
//                    }
//                    catch (Exception e)
//                    {
//                        Log.log.warn("Error " + e.getLocalizedMessage() + " in getting value of identifier " + idName + 
//                                     " from result for identification of distinct records from result set of type " + result.getClass().getName() +
//                                     ". Returned result set may contain multiple records for single " + idName + " value.");
//                        resultIdentifier = Integer.valueOf(i++);
//                    }
//                }
//                
//                resultSet.put(resultIdentifier, result);
//            }
//            
//            r = new ArrayList<T>(resultSet.values());
//        }
//        
//        return r;
//    }
//    
//    protected List<T> findByCriteriaNoCache(int maxResults, int firstResult, List<OrderbyProperty> props, Predicate... restrictions)
//    {
//        Boolean isLocal = startLocalTransaction();
//        
//        getSession().setCacheMode(CacheMode.IGNORE);
//        
//        CriteriaBuilder criteriaBldr = getSession().getCriteriaBuilder();
//        
//        CriteriaQuery<T> criteriaQry = criteriaBldr.createQuery(getPersistentClass());
//        
//        Root<T> qryFrom = criteriaQry.from(getPersistentClass());
//        
//        criteriaQry.select(qryFrom);
//        
//        if (restrictions != null && restrictions.length >= 1)
//        {
//            Predicate andCnjnctPred = criteriaBldr.and(restrictions);
//            criteriaQry.where(andCnjnctPred);
//        }
//        
//        List<String> dbOrderbyFunctionsBlackList = PropertiesUtil.getPropertyList(Constants.SECURITY_DB_ORDER_BY_FUNCTIONS_BLACKLIST_SYSTEM_PROPERTY);
//        
//        ArrayList<Order> orderBys = new ArrayList<Order>();
//        
//        Root<?> root = null;        
//        Set<Root<?>> roots = criteriaQry.getRoots();
//        
//        if (roots != null && roots.size() == 1)
//        {
//            root = roots.iterator().next();
//        
//            for (OrderbyProperty order : props)
//            {
//                // Validate order.property against order by functions black list
//                
//                if (dbOrderbyFunctionsBlackList != null && !dbOrderbyFunctionsBlackList.isEmpty() &&
//                    dbOrderbyFunctionsBlackList.contains(order.property.toLowerCase()))
//                {
//                    throw new RuntimeException("Unsupported function in order by clause.");
//                }
//                
//                Order orderElm = order.asc ? criteriaBldr.asc(root.get(order.property)) : 
//                                 criteriaBldr.desc(root.get(order.property));
//                
//                orderBys.add(orderElm);
//            }
//        }
//        
//        Query<T> qry = getSession().createQuery(criteriaQry.orderBy(orderBys));
//        
//        if (maxResults >= 0)
//        {
//            qry.setMaxResults(maxResults);
//        }
//        
//        if (firstResult >= 0)
//        {
//            qry.setFirstResult(firstResult);
//        }
//        
//        List<T> r = qry.getResultList();
//        
//        endLocalTransaction(isLocal);
//        
//        return r;
//    }
//    
//    public List<T> findAllNoCache(int maxResults)
//    {
//        return findByCriteriaNoCache(maxResults, -1);
//    }
//    
//    public List<T> findAllNoCache(int maxResults, Predicate... restrictions)
//    {
//        return findByCriteriaNoCache(maxResults, -1, restrictions);
//    }
//    
//    public List<T> findAllNoCache(int maxResults, List<OrderbyProperty> orderbys)
//    {
//        return findByCriteriaNoCache(maxResults, -1, orderbys);
//    }
//    
//    public List<T> findAllNoCache(int maxResults, List<OrderbyProperty> orderbys, Predicate... restrictions)
//    {
//        return findByCriteriaNoCache(maxResults, -1, orderbys, restrictions);
//    }
//    
//    public List<T> findAllNoCache(int maxResults, int firstResult)
//    {
//        return findByCriteriaNoCache(maxResults, firstResult);
//    }
//    
//    public List<T> findAllNoCache(int maxResults, int firstResult, List<OrderbyProperty> orderbys, Predicate... restrictions)
//    {
//        return findByCriteriaNoCache(maxResults, firstResult, orderbys, restrictions);
//    }
//    
//    public List<T> findAllNoCache(int maxResults, int firstResult, OrderbyProperty orderby, Predicate... restrictions)
//    {
//        ArrayList<OrderbyProperty> orders =  new ArrayList<OrderbyProperty>();
//        orders.add(orderby);
//        return findByCriteriaNoCache(maxResults, firstResult, orders, restrictions);
//    }
//    
//    public List<T> findAllNoCache(int maxResults, int firstResult, Predicate... restrictions)
//    {
//        return findByCriteriaNoCache(maxResults, firstResult, restrictions);
//    }
//    
//    public List<T> findNoCache(int maxResults, int firstResult)
//    {
//        return findAllNoCache(maxResults, firstResult);
//    }
//    
//    public List<T> findNoCache()
//    {
//        return findAllNoCache();
//    }
//    
//    public List<T> findNoCache(Predicate... restrictions)
//    {
//        return findAllNoCache(restrictions);
//    }
//    
//    public List<T> findNoCache(List<OrderbyProperty> orderbys)
//    {
//        return findAllNoCache(orderbys);
//    }
//
//    public List<T> findNoCache(List<OrderbyProperty> orderbys, Predicate... restrictions)
//    {
//        return findAllNoCache(orderbys, restrictions);
//    }
//    
//    public List<T> findNoCache(int maxResults)
//    {
//        return findAllNoCache(maxResults);
//    }
//
//    public List<T> findNoCache(int maxResults, Predicate... criterion)
//    {
//        return findAllNoCache(maxResults, criterion);
//    }
//    
//    public List<T> findNoCache(int maxResults, List<OrderbyProperty> orderbys)
//    {
//        return findAllNoCache(maxResults, orderbys);
//    }
//
//    public List<T> findNoCache(int maxResults, List<OrderbyProperty> orderbys, Predicate... restrictions)
//    {
//        return findAllNoCache(maxResults, orderbys, restrictions);
//    }
//    
//    public List<T> findNoCache(int maxResults, int firstResult, Predicate... restrictions)
//    {
//        return findAllNoCache(maxResults, firstResult, restrictions);
//    }
//    
//    public List<T> findNoCache(int maxResults, int firstResult, List<OrderbyProperty> orderbys)
//    {
//        return findAllNoCache(maxResults, firstResult, orderbys);
//    }
//
//    public List<T> findNoCache(int maxResults, int firstResult, List<OrderbyProperty> orderbys, Predicate... restrictions)
//    {
//        return findAllNoCache(maxResults, firstResult, orderbys, restrictions);
//    }
//    
//    public List<T> findNoCache(T exampleInstance, String... excludeProperty)
//    {
//        return findNoCache(exampleInstance, -1, -1, excludeProperty);
//    }
//
//    public List<T> findNoCache(T exampleInstance, List<OrderbyProperty> orderbys, String... excludeProperty)
//    {
//        return findNoCache(exampleInstance, -1, -1, orderbys, excludeProperty);
//    }
//    
//    
//    public List<T> findNoCache(T exampleInstance, int maxResults, String... excludeProperty)
//    {
//        return findNoCache(exampleInstance, maxResults, -1, excludeProperty);
//    }
//
//    public List<T> findNoCache(T exampleInstance, int maxResults, List<OrderbyProperty> orderbys, String... excludeProperty)
//    {
//        return findNoCache(exampleInstance, maxResults, -1, orderbys, excludeProperty);
//    }
//    
//    public List<T> findNoCache(T exampleInstance, int maxResults, int firstResult, String... excludeProperty)
//    {
//        Boolean isLocal = startLocalTransaction();
//        
//        getSession().setCacheMode(CacheMode.IGNORE);
//        
//        CriteriaQuery<T> criteriaQry = buildCriteriaQueryByExample(exampleInstance, excludeProperty).getRight();
//                
//        Query<T> qry = getSession().createQuery(criteriaQry);
//        
//        if (maxResults >= 0)
//        {
//            qry.setMaxResults(maxResults);
//        }
//        
//        if (firstResult >= 0)
//        {
//            qry.setFirstResult(firstResult);
//        }
//        
//        List<T> r = qry.getResultList();
//        endLocalTransaction(isLocal);
//        return r;
//    }
//
//    public List<T> findNoCache(T exampleInstance, int maxResults, int firstResult, List<OrderbyProperty> orderbys, String... excludeProperty)
//    {
//        Boolean isLocal = startLocalTransaction();
//        
//        getSession().setCacheMode(CacheMode.IGNORE);
//        
//        Pair<CriteriaBuilder, CriteriaQuery<T>> pair = buildCriteriaQueryByExample(exampleInstance, excludeProperty);
//        
//        List<String> dbOrderbyFunctionsBlackList = PropertiesUtil.getPropertyList(Constants.SECURITY_DB_ORDER_BY_FUNCTIONS_BLACKLIST_SYSTEM_PROPERTY);
//        
//        ArrayList<Order> orderBys = new ArrayList<Order>();
//        
//        Root<?> root = null;        
//        Set<Root<?>> roots = pair.getRight().getRoots();
//        
//        if (roots != null && roots.size() == 1)
//        {
//            root = roots.iterator().next();
//        
//            for (OrderbyProperty order : orderbys)
//            {
//                // Validate order.property against order by functions black list
//                
//                if (dbOrderbyFunctionsBlackList != null && !dbOrderbyFunctionsBlackList.isEmpty() &&
//                    dbOrderbyFunctionsBlackList.contains(order.property.toLowerCase()))
//                {
//                    throw new RuntimeException("Unsupported function in order by clause.");
//                }
//                
//                Order orderElm = order.asc ? pair.getLeft().asc(root.get(order.property)) : 
//                                 pair.getLeft().desc(root.get(order.property));
//                
//                orderBys.add(orderElm);
//            }
//        }
//        
//        Query<T> qry = getSession().createQuery(pair.getRight().orderBy(orderBys));
//        
//        if (maxResults >= 0)
//        {
//            qry.setMaxResults(maxResults);
//        }
//        
//        if (firstResult >= 0)
//        {
//            qry.setFirstResult(firstResult);
//        }
//
//        List<T> r = qry.getResultList();
//        endLocalTransaction(isLocal);
//        return r;
//    }
//    
//    @SuppressWarnings("unchecked")
//    public T findFirstNoCache(T example, String... excludeProperty)
//    {
//        if (example instanceof ModelToVO)
//        {
//            ((ModelToVO<T>)example).resetFieldsWithDefaultValues();
//        }
//        
//        List<T> results = findNoCache(example, excludeProperty);
//        
//        if (results == null || results.size() == 0)
//        {
//            return null;
//        }
//        else if (results != null && results.size() > 0)
//        {
//            return results.get(0);
//        }
//        
//        return null;
//    }
}
