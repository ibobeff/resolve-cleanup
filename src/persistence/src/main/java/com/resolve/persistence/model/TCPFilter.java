/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.services.hibernate.vo.TCPFilterVO;

/**
 * TCPFilter
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "tcp_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }, name = "tcpf_u_name_u_queue_uk") })
public class TCPFilter extends GatewayFilter<TCPFilterVO>
{
    private Integer UPort;
    private String UBeginSeparator;
    private String UEndSeparator;
    private Boolean UIncludeBeginSeparator;
    private Boolean UIncludeEndSeparator;

    public TCPFilter()
    {
    }

    public TCPFilter(TCPFilterVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_port")
    public Integer getUPort()
    {
        return UPort;
    }

    public void setUPort(Integer uPort)
    {
        UPort = uPort;
    }

    @Column(name = "u_begin_separator")
    public String getUBeginSeparator()
    {
        return UBeginSeparator;
    }

    public void setUBeginSeparator(String uBeginSeparator)
    {
        UBeginSeparator = uBeginSeparator;
    }

    @Column(name = "u_end_separator")
    public String getUEndSeparator()
    {
        return UEndSeparator;
    }

    public void setUEndSeparator(String uEndSeparator)
    {
        UEndSeparator = uEndSeparator;
    }

    @Column(name = "u_incl_begin_separator")
    public Boolean getUIncludeBeginSeparator()
    {
        return UIncludeBeginSeparator;
    }

    public void setUIncludeBeginSeparator(Boolean uIncludeBeginSeparator)
    {
        UIncludeBeginSeparator = uIncludeBeginSeparator;
    }

    @Column(name = "u_incl_end_separator")
    public Boolean getUIncludeEndSeparator()
    {
        return UIncludeEndSeparator;
    }

    public void setUIncludeEndSeparator(Boolean uIncludeEndSeparator)
    {
        UIncludeEndSeparator = uIncludeEndSeparator;
    }

    @Override
    public TCPFilterVO doGetVO()
    {
        TCPFilterVO vo = new TCPFilterVO();
        super.doGetBaseVO(vo);

        vo.setUPort(getUPort());
        vo.setUBeginSeparator(getUBeginSeparator());
        vo.setUEndSeparator(getUEndSeparator());
        vo.setUIncludeBeginSeparator(getUIncludeBeginSeparator());
        vo.setUIncludeEndSeparator(getUIncludeEndSeparator());

        return vo;
    }

    @Override
    public void applyVOToModel(TCPFilterVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            this.setUPort(vo.getUPort() != null ? vo.getUPort() : getUPort());
            this.setUBeginSeparator(vo.getUBeginSeparator() != null ? vo.getUBeginSeparator() : getUBeginSeparator());
            this.setUEndSeparator(vo.getUEndSeparator() != null ? vo.getUEndSeparator() : getUEndSeparator());
            this.setUIncludeBeginSeparator(vo.getUIncludeBeginSeparator() != null ? vo.getUIncludeBeginSeparator() : getUIncludeBeginSeparator());
            this.setUIncludeEndSeparator(vo.getUIncludeEndSeparator() != null ? vo.getUIncludeEndSeparator() : getUIncludeEndSeparator());
        }
    }
}
