/******************************************************************************
* (C) Copyright 2018
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import static com.resolve.util.Log.log;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Resolve Diration Convertor.
 * 
 * Sets the granularity of SLA Duration to minutes. 
 *
 * @author hemant.phanasgaonkar
 *
 */

@Converter(autoApply = false)
public class ResolveSLADurationConverter implements AttributeConverter<Duration, Long> {

    @Override
    public Long convertToDatabaseColumn(Duration duration) {
        Long durationMinutes = 0L;
        
        try {
            durationMinutes = duration.toMinutes();
        } catch (ArithmeticException ae) {
            log.warn(TimeUnit.MINUTES + duration.toString() + " is too large to fit in " + Long.class.getSimpleName() + 
                     ", limiting to " + Long.MAX_VALUE);
            durationMinutes = Long.MAX_VALUE;
        }
        
        log.trace("Converted " + duration + " to " + durationMinutes + " " + TimeUnit.MINUTES + " for persistence.");
        
        return durationMinutes;
    }

    @Override
    public Duration convertToEntityAttribute(Long durationMinutes) {
        Duration duration = Duration.ofMinutes(0L);
        
        if (Long.MAX_VALUE  > durationMinutes) {
            duration = Duration.ofMinutes(durationMinutes);
        } else {
            duration = Duration.ofMinutes(Long.MAX_VALUE);
            log.warn(durationMinutes + " " + TimeUnit.MINUTES + " is too large to fit in " + Long.class.getSimpleName() + 
                            ", limiting to " + Long.MAX_VALUE);
        }
        
        log.trace("Converted persisted " + durationMinutes + " to " + duration + ".");
        
        return duration;
    }
}