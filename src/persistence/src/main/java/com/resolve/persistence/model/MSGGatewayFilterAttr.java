package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.services.hibernate.vo.MSGGatewayFilterAttrVO;
import com.resolve.services.interfaces.VO;

@Entity
@Table(name = "msg_gateway_filter_attr", 
	   uniqueConstraints = { 
			   			       @UniqueConstraint(columnNames = { "u_name", "msg_filter_id" }, 
			   			    		   			 name = "msggfa_name_val_fltrid_uk") 
			   			   },
	   indexes = {
	                 @Index(columnList = "msg_filter_id", name = "msggfa_fltrid_idx")
		 	     })
public class MSGGatewayFilterAttr extends BaseModel<MSGGatewayFilterAttrVO> {

    private static final long serialVersionUID = 1L;

    public MSGGatewayFilterAttr() {
    }

    public MSGGatewayFilterAttr(MSGGatewayFilterAttrVO vo) {
        applyVOToModel(vo);
    }

    private String UName;
    private String UValue;
    private String UMSGFilterId;

    @Column(name = "u_name", length = 255)
    public String getUName()
    {
        return UName;
    }

    public void setUName(String uName)
    {
        UName = uName;
    }

    @Column(name = "u_value", length = 4000)
    public String getUValue()
    {
        return UValue;
    }

    public void setUValue(String uValue)
    {
        UValue = uValue;
    }

    @Column(name = "msg_filter_id", length = 32)
    public String getUMSGFilterId()
    {
        return UMSGFilterId;
    }

    public void setUMSGFilterId(String uMSGFilterId) {
        UMSGFilterId = uMSGFilterId;
    }

    public MSGGatewayFilterAttrVO doGetVO() {

        MSGGatewayFilterAttrVO vo = new MSGGatewayFilterAttrVO();

        super.doGetBaseVO(vo);

        vo.setUName(getUName());
        vo.setUValue(getUValue());
        vo.setUMSGFilterId(getUMSGFilterId());

        return vo;
    }

    @Override
    public void applyVOToModel(MSGGatewayFilterAttrVO vo) {

        if (vo == null) return;

        super.applyVOToModel(vo);

        if (!VO.STRING_DEFAULT.equals(vo.getUName())) this.setUName(vo.getUName()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUValue())) this.setUValue(vo.getUValue()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUMSGFilterId())) this.setUMSGFilterId(vo.getUMSGFilterId()); else ;
    }

}