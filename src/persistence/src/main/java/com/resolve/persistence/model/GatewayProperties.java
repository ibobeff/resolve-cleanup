package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.resolve.services.hibernate.vo.GatewayPropertiesVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
@MappedSuperclass
public abstract class GatewayProperties<T> extends BaseModel<T>
{
    protected Boolean UActive;
    protected String UQueue;
    protected String UOrgName;
    protected String URSRemoteName;
    protected String UGatewayName;
    protected String UDisplayName;
    protected String UPackageName;
    protected String UClassName;
    protected String ULicenseCode;
    protected String UEventType;

    protected Boolean UPrimary;
    protected Boolean USecondary;
    protected Boolean UWorker;
    protected Boolean UMock;
    protected Boolean UUppercase;
    protected Integer UInterval = 120;
    protected Integer UFailover = 120;
    protected Integer UHeartbeat = 120;

    protected Boolean USsl;
    protected String USslType;
    protected String UHost;
    protected Integer UPort;
    protected String UUser;
    protected String UPass;

    @Column(name = "u_active")
    public Boolean getUActive()
    {
        return UActive;
    }
    public void setUActive(Boolean uActive)
    {
        UActive = uActive;
    }
    
    @Column(name = "u_queue")
    public String getUQueue()
    {
        return UQueue;
    }
    public void setUQueue(String uQueue)
    {
        UQueue = uQueue;
    }
    
    @Column(name = "u_org_name")
    public String getUOrgName()
    {
        return UOrgName;
    }
    public void setUOrgName(String uOrgName)
    {
        UOrgName = uOrgName;
    }
    
    @Column(name = "u_rsremote_name")
    public String getURSRemoteName()
    {
        return URSRemoteName;
    }
    public void setURSRemoteName(String uRSRemoteName)
    {
        URSRemoteName = uRSRemoteName;
    }
    
    @Column(name = "u_gateway_name")
    public String getUGatewayName()
    {
        return UGatewayName;
    }
    public void setUGatewayName(String uGatewayName)
    {
        UGatewayName = uGatewayName;
    }

    @Column(name = "u_display_name")
    public String getUDisplayName()
    {
        return UDisplayName;
    }
    public void setUDisplayName(String uDisplayName)
    {
        UDisplayName = uDisplayName;
    }
    
    @Column(name = "u_package_name")
    public String getUPackageName()
    {
        return UPackageName;
    }
    public void setUPackageName(String uPackageName)
    {
        UPackageName = uPackageName;
    }
    
    @Column(name = "u_class_name")
    public String getUClassName()
    {
        return UClassName;
    }
    public void setUClassName(String uClassName)
    {
        UClassName = uClassName;
    }
    
    @Column(name = "u_license_code")
    public String getULicenseCode()
    {
        return ULicenseCode;
    }
    public void setULicenseCode(String uLicenseCode)
    {
        ULicenseCode = uLicenseCode;
    }
    
    @Column(name = "u__event_type")
    public String getUEventType()
    {
        return UEventType;
    }
    public void setUEventType(String uEventType)
    {
        UEventType = uEventType;
    }
    
    @Column(name = "u_primary")
    public Boolean getUPrimary()
    {
        return UPrimary;
    }
    public void setUPrimary(Boolean uPrimary)
    {
        UPrimary = uPrimary;
    }
    
    @Column(name = "u_secondary")
    public Boolean getUSecondary()
    {
        return USecondary;
    }
    public void setUSecondary(Boolean uSecondary)
    {
        USecondary = uSecondary;
    }
    
    @Column(name = "u_worker")
    public Boolean getUWorker()
    {
        return UWorker;
    }
    public void setUWorker(Boolean uWorker)
    {
        UWorker = uWorker;
    }
    
    @Column(name = "u_mock")
    public Boolean getUMock()
    {
        return UMock;
    }
    public void setUMock(Boolean uMock)
    {
        UMock = uMock;
    }
    
    @Column(name = "u_uppercase")
    public Boolean getUUppercase()
    {
        return UUppercase;
    }
    public void setUUppercase(Boolean uUppercase)
    {
        UUppercase = uUppercase;
    }
    
    @Column(name = "u_interval")
    public Integer getUInterval()
    {
        return UInterval;
    }
    public void setUInterval(Integer uInterval)
    {
        UInterval = uInterval;
    }
    
    @Column(name = "u_failover")
    public Integer getUFailover()
    {
        return UFailover;
    }
    public void setUFailover(Integer uFailover)
    {
        UFailover = uFailover;
    }
    
    @Column(name = "u_heartbeat")
    public Integer getUHeartbeat()
    {
        return UHeartbeat;
    }
    public void setUHeartbeat(Integer uHeartbeat)
    {
        UHeartbeat = uHeartbeat;
    }
    
    @Column(name = "u_ssl")
    public Boolean getUSsl()
    {
        return USsl;
    }
    public void setUSsl(Boolean uSsl)
    {
        USsl = uSsl;
    }
    
    @Column(name = "u_ssl_type")
    public String getUSslType()
    {
        return USslType;
    }
    public void setUSslType(String uSslType)
    {
        USslType = uSslType;
    }
    
    @Column(name = "u_host")
    public String getUHost()
    {
        return UHost;
    }
    public void setUHost(String uHost)
    {
        UHost = uHost;
    }
    
    @Column(name = "u_port")
    public Integer getUPort()
    {
        return UPort;
    }
    public void setUPort(Integer uPort)
    {
        UPort = uPort;
    }
    
    @Column(name = "u_user")
    public String getUUser()
    {
        return UUser;
    }
    public void setUUser(String uUser)
    {
        UUser = uUser;
    }
    
    @Column(name = "u_pass")
    public String getUPass()
    {
        return UPass;
    }
    public void setUPass(String uPass)
    {
        UPass = uPass;
    }

    protected void doGetBaseVO(GatewayPropertiesVO vo) {

        super.doGetBaseVO(vo);

        vo.setUActive(getUActive());
        vo.setUQueue(getUQueue());
        vo.setUOrgName(getUOrgName());
        vo.setURSRemoteName(getURSRemoteName());
        vo.setUGatewayName(getUGatewayName());
        vo.setUDisplayName(getUDisplayName());
        vo.setUPackageName(getUPackageName());
        vo.setUClassName(getUClassName());
        vo.setULicenseCode(getULicenseCode());
        vo.setUEventType(getUEventType());
        
        vo.setUPackageName(getUPackageName());
        vo.setUSecondary(getUSecondary());
        vo.setUWorker(getUWorker());
        vo.setUMock(getUMock());
        vo.setUUppercase(getUUppercase());
        vo.setUInterval(getUInterval());
        vo.setUInterval(getUInterval());
        vo.setUFailover(getUFailover());
        vo.setUHeartbeat(getUHeartbeat());
        
        vo.setUSsl(getUSsl());
        vo.setUSslType(getUSslType());
        vo.setUHost(getUHost());
        vo.setUPort(getUPort());
        vo.setUUser(getUUser());
        vo.setUPass(getUPass());
    }

    @Override
    public void applyVOToModel(T gatewayVO) {
        
        if (gatewayVO == null) 
            return;
        
        super.applyVOToModel(gatewayVO);
        
        GatewayPropertiesVO vo = (GatewayPropertiesVO)gatewayVO;

        this.setUActive(vo.getUActive() != null ? vo.getUActive() : getUActive());
        this.setUQueue(StringUtils.isNotBlank(vo.getUQueue()) && vo.getUQueue().equals(VO.STRING_DEFAULT) ? getUQueue() : vo.getUQueue());
        this.setUOrgName(StringUtils.isNotBlank(vo.getUOrgName()) && vo.getUOrgName().equals(VO.STRING_DEFAULT) ? getUOrgName() : vo.getUOrgName());
        this.setURSRemoteName(StringUtils.isNotBlank(vo.getURSRemoteName()) && vo.getURSRemoteName().equals(VO.STRING_DEFAULT) ? getURSRemoteName() : vo.getURSRemoteName());
        this.setUGatewayName(StringUtils.isNotBlank(vo.getUGatewayName()) && vo.getUGatewayName().equals(VO.STRING_DEFAULT) ? getUGatewayName() : vo.getUGatewayName());
        this.setUDisplayName(StringUtils.isNotBlank(vo.getUDisplayName()) && vo.getUDisplayName().equals(VO.STRING_DEFAULT) ? getUDisplayName() : vo.getUDisplayName());
        this.setUPackageName(StringUtils.isNotBlank(vo.getUPackageName()) && vo.getUPackageName().equals(VO.STRING_DEFAULT) ? getUPackageName() : vo.getUPackageName());
        this.setUClassName(StringUtils.isNotBlank(vo.getUClassName()) && vo.getUClassName().equals(VO.STRING_DEFAULT) ? getUClassName() : vo.getUClassName());
        this.setULicenseCode(StringUtils.isNotBlank(vo.getULicenseCode()) && vo.getULicenseCode().equals(VO.STRING_DEFAULT) ? getULicenseCode() : vo.getULicenseCode());
        this.setUEventType(StringUtils.isNotBlank(vo.getUEventType()) && vo.getUEventType().equals(VO.STRING_DEFAULT) ? getUEventType() : vo.getUEventType());

        this.setUPrimary(vo.getUPrimary()!= null && vo.getUPrimary().equals(VO.BOOLEAN_DEFAULT) ? getUPrimary() : vo.getUPrimary());
        this.setUSecondary(vo.getUSecondary()!= null && vo.getUSecondary().equals(VO.BOOLEAN_DEFAULT) ? getUSecondary() : vo.getUSecondary());
        this.setUWorker(vo.getUWorker()!= null && vo.getUWorker().equals(VO.BOOLEAN_DEFAULT) ? getUWorker() : vo.getUWorker());
        this.setUMock(vo.getUMock()!= null && vo.getUMock().equals(VO.BOOLEAN_DEFAULT) ? getUMock() : vo.getUMock());
        this.setUUppercase(vo.getUUppercase()!= null && vo.getUUppercase().equals(VO.BOOLEAN_DEFAULT) ? getUUppercase() : vo.getUUppercase());

        this.setUInterval(vo.getUInterval()!= null && vo.getUInterval().equals(VO.INTEGER_DEFAULT) ? getUInterval() : vo.getUInterval());
        this.setUFailover(vo.getUFailover()!= null && vo.getUFailover().equals(VO.INTEGER_DEFAULT) ? getUFailover() : vo.getUFailover());
        this.setUHeartbeat(vo.getUHeartbeat()!= null && vo.getUHeartbeat().equals(VO.INTEGER_DEFAULT) ? getUHeartbeat() : vo.getUHeartbeat());

        this.setUSsl(vo.getUSsl()!= null && vo.getUSsl().equals(VO.BOOLEAN_DEFAULT) ? getUSsl() : vo.getUSsl());
        this.setUPort(vo.getUPort()!= null && vo.getUPort().equals(VO.INTEGER_DEFAULT) ? getUPort() : vo.getUPort());
        this.setUSslType(StringUtils.isNotBlank(vo.getUSslType()) && vo.getUSslType().equals(VO.STRING_DEFAULT) ? getUSslType() : vo.getUSslType());
        this.setUHost(StringUtils.isNotBlank(vo.getUHost()) && vo.getUHost().equals(VO.STRING_DEFAULT) ? getUHost() : vo.getUHost());
        this.setUUser(StringUtils.isNotBlank(vo.getUUser()) && vo.getUUser().equals(VO.STRING_DEFAULT) ? getUUser() : vo.getUUser());
        this.setUPass(StringUtils.isNotBlank(vo.getUPass()) && vo.getUPass().equals(VO.STRING_DEFAULT) ? getUPass() : vo.getUPass());
    }

} // class GatewayProperties
