/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import org.dom4j.Element;

public class ManyToOneDefinition extends ColumnDefinition
{
    private String targetEntity;
    
    //by default, we use column name as relation name.
    public ManyToOneDefinition(String columnName, String targetEntity)
    {
        name=columnName;
        column=columnName;
        this.targetEntity=targetEntity;
    }
    
    public ManyToOneDefinition(String relationName, String columnName, String targetEntity)
    {
        name=relationName;
        column=columnName;
        this.targetEntity=targetEntity;
    }
    
    public String getTarget()
    {
        return targetEntity;
    }
    
    public void setTarget(String target)
    {
        targetEntity=target;
    }
    
    @Override
    public Element addColumn(Element entity)
    {
        Element manyToOne = entity.addElement("many-to-one");
        manyToOne.addAttribute("name", getName());
        manyToOne.addAttribute("entity-name", getTarget());
        manyToOne.addAttribute("column", getColumn());
        manyToOne.addAttribute("foreign-key", "none");
        return manyToOne;
    }
    
}
