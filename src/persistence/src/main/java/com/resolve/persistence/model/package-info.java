/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/
@GenericGenerator(name = "hibernate-uuid", strategy = "uuid")
@GenericGenerator(name = "resolve-uuid", strategy = "com.resolve.persistence.util.ResolveUUIDGenerator")
package com.resolve.persistence.model;

import org.hibernate.annotations.GenericGenerator;