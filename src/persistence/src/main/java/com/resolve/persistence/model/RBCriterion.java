/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.RBCriterionVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
@Entity
@Table(name = "rb_criterion")
public class RBCriterion extends BaseModel<RBCriterionVO>
{
    private RBCondition parent; //condition
    private String parentType; //currently only "condition"
    private String variable;
    private String variableSource;
    private String comparison; //EQUALS, >, >= etc.
    private String source;
    private String sourceName;
    
    public RBCriterion()
    {
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_parent_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public RBCondition getParent()
    {
        return this.parent;
    }

    public void setParent(RBCondition parent)
    {
        this.parent = parent;
    }

    public RBCriterion(RBCriterionVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_parent_type", length = 40)
    public String getParentType()
    {
        return parentType;
    }

    public void setParentType(String parentType)
    {
        this.parentType = parentType;
    }

    @Column(name = "u_variable", length = 40)
    public String getVariable()
    {
        return variable;
    }

    public void setVariable(String variable)
    {
        this.variable = variable;
    }

    @Column(name = "u_variable_source", length = 40)
    public String getVariableSource()
    {
        return variableSource;
    }

    public void setVariableSource(String variableSource)
    {
        this.variableSource = variableSource;
    }

    @Column(name = "u_comparison", length = 40)
    public String getComparison()
    {
        return comparison;
    }

    public void setComparison(String comparison)
    {
        this.comparison = comparison;
    }

    @Column(name = "u_source", length = 40)
    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    @Column(name = "u_source_name", length = 100)
    public String getSourceName()
    {
        return sourceName;
    }

    public void setSourceName(String sourceName)
    {
        this.sourceName = sourceName;
    }

    @Override
    public RBCriterionVO doGetVO()
    {
        RBCriterionVO vo = new RBCriterionVO();
        super.doGetBaseVO(vo);

        vo.setParentType(getParentType());
        vo.setVariable(getVariable());
        vo.setVariableSource(getVariableSource());
        vo.setComparison(getComparison());
        vo.setSource(getSource());
        vo.setSourceName(getSourceName());

        return vo;
    }

    @Override
    public void applyVOToModel(RBCriterionVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setParentType(StringUtils.isNotBlank(vo.getParentType()) && vo.getParentType().equals(VO.STRING_DEFAULT) ? getParentType() : vo.getParentType());
            this.setVariable(StringUtils.isNotBlank(vo.getVariable()) && vo.getVariable().equals(VO.STRING_DEFAULT) ? getVariable() : vo.getVariable());
            this.setVariableSource(StringUtils.isNotBlank(vo.getVariableSource()) && vo.getVariableSource().equals(VO.STRING_DEFAULT) ? getVariableSource() : vo.getVariableSource());
            this.setComparison(StringUtils.isNotBlank(vo.getComparison()) && vo.getComparison().equals(VO.STRING_DEFAULT) ? getComparison() : vo.getComparison());
            this.setSource(StringUtils.isNotBlank(vo.getSource()) && vo.getSource().equals(VO.STRING_DEFAULT) ? getSource() : vo.getSource());
            this.setSourceName(StringUtils.isNotBlank(vo.getSourceName()) && vo.getSourceName().equals(VO.STRING_DEFAULT) ? getSourceName() : vo.getSourceName());
            
            //references
            this.setParent(vo.getParent() != null ? new RBCondition(vo.getParent()) : null);
        }
    }
}
