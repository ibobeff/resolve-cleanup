package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.resolve.services.hibernate.vo.CaseAttributeVO;
import com.resolve.services.hibernate.vo.CaseValueVO;

@Entity
@Table(name = "case_attribute")
public class CaseAttribute extends BaseModel<CaseAttributeVO> {
	private static final long serialVersionUID = -6297477351638764280L;

	private String columnName;
	private String displayName;
	private Boolean active;
	private Boolean showOnDashboard;
	private Boolean showOnIncident;
	private Boolean useAsFilter;
	private Collection<CaseValue> valueRange = new ArrayList<>();
	private String description;

	@Column(name = "u_column_name")
	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	@Column(name = "u_display_name")
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

    @Column(name = "u_active", length = 1)
    @Type(type = "yes_no")
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

    @Column(name = "u_show_on_dashboard", length = 1)
    @Type(type = "yes_no")
	public Boolean getShowOnDashboard() {
		return showOnDashboard;
	}

	public void setShowOnDashboard(Boolean showOnDashboard) {
		this.showOnDashboard = showOnDashboard;
	}

    @Column(name = "u_show_on_incident", length = 1)
    @Type(type = "yes_no")
	public Boolean getShowOnIncident() {
		return showOnIncident;
	}

	public void setShowOnIncident(Boolean showOnIcident) {
		this.showOnIncident = showOnIcident;
	}

    @Column(name = "u_use_as_filter", length = 1)
    @Type(type = "yes_no")
	public Boolean getUseAsFilter() {
		return useAsFilter;
	}

	public void setUseAsFilter(Boolean useAsFilter) {
		this.useAsFilter = useAsFilter;
	}

	@ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "case_value", joinColumns = @JoinColumn(name = "case_attribute_id"))
    @Column(name = "u_value_range")
	public Collection<CaseValue> getValueRange() {
		return valueRange;
	}

	public void setValueRange(Collection<CaseValue> valueRange) {
		this.valueRange = valueRange;
	}

	@Column(name = "u_description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public CaseAttributeVO doGetVO() {
		CaseAttributeVO vo = new CaseAttributeVO();
        super.doGetBaseVO(vo);
        
    	vo.setColumnName(this.getColumnName());
    	vo.setDisplayName(this.getDisplayName());
        vo.setActive(this.getActive());
        vo.setShowOnDashboard(this.getShowOnDashboard());
        vo.setShowOnIncident(this.getShowOnIncident());
        vo.setUseAsFilter(this.getUseAsFilter());
        vo.setDescription(this.getDescription());

        if (this.getValueRange() != null) {
        	for (CaseValue cv : this.getValueRange()) {
        		vo.getValueRange().add(new CaseValueVO(cv.getValue(), cv.getDefaultValue()));
        	}
        }

		return vo;
	}

	@Override
	public void applyVOToModel(CaseAttributeVO caseVO) {
		super.applyVOToModel(caseVO);

		this.setColumnName(caseVO.getColumnName());
		this.setDisplayName(caseVO.getDisplayName());
		this.setActive(caseVO.getActive());
		this.setShowOnDashboard(caseVO.getShowOnDashboard());
		this.setShowOnIncident(caseVO.getShowOnIncident());
		this.setUseAsFilter(caseVO.getUseAsFilter());
		this.setDescription(caseVO.getDescription());
		
		if (caseVO.getValueRange() != null) {
			for (CaseValueVO cvVO : caseVO.getValueRange()) {
				this.getValueRange().add(new CaseValue(cvVO.getValue(), cvVO.getDefaultValue()));
			}
		}
	}
}
