/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.dao;

public class OrderbyProperty
{
	public String property;
	public boolean asc = true ;
	
	public OrderbyProperty(String propertyName, boolean ascent)
	{
		property = propertyName;
		asc = ascent;
	}
	
}
