/******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolveTaskExpressionVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "resolve_task_expression")
public class ResolveTaskExpression extends BaseModel<ResolveTaskExpressionVO>
{
	private static final long serialVersionUID = 8253000334779640279L;
	
	private String criteria; //ANY, ALL
	private String leftOperandType;
    private String leftOperandName;
    private String operator; //EQUALS, >, >= etc.
    private String rightOperandType;
    private String rightOperandName;
    private Integer order;
    private String expressionType; // condition or severity
    private Integer resultLevel; // 0-Good (both condition and severity), 1-Warning (Severify) and BAD (Condition), 2-Severe (severity), 3-Critical (severity)
    private String script;
    
    private ResolveActionInvoc invocation;
    
    public ResolveTaskExpression()
    {
    }
    
    public ResolveTaskExpression(ResolveTaskExpressionVO vo)
    {
    	applyVOToModel(vo);
    }
    
    @Column(name = "u_criteria", length = 10)
	public String getCriteria()
	{
		return criteria;
	}
	public void setCriteria(String criteria)
	{
		this.criteria = criteria;
	}
	
	@Column(name = "u_left_operand_type", length = 32)
	public String getLeftOperandType()
	{
		return leftOperandType;
	}
	public void setLeftOperandType(String leftOperandType)
	{
		this.leftOperandType = leftOperandType;
	}
	
	@Column(name = "u_left_operand_name", length = 40)
	public String getLeftOperandName()
	{
		return leftOperandName;
	}
	public void setLeftOperandName(String leftOperandName)
	{
		this.leftOperandName = leftOperandName;
	}
	
	@Column(name = "u_operator", length = 32)
	public String getOperator()
	{
		return operator;
	}
	public void setOperator(String operator)
	{
		this.operator = operator;
	}
	
	@Column(name = "u_right_operand_type", length = 32)
	public String getRightOperandType()
	{
		return rightOperandType;
	}
	public void setRightOperandType(String rightOperandType)
	{
		this.rightOperandType = rightOperandType;
	}
	
	@Column(name = "u_right_operand_name", length = 40)
	public String getRightOperandName()
	{
		return rightOperandName;
	}
	public void setRightOperandName(String rightOperandName)
	{
		this.rightOperandName = rightOperandName;
	}
	
	@Column(name = "u_order", scale = 2, precision = 10)
	public Integer getOrder()
	{
		return order;
	}
	public void setOrder(Integer order)
	{
		this.order = order;
	}
	
	@Column(name = "u_expression_type", length = 32)
	public String getExpressionType()
	{
		return expressionType;
	}
	public void setExpressionType(String expressionType)
	{
		this.expressionType = expressionType;
	}
	
	@Column(name = "u_result_level", precision = 2)
	public Integer getResultLevel()
	{
		return resultLevel;
	}
	public void setResultLevel(Integer resultLevel)
	{
		this.resultLevel = resultLevel;
	}
	
	@Lob
	@Column(name = "u_script", length = 16777215)
	public String getScript()
	{
		return script;
	}
	public void setScript(String script)
	{
		this.script = script;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_invocation", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
	public ResolveActionInvoc getInvocation()
	{
		return invocation;
	}

	public void setInvocation(ResolveActionInvoc invocation)
	{
		this.invocation = invocation;
	}
	
	@Override
	public ResolveTaskExpressionVO doGetVO()
	{
		ResolveTaskExpressionVO vo = new ResolveTaskExpressionVO();
		super.doGetBaseVO(vo);
		
		vo.setCriteria(this.getCriteria());
		vo.setLeftOperandType(this.getLeftOperandType());
		vo.setLeftOperandName(this.getLeftOperandName());
		vo.setOperator(this.getOperator());
		vo.setRightOperandName(this.getRightOperandName());
		vo.setRightOperandType(this.getRightOperandType());
		vo.setOrder(this.getOrder());
		vo.setExpressionType(this.getExpressionType());
		vo.setResultLevel(this.getResultLevel());
		vo.setScript(this.getScript());
		
		return vo;
	}
    
	@Override
	public void applyVOToModel(ResolveTaskExpressionVO vo)
	{
		if (vo != null)
		{
			super.applyVOToModel(vo);
			this.setCriteria(StringUtils.isNotBlank(vo.getCriteria()) && vo.getCriteria().equals(VO.STRING_DEFAULT) ? getCriteria() : vo.getCriteria());
			this.setLeftOperandName(StringUtils.isNotBlank(vo.getLeftOperandName()) && vo.getLeftOperandName().equals(VO.STRING_DEFAULT) ? getLeftOperandName() : vo.getLeftOperandName());
			this.setLeftOperandType(StringUtils.isNotBlank(vo.getLeftOperandType()) && vo.getLeftOperandType().equals(VO.STRING_DEFAULT) ? getLeftOperandType() : vo.getLeftOperandType());
			this.setOperator(StringUtils.isNotBlank(vo.getOperator()) && vo.getOperator().equals(VO.STRING_DEFAULT) ? getOperator() : vo.getOperator());
			this.setRightOperandName(StringUtils.isNotBlank(vo.getRightOperandName()) && vo.getRightOperandName().equals(VO.STRING_DEFAULT) ? getRightOperandName() : vo.getRightOperandName());
			this.setRightOperandType(StringUtils.isNotBlank(vo.getRightOperandType()) && vo.getRightOperandType().equals(VO.STRING_DEFAULT) ? getRightOperandType() : vo.getRightOperandType());
            this.setOrder(vo.getOrder() != null && vo.getOrder().equals(VO.INTEGER_DEFAULT) ? getOrder() : vo.getOrder());
            this.setExpressionType(StringUtils.isNotBlank(vo.getExpressionType()) && vo.getExpressionType().equals(VO.STRING_DEFAULT) ? getExpressionType() : vo.getExpressionType());
            this.setResultLevel(vo.getResultLevel() != null && vo.getResultLevel().equals(VO.INTEGER_DEFAULT) ? getResultLevel() : vo.getResultLevel());
            this.setScript(StringUtils.isNotBlank(vo.getScript()) && vo.getScript().equals(VO.STRING_DEFAULT) ? getScript() : vo.getScript());
		}
	}
}
