/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

//Internal blocking listener for update operation
public abstract class BlockingUListener implements Ordered, UListener, Comparable<BlockingUListener>
{
	@Override
	public final int compareTo(BlockingUListener t)
	{
		if (t.getOrder()!=this.getOrder())
		{
			return t.getOrder() - getOrder();
		}
		else
		{
			return t.hashCode() - this.hashCode();
		}
	}

	@Override
	public int getOrder() { return 0; }

}