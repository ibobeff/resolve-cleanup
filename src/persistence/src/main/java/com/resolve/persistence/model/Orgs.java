/******************************************************************************
* (C) Copyright 2017
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.GroupsVO;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * Orgs - Organizations
 */

@Entity
@Table(name = "orgs", uniqueConstraints = { @UniqueConstraint(columnNames = {"u_name"}, name = "orgs_u_name_uk")},
            indexes = {@Index(columnList = "u_parent_org_sys_id", name = "o_u_parent_org_sys_id_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Orgs extends BaseModel<OrgsVO>
{
    private static final long serialVersionUID = 2355925824690915909L;
    
    private static final String GROUP_NAMES_SEPARATOR = ", ";
    
    private String UName;
    private String UDescription;
    
    private Orgs UParentOrg;            // Parent Org or null
    private Set<Orgs> UChildOrgs;       // Set of Child Orgs (if any)
    
    private Boolean UHasNoOrgAccess;     // Users belonging to this Org has access to no org or "None" org
    
    // object references
    
    // object referenced by
    private Set<OrgGroupRel> orgGroupRels;

    public Orgs()
    {
    }

    public Orgs(OrgsVO vo)
    {
        applyVOToModel(vo);
    }

    public String toString()
    {
        return this.UName;
    } // toString

    @Column(name = "u_name", length = 200)
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
        
    @Column(name = "u_description", length = 1000)
    public String getUDescription()
    {
        return this.UDescription;
    }

    public void setUDescription(String UDescription)
    {
        this.UDescription = UDescription;
    }
    
    @ManyToOne
    @JoinColumn(name = "u_parent_org_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public Orgs getUParentOrg()
    {
        return UParentOrg;
    }
    
    public void setUParentOrg(Orgs UParentOrg)
    {
        this.UParentOrg = UParentOrg;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "UParentOrg")
    @NotFound(action = NotFoundAction.IGNORE)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public Set<Orgs> getUChildOrgs()
    {
        return UChildOrgs;
    }
    
    public void setUChildOrgs(Set<Orgs> UChildOrgs)
    {
        this.UChildOrgs = UChildOrgs;
    }
    
    @Column(name = "u_has_no_org_access", length = 1)
    @org.hibernate.annotations.Type(type = "yes_no")
    public Boolean getUHasNoOrgAccess()
    {
        return UHasNoOrgAccess;
    }
    
    public Boolean ugetUHasNoOrgAccess()
    {
        Boolean result = getUHasNoOrgAccess();
        
        if(result == null)
        {
            result = Boolean.FALSE;
        }
        
        return result;
    }
    
    public void setUHasNoOrgAccess(Boolean uHasNoOrgAccess)
    {
        UHasNoOrgAccess = uHasNoOrgAccess;
    }
    
    //Org-Group(s) relationship 
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "org")
    @NotFound(action = NotFoundAction.IGNORE)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public Set<OrgGroupRel> getOrgGroupRels()
    {
        return orgGroupRels;
    }

    public void setOrgGroupRels(Set<OrgGroupRel> orgGroupRels)
    {
        this.orgGroupRels = orgGroupRels;
    }
    
    public OrgsVO doGetVONoParentNoChildOrgs()
    {
        OrgsVO vo = new OrgsVO();
        super.doGetBaseVO(vo);
        
        vo.setUName(getUName());
        
        if (StringUtils.isNotBlank(vo.getUName()) && 
            vo.getUName().equalsIgnoreCase(OrgsVO.NONE_ORG_NAME) &&
            vo.getSys_id().equalsIgnoreCase(VO.STRING_DEFAULT) && 
            vo.getId().equalsIgnoreCase(VO.STRING_DEFAULT))
        {
            vo.setSys_id(null);
            vo.setId(null);
        }
        
        vo.setUDescription(getUDescription());
        
        vo.setUHasNoOrgAccess(getUHasNoOrgAccess());
        
        vo.setSysOrganizationName(getUName());
        
        return vo;
    }
    
    @Override
    public OrgsVO doGetVO()
    {
        OrgsVO vo = doGetVONoParentNoChildOrgs();

        // Get Parent without any parent or childrens
        if (getUParentOrg() != null)
        {            
            vo.setUParentOrg(getUParentOrg().doGetVONoParentNoChildOrgs());
        }
        
        if (Hibernate.isInitialized(this.UChildOrgs) && getUChildOrgs() != null && !getUChildOrgs().isEmpty())
        {
            Set<OrgsVO> childOrgs = new HashSet<OrgsVO>();
            
            for (Orgs childOrg : getUChildOrgs())
            {
                childOrgs.add(childOrg.doGetVONoParentNoChildOrgs());
            }
            
            vo.setUChildOrgs(childOrgs);
        }
        
        // Add orgGroups and comma separated group names
        
        if (Hibernate.isInitialized(this.orgGroupRels) && getOrgGroupRels() != null && !getOrgGroupRels().isEmpty())
        {
            List<GroupsVO> orgGroups = new ArrayList<GroupsVO>();
            StringBuilder sbGroups = new StringBuilder();
            
            for (OrgGroupRel orgGrpRel : getOrgGroupRels())
            {
                if (orgGrpRel.getGroup() != null)
                {
                    orgGroups.add(orgGrpRel.getGroup().doGetVO(true));
                }
                
                if (sbGroups.length() > 0)
                {
                    sbGroups.append(GROUP_NAMES_SEPARATOR);
                }
                
                if (orgGrpRel.getGroup() != null)
                {
                    sbGroups.append(orgGrpRel.getGroup().getUName());
                }
            }
            
            vo.setOrgGroups(orgGroups);
            vo.setGroups(sbGroups.toString());
        }
        else
        {
            vo.setGroups(null);
        }
        
        Log.log.trace("OrgsVO.doGetVO returning " + vo);
        
        return vo;
    }

    public OrgsVO doGetVONoOrgGroupRelations()
    {
        OrgsVO vo = doGetVONoParentNoChildOrgs();

        // Get Parent without any parent or childrens
        if (getUParentOrg() != null)
        {            
            vo.setUParentOrg(getUParentOrg().doGetVONoParentNoChildOrgs());
        }
        
        if (Hibernate.isInitialized(this.UChildOrgs) && getUChildOrgs() != null && !getUChildOrgs().isEmpty())
        {
            Set<OrgsVO> childOrgs = new HashSet<OrgsVO>();
            
            for (Orgs childOrg : getUChildOrgs())
            {
                childOrgs.add(childOrg.doGetVONoParentNoChildOrgs());
            }
            
            vo.setUChildOrgs(childOrgs);
        }
        
        // Add comma separated group names
        if (Hibernate.isInitialized(this.orgGroupRels) && getOrgGroupRels() != null && !getOrgGroupRels().isEmpty())
        {
            StringBuilder sbGroups = new StringBuilder();
            
            for (OrgGroupRel orgGrpRel : getOrgGroupRels())
            {
                if (sbGroups.length() > 0)
                {
                    sbGroups.append(GROUP_NAMES_SEPARATOR);
                }
                
                if (orgGrpRel.getGroup() != null)
                {
                    sbGroups.append(orgGrpRel.getGroup().getUName());
                }
            }
            
            vo.setGroups(sbGroups.toString());
        }
        
        Log.log.trace("OrgsVO.doGetVONoOrgGroupRelations returning " + vo);
        
        return vo;
    }
    
    @Override
    public void applyVOToModel(OrgsVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUDescription(StringUtils.isNotBlank(vo.getUDescription()) && vo.getUDescription().equals(VO.STRING_DEFAULT) ? getUDescription() : vo.getUDescription());
            
            this.setUHasNoOrgAccess(vo.getUHasNoOrgAccess() != null ? vo.getUHasNoOrgAccess() : getUHasNoOrgAccess());
            
            if (vo.getUParentOrg() != null)
            {
                OrgsVO parentOrgsVONoParentNoChildOrgs = vo.getUParentOrg();
                parentOrgsVONoParentNoChildOrgs.setUParentOrg(null);
                parentOrgsVONoParentNoChildOrgs.setUChildOrgs(null);
                this.setUParentOrg(new Orgs(parentOrgsVONoParentNoChildOrgs));
            }
            else
            {
                this.setUParentOrg(null);
            }
        }
    }
}
