/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

// Generated Dec 11, 2009 11:15:20 AM by Hibernate Tools 3.2.4.GA

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ArchiveExecuteResultVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

/**
 * ArchiveExecuteResult generated by hbm2java
 */
@Entity
@Table(name = "archive_execute_result", indexes = {
                @Index(columnList = "u_request", name = "aer_u_request_idx"),
                @Index(columnList = "u_execute_result_lob", name = "aer_u_execute_result_lob_idx")})
// @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ArchiveExecuteResult extends BaseModel<ArchiveExecuteResultVO>
{

    private static final long serialVersionUID = -8431595531282242378L;
    
    private String UTarget;
    private Integer UDuration;
    private Integer UReturncode;
    //private String URaw;
    //private String UCommand;
    private String UAddress;

    // object references
    private ArchiveExecuteRequest executeRequest;   // URequest;
    private ArchiveExecuteResultLob executeResultLob;
	
    // object referenced by
	
    public ArchiveExecuteResult()
    {
    }

    public ArchiveExecuteResult(ArchiveExecuteResultVO vo)
    {
        applyVOToModel(vo);
    }


    public ArchiveExecuteResult usetsys_id(String sys_id)
    {
        this.setSys_id(sys_id);
        return this;
    }

    @Column(name = "u_target", length = 32)
    public String getUTarget()
    {
        return this.UTarget;
    }

    public void setUTarget(String UTarget)
    {
        this.UTarget = UTarget;
    }

    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "u_request", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
	public ArchiveExecuteRequest getExecuteRequest()
	{
		return executeRequest;
	}

	public void setExecuteRequest(ArchiveExecuteRequest executeRequest)
	{
		this.executeRequest = executeRequest;
		
		if (executeRequest != null)
		{
			Collection<ArchiveExecuteResult> coll = executeRequest.getArchiveExecuteResults();
			if (coll == null)
			{
				coll = new HashSet<ArchiveExecuteResult>();
				coll.add(this);
				
			    executeRequest.setArchiveExecuteResults(coll);
			}
		}
	}
    
    @Column(name = "u_duration")
    public Integer getUDuration()
    {
        return this.UDuration;
    }

    public void setUDuration(Integer UDuration)
    {
        this.UDuration = UDuration;
    }

    @Column(name = "u_returncode")
    public Integer getUReturncode()
    {
        return this.UReturncode;
    }

    public void setUReturncode(Integer UReturncode)
    {
        this.UReturncode = UReturncode;
    }

    @Transient
    public String getURaw()
    {
        if(this.executeResultLob != null)
        {
            return this.executeResultLob.getURaw();
        }
        return null;
    }

    @Transient
    public String getUCommand()
    {
        if(this.executeResultLob != null)
        {
            return this.executeResultLob.getUCommand();
        }
        return null;
    }

    @Column(name = "u_address", length = 100)
    public String getUAddress()
    {
        return this.UAddress;
    }

    public void setUAddress(String UAddress)
    {
        this.UAddress = UAddress;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_execute_result_lob", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ArchiveExecuteResultLob getExecuteResultLob()
    {
        return this.executeResultLob;
    }

    public void setExecuteResultLob(ArchiveExecuteResultLob executeResultLob)
    {
        this.executeResultLob= executeResultLob;
    }


    @Override
    public ArchiveExecuteResultVO doGetVO()
    {
        ArchiveExecuteResultVO vo = new ArchiveExecuteResultVO();
        super.doGetBaseVO(vo);
        
        vo.setUAddress(getUAddress());
        vo.setUDuration(getUDuration());
        vo.setUReturncode(getUReturncode());
        vo.setUTarget(getUTarget());
        
        //references
        vo.setExecuteRequest(Hibernate.isInitialized(this.executeRequest) && getExecuteRequest() != null ? getExecuteRequest().doGetVO() : null);
        vo.setExecuteResultLob(Hibernate.isInitialized(this.executeResultLob) && getExecuteResultLob() != null ? getExecuteResultLob().doGetVO() : null);
        
        return vo;
    }

    @Override
    public void applyVOToModel(ArchiveExecuteResultVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo); 
            
            this.setUAddress(StringUtils.isNotBlank(vo.getUAddress()) && vo.getUAddress().equals(VO.STRING_DEFAULT) ? getUAddress() : vo.getUAddress());
            this.setUDuration(vo.getUDuration() != null && vo.getUDuration().equals(VO.INTEGER_DEFAULT) ? getUDuration() : vo.getUDuration());
            this.setUReturncode(vo.getUReturncode() != null && vo.getUReturncode().equals(VO.INTEGER_DEFAULT) ? getUReturncode() : vo.getUReturncode());
            this.setUTarget(StringUtils.isNotBlank(vo.getUTarget()) && vo.getUTarget().equals(VO.STRING_DEFAULT) ? getUTarget() : vo.getUTarget());
            
            //references
            this.setExecuteRequest(vo.getExecuteRequest() != null ? new ArchiveExecuteRequest(vo.getExecuteRequest()) : null);
            this.setExecuteResultLob(vo.getExecuteResultLob() != null ? new ArchiveExecuteResultLob(vo.getExecuteResultLob()) : null);

            
        }
        
    }

}
