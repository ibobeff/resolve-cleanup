/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.SocialUserNotificationVO;

@Entity
@Table(name = "social_user_notification")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SocialUserNotification  extends BaseModel<SocialUserNotificationVO> 
{
    private static final long serialVersionUID = -8771388825773913432L;
    
    private Users user;
//    private SocialNotifications notification;
//    private SocialNotificationRule notificationRule; //MAYBE WE CAN REMOVE THIS FOR NOW
    private ResolveNode node;
    
    //maybe we don't have to create a table for the rule.
    private String UNotification;// Mapped to UserGlobalNotificationContainerType
    private String UNotificationRule;
    private String UNotificationValue;
    
    public SocialUserNotification()
    {
    }

    public SocialUserNotification(String sysId)
    {
        super.setSys_id(sysId);
    }
    
    public SocialUserNotification(SocialUserNotificationVO vo)
    {
        applyVOToModel(vo);
    }
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_user_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public Users getUser()
    {
        return user;
    }

    public void setUser(Users user)
    {
        this.user = user;
    }

//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "u_social_notf_sys_id")
//    @NotFound(action = NotFoundAction.IGNORE)
//    public SocialNotifications getNotification()
//    {
//        return notification;
//    }
//
//    public void setNotification(SocialNotifications notification)
//    {
//        this.notification = notification;
//    }

//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "u_social_notf_rule_sys_id")
//    @NotFound(action = NotFoundAction.IGNORE)
//    public SocialNotificationRule getNotificationRule()
//    {
//        return notificationRule;
//    }
//
//    public void setNotificationRule(SocialNotificationRule notificationRule)
//    {
//        this.notificationRule = notificationRule;
//    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_node_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveNode getNode()
    {
        return node;
    }

    public void setNode(ResolveNode node)
    {
        this.node = node;
    }
    
    
    @Column(name = "u_notf_rule", length = 40)
    public String getUNotificationRule()
    {
        return UNotificationRule;
    }

    public void setUNotificationRule(String uNotificationRule)
    {
        UNotificationRule = uNotificationRule;
    }
    
    
    @Column(name = "u_notf_value", length = 40)
    public String getUNotificationValue()
    {
        return UNotificationValue;
    }

    public void setUNotificationValue(String uNotificationValue)
    {
        UNotificationValue = uNotificationValue;
    }
    
    @Column(name = "u_notification", length = 40)
    public String getUNotification()
    {
        return UNotification;
    }

    public void setUNotification(String uNotification)
    {
        UNotification = uNotification;
    }

    @Override
    public SocialUserNotificationVO doGetVO()
    {
        //*** NOT SURE IF WE WANT THIS FOR A RELATIONSHIP TABLE 
        return null;
    }

}
