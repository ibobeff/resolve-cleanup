/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.MetaFormTabVO;
import com.resolve.services.hibernate.vo.MetaxFormViewPanelVO;
import com.resolve.services.interfaces.VO;


@Entity
@Table(name = "metax_form_view_panel", indexes = {
                @Index(columnList = "u_meta_form_view_sys_id", name = "mxfvp_u_meta_form_view_pan_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaxFormViewPanel extends BaseModel<MetaxFormViewPanelVO>
{
    private static final long serialVersionUID = 5903821591792419640L;
    
    //    private String UPanelId;
    private String UPanelTitle;
    private Integer UOrder;
    private String UUrl;//if this has value, the 'metaFormTabFields' will be NULL or EMPTY
    private Integer UNoOfVerticalColumns; //1 or 2
    private Integer UPanelWidth;
//    private Boolean UIsTabPanel;
    private String UXType;//this will be rspanel or rstabpanel based on what kind of panel it is.
    
    // object referenced by
    private MetaFormView metaFormView;
//    private Collection<MetaxPanelFieldRel> metaxPanelFieldRels;
    private Collection<MetaFormTab> metaFormTabs;
    
    
    public MetaxFormViewPanel()
    {
    }
    
    public MetaxFormViewPanel(MetaxFormViewPanelVO vo)
    {
        applyVOToModel(vo);
    }
    
    /**
     * @return the uOrder
     */
    @Column(name = "u_order")
    public Integer getUOrder()
    {
        return UOrder;
    }
    /**
     * @param uOrder the uOrder to set
     */
    public void setUOrder(Integer uOrder)
    {
        UOrder = uOrder;
    }
    
    /**
     * @return the uNoOfVerticalColumns
     */
    @Column(name = "u_no_of_vertical_columns")
    public Integer getUNoOfVerticalColumns()
    {
        return UNoOfVerticalColumns;
    }
    public void setUNoOfVerticalColumns(Integer uNoOfVerticalColumns)
    {
        UNoOfVerticalColumns = uNoOfVerticalColumns;
    }

    /**
     * @return the uSource
     */
    @Column(name = "u_url", length = 4000)
    public String getUUrl()
    {
        return UUrl;
    }
    public void setUUrl(String uSource)
    {
        UUrl = uSource;
    }


    @Column(name = "u_panel_title", length = 200)
    public String getUPanelTitle()
    {
        return UPanelTitle;
    }

    public void setUPanelTitle(String uPanelTitle)
    {
        UPanelTitle = uPanelTitle;
    }

    @Column(name = "u_panel_width")
    public Integer getUPanelWidth()
    {
        return UPanelWidth;
    }

    public void setUPanelWidth(Integer uPanelWidth)
    {
        UPanelWidth = uPanelWidth;
    }

    @ManyToOne(fetch = FetchType.EAGER) //THIS IS DONE ON PURPOSE
    @JoinColumn(name = "u_meta_form_view_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaFormView getMetaFormView()
    {
        return metaFormView;
    }

    public void setMetaFormView(MetaFormView metaFormView)
    {
        this.metaFormView = metaFormView;

        if (metaFormView != null)
        {
            Collection<MetaxFormViewPanel> coll = metaFormView.getMetaxFormViewPanels();
            if (coll == null)
            {
                coll = new HashSet<MetaxFormViewPanel>();
                coll.add(this);
                
                metaFormView.setMetaxFormViewPanels(coll);
            }
        }
    }

//    @OneToMany(mappedBy = "metaxFormViewPanel")
//    @NotFound(action = NotFoundAction.IGNORE)
//    public Collection<MetaxPanelFieldRel> getMetaxPanelFieldRels()
//    {
//        return metaxPanelFieldRels;
//    }
//
//    public void setMetaxPanelFieldRels(Collection<MetaxPanelFieldRel> metaxPanelFieldRels)
//    {
//        this.metaxPanelFieldRels = metaxPanelFieldRels;
//    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "metaxFormViewPanel")
    @LazyCollection(LazyCollectionOption.FALSE)
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<MetaFormTab> getMetaFormTabs()
    {
        return this.metaFormTabs;
    }

    public void setMetaFormTabs(Collection<MetaFormTab> metaFormTabs)
    {
        this.metaFormTabs = metaFormTabs;
    }

    @Column(name = "u_xtype", length=50)
    public String getUXType()
    {
        return UXType;
    }

    public void setUXType(String uXType)
    {
        UXType = uXType;
    }

    @Override
    public MetaxFormViewPanelVO doGetVO()
    {
        MetaxFormViewPanelVO vo = new MetaxFormViewPanelVO();
        super.doGetBaseVO(vo);
        
        vo.setUNoOfVerticalColumns(getUNoOfVerticalColumns());
        vo.setUOrder(getUOrder());
        vo.setUPanelTitle(getUPanelTitle());
        vo.setUPanelWidth(getUPanelWidth());
        vo.setUUrl(getUUrl());
        vo.setUXType(getUXType());
        
        Collection<MetaFormTab> metaFormTabs = Hibernate.isInitialized(this.metaFormTabs) ? getMetaFormTabs() : null;
        if(metaFormTabs != null && metaFormTabs.size() > 0)
        {
            Collection<MetaFormTabVO> vos = new ArrayList<MetaFormTabVO>();
            for(MetaFormTab model : metaFormTabs)
            {
                vos.add(model.doGetVO());
            }
            vo.setMetaFormTabs(vos);
        }
        
        return vo;
    }

    @Override
    public void applyVOToModel(MetaxFormViewPanelVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);   
            
            this.setUNoOfVerticalColumns((Integer) VO.evaluateValue(vo.getUNoOfVerticalColumns(), getUNoOfVerticalColumns()));
            this.setUOrder((Integer) VO.evaluateValue(vo.getUOrder(), getUOrder()));
            this.setUPanelTitle((String) VO.evaluateValue(vo.getUPanelTitle(), getUPanelTitle()));
            this.setUPanelWidth((Integer) VO.evaluateValue(vo.getUPanelWidth(), getUPanelWidth()));
            this.setUUrl((String) VO.evaluateValue(vo.getUUrl(), getUUrl()));
            this.setUXType((String) VO.evaluateValue(vo.getUXType(), getUXType()));
            
            Collection<MetaFormTabVO> metaFormTabsVOs = vo.getMetaFormTabs();
            if(metaFormTabsVOs != null && metaFormTabsVOs.size() > 0)
            {
                Collection<MetaFormTab> models = new ArrayList<MetaFormTab>();
                for(MetaFormTabVO refVO : metaFormTabsVOs)
                {
                    models.add(new MetaFormTab(refVO));
                }
                this.setMetaFormTabs(models);
            }
        }
        
    }
    
    
    

}//MetaFormTab
