package com.resolve.persistence.util;

import org.hibernate.CacheMode;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class HibernateProxy {

	public static Object execute(HibernateMethod hibernateMethod) throws Exception {

		Session session = HibernateUtil.getSession();
		Transaction tx = HibernateUtil.beginTransaction(session);

		try {
			Object result = hibernateMethod.execute();
			HibernateUtil.commitTransaction(tx);
			return result;

		} catch (Exception e) {
			HibernateUtil.rollbackTransaction(tx);
			throw e;
		} 
	}

	public static void execute(HibernateVoidMethod hibernateVoidMethod) throws Exception {

		Session session = HibernateUtil.getSession();
		Transaction tx = HibernateUtil.beginTransaction(session);

		try {

			hibernateVoidMethod.execute();
			HibernateUtil.commitTransaction(tx);

		} catch (Exception e) {
			HibernateUtil.rollbackTransaction(tx);
			throw e;
		} 
	}

	
	
	public static Object executeNoCache(HibernateMethod hibernateMethod) throws Exception {

		Session session = HibernateUtil.getSession();
		Transaction tx = HibernateUtil.beginTransaction(session);
		session.setCacheMode(CacheMode.IGNORE);

		try {
			Object result = hibernateMethod.execute();
			HibernateUtil.commitTransaction(tx);
			return result;

		} catch (Exception e) {
			HibernateUtil.rollbackTransaction(tx);
			throw e;
		} 
	}
	
	public static void executeNoCache(HibernateVoidMethod hibernateVoidMethod) throws Exception {

		Session session = HibernateUtil.getSession();
		Transaction tx = HibernateUtil.beginTransaction(session);
		session.setCacheMode(CacheMode.IGNORE);

		try {

			hibernateVoidMethod.execute();
			HibernateUtil.commitTransaction(tx);

		} catch (Exception e) {
			HibernateUtil.rollbackTransaction(tx);
			throw e;
		}
	}

	/*
	 * Variants of above methods guarded against Hibernate session factory
	 * re-initialization due to creation of new custom table or update 
	 * (addition of field(s)/column(s)) into existing custom table. 
	 */
	
	public static Object executeHibernateInitLocked(HibernateMethod hibernateMethod) throws Throwable {

		HibernateUtil.lockHibernateInit();
		
		try {
			return execute(hibernateMethod);
		} finally {
			HibernateUtil.unlockHibernateInit();
		}
	}
	
	public static void executeHibernateInitLocked(HibernateVoidMethod hibernateVoidMethod) throws Throwable {

		HibernateUtil.lockHibernateInit();

		try {
			execute(hibernateVoidMethod);
		} finally {
			HibernateUtil.unlockHibernateInit();;
		}
	}
	
	public static Object executeNoCacheHibernateInitLocked(HibernateMethod hibernateMethod) throws Throwable {
		HibernateUtil.lockHibernateInit();
		
		try {
			return executeNoCache(hibernateMethod);
		} finally {
			HibernateUtil.unlockHibernateInit();
		}
	}
	
	public static void executeNoCacheHibernateInitLocked(HibernateVoidMethod hibernateVoidMethod) throws Throwable {
		HibernateUtil.lockHibernateInit();
		
		try {
			executeNoCache(hibernateVoidMethod);
		} finally {
			HibernateUtil.unlockHibernateInit();
		}
	}
	
	public static void setCurrentUser(String username) {
		HibernateUtil.setCurrentUserByUsername(username);
	}
}
