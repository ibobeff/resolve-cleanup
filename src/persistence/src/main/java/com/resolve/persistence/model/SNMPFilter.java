/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

//Generated Dec 11, 2009 11:15:20 AM by Hibernate Tools 3.2.4.GA

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.services.hibernate.vo.SNMPFilterVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

/**
 * SNMPFilter generated by hbm2java
 */
@Entity
@Table(name = "snmp_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }, name = "snmpf_u_name_u_queue_uk") })
// @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SNMPFilter extends GatewayFilter<SNMPFilterVO>
{
    private static final long serialVersionUID = 621829280400832086L;

    private Boolean UTrapReceiver;
    private String USnmpTrapOid;
    private Integer USNMPVersion;
    private String UIPAddresses;
    private String UReadCommunity;
    private Integer UTimeout;
    private Integer URetries;
    private String UOid;
    private String URegex;
    private String UComparator;
    private Integer UValue;

    // object references

    // object referenced by
    public SNMPFilter()
    {
    } // TSRMFilter

    public SNMPFilter(SNMPFilterVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_trap_receiver")
    public Boolean getUTrapReceiver()
    {
        return UTrapReceiver;
    }

    public void setUTrapReceiver(Boolean uTrapReceiver)
    {
        UTrapReceiver = uTrapReceiver;
    }

    @Column(name = "u_snmp_trap_oid")
    public String getUSnmpTrapOid()
    {
        return USnmpTrapOid;
    }

    public void setUSnmpTrapOid(String uSnmpTrapOid)
    {
        USnmpTrapOid = uSnmpTrapOid;
    }

    @Column(name = "u_snmp_version")
    public Integer getUSNMPVersion()
    {
        return USNMPVersion;
    }

    public void setUSNMPVersion(Integer uSNMPVersion)
    {
        USNMPVersion = uSNMPVersion;
    }

    @Lob
    @Column(name = "u_ip_addresses", length = 16777215)
    public String getUIPAddresses()
    {
        return UIPAddresses;
    }

    public void setUIPAddresses(String uIPAddresses)
    {
        UIPAddresses = uIPAddresses;
    }

    @Column(name = "u_read_community")
    public String getUReadCommunity()
    {
        return UReadCommunity;
    }

    public void setUReadCommunity(String uReadCommunity)
    {
        this.UReadCommunity = uReadCommunity;
    }

    @Column(name = "u_timeout")
    public Integer getUTimeout()
    {
        return UTimeout;
    }

    public void setUTimeout(Integer uTimeout)
    {
        UTimeout = uTimeout;
    }

    @Column(name = "u_retries")
    public Integer getURetries()
    {
        return URetries;
    }

    public void setURetries(Integer uRetries)
    {
        URetries = uRetries;
    }

    @Lob
    @Column(name = "u_oid", length = 16777215)
    public String getUOid()
    {
        return UOid;
    }

    public void setUOid(String uOid)
    {
        UOid = uOid;
    }

    @Lob
    @Column(name = "u_regex", length = 16777215)
    public String getURegex()
    {
        return this.URegex;
    } // getURegex

    public void setURegex(String uRegex)
    {
        this.URegex = uRegex;
    } // setURegex

    @Column(name = "u_comparator", length = 40)
    public String getUComparator()
    {
        return UComparator;
    }

    public void setUComparator(String uComparator)
    {
        UComparator = uComparator;
    }

    @Column(name = "u_value")
    public Integer getUValue()
    {
        return UValue;
    }

    public void setUValue(Integer uValue)
    {
        UValue = uValue;
    }

    @Override
    public SNMPFilterVO doGetVO()
    {
        SNMPFilterVO vo = new SNMPFilterVO();
        super.doGetBaseVO(vo);

        vo.setUComparator(getUComparator());
        vo.setUIpAddresses(getUIPAddresses());
        vo.setUOid(getUOid());
        vo.setUReadCommunity(getUReadCommunity());
        vo.setUTrapReceiver(getUTrapReceiver());
        vo.setURetries(getURetries());
        vo.setUSnmpTrapOid(getUSnmpTrapOid());
        vo.setUSnmpVersion(getUSNMPVersion());
        vo.setUTimeout(getUTimeout());
        vo.setURegex(getURegex());
        vo.setUValue(getUValue());

        return vo;
    }

    @Override
    public void applyVOToModel(SNMPFilterVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setUComparator(StringUtils.isNotBlank(vo.getUComparator()) && vo.getUComparator().equals(VO.STRING_DEFAULT) ? getUComparator() : vo.getUComparator());
            this.setUIPAddresses(StringUtils.isNotBlank(vo.getUIpAddresses()) && vo.getUIpAddresses().equals(VO.STRING_DEFAULT) ? getUIPAddresses() : vo.getUIpAddresses());
            this.setUOid(StringUtils.isNotBlank(vo.getUOid()) && vo.getUOid().equals(VO.STRING_DEFAULT) ? getUOid() : vo.getUOid());
            this.setUReadCommunity(StringUtils.isNotBlank(vo.getUReadCommunity()) && vo.getUReadCommunity().equals(VO.STRING_DEFAULT) ? getUReadCommunity() : vo.getUReadCommunity());
            this.setUSnmpTrapOid(StringUtils.isNotBlank(vo.getUSnmpTrapOid()) && vo.getUSnmpTrapOid().equals(VO.STRING_DEFAULT) ? getUSnmpTrapOid() : vo.getUSnmpTrapOid());
            this.setURegex(StringUtils.isNotBlank(vo.getURegex()) && vo.getURegex().equals(VO.STRING_DEFAULT) ? getURegex() : vo.getURegex());

            this.setUValue(vo.getUValue() != null && vo.getUValue().equals(VO.INTEGER_DEFAULT) ? getUValue() : vo.getUValue());
            this.setUTimeout(vo.getUTimeout() != null && vo.getUTimeout().equals(VO.INTEGER_DEFAULT) ? getUTimeout() : vo.getUTimeout());
            this.setUSNMPVersion(vo.getUSnmpVersion() != null && vo.getUSnmpVersion().equals(VO.INTEGER_DEFAULT) ? getUSNMPVersion() : vo.getUSnmpVersion());
            this.setURetries(vo.getURetries() != null && vo.getURetries().equals(VO.INTEGER_DEFAULT) ? getURetries() : vo.getURetries());
            this.setUTrapReceiver(vo.getUTrapReceiver() != null ? vo.getUTrapReceiver() : getUTrapReceiver());
        }
    }
    
    @Override
    public List<String> ugetCdataProperty()
    {
        List<String> list = super.ugetCdataProperty();
        list.add("UIPAddresses");
        list.add("UOid");
        list.add("URegex");
        
        return list;
    }//ugetCdataProperty
}
