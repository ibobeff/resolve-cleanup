package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.interfaces.VO;
import com.resolve.services.hibernate.vo.PushGatewayFilterVO;
import com.resolve.services.hibernate.vo.PushGatewayFilterAttrVO;

@Entity
@Table(name = "push_gateway_filter", 
	   uniqueConstraints = {
			   			       @UniqueConstraint(columnNames = { "u_name", "u_queue" }, name = "pushgf_u_name_u_queue_uk") 
			   			   },
	   indexes = {
				     @Index(columnList = "u_gateway_name", name = "pushgf_gtw_queue_idx"),
				   	 @Index(columnList = "u_queue", name = "pushgf_gtw_queue_idx"),
				   	 @Index(columnList = "u_name", name = "pushgf_filter_gtw_queue_idx"),
				   	 @Index(columnList = "u_gateway_name", name = "pushgf_filter_gtw_queue_idx"),
				   	 @Index(columnList = "u_queue", name = "pushgf_filter_gtw_queue_idx")
				 })
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PushGatewayFilter extends GatewayFilter<PushGatewayFilterVO> {

    private static final long serialVersionUID = 1L;

    private String UGatewayName;    
    private Boolean UDeployed;
    private Boolean UUpdated;
    private Integer UPort;
    private String UBlocking;
    private String UUri;
    private Boolean USsl;
    
    // object referenced by
    private Collection<PushGatewayFilterAttr> attrs;

    public PushGatewayFilter() {
    }

    public PushGatewayFilter(PushGatewayFilterVO vo) {
        applyVOToModel(vo);
    }
    
    @Column(name = "u_gateway_name", length = 40)
    public String getUGatewayName()
    {
        return UGatewayName;
    }

    public void setUGatewayName(String uGatewayName)
    {
        UGatewayName = uGatewayName;
    }
    
    @Column(name = "u_deployed")
    public Boolean getUDeployed()
    {
        return UDeployed;
    }

    public void setUDeployed(Boolean uDeployed)
    {
        UDeployed = uDeployed;
    }

    @Column(name = "u_updated")
    public Boolean getUUpdated()
    {
        return UUpdated;
    }

    public void setUUpdated(Boolean uUpdated)
    {
        UUpdated = uUpdated;
    }

    @Column(name = "u_port")
    public Integer getUPort() {
        return this.UPort;
    }

    public void setUPort(Integer uPort) {
        this.UPort = uPort;
    }

    @Column(name = "u_blocking", length = 40)
    public String getUBlocking() {
        return this.UBlocking;
    }

    public void setUBlocking(String uBlocking) {
        this.UBlocking = uBlocking;
    }

    @Column(name = "u_uri", length = 255)
    public String getUUri() {
        return this.UUri;
    }

    public void setUUri(String uUri) {
        this.UUri = uUri;
    }

    @Column(name = "u_ssl", length = 1)
    public Boolean getUSsl() {
        return this.USsl;
    }

    public void setUSsl(Boolean uSsl) {
        this.USsl = uSsl;
    }

    // Deprecated @ForeignKey should be used to match the other side of relationship
//    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pushGatewayFilter", cascade = CascadeType.ALL)
    @OneToMany(mappedBy = "pushGatewayFilter", cascade = CascadeType.ALL)
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<PushGatewayFilterAttr> getAttrs()
    {
        return attrs;
    }

    public void setAttrs(Collection<PushGatewayFilterAttr> attrs)
    {
        this.attrs = attrs;
    }

    public PushGatewayFilterVO doGetVO() {
        
        PushGatewayFilterVO vo = new PushGatewayFilterVO();
        super.doGetBaseVO(vo);

        vo.setUGatewayName(getUGatewayName());
        vo.setUDeployed(getUDeployed());
        vo.setUUpdated(getUUpdated());
        
        vo.setUPort(getUPort());
        vo.setUBlocking(getUBlocking());
        vo.setUUri(getUUri());
        vo.setUSsl(getUSsl());
        
        //references
        Collection<PushGatewayFilterAttr> attrModels = Hibernate.isInitialized(this.attrs) ? getAttrs() : null;
        if(attrModels != null && attrModels.size() > 0)
        {
            Collection<PushGatewayFilterAttrVO> vos = new ArrayList<PushGatewayFilterAttrVO>();

            for(PushGatewayFilterAttr model:attrs)
                vos.add(model.doGetVO());
            
            vo.setAttrsCol(vos);
        }        
        
        return vo;
    }

    @Override
    public void applyVOToModel(PushGatewayFilterVO vo) {
        
        if (vo == null) return;
        
        super.applyVOToModel(vo);

        if (!VO.STRING_DEFAULT.equals(vo.getUGatewayName())) this.setUGatewayName(vo.getUGatewayName()); else ;
        if (!VO.BOOLEAN_DEFAULT.equals(vo.getUDeployed())) this.setUDeployed(vo.getUDeployed()); else ;
        if (!VO.BOOLEAN_DEFAULT.equals(vo.getUUpdated())) this.setUUpdated(vo.getUUpdated()); else ;
        if (!VO.INTEGER_DEFAULT.equals(vo.getUPort())) this.setUPort(vo.getUPort()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUBlocking())) this.setUBlocking(vo.getUBlocking()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUUri())) this.setUUri(vo.getUUri()); else ;
        this.setUSsl(vo.getUSsl());
        
        //references
        Collection<PushGatewayFilterAttrVO> attrVOs = vo.getAttrsCol();
        if(attrVOs != null && attrVOs.size() > 0)
        {
            Collection<PushGatewayFilterAttr> models = new ArrayList<PushGatewayFilterAttr>();
            
            for(PushGatewayFilterAttrVO attrVO : attrVOs)
                models.add(new PushGatewayFilterAttr(attrVO));

            this.setAttrs(models);
        }
    }
    
} // class PushGatewayFilter