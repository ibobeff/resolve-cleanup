package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.resolve.services.hibernate.vo.ArchiveSirArtifactVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "archive_sir_artifact")
public class ArchiveSirArtifact extends BaseModel<ArchiveSirArtifactVO>
{
	private static final long serialVersionUID = -3306308720220649445L;
	
	private String name;
	private String value;
	private String activityId;
	private String description;
	private String incidentId;
	private String worksheetId;
	private String sir;
	private String source;         // name of a source which created this note.
    private String sourceValue;    // the value of the source.
    private String sourceAndValue; // for internal use only
	
	@Column(name = "u_name", nullable=false, length = 100)
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	
	@Column(name = "u_value", nullable=false, length = 4000)
	public String getValue()
	{
		return value;
	}
	public void setValue(String value)
	{
		this.value = value;
	}
	
	@Column(name = "u_activity_id", length = 32)
	public String getActivityId()
    {
        return activityId;
    }
    public void setActivityId(String activityId)
    {
        this.activityId = activityId;
    }
    
    @Column(name = "u_description", length = 4000)
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }
    
    @Column(name = "u_incident_id", length = 32)
    public String getIncidentId()
    {
        return incidentId;
    }
    public void setIncidentId(String incidentId)
    {
        this.incidentId = incidentId;
    }
    
    @Column(name = "u_worksheet_id", length = 32)
    public String getWorksheetId()
    {
        return worksheetId;
    }
    public void setWorksheetId(String worksheetId)
    {
        this.worksheetId = worksheetId;
    }
    
    @Column(name = "u_sir", length = 32)
    public String getSir()
    {
        return sir;
    }
    public void setSir(String sir)
    {
        this.sir = sir;
    }
    
    @Column(name = "u_source", length = 50)
    public String getSource()
    {
        return source;
    }
    public void setSource(String source)
    {
        this.source = source;
    }
    
    @Column(name = "u_source_value", length = 100)
    public String getSourceValue()
    {
        return sourceValue;
    }
    public void setSourceValue(String sourceValue)
    {
        this.sourceValue = sourceValue;
    }
    
    @Column(name = "u_source_and_value")
    public String getSourceAndValue()
    {
        return sourceAndValue;
    }
    public void setSourceAndValue(String sourceAndValue)
    {
        this.sourceAndValue = sourceAndValue;
    }
    
    @Override
	public ArchiveSirArtifactVO doGetVO()
	{
		ArchiveSirArtifactVO vo = new ArchiveSirArtifactVO();
		super.doGetBaseVO(vo);
		
		vo.setName(this.getName());
		vo.setValue(this.getValue());
		vo.setActivityId(this.getActivityId());
		vo.setDescription(this.getDescription());
		vo.setIncidentId(this.getIncidentId());
		vo.setWorksheetId(this.getWorksheetId());
		vo.setSir(this.getSir());
		vo.setSource(this.getSource());
        vo.setSourceValue(this.getSourceValue());
        vo.setSourceAndValue(this.getSourceAndValue());
		
		return vo;
	}
	
	@Override
	public void applyVOToModel(ArchiveSirArtifactVO vo)
	{
		if (vo != null)
		{
			super.applyVOToModel(vo);
			this.setName(StringUtils.isNotBlank(vo.getName()) && vo.getName().equals(VO.STRING_DEFAULT) ? getName() : vo.getName());
			this.setValue(StringUtils.isNotBlank(vo.getValue()) && vo.getValue().equals(VO.STRING_DEFAULT) ? getValue() : vo.getValue());
			this.setActivityId(StringUtils.isNotBlank(vo.getActivityId()) && vo.getActivityId().equals(VO.STRING_DEFAULT) ? getActivityId() : vo.getActivityId());
			this.setDescription(StringUtils.isNotBlank(vo.getDescription()) && vo.getDescription().equals(VO.STRING_DEFAULT) ? getDescription() : vo.getDescription());
			this.setWorksheetId(StringUtils.isNotBlank(vo.getWorksheetId()) && vo.getWorksheetId().equals(VO.STRING_DEFAULT) ? getWorksheetId() : vo.getWorksheetId());
            this.setIncidentId(StringUtils.isNotBlank(vo.getIncidentId()) && vo.getIncidentId().equals(VO.STRING_DEFAULT) ? getIncidentId() : vo.getIncidentId());
            this.setSir(StringUtils.isNotBlank(vo.getSir()) && vo.getSir().equals(VO.STRING_DEFAULT) ? getSir() : vo.getSir());
            this.setSource(StringUtils.isNotBlank(vo.getSource()) && vo.getSource().equals(VO.STRING_DEFAULT) ? getSource() : vo.getSource());
            this.setSourceValue(StringUtils.isNotBlank(vo.getSourceValue()) && vo.getSourceValue().equals(VO.STRING_DEFAULT) ? getSourceValue() : vo.getSourceValue());
            this.setSourceAndValue(StringUtils.isNotBlank(vo.getSourceAndValue()) && vo.getSourceAndValue().equals(VO.STRING_DEFAULT) ? getSourceAndValue() : vo.getSourceAndValue());
		}
	}
}
