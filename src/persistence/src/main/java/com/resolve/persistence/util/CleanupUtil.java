/******************************************************************************
* (C) Copyright 2014
import com.resolve.persistence.util.HibernateUtil
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import java.util.List;

import org.hibernate.query.Query;

import com.resolve.util.Log;
import com.resolve.util.SQLUtils;

/**
 * Purpose of this Util is to cleanup the data
 * 
 * @author jeet.marwah
 *
 */
public class CleanupUtil
{
    /**
     * use to cleanup the data, update the references with null if the
     * references are deleted Specially in case where we are using the
     * GenericAPI and there are dangling references. For eg, if a Preprocessor
     * is deleted, the ActionInvoc still has a ref to it.
     * 
     * @param modelName
     *            - ResolveActionInvoc, ResolveActionTask, etc
     * @param attibuteName
     *            - attribute of the model
     * @param queryString
     *            - '123123', '13131' - values that needs to be updated with
     *            null
     */
    public static void updateDeletedRefWithNull(String modelName, String attibuteName, List<String> sys_ids)
    {
        if (sys_ids != null && sys_ids.size() > 0)
        {
            String queryString = SQLUtils.prepareQueryString(sys_ids);

            try
            {
                String updateSql = "update " + modelName + " set " + attibuteName + " = null where " + attibuteName + " IN (" + queryString + ") ";
                executeHibernateSQL(updateSql, "resolve.maint");
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

    }// updateDeletedRefWithNull
    
    /**
     * to delete the records
     * 
     * @param modelName
     * @param attibuteName
     * @param sys_ids
     */
    public static void deleteDependents(String modelName, String attibuteName, List<String> sys_ids)
    {
        if (sys_ids != null && sys_ids.size() > 0)
        {
            String queryString = SQLUtils.prepareQueryString(sys_ids);

            try
            {
                String deleteSql = "delete from " + modelName + " where " + attibuteName + " IN (" + queryString + ") ";
                executeHibernateSQL(deleteSql, "resolve.maint");
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

    }// updateDeletedRefWithNull
    
    /**
     * utility method for insert, update and delete
     * 
     * @param sql
     * @throws WikiException
     */
    @SuppressWarnings("rawtypes")
    public static void executeHibernateSQL(String sql, String username) throws Exception
    {
        Log.log.debug("Executing SQL -->");
        Log.log.debug(sql);
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
	            Query query = HibernateUtil.createQuery(sql);
	            query.executeUpdate();
            });
        }
        catch (Exception e)
        {
                      HibernateUtil.rethrowNestedTransaction(e);
            Log.log.error("Error executing sql -->" + sql, e);
        }
    }
    
}//CleanupUtil
