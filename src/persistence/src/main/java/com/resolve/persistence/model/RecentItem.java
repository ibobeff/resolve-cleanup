/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.resolve.services.hibernate.vo.RecentItemVO;

@Entity
@Table(name = "recent_item")
public class RecentItem extends BaseModel<RecentItemVO>
{
	private static final long serialVersionUID = 7098471529375219278L;

    private String itemSysId;
    
    private String itemType;
    
    @Column(name = "item_sys_id", length = 32)
	public String getItemSysId() {
		return itemSysId;
	}
	
	public void setItemSysId(String itemSysId) {
		this.itemSysId = itemSysId;
	}
	
	@Column(name = "item_type", length = 30)
	public String getItemType() {
		return itemType;
	}
	
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	
	@Override
	public void applyVOToModel(RecentItemVO vo) {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            this.setItemSysId(vo.getItemSysId());
            this.setItemType(vo.getItemType());
        }		
	}
	
	@Override
	public RecentItemVO doGetVO() {
		
		RecentItemVO vo = new RecentItemVO();
        super.doGetBaseVO(vo);

        vo.setItemSysId(getItemSysId());
        vo.setItemType(getItemType());
        
        return vo;
	}
}
