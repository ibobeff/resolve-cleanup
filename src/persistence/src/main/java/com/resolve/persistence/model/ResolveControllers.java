/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

// Generated Dec 11, 2009 11:15:20 AM by Hibernate Tools 3.2.4.GA

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolveControllersVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "resolve_controller")
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveControllers extends BaseModel<ResolveControllersVO>
{
    private static final long serialVersionUID = -9167720921596915750L;
    
    private String UControllerName;
    private String UDescription;

    // object references

    // object referenced by
    private Collection<ResolveAppControllerRel> resolveAppControllerRels;

    public ResolveControllers()
    {
    }

    public ResolveControllers(ResolveControllersVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_controller_name", length = 3000)
    public String getUControllerName()
    {
        return UControllerName;
    }

    public void setUControllerName(String uControllerName)
    {
        UControllerName = uControllerName;
    }

    @Column(name = "u_description", length = 3000)
    public String getUDescription()
    {
        return UDescription;
    }

    public void setUDescription(String uDescription)
    {
        UDescription = uDescription;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "resolveController")
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.DELETE)
    public Collection<ResolveAppControllerRel> getResolveAppControllerRels()
    {
        return resolveAppControllerRels;
    }

    public void setResolveAppControllerRels(Collection<ResolveAppControllerRel> resolveAppControllerRels)
    {
        this.resolveAppControllerRels = resolveAppControllerRels;
    }

    @Override
    public ResolveControllersVO doGetVO()
    {
        ResolveControllersVO vo = new ResolveControllersVO();
        super.doGetBaseVO(vo);

        vo.setUControllerName(getUControllerName());
        vo.setUDescription(getUDescription());
        
        //DO NOT REFER TO REL AS THAT WILL LEAD TO INFINITE LOOP. 
        //we want to get a list of controllers for an app

        return vo;
    }

    @Override
    public void applyVOToModel(ResolveControllersVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setUControllerName(StringUtils.isNotBlank(vo.getUControllerName()) && vo.getUControllerName().equals(VO.STRING_DEFAULT) ? getUControllerName() : vo.getUControllerName());
            this.setUDescription(StringUtils.isNotBlank(vo.getUDescription()) && vo.getUDescription().equals(VO.STRING_DEFAULT) ? getUDescription() : vo.getUDescription());
        }

    }

} // AlertLog
