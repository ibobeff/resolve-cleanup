/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.dao;

import com.resolve.util.Log;

public abstract class DAOFactory
{
    /**
     * Creates a standalone DAOFactory that returns unmanaged DAO beans for use
     * in any environment Hibernate has been configured for. Uses
     * HibernateUtil/SessionFactory and Hibernate context propagation
     * (CurrentSessionContext), thread-bound or transaction-bound, and
     * transaction scoped.
     */
    @SuppressWarnings("rawtypes")
    public static final Class HIBERNATE = com.resolve.persistence.dao.hibernate.HibernateDAOFactory.class;

    /**
     * Factory method for instantiation of concrete factories.
     */
    @SuppressWarnings("rawtypes")
    public static DAOFactory instance(Class factory)
    {
        Log.log.debug("Inside DAOFactory.instance(Class factory)");
        try
        {
            if (Log.log.isTraceEnabled())
            {
                Log.log.trace("Creating concrete DAO factory: " + factory);
            }
            return (DAOFactory) factory.newInstance();
        }
        catch (Exception ex)
        {
            throw new RuntimeException("Couldn't create DAOFactory: " + factory);
        }
    }

    public static DAOFactory instance()
    {
        return instance(HIBERNATE);
    }

    public abstract AuditLogDAO getAuditLogDAO();

    public abstract AlertLogDAO getAlertLogDAO();

    public abstract GatewayLogDAO getGatewayLogDAO();

    public abstract NetcoolFilterDAO getNetcoolFilterDAO();

    public abstract ExchangeserverFilterDAO getExchangeserverFilterDAO();

    public abstract EmailFilterDAO getEmailFilterDAO();

    public abstract EmailAddressDAO getEmailAddressDAO();

    public abstract ServiceNowFilterDAO getServiceNowFilterDAO();

    public abstract SalesforceFilterDAO getSalesforceFilterDAO();

    public abstract TSRMFilterDAO getTSRMFilterDAO();

    public abstract DatabaseFilterDAO getDatabaseFilterDAO();

    public abstract DatabaseConnectionPoolDAO getDatabaseConnectionPoolDAO();

    public abstract XMPPFilterDAO getXMPPFilterDAO();

    public abstract XMPPAddressDAO getXMPPAddressDAO();

    public abstract SNMPFilterDAO getSNMPFilterDAO();

    public abstract ResolveActionInvocDAO getResolveActionInvocDAO();

    public abstract ResolveActionInvocOptionsDAO getResolveActionInvocOptionsDAO();

    public abstract ResolveActionParameterDAO getResolveActionParameterDAO();

    public abstract ResolveTagDAO getResolveTagDAO();

    public abstract ResolveActionPt2RelationDAO getResolveActionPt2RelationDAO();

    public abstract ResolveActionTaskDAO getResolveActionTaskDAO();
    
    public abstract ResolveActionTaskArchiveDAO getResolveActionTaskArchiveDAO();

    public abstract ResolveAssessDAO getResolveAssessDAO();

    public abstract ResolveCronDAO getResolveCronDAO();

    public abstract ResolveEventDAO getResolveEventDAO();

    public abstract ResolveEventHandlerDAO getResolveEventHandlerDAO();

    public abstract ResolveExecuteDependencyDAO getResolveExecuteDependencyDAO();

    public abstract ResolveExecuteLogDAO getResolveExecuteLogDAO();

    public abstract ResolveImpexGlideDAO getResolveImpexGlideDAO();

    public abstract ResolveImpexModuleDAO getResolveImpexModuleDAO();

    public abstract ResolveImpexWikiDAO getResolveImpexWikiDAO();

    public abstract ResolveParserDAO getResolveParserDAO();

    public abstract ResolveParserTemplateDAO getResolveParserTemplateDAO();

    public abstract ResolvePreprocessDAO getResolvePreprocessDAO();

    public abstract ResolvePropertiesDAO getResolvePropertiesDAO();

    public abstract ResolveRegistrationDAO getResolveRegistrationDAO();
    
    public abstract MSGGatewayFilterDAO getMSGGatewayFilterDAO();
    public abstract MSGGatewayFilterAttrDAO getMSGGatewayFilterAttrDAO();
    
    //Adding Pull gateway for the new SDK
    public abstract PullGatewayFilterDAO getPullGatewayFilterDAO();
    public abstract PullGatewayFilterAttrDAO getPullGatewayFilterAttrDAO();
    
    //DEC-32 Adding Push gateway for the new SDK
    public abstract PushGatewayFilterDAO getPushGatewayFilterDAO();
    public abstract PushGatewayFilterAttrDAO getPushGatewayFilterAttrDAO();
    
    public abstract ResolveRegistrationPropertyDAO getResolveRegistrationPropertyDAO();

    public abstract ResolveSessionDAO getResolveSessionDAO();

    public abstract ResolveSysScriptDAO getResolveSysScriptDAO();

    public abstract ResolveTriggerActionDAO getResolveTriggerActionDAO();

    public abstract ResolveWikiLookupDAO getResolveWikiLookupDAO();

    public abstract SysAppApplicationDAO getSysAppApplicationDAO();

    public abstract SysAppApplicationrolesDAO getSysAppApplicationrolesDAO();

    public abstract SysAppModuleDAO getSysAppModuleDAO();

    public abstract SysAppModulerolesDAO getSysAppModulerolesDAO();

    public abstract SysPerspectiveapplicationsDAO getSysPerspectiveapplicationsDAO();

    public abstract SysPerspectiveDAO getSysPerspectiveDAO();

    public abstract SysPerspectiverolesDAO getSysPerspectiverolesDAO();

    public abstract UsersDAO getUsersDAO();

    public abstract ResolveSequenceGeneratorDAO getResolveSequenceGeneratorDAO();

    public abstract AccessRightsDAO getAccessRightsDAO();

    public abstract GroupRoleRelDAO getGroupRoleRelDAO();

    public abstract GroupsDAO getGroupsDAO();

    public abstract RolesDAO getRolesDAO();

    public abstract UserRoleRelDAO getUserRoleRelDAO();

    public abstract UserGroupRelDAO getUserGroupRelDAO();

    public abstract WikiAttachmentDAO getWikiAttachmentDAO();

    public abstract WikiAttachmentContentDAO getWikiAttachmentContentDAO();

    public abstract WikiDocumentDAO getWikiDocumentDAO();

    public abstract WikiArchiveDAO getWikiArchiveDAO();

    public abstract ResolveArchiveDAO getResolveArchiveDAO();

    public abstract PropertiesDAO getPropertiesDAO();

    public abstract ContentWikidocWikidocRelDAO getContentWikidocWikidocRelDAO();

    public abstract MainWikidocWikidocRelDAO getMainWikidocWikidocRelDAO();

    public abstract ExceptionWikidocWikidocRelDAO getExceptionWikidocWikidocRelDAO();

    public abstract FinalWikidocWikidocRelDAO getFinalWikidocWikidocRelDAO();

    public abstract DTWikidocWikidocRelDAO getDTWikidocWikidocRelDAO();

    public abstract ContentWikidocActiontaskRelDAO getContentWikidocActiontaskRelDAO();

    public abstract MainWikidocActiontaskRelDAO getMainWikidocActiontaskRelDAO();

    public abstract ExceptionWikidocActiontaskRelDAO getExceptionWikidocActiontaskRelDAO();

    public abstract FinalWikidocActiontaskRelDAO getFinalWikidocActiontaskRelDAO();

    public abstract ResolveActionTaskResolveTagRelDAO getResolveActionTaskResolveTagRelDAO();

    public abstract WikidocResolveTagRelDAO getWikidocResolveTagRelDAO();

    public abstract WikidocStatisticsDAO getWikidocStatisticsDAO();

    public abstract ResolveBusinessRuleDAO getResolveBusinessRuleDAO();

    public abstract RoleWikidocHomepageRelDAO getRoleWikidocHomepageRelDAO();

    public abstract WikidocAttachmentRelDAO getWikidocAttachmentRelDAO();

    public abstract MetaAccessRightsDAO getMetaAccessRightsDAO();

    public abstract MetaStyleDAO getMetaStyleDAO();

    public abstract CustomTableDAO getCustomTableDAO();

    public abstract MetaTableDAO getMetaTableDAO();

    public abstract MetaFilterDAO getMetaFilterDAO();

    public abstract MetaTableViewDAO getMetaTableViewDAO();

    public abstract MetaControlDAO getMetaControlDAO();

    public abstract MetaControlItemDAO getMetaControlItemDAO();

    public abstract MetaTableViewFieldDAO getMetaTableViewFieldDAO();

    public abstract MetaViewFieldDAO getMetaViewFieldDAO();

    public abstract MetaFieldDAO getMetaFieldDAO();

    public abstract MetaFieldPropertiesDAO getMetaFieldPropertiesDAO();

    public abstract MetaFormViewDAO getMetaFormViewDAO();

    public abstract MetaFormViewPropertiesDAO getMetaFormViewPropertiesDAO();

    public abstract MetaFormActionDAO getMetaFormActionDAO();

    public abstract MetaFormTabDAO getMetaFormTabDAO();

    public abstract MetaFormTabFieldDAO getMetaFormTabFieldDAO();

    public abstract MetaViewLookupDAO getMetaViewLookupDAO();

    public abstract WikidocResolutionRatingDAO getWikidocResolutionRatingDAO();

    public abstract WikidocQualityRatingDAO getWikidocQualityRatingDAO();

    public abstract ResolveIndexLogDAO getResolveIndexLogDAO();

    public abstract MetaSourceDAO getMetaSourceDAO();

    public abstract ArchiveActionResultDAO getArchiveActionResultDAO();
    
    public abstract ArchiveActionResultLobDAO getArchiveActionResultLobDAO();

    public abstract ArchiveExecuteDependencyDAO getArchiveExecuteDependencyDAO();

    public abstract ArchiveExecuteRequestDAO getArchiveExecuteRequestDAO();

    public abstract ArchiveExecuteResultDAO getArchiveExecuteResultDAO();

    public abstract ArchiveProcessRequestDAO getArchiveProcessRequestDAO();

    public abstract ArchiveWorksheetDAO getArchiveWorksheetDAO();

    public abstract ArchiveWorksheetDebugDAO getArchiveWorksheetDebugDAO();

    public abstract custom_table_parent_exampleDAO getcustom_table_parent_exampleDAO();

    public abstract SocialPostAttachmentDAO getSocialPostAttachmentDAO();

    public abstract CatalogAttachmentDAO getCatalogAttachmentDAO();

    public abstract PostSyncDAO getPostSyncDAO();

    public abstract ResolveImpexLogDAO getResolveImpexLogDAO();
    
    public abstract ResolveImpexManifestDAO getResolveImpexManifestDAO();

    public abstract UserPreferencesDAO getSocialPreferencesDAO();

    public abstract ResolveBlueprintDAO getResolveBlueprintDAO();

    public abstract MetaxFormViewPanelDAO getMetaxFormViewPanelDAO();

    public abstract MetaxFieldDependencyDAO getMetaxFieldDependencyDAO();

    public abstract WikiDocumentMetaFormRelDAO getWikiDocumentMetaFormRelDAO();

    public abstract SSHConnectionPoolDAO getSSHConnectionPoolDAO();

    public abstract RemedyFilterDAO getRemedyFilterDAO();

    public abstract RemedyFormDAO getRemedyFormDAO();

    public abstract HPOMFilterDAO getHPOMFilterDAO();

    public abstract TelnetConnectionPoolDAO getTelnetConnectionPoolDAO();

    public abstract EWSFilterDAO getEWSFilterDAO();

    public abstract EWSAddressDAO getEWSAddressDAO();

    public abstract ResolvePermissionDAO getResolvePermissionDAO();

    public abstract ResolveActionTaskMockDataDAO getResolveActionTaskMockDataDAO();

    public abstract MCPBlueprintDAO getMCPBlueprintDAO();

    public abstract MCPServerDAO getMCPServerDAO();

    public abstract MCPComponentDAO getMCPComponentDAO();
    
    public abstract MCPFileDAO getMCPFileDAO();
    
    public abstract MCPFileContentDAO getMCPFileContentDAO();

    public abstract MetricThresholdDAO getMetricThresholdDAO();

    public abstract ConfigActiveDirectoryDAO getConfigActiveDirectoryDAO();

    public abstract OrganizationDAO getOrganizationDAO();

    public abstract ConfigLDAPDAO getConfigLDAPDAO();

    public abstract ResolveAppsDAO getResolveAppsDAO();

    public abstract ResolveControllersDAO getResolveControllersDAO();

    public abstract ResolveAppControllerRelDAO getResolveAppControllerRelDAO();

    public abstract ResolveAppRoleRelDAO getResolveAppRoleRelDAO();

    public abstract ResolveAppOrganizationRelDAO getResolveAppOrganizationRelDAO();

    public abstract HPSMFilterDAO getHPSMFilterDAO();

    public abstract NamePropertyDAO getGatewayNamePropertyDAO();
    
    public abstract ResolveAssessRelDAO getResolveAssessRelDAO();
    
    public abstract ResolveParserRelDAO getResolveParserRelDAO();

    public abstract ResolvePreprocessRelDAO getResolvePreprocessRelDAO();

    public abstract AmqpFilterDAO getAmqpFilterDAO();

    public abstract TCPFilterDAO getTCPFilterDAO();
    
    public abstract RBGeneralDAO getRBGeneralDAO();

    public abstract RBLoopDAO getRBLoopDAO();

    public abstract RBConnectionDAO getRBConnectionDAO();

    public abstract RBIfDAO getRBIfDAO();

    public abstract RBConditionDAO getRBConditionDAO();

    public abstract RBCriterionDAO getRBCriterionDAO();

    public abstract RBTaskDAO getRBTaskDAO();

    public abstract RBTaskConditionDAO getRBTaskConditionDAO();

    public abstract RBTaskSeverityDAO getRBTaskSeverityDAO();

    public abstract RBTaskVariableDAO getRBTaskVariableDAO();
    
    public abstract HTTPFilterDAO getHTTPFilterDAO();
    
    public abstract ResolveCatalogDAO getResolveCatalogDAO();

    public abstract ResolveCatalogNodeDAO getResolveCatalogNodeDAO();

    public abstract ResolveCatalogEdgeDAO getResolveCatalogEdgeDAO();

    public abstract ResolveCatalogNodeWikidocRelDAO getResolveCatalogNodeWikidocRelDAO();

    public abstract ResolveCatalogNodeTagRelDAO getResolveCatalogNodeTagRelDAO();

    public abstract ResolveNodeDAO getResolveNodeDAO();
    
    public abstract ResolveEdgeDAO getResolveEdgeDAO();
    
    public abstract ResolveNodePropertiesDAO getResolveNodePropertiesDAO();
    
    public abstract ResolveEdgePropertiesDAO getResolveEdgePropertiesDAO();
    
//    public abstract SocialNotificationsDAO getSocialNotificationsDAO();
//
//    public abstract SocialNotificationRuleDAO getSocialNotificationRuleDAO();

    public abstract SocialUserNotificationDAO getSocialUserNotificationDAO();

    public abstract TIBCOBespokeFilterDAO getTIBCOBespokeFilterDAO();
    
    public abstract CASpectrumFilterDAO getCASpectrumFilterDAO();
    
    public abstract ResolveMCPGroupDAO getResolveMCPGroupDAO();
    
    public abstract ResolveMCPClusterDAO getResolveMCPClusterDAO();
    
    public abstract ResolveMCPHostDAO getResolveMCPHostDAO();
    
    public abstract ResolveMCPComponentDAO getResolveMCPComponentDAO();
    
    public abstract ResolveMCPHostStatusDAO getResolveMCPHostStatusDAO();
    
    public abstract ResolveMCPBlueprintDAO getResolveMCPBlueprintDAO();
    
    public abstract ResolveTaskExpressionDAO getResolveTaskExpressionDAO();
    
    public abstract ResolveTaskOutputMappingDAO getResolveTaskOutputMappingDAO();
    
    public abstract ConfigRADIUSDAO getConfigRADIUSDAO();
    
    public abstract ArchiveSirActivityDAO getArchiveSirActivityDAO();
    
    public abstract ArchiveSirNoteDAO getArchiveSirNoteDAO();
    
    public abstract ArchiveSirAttachmentDAO getArchiveSirAttachmentDAO();
    
    public abstract ArchiveSirArtifactDAO getArchiveSirArtifactDAO();
    
    public abstract ArchiveSirAuditLogDAO getArchiveSirAuditLogDAO();
    
    public abstract ResolveSecurityIncidentDAO getResolveSecurityIncidentDAO();
    
    public abstract PlaybookActivitiesDAO getPlaybookActivitiesDAO();
    
    public abstract ResolveCompRelDAO getResolveCompRelDAO();
    
    public abstract OrgsDAO getOrgsDAO();
    
    public abstract OrgGroupRelDAO getOrgGroupRelDAO();
    
    public abstract CustomDictionaryItemDAO getCustomDictionaryDAO();

    public abstract CEFDictionaryItemDAO getCEFDictionaryDAO();

	public abstract ArtifactConfigurationDAO getArtifactConfigurationDAO();

    public abstract ArtifactTypeDAO getArtifactTypeDAO();
    
    public abstract RRSchemaDAO getRRSchemaDAO();
    
    public abstract RRRuleDAO getRRRuleDAO();
    
    public abstract CustomDataFormTabDAO getCustomDataFormTabDAO();
    
    public abstract SIRConfigDAO getSIRConfigDAO();
    
	public abstract ResolveCaseConfigDAO getResolveCaseConfigDAO();

	public abstract SavedSearchDAO getSavedSearchDAO();

	public abstract SIRPhaseMetaDataDAO getSIRPhaseMetaDataDAO();
    
    public abstract SIRActivityMetaDataDAO getSIRActivityMetaDataDAO();
    
    public abstract SIRPhaseRuntimeDataDAO getSIRPhaseRuntimeDataDAO();
    
    public abstract SIRActivityRuntimeDataDAO getSIRActivityRuntimeDataDAO();

    public abstract CaseAttributeDAO getCaseAttributeDAO();
    
    public abstract CustomDataFormDAO getCustomDataFormDAO();
    
    public abstract RecentItemDAO getRecentItemDAO();
    
    public abstract RecentSearchDAO getRecentSearchDAO();
    
    public abstract AggrExecSummaryDAO getAggrExecSummaryDAO();

    public abstract ResolveNamespaceDAO getResolveNamespaceDAO();
    
    public abstract GatewayHealthCheckDAO getGatewayHealthCheckDAO();
    
    public abstract ArchiveWorksheetDataDAO getArchiveWorksheetDataDAO();
}
