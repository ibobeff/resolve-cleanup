/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.RBConditionVO;
import com.resolve.services.hibernate.vo.RBCriterionVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
@Entity
@Table(name = "rb_condition")
public class RBCondition extends BaseModel<RBConditionVO>
{
    private String expression; //derived when the case criteria records are added/updated?
    private String parentId; //connectionId
    private String parentType; //currently only "if" 
    private Double order;
    
    private Collection<RBCriterion> criteria;
    
    public RBCondition()
    {
    }

    public RBCondition(RBConditionVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_expression", length = 2048)
    public String getExpression()
    {
        return expression;
    }

    public void setExpression(String expression)
    {
        this.expression = expression;
    }
    
    @Column(name = "u_parent_id", length = 32)
    public String getParentId()
    {
        return parentId;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }

    @Column(name = "u_parent_type", length = 40)
    public String getParentType()
    {
        return parentType;
    }

    public void setParentType(String parentType)
    {
        this.parentType = parentType;
    }

    @Column(name = "u_order", precision=10, scale=2)
    public Double getOrder()
    {
        return order;
    }

    public void setOrder(Double order)
    {
        this.order = order;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "parent", fetch=FetchType.EAGER)
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.ALL)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public Collection<RBCriterion> getCriteria()
    {
        return criteria;
    }

    public void setCriteria(Collection<RBCriterion> criteria)
    {
        this.criteria = criteria;
    }

    @Override
    public RBConditionVO doGetVO()
    {
        RBConditionVO vo = new RBConditionVO();
        super.doGetBaseVO(vo);

        vo.setParentId(getParentId());
        vo.setParentType(getParentType());
        vo.setOrder(getOrder());
        
        //criteria
        Collection<RBCriterion> criteria = getCriteria();
        if(criteria != null)
        {
            List<RBCriterionVO> criterionVOs = new ArrayList<RBCriterionVO>();
            for(RBCriterion param : criteria)
            {
                if (param != null)
                {
                    criterionVOs.add(param.doGetVO());
                }
            }
            
            vo.setCriteria(criterionVOs);
        }
        
        return vo;
    }

    @Override
    public void applyVOToModel(RBConditionVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setParentId(StringUtils.isNotBlank(vo.getParentId()) && vo.getParentId().equals(VO.STRING_DEFAULT) ? getParentId() : vo.getParentId());
            this.setParentType(StringUtils.isNotBlank(vo.getParentType()) && vo.getParentType().equals(VO.STRING_DEFAULT) ? getParentType() : vo.getParentType());
            this.setOrder(vo.getOrder() != null && vo.getOrder().equals(VO.DOUBLE_DEFAULT) ? getOrder() : vo.getOrder());
            
            Collection<RBCriterionVO> criterionVOs = vo.getCriteria();
            if (criterionVOs != null && criterionVOs.size() > 0)
            {
                List<RBCriterion> criteria = new ArrayList<RBCriterion>();
                for (RBCriterionVO criterionVO : criterionVOs)
                {
                    criteria.add(new RBCriterion(criterionVO));
                }
                this.setCriteria(criteria);
            }
        }
    }
}
