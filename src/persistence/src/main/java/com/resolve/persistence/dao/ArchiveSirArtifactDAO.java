/******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.dao;

import com.resolve.persistence.model.ArchiveSirArtifact;

public interface ArchiveSirArtifactDAO extends GenericDAO<ArchiveSirArtifact, String>
{

}
