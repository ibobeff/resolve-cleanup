/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.RBTaskConditionVO;
import com.resolve.services.hibernate.vo.RBTaskHeaderVO;
import com.resolve.services.hibernate.vo.RBTaskSeverityVO;
import com.resolve.services.hibernate.vo.RBTaskVO;
import com.resolve.services.hibernate.vo.RBTaskVariableVO;
import com.resolve.services.interfaces.CdataProperty;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
@Entity
@Table(name = "rb_task")
public class RBTask extends BaseModel<RBTaskVO> implements CdataProperty
{
    private String name;
    private String generalId;
    private String parentId;
    private String parentType; // CONNECTION or CONDITION or 
    private Double orderNumber;
    private String command;
    private String commandUrl;
    private String encodedCommand; //this is encoded command from the UI
    private String promptSource;
    private String promptSourceName;
    private String parserType; // TEXT, TABLE, XML, JSON etc.
    private String sampleOutput; // sample output of the command
    private Boolean repeat; // does pattern repeat?
    private String description;
    private String summary;
    private String detail;
    private String method;
    private String body;
    private String defaultAssess; // GOOD, BAD
    private String defaultSeverity; // GOOD, WARN, SEVERE, CRITICAL
    private String continuation; // ALWAYS, ONGOOD,ONBAD
    private String queueName;
    private String queueNameSource;
    private String parserConfig; //just a JSON string with bunch of markups from UI
    private String assessorScript;
    private String parserScript;
    private Boolean assessorAutoGenEnabled;
    private Boolean parserAutoGenEnabled;
    //TODO Introduce new runbook components object with reference to actual task, (sub)runbook 
    private RBGeneral refRunbook; // referenced (sub)runbook
    private String refRunbookParams; //Referenced sub-runbook parameters JSON object string will be {} as indicator for refRunbook
    private Integer timeout;
    private Integer expectTimeout;
    private String inputfile;
    
    private Collection<RBTaskCondition> conditions;
    private Collection<RBTaskSeverity> severities;
    private Collection<RBTaskVariable> variables;
    
    private Collection<RBTaskHeader> headers;
    private Collection<RBTaskPostParam> postParams;


    public RBTask()
    {
        this.assessorAutoGenEnabled = true;
        this.parserAutoGenEnabled = true;
    }

    public RBTask(RBTaskVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_name", length = 200)
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Column(name = "u_general_id", length = 32)
    public String getGeneralId()
    {
        return generalId;
    }

    public void setGeneralId(String generalId)
    {
        this.generalId = generalId;
    }

    @Column(name = "u_parent_id", length = 32)
    public String getParentId()
    {
        return parentId;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }

    @Column(name = "u_parent_type", length = 40)
    public String getParentType()
    {
        return parentType;
    }

    public void setParentType(String parentType)
    {
        this.parentType = parentType;
    }

    @Column(name = "u_order", precision=10, scale=2)
    public Double getOrderNumber()
    {
        return orderNumber;
    }

    public void setOrderNumber(Double orderNumber)
    {
        this.orderNumber = orderNumber;
    }

    @Column(name = "u_command", length = 500)
    public String getCommand()
    {
        return command;
    }

    public void setCommand(String command)
    {
        this.command = command;
    }

    @Column(name = "encoded_command", length = 2000)
    public String getEncodedCommand()
    {
        return encodedCommand;
    }

    public void setEncodedCommand(String encodedCommand)
    {
        this.encodedCommand = encodedCommand;
    }

    @Column(name = "u_prompt_source", length = 40)
    public String getPromptSource()
    {
        return promptSource;
    }

    public void setPromptSource(String promptSource)
    {
        this.promptSource = promptSource;
    }

    @Column(name = "u_prompt_source_name", length = 256)
    public String getPromptSourceName()
    {
        return promptSourceName;
    }

    public void setPromptSourceName(String promptSourceName)
    {
        this.promptSourceName = promptSourceName;
    }

    @Column(name = "u_parser_type", length = 30)
    public String getParserType()
    {
        return parserType;
    }

    public void setParserType(String parserType)
    {
        this.parserType = parserType;
    }

    @Lob
    @Column(name = "u_sample", length = 16777215)
    public String getSampleOutput()
    {
        return sampleOutput;
    }

    public void setSampleOutput(String sampleOutput)
    {
        this.sampleOutput = sampleOutput;
    }

    @Column(name = "u_repeat")
    public Boolean getRepeat()
    {
        return repeat;
    }

    public void setRepeat(Boolean repeat)
    {
        this.repeat = repeat;
    }

    @Lob
    @Column(name = "u_description", length = 16777215)
    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @Lob
    @Column(name = "u_summary", length = 16777215)
    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    @Lob
    @Column(name = "u_detail", length = 16777215)
    public String getDetail()
    {
        return detail;
    }

    public void setDetail(String detail)
    {
        this.detail = detail;
    }
    
    @Lob
    @Column(name = "u_body", length = 16777215)
    public String getBody()
    {
        return body;
    }

    public void setBody(String body)
    {
        this.body = body;
    }

    @Column(name = "u_default_assess", length = 40)
    public String getDefaultAssess()
    {
        return defaultAssess;
    }

    public void setDefaultAssess(String defaultAssess)
    {
        this.defaultAssess = defaultAssess;
    }

    @Column(name = "u_default_severity", length = 40)
    public String getDefaultSeverity()
    {
        return defaultSeverity;
    }

    public void setDefaultSeverity(String defaultSeverity)
    {
        this.defaultSeverity = defaultSeverity;
    }

    @Column(name = "u_continuation", length = 40)
    public String getContinuation()
    {
        return continuation;
    }

    public void setContinuation(String continuation)
    {
        this.continuation = continuation;
    }

    @Column(name = "u_queue_name", length = 40)
    public String getQueueName()
    {
        return queueName;
    }

    public void setQueueName(String queueName)
    {
        this.queueName = queueName;
    }

    @Column(name = "u_queue_name_src", length = 40)
    public String getQueueNameSource()
    {
        return queueNameSource;
    }

    public void setQueueNameSource(String queueNameSource)
    {
        this.queueNameSource = queueNameSource;
    }

    @Lob
    @Column(name = "u_parser_config", length = 16777215)
    public String getParserConfig()
    {
        return parserConfig;
    }

    public void setParserConfig(String parserConfig)
    {
        this.parserConfig = parserConfig;
    }

    @Lob
    @Column(name = "u_assessor_script", length = 16777215)
    public String getAssessorScript()
    {
        return this.assessorScript;
    }

    public void setAssessorScript(String assessorScript)
    {
        this.assessorScript = assessorScript;
    }
    
    @Lob
    @Column(name = "u_parser_script", length = 16777215)
    public String getParserScript()
    {
        return parserScript;
    }

    public void setParserScript(String parserScript)
    {
        this.parserScript = parserScript;
    }
    
    @Column(name="assessor_auto_gen")
    public Boolean getAssessorAutoGenEnabled()
    {
        return assessorAutoGenEnabled;
    }

    public void setAssessorAutoGenEnabled(Boolean assessorAutoGenEnabled)
    {
        this.assessorAutoGenEnabled = assessorAutoGenEnabled;
    }
    
    @Column(name="parser_auto_gen")
    public Boolean getParserAutoGenEnabled()
    {
        return parserAutoGenEnabled;
    }

    public void setParserAutoGenEnabled(Boolean parserAutoGenEnabled)
    {
        this.parserAutoGenEnabled = parserAutoGenEnabled;
    }

    @Column(name = "u_timeout")
    public Integer getTimeout()
    {
        return timeout;
    }

    public void setTimeout(Integer timeout)
    {
        this.timeout = timeout;
    }
    @Column(name = "u_expect_timeout")
    public Integer getExpectTimeout()
    {
        return expectTimeout;
    }

    public void setExpectTimeout(Integer expectTimeout)
    {
        this.expectTimeout = expectTimeout;
    }
    @Lob
    @Column(name = "u_inputfile",length = 16777215)
    public String getInputfile()
    {
        return inputfile;
    }

    public void setInputfile(String inputfile)
    {
        this.inputfile = inputfile;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_ref_runbook_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public RBGeneral getRefRunbook()
    {
        return this.refRunbook;
    }

    public void setRefRunbook(RBGeneral refRunbook)
    {
        this.refRunbook = refRunbook;
    }
    
    @Lob
    @Column(name = "u_ref_runbook_params", length = 16777215)
    public String getRefRunbookParams()
    {
        return refRunbookParams;
    }

    public void setRefRunbookParams(String refRunbookParams)
    {
        this.refRunbookParams = refRunbookParams;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "task", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.ALL)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @OrderBy("order")
    public Collection<RBTaskCondition> getConditions()
    {
        return conditions;
    }

    public void setConditions(Collection<RBTaskCondition> conditions)
    {
        this.conditions = conditions;
    }

    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "task", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.ALL)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @OrderBy("order")
    public Collection<RBTaskSeverity> getSeverities()
    {
        return severities;
    }

    public void setSeverities(Collection<RBTaskSeverity> severities)
    {
        this.severities = severities;
    }

    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "task", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.ALL)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public Collection<RBTaskVariable> getVariables()
    {
        return variables;
    }

    public void setVariables(Collection<RBTaskVariable> variables)
    {
        this.variables = variables;
    }
    
    @Column(name = "u_commandurl")
    public String getCommandUrl()
    {
        return commandUrl;
    }

    public void setCommandUrl(String commandUrl)
    {
        this.commandUrl = commandUrl;
    }

    @Column(name = "u_method")
    public String getMethod()
    {
        return method;
    }

    public void setMethod(String method)
    {
        this.method = method;
    }

    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "task", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.ALL)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public Collection<RBTaskHeader> getHeaders()
    {
        return headers;
    }

    public void setHeaders(Collection<RBTaskHeader> headers)
    {
        this.headers = headers;
    }

    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "task", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.ALL)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public Collection<RBTaskPostParam> getPostParams()
    {
        return postParams;
    }

    public void setPostParams(Collection<RBTaskPostParam> postParams)
    {
        this.postParams = postParams;
    }

    @Override
    public RBTaskVO doGetVO()
    {
        RBTaskVO vo = new RBTaskVO();
        super.doGetBaseVO(vo);

        vo.setName(getName());
        vo.setGeneralId(getGeneralId());
        vo.setParentId(getParentId());
        vo.setParentType(getParentType());
        vo.setOrder(getOrderNumber());
        vo.setCommand(getCommand());
        vo.setCommandUrl(getCommandUrl());
        vo.setEncodedCommand(getEncodedCommand());
        vo.setPromptSource(getPromptSource());
        vo.setPromptSourceName(getPromptSourceName());
        vo.setParserType(getParserType());
        vo.setSampleOutput(getSampleOutput());
        vo.setRepeat(getRepeat());
        vo.setDescription(getDescription());
        vo.setSummary(getSummary());
        vo.setDetail(getDetail());
        vo.setMethod(getMethod());
        vo.setBody(getBody());
        vo.setDefaultAssess(getDefaultAssess());
        vo.setDefaultSeverity(getDefaultSeverity());
        vo.setContinuation(getContinuation());
        vo.setQueueName(getQueueName());
        vo.setQueueNameSource(getQueueNameSource());
        vo.setParserConfig(getParserConfig());
        vo.setAssessorScript(getAssessorScript());
        vo.setParserScript(getParserScript());
        vo.setAssessorAutoGenEnabled(getAssessorAutoGenEnabled());
        vo.setParserAutoGenEnabled(getParserAutoGenEnabled());
        vo.setRefRunbook(getRefRunbook() != null ? getRefRunbook().doGetVO() : null);
        vo.setRefRunbookParams(getRefRunbookParams()); // 
        
        vo.setTimeout(getTimeout()!=null?getTimeout():300);
        vo.setExpectTimeout(getExpectTimeout()!=null?getExpectTimeout():10);
        vo.setInputfile(getInputfile());
        //RefRunbookParams are set to empty JSON object {} for Runbook reference
        if(vo.getRefRunbookParams() != null && !vo.getRefRunbookParams().equalsIgnoreCase(VO.STRING_DEFAULT))
        {
            vo.setEntityType(RBTaskVO.SUBRUNBOOK_ENTITY_TYPE);
        }
        //assess
        Collection<RBTaskCondition> conditions = getConditions();
        if(conditions != null)
        {
            List<RBTaskConditionVO> conditionVOs = new ArrayList<RBTaskConditionVO>();
            for(RBTaskCondition assess : conditions)
            {
                if (assess != null)
                {
                    conditionVOs.add(assess.doGetVO());
                }
            }
            
            vo.setConditions(conditionVOs);
        }

        //severity
        Collection<RBTaskSeverity> severities = getSeverities();
        if(severities != null)
        {
            List<RBTaskSeverityVO> severityVOs = new ArrayList<RBTaskSeverityVO>();
            for(RBTaskSeverity severity : severities)
            {
                if (severity != null)
                {
                    severityVOs.add(severity.doGetVO());
                }
            }
            
            vo.setSeverities(severityVOs);
        }

        //variables
        Collection<RBTaskVariable> variables = getVariables();
        if(variables != null)
        {
            List<RBTaskVariableVO> variableVOs = new ArrayList<RBTaskVariableVO>();
            for(RBTaskVariable variable : variables)
            {
                if (variable != null)
                {
                    variableVOs.add(variable.doGetVO());
                }
            }
            
            vo.setVariables(variableVOs);
        }
        
        // headers
        Collection<RBTaskHeader> headers = getHeaders();
        if(headers != null)
        {
            List<RBTaskHeaderVO> headerVOs = new ArrayList<RBTaskHeaderVO>();
            for(RBTaskHeader header : headers)
            {
                if (header != null)
                {
                    headerVOs.add(header.doGetVO());
                }
            }
            
            vo.setHeaders(headerVOs);
        }

        return vo;
    }

    @Override
    public void applyVOToModel(RBTaskVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setName(StringUtils.isNotBlank(vo.getName()) && vo.getName().equals(VO.STRING_DEFAULT) ? getName() : vo.getName());
            this.setGeneralId(StringUtils.isNotBlank(vo.getGeneralId()) && vo.getGeneralId().equals(VO.STRING_DEFAULT) ? getGeneralId() : vo.getGeneralId());
            this.setParentId(StringUtils.isNotBlank(vo.getParentId()) && vo.getParentId().equals(VO.STRING_DEFAULT) ? getParentId() : vo.getParentId());
            this.setParentType(StringUtils.isNotBlank(vo.getParentType()) && vo.getParentType().equals(VO.STRING_DEFAULT) ? getParentType() : vo.getParentType());
            this.setOrderNumber(vo.getOrder() != null && vo.getOrder().equals(VO.DOUBLE_DEFAULT) ? getOrderNumber() : vo.getOrder());
            this.setCommand(StringUtils.isNotBlank(vo.getCommand()) && vo.getCommand().equals(VO.STRING_DEFAULT) ? getCommand() : vo.getCommand());
            this.setCommandUrl(StringUtils.isNotBlank(vo.getCommandUrl()) && vo.getCommandUrl().equals(VO.STRING_DEFAULT) ? getCommandUrl() : vo.getCommandUrl());
            this.setEncodedCommand(StringUtils.isNotBlank(vo.getEncodedCommand()) && vo.getCommand().equals(VO.STRING_DEFAULT) ? getEncodedCommand() : vo.getEncodedCommand());
            this.setPromptSource(StringUtils.isNotBlank(vo.getPromptSource()) && vo.getPromptSource().equals(VO.STRING_DEFAULT) ? getPromptSource() : vo.getPromptSource());
            this.setPromptSourceName(StringUtils.isNotBlank(vo.getPromptSourceName()) && vo.getPromptSourceName().equals(VO.STRING_DEFAULT) ? getPromptSourceName() : vo.getPromptSourceName());
            this.setParserType(StringUtils.isNotBlank(vo.getParserType()) && vo.getParserType().equals(VO.STRING_DEFAULT) ? getParserType() : vo.getParserType());
            this.setSampleOutput(StringUtils.isNotBlank(vo.getSampleOutput()) && vo.getSampleOutput().equals(VO.STRING_DEFAULT) ? getSampleOutput() : vo.getSampleOutput());
            this.setRepeat(vo.getRepeat() != null ? vo.getRepeat() : getRepeat());
            this.setDescription(StringUtils.isNotBlank(vo.getDescription()) && vo.getDescription().equals(VO.STRING_DEFAULT) ? getDescription() : vo.getDescription());
            this.setSummary(StringUtils.isNotBlank(vo.getSummary()) && vo.getSummary().equals(VO.STRING_DEFAULT) ? getSummary() : vo.getSummary());
            this.setDetail(StringUtils.isNotBlank(vo.getDetail()) && vo.getDetail().equals(VO.STRING_DEFAULT) ? getDetail() : vo.getDetail());
            this.setMethod(StringUtils.isNotBlank(vo.getMethod()) && vo.getMethod().equals(VO.STRING_DEFAULT) ? getMethod() : vo.getMethod());
            this.setBody(StringUtils.isNotBlank(vo.getBody()) && vo.getBody().equals(VO.STRING_DEFAULT) ? getBody() : vo.getBody());
            this.setDefaultAssess(StringUtils.isNotBlank(vo.getDefaultAssess()) && vo.getDefaultAssess().equals(VO.STRING_DEFAULT) ? getDefaultAssess() : vo.getDefaultAssess());
            this.setDefaultSeverity(StringUtils.isNotBlank(vo.getDefaultSeverity()) && vo.getDefaultSeverity().equals(VO.STRING_DEFAULT) ? getDefaultSeverity() : vo.getDefaultSeverity());
            this.setContinuation(StringUtils.isNotBlank(vo.getContinuation()) && vo.getContinuation().equals(VO.STRING_DEFAULT) ? getContinuation() : vo.getContinuation());
            this.setQueueName(StringUtils.isNotBlank(vo.getQueueName()) && vo.getQueueName().equals(VO.STRING_DEFAULT) ? getQueueName() : vo.getQueueName());
            this.setQueueNameSource(StringUtils.isNotBlank(vo.getQueueNameSource()) && vo.getQueueNameSource().equals(VO.STRING_DEFAULT) ? getQueueNameSource() : vo.getQueueNameSource());
            this.setParserConfig(StringUtils.isNotBlank(vo.getParserConfig()) && vo.getParserConfig().equals(VO.STRING_DEFAULT) ? getParserConfig() : vo.getParserConfig());
            this.setAssessorScript(StringUtils.isNotBlank(vo.getAssessorScript()) && vo.getAssessorScript().equals(VO.STRING_DEFAULT) ? getAssessorScript() : vo.getAssessorScript());
            this.setParserScript(StringUtils.isNotBlank(vo.getParserScript()) && vo.getParserScript().equals(VO.STRING_DEFAULT) ? getParserScript() : vo.getParserScript());
            this.setAssessorAutoGenEnabled(vo.getAssessorAutoGenEnabled() != null ? vo.getAssessorAutoGenEnabled() : getAssessorAutoGenEnabled());
            this.setParserAutoGenEnabled(vo.getParserAutoGenEnabled() != null ? vo.getParserAutoGenEnabled() : getParserAutoGenEnabled());
            this.setRefRunbookParams(vo.getRefRunbookParams() != null ? vo.getRefRunbookParams() : getRefRunbookParams());
            this.setTimeout(vo.getTimeout()!=null?vo.getTimeout():300);
            this.setExpectTimeout(vo.getExpectTimeout()!=null?vo.getExpectTimeout():10);
            this.setInputfile(StringUtils.isNotBlank(vo.getInputfile())&&vo.getInputfile().equals(VO.STRING_DEFAULT)?getInputfile():vo.getInputfile());
            
            //condition
            Collection<RBTaskConditionVO> conditionVOs = vo.getConditions();
            if (conditionVOs != null && conditionVOs.size() > 0)
            {
                List<RBTaskCondition> conditions = new ArrayList<RBTaskCondition>();
                for (RBTaskConditionVO conditionVO : conditionVOs)
                {
                    conditions.add(new RBTaskCondition(conditionVO));
                }
                this.setConditions(conditions);
            }

            //severity
            Collection<RBTaskSeverityVO> severityVOs = vo.getSeverities();
            if (severityVOs != null && severityVOs.size() > 0)
            {
                List<RBTaskSeverity> severities = new ArrayList<RBTaskSeverity>();
                for (RBTaskSeverityVO severityVO : severityVOs)
                {
                    severities.add(new RBTaskSeverity(severityVO));
                }
                this.setSeverities(severities);
            }

            //variable
            Collection<RBTaskVariableVO> variableVOs = vo.getVariables();
            if (variableVOs != null && variableVOs.size() > 0)
            {
                List<RBTaskVariable> variables = new ArrayList<RBTaskVariable>();
                for (RBTaskVariableVO variableVO : variableVOs)
                {
                    variables.add(new RBTaskVariable(variableVO));
                }
                this.setVariables(variables);
            }
            
            // header
            Collection<RBTaskHeaderVO> headerVOs = vo.getHeaders();
            if (headerVOs != null && headerVOs.size() > 0)
            {
                List<RBTaskHeader> headers = new ArrayList<RBTaskHeader>();
                for (RBTaskHeaderVO headerVO : headerVOs)
                {
                    headers.add(new RBTaskHeader(headerVO));
                }
                this.setHeaders(headers);
            }
        }
    }

    @Override
    public List<String> ugetCdataProperty()
    {
        List<String> list = new ArrayList<String>();
        list.add("sampleOutput");
        list.add("assessorScript");
        list.add("parserScript");
        
        return list;
    }
}
