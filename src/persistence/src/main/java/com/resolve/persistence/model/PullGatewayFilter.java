package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.services.hibernate.vo.PullGatewayFilterVO;
import com.resolve.services.interfaces.VO;

@Entity
@Table(name = "pull_gateway_filter", 
	   uniqueConstraints = {
			   					@UniqueConstraint(columnNames = { "u_name", "u_queue" }, name = "pullgf_u_name_u_queue_uk")
			   			   },
	   indexes = {
			   	     @Index(columnList = "u_gateway_name", name = "pullgf_gtw_queue_idx"),
			   	     @Index(columnList = "u_queue", name = "pullgf_gtw_queue_idx"),
			   	     @Index(columnList = "u_name", name = "pullgf_filter_gtw_queue_idx"),
			   	     @Index(columnList = "u_gateway_name", name = "pullgf_filter_gtw_queue_idx"),
			   	     @Index(columnList = "u_queue", name = "pullgf_filter_gtw_queue_idx")
			   	 })
public class PullGatewayFilter extends GatewayFilter<PullGatewayFilterVO> {

    private static final long serialVersionUID = 1L;

    public PullGatewayFilter() {
    }

    public PullGatewayFilter(PullGatewayFilterVO vo) {
        applyVOToModel(vo);
    }

    private String UGatewayName;
    
    private Boolean UDeployed;
    
    private Boolean UUpdated;

    private String UQuery;

    private String UTimeRange;

    private String ULastTimestamp;

    private String ULastId;
    
    private String ULastValue;
    
    @Column(name = "u_gateway_name", length = 40)
    public String getUGatewayName()
    {
        return UGatewayName;
    }

    public void setUGatewayName(String uGatewayName)
    {
        UGatewayName = uGatewayName;
    }
    
    @Column(name = "u_deployed")
    public Boolean getUDeployed()
    {
        return UDeployed;
    }

    public void setUDeployed(Boolean uDeployed)
    {
        UDeployed = uDeployed;
    }
    
    @Column(name = "u_updated")
    public Boolean getUUpdated()
    {
        return UUpdated;
    }

    public void setUUpdated(Boolean uUpdated)
    {
        UUpdated = uUpdated;
    }

    @Column(name = "u_query", length = 4000)
    public String getUQuery()
    {
        return UQuery;
    }

    public void setUQuery(String uQuery)
    {
        UQuery = uQuery;
    }

    @Column(name = "u_time_range", length = 40)
    public String getUTimeRange()
    {
        return UTimeRange;
    }

    public void setUTimeRange(String uTimeRange)
    {
        UTimeRange = uTimeRange;
    }

    @Column(name = "u_last_timestamp", length = 40)
    public String getULastTimestamp()
    {
        return ULastTimestamp;
    }

    public void setULastTimestamp(String uLastTimestamp)
    {
        ULastTimestamp = uLastTimestamp;
    }

    @Column(name = "u_last_id", length = 40)
    public String getULastId()
    {
        return ULastId;
    }

    public void setULastId(String uLastId)
    {
        ULastId = uLastId;
    }

    @Column(name = "u_last_value", length = 40)
    public String getULastValue()
    {
        return ULastValue;
    }

    public void setULastValue(String ULastValue)
    {
        this.ULastValue = ULastValue;
    }

    public PullGatewayFilterVO doGetVO() {
        
        PullGatewayFilterVO vo = new PullGatewayFilterVO();
        
        super.doGetBaseVO(vo);

        vo.setUQuery(getUQuery());
        vo.setUTimeRange(getUTimeRange());
        vo.setULastTimestamp(getULastTimestamp());
        vo.setULastId(getULastId());
        vo.setULastValue(getULastValue());
        vo.setUGatewayName(getUGatewayName());
        return vo;
    }

    @Override
    public void applyVOToModel(PullGatewayFilterVO vo) {
        
        if (vo == null) return;
        
        super.applyVOToModel(vo);

        if (!VO.STRING_DEFAULT.equals(vo.getUGatewayName())) this.setUGatewayName(vo.getUGatewayName()); else ;
        if (!VO.BOOLEAN_DEFAULT.equals(vo.getUDeployed())) this.setUDeployed(vo.getUDeployed()); else ;
        if (!VO.BOOLEAN_DEFAULT.equals(vo.getUUpdated())) this.setUUpdated(vo.getUUpdated()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUQuery())) this.setUQuery(vo.getUQuery()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUTimeRange())) this.setUTimeRange(vo.getUTimeRange()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getULastTimestamp())) this.setULastTimestamp(vo.getULastTimestamp()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getULastId())) this.setULastId(vo.getULastId()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getULastValue())) this.setULastId(vo.getULastValue()); else ;
    }
    
} // class PullGatewayFilter