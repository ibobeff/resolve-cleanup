/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.persistence.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;

import com.resolve.services.hibernate.vo.ResolveNodePropertiesVO;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "resolve_node", indexes = {
                @Index(columnList = "u_comp_sys_id", name = "rn_u_comp_sys_id_idx"),
                @Index(columnList = "u_comp_name", name = "rn_u_comp_name_idx"),
                @Index(columnList = "u_type", name = "rn_u_type_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveNode  extends BaseModel<ResolveNodeVO> 
{
    private static final long serialVersionUID = 1192585585464556151L;
    
    private String UCompSysId;
    private String UCompName;
    private String UType;
    private Boolean ULock;
    private Boolean UPinned;
    private Boolean UMarkDeleted;
    private String UReadRoles;
    private String UEditRoles;
    private String UPostRoles;


    private Set<ResolveEdge> edges;
    private Set<ResolveNodeProperties> properties;

    public ResolveNode()
    {
    }

    public ResolveNode(String sysId)
    {
        super.setSys_id(sysId);
    }
    
    public ResolveNode(ResolveNodeVO vo)
    {
        applyVOToModel(vo);
    }
    
    
    @Column(name = "u_comp_sys_id", length = 40)
    public String getUCompSysId()
    {
        return UCompSysId;
    }

    public void setUCompSysId(String uCompSysId)
    {
        UCompSysId = uCompSysId;
    }

    @Column(name = "u_comp_name", length = 300)
    public String getUCompName()
    {
        return UCompName;
    }

    public void setUCompName(String uName)
    {
        UCompName = uName;
    }

    @Column(name = "u_type", length = 100)
    public String getUType()
    {
        return UType;
    }

    public void setUType(String uType)
    {
        UType = uType;
    }
    
    @Column(name = "u_is_locked", length = 1)
    @Type(type = "yes_no")
    public Boolean getULock()
    {
        return ULock;
    }

    public void setULock(Boolean uLock)
    {
        ULock = uLock;
    }
    
    public Boolean ugetULock()
    {
        if (this.ULock == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.ULock;
        }
    } 

    @Column(name = "u_is_pinned", length = 1)
    @Type(type = "yes_no")
    public Boolean getUPinned()
    {
        return UPinned;
    }

    public void setUPinned(Boolean uPinned)
    {
        UPinned = uPinned;
    }
    
    public Boolean ugetUPinned()
    {
        if (this.UPinned == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UPinned;
        }
    }
    
    @Column(name = "u_is_mark_deleted", length = 1)
    @Type(type = "yes_no")
    public Boolean getUMarkDeleted()
    {
        return UMarkDeleted;
    }
    
    public Boolean ugetUMarkDeleted()
    {
        if (this.UMarkDeleted == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UMarkDeleted;
        }
        
    }

    public void setUMarkDeleted(Boolean uMarkDeleted)
    {
        UMarkDeleted = uMarkDeleted;
    }
    
    
    @Column(name = "u_read_roles", length = 2000)
    public String getUReadRoles()
    {
        return UReadRoles;
    }

    public void setUReadRoles(String uReadRoles)
    {
        UReadRoles = uReadRoles;
    }

    @Column(name = "u_edit_roles", length = 2000)
    public String getUEditRoles()
    {
        return UEditRoles;
    }

    public void setUEditRoles(String uEditRoles)
    {
        UEditRoles = uEditRoles;
    }

    @Column(name = "u_post_roles", length = 2000)
    public String getUPostRoles()
    {
        return UPostRoles;
    }

    public void setUPostRoles(String uPostRoles)
    {
        UPostRoles = uPostRoles;
    }

    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "sourceNode", fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.DELETE)
    public Set<ResolveEdge> getEdges()
    {
        return edges;
    }

    public void setEdges(Set<ResolveEdge> edges)
    {
        this.edges = edges;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "node", fetch = FetchType.EAGER)
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.DELETE)
    public Set<ResolveNodeProperties> getProperties()
    {
        return properties;
    }

    public void setProperties(Set<ResolveNodeProperties> properties)
    {
        this.properties = properties;
    }

    @Override
    public ResolveNodeVO doGetVO()
    {
        ResolveNodeVO vo = new ResolveNodeVO();
        super.doGetBaseVO(vo);
        
        try {
        vo.setUCompSysId(getUCompSysId());
        vo.setUCompName(getUCompName());
        vo.setUType(NodeType.valueOf(getUType()));
        vo.setULock(ugetULock());
        vo.setUPinned(ugetUPinned());
        vo.setUMarkDeleted(ugetUMarkDeleted());
        vo.setUReadRoles(getUReadRoles());
        vo.setUEditRoles(getUEditRoles());
        vo.setUPostRoles(getUPostRoles());
        
        if(CollectionUtils.isNotEmpty(properties))
        {
            properties.clear();
        }
        
        //get the properties
        Set<ResolveNodeProperties> properties = this.properties != null ? 
        										(Hibernate.isInitialized(this.properties) ? getProperties() : null) : null;
        
        if(CollectionUtils.isNotEmpty(properties))
        {
            Set<ResolveNodePropertiesVO> vos = new HashSet<ResolveNodePropertiesVO>();           
            properties.parallelStream().forEach(model -> { vos.add(model.doGetVO()); });
            vo.setProperties(vos);
        }
        } catch (Throwable t) {
        	Log.log.error(String.format("Error %sin converting properties to Object for Resolve Session with id %s", 
        								(StringUtils.isNotEmpty(t.getMessage()) ? "[" + t.getMessage() + "] " : ""), this.getSys_id()), 
        				  t);
        }
               
        return vo;
    }
        
    @Override
    public void applyVOToModel(ResolveNodeVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);  
        
            this.setUCompSysId(StringUtils.isNotBlank(vo.getUCompSysId()) && vo.getUCompSysId().equals(VO.STRING_DEFAULT) ? getUCompSysId() : vo.getUCompSysId());
            this.setUCompName(StringUtils.isNotBlank(vo.getUCompName()) && vo.getUCompName().equals(VO.STRING_DEFAULT) ? getUCompName() : vo.getUCompName());
            this.setUType(StringUtils.isNotBlank(vo.getUType().name()) && vo.getUType().name().equals(VO.STRING_DEFAULT) ? getUType() : vo.getUType().name());
            this.setULock(vo.getULock() != null ? vo.getULock() : getULock());
            this.setUPinned(vo.getUPinned() != null ? vo.getUPinned() : getUPinned());
            this.setUMarkDeleted(vo.getUMarkDeleted() != null ? vo.getUMarkDeleted() : getUMarkDeleted());

            this.setUReadRoles(StringUtils.isNotBlank(vo.getUReadRoles()) && vo.getUReadRoles().equals(VO.STRING_DEFAULT) ? getUReadRoles() : vo.getUReadRoles());
            this.setUEditRoles(StringUtils.isNotBlank(vo.getUEditRoles()) && vo.getUEditRoles().equals(VO.STRING_DEFAULT) ? getUEditRoles() : vo.getUEditRoles());
            this.setUPostRoles(StringUtils.isNotBlank(vo.getUPostRoles()) && vo.getUPostRoles().equals(VO.STRING_DEFAULT) ? getUPostRoles() : vo.getUPostRoles());

            // I don't think we should do this 
//            Collection<ResolveEdgeVO> catalogEdgesVOs = vo.getEdges();
//            if(catalogEdgesVOs != null && catalogEdgesVOs.size() > 0)
//            {
//                Collection<ResolveEdge> models = new ArrayList<ResolveEdge>();
//                for (ResolveEdgeVO refVO : catalogEdgesVOs)
//                {
//                    models.add(new ResolveEdge(refVO));
//                }
//                this.setEdges(models);
//            }
            
            Collection<ResolveNodePropertiesVO> propertiesVOs = vo.getProperties();
            if(propertiesVOs != null && propertiesVOs.size() > 0)
            {
                Set<ResolveNodeProperties> models = new HashSet<ResolveNodeProperties>();
                for (ResolveNodePropertiesVO refVO : propertiesVOs)
                {
                    models.add(new ResolveNodeProperties(refVO));
                }
                this.setProperties(models);
            }
            
            
        }
    }

    @Override
    public String toString()
    {
        return this.getSys_id() + "-" + this.getUCompSysId() + "-" + this.getUCompName() + "-" + this.getUType();
    }
}
