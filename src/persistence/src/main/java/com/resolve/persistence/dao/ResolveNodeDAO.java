package com.resolve.persistence.dao;

import com.resolve.persistence.model.ResolveNode;

public interface ResolveNodeDAO extends GenericDAO<ResolveNode, String>
{

}
