/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import org.hibernate.cfg.ImprovedNamingStrategy;

public class OracleNamingStrategy extends ImprovedNamingStrategy
{
    private static final long serialVersionUID = 4184342849338691933L;

    public String columnName(String columnName)
	{
		return "\"" + columnName + "\"";
	}
}
