package com.resolve.persistence.dao;

import com.resolve.persistence.model.ResolveNodeProperties;

public interface ResolveNodePropertiesDAO extends GenericDAO<ResolveNodeProperties, String>
{

}
