package com.resolve.persistence.dao;

import com.resolve.persistence.model.CustomDictionaryItem;

public interface CustomDictionaryItemDAO extends GenericDAO<CustomDictionaryItem, String>{}
