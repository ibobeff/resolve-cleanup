/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.MetaxFieldDependencyVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "metax_field_dependency", indexes = {
                @Index(columnList = "u_meta_field_prop_sys_id", name = "mxfd_u_meta_field_prop_idx"),
                @Index(columnList = "u_meta_control_item_sys_id", name = "mxfd_u_meta_control_item_idx"),
                @Index(columnList = "u_meta_form_tab_sys_id", name = "mxfd_u_meta_form_tab_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaxFieldDependency  extends BaseModel<MetaxFieldDependencyVO>
{
    private static final long serialVersionUID = 3652425415319288787L;
    
    private String UTarget;
    private String UAction;
    private String UCondition;
    private String UValue;
    private String UActionOptions;

    // object referenced by
    private MetaFieldProperties metaFieldProperties;
    private MetaControlItem metaControlItem;
    private MetaFormTab metaFormTab;

    public MetaxFieldDependency()
    {
    }

    public MetaxFieldDependency(MetaxFieldDependencyVO vo)
    {
        applyVOToModel(vo);
    }

    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_meta_field_prop_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaFieldProperties getMetaFieldProperties()
    {
        return metaFieldProperties;
    }

    public void setMetaFieldProperties(MetaFieldProperties metaFieldProperties)
    {
        this.metaFieldProperties = metaFieldProperties;

        if (metaFieldProperties != null)
        {
            Collection<MetaxFieldDependency> coll = metaFieldProperties.getMetaxFieldDependencys();
            if (coll == null)
            {
                coll = new HashSet<MetaxFieldDependency>();
                coll.add(this);

                metaFieldProperties.setMetaxFieldDependencys(coll);
            }
        }
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_meta_control_item_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaControlItem getMetaControlItem()
    {
        return metaControlItem;
    }

    public void setMetaControlItem(MetaControlItem metaControlItem)
    {
        this.metaControlItem = metaControlItem;

        if (metaControlItem != null)
        {
            Collection<MetaxFieldDependency> coll = metaControlItem.getMetaxFieldDependencys();
            if (coll == null)
            {
                coll = new HashSet<MetaxFieldDependency>();
                coll.add(this);

                metaControlItem.setMetaxFieldDependencys(coll);
            }
        }
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_meta_form_tab_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaFormTab getMetaFormTab()
    {
        return metaFormTab;
    }

    public void setMetaFormTab(MetaFormTab metaFormTab)
    {
        this.metaFormTab = metaFormTab;

        if (metaFormTab != null)
        {
            Collection<MetaxFieldDependency> coll = metaFormTab.getMetaxFieldDependencys();
            if (coll == null)
            {
                coll = new HashSet<MetaxFieldDependency>();
                coll.add(this);

                metaFormTab.setMetaxFieldDependencys(coll);
            }
        }
    }

    @Column(name = "u_target", length = 100)
    public String getUTarget()
    {
        return UTarget;
    }

    public void setUTarget(String uTarget)
    {
        UTarget = uTarget;
    }

    @Column(name = "u_action", length = 100)
    public String getUAction()
    {
        return UAction;
    }

    public void setUAction(String uAction)
    {
        UAction = uAction;
    }

    @Column(name = "u_condition", length = 500)
    public String getUCondition()
    {
        return UCondition;
    }

    public void setUCondition(String uCondition)
    {
        UCondition = uCondition;
    }

    @Column(name = "u_value", length = 500)
    public String getUValue()
    {
        return UValue;
    }

    public void setUValue(String uValue)
    {
        UValue = uValue;
    }

    @Column(name = "u_action_options", length = 1024)
    public String getUActionOptions()
    {
        return UActionOptions;
    }

    public void setUActionOptions(String uActionOptions)
    {
        UActionOptions = uActionOptions;
    }

    @Override
    public MetaxFieldDependencyVO doGetVO()
    {
        MetaxFieldDependencyVO vo = new MetaxFieldDependencyVO();
        super.doGetBaseVO(vo);
        
        vo.setUTarget(getUTarget());
        vo.setUAction(getUAction());
        vo.setUActionOptions(getUActionOptions());
        vo.setUCondition(getUCondition());
        vo.setUValue(getUValue());
        
        //no refs to the parent
//        private MetaFieldProperties metaFieldProperties;
//        private MetaControlItem metaControlItem;
//        private MetaFormTab metaFormTab;
        
        
        return vo;
    }

    @Override
    public void applyVOToModel(MetaxFieldDependencyVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);   
            
            this.setUTarget(StringUtils.isNotBlank(getUTarget()) && vo.getUTarget().equals(VO.STRING_DEFAULT) ? getUTarget() : vo.getUTarget());
            this.setUAction(StringUtils.isNotBlank(getUAction()) && vo.getUAction().equals(VO.STRING_DEFAULT) ? getUAction() : vo.getUAction());
            this.setUActionOptions(StringUtils.isNotBlank(getUActionOptions()) && vo.getUActionOptions().equals(VO.STRING_DEFAULT) ? getUActionOptions() : vo.getUActionOptions());
            this.setUCondition(StringUtils.isNotBlank(getUCondition()) && vo.getUCondition().equals(VO.STRING_DEFAULT) ? getUCondition() : vo.getUCondition());
            this.setUValue(StringUtils.isNotBlank(getUValue()) && vo.getUValue().equals(VO.STRING_DEFAULT) ? getUValue() : vo.getUValue());

            
            //refs
            //no refs to the parent            

        }
        
    }

}
