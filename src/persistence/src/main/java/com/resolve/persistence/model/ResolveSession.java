/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

// Generated Dec 11, 2009 11:15:20 AM by Hibernate Tools 3.2.4.GA

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;

import com.resolve.services.hibernate.vo.ResolveSessionVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

/**
 * ResolveSession generated by hbm2java
 */
@Entity
@Table(name = "resolve_session", indexes = {
                @Index(columnList = "u_active_type", name = "u_active_type_idx"),
                @Index(columnList = "u_problem_sys_id", name = "rs_problem_idx"),
                @Index(columnList = "u_archive_problem", name = "rs_u_archive_problem_idx"),
                @Index(columnList = "sys_org, u_active_type, sys_updated_on", name = "rs_org_activetype_updatedon_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveSession extends BaseModel<ResolveSessionVO>
{
    private static final long serialVersionUID = -5778004912690522608L;
    
    private String UChangeRequest;      // deprecated
    private String UActiveType;

    private ArchiveWorksheet archiveWorksheet;
    private String UWorksheetId;
    private Boolean UIsArchive;
    private String worksheetNum;
    
    public ResolveSession()
    {
    }

    public ResolveSession(ResolveSessionVO vo)
    {
        applyVOToModel(vo);
    }


    @Column(name = "u_change_request", length = 32)
    public String getUChangeRequest()
    {
        return this.UChangeRequest;
    }

    public void setUChangeRequest(String UChangeRequest)
    {
        this.UChangeRequest = UChangeRequest;
    }

    // username
    @Column(name = "u_active_type", length = 255)
    public String getUActiveType()
    {
        return this.UActiveType;
    }

    public void setUActiveType(String UActiveType)
    {
        this.UActiveType = UActiveType;
    }

    @Column(name = "u_problem_sys_id", length = 40)
    public String getUWorksheetId()
    {
        return UWorksheetId;
    }

    public void setUWorksheetId(String uWorksheetId)
    {
        UWorksheetId = uWorksheetId;
    }

    @Column(name = "u_is_archive", length = 1)
    @Type(type = "yes_no")
    public Boolean getUIsArchive()
    {
        return UIsArchive;
    }

    public Boolean ugetUIsArchive()
    {
        Boolean isArchive = getUIsArchive();
        if(isArchive == null)
        {
            isArchive = false;
        }
        return isArchive;
    }

    public void setUIsArchive(Boolean uIsArchive)
    {
        UIsArchive = uIsArchive;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_archive_problem", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
     public ArchiveWorksheet getArchiveWorksheet()
    {
        return archiveWorksheet;
        
        // one-way only - no collections on the other side
    }

    public void setArchiveWorksheet(ArchiveWorksheet archiveWorksheet)
    {
        this.archiveWorksheet = archiveWorksheet;
    }
    
    // Worksheet Number
    @Column(name = "u_problem_num", length = 40)
    public String getWorksheetNum() {
        return worksheetNum;
    }

    public void setWorksheetNum(String worksheetNum) {
        this.worksheetNum = worksheetNum;
    }
    
    @Override
    public ResolveSessionVO doGetVO()
    {
        ResolveSessionVO vo = new ResolveSessionVO();
        super.doGetBaseVO(vo);
        
        vo.setUActiveType(getUActiveType());
        vo.setUChangeRequest(getUChangeRequest());
        vo.setUIsArchive(getUIsArchive());
        vo.setUWorksheetId(getUWorksheetId());
        vo.setWorksheetNum(getWorksheetNum());
        
        vo.setArchiveWorksheet(Hibernate.isInitialized(this.archiveWorksheet) && 
        					   getArchiveWorksheet() != null ? getArchiveWorksheet().doGetVO() : null);
        
        return vo;
    }

    @Override
    public void applyVOToModel(ResolveSessionVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUActiveType(StringUtils.isNotBlank(vo.getUActiveType()) && 
            					vo.getUActiveType().equals(VO.STRING_DEFAULT) ? getUActiveType() : vo.getUActiveType());
            this.setUChangeRequest(StringUtils.isNotBlank(vo.getUChangeRequest()) && 
            					   vo.getUChangeRequest().equals(VO.STRING_DEFAULT) ? 
            					   getUChangeRequest() : vo.getUChangeRequest());
            this.setUIsArchive(vo.getUIsArchive()!= null ? vo.getUIsArchive() : getUIsArchive());
            this.setUWorksheetId(StringUtils.isNotBlank(vo.getUWorksheetId()) && 
            					 vo.getUWorksheetId().equals(VO.STRING_DEFAULT) ? getUWorksheetId() : vo.getUWorksheetId());
            this.setWorksheetNum(StringUtils.isNotBlank(vo.getWorksheetNum()) && 
            					 vo.getWorksheetNum().equals(VO.STRING_DEFAULT) ? getWorksheetNum() : vo.getWorksheetNum());
            
            this.setArchiveWorksheet(vo.getArchiveWorksheet() != null ? new ArchiveWorksheet(vo.getArchiveWorksheet()) : null);
        }  
    }
}
