/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.resolve.services.hibernate.vo.MetaFormViewPropertiesVO;
import com.resolve.services.interfaces.VO;

//THIS IS DEPRECATED AND NOT USED ANYWHERE
@Entity
@Table(name = "meta_form_view_properties")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaFormViewProperties extends BaseModel<MetaFormViewPropertiesVO>
{
    private static final long serialVersionUID = -7799647513822766802L;
    
    private Integer ULabelWidth;
    private String ULabelAlign;
    
    public MetaFormViewProperties()
    {
    }
    
    public MetaFormViewProperties(MetaFormViewPropertiesVO vo)
    {
        applyVOToModel(vo);
    }
    
    @Column(name = "u_label_width")
    public Integer getULabelWidth()
    {
        return this.ULabelWidth;
    }

    public void setULabelWidth(Integer labelWidth)
    {
        this.ULabelWidth = labelWidth;
    }
    
    @Column(name = "u_label_align", length = 100)
    public String getULabelAlign()
    {
        return ULabelAlign;
    }

    public void setULabelAlign(String uLabelAlign)
    {
        ULabelAlign = uLabelAlign;
    }
    
    @Override
    public MetaFormViewPropertiesVO doGetVO()
    {
        MetaFormViewPropertiesVO vo = new MetaFormViewPropertiesVO();
        super.doGetBaseVO(vo);
        
        vo.setULabelWidth(getULabelWidth());
        
        return vo;
    }

    @Override
    public void applyVOToModel(MetaFormViewPropertiesVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);    
            
            this.setULabelWidth(vo.getULabelWidth() != null && vo.getULabelWidth().equals(VO.INTEGER_DEFAULT) ? getULabelWidth() : vo.getULabelWidth());
            
        }
        
    }

    
}
