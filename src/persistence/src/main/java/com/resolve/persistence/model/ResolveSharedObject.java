/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "resolve_shared_object")
public class ResolveSharedObject implements java.io.Serializable
{
    private static final long serialVersionUID = -1627262083711549312L;
    private String sys_id;
    private String UName;
    private Integer UNumber;
    private byte[] UContent;
    private Date sysCreatedOn;
    private String sysCreatedBy;

    public ResolveSharedObject()
    {
    }

    public ResolveSharedObject(String sys_id)
    {
        this.sys_id = sys_id;
    }

    public ResolveSharedObject(String sys_id, String UName, Integer UNumber)
    {
        this.sys_id = sys_id;
        this.UName = UName;
        this.UNumber = UNumber;
    }

    @Id
    @Column(name = "sys_id",  nullable = false, length = 32)
    @GeneratedValue(generator = "hibernate-uuid")
    public String getsys_id()
    {
        return this.sys_id;
    }

    public void setsys_id(String sys_id)
    {
        this.sys_id = sys_id;
    }

    @Column(name = "u_name", length = 400, unique = true)
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }

    @Column(name = "u_number")
    public Integer getUNumber()
    {
        return this.UNumber;
    }

    public void setUNumber(Integer number)
    {
        this.UNumber = number;
    }
    
    
    @Lob
    @Column(name = "u_content", length = 16777215)
    public byte[] getUContent()
    {
        return UContent;
    }

    public void setUContent(byte[] UContent)
    {
        this.UContent = UContent;
//
//    	if (UContent == null)
//            this.UContent = new byte[0];
//        else
//        {
//            this.UContent = UContent;
//        }
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sys_created_on", length = 19)
    public Date getSysCreatedOn() {
        return sysCreatedOn;
    }

    public void setSysCreatedOn(Date sysCreatedOn) {
        this.sysCreatedOn = sysCreatedOn;
    }
    
    @Column(name = "sys_created_by", length = 255)
    public String getSysCreatedBy() {
        return sysCreatedBy;
    }

    public void setSysCreatedBy(String sysCreatedBy) {
        this.sysCreatedBy = sysCreatedBy;
    }
}



