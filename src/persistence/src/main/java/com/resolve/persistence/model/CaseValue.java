package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CaseValue {
	private String value;
	private Boolean defaultValue = false;

	public CaseValue() {}
	
	public CaseValue(String value, Boolean defaultValue) {
		this.value = value;
		this.defaultValue = defaultValue;
	}

	@Column(name = "u_value", length = 255)
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Column(name = "u_default")
	public Boolean getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(Boolean defaultValue) {
		this.defaultValue = defaultValue;
	}
}
