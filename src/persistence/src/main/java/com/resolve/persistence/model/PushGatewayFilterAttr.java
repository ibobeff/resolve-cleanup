package com.resolve.persistence.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.interfaces.VO;
import com.resolve.services.hibernate.vo.PushGatewayFilterAttrVO;

@Entity
@Table(name = "push_gateway_filter_attr", 
	   uniqueConstraints = { 
			   			       @UniqueConstraint(columnNames = { "u_name", "push_filter_id" }, 
			   			    		   			 name = "pushgfa_name_val_fltrid_uk") 
			   			   },
	   indexes = {
		             @Index(columnList = "push_filter_id", name = "pushgfa_fltrid_idx")
 			 	 })
//@Table(name = "push_gateway_filter_attr", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "push_filter_id" }) })
public class PushGatewayFilterAttr extends BaseModel<PushGatewayFilterAttrVO> {

    private static final long serialVersionUID = 1L;

    private String UName;
    private String UValue;
//    private String UPushFilterId;
    // object reference
    private PushGatewayFilter pushGatewayFilter;

    public PushGatewayFilterAttr() {
    }

    public PushGatewayFilterAttr(PushGatewayFilterAttrVO vo) {
        applyVOToModel(vo);
    }
    
    @Column(name = "u_name", length = 255)
    public String getUName()
    {
        return UName;
    }

    public void setUName(String uName)
    {
        UName = uName;
    }

    @Column(name = "u_value", length = 4000)
    public String getUValue()
    {
        return UValue;
    }

    public void setUValue(String uValue)
    {
        UValue = uValue;
    }
    
//    @Column(name = "push_filter_id", length = 32)
//    public String getUPushFilterId() {
//		return UPushFilterId;
//	}
//
//	public void setUPushFilterId(String uPushFilterId) {
//		UPushFilterId = uPushFilterId;
//	}

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "push_filter_id")
    @NotFound(action = NotFoundAction.EXCEPTION)
    public PushGatewayFilter getPushGatewayFilter()
    {
        return pushGatewayFilter;
    }

    public void setPushGatewayFilter(PushGatewayFilter pushGatewayFilter)
    {
        this.pushGatewayFilter = pushGatewayFilter;
        
        if (pushGatewayFilter != null)
        {
        	Collection<PushGatewayFilterAttr> coll = pushGatewayFilter.getAttrs();
            
            if (coll == null) {
                coll = new HashSet<PushGatewayFilterAttr>();
                coll.add(this);

                pushGatewayFilter.setAttrs(coll);
            }
        }
    }

    public PushGatewayFilterAttrVO doGetVO() {
        
        PushGatewayFilterAttrVO vo = new PushGatewayFilterAttrVO();
        
        super.doGetBaseVO(vo);

        vo.setUName(getUName());
        vo.setUValue(getUValue());
//        vo.setUPushFilterId(UPushFilterId);
        return vo;
    }

    @Override
    public void applyVOToModel(PushGatewayFilterAttrVO vo) {
        
        if (vo == null) return;
        
        super.applyVOToModel(vo);

        if (!VO.STRING_DEFAULT.equals(vo.getUName())) this.setUName(vo.getUName()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUValue())) this.setUValue(vo.getUValue()); else ;
//        if (!VO.STRING_DEFAULT.equals(vo.getUPushFilterId())) this.setUValue(vo.getUPushFilterId()); else ;
    }
    
} // class PushGatewayFilterAttrVO