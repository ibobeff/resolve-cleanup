package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.resolve.services.hibernate.vo.ArchiveSirAuditLogVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "archive_sir_auditlog")
public class ArchiveSirAuditLog extends BaseModel<ArchiveSirAuditLogVO>
{
	private static final long serialVersionUID = 2635686742933984622L;

	private String userName;
	private String ipAddress;
	private String description;
	private String incidentId;
	
	@Column(name = "u_username",  length = 255)
	public String getUserName()
	{
		return userName;
	}
	public void setUserName(String user)
	{
		this.userName = user;
	}
	
	@Column(name = "u_ipaddress",  length = 45)
	public String getIpAddress()
	{
		return ipAddress;
	}
	public void setIpAddress(String ipAddress)
	{
		this.ipAddress = ipAddress;
	}

	@Column(name = "u_description",  length = 2000)
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	
	@Column(name = "u_incident_id",  length = 32)
	public String getIncidentId()
    {
        return incidentId;
    }
    public void setIncidentId(String incidentId)
    {
        this.incidentId = incidentId;
    }

	@Override
	public ArchiveSirAuditLogVO doGetVO()
	{
		ArchiveSirAuditLogVO vo = new ArchiveSirAuditLogVO();
		super.doGetBaseVO(vo);
		
		vo.setUserName(this.getUserName());
		vo.setIpAddress(this.getIpAddress());
		vo.setDescription(this.getDescription());
		vo.setIncidentId(this.getIncidentId());
		
		return vo;
	}
	
	@Override
	public void applyVOToModel(ArchiveSirAuditLogVO vo)
	{
		if (vo != null)
		{
			super.applyVOToModel(vo);
			
			this.setUserName(StringUtils.isNotBlank(vo.getUserName()) && vo.getUserName().equals(VO.STRING_DEFAULT) ? getUserName() : vo.getUserName());
			this.setIpAddress(StringUtils.isNotBlank(vo.getIpAddress()) && vo.getIpAddress().equals(VO.STRING_DEFAULT) ? getIpAddress() : vo.getIpAddress());
			this.setDescription(StringUtils.isNotBlank(vo.getDescription()) && vo.getDescription().equals(VO.STRING_DEFAULT) ? getDescription() : vo.getDescription());
			this.setIncidentId(StringUtils.isNotBlank(vo.getIncidentId()) && vo.getIncidentId().equals(VO.STRING_DEFAULT) ? getIncidentId() : vo.getIncidentId());
		}
	}
}
