/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import com.resolve.services.interfaces.VO;
import com.resolve.util.DateUtils;
import com.resolve.util.JavaUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
@MappedSuperclass
/**
 * Base entity that simply contains common properties of all hibernate entities.
 *
 * @param <T>
 */
public abstract class BaseModel<T> implements java.io.Serializable, ModelToVO<T>
{
    public static final String SYS_ORG_FIELD_NAME = "sysOrg";
    
    // sys fields
    private String sys_id;
    private Date sysCreatedOn;
    private String sysCreatedBy;
    private Date sysUpdatedOn;
    private String sysUpdatedBy;
    private Integer sysModCount;

    private String sysOrg;
    
    //to indicate if the record is deleted or not. Will be used on case by case basis
    private Boolean sysIsDeleted;
    private Integer checksum;
    
    /*
    // Version control fields
    
    // 0 - No version support (default), starts with 1
    private Long sysVerNumber;
    // 0 - Root, +ve # - sysVerNumber of previous version, -ve # - sysVerNumber ISR branch depends on
    private Long sysParentVerNumber;
    private Date sysVerCreatedOn;
    private Date sysVerExpiredOn;
    private String sys_isr_id;
    private String sysVerCopiedBy;
    */
    
    public BaseModel()
    {
        setSysModCount(VO.NON_NEGATIVE_INTEGER_DEFAULT);
        setSysIsDeleted(VO.BOOLEAN_DEFAULT);
    }

    public BaseModel(T t )
    {
        applyVOToModel(t);
    }

    @Id
    @Column(name = "sys_id",  nullable = false, length = 32)
    @GeneratedValue(generator = "resolve-uuid")
    public String getSys_id()
    {
        return sys_id;
    }
    public void setSys_id(String sys_id)
    {
        this.sys_id = sys_id != null ? sys_id.trim() : sys_id;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sys_created_on", length = 19)
    public Date getSysCreatedOn()
    {
        return sysCreatedOn;
    }
    public void setSysCreatedOn(Date sysCreatedOn)
    {
        this.sysCreatedOn = sysCreatedOn;
    }

    @Column(name = "sys_created_by", length = 255)
    public String getSysCreatedBy()
    {
        return sysCreatedBy;
    }
    public void setSysCreatedBy(String sysCreatedBy)
    {
        this.sysCreatedBy = sysCreatedBy != null ? sysCreatedBy.trim() : sysCreatedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sys_updated_on", length = 19)
    public Date getSysUpdatedOn()
    {
        return sysUpdatedOn;
    }
    public void setSysUpdatedOn(Date sysUpdatedOn)
    {
        this.sysUpdatedOn = sysUpdatedOn;
    }

    @Column(name = "sys_updated_by", length = 255)
    public String getSysUpdatedBy()
    {
        return sysUpdatedBy;
    }
    public void setSysUpdatedBy(String sysUpdatedBy)
    {
        this.sysUpdatedBy = sysUpdatedBy != null ? sysUpdatedBy.trim() : sysUpdatedBy;
    }

    @Column(name = "sys_mod_count")
    public Integer getSysModCount()
    {
        return sysModCount;
    }
    public Integer ugetSysModCount()
    {
        if (getSysModCount() != null)
        {
            return getSysModCount();
        }
        else
        {
            return VO.NON_NEGATIVE_INTEGER_DEFAULT;
        }
    }
    public void setSysModCount(Integer sysModCount)
    {
        this.sysModCount = sysModCount;
    }
    
    @Column(name = "sys_org", length = 32)
    public String getSysOrg()
    {
        return sysOrg;
    }

    public void setSysOrg(String sysOrg)
    {
        this.sysOrg = sysOrg != null ? sysOrg.trim() : sysOrg;
    }
    
    @Column(name = "sys_is_deleted", length = 1)
    @Type(type = "yes_no")
    public Boolean getSysIsDeleted()
    {
        return sysIsDeleted;
    }

    public Boolean ugetSysIsDeleted()
    {
        Boolean isDeleted = VO.BOOLEAN_DEFAULT;
        
        if(getSysIsDeleted() != null)
        {
            isDeleted = getSysIsDeleted();
        }
        
        return isDeleted;
    }

    public void setSysIsDeleted(Boolean sysIsDeleted)
    {
        this.sysIsDeleted = sysIsDeleted;
    }
    
    @Column(name = "checksum")
    public Integer getChecksum()
    {
        return checksum;
    }

    public void setChecksum(Integer checksum)
    {
        this.checksum = checksum;
    }

    public Map<String, Object> covertVOToMap()
    {
        return JavaUtils.covertObjectToMap(this);
    }
    
    public T doGetVO(boolean secured) {
        if(!secured) {
            return doGetVO();
        }
        return secureGetVO();
    }
    /** 
     * override this method for VO object that need to be secure such as blank out password
     */
    public T secureGetVO()
    {
        // TODO Auto-generated method stub
        return doGetVO();
    }

    protected void doGetBaseVO(VO vo)
    {
        try
        {
            vo.setId(this.getSys_id());
            vo.setSys_id(this.getSys_id());
            vo.setSysCreatedBy(this.getSysCreatedBy());
            vo.setSysModCount(this.ugetSysModCount());
            vo.setSysUpdatedBy(this.getSysUpdatedBy());
            vo.setSysOrg(this.getSysOrg());
            vo.setSysIsDeleted(this.ugetSysIsDeleted());
//            vo.setSysPerm(get)

            vo.setSysUpdatedOn(this.getSysUpdatedOn());
            vo.setSysCreatedOn(this.getSysCreatedOn());
            vo.setChecksum(this.getChecksum());
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    protected Date massageDateForUI(Date date)
    {
        Date newDate = date;
        if( date != null ) {
            try
            {
            	String dStr = DateUtils.getDateInUTCFormat(new Timestamp(date.getTime()));
                DateTime dt = DateUtils.parseISODate(dStr);
                newDate = dt.toDate();
            }
            catch(Throwable t)
            {
                //do nothing
            }
        }
        return newDate;
    }

    public void applyVOToModel(T t)
    {
        if(t != null)
        {
            try
            {
                VO vo = (VO) t;
                
                //if the sysId exist, DO NOT update the created fields
                if(StringUtils.isBlank(this.getSys_id()))
                {
                    this.setSysCreatedBy(StringUtils.isNotBlank(vo.getSysCreatedBy()) && vo.getSysCreatedBy().equals(VO.STRING_DEFAULT) ? getSysCreatedBy() : vo.getSysCreatedBy());
                    this.setSysCreatedOn(vo.getSysCreatedOn()!= null && vo.getSysCreatedOn().equals(VO.DATE_DEFAULT) ? getSysCreatedOn() : vo.getSysCreatedOn());
                    //this.setSysOrg(StringUtils.isNotBlank(vo.getSysOrg()) && vo.getSysOrg().equals(VO.STRING_DEFAULT) ? getSysOrg() : vo.getSysOrg());
                }
                
                this.setSysOrg(StringUtils.isNotBlank(vo.getSysOrg()) && vo.getSysOrg().equals(VO.STRING_DEFAULT) ? getSysOrg() : vo.getSysOrg());
                
                if(StringUtils.isBlank(vo.getSys_id()) || vo.getSys_id().equals(VO.STRING_DEFAULT))
                {
                    this.setSys_id(StringUtils.isEmpty(vo.getId()) || (StringUtils.isNotEmpty(vo.getId()) && vo.getId().equals(VO.STRING_DEFAULT)) ? getSys_id() : vo.getId());
                }
                else
                {
                    this.setSys_id(StringUtils.isEmpty(vo.getSys_id()) || (StringUtils.isNotEmpty(vo.getSys_id()) && vo.getSys_id().equals(VO.STRING_DEFAULT)) ? getSys_id() : vo.getSys_id());
                }
                                
                this.setSysUpdatedBy(StringUtils.isNotBlank(vo.getSysUpdatedBy()) && vo.getSysUpdatedBy().equals(VO.STRING_DEFAULT) ? getSysUpdatedBy() : vo.getSysUpdatedBy());
                this.setSysUpdatedOn(vo.getSysUpdatedOn()!= null && vo.getSysUpdatedOn().equals(VO.DATE_DEFAULT) ? getSysUpdatedOn() : vo.getSysUpdatedOn());
                this.setSysModCount(vo.getSysModCount() != null && vo.getSysModCount().equals(VO.INTEGER_DEFAULT) ? ugetSysModCount() : vo.getSysModCount());
                this.setSysIsDeleted(vo.getSysIsDeleted() != null ? vo.getSysIsDeleted() : ugetSysIsDeleted());
                this.setChecksum(vo.getChecksum() != null && vo.getChecksum().equals(VO.INTEGER_DEFAULT) ? getChecksum() : vo.getChecksum());
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
    
    public void resetFieldsWithDefaultValues()
    {
        setSysModCount(null);
        setSysIsDeleted(null);
    }
}
