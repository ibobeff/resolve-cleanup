/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolvePreprocessRelVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "resolve_preprocess_rel", indexes = {@Index(columnList = "u_preprocess_sys_id", name = "u_preprocess_rel_sys_id_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolvePreprocessRel extends BaseModel<ResolvePreprocessRelVO>
{
    private static final long serialVersionUID = -1208952426462587316L;
    
    private ResolvePreprocess preprocess;
	private String URefPreprocess; 
	
	public ResolvePreprocessRel()
	{
	}

    public ResolvePreprocessRel(ResolvePreprocessRelVO vo)
    {
        applyVOToModel(vo);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_preprocess_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolvePreprocess getPreprocess()
    {
        return preprocess;
    }

    public void setPreprocess(ResolvePreprocess preprocess)
    {
        this.preprocess = preprocess;
    }

    @Column(name = "u_ref_preprocess", length = 400)
    public String getURefPreprocess()
    {
        return URefPreprocess;
    }

    public void setURefPreprocess(String URefPreprocess)
    {
        this.URefPreprocess = URefPreprocess;
    }

    @Override
    public ResolvePreprocessRelVO doGetVO()
    {
        ResolvePreprocessRelVO vo = new ResolvePreprocessRelVO();
        super.doGetBaseVO(vo);
        
        vo.setURefPreprocess(getURefPreprocess());
        
        //references - do not do this to avoid cyclic ref
//        vo.setAssessor(Hibernate.isInitialized(this.assessor) && getAssessor() != null ? getAssessor().doGetVO() : null);
        
        return vo;
    }

    @Override
    public void applyVOToModel(ResolvePreprocessRelVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo); 

            this.setURefPreprocess(StringUtils.isNotBlank(vo.getURefPreprocess()) && vo.getURefPreprocess().equals(VO.STRING_DEFAULT) ? getURefPreprocess() : vo.getURefPreprocess());
        }
        
        
    }

}
