/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.resolve.services.hibernate.vo.MetaViewLookupVO;
import com.resolve.services.interfaces.VO;


@Entity
@Table(name = "meta_view_lookup")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaViewLookup  extends BaseModel<MetaViewLookupVO>
{
    private static final long serialVersionUID = 5087997264841844973L;

    private String UAppName;
    private String UViewName;
    private String URoles;
    private Integer UOrder;
    
    
    public MetaViewLookup()
    {
    }
    
    public MetaViewLookup(MetaViewLookupVO vo)
    {
        applyVOToModel(vo);
    }
    
    @Column(name = "u_app_name", length = 100)
    public String getUAppName()
    {
        return UAppName;
    }

    public void setUAppName(String uAppName)
    {
        UAppName = uAppName;
    }

    @Column(name = "u_view_name", length = 100)
    public String getUViewName()
    {
        return UViewName;
    }

    public void setUViewName(String uViewName)
    {
        UViewName = uViewName;
    }

    @Column(name = "u_roles", length = 500)
    public String getURoles()
    {
        return URoles;
    }

    public void setURoles(String uRoles)
    {
        URoles = uRoles;
    }

    @Column(name = "u_order")
    public Integer getUOrder()
    {
        return UOrder;
    }

    public void setUOrder(Integer uOrder)
    {
        UOrder = uOrder;
    }

    @Override
    public MetaViewLookupVO doGetVO()
    {
        MetaViewLookupVO vo = new MetaViewLookupVO();
        super.doGetBaseVO(vo);
        
        vo.setUAppName(getUAppName());
        vo.setUOrder(getUOrder());
        vo.setURoles(getURoles());
        vo.setUViewName(getUViewName());
        
        return vo;
    }

    @Override
    public void applyVOToModel(MetaViewLookupVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo); 

            this.setUAppName((String) VO.evaluateValue(vo.getUAppName(), getUAppName()));
            this.setUOrder((Integer) VO.evaluateValue(vo.getUOrder(), getUOrder()));
            this.setURoles((String) VO.evaluateValue(vo.getURoles(), getURoles()));
            this.setUViewName((String) VO.evaluateValue(vo.getUViewName(), getUViewName()));
        }
        
    }

    
    
}
