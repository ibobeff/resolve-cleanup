/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;


@Entity
@Table(name = "archive_tmp_process_req_id", indexes = {@Index(columnList = "u_problem", name = "atpr_u_problem")})
// @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ArchiveTmpProcessReqId implements java.io.Serializable
{
    private static final long serialVersionUID = -6410509642090721345L;
    private String sys_id;
    private String UProblem;

    public ArchiveTmpProcessReqId()
    {
    }

    @Id
    @Column(name = "sys_id", nullable = false, length = 32)
    public String getsys_id()
    {
        return this.sys_id;
    }

    public void setsys_id(String sys_id)
    {
        this.sys_id = sys_id;
    }
 
    @Column(name = "u_problem", length = 32)
    public String getUProblem()
    {
        return this.UProblem;
    }

    public void setUProblem(String UProblem)
    {
        this.UProblem = UProblem;
    }
} // ArchiveTmpProcessReqId
