/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.MetaFormVO;
import com.resolve.services.hibernate.vo.MetaFormViewVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;


@Deprecated
@Entity
@Table(name = "meta_form")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaForm extends BaseModel<MetaFormVO>
{
    private static final long serialVersionUID = 3058567806995690428L;
    
    private String UName;
    
    // object referenced by
    private Collection<MetaFormView> metaFormViews;
    
    public MetaForm()
    {
    }
    
    public MetaForm(MetaFormVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_name", nullable = false, length = 100)
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<MetaFormView> getMetaFormViews()
    {
        return this.metaFormViews;
    }

    public void setMetaFormViews(Collection<MetaFormView> metaFormViews)
    {
        this.metaFormViews = metaFormViews;
    }

    @Override
    public MetaFormVO doGetVO()
    {
        MetaFormVO vo = new MetaFormVO();
        super.doGetBaseVO(vo);
        
        vo.setUName(getUName());
        
        //refs
        Collection<MetaFormView> metaFormViews = Hibernate.isInitialized(this.metaFormViews) ? getMetaFormViews() : null;
        if(metaFormViews != null && metaFormViews.size() > 0)
        {
            Collection<MetaFormViewVO> vos = new ArrayList<MetaFormViewVO>();
            for(MetaFormView model : metaFormViews)
            {
                vos.add(model.doGetVO());
            }
            vo.setMetaFormViews(vos);
        }
        
        return vo;
    }

    @Override
    public void applyVOToModel(MetaFormVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);  
            
            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            
            Collection<MetaFormViewVO> metaFormViewVOs = vo.getMetaFormViews();
            if(metaFormViewVOs != null && metaFormViewVOs.size() > 0)
            {
                Collection<MetaFormView> models = new ArrayList<MetaFormView>();
                for(MetaFormViewVO refVO : metaFormViewVOs)
                {
                    models.add(new MetaFormView(refVO));
                }
                this.setMetaFormViews(models);
            }

        }
        
    }
}
