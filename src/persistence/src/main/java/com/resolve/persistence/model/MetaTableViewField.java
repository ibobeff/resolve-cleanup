/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.MetaTableViewFieldVO;
import com.resolve.services.interfaces.VO;


@Entity
@Table(name = "meta_table_view_field", indexes = {
                @Index(columnList = "u_meta_table_view_sys_id", name = "mtvf_u_meta_table_view_idx"),
                @Index(columnList = "u_meta_field_sys_id", name = "mtvf_u_meta_field_idx"),
                @Index(columnList = "u_meta_view_field_sys_id", name = "mtvf_u_meta_view_field_idx"),
                @Index(columnList = "u_meta_field_properties_sys_id", name = "mtvf_u_meta_field_prop_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaTableViewField  extends BaseModel<MetaTableViewFieldVO>
{
    private static final long serialVersionUID = 3721199823426939501L;

    // object referenced
    private MetaTableView metaTableView;
    private MetaField metaField;
//    private MetaStyle metaStyle;
    private MetaViewField metaViewField;
    
    //this will be for the DB field - and mapped to form - so 2 forms can have 1 metafield that can have different properties
    //if this is null, then use the metafield properties
    private MetaFieldProperties metaFieldTableProperties;

    private Integer UOrder;
    
    public MetaTableViewField()
    {
    }
    
    public MetaTableViewField(MetaTableViewFieldVO vo)
    {
        applyVOToModel(vo);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_meta_table_view_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaTableView getMetaTableView()
    {
        return metaTableView;
    }

    public void setMetaTableView(MetaTableView metaTableView)
    {
        this.metaTableView = metaTableView;

        if (metaTableView != null)
        {
            Collection<MetaTableViewField> coll = metaTableView.getMetaTableViewFields();
            if (coll == null)
            {
                coll = new HashSet<MetaTableViewField>();
                coll.add(this);
                
                metaTableView.setMetaTableViewFields(coll);
            }
        }
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_meta_field_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaField getMetaField()
    {
        return metaField;
    }

    public void setMetaField(MetaField metaField)
    {
        this.metaField = metaField;
    }
    
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "u_meta_style_sys_id")
//    @Index(name = "mtvf_u_meta_style_idx")
//    @NotFound(action = NotFoundAction.IGNORE)
//    public MetaStyle getMetaStyle()
//    {
//        return metaStyle;
//    }
//    
//    public void setMetaStyle(MetaStyle metaStyle)
//    {
//        this.metaStyle = metaStyle;
//    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_meta_view_field_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaViewField getMetaViewField()
    {
        return metaViewField;
    }
    
    public void setMetaViewField(MetaViewField metaViewField)
    {
        this.metaViewField = metaViewField;
    }
    
    @Column(name = "u_order")
    public Integer getUOrder()
    {
        return this.UOrder;
    }

    public void setUOrder(Integer UOrder)
    {
        this.UOrder = UOrder;
    }
    
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_meta_field_properties_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaFieldProperties getMetaFieldTableProperties()
    {
        return metaFieldTableProperties;
    }

    public void setMetaFieldTableProperties(MetaFieldProperties metaFieldFormProperties)
    {
        this.metaFieldTableProperties = metaFieldFormProperties;
    }

    @Override
    public MetaTableViewFieldVO doGetVO()
    {
        MetaTableViewFieldVO vo = new MetaTableViewFieldVO();
        super.doGetBaseVO(vo);
        
        vo.setUOrder(getUOrder());
        
        //except for the parent
        vo.setMetaField(Hibernate.isInitialized(this.metaField) && getMetaField() != null ? getMetaField().doGetVO() : null) ;
//        vo.setMetaStyle(Hibernate.isInitialized(this.metaStyle) && getMetaStyle() != null ? getMetaStyle().doGetVO() : null) ;
        vo.setMetaViewField(Hibernate.isInitialized(this.metaField) && getMetaViewField() != null ? getMetaViewField().doGetVO() : null) ;
        vo.setMetaFieldTableProperties(Hibernate.isInitialized(this.metaFieldTableProperties) && getMetaFieldTableProperties() != null ? getMetaFieldTableProperties().doGetVO() : null) ;
        
        return vo;
    }

    @Override
    public void applyVOToModel(MetaTableViewFieldVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUOrder((Integer) VO.evaluateValue(vo.getUOrder(), getUOrder()));
            
        }
    }
   
}
