package com.resolve.persistence.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.Hibernate;
import org.hibernate.annotations.ColumnDefault;

//import org.hibernate.annotations.Cache;
//import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.resolve.services.hibernate.vo.SIRActivityRuntimeDataVO;

import static com.resolve.services.interfaces.VO.*;
import static com.resolve.util.StringUtils.*;

import java.util.Date;

@Entity
@Table(name = "sir_activity_runtimedata",
       uniqueConstraints = {
                       @UniqueConstraint(columnNames = {"u_resolve_security_incident_id",
                                                        "u_sir_phase_metadata_id",
                                                        "u_sir_activity_metadata_id",
                                                        "u_position"}, 
                                         name = "sard_rsi_phase_activity_position_uk")
       },
       indexes = {
                @Index(columnList = "sys_org, u_resolve_security_incident_id", name = "sard_org_rsi_idx"),
                @Index(columnList = "sys_org, u_assignee, u_status", name = "sard_org_assignee_status_idx")
       })
/*
 * Enable Hibernate L2 Cache only if it is distributed 
 * accross all nodes in cluster.
 */
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SIRActivityRuntimeData extends BaseModel<SIRActivityRuntimeDataVO> {
    
    private static final long serialVersionUID = -198746069183294647L;

    public static final Date LATEST_DUE_BY = new Date(Long.MAX_VALUE);
    public static final Integer LAST_POSITION = Integer.MAX_VALUE;
    
    private String assignee;                        // User id of user assignd to this activity
    private String status;                          // Activiy status
    private Integer position;                       // Position of this Activity within the Phase in SIR
    
    // object references
    private SIRPhaseMetaData phaseMetaData;         // SIR Phase Meta Data (lazily loaded)
    private SIRActivityMetaData activityMetaData;   // SIR Activity Meta Data (lazily loaded)
    private ResolveSecurityIncident referencedRSI;  // Referenced Resolve Security Incident (lazily loaded)
    private Users assignedUser;                     // (fully hydrated) Users assignd to this activity (lazilly loaded) 
	 
	public SIRActivityRuntimeData() {
	    super();
	}
	
	public SIRActivityRuntimeData(SIRActivityRuntimeDataVO vo) {
        applyVOToModel(vo);
    }
	
	@Column(name = "u_assignee", length = 255, nullable = true, updatable = true)
    public String getAssignee() {
        return assignee;
    }
    
    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }
    
    @Column(name = "u_status", length = 100, nullable = false, updatable=true)
    public String getStatus() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    
    @Column(name = "u_position", nullable=false, updatable=true)
    @ColumnDefault(value = "2147483647")
    public Integer getPosition() {
        return position;
    }
    
    public Integer ugetPosition() {
        if (getPosition() != null && getPosition() > NON_NEGATIVE_INTEGER_DEFAULT) {
            return getPosition();
        } else {
            return LAST_POSITION;
        }
    }
    
    public void setPosition(Integer position) {
        this.position = position;
    }
    
	@ManyToOne(optional = false, cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
	@JoinColumn(name = "u_sir_phase_metadata_id", referencedColumnName = "sys_id", nullable=false, updatable=true, 
	            foreignKey = @ForeignKey(name = "sard_sir_phase_metadata_fk", value = ConstraintMode.CONSTRAINT))
	public SIRPhaseMetaData getPhaseMetaData() {
		return phaseMetaData;
	}
	
	public void setPhaseMetaData(SIRPhaseMetaData phaseMetaData) {
		this.phaseMetaData = phaseMetaData;
	}
	
	@ManyToOne(optional = false, cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @JoinColumn(name = "u_sir_activity_metadata_id", referencedColumnName = "sys_id", nullable=false, updatable=true, 
                foreignKey = @ForeignKey(name = "sard_sir_activity_metadata_fk", value = ConstraintMode.CONSTRAINT))
    public SIRActivityMetaData getActivityMetaData() {
        return activityMetaData;
    }
    
    public void setActivityMetaData(SIRActivityMetaData activityMetaData) {
        this.activityMetaData = activityMetaData;
    }
    
	@ManyToOne(optional = false, cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @JoinColumn(name = "u_resolve_security_incident_id", referencedColumnName = "sys_id", nullable=false, updatable=false, 
                foreignKey = @ForeignKey(name = "sard_rsi_fk", value = ConstraintMode.CONSTRAINT))
    public ResolveSecurityIncident getReferencedRSI() {
        return referencedRSI;
    }
    
    public void setReferencedRSI(ResolveSecurityIncident referencedRSI) {
        this.referencedRSI = referencedRSI;
    }
    
    @ManyToOne(optional = true, cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @JoinColumn(name = "u_user_sys_id", referencedColumnName = "sys_id", nullable=true, updatable = true, 
                foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    public Users getAssignedUser() {
        return assignedUser;
    }
    
    public void setAssignedUser(Users assignedUser) {
        this.assignedUser = assignedUser;
    }
    
    @Override
	public SIRActivityRuntimeDataVO doGetVO() {
		SIRActivityRuntimeDataVO vo = new SIRActivityRuntimeDataVO();
		
		super.doGetBaseVO(vo);
		
		vo.setAssignee(getAssignee());
		vo.setStatus(getStatus());
		vo.setPosition(getPosition());
		
		//references
		vo.setPhaseMetaData(Hibernate.isInitialized(phaseMetaData) && getPhaseMetaData() != null ? 
		                    getPhaseMetaData().doGetVO() : null);
		vo.setActivityMetaData(Hibernate.isInitialized(activityMetaData) && getActivityMetaData() != null ? 
                               getActivityMetaData().doGetVO() : null);
		vo.setReferencedRSI(Hibernate.isInitialized(referencedRSI) && getReferencedRSI() != null ?
		                    getReferencedRSI().doGetVO() : null);
		vo.setAssignedUser(Hibernate.isInitialized(assignedUser) && getAssignedUser() != null ?
		                   getAssignedUser().doGetVO() : null);
		
		return vo;
	}
	
	@Override
	public void applyVOToModel(SIRActivityRuntimeDataVO vo) {
		if (vo != null && vo.getPhaseMetaData() != null && vo.getReferencedRSI() != null) {
			super.applyVOToModel(vo);
			
			setAssignee(isNotBlank(vo.getAssignee()) && vo.getAssignee().equals(STRING_DEFAULT) ?
			            getAssignee() : vo.getAssignee());
			setStatus(isNotBlank(vo.getStatus()) && vo.getStatus().equals(STRING_DEFAULT) ?
	                  getStatus() : vo.getStatus());
			setPosition(vo.ugetPosition() != null && vo.ugetPosition() <= NON_NEGATIVE_INTEGER_DEFAULT ? 
			            ugetPosition() : vo.ugetPosition());
			
			//references
			setPhaseMetaData(new SIRPhaseMetaData(vo.getPhaseMetaData()));
			setActivityMetaData(new SIRActivityMetaData(vo.getActivityMetaData()));
			setReferencedRSI(new ResolveSecurityIncident(vo.getReferencedRSI()));
			
			if (vo.getAssignedUser() != null) { 
			    setAssignedUser(new Users(vo.getAssignedUser()));
			}
		}
	}
	
	@Override
    public String toString() {
        return "SIR Activity Runtime Data [" +
               (isNotBlank(getSys_id()) ? "Sys Id: " + getSys_id() + ", " : "") + 
               "Sys Org: " + (isNotBlank(getSysOrg()) ? getSysOrg() : "") + 
               ", Assignee: " + getAssignee() + ", Status: " + getStatus() + ", Position: " + ugetPosition() +
               ", Phase Meta Data: " + getPhaseMetaData() + 
               ", Activity Meta Data: " + getActivityMetaData() + 
               ", Referenced Rsolve Security Incident: " + getReferencedRSI() +
               ", Assigned User: " + (getAssignedUser() != null ? getAssignedUser() : "") +
               "]";
    }
}
