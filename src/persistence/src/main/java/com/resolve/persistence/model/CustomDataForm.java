package com.resolve.persistence.model;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.CustomDataFormVO;

@Entity
@Table(name = "custom_data_form")
public class CustomDataForm extends BaseModel<CustomDataFormVO>{
	private static final long serialVersionUID = -84401839739254255L;
	private String wikiId;
	private Map<String, String> formRecord = new HashMap<>();
	private ResolveSecurityIncident incident;

	public CustomDataForm() {}

	public CustomDataForm(String wikiId, Map<String, String> formRecord) {
		this.wikiId = wikiId;
		this.formRecord = formRecord;
	}

	@Column(name = "u_custom_data_wiki")
	public String getWikiId() {
		return wikiId;
	}

	public void setWikiId(String wikiId) {
		this.wikiId = wikiId;
	}

	@ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "custom_data_form_record", joinColumns = @JoinColumn(name = "custom_data_form_id"))
	@MapKeyColumn(name="u_form_name")
	@Column(name = "u_record_id")
	public Map<String, String> getFormRecord() {
		return formRecord;
	}

	public void setFormRecord(Map<String, String> formRecord) {
		this.formRecord = formRecord;
	}

	@ManyToOne(fetch = FetchType.LAZY)	
    @JoinColumn(name = "sir_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT) )
    @NotFound(action = NotFoundAction.EXCEPTION)
	public ResolveSecurityIncident getIncident() {
		return incident;
	}

	public void setIncident(ResolveSecurityIncident incident) {
		this.incident = incident;
	}

	@Override
	public CustomDataFormVO doGetVO() {
		CustomDataFormVO vo = new CustomDataFormVO();
        super.doGetBaseVO(vo);
        
    	vo.setWikiId(this.getWikiId());
    	vo.setFormRecord(new HashMap<String, String>());
    	for (String record : this.formRecord.keySet()) {
    		vo.getFormRecord().put(record, this.formRecord.get(record));
    	}
    	vo.setIncidentId(this.getIncident().getSys_id());
    	return vo;
	}
}
