/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.services.hibernate.vo.RemedyxFilterVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "remedyx_filter",  uniqueConstraints = {@UniqueConstraint(columnNames = {"u_name", "u_queue"}, name = "rmdyxf_u_name_u_queue_uk")})
// @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class RemedyxFilter extends GatewayFilter<RemedyxFilterVO>
{
    private static final long serialVersionUID = 5006182412766525005L;

    private String UFormName;
    private String UQuery;
    private String ULastValueField;
    private Integer ULimit;

    // transient

    // object references

    // object referenced by
    public RemedyxFilter()
    {
    } // TSRMFilter

    public RemedyxFilter(RemedyxFilterVO vo)
    {
        applyVOToModel(vo);
    } // TSRMFilter

    @Column(name = "u_form_name", length = 100)
    public String getUFormName()
    {
        return UFormName;
    }

    public void setUFormName(String uFormName)
    {
        UFormName = uFormName;
    }

    @Column(name = "u_query", length = 4000)
    public String getUQuery()
    {
        return this.UQuery;
    } // getUQuery

    public void setUQuery(String uQuery)
    {
        this.UQuery = uQuery;
    } // setUQuery

    @Column(name = "u_last_value_field", length = 400)
    public String getULastValueField()
    {
        return ULastValueField;
    }

    public void setULastValueField(String uLastValueField)
    {
        ULastValueField = uLastValueField;
    }
    
    @Column(name = "u_limit")
    public Integer getULimit()
    {
        return this.ULimit;
    }

    public void setULimit(Integer ULimit)
    {
        this.ULimit = ULimit;
    }

    @Override
    public RemedyxFilterVO doGetVO()
    {
        RemedyxFilterVO vo = new RemedyxFilterVO();
        super.doGetBaseVO(vo);

        vo.setUFormName(getUFormName());
        vo.setUQuery(getUQuery());
        vo.setULastValueField(getULastValueField());
        vo.setULimit(getULimit());

        return vo;
    }

    @Override
    public void applyVOToModel(RemedyxFilterVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);

            this.setUFormName(StringUtils.isNotBlank(vo.getUFormName()) && vo.getUFormName().equals(VO.STRING_DEFAULT) ? getUFormName() : vo.getUFormName());
            this.setUQuery(StringUtils.isNotBlank(vo.getUQuery()) && vo.getUQuery().equals(VO.STRING_DEFAULT) ? getUQuery() : vo.getUQuery());
            this.setULastValueField(StringUtils.isNotBlank(vo.getULastValueField()) && vo.getULastValueField().equals(VO.STRING_DEFAULT) ? getULastValueField() : vo.getULastValueField());
            this.setULimit(vo.getULimit()!= null && vo.getULimit().equals(VO.INTEGER_DEFAULT) ? getULimit() : vo.getULimit()); 
        }
    }
}

