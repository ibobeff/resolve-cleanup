/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.MetaFilterVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;


@Entity
@Table(name = "meta_filter", indexes = {
                @Index(columnList = "u_meta_access_rights_sys_id", name = "mfilter_access_rights_idx"),
                @Index(columnList = "u_meta_table_sys_id", name = "mfield_table")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaFilter extends BaseModel<MetaFilterVO>
{
    private static final long serialVersionUID = 8800332178963305642L;
    
    private String UName;
    private String UValue;
    private Boolean UIsGlobal;
    private Boolean UIsSelf;
    private Boolean UIsRole;
    private String UUserId;
    
    private MetaAccessRights metaAccessRights;
    private MetaTable metaTable;  
    
    public MetaFilter()
    {
    }
    
    public MetaFilter(MetaFilterVO vo)
    {
        applyVOToModel(vo);
    }
    
    @Column(name = "u_name", nullable = false, length = 100)
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    @Lob
    @Column(name = "u_value", length = 16777215)
    public String getUValue()
    {
        return this.UValue;
    }
    
    public void setUValue(String UValue)
    {
        this.UValue = UValue;
    }
    
    @Column(name = "u_is_global")
    public Boolean getUIsGlobal()
    {
        return this.UIsGlobal;
    } 
    
    public Boolean ugetUIsGlobal()
    {
        if (this.UIsGlobal == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UIsGlobal;
        }
    } 
    
    public void setUIsGlobal(Boolean UIsGlobal)
    {
        this.UIsGlobal = UIsGlobal;
    }
    
    @Column(name = "u_is_self")
    public Boolean getUIsSelf()
    {
        return this.UIsSelf;
    }
    
    public void setUIsSelf(Boolean UIsSelf)
    {
        this.UIsSelf = UIsSelf;
    }
    
    public Boolean ugetUIsSelf()
    {
        if(this.UIsSelf == null)
        {
            return new Boolean(false);
        }
        else
        {
            return new Boolean(true);
        }
    }
    
    @Column(name = "u_is_role")
    public Boolean getUIsRole()
    {
        return this.UIsRole;
    }
    
    public void setUIsRole(Boolean UIsRole)
    {
        this.UIsRole = UIsRole;
    }
    
    public Boolean ugetUIsRole()
    {
        if(this.UIsRole == null)
        {
            return new Boolean(false);
        }
        else
        {
            return new Boolean(true);
        }
    }
    
    @Column(name = "u_userid", nullable = false, length = 255)
    public String getUUserId()
    {
        return this.UUserId;
    }

    public void setUUserId(String UUserId)
    {
        this.UUserId = UUserId;
    }
    
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_meta_access_rights_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade({CascadeType.DELETE})
    public MetaAccessRights getMetaAccessRights()
    {
        return metaAccessRights;
    }

    public void setMetaAccessRights(MetaAccessRights metaAccessRights)
    {
        this.metaAccessRights = metaAccessRights;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_meta_table_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaTable getMetaTable()
    {
        return metaTable;
    }
    
    public void setMetaTable(MetaTable metaTable)
    {
        this.metaTable = metaTable;
        
        if(metaTable != null)
        {
            Collection<MetaFilter> coll = metaTable.getMetaFilters();
            if (coll == null)
            {
                coll = new HashSet<MetaFilter>();
                coll.add(this);
                
                metaTable.setMetaFilters(coll);
            }
        }
    }

    @Override
    public MetaFilterVO doGetVO()
    {
        MetaFilterVO vo = new MetaFilterVO();
        super.doGetBaseVO(vo);
        
        vo.setUIsGlobal(getUIsGlobal());
        vo.setUIsRole(getUIsRole());
        vo.setUIsSelf(getUIsSelf());
        vo.setUName(getUName());
        vo.setUUserId(getUUserId());
        vo.setUValue(getUValue());
        
        //refs
        vo.setMetaAccessRights(Hibernate.isInitialized(this.metaAccessRights) && getMetaAccessRights() != null ? getMetaAccessRights().doGetVO() : null) ;
        
        return vo;
    }

    @Override
    public void applyVOToModel(MetaFilterVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);   

            this.setUIsGlobal(vo.getUIsGlobal() != null ? vo.getUIsGlobal() : getUIsGlobal());
            this.setUIsRole(vo.getUIsRole() != null ? vo.getUIsRole() : getUIsRole());
            this.setUIsSelf(vo.getUIsSelf() != null ? vo.getUIsSelf() : getUIsSelf());
            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUUserId(StringUtils.isNotBlank(vo.getUUserId()) && vo.getUUserId().equals(VO.STRING_DEFAULT) ? getUUserId() : vo.getUUserId());
            this.setUValue(StringUtils.isNotBlank(vo.getUValue()) && vo.getUValue().equals(VO.STRING_DEFAULT) ? getUValue() : vo.getUValue());
            
            this.setMetaAccessRights(vo.getMetaAccessRights() != null ? new MetaAccessRights(vo.getMetaAccessRights()) : getMetaAccessRights()) ;
            
        }
        
    }
}
