/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.MetaFilterVO;
import com.resolve.services.hibernate.vo.MetaTableVO;
import com.resolve.services.hibernate.vo.MetaTableViewVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;


@Entity
@Table(name = "meta_table")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaTable extends BaseModel<MetaTableVO>
{
    private static final long serialVersionUID = -4903619214143515001L;
    
    private String UName;
    
    // object referenced by
    private Collection<MetaTableView> metaTableViews;
    private Collection<MetaFilter> metaFilters;
    
    public MetaTable()
    {
    }
    
    public MetaTable(MetaTableVO vo)
    {
        applyVOToModel(vo);
    }

    
    @Column(name = "u_name", nullable = false, length = 100)
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "metaTable")
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<MetaTableView> getMetaTableViews()
    {
        return this.metaTableViews;
    }

    public void setMetaTableViews(Collection<MetaTableView> metaTableViews)
    {
        this.metaTableViews = metaTableViews;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "metaTable")
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<MetaFilter> getMetaFilters()
    {
        return this.metaFilters;
    }

    public void setMetaFilters(Collection<MetaFilter> metaFilters)
    {
        this.metaFilters = metaFilters;
    }

    @Override
    public MetaTableVO doGetVO()
    {
        MetaTableVO vo = new MetaTableVO();
        super.doGetBaseVO(vo);
        
        vo.setUName(getUName());

        //refs
        Collection<MetaTableView> metaTableViews = Hibernate.isInitialized(this.metaTableViews) ? getMetaTableViews() : null;
        if(metaTableViews != null && metaTableViews.size() > 0)
        {
            Collection<MetaTableViewVO> vos = new ArrayList<MetaTableViewVO>();
            for(MetaTableView model : metaTableViews)
            {
                vos.add(model.doGetVO());
            }
            vo.setMetaTableViews(vos);
        }
        
        Collection<MetaFilter> metaFilters = Hibernate.isInitialized(this.metaFilters) ? getMetaFilters() : null;
        if(metaFilters != null && metaFilters.size() > 0)
        {
            Collection<MetaFilterVO> vos = new ArrayList<MetaFilterVO>();
            for(MetaFilter model : metaFilters)
            {
                vos.add(model.doGetVO());
            }
            vo.setMetaFilters(vos);
        }
        
        return vo;
    }

    @Override
    public void applyVOToModel(MetaTableVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);   
            
            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());

            //refs
            Collection<MetaTableViewVO> metaTableViewsVOs = vo.getMetaTableViews();
            if(metaTableViewsVOs != null && metaTableViewsVOs.size() > 0)
            {
                Collection<MetaTableView> models = new ArrayList<MetaTableView>();
                for(MetaTableViewVO refVO : metaTableViewsVOs)
                {
                    models.add(new MetaTableView(refVO));
                }
                this.setMetaTableViews(models);
            }
            
            
            Collection<MetaFilterVO> metaFiltersVOs = vo.getMetaFilters();
            if(metaFiltersVOs != null && metaFiltersVOs.size() > 0)
            {
                Collection<MetaFilter> models = new ArrayList<MetaFilter>();
                for(MetaFilterVO refVO : metaFiltersVOs)
                {
                    models.add(new MetaFilter(refVO));
                }
                this.setMetaFilters(models);
            }
            
        }

        
    }
}
