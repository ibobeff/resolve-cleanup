/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

// Generated Dec 11, 2009 11:15:20 AM by Hibernate Tools 3.2.4.GA

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * ResolveExecuteLog generated by hbm2java
 */
@Entity
@Table(name = "resolve_execute_log")
// @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveExecuteLog implements java.io.Serializable
{
    private static final long serialVersionUID = -2155821997351324925L;
    private String sys_id;
    private String UActiontask;
    private String UTrigger;
    private String UTarget;
    private String UAddress;

    // sys fields
    private String sysUpdatedBy;
    private Date sysUpdatedOn;
    private String sysCreatedBy;
    private Date sysCreatedOn;
    private Integer sysModCount;
	
	// transient
	
    // object references
	
    // object referenced by
	
    public ResolveExecuteLog()
    {
    }

    @Id
    @Column(name = "sys_id", nullable = false, length = 32)
    @GeneratedValue(generator = "hibernate-uuid")
    public String getsys_id()
    {
        return this.sys_id;
    }

    public void setsys_id(String sys_id)
    {
        this.sys_id = sys_id;
    }

    @Column(name = "u_actiontask", length = 100)
    public String getUActiontask()
    {
        return this.UActiontask;
    }

    public void setUActiontask(String UActiontask)
    {
        this.UActiontask = UActiontask;
    }

    @Column(name = "u_trigger", length = 100)
    public String getUTrigger()
    {
        return this.UTrigger;
    }

    public void setUTrigger(String UTrigger)
    {
        this.UTrigger = UTrigger;
    }

    @Column(name = "u_target", length = 32)
    public String getUTarget()
    {
        return this.UTarget;
    }

    public void setUTarget(String UTarget)
    {
        this.UTarget = UTarget;
    }

    @Column(name = "u_address", length = 100)
    public String getUAddress()
    {
        return this.UAddress;
    }

    public void setUAddress(String UAddress)
    {
        this.UAddress = UAddress;
    }

    @Column(name = "sys_updated_by", length = 40)
    public String getSysUpdatedBy()
    {
        return this.sysUpdatedBy;
    }

    public void setSysUpdatedBy(String sysUpdatedBy)
    {
        this.sysUpdatedBy = sysUpdatedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sys_updated_on", length = 19)
    public Date getSysUpdatedOn()
    {
        return this.sysUpdatedOn;
    }

    public void setSysUpdatedOn(Date sysUpdatedOn)
    {
        this.sysUpdatedOn = sysUpdatedOn;
    }

    @Column(name = "sys_created_by", length = 40)
    public String getSysCreatedBy()
    {
        return this.sysCreatedBy;
    }

    public void setSysCreatedBy(String sysCreatedBy)
    {
        this.sysCreatedBy = sysCreatedBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sys_created_on", length = 19)
    public Date getSysCreatedOn()
    {
        return this.sysCreatedOn;
    }

    public void setSysCreatedOn(Date sysCreatedOn)
    {
        this.sysCreatedOn = sysCreatedOn;
    }

    @Column(name = "sys_mod_count")
    public Integer getSysModCount()
    {
        return this.sysModCount;
    }

    public void setSysModCount(Integer sysModCount)
    {
        this.sysModCount = sysModCount;
    }

}
