/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.RBConnectionParamVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
@Entity
@Table(name = "rb_connection_param", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_connection_id" }, name = "rbcp_u_name_u_conn_id_uk") })
public class RBConnectionParam extends BaseModel<RBConnectionParamVO>
{
    private String name;
    private String displayName;
    private String dataType;
    private String defaultValue;
    private String source; //WSDATA,PARAM etc.
    private String sourceName; //Name to look in source

    private RBConnection connection;
    
    public RBConnectionParam()
    {
    }

    public RBConnectionParam(RBConnectionParamVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_name", length = 200)
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Column(name = "u_display_name", length = 512)
    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    @Column(name = "u_data_type", length = 40)
    public String getDataType()
    {
        return dataType;
    }

    public void setDataType(String dataType)
    {
        this.dataType = dataType;
    }

    @Column(name = "u_default_value", length = 255)
    public String getDefaultValue()
    {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue)
    {
        this.defaultValue = defaultValue;
    }

    @Column(name = "u_source", length = 40)
    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    @Column(name = "u_source_name", length = 40)
    public String getSourceName()
    {
        return sourceName;
    }

    public void setSourceName(String sourceName)
    {
        this.sourceName = sourceName;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_connection_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public RBConnection getConnection()
    {
        return this.connection;
    }

    public void setConnection(RBConnection connection)
    {
        this.connection = connection;
    }

    @Override
    public RBConnectionParamVO doGetVO()
    {
        RBConnectionParamVO vo = new RBConnectionParamVO();
        super.doGetBaseVO(vo);

        vo.setName(getName());
        vo.setDisplayName(getDisplayName());
        vo.setDataType(getDataType());
        vo.setDefaultValue(getDefaultValue());
        vo.setSource(getSource());
        vo.setSourceName(getSourceName());

        return vo;
    }

    @Override
    public void applyVOToModel(RBConnectionParamVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setName(StringUtils.isNotBlank(vo.getName()) && vo.getName().equals(VO.STRING_DEFAULT) ? getName() : vo.getName());
            this.setDisplayName(StringUtils.isNotBlank(vo.getDisplayName()) && vo.getDisplayName().equals(VO.STRING_DEFAULT) ? getDisplayName() : vo.getDisplayName());
            this.setDataType(StringUtils.isNotBlank(vo.getDataType()) && vo.getDataType().equals(VO.STRING_DEFAULT) ? getDataType() : vo.getDataType());
            this.setDefaultValue(StringUtils.isNotBlank(vo.getDefaultValue()) && vo.getDefaultValue().equals(VO.STRING_DEFAULT) ? getDefaultValue() : vo.getDefaultValue());
            this.setSource(StringUtils.isNotBlank(vo.getSource()) && vo.getSource().equals(VO.STRING_DEFAULT) ? getSource() : vo.getSource());
            this.setSourceName(StringUtils.isNotBlank(vo.getSourceName()) && vo.getSourceName().equals(VO.STRING_DEFAULT) ? getSourceName() : vo.getSourceName());
            
            //references
            this.setConnection(vo.getConnection() != null ? new RBConnection(vo.getConnection()) : null);
        }
    }
}
