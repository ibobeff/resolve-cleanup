package com.resolve.persistence.dao;

import com.resolve.persistence.model.MSGGatewayFilterAttr;

public interface MSGGatewayFilterAttrDAO extends GenericDAO<MSGGatewayFilterAttr, String> {

}
