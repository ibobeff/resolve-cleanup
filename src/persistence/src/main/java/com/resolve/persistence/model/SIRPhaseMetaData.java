package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

//import org.hibernate.annotations.Cache;
//import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.resolve.services.hibernate.vo.SIRPhaseMetaDataVO;
import com.resolve.services.interfaces.VO;
import static com.resolve.util.StringUtils.*;

@Entity
@Table(name = "sir_phase_metadata", 
       uniqueConstraints = {
                       @UniqueConstraint(columnNames = {"sys_org", "u_name"}, name = "spmd_u_org_name_uk")
       }, 
       indexes = {
                @Index(columnList = "sys_org, u_source", name = "spmd_org_source_idx")
       })
/*
 * Enable Hibernate L2 Cache only if it is distributed 
 * accross all nodes in cluster.
 */
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SIRPhaseMetaData extends BaseModel<SIRPhaseMetaDataVO> {
    
    public static final String CUSTOM_SIR_PHASE_SOURCE = "Custom";
    
    private static final long serialVersionUID = -4727077937107394093L;
    
    private String name;                // Phase Name
	private String source; 		        // Source of the phase. NIST/McAfee standards or Custom (per Org).
	                                    //
	                                    // Phase names will be unique only at the current Org level since 
	                                    // on Re-Org, the only phases Org has guaranteed access to are 
	                                    // phases belonging to itself.
	                                    //
	                                    // Cardinal (standard) phase names will be reserved i.e. it would not be 
	                                    // possible to create custom phases with same names as cardinal phases.
	                                    //
	                                    // System property "sir.phases.standards" will set standards used for cardinal
	                                    // phases by system. Default cardinal phases followed will be NIST in absence of
	                                    // the above system property.
	                                    //
	                                    // Currently only NIST and McAfee IR standards phases will be supported as cardinal
	                                    // phases.
	                                    //
	                                    // Cardinal phases are persisted (if not present) with cardinal phase ids.
	                                    //
	                                    // Cardinal Pahse Id = <Standards Name>:<Phase Name>
	                                    //                     If Cardinal Phase Name is > 32 characters then it is
	                                    //                     truncated to 29 characters and sufficed with "...".
	                                    // Example: NIST:Preparation
	
	public SIRPhaseMetaData() {
	    super();
	}
	
	public SIRPhaseMetaData(SIRPhaseMetaDataVO vo) {
        applyVOToModel(vo);
    }
	
	@Column(name = "u_name", length = 200, nullable = false, updatable = false)
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "u_source", length = 200)
	public String getSource() {
		return source;
	}
	
	public void setSource(String source) {
		this.source = source;
	}
	
    @Override
	public SIRPhaseMetaDataVO doGetVO() {
		SIRPhaseMetaDataVO vo = new SIRPhaseMetaDataVO();
		
		super.doGetBaseVO(vo);
				
		vo.setName(getName());
		vo.setSource(getSource());
		
		return vo;
	}
	
	@Override
	public void applyVOToModel(SIRPhaseMetaDataVO vo)
	{
		if (vo != null)
		{
			super.applyVOToModel(vo);
			
			setName(isNotBlank(vo.getName()) && vo.getName().equals(VO.STRING_DEFAULT) ? getName() : vo.getName());
			setSource(isNotBlank(vo.getSource()) && vo.getSource().equals(VO.STRING_DEFAULT) ? getSource() : vo.getSource());
		}
	}
	
	@Override
    public String toString() {
        return "SIR Phase Meta Data [" +
               (isNotBlank(getSys_id()) ? "Sys Id: " + getSys_id() + ", " : "") + 
               "Sys Org: " + (isNotBlank(getSysOrg()) ? getSysOrg() : "") + 
               ", Name: " + name + ", Source: " + source + "]";
    }
}
