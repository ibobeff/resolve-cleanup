package com.resolve.persistence.dao;

import com.resolve.persistence.model.PushGatewayFilterAttr;

public interface PushGatewayFilterAttrDAO extends GenericDAO<PushGatewayFilterAttr, String> {

}
