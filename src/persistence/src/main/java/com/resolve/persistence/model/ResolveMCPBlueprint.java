/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.resolve.services.hibernate.vo.ResolveMCPBlueprintVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "resolve_mcp_blueprint")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveMCPBlueprint extends BaseModel<ResolveMCPBlueprintVO>
{
    private static final long serialVersionUID = 2458768274760550378L;
    
    private String UBlueprint;
    private Integer UVersion;
    private String UMcpHostId;
    
    public ResolveMCPBlueprint()
    {
    }
    
    @Lob
    @Column (name = "u_blueprint", length = 16777215)
    public String getUBlueprint()
    {
        return UBlueprint;
    }
    public void setUBlueprint(String uBlueprint)
    {
        UBlueprint = uBlueprint;
    }
    
    @Column (name = "u_version")
    public Integer getUVersion()
    {
        return UVersion;
    }
    public void setUVersion(Integer uVersion)
    {
        UVersion = uVersion;
    }
    
    @Column (name = "u_mcp_host_id")
    public String getUMcpHostId()
    {
        return UMcpHostId;
    }
    public void setUMcpHostId(String uMcpHostId)
    {
        UMcpHostId = uMcpHostId;
    }
    
    @Override
    public ResolveMCPBlueprintVO doGetVO()
    {
        ResolveMCPBlueprintVO vo = new ResolveMCPBlueprintVO();
        super.doGetBaseVO(vo);
        
        vo.setUBlueprint(getUBlueprint());
        vo.setUVersion(getUVersion());
        vo.setUMcpHostId(getUMcpHostId());
        return vo;
    }
    
    public void applyVOToModel(ResolveMCPBlueprintVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUBlueprint(StringUtils.isNotBlank(vo.getUBlueprint()) && vo.getUBlueprint().equals(VO.STRING_DEFAULT) ? getUBlueprint() : vo.getUBlueprint());
            this.setUVersion(vo.getUVersion() != null && vo.getUVersion().equals(VO.INTEGER_DEFAULT) ? getUVersion() : vo.getUVersion());
            this.setUMcpHostId(StringUtils.isNotBlank(vo.getUMcpHostId()) && vo.getUMcpHostId().equals(VO.STRING_DEFAULT) ? getUMcpHostId() : vo.getUMcpHostId());
        }
    }
}
