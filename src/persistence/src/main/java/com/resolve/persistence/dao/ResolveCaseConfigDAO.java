package com.resolve.persistence.dao;

import com.resolve.persistence.model.ResolveCaseConfig;

public interface ResolveCaseConfigDAO extends GenericDAO<ResolveCaseConfig, String>
{

}
