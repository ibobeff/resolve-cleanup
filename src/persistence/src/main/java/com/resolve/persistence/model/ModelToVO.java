/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;


/**
 * A generic Interface for all the model object to implements its VO api
 *
 * @author jeet.marwah
 *
 */
public interface ModelToVO<T>
{
    public T doGetVO(boolean secured);
	public T doGetVO();
	public void applyVOToModel(T vo);
	public void resetFieldsWithDefaultValues();
}
 