/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListSet;

import org.hibernate.CallbackException;
import org.hibernate.EmptyInterceptor;
import org.hibernate.mapping.Column;
import org.hibernate.mapping.PersistentClass;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.Type;

import com.resolve.util.GMTDate;
import com.resolve.util.Log;

import groovy.lang.Binding;

import com.resolve.thread.TaskExecutor;
public class ResolveInterceptor extends EmptyInterceptor
{
    private static final long serialVersionUID = 1L;
    private String RESOLVE = "system";

	private static ConcurrentHashMap<String, SortedSet<BlockingSIDListener>> selectListeners = new ConcurrentHashMap<String, SortedSet<BlockingSIDListener>>();
	private static ConcurrentHashMap<String, ConcurrentLinkedQueue<SIDListener>> asyncSelectListeners = new ConcurrentHashMap<String, ConcurrentLinkedQueue<SIDListener>>();

	private static ConcurrentHashMap<String, SortedSet<BlockingSIDListener>> insertListeners = new ConcurrentHashMap<String, SortedSet<BlockingSIDListener>>();
    private static ConcurrentHashMap<String, SortedSet<BlockingSIDListener>> insertAfterListeners = new ConcurrentHashMap<String, SortedSet<BlockingSIDListener>>();
	private static ConcurrentHashMap<String, ConcurrentLinkedQueue<SIDListener>> asyncInsertListeners = new ConcurrentHashMap<String, ConcurrentLinkedQueue<SIDListener>>();

	private static ConcurrentHashMap<String, SortedSet<BlockingSIDListener>> deleteListeners = new ConcurrentHashMap<String, SortedSet<BlockingSIDListener>>();
	private static ConcurrentHashMap<String, ConcurrentLinkedQueue<SIDListener>> asyncDeleteListeners = new ConcurrentHashMap<String, ConcurrentLinkedQueue<SIDListener>>();

	private static ConcurrentHashMap<String, SortedSet<BlockingUListener>> updateListeners = new ConcurrentHashMap<String, SortedSet<BlockingUListener>>();
	private static ConcurrentHashMap<String, SortedSet<BlockingUListener>> updateAfterListeners = new ConcurrentHashMap<String, SortedSet<BlockingUListener>>();
	private static ConcurrentHashMap<String, ConcurrentLinkedQueue<UListener>> asyncUpdateListeners = new ConcurrentHashMap<String, ConcurrentLinkedQueue<UListener>>();
	

    
    public static int getAsyncUpdateListenerCount()
    {
        return asyncUpdateListeners.size();
    }
	
	
	private static final ThreadLocal < ConcurrentHashMap<Object, EntityState >> updateState  = 
        new ThreadLocal <ConcurrentHashMap<Object,EntityState>> () {
            @Override protected ConcurrentHashMap<Object,EntityState> initialValue() {
                return new ConcurrentHashMap<Object,EntityState>();
        }
    };

    private static final ThreadLocal < ConcurrentHashMap<Object, SIDEntityState >> insertState  = 
        new ThreadLocal <ConcurrentHashMap<Object,SIDEntityState>> () {
            @Override protected ConcurrentHashMap<Object,SIDEntityState> initialValue() {
                return new ConcurrentHashMap<Object,SIDEntityState>();
        }
    };

    public static final ThreadLocal < List<UListenerHelper >> updateAfterListenerQueue  = 
        new ThreadLocal <List<UListenerHelper>> () {
            @Override protected List<UListenerHelper> initialValue() {
                return new ArrayList<UListenerHelper>();
        }
    };
    
    public static final ThreadLocal < List<UListenerHelper >> asyncUpdateAfterListenerQueue  = 
        new ThreadLocal <List<UListenerHelper>> () {
            @Override protected List<UListenerHelper> initialValue() {
                return new ArrayList<UListenerHelper>();
        }
    };

    public static final ThreadLocal < List<SIDListenerHelper >> insertAfterListenerQueue  = 
        new ThreadLocal <List<SIDListenerHelper>> () {
            @Override protected List<SIDListenerHelper> initialValue() {
                return new ArrayList<SIDListenerHelper>();
        }
    };
    
    public static final ThreadLocal < List<SIDListenerHelper >> asyncInsertAfterListenerQueue  = 
        new ThreadLocal <List<SIDListenerHelper>> () {
            @Override protected List<SIDListenerHelper> initialValue() {
                return new ArrayList<SIDListenerHelper>();
        }
    };
    
//   public static void executeUpdateAfterListeners()
//   {
//       for (UListenerHelper u : updateAfterListenerQueue.get())
//       {
//           u.listener.onUpdate(u.entity, u.id, u.currentState, u.previousState, u.propertyNames, u.types);
//       }
//   }
//
//   public static void executeAsyncUpdateAfterListeners()
//   {
//       for (UListenerHelper u : asyncUpdateAfterListenerQueue.get())
//       {
//           SystemExecutor.execute(u, "runListener");
//       }
//   }
//   
   
//    private static final ThreadLocal < ConcurrentHashMap<Object, SIDEntityState >> deleteState  = 
//        new ThreadLocal <ConcurrentHashMap<Object,SIDEntityState>> () {
//            @Override protected ConcurrentHashMap<Object,SIDEntityState> initialValue() {
//                return new ConcurrentHashMap<Object,SIDEntityState>();
//        }
//    };
    
    
    public static void cleanUpdateState() 
    {
        updateState.get().clear();
        insertState.get().clear();
        updateAfterListenerQueue.get().clear();
        asyncUpdateAfterListenerQueue.get().clear();
        insertAfterListenerQueue.get().clear();
        asyncInsertAfterListenerQueue.get().clear();
        
        //deleteState.get().clear();
    }
	
	public static void addSelectListener(boolean isCustomTable, String entityClassName, BlockingSIDListener t)
	{
	    String table = isCustomTable ? CustomTableMappingUtil.class2TableTrueCase(entityClassName) : HibernateUtil.class2TableTrueCase(entityClassName);
        
        if (table == null)
        {
            throw new RuntimeException("Failed to add listener: couldn't find database " + (isCustomTable ? "custom" : "")+ " entity named " + entityClassName);
        }
		
		SortedSet<BlockingSIDListener> set = selectListeners.get(entityClassName);
		if (set == null)
		{
			selectListeners.put(entityClassName, set = new ConcurrentSkipListSet<BlockingSIDListener>());
		}
		set.add(t);
	}

	public static boolean isSelectListenerRegistred(String entityClassName, BlockingSIDListener t)
	{
		SortedSet<BlockingSIDListener> set = selectListeners.get(entityClassName);
		if (set == null)
		{
			return false;
		}
		else
		{
			return set.contains(t);
		}
	}

	public static boolean removeSelectListener(String entityClassName, BlockingSIDListener t)
	{
		SortedSet<BlockingSIDListener> set = selectListeners.get(entityClassName);
		if (set != null)
		{
			return set.remove(t);
		}
		else
		{
			return false;
		}
	}

	public static void addAsyncSelectListener(boolean isCustomTable, String entityClassName, SIDListener t)
	{
	    String table = isCustomTable ? CustomTableMappingUtil.class2TableTrueCase(entityClassName) : HibernateUtil.class2TableTrueCase(entityClassName);
        if (table==null)
        {
            throw new RuntimeException("Failed to add async listener: couldn't find database " + (isCustomTable ? "custom" : "")+ " entity named " + entityClassName);
        }
		
		ConcurrentLinkedQueue<SIDListener> queue = asyncSelectListeners.get(entityClassName);
		if (queue == null)
		{
			asyncSelectListeners.put(entityClassName, queue = new ConcurrentLinkedQueue<SIDListener>());
		}
		queue.add(t);
	}

	public static boolean isAsyncSelectListenerRegistered(String entityClassName, SIDListener t)
	{
		ConcurrentLinkedQueue<SIDListener> queue = asyncSelectListeners.get(entityClassName);
		if (queue == null)
		{
			return false;
		}
		else
		{
			return queue.contains(t);
		}
	}

	public static boolean removeAsyncSelectListener(String entityClassName, SIDListener t)
	{
		ConcurrentLinkedQueue<SIDListener> queue = asyncSelectListeners.get(entityClassName);
		if (queue != null)
		{
			return queue.remove(t);
		}
		else
		{
			return false;
		}
	}

	public static void addInsertListener(String entityClassName, BlockingSIDListener t)
	{
		String table = HibernateUtil.class2Table(entityClassName);
		if (table==null)
		{
			throw new RuntimeException("Failed to add listener: couldn't find database entity named " + entityClassName);
		}
		
		SortedSet<BlockingSIDListener> set = insertListeners.get(entityClassName);
		if (set == null)
		{
			insertListeners.put(entityClassName, set = new ConcurrentSkipListSet<BlockingSIDListener>());
		}
		set.add(t);
	}

    public static void addInsertAfterListener(String entityClassName, BlockingSIDListener t)
    {
		String tab = HibernateUtil.class2Table(entityClassName);
		if (tab==null)
		{
			throw new RuntimeException("Failed to add listener: couldn't find database entity named " + entityClassName);
		}
    	
		String table = HibernateUtil.class2Table(entityClassName);
		if (table==null)
		{
			throw new RuntimeException("Failed to add listener: couldn't find database entity named " + entityClassName);
		}
    	
        SortedSet<BlockingSIDListener> set = insertAfterListeners.get(entityClassName);
        if (set == null)
        {
            insertAfterListeners.put(entityClassName, set = new ConcurrentSkipListSet<BlockingSIDListener>());
        }
        set.add(t);
    }

	public static boolean isInsertListenerRegistred(String entityClassName, BlockingSIDListener t)
	{
		SortedSet<BlockingSIDListener> set = insertListeners.get(entityClassName);
		if (set == null)
		{
			return false;
		}
		else
		{
			return set.contains(t);
		}
	}

    public static boolean isInsertAfterListenerRegistred(String entityClassName, BlockingSIDListener t)
    {
        SortedSet<BlockingSIDListener> set = insertAfterListeners.get(entityClassName);
        if (set == null)
        {
            return false;
        }
        else
        {
            return set.contains(t);
        }
    }
	
	public static boolean removeInsertListener(String entityClassName, BlockingSIDListener t)
	{
		SortedSet<BlockingSIDListener> set = insertListeners.get(entityClassName);
		if (set != null)
		{
			return set.remove(t);
		}
		else
		{
			return false;
		}
	}

    public static boolean removeInsertAfterListener(String entityClassName, BlockingSIDListener t)
    {
        SortedSet<BlockingSIDListener> set = insertAfterListeners.get(entityClassName);
        if (set != null)
        {
            return set.remove(t);
        }
        else
        {
            return false;
        }
    }
    
	public static void addAsyncInsertListener(String entityClassName, SIDListener t)
	{
		String table = HibernateUtil.class2Table(entityClassName);
		if (table==null)
		{
			throw new RuntimeException("Failed to add listener: couldn't find database entity named " + entityClassName);
		}
		
		ConcurrentLinkedQueue<SIDListener> queue = asyncInsertListeners.get(entityClassName);
		if (queue == null)
		{
			asyncInsertListeners.put(entityClassName, queue = new ConcurrentLinkedQueue<SIDListener>());
		}
		queue.add(t);
	}

	public static boolean isAsyncInsertListenerRegistered(String entityClassName, SIDListener t)
	{
		ConcurrentLinkedQueue<SIDListener> queue = asyncInsertListeners.get(entityClassName);
		if (queue == null)
		{
			return false;
		}
		else
		{
			return queue.contains(t);
		}
	}

	public static boolean removeAsyncInsertListener(String entityClassName, SIDListener t)
	{
		ConcurrentLinkedQueue<SIDListener> queue = asyncInsertListeners.get(entityClassName);
		if (queue != null)
		{
			return queue.remove(t);
		}
		else
		{
			return false;
		}
	}

	public static void addDeleteListener(String entityClassName, BlockingSIDListener t)
	{
		String table = HibernateUtil.class2Table(entityClassName);
		if (table==null)
		{
			throw new RuntimeException("Failed to add listener: couldn't find database entity named " + entityClassName);
		}
		
		SortedSet<BlockingSIDListener> set = deleteListeners.get(entityClassName);
		if (set == null)
		{
			deleteListeners.put(entityClassName, set = new ConcurrentSkipListSet<BlockingSIDListener>());
		}
		set.add(t);
	}

//    public static void addDeleteAfterListener(String entityClassName, BlockingSIDListener t)
//    {
//        SortedSet<BlockingSIDListener> set = deleteAfterListeners.get(entityClassName);
//        if (set == null)
//        {
//            deleteAfterListeners.put(entityClassName, set = new ConcurrentSkipListSet<BlockingSIDListener>());
//        }
//        set.add(t);
//    }

	public static boolean isDeleteListenerRegistred(String entityClassName, BlockingSIDListener t)
	{
		SortedSet<BlockingSIDListener> set = deleteListeners.get(entityClassName);
		if (set == null)
		{
			return false;
		}
		else
		{
			return set.contains(t);
		}
	}

//    public static boolean isDeleteAfterListenerRegistred(String entityClassName, BlockingSIDListener t)
//    {
//        SortedSet<BlockingSIDListener> set = deleteAfterListeners.get(entityClassName);
//        if (set == null)
//        {
//            return false;
//        }
//        else
//        {
//            return set.contains(t);
//        }
//    }
    
	public static boolean removeDeleteListener(String entityClassName, BlockingSIDListener t)
	{
		SortedSet<BlockingSIDListener> set = deleteListeners.get(entityClassName);
		if (set != null)
		{
			return set.remove(t);
		}
		else
		{
			return false;
		}
	}

//    public static boolean removeDeleteAfterListener(String entityClassName, BlockingSIDListener t)
//    {
//        SortedSet<BlockingSIDListener> set = deleteAfterListeners.get(entityClassName);
//        if (set != null)
//        {
//            return set.remove(t);
//        }
//        else
//        {
//            return false;
//        }
//    }
    
	public static void addAsyncDeleteListener(String entityClassName, SIDListener t)
	{
		String table = HibernateUtil.class2Table(entityClassName);
		if (table==null)
		{
			throw new RuntimeException("Failed to add listener: couldn't find database entity named " + entityClassName);
		}

		ConcurrentLinkedQueue<SIDListener> queue = asyncDeleteListeners.get(entityClassName);
		if (queue == null)
		{
			asyncDeleteListeners.put(entityClassName, queue = new ConcurrentLinkedQueue<SIDListener>());
		}
		queue.add(t);
	}

	public static boolean isAsyncDeleteListenerRegistered(String entityClassName, SIDListener t)
	{
		ConcurrentLinkedQueue<SIDListener> queue = asyncDeleteListeners.get(entityClassName);
		if (queue == null)
		{
			return false;
		}
		else
		{
			return queue.contains(t);
		}
	}

	public static boolean removeAsyncDeleteListener(String entityClassName, SIDListener t)
	{
		ConcurrentLinkedQueue<SIDListener> queue = asyncDeleteListeners.get(entityClassName);
		if (queue != null)
		{
			return queue.remove(t);
		}
		else
		{
			return false;
		}
	}

	public static void addUpdateListener(String entityClassName, BlockingUListener t)
	{
		String table = HibernateUtil.class2Table(entityClassName);
		if (table==null)
		{
			throw new RuntimeException("Failed to add listener: couldn't find database entity named " + entityClassName);
		}
		
		SortedSet<BlockingUListener> set = updateListeners.get(entityClassName);
		if (set == null)
		{
			updateListeners.put(entityClassName, set = new ConcurrentSkipListSet<BlockingUListener>());
		}
		set.add(t);
	}

    public static void addUpdateAfterListener(String entityClassName, BlockingUListener t)
    {
		String table = HibernateUtil.class2Table(entityClassName);
		if (table==null)
		{
			throw new RuntimeException("Failed to add listener: couldn't find database entity named " + entityClassName);
		}

        SortedSet<BlockingUListener> set = updateAfterListeners.get(entityClassName);
        if (set == null)
        {
            updateAfterListeners.put(entityClassName, set = new ConcurrentSkipListSet<BlockingUListener>());
        }
        set.add(t);
    }

	public static boolean isUpdateListenerRegistred(String entityClassName, BlockingUListener t)
	{
		SortedSet<BlockingUListener> set = updateListeners.get(entityClassName);
		if (set == null)
		{
			return false;
		}
		else
		{
			return set.contains(t);
		}
	}

    public static boolean isUpdateAfterListenerRegistred(String entityClassName, BlockingUListener t)
    {
        SortedSet<BlockingUListener> set = updateAfterListeners.get(entityClassName);
        if (set == null)
        {
            return false;
        }
        else
        {
            return set.contains(t);
        }
    }

    public static boolean removeUpdateListener(String entityClassName, BlockingUListener t)
	{
		SortedSet<BlockingUListener> set = updateListeners.get(entityClassName);
		if (set != null)
		{
			return set.remove(t);
		}
		else
		{
			return false;
		}
	}

    public static boolean removeUpdateAfterListener(String entityClassName, BlockingUListener t)
    {
        SortedSet<BlockingUListener> set = updateAfterListeners.get(entityClassName);
        if (set != null)
        {
            return set.remove(t);
        }
        else
        {
            return false;
        }
    }
    
	public static int getAsyncUpdateListenerCount(String entityClassName)
	{
	    return asyncUpdateListeners.get(entityClassName).size();
	}
	
	public static void addAsyncUpdateListener(String entityClassName, UListener t)
	{
    	String table = HibernateUtil.class2Table(entityClassName);
		if (table==null)
		{
			throw new RuntimeException("Failed to add listener: couldn't find database entity named " + entityClassName);
		}
    	
		ConcurrentLinkedQueue<UListener> queue = asyncUpdateListeners.get(entityClassName);
		if (queue == null)
		{
			asyncUpdateListeners.put(entityClassName, queue = new ConcurrentLinkedQueue<UListener>());
		}
		queue.add(t);
	}

	public static boolean isAsyncUpdateListenerRegistered(String entityClassName, UListener t)
	{
		ConcurrentLinkedQueue<UListener> queue = asyncUpdateListeners.get(entityClassName);
		if (queue == null)
		{
			return false;
		}
		else
		{
			return queue.contains(t);
		}
	}

	public static boolean removeAsyncUpdateListener(String entityClassName, UListener t)
	{
		ConcurrentLinkedQueue<UListener> queue = asyncUpdateListeners.get(entityClassName);
		if (queue != null)
		{
			return queue.remove(t);
		}
		else
		{
			return false;
		}
	}

	private String getUserName()
	{
		HibernateUtil.User contextUser = HibernateUtil.getCurrentUser();
		if (contextUser != null)
		{
			return contextUser.name;
		}
		else
		{
			return RESOLVE;
		}
	}

	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) throws CallbackException

    {
        // Log.log.debug("onSave() on entity " + entity.getClass().getName());
        boolean changed = false;
        if (!ExportUtil.isFromImport.get())
        {
            for (int i = 0; i < state.length; ++i)
            {
                // Log.log.debug(propertyNames[i] + "=" + state[i]);
                if (state[i] != null) continue;
                if ("sysUpdatedBy".equals(propertyNames[i]) || "sys_updated_by".equals(propertyNames[i]))
                {
                    state[i] = getUserName();
                    changed = true;
                }
                else if ("sysUpdatedOn".equals(propertyNames[i]) || "sys_updated_on".equals(propertyNames[i]))
                {
                    state[i] = GMTDate.getDate();// new Date();
                    changed = true;
                }
                else if ("sysCreatedBy".equals(propertyNames[i]) || "sys_created_by".equals(propertyNames[i]))
                {
                    state[i] = getUserName();
                    changed = true;
                }
                else if ("sysCreatedOn".equals(propertyNames[i]) || "sys_created_on".equals(propertyNames[i]))
                {
                    state[i] = GMTDate.getDate(); // new Date();
                    changed = true;
                }
            }
        }

        String cls = getClassName(entity).cls;

        // Iterate internal listeners
        SortedSet<BlockingSIDListener> listeners = insertListeners.get(cls);
        if (listeners != null)
        {
            for (BlockingSIDListener l : listeners)
            {
                l.onDataOperation(entity, id, state, propertyNames, types);
            }
        }
        
        // Handle insert after listeners
        if ((insertAfterListeners.containsKey(cls) && !insertAfterListeners.get(cls).isEmpty())
                        || (asyncInsertListeners.containsKey(cls) && !asyncInsertListeners.get(cls).isEmpty()))
        {
            SIDEntityState sid = new SIDEntityState(entity, id, state, propertyNames, types);
            insertState.get().put(id.toString(), sid);
        }

        return changed;
    }

	@SuppressWarnings("rawtypes")
    public Map<String, Object> getTableRow(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types,
			Map<String, String> property2Column)
	{
		Map<String, Object> result = new HashMap<String, Object>();
		String entityName = entity.getClass().getName();
		PersistentClass ps = HibernateUtil.getClassMapping(entityName);
        if (ps!=null)
        {
            int i = 0;
            for (String propertyName : propertyNames)
            {
                org.hibernate.mapping.Property p = ps.getProperty(propertyName);
                Iterator cit = p.getColumnIterator();
                while (cit.hasNext())
                {
                    Column c = (Column) cit.next();
                    if (state!=null)
                    {
                        result.put(c.getName(), state[i]);
                    }
                    property2Column.put(propertyName, c.getName());
                    break;
                }
                i++;
            }
        }
        else
        {
            int i = 0;
            for (String propertyName : propertyNames)
            {
                result.put(propertyName, state[i]);
                property2Column.put(propertyName, propertyName);
                i++;
            }
        }
        
        result.put("sys_id", id);
		return result;
	}

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void postFlush(Iterator entities) throws CallbackException
    {

        if (updateState.get().isEmpty() && insertState.get().isEmpty()) 
            return;
        
//        if (updateState.get().isEmpty() && insertState.get().isEmpty() && deleteState.get().isEmpty()) 
//            return;
//
//        ArrayList<Object> afterObjs = new ArrayList<Object>();
//        for (Object key : updateState.get().keySet())
//        {
//            afterObjs.add(updateState.get().get(key).entity);
//        }
//
//        for (Object key : insertState.get().keySet())
//        {
//            afterObjs.add(insertState.get().get(key).entity);
//        }
//
//        for (Object key : deleteState.get().keySet())
//        {
//            afterObjs.add(deleteState.get().get(key).entity);
//        }
//
//        entities = afterObjs.iterator();
        
        while (entities.hasNext())
        {
            Object entity = entities.next();
            String sys_id = null;
            try
            {
                sys_id = ExportUtil.retrievesys_id(entity);
            }
            catch (Exception ex)
            {
                Log.log.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }

            if (sys_id != null)
            {
                if (updateState.get().get(sys_id) == null && insertState.get().get(sys_id) == null) 
                {
                    continue;
                }
            }
            else
            {
                throw new RuntimeException("Entity sys_id is null !");
            }

            ClassNameResult clsResult = getClassName(entity);
            if (updateState.get().get(sys_id) != null)
            {
                EntityState state = null;
                if (sys_id != null)
                {
                    state = updateState.get().get(sys_id);
                }

                String cls = clsResult.cls;
                if (clsResult.isEntity)
                {
                    try
                    {
                        state = EntityState.normalize(state);

                        // Normalize entity itself
                        Map<String, Object> newEntity = new HashMap<String, Object>();
                        Map<String, Object> oldEntity = (HashMap<String, Object>) entity;
                        for (String key : oldEntity.keySet())
                        {
                            if (!"$type$".equals(key))
                            {
                                newEntity.put(key, oldEntity.get(key));
                            }
                        }
                        entity = newEntity;
                    }
                    catch (Exception ex)
                    {
                        Log.log.error(ex.getMessage(), ex);
                        throw new RuntimeException(ex);
                    }
                }

                // Iterate internal listeners
                SortedSet<BlockingUListener> listeners = updateAfterListeners.get(cls);
                if (listeners != null)
                {
                    for (BlockingUListener l : listeners)
                    {
                        UListenerHelper helper = new UListenerHelper(entity, state.id, state.currentState, state.previousState, state.propertyNames,
                                        state.types, l);
                        updateAfterListenerQueue.get().add(helper);
                        l.onUpdate(entity, state.id, state.currentState, state.previousState, state.propertyNames, state.types);
                    }
                }

                ConcurrentLinkedQueue<UListener> alisteners = asyncUpdateListeners.get(cls);
                if (alisteners != null)
                {
                    for (UListener s : alisteners)
                    {
                        UListenerHelper helper = new UListenerHelper(entity, state.id, state.currentState, state.previousState, state.propertyNames,
                                        state.types, s);
                        asyncUpdateAfterListenerQueue.get().add(helper);
                        //SystemExecutor.execute(helper, "runListener");
                    }
                }
            }
            else if (insertState.get().get(sys_id) != null)
            {
                String cls = clsResult.cls;
                SIDEntityState state = null;
                state = insertState.get().get(sys_id);

                // Iterate internal listeners
                SortedSet<BlockingSIDListener> listeners = insertAfterListeners.get(cls);
                if (listeners != null)
                {
                    for (BlockingSIDListener l : listeners)
                    {
                        SIDListenerHelper helper = new SIDListenerHelper(entity, state.id, state.state, state.propertyNames, state.types, l);
                        insertAfterListenerQueue.get().add(helper);
                        l.onDataOperation(entity, state.id, state.state, state.propertyNames, state.types);
                    }
                }

                ConcurrentLinkedQueue<SIDListener> alisteners = asyncInsertListeners.get(cls);
                if (alisteners != null)
                {
                    for (SIDListener s : alisteners)
                    {
                        SIDListenerHelper helper = new SIDListenerHelper(entity, state.id, state.state, state.propertyNames, state.types, s);
                        asyncInsertAfterListenerQueue.get().add(helper);
                        //SystemExecutor.execute(helper, "runListener");
                    }
                }
            }
        }

    }

    private static class SIDEntityState
    {
        
        Object entity;
        Serializable id;
        Object[] state;
        String[] propertyNames;
        Type[] types;        
        
        SIDEntityState(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types)
        {
            this.entity=entity;
            this.id=id;
            this.state=state;
            this.propertyNames=propertyNames;
            this.types=types;           
        }
    }
    
	private static class EntityState
	{
	    Object entity;
	    Serializable id;
	    Object[] currentState;
	    Object[] previousState;
	    String[] propertyNames;
	    Type[] types;	  
	    
	    EntityState(Object entity, Serializable id, Object[] currentState, Object[] previousState, String[] propertyNames, Type[] types)
	    {
	        this.entity=entity;
	        this.id=id;
	        this.currentState=currentState;
	        this.previousState=previousState;
	        this.propertyNames=propertyNames;
	        this.types=types;	        
	    }
	    
	    private static EntityState normalize(EntityState state)
	    {
            ArrayList<Object> currents = new ArrayList<Object>();
            ArrayList<Object> previous = new ArrayList<Object>();
            ArrayList<Type>  types = new ArrayList<Type>();
            ArrayList<String>  names = new ArrayList<String>();
	        
	        for (int i = 0; i<state.propertyNames.length; i++)
	        {
	            String p = state.propertyNames[i];
	            if (!"$type$".equals(p))
	            {
	                currents.add(state.currentState[i]);
	                previous.add(state.previousState[i]);
	                types.add(state.types[i]);
	                names.add(state.propertyNames[i]);
	            }
	        }
	        
	        return new EntityState(state.entity, state.id, currents.toArray(), previous.toArray(), names.toArray(new String[]{}), types.toArray(new Type[]{}));
	    }
	}
	
	private static class ClassNameResult
	{
	    String cls;
	    boolean isEntity;
	    
	    ClassNameResult()
	    {
	        
	    }
	    
	    @SuppressWarnings("unused")
        ClassNameResult(String name, boolean b)
	    {
	        cls = name;
	        isEntity = b;
	    }
	}
	
    private ClassNameResult getClassName(final Object entity)
    {
        final ClassNameResult result = new ClassNameResult();
        if (!(entity instanceof java.util.Map))
        {
            result.cls = entity.getClass().getName();
            result.isEntity = false;
            return result;
        }
        
        HibernateUtil.invokeInTransaction(new Callable<Void>()
        {
            public Void call() throws Exception
            {
                result.cls = HibernateUtil.getCurrentSession().getEntityName(entity);
                PersistentClass p = HibernateUtil.getClassMapping(result.cls);
                if (p==null)
                {
                    result.isEntity = false;
                }
                else
                {
                    result.isEntity = (p.getClassName()==null);
                }
                return null;
            }
        });
        return result;

        //
        //
        // String cls = entity.getClass().getName();
        // boolean isEntity = false;
        // if ("java.util.HashMap".equals(cls))
        // {
        // try
        // {
        // cls = ExportUtil.retrieveEntityProperty(entity, "$type$").toString();
        // isEntity = true;
        // }
        // catch (Exception ex)
        // {
        // Log.log.error(ex.getMessage(), ex);
        // throw new RuntimeException(ex);
        // }
        // }
        // return new ClassNameResult(cls, isEntity);
    }
	
    public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState, String[] propertyNames, Type[] types)
                    throws CallbackException
    {
        boolean changed = false;
        if (!ExportUtil.isFromImport.get())
        {
            for (int i = 0; i < currentState.length; ++i)
            {
                // Log.log.debug(propertyNames[i] + "=" + currentState[i]);
                if ("sysUpdatedBy".equals(propertyNames[i]) || "sys_updated_by".equals(propertyNames[i]))
                {
                    currentState[i] = getUserName();
                    changed = true;
                }
                else if ("sysUpdatedOn".equals(propertyNames[i]) || "sys_updated_on".equals(propertyNames[i]))
                {
                    currentState[i] = GMTDate.getDate(); // new Date();
                    changed = true;
                }
            }
        }

        ClassNameResult clsResult = getClassName(entity);
        String cls = clsResult.cls;
        // Iterate internal listeners
        SortedSet<BlockingUListener> listeners = updateListeners.get(cls);
        if (listeners != null)
        {
            for (BlockingUListener l : listeners)
            {
                l.onUpdate(entity, id, currentState, previousState, propertyNames, types);
            }
        }
        
		if ((updateAfterListeners.containsKey(cls) && !updateAfterListeners.get(cls).isEmpty())
				|| (asyncUpdateListeners.containsKey(cls) && !asyncUpdateListeners.get(cls).isEmpty())) {
			
			EntityState state = new EntityState(entity, id, currentState, previousState, propertyNames, types);
			updateState.get().put(id.toString(), state);
		}

        return changed;
    }

	public static class SIDListenerHelper
	{
		Object entity;
		Serializable id;
		Object[] state;
		String[] propertyNames;
		Type[] types;
		SIDListener listener;

		SIDListenerHelper(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types, SIDListener listener)
		{
			this.entity = entity;
			this.id = id;
			this.state = state;
			this.propertyNames = propertyNames;
			this.types = types;
			this.listener = listener;
		}

		public void runListener()
		{
			this.listener.onDataOperation(entity, id, state, propertyNames, types);
		}
	}

	public static class UListenerHelper
	{
		Object entity;
		Serializable id;
		Object[] previousState;
		Object[] currentState;
		String[] propertyNames;
		Type[] types;
		UListener listener;

		UListenerHelper(Object entity, Serializable id, Object[] currentState, Object[] previousState, String[] propertyNames, Type[] types, UListener listener)
		{
			this.entity = entity;
			this.id = id;
			this.currentState = currentState;
			this.previousState = previousState;
			this.propertyNames = propertyNames;
			this.types = types;
			this.listener = listener;
		}

		public void runListener()
		{
			this.listener.onUpdate(entity, id, currentState, previousState, propertyNames, types);
		}
	}

    public boolean onLoad(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) throws CallbackException
    {
        // Log.log.debug("onLoad() on entity " + entity.getClass().getName());
        boolean changed = false;
        for (int i = 0; i < state.length; ++i)
        {
            // Log.log.debug(propertyNames[i] + "=" + state[i]);
            if (state[i] != null) continue;
            if (types[i] instanceof IntegerType)
            {
                state[i] = new Integer(0);
                changed = true;
            }
            else if (types[i] instanceof LongType)
            {
                state[i] = new Long(0);
                changed = true;
            }
        }

        String cls = getClassName(entity).cls;

        // Iterate internal listeners
        SortedSet<BlockingSIDListener> listeners = selectListeners.get(cls);
        if (listeners != null)
        {
            for (BlockingSIDListener l : listeners)
            {
                l.onDataOperation(entity, id, state, propertyNames, types);
            }
        }
        ConcurrentLinkedQueue<SIDListener> alisteners = asyncSelectListeners.get(cls);
        if (alisteners != null)
        {
            for (SIDListener s : alisteners)
            {
                SIDListenerHelper helper = new SIDListenerHelper(entity, id, state, propertyNames, types, s);
                TaskExecutor.execute(helper, "runListener");
            }
        }

        return changed;
    }

    public void onDelete(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types)
    {
        String cls = getClassName(entity).cls;

        // Iterate internal listeners
        SortedSet<BlockingSIDListener> listeners = deleteListeners.get(cls);
        if (listeners != null)
        {
            for (BlockingSIDListener l : listeners)
            {
                l.onDataOperation(entity, id, state, propertyNames, types);
            }
        }
        ConcurrentLinkedQueue<SIDListener> alisteners = asyncDeleteListeners.get(cls);
        if (alisteners != null)
        {
            for (SIDListener s : alisteners)
            {
                SIDListenerHelper helper = new SIDListenerHelper(entity, id, state, propertyNames, types, s);
                TaskExecutor.execute(helper, "runListener");
            }
        }

    }
}
