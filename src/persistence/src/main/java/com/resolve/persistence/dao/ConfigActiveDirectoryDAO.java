/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.dao;

import com.resolve.persistence.model.ConfigActiveDirectory;

/**
 * 
 * @author jeet.marwah
 *
 */
public interface ConfigActiveDirectoryDAO extends GenericDAO<ConfigActiveDirectory, String>
{

} // ConfigActiveDirectoryDAO
