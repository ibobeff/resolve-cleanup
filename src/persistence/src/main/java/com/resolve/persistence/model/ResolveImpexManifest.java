/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.resolve.services.hibernate.vo.ResolveImpexManifestVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "resolve_impex_manifest")
public class ResolveImpexManifest extends BaseModel<ResolveImpexManifestVO>
{
    private static final long serialVersionUID = -3972144617291943129L;
    
    private String UName;
    private String UModule;
    private String UFullName;
    private String UDisplayType;
    private String UCompSysId;
    private String UTableName;
    private String UFileName;
    private String UStatus;
    private String UOperation; // DELETE OR INSERT_OR_UPDATE
    private String UOperationType; // IMPORT or EXPORT
    private Boolean UInclude;
    private String UModuleName;
    
    @Column(name = "u_name", length = 333)
    public String getUName()
    {
        return UName;
    }
    public void setUName(String uName)
    {
        UName = uName;
    }
    
    @Column(name = "u_module", length = 333)
    public String getUModule()
    {
        return UModule;
    }
    public void setUModule(String uModule)
    {
        UModule = uModule;
    }
    
    @Column(name = "u_fullname", length = 333)
    public String getUFullName()
    {
        return UFullName;
    }
    public void setUFullName(String uFullName)
    {
        UFullName = uFullName;
    }
    
    @Column(name = "u_display_type", length = 333)
    public String getUDisplayType()
    {
        return UDisplayType;
    }
    public void setUDisplayType(String uDisplayType)
    {
        UDisplayType = uDisplayType;
    }
    
    @Column(name = "u_comp_sys_id", length = 32)
    public String getUCompSysId()
    {
        return UCompSysId;
    }
    public void setUCompSysId(String uCompSysId)
    {
        UCompSysId = uCompSysId;
    }
    
    @Column(name = "u_table_name", length = 333)
    public String getUTableName()
    {
        return UTableName;
    }
    public void setUTableName(String uTableName)
    {
        UTableName = uTableName;
    }
    
    @Column(name = "u_file_name", length = 333)
    public String getUFileName()
    {
        return UFileName;
    }
    public void setUFileName(String uFileName)
    {
        UFileName = uFileName;
    }
    
    @Column(name = "u_status", length = 333)
    public String getUStatus()
    {
        return UStatus;
    }
    public void setUStatus(String uStatus)
    {
        this.UStatus = uStatus;
    }
    
    @Column(name = "u_operation", length = 333)
    public String getUOperation()
    {
        return UOperation;
    }
    public void setUOperation(String uOperation)
    {
        UOperation = uOperation;
    }
    
    @Column(name = "u_operation_type", length = 32)
    public String getUOperationType()
    {
        return UOperationType;
    }
    public void setUOperationType(String uOperationType)
    {
        UOperationType = uOperationType;
    }
    
    @Column(name = "u_include")
    public Boolean getUInclude()
    {
        return UInclude;
    }
    public void setUInclude(Boolean uInclude)
    {
        UInclude = uInclude;
    }
    
    @Column(name = "u_module_name", length = 333)
    public String getUModuleName()
    {
        return UModuleName;
    }
    public void setUModuleName(String uModuleName)
    {
        UModuleName = uModuleName;
    }
    @Override
    public ResolveImpexManifestVO doGetVO()
    {
        ResolveImpexManifestVO vo = new ResolveImpexManifestVO();
        super.doGetBaseVO(vo);
        
        vo.setUName(getUName());
        vo.setUModule(getUModule());
        vo.setUFullName(getUFullName());
        vo.setUDisplayType(getUDisplayType());
        vo.setUCompSysId(getUCompSysId());
        vo.setUTableName(getUTableName());
        vo.setUFileName(getUFileName());
        vo.setUStatus(getUStatus());
        vo.setUOperation(getUOperation());
        vo.setUOperationType(getUOperationType());
        vo.setUInclude(getUInclude());
        vo.setUModuleName(getUModuleName());
        
        return vo;
    }
    
    @Override
    public void applyVOToModel(ResolveImpexManifestVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUModule(StringUtils.isNotBlank(vo.getUModule()) && vo.getUModule().equals(VO.STRING_DEFAULT) ? getUModule() : vo.getUModule());
            this.setUFullName(StringUtils.isNotBlank(vo.getUFullName()) && vo.getUFullName().equals(VO.STRING_DEFAULT) ? getUFullName() : vo.getUFullName());
            this.setUDisplayType(StringUtils.isNotBlank(vo.getUDisplayType()) && vo.getUDisplayType().equals(VO.STRING_DEFAULT) ? getUDisplayType() : vo.getUDisplayType());
            this.setUCompSysId(StringUtils.isNotBlank(vo.getUCompSysId()) && vo.getUCompSysId().equals(VO.STRING_DEFAULT) ? getUCompSysId() : vo.getUCompSysId());
            this.setUTableName(StringUtils.isNotBlank(vo.getUTableName()) && vo.getUTableName().equals(VO.STRING_DEFAULT) ? getUTableName() : vo.getUTableName());
            this.setUFileName(StringUtils.isNotBlank(vo.getUFileName()) && vo.getUFileName().equals(VO.STRING_DEFAULT) ? getUFileName() : vo.getUFileName());
            this.setUStatus(StringUtils.isNotBlank(vo.getUStatus()) && vo.getUStatus().equals(VO.STRING_DEFAULT) ? getUStatus() : vo.getUStatus());
            this.setUOperation(StringUtils.isNotBlank(vo.getUOperation()) && vo.getUOperation().equals(VO.STRING_DEFAULT) ? getUOperation() : vo.getUOperation());
            this.setUOperationType(StringUtils.isNotBlank(vo.getUOperationType()) && vo.getUOperationType().equals(VO.STRING_DEFAULT) ? getUOperationType() : vo.getUOperationType());
            this.setUInclude(vo.getUInclude() != null ? vo.getUInclude() : getUInclude());
            this.setUModuleName(StringUtils.isNotBlank(vo.getUModuleName()) && vo.getUModuleName().equals(VO.STRING_DEFAULT) ? getUModuleName() : vo.getUModuleName());
        }
    }
}
