/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolveEdgePropertiesVO;
import com.resolve.services.hibernate.vo.ResolveEdgeVO;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.interfaces.VO;

@Entity
@Table(name = "resolve_edge", indexes = {
                @Index(columnList = "u_rel_type", name = "re_u_rel_type_idx"),
                @Index(columnList = "u_src_sys_id", name = "re_u_src_node_idx"),
                @Index(columnList = "u_dest_sys_id", name = "re_u_dest_node_idx"),
                @Index(columnList = "u_src_sys_id, u_rel_type", name = "re_src_rel_idx"),
                @Index(columnList = "u_dest_sys_id, u_rel_type", name = "re_dest_rel_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveEdge extends BaseModel<ResolveEdgeVO>
{
    private static final long serialVersionUID = 3639875907274467515L;
    
    private ResolveNode sourceNode;
    private ResolveNode destinationNode;
    private Integer UOrder;
    private String URelType;
    
    private Collection<ResolveEdgeProperties> properties;

    public ResolveEdge()
    {
    }
    
    public ResolveEdge(String sysId)
    {
        this.setSys_id(sysId);
    }

    public ResolveEdge(ResolveEdgeVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_order")
    public Integer getUOrder()
    {
        return UOrder;
    }

    public void setUOrder(Integer uOrder)
    {
        UOrder = uOrder;
    }

    @Column(name = "u_rel_type")
    public String getURelType()
    {
        return URelType;
    }

    public void setURelType(String uRelType)
    {
        URelType = uRelType;
    }
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_src_sys_id", referencedColumnName = "sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveNode getSourceNode()
    {
        return sourceNode;
    }

    public void setSourceNode(ResolveNode sourceNode)
    {
        this.sourceNode = sourceNode;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_dest_sys_id", referencedColumnName = "sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveNode getDestinationNode()
    {
        return destinationNode;
    }

    public void setDestinationNode(ResolveNode destinationNode)
    {
        this.destinationNode = destinationNode;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "edge", fetch = FetchType.EAGER)
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.DELETE)
    public Collection<ResolveEdgeProperties> getProperties()
    {
        return properties;
    }

    public void setProperties(Collection<ResolveEdgeProperties> properties)
    {
        this.properties = properties;
    }

    
    @Override
    public ResolveEdgeVO doGetVO()
    {
        ResolveEdgeVO vo = new ResolveEdgeVO();
        super.doGetBaseVO(vo);
        
        vo.setUOrder(this.getUOrder());
        vo.setURelType(this.getURelType());
        
        //populate the sysIds only to avoid cyclic issues
        vo.setSourceNode(Hibernate.isInitialized(this.sourceNode) && getSourceNode() != null ? new ResolveNodeVO(this.getSourceNode().getSys_id()): null);
        vo.setDestinationNode(Hibernate.isInitialized(this.destinationNode) && getDestinationNode() != null ? new ResolveNodeVO(this.getDestinationNode().getSys_id()): null);
        
        //get the properties
        Collection<ResolveEdgeProperties> properties = Hibernate.isInitialized(this.properties) ? getProperties() : null;
        if(properties != null && properties.size() > 0)
        {
            Collection<ResolveEdgePropertiesVO> vos = new ArrayList<ResolveEdgePropertiesVO>();
            for (ResolveEdgeProperties model : properties)
            {
                vos.add(model.doGetVO());
            }
            
            vo.setProperties(vos);
        }
        
        
        return vo;
    }

    @Override
    public void applyVOToModel(ResolveEdgeVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUOrder(vo.getUOrder() != null && vo.getUOrder().equals(VO.INTEGER_DEFAULT) ? getUOrder() : vo.getUOrder());
            this.setURelType(vo.getURelType() != null && vo.getURelType().equals(VO.STRING_DEFAULT) ? getURelType() : vo.getURelType());

            //references
            this.setSourceNode(vo.getSourceNode() != null ? new ResolveNode(vo.getSourceNode().getSys_id()): getSourceNode());
            this.setDestinationNode(vo.getDestinationNode() != null ? new ResolveNode(vo.getDestinationNode().getSys_id()): getDestinationNode());
            
            Collection<ResolveEdgePropertiesVO> propertiesVOs = vo.getProperties();
            if(propertiesVOs != null && propertiesVOs.size() > 0)
            {
                Collection<ResolveEdgeProperties> models = new ArrayList<ResolveEdgeProperties>();
                for (ResolveEdgePropertiesVO refVO : propertiesVOs)
                {
                    models.add(new ResolveEdgeProperties(refVO));
                }
                this.setProperties(models);
            }
        }
    }

}
