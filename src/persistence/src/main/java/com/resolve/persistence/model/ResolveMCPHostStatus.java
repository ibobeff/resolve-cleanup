package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolveMCPHostStatusVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "resolve_mcp_host_status", indexes = {@Index(columnList = "u_component_id", name = "rmcs_comp_id_idx")})
public class ResolveMCPHostStatus extends BaseModel<ResolveMCPHostStatusVO>
{
    private static final long serialVersionUID = 8650462611927612802L;
    
    // Thread, CPU etc.
    private String UType;
    // warning, critical etc.
    private String UStatus;
    // hard disk is at critical level, CPU is at warning level. etc.
    private String UDescription;
    // The component to which this status belongs
    private String UComponentName;
    
    // The component to which this status belongs
    private ResolveMCPHost mcpHost;
    
    public ResolveMCPHostStatus()
    {
    }
    
    public ResolveMCPHostStatus(ResolveMCPHostStatusVO vo)
    {
        applyVOToModel(vo);
    }

    @Column (name = "u_type")
    public String getUType()
    {
        return UType;
    }

    public void setUType(String uType)
    {
        UType = uType;
    }

    @Column (name = "u_status")
    public String getUStatus()
    {
        return UStatus;
    }

    public void setUStatus(String uStatus)
    {
        UStatus = uStatus;
    }

    @Column (name = "u_description")
    public String getUDescription()
    {
        return UDescription;
    }
    
    public void setUDescription(String uDescription)
    {
        UDescription = uDescription;
    }

    @Column (name = "u_componentname")
    public String getUComponentName()
    {
        return UComponentName;
    }

    public void setUComponentName(String uComponentName)
    {
        UComponentName = uComponentName;
    }

    @ManyToOne (fetch = FetchType.EAGER)
    @JoinColumn(name = "u_component_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveMCPHost getMcpHost()
    {
        return mcpHost;
    }

    public void setMcpHost(ResolveMCPHost mcpHost)
    {
        this.mcpHost = mcpHost;
    }

    @Override
    public ResolveMCPHostStatusVO doGetVO()
    {
        ResolveMCPHostStatusVO vo = new ResolveMCPHostStatusVO();
        super.doGetBaseVO(vo);
        
        vo.setUDescription(getUDescription());
        vo.setUType(getUType());
        vo.setUStatus(getUStatus());
        vo.setMcpHostVO(Hibernate.isInitialized(getMcpHost()) && getMcpHost() != null ? getMcpHost().doGetVO() : null);
        
        return vo;
    }
    
    @Override
    public void applyVOToModel(ResolveMCPHostStatusVO vo)
    {
        if (vo != null)
        {
            this.setUStatus(StringUtils.isNotBlank(vo.getUStatus()) && vo.getUStatus().equals(VO.STRING_DEFAULT) ? getUStatus() : vo.getUStatus());
            this.setUDescription(StringUtils.isNotBlank(vo.getUDescription()) && vo.getUDescription().equals(VO.STRING_DEFAULT) ? getUDescription() : vo.getUDescription());
            this.setUType(StringUtils.isNotBlank(vo.getUType()) && vo.getUType().equals(VO.STRING_DEFAULT) ? getUType() : vo.getUType());
            
            this.setMcpHost(vo.getMcpHostVO() != null ? new ResolveMCPHost(vo.getMcpHostVO()) : null);
        }
    }
}
