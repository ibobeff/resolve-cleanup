/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolveAssessRelVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "resolve_assess_rel", indexes = {@Index(columnList = "u_assessor_sys_id", name = "u_assessor_rel_sys_id_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveAssessRel extends BaseModel<ResolveAssessRelVO>
{
    private static final long serialVersionUID = 5533403613358909038L;
    
	private ResolveAssess assessor;
	private String URefAssessor; 
	
	public ResolveAssessRel()
	{
	}

    public ResolveAssessRel(ResolveAssessRelVO vo)
    {
        applyVOToModel(vo);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_assessor_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveAssess getAssessor()
    {
        return assessor;
    }

    public void setAssessor(ResolveAssess assessor)
    {
        this.assessor = assessor;
    }

    @Column(name = "u_ref_assessor", length = 400)
    public String getURefAssessor()
    {
        return URefAssessor;
    }

    public void setURefAssessor(String refAssessor)
    {
        this.URefAssessor = refAssessor;
    }

    @Override
    public ResolveAssessRelVO doGetVO()
    {
        ResolveAssessRelVO vo = new ResolveAssessRelVO();
        super.doGetBaseVO(vo);
        
        vo.setURefAssessor(getURefAssessor());
        
        //references - do not do this to avoid cyclic ref
//        vo.setAssessor(Hibernate.isInitialized(this.assessor) && getAssessor() != null ? getAssessor().doGetVO() : null);
        
        return vo;
    }

    @Override
    public void applyVOToModel(ResolveAssessRelVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo); 

            this.setURefAssessor(StringUtils.isNotBlank(vo.getURefAssessor()) && vo.getURefAssessor().equals(VO.STRING_DEFAULT) ? getURefAssessor() : vo.getURefAssessor());
        }
        
        
    }

}
