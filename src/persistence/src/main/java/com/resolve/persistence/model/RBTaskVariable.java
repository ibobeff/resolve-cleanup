/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.RBTaskVariableVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
@Entity
@Table(name = "rb_task_variable", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_task_id" }, name = "rbtv_u_name_u_task_id_uk") })
public class RBTaskVariable extends BaseModel<RBTaskVariableVO>
{
    private String name;
    private String color;
    private Boolean isList;
    private Boolean isFlow;
    private Boolean isWsdata;

    private RBTask task;
    
    public RBTaskVariable()
    {
    }

    public RBTaskVariable(RBTaskVariableVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_name", length = 200)
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Column(name = "u_color", length = 40)
    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    @Column(name = "u_list")
    public Boolean isList()
    {
        return isList;
    }

    public void setList(Boolean isList)
    {
        this.isList = isList;
    }

    @Column(name = "u_flow")
    public Boolean isFlow()
    {
        return isFlow;
    }

    public void setFlow(Boolean isFlow)
    {
        this.isFlow = isFlow;
    }

    @Column(name = "u_wsdata")
    public Boolean isWsdata()
    {
        return isWsdata;
    }

    public void setWsdata(Boolean isWsdata)
    {
        this.isWsdata = isWsdata;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_task_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public RBTask getTask()
    {
        return this.task;
    }

    public void setTask(RBTask task)
    {
        this.task = task;
    }

    @Override
    public RBTaskVariableVO doGetVO()
    {
        RBTaskVariableVO vo = new RBTaskVariableVO();
        super.doGetBaseVO(vo);

        vo.setName(getName());
        vo.setColor(getColor());
        vo.setList(isList());
        vo.setFlow(isFlow());
        vo.setWsdata(isWsdata());

        return vo;
    }

    @Override
    public void applyVOToModel(RBTaskVariableVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setName(StringUtils.isNotBlank(vo.getName()) && vo.getName().equals(VO.STRING_DEFAULT) ? getName() : vo.getName());
            this.setColor(StringUtils.isNotBlank(vo.getColor()) && vo.getColor().equals(VO.STRING_DEFAULT) ? getColor() : vo.getColor());
            this.setList(vo.isList() != null ? vo.isList() : isList());
            this.setFlow(vo.isFlow() != null ? vo.isFlow() : isFlow());
            this.setWsdata(vo.isWsdata() != null ? vo.isWsdata() : isWsdata());

            //references
            this.setTask(vo.getTask() != null ? new RBTask(vo.getTask()) : null);
        }
    }
}
