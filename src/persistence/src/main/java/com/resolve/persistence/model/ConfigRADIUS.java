/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;


import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;

import com.resolve.services.hibernate.vo.ConfigRADIUSVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;


@Entity
@Table(name = "config_radius",  uniqueConstraints = {@UniqueConstraint(columnNames = {"u_domain"}, name = "cradius_u_domain_uk")},
    indexes = {@Index(columnList = "u_org", name = "cradius_u_org_idx")})
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ConfigRADIUS  extends BaseModel<ConfigRADIUSVO>
{
    private static final long serialVersionUID = 5182610806572810187L;
    
    private Boolean UActive;
    private String UPrimaryHost;
    private String USecondaryHost;
    private String USharedSecret;
    private String UAuthProtocol;
    private Integer UAuthPort;
    private Integer UAcctPort;
    private String UDefaultDomain;    
    private Boolean UFallback;
    private String UDomain;// this is mandatory from the UI and should be unique
    private Boolean UIsDefault;
    private String UGateway;
    
    //Radius conf belongs to this organization
    private Organization belongsToOrganization;

    // object references

    // object referenced by

    public ConfigRADIUS()
    {
    }
    
    @Column(name = "u_auth_port")
    public Integer getUAuthPort()
    {
        return UAuthPort;
    }

    public void setUAuthPort(Integer uAuthPort)
    {
        UAuthPort = uAuthPort;
    }
    
    @Column(name = "u_acct_port")
    public Integer getUAcctPort()
    {
        return UAcctPort;
    }

    public void setUAcctPort(Integer uAcctPort)
    {
        UAcctPort = uAcctPort;
    }
    
    @Column(name = "u_is_active",  length = 1)
    @Type(type = "yes_no")
    public Boolean getUActive()
    {
        return UActive;
    }
    
    public Boolean ugetUActive()
    {
        Boolean result = false;
        if(getUActive() != null)
        {
            result = getUActive();
        }
        
        return result;
    }

    public void setUActive(Boolean uActive)
    {
        UActive = uActive;
    }

    @Column(name = "u_domain", length = 300)
    public String getUDomain()
    {
        return UDomain;
    }

    public void setUDomain(String uDomain)
    {
        UDomain = uDomain;
    }

    @Column(name = "u_primary_host", length = 100)
    public String getUPrimaryHost()
    {
        return UPrimaryHost;
    }

    public void setUPrimaryHost(String uPrimaryHost)
    {
        UPrimaryHost = uPrimaryHost;
    }
    
    @Column(name = "u_secondary_host", length = 100)
    public String getUSecondaryHost()
    {
        return USecondaryHost;
    }

    public void setUSecondaryHost(String uSecondaryHost)
    {
        USecondaryHost = uSecondaryHost;
    }
    
    @Column(name = "u_shared_secret", length = 100)
    public String getUSharedSecret()
    {
        /*
        String decryptedSharedSecret = "";
        
        try
        {
            decryptedSharedSecret = CryptUtils.decrypt(USharedSecret);
        }
        catch (Exception e)
        {
            Log.log.error("Error " + e.getMessage() + " in decrypting shared secret.", e);
        }
        
        return decryptedSharedSecret;*/
        
        return USharedSecret;
    }

    public void setUSharedSecret(String uSharedSecret)
    {
        /*
        try
        {
            USharedSecret = CryptUtils.encrypt(uSharedSecret);
        }
        catch (Exception e)
        {
            Log.log.error("Error " + e.getMessage() + " in encrypting shared secret.", e);
        }*/
        
        USharedSecret = uSharedSecret;
    }
    
    @Column(name = "u_auth_protocol", length = 20)
    public String getUAuthProtocol()
    {
        return UAuthProtocol;
    }

    public void setUAuthProtocol(String uAuthProtocol)
    {
        UAuthProtocol = uAuthProtocol;
    }
    
    @Column(name = "u_fallback", length = 1)
    @Type(type = "yes_no")
    public Boolean getUFallback()
    {
        return UFallback;
    }
    
    public Boolean ugetUFallback()
    {
        Boolean result = true;
        if(getUFallback() != null)
        {
            result = getUFallback();
        }
        
        return result;
    }

    public void setUFallback(Boolean uFallback)
    {
        UFallback = uFallback;
    }
    
    @Column(name = "u_default_domain", length = 300)
    public String getUDefaultDomain()
    {
        return UDefaultDomain;
    }

    public void setUDefaultDomain(String uDefaultDomain)
    {
        UDefaultDomain = uDefaultDomain;
    }
    
    @Column(name = "u_is_default", length = 1)
    @Type(type = "yes_no")
    public Boolean getUIsDefault()
    {
        return UIsDefault;
    }

    public Boolean ugetUIsDefault()
    {
        Boolean result = false;
        if(getUIsDefault() != null)
        {
            result = getUIsDefault();
        }
        
        return result;
    }
    
    public void setUIsDefault(Boolean uIsDefault)
    {
        UIsDefault = uIsDefault;
    }

    @Column(name = "u_gateway", length = 40)
    public String getUGateway()
    {
        return UGateway;
    }

    public void setUGateway(String uGateway)
    {
        UGateway = uGateway;
    }
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_org", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public Organization getBelongsToOrganization()
    {
        return belongsToOrganization;
    }

    public void setBelongsToOrganization(Organization belongsToOrganization)
    {
        this.belongsToOrganization = belongsToOrganization;
        if (belongsToOrganization != null)
        {
            Collection<ConfigRADIUS> coll = belongsToOrganization.getConfigRadius();
            if (coll == null)
            {
                coll = new HashSet<ConfigRADIUS>();
                coll.add(this);
                
                belongsToOrganization.setConfigRadius(coll);
            }
        }
    }


    @Override
    public ConfigRADIUSVO doGetVO()
    {
        ConfigRADIUSVO vo = new ConfigRADIUSVO();
        super.doGetBaseVO(vo);
        
        vo.setUActive(getUActive());
        vo.setUDomain(getUDomain());
        vo.setUDefaultDomain(getUDefaultDomain());
        vo.setUFallback(getUFallback());
        vo.setUPrimaryHost(getUPrimaryHost());
        vo.setUSecondaryHost(getUSecondaryHost());
        vo.setUSharedSecret(getUSharedSecret());
        vo.setUAuthProtocol(getUAuthProtocol());
        vo.setUAuthPort(getUAuthPort());
        vo.setUAcctPort(getUAcctPort());
        vo.setUIsDefault(getUIsDefault());
        vo.setUGateway(getUGateway());
        
        vo.setBelongsToOrganization(Hibernate.isInitialized(this.belongsToOrganization) && getBelongsToOrganization() != null ? getBelongsToOrganization().doGetVO() : null);
        
        return vo;
    }

    @Override
    public void applyVOToModel(ConfigRADIUSVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUActive(vo.getUActive() != null ? vo.getUActive() : getUActive());
            this.setUDomain(StringUtils.isNotBlank(vo.getUDomain()) && vo.getUDomain().equals(VO.STRING_DEFAULT) ? getUDomain() : vo.getUDomain());
            this.setUDefaultDomain(StringUtils.isNotBlank(vo.getUDefaultDomain()) && vo.getUDefaultDomain().equals(VO.STRING_DEFAULT) ? getUDefaultDomain() : vo.getUDefaultDomain());
            this.setUFallback(vo.getUFallback() != null ? vo.getUFallback() : getUFallback());
            this.setUPrimaryHost(StringUtils.isNotBlank(vo.getUPrimaryHost()) && vo.getUPrimaryHost().equals(VO.STRING_DEFAULT) ? getUPrimaryHost() : vo.getUPrimaryHost());
            this.setUSecondaryHost(StringUtils.isNotBlank(vo.getUSecondaryHost()) && vo.getUSecondaryHost().equals(VO.STRING_DEFAULT) ? getUSecondaryHost() : vo.getUSecondaryHost());
            
            try
            {
                this.setUSharedSecret(StringUtils.isNotBlank(vo.getUSharedSecret()) && vo.getUSharedSecret().equals(VO.STRING_DEFAULT) ? getUSharedSecret() : CryptUtils.encrypt(vo.getUSharedSecret()));
            }
            catch (Exception e)
            {
                Log.log.warn("Error " + e.getMessage() + " in encrypting shared secret while saving RADIUS configuration, saving shared secret un-encrypted...", e);
                this.setUSharedSecret(StringUtils.isNotBlank(vo.getUSharedSecret()) && vo.getUSharedSecret().equals(VO.STRING_DEFAULT) ? getUSharedSecret() : vo.getUSharedSecret());
            }
            
            this.setUAuthProtocol(StringUtils.isNotBlank(vo.getUAuthProtocol()) && vo.getUAuthProtocol().equals(VO.STRING_DEFAULT) ? getUAuthProtocol() : vo.getUAuthProtocol());
            this.setUAuthPort(vo.getUAuthPort() != null && vo.getUAuthPort().equals(VO.INTEGER_DEFAULT) ? getUAuthPort() : vo.getUAuthPort());
            this.setUAcctPort(vo.getUAcctPort() != null && vo.getUAcctPort().equals(VO.INTEGER_DEFAULT) ? getUAcctPort() : vo.getUAcctPort());
            this.setUIsDefault(vo.getUIsDefault() != null ? vo.getUIsDefault() : getUIsDefault());
            this.setUGateway(StringUtils.isNotBlank(vo.getUGateway()) && vo.getUGateway().equals(VO.STRING_DEFAULT) ? getUGateway() : vo.getUGateway());
            
            this.setBelongsToOrganization(vo.getBelongsToOrganization() != null ? new Organization(vo.getBelongsToOrganization()) : getBelongsToOrganization());
        }
    }
}
