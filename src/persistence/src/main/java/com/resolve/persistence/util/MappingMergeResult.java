/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import com.resolve.util.XDoc;

public class MappingMergeResult
{
    XDoc doc;
    boolean docUpdated;

    MappingMergeResult(XDoc d, boolean updated)
    {
        doc = d;
        docUpdated = updated;
    }
}
