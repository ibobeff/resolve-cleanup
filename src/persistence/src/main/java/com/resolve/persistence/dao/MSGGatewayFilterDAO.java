package com.resolve.persistence.dao;

import com.resolve.persistence.model.MSGGatewayFilter;

public interface MSGGatewayFilterDAO extends GenericDAO<MSGGatewayFilter, String>
{

}
