/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import java.util.Map;

import org.springframework.util.StringUtils;

import com.resolve.util.Constants;
import com.resolve.util.Log;

public class HibernateUtilHelper
{
	private static final String HIBERANTE_REINIT_NOTIFICATION_MSG_RECEIVED_LOG = 
									"Received hibernate reinit notification from cluster member: %s, custom table xml = %s";
	private static final String RECEIVED_HIBERNATE_REINIT_NOTIFICATION_MSG_WITHOUT_SOURCE_ID = 
																"Received hibernate reinit message, but source id is null";
	private static final String RECEIVED_HIBERNATE_REINIT_NOTIFICATION_MSG_WITHOUT_CUSTOM_TABLE_XML = 
														"Received hibernate reinit message, but custom table xml is null";
	private static final String SKIPPING_HIBERNATE_REINIT_FOR_SELF = "Skipping hibernate reinit for self";
	private static final String BUSINESS_RULE_UPDATE_MSG_RECEIVED_LOG = 
												"Received Resolve business rule update message from cluster member: %s";
	private static final String RECEIVED_BUSINESS_RULE_UPDATE_MSG_WITHOUT_SOURCE_ID = 
															"Received business rule message, but its source id is null";												
	private static final String CLEAR_DB_CACHE_MSG_RECEIVED_LOG = 
												"Received DB cache clear message from cluster member guid: %s region: %s";
	private static final String RECEIVED_DB_CACHE_MSG_WITHOUT_SOURCE_ID = 
															"Received DB cache clear message, but its source id is null";
	private static final String CLEAR_HIBERNATE_CACHE_MSG_RECEIVED_LOG = 
										"Received Hibernate cache clear message from cluster member guid: %s region: %s";
	private static final String RECEIVED_HIBERNATE_CACHE_MSG_WITHOUT_SOURCE_ID = 
													"Received Hibernate cache clear message, but its source id is null";
	private static final String CLEAR_CACHED_USER_MSG_RECEIVED_LOG = 
												"Received clear cached user message from cluster member: %s userName: %s";
	private static final String  RECEIVED_CLEAR_CACHED_USER_MSG_WITHOUT_SOURCE_ID = 
												"Received clear cached user message, but its source id is null";
	
    public void reinitHibernate(Map<String, String> params)
    {
        String guid = (String)params.get(Constants.SOURCE_GUID);

        String xml = (String)params.get(Constants.CUSTOM_TABLE_XML);

        Log.log.trace(String.format(HIBERANTE_REINIT_NOTIFICATION_MSG_RECEIVED_LOG, guid, xml));
        
        if (guid==null)
        {
            throw new RuntimeException(RECEIVED_HIBERNATE_REINIT_NOTIFICATION_MSG_WITHOUT_SOURCE_ID);
        }
        
        if (xml==null)
        {
            throw new RuntimeException(RECEIVED_HIBERNATE_REINIT_NOTIFICATION_MSG_WITHOUT_CUSTOM_TABLE_XML);
        }
        
        if (guid.equals(HibernateUtil.getGuid()))
        {
            Log.log.trace(SKIPPING_HIBERNATE_REINIT_FOR_SELF);
        }
        else
        {
            try
            {
                CustomTableMappingUtil.saveMappingFile(xml);
            }
            catch (Exception ex)
            {
                throw new RuntimeException(ex);
            }
            
            HibernateUtil.reinitInternal(true);
        }
    }
    
    public void updateBusinessRuleCache(Map<String, String> params)
    {
        String guid = params.get(Constants.SOURCE_GUID);

        Log.log.trace(String.format(BUSINESS_RULE_UPDATE_MSG_RECEIVED_LOG, guid));
        
        if (guid==null)
        {
            throw new RuntimeException(RECEIVED_BUSINESS_RULE_UPDATE_MSG_WITHOUT_SOURCE_ID);
        }
        
        if (guid.equals(HibernateUtil.getGuid()))
        {
            //Don't need to update itself
            return;
        }
     
    }

    public void clearDBObjectsCache(Map<String, Object> params)
    {
        String guid = (String)params.get(Constants.SOURCE_GUID);
        DBCacheRegionConstants region = DBCacheRegionConstants.valueOf((String)params.get(HibernateUtil.DB_CACHE_REGION));
        String keyToInvalidate = (String) params.get(HibernateUtil.DB_CACHE_KEY);

        Log.log.trace(String.format(CLEAR_DB_CACHE_MSG_RECEIVED_LOG, guid, region.getValue()));
        
        if (guid==null)
        {
            throw new RuntimeException(RECEIVED_DB_CACHE_MSG_WITHOUT_SOURCE_ID);
        }
        
        if (guid.equals(HibernateUtil.getGuid()))
        {
            //Don't need to update itself
            return;
        }
        
        if (HibernateUtil.isHibernateInitializing2())
        {
            return;
        }
        
        try
        {
            HibernateUtil.clearDBCacheRegion(region, false, keyToInvalidate);
        }
        catch (Exception ex)
        {
            throw new RuntimeException(ex);
        }
    }
    
    public void clearHBObjectsCache(Map<String, String> params)
    {
        String guid = params.get(Constants.SOURCE_GUID);
        String region = params.get(HibernateUtil.HB_CACHE_REGION);

        Log.log.trace(String.format(CLEAR_HIBERNATE_CACHE_MSG_RECEIVED_LOG, guid, region));
        
        if (guid==null)
        {
            throw new RuntimeException(RECEIVED_HIBERNATE_CACHE_MSG_WITHOUT_SOURCE_ID);
        }
        
//        if (guid.equals(HibernateUtil.getGuid()))
//        {
//            //Don't need to update itself
//            return;
//        }
        
        if (HibernateUtil.isHibernateInitializing2())
        {
            return;
        }
        
        try
        {
            HibernateUtil.updateHibernateCacheRegion(region);
            HibernateUtil.getSessionFactory().getCache().evictQueryRegions();
        }
        catch (Exception ex)
        {
            throw new RuntimeException(ex);
        }
    }
    
    public void clearCachedUser(Map<String, String> params)
    {
        String guid = params.get(Constants.SOURCE_GUID);
        String userName = params.get(HibernateUtil.CACHED_USER_NAME);

        Log.log.trace(String.format(CLEAR_CACHED_USER_MSG_RECEIVED_LOG, guid, userName));
        
        if (guid==null)
        {
            throw new RuntimeException(RECEIVED_CLEAR_CACHED_USER_MSG_WITHOUT_SOURCE_ID);
        }
        
        if (guid.equals(HibernateUtil.getGuid()))
        {
            //Don't need to update itself
            return;
        }
        
        if (HibernateUtil.isHibernateInitializing2())
        {
            return;
        }
        
        try
        {
            HibernateUtil.clearCachedUser(userName, false);
        }
        catch (Exception ex)
        {
            throw new RuntimeException(ex);
        }
    } 
}
