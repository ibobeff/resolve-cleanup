package com.resolve.persistence.model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.RRSchemaVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
@Entity
@Table(name = "rr_schema", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name"}, name = "rrs_u_name_uk") })
public class RRSchema extends BaseModel<RRSchemaVO>
{
    private String name;
    private String source;
    private String description;
    private String gatewayQueues;
    private Integer order;
    private Collection<RRRule> rules;
    private String jsonFields;
    @Column(name = "u_name", length = 200)
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    @Column(name = "u_source", length = 200)
    public String getSource()
    {
        return source;
    }
    public void setSource(String source)
    {
        this.source = source;
    }
    @Column(name = "u_description", length = 300)
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }
    @Lob
    @Column(name = "u_gateway_queues", length = 16777215)
    public String getGatewayQueues()
    {
        return gatewayQueues;
    }
    public void setGatewayQueues(String gatewayQueues)
    {
        this.gatewayQueues = gatewayQueues;
    }
    @Lob
    @Column(name = "u_json_fields", length = 16777215)
    public String getJsonFields()
    {
        return jsonFields;
    }
    public void setJsonFields(String jsonFields)
    {
        this.jsonFields = jsonFields;
    }
    @Column(name = "u_order")
    public Integer getOrder()
    {
        return order;
    }
    public void setOrder(Integer order)
    {
        this.order = order;
    }

    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "schema", fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public Collection<RRRule> getRules()
    {
        return rules;
    }
    public void setRules(Collection<RRRule> rules)
    {
        this.rules = rules;
    }
    @Override
    public RRSchemaVO doGetVO()
    {
        RRSchemaVO vo = new RRSchemaVO();
        super.doGetBaseVO(vo);

        vo.setName(getName());
        vo.setOrder(getOrder());
        vo.setDescription(getDescription());
        vo.setGatewayQueues(getGatewayQueues());
        vo.setJsonFields(getJsonFields());
        vo.setSource(getSource());
//        List<RRRuleVO> ruleVOs = new ArrayList<RRRuleVO>();
//        if(this.getRules()!=null) {
//            for(RRRule rule:this.getRules())
//                ruleVOs.add(rule.doGetVO());
//            vo.setRules(ruleVOs);
//        }
        return vo;
    }
    
    @Override
    public void applyVOToModel(RRSchemaVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            this.setSysOrg(StringUtils.isNotBlank(vo.getSysOrg()) && vo.getSysOrg().equals(VO.STRING_DEFAULT) ? getSysOrg() : vo.getSysOrg());
            this.setOrder(vo.getOrder() != null && vo.getOrder().equals(VO.INTEGER_DEFAULT) ? getOrder() : vo.getOrder());
            this.setName(StringUtils.isNotBlank(vo.getName()) && vo.getName().equals(VO.STRING_DEFAULT) ? getName() : vo.getName());
            this.setDescription(StringUtils.isNotBlank(vo.getDescription()) && vo.getDescription().equals(VO.STRING_DEFAULT) ? getDescription() : vo.getDescription());
            this.setSource(StringUtils.isNotBlank(vo.getSource()) && vo.getSource().equals(VO.STRING_DEFAULT) ? getSource() : vo.getSource());
            this.setJsonFields(StringUtils.isNotBlank(vo.getJsonFields()) && vo.getJsonFields().equals(VO.STRING_DEFAULT) ? getJsonFields() : vo.getJsonFields());
            this.setGatewayQueues(StringUtils.isNotBlank(vo.getGatewayQueues()) && vo.getGatewayQueues().equals(VO.STRING_DEFAULT) ? getGatewayQueues() : vo.getGatewayQueues());
//            if(this.getRules()!=null)
//                for(RRRuleVO ruleVO:vo.getRules()) {
//                    RRRule rule = new RRRule();
//                    rule.applyVOToModel(ruleVO);
//                }
        }
    }
}
