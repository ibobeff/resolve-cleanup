/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolveActionTaskMockDataVO;
import com.resolve.services.interfaces.CdataProperty;
import com.resolve.services.interfaces.IndexComponent;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "resolve_action_task_mock_data", uniqueConstraints = {@UniqueConstraint(columnNames = {"u_name", "u_invocation"}, name = "ratmd_u_name_u_invoc_uk")},
        indexes = {
                @Index(columnList = "u_name", name = "mock_u_name_idx"),
                @Index(columnList = "u_invocation", name = "mock_u_invocation_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveActionTaskMockData extends BaseModel<ResolveActionTaskMockDataVO>  implements IndexComponent,  CdataProperty
{
    private static final long serialVersionUID = 3092790539672880425L;
    
    public ResolveActionTaskMockData()
    {
        
    }
    
    public ResolveActionTaskMockData(ResolveActionTaskMockDataVO vo)
    {
        applyVOToModel(vo);
    }
    
    private String UName;
    private String UData;
    private String UParams;
    private String UInputs;
    private String UFlows;
    private Boolean UIsActive;
    private String UDescription;
    
    private ResolveActionInvoc resolveActionInvoc;

    @Column(name = "u_name", length = 300, nullable=false)
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    @Lob
    @Column(name = "u_data", length = 16777215)
    public String getUData()
    {
        return this.UData;
    }
    public void setUData(String UData)
    {
        this.UData = UData;
    }
   
    @Lob
    @Column(name = "u_params", length = 16777215)
    public String getUParams()
    {
        return this.UParams;
    }
    
    public void setUParams(String UParams)
    {
        this.UParams = UParams;
    }
    
    @Lob
    @Column(name = "u_inputs", length = 16777215)
    public String getUInputs()
    {
        return this.UInputs;
    }
    public void setUInputs(String UData)
    {
        this.UInputs = UData;
    }
    
    @Lob
    @Column(name = "u_flows", length = 16777215)
    public String getUFlows()
    {
        return this.UFlows;
    }
    public void setUFlows(String UFlows)
    {
        this.UFlows = UFlows;
    }
    
    @Column(name = "u_is_active")
    public Boolean getUIsActive()
    {
        return this.UIsActive;
    }
    public void setUIsActive(Boolean active)
    {
        this.UIsActive = active;
    }
    
    
    
    @Lob
    @Column(name = "u_description", length = 16777215)
    public String getUDescription()
    {
        return UDescription;
    }

    public void setUDescription(String uDescription)
    {
        UDescription = uDescription;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_invocation", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveActionInvoc getResolveActionInvoc()
    {
        return resolveActionInvoc;
    }

    public void setResolveActionInvoc(ResolveActionInvoc resolveActionInvoc)
    {
        this.resolveActionInvoc = resolveActionInvoc;
        
        if (resolveActionInvoc != null)
        {
            Collection<ResolveActionTaskMockData> coll = resolveActionInvoc.getMockDataCollection();
            if (coll == null)
            {
                coll = new HashSet<ResolveActionTaskMockData>();
                coll.add(this);
                
                resolveActionInvoc.setMockDataCollection(coll);
            }
        }
    }
    @Override
    public ResolveActionTaskMockDataVO doGetVO()
    {
        ResolveActionTaskMockDataVO vo = new ResolveActionTaskMockDataVO();
        super.doGetBaseVO(vo);
        
        vo.setUName(getUName());
        vo.setUData(getUData());
        vo.setUParams(getUParams());
        vo.setUInputs(getUInputs());
        vo.setUFlows(getUFlows());
        vo.setUIsActive(getUIsActive());
        vo.setUDescription(getUDescription());
        
        return vo;
    }
    
    @Override
    public void applyVOToModel(ResolveActionTaskMockDataVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);  
            
            this.setUIsActive(vo.getUIsActive() != null ? vo.getUIsActive() : getUIsActive());
            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUData(StringUtils.isNotBlank(vo.getUData()) && vo.getUData().equals(VO.STRING_DEFAULT) ? getUData() : vo.getUData());
            this.setUParams(StringUtils.isNotBlank(vo.getUParams()) && vo.getUParams().equals(VO.STRING_DEFAULT) ? getUParams() : vo.getUParams());
            this.setUInputs(StringUtils.isNotBlank(vo.getUInputs()) && vo.getUInputs().equals(VO.STRING_DEFAULT) ? getUInputs() : vo.getUInputs());
            this.setUFlows(StringUtils.isNotBlank(vo.getUFlows()) && vo.getUFlows().equals(VO.STRING_DEFAULT) ? getUFlows() : vo.getUFlows());
            this.setUDescription(StringUtils.isNotBlank(vo.getUDescription()) && vo.getUDescription().equals(VO.STRING_DEFAULT) ? getUDescription() : vo.getUDescription());
            
            // Reference
            this.setResolveActionInvoc(vo.getResolveActionInvoc() != null ? new ResolveActionInvoc(vo.getResolveActionInvoc()) : null);
        }
    }
    
    @Override
    public String ugetIndexContent()
    {
        return this.UName + " " + this.getUData();
    }
    
    @Override
    public List<String> ugetCdataProperty()
    {
        List<String> list = new ArrayList<String>();
        list.add("UData");
        
        return list;
    }
}
