package com.resolve.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.resolve.services.hibernate.vo.ArchiveSirActivityVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "archive_sir_activity")
public class ArchiveSirActivity extends BaseModel<ArchiveSirActivityVO>
{
	private static final long serialVersionUID = 1880188148247409531L;
	
	private String activityId;
	private String altActivityId;
	private String worksheetId;
	private String incidentId;
	private Date dueDate;
	private String status;
	private String assignee;
	
	@Column(name = "u_activity_id",  length = 32)
	public String getActivityId()
    {
        return activityId;
    }
    public void setActivityId(String activityId)
    {
        this.activityId = activityId;
    }
    
    @Column(name = "u_alt_activity_id",  length = 32)
    public String getAltActivityId()
    {
        return altActivityId;
    }
    public void setAltActivityId(String altActivityId)
    {
        this.altActivityId = altActivityId;
    }
    
    @Column(name = "u_worksheet_id",  length = 32)
    public String getWorksheetId()
    {
        return worksheetId;
    }
    public void setWorksheetId(String worksheetId)
    {
        this.worksheetId = worksheetId;
    }
    
    @Column(name = "u_incident_id",  length = 32)
    public String getIncidentId()
    {
        return incidentId;
    }
    public void setIncidentId(String incidentId)
    {
        this.incidentId = incidentId;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "u_due_date", length = 19)
    public Date getDueDate()
	{
		return dueDate;
	}
	public void setDueDate(Date dueDate)
	{
		this.dueDate = dueDate;
	}
	
	@Column(name = "u_status",  length = 50)
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	
	@Column(name = "u_assignee", length = 255)
	public String getAssignee()
    {
        return assignee;
    }
    public void setAssignee(String assignee)
    {
        this.assignee = assignee;
    }
    
    @Override
	public ArchiveSirActivityVO doGetVO()
	{
		ArchiveSirActivityVO vo = new ArchiveSirActivityVO();
		super.doGetBaseVO(vo);
		
		vo.setActivityId(this.getActivityId());
		vo.setAltActivityId(this.getAltActivityId());
		vo.setAssignee(this.getAssignee());
		vo.setIncidentId(this.getIncidentId());
		vo.setWorksheetId(this.getWorksheetId());
		vo.setDueDate(this.getDueDate());
		vo.setStatus(this.getStatus());
		
		return vo;
	}
	
	@Override
	public void applyVOToModel(ArchiveSirActivityVO vo)
	{
		if (vo != null)
		{
			super.applyVOToModel(vo);
			
			this.setDueDate(vo.getDueDate() != null && vo.getDueDate().equals(VO.DATE_DEFAULT) ? getDueDate() : vo.getDueDate());
			this.setStatus(StringUtils.isNotBlank(vo.getStatus()) && vo.getStatus().equals(VO.STRING_DEFAULT) ? getStatus() : vo.getStatus());
			this.setActivityId(StringUtils.isNotBlank(vo.getActivityId()) && vo.getActivityId().equals(VO.STRING_DEFAULT) ? getActivityId() : vo.getActivityId());
			this.setAltActivityId(StringUtils.isNotBlank(vo.getAltActivityId()) && vo.getAltActivityId().equals(VO.STRING_DEFAULT) ? getAltActivityId() : vo.getAltActivityId());
			this.setWorksheetId(StringUtils.isNotBlank(vo.getWorksheetId()) && vo.getWorksheetId().equals(VO.STRING_DEFAULT) ? getWorksheetId() : vo.getWorksheetId());
			this.setIncidentId(StringUtils.isNotBlank(vo.getIncidentId()) && vo.getIncidentId().equals(VO.STRING_DEFAULT) ? getIncidentId() : vo.getIncidentId());
			this.setAssignee(StringUtils.isNotBlank(vo.getAssignee()) && vo.getAssignee().equals(VO.STRING_DEFAULT) ? getAssignee() : vo.getAssignee());
		}
	}
}
