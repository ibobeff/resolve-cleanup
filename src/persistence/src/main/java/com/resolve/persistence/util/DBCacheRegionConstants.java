/******************************************************************************
* (C) Copyright 2018
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import com.resolve.persistence.model.ResolveBlueprint;

public enum DBCacheRegionConstants
{
	USER_ROLES_CACHE_REGION("USER_ROLE_CACHE_REGION"),
	IS_ORG_ACCESSIBLE_CACHE_REGION("IS_ORG_ACCESSIBLE_CACHE_REGION"),
	USER_BY_NAME_CACHE_REGION("USER_BY_NAME_CACHE_REGION"),
	HAS_ACCESS_RIGHT_CACHE_REGION("HAS_ACCESS_RIGHT_CACHE_REGION"),
	WIKI_DOCUMENT_CACHE_REGION("WIKI_DOCUMENT_CACHE_REGION"),
    NEW_HAS_ACCESS_RIGHT_CACHE_REGION("NEW_HAS_ACCESS_RIGHT_CACHE_REGION"),
    PARSER_CACHE_REGION("PARSER_CACHE_REGION"),
    ASSESSOR_CACHE_REGION("ASSESSOR_CACHE_REGION"),
    PROPERTIES_CACHE_REGION("PROPERTIES_CACHE_REGION"),
	RESOLVE_BLUEPRINT_CACHE_REGION(ResolveBlueprint.class.getName());
	
    private final String value;
    
    private DBCacheRegionConstants(String value) {
    	this.value = value;
    }
    
    public String getValue() {
    	return value;
    }
}
