/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import java.io.Serializable;

import org.hibernate.CallbackException;
import org.hibernate.type.Type;

public interface SIDListener
{
	public boolean onDataOperation(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) throws CallbackException;
}
