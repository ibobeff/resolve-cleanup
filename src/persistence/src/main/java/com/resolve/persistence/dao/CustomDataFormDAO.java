package com.resolve.persistence.dao;

import com.resolve.persistence.model.CustomDataForm;

public interface CustomDataFormDAO extends GenericDAO<CustomDataForm, String> {}
