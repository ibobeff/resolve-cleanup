/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.CustomTableVO;
import com.resolve.services.hibernate.vo.MetaFieldVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;


@Entity
@Table(name = "custom_table",  uniqueConstraints = {@UniqueConstraint(columnNames = {"u_name"}, name = "cstmtbl_u_name_uk")},
        indexes = {@Index(columnList = "u_meta_table_sys_id", name = "u_meta_table_sys_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CustomTable extends BaseModel<CustomTableVO>
{
    private static final long serialVersionUID = -6824263711342826530L;
    
    private String UName;
    private String UModelName;
    private String UDisplayName;
    private String USchemaDefinition;
    private String UType;
    private String USource;
    private String UDestination;
    private String UReferenceTableNames;
    
    // object references
    private MetaTable metaTable;
    
    // Object refrences by
    private Collection<MetaField> metaFields;
    
    public CustomTable()
    {
    }
    
    public CustomTable(CustomTableVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_name", nullable = false, length = 100)
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    @Column(name = "u_model_name", length = 100)
    public String getUModelName()
    {
        return this.UModelName;
    }

    public void setUModelName(String UModelName)
    {
        this.UModelName = UModelName;
    }
    
    @Column(name = "u_display_name", length = 100)
    public String getUDisplayName()
    {
        return this.UDisplayName;
    }

    public void setUDisplayName(String UDisplayName)
    {
        this.UDisplayName = UDisplayName;
    }
    
    @Lob
    @Column(name = "u_schema_definition", length = 16777215)
    public String getUSchemaDefinition()
    {
        return this.USchemaDefinition;
    }

    public void setUSchemaDefinition(String USchemaDefinition)
    {
        this.USchemaDefinition = USchemaDefinition;
    }
    
    @Column(name = "u_type", length = 100)
    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }
    
    @Column(name = "u_source", length = 100)
    public String getUSource()
    {
        return this.USource;
    }

    public void setUSource(String USource)
    {
        this.USource = USource;
    }
    
    @Column(name = "u_destination", length = 100)
    public String getUDestination()
    {
        return this.UDestination;
    }

    public void setUDestination(String UDestination)
    {
        this.UDestination = UDestination;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_meta_table_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaTable getMetaTable()
    {
        return metaTable;
    }
    
    public void setMetaTable(MetaTable metaTable)
    {
        this.metaTable = metaTable;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "customTable", fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<MetaField> getMetaFields()
    {
        return this.metaFields;
    }

    public void setMetaFields(Collection<MetaField> metaFields)
    {
        this.metaFields = metaFields;
    }

    @Column(name = "u_reference_table_names", length = 500)
    public String getUReferenceTableNames()
    {
        return UReferenceTableNames;
    }

    public void setUReferenceTableNames(String uReferenceTableNames)
    {
        UReferenceTableNames = uReferenceTableNames;
    }

    @Override
    public CustomTableVO doGetVO()
    {
        CustomTableVO vo = new CustomTableVO();
        super.doGetBaseVO(vo);
        
        vo.setUDestination(getUDestination());
        vo.setUDisplayName(getUDisplayName());
        vo.setUModelName(getUModelName());
        vo.setUName(getUName());
        vo.setUReferenceTableNames(getUReferenceTableNames());
        vo.setUSchemaDefinition(getUSchemaDefinition());
        vo.setUSource(getUSource());
        vo.setUType(getUType());

        //references
        vo.setMetaTable(Hibernate.isInitialized(this.metaTable) && getMetaTable() != null ? getMetaTable().doGetVO() : null) ;

        Collection<MetaField> metaFields = Hibernate.isInitialized(this.metaFields) ? getMetaFields(): null;
        if(metaFields != null && metaFields.size() > 0)
        {
            Collection<MetaFieldVO> vos = new ArrayList<MetaFieldVO>();
            for(MetaField model : metaFields)
            {
                vos.add(model.doGetVO());
            }
            vo.setMetaFields(vos);
        }
        
        return vo;
    }

    @Override
    public void applyVOToModel(CustomTableVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo); 
            
            this.setUDestination(StringUtils.isNotBlank(vo.getUDestination()) && vo.getUDestination().equals(VO.STRING_DEFAULT) ? getUDestination() : vo.getUDestination());
            this.setUDisplayName(StringUtils.isNotBlank(vo.getUDisplayName()) && vo.getUDisplayName().equals(VO.STRING_DEFAULT) ? getUDisplayName() : vo.getUDisplayName());
            this.setUModelName(StringUtils.isNotBlank(vo.getUModelName()) && vo.getUModelName().equals(VO.STRING_DEFAULT) ? getUModelName() : vo.getUModelName());
            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUReferenceTableNames(StringUtils.isNotBlank(vo.getUReferenceTableNames()) && vo.getUReferenceTableNames().equals(VO.STRING_DEFAULT) ? getUReferenceTableNames() : vo.getUReferenceTableNames());
            this.setUSchemaDefinition(StringUtils.isNotBlank(vo.getUSchemaDefinition()) && vo.getUSchemaDefinition().equals(VO.STRING_DEFAULT) ? getUSchemaDefinition() : vo.getUSchemaDefinition());
            this.setUSource(StringUtils.isNotBlank(vo.getUSource()) && vo.getUSource().equals(VO.STRING_DEFAULT) ? getUSource() : vo.getUSource());
            this.setUType(StringUtils.isNotBlank(vo.getUType()) && vo.getUType().equals(VO.STRING_DEFAULT) ? getUType() : vo.getUType());

            //references
            this.setMetaTable(vo.getMetaTable() != null ? new MetaTable(vo.getMetaTable()) : null);
            
            Collection<MetaFieldVO> metaFieldVOs = vo.getMetaFields();
            if(metaFieldVOs != null && metaFieldVOs.size() > 0)
            {
                Collection<MetaField> models = new ArrayList<MetaField>();
                for(MetaFieldVO refVO : metaFieldVOs)
                {
                    models.add(new MetaField(refVO));
                }
                this.setMetaFields(models);
            }
        }
    }
    
    
    
}
