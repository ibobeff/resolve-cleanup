/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import org.dom4j.Element;

abstract public class ColumnDefinition
{
    String name;
    String column;
    
    public String getName() 
    {
        return name;
    }
    
    public void setName(String n)
    {
        name=n;
    }
    
    public String getColumn()
    {
        return column;
    }

    public void  setColumn(String c)
    {
       column = c;
    }
   
    abstract public Element addColumn(Element entity);
    
}
