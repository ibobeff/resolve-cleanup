package com.resolve.persistence.model;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;

import com.resolve.services.hibernate.vo.ResolveSecurityIncidentVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.Log;

@Entity
@Table(name = "resolve_security_incident", indexes = {
                @Index(columnList = "sys_created_on", name = "rsi_sys_created_on_idx"),
                @Index(columnList = "u_investigation_type", name = "rsi_investigation_type_idx"),
                @Index(columnList = "u_severity", name = "rsi_severity_idx"),
                @Index(columnList = "u_playbook", name = "rsi_u_playbook_idx"),
                @Index(columnList = "u_status", name = "rsi_status_idx"),
                @Index(columnList = "u_closed_on", name = "rsi_closed_on_idx"),
                @Index(columnList = "u_problem_id", name = "rsi_problem_id_idx")})
public class ResolveSecurityIncident extends BaseModel<ResolveSecurityIncidentVO>
{
	private static final long serialVersionUID = 2493634654402250L;
	
	public static final Integer MIN_RISK_SCORE = VO.NON_NEGATIVE_INTEGER_DEFAULT;
	public static final Integer MAX_RISK_SCORE = Integer.valueOf(500);
	public static final Integer RISK_SCORE_NOT_SET = Integer.valueOf(VO.NON_NEGATIVE_INTEGER_DEFAULT.intValue() - 1);
	public static final String RISK_SCORE_NOT_SET_TEXT = "Not Set";
	public static final String RISK_SCORE_FIELD_LABLE = "riskScore";
	public static final String MASTER_SIR_FIELD_LABLE = "masterSir";
	
	private String title;
	private String sourceSystem; 		// Source system from where the incident generated (Resolve or Splunk or Arcsight, etc...)
	private String externalReferenceId; // Incident ID from the external system.
	private String investigationType; 	// phishing, malware, etc...
	private String type; 				// security or IT
	private String severity;            // Critical, High, Medium, Low etc.
	private String playbook; 			// playbook which got assigned to this incident
	private String externalStatus; 		// status of this incident on the external system 
	private String status; 				// status in Resolve
	private String owner;               // User who is an owner of this incident.
	private Integer playbookVersion;    // Version of an assigned playbook.
	private String sir;					// Unique numeric ID with a width of 9 digits. Will only be used to display in UI.
	private Date closedOn;				// Date on which the incident is closed
	private String closedBy;			// User who closed the incident.
	private Boolean primary;    		// Flag indicating this as a primary incident.
	private String members;				// Comma separated list of users who are member of this incident.
	private Boolean sirStarted;			// Flag indicating whether the incident is started or not. FOR INTERNAL USE ONLY.
	private String problemId;			// problem id (worksheet id) of an assigned worksheet.
	private String dataCompromised;		// Text indicator showing whether the data is compromised or not.
	private Date dueBy;        			// SIR is due by date
	private String requestData; 		// All Data from a Gateway to an Investigation Record in SIR case
	private Long sla;                   // SLA in seconds
	private Integer riskScore;          // Risk Score 0 - 100 (both inclusive), -1 means not set
	private String classification;      // Classification set in the Case Configuration
	private Date assignmentDate;        // Determines when the incident was assigned for the last time
	
    private ResolveSecurityIncident masterSir;            // Master SIR 
    private Set<ResolveSecurityIncident> duplicateSirs = new HashSet<ResolveSecurityIncident>();       // All duplicate SIRs
    private String priority;
    private Map<String, String> caseAttributes = new HashMap<>();
    private List<CustomDataForm> customFormWiki = new ArrayList<>();
	
	public ResolveSecurityIncident()
	{
	    super();
	    
	    setPrimary(VO.BOOLEAN_DEFAULT);
	    setSirStarted(VO.BOOLEAN_DEFAULT);
	}
	
	public ResolveSecurityIncident(ResolveSecurityIncidentVO vo)
	{
	    applyVOToModel(vo);
	}
	
	@Column(name = "u_title", length = 200)
	public String getTitle()
	{
		return title;
	}
	public void setTitle(String title)
	{
		this.title = title;
	}
	
	@Column(name = "u_external_reference_id", length = 1000)
	public String getExternalReferenceId()
	{
		return externalReferenceId;
	}

	public void setExternalReferenceId(String referenceId)
	{
		this.externalReferenceId = referenceId;
	}
	
	@Column(name = "u_investigation_type", length = 100)
	public String getInvestigationType()
	{
		return investigationType;
	}

	public void setInvestigationType(String investigationType)
	{
		this.investigationType = investigationType;
	}
	
	@Column(name = "u_type", length = 100)
	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}
	
	@Column(name = "u_severity", length = 40)
	public String getSeverity()
	{
		return severity;
	}

	public void setSeverity(String severity)
	{
		this.severity = severity;
	}
	
	@Column(name = "u_source_system", length = 100)
	public String getSourceSystem()
	{
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem)
	{
		this.sourceSystem = sourceSystem;
	}
	
	@Column(name = "u_playbook", length = 100)
	public String getPlaybook()
	{
		return playbook;
	}

	public void setPlaybook(String playbook)
	{
		this.playbook = playbook;
	}
	
	@Column(name = "u_external_status", length = 100)
	public String getExternalStatus()
	{
		return externalStatus;
	}

	public void setExternalStatus(String externalStatus)
	{
		this.externalStatus = externalStatus;
	}
	
	@Column(name = "u_status", length = 100)
	public String getStatus()
	{
		return status;
	}
	
	public void setStatus(String status)
	{
		this.status = status;
	}
	
	@Column(name = "u_owner", length = 100)
	public String getOwner()
	{
		return owner;
	}
	
	public void setOwner(String owner)
	{
		this.owner = owner;
	}
	
	@Column(name = "u_playbook_version")
	public Integer getPlaybookVersion()
	{
		return playbookVersion;
	}
	
	public void setPlaybookVersion(Integer playbookVersion)
	{
		this.playbookVersion = playbookVersion;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "u_closed_on", length = 19)
	public Date getClosedOn()
	{
		return closedOn;
	}
	
	public void setClosedOn(Date closedOn)
	{
		this.closedOn = closedOn;
	}
	
	@Column(name = "u_closed_by", length = 100)
	public String getClosedBy()
	{
		return closedBy;
	}
	
	public void setClosedBy(String closedBy)
	{
		this.closedBy = closedBy;
	}
	
	@Column(name = "u_is_primary", length = 1)
    @Type(type = "yes_no")
	public Boolean getPrimary()
	{
		return primary;
	}
	
	public Boolean ugetPrimary()
	{
	    if (getPrimary() != null)
	    {
	        return getPrimary();
	    }
	    else
	    {
	        return VO.BOOLEAN_DEFAULT;
	    }
	}
	
	public void setPrimary(Boolean primary)
	{
		this.primary = primary;
	}
	
	@Column(name = "u_members", length = 500)
	public String getMembers()
	{
		return members;
	}
	
	public void setMembers(String members)
	{
		this.members = members;
	}
	
	@Column(name = "u_sir_started", length = 1)
    @Type(type = "yes_no")
	public Boolean getSirStarted()
	{
		return sirStarted;
	}
	
	public Boolean ugetSirStarted()
	{
	    if (getSirStarted() != null)
	    {
	        return getSirStarted();
	    }
	    else
	    {
	        return VO.BOOLEAN_DEFAULT;
	    }
	}
	
	public void setSirStarted(Boolean sirStarted)
	{
		this.sirStarted = sirStarted;
	}
	
	@Column(name = "u_problem_id",  nullable = false, length = 32)
	public String getProblemId()
	{
		return problemId;
	}
	
	public void setProblemId(String problemId)
	{
		this.problemId = problemId;
	}
	
	@Column(name = "u_data_compromised", length = 100)
	public String getDataCompromised()
	{
	    if (isBlank(dataCompromised))
	        return "Unknown";
		return dataCompromised;
	}

	public void setDataCompromised(String dataCompromised)
	{
		this.dataCompromised = dataCompromised;
	}
	
	@Column(name = "u_sir", length = 15, unique = true)
	public String getSir()
	{
		return this.sir;
	}
	public void setSir(String sir)
	{
		this.sir = sir;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "u_due_by", length = 19)
    public Date getDueBy()
    {
        return dueBy;
    }
    public void setDueBy(Date dueBy)
    {
        this.dueBy = dueBy;
    }
    
    // RBA-14934 : Save All Data from a Gateway to an Investigation Record in SIR case
    @Lob
    @Column (name = "u_request_data")
	public String getRequestData()
    {
		return requestData;
	}

    /*
     * requestData is String representation of params map which is getting passed to a gateway.
     * The Jackson mapper could not deserialize instance of Json object from requestData and map it to a String
     * By changing it to Object, mapper does not complain. Hibernate is also OK with this.
     */
    public void setRequestData(Object requestData)
    {
        if (requestData != null)
            this.requestData = requestData.toString();
    }

	@Column(name = "u_sla")
	public Long getSla()
    {
        return sla;
    }

    public void setSla(Long sla)
    {
        this.sla = sla;
    }
    
    @Column(name = "u_risk_score")
    @ColumnDefault(value = "-1")
    public Integer getRiskScore()
    {
        return riskScore;
    }
    
    public void setAssignmentDate(Date assignmentDate)
    {
        this.assignmentDate = assignmentDate;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "u_assignment_date", length = 19)
    public Date getAssignmentDate()
    {
        return assignmentDate;
    }
    
    public void setRiskScore(Integer riskScore)
    {
        Integer tmpRiskScore = riskScore;
                        
        if (tmpRiskScore != null)
        {
            if (tmpRiskScore.compareTo(RISK_SCORE_NOT_SET) < 0)
            {
                tmpRiskScore = RISK_SCORE_NOT_SET;
            }
            
            if (tmpRiskScore.compareTo(MAX_RISK_SCORE) > 0)
            {
                tmpRiskScore = MAX_RISK_SCORE;
            }
        }
        else
        {
            tmpRiskScore = RISK_SCORE_NOT_SET;
        }
            
        this.riskScore = tmpRiskScore;
    }
    
    @Column (name = "u_classification")
    public String getClassification()
    {
        return classification;
    }
    
    public void setClassification(String classification)
    {
        this.classification = classification;
    }

    @ManyToOne
    @JoinColumn(name = "u_master_sir", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    private ResolveSecurityIncident getMasterSir()
    {
        return masterSir;
    }
    
    private void setMasterSir(ResolveSecurityIncident sir)
    {
        this.masterSir = sir;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "masterSir", fetch = FetchType.EAGER)
    @NotFound(action = NotFoundAction.IGNORE)
    @BatchSize(size=30)
    private Set<ResolveSecurityIncident> getDuplicateSirs()
    {
        return duplicateSirs;
    }
    
    private void setDuplicateSirs(Set<ResolveSecurityIncident> duplicates )
    {
        this.duplicateSirs = duplicates;
    }


    // Public interface methods 
    @Transient
    public boolean isMaster()  
    {
    	return this.getMasterSir()==null;
    }
    
    @Transient
    public boolean isDuplicate()  
    {
    	return !this.isMaster();
    }

//    @Transient
//    public ResolveSecurityIncident getMaster()  
//    {
//    	return this.getMasterSir();
//    }
    
    @Transient
    public Collection<ResolveSecurityIncident> getDuplicates()  
    {
    	return Collections.unmodifiableCollection(this.getDuplicateSirs());
    }

    public void addDuplicate(ResolveSecurityIncident dup)
    {
		Log.log.info("Try to add duplicate SIR " + dup + " to master SIR " + this);

    	if (this.getMasterSir()!=null)
    	{
    		throw new RuntimeException("Duplicate should be added to a master SIR only. Can not add a duplicate SIR " + dup.getSir() + " to another duplicate " + this);
    	}
    	
    	if (dup.getMasterSir()==null) // dup is a master itself
    	{
    		dup.setMasterSir(this);
    		this.getDuplicateSirs().add(dup);
    		
    		for (ResolveSecurityIncident d : dup.getDuplicateSirs())  // move its duplicates to new master  
    		{
    			d.setMasterSir(this);
    			this.getDuplicateSirs().add(d);
    		}
    		dup.getDuplicateSirs().clear(); // dup is not a master any more, so need to clear its duplicate set 
    	}
    	else  // dup is a duplicate and has a master
    	{
    		ResolveSecurityIncident oldMaster = dup.getMasterSir();
    		oldMaster.getDuplicateSirs().remove(dup);
    		dup.setMasterSir(this);
    		this.getDuplicateSirs().add(dup);
    	}
    }

    public void removeDuplicate(ResolveSecurityIncident dup)  //remove existing duplicate from master
    {
		Log.log.info("Try to remove duplicate SIR " + dup + " from master SIR: " + this + ", duplicate's old master is " + dup.getMasterSir());

    	boolean removed = this.getDuplicateSirs().remove(dup);
    	if (removed)
    	{
    		Log.log.info("Removed duplicate SIR " + dup + " from master SIR: " + this + ", duplicate's old master is " + dup.getMasterSir());
    		dup.setMasterSir(null); // dup becomes a new master now
    	}
    	else
    	{
    		Log.log.info("Failed to remove duplicate SIR " + dup + " from master SIR " + this + ", duplicate's old master is " + dup.getMasterSir());
    		// not sure if dup is a duplicate of this SIR, so don't reset dup's master to null.
    	}
    }

    public void switchMaster(ResolveSecurityIncident currentMaster, ResolveSecurityIncident newMaster)  // this is duplicate and try to switch master
    {
    	Log.log.info("Duplicate SIR " + this + " try to switch master from SIR " + currentMaster + " to new master SIR " + newMaster);
    	currentMaster.getDuplicateSirs().remove(this);
    	newMaster.getDuplicateSirs().add(this);
    	this.setMasterSir(newMaster);
    }
    
    @Column (name = "u_priority")
    public String getPriority()
    {
        return priority;
    }
    public void setPriority(String priority)
    {
        this.priority = priority;
    }

	@ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "incident_case_attribute", joinColumns = @JoinColumn(name = "sir_id"))
	@MapKeyColumn(name="u_name")
	@Column(name = "u_value")
	public Map<String, String> getCaseAttributes() {
		return caseAttributes;
	}

	public void setCaseAttributes(Map<String, String> caseAttributes) {
		this.caseAttributes = caseAttributes;
	}

	@OneToMany(mappedBy = "incident", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public List<CustomDataForm> getCustomFormWiki() {
		return customFormWiki;
	}

	public void setCustomFormWiki(List<CustomDataForm> customFormWiki) {
		this.customFormWiki = customFormWiki;
	}

	@Override
	public ResolveSecurityIncidentVO doGetVO()
	{
		ResolveSecurityIncidentVO vo = new ResolveSecurityIncidentVO();
		
		super.doGetBaseVO(vo);
				
		vo.setInvestigationType(this.getInvestigationType());
		vo.setSourceSystem(this.getSourceSystem());
		vo.setExternalReferenceId(this.getExternalReferenceId());
		vo.setPlaybook(this.getPlaybook());
		vo.setSeverity(this.getSeverity());
		vo.setType(this.getType());
		vo.setTitle(this.getTitle());
		vo.setStatus(this.getStatus());
		vo.setExternalStatus(this.getExternalStatus());
		vo.setOwner(this.getOwner());
		vo.setPlaybookVersion(this.getPlaybookVersion());
		vo.setSir(this.getSir());
		vo.setClosedBy(this.getClosedBy());
		vo.setClosedOn(this.getClosedOn());
		vo.setPrimary(this.ugetPrimary());
		vo.setMembers(this.getMembers());
		vo.setAssignmentDate(this.getAssignmentDate());
		vo.setSirStarted(this.ugetSirStarted());
		vo.setProblemId(this.getProblemId());
		vo.setDataCompromised(this.getDataCompromised());
		vo.setDueBy(this.getDueBy());
		// RBA-14934 : Save All Data from a Gateway to an Investigation Record in SIR case
		vo.setRequestData(this.getRequestData());
		vo.setSla(this.getSla());
		vo.setRiskScore(this.getRiskScore());
		vo.setClassification(this.getClassification());
		
		if (masterSir!=null)
		{
			vo.setMasterSysId(masterSir.getSys_id());
			vo.setMasterSir(masterSir.getSir());
		}
		
		if (!this.getDuplicateSirs().isEmpty())
		{
			Map<String,String> dups = new HashMap<String,String>();
			for (ResolveSecurityIncident r : this.getDuplicateSirs())
			{
				dups.put(r.getSir(), r.getSys_id());
			}
			vo.setDuplicates(dups);
		}
		
		vo.setPriority(this.getPriority());
		
		if (this.getCaseAttributes() != null) {
			for (String key : this.getCaseAttributes().keySet()) {
				vo.getCaseAttributes().put(key, this.getCaseAttributes().get(key));
			}
		}

		this.getCustomFormWiki().forEach(f -> vo.getCustomFormWiki().add(f.doGetVO()));

		return vo;
	}
	
	@Override
	public void applyVOToModel(ResolveSecurityIncidentVO vo)
	{
		if (vo != null)
		{
			super.applyVOToModel(vo);
			
			this.setTitle(isNotBlank(vo.getTitle()) && vo.getTitle().equals(VO.STRING_DEFAULT) ? getTitle() : vo.getTitle());
			this.setSourceSystem(isNotBlank(vo.getSourceSystem()) && vo.getSourceSystem().equals(VO.STRING_DEFAULT) ? getSourceSystem() : vo.getSourceSystem());
			this.setSeverity(isNotBlank(vo.getSeverity()) && vo.getSeverity().equals(VO.STRING_DEFAULT) ? getSeverity() : vo.getSeverity());
			this.setExternalReferenceId(isNotBlank(vo.getExternalReferenceId()) && vo.getExternalReferenceId().equals(VO.STRING_DEFAULT) ? getExternalReferenceId() : vo.getExternalReferenceId());
			this.setType(isNotBlank(vo.getType()) && vo.getType().equals(VO.STRING_DEFAULT) ? getType() : vo.getType());
			this.setInvestigationType(isNotBlank(vo.getInvestigationType()) && vo.getInvestigationType().equals(VO.STRING_DEFAULT) ? getInvestigationType() : vo.getInvestigationType());
			this.setPlaybook(isNotBlank(vo.getPlaybook()) && vo.getPlaybook().equals(VO.STRING_DEFAULT) ? getPlaybook() : vo.getPlaybook());
			this.setStatus(isNotBlank(vo.getStatus()) && vo.getStatus().equals(VO.STRING_DEFAULT) ? getStatus() : vo.getStatus());
			this.setExternalStatus(isNotBlank(vo.getExternalStatus()) && vo.getExternalStatus().equals(VO.STRING_DEFAULT) ? getExternalStatus() : vo.getExternalStatus());
			this.setOwner(isNotBlank(vo.getOwner()) && vo.getOwner().equals(VO.STRING_DEFAULT) ? getOwner() : vo.getOwner());
			this.setPlaybookVersion(vo.getPlaybookVersion() != null && vo.getPlaybookVersion().equals(VO.INTEGER_DEFAULT) ? getPlaybookVersion() : vo.getPlaybookVersion());
			this.setSir(isNotBlank(vo.getSir()) && vo.getSir().equals(VO.STRING_DEFAULT) ? getSir() : vo.getSir());
			this.setClosedBy(isNotBlank(vo.getClosedBy()) && vo.getClosedBy().equals(VO.STRING_DEFAULT) ? getClosedBy() : vo.getClosedBy());
			this.setClosedOn(vo.getClosedOn() != null && vo.getClosedOn().equals(VO.DATE_DEFAULT) ? getClosedOn() : vo.getClosedOn());
			this.setPrimary(vo.getPrimary() != null ? vo.getPrimary() : this.ugetPrimary());
			this.setMembers(isNotBlank(vo.getMembers()) && vo.getMembers().equals(VO.STRING_DEFAULT) ? getMembers() : vo.getMembers());
			this.setSirStarted(vo.getSirStarted() != null ? vo.getSirStarted() : this.ugetSirStarted());
			this.setProblemId(isNotBlank(vo.getProblemId()) && vo.getProblemId().equals(VO.STRING_DEFAULT) ? getProblemId() : vo.getProblemId());
			this.setDataCompromised(isNotBlank(vo.getDataCompromised()) && vo.getDataCompromised().equals(VO.STRING_DEFAULT) ? getDataCompromised() : vo.getDataCompromised());
			this.setDueBy(vo.getDueBy() != null && vo.getDueBy().equals(VO.DATE_DEFAULT) ? getDueBy() : vo.getDueBy());
			// RBA-14934 : Save All Data from a Gateway to an Investigation Record in SIR case
			this.setRequestData(isNotBlank(vo.getRequestData()) && vo.getRequestData().equals(VO.STRING_DEFAULT) ? getRequestData() : vo.getRequestData());
			this.setSla(vo.getSla() != null && vo.getSla().equals(VO.LONG_DEFAULT) ? getSla() : vo.getSla());
			this.setRiskScore(vo.getRiskScore() != null && vo.getRiskScore().equals(RISK_SCORE_NOT_SET) ? getRiskScore() : vo.getRiskScore());
			this.setClassification(isNotBlank(vo.getClassification()) && vo.getClassification().equals(VO.STRING_DEFAULT) ? getClassification() : vo.getClassification());
			this.setAssignmentDate(vo.getAssignmentDate() != null && vo.getAssignmentDate().equals(VO.DATE_DEFAULT) ? getAssignmentDate() : vo.getAssignmentDate());
			
			this.setPriority(isNotBlank(vo.getPriority()) && vo.getPriority().equals(VO.STRING_DEFAULT) ? getPriority() : vo.getPriority());
			if (vo.getCaseAttributes() != null) {
				for (String key : vo.getCaseAttributes().keySet()) {
					this.getCaseAttributes().put(key, vo.getCaseAttributes().get(key));
				}
			}
			
			if (isNotBlank(vo.getMasterSysId()) && !vo.getMasterSysId().equals(VO.STRING_DEFAULT))
			{
				ResolveSecurityIncident master = new ResolveSecurityIncident();
				master.setSys_id(vo.getMasterSysId());
				master.setSir(vo.getMasterSir());
				master.addDuplicate(this);
			}
		}
	}
	
    @Override
	public void resetFieldsWithDefaultValues()
    {
	    super.resetFieldsWithDefaultValues();
	    
	    setPrimary(null);
        setSirStarted(null);
        setDataCompromised(null);
    }
	
	@Override
	public String toString()
	{
	    return "Resolve Security Incident [" +
	           (isNotBlank(getSys_id()) ? "Sys Id: " + getSys_id() + ", " : "") + 
	           "Sys Org: " + (isNotBlank(getSysOrg()) ? getSysOrg() : "") + 
	           ", SIR: " + sir + 
	           "]";
	}
}
