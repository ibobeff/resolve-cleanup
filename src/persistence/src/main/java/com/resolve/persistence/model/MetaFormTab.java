/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.MetaFormTabFieldVO;
import com.resolve.services.hibernate.vo.MetaFormTabVO;
import com.resolve.services.hibernate.vo.MetaxFieldDependencyVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;


@Entity
@Table(name = "meta_form_tab", indexes = {
                @Index(columnList = "u_meta_form_view_sys_id", name = "mft_u_meta_form_view_idx"),
                @Index(columnList = "u_metax_form_view_pnl_sys_id", name = "mft_u_meta_form_view_pnl_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaFormTab extends BaseModel<MetaFormTabVO>
{
    private static final long serialVersionUID = -2149194263358732941L;
    
    private String UTabName;
    private Integer UOrder;
    private String UPosition;
    private Integer UTabWidth;
    private Boolean UIsSelectedDefault;
    private Integer UNoOfVerticalColumns; //1 or 2
    private String UUrl;//if this has value, the 'metaFormTabFields' will be NULL or EMPTY

    // object referenced by
    private MetaFormView metaFormView;
    private Collection<MetaFormTabField> metaFormTabFields;
    
    private MetaxFormViewPanel metaxFormViewPanel;
    
    private Collection<MetaxFieldDependency> metaxFieldDependencys;

    public MetaFormTab()
    {
    }
    
    public MetaFormTab(MetaFormTabVO vo)
    {
        applyVOToModel(vo);
    }

    
    /**
     * @return the uTabName
     */
    @Column(name = "u_tab_name", length = 100)
    public String getUTabName()
    {
        return UTabName;
    }
    /**
     * @param uTabName the uTabName to set
     */
    public void setUTabName(String uTabName)
    {
        UTabName = uTabName;
    }

    /**
     * @return the uOrder
     */
    @Column(name = "u_order")
    public Integer getUOrder()
    {
        return UOrder;
    }
    /**
     * @param uOrder the uOrder to set
     */
    public void setUOrder(Integer uOrder)
    {
        UOrder = uOrder;
    }

    /**
     * @return the uPosition
     */
    @Column(name = "u_position", length = 100)
    public String getUPosition()
    {
        return UPosition;
    }
    /**
     * @param uPosition the uPosition to set
     */
    public void setUPosition(String uPosition)
    {
        UPosition = uPosition;
    }

    /**
     * @return the uTabWidth
     */
    @Column(name = "u_tab_width")
    public Integer getUTabWidth()
    {
        return UTabWidth;
    }
    /**
     * @param uTabWidth the uTabWidth to set
     */
    public void setUTabWidth(Integer uTabWidth)
    {
        UTabWidth = uTabWidth;
    }

    /**
     * @return the uIsSelectedDefault
     */
    @Column(name = "u_is_selected_default")
    public Boolean getUIsSelectedDefault()
    {
        return UIsSelectedDefault;
    }
    /**
     * @param uIsSelectedDefault the uIsSelectedDefault to set
     */
    public void setUIsSelectedDefault(Boolean uIsSelectedDefault)
    {
        UIsSelectedDefault = uIsSelectedDefault;
    }

    
    /**
     * @return the uNoOfVerticalColumns
     */
    @Column(name = "u_no_of_vertical_columns")
    public Integer getUNoOfVerticalColumns()
    {
        return UNoOfVerticalColumns;
    }
    public void setUNoOfVerticalColumns(Integer uNoOfVerticalColumns)
    {
        UNoOfVerticalColumns = uNoOfVerticalColumns;
    }

    /**
     * @return the uSource
     */
    @Column(name = "u_url", length = 4000)
    public String getUUrl()
    {
        return UUrl;
    }
    public void setUUrl(String uSource)
    {
        UUrl = uSource;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "metaFormTab")
    @LazyCollection(LazyCollectionOption.FALSE)
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<MetaFormTabField> getMetaFormTabFields()
    {
        return this.metaFormTabFields;
    }

    public void setMetaFormTabFields(Collection<MetaFormTabField> metaFormTabField)
    {
        this.metaFormTabFields = metaFormTabField;
    }

    @ManyToOne(fetch = FetchType.EAGER) //THIS IS DONE ON PURPOSE
    @JoinColumn(name = "u_meta_form_view_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaFormView getMetaFormView()
    {
        return metaFormView;
    }

    public void setMetaFormView(MetaFormView metaFormView)
    {
        this.metaFormView = metaFormView;

        if (metaFormView != null)
        {
            Collection<MetaFormTab> coll = metaFormView.getMetaFormTabs();
            if (coll == null)
            {
                coll = new HashSet<MetaFormTab>();
                coll.add(this);
                
                metaFormView.setMetaFormTabs(coll);
            }
        }
    }

    @ManyToOne(fetch = FetchType.EAGER) //THIS IS DONE ON PURPOSE
    @JoinColumn(name = "u_metax_form_view_pnl_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MetaxFormViewPanel getMetaxFormViewPanel()
    {
        return metaxFormViewPanel;
    }

    public void setMetaxFormViewPanel(MetaxFormViewPanel metaxFormViewPanel)
    {
        this.metaxFormViewPanel = metaxFormViewPanel;
        
        if (metaxFormViewPanel != null)
        {
            Collection<MetaFormTab> coll = metaxFormViewPanel.getMetaFormTabs();
            if (coll == null)
            {
                coll = new HashSet<MetaFormTab>();
                coll.add(this);
                
                metaxFormViewPanel.setMetaFormTabs(coll);
            }
        }
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "metaFormTab")
    @LazyCollection(LazyCollectionOption.FALSE)
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<MetaxFieldDependency> getMetaxFieldDependencys()
    {
        return this.metaxFieldDependencys;
    }

    public void setMetaxFieldDependencys(Collection<MetaxFieldDependency> metaxFieldDependencys)
    {
        this.metaxFieldDependencys = metaxFieldDependencys;
    }

    @Override
    public MetaFormTabVO doGetVO()
    {
        MetaFormTabVO vo = new MetaFormTabVO();
        super.doGetBaseVO(vo);
        
        vo.setUIsSelectedDefault(getUIsSelectedDefault());
        vo.setUNoOfVerticalColumns(getUNoOfVerticalColumns());
        vo.setUOrder(getUOrder());
        vo.setUPosition(getUPosition());
        vo.setUTabName(getUTabName());
        vo.setUTabWidth(getUTabWidth());
        vo.setUUrl(getUUrl());
        
        //refs
//        MetaFormView metaFormView;
//      MetaxFormViewPanel metaxFormViewPanel;
        Collection<MetaFormTabField> metaFormTabFields = Hibernate.isInitialized(this.metaFormTabFields) ? getMetaFormTabFields() : null;
        if(metaFormTabFields != null && metaFormTabFields.size() > 0)
        {
            Collection<MetaFormTabFieldVO> vos = new ArrayList<MetaFormTabFieldVO>();
            for(MetaFormTabField model : metaFormTabFields)
            {
                vos.add(model.doGetVO());
            }
            vo.setMetaFormTabFields(vos);
        }
        
        
        Collection<MetaxFieldDependency> metaxFieldDependencys = Hibernate.isInitialized(this.metaxFieldDependencys) ? getMetaxFieldDependencys() : null;
        if(metaxFieldDependencys != null && metaxFieldDependencys.size() > 0)
        {
            Collection<MetaxFieldDependencyVO> vos = new ArrayList<MetaxFieldDependencyVO>();
            for(MetaxFieldDependency model : metaxFieldDependencys)
            {
                vos.add(model.doGetVO());
            }
            vo.setMetaxFieldDependencys(vos);
        }
        
        return vo;
    }

    @Override
    public void applyVOToModel(MetaFormTabVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUIsSelectedDefault(vo.getUIsSelectedDefault() != null ?  vo.getUIsSelectedDefault() : getUIsSelectedDefault());
            this.setUNoOfVerticalColumns(vo.getUNoOfVerticalColumns() != null && vo.getUNoOfVerticalColumns().equals(VO.INTEGER_DEFAULT) ? getUNoOfVerticalColumns() : vo.getUNoOfVerticalColumns());
            this.setUOrder(vo.getUOrder() != null && vo.getUOrder().equals(VO.INTEGER_DEFAULT) ? getUOrder() : vo.getUOrder());
            this.setUPosition(StringUtils.isNotBlank(vo.getUPosition()) && vo.getUPosition().equals(VO.STRING_DEFAULT) ? getUPosition() : vo.getUPosition());
            this.setUTabName(StringUtils.isNotBlank(vo.getUTabName()) && vo.getUTabName().equals(VO.STRING_DEFAULT) ? getUTabName() : vo.getUTabName());
            this.setUTabWidth(vo.getUTabWidth() != null && vo.getUTabWidth().equals(VO.INTEGER_DEFAULT) ? getUTabWidth() : vo.getUTabWidth());
            this.setUUrl(StringUtils.isNotBlank(vo.getUUrl()) && vo.getUUrl().equals(VO.STRING_DEFAULT) ? getUUrl() : vo.getUUrl());
            
            //refs
            Collection<MetaFormTabFieldVO> metaFormTabFieldsVOs  = vo.getMetaFormTabFields();
            if(metaFormTabFieldsVOs != null && metaFormTabFieldsVOs.size() > 0)
            {
                Collection<MetaFormTabField> models = new ArrayList<MetaFormTabField>();
                for(MetaFormTabFieldVO refVO : metaFormTabFieldsVOs)
                {
                    models.add(new MetaFormTabField(refVO));
                }
                this.setMetaFormTabFields(models);
            }
            
            Collection<MetaxFieldDependencyVO> metaxFieldDependencysVOs = vo.getMetaxFieldDependencys();
            if(metaxFieldDependencysVOs != null && metaxFieldDependencysVOs.size() > 0)
            {
                Collection<MetaxFieldDependency> models = new ArrayList<MetaxFieldDependency>();
                for(MetaxFieldDependencyVO refVO : metaxFieldDependencysVOs)
                {
                    models.add(new MetaxFieldDependency(refVO));
                }
                this.setMetaxFieldDependencys(models);
            }
            
        }
            
        
    }
    
    

}//MetaFormTab
