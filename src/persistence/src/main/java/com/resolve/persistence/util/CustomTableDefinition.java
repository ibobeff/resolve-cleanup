/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import java.util.ArrayList;
import java.util.List;

public class CustomTableDefinition
{
    private String entityName;
    private String tableName;
    private List<ColumnDefinition> columns;

    public CustomTableDefinition(String entity, String table)
    {
        entityName=entity;
        tableName=table;
    }

    public CustomTableDefinition(String table)
    {
        this(table,table);
    }
    
    String getEntityName()
    {
        return entityName;
    }
    
    void setEntityName(String n)
    {
        entityName = n;
    }

    String getTableName()
    {
        return tableName;
    }
    
    void setTableName(String n)
    {
        tableName = n;
    }
    
    
    List<ColumnDefinition> getColumns()
    {
        return columns;
    }
    
    void setColumns(List<ColumnDefinition> c)
    {
        columns = c;
    }
    
    public CustomTableDefinition addColumnDefinition(ColumnDefinition d)
    {
        if (columns==null)
        {
            columns = new ArrayList<ColumnDefinition>();
        }
        columns.add(d);
        return this;
    }
    
}
