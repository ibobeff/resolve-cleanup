/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.resolve.services.hibernate.vo.MetaViewFieldVO;
import com.resolve.services.interfaces.VO;


@Entity
@Table(name = "meta_view_field")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaViewField  extends BaseModel<MetaViewFieldVO>
{
    private static final long serialVersionUID = -1352827424988567990L;
    
    private String UId;
    private String UHeader;
    private Integer UWidth;
    private Boolean URowHeader;
    private String UAlignment;
    //private String URendererJavascript;
    private Boolean UResizeable;
    private Boolean USortable;
    private String UToolTip;
    private Boolean UGroupable;
    private Boolean UFixed;
    private String UDateTimeFormat;
    
    public MetaViewField()
    {
    }
    
    public MetaViewField(MetaViewFieldVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_id", nullable = false, length = 100)
    public String getUId()
    {
        return this.UId;
    }

    public void setUId(String UId)
    {
        this.UId = UId;
    }
    
    @Column(name = "u_header", nullable = false, length = 100)
    public String getUHeader()
    {
        return this.UHeader;
    }

    public void setUHeader(String UHeader)
    {
        this.UHeader = UHeader;
    }
    
    @Column(name = "u_width")
    public Integer getUWidth()
    {
        return this.UWidth;
    }
    
    public void setUWidth(Integer UWidth)
    {
        this.UWidth = UWidth;
    }
    
    @Column(name = "u_rowheader")
    public Boolean getURowHeader()
    {
        return this.URowHeader;
    }
    
    public void setURowHeader(Boolean URowHeader)
    {
        this.URowHeader = URowHeader;
    }
    
    @Column(name = "u_alignment", nullable = false, length = 100)
    public String getUAlignment()
    {
        return this.UAlignment;
    }

    public void setUAlignment(String UAlignment)
    {
        this.UAlignment = UAlignment;
    }
    
//    @Column(name = "u_rendererjavascript", nullable = false, length = 333)
//    public String getURendererJavascript()
//    {
//        return this.URendererJavascript;
//    }
//
//    public void setURendererJavascript(String URendererJavascript)
//    {
//        this.URendererJavascript = URendererJavascript;
//    }
    
    @Column(name = "u_resizeable")
    public Boolean getUResizeable()
    {
        return this.UResizeable;
    }
    
    public void setUResizeable(Boolean UResizeable)
    {
        this.UResizeable = UResizeable;
    }
    
    @Column(name = "u_sortable")
    public Boolean getUSortable()
    {
        return this.USortable;
    }
    
    public void setUSortable(Boolean USortable)
    {
        this.USortable = USortable;
    }
    
    @Column(name = "u_tooltip", length = 333)
    public String getUToolTip()
    {
        return this.UToolTip;
    }

    public void setUToolTip(String UToolTip)
    {
        this.UToolTip = UToolTip;
    }
    
    @Column(name = "u_groupable")
    public Boolean getUGroupable()
    {
        return this.UGroupable;
    }
    
    public void setUGroupable(Boolean UGroupable)
    {
        this.UGroupable = UGroupable;
    }
    
    @Column(name = "u_fixed")
    public Boolean getUFixed()
    {
        return this.UFixed;
    }
    
    public void setUFixed(Boolean UFixed)
    {
        this.UFixed = UFixed;
    }
    
    @Column(name = "u_dateTimeformat", nullable = false, length = 333)
    public String getUDateTimeFormat()
    {
        return this.UDateTimeFormat;
    }

    public void setUDateTimeFormat(String UDateTimeFormat)
    {
        this.UDateTimeFormat = UDateTimeFormat;
    }
    

    @Override
    public MetaViewFieldVO doGetVO()
    {
        MetaViewFieldVO vo = new MetaViewFieldVO();
        super.doGetBaseVO(vo);
        
        vo.setUAlignment(getUAlignment());
        vo.setUDateTimeFormat(getUDateTimeFormat());
        vo.setUHeader(getUHeader());
        vo.setUId(getUId());
        vo.setUToolTip(getUToolTip());
        
        vo.setUWidth(getUWidth());
        
        vo.setUResizeable(getUResizeable());
        vo.setURowHeader(getURowHeader());
        vo.setUSortable(getUSortable());
        vo.setUFixed(getUFixed());
        vo.setUGroupable(getUGroupable());
        
        return vo;
    }

    @Override
    public void applyVOToModel(MetaViewFieldVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo); 
            
            this.setUAlignment((String) VO.evaluateValue(vo.getUAlignment(), getUAlignment()));
            this.setUDateTimeFormat((String) VO.evaluateValue(vo.getUDateTimeFormat(), getUDateTimeFormat()));
            this.setUHeader((String) VO.evaluateValue(vo.getUHeader(), getUHeader()));
            this.setUId((String) VO.evaluateValue(vo.getUId(), getUId()));
            this.setUToolTip((String) VO.evaluateValue(vo.getUToolTip(), getUToolTip()));
            
            this.setUWidth((Integer) VO.evaluateValue(vo.getUWidth(), getUWidth()));
            
            this.setUResizeable(vo.getUResizeable() != null ? vo.getUResizeable() : getUResizeable());
            this.setURowHeader(vo.getURowHeader() != null ? vo.getURowHeader() : getURowHeader());
            this.setUSortable(vo.getUSortable() != null ? vo.getUSortable() : getUSortable());
            this.setUFixed(vo.getUFixed() != null ? vo.getUFixed() : getUFixed());
            this.setUGroupable(vo.getUGroupable() != null ? vo.getUGroupable() : getUGroupable());
            
        }
    }
}
