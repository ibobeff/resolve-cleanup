/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

// Generated Dec 11, 2009 11:15:20 AM by Hibernate Tools 3.2.4.GA

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolveParserTemplateVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

/**
 * ResolveParserTemplate generated by hbm2java
 */
@Entity
@Table(name = "resolve_parser_template", indexes = {@Index(columnList = "u_parser", name = "rpt_u_parser_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveParserTemplate extends BaseModel<ResolveParserTemplateVO>
{
    private static final long serialVersionUID = -879401813023890532L;
    
    private String UType;
    private String UMatchRegex;
    private String UNamePos;
    private String UValuePos;

	// transient
	
    // object references
    private ResolveParser parser; // UParser;
	
    // object referenced by
	
    public ResolveParserTemplate()
    {
    }

    public ResolveParserTemplate(ResolveParserTemplateVO vo)
    {
        applyVOToModel(vo);
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_parser", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveParser getParser()
    {
        return parser;
    }

    public void setParser(ResolveParser parser)
    {
        this.parser = parser;

        if (parser != null)
        {
            Collection<ResolveParserTemplate> coll = parser.getResolveParserTemplates();
            if (coll == null)
            {
				coll = new HashSet<ResolveParserTemplate>();
	            coll.add(this);
	            
                parser.setResolveParserTemplates(coll);
            }
        }
    }
    
    @Column(name = "u_type", length = 40)
    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }

    @Column(name = "u_match_regex", length = 100)
    public String getUMatchRegex()
    {
        return this.UMatchRegex;
    }

    public void setUMatchRegex(String UMatchRegex)
    {
        this.UMatchRegex = UMatchRegex;
    }

    @Column(name = "u_name_pos", length = 100)
    public String getUNamePos()
    {
        return this.UNamePos;
    }

    public void setUNamePos(String UNamePos)
    {
        this.UNamePos = UNamePos;
    }

    @Column(name = "u_value_pos", length = 100)
    public String getUValuePos()
    {
        return this.UValuePos;
    }

    public void setUValuePos(String UValuePos)
    {
        this.UValuePos = UValuePos;
    }

    @Override
    public ResolveParserTemplateVO doGetVO()
    {
        ResolveParserTemplateVO vo = new ResolveParserTemplateVO();
        super.doGetBaseVO(vo);
        
        vo.setUMatchRegex(getUMatchRegex());
        vo.setUNamePos(getUNamePos());
        vo.setUType(getUType());
        vo.setUValuePos(getUValuePos());
        
        //references
//        vo.setParser(Hibernate.isInitialized(this.parser) && getParser() != null ? getParser().doGetVO() : null);
        
        return vo;
    }

    @Override
    public void applyVOToModel(ResolveParserTemplateVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUMatchRegex(StringUtils.isNotBlank(vo.getUMatchRegex()) && vo.getUMatchRegex().equals(VO.STRING_DEFAULT) ? getUMatchRegex() : vo.getUMatchRegex());
            this.setUNamePos(StringUtils.isNotBlank(vo.getUNamePos()) && vo.getUNamePos().equals(VO.STRING_DEFAULT) ? getUNamePos() : vo.getUNamePos());
            this.setUType(StringUtils.isNotBlank(vo.getUType()) && vo.getUType().equals(VO.STRING_DEFAULT) ? getUType() : vo.getUType());
            this.setUValuePos(StringUtils.isNotBlank(vo.getUValuePos()) && vo.getUValuePos().equals(VO.STRING_DEFAULT) ? getUValuePos() : vo.getUValuePos());
            
            //references
//            this.setParser(vo.getParser() != null ? new ResolveParser(vo.getParser()) : null);
            
        }
        
    }

}
