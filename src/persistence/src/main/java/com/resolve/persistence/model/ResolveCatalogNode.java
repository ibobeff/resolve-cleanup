/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;

import com.resolve.services.hibernate.vo.ResolveCatalogEdgeVO;
import com.resolve.services.hibernate.vo.ResolveCatalogNodeTagRelVO;
import com.resolve.services.hibernate.vo.ResolveCatalogNodeVO;
import com.resolve.services.hibernate.vo.ResolveCatalogNodeWikidocRelVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "catalog_node", indexes = {@Index(columnList = "u_catalog_sys_id", name = "rcn_u_catalog_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResolveCatalogNode  extends BaseModel<ResolveCatalogNodeVO> 
{
    private static final long serialVersionUID = -5459920319921663136L;
    
    //wiki namespace - for the root node. This will be applied to all the nodes if the namespace of the doc is not present
    private String UNamespace = null;
    private String UName;
    private String UType;
    protected String UOperation;

//    private Integer UOrder;

    private String URoles;
    private String UEditRoles;

    private String UTitle;
    private String UDescription;
    private String UImage;
    private String UImageName;
    private String UTooltip;
    private String UWiki;
    private String ULink;
    private String UForm;
    private String UDisplayType;
    private Integer UMaxImageWidth;
    private String UPath;
    private Boolean UOpenInNewTab;
    private Integer USideWidth;

    private String UCatalogType;

    private String UWikidocSysID;

    private Boolean UIsRoot;
    private Boolean UIsRootRef;
    
    private String UInternalName;
    private String UIcon;
    
    //attributes for manipulating the object to sql and vice versa
//    private Integer ULft;
//    private Integer URgt;
//    private Integer UDepth;
    
    private String UReferenceCatalogSysId; //node referencing the other catalog
    
    //references
    private ResolveCatalog catalog; //belongs to this catalog
    private Collection<ResolveCatalogNodeWikidocRel> catalogWikiRels; //referenceing which wiki
    private Collection<ResolveCatalogNodeTagRel> catalogTagRels; //catalog and tag rels
    
    private Collection<ResolveCatalogEdge> catalogEdges;

    public ResolveCatalogNode()
    {
    }

    public ResolveCatalogNode(String sysId)
    {
        super.setSys_id(sysId);
    }
    
    public ResolveCatalogNode(ResolveCatalogNodeVO vo)
    {
        applyVOToModel(vo);
    }
    
    @Column(name = "u_namespace", length = 300)
    public String getUNamespace()
    {
        return UNamespace;
    }


    public void setUNamespace(String uNamespace)
    {
        UNamespace = uNamespace;
    }

    @Column(name = "u_name", length = 300)
    public String getUName()
    {
        return UName;
    }


    public void setUName(String uName)
    {
        UName = uName;
    }

    @Column(name = "u_type", length = 100)
    public String getUType()
    {
        return UType;
    }


    public void setUType(String uType)
    {
        UType = uType;
    }

    @Column(name = "u_operation", length = 50)
    public String getUOperation()
    {
        return UOperation;
    }


    public void setUOperation(String uOperation)
    {
        UOperation = uOperation;
    }

//    @Column(name = "u_order")
//    public Integer getUOrder()
//    {
//        return UOrder;
//    }
//
//
//    public void setUOrder(Integer uOrder)
//    {
//        UOrder = uOrder;
//    }

    @Column(name = "u_roles", length = 1000)
    public String getURoles()
    {
        return URoles;
    }


    public void setURoles(String uRoles)
    {
        URoles = uRoles;
    }


    @Column(name = "u_editroles", length = 1000)
    public String getUEditRoles()
    {
        return UEditRoles;
    }


    public void setUEditRoles(String uEditRoles)
    {
        UEditRoles = uEditRoles;
    }

    @Column(name = "u_title", length = 300)
    public String getUTitle()
    {
        return UTitle;
    }


    public void setUTitle(String uTitle)
    {
        UTitle = uTitle;
    }

    @Column(name = "u_description", length = 1000)
    public String getUDescription()
    {
        return UDescription;
    }


    public void setUDescription(String uDescription)
    {
        UDescription = uDescription;
    }

    @Column(name = "u_image", length = 100)
    public String getUImage()
    {
        return UImage;
    }


    public void setUImage(String uImage)
    {
        UImage = uImage;
    }

    @Column(name = "u_imagename", length = 100)
    public String getUImageName()
    {
        return UImageName;
    }


    public void setUImageName(String uImageName)
    {
        UImageName = uImageName;
    }

    @Column(name = "u_tooltip", length = 300)
    public String getUTooltip()
    {
        return UTooltip;
    }


    public void setUTooltip(String uTooltip)
    {
        UTooltip = uTooltip;
    }

    @Column(name = "u_wiki", length = 300)
    public String getUWiki()
    {
        return UWiki;
    }


    public void setUWiki(String uWiki)
    {
        UWiki = uWiki;
    }

    @Column(name = "u_link", length = 2000)
    public String getULink()
    {
        return ULink;
    }


    public void setULink(String uLink)
    {
        ULink = uLink;
    }

    @Column(name = "u_form", length = 300)
    public String getUForm()
    {
        return UForm;
    }


    public void setUForm(String uForm)
    {
        UForm = uForm;
    }

    @Column(name = "u_displaytype", length = 50)
    public String getUDisplayType()
    {
        return UDisplayType;
    }


    public void setUDisplayType(String uDisplayType)
    {
        UDisplayType = uDisplayType;
    }

    @Column(name = "u_max_img_width")
    public Integer getUMaxImageWidth()
    {
        return UMaxImageWidth;
    }


    public void setUMaxImageWidth(Integer uMaxImageWidth)
    {
        UMaxImageWidth = uMaxImageWidth;
    }

    @Column(name = "u_path", length = 4000)
    public String getUPath()
    {
        return UPath;
    }


    public void setUPath(String uPath)
    {
        UPath = uPath;
    }

    @Column(name = "u_open_in_new_tab", length = 1)
    @Type(type = "yes_no")
    public Boolean getUOpenInNewTab()
    {
        return UOpenInNewTab;
    }


    public void setUOpenInNewTab(Boolean uOpenInNewTab)
    {
        UOpenInNewTab = uOpenInNewTab;
    }

    @Column(name = "u_side_width")
    public Integer getUSideWidth()
    {
        return USideWidth;
    }


    public void setUSideWidth(Integer uSideWidth)
    {
        USideWidth = uSideWidth;
    }

    @Column(name = "u_catalogtype", length = 50)
    public String getUCatalogType()
    {
        return UCatalogType;
    }


    public void setUCatalogType(String uCatalogType)
    {
        UCatalogType = uCatalogType;
    }

    @Column(name = "u_wiki_sys_id", length = 32)
    public String getUWikidocSysID()
    {
        return UWikidocSysID;
    }


    public void setUWikidocSysID(String uWikidocSysID)
    {
        UWikidocSysID = uWikidocSysID;
    }

    @Column(name = "u_is_root", length = 1)
    @Type(type = "yes_no")
    public Boolean getUIsRoot()
    {
        return UIsRoot;
    }


    public void setUIsRoot(Boolean uIsRoot)
    {
        UIsRoot = uIsRoot;
    }

    @Column(name = "u_is_root_ref", length = 1)
    @Type(type = "yes_no")
    public Boolean getUIsRootRef()
    {
        return UIsRootRef;
    }


    public void setUIsRootRef(Boolean uIsRootRef)
    {
        UIsRootRef = uIsRootRef;
    }

    @Column(name = "u_internalname", length = 100)
    public String getUInternalName()
    {
        return UInternalName;
    }


    public void setUInternalName(String uInternalName)
    {
        UInternalName = uInternalName;
    }

    @Column(name = "u_icon", length = 100)
    public String getUIcon()
    {
        return UIcon;
    }


    public void setUIcon(String uIcon)
    {
        UIcon = uIcon;
    }

//    @Column(name = "u_lft")
//    public Integer getULft()
//    {
//        return ULft;
//    }
//
//
//    public void setULft(Integer uLft)
//    {
//        ULft = uLft;
//    }
//
//    @Column(name = "u_rgt")
//    public Integer getURgt()
//    {
//        return URgt;
//    }
//
//
//    public void setURgt(Integer uLgt)
//    {
//        URgt = uLgt;
//    }
//
//    @Column(name = "u_depth")
//    public Integer getUDepth()
//    {
//        return UDepth;
//    }
//
//
//    public void setUDepth(Integer uDepth)
//    {
//        UDepth = uDepth;
//    }

    @Column(name = "u_ref_catalog_sys_id", length = 1000)
    public String getUReferenceCatalogSysId()
    {
        return UReferenceCatalogSysId;
    }


    public void setUReferenceCatalogSysId(String uReferenceCatalogSysId)
    {
        UReferenceCatalogSysId = uReferenceCatalogSysId;
    }
    
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_catalog_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveCatalog getCatalog()
    {
        return catalog;
    }

    public void setCatalog(ResolveCatalog catalog)
    {
        this.catalog = catalog;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "catalogNode")
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.DELETE)
    public Collection<ResolveCatalogNodeWikidocRel> getCatalogWikiRels()
    {
        return catalogWikiRels;
    }

    public void setCatalogWikiRels(Collection<ResolveCatalogNodeWikidocRel> catalogWikiRels)
    {
        this.catalogWikiRels = catalogWikiRels;
    }
    
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "catalogNode")
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.DELETE)
    public Collection<ResolveCatalogNodeTagRel> getCatalogTagRels()
    {
        return catalogTagRels;
    }

    public void setCatalogTagRels(Collection<ResolveCatalogNodeTagRel> catalogTagRels)
    {
        this.catalogTagRels = catalogTagRels;
    }
    
    
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "sourceNode", fetch = FetchType.EAGER)
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade(CascadeType.DELETE)
    public Collection<ResolveCatalogEdge> getCatalogEdges()
    {
        return catalogEdges;
    }

    public void setCatalogEdges(Collection<ResolveCatalogEdge> catalogEdges)
    {
        this.catalogEdges = catalogEdges;
    }

    @Override
    public ResolveCatalogNodeVO doGetVO()
    {
        ResolveCatalogNodeVO vo = new ResolveCatalogNodeVO();
        super.doGetBaseVO(vo);
        
        vo.setUName(getUName());
        vo.setUNamespace(getUNamespace());
        vo.setUType(getUType());
        vo.setUOperation(getUOperation());
        
//        vo.setUOrder(getUOrder());
        
        vo.setURoles(getURoles());
        vo.setUEditRoles(getUEditRoles());
        
        vo.setUTitle(getUTitle());
        vo.setUDescription(getUDescription());
        vo.setUImage(getUImage());
        vo.setUImageName(getUImageName());
        vo.setUTooltip(getUTooltip());
        vo.setUWiki(getUWiki());
        vo.setULink(getULink());
        vo.setUForm(getUForm());
        vo.setUDisplayType(getUDisplayType());
        vo.setUMaxImageWidth(getUMaxImageWidth());
        vo.setUPath(getUPath());
        vo.setUOpenInNewTab(getUOpenInNewTab());
        vo.setUSideWidth(getUSideWidth());
        
        vo.setUCatalogType(getUCatalogType());
        
        vo.setUWikidocSysID(getUWikidocSysID());
        
        vo.setUIsRoot(getUIsRoot());
        vo.setUIsRootRef(getUIsRootRef());
        
        vo.setUInternalName(getUInternalName());
        vo.setUIcon(getUIcon());
        
        vo.setUReferenceCatalogSysId(getUReferenceCatalogSysId());
        
        //references
        Collection<ResolveCatalogNodeWikidocRel> catalogWikiRels = Hibernate.isInitialized(this.catalogWikiRels) ? getCatalogWikiRels() : null; //referenceing which wiki
        if (catalogWikiRels != null && catalogWikiRels.size() > 0)
        {
            Collection<ResolveCatalogNodeWikidocRelVO> vos = new ArrayList<ResolveCatalogNodeWikidocRelVO>();
            for (ResolveCatalogNodeWikidocRel model : catalogWikiRels)
            {
                vos.add(model.doGetVO());
            }
            vo.setCatalogWikiRels(vos);
        }

        Collection<ResolveCatalogNodeTagRel> catalogTagRels = Hibernate.isInitialized(this.catalogTagRels) ? getCatalogTagRels() : null; //catalog and tag rels
        if (catalogTagRels != null && catalogTagRels.size() > 0)
        {
            Collection<ResolveCatalogNodeTagRelVO> vos = new ArrayList<ResolveCatalogNodeTagRelVO>();
            for (ResolveCatalogNodeTagRel model : catalogTagRels)
            {
                vos.add(model.doGetVO());
            }
            vo.setCatalogTagRels(vos);
        }
        
        Collection<ResolveCatalogEdge> catalogEdges = Hibernate.isInitialized(this.catalogEdges) ? getCatalogEdges() : null;
        if(catalogEdges != null && catalogEdges.size() > 0)
        {
            Collection<ResolveCatalogEdgeVO> vos = new ArrayList<ResolveCatalogEdgeVO>();
            for (ResolveCatalogEdge model : catalogEdges)
            {
                vos.add(model.doGetVO());
            }
            vo.setCatalogEdges(vos);
        }
        
        
        return vo;
    }
    
    @Override
    public void applyVOToModel(ResolveCatalogNodeVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);  
        
            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUNamespace(StringUtils.isNotBlank(vo.getUNamespace()) && vo.getUNamespace().equals(VO.STRING_DEFAULT) ? getUNamespace() : vo.getUNamespace());
            this.setUType(StringUtils.isNotBlank(vo.getUType()) && vo.getUType().equals(VO.STRING_DEFAULT) ? getUType() : vo.getUType());
            this.setUOperation(StringUtils.isNotBlank(vo.getUOperation()) && vo.getUOperation().equals(VO.STRING_DEFAULT) ? getUOperation() : vo.getUOperation());
            
//            this.setUOrder(vo.getUOrder() != null && vo.getUOrder().equals(VO.INTEGER_DEFAULT) ? getUOrder() : vo.getUOrder());
            
            this.setURoles(StringUtils.isNotBlank(vo.getURoles()) && vo.getURoles().equals(VO.STRING_DEFAULT) ? getURoles() : vo.getURoles());
            this.setUEditRoles(StringUtils.isNotBlank(vo.getUEditRoles()) && vo.getUEditRoles().equals(VO.STRING_DEFAULT) ? getUEditRoles() : vo.getUEditRoles());
            
            this.setUTitle(StringUtils.isNotBlank(vo.getUTitle()) && vo.getUTitle().equals(VO.STRING_DEFAULT) ? getUTitle() : vo.getUTitle());
            this.setUDescription(StringUtils.isNotBlank(vo.getUDescription()) && vo.getUDescription().equals(VO.STRING_DEFAULT) ? getUDescription() : vo.getUDescription());
            this.setUImage(StringUtils.isNotBlank(vo.getUImage()) && vo.getUImage().equals(VO.STRING_DEFAULT) ? getUImage() : vo.getUImage());
            this.setUImageName(StringUtils.isNotBlank(vo.getUImageName()) && vo.getUImageName().equals(VO.STRING_DEFAULT) ? getUImageName() : vo.getUImageName());
            this.setUTooltip(StringUtils.isNotBlank(vo.getUTooltip()) && vo.getUTooltip().equals(VO.STRING_DEFAULT) ? getUTooltip() : vo.getUTooltip());
            this.setUWiki(StringUtils.isNotBlank(vo.getUWiki()) && vo.getUWiki().equals(VO.STRING_DEFAULT) ? getUWiki() : vo.getUWiki());
            this.setULink(StringUtils.isNotBlank(vo.getULink()) && vo.getULink().equals(VO.STRING_DEFAULT) ? getULink() : vo.getULink());
            this.setUForm(StringUtils.isNotBlank(vo.getUForm()) && vo.getUForm().equals(VO.STRING_DEFAULT) ? getUForm() : vo.getUForm());
            this.setUDisplayType(StringUtils.isNotBlank(vo.getUDisplayType()) && vo.getUDisplayType().equals(VO.STRING_DEFAULT) ? getUDisplayType() : vo.getUDisplayType());
            this.setUMaxImageWidth(vo.getUMaxImageWidth() != null && vo.getUMaxImageWidth().equals(VO.INTEGER_DEFAULT) ? getUMaxImageWidth() : vo.getUMaxImageWidth());
            this.setUPath(StringUtils.isNotBlank(vo.getUPath()) && vo.getUPath().equals(VO.STRING_DEFAULT) ? getUPath() : vo.getUPath());
            this.setUOpenInNewTab(vo.getUOpenInNewTab() != null ? vo.getUOpenInNewTab() : getUOpenInNewTab());
            this.setUSideWidth(vo.getUSideWidth() != null && vo.getUSideWidth().equals(VO.INTEGER_DEFAULT) ? getUSideWidth() : vo.getUSideWidth());
            
            this.setUCatalogType(StringUtils.isNotBlank(vo.getUCatalogType()) && vo.getUCatalogType().equals(VO.STRING_DEFAULT) ? getUCatalogType() : vo.getUCatalogType());
            
            this.setUWikidocSysID(StringUtils.isNotBlank(vo.getUWikidocSysID()) && vo.getUWikidocSysID().equals(VO.STRING_DEFAULT) ? getUWikidocSysID() : vo.getUWikidocSysID());
            
            this.setUIsRoot(vo.getUIsRoot() != null ? vo.getUIsRoot() : getUIsRoot());
            this.setUIsRootRef(vo.getUIsRootRef() != null ? vo.getUIsRootRef() : getUIsRootRef());
            
            this.setUInternalName(StringUtils.isNotBlank(vo.getUInternalName()) && vo.getUInternalName().equals(VO.STRING_DEFAULT) ? getUInternalName() : vo.getUInternalName());
            this.setUIcon(StringUtils.isNotBlank(vo.getUIcon()) && vo.getUIcon().equals(VO.STRING_DEFAULT) ? getUIcon() : vo.getUIcon());
            
            this.setUReferenceCatalogSysId(StringUtils.isNotBlank(vo.getUReferenceCatalogSysId()) && vo.getUReferenceCatalogSysId().equals(VO.STRING_DEFAULT) ? getUReferenceCatalogSysId() : vo.getUReferenceCatalogSysId());
            
            //references
            Collection<ResolveCatalogNodeWikidocRelVO> catalogWikiRelsVOs = vo.getCatalogWikiRels();
            if (catalogWikiRelsVOs != null && catalogWikiRelsVOs.size() > 0)
            {
                Collection<ResolveCatalogNodeWikidocRel> models = new ArrayList<ResolveCatalogNodeWikidocRel>();
                for (ResolveCatalogNodeWikidocRelVO refVO : catalogWikiRelsVOs)
                {
                    models.add(new ResolveCatalogNodeWikidocRel(refVO));
                }
                this.setCatalogWikiRels(models);
            }
            
            Collection<ResolveCatalogNodeTagRelVO> catalogTagRelsVOs = vo.getCatalogTagRels();
            if (catalogTagRelsVOs != null && catalogTagRelsVOs.size() > 0)
            {
                Collection<ResolveCatalogNodeTagRel> models = new ArrayList<ResolveCatalogNodeTagRel>();
                for (ResolveCatalogNodeTagRelVO refVO : catalogTagRelsVOs)
                {
                    models.add(new ResolveCatalogNodeTagRel(refVO));
                }
                this.setCatalogTagRels(models);
            }
            
            Collection<ResolveCatalogEdgeVO> catalogEdgesVOs = vo.getCatalogEdges();
            if(catalogEdgesVOs != null && catalogEdgesVOs.size() > 0)
            {
                Collection<ResolveCatalogEdge> models = new ArrayList<ResolveCatalogEdge>();
                for (ResolveCatalogEdgeVO refVO : catalogEdgesVOs)
                {
                    models.add(new ResolveCatalogEdge(refVO));
                }
                this.setCatalogEdges(models);
            }
        }
    }
    

}
