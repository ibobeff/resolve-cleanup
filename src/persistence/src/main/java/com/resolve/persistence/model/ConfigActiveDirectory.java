/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;


import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;

import com.resolve.services.hibernate.vo.ConfigActiveDirectoryVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;


@Entity
@Table(name = "config_activedirectory", indexes = {@Index(columnList = "u_org", name = "cad_u_org_idx")})
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ConfigActiveDirectory  extends BaseModel<ConfigActiveDirectoryVO>
{
    public final static String AUTH_MODE_BIND = "BIND";            // uses CN=username,...
    public final static String AUTH_MODE_USERNAME = "USERNAME";    // uses <uidAttribute>=username, no password validation
    
    private static final long serialVersionUID = -6238205190763750442L;
    
    private Boolean UActive;
    private Integer UVersion;
    private String UIpAddress;
    private Integer UPort;
    private Boolean USsl;
    private String UDefaultDomain;    
    private String UUidAttribute;
    private String UMode;
    private String UBindDn;
    private String UBindPassword;
    private Boolean UFallback;
    private Boolean groupRequired;
    private String UDomain;// this is mandatory from the UI and should be unique
    private String UBaseDNList;//semi-colon separated string
    private String UProperties;//data that is in the ldap.properties file
    private Boolean UIsDefault;
    private String UGateway;
    
    //active directory conf belongs to this organization
    private Organization belongsToOrganization;


    // object references

    // object referenced by

    public ConfigActiveDirectory()
    {
    }
    
    public ConfigActiveDirectory(ConfigActiveDirectoryVO vo)
    {
        applyVOToModel(vo);
    }
    
    @Column(name = "u_port")
    public Integer getUPort()
    {
        return UPort;
    }

    public void setUPort(Integer uPort)
    {
        UPort = uPort;
    }

    @Column(name = "u_is_active", length = 1)
    @Type(type = "yes_no")
    public Boolean getUActive()
    {
        return UActive;
    }
    
    public Boolean ugetUActive()
    {
        Boolean result = false;
        if(getUActive() != null)
        {
            result = getUActive();
        }
        
        return result;
    }

    public void setUActive(Boolean uActive)
    {
        UActive = uActive;
    }

    @Column(name = "u_domain", length = 300)
    public String getUDomain()
    {
        return UDomain;
    }

    public void setUDomain(String uDomain)
    {
        UDomain = uDomain;
    }

    @Column(name = "u_ip_address", length = 50)
    public String getUIpAddress()
    {
        return UIpAddress;
    }

    public void setUIpAddress(String uIpAddress)
    {
        UIpAddress = uIpAddress;
    }
    
    @Column(name = "u_ssl", length = 1)
    @Type(type = "yes_no")
    public Boolean getUSsl()
    {
        return USsl;
    }
    
    public Boolean ugetUSsl()
    {
        Boolean result = false;
        if(getUSsl() != null)
        {
            result = getUSsl();
        }
        
        return result;
    }

    public void setUSsl(Boolean uSsl)
    {
        USsl = uSsl;
    }

    @Column(name = "u_version")
    public Integer getUVersion()
    {
        return UVersion;
    }

    public void setUVersion(Integer uVersion)
    {
        UVersion = uVersion;
    }

    @Column(name = "u_fallback",length = 1)
    @Type(type = "yes_no")
    public Boolean getUFallback()
    {
        return UFallback;
    }
    
    public Boolean ugetUFallback()
    {
        Boolean result = true;
        if(getUFallback() != null)
        {
            result = getUFallback();
        }
        
        return result;
    }

    public void setUFallback(Boolean uFallback)
    {
        UFallback = uFallback;
    }
    
    @Column(name = "u_mode", length = 100)
    public String getUMode()
    {
        return UMode;
    }

    public void setUMode(String uMode)
    {
        UMode = uMode;
    }
    
    @Column(name = "u_bind_dn", length = 1000)
    public String getUBindDn()
    {
        return UBindDn;
    }

    public void setUBindDn(String uBindDn)
    {
        UBindDn = uBindDn;
    }

    @Column(name = "u_bind_password", length = 100)
    public String getUBindPassword()
    {
        return UBindPassword;
    }

    public void setUBindPassword(String uBindPassword)
    {
        UBindPassword = uBindPassword;
    }

    @Column(name = "u_base_dn_list", length = 4000)
    public String getUBaseDNList()
    {
        return UBaseDNList;
    }

    public void setUBaseDNList(String uBaseDNList)
    {
        UBaseDNList = uBaseDNList;
    }
    
    @Column(name = "u_uid_attribute", length = 100)
    public String getUUidAttribute()
    {
        return UUidAttribute;
    }

    public void setUUidAttribute(String uUidAttribute)
    {
        UUidAttribute = uUidAttribute;
    }
    
    @Column(name = "u_default_domain", length = 300)
    public String getUDefaultDomain()
    {
        return UDefaultDomain;
    }

    public void setUDefaultDomain(String uDefaultDomain)
    {
        UDefaultDomain = uDefaultDomain;
    }
    
    @Lob
    @Column(name = "u_properties", length = 16777215)
    public String getUProperties()
    {
        return UProperties;
    }

    public void setUProperties(String uLdapProperties)
    {
        UProperties = uLdapProperties;
    }

    @Column(name = "u_is_default", length = 1)
    @Type(type = "yes_no")
    public Boolean getUIsDefault()
    {
        return UIsDefault;
    }

    public Boolean ugetUIsDefault()
    {
        Boolean result = false;
        if(getUIsDefault() != null)
        {
            result = getUIsDefault();
        }
        
        return result;
    }
    
    public void setUIsDefault(Boolean uIsDefault)
    {
        UIsDefault = uIsDefault;
    }
    
    @Column(name = "u_gateway", length = 40)
    public String getUGateway()
    {
        return UGateway;
    }

    public void setUGateway(String uGateway)
    {
        UGateway = uGateway;
    }

    @Column(name = "groupRequired", length = 1)
    @Type(type = "yes_no")
    public Boolean getGroupRequired()
    {
        return groupRequired;
    }

    public void setGroupRequired(Boolean groupRequired)
    {
        this.groupRequired = groupRequired;
    }
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_org", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public Organization getBelongsToOrganization()
    {
        return belongsToOrganization;
    }

    public void setBelongsToOrganization(Organization belongsToOrganization)
    {
        this.belongsToOrganization = belongsToOrganization;
        if (belongsToOrganization != null)
        {
            Collection<ConfigActiveDirectory> coll = belongsToOrganization.getConfigActiveDirectories();
            if (coll == null)
            {
                coll = new HashSet<ConfigActiveDirectory>();
                coll.add(this);
                
                belongsToOrganization.setConfigActiveDirectories(coll);
            }
        }
    }


    @Override
    public ConfigActiveDirectoryVO doGetVO()
    {
        ConfigActiveDirectoryVO vo = new ConfigActiveDirectoryVO();
        super.doGetBaseVO(vo);
        
        vo.setUActive(getUActive());
        vo.setUBaseDNList(getUBaseDNList());
        vo.setUBindDn(getUBindDn());
        vo.setUBindPassword(getUBindPassword());
        vo.setUDomain(getUDomain());
        vo.setUDefaultDomain(getUDefaultDomain());
        vo.setUFallback(getUFallback());
        vo.setUIpAddress(getUIpAddress());
        vo.setUMode(getUMode());
        vo.setUPort(getUPort());
        vo.setUSsl(getUSsl());
        vo.setGroupRequired(getGroupRequired());
        vo.setUUidAttribute(getUUidAttribute());
        vo.setUVersion(getUVersion());
        vo.setUProperties(getUProperties());
        vo.setUIsDefault(getUIsDefault());
        vo.setUGateway(getUGateway());
        vo.setBelongsToOrganization(Hibernate.isInitialized(this.belongsToOrganization) && getBelongsToOrganization() != null ? getBelongsToOrganization().doGetVO() : null);
        return vo;
    }

    @Override
    public void applyVOToModel(ConfigActiveDirectoryVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);            
            
            this.setUActive(vo.getUActive() != null ? vo.getUActive() : getUActive());
            this.setUBaseDNList(StringUtils.isNotBlank(vo.getUBaseDNList()) && vo.getUBaseDNList().equals(VO.STRING_DEFAULT) ? getUBaseDNList() : vo.getUBaseDNList());
            this.setUBindDn(StringUtils.isNotBlank(vo.getUBindDn()) && vo.getUBindDn().equals(VO.STRING_DEFAULT) ? getUBindDn() : vo.getUBindDn());
            this.setUBindPassword(StringUtils.isNotBlank(vo.getUBindPassword()) && vo.getUBindPassword().equals(VO.STRING_DEFAULT) ? getUBindPassword() : vo.getUBindPassword());
            this.setUDomain(StringUtils.isNotBlank(vo.getUDomain()) && vo.getUDomain().equals(VO.STRING_DEFAULT) ? getUDomain() : vo.getUDomain());
            this.setUDefaultDomain(StringUtils.isNotBlank(vo.getUDefaultDomain()) && vo.getUDefaultDomain().equals(VO.STRING_DEFAULT) ? getUDefaultDomain() : vo.getUDefaultDomain());
            this.setUFallback(vo.getUFallback() != null ? vo.getUFallback() : getUFallback());
            this.setUIpAddress(StringUtils.isNotBlank(vo.getUIpAddress()) && vo.getUIpAddress().equals(VO.STRING_DEFAULT) ? getUIpAddress() : vo.getUIpAddress());
            this.setUMode(StringUtils.isNotBlank(vo.getUMode()) && vo.getUMode().equals(VO.STRING_DEFAULT) ? getUMode() : vo.getUMode());
            this.setUPort(vo.getUPort() != null && vo.getUPort().equals(VO.INTEGER_DEFAULT) ? getUPort() : vo.getUPort());
            this.setUSsl(vo.getUSsl() != null ? vo.getUSsl() : getUSsl());
            this.setGroupRequired(vo.getGroupRequired() != null ? vo.getGroupRequired() : getGroupRequired());
            this.setUUidAttribute(StringUtils.isNotBlank(vo.getUUidAttribute()) && vo.getUUidAttribute().equals(VO.STRING_DEFAULT) ? getUUidAttribute() : vo.getUUidAttribute());
            this.setUVersion(vo.getUVersion() != null && vo.getUVersion().equals(VO.INTEGER_DEFAULT) ? getUVersion() : vo.getUVersion());
            this.setUProperties(StringUtils.isNotBlank(vo.getUProperties()) && vo.getUProperties().equals(VO.STRING_DEFAULT) ? getUProperties() : vo.getUProperties());
            this.setUIsDefault(vo.getUIsDefault() != null ? vo.getUIsDefault() : getUIsDefault());
            this.setUGateway(StringUtils.isNotBlank(vo.getUGateway()) && vo.getUGateway().equals(VO.STRING_DEFAULT) ? getUGateway() : vo.getUGateway());
            this.setBelongsToOrganization(vo.getBelongsToOrganization() != null ? new Organization(vo.getBelongsToOrganization()) : getBelongsToOrganization());
        }
        
    }
}
