/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.ResolveMCPComponentVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "resolve_mcp_component", uniqueConstraints = {@UniqueConstraint(columnNames = {"u_componentname", "u_host_id"}, name = "rmcpcmp_cmpname_hostid_uk")},
        indexes = {@Index(columnList = "u_host_id", name = "rmc_host_id_idx")})
public class ResolveMCPComponent extends BaseModel<ResolveMCPComponentVO>
{
    private static final long serialVersionUID = 4546485096390253379L;
    
    // Name of the component. RSView, RSRemote etc.
    private String UComponentName;
    // Component GUID
    private String UGuid;
    
    // The host to which this component belongs
    private ResolveMCPHost UComponentHost;
    
    
    public ResolveMCPComponent()
    {
    }
    
    public ResolveMCPComponent(ResolveMCPComponentVO vo)
    {
        applyVOToModel(vo);
    }
    
    @Column(name = "u_componentname")
    public String getUComponentName()
    {
        return UComponentName;
    }

    public void setUComponentName(String uComponentName)
    {
        UComponentName = uComponentName;
    }

    @Column(name = "u_guid")
    public String getUGuid()
    {
        return UGuid;
    }

    public void setUGuid(String uGuid)
    {
        UGuid = uGuid;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_host_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveMCPHost getUComponentHost()
    {
        return UComponentHost;
    }

    public void setUComponentHost(ResolveMCPHost uComponentHost)
    {
        UComponentHost = uComponentHost;
    }

    @Override
    public ResolveMCPComponentVO doGetVO()
    {
        ResolveMCPComponentVO vo = new ResolveMCPComponentVO();
        super.doGetBaseVO(vo);
        
        vo.setUComponentName(getUComponentName());
        vo.setUGuid(getUGuid());
        //vo.setUComponentHostVO(Hibernate.isInitialized(getUComponentHost()) && getUComponentHost() != null ? getUComponentHost().doGetVO() : null);
        
        return vo;
    }
    
    @Override
    public void applyVOToModel(ResolveMCPComponentVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUComponentHost(vo.getUComponentHostVO() != null ? new ResolveMCPHost(vo.getUComponentHostVO()) : null);
            this.setUComponentName(StringUtils.isNotBlank(vo.getUComponentName()) && vo.getUComponentName().equals(VO.STRING_DEFAULT) ? getUComponentName() : vo.getUComponentName());
            this.setUGuid(StringUtils.isNotBlank(vo.getUGuid()) && vo.getUGuid().equals(VO.STRING_DEFAULT) ? getUGuid() : vo.getUGuid());
        }
    }
}
