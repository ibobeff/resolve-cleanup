package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.PullGatewayPropertiesAttrVO;
import com.resolve.services.hibernate.vo.PullGatewayPropertiesVO;
import com.resolve.services.interfaces.VO;

@Entity
@Table(name = "pull_gateway_properties")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PullGatewayProperties extends GatewayProperties<PullGatewayPropertiesVO> {

    private static final long serialVersionUID = -3874231554626679007L;
    private Boolean UPoll;
    private Integer UPollingInterval;

    // object referenced by
    protected Collection<PullGatewayPropertiesAttr> attrs;
    
    public PullGatewayProperties() {
    }

    public PullGatewayProperties(PullGatewayPropertiesVO vo) {
        applyVOToModel(vo);
    }

    @Column(name="u_poll")
    public Boolean getUPoll()
    {
        return UPoll;
    }

    public void setUPoll(Boolean uPoll)
    {
        UPoll = uPoll;
    }
    
    @Column(name="u_polling_interval")
    public Integer getUPollingInterval()
    {
        return UPollingInterval;
    }

    public void setUPollingInterval(Integer uPollingInterval)
    {
        UPollingInterval = uPollingInterval;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "pullGatewayProperties")
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<PullGatewayPropertiesAttr> getAttrs()
    {
        return attrs;
    }

    public void setAttrs(Collection<PullGatewayPropertiesAttr> attrs)
    {
        this.attrs = attrs;
    }
    
    @Override 
    public PullGatewayPropertiesVO doGetVO() {
        
        PullGatewayPropertiesVO vo = new PullGatewayPropertiesVO();
        
        super.doGetBaseVO(vo);
        
        vo.setUPoll(getUPoll());
        vo.setUPollingInterval(getUPollingInterval());
        
        //references
        Collection<PullGatewayPropertiesAttr> attrModels = Hibernate.isInitialized(this.attrs) ? getAttrs() : null;
        if(attrModels != null && attrModels.size() > 0)
        {
            Collection<PullGatewayPropertiesAttrVO> vos = new ArrayList<PullGatewayPropertiesAttrVO>();

            for(PullGatewayPropertiesAttr model : attrs)
                vos.add(model.doGetVO());
            
            vo.setAttrs(vos);
        }       
        
        return vo;
    }

    @Override
    public void applyVOToModel(PullGatewayPropertiesVO vo) {
        
        if (vo == null) return;
        
        super.applyVOToModel(vo);

        if (!VO.BOOLEAN_DEFAULT.equals(vo.getUPoll())) this.setUPoll(vo.getUPoll()); else ;
        if (!VO.INTEGER_DEFAULT.equals(vo.getUPollingInterval())) this.setUPollingInterval(vo.getUPollingInterval()); else ;

    }

} // class PullGatewayProperties
