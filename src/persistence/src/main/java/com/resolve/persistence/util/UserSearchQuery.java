/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

public class UserSearchQuery
{
    long timestamp;
    String query;
    
    public UserSearchQuery(long t, String s)
    {
        timestamp = t;
        query = s;
    }
    
    public String getQuery()
    {
        return this.query;
    }
    
    public long getTimeStamp()
    {
        return timestamp;
    }
}
