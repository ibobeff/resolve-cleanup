/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.SRBTaskParameterVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
@Entity
@Table(name = "srb_task_parameter")
public class SRBTaskParameter extends BaseModel<SRBTaskParameterVO>
{
    private String variable;
    private String sourceName;
    private String source;
    
    private RBTask task;
    
    public SRBTaskParameter()
    {
    }

    public SRBTaskParameter(SRBTaskParameterVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_variable", length = 40)
    public String getVariable()
    {
        return variable;
    }

    public void setVariable(String variable)
    {
        this.variable = variable;
    }

    @Column(name = "u_source_name", length = 40)
    public String getSourceName()
    {
        return sourceName;
    }

    public void setSourceName(String sourceName)
    {
        this.sourceName = sourceName;
    }

    @Column(name = "u_source", length = 40)
    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_task_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public RBTask getTask()
    {
        return this.task;
    }

    public void setTask(RBTask task)
    {
        this.task = task;
    }

    @Override
    public SRBTaskParameterVO doGetVO()
    {
        SRBTaskParameterVO vo = new SRBTaskParameterVO();
        super.doGetBaseVO(vo);

        vo.setVariable(getVariable());
        vo.setSourceName(getSourceName());
        vo.setSource(getSource());

        return vo;
    }

    @Override
    public void applyVOToModel(SRBTaskParameterVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            this.setVariable(StringUtils.isNotBlank(vo.getVariable()) && vo.getVariable().equals(VO.STRING_DEFAULT) ? getVariable() : vo.getVariable());
            this.setSourceName(StringUtils.isNotBlank(vo.getSourceName()) && vo.getSourceName().equals(VO.STRING_DEFAULT) ? getSourceName() : vo.getSourceName());
            this.setSource(StringUtils.isNotBlank(vo.getSource()) && vo.getSource().equals(VO.STRING_DEFAULT) ? getSource() : vo.getSource());
            //references
            this.setTask(vo.getTask() != null ? new RBTask(vo.getTask()) : null);
        }
    }
}
