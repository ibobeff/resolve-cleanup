/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.util;

import org.dom4j.Element;

public class PropertyDefinition extends ColumnDefinition
{
    private String type;
    private int length=-1;

    //If no property name, by default column name is used as property name.
    public PropertyDefinition(String columnName, String propertyType )
    {
        name = columnName;
        column = columnName;
        type = propertyType;
    }
    
    public PropertyDefinition(String propertyName, String columnName, String propertyType )
    {
        name = propertyName;
        column = columnName;
        type = propertyType;
    }

    public PropertyDefinition(String columnName, String propertyType, int length )
    {
        name = columnName;
        column = columnName;
        type = propertyType;
        this.length=length;
    }
    
    public PropertyDefinition(String propertyName, String columnName, String propertyType, int length )
    {
        name = propertyName;
        column = columnName;
        type = propertyType;
        this.length=length;
    }
    
    
    public String getType()
    {
        return type;
    }

    public void  setType(String t)
    {
       type = t;
    }

    public int getLength()
    {
        return length;
    }

    public void  setLength(int length)
    {
       this.length=length;
    }
    
    
    @Override
    public Element addColumn(Element entity)
    {
        Element propElem = entity.addElement("property");
        propElem.addAttribute("name", getName());
        propElem.addAttribute("type", getType());
        propElem.addAttribute("column", getColumn());
        
        if (length>0)
        {
            propElem.addAttribute("length", ""+length);
            
        }
        
        return propElem;
    }
    
    
}
