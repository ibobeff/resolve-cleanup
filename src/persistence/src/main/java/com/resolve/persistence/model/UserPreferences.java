/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.UserPreferencesVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "user_preferences", indexes = {@Index(columnList = "u_user_sys_id", name = "sp_u_wikidoc_sys_id_idx")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserPreferences extends BaseModel<UserPreferencesVO>
{
    private static final long serialVersionUID = 116015956962950284L;
    
    private String UPrefGroup;
    private String UPrefKey;
    private String UPrefValue;
    private String UPrefDescription;
    
    private Users user;
    
    public UserPreferences() {}
    
    public UserPreferences(UserPreferencesVO vo)
    {
        applyVOToModel(vo);
    }


    
    @Column(name = "u_pref_group", length = 40)
    public String getUPrefGroup()
    {
        return UPrefGroup;
    }

    public void setUPrefGroup(String prefGroup)
    {
        this.UPrefGroup = prefGroup;
    }

    @Column(name = "u_pref_desc", length = 200)
    public String getUPrefDescription()
    {
        return UPrefDescription;
    }

    public void setUPrefDescription(String prefDescription)
    {
        this.UPrefDescription = prefDescription;
    }

    @Column(name = "u_pref_value", length = 200)
    public String getUPrefValue()
    {
        return UPrefValue;
    }

    public void setUPrefValue(String prefValue)
    {
        this.UPrefValue = prefValue;
    }
    
    @Column( name = "u_pref_key", length = 100)
    public String getUPrefKey()
    {
        return UPrefKey;
    }
    
    public void setUPrefKey (String prefKey)
    {
        this.UPrefKey = prefKey;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_user_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public Users getUser()
    {
        return this.user;
    }

    public void setUser(Users user)
    {
        this.user = user;
    }

    @Override
    public UserPreferencesVO doGetVO()
    {
        UserPreferencesVO vo = new UserPreferencesVO();
        super.doGetBaseVO(vo);
        
        vo.setUPrefDescription(getUPrefDescription());
        vo.setUPrefGroup(getUPrefGroup());
        vo.setUPrefKey(getUPrefKey());
        vo.setUPrefValue(getUPrefValue());
        
        //references
//        vo.setUser(Hibernate.isInitialized(this.user) && getUser() != null ? getUser().doGetVO() : null);

        return vo;
    }

    @Override
    public void applyVOToModel(UserPreferencesVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUPrefDescription(StringUtils.isNotBlank(vo.getUPrefDescription()) && vo.getUPrefDescription().equals(VO.STRING_DEFAULT) ? getUPrefDescription() : vo.getUPrefDescription());
            this.setUPrefGroup(StringUtils.isNotBlank(vo.getUPrefGroup()) && vo.getUPrefGroup().equals(VO.STRING_DEFAULT) ? getUPrefGroup() : vo.getUPrefGroup());
            this.setUPrefKey(StringUtils.isNotBlank(vo.getUPrefKey()) && vo.getUPrefKey().equals(VO.STRING_DEFAULT) ? getUPrefKey() : vo.getUPrefKey());
            this.setUPrefValue(StringUtils.isNotBlank(vo.getUPrefValue()) && vo.getUPrefValue().equals(VO.STRING_DEFAULT) ? getUPrefValue() : vo.getUPrefValue());
            
            //references
            this.setUser(vo.getUser() != null ? new Users(vo.getUser()) : null);
        }
        
    }


}
