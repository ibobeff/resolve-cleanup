/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;

import com.resolve.services.hibernate.vo.MetaFieldPropertiesVO;
import com.resolve.services.hibernate.vo.MetaxFieldDependencyVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

@Entity
@Table(name = "meta_field_properties")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaFieldProperties extends BaseModel<MetaFieldPropertiesVO>
{
    private static final long serialVersionUID = 6751591655635705322L;
    
    private String UName;
    private String UDisplayName;//this we have to add so that the same field can be shown with different display name on different forms
    private String UTable;
    private String UUIType;// FOR GXT
    private Integer UOrder;
    private String UJavascript;
    private Integer USize;
    private Boolean UIsMandatory;
    private Boolean UIsCrypt;
    private String UDefaultValue;
    private Integer UWidth;
    private Integer UHeight;
    private String ULabelAlign;
    private Integer UStringMinLength;
    private Integer UStringMaxLength;
    private Integer UUIStringMaxLength;
    private Integer UBooleanMaxLength;
    private Boolean USelectIsMultiSelect;
    private Boolean USelectUserInput;
    private Integer USelectMaxDisplay;
    private String USelectValues;
    private String UChoiceValues;
    private String UCheckboxValues;
    private Boolean UDateTimeHasCalendar;
    private Integer UIntegerMinValue;
    private Integer UIntegerMaxValue;
    private Double UDecimalMinValue;
    private Double UDecimalMaxValue;
    private Integer UJournalRows;
    private Integer UJournalColumns;
    private Integer UJournalMinValue;
    private Integer UJournalMaxValue;
    private Integer UListRows;
    private Integer UListColumns;
    private Integer UListMaxDisplay;
    private String UListValues;

    private String UReferenceTable;
    private String UReferenceDisplayColumn;
    private String UReferenceDisplayColumnList;
    private String UReferenceTarget;
    private String UReferenceParams;

    private String ULinkTarget;
    private String ULinkParams;
    private String ULinkTemplate;
    private String USequencePrefix;
    private String UHiddenValue;

    private String UGroups;
    private String UUsersOfTeams;
    private Boolean URecurseUsersOfTeam;
    private String UTeamsOfTeams;
    private Boolean URecurseTeamsOfTeam;
    private Boolean UAutoAssignToMe;
    private Boolean UAllowAssignToMe;

    private String UTeamPickerTeamsOfTeams;

    private Boolean UIsHidden;
    private Boolean UIsReadOnly;

    private String UTooltip;

    // file upload widget
    private String UFileUploadTableName;
    private String UFileUploadReferenceColumnName;
    private Boolean UAllowUpload;
    private Boolean UAllowDownload;
    private Boolean UAllowRemove;
    private String UAllowFileTypes;

    private Integer UWidgetColumns;

    // for Reference grid
    // select a, b, c from B where u_A_sys_id = 'ddddddd'
    private String URefGridReferenceByTable;// B
    private String URefGridSelectedReferenceColumns; // a, b, c
    private String URefGridReferenceColumnName; // u_A_sys_id
    private String UAllowReferenceTableAdd;
    private String UAllowReferenceTableRemove;
    private String UAllowReferenceTableView;
    private String UReferenceTableUrl;
    private String URefGridViewPopup;
    private String URefGridViewPopupWidth;
    private String URefGridViewPopupHeight;
    private String URefGridViewPopupTitle;

    // for choice additions
    private String UChoiceTable;
    private String UChoiceDisplayField;
    private String UChoiceValueField;
    private String UChoiceWhere;
    private String UChoiceField;
    private String UChoiceSql;
    private int UChoiceOptionSelection;
    
    // for spacer
    private Boolean UShowHorizontalRule;
    
    //for journal
    private Boolean UAllowJournalEdit;

    // For columns
    private int USectionNumberOfColumns;
    private String USectionType;
    private String USectionLayout;
    private String USectionTitle;
    private String USectionColumns;
    
    private Collection<MetaxFieldDependency> metaxFieldDependencys;
    
    private String UTransientFieldsJsonString;
    
    private volatile static List<Method> transientMethods = null;


    public MetaFieldProperties()
    {
    }
    
    public MetaFieldProperties(MetaFieldPropertiesVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_name", nullable = false, length = 100)
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    @Column(name = "u_display_name", length = 100)
    public String getUDisplayName()
    {
        return this.UDisplayName;
    }

    public void setUDisplayName(String UDisplayName)
    {
        this.UDisplayName = UDisplayName;
    }

    @Column(name = "u_table", length = 100)
    public String getUTable()
    {
        return this.UTable;
    }

    public void setUTable(String UTable)
    {
        this.UTable = UTable;
    }

    @Column(name = "u_ui_type", length = 100)
    public String getUUIType()
    {
        return this.UUIType;
    }

    public void setUUIType(String UUIType)
    {
        this.UUIType = UUIType;
    }

    @Column(name = "u_order")
    public Integer getUOrder()
    {
        return this.UOrder;
    }

    public Integer ugetUOrder()
    {
        if (UOrder == null)
            return new Integer(-1);
        
        return UOrder;
    }

    public void setUOrder(Integer UOrder)
    {
        this.UOrder = UOrder;
    }
    
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "u_javascript", length = 16777215)
    @Lob
    public String getUJavascript()
    {
        return this.UJavascript;
    }

    public void setUJavascript(String UJavascript)
    {
        this.UJavascript = UJavascript;
    }

    @Column(name = "u_size")
    public Integer getUSize()
    {
        return this.USize;
    }

    public Integer ugetUSize()
    {
        if (USize == null)
            return VO.NON_NEGATIVE_INTEGER_DEFAULT;

            return USize;
    }

    public void setUSize(Integer USize)
    {
        this.USize = USize;
    }

    @Column(name = "u_is_mandatory")
    public Boolean getUIsMandatory()
    {
        return this.UIsMandatory;
    }

    public Boolean ugetUIsMandatory()
    {
        if (this.UIsMandatory == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UIsMandatory;
        }
    }

    public void setUIsMandatory(Boolean UIsMandatory)
    {
        this.UIsMandatory = UIsMandatory;
    }

    @Column(name = "u_is_crypt")
    public Boolean getUIsCrypt()
    {
        return this.UIsCrypt;
    }

    public Boolean ugetUIsCrypt()
    {
        if (this.UIsCrypt == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UIsCrypt;
        }
    }

    public void setUIsCrypt(Boolean UIsCrypt)
    {
        this.UIsCrypt = UIsCrypt;
    }

    @Column(name = "u_default_value", length = 1000)
    public String getUDefaultValue()
    {
        return this.UDefaultValue;
    }

    public void setUDefaultValue(String UDefaultValue)
    {
        this.UDefaultValue = UDefaultValue;
    }

    @Column(name = "u_width")
    public Integer getUWidth()
    {
        return UWidth;
    }
    
    public Integer ugetUWidth()
    {
        if (UWidth == null)
            return VO.NON_NEGATIVE_INTEGER_DEFAULT;
        
        return UWidth;
    }
    
    public void setUWidth(Integer uWidth)
    {
        UWidth = uWidth;
    }

    @Column(name = "u_height")
    public Integer getUHeight()
    {
        return UHeight;
    }
    
    public Integer ugetUHeight()
    {
        if (UHeight == null)
            return VO.NON_NEGATIVE_INTEGER_DEFAULT;
        
        return UHeight;
    }
    
    public void setUHeight(Integer uHeight)
    {
        UHeight = uHeight;
    }

    @Column(name = "u_label_align", length = 100)
    public String getULabelAlign()
    {
        return ULabelAlign;
    }

    public void setULabelAlign(String uLabelAlign)
    {
        this.ULabelAlign = uLabelAlign;
    }

    @Column(name = "u_string_min_length")
    public Integer getUStringMinLength()
    {
        return UStringMinLength;
    }
    
    public Integer ugetUStringMinLength()
    {
        if (UStringMinLength == null)
            return VO.NON_NEGATIVE_INTEGER_DEFAULT;
        
        return UStringMinLength;
    }
    
    public void setUStringMinLength(Integer uStringMinLength)
    {
        this.UStringMinLength = uStringMinLength;
    }

    @Column(name = "u_string_max_length")
    public Integer getUStringMaxLength()
    {
        return this.UStringMaxLength;
    }

    public Integer ugetUStringMaxLength()
    {
        if (UStringMaxLength == null)
            return VO.NON_NEGATIVE_INTEGER_DEFAULT;
        
        return UStringMaxLength;
    }
    
    public void setUStringMaxLength(Integer UStringMaxLength)
    {
        this.UStringMaxLength = UStringMaxLength;
    }

    @Column(name = "u_ui_string_max_length")
    public Integer getUUIStringMaxLength()
    {
        return UUIStringMaxLength;
    }
    
    public Integer ugetUUIStringMaxLength()
    {
        if (UUIStringMaxLength == null)
            return VO.NON_NEGATIVE_INTEGER_DEFAULT;
        
        return UUIStringMaxLength;
    }
    
    public void setUUIStringMaxLength(Integer uUIStringMaxLength)
    {
        UUIStringMaxLength = uUIStringMaxLength;
    }

    @Column(name = "u_boolean_max_length")
    public Integer getUBooleanMaxLength()
    {
        return this.UBooleanMaxLength;
    }
    
    public Integer ugetUBooleanMaxLength()
    {
        if (UBooleanMaxLength == null)
            return new Integer(0);
        
        return UBooleanMaxLength;
    }
    
    public void setUBooleanMaxLength(Integer UBooleanMaxLength)
    {
        this.UBooleanMaxLength = UBooleanMaxLength;
    }

    @Column(name = "u_select_is_multi_select", length = 1)
    @Type(type = "yes_no")
    public Boolean getUSelectIsMultiSelect()
    {
        return this.USelectIsMultiSelect;
    }

    public Boolean ugetUSelectIsMultiSelect()
    {
        if (this.USelectIsMultiSelect == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.USelectIsMultiSelect;
        }
    }

    public void setUSelectIsMultiSelect(Boolean USelectIsMultiSelect)
    {
        this.USelectIsMultiSelect = USelectIsMultiSelect;
    }

    @Column(name = "u_select_user_input", length = 1)
    @Type(type = "yes_no")
    public Boolean getUSelectUserInput()
    {
        return USelectUserInput;
    }

    public Boolean ugetUSelectUserInput()
    {
        if (this.USelectUserInput == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.USelectUserInput;
        }
    }

    public void setUSelectUserInput(Boolean uSelectUserInput)
    {
        USelectUserInput = uSelectUserInput;
    }

    @Column(name = "u_select_max_display")
    public Integer getUSelectMaxDisplay()
    {
        return this.USelectMaxDisplay;
    }

    public Integer ugetUSelectMaxDisplay()
    {
        if (this.USelectMaxDisplay != null)
        {
            return this.USelectMaxDisplay;
        }
        else
        {
            return 0;
        }
    }

    public void setUSelectMaxDisplay(Integer USelectMaxDisplay)
    {
        this.USelectMaxDisplay = USelectMaxDisplay;
    }
    
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "u_select_values", length = 16777215)
    @Lob
    public String getUSelectValues()
    {
        return this.USelectValues;
    }
    
    public void setUSelectValues(String USelectValues)
    {
        this.USelectValues = USelectValues;
    }
    
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "u_choice_values", length = 16777215)
    @Lob
    public String getUChoiceValues()
    {
        return this.UChoiceValues;
    }
    
    public void setUChoiceValues(String UChoiceValues)
    {
        this.UChoiceValues = UChoiceValues;
    }
    
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "u_checkbox_values", length = 16777215)
    @Lob
    public String getUCheckboxValues()
    {
        return this.UCheckboxValues;
    }
    
    public void setUCheckboxValues(String UCheckboxValues)
    {
        this.UCheckboxValues = UCheckboxValues;
    }

    @Column(name = "u_date_time_has_calendar")
    public Boolean getUDateTimeHasCalendar()
    {
        return this.UDateTimeHasCalendar;
    }

    public Boolean ugetUDateTimeHasCalendar()
    {
        if (this.UDateTimeHasCalendar == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UDateTimeHasCalendar;
        }
    }

    public void setUDateTimeHasCalendar(Boolean UDateTimeHasCalendar)
    {
        this.UDateTimeHasCalendar = UDateTimeHasCalendar;
    }

    @Column(name = "u_integer_min_value")
    public Integer getUIntegerMinValue()
    {
        return this.UIntegerMinValue;
    }

    public Integer ugetUIntegerMinValue()
    {
        if (UIntegerMinValue == null)
            return VO.NON_NEGATIVE_INTEGER_DEFAULT;
        
        return UIntegerMinValue;
    }

    public void setUIntegerMinValue(Integer UIntegerMinValue)
    {
        this.UIntegerMinValue = UIntegerMinValue;
    }

    @Column(name = "u_integer_max_value")
    public Integer getUIntegerMaxValue()
    {
        return this.UIntegerMaxValue;
    }

    public Integer ugetUIntegerMaxValue()
    {
        if (UIntegerMaxValue == null)
            return VO.NON_NEGATIVE_INTEGER_DEFAULT;
        
        return UIntegerMaxValue;
    }

    public void setUIntegerMaxValue(Integer UIntegerMaxValue)
    {
        this.UIntegerMaxValue = UIntegerMaxValue;
    }

    @Column(name = "u_decimal_min_value")
    public Double getUDecimalMinValue()
    {
        return this.UDecimalMinValue;
    }

    public Double ugetUDecimalMinValue()
    {
        if (UDecimalMinValue == null)
            return VO.DOUBLE_DEFAULT;
        
        return this.UDecimalMinValue;
    }

    public void setUDecimalMinValue(Double UDecimalMinValue)
    {
        this.UDecimalMinValue = UDecimalMinValue;
    }

    @Column(name = "u_decimal_max_value")
    public Double getUDecimalMaxValue()
    {
        return this.UDecimalMaxValue;
    }

    public Double ugetUDecimalMaxValue()
    {
        if (UDecimalMaxValue == null)
            return VO.DOUBLE_DEFAULT;
        
        return UDecimalMaxValue;
    }

    public void setUDecimalMaxValue(Double UDecimalMaxValue)
    {
        this.UDecimalMaxValue = UDecimalMaxValue;
    }

    @Column(name = "u_journal_rows")
    public Integer getUJournalRows()
    {
        return this.UJournalRows;
    }

    public Integer ugetUJournalRows()
    {
        if (UJournalRows == null)
            return VO.NON_NEGATIVE_INTEGER_DEFAULT;
        
        return UJournalRows;
    }

    public void setUJournalRows(Integer UJournalRows)
    {
        this.UJournalRows = UJournalRows;
    }

    @Column(name = "u_journal_columns")
    public Integer getUJournalColumns()
    {
        return this.UJournalColumns;
    }

    public Integer ugetUJournalColumns()
    {
        if (UJournalColumns == null)
            return VO.NON_NEGATIVE_INTEGER_DEFAULT;
        
        return UJournalColumns;
    }

    public void setUJournalColumns(Integer UJournalColumns)
    {
        this.UJournalColumns = UJournalColumns;
    }

    @Column(name = "u_journal_min_value")
    public Integer getUJournalMinValue()
    {
        return this.UJournalMinValue;
    }

    public Integer ugetUJournalMinValue()
    {
        if (UJournalMinValue == null)
            return VO.NON_NEGATIVE_INTEGER_DEFAULT;
        
        return UJournalMinValue;
    }

    @Column(name = "u_journal_min_value")
    public void setUJournalMinValue(Integer UJournalMinValue)
    {
        this.UJournalMinValue = UJournalMinValue;
    }

    @Column(name = "u_journal_max_value")
    public Integer getUJournalMaxValue()
    {
        return this.UJournalMaxValue;
    }

    public Integer ugetUJournalMaxValue()
    {
        if (UJournalMaxValue == null)
        {
            return this.UJournalMaxValue;
        }
        else
        {
            return 0;
        }
    }

    @Column(name = "u_journal_max_value")
    public void setUJournalMaxValue(Integer UJournalMaxValue)
    {
        this.UJournalMaxValue = UJournalMaxValue;
    }

    @Column(name = "u_list_rows")
    public Integer getUListRows()
    {
        return this.UListRows;
    }

    public Integer ugetUListRows()
    {
        if (UListRows == null)
            return VO.NON_NEGATIVE_INTEGER_DEFAULT;
        
        return UListRows;
    }

    public void setUListRows(Integer UListRows)
    {
        this.UListRows = UListRows;
    }

    @Column(name = "u_list_columns")
    public Integer getUListColumns()
    {
        return this.UListColumns;
    }

    public Integer ugetUListColumns()
    {
        if (this.UListColumns == null)
            return VO.NON_NEGATIVE_INTEGER_DEFAULT;
        
        return UListColumns;
    }

    public void setUListColumns(Integer UListColumns)
    {
        this.UListColumns = UListColumns;
    }

    @Column(name = "u_list_max_display")
    public Integer getUListMaxDisplay()
    {
        return this.UListMaxDisplay;
    }
    
    public Integer ugetUListMaxDisplay()
    {
        if (UListMaxDisplay == null)
            return VO.NON_NEGATIVE_INTEGER_DEFAULT;
        
        return UListMaxDisplay;
    }

    public void setUListMaxDisplay(Integer UListMaxDisplay)
    {
        this.UListMaxDisplay = UListMaxDisplay;
    }

    /**
     * @return the uListValues
     */
    @Column(name = "u_list_values", length = 1000)
    public String getUListValues()
    {
        return UListValues;
    }

    public void setUListValues(String uListValues)
    {
        UListValues = uListValues;
    }

    @Column(name = "u_reference_table", length = 100)
    public String getUReferenceTable()
    {
        return this.UReferenceTable;
    }

    public void setUReferenceTable(String UReferenceTable)
    {
        this.UReferenceTable = UReferenceTable;
    }

    @Column(name = "u_ref_display_column", length = 333)
    public String getUReferenceDisplayColumn()
    {
        return this.UReferenceDisplayColumn;
    }

    public void setUReferenceDisplayColumn(String UReferenceDisplayColumnList)
    {
        this.UReferenceDisplayColumn = UReferenceDisplayColumnList;
    }

    @Column(name = "u_ref_display_column_list", length = 1000)
    public String getUReferenceDisplayColumnList()
    {
        return this.UReferenceDisplayColumnList;
    }

    public void setUReferenceDisplayColumnList(String UReferenceDisplayColumnList)
    {
        this.UReferenceDisplayColumnList = UReferenceDisplayColumnList;
    }

    @Column(name = "u_reference_target", length = 1000)
    public String getUReferenceTarget()
    {
        return this.UReferenceTarget;
    }

    public void setUReferenceTarget(String UReferenceTarget)
    {
        this.UReferenceTarget = UReferenceTarget;
    }
    
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "u_reference_params", length = 16777215)
    @Lob
    public String getUReferenceParams()
    {
        return this.UReferenceParams;
    }
    
    public void setUReferenceParams(String UReferenceParams)
    {
        this.UReferenceParams = UReferenceParams;
    }
    
    @Column(name = "u_link_target", length = 1000)
    public String getULinkTarget()
    {
        return this.ULinkTarget;
    }

    public void setULinkTarget(String ULinkTarget)
    {
        this.ULinkTarget = ULinkTarget;
    }
    
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "u_link_params", length = 16777215)
    @Lob
    public String getULinkParams()
    {
        return this.ULinkParams;
    }
    
    public void setULinkParams(String ULinkParams)
    {
        this.ULinkParams = ULinkParams;
    }
    
    @Column(name = "u_link_template", length = 1000)
    public String getULinkTemplate()
    {
        return this.ULinkTemplate;
    }

    public void setULinkTemplate(String ULinkTemplate)
    {
        this.ULinkTemplate = ULinkTemplate;
    }

    @Column(name = "u_sequence_prefix", length = 40)
    public String getUSequencePrefix()
    {
        return this.USequencePrefix;
    }

    public void setUSequencePrefix(String USequencePrefix)
    {
        this.USequencePrefix = USequencePrefix;
    }

    @Column(name = "u_hidden_value", length = 333)
    public String getUHiddenValue()
    {
        return UHiddenValue;
    }

    public void setUHiddenValue(String uHiddenValue)
    {
        UHiddenValue = uHiddenValue;
    }

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "u_meta_style_sys_id")
//    @Index(name = "mfp_u_meta_style_sys_id_idx")
//    @NotFound(action = NotFoundAction.IGNORE)
//    public MetaStyle getMetaStyle()
//    {
//        return metaStyle;
//    }
//
//    public void setMetaStyle(MetaStyle metaStyle)
//    {
//        this.metaStyle = metaStyle;
//    }

    @Column(name = "u_groups", length = 500)
    public String getUGroups()
    {
        return UGroups;
    }

    public void setUGroups(String uGroups)
    {
        UGroups = uGroups;
    }

    @Column(name = "u_users_of_teams", length = 500)
    public String getUUsersOfTeams()
    {
        return UUsersOfTeams;
    }

    public void setUUsersOfTeams(String uUsersOfTeams)
    {
        UUsersOfTeams = uUsersOfTeams;
    }

    @Column(name = "u_recurse_users_of_team", length = 1)
    @Type(type = "yes_no")
    public Boolean getURecurseUsersOfTeam()
    {
        return URecurseUsersOfTeam;
    }

    public Boolean ugetURecurseUsersOfTeam()
    {
        if (URecurseUsersOfTeam == null)
            return VO.BOOLEAN_DEFAULT;
        
        return URecurseUsersOfTeam;
    }

    public void setURecurseUsersOfTeam(Boolean uRecurseUsersOfTeam)
    {
        URecurseUsersOfTeam = uRecurseUsersOfTeam;
    }

    @Column(name = "u_teams_of_teams", length = 500)
    public String getUTeamsOfTeams()
    {
        return UTeamsOfTeams;
    }

    public void setUTeamsOfTeams(String uTeamsOfTeams)
    {
        UTeamsOfTeams = uTeamsOfTeams;
    }

    @Column(name = "u_tp_teams_of_teams", length = 500)
    public String getUTeamPickerTeamsOfTeams()
    {
        return UTeamPickerTeamsOfTeams;
    }

    public void setUTeamPickerTeamsOfTeams(String uTeamPickerTeamsOfTeams)
    {
        UTeamPickerTeamsOfTeams = uTeamPickerTeamsOfTeams;
    }

    @Column(name = "u_recurse_teams_of_teams", length = 1)
    @Type(type = "yes_no")
    public Boolean getURecurseTeamsOfTeam()
    {
        return URecurseTeamsOfTeam;
    }

    public void setURecurseTeamsOfTeam(Boolean uRecurseTeamsOfTeam)
    {
        URecurseTeamsOfTeam = uRecurseTeamsOfTeam;
    }

    public Boolean ugetURecurseTeamsOfTeam()
    {
        if (URecurseTeamsOfTeam == null)
            return VO.BOOLEAN_DEFAULT;
        
        return URecurseTeamsOfTeam;
    }

    @Column(name = "u_is_hidden", length = 1)
    @Type(type = "yes_no")
    public Boolean getUIsHidden()
    {
        return UIsHidden;
    }

    public void setUIsHidden(Boolean uIsHidden)
    {
        UIsHidden = uIsHidden;
    }

    public Boolean ugetUIsHidden()
    {
        if (UIsHidden == null)
            return VO.BOOLEAN_DEFAULT;

        return UIsHidden;
    }

    @Column(name = "u_is_read_only", length = 1)
    @Type(type = "yes_no")
    public Boolean getUIsReadOnly()
    {
        return UIsReadOnly;
    }

    public void setUIsReadOnly(Boolean uIsReadOnly)
    {
        UIsReadOnly = uIsReadOnly;
    }

    public Boolean ugetUIsReadOnly()
    {
        if (this.UIsReadOnly == null)
            return VO.BOOLEAN_DEFAULT;
            
        return UIsReadOnly;
    }

    @Column(name = "u_auto_assign_to_me", length = 1)
    @Type(type = "yes_no")
    public Boolean getUAutoAssignToMe()
    {
        return UAutoAssignToMe;
    }

    public Boolean ugetUAutoAssignToMe()
    {
        if (UAutoAssignToMe == null)
            return VO.BOOLEAN_DEFAULT;
            
        return UAutoAssignToMe;
    }

    public void setUAutoAssignToMe(Boolean uAutoAssignToMe)
    {
        UAutoAssignToMe = uAutoAssignToMe;
    }

    @Column(name = "u_allow_assign_to_me", length = 1)
    @Type(type = "yes_no")
    public Boolean getUAllowAssignToMe()
    {
        return UAllowAssignToMe;
    }

    public Boolean ugetUAllowAssignToMe()
    {
        if (UAllowAssignToMe == null)
            return VO.BOOLEAN_DEFAULT;
        
        return UAllowAssignToMe;
    }

    public void setUAllowAssignToMe(Boolean uAllowAssignToMe)
    {
        UAllowAssignToMe = uAllowAssignToMe;
    }

    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "metaFieldProperties", fetch = FetchType.EAGER)
    @NotFound(action = NotFoundAction.IGNORE)
    @JsonIgnore
    public Collection<MetaxFieldDependency> getMetaxFieldDependencys()
    {
        return this.metaxFieldDependencys;
    }

    public void setMetaxFieldDependencys(Collection<MetaxFieldDependency> metaxFieldDependencys)
    {
        this.metaxFieldDependencys = metaxFieldDependencys;
    }

    @Column(name = "u_allow_upload", length = 1)
    @Type(type = "yes_no")
    public Boolean getUAllowUpload()
    {
        return UAllowUpload;
    }

    public Boolean ugetUAllowUpload()
    {
        if (UAllowUpload == null)
            return VO.BOOLEAN_DEFAULT;
            
        return UAllowUpload;
    }

    public void setUAllowUpload(Boolean uAllowUpload)
    {
        UAllowUpload = uAllowUpload;
    }

    @Column(name = "u_allow_download", length = 1)
    @Type(type = "yes_no")
    public Boolean getUAllowDownload()
    {
        return UAllowDownload;
    }

    public Boolean ugetUAllowDownload()
    {
        if (UAllowDownload == null)
            return VO.BOOLEAN_DEFAULT;
        
        return UAllowDownload;
    }

    public void setUAllowDownload(Boolean uAllowDownload)
    {
        UAllowDownload = uAllowDownload;
    }

    @Column(name = "u_allow_remove", length = 1)
    @Type(type = "yes_no")
    public Boolean getUAllowRemove()
    {
        return UAllowRemove;
    }

    public Boolean ugetUAllowRemove()
    {
        if (UAllowRemove == null)
            return VO.BOOLEAN_DEFAULT;
        
        return UAllowRemove;
    }

    public void setUAllowRemove(Boolean uAllowRemove)
    {
        UAllowRemove = uAllowRemove;
    }

    @Column(name = "u_allow_file_types", length = 200)
    public String getUAllowFileTypes()
    {
        return UAllowFileTypes;
    }

    public void setUAllowFileTypes(String uAllowFileTypes)
    {
        UAllowFileTypes = uAllowFileTypes;
    }

    @Column(name = "u_widget_columns")
    public Integer getUWidgetColumns()
    {
        return UWidgetColumns;
    }

    public Integer ugetUWidgetColumns()
    {
        if (UWidgetColumns == null)
            return VO.NON_NEGATIVE_INTEGER_DEFAULT;
        
        return UWidgetColumns;
    }
    
    public void setUWidgetColumns(Integer uWidgetColumns)
    {
        UWidgetColumns = uWidgetColumns;
    }

    @Column(name = "u_file_upload_table_name", length = 200)
    public String getUFileUploadTableName()
    {
        return UFileUploadTableName;
    }

    public void setUFileUploadTableName(String uFileUploadTableName)
    {
        UFileUploadTableName = uFileUploadTableName;
    }

    @Column(name = "u_file_upload_ref_col_name", length = 100)
    public String getUFileUploadReferenceColumnName()
    {
        return UFileUploadReferenceColumnName;
    }

    public void setUFileUploadReferenceColumnName(String uFileUploadReferenceColumnName)
    {
        UFileUploadReferenceColumnName = uFileUploadReferenceColumnName;
    }

    @Column(name = "u_ref_grid_ref_by_table", length = 333)
    public String getURefGridReferenceByTable()
    {
        return URefGridReferenceByTable;
    }

    public void setURefGridReferenceByTable(String uRefGridReferenceByTable)
    {
        URefGridReferenceByTable = uRefGridReferenceByTable;
    }

    @Lob
    @Column(name = "u_ref_grid_sel_ref_cols", length = 16777215)
    public String getURefGridSelectedReferenceColumns()
    {
        return URefGridSelectedReferenceColumns;
    }

    public void setURefGridSelectedReferenceColumns(String uRefGridSelectedReferenceColumns)
    {
        URefGridSelectedReferenceColumns = uRefGridSelectedReferenceColumns;
    }

    @Column(name = "u_ref_grid_ref_col_name", length = 333)
    public String getURefGridReferenceColumnName()
    {
        return URefGridReferenceColumnName;
    }

    public void setURefGridReferenceColumnName(String uRefGridReferenceColumnName)
    {
        URefGridReferenceColumnName = uRefGridReferenceColumnName;
    }

    @Column(name = "u_allow_ref_table_add", length = 333)
    public String getUAllowReferenceTableAdd()
    {
        return UAllowReferenceTableAdd;
    }

    public void setUAllowReferenceTableAdd(String uAllowReferenceTableAdd)
    {
        UAllowReferenceTableAdd = uAllowReferenceTableAdd;
    }

    @Column(name = "u_allow_ref_table_remove", length = 333)
    public String getUAllowReferenceTableRemove()
    {
        return UAllowReferenceTableRemove;
    }

    public void setUAllowReferenceTableRemove(String uAllowReferenceTableRemove)
    {
        UAllowReferenceTableRemove = uAllowReferenceTableRemove;
    }

    @Column(name = "u_allow_ref_table_view", length = 333)
    public String getUAllowReferenceTableView()
    {
        return UAllowReferenceTableView;
    }

    public void setUAllowReferenceTableView(String uAllowReferenceTableView)
    {
        UAllowReferenceTableView = uAllowReferenceTableView;
    }

    @Column(name = "u_ref_grid_table_url", length = 1000)
    public String getUReferenceTableUrl()
    {
        return UReferenceTableUrl;
    }

    public void setUReferenceTableUrl(String uReferenceTableUrl)
    {
        UReferenceTableUrl = uReferenceTableUrl;
    }

    @Column(name = "u_ref_grid_view_popup", length = 100)
    public String getURefGridViewPopup()
    {
        return URefGridViewPopup;
    }

    public void setURefGridViewPopup(String uRefGridViewPopup)
    {
        URefGridViewPopup = uRefGridViewPopup;
    }

    @Column(name = "u_ref_grid_popup_width", length = 100)
    public String getURefGridViewPopupWidth()
    {
        return URefGridViewPopupWidth;
    }

    public void setURefGridViewPopupWidth(String uRefGridViewPopupWidth)
    {
        URefGridViewPopupWidth = uRefGridViewPopupWidth;
    }

    @Column(name = "u_ref_grid_popup_height", length = 100)
    public String getURefGridViewPopupHeight()
    {
        return URefGridViewPopupHeight;
    }

    public void setURefGridViewPopupHeight(String uRefGridViewPopupHeight)
    {
        URefGridViewPopupHeight = uRefGridViewPopupHeight;
    }

    @Column(name = "u_ref_grid_popup_title", length = 333)
    public String getURefGridViewPopupTitle()
    {
        return URefGridViewPopupTitle;
    }

    public void setURefGridViewPopupTitle(String uRefGridViewPopupTitle)
    {
        URefGridViewPopupTitle = uRefGridViewPopupTitle;
    }

    @Column(name = "u_tooltip", length = 500)
    public String getUTooltip()
    {
        return UTooltip;
    }

    public void setUTooltip(String tooltip)
    {
        UTooltip = tooltip;
    }

    @Transient
    public String getUChoiceTable()
    {
        return UChoiceTable;
    }

    public void setUChoiceTable(String uChoiceTable)
    {
        UChoiceTable = uChoiceTable;
    }

    @Transient
    public String getUChoiceDisplayField()
    {
        return UChoiceDisplayField;
    }

    public void setUChoiceDisplayField(String uChoiceDisplayField)
    {
        UChoiceDisplayField = uChoiceDisplayField;
    }

    @Transient
    public String getUChoiceValueField()
    {
        return UChoiceValueField;
    }

    public void setUChoiceValueField(String uChoiceValueField)
    {
        UChoiceValueField = uChoiceValueField;
    }

    @Transient
    public String getUChoiceWhere()
    {
        return UChoiceWhere;
    }

    public void setUChoiceWhere(String uChoiceWhere)
    {
        UChoiceWhere = uChoiceWhere;
    }

    @Transient
    public String getUChoiceField()
    {
        return UChoiceField;
    }

    public void setUChoiceField(String uChoiceField)
    {
        UChoiceField = uChoiceField;
    }

    @Transient
    public String getUChoiceSql()
    {
        return UChoiceSql;
    }

    public void setUChoiceSql(String uChoiceSql)
    {
        UChoiceSql = uChoiceSql;
    }

    @Transient
    public int getUChoiceOptionSelection()
    {
        return UChoiceOptionSelection;
    }

    public void setUChoiceOptionSelection(int uChoiceOptionSelection)
    {
        UChoiceOptionSelection = uChoiceOptionSelection;
    }
    
    @Transient
    public int getUSectionNumberOfColumns()
    {
        return USectionNumberOfColumns;
    }

    public void setUSectionNumberOfColumns(int uSectionNumberOfColumns)
    {
        USectionNumberOfColumns = uSectionNumberOfColumns;
    }

    @Transient
    public String getUSectionType()
    {
        return USectionType;
    }

    public void setUSectionType(String uSectionType)
    {
        USectionType = uSectionType;
    }

    @Transient
    public String getUSectionLayout()
    {
        return USectionLayout;
    }

    public void setUSectionLayout(String uSectionLayout)
    {
        USectionLayout = uSectionLayout;
    }

    @Transient
    public String getUSectionTitle()
    {
        return USectionTitle;
    }

    public void setUSectionTitle(String uSectionTitle)
    {
        USectionTitle = uSectionTitle;
    }

    @Transient
    public String getUSectionColumns()
    {
        return USectionColumns;
    }

    public void setUSectionColumns(String uSectionColumns)
    {
        USectionColumns = uSectionColumns;
    }
    
    //either a MetaFieldProperties object or MetaFieldPropertiesVO object as the parameter
    public static String createJSONfromVO(Object object)
    {
        JsonConfig config = new JsonConfig();
        config.setExcludes(new String[] { "metaxFieldDependencys" });
        config.setIgnoreDefaultExcludes(false);
        config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
        JSONObject temp = (JSONObject) JSONSerializer.toJSON(object, config);
        String json = temp.toString();
        return json;
    }
    
    @Override
    public MetaFieldPropertiesVO doGetVO()
    {
        MetaFieldPropertiesVO vo = new MetaFieldPropertiesVO();
        super.doGetBaseVO(vo);
        
        vo.setUAllowAssignToMe(getUAllowAssignToMe());
        vo.setUAllowDownload(getUAllowDownload());
        vo.setUAllowFileTypes(getUAllowFileTypes());
        vo.setUAllowReferenceTableAdd(getUAllowReferenceTableAdd());
        vo.setUAllowReferenceTableRemove(getUAllowReferenceTableRemove());
        vo.setUAllowReferenceTableView(getUAllowReferenceTableView());
        vo.setUAllowRemove(getUAllowRemove());
        vo.setUAllowUpload(getUAllowUpload());
        vo.setUAutoAssignToMe(getUAutoAssignToMe());
        vo.setUBooleanMaxLength(getUBooleanMaxLength());
        vo.setUCheckboxValues(getUCheckboxValues());
        vo.setUChoiceDisplayField(getUChoiceDisplayField());
        vo.setUChoiceField(getUChoiceField());
        vo.setUChoiceOptionSelection(getUChoiceOptionSelection());
        vo.setUChoiceSql(getUChoiceSql());
        vo.setUChoiceTable(getUChoiceTable());
        vo.setUChoiceValueField(getUChoiceValueField());
        vo.setUChoiceValues(getUChoiceValues());
        vo.setUChoiceWhere(getUChoiceWhere());
        vo.setUDateTimeHasCalendar(getUDateTimeHasCalendar());
        vo.setUDecimalMaxValue(getUDecimalMaxValue());
        vo.setUDecimalMinValue(getUDecimalMinValue());
        vo.setUDefaultValue(getUDefaultValue());
        vo.setUFileUploadReferenceColumnName(getUFileUploadReferenceColumnName());
        vo.setUFileUploadTableName(getUFileUploadTableName());
        vo.setUGroups(getUGroups());
        vo.setUHeight(getUHeight());
        vo.setUHiddenValue(getUHiddenValue());
        vo.setUIntegerMaxValue(getUIntegerMaxValue());
        vo.setUIntegerMinValue(getUIntegerMinValue());
        vo.setUIsCrypt(getUIsCrypt());
        vo.setUIsHidden(getUIsHidden());
        vo.setUIsMandatory(getUIsMandatory());
        vo.setUIsReadOnly(getUIsReadOnly());
        vo.setUJavascript(getUJavascript());
        vo.setUJournalColumns(getUJournalColumns());
        vo.setUJournalMaxValue(getUJournalMaxValue());
        vo.setUJournalMinValue(getUJournalMinValue());
        vo.setUJournalRows(getUJournalRows());
        vo.setULabelAlign(getULabelAlign());
        vo.setULinkParams(getULinkParams());
        vo.setULinkTarget(getULinkTarget());
        vo.setULinkTemplate(getULinkTemplate());
        vo.setUListColumns(getUListColumns());
        vo.setUListMaxDisplay(getUListMaxDisplay());
        vo.setUListRows(getUListRows());
        vo.setUListValues(getUListValues());
        vo.setUName(getUName());
        vo.setUDisplayName(getUDisplayName());
        vo.setUOrder(getUOrder());
        vo.setURecurseTeamsOfTeam(getURecurseTeamsOfTeam());
        vo.setURecurseUsersOfTeam(getURecurseUsersOfTeam());
        vo.setUReferenceDisplayColumn(getUReferenceDisplayColumnList());
        vo.setUReferenceDisplayColumnList(getUReferenceDisplayColumnList());
        vo.setUReferenceParams(getUReferenceParams());
        vo.setUReferenceTable(getUReferenceTable());
        vo.setUReferenceTableUrl(getUReferenceTableUrl());
        vo.setUReferenceTarget(getUReferenceTarget());
        vo.setURefGridReferenceByTable(getURefGridReferenceByTable());
        vo.setURefGridReferenceColumnName(getURefGridReferenceColumnName());
        vo.setURefGridSelectedReferenceColumns(getURefGridSelectedReferenceColumns());
        vo.setURefGridViewPopup(getURefGridViewPopup());
        vo.setURefGridViewPopupHeight(getURefGridViewPopupHeight());
        vo.setURefGridViewPopupTitle(getURefGridViewPopupTitle());
        vo.setURefGridViewPopupWidth(getURefGridViewPopupWidth());
        vo.setUSectionColumns(getUSectionColumns());
        vo.setUSectionLayout(getUSectionLayout());
        vo.setUSectionNumberOfColumns(getUSectionNumberOfColumns());
        vo.setUSectionTitle(getUSectionTitle());
        vo.setUSectionType(getUSectionType());
        vo.setUSelectIsMultiSelect(getUSelectIsMultiSelect());
        vo.setUSelectMaxDisplay(getUSelectMaxDisplay());
        vo.setUSelectUserInput(getUSelectUserInput());
        vo.setUSelectValues(getUSelectValues());
        vo.setUSequencePrefix(getUSequencePrefix());
        vo.setUSize(getUSize());
        vo.setUStringMaxLength(getUStringMaxLength());
        vo.setUStringMinLength(getUStringMinLength());
        vo.setUTable(getUTable());
        vo.setUTeamPickerTeamsOfTeams(getUTeamPickerTeamsOfTeams());
        vo.setUTeamsOfTeams(getUTeamsOfTeams());
        vo.setUTooltip(getUTooltip());
        vo.setUUIStringMaxLength(getUUIStringMaxLength());
        vo.setUUIType(getUUIType());
        vo.setUUsersOfTeams(getUUsersOfTeams());
        vo.setUWidgetColumns(getUWidgetColumns());
        vo.setUWidth(getUWidth());
        vo.setUShowHorizontalRule(getUShowHorizontalRule());
        vo.setUAllowJournalEdit(getUAllowJournalEdit());
        vo.setJsonString(createJSONfromVO(vo));
                
        //refs
//        MetaStyle metaStyle; - not functional yet

        Collection<MetaxFieldDependency> metaxFieldDependencys = Hibernate.isInitialized(this.metaxFieldDependencys) ? getMetaxFieldDependencys() : null;        
        if(metaxFieldDependencys != null && metaxFieldDependencys.size() > 0)
        {
            Collection<MetaxFieldDependencyVO> vos = new ArrayList<MetaxFieldDependencyVO>();
            for(MetaxFieldDependency model : metaxFieldDependencys)
            {
                vos.add(model.doGetVO());
            }
            vo.setMetaxFieldDependencys(vos);
        }
        
        return vo;
    }

    @Override
    public void applyVOToModel(MetaFieldPropertiesVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo); 
            
            this.setUAllowAssignToMe(vo.getUAllowAssignToMe() != null ? vo.getUAllowAssignToMe() : getUAllowAssignToMe());
            this.setUAllowDownload(vo.getUAllowDownload() != null ? vo.getUAllowDownload() : getUAllowDownload());
            this.setUAllowFileTypes(StringUtils.isNotBlank(vo.getUAllowFileTypes()) && vo.getUAllowFileTypes().equals(VO.STRING_DEFAULT) ? getUAllowFileTypes() : vo.getUAllowFileTypes());
            this.setUAllowReferenceTableAdd(StringUtils.isNotBlank(vo.getUAllowReferenceTableAdd()) && vo.getUAllowReferenceTableAdd().equals(VO.STRING_DEFAULT) ? getUAllowReferenceTableAdd() : vo.getUAllowReferenceTableAdd());
            this.setUAllowReferenceTableRemove(StringUtils.isNotBlank(vo.getUAllowReferenceTableRemove()) && vo.getUAllowReferenceTableRemove().equals(VO.STRING_DEFAULT) ? getUAllowReferenceTableRemove() : vo.getUAllowReferenceTableRemove());
            this.setUAllowReferenceTableView(StringUtils.isNotBlank(vo.getUAllowReferenceTableView()) && vo.getUAllowReferenceTableView().equals(VO.STRING_DEFAULT) ? getUAllowReferenceTableView() : vo.getUAllowReferenceTableView());
            this.setUAllowRemove(vo.getUAllowRemove() != null ? vo.getUAllowRemove() : getUAllowRemove());
            this.setUAllowUpload(vo.getUAllowUpload() != null ? vo.getUAllowUpload() : getUAllowUpload());
            this.setUAutoAssignToMe(vo.getUAutoAssignToMe() != null ? vo.getUAutoAssignToMe() : getUAutoAssignToMe());
            this.setUBooleanMaxLength(vo.getUBooleanMaxLength() != null && vo.getUBooleanMaxLength().equals(VO.INTEGER_DEFAULT) ? getUBooleanMaxLength() : vo.getUBooleanMaxLength());
            this.setUCheckboxValues(StringUtils.isNotBlank(vo.getUCheckboxValues()) && vo.getUCheckboxValues().equals(VO.STRING_DEFAULT) ? getUCheckboxValues() : vo.getUCheckboxValues());
            this.setUChoiceDisplayField(StringUtils.isNotBlank(vo.getUChoiceDisplayField()) && vo.getUChoiceDisplayField().equals(VO.STRING_DEFAULT) ? getUChoiceDisplayField() : vo.getUChoiceDisplayField());
            this.setUChoiceField(StringUtils.isNotBlank(vo.getUChoiceField()) && vo.getUChoiceField().equals(VO.STRING_DEFAULT) ? getUChoiceField() : vo.getUChoiceField());
            this.setUChoiceOptionSelection(vo.getUChoiceOptionSelection() == VO.INTEGER_DEFAULT ? getUChoiceOptionSelection() : vo.getUChoiceOptionSelection());
            this.setUChoiceSql(StringUtils.isNotBlank(vo.getUChoiceSql()) && vo.getUChoiceSql().equals(VO.STRING_DEFAULT) ? getUChoiceSql() : vo.getUChoiceSql());
            this.setUChoiceTable(StringUtils.isNotBlank(vo.getUChoiceTable()) && vo.getUChoiceTable().equals(VO.STRING_DEFAULT) ? getUChoiceTable() : vo.getUChoiceTable());
            this.setUChoiceValueField(StringUtils.isNotBlank(vo.getUChoiceValueField()) && vo.getUChoiceValueField().equals(VO.STRING_DEFAULT) ? getUChoiceValueField() : vo.getUChoiceValueField());
            this.setUChoiceValues(StringUtils.isNotBlank(vo.getUChoiceValues()) && vo.getUChoiceValues().equals(VO.STRING_DEFAULT) ? getUChoiceValues() : vo.getUChoiceValues());
            this.setUChoiceWhere(StringUtils.isNotBlank(vo.getUChoiceWhere()) && vo.getUChoiceWhere().equals(VO.STRING_DEFAULT) ? getUChoiceWhere() : vo.getUChoiceWhere());
            this.setUDateTimeHasCalendar(vo.getUDateTimeHasCalendar() != null ? vo.getUDateTimeHasCalendar() : getUDateTimeHasCalendar());
            this.setUDecimalMaxValue(vo.getUDecimalMaxValue() != null && vo.getUDecimalMaxValue().equals(VO.DOUBLE_DEFAULT) ? getUDecimalMaxValue() : vo.getUDecimalMaxValue());
            this.setUDecimalMinValue(vo.getUDecimalMinValue() != null && vo.getUDecimalMinValue().equals(VO.DOUBLE_DEFAULT) ? getUDecimalMinValue() : vo.getUDecimalMinValue());
            this.setUDefaultValue(StringUtils.isNotBlank(vo.getUDefaultValue()) && vo.getUDefaultValue().equals(VO.STRING_DEFAULT) ? getUDefaultValue() : vo.getUDefaultValue());
            this.setUFileUploadReferenceColumnName(StringUtils.isNotBlank(vo.getUFileUploadReferenceColumnName()) && vo.getUFileUploadReferenceColumnName().equals(VO.STRING_DEFAULT) ? getUFileUploadReferenceColumnName() : vo.getUFileUploadReferenceColumnName());
            this.setUFileUploadTableName(StringUtils.isNotBlank(vo.getUFileUploadTableName()) && vo.getUFileUploadTableName().equals(VO.STRING_DEFAULT) ? getUFileUploadTableName() : vo.getUFileUploadTableName());
            this.setUGroups(StringUtils.isNotBlank(vo.getUGroups()) && vo.getUGroups().equals(VO.STRING_DEFAULT) ? getUGroups() : vo.getUGroups());
            this.setUHeight(vo.getUHeight() != null && vo.getUHeight().equals(VO.INTEGER_DEFAULT) ? getUHeight() : vo.getUHeight());
            this.setUHiddenValue(StringUtils.isNotBlank(vo.getUHiddenValue()) && vo.getUHiddenValue().equals(VO.STRING_DEFAULT) ? getUHiddenValue() : vo.getUHiddenValue());
            this.setUIntegerMaxValue(vo.getUIntegerMaxValue() != null && vo.getUIntegerMaxValue().equals(VO.INTEGER_DEFAULT) ? getUIntegerMaxValue() : vo.getUIntegerMaxValue());
            this.setUIntegerMinValue(vo.getUIntegerMinValue() != null && vo.getUIntegerMinValue().equals(VO.INTEGER_DEFAULT) ? getUIntegerMinValue() : vo.getUIntegerMinValue());
            this.setUIsCrypt(vo.getUIsCrypt() != null ? vo.getUIsCrypt() : getUIsCrypt());
            this.setUIsHidden(vo.getUIsHidden() != null ? vo.getUIsHidden() : getUIsHidden());
            this.setUIsMandatory(vo.getUIsMandatory() != null ? vo.getUIsMandatory() : getUIsMandatory());
            this.setUIsReadOnly(vo.getUIsReadOnly() != null ? vo.getUIsReadOnly() : getUIsReadOnly());
            this.setUJavascript(StringUtils.isNotBlank(vo.getUJavascript()) && vo.getUJavascript().equals(VO.STRING_DEFAULT) ? getUJavascript() : vo.getUJavascript());
            this.setUJournalColumns(vo.getUJournalColumns() != null && vo.getUJournalColumns().equals(VO.INTEGER_DEFAULT) ? getUJournalColumns() : vo.getUJournalColumns());
            this.setUJournalMaxValue(vo.getUJournalMaxValue() != null && vo.getUJournalMaxValue().equals(VO.INTEGER_DEFAULT) ? getUJournalMaxValue() : vo.getUJournalMaxValue());
            this.setUJournalMinValue(vo.getUJournalMinValue() != null && vo.getUJournalMinValue().equals(VO.INTEGER_DEFAULT) ? getUJournalMinValue() : vo.getUJournalMinValue());
            this.setUJournalRows(vo.getUJournalRows() != null && vo.getUJournalRows().equals(VO.INTEGER_DEFAULT) ? getUJournalRows() : vo.getUJournalRows());
            this.setULabelAlign(StringUtils.isNotBlank(vo.getULabelAlign()) && vo.getULabelAlign().equals(VO.STRING_DEFAULT) ? getULabelAlign() : vo.getULabelAlign());
            this.setULinkParams(StringUtils.isNotBlank(vo.getULinkParams()) && vo.getULinkParams().equals(VO.STRING_DEFAULT) ? getULinkParams() : vo.getULinkParams());
            this.setULinkTarget(StringUtils.isNotBlank(vo.getULinkTarget()) && vo.getULinkTarget().equals(VO.STRING_DEFAULT) ? getULinkTarget() : vo.getULinkTarget());
            this.setULinkTemplate(StringUtils.isNotBlank(vo.getULinkTemplate()) && vo.getULinkTemplate().equals(VO.STRING_DEFAULT) ? getULinkTemplate() : vo.getULinkTemplate());
            this.setUListColumns(vo.getUListColumns() != null && vo.getUListColumns().equals(VO.INTEGER_DEFAULT) ? getUListColumns() : vo.getUListColumns());
            this.setUListMaxDisplay(vo.getUListMaxDisplay() != null && vo.getUListMaxDisplay().equals(VO.INTEGER_DEFAULT) ? getUListMaxDisplay() : vo.getUListMaxDisplay());
            this.setUListRows(vo.getUListRows() != null && vo.getUListRows().equals(VO.INTEGER_DEFAULT) ? getUListRows() : vo.getUListRows());
            this.setUListValues(StringUtils.isNotBlank(vo.getUListValues()) && vo.getUListValues().equals(VO.STRING_DEFAULT) ? getUListValues() : vo.getUListValues());
            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUDisplayName(StringUtils.isNotBlank(vo.getUDisplayName()) && vo.getUDisplayName().equals(VO.STRING_DEFAULT) ? getUDisplayName() : vo.getUDisplayName());
            this.setUOrder(vo.getUOrder() != null && vo.getUOrder().equals(VO.INTEGER_DEFAULT) ? getUOrder() : vo.getUOrder());
            this.setURecurseTeamsOfTeam(vo.getURecurseTeamsOfTeam() != null ? vo.getURecurseTeamsOfTeam() : getURecurseTeamsOfTeam());
            this.setURecurseUsersOfTeam(vo.getURecurseUsersOfTeam() != null ? vo.getURecurseUsersOfTeam() : getURecurseUsersOfTeam());
            this.setUReferenceDisplayColumn(StringUtils.isNotBlank(vo.getUReferenceDisplayColumn()) && vo.getUReferenceDisplayColumn().equals(VO.STRING_DEFAULT) ? getUReferenceDisplayColumn() : vo.getUReferenceDisplayColumn());
            this.setUReferenceDisplayColumnList(StringUtils.isNotBlank(vo.getUReferenceDisplayColumnList()) && vo.getUReferenceDisplayColumnList().equals(VO.STRING_DEFAULT) ? getUReferenceDisplayColumnList() : vo.getUReferenceDisplayColumnList());
            this.setUReferenceParams(StringUtils.isNotBlank(vo.getUReferenceParams()) && vo.getUReferenceParams().equals(VO.STRING_DEFAULT) ? getUReferenceParams() : vo.getUReferenceParams());
            this.setUReferenceTable(StringUtils.isNotBlank(vo.getUReferenceTable()) && vo.getUReferenceTable().equals(VO.STRING_DEFAULT) ? getUReferenceTable() : vo.getUReferenceTable());
            this.setUReferenceTableUrl(StringUtils.isNotBlank(vo.getUReferenceTableUrl()) && vo.getUReferenceTableUrl().equals(VO.STRING_DEFAULT) ? getUReferenceTableUrl() : vo.getUReferenceTableUrl());
            this.setUReferenceTarget(StringUtils.isNotBlank(vo.getUReferenceTarget()) && vo.getUReferenceTarget().equals(VO.STRING_DEFAULT) ? getUReferenceTarget() : vo.getUReferenceTarget());
            this.setURefGridReferenceByTable(StringUtils.isNotBlank(vo.getURefGridReferenceByTable()) && vo.getURefGridReferenceByTable().equals(VO.STRING_DEFAULT) ? getURefGridReferenceByTable() : vo.getURefGridReferenceByTable());
            this.setURefGridReferenceColumnName(StringUtils.isNotBlank(vo.getURefGridReferenceColumnName()) && vo.getURefGridReferenceColumnName().equals(VO.STRING_DEFAULT) ? getURefGridReferenceColumnName() : vo.getURefGridReferenceColumnName());
            this.setURefGridSelectedReferenceColumns(StringUtils.isNotBlank(vo.getURefGridSelectedReferenceColumns()) && vo.getURefGridSelectedReferenceColumns().equals(VO.STRING_DEFAULT) ? getURefGridSelectedReferenceColumns() : vo.getURefGridSelectedReferenceColumns());
            this.setURefGridViewPopup(StringUtils.isNotBlank(vo.getURefGridViewPopup()) && vo.getURefGridViewPopup().equals(VO.STRING_DEFAULT) ? getURefGridViewPopup() : vo.getURefGridViewPopup());
            this.setURefGridViewPopupHeight(StringUtils.isNotBlank(vo.getURefGridViewPopupHeight()) && vo.getURefGridViewPopupHeight().equals(VO.STRING_DEFAULT) ? getURefGridViewPopupHeight() : vo.getURefGridViewPopupHeight());
            this.setURefGridViewPopupTitle(StringUtils.isNotBlank(vo.getURefGridViewPopupTitle()) && vo.getURefGridViewPopupTitle().equals(VO.STRING_DEFAULT) ? getURefGridViewPopupTitle() : vo.getURefGridViewPopupTitle());
            this.setURefGridViewPopupWidth(StringUtils.isNotBlank(vo.getURefGridViewPopupWidth()) && vo.getURefGridViewPopupWidth().equals(VO.STRING_DEFAULT) ? getURefGridViewPopupWidth() : vo.getURefGridViewPopupWidth());
            this.setUSectionColumns(StringUtils.isNotBlank(vo.getUSectionColumns()) && vo.getUSectionColumns().equals(VO.STRING_DEFAULT) ? getUSectionColumns() : vo.getUSectionColumns());
            this.setUSectionLayout(StringUtils.isNotBlank(vo.getUSectionLayout()) && vo.getUSectionLayout().equals(VO.STRING_DEFAULT) ? getUSectionLayout() : vo.getUSectionLayout());
            this.setUSectionNumberOfColumns(vo.getUSectionNumberOfColumns() == VO.INTEGER_DEFAULT ? getUSectionNumberOfColumns() : vo.getUSectionNumberOfColumns());
            this.setUSectionTitle(StringUtils.isNotBlank(vo.getUSectionTitle()) && vo.getUSectionTitle().equals(VO.STRING_DEFAULT) ? getUSectionTitle() : vo.getUSectionTitle());
            this.setUSectionType(StringUtils.isNotBlank(vo.getUSectionType()) && vo.getUSectionType().equals(VO.STRING_DEFAULT) ? getUSectionType() : vo.getUSectionType());
            this.setUSelectIsMultiSelect(vo.getUSelectIsMultiSelect() != null ? vo.getUSelectIsMultiSelect() : getUSelectIsMultiSelect());
            this.setUSelectMaxDisplay(vo.getUSelectMaxDisplay() != null && vo.getUSelectMaxDisplay().equals(VO.INTEGER_DEFAULT) ? getUSelectMaxDisplay() : vo.getUSelectMaxDisplay());
            this.setUSelectUserInput(vo.getUSelectUserInput() != null ? vo.getUSelectUserInput() : getUSelectUserInput());
            this.setUSelectValues(StringUtils.isNotBlank(vo.getUSelectValues()) && vo.getUSelectValues().equals(VO.STRING_DEFAULT) ? getUSelectValues() : vo.getUSelectValues());
            this.setUSequencePrefix(StringUtils.isNotBlank(vo.getUSequencePrefix()) && vo.getUSequencePrefix().equals(VO.STRING_DEFAULT) ? getUSequencePrefix() : vo.getUSequencePrefix());
            this.setUSize(vo.getUSize() != null && vo.getUSize().equals(VO.INTEGER_DEFAULT) ? getUSize() : vo.getUSize());
            this.setUStringMaxLength(vo.getUStringMaxLength() != null && vo.getUStringMaxLength().equals(VO.INTEGER_DEFAULT) ? getUStringMaxLength() : vo.getUStringMaxLength());
            this.setUStringMinLength(vo.getUStringMinLength() != null && vo.getUStringMinLength().equals(VO.INTEGER_DEFAULT) ? getUStringMinLength() : vo.getUStringMinLength());
            this.setUTable(StringUtils.isNotBlank(vo.getUTable()) && vo.getUTable().equals(VO.STRING_DEFAULT) ? getUTable() : vo.getUTable());
            this.setUTeamPickerTeamsOfTeams(StringUtils.isNotBlank(vo.getUTeamPickerTeamsOfTeams()) && vo.getUTeamPickerTeamsOfTeams().equals(VO.STRING_DEFAULT) ? getUTeamPickerTeamsOfTeams() : vo.getUTeamPickerTeamsOfTeams());
            this.setUTeamsOfTeams(StringUtils.isNotBlank(vo.getUTeamsOfTeams()) && vo.getUTeamsOfTeams().equals(VO.STRING_DEFAULT) ? getUTeamsOfTeams() : vo.getUTeamsOfTeams());
            this.setUTooltip(StringUtils.isNotBlank(vo.getUTooltip()) && vo.getUTooltip().equals(VO.STRING_DEFAULT) ? getUTooltip() : vo.getUTooltip());
            this.setUUIStringMaxLength(vo.getUUIStringMaxLength() != null && vo.getUUIStringMaxLength().equals(VO.INTEGER_DEFAULT) ? getUUIStringMaxLength() : vo.getUUIStringMaxLength());
            this.setUUIType(StringUtils.isNotBlank(vo.getUUIType()) && vo.getUUIType().equals(VO.STRING_DEFAULT) ? getUUIType() : vo.getUUIType());
            this.setUUsersOfTeams(StringUtils.isNotBlank(vo.getUUsersOfTeams()) && vo.getUUsersOfTeams().equals(VO.STRING_DEFAULT) ? getUUsersOfTeams() : vo.getUUsersOfTeams());
            this.setUWidgetColumns(vo.getUWidgetColumns() != null && vo.getUWidgetColumns().equals(VO.INTEGER_DEFAULT) ? getUWidgetColumns() : vo.getUWidgetColumns());
            this.setUWidth(vo.getUWidth() != null && vo.getUWidth().equals(VO.INTEGER_DEFAULT) ? getUWidth() : vo.getUWidth());
            this.setUShowHorizontalRule(vo.getUShowHorizontalRule() != null ? vo.getUShowHorizontalRule() : false);
            this.setUAllowJournalEdit(vo.getUAllowJournalEdit() != null ? vo.getUAllowJournalEdit() : false);
            
            Collection<MetaxFieldDependencyVO> metaxFieldDependencysVOs  = vo.getMetaxFieldDependencys();
            if(metaxFieldDependencysVOs != null && metaxFieldDependencysVOs.size() > 0)
            {
                Collection<MetaxFieldDependency> models = new ArrayList<MetaxFieldDependency>();
                for(MetaxFieldDependencyVO refVO : metaxFieldDependencysVOs)
                {
                    models.add(new MetaxFieldDependency(refVO));
                }
                this.setMetaxFieldDependencys(models);
            }

        }
        
    }

    @Transient
    public Boolean getUShowHorizontalRule()
    {
        return UShowHorizontalRule;
    }

    public void setUShowHorizontalRule(Boolean uShowHorizontalRule)
    {
        UShowHorizontalRule = uShowHorizontalRule;
    }

    @Transient
    public Boolean getUAllowJournalEdit()
    {
        return UAllowJournalEdit;
    }

    public void setUAllowJournalEdit(Boolean uAllowJournalEdit)
    {
        UAllowJournalEdit = uAllowJournalEdit;
    }
    
    // Convert the transient fields of an object into a json string
    public static String createTransientFieldsJSONfromVO(Object object)
    {
        if(object == null)
            return null;
        
        JSONObject json = new JSONObject();
        
        final List<Method> methods = getTransientMethods();
        methods.stream().sorted( (m1, m2) -> m1.getName().compareTo(m2.getName())).forEach(method -> {
            try {
                String methodName = method.getName();
                if (StringUtils.isNotBlank(methodName)) {
                    String field = getFieldName(methodName);
                    Object value = method.invoke(object, (Object[])null);
    
                    if(field != null && value != null) {
                       if(value instanceof String) {
                           if(StringUtils.isNotBlank(value.toString()))
                               json.put(field, value);
                       }
                       else
                           json.put(field, value);
                    }
                }
            }
            catch(Exception e)
            {
                Log.log.error("Not able to create transient fields JSON from VO.", e);
                throw new RuntimeException("Not able to create transient fields JSON from VO.", e);
            }
        });

        return json.toString();
    }
    
    public static String getFieldName(String methodName) {
        
        String fieldName = null;
        
        if(StringUtils.isBlank(methodName))
            return null;
        
        fieldName = methodName.substring(3);
        if(fieldName.length() == 0)
            return null;

        return fieldName;
    }
    
    private static List<Method> loadTransientMethods() {
        
        List<Method> methodList = new ArrayList<Method>();
        
        try {
            Class<?> currentClass = Class.forName("com.resolve.persistence.model.MetaFieldProperties");
            final Method[] methods = currentClass.getDeclaredMethods();
            for (final Method method : methods) {

                Annotation[] annoations = method.getAnnotations();
                if(annoations.length == 0)
                    continue;
                
                for(Annotation annoation:annoations) {
                    Class<?> type = annoation.annotationType();

                    if(type != null && type.getName().indexOf("Transient") != -1 && !method.getName().equals("getTransientMethods"))
                        methodList.add(method);
                }
            }
        } catch(Exception e) {
            Log.log.error("Not able to load the transient methods from Class com.resolve.persistence.model.MetaFieldProperties", e);
            throw new RuntimeException("Not able to load the transient methods from Class com.resolve.persistence.model.MetaFieldProperties", e);
        }

        return methodList;
    }
    
    @Transient
    public static List<Method> getTransientMethods()
    {
        if(transientMethods == null)
            transientMethods = loadTransientMethods();
        
        return transientMethods;
    }

    public static void setTransientMethods(List<Method> transientMethods)
    {
        MetaFieldProperties.transientMethods = transientMethods;
    }
    
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "u_transient_storage", length = 16777215)
    @Lob
    public String getTransientFieldsJsonString()
    {
        return this.UTransientFieldsJsonString;
    }
    
    public void setTransientFieldsJsonString(String transientFieldsJsonString)
    {
        this.UTransientFieldsJsonString = transientFieldsJsonString;
    }
}
