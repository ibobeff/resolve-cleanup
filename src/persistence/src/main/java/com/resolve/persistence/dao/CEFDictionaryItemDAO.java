package com.resolve.persistence.dao;

import com.resolve.persistence.model.CEFDictionaryItem;

public interface CEFDictionaryItemDAO extends GenericDAO<CEFDictionaryItem, String>{}
