package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.MSGGatewayPropertiesAttrVO;
import com.resolve.services.hibernate.vo.MSGGatewayPropertiesVO;
import com.resolve.services.interfaces.VO;

@Entity
@Table(name = "msg_gateway_properties")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MSGGatewayProperties extends GatewayProperties<MSGGatewayPropertiesVO> {

    private static final long serialVersionUID = 2729772642669221961L;
    protected Integer UMSGPort;
    protected String UMSGUser;
    protected String UMSGPass;
    
    protected Boolean UMSGSsl;
    protected String UMSGSslCertificate ;
    protected String UMSGSslPassword;

    // object referenced by
    private Collection<MSGGatewayPropertiesAttr> attrs;

    public MSGGatewayProperties() {
    }

    public MSGGatewayProperties(MSGGatewayPropertiesVO vo) {
        applyVOToModel(vo);
    }

    @Column(name = "u_msg_port")
    public Integer getUMSGPort()
    {
        return UMSGPort;
    }

    public void setUMSGPort(Integer uMSGPort)
    {
        UMSGPort = uMSGPort;
    }
    
    @Column(name = "u_msg_user", length = 20)
    public String getUMSGUser()
    {
        return UMSGUser;
    }

    public void setUMSGUser(String uMSGUser)
    {
        UMSGUser = uMSGUser;
    }


    @Column(name = "u_msg_pass", length = 20)
    public String getUMSGPass()
    {
        return UMSGPass;
    }

    public void setUMSGPass(String uMSGPass)
    {
        UMSGPass = uMSGPass;
    }

    @Column(name = "u_msg_ssl")
    public Boolean getUMSGSsl()
    {
        return UMSGSsl;
    }

    public void setUMSGSsl(Boolean uMSGSsl)
    {
        UMSGSsl = uMSGSsl;
    }

    @Column(name = "u_msg_ssl_certificate", length = 100)
    public String getUMSGSslCertificate()
    { 
        return UMSGSslCertificate;
    }

    public void setUMSGSslCertificate(String uMSGSslCertificate)
    {
        UMSGSslCertificate = uMSGSslCertificate;
    }

    @Column(name = "u_msg_ssl_pass", length = 20)
    public String getUMSGSslPassword()
    {
        return UMSGSslPassword;
    }

    public void setUMSGSslPassword(String uMSGSslPassword)
    {
        UMSGSslPassword = uMSGSslPassword;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "msgGatewayProperties")
    @NotFound(action = NotFoundAction.EXCEPTION)
    public Collection<MSGGatewayPropertiesAttr> getAttrs()
    {
        return this.attrs;
    }

    public void setAttrs(Collection<MSGGatewayPropertiesAttr> attrs)
    {
        this.attrs = attrs;
    }

    @Override 
    public MSGGatewayPropertiesVO doGetVO() {
        
        MSGGatewayPropertiesVO vo = new MSGGatewayPropertiesVO();
        
        super.doGetBaseVO(vo);

        vo.setUMSGPort(getUMSGPort());
        vo.setUMSGUser(getUMSGUser());
        vo.setUMSGPass(getUMSGPass());
        vo.setUMSGSsl(getUMSGSsl());
        vo.setUMSGSslCertificate(getUMSGSslCertificate());
        vo.setUMSGSslPassword(getUMSGSslPassword());
        
        //references
        Collection<MSGGatewayPropertiesAttr> attrModels = Hibernate.isInitialized(this.attrs) ? getAttrs() : null;
        if(attrModels != null && attrModels.size() > 0)
        {
            Collection<MSGGatewayPropertiesAttrVO> vos = new ArrayList<MSGGatewayPropertiesAttrVO>();

            for(MSGGatewayPropertiesAttr model : attrs)
                vos.add(model.doGetVO());
            
            vo.setAttrs(vos);
        }       
        
        return vo;
    }

    @Override
    public void applyVOToModel(MSGGatewayPropertiesVO vo) {
        
        if (vo == null) return;
        
        super.applyVOToModel(vo);

        if (!VO.INTEGER_DEFAULT.equals(vo.getUMSGPort())) this.setUMSGPort(vo.getUMSGPort()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUMSGUser())) this.setUMSGUser(vo.getUMSGUser()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUMSGPass())) this.setUMSGPass(vo.getUMSGPass()); else ;

        if (!VO.BOOLEAN_DEFAULT.equals(vo.getUMSGSsl())) this.setUMSGSsl(vo.getUMSGSsl()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUMSGSslCertificate())) this.setUMSGSslCertificate(vo.getUMSGSslCertificate()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUMSGSslPassword())) this.setUMSGSslPassword(vo.getUMSGSslPassword()); else ;
        
        //references
        Collection<MSGGatewayPropertiesAttrVO> attrVOs = vo.getAttrs();
        if(attrVOs != null && attrVOs.size() > 0)
        {
            Collection<MSGGatewayPropertiesAttr> models = new ArrayList<MSGGatewayPropertiesAttr>();
            
            for(MSGGatewayPropertiesAttrVO attrVO : attrVOs)
                models.add(new MSGGatewayPropertiesAttr(attrVO));

            this.setAttrs(models);
        }
    }

}
