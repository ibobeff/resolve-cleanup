/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.resolve.services.hibernate.vo.MCPComponentVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
@Entity
@Table(name = "mcp_component")
public class MCPComponent extends BaseModel<MCPComponentVO>
{
    private String componentName;
    //sometime RSRemote may have more instances in a server
    private String instanceName;
    private String guid;
    private String serverIp;
    private String serverName;
    private String status;
    private Date lastStatusUpdatedOn;
    private String worksheetId;
    private String worksheet;

    public MCPComponent()
    {
    }

    public MCPComponent(MCPComponentVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_comp_name", nullable = false)
    public String getComponentName()
    {
        return componentName;
    }

    public void setComponentName(String componentName)
    {
        this.componentName = componentName;
    }

    @Column(name = "u_instance_name")
    public String getInstanceName()
    {
        return instanceName;
    }

    public void setInstanceName(String instanceName)
    {
        this.instanceName = instanceName;
    }

    @Column(name = "u_comp_guid")
    public String getGuid()
    {
        return guid;
    }

    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    @Column(name = "u_server_ip", nullable = false)
    public String getServerIp()
    {
        return serverIp;
    }

    public void setServerIp(String serverIp)
    {
        this.serverIp = serverIp;
    }

    @Column(name = "u_server_name")
    public String getServerName()
    {
        return serverName;
    }

    public void setServerName(String serverName)
    {
        this.serverName = serverName;
    }

    @Column(name = "u_status_updated")
    public Date getLastStatusUpdatedOn()
    {
        return lastStatusUpdatedOn;
    }

    public void setLastStatusUpdatedOn(Date lastStatusUpdatedOn)
    {
        this.lastStatusUpdatedOn = lastStatusUpdatedOn;
    }

    @Column(name = "u_status")
    public String getStatus()
    {
        return status;
    }

    public void setStatus(String uStatus)
    {
        status = uStatus;
    }

    @Column(name = "u_worksheet_id")
    public String getWorksheetId()
    {
        return worksheetId;
    }

    public void setWorksheetId(String worksheetId)
    {
        this.worksheetId = worksheetId;
    }

    @Column(name = "u_worksheet")
    public String getWorksheet()
    {
        return worksheet;
    }

    public void setWorksheet(String worksheet)
    {
        this.worksheet = worksheet;
    }

    @Override
    public MCPComponentVO doGetVO()
    {
        MCPComponentVO vo = new MCPComponentVO();
        try
        {
            super.doGetBaseVO(vo);

            vo.setComponentName(getComponentName());
            vo.setInstanceName(getInstanceName());
            vo.setGuid(getGuid());
            vo.setServerIp(getServerIp());
            vo.setServerName(getServerName());
            vo.setStatus(getStatus());
            vo.setLastStatusUpdatedOn(getLastStatusUpdatedOn());
            vo.setWorksheetId(getWorksheetId());
            vo.setWorksheet(getWorksheet());
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return vo;
    }

    @Override
    public void applyVOToModel(MCPComponentVO vo)
    {
        if (vo != null)
        {
            try
            {
                super.applyVOToModel(vo);

                this.setComponentName(StringUtils.isNotBlank(vo.getComponentName()) && vo.getComponentName().equals(VO.STRING_DEFAULT) ? getComponentName() : vo.getComponentName());
                this.setInstanceName(StringUtils.isNotBlank(vo.getInstanceName()) && vo.getInstanceName().equals(VO.STRING_DEFAULT) ? getInstanceName() : vo.getInstanceName());
                this.setGuid(StringUtils.isNotBlank(vo.getGuid()) && vo.getGuid().equals(VO.STRING_DEFAULT) ? getGuid() : vo.getGuid());
                this.setServerIp(StringUtils.isNotBlank(vo.getServerIp()) && vo.getServerIp().equals(VO.STRING_DEFAULT) ? getServerIp() : vo.getServerIp());
                this.setServerName(StringUtils.isNotBlank(vo.getServerName()) && vo.getServerName().equals(VO.STRING_DEFAULT) ? getServerName() : vo.getServerName());
                this.setStatus(StringUtils.isNotBlank(vo.getStatus()) && vo.getStatus().equals(VO.STRING_DEFAULT) ? getStatus() : vo.getStatus());
                this.setLastStatusUpdatedOn(vo.getLastStatusUpdatedOn() != null && vo.getLastStatusUpdatedOn().equals(VO.DATE_DEFAULT) ? getLastStatusUpdatedOn() : vo.getLastStatusUpdatedOn());
                this.setWorksheetId(StringUtils.isNotBlank(vo.getWorksheetId()) && vo.getWorksheetId().equals(VO.STRING_DEFAULT) ? getWorksheetId() : vo.getWorksheetId());
                this.setWorksheet(StringUtils.isNotBlank(vo.getWorksheet()) && vo.getWorksheet().equals(VO.STRING_DEFAULT) ? getWorksheet() : vo.getWorksheet());
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
}
