package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.PushGatewayPropertiesAttrVO;
import com.resolve.services.hibernate.vo.PushGatewayPropertiesVO;
import com.resolve.services.interfaces.VO;

@Entity
@Table(name = "push_gateway_properties")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PushGatewayProperties extends GatewayProperties<PushGatewayPropertiesVO> {

    private static final long serialVersionUID = 3690454245165636236L;
    protected Integer UHttpPort;
    protected String UHttpUser;
    protected String UHttpPass;
    
    protected Boolean UHttpSsl;
    protected String USslCertificate ;
    protected String USslPassword;
    
    protected Integer UExecuteTimeout = 120;
    protected Integer UScriptTimeout = 120;
    protected Integer UWaitTimeout = 120;
    
    // object referenced by
    private Collection<PushGatewayPropertiesAttr> attrs;

    public PushGatewayProperties() {
    }

    public PushGatewayProperties(PushGatewayPropertiesVO vo) {
        applyVOToModel(vo);
    }

    @Column(name = "u_http_port")
    public Integer getUHttpPort()
    {
        return UHttpPort;
    }

    public void setUHttpPort(Integer uHttpPort)
    {
        UHttpPort = uHttpPort;
    }
    
    @Column(name = "u_http_user", length = 20)
    public String getUHttpUser()
    {
        return UHttpUser;
    }

    public void setUHttpUser(String uHttpUser)
    {
        UHttpUser = uHttpUser;
    }


    @Column(name = "u_http_pass", length = 20)
    public String getUHttpPass()
    {
        return UHttpPass;
    }

    public void setUHttpPass(String uHttpPass)
    {
        UHttpPass = uHttpPass;
    }

    @Column(name = "u_http_ssl")
    public Boolean getUHttpSsl()
    {
        return UHttpSsl;
    }

    public void setUHttpSsl(Boolean uHttpSsl)
    {
        UHttpSsl = uHttpSsl;
    }

    @Column(name = "u_ssl_certificate", length = 100)
    public String getUSslCertificate()
    {
        return USslCertificate;
    }

    public void setUSslCertificate(String uSslCertificate)
    {
        USslCertificate = uSslCertificate;
    }

    @Column(name = "u_ssl_pass", length = 20)
    public String getUSslPassword()
    {
        return USslPassword;
    }

    public void setUSslPassword(String uSslPassword)
    {
        USslPassword = uSslPassword;
    }

    @Column(name = "u_execute_timeout")
    public Integer getUExecuteTimeout()
    {
        return UExecuteTimeout;
    }

    public void setUExecuteTimeout(Integer uExecuteTimeout)
    {
        UExecuteTimeout = uExecuteTimeout;
    }

    @Column(name = "u_script_timeout")
    public Integer getUScriptTimeout()
    {
        return UScriptTimeout;
    }

    public void setUScriptTimeout(Integer uScriptTimeout)
    {
        UScriptTimeout = uScriptTimeout;
    }

    @Column(name = "u_wait_timeout")
    public Integer getUWaitTimeout()
    {
        return UWaitTimeout;
    }

    public void setUWaitTimeout(Integer uWaitTimeout)
    {
        UWaitTimeout = uWaitTimeout;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "pushGatewayProperties")
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<PushGatewayPropertiesAttr> getAttrs()
    {
        return attrs;
    }

    public void setAttrs(Collection<PushGatewayPropertiesAttr> attrs)
    {
        this.attrs = attrs;
    }

    @Override 
    public PushGatewayPropertiesVO doGetVO() {
        
        PushGatewayPropertiesVO vo = new PushGatewayPropertiesVO();
        
        super.doGetBaseVO(vo);

        vo.setUHttpPort(getUHttpPort());
        vo.setUHttpUser(getUHttpUser());
        vo.setUHttpPass(getUHttpPass());
        vo.setUHttpSsl(getUHttpSsl());
        vo.setUSslCertificate(getUSslCertificate());
        vo.setUSslPassword(getUSslPassword());
        vo.setUExecuteTimeout(getUExecuteTimeout());
        vo.setUScriptTimeout(getUScriptTimeout());
        vo.setUWaitTimeout(getUWaitTimeout());
        
        //references
        Collection<PushGatewayPropertiesAttr> attrModels = Hibernate.isInitialized(this.attrs) ? getAttrs() : null;
        if(attrModels != null && attrModels.size() > 0)
        {
            Collection<PushGatewayPropertiesAttrVO> vos = new ArrayList<PushGatewayPropertiesAttrVO>();

            for(PushGatewayPropertiesAttr model : attrs)
                vos.add(model.doGetVO());
            
            vo.setAttrs(vos);
        }       
        
        return vo;
    }

    @Override
    public void applyVOToModel(PushGatewayPropertiesVO vo) {
        
        if (vo == null) return;
        
        super.applyVOToModel(vo);

        if (!VO.INTEGER_DEFAULT.equals(vo.getUHttpPort())) this.setUHttpPort(vo.getUHttpPort()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUHttpUser())) this.setUHttpUser(vo.getUHttpUser()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUHttpPass())) this.setUHttpPass(vo.getUHttpPass()); else ;

        if (!VO.BOOLEAN_DEFAULT.equals(vo.getUHttpSsl())) this.setUHttpSsl(vo.getUHttpSsl()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUSslCertificate())) this.setUSslCertificate(vo.getUSslCertificate()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUSslPassword())) this.setUSslPassword(vo.getUSslPassword()); else ;

        if (!VO.INTEGER_DEFAULT.equals(vo.getUExecuteTimeout())) this.setUExecuteTimeout(vo.getUExecuteTimeout()); else ;
        if (!VO.INTEGER_DEFAULT.equals(vo.getUScriptTimeout())) this.setUScriptTimeout(vo.getUScriptTimeout()); else ;
        if (!VO.INTEGER_DEFAULT.equals(vo.getUWaitTimeout())) this.setUWaitTimeout(vo.getUWaitTimeout()); else ;
        
        //references
        Collection<PushGatewayPropertiesAttrVO> attrVOs = vo.getAttrs();
        if(attrVOs != null && attrVOs.size() > 0)
        {
            Collection<PushGatewayPropertiesAttr> models = new ArrayList<PushGatewayPropertiesAttr>();
            
            for(PushGatewayPropertiesAttrVO attrVO : attrVOs)
                models.add(new PushGatewayPropertiesAttr(attrVO));

            this.setAttrs(models);
        }
    }

} // class PushGatewayProperties
