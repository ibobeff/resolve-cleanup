/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.criteria.Predicate;

import com.resolve.services.vo.QueryDTO;

public interface GenericDAO<T, ID extends Serializable>
{

	// Retrieve an Object from DB. Return null if object is not found from DB.
	T findById(ID id);

	T findById(ID id, boolean lock);
	
	// Retrieve all instances from DB.
	List<T> findAll();
	List<T> findAll(Predicate... restrictions);
	List<T> findAll(OrderbyProperty orderby);
	List<T> findAll(List<OrderbyProperty>  orderBy, Predicate... restrictions);
	
	// Retrieve at most maxResults number of instances from DB.
	List<T> findAll(int maxResults);
	List<T> findAll(int maxResults, Predicate... restrictions);
	List<T> findAll(int maxResults, List<OrderbyProperty> orderBy, Predicate... restrictions);

	// Retrieve at most maxResults number of instances from DB,
	// starting from the firstResult instance. e.g. default firstResult starts from 0.
	List<T> findAll(int maxResults, int firstResult);
	List<T> findAll(int maxResults, int firstResult, Predicate... restrictions);
	List<T> findAll(int maxResults, int firstResult, List<OrderbyProperty> orderBy, Predicate... restrictions);
	List<T> findAll(int maxResults, int firstResult, OrderbyProperty orderBy, Predicate... restrictions);

	
	//find() and findAll() will be the same in terms of functionality
	List<T> find();
	List<T> find(Predicate... restrictions);
	List<T> find(List<OrderbyProperty> orderbys, Predicate... restrictions);
	
	List<T> find(int maxResults);
	List<T> find(int maxResults, Predicate... restrictions);
	List<T> find(int maxResults, List<OrderbyProperty> orderbys, Predicate... restrictions);
	
	List<T> find(int maxResults, int firstResult);
	List<T> find(int maxResults, int firstResult, Predicate... restrictions);
	List<T> find(int maxResults, int firstResult, List<OrderbyProperty> orderbys, Predicate... restrictions);
	
	List<T> find(T exampleInstance, String... excludeProperty);
	List<T> findLike(T exampleInstance, List<String> likeQueryProperties, String... excludeProperty);
	List<T> find(T exampleInstance, List<OrderbyProperty> orderbys, String... excludeProperty);
	List<T> findLike(T exampleInstance, List<OrderbyProperty> orderbys, List<String> likeQueryProperties, 
					 String... excludeProperty);
	List<T> findNullProps(T exampleInstance, List<OrderbyProperty> orderbys, List<String> nullProps, 
			 			  String... excludeProperty);

	List<T> find(T exampleInstance, int maxResults, String... excludeProperty);
	List<T> find(T exampleInstance, int maxResults, List<OrderbyProperty> orderbys, String... excludeProperty);

	List<T> find(T exampleInstance, int maxResults, int firstResult, String... excludeProperty);
	List<T> findLike(T exampleInstance, int maxResults, int firstResult, List<String> likeQueryProperties, 
					 String... excludeProperty);
	List<T> find(T exampleInstance, int maxResults, int firstResult, List<OrderbyProperty> orderbys, 
				 String... excludeProperty);
	List<T> findLike(T exampleInstance, int maxResults, int firstResult, List<OrderbyProperty> orderbys, 
					 List<String> likeQueryProperties, String... excludeProperty);
	List<T> findNullProps(T exampleInstance, int maxResults, int firstResult, List<OrderbyProperty> orderbys, 
						  List<String> nullProps, String... excludeProperty);

	//get(T) is same as find(T) except it always returns the first result from query or null
	T findFirst(T example, String... excludeProperty);
	T findFirstLike(T example, List<String> likeQueryProperties, String... excludeProperty);
	T findFirstLike(T example, List<OrderbyProperty> orderbys, List<String> likeQueryProperties, String... excludeProperty);

	Long count();
	
	//Refresh entity from database. Entity must be a persistent or detached entity. 
	T refresh(T entity);
	
	// insert or update
	T insertOrUpdate(T entity);
	T persist(T entity);

	// insert
	T insert(T entity);
	T save(T entity);

    // update
    T update(T entity);

    // update
    List<T> update(List<T> entities);

	// delete
	void delete(T entity);
	void replicate(T entity);

	// Retrieve a proxy from DB. This is not real Object! Only use these methods
	// within transaction.
	// Exception will be thrown later if object can not found from DB!
	T loadById(ID id);

	T loadById(ID id, boolean lock);

	/**
	 * Affects every managed instance in the current persistence context!
	 */
	void flush();

	/**
	 * Affects every managed instance in the current persistence context!
	 */
	void clear();

	// merge
    T merge(T entity);
    
    List<T> findAll(QueryDTO dto);
    
//    /*
//     * Alternative methods without using Hibernate L2 cache
//     */
//     
//    // Retrieve an Object from DB. Return null if object is not found from DB.
//    T findByIdNoCache(ID id);
//
//    T findByIdNoCache(ID id, boolean lock);
//    
//    // Retrieve all instances from DB.
//    List<T> findAllNoCache();
//    List<T> findAllNoCache(Predicate... restrictions);
//    List<T> findAllNoCache(OrderbyProperty orderby);
//    List<T> findAllNoCache(List<OrderbyProperty>  orderBy, Predicate... restrictions);
//    
//    // Retrieve at most maxResults number of instances from DB.
//    List<T> findAllNoCache(int maxResults);
//    List<T> findAllNoCache(int maxResults, Predicate... restrictions);
//    List<T> findAllNoCache(int maxResults, List<OrderbyProperty> orderBy, Predicate... restrictions);
//
//    // Retrieve at most maxResults number of instances from DB,
//    // starting from the firstResult instance. e.g. default firstResult starts from 0.
//    List<T> findAllNoCache(int maxResults, int firstResult);
//    List<T> findAllNoCache(int maxResults, int firstResult, Predicate... restrictions);
//    List<T> findAllNoCache(int maxResults, int firstResult, List<OrderbyProperty> orderBy, Predicate... restrictions);
//    List<T> findAllNoCache(int maxResults, int firstResult, OrderbyProperty orderBy, Predicate... restrictions);
//
//    
//    //findNoCache() and findAllNoCache() will be the same in terms of functionality
//    List<T> findNoCache();
//    List<T> findNoCache(Predicate... restrictions);
//    List<T> findNoCache(List<OrderbyProperty> orderbys, Predicate... restrictions);
//    
//    List<T> findNoCache(int maxResults);
//    List<T> findNoCache(int maxResults, Predicate... restrictions);
//    List<T> findNoCache(int maxResults, List<OrderbyProperty> orderbys, Predicate... restrictions);
//    
//    List<T> findNoCache(int maxResults, int firstResult);
//    List<T> findNoCache(int maxResults, int firstResult, Predicate... restrictions);
//    List<T> findNoCache(int maxResults, int firstResult, List<OrderbyProperty> orderbys, Predicate... restrictions);
//    
//    List<T> findNoCache(T exampleInstance, String... excludeProperty);
//    List<T> findNoCache(T exampleInstance, List<OrderbyProperty> orderbys, String... excludeProperty);
//
//    List<T> findNoCache(T exampleInstance, int maxResults, String... excludeProperty);
//    List<T> findNoCache(T exampleInstance, int maxResults, List<OrderbyProperty> orderbys, String... excludeProperty);
//
//    List<T> findNoCache(T exampleInstance, int maxResults, int firstResult, String... excludeProperty);
//    List<T> findNoCache(T exampleInstance, int maxResults, int firstResult, List<OrderbyProperty> orderbys, String... excludeProperty);
//
//    //get(T) is same as find(T) except it always returns the first result from query or null
//    T findFirstNoCache(T example, String... excludeProperty);
}
