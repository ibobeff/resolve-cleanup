package com.resolve.persistence.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;

import com.resolve.services.hibernate.vo.ResolveMCPComponentVO;
import com.resolve.services.hibernate.vo.ResolveMCPHostStatusVO;
import com.resolve.services.hibernate.vo.ResolveMCPHostVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "resolve_mcp_host", uniqueConstraints = {@UniqueConstraint(columnNames = {"u_hostname", "u_cluster_id"}, name = "rmcphst_hstname_clstid_uk")},
        indexes = {
                @Index(columnList = "u_hostname", name = "rmh_u_hostname_idx"),
                @Index(columnList = "u_cluster_id", name = "rmh_cluster_id_idx"),
                @Index(columnList = "u_group_id", name = "rmh_group_id_idx")})
public class ResolveMCPHost extends BaseModel<ResolveMCPHostVO>
{
    private static final long serialVersionUID = -259016396485155816L;
    
    private String UHostName;
    private String UBluePrint;
    private String UHostIp;
    private String UStatus;
    private Boolean UIsMonitored;
    
    private ResolveMCPCluster cluster;
    private ResolveMCPGroup hostGroup;
    
    private Collection<ResolveMCPComponent> mcpComponents;
    
    // Collection of different status for this component
    private Collection<ResolveMCPHostStatus> UComponentStatusStatuses;
    
    public ResolveMCPHost()
    {
    }
    
    public ResolveMCPHost(ResolveMCPHostVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_hostname", length = 300, nullable=false)
    public String getUHostName()
    {
        return UHostName;
    }

    public void setUHostName(String uHostName)
    {
        UHostName = uHostName;
    }

    @Lob
    @Column(name = "u_blueprint", length = 16777215)
    public String getUBluePrint()
    {
        return UBluePrint;
    }

    public void setUBluePrint(String uBluePrint)
    {
        UBluePrint = uBluePrint;
    }

    @Column (name = "u_host_ip")
    public String getUHostIp()
    {
        return UHostIp;
    }

    public void setUHostIp(String uHostIp)
    {
        UHostIp = uHostIp;
    }

    @Column(name = "u_status")
    public String getUStatus()
    {
        return UStatus;
    }

    public void setUStatus(String uStatus)
    {
        UStatus = uStatus;
    }

    @Column(name = "u_is_monitored", length = 1)
    @Type(type = "yes_no")
    public Boolean getUIsMonitored()
	{
		return UIsMonitored;
	}

	public void setUIsMonitored(Boolean uIsMonitored)
	{
		UIsMonitored = uIsMonitored;
	}

	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_cluster_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveMCPCluster getCluster()
    {
        return cluster;
    }

    public void setCluster(ResolveMCPCluster cluster)
    {
        this.cluster = cluster;
    }
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "u_group_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public ResolveMCPGroup getHostGroup()
    {
        return hostGroup;
    }

    public void setHostGroup(ResolveMCPGroup group)
    {
        this.hostGroup = group;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "mcpHost", cascade=CascadeType.ALL)
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<ResolveMCPHostStatus> getUComponentStatusStatuses()
    {
        return UComponentStatusStatuses;
    }

    public void setUComponentStatusStatuses(Collection<ResolveMCPHostStatus> uComponentStatusStatuses)
    {
        UComponentStatusStatuses = uComponentStatusStatuses;
    }
    
    // Deprecated @ForeignKey should be used to match the other side of relationship
    @SuppressWarnings("deprecation")
    @org.hibernate.annotations.ForeignKey(name = "none")
    @OneToMany(mappedBy = "UComponentHost", cascade=CascadeType.ALL)
    @NotFound(action = NotFoundAction.IGNORE)
    public Collection<ResolveMCPComponent> getMcpComponents()
    {
        return mcpComponents;
    }

    public void setMcpComponents(Collection<ResolveMCPComponent> mcpComponents)
    {
        this.mcpComponents = mcpComponents;
    }

    @Override
    public ResolveMCPHostVO doGetVO()
    {
        ResolveMCPHostVO vo = new ResolveMCPHostVO();
        super.doGetBaseVO(vo);
        
        vo.setUHostName(getUHostName());
        vo.setUBluePrint(getUBluePrint());
        vo.setUHostIp(getUHostIp());
        vo.setUStatus(getUStatus());
        
        Collection<ResolveMCPHostStatus> mcpCompStatuses = Hibernate.isInitialized(this.getUComponentStatusStatuses()) ? getUComponentStatusStatuses() : null;
        if (mcpCompStatuses != null)
        {
            Collection<ResolveMCPHostStatusVO> mcpCompStatusVOs = new ArrayList<ResolveMCPHostStatusVO>();
            for (ResolveMCPHostStatus compStatus : mcpCompStatuses)
            {
                mcpCompStatusVOs.add(compStatus.doGetVO());
            }
            vo.setUComponentStatusStatuses(mcpCompStatusVOs);
        }
        
        Collection<ResolveMCPComponent> mcpComponents = Hibernate.isInitialized(this.getMcpComponents()) ? getMcpComponents() : null;
        if (mcpComponents != null)
        {
            Collection<ResolveMCPComponentVO> mcpComponentsVOs = new ArrayList<ResolveMCPComponentVO>();
            for (ResolveMCPComponent mcpComponent : mcpComponents)
            {
                mcpComponentsVOs.add(mcpComponent.doGetVO());
            }
            vo.setMcpComponents(mcpComponentsVOs);
        }
        
        return vo;
    }
    
    @Override
    public void applyVOToModel(ResolveMCPHostVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);
            
            this.setUHostName(StringUtils.isNotBlank(vo.getUHostName()) && vo.getUHostName().equals(VO.STRING_DEFAULT) ? getUHostName() : vo.getUHostName());
            this.setUBluePrint(StringUtils.isNotBlank(vo.getUBluePrint()) && vo.getUBluePrint().equals(VO.STRING_DEFAULT) ? getUBluePrint() : vo.getUBluePrint());
            this.setUHostIp(StringUtils.isNotBlank(vo.getUHostIp()) && vo.getUHostIp().equals(VO.STRING_DEFAULT) ? getUHostIp() : vo.getUHostIp());
            this.setUStatus(StringUtils.isNotBlank(vo.getUStatus()) && vo.getUStatus().equals(VO.STRING_DEFAULT) ? getUStatus() : vo.getUStatus());
        }
    }
}
