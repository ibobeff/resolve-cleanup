/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

//Generated Dec 11, 2009 11:15:20 AM by Hibernate Tools 3.2.4.GA

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;

import com.resolve.services.hibernate.vo.GatewayFilterVO;
import com.resolve.services.interfaces.CdataProperty;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

/**
 * Super class for all the gateway filter entities that stores all the common properties.
 * Gateway filters must have the properties defined by this class. That's the reason this
 * super class is added so subclasses only need to add the additional properties.
 */
@SuppressWarnings("serial")
@MappedSuperclass
public abstract class GatewayFilter<T> extends BaseModel<T> implements CdataProperty
{
    private String UName;
    private Boolean UActive;
    private Integer UOrder;
    private Integer UInterval;
    private String URunbook;
    private String UScript;
    private String UQueue;
    private String UEventEventId;

    public GatewayFilter()
    {
    } // BaseFilter

    @Column(name = "u_queue", length = 100)
    public String getUQueue()
    {
        return this.UQueue;
    } // getUQueue

    public void setUQueue(String UQueue)
    {
        this.UQueue = UQueue != null ? UQueue.trim() : UQueue;
    } // setUQueue

    @Column(name = "u_order")
    public Integer getUOrder()
    {
        return this.UOrder;
    } // getUOrder

    public void setUOrder(Integer UOrder)
    {
        this.UOrder = UOrder;
    } // setUOrder

    @Column(name = "u_runbook", length = 333)
    public String getURunbook()
    {
        return this.URunbook;
    } // getURunbook

    public void setURunbook(String URunbook)
    {
        this.URunbook = URunbook != null ? URunbook.trim() : URunbook;
    } // setURunbook

    @Column(name = "u_active")
    public Boolean getUActive()
    {
        return this.UActive;
    } // getUActive

    public void setUActive(Boolean UActive)
    {
        this.UActive = UActive;
    } // setUActive

    @Column(name = "u_interval")
    public Integer getUInterval()
    {
        return this.UInterval;
    }

    public void setUInterval(Integer UInterval)
    {
        this.UInterval = UInterval;
    }

    @Column(name = "u_name", length = 255)
    public String getUName()
    {
        return this.UName;
    } // getName

    public void setUName(String UName)
    {
        this.UName = UName != null ? UName.trim() : UName;
    } // setUName

    @Lob
    @Column(name = "u_script", length = 16777215)
    public String getUScript()
    {
        return this.UScript;
    }

    public void setUScript(String UScript)
    {
        this.UScript = UScript != null ? UScript.trim() : UScript;
    }

    @Column(name = "u_event_eventid", length = 100)
    public String getUEventEventId()
    {
        return UEventEventId;
    }

    public void setUEventEventId(String uEventEventId)
    {
        UEventEventId = uEventEventId != null ? uEventEventId.trim() : uEventEventId;
    }

    protected void doGetBaseVO(GatewayFilterVO vo)
    {
        super.doGetBaseVO(vo);

        vo.setUActive(getUActive());
        vo.setUEventEventId(getUEventEventId());
        vo.setUInterval(getUInterval());
        vo.setUName(getUName());
        vo.setUOrder(getUOrder());
        vo.setUQueue(getUQueue());
        vo.setURunbook(getURunbook());
        vo.setUScript(getUScript());
    }

    @Override
    public void applyVOToModel(T gatewayFilterVO)
    {
        if(gatewayFilterVO != null)
        {
            super.applyVOToModel(gatewayFilterVO);

            GatewayFilterVO vo = (GatewayFilterVO) gatewayFilterVO;
            this.setUActive(vo.getUActive() != null ? vo.getUActive() : getUActive());
            this.setUEventEventId(StringUtils.isNotBlank(vo.getUEventEventId()) && vo.getUEventEventId().equals(VO.STRING_DEFAULT) ? getUEventEventId() : vo.getUEventEventId());
            this.setUInterval(vo.getUInterval()!= null && vo.getUInterval().equals(VO.INTEGER_DEFAULT) ? getUInterval() : vo.getUInterval());
            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUOrder(vo.getUOrder()!= null && vo.getUOrder().equals(VO.INTEGER_DEFAULT) ? getUOrder() : vo.getUOrder());
            this.setUQueue(StringUtils.isNotBlank(vo.getUQueue()) && vo.getUQueue().equals(VO.STRING_DEFAULT) ? getUQueue() : vo.getUQueue());
            this.setURunbook(StringUtils.isNotBlank(vo.getURunbook()) && vo.getURunbook().equals(VO.STRING_DEFAULT) ? getURunbook() : vo.getURunbook());
            this.setUScript(StringUtils.isNotBlank(vo.getUScript()) && vo.getUScript().equals(VO.STRING_DEFAULT) ? getUScript() : vo.getUScript());
        }
    }
    
    public List<String> ugetCdataProperty()
    {
        List<String> list = new ArrayList<String>();
        list.add("UScript");
        
        return list;
    }//ugetCdataProperty
}
