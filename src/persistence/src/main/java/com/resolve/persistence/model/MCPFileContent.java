/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.resolve.services.hibernate.vo.MCPFileContentVO;
import com.resolve.services.interfaces.VO;

@SuppressWarnings("serial")
@Entity
@Table(name = "mcp_file_content", indexes = {@Index(columnList = "u_mcpfile_sys_id", name = "mfc_u_mcpfile_idx")})
public class MCPFileContent extends BaseModel<MCPFileContentVO>
{
    private byte[] content;
    
    private MCPFile mcpFile;
    
    public MCPFileContent()
    {
    }
    
    public MCPFileContent(MCPFileContentVO vo)
    {
        applyVOToModel(vo);
    }

    @Lob
    @Column(name = "u_content", length=Integer.MAX_VALUE)
    public byte[] getContent()
    {
        return content;
    }

    public void setContent(byte[] content)
    {
        this.content = content;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "u_mcpfile_sys_id", foreignKey = @ForeignKey(name = "none", value = ConstraintMode.NO_CONSTRAINT))
    @NotFound(action = NotFoundAction.IGNORE)
    public MCPFile getMcpFile()
    {
        return mcpFile;
    }

    public void setMcpFile(MCPFile mcpFile)
    {
        this.mcpFile = mcpFile;
        
        // one-way only - no collections on the other side
    }

    @Override
    public MCPFileContentVO doGetVO()
    {
        MCPFileContentVO vo = new MCPFileContentVO();
        super.doGetBaseVO(vo);
        
        vo.setContent(getContent());
        
        return vo;
    }

    @Override
    public void applyVOToModel(MCPFileContentVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setContent(vo.getContent() != null && vo.getContent().equals(VO.BYTE_ARRAY_DEFAULT) ? getContent() : vo.getContent());
        }
    }
}
