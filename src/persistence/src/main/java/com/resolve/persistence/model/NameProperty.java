/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.services.hibernate.vo.NamePropertyVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

@Entity
@Table(name = "name_property",  uniqueConstraints = {@UniqueConstraint(columnNames = {"u_name", "u_queue"}, name = "nameprop_u_name_u_queue_uk")})
// @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class NameProperty extends BaseModel<NamePropertyVO>
{
    private static final long serialVersionUID = -380580408412474074L;

    private String UName;
    private String UQueue;
    private byte[] UContent;

    public NameProperty()
    {
    }

    public NameProperty(NamePropertyVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_name", length = 128)
    public String getUName()
    {
        return UName;
    }

    public void setUName(String uName)
    {
        UName = uName;
    }

    @Column(name = "u_queue", length = 40)
    public String getUQueue()
    {
        return this.UQueue;
    }

    public void setUQueue(String queue)
    {
        this.UQueue = queue;
    }

    @Lob
    @Column(name = "u_content", length = 16777215)
    public byte[] getUContent()
    {
        return UContent;
    }

    public void setUContent(byte[] uContent)
    {
        UContent = uContent;
    }

    @Override
    public NamePropertyVO doGetVO()
    {
        NamePropertyVO vo = new NamePropertyVO();
        super.doGetBaseVO(vo);

        vo.setUName(getUName());
        vo.setUQueue(getUQueue());
        vo.setUContent(getUContent());

        return vo;
    }

    @Override
    public void applyVOToModel(NamePropertyVO vo)
    {
        if(vo != null)
        {
            super.applyVOToModel(vo);

            this.setUName(StringUtils.isNotBlank(vo.getUName()) && vo.getUName().equals(VO.STRING_DEFAULT) ? getUName() : vo.getUName());
            this.setUQueue(StringUtils.isNotBlank(vo.getUQueue()) && vo.getUQueue().equals(VO.STRING_DEFAULT) ? getUQueue() : vo.getUQueue());
            this.setUContent(vo.getUContent() != null ? getUContent() : vo.getUContent());
        }
    }
}

