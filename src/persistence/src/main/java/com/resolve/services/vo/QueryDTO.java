/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.PropertiesUtil;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONSerializer;

/**
 * This class encapsulates the query params. This is a generic class which can be used from the UI, Search as well as internal apis. 
 * Each api using this class may populate different attributes. So it is better to refer this class based on the api that one is working/testing on.
 * 
 * For the UI, generally it is used to get data for the grid. Here is an example which will give a better idea. 
 * UI Example:
 *  filter: [{"field":"uisActive","type":"bool","condition":"equals","value":true}]
 *  page: 3
 *  start: 100
 *  limit: 50
 *  group: [{"property":"unamespace","direction":"ASC"}]
 *  sort: [{"property":"unamespace","direction":"ASC"},{"property":"uhasActiveModel","direction":"DESC"}]
 *  
 *  
 *  Here is the example for search (Elastic Search) related queries
 *  UI search Example:  
 *  page:1
 *  start:0
 *  limit:15
 *  filter:[{"field":"","type":"auto","condition":"contains","value":"test"}]
 *  
 * 
 * 
 *
 * @author jeet.marwah
 *
 */
public class QueryDTO
{
    public static final String TABLE_TYPE = "table";
    public static final String FORM_TYPE = "form";
    private static final String ORDER_BY = "order by";

    public static final String AND_OPERATOR = "and";
    public static final String OR_OPERATOR = "or";

    // by default the operator is and, caller may set it to or as well
    private String operator;

    // if these are present, it will ignore the rest of the params to prepare
    // the query, can be used for complicated joins and query
    private String sqlQuery;
    private String hql;
    //this field is used when we want to query a certain RSSearch index
    //or document in it. This will be a free form search
    private String searchTerm;
    //in general used for aggregation query against ES 
    private String interval; //second,minute,hour,day,week,month,quarter,year, or some number

    // convenience to query
    private String selectColumns;// list of comma seperated cols for the query
    private String excludeColumns;// list of comma seperated cols to be excluded from the result
    private String tableName;// this is for Database table
    private String modelName;// this is for Hibernate table
    private String whereClause;
    private String orderBy;

    // will be either TABLE_TYPE (used for Grid, more than 1 rec) or FORM_TYPE
    // (used for Form, only 1 rec)
    private String type;// grid or form - grid will return a list of records and
                        // form will return only 1 record

    // for pagination and optional params
    private int page = 0;
    private int start = -1;
    private int limit = -1;// # of recs
//    private String sort;// from the ext action
//    private String filter;// from the ext action
    private String query;// comboboxes or basic search on a grid

    // params that are passed in the url
    private Map<String, String> urlParams;

    // used to indicate if its SQL or HQL
    private boolean useSql = false;

    // use when there is a join
    private String prefixTableAlias = "";

    private List<QueryFilter> filters = new ArrayList<QueryFilter>(); //setFilter(string) from ext action
    private List<QuerySort> sorts = new ArrayList<QuerySort>(); //setSort(string) from ext action

    // this Map stores the query parameter name and their values at runtime.
    // for example if the search criteria was UName='bob' and the HQL query
    // added the parameter as UName_0 then this Map will store UName_0 with
    // value "bob".
    // this is used to populated the hibernate Query object's parameters.
    private Map<String, Object> params = new HashMap<String, Object>();

    private List<QuerySort> parsedSorts = new ArrayList<QuerySort>();
    
    /*
     * select HQL distinct columns/fields will perform group by on 
     * selected columns/fields for generated HQL query.
     * 
     * NOTE: Supports simple field types only. Select distinct models
     *       is not supported. See the note below for select distinct models
     *       with or without start and limit.
     * 
     * select distinct model ids using group by model.sys_id with start and limit 
     * and then select models corresponding to selected sys_ids should be implemented here
     * and GeneralHibernateUtil. If start and limit is not 
     * there then returned list should be filtered using Criteria.DISTINCT_ROOT_ENTITY
     * result transformer. Please check PlaybookUtils.listSecurityIncidents() by 
     * Activity owner and status for example.
     */
    
    private boolean hqlDistinct = false;
    
    //Queries on base table using mapped data should set this flag to true
    
    private boolean hqlDistinctMap = false;
    
    /*
     * By default, the operator (AND/OR) between 'whereClause' and the filter(s) is defined by
     * 'operator' field. This customOperator, if populated, will overwrite the operator between
     * 'whereClause' and filter(s).
     */
    private String customOperator;
    
    public enum QUERY_COMPARISION
    {
        equals, notEquals, contains, notContains, startsWith, notStartsWith, endsWith, notEndsWith, startLeaf
    }

    public QueryDTO()
    {
        operator = AND_OPERATOR;
    }

    public QueryDTO(int start, int limit)
    {
        this(start, limit, AND_OPERATOR);
    }

    public QueryDTO(int start, int limit, String operator)
    {
        this.start = start;
        this.limit = limit;
        this.operator = operator;
    }

    public QueryDTO(QueryDTO org)
    {
        // Deep copy
        
        setOperator(org.getOperator());
        setSqlQuery(org.getSqlQuery());
        setHql(org.getHql());
        setSearchTerm(org.getSearchTerm());
        setInterval(org.getInterval());
        setSelectColumns(org.getSelectColumns());
        setExcludeColumns(org.getExcludeColumns());
        setTableName(org.getTableName());
        setModelName(org.getModelName());
        setWhereClause(org.getWhereClause());
        setOrderBy(org.getOrderBy());
        setType(org.getType());
        setPage(org.getPage());
        setStart(org.getStart());
        setLimit(org.getLimit());
        setQuery(org.getQuery());
        
        if (org.getUrlParams() != null && !org.getUrlParams().isEmpty())
        {
            Map<String, String>  newUrlParams = new HashMap<String, String>(org.getUrlParams().size());
            
            for (String paramKey : org.getUrlParams().keySet())
            {
                newUrlParams.put(paramKey, org.getUrlParams().get(paramKey));
            }
            
            setUrlParams(newUrlParams);
        }
        
        setUseSql(org.isUseSql());
        setPrefixTableAlias(org.getPrefixTableAlias());
        
        if (!org.getFilters().isEmpty())
        {
            List<QueryFilter> newFilters = new ArrayList<QueryFilter>(org.getFilters().size());
            
            newFilters.addAll(org.getFilters());
            
            setFilters(newFilters);
        }
        
        if (!org.getSortItems().isEmpty())
        {
            List<QuerySort> newSorts = new ArrayList<QuerySort>(org.getSortItems().size());
            
            newSorts.addAll(org.getSortItems());
            
            setSortItems(newSorts);
        }
        
        if (!org.getParams().isEmpty())
        {
            Map<String, Object> newParams = new HashMap<String, Object>(org.getParams().size());
            
            for (String paramKey : org.getParams().keySet())
            {
                newParams.put(paramKey, org.getParams().get(paramKey));
            }
            
            setParams(newParams);
        }
    }
    
    public String getSelectHQL()
    {
        // parse it first
        parseQuery(false);

        // prepare the qry
        StringBuilder executeSql = new StringBuilder("select ");
        // String tableDataStr = "";

        StringBuilder distinctGroupBySuffix = new StringBuilder();
        if (StringUtils.isNotBlank(getSelectColumns()))
        {
            if (getHqlDistinct())
            {
                String[] selectedColumns = StringUtils.split(getSelectColumns(), ",");
                
                for (int i = 0; i < selectedColumns.length; i++)
                {
                    if (i > 0)
                    {
                        executeSql.append(", ");
                        distinctGroupBySuffix.append(", ");
                    }
                    executeSql.append(getPrefixTableAlias()).append(".").append(selectedColumns[i].trim());
                    distinctGroupBySuffix.append(getPrefixTableAlias()).append(".").append(selectedColumns[i].trim());
                }
            }
            else
            {
                executeSql.append(getSelectColumns());
                // Reset table alias prefix
                setPrefixTableAlias(null);
            }
        }
        else
        {
            executeSql.append("tableData");
            //setPrefixTableAlias("tableData");
            
            if (getHqlDistinctMap()) {
            	distinctGroupBySuffix.append("tableData.sys_id");
            }
        }

        executeSql.append(" from ");

        executeSql.append(getModelName());
        
        if (StringUtils.isBlank(getSelectColumns()))
        {
            executeSql.append(" tableData ");
            // tableDataStr = "tableData.";
        }
        
        if (StringUtils.isNotBlank(getPrefixTableAlias()))
        {
            executeSql.append(" " + getPrefixTableAlias());
        }
        
        // where clause
        if (StringUtils.isNotBlank(getWhereClause()))
        {
            executeSql.append(" where ").append(getWhereClause());
        }

        String whereClause = getFilterWhereClause();
        if (StringUtils.isNotBlank(whereClause))
        {
            if (executeSql.indexOf("where") > 0)
            {
                String operator = (StringUtils.isBlank(getCustomOperator()) || 
                                !(getCustomOperator().equalsIgnoreCase(AND_OPERATOR) || getCustomOperator().equalsIgnoreCase(OR_OPERATOR))) ? getOperator() : getCustomOperator();
                executeSql.append(" " + operator + " ").append(whereClause);
            }
            else
            {
                executeSql.append(" where ").append(whereClause);
            }

        }

        // Process distinct for HQL by adding group by

        if (getHqlDistinct() || getHqlDistinctMap())
        {
            if (distinctGroupBySuffix.length() > 0)
            {
                executeSql.append(" group by ").append(distinctGroupBySuffix);
            }
        }
        
        // order by
        if (StringUtils.isNotBlank(getSort()))
        {
            executeSql.append(" order by lower(").append(getSort()).append(")");
        }
        else if (StringUtils.isNotBlank(getOrderBy()))
        {
            executeSql.append(" order by ").append(getOrderBy());
        }

        return executeSql.toString();
    }

    public List<QueryParameter> getWhereParameters()
    {
        List<QueryParameter> params = new ArrayList<QueryParameter>();

//        List<QueryFilter> filters = QueryFilter.decodeJson(filter);
        this.params.clear();
        for (QueryFilter filter : filters)
        {
            //evaluate the start count
            filter.setStartCount(evaluateStartCount(filter));

            List<QueryParameter> temp = filter.getParameters();
            for (QueryParameter p : temp)
            {
                params.add(p);
                this.params.put(p.getName(), p.getValue());
            }
        }

        return params;
    }

    public void parseSortClause()
    {
        // [{"property":"name","direction":"DESC"}]
//        if (StringUtils.isNotBlank(sort))
//        {
        if (!sorts.isEmpty())
        {
            StringBuilder orderBy = new StringBuilder();
//            List<QuerySort> sorts = QuerySort.decodeJson(sort);
            orderBy.setLength(0);
            for (QuerySort sort : sorts)
            {
                orderBy.append(" " + getPrefixTableAlias() + sort.getProperty() + " " + sort.getDirection() + " ,");
            }
            orderBy.setLength(orderBy.length() - 1);
            setSort("");
            setOrderBy(orderBy.toString());
        }
//        else if(StringUtils.isNotBlank(getOrderBy()))
//        {
//            StringBuilder orderBy = new StringBuilder();
//            String orderByWithAlias = getOrderBy();
//            String[] orderByArr = orderByWithAlias.split(",");
//            for(String ob : orderByArr)
//            {
//                orderBy.append(getPrefixTableAlias() + ob + ",");
//            }
//            orderBy.setLength(orderBy.length() - 1);
//            setSort("");
//            setOrderBy(orderBy.toString());
//        }
    }

    public List<QueryFilter> getFilterItems()
    {
        return filters;
    }

    public void setFilterItems(List<QueryFilter> filters)
    {
        this.filters = filters;
    }

    public QueryDTO addFilterItem(QueryFilter filter)
    {
    	this.filters.add(filter);
        return this;
    }

    public List<QueryFilter> getFilters()
    {
        return filters;
    }

    public void setFilters(List<QueryFilter> filters)
    {
        this.filters = filters;
    }

    public String getSelectSQL()
    {
        // parse it first
        parseQuery(false);

        // prepare the qry
        StringBuffer executeSql = new StringBuffer("select ");

        if (StringUtils.isNotBlank(getSelectColumns()))
        {
            executeSql.append(getSelectColumns());
        }
        else
        {
            executeSql.append("*");
        }

        executeSql.append(" from ");

        if (isUseSql())
        {
            executeSql.append(getTableName());
        }
        else
        {
            executeSql.append(getModelName());
        }

        // where clause
        if (StringUtils.isNotBlank(getWhereClause()))
        {
            executeSql.append(" where ").append(getWhereClause());
        }

        // order by
        if (StringUtils.isNotBlank(getSort()))
        {
            executeSql.append(" order by ").append(getSort());
        }
        else if (StringUtils.isNotBlank(getOrderBy()))
        {
            executeSql.append(" order by ").append(getOrderBy());
        }

        // Convert it to parameterized query and pass set of parameter values
        return executeSql.toString();
    }
    
    public String getSelectSQLNonParameterizedSort()
    {
        // parse it first
        parseQuery(false);

        // prepare the qry
        StringBuffer executeSql = new StringBuffer("select ");

        if (StringUtils.isNotBlank(getSelectColumns()))
        {
            executeSql.append(getSelectColumns());
        }
        else
        {
            executeSql.append("*");
        }

        executeSql.append(" from ");

        if (isUseSql())
        {
            executeSql.append(getTableName());
        }
        else
        {
            executeSql.append(getModelName());
        }

        // where clause
        if (StringUtils.isNotBlank(getWhereClause()))
        {
            executeSql.append(" where ").append(getWhereClause());
        }

        // order by
        if (StringUtils.isNotBlank(getSort()))
        {
            executeSql.append(" order by ").append(getSort());
        }
        else if (StringUtils.isNotBlank(getOrderBy()))
        {
            executeSql.append(" order by ").append(getOrderBy());
        }

        // Convert it to parameterized query and pass set of parameter values
        return executeSql.toString();
    }
    
    public String getDeleteQuery()
    {
        // prepare the qry
        StringBuffer deleteSql = new StringBuffer("delete from ");

        if (StringUtils.isNotEmpty(getTableName()))
        {
            deleteSql.append(getTableName());
        }

        if (StringUtils.isNotBlank(getWhereClause()))
        {
            if (getWhereClause().equals("-1"))
            {
                // this will delete everything in the table
            }
            else
            {
                deleteSql.append(" where ").append(getWhereClause());
            }
        }
        else
        {
            // this is not allowed as a precaution to delete everything in the
            // table..so setting the sql to empty string
            deleteSql = new StringBuffer();
        }

        return deleteSql.toString();
    }

    public String getCountQuery()
    {
        String selectSql = getSelectSQL();
        
        String countSql;
        
        if (selectSql.contains(" order by "))
        {
            countSql = "select COUNT(*) as COUNT from " + selectSql.substring(selectSql.indexOf("from") + "from".length(), 
                                                                              selectSql.indexOf(" order by "));
        }
        else
        {
            countSql = "select COUNT(*) as COUNT from " + selectSql.substring(selectSql.toLowerCase().indexOf("from") + "from".length());
        }
        return countSql;
    }

    public String getSelectCountHQL()
    {
        String selectHql = getSelectHQL();
        int startIndxOfTableOrModel = selectHql.indexOf("from");
        
        if (startIndxOfTableOrModel < 0)
        {
            Log.log.error("Could not find \"from \" in generated select HQL statement [" + selectHql + "]");
            throw new RuntimeException ("Error in getting total count of recrods.");
        }
        
        startIndxOfTableOrModel += "from".length();
        
        int offset = StringUtils.stripEnd(selectHql.substring(startIndxOfTableOrModel), " ").length() - selectHql.substring(startIndxOfTableOrModel).trim().length();
        
        if (offset > 0)
        {
            startIndxOfTableOrModel += offset;
        }
        
        int endIndxOfTableOrModel = selectHql.indexOf(" ", startIndxOfTableOrModel);
        
        if (endIndxOfTableOrModel > startIndxOfTableOrModel)
        {
            if (selectHql.contains(" order by "))
            {
                return "select COUNT(*) as COUNT from " + selectHql.substring(startIndxOfTableOrModel, endIndxOfTableOrModel) + StringUtils.stripStart(selectHql.substring(endIndxOfTableOrModel, selectHql.indexOf(" order by ") ), "tableData");
            }
            else
            {
                return "select COUNT(*) as COUNT from " + selectHql.substring(startIndxOfTableOrModel, endIndxOfTableOrModel) + StringUtils.stripStart(selectHql.substring(endIndxOfTableOrModel), "tableData");
            }
        }
        else
        {
            if (selectHql.contains(" order by "))
            {
                return "select COUNT(*) as COUNT from " + selectHql.substring(startIndxOfTableOrModel, selectHql.indexOf(" order by "));
            }
            else
            {
                return "select COUNT(*) as COUNT from " + selectHql.substring(startIndxOfTableOrModel);
            }
        }
    }

    public String getCountQueryUsingHQL()
    {
        String selectHql = getHql();
        
        int startIndxOfTableOrModel = selectHql.indexOf("from");
        
        if (startIndxOfTableOrModel < 0)
        {
            Log.log.error("Could not find \"from \" in generated select HQL statement [" + selectHql + "]");
            throw new RuntimeException ("Error in getting total count of recrods.");
        }
        
        startIndxOfTableOrModel += "from".length();
        
        int offset = StringUtils.stripEnd(selectHql.substring(startIndxOfTableOrModel), " ").length() - selectHql.substring(startIndxOfTableOrModel).trim().length();
        
        if (offset > 0)
        {
            startIndxOfTableOrModel += offset;
        }
        
        int endIndxOfTableOrModel = selectHql.indexOf(" ", startIndxOfTableOrModel);
        
        if (endIndxOfTableOrModel > startIndxOfTableOrModel)
        {
            return "select COUNT(*) as COUNT from " + selectHql.substring(startIndxOfTableOrModel, endIndxOfTableOrModel) + StringUtils.stripStart(selectHql.substring(endIndxOfTableOrModel), "tableData");
        }
        else
        {
            return "select COUNT(*) as COUNT from " + selectHql.substring(startIndxOfTableOrModel);
        }
    }

    public String getTableName()
    {
        return tableName;
    }

    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }

    public String getWhereClause()
    {
        return whereClause;
    }

    public void setWhereClause(String whereClause)
    {
        this.whereClause = whereClause;
    }

    public String getOrderBy()
    {
        return orderBy;
    }

    public void setOrderBy(String orderBy)
    {
        this.orderBy = orderBy;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Map<String, String> getUrlParams()
    {
        return urlParams;
    }

    public void setUrlParams(Map<String, String> urlParams)
    {
        this.urlParams = urlParams;
    }

    public String getModelName()
    {
        return modelName;
    }

    public void setModelName(String modelName)
    {
        this.modelName = modelName;
    }

    public String getSqlQuery()
    {
        return sqlQuery;
    }

    public void setSqlQuery(String sqlQuery)
    {
        this.sqlQuery = sqlQuery;
    }

    public String getHql()
    {
        return hql;
    }

    public void setHql(String hql)
    {
        this.hql = hql;
    }

    public String getSearchTerm()
    {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm)
    {
        this.searchTerm = searchTerm;
    }

    public String getInterval()
    {
        return interval;
    }

    public void setInterval(String interval)
    {
        this.interval = interval;
    }

    public String getSelectColumns()
    {
        return selectColumns;
    }

    public void setSelectColumns(String selectColumns)
    {
        this.selectColumns = selectColumns;
    }

    public String getExcludeColumns()
    {
        return excludeColumns;
    }

    public void setExcludeColumns(String excludeColumns)
    {
        this.excludeColumns = excludeColumns;
    }

    public int getStart()
    {
        return start;
    }

    public void setStart(int start)
    {
        this.start = start;
    }

    public int getLimit()
    {
        return limit;
    }

    public void setLimit(int limit)
    {
        this.limit = limit;
    }

    public String getSort()
    {
//        return sort;
        String sort = null;
        if (!sorts.isEmpty())
        {
            sort = JSONSerializer.toJSON(sorts).toString();
        }
        return sort;
    }

    public void setSort(String sort)
    {
//        this.sort = sort;
        this.sorts = QuerySort.decodeJson(sort);
    }

    public List<QuerySort> getSortItems()
    {
//        if(sorts.size() == 0 && StringUtils.isNotBlank(sort))
//        {
//            sorts = QuerySort.decodeJson(sort);
//        }
        return sorts;
    }

    public void setSortItems(List<QuerySort> sorts)
    {
//        this.sort = JSONSerializer.toJSON(sortItems).toString();
        this.sorts = sorts;
    }

    public void addSortItem(QuerySort sortItem)
    {
        getSortItems().add(sortItem);
    }

    public String getFilter()
    {
//        return filter;
        String filter = null;
        if (!filters.isEmpty())
        {
            filter = JSONSerializer.toJSON(filters).toString(); 
        }
        return filter;
    }

    public void setFilter(String filter)
    {
//        this.filter = filter;
        this.filters = QueryFilter.decodeJson(filter);
    }

    private void parseQuery(boolean parameterizeSort)
    { 
        if (StringUtils.isNotBlank(getHql()) || StringUtils.isNotBlank(getSqlQuery()))
        {
            String sql = "";
            String sqlForIndexPosition = "";
            if (isUseSql())
            {
                sql = getSqlQuery();
            }
            else
            {
                sql = getHql();
            }
            
            sql = sql.trim();
            sqlForIndexPosition = sql.toLowerCase();// doing this to parse it

            // select * from test where abc = '' order by ts
            String selectColumns = sql.substring(sqlForIndexPosition.indexOf("select") + "select".length(), sqlForIndexPosition.indexOf("from"));

            if (selectColumns.contains(";") || selectColumns.contains("--"))
            {
                throw new RuntimeException("Invalid query");
            }
                            
            String tableName = "";
            StringBuilder whereClause = new StringBuilder();
            StringBuilder orderBy = new StringBuilder();
            
            String postFromTxt;
            int tmpTableNameEndIndx;
            String tmpTableName = null;
            String customTableName = null;
            int tableNameStartIndx;            
            String postTableNameTxt;
            
            if (sqlForIndexPosition.contains(" where"))
            {
                postFromTxt = sqlForIndexPosition.substring(sqlForIndexPosition.indexOf("from") + "from".length(), sqlForIndexPosition.indexOf(" where"));
                postFromTxt = StringUtils.strip(postFromTxt);
                
                if (StringUtils.isBlank(postFromTxt))
                {
                    throw new RuntimeException("Invalid query");
                }
                
                tmpTableNameEndIndx = postFromTxt.indexOf(" ");
                
                Log.log.trace("QueryDTO.parseQuery(): postFromTxt : [" + postFromTxt + "], tmpTableNameEndIndx = " + tmpTableNameEndIndx);
                
                tmpTableName = tmpTableNameEndIndx == -1 ? postFromTxt : postFromTxt.substring(0, tmpTableNameEndIndx);
                
                Log.log.trace("QueryDTO.parseQuery(): tmpTableName = [" + tmpTableName + "]");
                
                tableNameStartIndx = sqlForIndexPosition.indexOf(tmpTableName);
                
                Log.log.trace("QueryDTO.parseQuery(): tableNameStartIndx = " + tableNameStartIndx + ", tableNameEndIndx = " + (tableNameStartIndx + tmpTableName.length()));
                
                customTableName = sql.substring(tableNameStartIndx, tableNameStartIndx + tmpTableName.length());
                // using previous way to handle nested sql strings
                tableName = sql.substring(sqlForIndexPosition.indexOf("from") + "from".length(), sqlForIndexPosition.lastIndexOf("where"));
                
                Log.log.trace("QueryDTO.parseQuery(): tableName = [" + customTableName + "]");
                
                if (sqlForIndexPosition.contains(ORDER_BY))
                {
                    whereClause.append(sql.substring(sqlForIndexPosition.lastIndexOf("where") + "where".length(), sqlForIndexPosition.lastIndexOf(ORDER_BY)));
                    orderBy.append(sql.substring(sqlForIndexPosition.lastIndexOf(ORDER_BY) + ORDER_BY.length()));
                }
                else
                {
                    whereClause.append(sql.substring(sqlForIndexPosition.lastIndexOf("where") + "where".length()));
                }

            }
            else if (sqlForIndexPosition.contains(ORDER_BY))
            {
                postFromTxt = sqlForIndexPosition.substring(sqlForIndexPosition.indexOf("from") + "from".length(), sqlForIndexPosition.indexOf(" order by "));
                
                postFromTxt = StringUtils.strip(postFromTxt);
                
                if (StringUtils.isBlank(postFromTxt))
                {
                    throw new RuntimeException("Invalid query");
                }
                
                tmpTableNameEndIndx = postFromTxt.indexOf(" ");
                
                Log.log.trace("QueryDTO.parseQuery(): postFromTxt : [" + postFromTxt + "], tmpTableNameEndIndx = " + tmpTableNameEndIndx);
                
                tmpTableName = tmpTableNameEndIndx == -1 ? postFromTxt : postFromTxt.substring(0, tmpTableNameEndIndx);
                
                Log.log.trace("QueryDTO.parseQuery(): tmpTableName = [" + tmpTableName + "]");
                
                tableNameStartIndx = sqlForIndexPosition.indexOf(tmpTableName);
                
                Log.log.trace("QueryDTO.parseQuery(): tableNameStartIndx = " + tableNameStartIndx + ", tableNameEndIndx = " + (tableNameStartIndx + tmpTableName.length()));
                
                customTableName = sql.substring(tableNameStartIndx, tableNameStartIndx + tmpTableName.length());
                
                Log.log.trace("QueryDTO.parseQuery(): tableName = [" + customTableName + "]");
                // using previous way to handle nested sql strings
                tableName = sql.substring(sqlForIndexPosition.indexOf("from") + "from".length(), sqlForIndexPosition.lastIndexOf(ORDER_BY));
                
                orderBy.append(sql.substring(sqlForIndexPosition.lastIndexOf(ORDER_BY) + ORDER_BY.length()));
            }
            else
            {
                postFromTxt = sqlForIndexPosition.substring(sqlForIndexPosition.indexOf("from") + "from".length());
                
                postFromTxt = StringUtils.strip(postFromTxt);
                
                if (StringUtils.isBlank(postFromTxt))
                {
                    throw new RuntimeException("Invalid query");
                }
                
                tmpTableNameEndIndx = postFromTxt.indexOf(" ");
                
                Log.log.trace("QueryDTO.parseQuery(): postFromTxt : [" + postFromTxt + "], tmpTableNameEndIndx = " + tmpTableNameEndIndx);
                
                tmpTableName = tmpTableNameEndIndx == -1 ? postFromTxt : postFromTxt.substring(0, tmpTableNameEndIndx);
                
                Log.log.trace("QueryDTO.parseQuery(): tmpTableName = [" + tmpTableName + "]");
                
                tableNameStartIndx = sqlForIndexPosition.indexOf(tmpTableName);
                
                Log.log.trace("QueryDTO.parseQuery(): tableNameStartIndx = " + tableNameStartIndx + ", tableNameEndIndx = " + (tableNameStartIndx + tmpTableName.length()));
                
                customTableName = sql.substring(tableNameStartIndx, tableNameStartIndx + tmpTableName.length());
                
                tableName = sql.substring(sqlForIndexPosition.indexOf("from") + "from".length());
                
                Log.log.trace("QueryDTO.parseQuery(): tableName = [" + tableName + "]");
                
                if (sql.length() > tableNameStartIndx + tmpTableName.length())
                {
                    postTableNameTxt = sql.substring(tableNameStartIndx + tmpTableName.length());
                    postTableNameTxt = StringUtils.strip(postTableNameTxt);
                    
                    Log.log.trace("QueryDTO.parseQuery(): postTableNameTxt = [" + postTableNameTxt + "]");
                    
                    if (StringUtils.isNotBlank(postTableNameTxt))
                    {
                        Log.log.debug("QueryDTO.parseQuery(): prefixTableAlias = [" + prefixTableAlias + "]");
                        prefixTableAlias = postTableNameTxt;
                    }
                }
                //tableName = sql.substring(sqlForIndexPosition.indexOf("from") + "from".length());
            }
            customTableName = customTableName.trim();
            tableName = tableName.trim();

            // update the query object
            setSelectColumns(selectColumns);
            if (isUseSql())
            {
//                commented out code as it caused nested sql statements to fail
//                boolean isCustomTable = CustomTableMappingUtil.tableExists(tableName);
//                
//                String className;
//                
//                if (isCustomTable)
//                {
//                    className = CustomTableMappingUtil.table2Class(tableName);
//                    
//                    if (StringUtils.isEmpty(className))
//                    {
//                        throw new RuntimeException("Invalid query");
//                    }
//                    
//                    setTableName(CustomTableMappingUtil.class2TableTrueCase(className));
//                }
//                else
//                {
//                    className = HibernateUtil.table2Class(tableName);
//                    
//                    if (StringUtils.isEmpty(className))
//                    {
//                        throw new RuntimeException("Invalid query");
//                    }
//                    
//                    setTableName(HibernateUtil.class2Table(className));
//                }
                setTableName(tableName);
            }
            else
            {
                String tblName = HibernateUtil.class2Table(customTableName);
                
                if (StringUtils.isEmpty(tblName))
                {
                    // Check if it is Resolve model name prefixed without "com.resolve.presistence.model."
                    
                    if (customTableName.startsWith("com.resolve.persistence.model."))
                    {
                        throw new RuntimeException("Invalid query");
                    }
                    
                    tblName = HibernateUtil.class2Table("com.resolve.persistence.model." + customTableName);
                    
                    if (StringUtils.isEmpty(tblName))
                    {
                        throw new RuntimeException("Invalid query");
                    }
                }
                
                setModelName(HibernateUtil.table2Class(tblName));
            }

            // Process extjs where clause and append it here before setting the
            // where clause
//            if (StringUtils.isNotBlank(filter))
//            {
//                List<QueryFilter> filters = QueryFilter.decodeJson(filter);
                if (filters.size() > 0)
                {
                    whereClause.setLength(0);
                    for (QueryFilter filter : filters)
                    {
                        //evaluate the start count
//                        filter.setStartCount(evaluateStartCount(filter));

                        whereClause.append(filter.getWhereClause(getPrefixTableAlias()) + " " + operator);
                    }
                    whereClause.setLength(whereClause.length() - 3);
                    setFilter("");
                }
//            }

            setWhereClause(whereClause.toString());

            // Process extjs sort and replace it if one already exists
//            if (StringUtils.isNotBlank(sort))
//            {
//                List<QuerySort> sorts = QuerySort.decodeJson(sort);
            
            parsedSorts.clear();
            
            if (!sorts.isEmpty())
            {
                orderBy.setLength(0);
                for (QuerySort sort : sorts)
                {
                    String parameterizedPropName = parameterizeSort ? 
                                                   ":" + QuerySort.SORT_PROP_NAME_PREFIX + sort.getProperty().replaceAll("\\.", "_") : 
                                                   sort.getProperty();
                    
                    orderBy.append(" " + parameterizedPropName + " " + sort.getDirection() + " ,");
                    
                    if (parameterizeSort)
                    {
                        parsedSorts.add(sort);
                    }
                }
                orderBy.setLength(orderBy.length() - 1);
                setSort("");
            }

            setOrderBy(orderBy.toString());
        }
        else
        {
            // Always process extjs sort and replace it if one already exists
//            if (StringUtils.isNotBlank(sort))
//            {
            // If select distinct HQL is set and model/table alias prefix is not set then set it to default "tableData"
            if (getHqlDistinct() && StringUtils.isBlank(getPrefixTableAlias()))
            {
                setPrefixTableAlias("tableData");
            }
            
            if (!sorts.isEmpty())
            {
                StringBuilder orderBy = new StringBuilder();
//                List<QuerySort> sorts = QuerySort.decodeJson(sort);
                orderBy.setLength(0);
                for (QuerySort sort : sorts)
                {
                    String parameterizedPropName = parameterizeSort ? 
                                                   ":" + QuerySort.SORT_PROP_NAME_PREFIX + sort.getProperty().replaceAll("\\.", "_") :
                                                   sort.getProperty();
                    
                    orderBy.append(" " + (StringUtils.isNotBlank(selectColumns) && 
                                          StringUtils.isNotBlank(prefixTableAlias) ? 
                                          prefixTableAlias + "." : "") + 
                                   parameterizedPropName + " " + sort.getDirection() + " ,");
                    
                    if (parameterizeSort)
                    {
                        parsedSorts.add(sort);
                    }
                }
                orderBy.setLength(orderBy.length() - 1);
                setSort("");
                setOrderBy(orderBy.toString());
            }
        }
        
        if (StringUtils.isNotBlank(/*StringUtils.substringBetween(*/getOrderBy()/*, "(", ")"))*/))
        {
            List<String> dbOrderbyFunctionBlackList = PropertiesUtil.getPropertyList(Constants.SECURITY_DB_ORDER_BY_FUNCTIONS_BLACKLIST_SYSTEM_PROPERTY);
            
            if (dbOrderbyFunctionBlackList != null && !dbOrderbyFunctionBlackList.isEmpty())
            {
               for (String dbBlackListedOrderByFunction : dbOrderbyFunctionBlackList)
               {
                   if (getOrderBy().toLowerCase().contains(dbBlackListedOrderByFunction) && 
                       getOrderBy().toLowerCase().contains(dbBlackListedOrderByFunction + "("))
                   {
                       throw new RuntimeException("Unsupported function in order by clause.");
                   }
               }
            }
        }
    }

    public String getFilterWhereClause()
    {
        StringBuilder whereClause = new StringBuilder();
//        if (StringUtils.isNotBlank(filter))
//        {
//            List<QueryFilter> filters = QueryFilter.decodeJson(filter);
        if (!filters.isEmpty())
        {
            whereClause.setLength(0);
            params.clear();
            //this segment prepares the clause with brackets surounding it. e.g.,  (xxx and|or yyy).
            //added 1=1 because there may not be real clause because of bad queryfilter object
            //this 1=1 will at least pass the consuming sql
            whereClause.append(" (");
            if (HibernateUtil.isOracle())
            {
                filters = HibernateUtil.massageFiltersForClobField(getModelName(), filters);
            }
            
            for (QueryFilter filter : filters)
            {
                if (filter.isValidForSQLOrHQL())
                {
                    // evaluate the start count
                    filter.setStartCount(evaluateStartCount(filter));

                    String filterWhereClause = filter.getWhereClause(getPrefixTableAlias());
                    
                    whereClause.append(filterWhereClause + " " + operator);
                    // add to the param Map.
                    List<QueryParameter> queryParams = filter.getParameters();
                    for (QueryParameter qp : queryParams)
                    {
                        params.put(qp.getName(), qp.getValue());
                    }
                }
            }
            //remove the last operator
            if (whereClause.length() > operator.length())
            {
                whereClause.setLength(whereClause.length() - operator.length());
            }
            //add the closing bracket
            whereClause.append(") ");
            
            String check = whereClause.toString().replaceAll("[( )]", "");
            if(check.length() == 0)
            {
                //there was no good filter
                whereClause.setLength(0);
            }
        }
        return whereClause.toString();
    }

    public int getPage()
    {
        return page;
    }

    public void setPage(int page)
    {
        this.page = page;
    }

    public boolean isUseSql()
    {
        return useSql;
    }

    public void setUseSql(boolean useSql)
    {
        this.useSql = useSql;
    }

    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query;
    }

    public String getPrefixTableAlias()
    {
        String local = this.prefixTableAlias;
        if (StringUtils.isBlank(local))
        {
            local = "";
        }

        return local;
    }

    public void setPrefixTableAlias(String prefixTableAlias)
    {
        this.prefixTableAlias = prefixTableAlias;
    }

    public Map<String, Object> getParams()
    {
        return params;
    }

    public void setParams(Map<String, Object> params)
    {
        this.params = params;
    }

    public String getOperator()
    {
        return operator;
    }

    public void setOperator(String operator)
    {
        this.operator = operator;
    }
    
    public String getCustomOperator()
    {
        return customOperator;
    }
    public void setCustomOperator(String customOperator)
    {
        this.customOperator = customOperator;
    }

    private int evaluateStartCount(QueryFilter filter)
    {
        int count = 0;
        Set<String> attributes = params.keySet();
        if(attributes.size() > 0)
        {
            for(String att : attributes)
            {
                if(att.startsWith(filter.getField()))
                {
                    count++;
                }
            }
        }
        
        return count;
    }
    
    @Override
    public String toString()
    {
        return "QueryDTO [operator=" + operator + ", sqlQuery=" + sqlQuery + ", hql=" + hql + 
               ", searchTerm=" + searchTerm + ", interval=" + interval + ", selectColumns=" + selectColumns +
               ", tableName=" + tableName + ", modelName=" + modelName + ", whereClause=" + whereClause +
               ", orderBy=" + orderBy + ", type=" + type + ", page=" + page + ", start=" + start + 
               ", limit=" + limit + ", query=" + query + ", urlParams=" + urlParams + 
               ", useSql=" + useSql + ", prefixTableAlias=" + prefixTableAlias + 
               ", filters=" + filters + ", sorts=" + sorts + "]";
    }
    
    public String getParameterizedSelectSQL()
    {
        // parse it first
        parseQuery(false);

        // prepare the qry
        StringBuffer executeSql = new StringBuffer("select ");

        if (StringUtils.isNotBlank(getSelectColumns()))
        {
            executeSql.append(getSelectColumns());
        }
        else
        {
            executeSql.append("*");
        }

        executeSql.append(" from ");

        if (isUseSql())
        {
            executeSql.append(getTableName());
        }
        else
        {
            executeSql.append(getModelName());
        }

        // where clause
        if (StringUtils.isNotBlank(getWhereClause()))
        {
            executeSql.append(" where ").append(getWhereClause());
        }

        // order by
        if (StringUtils.isNotBlank(getSort()))
        {
            executeSql.append(" order by ").append(getSort());
        }
        else if (StringUtils.isNotBlank(getOrderBy()))
        {
            executeSql.append(" order by ").append(getOrderBy());
        }
        
        return executeSql.toString();
    }
    
    public List<QuerySort> getParsedSorts()
    {
        return parsedSorts;
    }
    
    public boolean getHqlDistinct()
    {
        return hqlDistinct;
    }
    
    public void setHqlDistinct(boolean hqlDistinct)
    {
        this.hqlDistinct = hqlDistinct;
    }
    
    public class SQLParameterizedQuery
    {
        private String tableName;
        private ArrayList<String> selectColumns; 
        private String parameterizedWhereClause;
        private ArrayList<Object> parameterValues;
        
        public SQLParameterizedQuery(String tableName, ArrayList<String> selectColumns, String parameterizedWhereClause, ArrayList<Object> parameterVals)
        {
            this.tableName = tableName;
            this.selectColumns = selectColumns;
            this.parameterizedWhereClause = parameterizedWhereClause;
            this.parameterValues = parameterValues;
        }
        
        public String getTableName()
        {
            return parameterizedWhereClause;
        }
        
        public ArrayList<String> getSelectColumns()
        {
            return selectColumns;
        }
        
        public String getParameterizedWhereClause()
        {
            return parameterizedWhereClause;
        }
        
        public ArrayList<Object> getParameterValues()
        {
            return parameterValues;
        }
    }
    
    public String getSelectSysIdCountHQL()
    {
        String selectHql = getSelectHQL();
        int startIndxOfTableOrModel = selectHql.indexOf("from");
        
        if (startIndxOfTableOrModel < 0)
        {
            Log.log.error("Could not find \"from \" in generated select HQL statement [" + selectHql + "]");
            throw new RuntimeException ("Error in getting total count of recrods.");
        }
        
        startIndxOfTableOrModel += "from".length();
        
        int offset = StringUtils.stripEnd(selectHql.substring(startIndxOfTableOrModel), " ").length() - selectHql.substring(startIndxOfTableOrModel).trim().length();
        
        if (offset > 0)
        {
            startIndxOfTableOrModel += offset;
        }
        
        int endIndxOfTableOrModel = selectHql.indexOf(" ", startIndxOfTableOrModel);
        
        if (endIndxOfTableOrModel > startIndxOfTableOrModel)
        {
            if (selectHql.contains(" order by "))
            {
                return "select COUNT(sys_id) as COUNT from " + selectHql.substring(startIndxOfTableOrModel, endIndxOfTableOrModel) + StringUtils.stripStart(selectHql.substring(endIndxOfTableOrModel, selectHql.indexOf(" order by ") ), "tableData");
            }
            else
            {
                return "select COUNT(sys_id) as COUNT from " + selectHql.substring(startIndxOfTableOrModel, endIndxOfTableOrModel) + StringUtils.stripStart(selectHql.substring(endIndxOfTableOrModel), "tableData");
            }
        }
        else
        {
            if (selectHql.contains(" order by "))
            {
                return "select COUNT(sys_id) as COUNT from " + selectHql.substring(startIndxOfTableOrModel, selectHql.indexOf(" order by "));
            }
            else
            {
                return "select COUNT(sys_id) as COUNT from " + selectHql.substring(startIndxOfTableOrModel);
            }
        }
    }
    
    public String getSelectDistinctSysIdCountHQL()
    {
        String selectHql = getSelectHQL();
        int startIndxOfTableOrModel = selectHql.indexOf("from");
        
        if (startIndxOfTableOrModel < 0)
        {
            Log.log.error("Could not find \"from \" in generated select HQL statement [" + selectHql + "]");
            throw new RuntimeException ("Error in getting total count of recrods.");
        }
        
        startIndxOfTableOrModel += "from".length();
        
        int offset = StringUtils.stripEnd(selectHql.substring(startIndxOfTableOrModel), " ").length() - selectHql.substring(startIndxOfTableOrModel).trim().length();
        
        if (offset > 0)
        {
            startIndxOfTableOrModel += offset;
        }
        
        int endIndxOfTableOrModel = selectHql.indexOf(" ", startIndxOfTableOrModel);
        
        if (endIndxOfTableOrModel > startIndxOfTableOrModel)
        {
            if (selectHql.contains(" order by "))
            {
                return "select COUNT(distinct sys_id) as COUNT from " + selectHql.substring(startIndxOfTableOrModel, endIndxOfTableOrModel) + StringUtils.stripStart(selectHql.substring(endIndxOfTableOrModel, selectHql.indexOf(" order by ") ), "tableData");
            }
            else
            {
                return "select COUNT(distinct sys_id) as COUNT from " + selectHql.substring(startIndxOfTableOrModel, endIndxOfTableOrModel) + StringUtils.stripStart(selectHql.substring(endIndxOfTableOrModel), "tableData");
            }
        }
        else
        {
            if (selectHql.contains(" order by "))
            {
                return "select COUNT(distinct sys_id) as COUNT from " + selectHql.substring(startIndxOfTableOrModel, selectHql.indexOf(" order by "));
            }
            else
            {
                return "select COUNT(distinct sys_id) as COUNT from " + selectHql.substring(startIndxOfTableOrModel);
            }
        }
    }
    
    public boolean getHqlDistinctMap() {
        return hqlDistinctMap;
    }
    
    public void setHqlDistinctMap(boolean hqlDistinctMap)
    {
        this.hqlDistinctMap = hqlDistinctMap;
    }
}