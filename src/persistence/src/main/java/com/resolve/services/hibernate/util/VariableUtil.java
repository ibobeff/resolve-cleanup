/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.List;
import java.util.Map;

import com.resolve.persistence.dao.ResolvePropertiesDAO;
import com.resolve.persistence.model.ResolveProperties;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class VariableUtil
{
    public static String getProperty(String name)
    {
        return getProperty(name, null);
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static String getProperty(String name, Map resultAttr)
    {
    	try
        {    		
            return (String) HibernateProxy.execute(() -> {
            	String result = null;
            	ResolvePropertiesDAO rpDao = HibernateUtil.getDAOFactory().getResolvePropertiesDAO();
                ResolveProperties exp = new ResolveProperties();
                exp.setUName(name);
                List<ResolveProperties> lrp = rpDao.find(exp);
                if (lrp.size() > 0)
                {
                    ResolveProperties rp = lrp.get(0);
                    result = rp.getUValue();
                    
                    // set result attributes
                    if (resultAttr != null)
                    {
                        boolean isEncrypted = CryptUtils.isEncrypted(result);
                        resultAttr.put(Constants.RESULTATTR_ENCRYPTED, isEncrypted);
                    }
                    
                    return CryptUtils.decrypt(result);
                }
                
                return null;
            });

            
        }
        catch (Exception e)
        {
            Log.log.error("Failed to retrieve property: " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
            return null;
        }
    } // getProperty
    
    public static boolean isPropertyEncrypted(String name)
    {
        boolean result = false;
        try
        {
        	result = (boolean) HibernateProxy.execute(() -> {
            	ResolvePropertiesDAO rpDao = HibernateUtil.getDAOFactory().getResolvePropertiesDAO();
                ResolveProperties exp = new ResolveProperties();
                exp.setUName(name);
                List<ResolveProperties> lrp = rpDao.find(exp);
                if (lrp.size() > 0)
                {
                    ResolveProperties rp = lrp.get(0);
                    String value = rp.getUValue();
                    
                    return CryptUtils.isEncrypted(value);
                }
                
                 return false;
            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to retrieve property: " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        } 
        
        return result;
    }

    public static void setProperty(String name, String value, String module, boolean encrypt)
    {
        try
        {            
            HibernateProxy.execute(() -> {
            	
            	 ResolvePropertiesDAO rpDao = HibernateUtil.getDAOFactory().getResolvePropertiesDAO();
                 ResolveProperties exp = new ResolveProperties();
                 exp.setUName(name);

                 List<ResolveProperties> lrp = rpDao.find(exp, 1);
                 if (lrp.size() > 0)
                 {
                     exp = lrp.get(0);
                 }

                 if (encrypt)
                 {
                     exp.setUType("Encrypt");
                     exp.setUValue(CryptUtils.encrypt(value));
                 }
                 else
                 {
                     exp.setUType("Plain");
                     exp.setUValue(StringUtils.escapeHtml(value));
                 }
                 exp.setUModule(module);
                 
                 //save it
                 saveResolveProperties(exp, null);           	
            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist property: " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    } // setProperty
    
    public static void removeProperty(String name)
    {
        try
        {
            HibernateProxy.execute(() -> {
            	ResolvePropertiesDAO rpDao = HibernateUtil.getDAOFactory().getResolvePropertiesDAO();
                ResolveProperties exp = new ResolveProperties();
                exp.setUName(name);

                List<ResolveProperties> lrp = rpDao.find(exp, 1);
                if (lrp.size() > 0)
                {
                    exp = lrp.get(0);
                    rpDao.delete(exp);
                }
            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to remove property: " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    } // setProperty
    
    private static ResolveProperties saveResolveProperties(ResolveProperties model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getResolvePropertiesDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getResolvePropertiesDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist property: " + e.getMessage(), e);            
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
}
