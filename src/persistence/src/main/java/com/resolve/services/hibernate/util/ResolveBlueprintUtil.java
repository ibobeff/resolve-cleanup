/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.resolve.persistence.dao.OrderbyProperty;
import com.resolve.persistence.model.ResolveBlueprint;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ResolveBlueprintVO;
import com.resolve.util.Log;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;

public class ResolveBlueprintUtil
{
    @SuppressWarnings("unchecked")
	public static List<ResolveBlueprintVO> findAllResolveBlueprint()
    {
        List<ResolveBlueprintVO> result = new ArrayList<ResolveBlueprintVO>();
        List<ResolveBlueprint> blueprints = null;

        try
        {
        	blueprints = (List<ResolveBlueprint>) HibernateProxy.executeNoCache(() -> {
        		 OrderbyProperty orderBy = new OrderbyProperty("sysCreatedOn", false);
                 
                 return  HibernateUtil.getDAOFactory().getResolveBlueprintDAO().findAll(orderBy);
        	});

           
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        if (blueprints != null)
        {
            for (ResolveBlueprint bp : blueprints)
            {
                result.add(bp.doGetVO());
            }
        }

        return result;
    }

    public static List<String> getUserAuthenticationGateway(String gatewayType)
    {
        List<String> result = new ArrayList<String>();

        Set<String> gateways = new TreeSet<String>();

        List<ResolveBlueprintVO> blueprints = findAllResolveBlueprint();
        try
        {
            for (ResolveBlueprintVO blueprint : blueprints)
            {
                String blueprintContent = blueprint.getUBlueprint();
                InputStream inStream = new ByteArrayInputStream(blueprintContent.getBytes());
                Properties properties = new Properties();
                properties.load(inStream);
                
                List<String> gtwPropPrefixes = new ArrayList<String>();
                
                for (Object propKeyObj :  properties.keySet())
                {
                    String propKey = (String)propKeyObj;
                    
                    if (propKey.endsWith(".receive." + gatewayType + ".active"))
                    {
                        gtwPropPrefixes.add(StringUtils.substringBeforeLast(propKey, ".active"));
                    }
                }
                
                for (String propertyNamePrefix : gtwPropPrefixes)
                {
                    if ("true".equalsIgnoreCase(properties.get(propertyNamePrefix + ".active")) && "true".equalsIgnoreCase(properties.get(propertyNamePrefix + ".userauth")))
                    {
                        gateways.add(properties.get(propertyNamePrefix + ".queue"));
                    }
                }
            }
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
        }

        result.addAll(gateways);
        
        return result;
    } // getUserAuthenticationGateway
}
