package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import com.resolve.persistence.model.ResolveCaseConfig;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ResolveCaseConfigVO;
import com.resolve.util.Log;

public class CaseConfigUtil
{
	public static final String CASE_CONFIG_IOC_TYPE_ALL_IOCS = "RESOLVE.ALLIOCS";
	public static final String CASE_CONFIG_IOC_TYPE_IOC = "IOC";
	
    @SuppressWarnings("unchecked")
	public static List<ResolveCaseConfigVO> listCaseConfig(String configType, Optional<String> sysOrg, boolean forCase, String username) throws Exception
    {
        if (StringUtils.isBlank(configType))
        {
            throw new Exception("Configuration type, configType, is missing.");
        }
        
        List<ResolveCaseConfigVO> result = new ArrayList<>();
        List<ResolveCaseConfig> caseConfigList = null;
        
        try
        {
          HibernateProxy.setCurrentUser(username);
        	caseConfigList = (List<ResolveCaseConfig>) HibernateProxy.execute(() -> {
	            
	            CriteriaBuilder builder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
	            
	            CriteriaQuery<ResolveCaseConfig> query = builder.createQuery(ResolveCaseConfig.class);
	            Root<ResolveCaseConfig> caseConfig = query.from(ResolveCaseConfig.class);
	            
	            List<Predicate> predicates = new ArrayList<>();
	            predicates.add(builder.equal(builder.lower(caseConfig.get("configType")), configType.toLowerCase()));
	            if (sysOrg.isPresent() && StringUtils.isNotBlank(sysOrg.get()) && 
	            	!configType.equalsIgnoreCase(CASE_CONFIG_IOC_TYPE_IOC))
	            {
	                predicates.add(builder.equal(caseConfig.get("sysOrg"), sysOrg.get()));
	            }
	            if (forCase)
	            {
	                predicates.add(builder.equal(caseConfig.get("active"), forCase));
	            }
	            
	            query.where(predicates.stream().toArray(Predicate[]::new));
	            
	            return HibernateUtil.getCurrentSession().createQuery(query).list();
	            
        	});
        }
        catch(Exception e)
        {
            Log.log.error("Error while retrieving case config for config type: " + configType, e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        if (caseConfigList != null && caseConfigList.size() > 0)
        {
            result = caseConfigList.stream().map(c -> c.doGetVO()).collect(Collectors.<ResolveCaseConfigVO> toList());
        }
        
        return result;
    }
    
	public static List<ResolveCaseConfigVO> getIOCByConfig(String ioc, String org) {
		List<ResolveCaseConfigVO> configVOs = new ArrayList<>();
		try {

			HibernateProxy.execute(() -> {
				CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
				CriteriaQuery<ResolveCaseConfig> criteriaQuery = criteriaBuilder.createQuery(ResolveCaseConfig.class);
				Root<ResolveCaseConfig> from = criteriaQuery.from(ResolveCaseConfig.class);

	            List<Predicate> predicates = new ArrayList<>();

				// Should Case Config be Org based? (HP)
				predicates.add(StringUtils.isNotBlank(org) ? criteriaBuilder.equal(from.get("sysOrg"), org) :
															 criteriaBuilder.isNull(from.get("sysOrg")));
				predicates.add(criteriaBuilder.equal(from.get("configType"), "IOC"));
				
				// no iocType filtering if all
				if (StringUtils.isBlank(ioc) || (!CASE_CONFIG_IOC_TYPE_ALL_IOCS.equals(ioc))) {
					predicates.add(StringUtils.isNotBlank(ioc) ? criteriaBuilder.equal(from.get("iocType"), ioc) :
							   								     criteriaBuilder.isNull(from.get("iocType")));
				}

				criteriaQuery.select(from).where(predicates.stream().toArray(Predicate[]::new));

				List<ResolveCaseConfig> result = HibernateUtil.getCurrentSession().createQuery(criteriaQuery)
						.getResultList();

				for (ResolveCaseConfig config : result) {
					configVOs.add(config.doGetVO());
				}

				
			});
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}
		return configVOs;
	}

    public static List<ResolveCaseConfigVO> save(List<ResolveCaseConfigVO> configList, Optional<String> configType, Optional<String> sysOrg, String username) throws Exception
    {
        if (!GenericUserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_SETTINGS_CHANGE, ""))
        {
            throw new Exception("WARNING: User does not have privileges to perform this operation");
        }
        
        List<ResolveCaseConfigVO> result = new ArrayList<>();
        
        if (configList == null || configList.isEmpty())
        {
            throw new Exception ("WARNING: Case Configuration list cannot be null or empty");
        }
        
        for (ResolveCaseConfigVO configVO : configList)
        {
            if (configVO != null)
            {
                validateCaseConfig(configVO);
                ResolveCaseConfig localConfig = null;
                if (StringUtils.isNotBlank(configVO.getSys_id()) && !configVO.getSys_id().equals("UNDEFINED"))
                {
                    localConfig = getCaseConfig(configVO.getSys_id(), username);
                }
                
                ResolveCaseConfig config = new ResolveCaseConfig();
                config.applyVOToModel(configVO);
                
                // if config present, grab it's created by and created on fields.
                if (localConfig != null)
                {
                    config.setSysCreatedBy(localConfig.getSysCreatedBy());
                    config.setSysCreatedOn(localConfig.getSysCreatedOn());
                }
                
                if (StringUtils.isNotBlank(config.getConfigType()) && 
                	config.getConfigType().equalsIgnoreCase(CASE_CONFIG_IOC_TYPE_IOC)) {
                	config.setSysOrg(null); // IOCs are global not Org specific
                }
                
                insertOrUpdate(config, username);
            }
            else
            {
                Log.log.error("Case Configuration cannot be null.");
            }
        }
        
        if (configType.isPresent() && StringUtils.isNotBlank(configType.get()))
        {
            result = listCaseConfig(configType.get(), sysOrg, false, username);
        }
        
        return result;
    }
    
    private static void insertOrUpdate(ResolveCaseConfig caseConfig, String username)
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
        		HibernateUtil.getDAOFactory().getResolveCaseConfigDAO().insertOrUpdate(caseConfig);
        	});
            
        }
        catch(Exception e)
        {
            Log.log.error("Error while adding new case configuration", e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    }
    
    private static ResolveCaseConfig getCaseConfig(String configId, String username)
    {
        ResolveCaseConfig caseConfig = null;
        try
        {
          HibernateProxy.setCurrentUser(username);
            caseConfig = (ResolveCaseConfig) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getResolveCaseConfigDAO().findById(configId);
            });
            
        }
        catch(Exception e)
        {
            Log.log.error("Error while getting case configuration id: " + configId, e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return caseConfig;
    }
    
    private static void validateCaseConfig(ResolveCaseConfigVO configVO) throws Exception
    {
        String configType = configVO.getConfigType();
        if (StringUtils.isBlank(configType))
        {
            throw new Exception("Configuration Type, configType, cannot be blank.");
        }
        
        switch (configType.toLowerCase().trim())
        {
            case "casetype" :
            case "casestatus":
            case "casespriority":
            case "caseclassification":
            {
                if (StringUtils.isBlank(configVO.getConfigValue()))
                {
                    throw new Exception("For configuration Type, " + configType + ", configuration value cannot be blank.");
                }
                break;
            }
            case "caseattribute":
            {
                if (StringUtils.isBlank(configVO.getConfigValue()) || StringUtils.isBlank(configVO.getAttribValue()))
                {
                    throw new Exception("For configuration Type, " + configType + ", configuration value, configValue (which holds the name of the attribute) and attributeValue (which holds value of the attribute) cannot be blank.");
                }
                break;
            }
            case "ioc" :
            {
                if (StringUtils.isBlank(configVO.getIocKey()) || StringUtils.isBlank(configVO.getIocDisplayName()))
                {
                    throw new Exception("For configuration Type, " + configType + ", configuration value, iocKey and iocDisplayName cannot be blank.");
                }
                break;
            }
            default :
            {
                throw new Exception("Unknown configuration Type, " + configType);
            }
        }
    }
}
