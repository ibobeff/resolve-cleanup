/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.hibernate.query.Query;

import com.resolve.persistence.model.GroupRoleRel;
import com.resolve.persistence.model.Groups;
import com.resolve.persistence.model.Orgs;
import com.resolve.persistence.model.Roles;
import com.resolve.persistence.model.UserGroupRel;
import com.resolve.persistence.model.UserPreferences;
import com.resolve.persistence.model.UserRoleRel;
import com.resolve.persistence.model.Users;
import com.resolve.persistence.util.DBCacheRegionConstants;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.GroupsVO;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.RolesVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class GenericUserUtils {
	private final static String SYSTEM = "system";
	private final static String ADMIN = "admin";
	private final static String RESOLVE_MAINT = "resolve.maint";
	private static final String HASH_FUNCTION = "SHA-512";

	private static final String PRESUMED_LEGACY_RBAC_HAS_NO_NEW_RBAC_MAPPING = "Presumed legacy RBAC %s%s has no mapping"
			+ " in new RBAC!!!";

	private static final String RETURNING_FUNCTION_PERMISSIONS_FROM_LEGACY_LOG = "Returning %s permissions for %s%s"
			+ " from legacy permissions!!!";

	public static final String NOT_IMPLEMENTED_BY_UI_FUNCTION_PERMISSIONS_JSON = "[]";

	private static final String FUNCTION_PERMISSIONS_NOT_IMPLEMENTED_NY_UI_ADDED_FOR_USER_LOG = 
			"Function %s (not implemneted by UI) Permissions %s added for %s.";
	
	public static final String NOT_IMPLEMENTED_BY_UI_RBACS_JSON = "{}";
	
	private static final String RBAC_PERMISSIONS_NOT_IMPLEMENTED_NY_UI_ADDED_FOR_USER_LOG = 
			"RBAC %s (not implemneted by UI) Permission %s added for %s.";
	
	private static final String DB_LEGACY_OR_NEW_RBAC_JSON_LOG = "DB (Legacy/New) RBAC JSON : %s";
	
	private static final String ARBAC_NOT_IMPLEMENTED_BY_UI_ADDED_FOR_ROLE = "A-RBAC %s, " + RBACUtils.RBAC_SIR_ENABLED +
			 " (not implemneted by UI) Permission %s" +
			 " added for role.";
	
	private static final String TRANSFORMED_IF_DB_LEGACY_RBAC_JSON = "Transformed (if DB permissions "
			+ "were Legacy) RBAC JSON : %s";

	public static boolean isAdminUser(String username) {
		boolean isAdmin = false;

		try {
			isAdmin = isSuperUser(username);

			if (!isAdmin) {
				Set<String> userRoles = getUserRoles(username);
				if (userRoles.contains(ADMIN)) {
					isAdmin = true;
				}
			}
		} catch (Throwable t) {
			Log.log.error("error validating if the user is admin or not.", t);
		}

		return isAdmin;
	}

	public static boolean isSuperUser(String username) {
		if (StringUtils.isBlank(username)) {
			return false;
		}

		if (username.equalsIgnoreCase(SYSTEM) || username.equalsIgnoreCase(RESOLVE_MAINT)) {
			return true;
		}

		return hasSuperUserRole(username);
	}

	/*
	 * For time being treat users with admin role as super user. Once a separate
	 * super user role is created replace the below method to check whether user has
	 * super user role.
	 */
	private static boolean hasSuperUserRole(String username) {
		Set<String> userRoles = getUserRoles(username);

		if (userRoles.contains(ADMIN)) {
			return true;
		}

		return false;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Set<String> getUserRoles(String userName) {
		Set<String> userRoles = new TreeSet<>();

		Object cachedRoles = HibernateUtil.getCachedDBObject(DBCacheRegionConstants.USER_ROLES_CACHE_REGION, userName);
		if (cachedRoles != null) {
			return (Set<String>) cachedRoles;
		}

		if (StringUtils.isBlank(userName)) {
			return Collections.<String>emptySet();
		}

		if (userName.equalsIgnoreCase(RESOLVE_MAINT)) {
			userRoles.add(ADMIN);
			return userRoles;
		}

		userName = userName.toLowerCase();
		try {
			String usernameFinal = userName;
			HibernateProxy.execute(() -> {
				// UNION ALL is not supported by HIBERNATE. So have to prepare 2
				// qrys and assemble it.
				String sql = "select r.UName from Users as u" + " join u.userRoleRels as urr " + " join urr.role as r "
						+ " where u.UUserName = :username";
				Query q = HibernateUtil.createQuery(sql);
				q.setParameter("username", usernameFinal);
				List<String> roles = q.list();
				userRoles.addAll(roles);

				// query 2
				sql = "select r.UName from Users as u" + " join u.userGroupRels as ugr " + " join ugr.group as g "
						+ " join g.groupRoleRels as grr " + " join grr.role as r " + " where u.UUserName = :username";
				q = HibernateUtil.createQuery(sql);
				q.setParameter("username", usernameFinal);

				roles = q.list();
				userRoles.addAll(roles);
			});			

			
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}

		// trim the string in the list
		StringUtils.trimStringCollection(userRoles);
		HibernateUtil.cacheDBObject(DBCacheRegionConstants.USER_ROLES_CACHE_REGION,
				new String[] { "com.resolve.persistence.model.Users", "com.resolve.persistence.model.UserRoleRel",
						"com.resolve.persistence.model.Roles", "com.resolve.persistence.model.UserGroupRel",
						"com.resolve.persistence.model.Groups", "com.resolve.persistence.model.GroupRoleRel" },
				userName, userRoles);

		return userRoles;
	}

	public static UsersVO getUser(String username) {
		UsersVO result = null;

		if (StringUtils.isNotEmpty(username)) {
			Users userResult = getUserPrivate(null, username);

			if (userResult != null) {
				result = convertModelUsersToVO(userResult, null, null, null, true);
			}
		}

		return result;
	}

	private static Users getUserPrivate(String sysId, String username) {
		Users userResult = null;
		if (StringUtils.isNotBlank(username)) {
			boolean isResolveMaint = false;
			if (username.equalsIgnoreCase(RESOLVE_MAINT)) {
				isResolveMaint = true;
			}

			Object obj = HibernateUtil.getCachedDBObject(DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION, username);

			if (obj != null) {
				userResult = (Users) obj;
				Log.log.debug("Found cached DB object for user " + username + " in "
						+ DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION);
			}

			if (userResult == null) {
				try {
					if (isResolveMaint) {
						// create a object for RESOLVE_MAINT
						userResult = new Users();
						userResult.setUUserName(RESOLVE_MAINT);
					} else {
						userResult = findUserWithAllReferencesNoCache(sysId, username);
					}

					if (userResult != null) {
						HibernateUtil.cacheDBObject(DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION, new String[] {
								"com.resolve.persistence.model.Users", "com.resolve.persistence.model.UserRoleRel",
								"com.resolve.persistence.model.Roles", "com.resolve.persistence.model.UserGroupRel",
								"com.resolve.persistence.model.Groups", "com.resolve.persistence.model.GroupRoleRel",
								"com.resolve.persistence.model.UserPreferences",
								"com.resolve.persistence.model.Organization", "com.resolve.persistence.model.Orgs",
								"com.resolve.persistence.model.OrgGroupRel" }, userResult.getUUserName(), userResult);
						Log.log.debug("Inserted cached DB object for user " + username + " in "
								+ DBCacheRegionConstants.USER_BY_NAME_CACHE_REGION);
					}
				} catch (Throwable e) {
					Log.log.warn(e.getMessage(), e);
				}
			}
		} else if (StringUtils.isNotBlank(sysId)) {
			userResult = findUser(sysId, null);
		}
		return userResult;
	}

	/**
	 * This api is
	 * 
	 * @param sysId
	 * @param username
	 * @return
	 */
	public static Users findUserWithAllReferencesNoCache(String sysId, String username) {
		Users userResult = null;

		try {
			userResult = (Users) HibernateProxy.executeNoCache(() -> {
				return findUserWithAllReferences(sysId, username);
			});
		} catch (Throwable e) {
			Log.log.warn(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		return userResult;
	}

	private static Users findUserWithAllReferences(String sysId, String username) {
		Users userResult = null;

		try {
			userResult = (Users) HibernateProxy.execute(() -> {

				Users searchedUser = null;
				if (StringUtils.isNotEmpty(sysId)) {
					searchedUser = HibernateUtil.getDAOFactory().getUsersDAO().findById(sysId);
				} else if (StringUtils.isNotBlank(username)) {
					int pos = username.indexOf('\\');
					String normalizedUserName = username;
					if (pos > 0) {
						normalizedUserName = username.substring(0, pos) + "\\"
								+ username.substring(pos + 1, username.length());
					}
	
					// to make this case-insensitive
					// String sql = "from Users where UUserName = '" +
					// normalizedUserName.trim().toLowerCase() + "'";
					String sql = "from Users where UUserName = :UUserName";
	
					Map<String, Object> queryParams = new HashMap<String, Object>();
	
					queryParams.put("UUserName", normalizedUserName.trim().toLowerCase());
	
					try {
						List<? extends Object> result = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
						if (result != null && result.size() > 0) {
							searchedUser = (Users) result.get(0);
						}
					} catch (Exception e) {
						Log.log.error("Error while executing hql:" + sql, e);
					}
				}
	
				if (searchedUser != null) {
					// set the roles
					setUserRoles(searchedUser);
	
					Collection<UserRoleRel> userRoleRels = searchedUser.getUserRoleRels();
					if (userRoleRels != null) {
						userRoleRels.size(); // load it
					}
	
					Collection<UserGroupRel> userGroupRels = searchedUser.getUserGroupRels();
					if (userGroupRels != null) {
						userGroupRels.size(); // load it
					}
	
					Collection<UserPreferences> socialPreferences = searchedUser.getSocialPreferences();
					if (socialPreferences != null) {
						socialPreferences.size(); // load it
					}
				}
				return searchedUser;
			});
		} catch (Throwable e) {
			Log.log.warn(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		return userResult;
	}

	private static List<RolesVO> setUserRoles(Users user) {
		if (user == null) {
			return Collections.<RolesVO>emptyList();
		}

		List<RolesVO> roleVOList = new ArrayList<>();
		Set<String> roles = new HashSet<String>();

		if (user.getUUserName().equalsIgnoreCase(ADMIN) || user.getUUserName().equalsIgnoreCase(RESOLVE_MAINT)) {
			roles.add("admin");
		}

		Collection<UserRoleRel> userRoleRels = user.getUserRoleRels();
		if (CollectionUtils.isNotEmpty(userRoleRels)) {
			userRoleRels.stream().map(UserRoleRel::getRole).filter(Objects::nonNull).map(Roles::doGetVO)
					.forEach(roleVO -> {
						if (!roleVOList.contains(roleVO)) {
							roleVOList.add(roleVO);
						}
						roles.add(roleVO.getUName().replaceAll("&", "_"));
					});
		}

		Collection<UserGroupRel> userGroupRels = user.getUserGroupRels();
		if (CollectionUtils.isNotEmpty(userGroupRels)) {
			userGroupRels.stream().map(UserGroupRel::getGroup).filter(Objects::nonNull).map(Groups::getGroupRoleRels)
					.filter(Objects::nonNull).flatMap(Collection::stream).map(GroupRoleRel::getRole)
					.filter(Objects::nonNull).map(Roles::doGetVO).forEach(roleVO -> {
						if (!roleVOList.contains(roleVO)) {
							roleVOList.add(roleVO);
						}
						roles.add(roleVO.getUName().replaceAll("&", "_"));
					});

		}

		user.setRoles(StringUtils.setToString(roles, ","));

		return roleVOList;
	}

	private static UsersVO convertModelUsersToVO(Users model, Map<String, String> mapOfOrganization,
			Map<String, String> mapOfUsersAndRoles, Map<String, String> mapOfUsersAndGroups, boolean relTablesAlso) {
		if (model == null) {
			return null;
		}

		if (mapOfOrganization == null) {
			mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
		}

		UsersVO vo = model.doGetVO();
		if (StringUtils.isNotBlank(vo.getSysOrg())) {
			vo.setSysOrganizationName(mapOfOrganization.get(vo.getSysOrg()));
		} else {
			vo.setSysOrganizationName("");
		}

		// for one user rec
		if (relTablesAlso) {
			// for edit of a User rec, use the list of roles and groups
			vo.setRoles(null);
			vo.setGroups(null);

			Set<String> roleNames = new HashSet<String>();
			Set<String> groupNames = new HashSet<String>();

			// groups
			List<GroupsVO> userGroups = new ArrayList<GroupsVO>();
			Collection<UserGroupRel> userGroupRel = model.getUserGroupRels();
			if (userGroupRel != null) {
				List<Groups> groups = userGroupRel.stream().map(UserGroupRel::getGroup).filter(Objects::nonNull)
						.collect(Collectors.toList());
				groups.forEach(group -> {
					userGroups.add(group.doGetVO());
					groupNames.add(group.getUName());
				});

				groups.stream().map(Groups::getGroupRoleRels).filter(Objects::nonNull).flatMap(Collection::stream)
						.map(GroupRoleRel::getRole).filter(Objects::nonNull)
						.filter(role -> StringUtils.isNotBlank(role.getUName()))
						.forEach(role -> roleNames.add(role.getUName().replaceAll("&", "_")));

				vo.setUserGroups(userGroups);

				if (!groupNames.isEmpty()) {
					StringBuilder groupsCSStrBldr = new StringBuilder();

					for (String grpName : groupNames) {
						if (groupsCSStrBldr.length() > 0) {
							groupsCSStrBldr.append(",");
						}

						groupsCSStrBldr.append(grpName);
					}

					vo.setGroups(groupsCSStrBldr.toString());
				}
			}

			// roles
			List<RolesVO> userRoles = new ArrayList<RolesVO>();
			Collection<UserRoleRel> userRoleRels = model.getUserRoleRels();
			if (userRoleRels != null) {
				for (UserRoleRel rel : userRoleRels) {
					Roles role = rel.getRole();
					if (role != null) {
						userRoles.add(role.doGetVO());
						roleNames.add(role.getUName().replaceAll("&", "_"));
					}
				}

				vo.setUserRoles(userRoles);
			}

			if (!roleNames.isEmpty()) {
				StringBuilder rolesCSStrBldr = new StringBuilder();

				for (String roleName : roleNames) {
					if (rolesCSStrBldr.length() > 0) {
						rolesCSStrBldr.append(",");
					}

					rolesCSStrBldr.append(roleName);
				}

				vo.setRoles(rolesCSStrBldr.toString());
			}
		} else {
			// set the comma seperated Roles
			if (mapOfUsersAndRoles != null) {
				vo.setRoles(mapOfUsersAndRoles.get(vo.getSys_id()));
			}

			// set the comma seperated Groups
			if (mapOfUsersAndGroups != null) {
				vo.setGroups(mapOfUsersAndGroups.get(vo.getSys_id()));
			}

		}

		// set the dummy pwd for all the users
		// Uploading profile pic locks out user (http://10.20.2.111/browse/RBA-13209)
		// vo.setUUserP_assword(DUMMY_USER_P_ASSWORD);
		vo.setUUserP_assword(VO.STRING_DEFAULT);
		vo.setUPasswordHistory(null);

		return vo;
	}

	public static Users findUser(String sysId, String username) {
		Users userResult = null;

		try {
			userResult = (Users) HibernateProxy.execute(() -> {

				if (StringUtils.isNotEmpty(sysId)) {
					return HibernateUtil.getDAOFactory().getUsersDAO().findById(sysId);
				} else if (StringUtils.isNotBlank(username)) {
					// to make this case-insensitive
					int pos = username.indexOf('\\');
					String normalizedUserName = username;
					if (pos > 0) {
						normalizedUserName = username.substring(0, pos) + "\\"
								+ username.substring(pos + 1, username.length());
					}
	
					// String sql = "from Users where UUserName = '" +
					// normalizedUserName.trim().toLowerCase() + "'";
					String sql = "from Users where UUserName = :UUserName";
	
					Map<String, Object> queryParams = new HashMap<String, Object>();
	
					queryParams.put("UUserName", normalizedUserName.trim().toLowerCase());
	
					try {
						List<? extends Object> result = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
						if (result != null && result.size() > 0) {
							return  (Users) result.get(0);
						}
					} catch (Exception e) {
						Log.log.error("Error while executing sql:" + sql, e);
					}
				} 
				
				return null;

			});
		} catch (Throwable e) {
			Log.log.warn(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		return userResult;
	}

	public static String encrypt(String in) {
		try {
			if (StringUtils.isNotEmpty(in)) {
				return CryptUtils.encrypt(in);
			}
		} catch (Exception e) {
			Log.log.error("Unable to encrypt user data");
		}

		return StringUtils.EMPTY;
	}

	public static String encryptPassword(String x) {
		String answer = x;

		if (!StringUtils.isEmpty(x) && x.charAt(x.length() - 1) != '=') {
			try {
				MessageDigest d = MessageDigest.getInstance(HASH_FUNCTION);
				d.reset();
				d.update(x.getBytes());
				byte[] bytes = d.digest();

				answer = new String(Base64.encodeBase64(bytes)); // converted to apache encoding
			} catch (Exception e) {
				Log.log.warn(e);
			}
		}
		return answer;
	}

	public static GroupsVO getGroupNoCache(String sysId, String groupName, String username) {
		return getGroupNoCache(sysId, groupName, username, false);
	}

	public static GroupsVO getGroupNoCache(String sysId, String groupName, String username, boolean orgsOnly) {
		Groups groupModel = null;
		GroupsVO groupVO = null;

		try {
			groupModel = (Groups) HibernateProxy.executeNoCache(() -> {
				return getGroupModel(sysId, groupName, orgsOnly);
			});
		} catch (Throwable e) {
			Log.log.warn(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		if (groupModel != null) {
			groupVO = convertModelToVOForGroups(groupModel, null, null, orgsOnly);
		}

		return groupVO;
	}

	@SuppressWarnings("unchecked")
	private static Groups getGroupModel(String sysId, String groupName, boolean orgsOnly) {
		try {

			return (Groups) HibernateProxy.execute(() -> {
				Groups groupModel = null;
				if (StringUtils.isNotBlank(sysId)) {
					groupModel = HibernateUtil.getDAOFactory().getGroupsDAO().findById(sysId);
				} else if (StringUtils.isNotBlank(groupName)) {
					String sql = "from Groups where LOWER(UName) = '" + groupName.trim().toLowerCase() + "'";
					List<Groups> list = HibernateUtil.createQuery(sql).list();
					if (list != null && list.size() > 0) {
						groupModel = list.get(0);
					}
				}

				// load the roles
				if (groupModel != null) {
					if (!orgsOnly)
					{
						if (groupModel.getGroupRoleRels() != null)
							groupModel.getGroupRoleRels().size();
		
						if (groupModel.getUserGroupRels() != null)
							groupModel.getUserGroupRels().size();
					}

					if (groupModel.getOrgGroupRels() != null)
						groupModel.getOrgGroupRels().size();
				}
				
				return groupModel;

			});
		} catch (Throwable e) {
			Log.log.warn(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
			return null;
		}

		
	}
	
	@SuppressWarnings("unchecked")
	private static Groups getGroupModel(String sysId, String groupName) {
		return getGroupModel(sysId, groupName, false);
	}

	private static GroupsVO convertModelToVOForGroups(Groups model, Map<String, String> mapOfOrganization,
			Map<String, String> mapofGroupAndRoles, boolean orgsOnly) {
		GroupsVO vo = null;

		if (model != null) {
			if (mapOfOrganization == null) {
				mapOfOrganization = OrgsUtil.getListOfAllOrgNames();
			}

			vo = model.doGetVO(orgsOnly);
			if (StringUtils.isNotBlank(vo.getSysOrg())) {
				vo.setSysOrganizationName(mapOfOrganization.get(vo.getSysOrg()));
			} else {
				vo.setSysOrganizationName("");
			}

			// set the comma seperated Roles
			if (mapofGroupAndRoles != null) {
				vo.setRoles(mapofGroupAndRoles.get(vo.getSys_id()));
			}

		}

		return vo;
	}

	public static Set<OrgsVO> getAllOrgsInHierarchy(String orgId, String orgName) {
		Set<OrgsVO> allChildOrgs = new HashSet<OrgsVO>();

		OrgsVO rootOrgVO = getOrgNoCache(orgId, orgName);

		getAllOrgsInHierarchyInternal(rootOrgVO, allChildOrgs);

		if (rootOrgVO != null) {
			allChildOrgs.add(rootOrgVO);
		}

		return allChildOrgs;
	}

	public static OrgsVO getOrgNoCache(String sysId, String orgName) {
		OrgsVO orgVO = null;

		try {
			// this is used in conjuntion with the @Cache notation for the relationships
			orgVO = (OrgsVO) HibernateProxy.executeNoCache(() -> {
				return getOrg(sysId, orgName);
			});
		} catch (Throwable e) {
			Log.log.warn(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

		return orgVO;
	}

	public static OrgsVO getOrg(String sysId, String orgName) {
		Orgs orgModel = null;
		OrgsVO orgVO = null;

		try {
			orgModel = getOrgModel(sysId, orgName);
		} catch (Throwable e) {
			Log.log.warn(e.getMessage(), e);
		}

		if (orgModel != null) {
			orgVO = /* convertModelToVOForOrgs(orgModel, null) */orgModel.doGetVO();
		}

		return orgVO;
	}

	@SuppressWarnings("unchecked")
	private static Orgs getOrgModel(String sysId, String orgName) {
		try {
			
			return (Orgs) HibernateProxy.execute(() -> {
				Orgs orgModel = null;
				if (StringUtils.isNotBlank(sysId)) {
					orgModel = HibernateUtil.getDAOFactory().getOrgsDAO().findById(sysId);
				} else if (StringUtils.isNotBlank(orgName)) {
					String sql = "from Orgs where LOWER(UName) = '" + orgName.trim().toLowerCase() + "'";
					List<Orgs> list = HibernateUtil.createQuery(sql).list();
					if (list != null && list.size() > 0) {
						orgModel = list.get(0);
					}
				}

				// Load all collections eagerly (current Hibernate version allows only single
				// collection to be fetched eagerly)

				if (orgModel != null) {
					if (orgModel.getUChildOrgs() != null)
						orgModel.getUChildOrgs().size();

					if (orgModel.getOrgGroupRels() != null)
						orgModel.getOrgGroupRels().size();
				}
				
				return orgModel;
			});
			
		} catch (Throwable e) {
			Log.log.warn(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
			return null;
		}
		
	}

	private static void getAllOrgsInHierarchyInternal(OrgsVO curentRootOrgVO, Set<OrgsVO> orgsInHierarchy) {
		if (curentRootOrgVO != null && curentRootOrgVO.getUChildOrgs() != null
				&& !curentRootOrgVO.getUChildOrgs().isEmpty()) {
			for (OrgsVO childOrg : curentRootOrgVO.getUChildOrgs()) {
				orgsInHierarchy.add(childOrg);

				OrgsVO childOrgsVOWithChildrens = getOrgNoCache(childOrg.getSys_id(), null);

				getAllOrgsInHierarchyInternal(childOrgsVOWithChildrens, orgsInHierarchy);
			}
		}
	}

	@SuppressWarnings("rawtypes")
	public static Set<String> getUsersForGroup(String groupName) {
		Set<String> users = new TreeSet<String>();
		String sql = "select u.UUserName from Users as u join u.userGroupRels as ugr join ugr.group as g where g.UName = :groupname";

		try {
			HibernateProxy.execute(() -> {
				Query q = HibernateUtil.createQuery(sql);
				q.setParameter("groupname", groupName);
				List userNames = q.list();
				for (Object userName : userNames) {
					users.add((String) userName);
				}
			});
			
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}

		return users;
	} // getUsersForGroup

	public static boolean getUserfunctionPermission(String username, String fucntion, String property) {
		// resolve.maint/admin/system users will be granted full permission on all the
		// functions.
		if (isSuperUser(username)) {
			return true;
		}

		boolean result = false;

		Pair<JSONObject, JSONObject> permissionsPair = getUserPermissions(username);

		// Always check new permissions first and if not found search in old permissions
		// if there

		boolean found = false;

		if (permissionsPair.getRight() != null) {
			Object obj = permissionsPair.getRight().get(fucntion);

			// New function search which is without property

			if (obj != null && obj instanceof Boolean) {
				result = (Boolean) obj;
				found = true;
			}

			if (!found && StringUtils.isNotBlank(property)) {
				// Search by mapping old function.property to new function

				String newFunction = RBACUtils.legacyToNewRBAC.get(fucntion + RBACUtils.RBAC_SEPARATOR + property);

				if (StringUtils.isNotBlank(newFunction)) {
					String[] newFunctionNames = newFunction.split(RBACUtils.RBAC_VALUE_SEPARATOR);

					for (int i = 0; i < newFunctionNames.length; i++) {
						obj = permissionsPair.getRight().get(newFunctionNames[i]);

						if (obj != null && obj instanceof Boolean) {
							result = (Boolean) obj;
							found = true;
							break;
						}
					}
				} else {
					Log.log.trace(String.format(PRESUMED_LEGACY_RBAC_HAS_NO_NEW_RBAC_MAPPING, fucntion,
							(StringUtils.isNotBlank(property) ? RBACUtils.RBAC_SEPARATOR + property
									: StringUtils.EMPTY)));
				}
			}
		}

		if (!found && permissionsPair.getLeft() != null) {
			Object obj = permissionsPair.getLeft().get(fucntion);

			if (obj != null) {
				if (obj instanceof String) {
					result = obj.toString().equals(property);
				} else if (obj instanceof JSONObject) {
					JSONObject functionMap = (JSONObject) obj;
					result = functionMap.getBoolean(property);
				}

				Log.log.trace(String.format(RETURNING_FUNCTION_PERMISSIONS_FROM_LEGACY_LOG, username, fucntion,
						(StringUtils.isNotBlank(property) ? RBACUtils.RBAC_SEPARATOR + property : StringUtils.EMPTY)));
			}
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	public static Pair<JSONObject, JSONObject> getUserPermissions(String username) {
		Object userPermissions = HibernateUtil.getCachedDBObject(DBCacheRegionConstants.HAS_ACCESS_RIGHT_CACHE_REGION,
				username);
		Object userNewPermissions = HibernateUtil
				.getCachedDBObject(DBCacheRegionConstants.NEW_HAS_ACCESS_RIGHT_CACHE_REGION, username);
		JSONObject permissions = null;
		JSONObject newPermissions = null;

		if (userPermissions == null && userNewPermissions == null) {
			Users user = findUserWithAllReferences(null, username);

			permissions = new JSONObject();
			newPermissions = new JSONObject();
			List<RolesVO> roleVIList = setUserRoles(user);
			mergePermissions(roleVIList, permissions, newPermissions);

			if (StringUtils.isNotBlank(NOT_IMPLEMENTED_BY_UI_FUNCTION_PERMISSIONS_JSON)) {
				JSONArray noUIImplFunctsPermissions = StringUtils
						.stringToJSONArray(NOT_IMPLEMENTED_BY_UI_FUNCTION_PERMISSIONS_JSON, true);

				// Implemented function permissions will always override the non-implemented
				// function permissions

				Set<String> implementedFunctions = permissions.keySet();

				if (noUIImplFunctsPermissions != null && noUIImplFunctsPermissions.isArray()) {
					for (int i = 0; i < noUIImplFunctsPermissions.size(); i++) {
						JSONObject noUIImplFunctPermissions = noUIImplFunctsPermissions.getJSONObject(i);

						if (!noUIImplFunctPermissions.isNullObject() && noUIImplFunctPermissions.keySet() != null
								&& noUIImplFunctPermissions.keySet().size() == 1) {
							for (String function : (Set<String>) noUIImplFunctPermissions.keySet()) {
								if (!implementedFunctions.contains(function)
										&& !(noUIImplFunctPermissions.get(function) instanceof String)) {
									permissions.accumulate(function, noUIImplFunctPermissions.get(function));

									Log.log.trace(
											String.format(FUNCTION_PERMISSIONS_NOT_IMPLEMENTED_NY_UI_ADDED_FOR_USER_LOG,
													function,
													StringUtils.jsonObjectToString(
															(JSONObject) noUIImplFunctPermissions.get(function)),
													username));
								}
							}
						}
					}
				}
			}

			// Handle new RBACs not implemented as of yet by UI

			if (StringUtils.isNotBlank(NOT_IMPLEMENTED_BY_UI_RBACS_JSON)) {
				JSONObject noUIImplRBACPermissions = StringUtils.stringToJSONObject(NOT_IMPLEMENTED_BY_UI_RBACS_JSON);

				Set<String> implementedRBACs = newPermissions.keySet();

				// Implemented new RBAC permissions will always override the non-implemented new
				// RBAC permissions

				if (noUIImplRBACPermissions != null && !noUIImplRBACPermissions.isEmpty()
						&& !noUIImplRBACPermissions.isNullObject()) {
					for (String noUIImplRBAC : (Set<String>) noUIImplRBACPermissions.keySet()) {
						if (!implementedRBACs.contains(noUIImplRBAC)
								&& noUIImplRBACPermissions.get(noUIImplRBAC) instanceof Boolean) {
							newPermissions.accumulate(noUIImplRBAC, noUIImplRBACPermissions.get(noUIImplRBAC));

							Log.log.trace(String.format(RBAC_PERMISSIONS_NOT_IMPLEMENTED_NY_UI_ADDED_FOR_USER_LOG,
									noUIImplRBAC, noUIImplRBACPermissions.get(noUIImplRBAC), username));
						}
					}
				}
			}

			cacheUserPermissions(permissions, newPermissions, username);
		} else {
			permissions = (JSONObject) userPermissions;
			newPermissions = (JSONObject) userNewPermissions;
		}

		return Pair.of(permissions, newPermissions);
	}

	@SuppressWarnings("unchecked")
	public static void mergePermissions(List<RolesVO> userRoles, JSONObject permissions, JSONObject newPermissions) {
		if (userRoles != null) {
			Iterator<RolesVO> it = userRoles.iterator();
			// The user's permissions are merged (ORed) from the same permissions of all
			// roles.

			Collection<String> aRBACs = RBACUtils.getApplicationRBACs();

			while (it.hasNext()) {
				JSONObject cur = it.next().getUPermissions();

				Log.log.trace(String.format(DB_LEGACY_OR_NEW_RBAC_JSON_LOG, cur));
				JSONObject newCur = RBACUtils.mapToNewRBACs(cur);

				if ((cur == null || cur.isNullObject() || cur.isEmpty())
						&& (newCur == null || newCur.isNullObject() || newCur.isEmpty())) {
					continue;
				}

				// Handles sir A-RBAC ("sir") not implemented by UI.

				if (!newPermissions.keySet().contains(RBACUtils.RBAC_SIR)
						&& !newPermissions.keySet().contains(RBACUtils.RBAC_SIR_ENABLED)
						&& !newCur.keySet().contains(RBACUtils.RBAC_SIR)
						&& !newCur.keySet().contains(RBACUtils.RBAC_SIR_ENABLED)
						&& StringUtils.isNotBlank(NOT_IMPLEMENTED_BY_UI_RBACS_JSON)) {
					JSONObject noUIImplRBACPermissions = StringUtils
							.stringToJSONObject(NOT_IMPLEMENTED_BY_UI_RBACS_JSON);

					if (noUIImplRBACPermissions != null && !noUIImplRBACPermissions.isEmpty()
							&& !noUIImplRBACPermissions.isNullObject()) {
						for (String noUIImplRBAC : (Set<String>) noUIImplRBACPermissions.keySet()) {
							if (noUIImplRBAC.equals(RBACUtils.RBAC_SIR)
									&& noUIImplRBACPermissions.get(noUIImplRBAC) instanceof Boolean) {
								newCur.accumulate(noUIImplRBAC, noUIImplRBACPermissions.get(noUIImplRBAC));
								newCur.accumulate(RBACUtils.RBAC_SIR_ENABLED,
										noUIImplRBACPermissions.get(noUIImplRBAC));

								Log.log.trace(String.format(ARBAC_NOT_IMPLEMENTED_BY_UI_ADDED_FOR_ROLE, noUIImplRBAC,
										noUIImplRBACPermissions.get(noUIImplRBAC)));
								break;
							}
						}
					}
				}

				Log.log.trace(String.format(TRANSFORMED_IF_DB_LEGACY_RBAC_JSON, newCur));

				// merge legacy RBAC permissions

				Iterator<String> keys = cur.keys();
				while (keys.hasNext()) {
					String key = (String) keys.next();
					if (permissions.get(key) == null) {
						permissions.put(key, cur.get(key));
					} else {
						Object val = permissions.get(key);
						if (val instanceof JSONObject) {
							JSONObject permission = (JSONObject) val;
							JSONObject curPermission = (JSONObject) cur.get(key);
							/*
							 * 'disabled' flag will be treated as false;
							 */

							// existing view flag
							Boolean existingview = toBoolean(permission, "view");
							permission.put("view", existingview | toBoolean(curPermission, "view"));

							// existing create flag
							Boolean existingCreate = toBoolean(permission, "create");
							permission.put("create", existingCreate | toBoolean(curPermission, "create"));

							// existing modify flag
							Boolean existingModify = toBoolean(permission, "modify");
							permission.put("modify", existingModify | toBoolean(curPermission, "modify"));

							// existing delete flag
							Boolean existingDelete = toBoolean(permission, "delete");
							permission.put("delete", existingDelete | toBoolean(curPermission, "delete"));

							// existing execute flag
							Boolean existingExecute = toBoolean(permission, "execute");
							permission.put("execute", existingExecute | toBoolean(curPermission, "execute"));

							permissions.put(key, permission);
						} else if (val instanceof String) {
							String permission = (String) val;
							String curPermission = (String) cur.get(key);
							if (permission.equals("all") || curPermission.equals("all")) {
								permissions.put(key, "all");
							} else {
								permissions.put(key, "user");
							}
						}
					}
				}

				// merge new RBAC permissions

				Iterator<String> newKeys = newCur.keys();

				// Process all ARBACs first

				while (newKeys.hasNext()) {
					String newKey = (String) newKeys.next();

					if (aRBACs.contains(newKey)) {
						if (newPermissions.get(newKey) == null) {
							newPermissions.put(newKey, newCur.get(newKey));
						} else {
							newPermissions.put(newKey,
									Boolean.valueOf(((Boolean) newPermissions.get(newKey)).booleanValue()
											| ((Boolean) newCur.get(newKey)).booleanValue()));
						}
					}
				}

				/*
				 * Process RBACs after A-RBACs since effective RBAC will depend on A-RBACs. For
				 * example RBAC sir.xxx.yyy depends on A-RABC sir
				 */

				newKeys = newCur.keys();

				while (newKeys.hasNext()) {
					String newKey = (String) newKeys.next();

					String newKeyARBAC = !aRBACs.contains(newKey) ? RBACUtils.getApplicationRBAC(newKey) : null;

					boolean newCurrValue = ((Boolean) newCur.get(newKey)).booleanValue()
							& (StringUtils.isNotBlank(newKeyARBAC)
									? ((Boolean) newPermissions.get(newKeyARBAC)).booleanValue()
									: true);

					if (newPermissions.get(newKey) == null) {
						newPermissions.put(newKey, newCurrValue);
					} else {
						newPermissions.put(newKey,
								Boolean.valueOf(((Boolean) newPermissions.get(newKey)).booleanValue() | newCurrValue));
					}
				}
			}
		}
	}

	private static void cacheUserPermissions(JSONObject permissions, JSONObject newPermissions, String username) {
		HibernateUtil.cacheDBObject(DBCacheRegionConstants.HAS_ACCESS_RIGHT_CACHE_REGION,
				new String[] { "com.resolve.persistence.model.Users", "com.resolve.persistence.model.UserRoleRel",
						"com.resolve.persistence.model.Roles", "com.resolve.persistence.model.UserGroupRel",
						"com.resolve.persistence.model.Groups", "com.resolve.persistence.model.GroupRoleRel",
						"com.resolve.persistence.model.Organization", "com.resolve.persistence.model.Orgs",
						"com.resolve.persistence.model.OrgGroupRel" },
				username, permissions);

		HibernateUtil.cacheDBObject(DBCacheRegionConstants.NEW_HAS_ACCESS_RIGHT_CACHE_REGION,
				new String[] { "com.resolve.persistence.model.Users", "com.resolve.persistence.model.UserRoleRel",
						"com.resolve.persistence.model.Roles", "com.resolve.persistence.model.UserGroupRel",
						"com.resolve.persistence.model.Groups", "com.resolve.persistence.model.GroupRoleRel",
						"com.resolve.persistence.model.Organization", "com.resolve.persistence.model.Orgs",
						"com.resolve.persistence.model.OrgGroupRel" },
				username, newPermissions);
	}

	private static boolean toBoolean(JSONObject o, String k) {
		Object val = o.get(k);
		if (val instanceof Boolean) {
			return (Boolean) val;
		}
		return false;
	}
}
