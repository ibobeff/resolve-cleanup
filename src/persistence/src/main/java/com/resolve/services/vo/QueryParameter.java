/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

public class QueryParameter
{
    private String name;
    private Object value;
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
        if(name != null && name.contains("."))
        {
            //hibernate does not like . in a parameter name
            this.name = name.replaceAll("\\.", "_");
        }
    }
    public Object getValue()
    {
        return value;
    }
    public void setValue(Object value)
    {
        this.value = value;
    }
}
