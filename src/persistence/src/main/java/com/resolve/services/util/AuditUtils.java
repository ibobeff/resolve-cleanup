/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.util;

import java.util.Map;

import com.resolve.persistence.model.AuditLog;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class is responsible for storing audit information.
 */
public class AuditUtils
{
    public static void log(String source, String message)
    {
    	GeneralHibernateUtil.log(source, message);
    }
    
    public static void insertLog(String source, String[] names, Object[] values)
    {
        Map<String, String> content = StringUtils.arraysToMap(names, values);
        GeneralHibernateUtil.log(source, "INSERT - Values: " + content.toString());
    }

    public static void updateLog(String source, String[] names, Object[] values, Object[] prevValues)
    {
        Map<String, String> content = StringUtils.arraysToMap(names, values);
        Map<String, String> prevContent = StringUtils.arraysToMap(names, prevValues);

        GeneralHibernateUtil.log(source, "UPDATE - Previous Values: " + prevContent.toString() 
        		+ " New Values: " + content.toString());
    }

    public static void deleteLog(String source, String[] names, Object[] values)
    {
        Map<String, String> content = StringUtils.arraysToMap(names, values);
        GeneralHibernateUtil.log(source, "DELETE - Values: " + content.toString());
    }
    
    public static void saveAuditLog(AuditLog model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
            
	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getAuditLogDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getAuditLogDAO().persist(model);
	            }
            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);

        }
    }
}
