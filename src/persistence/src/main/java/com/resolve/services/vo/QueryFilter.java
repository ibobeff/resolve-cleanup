/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.hibernate.engine.jdbc.NonContextualLobCreator;

import com.resolve.persistence.model.BaseModel;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class QueryFilter
{
    public static final String BOOL_TYPE = "bool";
    public static final String DATE_TYPE = "date";
    public static final String NUMERIC_TYPE = "numeric";
    public static final String FLOAT_TYPE = "float";
    public static final String INT_TYPE = "int";
    public static final String DOUBLE_TYPE = "double";
    public static final String CLOB_TYPE = "clob";
    public static final String LONG_TYPE = "long";
    
    //experimental type. the intention is to support the SQL IN QUERY
    //or terms query/filter in ES. note that the value must be
    //a comma separated list of strings
    public static final String TERMS_TYPE = "terms";

    public static final String EQUALS = "equals";
    public static final String NOT_EQUALS = "notequals";
    public static final String STARTS_WITH = "startsWith";
    public static final String NOT_STARTS_WITH = "notStartsWith";
    public static final String ENDS_WITH = "endsWith";
    public static final String NOT_ENDS_WITH = "notEndsWith";
    public static final String CONTAINS = "contains";
    public static final String NOT_CONTAINS = "notContains";
    public static final String FULLTEXT = "fulltext";

    public static final String BEFORE = "before";
    public static final String AFTER = "after";
    public static final String ON = "on";
    private static final String IN = "in";
    private static final String NOT_IN = "not in";
    public static final String BETWEEN = "between"; //generally for Date range filter

    public static final String GREATER_THAN = "greaterThan";
    public static final String GREATER_THAN_OR_EQUAL = "greaterThanOrEqual";
    public static final String LESS_THAN = "lessThan";
    public static final String LESS_THAN_OR_EQUAL = "lessThanOrEqual";
    
    // Currently supported in ES only, EXISTS maps to ES exists filter and NOT EXISTS to ES mising filter
    // HP TODO  Adding support for SQL will need subquery support (NOT) EXISTS (subquery)
    
    public static final String EXISTS = "exists";
    public static final String NOT_EXISTS = "notExists";
    private static final Pattern PARENTHESES_PATTERN = Pattern.compile("\\((.*?)\\)");
    private static final Pattern MAP_OPERATOR_PATTERN = Pattern.compile("^(map)\\((.*?)\\)$");
    private static final Pattern VALUE_OPERATOR_PATTERN = Pattern.compile("^(value)\\((.*?)\\)$");

	private static final String MAP_COMMAND = "map";
	private static final String KEY_COMMAND = "key";
	private static final String VALUE_COMMAND = "value";
    private static final String OR_OPERATOR = " OR ";
	private static final String AND_OPERATOR = " AND ";
	
	private static final String OPENING_SQUARE_BRACKET = "[";
	private static final String CLOSING_SQUARE_BRACKET = "]";
	private static final String COMMA_SEPARATOR = ",";

	private String type;
    private String value;
    private String startValue; //generally for Date range filter
    private String endValue; //generally for Date range filter
    private String field;
    private String condition;
    private String paramName;
    private Boolean caseSensitive; //is the value case sensitive?
    
    //this count is for the condition where the 'where' clause has same column being twice
    //for eg. u_status != 'CLOSED' and u_status = 'REJECTED' ....
    private int startCount = 0;

    private static Set<String> DATE_NUMBER_CONDITIONS = new HashSet<String>();
    static
    {
        DATE_NUMBER_CONDITIONS.add(BEFORE);
        DATE_NUMBER_CONDITIONS.add(AFTER);
        DATE_NUMBER_CONDITIONS.add(ON);
        DATE_NUMBER_CONDITIONS.add(BETWEEN);

        DATE_NUMBER_CONDITIONS.add(GREATER_THAN);
        DATE_NUMBER_CONDITIONS.add(GREATER_THAN_OR_EQUAL);
        DATE_NUMBER_CONDITIONS.add(LESS_THAN);
        DATE_NUMBER_CONDITIONS.add(LESS_THAN_OR_EQUAL);
    }

    public QueryFilter()
    {
        this.condition = EQUALS;
    }
    
    public QueryFilter(String field, String comparison)
    {
        this("auto", null, field, comparison, false);
    }
    
    public QueryFilter(String field, String comparison, String value)
    {
        this("auto", value, field, comparison, false);
    }

    public QueryFilter(String type, String value, String field, String comparison)
    {
        this(type, value, field, comparison, false);
    }

    public QueryFilter(String type, String value, String field, String comparison, Boolean caseSensitive)
    {
        this.type = type;
        this.caseSensitive = caseSensitive;
        if(StringUtils.isBlank(value))
        {
            this.value = null;
        }
        else
        {
            if(caseSensitive)
            {
                this.value = value;
            }
            else
            {
                this.value = value.toLowerCase();
            }
        }
        if (StringUtils.isNotBlank(field) && field.startsWith("u") && !field.startsWith("u_"))
        {
            field = field.substring(0, 2).toUpperCase() + field.substring(2);
        }
        
        this.field = field;
        this.condition = comparison;
    }

    /**
     * In general used for date range.
     * Example: QueryFilter queryFilter = new QueryFilter("date", "sysCreatedOn", "131746899982", "1417801829982", "between");
     * 
     * @param type
     * @param field
     * @param startValue
     * @param endValue
     * @param comparison
     */
    public QueryFilter(String type, String field, String startValue, String endValue, String comparison)
    {
        this.type = type;
        if(StringUtils.isBlank(startValue))
        {
            this.startValue = "0";
        }
        else
        {
            this.startValue = startValue;
        }
        if(StringUtils.isBlank(endValue))
        {
            this.endValue = "0";
        }
        else
        {
            this.endValue = endValue;
        }
        
        if (StringUtils.isNotBlank(field) && field.startsWith("u") && !field.startsWith("u_"))
        {
            field = field.substring(0, 2).toUpperCase() + field.substring(2);
        }
        this.field = field;
        this.condition = comparison;
    }

    public static List<QueryFilter> decodeJson(String json)
    {
        List<QueryFilter> filters = new ArrayList<QueryFilter>();
        if (StringUtils.isNotBlank(json))
        {
            try
            {
                List<Map<String, Object>> filterList = new ObjectMapper().readValue(json, new TypeReference<List<Map<String, Object>>>()
                {
                });
                
                for (Map<String, Object> filter : filterList)
                {
                    if(filter != null && filter.size() > 0)
                    {
                        boolean isCaseSensitive = false;
                        QueryFilter queryFilter = new QueryFilter();
                        String type = "auto";
                        if(filter.get("type") != null)
                        {
                            type = filter.get("type").toString();
                        }
                        queryFilter.setType(type);

                        if(filter.get("field") != null)
                        {
                            String field = filter.get("field").toString();
                            if (StringUtils.isNotBlank(field) && field.startsWith("u") && !field.startsWith("u_"))
                            {
                                field = field.substring(0, 2).toUpperCase() + field.substring(2);
                            }
                            queryFilter.setField(field);
                        }
                        
                        if(filter.get("caseSensitive") != null)
                        {
                            isCaseSensitive = Boolean.valueOf(filter.get("caseSensitive").toString());
                            queryFilter.setCaseSensitive(isCaseSensitive);
                        }
                        if(filter.get("condition") != null)
                        {
                            queryFilter.setCondition(filter.get("condition").toString());
                        }
                        
                        String value = "";
                        if(filter.get("value") != null)
                        {
                            value = (isCaseSensitive ? filter.get("value").toString() : filter.get("value").toString().toLowerCase());
                        }
                        queryFilter.setValue(value);
                        
                        String startValue = "";
                        if(filter.get("startValue") != null)
                        {
                            startValue = (isCaseSensitive ? filter.get("startValue").toString() : filter.get("startValue").toString().toLowerCase());
                        }
                        queryFilter.setStartValue(startValue);
                        
                        String endValue = "";
                        if(filter.get("endValue") != null)
                        {
                            endValue = (isCaseSensitive ? filter.get("endValue").toString() : filter.get("endValue").toString().toLowerCase());
                        }
                        queryFilter.setEndValue(endValue);
                        
                        String paramName = filter.get("paramName") != null ? filter.get("paramName").toString().toLowerCase() : "";
                    	queryFilter.setParamName(paramName);
                    	
                        if(!queryFilter.isValid())
                        {
                            Log.log.warn("Filter does not have all the required attributes to filter the request :" + queryFilter.toString());
                        }
                        else
                        {
                            filters.add(queryFilter);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error decoding the QueryFilter", e);
            }
        }
        return filters;
    }
    
    public boolean isValid()
    {
        boolean result = false;
        
        // Filter is valid if and only if it has field name or condition and value or range (i.e. start and end value)
        // Value can be null if the condition is one of the EXISTS or NOT EXISTS OR
        // Field is "sysOrg" and condition is equals
        if((StringUtils.isNotBlank(getField()) || StringUtils.isNotBlank(getCondition())) &&
           ((getField().equals(BaseModel.SYS_ORG_FIELD_NAME) && getCondition().equalsIgnoreCase(EQUALS)) ||
            (getCondition().equalsIgnoreCase(EXISTS) || getCondition().equalsIgnoreCase(NOT_EXISTS)) || 
            (!getCondition().equalsIgnoreCase(EXISTS) && !getCondition().equalsIgnoreCase(NOT_EXISTS) &&
             (StringUtils.isNotBlank(getValue()) || (StringUtils.isNotBlank(getStartValue()) && StringUtils.isNotBlank(getEndValue()))))))
        {
            result = true;
        }
        
        return result;
    }
    
    public boolean isValidForSQLOrHQL()
    {
        //For SQL or HQL Filter is valid only if it has field name and condition and value or range (i.e. start and end value)
        return isValid() && StringUtils.isNotBlank(getField());
    }
    
    public static String encodeToJson(List<QueryFilter> filters) {
    	
    	StringBuilder sb = new StringBuilder();
    	
    	sb.append(OPENING_SQUARE_BRACKET);
    	
        if (filters != null && filters.size() > 0)
        {
        	int i = filters.size();
        	
        	for (QueryFilter filter : filters) {
	            try {
	                String json = new ObjectMapper().writeValueAsString(filter);
	                sb.append(json);
	                
	                if (--i > 0) {
	                	sb.append(COMMA_SEPARATOR);
	                }
	            }
	            catch (Exception e)
	            {
	            	Log.log.error("Error " + e.getLocalizedMessage() + " occurred while converting query filter [" + filter + 
	            				  "] into JSON.", e);
	            	throw new RuntimeException(e);
	            }
        	}
        }

        sb.append(CLOSING_SQUARE_BRACKET);
        
        return sb.toString();
    }

    public String getWhereClause(String prefixTableAlias)
    {
        String result = "";

        if(StringUtils.isBlank(prefixTableAlias))
        {
            prefixTableAlias = "";
        }
        
        if (StringUtils.isNotBlank(prefixTableAlias) && !prefixTableAlias.endsWith("."))
        {
            prefixTableAlias = prefixTableAlias + ".";
        }
        
        if(this.value != null || (this.startValue != null && this.endValue != null))
        {
            StringBuilder sb = new StringBuilder();
            String comp = getComp();

            String[] values = this.value != null ? this.value.split("\\|") : new String[0];
    
            //flag to know if its a string or any other datatype
            //used for making the search case insensitive
            boolean isStringQuery = true;
            if(DATE_NUMBER_CONDITIONS.contains(this.condition) || 
               (!this.type.equalsIgnoreCase("string") && !this.type.equalsIgnoreCase("auto") &&
                !this.type.equalsIgnoreCase(TERMS_TYPE)))
            {
                isStringQuery = false;
            }
    
            int count = getStartCount();

            if (BETWEEN.equalsIgnoreCase(this.condition) && StringUtils.isNotEmpty(this.startValue) && StringUtils.isNotEmpty(this.endValue)) {
                String startParam = this.field.replaceAll("\\.", "_") + "_" + count;
                String endParam = this.field.replaceAll("\\.", "_") + "_" + ++count;
            	sb.append("(").append(this.field).append(" BETWEEN ").append(":").append(startParam).append(AND_OPERATOR).append(":").append(endParam).append(")");
            } else {
        		int valueCount = 0;
                for (String value : values)
                {
            		 /*
            		  * Map command allows to filter data by key:value pairs. Syntax: map(field_name)
            		  * Values should be provided in key-value form using the colon(:) delimiter.
            		  * In case of multiple values for same key separate each key (same):value pair
            		  * by pipe "|" symbol.
            		  */
            		Matcher mapMatcher = MAP_OPERATOR_PATTERN.matcher(this.field);
                	
                	if (mapMatcher.find()) {
                		// Map command allows to filter data by key:value pairs. Syntax: map(field_name)
                		// Values should be provided in key-value form using the colon(:) delimiter.
                		String paramBase = (this.field.replaceAll("\\.", "_") + "_").replaceAll("[()]", "_");
                        String keyParam = paramBase + count++;
                        String keyName = this.field.replace(MAP_COMMAND, KEY_COMMAND);
                        String valueName = this.field.replace(MAP_COMMAND, VALUE_COMMAND);
                        String valueParam = paramBase + count++;
                        
                        sb.append("(").append(keyName).append("=:").append(keyParam).append(AND_OPERATOR);
    					                	
	                    // hibernate map implementation has issues when map has same values for different keys
	                    // the workaround is to reverse the statement and use :param in (valueName) instead of
	                    // valueName = :param 
                        
	            		sb.append(":").append(valueParam).append(" in (").append(valueName).append("))");
	
						if (++valueCount < values.length) {
	                		sb.append(OR_OPERATOR);
	                	}
                	} else {
                        boolean addField = true;
                        
                        if (count > getStartCount())
                        {
                        	if (comp.equals("!="))
                        	{
                        		/*
                        		 * Converts QueryFIlter of type string/auto with condition notequal and value "a|b|c" i.e. pipe delimited values
                        		 * SQL where clause (field != a AND field != b AND field != c)
                        		 */
                        		sb.append(AND_OPERATOR);
                        	}
                        	else
                        	{
                        		/*
                        		 * Converts QueryFIlter of type string/auto with condition equal and value "a|b|c" i.e. pipe delimited values
                        		 * SQL where clause (field = a OR field = b OR field = c)
                        		 */
                        		sb.append(OR_OPERATOR);
                        	}
                        }
                        
                        if (values.length > 1)
                        {
                            sb.append("(");
                        }
                        
                        if ("NULL".equalsIgnoreCase(value))
                        {
                            addField = false;
                            if (comp.equals("!="))
                                comp = " IS NOT NULL";
                            else
                                comp = " IS NULL";
            
                        }
                        if ("NIL".equalsIgnoreCase(value))
                        {
                            addField = false;
                            if (comp.equals("!="))
                                comp = " IS NOT NULL AND " + prefixTableAlias + this.field + " <> ''";
                            else
                                comp = " IS NULL OR " + prefixTableAlias + this.field + " = ''";
                            addField = false;
                        }

                    	if (QueryFilter.FULLTEXT.equalsIgnoreCase(this.condition)) {
                    		comp = IN;
                    	}

                        if(isStringQuery)
                        {
                        	Matcher mapValueMatcher = VALUE_OPERATOR_PATTERN.matcher(this.field);

                        	if (mapValueMatcher.find()) {
                        		sb.append("(").append(this.field);
                        	} else {
								String lowerPrefix = isCaseSensitive() ? "" : " (LOWER(";
								String lowerSuffix = isCaseSensitive() ? "" : ") ";

								if ((this.type.equalsIgnoreCase("string") || this.type.equalsIgnoreCase("auto"))
										&& value.contains("%") && (comp.equals("!=") || comp.equals("="))) {
									String likeComp = comp.equals("=") ? " like " : "not like ";
									sb.append(lowerPrefix + prefixTableAlias + this.field + lowerSuffix + likeComp);
								} else {
									sb.append(lowerPrefix + prefixTableAlias + this.field + lowerSuffix + comp);
								}
                        	}
                        }
                        else
                        {
                            sb.append(" (" + prefixTableAlias + this.field + " " + comp);
                        }

                        paramName = this.field.replaceAll("\\.", "_") + "_" + count;

                    	Matcher m = PARENTHESES_PATTERN.matcher(this.field);
                        if (m.find()) {
                            paramName = paramName.replaceAll("[()]", "_");
                    	}
                        
                        if (addField)
                        {
                            String inOrNotInPrefix = "";
                            String inOrNotInSuffix = "";
                            
							if (comp.equalsIgnoreCase(IN) || comp.equalsIgnoreCase(NOT_IN)) 
							{
                                inOrNotInPrefix = "(";
                                inOrNotInSuffix = ")";
                            }
                            String binary = "";
                            if (HibernateUtil.isMySQL() && isCaseSensitive() &&
                            	/*
                            	 * HP TODO 
                            	 * 
                            	 * Support case sensitive TERMS_TYPE query with 
                            	 * conditions equal or notequal
                            	 * i.e. SQL IN(a,b,c) or SQL NOT IN (a, b, c) for MySQL
                            	 * 
                            	 * Challenge here is conversion of comma separated elements 
    							 * has to happen during parameter setting. Not sure if it 
    							 * would be possible though. 
    							 * 
    							 * OR 
    							 * 
    							 * Transform TERMS_TYPE with conditions equal or notequal
    							 * QueryFilter for MySQL to sting/auto type QueryFilter with
    							 * pipe delimited values right before processing QueryFilter.
                            	 */
                                !comp.equalsIgnoreCase(IN) && !comp.equalsIgnoreCase(NOT_IN)) 
                            {
                                binary = " BINARY ";
                            }
                            
                        	Matcher mapValueMatcher = VALUE_OPERATOR_PATTERN.matcher(this.field);
                            if (mapValueMatcher.find()) {
                            	sb = new StringBuilder(":").append(paramName).append(" in ").append(sb);
                            } else {
								if (StringUtils.isNotBlank(binary)) {
									sb.append(inOrNotInPrefix + binary + "( :" + paramName + inOrNotInSuffix + " ) ");
								} else {
									sb.append(inOrNotInPrefix + " :" + paramName + inOrNotInSuffix);
								}
                            }
                            count++;
                        }
            
                        if (this.condition.equalsIgnoreCase("on"))
                        {
                            sb.append(" AND :" + paramName);
                            count++;
                        }
                        
                        if (!isStringQuery||!isCaseSensitive())
                        	sb.append(")");
                        
                        if (values.length > 1)
                        {
                            sb.append(")");
                        }
                	}
                }
            }
            
            result = "(" + sb.toString() + ")";
        }
        return result;
    }

    public String getComp()
    {
        String comp = "";
        
        // terms type with condition EQUALS/NOT_EQUALS i.e SQL IN/SQL NOT IN
        if (isTermsType() && 
            (this.condition.equalsIgnoreCase(EQUALS) || this.condition.equalsIgnoreCase(NOT_EQUALS)))
        {
            if (this.condition.equalsIgnoreCase(EQUALS)) comp = IN;
            else if (this.condition.equalsIgnoreCase(NOT_EQUALS)) comp = NOT_IN;
        }
        
        // String
        else if (this.condition.equalsIgnoreCase(EQUALS)) comp = "=";
        else if (this.condition.equalsIgnoreCase(NOT_EQUALS)) comp = "!=";
        else if (this.condition.equalsIgnoreCase(STARTS_WITH)) comp = "like ";
        else if (this.condition.equalsIgnoreCase("endsWith")) comp = "like ";
        else if (this.condition.equalsIgnoreCase("notStartsWith")) comp = "not like ";
        else if (this.condition.equalsIgnoreCase("notEndsWith")) comp = "not like ";
        else if (this.condition.equalsIgnoreCase("contains")) comp = "like ";
        else if (this.condition.equalsIgnoreCase("notContains")) comp = "not like";

        // Date
        else if (this.condition.equalsIgnoreCase("before")) comp = "<";
        else if (this.condition.equalsIgnoreCase("after")) comp = ">";
        else if (this.condition.equalsIgnoreCase("on")) comp = "between ";

        // Number
        else if (this.condition.equalsIgnoreCase("greaterThan")) comp = ">";
        else if (this.condition.equalsIgnoreCase("greaterThanOrEqualTo")) comp = ">=";
        else if (this.condition.equalsIgnoreCase("lessThan")) comp = "<";
        else if (this.condition.equalsIgnoreCase("lessThanOrEqualTo")) comp = "<=";

        return comp;
    }

    @JsonIgnore
    public List<QueryParameter> getParameters()
    {
        List<QueryParameter> temp = new ArrayList<QueryParameter>();

        int count = getStartCount();
        if (BETWEEN.equalsIgnoreCase(this.condition) && StringUtils.isNotEmpty(this.startValue) && StringUtils.isNotEmpty(this.endValue)) {
            QueryParameter startParam = new QueryParameter();
            startParam.setName(this.field + "_" + count);
            if (this.isDateType()) {
            	startParam.setValue(DateUtils.parseISODate(startValue).toDate());
            } else {
            	startParam.setValue(startValue);
            }
            temp.add(startParam);

            QueryParameter endParam = new QueryParameter();
            endParam.setName(this.field + "_" + ++count);
            if (this.isDateType()) {
            	endParam.setValue(DateUtils.parseISODate(endValue).toDate());
            } else {
            	endParam.setValue(endValue);
            }
            temp.add(endParam);
            
            return temp;
        }

        String[] values = {};
        if(value!=null)
            values = this.value.split("\\|");
        for (String value : values)
        {
        	Matcher mapMatcher = MAP_OPERATOR_PATTERN.matcher(this.field);
        	
        	if (mapMatcher.find()) {
        		String[] mapEntry = value.split(":");
        		// entry has name and value
        		if (mapEntry.length == 2) {
                	String paramName = this.field.replaceAll("[()]", "_");
                	temp.add(createQueryParameter(paramName + "_" + count++, mapEntry[0]));
                	temp.add(createQueryParameter(paramName + "_" + count++, mapEntry[1]));
        		}
        	} else {
                QueryParameter p = new QueryParameter();
            	String pName = this.field + "_" + count;

            	Matcher m = PARENTHESES_PATTERN.matcher(this.field);
            	if (m.find()) {
                    pName = pName.replaceAll("[()]", "_");
            	}

                p.setName(pName);
                p.setValue(value);

                QueryParameter p1 = new QueryParameter();
                p1.setName(this.field + "_" + (count + 1));
                p1.setValue(value);

                // Note: To edit this, also make changes to CacheUtils.getFieldNamesForModel
                // Parse out date, number, boolean objects to properly set them into
                // the params
                if (this.type.equalsIgnoreCase(BOOL_TYPE))
                {
                    p.setValue(Boolean.valueOf(value));
                }
                if (this.type.equalsIgnoreCase(DATE_TYPE))
                {
                    // Date is either a string indicating relative date or a
                    // formatted date
                    if (value.equalsIgnoreCase("today"))
                    {
                        p.setValue(DateUtils.GetUTCDate().toDate());
                        p1.setValue(DateUtils.GetUTCDate().plusDays(1).toDate());
                    }
                    else if (value.equalsIgnoreCase("tomorrow"))
                    {
                        p.setValue(DateUtils.GetUTCDate().plusDays(1).toDate());
                        p1.setValue(DateUtils.GetUTCDate().plusDays(2).toDate());
                    }
                    else if (value.equalsIgnoreCase("yesterday"))
                    {
                        p.setValue(DateUtils.GetUTCDate().minusDays(1).toDate());
                        p1.setValue(DateUtils.GetUTCDate().toDate());
                    }
                    else if (value.equalsIgnoreCase("lastWeek"))
                    {
                        p.setValue(DateUtils.GetUTCDate().minusDays(7).toDate());
                        p1.setValue(DateUtils.GetUTCDate().minusDays(6).toDate());
                    }
                    else if (value.equalsIgnoreCase("lastMonth"))
                    {
                        p.setValue(DateUtils.GetUTCDate().minusMonths(1).toDate());
                        p1.setValue(DateUtils.GetUTCDate().minusMonths(1).plusDays(1).toDate());
                    }
                    else if (value.equalsIgnoreCase("lastYear"))
                    {
                        p.setValue(DateUtils.GetUTCDate().minusYears(1).toDate());
                        p1.setValue(DateUtils.GetUTCDate().minusYears(1).plusDays(1).toDate());
                    }
                    else
                    {
                        p.setValue(DateUtils.parseISODate(value).toDate());
                        p1.setValue(DateUtils.parseISODate(value).plusDays(1).toDate());
                    }
                }
                if (this.type.equalsIgnoreCase(FLOAT_TYPE))
                {
                    if (value.contains("."))
                        p.setValue(Double.valueOf(value));
                    else
                        p.setValue(Long.valueOf(value));
                }
                
                //this is NOT available in UI, its manipulated on the server side so that the qrys have the correct type associated with it
                if (this.type.equalsIgnoreCase(INT_TYPE))
                {
                    if (value.contains("."))
                        p.setValue(Float.valueOf(value));
                    else
                        p.setValue(Integer.valueOf(value));
                }

                if (this.type.equalsIgnoreCase(DOUBLE_TYPE))
                {
                    p.setValue(Double.valueOf(value));
                }
                
                if (this.type.equalsIgnoreCase(CLOB_TYPE))
                {
                    try
                    {
                        p.setValue(NonContextualLobCreator.INSTANCE.wrap(NonContextualLobCreator.INSTANCE.createClob(value)));
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Error " + e.getMessage() + " in setting [" + value + "] as org.hibernate.lob.ClobImpl." , e);
                    }
                }
                if (this.type.equalsIgnoreCase(LONG_TYPE))
                {
                	p.setValue(Long.valueOf(value));
                }
                
                if (this.type.equalsIgnoreCase("string") || this.type.equalsIgnoreCase("auto"))
                {
                    String valueCS = isCaseSensitive() ? value : value.toLowerCase();
                    
                    if (this.condition.equalsIgnoreCase(EQUALS)) p.setValue(valueCS);
                    else if (this.condition.equalsIgnoreCase( NOT_EQUALS)) p.setValue(valueCS);
                    else if (this.condition.equalsIgnoreCase(STARTS_WITH)) p.setValue(valueCS + "%");
                    else if (this.condition.equalsIgnoreCase(NOT_STARTS_WITH)) p.setValue(valueCS + "%");
                    else if (this.condition.equalsIgnoreCase(ENDS_WITH)) p.setValue("%" + valueCS);
                    else if (this.condition.equalsIgnoreCase(NOT_ENDS_WITH)) p.setValue("%" + valueCS);
                    else if (this.condition.equalsIgnoreCase(CONTAINS)) p.setValue("%" + valueCS + "%");
                    else if (this.condition.equalsIgnoreCase(NOT_CONTAINS)) p.setValue("%" + valueCS + "%");
                }
                
                if (this.type.equalsIgnoreCase(TERMS_TYPE) || FULLTEXT.equalsIgnoreCase(this.condition))
                {
                    List<String> inOrNotInVals = StringUtils.stringToList(value, ",");
                    p.setValue(inOrNotInVals);
                }
                
                temp.add(p);
                count++;

                if (this.condition.equalsIgnoreCase("on"))
                {
                    temp.add(p1);
                    count++;
                }
        	}
        }

        return temp;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getValue()
    {
        if(caseSensitive != null && !caseSensitive && StringUtils.isNotBlank(this.value))
        {
            this.value = this.value.toLowerCase();
        }
        return this.value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getStartValue()
    {
        return startValue;
    }

    public void setStartValue(String startValue)
    {
        this.startValue = startValue;
    }

    public String getEndValue()
    {
        return endValue;
    }

    public void setEndValue(String endValue)
    {
        this.endValue = endValue;
    }

    public String getField()
    {
        return field;
    }

    public void setField(String field)
    {
        this.field = field;
    }

    public String getCondition()
    {
        return condition;
    }

    public void setCondition(String condition)
    {
        this.condition = condition;
    }

    public String getParamName()
    {
        return paramName;
    }

    public void setParamName(String paramName)
    {
        this.paramName = paramName;
    }

    @JsonProperty("caseSensitive")
    public Boolean getCaseSensitive()
    {
        return caseSensitive;
    }

    public void setCaseSensitive(Boolean caseSensitive)
    {
        this.caseSensitive = caseSensitive;
    }

    public boolean isBoolType()
    {
        return BOOL_TYPE.equals(getType());
    }

    public boolean isDateType()
    {
        return DATE_TYPE.equals(getType());
    }

    public boolean isTermsType()
    {
        return TERMS_TYPE.equals(getType());
    }

    public boolean isNumericType()
    {
        return NUMERIC_TYPE.equals(getType());
    }
    
    public int getStartCount()
    {
        return startCount;
    }

    public void setStartCount(int startCount)
    {
        this.startCount = startCount;
    }

    public boolean isCaseSensitive()
    {
        return Boolean.TRUE.equals(caseSensitive);
    }
    
    @Override
    public String toString()
    {
        return "QueryFilter [field=" + field + ", type=" + type + ", condition=" + condition + ", value=" + value + ", startValue=" + startValue + ", endValue=" + endValue + ", caseSensitive=" + caseSensitive + "]";
    }

    private QueryParameter createQueryParameter(String paramName, String paramValue) {
        QueryParameter param = new QueryParameter();
    	param.setName(paramName);
    	param.setValue(paramValue);
    	
    	return param;
    }
}