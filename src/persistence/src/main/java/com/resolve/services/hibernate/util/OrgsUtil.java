/******************************************************************************
* (C) Copyright 2017
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.persistence.model.Organization;
import com.resolve.persistence.model.Orgs;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class OrgsUtil
{
    public static List<OrgsVO> getOrg(QueryDTO query)
    {
        List<OrgsVO> result = new ArrayList<OrgsVO>();
        
        int limit = query.getLimit();
        int offset = query.getStart();
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            
            if(data != null)
            {
                String selectedColumns = query.getSelectColumns();
                
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        Orgs instance = new Orgs();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        OrgsVO vo = instance.doGetVO();
                        
                        /*if(vo.getUDisable() != null)
                        {
                            vo.setUDisable(!vo.getUDisable());
                        }*/
                        
                        result.add(vo);
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        Orgs instance = (Orgs) o;
                        OrgsVO vo = instance.doGetVO();
                        
                        /*if(vo.getUDisable() != null)
                        {
                            vo.setUDisable(!vo.getUDisable());
                        }*/
                        
                        result.add(vo);
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return result;
    }
    
    public static Map<String, String> getListOfAllOrgNames()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        QueryDTO query = new QueryDTO();
        query.setModelName("Orgs");
        query.setSelectColumns("sys_id,UName");
        //query.setWhereClause("UDisable is false");
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, -1, -1, false);
            
            if(data != null)
            {
                String selectedColumns = query.getSelectColumns();
                
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        Orgs instance = new Orgs();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        result.put(instance.getSys_id(), instance.getUName());
                    }//end of for loop
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        
        return result;
    }
    
    public static OrgsVO findOrgById(String sys_id)
    {
        Orgs model = null;
        OrgsVO result = null;
        
        if(!StringUtils.isEmpty(sys_id))
        {
            try
            {
                model = findOrgModelById(sys_id);
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }
        
        if(model != null)
        {
            result = model.doGetVO();
        }
        
        return result;
    }
    
    public static OrgsVO saveOrg(OrgsVO vo, String username)
    {
        if(vo != null)
        {
            Orgs model = null;
            
            if(StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equals(VO.STRING_DEFAULT))
            {
                model = findOrgModelById(vo.getSys_id());
            }
            else
            {
                model = findOrgModelByName(vo.getUName());
                
                if(model != null)
                {
                    throw new RuntimeException("Organization with name '" + vo.getUName() + "' already exists. Please add a unique name.");
                }
                else
                {
                    model = new Orgs();
                    //model.setUDisable(false);
                }
            }
            
            model.applyVOToModel(vo);
            model = saveOrg(model, username);
            vo = model.doGetVO();
        }
        
        return vo;
    }
    
    @SuppressWarnings("unchecked")
	public static Set<OrgsVO> getAllOrgVOs()
    {
        Set<OrgsVO> orgVOs = new HashSet<OrgsVO>();
        
        List<Orgs> orgs = null;
        
        try
        {
            orgs = (List<Orgs>) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getOrgsDAO().findAll();
            });
            
            
            if (orgs != null && !orgs.isEmpty())
            {
                for (Orgs org : orgs)
                {
                    orgVOs.add(org.doGetVO());
                }
            }
        }
        catch(Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return orgVOs;
    }
    
    private static Orgs findOrgModelById(String sys_id)
    {
        Orgs result = null;
        
        if(!StringUtils.isBlank(sys_id))
        {
            try
            {
            	result = (Orgs) HibernateProxy.execute(() -> {
            		return HibernateUtil.getDAOFactory().getOrgsDAO().findById(sys_id);
            	});
                
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return result;
    }
    
    public static Orgs findOrgModelByName(String name)
    {
        if(!StringUtils.isBlank(name))
        {
            
            
			try {
				return (Orgs) HibernateProxy.execute(() -> {
					Orgs result = new Orgs();
		            result.setUName(name);
					return HibernateUtil.getDAOFactory().getOrgsDAO().findFirst(result);

				});

			} catch (Exception e) {
				Log.log.warn(e.getMessage(), e);
                               HibernateUtil.rethrowNestedTransaction(e);
			}
        }
        
        return null;
    }   
    
    public static boolean hasCommonRoot(String orgsId1, String orgsId2)
    {

        boolean result = false; 
        
            try
            {
                result = (boolean) HibernateProxy.execute(() -> {
				Orgs org1 = null;
				Orgs org2 = null;

				org1 = HibernateUtil.getDAOFactory().getOrgsDAO().findById(orgsId1);
				org2 = HibernateUtil.getDAOFactory().getOrgsDAO().findById(orgsId2);

				if (org1 == null || org2 == null) {
					throw new RuntimeException("Failed to find Orgs by sys_id: " + orgsId1 + ", " + org2);
				}

				if (org1 == org2) {
					return true;
				}

				Orgs root1 = org1;
				while (root1.getUParentOrg() != null) {
					root1 = root1.getUParentOrg();
				}

				Orgs root2 = org2;
				while (root2.getUParentOrg() != null) {
					root2 = root2.getUParentOrg();
				}

				if (root1 == root2) {
					return true;
				}
                     
                     return false;
                });
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        
        return result;
    }   
    
    private static Orgs saveOrg(Orgs model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
        	final Orgs modelFinal = model;
          HibernateProxy.setCurrentUser(username);
        	model = (Orgs) HibernateProxy.execute(() -> {
        		
	            if (StringUtils.isNotBlank(modelFinal.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getOrgsDAO().update(modelFinal);
	            }
	            else
	            {
	            	modelFinal.setSys_id(null);
	                return  HibernateUtil.getDAOFactory().getOrgsDAO().persist(modelFinal);
	            }
	            return modelFinal;

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist Org " + model.getUName() + ": " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
         
        return model;
    }
}
