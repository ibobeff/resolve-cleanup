/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.query.Query;

import com.resolve.persistence.model.Properties;
import com.resolve.persistence.util.DBCacheRegionConstants;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.OrganizationVO;
import com.resolve.services.hibernate.vo.PropertiesVO;
import com.resolve.services.hibernate.vo.ResolveCaseConfigVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class PropertiesUtil
{
    // propertyname-sys_ids map
    private static Map<String, String> mapNamesAndIds = new HashMap<String, String>();
    
    public static void initProperties(String fileName) throws Exception
    {
        List<Properties> lDBProps = getPropertiesFromDB();
        List<Properties> lNewOrUpdatedDBProps = new ArrayList<Properties>();
        java.util.Properties properties = null;
        try
        {
            InputStream is = new FileInputStream(fileName);
            properties = new java.util.Properties();
            properties.load(is);
            is.close();
        }
        catch (Exception e)
        {
            throw new Exception("Failed to read from " + fileName + " file. " + e.getMessage());
        }

        Enumeration<Object> enu = properties.keys();
        while (enu.hasMoreElements())
        {
            String key = (String) enu.nextElement(); // eg.
                                                     // wiki.rights.view.Test<list>
            String tmpkey = key; // eg. wiki.rights.view.Test<list>
            String value = properties.getProperty(key);
            String type = "string"; // must reset it back to string

            // remove comments as description value
            String desc = "";
            int descPos = value.indexOf("##");
            if (descPos >= 0)
            {
                desc = value.substring(descPos + 2).trim();
                value = value.substring(0, descPos).trim();
            }

            // get type
            if (key.indexOf("<") > 0)
            {
                key = tmpkey.substring(0, tmpkey.indexOf("<"));
                type = tmpkey.substring(tmpkey.indexOf("<") + 1, tmpkey.indexOf(">"));
            }

            try
            {
                // check if its an int
                Long.parseLong(value);
                type = "long";
            }
            catch (Exception e)
            {
                // don;t do anything as it is just a test to see if this value
                // an int or string
            }

            
            //If the key is already there in DB table then update description only if it is modified
            
            Properties rsPropertyInDB = getDBProperty(key, lDBProps);
            
            if (/*!isAvailableInDB(key, lDBProps)*/rsPropertyInDB == null)
            {
                Properties p = new Properties();
                p.setUName(key);
                p.setUValue(value);
                p.setUType(type);
                p.setUDescription(desc);
                p.setSysCreatedBy("system");
                p.setSysCreatedOn(GMTDate.getDate());

                lNewOrUpdatedDBProps.add(p);
            }
            else
            {
                if (!desc.equals(rsPropertyInDB.getUDescription()))
                {
                    rsPropertyInDB.setUDescription(desc);
                    lNewOrUpdatedDBProps.add(rsPropertyInDB);
                }
            }
        }

        // insert/update it in the DB
        uploadPropertiesInDB(lNewOrUpdatedDBProps);

    } // initProperties
    
    public static void setPropertyString(String name, String value)
    {
        try
        {
            // update db
            Properties prop = getPropertiesWithName(name);
            if (prop == null)
            {
                prop = new Properties();
                prop.setUName(name);
            }
            prop.setUValue(value);

            saveProperties(prop, null);
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
    } // setPropertyString

    
    public static PropertiesVO findPropertyById(String sysId)
    {
        PropertiesVO prop = null;
        
        if(StringUtils.isNotBlank(sysId))
        {
            Properties p = findPropertyByIdPrivate(sysId);
            if(p != null)
            {
                prop = p.doGetVO();
                if(StringUtils.isNotBlank(prop.getSysOrg()))
                {
                    OrganizationVO org = OrganizationUtil.findOrganizationById(prop.getSysOrg());
                    if(org != null)
                    {
                        prop.setSysOrganizationName(org.getUOrganizationName());
                    }
                    else
                    {
                        prop.setSysOrganizationName("");
                    }
                }
            }
        }
        
        return prop;
    } // findPropertyByName
    
    public static void deletePropertiesById(String[] sysIds, String username) throws Exception
    {
        if(sysIds != null)
        {
            UsersVO user = GenericUserUtils.getUser(username);
            for(String sysId : sysIds)
            {
                deleteProperty(sysId, null, user);
            }
        }
    }
    
    public static void deletePropertiesByName(String name, String username) throws Exception
    {
        if(StringUtils.isNotEmpty(name))
        {
            UsersVO user = GenericUserUtils.getUser(username);
            deleteProperty(null, name, user);
        }
    }
    
    public static PropertiesVO findPropertyByName(String name)
    {
        PropertiesVO prop = null;
        
        if(StringUtils.isNotBlank(name))
        {
            Properties p = findPropertyByNamePrivate(name);
            if(p != null)
            {
                prop = p.doGetVO();
            }
        }
        
        return prop;
    } // findPropertyByName
    
    public static String getPropertyString(String name)
    {
        String value = null;
        try
        {
            Properties prop = getPropertiesWithName(name);

            if (prop != null)
            {
                value = prop.getUValue();
                if (value == null)
                {
                    value = "";
                }
            }

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return value;
    } // getPropertyString
    
    public static void setPropertyBoolean(String name, boolean boolValue)
    {
        try
        {
            // update db
            Properties prop = getPropertiesWithName(name);
            if (prop == null)
            {
                prop = new Properties();
                prop.setUName(name);
            }
            if (boolValue)
            {
                prop.setUValue("true");
            }
            else
            {
                prop.setUValue("false");
            }
            
            saveProperties(prop, null);
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
    } // setPropertyBoolean
    
    public static boolean getPropertyBoolean(String name)
    {
        boolean result = false;

        String value = null;
        try
        {
            Properties prop = getPropertiesWithName(name);

            if (prop != null)
            {
                value = prop.getUValue();
                if (value == null)
                {
                    value = "";
                }
            }
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }

        if (StringUtils.isEmpty(value) || value.equalsIgnoreCase("false") || value.equalsIgnoreCase("no"))
        {
            result = false;
        }
        else if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("yes"))
        {
            result = true;
        }

        return result;
    } // getPropertyBoolean

    public static void setPropertyList(String name, List<String> values)
    {

        try
        {
            Properties prop = getPropertiesWithName(name);
            prop.setUValue(StringUtils.convertCollectionToString(values.iterator(), ","));
            
            saveProperties(prop, null);
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
    } // setPropertyList
    
    public static List<String> getPropertyList(String name)
    {
        List<String> values = null;
        try
        {
            Properties prop = getPropertiesWithName(name);


            if (prop != null)
            {
                values = StringUtils.convertStringToList(prop.getUValue(), ",");
            }

        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return values;
    } // getPropertyList

    public static void setPropertyLong(String name, long value)
    {
        try
        {
            Properties prop = getPropertiesWithName(name);
            prop.setUValue(Long.toString(value));
            
            saveProperties(prop, null);
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }
    } // setPropertyLong
    
    public static int getPropertyInt(String name)
    {
        int value = -1;
        try
        {
            Properties prop = getPropertiesWithName(name);

            if (prop != null)
            {
                String strValue = prop.getUValue();
                if (!StringUtils.isEmpty(strValue))
                {
                    value = Integer.parseInt(strValue);
                }
            }
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return value;
    } // getPropertyInt
    
    public static long getPropertyLong(String name)
    {
        long value = -1;
        try
        {
            Properties prop = getPropertiesWithName(name);

            if (prop != null)
            {

                String strValue = prop.getUValue();
                if (!StringUtils.isEmpty(strValue))
                {
                    value = Long.parseLong(strValue);
                }

            }
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return value;
    } // getPropertyLong

    public static PropertiesVO saveProperties(PropertiesVO voprops, String username) throws Exception
    {
        if (voprops != null && voprops.validate())
        {
            String uiPropOrg = voprops.getSysOrg() != null && !voprops.getSysOrg().equals(VO.STRING_DEFAULT) ? voprops.getSysOrg() : "";
            String uiPropName = voprops.getUName();
            boolean isInsert = (StringUtils.isBlank(voprops.getSys_id()) || voprops.getSys_id().equals(VO.STRING_DEFAULT)) ? true : false;
            //get user info
            UsersVO user = GenericUserUtils.getUser(username);
            String userOrg = user.getBelongsToOrganization() != null ? user.getBelongsToOrganization().getSys_id() : "";
//            String dbPropOrg = "";
            Properties dbProp = null;
            if (isInsert)
            {
                dbProp = findPropertyByNamePrivate(voprops.getUName());
                if(dbProp != null)
                {
                    throw new Exception("'" + voprops.getUName() + "' already exist. Please try again.");
                }
//                
//                if (dbProp != null)
//                {
//                    dbPropOrg = StringUtils.isNotBlank(dbProp.getSysOrg()) ? dbProp.getSysOrg() : "";
//                }
//
//                if (uiPropOrg.equalsIgnoreCase(dbPropOrg) && dbProp != null)
//                {
//                    throw new Exception("Property '" + voprops.getUName() + "' already exist. Please try again.");
//                }

                dbProp = new Properties();
            }
            else
            {
                //update

                //make sure that the Property by the updated name does not exist
                dbProp = findPropertyByNamePrivate(voprops.getUName());
                if(dbProp != null && !voprops.getSys_id().equalsIgnoreCase(dbProp.getSys_id()))
                {
                    throw new Exception("'" + voprops.getUName() + "' already exist. Please try again.");
                }
                
                //get the property object by Id
                dbProp = findPropertyByIdPrivate(voprops.getSys_id());
                if(dbProp == null)
                {
                    throw new Exception("Property with sysId " + voprops.getSys_id() + " does not exist.");
                }
                

                //if the names are not same, than check if the name exist
                if (!uiPropName.equalsIgnoreCase(dbProp.getUName()))
                {
                    Properties duplicate = findPropertyByNamePrivate(voprops.getUName());
                    if(duplicate != null && !duplicate.getSys_id().equalsIgnoreCase(voprops.getSys_id()))
                    {
                        throw new Exception("'" + voprops.getUName() + "' already exist. Please try again.");
                    }
//                    
//                    if (duplicate != null)
//                    {
//                        dbPropOrg = StringUtils.isNotBlank(duplicate.getSysOrg()) ? duplicate.getSysOrg() : "";
//                    }
//
//                    if (uiPropOrg.equalsIgnoreCase(dbPropOrg) && duplicate != null)
//                    {
//                        throw new Exception("Property '" + voprops.getUName() + "' already exist. Please try again.");
//                    }
                }

                //if he is NOT AN ADMIN and if the user does not belong to the same org than he cannot edit it
                //this is multitenant user
                if (StringUtils.isNotEmpty(userOrg) && !userOrg.equalsIgnoreCase(uiPropOrg))
                {
                    throw new Exception("User '" + username + "' does not have rights to Edit property '" + voprops.getUName() + "'");
                }

            }
            // persist it
            dbProp.applyVOToModel(voprops);
            dbProp = saveProperties(dbProp, username);
            if (dbProp != null)
            {
                voprops = dbProp.doGetVO();
                
                // Not synchornizing since only sys admin should have control on system properties
                mapNamesAndIds.put(voprops.getUName(), voprops.getSys_id());
            }
        }
        return voprops;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<PropertiesVO> getPropertiesLikeName(String uname)
    {
        List<PropertiesVO> result = new ArrayList<PropertiesVO>();
        List<Properties> list = null;
        
        if (uname != null && uname.trim().length() > 0)
        {
            uname = uname.trim();

            // if there is no % in the string, then add it. Else leave it alone
            if (uname.indexOf("%") < 0)
            {
                uname = "%" + uname + "%";
            }
        }
        else
        {
            uname = null;
        }

        try
        {
        	String unameFinal = uname;
        	list = (List<Properties>) HibernateProxy.execute(() -> {
            	Query q = null;
                if (unameFinal != null)
                {
                    String sql = " from Properties where UName like :uname";
                    q = HibernateUtil.createQuery(sql);
                    q.setParameter("uname", unameFinal);
                }
                else
                {
                    String sql = " from Properties ";
                    q = HibernateUtil.createQuery(sql);

                }
                return q.list();
            });
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        
        if(list != null && list.size() > 0)
        {
            for(Properties p : list)
            {
                result.add(p.doGetVO());
            }
        }

        return result;
    } // getPropertiesLikeName
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<PropertiesVO> getPropertiesUsingWhereClause(String whereQuery, String username)
    {
        List<PropertiesVO> result = new ArrayList<PropertiesVO>();
        List<Properties> list = null;
        

        try
        {
          HibernateProxy.setCurrentUser(username);
        	list = (List<Properties>) HibernateProxy.execute(() -> {
        		
        		String query = "from Properties ";
                if(StringUtils.isNotBlank(whereQuery))
                {
                    query = query + " where " + whereQuery;
                }
        		
        		Query q = HibernateUtil.createQuery(query);
                return q.list();
        	});
            

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        
        if(list != null && list.size() > 0)
        {
            for(Properties p : list)
            {
                result.add(p.doGetVO());
            }
        }

        return result;
    } // getPropertiesLikeName
    
    public static List<PropertiesVO> getProperties(QueryDTO query, String username)
    {
        List<PropertiesVO> result = new ArrayList<PropertiesVO>();
//        UsersVO user = UserUtils.getUser(username);
//        String userOrg = user.getBelongsToOrganization() != null ? user.getBelongsToOrganization().getSys_id() : null;
        
//        String hql = "from Properties ";
//        if(StringUtils.isNotBlank(userOrg))
//        {
//            hql = hql + " where sysOrg is null or sysOrg = '" + userOrg + "' ";
//        }
//        hql = hql + " order by sysOrg, UName";
        
        int limit = query.getLimit();
        int offset = query.getStart();
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null && data.size() > 0)
            {
                //grab the map of sysId-organizationname  
                Map<String, String> mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
                
                //prepare the data
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        Properties instance = new Properties();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        //prepare the VO
                        PropertiesVO vo = instance.doGetVO();
                        if(StringUtils.isNotBlank(vo.getSysOrg()))
                        {
                            vo.setSysOrganizationName(mapOfOrganization.get(vo.getSysOrg()));
                        }
                        else
                        {
                            vo.setSysOrganizationName("");
                        }
                        result.add(vo);
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        Properties instance = (Properties) o;
                        PropertiesVO vo = instance.doGetVO();
                        if(StringUtils.isNotBlank(vo.getSysOrg()))
                        {
                            vo.setSysOrganizationName(mapOfOrganization.get(vo.getSysOrg()));
                        }
                        else
                        {
                            vo.setSysOrganizationName("");
                        }
                        result.add(vo);
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    }
    
	@SuppressWarnings("rawtypes")
	public static void deleteSocialNamespaceNotification(String namespace, String userName) {
		String queryString = "from Properties where UName = :namespace";

		try {
			HibernateProxy.execute(() -> {

				Query query = HibernateUtil.createQuery(queryString);
				List list = query.setParameter("namespace", "social.document.notification.create." + namespace).list();
				if (list.size() != 0) {
					for (Object obj : list) {
						Properties properties = (Properties) obj;
						String users = properties.getUValue();
						if (users != null) {
							List<String> userList = Arrays.asList(users.split(","));
							if (userList.contains(userName)) {
								users = users.replace(userName, "");
								properties.setUValue(users);

								saveProperties(properties, null);
							}
						}
					}
				}
			});
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}
    }    
    
    @SuppressWarnings("rawtypes")
    public static void saveNamespaceNotifications(String namespace, String userName)
    {
        String socialNotifNameSpacePropName = "social.document.notification.create." + namespace;
        String queryString = "from Properties where UName = :UName";
        Map<String, Object> queryParams = new HashMap<String, Object>();
        queryParams.put("UName", socialNotifNameSpacePropName);
        
        try
        {
            
            List list = getPropertiesUsingHql(queryString, queryParams);

            if (list.size() == 0)
            {
                // The namespace entry does not exist. So, add one.
                Properties properties = new Properties();
                properties.setUDescription("Document namespace notification user list");
                properties.setUName("social.document.notification.create." + namespace);
                properties.setUValue(userName);
                properties.setUType("list");
            
                saveProperties(properties, null);
            }
            else
            {
                Properties properties = (Properties) list.get(0);
                String users = properties.getUValue();
                if (!users.contains(userName))
                {
                    users = users + "," + userName;
                    properties.setUValue(users);
                    saveProperties(properties, null);
                }
            }

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }

    }
        
    
    ///////////////
    //private apis
    ///////////////
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static List<Properties> getPropertiesFromDB()
    {
        List<Properties> list = new ArrayList<Properties>();
        try
        {
            list = (List<Properties>) HibernateProxy.execute(() -> {
            	String sql = " from Properties ";
                Query q = HibernateUtil.createQuery(sql);
                return q.list();
            });

        }
        catch (Throwable e)
        {
            e.printStackTrace();
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return list;
    } // getPropertiesFromDB
    
    @SuppressWarnings("unused")
    private static boolean isAvailableInDB(String key, List<Properties> lDBProps)
    {
        boolean isAvailableInDB = false;

        for (Properties p : lDBProps)
        {
            if(p != null)
            {
                if (p.getUName().equalsIgnoreCase(key))
                {
                    isAvailableInDB = true;
                    break;
                }
            }
        }

        return isAvailableInDB;
    } // isAvailableInDB
    
    private static Properties getDBProperty(String key, List<Properties> lDBProps)
    {
        Properties rsProperty = null;

        for (Properties p : lDBProps)
        {
            if(p != null)
            {
                if (p.getUName().equalsIgnoreCase(key))
                {
                    rsProperty = p;
                    break;
                }
            }
        }

        return rsProperty;
    } // getDBProperty
    
    private static void uploadPropertiesInDB(List<Properties> lNewDBProps)
    {
        if (lNewDBProps != null && lNewDBProps.size() > 0)
        {
            for (Properties p : lNewDBProps)
            {
                if(p != null)
                {
                    saveProperties(p, null);
                }
            }
        }

    } // uploadPropertiesInDB
    
    private static Properties findPropertyByIdPrivate(String sys_id)
    {
        try
        {
          HibernateProxy.setCurrentUser("system");
        	return (Properties) HibernateProxy.execute(() -> {
            	Properties result = null;

	            result = HibernateUtil.getDAOFactory().getPropertiesDAO().findById(sys_id);
	            if (result != null)
	            {
	                mapNamesAndIds.put(result.getUName(), result.getSys_id());
	            }

	            return result;
            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage());
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return null;
    }
    
    private static Properties getPropertiesWithName(String uname)
    {
        Properties result = null;
        
        Object cachedObj = HibernateUtil.getCachedDBObject(DBCacheRegionConstants.PROPERTIES_CACHE_REGION, uname);
        if(StringUtils.isNotBlank(uname) && cachedObj != null && !"".equals(cachedObj)) {
            result = (Properties) HibernateUtil.getCachedDBObject(DBCacheRegionConstants.PROPERTIES_CACHE_REGION, uname);
            return result;
        }    

        String sys_id = null;
        if (mapNamesAndIds.containsKey(uname))
        {
            sys_id = mapNamesAndIds.get(uname);
        }

        if (sys_id != null)
        {
            result = findPropertyByIdPrivate(sys_id);
        }
        else
        {
            result = findPropertyByNamePrivate(uname);
        }

        if (StringUtils.isNotBlank(uname)) 
        {
            HibernateUtil.cacheDBObject(DBCacheRegionConstants.PROPERTIES_CACHE_REGION,  new String[] { "com.resolve.persistence.model.Properties"}, uname, result == null ? "" : result);
        } 
        return result;

    } // getPropertiesWithName
    
    private static Properties findPropertyByNamePrivate(String name)
    {
        Properties result = null;
        
        if(StringUtils.isNotEmpty(name))
        {
            name = name.trim();
            String hql = "from Properties where UName = :UName";
            Map<String, Object> queryParams = new HashMap<String, Object>();
            queryParams.put("UName", name);
            
            try
            {
                List<Properties> list = getPropertiesUsingHql(hql, queryParams);
                if(list != null && list.size() > 0)
                {
                    result = list.get(0);
                    mapNamesAndIds.put(name, result.getSys_id());
                    
                }
            }
            catch (Exception e)
            {
                Log.log.error("error in getting the property :" + name , e);
            }
        }
        
        return result;
    } // findPropertyByName
    
    @SuppressWarnings("unchecked")
    private static List<Properties> getPropertiesUsingHql(String hql, Map<String, Object> queryParams)
    {
        List<Properties> list = null;

        try
        {
            list = (List<Properties>) GeneralHibernateUtil.executeHQLSelect(hql, queryParams);
        }
        catch (Throwable t)
        {
            Log.log.error("Error querying Properties using HQL:" + hql + 
                          ", and query parameters " + StringUtils.mapToLogString(queryParams), t);
        }
        
        return list;
    }
    
    private static void deleteProperty(String sysId, String name, UsersVO user) throws Exception
    {
        String userOrg = user.getBelongsToOrganization() != null ? user.getBelongsToOrganization().getSys_id() : null;
        try
        {
          HibernateProxy.setCurrentUser(user.getUUserName());
        	HibernateProxy.execute(() -> {
        		Properties p = null;
            
	            if(StringUtils.isNotBlank(sysId))
	            {
	                p = HibernateUtil.getDAOFactory().getPropertiesDAO().findById(sysId);
	            }
	            else if(StringUtils.isNotBlank(name))
	            {
	                p = findPropertyByNamePrivate(name);
	            }
	                
	            
	            if(p != null)
	            {
	                String propOrg = p.getSysOrg();
	                if(userOrg == null || userOrg.equals(propOrg))
	                {
	                    HibernateUtil.getDAOFactory().getPropertiesDAO().delete(p);
	                }
	                else
	                {
	                    throw new Exception("User '" + user.getUUserName() + "' has no rights to delete property '" + p.getUName() + "'");
	                }
	            }

        	});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw new Exception(e); 
        }
    } // deleteProperty
    
    /*
     * This API should be called from update/upgrade script ONLY. 
     */
    public static void updateTaskWikiDefaultRoles(String username)
    {
        if (StringUtils.isBlank(username ))
        {
            username = "admin";
        }
        QueryDTO query = new QueryDTO();
        query.setModelName("Properties");//this("auto", value, field, comparison, false);
        query.addFilterItem(new QueryFilter("UName", QueryFilter.ENDS_WITH, ".all"));
        List<PropertiesVO> result = null;
        
        try
        {
            result = getProperties(query, username);
        }
        catch(Exception e)
        {
            Log.log.error(e);
        }
        
        if (result != null && result.size() > 0)
        {
            for (PropertiesVO propertyVO : result)
            {
                String value = propertyVO.getUValue();
                if (!value.contains("security_admin"))
                {
                    value += ",security_admin,security_designer";
                    propertyVO.setUValue(value);
                    /*
                     * This API will get called from update/upgrade operation. It's needed to handle the exception right here.
                     */
                    try
                    {
                        PropertiesUtil.saveProperties(propertyVO, username);
                    }
                    catch(Exception e)
                    {
                        Log.log.error("Could not update default roles for task, wiki and custom table", e);
                    }
                }
            }
        }
    }
    
    /**
     * API to populate Case Type from existing system property, app.security.types,
     * as default Case Types.
     * 
     * To Be Called From Update/Upgrade Script Only!
     */
    public static void populateCaseTypeFromProperty()
    {
        QueryDTO query = new QueryDTO();
        query.setModelName("Properties");//this("auto", value, field, comparison, false);
        query.addFilterItem(new QueryFilter("UName", QueryFilter.EQUALS, "app.security.types"));
        List<PropertiesVO> result = null;
        
        try
        {
            result = getProperties(query, "admin");
        }
        catch(Exception e)
        {
            Log.log.error(e);
        }
        
        if (result.size() > 0)
        {
            for (PropertiesVO propertyVO : result)
            {
                if (propertyVO != null && StringUtils.isNotBlank(propertyVO.getUValue()))
                {
                    List<String> caseTypes = new ArrayList<>();
                    caseTypes.addAll(Arrays.asList(propertyVO.getUValue().split(",")));
                    
                    ListIterator<String> caseTypeIterator = caseTypes.listIterator();
                    while (caseTypeIterator.hasNext())
                    {
                        String caseType = caseTypeIterator.next();
                        if (caseTypeExist(caseType))
                        {
                            caseTypeIterator.remove();
                        }
                    }
                    
                    if (!caseTypes.isEmpty())
                    {
                        List<ResolveCaseConfigVO> configVOList = new ArrayList<>();
                        for (String caseType : caseTypes)
                        {
                            ResolveCaseConfigVO configVO = new ResolveCaseConfigVO();
                            configVO.setConfigType("CaseType");
                            configVO.setConfigValue(caseType);
                            configVO.setActive(true);
                            configVOList.add(configVO);
                        }
                        
                        try
                        {
                            CaseConfigUtil.save(configVOList, Optional.empty(), Optional.empty(), "admin");
                        }
                        catch (Exception e)
                        {
                            Log.log.error("Error while populating default case types from system property.", e);
                        }
                    }
                }    
            }
        }
    }
    
    @SuppressWarnings("rawtypes")
    private static boolean caseTypeExist(String caseTypeValue)
    {
        boolean result = false;
        
        String hql = "from ResolveCaseConfig where LOWER(configType) = 'casetype' and LOWER(configValue) = :caseValue";
        Map<String, Object> queryParams = new HashMap<String, Object>();
        queryParams.put("caseValue", caseTypeValue.toLowerCase());
        
        try
        {
            List list = GeneralHibernateUtil.executeHQLSelect(hql, queryParams);
            if(list != null && list.size() > 0)
            {
                result = true;
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error getting case configuration for case type value:" + caseTypeValue , e);
        }
        
        return result;
    }
    
    private static Properties saveProperties(Properties model, String username)
    {
        Properties savedProperties = null;
        
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        // persist it
        try
        {
            model.setChecksum(Objects.hash(model.getUName(), model.getUValue()));
            
          HibernateProxy.setCurrentUser(username);
        	savedProperties = (Properties) HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                return HibernateUtil.getDAOFactory().getPropertiesDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                return HibernateUtil.getDAOFactory().getPropertiesDAO().persist(model);
	            }

            });

            HibernateUtil.clearDBCacheRegion(DBCacheRegionConstants.PROPERTIES_CACHE_REGION, true, model.getUName());
        }
        
        
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return /*model*/savedProperties;
    }
    
    public static Integer getSysPropertyChecksum(String name) {
        Integer checksum = null;
        
        PropertiesVO sysPropertyVO = findPropertyByName(name);
        if (sysPropertyVO != null) {
            checksum = sysPropertyVO.getChecksum();
        }
        
        return checksum;
    }
}
