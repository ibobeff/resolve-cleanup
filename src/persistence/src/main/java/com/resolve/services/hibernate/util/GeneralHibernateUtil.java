/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.util;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.CacheMode;
import org.hibernate.mapping.Column;
import org.hibernate.proxy.map.MapProxy;
import org.hibernate.query.Query;
import org.joda.time.DateTime;
import com.resolve.esb.ESB;
import com.resolve.persistence.model.AuditLog;
import com.resolve.persistence.model.BaseModel;
import com.resolve.persistence.model.ModelToVO;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.CacheUtil;
import com.resolve.persistence.util.CustomTableMappingUtil;
import com.resolve.persistence.util.DBCacheRegionConstants;
import com.resolve.persistence.util.EntityUtil;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.ConfigCAS;
import com.resolve.rsbase.ConfigSQL;
import com.resolve.services.hibernate.menu.MenuCache;
import com.resolve.services.interfaces.VO;
import com.resolve.services.util.AuditUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryParameter;
import com.resolve.services.vo.QuerySort;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.thread.TaskExecutor;
import com.resolve.util.BeanUtil;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.SysId;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.util.constants.HibernateConstantsEnum;

public class GeneralHibernateUtil
{
    final static int MAX_SOURCE_LENGTH = 333;
    final static int MAX_MESSAGE_LENGTH = 4000;

    public static Clob getClobFromString(String value)
    {
        return HibernateUtil.getClobFromString(value);
    }

    public static String getStringFromClob(Object obj)
    {
        return HibernateUtil.getStringFromClob(obj);
    }

    public static byte[] getBytesFromBlob(Blob blob)
    {
        return HibernateUtil.getBytesFromBlob(blob);
    }

    public static Blob getBlobFromBytes(byte[] content)
    {
        return HibernateUtil.getBlobFromBytes(content);
    }

    public static Integer getNextSequence(String sequenceName)
    {
        return HibernateUtil.getNextSequence(Constants.PERSISTENCE_SEQ_INIT);
    }

    public static String table2Class(String tableName)
    {
        String result = null;

        if (StringUtils.isNotBlank(tableName))
        {
            result = HibernateUtil.table2Class(tableName);
        }

        return result;
    }

    public static void log(String source, String message)
    {
        try {
			HibernateProxy.execute(() -> {
				
				String sourceFinal = source;
				String messageFinal = message;
				
		        if (sourceFinal.length() > MAX_SOURCE_LENGTH)
		        {
		        	sourceFinal = sourceFinal.substring(0, MAX_SOURCE_LENGTH);
		        }
		        if (messageFinal.length() > MAX_MESSAGE_LENGTH)
		        {
		        	messageFinal = messageFinal.substring(0, MAX_MESSAGE_LENGTH);
		        }

		        AuditLog auditLog = new AuditLog();
		        auditLog.setUUserName(HibernateUtil.getCurrentUser().toString());
		        auditLog.setUSource(sourceFinal);
		        auditLog.setUMessage(messageFinal);

		        // save it
		        AuditUtils.saveAuditLog(auditLog, null);
			    
			});
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}
        
    } // log

    @SuppressWarnings("rawtypes")
    public static void executeHQLUpdate(String hql, String username) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
            	Query query = HibernateUtil.createQuery(hql);
            	query.executeUpdate();

            });
        }
        catch (Exception e)
        {
            Log.log.error("Error in import executing SQL: " + hql, e);
            throw e;
        }
    }
    
    @SuppressWarnings("rawtypes")
    public static void executeHQLUpdate(String hql, Map<String, Object> updateParams, String username) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            Query query = HibernateUtil.createQuery(hql);
	            
	            if (updateParams != null && !updateParams.isEmpty())
	            {
	                for (String paramName : updateParams.keySet())
	                {
	                    if (updateParams.get(paramName) instanceof Collection<?>)
	                    {
	                        query.setParameterList(paramName, new ArrayList<Object>((Collection<?>)updateParams.get(paramName)));
	                    }
	                    else
	                    {
	                        query.setParameter(paramName, updateParams.get(paramName));
	                    }
	                }
	            }
	            
	            query.executeUpdate();

        	});
        }
        catch (Exception e)
        {
            Log.log.error("Error in import executing SQL: " + hql, e);
            throw e;
        }
    }

    @Deprecated
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<? extends Object> executeHQLSelect(String hql) throws Exception
    {
        List<? extends Object> result = null;

        try
        {          
			result = (List<? extends Object>) HibernateProxy.execute(() -> {
				Query query = HibernateUtil.createQuery(hql);
				return query.list();
			});
        }
        catch (Exception e)
        {
            Log.log.error("Error in import executing SQL: " + hql, e);
            throw e;
        }

        return result;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<? extends Object> executeHQLSelect(String hql, int start, int limit) throws Exception
    {
        List<? extends Object> result = null;

		try {
        HibernateProxy.setCurrentUser("admin");
			result = (List<? extends Object>) HibernateProxy.execute(() -> {
				Query query = HibernateUtil.createQuery(hql);
				// pagination
				if (limit > 0) {
					query.setFirstResult(start);
					query.setMaxResults(limit);
				}

				return query.list();
			});
		} catch (Exception e) {
			Log.log.error("Error in executing SQL: " + hql, e);
			throw e;
		}

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<VO> executeSelectHQL(QueryDTO queryDTO, int start, int limit, boolean forUI) throws Exception
    {
        List<VO> result = new ArrayList<VO>();

        List<? extends Object> models = executeHQLSelectModel(queryDTO, start, limit, forUI);
        if (models != null)
        {
            String modelName = queryDTO.getModelName();
            String selectedColumns = queryDTO.getSelectColumns();

            if (StringUtils.isNotBlank(selectedColumns))
            {
                String[] columns = selectedColumns.split(",");

                //this will be the array of objects
                for (Object o : models)
                {
                    Object[] record = (Object[]) o;
                    Object modelInstance = BeanUtil.getInstance("com.resolve.persistence.model." + modelName);

                    //set the attributes in the object
                    for (int x = 0; x < columns.length; x++)
                    {
                        String column = columns[x].trim();
                        Object value = record[x];

                        PropertyUtils.setProperty(modelInstance, column, value);
                    }

                    VO vo = ((ModelToVO<VO>) modelInstance).doGetVO();
                    result.add(vo);
                }//end of for loop
            }
            else
            {
                for (Object modelInstance : models)
                {
                    result.add(((ModelToVO<VO>) modelInstance).doGetVO());
                }
            }
        }

        return result;
    }

    /**
     * This api returns a generic version based on the HQL.
     *
     * So if the qry is like
     *          from Properties where ...
     *  it will return the list of Properties objects
     *
     *   and if the qry is like
     *          select sys_id, UName, UType, UDescription from Properties where ...
     *    it will return list of Object[]
     *
     * So you will have to type cast it manually and manipulate it to get the right data
     *
     *
     * @param queryDTO
     * @param start
     * @param limit
     * @param forUI
     * @return
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static List<? extends Object> executeHQLSelectModel(QueryDTO queryDTO, int start, int limit, boolean forUI) throws Exception
    {
        List result = null;

        //        List<Object> countList;

        if (StringUtils.isEmpty(queryDTO.getHql()) && StringUtils.isEmpty(queryDTO.getModelName()))
        {
            throw new Exception("Modelname is mandatory to execute this generic query.");
        }

        try
        {
          HibernateProxy.setCurrentUser("admin");
            result = (List) HibernateProxy.executeNoCache(() -> {
            	String hql = "";//queryDTO.getSelectHQL();

	            //########## READ THIS #############/
	            //Make sure this condition is never removed from this method otherwise there will
	            //be catastrophic disaster at runtime. BD
	            if (StringUtils.isNotEmpty(queryDTO.getHql()))
	            {
	                //this case is when we have to create complex query using joins to more than 1 table
	                hql = queryDTO.getHql();
	            }
	            else
	            {
	                if (StringUtils.isEmpty(queryDTO.getModelName()))
	                {
	                    String tempTableName = queryDTO.getTableName();
	                    String modelName = HibernateUtil.table2Class(tempTableName);
	                    queryDTO.setModelName(modelName);
	                }
	
	                hql = queryDTO.getSelectHQL();
	            }
	
	            Query query = HibernateUtil.createQuery(hql);
	
	            Log.log.trace("HQL to be Executed:" + hql);
	            //KEPT IT HERE - IN Case there is a need to find out the type of field using Introspection
	            //            String modelName = queryDTO.getModelName();
	            //            Class clazz = Class.forName("com.resolve.persistence.model." + modelName);
	            //            for (Field field : clazz.getDeclaredFields()) {
	            //                System.out.print("Field: " + field.getName() + " - ");
	            //                java.lang.reflect.Type type = field.getGenericType();
	            //                if (!(type instanceof ParameterizedType)) {
	            //
	            //                    System.out.println("Type: " + field.getType());
	            //                }
	            //            }
	
	            // Add in all the filtering parameters
	            for (QueryParameter param : queryDTO.getWhereParameters())
	            {
	                if (hql.contains(param.getName()))
	                {
	                    query.setParameter(param.getName(), param.getValue());
	                }
	            }
	            
	            // Add in all sort parameters
	            for (QuerySort qsort : queryDTO.getParsedSorts())
	            {
	                if (hql.contains(QuerySort.SORT_PROP_NAME_PREFIX + qsort.getProperty().replaceAll("\\.", "_")))
	                {
	                    query.setParameter(QuerySort.SORT_PROP_NAME_PREFIX + qsort.getProperty().replaceAll("\\.", "_"),
	                                       qsort.getProperty());
	                }
	            }
	
	            if (limit > -1)
	            {
	                query.setFirstResult(start);
	                query.setMaxResults(limit);
	            }
	
	            return query.list();

            });
        }
        catch (Throwable t)
        {
//            Log.log.error("Error in executing HQL for VO: " + hql, t);
//            throw new Exception(t);
          HibernateUtil.rethrowNestedTransaction(t);
        }

        //for UI, massage the data
        if (result != null && forUI && !(result instanceof List) )
        {
            List newResult = new ArrayList();
            for (Object o : result)
            {
                if (o instanceof Object[])
                {
                    Object[] oldArray = (Object[]) o;
                    Object[] newArray = new Object[oldArray.length];
                    for (int pos = 0; pos < oldArray.length; pos++)
                    {
                        Object oldValue = oldArray[pos];
                        Object newValue = getMassagedData(oldValue, forUI);
                        newArray[pos] = newValue;
                    }

                    newResult.add(newArray);
                }
                else if (o instanceof BaseModel)
                {
                    newResult.add(o);
                    //                    ((BaseModel) o).massageDataForUI();
                    //                    ((BaseModel) o).doGetVO();
                }
            }
            result = newResult;
        }

        return result;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<Map<String, Object>> executeHQLSelect(QueryDTO queryDTO, int start, int limit, boolean forUI) throws Exception
    {
        List<Map<String, Object>> resultData = new ArrayList<Map<String, Object>>();
        String sqlResult = "";
        try
        {

          HibernateProxy.setCurrentUser("admin");
            sqlResult = (String) HibernateProxy.executeNoCache(() -> {
	            if (StringUtils.isEmpty(queryDTO.getModelName()))
	            {
	                String tempTableName = queryDTO.getTableName();
	                String modelName = HibernateUtil.table2Class(tempTableName);
	                queryDTO.setModelName(modelName);
	                
	            }
	            String sql = queryDTO.getSelectHQL();
	            Query query = HibernateUtil.createQuery(sql);
	
	            // Add in all the filtering parameters
	            for (QueryParameter param : queryDTO.getWhereParameters())
	            {
	                if (sql.contains(param.getName())) query.setParameter(param.getName(), param.getValue());
	            }
	
	            // Add in all sort parameters
	            for (QuerySort qsort : queryDTO.getParsedSorts())
	            {
	                if (sql.contains(QuerySort.SORT_PROP_NAME_PREFIX + qsort.getProperty().replaceAll("\\.", "_")))
	                {
	                    query.setParameter(QuerySort.SORT_PROP_NAME_PREFIX + qsort.getProperty().replaceAll("\\.", "_"),
	                                       qsort.getProperty());
	                }
	            }
	            
	            query.setFirstResult(start);
	            if (limit > -1) query.setMaxResults(limit);
	            List<Object> rows = query.list();
	
	            for (Object row : rows)
	            {
	                Map<String, Object> hash;
	                if (row instanceof Map == false)
	                {
	                    hash = new HashMap<String, Object>();
	                    Class c = row.getClass();
	                    for (Field field : getInheritedPrivateFields(c))
	                    {
	                        field.setAccessible(true);
	                        String name = field.getName();
	                        Object r = field.get(row);
	                        boolean rIsLazy = false;
	                        if (!field.getType().isPrimitive())
	                        {
	                            if (r != null)
	                            {
	                                //check if the sub object is lazy
	                                for (Field subField : getInheritedPrivateFields(field.getType()))
	                                {
	                                    subField.setAccessible(true);
	                                    if (subField.get(r) != null && subField.get(r) instanceof Collection) rIsLazy = true;
	                                }
	                            }
	                        }
	                        if (!rIsLazy)
	                        {
	                            try
	                            {
	                                String columnName = ((Column) HibernateUtil.getClassMapping(c.getName()).getProperty(name).getColumnIterator().next()).getName();
	                                if (columnName != null)
	                                {
	                                    hash.put(columnName, r);
	                                    if ("sys_id".equals(columnName))
	                                    {
	                                        hash.put("id", r);
	                                    }
	                                }
	                            }
	                            catch (Exception e)
	                            {
	                                // There are exceptions thrown where the normal
	                                // tables have attributes that this class doesn't
	                                // have
	                            }
	                        }
	                    }
	                }
	                else
	                {
	                    hash = (Map<String, Object>) row;
	                }
	
	                resultData.add(getRecordData(hash, forUI));
	            }

	            return sql;
            });
        }
        catch (Exception e)
        {
            Log.log.error("Error in executing HQL: " + sqlResult, e);
            throw e;
        }

        return resultData;
    }

    private static List<Field> getInheritedPrivateFields(Class<?> type)
    {
        List<Field> result = new ArrayList<Field>();

        Class<?> i = type;
        while (i != null && i != Object.class)
        {
            for (Field field : i.getDeclaredFields())
            {
                if (!field.isSynthetic())
                {
                    result.add(field);
                }
            }
            i = i.getSuperclass();
        }

        return result;
    }

    public static boolean electMaster(int electionInterval, String guid)
    {
        ResultSet rs = null;
        boolean result = false;
        try
        {
        	result = (boolean)HibernateProxy.execute(() -> { 
        		 boolean isMaster = false;
                // get CRON_MASTER entry
                // int electionInterval = ((ConfigRegistration)Main.main.configRegistration).getElection();
                int interval = electionInterval * 3;
                // String guid = MainBase.main.configId.guid;
                String sql = "UPDATE resolve_event SET u_value = 'CRON:" + guid + "', sys_updated_on=CURRENT_TIMESTAMP WHERE u_value = 'CRON:" + guid + "' OR (u_value LIKE 'CRON:%' AND sys_updated_on < CURRENT_TIMESTAMP - INTERVAL '" + interval + "' SECOND)";
                Log.log.debug("Check Cron Master");
                Log.log.trace("CronElect SQL: " + sql);
                
                Query query = HibernateUtil.createSQLQuery(sql);
                int updateCount = query.executeUpdate();
                


                // elect if not defined or older than 30secs
                if (updateCount > 0)
                {
                    Log.log.info("CronElect: MASTER");
                    isMaster = true;
                }
                else
                {   
                    sql = "SELECT * from resolve_event where u_value like 'CRON:%' for update";
                    Log.log.trace("CronElect Check Insert SQL: " + sql);
                    
                    query = HibernateUtil.createSQLQuery(sql);
                    boolean isRsEmpty = query.list().isEmpty();
                      
                    
                    if (isRsEmpty)
                    {
                        sql = "Insert into resolve_event (sys_id, sys_updated_on, sys_updated_by, sys_created_on, sys_created_by, u_value, sys_mod_count) values ('" + UUID.randomUUID().toString().replaceAll("-", "") + "', CURRENT_TIMESTAMP, 'system', CURRENT_TIMESTAMP, 'system', 'CRON:" + guid + "', 0)";
                        Log.log.trace("CronElect Insert SQL: " + sql);
                        query = HibernateUtil.createSQLQuery(sql);
                        query.executeUpdate();
                        

                        Log.log.info("CronElect: MASTER");
                        isMaster = true;
                    }
                    else
                    {
                        sql = "Update resolve_event set sys_updated_by=sys_updated_by where u_value like 'CRON:%'";
                        Log.log.trace("CronElect Update Release Lock SQL: " + sql);
                        query = HibernateUtil.createSQLQuery(sql);
                        query.executeUpdate();

                        Log.log.info("CronElect: NOT MASTER");
                        isMaster = false;
                    }
                }
                return isMaster;
        	});

        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                }
                catch (SQLException e2)
                {
                    Log.log.error(e2.getMessage(), e2);
                }
            }

        }

        return result;
    } // electMaster

    public static void clearDBCacheRegion(DBCacheRegionConstants region, boolean sendMsg)
    {
        HibernateUtil.clearDBCacheRegion(region, sendMsg);
    }

    public static Object getCachedWikiDocumentForRSControl(DBCacheRegionConstants region, String accessId)
    {
        Object o = HibernateUtil.getCachedDBObject(region, accessId);
        Object result = null;

        if (o instanceof WikiDocument)
        {
            result = ((WikiDocument) o).doGetVO();
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<String> getAllResolveBusinessRulesNames(String whereClause)
    {
        List<String> result = new ArrayList<String>();
        List<? extends Object> list = null;

        StringBuffer sql = new StringBuffer("select UName from ResolveBusinessRule ");
        if (StringUtils.isNotBlank(whereClause))
        {
            sql.append(whereClause);
        }
        try
        {
            list = executeHQLSelect(sql.toString(), new HashMap<String, Object>());
            if (list != null && list.size() > 0)
            {
                result.addAll((List<String>) list);
                Collections.sort(result);
            }
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<String> getAllSystemScriptsName(String whereClause)
    {
        List<String> result = new ArrayList<String>();
        List<? extends Object> list = null;

        StringBuffer sql = new StringBuffer("select UName from ResolveSysScript ");
        if (StringUtils.isNotBlank(whereClause))
        {
            sql.append(whereClause);
        }

        try
        {
            list = executeHQLSelect(sql.toString(), new HashMap<String, Object>());
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }

        if (list != null && list.size() > 0)
        {
            result.addAll((List<String>) list);
            Collections.sort(result);
        }

        return result;
    }

    public static void setEsb(ESB esb, String id)
    {
        HibernateUtil.setEsb(esb, id);
    }

    public static void shutdownHibernate()
    {
        HibernateUtil.shutdown();
    }

    public static void initPersistence(ConfigSQL configSQL, ConfigCAS configCAS, String customMappingFileLocation, String hibernateCfgLocation)
    {
    	initPersistence(configSQL, configCAS, customMappingFileLocation, hibernateCfgLocation, true);
    }
    
    public static void initPersistence(ConfigSQL configSQL, ConfigCAS configCAS, String customMappingFileLocation, 
    								   String hibernateCfgLocation, boolean updateSchema) {
        if (StringUtils.isNotBlank(customMappingFileLocation)) {
            CustomTableMappingUtil.setCustomMappingFileLocation(customMappingFileLocation);
            CustomTableMappingUtil.syncDBCustomTableSchema();
        }
                
        HibernateUtil.configSQL = configSQL;

        CacheUtil.voidCacheManager();

        if (StringUtils.isNotEmpty(hibernateCfgLocation))
        {
            HibernateUtil.init(hibernateCfgLocation, updateSchema, false);
            HibernateUtil.registerDBPoolStatusProvider();
        }

        CacheUtil.initLocalCache();
        
        CustomTableMappingUtil.isHibernateInitialized = true;
        
        MenuCache.initMenuCache();        
    } // initPersistence

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static Map<String, Object> findById(String entityName, String sysId)
    {
        Map<String, Object> record = new HashMap<String, Object>(); 
        try
        {
        	record = (Map<String, Object>) HibernateProxy.execute(() -> {

            if (HibernateUtil.doesModelExist(entityName))
            {
            	ModelToVO vo = (ModelToVO)HibernateUtil.findById(entityName, sysId);
            	if (vo != null)
            	{
            	    return ((VO)vo.doGetVO()).covertVOToMap();
            	} else {
            		return new HashMap<String, Object>();
            	}
            }
            else
            {
            	return  EntityUtil.findById(entityName, sysId);
            }

            });

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return record;
    }
    
    /**
     * this api if for Model and does not convert to VO as findById()
     * 
     * @param entityName
     * @param sysId
     * @return
     */
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static Map<String, Object> findByIdForModel(String entityName, String sysId)
    {
        Map<String, Object> record = new HashMap<String, Object>();
        try
        {
			record = (Map<String, Object>) HibernateProxy.execute(() -> {

				if (HibernateUtil.doesModelExist(entityName)) {
					BaseModel model = (BaseModel) HibernateUtil.findById(entityName, sysId);
					if (model != null) {
						return model.covertVOToMap();
					} else {
						return new HashMap<String, Object>();
					}
				} else {
					return EntityUtil.findById(entityName, sysId);
				}

			});

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return record;
    }

    @SuppressWarnings("unchecked")
	public static Map<String, Object> findFirstCustomTableRecord(String tableName, Map<String, Object> example)
    {
        Map<String, Object> record = new HashMap<String, Object>();
        try
        {
            record = (Map<String, Object>) HibernateProxy.execute(() -> {
            	return EntityUtil.findFirst(tableName, example);
            });
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return record;
    }

    @SuppressWarnings("unchecked")
	public static List<Map<String, Object>> getAllData(String tableName, int limit, int offset)
    {
        List<Map<String, Object>> allData = new ArrayList<Map<String, Object>>();
        try
        {
            // ** USE THIS FOR PAGING
            allData = (List<Map<String, Object>>) HibernateProxy.execute(() -> {
            	return EntityUtil.findAll(tableName, limit, offset);
            });

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return allData;
    }

    @SuppressWarnings("unchecked")
	public static List<Map<String, Object>> getAllData(String tableName)
    {
        List<Map<String, Object>> allData = new ArrayList<Map<String, Object>>();
        try
        {
            allData = (List<Map<String, Object>>) HibernateProxy.execute(() -> {
            	return EntityUtil.findAll(tableName);
            });

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return allData;
    }

    public static Connection getConnection() throws Exception
    {
        return HibernateUtil.getConnection();
    }

    public static Set<String> getListOfResolveTables()
    {
        return HibernateUtil.getListOfResolveTables();
    }

    public static void rethrowNestedTransaction(Throwable t)
    {
        HibernateUtil.rethrowNestedTransaction(t);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static long getTotalRecordsIn(String modelName, String whereClause, List<Object> paramValueList)
    {
        String sql = "SELECT count(*) FROM " + modelName;
        if (StringUtils.isNotBlank(whereClause))
        {
        	sql += whereClause;
        }
        long count = 0;
        String sqlFinal = sql;

        try
        {
        	count = (long) HibernateProxy.execute(() -> {
        		Query q = HibernateUtil.createQuery(sqlFinal);
                if (StringUtils.isNotBlank(whereClause))
                {
                	for (int i = 0; i < paramValueList.size(); i++)
                	{
                		q.setParameter(i, paramValueList.get(i));
                	}
                }
                
                List<Long> recs = q.list();

                if (recs != null && recs.size() > 0)
                {
                    return  ((Long) recs.get(0)).longValue();
                }
                
                return 0;
        	});

            

        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return count;

    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static int getTotalHqlCount(QueryDTO queryDTO) throws Exception
    {
        int count = 0;
        try
        {
           
          HibernateProxy.setCurrentUser("admin");
			List<Object> rows = (List<Object>) HibernateProxy.executeNoCache(() -> {
				String countQuery = "";
				String modelName = queryDTO.getModelName();
				if (StringUtils.isBlank(modelName)) {
					String tempTableName = queryDTO.getTableName();
					if (StringUtils.isNotBlank(tempTableName)) {
						modelName = HibernateUtil.table2Class(tempTableName);
						queryDTO.setModelName(modelName);
					}
				}

				if (StringUtils.isNotEmpty(queryDTO.getHql())) {
					// this case is when we have to create complex query using joins to more than 1
					// table
					countQuery = queryDTO.getCountQueryUsingHQL();
				} else {
					countQuery = queryDTO.getSelectCountHQL();
				}

				Query query = HibernateUtil.createQuery(countQuery);

				// Add in all the filtering parameters
				for (QueryParameter param : queryDTO.getWhereParameters()) {
					if (countQuery.contains(param.getName()))
						query.setParameter(param.getName(), param.getValue());
				}

				return query.list();
			});
            
            if (rows.size() > 0)
            {
                if (queryDTO.getHqlDistinct())
                    count = rows.size(); // result here will be a list indivisual count of distinct records. So, grab the list size.
                else
                count = Integer.valueOf(rows.get(0).toString());
            }

        }
        catch (Exception e)
        {
//            Log.log.error("Error in import executing HQL: " + countQuery, e);
//            HibernateUtil.rollbackTransaction();
//            HibernateUtil.rethrowNestedTransaction(e);
//            throw e;
          HibernateUtil.rethrowNestedTransaction(e);
        }
        return count;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static int getTotalHqlCount(Query query) throws Exception
    {
        int count = 0;
        String countQuery = "";
        try
        {

          HibernateProxy.setCurrentUser("admin");
        	count = (int) HibernateProxy.execute(() -> {
                HibernateUtil.getCurrentSession().setCacheMode(CacheMode.IGNORE);

                List<Object> rows = query.list();
                if (rows.size() > 0)
                {
                     return Integer.valueOf(rows.get(0).toString());
                }

                return 0;
        	});
        }
        catch (Exception e)
        {
            Log.log.error("Error in import executing HQL: " + countQuery, e);
            throw e;
        }
        return count;
    }

    @SuppressWarnings({ "rawtypes" })
    public static int getTotalHqlCount(String countQuery) throws Exception
    {
        int count = 0;
        try
        {

          HibernateProxy.setCurrentUser("admin");
        	count = (int) HibernateProxy.execute(() -> {
                HibernateUtil.getCurrentSession().setCacheMode(CacheMode.IGNORE);

                Query query = HibernateUtil.createQuery(countQuery);
                
                Long recCount = (Long)query.uniqueResult();
                
                if (recCount.longValue() > Integer.MAX_VALUE)
                {
                    throw new Exception("Returned value " + recCount + " is more than largest int value of " + Integer.MAX_VALUE + 
                                        ". Please use getTotalHqlCountLong() method which returns long.");
                }
                    
                return  ((Long)query.uniqueResult()).intValue();
                
                /*
                List<Object> rows = query.list();
                if (rows.size() > 0)
                {
                    count = Integer.valueOf(rows.get(0).toString());
                }
                */
                
        	});
        }
        catch (Exception e)
        {
            Log.log.error("Error in getTotalHqlCount using  count query HQL: " + countQuery, e);
            throw e;
        }
        return count;
    }

    @SuppressWarnings({ "rawtypes" })
    public static long getTotalHqlCountLong(String countQuery) throws Exception
    {
        long count = 0l;
        try
        {
          HibernateProxy.setCurrentUser("admin");
        	count = (long) HibernateProxy.executeNoCache(() -> {
        		Query query = HibernateUtil.createQuery(countQuery);
                
                return ((Long)query.uniqueResult()).longValue();
            });
                        
        }
        catch (Exception e)
        {
            Log.log.error("Error in getTotalHqlCount using  count query HQL: " + countQuery, e);
            throw e;
        }
        return count;
    }
    
    @SuppressWarnings("rawtypes")
    public static Map<String, String> getHQLMetaData(String hql) throws Exception
    {
        Map<String, String> metaData = new HashMap<String, String>();

        // TODO: this is still not working...emailed it to Yu for a solution

        try
        {
          HibernateProxy.setCurrentUser("admin");
        	HibernateProxy.execute(() -> {
                Query query = HibernateUtil.getCurrentSession().createQuery(HibernateUtil.getValidQuery(hql));
                List rows = query.list();
                rows.size();
                Set<org.hibernate.query.QueryParameter<?>> qryParams = query.getParameterMetadata().collectAllParameters();
                
//                String[] cols = query.getParameterMetadata().getNamedParameterNames();
//                Type[] types = query.getReturnTypes();

//                for (int count = 0; count < cols.length; count++)
                for (org.hibernate.query.QueryParameter qryParam : qryParams)
                {
//                    String colname = cols[count];
//                    String type = types[count].getName();
                    String colname = qryParam.getName();
                    String type = qryParam.getType().getName();
                    
                    System.out.println(colname + " : " + type);

                    metaData.put(colname, type);
                }

        	});
        }
        catch (Exception e)
        {
            Log.log.error("Error in executing HQL: " + hql, e);
            throw e;
        }

        return metaData;
    }

    public static Collection<String> getAllColumnsByTable(String tableModelName)
    {
        List<String> result = new ArrayList<String>();

        if (StringUtils.isNotBlank(tableModelName))
        {
            result.addAll(HibernateUtil.getAllColumnsByTable(tableModelName));
        }

        Collections.sort(result);

        return result;
    }

    public static String getColumnTypeString(String tableModelName, String colName)
    {
        String result = "";

        if (StringUtils.isNotBlank(tableModelName) & StringUtils.isNotBlank(colName))
        {
            result = HibernateUtil.getColumnTypeString(tableModelName, colName);
        }

        return result;
    }

    public static void purgeDBTable(String modelName, String username) throws Exception
    {
        if (StringUtils.isNotBlank(modelName))
        {
            try
            {
                String hql = "delete from " + modelName;
                executeHQLUpdate(hql, username);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }

    public static void deleteQuery(String modelClassName, String paramName, String paramValue, String username) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
        		HibernateUtil.deleteQuery(modelClassName, paramName, paramValue);
        	});                

        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
    }

    public static void deleteAllQuery(String modelClassName, String paramName, String paramValue, String username) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
            	HibernateUtil.deleteAllQuery(modelClassName, paramName, paramValue);
            });                

        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
    }
    
    public static void deleteQuery(String modelClassName, String inParamName, Collection<String> inParamValues, String username) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
        		HibernateUtil.deleteQuery(modelClassName, inParamName, inParamValues);
        	});                

        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
    }
    
    public static Map<String, String> updateDBRow(String username, String modelClass, Map<String, Object> persistedRowData, boolean isCustomTable)
    {
        HashMap<String, String> result = new HashMap<String, String>();

        try
        {
            if (Log.log.isTraceEnabled())
            {
                Log.log.trace("Inserting/Updating record for table :" + modelClass);
                Log.log.trace("Row Data:" + persistedRowData);
            }

            String id = (String) persistedRowData.get(HibernateConstants.SYS_ID);
            if (!modelClass.equals("custom_table_parent_example") && isCustomTable)
            {
                // its a custom table - persist it
                // EntityUtil.persist(modelClass, persistedRowData);
                persistedRowData = insertOrUpdateCustomTable(username, modelClass, persistedRowData);

                // populate the DBRowData with sysId
                id = (String) persistedRowData.get(HibernateConstants.SYS_ID);
            }
            else
            {
                // TODO: for the resolve tables - refer to Old ExtFormSubmit.java from the repository

            }

            result.put(HibernateConstantsEnum.DB.getTagName(), "Record Updated successfully.");
            result.put("DB_SYS_ID", id);
        }
        catch (Throwable t)
        {
            Log.log.error("Error:" + t.getMessage(), t);
            result.put(HibernateConstants.ERROR_EXCEPTION_KEY, "DB Update Failed:" + t.getMessage());
        }

        return result;

    }// updateDBRow();

    @SuppressWarnings("unchecked")
	public static Map<String, Object> insertOrUpdateCustomTable(String username, String tableName, Map<String, Object> data) throws Exception
    {
        Map<String, Object> result = null;
        try
        {
          HibernateProxy.setCurrentUser(username);
        	result = (Map<String, Object>) HibernateProxy.execute(() -> {

        		return EntityUtil.insertOrUpdate(tableName, data);

        	});
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
            throw t;
        }

        return result;
    }// insertOrUpdateCustomTable

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Deprecated
    public static List<Map<String, Object>> queryCustomTable(String tableName, String whereClause)
    {
        List<Map<String, Object>> allData = new ArrayList<Map<String, Object>>();

        if (StringUtils.isNotBlank(tableName))
        {
            
            StringBuffer sql = new StringBuffer("FROM ").append(tableName);
            if (StringUtils.isNotBlank(whereClause))
            {
                sql.append(" WHERE ").append(whereClause);
            }
            try
            {
              HibernateProxy.setCurrentUser("admin");
                allData = (List<Map<String, Object>>) HibernateProxy.execute(() -> {
                	Query query = HibernateUtil.createQuery(sql.toString());
                    return query.list();
                });


            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }

        return allData;
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static List<Map<String, Object>> queryCustomTable(String tableName, String whereClause, Map<String, Object> whereClauseParams)
    {
        List<Map<String, Object>> allData = new ArrayList<Map<String, Object>>();

        if (StringUtils.isNotBlank(tableName))
        {
            
            StringBuilder sql = new StringBuilder("FROM ").append(tableName);
            if (StringUtils.isNotBlank(whereClause))
            {
                sql.append(" WHERE ").append(whereClause);
            }
            try
            {
              HibernateProxy.setCurrentUser("admin");
                allData = (List<Map<String, Object>>) HibernateProxy.execute(() -> {
                	Query query = HibernateUtil.createQuery(sql.toString());
                     
                     if (whereClauseParams != null && !whereClauseParams.isEmpty())
                     {
                         for (String columnName : whereClauseParams.keySet())
                         {
                             query.setParameter(columnName, whereClauseParams.get(columnName));
                         }
                     }
                     
                     return query.list();
                });

            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }

        return allData;
    }

    @SuppressWarnings("unchecked")
    public static List<Map<String, Object>> queryCustomTable(String tableName, String whereClause, int limit, int offset, boolean forUI) throws Exception
    {
        List<Map<String, Object>> allData = new ArrayList<Map<String, Object>>();

        if (StringUtils.isNotBlank(tableName))
        {
            StringBuffer sql = new StringBuffer("FROM ").append(tableName);
            if (StringUtils.isNotBlank(whereClause))
            {
                sql.append(" WHERE ").append(whereClause);
            }

            try
            {
                List<Map<String, Object>> result = (List<Map<String, Object>>) executeHQLSelect(sql.toString(), offset, limit);
                if (result != null)
                {
                    if (forUI)
                    {
                        for (Map<String, Object> record : result)
                        {
                            allData.add(getRecordData(record, forUI));
                        }
                    }
                    else
                    {
                        allData.addAll(result);
                    }
                }
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                throw new Exception(t);
            }
        }

        return allData;
    }

    public static void executeSystemExecutor(String className, String apiName, Map<String, Object> params)
    {
        if (StringUtils.isNotBlank(className) && StringUtils.isNotBlank(apiName))
        {
            try
            {
                Class<? extends Object> clazz = Class.forName(className);

                TaskExecutor.execute(clazz, apiName, params);
            }
            catch (Throwable e)
            {
                Log.log.error("Error in executing an api on a class", e);

            }
        }
    }

    public static String getTableName(String name)
    {
        return HibernateUtil.class2TableTrueCase(name);
    }

    // private apis

    private static Map<String, Object> getRecordData(Map<String, Object> record, boolean forUI) throws Exception
    {
        Map<String, Object> temp = new HashMap<String, Object>();

        for (String key : record.keySet())
        {
            Object rawdata = record.get(key);
            Object massagedData = rawdata;
            
            if(rawdata != null)
            {
                if (rawdata instanceof MapProxy)
                {
                    MapProxy map = (MapProxy) rawdata;
                    massagedData = map.containsKey("sys_id") ? map.get("sys_id") : "";
                }
                else
                {
                    massagedData = getMassagedData(record.get(key), forUI);
                }
            }
            
            temp.put(key, massagedData);
        }

        return temp;
    }

    public static VO findVOById(String modelName, String id) throws Exception
    {
        VO result = null;
        try
        {
            BaseModel<VO> model = findModelById(modelName, id);
            if (model != null)
            {
                result = model.doGetVO();
            }
        }
        catch (Throwable t)
        {
            Log.log.error("Error finding VO by sys id, model : " + modelName + ", sysId : " + id, t);
            throw new Exception(t);
        }

        return result;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static BaseModel<VO> findModelById(String modelName, String id) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser("admin");
            return (BaseModel<VO>) HibernateProxy.executeNoCache(() -> {
            	BaseModel<VO> result = null;

	            String hql = "select obj from " + modelName + " obj where obj.sys_id='" + id + "'";
	
	            Query query = HibernateUtil.createQuery(hql);
	
	            Log.log.trace("HQL to be Executed:" + hql);
	            List<? extends BaseModel<VO>> resultList = query.list();
	            if (resultList != null && resultList.size() > 0)
	            {
	                result = (BaseModel<VO>) resultList.get(0);
	            }
	            
	            return result;
            });

        }
        catch (Throwable t)
        {
            Log.log.error("Error finding model by sys id, model : " + modelName + ", sysId : " + id, t);
            throw new Exception(t);
        }
        
    }

    //returns set of sysIds for a model 
    @SuppressWarnings({ "unchecked", "rawtypes" })
    // Not safe in cases where whereClause is generated based on user inputs
    public static Set<String> getSysIdsFor(String modelName, String whereClause, String username)
    {
        Set<String> result = new HashSet<String>();
        
        if (StringUtils.isNotBlank(modelName))
        {
            try
            {

              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {
            		
            		 String sql = "SELECT sys_id FROM " + modelName;
                     if (StringUtils.isNotBlank(whereClause))
                     {
                         sql = sql + " where " + whereClause;
                     }
            		
                    Query q = HibernateUtil.createQuery(sql);
                    List<String> recs = q.list();

                    if (recs != null && recs.size() > 0)
                    {
                        result.addAll(recs);
                    }

            	});
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return result;
    }

    // returns set of sysIds for a model 
    public static Set<String> getSysIdsFor(String modelName, String inClauseFieldName, String username, Collection<String> inClauseParams)
    {
        Set<String> result = new HashSet<String>();
        
        if (StringUtils.isNotBlank(modelName))
        {
            StringBuilder hql = new StringBuilder("select ma.id from " + modelName + " ma ");
            
            if (StringUtils.isNotBlank(inClauseFieldName) && inClauseParams != null && !inClauseParams.isEmpty())
            {
                hql.append(" where ma.").append(inClauseFieldName).append(" in (:").append(inClauseFieldName.replaceAll("\\.", "_")).append(")");
            }
            
            try
            {
                Map<String, Object> queryParams = new HashMap<String, Object>();
                Map<String, Collection<Object>> queryInParams = new HashMap<String, Collection<Object>>();
                
                queryInParams.put(inClauseFieldName.replaceAll("\\.", "_"), new ArrayList<Object>(inClauseParams));
                
                List<? extends Object> recs = executeHQLSelectIn(hql.toString(), queryInParams, queryParams);
                
                if (recs != null && recs.size() > 0)
                {
                    for (Object rec : recs)
                    {
                        result.add((String)rec);
                    }
                }
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        return result;
    }
    
	public static Set<String> getSysIdsFor(String modelName, String inClauseFieldName, String username,
			Collection<String> inClauseParams, String notInClauseFieldName, Collection<String> notInClauseParams)
    {
        Set<String> result = new HashSet<String>();
        
        if (StringUtils.isNotBlank(modelName))
        {
            StringBuilder hql = new StringBuilder("select ma.id from " + modelName + " ma ");
            
            if (StringUtils.isNotBlank(inClauseFieldName))
            {
                hql.append(" where ma.").append(inClauseFieldName).append(" in (:").append(inClauseFieldName.replaceAll("\\.", "_")).append(")");
            }
            
            if (StringUtils.isNotBlank(notInClauseFieldName) && notInClauseParams != null && !notInClauseParams.isEmpty())
            {
                hql.append(" and ma.").append(notInClauseFieldName).append(" not in (:").append(notInClauseFieldName.replaceAll("\\.", "_")).append(")");
            }
            
            try
            {
                Map<String, Object> queryParams = new HashMap<String, Object>();
                Map<String, Collection<Object>> queryInParams = new HashMap<String, Collection<Object>>();
                
                queryInParams.put(inClauseFieldName.replaceAll("\\.", "_"), new ArrayList<Object>(inClauseParams));
                queryInParams.put(notInClauseFieldName.replaceAll("\\.", "_"), new ArrayList<Object>(notInClauseParams));
                
                List<? extends Object> recs = executeHQLSelectIn(hql.toString(), queryInParams, queryParams);
                
                if (recs != null && recs.size() > 0)
                {
                    for (Object rec : recs)
                    {
                        result.add((String)rec);
                    }
                }
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        return result;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static Set<String> getSysIdsFor(String modelName, String modelAlias, String parametrizedWhereClause, Map<String, Object> whereClauseParams, String username)
    {
        Set<String> result = new HashSet<String>();
        
        if (StringUtils.isNotBlank(modelName))
        {
            StringBuilder hql = new StringBuilder("select " + modelAlias + ".id from " + modelName + " " + modelAlias);
            
            if (StringUtils.isNotBlank(parametrizedWhereClause) && whereClauseParams != null && !whereClauseParams.isEmpty())
            {
                // sanitize :<parametrized field name> in where clause by replacing all . with _
                
                String sanitizedParametrizedWhereClause = parametrizedWhereClause;
                
                for (String whereClauseParamName : whereClauseParams.keySet())
                {
                    if (whereClauseParamName.contains("."))
                    {
                        sanitizedParametrizedWhereClause.replaceAll(":" + whereClauseParamName, ":" + whereClauseParamName.replaceAll("\\.", "_"));
                    }
                }
                
                hql.append(" where ").append(sanitizedParametrizedWhereClause);
            }
            
            try
            {

              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {
                    Query q = HibernateUtil.createQuery(hql.toString());
                    
                    if (StringUtils.isNotBlank(parametrizedWhereClause) && whereClauseParams != null && !whereClauseParams.isEmpty())
                    {
                        for (String whereClauseParamName : whereClauseParams.keySet())
                        {
                            if (whereClauseParams.get(whereClauseParamName) instanceof Collection<?>)
                            {
                                q.setParameterList(whereClauseParamName.replaceAll("\\.", "_"), 
                                                   new ArrayList<Object>((Collection<? extends Object>)whereClauseParams.get(whereClauseParamName)));
                            }
                            else
                            {
                                q.setParameter(whereClauseParamName.replaceAll("\\.", "_"), whereClauseParams.get(whereClauseParamName));
                            }
                        }
                    }
                    
                    List<? extends Object> recs = q.list();
                    
                    if (recs != null && !recs.isEmpty())
                    {
                        for (Object rec : recs)
                        {
                            result.add((String)rec);
                        }
                    }
                    
            	});
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return result;
    }
    
    public static Object findSharedJavaObject(String objectName)
    {
        return HibernateUtil.getJavaObjectOnly(objectName);
    }

    public static Object findSharedJavaObjectNonBlocking(String objectName)
    {
        return HibernateUtil.getJavaObjectOnlyNonBlocking(objectName);
    }
    
    public static Object saveSharedJavaObject(String objectName, Object obj)
    {
        return HibernateUtil.saveJavaObjectOnly(objectName, obj);
    }
    
    @SuppressWarnings("rawtypes")
    public static boolean modelExists(String modelName, String username)
    {
        long result = 0l;
        
        if (StringUtils.isNotBlank(modelName))
        {
            String sql = "SELECT count(sys_id) FROM " + modelName + " WHERE sys_id is not null";
            
            try
            {

              HibernateProxy.setCurrentUser(username);
            	result = (long) HibernateProxy.execute(() -> {
            		 Query q = HibernateUtil.createQuery(sql);
                     
                     return ((Long) q.uniqueResult()).longValue();

            	});
            	
               
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return result > 0l;
    }
    
    //returns set of sysIds for a model 
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static Set<String> getSysIdsForWithTimeout(String modelName, String whereClause, String username, int seconds)
    {
        Set<String> result = new HashSet<String>();
        
        if (StringUtils.isNotBlank(modelName))
        {
            String sql = "SELECT sys_id FROM " + modelName;
            if (StringUtils.isNotBlank(whereClause))
            {
                sql = sql + " where " + whereClause;
            }
            else
            {
                sql += " where sys_id is not null";
            }
            
            try
            {
                HibernateUtil.setTransactionTimeout(seconds);
                String sqlFinal = sql;
                
              HibernateProxy.setCurrentUser(username);
                List<String> recs = (List<String>) HibernateProxy.execute(() -> {
                	Query q = HibernateUtil.createQuery(sqlFinal);
                    return q.list();
                });    
                
                if (recs != null && recs.size() > 0)
                {
                    result.addAll(recs);
                }
            }
            catch (Throwable e)
            {
                Log.log.error("Failed to get SysIds for " + 
                              modelName + (StringUtils.isNotBlank(whereClause) ? " with filter " + whereClause + " and" : "") + 
                              " with timeout of " + seconds + " seconds due to " + e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return result;
    }
    
    public static boolean genericElectMaster(int electionInterval, String guid, String component)
    {
        SQLConnection sqlConn = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        boolean isMaster = false;

        try
        {
            sqlConn = SQL.getConnection();
            conn = sqlConn.getConnection();
            if (conn != null)
            {
                // get master entry
                int interval = electionInterval * 3;
                String sql = "UPDATE resolve_event SET u_value = '" + component + ":" + guid + "', sys_updated_on=CURRENT_TIMESTAMP WHERE u_value = '" + component + ":" + guid + "' OR (u_value LIKE '" + component + ":%' AND sys_updated_on < CURRENT_TIMESTAMP - INTERVAL '" + interval + "' SECOND)";
                Log.log.debug("Check Master for " + component);
                Log.log.trace("Elect update SQL: " + sql);
                stmt = conn.createStatement();
                int updateCount = stmt.executeUpdate(sql);
                stmt.close();

                // elect if not defined or older than interval
                if (updateCount > 0)
                {
                    Log.log.info("Election result for component " + component + ": MASTER");
                    isMaster = true;
                }
                else
                {
                    sql = "SELECT * from resolve_event where u_value like '" + component + ":%' for update";
                    Log.log.trace("Elect Check select SQL: " + sql);
                    stmt = conn.createStatement();
                    rs = stmt.executeQuery(sql);
                    boolean isRsEmpty = rs.next();
                    stmt.close();
                    
                    if (!isRsEmpty)
                    {
                        sql = "Insert into resolve_event (sys_id, sys_updated_on, sys_updated_by, sys_created_on, sys_created_by, u_value, sys_mod_count) values ('" + SysId.generate(conn) + "', CURRENT_TIMESTAMP, 'system', CURRENT_TIMESTAMP, 'system', '" + component + ":" + guid + "', 0)";
                        Log.log.trace("Elect Insert SQL: " + sql);
                        stmt = conn.createStatement();
                        stmt.executeUpdate(sql);
                        stmt.close();
                        Log.log.info("Election result for component " + component + ": MASTER");
                        isMaster = true;
                    }
                    else
                    {
                        sql = "Update resolve_event set sys_updated_by=sys_updated_by where u_value like '" + component + ":%'";
                        Log.log.trace("Elect Update Release Lock SQL: " + sql);
                        stmt = conn.createStatement();
                        stmt.executeUpdate(sql);
                        stmt.close();
                        Log.log.info("Election result for component " + component + ": NOT MASTER");
                        isMaster = false;
                    }
                }
            }
            else
            {
                isMaster = true;
                throw new Exception("Unable to get database connection. Setting Election for component " + component + ": MASTER");
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                }
                catch (SQLException e2)
                {
                    Log.log.error(e2.getMessage(), e2);
                }
            }
            if (stmt != null)
            {
                try
                {
                    stmt.close();
                }
                catch (SQLException e2)
                {
                    Log.log.error(e2.getMessage(), e2);
                }
            }
            if (sqlConn != null)
            {
                    SQL.close(sqlConn);
            }
        }

        return isMaster;
    } // genericElectMaster
    
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<? extends Object> executeHQLSelect(String hql, Map<String, Object> queryParams)
			throws Exception {
		List<? extends Object> result = null;

		try {        
			result = (List<? extends Object>) HibernateProxy.execute(() -> {

				Query query = HibernateUtil.createQuery(hql);

				if (queryParams != null) {
					for (String paramName : queryParams.keySet()) {
						query.setParameter(paramName, queryParams.get(paramName));
					}
				}
				return query.list();

			});
		} catch (Exception e) {
			Log.log.error("Error in executing HQL: " + hql, e);
			throw e;
		}

		return result;
	}
    
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<? extends Object> executeHQLSelectInOnly(String hql,
			Map<String, Collection<Object>> queryInParams) throws Exception {
		List<? extends Object> result = null;

		try {
			result = (List<? extends Object>) HibernateProxy.execute(() -> {

				Query query = HibernateUtil.createQuery(hql);

				if (queryInParams != null) {
					for (String paramInName : queryInParams.keySet()) {
						query.setParameterList(paramInName, queryInParams.get(paramInName));
					}
				}
				return query.list();

			});
		} catch (Exception e) {
			Log.log.error("Error in executing HQL: " + hql, e);
			throw e;
		}

		return result;
	}
    
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<? extends Object> executeHQLSelectIn(String hql, Map<String, Collection<Object>> queryInParams,
			Map<String, Object> queryParams) throws Exception {
		List<? extends Object> result = null;

		try {
			result = (List<? extends Object>) HibernateProxy.execute(() -> {
				Query query = HibernateUtil.createQuery(hql);

				if (queryInParams != null) {
					for (String paramInName : queryInParams.keySet()) {
						query.setParameterList(paramInName, queryInParams.get(paramInName));
					}
				}

				if (queryParams != null) {
					for (String paramName : queryParams.keySet()) {
						query.setParameter(paramName, queryParams.get(paramName));
					}
				}

				return query.list();

			});
		} catch (Exception e) {
			Log.log.error("Error in executing HQL: " + hql, e);
			throw e;
		}

		return result;
	}
    
    private static Object getMassagedData(Object rawdata, boolean forUI)
    {
        Object massagedData = null;

        if (rawdata != null)
        {
            if (rawdata instanceof Timestamp)
            {
                if(forUI)
                {
                    Timestamp ts = (Timestamp) rawdata;
                    String dStr = DateUtils.getDateInUTCFormat(ts);
                    DateTime dt = DateUtils.parseISODate(dStr);
                    massagedData = dt.toDate();
                    // massagedData = DateManipulator.getDateInUTCFormat((Timestamp) rawdata); //THIS IS STRING...
                }
                else
                {
                    massagedData = (Timestamp) rawdata;
                }
                
            }
            else if (rawdata instanceof oracle.sql.TIMESTAMP)
            {
                oracle.sql.TIMESTAMP ts = (oracle.sql.TIMESTAMP)rawdata;
                
                try
                {
                    if(forUI)
                    {
                        massagedData = new Date(ts.dateValue().getTime());
                    }
                    else
                    {
                        massagedData = ts.dateValue();
                    }
                }
                catch (SQLException e)
                {
                    massagedData = null;
                }
            }
            else if(rawdata instanceof BigDecimal)
            {
                massagedData = ((BigDecimal)rawdata).doubleValue();
            }
            else if (rawdata instanceof oracle.sql.CLOB || rawdata instanceof java.sql.Clob)
            {
                massagedData = getStringFromClob(rawdata);
            }
            else if (rawdata instanceof oracle.sql.BLOB || rawdata instanceof java.sql.Blob)
            {
                // NOTE : To get the file , use the QueryCustomForm.downloadFile api
                massagedData = "";
            }
            else if (rawdata instanceof byte[])
            {
                // NOTE : To get the file , use the QueryCustomForm.downloadFile api
                massagedData = "";
            } 
            else
            {
                massagedData = rawdata;
            }
        }
        
        return massagedData;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static int getTotalHqlSysIdCount(QueryDTO queryDTO) throws Exception
    {
        int count = 0;
        String countQuery = "";
        try
        {
            String modelName = queryDTO.getModelName();
            if (StringUtils.isBlank(modelName))
            {
                String tempTableName = queryDTO.getTableName();
                if (StringUtils.isNotBlank(tempTableName))
                {
                    modelName = HibernateUtil.table2Class(tempTableName);
                    queryDTO.setModelName(modelName);
                }
            }

            if (StringUtils.isNotEmpty(queryDTO.getHql()))
            {
                //this case is when we have to create complex query using joins to more than 1 table
                countQuery = queryDTO.getCountQueryUsingHQL();
            }
            else
            {
                countQuery = queryDTO.getSelectSysIdCountHQL();
            }

            String countQueryFinal = countQuery;

          HibernateProxy.setCurrentUser("admin");
            count = (int) HibernateProxy.executeNoCache(() -> {
                Query query = HibernateUtil.createQuery(countQueryFinal);

                // Add in all the filtering parameters
                for (QueryParameter param : queryDTO.getWhereParameters())
                {
                    if (countQueryFinal.contains(param.getName())) query.setParameter(param.getName(), param.getValue());
                }

                List<Object> rows = query.list();
                if (rows.size() > 0)
                {
                    return Integer.valueOf(rows.get(0).toString());
                }

                return 0;
            });
        }
        catch (Exception e)
        {
            Log.log.error("Error in import executing HQL: " + countQuery, e);
            throw e;
        }
        return count;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static int getTotalHqlDistinctSysIdCount(QueryDTO queryDTO) throws Exception
    {
        int count = 0;
        String countQuery = "";
        try
        {
            String modelName = queryDTO.getModelName();
            if (StringUtils.isBlank(modelName))
            {
                String tempTableName = queryDTO.getTableName();
                if (StringUtils.isNotBlank(tempTableName))
                {
                    modelName = HibernateUtil.table2Class(tempTableName);
                    queryDTO.setModelName(modelName);
                }
            }

            if (StringUtils.isNotEmpty(queryDTO.getHql()))
            {
                //this case is when we have to create complex query using joins to more than 1 table
                countQuery = queryDTO.getCountQueryUsingHQL();
            }
            else
            {
                countQuery = queryDTO.getSelectDistinctSysIdCountHQL();
            }


            String countQueryFinal = countQuery;
            
          HibernateProxy.setCurrentUser("admin");
            count = (int) HibernateProxy.executeNoCache(() -> {
                Query query = HibernateUtil.createQuery(countQueryFinal);

                // Add in all the filtering parameters
                for (QueryParameter param : queryDTO.getWhereParameters())
                {
                    if (countQueryFinal.contains(param.getName())) query.setParameter(param.getName(), param.getValue());
                }

                List<Object> rows = query.list();
                if (rows.size() > 0)
                {
                    return Integer.valueOf(rows.get(0).toString());
                }

                return 0;
            });
        }
        catch (Exception e)
        {
            Log.log.error("Error in import executing HQL: " + countQuery, e);
            throw e;
        }
        return count;
    }
}
