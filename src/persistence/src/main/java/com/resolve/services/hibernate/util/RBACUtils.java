/******************************************************************************
* (C) Copyright 2018
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;

public final class RBACUtils {
	
    // Legacy RBAC permission defines
    
	public static final String LEGACY_RBAC_SIR_DASHBOARD_CHART_VIEW = "sirDashboardCharts.view";
	public static final String LEGACY_RBAC_SIR_DASHBOARD_INVESTIGATION_LIST_CREATE = "sirDashboardInvestigationList.create";
	// Please note has non-boolean "all" value
    public static final String LEGACY_RBAC_SIR_INVESTIGATION_LIST_VIEW_MODE = "sirInvestigationListViewMode";
    public static final String LEGACY_RBAC_SIR_INVESTIGATION_LIST_VIEW_MODE_VALUE_ALL = "all";
    public static final String LEGACY_RBAC_SIR_DASHBOARD_TITLE_MODIFY = "sirDashboardTitle.modify";
    public static final String LEGACY_RBAC_DASHBOARD_TYPE_MODIFY = "sirDashboardType.modify";
    public static final String LEGACY_RBAC_INVESTIGATION_VIEWER_DATA_COMPROMISED = 
    																"sirInvestigationViewerDataCompromised.modify";
    public static final String LEGACY_RBAC_SIR_DASHBOARD_SEVERITY_MODIFY = "sirDashboardSeverity.modify";
    public static final String LEGACY_RBAC_SIR_DASHBOARD_STATUS_MODIFY = "sirDashboardStatus.modify";
    public static final String LEGACY_RBAC_SIR_DASHBOARD_PLAYBOOK_MODIFY = "sirDashboardPlaybook.modify";
    public static final String LEGACY_RBAC_SIR_DASHBOARD_OWNER_MODIFY = "sirDashboardOwner.modify";
    public static final String LEGACY_RBAC_SIR_DASHBOARD_DUEDATE_MODIFY = "sirDashboardDueDate.modify";
    public static final String LEGACY_RBAC_INVESTIGATION_VIEWER_ACTIVITIES_CREATE = 
    																		"sirInvestigationViewerActivities.create";
    public static final String LEGACY_RBAC_INVESTIGATION_VIEWER_ACTIVITIES_MODIFY = 
    																		"sirInvestigationViewerActivities.modify";
    public static final String LEGACY_RBAC_INVESTIGATION_VIEWER_ACTIVITIES_EXECUTE = 
																			"sirInvestigationViewerActivities.execute";
    public static final String LEGACY_RBAC_INVESTIGATION_VIEWER_NOTES_CREATE = "sirInvestigationViewerNotes.create";
    public static final String LEGACY_RBAC_INVESTIGATION_VIEWER_ARTIFACTS_CREATE = 
    																		"sirInvestigationViewerArtifacts.create";
    public static final String LEGACY_RBAC_INVESTIGATION_VIEWER_ARTIFACTS_DELETE = 
																			"sirInvestigationViewerArtifacts.delete";
    public static final String LEGACY_RBAC_INVESTIGATION_VIEWER_ATTACHMENTS_UPLOAD = 
    																		"sirInvestigationViewerAttachments.create";
    public static final String LEGACY_RBAC_INVESTIGATION_VIEWER_ATTACHMENTS_DELETE = 
																			"sirInvestigationViewerAttachments.delete";
    public static final String LEGACY_RBAC_SIR_INVESTIGATION_VIEWER_STATUS_MODIFY = 
    																		"sirInvestigationViewerStatus.modify";
    public static final String LEGACY_RBAC_SIR_INVESTIGATION_VIEWER_TEAM_MODIFY = "sirInvestigationViewerTeam.modify";
    public static final String LEGACY_RBAC_SIR_PLAYBOOKTEMPLATES_LIST = "sirPlaybookTemplates.view";
    public static final String LEGACY_RBAC_SIR_PLAYBOOKTEMPLATES_TEMPLATEBUILDER_VIEW = 
    																		"sirPlaybookTemplatesTemplateBuilder.view";
    public static final String LEGACY_RBAC_SIR_PLAYBOOKTEMPLATES_TEMPLATEBUILDER_CREATE =
    																		"sirPlaybookTemplatesTemplateBuilder.create";
    public static final String LEGACY_RBAC_SIR_PLAYBOOKTEMPLATES_TEMPLATEBUILDER_MODIFY =
    																		"sirPlaybookTemplatesTemplateBuilder.modify";
    public static final String LEGACY_RBAC_SIR_INVESTIGATION_VIEWER_VIEW = "sirInvestigationViewer.view";
    public static final String LEGACY_RBAC_SIR_DASHBOARD_VIEW = "sirDashboard.view";
    
    // New RBAC permission defines
    
    public static final String RBAC_SEPARATOR = ".";
    public static final String RBAC_VALUE_SEPARATOR = ",";
    
    // RBAC verbs
    public static final String RBAC_CHANGE = "change";
    public static final String RBAC_CREATE = Constants.CREATE_ACCESS;
    public static final String RBAC_UPLOAD = "upload";
    public static final String RBAC_DOWNLOAD = "download";
    public static final String RBAC_SET = "set";
    public static final String RBAC_UNSET = "unset";
    public static final String RBAC_DELETE = Constants.DELETE_ACCESS;
    public static final String RBAC_EXECUTE = Constants.EXECUTE_ACCESS;
    public static final String RBAC_VIEW = Constants.VIEW_ACCESS;
    public static final String RBAC_LIST = "list";
    public static final String RBAC_MODIFY = Constants.MODIFY_ACCESS;
    public static final String RBAC_ENABLED = "enabled";
    
    public static final String RBAC_SIR = "sir";
    public static final String RBAC_SIR_ENABLED =  RBAC_SIR + RBAC_SEPARATOR + RBAC_ENABLED;
    public static final String RBAC_SIR_ANALYTICS = RBAC_SIR + RBAC_SEPARATOR + "analytics";
    public static final String RBAC_SIR_ANALYTICS_GRAPHS = RBAC_SIR_ANALYTICS + RBAC_SEPARATOR + "graphs";    
    public static final String RBAC_SIR_ANALYTICS_GRAPHS_VIEW = RBAC_SIR_ANALYTICS_GRAPHS + RBAC_SEPARATOR + RBAC_VIEW;
    public static final String RBAC_SIR_DASHBOARD = RBAC_SIR + RBAC_SEPARATOR + "dashboard";
    public static final String RBAC_SIR_DASHBOARD_CASECREATION = RBAC_SIR_DASHBOARD + RBAC_SEPARATOR + "caseCreation";
    public static final String RBAC_SIR_DASHBOARD_CASECREATION_CREATE = 
    														RBAC_SIR_DASHBOARD_CASECREATION + RBAC_SEPARATOR + "create";
    public static final String RBAC_SIR_DASHBOARD_INCIDENTLIST = RBAC_SIR_DASHBOARD + RBAC_SEPARATOR + "incidentList";
    public static final String RBAC_SIR_DASHBOARD_INCIDENTLIST_VIEWALL = 
    														RBAC_SIR_DASHBOARD_INCIDENTLIST + RBAC_SEPARATOR + "viewAll";
    public static final String RBAC_SIR_INCIDENT = RBAC_SIR + RBAC_SEPARATOR + "incident";
    public static final String RBAC_SIR_INCIDENT_TITLE = RBAC_SIR_INCIDENT + RBAC_SEPARATOR + "title";
    public static final String RBAC_SIR_INCIDENT_TITLE_CHANGE = RBAC_SIR_INCIDENT_TITLE + RBAC_SEPARATOR + RBAC_CHANGE;
    public static final String RBAC_SIR_INCIDENT_CLASSIFICATION = RBAC_SIR_INCIDENT + RBAC_SEPARATOR + "classification";
    public static final String RBAC_SIR_INCIDENT_CLASSIFICATION_CHANGE = 
    													RBAC_SIR_INCIDENT_CLASSIFICATION + RBAC_SEPARATOR + RBAC_CHANGE;
    public static final String RBAC_SIR_INCIDENT_TYPE = RBAC_SIR_INCIDENT + RBAC_SEPARATOR + "type";
    public static final String RBAC_SIR_INCIDENT_TYPE_CHANGE = RBAC_SIR_INCIDENT_TYPE + RBAC_SEPARATOR + RBAC_CHANGE;
    public static final String RBAC_SIR_INCIDENT_DATACOMPROMISED = RBAC_SIR_INCIDENT + RBAC_SEPARATOR + "dataCompromised";
    public static final String RBAC_SIR_INCIDENT_DATACOMPROMISED_CHANGE = 
    													RBAC_SIR_INCIDENT_DATACOMPROMISED + RBAC_SEPARATOR + RBAC_CHANGE;
    public static final String RBAC_SIR_INCIDENT_SEVERITY = RBAC_SIR_INCIDENT + RBAC_SEPARATOR + "severity";
    public static final String RBAC_SIR_INCIDENT_SEVERITY_CHANGE = 
    														RBAC_SIR_INCIDENT_SEVERITY + RBAC_SEPARATOR + RBAC_CHANGE;
    public static final String RBAC_SIR_INCIDENT_STATUS = RBAC_SIR_INCIDENT + RBAC_SEPARATOR + "status";
    public static final String RBAC_SIR_INCIDENT_STATUS_CHANGE = RBAC_SIR_INCIDENT_STATUS + RBAC_SEPARATOR + RBAC_CHANGE;
    public static final String RBAC_SIR_INCIDENT_PLAYBOOK = RBAC_SIR_INCIDENT + RBAC_SEPARATOR + "playbook";
    public static final String RBAC_SIR_INCIDENT_PLAYBOOK_CHANGE = 
    														RBAC_SIR_INCIDENT_PLAYBOOK + RBAC_SEPARATOR + RBAC_CHANGE;
    public static final String RBAC_SIR_INCIDENT_OWNER = RBAC_SIR_INCIDENT + RBAC_SEPARATOR + "owner";
    public static final String RBAC_SIR_INCIDENT_OWNER_CHANGE = RBAC_SIR_INCIDENT_OWNER + RBAC_SEPARATOR + RBAC_CHANGE;
    public static final String RBAC_SIR_INCIDENT_DUEDATE = RBAC_SIR_INCIDENT + RBAC_SEPARATOR + "dueDate";
    public static final String RBAC_SIR_INCIDENT_DUEDATE_CHANGE = RBAC_SIR_INCIDENT_DUEDATE + RBAC_SEPARATOR + RBAC_CHANGE;
    public static final String RBAC_SIR_INCIDENT_RISKSCORE = RBAC_SIR_INCIDENT + RBAC_SEPARATOR + "riskScore";
    public static final String RBAC_SIR_INCIDENT_RISKSCORE_CHANGE = 
    														RBAC_SIR_INCIDENT_RISKSCORE + RBAC_SEPARATOR + RBAC_CHANGE;
    public static final String RBAC_SIR_INCIDENT_INCIDENTFLAGS = RBAC_SIR_INCIDENT + RBAC_SEPARATOR + "incidentFlags";
    public static final String RBAC_SIR_INCIDENT_INCIDENTFLAGS_CHANGE = 
    														RBAC_SIR_INCIDENT_INCIDENTFLAGS + RBAC_SEPARATOR + RBAC_CHANGE;
    public static final String RBAC_SIR_INCIDENT_MARKINCIDENT = RBAC_SIR_INCIDENT + RBAC_SEPARATOR + "markIncident";
    public static final String RBAC_SIR_INCIDENT_MARKINCIDENT_ASDUPLICATE = RBAC_SIR_INCIDENT_MARKINCIDENT + RBAC_SEPARATOR + "asDuplicate";
    public static final String RBAC_SIR_INCIDENT_MARKINCIDENT_ASMASTER = RBAC_SIR_INCIDENT_MARKINCIDENT + RBAC_SEPARATOR + "asMaster";
    public static final String RBAC_SIR_INCIDENT_PRIORITY = RBAC_SIR_INCIDENT + RBAC_SEPARATOR + "priority";
    public static final String RBAC_SIR_INCIDENT_PRIORITY_CHANGE = 
    														RBAC_SIR_INCIDENT_PRIORITY + RBAC_SEPARATOR + RBAC_CHANGE;
    
    public static final String RBAC_SIR_INCIDENTVIEWER = RBAC_SIR + RBAC_SEPARATOR + "incidentViewer";
    public static final String RBAC_SIR_INCIDENTVIEWER_TASK = RBAC_SIR_INCIDENTVIEWER + RBAC_SEPARATOR + "activity";
    public static final String RBAC_SIR_INCIDENTVIEWER_TASK_CREATE = 
    														RBAC_SIR_INCIDENTVIEWER_TASK + RBAC_SEPARATOR + RBAC_CREATE;
    public static final String RBAC_SIR_INCIDENTVIEWER_TASK_EXECUTE = 
															RBAC_SIR_INCIDENTVIEWER_TASK + RBAC_SEPARATOR + RBAC_EXECUTE;
    public static final String RBAC_SIR_INCIDENTVIEWER_TASK_STATUS = 
    														RBAC_SIR_INCIDENTVIEWER_TASK + RBAC_SEPARATOR + "status";
    public static final String RBAC_SIR_INCIDENTVIEWER_TASK_STATUS_CHANGE = 
    													RBAC_SIR_INCIDENTVIEWER_TASK_STATUS + RBAC_SEPARATOR + RBAC_CHANGE;
    public static final String RBAC_SIR_INCIDENTVIEWER_TASK_ASSIGNEE = 
															RBAC_SIR_INCIDENTVIEWER_TASK + RBAC_SEPARATOR + "assignee";
    public static final String RBAC_SIR_INCIDENTVIEWER_TASK_ASSIGNEE_CHANGE = 
    												RBAC_SIR_INCIDENTVIEWER_TASK_ASSIGNEE + RBAC_SEPARATOR + RBAC_CHANGE;
    public static final String RBAC_SIR_INCIDENTVIEWER_NOTE = RBAC_SIR_INCIDENTVIEWER + RBAC_SEPARATOR + "note";
    public static final String RBAC_SIR_INCIDENTVIEWER_NOTE_CREATE = 
    														RBAC_SIR_INCIDENTVIEWER_NOTE + RBAC_SEPARATOR + RBAC_CREATE;
    public static final String RBAC_SIR_INCIDENTVIEWER_ARTIFACT = RBAC_SIR_INCIDENTVIEWER + RBAC_SEPARATOR + "artifact";
    public static final String RBAC_SIR_INCIDENTVIEWER_ARTIFACT_CREATE = 
    													RBAC_SIR_INCIDENTVIEWER_ARTIFACT + RBAC_SEPARATOR + RBAC_CREATE;
    public static final String RBAC_SIR_INCIDENTVIEWER_ARTIFACT_EXECUTEAUTOMATION = 
    									RBAC_SIR_INCIDENTVIEWER_ARTIFACT + RBAC_SEPARATOR + RBAC_EXECUTE + "Automation";
    
    public static final String RBAC_SIR_INCIDENTVIEWER_ARTIFACT_DELETE = 
    													RBAC_SIR_INCIDENTVIEWER_ARTIFACT + RBAC_SEPARATOR + RBAC_DELETE;
    
    public static final String RBAC_SIR_INCIDENTVIEWER_ATTACHMENT = 
    															RBAC_SIR_INCIDENTVIEWER + RBAC_SEPARATOR + "attachment";
    public static final String RBAC_SIR_INCIDENTVIEWER_ATTACHMENT_UPLOAD = 
    												RBAC_SIR_INCIDENTVIEWER_ATTACHMENT + RBAC_SEPARATOR + RBAC_UPLOAD;
    public static final String RBAC_SIR_INCIDENTVIEWER_ATTACHMENT_DOWNLOAD = 
													RBAC_SIR_INCIDENTVIEWER_ATTACHMENT + RBAC_SEPARATOR + RBAC_DOWNLOAD;
    public static final String RBAC_SIR_INCIDENTVIEWER_ATTACHMENT_DELETE = 
													RBAC_SIR_INCIDENTVIEWER_ATTACHMENT + RBAC_SEPARATOR + RBAC_DELETE;
    public static final String RBAC_SIR_INCIDENTVIEWER_ATTACHMENT_DOWNLOADMALICIOUS = 
										RBAC_SIR_INCIDENTVIEWER_ATTACHMENT + RBAC_SEPARATOR + RBAC_DOWNLOAD + "Malicious";
    public static final String RBAC_SIR_INCIDENTVIEWER_ATTACHMENT_UNSETMALICIOUS = 
											RBAC_SIR_INCIDENTVIEWER_ATTACHMENT + RBAC_SEPARATOR + RBAC_UNSET + "Malicious";
    public static final String RBAC_SIR_INCIDENTVIEWER_ACTIVITY_TIMELINE = 
															RBAC_SIR_INCIDENTVIEWER + RBAC_SEPARATOR + "activityTimeline";
    public static final String RBAC_SIR_INCIDENTVIEWER_ACTIVITY_TIMELINE_VIEW = 
    												RBAC_SIR_INCIDENTVIEWER_ACTIVITY_TIMELINE + RBAC_SEPARATOR + RBAC_VIEW;
    public static final String RBAC_SIR_INCIDENTVIEWER_TEAM = RBAC_SIR_INCIDENTVIEWER + RBAC_SEPARATOR + "team";
    public static final String RBAC_SIR_INCIDENTVIEWER_TEAM_CHANGE = 
    														RBAC_SIR_INCIDENTVIEWER_TEAM + RBAC_SEPARATOR + RBAC_CHANGE;
    public static final String RBAC_SIR_PLAYBOOK_TEMPLATES = RBAC_SIR + RBAC_SEPARATOR + "playbookTemplates";
    public static final String RBAC_SIR_PLAYBOOK_TEMPLATES_LIST = RBAC_SIR_PLAYBOOK_TEMPLATES + RBAC_SEPARATOR + RBAC_LIST;
    public static final String RBAC_SIR_PLAYBOOK_TEMPLATES_TEMPLATEBUILDER = 
    													RBAC_SIR_PLAYBOOK_TEMPLATES + RBAC_SEPARATOR + "templateBuilder";
    public static final String RBAC_SIR_PLAYBOOK_TEMPLATES_TEMPLATEBUILDER_VIEW = 
    											RBAC_SIR_PLAYBOOK_TEMPLATES_TEMPLATEBUILDER + RBAC_SEPARATOR + RBAC_VIEW;
    public static final String RBAC_SIR_PLAYBOOK_TEMPLATES_TEMPLATEBUILDER_CHANGE =
    										RBAC_SIR_PLAYBOOK_TEMPLATES_TEMPLATEBUILDER + RBAC_SEPARATOR + RBAC_CHANGE;

    public static final String RBAC_SETTINGS = "settings";
    public static final String RBAC_SIR_SETTINGS = RBAC_SIR + RBAC_SEPARATOR + RBAC_SETTINGS;
    public static final String RBAC_SIR_SETTINGS_CHANGE = RBAC_SIR_SETTINGS + RBAC_SEPARATOR + RBAC_CHANGE;

    public static final String ANALYTICS = "analytics";
    public static final String ANALYTICS_REPORTING = ANALYTICS + RBAC_SEPARATOR + "reporting";
    public static final String ANALYTICS_REPORTING_VIEW = ANALYTICS_REPORTING + RBAC_SEPARATOR + RBAC_VIEW;
    
    private static final String OLD_RBAC_SETTINGS_SIR_CONFIGURATION_CHANGE = 
    										RBAC_SETTINGS + RBAC_SEPARATOR + RBAC_SIR + RBAC_SEPARATOR + "configuration" + 
    										RBAC_SEPARATOR + RBAC_CHANGE;
    
    public static final String ARBAC_URI_WILD_CARD_SUFFIX = "*";
    public static final String ARBAC_URI_SEPARATOR = "/";
    
    public static final Map<String, String> legacyToNewRBAC = new HashMap<String, String>();
    public static final Collection<String> applicationRBACs = new ArrayList<String>();
    public static final Map<String, String> uriToARBAC = new HashMap<String, String>();

    // Initialize new to old RBAC permission mapping
    
    static {
    	
    	legacyToNewRBAC.put(LEGACY_RBAC_SIR_DASHBOARD_CHART_VIEW, RBAC_SIR_ANALYTICS_GRAPHS_VIEW);
    	legacyToNewRBAC.put(LEGACY_RBAC_SIR_DASHBOARD_INVESTIGATION_LIST_CREATE, RBAC_SIR_DASHBOARD_CASECREATION_CREATE);
    	legacyToNewRBAC.put(LEGACY_RBAC_SIR_INVESTIGATION_LIST_VIEW_MODE, RBAC_SIR_DASHBOARD_INCIDENTLIST_VIEWALL);
    	legacyToNewRBAC.put(LEGACY_RBAC_SIR_DASHBOARD_TITLE_MODIFY, RBAC_SIR_INCIDENT_TITLE_CHANGE);
    	legacyToNewRBAC.put(LEGACY_RBAC_DASHBOARD_TYPE_MODIFY, RBAC_SIR_INCIDENT_TYPE_CHANGE);
    	legacyToNewRBAC.put(LEGACY_RBAC_INVESTIGATION_VIEWER_DATA_COMPROMISED, RBAC_SIR_INCIDENT_DATACOMPROMISED_CHANGE);
    	legacyToNewRBAC.put(LEGACY_RBAC_SIR_DASHBOARD_SEVERITY_MODIFY, RBAC_SIR_INCIDENT_SEVERITY_CHANGE);
    	legacyToNewRBAC.put(LEGACY_RBAC_SIR_DASHBOARD_STATUS_MODIFY, RBAC_SIR_INCIDENT_STATUS_CHANGE);
    	legacyToNewRBAC.put(LEGACY_RBAC_SIR_DASHBOARD_PLAYBOOK_MODIFY, RBAC_SIR_INCIDENT_PLAYBOOK_CHANGE);
    	legacyToNewRBAC.put(LEGACY_RBAC_SIR_DASHBOARD_OWNER_MODIFY, RBAC_SIR_INCIDENT_OWNER_CHANGE);
    	legacyToNewRBAC.put(LEGACY_RBAC_SIR_DASHBOARD_DUEDATE_MODIFY, RBAC_SIR_INCIDENT_DUEDATE_CHANGE);
    	legacyToNewRBAC.put(LEGACY_RBAC_INVESTIGATION_VIEWER_ACTIVITIES_CREATE, RBAC_SIR_INCIDENTVIEWER_TASK_CREATE);
    	// Old Activity/Task modify RBAC => New Activity/Task State + Assignee change
    	legacyToNewRBAC.put(LEGACY_RBAC_INVESTIGATION_VIEWER_ACTIVITIES_MODIFY,
    					 	RBAC_SIR_INCIDENTVIEWER_TASK_STATUS_CHANGE + RBAC_VALUE_SEPARATOR +
    					 	RBAC_SIR_INCIDENTVIEWER_TASK_ASSIGNEE_CHANGE);
    	legacyToNewRBAC.put(LEGACY_RBAC_INVESTIGATION_VIEWER_NOTES_CREATE, RBAC_SIR_INCIDENTVIEWER_NOTE_CREATE);
    	legacyToNewRBAC.put(LEGACY_RBAC_INVESTIGATION_VIEWER_ARTIFACTS_CREATE, RBAC_SIR_INCIDENTVIEWER_ARTIFACT_CREATE);
    	legacyToNewRBAC.put(LEGACY_RBAC_INVESTIGATION_VIEWER_ATTACHMENTS_UPLOAD, RBAC_SIR_INCIDENTVIEWER_ATTACHMENT_UPLOAD);
    	legacyToNewRBAC.put(LEGACY_RBAC_INVESTIGATION_VIEWER_ATTACHMENTS_DELETE, RBAC_SIR_INCIDENTVIEWER_ATTACHMENT_DELETE);
    	legacyToNewRBAC.put(LEGACY_RBAC_SIR_INVESTIGATION_VIEWER_STATUS_MODIFY, RBAC_SIR_INCIDENT_STATUS_CHANGE);
    	legacyToNewRBAC.put(LEGACY_RBAC_SIR_INVESTIGATION_VIEWER_TEAM_MODIFY, RBAC_SIR_INCIDENTVIEWER_TEAM_CHANGE);
    	legacyToNewRBAC.put(LEGACY_RBAC_SIR_PLAYBOOKTEMPLATES_LIST, RBAC_SIR_PLAYBOOK_TEMPLATES_LIST);
    	legacyToNewRBAC.put(LEGACY_RBAC_SIR_PLAYBOOKTEMPLATES_TEMPLATEBUILDER_VIEW, 
    						RBAC_SIR_PLAYBOOK_TEMPLATES_TEMPLATEBUILDER_VIEW);
    	legacyToNewRBAC.put(LEGACY_RBAC_SIR_PLAYBOOKTEMPLATES_TEMPLATEBUILDER_CREATE, 
    						RBAC_SIR_PLAYBOOK_TEMPLATES_TEMPLATEBUILDER_CHANGE);
    	legacyToNewRBAC.put(LEGACY_RBAC_SIR_PLAYBOOKTEMPLATES_TEMPLATEBUILDER_MODIFY, 
							RBAC_SIR_PLAYBOOK_TEMPLATES_TEMPLATEBUILDER_CHANGE);
    	legacyToNewRBAC.put(LEGACY_RBAC_INVESTIGATION_VIEWER_ARTIFACTS_DELETE, RBAC_SIR_INCIDENTVIEWER_ARTIFACT_DELETE);
    	legacyToNewRBAC.put(LEGACY_RBAC_INVESTIGATION_VIEWER_ACTIVITIES_EXECUTE, RBAC_SIR_INCIDENTVIEWER_TASK_EXECUTE);

    	/*
    	 * Application RBACS
    	 */
    	
    	// SIR 
    	applicationRBACs.add(RBAC_SIR);
    	
    	/*
    	 * URL to A-RBAC
    	 */
    	
    	// SIR URLs (in lower case only and should not end with /) to A-RBAC
    	
    	uriToARBAC.put("/resolve/service/playbook/" + ARBAC_URI_WILD_CARD_SUFFIX, RBAC_SIR);    	
    	uriToARBAC.put("/resolve/sir/" + ARBAC_URI_WILD_CARD_SUFFIX, RBAC_SIR);
    	uriToARBAC.put("/resolve/service/wikiadmin/listplaybooktemplates", RBAC_SIR);
    }
    
    private RBACUtils() {}
    
    @SuppressWarnings("unchecked")
	public static final JSONObject mapToNewRBACs(JSONObject dbRBACs) {
    	
    	JSONObject newRBACs = new JSONObject();
    	
    	if (dbRBACs != null && !dbRBACs.isNullObject() && !dbRBACs.isEmpty()) {
    		
	    	Iterator<String> dbRBACKeyIter = dbRBACs.keys();
	    	
	    	while (dbRBACKeyIter.hasNext()) {
	    		String dbRBACKey =  dbRBACKeyIter.next();
	    		Collection<String> dbRBACKeys = new ArrayList<String>();
	    		
	    		if (!dbRBACKey.contains(RBAC_SEPARATOR)) {
	    			
//	    			Collection<String> dbRBACKeySuffixes = new ArrayList<String>();
	    			
	    			if (legacyToNewRBAC.containsKey(dbRBACKey + RBAC_SEPARATOR + Constants.VIEW_ACCESS)) {
	    				dbRBACKeys.add(dbRBACKey + RBAC_SEPARATOR + Constants.VIEW_ACCESS);
	    			}
	    			
	    			if (legacyToNewRBAC.containsKey(dbRBACKey + RBAC_SEPARATOR + Constants.CREATE_ACCESS)) {
	    				dbRBACKeys.add(dbRBACKey + RBAC_SEPARATOR + Constants.CREATE_ACCESS);
	    			}
	    			
	    			if (legacyToNewRBAC.containsKey(dbRBACKey + RBAC_SEPARATOR + Constants.MODIFY_ACCESS)) {
	    				dbRBACKeys.add(dbRBACKey + RBAC_SEPARATOR + Constants.MODIFY_ACCESS);
	    			}
	    			
	    			if (legacyToNewRBAC.containsKey(dbRBACKey + RBAC_SEPARATOR + Constants.DELETE_ACCESS)) {
	    				dbRBACKeys.add(dbRBACKey + RBAC_SEPARATOR + Constants.DELETE_ACCESS);
	    			}
	    			
	    			if (legacyToNewRBAC.containsKey(dbRBACKey + RBAC_SEPARATOR + Constants.EXECUTE_ACCESS)) {
	    				dbRBACKeys.add(dbRBACKey + RBAC_SEPARATOR + Constants.EXECUTE_ACCESS);
	    			}
	    			
	    			if (dbRBACKeys.isEmpty()) {
	    				dbRBACKeys.add(dbRBACKey);
	    			}
	    		} else {
	    			dbRBACKeys.add(dbRBACKey);
	    		}
	    			    		
	    		dbRBACKeys.forEach(tDBRBACKey -> {
	    			Collection<Pair<String, Boolean>> txedRBACs = new ArrayList<Pair<String, Boolean>>();
	    			
		    		if (legacyToNewRBAC.containsKey(tDBRBACKey)) {
		    			txedRBACs = txOldRBACToNew(tDBRBACKey, dbRBACs.get(StringUtils.substringBeforeLast(tDBRBACKey, RBAC_SEPARATOR)));
		    		} else {
		    			if (dbRBACs.has(tDBRBACKey) && dbRBACs.get(tDBRBACKey) instanceof Boolean) {
		    				txedRBACs.add(Pair.of(tDBRBACKey, (Boolean)dbRBACs.get(tDBRBACKey)));
		    				
		    				// A-RBAC "sir.enabled" (old) <-> "sir" (new)
		    				
		    				if (tDBRBACKey.equals(RBAC_SIR) && !newRBACs.containsKey(RBAC_SIR_ENABLED)) {
		    					txedRBACs.add(Pair.of(RBAC_SIR_ENABLED, (Boolean)dbRBACs.get(tDBRBACKey)));
		    				} else if (tDBRBACKey.equals(RBAC_SIR_ENABLED) && !newRBACs.containsKey(RBAC_SIR)) {
		    					txedRBACs.add(Pair.of(RBAC_SIR, (Boolean)dbRBACs.get(tDBRBACKey)));
		    				}
		    			} else {
	    					Log.log.trace("Possible Legacy RBAC " + tDBRBACKey + " value is of unsupported type " + 
	    							 	  dbRBACs.get(tDBRBACKey).getClass().getName() + 
	    							 	  ". Unable to transform possible legacy RBAC permissions for " +
	    							 	  dbRBACs.get(tDBRBACKey) + "!!!");
		    			}
		    		}
		    		
		    		if (!txedRBACs.isEmpty()) {
		    			for (Pair<String, Boolean> txedRBACPair : txedRBACs) {
		    				newRBACs.put(txedRBACPair.getKey(), txedRBACPair.getValue());
		    			}
		    		}
	    		});
	    	}
	    	
	    	dependencyValidation(newRBACs);
    	}
    	
    	return newRBACs;
    }
    
    private static final Collection<Pair<String, Boolean>> txOldRBACToNew(String legacyRBACKey, Object legacyRBACVal) {
    	
    	Collection<Pair<String, Boolean>> txedRBACs = new ArrayList<Pair<String, Boolean>>();
    	
    	String[] newRBACKeys = legacyToNewRBAC.get(legacyRBACKey).split(RBAC_VALUE_SEPARATOR);
    	
    	String legacyRBACPerm = StringUtils.substringAfterLast(legacyRBACKey, RBAC_SEPARATOR);
    	
    	for (int i = 0; i < newRBACKeys.length; i++) {
    		String newRBACKey = newRBACKeys[i];
    		
    		Boolean perm = Boolean.FALSE;
    		
    		if (legacyRBACKey.equals(LEGACY_RBAC_SIR_INVESTIGATION_LIST_VIEW_MODE) && legacyRBACVal instanceof String) {
    			perm = Boolean.valueOf(
    							LEGACY_RBAC_SIR_INVESTIGATION_LIST_VIEW_MODE_VALUE_ALL.equalsIgnoreCase(
    									(String)legacyRBACVal));   			
    		} else if (legacyRBACVal instanceof JSONObject) {
    			if (((JSONObject)legacyRBACVal).has(legacyRBACPerm)) {
    				if (((JSONObject)legacyRBACVal).get(legacyRBACPerm) instanceof Boolean) {
    					perm = Boolean.valueOf(((JSONObject)legacyRBACVal).getBoolean(legacyRBACPerm));
    				} else if (((JSONObject)legacyRBACVal).get(legacyRBACPerm) instanceof String) {
    					perm = Boolean.valueOf(((JSONObject)legacyRBACVal).getString(legacyRBACPerm));
    				}
    			}
    		} else {
    			Log.log.warn("Legacy RBAC " + legacyRBACKey + " value is of unsupported type " + 
    						 legacyRBACVal.getClass().getName() + ". Unable to transform legacy RBAC permissions for " +
    						 legacyRBACVal + "!!!");
    		}
    		
    		txedRBACs.add(Pair.of(newRBACKey, perm));
    	}
    	
    	return txedRBACs;
    }
    
    private static final void dependencyValidation(JSONObject newRBACs) {
    	/*
    	 * Rule # 1
    	 * 
    	 * User can have permission to download malicious attachment only if 
    	 * user has permission to download attachments.  
    	 */
    	
    	if (newRBACs.has(RBAC_SIR_INCIDENTVIEWER_ATTACHMENT_DOWNLOADMALICIOUS) &&
    		newRBACs.getBoolean(RBAC_SIR_INCIDENTVIEWER_ATTACHMENT_DOWNLOADMALICIOUS)) {
    		if (!newRBACs.has(RBAC_SIR_INCIDENTVIEWER_ATTACHMENT_DOWNLOAD) ||
    			!newRBACs.getBoolean(RBAC_SIR_INCIDENTVIEWER_ATTACHMENT_DOWNLOAD)) {
    			newRBACs.put(RBAC_SIR_INCIDENTVIEWER_ATTACHMENT_DOWNLOADMALICIOUS, Boolean.FALSE);
    		}
    	}
    	
    	/*
    	 * Map RBAC "settings.sir.configuration.change" (old) -> "sir.settings.change" (new)
    	 */
    	
    	if (newRBACs.has(OLD_RBAC_SETTINGS_SIR_CONFIGURATION_CHANGE) && !newRBACs.has(RBAC_SIR_SETTINGS_CHANGE)) {
    		newRBACs.put(RBAC_SIR_SETTINGS_CHANGE, newRBACs.get(OLD_RBAC_SETTINGS_SIR_CONFIGURATION_CHANGE));
    	}
    	
    	/*
    	 * Map A-RBAC "sir" <-> "sir.enabled"
    	 */
    	
    	if (newRBACs.has(RBAC_SIR) && !newRBACs.has(RBAC_SIR_ENABLED)) {
    		newRBACs.put(RBAC_SIR_ENABLED, newRBACs.get(RBAC_SIR));
		} else if (newRBACs.has(RBAC_SIR_ENABLED) && !newRBACs.has(RBAC_SIR)) {
			newRBACs.put(RBAC_SIR, newRBACs.get(RBAC_SIR_ENABLED));
		}
    	
    	/*
    	 * Add A-RBAC "sir" set to false if missing.
    	 */
    	
    	if (!newRBACs.has(RBAC_SIR) && !newRBACs.has(RBAC_SIR_ENABLED)) {
    		newRBACs.put(RBAC_SIR, Boolean.FALSE);
    	}
    }
    
    public static final Collection<String> getApplicationRBACs() {
    	return applicationRBACs;
    }
    
    public static final boolean isApplicationRBAC(String rbac) {
    	return applicationRBACs.contains(rbac);
    }
    
    public static final String getApplicationRBAC(String rbac) {
    	String arbac = null;
    	
    	if (StringUtils.isNotBlank(rbac) && rbac.contains(RBAC_SEPARATOR)) {
    		
    		String lastCheckedPartOfRBAC = "";
    		
    		while(StringUtils.isNotBlank(StringUtils.substringBetween(rbac, lastCheckedPartOfRBAC, RBAC_SEPARATOR))) {
	    		String partOfRBACToCheck = lastCheckedPartOfRBAC + 
	    								   StringUtils.substringBetween(rbac, lastCheckedPartOfRBAC, RBAC_SEPARATOR);
	    		
	    		if (StringUtils.isNotBlank(partOfRBACToCheck) && applicationRBACs.contains(partOfRBACToCheck)) {
	    			arbac = partOfRBACToCheck;
	    			break;
	    		}
	    		
	    		lastCheckedPartOfRBAC = partOfRBACToCheck;
    		}
    	}
    	
    	return arbac;
    }
    
    public static final String getApplicationRBACForURI(String uri) {
    	String arbac = null;
    	
    	if (StringUtils.isNotBlank(uri) && uri.length() > 1) {
    		
    		// Add / at the end of URI if not present 
    		
    		String normalizedURI = uri.toLowerCase();
    		
    		if (!normalizedURI.endsWith(ARBAC_URI_SEPARATOR)) {
    			normalizedURI = normalizedURI + ARBAC_URI_SEPARATOR;
    		}
    		
    		String lastCheckedPartOfURI = ARBAC_URI_SEPARATOR;
    		
    		while(StringUtils.isNotBlank(StringUtils.substringBetween(normalizedURI, lastCheckedPartOfURI, ARBAC_URI_SEPARATOR)) ) {
  	    		String partOfURIToCheck = lastCheckedPartOfURI + 
  	    								  StringUtils.substringBetween(normalizedURI, lastCheckedPartOfURI, ARBAC_URI_SEPARATOR);
  	    		
  	    		if (StringUtils.isNotBlank(partOfURIToCheck)) { 
  	    			if (uriToARBAC.containsKey(partOfURIToCheck) && StringUtils.isNotBlank(uriToARBAC.get(partOfURIToCheck))) {
  	    				arbac = uriToARBAC.get(partOfURIToCheck);
  	    				break;
  	    			} else if (uriToARBAC.containsKey(partOfURIToCheck + ARBAC_URI_SEPARATOR + ARBAC_URI_WILD_CARD_SUFFIX)) {
  	    				arbac = uriToARBAC.get(partOfURIToCheck + ARBAC_URI_SEPARATOR + ARBAC_URI_WILD_CARD_SUFFIX);
  	    				break;
  	    			}
  	    		}
  	    		
  	    		lastCheckedPartOfURI = partOfURIToCheck + ARBAC_URI_SEPARATOR;
  	    		
  	    		if (lastCheckedPartOfURI.length() > uri.length()) {
  	    			break;
  	    		}
      		}
    	}
    	
    	return arbac;
    }
}
