/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.persistence.model.Organization;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.OrganizationVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class OrganizationUtil
{
    public static List<OrganizationVO> getOrganization(QueryDTO query)
    {
        List<OrganizationVO> result = new ArrayList<OrganizationVO>();
        
        int limit = query.getLimit();
        int offset = query.getStart();
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null)
            {
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        Organization instance = new Organization();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }
                        OrganizationVO vo = instance.doGetVO();
                        if(vo.getUDisable() != null)
                        {
                            vo.setUDisable(!vo.getUDisable());
                        }
                        result.add(vo);
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        Organization instance = (Organization) o;
                        OrganizationVO vo = instance.doGetVO();
                        if(vo.getUDisable() != null)
                        {
                            vo.setUDisable(!vo.getUDisable());
                        }
                        result.add(vo);
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return result;
    }
    
    public static Map<String, String> getListOfAllOraganizationNames()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        QueryDTO query = new QueryDTO();
        query.setModelName("Organization");
        query.setSelectColumns("sys_id,UOrganizationName");
        query.setWhereClause("UDisable is false");
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, -1, -1, false);
            if(data != null)
            {
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        Organization instance = new Organization();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        result.put(instance.getSys_id(), instance.getUOrganizationName());
                    }//end of for loop
                }
//                else
//                {
//                    for(Object o : data)
//                    {
//                        Organization instance = (Organization) o;
//                        result.put(instance.getSys_id(), instance.getUOrganizationName());
//                    }
//                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        
        return result;
    }
    
    public static OrganizationVO findOrganizationById(String sys_id)
    {
        Organization model = null;
        OrganizationVO result = null;
        
        if(!StringUtils.isEmpty(sys_id))
        {
            try
            {
                model = findOrganizationModelById(sys_id);
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }
        
        if(model != null)
        {
            result = model.doGetVO();
        }
        
        return result;
    }
    
    public static OrganizationVO saveOrganization(OrganizationVO vo, String username)
    {
        if(vo != null)
        {
            Organization model = null;
            if(StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equals(VO.STRING_DEFAULT))
            {
                model = findOrganizationModelById(vo.getSys_id());
            }
            else
            {
                model = findOrganizationModelByName(vo.getUOrganizationName());
                if(model != null)
                {
                    throw new RuntimeException("Organization with name '" + vo.getUOrganizationName() + "' already exist. Please add a unique name.");
                }
                else
                {
                    model = new Organization();
                    model.setUDisable(false);
                }
            }
            model.applyVOToModel(vo);
            model = saveOrganization(model, username);
            vo = model.doGetVO();
        }
        
        return vo;
    }
    
    public static void disableEnableOrganizationByIds(String[] sysIds, boolean disable, String username) throws Exception
    {
        if(sysIds != null)
        {
            for(String sysId : sysIds)
            {
                disableEnableOrganizationById(sysId, disable, username);
            }
        }
    }
    
    private static void disableEnableOrganizationById(String sysId, boolean disable, String username) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
            
	            Organization model = HibernateUtil.getDAOFactory().getOrganizationDAO().findById(sysId);
	            if(model != null)
	            {
	                model.setUDisable(disable);
	            }

        	});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw new Exception(e); 
        }
    } 

    private static Organization findOrganizationModelById(String sys_id)
    {
        Organization result = null;
        
        if(!StringUtils.isEmpty(sys_id))
        {
            try
            {
            	result = (Organization) HibernateProxy.execute(() -> {
                	return HibernateUtil.getDAOFactory().getOrganizationDAO().findById(sys_id);
                });
                
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return result;
    }
    
    private static Organization findOrganizationModelByName(String name)
    {
        if(!StringUtils.isEmpty(name))
        {
            
            
            try
            {
            	return (Organization) HibernateProxy.execute(() -> {
            		Organization result = new Organization();
                    result.setUOrganizationName(name);
            		return HibernateUtil.getDAOFactory().getOrganizationDAO().findFirst(result);
                
            	});
                
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return null;
    }
    
    public static Organization saveOrganization(Organization model, String username)
    {
        if(StringUtils.isEmpty(username))
        {
            username = "system";
        }
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(model.getSys_id()))
	            {
	                HibernateUtil.getDAOFactory().getOrganizationDAO().update(model);
	            }
	            else
	            {
	                model.setSys_id(null);
	                HibernateUtil.getDAOFactory().getOrganizationDAO().persist(model);
	            }

            });
        }
        catch (Exception e)
        {
            Log.log.error("Failed to persist Organization: " + e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return model;
    }
}
