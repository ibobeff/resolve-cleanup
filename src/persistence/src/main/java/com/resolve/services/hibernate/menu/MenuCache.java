/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.menu;

import net.sf.ehcache.Cache;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.PersistenceConfiguration;
import net.sf.ehcache.config.PersistenceConfiguration.Strategy;
import net.sf.ehcache.store.MemoryStoreEvictionPolicy;

import com.resolve.persistence.util.CacheUtil;
import com.resolve.services.cache.ResolveCacheAPI;

/**
 * EhCache for User Menu
 * 
 * @author jeet.marwah
 *
 */
public class MenuCache implements ResolveCacheAPI
{
    public static final String MENU_CACHE = "MENU_CACHE";
    private static MenuCache instance;

    private MenuCache()
    {
        
    }
    
    public static MenuCache getInstance()
    {
        if (instance == null)
        {
            synchronized (MenuCache.class)
            {
                if (instance == null)
                {
                    instance = new MenuCache();
                    createMenuCache();
                }
            }
        }

        return instance;
    }
    
    /**
     * init and create the Menu cache. Initialize when the server comes up
     */
    public static void initMenuCache()
    {
        getInstance();
    }
    
    public void putCacheObj(String name, Object obj)
    {
        //verify if the cache exist first
        verifyIfCacheExist();
        
        CacheUtil.putCacheObj(MENU_CACHE, name, obj);
    }

    public Object getCacheObj(String name)
    {
//        CacheUtil.voidCacheManager();//test
        //verify if the cache exist first
        verifyIfCacheExist();

        return CacheUtil.getCacheObj(MENU_CACHE, name);
    }

    public boolean isKeyInCache(String name)
    {
        //verify if the cache exist first
        verifyIfCacheExist();

        return CacheUtil.isKeyInCache(MENU_CACHE, name);
    }

    public void removeCacheObj(String name)
    {
        //verify if the cache exist first
        verifyIfCacheExist();

        CacheUtil.removeCacheObj(MENU_CACHE, name);
    }
    
    public void invalidate()
    {
        //verify if the cache exist first
        verifyIfCacheExist();
        
        CacheUtil.invalidateCache(MENU_CACHE);
//        CacheUtil.createCache(createMenuCache());
    }
    
    private void verifyIfCacheExist()
    {
        if(!CacheUtil.doesCacheExist(MENU_CACHE))
        {
            createMenuCache();
        }
    }
    
    
    //private apis
    private static void createMenuCache()
    {
        long timeToLiveSecs = 7 * 24* 60* 60; //7 days to live
        
        Cache menuCache = new Cache(
                        new CacheConfiguration(MENU_CACHE, 10000)
                          .memoryStoreEvictionPolicy(MemoryStoreEvictionPolicy.FIFO) //first in first out policy so that user cache gets evicted after a week
//                          .maxEntriesLocalDisk(10000) //same as memory value
                          .eternal(false)
                          .timeToLiveSeconds(timeToLiveSecs)
                          .timeToIdleSeconds(0)//do not check for idleing
                          .diskExpiryThreadIntervalSeconds(0) // default is 2 mins
                          .persistence(new PersistenceConfiguration().strategy(Strategy.NONE))); //no persistence, just in RAM
        
        
        //create the cache
        CacheUtil.createCache(menuCache);
    }
}
