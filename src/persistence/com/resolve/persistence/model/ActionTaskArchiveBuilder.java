package com.resolve.persistence.model;

public class ActionTaskArchiveBuilder {

	private int UVersion;
	private String UContent;
	private String UComment;
	private boolean UIsStable;
	private String UTableId;

	public ActionTaskArchiveBuilder(int uVersion, String uContent, String uTableId) {
		UVersion = uVersion;
		UContent = uContent;
		UTableId = uTableId;
	}

	public ActionTaskArchiveBuilder setUComment(String uComment) {
		UComment = uComment;
		return this;
	}

	public ActionTaskArchiveBuilder setUIsStable(boolean uIsStable) {
		UIsStable = uIsStable;
		return this;
	}

	public ActionTaskArchive build() {
		return new ActionTaskArchive(this);
	}

	public int getUVersion() {
		return UVersion;
	}

	public String getUContent() {
		return UContent;
	}

	public String getUComment() {
		return UComment;
	}

	public boolean isUIsStable() {
		return UIsStable;
	}

	public String getUTableId() {
		return UTableId;
	}

}
