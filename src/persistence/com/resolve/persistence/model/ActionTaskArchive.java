package com.resolve.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;

import com.resolve.services.hibernate.vo.ActionTaskArchiveVO;

/**
 * Action task archive table used to store the versions for the action tasks in
 * the system.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "resolve_action_task_archive", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "u_table_sys_id", "u_version" }) })
public class ActionTaskArchive extends BaseModel<ActionTaskArchiveVO> {

	private int UVersion;

	private String UContent;

	private String UComment;

	private boolean UIsStable;

	private String UTableId;

	public ActionTaskArchive() {
		// for Hibernate initialization
	}

	ActionTaskArchive(ActionTaskArchiveBuilder builder) {
		this.UVersion = builder.getUVersion();
		this.UContent = builder.getUContent();
		this.UComment = builder.getUComment();
		this.UIsStable = builder.isUIsStable();
		this.UTableId = builder.getUTableId();
	}

	@Column(name = "u_version", nullable = false)
	public int getUVersion() {
		return UVersion;
	}

	public void setUVersion(int uVersion) {
		UVersion = uVersion;
	}

	@Column(name = "u_content", nullable = false, length = 16777215)
	@Lob
	public String getUContent() {
		return UContent;
	}

	public void setUContent(String uContent) {
		UContent = uContent;
	}

	@Column(name = "u_comment", length = 4000)
	public String getUComment() {
		return UComment;
	}

	public void setUComment(String uComment) {
		UComment = uComment;
	}

	@Column(name = "u_stable", length = 1)
	@Type(type = "yes_no")
	public boolean isUIsStable() {
		return UIsStable;
	}

	public void setUIsStable(boolean uIsStable) {
		UIsStable = uIsStable;
	}

	@Column(name = "u_table_sys_id", nullable = false)
	public String getUTableId() {
		return UTableId;
	}

	public void setUTableId(String uTableId) {
		UTableId = uTableId;
	}

	@Override
	public ActionTaskArchiveVO doGetVO() {
		return null;
	}

}
