/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rest.server;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.restlet.Component;
import org.restlet.Server;
import org.restlet.data.MediaType;
import org.restlet.data.Parameter;
import org.restlet.data.Protocol;
import org.restlet.data.Status;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
import org.restlet.util.Series;

import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class RestServer extends ServerResource
{
    private static final String RESOLVE_RESTSERVICE_ENDPOINT = "/resolve/restservice";

    // weird if these are instance variable then they
    // lose the values at runtime.
    private static String localhost = "127.0.0.1";
    private static String tempDir = null;
    private static int httpPort = 8082;
    private static int httpsPort = 8083;
    private static boolean isSSL = false;
    private static String keystore = null; // e.g.,
                                           // /opt/resolve/jdk/lib/security/cacerts
    private static String k_eystorep_assword = "changeit";
    private static String k_eyp_assword = "changeit";

    private Component component = new Component();

    // observed a somewhat interesting behavior. if we have a constructor
    // with parameters then the server fails with
    // java.lang.InstantiationException:
    // com.resolve.rest.webservice.RestServer
    public RestServer()
    {

    }

    public void init(Config config)
    {
        localhost = config.getLocalhost();
        if (StringUtils.isBlank(config.getTempDir()))
        {
            Log.log.error("TEMPORARY DIRECTORY must be provided.");
        }
        else
        {
            tempDir = FileUtils.separatorsToUnix(config.getTempDir());
        }
        httpPort = config.getHttpport();
        httpsPort = config.getHttpsport();
        isSSL = config.isSsl();
        keystore = config.getKeystore();
        k_eystorep_assword = config.getKeystorepassword();
        k_eyp_assword = config.getKeypassword();
    }

    public void start()
    {
        try
        {
            Log.log.info("Resolve REST web service starting up.");
            // if the SSL is on we'll not even turn on the plain HTTP for
            // security
            if (isSSL)
            {
                component.getDefaultHost().attach("/", RestServer.class);
                component.getDefaultHost().attach(RESOLVE_RESTSERVICE_ENDPOINT, RestServer.class);
                Server sslServer = component.getServers().add(Protocol.HTTPS, localhost, httpsPort);
                Series<Parameter> parametersHTTPS = sslServer.getContext().getParameters();

                // increase maximumSeries<Parameter>ds (RESTlet default is 10)
                parametersHTTPS.add("maxThreads", "10");
                parametersHTTPS.add("sslContextFactory", "org.restlet.ext.ssl.PkixSslContextFactory");
                parametersHTTPS.add("keystorePath", keystore);
                parametersHTTPS.add("keystorePassword", k_eystorep_assword);
                parametersHTTPS.add("keystoreType", "JKS");
                parametersHTTPS.add("keyPassword", k_eyp_assword);
                // parametersHTTPS.add("needClientAuthentication", "false");

                component.start();
                Log.log.info("Resolve REST web service started on HTTPS URL: https://" + localhost + ":" + httpsPort + RESOLVE_RESTSERVICE_ENDPOINT);
            }
            else
            {
                component.getServers().add(Protocol.HTTP, httpPort);
                component.getDefaultHost().attach("/", RestServer.class);
                component.getDefaultHost().attach(RESOLVE_RESTSERVICE_ENDPOINT, RestServer.class);
                component.start();
                Log.log.info("Resolve REST web service started on HTTP URL: http://" + localhost + ":" + httpPort + RESOLVE_RESTSERVICE_ENDPOINT);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Resolve REST web service failed to start." + e.getMessage(), e);
        }
    }

    public void stop()
    {
        Log.log.info("Resolve REST web service shutting down.");
        try
        {
            component.stop();
        }
        catch (Exception e)
        {
            // Log.log.error(e.getMessage(), e);
        }
        Log.log.info("Resolve REST web service shutdown completed.");
    }

    /**
     * This method is just for pinging.
     *
     * @return
     */
    @Get()
    public String ping()
    {
        Log.log.trace("Ping received...");
        return "Welcome to Resolve REST service";
    }

    /**
     * This method is the receiver of POST operation. This method must get a
     * parameter "action" with one of the following values (case insensitive):
     *
     * 1. fileupload - this action requies additional parameters as follows: -
     * fileToUpload is a stream of bytes, use Resolve RestAPI to upload. -
     * isFolder with a value TRUE if the upload was a folder 2. filedelete -
     * this action requies additional parameters as follows: - filetodelete is
     * the absolute path in the resolve tmp folder.
     *
     * @param entity
     * @return
     * @throws Exception
     */
    @Post
    public Representation accept(Representation entity)
    {
        Log.log.trace("RestServer.accept(): POST received...");

        Representation rep = null;

        try
        {
            if (entity != null)
            {
                Log.log.trace("RestServer.accept(): entity is not null");
                // if(entity.getMediaType().isCompatible(MediaType.APPLICATION_JSON))
                if (entity.getMediaType().isCompatible(MediaType.TEXT_PLAIN))
                {
                    Log.log.trace("RestServer.accept(): TEXT_PLAIN submit.");
                    // this is in the form
                    // "action=filedelete&isFolder=falsefile:
                    String request = entity.getText();
                    Map<String, String> params = StringUtils.urlToMap(request);
                    if (params != null && params.size() > 0 && params.containsKey("action"))
                    {
                        if ("filedelete".equalsIgnoreCase(params.get("action")))
                        {
                            deleteFile(params);
                            rep = new StringRepresentation("File deleted successfully.", MediaType.TEXT_PLAIN);
                        }
                    }
                    else
                    {
                        // this means that the caller wants us to pass the
                        // incoming paremeter to the method
                        // of the class. The classname must be fully qualified
                        // with package name
                        // e.g., com.resolve.rsmgmt.MMCP
                        if (params.containsKey(Constants.REST_PARAM_CLASSMETHOD))
                        {
                            String classMethodName = params.get(Constants.REST_PARAM_CLASSMETHOD);
                            params.remove(Constants.REST_PARAM_CLASSMETHOD);
                            // Implement the reflection code here
                            String status = executeMethod(classMethodName, params);
                            if (StringUtils.isNotBlank(status))
                            {
                                rep = new StringRepresentation(status, MediaType.TEXT_PLAIN);
                            }
                            else
                            {
                                rep = new StringRepresentation("Successfully executed " + classMethodName, MediaType.TEXT_PLAIN);
                            }
                        }
                        else
                        {
                            Log.log.warn("RestServer.accept(): ERROR: Invalid POST request. parameters: " + params);
                            rep = new StringRepresentation("ERROR: Invalid POST request.", MediaType.TEXT_PLAIN);
                            setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
                        }
                    }
                }
                else if (MediaType.MULTIPART_FORM_DATA.equals(entity.getMediaType(), true))
                {
                    Log.log.trace("RestServer.accept(): MULTIPART_FORM_DATA submit.");
                    rep = acceptFileUpload(rep);
                }
            }
            else
            {
                // POST request with no entity.
                Log.log.warn("POST request with no entity.");
                rep = new StringRepresentation("ERROR: POST request with no entity.", MediaType.TEXT_PLAIN);
                setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
            }
        }
        catch (Exception e)
        {
            // POST request with no entity.
            Log.log.error(e.getMessage(), e);
            rep = new StringRepresentation("ERROR: " + e.getMessage(), MediaType.TEXT_PLAIN);
            setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
        }
        return rep;
    }

    /**
     * There is a RESTRICTION here, due to serialization issue over the wire, this class
     * method must always return a String.
     *
     * @param classMethodName
     * @param params
     * @return
     */
    private String executeMethod(String classMethodName, Map<String, String> params)
    {
        String result = "";

        try
        {
            if(StringUtils.isNotBlank(classMethodName))
            {
                String methodName = classMethodName.substring(classMethodName.lastIndexOf('.') + 1);
                String className = classMethodName.substring(0, classMethodName.lastIndexOf('.'));
                Class clazz = Class.forName(className);
                System.out.println(clazz.getName());
                System.out.println(clazz.getPackage().getName());
                Method method = clazz.getDeclaredMethod(methodName, Map.class);
                Object response = method.invoke(null, params);
                result = (String) response;
            }
            else
            {
                result = "classMethodName parameter must be provided";
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result = e.getMessage();
        }

        return result;
    }

    private void deleteFile(Map<String, String> params) throws Exception
    {
        File file = null;
        if (params.containsKey("filetodelete"))
        {
            file = new File(params.get("filetodelete"));
            String fileName = FileUtils.separatorsToUnix(file.getAbsolutePath());

            // make sure that the caller isn't trying to delete any arbitrary
            // file in the
            // server
            if (!fileName.toLowerCase().contains(tempDir.toLowerCase()))
            {
                throw new Exception("This file/dir cannot be deleted as it's out of scope.");
            }

            // we'll have to wipe out the random directory.
            int endIndex = FileUtils.separatorsToUnix(file.getAbsolutePath()).lastIndexOf("/");
            String randomFolderName = file.getAbsolutePath().substring(0, endIndex);
            Log.log.debug("Deleting the folder: " + randomFolderName);
            file = new File(randomFolderName);
            // the length check protects hacker with /opt/resolve/tmp/abc which
            // would
            // have removed the entire tmp folder.
            if (file.exists() && randomFolderName.length() > tempDir.length())
            {
                FileUtils.deleteDirectory(file);
            }
            else
            {
                throw new Exception("Invalid file/dir provided for deletion.");
            }
        }
    }

    private Representation acceptFileUpload(Representation rep) throws FileUploadException, Exception, IOException
    {
        // The Apache FileUpload project parses HTTP requests which conform to
        // RFC 1867,
        // "Form-based File Upload in HTML". That is, if an HTTP request is
        // submitted
        // using the POST method, and with a content type of
        // "multipart/form-data", then
        // FileUpload can parse that request, and get all uploaded files as
        // FileItem.

        // 1. Create a factory for disk-based file items
        DiskFileItemFactory factory = new DiskFileItemFactory();

        factory.setSizeThreshold(1000240);

        // 2. Create a new file upload handler based on the Restlet FileUpload
        // extension
        // that will parse Restlet requests and generates FileItems.
        RestletFileUpload upload = new RestletFileUpload(factory);

        // 3. Request is parsed by the handler which generates a list of
        // FileItems
        List<FileItem> items = upload.parseRequest(getRequest());

        String action = null;
        // First find the action
        for (FileItem fileItem : items)
        {
            if (Log.log.isTraceEnabled())
            {
                Log.log.trace("FIELD name: " + fileItem.getFieldName() + ", VALUE: " + fileItem.getString());
            }
            if ("action".equalsIgnoreCase(fileItem.getFieldName()))
            {
                action = fileItem.getString();
                break;
            }
        }

        if (action == null)
        {
            rep = new StringRepresentation("ERROR: Must provide parameter name \"action\"", MediaType.TEXT_PLAIN);
        }
        else if ("fileupload".equalsIgnoreCase(action))
        {
            boolean isFolder = false;
            String saveFileName = null;
            // have a random folder
            File destination = new File(tempDir + "/" + UUID.randomUUID().toString());
            for (FileItem fileItem : items)
            {
                // Log.log.trace("FIELD name: " + fileItem.getFieldName() +
                // ", VALUE: " + fileItem.getString());
                if ("fileToUpload".equalsIgnoreCase(fileItem.getFieldName()))
                {
                    try
                    {
                        destination.mkdir();
                        saveFileName = FileHandler.saveFile(destination, fileItem);
                    }
                    catch (RestServiceException e)
                    {
                        throw new Exception(e.getMessage());
                    }
                }
                if ("isFolder".equalsIgnoreCase(fileItem.getFieldName()))
                {
                    isFolder = Boolean.parseBoolean(fileItem.getString());
                }
            }

            if (isFolder && StringUtils.isNotBlank(saveFileName))
            {
                // unzip the folder
                File savedFile = new File(saveFileName);
                FileUtils.unzip(savedFile, destination);
                FileUtils.deleteQuietly(savedFile);
                String folderName = FileUtils.removeExtension(saveFileName);
                rep = new StringRepresentation(folderName, MediaType.TEXT_PLAIN);
            }
            else
            {
                rep = new StringRepresentation(saveFileName, MediaType.TEXT_PLAIN);
            }
        }
        return rep;
    }
}
