/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rest.server;

/**
 * Config class to carry the configuration for REST web service engine.
 *
 */
public class Config
{
    private String localhost = "127.0.0.1";
    private boolean active = true;
    private int httpport = 8082;
    private int httpsport = 8083;
    private String username = "resolve";
    private String password = "resolve";
    private String tempDir = "/opt/resolve/tmp";
    private boolean ssl = false;
    private String keystore = "/opt/resolve/tmp"; //e.g., /opt/resolve/jdk/lib/security/cacerts
    private String keystorepassword = "changeit";
    private String keypassword = "changeit";

    public Config(String localhost, boolean active, int httpport, int httpsport, String username, String password, String tempDir, boolean ssl, String keystore, String keystorepassword, String keypassword)
    {
        this.localhost = localhost;
        this.active = active;
        this.httpport = httpport;
        this.httpsport = httpsport;
        this.username = username;
        this.password = password;
        this.tempDir = tempDir;
        this.ssl = ssl;
        this.keystore = keystore;
        //this.keystore = "c:/project/resolve3/dist/jre/security/lib/cacerts";
        this.keystorepassword = keystorepassword;
        this.keypassword = keypassword;
    } // ConfigRestWebservice
    public String getLocalhost()
    {
        return localhost;
    }

    public void setLocalhost(String localhost)
    {
        this.localhost = localhost;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public int getHttpport()
    {
        return httpport;
    }

    public void setHttpport(int httpport)
    {
        this.httpport = httpport;
    }

    public int getHttpsport()
    {
        return httpsport;
    }

    public void setHttpsport(int httpsport)
    {
        this.httpsport = httpsport;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getTempDir()
    {
        return tempDir;
    }

    public void setTempDir(String tempDir)
    {
        this.tempDir = tempDir;
    }

    public boolean isSsl()
    {
        return ssl;
    }

    public void setSsl(boolean ssl)
    {
        this.ssl = ssl;
    }

    public String getKeystore()
    {
        return keystore;
    }

    public void setKeystore(String keystore)
    {
        this.keystore = keystore;
    }

    public String getKeystorepassword()
    {
        return keystorepassword;
    }

    public void setKeystorepassword(String keystorepassword)
    {
        this.keystorepassword = keystorepassword;
    }

    public String getKeypassword()
    {
        return keypassword;
    }

    public void setKeypassword(String keypassword)
    {
        this.keypassword = keypassword;
    }
} // Config