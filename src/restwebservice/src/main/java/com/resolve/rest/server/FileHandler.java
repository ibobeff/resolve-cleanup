/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rest.server;

import java.io.File;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang3.StringUtils;

import com.resolve.util.Log;

public final class FileHandler
{
    public static String saveFile(File destinationFolder, FileItem fileItem) throws RestServiceException
    {
        String result = null;

        String fileName = fileItem.getName();

        if (StringUtils.isBlank(fileName))
        {
            // cannot happen
            Log.log.error("File name couldn't be found from the post to the URL.");
            throw new RestServiceException("ERROR: File name couldn't be found from the post to the URL.");
        }

        File file = new File(destinationFolder + "/" + fileName);
        try
        {
            fileItem.write(file);
            result = file.getAbsolutePath();
            //"SUCCESS: The file is successfully uploaded to " + fileLocation + "/" + fileName;
            Log.log.debug(result);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result = "ERROR: " + e.getMessage();
            throw new RestServiceException(e.getMessage(), e);
        }
        return result;
    }
}
