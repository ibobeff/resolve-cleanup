/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rest.server;

@SuppressWarnings("serial")
public class RestServiceException extends Exception
{
    private static final long serialVersionUID = 5339195922794253690L;

	public RestServiceException(String message)
    {
        super(message);
    }

    public RestServiceException(String message, Throwable t)
    {
        super(message, t);
    }
}
