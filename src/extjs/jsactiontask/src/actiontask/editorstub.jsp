<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title id="page-title">Resolve Template Project</title>
    <link rel="stylesheet" type="text/css" href="/resolve/css/font-awesome.min.css"/>
    <style>
        /*Scrollbar Style*/
        ::-webkit-scrollbar {
        background: transparent;
            overflow: visible;
            width: 18px;
        }
        ::-webkit-scrollbar-thumb {
            background-color: rgba(0, 0, 0, 0.2);
            border: solid #fff;
        }
        ::-webkit-scrollbar-thumb:hover {
            background: rgba(0, 0, 0, 0.8);
        }
        ::-webkit-scrollbar-thumb:horizontal {
            border-width: 4px 6px;
            min-width: 20px;
        }
        ::-webkit-scrollbar-thumb:vertical {
            border-width: 6px 4px;
            min-height: 20px;
        }
        ::-webkit-scrollbar-track-piece {
            background-color: #fff;
        }
        ::-webkit-scrollbar-corner {
            background: transparent;
        }
        ::-webkit-scrollbar-thumb {
            background-color: #ADADAD;
            -webkit-box-shadow: inset 1px 1px 0 rgba(0,0,0,0.10),inset 0 -1px 0 rgba(0,0,0,0.07);
        }
        ::-webkit-scrollbar-thumb:hover {
            background-color: #999;
        }
        body,html{
            height:100%;
            width:100%;           
            line-height: 1.5;  
            font-size: 14px;        
            margin:0;
            background-color : white;        
        }       
        .parser-content{
            color:#777;
            padding : 0 8px; 
            margin: 0;
            outline-width: 0; 
            height: 100%;   
        }
        span[boundary]{
            color:#DCD5D5 !important;
        }
        span[unselectable]{
            cursor : default;
            -webkit-touch-callout: none; /* iOS Safari */
            -webkit-user-select: none;   /* Chrome/Safari/Opera */
            -khtml-user-select: none;    /* Konqueror */
            -moz-user-select: none;      /* Firefox */
            -ms-user-select: none;       /* Internet Explorer/Edge */
        }
        span[marker] {
            font-size: 15px;
        }
        span[marked] {
            border-bottom: 1px solid orange;
            cursor: pointer;         
        }
        span[marked].markup-over{
            border-bottom: 1px solid blue;
        }
        span[marked][action='capture']{
            border : none;
            color : white;
            cursor: default;
        }
         span[marked][type='delimiter']{
            border : none;           
            cursor: default;
        }             
        span[marked][type='endOfLine']{
            color : orange;
            font-size: 15px;
        }
        span[marked][type='parseStart']{
            color : orange;
            border-bottom: none;  
            border-left: 4px solid orange;
        }
         span[marked][type='parseEnd']{
            color : orange;
            border-bottom: none;  
            border-right: 4px solid orange;
        }
        span[marked][activated]{
            border-bottom: 1px solid blue;  
            color : blue;        
        }   
        /*XML*/
        .emphasize-xml .xmlSp[ignore]{
            border-color: white !important;
            cursor: default;
        }
        .emphasize-xml .xmlSp{
            border-bottom : 1px solid orange;
            cursor : pointer;
        }
        .emphasize-xml.text{           
            cursor : pointer;
            border-bottom : 1px solid orange;
        }
        .tagName{
            color: #006699;
            font-weight: bold;        
        }
        .attrName{
            color:gray ;
        }
        .attrValue{
            color:blue;
        } 
        .text{           
            color : green;
        }
        .tag{
            color:black;
        }  
        .emphasize-left{
            border-left:1px solid #99CCFF !important;
        }
        .emphasize-center{
            background-color: #1F6DFF !important;
            color:white;
        }
        .emphasize-right{
            border-right:1px solid #99CCFF !important;
        }
        .emphasize-right .return:last-of-type{
            border-right: 1px solid #99CCFF;
        }
        .mark{
            font-weight: bold;
        }
        .beginParse{
            color: #C674FC;   
            background-color: #FDF0FF;
            border-left: 4px solid #C674FC;
        }
        .beginParse-mixed{
            color: #C674FC;   
            background-color: #FDF0FF;
        }
        .endParse{
            color: #C674FC;   
            background-color: #FDF0FF;
            border-right: 4px solid #C674FC;
        }
        .endParse .return:last-of-type{
            color: #C674FC !important;
            background-color: #FDF0FF !important;
            border-right: 4px solid #C674FC !important;
        }
        .type{
            border-bottom: 1px solid #FF6600;
        }
        .parseFromEndOfLine{
            color: #C674FC;
            font-size: small;
        }
        .stopParseAtStartOfLine{
            color: #C674FC;   
            font-size: small;
        }       
        .column{
            background-color: #ADD6F7;
        }
        .overlap-left{
            border-left:2px dashed #99CCFF !important;
        }
        .overlap-center{
            border-top: 2px dashed #99CCFF !important;
            border-bottom: 2px dashed #99CCFF !important;
        }
        .overlap-right{
            border-right:2px dashed #99CCFF !important;
        }
        .overlap-right .return:last-child{
            border-right: 2px dashed #99CCFF;
        }
            
        .attribute{
            color:black;
        }
       
       
        .cdata{
            color:black;
        }
        .cdata::before{
            color:#999;
            content: '<![CDATA['
        }
        .cdata::after{
            color:#999;
            content: ']]>';
        }       
        .rootText{
            color:#999;
            text-decoration: line-through;
        }
        .parserHintText {
            text-align: center;
            font-size: 2.5em;
            font-family: sans-serif;
            font-weight: bold;
            color: #e2e2e2;
            padding-top: 15%;
            -webkit-touch-callout: none; /* iOS Safari */
            -webkit-user-select: none; /* Safari */
            -khtml-user-select: none; /* Konqueror HTML */
            -moz-user-select: none; /* Firefox */
            -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none; /* Non-prefixed
        }
    </style>    
</head>
<body></body>
</html>
