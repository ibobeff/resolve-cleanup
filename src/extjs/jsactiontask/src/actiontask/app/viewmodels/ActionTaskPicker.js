glu.defModel('RS.actiontask.ActionTaskPicker', {
    title: '',
    allTaskTabIsPressed: true,
    fromAutomationTabIsPressed : false,
    dumper: null,
    activeTab : 0,
    multiSelection : false, 
    currentDocName : '',
    allTask : {
        mtype : 'AllTask'
    },
    fromAutomation : {
        mtype : 'FromAutomation'
    },
    init : function(){
        this.set('title', this.localize('selectTitle'));
        this.allTask.set('multiSelection', this.multiSelection);    
        this.fromAutomation.set('multiSelection', this.multiSelection);
        this.fromAutomation.set('automationSearch', this.currentDocName ? this.currentDocName : '');   
    },
    dump: function() {
        var actiontaskSelection = this.allTaskTabIsPressed ? this.allTask.actionTaskGridStoreSelections : this.fromAutomation.actionTaskGridStoreSelections;
        if (!this.dumper)
            return
        if (Ext.isFunction(this.dumper))
            this.dumper(actiontaskSelection);
        else {
            var scope = this.dumper.scope || this;
            this.dumper.dump.call(scope, actiontaskSelection);
        }
        this.doClose();
    },  
    cancel: function() {
        this.doClose();
    }, 
    allTaskTab : function(){
       this.displayTab("allTask");
    },
    fromAutomationTab : function(){
        this.displayTab("fromAutomation");
    },
    displayTab : function(tabName){
        this.set('allTaskTabIsPressed', tabName == "allTask");
        this.set('fromAutomationTabIsPressed', tabName == "fromAutomation");
        this.set('activeTab', tabName == "allTask" ? 0 : 1);
    }   
})
glu.defModel('RS.actiontask.AllTask',{
    mixins: ['Main'],
    multiSelection : false,  
    actionTaskGridStore: {
        mtype: 'store',
        sorters: ['uname'],
        fields: ['id', 'ufullName','uname', 'uactive', 'unamespace', 'usummary', 'udescription', 'resolveActionInvoc'].concat(RS.common.grid.getSysFields()),
        remoteSort: true,
        proxy: {
            type: 'ajax',
            url: '/resolve/service/actiontask/list',
            reader: {
                type: 'json',
                root: 'records'
            },
            listeners: {
                exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
            }
        }
    },
    columns: [],
    actionTaskTreeMenuPathColumns: [],
    actionTaskTreeModuleColumns: [],

       init: function() {      
        this.set('columns', [{
            header: '~~name~~',
            dataIndex: 'uname',
            filterable: true,
            width: 250
        }, {
            header: '~~active~~',
            dataIndex: 'uactive',
            filterable: true
        }, {
            header: '~~module~~',
            dataIndex: 'unamespace',
            filterable: true
        }, {
            header: '~~summary~~',
            dataIndex: 'usummary',
            filterable: true,
            renderer: RS.common.grid.plainRenderer(),
            flex: 1
        }].concat(RS.common.grid.getSysColumns()))

        this.set('actionTaskTreeMenuPathColumns', [{
            xtype: 'treecolumn',
            dataIndex: 'name',
            text: this.localize('menuPath'),
            flex: 1
        }])

        this.set('actionTaskTreeModuleColumns', [{
            xtype: 'treecolumn',
            dataIndex: 'name',
            text: this.localize('module'),
            flex: 1
        }])

        this.actionTaskGridStore.on('load', function(store, rec, suc) {
            if (!suc) {
                this.doClose();
            }
        }, this);

        this.set('menuPathSelected', this.actionTaskTreeMenuStore.getRootNode());
        this.set('moduleSelected', this.actionTaskTreeModuleStore.getRootNode());
        this.attachReqParam();
        this.menuPath();
        this.actionTaskGridStore.load();
    },

    actionTaskGridStoreSelections: [],
    selectedTreeNodeId: '',
    searchDelimiter: '',
    activeItem : 0,
   
    menuPath: function() {
        this.set('searchDelimiter', '/');
        this.set('activeItem', 0);
        if (!this.menuPathSelected || this.menuPathSelected.get('name') == 'All')
            with(this.actionTaskTreeMenuStore.getRootNode()) {
                collapse();
                expand();
            }
        this.actionTaskGridStore.load();
    },

    module: function() {
        this.set('searchDelimiter', '.')
        this.set('activeItem', 1);
        if (!this.moduleSelected || this.moduleSelected.get('name') == 'All')
            with(this.actionTaskTreeModuleStore.getRootNode()) {
                collapse();
                expand();
            }
        this.actionTaskGridStore.load();
    },
    editActionTask: function(id) {
        clientVM.handleNavigation({
            modelName: 'RS.actiontask.ActionTask',
            target: '_blank',
            params: {
                id: id
            }
        })
    }
})
glu.defModel('RS.actiontask.FromAutomation',{  
    columns: [],
    multiSelection : false,
    automationSearch : '',
    automationSearchIsValid : true, 
    actionTaskGridStoreSelections: [], 
    prevSelections : [],
    automationStore: {
        mtype: 'store',
        fields: ['ufullname'],
        proxy: {
            type: 'ajax',
            url: '/resolve/service/wikiadmin/listRunbooks',           
            reader: {
                type: 'json',
                root: 'records'
            },
            listeners: {
                exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
            }
        }
    },
    actionTaskGridStore: {
        mtype: 'store',
        sorters: ['uname'],
        fields: ['id','ufullName', 'uname', 'uactive', 'unamespace', 'usummary', 'udescription', 'resolveActionInvoc'].concat(RS.common.grid.getSysFields()),
        remoteSort: true,
        proxy: {
            type: 'ajax',
            url: '/resolve/service/wikiadmin/listABTasks',
            reader: {
                type: 'json',
                root: 'records'
            },
            listeners: {
                exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
            }
        }
    },
    when_automation_changed : {
        on : ['automationSearchChanged'],
        action : function(){
            this.validateAutomation(this.automationSearch);
        }
    },
    init: function() {      
        this.set('columns', [{
            header: '~~name~~',
            dataIndex: 'uname',
            filterable: true,
            width: 250
        }, {
            header: '~~active~~',
            dataIndex: 'uactive',
            filterable: true
        }, {
            header: '~~module~~',
            dataIndex: 'unamespace',
            filterable: true
        }, {
            header: '~~summary~~',
            dataIndex: 'usummary',
            filterable: true,
            renderer: RS.common.grid.plainRenderer(),
            flex: 1
        }].concat(RS.common.grid.getSysColumns()))      
        this.actionTaskGridStore.on('beforeload',function(store,op){
            op.params = op.params || {};
            Ext.apply(op.params, {
                name : this.automationSearch
            })
        },this);
        this.automationStore.on('beforeload', function(store, op) {
            op.params = op.params || {};
            Ext.apply(op.params, {
                filter: Ext.encode([{
                    field: 'ufullname',
                    condition: 'contains',
                    type: 'auto',
                    value: op.params.query || this.automationSearch
                }])
            });          
        }, this);
        this.automationStore.on('load', function() {
            this.validateAutomation(this.automationSearch);          
        }, this)
        this.automationStore.load();
    },
    validateAutomation: function(automation) {
        var r = this.automationStore.findRecord('ufullname', automation, 0, false, false, true);
        if (!r){
            this.set('automationSearchIsValid', this.localize('invalidAutomation'));        
            this.actionTaskGridStore.removeAll();
            //this.actionTaskGridStore.loadData(this.actionTaskGridStoreSelections);           
        }
        else {
            this.set('automationSearchIsValid', true);
            this.actionTaskGridStore.load();
        }  
    },
    editActionTask: function(id) {
        clientVM.handleNavigation({
            modelName: 'RS.actiontask.ActionTask',
            target: '_blank',
            params: {
                id: id
            }
        })
    },  
})