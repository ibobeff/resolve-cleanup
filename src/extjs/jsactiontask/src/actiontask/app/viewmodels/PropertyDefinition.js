glu.defModel('RS.actiontask.PropertyDefinition', {

	wait: false,

	id: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrganizationName: '',

	uname: '',
	unameIsValid$: function() {
		return this.uname && (/^[\w_]+$/.test(this.uname) || /^[\w_][\w_\.\s]*[\w_]$/.test(this.uname)) ? true : this.localize('invalidName');
	},
	umodule: '',
	umoduleIsValid$: function() {
		return this.umodule && (/^[\w_]+$/.test(this.umodule) || /^[\w_][\w_\.\s]*[\w_]$/.test(this.umodule)) ? true : this.localize('invalidModule');
	},
	utype: 'Plain',
	uvalue: '',
	// uvalueIsValid$: function() {
	// 	return !this.uvalue || /^\s*$/.test(this.uvalue) || (/^[\w_]+$/.test(this.uvalue) || /^[\w_][\w_\.\s]*[\w_]$/.test(this.uvalue)) ? true : this.localize('invalidValue');
	// },
	fields: ['uname', 'umodule', 'utype', 'uvalue'],

	moduleStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	typeStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	isEncrypt$: function() {
		return this.utype.toUpperCase() == 'Encrypt'.toUpperCase();
	},

	init: function() {
		this.typeStore.add([{
			name: 'Plain',
			value: 'Plain'
		}, {
			name: 'Encrypt',
			value: 'Encrypt'
		}]);
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/atproperties/modules',
			success: function(resp) {
				this.set('wait', false);
				var respData = Ext.JSON.decode(resp.responseText);
				if (!respData.success) {
					this.parentVM.clientVM.displayError(this.localize('GetModulesErr') + '[' + respData.message + ']');
					return;
				}
				Ext.each(respData.records, function(r) {
					this.moduleStore.add({
						name: r,
						value: r
					})
				}, this)
			},
			failure: function(resp) {
				this.set('wait', false);
				this.parentVM.clientVM.displayError(this.localize('ServerError') + '[' + resp.status + ']');
			}
		})
		if (this.id)
			this.loadRecord();
	},

	loadRecord: function() {
		this.set('wait', true)
		this.ajax({
			url: '/resolve/service/atproperties/get',
			params: {
				id: this.id
			},

			success: function(resp) {
				this.set('wait', false);
				var respData = Ext.JSON.decode(resp.responseText);
				if (!respData.success)
					this.parentVM.clientVM.displayError(this.localize('GetPropertyErr') + '[' + respData.message + ']');
				
				//Handle this "uvalue" specially check RBA-12758
				var uvalue = respData.data.uvalue;
				respData.data.uvalue = uvalue.replace(/\r/g, '\n');
				this.loadData(respData.data);

			},
			failure: function(resp) {
				this.set('wait', false);
				this.parentVM.clientVM.displayError(this.localize('ServerError') + '[' + resp.status + ']');
			}
		})
	},

	userDateFormat$: function() {
		return this.parentVM.clientVM.getUserDefaultDateFormat()
	},

	saveRecord: function() {
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/atproperties/save',
			jsonData: this.asObject(),
			success: function(resp) {
				this.set('wait', false);
				var respData = Ext.JSON.decode(resp.responseText);
				if (!respData.success)
					this.parentVM.clientVM.displayError(this.localize('SavePropertyDefintionErr') + '[' + respData.message + ']');
				else {
					this.parentVM.clientVM.displaySuccess(this.localize('SavePropertyDefintionSuc'));
					this.loadData(respData.data);
				}
				this.close();
				this.parentVM.store.load();
			},

			failure: function(resp) {
				this.set('wait', false);
				this.parentVM.clientVM.displayError(this.localize('ServerError') + '[' + resp.status + ']');
			}
		})
	},

	save: function(exitAfterSave) {
		this.saveRecord();
	},
	saveIsEnabled$: function() {
		return !this.wait && this.isDirty && this.isValid;
	},
	close: function() {
		this.doClose();
	}
})