glu.defModel('RS.actiontask.PropertyDefinitionsPicker', {

	height$: function() {
		return Ext.isIE8m ? 600 : '80%'
	},
	width$: function() {
		return Ext.isIE8m ? 800 : '80%'
	},

	displayName: '',

	columns: null,

	stateId: 'rsactiontaskpropertiesgrid',

	store: {
		mtype: 'store',

		fields: ['uname', 'utype', 'uvalue', 'umodule'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'uname',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/atproperties/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	recordsSelections: [],

	init: function() {
		this.set('displayName', this.localize('displayName'));

		this.set('columns', [{
			header: '~~uname~~',
			filterable: true,
			sortable: true,
			dataIndex: 'uname',
			flex: 3
		}, {
			header: '~~utype~~',
			dataIndex: 'utype',
			filterable: true,
			sortable: true,
			width: 100
		}, {
			header: '~~uvalue~~',
			filterable: false,
			sortable: false,
			dataIndex: 'uvalue',
			flex: 1,
			renderer: function(value, row) {
				var record = row.record;
				var type = record.get('utype');
				if (type.toUpperCase() == 'PLAIN')
					return RS.common.grid.plainRenderer()(value);
				return "*****";
			}
		}, {
			header: '~~umodule~~',
			dataIndex: 'umodule',
			filterable: true,
			sortable: true,
			width: 120
		}].concat((function() {
			var cols = RS.common.grid.getSysColumns();
			Ext.each(cols, function(c) {
				if (c.dataIndex == 'sysUpdatedOn') {
					c.hidden = false;
					c.initialShow = true;
				}
			})
			return cols;
		})()));

		this.store.load()
	},

	select: function() {
		if (Ext.isFunction(this.dumper)) this.dumper.call(this, this.recordsSelections)
		this.doClose()
	},
	selectIsEnabled$: function() {
		return this.recordsSelections.length == 1
	},
	cancel: function() {
		this.doClose()
	}
})