glu.defModel('RS.actiontask.Main', {

    mock: false,

    reqCount: 0,

    assignedToUsername: '',

    actionTaskGridStore: {
        mtype: 'store',
        fields: ['uname', {
            name: 'uactive',
            type: 'bool'
        }, 'unamespace', 'usummary', {
            name: 'assignedTo.UUserName',
            convert: function(v, r) {
                return r.raw.assignedTo ? r.raw.assignedTo.uuserName : '';
            }
        }].concat(RS.common.grid.getSysFields()),
        remoteSort: true,
        sorters: [{
            property: 'uname',
            direction: 'ASC'
        }],
        proxy: {
            type: 'ajax',
            url: '/resolve/service/actiontask/list',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },

    actionTaskTreeMenuStore: {
        mtype: 'treestore',
        fields: ['id', 'name'],
        proxy: {
            type: 'memory'
        },
        root: {
            name: 'All',
            id: 'root_menu_id',
            expanded: true
        }
    },

    actionTaskTreeModuleStore: {
        mtype: 'treestore',
        fields: ['id', 'name'],
        proxy: {
            type: 'memory'
        },
        root: {
            name: 'All',
            id: 'root_module_id',
            expanded: true
        }
    },

    columns: [],
    actionTaskTreeMenuPathColumns: [],
    actionTaskTreeModuleColumns: [],

    loadTreeLayer: function(node) {
        var path = node.getPath('name').substring(5);
        if (!node.isLeaf() && path)
            path += '/';
        if (this.activeItem == 1)
            path = path.replace(/\//g, '\.');
        var filter = '';
        if (this.assignedToUsername)
            filter = Ext.JSON.encode([{
                "field": "assignedTo.UUserName",
                "type": "auto",
                "condition": "equals",
                "value": this.assignedToUsername
            }])
        this.ajax({
            url: '/resolve/service/actiontask/treelist',
            params: {
                node: path,
                delim: this.activeItem == 0 ? '/' : '\.',
                filter: filter
            },
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    clientVM.displayError(this.localize(this.activeItem == 1 ? 'ListModuleTreeError' : 'ListMenuTreeError') + '[' + respData.message + ']');
                    return;
                }
                node = node || (this.activeItem == 0 ? this.actionTaskTreeMenuStore.getRootNode() : this.actionTaskTreeModuleStore.getRootNode());
                path = node.getPath('name');
                if (respData.records.length == 0)
                    node.set('leaf', true);
                else
                    Ext.each(respData.records, function(r) {
                        delete r.children;
                        r.id = path + '/' + r.id;
                        node.appendChild(r);
                    });
            },
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
        });
    },
    inited: false,
    activate: function() {
        clientVM.setWindowTitle(this.localize('windowTitle'));
        this.set('menuPathSelected', this.menuPathSelected || this.actionTaskTreeMenuStore.getRootNode());

        if (!this.inited) {
            this.menuPath();
            this.set('inited', true);
        }
        else
            this.actionTaskGridStore.load();
    },

    init: function() {
        if (this.mock)
            this.backend = RS.actiontask.createMockBackend(true)
        this.set('columns', [{
            header: '~~name~~',
            dataIndex: 'uname',
            filterable: true,
            width: 250,
            renderer: RS.common.grid.plainRenderer()
        }, {
            header: '~~module~~',
            dataIndex: 'unamespace',
            filterable: true
        }, {
            header: '~~active~~',
            dataIndex: 'uactive',
            filterable: true,
            renderer: RS.common.grid.booleanRenderer(),
            width: 60,
            align : 'center'
        }, {
            header: '~~summary~~',
            dataIndex: 'usummary',
            filterable: true,
            flex: 1,
            renderer: RS.common.grid.plainRenderer()
        }, {
            header: '~~assignedToUsername~~',
            dataIndex: 'assignedTo.UUserName',
            filterable: true,
            hideable: false,
            hidden: false
        }].concat((function(cols) {
            Ext.each(cols, function(c) {
                if (c.dataIndex == 'sysUpdatedOn') {
                    c.initialShow = true;
                    c.hidden = false;
                }
            });
            return cols;
        })(RS.common.grid.getSysColumns())))

        this.set('actionTaskTreeMenuPathColumns', [{
            xtype: 'treecolumn',
            dataIndex: 'name',
            text: 'Menu Path',
            flex: 1
        }])

        this.set('actionTaskTreeModuleColumns', [{
            xtype: 'treecolumn',
            dataIndex: 'name',
            text: 'Module',
            flex: 1
        }])

        this.attachReqParam();
        this.actionTaskGridStore.on('load', function(store, records, success, eOpts) {
            // Update selections
            if (success && records.length > 0) {
                for (var i=0; i<this.actionTasksSelections.length; i++) {
                    var newRecord = store.getById(this.actionTasksSelections[i].get('id'));
                    if (newRecord) {
                        this.actionTasksSelections[i] = newRecord;
                    }
                }
            }
        }, this);
    },

    getNodePath: function() {
        var selected = this.activeItem == 0 ? this.menuPathSelected : this.moduleSelected;
        var path = selected ? selected.getPath('name').substring(5) : '';
        if (path) {
            path += !selected.isLeaf() ? '/' : '';
            if (this.activeItem == 1)
                path = path.replace(/\//g, '\.');
            else
                path = '/' + path;
        } else {
            path = this.activeItem == 0 ? 'root_menu_id' : 'root_module_id'
        }
        return path;
    },
    attachReqParam: function() {
        this.actionTaskGridStore.on('beforeload', function(store, operation, eOpts) {
            operation.params = operation.params || {};
            var path = this.getNodePath();
            Ext.apply(operation.params, {
                delim: this.activeItem == 0 ? '/' : '\.',
                id: path
            });
            this.set('reqCount', this.reqCount + 1);
        }, this)

        this.actionTaskGridStore.on('load', function(store, rec, suc) {
			/*
            if (!suc)
                clientVM.displayError(this.localize('LoadActionTaskErr'));
			*/
            this.set('reqCount', this.reqCount - 1);
        }, this);

        this.actionTaskTreeMenuStore.on('beforeexpand', function(node) {
            node.removeAll();
            this.loadTreeLayer(node);
        }, this);

        this.actionTaskTreeModuleStore.on('beforeexpand', function(node) {
            node.removeAll();
            this.loadTreeLayer(node);
        }, this);
    },

    actionTasksSelections: [],

    allSelected: false,
    selectAllAcrossPages: function(selectAll) {
        this.set('allSelected', selectAll)
    },

	selectedInResolveNS: false,
    selectionChanged: function(selected) {
		var selectedInResolveNS = false;
		for (var i=0; i<selected.length; i++) {
			if (selected[i].get('unamespace').toLowerCase() == 'resolve') {
				selectedInResolveNS = true;
				break;
			}
		}
		this.set('selectedInResolveNS', selectedInResolveNS);
        this.selectAllAcrossPages(false)
    },

    createActionTask: function() {
        clientVM.handleNavigation({
            modelName: 'RS.actiontask.ActionTask'
        });
    },

    createActionTaskIsEnabled$: function() {
        return this.reqCount == 0;
    },

    deleteActionTasks: function() {
        this.message({
            title: this.localize('DeleteTitle'),
            msg: this.localize(this.actionTasksSelections.length > 1 ? 'DeletesMsg' : 'DeleteMsg', {
                num: this.allSelected ? this.actionTaskGridStore.getTotalCount() : this.actionTasksSelections.length
            }),
            buttons: Ext.MessageBox.YESNO,
            buttonText: {
                'yes': this.localize('deleteActionTasks'),
                'no': this.localize('cancel')
            },
            scope: this,
            fn: function(btn) {
                if (btn == 'no')
                    return;
                this.sendDeleteActionTasks();
            }
        })
    },

    sendDeleteActionTasks: function(hasBeenValidated) {
        var ids = []
        Ext.Array.forEach(this.actionTasksSelections, function(r) {
            ids.push(r.get('id'));
        })

        this.set('reqCount', this.reqCount + 1);

        this.ajax({
            url: '/resolve/service/actiontask/delete',
            params: {
                ids: ids,
                all: this.allSelected,
                validate: !!!hasBeenValidated
            },
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    clientVM.displayError(this.localize('DeleteErrMsg') + '[' + respData.message + ']')
                    return;
                }
                if (!!!hasBeenValidated) {
                    if (respData.records.length > 0) {
                        this.open({
                            mtype: 'RS.actiontask.ReferenceList',
                            detailScreen: 'RS.wiki.Main',
                            referenceNames: respData.records
                        })
                        return;
                    }
                }
                clientVM.displaySuccess(this.localize('DeleteSucMsg'));
                this.actionTaskGridStore.load()
            },

            failure: function(resp) {
                clientVM.displayFailure(resp);
            },
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
        })
    },

    editDefinition: function(id) {
        clientVM.handleNavigation({
            modelName: 'RS.actiontask.ActionTask',
            params: {
                id: id
            }
        })
    },
    deleteActionTasksIsEnabled$: function() {
        return this.actionTasksSelections.length > 0 && this.reqCount == 0 && !this.selectedInResolveNS;
    },

    searchDelimiter: '',

    activeItem: 0,

    menuPathSelected: null,

    moduleSelected: null,

    menuPath: function() {
        this.set('searchDelimiter', '/');
        this.set('activeItem', 0);
        if (!this.menuPathSelected || this.menuPathSelected.get('name').indexOf('All') != -1)
            with(this.actionTaskTreeMenuStore.getRootNode()) {
                collapse();
                expand();
            }
        this.actionTaskGridStore.load();
    },

    menuPathIsPressed$: function() {
        return this.activeItem == 0;
    },

    menuPathIsEnabled$: function() {
        return this.reqCount == 0;
    },

    module: function() {
        this.set('searchDelimiter', '.')
        this.set('activeItem', 1);
        if (!this.moduleSelected || this.moduleSelected.get('name').indexOf('All') != -1)
            with(this.actionTaskTreeModuleStore.getRootNode()) {
                collapse();
                expand();
            }
        this.actionTaskGridStore.load();
    },

    moduleIsEnabled$: function() {
        return this.reqCount == 0;
    },

    moduleIsPressed$: function() {
        return this.activeItem == 1;
    },

    menuPathTreeSelectionchange: function(selected, eOpts) {
        if (selected.length > 0)
            this.set('menuPathSelected', selected[0]);
        this.actionTaskGridStore.loadPage(1);
    },

    moduleTreeSelectionchange: function(selected, eOpts) {
        if (selected.length > 0)
            this.set('moduleSelected', selected[0]);
        this.actionTaskGridStore.loadPage(1);
    },

    actionConfirm: function(title, msg, sucMsg, errMsg, params) {
        this.message({
            title: title,
            msg: msg,
            buttons: Ext.MessageBox.YESNO,
            buttonText: {
                yes: this.localize(params.action),
                no: this.localize('cancel')
            },
            scope: this,
            fn: function(btn) {
                if (btn == 'no')
                    return;
                this.sendReq(sucMsg, errMsg, params);
            }
        })
    },

    sendReq: function(sucMsg, errMsg, params) {
        this.set('reqCount', this.reqCount + 1);
        this.ajax({
            url: '/resolve/service/actiontask/actiontaskmanipulation',
            params: params,
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success)
                    clientVM.displayError(errMsg + '[' + respData.message + ']');
                else if (respData.records.length > 0) {
                    this.open({
                        mtype: 'RS.actiontask.BulkOpWarningList',
                        msg: respData.message,
                        records: respData.records,
                        action: params.action,
                        dumper: (function(self) {
                            return function() {
                                self.sendReq(sucMsg, errMsg, Ext.apply(params, {
                                    overwrite: true
                                }));
                            }
                        })(this)
                    });
                } else
                    clientVM.displaySuccess(sucMsg);
                this.actionTaskGridStore.load();
            },
            failure: function(resp) {
                clientVM.displayFailure(resp);
            },
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
        })
    },

    doRename: function(params) {
        var title = this.localize('RenameTitle');
        var msg = this.localize(this.actionTasksSelections.length > 1 ? 'RenamesMsg' : 'RenameMsg', {
            num: this.actionTasksSelections.length
        });
        var sucMsg = this.localize('RenameSuc');
        var errMsg = this.localize('RenameErr');
        this.actionConfirm(title, msg, sucMsg, errMsg, params);
    },

    rename: function() {
        var unamespace = this.actionTasksSelections[0].get('unamespace');
        var uname = this.actionTasksSelections[0].get('uname');
        var ids = [];
        Ext.each(this.actionTasksSelections, function(r) {
            ids.push(r.get('id'));
        }, this);
        var self = this;
        this.open({
            mtype: 'RS.actiontask.MoveOrRenameOrCopy',
            unamespace: unamespace,
            uname: uname,
            action: 'rename',
            bulk: this.actionTasksSelections.length > 1,
            dumper: function(action, unamespace, uname, overwrite) {
                var params = {
                    ids: ids,
                    action: action,
                    module: unamespace,
                    name: uname,
                    overwrite: overwrite
                };
                self.doRename(params);
            }
        });
    },
    renameIsEnabled$: function() {
        return this.reqCount == 0 && this.actionTasksSelections.length > 0
    },

    doCopy: function(params) {
        var title = this.localize('CopyTitle');
        var msg = this.localize(this.actionTasksSelections.length > 1 ? 'CopysMsg' : 'CopyMsg', {
            num: this.actionTasksSelections.length
        });
        var sucMsg = this.localize('CopySuc');
        var errMsg = this.localize('CopyErr');
        this.actionConfirm(title, msg, sucMsg, errMsg, params);
    },

    copy: function() {
        var unamespace = this.actionTasksSelections[0].get('unamespace');
        var uname = this.actionTasksSelections[0].get('uname');
        var ids = [];
        var self = this;
        Ext.each(this.actionTasksSelections, function(r) {
            ids.push(r.get('id'));
        }, this);
        this.open({
            mtype: 'RS.actiontask.MoveOrRenameOrCopy',
            unamespace: unamespace,
            uname: uname,
            action: 'copy',
            bulk: this.actionTasksSelections.length > 1,
            dumper: function(action, unamespace, uname, overwrite) {
                var params = {
                    ids: ids,
                    action: action,
                    module: unamespace,
                    name: uname,
                    overwrite: overwrite
                };
                self.doCopy(params);
            }
        });
    },

    copyIsEnabled$: function() {
        return this.reqCount == 0 && this.actionTasksSelections.length > 0
    },
    index: function() {
        var ids = [];
        Ext.each(this.actionTasksSelections, function(t) {
            ids.push(t.get('id'));
        });
        this.ajax({
            url: '/resolve/service/actiontask/index',
            params: {
                all: false,
                ids: ids
            },
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    clientVM.displayError(this.localize('submitIndexReqError', respData.message));
                    return;
                }

                clientVM.displaySuccess(this.localize('submitIndexReqSuccess'));
            },
            failure: function(resp) {
				clientVM.displayFailure(resp);
			}
        });
    },
    indexIsEnabled$: function() {
        return this.reqCount == 0 && this.actionTasksSelections.length > 0;
    },
    indexAll: function() {
        this.ajax({
            url: '/resolve/service/actiontask/index',
            params: {
                all: true,
                ids: []
            },
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    clientVM.displayError(this.localize('submitIndexReqError', respData.message));
                    return;
                }
                clientVM.displaySuccess(this.localize('submitIndexReqSuccess'));
            },
            failure: function(resp) {
				clientVM.displayFailure(resp);
			}
        })
    },
    executionIsEnabled$: function() {
        return this.actionTasksSelections.length == 1 && this.reqCount == 0;
    },
    execution: function(button) {
        this.set('reqCount', this.reqCount + 1);
        var record = this.actionTasksSelections[0];
        this.ajax({
            url: '/resolve/service/actiontask/get',
            params: {
                id: record.get('id'),
                name: ''
            },
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    clientVM.displayError(this.localize('LoadActionTaskErr'), respData.message);
                    return;
                }
                var task = respData.data;
                var params = task.resolveActionInvoc.resolveActionParameters;
                var mockData = task.resolveActionInvoc.resolveActionTaskMockData;
                if (this.executionWin)
                    this.executionWin.doClose();
                this.executionWin = this.open({
                    mtype: 'RS.actiontask.Execute',
                    target: button.getEl().id,
                    executeDTO: {
                        actiontask: task.ufullName,
                        actiontaskSysId: task.id,
                        // problemId: this.newWorksheet ? 'NEW' : null,
                        action: 'TASK'
                    },
                    inputsLoader: function(paramsStore, mockStore) {
                        Ext.each(params, function(param) {
                            if (param.utype == 'INPUT')
                                paramsStore.add({
                                    name: param.uname,
                                    value: param.udefaultValue,
                                    order: param.uorder
                                })
                        });
                        paramsStore.sort('order');
                        if (mockData)
                            mockStore.add(mockData);
                    }
                });
            },
            failure: function(resp) {
                clientVM.displayFailure(resp);
            },
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
        });
    }
})