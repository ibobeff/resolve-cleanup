glu.defModel('RS.actiontask.ActionTaskValidation', {
	validateName: function(name) {
		return RS.common.validation.TaskName.test(name);
	},
	validateNamespace: function(namespace) {
		return RS.common.validation.TaskNamespace.test(namespace);
	}
});