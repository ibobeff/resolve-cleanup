glu.defModel('RS.actiontask.MoveOrRenameOrCopy', {
    title: '',
    fields: ['unamespace', 'uname'],
    unamespace: '',
    overwrite: false,
    overwriteIsVisible$: function() {
        return this.action == 'copy';
    },
    unamespaceIsValid$: function() {
        return RS.common.validation.TaskNamespace.test(this.unamespace) ? true : this.localize('invalidNamespace');
    },
    uname: '',
    unameIsVisible$: function() {
        return !this.bulk;
    },
    unameIsValid$: function() {
        return RS.common.validation.TaskName.test(this.uname) ? true : this.localize('invalidName');
    },
    action: '',
    bulk: false,
    comboBoxStore: {
        mtype: 'store',
        fields: ['name']       
    },
    oriNamespace: '',
    oriName:  '',

    init: function() {
        this.set('oriNamespace', this.unamespace);
        this.set('oriName', this.uname);
        this.ajax({
            url: '/resolve/service/common/actiontask/namespaces',
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    clientVM('listNamespaceError' + '[' + resp.message + ']');
                    return;
                }
                Ext.each(respData.records, function(r, index) {
                    this.comboBoxStore.add({
                        name: r
                    });
                    if (this.get('unamespace') === r) {
                        // GVo Note
                        // This is a get-around
                        // solution on the combobox not populating the value.
                        // I have no idea why. According to Sencha docs, we can set the
                        // initial selection by setting the value property but some how it doesn't work in the case.
                        this.comboBoxStore.fireEvent('foundComboSelection', this.comboBoxStore.getAt(index));
                    }
                }, this);
            },
            failure: function(resp) {
                clientVM.displayFailure(resp);
            }
        });
        this.set('title', this.localize(this.action + "Title"));
    },

    dumper: null,
    dump: function() {
        this.dumper(this.action, this.unamespace, this.uname, this.overwrite);
        this.cancel();
    },
    nameChange$: function() {
        return (this.unamespace != this.oriNamespace) || (this.uname != this.oriName);
    },
    dumpIsEnabled$: function() {
        return this.isValid && this.nameChange;
    },
    actionText: '',
    dumpText$: function() {
        return this.actionText || this.localize(this.action);
    },

    cancel: function() {
        this.doClose();
    }
})