glu.defModel('RS.actiontask.BulkOpWarningList', {
	action: '',
	bulkWarning: '',
	msg: '',
	selected: null,
	records: [],
	html: '',
	forceToProceed: '',
	dumper: null,	
	init: function() {
		this.set('bulkWarning', this.localize(this.action + 'BulkWarning'));
		this.set('forceToProceed', this.localize(this.action + 'forceToProceed'), {
			msg: this.msg
		})
		var list = '';
		Ext.each(this.records, function(r) {
			list += Ext.String.format('<li>{0}</li>', r);
		}, this);
		list = '<ul>' + list + '</ul>';
		this.set('html', list);
	},
	selectionChanged: function(selected) {
		this.set('selected', selected.length > 0 ? selected[0] : null);
	},
	proceedBulk: function() {
		this.dumper();
		this.close();
	},
	proceedBulkIsVisible$: function() {
		return this.action == 'copy';
	},
	closeBulkIsVisible$ : function(){
		return this.action == 'copy';
	},
	closeIsVisible$ : function(){
		return this.action != 'copy';
	},
	buttonAlign$ : function(){
		return this.proceedBulkIsVisible ? 'right' : 'center'
	},
	close: function() {
		this.doClose();
	},
	closeBulk : function(){
		this.doClose();
	}
});