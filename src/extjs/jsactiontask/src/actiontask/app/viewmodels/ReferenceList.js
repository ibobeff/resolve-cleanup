glu.defModel('RS.actiontask.ReferenceList', {
	store: {
		mtype: 'store',
		fields: ['name'],
		proxy: {
			type: 'memory'
		}
	},

	referenceNames: null,
	gotoDetail: true,
	detailScreen: '',
	init: function() {
		Ext.each(this.referenceNames, function(name) {
			this.store.add({
				name: name
			})
		}, this)
	},
	selected: null,
	selectionChanged: function(selected) {
		this.set('selected', selected.length > 0 ? selected[0] : null);
	},
	gotoReference: function() {
		var name = this.selected.get('name');
		clientVM.handleNavigation({
			modelName: this.detailScreen,
			params: {
				name: name
			},
			target: '_blank'
		})
	},

	gotoReferenceIsEnabled$: function() {
		return this.selected;
	},

	gotoReferenceIsVisible$: function() {
		return this.detailScreen;
	},

	deleteRecords: function() {
		this.parentVM.sendDeleteActionTasks(true);
		this.close();
	},

	close: function() {
		this.doClose();
	}
});