glu.defModel('RS.actiontask.PropertyDefinitions', {
	displayName: '',

	columns: null,

	stateId: 'rsactiontaskpropertiesgrid',

	wait: false,

	store: {
		mtype: 'store',

		fields: ['uname', 'utype', 'uvalue', 'umodule'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'uname',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/atproperties/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	recordsSelections: [],

	activate: function() {
		this.clientVM.setWindowTitle(this.localize('windowTitle'))
		this.store.load();
	},

	init: function() {
		this.set('displayName', this.localize('displayName'));

		this.set('columns', [{
			header: '~~uname~~',
			filterable: true,
			sortable: true,
			dataIndex: 'uname',
			flex: 3
		}, {
			header: '~~utype~~',
			dataIndex: 'utype',
			filterable: true,
			sortable: true,
			width: 100
		}, {
			header: '~~uvalue~~',
			filterable: false,
			sortable: false,
			dataIndex: 'uvalue',
			flex: 1,
			renderer: function(value, row) {
				var record = row.record;
				var type = record.get('utype');
				if (type.toUpperCase() == 'PLAIN')
					return RS.common.grid.plainRenderer()(value);
				return "*****";
			}
		}, {
			header: '~~umodule~~',
			dataIndex: 'umodule',
			filterable: true,
			sortable: true,
			width: 120
		}].concat((function() {
			var cols = RS.common.grid.getSysColumns();
			Ext.each(cols, function(c) {
				if (c.dataIndex == 'sysUpdatedOn') {
					c.hidden = false;
					c.initialShow = true;
				}
			})
			return cols;
		})()));
		this.store.on('beforeload', function() {
			this.set('wait', true)
		}, this)
		this.store.on('load', function(store, rec, suc) {
			this.set('wait', false);
			/*
			if (!suc) {
				this.clientVM.displayError(this.localize('ListRecordsErr'))
			}
			*/
		}, this);
	},

	editRecord: function(id) {
		this.open({
			mtype: 'RS.actiontask.PropertyDefinition',
			id: id
		})
	},

	createRecord: function() {
		this.open({
			mtype: 'RS.actiontask.PropertyDefinition',
			id: ''
		})
	},

	deleteRecords: function() {
		this.message({
			title: this.localize('DeleteTitle'),
			msg: this.localize(this.recordsSelections.length > 1 ? 'DeletesMsg' : 'DeleteMsg', {
				num: this.recordsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirmDelete'),
				no: this.localize('cancel')
			},

			fn: this.sendDeleteReq
		})
	},

	sendDeleteReq: function(btn) {
		if (btn != 'yes')
			return;

		this.set('wait', true)

		var ids = [];

		Ext.each(this.recordsSelections, function(r) {
			ids.push(r.get('id'));
		})

		this.ajax({
			url: '/resolve/service/atproperties/delete',
			params: {
				ids: ids,
				all: this.allSelected
			},
			success: function(resp) {
				this.set('wait', false);
				this.handleDeleteSucces(resp);
			},
			failure: function(resp) {
				this.set('wait', false);
				this.clientVM.displayError(this.localize('ServerError') + '[' + resp.status + ']');
			}
		})
	},

	handleDeleteSucces: function(resp) {
		this.set('wait', false);
		var respData = Ext.JSON.decode(resp.responseText);
		if (!respData.success)
			this.clientVM.displayError(this.localize('DeleteErrMsg') + '[' + respData.message + ']')
		else
			this.clientVM.displaySuccess(this.localize('DeleteSucMsg'));
		this.store.load();
	},

	createRecordIsEnabled$: function() {
		return !this.wait;
	},

	deleteRecordsIsEnabled$: function() {
		return this.recordsSelections.length > 0;
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	}
})