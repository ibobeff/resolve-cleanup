glu.defModel('RS.actiontask.Execute', {
	API : {
		getAT : '/resolve/service/actiontask/get',
		submit : '/resolve/service/execute/submit'
	},
	target: null,
	newWorksheet: true,
	activeWorksheet: false,
	debug: false,
	mock: false,
	mockName: '',
	sirProblemId: '',

	fromWiki: false,
	mockEditable$: function() {
		return this.fromWiki
	},

	mockStore: {
		mtype: 'store',
		fields: ['uname'],
		proxy: {
			type: 'memory'
		}
	},

	paramStore: {
		mtype: 'store',
		fields: ['name', 'value', 'order'],
		proxy: {
			type: 'memory'
		}
	},

	paramColumns: [{
		header: '~~name~~',
		dataIndex: 'name',
		filterable: false,
		flex: 1,
		editor: {},
		renderer: RS.common.grid.plainRenderer()
	}, {
		header: '~~value~~',
		dataIndex: 'value',
		filterable: false,
		flex: 1,
		editor: {},
		renderer: RS.common.grid.plainRenderer()
	}],

	//when another module use this view, it can use this to init the vm
	inputsLoader: function() {
		this.parentVM.paramsTabCard.paramsStore.each(function(r) {
			if (r.get('utype') == 'INPUT')
				this.paramStore.add({
					name: r.get('uname'),
					value: r.get('udefaultValue'),
					order: r.get('uorder')
				});
		}, this);
		this.paramStore.sort('order');
		this.parentVM.mockTabCard.mockStore.each(function(r) {
			this.mockStore.add(r);
		}, this);
	},
	embedded: false,
	init: function() {
		this.set('debug', clientVM.executionDebugDefaultMode || false);
		if (this.executeDTO && this.embedded)
			this.embeddedExecution();
		else
			this.inputsLoader(this.paramStore, this.mockStore);
	},

	embeddedExecution: function() {
		this.doClose();
		this.ajax({
			url: this.API['getAT'],
			params: {
				id: this.executeDTO.id,
				name: this.executeDTO.name
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('LoadActionTaskErr'), respData.message);
					return;
				}
				var task = respData.data;
				var params = task.resolveActionInvoc.resolveActionParameters;
				Ext.Object.each(this.executeDTO.inputParams, function(k, v) {
					Ext.each(params, function(param) {
						if (param.utype != 'INPUT')
							return;
						if (param.uname != k)
							return;
						param.udefaultValue = v;
					});
				});
				var mockData = task.resolveActionInvoc.resolveActionTaskMockData;
				if (this.executionWin)
					this.executionWin.doClose();
				this.executionWin = this.open({
					mtype: 'RS.actiontask.Execute',
					// target: button.getEl().id,
					executeDTO: {
						actiontask: task.ufullName,
						actiontaskSysId: task.id,
						// problemId: this.newWorksheet ? 'NEW' : null,
						action: 'TASK'
					},
					inputsLoader: function(paramsStore, mockStore) {
						Ext.each(params, function(param) {
							if (param.utype == 'INPUT')
								paramsStore.add({
									name: param.uname,
									value: param.udefaultValue,
									order: param.uorder
								})
						});
						paramsStore.sort('order');
						if (mockData)
							mockStore.add(mockData);
						this.set('embedded', true);
					}
				});
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	paramsSelections: [],
	addParam: function() {
		var max = 0;
		var reg = /^NewParam(\d*)$/;
		var newParam = 'NewParam';

		this.paramStore.each(function(param) {
			if (!reg.test(param.get('name')))
				return;
			var m = reg.exec(param.get('name'));
			var idx = parseInt(m[1]);
			if (idx > max)
				max = idx;
		}, this);
		newParam += ((max + 1) || ''); 

		this.paramStore.add({
			name: newParam,
			value: ''
		})

		// clear out all previously selected items
		this.paramStore.grid.getSelectionModel().deselectAll();

		// determine row of newly added "NewParam"
		var row;
		for (row = this.paramStore.data.items.length-1; row > 0; row--) {
			if (this.paramStore.data.items[row].data.name == newParam) {
				break;
			}
		}
		// start editing the newly selected item
		this.paramStore.grid.getPlugin().startEditByPosition({
			row: row,
			column: 0
		});
	},

	removeParams: function() {
		this.paramStore.remove(this.paramsSelections);
	},

	mockNameIsEnabled$: function() {
		return this.mock
	},

	mockNameIsValid$: function() {
		return (!this.mock || this.mockName) ? true : this.localize('invalidMockName');
	},

	cancel: function() {
		this.doClose()
	},
	executeDTO: null,
	execute: function() {
		var map = {};
		this.paramStore.each(function(param) {
			if (param.get('name') === 'PROBLEMID' && this.sirProblemId !== '') {
				//Pass sir activity worksheet id when called from playbook
				map[param.get('name')] = this.sirProblemId;
			} else {
				map[param.get('name')] = param.get('value');
			}
		}, this);
		var dto = Ext.apply({
			problemId: this.newWorksheet ? 'NEW' : 'ACTIVE',
			isDebug: this.debug,
			mockName: this.mockName,
			params: Ext.apply(map, {
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			}),			
		}, this.executeDTO);

		if (this.fromWiki) {
			var mockNames = Ext.state.Manager.get('wikiMockNames', []);
			if (Ext.Array.indexOf(mockNames, this.mockName) == -1) {
				mockNames.splice(0, 0, this.mockName)
				Ext.state.Manager.set('wikiMockNames', mockNames.slice(0, 10))
			}
		}

		this.ajax({
			url: this.API['submit'],
			jsonData: dto,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				this.doClose();
				if (response.success) {
					clientVM.updateProblemInfo(response.data.problemId, response.data.problemNumber)
					clientVM.fireEvent('worksheetExecuted', clientVM);
					if (this.activeWorksheet) {
						clientVM.fireEvent('refreshActiveWorksheet');
					}
					clientVM.displaySuccess(this.localize('executeSuccess', {
						type: !dto.wiki ? 'ActionTask' : 'Runbook',
						fullName: !dto.wiki ? dto.actiontask : dto.wiki
					}))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	executeIsEnabled$: function() {
		return this.mockNameIsValid;
	}
});