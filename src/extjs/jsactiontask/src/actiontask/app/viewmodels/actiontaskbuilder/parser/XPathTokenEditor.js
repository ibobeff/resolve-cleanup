glu.defModel('RS.actiontaskbuilder.XPathTokenEditor', {	
	currentPath: '',
	selector : '',
	selectorToken : null,
	selectorIsVisible : false,	
	currentSampleDom: null,
	attributeStore: {
		mtype: 'store',
		fields: ['name']
	},
	attrConditionStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	textConditionStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	predicatesSelections: [],
	augmentedXPathToken: null,
	loading: false,
	and: true,	
	//allValid: true,
	predicates: {
		mtype: 'list',
	},
	nodeName : '',
	noProperty : true,	
	title$ : function(){
		return !this.noProperty ? this.nodeName + "'s Property" : this.localize('xpathProperty');
	},	
	noPredicate$ :function(){
		return  this.predicates.getCount() == 0;
	},
	init: function() {
		this.set('title', this.localize('xpathProperty'));
		this.textConditionStore.add([{
			name: this.localize('startsWith'),
			value: 'starts-with'
		}, {
			name: this.localize('endsWith'),
			value: 'ends-with'
		}, {
			name: this.localize('contains'),
			value: 'contains'
		}]);

		this.attrConditionStore.add([{
			name: '=',
			value: '='
		}, {
			name: '!=',
			value: '!='
		}, {
			name: '>=',
			value: '>='
		}, {
			name: '<=',
			value: '<='
		}, {
			name: this.localize('startsWith'),
			value: 'starts-with'
		}, {
			name: this.localize('endsWith'),
			value: 'ends-with'
		}, {
			name: this.localize('contains'),
			value: 'contains'
		}])
		this.predicates.on('lengthChanged', function(){
			this.fireEvent('predicatesChanged');
		},this);

	},	
	resetProperty : function (){
		this.predicates.removeAll();
		this.set('selectorToken', null);
		this.set('selector', null);
	},
	updateXPath : function(xpathDetail){
		this.resetProperty();		
		this.set('augmentedXPathToken', xpathDetail.augmentedXPathToken);
		this.set('currentSampleDom', xpathDetail.currentSampleDom);
		this.set('selectorToken', xpathDetail.selectorToken);
		var attributeCustomName = [];
		Ext.each(this.augmentedXPathToken.predicates, function(p) {
			//Copy config object so we can preserve original data otherwise glu will overwrite this object when using glu.model method...
			var predicateConfig = Ext.apply({},p);
			if (predicateConfig.type == 'attribute'){
				this.doAddPredicate('RS.actiontaskbuilder.XPathAttrPredicate', predicateConfig);
				attributeCustomName.push(predicateConfig.name);
			}
			else if (p.type == 'text')
				this.doAddPredicate('RS.actiontaskbuilder.XPathTextPredicate', predicateConfig);
			else
				this.doAddPredicate('RS.actiontaskbuilder.XPathCustomizedPredicate', predicateConfig);
		}, this)
		this.set('and', this.augmentedXPathToken.op == 'and');
		this.set('selectorIsVisible', this.augmentedXPathToken.isLeaf);
		if(this.augmentedXPathToken.isLeaf && this.selectorToken){
			this.set('selector', this.selectorToken.value);
		}	

		var nodes = Ext.fly(this.currentSampleDom).query(this.augmentedXPathToken.element)
		var attrs = [];
		this.attributeStore.removeAll();
		Ext.each(nodes, function(node) {
			for (var i = 0; i < node.attributes.length; i++) {
				var attr = node.attributes[i];
				if (this.attributeStore.findExact('name', attr.name) != -1)
					return;
				if(attr.name != 'pnid'){					
					this.attributeStore.add({
						name: attr.name
					});
				}				
			}
		}, this);
		for(var i = 0; i < attributeCustomName.length; i++){
			if (this.attributeStore.findExact('name', attributeCustomName[i]) == -1){
				this.attributeStore.add({
					name: attributeCustomName[i]
				});
			}
		}
		this.set('nodeName', this.augmentedXPathToken.element);
		this.set('noProperty', false);		
	},	
	addTextPredicate: function() {
		this.doAddPredicate('RS.actiontaskbuilder.XPathTextPredicate', {
			pnid: this.augmentedXPathToken.pnid,
			ptid: Ext.data.IdGenerator.get('uuid').generate(),
			condition: 'contains',
			text: 'NewText'
		});
	},
	addAttrPredicate: function() {
		this.doAddPredicate('RS.actiontaskbuilder.XPathAttrPredicate', {
			pnid: this.augmentedXPathToken.pnid,
			ptid: Ext.data.IdGenerator.get('uuid').generate(),
			name: 'NewAttribute',
			condition: '=',
			value: 'Value'
		});
	},
	addCustPredicate: function() {
		this.doAddPredicate('RS.actiontaskbuilder.XPathCustomizedPredicate', {
			pnid: this.augmentedXPathToken.pnid,
			ptid: Ext.data.IdGenerator.get('uuid').generate(),
			predicate: 'NewPredicate'
		});
	},
	doAddPredicate: function(modelName, predicate) {
		this.predicates.add(this.model(modelName, predicate));	
	},
	doRemovePredicate: function(predicateVM) {
		this.predicates.remove(predicateVM);	
	},
	doUpdate: function() {
		var newPredicates = [];
		this.predicates.forEach(function(p) {
			newPredicates.push(p.getData());
		});
		Ext.each(this.parentVM.augmentedXPath, function(token) {
			if (token.ptid == this.augmentedXPathToken.ptid){
				token.predicates = newPredicates;
				token.op = this.and ? 'and' : 'or';
			}
			if(token.type == "selector")
				token.value = this.selector;
		}, this);
		//Add selector token if there is no selector token in current Path
		if(this.selectorToken == null){
			this.parentVM.augmentedXPath.push({
				type: 'selector',			
				ptid: Ext.data.IdGenerator.get('uuid').generate(),
				value: this.selector
			})
		}

		var decodedPath = '';

		try {
			decodedPath = Ext.decode(Ext.encode(this.parentVM.augmentedXPath));
		} catch (ex) {
			decodedPath = 'Decode Failure (Figure out this bug and fix it)';
		}		

		this.parentVM.set('augmentedXPath', decodedPath);
		this.parentVM.set('textXPath', this.parentVM.xpath);		
	},
	removeNode: function() {
		var pnid = this.augmentedXPathToken.pnid;
		var ptid = this.augmentedXPathToken.ptid;
		this.parentVM.removeToken(pnid,ptid);
		this.set('noProperty', true);	
	},
	update: function() {
		this.doUpdate();		
	},
	/*updateIsEnabled$: function() {
		return true; //TODO
	}	*/
});
glu.defModel('RS.actiontaskbuilder.XPathPredicate', {	
	remove: function() {
		this.parentVM.doRemovePredicate(this);
	},
	getData: function() {
		return this.asObject();
	}
});

glu.defModel('RS.actiontaskbuilder.XPathTextPredicate', {
	mixins: ['XPathPredicate'],
	type: 'text',
	condition: 'contains',
	fields: ['pnid', 'ptid', 'condition', 'value', 'type'],
	/*valueIsValid$: function() {
		return Ext.String.trim(this.value) ? true : this.localize('invalidValue');
	}*/
});

glu.defModel('RS.actiontaskbuilder.XPathAttrPredicate', {
	mixins: ['XPathPredicate'],
	type: 'attribute',
	fields: ['pnid', 'ptid', 'name', 'condition', 'value', 'type'],
	/*nameIsValid$: function() {
		return Ext.String.trim(this.name) ? true : this.localize('invalidName');
	},
	valueIsValid$: function() {
		return Ext.String.trim(this.value) ? true : this.localize('invalidValue');
	}*/
});

glu.defModel('RS.actiontaskbuilder.XPathCustomizedPredicate', {
	mixins: ['XPathPredicate'],
	type : 'custom',
	fields: ['pnid', 'ptid', 'type','value'],
/*	predicateIsValid$: function() {
		return Ext.String.trim(this.value) ? true : this.localize('invalidPredicate');
	}*/
});