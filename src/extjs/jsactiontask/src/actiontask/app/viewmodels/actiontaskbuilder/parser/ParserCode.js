glu.defModel("RS.actiontaskbuilder.ParserCode",{
	autoGenCheck : true,	
	assessTask : false,	
	codeContent : "",	
	parserType : 'text',
	parserConfig : null,
	sample : null,
	viewOnly : false,
	ignoreDirtyChecking: false,
	API : {
		getParserAutoGenCode  : '/resolve/service/actiontask/getParserAutoGenCode'
	},
	update_dirty_flag : {
		on : ['autoGenCheckChanged','codeContentChanged'],
		action : function(){
			if(!this.viewOnly && !this.ignoreDirtyChecking)
				this.parentVM.notifyDirty();
		}
	},
	editorCls$ : function(){
		return this.viewOnly ? 'editor-disabled' : '';
	},
	populateCode: function(autoGenCheck, code){
		this.set('ignoreDirtyChecking', true);
		var autoGenCheck = autoGenCheck ? autoGenCheck : this.code == "";
		this.set('autoGenCheck', autoGenCheck);
		this.set('codeContent', code);
		setTimeout(function() {
			this.set('ignoreDirtyChecking', false);
		}.bind(this), 100);
	},
	updateMarkupInfo : function(){	
		this.set('parserConfig', this.parentVM.getParserConfig());
		this.set('sample', this.parentVM.getSample());		
	},
	generateCode : function(){
		this.updateMarkupInfo();
		if(this.autoGenCheck){
			if(this.parserConfig){
				var data = {
					parserConfig : this.parserConfig,
					sampleOutput : this.sample,
					parserType : this.parserType,
					isAssessTask : this.assessTask
				}
				this.ajax({
					url : this.API['getParserAutoGenCode'],
					jsonData : data,		
					success : function(response){
						var respData = RS.common.parsePayload(response);

						if (respData.success) {
							this.set('codeContent', respData.data);
						}						
					}
				})
			}
			else{
				this.set('codeContent', "");			
			}
		}
		else {
			setTimeout(function(){
				this.message({
					title : this.localize('autoGenIsUncheckTitle'),
					msg : this.localize('autoGenIsUncheckMsg'),
					button : Ext.MessageBox.YESNO,
					scope : this,
					buttonText : {
						yes : this.localize('yes'),
						no : this.localize('cancel')
					},
					fn : function(btn){
						if(btn == 'yes'){
							this.set('autoGenCheck', true);
							this.generateCode();
						}
					}
				})
			}.bind(this),500);			
		}		
	},
	resetCode: function(){
		this.set('autoGenCheck', true);		
		this.set('codeContent', "");	
	},
	loadCode : function(autoGenFlag, codeContent){
		this.set('autoGenCheck', autoGenFlag);
		this.set('codeContent', codeContent);
	},
	isAssessTask : function(flag){
		this.set('assessTask', flag);
	},
	getCode : function(){
		return this.codeContent;
	},
	isAutoGenCode : function(){
		return this.autoGenCheck;
	},
	checkBoxChange : function(newVal, oldVal){
		var parserType = this.parserType;
		if(newVal){
			this.message({
				title : this.localize('autoGenConfirmTitle'),
				msg : this.localize('autoGenConfirmBody'),
				button : Ext.MessageBox.YESNO,
				scope : this,
				buttonText : {
					yes : this.localize('yes'),
					no : this.localize('cancel')
				},
				fn : function(btn){
					if(btn == 'yes')
						this.generateCode();
					else 
						this.set('autoGenCheck', false);
				}
			})
		}
	},
	codeContentIsReadOnly$ : function(){
		return this.viewOnly || this.autoGenCheck;
	}
})
