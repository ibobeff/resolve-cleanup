glu.defModel('RS.actiontaskbuilder.ParserControl',{
	activeParserType : 'text',
	smallResolution : false,
	textParser: {
		mtype: 'PlainTextParser'
	},	
	tableParser: {
		mtype: 'TableParser'	
	},
	xmlParser: {
		mtype: 'XMLParser'	
	},
	when_resolution_change : {
		on : ['smallResolutionChanged'],
		action : function(){
			this.textParser.set('smallResolution', this.smallResolution);
			this.tableParser.set('smallResolution',  this.smallResolution);
			this.xmlParser.set('smallResolution',  this.smallResolution);
		}
	},
	loadData : function(data){
		this[this.activeParserType + 'Parser'].loadData(data);
	},	
	setDefaultParser : function(){
		this.textParser.setDefaultParser();
		this.tableParser.setDefaultParser();
		this.xmlParser.setDefaultParser();
	},
	updateDefaultTestSample : function(data){
		this.parentVM.updateDefaultTestSample(this.activeParserType, data);
	},
	updateActiveparserType : function(parserType){
		this.set('activeParserType', parserType);
		this.notifyDataChange();
	},
	isAssessTask: function(flag){
		this[this.activeParserType + 'Parser'].isAssessTask(flag);	
	},
	getActiveType : function(){
		return this.activeParserType;
	},	
	getCode : function(){
		return this[this.activeParserType + 'Parser'].getCode();
	},
	getParserConfig : function(){
		return this[this.activeParserType + 'Parser'].getParserConfig();
	},	
	getSample : function(){
		return this[this.activeParserType + 'Parser'].getSample();
	},
	getParameterList : function(){
		return this[this.activeParserType + 'Parser'].getParameterList();
	},
	isAutoGenCode : function(){
		return this[this.activeParserType + 'Parser'].isAutoGenCode();
	},
	isParserReady : function(){
		return this[this.activeParserType + 'Parser'].isParserReady();
	},
	notifyDirty : function(){
		this.parentVM.notifyDirty();
	},
	notifyDataChange : function(){		
		this.parentVM.notifyDataChange();
	}
})