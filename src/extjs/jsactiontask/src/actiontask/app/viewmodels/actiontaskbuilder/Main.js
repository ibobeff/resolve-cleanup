glu.defModel('RS.actiontask.ActionTask', {
	itemList : { mtype : 'list'	},

	//for distinguishing new and edit
	name: '',
	namespace: '',
	id: '',
	version: '',
	ufullName: '',
	uversion: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	uname: '',
	unamespace: '',
	saveIsEnabled: false,
	saveAsRenameIsEnabled: false,
	editable: false,
	executable: false,
	embed: false,
	generalIsValid: false,
	uisStable: false,
	saveSuccess: '',
	saveAsTitle: '',
	requestFailedMessage: '',
	reloadWithoutSaveTitle: '',
	reloadWithoutSaveBody: '',
	confirmButtonText: '',
	cancelButtonText: '',
	actionTaskTitle: 'Action Task',
	yesButtonText: '',
	noButtonText: '',
	editNewTask: '',
	editNewActionTaskTitle: '',
	editNewActionTaskMessage: '',
	newActionTaskTitle: '',
	actionTaskTitle: '',
	blockContainer: null,
	generalBlock: null,
	generalRead: null,
	generalEdit: null,
	referencesRead: null,
	contentEdit: null,
	contentRead: null,
	assessEdit: null,
	assessRead: null,
	parser: null,
	preprocessorEdit: null,
	preprocessorRead: null,
	rolesEdit: null,
	optionsEdit: null,
	parametersRead: null,
	parametersInputEdit: null,
	parametersOutputEdit: null,
	assignValuesToOutputsEdit: null,
	assignValuesToOutputsRead: null,
	severitiesEdit: null,
	conditionsEdit: null,
	severitiesAndConditionsRead: null,
	summaryEdit: null,
	summaryAndDetailsRead: null,
	detailsEdit: null,
	mockEdit: null,
	mockRead: null,
	tagsRead: null,
	tagsEdit: null,
	emptyActionTaskData: null,
	dateFormat: '',
	isDirty: false,
	isValid: true,
	numSectionsOpen: 0,
	sectionsOpenList: {},
	isMaxScreenSize: false,
	bodyPadding$ : function(){
		return !this.embed ? 15 : 0;
	},
	view: null,
	allTags: [],
	allATProperties: [],
	getAllTagsInProgress: false,
	getAllATPropertiesInProgress: false,

	API : {
		getAT : '/resolve/service/actiontask/get',
		getAllTags : '/resolve/service/tag/listTags',
		getAllATProperties : '/resolve/service/atproperties/list',
		saveAT : '/resolve/service/actiontask/save',
		commitAT : '/resolve/service/actiontask/commit',
	},

	preview: false,
	readOnlyMode: true,
	generalEditSectionExpanded: null,
	generalEditSectionMaximized: null,
	parametersEditSectionExpanded: null,
	parametersEditSectionMaximized: null,
	severitiesAndConditionsEditSectionExpanded: null,
	severitiesAndConditionsEditSectionMaximized: null,
	summaryAndDetailsEditSectionExpanded: null,
	summaryAndDetailsEditSectionMaximized: null,

	saveIsEnabled$: function(){
		return this.isValid && this.isDirty;
	},
	saveAsRenameIsEnabled$ : function(){
		return this.isValid && !this.isNew;
	},

	versionTxt$: function() {
		return this.uversion ? this.localize('versionTxt', this.uversion) : '';
	},

	init: function() {
		this.set('dateFormat', clientVM.getUserDefaultDateFormat());
		this.saveWithoutDoneBody = this.localize('saveWithoutDoneBody');
		this.saveSuccess = this.localize('SaveSuc');
		this.saveAsTitle = this.localize('saveAsTitle');
		this.requestFailedMessage = this.localize('requestFailed');
		this.newActionTaskTitle = this.localize('NewActionTaskTitle');
		this.reloadWithoutSaveTitle = this.localize('reloadWithoutSaveTitle');
		this.reloadWithoutSaveBody = this.localize('reloadWithoutSaveBody');
		this.confirmButtonText = this.localize('confirm');
		this.cancelButtonText = this.localize('cancel');
		this.actionTaskTitle = this.localize('ActionTaskTitle');
		this.editNewTask = this.localize('editNewTask');
		this.editNewActionTaskTitle = this.localize('EditNewActionTaskTitle');
		this.editNewActionTaskMessage = this.localize('EditNewActionTaskMsg');
		this.yesButtonText = this.localize('yesButtonText');
		this.noButtonText = this.localize('noButtonText');

		this.allText = this.localize('allBtn');
		this.generalText = this.localize('generalBtn');
		this.referenceText = this.localize('referenceBtn');
		this.parametersText = this.localize('parametersBtn');
		this.preprocessorText = this.localize('preprocessorBtn');
		this.contentText = this.localize('contentBtn');
		this.parserText = this.localize('parserBtn');
		this.assessText = this.localize('assessBtn');
		this.outputsText = this.localize('outputsBtn');
		this.severityConditionsText = this.localize('severityConditionsBtn');
		this.summaryDetailsText = this.localize('summaryDetailsBtn');
		this.mockText = this.localize('mockBtn');
		this.tagsText = this.localize('tagsBtn');

		if (!this.embed) {
			this.getAllTags(function(records){
				this.set('allTags', records);
			}.bind(this))
			this.getAllATProperties(function(records){
				var propertyNames = [];
				for(var i = 0; i < records.length; i++){
					propertyNames.push(records[i]['uname']);
				}
				this.set('allATProperties', propertyNames);
			}.bind(this))

			this.initActionTaskBuilder();

			var actionTaskToolbarSetting = Ext.state.Manager.get('actionTaskToolbarSetting');
			if (actionTaskToolbarSetting && parseInt(actionTaskToolbarSetting) > 0) {
				this.set('toolbarSetting', actionTaskToolbarSetting);
			} else if (window.mobile) {
				this.set('toolbarSetting', this.toolbarSettingMap['ICONONLY']);
			} else {
				this.set('toolbarSetting', this.toolbarSettingMap['TEXTANDICON']);
			}
		}
		
		// generate page token for actiontask/editorstub.jsp
		clientVM.getCSRFToken_ForURI('/resolve/actiontask/editorstub.jsp');
	},

	activate: function(screen, params) {
		if (this.creatingNewTask) {
			this.creatingNewTask = false;
			this.generalEdit.gluModel.hideShowGeneralEditTab(this.id);
			return;
		}

		this.set('version', '');
		if (params && params.version) {
			var version = params.version.toLowerCase();
			if (version == 'lateststable' || version == 'latestcommit' || version == 'latestcommitted') {
				this.set('version', 'latestStable');
			} else if (Ext.isNumeric(version)) {
				this.set('version', version);
			}
		}

		if (!params || !params.id || this.id !== params.id) {
			var id = '',
				name = '',
				namespace = '';

			if (params) {
				if (typeof params.id === 'string') {
					id = params.id;
				}

				if (typeof params.name === 'string') {
					name = params.name.trim();
				}

				if (typeof params.namespace === 'string') {
					namespace = params.namespace.trim();
				}
			}

			var hasName = name.length > 0 && namespace.length > 0,
				hasID = id.length > 0;

			this.set('id', id);
			this.set('name', name);
			this.set('namespace', namespace);
			this.set('saveIsEnabled', false);
			this.set('saveAsRenameIsEnabled', false);

			if (this.callbackIdForNewTask && this.blockContainer) {
				this.blockContainer.removeCallback(this.generalEdit, 'onLoadOrClose', this.callbackIdForNewTask);
			}

			if (hasID || hasName) {
				this.set('saveAsRenameIsEnabled', true);
				this.loadActionTask(function (payload) {

					if (!hasID) {
						this.modifyHash(payload.data.id);
					}

					this.successfulActionTaskLoad(payload);
					this.set('sectionButtonSelected', this.sectionButtonMap['ALL']);
					this.set('readOnlyMode', true);
					this.collapseAll();
					this.resizeNormalAllSection(true);
				}.bind(this), this.failActionTaskLoad.bind(this), true);

			} else {
				this.initNew();
			}
		} else {
			this.set('saveAsRenameIsEnabled', true);
			this.set('sectionButtonSelected', this.sectionButtonMap['ALL']);
			this.set('readOnlyMode', true);
			this.loadActionTask(function (payload) {
				this.successfulActionTaskLoad(payload);
			}.bind(this), this.failActionTaskLoad.bind(this), true);
		}
		this.generalEdit.gluModel.hideShowGeneralEditTab(this.id);
	},

	isInitialized: false,

	initActionTaskBuilder: function() {
		var blockContainer = new RS.common.blocks.BlockContainer(),
			fullScreenText = this.localize('resizeFullscreen'),
			normalScreenText = this.localize('resizeNormalscreen');

		// General section
		var generalBlock = blockContainer.addBlock('generalBlock');
		var generalRead = generalBlock.addModel(this, 'generalTitle', 'RS.actiontaskbuilder.GeneralRead', RS.actiontaskbuilder.GeneralReadBlockModel);
		var generalEdit = generalBlock.addModel(this, 'generalTitle', 'RS.actiontaskbuilder.GeneralEdit', RS.actiontaskbuilder.GeneralEditBlockModel);

		// General section wizard button (order matters)
		generalRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.generalRead.gluModel, this.sectionButtonMap['GENERAL']);
		}.bind(this));
		generalRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.generalRead.gluModel);
		}.bind(this));

		// Roles sub-section
		var rolesEdit = generalBlock.addModel(this, 'generalTitle', 'RS.actiontaskbuilder.RolesEdit', RS.actiontaskbuilder.RolesEditBlockModel);

		// Options sub-section
		var optionsEdit = generalBlock.addModel(this, 'generalTitle', 'RS.actiontaskbuilder.OptionsEdit', RS.actiontaskbuilder.OptionsEditBlockModel);

		// References section
		var referencesBlock = blockContainer.addBlock('referencesBlock');
		var referencesRead = referencesBlock.addModel(this, 'referencesReadTitle', 'RS.actiontaskbuilder.ReferencesRead', RS.actiontaskbuilder.ReferencesReadBlockModel);
		referencesRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.referencesRead.gluModel, this.sectionButtonMap['REFERENCE']);
		}.bind(this));
		referencesRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.referencesRead.gluModel);
		}.bind(this));

		// Parameters section
		var parametersBlock = blockContainer.addBlock('parametersBlock');
		var parametersRead = parametersBlock.addModel(this, 'parametersTitle', 'RS.actiontaskbuilder.ParametersRead', RS.actiontaskbuilder.ParametersReadBlockModel);
		var parametersInputEdit = parametersBlock.addModel(this, 'inputParametersWizardStepTitle', 'RS.actiontaskbuilder.ParametersInputEdit', RS.actiontaskbuilder.ParametersInputEditBlockModel);
		var parametersOutputEdit = parametersBlock.addModel(this, 'outputParametersWizardStepTitle', 'RS.actiontaskbuilder.ParametersOutputEdit', RS.actiontaskbuilder.ParametersOutputEditBlockModel);

		// Parameters section wizard button order matters)
		parametersRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.parametersRead.gluModel, this.sectionButtonMap['PARAMETERS']);
		}.bind(this));
		parametersRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.parametersRead.gluModel);
		}.bind(this));

		// Preprocessor section
		var preprocessorBlock = blockContainer.addBlock('preprocessorBlock');
		var preprocessorRead = preprocessorBlock.addModel(this, 'preprocessorReadTitle', 'RS.actiontaskbuilder.PreprocessorRead', RS.actiontaskbuilder.PreprocessorReadBlockModel);
		var preprocessorEdit = preprocessorBlock.addModel(this, 'preprocessorEditTitle', 'RS.actiontaskbuilder.PreprocessorEdit', RS.actiontaskbuilder.PreprocessorEditBlockModel);

		// Preprocessor section Read buttons (order matters)
		preprocessorRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.preprocessorRead.gluModel, this.sectionButtonMap['PREPROCESSOR']);
		}.bind(this));
		preprocessorRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.preprocessorRead.gluModel);
		}.bind(this));

		// Preprocessor section Edit buttons (order matters)
		preprocessorEdit.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.preprocessorEdit.gluModel, this.sectionButtonMap['PREPROCESSOR']);
		}.bind(this));
		preprocessorEdit.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.preprocessorEdit.gluModel);
		}.bind(this));

		// Content section
		var contentBlock = blockContainer.addBlock('contentBlock');
		var contentRead = contentBlock.addModel(this, 'contentReadTitle', 'RS.actiontaskbuilder.ContentRead', RS.actiontaskbuilder.ContentReadBlockModel);
		var contentEdit = contentBlock.addModel(this, 'contentEditTitle', 'RS.actiontaskbuilder.ContentEdit', RS.actiontaskbuilder.ContentEditBlockModel);

		// Content section Read buttons (order matters)
		contentRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.contentRead.gluModel, this.sectionButtonMap['CONTENT']);
		}.bind(this));
		contentRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.contentRead.gluModel);
		}.bind(this));

		// Content section Edit buttons (order matters)
		contentEdit.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.contentEdit.gluModel, this.sectionButtonMap['CONTENT']);
		}.bind(this));
		contentEdit.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.contentEdit.gluModel);
		}.bind(this));

		// Parser section
		var parserBlock = blockContainer.addBlock('parserBlock');
		var parserRead = parserBlock.addModel(this, 'parserReadTitle', 'RS.actiontaskbuilder.ParserRead', RS.actiontaskbuilder.ParserReadBlockModel);
		var parserEdit = parserBlock.addModel(this, 'parserEditTitle', 'RS.actiontaskbuilder.ParserEdit', RS.actiontaskbuilder.ParserEditBlockModel);

		// Parser section buttons (order matters)
		parserRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.parserRead.gluModel, this.sectionButtonMap['PARSER']);
		}.bind(this));
		parserRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.parserRead.gluModel);
		}.bind(this));

		// Parser section buttons (order matters)
		parserEdit.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.parserEdit.gluModel, this.sectionButtonMap['PARSER']);
		}.bind(this));
		parserEdit.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.parserEdit.gluModel);
		}.bind(this));

		// Assessor section
		var assessBlock = blockContainer.addBlock('assessBlock');
		var assessRead = assessBlock.addModel(this, 'assessReadTitle', 'RS.actiontaskbuilder.AssessorRead', RS.actiontaskbuilder.AssessorReadBlockModel);
		var assessEdit = assessBlock.addModel(this, 'assessEditTitle', 'RS.actiontaskbuilder.AssessorEdit', RS.actiontaskbuilder.AssessorEditBlockModel);

		// Content section Read buttons (order matters)
		assessRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.assessRead.gluModel, this.sectionButtonMap['ASSESS']);
		}.bind(this));
		assessRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.assessRead.gluModel);
		}.bind(this));

		// Content section Edit buttons (order matters)
		assessEdit.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.assessEdit.gluModel, this.sectionButtonMap['ASSESS']);
		}.bind(this));
		assessEdit.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.assessEdit.gluModel);
		}.bind(this));

		// Assign Values to Outputs section
		var assignValuesToOutputsBlock = blockContainer.addBlock('assignValuesToOutputsBlock');
		var assignValuesToOutputsRead = assignValuesToOutputsBlock.addModel(this, 'valueToOutputsReadTitle', 'RS.actiontaskbuilder.AssignValuesToOutputsRead', RS.actiontaskbuilder.AssignValuesToOutputsReadBlockModel);
		var assignValuesToOutputsEdit = assignValuesToOutputsBlock.addModel(this, 'valueToOutputsEditTitle', 'RS.actiontaskbuilder.AssignValuesToOutputsEdit', RS.actiontaskbuilder.AssignValuesToOutputsEditBlockModel);

		// Assign Values to Outputs section Read button (order matters)
		assignValuesToOutputsRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.assignValuesToOutputsRead.gluModel, this.sectionButtonMap['OUTPUT']);
		}.bind(this));
		assignValuesToOutputsRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.assignValuesToOutputsRead.gluModel);
		}.bind(this));

		// Assign Values to Outputs section Edit buttons (order matters)
		assignValuesToOutputsEdit.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.assignValuesToOutputsEdit.gluModel, this.sectionButtonMap['OUTPUT']);
		}.bind(this));
		assignValuesToOutputsEdit.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.assignValuesToOutputsEdit.gluModel);
		}.bind(this));

		// Severities and Conditions section
		var severitiesAndConditionsBlock = blockContainer.addBlock('severitiesAndConditionsBlock');
		var severitiesAndConditionsRead = severitiesAndConditionsBlock.addModel(this, 'severitiesAndConditionsTitle', 'RS.actiontaskbuilder.SeveritiesAndConditionsRead', RS.actiontaskbuilder.SeveritiesAndConditionsBlockModel);
		var severitiesEdit = severitiesAndConditionsBlock.addModel(this, 'severitiesAndConditionsTitle', 'RS.actiontaskbuilder.SeveritiesEdit', RS.actiontaskbuilder.SeveritiesEditBlockModel);
		var conditionsEdit = severitiesAndConditionsBlock.addModel(this, 'severitiesAndConditionsTitle', 'RS.actiontaskbuilder.ConditionsEdit', RS.actiontaskbuilder.ConditionsEditBlockModel);

		// Severities and Conditions section Read button (order matters)
		severitiesAndConditionsRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.severitiesAndConditionsRead.gluModel, this.sectionButtonMap['SEVERITYCONDITION']);
		}.bind(this));
		severitiesAndConditionsRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.severitiesAndConditionsRead.gluModel);
		}.bind(this));

		// Summary and Details section
		var summaryAndDetailsBlock = blockContainer.addBlock('summaryAndDetailsBlock');
		var summaryAndDetailsRead = summaryAndDetailsBlock.addModel(this, 'summaryAndDetailsTitle', 'RS.actiontaskbuilder.SummaryAndDetailsRead', RS.actiontaskbuilder.SummaryAndDetailsReadBlockModel);
		var summaryEdit = summaryAndDetailsBlock.addModel(this, 'summaryAndDetailsTitle', 'RS.actiontaskbuilder.SummaryEdit', RS.actiontaskbuilder.SummaryEditBlockModel);
		var detailsEdit = summaryAndDetailsBlock.addModel(this, 'summaryAndDetailsTitle', 'RS.actiontaskbuilder.DetailsEdit', RS.actiontaskbuilder.DetailsEditBlockModel);

		// Summary and Details section Read button (order matters)
		summaryAndDetailsRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.summaryAndDetailsRead.gluModel, this.sectionButtonMap['SUMMARYDETAILS']);
		}.bind(this));
		summaryAndDetailsRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.summaryAndDetailsRead.gluModel);
		}.bind(this));

		// Mock section
		var mockBlock = blockContainer.addBlock('mockBlock');
		var mockRead = mockBlock.addModel(this, 'mockTitle', 'RS.actiontaskbuilder.MockRead', RS.actiontaskbuilder.MockReadBlockModel);
		var mockEdit = mockBlock.addModel(this, 'mockTitle', 'RS.actiontaskbuilder.MockEdit', RS.actiontaskbuilder.MockEditBlockModel);

		// Mock section Read button (order matters)
		mockRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.mockRead.gluModel, this.sectionButtonMap['MOCK']);
		}.bind(this));
		mockRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.mockRead.gluModel);
		}.bind(this));

		// Mock section Edit buttons (order matters)
		mockEdit.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.mockEdit.gluModel, this.sectionButtonMap['MOCK']);
		}.bind(this));
		mockEdit.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.mockEdit.gluModel);
		}.bind(this));

		// Tags section
		var tagsBlock = blockContainer.addBlock('tagsBlock');
		var tagsRead = tagsBlock.addModel(this, 'tagsReadTitle', 'RS.actiontaskbuilder.TagsRead', RS.actiontaskbuilder.TagsReadBlockModel);
		var tagsEdit = tagsBlock.addModel(this, 'tagsEditTitle', 'RS.actiontaskbuilder.TagsEdit', RS.actiontaskbuilder.TagsEditBlockModel);

		// Tags section Read button (order matters)
		tagsRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.tagsRead.gluModel, this.sectionButtonMap['TAGS']);
		}.bind(this));
		tagsRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.tagsRead.gluModel);
		}.bind(this));

		// Tags section Edit buttons (order matters)
		tagsEdit.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.tagsEdit.gluModel, this.sectionButtonMap['TAGS']);
		}.bind(this));
		tagsEdit.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.tagsEdit.gluModel);
		}.bind(this));

		// addDataSource
		generalBlock.addDataSource(generalEdit);
		generalBlock.addDataSource(rolesEdit);
		generalBlock.addDataSource(optionsEdit);
		parametersBlock.addDataSource(parametersInputEdit);
		parametersBlock.addDataSource(parametersOutputEdit);
		preprocessorBlock.addDataSource(preprocessorEdit);
		contentBlock.addDataSource(contentEdit);
		parserBlock.addDataSource(parserEdit);
		referencesBlock.addDataSource(referencesRead);
		assessBlock.addDataSource(assessEdit);
		assignValuesToOutputsBlock.addDataSource(assignValuesToOutputsEdit);
		severitiesAndConditionsBlock.addDataSource(severitiesEdit);
		severitiesAndConditionsBlock.addDataSource(conditionsEdit);
		summaryAndDetailsBlock.addDataSource(summaryEdit);
		summaryAndDetailsBlock.addDataSource(detailsEdit);
		mockBlock.addDataSource(mockEdit);
		tagsBlock.addDataSource(tagsEdit);

		// Callbacks
		blockContainer.defineCallback(generalRead, function (data, resolveActionInvoc) {
			this.set('id', data.id);
			this.set('uname', data.uname);
			this.set('unamespace', data.unamespace);
			this.set('ufullName', data.ufullName);
			this.set('uversion', data.uversion);
			this.set('sysCreatedOn', data.sysCreatedOn);
			this.set('sysCreatedBy', data.sysCreatedBy);
			this.set('sysUpdatedOn', data.sysUpdatedOn);
			this.set('sysUpdatedBy', data.sysUpdatedBy);

			if (data.unamespace && data.unamespace.length > 0 && data.uname  && data.uname.length > 0) {
				this.modifyActionTaskTitle(data.uname + '#' + data.unamespace);
			} else {
				this.modifyActionTaskTitle(this.newActionTaskTitle);
			}
		}.bind(this));

		blockContainer.defineCallback(rolesEdit, function (data, accessRights) {
			if (accessRights.canEdit || this.isNew) {
				this.set('editable', true);
				this.enableEditLinks();
			} else {
				this.set('editable', false);
				this.disableEditLinks();
			}
		}.bind(this))

		blockContainer.subscribeDirty(function (dirty) {
			this.set('isDirty', dirty);
			var title = this.actionTaskTitle;
			title = title.replace(/ <span style=\"color:crimson;\">!<\/span>/, '');
        	title = title.replace(/\*/,'');

			this.modifyActionTaskTitle(title);
		}.bind(this));

		blockContainer.subscribeValid(function (valid) {
			this.set('isValid', valid);
			var title = this.actionTaskTitle;
			title = title.replace(/ <span style=\"color:crimson;\">!<\/span>/, '');
        	title = title.replace(/\*/,'');

			this.modifyActionTaskTitle(title);
		}.bind(this));

		// defineDataDependency
		blockContainer.defineDataDependency(generalEdit, contentRead);
		blockContainer.defineDataDependency(generalEdit, contentEdit);
		blockContainer.defineDataDependency(generalEdit, parserEdit);
		blockContainer.defineDataDependency(generalEdit, mockRead);
		blockContainer.defineDataDependency(generalEdit, mockEdit);

		//Internal data dependency (not quite generic, might need to enhance in the future). ParserEdit will kick off the chain event.
		blockContainer.defineInternalDataDependency(parserEdit, parametersOutputEdit);
		blockContainer.defineInternalDataDependency(parametersOutputEdit, summaryEdit);
		blockContainer.defineInternalDataDependency(parametersOutputEdit, detailsEdit);
		blockContainer.defineInternalDataDependency(parametersOutputEdit, assignValuesToOutputsEdit);
		blockContainer.defineInternalDataDependency(parametersOutputEdit, severitiesEdit);
		blockContainer.defineInternalDataDependency(parametersOutputEdit, conditionsEdit);
		blockContainer.defineInternalDataDependency(parametersInputEdit, summaryEdit);
		blockContainer.defineInternalDataDependency(parametersInputEdit, detailsEdit);
		blockContainer.defineInternalDataDependency(parametersInputEdit, assignValuesToOutputsEdit);
		blockContainer.defineInternalDataDependency(parametersInputEdit, severitiesEdit);
		blockContainer.defineInternalDataDependency(parametersInputEdit, conditionsEdit);


		// itemList
		this.itemList.add(generalRead.gluModel);
		this.itemList.add(generalEdit.gluModel);
		this.itemList.add(rolesEdit.gluModel);
		this.itemList.add(optionsEdit.gluModel);
		this.itemList.add(referencesRead.gluModel);
		this.itemList.add(parametersRead.gluModel);
		this.itemList.add(parametersInputEdit.gluModel);
		this.itemList.add(parametersOutputEdit.gluModel);
		this.itemList.add(preprocessorRead.gluModel);
		this.itemList.add(preprocessorEdit.gluModel);
		this.itemList.add(contentRead.gluModel);
		this.itemList.add(contentEdit.gluModel);
		this.itemList.add(parserRead.gluModel);
		this.itemList.add(parserEdit.gluModel);
		this.itemList.add(assessRead.gluModel);
		this.itemList.add(assessEdit.gluModel);
		this.itemList.add(assignValuesToOutputsRead.gluModel);
		this.itemList.add(assignValuesToOutputsEdit.gluModel);
		this.itemList.add(severitiesAndConditionsRead.gluModel);
		this.itemList.add(severitiesEdit.gluModel);
		this.itemList.add(conditionsEdit.gluModel);
		this.itemList.add(summaryAndDetailsRead.gluModel);
		this.itemList.add(summaryEdit.gluModel);
		this.itemList.add(detailsEdit.gluModel);
		this.itemList.add(mockRead.gluModel);
		this.itemList.add(mockEdit.gluModel);
		this.itemList.add(tagsRead.gluModel);
		this.itemList.add(tagsEdit.gluModel);

		// actiontaskbuilder variables
		this.blockContainer = blockContainer;
		this.generalBlock = generalBlock;
		this.generalRead = generalRead;
		this.generalEdit = generalEdit;
		this.referencesRead = referencesRead;
		this.rolesEdit = rolesEdit;
		this.optionsEdit = optionsEdit;
		this.contentEdit = contentEdit;
		this.contentRead = contentRead;
		this.preprocessorEdit = preprocessorEdit;
		this.preprocessorRead = preprocessorRead;
		this.assessEdit = assessEdit;
		this.assessRead = assessRead;
		this.parserEdit = parserEdit;
		this.parserRead = parserRead;
		this.parametersInputEdit = parametersInputEdit;
		this.parametersOutputEdit = parametersOutputEdit;
		this.parametersRead = parametersRead;
		this.assignValuesToOutputsEdit = assignValuesToOutputsEdit;
		this.assignValuesToOutputsRead = assignValuesToOutputsRead;
		this.severitiesEdit = severitiesEdit;
		this.conditionsEdit = conditionsEdit;
		this.severitiesAndConditionsRead = severitiesAndConditionsRead;
		this.summaryEdit = summaryEdit;
		this.detailsEdit = detailsEdit;
		this.summaryAndDetailsRead = summaryAndDetailsRead;
		this.mockEdit = mockEdit;
		this.mockRead = mockRead;
		this.tagsRead = tagsRead;
		this.tagsEdit = tagsEdit;
		this.emptyActionTaskData = blockContainer.getData();
		blockContainer.load(this.emptyActionTaskData);

		this.isInitialized = true;
	},

	hideAllSections: function(section, ignoreCurrentSection, onComplete) {
		for (var i=0, l = this.itemList.length; i < l; i++) {
			var currentSection = this.itemList.getAt(i);
			if (ignoreCurrentSection || currentSection.viewmodelName != section.viewmodelName) {
				this.hideSection(currentSection);
			}
		}
		if (Ext.isFunction(onComplete)) {
			onComplete.call(this);
		}
	},

	hideSection: function(section) {
		if (section.isSectionMaxSize) {
			if (section.height) {
				this.setSectionHeight(section, section.minHeight);
			}
			section.isSectionMaxSize = false;
			this.showSectionMaxResizeBtn(section);
			this.hideSectionNormalResizeBtn(section);
			this.toggleSectionCollapseTool(section);
		}

		this.collapseAndHideSection(section);
	},

	collapseAndHideSection: function(section) {
		if (section.hidden && section.collapsed) {
			// do nothing
		} else if (section.hidden) {
			section.ignoreDecrementFlag = true;
			section.set('collapsed', true);
		} else if (section.collapsed) {
			section.set('hidden', true);
		} else {
			section.collapseAndHideSectionFlag = true;
			section.ignoreDecrementFlag = true;
			section.set('collapsed', true);
		}
	},

	collapseAndHideSectionCallback: function(section) {
		section.collapseAndHideSectionFlag = false;
		section.set('hidden', true);
	},

	showAndExpandSectionCallback: function(section) {
		section.showAndExpandSectionFlag = false;
		section.set('collapsed', false);

		if (section.showSectionMaxFlag) {
			section.showSectionMaxFlag = false;
			this.doResizeMaxSection(section);
		}
	},

	setSectionHeight: function(section, height) {
		 if (section.height) {
			section.set('height', 0);
			if (section.contentIsEmpty) {
				section.set('height', 'auto');
			} else {
				section.set('height', height);
			}
		}
	},

	doResizeMaxSection: function(section) {
		if (!section.isSectionMaxSize) {
			this.set('isMaxScreenSize', true);
			section.isSectionMaxSize = true;

			var id, offset;
			if (this.embed) {
				id = this.view.up('#TaskProperty').getEl().dom.id;
				offset = 131;
			} else {
				id = this.view.getEl().dom.id;
				offset = 35;
			}

			var el = Ext.getElementById(id+'-body'),
				height = el.style.height;

			if (section.height) {
				this.setSectionHeight(section, parseInt(height) - offset);
			}

			if (this.isNew && section.viewmodelName == 'GeneralEdit') {
				this.hideSectionNormalResizeBtn(section);
			} else {
				this.showSectionNormalResizeBtn(section);
			}
			this.hideSectionMaxResizeBtn(section);
			this.toggleSectionCollapseTool(section);
		}
	},

	resizeMaxSection: function(section, sectionButtonName) {
		this.hideAllSections(section, null, function() {
			setTimeout(function() {
				if (section.hidden) {
					section.showAndExpandSectionFlag = true;
					section.showSectionMaxFlag = true;
					section.set('hidden', false);
				} else if (section.collapsed) {
					section.set('collapsed', false);
					this.doResizeMaxSection(section);
				} else {
					this.doResizeMaxSection(section);
				}
		
				this.set('sectionButtonSelected', sectionButtonName);
			}.bind(this), 100);
		});
		if (this.readOnlyChangeFlag) {
			this.readOnlyChangeFlag = false;
		}
	},

	resizeNormalSection: function(section) {
		this.set('isMaxScreenSize', false);
		if (this.readOnlyMode) {
			this.showAllReadSections();
		} else {
			this.showAllEditSections();
		}
		this.set('sectionButtonSelected', this.sectionButtonMap['ALL']);
	},

	resizeNormalAllSection: function(isInitial) {
		this.set('isMaxScreenSize', false);

		if (!isInitial) {
			var blockNames = ['generalBlock', 'parametersBlock', 'preprocessorBlock',
				'parserBlock', 'assessBlock', 'assignValuesToOutputsBlock', 'severitiesAndConditionsBlock',
				'summaryAndDetailsBlock', 'mockBlock', 'tagsBlock', 'referencesBlock'];

			if (this.contentSectionVisible) {
				blockNames.push('contentBlock');
			}
			this.blockContainer.showBlocks(blockNames);
		}

		if (this.readOnlyMode) {
			this.showAllReadSections();
		} else {
			this.showAllEditSections();
		}

		if (this.syncExpandedSectionsFlag) {
			this.syncExpandedSections();
		} else {
			this.collapseAll();
		}
		this.set('sectionButtonSelected', this.sectionButtonMap['ALL']);
	},

	showAllReadSections: function() {
		for (var i=0, l = this.itemList.length; i < l; i++) {
			var section = this.itemList.getAt(i);

			if (section.editBlock || section.editSubBlock || section.conditionalEditBlock) {
				this.collapseAndHideSection(section);

			} else if (section.conditionalReadBlock && section.viewmodelName == 'ContentRead') {
				if (this.contentSectionVisible) {
					this.showThisSection(section);
				} else {
					this.collapseAndHideSection(section);
				}
			} else {
				this.showThisSection(section);
				if (section.readAndEditBlock && section.viewmodelName == 'Parser') {
					section.set('isViewOnly', true);
				}
			}

			if (section.isSectionMaxSize) {
				if (section.height) {
					this.setSectionHeight(section, section.minHeight);
				}
				section.isSectionMaxSize = false;
				if (section.collapsed) {
					this.hideSectionMaxResizeBtn(section);
				} else {
					this.showSectionMaxResizeBtn(section);
				}
				this.hideSectionNormalResizeBtn(section);
				this.toggleSectionCollapseTool(section);
			}
		}
	},

	showThisSection: function(section) {
		if (section.hidden) {
			section.set('hidden', false);
		}
	},

	showAndExpandThisSection: function(section) {
		if (section.hidden) {
			if (section.collapsed) {
				section.showAndExpandSectionFlag = true;
			}
			section.set('hidden', false);
		}
	},

	showAllEditSections: function() {
		for (var i=0, l = this.itemList.length; i < l; i++) {
			var section = this.itemList.getAt(i);

			if (section.editBlock) {
				this.showThisSection(section);
			} else if (section.conditionalEditBlock && section.viewmodelName == 'ContentEdit') {
				if (this.contentSectionVisible) {
					this.showThisSection(section);
				} else {
					this.collapseAndHideSection(section);
				}
			} else if (section.readAndEditBlock) {
				this.showThisSection(section);
				if (section.viewmodelName == 'Parser') {
					section.set('isViewOnly', false);
				}
			} else {
				this.collapseAndHideSection(section);
			}

			if (section.isSectionMaxSize) {
				if (section.height) {
					this.setSectionHeight(section, section.minHeight);
				}
				section.isSectionMaxSize = false;
				if (section.collapsed) {
					this.hideSectionMaxResizeBtn(section);
				} else {
					this.showSectionMaxResizeBtn(section);
				}
				this.hideSectionNormalResizeBtn(section);
				this.toggleSectionCollapseTool(section);
			}
		}
	},

	showSectionMaxResizeBtn: function(section) {
		var view = section.view;
		if (view && view.header && view.header.items.length) {
			var headerItems = view.header.items.items;
			if (headerItems) {
				for (var i=0, l=headerItems.length; i < l; i++) {
					var headerItem = headerItems[i];
					if (headerItem.isResizeMaxBtn && headerItem.hidden && typeof headerItem.show === 'function') {
						headerItem.show();
					}
				}
			}
		}
	},

	hideSectionMaxResizeBtn: function(section) {
		var view = section.view;
		if (view && view.header && view.header.items.length) {
			var headerItems = view.header.items.items;
			if (headerItems) {
				for (var i=0, l=headerItems.length; i < l; i++) {
					var headerItem = headerItems[i];
					if (headerItem.isResizeMaxBtn && headerItem.show && typeof headerItem.hide === 'function') {
						headerItem.hide();
					}
				}
			}
		}
	},

	showSectionNormalResizeBtn: function(section) {
		var view = section.view;
		if (view && view.header && view.header.items.length) {
			var headerItems = view.header.items.items;
			if (headerItems) {
				for (var i=0, l=headerItems.length; i < l; i++) {
					var headerItem = headerItems[i];
					if (headerItem.isResizeNormalBtn && headerItem.hidden && typeof headerItem.show === 'function') {
						headerItem.show();
					}
				}
			}
		}
	},

	hideSectionNormalResizeBtn: function(section) {
		var view = section.view;
		if (view && view.header && view.header.items.length) {
			var headerItems = view.header.items.items;
			if (headerItems) {
				for (var i=0, l=headerItems.length; i < l; i++) {
					var headerItem = headerItems[i];
					if (headerItem.isResizeNormalBtn && headerItem.show && typeof headerItem.hide === 'function') {
						headerItem.hide();
					}
				}
			}
		}
	},

	toggleSectionCollapseTool: function(section, forceHide) {
		var view = section.view;
		if (view && view.header && view.header.items.length) {
			var items = view.header.items;

			// collapse tool is usually the last item in the header list, unless there is no collapse tool
			var headerItem = items.getAt(items.length -1);

			if (headerItem.xtype == 'tool' || headerItem.isCollapseBtn) {
				if ((forceHide || section.isSectionMaxSize) &&
					(typeof headerItem.hide === 'function'))
				{
					headerItem.hide();
				} else if (typeof headerItem.show === 'function') {
					headerItem.show();
				}
			}
		}
	},

	hideAllSectionBtns: function(section) {
		 this.hideSectionMaxResizeBtn(section);
		 this.hideSectionNormalResizeBtn(section);
		 this.toggleSectionCollapseTool(section, true);
	},

	modifyActionTaskTitle: function (title) {
		title = Ext.String.htmlEncode(title);
		if(!this.isValid){
			this.set('actionTaskTitle', title + ' <span style="color:crimson;">!</span>');
		}
		else if (this.isDirty) {
			this.set('actionTaskTitle', title + '*');
		} else {
			this.set('actionTaskTitle', title);
		}

		if (!this.embed) {
			clientVM.setWindowTitle(this.actionTaskTitle);
		}
	},

	callbackIdForNewTask : null,

	creatingNewTask: false,

	initNew: function() {
		this.set('isNew', true);
		this.set('editable', true);
		this.set('executable', false);
		this.set('readOnlyMode', false);
		this.blockContainer.hideBlocks();
		this.blockContainer.load(this.emptyActionTaskData);
		this.resizeMaxSection(this.generalEdit.gluModel, this.sectionButtonMap['GENERAL']);
		setTimeout(function() {
			this.hideAllSectionBtns(this.generalEdit.gluModel);
		}.bind(this), 100);

		/*
		var generalEditDoneCallbackID = this.blockContainer.defineCallback(this.generalEdit, function () {
			this.rolesEdit.disableForm();
			this.optionsEdit.disableForm();
			this.saveActionTask(false, null, function onSuccess(payload) {
				this.creatingNewTask = true;
				this.blockContainer.load(payload);
				var blockNames = ['parametersBlock', 'preprocessorBlock',
					'parserBlock', 'assessBlock', 'assignValuesToOutputsBlock', 'severitiesAndConditionsBlock',
					'summaryAndDetailsBlock', 'mockBlock', 'tagsBlock', 'referencesBlock'];

				if (payload.data.resolveActionInvoc.utype.toUpperCase() !== 'ASSESS') {
					blockNames.push('contentBlock');
				}

				this.blockContainer.showBlocks(blockNames);
				this.set('generalIsValid', true);
				this.modifyHash(payload.data.id);
				this.blockContainer.removeCallback(this.generalEdit, 'onClose', generalEditDoneCallbackID);
				this.rolesEdit.enableForm();
				this.optionsEdit.enableForm();
			}.bind(this), function onFailure() {
				this.generalBlock.hide();
				this.generalEdit.show();
				this.rolesEdit.enableForm();
				this.optionsEdit.enableForm();
			}.bind(this));
		}.bind(this), 'onClose');
		this.set('callbackIdForNewTask', generalEditDoneCallbackID);
		*/

		this.closeDetailSections();
		this.set('sectionButtonDisabled', true);
	},

	closeDetailSections: function() {
		// close Mock Edit/Read detail sections
		if (this.mockEdit && this.mockEdit.gluModel.isEditingDetails) {
			this.mockEdit.gluModel.set('isEditingDetails', false);
		}
		if (this.mockRead && this.mockRead.gluModel.isShowingDetails) {
			this.mockRead.gluModel.set('isShowingDetails', false);
		}
	},

	modifyHash: function (id, isReplaceId) {
		var hashParts = [],
			hasIdInHash;

		if (clientVM.windowHash && clientVM.windowHash.length > 0) {
			var hash = clientVM.windowHash,
				hasIdInHash = hash.indexOf('/id=') >= 0;

			if (!hasIdInHash && hash[hash.length - 1] === '/') {
				hash = hash.substr(0, hash.length - 1);
			} else if (hasIdInHash && isReplaceId) {
				hash = hash.substr(0, hash.indexOf('/id='))
			}

			hashParts.push(hash);

			if (!hasIdInHash || isReplaceId) {
				hashParts.push('id=' + id);
			}
		}
		var params = {
			id: id
		};
		if (this.version) {
			Ext.apply(params, {
				version: this.version
			})
		}
		clientVM.handleNavigation({
			modelName: 'RS.actiontask.ActionTask',
			params: params
		});
	},

	loadActionTask: function(onSuccess, onFailure, suppressError) {
		this.set('saveIsEnabled', false);
		//To indicate a new AT is loaded into view.
		window.loadingNewAT = true;
		var params = {
			id: this.id,
			name: this.name ? this.name + '#' + this.namespace : ''
		};
		if (this.version) {
			Ext.apply(params, {
				version: this.version
			})
		}
		this.ajax({
			url: this.API['getAT'],
			method: 'post',
			params: params,
			scope: this,
			success: function (response) {
				var payload = RS.common.parsePayload(response);

				if (payload.success) {
					this.set('generalIsValid', true);
					this.set('uisStable', payload.data.uisStable);

					if (typeof onSuccess === 'function') {
						onSuccess(payload);
					}
				} else {
					if (!suppressError) {
						clientVM.displayError(this.localize('serverError', payload.message));
					}

					if (typeof onFailure === 'function') {
						onFailure(payload);
					}
				}
			},
			failure: function(response) {
				clientVM.displayFailure(response);

				if (typeof onFailure === 'function') {
					onFailure(response);
				}
			}
		});
	},

	successfulActionTaskLoad: function (payload) {
		if (!this.isInitialized) {
			this.initActionTaskBuilder();
		}
		this.blockContainer.load(payload);
		var blockNames = ['generalBlock', 'parametersBlock', 'preprocessorBlock',
			'parserBlock', 'assessBlock', 'assignValuesToOutputsBlock', 'severitiesAndConditionsBlock',
			'summaryAndDetailsBlock', 'mockBlock', 'tagsBlock', 'referencesBlock'];
		this.blockContainer.showBlocks(blockNames);

		if (this.embed && this.itemList) {
			this.resizeNormalAllSection(true);
		}
	},

	failActionTaskLoad: function(payload) {
		if (this.id) {
			this.message({
				title: this.localize('errorTitle'),
				msg: payload.message,
				buttons: Ext.MessageBox.OK,
				buttonText: {
					ok: this.localize('close')
				},
				scope: this,
				fn: function(btn) {
					this.accessActiontTaskList();
				}
			});
		} else {
			this.initNew();
			this.set('id', '');
			this.set('namespace', '');
			this.set('name', '');
		}
	},

	isembedded$: function() {
		return this.embed;
	},

	actionTaskPadding$: function() {
		var padding = 0;

		if (!this.embed) {
			padding = 10;
		}

		return padding;
	},

	accessActiontTaskList: function() {
		clientVM.handleNavigation({
			modelName: 'RS.actiontask.Main'
		})
	},

	isEditable$: function() {
		return this.editable;
	},

	isExecutable$: function() {
		if (this.isDirty) {
			return false;
		}
		return this.executable || !this.isNew;
	},

	execution: function(button) {
		if (this.executionWin) {
			this.executionWin.doClose();
		}

		this.executionWin = this.open({
			mtype: 'RS.actiontaskbuilder.Execute',
			target: button.getEl().id,
			executeDTO: {
				actiontask: this.ufullName,
				actiontaskSysId: this.id,
				action: 'TASK'
			}
		});
	},

	isNew: false,

	fileOptions: function() {},
	
	edit: function() {
		this.showAllEditSections();
		this.set('readOnlyMode', false);
		//this.set('sectionButtonSelected', this.sectionButtonMap['ALL']);
	},

	exitToView: function() {
		if (this.isDirty && !this.embed) {
			this.message({
				title: this.localize('exitWithoutSaveTitle'),
				msg: this.localize('exitWithoutSaveBody'),
				buttons: Ext.MessageBox.YESNOCANCEL,
				buttonText: {
					yes: this.localize('save'),
					no: this.localize('dontSave'),
					cancel: this.localize('cancel')
				},
				scope: this,
				fn: function(btn) {
					if (btn == "yes") {
						this.doSave(function() {
							this.doExitToView();
						}.bind(this));
					} else if (btn == "no") {
						this.reloadActionTask(function(){
							this.doExitToView();
						}.bind(this));
					}
				}
			})
		} else {
			this.doExitToView();
		}
	},

	exitToViewIsEnabled$: function(){
		return !this.isNew;
	},

	doExitToView: function() {
		this.showAllReadSections();
		this.set('readOnlyMode', true);
		//this.set('sectionButtonSelected', this.sectionButtonMap['ALL']);
	},

	editIsEnabled$: function() {
		if (this.preview) {
			return false;
		} else {
			return true;
		}
	},

	save: function() {
		this.doSave();
	},

	doSave: function (onComplete, commit, comment) {
		var isNew = this.isNew;
		this.saveActionTask(false, null, function(payload) {
			//To indicate we're still working on same AT after save.
			window.loadingNewAT = false;
			if (isNew) {
				this.successfulActionTaskLoad(payload);
			} else {
				this.blockContainer.load(payload);
				if (this.version) {
					this.set('version', '');
					clientVM.removeURLHashVersion();
					this.reloadActionTask();
				} 
			}
			if (Ext.isFunction(onComplete)) {
				onComplete(payload);
			}
		}.bind(this), null, commit, comment);
	},

	saveAs: function() {
		var me = this;
		this.open({
			mtype: 'RS.actiontask.MoveOrRenameOrCopy',
			uname: this.uname,
			unamespace: this.unamespace,
			action: 'saveAs',
			actionText: this.saveAsTitle,
			dumper: function(action, namespace, name) {
				me.saveActionTask(false, function(actionTask) {
					var nullTheId = function(obj) {
						for (key in obj) {
							if (key == 'assignedTo' || !obj[key])
								continue;
							if (key == 'id' || key == 'sysId' || key == 'sys_id')
								obj[key] = '';
							if (Ext.isObject(obj[key]))
								nullTheId(obj[key]);
							if (Ext.isArray(obj[key]))
								Ext.each(obj[key], function(o) {
									nullTheId(o)
								});
						}
					}
					nullTheId(actionTask);
					actionTask.uname = name;
					actionTask.unamespace = namespace;
				}, function(actiontask) {
					// reload all sections with newly save as actiontask data
					me.set('id', actiontask.data.id);
					me.set('name', name);
					me.set('namespace', namespace);
					me.reloadActionTask(function() {
						me.allBtn();
					});
					me.modifyHash(actiontask.data.id, true);
				});
			}
		});
	},

	verifyCommitVersion: function(comment) {
		this.ajax({
			url: '/resolve/service/actiontask/getHistory',
			params: {
				id: this.id,
				docFullName: this.ufullName
			},
			method: 'GET',
			success: function(resp) {
				var response = RS.common.parsePayload(resp);
				if (response.success) {
					if (response.records && response.records.length && response.records[0].version > this.uversion) {
						this.displayCommitWarning(comment);
					} else {
						this.saveAndCommit(comment);
					}
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	displayCommitWarning: function(comment) {
		this.message({
			title: this.localize('attention'),
			msg: this.localize('newVersionsAvailabe'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('commitNewVersion'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn == 'yes') {
					this.saveAndCommit(comment);
				}
			},
		});
		this.doClose()
	},

	saveAndCommit: function(comment) {
		if (!this.embed) {
			this.doSave(function() {
				this.doExitToView();
			}.bind(this), true, comment);
		} else {
			this.doExitToView();
		}
	},

	commit: function() {
		this.open({
			mtype: 'RS.actiontaskbuilder.Commit'
		})
	},
	commitIsEnabled$: function() {
		return !this.uisStable || this.isDirty;
	},

	viewVersionList: function() {
		this.open({
			mtype: 'RS.actiontaskbuilder.Version',
			name : this.ufullName,
			uname: this.uname,
			id : this.id,
			unamespace: this.unamespace,
			dumper: {
				dump: function(selections) {
					if (selections.length > 0) {
						var selection = selections[0];
						this.loadSelectedVersion(selection.get('version'));
					}
				},
				scope: this
			}
		});
	},
	loadSelectedVersion: function(version) {
		if (this.isDirty && !this.embed) {
			this.message({
				title: this.localize('loadVersionWithoutSaveTitle', version),
				msg: this.localize('loadVersionWithoutSaveBody'),
				buttons: Ext.MessageBox.YESNOCANCEL,
				buttonText: {
					yes: this.localize('save'),
					no: this.localize('dontSave'),
					cancel: this.localize('cancel')
				},
				scope: this,
				fn: function(btn) {
					if (btn == "yes") {
						this.doSave(function() {
							this.doLoadSelectedVersion(version);
						}.bind(this));
					} else if (btn == "no") {
						this.doLoadSelectedVersion(version);
					}
				}
			})
		} else {
			this.doLoadSelectedVersion(version);
		}
	},

	doLoadSelectedVersion: function(version) {
		this.set('version', version);
		clientVM.updateURLHashVersion(this.version);
		this.reloadActionTask();
	},

	saveActionTask: function(exitAfterSave, beforeSave, onSuccess, onFailure, commit, comment) {
		var actionTask = this.gatherData();
		if (!actionTask) {
			this.set('saveIsEnabled', true);
			clientVM.displayError(this.localize('SaveErr'));
			return;
		}

		var data = actionTask.data;
		if (!data.uname && !data.unamespace) {
			this.set('saveIsEnabled', true);
			return;
		}

		//Work around : AssignedTo field should be null if there is no user is assignedTo to the task.
		if(!data.assignedTo || !data.assignedTo.hasOwnProperty('id'))
			data.assignedTo = null;

		if (typeof beforeSave === 'function') {
			actiontask = beforeSave(actionTask.data);
		}

		var params = {};
		var url = this.API.saveAT;
		if (commit) {
			params = {
				comment: comment
			}
			url = this.API.commitAT;
			actionTask.data.uisStable = true;
		} else {
			actionTask.data.uisStable = false;
		}

		this.set('saveIsEnabled', false);

		this.ajax({
			url: url,
			jsonData: actionTask.data,
			params: params,
			success: function(response) {
				var payload = RS.common.parsePayload(response);

				if (payload.success) {
					if (exitAfterSave === true) {
						clientVM.handleNavigation({
							modelName: 'RS.actiontask.Main'
						});
					}

					clientVM.displaySuccess(this.saveSuccess);

					this.blockContainer.saveComplete();

					if (typeof onSuccess === 'function') {
						onSuccess(payload);
					}
				} else {
					if (typeof onFailure === 'function') {
						onFailure();
					}

					clientVM.displayError(this.localize('SaveErr') + '[' + payload.message + ']')
				}
			},
			failure: function(response) {
				clientVM.displayFailure(response);
			}
		});
	},

	gatherData: function() {
		return this.blockContainer.getData();
	},

	gatherOriginalData: function() {
		return this.blockContainer.getOriginalData();
	},

	loadOriginalData: function(originalData, originalDirtyFlag) {
		this.blockContainer.loadOriginalData(originalData, originalDirtyFlag);
	},

	subscribeDirty: function(fn) {
		return this.blockContainer.subscribeDirty(fn);
	},

	rename: function() {
		this.open({
			mtype: 'RS.actiontask.MoveOrRenameOrCopy',
			action: 'rename',
			uname: this.uname,
			unamespace: this.unamespace,
			dumper: function(action, unamespace, uname, overwrite) {
				this.message({
					title: this.localize('RenameTitle'),
					msg: this.localize('RenameMsg'),
					buttons: Ext.MessageBox.YESNO,
					buttonText: {
						yes: this.localize('rename'),
						no: this.cancelButtonText
					},
					scope: this,
					fn: function(btn) {
						if (btn !== 'no') {
							this.doRename(this.localize('RenameSuc'), this.localize('RenameErr'), {
								ids: [this.id],
								action: 'rename',
								module: unamespace,
								name: uname,
								overwrite: overwrite
							});
						}
					}
				})
			}.bind(this)
		});
	},

	doRename: function(sucMsg, errMsg, params) {
		this.sendReq(sucMsg, errMsg, params);
	},

	sendReq: function(sucMsg, errMsg, params) {
		this.set('saveIsEnabled', false);
		this.ajax({
			url: '/resolve/service/actiontask/actiontaskmanipulation',
			params: params,
			success: function(response) {
				var payload = RS.common.parsePayload(response);

				if (!payload.success) {
					clientVM.displayError(errMsg, payload.message);
				}
				else if (payload.records && payload.records.length > 0) {
                    this.open({
                        mtype: 'RS.actiontask.BulkOpWarningList',
                        msg: payload.message,
                        records: payload.records,
                        action: params.action,
                        dumper: (function(self) {
                            return function() {
                                self.sendReq(sucMsg, errMsg, Ext.apply(params, {
                                    overwrite: true
                                }));
                            }
                        })(this)
                    });
                }
                else{
					clientVM.displaySuccess(sucMsg);
					this.reloadActionTask();
				}
			},
			failure: function(response) {
				clientVM.displayFailure(response);
			}
		})
	},

	/*
	reference: function() {
		clientVM.handleNavigation({
			modelName: 'RS.actiontaskbuilder.GeneralReference',
			params: {
				id: this.id
			},
			target: '_blank'
		})
	},
	*/

	sysInfo: function() {
		this.open({
			mtype: 'RS.common.SysInfo',
			sysId: this.id,
			sysCreatedOn: this.sysCreatedOn,
			sysCreatedBy: this.sysCreatedBy,
			sysUpdatedOn: this.sysUpdatedOn,
			sysUpdatedBy: this.sysUpdatedBy
		})
	},

	reload: function() {
		if (this.isDirty) {
			this.message({
				title: this.localize('reloadWithoutSaveTitle'),
				msg: this.localize('reloadWithoutSaveBody'),
				buttons: Ext.MessageBox.YESNO,
				buttonText: {
					yes: this.localize('confirm'),
					no: this.localize('cancel')
				},
				scope: this,
				fn: function(btn) {
					if (btn === 'yes') {
						this.reloadActionTask(function() {
							this.allBtn();
						}.bind(this));
					}
				}
			});
		} else {
			this.reloadActionTask(function() {
				this.allBtn();
			}.bind(this));
		}
	},

	reloadActionTask: function(onComplete) {
		this.loadActionTask(function(payload) {
			this.blockContainer.load(payload);
			if (Ext.isFunction(onComplete)) {
				onComplete();
			}
		}.bind(this));
	},

	afterRendered: function(theView) {
		this.view = theView;
		this.viewOnly = theView.viewOnly;
		this.set('editable', !this.viewOnly && this.editable);
		if (this.editable ) {
			this.enableEditLinks();
		} else {
			this.disableEditLinks();
		}
	},

	userDateFormat$: function() {
		return this.dateFormat;
	},

	isDisabled$: function() {
		return !this.editable;
	},

	enableEditLinks: function() {
		for (var i=0; i < this.itemList.length; i++) {
			var item = this.itemList.getAt(i),
				view = item.view;
			if (view && view.header) {
				if (view.header.items.length) {
					var headerItems = view.header.items.items;

					if (headerItems) {
						for (var j=0; j < headerItems.length; j++) {
							var headerItem = headerItems[j];
							if (headerItem.isEdit && typeof headerItem.show === 'function') {
								headerItem.show();
							}
						}
					}
				}
			}
		}
	},

	disableEditLinks: function() {
		for (var i=0; i < this.itemList.length; i++) {
			var item = this.itemList.getAt(i),
				view = item.view;

			if (view && view.header) {

				if (view.header.items.length) {
					var headerItems = view.header.items.items;

					if (headerItems) {
						for (var j=0; j < headerItems.length; j++) {
							var headerItem = headerItems[j];
							if (headerItem.isEdit && typeof headerItem.hide === 'function') {
								headerItem.hide();
							}
						}
					}
				}
			}
		}
	},

	incNumSectionsOpen: function(section) {
		if (!this.sectionsOpenList[section.blockModelClass]) {
			this.sectionsOpenList[section.blockModelClass] = true;
		this.set('numSectionsOpen', ++this.numSectionsOpen);
		}
	},
	decNumSectionsOpen: function(section) {
		if (this.sectionsOpenList[section.blockModelClass]) {
			if (section.ignoreDecrementFlag) {
				section.ignoreDecrementFlag = false;
			} else {
				this.sectionsOpenList[section.blockModelClass] = false;
		this.set('numSectionsOpen', --this.numSectionsOpen);
		if (this.numSectionsOpen < 0) {
			this.set('numSectionsOpen', 0);
		}
			}
		}
	},

	sectionButtonSelected: 0,
	sectionButtonMap : {
		ALL : 0,
		GENERAL : 1,
		REFERENCE : 2,
		PARAMETERS : 3,
		PREPROCESSOR : 4,
		CONTENT : 5,
		PARSER : 6,
		ASSESS : 7,
		OUTPUT : 8,
		SEVERITYCONDITION : 9,
		SUMMARYDETAILS : 10,
		MOCK : 11,
		TAGS : 12
	},

	toolbarSetting: 0,
	toolbarSettingMap : {
		NONE : 1,
		ICONONLY: 2,
		TEXTONLY: 3,
		TEXTANDICON: 4
	},

	when_id_changed: {
		on: ['idChanged'],
		action: function() {
			this.set('isNew', !this.id);
			this.updateToolbarSettingDisplay();
		}
	},

	when_toolbarSetting_changed: {
		on: ['toolbarSettingChanged'],
		action: function() {
			this.updateToolbarSettingDisplay();
		}
	},

	readOnlyChangeFlag: false,
	when_readOnlyMode_changed: {
		on: ['readOnlyModeChanged'],
		action: function() {
			if (!this.isNew) {
				var currentSectionButtonSelected = this.sectionButtonSelected;
				this.set('sectionButtonSelected', null);
				setTimeout(function() {
					this.readOnlyChangeFlag = true;

					switch(currentSectionButtonSelected) {
					case this.sectionButtonMap['GENERAL']:
						this.generalBtn();
						break;
					case this.sectionButtonMap['REFERENCE']:
						this.referenceBtn();
						break;
					case this.sectionButtonMap['PARAMETERS']:
						this.parametersBtn();
						break;
					case this.sectionButtonMap['PREPROCESSOR']:
						this.preprocessorBtn();
						break;
					case this.sectionButtonMap['CONTENT']:
						this.contentBtn();
						break;
					case this.sectionButtonMap['PARSER']:
						this.parserBtn();
						break;
					case this.sectionButtonMap['ASSESS']:
						this.assessBtn();
						break;
					case this.sectionButtonMap['OUTPUT']:
						this.outputsBtn();
						break;
					case this.sectionButtonMap['SEVERITYCONDITION']:
						this.severityConditionsBtn();
						break;
					case this.sectionButtonMap['SUMMARYDETAILS']:
						this.summaryDetailsBtn();
						break;
					case this.sectionButtonMap['MOCK']:
						this.mockBtn();
						break;
					case this.sectionButtonMap['TAGS']:
						this.tagsBtn();
						break;
					case this.sectionButtonMap['ALL']:
					default:
						this.readOnlyChangeFlag = false;
						this.syncExpandedSectionsFlag = true;
						this.allBtn();
						break;
					}
				}.bind(this), 200);
			}
		}
	},

	updateToolbarSettingDisplay: function() {
		var sectionButtonDisabled = false,
			iconShowSectionNone = 'rs-social-menu-button',
			iconShowSectionTextOnly = 'rs-social-menu-button',
			iconShowSectionIconAndText = 'rs-social-menu-button',
			iconShowSectionIconOnly = 'rs-social-menu-button';

		switch(this.toolbarSetting) {
		case this.toolbarSettingMap['NONE']:
			sectionButtonDisabled = true;
			iconShowSectionNone += ' icon-check';
			break;
		case this.toolbarSettingMap['TEXTONLY']:
			iconShowSectionTextOnly += ' icon-check';
			break;
		case this.toolbarSettingMap['TEXTANDICON']:
			iconShowSectionIconAndText += ' icon-check';
			break;
		case this.toolbarSettingMap['ICONONLY']:
			iconShowSectionIconOnly += ' icon-check';
			break;
		default:
			break;
		}

		this.set('sectionButtonDisabled', sectionButtonDisabled || this.isNew);
		this.set('iconShowSectionNone', iconShowSectionNone);
		this.set('iconShowSectionTextOnly', iconShowSectionTextOnly);
		this.set('iconShowSectionIconAndText', iconShowSectionIconAndText);
		this.set('iconShowSectionIconOnly', iconShowSectionIconOnly);

		Ext.state.Manager.set('actionTaskToolbarSetting', this.toolbarSetting);
	},

	sectionButtonHidden$: function() {
		return this.sectionButtonDisabled || this.isNew;
	},

	hideNewActionTaskToolBar$: function() {
		return this.isembedded || !this.sectionButtonHidden;
	},

	hideActionTaskToolBar$: function() {
		return this.isembedded || this.sectionButtonHidden;
	},

	showSectionNone: function() {
		this.set('toolbarSetting', this.toolbarSettingMap['NONE']);
	},
	showSectionTextOnly: function() {
		this.set('toolbarSetting', this.toolbarSettingMap['TEXTONLY']);
	},
	showSectionTextIcon: function() {
		this.set('toolbarSetting', this.toolbarSettingMap['TEXTANDICON']);
	},
	showSectionIconOnly: function() {
		this.set('toolbarSetting', this.toolbarSettingMap['ICONONLY']);
	},

	sectionButtonDisabled: false,
	iconShowSectionNone: 'rs-social-menu-button',
	iconShowSectionTextOnly: 'rs-social-menu-button',
	iconShowSectionIconAndText: 'rs-social-menu-button',
	iconShowSectionIconOnly: 'rs-social-menu-button',

	toolbarSettingsIsEnabled$: function() {
		return !this.isNew;
	},

	allText: '',
	allBtn: function() {
		if (!this.allBtnIsPressed) {
			this.resizeNormalAllSection();
		}
	},
	allBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['ALL'];
	},
	allBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-th';
		}
		return '';
	},
	allBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.allText;
		}
		return '';
	},
	allBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.allText;
		}
		return '';
	},

	generalText: '',
	generalBtn: function() {
		if (!this.generalBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.generalRead.gluModel, this.sectionButtonMap['GENERAL']);
			} else {
				this.resizeMaxSection(this.generalEdit.gluModel, this.sectionButtonMap['GENERAL']);
			}
		}
	},
	generalBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['GENERAL'];
	},
	generalBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-info-sign';
		}
		return '';
	},
	generalBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.generalText;
		}
		return '';
	},
	generalBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.generalText;
		}
		return '';
	},

	referenceText: '',
	referenceBtn: function() {
		if (!this.referenceBtnIsPressed) {
			this.resizeMaxSection(this.referencesRead.gluModel, this.sectionButtonMap['REFERENCE']);
		}
	},
	referenceBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['REFERENCE'];
	},
	referenceBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-book';
		}
		return '';
	},
	referenceBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.referenceText
		}
		return '';
	},
	referenceBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.referenceText;
		}
		return '';
	},

	parametersText: '',
	parametersBtn: function() {
		if (!this.parametersBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.parametersRead.gluModel, this.sectionButtonMap['PARAMETERS']);
			} else {
				this.resizeMaxSection(this.parametersInputEdit.gluModel, this.sectionButtonMap['PARAMETERS']);
			}
		}
	},
	parametersBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['PARAMETERS'];
	},
	parametersBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-gear';
		}
		return '';
	},
	parametersBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.parametersText;
		}
		return '';
	},
	parametersBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.parametersText;
		}
		return '';
	},

	preprocessorText: '',
	preprocessorBtn: function() {
		if (!this.preprocessorBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.preprocessorRead.gluModel, this.sectionButtonMap['PREPROCESSOR']);
			} else {
				this.resizeMaxSection(this.preprocessorEdit.gluModel, this.sectionButtonMap['PREPROCESSOR']);
			}
		}
	},
	preprocessorBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['PREPROCESSOR'];
	},
	preprocessorBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-gears';
		}
		return '';
	},
	preprocessorBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.preprocessorText;
		}
		return '';
	},
	preprocessorBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.preprocessorText;
		}
		return '';
	},

	contentSectionVisible: false,
	contentText: '',
	contentBtn: function() {
		if (!this.contentBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.contentRead.gluModel, this.sectionButtonMap['CONTENT']);
			} else {
				this.resizeMaxSection(this.contentEdit.gluModel, this.sectionButtonMap['CONTENT']);
			}
		}
	},
	contentBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['CONTENT'];
	},
	contentBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-edit';
		}
		return '';
	},
	contentBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.contentText;
		}
		return '';
	},
	contentBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.contentText;
		}
		return '';
	},
	contentBtnDisabled$: function() {
		return !this.contentSectionVisible || this.isNew || this.sectionButtonHidden;
	},

	parserText: '',
	parserBtn: function() {
		if (!this.parserBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.parserRead.gluModel, this.sectionButtonMap['PARSER']);
			} else {
				this.resizeMaxSection(this.parserEdit.gluModel, this.sectionButtonMap['PARSER']);
			}
		}
	},
	parserBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['PARSER'];
	},
	parserBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-code';
		}
		return '';
	},
	parserBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.parserText;
		}
		return '';
	},
	parserBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.parserText;
		}
		return '';
	},

	assessText: '',
	assessBtn: function() {
		if (!this.assessBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.assessRead.gluModel, this.sectionButtonMap['ASSESS']);
			} else {
				this.resizeMaxSection(this.assessEdit.gluModel, this.sectionButtonMap['ASSESS']);
			}
		}
	},
	assessBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['ASSESS'];
	},
	assessBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-eye-open';
		}
		return '';
	},
	assessBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.assessText;
		}
		return '';
	},
	assessBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.assessText;
		}
		return '';
	},

	outputsText: '',
	outputsBtn: function() {
		if (!this.outputsBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.assignValuesToOutputsRead.gluModel, this.sectionButtonMap['OUTPUT']);
			} else {
				this.resizeMaxSection(this.assignValuesToOutputsEdit.gluModel, this.sectionButtonMap['OUTPUT']);
			}
		}
	},
	outputsBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['OUTPUT'];
	},
	outputsBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-circle-arrow-right';
		}
		return '';
	},
	outputsBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.outputsText;
		}
		return '';
	},
	outputsBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.outputsText;
		}
		return '';
	},

	severityConditionsText: '',
	severityConditionsBtn: function() {
		if (!this.severityConditionsBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.severitiesAndConditionsRead.gluModel, this.sectionButtonMap['SEVERITYCONDITION']);
			} else {
				this.resizeMaxSection(this.severitiesEdit.gluModel, this.sectionButtonMap['SEVERITYCONDITION']);
			}
		}
	},
	severityConditionsBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['SEVERITYCONDITION'];
	},
	severityConditionsBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-flag-alt';
		}
		return '';
	},
	severityConditionsBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.severityConditionsText;
		}
		return '';
	},
	severityConditionsBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.severityConditionsText;
		}
		return '';
	},

	summaryDetailsText: '',
	summaryDetailsBtn: function() {
		if (!this.summaryDetailsBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.summaryAndDetailsRead.gluModel, this.sectionButtonMap['SUMMARYDETAILS']);
			} else {
				this.resizeMaxSection(this.summaryEdit.gluModel, this.sectionButtonMap['SUMMARYDETAILS']);
			}
		}
	},
	summaryDetailsBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['SUMMARYDETAILS'];
	},
	summaryDetailsBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-file-text-alt';
		}
		return '';
	},
	summaryDetailsBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.summaryDetailsText;
		}
		return '';
	},
	summaryDetailsBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.summaryDetailsText;
		}
		return '';
	},

	mockText: '',
	mockBtn: function() {
		if (!this.mockBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.mockRead.gluModel, this.sectionButtonMap['MOCK']);
			} else {
				this.resizeMaxSection(this.mockEdit.gluModel, this.sectionButtonMap['MOCK']);
			}
		}
	},
	mockBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['MOCK'];
	},
	mockBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-exchange';
		}
		return '';
	},
	mockBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.mockText;
		}
		return '';
	},
	mockBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.mockText;
		}
		return '';
	},

	tagsText: '',
	tagsBtn: function() {
		if (!this.tagsBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.tagsRead.gluModel, this.sectionButtonMap['TAGS']);
			} else {
				this.resizeMaxSection(this.tagsEdit.gluModel, this.sectionButtonMap['TAGS']);
			}
		}
	},
	tagsBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['TAGS'];
	},
	tagsBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-tags';
		}
		return '';
	},
	tagsBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.tagsText;
		}
		return '';
	},
	tagsBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.tagsText;
		}
		return '';
	},

	expandAllDisabled$: function() {
		return (this.isNew || this.numSectionsOpen > 0 || this.isMaxScreenSize);
	},
	expandAll: function() {
		for (var i=0; i < this.itemList.length; i++) {
			var item = this.itemList.getAt(i);
			if (!item.hidden && item.collapsed) {
				item.set('collapsed', false);
			}
		}
	},

	collapseAllDisabled$: function() {
		return (this.isNew || this.numSectionsOpen == 0 || this.isMaxScreenSize);
	},
	collapseAll: function() {
		this.closeDetailSections();
		for (var i=0; i < this.itemList.length; i++) {
			var item = this.itemList.getAt(i);
			if (!item.hidden) {
				item.set('collapsed', true);
			}
		}
		setTimeout(function() {
			if (this.numSectionsOpen) {
				this.set('numSectionsOpen', 0);
			}
		}.bind(this), 500);
	},
	syncExpandedSections: function() {
		for (var i=0; i < this.itemList.length; i++) {
			var item = this.itemList.getAt(i);
			if (!item.hidden) {
				if (this.sectionsOpenList[item.blockModelClass]) {
					if (item.collapsed) {
						item.set('collapsed', false);
					}
				} else {
					if (!item.collapsed) {
						item.set('collapsed', true);
					}
				}
			}
		}
		this.syncExpandedSectionsFlag = false;
	},

	expandCollapseSeparatorHidden$: function() {
		return this.collapseAllDisabled && this.expandAllDisabled;
	},

	getAllTags: function (onSuccess, onFailure) {
		this.set('getAllTagsInProgress', true);
		this.ajax({
			url: this.API['getAllTags'],
			method: 'get',
			scope: this,
			success: function (response) {
				var payload = RS.common.parsePayload(response);

				if (payload.success) {
					if (Ext.isFunction(onSuccess)) {
						onSuccess(payload.records);
					}
				} else {
					//clientVM.displayError(this.localize('serverError', payload.message));
					if (Ext.isFunction(onFailure)) {
						onFailure(response);
					}
				}
			},
			failure: function (response) {
				clientVM.displayFailure(response);
				if (Ext.isFunction(onFailure)) {
					onFailure(response);
				}
			},
			callback: function() {
				this.set('getAllTagsInProgress', false);
			}
		});
	},

	getAllATProperties: function(onSuccess, onFailure) {
		this.set('getAllATPropertiesInProgress', true);
		this.ajax({
			url: this.API['getAllATProperties'],
			scope: this,
			success : function (response){
				var payload = RS.common.parsePayload(response);

				if (payload.success) {
					if (Ext.isFunction(onSuccess)) {
						onSuccess(payload.records);
					}
				} else {
					//clientVM.displayError(this.localize('serverError', payload.message));
					if (Ext.isFunction(onFailure)) {
						onFailure(response);
					}
				}
			},
			failure: function (response) {
				clientVM.displayFailure(response);
				if (Ext.isFunction(onFailure)) {
					onFailure(response);
				}
			},
			callback: function() {
				this.set('getAllATPropertiesInProgress', false);
			}
		});
	}

});
