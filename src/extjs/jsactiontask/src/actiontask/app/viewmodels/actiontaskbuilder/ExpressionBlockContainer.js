glu.defModel('RS.actiontaskbuilder.ExpressionBlockContainer', {
	allParameters : {
		INPUT : [],
		OUTPUT : [],
		FLOW : [],
		WSDATA : [],
		PROPERTY : []
	},	
	category: 0,
	isReadOnly: false,
	title: '',
	style: {
		borderColor: '#f00'
	},
	isAndHidden: true,
	isReadOnly: false,
	isHidden: false,
	blocks: {
		mtype: 'list'
	},

	initContainer: function (level, title, color, readOnly) {
		this.category = level;
		this.set('title', this.localize(title));
		this.set('style', { borderColor: color });	
		this.isReadOnly = !!readOnly;

		if (this.isReadOnly) {
			this.set('isHidden', true);
		}
	},

	isCategoryForExpressionChanged : function(expressionCategory){
        return expressionCategory != this.category;
    },

    reclassifyExpression : function(expression){
    	this.parentVM.reclassifyExpression(expression);
    },

    updateCategoryForExpression : function(expression){
    	var order = this.blocks.length - 1;
    	var lastExpressionBlock = this.blocks.getAt(order);
    	lastExpressionBlock.updateCategoryForExpression(expression, order);
    },

	load: function (expression, order) {
		expression.isExpressionHidden = false;

		while (this.count() <= order) {
			this.addBlock();
		}

		var blocks = this.blocks.toArray();
		blocks[order].add(expression);

		if (this.isHidden) {
			this.set('isHidden', false);
		}
	},

	enableAdd: function () {
		this.addBlock();
		var blocks = this.blocks.toArray();

		for (var i = 0; i < blocks.length; i++) {
			blocks[i].addEmpty();				
		}
	},

	getData: function (expressionType) {
		var data = [];
		var blocks = this.blocks.toArray();
		var indexOfLastBlock = this.count() - 1;

		if (!this.isReadOnly) {
			indexOfLastBlock--;
		}

		for (var i = 0; i <= indexOfLastBlock; i++) {
			data.push.apply(data, blocks[i].getData(expressionType, i));
		}

		return data;
	},

	addBlock: function () {
		var block = this.model('RS.actiontaskbuilder.ExpressionBlock', { category: this.category });
		block.initBlock(this.isReadOnly);
		block.updateParameters(this.allParameters);
		block.updateProperties(this.properties);
		this.blocks.add(block);
		this.set('isAndHidden', this.count() < 2);
		return block;
	},

	removeBlock: function (block) {
		this.blocks.remove(block);
		this.set('isAndHidden', this.count() < 2);
	},

	updateParameters: function (data) {
		for(var type in data){
			var transformedData = [];
			for(var i = 0; i < data[type].length; i++){
				transformedData.push({
					name : data[type][i]
				})
			}
			this.allParameters[type] = transformedData;
		}
		this.blocks.foreach(function(block){
			block.updateParameters(this.allParameters);
		},this)
	},

	updateProperties: function (allProperties) {
		this.properties = allProperties;
		var items = this.blocks.toArray();

		for (var i = 0; i < items.length; i++) {
			items[i].updateProperties(allProperties);
		}
	},

	clear: function () {	
		this.blocks.foreach(function(block){
			block.clear();
		})
		this.blocks.removeAll();
		this.fireEvent('onClear');
		this.set('isAndHidden', true);
	},

	isEmpty: function () {
		return this.count() === 0;
	},

	count: function () {
		return this.blocks.length;
	},

	isLastBlock: function (block) {
		var count = this.count();
		return count >= 0 && count - 1 === this.blocks.indexOf(block);
	}
});