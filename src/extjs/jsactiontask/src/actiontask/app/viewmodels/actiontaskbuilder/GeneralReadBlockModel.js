Ext.define('RS.actiontaskbuilder.GeneralReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',
	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'General',
		hidden: false,
		collapsed: true,
        title: '',
        header: {
            titlePosition: 0,
            defaults: {
            	margin: 0
        	},
            items: []
        },
		type: '',
		id: '',
		namespace: '',
		name: '',
		timeout: 300,
		assignedToUserName: '',
		assignedToUName: '',
		summary: '',
		description: '',
		active: true,
		logResult: true,
		displayInWorksheet: true,
		jumpToAssignedToIsHidden: true,

		// roles
		accessStore: {
			mtype : 'store',
			fields: ['uname', 'read', 'write', 'execute', 'admin'],
			data: []
		},
		hideAdminColumn: true,
		defaultRoles: null,
		hideAccessRights: true,
		defaultRolesMessage: '',
		hideAccessRights: true,
		// end roles

		// options
		optionsStore: null,
		hasOptions: false,
		// end options

		postInit: function (blockModel) {
			this.blockModel = blockModel;
			var hidden = true;

			if (clientVM.user.roles === 'undefined' || typeof clientVM.user.roles === null) {
				// resolve.maintenance user
				hidden = false;
			} else {
				if (clientVM.user.roles && clientVM.user.roles.constructor === Array) {
					hidden = clientVM.user.roles.indexOf('admin') === -1;
				}
			}

			this.set('hideAdminColumn', hidden);
			this.set('title', this.localize('rolesReadTitle'));
			this.set('defaultRolesMessage', this.localize('defaultRolesMessage'));
			this.set('optionsStore', new RS.common.data.FlatStore({
				fields: ['uname', 'uvalue', 'udescription']
			}));
		},

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden && this.parentVM.embed) {
					var generalData = this.parentVM.generalEdit.getData(),
						accessRightsData = this.parentVM.rolesEdit.getData(),
						optionsData = this.parentVM.optionsEdit.getData();

					var data = generalData[0],
						resolveActionInvoc = generalData[1],
						assignedTo = generalData[2],
						accessRights = accessRightsData[1],
						options = optionsData[0];

					Ext.apply(data, {
						uisDefaultRole: accessRightsData[0].uisDefaultRole,
					});

					this.parentVM.generalRead.load(data, resolveActionInvoc, assignedTo, accessRights, options);
				}
			}
		},

		adminRights: function (column) {
			if (this.hideAdminColumn) {
				column.hide();
			} else {
				column.show();
			}
		},

		jumpToAssignedTo: function() {
			clientVM.handleNavigation({
				modelName: 'RS.user.User',
				params: {
					username: this.assignedToUName
				},
				target: '_blank'
			})
		},

		jumpToAssignedToIsHidden$ : function(){
			return !this.assignedToUName;
		}
	},

	getDataPaths: function () {
		return [
			'data', 'data.resolveActionInvoc', 'data.assignedTo', // general
			'data.accessRights', // roles
			{ // options
				path:'data.resolveActionInvoc.resolveActionInvocOptions',
				filter: function (o) {
					return o.uname !== 'INPUTFILE';
				}
			}];
	},

	load: function (data, resolveActionInvoc, assignedTo, accessRights, options) {
		assignedTo = assignedTo || {};

		var desc = data.udescription || '',
			timeout = resolveActionInvoc.utimeout || 1,
			secondMessage = '',
			displayName = assignedTo.udisplayName || '',
			g = this.gluModel;

		if (desc.length === 0) {
			desc = g.localize('noDescription');
		} else {
			desc = Ext.String.htmlEncode(desc).replace(/(\r\n|\n|\r)/g, '<br />');
		}

		if (timeout === 1) {
			secondMessage = g.localize('timeoutValueSingular');
		} else {
			secondMessage = g.localize('timeoutValue');
		}

		if (displayName.length === 0) {
			displayName = g.localize('noAssignedUser');
		}

		this.set('description', desc)
			.set('id', data.id)
			.set('namespace', data.unamespace)
			.set('name', data.uname)
			.set('summary', data.usummary)
			.set('active', data.uactive)
			.set('logResult', data.ulogresult)
			.set('displayInWorksheet', !data.uisHidden)
			.set('type', resolveActionInvoc.utype.toUpperCase())
			.set('timeout', [timeout, secondMessage].join(' '))
			.set('assignedToUserName', displayName)
		    .set('assignedToUName', assignedTo ? assignedTo.uuserName : '');

		// roles
		this.set('defaultRoles', data.uisDefaultRole);
		var data = accessRights;
		if (accessRights.hasOwnProperty('ureadAccess'))
			data = this.shapeData(accessRights);
		g.accessStore.loadData(data);
		this.set('hideAccessRights', g.accessStore.count() === 0);

		// options
		g.optionsStore.loadData(options);
		this.set('hasOptions', options.length > 0);
	},
	shapeData : function(accessRights) {
		function consolidateArrays(destination, source) {
			for (var i = 0; i < source.length; i++) {
				if (destination.indexOf(source[i]) === -1) {
					destination.push(source[i]);
				}
			}
		}
		var data = [];

		if (accessRights) {
			var	readUsers = accessRights.ureadAccess.length > 0 ? accessRights.ureadAccess.split(',') : [],
				writeUsers = accessRights.uwriteAccess.length > 0 ? accessRights.uwriteAccess.split(',') : [],
				executeUsers = accessRights.uexecuteAccess.length > 0 ? accessRights.uexecuteAccess.split(',') : [],
				adminUsers = accessRights.uadminAccess.length > 0 ? accessRights.uadminAccess.split(',') : [],
				users = readUsers.slice();
			consolidateArrays(users, writeUsers);
			consolidateArrays(users, executeUsers);
			consolidateArrays(users, adminUsers);

			for (var i = 0; i < users.length; i++) {
				var u = users[i];
				data.push({
					uname: u,
					read: readUsers.indexOf(u) !== -1,
					write: writeUsers.indexOf(u) !== -1,
					execute: executeUsers.indexOf(u) !== -1,
					admin: adminUsers.indexOf(u) !== -1
				});
			}
		}

		return data;
	}
})
