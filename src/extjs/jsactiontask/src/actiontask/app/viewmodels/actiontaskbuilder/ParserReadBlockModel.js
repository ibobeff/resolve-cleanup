Ext.define('RS.actiontaskbuilder.ParserReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',
	modelConfig : {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Parser',
		collapsed: true,
		classList: ['block-model'],
		title: '',
		hidden: false,
		hasNoContent : false,	
		header: {
            titlePosition: 0,
            defaults: {
            	margin: 0
        	},
            items: []
        },        
		dump : null,

		sample : null,
		markups : [],
		parserData : null,
		parserReady : false,
		parserType : 'text',
		activeWindow : 0,
		activeMarkupScreen : 0,
		colorMap:['black','#276888','#BD6161','#F3955D','#96BB35','#50C6E8'],
		variableStore : {
			mtype : 'store',
			fields : ['variable','type','column','xpath','color','containerType',],
			proxy : {
				type : 'memory'
			}
		},
		variableColumn : null,
		when_parser_ready : {
			on : ['parserReadyChanged'],
			action : function(){
				if(this.parserReady){
					this.populateData(this.parserData);
				}
			}
		},
		codeScreen : {	
			mtype : 'ParserCode',
			parserType : 'text',
			viewOnly : true
		},
		testControl : {
			mtype : 'TestControl'
		},
		
		init: function() {
			this.set('variableColumn', this.getColumnConfigForType());
			this.variableStore.on('datachanged', function(){
				this.fireEvent('variableStoreChanged');
			},this);
			this.variableStore.on('update', function(){
				this.fireEvent('variableStoreChanged');
			},this);			
		},		
		getColumnConfigForType : function(){
			var me = this;
			if(this.parserType == 'text'){
				return [{
							header: 'Name',
							dataIndex: 'variable',						
							flex: 3
						},{
							header : 'Pattern',
							dataIndex : 'type',
							align : 'center',
							width : 200,
							renderer : function(value){
								return me.localize(value);
							}									
						},{
							header: 'Color',
							dataIndex: 'color',
							align : 'center',
							renderer: function(value, meta) {
								meta.style = 'background-color:' + value;
							},
							width : 200						
						},{
							header : 'Container',
							dataIndex : 'containerType',
							align : 'center',
							width : 200						
						}]
			}
			else if (this.parserType == 'table'){
				return [{
							header: 'Name',
							dataIndex: 'variable',					
							flex: 3
						},{
							header : 'Column Number',
							dataIndex : 'column',
							align : 'center',				
							width : 200
						},{
							header: 'Color',
							dataIndex: 'color',
							align : 'center',
							renderer: function(value, meta) {
								meta.style = 'background-color:' + value;
							},
							width : 200						
						},{
							header : 'Container',
							dataIndex : 'containerType',
							align : 'center',
							width : 200						
						}]
			}
			else {
				return [{
							header: 'Name',
							dataIndex: 'variable',
							editor: 'textfield',
							flex: 3
						},{
							header : 'XPath',
							dataIndex : 'xpath',
							editor : 'textfield',
							flex: 5
						},{
							header : 'Container',
							dataIndex : 'containerType',
							align : 'center',
							width : 200						
						}]
			}
		},
		resetParser : function(){
			this.set('markups',[]);
			this.set('sample','');
			this.variableStore.removeAll();
			this.set('activeMarkupScreen', 0);
			this.set('hasNoContent', false);
			this.set('activeWindow', 0);
		},
		loadData : function(parserData){		
			this.set('parserData', parserData);
			if(this.parserReady){
				this.populateData(parserData);
			}
		},
		populateData : function(parserData){
			if(parserData != null && Object.keys(parserData).length !== 0){			
				
				//Set up sample output
				this.updateSample(parserData.sampleOutput || "");

				if(parserData.parserConfig)
				{				
					try {
						var parserConfig = JSON.parse(parserData.parserConfig);
					}catch (e) {
						clientVM.displayError(this.localize('invalidJSON'));
					}
					
					if(parserConfig){
						//Set up markups
						if(this.parserType != 'xml')						
							this.set('markups', parserConfig['markups']);

						//Variable Table
						this.populateVariableTable(parserConfig);						
					}
					this.set('activeWindow', 1);				
				}
				else if(parserData['uscript']){
					//Show Code if this only has code
					this.set('activeMarkupScreen', 1);
					this.set('activeWindow', 1);
				}
				else {
					this.set('hasNoContent', true);
					this.set('activeWindow', 0);
				}

				//Populate Code
				this.codeScreen.populateCode(parserData['parserAutoGenEnabled'], parserData['uscript']);

				//Render Markup/XML
				if(this.parserType == 'text' || this.parserType == 'table')
					this.fireEvent('renderMarkup', this.sample, this.markups);
				else 
					this.fireEvent('renderXML', this.currentSampleDom);
			}
		},
		populateVariableTable : function(parserConfig){		
			if(this.parserType == 'text')
			{
				for(var i = 0; i < parserConfig['markups'].length; i++){
					var currentMarkup = parserConfig['markups'][i];
					if(currentMarkup.flow)
						var containerType = 'FLOW';
					else if (currentMarkup.wsdata)
						containerType = 'WSDATA';
					else
						containerType = 'OUTPUT';
					if(currentMarkup.action == "capture" ){
						this.variableStore.add({								
							variable : currentMarkup.variable,
							containerType : containerType,
							type : currentMarkup.type,
							color : currentMarkup.color,						
						})
					}
				}
			}
			else if(this.parserType == 'table'){
				for(var i = 0; i < parserConfig['tblVariables'].length; i++){
					var currentVariable = parserConfig['tblVariables'][i];
					if(currentVariable.flow)
						var containerType = 'FLOW';
					else if (currentVariable.wsdata)
						containerType = 'WSDATA';
					else
						containerType = 'OUTPUT';
					this.variableStore.add({						
						variable : currentVariable.variable,
						column : currentVariable.columnNum,
						color : this.colorMap[(currentVariable.columnNum - 1) % 6],
						containerType : containerType			
					})
				}
			}
			else {
				for(var i = 0; i < parserConfig['variables'].length; i++){
					var currentVariable = parserConfig['variables'][i];
					if(currentVariable.flow)
						var containerType = 'FLOW';
					else if (currentVariable.wsdata)
						containerType = 'WSDATA';
					else
						containerType = 'OUTPUT';
					this.variableStore.add({					
						variable : currentVariable.variable,		
						xpath : currentVariable.xpath,
						containerType : containerType			
					})	
				}
			}		
		},
		updateSample : function(data){
			this.set('sample', data);
			this.testControl.updateDefaultTestSample(this.parserType, data);
			if(this.parserType == 'xml')
				this.contructXMLDom();
		},
		getCode : function(){
			return this.codeScreen.getCode();
		},
		//XML Parser
		validXml : null,
		currentSampleDom : null,
		contructXMLDom: function(forClear) {		
			var clean = this.sample.replace(/\r*\n/g, '\n');
			//Clean up all declaration if any.
			clean = clean.replace(/<\?xml .*\?>/g,'');
			var doc = null;
			var err = false;
			var domParser = new DOMParser();		
			doc = domParser.parseFromString('<resolveparserroot>' + clean + '</resolveparserroot>', 'text/xml');
			this.set('validXml', doc.getElementsByTagName('parsererror').length == 0);
			this.set('currentSampleDom', doc);		

			if (!this.validXml) {
				if (this.parentVM.activeItem == this)
					clientVM.displayError(this.localize('invalidXmlSyntaxError'))
				doc = domParser.parseFromString('<error>' + Ext.htmlEncode(clean) + '</error>', 'text/xml');			
				this.set('currentSampleDom', doc)			
				return;
			}

			//Only handle 1 XML structure at this time.
			var xmlCount = 0;
			Ext.each(doc.childNodes[0].childNodes, function(node) {
				if (node.nodeType == 1) {
					xmlCount++;
					if (xmlCount > 1)
						node.ignore = true;
				}
			});
			this.set('validXml', xmlCount > 0);
			if (!this.validXml) {
				if (!forClear && this.parentVM.activeItem == this)//TODO
					clientVM.displayError(this.localize('noXmlSegFoundError'));
				doc = domParser.parseFromString('<error>' + clean + '</error>', 'text/xml');			
				this.set('currentSampleDom', doc)		
				return;
			}	
		},

		markupAndCapture: function(){
			this.set('activeWindow', 1);		
		},
		markupAndCaptureIsVisible$: function(){
			return this.activeWindow == 2;
		},
		test : function(){
			this.testControl.updateActiveParserType(this.parserType);
			this.set('activeWindow', 2);
		},		
		testIsVisible$ : function(){
			return this.activeWindow == 1;
		},
		showCode : function(){
			this.set('activeMarkupScreen', 1);
		},
		showMarkup : function(){
			this.set('activeMarkupScreen', 0);
		},
		showCodeIsVisible$: function(){		
			return this.activeMarkupScreen == 0;
		},
		showMarkupIsVisible$: function(){
			return this.activeMarkupScreen == 1;
		},
		variableStoreIsEmpty$ : function(){		
			return this.variableStore.getCount() == 0;
		},
		parserTypeName$ : function(){
			return this.localize(this.parserType);
		}
	},
	getDataPaths: function () {
		return ['data.resolveActionInvoc.parser'];
	},
	load: function (parserData) {
		var g = this.gluModel;
		g.resetParser();
		g.set('parserType', parserData ? parserData.parserType || 'text' : 'text');
		g.fireEvent('reconfigureVariableColumn', g.getColumnConfigForType());	
		g.loadData(parserData);		
	},
})