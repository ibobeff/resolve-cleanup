glu.defModel('RS.actiontaskbuilder.OptionEditDetail', {
	uname: null,
	uvalue: '',
	udescription: '',
	optionsStore: null,
	defaultOptionsMap: {},	

	init: function () {
		this.set('optionsStore', new RS.common.data.FlatStore({
			fields: ['uname', 'udescription']
		}).accessors([
			function (o) { return o.uname; },
			function (o) { return o.udescription; }
		]));
	},

	addOption: function (optionName) {
		this.set('uname', null);
		this.set('udescription', '');
		this.optionsStore.add({
			uname: optionName,
			udescription: this.defaultOptionsMap[optionName]
		});
	},

	removeOption: function (optionName) {
		this.set('uname', null);
		this.set('udescription', '');
		this.optionsStore.removeBy('uname', optionName);
	},

	addIsDisabled$: function () {		
		return !this.uname || this.uname.length === 0;
	},

	addIsEnabled$: function () {
		return this.uname && this.uname.length > 1;
	},

	setOptions: function (options) {
		for (var i = 0; i < options.length; i++) {
			this.defaultOptionsMap[options[i].uname] = options[i].udescription;
		}

		this.optionsStore.loadData(options)
	},

	add: function () {
		this.optionsStore.removeBy('uname', this.uname);
		this.parentVM.add(this);
		this.set('uname', '');
		this.set('uvalue', '');
		this.set('udescription', '');
	},

	addNewOptionBlankValues: function (paramName) {
		this.set('uname', 'BLANKVALUES');
		this.set('uvalue', '$INPUT{'+paramName+'}');
		this.set('udescription', 'Replace matching values with *****. Use $CONFIGname for Registration properties and $TARGETname for Proxy Target properties. Separate multiple values with a comma');
		this.parentVM.add(this);
		this.set('uname', '');
		this.set('uvalue', '');
		this.set('udescription', '');
	},

	addRemainingOption: function (option) {
		this.optionsStore.add(option);
	},

	focusOption: function (e) {
		if (e && e.getKey && typeof e.getKey() !== 'undefined' && e.currentTarget) {
			var id = e.currentTarget.id;

			if (id && id.length > 0) {
				var componentID = id.substring(0, id.indexOf('-inputEl'));
				var combo = Ext.ComponentQuery.query('*[id^='+ componentID +']');

				if (combo && combo.length > 0) {
					combo[0].expand();
				}
			}
		}
	},

	selectOption: function (records) {
		if (records[0]) {
			this.set('udescription', records[0].data.udescription);	
		}		
	}
});