Ext.define('RS.actiontaskbuilder.TagsEditBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Tags',
		itemsExhausted: false,
		hidden: true,
		collapsed: true,
		editBlock: true,
		title: '',
		header: {
			titlePosition: 0,
			items: []
		},
		name: null,
		description: '',
		noTagsMessage: '',
		allowedTags: null, // Possible allowed tags that can be added to the action task.
		store: null, // Current tags assigned to the action task
		allTags: [],
		gridIsEmpty: false,

		noDescription: '',
		tagDescriptions: {},

		postInit: function (blockModel) {
			this.blockModel = blockModel;
			this.noDescription = this.localize('noTagDescription');
			this.set('noTagsMessage', this.localize('tagsEmptyText'));

			this.set('allowedTags', new RS.common.data.FlatStore().accessors([
				function (o) { return o.name; },
				function (o) { return o.name; }
			]));

			this.set('store', new RS.common.data.FlatStore({
				fields: ['name', 'description']
			}));

			this.allTags = this.rootVM.allTags;
		},

		tagItemChange: function(combo, records) {
			var tagName = combo.value;
			var selection = this.grid.getSelectionModel().getSelection()[0];
			selection.set('name', tagName);
			selection.set('description', this.tagDescriptions[tagName]);
			this.generateAllowedTags();
			this.notifyDirty();
		},

		recordRemoved: function (record) {
			record.store.remove(record);
			this.set('itemsExhausted', false);
			this.generateAllowedTags();
			this.set('gridIsEmpty', this.store.count() === 0);
			this.notifyDirty();
		},
		validateDescription : function(newVal){
			if(!this.tagDescriptions.hasOwnProperty(newVal)){
				this.set('description','');
				this.set('recordAddedIsEnabled', false);
			}
			else{
				this.set('description',this.tagDescriptions[newVal]);
				this.set('recordAddedIsEnabled', true);
			}
		},
		tagSelected: function (records) {
			var rec = Array.isArray(records)? records[0]: records;
			this.set('description', this.tagDescriptions[rec.get('text')] || '');
			this.set('recordAddedIsEnabled', true);
		},
		recordAdded: function () {
			this.store.add({
				name: this.name,
				description: this.description
			});
			this.set('name', null);
			this.set('description', '');
			this.set('recordAddedIsEnabled',false);
			this.form.isValid();
			this.generateAllowedTags();
			this.hideAddFormOnExhaust();
			this.set('gridIsEmpty', this.store.count() === 0);
			this.notifyDirty();
		},
		recordAddedIsEnabled: false,
		generateAllowedTags: function (context) {
			var remaining = this.allTags.slice(),
				range = this.store.getRange();

			for (var i = 0; i < range.length; i++) {
				var tag = range[i].data;

				for (var j = 0; j < remaining.length; j++) {
					if (tag.name === remaining[j].name) {
						remaining.splice(j, 1);
						break;
					}
				}
			}

			this.allowedTags.loadData(this.allowedTags.shapeData(remaining));
			this.set('name', '');
			this.set('description', '');

			if (this.form) {
				this.form.isValid();
			}
		},

		hideAddFormOnExhaust: function() {
			this.set('itemsExhausted', this.store.getCount() === this.allTags.length);
		},

		grid: null,
		gridRenderCompleted: function (grid) {
			this.grid = grid;
		},

		form: null,
		registerForm: function (form) {
			this.form = form;
			form.isValid();
		},

		updateAllTags: function() {
			this.set('allTags', this.rootVM.allTags);
		},

		isGetAllTagsInProgress: function() {
			return this.rootVM.getAllTagsInProgress;
		},

		processAllTags: function() {
			for (var i = 0; i < this.allTags.length; i++) {
				var tag = this.allTags[i];
				this.tagDescriptions[tag.name] = tag.description;
			}

			var shapedTags = [];

			for (var i = 0; i < this.tags.length; i++) {
				var t = {
					name: '',
					description: ''
				};

				t.name = this.tags[i];
				t.description = this.tagDescriptions[this.tags[i]] || this.noDescription;
				shapedTags.push(t);
			}

			this.store.loadData(shapedTags);
			this.set('gridIsEmpty', this.store.count() === 0);
			this.generateAllowedTags();
			this.hideAddFormOnExhaust();
		},

		notifyDirty: function() {
			this.blockModel.notifyDirty();
		},

		closeFlag: false,
		close: function () {
			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		}
	},

	getData: function () {
		var range = this.gluModel.store.getRange(),
			tags = [];

		for (var i = 0; i < range.length; i++) {
			tags.push(range[i].data.name);
		}

		return [tags];
	},

	getDataPaths: function () {
		return ['data.tags'];
	},

	load: function (tags) {
		this.getAllTags(tags || []);
	},

	getAllTags: function (tags) {
		var g = this.gluModel;

		g.tags = tags;
		g.updateAllTags();

		if (g.allTags.length) {
			g.processAllTags();
		}
		else if (g.isGetAllTagsInProgress()) {
			var id = setInterval(function() {
				if (!g.isGetAllTagsInProgress()) {
					clearTimeout(id); // exit
					g.updateAllTags();
					g.processAllTags();
				}
			}, 100);
		}
	}
});