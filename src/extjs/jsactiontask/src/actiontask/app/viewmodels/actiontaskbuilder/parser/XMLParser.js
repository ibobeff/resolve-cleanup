glu.defModel("RS.actiontaskbuilder.XMLParser",{
	mixins : ['VariableUtil'],
	sample : '',
	smallResolution : false,
	newVariableCounter : 1,
	currentSampleDom : null,
	augmentedXPath: {},
	validXml : null,
	xpathSelections: [],
	invalidXPath: false,
	parserReady : false,
	loadingData : false,
	parserData : null,
	textXPath: '',
	xPathIsEditable : false,
	variableStore : {
		mtype : 'store',
		fields : ['variable','xpath','containerType']
	},
	activeScreen : 0,
	codeScreen : {	
		mtype : 'ParserCode',
		parserType : 'xml'
	},
	xPathDetail : {
		mtype : 'XPathTokenEditor'
	},
	variableColumn : null,
	supportActiveXObject: ((Object.getOwnPropertyDescriptor && Object.getOwnPropertyDescriptor(window, "ActiveXObject")) || ("ActiveXObject" in window)),
	when_parser_ready : {
		on : ['parserReadyChanged'],
		action : function(){
			if(this.parserReady){
				this.populateData(this.parserData);
			}
		}
	},
	update_script : {
		on :['markupsChanged','variableStoreChanged'],
		action : function(){
			if(this.parserReady && !this.loadingData)
				this.codeScreen.generateCode();
		}
	},
	update_dirty_flag : {
		on : ['sampleChanged'],
		action : function(){
			this.notifyDirty();
		}
	},
	notify_data_changed : {
		on : ['variableStoreChanged'],
		action : function(){
			this.notifyDataChange();
		}
	},
	when_resolution_change : {
		on : ['smallResolutionChanged'],
		action : function(){			
			this.fireEvent('embeddedLayout',  this.smallResolution);
		}
	},
	
	init : function(){
		this.set('variableColumn', this.getXmlVariableColumnConfig());
		this.variableStore.on('datachanged', function(){
			this.fireEvent('variableStoreChanged');
		},this);
		this.variableStore.on('update', function(){
			this.fireEvent('variableStoreChanged');
		},this);
	},

	//HELPERS
	showCode : function(){
		this.set('activeScreen', 1);
	},
	showMarkup : function(){
		this.set('activeScreen', 0);
	},	
	isAssessTask: function(flag){
		this.codeScreen.isAssessTask(flag);
	},
	setDefaultParser: function(){
		if(this.parserReady){
			this.clearSample();		
			this.codeScreen.resetCode();
			this.set('activeScreen',0);
		}
	},
	loadData : function(parserData){
		this.set('parserData', parserData);
		if(this.parserReady){
			this.populateData(parserData);
		}
	},
	populateData : function(parserData){
		if(parserData != null && Object.keys(parserData).length !== 0){
			this.set('loadingData',true);		
			this.updateSample(parserData.sampleOutput || "");	
			this.variableStore.removeAll();

			//Populate Markups
			if(parserData.parserConfig){
				try {
				var parserConfig = JSON.parse(parserData.parserConfig);
				}catch (e) {
					clientVM.displayError(this.localize('invalidJSON'));
				}
				if(parserConfig){
					var variableData = [];
					for(var i = 0; i < parserConfig.variables.length; i++){
						var currentVariable = parserConfig.variables[i];
						if(currentVariable.flow)
							var containerType = 'FLOW';
						else if (currentVariable.wsdata)
							containerType = 'WSDATA';
						else
							containerType = 'OUTPUT';
						variableData.push({
							id : currentVariable.id,
							variable : currentVariable.variable,		
							xpath : currentVariable.xpath,
							containerType : containerType			
						})	
					}
					this.variableStore.loadData(variableData);
				}
			}
			else if(parserData['uscript']){
				//Show Code if this only has code
				this.set('activeScreen', 1);
			}

			//Populate Code
			this.codeScreen.populateCode(parserData['parserAutoGenEnabled'], parserData['uscript']);
		
			this.set('loadingData', false);
		}
	},
	updateSample : function(data, asPasted){		
		if((this.variableStore.data.length > 0 || this.textXPath) && asPasted){
			this.message({
				title : this.localize('overwriteMarkupTitle'),
				msg : this.localize('overwriteMarkupBody'),
				button : Ext.MessageBox.YESNO,
				buttonText : {
					yes : this.localize('confirm'),
					no : this.localize('cancel')
				},
				scope : this,
				fn : function(btn){
					if(btn == 'yes'){						
						this.clearSample();
						this.set('sample', data);
						this.parentVM.updateDefaultTestSample(data);	
						this.constructXMLDom(data == "" ? true : false);			
					}
				}
			})
		}
		else
		{
			this.set('sample', data);
			this.parentVM.updateDefaultTestSample(data);	
			this.constructXMLDom(data == "" ? true : false);		
		}
	},
	getParserConfig : function(){
		var variables = [];
		this.variableStore.each(function(r) {
			variables.push(Ext.apply(r.data, {				
				xpath: r.data.xpath,
				flow : r.data.containerType == "FLOW",
				wsdata : r.data.containerType == "WSDATA",
				output : r.data.containerType == "OUTPUT"
			}));
		}, this);
		if(variables.length != 0){
			var	parserConfig = JSON.stringify({ 
				type : 'xml',
				sample : this.sample,
				variables : variables,
				generatedFromATB : true
			});
			return parserConfig;
		}
		else
			return null;		
	},	
	getSample : function(){
		return this.sample;
	},
	getCode : function(){
		return this.codeScreen.getCode();
	},
	getParameterList : function(){
		var variableArray = [];
		this.variableStore.each(function(entry){
			variableArray.push({
				variableId : entry.get('id'),
				name : entry.get('variable'),
				containerType : entry.get('containerType')
			})
		})
		return variableArray;		
	},
	isAutoGenCode : function(){
		return this.codeScreen.isAutoGenCode();
	},
	isParserReady : function(){
		return this.parserReady;
	},
	resetParser : function(){
		this.clearPath();	
	},
	notifyDirty : function(){
		if(!this.loadingData)
			this.parentVM.notifyDirty();
	},
	notifyDataChange : function(){
		this.parentVM.notifyDataChange();
	},
	xmlHintText$ : function(){
		return this.sample == "" ? "" : this.localize("xmlHintText");
	},
	//VIEW EVENT HANDLERS
	clearSample : function(){
		this.updateSample("");	
		this.clearPath();
		this.variableStore.removeAll();
		this.newVariableCounter = 1;
	},	
	capturePath : function(captureInfo){
		var variableName = "NewVariable" + this.newVariableCounter;
		while(this.variableStore.findExact("variable",variableName) != -1){
			this.newVariableCounter++;
			variableName = "NewVariable" + this.newVariableCounter;
		} 
		var existingVariableName = this.variableStore.collect('variable');
		var newMarkup = {
			id : Ext.data.IdGenerator.get('uuid').generate(),
			variable : variableName,		
			xpath : this.xpath
		}
		this.open({
			mtype : 'MarkupCapture',
			newMarkup : newMarkup,
			existingVariableName : existingVariableName,
			parserType : 'xml',
			dumper : {
				dump : this.processCapturePath,
				scope : this
			}
		})
	
	},
	processCapturePath : function(newMarkup){
		//Add to variable grid
		this.variableStore.add(newMarkup);
		//this.resetParser();
	},
	removeVariable : function(record){
		if(!this.isViewOnly){
			var removedVariableId = record.raw.id;
			var variableStoreIndex = this.variableStore.indexOfId(removedVariableId);
			this.variableStore.removeAt(variableStoreIndex);	
		}
	},	
	clearPath : function(){
		this.set('augmentedXPath', null);
		this.set('xpath', null);
		this.set('textXPath', '');
		this.xPathDetail.set('noProperty',true);
	},

	//VIEW COMPONENT VISIBILITY	
	clearSampleIsEnabled$ : function(){
		return this.sample != '' && this.activeScreen == 0;
	},
	clearPathIsEnabled$ : function(){
		return this.textXPath != '';
	},
	capturePathIsEnabled$ : function(){
		return this.textXPath != '';
	},	
	variableStoreIsEmpty$ : function(){
		return this.variableStore.getCount() == 0;
	},
	showCodeIsVisible$: function(){		
		return this.activeScreen == 0;
	},
	showMarkupIsVisible$: function(){
		return this.activeScreen == 1;
	},
	controlBorderRegion$ : function(){
		return this.smallResolution ? 'south' : 'east';
	},
	controlHeight$ : function(){
		return this.smallResolution ? 900 : 450;
	},
	controlMargin$ : function(){
		return this.smallResolution ? '10 0 0 0' : '0 0 0 10';
	},

	//XPATH LOGIC
	constructXMLDom: function(forClear) {		
		var clean = this.sample.replace(/\r*\n/g, '\n');
		//Clean up all declaration if any.
		clean = clean.replace(/<\?xml .*\?>/g,'');
		var doc = null;
		var err = false;
		var domParser = new DOMParser();		
		doc = domParser.parseFromString('<resolveparserroot>' + clean + '</resolveparserroot>', 'text/xml');
		this.set('validXml', doc.getElementsByTagName('parsererror').length == 0);
		this.set('currentSampleDom', doc);		

		if (!this.validXml) {
			clientVM.displayError(this.localize('invalidXmlSyntaxError'))
			doc = domParser.parseFromString('<error>' + Ext.htmlEncode(clean) + '</error>', 'text/xml');			
			this.set('currentSampleDom', doc)
			this.fireEvent('renderSample');
			return;
		}

		//Only handle 1 XML structure at this time.
		var xmlCount = 0;
		Ext.each(doc.childNodes[0].childNodes, function(node) {
			if (node.nodeType == 1) {
				xmlCount++;
				if (xmlCount > 1)
					node.ignore = true;
			}
		});
		this.set('validXml', xmlCount > 0);
		if (!this.validXml) {
			if(!forClear)
				clientVM.displayError(this.localize('noXmlSegFoundError'));
			doc = domParser.parseFromString('<error>' + clean + '</error>', 'text/xml');			
			this.set('currentSampleDom', doc)
			this.fireEvent('renderSample', forClear);
			return;
		}
		this.fireEvent('renderSample');
	},	
	selectXPath: function(type, containerElementPnid, rendererNode) {		
		var element = Ext.fly(this.currentSampleDom).query('*[pnid="' + containerElementPnid + '"]')[0];
		var current = element;
		var nodes = [current];
		while (current.parentNode && current.parentNode.tagName.toLowerCase() != 'resolveparserroot') {
			current = current.parentNode;
			if (current.nodeType != 1)
				continue;
			nodes.push(current);
		}
		nodes.reverse();
		var tokens = [];
		var augmentedXPath = [];
		var predicates = [];
		Ext.each(nodes, function(node) {
			tokens.push(node.tagName)
			augmentedXPath.push({
				type: 'element',
				element: node.tagName,
				ptid: Ext.data.IdGenerator.get('uuid').generate(),
				pnid: node.pnid,
				op: 'and',
				predicates: []
			});
		});
		//Set the last node as leaf.
		augmentedXPath[augmentedXPath.length - 1]['isLeaf'] = true;
		var xpath = '//' + tokens.join('/');	
		if (type == 'text' || type == 'cdata') {
			augmentedXPath[augmentedXPath.length - 1].predicates = [{
				type: 'text',
				pnid: augmentedXPath[augmentedXPath.length - 1].pnid,
				ptid: Ext.data.IdGenerator.get('uuid').generate(),
				condition: 'contains',
				originValue: element.textContent,
				value: Ext.String.escape(element.textContent).replace(/\n/g, '\\n').replace(/"/g, '\\"')
			}];
			augmentedXPath.push({
				type: 'selector',
				ptid: Ext.data.IdGenerator.get('uuid').generate(),
				value: type + "()"
			});
		} else if (type == 'attribute') {
			var trimmer = /^\s*(.+)\s*=\s*"(.*)"\s*$/
			var groups = trimmer.exec(rendererNode.textContent);
			var name = groups[1];
			var value = groups[2];
			var isNumberTest = /^[0-9]+([.][0-9]+)?$/
			var isNumber = isNumberTest.test(value);
			augmentedXPath[augmentedXPath.length - 1].predicates = [{
				type: 'attribute',
				pnid: augmentedXPath[augmentedXPath.length - 1].pnid,
				ptid: Ext.data.IdGenerator.get('uuid').generate(),
				name: name,
				condition: '=',
				isNumber: isNumber,
				value: value
			}];
			augmentedXPath.push({
				type: 'selector',			
				ptid: Ext.data.IdGenerator.get('uuid').generate(),
				value: '@' + name
			});
		}

		this.set('augmentedXPath', augmentedXPath);
		this.set('textXPath', this.xpath);
		this.xPathDetail.set('noProperty', true);
	},
	xpath$: function() {
		var tokens = [];
		Ext.each(this.augmentedXPath, function(augmentedToken) {
			tokens.push(this.serializeXPathToken(augmentedToken));
		}, this);
		return '//' + tokens.join('\/');
	},	
	serializeXPathToken: function(augmentedToken) {
		if (augmentedToken.type == 'selector')
			return this.serializeSelector(augmentedToken);

		var predicates = [];
		Ext.each(augmentedToken.predicates, function(p) {
			if (p.type == 'text')
				predicates.push(this.serializeTextPredicate(p));
			else if (p.type == 'attribute')
				predicates.push(this.serializeAttributePredicate(p));
			else if (p.type == 'custom')
				predicates.push(this.serializeCustomPredicate(p));
		}, this);
		var token = augmentedToken.element;
		if (predicates.length > 0)
			token += '[' + predicates.join(' ' + augmentedToken.op + ' ') + ']'
		return token;
	},
	serializeSelector: function(token) {
		return token.value;
	},
	serializeTextPredicate: function(predicate) {
		if (['contains', 'starts-with', 'ends-with'].indexOf(predicate.condition) != -1)
			return Ext.String.format('{0}(., \'{1}\')', predicate.condition, predicate.value);;
		return '';
	},
	serializeAttributePredicate: function(predicate) {
		if (['contains', 'starts-with', 'ends-with'].indexOf(predicate.condition) != -1)
			return Ext.String.format('{0}(@{1}, \'{2}\')', predicate.condition,predicate.name, predicate.value);
		return Ext.String.format('@{0} {1} {2}', predicate.name, predicate.condition, predicate.isNumber ? predicate.value : '"' + predicate.value + '"');
	},	
	serializeCustomPredicate: function(predicate) {		
		return predicate.value;
	},	
	showDetail: function(pnid, ptid, wndPosAdjustFn) {		
		var targetToken = null;
		var selectorToken = null;
		var leafNode = null;
		Ext.each(this.augmentedXPath, function(token) {
			if (pnid == token.pnid) {
				targetToken = token;				
			}
			if(token.type == "selector")
				selectorToken = token;
			if(token.isLeaf)
				leafNode = token;
		}, this);		
		//Select Leaf node instead selector node.
		targetToken = targetToken.type == "selector" ? leafNode : targetToken;	
		var detailConfig = {		
			augmentedXPathToken:  targetToken,
			currentSampleDom: this.currentSampleDom,		
			isLeaf : targetToken.isLeaf
		}
		if(targetToken.isLeaf){
			detailConfig['selectorToken'] = selectorToken;
		}
		this.xPathDetail.updateXPath(detailConfig);		
	},
	removeToken : function(pnid, ptid){
		var newPath = [];
		var isLeaf = false;
		Ext.each(this.augmentedXPath, function(token) {
			if (token.ptid == ptid){
				isLeaf = token.isLeaf;
				return;
			}
			newPath.push(token);
			if (ptid == 'predicates') {
				token.predicates = [];
				return;
			}
			if (token.predicates)
				token.predicates = Ext.Array.filter(token.predicates, function(p) {
					return p.ptid != ptid;
				});
		}, this);
		if(isLeaf)
			newPath.splice(newPath.length - 1,1);
		var decodedPath = '';

		try {
			decodedPath = Ext.decode(Ext.encode(newPath));
		} catch (ex) {
			decodedPath = 'Decode Failure (Figure out this bug and fix it)';
		}

		this.set('augmentedXPath', decodedPath);
		this.set('textXPath', this.xpath);
	}
});