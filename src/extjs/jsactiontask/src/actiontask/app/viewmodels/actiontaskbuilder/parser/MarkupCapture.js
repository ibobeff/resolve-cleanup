glu.defModel('RS.actiontaskbuilder.MarkupCapture',{
	fields : ['variable','color','column','xpath'],	
	existingVariableName : [],
	validNameRegex: RS.common.validation.VariableName,
	newMarkup : null,
	parserType : 'text',
	containerType : 'OUTPUT',
	containerTypeStore : {
		mtype : 'store',
		proxy : {
			type : 'memory'
		},
		fields : [{
			name : 'value',
			convert : function(v, record){
				return record.raw;
			}
		}],
		data: ['WSDATA','FLOW','OUTPUT']
	},

	//For Text Parser
	patternProcessor : {
		mtype : 'PatternProcessor',
		ignorePattern : ['literal']
	},

	init : function(){
		this.loadData(this.newMarkup);
		if(this.parserType == "text"){
			//Let the init of patternProcessor run first before doing update.
			setTimeout(function(){
				this.patternProcessor.updatePatternForMarkup(this.newMarkup['type'],this.newMarkup['autoType']);
			}.bind(this),0);
		}		
	},
	parserTypeIsText$ : function(){
		return this.parserType == 'text';
	},
	parserTypeIsTable$ : function(){
		return this.parserType == 'table';
	},
	parserTypeIsXML$ : function(){
		return this.parserType == 'xml';
	},
	isNameDuplicated : function(name){
		return this.existingVariableName.indexOf(name) != -1;
	},
	variableIsValid$ : function(){
		if(this.isNameDuplicated(this.variable))
			return this.localize('duplicateVariableMsg');
		else if (!this.validNameRegex.test(this.variable))
			return this.localize('validKeyCharacters');
		else
			return true;
	},
	add : function(){
		if(!this.isValid){
			this.set('isPristine',false);
			return;
		}
		var newData = {
			variable : this.variable,			
			containerType : this.containerType
		}
		if(this.parserType == "text"){
			var patternName = this.patternProcessor['activePattern']['name'];
			Ext.apply(newData, {				
				color : this.color,
				type : patternName,
			})
		}
		else if(this.parserType == "xml"){
			Ext.apply(newData, {				
				xpath : this.xpath,
			})
		}

		var data = Ext.apply(this.newMarkup, newData);
		if(this.dumper && Ext.isFunction(this.dumper.dump))		
			this.dumper.dump.apply(this.dumper.scope || this, [data]);
		this.doClose();		
	},
	cancel : function(){
		this.doClose();
	},
})