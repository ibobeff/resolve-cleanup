glu.defModel('RS.actiontaskbuilder.BoundaryUtil',{
	startOutput$ : function(){
		return !(this.parseStart && this.parseStart != '');
	},
	endOutput$ : function(){
		return !(this.parseEnd && this.parseEnd != '');
	},
	startOutputIsSelected : function(){
		this.set('parseStart','');
	},		
	endOutputIsSelected : function(){
		this.set('parseEnd','');
	},
	parseStartIsValid$: function(){
		var self = this;
		//Check for new line
		var indx = self.sample.indexOf(this.parseStart);
		if(indx == -1 && self.sample.replace(/[\n\r\s]/g,'').indexOf(this.parseStart.replace(/[\n\r\s]/g,'')) != -1)
			return this.localize('invalidBoundaryInput');
		else
			return true;
	},
	parseEndIsValid$: function(){
		var self = this;
		//Check for new line
		var indx = self.sample.indexOf(this.parseEnd);
		if(indx == -1 && self.sample.replace(/[\n\r\s]/g,'').indexOf(this.parseEnd.replace(/[\n\r\s]/g,'')) != -1)
			return this.localize('invalidBoundaryInput');
		else
			return true;
	},
	updateBoundaryIsEnabled$: function(){
		return this.sample != '' && (this.parseEndIsValid === true) && (this.parseStartIsValid === true);
	},
	updateBoundary : function(){		
		//Gather boundary
		var startPosition = 0;
		var endPosition = this.sample.length;		
		if(this.parseStart != '' ){	
			var indexStartBoundary = this.sample.indexOf(this.parseStart);		
			startPosition = indexStartBoundary != -1 ? indexStartBoundary : 0;
			startPosition += this.startTextIncluded ? 0 : this.parseStart.length;
		}
		if(this.parseEnd != '' ){
			var indexEndBoundary = this.sample.indexOf(this.parseEnd);
			endPosition = indexEndBoundary != -1 ? indexEndBoundary : 0;
			endPosition += this.endTextIncluded ? this.parseEnd.length : 0;
		}
		//Validate this boundary.
		if(endPosition < startPosition){
			this.set('parseEnd','');
			endPosition = this.sample.length - 1;
		}
		//Compare boundary with current markups
		var outOfBoundMarkupActionId = [];
		for(var i = 0; i < this.markups.length; i++){
			var markup = this.markups[i];
			if(markup.type == 'parseStart' || markup.type == 'parseEnd')
				continue;
			if(markup.from < startPosition || markup.from > endPosition)
				outOfBoundMarkupActionId.push(markup.actionId);
			else {
				if((markup.from + markup.length) > endPosition)
					outOfBoundMarkupActionId.push(markup.actionId);
			}
		}
		if(outOfBoundMarkupActionId.length > 0){
			this.message({
				title : this.localize('markupOutOfBoundaryTitle'),
				msg : this.localize('markupOutOfBoundaryBody'),
				buttons : Ext.MessageBox.YESNO,
				buttonText : {
					yes : this.localize('yes'),
					no : this.localize('no')
				},
				scope : this,
				fn: function(btn){
					if(btn == "yes")
						this.addBoundaries(outOfBoundMarkupActionId);
					else {
						this.set('parseStart', this.parseStartOrignalData);
						this.set('parseEnd', this.parseEndOrignalData);
					}
				}
			})
		}
		else{
			this.addBoundaries(outOfBoundMarkupActionId);
		}	
	},
	addBoundaries : function(removedMarkups){
		var newMarkups = [];
		//Remove out of bound markups as well as old boundaries.
		for(var i = 0; i < this.markups.length; i++){
			var markup = this.markups[i];
			if(removedMarkups.indexOf(markup.actionId) == -1 && markup.type != 'parseStart' && markup.type != 'parseEnd')
				newMarkups.push(markup);
		}
		//Clean up any variables out of bound
		for(var i = 0; i < removedMarkups.length; i++){
			var actionId = removedMarkups[i];
			var indexOfVariable = this.variableStore.indexOfId(actionId);
			if(indexOfVariable != -1)
				this.variableStore.removeAt(indexOfVariable);
		}
		//Add in boundaries
		if(this.parseStart != ''){
			var indexStartBoundary = this.sample.indexOf(this.parseStart);		
			newMarkups.push({
				from :  indexStartBoundary != -1 ? indexStartBoundary : 0,
				length : this.parseStart.length,
				boundaryInput : this.parseStart,
				type : 'parseStart',
				action : 'beginParse',
				included : this.startTextIncluded,
				actionId: Ext.data.IdGenerator.get('uuid').generate()
			})
		}
		if(this.parseEnd != ''){
			var indexEndBoundary = this.sample.indexOf(this.parseEnd);
			newMarkups.push({
				from : indexEndBoundary != -1 ? indexEndBoundary : this.sample.length ,
				length : this.parseEnd.length,
				boundaryInput : this.parseEnd,
				type : 'parseEnd',
				action : 'endParse',
				included : this.endTextIncluded,
				actionId: Ext.data.IdGenerator.get('uuid').generate()
			})
		}
		this.set('markups', newMarkups);
		this.set('parseStartOrignalData', this.parseStart);
		this.set('parseEndOrignalData', this.parseEnd);
		this.resetParser();
	},
	updateBoundaryFromMarkup : function(postion, markupId){
		if(postion == 'start'){
			var targetMarkup = null;
			for(var i = 0; i < this.markups.length; i++){
				var markup = this.markups[i];
				if(markupId == markup.actionId){
					targetMarkup = markup;
					break;
				}
			}
			if(targetMarkup.type == 'literal'){
				var txt = this.sample.substring(targetMarkup.from, targetMarkup.from + targetMarkup.length);
				this.set('parseStart',txt);
			}
			else
				this.set('parseStart',this.getRegexForType(targetMarkup.type));
		}
		else if (postion == 'end'){
			var targetMarkup = null;
			for(var i = 0; i < this.markups.length; i++){
				var markup = this.markups[i];
				if(markupId == markup.actionId){
					targetMarkup = markup;
					break;
				}
			}
			if(targetMarkup.type == 'literal'){
				var txt = this.sample.substring(targetMarkup.from, targetMarkup.from + targetMarkup.length);
				this.set('parseEnd',txt);
			}
			else
				this.set('parseEnd',this.getRegexForType(targetMarkup.type));
		}
	}
})