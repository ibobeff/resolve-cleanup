glu.defModel('RS.actiontaskbuilder.PatternTag',{
	name : '',
	selected : false,
	hidden : true,
	displayName$ : function(){
		return this.localize(this.name);
	},	
	select : function(){
		this.parentVM.select(this);
	},	
	selectedCss$ : function(){
		return this.selected ? 'selected' : '';
	}
})
