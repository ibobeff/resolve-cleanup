Ext.define('RS.actiontaskbuilder.SummaryEditBlockModel',  {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers','SummaryAndDetailUtil'],
		blockModel: null,
		blockModelClass: 'SummaryAndDetails',
		hidden: true,
		collapsed: true,
		editBlock: true,
		stopPropagation: false,
		closeFlag: false,
		title: '',
		header: {
			xtype: 'toolbar',
			margin: 0,
			padding: 0,
			items: []
		},
		fields : ['fieldType','fieldName','condition','severity','display'],
		
		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden) {
					setTimeout(function() {
						if (this.parentVM.summaryAndDetailsEditSectionExpanded) {
							this.parentVM.summaryAndDetailsEditSectionExpanded = null;
							this.set('collapsed', false);
						}
						if (this.parentVM.summaryAndDetailsEditSectionMaximized) {
							this.parentVM.summaryAndDetailsEditSectionMaximized = null;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['SUMMARYDETAILS']);
						}
					}.bind(this), 10);
				}
			}
		},

		postInit: function (blockModel) {
			this.blockModel = blockModel;
				
			this.header.items = [/*{
				xtype: 'component',
				padding: '0 0 0 15',
				cls: 'x-header-text x-panel-header-text-container-default',
				html: this.localize('summaryAndDetailsTitle')
			},*/
			{
	        	xtype: 'container',
	        	autoEl: 'ol',
	        	cls: 'breadcrumb flat',
	        	margin: 0,
	        	padding: 0,
	        	items: [{
		    		xtype: 'component',
		    		autoEl: 'li',
		    		html: this.localize('summaryEditTitle'),
		    		cls: 'active'
		    	},{
		    		xtype: 'component',
		    		autoEl: 'li',
					html: this.localize('detailsEditTitle'),
	    			listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.jumpTo('DetailsEdit', this.parentVM.detailsEdit.gluModel);
				            }, this);
						}.bind(this)
		    		}
		    	}]
	        }, '->', {
				xtype: 'button',
				tooltip: this.localize('resizeFullscreen'),
				iconCls: 'rs-block-button icon-resize-full',
				isResizeMaxBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['SUMMARYDETAILS']);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				tooltip: this.localize('resizeNormalscreen'),
				iconCls: 'rs-block-button icon-resize-small',
				hidden: true,
				isResizeNormalBtn: true,
				margin: '0 20',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeNormalSection(this);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				iconCls: 'rs-block-button icon-chevron-sign-up',
				isCollapseBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.set('collapsed', true);
						}, this);
					}.bind(this)
				}
			}];
	
			this.ruleStore.on('datachanged', function(){
				this.fireEvent('ruleStoreChanged');
			}.bind(this));
			this.ruleStore.on('update', function(){
				this.fireEvent('ruleStoreChanged');
			}.bind(this));
			this.loadData(this.defaultData);
			this.set('isPristine', true);
		},
		
		jumpTo: function(nextBlockName, nextSection){
			this.parentVM.summaryAndDetailsEditSectionExpanded = !this.collapsed;
			this.parentVM.summaryAndDetailsEditSectionMaximized = this.isSectionMaxSize;
			this.parentVM.collapseAndHideSection(this);
			this.parentVM.showAndExpandThisSection(nextSection);
		},		
		close: function () {
			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		},
		notifyDirty: function(format) {
			this.blockModel.notifyDirty();
		}			
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.assess'];
	},

	getData: function () {
		//Temporary code for now until API is ready
		var ruleStore = this.gluModel.ruleStore;
		var summaryRules = [];
		ruleStore.each(function(rule){
			summaryRules.push({
				severity : rule.get('severity'),
				condition : rule.get('condition'),
				display : rule.get('display')
			})
		})
		return [{summaryRule : JSON.stringify(summaryRules)}];
	},

	load: function (data) {
		var data = JSON.parse(data.summaryRule);
		var summaryRules = Array.isArray(data) ? data : [];
		this.gluModel.ruleStore.loadData(summaryRules);
	},		
	processDataChange : function(data){
		var g = this.gluModel;		
		for(var type in data){
			var processedData = [];
			for(var i = 0; i < data[type].length; i++){
				processedData.push({
					name : data[type][i]
				})
			}
			g.allParameters[type] = processedData;			
		}
		//Repopulate already selected type for each field.
		g.loadParameterStore(g.parametersStore, g.fieldType);
	}	
});
