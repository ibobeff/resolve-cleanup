glu.defModel('RS.actiontaskbuilder.Execute', {
	API : {
		submit : '/resolve/service/execute/submit'
	},
	sirProblemId: '',
	sirContext: false,
	target: null,
	newWorksheet: true,
	activeWorksheet: false,
	debug: false,
	mock: false,
	mockName: '',

	fromWiki: false,
	mockEditable$: function() {
		return this.fromWiki
	},

	mockStore: {
		mtype: 'store',
		fields: ['id', 'uname', 'uparams', 'uinputs', 'uflows'],
		proxy: {
			type: 'memory'
		}
	},

	paramStore: {
		mtype: 'store',
		groupField: 'utype',
		fields: ['name', 'value', 'origValue', 'order', 'utype', 'protected', 'encrypt', 'modified'],
		proxy: {
			type: 'memory'
		}
	},

	origParamStore: {
		mtype: 'store',
		fields: ['name', 'value', 'origValue', 'order', 'utype', 'protected', 'encrypt', 'modified'],
		proxy: {
			type: 'memory'
		}
	},

	paramColumns: [{
		header: '~~name~~',
		dataIndex: 'name',
		filterable: false,
		flex: 1,
		editor: {
			xtype: 'textfield',
			allowBlank: false,
			regex: RS.common.validation.VariableName,
			listeners: {
				validitychange: function(grid, isValid){
					if (!isValid) {
						this.up('grid')._vm.set('executeIsEnabled', false)
					} else {
						this.up('grid')._vm.set('executeIsEnabled', true)
					}
			    },
				blur: function(){
					this.up('grid')._vm.set('executeIsEnabled', true)
				}
			}
		},
		renderer: RS.common.grid.plainRenderer()
	}, {
		header: '~~value~~',
		dataIndex: 'value',
		filterable: false,
		flex: 1,
		editor: {},
		renderer: RS.common.grid.plainRenderer()
	}],

	//when another module use this view, it can use this to init the vm
	inputsLoader: function() {
		var parametersBlockData = this.parentVM.parametersInputEdit.getData(),
			mockBlockData = this.parentVM.mockEdit.getData();

		if (parametersBlockData.length) {
			var params = parametersBlockData[0];
			for (var i=0; i< params.length; i++) {
				if (params[i].utype === 'INPUT' || params[i].utype === null) {
					var param = {
						name: params[i].uname,
						value: params[i].udefaultValue,
						order: params[i].uorder,
						protected: params[i].uprotected,
						encrypt: params[i].uencrypt,
						utype: 'INPUT'
					};
					this.paramStore.add(param);
					this.origParamStore.add(param);
				}
			}
			this.origParamStore.sort('order');
		}

		if (mockBlockData.length) {
			var mockData = mockBlockData[0];
			if (mockData) {
				this.mockStore.add(mockData);
			}
		}
	},
	embedded: false,
	init: function() {
		this.set('sirContext', false);
		this.set('debug', clientVM.executionDebugDefaultMode || false);
		var urlParams = clientVM.rootVM.screens.getAt(clientVM.rootVM.screens.getActiveIndex()).params;
		if (((urlParams.SIR || urlParams.sir) && (urlParams.PROBLEMID || urlParams.problemId)) || this.sirProblemId !== '') {
			this.set('sirContext', true);
		}
		if (this.executeDTO && this.embedded)
			this.embeddedExecution();
		else {
			this.inputsLoader(this.paramStore, this.origParamStore, this.mockStore);
		}
	},

	editParamsIsEnabled$: function() {
		return this.paramsSelections.length == 1 && !this.paramsSelections[0].get('protected');
	},

	embeddedExecution: function() {
		this.doClose();
		this.ajax({
			url: '/resolve/service/actiontask/get',
			params: {
				id: this.executeDTO.id,
				name: this.executeDTO.name
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('LoadActionTaskErr'), respData.message);
					return;
				}
				var task = respData.data;
				var params = task.resolveActionInvoc.resolveActionParameters;
				Ext.Object.each(this.executeDTO.inputParams, function(k, v) {
					Ext.each(params, function(param) {
						if (param.utype != 'INPUT')
							return;
						if (param.uname != k)
							return;
						param.udefaultValue = v;
					});
				});
				var mockData = task.resolveActionInvoc.resolveActionTaskMockData;
				if (this.executionWin)
					this.executionWin.doClose();
				this.executionWin = this.open({
					mtype: 'RS.actiontaskbuilder.Execute',
					// target: button.getEl().id,
					executeDTO: {
						actiontask: task.ufullName,
						actiontaskSysId: task.id,
						// problemId: this.newWorksheet ? 'NEW' : null,
						action: 'TASK'
					},
					inputsLoader: function(paramStore, origParamStore, mockStore) {
						for (var i=0; i< params.length; i++) {
							if (params[i].utype === 'INPUT' || params[i].utype === null) {
								var param = {
									name: params[i].uname,
									value: params[i].udefaultValue,
									order: params[i].uorder,
									protected: params[i].uprotected,
									encrypt: params[i].uencrypt,
									utype: 'INPUT'
								};
								paramStore.add(param);
								origParamStore.add(param);
							}
						}
						origParamStore.sort('order');
						if (mockData) {
							mockStore.add(mockData);
						}
						this.set('embedded', true);
					}
				});
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	paramsSelections: [],
	addParam: function() {
		var max = 0;
		var reg = /^NewParam(\d*)$/;
		var newParam = 'NewParam';

		this.paramStore.each(function(param) {
			if (!reg.test(param.get('name')))
				return;
			var m = reg.exec(param.get('name'));
			var idx = parseInt(m[1]);
			if (idx > max)
				max = idx;
		}, this);
		newParam += ((max + 1) || '');

		var param = {
			name: newParam,
			value: '',
			order: 0,
			utype: 'PARAM'
		}
		this.paramStore.add(param);
		this.origParamStore.add(param);

		// clear out all previously selected items
		this.paramStore.grid.getSelectionModel().deselectAll();

		// determine row of newly added "NewParam"
		var row;
		for (row = this.paramStore.data.items.length-1; row > 0; row--) {
			if (this.paramStore.data.items[row].data.name == newParam) {
				break;
			}
		}
		// start editing the newly selected item
		this.paramStore.grid.getPlugin().startEditByPosition({
			row: row,
			column: 0
		});
	},

	removeParams: function() {
		this.paramStore.remove(this.paramsSelections);
		this.origParamStore.remove(this.paramsSelections);
	},

	mockNameIsEnabled$: function() {
		return this.mock
	},

	mockNameIsValid$: function() {
		return (!this.mock || this.mockName) ? true : this.localize('invalidMockName');
	},

	when_mockName_changed: {
		on: ['mockNameChanged'],
		action: function() {
			this.updateParamList(true);
		}
	},

	when_mock_changed: {
		on: ['mockChanged'],
		action: function() {
			if (this.mock) {
				this.updateParamList(true);
			} else {
				this.updateParamList(false);
			}
		}
	},

	updateParamList: function(isMock) {
		this.paramStore.removeAll();

		for (var i=0; i< this.origParamStore.getCount(); i++) {
			var param = this.origParamStore.getAt(i);
			this.paramStore.add({
				name: param.get('name'),
				value: param.get('value'),
				utype: param.get('utype'),
				order: param.get('order')
			});
		}

		if (isMock) {
			for (var i=0; i< this.mockStore.getCount(); i++) {
				var mock = this.mockStore.getAt(i);
				if (mock.get('uname') == this.mockName) {
					var uparams = this.jsonStrToArray(mock.get('uparams'));
					for (var i=0; i< uparams.length; i++) {
						this.paramStore.add({
							name: uparams[i].name,
							value: uparams[i].value,
							utype: 'PARAM',
							order: 0
						});
					}
					var uinputs = this.jsonStrToArray(mock.get('uinputs'));
					for (var i=0; i< uinputs.length; i++) {
						var name = uinputs[i].name;
						var paramIdx = this.paramStore.findExact('name', name);
						if (paramIdx != -1) {
							var currentParam = this.paramStore.getAt(paramIdx);
							if (currentParam.get('utype') == 'INPUT') {
								currentParam.set('value', uinputs[i].value);
							}
						}
						else {
							this.paramStore.add({
								name: name,
								value: uinputs[i].value,
								utype: 'INPUT',
								order: 0
							});
						}
					}
					/* TODO
					var uflows = this.jsonStrToArray(mock.get('uflows'));
					for (var i=0; i< uflows.length; i++) {
						this.paramStore.add({
							name: uflows[i].name,
							value: uflows[i].value,
							utype: 'FLOW',
							order: 0
						});
					}
					*/
					break;
				}
			}
		}
	},

	cancel: function() {
		this.doClose()
	},
	executeDTO: null,
	execute: function() {
		var params = {};
		var flows = {};
		this.paramStore.each(function(param) {
			var paramName = param.get('name');
			var paramValue = param.get('value');
			if (param.get('utype') == 'FLOW') {
				flows[paramName] = paramValue;
			} else if (param.get('encrypt') && param.get('modified')) {
				var origValue = param.get('origValue');
				if (paramValue == '*****' && origValue) {
					params[paramName] = '_usr:' + origValue;
				} else {
					params[paramName] = '_usr:' + paramValue;
				}
			} else {
				params[paramName] = paramValue;
			}
		}, this);

		var urlParams = clientVM.rootVM.screens.getAt(clientVM.rootVM.screens.getActiveIndex()).params;
		if ((urlParams.SIR || urlParams.sir) && (urlParams.PROBLEMID || urlParams.problemId)) {
			params.PROBLEMID = urlParams.problemId;
		} else if (this.sirProblemId !== '') {
			params.PROBLEMID = this.sirProblemId
		}

		if (this.activityId) {
			params.activityId = this.activityId;
		}

		var mockName = (this.mock) ? this.mockName : '';
		var dto = Ext.apply({
			problemId: this.newWorksheet ? 'NEW' : 'ACTIVE',
			isDebug: this.debug,
			mockName: mockName,
			params: Ext.apply(params, {
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			}),	
		}, this.executeDTO);


		this.ajax({
			url: this.API['submit'],
			jsonData: dto,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				this.doClose();
				if (response.success) {
					clientVM.updateProblemInfo(response.data.problemId, response.data.problemNumber)
					clientVM.fireEvent('worksheetExecuted', clientVM);
					if (this.activeWorksheet) {
						clientVM.fireEvent('refreshActiveWorksheet');
					}
					clientVM.displaySuccess(this.localize('executeSuccess', {
						type: !dto.wiki ? 'ActionTask' : 'Runbook',
						fullName: !dto.wiki ? dto.actiontask : dto.wiki
					}))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	executeIsEnabled$: function() {
		if (this.mockNameIsValid == true) {
			return true;
		}
		else {
			return false;
		}
	},

	jsonStrToArray: function(jsonStr) {
		if (!jsonStr)
			return [];
		var arr = [];
		var json = Ext.JSON.decode(jsonStr);
		for (key in json) {
			arr.push({
				name: key,
				value: json[key]
			})
		}
		return arr;
	}
});