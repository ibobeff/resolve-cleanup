Ext.define('RS.actiontaskbuilder.ParserEditBlockModel', {
	extend: 'RS.common.blocks.BlockModel',
	modelConfig: {		
		mixins: ['QuickAccessHelpers'],
		activeItem: 0,
		suspendBroadcastDataChange : true,
		blockModel: null,
		blockModelClass: 'Parser',
		classList: 'parser-block block-model',
		smallResolution : false,
		collapsed: true,
		hidden: true,
		editBlock: true,
		localOriginalData: {},
        title: '',	
        header: {
            titlePosition: 0,
            defaults: {
            	margin: 0
        	},
            items: []
        },			
		parserControl : {
			mtype : 'ParserControl'
		},
		testControl : {
			mtype : 'TestControl'
		},
		parserType : 'text',
		when_parser_type_change : {
			on : ['parserTypeChanged'],
			action : function(){
				this.parserControl.updateActiveparserType(this.parserType);				
			}
		},
		when_resolution_change : {
			on : ['smallResolutionChanged'],
			action : function(){
				this.parserControl.set('smallResolution', this.smallResolution);
			}
		},
		textParserIsActive$: function(){
			return this.parserType == 'text';
		},	
		tableParserIsActive$: function(){
			return this.parserType == 'table';
		},	
		xmlParserIsActive$: function(){
			return this.parserType == 'xml';
		},
		markupAndCapture: function(){
			this.set('activeItem', 0);		
		},
		markupAndCaptureIsVisible$: function(){
			return this.activeItem == 1;
		},
		parserSwitchVisibility$ : function(){
			return this.activeItem == 0 ? 'visible' : 'hidden';
		},
		test : function(){
			this.testControl.updateActiveParserType(this.getActiveType());
			this.set('activeItem', 1);
		},		
		testIsVisible$ : function(){
			return this.activeItem == 0;
		},

		updateDefaultTestSample : function(parserType,data){
			this.testControl.updateDefaultTestSample(parserType , data);
		},		
		isAssessTask: function(flag){
			this.parserControl.isAssessTask(flag);	
		},	

		getCode : function(){
			return this.parserControl.getCode();
		},
		getParserConfig : function(){
			return this.parserControl.getParserConfig();
		},
		getActiveType : function(){
			return this.parserControl.getActiveType();
		},
		getSample : function(){
			return this.parserControl.getSample();
		},
		isAutoGenCode : function(){
			return this.parserControl.isAutoGenCode();
		},
		isParserReady : function(){
			return this.parserControl.isParserReady();
		},	

		setDefaultParser: function(){		
			this.set('parserType','text');
			this.parserControl.setDefaultParser();
			if(window.loadingNewAT)
				this.testControl.resetTest();
			this.set("activeItem",0);	
		},		
		postInit: function (blockModel) {		
			this.blockModel = blockModel;
		},			
		
		closeFlag: false,
		close: function () {					
			this.updateClasses();
			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		},
		
		notifyDirty: function(){			
			this.blockModel.notifyDirty();
			this.updateClasses();			
		},

		notifyDataChange : function(){
			if(!this.suspendBroadcastDataChange){
				var newData = this.parserControl.getParameterList();
				this.blockModel.notifyDataChange(newData);
			}
		},
		updateClasses: function () {//TODO
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.parser'];
	},
	
	modifyByNotification: function(data, resolveActionInvoc, assignedTo){
		var taskType = resolveActionInvoc.utype ? resolveActionInvoc.utype.toUpperCase(): null;
		var g = this.gluModel;
		g.isAssessTask(taskType == "ASSESS");
	},

	load: function (parserData) {
		var g = this.gluModel;
		g.set('localOriginalData', parserData);
		g.set('suspendBroadcastDataChange', true);
 		g.setDefaultParser();
		var parserType = parserData ? parserData.parserType : null;		
		
		if (parserType) {		
			g.set('parserType', parserType);
			g.parserControl.loadData(parserData);
		}		
		else{
			//Make new parser compatible with old code generared from AT/AB
			var autoGenCode = true;
			if(parserData['uscript']){
				g.parserControl.loadData(parserData);
				autoGenCode = false;
			}
			//Those default setting must match whatever return from getData method.
			g.localOriginalData['parserType'] = "text";
			g.localOriginalData['sampleOutput'] = "";
			g.localOriginalData['parserAutoGenEnabled'] = autoGenCode;	
		}
		g.set('suspendBroadcastDataChange', false);
		g.notifyDataChange();
		g.updateClasses();	
	},

	getData: function () {
		var g = this.gluModel;		
		var parserData = {};
		
		//If parser is not rendered the data is not modified. (this is for embeded mode).
		if(g.isParserReady()) {
			//Clone orignal data
			var clonedData = Ext.Object.merge({},  g.get('localOriginalData'));
			parserData = Ext.apply(clonedData, {			
				parserConfig: g.getParserConfig(),
				parserType: g.getActiveType(),
				sampleOutput: g.getSample(),
				parserAutoGenEnabled: g.isAutoGenCode(),
				uscript: g.getCode()
			});		
		} else {
			parserData = g.get('localOriginalData');	
		}

		return [parserData];
	}
});