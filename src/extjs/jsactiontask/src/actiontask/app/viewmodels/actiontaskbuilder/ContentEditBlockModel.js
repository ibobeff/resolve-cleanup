Ext.define('RS.actiontaskbuilder.ContentEditBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Content',
		hidden: true,
		collapsed: true,
		conditionalEditBlock: true,
        title: '',
       	header: {
            titlePosition: 0,
            defaults: {
            	margin: 0
        	},
            items: []
        },
		classList: 'block-model',
		content: '',
		syntax: '',
		command: '',
		contentLabel: 'Content',
		editorType: 'text',
		syntaxStore: null,
		commandIsVisible: true,
		syntaxIsVisible: true,

		minHeight: 71,
		minResize: 133,
		height: 371,
		editorFlexValue$: function() {
			if (!this.parentVM.embed) {
				return 1;
			}
		},
		editorHeight$: function() {
			if (!this.parentVM.embed) {
				return this.height;
			}

			var editorHeight = this.height;
			if (this.commandIsVisible && this.syntaxIsVisible) {
				editorHeight = editorHeight - 100;
			} else if (this.syntaxIsVisible) {
				editorHeight = editorHeight - 40;
			}

			editorHeight = editorHeight - 100; // normal offset

			return editorHeight;
		},

		expand: function() {
			this.validateAndDisplayWarning(this.content);
		},

		contentChange: function() {
			if (!this.task) {
				this.task = new Ext.util.DelayedTask();
			}

			this.task.delay(500, this.saveContent, this, arguments);
		},

		saveContent: function(content) {
			var val = content.getValue();
			this.set('content', val);
			this.notifyDirty();
		},

		when_command_changed: {
			on: ['commandChanged'],
			action: function() {
				this.notifyDirty();
			}
		},

		postInit: function (blockModel) {
			this.blockModel = blockModel;
			this.set('syntaxStore', new RS.common.data.FlatStore(
				['groovy', 'golang','perl', 'powershell', 'ruby', 'sql', 'text', 'xml']
			));
		},

		trimRegex: /^\s+|\s+$/g,

		addInputFile: function() {
			this.set('command', this.command+' ${INPUTFILE}');
		},

		shrinkPanel: function () {
			this.set('minHeight', 71);
			this.set('minResize', 133);
		},

		growPanel: function () {
			this.set('minHeight', 371);
			this.set('minResize', 433);
		},

		notifyDirty: function() {
			this.blockModel.notifyDirty();
		},

		resizeSection: function(height) {
			this.set('height', height);
		},

		closeFlag: false,
		close: function () {
			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc', {
			path: 'data.resolveActionInvoc.resolveActionInvocOptions',
			filter: function (o) {
				return o.uname === 'INPUTFILE'
			}
		}];
	},

	contentOptionID: '',

	load: function (resolveActionInvoc, inputFile) {
		var g = this.gluModel;
		this.set('command', '');

		switch(resolveActionInvoc.utype) {
		case 'ASSESS':
		case 'REMOTE':
			g.set('commandIsVisible', false);
			g.set('syntaxIsVisible', false);
			break;
		case 'CSCRIPT':
		case 'POWERSHELL':
			g.set('commandIsVisible', false);
			g.set('syntaxIsVisible', true);
			break;
		default:
			g.set('commandIsVisible', true);
			g.set('syntaxIsVisible', true);

			var command = resolveActionInvoc.ucommand || '';
			var args = resolveActionInvoc.uargs || '';

			if (args.length) {
				command += ' ';
				command += args;
			}

			this.set('command', command);
			break;
		}

		if (g.syntaxIsVisible) {
			var type = resolveActionInvoc.utype;
			this.set('syntax', this.typeMaps[type]);
		}

		if (inputFile && inputFile.length > 0) {
			this.set('content', inputFile[0].uvalue);
			this.contentOptionID = inputFile[0].id;
		}
	},

	getData: function () {
		return [{
			ucommand: this.gluModel.command
		}, [{
			id: this.contentOptionID,
			uname: 'INPUTFILE',
			udescription: '',
			uvalue: this.gluModel.content
		}]];
	},

	typeMaps: {
		REMOTE: 'groovy',
		POWERSHELL: 'powershell',
		PROCESS: 'groovy'
	},

	modifyByNotification: function(data, resolveActionInvoc, assignedTo) {
		var t = resolveActionInvoc.utype,
			isNew = !data.id;

		if (typeof this.typeMaps[t] !== 'undefined') {
			this.set('editorType', this.typeMaps[t]);
		} else {
			this.set('editorType', 'text');
		}

		if (t === 'ASSESS') {
			this.set('commandIsVisible', false)
				.set('syntaxIsVisible', false);
			this.hide();
		} else {
			switch (t) {
			case 'REMOTE':
				this.setViewTitle(this.gluModel.localize('titleForRemote'));
				this.set('commandIsVisible', false)
					.set('syntaxIsVisible', false);
				break;
			case 'CSCRIPT':
				this.setViewTitle(this.gluModel.localize('titleForCScript'));
				this.set('commandIsVisible', false)
					.set('syntaxIsVisible', true)
					.set('contentLabel', this.gluModel.localize('contentLabel'));
				break;
			case 'POWERSHELL': 
				this.setViewTitle(this.gluModel.localize('titleForPowershell'));
				this.set('commandIsVisible', false)
					.set('syntaxIsVisible', true)
					.set('contentLabel', this.gluModel.localize('contentLabel'));
				break;
			case 'CMD': 
			case 'OS': 
			case 'BASH': 
				this.setViewTitle(this.gluModel.localize('titleForCommand'));
				this.set('commandIsVisible', true)
					.set('syntaxIsVisible', true)
					.set('contentLabel', this.gluModel.localize('inputFileLabel'));			
				break;
			default:
				// process or external
				this.setViewTitle(this.gluModel.localize('titleForDefault'));
				this.set('commandIsVisible', true)
					.set('syntaxIsVisible', true)
					.set('contentLabel', this.gluModel.localize('contentLabel'));
				break;
			}

			if ((!isNew && !this.gluModel.parentVM.readOnlyMode) &&
				(this.gluModel.parentVM.sectionButtonSelected == this.gluModel.parentVM.sectionButtonMap['CONTENT'] || this.gluModel.parentVM.sectionButtonSelected == this.gluModel.parentVM.sectionButtonMap['ALL']))
			{
				//this.showCurrent();
				this.gluModel.set('hidden', false);
			}
		}
	}	
});