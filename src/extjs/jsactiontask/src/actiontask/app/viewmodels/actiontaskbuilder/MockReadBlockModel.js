Ext.define('RS.actiontaskbuilder.MockReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Mock',
		classList: 'block-model',
		collapsed: true,
		hidden: false,
        title: '',
        header: {
            titlePosition: 0,
            defaults: {
            	margin: '0'
        	},
            items: []
        },

		mockStore: {
			mtype: 'store',
			fields: ['id', 'uname', 'udescription', 'udata', 'uparams', 'uinputs', 'uflows', 'sysUpdatedBy', 'sysUpdatedOn', 'sysCreatedBy', 'sysCreatedOn'],
			proxy: {
				type: 'memory'
			}
		},

		mockDetailsStore: {
			mtype: 'store',
			fields: ['type', 'name', 'value'],
			proxy: {
				type: 'memory'
			}
		},

		maxHeight: 300,
		hasMockValues: false,
		hasMockParamValues: false,
		rawIsHidden: false,
		hasMockDescription: false,

		postInit: function (blockModel) {
			this.blockModel = blockModel;
			this.maxHeight = Math.round(clientVM.getWindowHeight() / 3);
		},

		isShowingDetails: false,

		Mock: function() {},
		MockIsVisible$: function() {
			return !this.isShowingDetails;
		},

		MockDetails: function() {
		},
		MockDetailsIsVisible$: function() {
			return this.isShowingDetails;
		},

		viewDetailsTooltip$: function() {
			return this.localize('viewMockDetails');
		},

		mockName: '',
		mockDescription: '',
		mockRaw: '',

		when_isShowingDetails_changed: {
			on: ['isShowingDetailsChanged'],
			action: function () {
				if (this.isShowingDetails) {
					this.set('title', this.localize('mockDetailsTitle') + ' - ' + this.mockName);
				} else {
					this.set('title', this.localize('mockTitle'));
				}
			}
		},

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden && this.parentVM.embed) {
					var mockData = this.parentVM.mockEdit.getData();
					this.parentVM.mockRead.load(mockData[0]);
				}
			}
		},

		viewMockDetails: function(record) {
			this.set('mockName', record.get('uname'));
			this.set('mockDescription', record.get('udescription') || '');
			this.set('isShowingDetails', true);
			this.set('mockRaw', record.get('udata'));
			this.set('hasMockDescription', this.mockDescription.length > 0);
			this.mockDetailsStore.removeAll();

			var inputs = record.get('uinputs');
			for (var i = 0; i < inputs.length; i++) {
				this.mockDetailsStore.add({
					type: 'INPUT',
					name: inputs[i].name,
					value: inputs[i].value
				});
			}

			var flows = record.get('uflows');
			for (var i = 0; i < flows.length; i++) {
				this.mockDetailsStore.add({
					type: 'FLOW',
					name: flows[i].name,
					value: flows[i].value
				});
			}

			var params = record.get('uparams');
			for (var i = 0; i < params.length; i++) {
				this.mockDetailsStore.add({
					type: 'PARAMS',
					name: params[i].name,
					value: params[i].value
				});
			}

			var hasMockParamValues = this.mockDetailsStore.getCount() > 0;
			this.set('hasMockParamValues', hasMockParamValues);
		},

		notifyMockDetailsDataDirty: function(record) {
			if (this.isShowingDetails) {
				this.set('mockName', record.get('uname'));
				this.set('mockDescription', record.get('udescription') || '');
				this.set('mockRaw', record.get('udata'));
				this.set('hasMockDescription', this.mockDescription.length > 0);
				this.mockDetailsStore.removeAll();
	
				var inputs = record.get('uinputs');
				for (var i = 0; i < inputs.length; i++) {
					this.mockDetailsStore.add({
						type: 'INPUT',
						name: inputs[i].name,
						value: inputs[i].value
					});
				}
	
				var flows = record.get('uflows');
				for (var i = 0; i < flows.length; i++) {
					this.mockDetailsStore.add({
						type: 'FLOW',
						name: flows[i].name,
						value: flows[i].value
					});
				}
	
				var params = record.get('uparams');
				for (var i = 0; i < params.length; i++) {
					this.mockDetailsStore.add({
						type: 'PARAMS',
						name: params[i].name,
						value: params[i].value
					});
				}
	
				var hasMockParamValues = this.mockDetailsStore.getCount() > 0;
				this.set('hasMockParamValues', hasMockParamValues);
			}
		},

		back: function () {
			this.set('isShowingDetails', false);			
		},

		backIsVisible$: function() {
			return this.isShowingDetails;
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.resolveActionTaskMockData'];
	},

	load: function (resolveActionTaskMockData) {
		resolveActionTaskMockData = resolveActionTaskMockData || [];
		this.callParent(arguments);
		this.gluModel.mockStore.removeAll();

		for (var i = 0; i < resolveActionTaskMockData.length; i++) {
			var mock = resolveActionTaskMockData[i];

			var uinputs = this.jsonStrToArray(mock.uinputs),
				uflows = this.jsonStrToArray(mock.uflows),
				uparams = this.jsonStrToArray(mock.uparams);

			this.gluModel.mockStore.add({
				id: mock.id || '',
				uname: mock.uname || '',
				udescription: mock.udescription || '',
				udata: mock.udata,
				uparams: uparams,
				uinputs: uinputs,
				uflows: uflows,
				sysUpdatedBy: mock.sysUpdatedBy,
				sysUpdatedOn: mock.sysUpdatedOn,
				sysCreatedBy: mock.sysCreatedBy,
				sysCreatedOn: mock.sysCreatedOn
			});
		}

		var hasMockValues = resolveActionTaskMockData.length > 0,
			classes = ['block-model'];

		this.set('hasMockValues', hasMockValues)
			.set('classList', classes);
	},

	jsonStrToArray: function(jsonStr) {
		if (!jsonStr)
			return [];
		var arr = [];
		var json = Ext.JSON.decode(jsonStr);
		for (key in json) {
			arr.push({
				name: key,
				value: json[key]
			})
		}
		return arr;
	},

	modifyByNotification: function (data, resolveActionInvoc) {
		this.set('rawIsHidden', resolveActionInvoc.utype === 'ASSESS');
	},

	notifyMockDetailsDataDirty: function (record) {
		if (this.gluModel.isShowingDetails) {
			this.gluModel.notifyMockDetailsDataDirty(record);
		}
	}

});
