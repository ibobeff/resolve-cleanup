Ext.define('RS.actiontaskbuilder.ReferencesReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		referencesData : [],
		blockModel: null,
		blockModelClass: 'References',
		hidden: false,
		readAndEditBlock: true,
		collapsed: true,
		classList: 'block-model',
		title: '',
		header: {
			titlePosition: 0,
			defaults: {
				margin: 0
			},
			items: []
		},
		gridIsEmpty: false,

		referencesStore: {
			mtype: 'store',
			fields: ['name', 'refInContent', 'refInMain', 'refInAbort'],
			sorters: [{
		    	property: 'name',
		    	direction: 'ASC'
			}],
			proxy: {
				type: 'memory'
			}
		},

		postInit: function (blockModel) {
			this.blockModel = blockModel;
		},		

		editReference: function(docName) {
			clientVM.handleNavigation({
				modelName: 'RS.wiki.Main',
				params: {
					name: docName
				},
				target: '_blank'
			})
		}
	},

	getDataPaths: function () {
		return ['data.referencedIn'];
	},
	getData : function() {
		return [this.gluModel.referencesData];		
	},
	load: function (referencesData) {
		referencesData = referencesData || [];
		this.callParent(arguments);
		this.gluModel.referencesStore.removeAll();

		for (var i = 0; i < referencesData.length; i++) {
			var reference = referencesData[i];

			this.gluModel.referencesStore.add({
				name: reference.name || '',
				refInContent: reference.refInContent,
				refInMain: reference.refInMain,
				refInAbort: reference.refInAbort
			});
		}
		this.gluModel.set('referencesData', referencesData);
		this.gluModel.set('gridIsEmpty', this.gluModel.referencesStore.count() === 0);
	}
});