Ext.define('RS.actiontaskbuilder.RolesEditBlockModel', (function () {

	function getAllowedRoles(accessStore, roles) {
		if (roles.constructor !== Array) {
			roles = roles.getRange();
		}

		var roleNames = [];

		for (var i = 0; i < roles.length; i++) {
			roleNames.push(roles[i].data.uname);
		}

		var assignedRoles = accessStore.getRoles();

		for (var i = 0; i < assignedRoles.length; i++) {
			var indexOfAssignedRole = roleNames.indexOf(assignedRoles[i]);

			if (indexOfAssignedRole > -1) {
				roleNames.splice(indexOfAssignedRole, 1);
			}
		}

		return roleNames;
	}

	return {
		extend: 'RS.common.blocks.BlockModel',

		modelConfig: {
			mixins: ['QuickAccessHelpers'],
			blockModel: null,
			blockModelClass: 'General',
			hidden: true,
			collapsed: true,
			editSubBlock: true,
			stopPropagation: false,
			title: '',
			header: {
		 		xtype: 'toolbar',
		    	margin: 0,
		    	padding: 0,
		    	items: []
			},
			defaultRoles: true,
			hideAdminColumn: false,
			accessStore: null,
			roleCount: 0,
			gridIsEmpty: false,
			itemsExhausted: false,
			savingNewTask: false,

			rolesStore: {
				mtype: 'store',
				fields: ['uname'],
				proxy: {
					type: 'ajax',
					url: '/resolve/service/common/roles/list',
					reader: {
						type: 'json',
						root: 'records'
					}
				}
			},
			allowedRolesStore: null,
			allowedRolesStoreForGridRow: null,
			detail: {
				mtype: 'Access'
			},

			when_defaultRoles_changed: {
				on: ['defaultRolesChanged'],
				action: function () {
					this.notifyDirty();
				}
			},

			when_section_isValid_changed: {
				on: ['sectionIsValidChanged'],
				action: function(){
					if(this.blockModel)
						this.notifyValid(this.sectionIsValid);
				}
			},
			
			sectionIsValid$: function(){
				return this.defaultRoles ||	 this.roleCount > 0;
			},

			isButtonEnabled$: function () {
				return this.sectionIsValid;
			},

			defaultRolesModified: function (value) {
				this.detail.setDisabled(value);
				
				if (value) {
					this.loadDefaultRoles(false);
				}
			},

			loadDefaultRoles: function (suspendDirtyNotification) {
				if (this.defaultRoles) {
					this.ajax({
						scope: this,
						url: '/resolve/service/actiontask/getDefaultRoles',
						method: 'post',
						success: function (response) {
							var payload = RS.common.parsePayload(response);
							
							if (payload.success) {
								this.blockModel.suspendDirtyNotification = suspendDirtyNotification;
								this.accessStore.loadData(payload.data);
								this.set('defaultRoles', true);
								var count = this.accessStore.count();
								this.set('roleCount', count);
								this.set('gridIsEmpty', count === 0);
								this.detail.setDisabled(true);

								var records = this.rolesStore.getRange();
								var roles = getAllowedRoles(this.accessStore, records);
								this.allowedRolesStore.loadData(this.allowedRolesStore.shapeData(roles));
								this.detail.setRoles(roles);
								this.blockModel.suspendDirtyNotification = false;
							} else {
								clientVM.displayError(this.localize('serverError', payload.message));
							}
						},
						failure: function (response) {
							clientVM.displayFailure(response);
						}
					});
				}
			},

			postInit: function (blockModel) {
				this.blockModel = blockModel;			
				this.set('allowedRolesStore', new RS.common.data.FlatStore());
				this.set('allowedRolesStoreForGridRow', new RS.common.data.FlatStore());
				this.rolesStore.on('beforeload', function(store, operation, opts) {
					operation.params = operation.params || {};
					Ext.apply(operation.params, {
						includePublic : "true",
					});
				}, this);

				this.rolesStore.load({
					scope: this,
					addRecords: false,
					limit: 100,
					page: 1,
					start: 0,
					callback: function (records, operation, success) {
						if (success) {
							this.allowedRolesStore.removeAll();
							var roles = getAllowedRoles(this.accessStore, records);
							this.allowedRolesStore.loadData(this.allowedRolesStore.shapeData(roles));
							this.detail.setRoles(roles);
						}
					}
				});
				
				this.set('accessStore', new RS.common.data.AccessStore({
					datachanged: function () {
						this.notifyDirty();
					}.bind(this),
					update: function (record, operation) {
						var modified = operation.modified;

						if (modified.hasOwnProperty('uname') && modified.uname !== operation.data.uname) {
							this.detail.removeRole(operation.data.uname);
							this.detail.addRole(modified.uname);
						}

						this.notifyDirty();
					}.bind(this)
				}));
				
				var hidden = true;

				if (clientVM.user.roles === 'undefined' || typeof clientVM.user.roles === null) {
					// resolve.maintenance user
					hidden = false;
				} else {
					if (clientVM.user.roles && clientVM.user.roles.constructor === Array) {
						hidden = clientVM.user.roles.indexOf('admin') === -1;	
					}
				}				

				this.set('hideAdminColumn', hidden);
				this.titleComponent = {
		        	xtype: 'component',
		        	padding: '0 0 0 15',
		        	cls: 'x-header-text x-panel-header-text-container-default',
		        	html: this.localize('generalTitle'),
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
							}, this);
						}.bind(this)
					}
		        };
				this.header.items = [this.titleComponent, {
		        	xtype: 'container',
		        	autoEl: 'ol',
		        	cls: 'breadcrumb flat',
		        	margin: 0,
		        	padding: 0,
		        	items: [{
			    		xtype: 'component',
			    		autoEl: 'li',
			    		html: this.localize('generalPropertiesEditTitle'),
						listeners: {
							afterrender: function (c) {
								c.getEl().on('click', function () {
									this.stopPropagation = true;
									this.jumpTo('GeneralEdit', this.parentVM.generalEdit.gluModel);
					            }, this);
							}.bind(this)
			    		}			    		
			    	}, {
			    		xtype: 'component',
			    		autoEl: 'li',
			    		cls: 'active',
			    		html: this.localize('rolesEditTitle'),
						listeners: {
							afterrender: function (c) {
								c.getEl().on('click', function () {
									this.stopPropagation = true;
								}, this);
							}.bind(this)
						}
			    	}, {
			    		xtype: 'component',
			    		autoEl: 'li',
			    		html: this.localize('optionsEditTitle'),
						listeners: {
							afterrender: function (c) {
								c.getEl().on('click', function () {
									this.stopPropagation = true;
									this.jumpTo('OptionsEdit', this.parentVM.optionsEdit.gluModel);
					            }, this);						
							}.bind(this)
			    		}
			        }]
		        }, '->', {
					xtype: 'button',
					tooltip: this.localize('resizeFullscreen'),
					iconCls: 'rs-block-button icon-resize-full',
					isResizeMaxBtn: true,
					margin: '0 5',
					text: '',
					style: {
						opacity: 0.75
					},
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['GENERAL']);
							}, this);
						}.bind(this)
					}
				}, {
					xtype: 'button',
					tooltip: this.localize('resizeNormalscreen'),
					iconCls: 'rs-block-button icon-resize-small',
					hidden: true,
					isResizeNormalBtn: true,
					margin: '0 20',
					text: '',
					style: {
						opacity: 0.75
					},
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.parentVM.resizeNormalSection(this);
							}, this);
						}.bind(this)
					}
				}, {
					xtype: 'button',
					iconCls: 'rs-block-button icon-chevron-sign-up',
					isCollapseBtn: true,
					margin: '0 5',
					text: '',
					style: {
						opacity: 0.75
					},
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.set('collapsed', true);
							}, this);
						}.bind(this)
					}
				}];
			},

			defaultRolesOrSavingNewTask$: function () {
				return this.defaultRoles || this.savingNewTask;
			},

			notifyDirty: function() {
				this.blockModel.notifyDirty();
			},

			notifyValid: function (valid) {
				this.blockModel.notifyValid(valid);		
			},

			jumpTo: function(nextBlockName, nextSection){
				this.parentVM.generalEditSectionExpanded = !this.collapsed;
				this.parentVM.generalEditSectionMaximized = this.isSectionMaxSize;
				this.parentVM.collapseAndHideSection(this);
				this.parentVM.showAndExpandThisSection(nextSection);
			},

			when_hidden_changed: {
				on: ['hiddenChanged'],
				action: function() {
					if (!this.hidden) {
						setTimeout(function() {
							if (this.parentVM.generalEditSectionExpanded) {
								this.parentVM.generalEditSectionExpanded = null;
								this.set('collapsed', false);
							}
							if (this.parentVM.generalEditSectionMaximized) {
								this.parentVM.generalEditSectionMaximized = null;
								this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['GENERAL']);
							}
						}.bind(this), 10);
					}
				}
			},

			closeFlag: false,
			close: function () {
				if (this.isSectionMaxSize) {
					this.parentVM.resizeNormalSection(this);
				}
				this.closeFlag = true;
				this.set('collapsed', true);
			},
	
			add: function (access) {
				this.accessStore.add(access);
				var count = this.accessStore.count();
				this.set('roleCount', count);
				this.set('gridIsEmpty', count === 0);
				this.hideAddFormOnExhaust();
			},
			
			updateAllowedRolesStore: function(context) {
				var roles = getAllowedRoles(this.accessStore, this.rolesStore);
				roles.push(context.value);
				var roleData = this.allowedRolesStoreForGridRow.shapeData(roles);
				this.allowedRolesStoreForGridRow.loadData(roleData);
			},

			addRoleToDetail: function (record) {
				var count = this.accessStore.count();
				this.set('roleCount', count);
				this.set('gridIsEmpty', count === 0);
				this.detail.addRole(record.data.uname);
				this.set('itemsExhausted', false);
			},

			hideAddFormOnExhaust: function() {
				this.set('itemsExhausted', this.accessStore.getCount() === this.rolesStore.getCount());
			},

			hideAddRoleForm$: function() {
				if (this.itemsExhausted || this.defaultRoles) {
					return true;
				} else {
					return false;
				}
			}
		},

		disableForm: function () {
			this.set('savingNewTask', true);
		},

		enableForm: function () {
			this.set('savingNewTask', false);
		},

		getDataPaths: function () {
			return ['data', 'data.accessRights'];
		},

		getData: function (dataIsForServer) {
			var g = this.gluModel;

			return [{
				uisDefaultRole: g.defaultRoles
			}, g.accessStore.getAccessRights(dataIsForServer)];
		},

		load: function (data, accessRights) {
			var g = this.gluModel;
			g.accessStore.loadData(accessRights);
			var count = g.accessStore.count();
			this.set('defaultRoles', data.uisDefaultRole)
				.set('roleCount', count)
				.set('gridIsEmpty', count === 0);
			g.detail.setDisabled(data.uisDefaultRole);
			var records = g.rolesStore.getRange();
			var roles = getAllowedRoles(g.accessStore, records);
			g.allowedRolesStore.loadData(g.allowedRolesStore.shapeData(roles));
			g.detail.setRoles(roles);
		},

		show: function () {
			this.gluModel.loadDefaultRoles(true);
			this.callParent(arguments);
			this.gluModel.hideAddFormOnExhaust();
		}
	};
})());