glu.defModel('RS.actiontaskbuilder.Expression', (function () {
	var enterOperand = '',
		selectOperand = '',
		leftOperandCombo = null,
		rightOperandCombo = null,
		operandTypes = ['CONSTANT', 'INPUT', 'OUTPUT', 'FLOW', 'PARAM', 'WSDATA', 'PROPERTY'];

	function operandFilterFactory (operandType) {
		var filter = function () { return true; };

		if (operandType === 'INPUT') {
			filter = function (p) {
				return p && p.utype === 'INPUT' || p.utype === null;
			}
		}else if (operandType === 'OUTPUT') {
			filter = function (p) {
				return p && p.utype === 'OUTPUT' || p.utype === null;
			}
		} else if (operandType === 'PROPERTY') {
			filter = function (p) {
				return p && p.utype === 'Plain';
			}
		}

		return filter;
	}

	function isOperandEditable(value) {
		return value !== 'INPUT' && value !== 'OUTPUT' && value !== 'PROPERTY';
	}

	function loadOperandStoreByParameters(store, operandName, parameters) {
		var operandNameIsValid = true;

		store.accessors([
			function (p) { return p.uname; },
			function (p) { return p.uname; }
		]);

		operandNameIsValid = loadDynamicOperandStore(store, operandName, parameters);
		store.filterBy(function (p) {
			return p && p.utype === 'INPUT' || p.utype === null;
		});
		return operandNameIsValid;
	}

	function loadOperandStoreByProperties(store, operandName, properties) {
		var operandNameIsValid = true;

		store.accessors([
			function (p) { return p.uname; },
			function (p) { return p.uname; }
		]);

		operandNameIsValid = loadDynamicOperandStore(store, operandName, properties);
		store.filterBy(function (p) {
			return p && p.utype === 'Plain';
		});
		return operandNameIsValid;
	}

	function loadDynamicOperandStore (store, operandName, reportedData) {
		var memberMap = {},
			current = store.getData();

		for (var i = 0; i < reportedData.length; i++) {
			memberMap[reportedData[i].uname] = {
				isReported: true,
				isCurrent: false
			};
		}

		for (var i = 0; i < current.length; i++) {
			if (memberMap[current[i].value]) {
				memberMap[current[i].value].isCurrent = true;
			} else {
				memberMap[current[i].value] = {
					isReported: false,
					isCurrent: true
				}
			}
		}

		for (var key in memberMap) {
			var m = memberMap[key];

			if (m.isReported && !m.isCurrent) {
				store.add({
					text: key,
					value: key
				});
			} else if (!m.isReported && m.isCurrent) {
				store.removeBy('value', key);
			}
		}

		var operandIsValid = false;

		if (operandName && memberMap[operandName]) {
			operandIsValid = memberMap[operandName].isReported;
		}

		return operandIsValid;
	}

	function modifyOperandTypesByParameters(parameters, parameterType) {
		var typeExists = parameters.length > 0;
		if(parameterType == 'INPUT')
			spliceOperandType(typeExists, 'INPUT', 1, operandTypes);
		else 
			spliceOperandType(typeExists, 'OUTPUT', 1, operandTypes);
		return typeExists;
	}
	
	function modifyOperandTypesByProperties(properties) {
		var i = 0,
			propertyTypeName = operandTypesMap['PROPERTY'],
			propertyTypeExists = false;

		while (!propertyTypeExists && i < properties.length) {
			propertyTypeExists = properties[i].utype === propertyTypeName;
			i++;
		}

		spliceOperandType(propertyTypeExists, 'PROPERTY', 4, operandTypes);
		return propertyTypeExists;
	}

	function spliceOperandType (operandTypeExists, operandType, desiredIndex, operandTypes) {
		if (operandTypeExists) {
			var found = false;

			for (i = 0; i < operandTypes.length; i++) {
				if (operandTypes[i] === operandType) {
					found = true;
					break;
				}
			}

			if (!found) {
		    	operandTypes.splice(desiredIndex, 0, operandType);
			}
		} else {
			var foundAtIndex = -1;

			for (i = 0; i <= desiredIndex; i++) {
				if (operandTypes[i] === operandType) {
					foundAtIndex = i;
					break;
				}
			}

			if (foundAtIndex >= 0) {
				operandTypes.splice(foundAtIndex, 1);
			}
		}
	}

	function updateExpression(expression) {
		var expressionData = [expression.criteria, ' '];
		var expressionOperatorStoreIndex = expression.operatorStore.find('value', expression.operator);
		if(expressionOperatorStoreIndex != -1)
			var expressionOperatorName = expression.operatorStore.getAt(expressionOperatorStoreIndex).get('text');
		else
			expressionOperatorName = '';
		if (expression.leftOperandType !== 'CONSTANT' && expression.rightOperandType !== null) {
			expressionData.push(expression.leftOperandType);
			expressionData.push('.');
		}

		expressionData.push(expression.leftOperandName);
		expressionData.push(' ');
		expressionData.push(expressionOperatorName);
		expressionData.push(' ');

		if (expression.rightOperandType !== 'CONSTANT' && expression.rightOperandType !== null) {
			expressionData.push(expression.rightOperandType);
			expressionData.push('.');
		}

		expressionData.push(expression.rightOperandName);
		return expressionData.join('');
	}

	var operandTypesMap = {
		CONSTANT: 'CONSTANT',
		INPUT: 'INPUT',
		FLOW: 'FLOW',
		PARAM: 'PARAM',
		WSDATA: 'WSDATA',
		PROPERTY: 'Plain'
	};

	return {
		fields : ['category','leftOperandType','leftOperandName','operator','rightOperandType','rightOperandName'],
		key: 0,
		id: '',
		priorData: null,
		isReadOnly: false,	
		allParameters : {
			INPUT : [],
			OUTPUT : [],
			FLOW : [],
			WSDATA : [],
			PROPERTY : []
		},		
		propertiesUpdated: false,
		expression: '',
		determinerStore: null,
		operandTypeStore: null,		
		category: 0,
		categoryStore : null,
		operatorStore: {
			mtype : 'store',
			fields : ['text', 'value'],
			proxy : {
				type : 'memory'
			},
			data : [{
				text : '=',
				value : '='
			},{
				text :'!=',
				value : '!='
			},{
				text : '>',
				value : '>'
			},{
				text : '>=',
				value : '>='
			},{
				text : '<',
				value : '<'
			},{
				text : '<=',
				value : '<='
			},{
				text : 'contains',
				value : 'contains'
			},{
				text : 'not contains',
				value : 'notContains'
			},{
				text : 'starts with',
				value : 'startsWith'
			},{
				text : 'ends with',
				value : 'endsWith'
			}]
		},
		criteria: 'ANY',		
		leftOperandStore : {
			mtype : 'store',
		    fields: ['name']	   	
		},
		rightOperandStore : {
			mtype : 'store',
		    fields: ['name']		  
		},
		isRemoveHidden: false,
		isOrHidden: false,
		isAddHidden: true,
		isExpressionHidden: true,
		isFormHidden: true,
		addMode: false,		
	
		when_left_operand_type_changed : {
			on : ['leftOperandTypeChanged'],
			action : function(){
				var currentType = this.leftOperandType;
				this.set('leftOperandName', null);
				this.loadOperandNameStore(this.leftOperandStore, currentType);
			}
		},			
		when_right_operand_type_changed : {
			on : ['rightOperandTypeChanged'],
			action : function(){
				var currentType = this.rightOperandType;
				this.set('rightOperandName', null);
				this.loadOperandNameStore(this.rightOperandStore, currentType);				
			}
		},

		init: function () {
			this.key = new Date().getTime();
		},

		leftOperandIsText$: function(){
			return this.leftOperandType === 'CONSTANT' || this.leftOperandType == 'PARAM';
		},
		leftOperandEditable$ : function(){
			return this.leftOperandType === 'WSDATA' || this.leftOperandType == 'FLOW';
		},
		rightOperandIsText$: function(){
			return this.rightOperandType === 'CONSTANT' || this.rightOperandType == 'PARAM';
		},
		rightOperandEditable$ : function(){
			return this.rightOperandType === 'WSDATA' || this.rightOperandType == 'FLOW';
		},

		//Validation
		leftOperandTypeIsValid$ : function(){
			return this.leftOperandType ? true : this.localize('requiredField'); 
		},
		leftOperandNameIsValid$ : function(){
			return this.leftOperandName ? true : this.localize('requiredField'); 
		},
		rightOperandTypeIsValid$ : function(){
			return this.rightOperandType ? true : this.localize('requiredField'); 
		},
		rightOperandNameIsValid$ : function(){
			return this.rightOperandName? true : this.localize('requiredField'); 
		},
		operatorIsValid$ : function(){
			return this.operator ? true : this.localize('requiredField'); 
		},
		
		markParametersUpdated: function () {
			this.parametersUpdated = true;
		},

		markPropertiesUpdated: function () {
			this.propertiesUpdated = true;
		},

		initExpression: function (allParameters, allProperties, readOnly) {
			this.set('isReadOnly', !!readOnly);
			this.set('determinerStore', new RS.common.data.FlatStore(['ANY', 'ALL']));
			this.set('operandTypeStore', new RS.common.data.FlatStore(operandTypes));
			var categoryStore = new RS.common.data.FlatStore();

			if(this.parentVM.parentVM.parentVM.expressionType == "condition"){
				categoryStore.loadData([{
					text: 'BAD',
					value: 1
				}, {
					text: 'GOOD',
					value: 0
				}]);
			} else {
				categoryStore.loadData([{
					text: 'CRITICAL',
					value: 3
				}, {
					text: 'SEVERE',
					value: 2
				}, {
					text: 'WARNING',
					value: 1
				}, {
					text: 'GOOD',
					value: 0
				}]);
			}

			this.set('categoryStore', categoryStore);
			this.updateParameters(allParameters);		
			this.set('expression', updateExpression(this));
		},		
		loadOperandNameStore : function(store, dataType){
			if(dataType == 'INPUT')
				store.loadData(this.allParameters['INPUT']);
			else if(dataType == 'OUTPUT')
				store.loadData(this.allParameters['OUTPUT']);
			else if(dataType == 'FLOW')
				store.loadData(this.allParameters['FLOW']);
			else if(dataType == 'WSDATA')
				store.loadData(this.allParameters['WSDATA']);
			else if(dataType == 'PROPERTY')
				store.loadData(this.allParameters['PROPERTY']);
		},
		
		enableRemove: function () {
			this.set('isOrHidden', false);

			if (!this.isReadOnly) {
				this.set('isRemoveHidden', false);
			}
		},

		disableRemove: function () {
			this.set('isOrHidden', true);
			this.set('isRemoveHidden', true);
		},

		enableAdd: function () {
			this.set('isAddHidden', false);
		},

		disableAdd: function () {
			this.set('isAddHidden', true);
		},

		showExpression : function(){
			this.set('isExpressionHidden', false);
			this.set('isFormHidden', true);
			this.disableAdd();
		},

		getData: function (expressionType, order) {
			return {
				id: this.id,
				expressionType: expressionType || null,
				criteria: this.criteria,
				leftOperandType: this.leftOperandType,
				leftOperandName: this.leftOperandName,
				operator: this.operator,
				rightOperandType: this.rightOperandType,
				rightOperandName: this.rightOperandName,
				resultLevel: this.category || 0,
				order: order || 0
			};
		},

		add: function () {
			this.set('addMode', true);
			this.disableAdd();
			this.edit();
		},

		edit: function () {
			if (!this.isReadOnly) {
				this.priorData = this.getData();			
				this.set('isExpressionHidden', true);
				this.set('isFormHidden', false);
				this.disableRemove();
			}
		},
		

		cancel: function () {
			this.set('criteria', this.priorData.criteria);
			this.set('leftOperandType', this.priorData.leftOperandType);
			this.set('leftOperandName', this.priorData.leftOperandName);
			this.set('operator', this.priorData.operator);
			this.set('rightOperandType', this.priorData.rightOperandType);
			this.set('rightOperandName', this.priorData.rightOperandName);
			this.set('isFormHidden', true);		
		
			if (this.isLast()) {
				this.enableAdd();
				this.disableRemove();
				this.set('isExpressionHidden', true);
			} else {
				this.enableRemove();
				this.set('isExpressionHidden', false);
			}

			this.set('addMode', false);
		},

		done: function (btn) {
			if(!this.isValid){
				this.set('isPristine', false);
				return;
			}
			if (this.isLast()) {
				this.parentVM.addEmpty();
			}

			this.set('expression', updateExpression(this));
			this.set('isFormHidden', true);
			this.set('isExpressionHidden', false);
			this.enableRemove();

			if (this.parentVM.isLastBlock() && this.parentCount() > 1) {
				this.parentVM.addEmptyBlock();
			}

			this.parentVM.checkExpressionCategory(this);
			this.set('addMode', false);
			this.parentVM.parentVM.parentVM.blockModel.notifyDirty();
		},

		parentCount: function () {
			return this.parentVM.count();
		},

		remove: function () {
			this.parentVM.remove(this);
			this.parentVM.parentVM.parentVM.blockModel.notifyDirty();
		},

		updateParameters: function (data) {
			for(var type in data){
				this.allParameters[type] = data[type];
			}
			//Repopulate store for each operands
			this.loadOperandNameStore(this.leftOperandStore, this.leftOperandType);
			this.loadOperandNameStore(this.rightOperandStore, this.rightOperandType);
		},	

		isLast: function () {
			return this.parentVM.isLastExpression(this);
		}
	};
})());