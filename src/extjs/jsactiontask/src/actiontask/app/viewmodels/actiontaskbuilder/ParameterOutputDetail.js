glu.defModel('RS.actiontaskbuilder.ParameterOutputDetail', {
	uname: '',
	udescription: '',
	udefaultValue: '',
	uencrypt: false,
	urequired: true,
	ugroupName: null,
	utype: 'OUTPUT',
	uorder: 0,
	addOutputTitle: '',

	setAddOutputButtonTitle: function (title) {
		this.set('addOutputTitle', title);
	},

	addOutput: function () {
		this.parentVM.addOutputToList(this);
		this.set('uname', '');
		this.set('udescription', '');
		this.set('udefaultValue', '');
		this.set('uencrypt', false);
		this.set('urequired', true);
	}
});