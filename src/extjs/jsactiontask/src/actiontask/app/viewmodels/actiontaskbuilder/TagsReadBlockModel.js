Ext.define('RS.actiontaskbuilder.TagsReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Tags',
		hidden: false,
		collapsed: true,
		classList: 'block-model',
		title: '',
		header: {
			titlePosition: 0,
			defaults: {
				margin: 0
			},
			items: []
		},
		gridIsEmpty: false,
		store: null,
		tags: [],
		allTags: [],
		noDescription: '',

		postInit: function (blockModel) {
			this.blockModel = blockModel;
			this.noDescription = this.localize('noTagDescription');
			this.set('store', new RS.common.data.FlatStore({
				fields: ['name', 'description']
			}));

			this.allTags = this.rootVM.allTags;
		},		

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden && this.parentVM.embed) {
					var tagsData = this.parentVM.tagsEdit.getData();
					this.parentVM.tagsRead.load(tagsData[0]);
				}
			}
		},

		updateAllTags: function() {
			this.set('allTags', this.rootVM.allTags);
		},
	
		isGetAllTagsInProgress: function() {
			return this.rootVM.getAllTagsInProgress;
		},

		processAllTags: function() {
			var tagDescriptions = {};
	
			for (var i = 0; i < this.allTags.length; i++) {
				var tag = this.allTags[i];
				tagDescriptions[tag.name] = tag.description;
			}
	
			var shapedTags = [];
	
			for (var i = 0; i < this.tags.length; i++) {
				var t = {
					name: '',
					description: ''
				};
	
				t.name = this.tags[i];
				t.description = tagDescriptions[this.tags[i]] || this.noDescription;
				shapedTags.push(t);
			}
	
			this.store.loadData(shapedTags);
			this.set('gridIsEmpty', this.store.count() === 0);
		}
	},

	getDataPaths: function () {
		return ['data.tags'];
	},

	load: function (tags) {
		this.getAllTags(tags || []);
	},

	getAllTags: function (tags) {
		var g = this.gluModel,
			classes = ['block-model'];

		g.tags = tags;

		g.set('classList', classes);

		g.updateAllTags();
		if (g.allTags.length) {
			g.processAllTags();
		} 
		else if (g.isGetAllTagsInProgress()) {
			var id = setInterval(function() {
				if (!g.isGetAllTagsInProgress()) {
					clearTimeout(id); // exit 
					g.updateAllTags();
					g.processAllTags();
				}
			}, 100);
		}
	}
});
