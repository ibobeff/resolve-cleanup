glu.defModel("RS.actiontaskbuilder.TableParser",{
	mixins : ['VariableUtil','BoundaryUtil'],	
	sample :'',
	smallResolution : false,	
	newVariableCounter : 1,
	markups : [],
	columnSeparator : null,
	rowSeparator : null,
	trim : true,	
	parserReady : false,
	loadingData: false,
	parserData : null,
	selectedColumn : null,
	tabWidth : 8,
	variableColumn : null,
	selectionInfo : null,
	parseStart : '',
	parseStartOrignalData : '',
	startTextIncluded : true,
	parseEnd : '',
	parseEndOrignalData : '',
	endTextIncluded : true,
	totalColumn : 0,
	activeScreen : 0,
	regExpForSeparatorMap : {
		comma : '[,]+',
		space : '[ ]+',
		tab : '\\t+',
		unix :  Ext.is.Windows ? '\r\n' : '\n',
		win :  Ext.is.Windows ? '\r\n' : '\n',
	},
	codeScreen : {	
		mtype : 'ParserCode',
		parserType : 'table'
	},
	colorMap:['black','#276888','#BD6161','#F3955D','#96BB35','#50C6E8'],
	variableStore : {
		mtype : 'store',
		fields : ['variable','column','color','containerType']
	},
	columnSeparatorStore : null,
	rowSeparatorStore : null,
	columnDropdownStore : {
		mtype : 'store',
		fields : ['display','value','color'],
		proxy : {
			type : 'memory'
		}
	},	
	total_column_changed : {
		on : ['totalColumnChanged'],
		action : function(){
			this.columnDropdownStore.removeAll();
			if(this.totalColumn > 0){				
				for(var i = 1; i <= this.totalColumn;i++){
					var color = this.colorMap[(i - 1) % 6];
					this.columnDropdownStore.add({
						display : 'Column ' + i,
						value : i,
						color : color
					})
				}
			}
		}
	},
	when_selectedColumn_changed : {
		on : ['selectedColumnChanged'],
		action : function(){
			if(this.selectedColumn != null){
				this.fireEvent('updateColumnSelection', this.selectedColumn);
			}
		}
	},
	when_parser_ready : {
		on : ['parserReadyChanged'],
		action : function(){
			if(this.parserReady){
				this.populateData(this.parserData);
			}
		}
	},
	update_script : {
		on :['markupsChanged','variableStoreChanged'],
		action : function(){
			if(this.parserReady && !this.loadingData)
				this.codeScreen.generateCode ();
		}
	},	
	update_dirty_flag : {
		on : ['sampleChanged'],
		action : function(){
			this.notifyDirty();
		}
	},
	notify_data_changed : {
		on : ['variableStoreChanged'],
		action : function(){
			this.notifyDataChange();
		}
	},
	when_resolution_change : {
		on : ['smallResolutionChanged'],
		action : function(){			
			this.fireEvent('embeddedLayout',  this.smallResolution);
		}
	},

	init : function(){		
		var me = this;
		var colSeparatorStore = Ext.create('Ext.data.Store',{
			fields : ['display','value'],
			proxy : {
				type : 'memory'
			},
			data : [{
				display : this.localize('comma'),
				value : 'comma'
			},{
				display : this.localize('space'),
				value : 'space'
			},{
				display : this.localize('tab'),
				value : 'tab'
			}]
		});
		this.set('columnSeparatorStore',colSeparatorStore);
		this.set('columnSeparator','space');
		var rowSeparatorStore = Ext.create('Ext.data.Store',{
			fields : ['display','value'],
			proxy : {
				type : 'memory'
			},
			data : [{
				display : this.localize('unixLinebreak'),
				value : 'unix'
			},{
				display : this.localize('winLinebreak'),
				value : 'win'
			}]
		});
		this.set('rowSeparatorStore', rowSeparatorStore);
		this.set('rowSeparator','unix');
		this.set('variableColumn', this.getTableVariableColumnConfig());
		this.variableStore.on('datachanged', function(){
			this.fireEvent('variableStoreChanged');
		},this);
		this.variableStore.on('update', function(){
			this.fireEvent('variableStoreChanged');
		},this);
		this.columnDropdownStore.on('datachanged', function(){
			this.fireEvent('columnDropdownStoreChanged');
		},this);
		this.columnDropdownStore.on('update', function(){
			this.fireEvent('columnDropdownStoreChanged');
		},this);	
	},

	//HELPERS
	showCode : function(){
		this.set('activeScreen', 1);
	},
	showMarkup : function(){
		this.set('activeScreen', 0);
	},	
	getCode : function(){
		return this.codeScreen.getCode();
	},	
	isAssessTask: function(flag){
		this.codeScreen.isAssessTask(flag);
	},
	setDefaultParser: function(){
		if(this.parserReady){
			this.clearSample();		
			this.codeScreen.resetCode();
			this.set('activeScreen',0);			
		}
	},
	loadData : function(parserData){
		this.set('parserData', parserData);
		if(this.parserReady){
			this.populateData(parserData);
		}
	},
	populateData : function(parserData){
		if(parserData != null && Object.keys(parserData).length !== 0){
			this.set('loadingData', true);		
			this.updateSample(parserData.sampleOutput || "");
			this.variableStore.removeAll();

			//Populate Markups
			if(parserData.parserConfig){
				try {
				var parserConfig = JSON.parse(parserData.parserConfig);
				}catch (e) {
					clientVM.displayError(this.localize('invalidJSON'));
				}
				if(parserConfig){
					this.set('markups', parserConfig['markups'] );
				
					//Column,row separator
					this.set('rowSeparator', parserConfig.rowSeparator.replace(/\u00a7/g, "").toLowerCase());
					this.set('columnSeparator', parserConfig.columnSeparator.replace(/\u00a7/g, "").toLowerCase());

					//Set up boundary
					var parserOptions = parserConfig['options'];
					this.set('startTextIncluded', parserOptions.boundaryStartIncluded);
					this.set('endTextIncluded', parserOptions.boundaryEndIncluded);
					for(var i = 0; i < this.markups.length; i++){
						var currentMarkup = this.markups[i];
						if(currentMarkup.type == "parseStart" ){
							this.set('parseStart', currentMarkup.boundaryInput);
						}
						else if(currentMarkup.type == "parseEnd" ){
							this.set('parseEnd', currentMarkup.boundaryInput);
						}
					}
					this.set('parseStartOrignalData', this.parseStart);
					this.set('parseEndOrignalData', this.parseEnd);
					//Populate column pickers
					var totalColumn = 0;
					for(var i = 0; i < this.markups.length; i++){
						var currentMarkup = this.markups[i];
						if(currentMarkup.type == "column"){
							totalColumn = Math.max(totalColumn, currentMarkup.column);
						}			
					}
					this.set('totalColumn', totalColumn);

					//Variable Table	
					var variableData = [];				
					for(var i = 0; i < parserConfig.tblVariables.length; i++){
						var currentVariable = parserConfig.tblVariables[i];
						if(currentVariable.flow)
							var containerType = 'FLOW';
						else if (currentVariable.wsdata)
							containerType = 'WSDATA';
						else
							containerType = 'OUTPUT';
						variableData.push({	
							id : currentVariable.id,		
							variable : currentVariable.variable,
							column : currentVariable.columnNum,
							color : this.colorMap[(currentVariable.columnNum - 1) % 6],
							containerType : containerType			
						})
					}
					this.variableStore.loadData(variableData);
				}
			}
			else if(parserData['uscript']){
				//Show Code if this only has code
				this.set('activeScreen', 1);
			}

			//Populate Code
			this.codeScreen.populateCode(parserData['parserAutoGenEnabled'], parserData['uscript']);
		
			this.set('loadingData',false);		
		}
	},
	updateSample : function(data, asPasted){
		if(this.markups.length > 0 && asPasted){
			this.message({
				title : this.localize('overwriteMarkupTitle'),
				msg : this.localize('overwriteMarkupBody'),
				button : Ext.MessageBox.YESNO,
				buttonText : {
					yes : this.localize('confirm'),
					no : this.localize('cancel')
				},
				scope : this,
				fn : function(btn){
					if(btn == 'yes'){					
						this.clearSample();
						this.set('sample', data);
						this.parentVM.updateDefaultTestSample(data);							
					}
				}
			})
		}
		else
		{
			this.set('sample', data);
			this.parentVM.updateDefaultTestSample(data);				
		}
	},	
	getSample : function(){
		return this.sample;
	},
	getParserConfig : function(){
		if(this.markups.length != 0){
			var options = {
				boundaryStartIncluded : this.startTextIncluded,
				boundaryEndIncluded : this.endTextIncluded
			}
			var variables = [];
			for(var i = 0; i < this.variableStore.data.length;i++){
				var variableData = this.variableStore.getAt(i).data;
				variables.push({
					id : variableData.id,
					flow : (variableData.containerType == "FLOW"),
					wsdata : (variableData.containerType == "WSDATA"),
					output : (variableData.containerType == "OUTPUT"),
					variable : variableData.variable,
					columnNum: variableData.column,
				})
			}		
			var	parserConfig = JSON.stringify({ 
				markups : this.markups,
				columnSeparator : "\u00a7" + this.columnSeparator.toUpperCase() + "\u00a7",
				rowSeparator : "\u00a7" + this.rowSeparator.toUpperCase() + "\u00a7",
				tblVariables: variables,
				options : options,
				generatedFromATB : true
			});		
			return parserConfig;
		}
		else
			return null;
	},
	getCode : function(){
		return this.codeScreen.getCode();
	},
	getParameterList : function(){
		var variableArray = [];
		this.variableStore.each(function(entry){
			variableArray.push({
				variableId : entry.get('id'),
				name : entry.get('variable'),
				containerType : entry.get('containerType')
			})
		})
		return variableArray;		
	},
	isAutoGenCode : function(){
		return this.codeScreen.isAutoGenCode();
	},
	isParserReady : function(){
		return this.parserReady;
	},	
	resetParser : function(){
		this.set('selectionInfo',null);
		this.set('selectedColumn',null);		
	},
	notifyDirty : function(){
		if(!this.loadingData)
			this.parentVM.notifyDirty();
	},
	notifyDataChange : function(){
		this.parentVM.notifyDataChange();
	},
	//VIEW EVENT HANDLERS
	applySeparator : function(){
		if(this.variableStore.getCount() > 0){
			this.message({
				title : this.localize('overwriteCaptureTitle'),
				msg : this.localize('overwriteCaptureBody'),
				buttons : Ext.MessageBox.YESNO,
				buttonText : {
					yes : this.localize('yes'),
					no : this.localize('no')
				},
				scope : this,
				fn: function(btn){
					if(btn == "yes"){
						this.clearMarkup();
						this.processApplySeperator();
					}
				}
			})
		}
		else {
			this.processApplySeperator();
		}
		
	},
	processApplySeperator : function(){
		if(this.selectionInfo == null){
			//Select entire reference output
			var selectionInfo = {
				actionId : Ext.data.IdGenerator.get('uuid').generate(),
				from : 0,
				length : this.sample.length
			}
		}
		else
			selectionInfo = this.selectionInfo;

		var newMarkups = this.computeTableData(selectionInfo);
		for(var i = 0; i < this.markups.length; i++){
			var markup = this.markups[i];
			if(markup.type == 'parseStart' || markup.type == 'parseEnd'){
				newMarkups.push(markup);
			}
		}
		this.set('markups', newMarkups);
		this.set('selectionInfo', null);
	},
	clearSample : function(){	
		this.updateSample("");
		this.clearMarkup(true);
		this.set('parseStart', '');
		this.set('parseEnd', '');		
	},	
	clearMarkup : function(clearAll){
		var boundaryMarkups = [];
		for(var i = 0; i < this.markups.length; i++){
			var markup = this.markups[i];
			if(markup.type == "parseStart" || markup.type == "parseEnd"){
				boundaryMarkups.push(markup);
			}
		}
		this.set('markups', clearAll ? [] : boundaryMarkups);
		this.variableStore.removeAll();
		this.newVariableCounter = 1;
		this.set('totalColumn',0);
		this.resetParser();
	},	
	captureVariable : function(){			
		var variableName = "NewVariable" + this.newVariableCounter;
		while(this.variableStore.findExact("variable",variableName) != -1){
			this.newVariableCounter++;
			variableName = "NewVariable" + this.newVariableCounter;
		}
		var existingVariableName = this.variableStore.collect('variable');
		var newMarkup = {
			id : Ext.data.IdGenerator.get('uuid').generate(),
			variable : variableName,
			column : this.selectedColumn,
			color : this.colorMap[(this.selectedColumn - 1) % 6]		
		}		
		this.open({
			mtype : 'MarkupCapture',
			newMarkup : newMarkup,
			existingVariableName : existingVariableName,
			parserType : 'table',
			dumper : {
				dump : this.processCaptureVariable,
				scope : this
			}
		})
	},
	processCaptureVariable : function(newMarkup){
		for(var i = 0; i < this.markups.length; i++){
			var currentMarkup = this.markups[i];
			if(currentMarkup.column == newMarkup.column && currentMarkup.type == "column"){
				currentMarkup.action = 'capture';
			}
		}
		//Add to variable grid
		this.variableStore.add(newMarkup);
		this.fireEvent('updateCaptureColumn', this.markups);
	},
	removeVariable : function(record){
		if(!this.isViewOnly){
			var removedVariableId = record.raw.id;
			var variableStoreIndex = this.variableStore.indexOfId(removedVariableId);
			var columnNo = this.variableStore.getAt(variableStoreIndex).raw.column;		
			for(var i = 0; i < this.markups.length; i++){
				var currentMarkup = this.markups[i];
				if(currentMarkup.column == columnNo && currentMarkup.type == "column"){
					currentMarkup.action = 'default';
				}
			}
			this.fireEvent('updateCaptureColumn', this.markups);
			this.variableStore.removeAt(variableStoreIndex);
		}
	},

	//VIEW COMPONENT VISIBILITY
	applySeparatorIsEnabled$ : function(){
		return this.sample != '' && this.activeScreen == 0;
	},
	captureVariableIsEnabled$ : function(){
		return this.selectedColumn !=null;
	},
	clearSampleIsEnabled$ : function(){
		return this.sample != '' && this.activeScreen == 0;
	},	
	clearMarkupIsEnabled$ : function(){
		return this.sample != '' && this.activeScreen == 0 && this.markups && this.markups.length > 0;
	},
	variableStoreIsEmpty$ : function(){		
		return this.variableStore.getCount() == 0;
	},
	showCodeIsVisible$: function(){		
		return this.activeScreen == 0;
	},
	showMarkupIsVisible$: function(){
		return this.activeScreen == 1;
	},
	controlBorderRegion$ : function(){
		return this.smallResolution ? 'south' : 'east';
	},
	controlHeight$ : function(){
		return this.smallResolution ? 900 : 450;
	},
	controlMargin$ : function(){
		return this.smallResolution ? '10 0 0 0' : '0 0 0 10';
	},
	selectedColumnIsEnabled$ : function(){
		return this.columnDropdownStore.getCount() > 0;
	},
	
	//SELECTION LOGIC	
	processHighlightSelection : function(selection,documentBody, isLinebreak){	
		var selectionInfo = this.getSelectionInfo(selection,documentBody,isLinebreak);		
		this.set('selectionInfo',selectionInfo);
	},	
	processColumnSelection : function(colNo){	
		var colName = this.columnDropdownStore.getAt(colNo - 1);
		this.set('selectedColumn', colName.get('value'));
	},
	//This method will return start position, length of selection as well as unique actionId for that selection.
	getSelectionInfo : function(selection, documentBody, isLinebreak){
		var range = selection.getRangeAt(0);
		var startNode = range.startContainer;
		//Incase selection start from linebreak.
		while(!isLinebreak && startNode.nodeType == 1 && startNode.getAttribute('marker') == "new_line"){
			startNode = startNode.nextSibling;	
		}
		//Get wrapper node of marker instead of content
		if (startNode.parentNode && startNode.parentNode.hasAttribute('marker'))
			startNode = startNode.parentNode
		var endNode = range.endContainer;
		if (endNode.parentNode && endNode.parentNode.hasAttribute('marker'))
			endNode = endNode.parentNode;
		var segments = [];
		var currentSegment = [];
		var selecting = false;

		//Step 1 - Put DOM nodes in correct segments based on selection.
		function segmentDOMNode(node){
			for (var i = 0; i < node.childNodes.length; i++) {
				if (node.childNodes[i] == startNode || node.childNodes[i] == endNode) {
					if (!selecting) {
						segments.push(currentSegment);
						if (startNode == endNode) {
							currentSegment = [node.childNodes[i]]; //which is also start,end
							segments.push(currentSegment);
							currentSegment = [];
							continue;
						}
						currentSegment = [];
						selecting = true;
					} else {
						currentSegment.push(node.childNodes[i]); //which is the logical 'end'(it can be start or end coz the selection direction may be reversed)
						segments.push(currentSegment);
						currentSegment = [];
						selecting = false;
						continue;
					}
				}
				if (node.childNodes[i].nodeType == 3)
					currentSegment.push(node.childNodes[i]);
				else if (node.childNodes[i].hasAttribute('marker'))
					currentSegment.push(node.childNodes[i]);
				else if (!node.childNodes[i].getAttribute('exclude'))
					segmentDOMNode(node.childNodes[i]);
			}
		}
		segmentDOMNode(documentBody);
		segments.push(currentSegment);
		
		//Step 2 - Extract raw text from each segment
		var pre = this.extractTextFromSegment(segments[0]);
		var selection = this.extractTextFromSegment(segments[1]);
		var post = this.extractTextFromSegment(segments[2]);

		//Step 3 - Extract partial text from selection segment.
		if (segments[1].length == 1) {
			var prePartialResult = this.parsePartialStartNode(segments[1][0], range.startOffset);
			var postPartialResult = this.parsePartialEndNode(segments[1][0], range.endOffset);
			pre = pre + prePartialResult.pre
			selection = (segments[1][0].nodeType != 3) ? prePartialResult.selected : this.getTextFromNode(segments[1][0]).substring(range.startOffset, range.endOffset);
			post = postPartialResult.post + post;
		} else {
			var partialStart = segments[1][0];
			var partialEnd = segments[1][segments[1].length - 1];
			var prePartialResult = this.parsePartialStartNode(partialStart, range.startOffset);
			var postPartialResult = this.parsePartialEndNode(partialEnd, range.endOffset);
			var midSelected = this.extractTextFromSegment(segments[1].slice(1, segments[1].length - 1));
			pre = pre + prePartialResult.pre
			selection = prePartialResult.selected + midSelected + postPartialResult.selected;
			post = postPartialResult.post + post;
		}

		//Step 4 - return info for this selection.
		if(isLinebreak){
			return {
				from: pre.length,
				length: selection.length + 2,
				actionId: Ext.data.IdGenerator.get('uuid').generate()
			}
		}
		return {
			from: pre.length,
			length: selection.length,
			actionId: Ext.data.IdGenerator.get('uuid').generate()
		};
	},
	extractTextFromSegment : function(segment){
		var str = '';
		for (var i = 0; i < segment.length; i++) {
			if (segment[i].nodeType == 3)
				str += this.getTextFromNode(segment[i]);
			else {
				if (segment[i].getAttribute('marker') == "new_line")
					str += (Ext.is.Windows ? '\r\n' : '\n');
				else if (segment[i].getAttribute('marker') == "space")
					str += ' ';
			}
		}	
		return str;
	},
	getTextFromNode: function(node) {
		if(node.parentNode && node.parentNode.getAttribute('type') == 'endOfLine')
			return '';
		var textPropName = node.textContent === undefined ? 'nodeValue' : 'textContent';
		return node[textPropName];
	},
	parsePartialStartNode: function(node, startOffset) {
		return {
			pre: this.getTextFromNode(node).substring(0, startOffset),
			selected: this.getTextFromNode(node).substring(startOffset, this.getTextFromNode(node).length)
		};
	},
	parsePartialEndNode: function(node, endOffset) {
		return {
			post: this.getTextFromNode(node).substring(endOffset, this.getTextFromNode(node).length),
			selected: this.getTextFromNode(node).substring(0, endOffset)
		};
	},	

	//APPLY SEPARATOR LOGIC	
	getRegexForSeparator : function(separator){
		return new RegExp(this.regExpForSeparatorMap[separator], 'g');
	},
	computeTableData : function(selectionInfo){
		var processedTable = this.extractTableFromSample(selectionInfo);
		var rowSeparatorRegEx = this.getRegexForSeparator(this.rowSeparator);
		var offset = selectionInfo.from;
		var markups = [];
		var colors = [];
		var totalColumn = 0;
		for (var i = 0; i < processedTable.length; i++) {
			var row = processedTable[i];
			var col = 1;
			if (!row[0])
				col--;
			for (var j = 0; j < row.length; j++) {				
				markups.push({
					from: offset,
					length: row[j].length,
					type: (j % 2 == 0 ? 'column' : 'delimiter'),					
					actionId: Ext.data.IdGenerator.get('uuid').generate(),
					column: col,
					expand: true,
					action: "type",				
					color: (j % 2 == 0 ? this.colorMap[(col - 1) % 6] : null)					
				});
				col += (j % 2 == 0 ? 1 : 0);
				offset += row[j].length;
			}
			//Dont add end of line for last row.
			if(i != processedTable.length - 1){
				markups.push({
					from: offset,
					length: 2,
					action: "type",			
					actionId: Ext.data.IdGenerator.get('uuid').generate(),
					column: col,
					expand: true,
					type : 'endOfLine'
				})
				offset+= 2;
			}
			totalColumn = Math.max(col - 1,totalColumn);
		}
		this.set('totalColumn', totalColumn);
		return markups;
	},
	extractTableFromSample : function(selectionInfo){
		var table = this.sample.substring(selectionInfo.from, selectionInfo.from + selectionInfo.length);
		var rows = table.split(this.getRegexForSeparator(this.rowSeparator));
		var splittedRows = [];
		var matchedColSep = [];
		var columnRegex = this.getRegexForSeparator(this.columnSeparator);
		Ext.each(rows, function(row) {
			splittedRows.push(row.split(columnRegex));
			matchedColSep.push(row.match(columnRegex));
		}, this)
		var spaceHead = false;
		var spaceTail = false;
		var processed = [];
		for (var i = 0; i < splittedRows.length; i++) {
			var r = [];
			for (var j = 0; j < splittedRows[i].length - 1; j++) {
				r.push(splittedRows[i][j]);
				r.push(matchedColSep[i][j]);
			}
			r.push(splittedRows[i][splittedRows[i].length - 1]);
			processed.push(r);
			if (r[0] == '')
				spaceHead = true;
			if (r[r.length - 1] == '')
				spaceTail = true;
		}
		Ext.each(processed, function(row) {
			if (row[0] != '' && spaceHead)
				Ext.Array.insert(row, 0, ['', '']);
		});
		return processed;
	}	
});