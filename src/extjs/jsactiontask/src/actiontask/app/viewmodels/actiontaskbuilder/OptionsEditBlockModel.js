Ext.define('RS.actiontaskbuilder.OptionsEditBlockModel', (function () {
	var defaultOptions = [{
		uname: 'QUEUE_NAME',
		udescription: 'Name of the JMS Queue to send the ActionTask request'
	}, {
		uname: 'EXECPATH',
		udescription: 'OS execution path location'
	}, {
		uname: 'CMDLINE_EXCLUDE',
		udescription: 'Exclude the command line from being displayed in the result RAW output'
	}, {
		uname: 'RESULTTYPE',
		udescription: 'Method for retrieving result value e.g. FILE, STDOUTERR (default)'
	}, {
		uname: 'RESULTFILE',
		udescription: 'Filename of the result file to be retrieved (RESULTTYPE must be set to FILE). Set RESULTFILE to "$TMPFILE" to generate the filename dynamically at execution time'
	}, {
		uname: 'RESULTFILE_REMOVE',
		udescription: 'Remove resultfile on completion (default: false)'
	}, {
		uname: 'RESULTFILE_WAIT',
		udescription: 'Wait the specified number of seconds before retrieving RESULTFILE'
	}, {
		uname: 'CLASSPATH',
		udescription: 'Additonal Java classpath'
	}, {
		uname: 'JAVAOPTIONS',
		udescription: 'Additonal Java command-line options'
	}, {
		uname: 'TIMEOUT_DESTROY_THREAD',
		udescription: 'Force destroy thread (warning: may leak resources, etc)'
	}, {
		uname: 'TMPFILE_PREFIX',
		udescription: 'Prefix string prepended to tmpfile (default: blank)'
	}, {
		uname: 'TMPFILE_POSTFIX',
		udescription: 'Postfix string appended to tmpfile (default: .tmp)'
	}, {
		uname: 'TMPFILE_REMOVE',
		udescription: 'Remove tmpfile on completion (default: true)'
	}, {
		uname: 'INPUTFILE_POSTFIX',
		udescription: 'Filename extension to use for the temporary INPUTFILE (default: .in)'
	}, {
		uname: 'INPUTFILE_ENCODING',
		udescription: 'file encoding for input file to be generated'
	}, {
		uname: 'BLANKVALUES',
		udescription: 'Replace matching values with *****. Use $CONFIGname for Registration properties and $TARGETname for Proxy Target properties. Separate multiple values with a comma'
	}, {
		uname: 'TAILLENGTH',
		udescription: 'Number of lines to retrieve from the end of the file'
	}, {
		uname: 'EXTERNAL_TYPE',
		udescription: ''
	}, {
		uname: 'METRIC_GROUP',
		udescription: 'Metric group'
	}, {
		uname: 'METRIC_ID',
		udescription: 'Metric ID'
	}, {
		uname: 'METRIC_UNIT',
		udescription: 'Metric unit'
	}];

	function getRemainingOptions(options) {
		var remainingOptions = defaultOptions.slice();

		for (var i = 0; i < options.length; i++) {
			for (var j = 0; j < remainingOptions.length; j++) {
				if (options[i].uname === remainingOptions[j].uname) {
					remainingOptions.splice(j, 1);
					break;
				}
			}
		}

		return remainingOptions;
	}

	function getDefaultOptionByOptionName(optionName) {
		var option = null;

		for (var i = 0; i < defaultOptions.length; i++) {
			if (defaultOptions[i].uname === optionName) {
				option = defaultOptions[i];
				break;
			}
		}

		return option;
	}

	return {
		extend: 'RS.common.blocks.BlockModel',

		modelConfig: {
			mixins: ['QuickAccessHelpers'],
			blockModel: null,
			blockModelClass: 'General',
			hidden: true,
			collapsed: true,
			editSubBlock: true,
			stopPropagation: false,
			title: '',
			header: {
		 		xtype: 'toolbar',
		    	margin: 0,
		    	padding: 0,
		    	items: []
			},			
			optionsStore: null,
			allowedOptionsForGridRow: null,
			detail: {
				mtype: 'OptionEditDetail'
			},
			savingNewTask: false,
			gridIsEmpty: false,
			itemsExhausted: false,
			
			postInit: function (blockModel) {
				this.blockModel = blockModel;
				this.set('allowedOptionsForGridRow', new RS.common.data.FlatStore({
					fields: ['uname', 'udescription']
				}));
				this.set('optionsStore', new RS.common.data.FlatStore({
					fields: ['uname', 'uvalue', 'udescription'],
					listeners: {
						datachanged: function () {
							this.notifyDirty();
						}.bind(this),
						update: function (record, operation) {
							var modified = operation.modified;

							if (modified.hasOwnProperty('uname') && modified.uname !== operation.data.uname) {
								this.detail.removeOption(operation.data.uname);
								this.detail.addOption(modified.uname);
							}

							this.notifyDirty();
						}.bind(this)						
					}
				}));
				this.header.items = [{
		        	xtype: 'component',
		        	padding: '0 0 0 15',
		        	cls: 'x-header-text x-panel-header-text-container-default',
		        	html: this.localize('generalTitle'),
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
							}, this);
						}.bind(this)
					}
		        }, {
		        	xtype: 'container',
		        	autoEl: 'ol',
		        	cls: 'breadcrumb flat',
		        	margin: 0,
		        	padding: 0,
		        	items: [{
			    		xtype: 'component',
			    		autoEl: 'li',
			    		html: this.localize('generalPropertiesEditTitle'),
						listeners: {
							afterrender: function (c) {
								c.getEl().on('click', function () {
									this.stopPropagation = true;
									this.jumpTo('GeneralEdit', this.parentVM.generalEdit.gluModel);
					            }, this);
							}.bind(this)
			    		}			    		
			    	}, {
			    		xtype: 'component',
			    		autoEl: 'li',
						html: this.localize('rolesEditTitle'),
						listeners: {
							afterrender: function (c) {
								c.getEl().on('click', function () {
									this.stopPropagation = true;
									this.jumpTo('RolesEdit', this.parentVM.rolesEdit.gluModel);
					            }, this);
							}.bind(this)
			    		}			   
			    	}, {
			    		xtype: 'component',
			    		autoEl: 'li',
			    		cls: 'active',
			    		html: this.localize('optionsEditTitle'),
						listeners: {
							afterrender: function (c) {
								c.getEl().on('click', function () {
									this.stopPropagation = true;
								}, this);
							}.bind(this)
						}
			        }]
		        }, '->', {
					xtype: 'button',
					tooltip: this.localize('resizeFullscreen'),
					iconCls: 'rs-block-button icon-resize-full',
					isResizeMaxBtn: true,
					margin: '0 5',
					text: '',
					style: {
						opacity: 0.75
					},
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['GENERAL']);
							}, this);
						}.bind(this)
					}
				}, {
					xtype: 'button',
					tooltip: this.localize('resizeNormalscreen'),
					iconCls: 'rs-block-button icon-resize-small',
					hidden: true,
					isResizeNormalBtn: true,
					margin: '0 20',
					text: '',
					style: {
						opacity: 0.75
					},
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.parentVM.resizeNormalSection(this);
							}, this);
						}.bind(this)
					}
				}, {
					xtype: 'button',
					iconCls: 'rs-block-button icon-chevron-sign-up',
					isCollapseBtn: true,
					margin: '0 5',
					text: '',
					style: {
						opacity: 0.75
					},
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.set('collapsed', true);
							}, this);
						}.bind(this)
					}
				}];				
			},

			notifyDirty: function() {
				this.blockModel.notifyDirty();
			},

			selectOption: function (args) {
				var selection = args[0].ownerCt.grid.getSelectionModel().getSelection();
				var originalRecord = selection[0];
				var option = args[1][0].data;

				if (originalRecord.data.uname !== option.uname) {
					originalRecord.set('udescription', option.udescription);
				}
			},

			jumpTo: function(nextBlockName, nextSection){
				this.parentVM.generalEditSectionExpanded = !this.collapsed;
				this.parentVM.generalEditSectionMaximized = this.isSectionMaxSize;
				this.parentVM.collapseAndHideSection(this);
				this.parentVM.showAndExpandThisSection(nextSection);
			}, 

			when_hidden_changed: {
				on: ['hiddenChanged'],
				action: function() {
					if (!this.hidden) {
						setTimeout(function() {
							if (this.parentVM.generalEditSectionExpanded) {
								this.parentVM.generalEditSectionExpanded = null;
								this.set('collapsed', false);
							}
							if (this.parentVM.generalEditSectionMaximized) {
								this.parentVM.generalEditSectionMaximized = null;
								this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['GENERAL']);
							}
						}.bind(this), 10);
					}
				}
			},

			closeFlag: false,
			close: function () {
				if (this.isSectionMaxSize) {
					this.parentVM.resizeNormalSection(this);
				}
				this.closeFlag = true;
				this.set('collapsed', true);
			},
	
			add: function (option) {
				this.optionsStore.add(option);
				var count = this.optionsStore.count();
				this.set('gridIsEmpty', count === 0);
				this.hideAddFormOnExhaust();
			},

			addInputParamToOptionBlankValue: function(paramName) {
				var idx = this.optionsStore.findExact('uname', 'BLANKVALUES');
				if (idx != -1) {
					var record = this.optionsStore.getAt(idx);
					var value = record.get('uvalue');
					var valueList = value.split(',');
					valueList.push('$INPUT{'+paramName+'}');
					record.set('uvalue', valueList.join());
				} else {
					this.detail.addNewOptionBlankValues(paramName);
				}
			},

			removeInputParamFromOptionBlankValue: function(paramName) {
				var idx = this.optionsStore.findExact('uname', 'BLANKVALUES');
				if (idx != -1) {
					var record = this.optionsStore.getAt(idx);
					var value = record.get('uvalue');
					var valueList = value.split(',');
					var i = valueList.indexOf('$INPUT{'+paramName+'}');
					if (i != -1) {
						valueList.splice(i, 1);
						// remove BLANKVALUES option if no values after removing last entry
						if (!valueList.length) {
							this.optionsStore.remove(record);
						} else {
							record.set('uvalue', valueList.join());
						}
					}
				}
			},

			updateAllowedOptions: function(context) {
				var options = this.optionsStore.getData();
				var remainingOptions = getRemainingOptions(options);
				remainingOptions.push(context.value);
				this.allowedOptionsForGridRow.loadData(remainingOptions);
			},

			addOptionToDetail: function (record) {
				this.detail.addOption(record.data.uname);
				this.set('itemsExhausted', false);
				var count = this.optionsStore.count();
				this.set('gridIsEmpty', count === 0);				
			},

			hideAddFormOnExhaust: function() {
				this.set('itemsExhausted', this.optionsStore.count() === defaultOptions.length);
			}
		},

		disableForm: function () {
			this.set('savingNewTask', true);
		},

		enableForm: function () {
			this.set('savingNewTask', false);
		},		

		getDataPaths: function () {
			return [{
				path:'data.resolveActionInvoc.resolveActionInvocOptions',
				filter: function (o) {
					return o.uname !== 'INPUTFILE';
				}
			}];
		},

		getData: function () {
			return [this.gluModel.optionsStore.getData()];
		},

		load: function (options) {
			var g = this.gluModel;
			g.optionsStore.loadData(options);
			var count = g.optionsStore.count();
			this.set('gridIsEmpty', count === 0);
			var remainingOptions = getRemainingOptions(options);
			g.allowedOptionsForGridRow.loadData(remainingOptions);
			g.detail.setOptions(remainingOptions);
			g.hideAddFormOnExhaust();
		}
	};
})());
