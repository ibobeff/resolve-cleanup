glu.defModel('RS.actiontaskbuilder.GeneralReference', {

	ufullname: 'New',

	activate: function(screen, params) {
		this.set('id', params ? params.id || '' : '');
		this.referencesStore.removeAll();
		this.show();
	},

	referenceTitle$: function() {
		return this.localize('referenceTitle', this.ufullname);
	},

	show: function() {
		 this.ajax({
			url: '/resolve/service/actiontask/get',
			method: 'post',
			params: {
				id: this.id,
				name: ''
			},
			scope: this,
			success: function (resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(respData.message);
				}
				else {
					this.referencesStore.removeAll();
					if (respData.data.referencedIn) {
						this.referencesStore.add(respData.data.referencedIn);
					}
					this.set('ufullname', respData.data.ufullName);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	referencesStore: {
		mtype: 'store',
		fields: ['name', 'refInContent', 'refInMain', 'refInAbort'],
		proxy: {
			type: 'memory'
		}
	},

	editReference: function(docName) {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			params: {
				name: docName
			},
			target: '_blank'
		})
	}
});
