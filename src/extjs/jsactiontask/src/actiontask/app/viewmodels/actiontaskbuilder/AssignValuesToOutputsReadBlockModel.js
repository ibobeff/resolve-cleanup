Ext.define('RS.actiontaskbuilder.AssignValuesToOutputsReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'AssignValuesToOutputs',
		classList: 'block-model',
		hidden: false,	
		title: '',
		collapsed: true,
		header: {
			titlePosition: 0,
            defaults: {
                margin: 0
            },					
			items: []
		},
		valuesStore: null,
		hasValues: false,

		postInit: function (blockModel) {
			this.blockModel = blockModel;
			this.set('valuesStore', new RS.common.data.FlatStore({
				fields: ['outputType', 'outputName', 'sourceType', 'sourceName']
			}));		
		},

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden && this.parentVM.embed) {
					var assignmentsData = this.parentVM.assignValuesToOutputsEdit.getData();
					this.parentVM.assignValuesToOutputsRead.load(assignmentsData[0]);
				}
			}
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.outputMappings'];
	},

	getData: function () {
		return [this.gluModel.valuesStore.getData()];
	},

	load: function (outputMappings) {
		this.gluModel.valuesStore.loadData(outputMappings);
		var hasValues = outputMappings.length > 0,
			classes = ['block-model'];

		this.set('hasValues', hasValues)
			.set('classList', classes);
	}
});
