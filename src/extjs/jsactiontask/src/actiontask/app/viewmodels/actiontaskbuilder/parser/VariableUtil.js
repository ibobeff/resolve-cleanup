glu.defModel('RS.actiontaskbuilder.VariableUtil',{
	colorPickerStore : {
		mtype :'store',
		proxy : {
			type : 'memory'
		},
		fields: [
		{	
			name: 'value',
			convert: function(v, record) {
				return record.raw;
			}
		}],
		data: ['#50C6E8','#96BB35','#F3955D','#BD6161','#276888','black']
	},
	containerTypeStore : {
		mtype : 'store',
		proxy : {
			type : 'memory'
		},
		fields : [{
			name : 'value',
			convert : function(v, record){
				return record.raw;
			}
		}],
		data: ['WSDATA','FLOW','OUTPUT']
	},
	patternTypeStore : null,
	textVariableColumnConfig : null,
	tableVariableColumnConfig : null,
	xmlVariableColumnConfig : null,
	initMixin : function(){
		var me = this;
		this.set('patternTypeStore', Ext.create('Ext.data.Store',
		{	
			proxy : {
				type :'memory'
			},
			fields: [{
				name: 'display',
				convert: function(v, record) {
					if (record.raw == '-' || record.raw == '-<>')
						return '';
					return me.localize(record.raw);
				}
			}, {
				name: 'value',
				convert: function(v, record) {
					return record.raw;
				}
			}],
			data: [
				'ignoreNotGreedy',
				'ignoreRemainder',
				'-',
				'spaceOrTab',
				'whitespace',
				'-',
				'letters',
				'lettersAndNumbers',
				'notLettersOrNumbers',
				'lowerCase',
				'upperCase',
				'-',
				'number',
				'notNumber',
				'-',
				'IPv4',
				'MAC',
				'decimal'
			]		
		}));

		this.set('textVariableColumnConfig', [
		{
			header: 'Name',
			dataIndex: 'variable',
			editor: 'textfield',
			flex: 3
		},{
			header : 'Pattern',
			dataIndex : 'type',
			align : 'center',
			width : 200,
			renderer : function(value){
				return me.localize(value);
			},
			editor : 
			{
				xtype : 'combo',	
				store : this.patternTypeStore,
				editable : false,				
				displayField : 'display',
				valueField : 'value',
				queryMode : 'local',
				tpl: Ext.create('Ext.XTemplate',
					'<tpl for=".">',
					'<div class="x-boundlist-item">{display}<tpl if="value==\'-\'"><hr/></tpl></div>',
					'</tpl>'
				)
			}				
		},{
			header: 'Color',
			dataIndex: 'color',
			align : 'center',
			renderer: function(value, meta) {
				meta.style = 'background-color:' + value;
			},
			width : 200,
			editor: {
				xtype: 'combo',				
				editable : false,
				queryMode: 'local',					
				valueField: 'value',
				store: this.colorPickerStore,
				tpl: Ext.create('Ext.XTemplate',
					'<tpl for=".">',
					'<div class="x-boundlist-item capture-color-picker" style="background:{value}"></div>',
					'</tpl>'
				),
				listeners: {
					select:function(combo, records){
				        var value = this.getValue();
				        this.setFieldStyle('background:'+ value);
				    }
				}
			}
		},{
			header : 'Container',
			dataIndex : 'containerType',
			align : 'center',
			width : 200,
			editor: {
				xtype: 'combo',				
				editable : false,
				queryMode: 'local',					
				valueField: 'value',
				displayField: 'value',
				store: this.containerTypeStore				
			}
		},{
			xtype: 'actioncolumn',		
			hideable: false,
			sortable: true,
			align: 'center',			
			width: 45,			
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record ) {
					this.up('grid').fireEvent('removeVariable', view, record);
				}
			}]
		}]);

		this.set('tableVariableColumnConfig', [{
			header: 'Name',
			dataIndex: 'variable',
			editor : 'textfield',
			flex: 3
		},{
			header : 'Column Number',
			dataIndex : 'column',
			align : 'center',				
			width : 200
		},{
			header: 'Color',
			dataIndex: 'color',
			align : 'center',
			renderer: function(value, meta) {
				meta.style = 'background-color:' + value;
			},
			width : 200
		},{
			header : 'Container',
			dataIndex : 'containerType',
			align : 'center',
			width : 200,
			editor: {
				xtype: 'combo',				
				editable : false,
				queryMode: 'local',					
				valueField: 'value',
				displayField: 'value',
				store: this.containerTypeStore				
			}
		},{
			xtype: 'actioncolumn',		
			hideable: false,
			sortable: true,
			align: 'center',			
			width: 45,		
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record ) {
					this.up('grid').fireEvent('removeVariable', view, record);
				}
			}]
		}]);
		
		this.set('xmlVariableColumnConfig',[{
			header: 'Name',
			dataIndex: 'variable',
			editor : {
				xtype : 'textfield'
			},
			flex: 1
		},{
			header : 'XPath',
			dataIndex : 'xpath',
			editor : 'textfield',
			flex: 5
		},{
			header : 'Container',
			dataIndex : 'containerType',
			align : 'center',
			width : 200,
			editor: {
				xtype: 'combo',				
				editable : false,
				queryMode: 'local',					
				valueField: 'value',
				displayField: 'value',
				store: this.containerTypeStore				
			}
		},{
			xtype: 'actioncolumn',		
			hideable: false,
			sortable: true,
			align: 'center',			
			width: 45,
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record ) {
					this.up('grid').fireEvent('removeVariable', view, record);
				}
			}]
		}]);
	},
	convertToRealType : function(autoType){
		var realType = autoType.substring(2, autoType.length);
		if(realType == "MAC" || realType == "IPv4")
			return realType
		else
			return realType.charAt(0).toLowerCase() + realType.slice(1);
	},
	getRandomColor : function(){
		var totalColor = this.colorPickerStore.data.length;
		var radomColorIndex = Math.floor(Math.random() * (totalColor - 1));
		return this.colorPickerStore.getAt(radomColorIndex).raw;
	},
	getTextVariableColumnConfig : function(){
		return this.textVariableColumnConfig;
	},
	getTableVariableColumnConfig : function(){
		return this.tableVariableColumnConfig;
	},
	getXmlVariableColumnConfig : function(){
		return this.xmlVariableColumnConfig;
	},
	displayDuplicateNameMsg : function(){
		clientVM.message(this.localize('invalidVariableTitle'),this.localize('duplicateVariableMsg'));		
	},
	displayInvalidCharacter : function(){
		clientVM.message(this.localize('invalidVariableTitle'),this.localize('validKeyCharacters'));		
	}
});