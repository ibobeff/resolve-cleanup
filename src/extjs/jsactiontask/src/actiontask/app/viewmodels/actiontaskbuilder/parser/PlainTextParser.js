glu.defModel("RS.actiontaskbuilder.PlainTextParser",{
	mixins : ['VariableUtil','BoundaryUtil'],		
	sample :'',	
	activeScreen : 0,
	smallResolution : false,
	newVariableCounter : 1,
	markups : [],
	block : false,
	parserData : null,
	parserReady : false,
	loadingData : false,
	variableColumn : null,
	selectedMarkup : null,
	currentSelection : null,
	parseStart : '',
	parseStartOrignalData : '',
	startTextIncluded : true,
	parseEnd : '',
	parseEndOrignalData : '',
	endTextIncluded : true,
	selectionIsInvalid : false,	
	codeScreen : {	
		mtype : 'ParserCode',
		parserType : 'text'
	},
	patternProcessor : {
		mtype : 'PatternProcessor'
	},	
	variableStore : {
		mtype : 'store',
		fields : ['variable','type','color','containerType']		
	},
	when_parser_ready : {
		on : ['parserReadyChanged'],
		action : function(){
			if(this.parserReady){
				this.populateData(this.parserData);
			}
		}
	},
	update_script : {
		on :['markupsChanged','blockChanged'],
		action : function(){
			if(this.parserReady && !this.loadingData)
				this.codeScreen.generateCode();
		}
	},
	update_dirty_flag : {
		on : ['sampleChanged'],
		action : function(){
			this.notifyDirty();
		}
	},
	notify_data_changed : {
		on : ['variableStoreChanged'],
		action : function(){
			this.notifyDataChange();
		}
	},
	getParameterList : function(){
		var variableArray = [];
		this.variableStore.each(function(entry){
			variableArray.push({
				name : entry.get('variable'),
				containerType : entry.get('containerType')
			})
		})
		return variableArray;
	},
	when_resolution_change : {
		on : ['smallResolutionChanged'],
		action : function(){			
			this.fireEvent('embeddedLayout',  this.smallResolution);
		}
	},
	
	init : function(){
		this.set('variableColumn', this.getTextVariableColumnConfig());	
		this.variableStore.on('datachanged', function(){
			this.fireEvent('variableStoreChanged');
		},this);	
		this.variableStore.on('update', function(){
			this.fireEvent('variableStoreChanged');
		},this);
		this.patternProcessor.on('patternSelectionChanged', function(patternName){
			this.updatePatternSelection(patternName);
		},this);
	},

	//HELPERS
	showCode : function(){
		this.set('activeScreen', 1);
	},
	showMarkup : function(){
		this.set('activeScreen', 0);
	},	
	isAssessTask: function(flag){
		this.codeScreen.isAssessTask(flag);
	},
	setDefaultParser: function(){
		if(this.parserReady){
			this.clearSample();			
			this.codeScreen.resetCode();
			this.set('activeScreen',0);
		}
	},
	loadData : function(parserData){		
		this.set('parserData', parserData);
		if(this.parserReady){
			this.populateData(parserData);
		}
	},
	populateData : function(parserData){
		if(parserData != null && Object.keys(parserData).length !== 0){
			this.set('loadingData', true);		
			this.variableStore.removeAll();
			
			//Set up sample output
			this.updateSample(parserData.sampleOutput || "");		

			//Set up markup
			if(parserData.parserConfig)
			{
				//Set up markups
				try {
					var parserConfig = JSON.parse(parserData.parserConfig);
				}catch (e) {
					clientVM.displayError(this.localize('invalidJSON'));
				}
				
				if(parserConfig){
					this.set('markups', parserConfig['markups']);

					//Set up boundary
					var parserOptions = parserConfig['options'];
					this.set('startTextIncluded', parserOptions.boundaryStartIncluded);
					this.set('parseStart','');
					this.set('endTextIncluded', parserOptions.boundaryEndIncluded);
					this.set('parseEnd', '');
					for(var i = 0; i < this.markups.length; i++){
						var currentMarkup = this.markups[i];
						if(currentMarkup.type == "parseStart" ){
							this.set('parseStart', currentMarkup.boundaryInput);
						}
						else if(currentMarkup.type == "parseEnd" ){
							this.set('parseEnd', currentMarkup.boundaryInput);
						}
					}
					//Save original boundary
					this.set('parseStartOrignalData', this.parseStart);
					this.set('parseEndOrignalData', this.parseEnd);
					this.set('block', parserOptions.repeat);

					//Variable Table
					var variableData = [];			
					for(var i = 0; i < this.markups.length; i++){
						var currentMarkup = this.markups[i];					
						if(currentMarkup.action == "capture" ){
							if(currentMarkup.flow)
								var containerType = 'FLOW';
							else if (currentMarkup.wsdata)
								containerType = 'WSDATA';
							else
								containerType = 'OUTPUT';
							variableData.push({
								id : currentMarkup.actionId,
								actionId : currentMarkup.actionId,
								variable : currentMarkup.variable,
								type : currentMarkup.type,
								color : currentMarkup.color,
								containerType : containerType
							})
						}
					}
					this.variableStore.loadData(variableData);			
				}
			}
			//Show Code if this only has code
			else if (parserData['uscript']){				
				this.set('activeScreen', 1);
			}

			//Populate Code
			this.codeScreen.populateCode(parserData['parserAutoGenEnabled'], parserData['uscript']);

			this.set('loadingData', false);			
		}
	},
	updateSample : function(data, asPasted){
		if(this.markups.length > 0 && asPasted){
			this.message({
				title : this.localize('overwriteMarkupTitle'),
				msg : this.localize('overwriteMarkupBody'),
				button : Ext.MessageBox.YESNO,
				buttonText : {
					yes : this.localize('confirm'),
					no : this.localize('cancel')
				},
				scope : this,
				fn : function(btn){
					if(btn == 'yes'){						
						this.clearSample();					
						this.set('sample', data);
						this.parentVM.updateDefaultTestSample(data);				
					}
				}
			})
		}
		else
		{
			this.set('sample', data);
			this.parentVM.updateDefaultTestSample(data);
		}
	},
	getMarkups : function(){
		return this.markups;
	},
	getSample : function(){
		return this.sample;
	},
	getParserConfig : function(){
		if(this.markups.length != 0){
			var options = {
				boundaryStartIncluded : this.startTextIncluded,
				boundaryEndIncluded : this.endTextIncluded,
				repeat : this.block
			}
			var	parserConfig = JSON.stringify({ 
				markups : this.markups,
				options : options,
				generatedFromATB : true
			});
			return parserConfig;
		}
		else
			return null;
		
	},
	getCode : function(){
		return this.codeScreen.getCode();
	},
	getParameterList : function(){
		var variableArray = [];
		this.variableStore.each(function(entry){
			variableArray.push({
				variableId : entry.get('id'),
				name : entry.get('variable'),
				containerType : entry.get('containerType')
			})
		})
		return variableArray;		
	},
	isAutoGenCode : function(){
		return this.codeScreen.isAutoGenCode();
	},
	isParserReady : function(){
		return this.parserReady;
	},
	resetParser : function(){
		this.set('selectionIsInvalid', false);
		this.set('currentSelection',null);	
		this.set('selectedMarkup',null);	
		this.patternProcessor.hidePattern(true);
	},	
	notifyDirty : function(){
		if(!this.loadingData)
			this.parentVM.notifyDirty();
	},
	notifyDataChange : function(){
		this.parentVM.notifyDataChange();
	},		
	//VIEW EVENT HANDLERS
	clearSample : function(){	
		this.updateSample("");
		this.clearMarkup(true);
		this.set('parseStart', '');
		this.set('parseEnd', '');			
	},	
	clearMarkup : function(clearAll){
		var boundaryMarkups = [];
		for(var i = 0; i < this.markups.length; i++){
			var markup = this.markups[i];
			if(markup.type == "parseStart" || markup.type == "parseEnd"){
				boundaryMarkups.push(markup);
			}
		}		
		this.set('markups', clearAll ? [] : boundaryMarkups);
		this.variableStore.removeAll();
		this.newVariableCounter = 1;
		this.resetParser();	
	},	
	updatePatternSelection : function(patternName){
		if(this.selectedMarkup != null){
			this.updateMarkup(patternName);
			this.fireEvent('selectedMarkupChanged');
		}
		else
			this.addMarkUp(patternName);
	},
	addMarkUp : function(markupName){
		var currentSelection = this.currentSelection;
		currentSelection.type = markupName;
		currentSelection.action = markupName == 'literal' ? 'literal' : 'type';
		var markups = this.markups.slice(0);
		markups.push(currentSelection);

		//This will trigger setMarkups method on the view and in turn update parser's view.
		this.set('markups', markups);
		
		this.fireEvent('reselectMarkup', currentSelection.actionId);
	},
	updateMarkup : function(patternName){
		var selectedMarkupActionId = this.selectedMarkup['actionId'];
		var markups = this.markups;
		var targetMarkup = null;
		for(var i = 0; i < markups.length; i++){
			if(markups[i].actionId == selectedMarkupActionId){
				targetMarkup = markups[i];
				break;
			}
		}
		if(targetMarkup != null){
			targetMarkup['type'] = patternName;	
			targetMarkup['action'] = patternName == 'literal' ? 'literal' : 'type';
			this.set('selectedMarkup', targetMarkup);		
			this.fireEvent('markupsChanged',this.markups);
		}
	},
	removeMarkup : function(){
		var selectedMarkup = this.selectedMarkup;
		var targetMarkupIndex = null;
		var markups = this.markups.slice(0);
		for(var i = 0; i < markups.length; i++){
			if(markups[i].actionId == selectedMarkup.actionId){
				targetMarkupIndex = i;
				break;
			}
		}
		if(targetMarkupIndex != null){
			markups.splice(targetMarkupIndex,1);
			//This will trigger setMarkups method on the view and in turn update parser's view.
			this.set('markups', markups);
			//Clear pattern table after selection.
			this.set('selectedMarkup', null);
			this.patternProcessor.hidePattern(true);
		}	
	},	
	captureMarkup : function(){
		//Already markup -> just use the current type and capture
		if(this.selectedMarkup && this.selectedMarkup.type)
			var selectedMarkup = this.selectedMarkup;
		else {
			//Element has no type (from highlight text). Pick most logical type for this
			var selectedMarkup = this.currentSelection;
			selectedMarkup['type'] = this.patternProcessor.getAutoSelectType(selectedMarkup['autoType']);
		}
		var randomColor = this.getRandomColor();
		var variableName = "NewVariable" + this.newVariableCounter;
		while(this.variableStore.findExact("variable",variableName) != -1){
			this.newVariableCounter++;
			variableName = "NewVariable" + this.newVariableCounter;
		} 
		var existingVariableName = this.variableStore.collect('variable');
		//SET DEFAULT TO OUTPUT.
		var newMarkup = {
			id : selectedMarkup.actionId,
			variable : variableName,
			from : selectedMarkup.from,
			length : selectedMarkup.length,
			actionId : selectedMarkup.actionId,				
			type : selectedMarkup.type,
			color : randomColor,
			flow : false,
			wsdata : false,
			output : true,
			action : 'capture',
			autoType : selectedMarkup.autoType					
		}
		this.open({
			mtype : 'MarkupCapture',
			newMarkup : newMarkup,
			parserType : 'text',
			existingVariableName : existingVariableName,
			dumper : {
				dump : this.processCaptureMarkup,
				scope : this
			}
		})
	},
	processCaptureMarkup : function(newMarkup){
		//Add to variable grid
		this.variableStore.add(newMarkup);

		var markups = this.markups.slice(0);
		var targetMarkupIndex = -1;
		for(var i = 0; i < markups.length; i++){
			if(markups[i].actionId == newMarkup.actionId){
				targetMarkupIndex = i;
				break;
			}
		}
		if(targetMarkupIndex != -1)
			markups.splice(targetMarkupIndex,1);
		markups.push(Ext.apply(newMarkup,{			
			flow : newMarkup.containerType == "FLOW",
			wsdata : newMarkup.containerType == "WSDATA",
			output : newMarkup.containerType == "OUTPUT",
		}));		
		
		//This will trigger setMarkups method on the view and in turn update parser's view.
		this.set('markups', markups);
		this.resetParser();
	},
	updateVariable : function(cellInfo){
		var newData = cellInfo.record.data;
		var markups = this.markups.slice(0);
		var targetMarkupIndex = null;
		var targetMarkup = null;
		for(var i = 0; i < markups.length; i++){
			if(markups[i].actionId == newData.id){
				targetMarkup = markups[i];
				targetMarkupIndex = i;
				break;
			}
		}
		if(targetMarkupIndex != null){
			markups.splice(targetMarkupIndex,1);			
			var newMarkup = {
				actionId : targetMarkup.actionId,
				from : targetMarkup.from,
				length : targetMarkup.length,
				action : 'capture',
				flow : newData.containerType == "FLOW",
				wsdata : newData.containerType == "WSDATA",
				output : newData.containerType == "OUTPUT",
				type : newData.type,
				color : newData.color,
				variable : newData.variable
			};
			markups.push(newMarkup);
			//This will trigger setMarkups method on the view and in turn update parser's view.
			this.set('markups', markups);
		}
		this.resetParser();
	},
	removeVariable : function(record){
		if(!this.isViewOnly){
			var removedVariableId = record.raw.actionId;
			var variableStoreIndex = this.variableStore.indexOfId(removedVariableId);
			this.variableStore.removeAt(variableStoreIndex);
			var markups = this.markups.slice(0);
			var targetMarkupIndex = null;
			for(var i = 0; i < markups.length; i++){
				var markup = this.markups[i];
				if(markup.actionId == removedVariableId){
					targetMarkupIndex= i;
					break;
				}
			}
			if(targetMarkupIndex != null){
				markups.splice(targetMarkupIndex,1);
				//This will trigger setMarkups method on the view and in turn update parser's view.
				this.set('markups', markups);
				//Clear pattern table after selection.		
				this.patternProcessor.hidePattern(true);
			}
		}
	},
	
	//VIEW COMPONENT VISIBILITY
	captureMarkupIsEnabled$ : function(){
		return (this.selectedMarkup != null && this.selectedMarkup.action != 'literal' ) || this.currentSelection != null;
	},
	removeMarkupIsEnabled$ : function(){
		return this.selectedMarkup != null;
	},	
	clearSampleIsEnabled$ : function(){
		return this.sample != '' && this.activeScreen == 0;
	},	
	clearMarkupIsEnabled$ : function(){
		return this.sample != '' && this.activeScreen == 0 && this.markups && this.markups.length > 0;
	},	
	variableStoreIsEmpty$ : function(){		
		return this.variableStore.getCount() == 0;
	},
	showCodeIsVisible$: function(){		
		return this.activeScreen == 0;
	},
	showMarkupIsVisible$: function(){
		return this.activeScreen == 1;
	},
	controlBorderRegion$ : function(){
		return this.smallResolution ? 'south' : 'east';
	},
	controlHeight$ : function(){
		return this.smallResolution ? 900 : 450;
	},
	controlMargin$ : function(){
		return this.smallResolution ? '10 0 0 0' : '0 0 0 10';
	},

	//SELECTION HIGHTLIGHT LOGIC	
	processHighlightSelection : function(selection,documentBody, isLinebreak){
		this.set('selectionIsInvalid', false);
		this.set('selectedMarkup',null);
		var selectionInfo = this.getSelectionInfo(selection,documentBody,isLinebreak);		
		var selectedText = this.sample.substring(selectionInfo.from, selectionInfo.from + selectionInfo.length);
		var autoType = this.patternProcessor.getAutoTypeLogic(selectedText);
		selectionInfo.autoType = autoType;
		this.set('currentSelection', selectionInfo);	
		this.patternProcessor.updatePatternForTypeLogic(autoType);	
	},
	//This method will return start position, length of selection as well as unique actionId for that selection.
	getSelectionInfo : function(selection, documentBody, isLinebreak){
		var range = selection.getRangeAt(0);
		var startNode = range.startContainer;
		//Incase selection start from linebreak.
		while(!isLinebreak && startNode.nodeType == 1 && startNode.getAttribute('marker') == "new_line"){
			startNode = startNode.nextSibling;	
		}
		//Get wrapper node of marker instead of content
		if (startNode.parentNode && startNode.parentNode.hasAttribute('marker'))
			startNode = startNode.parentNode
		var endNode = range.endContainer;
		if (endNode.parentNode && endNode.parentNode.hasAttribute('marker'))
			endNode = endNode.parentNode;
		var segments = [];
		var currentSegment = [];
		var selecting = false;

		//Step 1 - Put DOM nodes in correct segments based on selection.
		function segmentDOMNode(node){
			for (var i = 0; i < node.childNodes.length; i++) {
				if (node.childNodes[i] == startNode || node.childNodes[i] == endNode) {
					if (!selecting) {
						segments.push(currentSegment);
						if (startNode == endNode) {
							currentSegment = [node.childNodes[i]]; //which is also start,end
							segments.push(currentSegment);
							currentSegment = [];
							continue;
						}
						currentSegment = [];
						selecting = true;
					} else {
						currentSegment.push(node.childNodes[i]); //which is the logical 'end'(it can be start or end coz the selection direction may be reversed)
						segments.push(currentSegment);
						currentSegment = [];
						selecting = false;
						continue;
					}
				}
				if (node.childNodes[i].nodeType == 3)
					currentSegment.push(node.childNodes[i]);
				else if (node.childNodes[i].hasAttribute('marker'))
					currentSegment.push(node.childNodes[i]);
				else if (!node.childNodes[i].getAttribute('exclude'))
					segmentDOMNode(node.childNodes[i]);
			}
		}
		segmentDOMNode(documentBody);
		segments.push(currentSegment);
		
		//Step 2 - Extract raw text from each segment
		var pre = this.extractTextFromSegment(segments[0]);
		var selection = this.extractTextFromSegment(segments[1]);
		var post = this.extractTextFromSegment(segments[2]);

		//Step 3 - Extract partial text from selection segment.
		if (segments[1].length == 1) {
			var prePartialResult = this.parsePartialStartNode(segments[1][0], range.startOffset);
			var postPartialResult = this.parsePartialEndNode(segments[1][0], range.endOffset);
			pre = pre + prePartialResult.pre
			selection = (segments[1][0].nodeType != 3) ? prePartialResult.selected : this.getTextFromNode(segments[1][0]).substring(range.startOffset, range.endOffset);
			post = postPartialResult.post + post;
		} else {
			var partialStart = segments[1][0];
			var partialEnd = segments[1][segments[1].length - 1];
			var prePartialResult = this.parsePartialStartNode(partialStart, range.startOffset);
			var postPartialResult = this.parsePartialEndNode(partialEnd, range.endOffset);
			var midSelected = this.extractTextFromSegment(segments[1].slice(1, segments[1].length - 1));
			pre = pre + prePartialResult.pre
			selection = prePartialResult.selected + midSelected + postPartialResult.selected;
			post = postPartialResult.post + post;
		}

		// console.log("PRE TEXT: " + pre);
		// console.log("SELECTION TEXT: " + selection);
		// console.log("POST TEXT: " + post);

		//Step 4 - return info for this selection.
		if(isLinebreak){
			return {
				from: pre.length,
				length: selection.length + 2,
				actionId: Ext.data.IdGenerator.get('uuid').generate()
			}
		}
		return {
			from: pre.length,
			length: selection.length,
			actionId: Ext.data.IdGenerator.get('uuid').generate()
		};
	},
	extractTextFromSegment : function(segment){
		var str = '';
		for (var i = 0; i < segment.length; i++) {
			if (segment[i].nodeType == 3)
				str += this.getTextFromNode(segment[i]);
			else {
				if (segment[i].getAttribute('marker') == "new_line")
					str += (Ext.is.Windows ? '\r\n' : '\n');
				else if (segment[i].getAttribute('marker') == "space")
					str += ' ';
			}
		}	
		return str;
	},
	getTextFromNode: function(node) {
		if(node.parentNode && node.parentNode.getAttribute('type') == 'endOfLine')
			return '';
		var textPropName = node.textContent === undefined ? 'nodeValue' : 'textContent';
		return node[textPropName];
	},
	parsePartialStartNode: function(node, startOffset) {
		return {
			pre: this.getTextFromNode(node).substring(0, startOffset),
			selected: this.getTextFromNode(node).substring(startOffset, this.getTextFromNode(node).length)
		};
	},
	parsePartialEndNode: function(node, endOffset) {
		return {
			post: this.getTextFromNode(node).substring(endOffset, this.getTextFromNode(node).length),
			selected: this.getTextFromNode(node).substring(0, endOffset)
		};
	},
	
	//MARKUP SELECTION LOGIC
	processMarkupSelection : function(actionId){
		this.set('currentSelection',null);
		this.set('selectionIsInvalid', false);		
		var selectedMarkup = {};
		for(var i = 0; i < this.markups.length; i++){
			selectedMarkup = this.markups[i];
			if(selectedMarkup.actionId == actionId)
				break;
		}
		this.set('selectedMarkup', selectedMarkup);		
		this.patternProcessor.updatePatternForMarkup(selectedMarkup['type'],selectedMarkup['autoType']);
	}
});