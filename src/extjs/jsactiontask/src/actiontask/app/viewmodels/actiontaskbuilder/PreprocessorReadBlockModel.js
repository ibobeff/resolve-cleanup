Ext.define('RS.actiontaskbuilder.PreprocessorReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Preprocessor',
		collapsed: true,
		classList: ['block-model'],
		title: '',
		hidden: false,
		header: {
            titlePosition: 0,
            defaults: {
            	margin: 0
        	},
            items: []
        },
		content: '',
		contentIsEmpty: false,

		minResize: 71,
		minHeight: 371,
		height: 371,
		
		postInit: function (blockModel) {
			this.blockModel = blockModel;
		},

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden && this.parentVM.embed) {
					var preprocessorData = this.parentVM.preprocessorEdit.getData();
					var preprocessor = preprocessorData[0];

					this.parentVM.preprocessorRead.load(preprocessor);
				}

				if (this.contentIsEmpty) {
					this.shrinkPanel();
				} else {
					this.growPanel();
				}
			}
		},

		when_collapsed_changed: {
			on: ['collapsedChanged'],
			action: function() {
				if (this.contentIsEmpty) {
					this.shrinkPanel();
				} else {
					this.growPanel();
				}
			}
		},

		when_contentIsEmpty_changed: {
			on: ['contentIsEmptyChanged'],
			action: function() {
				if (this.contentIsEmpty) {
					this.shrinkPanel();
				} else {
					this.growPanel();
				}
			}
		},

		shrinkPanel: function () {
			this.set('minHeight', 86);
			this.set('minResize', 86);
			this.set('height', 0);
			this.set('height', 'auto');
		},

		growPanel: function () {
			this.set('minHeight', 371);
			this.set('minResize', 133);
			this.set('height', 0);
			this.set('height', this.minHeight);
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.preprocess'];
	},

	load: function (preprocessor) {
		var g = this.gluModel;
		var script = preprocessor.uscript || '',
			classes = ['block-model'];

		this.set('content', script)
			.set('classList', classes);

		g.set('contentIsEmpty', !g.content || g.content.length === 0);
	}
});
