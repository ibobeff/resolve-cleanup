glu.defModel('RS.actiontaskbuilder.AssignValueToOutputDetail', {
	fields : ['outputType','outputName','sourceType','sourceName'],			
	validationRegex : RS.common.validation.VariableName,	
	outputTypeDisplay: '',	
	outputNameEmptyText: '',	
	sourceTypeDisplay: '',			
	sourceNameEmptyText: '',
	outputTypesStore: {
		mtype : 'store',
		fields : ['name']
	},		
	sourceTypesStore:  {
		mtype : 'store',
		fields : ['name']
	},
	sourceLabelText: '',
	inputParametersStore: {
		mtype : 'store',
		fields : ['name']
	},
	outputParametersStore: {
		mtype : 'store',
		fields : ['name']
	},
	allParameters : {
		INPUT : [],
		OUTPUT : [],
		FLOW : [],
		WSDATA : []			
	},
	defaultValue : {
		outputType : '',
		outputName : '',
		sourceType : '',
		sourceName : ''
	},	
	when_output_type_changed : {
		on : ['outputTypeChanged'],
		action : function(){
			var currentType = this.outputType;
			this.set('outputName', '');
			if(currentType == 'OUTPUT')
				this.set('outputNameEmptyText',this.localize('selectOutput'));
			else if(currentType == 'WSDATA' || currentType == 'FLOW')
				this.set('outputNameEmptyText', this.localize('selectOrEnterOutput'));
			else 
				this.set('outputNameEmptyText', this.localize('enterOutputName'));
			this.set('outputNameIsPristine', true);
			this.loadParameterStore(this.outputParametersStore, currentType);
		}
	},			
	when_source_type_changed : {
		on : ['sourceTypeChanged'],
		action : function(){
			var currentType = this.sourceType;
			this.set('sourceName', '');
			if(currentType == 'INPUT')
				this.set('sourceNameEmptyText',this.localize('selectInput'));
			else if(currentType == 'WSDATA' || currentType == 'FLOW')
				this.set('sourceNameEmptyText', this.localize('selectOrEnterInput'));
			else 
				this.set('sourceNameEmptyText', this.localize('enterSourceName'));
			this.set('sourceNameIsPristine', true);
			this.loadParameterStore(this.inputParametersStore, currentType);				
		}
	},
	init : function(){
		this.set('outputNameEmptyText', this.localize('enterOutputName'));
		this.set('sourceNameEmptyText', this.localize('enterSourceName'));
	},
	//Validation		
	outputTypeIsValid$ : function(){
		return this.outputType ? true : this.localize('requiredField');
	},
	outputNameIsText$ : function(){
		return this.outputType == 'PARAM';
	},
	outputNameIsEditable$ : function(){
		return this.outputType == 'FLOW' || this.outputType == 'WSDATA' || this.outputType == 'OUTPUT';
	},
	outputNameIsValid$ : function(){
		return this.outputName/* && this.validationRegex.test(this.outputName)*/ ? true : this.localize('validKeyCharacters');
	},
	sourceTypeIsValid$ : function(){
		return this.sourceType ? true : this.localize('requiredField');
	},
	sourceNameIsText$ : function(){
		return this.sourceType == 'PARAM' || this.sourceType == 'CONSTANT';
	},
	sourceNameIsEditable$ : function(){
		return this.sourceType == 'FLOW' || this.sourceType == 'WSDATA'
	},
	sourceNameIsValid$ : function(){
		return this.sourceName /*&& this.validationRegex.test(this.sourceName)*/ ? true : this.localize('validKeyCharacters');
	},		
	
	loadParameterStore : function(store, dataType){
		if(dataType == 'INPUT')
			store.loadData(this.allParameters['INPUT']);
		else if(dataType == 'OUTPUT')
			store.loadData(this.allParameters['OUTPUT']);
		else if(dataType == 'FLOW')
			store.loadData(this.allParameters['FLOW']);
		else if(dataType == 'WSDATA')
			store.loadData(this.allParameters['WSDATA']);
	},	
	setOutputTypes: function (outputTypes) {
		this.outputTypesStore.loadData(outputTypes);		
	},
	setSourceTypes: function (sourceTypes) {
		this.sourceTypesStore.loadData(sourceTypes);	
	},		
	addAssignment: function () {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		var added = this.parentVM.addAssignment(this.asObject());

		if (added) {
			this.resetForm();	
		}
	},
	resetForm : function(){
		this.loadData(this.defaultValue);
		this.set('isPristine', true);
	}
});