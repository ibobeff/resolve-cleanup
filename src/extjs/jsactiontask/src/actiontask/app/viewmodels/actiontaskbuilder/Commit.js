glu.defModel('RS.actiontaskbuilder.Commit', {
	comment: '',

	commit: function() {
		this.parentVM.verifyCommitVersion(this.comment);
		this.doClose()
	},
	commitIsEnabled$: function() {
		return this.comment;
	},
	cancel: function() {
		this.doClose()
	}
})