Ext.define('RS.actiontaskbuilder.ParametersInputEditBlockModel', {
	extend: 'RS.common.blocks.BlockModel',
	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Parameters',
        title: '',
		hidden: true,
		collapsed: true,
		editBlock: true,
		stopPropagation: false,

       	header: {
	 		xtype: 'toolbar',
	    	margin: 0,
	    	padding: 0,
	    	items: []
	 	},	
	 	fields : ['newGroupName'],
		inputParametersStore: null,
		hasInputParameters: false,

		newGroupName: '',

		detail: {
			mtype: 'ParameterInputDetail'
		},

		unclassified: '',
		
		postInit: function (blockModel, context) {
			this.blockModel = blockModel;			
			this.unclassified = context.localize('unclassified');
			this.detail.setAddInputButtonTitle(context.localize('addInput'));
			this.set('inputParametersStore', new RS.common.data.FlatStore({
				fields: ['id', 'uname', 'udescription', 'udefaultValue', 'uencrypt', 'urequired', 'uprotected', 'ugroupName', 'utype', 'uorder'],
				groupField: 'ugroupName',
				sorters: [{
			    	property: 'value',
			    	direction: 'uorder'
				}],
				listeners: {
					datachanged: function () {
						this.notifyDirty();					
					}.bind(this),
					update: function () {
						this.notifyDirty();						
					}.bind(this)						
				}
			}));
			this.header.items = [{
				xtype: 'component',
				padding: '0 0 0 15',
				cls: 'x-header-text x-panel-header-text-container-default',
				html: this.localize('parametersTitle'),
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
						}, this);
					}.bind(this)
				}
			}, {
	        	xtype: 'container',
	        	autoEl: 'ol',
	        	cls: 'breadcrumb flat',
	        	margin: 0,
	        	padding: 0,
	        	items: [{
		    		xtype: 'component',
		    		autoEl: 'li',
		    		cls: 'active',
		    		html: this.localize('inputParametersEditTitle'),
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
							}, this);
						}.bind(this)
					}
		    	}, {
		    		xtype: 'component',
		    		autoEl: 'li',
		    		html: this.localize('outputParametersEditTitle'),
		    		listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.jumpTo('ParametersOutputEdit', this.parentVM.parametersOutputEdit.gluModel);
				            }, this);						
						}.bind(this)
		    		}
		    	}]
			}, '->', {
				xtype: 'button',
				tooltip: this.localize('resizeFullscreen'),
				iconCls: 'rs-block-button icon-resize-full',
				isResizeMaxBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['PARAMETERS']);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				tooltip: this.localize('resizeNormalscreen'),
				iconCls: 'rs-block-button icon-resize-small',
				hidden: true,
				isResizeNormalBtn: true,
				margin: '0 20',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeNormalSection(this);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				iconCls: 'rs-block-button icon-chevron-sign-up',
				isCollapseBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.set('collapsed', true);
						}, this);
					}.bind(this)
				}
			}];
			this.inputParametersStore.on('add', function(store, records){
				// for Encrypted INPUT variables, automatically add the resolveActionInvocOptions BLANKVALUES with value "$INPUT{<paramName>}"
				if (records && records[0].data.uencrypt) {
					var resolveActionInvocOptions = this.rootVM.optionsEdit.gluModel;
					resolveActionInvocOptions.addInputParamToOptionBlankValue(records[0].data.uname);
				}
			},this);
			this.inputParametersStore.on('remove', function(store, record){
				// for Encrypted INPUT variables, automatically remove the resolveActionInvocOptions BLANKVALUES with value "$INPUT{<paramName>}"
				if (record.data.uencrypt) {
					var resolveActionInvocOptions = this.rootVM.optionsEdit.gluModel;
					resolveActionInvocOptions.removeInputParamFromOptionBlankValue(record.data.uname);
				}
			},this);
			this.inputParametersStore.on('update', function(store, record, operation, modifiedFieldNames){
				if (modifiedFieldNames.length && modifiedFieldNames[0] == 'uencrypt') {
					// for Encrypted INPUT variables, automatically add/remove the resolveActionInvocOptions BLANKVALUES with value "$INPUT{<input.uname>}"
					var resolveActionInvocOptions = this.rootVM.optionsEdit.gluModel;
					if (record.data.uencrypt) {
						resolveActionInvocOptions.addInputParamToOptionBlankValue(record.data.uname);
					} else {
						resolveActionInvocOptions.removeInputParamFromOptionBlankValue(record.data.uname);
					}
				}
			},this);
		},
		
		jumpTo: function(nextBlockName, nextSection){
			this.parentVM.parametersEditSectionExpanded = !this.collapsed;
			this.parentVM.parametersEditSectionMaximized = this.isSectionMaxSize;
			this.parentVM.collapseAndHideSection(this);
			this.parentVM.showAndExpandThisSection(nextSection);
		},
		
		notifyDirty: function() {
			this.blockModel.notifyDirty();
			if(this.inputParametersStore){
				this.blockModel.notifyDataChange({
					INPUT : this.inputParametersStore.collect('uname')
				});
			}				
		},
		newGroupNameIsValid$ : function(){
			return RS.common.validation.PlainText.test(this.newGroupName) ? true : this.localize('invalidGroup');
		},
		addNewGroupIsEnabled$ : function(){
			return this.newGroupNameIsValid == true;
		},
		addNewGroup: function() {		
			var recordIdx = this.detail.groupStore.find('text', this.newGroupName, 0, false, false, true);
			if (recordIdx == -1) {
				this.detail.addGroupName(this.newGroupName);
			}
			else {
				clientVM.message(this.localize('invalidGroupNameTitle'),this.localize('duplicateGroupName'));		
			}
		},

		// called by detail.
		addInputToList: function(input) {
			var store = this.inputParametersStore;
			
			if (store.findExact('uname', input.uname) === -1) {
				if (store.count()) {
					input.uorder = store.last().data.uorder;
				}

				store.suspendEvents();
				var model = store.add(input)[0];
				store.remove(model);
				store.resumeEvents();
				store.addSorted(model);
				this.set('hasInputParameters', this.inputParametersStore.count() > 0);

			} else {
				this.displayDuplicateNameMsg();		
			}
		},
		displayDuplicateNameMsg : function(){
			clientVM.message(this.localize('invalidParameterTitle'),this.localize('parameterAlreadyExists'));		
		},
		removeParameter: function (p) {
			var store = this.inputParametersStore;
			var count = store.count();

			if (count === 1) {
				store.removeAll();
				this.set('hasInputParameters', false);
			} else {
				store.remove(p);
				this.set('hasInputParameters', true);
			}
		},

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden) {
					setTimeout(function() {
						if (this.parentVM.parametersEditSectionExpanded) {
							this.parentVM.parametersEditSectionExpanded = null;
							this.set('collapsed', false);
						}
						if (this.parentVM.parametersEditSectionMaximized) {
							this.parentVM.parametersEditSectionMaximized = null;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['PARAMETERS']);
						}
					}.bind(this), 10);
				}
			}
		},

		closeFlag: false,
		close: function () {
			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		}
	},

	getDataPaths: function () {
		return [{
			filter: function (p) {
				return p.utype === 'INPUT' || p.utype === null;
			},
			path: 'data.resolveActionInvoc.resolveActionParameters'
		}];
	},

	getData: function () {		
		return [this.gluModel.inputParametersStore.getData()];
	},

	load: function (inputParameters) {
		var g = this.gluModel,
			store = g.inputParametersStore;

		for (var i = 0; i < inputParameters.length; i++) {
			var p = inputParameters[i];
			p.id = p.id || '';
			p.uname = p.uname || '';
			p.udescription = p.udescription || '';
			p.udefaultValue = p.udefaultValue || '';
			p.uencrypt = p.uencrypt || false;
			p.urequired = p.urequired || false;
			p.uprotected = p.uprotected || false;
			p.uorder = p.uorder || 0;

			if (p.ugroupName === null) {
				p.ugroupName = g.unclassified;
			}
		}

		g.detail.setGroupsByParameters(inputParameters);
		store.loadData(inputParameters);
		this.set('hasInputParameters', store.count() > 0);
	}
});
