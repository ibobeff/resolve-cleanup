glu.defModel('RS.actiontaskbuilder.TestControl',{
	testTabPanel : null,
	activeTestSample : 0,
	code : null,
	testResultTitle : '',
	testResultStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},
	API : {		
		testCode : '/resolve/service/resolutionbuilder/testCode',
		pollTest : '/resolve/service/resolutionbuilder/testCode/poll'
	},	
	testDataMap : {},
	defaultTestSample : '',
	activeParserType : 'text',
	when_default_test_sample : {
		on : ['defaultTestSampleChanged'],
		action : function(newVal){
			this.testDataMap[this.activeParserType] = newVal;
		}
	},
	init : function(){
		this.set('testResultTitle', this.localize('testResult', this.localize('defaultTestSampleTitle')));
	},
	registerTestTab : function(testTabPanel){
		this.set('testTabPanel', testTabPanel);
	},
	updateActiveParserType : function(parserType){
		this.set('activeParserType', parserType);
		this.set('defaultTestSample', this.testDataMap[parserType]);
	},
	updateDefaultTestSample : function(parserType, data){
		this.testDataMap[parserType] = data;
	},
	resetTest : function(){
		if(this.testTabPanel){
			while(this.testTabPanel && this.testTabPanel.items.length > 2){
				var testTab = this.testTabPanel.items.getAt(1);
				this.testTabPanel.remove(testTab);
			}
			this.testTabPanel.items.getAt(1).testTabCounter = 1;
		}
		
		this.set('activeTestSample', 0);
		this.set('testDataMap', {});
		this.testResultStore.removeAll();
	},
	executeTest: function() {
		this.set('code', this.parentVM.getCode());
 		this.startTestProcess();	
	},
	startTestProcess : function(){
		this.testResultStore.removeAll();
		if(!this.code){
			this.testResultStore.add({
				name: 'Message',					
				value: this.localize('noScriptToExecute')
			});
			return;
		}
		var currentActiveTestTab = this.testTabPanel.getActiveTab();
		this.set('testResultTitle', this.localize('testResult', currentActiveTestTab.title));
		var testSample = currentActiveTestTab.getValue();
		this.ajax({
			url: this.API['testCode'],
			params: {
				code: this.code,
				testData: testSample
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);

				if (respData.success) {
					var indicator = clientVM.displayMessage(this.localize('executeTest'), this.localize('executeTestInProgress'), {
						duration: -1
					});
					
					this.pollTest(indicator);
				} else {
					clientVM.displayError(respData.message)
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},	
	pollTest: function(indicator) {
		this.ajax({
			url: this.API['pollTest'],
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				var me = this;

				if (!respData.success || respData.data.stage !== 'DONE') {
					setTimeout(function() {
						me.pollTest(indicator);
					});
					return;
				}

				var data = respData.data;
				this.testResultStore.removeAll();
				if(data.success){
					var result = data.result;
					if(result && Object.keys(result).length > 0){
						Ext.Object.each(result, function(k, v) {
							//Ignore those 2 entries since they show useless infomation (maybe remove from ajax later).
							if(k == 'parsing content' || k == 'regex matched')
								return;
							this.testResultStore.add({
								name: k,
								value: v							
							});
						}, this);	
					}
					else
					{
						this.testResultStore.add({
							name: 'Message',					
							value: this.localize('noResultMsg')
						});
					}
					
				}
				else{
					this.testResultStore.add({
						name: 'Error',					
						value: data.message
					});
				}
				indicator.fadeOut({
					remove: true
				});							
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},	
})
