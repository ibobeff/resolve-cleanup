glu.defModel('RS.actiontaskbuilder.QuickAccessHelpers', {

	isSectionMaxSize: false,
	ignoreDecrementFlag: false,

	showSectionCallback: function() {
		if (this.showAndExpandSectionFlag) {
			setTimeout(function() {
				this.parentVM.showAndExpandSectionCallback(this);
			}.bind(this), 200);
		}
	},

	collapseSectionCallback: function() {
		if (this.collapseAndHideSectionFlag) {
			setTimeout(function() {
				this.parentVM.collapseAndHideSectionCallback(this);
			}.bind(this), 200);
		}
	},

	toggleExpandCollapsePanel: function() {
		if (this.stopPropagation) {
			this.stopPropagation = false;
		} else if (this.closeFlag) {
			this.closeFlag = false;
			this.set('collapsed', true);
		} else {
			this.set('collapsed', !this.collapsed);
		}
	},

	toggleResizeMaxBtn: function() {
		if (!this.isSectionMaxSize) {
			if (this.collapsed) {
				this.parentVM.hideSectionMaxResizeBtn(this);
			} else {
				this.parentVM.showSectionMaxResizeBtn(this);
			}
		}
	},

	incNumSectionsOpen: function() {
		this.parentVM.incNumSectionsOpen(this);
	},

	decNumSectionsOpen: function() {
		this.parentVM.decNumSectionsOpen(this);
	},

	view: null,
	viewRenderCompleted: function(view) {
		this.set('view', view);
	}
});
