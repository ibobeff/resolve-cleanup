glu.defModel('RS.actiontaskbuilder.Version', {
	name : '',
	uname: '',
	id : '',
	unamespace: '',
	versionsList: {
		mtype: 'store',
		fields: ['documentName', 'isStable', 'referenced', 'version', 'type', 'comment', 'createdBy', {
			name: 'sysCreatedOn',
			type: 'time',
			format: 'c',
			dateFormat: 'c'
		}],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	compareVersionIsVisible: true,
	rollbackVersionIsVisible: true,
	versionsListSelections: [],
	versionsListColumns: [],
	init: function() {
		this.initPagination();
		this.loadVersion();
		var dateFormat = this.parentVM.userDateFormat || this.rootVM.userDateFormat;
		this.set('versionsListColumns', [{
			header: '~~documentName~~',
			dataIndex: 'documentName',
			width: 132,
			sortable: false
		}, {
			header: '~~versionNumber~~',
			dataIndex: 'version',
			width: 100
		}, {
			header: '~~stableVersion~~',
			dataIndex: 'isStable',
			width: 100,
			renderer: RS.common.grid.booleanRenderer(),
		}, {
			header: '~~referenced~~',
			dataIndex: 'referenced',
			width: 100,
			renderer: RS.common.grid.booleanRenderer(),
		}, {
			header: '~~comment~~',
			flex: 1,
			dataIndex: 'comment',
			sortable: false,
			renderer: function(value, metaData) {
				metaData['tdAttr'] = Ext.String.format('data-qtip="{0}"', Ext.String.htmlEncode(value));
				return Ext.String.htmlEncode(value);
			},
		}, {
			header: '~~sysUpdatedBy~~',
			dataIndex: 'createdBy'
		}, {
			header: '~~createdOn~~',
			dataIndex: 'sysCreatedOn',
			width: 150,
			renderer: function(value) {
				return Ext.Date.format(new Date(value), dateFormat)
			}
		}]);
		if (this.isDetails) {
			this.set('compareVersionIsVisible', false);
			this.set('rollbackVersionIsVisible', false);
		}
	},
	loadVersion: function() {
		this.versionsList.removeAll();
		this.ajax({
			url: '/resolve/service/actiontask/getHistory',
			params: {
				id: this.id,
				docFullName: this.name
			},
			method: 'GET',
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('listVersionsError'), respData.message);
					return;
				}
				this.loadPageRecords(respData.records);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},
	commitVersion: function() {

	},

	// Client-side pagination
	list: [],
	pageList: [],
	currentPage: 1,
	numberPerPage: 20,
	numberOfPages: 1,
	paginationPadding: '0 0 0 0',

	nextDisabled: false,
	previousDisabled: false,
	firstDisabled: false,
	lastDisabled: false,

	initPagination: function() {
		var versionDialogWidth = Math.round(Ext.getBody().getWidth() * 0.8);
		this.set('paginationPadding', Ext.String.format('0 0 0 {0}', Math.round(versionDialogWidth / 2) - 100));
	},

	loadPageRecords: function(records) {
		this.set('list', records);
		this.set('numberOfPages', Math.ceil(this.list.length / this.numberPerPage));
		this.loadList();
	},

	firstPage: function() {
		this.set('currentPage', 1);
		this.loadList();
	},
	lastPage: function() {
		this.set('currentPage', this.numberOfPages);
		this.loadList();
	},
	nextPage: function() {
		this.set('currentPage', this.currentPage += 1);
		this.loadList();
	},
	previousPage: function() {
		this.set('currentPage', this.currentPage -= 1);
		this.loadList();
	},
	loadList: function() {
		var begin = ((this.currentPage - 1) * this.numberPerPage);
		var end = begin + this.numberPerPage;
		this.set('pageList', this.list.slice(begin, end));
		this.versionsList.loadData(this.pageList);
		this.check();
	},
	check: function() {
		this.set('nextDisabled', (!this.numberOfPages || this.currentPage == this.numberOfPages) ? true : false);
		this.set('previousDisabled', this.currentPage == 1 ? true : false);
		this.set('firstDisabled', this.currentPage == 1 ? true : false);
		this.set('lastDisabled', (!this.numberOfPages || this.currentPage == this.numberOfPages) ? true : false);
	},
	paginationText$: function() {
		var max = this.list.length;
		var begin = max ? ((this.currentPage - 1) * this.numberPerPage) + 1 : 0;
		var end = begin + this.numberPerPage - 1;
		if (end > max) {
			end = max;
		}
		return Ext.String.format('{0} - {1} of {2}', begin, end, max);
	},

	viewVersion: function() {
		/* TODO
		var versionNo = this.versionsListSelections[0].get('version');
		this.open({
			mtype: 'RS.actiontaskbuilder.VersionView',
			id: this.id,
			namespace: this.unamespace,
			name: this.uname,
			version: versionNo
		}); 
		*/ 
	},
	viewVersionIsEnabled$: function() {
		return this.versionsListSelections.length == 1;
	},
	compareVersion: function() {
		/* TODO
		this.open({
			mtype: 'RS.pagebuilder.VersionComparison',
			id: this.id,
			namespace: this.unamespace,
			name: this.uname,
			type: this.versionsListSelections[0].get('type'),
			version1: this.versionsListSelections[0].get('version'),
			version2: this.versionsListSelections[1].get('version')
		}); 
		*/ 
	},
	compareVersionIsEnabled$: function() {
		if (this.versionsListSelections.length != 2)
			return false;
		var type1 = this.versionsListSelections[0].get('type');
		var type2 = this.versionsListSelections[1].get('type');
		return type1 == type2;
	},
	rollbackVersion: function() {
		/* TODO
		this.message({
			title: this.localize('rollbackVersion'),
			msg: this.localize('rollbackVersionMsg'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				'yes': this.localize('rollbackVersion'),
				'no': this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.ajax({
					url: '/resolve/service/actiontask/version/rollback',
					params: {
						id: this.id,
						docFullname: this.ufullname,
						version: this.versionsListSelections[0].get('version')
					},
					success: function(resp) {
						var respData = RS.common.parsePayload(resp);
						if (!respData.success) {
							clientVM.displayError(this.localize('rollbackError', {
								msg: respData.message
							}));
							return;
						}
						clientVM.displaySuccess(this.localize('rollbackSuccess'));
						this.parentVM.set('havePersistedWikiSinceLastView', true);					
						this.loadVersion();
						this.parentVM.refresh();
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				});
			}
		}) 
		*/ 
	},
	rollbackVersionIsEnabled$: function() {
		return this.versionsListSelections.length == 1;
	},
	selectVersion: function() {
        if (!this.dumper)
            return
        if (Ext.isFunction(this.dumper))
            this.dumper(this.versionsListSelections[0]);
        else {
            var scope = this.dumper.scope;         
            this.dumper.dump.call(scope, [this.versionsListSelections[0]]);
        }
        this.doClose()
	},
	selectVersionIsEnabled$: function() {
		return this.versionsListSelections.length == 1;
	},
	close: function() {
		this.doClose();
	}
});