Ext.define('RS.actiontaskbuilder.ContentReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	typeMaps: {
		REMOTE: 'groovy',
		POWERSHELL: 'powershell',
		PROCESS: 'groovy'
	},

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Content',
		collapsed: true,
        title: '',
		hidden: true,
		conditionalReadBlock: true,
       	header: {
            titlePosition: 0,
            defaults: {
            	margin: 0
        	},
            items: []
        },
		content: '',
		contentIsEmpty: false,
		syntax: '',
		command: '',
		contentLabel: 'Content',
		editorType: 'text',
		commandIsVisible: true,

		minHeight: 71,
		minResize: 133,
		height: 371,

		noCommand: '',

		postInit: function (blockModel) {
			this.blockModel = blockModel;
			this.noCommand = this.localize('noCommand');
		},

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden && this.parentVM.embed) {
					var contentData = this.parentVM.contentEdit.getData();

					var resolveActionInvoc = contentData[0],
						inputFile = contentData[1];

					this.parentVM.contentRead.load(resolveActionInvoc, inputFile);
				}

				if (this.contentIsEmpty) {
					this.shrinkPanel();
				} else {
					this.growPanel();
				}
			}
		},

		when_collapsed_changed: {
			on: ['collapsedChanged'],
			action: function() {
				if (this.contentIsEmpty) {
					this.shrinkPanel();
				} else {
					this.growPanel();
				}
			}
		},

		when_contentIsEmpty_changed: {
			on: ['contentIsEmptyChanged'],
			action: function() {
				if (this.contentIsEmpty) {
					this.shrinkPanel();
				} else {
					this.growPanel();
				}
			}
		},

		contentEmptyText$: function() {
			if (this.commandIsVisible) {
				return this.localize('inputFileEmptyText');
			} else {
				return this.localize('sourceCodeSectionEmptyText');
			}
		},

		shrinkPanel: function () {
			if (this.commandIsVisible) {
				this.set('minHeight', 126);
				this.set('minResize', 126);
			} else {
				this.set('minHeight', 86);
				this.set('minResize', 86);
			} 
			this.set('height', 0);
			this.set('height', 'auto');
		},

		growPanel: function () {
			this.set('minHeight', 371);
			this.set('minResize', 133);
			this.set('height', 0);
			this.set('height', this.minHeight);
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc', {
			path: 'data.resolveActionInvoc.resolveActionInvocOptions',
			filter: function (o) {
				return o.uname === 'INPUTFILE'
			}
		}];
	},

	load: function (resolveActionInvoc, inputFile) {
		var g = this.gluModel;
		this.set('command', '');

		switch(resolveActionInvoc.utype) {
		case 'ASSESS':
		case 'REMOTE':
		case 'CSCRIPT':
		case 'POWERSHELL':
			g.set('commandIsVisible', false);
			break;
		default:
			g.set('commandIsVisible', true);
			var command = resolveActionInvoc.ucommand || '';
			var args = resolveActionInvoc.uargs || '';

			if (args.length) {
				command += ' ';
				command += args;
			}

			if (command.length === 0) {
				command = g.noCommand;
			}

			this.set('command', command);
			break;
		}

		if (inputFile && inputFile.length > 0) {
			this.set('content', inputFile[0].uvalue);
		}

		g.set('contentIsEmpty', !g.content || g.content.length === 0);
	},

	modifyByNotification: function(data, resolveActionInvoc, assignedTo) {
		var t = resolveActionInvoc.utype,
			isNew = !data.id;

		if (typeof this.typeMaps[t] !== 'undefined') {
			this.set('editorType', this.typeMaps[t]);
		} else {
			this.set('editorType', 'text');
		}

		if (t === 'ASSESS') {
			this.hide();
			this.set('commandIsVisible', false);
		} else {
			switch(t) {
			case 'REMOTE':
				this.setViewTitle(this.gluModel.localize('titleForRemote'));
				this.set('commandIsVisible', false);
				break;
			case 'CSCRIPT':
				this.setViewTitle(this.gluModel.localize('titleForCScript'));
				this.set('commandIsVisible', false)
					.set('contentLabel', this.gluModel.localize('contentLabel'));
				break;
			case 'POWERSHELL':
				this.setViewTitle(this.gluModel.localize('titleForPowershell'));
				this.set('commandIsVisible', false)
					.set('contentLabel', this.gluModel.localize('contentLabel'));
				break;
			case 'CMD':
			case 'OS':
			case 'BASH':
				this.setViewTitle(this.gluModel.localize('titleForCommand'));
				this.set('commandIsVisible', true)
					.set('contentLabel', this.gluModel.localize('inputFileLabel'));
				break;
			default:
				// process or external
				this.setViewTitle(this.gluModel.localize('titleForDefault'));
				this.set('commandIsVisible', true)
					.set('contentLabel', this.gluModel.localize('contentLabel'));
				break;
			}

			if (!isNew && this.gluModel.parentVM.readOnlyMode) {
				//this.showCurrent();
				this.gluModel.set('hidden', false);
			}
		}
	}
});