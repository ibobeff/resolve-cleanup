glu.defModel('RS.actiontaskbuilder.Access', {
	uname: null,
	read: true,
	write: true,
	execute: true,
	admin: false,
	isDisabled: true,
	isAdmin: false,
	rolesStore: null,

	setRoles: function (roles) {
		roles = this.rolesStore.shapeData(roles);	
		this.rolesStore.loadData(roles);
		this.set('uname', null);
	},

	addRole: function (role) {
		role = this.rolesStore.shapeData(role);
		this.rolesStore.add(role);
		this.set('uname', null);
	},

	removeRole: function (role) {
		this.rolesStore.removeBy('value', role);
		this.set('uname', null);
	},

	addIsDisabled$: function () {
		var allCheckboxesAreUnchecked = !this.read && !this.write && !this.execute && !this.admin;
		return this.uname === null || this.uname.length === 0 || allCheckboxesAreUnchecked;
	},

	addIsEnabled$: function () {
		var oneCheckboxIsChecked = this.read || this.write || this.execute || this.admin;
		return this.uname !== null && this.uname.length > 1 && oneCheckboxIsChecked;
	},

	init: function () {
		this.set('rolesStore', new RS.common.data.FlatStore());

		if (clientVM.user.name == 'resolve.maint' || (clientVM.user.roles && Ext.Array.indexOf(clientVM.user.roles, 'admin') > -1)) {
			this.set('isAdmin', true);
			this.set('admin', this.isAdmin);
		}
	},

	setDisabled: function (isDisabled) {
		this.set('isDisabled', isDisabled);
	},

	add: function () {
		this.rolesStore.removeBy('value', this.uname);
		this.parentVM.add(this);
		this.set('uname', '');
		this.set('read', true);
		this.set('write', true);
		this.set('execute', true);
		this.set('admin', this.isAdmin);
	},	

	focusRole: function (e) {
		if (e && e.getKey && typeof e.getKey() !== 'undefined' && e.currentTarget) {
			var id = e.currentTarget.id;

			if (id && id.length > 0) {
				var componentID = id.substring(0, id.indexOf('-inputEl'));
				var combo = Ext.ComponentQuery.query('*[id^='+ componentID +']');

				if (combo && combo.length > 0) {
					combo[0].expand();
				}
			}
		}
	}
});