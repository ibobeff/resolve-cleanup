Ext.define('RS.actiontaskbuilder.SeveritiesEditBlockModel', {
    extend: 'RS.common.blocks.BlockModel',

    modelConfig: {
		mixins: ['QuickAccessHelpers'],
        blockModel: null,
		blockModelClass: 'SeveritiesAndConditions',
        hidden: true,
		collapsed: true,
		editBlock: true,
		stopPropagation: false,
        title: '',
        header: {
            xtype: 'toolbar',
            margin: 0,
            padding: 0,
            items: []
        },

        items: [],
        expressionType : 'severity',
		allATProperties: [],

        postInit: function (blockModel) {
            this.blockModel = blockModel;
            var critical = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            critical.initContainer(3, '~~criticalTitle~~', '#c00');
            var severe = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            severe.initContainer(2, '~~severeTitle~~', '#f80');
            var warning = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            warning.initContainer(1, '~~warningTitle~~', '#f2cc11');
            var good = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            good.initContainer(0, '~~goodTitle~~', '#0b0');
            this.items.push(critical);
            this.items.push(severe);
            this.items.push(warning);
            this.items.push(good);
            this.header.items = [/*{
				xtype: 'component',
				padding: '0 0 0 15',
				cls: 'x-header-text x-panel-header-text-container-default',
				html: this.localize('severitiesEditTitle')
			},*/
			{
                xtype: 'container',
                autoEl: 'ol',
                cls: 'breadcrumb flat',
                margin: 0,
                padding: 0,
                items: [{
                    xtype: 'component',
                    autoEl: 'li',                  
                    html: this.localize('severitiesEditTitle'),
                    cls : 'active',
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
							}, this);
						}.bind(this)
					}
                }, {
                    xtype: 'component',
                    autoEl: 'li',
                    html: this.localize('conditionsEditTitle'),
                    listeners: {
                        afterrender: function (c) {
                            c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.jumpTo('ConditionsEdit', this.parentVM.conditionsEdit.gluModel);
                            }, this);                       
                        }.bind(this)
                    }
                }]
            }, '->', {
				xtype: 'button',
				tooltip: this.localize('resizeFullscreen'),
				iconCls: 'rs-block-button icon-resize-full',
				isResizeMaxBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['SEVERITYCONDITION']);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				tooltip: this.localize('resizeNormalscreen'),
				iconCls: 'rs-block-button icon-resize-small',
				hidden: true,
				isResizeNormalBtn: true,
				margin: '0 20',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeNormalSection(this);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				iconCls: 'rs-block-button icon-chevron-sign-up',
				isCollapseBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.set('collapsed', true);
						}, this);
					}.bind(this)
				}
			}];
            if(this.rootVM.getAllATPropertiesInProgress){
                var checkingForATProperties = setInterval(function(){
                    if(!this.rootVM.getAllATPropertiesInProgress){
                        var severityCategoryCount = 4;
                        var items = this.items;
                        
                        for (var i = 0; i < severityCategoryCount; i++) {
                            items[i].updateParameters({
                                PROPERTY : this.rootVM.allATProperties
                            });
                        }                       
                        clearInterval(checkingForATProperties);
                    }
                }.bind(this),500)
            }	
        },

        jumpTo: function(nextBlockName, nextSection){
			this.parentVM.severitiesAndConditionsEditSectionExpanded = !this.collapsed;
			this.parentVM.severitiesAndConditionsEditSectionMaximized = this.isSectionMaxSize;
			this.parentVM.collapseAndHideSection(this);
			this.parentVM.showAndExpandThisSection(nextSection);
        },
        
        reclassifyExpression : function(expression){
            for(var i = 0; i < this.items.length; i++){
                if(this.items[i].category === expression.category){
                    this.items[i].updateCategoryForExpression(expression);
                }
            }
        }, 

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden) {
					setTimeout(function() {
						if (this.parentVM.severitiesAndConditionsEditSectionExpanded) {
							this.parentVM.severitiesAndConditionsEditSectionExpanded = null;
							this.set('collapsed', false);
						}
						if (this.parentVM.severitiesAndConditionsEditSectionMaximized) {
							this.parentVM.severitiesAndConditionsEditSectionMaximized = null;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['SEVERITYCONDITION']);
						}
					}.bind(this), 10);
				}
			}
		},
               
        close: function (btn) {
            this.expShowAndValid = false;
            this.traverse(this.setFormValid);

            if (this.expShowAndValid) {
                var box = this.message({
                    scope: this,
                    title: this.localize('activityNotCompleted'),
                    msg: this.localize('confirmAddSeverities'),
                    buttons: Ext.MessageBox.YESNO,
                    buttonText: {
                        yes: this.localize('confirmYes'),
                        no: this.localize('confirmNo')
                    },
                    fn: function(btn) {
                        if (btn === 'yes') {
                            this.traverse(this.applyAdd);
                        }
                        //this.blockModel.close('SeveritiesAndConditionsRead');
						this.doClose();
                    }
                });
            } else {
                //this.blockModel.close('SeveritiesAndConditionsRead');
				this.doClose();
            }
        },


		closeFlag: false,
		doClose: function () {
			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		},

        applyAdd: function(exp) {
            if (exp.form && !exp.form.isHidden() && exp.form.isValid()) {
                var btn = exp.form.down('#doneBtn');
                btn? btn.getEl().dom.click(): null;
            }
        },

        setFormValid: function(exp) {
            if (exp.form && !exp.form.isHidden() && exp.form.isValid()) {
                this.expShowAndValid = true;
            }
        },

        traverseAndApply: function(blk, fn, blkName, args) {
            if (blkName) {
                var items = blk[blkName];
                for (var i=0; i<items.length; i++) {
                    args? fn.apply(this, [items.getAt(i)].concat(args)): fn.apply(this, [items.getAt(i)].concat(fn));
                }
            } else {
                fn.apply(this, blk);
            }
        },

        traverse: function(applyFn) {
            for (var i=0; i<this.items.length; i++) {
                this.traverseAndApply(this.items[i], this.traverseAndApply, 'blocks', [applyFn, 'expressions']);
            }
        }		
    },

    getDataPaths: function () {
        return [{
            filter: function (expression) { return expression.expressionType === 'severity'; },
            path:'data.resolveActionInvoc.expressions'
        }];
    },

    getData: function () {
        var severities = [],
            items = this.gluModel.items,
            pluralMap = {
                INPUT: 'INPUTS',
                OUTPUT: 'OUTPUTS',
                FLOW: 'FLOWS',
                PARAM: 'PARAMS',
                PROPERTY: 'PROPERTIES',
                WSDATA: 'WSDATA',
                CONSTANT: 'CONSTANT'
            };

        for (var i = 0; i < items.length; i++) {
            var serveritiesGroup = items[i],
                data = serveritiesGroup.getData(this.gluModel.expressionType);
            
            for (var j = 0; j < data.length; j++) {
                var e = data[j];
                e.leftOperandType = pluralMap[e.leftOperandType] || e.leftOperandType;
                e.rightOperandType = pluralMap[e.rightOperandType] || e.rightOperandType;
            }

            severities.push.apply(severities, data);
        }

        return [severities];
    },

    load: function (expressions) {
        var g = this.gluModel,
			items = g.items,
            singularMap = {
            INPUTS: 'INPUT',
            OUTPUTS: 'OUTPUT',
            FLOWS: 'FLOW',
            PARAMS: 'PARAM',
            PROPERTIES: 'PROPERTY',
            WSDATA: 'WSDATA',
            CONSTANT: 'CONSTANT'
        };

        for (var i = 0; i < items.length; i++) {
            items[i].clear();
        }

        var itemLevelMap = [3, 2, 1, 0],
            orderMap = [[], [] ,[] , []];            

        // generate order plan
        for (var i = 0; i < expressions.length; i++) {
            var e = expressions[i],
                itemIndex = itemLevelMap[e.resultLevel];

            e.leftOperandType = singularMap[e.leftOperandType] || e.leftOperandType;
            e.rightOperandType = singularMap[e.rightOperandType] || e.rightOperandType;               
            orderMap[itemIndex].push(e);
        }

        // eliminate gaps in order and load as we process the expressions.
        for (var i = 0; i < orderMap.length; i++) {
            var t = orderMap[i];
            t.sort(function (a, b) {
                var result = 0;

                if (a.order < b.order) {
                    result = -1;
                } else if (a.order > b.order) {
                    result = 1;
                }

                return result;
            });

            if (t.length > 0) {
                var reducedOrder = 0;
                var lastOrderEncountered = t[0].order;

                for (var j = 0; j < t.length; j++) {
                    var e = t[j];

                    if (e.order > lastOrderEncountered) {
                        lastOrderEncountered = e.order;
                        reducedOrder++;
                    }

                    var itemIndex = itemLevelMap[e.resultLevel];
                    e.isRemoveHidden = false;
                    e.isOrHidden = false;
                    items[itemIndex].load(e, reducedOrder);
                }
            }                
        }

        for (var i = 0; i < items.length; i++) {
            items[i].enableAdd();
        }		
    },

    processDataChange : function(data){
        var severityCategoryCount = 4;
        var items = this.gluModel.items;
        
        for (var i = 0; i < severityCategoryCount; i++) {
            items[i].updateParameters(data);
        }     
    }   
});
