glu.defModel('RS.actiontaskbuilder.ExpressionBlock', {
	key: 0,
	allParameters : {},
	isReadOnly: false,
	category: 0,
	expressions: {
		mtype: 'list'

	},
	init: function () {
		this.key = new Date().getTime();
	},

	initBlock: function (readOnly) {
		this.isReadOnly = !!readOnly;
	},

	checkExpressionCategory: function(expression){
		if(this.parentVM.isCategoryForExpressionChanged(expression.category)){
			//Copy to new location then remove.			
			this.parentVM.reclassifyExpression(expression);
			this.remove(expression);
		}
	},

	updateCategoryForExpression: function(expression, order){
		var expressionConfig = expression.getData(expression.expressionType, order);
		expressionConfig.category = this.category;
		var expression = this.add(expressionConfig, 0);
		expression.showExpression();
		this.addEmptyBlock();
	},

	getData: function (expressionType,order) {
		var expressions = this.expressions.toArray(),
			data = [],
			indexOfLastExpression = expressions.length - 1;

		if (!this.isReadOnly) {
			indexOfLastExpression--;
		}

		for (var i = 0; i <= indexOfLastExpression; i++) {
			data.push(expressions[i].getData(expressionType, order));
		}

		return data;
	},

	addEmpty: function () {
		var expression = this.model('RS.actiontaskbuilder.Expression', {
			criteria: 'ANY',
			leftOperandType: null,
			leftOperandName: null,
			operator: null,
			rightOperandType: null,
			rightOperandName: null,
			isRemoveHidden: true,
			isOrHidden: true,
			isAddHidden: false,
			isExpressionHidden: true,
			isFormHidden: true,
			category : this.category
		});
		expression.initExpression(this.allParameters, this.properties, this.isReadOnly);
		this.expressions.add(expression);
	},

	add: function (expressionConfig, position) {
		var expression = this.model('RS.actiontaskbuilder.Expression', expressionConfig);
		expression.set('category', expressionConfig.resultLevel);
		expression.initExpression(this.allParameters, this.properties, this.isReadOnly);

		if (position || position === 0) {
			this.expressions.insert(position, expression);
		} else {
			this.expressions.add(expression);
		}

		return expression;
	},	

	remove: function (expression) {		
		this.expressions.remove(expression);
		
		if (this.count() === 1) {
			this.parentVM.removeBlock(this);
		}
	},

	clear: function(){
		this.expressions.removeAll();
	},
	addEmptyBlock: function () {
		var block = this.parentVM.addBlock();

		if (!block.isReadOnly) {
			block.addEmpty();
		}
	},

	updateParameters: function (allParameters) {
		this.allParameters = allParameters;
		this.expressions.foreach(function(expression){
			expression.updateParameters(allParameters);
		},this)
	},

	updateProperties: function (allProperties) {
		this.properties = allProperties;	
		var expressions = this.expressions.toArray();

		for (var i = 0; i < expressions.length; i++) {
			expressions[i].markPropertiesUpdated();
			expressions[i].updateProperties(allProperties);
		}
	},

	isLastExpression: function (expression) {
		var count = this.count();
		return count >= 0 && count - 1 === this.expressions.indexOf(expression);
	},

	isLastBlock: function () {
		return this.parentVM.isLastBlock(this);
	},

	isEmpty: function () {
		return this.count() === 0;
	},

	count: function () {
		return this.expressions.length;
	}
});