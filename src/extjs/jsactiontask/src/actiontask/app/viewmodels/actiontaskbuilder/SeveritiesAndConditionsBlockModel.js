Ext.define('RS.actiontaskbuilder.SeveritiesAndConditionsBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'SeveritiesAndConditions',
        classList: 'block-model',
		collapsed: true,
		hidden: false,
        title: '',
        header: {
            titlePosition: 0,
            defaults: {
                margin: '0'
            },
            items: []
        },

        severityItems: [],
        conditionItems: [],
        isSeveritiesMessageHidden: true,
        isConditionsMessageHidden: true,
        isCriticalSeverityHidden: true,
        isSevereSeverityHidden: true,
        isWarningSeverityHidden: true,
        isGoodSeverityHidden: true,
        isBadConditionHidden: true,
        isGoodConditionHidden: true,

        postInit: function (blockModel) {
            this.blockModel = blockModel;
            var critical = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            critical.initContainer(3, '~~criticalTitle~~', '#c00', true);
            var severe = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            severe.initContainer(2, '~~severeTitle~~', '#f80', true);
            var warning = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            warning.initContainer(1, '~~warningTitle~~', '#f2cc11', true);
            var good = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            good.initContainer(0, '~~goodTitle~~', '#0b0', true);
            this.severityItems.push(critical);
            this.severityItems.push(severe);
            this.severityItems.push(warning);
            this.severityItems.push(good);

            var bad = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            bad.initContainer(1, '~~badTitle~~', '#c00', true);
            var good = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            good.initContainer(0, '~~goodTitle~~', '#0b0', true);
            this.conditionItems.push(bad);
            this.conditionItems.push(good);            
        },

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden && this.parentVM.embed) {
					var severitiesData = this.parentVM.severitiesEdit.getData(),
						conditionsData = this.parentVM.conditionsEdit.getData();

					var expressions = severitiesData[0];

					for (var i=0, l=conditionsData[0].length; i<l; i++) {
						var conditions = conditionsData[0][i];
						expressions.push(conditions);
					}
					
					this.parentVM.severitiesAndConditionsRead.load(expressions);
				}
			}
		}
    },

    getDataPaths: function () {
        return ['data.resolveActionInvoc.expressions'];
    },

    load: function (expressions) {
        var severityItems = this.gluModel.severityItems,
            conditionItems = this.gluModel.conditionItems;

        if (expressions) {
            var singularMap = {
                INPUTS: 'INPUT',
				OUTPUTS: 'OUTPUT',
                FLOWS: 'FLOW',
                PARAMS: 'PARAM',
                PROPERTIES: 'PROPERTY',
                WSDATA: 'WSDATA',
                CONSTANT: 'CONSTANT'
            };

            for (var i = 0; i < expressions.length; i++) {
                var e = expressions[i];
                e.leftOperandType = singularMap[e.leftOperandType] || e.leftOperandType;
                e.rightOperandType = singularMap[e.rightOperandType] || e.rightOperandType;
            }

            this.loadWithoutGaps(expressions, 'severity', severityItems, [3, 2, 1, 0]);
            this.loadWithoutGaps(expressions, 'condition', conditionItems, [1, 0]);
        }

        this.set('isSeveritiesMessageHidden', 
            severityItems[0].count() > 0 ||
            severityItems[1].count() > 0 ||
            severityItems[2].count() > 0 ||
            severityItems[3].count() > 0);

        var classes = ['block-model'];

        this.set('isConditionsMessageHidden', conditionItems[0].count() > 0 || conditionItems[1].count() > 0)
            .set('isCriticalSeverityHidden', severityItems[0].count() === 0)
            .set('isSevereSeverityHidden', severityItems[1].count() === 0)
            .set('isWarningSeverityHidden', severityItems[2].count() === 0)
            .set('isGoodSeverityHidden', severityItems[3].count() === 0)
            .set('isBadConditionHidden', conditionItems[0].count() === 0)
            .set('isGoodConditionHidden', conditionItems[1].count() === 0)
            .set('classList', classes);
    },

    loadWithoutGaps: function (expressions, expressionType, items, itemLevelMap) {
        for (var i = 0; i < items.length; i++) {
            items[i].clear();
            //Hide all level 1st then enable only the level that has expression later.
            items[i].set('isHidden', true);
        }

        var orderMap = [[], [] ,[] , []];

        // generate order plan
        for (var i = 0; i < expressions.length; i++) {            
            var e = expressions[i];

            if (e.expressionType === expressionType) {
                var itemIndex = itemLevelMap[e.resultLevel];
                orderMap[itemIndex].push(e);
            }
        }

        // eliminate gaps in order and load as we process the expressions.
        for (var i = 0; i < orderMap.length; i++) {
            var t = orderMap[i];
            t.sort(function (a, b) {
                var result = 0;

                if (a.order < b.order) {
                    result = -1;                    
                } else if (a.order > b.order) {
                    result = 1;
                }

                return result;
            });

            if (t.length > 0) {
                var reducedOrder = 0;
                var lastOrderEncountered = t[0].order;

                for (var j = 0; j < t.length; j++) {
                    var e = t[j];

                    if (e.order > lastOrderEncountered) {
                        lastOrderEncountered = e.order;
                        reducedOrder++;
                    }

                    var itemIndex = itemLevelMap[e.resultLevel];
                    e.isRemoveHidden = true;
                    e.isOrHidden = j === t.length - 1;
                    items[itemIndex].load(e, reducedOrder);
                }
            }
        }
    },

    modifyByNotification: function (parameters) {
        var severityItems = this.gluModel.severityItems,
            conditionItems = this.gluModel.conditionItems;
            severityCategoryCount = 4,
            conditionCategoryCount = 2;
        
        for (var i = 0; i < severityCategoryCount; i++) {
            severityItems[i].updateParameters(parameters);
        }

        for (var i = 0; i < conditionCategoryCount; i++) {
            conditionItems[i].updateParameters(parameters);
        }
    }    
});
