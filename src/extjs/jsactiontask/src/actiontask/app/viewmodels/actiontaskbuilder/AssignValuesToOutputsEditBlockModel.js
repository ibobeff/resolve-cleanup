Ext.define('RS.actiontaskbuilder.AssignValuesToOutputsEditBlockModel', (function () {
	var pluralMap = {
		INPUT: 'INPUTS',
		OUTPUT: 'OUTPUTS',
		FLOW: 'FLOWS',
		PARAM: 'PARAMS',
		WSDATA: 'WSDATA',
		CONSTANT: 'CONSTANT'
	};
	var singularMap = {
		INPUTS: 'INPUT',
		OUTPUTS: 'OUTPUT',
		FLOWS: 'FLOW',
		PARAMS: 'PARAM',
		WSDATA: 'WSDATA',
		CONSTANT: 'CONSTANT'
	};

	function isDuplicate (a, b) {
		return a.outputType === b.outputType
			&& a.outputName === b.outputName
			&& a.sourceType === b.sourceType
			&& a.sourceName === b.sourceName;
	}

	function isDuplicateAssignment (a, b) {
		return a.outputType === b.outputType
			&& a.outputName === b.outputName;
	}	
	return {
		extend: 'RS.common.blocks.BlockModel',
		modelConfig: {
			mixins: ['QuickAccessHelpers'],
			blockModel: null,
			blockModelClass: 'AssignValuesToOutputs',
			title: '',
			hidden: true,
			collapsed: true,
			editBlock: true,
			duplicateAssignmentCounter : 0,
			header: {
				titlePosition: 0,
				items: []
			},			
			mappingStore: null,
			valuesCount: 0,
			gridIsEmpty: false,
			outputTypesStore: {
				mtype : 'store',
				fields : ['name']				
			},		
			sourceTypesStore: {
				mtype : 'store',
				fields : ['name']			
			},		
			inputParametersStore: {
				mtype : 'store',
				fields : ['name']
			},
			outputParametersStore: {
				mtype : 'store',
				fields : ['name']
			},
			allParameters : {
				INPUT : [],
				OUTPUT : [],
				FLOW : [],
				WSDATA : []			
			},
			detail: {
				mtype: 'AssignValueToOutputDetail'
			},		

			postInit: function (blockModel) {
				this.blockModel = blockModel;
				this.set('mappingStore', Ext.create('Ext.data.Store',{ 
					fields: ['outputType', 'outputName', 'sourceType', 'sourceName','isDuplicateAssignment'],
					listeners: {
						datachanged: function () {
							this.notifyDirty();
						}.bind(this),
						update: function () {
							this.notifyDirty();
						}.bind(this)	
					}
				}));

				var outputTypes = [{
					name : 'OUTPUT'
				},{
					name : 'FLOW'
				},{
					name : 'PARAM'
				},{
					name : 'WSDATA'
				}];

				var sourceTypes =  [{
					name : 'INPUT'
				},{
					name : 'FLOW'
				},{
					name : 'PARAM'
				},{
					name : 'WSDATA'
				},{
					name : 'CONSTANT'
				}];

				this.outputTypesStore.loadData(outputTypes);
				this.detail.setOutputTypes(outputTypes);
				this.sourceTypesStore.loadData(sourceTypes);
				this.detail.setSourceTypes(sourceTypes);					
			},
			notifyDirty: function() {
				this.blockModel.notifyDirty();
			},

			validateOnEdit : function(context){
				var field = context.field;
				if(field == 'outputType' || field == 'outputName'){
					var duplicateAssignmentCounter = this.duplicateAssignmentCounter;
					var oldDuplicatedIndexes = [];
					var newDuplicatedIndexes = [];
					var store = context.store;
					var range = store.getRange();
					var currentRecordIndex = context.rowIdx;
					var oldRecord = context.record.data;
					var newRecord = Ext.apply({}, oldRecord);
					newRecord[field] = context.value;

					//Check for dupplicate assignment
					for (var i = 0; i < range.length; i++) {
						var d = range[i].data;
						if(i == currentRecordIndex)
							continue;
						if (isDuplicate(d, newRecord)) {							
							this.message({
								scope: this,
								title: this.localize('invalidAssignmentTitle'),
								msg: this.localize('assignmentAlreadyExists'),
								buttons: Ext.MessageBox.OK
							});
							return false;
						} else if (isDuplicateAssignment(d, oldRecord)) {							
							oldDuplicatedIndexes.push(i);		
						} else if (isDuplicateAssignment(d, newRecord)) {							
							newDuplicatedIndexes.push(i);		
						}
					}

					if(oldDuplicatedIndexes.length == 1){
						range[oldDuplicatedIndexes[0]].set('isDuplicateAssignment', false);
						duplicateAssignmentCounter--;
					}

					if(newDuplicatedIndexes.length > 0)
						range[currentRecordIndex].set('isDuplicateAssignment', true);					
					else
						range[currentRecordIndex].set('isDuplicateAssignment', false);		
					
					if(newDuplicatedIndexes.length == 1){
						range[newDuplicatedIndexes[0]].set('isDuplicateAssignment', true);
						duplicateAssignmentCounter++;
					}
					this.set('duplicateAssignmentCounter',duplicateAssignmentCounter);
				}
			},
			displayInvalidNameMsg : function(msg){
				clientVM.message(this.localize('invalidAssignmentTitle'), this.localize(msg));		
			},
			loadParameterStore : function(fieldName, fieldValue){
				var store = fieldName == 'outputName' ? this.outputParametersStore : this.inputParametersStore;
				if(fieldValue == 'INPUT')
					store.loadData(this.allParameters['INPUT']);
				else if(fieldValue == 'OUTPUT')
					store.loadData(this.allParameters['OUTPUT']);
				else if(fieldValue == 'FLOW')
					store.loadData(this.allParameters['FLOW']);
				else if(fieldValue == 'WSDATA')
					store.loadData(this.allParameters['WSDATA']);
			},
			addAssignment: function (m) {
				var isValid = true;				
				var	range = this.mappingStore.getRange();
				var duplicateAssignmentIndexes = [];
			
				for (var i = 0; i < range.length; i++) {
					var d = range[i].data;

					if (isDuplicate(d, m)) {
						isValid = false;
						this.message({
							scope: this,
							title: this.localize('invalidAssignmentTitle'),
							msg: this.localize('assignmentAlreadyExists'),
							buttons: Ext.MessageBox.OK
						});
						break;
					} else if (isDuplicateAssignment(d, m)) {					
						range[i].set('isDuplicateAssignment', true);
						duplicateAssignmentIndexes.push(i);		
					}
				}
				
				//New duplicate assignment
				if(duplicateAssignmentIndexes.length == 1)
					this.set('duplicateAssignmentCounter', this.duplicateAssignmentCounter + 1);
				if (isValid) {
					var newRecords = this.mappingStore.add(m);
					if(duplicateAssignmentIndexes.length > 0)
						newRecords[0].set('isDuplicateAssignment', true);					
					var count = this.mappingStore.getCount();
					this.set('gridIsEmpty', count === 0);
					this.set('valuesCount', count);
				}

				return isValid;
			},

			removeAssignment: function (record) {
				if(record.get('isDuplicateAssignment')){
					var indexOfRecord = this.mappingStore.indexOf(record);
					var range = this.mappingStore.getRange();
					var duplicateAssignmentIndexes = [];
					for(var i = 0; i < range.length; i++){
						var currentData = range[i].getData();
						if(i != indexOfRecord && isDuplicateAssignment(currentData, record.getData()))
							duplicateAssignmentIndexes.push(i);
					}
					//Only 1 pair of duplicated. Update that record then decrease counter.
					if(duplicateAssignmentIndexes.length == 1){
						var duplicatedRecord = this.mappingStore.getAt(duplicateAssignmentIndexes[0]);
						duplicatedRecord.set('isDuplicateAssignment', false);
						this.set('duplicateAssignmentCounter', this.duplicateAssignmentCounter - 1);
					}
				}
				this.mappingStore.remove(record);
				var count = this.mappingStore.getCount();
				this.set('gridIsEmpty', count === 0);
				this.set('valuesCount', count);
			},
			hasDuplicateAssignment$ : function(){
				return this.duplicateAssignmentCounter > 0;
			},
			closeFlag: false,
			close: function () {
				if (this.isSectionMaxSize) {
					this.parentVM.resizeNormalSection(this);
				}
				this.closeFlag = true;
				this.set('collapsed', true);
			}
		},

		getData: function (getDataForServer) {
			var g = this.gluModel;
			var assignments = [];

			//Map source to plural
			g.mappingStore.each(function(record){
				var data = record.data;
				var oldRecord = Ext.apply({}, record.raw);
				assignments.push(Ext.apply(oldRecord, {
					outputType : pluralMap[data['outputType']],
					outputName : data['outputName'],
					sourceType : pluralMap[data['sourceType']],
					sourceName : data['sourceName'],
				}));			
			})
				
			return [assignments];
		},

		getDataPaths: function () {
			return ['data.resolveActionInvoc.outputMappings'];
		},

		load: function (outputMappings) {
			var g = this.gluModel;

			for(var i = 0; i < outputMappings.length; i++){
				var entry = outputMappings[i];
				entry['outputType'] = singularMap[entry['outputType']];
				entry['sourceType'] = singularMap[entry['sourceType']];
			}
			g.mappingStore.loadData(outputMappings);

			var range = g.mappingStore.getRange();
			var duplicateAssignmentCounter = 0;
			for (var i = 0; i < range.length; i++) {
				var duplicateAssignmentFound = false;
				var d = range[i];
				//If already mark then continue
				if(d.get('isDuplicateAssignment'))
					continue;
				else{
					for (var j = i + 1; j < range.length; j++) {
						var other = range[j];

						if (isDuplicateAssignment(d.getData(), other.getData())) {						
							other.set('isDuplicateAssignment',true);
							duplicateAssignmentFound = true;							
						}
					}
					if(duplicateAssignmentFound){
						d.set('isDuplicateAssignment',true);
						duplicateAssignmentCounter++;
					}
				}				
			}
			this.set('duplicateAssignmentCounter', duplicateAssignmentCounter);
			
			var count = g.mappingStore.getCount();
			g.set('gridIsEmpty', count === 0)
			g.set('valuesCount', count);
		},
		processDataChange : function(data){
			var g = this.gluModel;
			var detail = g.detail;
			for(var type in data){
				var processedData = [];
				for(var i = 0; i < data[type].length; i++){
					processedData.push({
						name : data[type][i]
					})
				}
				g.allParameters[type] = processedData;
				detail.allParameters[type] = processedData;
			}
			//Repopulate already selected type for each field.
			detail.loadParameterStore(detail.outputParametersStore, detail.outputType);
			detail.loadParameterStore(detail.inputParametersStore, detail.sourceType);
		}	  
	};
})());
