Ext.define('RS.actiontaskbuilder.ParametersOutputEditBlockModel', {
	extend: 'RS.common.blocks.BlockModel',
	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Parameters',
        title: '',
		hidden: true,
		collapsed: true,
		editSubBlock: true,
		stopPropagation: false,

        header: {
	 		xtype: 'toolbar',
	    	margin: 0,
	    	padding: 0,
	    	items: []
		},	

		outputParametersStore: null,
		hasOutputParameters: false,

		detail: {
			mtype: 'ParameterOutputDetail'
		},		
		allParameters : {
			OUTPUT : [], 
			FLOW : [],
			WSDATA : []
		},
		postInit: function (blockModel, context) {
			this.blockModel = blockModel;
			this.detail.setAddOutputButtonTitle(context.localize('addOutput'));
			this.set('outputParametersStore', new RS.common.data.FlatStore({
				fields: ['id', 'uname', 'udescription', 'udefaultValue', 'uencrypt', 'urequired', 'utype'],
				listeners: {
					datachanged: function () {
						this.notifyDirty();
						this.notifyDataChange();
					}.bind(this),
					update: function () {
						this.notifyDirty();
						this.notifyDataChange();
					}.bind(this)						
				}
			}));
			this.header.items = [{
				xtype: 'component',
				padding: '0 0 0 15',
				cls: 'x-header-text x-panel-header-text-container-default',
				html: this.localize('parametersTitle'),
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
						}, this);
					}.bind(this)
				}
			}, {
	        	xtype: 'container',
	        	autoEl: 'ol',
	        	cls: 'breadcrumb flat',
	        	margin: 0,
	        	padding: 0,
	        	items: [{
		    		xtype: 'component',
		    		autoEl: 'li',		    		
		    		html: this.localize('inputParametersEditTitle'),
		    		listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.jumpTo('ParametersInputEdit', this.parentVM.parametersInputEdit.gluModel);
				            }, this);						
						}.bind(this)
		    		}
		    	}, {
		    		xtype: 'component',
		    		autoEl: 'li',
		    		cls: 'active',
		    		html: this.localize('outputParametersEditTitle'),
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
							}, this);
						}.bind(this)
					}
		    	}]
	        }, '->', {
				xtype: 'button',
				tooltip: this.localize('resizeFullscreen'),
				iconCls: 'rs-block-button icon-resize-full',
				isResizeMaxBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['PARAMETERS']);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				tooltip: this.localize('resizeNormalscreen'),
				iconCls: 'rs-block-button icon-resize-small',
				hidden: true,
				isResizeNormalBtn: true,
				margin: '0 20',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeNormalSection(this);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				iconCls: 'rs-block-button icon-chevron-sign-up',
				isCollapseBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.set('collapsed', true);
						}, this);
					}.bind(this)
				}
			}];
		},
		jumpTo: function(nextBlockName, nextSection){
			this.parentVM.parametersEditSectionExpanded = !this.collapsed;
			this.parentVM.parametersEditSectionMaximized = this.isSectionMaxSize;
			this.parentVM.collapseAndHideSection(this);
			this.parentVM.showAndExpandThisSection(nextSection);
		},
		notifyDirty: function() {
			this.blockModel.notifyDirty();			
		},
		notifyDataChange : function(){
			if(this.outputParametersStore){			
				//console.log("broadcasting event....");
				this.blockModel.notifyDataChange(Ext.apply(this.allParameters, {
					OUTPUT : this.outputParametersStore.collect('uname')				
				}));
			}
		},
		// called by detail.
		addOutputToList: function(output, ignoreMsg) {
			if (this.outputParametersStore.findExact('uname', output.uname) === -1) {
				this.outputParametersStore.add(output);		
				this.set('hasOutputParameters', this.outputParametersStore.count() > 0);
			} else if(!ignoreMsg) {
				this.displayDuplicateNameMsg();						
			}
		},
		displayDuplicateNameMsg : function(){
			clientVM.message(this.localize('invalidParameterTitle'),this.localize('parameterAlreadyExists'));		
		},
		removeParameter: function (p) {
			if (this.parentVM.embed && this.rootVM.automation.activeItem && this.rootVM.automation.activeItem.activeItem) {
				var outputParamName = p.get('uname');
				this.rootVM.automation.activeItem.activeItem.checkTargetParamDependency(outputParamName, function() {
					this.doRemoveParameter(p);
				}.bind(this));
			} else {
				this.doRemoveParameter(p);
			}
		},

		doRemoveParameter: function (p) {
			if(this.validateParserOutput(p)){
				var store = this.outputParametersStore;
				var count = store.count();

				if (count === 1) {
					store.removeAll();
					this.set('hasOutputParameters', false);
				} else {
					store.remove(p);
					this.set('hasOutputParameters', true);
				}
			}			
		},
		//Prevent remove variable created from parser.
		validateParserOutput : function(record){
			if(this.blockModel.cachedParserOutputStore.find('name', record.get('uname')) != -1){
				clientVM.message(this.localize('parserVariableRestrictionTitle'),this.localize('parserVariableRestrictionMsg'));		
				return false;
			}
			return true;
		},
		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden) {
					setTimeout(function() {
						if (this.parentVM.parametersEditSectionExpanded) {
							this.parentVM.parametersEditSectionExpanded = null;
							this.set('collapsed', false);
						}
						if (this.parentVM.parametersEditSectionMaximized) {
							this.parentVM.parametersEditSectionMaximized = null;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['PARAMETERS']);
						}
					}.bind(this), 10);
				}
			}
		},

		closeFlag: false,
		close: function () {
			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		}
	},
	//Processing output from parser
	processDataChange : function(variableDataArray){	
		//console.log("processing data....");
		var g = this.gluModel;
		var outputStore = g.outputParametersStore;
		outputStore.suspendEvents();
		var updatedOn = new Date().getTime();
		g.allParameters['FLOW'] = [];
		g.allParameters['WSDATA'] = [];	
		for(var i = 0; i < variableDataArray.length; i++){
			var containerType = variableDataArray[i]['containerType'];
			var variableName = variableDataArray[i]['name'];
			if(containerType == 'OUTPUT'){
				//Process all output and put to cached store first
				this.processOutputVariable(variableDataArray[i], updatedOn);
			}
			else
				g.allParameters[containerType].push(variableName);
		}		
		this.finalizeOutputVariable(updatedOn);	
		outputStore.resumeEvents();
		g.fireEvent('refreshGrid');
		g.notifyDataChange();
	},
	cachedParserOutputStore : Ext.create('Ext.data.Store',{
		mtype : 'store',
		fields : ['name','variableId','updatedOn']
	}),
	processOutputVariable : function(output, updatedOn){
		var g = this.gluModel;
		var outputStore = g.outputParametersStore;
	
		var cachedOutputIndex = this.cachedParserOutputStore.find('variableId', output['variableId']);
		//New Output just add to the store
		if(cachedOutputIndex == -1){
			this.cachedParserOutputStore.add(Ext.apply(output, { updatedOn : updatedOn}));
			g.addOutputToList({
				uname : output['name'],
				udescription : g.localize('outputFromParser'),
				utype : 'OUTPUT'
			}, true)
		}
		//Output has changed. Update it
		else if(this.cachedParserOutputStore.getAt(cachedOutputIndex).get('name') != output['name']){
			var cachedRecord = this.cachedParserOutputStore.getAt(cachedOutputIndex);
			var oldName = cachedRecord.get('name');
			cachedRecord.set('name',output['name']);
			cachedRecord.set('updatedOn',updatedOn);
			var recordIndex = outputStore.find('uname', oldName);
			if(recordIndex != -1)
				outputStore.getAt(recordIndex).set('uname', output['name']);
		}
		else
			this.cachedParserOutputStore.getAt(cachedOutputIndex).set('updatedOn',updatedOn);		
	},
	finalizeOutputVariable : function(updatedOn){
		var g = this.gluModel;
		var outputStore = g.outputParametersStore;
		var deletedCachedRecords = [];
		this.cachedParserOutputStore.each(function(cachedRecord){
			//Record that has not recently updated mean it's no longer exist.
			if(cachedRecord.get('updatedOn') != updatedOn){
				var r = outputStore.findRecord('uname', cachedRecord.get('name'));
				if(r)
					outputStore.remove(r);
				deletedCachedRecords.push(cachedRecord);
			}			
		},this)
		for(var i = 0; i < deletedCachedRecords.length; i++){
			this.cachedParserOutputStore.remove(deletedCachedRecords[i]);
		}
		g.set('hasOutputParameters', outputStore.count() > 0);	
	},
	getDataPaths: function () {
		return [{
			filter: function (p) {
				return p.utype === 'OUTPUT';
			},
			path: 'data.resolveActionInvoc.resolveActionParameters'
		}];
	},

	getData: function () {
		return [this.gluModel.outputParametersStore.getData()];
	},

	load: function (outputParameters) {
		var g = this.gluModel,
			store = g.outputParametersStore;

		for (var i = 0; i < outputParameters.length; i++) {
			var p = outputParameters[i];
			p.uname = p.uname || '';
			p.udescription = p.udescription || '';
			p.udefaultValue = p.udefaultValue || '';
			p.uencrypt = p.uencrypt || false;
			p.urequired = p.urequired || false;
			p.uorder = p.uorder || 0;
		}
		store.loadData(outputParameters);
		this.set('hasOutputParameters', store.count() > 0);
	}
});
