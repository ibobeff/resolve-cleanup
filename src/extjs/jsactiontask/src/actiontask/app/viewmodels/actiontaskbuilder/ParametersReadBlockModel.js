Ext.define('RS.actiontaskbuilder.ParametersReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Parameters',
		classList: 'block-model',
		collapsed: true,
		hidden: false,
        title: '',
        header: {
            titlePosition: 0,
            defaults: {
                margin: '0'
            },
            items: []
        },

		inputParametersStore: null,
		outputParametersStore: null,
		hasInputParameters: false,
		hasOutputParameters: false,
		unclassified: '',

		postInit: function (blockModel, context) {
			this.blockModel = blockModel;
			this.set('inputParametersStore', new RS.common.data.FlatStore({
				fields: ['id', 'uname', 'udescription', 'udefaultValue', 'uencrypt', 'urequired', 'uprotected', 'ugroupName', 'utype', 'uorder'],
				groupField: 'ugroupName',
				sorters: [{
			    	property: 'value',
			    	direction: 'uorder'
				}]
			}));

			this.set('outputParametersStore', new RS.common.data.FlatStore({
				fields: ['id', 'uname', 'udescription', 'udefaultValue', 'uencrypt', 'urequired', 'utype']
			}));

			this.unclassified = context.localize('unclassified');
		},

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden && this.parentVM.embed) {
					var inputData = this.parentVM.parametersInputEdit.getData(),
						outputData = this.parentVM.parametersOutputEdit.getData();

					var parameters = inputData[0];

					for (var i=0, l=outputData[0].length; i<l; i++) {
						var outParam = outputData[0][i];
						parameters.push(outParam);
					}

					this.parentVM.parametersRead.load(parameters);
				}
			}
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.resolveActionParameters'];
	},

	load: function (parameters) {
		var g = this.gluModel,
			inputStore = g.inputParametersStore,
			outputStore = g.outputParametersStore,
			inputParameters = [],
			outputParameters = [];

		for (var i = 0; i < parameters.length; i++) {
			var p = parameters[i];

			if (p.utype === 'INPUT' || p.utype === null) {
				p.uname = p.uname || '';
				p.udescription = p.udescription || '';
				p.udefaultValue = p.udefaultValue || '';
				p.uencrypt = p.uencrypt || false;
				p.urequired = p.urequired || false;
				p.uprotected = p.uprotected || false;
				p.uorder = p.uorder || 0;

				if (p.ugroupName === null) {
					p.ugroupName = g.unclassified;
				}
				else {
					// HTML tags are not allowed in parameter grouping
					p.ugroupName = Ext.util.Format.stripTags(p.ugroupName);
				}

				inputParameters.push(p);
			} else {
				p.uname = p.uname || '';
				p.udescription = p.udescription || '';
				p.udefaultValue = p.udefaultValue || '';
				p.uencrypt = p.uencrypt || false;
				p.urequired = p.urequired || false;
				p.uorder = p.uorder || 0;
				outputParameters.push(p);
			}
		}

		var classes = ['block-model'];

		inputStore.loadData(inputParameters);
		outputStore.loadData(outputParameters);
		this.set('hasInputParameters', inputStore.count() > 0)
			.set('hasOutputParameters', outputStore.count() > 0)
			.set('classList', classes);
	}
});
