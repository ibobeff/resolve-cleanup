Ext.define('RS.actiontaskbuilder.GeneralEditBlockModel', {
	extend: 'RS.common.blocks.BlockModel',
	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'General',
		hidden: true,
		collapsed: true,
		editBlock: true,
		stopPropagation: false,
		header: {
	 		xtype: 'toolbar',
           	margin: 0,
	    	padding: 0,
	    	items: []
	 	},
        title: '',
		typeStore: null,
		type: 'ASSESS',
		id: '',
		namespace: '',
		name: '',
		menuPath: '/',
		timeout: 300,
		assignedToUserName: '',
		assignedToUName: '',
		assignedTo: null,
		summary: '',
		description: '',
		active: true,
		logResult: true,
		displayInWorksheet: true,
		isExistingActionTask: false,
		isNewActionTask: true,
		isTypeComboHidden: false,
		isProcessFieldHidden: true,
		autoMenuPathEnabled: true,
		jumpToAssignedToIsHidden : true,
		ufullName: '',
		wizardType : '',
		form: null,
		blockWizard: true,
		fields : ['timeout'],
		generalEditTabContrl: false,
		hideShowGeneralEditTab: function(atId) {
			this.set('generalEditTabContrl', !this.generalEditTabContrl);
		},
        validityChange: function (valid) {
        	//When form is not valid, block navigation and set section invalid.
        	this.set('blockWizard', !valid);
        	this.notifyValid(valid);
        },

		postInit: function (blockModel) {
			this.blockModel = blockModel;
			this.set('typeStore', new RS.common.data.FlatStore(['ASSESS', 'REMOTE', 'OS', 'BASH', 'CMD', 'CSCRIPT', 'POWERSHELL']));

			if (typeof this.parentVM.id === "string" && this.parentVM.id.length === 0) {
				var user = clientVM.user,
					fullName = user.fullName.trim(),
					username = '';

				if (fullName.length > 0) {
					username = fullName;
				} else {
					username = user.name.trim();
				}

				this.set('assignedToUserName', username);
				this.set('assignedToUName', user.name);
				this.set('assignedTo', {
					id: user.id,
					udisplayName: username,
					uuserName: user.name
				});
			}

			this.header.items = [{
	        	xtype: 'component',
	        	padding: '0 0 0 15',
	        	cls: 'x-header-text x-panel-header-text-container-default',
	        	html: this.localize('generalTitle'),
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
			            }, this);
					}.bind(this)
	    		}
	        },{
	        	xtype: 'container',
	        	autoEl: 'ol',
	        	cls: 'breadcrumb flat',
	        	margin: 0,
	        	padding: 0,
	        	items: [{
		    		xtype: 'component',
					itemId: 'generalProperties',
		    		autoEl: 'li',
		    		cls: 'active',
		    		html: this.localize('generalPropertiesEditTitle'),
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
							}, this);
						}.bind(this)
					}
		    	}, {
		    		xtype: 'component',
					itemId: 'rolesEdit',
		    		autoEl: 'li',
		    		html: this.localize('rolesEditTitle'),
		    		listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								if (!this.blockWizard) {
				                	this.jumpTo('RolesEdit', this.parentVM.rolesEdit.gluModel);
				            	}
				            }, this);
						}.bind(this),
		    		}
		    	}, {
		    		xtype: 'component',
					itemId: 'optionsEdit',
		    		autoEl: 'li',
					html: this.localize('optionsEditTitle'),
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								if (!this.blockWizard) {
									this.jumpTo('OptionsEdit', this.parentVM.optionsEdit.gluModel);
				            	}
				            }, this);
						}.bind(this)
		    		}
		        }]
	        }, '->', {
				xtype: 'button',
				tooltip: this.localize('resizeFullscreen'),
				iconCls: 'rs-block-button icon-resize-full',
				isResizeMaxBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['GENERAL']);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				tooltip: this.localize('resizeNormalscreen'),
				iconCls: 'rs-block-button icon-resize-small',
				hidden: true,
				isResizeNormalBtn: true,
				margin: '0 20',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeNormalSection(this);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				iconCls: 'rs-block-button icon-chevron-sign-up',
				isCollapseBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.set('collapsed', true);
						}, this);
					}.bind(this)
				}
			}];
		},

		setForm: function (form) {
			this.form = form;
		},

		jumpToAssignedTo: function() {
			clientVM.handleNavigation({
				modelName: 'RS.user.User',
				params: {
					username: this.assignedToUName
				},
				target: '_blank'
			})
		},

		assignedToClicked: function() {
			this.open({
				mtype: 'RS.user.UserPicker',
				pick: true,
				callback: this.assignedToSelected
			})
		},

		assignedToSelected: function(records) {
			var user = records[0].data;
			this.set('assignedTo', user);
			this.set('assignedToUserName', user.udisplayName || (user.ufirstName + ' ' + user.ulastName).trim() || user.uuserName);
			this.set('assignedToUName', user.uuserName);
			this.form.isValid();
		},
		jumpToAssignedToIsHidden$ : function(){
			return !this.assignedToUName;
		},

		periodRegex: /\./g,
		illegalNamespaceCharacters: /[^A-Za-z0-9\./_ ]/g,
		nameIsValid$: function() {
	        return RS.common.validation.TaskName.test(this.name) ? true : this.localize('invalidName');
	    },
	    namespaceIsValid$: function() {
	        return RS.common.validation.TaskNamespace.test(this.namespace) ? true : this.localize('invalidNamespace');
	    },
		namespaceChangeHandler: function (field, newValue) {
			this.setLegalNamespace(newValue);
			field.checkChange();
		},

		saveBtnIsEnabled$: function() {
			var newActionTaskIsValid = this.nameIsValid == true && this.namespaceIsValid == true;
			if (newActionTaskIsValid) {
				this.parentVM.set('isValid', true);
			} else {
				this.parentVM.set('isValid', false);
			}
			return newActionTaskIsValid;
		},

		namespaceBlurHandler: function () {
			this.setLegalNamespace(this.namespace);
		},

		setLegalNamespace: function (namespace) {		
			if (namespace === '' && this.menuPath === '/') {
				this.autoMenuPathEnabled = true;
			}
			this.generateMenuPath();
		},

		generateMenuPath: function () {
			if (this.autoMenuPathEnabled) {
				var menuPath = this.namespace.replace(this.periodRegex, '/');

				if (menuPath.length === 0 || menuPath[0] !== '/') {
					menuPath = '/' + menuPath;
				}

				this.set('menuPath', menuPath);
			}
		},

		menuPathChangeHandler: function (newValue) {
			this.setLegalMenuPath(newValue);
		},

		menuPathBlurHandler: function (field) {
			this.setLegalMenuPath(this.menuPath);
			field.validate();
		},

		setLegalMenuPath: function (menuPath) {
			if (this.namespace === '' && menuPath === '/') {
				this.autoMenuPathEnabled = true;
			} else {
				this.autoMenuPathEnabled = false;
			}

			if (menuPath.length === 0) {
				menuPath = '/';
			} else if (menuPath[0] !== '/') {
				menuPath = '/' + menuPath;
			}

			menuPath = menuPath.replace(this.illegalNamespaceCharacters, '').replace(this.periodRegex, '/');

			if (menuPath !== this.menuPath) {
				this.set('menuPath', menuPath);
			}
		},

		notify_dirty : {
			on: ['typeChanged','activeChanged','logResultChanged','displayInWorksheetChanged','assignedToChanged','menuPathChanged'],
			action: function () {
				this.notifyDirty();
			}
		},

		when_type_changed: {
			on: ['typeChanged'],
			action: function() {
				if (this.type === 'ASSESS') {
					this.parentVM.set('contentSectionVisible', false);
				} else {
					this.parentVM.set('contentSectionVisible', true);
					if (this.parentVM.isMaxScreenSize) {
						this.parentVM.blockContainer.hideBlocks('contentBlock');
					}
				}
			}
		},

		notifyDirty: function() {
			this.blockModel.notifyDirty();
		},

		notifyValid: function (valid) {
			this.blockModel.notifyValid(valid);
		},

		jumpTo: function(nextBlockName, nextSection){
			this.parentVM.generalEditSectionExpanded = !this.collapsed;
			this.parentVM.generalEditSectionMaximized = this.isSectionMaxSize;
			if (this.id) {
				this.parentVM.collapseAndHideSection(this);
				this.parentVM.showAndExpandThisSection(nextSection);
			} else{
				this.parentVM.doSave(function(payload){
					this.parentVM.creatingNewTask = true;
					this.parentVM.modifyHash(payload.data.id);
					this.collapseAndHideSectionFlag = true;
					this.set('collapsed', true);

					setTimeout(function() {
						this.parentVM.resizeMaxSection(nextSection, this.parentVM.sectionButtonMap['GENERAL']);
					}.bind(this), 100);

				}.bind(this));
			}
		},

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden) {
					setTimeout(function() {
						if (this.parentVM.generalEditSectionExpanded) {
							this.parentVM.generalEditSectionExpanded = null;
							this.set('collapsed', false);
						}
						if (this.parentVM.generalEditSectionMaximized) {
							this.parentVM.generalEditSectionMaximized = null;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['GENERAL']);
						}
					}.bind(this), 10);
				}
			}
		},

		closeFlag: false,
		close: function () {
			if (this.isNewActionTask) {
				this.parentVM.doSave(function(payload){
					this.parentVM.creatingNewTask = true;
					this.parentVM.modifyHash(payload.data.id);

					//this.closeFlag = true;
					//this.collapseAndHideSectionFlag = true;
					this.set('collapsed', true);

					setTimeout(function() {
						this.parentVM.resizeNormalSection(this);
						this.parentVM.edit();
					}.bind(this), 100);

				}.bind(this));
			} else {
				if (this.isSectionMaxSize) {
					this.parentVM.resizeNormalSection(this);
				}
				this.closeFlag = true;
				this.set('collapsed', true);
			}
		}
	},

	getDataPaths: function () {
		return ['data', 'data.resolveActionInvoc', 'data.assignedTo'];
	},

	getData: function () {
		var g = this.gluModel,
		data = {
			id: g.id,
			unamespace: g.namespace,
			uname: g.name,
			ufullName: g.ufullName,
			umenuPath: g.menuPath,
			usummary: g.summary,
			udescription: g.description,
			uactive: g.active,
			ulogresult: g.logResult,
			uisHidden: !g.displayInWorksheet,
			wizardType : g.wizardType
		};

		var resolveActionInvoc = {
			utype: g.type,
			utimeout: g.timeout
		};

		return [data, resolveActionInvoc, g.assignedTo];
	},

	load: function (data, resolveActionInvoc, assignedTo) {

		var g = this.gluModel,
			typeStore = g.typeStore,
			type = resolveActionInvoc.utype.toUpperCase();

		if (type === 'PROCESS') {
			if (g.isProcessFieldHidden || !g.isTypeComboHidden) {
				this.set('isProcessFieldHidden', false)
					.set('isTypeComboHidden', true);
			}

			typeStore.removeBy('value', 'EXTERNAL');
		} else if (type === 'EXTERNAL') {
			if (typeStore.findExact('value', 'EXTERNAL') === -1) {
				typeStore.add({
					text: 'EXTERNAL',
					value: 'EXTERNAL'
				});
			}
		} else {
			typeStore.removeBy('value', 'EXTERNAL');

			if (!g.isProcessFieldHidden || g.isTypeComboHidden) {
				this.set('isProcessFieldHidden', true)
					.set('isTypeComboHidden', false);
			}
		}
		g.autoMenuPathEnabled = data.unamespace === '' && data.umenuPath === '/';

		this.set('id', data.id)
			.set('namespace', data.unamespace)
			.set('name', data.uname)
			.set('isExistingActionTask', data.id.length > 0)
			.set('isNewActionTask', data.id.length === 0)
			.set('menuPath', data.umenuPath)
			.set('summary', data.usummary)
			.set('description', data.udescription)
			.set('active', data.uactive)
			.set('logResult', data.ulogresult)
			.set('displayInWorksheet', !data.uisHidden)
			.set('type', type)
			.set('timeout', resolveActionInvoc.utimeout || 1)
			.set('ufullName', data.uname + '#' + data.unamespace)
			.set('wizardType', data.wizardType);

		if (assignedTo) {
			this.set('assignedToUserName', assignedTo.udisplayName || assignedTo.name)
				.set('assignedToUName', assignedTo.uuserName)
				.set('assignedTo', assignedTo);
		}
		else
		{
			this.set('assignedToUserName', '')
				.set('assignedToUName', '')
				.set('assignedTo', null)
		}

		if (g.form) {
			g.form.isValid();
		}
	}
});
