glu.defModel('RS.actiontaskbuilder.PatternProcessor', {
	patternIsHidden : true,
	patternList : {
		mtype : 'list',
		autoParent : true
	},	
	activePattern : null,
	ignorePattern : [],
	patternCategory : {
		ANY : ['ignoreNotGreedy','ignoreRemainder','literal'],
		SPACE : ['spaceOrTab', 'whitespace','endOfLine'],
		NUMBER : ['number','notNumber'],
		LETTER : ['letters', 'lettersAndNumbers', 'notLettersOrNumbers', 'lowerCase', 'upperCase'],	
		SPECIAL : ['IPv4','MAC','decimal']
	},
	patternForTypeLogicMap : {
		'isWhitespace' : ['ignoreNotGreedy','ignoreRemainder','literal','spaceOrTab', 'whitespace'],
		'isDecimal' : ['ignoreNotGreedy','ignoreRemainder','literal', 'decimal'],
		'isNumber' : ['ignoreNotGreedy','ignoreRemainder','literal', 'number'],
		'isLetters' : ['ignoreNotGreedy','ignoreRemainder','literal', 'notNumber','letters','lowerCase', 'upperCase'],
		'isLettersAndNumbers' : ['ignoreNotGreedy','ignoreRemainder','literal', 'lettersAndNumbers'],
		'isIPv4' : ['ignoreNotGreedy','ignoreRemainder','literal', 'IPv4'],
		'isMAC' : ['ignoreNotGreedy','ignoreRemainder','literal', 'MAC'],
		'isIgnoreNotGreedy' :  ['ignoreNotGreedy','ignoreRemainder','literal', 'notNumber', 'notLettersOrNumbers'],	
		'isLineBreak' : ['endOfLine']
	},
	autoSelectTypeMap : {
		'isWhitespace' : 'whitespace',
		'isDecimal' :'decimal',
		'isNumber' : 'number',
		'isLetters' :'letters',
		'isLettersAndNumbers' : 'lettersAndNumbers',
		'isIPv4' : 'IPv4',
		'isMAC' : 'MAC',
		'isIgnoreNotGreedy' : 'ignoreNotGreedy',	
		'isLineBreak' : 'endOfLine'
	},
	typeToRegexMap : {
		'letters' : '[a-zA-Z]+',
		'number' : '-?\\d+',
		'notLettersOrNumbers' : '\\W+',	
		'lowerCase' : '[a-z]+',
		'upperCase' : '[A-Z]+',
		'ignoreNotGreedy' : '.*?',
		'ignoreRemainder' : '.*',
		'spaceOrTab' : '[\\s\\t]+',
		'whitespace' : '\\s+',
		'endOfLine' : '(?:\\n|\\r\\n)',
		'notNumber' : '\\D+',
		'lettersAndNumbers' : '\\w+',
		'MAC' : '(?:[0-9A-Fa-f]{2}[:-]){5}(?:[0-9A-Fa-f]{2})',
		'IPv4' : '(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)'
	},
	init : function(){
		for(var category in this.patternCategory){
			var patternMetaData = this.patternCategory[category];
			for(var i = 0; i < patternMetaData.length; i++){
				var patternName = patternMetaData[i];
				if(this.ignorePattern.indexOf(patternName) == -1){
					this.patternList.add({
						mtype : 'PatternTag',
						name : patternName
					})
				}			
			}		
		}		
	},
	getAutoTypeLogic: function(text) {
		var guesses = [{
			type : 'isLineBreak',
			v : Ext.is.Windows ? (/^\r\n$/) : (/^\n$/)
		},{
			type: 'isWhitespace',
			v: /^\s+$/
		}, {
			type: 'isDecimal',
			v: /^\d*\.{1}\d+$/
		}, {
			type: 'isNumber',
			v: /^\d+$/
		}, {
			type: 'isLetters',
			v: /^[a-zA-z]+$/
		}, {
			type: 'isLettersAndNumbers',
			v: /^[a-zA-z|\d]+$/
		}, {
			type: 'isIPv4',
			v: {
				test: function(text) {
					var match = text.match(/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/);
					return match != null &&
						match[1] <= 255 && match[2] <= 255 &&
						match[3] <= 255 && match[4] <= 255;
				}
			}
		}, 
		/*{
			type: 'isIPv6',
			v: /^(([0-9a-fA-F]{1,4}:){7,7}[0-9a''-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))$/
		},*/
		 {
			type: 'isMAC',
			v: /^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$/
		}];
		for (var i = 0; i < guesses.length; i++)
			if (guesses[i].v.test(text))
				return guesses[i].type;
		return 'isIgnoreNotGreedy';
	},	
	getRegexForType : function(type){
		return this.typeToRegexMap[type];
	},
	getAutoSelectType : function(typeLogic){
		return this.autoSelectTypeMap[typeLogic];
	},
	selectActivePattern : function(patternName){
		this.patternList.each(function(pattern){		
			if(patternName ==  pattern.name){
				this.set('activePattern', pattern);
				this.activePattern.set('selected',true);
			}
		},this)
	},
	deselectAll : function(){
		if(this.activePattern)
			this.activePattern.set('selected',false);
		this.set('activePattern', null);
	},
	select : function(pattern){
		if(this.activePattern)
			this.activePattern.set('selected', false);
		this.set('activePattern', pattern);
		this.activePattern.set('selected', true);
		this.fireEvent('patternSelectionChanged', this.activePattern['name']);		
	},
	updatePatternForTypeLogic : function(typeLogic){
		this.deselectAll();		
		var availablePatterns = this.patternForTypeLogicMap[typeLogic] || this.patternForTypeLogicMap['isIgnoreNotGreedy'];
		this.patternList.each(function(pattern){
			var patternName = pattern.name;
			if(availablePatterns.includes(patternName))
				pattern.set('hidden', false);
			else 
				pattern.set('hidden', true);
		})
		this.hidePattern(false);	
	},
	updatePatternForMarkup : function(markupName, typeLogic){
		this.updatePatternForTypeLogic(typeLogic);
		this.selectActivePattern(markupName);
	},
	hidePattern : function(flag){
		this.set('patternIsHidden', flag);
	}
})