Ext.define('RS.actiontaskbuilder.SummaryAndDetailsReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'SummaryAndDetails',
		classList: 'block-model',
		collapsed: true,
		hidden: false,
        title: '',
        summaryRuleCount : 0,
		detailRuleCount : 0,
        header: {
            titlePosition: 0,
            defaults: {
                margin: 0
            },	            
            items: []
        },		
        summaryRuleStore : {
        	mtype : 'store',
        	fields : ['severity','condition','display']
        },
        detailRuleStore : {
        	mtype : 'store',
        	fields : ['severity','condition','display']
        },

        when_rulestore_change : {
			on : ['ruleStoreChanged'],
			action : function(){
				this.set('summaryRuleCount', this.summaryRuleStore.getCount());	
				this.set('detailRuleCount', this.detailRuleStore.getCount());			
			}
		},

		postInit: function (blockModel) {
			this.blockModel = blockModel;	
			this.summaryRuleStore.on('datachanged', function(){
				this.fireEvent('ruleStoreChanged');
			}.bind(this));
			this.detailRuleStore.on('datachanged', function(){
				this.fireEvent('ruleStoreChanged');
			}.bind(this));	
		},
		
		multipleSummaryRule$ : function(){
			return this.summaryRuleCount > 1;
		},
		noSummaryRule$ : function(){
			return this.summaryRuleCount == 0
		},
		multipleDetailRule$ : function(){
			return this.detailRuleCount > 1;
		},
		noDetailRule$ : function(){
			return this.detailRuleCount == 0
		},
		
		when_hidden_changed: { 
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden && this.parentVM.embed) {
					var summaryData = this.parentVM.summaryEdit.getData(),
						detailsData = this.parentVM.detailsEdit.getData();

					var assess = {
						summaryRule: summaryData[0].summaryRule,
						detailRule: detailsData[0].detailRule
					}

					this.parentVM.summaryAndDetailsRead.load(assess);
				}
			}
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.assess'];
	},

	load: function (assess) {
		//Temporary code for now until API is ready
		this.gluModel.summaryRuleStore.removeAll();
		if(assess.summaryRule){
			var data = JSON.parse(assess.summaryRule);
			var summaryRules = Array.isArray(data) ? data : [];
			this.gluModel.summaryRuleStore.loadData(summaryRules);
		}
		this.gluModel.detailRuleStore.removeAll();
		if(assess.detailRule){
			var data = JSON.parse(assess.detailRule);
			var detailRules = Array.isArray(data) ? data : [];
			this.gluModel.detailRuleStore.loadData(detailRules);
		}
	}
});
