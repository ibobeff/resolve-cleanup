Ext.define('RS.actiontaskbuilder.PreprocessorEditBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		hidden: true,
		collapsed: true,
		editBlock: true,
		title: '',
		header: {
            titlePosition: 0,
            defaults: {
            	margin: 0
        	},
            items: []
        },
		classList: 'block-model',
		editorClass: 'at-code-editor-length',
		editorName: '',
		content: '',
		warningMsg: '',
		warningLabel: null,
		MAX_LEN: 150000,

		minResize: 133,
		minHeight: 371,
		height: 371,

		expand: function() {
			this.validateAndDisplayWarning(this.content);
		},

		validateAndDisplayWarning: function(val) {
			var html = '',
				charsRemaining = this.MAX_LEN - val.length,
				messageParts = [];

			if (val.length > this.MAX_LEN) {
				messageParts = [this.editorName, val.length, this.MAX_LEN, charsRemaining * -1];
				html = this.localize('editorExceedWarningMsg', messageParts);
				this.set('editorClass', 'at-code-editor-max-length-exceeded');
			} else {
				messageParts = [charsRemaining, val.length, this.MAX_LEN];
				html = this.localize('editorLengthMsg', messageParts);
				this.set('editorClass', 'at-code-editor-length');
			}

			if (this.warningLabel) {
				this.warningLabel.getEl().setHTML(html);
			} else {
				this.set('warningMsg', html);
			}
		},

		contentChange: function() {
			if (!this.task) {
				this.task = new Ext.util.DelayedTask();
			}

			this.task.delay(500, this.saveContent, this, arguments);
		},

		saveContent: function(content) {
			var val = content.getValue();
			this.validateAndDisplayWarning(val);
			this.set('content', val);
			this.notifyDirty();
		},

		notifyDirty: function() {
			this.blockModel.notifyDirty();
		},

		warningMsgShowed: function(warnLabel) {
			this.set('warningLabel', warnLabel);
			this.validateAndDisplayWarning(this.content);
		},

		postInit: function (blockModel) {
			this.blockModel = blockModel;
			this.editorName = this.localize('preprocess');		
		},
		
		closeFlag: false,
		close: function () {
			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.preprocess'];
	},

    id: '',
    uname: '',

	load: function (preprocessor) {
		this.id = preprocessor.id;
		this.uname = preprocessor.uname;
		this.set('content', preprocessor.uscript || '');
	},

	getData: function () {
		return [{
			id: this.id,
			uname: this.uname,
			uscript: this.gluModel.content
		}];
	}
});
