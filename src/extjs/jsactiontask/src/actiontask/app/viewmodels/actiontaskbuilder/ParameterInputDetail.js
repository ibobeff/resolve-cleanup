glu.defModel('RS.actiontaskbuilder.ParameterInputDetail', {
	uname: '',
	udescription: '',
	udefaultValue: '',
	uencrypt: false,
	urequired: true,
	uprotected: false,
	ugroupName: null,
	utype: 'INPUT',
	uorder: 0,
	groupStore: null,
	groupNameMap: {},
	unclassified: '',
	addInputTitle: '',

	init: function () {
		this.set('addInputTitle', this.localize('addInput'));
		this.set('unclassified', this.localize('unclassified'));
		this.set('ugroupName', this.unclassified);
		this.groupNameMap[this.unclassified] = true;
		this.set('groupStore', new RS.common.data.FlatStore({
			sorters: [{
		    	property: 'value',
		    	direction: 'ASC'
			}]
		}));
		this.groupStore.add({
			text: this.unclassified,
			value: '~~__'
		});
	},

	setAddInputButtonTitle: function (title) {
		this.set('addInputTitle', title);
	},

	setGroupsByParameters: function (parameters) {
		var groupNames = [];
		this.groupStore.removeAll();
		this.groupNameMap = [];
		this.groupNameMap[this.unclassified] = true;
		this.set('ugroupName', this.unclassified);

		for (var i = 0; i < parameters.length; i++) {
			var groupName = parameters[i].ugroupName;
			
			if (typeof groupName === 'string' && groupName.length > 0 && !this.groupNameMap[groupName]) {
				groupNames.push(groupName);
				this.groupNameMap[groupName] = true;
			}
		}

		groupNames.sort();
		var shapedGroups = this.groupStore.shapeData(groupNames);

		if (this.groupStore.count() === 0) {
			shapedGroups.unshift({
				text: this.unclassified,
				value: '~~__'
			});
		}
	
		for (var i = 0; i < shapedGroups.length; i++) {
			this.groupStore.suspendEvents();
			var model = this.groupStore.add(shapedGroups[i])[0];
			this.groupStore.remove(model);
			this.groupStore.resumeEvents();
			this.groupStore.addSorted(model);
		}	
	},

	addGroupName: function (groupName) {
		if (!this.groupNameMap[groupName]) {
			var store = this.groupStore,
				unclassified = store.first();
			store.suspendEvents();
			store.removeAt(0);
			var model = store.add(this.groupStore.shapeData(groupName))[0];
			store.remove(model);
			store.insert(0, unclassified);
			store.resumeEvents();
			store.addSorted(model);
			this.groupNameMap[groupName] = true;
			this.set('ugroupName', groupName);
		}
	},

	addInput: function () {
		if (this.ugroupName === '~~__') {
			this.set('ugroupName', this.unclassified);
		}			

		this.parentVM.addInputToList(this);
		this.set('uname', '');
		this.set('udescription', '');
		this.set('udefaultValue', '');
		this.set('uencrypt', false);
		this.set('urequired', true);
		this.set('uprotected', false);
	}
});