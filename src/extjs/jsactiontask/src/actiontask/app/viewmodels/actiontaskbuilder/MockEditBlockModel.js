Ext.define('RS.actiontaskbuilder.MockEditBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Mock',
		hidden: true,
		collapsed: true,
		editBlock: true,
		title: '',
		gridIsEmpty: false,
		parametersGridIsEmpty: false,
		typeStore: null,
		enterMockContentNameTxt: '',
		selectMockContentNameTxt: '',
		mockContentNameEmptyTxt: '',

		header: {
			titlePosition: 0,
			items: []
		},

		mockStore: {
			mtype: 'store',
			fields: ['id', 'uname', 'udescription', 'udata', 'uparams', 'uinputs', 'uflows', 'sysUpdatedBy', 'sysUpdatedOn', 'sysCreatedBy', 'sysCreatedOn'],
			//model: 'RS.actiontask.model.Mocks',
			proxy: {
				type: 'memory'
			}
		},

		mockDetailsStore: {
			mtype: 'store',
			model: 'RS.actiontask.model.MockDetails',
			proxy: {
				type: 'memory'
			}
		},

		allMockInputParametersStore: {
			mtype: 'store',
			model: 'RS.actiontask.model.MockDetails',
			proxy: {
				type: 'memory'
			}
		},

		remainingMockInputParametersStore: {
			mtype: 'store',
			model: 'RS.actiontask.model.MockDetails',
			proxy: {
				type: 'memory'
			}
		},
							
		postInit: function (blockModel) {
			this.blockModel = blockModel;

			var config = {
				mtype: 'store',
				fields: ['name', 'value'],
				data: blockModel.createNameValueItems('input', 'flow', 'param')
			};

			this.set('typeStore', Ext.create('Ext.data.Store', config));
			this.set('enterMockContentNameTxt', this.localize('enterMockContentName'));
			this.set('selectMockContentNameTxt', this.localize('selectMockContentName'));
			this.set('mockContentNameEmptyTxt', this.enterMockContentNameTxt);

			this.set('mockStore', new RS.common.data.FlatStore({ 
				fields: ['id', 'uname', 'udescription', 'udata', 'uparams', 'uinputs', 'uflows', 'sysUpdatedBy', 'sysUpdatedOn', 'sysCreatedBy', 'sysCreatedOn'],
				listeners : {
					datachanged: function () {
						this.notifyDirty();
					}.bind(this),
					update: function () {
						this.notifyDirty();
					}.bind(this)	
				}
			}));

			this.set('mockDetailsStore', new RS.common.data.FlatStore({ 
				fields: ['type', 'name', 'value'],
				listeners : {
					datachanged: function () {
						if (!this.ignoreDirtyChecking) {
							this.notifyMockDetailsDataDirty();
						}
					}.bind(this),
					update: function () {
						if (!this.ignoreDirtyChecking) {
							this.notifyMockDetailsDataDirty();
						}
					}.bind(this)	
				}
			}));
		},

		notifyDirty: function() {
			this.blockModel.notifyDirty();
			if (this.currentRecord && this.parentVM.mockRead) {
				this.parentVM.mockRead.notifyMockDetailsDataDirty(this.currentRecord);
			}
		},

		mockRenderCompleted: function(gridPanel) {
			gridPanel.up().down('form').loadRecord(new RS.actiontask.model.Mocks());
		},

		mockDetailsRenderCompleted: function(gridPanel) {
			gridPanel.up().up().down('form').loadRecord(new RS.actiontask.model.MockDetails());
		},

		editDetailsTooltip$: function() {
			return this.localize('editMockDetails');
		},

		addMockTooltip$: function() {
			return this.localize('addMock');
		},

		removeMockTooltip$: function() {
			return this.localize('removeMock');
		},

		recordRemoved: function(record) {
			this.mockStore.remove(record);
			this.set('gridIsEmpty', this.mockStore.count() === 0);
		},

		isEditingDetails: false,

		closeIsVisible$: function() {
			return !this.isEditingDetails;
		},

		closeDetailsIsVisible$: function() {
			return this.isEditingDetails;
		},

		Mock: function() {},
		MockIsVisible$: function() {
			return !this.isEditingDetails;
		},

		MockDetails: function() {},
		MockDetailsIsVisible$: function() {
			return this.isEditingDetails;
		},

		id: '',
		mockName: '',
		mockDescription: '',
		mockRaw: '',
		ignoreDirtyChecking: false,
		rawIsHidden: false,
		hasMockDescription: false,

		when_mockRaw_changed: {
			on: ['mockRawChanged'],
			action: function () {
				if (!this.ignoreDirtyChecking) {
					this.notifyMockDetailsDataDirty();
				}
			}
		},

		parameters: function() {},

		currentRecord: null,

		when_isEditingDetails_changed: {
			on: ['isEditingDetailsChanged'],
			action: function () {
				if (this.isEditingDetails) {
					this.set('title', this.localize('mockDetailsTitle') + ' - ' + this.mockName);
				} else {
					this.set('title', this.localize('mockTitle'));
				}
			}
		},

		editMockDetails: function(record) {
			this.set('ignoreDirtyChecking', true);
			this.set('currentRecord', record);
			this.set('type', '');
			this.set('name', '');
			this.set('value', '');
			this.set('mockName', record.get('uname'));
			this.set('mockDescription', record.get('udescription') || '');
			this.set('hasMockDescription', this.mockDescription.length > 0);
			this.set('mockRaw', record.get('udata'));
			this.set('isEditingDetails', true);

			this.mockDetailsStore.removeAll();

			var inputs = record.get('uinputs');
			for (var i = 0; i < inputs.length; i++) {
				this.mockDetailsStore.add({
					type: 'INPUT',
					name: inputs[i].name,
					value: inputs[i].value
				});
			}

			var flows = record.get('uflows');
			for (var i = 0; i < flows.length; i++) {
				this.mockDetailsStore.add({
					type: 'FLOW',
					name: flows[i].name,
					value: flows[i].value
				});
			}

			var params = record.get('uparams');
			for (var i = 0; i < params.length; i++) {
				this.mockDetailsStore.add({
					type: 'PARAM',
					name: params[i].name,
					value: params[i].value
				});
			}

			this.getAllInputParametersData();
			this.getRemainingInputParametersData();

			this.set('ignoreDirtyChecking', false);
			this.set('parametersGridIsEmpty', this.mockDetailsStore.count() === 0);
		},

		getAllInputParametersData: function() {
			this.allMockInputParametersStore.removeAll();

			var parametersBlockData = this.parentVM.parametersInputEdit.getData();
			if (parametersBlockData.length) {
				var params = parametersBlockData[0];
				for (var i=0; i< params.length; i++) {
					if (params[i].utype === 'INPUT' || params[i].utype === null) {
						var name = params[i].uname;
						var param = {
							name: name,
							value: params[i].udefaultValue,
							type: 'INPUT'
						};
						this.allMockInputParametersStore.add(param);
					}
				}
				this.allMockInputParametersStore.sort('order');
			}
		},

		getRemainingInputParametersData: function(currentSelectedName) {
			this.remainingMockInputParametersStore.removeAll();

			for (var i = 0; i < this.allMockInputParametersStore.getCount(); i++) {
				var inputParam = this.allMockInputParametersStore.getAt(i);
				var type = inputParam.get('type');
				if (type == 'INPUT') {
					var name = inputParam.get('name');
					var param = {
						name: name,
						value: inputParam.get('value'),
						type: 'INPUT'
					};

					var addToStore = false;

					if (currentSelectedName && currentSelectedName == name) {
						addToStore = true;
					}
					else {
						var pos = this.mockDetailsStore.findExact('name', name);
						if (pos == -1) {
							addToStore = true;
						}
						else {
							var rec = this.mockDetailsStore.getAt(pos);
							if (rec.type == 'INPUT') {
								addToStore = true;
							}
						}
					}
	
					if (addToStore) {
						this.remainingMockInputParametersStore.add(param);
					}
				}
			}

			if (this.remainingMockInputParametersStore.getCount()) {
				if (this.typeStore.findExact('name', 'INPUT') == -1) {
					this.typeStore.insert(0, {
						name: 'INPUT',
						value: 'input'
					});
				}
			}
			else {
				var inputIdx = this.typeStore.findExact('name', 'INPUT');
				if (inputIdx != -1) {
					this.typeStore.removeAt(inputIdx);
				}
			}
		},

		clearMockDetailsInputParametersData: function() {
			this.remainingMockInputParametersStore.removeAll();
		},	

		uname: '',
		udescription: '',
		type: '',
		name: '',
		value: '',		
		validNameRegEx : RS.common.validation.VariableName,
		nameValidation : function(field, store){
			if(this[field]){
				var store = this[store];
				
				//Check for valid characters
				if (!this.validNameRegEx.test(this[field])){
					return this.localize('validKeyCharacters');					
				}
				//Check for duplicate
				else if (store.findExact(field, this[field]) != -1) {
					return this.localize('duplicateMockName', this[field]);
				}
				else
					return true;
			}		
		},
		validateNameOnEdit : function(context, field, store){
			if(context.field == field){
				var newVal = context.value;
				var currentRowIdx = context.rowIdx;

				if(newVal == ""){
					this.displayInvalidNameMsg('emptyMockName');
					return false;
				}
				else if (!this.validNameRegEx.test(newVal)){
					this.displayInvalidNameMsg('validKeyCharacters');
					return false;				
				}
				//Check for duplicate
				else if (this[store].findExact(field, newVal) != currentRowIdx && this[store].findExact(field, newVal) != -1) {
					this.displayInvalidNameMsg('duplicateMockName', newVal);
					return false;
				}
				else
					return true;
			}
		},
		unameIsValid$: function(){
			if(this.uname)
				return this.nameValidation('uname','mockStore');				
		},
		
		//Name of parameter in mock detail.
		nameIsValid$ : function(){
			if(this.name)
				return this.nameValidation('name','mockDetailsStore');	
		},

		displayInvalidNameMsg : function(msg, extraVariables){
			clientVM.message(this.localize('invalidMockTitle'), this.localize(msg,extraVariables));		
		},

		isMockDetailsNameEditable: false,
		isMockDetailsEditNameEditable: false,

		mockDetailsEditTypesSelected: function(combo) {
			var mockDetailsEditNameCombo = combo.up().up().queryById('mockDetailsEditNameCombo');
			var mockType = combo.value;

			if (mockType === 'INPUT') {
				this.set('isMockDetailsEditNameEditable', false);
				this.set('mockContentNameEmptyTxt', this.selectMockContentNameTxt); 
				mockDetailsEditNameCombo.emptyText = this.selectMockContentNameTxt;
				this.getRemainingInputParametersData();
			} else {
				this.set('isMockDetailsEditNameEditable', true);
				this.set('mockContentNameEmptyTxt', this.enterMockContentNameTxt); 
				mockDetailsEditNameCombo.emptyText = this.enterMockContentNameTxt;
				this.clearMockDetailsInputParametersData();
			}
		},

		mockDetailsEditNameSelected: function(combo) {
			var mockType = combo.ownerCt.grid.getSelectionModel().getSelection()[0].get('type');

			if (mockType === 'INPUT') {
				this.set('name', '');
				this.set('type', '');
				this.set('value', '');
				this.getRemainingInputParametersData();
			} else {
				this.clearMockDetailsInputParametersData();
			}
		},

		populateMockDetailsDataSources: function (context) {
			var d = context.record.data;
			var editor = context.column.getEditor(context.record);

			switch(context.colIdx) {
			case 0:
				if (d.type === 'INPUT') {
					this.getRemainingInputParametersData();
				} else {
					this.clearMockDetailsInputParametersData();
				}
				break;
			case 1:
				if (d.type === 'INPUT') {
					this.getRemainingInputParametersData(d.name);
					editor.emptyText = this.selectMockContentNameTxt;
					editor.setEditable(false);
				} else {
					this.clearMockDetailsInputParametersData();
					editor.emptyText = this.enterMockContentNameTxt;
					editor.setEditable(true);
				}

				editor.applyEmptyText();
				break;
			}
		},

		newMockDetailNameFocus: function() {
			if (this.type && this.type === 'INPUT') {
				this.getRemainingInputParametersData();
			} else {
				this.clearMockDetailsInputParametersData();
			}
		},

		addMock: function() {
			var store = this.mockStore,
				name = this.uname,
				description = this.udescription;			

			if (store.findExact('uname', name) === -1) {
				store.add({
					uname: name,
					udescription: description
				});

				this.set('gridIsEmpty', this.mockStore.count() === 0);
			}

			this.resetFormMockForm();
		},

		addMockIsEnabled$ : function(){
			return this.unameIsValid == true && this.uname !== '';
		},

		resetFormMockForm : function(){
			this.set('uname', '');
			this.set('udescription', '');
		},

		recordParamRemoved: function(record) {
			this.mockDetailsStore.remove(record);
			this.set('parametersGridIsEmpty', this.mockDetailsStore.count() === 0);

			if (record.get('type') === 'INPUT') {
				this.getRemainingInputParametersData();
			}
		},

		addParams: function(form) {
			var store = this.mockDetailsStore,
				type, name, value;		
		
			type = this.type;
			name = this.name;
			value = this.value;

			if (store.findExact('name', name) === -1) {
				var param = {
					type: type,
					name: name,
					value: value
				};
				store.add(param);
				this.set('parametersGridIsEmpty', this.mockDetailsStore.count() === 0);
				
				if (type === 'INPUT') {
					this.getRemainingInputParametersData();
				}
			}

			this.resetParameterForm();
		},

		parametersMesg$: function() {
			if (this.parametersGridIsEmpty) {
				return this.localize('emptyMockParamMsg');
			} else {
				return '';
			}
		},

		addParamsIsEnabled$ : function(){
			return this.nameIsValid == true && this.name !== '' && this.value !== '';
		},
		
		resetParameterForm : function(){
			this.set('type', null);
			this.set('name', '');
			this.set('value', null);
		},		

		mockDetailsEditTypeFocus: function() {
			this.getRemainingInputParametersData();
		},

		newMockDetailTypeFocus: function() {
			this.getRemainingInputParametersData();
		},

		mockDetailTypeSelected: function(combo) {
			var mockDetailsNameCombo = combo.ownerCt.ownerCt.queryById('mockDetailsNameCombo');
			var mockType = combo.value;

			if (mockType === 'INPUT') {
				this.set('isMockDetailsNameEditable', false);
				this.set('mockContentNameEmptyTxt', this.selectMockContentNameTxt); 
				mockDetailsNameCombo.emptyText = this.selectMockContentNameTxt;
				this.getRemainingInputParametersData();
			} else {
				this.set('isMockDetailsNameEditable', true);
				this.set('mockContentNameEmptyTxt', this.enterMockContentNameTxt); 
				mockDetailsNameCombo.emptyText = this.enterMockContentNameTxt;
				this.clearMockDetailsInputParametersData();
			}
			this.set('name', '');
			mockDetailsNameCombo.applyEmptyText();
		},

		notifyMockDetailsDataDirty: function() {
			if (this.currentRecord) {
				this.currentRecord.set('uname', this.mockName);
				this.currentRecord.set('udescription', this.mockDescription);
				this.currentRecord.set('udata', this.mockRaw);
	
				var uinputs = [],
					uflows = [],
					uparams = [];
	
				for (var i = 0; i < this.mockDetailsStore.getCount(); i++) {
					var param = this.mockDetailsStore.getAt(i);
					var type = param.get('type');
					var content = {
						name: param.get('name'),
						value: param.get('value')
					};
					if (type === 'INPUT') {
						uinputs.push(content);
					}
					else if (type === 'FLOW') {
						uflows.push(content);
					}
					else if (type === 'PARAM') {
						uparams.push(content);
					}
				}
	
				this.currentRecord.set('uinputs', uinputs);
				this.currentRecord.set('uflows', uflows);
				this.currentRecord.set('uparams', uparams);
				
				this.notifyDirty();
			}
		},

		closeDetails: function () {
			this.set('isEditingDetails', false);
			this.notifyMockDetailsDataDirty();
			this.set('currentRecord', null);
		},

		closeFlag: false,
		close: function () {
			this.set('isEditingDetails', false);
			this.set('currentRecord', null);

			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.resolveActionTaskMockData'];
	},

	getData: function () {
		var resolveActionTaskMockData = [];

		var nameMap = {};
		var collectResult = {
			success: true
		};

		this.gluModel.mockStore.each(function(mock) {
			if (nameMap[mock.get('uname')]) {
				collectResult = this.gatherDataError('duplicateMockName', {
					name: mock.get('uname')
				});
				return true;
			}
			nameMap[mock.get('uname')] = true;
			var uinputs = this.convertArrToJson(mock, 'uinputs', 'duplicateMockInput');
			if (uinputs.success == false) {
				collectResult = uinputs;
				return;
			}
			var uflows = this.convertArrToJson(mock, 'uflows', 'duplicateMockFlow');
			if (uflows.success == false) {
				collectResult = uflows;
				return;
			}
			var uparams = this.convertArrToJson(mock, 'uparams', 'duplicateMockParam');
			if (uparams.success == false) {
				collectResult = uparams;
				return;
			}

			resolveActionTaskMockData.push({
				id: mock.get('id'),
				uname: mock.get('uname'),
				udescription: mock.get('udescription'),
				udata: mock.get('udata'),
				uinputs: Ext.JSON.encode(uinputs),
				uflows: Ext.JSON.encode(uflows),
				uparams: Ext.JSON.encode(uparams)
			});
		}, this);

		return [resolveActionTaskMockData];
	},

	load: function (resolveActionTaskMockData) {
		resolveActionTaskMockData = resolveActionTaskMockData || [];
		this.callParent(arguments);
		this.gluModel.mockStore.removeAll();

		if (resolveActionTaskMockData) {
			for (var i = 0; i < resolveActionTaskMockData.length; i++) {
				var mock = resolveActionTaskMockData[i];
				if (this.gluModel.currentRecord && this.gluModel.currentRecord.get('id') == mock.id) {
					this.gluModel.mockStore.insert(0, this.gluModel.currentRecord);
					continue;
				}

				var uinputs = this.jsonStrToArray(mock.uinputs),
					uflows = this.jsonStrToArray(mock.uflows),
					uparams = this.jsonStrToArray(mock.uparams);

				this.gluModel.mockStore.add({
					id: mock.id || '',
					uname: mock.uname || '',
					udescription: mock.udescription || '',
					udata: mock.udata,
					uparams: uparams,
					uinputs: uinputs,
					uflows: uflows,
					sysUpdatedBy: mock.sysUpdatedBy,
					sysUpdatedOn: mock.sysUpdatedOn,
					sysCreatedBy: mock.sysCreatedBy,
					sysCreatedOn: mock.sysCreatedOn
				});				
			}
		}

		this.set('gridIsEmpty', this.gluModel.mockStore.count() === 0);
	},

	jsonStrToArray: function(jsonStr) {
		if (!jsonStr)
			return [];
		var arr = [];
		var json = Ext.JSON.decode(jsonStr);
		for (key in json) {
			arr.push({
				name: key,
				value: json[key]
			})
		}
		return arr;
	},

	convertArrToJson: function(mock, field, errMsg) {
		if (!mock.get(field)) return {};
		var keyValue = {};
		var collectResult = null;
		Ext.each(mock.get(field), function(r) {
			if (keyValue[r.name]) {
				collectResult = this.gatherDataError(errMsg, {
					mock: mock.get('uname'),
					name: r.name
				});
			}
			keyValue[r.name] = r.value
		}, this);
		if (collectResult)
			return collectResult;
		return keyValue;
	},

	gatherDataError: function(templateMsg, templateParam) {
		return {
			success: false,
			msg: this.gluModel.localize(templateMsg, templateParam)
		};
	},

	modifyByNotification: function (data, resolveActionInvoc) {
		this.gluModel.getAllInputParametersData();
		this.gluModel.getRemainingInputParametersData();
		this.set('rawIsHidden', resolveActionInvoc.utype === 'ASSESS');
	}
});