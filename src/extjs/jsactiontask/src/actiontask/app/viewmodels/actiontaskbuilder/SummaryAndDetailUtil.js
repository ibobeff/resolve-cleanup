glu.defModel('RS.actiontaskbuilder.SummaryAndDetailUtil',{		
	isFieldNameEditable: true,		
	htmlEncode : false,
	preview : '',			
	ruleCount : 0,	
	typesStore: {
		mtype : 'store',
		fields : ['text','value'],
		data : [{
			text: 'OUTPUT',
			value: 'OUTPUTS'
		}, {
			text: 'INPUT',
			value: 'INPUTS'
		}, {
			text: 'FLOW',
			value: 'FLOWS'
		}, {
			text: 'PARAM',
			value: 'PARAMS'
		}, {
			text:  'WSDATA',
			value: 'WSDATA'
		}]
	},
 	allParameters : {
		INPUT : [],
		OUTPUT : [],
		FLOW : [],
		WSDATA : []
	},
	defaultData : {
		condition : 'ANY',
		severity : 'ANY',
		text : '',
		fieldName : '',
		fieldType : ''
	},		
	parametersStore: {
		mtype : ['store'],
		fields : ['name']
	},			
	ruleStore : {
		mtype : 'store',
		fields : ['condition','severity','display']
	},
	conditionStore : {
		mtype : 'store',
		fields : ['name'],
		data  : [{
			name : 'ANY',
		},{
			name : 'BAD'
		},{
			name : 'GOOD'
		}]
	},
	severityStore : {
		mtype : 'store',
		fields : ['name'],
		data  : [{
			name : 'ANY',
		},{
			name : 'CRITICAL'
		},{
			name : 'SEVERE'
		},{
			name : 'WARNING'
		},{
			name : 'GOOD'
		}]
	},

	when_rulestore_change : {
		on : ['ruleStoreChanged'],
		action : function(){
			this.set('ruleCount', this.ruleStore.getCount());
			this.notifyDirty();
		}
	},
	render_preview : {
		on : ['htmlEncodeChanged','displayChanged'],
		action : function(){
			this.renderPreview();
		}
	},
	when_field_type_changed : {
		on : ['fieldTypeChanged'],
		action : function(){
			var currentType = this.fieldType;
			this.set('fieldName', '');	
			this.set('fieldNameIsPristine', true);		

			if(currentType == 'OUTPUTS' || currentType == 'INPUTS')				
				this.set('isFieldNameEditable', false);		
			else if(currentType == 'WSDATA' || currentType == 'FLOWS' || currentType == 'PARAMS')				
				this.set('isFieldNameEditable', true);
							
			this.loadParameterStore(this.parametersStore, currentType);				
		}
	},
	conditionIsValid$ : function(){
		return this.condition != '' ? true : this.localize('requiredField');
	},
	severityIsValid$ : function(){
		return this.severity != '' ? true : this.localize('requiredField');
	},
	displayIsValid$ : function(){
		return this.display != '' ? true : this.localize('requiredField');
	},
	multipleRule$ : function(){
		return this.ruleCount > 1;
	},
	noRule$ : function(){
		return this.ruleCount == 0
	},
	insertVariableIsEnabled$ : function(){
		return this.fieldType && this.fieldName;
	},

	renderPreview : function(){
		var preview = this.htmlEncode ? Ext.String.htmlEncode(this.display) : this.display;
		this.fireEvent('renderPreview', preview.replace(/\n/g,'<br >'));
	},
	addRule : function(){
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		this.ruleStore.add({
			condition : this.condition,
			severity : this.severity,
			display : this.display
		})
		this.loadData(this.defaultData);
		this.set('isPristine',true);
	},
	removeRule : function(record){
		this.ruleStore.remove(record);
	},			
	loadParameterStore : function(store, dataType){
		if(dataType == 'INPUTS')
			store.loadData(this.allParameters['INPUT']);
		else if(dataType == 'OUTPUTS')
			store.loadData(this.allParameters['OUTPUT']);
		else if(dataType == 'FLOWS')
			store.loadData(this.allParameters['FLOW']);
		else if(dataType == 'WSDATA')
			store.loadData(this.allParameters['WSDATA']);
		else
			store.removeAll();
	},
	insertVariable: function () {			
		this.set('display', this.display + ' ${' + this.fieldType + '.' + this.fieldName + '}');				
	}
})