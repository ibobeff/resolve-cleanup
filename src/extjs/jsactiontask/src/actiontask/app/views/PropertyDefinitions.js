glu.defView('RS.actiontask.PropertyDefinitions', {
	xtype: 'grid',
	cls : 'rs-grid-dark',
	padding : 15,
	layout: 'fit',
	stateId: '@{stateId}',
	stateful: true,
	displayName: '@{displayName}',
	name: 'records',
	store: '@{store}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['createRecord', 'deleteRecords']
	}],
	columns: '@{columns}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		useWindowParams: true,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.editColumnTooltip,
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		glyph : 0xF044
	},

	listeners: {
		editAction: '@{editRecord}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
});