glu.defView('RS.actiontask.ActionTaskPicker', {
    title: '@{title}',
    padding : 15,
    modal: true,
    cls : 'rs-modal-popup',
    defaults : {
        margin : '0 0 8 0'
    },
    layout : 'card',
    activeItem : '@{activeTab}',
    dockedItems : [{
        xtype : 'toolbar',
        ui: 'display-toolbar',
        defaultButtonUI: 'display-toolbar-button',
        items: ['->', {
            name: 'allTaskTab',
            pressed: '@{allTaskTabIsPressed}'
        }, {
            name: 'fromAutomationTab',
            pressed: '@{fromAutomationTabIsPressed}'
        }]
    }],
    items: [{
        xtype : '@{allTask}'
    },{
        xtype : '@{fromAutomation}'
    }],
    buttons: [{
        name : 'dump',
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'cancel',
        cls : 'rs-med-btn rs-btn-light'
    }],
    listeners: {
        beforeshow: function(win) {
            win.setWidth(Math.round(Ext.getBody().getWidth() * 0.8));
            win.setHeight(Math.round(Ext.getBody().getHeight() * 0.8));
        }
    }
});
glu.defView('RS.actiontask.AllTask',{
    layout: 'border',
    plugins: [{
        ptype: 'searchfilter',
        allowPersistFilter: false,
        hideMenu: true,
        useWindowParams: false
    }, {
        ptype: 'pager'
    }],
    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',
        cls: 'actionBar rs-dockedtoolbar',
        name: 'actionBar',
        items: ['menuPath', {
            name: 'module',
            handler: '@{module}'
        }]
    }],
    items: [{
        layout: 'card',
        region: 'west',
        width: 200,
        split : true,
        maxWidth: 300,
        activeItem: '@{activeItem}',
        items: [{
            xtype: 'treepanel',
            cls : 'rs-grid-dark',
            columns: '@{actionTaskTreeMenuPathColumns}',
            name: 'actionTaskTreeMenuStore',
            listeners: {
                selectionchange: '@{menuPathTreeSelectionchange}'
            }
        },{
            xtype: 'treepanel',
            cls : 'rs-grid-dark',
            columns: '@{actionTaskTreeModuleColumns}',
            name: 'actionTaskTreeModuleStore',
            root: {
                name: 'All',
                id: 'root_module_id',
                expanded: true
            },
            listeners: {
                selectionchange: '@{moduleTreeSelectionchange}'
            }
        }]
    }, {
        //Multi selection (for Result Macro)
        hidden : '@{!multiSelection}',
        region: 'center',
        xtype: 'grid',
        cls : 'rs-grid-dark',
        displayName: '~~actionTaskDisplayName~~',
        name: 'actionTaskGridStore',
        store: '@{actionTaskGridStore}',
        columns: '@{columns}',
        autoScroll: true,
        viewConfig: {
            enableTextSelection: true
        },
        plugins: [{
            ptype: 'resolveexpander',
            rowBodyTpl: new Ext.XTemplate(
                '<div resolveId="resolveRowBody" style="color:#333">{[this.formatDescription(values.udescription)]}{[this.convertJson(values.resolveActionInvoc.resolveActionParameters)]}</div>', {
                    formatDescription: function(desc) {
                        if (!desc)
                            return '';
                        else
                            return '<span class="actiontaskpicker-heading">Description:</span><br/><br/>&nbsp&nbsp&nbsp&nbsp' + Ext.String.htmlEncode(desc) + '<br/><br/>';
                    },
                    convertJson: function(params) {
                        if (!params)
                            return;
                        var inputRows = '<span class="actiontaskpicker-heading">Input Parameters</span>:<br/><br/>';
                        var outputRows = '<span class="actiontaskpicker-heading">Output Parameters</span>:<br/><br/>';
                        Ext.each(params, function(param, idx) {
                            if (!param.utype || param.utype.toLowerCase() == 'input')
                                inputRows += '&nbsp&nbsp&nbsp&nbsp' + param.uname + ' - ' + Ext.String.htmlEncode(param.udescription || '') + '<br/>'
                            else
                                outputRows += '&nbsp&nbsp&nbsp&nbsp' + param.uname + ' - ' + Ext.String.htmlEncode(param.udescription || '') + '<br/>'
                        });
                        return inputRows + '<br/>' + outputRows;
                    }
                }
            )
        }],
        selType:'checkboxmodel',
        selModel : {
            mode : 'simple'
        }
    },{
        //Single selection with option to view detail of task (for Automation Designer)
        hidden : '@{multiSelection}',
        region: 'center',
        xtype: 'grid',
        cls : 'rs-grid-dark',
        displayName: '~~actionTaskDisplayName~~',
        name: 'actionTaskGridStore',
        store: '@{actionTaskGridStore}',
        columns: '@{columns}',
        autoScroll: true,
        viewConfig: {
            enableTextSelection: true
        },
        plugins: [{
            ptype: 'resolveexpander',
            rowBodyTpl: new Ext.XTemplate(
                '<div resolveId="resolveRowBody" style="color:#333">{[this.formatDescription(values.udescription)]}{[this.convertJson(values.resolveActionInvoc.resolveActionParameters)]}</div>', {
                    formatDescription: function(desc) {
                        if (!desc)
                            return '';
                        else
                            return '<span class="actiontaskpicker-heading">Description:</span><br/><br/>&nbsp&nbsp&nbsp&nbsp' + Ext.String.htmlEncode(desc) + '<br/><br/>';
                    },
                    convertJson: function(params) {
                        if (!params)
                            return;
                        var inputRows = '<span class="actiontaskpicker-heading">Input Parameters</span>:<br/><br/>';
                        var outputRows = '<span class="actiontaskpicker-heading">Output Parameters</span>:<br/><br/>';
                        Ext.each(params, function(param, idx) {
                            if (!param.utype || param.utype.toLowerCase() == 'input')
                                inputRows += '&nbsp&nbsp&nbsp&nbsp' + param.uname + ' - ' + Ext.String.htmlEncode(param.udescription || '') + '<br/>'
                            else
                                outputRows += '&nbsp&nbsp&nbsp&nbsp' + param.uname + ' - ' + Ext.String.htmlEncode(param.udescription || '') + '<br/>'
                        });
                        return inputRows + '<br/>' + outputRows;
                    }
                }
            )
        }],
        selModel: {
            selType: 'resolvecheckboxmodel',
            columnTooltip: RS.common.locale.editColumnTooltip,
            columnTarget: '_self',
            columnEventName: 'editAction',
            columnIdField: 'id'
        },
        listeners: {
            editAction: '@{editActionTask}'
        }
    }]
});
glu.defView('RS.actiontask.FromAutomation',{
    padding : '15 0 0 0',
    plugins: [{
        ptype: 'pager'
    }],
    layout : 'fit',
    dockedItems : [{
        xtype : 'toolbar',
        cls : 'actionBar rs-dockedtoolbar',
        name: 'actionBar',
        items : [{
            xtype: 'combo',
            labelStyle : 'font-size: 16px;font-weight: bold;',
            labelWidth : 130,
            width : 700,
            name : 'automationSearch',
            store: '@{automationStore}',
            emptyText : '~~runbookNameEmptyText~~',
            displayField: 'ufullname',
            valueField: 'ufullname',
            queryMode: 'remote',
            typeAhead: false,
            minChars: 1,
            //queryDelay: 500,
            keyDelay : 300,
            //This will fix the issue where value in control in glu is out of sync with what send to server.
            ignoreSetValueOnLoad : true
        }]
    }],
    items : [{
        //Multi selection (for Result Macro)
        hidden : '@{!multiSelection}',
        xtype: 'grid',
        cls : 'rs-grid-dark',
        name: 'actionTaskGridStore',
        store: '@{actionTaskGridStore}',
        columns: '@{columns}',
        autoScroll: true,
        viewConfig: {
            enableTextSelection: true
        },
        plugins: [{
            ptype: 'resolveexpander',
            rowBodyTpl: new Ext.XTemplate(
                '<div resolveId="resolveRowBody" style="color:#333">{[this.formatDescription(values.udescription)]}{[this.convertJson(values.resolveActionInvoc.resolveActionParameters)]}</div>', {
                    formatDescription: function(desc) {
                        if (!desc)
                            return '';
                        else
                            return '<span class="actiontaskpicker-heading">Description:</span><br/><br/>&nbsp&nbsp&nbsp&nbsp' + Ext.String.htmlEncode(desc) + '<br/><br/>';
                    },
                    convertJson: function(params) {
                        if (!params)
                            return;
                        var inputRows = '<span class="actiontaskpicker-heading">Input Parameters</span>:<br/><br/>';
                        var outputRows = '<span class="actiontaskpicker-heading">Output Parameters</span>:<br/><br/>';
                        Ext.each(params, function(param, idx) {
                            if (!param.utype || param.utype.toLowerCase() == 'input')
                                inputRows += '&nbsp&nbsp&nbsp&nbsp' + param.uname + ' - ' + Ext.String.htmlEncode(param.udescription || '') + '<br/>'
                            else
                                outputRows += '&nbsp&nbsp&nbsp&nbsp' + param.uname + ' - ' + Ext.String.htmlEncode(param.udescription || '') + '<br/>'
                        });
                        return inputRows + '<br/>' + outputRows;
                    }
                }
            )
        }],
        selType:'checkboxmodel',
        selModel : {
            mode : 'simple'
        }
    },{
        //Single selection with option to view detail of task (for Automation Designer)
        hidden : '@{multiSelection}',
        region: 'center',
        xtype: 'grid',
        cls : 'rs-grid-dark',
        name: 'actionTaskGridStore',
        store: '@{actionTaskGridStore}',
        columns: '@{columns}',
        autoScroll: true,
        viewConfig: {
            enableTextSelection: true
        },
        plugins: [{
            ptype: 'resolveexpander',
            rowBodyTpl: new Ext.XTemplate(
                '<div resolveId="resolveRowBody" style="color:#333">{[this.formatDescription(values.udescription)]}{[this.convertJson(values.resolveActionInvoc.resolveActionParameters)]}</div>', {
                    formatDescription: function(desc) {
                        if (!desc)
                            return '';
                        else
                            return '<span class="actiontaskpicker-heading">Description:</span><br/><br/>&nbsp&nbsp&nbsp&nbsp' + Ext.String.htmlEncode(desc) + '<br/><br/>';
                    },
                    convertJson: function(params) {
                        if (!params)
                            return;
                        var inputRows = '<span class="actiontaskpicker-heading">Input Parameters</span>:<br/><br/>';
                        var outputRows = '<span class="actiontaskpicker-heading">Output Parameters</span>:<br/><br/>';
                        Ext.each(params, function(param, idx) {
                            if (!param.utype || param.utype.toLowerCase() == 'input')
                                inputRows += '&nbsp&nbsp&nbsp&nbsp' + param.uname + ' - ' + Ext.String.htmlEncode(param.udescription || '') + '<br/>'
                            else
                                outputRows += '&nbsp&nbsp&nbsp&nbsp' + param.uname + ' - ' + Ext.String.htmlEncode(param.udescription || '') + '<br/>'
                        });
                        return inputRows + '<br/>' + outputRows;
                    }
                }
            )
        }],
        selModel: {
            selType: 'resolvecheckboxmodel',
            columnTooltip: RS.common.locale.editColumnTooltip,
            columnTarget: '_self',
            columnEventName: 'editAction',
            columnIdField: 'id'
        },
        listeners: {
            editAction: '@{editActionTask}'
        }
    }]
})