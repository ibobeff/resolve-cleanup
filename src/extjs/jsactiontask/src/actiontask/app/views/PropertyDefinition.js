glu.defView('RS.actiontask.PropertyDefinition', {
    title: '~~property~~',
    padding : 15,
    modal: true,
    cls : 'rs-modal-popup',
    width: 600,
    height: 400,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults : {
        margin : '4 0'
    },
    defaultType: 'textfield',
    items: ['uname', {
        xtype: 'combobox',
        queryMode: 'local',
        displayField: 'name',
        valueField: 'value',
        store: '@{moduleStore}',
        name: 'umodule'
    }, {
        xtype: 'combobox',
        editable: false,
        queryMode: 'local',
        displayField: 'name',
        valueField: 'value',
        store: '@{typeStore}',
        name: 'utype'
    }, {
        xtype: 'textarea',
        flex: 1,
        name: 'uvalue',
        hidden: '@{isEncrypt}'
    }, {
        name: 'uvalue',
        hidden: '@{!isEncrypt}',
        inputType: 'password',
		listeners:{
			afterrender:function(cmp){
				cmp.inputEl.set({
					//Prevent autofill from Chrome.
					autocomplete:'new-password'
				});
			}
		}
    }], 
    buttons: [{
        name :'save',
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'close',
        cls : 'rs-med-btn rs-btn-light'
    }]
})