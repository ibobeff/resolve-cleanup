glu.defView('RS.actiontask.PropertyDefinitionsPicker', {
	asWindow: {
		title: '~~PropertyDefinitionsPickerTitle~~',
		height: '@{height}',
		width: '@{width}',
		modal: true
	},
	layout: 'fit',
	buttonAlign: 'left',
	buttons: ['select', 'cancel'],
	items: [{
		xtype: 'grid',
		displayName: '@{displayName}',
		name: 'records',
		store: '@{store}',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: []
		}],
		columns: '@{columns}',
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}]
	}]
});