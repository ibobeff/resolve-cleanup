glu.defView('RS.actiontask.MoveOrRenameOrCopy', {
    title: '@{title}',
    padding : 15,
    modal: true,
    height : 250,   
    width: 600,   
    cls : 'rs-modal-popup',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        allowBlank: false,
        labelWidth: 100
    },
    items: [{
        xtype: 'combo',
        name: 'unamespace',
        itemId: 'unamespaceCombo',
        store: '@{comboBoxStore}',
        displayField: 'name',
        valueField: 'name',
        typeAhead: true,
        queryMode: 'local',
        editable: true	
    }, {
        xtype: 'textfield',
        name: 'uname'
    }, {
        xtype: 'checkbox',
        name: 'overwrite',
        hideLabel: true,
        boxLabel: '~~overwrite~~',
        padding: '0px 0px 0px 110px'
    }],
    buttons : [{
        name: 'dump',
        text: '@{dumpText}',
        cls : 'rs-med-btn rs-btn-dark'
    }, {
        name :'cancel',
        cls : 'rs-med-btn rs-btn-light'
    }],
    listeners: {
        render: function() {
            // GVo Note
            // This is a get-around
            // solution on the combobox not populating the value.
            // I have no idea why. According to Sencha docs, we can set the
            // initial selection by setting the value property but some how it doesn't work in the case.
            var comboBox = this.down('#unamespaceCombo');
            comboBox.getStore().on('foundComboSelection', function(selectedRecord) {
                comboBox.select(selectedRecord);
            });
        }
    }
})