glu.defView('RS.actiontaskbuilder.Access', {
	xtype: 'fieldset',
	title: '~~addRole~~',
	margin: '10 0 0 0',
	padding: '5 15 10 15',
	hidden: '@{isDisabled}',
	height: 215,
	items: [{
		xtype: 'form',
		margin: 0,
		padding: 0,

		layout: {
			type: 'vbox',
			align: 'stretch'		
		},		

		defaults: {
			labelWidth: 130,
			maxWidth: 500,
		},
		items: [{
			xtype: 'combo',
			name: 'uname',
			allowBlank: false,
			emptyText: '~~selectRole~~',
			store: '@{rolesStore}',
			value: '@{uname}',				
			queryMode: 'local',
			displayField: 'text',
			valueField: 'value',			
			cls: 'no-input-border combo',
			editable: false,
			listeners: {
				focus: '@{focusRole}'
			}
		}, {
			xtype: 'checkbox',			
			name: 'read',
			uncheckedValue: false,			
			boxLabel: '~~roleReadRight~~',
			margin: '0 0 5 0'
		}, {
			xtype: 'checkbox',
			name: 'write',
			uncheckedValue: false,			
			hideLabel: true,
			boxLabel: '~~roleWriteRight~~',
			margin: '0 0 5 135'
		}, {
			xtype: 'checkbox',
			name: 'execute',
			uncheckedValue: false,		
			hideLabel: true,
			boxLabel: '~~roleExecuteRight~~',
			margin: '0 0 5 135'
		}, {
			xtype: 'checkbox',
			name: 'admin',
			uncheckedValue: false,			
			hideLabel: true,
			boxLabel: '~~roleAdminRight~~',
			disabled: '@{!isAdmin}',
			margin: '0 0 5 135'
		}, {	
			xtype: 'button',
			text: '~~addRole~~',			
			cls: 'rs-small-btn rs-btn-dark',		
			margin: '0 0 0 135',
			maxWidth: 90,
			name: 'add',
			formBind: true
		}]
	}]
});