glu.defView('RS.actiontaskbuilder.Assessor', {
	xtype: 'codeeditorpanel',
	title: '@{title}',
	cls: '@{classList}',
	hidden: '@{hidden}',
	header: '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	resizable: {
		handles: 's',
		pinned: true
	},
	padding: 0,
	bodyPadding: '10 15 12 15',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},	
	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function () {
				if (panel._vm.isViewOnly) {
					if (panel.collapsed) {
						panel.expand();
					} else {
						panel.collapse();
					}
				}
			});
			
			panel.header.items.getAt(1).on('click', function(c, e) {
				panel._vm.set('isViewOnly', false);
				panel._vm.set('collapsed', false);
				e.stopPropagation();
				return false;
			});
		},
		afterRendered: '@{viewRenderCompleted}',
		contentchange: '@{contentChange}',
		warningmsgshowed: '@{warningMsgShowed}',
		expand: '@{expand}',
		beforecollapse: function (panel) {
			panel._vm.set('isViewOnly', true);
			return this.getCollapsible();
		}
	},
	items: [{
		xtype: 'checkbox',
		disabled: '@{isViewOnly}',
		disabledCls: 'disabled-readable',
		padding: '0 0 7 0',
		checked: '@{onlyCompleted}',
		boxLabel: '~~onlyCompletedLabel~~'
	}, {
		xtype: 'AceEditor',
		parser: 'groovy',
		readOnly: '@{isViewOnly}',
		height: 500,
		name: 'content',		
		flex: 1,		
		listeners: {
			change: function () {
				this.up('codeeditorpanel').fireEvent('contentchange', this, this);
			}
		}
	}],
	warningMsg: '@{warningMsg}',
	dockedItems: [
	{
		xtype: 'toolbar',
		hidden: '@{isViewOnly}',
		dock: 'bottom',
		margin: 0,
		padding: '0 15 10 15',
		items: [{
			xtype: 'label',
			cls: '@{editorClass}',
			margin: 0,
			padding: '0 0 10 0',
			listeners: {
				afterrender: function(label) {
					this.up('codeeditorpanel').fireEvent('warningmsgshowed', this, label);
				}
			}
		},'->', {
			margin: 0,
			name: 'close',		
			cls: 'at-med-btn'
		}]
	}]
});
