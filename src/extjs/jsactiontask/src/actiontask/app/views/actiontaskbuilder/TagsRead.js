glu.defView('RS.actiontaskbuilder.TagsRead', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: '@{classList}',
	header: '@{header}',
	hidden: '@{hidden}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: 15,
	items: [{
		xtype: 'displayfield',
		hidden: '@{!gridIsEmpty}',
		margin: 0,
		padding: 0,
		value: '~~tagsEmptyText~~'
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		hidden: '@{gridIsEmpty}',
		store: '@{store}',
		columns: [{
			dataIndex: 'name',
			filterable: true,
			flex: 2,
			header: '~~uname~~',
			hideable: false,
			sortable: true
		}, {
			header: '~~udescription~~',
			dataIndex: 'description',
			filterable: false,
			flex: 4,
			sortable: true,
			hideable: false
		}]
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},
		viewRendered: '@{viewRenderCompleted}'
	}	
});
