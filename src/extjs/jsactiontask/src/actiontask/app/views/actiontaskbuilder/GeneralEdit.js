glu.defView('RS.actiontaskbuilder.GeneralEdit', {
	parentLayout: 'quickAccess',
	xtype: 'form',
	title: '@{title}',
	cls: 'block-model subEditModel',
 	hidden: '@{hidden}',
 	header: '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '10 15 15 15',
	margin: '0 0 2.5 0',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		labelWidth: 130,
		margin: '4 0',
		padding: 0
	},
	generalEditTabContrl: '@{generalEditTabContrl}',
	setGeneralEditTabContrl: function() {
		// action task w/o an id means it is being created
		var hideOrShow = !this._vm.rootVM.id? 'hide': 'show';
		[
			this.down('#generalProperties'),
			this.down('#rolesEdit'),
			this.down('#optionsEdit')
		].forEach(function(tab) {
			// hide (show) General, Roles and Options tab on creating new (opening existing) task.
			tab? tab[hideOrShow].call(tab): tab;
		})

	},
	listeners: {
		afterrender: function (panel) {
			this.fireEvent('setform', this, this.form);
			this.fireEvent('afterRendered', this, this);

			if (!this._vm.isExistingActionTask) {
				this.form.reset();
			}

			this.form.isValid();

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize && !panel._vm.isNewActionTask) {
					panel._vm.close();
				}
			});

			panel.getEl().on('click', function() {
				if (panel._vm.collapsed && !panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});
		},

		setform: '@{setForm}',
		validitychange: '@{validityChange}',
		afterRendered: '@{viewRenderCompleted}'
	},

	dockedItems: [{
		xtype: 'toolbar',
		dock: 'bottom',
		padding : '0 15 10',
		items: ['->', {
			name: 'close',
			hidden: '@{isNewActionTask}',
			formBind: true,
			cls: 'rs-med-btn rs-btn-dark'
		}, {
			text: '~~save~~',
			hidden: '@{!isNewActionTask}',
			disabled: '@{!saveBtnIsEnabled}',
			cls: 'rs-med-btn rs-btn-dark',
			handler : '@{close}'
		}]
	}],

	items: [{
		xtype: 'textfield',
		name: 'namespace',
		hidden: '@{isExistingActionTask}',
		disabled: '@{isExistingActionTask}',	
		listeners: {			
			change: function (x, newValue) {
				this.fireEvent('namespacechangehandler', this, this, newValue);
			},
			namespacechangehandler: '@{namespaceChangeHandler}',
			blur: '@{namespaceBlurHandler}'
		}
	}, {
		xtype: 'displayfield',
		margin: 0,
		padding: 0,
		name: 'namespace',
		hidden: '@{isNewActionTask}',
		htmlEncode : true
	}, {
		xtype: 'textfield',
		name: 'menuPath',
		allowBlank: false,		
		validateOnBlur: false,
		listeners: {
			change: '@{menuPathChangeHandler}',
			blur: function () {
				this.fireEvent('menupathblurhandler', this, this);
			},
			menupathblurhandler: '@{menuPathBlurHandler}'
		}
	}, {
		xtype: 'textfield',
		name: 'name',
		hidden: '@{isExistingActionTask}',
		disabled: '@{isExistingActionTask}',	
	}, {
		xtype: 'displayfield',
		margin: 0,
		padding: 0,
		name: 'name',
		htmlEncode : true,
		hidden: '@{isNewActionTask}'
	}, {
		xtype: 'combobox',
		name: 'type',
		hidden: '@{isTypeComboHidden}',
		disabled: '@{isTypeComboHidden}',
		cls: 'no-input-border combo',
		editable: false,
		forceSelection: true,
		displayField: 'text',
		valueField: 'value',
		queryMode: 'local',
		value : '@{type}',
		maxWidth: 500,
		store: '@{typeStore}'
	}, {
		xtype: 'displayfield',
		name: 'type',
		hidden: '@{isProcessFieldHidden}',
		margin: '0 0 5 0',
		padding: 0
	}, {
		xtype: 'numberfield',
		cls: 'no-input-border combo',
		name: 'timeout',
		maxWidth: 500,
		minValue: 1,
		listeners: {
			blur: '@{notifyDirty}'
		}
	}, {
		xtype: 'fieldcontainer',
		layout: 'hbox',
		items: [{
			xtype: 'triggerfield',
			name: 'assignedToUserName',
			fieldLabel: '~~assignedTo~~',
			labelWidth: 130,
			flex: 1,
			editable: false,
			allowBlank: true,
			cls: 'no-input-border combo',
			triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
			maxWidth: 500,
			onTriggerClick: function() {
				this.fireEvent('handleTriggerClick', this);
			},
			listeners: {
				handleTriggerClick: '@{assignedToClicked}',
				blur: '@{notifyDirty}'
			}
		}, {
			xtype: 'image',
			cls: 'rs-btn-edit',
			margin: '6 0 0 10',
			hidden: '@{jumpToAssignedToIsHidden}',
			listeners: {
				handler: '@{jumpToAssignedTo}',
				render: function(image) {
					image.getEl().on('click', function() {
						image.fireEvent('handler', this);
					}, this);
				}
			}
		}]
	}, {
		xtype: 'textfield',
		name: 'summary',
		allowBlank: true,
		maxLength: 300,
		enforceMaxLength: true,
		listeners: {
			blur: '@{notifyDirty}'
		}
	}, {
		xtype: 'container',

		layout: {
			type: 'hbox',
			align: 'stretch'
		},

		items: [{
			xtype: 'label',
			text: '~~generalDescription~~',
			margin: '0 5 0 0',
			padding: 0,
			width: 130
		}, {
			xtype: 'container',
			margin: 0,
			padding: 0,
			flex: 1,
			layout: 'fit',

			resizable: {
				handles: 's',
				pinned: true
			},

			items: [{
				xtype: 'textarea',
				maxLength: 4000,
				enforceMaxLength: true,
				margin: 0,
				flex: 1,
				name: 'description',
				hideLabel: true,
				allowBlank: true,
				listeners: {
					blur: '@{notifyDirty}'
				}
			}]
		}]
	}, {
		xtype: 'checkbox',
		name: 'active',
		hideLabel: true,
		uncheckedValue: '',
		boxLabel: '~~active~~',
		margin: '0 0 3 135'
	}, {
		xtype: 'checkbox',
		name: 'logResult',
		hideLabel: true,
		uncheckedValue: '',
		boxLabel: '~~logResult~~',
		margin: '0 0 3 135'
	}, {
		xtype: 'checkbox',
		name: 'displayInWorksheet',
		hideLabel: true,
		uncheckedValue: '',
		boxLabel: '~~displayInWorksheet~~',
		margin: '0 0 3 135'
	}]
});
