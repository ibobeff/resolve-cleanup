glu.defView('RS.actiontaskbuilder.ExpressionBlock', {
	xtype: 'container',
	layout: { 
		type: 'column'
	},
	cls: 'expression-block',
	items: '@{expressions}',
	border: 1,
	shrinkWrap: 3,
	flex: 1,
	style: {
		borderColor: '#aaa',
		borderStyle: 'solid'
	}
});