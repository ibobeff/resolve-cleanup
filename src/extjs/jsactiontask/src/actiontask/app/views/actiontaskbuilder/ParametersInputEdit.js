glu.defView('RS.actiontaskbuilder.ParametersInputEdit', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: 'block-model subEditModel',
 	hidden: '@{hidden}',
 	header: '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: 15,
	margin: '0 0 2.5 0',
	items: [{
		xtype: 'displayfield',
		value: '~~inputParametersEmptyText~~',
		hidden: '@{hasInputParameters}'
	},{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		hidden: '@{!hasInputParameters}',
		store: '@{inputParametersStore}',
		features: [{
			ftype: 'grouping',
			groupField: 'ugroupName',
			groupHeaderTpl: '{name}',
			enableNoGroups: false,
			enableGroupingMenu : false
		}],
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		selModel: {
			selType: 'cellmodel'
		},
		columns: [{
			header: '~~name~~',
			dataIndex: 'uname',
			editor: {
				xtype: 'textfield',
				allowBlank: false,
				regex: RS.common.validation.VariableName,
				regexText: '~~validKeyCharacters~~'
			},
			hideable: false,
			flex: 2,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~description~~',
			dataIndex: 'udescription',
			editor: {
				xtype:'textarea',
				grow: true,
				flex: 1
			},
			hideable: false,
			flex: 4,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~defaultValue~~',
			dataIndex: 'udefaultValue',
			editor: {
				xtype: 'textarea',
				grow: true
			},
			hideable: false,
			flex: 4,
			renderer: RS.common.grid.plainRenderer()
		}, {
			xtype: 'checkcolumn',
			align: 'center',
			header: '~~encrypt~~',
			dataIndex: 'uencrypt',
			filterable: false,
			groupable: false,
			flex: 1,
			stopSelection: false
		}, {
			xtype: 'checkcolumn',
			align: 'center',
			header: '~~required~~',
			dataIndex: 'urequired',
			groupable: false,
			flex: 1,
			stopSelection: false
		}, {
			xtype: 'checkcolumn',
			align: 'center',
			header: '~~protected~~',
			dataIndex: 'uprotected',
			groupable: false,
			flex: 1,
			stopSelection: false
		}, {
			xtype: 'actioncolumn',
			hideable: false,
			sortable: false,
			align: 'center',
			width: 45,
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record) {
					view.ownerCt._vm.removeParameter(record);
				}
			}]
		}],
		listeners : {
			validateedit : function(editor, context){
				if(context.field == "uname"){
					//Find the correct store (nested store because of the grouping feature)
					var store = context.store.store;
					var newVal = context.value;
					var currentRowIdx = context.rowIdx;
					if(store.findExact(context.field, newVal) != currentRowIdx && store.findExact(context.field, newVal) != -1){
						this._vm.displayDuplicateNameMsg();
						return false;
					}
				}
			},
		}
	}, {
		xtype: 'fieldset',
		title : '~~createGroupTitle~~',
		margin: '10 0 0 0',
		padding: '5 15 10 15',
		items: [{
			xtype: 'form',
			layout: 'vbox',
			margin: 0,
			padding: 0,
			items: [{
				xtype: 'textfield',
				labelWidth: 130,
				width: 500,
				margin: '0 0 10 0',
				padding: 0,
				name: 'newGroupName'				
			}, {
				xtype: 'button',
				name: 'addNewGroup',
				cls: 'rs-small-btn rs-btn-dark',
				margin: '0 0 0 135',			
				listeners: {
					click: function () {
			        	setTimeout(function () {
							this.reset();
						}.bind(this.up('form').getForm()), 50);
			        }
			    }
			}]
		}]
	}, {
		xtype: 'fieldset',
		title : '~~createInputTitle~~',
		margin: '10 0 0 0',
		padding: '5 15 10 15',
		items: [{
			xtype: '@{detail}'
		}]
	}],
	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});

			panel.getEl().on('click', function() {
				if (panel._vm.collapsed && !panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	},

	dockedItems: [{
		xtype: 'toolbar',
		dock: 'bottom',
		padding : '0 15 10 15',
		items: ['->', {
			name: 'close',
			cls: 'rs-med-btn rs-btn-dark',
			disabledCls: 'link-button-disabled'
		}]
	}]
});

