glu.defView('RS.actiontaskbuilder.TestControl',{
	flex : 1,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		cls : 'rs-dockedtoolbar',
		defaults : {
			cls: 'rs-small-btn rs-btn-dark',		
		},
		items:['executeTest']
	}],
	items: [{
		xtype: 'tabpanel',
		height : 450,
		flex : 1,
		cls: 'rs-tabpanel-dark',
		activeItem : '@{activeTestSample}',
		defaults : {
			flex : 1
		},
		items: [{
			xtype: 'textarea',
			title: '~~defaultTestSampleTitle~~',
			closable: false,
			value : '@{defaultTestSample}'		
		},{
			iconCls: 'icon-large icon-plus',
			testTabCounter: 1,
			listeners: {
				beforeactivate: function(addTab){
					var parent = addTab.up();
					var numOfTabs = parent.items.length;					
					if(numOfTabs < 8){
						var testSampleNumber = addTab.testTabCounter + 1;
						addTab.testTabCounter++;
						var testSampleTitle = 'Test Sample ' + testSampleNumber; //Use this as id to register it to vm
						parent.insert(numOfTabs - 1,{
							xtype: 'textarea',
							title: testSampleTitle,
							closable : true
						})						
					}
					return false;
				}				
			}
		}],
		listeners : {
			afterrender : function(panel){
				this._vm.registerTestTab(panel);
			}
		}
	},{
		xtype: 'displayfield',
		value: '@{testResultTitle}',
		htmlEncode: false,	
		margin: '10 0 0 0'
	},{
		xtype: 'grid',
		minHeight : 150,
		cls : 'rs-grid-dark',
		store: '@{testResultStore}',
		columns: [{
			header: 'Name',
			dataIndex: 'name',
			width: 200
		}, {
			header: 'Value',
			dataIndex: 'value',
			flex: 1,
			renderer: function(value) {
				return Ext.util.Format.nl2br('<pre>' + Ext.String.htmlEncode(value) + '</pre>')
			}
		}]
	}]	
	
})