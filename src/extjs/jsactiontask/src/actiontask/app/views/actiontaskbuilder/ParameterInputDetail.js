glu.defView('RS.actiontaskbuilder.ParameterInputDetail', {
	xtype: 'form',	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults : {
		margin : '4 0'
	},
	items: [{
		xtype: 'container',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},		
		items: [{
			xtype: 'textfield',
			labelWidth: 130,
			allowBlank: false,
			validateOnBlur: true,
			name: 'uname',
			margin: '0 10 0 0',
			regex: RS.common.validation.VariableName,
			regexText: '~~validKeyCharacters~~',			
			flex: 2
		}, {
			xtype: 'combobox',
			cls: 'no-input-border combo',
			editable: false,
			hideLabel: true,
			labelWidth: 130,
			displayField: 'text',
			valueField: 'value',
			queryMode: 'local',
			name: 'ugroupName',
			value: '@{ugroupName}',
			store: '@{groupStore}',	
			flex: 1
		}]
	}, {
		xtype: 'container',		
		flex: 1,		
		layout: {
			type: 'hbox',
			align: 'stretchmax'
		},
		items: [{
			xtype: 'displayfield',
			width: 130,
			margin: 0,
			value: '~~parameterDescription~~'
		}, {
			xtype: 'container',
			flex: 1,
			margin: '0 0 6 0',
			padding: 0,
			cls: 'reverse-handle-borders',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},			
			resizable: {
				handles: 's',
				pinned: true
			},
			items: [{
				xtype: 'textarea',
				name: 'udescription',
				rows: 3,
				flex: 1,
				fieldLabel: null,
				labelWidth: 0,
				margin: '0 0 0 5',
				padding: 0
			}]
		}]
	}, {
		xtype: 'container',
		flex: 1,		
		layout: {
			type: 'hbox',
			align: 'stretchmax'
		},
		items: [{
			xtype: 'displayfield',
			width: 130,
			margin: 0,
			value: '~~labelDefaultValue~~'
		}, {
			xtype: 'container',
			flex: 1,
			margin: 0,
			padding: 0,
			cls: 'reverse-handle-borders',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},			
			resizable: {
				handles: 's',
				pinned: true
			},
			items: [{
				xtype: 'textarea',
				name: 'udefaultValue',
				rows: 3,
				flex: 1,
				fieldLabel: null,
				labelWidth: 0,
				margin: '0 0 0 5',
				padding: 0
			}]
		}]
	}, {
		xtype: 'checkbox',
		margin: '4 0 0 135',		
		name: 'uencrypt',
		hideLabel: true,
		boxLabel: '~~encrypt~~'
	}, {
		xtype: 'checkbox',
		margin: '4 0 0 135',		
		name: 'urequired',
		hideLabel: true,
		boxLabel: '~~required~~'
	}, {
		xtype: 'checkbox',
		margin: '4 0 0 135',		
		name: 'uprotected',
		hideLabel: true,
		boxLabel: '~~protected~~'
	},{
		xtype: 'button',
		cls: 'rs-small-btn rs-btn-dark',
		margin: '8 0 4 135',
		maxWidth : 90,
		name: 'addInput',
		text: '@{addInputTitle}',			
		formBind: true,
		listeners: {
			click: function (view) {
				var form = this.up('form').getForm();
				form.findField('ugroupName').resetOriginalValue();

				setTimeout(function () {
					this.reset();
				}.bind(form), 50);
			}
		}
	}]
});