glu.defView('RS.actiontaskbuilder.ContentRead', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: 'block-model',
 	header: '@{header}',
 	hidden: '@{hidden}',
 	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '10 15 12 15',
	minHeight: '@{minHeight}',
	height: '@{height}',

	resizable: {
		handles: 's',
		pinned: true,
		minHeight: '@{minResize}'
	},	

	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	defaults: {
		labelWidth: 130,
		margin: '0 0 10 0'
	},

	items: [{
		xtype: 'displayfield',
		name: 'command',
		hidden: '@{!commandIsVisible}'
	}, {
		xtype: 'container',
		hidden: '@{contentIsEmpty}',
		flex: 1,
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		defaults: {
			margin: 0
		},
		items: [{
			xtype: 'textfield',
			labelWidth: 130,
			name:'contentLabel',
			fieldLabel: '@{contentLabel}',
			width: 135,
			hidden: '@{!commandIsVisible}'
		}, {
			xtype: 'AceEditor',
			parser: '@{editorType}',
			readOnly: true,
			height: '@{height}',
			name: 'content',		
			cls: 'editor-disabled',
			flex: 1,
			highlightActiveLine: false, 
			highlightGutterLine: false,
			showCursor: false		
		}]
	}, {
		xtype: 'displayfield',
		hidden: '@{!contentIsEmpty}',
		margin: 0,
		padding: 0,
		value: '@{contentEmptyText}'
	}],	

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},		
		viewRendered: '@{viewRenderCompleted}'
	}
});