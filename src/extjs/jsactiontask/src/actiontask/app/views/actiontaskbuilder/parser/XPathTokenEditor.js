glu.defView('RS.actiontaskbuilder.XPathTokenEditor', {
	xtype : 'fieldset',
	title : '@{title}',
	cls : 'xpath-detail',
	padding : 0,
	defaults : {
		margin : '4 0',
		labelWidth : 80		
	},
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	items : [{
		xtype :  'component',
		html : '~~noProperty~~',
		hidden : '@{!noProperty}',
		padding : '0 10'
	},{
		xtype : 'container',		
		flex : 1,
		defaults : {
			padding : '0 10'
		},
		layout : {
			type : 'vbox',
			align : 'stretch'
		},
		hidden : '@{noProperty}',
		items : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			items : [{			
				cls : 'rs-btn-dark rs-small-btn',
				name : 'update', 
			},{			
				cls : 'rs-btn-dark rs-small-btn',
				name : 'removeNode',
			}]
		},{
			xtype : 'textfield',
			name : 'selector',			
			labelWidth : 80
		},{
			xtype : 'component',
		 	html : '~~predicates~~',
		 	margin : '5 0'
		},{
			xtype : 'panel',
			padding : 0,
			flex : 1,
			autoScroll : true,		
			dockedItems : [{
				xtype : 'toolbar',
				padding : '0 10',
				cls : 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-small-btn rs-btn-dark',
				},
				items : [{
					xtype : 'button',
					text : 'Add',
					plain : true,
					menu : ['addTextPredicate', 'addAttrPredicate', 'addCustPredicate']
				},{
					xtype : 'tbseparator'
				},{
					fieldLabel : 'Condition',
					xtype: 'radiofield',
					name: 'operator',
					value: '@{and}',			
					boxLabel: '~~and~~',
					labelWidth : 80
				}, {
					xtype: 'radiofield',
					name: 'operator',
					value: '@{!and}',
					hideLabel: true,
					boxLabel: '~~or~~'
				}]
			}],
			items : [{
				xtype : 'component',
				padding : '0 10',
				html : '~~noPredicate~~',
				hidden : '@{!noPredicate}',
			},{
				xtype : 'container',
				overflowY : 'scroll',
				padding : '0 0 0 10',				
				items : '@{predicates}',			
				hidden : '@{noPredicate}',
			}]
		}]
	}]
});
glu.defView('RS.actiontaskbuilder.XPathTextPredicate', {
	xtype : 'container',
	layout: 'hbox',
	defaults: {
		hideLabel: true,
		labelWidth : 80
	},	
	items: [{
		xtype: 'displayfield',		
		hideLabel: false,
		fieldLabel: '~~text~~'		
	}, {
		xtype: 'combo',	
		name: 'condition',
		displayField: 'name',
		valueField: 'value',
		store: '@{..textConditionStore}',
		width: 120,
		queryMode: 'local',
		margin : '0 5 0 0',
	}, {
		xtype: 'textfield',		
		name: 'value',
		flex: 1,	
		margin : '0 5 0 0',
	}, {
		xtype: 'container',	
		width : 20,	
		items : [{
			xtype : 'component',						
			style: {				
				'color': '#999',
				'padding-left': '3px',
				'padding-top': '3px',
			},			
			autoEl: {				
				cls: 'icon-minus-sign-alt icon-large'
			},
			listeners: {
				render: function() {
					this.getEl().on('mouseover', function() {
						this.setStyle('cursor', 'pointer');
					});
					this.getEl().on('click', function(e) {
						e.stopPropagation();
						this.fireEvent('remove');
					}, this);
				},
				remove: '@{remove}'
			}	
		}]
	}]
});

glu.defView('RS.actiontaskbuilder.XPathAttrPredicate', {
	xtype : 'container',
	layout: 'hbox',
	defaults: {
		hideLabel: true,
		labelWidth : 80
	},	
	items: [{
		xtype: 'displayfield',
		hideLabel: false,	
		fieldLabel: '~~attribute~~'	
	}, {
		xtype: 'combo',		
		name: 'name',
		store: '@{..attributeStore}',
		width: 120,
		displayField: 'name',
		valueField: 'name',
		queryMode: 'local',
		margin : '0 5 0 0',
	}, {
		xtype: 'combo',
		name: 'condition',
		store: '@{..attrConditionStore}',
		width: 120,
		displayField: 'name',
		valueField: 'value',
		queryMode: 'local',
		margin : '0 5 0 0',
	}, {
		xtype: 'textfield',		
		name: 'value',
		flex: 1,
		margin : '0 5 0 0',
	}, {
		xtype: 'container',	
		width : 20,	
		items : [{
			xtype : 'component',						
			style: {				
				'color': '#999',
				'padding-left': '3px',
				'padding-top': '3px',
			},			
			autoEl: {				
				cls: 'icon-minus-sign-alt icon-large'
			},
			listeners: {
				render: function() {
					this.getEl().on('mouseover', function() {
						this.setStyle('cursor', 'pointer');
					});
					this.getEl().on('click', function(e) {
						e.stopPropagation();
						this.fireEvent('remove');
					}, this);
				},
				remove: '@{remove}'
			}	
		}]
	}]
});

glu.defView('RS.actiontaskbuilder.XPathCustomizedPredicate', {
	xtype : 'container',
	layout: 'hbox',
	margin : '0 0 5 0',
	items: [{
		xtype: 'textfield',	
		fieldLabel: '~~customized~~',
		name: 'value',		
		flex: 1,
		labelWidth : 80,
		margin : '0 5 0 0',
	}, {
		xtype: 'container',	
		width : 20,	
		items : [{
			xtype : 'component',								
			style: {				
				'color': '#999',
				'padding-left': '3px',
				'padding-top': '3px',
			},			
			autoEl: {				
				cls: 'icon-minus-sign-alt icon-large'
			},
			listeners: {
				render: function() {
					this.getEl().on('mouseover', function() {
						this.setStyle('cursor', 'pointer');
					});
					this.getEl().on('click', function(e) {
						e.stopPropagation();
						this.fireEvent('remove');
					}, this);
				},
				remove: '@{remove}'
			}	
		}]
	}]
})