glu.defView('RS.actiontaskbuilder.MarkupCapture',{
	cls : 'rs-modal-popup',
	title : '~~markupCapture~~',
	padding : 15,
	modal : true,
	width : 600,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		margin : '4 0',
		labelWidth : 120
	},
	items : [{
		xtype : 'textfield',
		name : 'variable',
	},{
		xtype : 'combo',
		name : 'containerType',
		store : '@{containerTypeStore}',
		displayField : 'value',
		valueField : 'value',
		queryMode : 'local',
		editable : false,
	},{
		xtype : '@{patternProcessor}',
		hidden : '@{!..parserTypeIsText}'
	},{
		xtype : 'displayfield',
		name : 'column',
		hidden : '@{!parserTypeIsTable}'
	},{
		xtype : 'textarea',
		name : 'xpath',
		hidden : '@{!parserTypeIsXML}'
	}],
	buttons : [{
		name : 'add',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})