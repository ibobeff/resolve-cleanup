glu.defView('RS.actiontaskbuilder.Version',{
	xtype: 'grid',
	cls : 'rs-grid-dark',
	name: 'versionsList',	
	selected: '@{versionsListSelections}',
	columns: '@{versionsListColumns}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [/*'viewVersion', 'compareVersion', */
		'rollbackVersion',
		'->', {
			iconCls: 'x-tbar-loading',
			handler: '@{loadVersion}',
			tooltip: '~~refresh~~'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'bottom',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		padding: '10 0 5 0',
		items: [{
			xtype: 'toolbar',
			defaults: {
				text: '',
			},
			cls: 'version-pagination',
			padding: '@{paginationPadding}',
			items: [{
				xtype: 'text',
				text: '@{paginationText}',
			}, {
				name: 'firstPage',
				disabled: '@{firstDisabled}',
				cls: 'pagination first-page',
				tooltip: '~~firstPage~~'
			}, {
				name: 'previousPage',
				disabled: '@{previousDisabled}',
				cls: 'pagination previous-page',
				tooltip: '~~previousPage~~'
			}, {
				name: 'nextPage',
				disabled: '@{nextDisabled}',
				cls: 'pagination next-page',
				tooltip: '~~nextPage~~'
			}, {
				name: 'lastPage',
				disabled: '@{lastDisabled}',
				cls: 'pagination last-page',
				tooltip: '~~lastPage~~'
			}]
		}, '->', '->', {	
			name : 'selectVersion',
			xtype: 'button',
			cls: 'rs-med-btn rs-btn-dark',
		}, {
			name : 'close',
			xtype: 'button',
			cls : 'rs-med-btn rs-btn-dark',
		}]
	}],    
	asWindow : {
		title: '~~versions~~',
		modal: true,
		cls : 'rs-modal-popup',
		padding : '15 15 0 15',
		listeners: {
			beforeshow: function() {
				this.setWidth(Math.round(Ext.getBody().getWidth() * 0.8));
				this.setHeight(Math.round(Ext.getBody().getHeight() * 0.8));
			}
		}
	}
});