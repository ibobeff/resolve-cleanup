glu.defView('RS.actiontaskbuilder.ReferencesRead', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: '@{classList}',
	header: '@{header}',
	hidden: '@{hidden}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: 15,
	items: [{
		xtype: 'displayfield',
		hidden: '@{!gridIsEmpty}',
		margin: 0,
		padding: 0,
		value: '~~referencesEmptyText~~'
	}, {
		id: 'referencesTab',
		autoScroll: true,
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'references',
		store: '@{referencesStore}',
		hidden: '@{gridIsEmpty}',
		columns: [{
			header: '~~name~~',
			dataIndex: 'name',
			hideable: false,
			filterable: false,
			sortable: true,
			flex: 1
		}, {
			header: '~~refInContent~~',
			dataIndex: 'refInContent',
			filterable: false,
			width: 100,
			renderer: RS.common.grid.booleanRenderer()
		}, {
			header: '~~refInMain~~',
			dataIndex: 'refInMain',
			filterable: false,
			width: 100,
			renderer: RS.common.grid.booleanRenderer()

		}, {
			header: '~~refInAbort~~',
			dataIndex: 'refInAbort',
			filterable: false,
			width: 100,
			renderer: RS.common.grid.booleanRenderer()
		}],
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTooltip: RS.common.locale.viewColumnTooltip,
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'name'
		},
		listeners: {
			editAction: '@{editReference}'
		}
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},
		viewRendered: '@{viewRenderCompleted}',
		hello: function() {return null}
	},
});