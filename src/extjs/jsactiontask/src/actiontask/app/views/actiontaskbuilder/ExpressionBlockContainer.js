glu.defView('RS.actiontaskbuilder.ExpressionBlockContainer', {
	xtype: 'fieldset',
	cls: 'expression-block-container-fieldset',
	title: '@{title}',
	border: 2,
	style: '@{style}',
	hidden: '@{isHidden}',
	margin: '0 0 10 0',
	items: [{
		xtype: 'container',
	    layout: {
	    	type: 'hbox',
	    	align: 'stretchmax'
	    },
	    cls: 'expression-block-container-fieldset-inner',
	    defaults: {
	        margin: '5 10 10 10'
	    },
	    items: [{
	        xtype: 'displayfield',
	        fieldStyle: {
	            fontWeight: 'bold'
	        },
	        hidden: '@{isAndHidden}',
	        height: '100%',
	        value: '~~AND~~'
	    }, {
			xtype: 'container',
			flex: 1,
			cls: 'expression-block-container',
			items: '@{blocks}'
		}]
	}],
	listeners : {
		afterrender : function(panel){
			this._vm.on('onClear', function(){
				panel.clearManagedListeners();
			})
		}
	}
});