glu.defView('RS.actiontaskbuilder.PatternTag',{
	xtype : 'component',
	hidden : '@{hidden}',	
	margin : '0 8 8 0',
	cls : 'text-parser-tag @{selectedCss}',	
	html : '@{displayName}',
	listeners: {
		render: function(panel) {
			panel.getEl().on('click', function() {
				panel.fireEvent('select', panel)
			})
		},
		select: '@{select}'		
	}
})