glu.defView('RS.actiontaskbuilder.AssessorRead', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: '@{classList}',
	hidden: '@{hidden}',
	header : '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '10 15 12 15',
	minHeight: '@{minHeight}',
	height: '@{height}',
	
	resizable: {
		handles: 's',
		pinned: true,
		minHeight: '@{minResize}'
	},
	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},	

	defaults: {
		margin: '0 0 10 0'
	},

	items: [{
		xtype: 'checkbox',
		disabled: true,
		disabledCls: 'disabled-readable',
		padding: 0,
		checked: '@{onlyCompleted}',
		boxLabel: '~~onlyCompletedLabel~~',
		hidden: '@{contentIsEmpty}'
	}, {
		xtype: 'AceEditor',
		parser: 'groovy',
		readOnly: true,
		height: '@{height}',
		name: 'content',	
		cls: 'editor-disabled',
		flex: 1,
		highlightActiveLine: false, 
		highlightGutterLine: false,
		showCursor: false,
		hidden: '@{contentIsEmpty}',		
	}, {
		xtype: 'displayfield',
		hidden: '@{!contentIsEmpty}',
		margin: 0,
		padding: 0,
		value: '~~sourceCodeSectionEmptyText~~'
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},
		viewRendered: '@{viewRenderCompleted}'
	}	
});
