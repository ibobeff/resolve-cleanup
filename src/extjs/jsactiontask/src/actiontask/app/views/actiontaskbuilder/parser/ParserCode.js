glu.defView("RS.actiontaskbuilder.ParserCode",{
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	items : [{
		xtype : 'checkboxfield',
		name : 'autoGenCheck',
		readOnly : '@{viewOnly}',
		hideLabel : true,		
		boxLabel : '~~autoGenCheck~~',
		listeners : {
			change : '@{checkBoxChange}'
		}
	},{
		xtype: 'AceEditor',
		name: 'codeContent',
		cls: '@{editorCls}',
		parser: 'groovy',
		flex : 1,
		readOnly : '@{codeContentIsReadOnly}',	
		minHeight : 500
	}]
})