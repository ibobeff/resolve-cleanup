glu.defView('RS.actiontaskbuilder.ContentEdit', {
	parentLayout: 'quickAccess',
	xtype: 'codeeditorpanel',
	title: '@{title}',
	cls: '@{classList}',
 	hidden: '@{hidden}',
 	header: '@{header}',	
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '10 15 15 15',
	minHeight: '@{minHeight}',
	height: '@{height}',
	resizable: {
		handles: 's',
		pinned: true,
		minHeight: '@{minResize}',
		listeners: {
			resize: function (panel, width, height) {
				this.target.fireEvent('resizesection', this, height);
			}
		}
	},	

	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	defaults: {
		labelWidth: 130,
		margin: '0 0 10 0',
	},

	items: [{
		layout: {
			type: 'hbox'
		},
		items: [{
			labelWidth: 130,
			flex: 1,
			margin: '0 0 0 0',
			xtype: 'textfield',
			name: 'command'
		}, {
			xtype: 'button',
			hidden: '@{!commandIsVisible}',
			cls: 'rs-small-btn rs-btn-dark',
			name: 'addInputFile',
			margin: '0 0 0 10',
			height : '100%',
			padding: '0 10'
		}]
	}, {
		xtype: 'combobox',
		cls: 'no-input-border combo',
		editable: false,
		displayField: 'text',
		valueField: 'value',
		queryMode: 'local',
		name: 'syntax',
		value: '@{editorType}',
		maxWidth: 500,
		store: '@{syntaxStore}'
	}, {
		xtype: 'container',
		flex: '@{editorFlexValue}',
		
		layout: {
			type: 'hbox',
			align: 'stretch'
		},

		listeners: {
			afterlayout: function (panel) {
				if (panel.hidden) {
					this.ownerCt._vm.shrinkPanel();
				} else {
					this.ownerCt._vm.growPanel();
				}
			}
		},

		defaults: {
			margin: 0
		},
		
		items: [{
			xtype: 'textfield',
			labelWidth: 130,
			name:'contentLabel',
			fieldLabel: '@{contentLabel}',
			width: 135,
			hidden: '@{!syntaxIsVisible}'
		}, {
			xtype: 'AceEditor',			
			name: 'content',
			height: '@{editorHeight}',
			parser: '@{editorType}',	
			flex : 1,			
			listeners: {
				change: function () {
					this.up('codeeditorpanel').fireEvent('contentchange', this, this);
				}
			}
		}]
	}],	

	dockedItems: [{
		xtype: 'toolbar',		
		padding : '0 15 10',
		dock: 'bottom',
		items: ['->', {			
			name: 'close',
			cls: 'rs-med-btn rs-btn-dark'
		}]
	}],

	listeners: {
		contentchange: '@{contentChange}',
		resizesection: '@{resizeSection}',
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});	

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	}	
});