glu.defView('RS.actiontaskbuilder.SummaryAndDetailsRead', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: 'block-model',
 	header: '@{header}',
	hidden: '@{hidden}',
 	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: 15,
	defaults: {
		margin: '4 0'		
	},
	items: [{
		xtype: 'component',
		html: '~~summary~~',
		margin : '0 0 10 0'	
	},{
		xtype : 'component',
		html : '~~noSummaryDefined~~',
		hidden : '@{!noSummaryRule}'	
	},{
		xtype : 'grid',
		hidden : '@{noSummaryRule}',
		store : '@{summaryRuleStore}',
		cls : 'rs-grid-dark summary-detail-grid',
		columns : [{
			header : '~~severity~~',
			dataIndex : 'severity',
			sortable : false,
			align : 'center',
			width : 150,
			renderer : function(value, metaData){			
				metaData.tdAttr = 'status=' + value.toUpperCase();
				return value;     
			}
		},{
			header : '~~condition~~',
			dataIndex : 'condition',
			sortable : false,
			align : 'center',
			width : 150,
			renderer : function(value, metaData){			
				metaData.tdAttr = 'status=' + value.toUpperCase();
				return value;     
			}
		},{
			header : '~~textToDisplay~~',
			dataIndex : 'display',
			sortable : false,
			editor : 'textarea',
			flex : 1,
			renderer : RS.common.grid.plainRenderer()
		}]
	},{
		xtype: 'container',
		hidden: '@{!multipleSummaryRule}',
		layout: {
			type: 'hbox',
			align: 'middle'
		},
		margin: '10 0 0 0',
		items: [{
            xtype: 'button',
            iconCls: 'icon-info-sign',
            text: '',
            preventDefault: true,
            disabled: true,
		    height: 30,
		    width: 30,
   		    margin: '5 10 0 0',
		    padding: '5 5 5 7',
			style: {
				color: '#00daff',
				backgroundColor: '#d5f9ff',
				border: '1px solid #00daff',
				fontSize: '17px'
			}
        }, {
			xtype: 'component',
			html: '~~multipleRuleMessage~~',			
			flex: 1
		}]
	},{
		xtype: 'component',
		html: '~~details~~',
		margin : '25 0 10 0'	
	},{
		xtype: 'component',	
		html: '~~noDetailDefined~~',
		hidden: '@{!noDetailRule}',
	},{
		xtype : 'grid',
		hidden : '@{noDetailRule}',
		store : '@{detailRuleStore}',
		cls : 'rs-grid-dark summary-detail-grid',
		columns : [{
			header : '~~severity~~',
			dataIndex : 'severity',
			align : 'center',
			sortable : false,
			width : 150,
			renderer : function(value, metaData){			
				metaData.tdAttr = 'status=' + value.toUpperCase();
				return value;     
			}
		},{
			header : '~~condition~~',
			dataIndex : 'condition',
			align : 'center',
			sortable : false,
			width : 150,
			renderer : function(value, metaData){			
				metaData.tdAttr = 'status=' + value.toUpperCase();
				return value;     
			}
		},{
			header : '~~textToDisplay~~',
			dataIndex : 'display',
			editor : 'textarea',
			sortable : false,
			flex : 1,
			renderer : RS.common.grid.plainRenderer()
		}]
	},{
		xtype: 'container',
		hidden: '@{!multipleDetailRule}',
		layout: {
			type: 'hbox',
			align: 'middle'
		},
		margin: '10 0 0 0',
		items: [{
            xtype: 'button',
            iconCls: 'icon-info-sign',
            text: '',
            preventDefault: true,
            disabled: true,
		    height: 30,
		    width: 30,
   		    margin: '5 10 0 0',
		    padding: '5 5 5 7',
			style: {
				color: '#00daff',
				backgroundColor: '#d5f9ff',
				border: '1px solid #00daff',
				fontSize: '17px'
			}
        }, {
			xtype: 'component',
			html: '~~multipleRuleMessage~~',			
			flex: 1
		}]
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},
		viewRendered: '@{viewRenderCompleted}'
	}	
});
