glu.defView('RS.actiontaskbuilder.dummy') //to make sure the ns is init before creating the factory
RS.actiontaskbuilder.views.quickAccessFactory = function(actualView) {
	if (actualView.listeners) {
		Ext.apply(actualView.listeners, {
			show: function(panel) {
				panel.fireEvent('showsectioncallback', panel);
			},
			expand: function(panel) {
				panel.fireEvent('incnumsectionsopen', panel);
			},
			beforeexpand: function(panel) {
				panel.fireEvent('toggleresizemaxbtn', panel);
				return true;
			},
			collapse: function(panel) {
				panel.fireEvent('decnumsectionsopen', panel);
				panel.fireEvent('collapsesectioncallback', panel);
			},
			beforecollapse: function(panel) {
				panel.fireEvent('toggleresizemaxbtn', panel);
				return true;
			},
			showsectioncallback: '@{showSectionCallback}',
			collapsesectioncallback: '@{collapseSectionCallback}',
			incnumsectionsopen: '@{incNumSectionsOpen}',
			decnumsectionsopen: '@{decNumSectionsOpen}',
			toggleresizemaxbtn: '@{toggleResizeMaxBtn}',
		});
	}
	return actualView;
}
