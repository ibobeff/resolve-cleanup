glu.defView('RS.actiontaskbuilder.ParametersRead', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: '@{classList}',
 	header: '@{header}',
	hidden: '@{hidden}',
 	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '0 15 15 15',
	defaults: {
		margin: 0,
		padding: 0
	},
	items: [{
		xtype: 'panel',
		defaults: {
			labelWidth: 130,
			margin: 0,
			padding: 0
		},
		items: [{
			xtype: 'displayfield',
			fieldStyle: {
				fontWeight: 'bold'
			},
			value: '~~inputParam~~'
		}, {
			xtype: 'displayfield',
			value: '~~inputParametersEmptyText~~',
			hidden: '@{hasInputParameters}'
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			hidden: '@{!hasInputParameters}',
			store: '@{inputParametersStore}',
			features: [{
				ftype: 'grouping',
				groupField: 'ugroupName',
				groupHeaderTpl: '{name}',
				enableNoGroups: false,
				enableGroupingMenu : false,
			}],
			columns: [{
				header: '~~name~~',
				dataIndex: 'uname',
				flex: 2,
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~description~~',
				dataIndex: 'udescription',
				flex: 4,
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~defaultValue~~',
				dataIndex: 'udefaultValue',
				flex: 4,
				renderer: RS.common.grid.plainRenderer()
			}, {
				renderer: RS.common.grid.booleanRenderer(),
				align: 'center',
				header: '~~encrypt~~',
				dataIndex: 'uencrypt',
				flex: 1
			}, {
				renderer: RS.common.grid.booleanRenderer(),
				align: 'center',
				header: '~~required~~',
				dataIndex: 'urequired',
				flex: 1
			}, {
				renderer: RS.common.grid.booleanRenderer(),
				align: 'center',
				header: '~~protected~~',
				dataIndex: 'uprotected',
				flex: 1
			}]
		}, {
			xtype: 'displayfield',
			fieldStyle: {
				fontWeight: 'bold'
			},
			value: '~~outputParam~~',
			padding: '10 0 0 0'
		}, {
			xtype: 'displayfield',
			value: '~~outputParametersEmptyText~~',
			hidden: '@{hasOutputParameters}'
		},{
			xtype: 'grid',
			cls : 'rs-grid-dark',
			hidden: '@{!hasOutputParameters}',
			store: '@{outputParametersStore}',
			columns: [{
				header: '~~name~~',
				dataIndex: 'uname',
				flex: 2,
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~description~~',
				dataIndex: 'udescription',
				flex: 4,
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~defaultValue~~',
				dataIndex: 'udefaultValue',
				flex: 4,
				renderer: RS.common.grid.plainRenderer()
			}, {
				renderer: RS.common.grid.booleanRenderer(),
				align: 'center',
				header: '~~encrypt~~',
				dataIndex: 'uencrypt',
				flex: 1
			}, {
				renderer: RS.common.grid.booleanRenderer(),
				align: 'center',
				header: '~~required~~',
				dataIndex: 'urequired',
				flex: 1
			}]
		}]
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},
		viewRendered: '@{viewRenderCompleted}'
	}
});
