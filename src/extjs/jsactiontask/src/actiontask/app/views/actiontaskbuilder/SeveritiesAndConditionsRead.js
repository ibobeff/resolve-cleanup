glu.defView('RS.actiontaskbuilder.SeveritiesAndConditionsRead', {
	parentLayout: 'quickAccess',
	title: '@{title}',		
	cls: '@{classList}',
 	hidden: '@{hidden}',
 	header: '@{header}',
	bodyPadding: '10 15',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		flex: 1,
		margin : '4 0'
	},
	items: [{
		xtype: 'component',		
		html: '~~severitiesTitle~~'
	}, {
		xtype: 'component',
		html: '~~noSeveritiesMessage~~',
		hidden: '@{isSeveritiesMessageHidden}'
	}, {
		xtype: 'container',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaults: {
			flex: 1
		},
		items: '@{severityItems}'
	}, {
		xtype: 'component',		
		html: '~~conditionsTitle~~'
	}, {
		xtype: 'component',
		html: '~~noConditionsMessage~~',
		hidden: '@{isConditionsMessageHidden}'
	}, {
		xtype: 'container',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaults: {
			flex: 1
		},
		items: '@{conditionItems}'
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},
		viewRendered: '@{viewRenderCompleted}'
	}	
});