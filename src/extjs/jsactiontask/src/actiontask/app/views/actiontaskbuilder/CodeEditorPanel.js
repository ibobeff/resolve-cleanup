Ext.define('RS.actiontaskbuilder.CodeEditorPanel', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.codeeditorpanel',
	margin: 0,
	padding: 0,

	// exporting getter/setter in order for glu binding to work
	config: {
		warningMsg: ''
	}
});