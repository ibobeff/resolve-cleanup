glu.defView("RS.actiontaskbuilder.ParserEdit",{
	parentLayout: 'quickAccess',
	title: '@{title}',
	hidden: '@{hidden}',
	cls: '@{classList}',
 	header: '@{header}',
 	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '0 15 10 15',
	layout: 'card',
	activeItem: '@{activeItem}',
	dockedItems:[{		
		xtype: 'toolbar',
		cls : 'rs-dockedtoolbar',
		padding : '5 15 0',
		defaults : {
			cls : 'rs-med-btn rs-btn-dark'
		},
		items: [{
			xtype: 'radiogroup',
			style : 'visibility:@{parserSwitchVisibility}',
			layout: 'hbox',		
			fieldLabel: '~~outputFormat~~',
			labelStyle : 'font-weight:bold',
			cls: 'bold-label rs-dockedtoolbar',
			labelWidth: 130,
			defaultType: 'radio',
			value : '@{parserType}',	
			defaults:{	
				margin: '0 20 0 0',
				name : 'outputFormat'	
			},
			items: [{
				boxLabel: 'Text',
				checked: '@{textParserIsActive}',
				inputValue : 'text'
			},{
				boxLabel: 'Table',
				checked: '@{tableParserIsActive}',
				inputValue : 'table'	
			},{
				boxLabel: 'XML',
				checked: '@{xmlParserIsActive}',
				inputValue : 'xml'
			}]
		},'->',{			
			name: 'markupAndCapture',
			margin : 0,
			iconCls: 'icon-large icon-reply-all rs-icon'		
		},'test']
	},{
		xtype: 'toolbar',	
		dock: 'bottom',	
		padding : '0 15 10 15',
		items: ['->', { 
			name: 'close', 
			cls : 'rs-med-btn rs-btn-dark'
		}]
	}],
	items: [{
		xtype : '@{parserControl}'
	},{
		xtype : '@{testControl}'
	}],	
	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					if (panel._vm.isViewOnly) {
						panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
					} else {
						panel._vm.close();
					}
				}
			});			
		},
		resize : function(panel){			
			if(panel.getWidth() < 1000)
				panel._vm.set('smallResolution', true);
			else 
				panel._vm.set('smallResolution', false);
		},	
		afterRendered: '@{viewRenderCompleted}'
	}
});