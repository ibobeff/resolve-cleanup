glu.defView('RS.actiontaskbuilder.AssignValuesToOutputsRead', {
	parentLayout: 'quickAccess',
	title: '@{title}',	
	cls: '@{classList}',
	hidden: '@{hidden}',
	header: '@{header}',
	bodyPadding: 15,
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,    
	resizable: false,
	items: [{
		xtype: 'displayfield',
		hidden: '@{hasValues}',
		margin: 0,
		padding: 0,
		value: '~~assignValuesToOutputsEmptyText~~' 
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		hidden: '@{!hasValues}',
		store: '@{valuesStore}',
		columns: [{
			flex: 1,
			header: '~~outputType~~',
			dataIndex: 'outputType',
			renderer : function(v){
				if(v == 'OUTPUTS')
					return 'OUTPUT';
				else if(v == 'FLOWS')
					return 'FLOW';
				else if(v == 'PARAMS')
					return 'PARAM';
				else 
					return v;
			},
		}, {
			flex: 1,
			header: '~~outputName~~',
			dataIndex: 'outputName'
		}, {
			flex: 1,
			header: '~~sourceType~~',
			dataIndex: 'sourceType',
			renderer : function(v){
				if(v == 'INPUTS')
					return 'INPUT';
				else if(v == 'FLOWS')
					return 'FLOW';
				else if(v == 'PARAMS')
					return 'PARAM';
				else 
					return v;
			},
		}, {
			flex: 1,
			header: '~~sourceName~~',
			dataIndex: 'sourceName'
		}]
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);			

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},
		viewRendered: '@{viewRenderCompleted}'
	}
});
