glu.defView('RS.actiontaskbuilder.MockEdit', {
	parentLayout: 'quickAccess',
	title: '@{title}',		
	cls: 'block-model',
 	header: '@{header}',
	hidden: '@{hidden}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: 15,	
	dockedItems: [{
		xtype: 'toolbar',	
		padding : '10 15 0 15',	
		items: [{ 
			name:'closeDetails', 
			cls: 'rs-small-btn rs-btn-dark',		
		}]
	}, {
		xtype: 'toolbar',		
		padding : '0 15 10',
		dock: 'bottom',
		items: ['->', {			
			name: 'close',
			cls: 'rs-med-btn rs-btn-dark'
		}]
	}],	
	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	
	items: [{
		name: 'Mock',
		xtype: 'container',
		items: [{
			xtype: 'displayfield',
			hidden: '@{!gridIsEmpty}',
			value: '~~emptyMockMsg~~'
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			hidden: '@{gridIsEmpty}',
			store: '@{mockStore}',
			listeners: {				
				afterrender: function(grid) {
					grid.fireEvent('rendercompleted', grid, grid);
				},
				rendercompleted: '@{mockRenderCompleted}',
				editmockdetails: '@{editMockDetails}',
				recordremoved: '@{recordRemoved}',
				validateedit: function(editor, context){
					return this._vm.validateNameOnEdit(context, 'uname', 'mockStore');
				}					
			},
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}],
			selModel: {
				selType: 'cellmodel'
			},
			userDateFormat: '@{..userDateFormat}',
			editDetailsTooltip: '@{editDetailsTooltip}',
			removeMockTooltip: '@{removeMockTooltip}',
			columns: [{
				xtype: 'actioncolumn',			
				hideable: false,
				sortable: false,
				align: 'center',			
				width: 45,			
				iconCls: 'rs-fa-details',
				items: [{
					getTip: function(v, meta, record) {
						return this.up('grid').editDetailsTooltip;
					},
					handler: function(view, rowIndex, colIndex, item, e, record ) {
						this.up('grid').fireEvent('editmockdetails', view, record);
					}
				}]
			}, {
				header: '~~name~~',
				dataIndex: 'uname',					
				hideable: false,
				sortable: true,
				flex: 1,
				editor: {
					xtype:'textfield',
					grow: true,
					flex: 1
				},
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~udescription~~',
				dataIndex: 'udescription',
				editor: {
					xtype: 'textfield',
					allowBlank: true
				},
				hideable: false,
				sortable: true,
				flex: 3,
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~sysUpdatedOn~~',
				dataIndex: 'sysUpdatedOn',
				hideable: false,
				sortable: true,
				flex: 1,
				renderer: function(value) {
					if (value) {
						return Ext.Date.format(new Date(value), this.userDateFormat);
					}
				}
			}, {
				header: '~~sysUpdatedBy~~',
				dataIndex: 'sysUpdatedBy',
				hideable: false,
				sortable: true,
				flex: 1,
				renderer: RS.common.grid.plainRenderer()
			}, {
				xtype: 'actioncolumn',			
				hideable: false,
				sortable: false,
				align: 'center',				
				width: 45,
				items: [{
					glyph: 0xF146,
					tooltip : 'Remove',
					handler: function(view, rowIndex, colIndex, item, e, record) {
						this.up('grid').fireEvent('recordremoved', view, record);
					}
				}]
			}]
		}, {
			xtype: 'fieldset',
			title : '~~addMock~~',
			margin: '10 0 5 0',
			padding: 15,
			items: [{
				xtype: 'form',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaults: {
					labelWidth: 130,
					margin: '0 0 10 0'
				},
				items: [{
					xtype: 'textfield',
					name: 'uname',
					allowBlank: false,
					maxWidth: 500
				}, {
					xtype: 'textarea',
					name: 'udescription',
					margin: '0 0 7 0'
				}, {
					xtype: 'button',
					name : 'addMock',						
					cls: 'rs-small-btn rs-btn-dark',							
					margin: '0 0 0 135',
					maxWidth: 100,				
				}]
			}]
		}]
	}, {
		name: 'MockDetails',
		xtype: 'container',
		margin: 0,
		padding: 0,

		layout: {
			type : 'vbox',
			align: 'stretch'
		},

		defaults: {
			labelWidth: 130,
			margin: '0 0 10 0',
			padding: 0			
		},

		items: [{
			xtype: 'displayfield',
			name: 'mockName',
			margin: '0 0 2 0',
			fieldLabel: '~~name~~'
		}, {
			xtype: 'displayfield',
			fieldLabel: '~~mockDescription~~',
			value: '@{mockDescription}',
			hidden: '@{!hasMockDescription}',
			margin: 0,
			renderer: RS.common.grid.plainRenderer()
		}, {
			xtype: 'displayfield',
			fieldLabel: '~~mockDescription~~',
			value: '~~noMockDescription~~',
			hidden: '@{hasMockDescription}',
			margin: 0
		}, {
			xtype: 'displayfield',
			margin: '0 0 2 0',
			fieldLabel: '~~parameters~~',
			value: '@{parametersMesg}',
		}, {
			xtype: 'grid',
			cls: 'rs-grid-dark',
			hidden: '@{parametersGridIsEmpty}',
			store: '@{mockDetailsStore}',
			maxHeight: 300,
			flex: 1,
			margin: '-20 0 5 135',
			listeners: {
				validateedit: function(editor, context){
					return this._vm.validateNameOnEdit(context, 'name', 'mockDetailsStore');
				},		
				beforeedit: function (editor, context) {
					this.fireEvent('populatedatasources', this, context);
				},
				afterrender: function(grid) {
					grid.fireEvent('rendercompleted', grid, grid);
				},
				populatedatasources: '@{populateMockDetailsDataSources}',
				rendercompleted: '@{mockDetailsRenderCompleted}',
				recordparamremoved: '@{recordParamRemoved}'
			},

			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}],

			selModel: {
				selType: 'cellmodel'
			},

			removeMockTooltip: '@{removeMockTooltip}',
			columns: [{
				header: '~~type~~',
				dataIndex: 'type',
				itemId: 'mockDetailsEditTypeCombo',
				hideable: false,
				sortable: true,
				flex: 1,
				editor: {
					xtype: 'combo',
					allowBlank : false,
					editable: false,
					emptyText: '~~selectType~~',
					queryMode: 'local',
					displayField: 'name',
					valueField: 'name',
					store: '@{typeStore}',
					listeners: {
						focus: function () {
							this.fireEvent('mockdetailsedittypefocus');
						},
						select: function (combo, records) {
							this.fireEvent('mockdetailsedittypeselected', this, combo, records);
						},
						mockdetailsedittypefocus: '@{mockDetailsEditTypeFocus}',
						mockdetailsedittypeselected: '@{mockDetailsEditTypesSelected}'
					}
				}
			}, {
				header: '~~name~~',
				dataIndex: 'name',
				itemId: 'mockDetailsEditNameCombo',
				editor: {
					xtype: 'combo',
					allowBlank : false,
					editable: '@{isMockDetailsEditNameEditable}',
					emptyText: '~~enterMockContentName~~',
					queryMode: 'local',
					displayField: 'name',
					valueField: 'name',
					store: '@{remainingMockInputParametersStore}',
					listeners: {
						select: function (combo, records) {
							this.fireEvent('mockdetailseditnameselected', this, combo, records);
						},
						mockdetailseditnameselected: '@{mockDetailsEditNameSelected}'
					}
				},
				hideable: false,
				sortable: true,
				flex: 2,
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~uvalue~~',
				dataIndex: 'value',
				editor: 'textfield',
				hideable: false,
				sortable: true,
				flex: 2,
				renderer: RS.common.grid.plainRenderer()
			}, {
				xtype: 'actioncolumn',			
				hideable: false,
				sortable: false,
				align: 'center',				
				width: 45,
				items: [{
					glyph: 0xF146,
					tooltip : 'Remove',
					handler: function(view, rowIndex, colIndex, item, e, record ) {
						this.up('grid').fireEvent('recordparamremoved', view, record);
					}
				}]
			}]
		}, {
			xtype: 'fieldset',
			title : '~~addParameter~~',
			margin: '0 0 5 135',
			padding: 15,
			items: [{
				xtype: 'form',
				
				layout: {
					type: 'vbox',
					align: 'stretch'
				},

				defaults: {
					labelWidth: 80,
					margin: '0 0 10 0',				
				},

				items: [{
					xtype: 'combo',
					name: 'type',
					maxWidth: 500,
					emptyText: '~~selectType~~',
					store: '@{typeStore}',
					queryMode: 'local',
					displayField: 'name',
					valueField: 'name',
					cls: 'no-input-border combo',
					editable: false,
					listeners: {
						focus: function() {
							this.fireEvent('newmockdetailtypefocus');
						},
						select: function(combo, records) {
							this.fireEvent('mocktypeselected', this, combo, records);
						},
						newmockdetailtypefocus: '@{newMockDetailTypeFocus}',
						mocktypeselected: '@{mockDetailTypeSelected}'
					}
				}, {
					xtype: 'combo',
					itemId: 'mockDetailsNameCombo',
					name: 'name',
					allowBlank: false,
					maxWidth: 500,
					cls: 'no-input-border combo',
					editable: '@{isMockDetailsNameEditable}',
					emptyText: '@{mockContentNameEmptyTxt}',
					queryMode: 'local',
					displayField: 'name',
					valueField: 'name',
					store: '@{remainingMockInputParametersStore}',						
					listeners: {
						focus: function() {
							this.fireEvent('newmockdetailnamefocus');
						},
						newmockdetailnamefocus: '@{newMockDetailNameFocus}'
					}
				}, {
					xtype: 'textfield',
					name: 'value',
					allowBlank: false,
					maxWidth: 500
				}, {
					xtype: 'button',
					name: 'addParams',
					cls: 'rs-small-btn rs-btn-dark',				
					margin: '0 0 0 85',
					maxWidth: 120
				}]
			}]
		}, {
			xtype: 'container',
			margin: '0 0 5 0',			
			hidden: '@{rawIsHidden}',

			layout: {
				type: 'hbox',
				align: 'stretch'
			},

			items: [{
				xtype: 'displayfield',
				hidden: '@{rawIsHidden}',
				labelWidth: 130,
				fieldLabel: '~~raw~~',
				width: 135,
				margin: '10 0 0 0'
			}, {
				xtype: 'container',
				hidden: '@{rawIsHidden}',
				margin: '10 0 0 0',
				padding: 0,
				flex: 1,
				
				layout: {
					type: 'vbox',
					align: 'stretch'
				},

				resizable: {
					handles: 's',
					pinned: true
				},

				items: [{
					xtype: 'AceEditor',
					hidden: '@{rawIsHidden}',
					margin: 0,
					padding: 0,
					name: 'mockRaw',
					parser: 'text',
					height: 300,			
					flex: 1					
				}]
			}]
		}]
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});	

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	}	
});
