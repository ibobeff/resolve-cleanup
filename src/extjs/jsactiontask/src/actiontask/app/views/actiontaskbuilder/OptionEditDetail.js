glu.defView('RS.actiontaskbuilder.OptionEditDetail', {
	xtype: 'fieldset',
	title: '~~addOption~~',
	margin: '10 0 0 0',
	padding: '5 15 10 15',
	items: [{
		xtype: 'form',
		margin: 0,
		padding: 0,

		layout: {
			type: 'vbox',
			align: 'stretch'
		},

		defaults: {
			labelWidth: 130,
			margin: '0 0 10 0'		
		},

		items: [{
			xtype: 'combobox',
			name: 'uname',
			emptyText: '~~selectOption~~',
			store: '@{optionsStore}',
			value: '@{uname}',
			editable: false,
			allowBlank: false,
			queryMode: 'local',
			displayField: 'uname',
			valueField: 'uname',			
			cls: 'no-input-border combo',
			maxWidth: 500,
			listeners: {
				focus: '@{focusOption}',
				select: '@{selectOption}'
			}
		}, {
			xtype: 'textfield',
			name: 'uvalue',
			maxWidth: 500,
		}, {
			xtype: 'textarea',
			name: 'udescription',			
		}, {
			xtype: 'button',
			text: '~~addOption~~',
			cls: 'rs-small-btn rs-btn-dark',		
			margin: '0 0 0 135',
			maxWidth: 90,
			name: 'add',
			formBind: true
		}]
	}]
});