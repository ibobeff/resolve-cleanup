RS.actiontaskbuilder.parserXMLConfig = {	
	region : 'center',	
	xtype : 'container',		
	html: '<iframe spellcheck="false" autocomplete="off" frameborder="0" style="width:100%;height:100%;border:1px solid #999;line-height: 0;" src=""></iframe>',
	sample: '@{sample}',
	parser: {
		iframe: null,
		win: null,
		parserHintElement : null,
		initParser: function(iframe){		
			var me = this;			
			me.iframe = iframe;

			me.win = iframe.contentWindow;	
			me.parserHintElement = '<div class=parserHintText contenteditable="false">PASTE YOUR OUTPUT SAMPLE HERE.</div>';	
			var document = this.cdoc();		
			var docBody = document.body;
			docBody.innerHTML = '<pre class="parser-content" contenteditable="true">' + me.parserHintElement + '</pre>';				
			if (docBody && ('spellcheck' in docBody))
				docBody.spellcheck = false;

			//Disable typing on this iframe ( might add feature to allow user to type in future)
			Ext.fly(document).on('keydown', function(e) {
				if (!e.ctrlKey)
					e.preventDefault();
			});

			//Events on parser's sample editor.
			docBody.onpaste = function(e) {
				if(!me.eventEmitter.isViewOnly){
					var rawData = e && e.clipboardData ? e.clipboardData.getData('text/plain'): window.clipboardData.getData('Text');
					//Prettify XML
					rawData = vkbeautify.xml(rawData);
					var lines = rawData.split(/\r*\n/);
					var data = lines.join(Ext.is.Windows ? '\r\n': '\n');
					var asPasted = true;			
					me.eventEmitter.fireEvent('sampleChanged', me, data, asPasted);	
				}
				return false;
			}			
			me.eventEmitter.fireEvent('parserIsLoaded',me,true);		
		},
		cdoc: function () {
			return this.win.document;
		},	
		updateParserView: function(currentSampleDom, forClear){
			var html = forClear ? this.parserHintElement : this.renderNode(currentSampleDom,0);
			this.cdoc().body.innerHTML = '<pre class="parser-content" contenteditable="true">' + html + '</pre>';
			this.liveXml();
		},
		renderNode: function(node, indent) {			
			var id = Ext.data.IdGenerator.get('uuid').generate();
			node.pnid = id;
			if (Ext.isFunction(node.setAttribute))
				node.setAttribute('pnid', id);
			var str = '';
			switch (node.nodeType) {
				case 1:
					if (node.ignore) {
						str = Ext.String.format(
							'<span pnid="{0}" class="xmlSp rootText">{1}</span>',
							id,
							Ext.htmlEncode(Ext.String.format('<{0}>{1}</{0}>', node.tagName, node.innerHTML)));
						break;
					}
					var text = [];
					if (node.tagName != 'resolveparserroot' && node.tagName != 'error')
						text.push(this.preTag(node.tagName, node.attributes, id));
					if (node.tagName != 'error')
						Ext.each(node.childNodes, function(child) {
							text.push(this.renderNode(child, indent + 1));
						}, this)
					else
						text.push(Ext.htmlEncode(node.textContent))
					if (node.tagName != 'resolveparserroot' && node.tagName != 'error')
						text.push(this.postTag(node.tagName, id));
					var textStyle = '';
					if (node.tagName == 'error')
						textStyle = 'style="color:red"';					
					str = Ext.String.format(
						'<span pnid="{0}" class="xmlSp element"{2}>{1}</span>',
						id,
						text.join(''),
						textStyle);
					break;
				case 3:
					str = Ext.htmlEncode(node.textContent)
						.replace(/ /g, '<span ignore class="xmlSp space">&nbsp;</span>')
						.replace(/\n/g, '<span ignore class="xmlSp newline"><br></span>');
					str = Ext.String.format(
						'<span pnid="{0}" class="xmlSp {2}">{1}</span>',
						id,
						str, (node.parentNode.tagName != 'resolveparserroot' && node.parentNode.tagName != 'error' ? 'text': 'rootText'));
					break;
				case 4:
					//DEPRECATED
					str = Ext.htmlEncode(node.textContent)
						.replace(/ /g, '<span class="xmlSp space"></span>')
						.replace(/\n/g, '<span class="xmlSp newline"></span><br>');
					str = Ext.String.format(
						'<span pnid="{0}" class="xmlSp cdata"{2}>{1}</span>',
						id,
						str);
					break;
				case 9:
					var text = [];
					Ext.each(node.childNodes, function(child) {
						text.push(this.renderNode(child, 0));
					}, this)
					str = text.join('');
					break;
			}
			return str;
		},
		preTag: function(tagName, attrs, pnid) {
			var kv = [];
			for (var i = 0; i < attrs.length; i++) {
				if (attrs[i].name == 'pnid')
					continue;
				kv.push(Ext.String.format(
					'<span class="xmlSp attribute"{2}><span class="xmlSp attrName">{0}</span>=<span class="xmlSp attrValue">"{1}"</span></span>',
					attrs[i].name,
					Ext.String.escape(attrs[i].value).replace(/"/g, '\\"')
				));
			}

			return Ext.String.format(
				'<span class="xmlSp tag preTag">{0}</span><span pnid="{4}" class="xmlSp tagName">{1}</span>{3}<span class="xmlSp tag preTag">{2}</span>',
				Ext.htmlEncode('<'),
				tagName,
				Ext.htmlEncode('>'),
				kv.length > 0 ? ' ' + kv.join(' '): '',
				pnid
			);
		},
		postTag: function(tagName, id) {
			return Ext.String.format(
				'<span class="xmlSp tag postTag">{0}</span><span pnid="{3}" class="xmlSp tagName">{1}</span><span class="xmlSp tag postTag">{2}</span>',
				Ext.htmlEncode('</'),
				tagName,
				Ext.htmlEncode('>'),
				id
			);
		},
		isNodeSelectedByXPath: function(node, xpathSelections) {
			return xpathSelections.indexOf(node) != -1
		},
		getSelectedStyle: function(color) {
			return ' highlighted style="background-color:' + color + '"';
		},		
		toggleElement: function(node, on) {
			Ext.fly(node.parentNode)[on ? 'addCls': 'removeCls']('emphasize-xml');
		},
		toggleNonElement: function(node,on){
			Ext.fly(node)[on ? 'addCls': 'removeCls']('emphasize-xml');
		},	
		liveXml: function() {
			var me = this;
			var nodes = Ext.fly(this.cdoc().body).query('.xmlSp');
			Ext.each(nodes, function(node) {
				if(node.hasAttribute('ignore')){
					Ext.fly(node).on('mouseenter', function(event) {
						event.preventDefault();
						event.stopPropagation();
					})
					Ext.fly(node).on('click', function(event) {
						event.preventDefault();
						event.stopPropagation();
					})
					Ext.fly(node).on('leave', function(event) {
						event.preventDefault();
						event.stopPropagation();
					})
				}
				else
				{
					Ext.fly(node).on('mouseenter', function() {
						if(!me.eventEmitter.isViewOnly){
							if (Ext.fly(node).hasCls('tagName'))
								this.toggleElement(node, true);						
							else if (Ext.fly(node).hasCls('text') || Ext.fly(node).hasCls('attribute') || Ext.fly(node).hasCls('cdata'))
								this.toggleNonElement(node, true);
						}
					}, this);
					Ext.fly(node).on('click', function() {
						if(!me.eventEmitter.isViewOnly){
							if (Ext.fly(node).hasCls('tagName'))
								this.eventEmitter.fireEvent('nodeSelectionChanged', node, 'element');
							else if (Ext.fly(node).hasCls('text'))
								this.eventEmitter.fireEvent('nodeSelectionChanged', node, 'text');
							else if (Ext.fly(node).hasCls('attribute'))
								this.eventEmitter.fireEvent('nodeSelectionChanged', node, 'attribute');
							else if (Ext.fly(node).hasCls('cdata'))
								this.eventEmitter.fireEvent('nodeSelectionChanged', node, 'cdata');
						}
					}, this);
					Ext.fly(node).on('mouseleave', function() {
						if(!me.eventEmitter.isViewOnly){
							if (Ext.fly(node).hasCls('tagName'))
								this.toggleElement(node, false);						
							else if (Ext.fly(node).hasCls('text') || Ext.fly(node).hasCls('attribute') || Ext.fly(node).hasCls('cdata'))
								this.toggleNonElement(node, false);
						}
					}, this);
				}			
			}, this);
		}
	},
	listeners: {
		afterrender: function() {					
			var iframe = this.el.query('iframe')[0];
			iframe.src = '/resolve/actiontask/editorstub.jsp?type=xml&' + clientVM.CSRFTOKEN_NAME + '=' + clientVM.getPageToken('actiontask/editorstub.jsp');
			var me = this;			
			me.parser.eventEmitter = this;
			//Initialize Parser Once the frame is ready.	
			if (iframe.attachEvent)
				iframe.attachEvent('onload', function() {
					me.parser.initParser(iframe);
				});
			else if (iframe.addEventListener){
				iframe.addEventListener('load', function() {
					me.parser.initParser(iframe);
				}, false);
			}
			this._vm.on('renderSample', function(forClear){
				me.parser.updateParserView(this.currentSampleDom, forClear);
			});
			this._vm.on('hightLightSelections', function(xpathSelections, currentColor, scroll) {
				me.parser.highLightSelections(xpathSelections, currentColor, scroll);
			});
		},
		sampleChanged: '@{updateSample}',
		selectXPath: '@{selectXPath}',	
		parserIsLoaded: function(){
			this._vm.set('parserReady', true);
		},
		nodeSelectionChanged: function(node, type) {
			var current = node;
			while (!Ext.fly(current).hasCls('element'))
				current = current.parentNode;
			this.fireEvent('selectXPath', this, type, current.getAttribute('pnid'), node);
		},
		beforedestroy : function(){
			//Mark this iframe dom to be collected by GC. 
			//By default events that are registered on #document is marked as not collected by GC (Not sure why Extjs is doing that)
			if(this.rendered){
				var document = this.parser.cdoc();
				var iframeDom = Ext.fly(document);
				if(iframeDom && iframeDom.$cache)
					iframeDom.$cache.skipGarbageCollection = false;
			}
		}
	}
};

glu.defView('RS.actiontaskbuilder.XMLParser',{
	itemId : 'xml',
	layout : 'card',
	activeItem : '@{activeScreen}',
	dockedItems: [{
		xtype: 'toolbar',
		cls : 'rs-dockedtoolbar',
		defaults: {
			cls: 'rs-small-btn rs-btn-dark'			
		},
		items:['clearSample','|','showCode','showMarkup']
	}],	
	cls: 'xml-parser-wrapper',	
	items : [{
		//Markup Screen
		xtype : 'container',		
		layout : {
			type : 'vbox',
			align : 'stretch'
		},	
		items:[{
			xtype: 'container',	
			layout : 'border',	
			height : '@{controlHeight}',					
			items: [RS.actiontaskbuilder.parserXMLConfig,{
				//Control
				width : 600,	
				region : '@{controlBorderRegion}',
				margin : '@{controlMargin}',
				xtype : 'container',
				layout : {
					type : 'vbox',
					align : 'stretch'
				},
				items : [{
					xtype : 'fieldset',	
					padding : '5 10 10',							
					height : 180,
					title : '~~xpathControl~~',
					layout : {
						type : 'vbox',
						align : 'stretch'
					},				
					items: [{
						xtype: 'toolbar',
						cls : 'rs-dockedtoolbar',
						defaults: {
							cls: 'rs-small-btn rs-btn-dark',				
						},			
						items: ['clearPath','capturePath']		
					},{
						xtype : 'panel',
						augmentedXPath: '@{augmentedXPath}',
						flex : 1,
						autoScroll : true,						
						style: 'border:1px solid #d9d9d9; padding:5px; background: white; width: 100%',		
						setAugmentedXPath: function(augmentedXPath) {
							this.augmentedXPath = augmentedXPath;
							var tokens = [];
							Ext.each(augmentedXPath, function(augmentedToken) {
								tokens.push(this.cookToken(augmentedToken))
							}, this);
							var beautified = '<div class="xpath">&#47;' + tokens.join('') + '</div>';
							if (!this.getContentTarget())
								return;
							this.getContentTarget().dom.innerHTML = beautified;
							this.liveXPath();
						},
						cookPredicates: function(augmentedToken) {
							var predicates = augmentedToken.predicates;
							var pnid = augmentedToken.pnid;
							var tokens = [];
							Ext.each(predicates, function(p) {
								if (p.type == 'text')
									tokens.push(this.cookTextPredicate(p));
								else if (p.type == 'attribute')
									tokens.push(this.cookAttributePredicate(p));
								else if (p.type == 'custom')
									tokens.push(this.cookCustomPredicate(p));
							}, this);
							return Ext.String.format('<span pnid="{1}" ptid="predicates" class="xpath-predicate">&#91;{0}&#93;</span>', tokens.join('&nbsp;' + augmentedToken.op + '&nbsp;'), pnid);
						},
						cookTextPredicate: function(predicate) {
							return Ext.String.format(
								'<span pnid="{2}" ptid="{3}" class="xpath-predicate-text"><span class="xpath-predicate-condition">{0}</span>(., \'{1}\')</span>',
								predicate.condition,
								Ext.htmlEncode(predicate.value),
								predicate.pnid,
								predicate.ptid
							);
						},
						cookAttributePredicate: function(predicate) {
							if (['contains', 'starts-with', 'ends-with'].indexOf(predicate.condition) != -1){
								return Ext.String.format(
									'<span pnid="{3}" ptid="{4}" class="xpath-predicate-attribute">{1}(@{0},{2})</span>',
									predicate.name,
									predicate.condition, (predicate.isNumber ? predicate.value: Ext.htmlEncode('"' + predicate.value + '"')),
									predicate.pnid,
									predicate.ptid
								);
							}
							else 
							return Ext.String.format(
								'<span pnid="{3}" ptid="{4}" class="xpath-predicate-attribute">@{0} {1} {2}</span>',
								predicate.name,
								predicate.condition, (predicate.isNumber ? predicate.value: Ext.htmlEncode('"' + predicate.value + '"')),
								predicate.pnid,
								predicate.ptid
							);
						},
						cookCustomPredicate: function(predicate) {
							return Ext.String.format('<span pnid="{1}" ptid="{2}" class="xpath-predicate-customized">{0}</span>', predicate.value, predicate.pnid, predicate.ptid);
						},
						cookToken: function(augmentedToken) {
							if (augmentedToken.type == 'selector')
								return this.cookSelector(augmentedToken);
							var predicate = augmentedToken.predicates.length == 0 ? '': this.cookPredicates(augmentedToken);
							return Ext.String.format(
								'&#47;<span pnid={2} ptid={3} class="xpath-element">{0}</span>{1}',
								augmentedToken.element,
								predicate,
								augmentedToken.pnid,
								augmentedToken.ptid
							);
						},
						cookSelector: function(augmentedToken) {
							return Ext.String.format('<span ptid={0} ignore class="xpath-selector-text">/{1}</span>', augmentedToken.ptid, augmentedToken.value);
						},
						previousSelectedPathId : null,
						liveXPath: function() {				
							var list = this.getEl().query('*[class*="xpath"]');
							var me = this;
							Ext.each(list, function(li) {
								var path = 	Ext.get(li);
								if(this.previousSelectedPathId == path.dom.getAttribute('pnid'))
									path.dom.setAttribute('selected','');				
								path.on('click', this.showDetail, this);
							}, this);
						},
				
						showDetail: function(evt, dom) {
							evt.stopEvent(); //so that the predicate container span won't receive the click event

							//Already selected or selection is not on node, just return. This will depricate checking for leaf node on VM.
							if(dom.hasAttribute('selected') || dom.className.indexOf('xpath-element') == -1)
								return;
							else{
								var list = this.getEl().query('*[class*="xpath"]');													
								Ext.each(list, function(li) {							
									li.removeAttribute('selected');
								}, this);
								dom.setAttribute('selected','');
								//Save this so we can selected later if this path is updated
								this.previousSelectedPathId = dom.getAttribute('pnid');
							}

							var parentTokenNode = dom;
							var ptid = '';
							while (parentTokenNode.parentNode && !(ptid = parentTokenNode.getAttribute('ptid')))
								parentTokenNode = parentTokenNode.parentNode
							if (!ptid)
								return;							
						
							var pnid = parentTokenNode.getAttribute('pnid');
							var height = Ext.fly(dom).getHeight();
							var x = Ext.fly(dom).getX();
							var y = Ext.fly(dom).getY();
							var parser = this.up().up().down('*[parserType="xml"]');
							this.fireEvent('showDetail', this, pnid, ptid, function(wnd) {
								var bodyHeight = Ext.getBody().getHeight();
								var bodyWidth = Ext.getBody().getWidth();
								if (x + wnd.getWidth() > bodyWidth)
									x = bodyWidth - wnd.getWidth();
								if (y + wnd.getHeight() > bodyHeight)
									y -= wnd.getHeight();
								else
									y += height;
								return [x, y];
							});
						},
						removeToken: function(evt, dom) {
							evt.stopEvent();
							var parentTokenNode = dom;
							var ptid = '';
							while (parentTokenNode.parentNode && !(ptid = parentTokenNode.getAttribute('ptid')))
								parentTokenNode = parentTokenNode.parentNode
							if (!ptid)
								return;
							var pnid = parentTokenNode.getAttribute('pnid');
							this.fireEvent('removeToken', this, pnid, ptid);
						},
						listeners: {
							showDetail: '@{showDetail}',
							removeToken: '@{removeToken}'
						}
					}]						
				},{
					flex : 1,
					margin : 0,				
					xtype : '@{xPathDetail}'				
				}],
				listeners : {
					afterrender : function(panel){
						panel._vm.on('embeddedLayout', function(isEmbedded){								
							if(isEmbedded){
								panel.setBorderRegion('south');							
							}
							else {
								panel.setBorderRegion('east');							
							}
						});
					}
				}
			}]	
		},{
			xtype: 'component',		
			html: '~~variableTable~~',
			margin : '15 0 8 0',	
		},{
			xtype: 'component',		
			hidden: '@{!variableStoreIsEmpty}',
			html: '~~noVariableText~~'
		},{
			xtype: 'grid',		
			cls: 'rs-grid-dark',						
			store: '@{variableStore}',
			columns: '@{variableColumn}',
			hidden: '@{variableStoreIsEmpty}',
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}],
			selModel: {
				selType: 'cellmodel'
			},
			listeners: {					
				removeVariable: '@{removeVariable}',
				validateedit: function(editor, context){
					if(context.field == "variable"){						
						var store = context.store;
						var newVal = context.value;
						var currentRowIdx = context.rowIdx;
						if(store.findExact(context.field, newVal) != currentRowIdx && store.findExact(context.field, newVal) != -1){
							this._vm.displayDuplicateNameMsg();
							return false;
						}
						else if(!RS.common.validation.VariableName.test(newVal)){
							this._vm.displayInvalidCharacter();
							return false;
						}
					}
				}		
			}
		}]
	},{
		xtype : '@{codeScreen}'
	}]
})