glu.defView('RS.actiontask.ActionTask',{
	bodyPadding : '@{bodyPadding}',
	dockedItems: [{		
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		padding : '10 15 4 15',
		hidden: '@{isembedded}',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{actionTaskTitle}',
			htmlEncode : false
		},
		/* TODO - support Version Control
		{
			xtype: 'tbtext',
			text: '@{versionTxt}'
		},
		*/ 
		{
			xtype: 'tbtext',
			text: '~~readOnlyTxt~~',
			hidden: '@{!readOnlyMode}'
		},
		'->', {
			text: '',
			cls: 'rs-execute-button',
			iconCls: 'rs-social-button icon-play',
			tooltip: '~~executionTooltip~~',
			disabled: '@{!isExecutable}',
			handler: function(button) {
				button.fireEvent('execution', button, button)
			},
			listeners: {
				execution: '@{execution}'
			}
		}, {
			name: 'save',
			iconCls: 'rs-social-button icon-save',
			text: '',
			tooltip: '~~save~~',
			disabled: '@{!saveIsEnabled}',
			hidden: '@{readOnlyMode}'
		}, {
			name: 'edit',
			iconCls: 'rs-social-button icon-pencil',
			text: '',
			tooltip: '~~edit~~',
			disabled: '@{!editIsEnabled}',
			hidden: '@{!readOnlyMode}'
		}, {
			name: 'exitToView',
			iconCls: 'rs-social-button icon-reply-all',
			style: {
				width: '25px'
			},
			text: '',
			tooltip: '~~exitToView~~',
			hidden: '@{readOnlyMode}'
		}, 
		/*
		{
			name: 'expandAll',
			text: '',
			iconCls: 'rs-social-button enlarge120 icon-expand-alt',
			tooltip: '~~expandAll~~',
			hidden: '@{expandAllDisabled}'
		}, {
			name: 'collapseAll',
			text: '',
			iconCls: 'rs-social-button enlarge120 icon-collapse-alt',
			tooltip: '~~collapseAll~~',
			hidden: '@{collapseAllDisabled}'
		}, 
		*/
		{
			text: '',
			iconCls: 'rs-social-button icon-repeat',
			tooltip: '~~reload~~',
			disabled: '@{!saveAsRenameIsEnabled}',
			handler: function() {
				this.fireEvent('reload', this);
			},
			listeners: {
				reload: '@{reload}',
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}, 
	{
		xtype: 'toolbar',
		padding : '4 15',
		dock: 'top',
		cls: 'actionBar actionBar-form',		
		hidden: '@{hideNewActionTaskToolBar}',
		defaults : {
			cls: 'rs-toggle-btn rs-small-btn' 
		},
		items: [{	
			iconCls: 'rs-icon-button icon-file-text',
			name: 'fileOptions',
			cls : 'rs-small-btn rs-btn-light',			
			menu: [{
				name: 'save',
				disabled: '@{!saveIsEnabled}',
				hidden: '@{readOnlyMode}',
			}, {
				xtype: 'menuseparator',
				width: 200
			}, {
				name: 'accessActiontTaskList',
				text: '~~cancel~~'
			}]
		}]
	}, {
		xtype: 'toolbar',
		padding : '4 15',
		dock: 'top',
		cls: 'actionBar actionBar-form',		
		hidden: '@{hideActionTaskToolBar}',
		defaults : {
			cls: 'rs-toggle-btn rs-small-btn' 
		},
		items: [{	
			iconCls: 'rs-icon-button icon-file-text',
			name: 'fileOptions',
			cls : 'rs-small-btn rs-btn-light',			
			menu: [{
				name: 'save',
				disabled: '@{!saveIsEnabled}',
				hidden: '@{readOnlyMode}',
			}, {
				name: 'saveAs',
				disabled: '@{!saveAsRenameIsEnabled}',
				hidden: '@{readOnlyMode}',
			}, 
			/* TODO - support Version Control
			{
				xtype: 'menuseparator',
				hidden: '@{readOnlyMode}',
			}, {
				name: 'commit',
				hidden: '@{readOnlyMode}',
			}, {
				name: 'viewVersionList',
				hidden: '@{readOnlyMode}',
			}, 
			*/
			{
				name: 'edit',
				disabled: '@{!editIsEnabled}',
				hidden: '@{!readOnlyMode}'
			}, {
				name: 'rename',
				disabled: '@{!saveAsRenameIsEnabled}',
				hidden: '@{!readOnlyMode}',
			}, {
				xtype: 'menuseparator',
				width: 200
			}, {
				name: 'expandAll',
				hidden: '@{expandAllDisabled}'
			}, {
				name: 'collapseAll',
				hidden: '@{collapseAllDisabled}'
			}, {
				xtype: 'menuseparator',
				hidden: '@{expandCollapseSeparatorHidden}'
			},
			/* 
			{
				name: 'reference', 
				disabled: '@{isNew}'
			}, 
			*/
			{
				name: 'sysInfo',
				disabled: '@{isNew}'
			}, {  
				text: '~~toolbarSettings~~',
				disabled: '@{!toolbarSettingsIsEnabled}',
				menu: [{
					name: 'showSectionNone',
					iconCls: '@{iconShowSectionNone}',
				}, {
					name: 'showSectionIconOnly',
					iconCls: '@{iconShowSectionIconOnly}',
				}, {
					name: 'showSectionTextOnly',
					iconCls: '@{iconShowSectionTextOnly}',
				}, {
					name: 'showSectionTextIcon',
					iconCls: '@{iconShowSectionIconAndText}',
				}] 
			}, {
				xtype: 'menuseparator'
			}, {
				name: 'exitToView',
				text: '~~exitToView~~',
				hidden: '@{readOnlyMode}',
				disabled: '@{!exitToViewIsEnabled}',
			}, {
				//iconCls: 'rs-social-menu-button icon-reply-all',
				name: 'accessActiontTaskList',
				text: '~~viewActionTaskList~~'
			}]
		},
		/*
		{
			xtype: 'tbseparator',
			hidden: '@{expandCollapseSeparatorHidden}'
		}, {
			name: 'expandAll',
			iconCls: 'rs-social-button icon-expand-alt',
			hidden: '@{expandAllDisabled}'
		}, {
			name: 'collapseAll',
			iconCls: 'rs-social-button icon-collapse-alt',
			hidden: '@{collapseAllDisabled}'
		},
		*/
		{
			xtype: 'tbseparator',
		}, {
			name: 'allBtn',
			text: '@{allBtnText}',
			iconCls: '@{allBtnIcon}',
			tooltip: '@{allBtnTooltip}',
		}, {
			name: 'generalBtn',
			text: '@{generalBtnText}',
			iconCls: '@{generalBtnIcon}',
			tooltip: '@{generalBtnTooltip}',
		},
		{
			name: 'referenceBtn',
			text: '@{referenceBtnText}',
			iconCls: '@{referenceBtnIcon}',
			tooltip: '@{referenceBtnTooltip}',
		}, 
		{
			name: 'parametersBtn',
			text: '@{parametersBtnText}',
			iconCls: '@{parametersBtnIcon}',
			tooltip: '@{parametersBtnTooltip}',
		}, {
			name: 'preprocessorBtn',
			text: '@{preprocessorBtnText}',
			iconCls: '@{preprocessorBtnIcon}',
			tooltip: '@{preprocessorBtnTooltip}',
		}, {
			name: 'contentBtn',
			text: '@{contentBtnText}',
			iconCls: '@{contentBtnIcon}',
			tooltip: '@{contentBtnTooltip}',
			hidden: '@{contentBtnDisabled}',
		}, {
			name: 'parserBtn',
			text: '@{parserBtnText}',
			iconCls: '@{parserBtnIcon}',
			tooltip: '@{parserBtnTooltip}',
		}, {
			name: 'assessBtn',
			text: '@{assessBtnText}',
			iconCls: '@{assessBtnIcon}',
			tooltip: '@{assessBtnTooltip}',
		}, {
			name: 'outputsBtn',
			text: '@{outputsBtnText}',
			iconCls: '@{outputsBtnIcon}',
			tooltip: '@{outputsBtnTooltip}',
		}, {
			name: 'severityConditionsBtn',
			text: '@{severityConditionsBtnText}',
			iconCls: '@{severityConditionsBtnIcon}',
			tooltip: '@{severityConditionsBtnTooltip}',
		}, {
			name: 'summaryDetailsBtn',
			text: '@{summaryDetailsBtnText}',
			iconCls: '@{summaryDetailsBtnIcon}',
			tooltip: '@{summaryDetailsBtnTooltip}',
		}, {
			name: 'mockBtn',
			text: '@{mockBtnText}',
			iconCls: '@{mockBtnIcon}',
			tooltip: '@{mockBtnTooltip}',
		}, {
			name: 'tagsBtn',
			text: '@{tagsBtnText}',
			iconCls: '@{tagsBtnIcon}',
			tooltip: '@{tagsBtnTooltip}',
		}]
	}],		
	overflowY: 'auto',		
	layout:{
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		margin: '0 0 5 0',
	},
	items: '@{itemList}',
	viewOnly: '@?{..viewOnly}',
	listeners: {
		afterrender: function(view) {
			this.fireEvent('afterRendered', this, view);			
		},	
		afterRendered: '@{afterRendered}'
	}
});
