glu.defView('RS.actiontaskbuilder.AssignValuesToOutputsEdit', {
	parentLayout: 'quickAccess',
	cls: 'block-model',
	title: '@{title}',	
	hidden: '@{hidden}',
	header: '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '10 15 15 15',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	
	dockedItems: [{
		xtype: 'toolbar',		
		padding : '0 15 10',
		dock: 'bottom',
		items: ['->', {			
			name: 'close',
			cls: 'rs-med-btn rs-btn-dark'
		}]
	}],

	items: [{
		xtype: 'component',
		hidden: '@{!gridIsEmpty}',
		html: '~~noAssignmentsMessage~~',
		margin: '10 0',	
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		store: '@{mappingStore}',
		hidden: '@{gridIsEmpty}',
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		selModel: {
			selType: 'cellmodel'
		},
		listeners: {
			validateedit: function(editor, context){
				if(context.value == '' || !context.value)
					return false;
				else if(context.value == context.originalValue)
					return;
				else
					return this._vm.validateOnEdit(context);
			},
			beforeedit : function(editor, context){
				var fieldName = context.field;
				if(fieldName == 'outputName')
					var fieldValue = context.record.get('outputType');
				else if(fieldName == 'sourceName')
					var fieldValue = context.record.get('sourceType');
				else
					return true;

				this._vm.loadParameterStore(fieldName, fieldValue);
			},
			removeAssignment: '@{removeAssignment}',
		},
		viewConfig: {
			getRowClass: function (record) {
		        if (record.get('isDuplicateAssignment')) {
		            return 'duplicate-row';
		        } else {
		            return '';
		        }
		    }
		},
		columns: [{
			header: '~~outputType~~',
			dataIndex: 'outputType',
			resizable : false,			
			flex: 1,
			editor: {
				xtype: 'combo',				
				editable: false,			
				queryMode: 'local',
				displayField: 'name',
				valueField: 'name',
				store: '@{outputTypesStore}',
				cls: 'no-input-border combo'				
			}
		}, {
			header: '~~outputName~~',
			dataIndex: 'outputName',
			resizable : false,
			flex: 1,
			renderer: RS.common.grid.plainRenderer(),
			editor: {
				xtype: 'combo',			
				queryMode: 'local',
				displayField: 'name',
				valueField: 'name',
				store: '@{outputParametersStore}',
				cls: 'no-input-border combo'
			}
		}, {
			header: '~~sourceType~~',
			dataIndex: 'sourceType',			
			flex: 1,
			editor: {
				xtype: 'combo',			
				editable: false,			
				queryMode: 'local',
				displayField: 'name',
				valueField: 'name',
				store: '@{sourceTypesStore}',
				cls: 'no-input-border combo'			
			}
		}, {
			header: '~~sourceName~~',
			dataIndex: 'sourceName',
			resizable : false,
			flex: 1,
			renderer: RS.common.grid.plainRenderer(),
			editor: {
				xtype: 'combo',				
				queryMode: 'local',
				displayField: 'name',
				valueField: 'name',
				store: '@{inputParametersStore}',
				cls: 'no-input-border combo'					
			}
		}, {
			xtype: 'actioncolumn',		
			hideable: false,
			align: 'center',		
			width: 45,			
			resizable : false,
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record) {
					this.up('grid').fireEvent('removeAssignment', view, record);
				}
			}]
		}]
	}, {		
	  	xtype : 'infoicon',
	  	iconType : 'warn',
        displayText : '~~duplicateAssignmentMessage~~',
		hidden: '@{!hasDuplicateAssignment}',	
		margin: '8 0 0 0'
	}, {
		xtype: '@{detail}'
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});	

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	}
});
