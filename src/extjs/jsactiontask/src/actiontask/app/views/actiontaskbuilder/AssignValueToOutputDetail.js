
glu.defView('RS.actiontaskbuilder.AssignValueToOutputDetail', {
	xtype: 'fieldset',
	title: '~~assignValueToOutput~~',
	margin: '10 0 0 0',
	padding: '5 15',	
	layout: {
		type : 'vbox',
		align : 'stretch'
	},
	defaults: {
		labelWidth: 130,
		maxWidth : 500,
		margin: '4 0'
	},
	items: [{
		xtype: 'combobox',
		name: 'outputType',
		emptyText: '~~selectOutputType~~',
		editable: false,			
		queryMode: 'local',
		displayField: 'name',
		valueField: 'name',
		store: '@{outputTypesStore}'		
	},{
		xtype: 'textfield',	
		name: 'outputName',
		emptyText: '~~enterOutputName~~',
		hidden : '@{!outputNameIsText}'		
	},{
		xtype: 'combobox',	
		name: 'outputName',
		hidden : '@{outputNameIsText}',	
		emptyText: '@{outputNameEmptyText}',
		editable: '@{outputNameIsEditable}',		
		queryMode: 'local',
		displayField: 'name',
		valueField: 'name',			
		store: '@{outputParametersStore}',
	}, {
		xtype: 'combobox',
		name: 'sourceType',
		emptyText: '~~selectSourceType~~',
		editable: false,	
		queryMode: 'local',
		displayField: 'name',
		valueField: 'name',
		store: '@{sourceTypesStore}'	
	}, {
		xtype: 'textfield',
		name: 'sourceName',			
		hidden: '@{!sourceNameIsText}',	
		fieldLabel: '~~sourceValueEdit~~',
		emptyText: '~~enterSourceValue~~'
	}, {
		xtype: 'combobox',
		name: 'sourceName',
		hidden: '@{sourceNameIsText}',	
		fieldLabel: '~~sourceNameEdit~~',
		emptyText: '@{sourceNameEmptyText}',
		editable: '@{sourceNameIsEditable}',		
		queryMode: 'local',
		displayField: 'name',
		valueField: 'name',
		store: '@{inputParametersStore}'		
	}, {			
		xtype: 'button',
		name: 'addAssignment',		
		maxWidth: 140,		
		cls: 'rs-small-btn rs-btn-dark',		
		margin: '4 0 4 135'
	}]
});