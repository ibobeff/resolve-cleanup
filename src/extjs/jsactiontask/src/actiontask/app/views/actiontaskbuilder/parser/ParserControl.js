glu.defView('RS.actiontaskbuilder.ParserControl',{
	xtype : 'panel',
	layout : 'card',	
	activeItem : '@{activeParserType}',
	items : [{
		xtype : '@{textParser}',
	},{
		xtype : '@{tableParser}'
	},{
		xtype : '@{xmlParser}'
	}]
})