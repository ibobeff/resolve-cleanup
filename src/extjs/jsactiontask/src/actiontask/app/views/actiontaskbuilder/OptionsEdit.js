glu.defView('RS.actiontaskbuilder.OptionsEdit', {
	parentLayout: 'quickAccess',
	title: '@{title}',	
	cls: 'block-model subEditModel',
	hidden: '@{hidden}',
	header: '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '10 15 15 15',	
	margin: '0 0 2.5 0',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});	

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});

			panel.getEl().on('click', function() {
				if (panel._vm.collapsed && !panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	},

	dockedItems: [{
		xtype: 'toolbar',	
		dock: 'bottom',
		padding : '0 15 10',			
		items: ['->', { 
			name: 'close', 
			cls: 'rs-med-btn rs-btn-dark'
		}]
	}],

	items: [{
		xtype: 'displayfield',
		hidden: '@{!gridIsEmpty}',
		value: '~~optionsEmptyText~~',
		margin: 0,
		padding: 0
	}, {
		xtype: 'grid',
		disabled: '@{savingNewTask}',
		cls : 'rs-grid-dark',
		hidden: '@{gridIsEmpty}',
		store: '@{optionsStore}',			
		
		listeners: {
			beforeedit: '@{updateAllowedOptions}'
		},
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		viewConfig: {
			getRowClass: function(record, rowIndex, rowParams, store){
				if (record.data.uname === "BLANKVALUES") {
					return "disable-entry"
				}
				else return "";
			}
		},
		selModel: {
			selType: 'cellmodel'
		},
		columns: [{
			dataIndex: 'uname',
			header: '~~uname~~',
			flex: 1,
			resizable: false,
			hideable: false,
			editor: {
				xtype: 'combobox',
				queryMode: 'local',
				displayField: 'uname',
				valueField: 'uname',
				store: '@{allowedOptionsForGridRow}',
				editable: false,
				listeners: {
					select: function () {
						this.fireEvent('selectoption', this, arguments);
					},
					selectoption: '@{selectOption}'
				}
			}
		}, {
			dataIndex: 'uvalue',
			header: '~~uvalue~~',
			editor: 'textfield',
			flex: 2,
			resizable: false,
			hideable: false,
			renderer: RS.common.grid.plainRenderer()
		}, {
			dataIndex: 'udescription',
			header: '~~description~~',
			editor: 'textfield',
			flex: 3,
			resizable: false,
			hideable: false,
			renderer: RS.common.grid.plainRenderer()
		}, {
			xtype: 'actioncolumn',		
			width : 45,
			hideable: false,			
			resizable: false,
			hideable: false,			
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record ) {
					record.store.remove(record);
					view.ownerCt._vm.addOptionToDetail(record);
				}
			}]
		}]
	}, {
		xtype: '@{detail}',
		hidden: '@{..itemsExhausted}',
		disabled: '@{..savingNewTask}',
	}]
});
