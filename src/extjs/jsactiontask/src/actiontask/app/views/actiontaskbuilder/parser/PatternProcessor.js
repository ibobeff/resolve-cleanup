glu.defView('RS.actiontaskbuilder.PatternProcessor', {
	xtype : 'panel',
	items : [{
		xtype : 'component',
		html : '~~patternTableTitle~~',
		margin : '0 0 8 0'
	},{
		xtype : 'component',
		html : '~~patternHintText~~',
		hidden : '@{!patternIsHidden}'
	},{		
		items : '@{patternList}',
		hidden : '@{patternIsHidden}'
	}]
})