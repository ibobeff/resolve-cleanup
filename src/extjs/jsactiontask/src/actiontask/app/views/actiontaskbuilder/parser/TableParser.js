RS.actiontaskbuilder.parserTableConfig = {	
	region : 'center',	
	xtype : 'container',	
	html: '<iframe spellcheck="false" autocomplete="off" frameborder="0" style="width:100%;height:100%;border:1px solid #999;line-height: 0;" src=""></iframe>',
	parser: {
		iframe: null,
		win: null,	
		parserHintElement: null,
		mousePreviousPosition : false,
		hightLightTextFlag: false,	

		initParser: function (iframe){			
			var me = this;			
			me.iframe = iframe;

			me.win = iframe.contentWindow;
			me.parserHintElement = '<div class=parserHintText contenteditable="false">PASTE YOUR OUTPUT SAMPLE HERE.</div>';	
			var document = this.cdoc();
			var docBody = document.body;
			docBody.innerHTML = '<pre class="parser-content" contenteditable="true">' + me.parserHintElement + '</pre>';							
			if (docBody && ('spellcheck' in docBody))
				docBody.spellcheck = false;

			//Disable typing on this iframe ( might add feature to allow user to type in future)
			Ext.fly(document).on('keydown', function(e) {
				if (!e.ctrlKey)
					e.preventDefault()
			});

			//Events on parser's sample editor.
			docBody.onpaste = function (e) {				
				var rawData = e && e.clipboardData ? e.clipboardData.getData('text/plain'): window.clipboardData.getData('Text');
				var lines = rawData.split(/\r*\n/);
				var data = lines.join(Ext.is.Windows ? '\r\n': '\n');
				var asPasted = true;		
				me.eventEmitter.fireEvent('sampleChanged', me, data, asPasted);	
				return false;
			}
			
			docBody.onmousedown = function(e) {
				if(e.button == 0){
					me.hightLightTextFlag = false;
					me.mousePreviousPosition = [e.pageX,e.pageY];				
				}
				else 
					me.mousePreviousPosition = null;
			}
			
			docBody.onmousemove = function(e) {				
				if(me.mousePreviousPosition && e.pageX != me.mousePreviousPosition[0] && e.pageX != me.mousePreviousPosition[1])
					me.hightLightTextFlag = true;			
				else
					e.hightLightTextFlag = false;
			}

			docBody.onmouseup = function (e) {				
				var selection = rangy.getSelection(me.iframe);

				if (!me.isZeroLengthSelection(selection) && me.hightLightTextFlag) {	
					me.eventEmitter.fireEvent('contentSelectionChanged', me, selection, docBody, false);
				}				
			}

			//Disable drag and drop elements
			docBody.ondragstart = function (e) {
				return false;
			}
			docBody.ondrop = function () {
				return false;
			}
			me.eventEmitter.fireEvent('parserIsLoaded',me,true);	
		},
		cdoc: function(){
			return this.win.document;
		},
		placeHolders: {
			space: '<span marker="space" class="format">&nbsp;</span>',
			new_line:  '<span marker="new_line" class="return"><br></span>'
		},
		convert: function(text, pre, post) {
			if(!text)
				return "";
			var processedText = text.replace(/ /g, this.placeHolders.space).replace(Ext.is.Windows ? (/\r\n/g): (/\n/g), this.placeHolders.new_line);
			return processedText;
		},
		isZeroLengthSelection: function(selection){
			var isZeroLengthSelection = (selection.anchorNode == selection.focusNode && selection.anchorOffset == selection.focusOffset);
			return isZeroLengthSelection;
		},
		//This method will render markups into HTML elements
		updateParserView: function(sample,markups){
			//Get pts for each markup
			var pts = [];
			var parseStartPosition = -1;
			var parseEndPosition = -1;
			var hasParseStart = false;
			var hasParseEnd = false;
			Ext.each(markups, function(markup){			
				if(markup.type == 'parseStart'){
					hasParseStart = true;
					parseStartPosition = markup.from + ( markup.included ? 0: markup.length );
					pts.push(parseStartPosition);
				}
				else if(markup.type == 'parseEnd'){
					hasParseEnd = true;
					parseEndPosition = markup.from + ( markup.included ? markup.length: 0 );
					pts.push(parseEndPosition);
				}
				else{
					pts.push(markup.from);
					pts.push(markup.from + markup.length);
				}
			})
			pts = Ext.Array.unique(pts);
			//Add start and end pts
			if(pts.indexOf(0) == -1)
				pts.push(0);
			if (pts.indexOf(sample.length) == -1)
				pts.push(sample.length);
			pts.sort(function(a, b) {
				return a > b ? 1: (a == b ? 0: -1);
			});
			var chopped = [];
			for (var i = 0; i < pts.length - 1; i++){
				if(parseStartPosition == pts[i]){
					chopped.push({
						type: "parseStart"					
					});
				}
				else if(parseEndPosition == pts[i]){
					chopped.push({
						type: "parseEnd"					
					});
				}
				chopped.push({
					text: sample.substring(pts[i], pts[i + 1]),
					from: pts[i],
					to: pts[i + 1],
					actionId: null
				});
			}
		
			//Sort all pieces and add type and id for each chopped piece.
			Ext.each(chopped, function(slice, idx) {
				Ext.each(markups, function(markup) {
					if(markup.type == slice.type && ( markup.type == "parseStart" || markup.type == "parseEnd")){ //Only slice with parseEnd and parseStart has a type at this point						
						slice.actionId = markup.actionId;
					}
					else if (markup.type != "parseStart" && markup.type != "parseEnd" && slice.from >= markup.from && slice.to <= (markup.from + markup.length)) {
						slice.actionId = markup.actionId;
						slice.type = markup.type;	
						slice.action = markup.action || 'default';				
						if(markup.action == 'capture' || markup.type == 'column'){
							slice.color = markup.color;
						}
						if(markup.type == 'column'){
							slice.col = markup.column;
						}
					}
				});
			});
			var done = [];
			var beginParseSection = false ;
			var endParseSection = false;
			Ext.each(chopped, function(slice, idx) {				
				var convertedContent = this.convert(Ext.htmlEncode(slice.text));
				if(slice.type == 'parseStart')
					beginParseSection = true;				
				if((hasParseStart && !beginParseSection) || (hasParseEnd && endParseSection)){
					done.push(Ext.String.format('<span unselectable boundary contenteditable="false">{0}</span>', convertedContent));
				}
				else {
					if (slice.actionId != null){
						if(slice.action == 'capture'){
							done.push(Ext.String.format('<span {1} marked unselectable contenteditable="false" action="capture" type={2} style="background:{3}" ignore>{0}</span>', convertedContent, 'actionId="' + slice.actionId + '"', slice.type, slice.color));
						}
						else if(slice.type == 'column'){
							done.push(Ext.String.format('<span {1} marked unselectable contenteditable="false" action="default" type={2} col={4} style="border-color:{3};border-width:2px;">{0}</span>', convertedContent, 'actionId="' + slice.actionId + '"', slice.type, slice.color, slice.col));
						}
						else
							done.push(Ext.String.format('<span {1} unselectable contenteditable="false" type={2}>{0}</span>', convertedContent, 'actionId="' + slice.actionId + '"', slice.type))
					}
					else
						done.push(convertedContent);
				if(slice.type == 'parseEnd')
					endParseSection = true;
				}
			},this);
			if(done.length == 0)
				done.push(this.parserHintElement);
			this.cdoc().body.innerHTML ='<pre class="parser-content" contenteditable="true">' + done.join('') +'</pre>';	

			//Add Event Handler for each markups
			this.liveMarkup();
		},
		liveMarkup: function(){
			var me = this;
			Ext.each(Ext.fly(this.cdoc().body).query('span'),function(span){
				if(span.hasAttribute('marked') && !span.hasAttribute('ignore')){					
					Ext.fly(span).on('click', function(e) {						
						var colNo = span.getAttribute('col');
						me.eventEmitter.fireEvent('columnSelectionChanged', this, colNo);						
					});									
				}
			})
		},
		updateColumnSelection: function(colNo){
			//Disable all other activated markup.
			var activatedMarkups = Ext.fly(this.cdoc().body).query('span[marked][activated]');
			for(var i = 0; i < activatedMarkups.length; i++){
				var activatedSpan = activatedMarkups[i];
				activatedSpan.removeAttribute('activated');
			}			
			var columnCells = Ext.fly(this.cdoc().body).query('span[col='+ colNo +']');
			for(var i = 0; i < columnCells.length; i++){
				var currentCell = columnCells[i];
				currentCell.setAttribute('activated','');
			}
		}
	},	
	sample: '@{sample}',
	setSample: function(sample){
		this.sample = sample;
		this.parser.updateParserView(this.sample, []);
	},
	markups: '@{markups}',
	setMarkups: function(markups){
		this.parser.updateParserView(this.sample, markups);
	},	
	listeners: {
		afterrender: function() {					
			var iframe = this.el.query('iframe')[0];
			iframe.src = '/resolve/actiontask/editorstub.jsp?type=table&' + clientVM.CSRFTOKEN_NAME + '=' + clientVM.getPageToken('actiontask/editorstub.jsp');
			var me = this;
			this._vm.on('updateColumnSelection',function(colNo){
				me.parser.updateColumnSelection(colNo);
			});	
			this._vm.on('updateCaptureColumn',function(markups){
				me.parser.updateParserView(me.sample, markups);
			});					
			me.parser.eventEmitter = this;
			//Initialize Parser Once the frame is ready.	
			if (iframe.attachEvent)
				iframe.attachEvent('onload', function() {
					me.parser.initParser(iframe);
				});
			else if (iframe.addEventListener){
				iframe.addEventListener('load', function() {
					me.parser.initParser(iframe);
				}, false);
			};
		},
		sampleChanged: '@{updateSample}',
		parserIsLoaded: function(){
			this._vm.set('parserReady', true);
		},
		contentSelectionChanged: '@{processHighlightSelection}',
		columnSelectionChanged: '@{processColumnSelection}',	
		beforedestroy : function(){
			//Mark this iframe dom to be collected by GC. 
			//By default events that are registered on #document is marked as not collected by GC (Not sure why Extjs doing that)
			if(this.rendered){
				var document = this.parser.cdoc();
				var iframeDom = Ext.fly(document);
				if(iframeDom && iframeDom.$cache)
					iframeDom.$cache.skipGarbageCollection = false;
			}
		}
	}
};

glu.defView('RS.actiontaskbuilder.TableParser',{
	itemId : 'table',
	layout : 'card',
	activeItem : '@{activeScreen}',
	dockedItems: [{
		xtype: 'toolbar',
		cls : 'rs-dockedtoolbar',
		defaults: {
			cls: 'rs-small-btn rs-btn-dark'		
		},
		items:['clearSample','clearMarkup','|','showCode','showMarkup']
	}],	
	items : [{
		//Markup Screen
		xtype : 'container',		
		layout : {
			type : 'vbox',
			align : 'stretch'
		},	
		items:[{
			xtype: 'container',	
			layout : 'border',	
			height : '@{controlHeight}',		
			items: [RS.actiontaskbuilder.parserTableConfig,{
				//Control
				width : 600,				
				region : '@{controlBorderRegion}',
				margin : '@{controlMargin}',
				xtype : 'container',			
				layout : {
					type : 'vbox',
					align : 'stretch'
				},
				items : [{
					xtype : 'fieldset',
					title : '~~parserControl~~',	
					padding : '5 10',
					height : 170,
					layout: {
						type:'vbox',
						align: 'stretch'
					},
					defaults : {
						labelWidth : 140,
						editable : false	
					},
					items: [{
						xtype : 'toolbar',
						cls : 'rs-dockedtoolbar',
						defaults : {
							cls : 'rs-small-btn rs-btn-dark'
						},
						items : ['applySeparator','captureVariable']
					},{
						xtype: 'combobox',						
						store: '@{columnSeparatorStore}',
						queryMode: 'local',
						displayField: 'display',
						valueField: 'value',
						name:  'columnSeparator'
					},{
						xtype: 'combobox',						
						store: '@{rowSeparatorStore}',
						queryMode: 'local',
						displayField: 'display',
						valueField: 'value',
						fieldLabel:  '~~rowSeparator~~',
						value: '@{rowSeparator}'						
					},{
						xtype: 'combobox',					
						name: 'selectedColumn',
						store: '@{columnDropdownStore}',
						displayField: 'display',
						valueField: 'value',
						queryMode: 'local',									
						cls: 'no-input-border combo',
						tpl: Ext.create('Ext.XTemplate',
							'<tpl for=".">',
							'<div class="x-boundlist-item"><div style="margin-top:5px;">Column {value} </div><div class="column-picker" style="background:{color}"></div></div>',
							'</tpl>'
						)
					}]				
				},{
					//Boundary
					xtype : 'fieldset',
					margin : 0,	
					flex : 1,				
					padding : '5 10 10 10',			
					title: '~~boundaryTitle~~',			
					layout: {
						type:'vbox',
						align: 'stretch'
					},				
					items: [{
						xtype: 'button',
						name: 'updateBoundary',
						maxWidth : 80,
						cls: 'rs-small-btn rs-btn-dark'
					},{
						xtype : 'fieldcontainer',
						fieldLabel: '~~fromField~~',				
						labelWidth : 100,
						defaults : {
							margin : '1 0'
						},
						layout : {
							type : 'vbox',
							align : 'stretch'
						},
						items: [{
							xtype : 'radiofield',					
							boxLabel: '~~beginningOutputLabel~~',
							value: '@{startOutput}',					
							handler: '@{startOutputIsSelected}'
						},{
							xtype : 'container',					
							layout : 'hbox',					
							items : [{
								xtype : 'radiofield',
								boxLabel: '~~firstOccurrenceLabel~~',
								value: '@{!startOutput}',					
							},{
								xtype: 'textfield',
								name: 'parseStart',						
								hideLabel : true,
								margin : '0 0 0 10',
								flex : 1
							}]
						},{
							xtype: 'checkboxfield',					
							boxLabel: '~~inclusive~~',
							value: '@{startTextIncluded}'
						}]
					},{
						xtype : 'fieldcontainer',
						fieldLabel: '~~toField~~',
						labelWidth : 100,
						defaults : {
							margin : '1 0'
						},
						layout : {
							type : 'vbox',
							align : 'stretch'
						},
						items: [{
							xtype : 'radiofield',					
							boxLabel: '~~endOutputLabel~~',
							value: '@{endOutput}',			
							handler: '@{endOutputIsSelected}'
						},{
							xtype : 'container',					
							layout : 'hbox',
							items : [{
								xtype : 'radiofield',
								boxLabel: '~~firstOccurrenceLabel~~',
								value: '@{!endOutput}',					
							},{
								xtype: 'textfield',
								name: 'parseEnd',						
								hideLabel : true,
								margin : '0 0 0 10',
								flex : 1
							}]
						},{
							xtype: 'checkboxfield',					
							boxLabel: '~~inclusive~~',
							value: '@{endTextIncluded}'
						}]
					}]
				}],
				listeners : {
					afterrender : function(panel){
						panel._vm.on('embeddedLayout', function(isEmbedded){								
							if(isEmbedded){
								panel.setBorderRegion('south');							
							}
							else {
								panel.setBorderRegion('east');							
							}
						});
					}
				}
			}]	
		},{
			xtype: 'component',		
			html: '~~variableTable~~',
			margin : '15 0 8 0',	
		},{
			xtype: 'component',		
			hidden: '@{!variableStoreIsEmpty}',
			html: '~~noVariableText~~'
		},{
			xtype: 'grid',	
			cls: 'rs-grid-dark',						
			store: '@{variableStore}',
			columns: '@{variableColumn}',
			hidden: '@{variableStoreIsEmpty}',
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}],
			selModel: {
				selType: 'cellmodel'
			},
			listeners: {					
				removeVariable: '@{removeVariable}',
				validateedit: function(editor, context){
					if(context.field == "variable"){						
						var store = context.store;
						var newVal = context.value;
						var currentRowIdx = context.rowIdx;
						if(store.findExact(context.field, newVal) != currentRowIdx && store.findExact(context.field, newVal) != -1){
							this._vm.displayDuplicateNameMsg();
							return false;
						}
						else if(!RS.common.validation.VariableName.test(newVal)){
							this._vm.displayInvalidCharacter();
							return false;
						}
					}
				}				
			}
		}]
	},{
		xtype : '@{codeScreen}'
	}]
})