glu.defView('RS.actiontaskbuilder.Execute', {
	asWindow: {
		width: 400,
		maxHeight: 500,
		title: '~~executeTitle~~',
		cls : 'rs-modal-popup rs-execute-popup',
		draggable: false,
		target: '@{target}',
		listeners: {
			render: function(panel) {
				if (panel.target) {
					Ext.defer(function() {
						panel.alignTo(panel.target, 'tr-br')
					}, 1)
				}
			}
		}
	},
	buttonAlign: 'left',
	buttons: [{
		name:'execute', 
		cls:'rs-proceed-button'
	},{
		name : 'cancel',
		cls : 'rs-small-btn rs-btn-light'
	}],
	padding : 10,

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'fieldcontainer',
		layout: 'hbox',
		hidden: '@{embedded}',
		items: [{
			xtype: 'radio',
			name: 'worksheetType',
			value: '@{newWorksheet}',
			boxLabel: '~~newWorksheet~~',
			hidden: '@{sirContext}'
		}, {
			xtype: 'radio',
			padding: '0px 0px 0px 10px',
			name: 'worksheetType',
			value: '@{activeWorksheet}',
			boxLabel: '~~activeWorksheet~~',
			hidden: '@{sirContext}'
		}]
	}, {
		xtype: 'checkbox',
		name: 'debug',
		hideLabel: true,
		boxLabel: '~~debug~~'
	}, {
		xtype: 'fieldcontainer',
		layout: 'hbox',
		items: [{
			xtype: 'checkbox',
			name: 'mock',
			hideLabel: true,
			boxLabel: '~~mock~~'
		}, {
			xtype: 'combo',
			padding: '0px 0px 0px 10px',
			flex: 1,
			name: 'mockName',
			store: '@{mockStore}',
			editable: '@{mockEditable}',
			displayField: 'uname',
			valueField: 'uname',
			queryMode: 'local',
			hideLabel: true
		}]
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		title: '~~parameters~~',
		name: 'params',
		margin : '0 0 10 0',
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items :  ['addParam','removeParams']
		}],
		store: '@{paramStore}',
		columns: '@{paramColumns}',
		features: [{
			ftype: 'grouping',
			groupHeaderTpl: [
				'{name:this.formatName}',
				{
					formatName: function(name) {
						if (name) {
							return name+'S';
						} else {
							return 'PARAMS'
						}
					}
				}
			]
		}],
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 2
		}],
		viewConfig: {
			markDirty: false
		},
		flex: 1,
		listeners: {
			render: function(grid) {
				grid.store.grid = grid;
				grid.on('celldblclick', function(comp ,td, cellIndex, record, tr, rowIndex, e, eOpts )  {
					if (!this._vm.editParamsIsEnabled) {
						return false;
					}
					var param = this.store.getAt(rowIndex);
					if (param.get('encrypt') && param.get('value') == '*****') {
						param.set('value', '');
						param.set('origValue', '');
						param.set('modified', true);
					}
				})				
			}
		}
	}]
})
