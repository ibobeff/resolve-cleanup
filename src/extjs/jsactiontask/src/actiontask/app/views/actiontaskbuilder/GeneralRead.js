glu.defView('RS.actiontaskbuilder.GeneralRead', {
	parentLayout: 'quickAccess',
	itemId: 'GeneralRead',
	title: '@{title}',
	cls: 'block-model',
	hidden: '@{hidden}',
 	header: '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '10 15 15 15',
	defaults: {
		labelWidth: 130,
		margin: 0,
		padding: 0
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'displayfield',
		name: 'namespace',
		htmlEncode : true
	}, {
		xtype: 'displayfield',
		name: 'name',
		htmlEncode : true
	}, {
		xtype: 'displayfield',
		fieldLabel: '~~type~~',
		name: 'type'
	}, {
		defaults: {
			labelWidth: 130,
		},
		items: [{
		xtype: 'container',
		layout: 'hbox',
		items:[{
			xtype: 'displayfield',
			name: 'assignedToUserName',
			fieldLabel: '~~assignedTo~~',
			labelWidth: 130,
			border: 'none'
		}, {
			xtype: 'image',
			cls: 'rs-btn-edit',
			hidden: '@{jumpToAssignedToIsHidden}',
			listeners: {
				handler: '@{jumpToAssignedTo}',
				render: function(image) {
					image.getEl().on('click', function() {
						image.fireEvent('handler', this);
					}, this);
				}
			}
		}]
	}, {
		xtype: 'displayfield',
		cls: 'summary-displayfield',
		name: 'summary',
		padding: '0 0 5 0',
		renderer: RS.common.grid.plainRenderer()
	}, {
		xtype: 'displayfield',
		cls: 'description-displayfield',
		fieldLabel: '~~description~~',
		name: 'description',
		html: '@{description}',
		htmlEncode : false
	}, {
		xtype: 'checkbox',
		name: 'active',
		margin: '0 0 5 135',
		hideLabel: true,
		readOnly: true,
		boxLabel: '~~active~~'
	}, {
		xtype: 'checkbox',
		name: 'logResult',
		margin: '0 0 5 135',
		hideLabel: true,
		readOnly: true,
		boxLabel: '~~logResult~~'
	}, {
		xtype: 'checkbox',
		name: 'displayInWorksheet',
		margin: '0 0 5 135',
		hideLabel: true,
		readOnly: true,
		boxLabel: '~~displayInWorksheet~~'
		}]
	}, {
		xtype: 'fieldset',
		title : '~~rolesReadTitle~~',
		margin: '0 0 10 0',
		padding: '10 15 15 15',
		items: [{
			xtype: 'checkbox',
			name: 'defaultRoles',
			margin: 0,
			hideLabel: true,
			readOnly: true,
			boxLabel: '~~usesDefaultRoles~~'
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			hidden: '@{hideAccessRights}',
			name: 'right',
			store: '@{accessStore}',
			emptyText: '~~rolesEmptyText~~',
			viewConfig: {
				deferEmptyText: false
			},
			columns: [{
				dataIndex: 'uname',
				filterable: true,
				flex: 5,
				header: '~~roleName~~',
				hideable: false,
				sortable: true
			}, {
				renderer: RS.common.grid.booleanRenderer(),
				align: 'center',
				dataIndex: 'read',
				filterable: true,
				flex: 1,
				header: '~~roleReadRight~~',
				hideable: false,
				sortable: true
			}, {
				renderer: RS.common.grid.booleanRenderer(),
				align: 'center',
				dataIndex: 'write',
				filterable: true,
				flex: 1,
				header: '~~roleWriteRight~~',
				hideable: false,
				sortable: true
			}, {
				renderer: RS.common.grid.booleanRenderer(),
				align: 'center',
				dataIndex: 'execute',
				filterable: true,
				flex: 1,
				header: '~~roleExecuteRight~~',
				hideable: false,
				sortable: true
			}, {
				renderer: RS.common.grid.booleanRenderer(),
				align: 'center',
				dataIndex: 'admin',
				filterable: true,
				flex: 1,
				header: '~~roleAdminRight~~',
				hideable: false,
				sortable: true,
				listeners: {
					afterrender: function (column) {
						if (this.up('grid')._vm.hideAdminColumn) {
							column.hide();
						} else {
							column.show();
						}
					}
				}
			}]
		}]
	}, {
		xtype: 'fieldset',
		title: '~~optionsReadTitle~~',
		margin: 0,
		padding: '10 15 15 15',
		items: [{
			xtype: 'displayfield',
			hidden: '@{hasOptions}',
			margin: 0,
			padding: 0,
			value: '~~optionsEmptyText~~'
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			hidden: '@{!hasOptions}',
			store: '@{optionsStore}',
			emptyText: '~~optionsEmptyText~~',
			viewConfig: {
				deferEmptyText: false
			},
			columns: [{
				dataIndex: 'uname',
				filterable: true,
				flex: 1,
				header: '~~uname~~',
				hideable: false,
				sortable: true
			}, {
				header: '~~uvalue~~',
				dataIndex: 'uvalue',
				filterable: false,
				flex: 2,
				sortable: true,
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~udescription~~',
				dataIndex: 'udescription',
				filterable: false,
				flex: 3,
				sortable: true,
				renderer: RS.common.grid.plainRenderer()
			}]
		}]
	}],

	listeners: {
		afterrender: function(panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	}
});