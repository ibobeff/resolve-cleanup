glu.defView('RS.actiontaskbuilder.Expression', {
	xtype: 'container',
	cls: 'expression-container',
	defaults: {
		padding: 5
	},
	items: [{
		xtype: 'container',
		layout: 'hbox',
		cls: 'expression',
		defaults: {
			margin: '0 5 0 0',
			padding: 0,
		},
		items: [{
			xtype: 'displayfield',
			fieldCls: 'expression-field',
			hidden: '@{isExpressionHidden}',
			value: '@{expression}',
			fieldCls: 'expression-text',
			renderer: RS.common.grid.plainRenderer(),
			listeners: {
				render: function (c) {
					c.getEl().on('click', function () {
		                this.fireEvent('edit', this);
		            }, this);
				},
				edit: '@{edit}'
			}
		}, {
			xtype: 'image',
			hidden: '@{isRemoveHidden}',
		    src: '/resolve/css/resources/images/remove.png',
		    height: '21px',
			width: '28px',
			margin: '0 5 0 5',
			style: {
				verticalAlign: 'bottom'
			},
		    listeners: {
		        render: function (c) {
		            c.getEl().on('click', function () {
		                this.fireEvent('remove', this);
		            }, this);
		        },
		        remove: '@{remove}'
		    }
		}, {
			xtype: 'displayfield',
			fieldCls: 'expression-text-bold',
			value: 'OR',
			hidden: '@{isOrHidden}',
			margin: '0 0 0 5'
		}, {
			xtype: 'image',
			hidden: '@{isAddHidden}',
			src: '/resolve/css/resources/images/add.png',
		    height: '21px',
			width: '28px',
			margin: 0,
			style: {
				verticalAlign: 'bottom'
			},
		    listeners: {
		        render: function (c) {
		            c.getEl().on('click', function () {
		                this.fireEvent('add', this);
		            }, this);
		        },
		        add: '@{add}'
		    }
		}]
	}, {
		xtype: 'form',
		cls: 'expression-form',
		hidden: '@{isFormHidden}',
		defaults: {
			width: 400,
			margin: '0 0 10 0',
			padding: 0,
			queryMode: 'local',
			displayField: 'text',
			valueField: 'value'			
		},
		padding: '10 10 0 10',
		margin: 10,
		items: [{
			xtype: 'combobox',
			name: 'category',
			store: '@{categoryStore}',
			editable: false,
			hidden: '@{addMode}'
		}, {
			xtype: 'combobox',
			name: 'criteria',
			fieldLabel: '~~determiner~~',
			store: '@{determinerStore}',
			editable: false
		}, {
			xtype: 'combobox',
			name: 'leftOperandType',
			fieldLabel: '~~operandType~~',
			emptyText: '~~selectType~~',
			store: '@{operandTypeStore}',
			editable: false			
		}, {
			xtype: 'textfield',
			name: 'leftOperandName',
			fieldLabel: '~~operand~~',
			hidden: '@{!leftOperandIsText}',		
			emptyText: '~~enterOperandValue~~',			
		}, {
			xtype: 'combobox',
			name: 'leftOperandName',
			fieldLabel: '~~operand~~',
			hidden: '@{leftOperandIsText}',		
			emptyText: '~~selectOperand~~',
			store: '@{leftOperandStore}',
			editable: '@{leftOperandEditable}',
			displayField: 'name',
			valueField: 'name',	
			listeners : {
				afterrender : function(){
					this.setRawValue(this._vm.leftOperandName);
				}
			}	
		}, {
			xtype: 'combobox',
			name: 'operator',
			valueField : 'value',
			emptyText: '~~selectOperator~~',
			store: '@{operatorStore}',
			editable: false
		}, {
			xtype: 'combobox',
			name: 'rightOperandType',
			fieldLabel: '~~operandType~~',
			emptyText: '~~selectType~~',
			store: '@{operandTypeStore}',
			editable: false,			
		}, {
			xtype: 'textfield',
			name: 'rightOperandName',
			fieldLabel: '~~operand~~',
			hidden: '@{!rightOperandIsText}',		
			emptyText: '~~enterOperandValue~~'			
		}, {
			xtype: 'combobox',
			name: 'rightOperandName',
			fieldLabel: '~~operand~~',
			hidden: '@{rightOperandIsText}',		
			emptyText: '~~selectOperand~~',
			store: '@{rightOperandStore}',
			editable: '@{rightOperandEditable}',
			displayField: 'name',
			valueField: 'name',	
			listeners : {
				afterrender : function(){
					this.setRawValue(this._vm.rightOperandName);
				}
			}			
		}],

		dockedItems: [{
			xtype: 'toolbar',
			dock: 'bottom',
			margin: '0 0 10 0',
			padding: 0,
			items: ['->',{
				name:'done',		
				hidden : '@{addMode}',
				cls: 'rs-small-btn rs-btn-dark',
				itemId: 'doneBtn'
			},{
				name:'done',
				text : '~~add~~',
				hidden : '@{!addMode}',			
				cls: 'rs-small-btn rs-btn-dark',
			},{
				name:'cancel',
				cls: 'rs-small-btn rs-btn-light'
			}]
		}]
	}]
});