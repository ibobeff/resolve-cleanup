glu.defView('RS.actiontaskbuilder.MockRead', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: '@{classList}',
 	header: '@{header}',
	hidden: '@{hidden}',
 	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: 15,

	defaults: {
		margin: 0,
		padding: 0
	},

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	
	dockedItems: [{
		xtype: 'toolbar',	
		hidden: '@{!isShowingDetails}',
		padding : '10 15 0 15',
		items: [{ 
			name:'back',
			cls: 'rs-small-btn rs-btn-dark' 
		}]
	}],

	items: [{
		name: 'Mock',
		xtype: 'container',

		defaults: {
			margin: 0,
			padding: 0
		},

		items:[{
			xtype: 'displayfield',
			hidden: '@{hasMockValues}',
			value: '~~emptyMockMsg~~'
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			hidden: '@{!hasMockValues}',
			store: '@{mockStore}',
			maxHeight: 300,
			userDateFormat: '@{..userDateFormat}',
			viewDetailsTooltip: '@{viewDetailsTooltip}',
			listeners: {
				scope: this,
				viewmockdetails: '@{viewMockDetails}',
			},

			defaults: {
				margin: 0,
				padding: 0
			},			

			columns: [{
				xtype: 'actioncolumn',
				tdCls: 'action-cell',
				hideable: false,
				sortable: false,
				align: 'center',
				flex: 0,
				width: 34,			
				iconCls: 'rs-fa-details',
				items: [{
					getTip: function(v, meta, rec) {
						return this.up('grid').viewDetailsTooltip;
					},
					handler: function(view, rowIndex, colIndex, item, e, record ) {
						if (record.get('id') != -1) {
							this.up('grid').fireEvent('viewmockdetails', view, record);
						}
					}
				}]
			}, {
				header: '~~name~~',
				dataIndex: 'uname',
				hideable: false,
				sortable: true,
				flex: 1,
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~udescription~~',
				dataIndex: 'udescription',
				hideable: false,
				sortable: true,
				flex: 3,
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~sysUpdatedOn~~',
				dataIndex: 'sysUpdatedOn',
				hideable: true,
				sortable: true,
				flex: 1,
				renderer: function(value) {
					if (value) {
						return Ext.Date.format(new Date(value), this.userDateFormat);
					}
				}
			}, {
				header: '~~sysUpdatedBy~~',
				dataIndex: 'sysUpdatedBy',
				hideable: true,
				sortable: true,
				flex: 1,
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~sysCreatedOn~~',
				dataIndex: 'sysCreatedOn',
				hideable: true,
				sortable: true,
				hidden: true,
				flex: 1,
				renderer: function(value) {
					if (value) {
						return Ext.Date.format(new Date(value), this.userDateFormat);
					}
				}
			}, {
				header: '~~sysCreatedBy~~',
				dataIndex: 'sysCreatedBy',
				hideable: true,
				sortable: true,
				hidden: true,
				flex: 1,
				renderer: RS.common.grid.plainRenderer()
			}]
		}]
	}, {
		name: 'MockDetails',
		xtype: 'container',
		margin: 0,
		padding: 0,

		defaults: {
			margin: '0 0 10 0',
			padding: 0,
		},

		items: [{
			xtype: 'displayfield',
			fieldLabel: '~~mockName~~',
			value: '@{mockName}',
			margin: 0
		}, {
			xtype: 'displayfield',
			fieldLabel: '~~mockDescription~~',
			value: '@{mockDescription}',
			hidden: '@{!hasMockDescription}',
			margin: 0,
			renderer: RS.common.grid.plainRenderer()
		}, {
			xtype: 'displayfield',
			fieldLabel: '~~mockDescription~~',
			value: '~~noMockDescription~~',
			hidden: '@{hasMockDescription}',
			margin: 0
		}, {
			xtype: 'displayfield',
			fieldStyle: {
				fontWeight: 'bold'
			},
			value: '~~parameters~~',
			margin: 0
		}, {
			xtype: 'container',
			margin: 0,

			defaults: {
				margin: '0 0 10 0',
				padding: 0,
			},

			items: [{
				xtype: 'displayfield',
				hidden: '@{hasMockParamValues}',
				value: '~~emptyMockParamMsg~~',
				margin: 0,
			}, {
				xtype: 'grid',
				cls : 'rs-grid-dark',
				store: '@{mockDetailsStore}',
				hidden: '@{!hasMockParamValues}',
				maxHeight: '@{maxHeight}',
				columns: [{
					header: '~~type~~',
					dataIndex: 'type',
					hideable: false,
					sortable: true,
					flex: 1,
					renderer: RS.common.grid.plainRenderer()
				}, {
					header: '~~name~~',
					dataIndex: 'name',
					hideable: false,
					sortable: true,
					flex: 2,
					renderer: RS.common.grid.plainRenderer()
				}, {
					header: '~~uvalue~~',
					dataIndex: 'value',
					hideable: false,
					sortable: true,
					flex: 2,
					renderer: RS.common.grid.plainRenderer()
				}]
			}, {
				xtype: 'displayfield',
				fieldStyle: {
					fontWeight: 'bold'
				},
				hidden: '@{rawIsHidden}',
				value: '~~raw~~'
			}, {
				xtype: 'container',
				margin: 0,
				padding: 0,
				hidden: '@{rawIsHidden}',

				layout: {
					type: 'vbox',
					align: 'stretch'
				},
	
				resizable: {
					handles: 's',
					pinned: true
				},

				items: [{
					xtype: 'textarea',
					name: 'mockRaw',
					readOnly: true,
					fieldLabel: '',
					flex: 1,
					margin: 0,
					padding: 0
				}]
			}]
		}]
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});			

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},
		viewRendered: '@{viewRenderCompleted}'
	}
});
