glu.defView('RS.actiontaskbuilder.PreprocessorEdit', {
	parentLayout: 'quickAccess',
	xtype: 'codeeditorpanel',
	title: '@{title}',
	cls: '@{classList}',
	hidden: '@{hidden}',
	header: '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	padding: 0,
	bodyPadding: '10 15 15 15',
	minHeight: '@{minHeight}',
	height: '@{height}',

	resizable: {
		handles: 's',
		pinned: true,
		minHeight: '@{minResize}'
	},
	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	
	defaults: {
		margin: 0,
		padding: 0
	},

	items: [{
		xtype: 'AceEditor',
		parser: 'groovy',
		height: '@{height}',
		name: 'content',	
		flex: 1,		
		listeners: {
			change: function () {
				this.up('codeeditorpanel').fireEvent('contentchange', this, this);
			}
		}
	},{
		xtype: 'label',
		cls: '@{editorClass}',
		margin : '10 0 0 0',
		listeners: {
			afterrender: function(label) {
				this.up('codeeditorpanel').fireEvent('warningmsgshowed', this, label);
			}
		}
	}],

	dockedItems: [{
		xtype: 'toolbar',		
		padding : '0 15 10',
		dock: 'bottom',
		items: ['->', {			
			name: 'close',
			cls: 'rs-med-btn rs-btn-dark'
		}]
	}],

	listeners: {
		contentchange: '@{contentChange}',
		warningmsgshowed: '@{warningMsgShowed}',
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});	

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	}	
});
