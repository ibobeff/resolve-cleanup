glu.defView('RS.actiontaskbuilder.GeneralReference',{
	autoScroll : true,
	layout : 'fit',
	store: '@{referencesStore}',
	padding: '10',
	bodyPadding: '10 0 0 0',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{referenceTitle}'
		}]
	}],
	items: [{
		id: 'referencesTab',
		autoScroll: true,
		xtype: 'grid',
		name: 'references',
		store: '@{referencesStore}',
		columns: [{
			header: '~~name~~',
			dataIndex: 'name',
			hideable: false,
			filterable: false,
			sortable: true,
			flex: 1
		}, {
			header: '~~refInContent~~',
			dataIndex: 'refInContent',
			filterable: false,
			width: 100,
			renderer: RS.common.grid.booleanRenderer()
		}, {
			header: '~~refInMain~~',
			dataIndex: 'refInMain',
			filterable: false,
			width: 100,
			renderer: RS.common.grid.booleanRenderer()

		}, {
			header: '~~refInAbort~~',
			dataIndex: 'refInAbort',
			filterable: false,
			width: 100,
			renderer: RS.common.grid.booleanRenderer()
		}],
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTooltip: RS.common.locale.viewColumnTooltip,
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'name'
		},
		listeners: {
			editAction: '@{editReference}'
		}
	}]
});
