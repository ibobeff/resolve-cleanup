RS.actiontaskbuilder.ReadOnlyParserGenericRenderer = {
	height : 480,
	html: '<iframe spellcheck="false" autocomplete="off" frameborder="0" style="background:#fcfaf8;width:100%;height:100%;border:1px solid #999;line-height: 0;" src=""></iframe>',
	dump : '@{dump}',
	parser : {
		parserType : '@{parserType}',
		initParser: function(iframe){		
			var me = this;			
			me.iframe = iframe;
			me.win = iframe.contentWindow;	
			var document = this.cdoc();
			var docBody = document.body;	
			docBody.innerHTML = '<pre class="parser-content" contenteditable="false"></pre>';		
			if (docBody && ('spellcheck' in docBody))
				docBody.spellcheck = false;			
			me.eventEmitter.fireEvent('parserIsLoaded',me,true);
		},
		cdoc: function(){
			return this.win.document;
		},
		placeHolders: {
			space: '<span marker="space" class="format">&nbsp;</span>',
			new_line:  '<span marker="new_line" class="return"><br></span>'
		},
		convert: function(text, pre, post) {
			if(!text)
				return "";
			var processedText = text.replace(/ /g, this.placeHolders.space).replace(Ext.is.Windows ? (/\r\n/g): (/\n/g), this.placeHolders.new_line);
			return processedText;
		},		

		//Text and Table Parser
		renderTextBaseMarkup: function(sample,markups){
			//Get pts for each markup
			var pts = [];
			var parseStartPosition = -1;
			var parseEndPosition = -1;
			var hasParseStart = false;
			var hasParseEnd = false;
			Ext.each(markups, function(markup){			
				if(markup.type == 'parseStart'){
					hasParseStart = true;
					parseStartPosition = markup.from + ( markup.included ? 0: markup.length );
					pts.push(parseStartPosition);
				}
				else if(markup.type == 'parseEnd'){
					hasParseEnd = true;
					parseEndPosition = markup.from + ( markup.included ? markup.length: 0 );
					pts.push(parseEndPosition);
				}
				else{
					pts.push(markup.from);
					pts.push(markup.from + markup.length);
				}
			})
			pts = Ext.Array.unique(pts);
			//Add start and end pts
			if(pts.indexOf(0) == -1)
				pts.push(0);
			if (pts.indexOf(sample.length) == -1)
				pts.push(sample.length);
			pts.sort(function(a, b) {
				return a > b ? 1: (a == b ? 0: -1);
			});
			var chopped = [];
			for (var i = 0; i < pts.length - 1; i++){
				if(parseStartPosition == pts[i]){
					chopped.push({
						type: "parseStart"					
					});
				}
				else if(parseEndPosition == pts[i]){
					chopped.push({
						type: "parseEnd"					
					});
				}
				chopped.push({
					text: sample.substring(pts[i], pts[i + 1]),
					from: pts[i],
					to: pts[i + 1],
					actionId: null
				});
			}
		
			//Sort all pieces and add type and id for each chopped piece.
			Ext.each(chopped, function(slice, idx) {
				Ext.each(markups, function(markup) {
					if(markup.type == slice.type && ( markup.type == "parseStart" || markup.type == "parseEnd")){ //Only slice with parseEnd and parseStart has a type at this point						
						slice.actionId = markup.actionId;
					}
					else if (markup.type != "parseStart" && markup.type != "parseEnd" && slice.from >= markup.from && slice.to <= (markup.from + markup.length)) {
						slice.actionId = markup.actionId;
						slice.type = markup.type;	
						slice.action = markup.action || 'default';				
						if(markup.action == 'capture' || markup.type == 'column'){
							slice.color = markup.color;
						}
						if(markup.type == 'column'){
							slice.col = markup.column;
						}
					}
				});
			});
			var done = [];
			var beginParseSection = false ;
			var endParseSection = false;
			Ext.each(chopped, function(slice, idx) {				
				var convertedContent = this.convert(Ext.htmlEncode(slice.text));
				if(slice.type == 'parseStart')
					beginParseSection = true;				
				if((hasParseStart && !beginParseSection) || (hasParseEnd && endParseSection)){
					done.push(Ext.String.format('<span unselectable boundary contenteditable="false">{0}</span>', convertedContent));
				}
				else {
					if (slice.actionId != null){
						if(slice.action == 'capture'){
							done.push(Ext.String.format('<span {1} marked unselectable contenteditable="false" action="capture" type={2} style="background:{3}" ignore>{0}</span>', convertedContent, 'actionId="' + slice.actionId + '"', slice.type, slice.color));
						}
						else if(slice.type == 'column'){
							done.push(Ext.String.format('<span {1} marked unselectable contenteditable="false" action="default" type={2} col={4} style="border-color:{3};border-width:2px;">{0}</span>', convertedContent, 'actionId="' + slice.actionId + '"', slice.type, slice.color, slice.col));
						}
						else
							done.push(Ext.String.format('<span {1} marked unselectable contenteditable="false" type={2}>{0}</span>', convertedContent, 'actionId="' + slice.actionId + '"', slice.type))
					}
					else
						done.push(convertedContent);
				if(slice.type == 'parseEnd')
					endParseSection = true;
				}
			},this);
			this.cdoc().body.innerHTML ='<pre class="parser-content" contenteditable="false">' + done.join('') +'</pre>';;			
		},

		//XML Parser
		renderXMLBaseMarkup : function(currentSampleDom){
			var html = this.renderNode(currentSampleDom,0);
			this.cdoc().body.innerHTML = '<pre class="parser-content" contenteditable="true">' + html + '</pre>';
		},
		renderNode: function(node, indent) {			
			var id = Ext.data.IdGenerator.get('uuid').generate();
			node.pnid = id;
			if (Ext.isFunction(node.setAttribute))
				node.setAttribute('pnid', id);
			var str = '';
			switch (node.nodeType) {
				case 1:
					if (node.ignore) {
						str = Ext.String.format(
							'<span pnid="{0}" class="xmlSp rootText">{1}</span>',
							id,
							Ext.htmlEncode(Ext.String.format('<{0}>{1}</{0}>', node.tagName, node.innerHTML)));
						break;
					}
					var text = [];
					if (node.tagName != 'resolveparserroot' && node.tagName != 'error')
						text.push(this.preTag(node.tagName, node.attributes, id));
					if (node.tagName != 'error')
						Ext.each(node.childNodes, function(child) {
							text.push(this.renderNode(child, indent + 1));
						}, this)
					else
						text.push(Ext.htmlEncode(node.textContent))
					if (node.tagName != 'resolveparserroot' && node.tagName != 'error')
						text.push(this.postTag(node.tagName, id));
					var textStyle = '';
					if (node.tagName == 'error')
						textStyle = 'style="color:red"';					
					str = Ext.String.format(
						'<span pnid="{0}" class="xmlSp element"{2}>{1}</span>',
						id,
						text.join(''),
						textStyle);
					break;
				case 3:
					str = Ext.htmlEncode(node.textContent)
						.replace(/ /g, '<span ignore class="xmlSp space">&nbsp;</span>')
						.replace(/\n/g, '<span ignore class="xmlSp newline"><br></span>');
					str = Ext.String.format(
						'<span pnid="{0}" class="xmlSp {2}">{1}</span>',
						id,
						str, (node.parentNode.tagName != 'resolveparserroot' && node.parentNode.tagName != 'error' ? 'text': 'rootText'));
					break;
				case 4:
					//DEPRECATED
					str = Ext.htmlEncode(node.textContent)
						.replace(/ /g, '<span class="xmlSp space"></span>')
						.replace(/\n/g, '<span class="xmlSp newline"></span><br>');
					str = Ext.String.format(
						'<span pnid="{0}" class="xmlSp cdata"{2}>{1}</span>',
						id,
						str);
					break;
				case 9:
					var text = [];
					Ext.each(node.childNodes, function(child) {
						text.push(this.renderNode(child, 0));
					}, this)
					str = text.join('');
					break;
			}
			return str;
		},
		preTag: function(tagName, attrs, pnid) {
			var kv = [];
			for (var i = 0; i < attrs.length; i++) {
				if (attrs[i].name == 'pnid')
					continue;
				kv.push(Ext.String.format(
					'<span class="xmlSp attribute"{2}><span class="xmlSp attrName">{0}</span>=<span class="xmlSp attrValue">"{1}"</span></span>',
					attrs[i].name,
					Ext.String.escape(attrs[i].value).replace(/"/g, '\\"')
				));
			}

			return Ext.String.format(
				'<span class="xmlSp tag preTag">{0}</span><span pnid="{4}" class="xmlSp tagName">{1}</span>{3}<span class="xmlSp tag preTag">{2}</span>',
				Ext.htmlEncode('<'),
				tagName,
				Ext.htmlEncode('>'),
				kv.length > 0 ? ' ' + kv.join(' '): '',
				pnid
			);
		},
		postTag: function(tagName, id) {
			return Ext.String.format(
				'<span class="xmlSp tag postTag">{0}</span><span pnid="{3}" class="xmlSp tagName">{1}</span><span class="xmlSp tag postTag">{2}</span>',
				Ext.htmlEncode('</'),
				tagName,
				Ext.htmlEncode('>'),
				id
			);
		},
	},
	listeners: {
		afterrender: function() {		
			var iframe = this.el.query('iframe')[0];
			iframe.src = '/resolve/actiontask/editorstub.jsp?' + clientVM.CSRFTOKEN_NAME + '=' + clientVM.getPageToken('actiontask/editorstub.jsp');

			var me = this;
			me.parser.eventEmitter = this;
			//Initialize Parser Once the frame is ready.	
			if (iframe.attachEvent)
				iframe.attachEvent('onload', function() {
					me.parser.initParser(iframe);
				});
			else if (iframe.addEventListener){
				iframe.addEventListener('load', function() {
					me.parser.initParser(iframe);
				}, false);
			}	
			this._vm.on('renderMarkup', me.parser.renderTextBaseMarkup, me.parser);
			this._vm.on('renderXML', me.parser.renderXMLBaseMarkup, me.parser);			
		},
		parserIsLoaded: function(){
			this._vm.set('parserReady', true);
		},
		beforedestroy : function(){
			//Mark this iframe dom to be collected by GC. 
			//By default events that are registered on #document is marked as not collected by GC (Not sure why Extjs doing that)
			if(this.rendered){
				var document = this.parser.cdoc();
				var iframeDom = Ext.fly(document);
				if(iframeDom && iframeDom.$cache)
					iframeDom.$cache.skipGarbageCollection = false;
			}
		}
	}
} 
glu.defView('RS.actiontaskbuilder.ParserRead',{
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: '@{classList}',
	hidden: '@{hidden}',
	header : '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,	
	animCollapse: false,
	bodyPadding: '10 15 12 15',
	layout : 'card',
	activeItem : '@{activeWindow}',
	dockedItems : [{
		xtype : 'toolbar',
		hidden : '@{hasNoContent}',
		padding : '5 15 0',
		defaults : {
			cls : 'rs-med-btn rs-btn-dark'
		},
		items : [{
			xtype : 'displayfield',
			fieldLabel : '~~outputFormat~~',
			labelStyle : 'font-weight:bold',
			labelWidth: 130,
			value : '@{parserTypeName}'
		},'->',{			
			name: 'markupAndCapture',
			margin : 0,
			iconCls: 'icon-large icon-reply-all rs-icon'		
		},'test']
	}],
	items : [{
		xtype : 'component',	
		margin : '0 0 10 0',
		html : '~~sourceCodeSectionEmptyText~~'
	},{	
		dockedItems: [{
			xtype: 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults: {
				cls: 'rs-small-btn rs-btn-dark',
				margin: '5 8 0 0'
			},
			items:['showCode','showMarkup']
		}],
		layout : 'card',
		activeItem : '@{activeMarkupScreen}',
		items : [{
			xtype : 'container',
			items : [RS.actiontaskbuilder.ReadOnlyParserGenericRenderer,
			{
				xtype: 'component',		
				html: '~~variableTable~~',
				margin : '15 0 8 0',	
			},{
				xtype: 'component',		
				hidden: '@{!variableStoreIsEmpty}',
				html: '~~noVariableText~~'
			},{
				xtype: 'grid',	
				cls: 'rs-grid-dark',						
				store: '@{variableStore}',
				columns: '@{variableColumn}',
				hidden: '@{variableStoreIsEmpty}',
				listeners : {
					afterrender : function(panel){
						this._vm.on('reconfigureVariableColumn', function(variableColumn){
							panel.reconfigure(undefined, variableColumn);
						})
					}
				}					
			}]
		},{		
			xtype : '@{codeScreen}'
		}]		
	},{
		xtype : '@{testControl}'
	}],	
	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},
		viewRendered: '@{viewRenderCompleted}'
	}	
})