RS.actiontaskbuilder.parserTextConfig = {
	region : 'center',	
	xtype : 'container',		
	html: '<iframe spellcheck="false" autocomplete="off" frameborder="0" style="width:100%;height:100%;border:1px solid #999;line-height: 0;" src=""></iframe>',
	parser: {	
		iframe: null,
		win: null,
		parserHintElement: null,
		//In Chrome mousedown and mousemove fire simultanuousely use the position to detect actual movement.
		mousePreviousPosition : false,
		hightLightTextFlag: false,

		initParser: function(iframe){		
			var me = this;			
			me.iframe = iframe;

			me.win = iframe.contentWindow;
			me.parserHintElement = '<div class=parserHintText contenteditable="false">PASTE YOUR OUTPUT SAMPLE HERE.</div>';
			var document = this.cdoc();
			var docBody = document.body;	
			docBody.innerHTML = '<pre class="parser-content" contenteditable="true">' + me.parserHintElement + '</pre>';		
			if (docBody && ('spellcheck' in docBody))
				docBody.spellcheck = false;

			//Disable typing on this iframe ( might add feature to allow user to type in future)
			Ext.fly(document).on('keydown', function(e) {
				if (!e.ctrlKey)
					e.preventDefault()
			});

			//Events on parser's sample editor.
			docBody.onpaste = function(e) {									
				var rawData = e && e.clipboardData ? e.clipboardData.getData('text/plain'): window.clipboardData.getData('Text');
				var lines = rawData.split(/\r*\n/);
				var data = lines.join(Ext.is.Windows ? '\r\n': '\n');
				var asPasted = true;			
				me.eventEmitter.fireEvent('sampleChanged', me, data, asPasted);				
				return false;
			}
			
			docBody.onmousedown = function(e) {
				if(e.button == 0){
					me.hightLightTextFlag = false;
					me.mousePreviousPosition = [e.pageX,e.pageY];
					me.deselectAllMarkup();
				}
				else 
					me.mousePreviousPosition = null;
			}

			docBody.onmousemove = function(e) {				
				if(me.mousePreviousPosition && e.pageX != me.mousePreviousPosition[0] && e.pageX != me.mousePreviousPosition[1])
					me.hightLightTextFlag = true;			
				else
					e.hightLightTextFlag = false;
			}
			function patternSelectionHandler(e) {				
				var selection = rangy.getSelection(me.iframe),
					validParent = selection && selection.anchorNode && selection.parentNode;
				
				//Selection outside boundary
				if(validParent && selection.anchorNode.parentNode.nodeType == 1 && selection.anchorNode.parentNode.hasAttribute('unselectable')){
					me.eventEmitter.fireEvent('selectionIsInvalid');
					return;
				}
				//Handle zero length selection
				if(me.isZeroLengthSelection(selection)){					
					return true;
				}

				if(me.hightLightTextFlag){
					//Selection happens inside marked node.
					if(validParent && selection.anchorNode.parentNode.nodeType == 1 && selection.anchorNode.parentNode.hasAttribute('marked')){
						me.eventEmitter.fireEvent('selectionIsInvalid');
						return;
					} else if (selection && selection.rangeCount) {
					    var range = selection.getRangeAt(0);
					    var markedElements = range.getNodes([1], function(node) {
					    	return node.hasAttribute('marked');				    	
					    });

					    if(markedElements.length > 0){
					    	me.eventEmitter.fireEvent('selectionIsInvalid');
					    	return;
					    }
					}

					me.eventEmitter.fireEvent('contentSelectionChanged', me, selection, docBody, false);
				}
				else {
					me.eventEmitter.fireEvent('parserIsDefault');
				}			
			}	
			Ext.fly(document).on('mouseup',function(){
				me.mousePreviousPosition = null;
				patternSelectionHandler();
			});		
			docBody.ondblclick = function(e){
				me.hightLightTextFlag = true;
				patternSelectionHandler();
				e.preventDefault();
			};
			//Disable drag and drop elements
			docBody.ondragstart = function(){
				return false;
			}
			docBody.ondrop = function(){
				return false;
			}			
			me.eventEmitter.fireEvent('parserIsLoaded',me,true);
		},
		cdoc: function(){
			return this.win.document;
		},
		placeHolders: {
			space: '<span marker="space" class="format">&nbsp;</span>',
			new_line:  '<span marker="new_line" class="return"><br></span>'
		},
		convert: function(text, pre, post) {
			if(!text)
				return "";
			var processedText = text.replace(/ /g, this.placeHolders.space).replace(Ext.is.Windows ? (/\r\n/g): (/\n/g), this.placeHolders.new_line);
			return processedText;
		},
		isZeroLengthSelection: function(selection){
			var isZeroLengthSelection = (selection.anchorNode == selection.focusNode && selection.anchorOffset == selection.focusOffset);
			//Check for Line Break.	
			if(isZeroLengthSelection){		
				var validAnchor = selection && selection.anchorNode;

				if(validAnchor && selection.anchorNode.nodeType == 1 && selection.anchorNode.getAttribute('marker') == 'new_line'){
					//Case 1: Selection on a line-break node (Line only contains linebreak.)
					//Make sure this new-line node is not inside a marked node.
					if(selection.anchorNode.parentNode && selection.anchorNode.parentNode.nodeType== 1 && selection.anchorNode.parentNode.hasAttribute('marked')) {
						this.eventEmitter.fireEvent('selectionIsInvalid');						
					} else {
						this.eventEmitter.fireEvent('contentSelectionChanged', this, selection, this.cdoc().body, true);
					}

				} else if(validAnchor && selection.anchorNode.length == selection.anchorOffset){			
					//Case 2: Selection on a node.
					var node = selection.anchorNode.nextSibling;

					if(!node && selection.anchorNode.parentNode) {
						node = selection.anchorNode.parentNode.nextSibling;
					}

					if(node && node.nodeType == 1 && node.getAttribute('marker') == 'new_line'){
						//Make sure this new-line node is not inside a marked node.
						if (node.parentNode) {
							if(node.parentNode.nodeType == 1 && node.parentNode.hasAttribute('marked'))
								this.eventEmitter.fireEvent('selectionIsInvalid');
							else
								this.eventEmitter.fireEvent('contentSelectionChanged', this, selection, this.cdoc().body, true);							
						}
					}
				}else {
					this.eventEmitter.fireEvent('parserIsDefault');
				}
			}			
			return isZeroLengthSelection;
		},
		updateParserView: function(sample,markups){
			//Get pts for each markup
			var pts = [];
			var parseStartPosition = -1;
			var parseEndPosition = -1;
			var hasParseStart = false;
			var hasParseEnd = false;
			Ext.each(markups, function(markup){			
				if(markup.type == 'parseStart'){
					hasParseStart = true;
					parseStartPosition = markup.from + ( markup.included ? 0: markup.length );
					pts.push(parseStartPosition);
				}
				else if(markup.type == 'parseEnd'){
					hasParseEnd = true;
					parseEndPosition = markup.from + ( markup.included ? markup.length: 0 );
					pts.push(parseEndPosition);
				}
				else{
					pts.push(markup.from);
					pts.push(markup.from + markup.length);
				}
			})
			pts = Ext.Array.unique(pts);
			//Add start and end pts
			if(pts.indexOf(0) == -1)
				pts.push(0);
			if (pts.indexOf(sample.length) == -1)
				pts.push(sample.length);
			pts.sort(function(a, b) {
				return a > b ? 1: (a == b ? 0: -1);
			});
			var chopped = [];
			for (var i = 0; i < pts.length - 1; i++){
				if(parseStartPosition == pts[i]){
					chopped.push({
						type: "parseStart"					
					});
				}
				else if(parseEndPosition == pts[i]){
					chopped.push({
						type: "parseEnd"					
					});
				}
				chopped.push({
					text: sample.substring(pts[i], pts[i + 1]),
					from: pts[i],
					to: pts[i + 1],
					actionId: null
				});
			}
		
			//Sort all pieces and add type and id for each chopped piece.
			Ext.each(chopped, function(slice, idx) {
				Ext.each(markups, function(markup) {
					if(markup.type == slice.type && ( markup.type == "parseStart" || markup.type == "parseEnd")){ //Only slice with parseEnd and parseStart has a type at this point						
						slice.actionId = markup.actionId;
					}
					else if (markup.type != "parseStart" && markup.type != "parseEnd" && slice.from >= markup.from && slice.to <= (markup.from + markup.length)) {
						slice.actionId = markup.actionId;
						slice.type = markup.type;	
						slice.action = markup.action || 'default';				
						if(markup.action == 'capture'){
							slice.color = markup.color;
						}
					}
				});
			});
			var done = [];
			var beginParseSection = false ;
			var endParseSection = false;
			Ext.each(chopped, function(slice, idx) {				
				var convertedContent = ((slice.type == 'endOfLine') ? '&crarr;': '') + this.convert(Ext.htmlEncode(slice.text));
				if(slice.type == 'parseStart')
					beginParseSection = true;				
				if((hasParseStart && !beginParseSection) || (hasParseEnd && endParseSection)){
					done.push(Ext.String.format('<span unselectable boundary contenteditable="false">{0}</span>', convertedContent));
				}
				else {
					if (slice.actionId != null){
						if(slice.action == 'capture'){
							done.push(Ext.String.format('<span {1} unselectable marked contenteditable="false" action="capture" type={2} style="background:{3}">{0}</span>', convertedContent, 'actionId="' + slice.actionId + '"', slice.type, slice.color));
						}
						else
							done.push(Ext.String.format('<span {1} unselectable marked contenteditable="false" type={2}>{0}</span>', convertedContent, 'actionId="' + slice.actionId + '"', slice.type))
					}
					else
						done.push(convertedContent);
				if(slice.type == 'parseEnd')
					endParseSection = true;
				}
			},this);
			if(done.length == 0)
				done.push(this.parserHintElement);
			this.cdoc().body.innerHTML ='<pre class="parser-content" contenteditable="true">' + done.join('') +'</pre>';

			//Add Event Handler for each markups
			this.liveMarkup();
		},		
		liveMarkup: function(){
			var me = this;
			Ext.each(Ext.fly(this.cdoc().body).query('span'),function(span){
				if(span.hasAttribute('marked') && span.getAttribute('action') != 'capture'){				
					Ext.fly(span).on('mouseover', function(e) {							
						Ext.fly(span).addCls('markup-over');
					});
					Ext.fly(span).on('mouseout', function(e) {
						Ext.fly(span).removeCls('markup-over');
					});
					Ext.fly(span).on('click', function(e) {	
						span.setAttribute('activated','');
						var actionId = span.getAttribute('actionId');
						me.eventEmitter.fireEvent('markupSelectionChanged', me, actionId);						
					});						
				}
			})
		},
		reselectMarkup : function(actionId){
			Ext.each(Ext.fly(this.cdoc().body).query('span[actionid=' + actionId + ']'),function(markup){
				markup.click();
			})
		},
		deselectAllMarkup : function(){		
			var activatedMarkups = Ext.fly(this.cdoc().body).query('span[marked][activated]');
			if(activatedMarkups.length > 0)
				activatedMarkups[0].removeAttribute('activated');
		}
	},	
	sample: '@{sample}',
	setSample: function(sample){
		this.sample = sample;
		this.parser.updateParserView(this.sample, []);
	},
	markups: '@{markups}',
	setMarkups: function(markups){
		this.markups = markups;
		this.parser.updateParserView(this.sample, markups);
	},	
	listeners: {
		afterrender: function() {					
			var iframe = this.el.query('iframe')[0];
			iframe.src = '/resolve/actiontask/editorstub.jsp?type=text&' + clientVM.CSRFTOKEN_NAME + '=' + clientVM.getPageToken('actiontask/editorstub.jsp');
			var me = this;			
			me.parser.eventEmitter = this;
			//Initialize Parser Once the frame is ready.	
			if (iframe.attachEvent)
				iframe.attachEvent('onload', function() {
					me.parser.initParser(iframe);
				});
			else if (iframe.addEventListener){
				iframe.addEventListener('load', function() {
					me.parser.initParser(iframe);
				}, false);
			}			
			this._vm.on('reselectMarkup', function(actionId){
				me.parser.reselectMarkup(actionId);
			});
		},
		sampleChanged: '@{updateSample}',		
		contentSelectionChanged: '@{processHighlightSelection}',
		markupSelectionChanged: '@{processMarkupSelection}',
		parserIsDefault: function(){
			this._vm.resetParser();			
		},
		parserIsLoaded: function(){
			this._vm.set('parserReady', true);
		},
		selectionIsInvalid: function(){			
			this._vm.set('selectionIsInvalid', true);
		},
		updateBoundaryFromMarkup: '@{updateBoundaryFromMarkup}',
		beforedestroy : function(){
			//Mark this iframe dom to be collected by GC. 
			//By default events that are registered on #document is marked as not collected by GC (Not sure why Extjs doing that)
			if(this.rendered){
				var document = this.parser.cdoc();
				var iframeDom = Ext.fly(document);
				if(iframeDom && iframeDom.$cache)
					iframeDom.$cache.skipGarbageCollection = false;
			}
		}
	}
}
glu.defView('RS.actiontaskbuilder.PlainTextParser',{
	itemId : 'text',
	layout : 'card',	
	activeItem : '@{activeScreen}',
	dockedItems: [{
		xtype: 'toolbar',
		cls : 'rs-dockedtoolbar',
		defaults: {
			cls: 'rs-small-btn rs-btn-dark'			
		},
		items:['clearSample','clearMarkup','|','showCode','showMarkup']
	}],	
	items : [{	
		//Markup Screen
		xtype : 'container',		
		layout : {
			type : 'vbox',
			align : 'stretch'
		},	
		items:[{
			xtype: 'container',				
			layout : 'border',	
			height : '@{controlHeight}',		
			items: [RS.actiontaskbuilder.parserTextConfig,{	
				//Control Panel
				width : 600,				
				region : '@{controlBorderRegion}',
				margin : '@{controlMargin}',
				xtype : 'container',			
				layout : {
					type : 'vbox',
					align : 'stretch'
				},
				items : [{
					//Control
					xtype : 'fieldset',
					height : 170,
					padding : '5 10',			
					title : '~~parserControl~~',
					layout : {
						type : 'vbox',
						align : 'stretch'
					},
					items : [{
						xtype : 'component',
						html : '~~invalidSelectionText~~',
						hidden : '@{!selectionIsInvalid}'
					},{
						xtype: 'toolbar',
						hidden : '@{selectionIsInvalid}',
						cls : 'rs-dockedtoolbar',
						defaults: {
							cls: 'rs-small-btn rs-btn-dark'					
						},
						items: ['removeMarkup','captureMarkup']
					},{
						xtype : '@{patternProcessor}',
						flex : 1,
						hidden : '@{..selectionIsInvalid}'					
					}]
				},{
					//Boundary
					xtype : 'fieldset',
					margin : 0,	
					flex : 1,		
					padding : '5 10 10 10',		
					title: '~~boundaryTitle~~',			
					layout: {
						type:'vbox',
						align: 'stretch'
					},				
					items: [{
						xtype: 'button',
						name: 'updateBoundary',
						maxWidth : 80,
						cls: 'rs-small-btn rs-btn-dark'
					},{
						xtype : 'fieldcontainer',
						fieldLabel: '~~fromField~~',				
						labelWidth : 100,
						defaults : {
							margin : '1 0'
						},
						layout : {
							type : 'vbox',
							align : 'stretch'
						},
						items: [{
							xtype : 'radiofield',					
							boxLabel: '~~beginningOutputLabel~~',
							value: '@{startOutput}',					
							handler: '@{startOutputIsSelected}'
						},{
							xtype : 'container',					
							layout : 'hbox',					
							items : [{
								xtype : 'radiofield',
								boxLabel: '~~firstOccurrenceLabel~~',
								value: '@{!startOutput}',					
							},{
								xtype: 'textfield',
								name: 'parseStart',						
								hideLabel : true,
								margin : '0 0 0 10',
								flex : 1
							}]
						},{
							xtype: 'checkboxfield',					
							boxLabel: '~~inclusive~~',
							value: '@{startTextIncluded}'
						}]
					},{
						xtype : 'fieldcontainer',
						fieldLabel: '~~toField~~',
						labelWidth : 100,
						defaults : {
							margin : '1 0'
						},
						layout : {
							type : 'vbox',
							align : 'stretch'
						},
						items: [{
							xtype : 'radiofield',					
							boxLabel: '~~endOutputLabel~~',
							value: '@{endOutput}',			
							handler: '@{endOutputIsSelected}'
						},{
							xtype : 'container',					
							layout : 'hbox',
							items : [{
								xtype : 'radiofield',
								boxLabel: '~~firstOccurrenceLabel~~',
								value: '@{!endOutput}',					
							},{
								xtype: 'textfield',
								name: 'parseEnd',						
								hideLabel : true,
								margin : '0 0 0 10',
								flex : 1
							}]
						},{
							xtype: 'checkboxfield',					
							boxLabel: '~~inclusive~~',
							value: '@{endTextIncluded}'
						}]
					},{
						xtype: 'checkboxfield',
						value: '@{block}',
						hideLabel : true,
						margin : '5 0',	
						boxLabel: '~~repeatedBlock~~'				
					}]
				}],
				listeners : {
					afterrender : function(panel){
						panel._vm.on('embeddedLayout', function(isEmbedded){								
							if(isEmbedded){
								panel.setBorderRegion('south');							
							}
							else {
								panel.setBorderRegion('east');							
							}
						});
					}
				}
			}]	
		},{
			xtype: 'component',		
			html: '~~variableTable~~',
			margin : '15 0 8 0',	
		},{
			xtype: 'component',		
			hidden: '@{!variableStoreIsEmpty}',
			html: '~~noVariableText~~'
		},{
			xtype: 'grid',		
			cls: 'rs-grid-dark',						
			store: '@{variableStore}',
			columns: '@{variableColumn}',
			hidden: '@{variableStoreIsEmpty}',
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}],
			selModel: {
				selType: 'cellmodel'
			},
			listeners: {					
				removeVariable: '@{removeVariable}',
				validateedit: function(editor, context){
					if(context.field == "variable"){						
						var store = context.store;
						var newVal = context.value;
						var currentRowIdx = context.rowIdx;
						if(store.findExact(context.field, newVal) != currentRowIdx && store.findExact(context.field, newVal) != -1){
							this._vm.displayDuplicateNameMsg();
							return false;
						}
						else if(!RS.common.validation.VariableName.test(newVal)){
							this._vm.displayInvalidCharacter();
							return false;
						}
					}
				},
				edit: '@{updateVariable}'				
			}
		}]
	},{
		xtype : '@{codeScreen}'		
	}]
})
