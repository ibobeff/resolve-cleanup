glu.defView('RS.actiontaskbuilder.RolesEdit', {
	parentLayout: 'quickAccess',
	title: '@{title}',	
	cls: 'block-model subEditModel',
	hidden: '@{hidden}',
	header: '@{header}',
	bodyPadding: '10 15 15 15',
	margin: '0 0 2.5 0',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});	

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});

			panel.getEl().on('click', function() {
				if (panel._vm.collapsed && !panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	},
	
	dockedItems: [{
		xtype: 'toolbar',		
		padding : '0 15 10',
		dock: 'bottom',		
		items: ['->', { 
			name: 'close',
			cls: 'rs-med-btn rs-btn-dark',			
			disabled: '@{!isButtonEnabled}'					
		}]
	}],

	items: [{
		xtype: 'checkbox',
		hideLabel: true,
		disabled: '@{savingNewTask}',
		name: 'defaultRoles',
		boxLabel: '~~defaultRolesText~~',
		listeners: {
			change: '@{defaultRolesModified}'
		}
	}, {
		xtype: 'displayfield',
		hidden: '@{!gridIsEmpty}',
		value: '~~rolesEmptyText~~',
		margin: 0,
		padding: 0
	}, {
		xtype: 'grid',
		cls : 'roles-grid rs-grid-dark',
		hidden: '@{gridIsEmpty}',
		disabled: '@{defaultRolesOrSavingNewTask}',
		disabledCls: 'default-roles-enabled',
		itemId: 'rolesGrid',
		store: '@{accessStore}',
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		selModel: {
			selType: 'cellmodel'
		},
		viewConfig: {
			markDirty: false
		},
		listeners: {
			beforeedit: '@{updateAllowedRolesStore}'
		},
		columns: [{
			dataIndex: 'uname',
			flex: 5,
			header: '~~roleName~~',
			resizable: false,
			hideable: false,
			editor: {
				xtype: 'combo',
				cls: 'no-input-border combo',
				forceSelection: true,
				editable: false,
				store: '@{allowedRolesStoreForGridRow}',
				queryMode: 'local',
				displayField: 'text',
				valueField: 'value',
				width: '80%'		
			},
			renderer: function(value, meta, record) {
				if (record.get('uname') == 'admin') {
					meta.tdCls = 'x-item-disabled'; 
				}
				return value;
			}
		}, {
			xtype: 'checkcolumn',
			dataIndex: 'read',
			flex: 1,
			header: '~~roleReadRight~~',
			resizable: false,
			hideable: false,
			renderer: function(value, meta, record) {
				if (record.get('admin') || record.get('write')) {
					meta.tdCls = 'x-item-disabled';
					if (!record.get('read')) {
						record.data.read = true;
					}
				}
				if (record.get('read')) {
					return '<img class="x-grid-checkcolumn x-grid-checkcolumn-checked" src="/resolve/images/s.gif">';
				} else {
					return '<img class="x-grid-checkcolumn" src="/resolve/images/s.gif">';
				}
			}
		}, {
			xtype: 'checkcolumn',
			dataIndex: 'write',
			flex: 1,
			header: '~~roleWriteRight~~',
			resizable: false,
			hideable: false,
			renderer: function(value, meta, record) {
				if (record.get('admin')) {
					meta.tdCls = 'x-item-disabled';
					if (!record.get('write')) {
						record.data.write = true;
					}
				}
				if (record.get('write')) {
					return '<img class="x-grid-checkcolumn x-grid-checkcolumn-checked" src="/resolve/images/s.gif">';
				} else {
					return '<img class="x-grid-checkcolumn" src="/resolve/images/s.gif">';
				}
			}
		}, {
			xtype: 'checkcolumn',
			dataIndex: 'execute',
			flex: 1,
			header: '~~roleExecuteRight~~',
			resizable: false,
			hideable: false,
			renderer: function(value, meta, record) {
				if (record.get('admin')) {
					meta.tdCls = 'x-item-disabled';
					if (!record.get('execute')) {
						record.data.execute = true;
					}
				}
				if (record.get('execute')) {
					return '<img class="x-grid-checkcolumn x-grid-checkcolumn-checked" src="/resolve/images/s.gif">';
				} else {
					return '<img class="x-grid-checkcolumn" src="/resolve/images/s.gif">';
				}
			}
		}, {
			xtype: 'checkcolumn',
			dataIndex: 'admin',
			flex: 1,
			header: '~~roleAdminRight~~',
			resizable: false,
			hideable: false,
			renderer: function(value, meta, record) {
				if (record.get('uname') == 'admin' || !clientVM.isAdmin) {
					meta.tdCls = 'x-item-disabled'; 
				}
				if (record.get('admin')) {
					return '<img class="x-grid-checkcolumn x-grid-checkcolumn-checked" src="/resolve/images/s.gif">';
				} else {
					return '<img class="x-grid-checkcolumn" src="/resolve/images/s.gif">';
				}
			},
			listeners: {
				afterrender: function (column) {
					if (this.up('grid')._vm.hideAdminColumn) {
						column.disable();
					} else {
						column.enable();
					}
				}
			}
		}, {
			xtype: 'actioncolumn',		
			width : 45,
			resizable: false,
			hideable: false,
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record) {
					record.store.remove(record);
					view.ownerCt._vm.addRoleToDetail(record);
				}
			}],
			renderer: function(value, meta, record) {
				if (record.get('uname') == 'admin') {
					meta.tdCls = 'x-item-disabled'; 
				}
			}
		}]
	}, {
		xtype: '@{detail}',
		hidden: '@{..hideAddRoleForm}'
	}]
});