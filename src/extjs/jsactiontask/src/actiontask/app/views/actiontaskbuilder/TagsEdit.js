glu.defView('RS.actiontaskbuilder.TagsEdit', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: 'block-model',
	header: '@{header}',
	hidden: '@{hidden}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: 15,

	dockedItems: [{
		xtype: 'toolbar',		
		padding : '0 15 10',
		dock: 'bottom',
		items: ['->', {			
			name: 'close',
			cls: 'rs-med-btn rs-btn-dark'
		}]
	}],

	items: [{
		xtype: 'displayfield',
		hidden: '@{!gridIsEmpty}',
		value: '@{noTagsMessage}'
	}, {
		xtype: 'grid',
		store: '@{store}',
		cls : 'rs-grid-dark',
		name: 'tags',
		hidden: '@{gridIsEmpty}',
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		selModel: {
			selType: 'cellmodel'
		},
		listeners: {
			scope: this,
			afterrender: function (grid) {
				grid.fireEvent('gridrendercompleted', grid, grid);
			},
			gridrendercompleted: '@{gridRenderCompleted}',
			recordremoved: '@{recordRemoved}',
			recordadded: '@{recordAdded}'
		},
		columns: [{
			header: '~~uname~~',
			dataIndex: 'name',
			filterable: true,
			flex: 2,
			hideable: false,
			sortable: true,
			editor: {
				xtype: 'combobox',
				queryMode: 'local',
				displayField: 'text',
				valueField: 'value',
				store: '@{allowedTags}',
				editable: false,
				listeners: {
					select: function (combo, records) {
						this.fireEvent('tagitemchange', this, combo, records);
					},
					tagitemchange: '@{tagItemChange}'
				}
			}
		}, {
			header: '~~udescription~~',
			dataIndex: 'description',
			filterable: false,
			flex: 4,
			sortable: true
		}, {
			xtype: 'actioncolumn',		
			hideable: false,
			sortable: true,
			align: 'center',			
			width: 45,			
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record ) {
					this.up('grid').fireEvent('recordremoved', view, record);
				}
			}]
		}]
	}, {
		xtype: 'fieldset',
		hidden: '@{itemsExhausted}',
		title : '~~addTag~~',
		margin: '5 0 0 0',
		padding: 10,
		items: [{
			xtype: 'form',

			listeners: {
				afterrender: function (form) {
					form.fireEvent('registerform', this, form);
				},
				registerform: '@{registerForm}'
			},

			layout: {
				type: 'vbox',
				align: 'stretch'
			},

			defaults: {
				labelWidth: 130,
				margin: '0 0 5 0'		
			},

			items: [{
				xtype: 'combobox',
				name: 'name',
				emptyText: '~~selectTag~~',
				allowBlank: false,
				store: '@{allowedTags}',
				queryMode: 'local',
				displayField: 'text',
				valueField: 'value',
				cls: 'no-input-border combo',
				maxWidth: 500,
				editable: true,				
				listeners: {
					select: '@{tagSelected}',
					change: '@{validateDescription}'
				}
			}, {
				xtype: 'displayfield',
				name: 'description',
			}, {
				xtype: 'button',
				name: 'recordAdded',
				text: '~~addTag~~',
				cls: 'rs-small-btn rs-btn-dark',			
				margin: '0 0 0 135',
				maxWidth: 90
			}]
		}]
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	}	
});
