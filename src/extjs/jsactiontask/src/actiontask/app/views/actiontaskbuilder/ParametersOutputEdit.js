glu.defView('RS.actiontaskbuilder.ParametersOutputEdit', {
	parentLayout: 'quickAccess',
	title: '@{title}',		
	cls: 'block-model subEditModel',
 	hidden: '@{hidden}',
 	header: '@{header}',
	bodyPadding: 15,
	margin: '0 0 2.5 0',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	items: [{
		xtype: 'displayfield',
		value: '~~outputParametersEmptyText~~',
		hidden: '@{hasOutputParameters}'
	},{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		hidden: '@{!hasOutputParameters}',
		store: '@{outputParametersStore}',
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		selModel: {
			selType: 'cellmodel'
		},
		columns: [{
			header: '~~name~~',
			dataIndex: 'uname',
			editor: {
				xtype: 'textfield',
				allowBlank: false,
				regex: RS.common.validation.VariableName,
				regexText: '~~validKeyCharacters~~'				
			},
			hideable: false,
			flex: 2,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~description~~',
			dataIndex: 'udescription',
			editor: {
				xtype: 'textarea',
				grow: true
			},
			hideable: false,
			flex: 4,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~defaultValue~~',
			dataIndex: 'udefaultValue',
			editor: {
				xtype: 'textarea',
				grow: true
			},
			hideable: false,
			flex: 4,
			renderer: RS.common.grid.plainRenderer()
		}, {
			xtype: 'checkcolumn',
			align: 'center',
			header: '~~encrypt~~',
			dataIndex: 'uencrypt',
			filterable: false,
			groupable: false,
			flex: 1
		}, {
			xtype: 'checkcolumn',
			align: 'center',
			header: '~~required~~',
			dataIndex: 'urequired',
			groupable: false,
			flex: 1
		}, {
			xtype: 'actioncolumn',			
			hideable: false,
			sortable: false,
			align: 'center',		
			width: 45,			
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record) {
					view.ownerCt._vm.removeParameter(record);	
				}
			}]
		}],
		listeners : {
			afterrender : function(grid){
				this._vm.on('refreshGrid', function(){
					grid.getView().refresh();
				});
			},
			validateedit : function(editor, context){
				if(context.field == "uname"){
					//Custom Flatstore
					var store = context.store;
					var newVal = context.value;
					var currentRowIdx = context.rowIdx;
					if(this._vm.validateParserOutput(context.record)){
						if(store.findExact(context.field, newVal) != currentRowIdx && store.findExact(context.field, newVal) != -1){
							this._vm.displayDuplicateNameMsg();
							return false;
						}
					}
					else
						return false;
				}
			},
			edit : function(editor, context){
				if(context.field == "uname"){
					if (this._vm.parentVM.embed && this._vm.rootVM.automation.activeItem && this._vm.rootVM.automation.activeItem.activeItem) {
						var outputParamName = context.originalValue;
						this._vm.rootVM.automation.activeItem.activeItem.checkTargetParamDependency(outputParamName, function() {}, function() {
							context.record.reject();
						}.bind(this));
					}
				}
			}
		}
	}, {
		xtype: 'fieldset',
		title : '~~createOutputTitle~~',
		margin: '10 0 0 0',
		padding: '5 15 10 15',
		items: [{
			xtype: '@{detail}'
		}]
	}],
	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});	

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});

			panel.getEl().on('click', function() {
				if (panel._vm.collapsed && !panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	},

	dockedItems: [{
		xtype: 'toolbar',
		dock: 'bottom',		
		padding : '0 15 10 15',
		items: ['->', { 
			name: 'close', 
			cls: 'rs-med-btn rs-btn-dark',
			disabledCls: 'link-button-disabled'		
		}]
	}]
});

