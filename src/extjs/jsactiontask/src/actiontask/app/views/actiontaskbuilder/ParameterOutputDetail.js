glu.defView('RS.actiontaskbuilder.ParameterOutputDetail', {
	xtype: 'form',	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults : {
		margin : '4 0'
	},
	items: [{
		xtype: 'textfield',
		labelWidth: 130,
		allowBlank: false,
		validateOnBlur: true,
		name: 'uname',
		regex: RS.common.validation.VariableName,
		regexText: '~~validKeyCharacters~~'		
	}, {
		xtype: 'container',
		flex: 1,	
		layout: {
			type: 'hbox',
			align: 'stretchmax'
		},

		items: [{
			xtype: 'displayfield',
			width: 130,
			margin: '0 5 0 0',
			value: '~~parameterDescription~~'
		}, {
			xtype: 'container',
			flex: 1,			
			cls: 'reverse-handle-borders',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},			
			resizable: {
				handles: 's',
				pinned: true
			},
			items: [{
				xtype: 'textarea',
				name: 'udescription',
				rows: 3,
				flex: 1,
				fieldLabel: null,
				labelWidth: 0,
				margin: 0,
				padding: 0
			}]
		}]
	}, {
		xtype: 'container',
		flex: 1,		
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'displayfield',
			width: 130,
			margin: 0,
			value: '~~labelDefaultValue~~'
		}, {
			xtype: 'container',
			flex: 1,			
			cls: 'reverse-handle-borders',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},			
			resizable: {
				handles: 's',
				pinned: true
			},
			items: [{
				xtype: 'textarea',
				name: 'udefaultValue',
				rows: 3,
				flex: 1,
				fieldLabel: null,
				labelWidth: 0,
				margin: '0 0 0 5',
				padding: 0
			}]
		}]
	}, {
		xtype: 'checkbox',
		margin: '4 0 0 135',
		cls: 'no-margin',
		name: 'uencrypt',
		hideLabel: true,
		boxLabel: '~~encrypt~~'
	}, {
		xtype: 'checkbox',
		margin: '4 0 0 135',		
		name: 'urequired',
		hideLabel: true,
		boxLabel: '~~required~~'
	},{
		xtype: 'button',
		cls: 'rs-small-btn rs-btn-dark',
		margin: '8 0 0 135',
		maxWidth : 90,
		name: 'addOutput',
		text: '@{addOutputTitle}',
		formBind: true,
		listeners: {
			click: function (view) {
				setTimeout(function (vm) {
					this.reset();
				}.bind(this.up('form').getForm(), view._vm), 50);
			}
		}
	}]	
});