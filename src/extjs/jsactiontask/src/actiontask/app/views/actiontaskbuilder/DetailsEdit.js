glu.defView('RS.actiontaskbuilder.DetailsEdit', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: 'block-model subEditModel',
	hidden: '@{hidden}',
	header: '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: 15,
	margin: '0 0 2.5 0',

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		margin: 0,
		padding: 0
	},
	
	dockedItems: [{
		xtype: 'toolbar',		
		padding : '0 15 10',
		dock: 'bottom',
		items: ['->', {			
			name: 'close',
			cls: 'rs-med-btn rs-btn-dark'
		}]
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});	

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});

			panel.getEl().on('click', function() {
				if (panel._vm.collapsed && !panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	},
	items : [{
		xtype : 'component',
		html : '~~noDetailDefined~~',
		hidden : '@{!noRule}',
		margin: '10 0'
	},{
		xtype : 'component',
		html : '~~detailDisplayRule~~',
		margin : '0 0 10 0',
		hidden : '@{noRule}'
	},{
		xtype : 'grid',
		hidden : '@{noRule}',
		store : '@{ruleStore}',
		cls : 'rs-grid-dark summary-detail-grid',
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		columns : [{
			header : '~~severity~~',
			dataIndex : 'severity',
			align : 'center',
			width : 150,
			editor: {
				xtype: 'combo',				
				editable: false,			
				queryMode: 'local',
				displayField: 'name',
				valueField: 'name',
				store: '@{severityStore}',
				cls: 'no-input-border combo'				
			},
			renderer : function(value, metaData){			
				metaData.tdAttr = 'status=' + value.toUpperCase(); 
				return value;               	
			}
		},{
			header : '~~condition~~',
			dataIndex : 'condition',
			align : 'center',
			width : 150,
			editor: {
				xtype: 'combo',				
				editable: false,			
				queryMode: 'local',
				displayField: 'name',
				valueField: 'name',
				store: '@{conditionStore}',
				cls: 'no-input-border combo'				
			},
			renderer : function(value, metaData){			
				metaData.tdAttr = 'status=' + value.toUpperCase();
				return value;     
			}
		},{
			header : '~~textToDisplay~~',
			dataIndex : 'display',
			editor : 'textarea',
			flex : 1,
			renderer : RS.common.grid.plainRenderer()
		},{
			xtype: 'actioncolumn',		
			hideable: false,
			align: 'center',		
			width: 45,			
			resizable : false,
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record) {
					this.up('grid').fireEvent('removeRule', view, record);
				}
			}]
		}],
		listeners : {
			removeRule : '@{removeRule}'
		}
	},{		
		xtype : 'infoicon',
		displayText : '~~multipleRuleMessage~~',
		hidden: '@{!multipleRule}',
		margin: '8 0 0 0'
	},{
		xtype : 'fieldset',
		title: '~~detailRule~~',
		margin: '20 0 0 0',
		padding: '5 15',	
		layout: {
			type : 'hbox',
			align : 'stretch'
		},		
		items : [{
			xtype : 'container',
			flex : 1,
			layout: {
				type : 'vbox',
				align : 'stretch'
			},
			defaults : {
				labelWidth: 130,			
				margin: '4 0',
				editable : false
			},
			items : [{
				xtype : 'combobox',
				store : '@{severityStore}',
				displayField : 'name',
				valueField : 'name',
				name : 'severity'
			},{
				xtype : 'combobox',
				store : '@{conditionStore}',
				displayField : 'name',
				valueField : 'name',
				name : 'condition'
			},{
				xtype : 'textarea',
				height : 250,
				fieldLabel : '~~textToDisplay~~',
				name : 'display',
				editable : true
			},{
				xtype: 'container',			
				margin: '4 0 4 135',
				layout: {
					type: 'hbox'
				},
				defaults : {
					margin: '0 4 0 0',
					hideLabel : true
				},
				items: [{
					xtype: 'combobox',
					name: 'fieldType',
					emptyText: '~~selectType~~',
					editable: false,
					queryMode: 'local',
					displayField: 'text',
					valueField: 'value',	
					store: '@{typesStore}',
					width : 150
				}, {
					xtype: 'combobox',		
					name: 'fieldName',
					emptyText: '~~fieldNameTextForOthers~~',
					editable: '@{isFieldNameEditable}',
					queryMode: 'local',
					displayField: 'name',
					valueField: 'name',
					store: '@{parametersStore}',			
					flex: 1,
					listeners : {
						focus : function(){
							this.expand();
						}
					}				
				}, {
					xtype: 'button',
					cls : 'rs-small-btn rs-btn-dark',
					height : '100%',
					name :  'insertVariable',
					text : '~~insertVariable~~',
					margin : 0
				}]
			},{
				xtype : 'button',
				name : 'addRule',			
				maxWidth: 140,		
				cls : 'rs-small-btn rs-btn-dark',
				margin: '4 0 4 135'
			}]
		},{
			xtype : 'container',
			hidden : '@{..embed}',	
			flex : 1,
			margin : '44 0 0 50',
			layout : {
				type : 'vbox',
				align : 'stretch'
			},
			defaults : {
				margin : '4 0'
			},
			items : [{
				xtype : 'component',
				html : '~~previewLabel~~'
			},{
				xtype : 'component',			
				html : '@{preview}',
				overflowY : 'auto',
				height : 250,
				autoScroll : true,
				cls : 'summary-detail-preview',
				listeners : {
					afterrender : function(component){
						this._vm.on('renderPreview', function(preview){
							component.update(preview);
						})
					}										
				}			
			},{
				xtype : 'checkbox',
				name : 'htmlEncode',
				hideLabel : true,
				boxLabel : '~~htmlEncodeDetail~~'
			}]
		}]
	}]
});


