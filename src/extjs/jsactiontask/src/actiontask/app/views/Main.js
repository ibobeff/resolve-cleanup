glu.defView('RS.actiontask.Main', {
    padding: 15,
    layout: 'border',
    plugins: [{
        ptype: 'searchfilter',
        allowPersistFilter: false,
        hideMenu: true
    }],

    items: [{
        xtype: 'panel',
        layout: 'card',
        region: 'west',
        width: 300,
        split: true,
        maxWidth: 450,
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            cls: 'actionBar rs-dockedtoolbar',
			defaultButtonUI: 'display-toolbar-button',		
            name: 'actionBar',
            items: ['menuPath', 'module']
        }],
        activeItem: '@{activeItem}',
        items: [{
            xtype: 'treepanel',
            id: 'menuPathTreePanel',
            cls : 'rs-grid-dark',
            viewConfig: {
                markDirty: false
            },
            selected: '@{menuPathSelected}',
            dockedItems: [{
                xtype: 'toolbar',
                style: '!important;padding:0px 0px 2px 0px !important'
            }],
            animate: false,
            columns: '@{actionTaskTreeMenuPathColumns}',
            name: 'actionTaskTreeMenuStore',
            listeners: {
                selectionchange: '@{menuPathTreeSelectionchange}'
            }
        }, {
            xtype: 'treepanel',
            id: 'moduleTreePanel',
            cls : 'rs-grid-dark',
            viewConfig: {
                markDirty: false
            },
            dockedItems: [{
                xtype: 'toolbar',
                style: '!important;padding:0px 0px 2px 0px !important'
            }],
            animate: false,
            columns: '@{actionTaskTreeModuleColumns}',
            name: 'actionTaskTreeModuleStore',

            listeners: {
                selectionchange: '@{moduleTreeSelectionchange}'
            }
        }]
    }, {
        region: 'center',
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            cls: 'actionBar rs-dockedtoolbar',
            name: 'actionBar',
            defaults : {
                cls : 'rs-small-btn rs-btn-light'
            },
            items: ['createActionTask','deleteActionTasks', 'rename', 'copy', {
                text: '~~index~~',
                menu: {
                    xtype: 'menu',
                    items: ['index', 'indexAll']
                }
            }, {
                xtype: 'button',
                iconCls: 'rs-social-button icon-play',
                tooltip: '~~executionTooltip~~',
                disabled: '@{!executionIsEnabled}',
                name: 'execution',
                paddings: {
                    right: 10
                },
                text: '',
                handler: function(button) {
                    button.fireEvent('execution', button, button)
                },
                listeners: {
                    execution: '@{execution}'
                }
            }]
        }],
        xtype: 'grid',
        cls : 'rs-grid-dark',
        plugins: [{
            ptype: 'pager'
        }, {
            init: function(grid) {
                grid.dockedItems.each(function(item) {
                    if (item.name == 'actionBar')
                        this.toolbar = item;
                }, this);
                if (!this.toolbar)
                    return;
                target = null;
                var remove = [];
                this.toolbar.items.each(function(item, idx) {
                    if (item.name == 'execution') {
                        target = item;
                        remove.push(target);
                    }

                })
                this.toolbar.insert(this.toolbar.items.items.length - 2, target.initialConfig);
            }
        }],
        displayName: '~~actionTaskDisplayName~~',
        name: 'actionTasks',
        store: '@{actionTaskGridStore}',
        columns: '@{columns}',
        autoScroll: true,

        viewConfig: {
            enableTextSelection: true
        },
        selModel: {
            selType: 'resolvecheckboxmodel',
            //columnTooltip: RS.common.locale.editColumnTooltip,
			columnTooltip: RS.common.locale.gotoDetailsColumnTooltip,
            columnTarget: '_self',
            columnEventName: 'editAction',
            columnIdField: 'id'
        },
        listeners: {
            editAction: '@{editDefinition}',
            selectAllAcrossPages: '@{selectAllAcrossPages}',
            selectionChange: '@{selectionChanged}'
        }
    }]
});