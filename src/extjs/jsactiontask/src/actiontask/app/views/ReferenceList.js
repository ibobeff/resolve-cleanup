glu.defView('RS.actiontask.ReferenceList', {
	xtype: 'panel',
	autoScroll: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10px',
	items: [{
		xtype: 'panel',
		html: '~~referenceWarning~~'
	}, {
		region: 'center',
		tbar: [],
		xtype: 'grid',
		padding: '10px 0px 0px 0px',
		plugins: [{
			ptype: 'pager'
		}],
		flex: 1,
		name: 'reference',
		store: '@{store}',
		columns: [{
			header: '~~name~~',
			dataIndex: 'name',
			filterable: false,
			flex: 1
		}],
		minHeight: 200,
		listeners: {
			selectionchange: '@{selectionChanged}'
		}
	}, {
		xtype: 'panel',
		html: '~~stillDelete~~'
	}],

	asWindow: {
		title: '~~reference~~',
		width: 460,
		height: 400,
		modal: true
	},

	buttonAlign: 'left',
	buttons: ['gotoReference', 'deleteRecords', 'close']
});