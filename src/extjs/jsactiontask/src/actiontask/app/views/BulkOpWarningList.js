glu.defView('RS.actiontask.BulkOpWarningList', {
	title: '~~bulkWarningTitle~~',
	cls : 'rs-modal-popup',
	width: 550,
	height: 300,
	modal: true,
	autoScroll: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	padding: 15,
	items: [{
			xtype: 'component',
			html: '@{bulkWarning}'
		}, {
			xtype: 'component',
			html: '@{html}'
		}		
	],
	buttonAlign: '@{buttonAlign}',
	buttons: [{
		name :'proceedBulk',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'closeBulk',
		text : '~~close~~',
		cls : 'rs-med-btn rs-btn-light'
	},{
		name : 'close',
		cls : 'rs-med-btn rs-btn-dark'
	}]
});