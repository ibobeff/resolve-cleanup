
glu.defModel('RS.actiontaskbuilder.model.General', {
	fields: [{
		name: 'id',
		type: 'string'
	}, {
		name: 'uname',
		type: 'string'
	}, {
		name: 'unamespace',
		type: 'string'
	}, {
		name: 'ufullName',
		type: 'string'
	}, {
		name: 'umenuPath',
		type: 'string'
	}, {
		name: 'usummary',
		type: 'string'
	}, {
		name: 'udescription',
		type: 'string'
	}, {
		name: 'uactive',
		type: 'bool',
		defaultValue: true
	}, {
		name: 'ulogresult',
		type: 'bool',
		defaultValue: true
	}, {
		name: 'uisHidden',
		type: 'bool',
		defaultValue: false
	}, {
		name: 'uisDefaultRole',
		type: 'bool',
		defaultValue: true
	}]
});

glu.defModel('RS.actiontaskbuilder.model.AssignedTo', {
	fields: [{
		name: 'id',
		type: 'string'
	}, {
		name: 'udisplayName',
		type: 'string'
	}]
});

glu.defModel('RS.actiontaskbuilder.model.AccessRights', {
	fields: [{
		name: 'sys_id',
		type: 'string'
	}, {
		name: 'uadminAccess',
		type: 'string'
	}, {
		name: 'uexecuteAccess',
		type: 'string'
	}, {
		name: 'ureadAccess',
		type: 'string'
	}, {
		name: 'uwriteAccess',
		type: 'string'
	}]
});

glu.defModel('RS.actiontaskbuilder.model.resolveActionInvoc', {
	fields: [{
		name: 'id',
		type: 'string'
	}, {
		name: 'uargs',
		type: 'string'
	}, {
		name: 'ucommand',
		type: 'string'
	}, {
		name: 'utimeout',
		type: 'string',
		defaultValue: '300'
	}, {
		name: 'utype',
		type: 'string',
		defaultValue: 'assess'
	}]
});

glu.defModel('RS.actiontaskbuilder.model.Assess', {
	fields: [{
		name: 'id',
		type: 'string'
	} ,{
		name: 'udescription',
		type: 'string'
	}, {
		name: 'uname',
		type: 'string'
	}, {
		name: 'uonlyCompleted',
		type: 'bool',
		defaultValue: true
	}, {
		name: 'uparse',
		type: 'string'
	}, {
		name: 'uscript',
		type: 'string'
	}, {
		name: 'detailFormat',
		type: 'string'
	}, {
		name: 'summaryFormat',
		type: 'string'
	}]
});

glu.defModel('RS.actiontaskbuilder.model.Parser', {
	fields: [{
		name: 'id',
		type: 'string'
	}, {
		name: 'uconfiguration',
		type: 'string'
	}, {
		name: 'uname',
		type: 'string'
	}, {
		name: 'uscript',
		type: 'string'
	}]
});

glu.defModel('RS.actiontaskbuilder.model.Preprocess', {
	fields: [{
		name: 'id',
		type: 'string'
	}, {
		name: 'udescription',
		type: 'string'
	}, {
		name: 'uname',
		type: 'string'
	}, {
		name: 'uscript',
		type: 'string'
	}]
});

