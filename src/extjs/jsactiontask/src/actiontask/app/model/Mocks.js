Ext.define('RS.actiontask.model.Mocks', {
	extend: 'Ext.data.Model',
	idProperty: 'name',
	fields: [
		{
			name: 'id',
			type: 'string'
		}, {
			name: 'uname',
			type: 'string'
		}, {
			name: 'udescription',
			type: 'string'
		}, {
			name: 'udata',
			type: 'string'
		}, {
			name: 'uparams',
			type: 'string'
		}, {
			name: 'uinputs',
			type: 'string'
		}, {
			name: 'uflows',
			type: 'string'
		}, {
			name: 'sysUpdatedBy',
			type: 'string'
		}, {
			name: 'sysUpdatedOn',
			type: 'string'
		}, {
			name: 'sysCreatedBy',
			type: 'string'
		}, {
			name: 'sysCreatedOn',
			type: 'string'
		}
	]
});
Ext.define('RS.actiontask.model.MockDetails', {
	extend: 'Ext.data.Model',
	idProperty: 'name',
	fields: [
		{
			name: 'type',
			type: 'string'
		}, {
			name: 'name',
			type: 'string'
		}, {
			name: 'value',
			type: 'string'
		}
	]
});
