Ext.define('RS.actiontask.model.ValueToOutputs', {
	extend: 'Ext.data.Model',
	idProperty: 'id',
	fields: [
		{
			name: 'id',
			type: 'string'
		}, {
			name: 'outputType',
			type: 'string'
		}, {
			type: 'string',
			name: 'outputName'
		}, {
			name: 'sourceType',
			type: 'string'
		}, {
			name: 'sourceName',
			type: 'string'
		}, {
			name: 'script',
			type: 'string'
		}, {
			name: 'dummy',
			type: 'boolean'
		}, {
			name: 'editable',
			type: 'boolean',
			defaultValue: false
		}, {
			name: 'isPromoted',
			type: 'boolean',
			defaultValue: false
		}
	]
});
