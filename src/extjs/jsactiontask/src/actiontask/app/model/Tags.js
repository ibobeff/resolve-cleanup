Ext.define('RS.actiontask.model.Tags', {
	extend: 'Ext.data.Model',
	idProperty: 'name',
	fields: [
		{
			name: 'id',
			type: 'string'
		}, {
			name: 'name',
			type: 'string'
		}, {
			name: 'description',
			type: 'string'
		}, {
			name: 'dummy',
			type: 'bool'
		}
	]
});
