Ext.define('RS.actiontask.model.Parameters', {
	extend: 'Ext.data.Model',
	idProperty: 'id',
	fields: [
		{
			name: 'id',
			type: 'string'
		}, {
			name: 'uname',
			type: 'string'
		}, {
			name: 'udescription',
			type: 'string'
		}, {
			name: 'udefaultValue',
			type: 'string'
		}, {
			name: 'uencrypt',
			type: 'boolean'
		}, {
			name: 'urequired',
			type: 'boolean'
		}, {
			name: 'uprotected',
			type: 'boolean'
		}, {
			name: 'uorder',
			type: 'int'
		}, {
			name: 'ugroupName',
			type: 'string'
		}, {
			name: 'utype',
			type: 'string'
		}
	]
});
