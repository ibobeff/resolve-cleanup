<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.resolve.util.JspUtils" %>
<%@page import="org.owasp.esapi.ESAPI"%>
<%
    String ver = ESAPI.encoder().encodeForURL(JspUtils.getResolveVersion());

    boolean debug = false;
    String d = request.getParameter("debug");
    if( d != null && d.indexOf("t") > -1){
        debug = true;
    }
    
    boolean debugActiontask = false;
    String dT = request.getParameter("debugActiontask");
    if( dT != null && dT.indexOf("t") > -1){
        debugActiontask = true;
    }

    boolean mock = false;
    String m = request.getParameter("mock");
    if( m != null && m.indexOf("t") > -1){
        mock = true;
    }
%>

<%
    if( debug || debugActiontask){
%>
<link rel="stylesheet" type="text/css" href="/resolve/actiontask/css/actiontask-all.css?_v=<%=ver%>"/>
<script type="text/javascript" src="/resolve/actiontask/js/actiontask-classes.js?_v=<%=ver%>"></script>
<%
    }else{
%>
<link rel="stylesheet" type="text/css" href="/resolve/actiontask/css/actiontask-all.css?_v=<%=ver%>"/>
<script type="text/javascript" src="/resolve/actiontask/js/actiontask-classes.js?_v=<%=ver%>"></script>
<%
    }
%>

