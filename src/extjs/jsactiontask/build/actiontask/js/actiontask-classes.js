/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/

glu.defModel('RS.actiontaskbuilder.model.General', {
	fields: [{
		name: 'id',
		type: 'string'
	}, {
		name: 'uname',
		type: 'string'
	}, {
		name: 'unamespace',
		type: 'string'
	}, {
		name: 'ufullName',
		type: 'string'
	}, {
		name: 'umenuPath',
		type: 'string'
	}, {
		name: 'usummary',
		type: 'string'
	}, {
		name: 'udescription',
		type: 'string'
	}, {
		name: 'uactive',
		type: 'bool',
		defaultValue: true
	}, {
		name: 'ulogresult',
		type: 'bool',
		defaultValue: true
	}, {
		name: 'uisHidden',
		type: 'bool',
		defaultValue: false
	}, {
		name: 'uisDefaultRole',
		type: 'bool',
		defaultValue: true
	}]
});

glu.defModel('RS.actiontaskbuilder.model.AssignedTo', {
	fields: [{
		name: 'id',
		type: 'string'
	}, {
		name: 'udisplayName',
		type: 'string'
	}]
});

glu.defModel('RS.actiontaskbuilder.model.AccessRights', {
	fields: [{
		name: 'sys_id',
		type: 'string'
	}, {
		name: 'uadminAccess',
		type: 'string'
	}, {
		name: 'uexecuteAccess',
		type: 'string'
	}, {
		name: 'ureadAccess',
		type: 'string'
	}, {
		name: 'uwriteAccess',
		type: 'string'
	}]
});

glu.defModel('RS.actiontaskbuilder.model.resolveActionInvoc', {
	fields: [{
		name: 'id',
		type: 'string'
	}, {
		name: 'uargs',
		type: 'string'
	}, {
		name: 'ucommand',
		type: 'string'
	}, {
		name: 'utimeout',
		type: 'string',
		defaultValue: '300'
	}, {
		name: 'utype',
		type: 'string',
		defaultValue: 'assess'
	}]
});

glu.defModel('RS.actiontaskbuilder.model.Assess', {
	fields: [{
		name: 'id',
		type: 'string'
	}, {
		name: 'ucnsName',
		type: 'string'
	}, {
		name: 'udescription',
		type: 'string'
	}, {
		name: 'uname',
		type: 'string'
	}, {
		name: 'uonlyCompleted',
		type: 'bool',
		defaultValue: true
	}, {
		name: 'uparse',
		type: 'string'
	}, {
		name: 'uscript',
		type: 'string'
	}, {
		name: 'detailFormat',
		type: 'string'
	}, {
		name: 'summaryFormat',
		type: 'string'
	}]
});

glu.defModel('RS.actiontaskbuilder.model.Parser', {
	fields: [{
		name: 'id',
		type: 'string'
	}, {
		name: 'uconfiguration',
		type: 'string'
	}, {
		name: 'uname',
		type: 'string'
	}, {
		name: 'uscript',
		type: 'string'
	}]
});

glu.defModel('RS.actiontaskbuilder.model.Preprocess', {
	fields: [{
		name: 'id',
		type: 'string'
	}, {
		name: 'udescription',
		type: 'string'
	}, {
		name: 'uname',
		type: 'string'
	}, {
		name: 'uscript',
		type: 'string'
	}]
});


Ext.define('RS.actiontask.model.Mocks', {
	extend: 'Ext.data.Model',
	idProperty: 'name',
	fields: [
		{
			name: 'id',
			type: 'string'
		}, {
			name: 'uname',
			type: 'string'
		}, {
			name: 'udescription',
			type: 'string'
		}, {
			name: 'udata',
			type: 'string'
		}, {
			name: 'uparams',
			type: 'string'
		}, {
			name: 'uinputs',
			type: 'string'
		}, {
			name: 'uflows',
			type: 'string'
		}, {
			name: 'sysUpdatedBy',
			type: 'string'
		}, {
			name: 'sysUpdatedOn',
			type: 'string'
		}, {
			name: 'sysCreatedBy',
			type: 'string'
		}, {
			name: 'sysCreatedOn',
			type: 'string'
		}
	]
});
Ext.define('RS.actiontask.model.MockDetails', {
	extend: 'Ext.data.Model',
	idProperty: 'name',
	fields: [
		{
			name: 'type',
			type: 'string'
		}, {
			name: 'name',
			type: 'string'
		}, {
			name: 'value',
			type: 'string'
		}
	]
});

Ext.define('RS.actiontask.model.Parameters', {
	extend: 'Ext.data.Model',
	idProperty: 'id',
	fields: [
		{
			name: 'id',
			type: 'string'
		}, {
			name: 'uname',
			type: 'string'
		}, {
			name: 'udescription',
			type: 'string'
		}, {
			name: 'udefaultValue',
			type: 'string'
		}, {
			name: 'uencrypt',
			type: 'boolean'
		}, {
			name: 'urequired',
			type: 'boolean'
		}, {
			name: 'uprotected',
			type: 'boolean'
		}, {
			name: 'uorder',
			type: 'int'
		}, {
			name: 'ugroupName',
			type: 'string'
		}, {
			name: 'utype',
			type: 'string'
		}
	]
});

Ext.define('RS.actiontask.model.Tags', {
	extend: 'Ext.data.Model',
	idProperty: 'name',
	fields: [
		{
			name: 'id',
			type: 'string'
		}, {
			name: 'name',
			type: 'string'
		}, {
			name: 'description',
			type: 'string'
		}, {
			name: 'dummy',
			type: 'bool'
		}
	]
});

Ext.define('RS.actiontask.model.ValueToOutputs', {
	extend: 'Ext.data.Model',
	idProperty: 'id',
	fields: [
		{
			name: 'id',
			type: 'string'
		}, {
			name: 'outputType',
			type: 'string'
		}, {
			type: 'string',
			name: 'outputName'
		}, {
			name: 'sourceType',
			type: 'string'
		}, {
			name: 'sourceName',
			type: 'string'
		}, {
			name: 'script',
			type: 'string'
		}, {
			name: 'dummy',
			type: 'boolean'
		}, {
			name: 'editable',
			type: 'boolean',
			defaultValue: false
		}, {
			name: 'isPromoted',
			type: 'boolean',
			defaultValue: false
		}
	]
});

glu.defModel('RS.actiontaskbuilder.Access', {
	uname: null,
	read: true,
	write: true,
	execute: true,
	admin: true,
	isDisabled: true,
	isAdminHidden: false,
	rolesStore: null,

	setRoles: function (roles) {
		roles = this.rolesStore.shapeData(roles);	
		this.rolesStore.loadData(roles);
		this.set('uname', null);
	},

	addRole: function (role) {
		role = this.rolesStore.shapeData(role);
		this.rolesStore.add(role);
		this.set('uname', null);
	},

	removeRole: function (role) {
		this.rolesStore.removeBy('value', role);
		this.set('uname', null);
	},

	addIsDisabled$: function () {
		var allCheckboxesAreUnchecked = !this.read && !this.write && !this.execute && !this.admin;
		return this.uname === null || this.uname.length === 0 || allCheckboxesAreUnchecked;
	},

	addIsEnabled$: function () {
		var oneCheckboxIsChecked = this.read || this.write || this.execute || this.admin;
		return this.uname !== null && this.uname.length > 1 && oneCheckboxIsChecked;
	},

	init: function () {
		this.set('rolesStore', new RS.common.data.FlatStore());
		var hidden = true;

		if (clientVM.user.roles && clientVM.user.roles.constructor === Array) {
			hidden = clientVM.user.roles.indexOf('admin') === -1;
		}

		this.set('isAdminHidden', hidden);
	},

	setDisabled: function (isDisabled) {
		this.set('isDisabled', isDisabled);
	},

	add: function () {
		this.rolesStore.removeBy('value', this.uname);
		this.parentVM.add(this);
		this.set('uname', '');
		this.set('read', true);
		this.set('write', true);
		this.set('execute', true);
		this.set('admin', true);
	},	

	focusRole: function (e) {
		if (e && e.getKey && typeof e.getKey() !== 'undefined' && e.currentTarget) {
			var id = e.currentTarget.id;

			if (id && id.length > 0) {
				var componentID = id.substring(0, id.indexOf('-inputEl'));
				var combo = Ext.ComponentQuery.query('*[id^='+ componentID +']');

				if (combo && combo.length > 0) {
					combo[0].expand();
				}
			}
		}
	}
});
Ext.define('RS.actiontaskbuilder.AssessorEditBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Assessor',
		hidden: true,
		collapsed: true,
		editBlock: true,
		title: '',
		header: {
            titlePosition: 0,
            defaults: {
            	margin: 0
        	},
            items: []
        },
		classList: 'block-model',
		editorClass: 'at-code-editor-length',
		editorName: '',
		content: '',
		onlyCompleted: true,
		content: '',
		warningMsg: '',
		warningLabel: null,
		MAX_LEN: 150000,

		minResize: 133,
		minHeight: 371,
		height: 371,

		when_onlyCompleted_changed: {
			on: ['onlyCompletedChanged'],
			action: function () {
				this.notifyDirty();
			}
		},

		expand: function() {
			this.validateAndDisplayWarning(this.content);
		},

		validateAndDisplayWarning: function(val) {
			var html = '',
				charsRemaining = this.MAX_LEN - val.length,
				messageParts = [];

			if (val.length > this.MAX_LEN) {
				messageParts = [this.editorName, val.length, this.MAX_LEN, charsRemaining * -1];
				html = this.localize('editorExceedWarningMsg', messageParts);
				this.set('editorClass', 'at-code-editor-max-length-exceeded');
			} else {
				messageParts = [charsRemaining, val.length, this.MAX_LEN];
				html = this.localize('editorLengthMsg', messageParts);
				this.set('editorClass', 'at-code-editor-length');
			}

			if (this.warningLabel) {
				this.warningLabel.getEl().setHTML(html);
			} else {
				this.set('warningMsg', html);
			}
		},

		contentChange: function() {
			if (!this.task) {
				this.task = new Ext.util.DelayedTask();
			}

			this.task.delay(500, this.saveContent, this, arguments);
		},

		saveContent: function(content) {
			var val = content.getValue();
			this.validateAndDisplayWarning(val);
			this.set('content', val);
			this.notifyDirty();
		},

		warningMsgShowed: function(warnLabel) {
			this.set('warningLabel', warnLabel);
			this.validateAndDisplayWarning(this.content);
		},	

		postInit: function (blockModel) {
			this.blockModel = blockModel;
			this.editorName = this.localize('assess');
		},

		notifyDirty: function() {
			this.blockModel.notifyDirty();
		},

		closeFlag: false,
		close: function () {
			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.assess'];
	},

	id: '',
	uname: '',

	load: function (assess) {
		this.id = assess.id;
		this.uname = assess.uname;		
		this.set('content', assess.uscript || '')
			.set('onlyCompleted', !!assess.uonlyCompleted);
	},

	getData: function () {
		return [{
			id: this.id,
			uname: this.uname,
			uscript: this.gluModel.content,
			uonlyCompleted: this.gluModel.onlyCompleted
		}];
	}
});

Ext.define('RS.actiontaskbuilder.AssessorReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Assessor',
		collapsed: true,
		classList: ['block-model'],
		title: '',
		hidden: false,
		hasDetails: false,
		header: {
            titlePosition: 0,
            defaults: {
            	margin: 0
        	},
            items: []
        },
		content: '',
		contentIsEmpty: false,
		onlyCompleted: true,

		minResize: 71,
		minHeight: 371,
		height: 371,
		
		postInit: function (blockModel) {
			this.blockModel = blockModel;
		},

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden && this.parentVM.embed) {
					var preprocessorData = this.parentVM.assessEdit.getData();
					this.parentVM.assessRead.load(preprocessorData[0]);
				}

				if (this.contentIsEmpty) {
					this.shrinkPanel();
				} else {
					this.growPanel();
				}
			}
		},

		when_collapsed_changed: {
			on: ['collapsedChanged'],
			action: function() {
				if (this.contentIsEmpty) {
					this.shrinkPanel();
				} else {
					this.growPanel();
				}
			}
		},

		when_contentIsEmpty_changed: {
			on: ['contentIsEmptyChanged'],
			action: function() {
				if (this.contentIsEmpty) {
					this.shrinkPanel();
				} else {
					this.growPanel();
				}
			}
		},

		shrinkPanel: function () {
			this.set('minHeight', 86);
			this.set('minResize', 86);
			this.set('height', 0);
			this.set('height', 'auto');
		},

		growPanel: function () {
			this.set('minHeight', 371);
			this.set('minResize', 133);
			this.set('height', 0);
			this.set('height', this.minHeight);
		},

		edit: function() {
			this.blockModel.close('AssessorEdit');
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.assess'];
	},

	load: function (assess) {
		var g = this.gluModel;
		var script = assess.uscript || '',
			classes = ['block-model'];

		this.set('content', script)
			.set('onlyCompleted', !!assess.uonlyCompleted)
			.set('classList', classes);

		g.set('contentIsEmpty', !g.content || g.content.length === 0);
	}
});

Ext.define('RS.actiontaskbuilder.AssignValuesToOutputsEditBlockModel', (function () {
	var pluralMap = {
		INPUT: 'INPUTS',
		OUTPUT: 'OUTPUTS',
		FLOW: 'FLOWS',
		PARAM: 'PARAMS',
		WSDATA: 'WSDATA',
		CONSTANT: 'CONSTANT'
	};
	var singularMap = {
		INPUTS: 'INPUT',
		OUTPUTS: 'OUTPUT',
		FLOWS: 'FLOW',
		PARAMS: 'PARAM',
		WSDATA: 'WSDATA',
		CONSTANT: 'CONSTANT'
	};

	function isDuplicate (a, b) {
		return a.outputType === b.outputType
			&& a.outputName === b.outputName
			&& a.sourceType === b.sourceType
			&& a.sourceName === b.sourceName;
	}

	function isDuplicateAssignment (a, b) {
		return a.outputType === b.outputType
			&& a.outputName === b.outputName;
	}	
	return {
		extend: 'RS.common.blocks.BlockModel',
		modelConfig: {
			mixins: ['QuickAccessHelpers'],
			blockModel: null,
			blockModelClass: 'AssignValuesToOutputs',
			title: '',
			hidden: true,
			collapsed: true,
			editBlock: true,
			duplicateAssignmentCounter : 0,
			header: {
				titlePosition: 0,
				items: []
			},			
			mappingStore: null,
			valuesCount: 0,
			gridIsEmpty: false,
			outputTypesStore: {
				mtype : 'store',
				fields : ['name']				
			},		
			sourceTypesStore: {
				mtype : 'store',
				fields : ['name']			
			},		
			inputParametersStore: {
				mtype : 'store',
				fields : ['name']
			},
			outputParametersStore: {
				mtype : 'store',
				fields : ['name']
			},
			allParameters : {
				INPUT : [],
				OUTPUT : [],
				FLOW : [],
				WSDATA : []			
			},
			detail: {
				mtype: 'AssignValueToOutputDetail'
			},		

			postInit: function (blockModel) {
				this.blockModel = blockModel;
				this.set('mappingStore', Ext.create('Ext.data.Store',{ 
					fields: ['outputType', 'outputName', 'sourceType', 'sourceName','isDuplicateAssignment'],
					listeners: {
						datachanged: function () {
							this.notifyDirty();
						}.bind(this),
						update: function () {
							this.notifyDirty();
						}.bind(this)	
					}
				}));

				var outputTypes = [{
					name : 'OUTPUT'
				},{
					name : 'FLOW'
				},{
					name : 'PARAM'
				},{
					name : 'WSDATA'
				}];

				var sourceTypes =  [{
					name : 'INPUT'
				},{
					name : 'FLOW'
				},{
					name : 'PARAM'
				},{
					name : 'WSDATA'
				},{
					name : 'CONSTANT'
				}];

				this.outputTypesStore.loadData(outputTypes);
				this.detail.setOutputTypes(outputTypes);
				this.sourceTypesStore.loadData(sourceTypes);
				this.detail.setSourceTypes(sourceTypes);					
			},
			notifyDirty: function() {
				this.blockModel.notifyDirty();
			},

			validateOnEdit : function(context){
				var field = context.field;
				if(field == 'outputType' || field == 'outputName'){
					var duplicateAssignmentCounter = this.duplicateAssignmentCounter;
					var oldDuplicatedIndexes = [];
					var newDuplicatedIndexes = [];
					var store = context.store;
					var range = store.getRange();
					var currentRecordIndex = context.rowIdx;
					var oldRecord = context.record.data;
					var newRecord = Ext.apply({}, oldRecord);
					newRecord[field] = context.value;

					//Check for dupplicate assignment
					for (var i = 0; i < range.length; i++) {
						var d = range[i].data;
						if(i == currentRecordIndex)
							continue;
						if (isDuplicate(d, newRecord)) {							
							this.message({
								scope: this,
								title: this.localize('invalidAssignmentTitle'),
								msg: this.localize('assignmentAlreadyExists'),
								buttons: Ext.MessageBox.OK
							});
							return false;
						} else if (isDuplicateAssignment(d, oldRecord)) {							
							oldDuplicatedIndexes.push(i);		
						} else if (isDuplicateAssignment(d, newRecord)) {							
							newDuplicatedIndexes.push(i);		
						}
					}

					if(oldDuplicatedIndexes.length == 1){
						range[oldDuplicatedIndexes[0]].set('isDuplicateAssignment', false);
						duplicateAssignmentCounter--;
					}

					if(newDuplicatedIndexes.length > 0)
						range[currentRecordIndex].set('isDuplicateAssignment', true);					
					else
						range[currentRecordIndex].set('isDuplicateAssignment', false);		
					
					if(newDuplicatedIndexes.length == 1){
						range[newDuplicatedIndexes[0]].set('isDuplicateAssignment', true);
						duplicateAssignmentCounter++;
					}
					this.set('duplicateAssignmentCounter',duplicateAssignmentCounter);
				}
			},
			displayInvalidNameMsg : function(msg){
				clientVM.message(this.localize('invalidAssignmentTitle'), this.localize(msg));		
			},
			loadParameterStore : function(fieldName, fieldValue){
				var store = fieldName == 'outputName' ? this.outputParametersStore : this.inputParametersStore;
				if(fieldValue == 'INPUT')
					store.loadData(this.allParameters['INPUT']);
				else if(fieldValue == 'OUTPUT')
					store.loadData(this.allParameters['OUTPUT']);
				else if(fieldValue == 'FLOW')
					store.loadData(this.allParameters['FLOW']);
				else if(fieldValue == 'WSDATA')
					store.loadData(this.allParameters['WSDATA']);
			},
			addAssignment: function (m) {
				var isValid = true;				
				var	range = this.mappingStore.getRange();
				var duplicateAssignmentIndexes = [];
			
				for (var i = 0; i < range.length; i++) {
					var d = range[i].data;

					if (isDuplicate(d, m)) {
						isValid = false;
						this.message({
							scope: this,
							title: this.localize('invalidAssignmentTitle'),
							msg: this.localize('assignmentAlreadyExists'),
							buttons: Ext.MessageBox.OK
						});
						break;
					} else if (isDuplicateAssignment(d, m)) {					
						range[i].set('isDuplicateAssignment', true);
						duplicateAssignmentIndexes.push(i);		
					}
				}
				
				//New duplicate assignment
				if(duplicateAssignmentIndexes.length == 1)
					this.set('duplicateAssignmentCounter', this.duplicateAssignmentCounter + 1);
				if (isValid) {
					var newRecords = this.mappingStore.add(m);
					if(duplicateAssignmentIndexes.length > 0)
						newRecords[0].set('isDuplicateAssignment', true);					
					var count = this.mappingStore.getCount();
					this.set('gridIsEmpty', count === 0);
					this.set('valuesCount', count);
				}

				return isValid;
			},

			removeAssignment: function (record) {
				if(record.get('isDuplicateAssignment')){
					var indexOfRecord = this.mappingStore.indexOf(record);
					var range = this.mappingStore.getRange();
					var duplicateAssignmentIndexes = [];
					for(var i = 0; i < range.length; i++){
						var currentData = range[i].getData();
						if(i != indexOfRecord && isDuplicateAssignment(currentData, record.getData()))
							duplicateAssignmentIndexes.push(i);
					}
					//Only 1 pair of duplicated. Update that record then decrease counter.
					if(duplicateAssignmentIndexes.length == 1){
						var duplicatedRecord = this.mappingStore.getAt(duplicateAssignmentIndexes[0]);
						duplicatedRecord.set('isDuplicateAssignment', false);
						this.set('duplicateAssignmentCounter', this.duplicateAssignmentCounter - 1);
					}
				}
				this.mappingStore.remove(record);
				var count = this.mappingStore.getCount();
				this.set('gridIsEmpty', count === 0);
				this.set('valuesCount', count);
			},
			hasDuplicateAssignment$ : function(){
				return this.duplicateAssignmentCounter > 0;
			},
			closeFlag: false,
			close: function () {
				if (this.isSectionMaxSize) {
					this.parentVM.resizeNormalSection(this);
				}
				this.closeFlag = true;
				this.set('collapsed', true);
			}
		},

		getData: function (getDataForServer) {
			var g = this.gluModel;
			var assignments = [];

			//Map source to plural
			g.mappingStore.each(function(record){
				var data = record.data;
				var oldRecord = Ext.apply({}, record.raw);
				assignments.push(Ext.apply(oldRecord, {
					outputType : pluralMap[data['outputType']],
					outputName : data['outputName'],
					sourceType : pluralMap[data['sourceType']],
					sourceName : data['sourceName'],
				}));			
			})
				
			return [assignments];
		},

		getDataPaths: function () {
			return ['data.resolveActionInvoc.outputMappings'];
		},

		load: function (outputMappings) {
			var g = this.gluModel;

			for(var i = 0; i < outputMappings.length; i++){
				var entry = outputMappings[i];
				entry['outputType'] = singularMap[entry['outputType']];
				entry['sourceType'] = singularMap[entry['sourceType']];
			}
			g.mappingStore.loadData(outputMappings);

			var range = g.mappingStore.getRange();
			var duplicateAssignmentCounter = 0;
			for (var i = 0; i < range.length; i++) {
				var duplicateAssignmentFound = false;
				var d = range[i];
				//If already mark then continue
				if(d.get('isDuplicateAssignment'))
					continue;
				else{
					for (var j = i + 1; j < range.length; j++) {
						var other = range[j];

						if (isDuplicateAssignment(d.getData(), other.getData())) {						
							other.set('isDuplicateAssignment',true);
							duplicateAssignmentFound = true;							
						}
					}
					if(duplicateAssignmentFound){
						d.set('isDuplicateAssignment',true);
						duplicateAssignmentCounter++;
					}
				}				
			}
			this.set('duplicateAssignmentCounter', duplicateAssignmentCounter);
			
			var count = g.mappingStore.getCount();
			g.set('gridIsEmpty', count === 0)
			g.set('valuesCount', count);
		},
		processDataChange : function(data){
			var g = this.gluModel;
			var detail = g.detail;
			for(var type in data){
				var processedData = [];
				for(var i = 0; i < data[type].length; i++){
					processedData.push({
						name : data[type][i]
					})
				}
				g.allParameters[type] = processedData;
				detail.allParameters[type] = processedData;
			}
			//Repopulate already selected type for each field.
			detail.loadParameterStore(detail.outputParametersStore, detail.outputType);
			detail.loadParameterStore(detail.inputParametersStore, detail.sourceType);
		}	  
	};
})());

Ext.define('RS.actiontaskbuilder.AssignValuesToOutputsReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'AssignValuesToOutputs',
		classList: 'block-model',
		hidden: false,	
		title: '',
		collapsed: true,
		header: {
			titlePosition: 0,
            defaults: {
                margin: 0
            },					
			items: []
		},
		valuesStore: null,
		hasValues: false,

		postInit: function (blockModel) {
			this.blockModel = blockModel;
			this.set('valuesStore', new RS.common.data.FlatStore({
				fields: ['outputType', 'outputName', 'sourceType', 'sourceName']
			}));		
		},

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden && this.parentVM.embed) {
					var assignmentsData = this.parentVM.assignValuesToOutputsEdit.getData();
					this.parentVM.assignValuesToOutputsRead.load(assignmentsData[0]);
				}
			}
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.outputMappings'];
	},

	getData: function () {
		return [this.gluModel.valuesStore.getData()];
	},

	load: function (outputMappings) {
		this.gluModel.valuesStore.loadData(outputMappings);
		var hasValues = outputMappings.length > 0,
			classes = ['block-model'];

		this.set('hasValues', hasValues)
			.set('classList', classes);
	}
});

glu.defModel('RS.actiontaskbuilder.AssignValueToOutputDetail', {
	fields : ['outputType','outputName','sourceType','sourceName'],			
	validationRegex : RS.common.validation.VariableName,	
	outputTypeDisplay: '',	
	outputNameEmptyText: '',	
	sourceTypeDisplay: '',			
	sourceNameEmptyText: '',
	outputTypesStore: {
		mtype : 'store',
		fields : ['name']
	},		
	sourceTypesStore:  {
		mtype : 'store',
		fields : ['name']
	},
	sourceLabelText: '',
	inputParametersStore: {
		mtype : 'store',
		fields : ['name']
	},
	outputParametersStore: {
		mtype : 'store',
		fields : ['name']
	},
	allParameters : {
		INPUT : [],
		OUTPUT : [],
		FLOW : [],
		WSDATA : []			
	},
	defaultValue : {
		outputType : '',
		outputName : '',
		sourceType : '',
		sourceName : ''
	},	
	when_output_type_changed : {
		on : ['outputTypeChanged'],
		action : function(){
			var currentType = this.outputType;
			this.set('outputName', '');
			if(currentType == 'OUTPUT')
				this.set('outputNameEmptyText',this.localize('selectOutput'));
			else if(currentType == 'WSDATA' || currentType == 'FLOW')
				this.set('outputNameEmptyText', this.localize('selectOrEnterOutput'));
			else 
				this.set('outputNameEmptyText', this.localize('enterOutputName'));
			this.set('outputNameIsPristine', true);
			this.loadParameterStore(this.outputParametersStore, currentType);
		}
	},			
	when_source_type_changed : {
		on : ['sourceTypeChanged'],
		action : function(){
			var currentType = this.sourceType;
			this.set('sourceName', '');
			if(currentType == 'INPUT')
				this.set('sourceNameEmptyText',this.localize('selectInput'));
			else if(currentType == 'WSDATA' || currentType == 'FLOW')
				this.set('sourceNameEmptyText', this.localize('selectOrEnterInput'));
			else 
				this.set('sourceNameEmptyText', this.localize('enterSourceName'));
			this.set('sourceNameIsPristine', true);
			this.loadParameterStore(this.inputParametersStore, currentType);				
		}
	},
	init : function(){
		this.set('outputNameEmptyText', this.localize('enterOutputName'));
		this.set('sourceNameEmptyText', this.localize('enterSourceName'));
	},
	//Validation		
	outputTypeIsValid$ : function(){
		return this.outputType ? true : this.localize('requiredField');
	},
	outputNameIsText$ : function(){
		return this.outputType == 'PARAM';
	},
	outputNameIsEditable$ : function(){
		return this.outputType == 'FLOW' || this.outputType == 'WSDATA' || this.outputType == 'OUTPUT';
	},
	outputNameIsValid$ : function(){
		return this.outputName/* && this.validationRegex.test(this.outputName)*/ ? true : this.localize('validKeyCharacters');
	},
	sourceTypeIsValid$ : function(){
		return this.sourceType ? true : this.localize('requiredField');
	},
	sourceNameIsText$ : function(){
		return this.sourceType == 'PARAM' || this.sourceType == 'CONSTANT';
	},
	sourceNameIsEditable$ : function(){
		return this.sourceType == 'FLOW' || this.sourceType == 'WSDATA'
	},
	sourceNameIsValid$ : function(){
		return this.sourceName /*&& this.validationRegex.test(this.sourceName)*/ ? true : this.localize('validKeyCharacters');
	},		
	
	loadParameterStore : function(store, dataType){
		if(dataType == 'INPUT')
			store.loadData(this.allParameters['INPUT']);
		else if(dataType == 'OUTPUT')
			store.loadData(this.allParameters['OUTPUT']);
		else if(dataType == 'FLOW')
			store.loadData(this.allParameters['FLOW']);
		else if(dataType == 'WSDATA')
			store.loadData(this.allParameters['WSDATA']);
	},	
	setOutputTypes: function (outputTypes) {
		this.outputTypesStore.loadData(outputTypes);		
	},
	setSourceTypes: function (sourceTypes) {
		this.sourceTypesStore.loadData(sourceTypes);	
	},		
	addAssignment: function () {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		var added = this.parentVM.addAssignment(this.asObject());

		if (added) {
			this.resetForm();	
		}
	},
	resetForm : function(){
		this.loadData(this.defaultValue);
		this.set('isPristine', true);
	}
});
glu.defModel('RS.actiontaskbuilder.Commit', {
	comment: '',

	commit: function() {
		this.parentVM.verifyCommitVersion(this.comment);
		this.doClose()
	},
	commitIsEnabled$: function() {
		return this.comment;
	},
	cancel: function() {
		this.doClose()
	}
})
Ext.define('RS.actiontaskbuilder.ConditionsEditBlockModel', {
    extend: 'RS.common.blocks.BlockModel',

    modelConfig: {
		mixins: ['QuickAccessHelpers'],
        blockModel: null,
		blockModelClass: 'SeveritiesAndConditions',
        hidden: true,
		collapsed: true,
		editSubBlock: true,
		stopPropagation: false,
        title: '',
        header: {
            xtype: 'toolbar',
            margin: 0,
            padding: 0,
            items: []
        },

        items: [],
        expressionType : 'condition',	

        postInit: function (blockModel) {
            this.blockModel = blockModel;
            var bad = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            bad.initContainer(1, '~~badTitle~~', '#c00');
            var good = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            good.initContainer(0, '~~goodTitle~~', '#0b0');
            this.items.push(bad);
            this.items.push(good);
            this.header.items = [/*{
				xtype: 'component',
				padding: '0 0 0 15',
				cls: 'x-header-text x-panel-header-text-container-default',
				html: this.localize('conditionsEditTitle')
			},*/ 
			{
                xtype: 'container',
                autoEl: 'ol',
                cls: 'breadcrumb flat',
                margin: 0,
                padding: 0,
                items: [{
                    xtype: 'component',
                    autoEl: 'li',                  
                    html: this.localize('severitiesEditTitle'),
                    listeners: {
                        afterrender: function (c) {
                            c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.jumpTo('SeveritiesEdit', this.parentVM.severitiesEdit.gluModel);
                            }, this);                       
                        }.bind(this)
                    }
                }, {
                    xtype: 'component',
                    autoEl: 'li',
                    html: this.localize('conditionsEditTitle'),
                    cls: 'active',
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
							}, this);
						}.bind(this)
					}
                }]
            }, '->', {
				xtype: 'button',
				tooltip: this.localize('resizeFullscreen'),
				iconCls: 'rs-block-button icon-resize-full',
				isResizeMaxBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['SEVERITYCONDITION']);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				tooltip: this.localize('resizeNormalscreen'),
				iconCls: 'rs-block-button icon-resize-small',
				hidden: true,
				isResizeNormalBtn: true,
				margin: '0 20',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeNormalSection(this);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				iconCls: 'rs-block-button icon-chevron-sign-up',
				isCollapseBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.set('collapsed', true);
						}, this);
					}.bind(this)
				}
			}];

			if(this.rootVM.getAllATPropertiesInProgress){
                var checkingForATProperties = setInterval(function(){
                    if(!this.rootVM.getAllATPropertiesInProgress){
                        var conditionCategoryCount = 2;
                        var items = this.items;
                        
                        for (var i = 0; i < conditionCategoryCount; i++) {
                            items[i].updateParameters({
                                PROPERTY : this.rootVM.allATProperties
                            });
                        }             
                        clearInterval(checkingForATProperties);
                    }
                }.bind(this),500)
            }   
        },
        
        jumpTo: function(nextBlockName, nextSection){
			this.parentVM.severitiesAndConditionsEditSectionExpanded = !this.collapsed;
			this.parentVM.severitiesAndConditionsEditSectionMaximized = this.isSectionMaxSize;
			
			this.parentVM.collapseAndHideSection(this);
			this.parentVM.showAndExpandThisSection(nextSection);
        }, 

        reclassifyExpression: function(expression){
            for(var i = 0; i < this.items.length; i++){
                if(this.items[i].category == expression.category){
                    this.items[i].updateCategoryForExpression(expression);
                }
            }
        }, 

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden) {
					setTimeout(function() {
						if (this.parentVM.severitiesAndConditionsEditSectionExpanded) {
							this.parentVM.severitiesAndConditionsEditSectionExpanded = null;
							this.set('collapsed', false);
						}
						if (this.parentVM.severitiesAndConditionsEditSectionMaximized) {
							this.parentVM.severitiesAndConditionsEditSectionMaximized = null;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['SEVERITYCONDITION']);
						}
					}.bind(this), 10);
				}
			}
		},

		closeFlag: false,
		close: function () {
			this.expShowAndValid = false;
            this.traverse(this.setFormValid);

			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		},

        applyAdd: function(exp) {
            if (exp.form && !exp.form.isHidden() && exp.form.isValid()) {
                var btn = exp.form.down('#doneBtn');
                btn? btn.getEl().dom.click(): null;
            }
        },

        setFormValid: function(exp) {
            if (exp.form && !exp.form.isHidden() && exp.form.isValid()) {
                this.expShowAndValid = true;
            }
        },

        traverseAndApply: function(blk, fn, blkName, args) {
            if (blkName) {
                var items = blk[blkName];
                for (var i=0; i<items.length; i++) {
                    args? fn.apply(this, [items.getAt(i)].concat(args)): fn.apply(this, [items.getAt(i)].concat(fn));
                }
            } else {
                fn.apply(this, blk);
            }
        },

        traverse: function(applyFn) {
            for (var i=0; i<this.items.length; i++) {
                this.traverseAndApply(this.items[i], this.traverseAndApply, 'blocks', [applyFn, 'expressions']);
            }
        }
    },

    getDataPaths: function () {
        return [{
            filter: function (expression) { return expression.expressionType === 'condition'; },
            path:'data.resolveActionInvoc.expressions'
        }];
    },

    getData: function () {
        var conditions = [],
            items = this.gluModel.items,
            pluralMap = {
                INPUT: 'INPUTS',
                OUTPUT : 'OUTPUTS',
                FLOW: 'FLOWS',
                PARAM: 'PARAMS',
                PROPERTY: 'PROPERTIES',
                WSDATA: 'WSDATA',
                CONSTANT: 'CONSTANT'
            };

        for (var i = 0; i < items.length; i++) {
            var conditionGroup = items[i],
                data = conditionGroup.getData(this.gluModel.expressionType);

            for (var j = 0; j < data.length; j++) {
                var e = data[j];
                e.leftOperandType = pluralMap[e.leftOperandType] || e.leftOperandType;
                e.rightOperandType = pluralMap[e.rightOperandType] || e.rightOperandType;
            }

            conditions.push.apply(conditions, data);
        }

        return [conditions];
    },	

    load: function (expressions) {
        var g = this.gluModel,
			items = g.items,
            singularMap = {
            INPUTS: 'INPUT',
            OUTPUTS: 'OUTPUT',
            FLOWS: 'FLOW',
            PARAMS: 'PARAM',
            PROPERTIES: 'PROPERTY',
            WSDATA: 'WSDATA',
            CONSTANT: 'CONSTANT'
        };        

        for (var i = 0; i < items.length; i++) {
            items[i].clear();
        }

        var itemLevelMap = [1, 0],
            orderMap = [[], []];

        // generate order plan
        for (var i = 0; i < expressions.length; i++) {
            var e = expressions[i],
                itemIndex = itemLevelMap[e.resultLevel];
            e.leftOperandType = singularMap[e.leftOperandType] || e.leftOperandType;
            e.rightOperandType = singularMap[e.rightOperandType] || e.rightOperandType;                
            orderMap[itemIndex].push(e);
        }

        // eliminate gaps in order and load as we process the expressions.
        for (var i = 0; i < orderMap.length; i++) {
            var t = orderMap[i];
            t.sort(function (a, b) {
                var result = 0;

                if (a.order < b.order) {
                    result = -1;
                } else if (a.order > b.order) {
                    result = 1;
                }

                return result;
            });

            if (t.length > 0) {
                var reducedOrder = 0;
                var lastOrderEncountered = t[0].order;

                for (var j = 0; j < t.length; j++) {
                    var e = t[j];

                    if (e.order > lastOrderEncountered) {
                        lastOrderEncountered = e.order;
                        reducedOrder++;
                    }

                    var itemIndex = itemLevelMap[e.resultLevel];
                    e.isRemoveHidden = false;
                    e.isOrHidden = false;
                    items[itemIndex].load(e, reducedOrder);
                }
            }               
        }

        for (var i = 0; i < items.length; i++) {
            items[i].enableAdd();
        }		
    },  

    processDataChange : function(data){
        var conditionCategoryCount = 2,
            items = this.gluModel.items;
        
        for (var i = 0; i < conditionCategoryCount; i++) {
            items[i].updateParameters(data);
        }     
    }  
});

Ext.define('RS.actiontaskbuilder.ContentEditBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Content',
		hidden: true,
		collapsed: true,
		conditionalEditBlock: true,
        title: '',
       	header: {
            titlePosition: 0,
            defaults: {
            	margin: 0
        	},
            items: []
        },
		classList: 'block-model',
		content: '',
		syntax: '',
		command: '',
		contentLabel: 'Content',
		editorType: 'text',
		syntaxStore: null,
		commandIsVisible: true,
		syntaxIsVisible: true,

		minHeight: 71,
		minResize: 133,
		height: 371,
		editorFlexValue$: function() {
			if (!this.parentVM.embed) {
				return 1;
			}
		},
		editorHeight$: function() {
			if (!this.parentVM.embed) {
				return this.height;
			}

			var editorHeight = this.height;
			if (this.commandIsVisible && this.syntaxIsVisible) {
				editorHeight = editorHeight - 100;
			} else if (this.syntaxIsVisible) {
				editorHeight = editorHeight - 40;
			}

			editorHeight = editorHeight - 100; // normal offset

			return editorHeight;
		},

		expand: function() {
			this.validateAndDisplayWarning(this.content);
		},

		contentChange: function() {
			if (!this.task) {
				this.task = new Ext.util.DelayedTask();
			}

			this.task.delay(500, this.saveContent, this, arguments);
		},

		saveContent: function(content) {
			var val = content.getValue();
			this.set('content', val);
			this.notifyDirty();
		},

		when_command_changed: {
			on: ['commandChanged'],
			action: function() {
				this.notifyDirty();
			}
		},

		postInit: function (blockModel) {
			this.blockModel = blockModel;
			this.set('syntaxStore', new RS.common.data.FlatStore(
				['groovy', 'golang','perl', 'powershell', 'ruby', 'sql', 'text', 'xml']
			));
		},

		trimRegex: /^\s+|\s+$/g,

		addInputFile: function() {
			this.set('command', this.command+' ${INPUTFILE}');
		},

		shrinkPanel: function () {
			this.set('minHeight', 71);
			this.set('minResize', 133);
		},

		growPanel: function () {
			this.set('minHeight', 371);
			this.set('minResize', 433);
		},

		notifyDirty: function() {
			this.blockModel.notifyDirty();
		},

		resizeSection: function(height) {
			this.set('height', height);
		},

		closeFlag: false,
		close: function () {
			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc', {
			path: 'data.resolveActionInvoc.resolveActionInvocOptions',
			filter: function (o) {
				return o.uname === 'INPUTFILE'
			}
		}];
	},

	contentOptionID: '',

	load: function (resolveActionInvoc, inputFile) {
		var g = this.gluModel;
		this.set('command', '');

		switch(resolveActionInvoc.utype) {
		case 'ASSESS':
		case 'REMOTE':
			g.set('commandIsVisible', false);
			g.set('syntaxIsVisible', false);
			break;
		case 'CSCRIPT':
		case 'POWERSHELL':
			g.set('commandIsVisible', false);
			g.set('syntaxIsVisible', true);
			break;
		default:
			g.set('commandIsVisible', true);
			g.set('syntaxIsVisible', true);

			var command = resolveActionInvoc.ucommand || '';
			var args = resolveActionInvoc.uargs || '';

			if (args.length) {
				command += ' ';
				command += args;
			}

			this.set('command', command);
			break;
		}

		if (g.syntaxIsVisible) {
			var type = resolveActionInvoc.utype;
			this.set('syntax', this.typeMaps[type]);
		}

		if (inputFile && inputFile.length > 0) {
			this.set('content', inputFile[0].uvalue);
			this.contentOptionID = inputFile[0].id;
		}
	},

	getData: function () {
		return [{
			ucommand: this.gluModel.command
		}, [{
			id: this.contentOptionID,
			uname: 'INPUTFILE',
			udescription: '',
			uvalue: this.gluModel.content
		}]];
	},

	typeMaps: {
		REMOTE: 'groovy',
		POWERSHELL: 'powershell',
		PROCESS: 'groovy'
	},

	modifyByNotification: function(data, resolveActionInvoc, assignedTo) {
		var t = resolveActionInvoc.utype,
			isNew = !data.id;

		if (typeof this.typeMaps[t] !== 'undefined') {
			this.set('editorType', this.typeMaps[t]);
		} else {
			this.set('editorType', 'text');
		}

		if (t === 'ASSESS') {
			this.set('commandIsVisible', false)
				.set('syntaxIsVisible', false);
			this.hide();
		} else {
			switch (t) {
			case 'REMOTE':
				this.setViewTitle(this.gluModel.localize('titleForRemote'));
				this.set('commandIsVisible', false)
					.set('syntaxIsVisible', false);
				break;
			case 'CSCRIPT':
				this.setViewTitle(this.gluModel.localize('titleForCScript'));
				this.set('commandIsVisible', false)
					.set('syntaxIsVisible', true)
					.set('contentLabel', this.gluModel.localize('contentLabel'));
				break;
			case 'POWERSHELL': 
				this.setViewTitle(this.gluModel.localize('titleForPowershell'));
				this.set('commandIsVisible', false)
					.set('syntaxIsVisible', true)
					.set('contentLabel', this.gluModel.localize('contentLabel'));
				break;
			case 'CMD': 
			case 'OS': 
			case 'BASH': 
				this.setViewTitle(this.gluModel.localize('titleForCommand'));
				this.set('commandIsVisible', true)
					.set('syntaxIsVisible', true)
					.set('contentLabel', this.gluModel.localize('inputFileLabel'));			
				break;
			default:
				// process or external
				this.setViewTitle(this.gluModel.localize('titleForDefault'));
				this.set('commandIsVisible', true)
					.set('syntaxIsVisible', true)
					.set('contentLabel', this.gluModel.localize('contentLabel'));
				break;
			}

			if ((!isNew && !this.gluModel.parentVM.readOnlyMode) &&
				(this.gluModel.parentVM.sectionButtonSelected == this.gluModel.parentVM.sectionButtonMap['CONTENT'] || this.gluModel.parentVM.sectionButtonSelected == this.gluModel.parentVM.sectionButtonMap['ALL']))
			{
				//this.showCurrent();
				this.gluModel.set('hidden', false);
			}
		}
	}	
});
Ext.define('RS.actiontaskbuilder.ContentReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	typeMaps: {
		REMOTE: 'groovy',
		POWERSHELL: 'powershell',
		PROCESS: 'groovy'
	},

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Content',
		collapsed: true,
        title: '',
		hidden: true,
		conditionalReadBlock: true,
       	header: {
            titlePosition: 0,
            defaults: {
            	margin: 0
        	},
            items: []
        },
		content: '',
		contentIsEmpty: false,
		syntax: '',
		command: '',
		contentLabel: 'Content',
		editorType: 'text',
		commandIsVisible: true,

		minHeight: 71,
		minResize: 133,
		height: 371,

		noCommand: '',

		postInit: function (blockModel) {
			this.blockModel = blockModel;
			this.noCommand = this.localize('noCommand');
		},

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden && this.parentVM.embed) {
					var contentData = this.parentVM.contentEdit.getData();

					var resolveActionInvoc = contentData[0],
						inputFile = contentData[1];

					this.parentVM.contentRead.load(resolveActionInvoc, inputFile);
				}

				if (this.contentIsEmpty) {
					this.shrinkPanel();
				} else {
					this.growPanel();
				}
			}
		},

		when_collapsed_changed: {
			on: ['collapsedChanged'],
			action: function() {
				if (this.contentIsEmpty) {
					this.shrinkPanel();
				} else {
					this.growPanel();
				}
			}
		},

		when_contentIsEmpty_changed: {
			on: ['contentIsEmptyChanged'],
			action: function() {
				if (this.contentIsEmpty) {
					this.shrinkPanel();
				} else {
					this.growPanel();
				}
			}
		},

		contentEmptyText$: function() {
			if (this.commandIsVisible) {
				return this.localize('inputFileEmptyText');
			} else {
				return this.localize('sourceCodeSectionEmptyText');
			}
		},

		shrinkPanel: function () {
			if (this.commandIsVisible) {
				this.set('minHeight', 126);
				this.set('minResize', 126);
			} else {
				this.set('minHeight', 86);
				this.set('minResize', 86);
			} 
			this.set('height', 0);
			this.set('height', 'auto');
		},

		growPanel: function () {
			this.set('minHeight', 371);
			this.set('minResize', 133);
			this.set('height', 0);
			this.set('height', this.minHeight);
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc', {
			path: 'data.resolveActionInvoc.resolveActionInvocOptions',
			filter: function (o) {
				return o.uname === 'INPUTFILE'
			}
		}];
	},

	load: function (resolveActionInvoc, inputFile) {
		var g = this.gluModel;
		this.set('command', '');

		switch(resolveActionInvoc.utype) {
		case 'ASSESS':
		case 'REMOTE':
		case 'CSCRIPT':
		case 'POWERSHELL':
			g.set('commandIsVisible', false);
			break;
		default:
			g.set('commandIsVisible', true);
			var command = resolveActionInvoc.ucommand || '';
			var args = resolveActionInvoc.uargs || '';

			if (args.length) {
				command += ' ';
				command += args;
			}

			if (command.length === 0) {
				command = g.noCommand;
			}

			this.set('command', command);
			break;
		}

		if (inputFile && inputFile.length > 0) {
			this.set('content', inputFile[0].uvalue);
		}

		g.set('contentIsEmpty', !g.content || g.content.length === 0);
	},

	modifyByNotification: function(data, resolveActionInvoc, assignedTo) {
		var t = resolveActionInvoc.utype,
			isNew = !data.id;

		if (typeof this.typeMaps[t] !== 'undefined') {
			this.set('editorType', this.typeMaps[t]);
		} else {
			this.set('editorType', 'text');
		}

		if (t === 'ASSESS') {
			this.hide();
			this.set('commandIsVisible', false);
		} else {
			switch(t) {
			case 'REMOTE':
				this.setViewTitle(this.gluModel.localize('titleForRemote'));
				this.set('commandIsVisible', false);
				break;
			case 'CSCRIPT':
				this.setViewTitle(this.gluModel.localize('titleForCScript'));
				this.set('commandIsVisible', false)
					.set('contentLabel', this.gluModel.localize('contentLabel'));
				break;
			case 'POWERSHELL':
				this.setViewTitle(this.gluModel.localize('titleForPowershell'));
				this.set('commandIsVisible', false)
					.set('contentLabel', this.gluModel.localize('contentLabel'));
				break;
			case 'CMD':
			case 'OS':
			case 'BASH':
				this.setViewTitle(this.gluModel.localize('titleForCommand'));
				this.set('commandIsVisible', true)
					.set('contentLabel', this.gluModel.localize('inputFileLabel'));
				break;
			default:
				// process or external
				this.setViewTitle(this.gluModel.localize('titleForDefault'));
				this.set('commandIsVisible', true)
					.set('contentLabel', this.gluModel.localize('contentLabel'));
				break;
			}

			if (!isNew && this.gluModel.parentVM.readOnlyMode) {
				//this.showCurrent();
				this.gluModel.set('hidden', false);
			}
		}
	}
});
Ext.define('RS.actiontaskbuilder.DetailsEditBlockModel',{
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers','SummaryAndDetailUtil'],
		blockModel: null,
		hidden: true,
		collapsed: true,
		editSubBlock: true,
		stopPropagation: false,
		closeFlag: false,
		title: '',
		header: {
	 		xtype: 'toolbar',
	    	margin: 0,
	    	padding: 0,
	    	items: []
	 	},
	 	fields : ['fieldType','fieldName','condition','severity','display'],
	 	when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden) {
					setTimeout(function() {
						if (this.parentVM.summaryAndDetailsEditSectionExpanded) {
							this.parentVM.summaryAndDetailsEditSectionExpanded = null;
							this.set('collapsed', false);
						}
						if (this.parentVM.summaryAndDetailsEditSectionMaximized) {
							this.parentVM.summaryAndDetailsEditSectionMaximized = null;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['SUMMARYDETAILS']);
						}
					}.bind(this), 10);
				}
			}
		},

		postInit: function (blockModel) {
			this.blockModel = blockModel;

			this.header.items = [/*{
				xtype: 'component',
				padding: '0 0 0 15',
				cls: 'x-header-text x-panel-header-text-container-default',
				html: this.localize('detailsEditTitle')
			},*/
			{
	        	xtype: 'container',
	        	autoEl: 'ol',
	        	cls: 'breadcrumb flat',
	        	margin: 0,
	        	padding: 0,
	        	items: [{
		    		xtype: 'component',
		    		autoEl: 'li',			    	
					html: this.localize('summaryEditTitle'),
		    		listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.jumpTo('SummaryEdit', this.parentVM.summaryEdit.gluModel);
				            }, this);						
						}.bind(this)
		    		}
		    	},{
		    		xtype: 'component',
		    		autoEl: 'li',
					html: this.localize('detailsEditTitle'),
		    		cls: 'active',
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
							}, this);
						}.bind(this)
					}
		    	}]
	        }, '->', {
				xtype: 'button',
				tooltip: this.localize('resizeFullscreen'),
				iconCls: 'rs-block-button icon-resize-full',
				isResizeMaxBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['SUMMARYDETAILS']);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				tooltip: this.localize('resizeNormalscreen'),
				iconCls: 'rs-block-button icon-resize-small',
				hidden: true,
				isResizeNormalBtn: true,
				margin: '0 20',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeNormalSection(this);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				iconCls: 'rs-block-button icon-chevron-sign-up',
				isCollapseBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.set('collapsed', true);
						}, this);
					}.bind(this)
				}
			}];

			this.ruleStore.on('datachanged', function(){
				this.fireEvent('ruleStoreChanged');
			}.bind(this));
			this.ruleStore.on('update', function(){
				this.fireEvent('ruleStoreChanged');
			}.bind(this));
			this.loadData(this.defaultData);
			this.set('isPristine', true);
		},
		jumpTo: function(nextBlockName, nextSection){
			this.parentVM.summaryAndDetailsEditSectionExpanded = !this.collapsed;
			this.parentVM.summaryAndDetailsEditSectionMaximized = this.isSectionMaxSize;

			this.parentVM.collapseAndHideSection(this);
			this.parentVM.showAndExpandThisSection(nextSection);
		},		
		notifyDirty: function(format) {
			this.blockModel.notifyDirty();
		},	
		close: function () {
			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		}		
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.assess'];
	},

	getData: function () {
		//Temporary code for now until API is ready
		var ruleStore = this.gluModel.ruleStore;
		var detailRules = [];
		ruleStore.each(function(rule){
			detailRules.push({
				severity : rule.get('severity'),
				condition : rule.get('condition'),
				display : rule.get('display')
			})
		})
		return [{detailRule : JSON.stringify(detailRules)}];
	},


	load: function (data) {
		var data = JSON.parse(data.detailRule);
		var detailRules = Array.isArray(data) ? data : [];
		this.gluModel.ruleStore.loadData(detailRules);
	},		

	processDataChange : function(data){
		var g = this.gluModel;		
		for(var type in data){
			var processedData = [];
			for(var i = 0; i < data[type].length; i++){
				processedData.push({
					name : data[type][i]
				})
			}
			g.allParameters[type] = processedData;			
		}
		//Repopulate already selected type for each field.
		g.loadParameterStore(g.parametersStore, g.fieldType);
	}
});

glu.defModel('RS.actiontaskbuilder.Execute', {
	API : {
		submit : '/resolve/service/execute/submit'
	},
	sirProblemId: '',
	sirContext: false,
	target: null,
	newWorksheet: true,
	activeWorksheet: false,
	debug: false,
	mock: false,
	mockName: '',

	fromWiki: false,
	mockEditable$: function() {
		return this.fromWiki
	},

	mockStore: {
		mtype: 'store',
		fields: ['id', 'uname', 'uparams', 'uinputs', 'uflows'],
		proxy: {
			type: 'memory'
		}
	},

	paramStore: {
		mtype: 'store',
		groupField: 'utype',
		fields: ['name', 'value', 'origValue', 'order', 'utype', 'protected', 'encrypt', 'modified'],
		proxy: {
			type: 'memory'
		}
	},

	origParamStore: {
		mtype: 'store',
		fields: ['name', 'value', 'origValue', 'order', 'utype', 'protected', 'encrypt', 'modified'],
		proxy: {
			type: 'memory'
		}
	},

	paramColumns: [{
		header: '~~name~~',
		dataIndex: 'name',
		filterable: false,
		flex: 1,
		editor: {
			xtype: 'textfield',
			allowBlank: false,
			regex: RS.common.validation.VariableName,
			listeners: {
				validitychange: function(grid, isValid){
					if (!isValid) {
						this.up('grid')._vm.set('executeIsEnabled', false)
					} else {
						this.up('grid')._vm.set('executeIsEnabled', true)
					}
			    },
				blur: function(){
					this.up('grid')._vm.set('executeIsEnabled', true)
				}
			}
		},
		renderer: RS.common.grid.plainRenderer()
	}, {
		header: '~~value~~',
		dataIndex: 'value',
		filterable: false,
		flex: 1,
		editor: {},
		renderer: RS.common.grid.plainRenderer()
	}],

	//when another module use this view, it can use this to init the vm
	inputsLoader: function() {
		var parametersBlockData = this.parentVM.parametersInputEdit.getData(),
			mockBlockData = this.parentVM.mockEdit.getData();

		if (parametersBlockData.length) {
			var params = parametersBlockData[0];
			for (var i=0; i< params.length; i++) {
				if (params[i].utype === 'INPUT' || params[i].utype === null) {
					var param = {
						name: params[i].uname,
						value: params[i].udefaultValue,
						order: params[i].uorder,
						protected: params[i].uprotected,
						encrypt: params[i].uencrypt,
						utype: 'INPUT'
					};
					this.paramStore.add(param);
					this.origParamStore.add(param);
				}
			}
			this.origParamStore.sort('order');
		}

		if (mockBlockData.length) {
			var mockData = mockBlockData[0];
			if (mockData) {
				this.mockStore.add(mockData);
			}
		}
	},
	embedded: false,
	init: function() {
		this.set('sirContext', false);
		this.set('debug', clientVM.executionDebugDefaultMode || false);
		var urlParams = clientVM.rootVM.screens.getAt(clientVM.rootVM.screens.getActiveIndex()).params;
		if (((urlParams.SIR || urlParams.sir) && (urlParams.PROBLEMID || urlParams.problemId)) || this.sirProblemId !== '') {
			this.set('sirContext', true);
		}
		if (this.executeDTO && this.embedded)
			this.embeddedExecution();
		else {
			this.inputsLoader(this.paramStore, this.origParamStore, this.mockStore);
		}
	},

	editParamsIsEnabled$: function() {
		return this.paramsSelections.length == 1 && !this.paramsSelections[0].get('protected');
	},

	embeddedExecution: function() {
		this.doClose();
		this.ajax({
			url: '/resolve/service/actiontask/get',
			params: {
				id: this.executeDTO.id,
				name: this.executeDTO.name
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('LoadActionTaskErr'), respData.message);
					return;
				}
				var task = respData.data;
				var params = task.resolveActionInvoc.resolveActionParameters;
				Ext.Object.each(this.executeDTO.inputParams, function(k, v) {
					Ext.each(params, function(param) {
						if (param.utype != 'INPUT')
							return;
						if (param.uname != k)
							return;
						param.udefaultValue = v;
					});
				});
				var mockData = task.resolveActionInvoc.resolveActionTaskMockData;
				if (this.executionWin)
					this.executionWin.doClose();
				this.executionWin = this.open({
					mtype: 'RS.actiontaskbuilder.Execute',
					// target: button.getEl().id,
					executeDTO: {
						actiontask: task.ufullName,
						actiontaskSysId: task.id,
						// problemId: this.newWorksheet ? 'NEW' : null,
						action: 'TASK'
					},
					inputsLoader: function(paramStore, origParamStore, mockStore) {
						for (var i=0; i< params.length; i++) {
							if (params[i].utype === 'INPUT' || params[i].utype === null) {
								var param = {
									name: params[i].uname,
									value: params[i].udefaultValue,
									order: params[i].uorder,
									protected: params[i].uprotected,
									encrypt: params[i].uencrypt,
									utype: 'INPUT'
								};
								paramStore.add(param);
								origParamStore.add(param);
							}
						}
						origParamStore.sort('order');
						if (mockData) {
							mockStore.add(mockData);
						}
						this.set('embedded', true);
					}
				});
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	paramsSelections: [],
	addParam: function() {
		var max = 0;
		var reg = /^NewParam(\d*)$/;
		var newParam = 'NewParam';

		this.paramStore.each(function(param) {
			if (!reg.test(param.get('name')))
				return;
			var m = reg.exec(param.get('name'));
			var idx = parseInt(m[1]);
			if (idx > max)
				max = idx;
		}, this);
		newParam += ((max + 1) || '');

		var param = {
			name: newParam,
			value: '',
			order: 0,
			utype: 'PARAM'
		}
		this.paramStore.add(param);
		this.origParamStore.add(param);

		// clear out all previously selected items
		this.paramStore.grid.getSelectionModel().deselectAll();

		// determine row of newly added "NewParam"
		var row;
		for (row = this.paramStore.data.items.length-1; row > 0; row--) {
			if (this.paramStore.data.items[row].data.name == newParam) {
				break;
			}
		}
		// start editing the newly selected item
		this.paramStore.grid.getPlugin().startEditByPosition({
			row: row,
			column: 0
		});
	},

	removeParams: function() {
		this.paramStore.remove(this.paramsSelections);
		this.origParamStore.remove(this.paramsSelections);
	},

	mockNameIsEnabled$: function() {
		return this.mock
	},

	mockNameIsValid$: function() {
		return (!this.mock || this.mockName) ? true : this.localize('invalidMockName');
	},

	when_mockName_changed: {
		on: ['mockNameChanged'],
		action: function() {
			this.updateParamList(true);
		}
	},

	when_mock_changed: {
		on: ['mockChanged'],
		action: function() {
			if (this.mock) {
				this.updateParamList(true);
			} else {
				this.updateParamList(false);
			}
		}
	},

	updateParamList: function(isMock) {
		this.paramStore.removeAll();

		for (var i=0; i< this.origParamStore.getCount(); i++) {
			var param = this.origParamStore.getAt(i);
			this.paramStore.add({
				name: param.get('name'),
				value: param.get('value'),
				utype: param.get('utype'),
				order: param.get('order')
			});
		}

		if (isMock) {
			for (var i=0; i< this.mockStore.getCount(); i++) {
				var mock = this.mockStore.getAt(i);
				if (mock.get('uname') == this.mockName) {
					var uparams = this.jsonStrToArray(mock.get('uparams'));
					for (var i=0; i< uparams.length; i++) {
						this.paramStore.add({
							name: uparams[i].name,
							value: uparams[i].value,
							utype: 'PARAM',
							order: 0
						});
					}
					var uinputs = this.jsonStrToArray(mock.get('uinputs'));
					for (var i=0; i< uinputs.length; i++) {
						var name = uinputs[i].name;
						var paramIdx = this.paramStore.findExact('name', name);
						if (paramIdx != -1) {
							var currentParam = this.paramStore.getAt(paramIdx);
							if (currentParam.get('utype') == 'INPUT') {
								currentParam.set('value', uinputs[i].value);
							}
						}
						else {
							this.paramStore.add({
								name: name,
								value: uinputs[i].value,
								utype: 'INPUT',
								order: 0
							});
						}
					}
					/* TODO
					var uflows = this.jsonStrToArray(mock.get('uflows'));
					for (var i=0; i< uflows.length; i++) {
						this.paramStore.add({
							name: uflows[i].name,
							value: uflows[i].value,
							utype: 'FLOW',
							order: 0
						});
					}
					*/
					break;
				}
			}
		}
	},

	cancel: function() {
		this.doClose()
	},
	executeDTO: null,
	execute: function() {
		var params = {};
		var flows = {};
		this.paramStore.each(function(param) {
			var paramName = param.get('name');
			var paramValue = param.get('value');
			if (param.get('utype') == 'FLOW') {
				flows[paramName] = paramValue;
			} else if (param.get('encrypt') && param.get('modified')) {
				var origValue = param.get('origValue');
				if (paramValue == '*****' && origValue) {
					params[paramName] = '_usr:' + origValue;
				} else {
					params[paramName] = '_usr:' + paramValue;
				}
			} else {
				params[paramName] = paramValue;
			}
		}, this);

		var urlParams = clientVM.rootVM.screens.getAt(clientVM.rootVM.screens.getActiveIndex()).params;
		if ((urlParams.SIR || urlParams.sir) && (urlParams.PROBLEMID || urlParams.problemId)) {
			params.PROBLEMID = urlParams.problemId;
		} else if (this.sirProblemId !== '') {
			params.PROBLEMID = this.sirProblemId
		}

		if (this.activityId) {
			params.activityId = this.activityId;
		}

		var mockName = (this.mock) ? this.mockName : '';
		var dto = Ext.apply({
			problemId: this.newWorksheet ? 'NEW' : 'ACTIVE',
			isDebug: this.debug,
			mockName: mockName,
			params: Ext.apply(params, {
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			}),	
		}, this.executeDTO);


		this.ajax({
			url: this.API['submit'],
			jsonData: dto,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				this.doClose();
				if (response.success) {
					clientVM.updateProblemInfo(response.data.problemId, response.data.problemNumber)
					clientVM.fireEvent('worksheetExecuted', clientVM);
					if (this.activeWorksheet) {
						clientVM.fireEvent('refreshActiveWorksheet');
					}
					clientVM.displaySuccess(this.localize('executeSuccess', {
						type: !dto.wiki ? 'ActionTask' : 'Runbook',
						fullName: !dto.wiki ? dto.actiontask : dto.wiki
					}))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	executeIsEnabled$: function() {
		if (this.mockNameIsValid == true) {
			return true;
		}
		else {
			return false;
		}
	},

	jsonStrToArray: function(jsonStr) {
		if (!jsonStr)
			return [];
		var arr = [];
		var json = Ext.JSON.decode(jsonStr);
		for (key in json) {
			arr.push({
				name: key,
				value: json[key]
			})
		}
		return arr;
	}
});
glu.defModel('RS.actiontaskbuilder.Expression', (function () {
	var enterOperand = '',
		selectOperand = '',
		leftOperandCombo = null,
		rightOperandCombo = null,
		operandTypes = ['CONSTANT', 'INPUT', 'OUTPUT', 'FLOW', 'PARAM', 'WSDATA', 'PROPERTY'];

	function operandFilterFactory (operandType) {
		var filter = function () { return true; };

		if (operandType === 'INPUT') {
			filter = function (p) {
				return p && p.utype === 'INPUT' || p.utype === null;
			}
		}else if (operandType === 'OUTPUT') {
			filter = function (p) {
				return p && p.utype === 'OUTPUT' || p.utype === null;
			}
		} else if (operandType === 'PROPERTY') {
			filter = function (p) {
				return p && p.utype === 'Plain';
			}
		}

		return filter;
	}

	function isOperandEditable(value) {
		return value !== 'INPUT' && value !== 'OUTPUT' && value !== 'PROPERTY';
	}

	function loadOperandStoreByParameters(store, operandName, parameters) {
		var operandNameIsValid = true;

		store.accessors([
			function (p) { return p.uname; },
			function (p) { return p.uname; }
		]);

		operandNameIsValid = loadDynamicOperandStore(store, operandName, parameters);
		store.filterBy(function (p) {
			return p && p.utype === 'INPUT' || p.utype === null;
		});
		return operandNameIsValid;
	}

	function loadOperandStoreByProperties(store, operandName, properties) {
		var operandNameIsValid = true;

		store.accessors([
			function (p) { return p.uname; },
			function (p) { return p.uname; }
		]);

		operandNameIsValid = loadDynamicOperandStore(store, operandName, properties);
		store.filterBy(function (p) {
			return p && p.utype === 'Plain';
		});
		return operandNameIsValid;
	}

	function loadDynamicOperandStore (store, operandName, reportedData) {
		var memberMap = {},
			current = store.getData();

		for (var i = 0; i < reportedData.length; i++) {
			memberMap[reportedData[i].uname] = {
				isReported: true,
				isCurrent: false
			};
		}

		for (var i = 0; i < current.length; i++) {
			if (memberMap[current[i].value]) {
				memberMap[current[i].value].isCurrent = true;
			} else {
				memberMap[current[i].value] = {
					isReported: false,
					isCurrent: true
				}
			}
		}

		for (var key in memberMap) {
			var m = memberMap[key];

			if (m.isReported && !m.isCurrent) {
				store.add({
					text: key,
					value: key
				});
			} else if (!m.isReported && m.isCurrent) {
				store.removeBy('value', key);
			}
		}

		var operandIsValid = false;

		if (operandName && memberMap[operandName]) {
			operandIsValid = memberMap[operandName].isReported;
		}

		return operandIsValid;
	}

	function modifyOperandTypesByParameters(parameters, parameterType) {
		var typeExists = parameters.length > 0;
		if(parameterType == 'INPUT')
			spliceOperandType(typeExists, 'INPUT', 1, operandTypes);
		else 
			spliceOperandType(typeExists, 'OUTPUT', 1, operandTypes);
		return typeExists;
	}
	
	function modifyOperandTypesByProperties(properties) {
		var i = 0,
			propertyTypeName = operandTypesMap['PROPERTY'],
			propertyTypeExists = false;

		while (!propertyTypeExists && i < properties.length) {
			propertyTypeExists = properties[i].utype === propertyTypeName;
			i++;
		}

		spliceOperandType(propertyTypeExists, 'PROPERTY', 4, operandTypes);
		return propertyTypeExists;
	}

	function spliceOperandType (operandTypeExists, operandType, desiredIndex, operandTypes) {
		if (operandTypeExists) {
			var found = false;

			for (i = 0; i < operandTypes.length; i++) {
				if (operandTypes[i] === operandType) {
					found = true;
					break;
				}
			}

			if (!found) {
		    	operandTypes.splice(desiredIndex, 0, operandType);
			}
		} else {
			var foundAtIndex = -1;

			for (i = 0; i <= desiredIndex; i++) {
				if (operandTypes[i] === operandType) {
					foundAtIndex = i;
					break;
				}
			}

			if (foundAtIndex >= 0) {
				operandTypes.splice(foundAtIndex, 1);
			}
		}
	}

	function updateExpression(expression) {
		var expressionData = [expression.criteria, ' '];
		var expressionOperatorStoreIndex = expression.operatorStore.find('value', expression.operator);
		if(expressionOperatorStoreIndex != -1)
			var expressionOperatorName = expression.operatorStore.getAt(expressionOperatorStoreIndex).get('text');
		else
			expressionOperatorName = '';
		if (expression.leftOperandType !== 'CONSTANT' && expression.rightOperandType !== null) {
			expressionData.push(expression.leftOperandType);
			expressionData.push('.');
		}

		expressionData.push(expression.leftOperandName);
		expressionData.push(' ');
		expressionData.push(expressionOperatorName);
		expressionData.push(' ');

		if (expression.rightOperandType !== 'CONSTANT' && expression.rightOperandType !== null) {
			expressionData.push(expression.rightOperandType);
			expressionData.push('.');
		}

		expressionData.push(expression.rightOperandName);
		return expressionData.join('');
	}

	var operandTypesMap = {
		CONSTANT: 'CONSTANT',
		INPUT: 'INPUT',
		FLOW: 'FLOW',
		PARAM: 'PARAM',
		WSDATA: 'WSDATA',
		PROPERTY: 'Plain'
	};

	return {
		fields : ['category','leftOperandType','leftOperandName','operator','rightOperandType','rightOperandName'],
		key: 0,
		id: '',
		priorData: null,
		isReadOnly: false,	
		allParameters : {
			INPUT : [],
			OUTPUT : [],
			FLOW : [],
			WSDATA : [],
			PROPERTY : []
		},		
		propertiesUpdated: false,
		expression: '',
		determinerStore: null,
		operandTypeStore: null,		
		category: 0,
		categoryStore : null,
		operatorStore: {
			mtype : 'store',
			fields : ['text', 'value'],
			proxy : {
				type : 'memory'
			},
			data : [{
				text : '=',
				value : '='
			},{
				text :'!=',
				value : '!='
			},{
				text : '>',
				value : '>'
			},{
				text : '>=',
				value : '>='
			},{
				text : '<',
				value : '<'
			},{
				text : '<=',
				value : '<='
			},{
				text : 'contains',
				value : 'contains'
			},{
				text : 'not contains',
				value : 'notContains'
			},{
				text : 'starts with',
				value : 'startsWith'
			},{
				text : 'ends with',
				value : 'endsWith'
			}]
		},
		criteria: 'ANY',		
		leftOperandStore : {
			mtype : 'store',
		    fields: ['name']	   	
		},
		rightOperandStore : {
			mtype : 'store',
		    fields: ['name']		  
		},
		isRemoveHidden: false,
		isOrHidden: false,
		isAddHidden: true,
		isExpressionHidden: true,
		isFormHidden: true,
		addMode: false,		
	
		when_left_operand_type_changed : {
			on : ['leftOperandTypeChanged'],
			action : function(){
				var currentType = this.leftOperandType;
				this.set('leftOperandName', null);
				this.loadOperandNameStore(this.leftOperandStore, currentType);
			}
		},			
		when_right_operand_type_changed : {
			on : ['rightOperandTypeChanged'],
			action : function(){
				var currentType = this.rightOperandType;
				this.set('rightOperandName', null);
				this.loadOperandNameStore(this.rightOperandStore, currentType);				
			}
		},

		init: function () {
			this.key = new Date().getTime();
		},

		leftOperandIsText$: function(){
			return this.leftOperandType === 'CONSTANT' || this.leftOperandType == 'PARAM';
		},
		leftOperandEditable$ : function(){
			return this.leftOperandType === 'WSDATA' || this.leftOperandType == 'FLOW';
		},
		rightOperandIsText$: function(){
			return this.rightOperandType === 'CONSTANT' || this.rightOperandType == 'PARAM';
		},
		rightOperandEditable$ : function(){
			return this.rightOperandType === 'WSDATA' || this.rightOperandType == 'FLOW';
		},

		//Validation
		leftOperandTypeIsValid$ : function(){
			return this.leftOperandType ? true : this.localize('requiredField'); 
		},
		leftOperandNameIsValid$ : function(){
			return this.leftOperandName ? true : this.localize('requiredField'); 
		},
		rightOperandTypeIsValid$ : function(){
			return this.rightOperandType ? true : this.localize('requiredField'); 
		},
		rightOperandNameIsValid$ : function(){
			return this.rightOperandName? true : this.localize('requiredField'); 
		},
		operatorIsValid$ : function(){
			return this.operator ? true : this.localize('requiredField'); 
		},
		
		markParametersUpdated: function () {
			this.parametersUpdated = true;
		},

		markPropertiesUpdated: function () {
			this.propertiesUpdated = true;
		},

		initExpression: function (allParameters, allProperties, readOnly) {
			this.set('isReadOnly', !!readOnly);
			this.set('determinerStore', new RS.common.data.FlatStore(['ANY', 'ALL']));
			this.set('operandTypeStore', new RS.common.data.FlatStore(operandTypes));
			var categoryStore = new RS.common.data.FlatStore();

			if(this.parentVM.parentVM.parentVM.expressionType == "condition"){
				categoryStore.loadData([{
					text: 'BAD',
					value: 1
				}, {
					text: 'GOOD',
					value: 0
				}]);
			} else {
				categoryStore.loadData([{
					text: 'CRITICAL',
					value: 3
				}, {
					text: 'SEVERE',
					value: 2
				}, {
					text: 'WARNING',
					value: 1
				}, {
					text: 'GOOD',
					value: 0
				}]);
			}

			this.set('categoryStore', categoryStore);
			this.updateParameters(allParameters);		
			this.set('expression', updateExpression(this));
		},		
		loadOperandNameStore : function(store, dataType){
			if(dataType == 'INPUT')
				store.loadData(this.allParameters['INPUT']);
			else if(dataType == 'OUTPUT')
				store.loadData(this.allParameters['OUTPUT']);
			else if(dataType == 'FLOW')
				store.loadData(this.allParameters['FLOW']);
			else if(dataType == 'WSDATA')
				store.loadData(this.allParameters['WSDATA']);
			else if(dataType == 'PROPERTY')
				store.loadData(this.allParameters['PROPERTY']);
		},
		
		enableRemove: function () {
			this.set('isOrHidden', false);

			if (!this.isReadOnly) {
				this.set('isRemoveHidden', false);
			}
		},

		disableRemove: function () {
			this.set('isOrHidden', true);
			this.set('isRemoveHidden', true);
		},

		enableAdd: function () {
			this.set('isAddHidden', false);
		},

		disableAdd: function () {
			this.set('isAddHidden', true);
		},

		showExpression : function(){
			this.set('isExpressionHidden', false);
			this.set('isFormHidden', true);
			this.disableAdd();
		},

		getData: function (expressionType, order) {
			return {
				id: this.id,
				expressionType: expressionType || null,
				criteria: this.criteria,
				leftOperandType: this.leftOperandType,
				leftOperandName: this.leftOperandName,
				operator: this.operator,
				rightOperandType: this.rightOperandType,
				rightOperandName: this.rightOperandName,
				resultLevel: this.category || 0,
				order: order || 0
			};
		},

		add: function () {
			this.set('addMode', true);
			this.disableAdd();
			this.edit();
		},

		edit: function () {
			if (!this.isReadOnly) {
				this.priorData = this.getData();			
				this.set('isExpressionHidden', true);
				this.set('isFormHidden', false);
				this.disableRemove();
			}
		},
		

		cancel: function () {
			this.set('criteria', this.priorData.criteria);
			this.set('leftOperandType', this.priorData.leftOperandType);
			this.set('leftOperandName', this.priorData.leftOperandName);
			this.set('operator', this.priorData.operator);
			this.set('rightOperandType', this.priorData.rightOperandType);
			this.set('rightOperandName', this.priorData.rightOperandName);
			this.set('isFormHidden', true);		
		
			if (this.isLast()) {
				this.enableAdd();
				this.disableRemove();
				this.set('isExpressionHidden', true);
			} else {
				this.enableRemove();
				this.set('isExpressionHidden', false);
			}

			this.set('addMode', false);
		},

		done: function (btn) {
			if(!this.isValid){
				this.set('isPristine', false);
				return;
			}
			if (this.isLast()) {
				this.parentVM.addEmpty();
			}

			this.set('expression', updateExpression(this));
			this.set('isFormHidden', true);
			this.set('isExpressionHidden', false);
			this.enableRemove();

			if (this.parentVM.isLastBlock() && this.parentCount() > 1) {
				this.parentVM.addEmptyBlock();
			}

			this.parentVM.checkExpressionCategory(this);
			this.set('addMode', false);
			this.parentVM.parentVM.parentVM.blockModel.notifyDirty();
		},

		parentCount: function () {
			return this.parentVM.count();
		},

		remove: function () {
			this.parentVM.remove(this);
			this.parentVM.parentVM.parentVM.blockModel.notifyDirty();
		},

		updateParameters: function (data) {
			for(var type in data){
				this.allParameters[type] = data[type];
			}
			//Repopulate store for each operands
			this.loadOperandNameStore(this.leftOperandStore, this.leftOperandType);
			this.loadOperandNameStore(this.rightOperandStore, this.rightOperandType);
		},	

		isLast: function () {
			return this.parentVM.isLastExpression(this);
		}
	};
})());
glu.defModel('RS.actiontaskbuilder.ExpressionBlock', {
	key: 0,
	allParameters : {},
	isReadOnly: false,
	category: 0,
	expressions: {
		mtype: 'list'

	},
	init: function () {
		this.key = new Date().getTime();
	},

	initBlock: function (readOnly) {
		this.isReadOnly = !!readOnly;
	},

	checkExpressionCategory: function(expression){
		if(this.parentVM.isCategoryForExpressionChanged(expression.category)){
			//Copy to new location then remove.			
			this.parentVM.reclassifyExpression(expression);
			this.remove(expression);
		}
	},

	updateCategoryForExpression: function(expression, order){
		var expressionConfig = expression.getData(expression.expressionType, order);
		expressionConfig.category = this.category;
		var expression = this.add(expressionConfig, 0);
		expression.showExpression();
		this.addEmptyBlock();
	},

	getData: function (expressionType,order) {
		var expressions = this.expressions.toArray(),
			data = [],
			indexOfLastExpression = expressions.length - 1;

		if (!this.isReadOnly) {
			indexOfLastExpression--;
		}

		for (var i = 0; i <= indexOfLastExpression; i++) {
			data.push(expressions[i].getData(expressionType, order));
		}

		return data;
	},

	addEmpty: function () {
		var expression = this.model('RS.actiontaskbuilder.Expression', {
			criteria: 'ANY',
			leftOperandType: null,
			leftOperandName: null,
			operator: null,
			rightOperandType: null,
			rightOperandName: null,
			isRemoveHidden: true,
			isOrHidden: true,
			isAddHidden: false,
			isExpressionHidden: true,
			isFormHidden: true,
			category : this.category
		});
		expression.initExpression(this.allParameters, this.properties, this.isReadOnly);
		this.expressions.add(expression);
	},

	add: function (expressionConfig, position) {
		var expression = this.model('RS.actiontaskbuilder.Expression', expressionConfig);
		expression.set('category', expressionConfig.resultLevel);
		expression.initExpression(this.allParameters, this.properties, this.isReadOnly);

		if (position || position === 0) {
			this.expressions.insert(position, expression);
		} else {
			this.expressions.add(expression);
		}

		return expression;
	},	

	remove: function (expression) {		
		this.expressions.remove(expression);
		
		if (this.count() === 1) {
			this.parentVM.removeBlock(this);
		}
	},

	clear: function(){
		this.expressions.removeAll();
	},
	addEmptyBlock: function () {
		var block = this.parentVM.addBlock();

		if (!block.isReadOnly) {
			block.addEmpty();
		}
	},

	updateParameters: function (allParameters) {
		this.allParameters = allParameters;
		this.expressions.foreach(function(expression){
			expression.updateParameters(allParameters);
		},this)
	},

	updateProperties: function (allProperties) {
		this.properties = allProperties;	
		var expressions = this.expressions.toArray();

		for (var i = 0; i < expressions.length; i++) {
			expressions[i].markPropertiesUpdated();
			expressions[i].updateProperties(allProperties);
		}
	},

	isLastExpression: function (expression) {
		var count = this.count();
		return count >= 0 && count - 1 === this.expressions.indexOf(expression);
	},

	isLastBlock: function () {
		return this.parentVM.isLastBlock(this);
	},

	isEmpty: function () {
		return this.count() === 0;
	},

	count: function () {
		return this.expressions.length;
	}
});
glu.defModel('RS.actiontaskbuilder.ExpressionBlockContainer', {
	allParameters : {
		INPUT : [],
		OUTPUT : [],
		FLOW : [],
		WSDATA : [],
		PROPERTY : []
	},	
	category: 0,
	isReadOnly: false,
	title: '',
	style: {
		borderColor: '#f00'
	},
	isAndHidden: true,
	isReadOnly: false,
	isHidden: false,
	blocks: {
		mtype: 'list'
	},

	initContainer: function (level, title, color, readOnly) {
		this.category = level;
		this.set('title', this.localize(title));
		this.set('style', { borderColor: color });	
		this.isReadOnly = !!readOnly;

		if (this.isReadOnly) {
			this.set('isHidden', true);
		}
	},

	isCategoryForExpressionChanged : function(expressionCategory){
        return expressionCategory != this.category;
    },

    reclassifyExpression : function(expression){
    	this.parentVM.reclassifyExpression(expression);
    },

    updateCategoryForExpression : function(expression){
    	var order = this.blocks.length - 1;
    	var lastExpressionBlock = this.blocks.getAt(order);
    	lastExpressionBlock.updateCategoryForExpression(expression, order);
    },

	load: function (expression, order) {
		expression.isExpressionHidden = false;

		while (this.count() <= order) {
			this.addBlock();
		}

		var blocks = this.blocks.toArray();
		blocks[order].add(expression);

		if (this.isHidden) {
			this.set('isHidden', false);
		}
	},

	enableAdd: function () {
		this.addBlock();
		var blocks = this.blocks.toArray();

		for (var i = 0; i < blocks.length; i++) {
			blocks[i].addEmpty();				
		}
	},

	getData: function (expressionType) {
		var data = [];
		var blocks = this.blocks.toArray();
		var indexOfLastBlock = this.count() - 1;

		if (!this.isReadOnly) {
			indexOfLastBlock--;
		}

		for (var i = 0; i <= indexOfLastBlock; i++) {
			data.push.apply(data, blocks[i].getData(expressionType, i));
		}

		return data;
	},

	addBlock: function () {
		var block = this.model('RS.actiontaskbuilder.ExpressionBlock', { category: this.category });
		block.initBlock(this.isReadOnly);
		block.updateParameters(this.allParameters);
		block.updateProperties(this.properties);
		this.blocks.add(block);
		this.set('isAndHidden', this.count() < 2);
		return block;
	},

	removeBlock: function (block) {
		this.blocks.remove(block);
		this.set('isAndHidden', this.count() < 2);
	},

	updateParameters: function (data) {
		for(var type in data){
			var transformedData = [];
			for(var i = 0; i < data[type].length; i++){
				transformedData.push({
					name : data[type][i]
				})
			}
			this.allParameters[type] = transformedData;
		}
		this.blocks.foreach(function(block){
			block.updateParameters(this.allParameters);
		},this)
	},

	updateProperties: function (allProperties) {
		this.properties = allProperties;
		var items = this.blocks.toArray();

		for (var i = 0; i < items.length; i++) {
			items[i].updateProperties(allProperties);
		}
	},

	clear: function () {	
		this.blocks.foreach(function(block){
			block.clear();
		})
		this.blocks.removeAll();
		this.fireEvent('onClear');
		this.set('isAndHidden', true);
	},

	isEmpty: function () {
		return this.count() === 0;
	},

	count: function () {
		return this.blocks.length;
	},

	isLastBlock: function (block) {
		var count = this.count();
		return count >= 0 && count - 1 === this.blocks.indexOf(block);
	}
});
Ext.define('RS.actiontaskbuilder.GeneralEditBlockModel', {
	extend: 'RS.common.blocks.BlockModel',
	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'General',
		hidden: true,
		collapsed: true,
		editBlock: true,
		stopPropagation: false,
		header: {
	 		xtype: 'toolbar',
           	margin: 0,
	    	padding: 0,
	    	items: []
	 	},
        title: '',
		typeStore: null,
		type: 'ASSESS',
		id: '',
		namespace: '',
		name: '',
		menuPath: '/',
		timeout: 300,
		assignedToUserName: '',
		assignedToUName: '',
		assignedTo: null,
		summary: '',
		description: '',
		active: true,
		logResult: true,
		displayInWorksheet: true,
		isExistingActionTask: false,
		isNewActionTask: true,
		isTypeComboHidden: false,
		isProcessFieldHidden: true,
		autoMenuPathEnabled: true,
		jumpToAssignedToIsHidden : true,
		ufullName: '',
		wizardType : '',
		form: null,
		blockWizard: true,
		fields : ['timeout'],
		generalEditTabContrl: false,
		hideShowGeneralEditTab: function(atId) {
			this.set('generalEditTabContrl', !this.generalEditTabContrl);
		},
        validityChange: function (valid) {
        	//When form is not valid, block navigation and set section invalid.
        	this.set('blockWizard', !valid);
        	this.notifyValid(valid);
        },

		postInit: function (blockModel) {
			this.blockModel = blockModel;
			this.set('typeStore', new RS.common.data.FlatStore(['ASSESS', 'REMOTE', 'OS', 'BASH', 'CMD', 'CSCRIPT', 'POWERSHELL']));

			if (typeof this.parentVM.id === "string" && this.parentVM.id.length === 0) {
				var user = clientVM.user,
					fullName = user.fullName.trim(),
					username = '';

				if (fullName.length > 0) {
					username = fullName;
				} else {
					username = user.name.trim();
				}

				this.set('assignedToUserName', username);
				this.set('assignedToUName', user.name);
				this.set('assignedTo', {
					id: user.id,
					udisplayName: username,
					uuserName: user.name
				});
			}

			this.header.items = [{
	        	xtype: 'component',
	        	padding: '0 0 0 15',
	        	cls: 'x-header-text x-panel-header-text-container-default',
	        	html: this.localize('generalTitle'),
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
			            }, this);
					}.bind(this)
	    		}
	        },{
	        	xtype: 'container',
	        	autoEl: 'ol',
	        	cls: 'breadcrumb flat',
	        	margin: 0,
	        	padding: 0,
	        	items: [{
		    		xtype: 'component',
					itemId: 'generalProperties',
		    		autoEl: 'li',
		    		cls: 'active',
		    		html: this.localize('generalPropertiesEditTitle'),
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
							}, this);
						}.bind(this)
					}
		    	}, {
		    		xtype: 'component',
					itemId: 'rolesEdit',
		    		autoEl: 'li',
		    		html: this.localize('rolesEditTitle'),
		    		listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								if (!this.blockWizard) {
				                	this.jumpTo('RolesEdit', this.parentVM.rolesEdit.gluModel);
				            	}
				            }, this);
						}.bind(this),
		    		}
		    	}, {
		    		xtype: 'component',
					itemId: 'optionsEdit',
		    		autoEl: 'li',
					html: this.localize('optionsEditTitle'),
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								if (!this.blockWizard) {
									this.jumpTo('OptionsEdit', this.parentVM.optionsEdit.gluModel);
				            	}
				            }, this);
						}.bind(this)
		    		}
		        }]
	        }, '->', {
				xtype: 'button',
				tooltip: this.localize('resizeFullscreen'),
				iconCls: 'rs-block-button icon-resize-full',
				isResizeMaxBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['GENERAL']);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				tooltip: this.localize('resizeNormalscreen'),
				iconCls: 'rs-block-button icon-resize-small',
				hidden: true,
				isResizeNormalBtn: true,
				margin: '0 20',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeNormalSection(this);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				iconCls: 'rs-block-button icon-chevron-sign-up',
				isCollapseBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.set('collapsed', true);
						}, this);
					}.bind(this)
				}
			}];
		},

		setForm: function (form) {
			this.form = form;
		},

		jumpToAssignedTo: function() {
			clientVM.handleNavigation({
				modelName: 'RS.user.User',
				params: {
					username: this.assignedToUName
				},
				target: '_blank'
			})
		},

		assignedToClicked: function() {
			this.open({
				mtype: 'RS.user.UserPicker',
				pick: true,
				callback: this.assignedToSelected
			})
		},

		assignedToSelected: function(records) {
			var user = records[0].data;
			this.set('assignedTo', user);
			this.set('assignedToUserName', user.udisplayName || (user.ufirstName + ' ' + user.ulastName).trim() || user.uuserName);
			this.set('assignedToUName', user.uuserName);
			this.form.isValid();
		},
		jumpToAssignedToIsHidden$ : function(){
			return !this.assignedToUName;
		},

		periodRegex: /\./g,
		illegalNamespaceCharacters: /[^A-Za-z0-9\./_ ]/g,
		nameIsValid$: function() {
	        return RS.common.validation.TaskName.test(this.name) ? true : this.localize('invalidName');
	    },
	    namespaceIsValid$: function() {
	        return RS.common.validation.TaskNamespace.test(this.namespace) ? true : this.localize('invalidNamespace');
	    },
		namespaceChangeHandler: function (field, newValue) {
			this.setLegalNamespace(newValue);
			field.checkChange();
		},

		saveBtnIsEnabled$: function() {
			var newActionTaskIsValid = this.nameIsValid == true && this.namespaceIsValid == true;
			if (newActionTaskIsValid) {
				this.parentVM.set('isValid', true);
			} else {
				this.parentVM.set('isValid', false);
			}
			return newActionTaskIsValid;
		},

		namespaceBlurHandler: function () {
			this.setLegalNamespace(this.namespace);
		},

		setLegalNamespace: function (namespace) {		
			if (namespace === '' && this.menuPath === '/') {
				this.autoMenuPathEnabled = true;
			}
			this.generateMenuPath();
		},

		generateMenuPath: function () {
			if (this.autoMenuPathEnabled) {
				var menuPath = this.namespace.replace(this.periodRegex, '/');

				if (menuPath.length === 0 || menuPath[0] !== '/') {
					menuPath = '/' + menuPath;
				}

				this.set('menuPath', menuPath);
			}
		},

		menuPathChangeHandler: function (newValue) {
			this.setLegalMenuPath(newValue);
		},

		menuPathBlurHandler: function (field) {
			this.setLegalMenuPath(this.menuPath);
			field.validate();
		},

		setLegalMenuPath: function (menuPath) {
			if (this.namespace === '' && menuPath === '/') {
				this.autoMenuPathEnabled = true;
			} else {
				this.autoMenuPathEnabled = false;
			}

			if (menuPath.length === 0) {
				menuPath = '/';
			} else if (menuPath[0] !== '/') {
				menuPath = '/' + menuPath;
			}

			menuPath = menuPath.replace(this.illegalNamespaceCharacters, '').replace(this.periodRegex, '/');

			if (menuPath !== this.menuPath) {
				this.set('menuPath', menuPath);
			}
		},

		notify_dirty : {
			on: ['typeChanged','activeChanged','logResultChanged','displayInWorksheetChanged','assignedToChanged','menuPathChanged'],
			action: function () {
				this.notifyDirty();
			}
		},

		when_type_changed: {
			on: ['typeChanged'],
			action: function() {
				if (this.type === 'ASSESS') {
					this.parentVM.set('contentSectionVisible', false);
				} else {
					this.parentVM.set('contentSectionVisible', true);
					if (this.parentVM.isMaxScreenSize) {
						this.parentVM.blockContainer.hideBlocks('contentBlock');
					}
				}
			}
		},

		notifyDirty: function() {
			this.blockModel.notifyDirty();
		},

		notifyValid: function (valid) {
			this.blockModel.notifyValid(valid);
		},

		jumpTo: function(nextBlockName, nextSection){
			this.parentVM.generalEditSectionExpanded = !this.collapsed;
			this.parentVM.generalEditSectionMaximized = this.isSectionMaxSize;
			if (this.id) {
				this.parentVM.collapseAndHideSection(this);
				this.parentVM.showAndExpandThisSection(nextSection);
			} else{
				this.parentVM.doSave(function(payload){
					this.parentVM.creatingNewTask = true;
					this.parentVM.modifyHash(payload.data.id);
					this.collapseAndHideSectionFlag = true;
					this.set('collapsed', true);

					setTimeout(function() {
						this.parentVM.resizeMaxSection(nextSection, this.parentVM.sectionButtonMap['GENERAL']);
					}.bind(this), 100);

				}.bind(this));
			}
		},

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden) {
					setTimeout(function() {
						if (this.parentVM.generalEditSectionExpanded) {
							this.parentVM.generalEditSectionExpanded = null;
							this.set('collapsed', false);
						}
						if (this.parentVM.generalEditSectionMaximized) {
							this.parentVM.generalEditSectionMaximized = null;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['GENERAL']);
						}
					}.bind(this), 10);
				}
			}
		},

		closeFlag: false,
		close: function () {
			if (this.isNewActionTask) {
				this.parentVM.doSave(function(payload){
					this.parentVM.creatingNewTask = true;
					this.parentVM.modifyHash(payload.data.id);

					//this.closeFlag = true;
					//this.collapseAndHideSectionFlag = true;
					this.set('collapsed', true);

					setTimeout(function() {
						this.parentVM.resizeNormalSection(this);
						this.parentVM.edit();
					}.bind(this), 100);

				}.bind(this));
			} else {
				if (this.isSectionMaxSize) {
					this.parentVM.resizeNormalSection(this);
				}
				this.closeFlag = true;
				this.set('collapsed', true);
			}
		}
	},

	getDataPaths: function () {
		return ['data', 'data.resolveActionInvoc', 'data.assignedTo'];
	},

	getData: function () {
		var g = this.gluModel,
		data = {
			id: g.id,
			unamespace: g.namespace,
			uname: g.name,
			ufullName: g.ufullName,
			umenuPath: g.menuPath,
			usummary: g.summary,
			udescription: g.description,
			uactive: g.active,
			ulogresult: g.logResult,
			uisHidden: !g.displayInWorksheet,
			wizardType : g.wizardType
		};

		var resolveActionInvoc = {
			utype: g.type,
			utimeout: g.timeout
		};

		return [data, resolveActionInvoc, g.assignedTo];
	},

	load: function (data, resolveActionInvoc, assignedTo) {

		var g = this.gluModel,
			typeStore = g.typeStore,
			type = resolveActionInvoc.utype.toUpperCase();

		if (type === 'PROCESS') {
			if (g.isProcessFieldHidden || !g.isTypeComboHidden) {
				this.set('isProcessFieldHidden', false)
					.set('isTypeComboHidden', true);
			}

			typeStore.removeBy('value', 'EXTERNAL');
		} else if (type === 'EXTERNAL') {
			if (typeStore.findExact('value', 'EXTERNAL') === -1) {
				typeStore.add({
					text: 'EXTERNAL',
					value: 'EXTERNAL'
				});
			}
		} else {
			typeStore.removeBy('value', 'EXTERNAL');

			if (!g.isProcessFieldHidden || g.isTypeComboHidden) {
				this.set('isProcessFieldHidden', true)
					.set('isTypeComboHidden', false);
			}
		}
		g.autoMenuPathEnabled = data.unamespace === '' && data.umenuPath === '/';

		this.set('id', data.id)
			.set('namespace', data.unamespace)
			.set('name', data.uname)
			.set('isExistingActionTask', data.id.length > 0)
			.set('isNewActionTask', data.id.length === 0)
			.set('menuPath', data.umenuPath)
			.set('summary', data.usummary)
			.set('description', data.udescription)
			.set('active', data.uactive)
			.set('logResult', data.ulogresult)
			.set('displayInWorksheet', !data.uisHidden)
			.set('type', type)
			.set('timeout', resolveActionInvoc.utimeout || 1)
			.set('ufullName', data.uname + '#' + data.unamespace)
			.set('wizardType', data.wizardType);

		if (assignedTo) {
			this.set('assignedToUserName', assignedTo.udisplayName || assignedTo.name)
				.set('assignedToUName', assignedTo.uuserName)
				.set('assignedTo', assignedTo);
		}
		else
		{
			this.set('assignedToUserName', '')
				.set('assignedToUName', '')
				.set('assignedTo', null)
		}

		if (g.form) {
			g.form.isValid();
		}
	}
});

Ext.define('RS.actiontaskbuilder.GeneralReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',
	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'General',
		hidden: false,
		collapsed: true,
        title: '',
        header: {
            titlePosition: 0,
            defaults: {
            	margin: 0
        	},
            items: []
        },
		type: '',
		id: '',
		namespace: '',
		name: '',
		timeout: 300,
		assignedToUserName: '',
		assignedToUName: '',
		summary: '',
		description: '',
		active: true,
		logResult: true,
		displayInWorksheet: true,
		jumpToAssignedToIsHidden: true,

		// roles
		accessStore: {
			mtype : 'store',
			fields: ['uname', 'read', 'write', 'execute', 'admin'],
			data: []
		},
		hideAdminColumn: true,
		defaultRoles: null,
		hideAccessRights: true,
		defaultRolesMessage: '',
		hideAccessRights: true,
		// end roles

		// options
		optionsStore: null,
		hasOptions: false,
		// end options

		postInit: function (blockModel) {
			this.blockModel = blockModel;
			var hidden = true;

			if (clientVM.user.roles === 'undefined' || typeof clientVM.user.roles === null) {
				// resolve.maintenance user
				hidden = false;
			} else {
				if (clientVM.user.roles && clientVM.user.roles.constructor === Array) {
					hidden = clientVM.user.roles.indexOf('admin') === -1;
				}
			}

			this.set('hideAdminColumn', hidden);
			this.set('title', this.localize('rolesReadTitle'));
			this.set('defaultRolesMessage', this.localize('defaultRolesMessage'));
			this.set('optionsStore', new RS.common.data.FlatStore({
				fields: ['uname', 'uvalue', 'udescription']
			}));
		},

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden && this.parentVM.embed) {
					var generalData = this.parentVM.generalEdit.getData(),
						accessRightsData = this.parentVM.rolesEdit.getData(),
						optionsData = this.parentVM.optionsEdit.getData();

					var data = generalData[0],
						resolveActionInvoc = generalData[1],
						assignedTo = generalData[2],
						accessRights = accessRightsData[1],
						options = optionsData[0];

					Ext.apply(data, {
						uisDefaultRole: accessRightsData[0].uisDefaultRole,
					});

					this.parentVM.generalRead.load(data, resolveActionInvoc, assignedTo, accessRights, options);
				}
			}
		},

		adminRights: function (column) {
			if (this.hideAdminColumn) {
				column.hide();
			} else {
				column.show();
			}
		},

		jumpToAssignedTo: function() {
			clientVM.handleNavigation({
				modelName: 'RS.user.User',
				params: {
					username: this.assignedToUName
				},
				target: '_blank'
			})
		},

		jumpToAssignedToIsHidden$ : function(){
			return !this.assignedToUName;
		}
	},

	getDataPaths: function () {
		return [
			'data', 'data.resolveActionInvoc', 'data.assignedTo', // general
			'data.accessRights', // roles
			{ // options
				path:'data.resolveActionInvoc.resolveActionInvocOptions',
				filter: function (o) {
					return o.uname !== 'INPUTFILE';
				}
			}];
	},

	load: function (data, resolveActionInvoc, assignedTo, accessRights, options) {
		assignedTo = assignedTo || {};

		var desc = data.udescription || '',
			timeout = resolveActionInvoc.utimeout || 1,
			secondMessage = '',
			displayName = assignedTo.udisplayName || '',
			g = this.gluModel;

		if (desc.length === 0) {
			desc = g.localize('noDescription');
		} else {
			desc = Ext.String.htmlEncode(desc).replace(/(\r\n|\n|\r)/g, '<br />');
		}

		if (timeout === 1) {
			secondMessage = g.localize('timeoutValueSingular');
		} else {
			secondMessage = g.localize('timeoutValue');
		}

		if (displayName.length === 0) {
			displayName = g.localize('noAssignedUser');
		}

		this.set('description', desc)
			.set('id', data.id)
			.set('namespace', data.unamespace)
			.set('name', data.uname)
			.set('summary', data.usummary)
			.set('active', data.uactive)
			.set('logResult', data.ulogresult)
			.set('displayInWorksheet', !data.uisHidden)
			.set('type', resolveActionInvoc.utype.toUpperCase())
			.set('timeout', [timeout, secondMessage].join(' '))
			.set('assignedToUserName', displayName)
		    .set('assignedToUName', assignedTo ? assignedTo.uuserName : '');

		// roles
		this.set('defaultRoles', data.uisDefaultRole);
		var data = accessRights;
		if (accessRights.hasOwnProperty('ureadAccess'))
			data = this.shapeData(accessRights);
		g.accessStore.loadData(data);
		this.set('hideAccessRights', g.accessStore.count() === 0);

		// options
		g.optionsStore.loadData(options);
		this.set('hasOptions', options.length > 0);
	},
	shapeData : function(accessRights) {
		function consolidateArrays(destination, source) {
			for (var i = 0; i < source.length; i++) {
				if (destination.indexOf(source[i]) === -1) {
					destination.push(source[i]);
				}
			}
		}
		var data = [];

		if (accessRights) {
			var	readUsers = accessRights.ureadAccess.length > 0 ? accessRights.ureadAccess.split(',') : [],
				writeUsers = accessRights.uwriteAccess.length > 0 ? accessRights.uwriteAccess.split(',') : [],
				executeUsers = accessRights.uexecuteAccess.length > 0 ? accessRights.uexecuteAccess.split(',') : [],
				adminUsers = accessRights.uadminAccess.length > 0 ? accessRights.uadminAccess.split(',') : [],
				users = readUsers.slice();
			consolidateArrays(users, writeUsers);
			consolidateArrays(users, executeUsers);
			consolidateArrays(users, adminUsers);

			for (var i = 0; i < users.length; i++) {
				var u = users[i];
				data.push({
					uname: u,
					read: readUsers.indexOf(u) !== -1,
					write: writeUsers.indexOf(u) !== -1,
					execute: executeUsers.indexOf(u) !== -1,
					admin: adminUsers.indexOf(u) !== -1
				});
			}
		}

		return data;
	}
})

glu.defModel('RS.actiontaskbuilder.GeneralReference', {

	ufullname: 'New',

	activate: function(screen, params) {
		this.set('id', params ? params.id || '' : '');
		this.referencesStore.removeAll();
		this.show();
	},

	referenceTitle$: function() {
		return this.localize('referenceTitle', this.ufullname);
	},

	show: function() {
		 this.ajax({
			url: '/resolve/service/actiontask/get',
			method: 'post',
			params: {
				id: this.id,
				name: ''
			},
			scope: this,
			success: function (resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(respData.message);
				}
				else {
					this.referencesStore.removeAll();
					if (respData.data.referencedIn) {
						this.referencesStore.add(respData.data.referencedIn);
					}
					this.set('ufullname', respData.data.ufullName);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	referencesStore: {
		mtype: 'store',
		fields: ['name', 'refInContent', 'refInMain', 'refInAbort'],
		proxy: {
			type: 'memory'
		}
	},

	editReference: function(docName) {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			params: {
				name: docName
			},
			target: '_blank'
		})
	}
});

glu.defModel('RS.actiontask.ActionTask', {
	itemList : { mtype : 'list'	},

	//for distinguishing new and edit
	name: '',
	namespace: '',
	id: '',
	version: '',
	ufullName: '',
	uversion: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	uname: '',
	unamespace: '',
	saveIsEnabled: false,
	saveAsRenameIsEnabled: false,
	editable: false,
	executable: false,
	embed: false,
	generalIsValid: false,
	uisStable: false,
	saveSuccess: '',
	saveAsTitle: '',
	requestFailedMessage: '',
	reloadWithoutSaveTitle: '',
	reloadWithoutSaveBody: '',
	confirmButtonText: '',
	cancelButtonText: '',
	actionTaskTitle: 'Action Task',
	yesButtonText: '',
	noButtonText: '',
	editNewTask: '',
	editNewActionTaskTitle: '',
	editNewActionTaskMessage: '',
	newActionTaskTitle: '',
	actionTaskTitle: '',
	blockContainer: null,
	generalBlock: null,
	generalRead: null,
	generalEdit: null,
	referencesRead: null,
	contentEdit: null,
	contentRead: null,
	assessEdit: null,
	assessRead: null,
	parser: null,
	preprocessorEdit: null,
	preprocessorRead: null,
	rolesEdit: null,
	optionsEdit: null,
	parametersRead: null,
	parametersInputEdit: null,
	parametersOutputEdit: null,
	assignValuesToOutputsEdit: null,
	assignValuesToOutputsRead: null,
	severitiesEdit: null,
	conditionsEdit: null,
	severitiesAndConditionsRead: null,
	summaryEdit: null,
	summaryAndDetailsRead: null,
	detailsEdit: null,
	mockEdit: null,
	mockRead: null,
	tagsRead: null,
	tagsEdit: null,
	emptyActionTaskData: null,
	dateFormat: '',
	isDirty: false,
	isValid: true,
	numSectionsOpen: 0,
	sectionsOpenList: {},
	isMaxScreenSize: false,
	bodyPadding$ : function(){
		return !this.embed ? 15 : 0;
	},
	view: null,
	allTags: [],
	allATProperties: [],
	getAllTagsInProgress: false,
	getAllATPropertiesInProgress: false,

	API : {
		getAT : '/resolve/service/actiontask/get',
		getAllTags : '/resolve/service/tag/listTags',
		getAllATProperties : '/resolve/service/atproperties/list',
		saveAT : '/resolve/service/actiontask/save',
		commitAT : '/resolve/service/actiontask/commit',
	},

	preview: false,
	readOnlyMode: true,
	generalEditSectionExpanded: null,
	generalEditSectionMaximized: null,
	parametersEditSectionExpanded: null,
	parametersEditSectionMaximized: null,
	severitiesAndConditionsEditSectionExpanded: null,
	severitiesAndConditionsEditSectionMaximized: null,
	summaryAndDetailsEditSectionExpanded: null,
	summaryAndDetailsEditSectionMaximized: null,

	saveIsEnabled$: function(){
		return this.isValid && this.isDirty;
	},
	saveAsRenameIsEnabled$ : function(){
		return this.isValid && !this.isNew;
	},

	init: function() {
		this.set('dateFormat', clientVM.getUserDefaultDateFormat());
		this.saveWithoutDoneBody = this.localize('saveWithoutDoneBody');
		this.saveSuccess = this.localize('SaveSuc');
		this.saveAsTitle = this.localize('saveAsTitle');
		this.requestFailedMessage = this.localize('requestFailed');
		this.newActionTaskTitle = this.localize('NewActionTaskTitle');
		this.reloadWithoutSaveTitle = this.localize('reloadWithoutSaveTitle');
		this.reloadWithoutSaveBody = this.localize('reloadWithoutSaveBody');
		this.confirmButtonText = this.localize('confirm');
		this.cancelButtonText = this.localize('cancel');
		this.actionTaskTitle = this.localize('ActionTaskTitle');
		this.editNewTask = this.localize('editNewTask');
		this.editNewActionTaskTitle = this.localize('EditNewActionTaskTitle');
		this.editNewActionTaskMessage = this.localize('EditNewActionTaskMsg');
		this.yesButtonText = this.localize('yesButtonText');
		this.noButtonText = this.localize('noButtonText');

		this.allText = this.localize('allBtn');
		this.generalText = this.localize('generalBtn');
		this.referenceText = this.localize('referenceBtn');
		this.parametersText = this.localize('parametersBtn');
		this.preprocessorText = this.localize('preprocessorBtn');
		this.contentText = this.localize('contentBtn');
		this.parserText = this.localize('parserBtn');
		this.assessText = this.localize('assessBtn');
		this.outputsText = this.localize('outputsBtn');
		this.severityConditionsText = this.localize('severityConditionsBtn');
		this.summaryDetailsText = this.localize('summaryDetailsBtn');
		this.mockText = this.localize('mockBtn');
		this.tagsText = this.localize('tagsBtn');

		if (!this.embed) {
			this.getAllTags(function(records){
				this.set('allTags', records);
			}.bind(this))
			this.getAllATProperties(function(records){
				var propertyNames = [];
				for(var i = 0; i < records.length; i++){
					propertyNames.push(records[i]['uname']);
				}
				this.set('allATProperties', propertyNames);
			}.bind(this))

			this.initActionTaskBuilder();

			var actionTaskToolbarSetting = Ext.state.Manager.get('actionTaskToolbarSetting');
			if (actionTaskToolbarSetting && parseInt(actionTaskToolbarSetting) > 0) {
				this.set('toolbarSetting', actionTaskToolbarSetting);
			} else if (window.mobile) {
				this.set('toolbarSetting', this.toolbarSettingMap['ICONONLY']);
			} else {
				this.set('toolbarSetting', this.toolbarSettingMap['TEXTANDICON']);
			}
		}
		
		// generate page token for actiontask/editorstub.jsp
		clientVM.getCSRFToken_ForURI('/resolve/actiontask/editorstub.jsp');
	},

	activate: function(screen, params) {
		if (this.creatingNewTask) {
			this.creatingNewTask = false;
			this.generalEdit.gluModel.hideShowGeneralEditTab(this.id);
			return;
		}

		this.set('version', '');
		if (params && params.version) {
			var version = params.version.toLowerCase();
			if (version == 'lateststable' || version == 'latestcommit' || version == 'latestcommitted') {
				this.set('version', 'latestStable');
			} else if (Ext.isNumeric(version)) {
				this.set('version', version);
			}
		}

		if (!params || !params.id || this.id !== params.id) {
			var id = '',
				name = '',
				namespace = '';

			if (params) {
				if (typeof params.id === 'string') {
					id = params.id;
				}

				if (typeof params.name === 'string') {
					name = params.name.trim();
				}

				if (typeof params.namespace === 'string') {
					namespace = params.namespace.trim();
				}
			}

			var hasName = name.length > 0 && namespace.length > 0,
				hasID = id.length > 0;

			this.set('id', id);
			this.set('name', name);
			this.set('namespace', namespace);
			this.set('saveIsEnabled', false);
			this.set('saveAsRenameIsEnabled', false);

			if (this.callbackIdForNewTask && this.blockContainer) {
				this.blockContainer.removeCallback(this.generalEdit, 'onLoadOrClose', this.callbackIdForNewTask);
			}

			if (hasID || hasName) {
				this.set('saveAsRenameIsEnabled', true);
				this.loadActionTask(function (payload) {

					if (!hasID) {
						this.modifyHash(payload.data.id);
					}

					this.successfulActionTaskLoad(payload);
					this.set('sectionButtonSelected', this.sectionButtonMap['ALL']);
					this.set('readOnlyMode', true);
					this.collapseAll();
					this.resizeNormalAllSection(true);
				}.bind(this), this.failActionTaskLoad.bind(this), true);

			} else {
				this.initNew();
			}
		} else {
			this.set('saveAsRenameIsEnabled', true);
			this.set('sectionButtonSelected', this.sectionButtonMap['ALL']);
			this.set('readOnlyMode', true);
			this.loadActionTask(function (payload) {
				this.successfulActionTaskLoad(payload);
			}.bind(this), this.failActionTaskLoad.bind(this), true);
		}
		this.generalEdit.gluModel.hideShowGeneralEditTab(this.id);
	},

	isInitialized: false,

	initActionTaskBuilder: function() {
		var blockContainer = new RS.common.blocks.BlockContainer(),
			fullScreenText = this.localize('resizeFullscreen'),
			normalScreenText = this.localize('resizeNormalscreen');

		// General section
		var generalBlock = blockContainer.addBlock('generalBlock');
		var generalRead = generalBlock.addModel(this, 'generalTitle', 'RS.actiontaskbuilder.GeneralRead', RS.actiontaskbuilder.GeneralReadBlockModel);
		var generalEdit = generalBlock.addModel(this, 'generalTitle', 'RS.actiontaskbuilder.GeneralEdit', RS.actiontaskbuilder.GeneralEditBlockModel);

		// General section wizard button (order matters)
		generalRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.generalRead.gluModel, this.sectionButtonMap['GENERAL']);
		}.bind(this));
		generalRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.generalRead.gluModel);
		}.bind(this));

		// Roles sub-section
		var rolesEdit = generalBlock.addModel(this, 'generalTitle', 'RS.actiontaskbuilder.RolesEdit', RS.actiontaskbuilder.RolesEditBlockModel);

		// Options sub-section
		var optionsEdit = generalBlock.addModel(this, 'generalTitle', 'RS.actiontaskbuilder.OptionsEdit', RS.actiontaskbuilder.OptionsEditBlockModel);

		// References section
		var referencesBlock = blockContainer.addBlock('referencesBlock');
		var referencesRead = referencesBlock.addModel(this, 'referencesReadTitle', 'RS.actiontaskbuilder.ReferencesRead', RS.actiontaskbuilder.ReferencesReadBlockModel);
		referencesRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.referencesRead.gluModel, this.sectionButtonMap['REFERENCE']);
		}.bind(this));
		referencesRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.referencesRead.gluModel);
		}.bind(this));

		// Parameters section
		var parametersBlock = blockContainer.addBlock('parametersBlock');
		var parametersRead = parametersBlock.addModel(this, 'parametersTitle', 'RS.actiontaskbuilder.ParametersRead', RS.actiontaskbuilder.ParametersReadBlockModel);
		var parametersInputEdit = parametersBlock.addModel(this, 'inputParametersWizardStepTitle', 'RS.actiontaskbuilder.ParametersInputEdit', RS.actiontaskbuilder.ParametersInputEditBlockModel);
		var parametersOutputEdit = parametersBlock.addModel(this, 'outputParametersWizardStepTitle', 'RS.actiontaskbuilder.ParametersOutputEdit', RS.actiontaskbuilder.ParametersOutputEditBlockModel);

		// Parameters section wizard button order matters)
		parametersRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.parametersRead.gluModel, this.sectionButtonMap['PARAMETERS']);
		}.bind(this));
		parametersRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.parametersRead.gluModel);
		}.bind(this));

		// Preprocessor section
		var preprocessorBlock = blockContainer.addBlock('preprocessorBlock');
		var preprocessorRead = preprocessorBlock.addModel(this, 'preprocessorReadTitle', 'RS.actiontaskbuilder.PreprocessorRead', RS.actiontaskbuilder.PreprocessorReadBlockModel);
		var preprocessorEdit = preprocessorBlock.addModel(this, 'preprocessorEditTitle', 'RS.actiontaskbuilder.PreprocessorEdit', RS.actiontaskbuilder.PreprocessorEditBlockModel);

		// Preprocessor section Read buttons (order matters)
		preprocessorRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.preprocessorRead.gluModel, this.sectionButtonMap['PREPROCESSOR']);
		}.bind(this));
		preprocessorRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.preprocessorRead.gluModel);
		}.bind(this));

		// Preprocessor section Edit buttons (order matters)
		preprocessorEdit.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.preprocessorEdit.gluModel, this.sectionButtonMap['PREPROCESSOR']);
		}.bind(this));
		preprocessorEdit.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.preprocessorEdit.gluModel);
		}.bind(this));

		// Content section
		var contentBlock = blockContainer.addBlock('contentBlock');
		var contentRead = contentBlock.addModel(this, 'contentReadTitle', 'RS.actiontaskbuilder.ContentRead', RS.actiontaskbuilder.ContentReadBlockModel);
		var contentEdit = contentBlock.addModel(this, 'contentEditTitle', 'RS.actiontaskbuilder.ContentEdit', RS.actiontaskbuilder.ContentEditBlockModel);

		// Content section Read buttons (order matters)
		contentRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.contentRead.gluModel, this.sectionButtonMap['CONTENT']);
		}.bind(this));
		contentRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.contentRead.gluModel);
		}.bind(this));

		// Content section Edit buttons (order matters)
		contentEdit.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.contentEdit.gluModel, this.sectionButtonMap['CONTENT']);
		}.bind(this));
		contentEdit.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.contentEdit.gluModel);
		}.bind(this));

		// Parser section
		var parserBlock = blockContainer.addBlock('parserBlock');
		var parserRead = parserBlock.addModel(this, 'parserReadTitle', 'RS.actiontaskbuilder.ParserRead', RS.actiontaskbuilder.ParserReadBlockModel);
		var parserEdit = parserBlock.addModel(this, 'parserEditTitle', 'RS.actiontaskbuilder.ParserEdit', RS.actiontaskbuilder.ParserEditBlockModel);

		// Parser section buttons (order matters)
		parserRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.parserRead.gluModel, this.sectionButtonMap['PARSER']);
		}.bind(this));
		parserRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.parserRead.gluModel);
		}.bind(this));

		// Parser section buttons (order matters)
		parserEdit.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.parserEdit.gluModel, this.sectionButtonMap['PARSER']);
		}.bind(this));
		parserEdit.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.parserEdit.gluModel);
		}.bind(this));

		// Assessor section
		var assessBlock = blockContainer.addBlock('assessBlock');
		var assessRead = assessBlock.addModel(this, 'assessReadTitle', 'RS.actiontaskbuilder.AssessorRead', RS.actiontaskbuilder.AssessorReadBlockModel);
		var assessEdit = assessBlock.addModel(this, 'assessEditTitle', 'RS.actiontaskbuilder.AssessorEdit', RS.actiontaskbuilder.AssessorEditBlockModel);

		// Content section Read buttons (order matters)
		assessRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.assessRead.gluModel, this.sectionButtonMap['ASSESS']);
		}.bind(this));
		assessRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.assessRead.gluModel);
		}.bind(this));

		// Content section Edit buttons (order matters)
		assessEdit.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.assessEdit.gluModel, this.sectionButtonMap['ASSESS']);
		}.bind(this));
		assessEdit.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.assessEdit.gluModel);
		}.bind(this));

		// Assign Values to Outputs section
		var assignValuesToOutputsBlock = blockContainer.addBlock('assignValuesToOutputsBlock');
		var assignValuesToOutputsRead = assignValuesToOutputsBlock.addModel(this, 'valueToOutputsReadTitle', 'RS.actiontaskbuilder.AssignValuesToOutputsRead', RS.actiontaskbuilder.AssignValuesToOutputsReadBlockModel);
		var assignValuesToOutputsEdit = assignValuesToOutputsBlock.addModel(this, 'valueToOutputsEditTitle', 'RS.actiontaskbuilder.AssignValuesToOutputsEdit', RS.actiontaskbuilder.AssignValuesToOutputsEditBlockModel);

		// Assign Values to Outputs section Read button (order matters)
		assignValuesToOutputsRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.assignValuesToOutputsRead.gluModel, this.sectionButtonMap['OUTPUT']);
		}.bind(this));
		assignValuesToOutputsRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.assignValuesToOutputsRead.gluModel);
		}.bind(this));

		// Assign Values to Outputs section Edit buttons (order matters)
		assignValuesToOutputsEdit.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.assignValuesToOutputsEdit.gluModel, this.sectionButtonMap['OUTPUT']);
		}.bind(this));
		assignValuesToOutputsEdit.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.assignValuesToOutputsEdit.gluModel);
		}.bind(this));

		// Severities and Conditions section
		var severitiesAndConditionsBlock = blockContainer.addBlock('severitiesAndConditionsBlock');
		var severitiesAndConditionsRead = severitiesAndConditionsBlock.addModel(this, 'severitiesAndConditionsTitle', 'RS.actiontaskbuilder.SeveritiesAndConditionsRead', RS.actiontaskbuilder.SeveritiesAndConditionsBlockModel);
		var severitiesEdit = severitiesAndConditionsBlock.addModel(this, 'severitiesAndConditionsTitle', 'RS.actiontaskbuilder.SeveritiesEdit', RS.actiontaskbuilder.SeveritiesEditBlockModel);
		var conditionsEdit = severitiesAndConditionsBlock.addModel(this, 'severitiesAndConditionsTitle', 'RS.actiontaskbuilder.ConditionsEdit', RS.actiontaskbuilder.ConditionsEditBlockModel);

		// Severities and Conditions section Read button (order matters)
		severitiesAndConditionsRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.severitiesAndConditionsRead.gluModel, this.sectionButtonMap['SEVERITYCONDITION']);
		}.bind(this));
		severitiesAndConditionsRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.severitiesAndConditionsRead.gluModel);
		}.bind(this));

		// Summary and Details section
		var summaryAndDetailsBlock = blockContainer.addBlock('summaryAndDetailsBlock');
		var summaryAndDetailsRead = summaryAndDetailsBlock.addModel(this, 'summaryAndDetailsTitle', 'RS.actiontaskbuilder.SummaryAndDetailsRead', RS.actiontaskbuilder.SummaryAndDetailsReadBlockModel);
		var summaryEdit = summaryAndDetailsBlock.addModel(this, 'summaryAndDetailsTitle', 'RS.actiontaskbuilder.SummaryEdit', RS.actiontaskbuilder.SummaryEditBlockModel);
		var detailsEdit = summaryAndDetailsBlock.addModel(this, 'summaryAndDetailsTitle', 'RS.actiontaskbuilder.DetailsEdit', RS.actiontaskbuilder.DetailsEditBlockModel);

		// Summary and Details section Read button (order matters)
		summaryAndDetailsRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.summaryAndDetailsRead.gluModel, this.sectionButtonMap['SUMMARYDETAILS']);
		}.bind(this));
		summaryAndDetailsRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.summaryAndDetailsRead.gluModel);
		}.bind(this));

		// Mock section
		var mockBlock = blockContainer.addBlock('mockBlock');
		var mockRead = mockBlock.addModel(this, 'mockTitle', 'RS.actiontaskbuilder.MockRead', RS.actiontaskbuilder.MockReadBlockModel);
		var mockEdit = mockBlock.addModel(this, 'mockTitle', 'RS.actiontaskbuilder.MockEdit', RS.actiontaskbuilder.MockEditBlockModel);

		// Mock section Read button (order matters)
		mockRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.mockRead.gluModel, this.sectionButtonMap['MOCK']);
		}.bind(this));
		mockRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.mockRead.gluModel);
		}.bind(this));

		// Mock section Edit buttons (order matters)
		mockEdit.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.mockEdit.gluModel, this.sectionButtonMap['MOCK']);
		}.bind(this));
		mockEdit.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.mockEdit.gluModel);
		}.bind(this));

		// Tags section
		var tagsBlock = blockContainer.addBlock('tagsBlock');
		var tagsRead = tagsBlock.addModel(this, 'tagsReadTitle', 'RS.actiontaskbuilder.TagsRead', RS.actiontaskbuilder.TagsReadBlockModel);
		var tagsEdit = tagsBlock.addModel(this, 'tagsEditTitle', 'RS.actiontaskbuilder.TagsEdit', RS.actiontaskbuilder.TagsEditBlockModel);

		// Tags section Read button (order matters)
		tagsRead.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.tagsRead.gluModel, this.sectionButtonMap['TAGS']);
		}.bind(this));
		tagsRead.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.tagsRead.gluModel);
		}.bind(this));

		// Tags section Edit buttons (order matters)
		tagsEdit.addResizeMaxBtn(fullScreenText, true, function(){
			this.resizeMaxSection(this.tagsEdit.gluModel, this.sectionButtonMap['TAGS']);
		}.bind(this));
		tagsEdit.addResizeNormalBtn(normalScreenText, true, function(){
			this.resizeNormalSection(this.tagsEdit.gluModel);
		}.bind(this));

		// addDataSource
		generalBlock.addDataSource(generalEdit);
		generalBlock.addDataSource(rolesEdit);
		generalBlock.addDataSource(optionsEdit);
		parametersBlock.addDataSource(parametersInputEdit);
		parametersBlock.addDataSource(parametersOutputEdit);
		preprocessorBlock.addDataSource(preprocessorEdit);
		contentBlock.addDataSource(contentEdit);
		parserBlock.addDataSource(parserEdit);
		referencesBlock.addDataSource(referencesRead);
		assessBlock.addDataSource(assessEdit);
		assignValuesToOutputsBlock.addDataSource(assignValuesToOutputsEdit);
		severitiesAndConditionsBlock.addDataSource(severitiesEdit);
		severitiesAndConditionsBlock.addDataSource(conditionsEdit);
		summaryAndDetailsBlock.addDataSource(summaryEdit);
		summaryAndDetailsBlock.addDataSource(detailsEdit);
		mockBlock.addDataSource(mockEdit);
		tagsBlock.addDataSource(tagsEdit);

		// Callbacks
		blockContainer.defineCallback(generalRead, function (data, resolveActionInvoc) {
			this.set('id', data.id);
			this.set('uname', data.uname);
			this.set('unamespace', data.unamespace);
			this.set('ufullName', data.ufullName);
			this.set('uversion', data.uversion);
			this.set('sysCreatedOn', data.sysCreatedOn);
			this.set('sysCreatedBy', data.sysCreatedBy);
			this.set('sysUpdatedOn', data.sysUpdatedOn);
			this.set('sysUpdatedBy', data.sysUpdatedBy);

			if (data.unamespace && data.unamespace.length > 0 && data.uname  && data.uname.length > 0) {
				var version = this.version && data.uversion && data.uversion > 0 ? ' v' + data.uversion : '';
				this.modifyActionTaskTitle(data.uname + '#' + data.unamespace + version);
			} else {
				this.modifyActionTaskTitle(this.newActionTaskTitle);
			}
		}.bind(this));

		blockContainer.defineCallback(rolesEdit, function (data, accessRights) {
			if (accessRights.canEdit || this.isNew) {
				this.set('editable', true);
				this.enableEditLinks();
			} else {
				this.set('editable', false);
				this.disableEditLinks();
			}
		}.bind(this))

		blockContainer.subscribeDirty(function (dirty) {
			this.set('isDirty', dirty);
			var title = this.actionTaskTitle;
			title = title.replace(/ <span style=\"color:crimson;\">!<\/span>/, '');
        	title = title.replace(/\*/,'');

			this.modifyActionTaskTitle(title);
		}.bind(this));

		blockContainer.subscribeValid(function (valid) {
			this.set('isValid', valid);
			var title = this.actionTaskTitle;
			title = title.replace(/ <span style=\"color:crimson;\">!<\/span>/, '');
        	title = title.replace(/\*/,'');

			this.modifyActionTaskTitle(title);
		}.bind(this));

		// defineDataDependency
		blockContainer.defineDataDependency(generalEdit, contentRead);
		blockContainer.defineDataDependency(generalEdit, contentEdit);
		blockContainer.defineDataDependency(generalEdit, parserEdit);
		blockContainer.defineDataDependency(generalEdit, mockRead);
		blockContainer.defineDataDependency(generalEdit, mockEdit);

		//Internal data dependency (not quite generic, might need to enhance in the future). ParserEdit will kick off the chain event.
		blockContainer.defineInternalDataDependency(parserEdit, parametersOutputEdit);
		blockContainer.defineInternalDataDependency(parametersOutputEdit, summaryEdit);
		blockContainer.defineInternalDataDependency(parametersOutputEdit, detailsEdit);
		blockContainer.defineInternalDataDependency(parametersOutputEdit, assignValuesToOutputsEdit);
		blockContainer.defineInternalDataDependency(parametersOutputEdit, severitiesEdit);
		blockContainer.defineInternalDataDependency(parametersOutputEdit, conditionsEdit);
		blockContainer.defineInternalDataDependency(parametersInputEdit, summaryEdit);
		blockContainer.defineInternalDataDependency(parametersInputEdit, detailsEdit);
		blockContainer.defineInternalDataDependency(parametersInputEdit, assignValuesToOutputsEdit);
		blockContainer.defineInternalDataDependency(parametersInputEdit, severitiesEdit);
		blockContainer.defineInternalDataDependency(parametersInputEdit, conditionsEdit);


		// itemList
		this.itemList.add(generalRead.gluModel);
		this.itemList.add(generalEdit.gluModel);
		this.itemList.add(rolesEdit.gluModel);
		this.itemList.add(optionsEdit.gluModel);
		this.itemList.add(referencesRead.gluModel);
		this.itemList.add(parametersRead.gluModel);
		this.itemList.add(parametersInputEdit.gluModel);
		this.itemList.add(parametersOutputEdit.gluModel);
		this.itemList.add(preprocessorRead.gluModel);
		this.itemList.add(preprocessorEdit.gluModel);
		this.itemList.add(contentRead.gluModel);
		this.itemList.add(contentEdit.gluModel);
		this.itemList.add(parserRead.gluModel);
		this.itemList.add(parserEdit.gluModel);
		this.itemList.add(assessRead.gluModel);
		this.itemList.add(assessEdit.gluModel);
		this.itemList.add(assignValuesToOutputsRead.gluModel);
		this.itemList.add(assignValuesToOutputsEdit.gluModel);
		this.itemList.add(severitiesAndConditionsRead.gluModel);
		this.itemList.add(severitiesEdit.gluModel);
		this.itemList.add(conditionsEdit.gluModel);
		this.itemList.add(summaryAndDetailsRead.gluModel);
		this.itemList.add(summaryEdit.gluModel);
		this.itemList.add(detailsEdit.gluModel);
		this.itemList.add(mockRead.gluModel);
		this.itemList.add(mockEdit.gluModel);
		this.itemList.add(tagsRead.gluModel);
		this.itemList.add(tagsEdit.gluModel);

		// actiontaskbuilder variables
		this.blockContainer = blockContainer;
		this.generalBlock = generalBlock;
		this.generalRead = generalRead;
		this.generalEdit = generalEdit;
		this.referencesRead = referencesRead;
		this.rolesEdit = rolesEdit;
		this.optionsEdit = optionsEdit;
		this.contentEdit = contentEdit;
		this.contentRead = contentRead;
		this.preprocessorEdit = preprocessorEdit;
		this.preprocessorRead = preprocessorRead;
		this.assessEdit = assessEdit;
		this.assessRead = assessRead;
		this.parserEdit = parserEdit;
		this.parserRead = parserRead;
		this.parametersInputEdit = parametersInputEdit;
		this.parametersOutputEdit = parametersOutputEdit;
		this.parametersRead = parametersRead;
		this.assignValuesToOutputsEdit = assignValuesToOutputsEdit;
		this.assignValuesToOutputsRead = assignValuesToOutputsRead;
		this.severitiesEdit = severitiesEdit;
		this.conditionsEdit = conditionsEdit;
		this.severitiesAndConditionsRead = severitiesAndConditionsRead;
		this.summaryEdit = summaryEdit;
		this.detailsEdit = detailsEdit;
		this.summaryAndDetailsRead = summaryAndDetailsRead;
		this.mockEdit = mockEdit;
		this.mockRead = mockRead;
		this.tagsRead = tagsRead;
		this.tagsEdit = tagsEdit;
		this.emptyActionTaskData = blockContainer.getData();
		blockContainer.load(this.emptyActionTaskData);

		this.isInitialized = true;
	},

	hideAllSections: function(section, ignoreCurrentSection, onComplete) {
		for (var i=0, l = this.itemList.length; i < l; i++) {
			var currentSection = this.itemList.getAt(i);
			if (ignoreCurrentSection || currentSection.viewmodelName != section.viewmodelName) {
				this.hideSection(currentSection);
			}
		}
		if (Ext.isFunction(onComplete)) {
			onComplete.call(this);
		}
	},

	hideSection: function(section) {
		if (section.isSectionMaxSize) {
			if (section.height) {
				this.setSectionHeight(section, section.minHeight);
			}
			section.isSectionMaxSize = false;
			this.showSectionMaxResizeBtn(section);
			this.hideSectionNormalResizeBtn(section);
			this.toggleSectionCollapseTool(section);
		}

		this.collapseAndHideSection(section);
	},

	collapseAndHideSection: function(section) {
		if (section.hidden && section.collapsed) {
			// do nothing
		} else if (section.hidden) {
			section.ignoreDecrementFlag = true;
			section.set('collapsed', true);
		} else if (section.collapsed) {
			section.set('hidden', true);
		} else {
			section.collapseAndHideSectionFlag = true;
			section.ignoreDecrementFlag = true;
			section.set('collapsed', true);
		}
	},

	collapseAndHideSectionCallback: function(section) {
		section.collapseAndHideSectionFlag = false;
		section.set('hidden', true);
	},

	showAndExpandSectionCallback: function(section) {
		section.showAndExpandSectionFlag = false;
		section.set('collapsed', false);

		if (section.showSectionMaxFlag) {
			section.showSectionMaxFlag = false;
			this.doResizeMaxSection(section);
		}
	},

	setSectionHeight: function(section, height) {
		 if (section.height) {
			section.set('height', 0);
			if (section.contentIsEmpty) {
				section.set('height', 'auto');
			} else {
				section.set('height', height);
			}
		}
	},

	doResizeMaxSection: function(section) {
		if (!section.isSectionMaxSize) {
			this.set('isMaxScreenSize', true);
			section.isSectionMaxSize = true;

			var id, offset;
			if (this.embed) {
				id = this.view.up('#TaskProperty').getEl().dom.id;
				offset = 131;
			} else {
				id = this.view.getEl().dom.id;
				offset = 35;
			}

			var el = Ext.getElementById(id+'-body'),
				height = el.style.height;

			if (section.height) {
				this.setSectionHeight(section, parseInt(height) - offset);
			}

			if (this.isNew && section.viewmodelName == 'GeneralEdit') {
				this.hideSectionNormalResizeBtn(section);
			} else {
				this.showSectionNormalResizeBtn(section);
			}
			this.hideSectionMaxResizeBtn(section);
			this.toggleSectionCollapseTool(section);
		}
	},

	resizeMaxSection: function(section, sectionButtonName) {
		this.hideAllSections(section, null, function() {
			setTimeout(function() {
				if (section.hidden) {
					section.showAndExpandSectionFlag = true;
					section.showSectionMaxFlag = true;
					section.set('hidden', false);
				} else if (section.collapsed) {
					section.set('collapsed', false);
					this.doResizeMaxSection(section);
				} else {
					this.doResizeMaxSection(section);
				}
		
				this.set('sectionButtonSelected', sectionButtonName);
			}.bind(this), 100);
		});
		if (this.readOnlyChangeFlag) {
			this.readOnlyChangeFlag = false;
		}
	},

	resizeNormalSection: function(section) {
		this.set('isMaxScreenSize', false);
		if (this.readOnlyMode) {
			this.showAllReadSections();
		} else {
			this.showAllEditSections();
		}
		this.set('sectionButtonSelected', this.sectionButtonMap['ALL']);
	},

	resizeNormalAllSection: function(isInitial) {
		this.set('isMaxScreenSize', false);

		if (!isInitial) {
			var blockNames = ['generalBlock', 'parametersBlock', 'preprocessorBlock',
				'parserBlock', 'assessBlock', 'assignValuesToOutputsBlock', 'severitiesAndConditionsBlock',
				'summaryAndDetailsBlock', 'mockBlock', 'tagsBlock', 'referencesBlock'];

			if (this.contentSectionVisible) {
				blockNames.push('contentBlock');
			}
			this.blockContainer.showBlocks(blockNames);
		}

		if (this.readOnlyMode) {
			this.showAllReadSections();
		} else {
			this.showAllEditSections();
		}

		if (this.syncExpandedSectionsFlag) {
			this.syncExpandedSections();
		} else {
			this.collapseAll();
		}
		this.set('sectionButtonSelected', this.sectionButtonMap['ALL']);
	},

	showAllReadSections: function() {
		for (var i=0, l = this.itemList.length; i < l; i++) {
			var section = this.itemList.getAt(i);

			if (section.editBlock || section.editSubBlock || section.conditionalEditBlock) {
				this.collapseAndHideSection(section);

			} else if (section.conditionalReadBlock && section.viewmodelName == 'ContentRead') {
				if (this.contentSectionVisible) {
					this.showThisSection(section);
				} else {
					this.collapseAndHideSection(section);
				}
			} else {
				this.showThisSection(section);
				if (section.readAndEditBlock && section.viewmodelName == 'Parser') {
					section.set('isViewOnly', true);
				}
			}

			if (section.isSectionMaxSize) {
				if (section.height) {
					this.setSectionHeight(section, section.minHeight);
				}
				section.isSectionMaxSize = false;
				if (section.collapsed) {
					this.hideSectionMaxResizeBtn(section);
				} else {
					this.showSectionMaxResizeBtn(section);
				}
				this.hideSectionNormalResizeBtn(section);
				this.toggleSectionCollapseTool(section);
			}
		}
	},

	showThisSection: function(section) {
		if (section.hidden) {
			section.set('hidden', false);
		}
	},

	showAndExpandThisSection: function(section) {
		if (section.hidden) {
			if (section.collapsed) {
				section.showAndExpandSectionFlag = true;
			}
			section.set('hidden', false);
		}
	},

	showAllEditSections: function() {
		for (var i=0, l = this.itemList.length; i < l; i++) {
			var section = this.itemList.getAt(i);

			if (section.editBlock) {
				this.showThisSection(section);
			} else if (section.conditionalEditBlock && section.viewmodelName == 'ContentEdit') {
				if (this.contentSectionVisible) {
					this.showThisSection(section);
				} else {
					this.collapseAndHideSection(section);
				}
			} else if (section.readAndEditBlock) {
				this.showThisSection(section);
				if (section.viewmodelName == 'Parser') {
					section.set('isViewOnly', false);
				}
			} else {
				this.collapseAndHideSection(section);
			}

			if (section.isSectionMaxSize) {
				if (section.height) {
					this.setSectionHeight(section, section.minHeight);
				}
				section.isSectionMaxSize = false;
				if (section.collapsed) {
					this.hideSectionMaxResizeBtn(section);
				} else {
					this.showSectionMaxResizeBtn(section);
				}
				this.hideSectionNormalResizeBtn(section);
				this.toggleSectionCollapseTool(section);
			}
		}
	},

	showSectionMaxResizeBtn: function(section) {
		var view = section.view;
		if (view && view.header && view.header.items.length) {
			var headerItems = view.header.items.items;
			if (headerItems) {
				for (var i=0, l=headerItems.length; i < l; i++) {
					var headerItem = headerItems[i];
					if (headerItem.isResizeMaxBtn && headerItem.hidden && typeof headerItem.show === 'function') {
						headerItem.show();
					}
				}
			}
		}
	},

	hideSectionMaxResizeBtn: function(section) {
		var view = section.view;
		if (view && view.header && view.header.items.length) {
			var headerItems = view.header.items.items;
			if (headerItems) {
				for (var i=0, l=headerItems.length; i < l; i++) {
					var headerItem = headerItems[i];
					if (headerItem.isResizeMaxBtn && headerItem.show && typeof headerItem.hide === 'function') {
						headerItem.hide();
					}
				}
			}
		}
	},

	showSectionNormalResizeBtn: function(section) {
		var view = section.view;
		if (view && view.header && view.header.items.length) {
			var headerItems = view.header.items.items;
			if (headerItems) {
				for (var i=0, l=headerItems.length; i < l; i++) {
					var headerItem = headerItems[i];
					if (headerItem.isResizeNormalBtn && headerItem.hidden && typeof headerItem.show === 'function') {
						headerItem.show();
					}
				}
			}
		}
	},

	hideSectionNormalResizeBtn: function(section) {
		var view = section.view;
		if (view && view.header && view.header.items.length) {
			var headerItems = view.header.items.items;
			if (headerItems) {
				for (var i=0, l=headerItems.length; i < l; i++) {
					var headerItem = headerItems[i];
					if (headerItem.isResizeNormalBtn && headerItem.show && typeof headerItem.hide === 'function') {
						headerItem.hide();
					}
				}
			}
		}
	},

	toggleSectionCollapseTool: function(section, forceHide) {
		var view = section.view;
		if (view && view.header && view.header.items.length) {
			var items = view.header.items;

			// collapse tool is usually the last item in the header list, unless there is no collapse tool
			var headerItem = items.getAt(items.length -1);

			if (headerItem.xtype == 'tool' || headerItem.isCollapseBtn) {
				if ((forceHide || section.isSectionMaxSize) &&
					(typeof headerItem.hide === 'function'))
				{
					headerItem.hide();
				} else if (typeof headerItem.show === 'function') {
					headerItem.show();
				}
			}
		}
	},

	hideAllSectionBtns: function(section) {
		 this.hideSectionMaxResizeBtn(section);
		 this.hideSectionNormalResizeBtn(section);
		 this.toggleSectionCollapseTool(section, true);
	},

	modifyActionTaskTitle: function (title) {
		title = Ext.String.htmlEncode(title);
		if (!this.isValid && !this.isNew){
			this.set('actionTaskTitle', title + ' <span style="color:crimson;">!</span>');
		} else if (this.isDirty && !this.isNew) {
			this.set('actionTaskTitle', title + '*');
		} else {
			this.set('actionTaskTitle', title);
		}

		if (!this.embed) {
			clientVM.setWindowTitle(this.actionTaskTitle);
		}
	},

	callbackIdForNewTask : null,

	creatingNewTask: false,

	initNew: function() {
		this.set('isNew', true);
		this.set('editable', true);
		this.set('executable', false);
		this.set('readOnlyMode', false);
		this.blockContainer.hideBlocks();
		this.blockContainer.load(this.emptyActionTaskData);
		this.resizeMaxSection(this.generalEdit.gluModel, this.sectionButtonMap['GENERAL']);
		setTimeout(function() {
			this.hideAllSectionBtns(this.generalEdit.gluModel);
		}.bind(this), 100);

		/*
		var generalEditDoneCallbackID = this.blockContainer.defineCallback(this.generalEdit, function () {
			this.rolesEdit.disableForm();
			this.optionsEdit.disableForm();
			this.saveActionTask(false, null, function onSuccess(payload) {
				this.creatingNewTask = true;
				this.blockContainer.load(payload);
				var blockNames = ['parametersBlock', 'preprocessorBlock',
					'parserBlock', 'assessBlock', 'assignValuesToOutputsBlock', 'severitiesAndConditionsBlock',
					'summaryAndDetailsBlock', 'mockBlock', 'tagsBlock', 'referencesBlock'];

				if (payload.data.resolveActionInvoc.utype.toUpperCase() !== 'ASSESS') {
					blockNames.push('contentBlock');
				}

				this.blockContainer.showBlocks(blockNames);
				this.set('generalIsValid', true);
				this.modifyHash(payload.data.id);
				this.blockContainer.removeCallback(this.generalEdit, 'onClose', generalEditDoneCallbackID);
				this.rolesEdit.enableForm();
				this.optionsEdit.enableForm();
			}.bind(this), function onFailure() {
				this.generalBlock.hide();
				this.generalEdit.show();
				this.rolesEdit.enableForm();
				this.optionsEdit.enableForm();
			}.bind(this));
		}.bind(this), 'onClose');
		this.set('callbackIdForNewTask', generalEditDoneCallbackID);
		*/

		this.closeDetailSections();
		this.set('sectionButtonDisabled', true);
	},

	closeDetailSections: function() {
		// close Mock Edit/Read detail sections
		if (this.mockEdit && this.mockEdit.gluModel.isEditingDetails) {
			this.mockEdit.gluModel.set('isEditingDetails', false);
		}
		if (this.mockRead && this.mockRead.gluModel.isShowingDetails) {
			this.mockRead.gluModel.set('isShowingDetails', false);
		}
	},

	modifyHash: function (id, isReplaceId) {
		var hashParts = [],
			hasIdInHash;

		if (clientVM.windowHash && clientVM.windowHash.length > 0) {
			var hash = clientVM.windowHash,
				hasIdInHash = hash.indexOf('/id=') >= 0;

			if (!hasIdInHash && hash[hash.length - 1] === '/') {
				hash = hash.substr(0, hash.length - 1);
			} else if (hasIdInHash && isReplaceId) {
				hash = hash.substr(0, hash.indexOf('/id='))
			}

			hashParts.push(hash);

			if (!hasIdInHash || isReplaceId) {
				hashParts.push('id=' + id);
			}
		}
		var params = {
			id: id
		};
		if (this.version) {
			Ext.apply(params, {
				version: this.version
			})
		}
		clientVM.handleNavigation({
			modelName: 'RS.actiontask.ActionTask',
			params: params
		});
	},

	loadActionTask: function(onSuccess, onFailure, suppressError) {
		this.set('saveIsEnabled', false);
		//To indicate a new AT is loaded into view.
		window.loadingNewAT = true;
		var params = {
			id: this.id,
			name: this.name ? this.name + '#' + this.namespace : ''
		};
		if (this.version) {
			Ext.apply(params, {
				version: this.version
			})
		}
		this.ajax({
			url: this.API['getAT'],
			method: 'post',
			params: params,
			scope: this,
			success: function (response) {
				var payload = RS.common.parsePayload(response);

				if (payload.success) {
					this.set('generalIsValid', true);
					this.set('uisStable', payload.data.uisStable);

					if (typeof onSuccess === 'function') {
						onSuccess(payload);
					}
				} else {
					if (!suppressError) {
						clientVM.displayError(this.localize('serverError', payload.message));
					}

					if (typeof onFailure === 'function') {
						onFailure(payload);
					}
				}
			},
			failure: function(response) {
				clientVM.displayFailure(response);

				if (typeof onFailure === 'function') {
					onFailure(response);
				}
			}
		});
	},

	successfulActionTaskLoad: function (payload) {
		if (!this.isInitialized) {
			this.initActionTaskBuilder();
		}
		this.blockContainer.load(payload);
		var blockNames = ['generalBlock', 'parametersBlock', 'preprocessorBlock',
			'parserBlock', 'assessBlock', 'assignValuesToOutputsBlock', 'severitiesAndConditionsBlock',
			'summaryAndDetailsBlock', 'mockBlock', 'tagsBlock', 'referencesBlock'];
		this.blockContainer.showBlocks(blockNames);

		if (this.embed && this.itemList) {
			this.resizeNormalAllSection(true);
		}
	},

	failActionTaskLoad: function(payload) {
		if (this.id) {
			this.message({
				title: this.localize('errorTitle'),
				msg: payload.message,
				buttons: Ext.MessageBox.OK,
				buttonText: {
					ok: this.localize('close')
				},
				scope: this,
				fn: function(btn) {
					this.accessActiontTaskList();
				}
			});
		} else {
			this.initNew();
			this.set('id', '');
			this.set('namespace', '');
			this.set('name', '');
		}
	},

	isembedded$: function() {
		return this.embed;
	},

	actionTaskPadding$: function() {
		var padding = 0;

		if (!this.embed) {
			padding = 10;
		}

		return padding;
	},

	accessActiontTaskList: function() {
		clientVM.handleNavigation({
			modelName: 'RS.actiontask.Main'
		})
	},

	isEditable$: function() {
		return this.editable;
	},

	isExecutable$: function() {
		if (this.isDirty) {
			return false;
		}
		return this.executable || !this.isNew;
	},

	execution: function(button) {
		if (this.executionWin) {
			this.executionWin.doClose();
		}

		this.executionWin = this.open({
			mtype: 'RS.actiontaskbuilder.Execute',
			target: button.getEl().id,
			executeDTO: {
				actiontask: this.ufullName,
				actiontaskSysId: this.id,
				action: 'TASK'
			}
		});
	},

	isNew: false,

	fileOptions: function() {},
	
	edit: function() {
		this.showAllEditSections();
		this.set('readOnlyMode', false);
		//this.set('sectionButtonSelected', this.sectionButtonMap['ALL']);
	},

	exitToView: function() {
		if (this.isDirty && !this.embed) {
			this.message({
				title: this.localize('exitWithoutSaveTitle'),
				msg: this.localize('exitWithoutSaveBody'),
				buttons: Ext.MessageBox.YESNOCANCEL,
				buttonText: {
					yes: this.localize('save'),
					no: this.localize('dontSave'),
					cancel: this.localize('cancel')
				},
				scope: this,
				fn: function(btn) {
					if (btn == "yes") {
						this.doSave(function() {
							this.doExitToView();
						}.bind(this));
					} else if (btn == "no") {
						this.reloadActionTask(function(){
							this.doExitToView();
						}.bind(this));
					}
				}
			})
		} else {
			this.doExitToView();
		}
	},

	exitToViewIsEnabled$: function(){
		return !this.isNew;
	},

	doExitToView: function() {
		this.showAllReadSections();
		this.set('readOnlyMode', true);
		//this.set('sectionButtonSelected', this.sectionButtonMap['ALL']);
	},

	editIsEnabled$: function() {
		if (this.preview) {
			return false;
		} else {
			return true;
		}
	},

	save: function() {
		this.doSave();
	},

	doSave: function (onComplete, commit, comment) {
		var isNew = this.isNew;
		this.saveActionTask(false, null, function(payload) {
			//To indicate we're still working on same AT after save.
			window.loadingNewAT = false;
			if (isNew) {
				this.successfulActionTaskLoad(payload);
			} else {
				this.blockContainer.load(payload);
				if (this.version) {
					this.set('version', '');
					clientVM.removeURLHashVersion();
					this.reloadActionTask();
				} 
			}
			if (Ext.isFunction(onComplete)) {
				onComplete(payload);
			}
		}.bind(this), null, commit, comment);
	},

	saveAs: function() {
		var me = this;
		this.open({
			mtype: 'RS.actiontask.MoveOrRenameOrCopy',
			uname: this.uname,
			unamespace: this.unamespace,
			action: 'saveAs',
			actionText: this.saveAsTitle,
			dumper: function(action, namespace, name) {
				me.saveActionTask(false, function(actionTask) {
					var nullTheId = function(obj) {
						for (key in obj) {
							if (key == 'assignedTo' || !obj[key])
								continue;
							if (key == 'id' || key == 'sysId' || key == 'sys_id')
								obj[key] = '';
							if (Ext.isObject(obj[key]))
								nullTheId(obj[key]);
							if (Ext.isArray(obj[key]))
								Ext.each(obj[key], function(o) {
									nullTheId(o)
								});
						}
					}
					nullTheId(actionTask);
					actionTask.uname = name;
					actionTask.unamespace = namespace;
				}, function(actiontask) {
					// reload all sections with newly save as actiontask data
					me.set('id', actiontask.data.id);
					me.set('name', name);
					me.set('namespace', namespace);
					me.reloadActionTask(function() {
						me.allBtn();
					});
					me.modifyHash(actiontask.data.id, true);
				});
			}
		});
	},

	verifyCommitVersion: function(comment) {
		this.ajax({
			url: '/resolve/service/actiontask/getHistory',
			params: {
				id: this.id,
				docFullName: this.ufullName
			},
			method: 'GET',
			success: function(resp) {
				var response = RS.common.parsePayload(resp);
				if (response.success) {
					if (response.records && response.records.length && response.records[0].version > this.uversion) {
						this.displayCommitWarning(comment);
					} else {
						this.saveAndCommit(comment);
					}
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	displayCommitWarning: function(comment) {
		this.message({
			title: this.localize('attention'),
			msg: this.localize('newVersionsAvailabe'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('commitNewVersion'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn == 'yes') {
					this.saveAndCommit(comment);
				}
			},
		});
		this.doClose()
	},

	saveAndCommit: function(comment) {
		if (!this.embed) {
			this.doSave(function() {
				this.doExitToView();
			}.bind(this), true, comment);
		} else {
			this.doExitToView();
		}
	},

	commit: function() {
		this.open({
			mtype: 'RS.actiontaskbuilder.Commit'
		})
	},
	commitIsEnabled$: function() {
		return !this.uisStable || this.isDirty;
	},

	viewVersionList: function() {
		this.open({
			mtype: 'RS.actiontaskbuilder.Version',
			name : this.ufullName,
			uname: this.uname,
			id : this.id,
			unamespace: this.unamespace,
			dumper: {
				dump: function(selections) {
					if (selections.length > 0) {
						var selection = selections[0];
						this.loadSelectedVersion(selection.get('version'));
					}
				},
				scope: this
			}
		});
	},
	loadSelectedVersion: function(version) {
		if (this.isDirty && !this.embed) {
			this.message({
				title: this.localize('loadVersionWithoutSaveTitle', version),
				msg: this.localize('loadVersionWithoutSaveBody'),
				buttons: Ext.MessageBox.YESNOCANCEL,
				buttonText: {
					yes: this.localize('save'),
					no: this.localize('dontSave'),
					cancel: this.localize('cancel')
				},
				scope: this,
				fn: function(btn) {
					if (btn == "yes") {
						this.doSave(function() {
							this.doLoadSelectedVersion(version);
						}.bind(this));
					} else if (btn == "no") {
						this.doLoadSelectedVersion(version);
					}
				}
			})
		} else {
			this.doLoadSelectedVersion(version);
		}
	},

	doLoadSelectedVersion: function(version) {
		this.set('version', version);
		clientVM.updateURLHashVersion(this.version);
		this.reloadActionTask();
	},

	saveActionTask: function(exitAfterSave, beforeSave, onSuccess, onFailure, commit, comment) {
		var actionTask = this.gatherData();
		if (!actionTask) {
			this.set('saveIsEnabled', true);
			clientVM.displayError(this.localize('SaveErr'));
			return;
		}

		var data = actionTask.data;
		if (!data.uname && !data.unamespace) {
			this.set('saveIsEnabled', true);
			return;
		}

		//Work around : AssignedTo field should be null if there is no user is assignedTo to the task.
		if(!data.assignedTo || !data.assignedTo.hasOwnProperty('id'))
			data.assignedTo = null;

		if (typeof beforeSave === 'function') {
			actiontask = beforeSave(actionTask.data);
		}

		var params = {};
		var url = this.API.saveAT;
		if (commit) {
			params = {
				comment: comment
			}
			url = this.API.commitAT;
			actionTask.data.uisStable = true;
		} else {
			actionTask.data.uisStable = false;
		}

		this.set('saveIsEnabled', false);

		this.ajax({
			url: url,
			jsonData: actionTask.data,
			params: params,
			success: function(response) {
				var payload = RS.common.parsePayload(response);

				if (payload.success) {
					if (exitAfterSave === true) {
						clientVM.handleNavigation({
							modelName: 'RS.actiontask.Main'
						});
					}

					clientVM.displaySuccess(this.saveSuccess);

					this.blockContainer.saveComplete();

					if (typeof onSuccess === 'function') {
						onSuccess(payload);
					}
				} else {
					if (typeof onFailure === 'function') {
						onFailure();
					}

					clientVM.displayError(this.localize('SaveErr') + '[' + payload.message + ']')
				}
			},
			failure: function(response) {
				clientVM.displayFailure(response);
			}
		});
	},

	gatherData: function() {
		return this.blockContainer.getData();
	},

	gatherOriginalData: function() {
		return this.blockContainer.getOriginalData();
	},

	loadOriginalData: function(originalData, originalDirtyFlag) {
		this.blockContainer.loadOriginalData(originalData, originalDirtyFlag);
	},

	subscribeDirty: function(fn) {
		return this.blockContainer.subscribeDirty(fn);
	},

	rename: function() {
		this.open({
			mtype: 'RS.actiontask.MoveOrRenameOrCopy',
			action: 'rename',
			uname: this.uname,
			unamespace: this.unamespace,
			dumper: function(action, unamespace, uname, overwrite) {
				this.message({
					title: this.localize('RenameTitle'),
					msg: this.localize('RenameMsg'),
					buttons: Ext.MessageBox.YESNO,
					buttonText: {
						yes: this.localize('rename'),
						no: this.cancelButtonText
					},
					scope: this,
					fn: function(btn) {
						if (btn !== 'no') {
							this.doRename(this.localize('RenameSuc'), this.localize('RenameErr'), {
								ids: [this.id],
								action: 'rename',
								module: unamespace,
								name: uname,
								overwrite: overwrite
							});
						}
					}
				})
			}.bind(this)
		});
	},

	doRename: function(sucMsg, errMsg, params) {
		this.sendReq(sucMsg, errMsg, params);
	},

	sendReq: function(sucMsg, errMsg, params) {
		this.set('saveIsEnabled', false);
		this.ajax({
			url: '/resolve/service/actiontask/actiontaskmanipulation',
			params: params,
			success: function(response) {
				var payload = RS.common.parsePayload(response);

				if (!payload.success) {
					clientVM.displayError(errMsg, payload.message);
				}
				else if (payload.records && payload.records.length > 0) {
                    this.open({
                        mtype: 'RS.actiontask.BulkOpWarningList',
                        msg: payload.message,
                        records: payload.records,
                        action: params.action,
                        dumper: (function(self) {
                            return function() {
                                self.sendReq(sucMsg, errMsg, Ext.apply(params, {
                                    overwrite: true
                                }));
                            }
                        })(this)
                    });
                }
                else{
					clientVM.displaySuccess(sucMsg);
					this.reloadActionTask();
				}
			},
			failure: function(response) {
				clientVM.displayFailure(response);
			}
		})
	},

	/*
	reference: function() {
		clientVM.handleNavigation({
			modelName: 'RS.actiontaskbuilder.GeneralReference',
			params: {
				id: this.id
			},
			target: '_blank'
		})
	},
	*/

	sysInfo: function() {
		this.open({
			mtype: 'RS.common.SysInfo',
			sysId: this.id,
			sysCreatedOn: this.sysCreatedOn,
			sysCreatedBy: this.sysCreatedBy,
			sysUpdatedOn: this.sysUpdatedOn,
			sysUpdatedBy: this.sysUpdatedBy
		})
	},

	reload: function() {
		if (this.isDirty) {
			this.message({
				title: this.localize('reloadWithoutSaveTitle'),
				msg: this.localize('reloadWithoutSaveBody'),
				buttons: Ext.MessageBox.YESNO,
				buttonText: {
					yes: this.localize('confirm'),
					no: this.localize('cancel')
				},
				scope: this,
				fn: function(btn) {
					if (btn === 'yes') {
						this.reloadActionTask(function() {
							this.allBtn();
						}.bind(this));
					}
				}
			});
		} else {
			this.reloadActionTask(function() {
				this.allBtn();
			}.bind(this));
		}
	},

	reloadActionTask: function(onComplete) {
		this.loadActionTask(function(payload) {
			this.blockContainer.load(payload);
			if (Ext.isFunction(onComplete)) {
				onComplete();
			}
		}.bind(this));
	},

	afterRendered: function(theView) {
		this.view = theView;
		this.viewOnly = theView.viewOnly;
		this.set('editable', !this.viewOnly && this.editable);
		if (this.editable ) {
			this.enableEditLinks();
		} else {
			this.disableEditLinks();
		}
	},

	userDateFormat$: function() {
		return this.dateFormat;
	},

	isDisabled$: function() {
		return !this.editable;
	},

	enableEditLinks: function() {
		for (var i=0; i < this.itemList.length; i++) {
			var item = this.itemList.getAt(i),
				view = item.view;
			if (view && view.header) {
				if (view.header.items.length) {
					var headerItems = view.header.items.items;

					if (headerItems) {
						for (var j=0; j < headerItems.length; j++) {
							var headerItem = headerItems[j];
							if (headerItem.isEdit && typeof headerItem.show === 'function') {
								headerItem.show();
							}
						}
					}
				}
			}
		}
	},

	disableEditLinks: function() {
		for (var i=0; i < this.itemList.length; i++) {
			var item = this.itemList.getAt(i),
				view = item.view;

			if (view && view.header) {

				if (view.header.items.length) {
					var headerItems = view.header.items.items;

					if (headerItems) {
						for (var j=0; j < headerItems.length; j++) {
							var headerItem = headerItems[j];
							if (headerItem.isEdit && typeof headerItem.hide === 'function') {
								headerItem.hide();
							}
						}
					}
				}
			}
		}
	},

	incNumSectionsOpen: function(section) {
		if (!this.sectionsOpenList[section.blockModelClass]) {
			this.sectionsOpenList[section.blockModelClass] = true;
		this.set('numSectionsOpen', ++this.numSectionsOpen);
		}
	},
	decNumSectionsOpen: function(section) {
		if (this.sectionsOpenList[section.blockModelClass]) {
			if (section.ignoreDecrementFlag) {
				section.ignoreDecrementFlag = false;
			} else {
				this.sectionsOpenList[section.blockModelClass] = false;
		this.set('numSectionsOpen', --this.numSectionsOpen);
		if (this.numSectionsOpen < 0) {
			this.set('numSectionsOpen', 0);
		}
			}
		}
	},

	sectionButtonSelected: 0,
	sectionButtonMap : {
		ALL : 0,
		GENERAL : 1,
		REFERENCE : 2,
		PARAMETERS : 3,
		PREPROCESSOR : 4,
		CONTENT : 5,
		PARSER : 6,
		ASSESS : 7,
		OUTPUT : 8,
		SEVERITYCONDITION : 9,
		SUMMARYDETAILS : 10,
		MOCK : 11,
		TAGS : 12
	},

	toolbarSetting: 0,
	toolbarSettingMap : {
		NONE : 1,
		ICONONLY: 2,
		TEXTONLY: 3,
		TEXTANDICON: 4
	},

	when_id_changed: {
		on: ['idChanged'],
		action: function() {
			this.set('isNew', !this.id);
			this.updateToolbarSettingDisplay();
		}
	},

	when_toolbarSetting_changed: {
		on: ['toolbarSettingChanged'],
		action: function() {
			this.updateToolbarSettingDisplay();
		}
	},

	readOnlyChangeFlag: false,
	when_readOnlyMode_changed: {
		on: ['readOnlyModeChanged'],
		action: function() {
			if (!this.isNew) {
				var currentSectionButtonSelected = this.sectionButtonSelected;
				this.set('sectionButtonSelected', null);
				setTimeout(function() {
					this.readOnlyChangeFlag = true;

					switch(currentSectionButtonSelected) {
					case this.sectionButtonMap['GENERAL']:
						this.generalBtn();
						break;
					case this.sectionButtonMap['REFERENCE']:
						this.referenceBtn();
						break;
					case this.sectionButtonMap['PARAMETERS']:
						this.parametersBtn();
						break;
					case this.sectionButtonMap['PREPROCESSOR']:
						this.preprocessorBtn();
						break;
					case this.sectionButtonMap['CONTENT']:
						this.contentBtn();
						break;
					case this.sectionButtonMap['PARSER']:
						this.parserBtn();
						break;
					case this.sectionButtonMap['ASSESS']:
						this.assessBtn();
						break;
					case this.sectionButtonMap['OUTPUT']:
						this.outputsBtn();
						break;
					case this.sectionButtonMap['SEVERITYCONDITION']:
						this.severityConditionsBtn();
						break;
					case this.sectionButtonMap['SUMMARYDETAILS']:
						this.summaryDetailsBtn();
						break;
					case this.sectionButtonMap['MOCK']:
						this.mockBtn();
						break;
					case this.sectionButtonMap['TAGS']:
						this.tagsBtn();
						break;
					case this.sectionButtonMap['ALL']:
					default:
						this.readOnlyChangeFlag = false;
						this.syncExpandedSectionsFlag = true;
						this.allBtn();
						break;
					}
				}.bind(this), 200);
			}
		}
	},

	updateToolbarSettingDisplay: function() {
		var sectionButtonDisabled = false,
			iconShowSectionNone = 'rs-social-menu-button',
			iconShowSectionTextOnly = 'rs-social-menu-button',
			iconShowSectionIconAndText = 'rs-social-menu-button',
			iconShowSectionIconOnly = 'rs-social-menu-button';

		switch(this.toolbarSetting) {
		case this.toolbarSettingMap['NONE']:
			sectionButtonDisabled = true;
			iconShowSectionNone += ' icon-check';
			break;
		case this.toolbarSettingMap['TEXTONLY']:
			iconShowSectionTextOnly += ' icon-check';
			break;
		case this.toolbarSettingMap['TEXTANDICON']:
			iconShowSectionIconAndText += ' icon-check';
			break;
		case this.toolbarSettingMap['ICONONLY']:
			iconShowSectionIconOnly += ' icon-check';
			break;
		default:
			break;
		}

		this.set('sectionButtonDisabled', sectionButtonDisabled || this.isNew);
		this.set('iconShowSectionNone', iconShowSectionNone);
		this.set('iconShowSectionTextOnly', iconShowSectionTextOnly);
		this.set('iconShowSectionIconAndText', iconShowSectionIconAndText);
		this.set('iconShowSectionIconOnly', iconShowSectionIconOnly);

		Ext.state.Manager.set('actionTaskToolbarSetting', this.toolbarSetting);
	},

	sectionButtonHidden$: function() {
		return this.sectionButtonDisabled || this.isNew;
	},

	hideNewActionTaskToolBar$: function() {
		return this.isembedded || !this.sectionButtonHidden;
	},

	hideActionTaskToolBar$: function() {
		return this.isembedded || this.sectionButtonHidden;
	},

	showSectionNone: function() {
		this.set('toolbarSetting', this.toolbarSettingMap['NONE']);
	},
	showSectionTextOnly: function() {
		this.set('toolbarSetting', this.toolbarSettingMap['TEXTONLY']);
	},
	showSectionTextIcon: function() {
		this.set('toolbarSetting', this.toolbarSettingMap['TEXTANDICON']);
	},
	showSectionIconOnly: function() {
		this.set('toolbarSetting', this.toolbarSettingMap['ICONONLY']);
	},

	sectionButtonDisabled: false,
	iconShowSectionNone: 'rs-social-menu-button',
	iconShowSectionTextOnly: 'rs-social-menu-button',
	iconShowSectionIconAndText: 'rs-social-menu-button',
	iconShowSectionIconOnly: 'rs-social-menu-button',

	toolbarSettingsIsEnabled$: function() {
		return !this.isNew;
	},

	allText: '',
	allBtn: function() {
		if (!this.allBtnIsPressed) {
			this.resizeNormalAllSection();
		}
	},
	allBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['ALL'];
	},
	allBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-th';
		}
		return '';
	},
	allBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.allText;
		}
		return '';
	},
	allBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.allText;
		}
		return '';
	},

	generalText: '',
	generalBtn: function() {
		if (!this.generalBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.generalRead.gluModel, this.sectionButtonMap['GENERAL']);
			} else {
				this.resizeMaxSection(this.generalEdit.gluModel, this.sectionButtonMap['GENERAL']);
			}
		}
	},
	generalBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['GENERAL'];
	},
	generalBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-info-sign';
		}
		return '';
	},
	generalBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.generalText;
		}
		return '';
	},
	generalBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.generalText;
		}
		return '';
	},

	referenceText: '',
	referenceBtn: function() {
		if (!this.referenceBtnIsPressed) {
			this.resizeMaxSection(this.referencesRead.gluModel, this.sectionButtonMap['REFERENCE']);
		}
	},
	referenceBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['REFERENCE'];
	},
	referenceBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-book';
		}
		return '';
	},
	referenceBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.referenceText
		}
		return '';
	},
	referenceBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.referenceText;
		}
		return '';
	},

	parametersText: '',
	parametersBtn: function() {
		if (!this.parametersBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.parametersRead.gluModel, this.sectionButtonMap['PARAMETERS']);
			} else {
				this.resizeMaxSection(this.parametersInputEdit.gluModel, this.sectionButtonMap['PARAMETERS']);
			}
		}
	},
	parametersBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['PARAMETERS'];
	},
	parametersBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-gear';
		}
		return '';
	},
	parametersBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.parametersText;
		}
		return '';
	},
	parametersBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.parametersText;
		}
		return '';
	},

	preprocessorText: '',
	preprocessorBtn: function() {
		if (!this.preprocessorBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.preprocessorRead.gluModel, this.sectionButtonMap['PREPROCESSOR']);
			} else {
				this.resizeMaxSection(this.preprocessorEdit.gluModel, this.sectionButtonMap['PREPROCESSOR']);
			}
		}
	},
	preprocessorBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['PREPROCESSOR'];
	},
	preprocessorBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-gears';
		}
		return '';
	},
	preprocessorBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.preprocessorText;
		}
		return '';
	},
	preprocessorBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.preprocessorText;
		}
		return '';
	},

	contentSectionVisible: false,
	contentText: '',
	contentBtn: function() {
		if (!this.contentBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.contentRead.gluModel, this.sectionButtonMap['CONTENT']);
			} else {
				this.resizeMaxSection(this.contentEdit.gluModel, this.sectionButtonMap['CONTENT']);
			}
		}
	},
	contentBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['CONTENT'];
	},
	contentBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-edit';
		}
		return '';
	},
	contentBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.contentText;
		}
		return '';
	},
	contentBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.contentText;
		}
		return '';
	},
	contentBtnDisabled$: function() {
		return !this.contentSectionVisible || this.isNew || this.sectionButtonHidden;
	},

	parserText: '',
	parserBtn: function() {
		if (!this.parserBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.parserRead.gluModel, this.sectionButtonMap['PARSER']);
			} else {
				this.resizeMaxSection(this.parserEdit.gluModel, this.sectionButtonMap['PARSER']);
			}
		}
	},
	parserBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['PARSER'];
	},
	parserBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-code';
		}
		return '';
	},
	parserBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.parserText;
		}
		return '';
	},
	parserBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.parserText;
		}
		return '';
	},

	assessText: '',
	assessBtn: function() {
		if (!this.assessBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.assessRead.gluModel, this.sectionButtonMap['ASSESS']);
			} else {
				this.resizeMaxSection(this.assessEdit.gluModel, this.sectionButtonMap['ASSESS']);
			}
		}
	},
	assessBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['ASSESS'];
	},
	assessBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-eye-open';
		}
		return '';
	},
	assessBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.assessText;
		}
		return '';
	},
	assessBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.assessText;
		}
		return '';
	},

	outputsText: '',
	outputsBtn: function() {
		if (!this.outputsBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.assignValuesToOutputsRead.gluModel, this.sectionButtonMap['OUTPUT']);
			} else {
				this.resizeMaxSection(this.assignValuesToOutputsEdit.gluModel, this.sectionButtonMap['OUTPUT']);
			}
		}
	},
	outputsBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['OUTPUT'];
	},
	outputsBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-circle-arrow-right';
		}
		return '';
	},
	outputsBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.outputsText;
		}
		return '';
	},
	outputsBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.outputsText;
		}
		return '';
	},

	severityConditionsText: '',
	severityConditionsBtn: function() {
		if (!this.severityConditionsBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.severitiesAndConditionsRead.gluModel, this.sectionButtonMap['SEVERITYCONDITION']);
			} else {
				this.resizeMaxSection(this.severitiesEdit.gluModel, this.sectionButtonMap['SEVERITYCONDITION']);
			}
		}
	},
	severityConditionsBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['SEVERITYCONDITION'];
	},
	severityConditionsBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-flag-alt';
		}
		return '';
	},
	severityConditionsBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.severityConditionsText;
		}
		return '';
	},
	severityConditionsBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.severityConditionsText;
		}
		return '';
	},

	summaryDetailsText: '',
	summaryDetailsBtn: function() {
		if (!this.summaryDetailsBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.summaryAndDetailsRead.gluModel, this.sectionButtonMap['SUMMARYDETAILS']);
			} else {
				this.resizeMaxSection(this.summaryEdit.gluModel, this.sectionButtonMap['SUMMARYDETAILS']);
			}
		}
	},
	summaryDetailsBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['SUMMARYDETAILS'];
	},
	summaryDetailsBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-file-text-alt';
		}
		return '';
	},
	summaryDetailsBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.summaryDetailsText;
		}
		return '';
	},
	summaryDetailsBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.summaryDetailsText;
		}
		return '';
	},

	mockText: '',
	mockBtn: function() {
		if (!this.mockBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.mockRead.gluModel, this.sectionButtonMap['MOCK']);
			} else {
				this.resizeMaxSection(this.mockEdit.gluModel, this.sectionButtonMap['MOCK']);
			}
		}
	},
	mockBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['MOCK'];
	},
	mockBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-exchange';
		}
		return '';
	},
	mockBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.mockText;
		}
		return '';
	},
	mockBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.mockText;
		}
		return '';
	},

	tagsText: '',
	tagsBtn: function() {
		if (!this.tagsBtnIsPressed) {
			if (this.readOnlyMode) {
				this.resizeMaxSection(this.tagsRead.gluModel, this.sectionButtonMap['TAGS']);
			} else {
				this.resizeMaxSection(this.tagsEdit.gluModel, this.sectionButtonMap['TAGS']);
			}
		}
	},
	tagsBtnIsPressed$: function() {
		return this.sectionButtonSelected == this.sectionButtonMap['TAGS'];
	},
	tagsBtnIcon$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['TEXTANDICON'] || this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return 'rs-social-button icon-tags';
		}
		return '';
	},
	tagsBtnTooltip$: function() {
		if (this.toolbarSetting == this.toolbarSettingMap['ICONONLY']) {
			return this.tagsText;
		}
		return '';
	},
	tagsBtnText$: function() {
		if (this.toolbarSetting != this.toolbarSettingMap['ICONONLY']) {
			return this.tagsText;
		}
		return '';
	},

	expandAllDisabled$: function() {
		return (this.isNew || this.numSectionsOpen > 0 || this.isMaxScreenSize);
	},
	expandAll: function() {
		for (var i=0; i < this.itemList.length; i++) {
			var item = this.itemList.getAt(i);
			if (!item.hidden && item.collapsed) {
				item.set('collapsed', false);
			}
		}
	},

	collapseAllDisabled$: function() {
		return (this.isNew || this.numSectionsOpen == 0 || this.isMaxScreenSize);
	},
	collapseAll: function() {
		this.closeDetailSections();
		for (var i=0; i < this.itemList.length; i++) {
			var item = this.itemList.getAt(i);
			if (!item.hidden) {
				item.set('collapsed', true);
			}
		}
		setTimeout(function() {
			if (this.numSectionsOpen) {
				this.set('numSectionsOpen', 0);
			}
		}.bind(this), 500);
	},
	syncExpandedSections: function() {
		for (var i=0; i < this.itemList.length; i++) {
			var item = this.itemList.getAt(i);
			if (!item.hidden) {
				if (this.sectionsOpenList[item.blockModelClass]) {
					if (item.collapsed) {
						item.set('collapsed', false);
					}
				} else {
					if (!item.collapsed) {
						item.set('collapsed', true);
					}
				}
			}
		}
		this.syncExpandedSectionsFlag = false;
	},

	expandCollapseSeparatorHidden$: function() {
		return this.collapseAllDisabled && this.expandAllDisabled;
	},

	getAllTags: function (onSuccess, onFailure) {
		this.set('getAllTagsInProgress', true);
		this.ajax({
			url: this.API['getAllTags'],
			method: 'get',
			scope: this,
			success: function (response) {
				var payload = RS.common.parsePayload(response);

				if (payload.success) {
					if (Ext.isFunction(onSuccess)) {
						onSuccess(payload.records);
					}
				} else {
					//clientVM.displayError(this.localize('serverError', payload.message));
					if (Ext.isFunction(onFailure)) {
						onFailure(response);
					}
				}
			},
			failure: function (response) {
				clientVM.displayFailure(response);
				if (Ext.isFunction(onFailure)) {
					onFailure(response);
				}
			},
			callback: function() {
				this.set('getAllTagsInProgress', false);
			}
		});
	},

	getAllATProperties: function(onSuccess, onFailure) {
		this.set('getAllATPropertiesInProgress', true);
		this.ajax({
			url: this.API['getAllATProperties'],
			scope: this,
			success : function (response){
				var payload = RS.common.parsePayload(response);

				if (payload.success) {
					if (Ext.isFunction(onSuccess)) {
						onSuccess(payload.records);
					}
				} else {
					//clientVM.displayError(this.localize('serverError', payload.message));
					if (Ext.isFunction(onFailure)) {
						onFailure(response);
					}
				}
			},
			failure: function (response) {
				clientVM.displayFailure(response);
				if (Ext.isFunction(onFailure)) {
					onFailure(response);
				}
			},
			callback: function() {
				this.set('getAllATPropertiesInProgress', false);
			}
		});
	}

});

Ext.define('RS.actiontaskbuilder.MockEditBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Mock',
		hidden: true,
		collapsed: true,
		editBlock: true,
		title: '',
		gridIsEmpty: false,
		parametersGridIsEmpty: false,
		typeStore: null,
		enterMockContentNameTxt: '',
		selectMockContentNameTxt: '',
		mockContentNameEmptyTxt: '',

		header: {
			titlePosition: 0,
			items: []
		},

		mockStore: {
			mtype: 'store',
			fields: ['id', 'uname', 'udescription', 'udata', 'uparams', 'uinputs', 'uflows', 'sysUpdatedBy', 'sysUpdatedOn', 'sysCreatedBy', 'sysCreatedOn'],
			//model: 'RS.actiontask.model.Mocks',
			proxy: {
				type: 'memory'
			}
		},

		mockDetailsStore: {
			mtype: 'store',
			model: 'RS.actiontask.model.MockDetails',
			proxy: {
				type: 'memory'
			}
		},

		allMockInputParametersStore: {
			mtype: 'store',
			model: 'RS.actiontask.model.MockDetails',
			proxy: {
				type: 'memory'
			}
		},

		remainingMockInputParametersStore: {
			mtype: 'store',
			model: 'RS.actiontask.model.MockDetails',
			proxy: {
				type: 'memory'
			}
		},
							
		postInit: function (blockModel) {
			this.blockModel = blockModel;

			var config = {
				mtype: 'store',
				fields: ['name', 'value'],
				data: blockModel.createNameValueItems('input', 'flow', 'param')
			};

			this.set('typeStore', Ext.create('Ext.data.Store', config));
			this.set('enterMockContentNameTxt', this.localize('enterMockContentName'));
			this.set('selectMockContentNameTxt', this.localize('selectMockContentName'));
			this.set('mockContentNameEmptyTxt', this.enterMockContentNameTxt);

			this.set('mockStore', new RS.common.data.FlatStore({ 
				fields: ['id', 'uname', 'udescription', 'udata', 'uparams', 'uinputs', 'uflows', 'sysUpdatedBy', 'sysUpdatedOn', 'sysCreatedBy', 'sysCreatedOn'],
				listeners : {
					datachanged: function () {
						this.notifyDirty();
					}.bind(this),
					update: function () {
						this.notifyDirty();
					}.bind(this)	
				}
			}));

			this.set('mockDetailsStore', new RS.common.data.FlatStore({ 
				fields: ['type', 'name', 'value'],
				listeners : {
					datachanged: function () {
						if (!this.ignoreDirtyChecking) {
							this.notifyMockDetailsDataDirty();
						}
					}.bind(this),
					update: function () {
						if (!this.ignoreDirtyChecking) {
							this.notifyMockDetailsDataDirty();
						}
					}.bind(this)	
				}
			}));
		},

		notifyDirty: function() {
			this.blockModel.notifyDirty();
			if (this.currentRecord && this.parentVM.mockRead) {
				this.parentVM.mockRead.notifyMockDetailsDataDirty(this.currentRecord);
			}
		},

		mockRenderCompleted: function(gridPanel) {
			gridPanel.up().down('form').loadRecord(new RS.actiontask.model.Mocks());
		},

		mockDetailsRenderCompleted: function(gridPanel) {
			gridPanel.up().up().down('form').loadRecord(new RS.actiontask.model.MockDetails());
		},

		editDetailsTooltip$: function() {
			return this.localize('editMockDetails');
		},

		addMockTooltip$: function() {
			return this.localize('addMock');
		},

		removeMockTooltip$: function() {
			return this.localize('removeMock');
		},

		recordRemoved: function(record) {
			this.mockStore.remove(record);
			this.set('gridIsEmpty', this.mockStore.count() === 0);
		},

		isEditingDetails: false,

		closeIsVisible$: function() {
			return !this.isEditingDetails;
		},

		closeDetailsIsVisible$: function() {
			return this.isEditingDetails;
		},

		Mock: function() {},
		MockIsVisible$: function() {
			return !this.isEditingDetails;
		},

		MockDetails: function() {},
		MockDetailsIsVisible$: function() {
			return this.isEditingDetails;
		},

		id: '',
		mockName: '',
		mockDescription: '',
		mockRaw: '',
		ignoreDirtyChecking: false,
		rawIsHidden: false,
		hasMockDescription: false,

		when_mockRaw_changed: {
			on: ['mockRawChanged'],
			action: function () {
				if (!this.ignoreDirtyChecking) {
					this.notifyMockDetailsDataDirty();
				}
			}
		},

		parameters: function() {},

		currentRecord: null,

		when_isEditingDetails_changed: {
			on: ['isEditingDetailsChanged'],
			action: function () {
				if (this.isEditingDetails) {
					this.set('title', this.localize('mockDetailsTitle') + ' - ' + this.mockName);
				} else {
					this.set('title', this.localize('mockTitle'));
				}
			}
		},

		editMockDetails: function(record) {
			this.set('ignoreDirtyChecking', true);
			this.set('currentRecord', record);
			this.set('type', '');
			this.set('name', '');
			this.set('value', '');
			this.set('mockName', record.get('uname'));
			this.set('mockDescription', record.get('udescription') || '');
			this.set('hasMockDescription', this.mockDescription.length > 0);
			this.set('mockRaw', record.get('udata'));
			this.set('isEditingDetails', true);

			this.mockDetailsStore.removeAll();

			var inputs = record.get('uinputs');
			for (var i = 0; i < inputs.length; i++) {
				this.mockDetailsStore.add({
					type: 'INPUT',
					name: inputs[i].name,
					value: inputs[i].value
				});
			}

			var flows = record.get('uflows');
			for (var i = 0; i < flows.length; i++) {
				this.mockDetailsStore.add({
					type: 'FLOW',
					name: flows[i].name,
					value: flows[i].value
				});
			}

			var params = record.get('uparams');
			for (var i = 0; i < params.length; i++) {
				this.mockDetailsStore.add({
					type: 'PARAM',
					name: params[i].name,
					value: params[i].value
				});
			}

			this.getAllInputParametersData();
			this.getRemainingInputParametersData();

			this.set('ignoreDirtyChecking', false);
			this.set('parametersGridIsEmpty', this.mockDetailsStore.count() === 0);
		},

		getAllInputParametersData: function() {
			this.allMockInputParametersStore.removeAll();

			var parametersBlockData = this.parentVM.parametersInputEdit.getData();
			if (parametersBlockData.length) {
				var params = parametersBlockData[0];
				for (var i=0; i< params.length; i++) {
					if (params[i].utype === 'INPUT' || params[i].utype === null) {
						var name = params[i].uname;
						var param = {
							name: name,
							value: params[i].udefaultValue,
							type: 'INPUT'
						};
						this.allMockInputParametersStore.add(param);
					}
				}
				this.allMockInputParametersStore.sort('order');
			}
		},

		getRemainingInputParametersData: function(currentSelectedName) {
			this.remainingMockInputParametersStore.removeAll();

			for (var i = 0; i < this.allMockInputParametersStore.getCount(); i++) {
				var inputParam = this.allMockInputParametersStore.getAt(i);
				var type = inputParam.get('type');
				if (type == 'INPUT') {
					var name = inputParam.get('name');
					var param = {
						name: name,
						value: inputParam.get('value'),
						type: 'INPUT'
					};

					var addToStore = false;

					if (currentSelectedName && currentSelectedName == name) {
						addToStore = true;
					}
					else {
						var pos = this.mockDetailsStore.findExact('name', name);
						if (pos == -1) {
							addToStore = true;
						}
						else {
							var rec = this.mockDetailsStore.getAt(pos);
							if (rec.type == 'INPUT') {
								addToStore = true;
							}
						}
					}
	
					if (addToStore) {
						this.remainingMockInputParametersStore.add(param);
					}
				}
			}

			if (this.remainingMockInputParametersStore.getCount()) {
				if (this.typeStore.findExact('name', 'INPUT') == -1) {
					this.typeStore.insert(0, {
						name: 'INPUT',
						value: 'input'
					});
				}
			}
			else {
				var inputIdx = this.typeStore.findExact('name', 'INPUT');
				if (inputIdx != -1) {
					this.typeStore.removeAt(inputIdx);
				}
			}
		},

		clearMockDetailsInputParametersData: function() {
			this.remainingMockInputParametersStore.removeAll();
		},	

		uname: '',
		udescription: '',
		type: '',
		name: '',
		value: '',		
		validNameRegEx : RS.common.validation.VariableName,
		nameValidation : function(field, store){
			if(this[field]){
				var store = this[store];
				
				//Check for valid characters
				if (!this.validNameRegEx.test(this[field])){
					return this.localize('validKeyCharacters');					
				}
				//Check for duplicate
				else if (store.findExact(field, this[field]) != -1) {
					return this.localize('duplicateMockName', this[field]);
				}
				else
					return true;
			}		
		},
		validateNameOnEdit : function(context, field, store){
			if(context.field == field){
				var newVal = context.value;
				var currentRowIdx = context.rowIdx;

				if(newVal == ""){
					this.displayInvalidNameMsg('emptyMockName');
					return false;
				}
				else if (!this.validNameRegEx.test(newVal)){
					this.displayInvalidNameMsg('validKeyCharacters');
					return false;				
				}
				//Check for duplicate
				else if (this[store].findExact(field, newVal) != currentRowIdx && this[store].findExact(field, newVal) != -1) {
					this.displayInvalidNameMsg('duplicateMockName', newVal);
					return false;
				}
				else
					return true;
			}
		},
		unameIsValid$: function(){
			if(this.uname)
				return this.nameValidation('uname','mockStore');				
		},
		
		//Name of parameter in mock detail.
		nameIsValid$ : function(){
			if(this.name)
				return this.nameValidation('name','mockDetailsStore');	
		},

		displayInvalidNameMsg : function(msg, extraVariables){
			clientVM.message(this.localize('invalidMockTitle'), this.localize(msg,extraVariables));		
		},

		isMockDetailsNameEditable: false,
		isMockDetailsEditNameEditable: false,

		mockDetailsEditTypesSelected: function(combo) {
			var mockDetailsEditNameCombo = combo.up().up().queryById('mockDetailsEditNameCombo');
			var mockType = combo.value;

			if (mockType === 'INPUT') {
				this.set('isMockDetailsEditNameEditable', false);
				this.set('mockContentNameEmptyTxt', this.selectMockContentNameTxt); 
				mockDetailsEditNameCombo.emptyText = this.selectMockContentNameTxt;
				this.getRemainingInputParametersData();
			} else {
				this.set('isMockDetailsEditNameEditable', true);
				this.set('mockContentNameEmptyTxt', this.enterMockContentNameTxt); 
				mockDetailsEditNameCombo.emptyText = this.enterMockContentNameTxt;
				this.clearMockDetailsInputParametersData();
			}
		},

		mockDetailsEditNameSelected: function(combo) {
			var mockType = combo.ownerCt.grid.getSelectionModel().getSelection()[0].get('type');

			if (mockType === 'INPUT') {
				this.set('name', '');
				this.set('type', '');
				this.set('value', '');
				this.getRemainingInputParametersData();
			} else {
				this.clearMockDetailsInputParametersData();
			}
		},

		populateMockDetailsDataSources: function (context) {
			var d = context.record.data;
			var editor = context.column.getEditor(context.record);

			switch(context.colIdx) {
			case 0:
				if (d.type === 'INPUT') {
					this.getRemainingInputParametersData();
				} else {
					this.clearMockDetailsInputParametersData();
				}
				break;
			case 1:
				if (d.type === 'INPUT') {
					this.getRemainingInputParametersData(d.name);
					editor.emptyText = this.selectMockContentNameTxt;
					editor.setEditable(false);
				} else {
					this.clearMockDetailsInputParametersData();
					editor.emptyText = this.enterMockContentNameTxt;
					editor.setEditable(true);
				}

				editor.applyEmptyText();
				break;
			}
		},

		newMockDetailNameFocus: function() {
			if (this.type && this.type === 'INPUT') {
				this.getRemainingInputParametersData();
			} else {
				this.clearMockDetailsInputParametersData();
			}
		},

		addMock: function() {
			var store = this.mockStore,
				name = this.uname,
				description = this.udescription;			

			if (store.findExact('uname', name) === -1) {
				store.add({
					uname: name,
					udescription: description
				});

				this.set('gridIsEmpty', this.mockStore.count() === 0);
			}

			this.resetFormMockForm();
		},

		addMockIsEnabled$ : function(){
			return this.unameIsValid == true && this.uname !== '';
		},

		resetFormMockForm : function(){
			this.set('uname', '');
			this.set('udescription', '');
		},

		recordParamRemoved: function(record) {
			this.mockDetailsStore.remove(record);
			this.set('parametersGridIsEmpty', this.mockDetailsStore.count() === 0);

			if (record.get('type') === 'INPUT') {
				this.getRemainingInputParametersData();
			}
		},

		addParams: function(form) {
			var store = this.mockDetailsStore,
				type, name, value;		
		
			type = this.type;
			name = this.name;
			value = this.value;

			if (store.findExact('name', name) === -1) {
				var param = {
					type: type,
					name: name,
					value: value
				};
				store.add(param);
				this.set('parametersGridIsEmpty', this.mockDetailsStore.count() === 0);
				
				if (type === 'INPUT') {
					this.getRemainingInputParametersData();
				}
			}

			this.resetParameterForm();
		},

		parametersMesg$: function() {
			if (this.parametersGridIsEmpty) {
				return this.localize('emptyMockParamMsg');
			} else {
				return '';
			}
		},

		addParamsIsEnabled$ : function(){
			return this.nameIsValid == true && this.name !== '' && this.value !== '';
		},
		
		resetParameterForm : function(){
			this.set('type', null);
			this.set('name', '');
			this.set('value', null);
		},		

		mockDetailsEditTypeFocus: function() {
			this.getRemainingInputParametersData();
		},

		newMockDetailTypeFocus: function() {
			this.getRemainingInputParametersData();
		},

		mockDetailTypeSelected: function(combo) {
			var mockDetailsNameCombo = combo.ownerCt.ownerCt.queryById('mockDetailsNameCombo');
			var mockType = combo.value;

			if (mockType === 'INPUT') {
				this.set('isMockDetailsNameEditable', false);
				this.set('mockContentNameEmptyTxt', this.selectMockContentNameTxt); 
				mockDetailsNameCombo.emptyText = this.selectMockContentNameTxt;
				this.getRemainingInputParametersData();
			} else {
				this.set('isMockDetailsNameEditable', true);
				this.set('mockContentNameEmptyTxt', this.enterMockContentNameTxt); 
				mockDetailsNameCombo.emptyText = this.enterMockContentNameTxt;
				this.clearMockDetailsInputParametersData();
			}
			this.set('name', '');
			mockDetailsNameCombo.applyEmptyText();
		},

		notifyMockDetailsDataDirty: function() {
			if (this.currentRecord) {
				this.currentRecord.set('uname', this.mockName);
				this.currentRecord.set('udescription', this.mockDescription);
				this.currentRecord.set('udata', this.mockRaw);
	
				var uinputs = [],
					uflows = [],
					uparams = [];
	
				for (var i = 0; i < this.mockDetailsStore.getCount(); i++) {
					var param = this.mockDetailsStore.getAt(i);
					var type = param.get('type');
					var content = {
						name: param.get('name'),
						value: param.get('value')
					};
					if (type === 'INPUT') {
						uinputs.push(content);
					}
					else if (type === 'FLOW') {
						uflows.push(content);
					}
					else if (type === 'PARAM') {
						uparams.push(content);
					}
				}
	
				this.currentRecord.set('uinputs', uinputs);
				this.currentRecord.set('uflows', uflows);
				this.currentRecord.set('uparams', uparams);
				
				this.notifyDirty();
			}
		},

		closeDetails: function () {
			this.set('isEditingDetails', false);
			this.notifyMockDetailsDataDirty();
			this.set('currentRecord', null);
		},

		closeFlag: false,
		close: function () {
			this.set('isEditingDetails', false);
			this.set('currentRecord', null);

			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.resolveActionTaskMockData'];
	},

	getData: function () {
		var resolveActionTaskMockData = [];

		var nameMap = {};
		var collectResult = {
			success: true
		};

		this.gluModel.mockStore.each(function(mock) {
			if (nameMap[mock.get('uname')]) {
				collectResult = this.gatherDataError('duplicateMockName', {
					name: mock.get('uname')
				});
				return true;
			}
			nameMap[mock.get('uname')] = true;
			var uinputs = this.convertArrToJson(mock, 'uinputs', 'duplicateMockInput');
			if (uinputs.success == false) {
				collectResult = uinputs;
				return;
			}
			var uflows = this.convertArrToJson(mock, 'uflows', 'duplicateMockFlow');
			if (uflows.success == false) {
				collectResult = uflows;
				return;
			}
			var uparams = this.convertArrToJson(mock, 'uparams', 'duplicateMockParam');
			if (uparams.success == false) {
				collectResult = uparams;
				return;
			}

			resolveActionTaskMockData.push({
				id: mock.get('id'),
				uname: mock.get('uname'),
				udescription: mock.get('udescription'),
				udata: mock.get('udata'),
				uinputs: Ext.JSON.encode(uinputs),
				uflows: Ext.JSON.encode(uflows),
				uparams: Ext.JSON.encode(uparams)
			});
		}, this);

		return [resolveActionTaskMockData];
	},

	load: function (resolveActionTaskMockData) {
		resolveActionTaskMockData = resolveActionTaskMockData || [];
		this.callParent(arguments);
		this.gluModel.mockStore.removeAll();

		if (resolveActionTaskMockData) {
			for (var i = 0; i < resolveActionTaskMockData.length; i++) {
				var mock = resolveActionTaskMockData[i];
				if (this.gluModel.currentRecord && this.gluModel.currentRecord.get('id') == mock.id) {
					this.gluModel.mockStore.insert(0, this.gluModel.currentRecord);
					continue;
				}

				var uinputs = this.jsonStrToArray(mock.uinputs),
					uflows = this.jsonStrToArray(mock.uflows),
					uparams = this.jsonStrToArray(mock.uparams);

				this.gluModel.mockStore.add({
					id: mock.id || '',
					uname: mock.uname || '',
					udescription: mock.udescription || '',
					udata: mock.udata,
					uparams: uparams,
					uinputs: uinputs,
					uflows: uflows,
					sysUpdatedBy: mock.sysUpdatedBy,
					sysUpdatedOn: mock.sysUpdatedOn,
					sysCreatedBy: mock.sysCreatedBy,
					sysCreatedOn: mock.sysCreatedOn
				});				
			}
		}

		this.set('gridIsEmpty', this.gluModel.mockStore.count() === 0);
	},

	jsonStrToArray: function(jsonStr) {
		if (!jsonStr)
			return [];
		var arr = [];
		var json = Ext.JSON.decode(jsonStr);
		for (key in json) {
			arr.push({
				name: key,
				value: json[key]
			})
		}
		return arr;
	},

	convertArrToJson: function(mock, field, errMsg) {
		if (!mock.get(field)) return {};
		var keyValue = {};
		var collectResult = null;
		Ext.each(mock.get(field), function(r) {
			if (keyValue[r.name]) {
				collectResult = this.gatherDataError(errMsg, {
					mock: mock.get('uname'),
					name: r.name
				});
			}
			keyValue[r.name] = r.value
		}, this);
		if (collectResult)
			return collectResult;
		return keyValue;
	},

	gatherDataError: function(templateMsg, templateParam) {
		return {
			success: false,
			msg: this.gluModel.localize(templateMsg, templateParam)
		};
	},

	modifyByNotification: function (data, resolveActionInvoc) {
		this.gluModel.getAllInputParametersData();
		this.gluModel.getRemainingInputParametersData();
		this.set('rawIsHidden', resolveActionInvoc.utype === 'ASSESS');
	}
});
Ext.define('RS.actiontaskbuilder.MockReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Mock',
		classList: 'block-model',
		collapsed: true,
		hidden: false,
        title: '',
        header: {
            titlePosition: 0,
            defaults: {
            	margin: '0'
        	},
            items: []
        },

		mockStore: {
			mtype: 'store',
			fields: ['id', 'uname', 'udescription', 'udata', 'uparams', 'uinputs', 'uflows', 'sysUpdatedBy', 'sysUpdatedOn', 'sysCreatedBy', 'sysCreatedOn'],
			proxy: {
				type: 'memory'
			}
		},

		mockDetailsStore: {
			mtype: 'store',
			fields: ['type', 'name', 'value'],
			proxy: {
				type: 'memory'
			}
		},

		maxHeight: 300,
		hasMockValues: false,
		hasMockParamValues: false,
		rawIsHidden: false,
		hasMockDescription: false,

		postInit: function (blockModel) {
			this.blockModel = blockModel;
			this.maxHeight = Math.round(clientVM.getWindowHeight() / 3);
		},

		isShowingDetails: false,

		Mock: function() {},
		MockIsVisible$: function() {
			return !this.isShowingDetails;
		},

		MockDetails: function() {
		},
		MockDetailsIsVisible$: function() {
			return this.isShowingDetails;
		},

		viewDetailsTooltip$: function() {
			return this.localize('viewMockDetails');
		},

		mockName: '',
		mockDescription: '',
		mockRaw: '',

		when_isShowingDetails_changed: {
			on: ['isShowingDetailsChanged'],
			action: function () {
				if (this.isShowingDetails) {
					this.set('title', this.localize('mockDetailsTitle') + ' - ' + this.mockName);
				} else {
					this.set('title', this.localize('mockTitle'));
				}
			}
		},

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden && this.parentVM.embed) {
					var mockData = this.parentVM.mockEdit.getData();
					this.parentVM.mockRead.load(mockData[0]);
				}
			}
		},

		viewMockDetails: function(record) {
			this.set('mockName', record.get('uname'));
			this.set('mockDescription', record.get('udescription') || '');
			this.set('isShowingDetails', true);
			this.set('mockRaw', record.get('udata'));
			this.set('hasMockDescription', this.mockDescription.length > 0);
			this.mockDetailsStore.removeAll();

			var inputs = record.get('uinputs');
			for (var i = 0; i < inputs.length; i++) {
				this.mockDetailsStore.add({
					type: 'INPUT',
					name: inputs[i].name,
					value: inputs[i].value
				});
			}

			var flows = record.get('uflows');
			for (var i = 0; i < flows.length; i++) {
				this.mockDetailsStore.add({
					type: 'FLOW',
					name: flows[i].name,
					value: flows[i].value
				});
			}

			var params = record.get('uparams');
			for (var i = 0; i < params.length; i++) {
				this.mockDetailsStore.add({
					type: 'PARAMS',
					name: params[i].name,
					value: params[i].value
				});
			}

			var hasMockParamValues = this.mockDetailsStore.getCount() > 0;
			this.set('hasMockParamValues', hasMockParamValues);
		},

		notifyMockDetailsDataDirty: function(record) {
			if (this.isShowingDetails) {
				this.set('mockName', record.get('uname'));
				this.set('mockDescription', record.get('udescription') || '');
				this.set('mockRaw', record.get('udata'));
				this.set('hasMockDescription', this.mockDescription.length > 0);
				this.mockDetailsStore.removeAll();
	
				var inputs = record.get('uinputs');
				for (var i = 0; i < inputs.length; i++) {
					this.mockDetailsStore.add({
						type: 'INPUT',
						name: inputs[i].name,
						value: inputs[i].value
					});
				}
	
				var flows = record.get('uflows');
				for (var i = 0; i < flows.length; i++) {
					this.mockDetailsStore.add({
						type: 'FLOW',
						name: flows[i].name,
						value: flows[i].value
					});
				}
	
				var params = record.get('uparams');
				for (var i = 0; i < params.length; i++) {
					this.mockDetailsStore.add({
						type: 'PARAMS',
						name: params[i].name,
						value: params[i].value
					});
				}
	
				var hasMockParamValues = this.mockDetailsStore.getCount() > 0;
				this.set('hasMockParamValues', hasMockParamValues);
			}
		},

		back: function () {
			this.set('isShowingDetails', false);			
		},

		backIsVisible$: function() {
			return this.isShowingDetails;
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.resolveActionTaskMockData'];
	},

	load: function (resolveActionTaskMockData) {
		resolveActionTaskMockData = resolveActionTaskMockData || [];
		this.callParent(arguments);
		this.gluModel.mockStore.removeAll();

		for (var i = 0; i < resolveActionTaskMockData.length; i++) {
			var mock = resolveActionTaskMockData[i];

			var uinputs = this.jsonStrToArray(mock.uinputs),
				uflows = this.jsonStrToArray(mock.uflows),
				uparams = this.jsonStrToArray(mock.uparams);

			this.gluModel.mockStore.add({
				id: mock.id || '',
				uname: mock.uname || '',
				udescription: mock.udescription || '',
				udata: mock.udata,
				uparams: uparams,
				uinputs: uinputs,
				uflows: uflows,
				sysUpdatedBy: mock.sysUpdatedBy,
				sysUpdatedOn: mock.sysUpdatedOn,
				sysCreatedBy: mock.sysCreatedBy,
				sysCreatedOn: mock.sysCreatedOn
			});
		}

		var hasMockValues = resolveActionTaskMockData.length > 0,
			classes = ['block-model'];

		this.set('hasMockValues', hasMockValues)
			.set('classList', classes);
	},

	jsonStrToArray: function(jsonStr) {
		if (!jsonStr)
			return [];
		var arr = [];
		var json = Ext.JSON.decode(jsonStr);
		for (key in json) {
			arr.push({
				name: key,
				value: json[key]
			})
		}
		return arr;
	},

	modifyByNotification: function (data, resolveActionInvoc) {
		this.set('rawIsHidden', resolveActionInvoc.utype === 'ASSESS');
	},

	notifyMockDetailsDataDirty: function (record) {
		if (this.gluModel.isShowingDetails) {
			this.gluModel.notifyMockDetailsDataDirty(record);
		}
	}

});

glu.defModel('RS.actiontaskbuilder.OptionEditDetail', {
	uname: null,
	uvalue: '',
	udescription: '',
	optionsStore: null,
	defaultOptionsMap: {},	

	init: function () {
		this.set('optionsStore', new RS.common.data.FlatStore({
			fields: ['uname', 'udescription']
		}).accessors([
			function (o) { return o.uname; },
			function (o) { return o.udescription; }
		]));
	},

	addOption: function (optionName) {
		this.set('uname', null);
		this.set('udescription', '');
		this.optionsStore.add({
			uname: optionName,
			udescription: this.defaultOptionsMap[optionName]
		});
	},

	removeOption: function (optionName) {
		this.set('uname', null);
		this.set('udescription', '');
		this.optionsStore.removeBy('uname', optionName);
	},

	addIsDisabled$: function () {		
		return !this.uname || this.uname.length === 0;
	},

	addIsEnabled$: function () {
		return this.uname && this.uname.length > 1;
	},

	setOptions: function (options) {
		for (var i = 0; i < options.length; i++) {
			this.defaultOptionsMap[options[i].uname] = options[i].udescription;
		}

		this.optionsStore.loadData(options)
	},

	add: function () {
		this.optionsStore.removeBy('uname', this.uname);
		this.parentVM.add(this);
		this.set('uname', '');
		this.set('uvalue', '');
		this.set('udescription', '');
	},

	addNewOptionBlankValues: function (paramName) {
		this.set('uname', 'BLANKVALUES');
		this.set('uvalue', '$INPUT{'+paramName+'}');
		this.set('udescription', 'Replace matching values with *****. Use $CONFIGname for Registration properties and $TARGETname for Proxy Target properties. Separate multiple values with a comma');
		this.parentVM.add(this);
		this.set('uname', '');
		this.set('uvalue', '');
		this.set('udescription', '');
	},

	addRemainingOption: function (option) {
		this.optionsStore.add(option);
	},

	focusOption: function (e) {
		if (e && e.getKey && typeof e.getKey() !== 'undefined' && e.currentTarget) {
			var id = e.currentTarget.id;

			if (id && id.length > 0) {
				var componentID = id.substring(0, id.indexOf('-inputEl'));
				var combo = Ext.ComponentQuery.query('*[id^='+ componentID +']');

				if (combo && combo.length > 0) {
					combo[0].expand();
				}
			}
		}
	},

	selectOption: function (records) {
		if (records[0]) {
			this.set('udescription', records[0].data.udescription);	
		}		
	}
});
Ext.define('RS.actiontaskbuilder.OptionsEditBlockModel', (function () {
	var defaultOptions = [{
		uname: 'QUEUE_NAME',
		udescription: 'Name of the JMS Queue to send the ActionTask request'
	}, {
		uname: 'EXECPATH',
		udescription: 'OS execution path location'
	}, {
		uname: 'CMDLINE_EXCLUDE',
		udescription: 'Exclude the command line from being displayed in the result RAW output'
	}, {
		uname: 'RESULTTYPE',
		udescription: 'Method for retrieving result value e.g. FILE, STDOUTERR (default)'
	}, {
		uname: 'RESULTFILE',
		udescription: 'Filename of the result file to be retrieved (RESULTTYPE must be set to FILE). Set RESULTFILE to "$TMPFILE" to generate the filename dynamically at execution time'
	}, {
		uname: 'RESULTFILE_REMOVE',
		udescription: 'Remove resultfile on completion (default: false)'
	}, {
		uname: 'RESULTFILE_WAIT',
		udescription: 'Wait the specified number of seconds before retrieving RESULTFILE'
	}, {
		uname: 'CLASSPATH',
		udescription: 'Additonal Java classpath'
	}, {
		uname: 'JAVAOPTIONS',
		udescription: 'Additonal Java command-line options'
	}, {
		uname: 'TIMEOUT_DESTROY_THREAD',
		udescription: 'Force destroy thread (warning: may leak resources, etc)'
	}, {
		uname: 'TMPFILE_PREFIX',
		udescription: 'Prefix string prepended to tmpfile (default: blank)'
	}, {
		uname: 'TMPFILE_POSTFIX',
		udescription: 'Postfix string appended to tmpfile (default: .tmp)'
	}, {
		uname: 'TMPFILE_REMOVE',
		udescription: 'Remove tmpfile on completion (default: true)'
	}, {
		uname: 'INPUTFILE_POSTFIX',
		udescription: 'Filename extension to use for the temporary INPUTFILE (default: .in)'
	}, {
		uname: 'INPUTFILE_ENCODING',
		udescription: 'file encoding for input file to be generated'
	}, {
		uname: 'BLANKVALUES',
		udescription: 'Replace matching values with *****. Use $CONFIGname for Registration properties and $TARGETname for Proxy Target properties. Separate multiple values with a comma'
	}, {
		uname: 'TAILLENGTH',
		udescription: 'Number of lines to retrieve from the end of the file'
	}, {
		uname: 'EXTERNAL_TYPE',
		udescription: ''
	}, {
		uname: 'METRIC_GROUP',
		udescription: 'Metric group'
	}, {
		uname: 'METRIC_ID',
		udescription: 'Metric ID'
	}, {
		uname: 'METRIC_UNIT',
		udescription: 'Metric unit'
	}];

	function getRemainingOptions(options) {
		var remainingOptions = defaultOptions.slice();

		for (var i = 0; i < options.length; i++) {
			for (var j = 0; j < remainingOptions.length; j++) {
				if (options[i].uname === remainingOptions[j].uname) {
					remainingOptions.splice(j, 1);
					break;
				}
			}
		}

		return remainingOptions;
	}

	function getDefaultOptionByOptionName(optionName) {
		var option = null;

		for (var i = 0; i < defaultOptions.length; i++) {
			if (defaultOptions[i].uname === optionName) {
				option = defaultOptions[i];
				break;
			}
		}

		return option;
	}

	return {
		extend: 'RS.common.blocks.BlockModel',

		modelConfig: {
			mixins: ['QuickAccessHelpers'],
			blockModel: null,
			blockModelClass: 'General',
			hidden: true,
			collapsed: true,
			editSubBlock: true,
			stopPropagation: false,
			title: '',
			header: {
		 		xtype: 'toolbar',
		    	margin: 0,
		    	padding: 0,
		    	items: []
			},			
			optionsStore: null,
			allowedOptionsForGridRow: null,
			detail: {
				mtype: 'OptionEditDetail'
			},
			savingNewTask: false,
			gridIsEmpty: false,
			itemsExhausted: false,
			
			postInit: function (blockModel) {
				this.blockModel = blockModel;
				this.set('allowedOptionsForGridRow', new RS.common.data.FlatStore({
					fields: ['uname', 'udescription']
				}));
				this.set('optionsStore', new RS.common.data.FlatStore({
					fields: ['uname', 'uvalue', 'udescription'],
					listeners: {
						datachanged: function () {
							this.notifyDirty();
						}.bind(this),
						update: function (record, operation) {
							var modified = operation.modified;

							if (modified.hasOwnProperty('uname') && modified.uname !== operation.data.uname) {
								this.detail.removeOption(operation.data.uname);
								this.detail.addOption(modified.uname);
							}

							this.notifyDirty();
						}.bind(this)						
					}
				}));
				this.header.items = [{
		        	xtype: 'component',
		        	padding: '0 0 0 15',
		        	cls: 'x-header-text x-panel-header-text-container-default',
		        	html: this.localize('generalTitle'),
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
							}, this);
						}.bind(this)
					}
		        }, {
		        	xtype: 'container',
		        	autoEl: 'ol',
		        	cls: 'breadcrumb flat',
		        	margin: 0,
		        	padding: 0,
		        	items: [{
			    		xtype: 'component',
			    		autoEl: 'li',
			    		html: this.localize('generalPropertiesEditTitle'),
						listeners: {
							afterrender: function (c) {
								c.getEl().on('click', function () {
									this.stopPropagation = true;
									this.jumpTo('GeneralEdit', this.parentVM.generalEdit.gluModel);
					            }, this);
							}.bind(this)
			    		}			    		
			    	}, {
			    		xtype: 'component',
			    		autoEl: 'li',
						html: this.localize('rolesEditTitle'),
						listeners: {
							afterrender: function (c) {
								c.getEl().on('click', function () {
									this.stopPropagation = true;
									this.jumpTo('RolesEdit', this.parentVM.rolesEdit.gluModel);
					            }, this);
							}.bind(this)
			    		}			   
			    	}, {
			    		xtype: 'component',
			    		autoEl: 'li',
			    		cls: 'active',
			    		html: this.localize('optionsEditTitle'),
						listeners: {
							afterrender: function (c) {
								c.getEl().on('click', function () {
									this.stopPropagation = true;
								}, this);
							}.bind(this)
						}
			        }]
		        }, '->', {
					xtype: 'button',
					tooltip: this.localize('resizeFullscreen'),
					iconCls: 'rs-block-button icon-resize-full',
					isResizeMaxBtn: true,
					margin: '0 5',
					text: '',
					style: {
						opacity: 0.75
					},
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['GENERAL']);
							}, this);
						}.bind(this)
					}
				}, {
					xtype: 'button',
					tooltip: this.localize('resizeNormalscreen'),
					iconCls: 'rs-block-button icon-resize-small',
					hidden: true,
					isResizeNormalBtn: true,
					margin: '0 20',
					text: '',
					style: {
						opacity: 0.75
					},
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.parentVM.resizeNormalSection(this);
							}, this);
						}.bind(this)
					}
				}, {
					xtype: 'button',
					iconCls: 'rs-block-button icon-chevron-sign-up',
					isCollapseBtn: true,
					margin: '0 5',
					text: '',
					style: {
						opacity: 0.75
					},
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.set('collapsed', true);
							}, this);
						}.bind(this)
					}
				}];				
			},

			notifyDirty: function() {
				this.blockModel.notifyDirty();
			},

			selectOption: function (args) {
				var selection = args[0].ownerCt.grid.getSelectionModel().getSelection();
				var originalRecord = selection[0];
				var option = args[1][0].data;

				if (originalRecord.data.uname !== option.uname) {
					originalRecord.set('udescription', option.udescription);
				}
			},

			jumpTo: function(nextBlockName, nextSection){
				this.parentVM.generalEditSectionExpanded = !this.collapsed;
				this.parentVM.generalEditSectionMaximized = this.isSectionMaxSize;
				this.parentVM.collapseAndHideSection(this);
				this.parentVM.showAndExpandThisSection(nextSection);
			}, 

			when_hidden_changed: {
				on: ['hiddenChanged'],
				action: function() {
					if (!this.hidden) {
						setTimeout(function() {
							if (this.parentVM.generalEditSectionExpanded) {
								this.parentVM.generalEditSectionExpanded = null;
								this.set('collapsed', false);
							}
							if (this.parentVM.generalEditSectionMaximized) {
								this.parentVM.generalEditSectionMaximized = null;
								this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['GENERAL']);
							}
						}.bind(this), 10);
					}
				}
			},

			closeFlag: false,
			close: function () {
				if (this.isSectionMaxSize) {
					this.parentVM.resizeNormalSection(this);
				}
				this.closeFlag = true;
				this.set('collapsed', true);
			},
	
			add: function (option) {
				this.optionsStore.add(option);
				var count = this.optionsStore.count();
				this.set('gridIsEmpty', count === 0);
				this.hideAddFormOnExhaust();
			},

			addInputParamToOptionBlankValue: function(paramName) {
				var idx = this.optionsStore.findExact('uname', 'BLANKVALUES');
				if (idx != -1) {
					var record = this.optionsStore.getAt(idx);
					var value = record.get('uvalue');
					var valueList = value.split(',');
					valueList.push('$INPUT{'+paramName+'}');
					record.set('uvalue', valueList.join());
				} else {
					this.detail.addNewOptionBlankValues(paramName);
				}
			},

			removeInputParamFromOptionBlankValue: function(paramName) {
				var idx = this.optionsStore.findExact('uname', 'BLANKVALUES');
				if (idx != -1) {
					var record = this.optionsStore.getAt(idx);
					var value = record.get('uvalue');
					var valueList = value.split(',');
					var i = valueList.indexOf('$INPUT{'+paramName+'}');
					if (i != -1) {
						valueList.splice(i, 1);
						// remove BLANKVALUES option if no values after removing last entry
						if (!valueList.length) {
							this.optionsStore.remove(record);
						} else {
							record.set('uvalue', valueList.join());
						}
					}
				}
			},

			updateAllowedOptions: function(context) {
				var options = this.optionsStore.getData();
				var remainingOptions = getRemainingOptions(options);
				remainingOptions.push(context.value);
				this.allowedOptionsForGridRow.loadData(remainingOptions);
			},

			addOptionToDetail: function (record) {
				this.detail.addOption(record.data.uname);
				this.set('itemsExhausted', false);
				var count = this.optionsStore.count();
				this.set('gridIsEmpty', count === 0);				
			},

			hideAddFormOnExhaust: function() {
				this.set('itemsExhausted', this.optionsStore.count() === defaultOptions.length);
			}
		},

		disableForm: function () {
			this.set('savingNewTask', true);
		},

		enableForm: function () {
			this.set('savingNewTask', false);
		},		

		getDataPaths: function () {
			return [{
				path:'data.resolveActionInvoc.resolveActionInvocOptions',
				filter: function (o) {
					return o.uname !== 'INPUTFILE';
				}
			}];
		},

		getData: function () {
			return [this.gluModel.optionsStore.getData()];
		},

		load: function (options) {
			var g = this.gluModel;
			g.optionsStore.loadData(options);
			var count = g.optionsStore.count();
			this.set('gridIsEmpty', count === 0);
			var remainingOptions = getRemainingOptions(options);
			g.allowedOptionsForGridRow.loadData(remainingOptions);
			g.detail.setOptions(remainingOptions);
			g.hideAddFormOnExhaust();
		}
	};
})());

glu.defModel('RS.actiontaskbuilder.ParameterInputDetail', {
	uname: '',
	udescription: '',
	udefaultValue: '',
	uencrypt: false,
	urequired: true,
	uprotected: false,
	ugroupName: null,
	utype: 'INPUT',
	uorder: 0,
	groupStore: null,
	groupNameMap: {},
	unclassified: '',
	addInputTitle: '',

	init: function () {
		this.set('addInputTitle', this.localize('addInput'));
		this.set('unclassified', this.localize('unclassified'));
		this.set('ugroupName', this.unclassified);
		this.groupNameMap[this.unclassified] = true;
		this.set('groupStore', new RS.common.data.FlatStore({
			sorters: [{
		    	property: 'value',
		    	direction: 'ASC'
			}]
		}));
		this.groupStore.add({
			text: this.unclassified,
			value: '~~__'
		});
	},

	setAddInputButtonTitle: function (title) {
		this.set('addInputTitle', title);
	},

	setGroupsByParameters: function (parameters) {
		var groupNames = [];
		this.groupStore.removeAll();
		this.groupNameMap = [];
		this.groupNameMap[this.unclassified] = true;
		this.set('ugroupName', this.unclassified);

		for (var i = 0; i < parameters.length; i++) {
			var groupName = parameters[i].ugroupName;
			
			if (typeof groupName === 'string' && groupName.length > 0 && !this.groupNameMap[groupName]) {
				groupNames.push(groupName);
				this.groupNameMap[groupName] = true;
			}
		}

		groupNames.sort();
		var shapedGroups = this.groupStore.shapeData(groupNames);

		if (this.groupStore.count() === 0) {
			shapedGroups.unshift({
				text: this.unclassified,
				value: '~~__'
			});
		}
	
		for (var i = 0; i < shapedGroups.length; i++) {
			this.groupStore.suspendEvents();
			var model = this.groupStore.add(shapedGroups[i])[0];
			this.groupStore.remove(model);
			this.groupStore.resumeEvents();
			this.groupStore.addSorted(model);
		}	
	},

	addGroupName: function (groupName) {
		if (!this.groupNameMap[groupName]) {
			var store = this.groupStore,
				unclassified = store.first();
			store.suspendEvents();
			store.removeAt(0);
			var model = store.add(this.groupStore.shapeData(groupName))[0];
			store.remove(model);
			store.insert(0, unclassified);
			store.resumeEvents();
			store.addSorted(model);
			this.groupNameMap[groupName] = true;
			this.set('ugroupName', groupName);
		}
	},

	addInput: function () {
		if (this.ugroupName === '~~__') {
			this.set('ugroupName', this.unclassified);
		}			

		this.parentVM.addInputToList(this);
		this.set('uname', '');
		this.set('udescription', '');
		this.set('udefaultValue', '');
		this.set('uencrypt', false);
		this.set('urequired', true);
		this.set('uprotected', false);
	}
});
glu.defModel('RS.actiontaskbuilder.ParameterOutputDetail', {
	uname: '',
	udescription: '',
	udefaultValue: '',
	uencrypt: false,
	urequired: true,
	ugroupName: null,
	utype: 'OUTPUT',
	uorder: 0,
	addOutputTitle: '',

	setAddOutputButtonTitle: function (title) {
		this.set('addOutputTitle', title);
	},

	addOutput: function () {
		this.parentVM.addOutputToList(this);
		this.set('uname', '');
		this.set('udescription', '');
		this.set('udefaultValue', '');
		this.set('uencrypt', false);
		this.set('urequired', true);
	}
});
Ext.define('RS.actiontaskbuilder.ParametersInputEditBlockModel', {
	extend: 'RS.common.blocks.BlockModel',
	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Parameters',
        title: '',
		hidden: true,
		collapsed: true,
		editBlock: true,
		stopPropagation: false,

       	header: {
	 		xtype: 'toolbar',
	    	margin: 0,
	    	padding: 0,
	    	items: []
	 	},	
	 	fields : ['newGroupName'],
		inputParametersStore: null,
		hasInputParameters: false,

		newGroupName: '',

		detail: {
			mtype: 'ParameterInputDetail'
		},

		unclassified: '',
		
		postInit: function (blockModel, context) {
			this.blockModel = blockModel;			
			this.unclassified = context.localize('unclassified');
			this.detail.setAddInputButtonTitle(context.localize('addInput'));
			this.set('inputParametersStore', new RS.common.data.FlatStore({
				fields: ['id', 'uname', 'udescription', 'udefaultValue', 'uencrypt', 'urequired', 'uprotected', 'ugroupName', 'utype', 'uorder'],
				groupField: 'ugroupName',
				sorters: [{
			    	property: 'value',
			    	direction: 'uorder'
				}],
				listeners: {
					datachanged: function () {
						this.notifyDirty();					
					}.bind(this),
					update: function () {
						this.notifyDirty();						
					}.bind(this)						
				}
			}));
			this.header.items = [{
				xtype: 'component',
				padding: '0 0 0 15',
				cls: 'x-header-text x-panel-header-text-container-default',
				html: this.localize('parametersTitle'),
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
						}, this);
					}.bind(this)
				}
			}, {
	        	xtype: 'container',
	        	autoEl: 'ol',
	        	cls: 'breadcrumb flat',
	        	margin: 0,
	        	padding: 0,
	        	items: [{
		    		xtype: 'component',
		    		autoEl: 'li',
		    		cls: 'active',
		    		html: this.localize('inputParametersEditTitle'),
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
							}, this);
						}.bind(this)
					}
		    	}, {
		    		xtype: 'component',
		    		autoEl: 'li',
		    		html: this.localize('outputParametersEditTitle'),
		    		listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.jumpTo('ParametersOutputEdit', this.parentVM.parametersOutputEdit.gluModel);
				            }, this);						
						}.bind(this)
		    		}
		    	}]
			}, '->', {
				xtype: 'button',
				tooltip: this.localize('resizeFullscreen'),
				iconCls: 'rs-block-button icon-resize-full',
				isResizeMaxBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['PARAMETERS']);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				tooltip: this.localize('resizeNormalscreen'),
				iconCls: 'rs-block-button icon-resize-small',
				hidden: true,
				isResizeNormalBtn: true,
				margin: '0 20',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeNormalSection(this);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				iconCls: 'rs-block-button icon-chevron-sign-up',
				isCollapseBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.set('collapsed', true);
						}, this);
					}.bind(this)
				}
			}];
			this.inputParametersStore.on('add', function(store, records){
				// for Encrypted INPUT variables, automatically add the resolveActionInvocOptions BLANKVALUES with value "$INPUT{<paramName>}"
				if (records && records[0].data.uencrypt) {
					var resolveActionInvocOptions = this.rootVM.optionsEdit.gluModel;
					resolveActionInvocOptions.addInputParamToOptionBlankValue(records[0].data.uname);
				}
			},this);
			this.inputParametersStore.on('remove', function(store, record){
				// for Encrypted INPUT variables, automatically remove the resolveActionInvocOptions BLANKVALUES with value "$INPUT{<paramName>}"
				if (record.data.uencrypt) {
					var resolveActionInvocOptions = this.rootVM.optionsEdit.gluModel;
					resolveActionInvocOptions.removeInputParamFromOptionBlankValue(record.data.uname);
				}
			},this);
			this.inputParametersStore.on('update', function(store, record, operation, modifiedFieldNames){
				if (modifiedFieldNames.length && modifiedFieldNames[0] == 'uencrypt') {
					// for Encrypted INPUT variables, automatically add/remove the resolveActionInvocOptions BLANKVALUES with value "$INPUT{<input.uname>}"
					var resolveActionInvocOptions = this.rootVM.optionsEdit.gluModel;
					if (record.data.uencrypt) {
						resolveActionInvocOptions.addInputParamToOptionBlankValue(record.data.uname);
					} else {
						resolveActionInvocOptions.removeInputParamFromOptionBlankValue(record.data.uname);
					}
				}
			},this);
		},
		
		jumpTo: function(nextBlockName, nextSection){
			this.parentVM.parametersEditSectionExpanded = !this.collapsed;
			this.parentVM.parametersEditSectionMaximized = this.isSectionMaxSize;
			this.parentVM.collapseAndHideSection(this);
			this.parentVM.showAndExpandThisSection(nextSection);
		},
		
		notifyDirty: function() {
			this.blockModel.notifyDirty();
			if(this.inputParametersStore){
				this.blockModel.notifyDataChange({
					INPUT : this.inputParametersStore.collect('uname')
				});
			}				
		},
		newGroupNameIsValid$ : function(){
			return RS.common.validation.PlainText.test(this.newGroupName) ? true : this.localize('invalidGroup');
		},
		addNewGroupIsEnabled$ : function(){
			return this.newGroupNameIsValid == true;
		},
		addNewGroup: function() {		
			var recordIdx = this.detail.groupStore.find('text', this.newGroupName, 0, false, false, true);
			if (recordIdx == -1) {
				this.detail.addGroupName(this.newGroupName);
			}
			else {
				clientVM.message(this.localize('invalidGroupNameTitle'),this.localize('duplicateGroupName'));		
			}
		},

		// called by detail.
		addInputToList: function(input) {
			var store = this.inputParametersStore;
			
			if (store.findExact('uname', input.uname) === -1) {
				if (store.count()) {
					input.uorder = store.last().data.uorder;
				}

				store.suspendEvents();
				var model = store.add(input)[0];
				store.remove(model);
				store.resumeEvents();
				store.addSorted(model);
				this.set('hasInputParameters', this.inputParametersStore.count() > 0);

			} else {
				this.displayDuplicateNameMsg();		
			}
		},
		displayDuplicateNameMsg : function(){
			clientVM.message(this.localize('invalidParameterTitle'),this.localize('parameterAlreadyExists'));		
		},
		removeParameter: function (p) {
			var store = this.inputParametersStore;
			var count = store.count();

			if (count === 1) {
				store.removeAll();
				this.set('hasInputParameters', false);
			} else {
				store.remove(p);
				this.set('hasInputParameters', true);
			}
		},

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden) {
					setTimeout(function() {
						if (this.parentVM.parametersEditSectionExpanded) {
							this.parentVM.parametersEditSectionExpanded = null;
							this.set('collapsed', false);
						}
						if (this.parentVM.parametersEditSectionMaximized) {
							this.parentVM.parametersEditSectionMaximized = null;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['PARAMETERS']);
						}
					}.bind(this), 10);
				}
			}
		},

		closeFlag: false,
		close: function () {
			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		}
	},

	getDataPaths: function () {
		return [{
			filter: function (p) {
				return p.utype === 'INPUT' || p.utype === null;
			},
			path: 'data.resolveActionInvoc.resolveActionParameters'
		}];
	},

	getData: function () {		
		return [this.gluModel.inputParametersStore.getData()];
	},

	load: function (inputParameters) {
		var g = this.gluModel,
			store = g.inputParametersStore;

		for (var i = 0; i < inputParameters.length; i++) {
			var p = inputParameters[i];
			p.id = p.id || '';
			p.uname = p.uname || '';
			p.udescription = p.udescription || '';
			p.udefaultValue = p.udefaultValue || '';
			p.uencrypt = p.uencrypt || false;
			p.urequired = p.urequired || false;
			p.uprotected = p.uprotected || false;
			p.uorder = p.uorder || 0;

			if (p.ugroupName === null) {
				p.ugroupName = g.unclassified;
			}
		}

		g.detail.setGroupsByParameters(inputParameters);
		store.loadData(inputParameters);
		this.set('hasInputParameters', store.count() > 0);
	}
});

Ext.define('RS.actiontaskbuilder.ParametersOutputEditBlockModel', {
	extend: 'RS.common.blocks.BlockModel',
	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Parameters',
        title: '',
		hidden: true,
		collapsed: true,
		editSubBlock: true,
		stopPropagation: false,

        header: {
	 		xtype: 'toolbar',
	    	margin: 0,
	    	padding: 0,
	    	items: []
		},	

		outputParametersStore: null,
		hasOutputParameters: false,

		detail: {
			mtype: 'ParameterOutputDetail'
		},		
		allParameters : {
			OUTPUT : [], 
			FLOW : [],
			WSDATA : []
		},
		postInit: function (blockModel, context) {
			this.blockModel = blockModel;
			this.detail.setAddOutputButtonTitle(context.localize('addOutput'));
			this.set('outputParametersStore', new RS.common.data.FlatStore({
				fields: ['id', 'uname', 'udescription', 'udefaultValue', 'uencrypt', 'urequired', 'utype'],
				listeners: {
					datachanged: function () {
						this.notifyDirty();
						this.notifyDataChange();
					}.bind(this),
					update: function () {
						this.notifyDirty();
						this.notifyDataChange();
					}.bind(this)						
				}
			}));
			this.header.items = [{
				xtype: 'component',
				padding: '0 0 0 15',
				cls: 'x-header-text x-panel-header-text-container-default',
				html: this.localize('parametersTitle'),
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
						}, this);
					}.bind(this)
				}
			}, {
	        	xtype: 'container',
	        	autoEl: 'ol',
	        	cls: 'breadcrumb flat',
	        	margin: 0,
	        	padding: 0,
	        	items: [{
		    		xtype: 'component',
		    		autoEl: 'li',		    		
		    		html: this.localize('inputParametersEditTitle'),
		    		listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.jumpTo('ParametersInputEdit', this.parentVM.parametersInputEdit.gluModel);
				            }, this);						
						}.bind(this)
		    		}
		    	}, {
		    		xtype: 'component',
		    		autoEl: 'li',
		    		cls: 'active',
		    		html: this.localize('outputParametersEditTitle'),
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
							}, this);
						}.bind(this)
					}
		    	}]
	        }, '->', {
				xtype: 'button',
				tooltip: this.localize('resizeFullscreen'),
				iconCls: 'rs-block-button icon-resize-full',
				isResizeMaxBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['PARAMETERS']);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				tooltip: this.localize('resizeNormalscreen'),
				iconCls: 'rs-block-button icon-resize-small',
				hidden: true,
				isResizeNormalBtn: true,
				margin: '0 20',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeNormalSection(this);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				iconCls: 'rs-block-button icon-chevron-sign-up',
				isCollapseBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.set('collapsed', true);
						}, this);
					}.bind(this)
				}
			}];
		},
		jumpTo: function(nextBlockName, nextSection){
			this.parentVM.parametersEditSectionExpanded = !this.collapsed;
			this.parentVM.parametersEditSectionMaximized = this.isSectionMaxSize;
			this.parentVM.collapseAndHideSection(this);
			this.parentVM.showAndExpandThisSection(nextSection);
		},
		notifyDirty: function() {
			this.blockModel.notifyDirty();			
		},
		notifyDataChange : function(){
			if(this.outputParametersStore){			
				//console.log("broadcasting event....");
				this.blockModel.notifyDataChange(Ext.apply(this.allParameters, {
					OUTPUT : this.outputParametersStore.collect('uname')				
				}));
			}
		},
		// called by detail.
		addOutputToList: function(output, ignoreMsg) {
			if (this.outputParametersStore.findExact('uname', output.uname) === -1) {
				this.outputParametersStore.add(output);		
				this.set('hasOutputParameters', this.outputParametersStore.count() > 0);
			} else if(!ignoreMsg) {
				this.displayDuplicateNameMsg();						
			}
		},
		displayDuplicateNameMsg : function(){
			clientVM.message(this.localize('invalidParameterTitle'),this.localize('parameterAlreadyExists'));		
		},
		removeParameter: function (p) {
			if (this.parentVM.embed && this.rootVM.automation.activeItem && this.rootVM.automation.activeItem.activeItem) {
				var outputParamName = p.get('uname');
				this.rootVM.automation.activeItem.activeItem.checkTargetParamDependency(outputParamName, function() {
					this.doRemoveParameter(p);
				}.bind(this));
			} else {
				this.doRemoveParameter(p);
			}
		},

		doRemoveParameter: function (p) {
			if(this.validateParserOutput(p)){
				var store = this.outputParametersStore;
				var count = store.count();

				if (count === 1) {
					store.removeAll();
					this.set('hasOutputParameters', false);
				} else {
					store.remove(p);
					this.set('hasOutputParameters', true);
				}
			}			
		},
		//Prevent remove variable created from parser.
		validateParserOutput : function(record){
			if(this.blockModel.cachedParserOutputStore.find('name', record.get('uname')) != -1){
				clientVM.message(this.localize('parserVariableRestrictionTitle'),this.localize('parserVariableRestrictionMsg'));		
				return false;
			}
			return true;
		},
		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden) {
					setTimeout(function() {
						if (this.parentVM.parametersEditSectionExpanded) {
							this.parentVM.parametersEditSectionExpanded = null;
							this.set('collapsed', false);
						}
						if (this.parentVM.parametersEditSectionMaximized) {
							this.parentVM.parametersEditSectionMaximized = null;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['PARAMETERS']);
						}
					}.bind(this), 10);
				}
			}
		},

		closeFlag: false,
		close: function () {
			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		}
	},
	//Processing output from parser
	processDataChange : function(variableDataArray){	
		//console.log("processing data....");
		var g = this.gluModel;
		var outputStore = g.outputParametersStore;
		outputStore.suspendEvents();
		var updatedOn = new Date().getTime();
		g.allParameters['FLOW'] = [];
		g.allParameters['WSDATA'] = [];	
		for(var i = 0; i < variableDataArray.length; i++){
			var containerType = variableDataArray[i]['containerType'];
			var variableName = variableDataArray[i]['name'];
			if(containerType == 'OUTPUT'){
				//Process all output and put to cached store first
				this.processOutputVariable(variableDataArray[i], updatedOn);
			}
			else
				g.allParameters[containerType].push(variableName);
		}		
		this.finalizeOutputVariable(updatedOn);	
		outputStore.resumeEvents();
		g.fireEvent('refreshGrid');
		g.notifyDataChange();
	},
	cachedParserOutputStore : Ext.create('Ext.data.Store',{
		mtype : 'store',
		fields : ['name','variableId','updatedOn']
	}),
	processOutputVariable : function(output, updatedOn){
		var g = this.gluModel;
		var outputStore = g.outputParametersStore;
	
		var cachedOutputIndex = this.cachedParserOutputStore.find('variableId', output['variableId']);
		//New Output just add to the store
		if(cachedOutputIndex == -1){
			this.cachedParserOutputStore.add(Ext.apply(output, { updatedOn : updatedOn}));
			g.addOutputToList({
				uname : output['name'],
				udescription : g.localize('outputFromParser'),
				utype : 'OUTPUT'
			}, true)
		}
		//Output has changed. Update it
		else if(this.cachedParserOutputStore.getAt(cachedOutputIndex).get('name') != output['name']){
			var cachedRecord = this.cachedParserOutputStore.getAt(cachedOutputIndex);
			var oldName = cachedRecord.get('name');
			cachedRecord.set('name',output['name']);
			cachedRecord.set('updatedOn',updatedOn);
			var recordIndex = outputStore.find('uname', oldName);
			if(recordIndex != -1)
				outputStore.getAt(recordIndex).set('uname', output['name']);
		}
		else
			this.cachedParserOutputStore.getAt(cachedOutputIndex).set('updatedOn',updatedOn);		
	},
	finalizeOutputVariable : function(updatedOn){
		var g = this.gluModel;
		var outputStore = g.outputParametersStore;
		var deletedCachedRecords = [];
		this.cachedParserOutputStore.each(function(cachedRecord){
			//Record that has not recently updated mean it's no longer exist.
			if(cachedRecord.get('updatedOn') != updatedOn){
				var r = outputStore.findRecord('uname', cachedRecord.get('name'));
				if(r)
					outputStore.remove(r);
				deletedCachedRecords.push(cachedRecord);
			}			
		},this)
		for(var i = 0; i < deletedCachedRecords.length; i++){
			this.cachedParserOutputStore.remove(deletedCachedRecords[i]);
		}
		g.set('hasOutputParameters', outputStore.count() > 0);	
	},
	getDataPaths: function () {
		return [{
			filter: function (p) {
				return p.utype === 'OUTPUT';
			},
			path: 'data.resolveActionInvoc.resolveActionParameters'
		}];
	},

	getData: function () {
		return [this.gluModel.outputParametersStore.getData()];
	},

	load: function (outputParameters) {
		var g = this.gluModel,
			store = g.outputParametersStore;

		for (var i = 0; i < outputParameters.length; i++) {
			var p = outputParameters[i];
			p.uname = p.uname || '';
			p.udescription = p.udescription || '';
			p.udefaultValue = p.udefaultValue || '';
			p.uencrypt = p.uencrypt || false;
			p.urequired = p.urequired || false;
			p.uorder = p.uorder || 0;
		}
		store.loadData(outputParameters);
		this.set('hasOutputParameters', store.count() > 0);
	}
});

Ext.define('RS.actiontaskbuilder.ParametersReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Parameters',
		classList: 'block-model',
		collapsed: true,
		hidden: false,
        title: '',
        header: {
            titlePosition: 0,
            defaults: {
                margin: '0'
            },
            items: []
        },

		inputParametersStore: null,
		outputParametersStore: null,
		hasInputParameters: false,
		hasOutputParameters: false,
		unclassified: '',

		postInit: function (blockModel, context) {
			this.blockModel = blockModel;
			this.set('inputParametersStore', new RS.common.data.FlatStore({
				fields: ['id', 'uname', 'udescription', 'udefaultValue', 'uencrypt', 'urequired', 'uprotected', 'ugroupName', 'utype', 'uorder'],
				groupField: 'ugroupName',
				sorters: [{
			    	property: 'value',
			    	direction: 'uorder'
				}]
			}));

			this.set('outputParametersStore', new RS.common.data.FlatStore({
				fields: ['id', 'uname', 'udescription', 'udefaultValue', 'uencrypt', 'urequired', 'utype']
			}));

			this.unclassified = context.localize('unclassified');
		},

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden && this.parentVM.embed) {
					var inputData = this.parentVM.parametersInputEdit.getData(),
						outputData = this.parentVM.parametersOutputEdit.getData();

					var parameters = inputData[0];

					for (var i=0, l=outputData[0].length; i<l; i++) {
						var outParam = outputData[0][i];
						parameters.push(outParam);
					}

					this.parentVM.parametersRead.load(parameters);
				}
			}
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.resolveActionParameters'];
	},

	load: function (parameters) {
		var g = this.gluModel,
			inputStore = g.inputParametersStore,
			outputStore = g.outputParametersStore,
			inputParameters = [],
			outputParameters = [];

		for (var i = 0; i < parameters.length; i++) {
			var p = parameters[i];

			if (p.utype === 'INPUT' || p.utype === null) {
				p.uname = p.uname || '';
				p.udescription = p.udescription || '';
				p.udefaultValue = p.udefaultValue || '';
				p.uencrypt = p.uencrypt || false;
				p.urequired = p.urequired || false;
				p.uprotected = p.uprotected || false;
				p.uorder = p.uorder || 0;

				if (p.ugroupName === null) {
					p.ugroupName = g.unclassified;
				}
				else {
					// HTML tags are not allowed in parameter grouping
					p.ugroupName = Ext.util.Format.stripTags(p.ugroupName);
				}

				inputParameters.push(p);
			} else {
				p.uname = p.uname || '';
				p.udescription = p.udescription || '';
				p.udefaultValue = p.udefaultValue || '';
				p.uencrypt = p.uencrypt || false;
				p.urequired = p.urequired || false;
				p.uorder = p.uorder || 0;
				outputParameters.push(p);
			}
		}

		var classes = ['block-model'];

		inputStore.loadData(inputParameters);
		outputStore.loadData(outputParameters);
		this.set('hasInputParameters', inputStore.count() > 0)
			.set('hasOutputParameters', outputStore.count() > 0)
			.set('classList', classes);
	}
});

glu.defModel('RS.actiontaskbuilder.BoundaryUtil',{
	startOutput$ : function(){
		return !(this.parseStart && this.parseStart != '');
	},
	endOutput$ : function(){
		return !(this.parseEnd && this.parseEnd != '');
	},
	startOutputIsSelected : function(){
		this.set('parseStart','');
	},		
	endOutputIsSelected : function(){
		this.set('parseEnd','');
	},
	parseStartIsValid$: function(){
		var self = this;
		//Check for new line
		var indx = self.sample.indexOf(this.parseStart);
		if(indx == -1 && self.sample.replace(/[\n\r\s]/g,'').indexOf(this.parseStart.replace(/[\n\r\s]/g,'')) != -1)
			return this.localize('invalidBoundaryInput');
		else
			return true;
	},
	parseEndIsValid$: function(){
		var self = this;
		//Check for new line
		var indx = self.sample.indexOf(this.parseEnd);
		if(indx == -1 && self.sample.replace(/[\n\r\s]/g,'').indexOf(this.parseEnd.replace(/[\n\r\s]/g,'')) != -1)
			return this.localize('invalidBoundaryInput');
		else
			return true;
	},
	updateBoundaryIsEnabled$: function(){
		return this.sample != '' && (this.parseEndIsValid === true) && (this.parseStartIsValid === true);
	},
	updateBoundary : function(){		
		//Gather boundary
		var startPosition = 0;
		var endPosition = this.sample.length;		
		if(this.parseStart != '' ){	
			var indexStartBoundary = this.sample.indexOf(this.parseStart);		
			startPosition = indexStartBoundary != -1 ? indexStartBoundary : 0;
			startPosition += this.startTextIncluded ? 0 : this.parseStart.length;
		}
		if(this.parseEnd != '' ){
			var indexEndBoundary = this.sample.indexOf(this.parseEnd);
			endPosition = indexEndBoundary != -1 ? indexEndBoundary : 0;
			endPosition += this.endTextIncluded ? this.parseEnd.length : 0;
		}
		//Validate this boundary.
		if(endPosition < startPosition){
			this.set('parseEnd','');
			endPosition = this.sample.length - 1;
		}
		//Compare boundary with current markups
		var outOfBoundMarkupActionId = [];
		for(var i = 0; i < this.markups.length; i++){
			var markup = this.markups[i];
			if(markup.type == 'parseStart' || markup.type == 'parseEnd')
				continue;
			if(markup.from < startPosition || markup.from > endPosition)
				outOfBoundMarkupActionId.push(markup.actionId);
			else {
				if((markup.from + markup.length) > endPosition)
					outOfBoundMarkupActionId.push(markup.actionId);
			}
		}
		if(outOfBoundMarkupActionId.length > 0){
			this.message({
				title : this.localize('markupOutOfBoundaryTitle'),
				msg : this.localize('markupOutOfBoundaryBody'),
				buttons : Ext.MessageBox.YESNO,
				buttonText : {
					yes : this.localize('yes'),
					no : this.localize('no')
				},
				scope : this,
				fn: function(btn){
					if(btn == "yes")
						this.addBoundaries(outOfBoundMarkupActionId);
					else {
						this.set('parseStart', this.parseStartOrignalData);
						this.set('parseEnd', this.parseEndOrignalData);
					}
				}
			})
		}
		else{
			this.addBoundaries(outOfBoundMarkupActionId);
		}	
	},
	addBoundaries : function(removedMarkups){
		var newMarkups = [];
		//Remove out of bound markups as well as old boundaries.
		for(var i = 0; i < this.markups.length; i++){
			var markup = this.markups[i];
			if(removedMarkups.indexOf(markup.actionId) == -1 && markup.type != 'parseStart' && markup.type != 'parseEnd')
				newMarkups.push(markup);
		}
		//Clean up any variables out of bound
		for(var i = 0; i < removedMarkups.length; i++){
			var actionId = removedMarkups[i];
			var indexOfVariable = this.variableStore.indexOfId(actionId);
			if(indexOfVariable != -1)
				this.variableStore.removeAt(indexOfVariable);
		}
		//Add in boundaries
		if(this.parseStart != ''){
			var indexStartBoundary = this.sample.indexOf(this.parseStart);		
			newMarkups.push({
				from :  indexStartBoundary != -1 ? indexStartBoundary : 0,
				length : this.parseStart.length,
				boundaryInput : this.parseStart,
				type : 'parseStart',
				action : 'beginParse',
				included : this.startTextIncluded,
				actionId: Ext.data.IdGenerator.get('uuid').generate()
			})
		}
		if(this.parseEnd != ''){
			var indexEndBoundary = this.sample.indexOf(this.parseEnd);
			newMarkups.push({
				from : indexEndBoundary != -1 ? indexEndBoundary : this.sample.length ,
				length : this.parseEnd.length,
				boundaryInput : this.parseEnd,
				type : 'parseEnd',
				action : 'endParse',
				included : this.endTextIncluded,
				actionId: Ext.data.IdGenerator.get('uuid').generate()
			})
		}
		this.set('markups', newMarkups);
		this.set('parseStartOrignalData', this.parseStart);
		this.set('parseEndOrignalData', this.parseEnd);
		this.resetParser();
	},
	updateBoundaryFromMarkup : function(postion, markupId){
		if(postion == 'start'){
			var targetMarkup = null;
			for(var i = 0; i < this.markups.length; i++){
				var markup = this.markups[i];
				if(markupId == markup.actionId){
					targetMarkup = markup;
					break;
				}
			}
			if(targetMarkup.type == 'literal'){
				var txt = this.sample.substring(targetMarkup.from, targetMarkup.from + targetMarkup.length);
				this.set('parseStart',txt);
			}
			else
				this.set('parseStart',this.getRegexForType(targetMarkup.type));
		}
		else if (postion == 'end'){
			var targetMarkup = null;
			for(var i = 0; i < this.markups.length; i++){
				var markup = this.markups[i];
				if(markupId == markup.actionId){
					targetMarkup = markup;
					break;
				}
			}
			if(targetMarkup.type == 'literal'){
				var txt = this.sample.substring(targetMarkup.from, targetMarkup.from + targetMarkup.length);
				this.set('parseEnd',txt);
			}
			else
				this.set('parseEnd',this.getRegexForType(targetMarkup.type));
		}
	}
})
glu.defModel('RS.actiontaskbuilder.MarkupCapture',{
	fields : ['variable','color','column','xpath'],	
	existingVariableName : [],
	validNameRegex: RS.common.validation.VariableName,
	newMarkup : null,
	parserType : 'text',
	containerType : 'OUTPUT',
	containerTypeStore : {
		mtype : 'store',
		proxy : {
			type : 'memory'
		},
		fields : [{
			name : 'value',
			convert : function(v, record){
				return record.raw;
			}
		}],
		data: ['WSDATA','FLOW','OUTPUT']
	},

	//For Text Parser
	patternProcessor : {
		mtype : 'PatternProcessor',
		ignorePattern : ['literal']
	},

	init : function(){
		this.loadData(this.newMarkup);
		if(this.parserType == "text"){
			//Let the init of patternProcessor run first before doing update.
			setTimeout(function(){
				this.patternProcessor.updatePatternForMarkup(this.newMarkup['type'],this.newMarkup['autoType']);
			}.bind(this),0);
		}		
	},
	parserTypeIsText$ : function(){
		return this.parserType == 'text';
	},
	parserTypeIsTable$ : function(){
		return this.parserType == 'table';
	},
	parserTypeIsXML$ : function(){
		return this.parserType == 'xml';
	},
	isNameDuplicated : function(name){
		return this.existingVariableName.indexOf(name) != -1;
	},
	variableIsValid$ : function(){
		if(this.isNameDuplicated(this.variable))
			return this.localize('duplicateVariableMsg');
		else if (!this.validNameRegex.test(this.variable))
			return this.localize('validKeyCharacters');
		else
			return true;
	},
	add : function(){
		if(!this.isValid){
			this.set('isPristine',false);
			return;
		}
		var newData = {
			variable : this.variable,			
			containerType : this.containerType
		}
		if(this.parserType == "text"){
			var patternName = this.patternProcessor['activePattern']['name'];
			Ext.apply(newData, {				
				color : this.color,
				type : patternName,
			})
		}
		else if(this.parserType == "xml"){
			Ext.apply(newData, {				
				xpath : this.xpath,
			})
		}

		var data = Ext.apply(this.newMarkup, newData);
		if(this.dumper && Ext.isFunction(this.dumper.dump))		
			this.dumper.dump.apply(this.dumper.scope || this, [data]);
		this.doClose();		
	},
	cancel : function(){
		this.doClose();
	},
})
glu.defModel("RS.actiontaskbuilder.ParserCode",{
	autoGenCheck : true,	
	assessTask : false,	
	codeContent : "",	
	parserType : 'text',
	parserConfig : null,
	sample : null,
	viewOnly : false,
	ignoreDirtyChecking: false,
	API : {
		getParserAutoGenCode  : '/resolve/service/actiontask/getParserAutoGenCode'
	},
	update_dirty_flag : {
		on : ['autoGenCheckChanged','codeContentChanged'],
		action : function(){
			if(!this.viewOnly && !this.ignoreDirtyChecking)
				this.parentVM.notifyDirty();
		}
	},
	editorCls$ : function(){
		return this.viewOnly ? 'editor-disabled' : '';
	},
	populateCode: function(autoGenCheck, code){
		this.set('ignoreDirtyChecking', true);
		var autoGenCheck = autoGenCheck ? autoGenCheck : this.code == "";
		this.set('autoGenCheck', autoGenCheck);
		this.set('codeContent', code);
		setTimeout(function() {
			this.set('ignoreDirtyChecking', false);
		}.bind(this), 100);
	},
	updateMarkupInfo : function(){	
		this.set('parserConfig', this.parentVM.getParserConfig());
		this.set('sample', this.parentVM.getSample());		
	},
	generateCode : function(){
		this.updateMarkupInfo();
		if(this.autoGenCheck){
			if(this.parserConfig){
				var data = {
					parserConfig : this.parserConfig,
					sampleOutput : this.sample,
					parserType : this.parserType,
					isAssessTask : this.assessTask
				}
				this.ajax({
					url : this.API['getParserAutoGenCode'],
					jsonData : data,		
					success : function(response){
						var respData = RS.common.parsePayload(response);

						if (respData.success) {
							this.set('codeContent', respData.data);
						}						
					}
				})
			}
			else{
				this.set('codeContent', "");			
			}
		}
		else {
			setTimeout(function(){
				this.message({
					title : this.localize('autoGenIsUncheckTitle'),
					msg : this.localize('autoGenIsUncheckMsg'),
					button : Ext.MessageBox.YESNO,
					scope : this,
					buttonText : {
						yes : this.localize('yes'),
						no : this.localize('cancel')
					},
					fn : function(btn){
						if(btn == 'yes'){
							this.set('autoGenCheck', true);
							this.generateCode();
						}
					}
				})
			}.bind(this),500);			
		}		
	},
	resetCode: function(){
		this.set('autoGenCheck', true);		
		this.set('codeContent', "");	
	},
	loadCode : function(autoGenFlag, codeContent){
		this.set('autoGenCheck', autoGenFlag);
		this.set('codeContent', codeContent);
	},
	isAssessTask : function(flag){
		this.set('assessTask', flag);
	},
	getCode : function(){
		return this.codeContent;
	},
	isAutoGenCode : function(){
		return this.autoGenCheck;
	},
	checkBoxChange : function(newVal, oldVal){
		var parserType = this.parserType;
		if(newVal){
			this.message({
				title : this.localize('autoGenConfirmTitle'),
				msg : this.localize('autoGenConfirmBody'),
				button : Ext.MessageBox.YESNO,
				scope : this,
				buttonText : {
					yes : this.localize('yes'),
					no : this.localize('cancel')
				},
				fn : function(btn){
					if(btn == 'yes')
						this.generateCode();
					else 
						this.set('autoGenCheck', false);
				}
			})
		}
	},
	codeContentIsReadOnly$ : function(){
		return this.viewOnly || this.autoGenCheck;
	}
})

glu.defModel('RS.actiontaskbuilder.ParserControl',{
	activeParserType : 'text',
	smallResolution : false,
	textParser: {
		mtype: 'PlainTextParser'
	},	
	tableParser: {
		mtype: 'TableParser'	
	},
	xmlParser: {
		mtype: 'XMLParser'	
	},
	when_resolution_change : {
		on : ['smallResolutionChanged'],
		action : function(){
			this.textParser.set('smallResolution', this.smallResolution);
			this.tableParser.set('smallResolution',  this.smallResolution);
			this.xmlParser.set('smallResolution',  this.smallResolution);
		}
	},
	loadData : function(data){
		this[this.activeParserType + 'Parser'].loadData(data);
	},	
	setDefaultParser : function(){
		this.textParser.setDefaultParser();
		this.tableParser.setDefaultParser();
		this.xmlParser.setDefaultParser();
	},
	updateDefaultTestSample : function(data){
		this.parentVM.updateDefaultTestSample(this.activeParserType, data);
	},
	updateActiveparserType : function(parserType){
		this.set('activeParserType', parserType);
		this.notifyDataChange();
	},
	isAssessTask: function(flag){
		this[this.activeParserType + 'Parser'].isAssessTask(flag);	
	},
	getActiveType : function(){
		return this.activeParserType;
	},	
	getCode : function(){
		return this[this.activeParserType + 'Parser'].getCode();
	},
	getParserConfig : function(){
		return this[this.activeParserType + 'Parser'].getParserConfig();
	},	
	getSample : function(){
		return this[this.activeParserType + 'Parser'].getSample();
	},
	getParameterList : function(){
		return this[this.activeParserType + 'Parser'].getParameterList();
	},
	isAutoGenCode : function(){
		return this[this.activeParserType + 'Parser'].isAutoGenCode();
	},
	isParserReady : function(){
		return this[this.activeParserType + 'Parser'].isParserReady();
	},
	notifyDirty : function(){
		this.parentVM.notifyDirty();
	},
	notifyDataChange : function(){		
		this.parentVM.notifyDataChange();
	}
})
glu.defModel('RS.actiontaskbuilder.PatternProcessor', {
	patternIsHidden : true,
	patternList : {
		mtype : 'list',
		autoParent : true
	},	
	activePattern : null,
	ignorePattern : [],
	patternCategory : {
		ANY : ['ignoreNotGreedy','ignoreRemainder','literal'],
		SPACE : ['spaceOrTab', 'whitespace','endOfLine'],
		NUMBER : ['number','notNumber'],
		LETTER : ['letters', 'lettersAndNumbers', 'notLettersOrNumbers', 'lowerCase', 'upperCase'],	
		SPECIAL : ['IPv4','MAC','decimal']
	},
	patternForTypeLogicMap : {
		'isWhitespace' : ['ignoreNotGreedy','ignoreRemainder','literal','spaceOrTab', 'whitespace'],
		'isDecimal' : ['ignoreNotGreedy','ignoreRemainder','literal', 'decimal'],
		'isNumber' : ['ignoreNotGreedy','ignoreRemainder','literal', 'number'],
		'isLetters' : ['ignoreNotGreedy','ignoreRemainder','literal', 'notNumber','letters','lowerCase', 'upperCase'],
		'isLettersAndNumbers' : ['ignoreNotGreedy','ignoreRemainder','literal', 'lettersAndNumbers'],
		'isIPv4' : ['ignoreNotGreedy','ignoreRemainder','literal', 'IPv4'],
		'isMAC' : ['ignoreNotGreedy','ignoreRemainder','literal', 'MAC'],
		'isIgnoreNotGreedy' :  ['ignoreNotGreedy','ignoreRemainder','literal', 'notNumber', 'notLettersOrNumbers'],	
		'isLineBreak' : ['endOfLine']
	},
	autoSelectTypeMap : {
		'isWhitespace' : 'whitespace',
		'isDecimal' :'decimal',
		'isNumber' : 'number',
		'isLetters' :'letters',
		'isLettersAndNumbers' : 'lettersAndNumbers',
		'isIPv4' : 'IPv4',
		'isMAC' : 'MAC',
		'isIgnoreNotGreedy' : 'ignoreNotGreedy',	
		'isLineBreak' : 'endOfLine'
	},
	typeToRegexMap : {
		'letters' : '[a-zA-Z]+',
		'number' : '-?\\d+',
		'notLettersOrNumbers' : '\\W+',	
		'lowerCase' : '[a-z]+',
		'upperCase' : '[A-Z]+',
		'ignoreNotGreedy' : '.*?',
		'ignoreRemainder' : '.*',
		'spaceOrTab' : '[\\s\\t]+',
		'whitespace' : '\\s+',
		'endOfLine' : '(?:\\n|\\r\\n)',
		'notNumber' : '\\D+',
		'lettersAndNumbers' : '\\w+',
		'MAC' : '(?:[0-9A-Fa-f]{2}[:-]){5}(?:[0-9A-Fa-f]{2})',
		'IPv4' : '(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)'
	},
	init : function(){
		for(var category in this.patternCategory){
			var patternMetaData = this.patternCategory[category];
			for(var i = 0; i < patternMetaData.length; i++){
				var patternName = patternMetaData[i];
				if(this.ignorePattern.indexOf(patternName) == -1){
					this.patternList.add({
						mtype : 'PatternTag',
						name : patternName
					})
				}			
			}		
		}		
	},
	getAutoTypeLogic: function(text) {
		var guesses = [{
			type : 'isLineBreak',
			v : Ext.is.Windows ? (/^\r\n$/) : (/^\n$/)
		},{
			type: 'isWhitespace',
			v: /^\s+$/
		}, {
			type: 'isDecimal',
			v: /^\d*\.{1}\d+$/
		}, {
			type: 'isNumber',
			v: /^\d+$/
		}, {
			type: 'isLetters',
			v: /^[a-zA-z]+$/
		}, {
			type: 'isLettersAndNumbers',
			v: /^[a-zA-z|\d]+$/
		}, {
			type: 'isIPv4',
			v: {
				test: function(text) {
					var match = text.match(/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/);
					return match != null &&
						match[1] <= 255 && match[2] <= 255 &&
						match[3] <= 255 && match[4] <= 255;
				}
			}
		}, 
		/*{
			type: 'isIPv6',
			v: /^(([0-9a-fA-F]{1,4}:){7,7}[0-9a''-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))$/
		},*/
		 {
			type: 'isMAC',
			v: /^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$/
		}];
		for (var i = 0; i < guesses.length; i++)
			if (guesses[i].v.test(text))
				return guesses[i].type;
		return 'isIgnoreNotGreedy';
	},	
	getRegexForType : function(type){
		return this.typeToRegexMap[type];
	},
	getAutoSelectType : function(typeLogic){
		return this.autoSelectTypeMap[typeLogic];
	},
	selectActivePattern : function(patternName){
		this.patternList.each(function(pattern){		
			if(patternName ==  pattern.name){
				this.set('activePattern', pattern);
				this.activePattern.set('selected',true);
			}
		},this)
	},
	deselectAll : function(){
		if(this.activePattern)
			this.activePattern.set('selected',false);
		this.set('activePattern', null);
	},
	select : function(pattern){
		if(this.activePattern)
			this.activePattern.set('selected', false);
		this.set('activePattern', pattern);
		this.activePattern.set('selected', true);
		this.fireEvent('patternSelectionChanged', this.activePattern['name']);		
	},
	updatePatternForTypeLogic : function(typeLogic){
		this.deselectAll();		
		var availablePatterns = this.patternForTypeLogicMap[typeLogic] || this.patternForTypeLogicMap['isIgnoreNotGreedy'];
		this.patternList.each(function(pattern){
			var patternName = pattern.name;
			if(availablePatterns.includes(patternName))
				pattern.set('hidden', false);
			else 
				pattern.set('hidden', true);
		})
		this.hidePattern(false);	
	},
	updatePatternForMarkup : function(markupName, typeLogic){
		this.updatePatternForTypeLogic(typeLogic);
		this.selectActivePattern(markupName);
	},
	hidePattern : function(flag){
		this.set('patternIsHidden', flag);
	}
})
glu.defModel('RS.actiontaskbuilder.PatternTag',{
	name : '',
	selected : false,
	hidden : true,
	displayName$ : function(){
		return this.localize(this.name);
	},	
	select : function(){
		this.parentVM.select(this);
	},	
	selectedCss$ : function(){
		return this.selected ? 'selected' : '';
	}
})

glu.defModel("RS.actiontaskbuilder.PlainTextParser",{
	mixins : ['VariableUtil','BoundaryUtil'],		
	sample :'',	
	activeScreen : 0,
	smallResolution : false,
	newVariableCounter : 1,
	markups : [],
	block : false,
	parserData : null,
	parserReady : false,
	loadingData : false,
	variableColumn : null,
	selectedMarkup : null,
	currentSelection : null,
	parseStart : '',
	parseStartOrignalData : '',
	startTextIncluded : true,
	parseEnd : '',
	parseEndOrignalData : '',
	endTextIncluded : true,
	selectionIsInvalid : false,	
	codeScreen : {	
		mtype : 'ParserCode',
		parserType : 'text'
	},
	patternProcessor : {
		mtype : 'PatternProcessor'
	},	
	variableStore : {
		mtype : 'store',
		fields : ['variable','type','color','containerType']		
	},
	when_parser_ready : {
		on : ['parserReadyChanged'],
		action : function(){
			if(this.parserReady){
				this.populateData(this.parserData);
			}
		}
	},
	update_script : {
		on :['markupsChanged','blockChanged'],
		action : function(){
			if(this.parserReady && !this.loadingData)
				this.codeScreen.generateCode();
		}
	},
	update_dirty_flag : {
		on : ['sampleChanged'],
		action : function(){
			this.notifyDirty();
		}
	},
	notify_data_changed : {
		on : ['variableStoreChanged'],
		action : function(){
			this.notifyDataChange();
		}
	},
	getParameterList : function(){
		var variableArray = [];
		this.variableStore.each(function(entry){
			variableArray.push({
				name : entry.get('variable'),
				containerType : entry.get('containerType')
			})
		})
		return variableArray;
	},
	when_resolution_change : {
		on : ['smallResolutionChanged'],
		action : function(){			
			this.fireEvent('embeddedLayout',  this.smallResolution);
		}
	},
	
	init : function(){
		this.set('variableColumn', this.getTextVariableColumnConfig());	
		this.variableStore.on('datachanged', function(){
			this.fireEvent('variableStoreChanged');
		},this);	
		this.variableStore.on('update', function(){
			this.fireEvent('variableStoreChanged');
		},this);
		this.patternProcessor.on('patternSelectionChanged', function(patternName){
			this.updatePatternSelection(patternName);
		},this);
	},

	//HELPERS
	showCode : function(){
		this.set('activeScreen', 1);
	},
	showMarkup : function(){
		this.set('activeScreen', 0);
	},	
	isAssessTask: function(flag){
		this.codeScreen.isAssessTask(flag);
	},
	setDefaultParser: function(){
		if(this.parserReady){
			this.clearSample();			
			this.codeScreen.resetCode();
			this.set('activeScreen',0);
		}
	},
	loadData : function(parserData){		
		this.set('parserData', parserData);
		if(this.parserReady){
			this.populateData(parserData);
		}
	},
	populateData : function(parserData){
		if(parserData != null && Object.keys(parserData).length !== 0){
			this.set('loadingData', true);		
			this.variableStore.removeAll();
			
			//Set up sample output
			this.updateSample(parserData.sampleOutput || "");		

			//Set up markup
			if(parserData.parserConfig)
			{
				//Set up markups
				try {
					var parserConfig = JSON.parse(parserData.parserConfig);
				}catch (e) {
					clientVM.displayError(this.localize('invalidJSON'));
				}
				
				if(parserConfig){
					this.set('markups', parserConfig['markups']);

					//Set up boundary
					var parserOptions = parserConfig['options'];
					this.set('startTextIncluded', parserOptions.boundaryStartIncluded);
					this.set('parseStart','');
					this.set('endTextIncluded', parserOptions.boundaryEndIncluded);
					this.set('parseEnd', '');
					for(var i = 0; i < this.markups.length; i++){
						var currentMarkup = this.markups[i];
						if(currentMarkup.type == "parseStart" ){
							this.set('parseStart', currentMarkup.boundaryInput);
						}
						else if(currentMarkup.type == "parseEnd" ){
							this.set('parseEnd', currentMarkup.boundaryInput);
						}
					}
					//Save original boundary
					this.set('parseStartOrignalData', this.parseStart);
					this.set('parseEndOrignalData', this.parseEnd);
					this.set('block', parserOptions.repeat);

					//Variable Table
					var variableData = [];			
					for(var i = 0; i < this.markups.length; i++){
						var currentMarkup = this.markups[i];					
						if(currentMarkup.action == "capture" ){
							if(currentMarkup.flow)
								var containerType = 'FLOW';
							else if (currentMarkup.wsdata)
								containerType = 'WSDATA';
							else
								containerType = 'OUTPUT';
							variableData.push({
								id : currentMarkup.actionId,
								actionId : currentMarkup.actionId,
								variable : currentMarkup.variable,
								type : currentMarkup.type,
								color : currentMarkup.color,
								containerType : containerType
							})
						}
					}
					this.variableStore.loadData(variableData);			
				}
			}
			//Show Code if this only has code
			else if (parserData['uscript']){				
				this.set('activeScreen', 1);
			}

			//Populate Code
			this.codeScreen.populateCode(parserData['parserAutoGenEnabled'], parserData['uscript']);

			this.set('loadingData', false);			
		}
	},
	updateSample : function(data, asPasted){
		if(this.markups.length > 0 && asPasted){
			this.message({
				title : this.localize('overwriteMarkupTitle'),
				msg : this.localize('overwriteMarkupBody'),
				button : Ext.MessageBox.YESNO,
				buttonText : {
					yes : this.localize('confirm'),
					no : this.localize('cancel')
				},
				scope : this,
				fn : function(btn){
					if(btn == 'yes'){						
						this.clearSample();					
						this.set('sample', data);
						this.parentVM.updateDefaultTestSample(data);				
					}
				}
			})
		}
		else
		{
			this.set('sample', data);
			this.parentVM.updateDefaultTestSample(data);
		}
	},
	getMarkups : function(){
		return this.markups;
	},
	getSample : function(){
		return this.sample;
	},
	getParserConfig : function(){
		if(this.markups.length != 0){
			var options = {
				boundaryStartIncluded : this.startTextIncluded,
				boundaryEndIncluded : this.endTextIncluded,
				repeat : this.block
			}
			var	parserConfig = JSON.stringify({ 
				markups : this.markups,
				options : options,
				generatedFromATB : true
			});
			return parserConfig;
		}
		else
			return null;
		
	},
	getCode : function(){
		return this.codeScreen.getCode();
	},
	getParameterList : function(){
		var variableArray = [];
		this.variableStore.each(function(entry){
			variableArray.push({
				variableId : entry.get('id'),
				name : entry.get('variable'),
				containerType : entry.get('containerType')
			})
		})
		return variableArray;		
	},
	isAutoGenCode : function(){
		return this.codeScreen.isAutoGenCode();
	},
	isParserReady : function(){
		return this.parserReady;
	},
	resetParser : function(){
		this.set('selectionIsInvalid', false);
		this.set('currentSelection',null);	
		this.set('selectedMarkup',null);	
		this.patternProcessor.hidePattern(true);
	},	
	notifyDirty : function(){
		if(!this.loadingData)
			this.parentVM.notifyDirty();
	},
	notifyDataChange : function(){
		this.parentVM.notifyDataChange();
	},		
	//VIEW EVENT HANDLERS
	clearSample : function(){	
		this.updateSample("");
		this.clearMarkup(true);
		this.set('parseStart', '');
		this.set('parseEnd', '');			
	},	
	clearMarkup : function(clearAll){
		var boundaryMarkups = [];
		for(var i = 0; i < this.markups.length; i++){
			var markup = this.markups[i];
			if(markup.type == "parseStart" || markup.type == "parseEnd"){
				boundaryMarkups.push(markup);
			}
		}		
		this.set('markups', clearAll ? [] : boundaryMarkups);
		this.variableStore.removeAll();
		this.newVariableCounter = 1;
		this.resetParser();	
	},	
	updatePatternSelection : function(patternName){
		if(this.selectedMarkup != null){
			this.updateMarkup(patternName);
			this.fireEvent('selectedMarkupChanged');
		}
		else
			this.addMarkUp(patternName);
	},
	addMarkUp : function(markupName){
		var currentSelection = this.currentSelection;
		currentSelection.type = markupName;
		currentSelection.action = markupName == 'literal' ? 'literal' : 'type';
		var markups = this.markups.slice(0);
		markups.push(currentSelection);

		//This will trigger setMarkups method on the view and in turn update parser's view.
		this.set('markups', markups);
		
		this.fireEvent('reselectMarkup', currentSelection.actionId);
	},
	updateMarkup : function(patternName){
		var selectedMarkupActionId = this.selectedMarkup['actionId'];
		var markups = this.markups;
		var targetMarkup = null;
		for(var i = 0; i < markups.length; i++){
			if(markups[i].actionId == selectedMarkupActionId){
				targetMarkup = markups[i];
				break;
			}
		}
		if(targetMarkup != null){
			targetMarkup['type'] = patternName;	
			targetMarkup['action'] = patternName == 'literal' ? 'literal' : 'type';
			this.set('selectedMarkup', targetMarkup);		
			this.fireEvent('markupsChanged',this.markups);
		}
	},
	removeMarkup : function(){
		var selectedMarkup = this.selectedMarkup;
		var targetMarkupIndex = null;
		var markups = this.markups.slice(0);
		for(var i = 0; i < markups.length; i++){
			if(markups[i].actionId == selectedMarkup.actionId){
				targetMarkupIndex = i;
				break;
			}
		}
		if(targetMarkupIndex != null){
			markups.splice(targetMarkupIndex,1);
			//This will trigger setMarkups method on the view and in turn update parser's view.
			this.set('markups', markups);
			//Clear pattern table after selection.
			this.set('selectedMarkup', null);
			this.patternProcessor.hidePattern(true);
		}	
	},	
	captureMarkup : function(){
		//Already markup -> just use the current type and capture
		if(this.selectedMarkup && this.selectedMarkup.type)
			var selectedMarkup = this.selectedMarkup;
		else {
			//Element has no type (from highlight text). Pick most logical type for this
			var selectedMarkup = this.currentSelection;
			selectedMarkup['type'] = this.patternProcessor.getAutoSelectType(selectedMarkup['autoType']);
		}
		var randomColor = this.getRandomColor();
		var variableName = "NewVariable" + this.newVariableCounter;
		while(this.variableStore.findExact("variable",variableName) != -1){
			this.newVariableCounter++;
			variableName = "NewVariable" + this.newVariableCounter;
		} 
		var existingVariableName = this.variableStore.collect('variable');
		//SET DEFAULT TO OUTPUT.
		var newMarkup = {
			id : selectedMarkup.actionId,
			variable : variableName,
			from : selectedMarkup.from,
			length : selectedMarkup.length,
			actionId : selectedMarkup.actionId,				
			type : selectedMarkup.type,
			color : randomColor,
			flow : false,
			wsdata : false,
			output : true,
			action : 'capture',
			autoType : selectedMarkup.autoType					
		}
		this.open({
			mtype : 'MarkupCapture',
			newMarkup : newMarkup,
			parserType : 'text',
			existingVariableName : existingVariableName,
			dumper : {
				dump : this.processCaptureMarkup,
				scope : this
			}
		})
	},
	processCaptureMarkup : function(newMarkup){
		//Add to variable grid
		this.variableStore.add(newMarkup);

		var markups = this.markups.slice(0);
		var targetMarkupIndex = -1;
		for(var i = 0; i < markups.length; i++){
			if(markups[i].actionId == newMarkup.actionId){
				targetMarkupIndex = i;
				break;
			}
		}
		if(targetMarkupIndex != -1)
			markups.splice(targetMarkupIndex,1);
		markups.push(Ext.apply(newMarkup,{			
			flow : newMarkup.containerType == "FLOW",
			wsdata : newMarkup.containerType == "WSDATA",
			output : newMarkup.containerType == "OUTPUT",
		}));		
		
		//This will trigger setMarkups method on the view and in turn update parser's view.
		this.set('markups', markups);
		this.resetParser();
	},
	updateVariable : function(cellInfo){
		var newData = cellInfo.record.data;
		var markups = this.markups.slice(0);
		var targetMarkupIndex = null;
		var targetMarkup = null;
		for(var i = 0; i < markups.length; i++){
			if(markups[i].actionId == newData.id){
				targetMarkup = markups[i];
				targetMarkupIndex = i;
				break;
			}
		}
		if(targetMarkupIndex != null){
			markups.splice(targetMarkupIndex,1);			
			var newMarkup = {
				actionId : targetMarkup.actionId,
				from : targetMarkup.from,
				length : targetMarkup.length,
				action : 'capture',
				flow : newData.containerType == "FLOW",
				wsdata : newData.containerType == "WSDATA",
				output : newData.containerType == "OUTPUT",
				type : newData.type,
				color : newData.color,
				variable : newData.variable
			};
			markups.push(newMarkup);
			//This will trigger setMarkups method on the view and in turn update parser's view.
			this.set('markups', markups);
		}
		this.resetParser();
	},
	removeVariable : function(record){
		if(!this.isViewOnly){
			var removedVariableId = record.raw.actionId;
			var variableStoreIndex = this.variableStore.indexOfId(removedVariableId);
			this.variableStore.removeAt(variableStoreIndex);
			var markups = this.markups.slice(0);
			var targetMarkupIndex = null;
			for(var i = 0; i < markups.length; i++){
				var markup = this.markups[i];
				if(markup.actionId == removedVariableId){
					targetMarkupIndex= i;
					break;
				}
			}
			if(targetMarkupIndex != null){
				markups.splice(targetMarkupIndex,1);
				//This will trigger setMarkups method on the view and in turn update parser's view.
				this.set('markups', markups);
				//Clear pattern table after selection.		
				this.patternProcessor.hidePattern(true);
			}
		}
	},
	
	//VIEW COMPONENT VISIBILITY
	captureMarkupIsEnabled$ : function(){
		return (this.selectedMarkup != null && this.selectedMarkup.action != 'literal' ) || this.currentSelection != null;
	},
	removeMarkupIsEnabled$ : function(){
		return this.selectedMarkup != null;
	},	
	clearSampleIsEnabled$ : function(){
		return this.sample != '' && this.activeScreen == 0;
	},	
	clearMarkupIsEnabled$ : function(){
		return this.sample != '' && this.activeScreen == 0 && this.markups && this.markups.length > 0;
	},	
	variableStoreIsEmpty$ : function(){		
		return this.variableStore.getCount() == 0;
	},
	showCodeIsVisible$: function(){		
		return this.activeScreen == 0;
	},
	showMarkupIsVisible$: function(){
		return this.activeScreen == 1;
	},
	controlBorderRegion$ : function(){
		return this.smallResolution ? 'south' : 'east';
	},
	controlHeight$ : function(){
		return this.smallResolution ? 900 : 450;
	},
	controlMargin$ : function(){
		return this.smallResolution ? '10 0 0 0' : '0 0 0 10';
	},

	//SELECTION HIGHTLIGHT LOGIC	
	processHighlightSelection : function(selection,documentBody, isLinebreak){
		this.set('selectionIsInvalid', false);
		this.set('selectedMarkup',null);
		var selectionInfo = this.getSelectionInfo(selection,documentBody,isLinebreak);		
		var selectedText = this.sample.substring(selectionInfo.from, selectionInfo.from + selectionInfo.length);
		var autoType = this.patternProcessor.getAutoTypeLogic(selectedText);
		selectionInfo.autoType = autoType;
		this.set('currentSelection', selectionInfo);	
		this.patternProcessor.updatePatternForTypeLogic(autoType);	
	},
	//This method will return start position, length of selection as well as unique actionId for that selection.
	getSelectionInfo : function(selection, documentBody, isLinebreak){
		var range = selection.getRangeAt(0);
		var startNode = range.startContainer;
		//Incase selection start from linebreak.
		while(!isLinebreak && startNode.nodeType == 1 && startNode.getAttribute('marker') == "new_line"){
			startNode = startNode.nextSibling;	
		}
		//Get wrapper node of marker instead of content
		if (startNode.parentNode && startNode.parentNode.hasAttribute('marker'))
			startNode = startNode.parentNode
		var endNode = range.endContainer;
		if (endNode.parentNode && endNode.parentNode.hasAttribute('marker'))
			endNode = endNode.parentNode;
		var segments = [];
		var currentSegment = [];
		var selecting = false;

		//Step 1 - Put DOM nodes in correct segments based on selection.
		function segmentDOMNode(node){
			for (var i = 0; i < node.childNodes.length; i++) {
				if (node.childNodes[i] == startNode || node.childNodes[i] == endNode) {
					if (!selecting) {
						segments.push(currentSegment);
						if (startNode == endNode) {
							currentSegment = [node.childNodes[i]]; //which is also start,end
							segments.push(currentSegment);
							currentSegment = [];
							continue;
						}
						currentSegment = [];
						selecting = true;
					} else {
						currentSegment.push(node.childNodes[i]); //which is the logical 'end'(it can be start or end coz the selection direction may be reversed)
						segments.push(currentSegment);
						currentSegment = [];
						selecting = false;
						continue;
					}
				}
				if (node.childNodes[i].nodeType == 3)
					currentSegment.push(node.childNodes[i]);
				else if (node.childNodes[i].hasAttribute('marker'))
					currentSegment.push(node.childNodes[i]);
				else if (!node.childNodes[i].getAttribute('exclude'))
					segmentDOMNode(node.childNodes[i]);
			}
		}
		segmentDOMNode(documentBody);
		segments.push(currentSegment);
		
		//Step 2 - Extract raw text from each segment
		var pre = this.extractTextFromSegment(segments[0]);
		var selection = this.extractTextFromSegment(segments[1]);
		var post = this.extractTextFromSegment(segments[2]);

		//Step 3 - Extract partial text from selection segment.
		if (segments[1].length == 1) {
			var prePartialResult = this.parsePartialStartNode(segments[1][0], range.startOffset);
			var postPartialResult = this.parsePartialEndNode(segments[1][0], range.endOffset);
			pre = pre + prePartialResult.pre
			selection = (segments[1][0].nodeType != 3) ? prePartialResult.selected : this.getTextFromNode(segments[1][0]).substring(range.startOffset, range.endOffset);
			post = postPartialResult.post + post;
		} else {
			var partialStart = segments[1][0];
			var partialEnd = segments[1][segments[1].length - 1];
			var prePartialResult = this.parsePartialStartNode(partialStart, range.startOffset);
			var postPartialResult = this.parsePartialEndNode(partialEnd, range.endOffset);
			var midSelected = this.extractTextFromSegment(segments[1].slice(1, segments[1].length - 1));
			pre = pre + prePartialResult.pre
			selection = prePartialResult.selected + midSelected + postPartialResult.selected;
			post = postPartialResult.post + post;
		}

		// console.log("PRE TEXT: " + pre);
		// console.log("SELECTION TEXT: " + selection);
		// console.log("POST TEXT: " + post);

		//Step 4 - return info for this selection.
		if(isLinebreak){
			return {
				from: pre.length,
				length: selection.length + 2,
				actionId: Ext.data.IdGenerator.get('uuid').generate()
			}
		}
		return {
			from: pre.length,
			length: selection.length,
			actionId: Ext.data.IdGenerator.get('uuid').generate()
		};
	},
	extractTextFromSegment : function(segment){
		var str = '';
		for (var i = 0; i < segment.length; i++) {
			if (segment[i].nodeType == 3)
				str += this.getTextFromNode(segment[i]);
			else {
				if (segment[i].getAttribute('marker') == "new_line")
					str += (Ext.is.Windows ? '\r\n' : '\n');
				else if (segment[i].getAttribute('marker') == "space")
					str += ' ';
			}
		}	
		return str;
	},
	getTextFromNode: function(node) {
		if(node.parentNode && node.parentNode.getAttribute('type') == 'endOfLine')
			return '';
		var textPropName = node.textContent === undefined ? 'nodeValue' : 'textContent';
		return node[textPropName];
	},
	parsePartialStartNode: function(node, startOffset) {
		return {
			pre: this.getTextFromNode(node).substring(0, startOffset),
			selected: this.getTextFromNode(node).substring(startOffset, this.getTextFromNode(node).length)
		};
	},
	parsePartialEndNode: function(node, endOffset) {
		return {
			post: this.getTextFromNode(node).substring(endOffset, this.getTextFromNode(node).length),
			selected: this.getTextFromNode(node).substring(0, endOffset)
		};
	},
	
	//MARKUP SELECTION LOGIC
	processMarkupSelection : function(actionId){
		this.set('currentSelection',null);
		this.set('selectionIsInvalid', false);		
		var selectedMarkup = {};
		for(var i = 0; i < this.markups.length; i++){
			selectedMarkup = this.markups[i];
			if(selectedMarkup.actionId == actionId)
				break;
		}
		this.set('selectedMarkup', selectedMarkup);		
		this.patternProcessor.updatePatternForMarkup(selectedMarkup['type'],selectedMarkup['autoType']);
	}
});
glu.defModel("RS.actiontaskbuilder.TableParser",{
	mixins : ['VariableUtil','BoundaryUtil'],	
	sample :'',
	smallResolution : false,	
	newVariableCounter : 1,
	markups : [],
	columnSeparator : null,
	rowSeparator : null,
	trim : true,	
	parserReady : false,
	loadingData: false,
	parserData : null,
	selectedColumn : null,
	tabWidth : 8,
	variableColumn : null,
	selectionInfo : null,
	parseStart : '',
	parseStartOrignalData : '',
	startTextIncluded : true,
	parseEnd : '',
	parseEndOrignalData : '',
	endTextIncluded : true,
	totalColumn : 0,
	activeScreen : 0,
	regExpForSeparatorMap : {
		comma : '[,]+',
		space : '[ ]+',
		tab : '\\t+',
		unix :  Ext.is.Windows ? '\r\n' : '\n',
		win :  Ext.is.Windows ? '\r\n' : '\n',
	},
	codeScreen : {	
		mtype : 'ParserCode',
		parserType : 'table'
	},
	colorMap:['black','#276888','#BD6161','#F3955D','#96BB35','#50C6E8'],
	variableStore : {
		mtype : 'store',
		fields : ['variable','column','color','containerType']
	},
	columnSeparatorStore : null,
	rowSeparatorStore : null,
	columnDropdownStore : {
		mtype : 'store',
		fields : ['display','value','color'],
		proxy : {
			type : 'memory'
		}
	},	
	total_column_changed : {
		on : ['totalColumnChanged'],
		action : function(){
			this.columnDropdownStore.removeAll();
			if(this.totalColumn > 0){				
				for(var i = 1; i <= this.totalColumn;i++){
					var color = this.colorMap[(i - 1) % 6];
					this.columnDropdownStore.add({
						display : 'Column ' + i,
						value : i,
						color : color
					})
				}
			}
		}
	},
	when_selectedColumn_changed : {
		on : ['selectedColumnChanged'],
		action : function(){
			if(this.selectedColumn != null){
				this.fireEvent('updateColumnSelection', this.selectedColumn);
			}
		}
	},
	when_parser_ready : {
		on : ['parserReadyChanged'],
		action : function(){
			if(this.parserReady){
				this.populateData(this.parserData);
			}
		}
	},
	update_script : {
		on :['markupsChanged','variableStoreChanged'],
		action : function(){
			if(this.parserReady && !this.loadingData)
				this.codeScreen.generateCode ();
		}
	},	
	update_dirty_flag : {
		on : ['sampleChanged'],
		action : function(){
			this.notifyDirty();
		}
	},
	notify_data_changed : {
		on : ['variableStoreChanged'],
		action : function(){
			this.notifyDataChange();
		}
	},
	when_resolution_change : {
		on : ['smallResolutionChanged'],
		action : function(){			
			this.fireEvent('embeddedLayout',  this.smallResolution);
		}
	},

	init : function(){		
		var me = this;
		var colSeparatorStore = Ext.create('Ext.data.Store',{
			fields : ['display','value'],
			proxy : {
				type : 'memory'
			},
			data : [{
				display : this.localize('comma'),
				value : 'comma'
			},{
				display : this.localize('space'),
				value : 'space'
			},{
				display : this.localize('tab'),
				value : 'tab'
			}]
		});
		this.set('columnSeparatorStore',colSeparatorStore);
		this.set('columnSeparator','space');
		var rowSeparatorStore = Ext.create('Ext.data.Store',{
			fields : ['display','value'],
			proxy : {
				type : 'memory'
			},
			data : [{
				display : this.localize('unixLinebreak'),
				value : 'unix'
			},{
				display : this.localize('winLinebreak'),
				value : 'win'
			}]
		});
		this.set('rowSeparatorStore', rowSeparatorStore);
		this.set('rowSeparator','unix');
		this.set('variableColumn', this.getTableVariableColumnConfig());
		this.variableStore.on('datachanged', function(){
			this.fireEvent('variableStoreChanged');
		},this);
		this.variableStore.on('update', function(){
			this.fireEvent('variableStoreChanged');
		},this);
		this.columnDropdownStore.on('datachanged', function(){
			this.fireEvent('columnDropdownStoreChanged');
		},this);
		this.columnDropdownStore.on('update', function(){
			this.fireEvent('columnDropdownStoreChanged');
		},this);	
	},

	//HELPERS
	showCode : function(){
		this.set('activeScreen', 1);
	},
	showMarkup : function(){
		this.set('activeScreen', 0);
	},	
	getCode : function(){
		return this.codeScreen.getCode();
	},	
	isAssessTask: function(flag){
		this.codeScreen.isAssessTask(flag);
	},
	setDefaultParser: function(){
		if(this.parserReady){
			this.clearSample();		
			this.codeScreen.resetCode();
			this.set('activeScreen',0);			
		}
	},
	loadData : function(parserData){
		this.set('parserData', parserData);
		if(this.parserReady){
			this.populateData(parserData);
		}
	},
	populateData : function(parserData){
		if(parserData != null && Object.keys(parserData).length !== 0){
			this.set('loadingData', true);		
			this.updateSample(parserData.sampleOutput || "");
			this.variableStore.removeAll();

			//Populate Markups
			if(parserData.parserConfig){
				try {
				var parserConfig = JSON.parse(parserData.parserConfig);
				}catch (e) {
					clientVM.displayError(this.localize('invalidJSON'));
				}
				if(parserConfig){
					this.set('markups', parserConfig['markups'] );
				
					//Column,row separator
					this.set('rowSeparator', parserConfig.rowSeparator.replace(/\u00a7/g, "").toLowerCase());
					this.set('columnSeparator', parserConfig.columnSeparator.replace(/\u00a7/g, "").toLowerCase());

					//Set up boundary
					var parserOptions = parserConfig['options'];
					this.set('startTextIncluded', parserOptions.boundaryStartIncluded);
					this.set('endTextIncluded', parserOptions.boundaryEndIncluded);
					for(var i = 0; i < this.markups.length; i++){
						var currentMarkup = this.markups[i];
						if(currentMarkup.type == "parseStart" ){
							this.set('parseStart', currentMarkup.boundaryInput);
						}
						else if(currentMarkup.type == "parseEnd" ){
							this.set('parseEnd', currentMarkup.boundaryInput);
						}
					}
					this.set('parseStartOrignalData', this.parseStart);
					this.set('parseEndOrignalData', this.parseEnd);
					//Populate column pickers
					var totalColumn = 0;
					for(var i = 0; i < this.markups.length; i++){
						var currentMarkup = this.markups[i];
						if(currentMarkup.type == "column"){
							totalColumn = Math.max(totalColumn, currentMarkup.column);
						}			
					}
					this.set('totalColumn', totalColumn);

					//Variable Table	
					var variableData = [];				
					for(var i = 0; i < parserConfig.tblVariables.length; i++){
						var currentVariable = parserConfig.tblVariables[i];
						if(currentVariable.flow)
							var containerType = 'FLOW';
						else if (currentVariable.wsdata)
							containerType = 'WSDATA';
						else
							containerType = 'OUTPUT';
						variableData.push({	
							id : currentVariable.id,		
							variable : currentVariable.variable,
							column : currentVariable.columnNum,
							color : this.colorMap[(currentVariable.columnNum - 1) % 6],
							containerType : containerType			
						})
					}
					this.variableStore.loadData(variableData);
				}
			}
			else if(parserData['uscript']){
				//Show Code if this only has code
				this.set('activeScreen', 1);
			}

			//Populate Code
			this.codeScreen.populateCode(parserData['parserAutoGenEnabled'], parserData['uscript']);
		
			this.set('loadingData',false);		
		}
	},
	updateSample : function(data, asPasted){
		if(this.markups.length > 0 && asPasted){
			this.message({
				title : this.localize('overwriteMarkupTitle'),
				msg : this.localize('overwriteMarkupBody'),
				button : Ext.MessageBox.YESNO,
				buttonText : {
					yes : this.localize('confirm'),
					no : this.localize('cancel')
				},
				scope : this,
				fn : function(btn){
					if(btn == 'yes'){					
						this.clearSample();
						this.set('sample', data);
						this.parentVM.updateDefaultTestSample(data);							
					}
				}
			})
		}
		else
		{
			this.set('sample', data);
			this.parentVM.updateDefaultTestSample(data);				
		}
	},	
	getSample : function(){
		return this.sample;
	},
	getParserConfig : function(){
		if(this.markups.length != 0){
			var options = {
				boundaryStartIncluded : this.startTextIncluded,
				boundaryEndIncluded : this.endTextIncluded
			}
			var variables = [];
			for(var i = 0; i < this.variableStore.data.length;i++){
				var variableData = this.variableStore.getAt(i).data;
				variables.push({
					id : variableData.id,
					flow : (variableData.containerType == "FLOW"),
					wsdata : (variableData.containerType == "WSDATA"),
					output : (variableData.containerType == "OUTPUT"),
					variable : variableData.variable,
					columnNum: variableData.column,
				})
			}		
			var	parserConfig = JSON.stringify({ 
				markups : this.markups,
				columnSeparator : "\u00a7" + this.columnSeparator.toUpperCase() + "\u00a7",
				rowSeparator : "\u00a7" + this.rowSeparator.toUpperCase() + "\u00a7",
				tblVariables: variables,
				options : options,
				generatedFromATB : true
			});		
			return parserConfig;
		}
		else
			return null;
	},
	getCode : function(){
		return this.codeScreen.getCode();
	},
	getParameterList : function(){
		var variableArray = [];
		this.variableStore.each(function(entry){
			variableArray.push({
				variableId : entry.get('id'),
				name : entry.get('variable'),
				containerType : entry.get('containerType')
			})
		})
		return variableArray;		
	},
	isAutoGenCode : function(){
		return this.codeScreen.isAutoGenCode();
	},
	isParserReady : function(){
		return this.parserReady;
	},	
	resetParser : function(){
		this.set('selectionInfo',null);
		this.set('selectedColumn',null);		
	},
	notifyDirty : function(){
		if(!this.loadingData)
			this.parentVM.notifyDirty();
	},
	notifyDataChange : function(){
		this.parentVM.notifyDataChange();
	},
	//VIEW EVENT HANDLERS
	applySeparator : function(){
		if(this.variableStore.getCount() > 0){
			this.message({
				title : this.localize('overwriteCaptureTitle'),
				msg : this.localize('overwriteCaptureBody'),
				buttons : Ext.MessageBox.YESNO,
				buttonText : {
					yes : this.localize('yes'),
					no : this.localize('no')
				},
				scope : this,
				fn: function(btn){
					if(btn == "yes"){
						this.clearMarkup();
						this.processApplySeperator();
					}
				}
			})
		}
		else {
			this.processApplySeperator();
		}
		
	},
	processApplySeperator : function(){
		if(this.selectionInfo == null){
			//Select entire reference output
			var selectionInfo = {
				actionId : Ext.data.IdGenerator.get('uuid').generate(),
				from : 0,
				length : this.sample.length
			}
		}
		else
			selectionInfo = this.selectionInfo;

		var newMarkups = this.computeTableData(selectionInfo);
		for(var i = 0; i < this.markups.length; i++){
			var markup = this.markups[i];
			if(markup.type == 'parseStart' || markup.type == 'parseEnd'){
				newMarkups.push(markup);
			}
		}
		this.set('markups', newMarkups);
		this.set('selectionInfo', null);
	},
	clearSample : function(){	
		this.updateSample("");
		this.clearMarkup(true);
		this.set('parseStart', '');
		this.set('parseEnd', '');		
	},	
	clearMarkup : function(clearAll){
		var boundaryMarkups = [];
		for(var i = 0; i < this.markups.length; i++){
			var markup = this.markups[i];
			if(markup.type == "parseStart" || markup.type == "parseEnd"){
				boundaryMarkups.push(markup);
			}
		}
		this.set('markups', clearAll ? [] : boundaryMarkups);
		this.variableStore.removeAll();
		this.newVariableCounter = 1;
		this.set('totalColumn',0);
		this.resetParser();
	},	
	captureVariable : function(){			
		var variableName = "NewVariable" + this.newVariableCounter;
		while(this.variableStore.findExact("variable",variableName) != -1){
			this.newVariableCounter++;
			variableName = "NewVariable" + this.newVariableCounter;
		}
		var existingVariableName = this.variableStore.collect('variable');
		var newMarkup = {
			id : Ext.data.IdGenerator.get('uuid').generate(),
			variable : variableName,
			column : this.selectedColumn,
			color : this.colorMap[(this.selectedColumn - 1) % 6]		
		}		
		this.open({
			mtype : 'MarkupCapture',
			newMarkup : newMarkup,
			existingVariableName : existingVariableName,
			parserType : 'table',
			dumper : {
				dump : this.processCaptureVariable,
				scope : this
			}
		})
	},
	processCaptureVariable : function(newMarkup){
		for(var i = 0; i < this.markups.length; i++){
			var currentMarkup = this.markups[i];
			if(currentMarkup.column == newMarkup.column && currentMarkup.type == "column"){
				currentMarkup.action = 'capture';
			}
		}
		//Add to variable grid
		this.variableStore.add(newMarkup);
		this.fireEvent('updateCaptureColumn', this.markups);
	},
	removeVariable : function(record){
		if(!this.isViewOnly){
			var removedVariableId = record.raw.id;
			var variableStoreIndex = this.variableStore.indexOfId(removedVariableId);
			var columnNo = this.variableStore.getAt(variableStoreIndex).raw.column;		
			for(var i = 0; i < this.markups.length; i++){
				var currentMarkup = this.markups[i];
				if(currentMarkup.column == columnNo && currentMarkup.type == "column"){
					currentMarkup.action = 'default';
				}
			}
			this.fireEvent('updateCaptureColumn', this.markups);
			this.variableStore.removeAt(variableStoreIndex);
		}
	},

	//VIEW COMPONENT VISIBILITY
	applySeparatorIsEnabled$ : function(){
		return this.sample != '' && this.activeScreen == 0;
	},
	captureVariableIsEnabled$ : function(){
		return this.selectedColumn !=null;
	},
	clearSampleIsEnabled$ : function(){
		return this.sample != '' && this.activeScreen == 0;
	},	
	clearMarkupIsEnabled$ : function(){
		return this.sample != '' && this.activeScreen == 0 && this.markups && this.markups.length > 0;
	},
	variableStoreIsEmpty$ : function(){		
		return this.variableStore.getCount() == 0;
	},
	showCodeIsVisible$: function(){		
		return this.activeScreen == 0;
	},
	showMarkupIsVisible$: function(){
		return this.activeScreen == 1;
	},
	controlBorderRegion$ : function(){
		return this.smallResolution ? 'south' : 'east';
	},
	controlHeight$ : function(){
		return this.smallResolution ? 900 : 450;
	},
	controlMargin$ : function(){
		return this.smallResolution ? '10 0 0 0' : '0 0 0 10';
	},
	selectedColumnIsEnabled$ : function(){
		return this.columnDropdownStore.getCount() > 0;
	},
	
	//SELECTION LOGIC	
	processHighlightSelection : function(selection,documentBody, isLinebreak){	
		var selectionInfo = this.getSelectionInfo(selection,documentBody,isLinebreak);		
		this.set('selectionInfo',selectionInfo);
	},	
	processColumnSelection : function(colNo){	
		var colName = this.columnDropdownStore.getAt(colNo - 1);
		this.set('selectedColumn', colName.get('value'));
	},
	//This method will return start position, length of selection as well as unique actionId for that selection.
	getSelectionInfo : function(selection, documentBody, isLinebreak){
		var range = selection.getRangeAt(0);
		var startNode = range.startContainer;
		//Incase selection start from linebreak.
		while(!isLinebreak && startNode.nodeType == 1 && startNode.getAttribute('marker') == "new_line"){
			startNode = startNode.nextSibling;	
		}
		//Get wrapper node of marker instead of content
		if (startNode.parentNode && startNode.parentNode.hasAttribute('marker'))
			startNode = startNode.parentNode
		var endNode = range.endContainer;
		if (endNode.parentNode && endNode.parentNode.hasAttribute('marker'))
			endNode = endNode.parentNode;
		var segments = [];
		var currentSegment = [];
		var selecting = false;

		//Step 1 - Put DOM nodes in correct segments based on selection.
		function segmentDOMNode(node){
			for (var i = 0; i < node.childNodes.length; i++) {
				if (node.childNodes[i] == startNode || node.childNodes[i] == endNode) {
					if (!selecting) {
						segments.push(currentSegment);
						if (startNode == endNode) {
							currentSegment = [node.childNodes[i]]; //which is also start,end
							segments.push(currentSegment);
							currentSegment = [];
							continue;
						}
						currentSegment = [];
						selecting = true;
					} else {
						currentSegment.push(node.childNodes[i]); //which is the logical 'end'(it can be start or end coz the selection direction may be reversed)
						segments.push(currentSegment);
						currentSegment = [];
						selecting = false;
						continue;
					}
				}
				if (node.childNodes[i].nodeType == 3)
					currentSegment.push(node.childNodes[i]);
				else if (node.childNodes[i].hasAttribute('marker'))
					currentSegment.push(node.childNodes[i]);
				else if (!node.childNodes[i].getAttribute('exclude'))
					segmentDOMNode(node.childNodes[i]);
			}
		}
		segmentDOMNode(documentBody);
		segments.push(currentSegment);
		
		//Step 2 - Extract raw text from each segment
		var pre = this.extractTextFromSegment(segments[0]);
		var selection = this.extractTextFromSegment(segments[1]);
		var post = this.extractTextFromSegment(segments[2]);

		//Step 3 - Extract partial text from selection segment.
		if (segments[1].length == 1) {
			var prePartialResult = this.parsePartialStartNode(segments[1][0], range.startOffset);
			var postPartialResult = this.parsePartialEndNode(segments[1][0], range.endOffset);
			pre = pre + prePartialResult.pre
			selection = (segments[1][0].nodeType != 3) ? prePartialResult.selected : this.getTextFromNode(segments[1][0]).substring(range.startOffset, range.endOffset);
			post = postPartialResult.post + post;
		} else {
			var partialStart = segments[1][0];
			var partialEnd = segments[1][segments[1].length - 1];
			var prePartialResult = this.parsePartialStartNode(partialStart, range.startOffset);
			var postPartialResult = this.parsePartialEndNode(partialEnd, range.endOffset);
			var midSelected = this.extractTextFromSegment(segments[1].slice(1, segments[1].length - 1));
			pre = pre + prePartialResult.pre
			selection = prePartialResult.selected + midSelected + postPartialResult.selected;
			post = postPartialResult.post + post;
		}

		//Step 4 - return info for this selection.
		if(isLinebreak){
			return {
				from: pre.length,
				length: selection.length + 2,
				actionId: Ext.data.IdGenerator.get('uuid').generate()
			}
		}
		return {
			from: pre.length,
			length: selection.length,
			actionId: Ext.data.IdGenerator.get('uuid').generate()
		};
	},
	extractTextFromSegment : function(segment){
		var str = '';
		for (var i = 0; i < segment.length; i++) {
			if (segment[i].nodeType == 3)
				str += this.getTextFromNode(segment[i]);
			else {
				if (segment[i].getAttribute('marker') == "new_line")
					str += (Ext.is.Windows ? '\r\n' : '\n');
				else if (segment[i].getAttribute('marker') == "space")
					str += ' ';
			}
		}	
		return str;
	},
	getTextFromNode: function(node) {
		if(node.parentNode && node.parentNode.getAttribute('type') == 'endOfLine')
			return '';
		var textPropName = node.textContent === undefined ? 'nodeValue' : 'textContent';
		return node[textPropName];
	},
	parsePartialStartNode: function(node, startOffset) {
		return {
			pre: this.getTextFromNode(node).substring(0, startOffset),
			selected: this.getTextFromNode(node).substring(startOffset, this.getTextFromNode(node).length)
		};
	},
	parsePartialEndNode: function(node, endOffset) {
		return {
			post: this.getTextFromNode(node).substring(endOffset, this.getTextFromNode(node).length),
			selected: this.getTextFromNode(node).substring(0, endOffset)
		};
	},	

	//APPLY SEPARATOR LOGIC	
	getRegexForSeparator : function(separator){
		return new RegExp(this.regExpForSeparatorMap[separator], 'g');
	},
	computeTableData : function(selectionInfo){
		var processedTable = this.extractTableFromSample(selectionInfo);
		var rowSeparatorRegEx = this.getRegexForSeparator(this.rowSeparator);
		var offset = selectionInfo.from;
		var markups = [];
		var colors = [];
		var totalColumn = 0;
		for (var i = 0; i < processedTable.length; i++) {
			var row = processedTable[i];
			var col = 1;
			if (!row[0])
				col--;
			for (var j = 0; j < row.length; j++) {				
				markups.push({
					from: offset,
					length: row[j].length,
					type: (j % 2 == 0 ? 'column' : 'delimiter'),					
					actionId: Ext.data.IdGenerator.get('uuid').generate(),
					column: col,
					expand: true,
					action: "type",				
					color: (j % 2 == 0 ? this.colorMap[(col - 1) % 6] : null)					
				});
				col += (j % 2 == 0 ? 1 : 0);
				offset += row[j].length;
			}
			//Dont add end of line for last row.
			if(i != processedTable.length - 1){
				markups.push({
					from: offset,
					length: 2,
					action: "type",			
					actionId: Ext.data.IdGenerator.get('uuid').generate(),
					column: col,
					expand: true,
					type : 'endOfLine'
				})
				offset+= 2;
			}
			totalColumn = Math.max(col - 1,totalColumn);
		}
		this.set('totalColumn', totalColumn);
		return markups;
	},
	extractTableFromSample : function(selectionInfo){
		var table = this.sample.substring(selectionInfo.from, selectionInfo.from + selectionInfo.length);
		var rows = table.split(this.getRegexForSeparator(this.rowSeparator));
		var splittedRows = [];
		var matchedColSep = [];
		var columnRegex = this.getRegexForSeparator(this.columnSeparator);
		Ext.each(rows, function(row) {
			splittedRows.push(row.split(columnRegex));
			matchedColSep.push(row.match(columnRegex));
		}, this)
		var spaceHead = false;
		var spaceTail = false;
		var processed = [];
		for (var i = 0; i < splittedRows.length; i++) {
			var r = [];
			for (var j = 0; j < splittedRows[i].length - 1; j++) {
				r.push(splittedRows[i][j]);
				r.push(matchedColSep[i][j]);
			}
			r.push(splittedRows[i][splittedRows[i].length - 1]);
			processed.push(r);
			if (r[0] == '')
				spaceHead = true;
			if (r[r.length - 1] == '')
				spaceTail = true;
		}
		Ext.each(processed, function(row) {
			if (row[0] != '' && spaceHead)
				Ext.Array.insert(row, 0, ['', '']);
		});
		return processed;
	}	
});
glu.defModel('RS.actiontaskbuilder.TestControl',{
	testTabPanel : null,
	activeTestSample : 0,
	code : null,
	testResultTitle : '',
	testResultStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},
	API : {		
		testCode : '/resolve/service/resolutionbuilder/testCode',
		pollTest : '/resolve/service/resolutionbuilder/testCode/poll'
	},	
	testDataMap : {},
	defaultTestSample : '',
	activeParserType : 'text',
	when_default_test_sample : {
		on : ['defaultTestSampleChanged'],
		action : function(newVal){
			this.testDataMap[this.activeParserType] = newVal;
		}
	},
	init : function(){
		this.set('testResultTitle', this.localize('testResult', this.localize('defaultTestSampleTitle')));
	},
	registerTestTab : function(testTabPanel){
		this.set('testTabPanel', testTabPanel);
	},
	updateActiveParserType : function(parserType){
		this.set('activeParserType', parserType);
		this.set('defaultTestSample', this.testDataMap[parserType]);
	},
	updateDefaultTestSample : function(parserType, data){
		this.testDataMap[parserType] = data;
	},
	resetTest : function(){
		if(this.testTabPanel){
			while(this.testTabPanel && this.testTabPanel.items.length > 2){
				var testTab = this.testTabPanel.items.getAt(1);
				this.testTabPanel.remove(testTab);
			}
			this.testTabPanel.items.getAt(1).testTabCounter = 1;
		}
		
		this.set('activeTestSample', 0);
		this.set('testDataMap', {});
		this.testResultStore.removeAll();
	},
	executeTest: function() {
		this.set('code', this.parentVM.getCode());
 		this.startTestProcess();	
	},
	startTestProcess : function(){
		this.testResultStore.removeAll();
		if(!this.code){
			this.testResultStore.add({
				name: 'Message',					
				value: this.localize('noScriptToExecute')
			});
			return;
		}
		var currentActiveTestTab = this.testTabPanel.getActiveTab();
		this.set('testResultTitle', this.localize('testResult', currentActiveTestTab.title));
		var testSample = currentActiveTestTab.getValue();
		this.ajax({
			url: this.API['testCode'],
			params: {
				code: this.code,
				testData: testSample
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);

				if (respData.success) {
					var indicator = clientVM.displayMessage(this.localize('executeTest'), this.localize('executeTestInProgress'), {
						duration: -1
					});
					
					this.pollTest(indicator);
				} else {
					clientVM.displayError(respData.message)
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},	
	pollTest: function(indicator) {
		this.ajax({
			url: this.API['pollTest'],
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				var me = this;

				if (!respData.success || respData.data.stage !== 'DONE') {
					setTimeout(function() {
						me.pollTest(indicator);
					});
					return;
				}

				var data = respData.data;
				this.testResultStore.removeAll();
				if(data.success){
					var result = data.result;
					if(result && Object.keys(result).length > 0){
						Ext.Object.each(result, function(k, v) {
							//Ignore those 2 entries since they show useless infomation (maybe remove from ajax later).
							if(k == 'parsing content' || k == 'regex matched')
								return;
							this.testResultStore.add({
								name: k,
								value: v							
							});
						}, this);	
					}
					else
					{
						this.testResultStore.add({
							name: 'Message',					
							value: this.localize('noResultMsg')
						});
					}
					
				}
				else{
					this.testResultStore.add({
						name: 'Error',					
						value: data.message
					});
				}
				indicator.fadeOut({
					remove: true
				});							
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},	
})

glu.defModel('RS.actiontaskbuilder.VariableUtil',{
	colorPickerStore : {
		mtype :'store',
		proxy : {
			type : 'memory'
		},
		fields: [
		{	
			name: 'value',
			convert: function(v, record) {
				return record.raw;
			}
		}],
		data: ['#50C6E8','#96BB35','#F3955D','#BD6161','#276888','black']
	},
	containerTypeStore : {
		mtype : 'store',
		proxy : {
			type : 'memory'
		},
		fields : [{
			name : 'value',
			convert : function(v, record){
				return record.raw;
			}
		}],
		data: ['WSDATA','FLOW','OUTPUT']
	},
	patternTypeStore : null,
	textVariableColumnConfig : null,
	tableVariableColumnConfig : null,
	xmlVariableColumnConfig : null,
	initMixin : function(){
		var me = this;
		this.set('patternTypeStore', Ext.create('Ext.data.Store',
		{	
			proxy : {
				type :'memory'
			},
			fields: [{
				name: 'display',
				convert: function(v, record) {
					if (record.raw == '-' || record.raw == '-<>')
						return '';
					return me.localize(record.raw);
				}
			}, {
				name: 'value',
				convert: function(v, record) {
					return record.raw;
				}
			}],
			data: [
				'ignoreNotGreedy',
				'ignoreRemainder',
				'-',
				'spaceOrTab',
				'whitespace',
				'-',
				'letters',
				'lettersAndNumbers',
				'notLettersOrNumbers',
				'lowerCase',
				'upperCase',
				'-',
				'number',
				'notNumber',
				'-',
				'IPv4',
				'MAC',
				'decimal'
			]		
		}));

		this.set('textVariableColumnConfig', [
		{
			header: 'Name',
			dataIndex: 'variable',
			editor: 'textfield',
			flex: 3
		},{
			header : 'Pattern',
			dataIndex : 'type',
			align : 'center',
			width : 200,
			renderer : function(value){
				return me.localize(value);
			},
			editor : 
			{
				xtype : 'combo',	
				store : this.patternTypeStore,
				editable : false,				
				displayField : 'display',
				valueField : 'value',
				queryMode : 'local',
				tpl: Ext.create('Ext.XTemplate',
					'<tpl for=".">',
					'<div class="x-boundlist-item">{display}<tpl if="value==\'-\'"><hr/></tpl></div>',
					'</tpl>'
				)
			}				
		},{
			header: 'Color',
			dataIndex: 'color',
			align : 'center',
			renderer: function(value, meta) {
				meta.style = 'background-color:' + value;
			},
			width : 200,
			editor: {
				xtype: 'combo',				
				editable : false,
				queryMode: 'local',					
				valueField: 'value',
				store: this.colorPickerStore,
				tpl: Ext.create('Ext.XTemplate',
					'<tpl for=".">',
					'<div class="x-boundlist-item capture-color-picker" style="background:{value}"></div>',
					'</tpl>'
				),
				listeners: {
					select:function(combo, records){
				        var value = this.getValue();
				        this.setFieldStyle('background:'+ value);
				    }
				}
			}
		},{
			header : 'Container',
			dataIndex : 'containerType',
			align : 'center',
			width : 200,
			editor: {
				xtype: 'combo',				
				editable : false,
				queryMode: 'local',					
				valueField: 'value',
				displayField: 'value',
				store: this.containerTypeStore				
			}
		},{
			xtype: 'actioncolumn',		
			hideable: false,
			sortable: true,
			align: 'center',			
			width: 45,			
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record ) {
					this.up('grid').fireEvent('removeVariable', view, record);
				}
			}]
		}]);

		this.set('tableVariableColumnConfig', [{
			header: 'Name',
			dataIndex: 'variable',
			editor : 'textfield',
			flex: 3
		},{
			header : 'Column Number',
			dataIndex : 'column',
			align : 'center',				
			width : 200
		},{
			header: 'Color',
			dataIndex: 'color',
			align : 'center',
			renderer: function(value, meta) {
				meta.style = 'background-color:' + value;
			},
			width : 200
		},{
			header : 'Container',
			dataIndex : 'containerType',
			align : 'center',
			width : 200,
			editor: {
				xtype: 'combo',				
				editable : false,
				queryMode: 'local',					
				valueField: 'value',
				displayField: 'value',
				store: this.containerTypeStore				
			}
		},{
			xtype: 'actioncolumn',		
			hideable: false,
			sortable: true,
			align: 'center',			
			width: 45,		
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record ) {
					this.up('grid').fireEvent('removeVariable', view, record);
				}
			}]
		}]);
		
		this.set('xmlVariableColumnConfig',[{
			header: 'Name',
			dataIndex: 'variable',
			editor : {
				xtype : 'textfield'
			},
			flex: 1
		},{
			header : 'XPath',
			dataIndex : 'xpath',
			editor : 'textfield',
			flex: 5
		},{
			header : 'Container',
			dataIndex : 'containerType',
			align : 'center',
			width : 200,
			editor: {
				xtype: 'combo',				
				editable : false,
				queryMode: 'local',					
				valueField: 'value',
				displayField: 'value',
				store: this.containerTypeStore				
			}
		},{
			xtype: 'actioncolumn',		
			hideable: false,
			sortable: true,
			align: 'center',			
			width: 45,
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record ) {
					this.up('grid').fireEvent('removeVariable', view, record);
				}
			}]
		}]);
	},
	convertToRealType : function(autoType){
		var realType = autoType.substring(2, autoType.length);
		if(realType == "MAC" || realType == "IPv4")
			return realType
		else
			return realType.charAt(0).toLowerCase() + realType.slice(1);
	},
	getRandomColor : function(){
		var totalColor = this.colorPickerStore.data.length;
		var radomColorIndex = Math.floor(Math.random() * (totalColor - 1));
		return this.colorPickerStore.getAt(radomColorIndex).raw;
	},
	getTextVariableColumnConfig : function(){
		return this.textVariableColumnConfig;
	},
	getTableVariableColumnConfig : function(){
		return this.tableVariableColumnConfig;
	},
	getXmlVariableColumnConfig : function(){
		return this.xmlVariableColumnConfig;
	},
	displayDuplicateNameMsg : function(){
		clientVM.message(this.localize('invalidVariableTitle'),this.localize('duplicateVariableMsg'));		
	},
	displayInvalidCharacter : function(){
		clientVM.message(this.localize('invalidVariableTitle'),this.localize('validKeyCharacters'));		
	}
});
glu.defModel("RS.actiontaskbuilder.XMLParser",{
	mixins : ['VariableUtil'],
	sample : '',
	smallResolution : false,
	newVariableCounter : 1,
	currentSampleDom : null,
	augmentedXPath: {},
	validXml : null,
	xpathSelections: [],
	invalidXPath: false,
	parserReady : false,
	loadingData : false,
	parserData : null,
	textXPath: '',
	xPathIsEditable : false,
	variableStore : {
		mtype : 'store',
		fields : ['variable','xpath','containerType']
	},
	activeScreen : 0,
	codeScreen : {	
		mtype : 'ParserCode',
		parserType : 'xml'
	},
	xPathDetail : {
		mtype : 'XPathTokenEditor'
	},
	variableColumn : null,
	supportActiveXObject: ((Object.getOwnPropertyDescriptor && Object.getOwnPropertyDescriptor(window, "ActiveXObject")) || ("ActiveXObject" in window)),
	when_parser_ready : {
		on : ['parserReadyChanged'],
		action : function(){
			if(this.parserReady){
				this.populateData(this.parserData);
			}
		}
	},
	update_script : {
		on :['markupsChanged','variableStoreChanged'],
		action : function(){
			if(this.parserReady && !this.loadingData)
				this.codeScreen.generateCode();
		}
	},
	update_dirty_flag : {
		on : ['sampleChanged'],
		action : function(){
			this.notifyDirty();
		}
	},
	notify_data_changed : {
		on : ['variableStoreChanged'],
		action : function(){
			this.notifyDataChange();
		}
	},
	when_resolution_change : {
		on : ['smallResolutionChanged'],
		action : function(){			
			this.fireEvent('embeddedLayout',  this.smallResolution);
		}
	},
	
	init : function(){
		this.set('variableColumn', this.getXmlVariableColumnConfig());
		this.variableStore.on('datachanged', function(){
			this.fireEvent('variableStoreChanged');
		},this);
		this.variableStore.on('update', function(){
			this.fireEvent('variableStoreChanged');
		},this);
	},

	//HELPERS
	showCode : function(){
		this.set('activeScreen', 1);
	},
	showMarkup : function(){
		this.set('activeScreen', 0);
	},	
	isAssessTask: function(flag){
		this.codeScreen.isAssessTask(flag);
	},
	setDefaultParser: function(){
		if(this.parserReady){
			this.clearSample();		
			this.codeScreen.resetCode();
			this.set('activeScreen',0);
		}
	},
	loadData : function(parserData){
		this.set('parserData', parserData);
		if(this.parserReady){
			this.populateData(parserData);
		}
	},
	populateData : function(parserData){
		if(parserData != null && Object.keys(parserData).length !== 0){
			this.set('loadingData',true);		
			this.updateSample(parserData.sampleOutput || "");	
			this.variableStore.removeAll();

			//Populate Markups
			if(parserData.parserConfig){
				try {
				var parserConfig = JSON.parse(parserData.parserConfig);
				}catch (e) {
					clientVM.displayError(this.localize('invalidJSON'));
				}
				if(parserConfig){
					var variableData = [];
					for(var i = 0; i < parserConfig.variables.length; i++){
						var currentVariable = parserConfig.variables[i];
						if(currentVariable.flow)
							var containerType = 'FLOW';
						else if (currentVariable.wsdata)
							containerType = 'WSDATA';
						else
							containerType = 'OUTPUT';
						variableData.push({
							id : currentVariable.id,
							variable : currentVariable.variable,		
							xpath : currentVariable.xpath,
							containerType : containerType			
						})	
					}
					this.variableStore.loadData(variableData);
				}
			}
			else if(parserData['uscript']){
				//Show Code if this only has code
				this.set('activeScreen', 1);
			}

			//Populate Code
			this.codeScreen.populateCode(parserData['parserAutoGenEnabled'], parserData['uscript']);
		
			this.set('loadingData', false);
		}
	},
	updateSample : function(data, asPasted){		
		if((this.variableStore.data.length > 0 || this.textXPath) && asPasted){
			this.message({
				title : this.localize('overwriteMarkupTitle'),
				msg : this.localize('overwriteMarkupBody'),
				button : Ext.MessageBox.YESNO,
				buttonText : {
					yes : this.localize('confirm'),
					no : this.localize('cancel')
				},
				scope : this,
				fn : function(btn){
					if(btn == 'yes'){						
						this.clearSample();
						this.set('sample', data);
						this.parentVM.updateDefaultTestSample(data);	
						this.constructXMLDom(data == "" ? true : false);			
					}
				}
			})
		}
		else
		{
			this.set('sample', data);
			this.parentVM.updateDefaultTestSample(data);	
			this.constructXMLDom(data == "" ? true : false);		
		}
	},
	getParserConfig : function(){
		var variables = [];
		this.variableStore.each(function(r) {
			variables.push(Ext.apply(r.data, {				
				xpath: r.data.xpath,
				flow : r.data.containerType == "FLOW",
				wsdata : r.data.containerType == "WSDATA",
				output : r.data.containerType == "OUTPUT"
			}));
		}, this);
		if(variables.length != 0){
			var	parserConfig = JSON.stringify({ 
				type : 'xml',
				sample : this.sample,
				variables : variables,
				generatedFromATB : true
			});
			return parserConfig;
		}
		else
			return null;		
	},	
	getSample : function(){
		return this.sample;
	},
	getCode : function(){
		return this.codeScreen.getCode();
	},
	getParameterList : function(){
		var variableArray = [];
		this.variableStore.each(function(entry){
			variableArray.push({
				variableId : entry.get('id'),
				name : entry.get('variable'),
				containerType : entry.get('containerType')
			})
		})
		return variableArray;		
	},
	isAutoGenCode : function(){
		return this.codeScreen.isAutoGenCode();
	},
	isParserReady : function(){
		return this.parserReady;
	},
	resetParser : function(){
		this.clearPath();	
	},
	notifyDirty : function(){
		if(!this.loadingData)
			this.parentVM.notifyDirty();
	},
	notifyDataChange : function(){
		this.parentVM.notifyDataChange();
	},
	xmlHintText$ : function(){
		return this.sample == "" ? "" : this.localize("xmlHintText");
	},
	//VIEW EVENT HANDLERS
	clearSample : function(){
		this.updateSample("");	
		this.clearPath();
		this.variableStore.removeAll();
		this.newVariableCounter = 1;
	},	
	capturePath : function(captureInfo){
		var variableName = "NewVariable" + this.newVariableCounter;
		while(this.variableStore.findExact("variable",variableName) != -1){
			this.newVariableCounter++;
			variableName = "NewVariable" + this.newVariableCounter;
		} 
		var existingVariableName = this.variableStore.collect('variable');
		var newMarkup = {
			id : Ext.data.IdGenerator.get('uuid').generate(),
			variable : variableName,		
			xpath : this.xpath
		}
		this.open({
			mtype : 'MarkupCapture',
			newMarkup : newMarkup,
			existingVariableName : existingVariableName,
			parserType : 'xml',
			dumper : {
				dump : this.processCapturePath,
				scope : this
			}
		})
	
	},
	processCapturePath : function(newMarkup){
		//Add to variable grid
		this.variableStore.add(newMarkup);
		//this.resetParser();
	},
	removeVariable : function(record){
		if(!this.isViewOnly){
			var removedVariableId = record.raw.id;
			var variableStoreIndex = this.variableStore.indexOfId(removedVariableId);
			this.variableStore.removeAt(variableStoreIndex);	
		}
	},	
	clearPath : function(){
		this.set('augmentedXPath', null);
		this.set('xpath', null);
		this.set('textXPath', '');
		this.xPathDetail.set('noProperty',true);
	},

	//VIEW COMPONENT VISIBILITY	
	clearSampleIsEnabled$ : function(){
		return this.sample != '' && this.activeScreen == 0;
	},
	clearPathIsEnabled$ : function(){
		return this.textXPath != '';
	},
	capturePathIsEnabled$ : function(){
		return this.textXPath != '';
	},	
	variableStoreIsEmpty$ : function(){
		return this.variableStore.getCount() == 0;
	},
	showCodeIsVisible$: function(){		
		return this.activeScreen == 0;
	},
	showMarkupIsVisible$: function(){
		return this.activeScreen == 1;
	},
	controlBorderRegion$ : function(){
		return this.smallResolution ? 'south' : 'east';
	},
	controlHeight$ : function(){
		return this.smallResolution ? 900 : 450;
	},
	controlMargin$ : function(){
		return this.smallResolution ? '10 0 0 0' : '0 0 0 10';
	},

	//XPATH LOGIC
	constructXMLDom: function(forClear) {		
		var clean = this.sample.replace(/\r*\n/g, '\n');
		//Clean up all declaration if any.
		clean = clean.replace(/<\?xml .*\?>/g,'');
		var doc = null;
		var err = false;
		var domParser = new DOMParser();		
		doc = domParser.parseFromString('<resolveparserroot>' + clean + '</resolveparserroot>', 'text/xml');
		this.set('validXml', doc.getElementsByTagName('parsererror').length == 0);
		this.set('currentSampleDom', doc);		

		if (!this.validXml) {
			clientVM.displayError(this.localize('invalidXmlSyntaxError'))
			doc = domParser.parseFromString('<error>' + Ext.htmlEncode(clean) + '</error>', 'text/xml');			
			this.set('currentSampleDom', doc)
			this.fireEvent('renderSample');
			return;
		}

		//Only handle 1 XML structure at this time.
		var xmlCount = 0;
		Ext.each(doc.childNodes[0].childNodes, function(node) {
			if (node.nodeType == 1) {
				xmlCount++;
				if (xmlCount > 1)
					node.ignore = true;
			}
		});
		this.set('validXml', xmlCount > 0);
		if (!this.validXml) {
			if(!forClear)
				clientVM.displayError(this.localize('noXmlSegFoundError'));
			doc = domParser.parseFromString('<error>' + clean + '</error>', 'text/xml');			
			this.set('currentSampleDom', doc)
			this.fireEvent('renderSample', forClear);
			return;
		}
		this.fireEvent('renderSample');
	},	
	selectXPath: function(type, containerElementPnid, rendererNode) {		
		var element = Ext.fly(this.currentSampleDom).query('*[pnid="' + containerElementPnid + '"]')[0];
		var current = element;
		var nodes = [current];
		while (current.parentNode && current.parentNode.tagName.toLowerCase() != 'resolveparserroot') {
			current = current.parentNode;
			if (current.nodeType != 1)
				continue;
			nodes.push(current);
		}
		nodes.reverse();
		var tokens = [];
		var augmentedXPath = [];
		var predicates = [];
		Ext.each(nodes, function(node) {
			tokens.push(node.tagName)
			augmentedXPath.push({
				type: 'element',
				element: node.tagName,
				ptid: Ext.data.IdGenerator.get('uuid').generate(),
				pnid: node.pnid,
				op: 'and',
				predicates: []
			});
		});
		//Set the last node as leaf.
		augmentedXPath[augmentedXPath.length - 1]['isLeaf'] = true;
		var xpath = '//' + tokens.join('/');	
		if (type == 'text' || type == 'cdata') {
			augmentedXPath[augmentedXPath.length - 1].predicates = [{
				type: 'text',
				pnid: augmentedXPath[augmentedXPath.length - 1].pnid,
				ptid: Ext.data.IdGenerator.get('uuid').generate(),
				condition: 'contains',
				originValue: element.textContent,
				value: Ext.String.escape(element.textContent).replace(/\n/g, '\\n').replace(/"/g, '\\"')
			}];
			augmentedXPath.push({
				type: 'selector',
				ptid: Ext.data.IdGenerator.get('uuid').generate(),
				value: type + "()"
			});
		} else if (type == 'attribute') {
			var trimmer = /^\s*(.+)\s*=\s*"(.*)"\s*$/
			var groups = trimmer.exec(rendererNode.textContent);
			var name = groups[1];
			var value = groups[2];
			var isNumberTest = /^[0-9]+([.][0-9]+)?$/
			var isNumber = isNumberTest.test(value);
			augmentedXPath[augmentedXPath.length - 1].predicates = [{
				type: 'attribute',
				pnid: augmentedXPath[augmentedXPath.length - 1].pnid,
				ptid: Ext.data.IdGenerator.get('uuid').generate(),
				name: name,
				condition: '=',
				isNumber: isNumber,
				value: value
			}];
			augmentedXPath.push({
				type: 'selector',			
				ptid: Ext.data.IdGenerator.get('uuid').generate(),
				value: '@' + name
			});
		}

		this.set('augmentedXPath', augmentedXPath);
		this.set('textXPath', this.xpath);
		this.xPathDetail.set('noProperty', true);
	},
	xpath$: function() {
		var tokens = [];
		Ext.each(this.augmentedXPath, function(augmentedToken) {
			tokens.push(this.serializeXPathToken(augmentedToken));
		}, this);
		return '//' + tokens.join('\/');
	},	
	serializeXPathToken: function(augmentedToken) {
		if (augmentedToken.type == 'selector')
			return this.serializeSelector(augmentedToken);

		var predicates = [];
		Ext.each(augmentedToken.predicates, function(p) {
			if (p.type == 'text')
				predicates.push(this.serializeTextPredicate(p));
			else if (p.type == 'attribute')
				predicates.push(this.serializeAttributePredicate(p));
			else if (p.type == 'custom')
				predicates.push(this.serializeCustomPredicate(p));
		}, this);
		var token = augmentedToken.element;
		if (predicates.length > 0)
			token += '[' + predicates.join(' ' + augmentedToken.op + ' ') + ']'
		return token;
	},
	serializeSelector: function(token) {
		return token.value;
	},
	serializeTextPredicate: function(predicate) {
		if (['contains', 'starts-with', 'ends-with'].indexOf(predicate.condition) != -1)
			return Ext.String.format('{0}(., \'{1}\')', predicate.condition, predicate.value);;
		return '';
	},
	serializeAttributePredicate: function(predicate) {
		if (['contains', 'starts-with', 'ends-with'].indexOf(predicate.condition) != -1)
			return Ext.String.format('{0}(@{1}, \'{2}\')', predicate.condition,predicate.name, predicate.value);
		return Ext.String.format('@{0} {1} {2}', predicate.name, predicate.condition, predicate.isNumber ? predicate.value : '"' + predicate.value + '"');
	},	
	serializeCustomPredicate: function(predicate) {		
		return predicate.value;
	},	
	showDetail: function(pnid, ptid, wndPosAdjustFn) {		
		var targetToken = null;
		var selectorToken = null;
		var leafNode = null;
		Ext.each(this.augmentedXPath, function(token) {
			if (pnid == token.pnid) {
				targetToken = token;				
			}
			if(token.type == "selector")
				selectorToken = token;
			if(token.isLeaf)
				leafNode = token;
		}, this);		
		//Select Leaf node instead selector node.
		targetToken = targetToken.type == "selector" ? leafNode : targetToken;	
		var detailConfig = {		
			augmentedXPathToken:  targetToken,
			currentSampleDom: this.currentSampleDom,		
			isLeaf : targetToken.isLeaf
		}
		if(targetToken.isLeaf){
			detailConfig['selectorToken'] = selectorToken;
		}
		this.xPathDetail.updateXPath(detailConfig);		
	},
	removeToken : function(pnid, ptid){
		var newPath = [];
		var isLeaf = false;
		Ext.each(this.augmentedXPath, function(token) {
			if (token.ptid == ptid){
				isLeaf = token.isLeaf;
				return;
			}
			newPath.push(token);
			if (ptid == 'predicates') {
				token.predicates = [];
				return;
			}
			if (token.predicates)
				token.predicates = Ext.Array.filter(token.predicates, function(p) {
					return p.ptid != ptid;
				});
		}, this);
		if(isLeaf)
			newPath.splice(newPath.length - 1,1);
		var decodedPath = '';

		try {
			decodedPath = Ext.decode(Ext.encode(newPath));
		} catch (ex) {
			decodedPath = 'Decode Failure (Figure out this bug and fix it)';
		}

		this.set('augmentedXPath', decodedPath);
		this.set('textXPath', this.xpath);
	}
});
glu.defModel('RS.actiontaskbuilder.XPathTokenEditor', {	
	currentPath: '',
	selector : '',
	selectorToken : null,
	selectorIsVisible : false,	
	currentSampleDom: null,
	attributeStore: {
		mtype: 'store',
		fields: ['name']
	},
	attrConditionStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	textConditionStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	predicatesSelections: [],
	augmentedXPathToken: null,
	loading: false,
	and: true,	
	//allValid: true,
	predicates: {
		mtype: 'list',
	},
	nodeName : '',
	noProperty : true,	
	title$ : function(){
		return !this.noProperty ? this.nodeName + "'s Property" : this.localize('xpathProperty');
	},	
	noPredicate$ :function(){
		return  this.predicates.getCount() == 0;
	},
	init: function() {
		this.set('title', this.localize('xpathProperty'));
		this.textConditionStore.add([{
			name: this.localize('startsWith'),
			value: 'starts-with'
		}, {
			name: this.localize('endsWith'),
			value: 'ends-with'
		}, {
			name: this.localize('contains'),
			value: 'contains'
		}]);

		this.attrConditionStore.add([{
			name: '=',
			value: '='
		}, {
			name: '!=',
			value: '!='
		}, {
			name: '>=',
			value: '>='
		}, {
			name: '<=',
			value: '<='
		}, {
			name: this.localize('startsWith'),
			value: 'starts-with'
		}, {
			name: this.localize('endsWith'),
			value: 'ends-with'
		}, {
			name: this.localize('contains'),
			value: 'contains'
		}])
		this.predicates.on('lengthChanged', function(){
			this.fireEvent('predicatesChanged');
		},this);

	},	
	resetProperty : function (){
		this.predicates.removeAll();
		this.set('selectorToken', null);
		this.set('selector', null);
	},
	updateXPath : function(xpathDetail){
		this.resetProperty();		
		this.set('augmentedXPathToken', xpathDetail.augmentedXPathToken);
		this.set('currentSampleDom', xpathDetail.currentSampleDom);
		this.set('selectorToken', xpathDetail.selectorToken);
		var attributeCustomName = [];
		Ext.each(this.augmentedXPathToken.predicates, function(p) {
			//Copy config object so we can preserve original data otherwise glu will overwrite this object when using glu.model method...
			var predicateConfig = Ext.apply({},p);
			if (predicateConfig.type == 'attribute'){
				this.doAddPredicate('RS.actiontaskbuilder.XPathAttrPredicate', predicateConfig);
				attributeCustomName.push(predicateConfig.name);
			}
			else if (p.type == 'text')
				this.doAddPredicate('RS.actiontaskbuilder.XPathTextPredicate', predicateConfig);
			else
				this.doAddPredicate('RS.actiontaskbuilder.XPathCustomizedPredicate', predicateConfig);
		}, this)
		this.set('and', this.augmentedXPathToken.op == 'and');
		this.set('selectorIsVisible', this.augmentedXPathToken.isLeaf);
		if(this.augmentedXPathToken.isLeaf && this.selectorToken){
			this.set('selector', this.selectorToken.value);
		}	

		var nodes = Ext.fly(this.currentSampleDom).query(this.augmentedXPathToken.element)
		var attrs = [];
		this.attributeStore.removeAll();
		Ext.each(nodes, function(node) {
			for (var i = 0; i < node.attributes.length; i++) {
				var attr = node.attributes[i];
				if (this.attributeStore.findExact('name', attr.name) != -1)
					return;
				if(attr.name != 'pnid'){					
					this.attributeStore.add({
						name: attr.name
					});
				}				
			}
		}, this);
		for(var i = 0; i < attributeCustomName.length; i++){
			if (this.attributeStore.findExact('name', attributeCustomName[i]) == -1){
				this.attributeStore.add({
					name: attributeCustomName[i]
				});
			}
		}
		this.set('nodeName', this.augmentedXPathToken.element);
		this.set('noProperty', false);		
	},	
	addTextPredicate: function() {
		this.doAddPredicate('RS.actiontaskbuilder.XPathTextPredicate', {
			pnid: this.augmentedXPathToken.pnid,
			ptid: Ext.data.IdGenerator.get('uuid').generate(),
			condition: 'contains',
			text: 'NewText'
		});
	},
	addAttrPredicate: function() {
		this.doAddPredicate('RS.actiontaskbuilder.XPathAttrPredicate', {
			pnid: this.augmentedXPathToken.pnid,
			ptid: Ext.data.IdGenerator.get('uuid').generate(),
			name: 'NewAttribute',
			condition: '=',
			value: 'Value'
		});
	},
	addCustPredicate: function() {
		this.doAddPredicate('RS.actiontaskbuilder.XPathCustomizedPredicate', {
			pnid: this.augmentedXPathToken.pnid,
			ptid: Ext.data.IdGenerator.get('uuid').generate(),
			predicate: 'NewPredicate'
		});
	},
	doAddPredicate: function(modelName, predicate) {
		this.predicates.add(this.model(modelName, predicate));	
	},
	doRemovePredicate: function(predicateVM) {
		this.predicates.remove(predicateVM);	
	},
	doUpdate: function() {
		var newPredicates = [];
		this.predicates.forEach(function(p) {
			newPredicates.push(p.getData());
		});
		Ext.each(this.parentVM.augmentedXPath, function(token) {
			if (token.ptid == this.augmentedXPathToken.ptid){
				token.predicates = newPredicates;
				token.op = this.and ? 'and' : 'or';
			}
			if(token.type == "selector")
				token.value = this.selector;
		}, this);
		//Add selector token if there is no selector token in current Path
		if(this.selectorToken == null){
			this.parentVM.augmentedXPath.push({
				type: 'selector',			
				ptid: Ext.data.IdGenerator.get('uuid').generate(),
				value: this.selector
			})
		}

		var decodedPath = '';

		try {
			decodedPath = Ext.decode(Ext.encode(this.parentVM.augmentedXPath));
		} catch (ex) {
			decodedPath = 'Decode Failure (Figure out this bug and fix it)';
		}		

		this.parentVM.set('augmentedXPath', decodedPath);
		this.parentVM.set('textXPath', this.parentVM.xpath);		
	},
	removeNode: function() {
		var pnid = this.augmentedXPathToken.pnid;
		var ptid = this.augmentedXPathToken.ptid;
		this.parentVM.removeToken(pnid,ptid);
		this.set('noProperty', true);	
	},
	update: function() {
		this.doUpdate();		
	},
	/*updateIsEnabled$: function() {
		return true; //TODO
	}	*/
});
glu.defModel('RS.actiontaskbuilder.XPathPredicate', {	
	remove: function() {
		this.parentVM.doRemovePredicate(this);
	},
	getData: function() {
		return this.asObject();
	}
});

glu.defModel('RS.actiontaskbuilder.XPathTextPredicate', {
	mixins: ['XPathPredicate'],
	type: 'text',
	condition: 'contains',
	fields: ['pnid', 'ptid', 'condition', 'value', 'type'],
	/*valueIsValid$: function() {
		return Ext.String.trim(this.value) ? true : this.localize('invalidValue');
	}*/
});

glu.defModel('RS.actiontaskbuilder.XPathAttrPredicate', {
	mixins: ['XPathPredicate'],
	type: 'attribute',
	fields: ['pnid', 'ptid', 'name', 'condition', 'value', 'type'],
	/*nameIsValid$: function() {
		return Ext.String.trim(this.name) ? true : this.localize('invalidName');
	},
	valueIsValid$: function() {
		return Ext.String.trim(this.value) ? true : this.localize('invalidValue');
	}*/
});

glu.defModel('RS.actiontaskbuilder.XPathCustomizedPredicate', {
	mixins: ['XPathPredicate'],
	type : 'custom',
	fields: ['pnid', 'ptid', 'type','value'],
/*	predicateIsValid$: function() {
		return Ext.String.trim(this.value) ? true : this.localize('invalidPredicate');
	}*/
});
Ext.define('RS.actiontaskbuilder.ParserEditBlockModel', {
	extend: 'RS.common.blocks.BlockModel',
	modelConfig: {		
		mixins: ['QuickAccessHelpers'],
		activeItem: 0,
		suspendBroadcastDataChange : true,
		blockModel: null,
		blockModelClass: 'Parser',
		classList: 'parser-block block-model',
		smallResolution : false,
		collapsed: true,
		hidden: true,
		editBlock: true,
		localOriginalData: {},
        title: '',	
        header: {
            titlePosition: 0,
            defaults: {
            	margin: 0
        	},
            items: []
        },			
		parserControl : {
			mtype : 'ParserControl'
		},
		testControl : {
			mtype : 'TestControl'
		},
		parserType : 'text',
		when_parser_type_change : {
			on : ['parserTypeChanged'],
			action : function(){
				this.parserControl.updateActiveparserType(this.parserType);				
			}
		},
		when_resolution_change : {
			on : ['smallResolutionChanged'],
			action : function(){
				this.parserControl.set('smallResolution', this.smallResolution);
			}
		},
		textParserIsActive$: function(){
			return this.parserType == 'text';
		},	
		tableParserIsActive$: function(){
			return this.parserType == 'table';
		},	
		xmlParserIsActive$: function(){
			return this.parserType == 'xml';
		},
		markupAndCapture: function(){
			this.set('activeItem', 0);		
		},
		markupAndCaptureIsVisible$: function(){
			return this.activeItem == 1;
		},
		parserSwitchVisibility$ : function(){
			return this.activeItem == 0 ? 'visible' : 'hidden';
		},
		test : function(){
			this.testControl.updateActiveParserType(this.getActiveType());
			this.set('activeItem', 1);
		},		
		testIsVisible$ : function(){
			return this.activeItem == 0;
		},

		updateDefaultTestSample : function(parserType,data){
			this.testControl.updateDefaultTestSample(parserType , data);
		},		
		isAssessTask: function(flag){
			this.parserControl.isAssessTask(flag);	
		},	

		getCode : function(){
			return this.parserControl.getCode();
		},
		getParserConfig : function(){
			return this.parserControl.getParserConfig();
		},
		getActiveType : function(){
			return this.parserControl.getActiveType();
		},
		getSample : function(){
			return this.parserControl.getSample();
		},
		isAutoGenCode : function(){
			return this.parserControl.isAutoGenCode();
		},
		isParserReady : function(){
			return this.parserControl.isParserReady();
		},	

		setDefaultParser: function(){		
			this.set('parserType','text');
			this.parserControl.setDefaultParser();
			if(window.loadingNewAT)
				this.testControl.resetTest();
			this.set("activeItem",0);	
		},		
		postInit: function (blockModel) {		
			this.blockModel = blockModel;
		},			
		
		closeFlag: false,
		close: function () {					
			this.updateClasses();
			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		},
		
		notifyDirty: function(){			
			this.blockModel.notifyDirty();
			this.updateClasses();			
		},

		notifyDataChange : function(){
			if(!this.suspendBroadcastDataChange){
				var newData = this.parserControl.getParameterList();
				this.blockModel.notifyDataChange(newData);
			}
		},
		updateClasses: function () {//TODO
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.parser'];
	},
	
	modifyByNotification: function(data, resolveActionInvoc, assignedTo){
		var taskType = resolveActionInvoc.utype ? resolveActionInvoc.utype.toUpperCase(): null;
		var g = this.gluModel;
		g.isAssessTask(taskType == "ASSESS");
	},

	load: function (parserData) {
		var g = this.gluModel;
		g.set('localOriginalData', parserData);
		g.set('suspendBroadcastDataChange', true);
 		g.setDefaultParser();
		var parserType = parserData ? parserData.parserType : null;		
		
		if (parserType) {		
			g.set('parserType', parserType);
			g.parserControl.loadData(parserData);
		}		
		else{
			//Make new parser compatible with old code generared from AT/AB
			var autoGenCode = true;
			if(parserData['uscript']){
				g.parserControl.loadData(parserData);
				autoGenCode = false;
			}
			//Those default setting must match whatever return from getData method.
			g.localOriginalData['parserType'] = "text";
			g.localOriginalData['sampleOutput'] = "";
			g.localOriginalData['parserAutoGenEnabled'] = autoGenCode;	
		}
		g.set('suspendBroadcastDataChange', false);
		g.notifyDataChange();
		g.updateClasses();	
	},

	getData: function () {
		var g = this.gluModel;		
		var parserData = {};
		
		//If parser is not rendered the data is not modified. (this is for embeded mode).
		if(g.isParserReady()) {
			//Clone orignal data
			var clonedData = Ext.Object.merge({},  g.get('localOriginalData'));
			parserData = Ext.apply(clonedData, {			
				parserConfig: g.getParserConfig(),
				parserType: g.getActiveType(),
				sampleOutput: g.getSample(),
				parserAutoGenEnabled: g.isAutoGenCode(),
				uscript: g.getCode()
			});		
		} else {
			parserData = g.get('localOriginalData');	
		}

		return [parserData];
	}
});
Ext.define('RS.actiontaskbuilder.ParserReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',
	modelConfig : {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Parser',
		collapsed: true,
		classList: ['block-model'],
		title: '',
		hidden: false,
		hasNoContent : false,	
		header: {
            titlePosition: 0,
            defaults: {
            	margin: 0
        	},
            items: []
        },        
		dump : null,

		sample : null,
		markups : [],
		parserData : null,
		parserReady : false,
		parserType : 'text',
		activeWindow : 0,
		activeMarkupScreen : 0,
		colorMap:['black','#276888','#BD6161','#F3955D','#96BB35','#50C6E8'],
		variableStore : {
			mtype : 'store',
			fields : ['variable','type','column','xpath','color','containerType',],
			proxy : {
				type : 'memory'
			}
		},
		variableColumn : null,
		when_parser_ready : {
			on : ['parserReadyChanged'],
			action : function(){
				if(this.parserReady){
					this.populateData(this.parserData);
				}
			}
		},
		codeScreen : {	
			mtype : 'ParserCode',
			parserType : 'text',
			viewOnly : true
		},
		testControl : {
			mtype : 'TestControl'
		},
		
		init: function() {
			this.set('variableColumn', this.getColumnConfigForType());
			this.variableStore.on('datachanged', function(){
				this.fireEvent('variableStoreChanged');
			},this);
			this.variableStore.on('update', function(){
				this.fireEvent('variableStoreChanged');
			},this);			
		},		
		getColumnConfigForType : function(){
			var me = this;
			if(this.parserType == 'text'){
				return [{
							header: 'Name',
							dataIndex: 'variable',						
							flex: 3
						},{
							header : 'Pattern',
							dataIndex : 'type',
							align : 'center',
							width : 200,
							renderer : function(value){
								return me.localize(value);
							}									
						},{
							header: 'Color',
							dataIndex: 'color',
							align : 'center',
							renderer: function(value, meta) {
								meta.style = 'background-color:' + value;
							},
							width : 200						
						},{
							header : 'Container',
							dataIndex : 'containerType',
							align : 'center',
							width : 200						
						}]
			}
			else if (this.parserType == 'table'){
				return [{
							header: 'Name',
							dataIndex: 'variable',					
							flex: 3
						},{
							header : 'Column Number',
							dataIndex : 'column',
							align : 'center',				
							width : 200
						},{
							header: 'Color',
							dataIndex: 'color',
							align : 'center',
							renderer: function(value, meta) {
								meta.style = 'background-color:' + value;
							},
							width : 200						
						},{
							header : 'Container',
							dataIndex : 'containerType',
							align : 'center',
							width : 200						
						}]
			}
			else {
				return [{
							header: 'Name',
							dataIndex: 'variable',
							editor: 'textfield',
							flex: 3
						},{
							header : 'XPath',
							dataIndex : 'xpath',
							editor : 'textfield',
							flex: 5
						},{
							header : 'Container',
							dataIndex : 'containerType',
							align : 'center',
							width : 200						
						}]
			}
		},
		resetParser : function(){
			this.set('markups',[]);
			this.set('sample','');
			this.variableStore.removeAll();
			this.set('activeMarkupScreen', 0);
			this.set('hasNoContent', false);
			this.set('activeWindow', 0);
		},
		loadData : function(parserData){		
			this.set('parserData', parserData);
			if(this.parserReady){
				this.populateData(parserData);
			}
		},
		populateData : function(parserData){
			if(parserData != null && Object.keys(parserData).length !== 0){			
				
				//Set up sample output
				this.updateSample(parserData.sampleOutput || "");

				if(parserData.parserConfig)
				{				
					try {
						var parserConfig = JSON.parse(parserData.parserConfig);
					}catch (e) {
						clientVM.displayError(this.localize('invalidJSON'));
					}
					
					if(parserConfig){
						//Set up markups
						if(this.parserType != 'xml')						
							this.set('markups', parserConfig['markups']);

						//Variable Table
						this.populateVariableTable(parserConfig);						
					}
					this.set('activeWindow', 1);				
				}
				else if(parserData['uscript']){
					//Show Code if this only has code
					this.set('activeMarkupScreen', 1);
					this.set('activeWindow', 1);
				}
				else {
					this.set('hasNoContent', true);
					this.set('activeWindow', 0);
				}

				//Populate Code
				this.codeScreen.populateCode(parserData['parserAutoGenEnabled'], parserData['uscript']);

				//Render Markup/XML
				if(this.parserType == 'text' || this.parserType == 'table')
					this.fireEvent('renderMarkup', this.sample, this.markups);
				else 
					this.fireEvent('renderXML', this.currentSampleDom);
			}
		},
		populateVariableTable : function(parserConfig){		
			if(this.parserType == 'text')
			{
				for(var i = 0; i < parserConfig['markups'].length; i++){
					var currentMarkup = parserConfig['markups'][i];
					if(currentMarkup.flow)
						var containerType = 'FLOW';
					else if (currentMarkup.wsdata)
						containerType = 'WSDATA';
					else
						containerType = 'OUTPUT';
					if(currentMarkup.action == "capture" ){
						this.variableStore.add({								
							variable : currentMarkup.variable,
							containerType : containerType,
							type : currentMarkup.type,
							color : currentMarkup.color,						
						})
					}
				}
			}
			else if(this.parserType == 'table'){
				for(var i = 0; i < parserConfig['tblVariables'].length; i++){
					var currentVariable = parserConfig['tblVariables'][i];
					if(currentVariable.flow)
						var containerType = 'FLOW';
					else if (currentVariable.wsdata)
						containerType = 'WSDATA';
					else
						containerType = 'OUTPUT';
					this.variableStore.add({						
						variable : currentVariable.variable,
						column : currentVariable.columnNum,
						color : this.colorMap[(currentVariable.columnNum - 1) % 6],
						containerType : containerType			
					})
				}
			}
			else {
				for(var i = 0; i < parserConfig['variables'].length; i++){
					var currentVariable = parserConfig['variables'][i];
					if(currentVariable.flow)
						var containerType = 'FLOW';
					else if (currentVariable.wsdata)
						containerType = 'WSDATA';
					else
						containerType = 'OUTPUT';
					this.variableStore.add({					
						variable : currentVariable.variable,		
						xpath : currentVariable.xpath,
						containerType : containerType			
					})	
				}
			}		
		},
		updateSample : function(data){
			this.set('sample', data);
			this.testControl.updateDefaultTestSample(this.parserType, data);
			if(this.parserType == 'xml')
				this.contructXMLDom();
		},
		getCode : function(){
			return this.codeScreen.getCode();
		},
		//XML Parser
		validXml : null,
		currentSampleDom : null,
		contructXMLDom: function(forClear) {		
			var clean = this.sample.replace(/\r*\n/g, '\n');
			//Clean up all declaration if any.
			clean = clean.replace(/<\?xml .*\?>/g,'');
			var doc = null;
			var err = false;
			var domParser = new DOMParser();		
			doc = domParser.parseFromString('<resolveparserroot>' + clean + '</resolveparserroot>', 'text/xml');
			this.set('validXml', doc.getElementsByTagName('parsererror').length == 0);
			this.set('currentSampleDom', doc);		

			if (!this.validXml) {
				if (this.parentVM.activeItem == this)
					clientVM.displayError(this.localize('invalidXmlSyntaxError'))
				doc = domParser.parseFromString('<error>' + Ext.htmlEncode(clean) + '</error>', 'text/xml');			
				this.set('currentSampleDom', doc)			
				return;
			}

			//Only handle 1 XML structure at this time.
			var xmlCount = 0;
			Ext.each(doc.childNodes[0].childNodes, function(node) {
				if (node.nodeType == 1) {
					xmlCount++;
					if (xmlCount > 1)
						node.ignore = true;
				}
			});
			this.set('validXml', xmlCount > 0);
			if (!this.validXml) {
				if (!forClear && this.parentVM.activeItem == this)//TODO
					clientVM.displayError(this.localize('noXmlSegFoundError'));
				doc = domParser.parseFromString('<error>' + clean + '</error>', 'text/xml');			
				this.set('currentSampleDom', doc)		
				return;
			}	
		},

		markupAndCapture: function(){
			this.set('activeWindow', 1);		
		},
		markupAndCaptureIsVisible$: function(){
			return this.activeWindow == 2;
		},
		test : function(){
			this.testControl.updateActiveParserType(this.parserType);
			this.set('activeWindow', 2);
		},		
		testIsVisible$ : function(){
			return this.activeWindow == 1;
		},
		showCode : function(){
			this.set('activeMarkupScreen', 1);
		},
		showMarkup : function(){
			this.set('activeMarkupScreen', 0);
		},
		showCodeIsVisible$: function(){		
			return this.activeMarkupScreen == 0;
		},
		showMarkupIsVisible$: function(){
			return this.activeMarkupScreen == 1;
		},
		variableStoreIsEmpty$ : function(){		
			return this.variableStore.getCount() == 0;
		},
		parserTypeName$ : function(){
			return this.localize(this.parserType);
		}
	},
	getDataPaths: function () {
		return ['data.resolveActionInvoc.parser'];
	},
	load: function (parserData) {
		var g = this.gluModel;
		g.resetParser();
		g.set('parserType', parserData ? parserData.parserType || 'text' : 'text');
		g.fireEvent('reconfigureVariableColumn', g.getColumnConfigForType());	
		g.loadData(parserData);		
	},
})
Ext.define('RS.actiontaskbuilder.PreprocessorEditBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		hidden: true,
		collapsed: true,
		editBlock: true,
		title: '',
		header: {
            titlePosition: 0,
            defaults: {
            	margin: 0
        	},
            items: []
        },
		classList: 'block-model',
		editorClass: 'at-code-editor-length',
		editorName: '',
		content: '',
		warningMsg: '',
		warningLabel: null,
		MAX_LEN: 150000,

		minResize: 133,
		minHeight: 371,
		height: 371,

		expand: function() {
			this.validateAndDisplayWarning(this.content);
		},

		validateAndDisplayWarning: function(val) {
			var html = '',
				charsRemaining = this.MAX_LEN - val.length,
				messageParts = [];

			if (val.length > this.MAX_LEN) {
				messageParts = [this.editorName, val.length, this.MAX_LEN, charsRemaining * -1];
				html = this.localize('editorExceedWarningMsg', messageParts);
				this.set('editorClass', 'at-code-editor-max-length-exceeded');
			} else {
				messageParts = [charsRemaining, val.length, this.MAX_LEN];
				html = this.localize('editorLengthMsg', messageParts);
				this.set('editorClass', 'at-code-editor-length');
			}

			if (this.warningLabel) {
				this.warningLabel.getEl().setHTML(html);
			} else {
				this.set('warningMsg', html);
			}
		},

		contentChange: function() {
			if (!this.task) {
				this.task = new Ext.util.DelayedTask();
			}

			this.task.delay(500, this.saveContent, this, arguments);
		},

		saveContent: function(content) {
			var val = content.getValue();
			this.validateAndDisplayWarning(val);
			this.set('content', val);
			this.notifyDirty();
		},

		notifyDirty: function() {
			this.blockModel.notifyDirty();
		},

		warningMsgShowed: function(warnLabel) {
			this.set('warningLabel', warnLabel);
			this.validateAndDisplayWarning(this.content);
		},

		postInit: function (blockModel) {
			this.blockModel = blockModel;
			this.editorName = this.localize('preprocess');		
		},
		
		closeFlag: false,
		close: function () {
			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.preprocess'];
	},

    id: '',
    uname: '',

	load: function (preprocessor) {
		this.id = preprocessor.id;
		this.uname = preprocessor.uname;
		this.set('content', preprocessor.uscript || '');
	},

	getData: function () {
		return [{
			id: this.id,
			uname: this.uname,
			uscript: this.gluModel.content
		}];
	}
});

Ext.define('RS.actiontaskbuilder.PreprocessorReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Preprocessor',
		collapsed: true,
		classList: ['block-model'],
		title: '',
		hidden: false,
		header: {
            titlePosition: 0,
            defaults: {
            	margin: 0
        	},
            items: []
        },
		content: '',
		contentIsEmpty: false,

		minResize: 71,
		minHeight: 371,
		height: 371,
		
		postInit: function (blockModel) {
			this.blockModel = blockModel;
		},

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden && this.parentVM.embed) {
					var preprocessorData = this.parentVM.preprocessorEdit.getData();
					var preprocessor = preprocessorData[0];

					this.parentVM.preprocessorRead.load(preprocessor);
				}

				if (this.contentIsEmpty) {
					this.shrinkPanel();
				} else {
					this.growPanel();
				}
			}
		},

		when_collapsed_changed: {
			on: ['collapsedChanged'],
			action: function() {
				if (this.contentIsEmpty) {
					this.shrinkPanel();
				} else {
					this.growPanel();
				}
			}
		},

		when_contentIsEmpty_changed: {
			on: ['contentIsEmptyChanged'],
			action: function() {
				if (this.contentIsEmpty) {
					this.shrinkPanel();
				} else {
					this.growPanel();
				}
			}
		},

		shrinkPanel: function () {
			this.set('minHeight', 86);
			this.set('minResize', 86);
			this.set('height', 0);
			this.set('height', 'auto');
		},

		growPanel: function () {
			this.set('minHeight', 371);
			this.set('minResize', 133);
			this.set('height', 0);
			this.set('height', this.minHeight);
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.preprocess'];
	},

	load: function (preprocessor) {
		var g = this.gluModel;
		var script = preprocessor.uscript || '',
			classes = ['block-model'];

		this.set('content', script)
			.set('classList', classes);

		g.set('contentIsEmpty', !g.content || g.content.length === 0);
	}
});

glu.defModel('RS.actiontaskbuilder.QuickAccessHelpers', {

	isSectionMaxSize: false,
	ignoreDecrementFlag: false,

	showSectionCallback: function() {
		if (this.showAndExpandSectionFlag) {
			setTimeout(function() {
				this.parentVM.showAndExpandSectionCallback(this);
			}.bind(this), 200);
		}
	},

	collapseSectionCallback: function() {
		if (this.collapseAndHideSectionFlag) {
			setTimeout(function() {
				this.parentVM.collapseAndHideSectionCallback(this);
			}.bind(this), 200);
		}
	},

	toggleExpandCollapsePanel: function() {
		if (this.stopPropagation) {
			this.stopPropagation = false;
		} else if (this.closeFlag) {
			this.closeFlag = false;
			this.set('collapsed', true);
		} else {
			this.set('collapsed', !this.collapsed);
		}
	},

	toggleResizeMaxBtn: function() {
		if (!this.isSectionMaxSize) {
			if (this.collapsed) {
				this.parentVM.hideSectionMaxResizeBtn(this);
			} else {
				this.parentVM.showSectionMaxResizeBtn(this);
			}
		}
	},

	incNumSectionsOpen: function() {
		this.parentVM.incNumSectionsOpen(this);
	},

	decNumSectionsOpen: function() {
		this.parentVM.decNumSectionsOpen(this);
	},

	view: null,
	viewRenderCompleted: function(view) {
		this.set('view', view);
	}
});

Ext.define('RS.actiontaskbuilder.ReferencesReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		referencesData : [],
		blockModel: null,
		blockModelClass: 'References',
		hidden: false,
		readAndEditBlock: true,
		collapsed: true,
		classList: 'block-model',
		title: '',
		header: {
			titlePosition: 0,
			defaults: {
				margin: 0
			},
			items: []
		},
		gridIsEmpty: false,

		referencesStore: {
			mtype: 'store',
			fields: ['name', 'refInContent', 'refInMain', 'refInAbort'],
			sorters: [{
		    	property: 'name',
		    	direction: 'ASC'
			}],
			proxy: {
				type: 'memory'
			}
		},

		postInit: function (blockModel) {
			this.blockModel = blockModel;
		},		

		editReference: function(docName) {
			clientVM.handleNavigation({
				modelName: 'RS.wiki.Main',
				params: {
					name: docName
				},
				target: '_blank'
			})
		}
	},

	getDataPaths: function () {
		return ['data.referencedIn'];
	},
	getData : function() {
		return [this.gluModel.referencesData];		
	},
	load: function (referencesData) {
		referencesData = referencesData || [];
		this.callParent(arguments);
		this.gluModel.referencesStore.removeAll();

		for (var i = 0; i < referencesData.length; i++) {
			var reference = referencesData[i];

			this.gluModel.referencesStore.add({
				name: reference.name || '',
				refInContent: reference.refInContent,
				refInMain: reference.refInMain,
				refInAbort: reference.refInAbort
			});
		}
		this.gluModel.set('referencesData', referencesData);
		this.gluModel.set('gridIsEmpty', this.gluModel.referencesStore.count() === 0);
	}
});
Ext.define('RS.actiontaskbuilder.RolesEditBlockModel', (function () {

	function getAllowedRoles(accessStore, roles) {
		if (roles.constructor !== Array) {
			roles = roles.getRange();
		}

		var roleNames = [];

		for (var i = 0; i < roles.length; i++) {
			roleNames.push(roles[i].data.uname);
		}

		var assignedRoles = accessStore.getRoles();

		for (var i = 0; i < assignedRoles.length; i++) {
			var indexOfAssignedRole = roleNames.indexOf(assignedRoles[i]);

			if (indexOfAssignedRole > -1) {
				roleNames.splice(indexOfAssignedRole, 1);
			}
		}

		return roleNames;
	}

	return {
		extend: 'RS.common.blocks.BlockModel',

		modelConfig: {
			mixins: ['QuickAccessHelpers'],
			blockModel: null,
			blockModelClass: 'General',
			hidden: true,
			collapsed: true,
			editSubBlock: true,
			stopPropagation: false,
			title: '',
			header: {
		 		xtype: 'toolbar',
		    	margin: 0,
		    	padding: 0,
		    	items: []
			},
			defaultRoles: true,
			hideAdminColumn: false,
			accessStore: null,
			roleCount: 0,
			gridIsEmpty: false,
			itemsExhausted: false,
			savingNewTask: false,

			rolesStore: {
				mtype: 'store',
				fields: ['uname'],
				proxy: {
					type: 'ajax',
					url: '/resolve/service/common/roles/list',
					reader: {
						type: 'json',
						root: 'records'
					}
				}
			},
			allowedRolesStore: null,
			allowedRolesStoreForGridRow: null,
			detail: {
				mtype: 'Access'
			},

			when_defaultRoles_changed: {
				on: ['defaultRolesChanged'],
				action: function () {
					this.notifyDirty();
				}
			},

			when_section_isValid_changed: {
				on: ['sectionIsValidChanged'],
				action: function(){
					if(this.blockModel)
						this.notifyValid(this.sectionIsValid);
				}
			},
			
			sectionIsValid$: function(){
				return this.defaultRoles ||	 this.roleCount > 0;
			},

			isButtonEnabled$: function () {
				return this.sectionIsValid;
			},

			defaultRolesModified: function (value) {
				this.detail.setDisabled(value);
				
				if (value) {
					this.loadDefaultRoles(false);
				}
			},

			loadDefaultRoles: function (suspendDirtyNotification) {
				if (this.defaultRoles) {
					this.ajax({
						scope: this,
						url: '/resolve/service/actiontask/getDefaultRoles',
						method: 'post',
						success: function (response) {
							var payload = RS.common.parsePayload(response);
							
							if (payload.success) {
								this.blockModel.suspendDirtyNotification = suspendDirtyNotification;
								this.accessStore.loadData(payload.data);
								this.set('defaultRoles', true);
								var count = this.accessStore.count();
								this.set('roleCount', count);
								this.set('gridIsEmpty', count === 0);
								this.detail.setDisabled(true);

								var records = this.rolesStore.getRange();
								var roles = getAllowedRoles(this.accessStore, records);
								this.allowedRolesStore.loadData(this.allowedRolesStore.shapeData(roles));
								this.detail.setRoles(roles);
								this.blockModel.suspendDirtyNotification = false;
							} else {
								clientVM.displayError(this.localize('serverError', payload.message));
							}
						},
						failure: function (response) {
							clientVM.displayFailure(response);
						}
					});
				}
			},

			postInit: function (blockModel) {
				this.blockModel = blockModel;			
				this.set('allowedRolesStore', new RS.common.data.FlatStore());
				this.set('allowedRolesStoreForGridRow', new RS.common.data.FlatStore());
				this.rolesStore.on('beforeload', function(store, operation, opts) {
					operation.params = operation.params || {};
					Ext.apply(operation.params, {
						includePublic : "true",
					});
				}, this);

				this.rolesStore.load({
					scope: this,
					addRecords: false,
					limit: 100,
					page: 1,
					start: 0,
					callback: function (records, operation, success) {
						if (success) {
							this.allowedRolesStore.removeAll();
							var roles = getAllowedRoles(this.accessStore, records);
							this.allowedRolesStore.loadData(this.allowedRolesStore.shapeData(roles));
							this.detail.setRoles(roles);
						}
					}
				});
				
				this.set('accessStore', new RS.common.data.AccessStore({
					datachanged: function () {
						this.notifyDirty();
					}.bind(this),
					update: function (record, operation) {
						var modified = operation.modified;

						if (modified.hasOwnProperty('uname') && modified.uname !== operation.data.uname) {
							this.detail.removeRole(operation.data.uname);
							this.detail.addRole(modified.uname);
						}

						this.notifyDirty();
					}.bind(this)
				}));
				
				var hidden = true;

				if (clientVM.user.roles === 'undefined' || typeof clientVM.user.roles === null) {
					// resolve.maintenance user
					hidden = false;
				} else {
					if (clientVM.user.roles && clientVM.user.roles.constructor === Array) {
						hidden = clientVM.user.roles.indexOf('admin') === -1;	
					}
				}				

				this.set('hideAdminColumn', hidden);
				this.titleComponent = {
		        	xtype: 'component',
		        	padding: '0 0 0 15',
		        	cls: 'x-header-text x-panel-header-text-container-default',
		        	html: this.localize('generalTitle'),
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
							}, this);
						}.bind(this)
					}
		        };
				this.header.items = [this.titleComponent, {
		        	xtype: 'container',
		        	autoEl: 'ol',
		        	cls: 'breadcrumb flat',
		        	margin: 0,
		        	padding: 0,
		        	items: [{
			    		xtype: 'component',
			    		autoEl: 'li',
			    		html: this.localize('generalPropertiesEditTitle'),
						listeners: {
							afterrender: function (c) {
								c.getEl().on('click', function () {
									this.stopPropagation = true;
									this.jumpTo('GeneralEdit', this.parentVM.generalEdit.gluModel);
					            }, this);
							}.bind(this)
			    		}			    		
			    	}, {
			    		xtype: 'component',
			    		autoEl: 'li',
			    		cls: 'active',
			    		html: this.localize('rolesEditTitle'),
						listeners: {
							afterrender: function (c) {
								c.getEl().on('click', function () {
									this.stopPropagation = true;
								}, this);
							}.bind(this)
						}
			    	}, {
			    		xtype: 'component',
			    		autoEl: 'li',
			    		html: this.localize('optionsEditTitle'),
						listeners: {
							afterrender: function (c) {
								c.getEl().on('click', function () {
									this.stopPropagation = true;
									this.jumpTo('OptionsEdit', this.parentVM.optionsEdit.gluModel);
					            }, this);						
							}.bind(this)
			    		}
			        }]
		        }, '->', {
					xtype: 'button',
					tooltip: this.localize('resizeFullscreen'),
					iconCls: 'rs-block-button icon-resize-full',
					isResizeMaxBtn: true,
					margin: '0 5',
					text: '',
					style: {
						opacity: 0.75
					},
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['GENERAL']);
							}, this);
						}.bind(this)
					}
				}, {
					xtype: 'button',
					tooltip: this.localize('resizeNormalscreen'),
					iconCls: 'rs-block-button icon-resize-small',
					hidden: true,
					isResizeNormalBtn: true,
					margin: '0 20',
					text: '',
					style: {
						opacity: 0.75
					},
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.parentVM.resizeNormalSection(this);
							}, this);
						}.bind(this)
					}
				}, {
					xtype: 'button',
					iconCls: 'rs-block-button icon-chevron-sign-up',
					isCollapseBtn: true,
					margin: '0 5',
					text: '',
					style: {
						opacity: 0.75
					},
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.set('collapsed', true);
							}, this);
						}.bind(this)
					}
				}];
			},

			defaultRolesOrSavingNewTask$: function () {
				return this.defaultRoles || this.savingNewTask;
			},

			notifyDirty: function() {
				this.blockModel.notifyDirty();
			},

			notifyValid: function (valid) {
				this.blockModel.notifyValid(valid);		
			},

			jumpTo: function(nextBlockName, nextSection){
				this.parentVM.generalEditSectionExpanded = !this.collapsed;
				this.parentVM.generalEditSectionMaximized = this.isSectionMaxSize;
				this.parentVM.collapseAndHideSection(this);
				this.parentVM.showAndExpandThisSection(nextSection);
			},

			when_hidden_changed: {
				on: ['hiddenChanged'],
				action: function() {
					if (!this.hidden) {
						setTimeout(function() {
							if (this.parentVM.generalEditSectionExpanded) {
								this.parentVM.generalEditSectionExpanded = null;
								this.set('collapsed', false);
							}
							if (this.parentVM.generalEditSectionMaximized) {
								this.parentVM.generalEditSectionMaximized = null;
								this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['GENERAL']);
							}
						}.bind(this), 10);
					}
				}
			},

			closeFlag: false,
			close: function () {
				if (this.isSectionMaxSize) {
					this.parentVM.resizeNormalSection(this);
				}
				this.closeFlag = true;
				this.set('collapsed', true);
			},
	
			add: function (access) {
				this.accessStore.add(access);
				var count = this.accessStore.count();
				this.set('roleCount', count);
				this.set('gridIsEmpty', count === 0);
				this.hideAddFormOnExhaust();
			},
			
			updateAllowedRolesStore: function(context) {
				var roles = getAllowedRoles(this.accessStore, this.rolesStore);
				roles.push(context.value);
				var roleData = this.allowedRolesStoreForGridRow.shapeData(roles);
				this.allowedRolesStoreForGridRow.loadData(roleData);
			},

			addRoleToDetail: function (record) {
				var count = this.accessStore.count();
				this.set('roleCount', count);
				this.set('gridIsEmpty', count === 0);
				this.detail.addRole(record.data.uname);
				this.set('itemsExhausted', false);
			},

			hideAddFormOnExhaust: function() {
				this.set('itemsExhausted', this.accessStore.getCount() === this.rolesStore.getCount());
			},

			hideAddRoleForm$: function() {
				if (this.itemsExhausted || this.defaultRoles) {
					return true;
				} else {
					return false;
				}
			}
		},

		disableForm: function () {
			this.set('savingNewTask', true);
		},

		enableForm: function () {
			this.set('savingNewTask', false);
		},

		getDataPaths: function () {
			return ['data', 'data.accessRights'];
		},

		getData: function (dataIsForServer) {
			var g = this.gluModel;

			return [{
				uisDefaultRole: g.defaultRoles
			}, g.accessStore.getAccessRights(dataIsForServer)];
		},

		load: function (data, accessRights) {
			var g = this.gluModel;
			g.accessStore.loadData(accessRights);
			var count = g.accessStore.count();
			this.set('defaultRoles', data.uisDefaultRole)
				.set('roleCount', count)
				.set('gridIsEmpty', count === 0);
			g.detail.setDisabled(data.uisDefaultRole);
			var records = g.rolesStore.getRange();
			var roles = getAllowedRoles(g.accessStore, records);
			g.allowedRolesStore.loadData(g.allowedRolesStore.shapeData(roles));
			g.detail.setRoles(roles);
		},

		show: function () {
			this.gluModel.loadDefaultRoles(true);
			this.callParent(arguments);
			this.gluModel.hideAddFormOnExhaust();
		}
	};
})());
Ext.define('RS.actiontaskbuilder.SeveritiesAndConditionsBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'SeveritiesAndConditions',
        classList: 'block-model',
		collapsed: true,
		hidden: false,
        title: '',
        header: {
            titlePosition: 0,
            defaults: {
                margin: '0'
            },
            items: []
        },

        severityItems: [],
        conditionItems: [],
        isSeveritiesMessageHidden: true,
        isConditionsMessageHidden: true,
        isCriticalSeverityHidden: true,
        isSevereSeverityHidden: true,
        isWarningSeverityHidden: true,
        isGoodSeverityHidden: true,
        isBadConditionHidden: true,
        isGoodConditionHidden: true,

        postInit: function (blockModel) {
            this.blockModel = blockModel;
            var critical = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            critical.initContainer(3, '~~criticalTitle~~', '#c00', true);
            var severe = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            severe.initContainer(2, '~~severeTitle~~', '#f80', true);
            var warning = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            warning.initContainer(1, '~~warningTitle~~', '#f2cc11', true);
            var good = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            good.initContainer(0, '~~goodTitle~~', '#0b0', true);
            this.severityItems.push(critical);
            this.severityItems.push(severe);
            this.severityItems.push(warning);
            this.severityItems.push(good);

            var bad = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            bad.initContainer(1, '~~badTitle~~', '#c00', true);
            var good = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            good.initContainer(0, '~~goodTitle~~', '#0b0', true);
            this.conditionItems.push(bad);
            this.conditionItems.push(good);            
        },

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden && this.parentVM.embed) {
					var severitiesData = this.parentVM.severitiesEdit.getData(),
						conditionsData = this.parentVM.conditionsEdit.getData();

					var expressions = severitiesData[0];

					for (var i=0, l=conditionsData[0].length; i<l; i++) {
						var conditions = conditionsData[0][i];
						expressions.push(conditions);
					}
					
					this.parentVM.severitiesAndConditionsRead.load(expressions);
				}
			}
		}
    },

    getDataPaths: function () {
        return ['data.resolveActionInvoc.expressions'];
    },

    load: function (expressions) {
        var severityItems = this.gluModel.severityItems,
            conditionItems = this.gluModel.conditionItems;

        if (expressions) {
            var singularMap = {
                INPUTS: 'INPUT',
				OUTPUTS: 'OUTPUT',
                FLOWS: 'FLOW',
                PARAMS: 'PARAM',
                PROPERTIES: 'PROPERTY',
                WSDATA: 'WSDATA',
                CONSTANT: 'CONSTANT'
            };

            for (var i = 0; i < expressions.length; i++) {
                var e = expressions[i];
                e.leftOperandType = singularMap[e.leftOperandType] || e.leftOperandType;
                e.rightOperandType = singularMap[e.rightOperandType] || e.rightOperandType;
            }

            this.loadWithoutGaps(expressions, 'severity', severityItems, [3, 2, 1, 0]);
            this.loadWithoutGaps(expressions, 'condition', conditionItems, [1, 0]);
        }

        this.set('isSeveritiesMessageHidden', 
            severityItems[0].count() > 0 ||
            severityItems[1].count() > 0 ||
            severityItems[2].count() > 0 ||
            severityItems[3].count() > 0);

        var classes = ['block-model'];

        this.set('isConditionsMessageHidden', conditionItems[0].count() > 0 || conditionItems[1].count() > 0)
            .set('isCriticalSeverityHidden', severityItems[0].count() === 0)
            .set('isSevereSeverityHidden', severityItems[1].count() === 0)
            .set('isWarningSeverityHidden', severityItems[2].count() === 0)
            .set('isGoodSeverityHidden', severityItems[3].count() === 0)
            .set('isBadConditionHidden', conditionItems[0].count() === 0)
            .set('isGoodConditionHidden', conditionItems[1].count() === 0)
            .set('classList', classes);
    },

    loadWithoutGaps: function (expressions, expressionType, items, itemLevelMap) {
        for (var i = 0; i < items.length; i++) {
            items[i].clear();
            //Hide all level 1st then enable only the level that has expression later.
            items[i].set('isHidden', true);
        }

        var orderMap = [[], [] ,[] , []];

        // generate order plan
        for (var i = 0; i < expressions.length; i++) {            
            var e = expressions[i];

            if (e.expressionType === expressionType) {
                var itemIndex = itemLevelMap[e.resultLevel];
                orderMap[itemIndex].push(e);
            }
        }

        // eliminate gaps in order and load as we process the expressions.
        for (var i = 0; i < orderMap.length; i++) {
            var t = orderMap[i];
            t.sort(function (a, b) {
                var result = 0;

                if (a.order < b.order) {
                    result = -1;                    
                } else if (a.order > b.order) {
                    result = 1;
                }

                return result;
            });

            if (t.length > 0) {
                var reducedOrder = 0;
                var lastOrderEncountered = t[0].order;

                for (var j = 0; j < t.length; j++) {
                    var e = t[j];

                    if (e.order > lastOrderEncountered) {
                        lastOrderEncountered = e.order;
                        reducedOrder++;
                    }

                    var itemIndex = itemLevelMap[e.resultLevel];
                    e.isRemoveHidden = true;
                    e.isOrHidden = j === t.length - 1;
                    items[itemIndex].load(e, reducedOrder);
                }
            }
        }
    },

    modifyByNotification: function (parameters) {
        var severityItems = this.gluModel.severityItems,
            conditionItems = this.gluModel.conditionItems;
            severityCategoryCount = 4,
            conditionCategoryCount = 2;
        
        for (var i = 0; i < severityCategoryCount; i++) {
            severityItems[i].updateParameters(parameters);
        }

        for (var i = 0; i < conditionCategoryCount; i++) {
            conditionItems[i].updateParameters(parameters);
        }
    }    
});

Ext.define('RS.actiontaskbuilder.SeveritiesEditBlockModel', {
    extend: 'RS.common.blocks.BlockModel',

    modelConfig: {
		mixins: ['QuickAccessHelpers'],
        blockModel: null,
		blockModelClass: 'SeveritiesAndConditions',
        hidden: true,
		collapsed: true,
		editBlock: true,
		stopPropagation: false,
        title: '',
        header: {
            xtype: 'toolbar',
            margin: 0,
            padding: 0,
            items: []
        },

        items: [],
        expressionType : 'severity',
		allATProperties: [],

        postInit: function (blockModel) {
            this.blockModel = blockModel;
            var critical = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            critical.initContainer(3, '~~criticalTitle~~', '#c00');
            var severe = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            severe.initContainer(2, '~~severeTitle~~', '#f80');
            var warning = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            warning.initContainer(1, '~~warningTitle~~', '#f2cc11');
            var good = this.model('RS.actiontaskbuilder.ExpressionBlockContainer');
            good.initContainer(0, '~~goodTitle~~', '#0b0');
            this.items.push(critical);
            this.items.push(severe);
            this.items.push(warning);
            this.items.push(good);
            this.header.items = [/*{
				xtype: 'component',
				padding: '0 0 0 15',
				cls: 'x-header-text x-panel-header-text-container-default',
				html: this.localize('severitiesEditTitle')
			},*/
			{
                xtype: 'container',
                autoEl: 'ol',
                cls: 'breadcrumb flat',
                margin: 0,
                padding: 0,
                items: [{
                    xtype: 'component',
                    autoEl: 'li',                  
                    html: this.localize('severitiesEditTitle'),
                    cls : 'active',
					listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
							}, this);
						}.bind(this)
					}
                }, {
                    xtype: 'component',
                    autoEl: 'li',
                    html: this.localize('conditionsEditTitle'),
                    listeners: {
                        afterrender: function (c) {
                            c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.jumpTo('ConditionsEdit', this.parentVM.conditionsEdit.gluModel);
                            }, this);                       
                        }.bind(this)
                    }
                }]
            }, '->', {
				xtype: 'button',
				tooltip: this.localize('resizeFullscreen'),
				iconCls: 'rs-block-button icon-resize-full',
				isResizeMaxBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['SEVERITYCONDITION']);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				tooltip: this.localize('resizeNormalscreen'),
				iconCls: 'rs-block-button icon-resize-small',
				hidden: true,
				isResizeNormalBtn: true,
				margin: '0 20',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeNormalSection(this);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				iconCls: 'rs-block-button icon-chevron-sign-up',
				isCollapseBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.set('collapsed', true);
						}, this);
					}.bind(this)
				}
			}];
            if(this.rootVM.getAllATPropertiesInProgress){
                var checkingForATProperties = setInterval(function(){
                    if(!this.rootVM.getAllATPropertiesInProgress){
                        var severityCategoryCount = 4;
                        var items = this.items;
                        
                        for (var i = 0; i < severityCategoryCount; i++) {
                            items[i].updateParameters({
                                PROPERTY : this.rootVM.allATProperties
                            });
                        }                       
                        clearInterval(checkingForATProperties);
                    }
                }.bind(this),500)
            }	
        },

        jumpTo: function(nextBlockName, nextSection){
			this.parentVM.severitiesAndConditionsEditSectionExpanded = !this.collapsed;
			this.parentVM.severitiesAndConditionsEditSectionMaximized = this.isSectionMaxSize;
			this.parentVM.collapseAndHideSection(this);
			this.parentVM.showAndExpandThisSection(nextSection);
        },
        
        reclassifyExpression : function(expression){
            for(var i = 0; i < this.items.length; i++){
                if(this.items[i].category === expression.category){
                    this.items[i].updateCategoryForExpression(expression);
                }
            }
        }, 

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden) {
					setTimeout(function() {
						if (this.parentVM.severitiesAndConditionsEditSectionExpanded) {
							this.parentVM.severitiesAndConditionsEditSectionExpanded = null;
							this.set('collapsed', false);
						}
						if (this.parentVM.severitiesAndConditionsEditSectionMaximized) {
							this.parentVM.severitiesAndConditionsEditSectionMaximized = null;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['SEVERITYCONDITION']);
						}
					}.bind(this), 10);
				}
			}
		},
               
        close: function (btn) {
            this.expShowAndValid = false;
            this.traverse(this.setFormValid);

            if (this.expShowAndValid) {
                var box = this.message({
                    scope: this,
                    title: this.localize('activityNotCompleted'),
                    msg: this.localize('confirmAddSeverities'),
                    buttons: Ext.MessageBox.YESNO,
                    buttonText: {
                        yes: this.localize('confirmYes'),
                        no: this.localize('confirmNo')
                    },
                    fn: function(btn) {
                        if (btn === 'yes') {
                            this.traverse(this.applyAdd);
                        }
                        //this.blockModel.close('SeveritiesAndConditionsRead');
						this.doClose();
                    }
                });
            } else {
                //this.blockModel.close('SeveritiesAndConditionsRead');
				this.doClose();
            }
        },


		closeFlag: false,
		doClose: function () {
			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		},

        applyAdd: function(exp) {
            if (exp.form && !exp.form.isHidden() && exp.form.isValid()) {
                var btn = exp.form.down('#doneBtn');
                btn? btn.getEl().dom.click(): null;
            }
        },

        setFormValid: function(exp) {
            if (exp.form && !exp.form.isHidden() && exp.form.isValid()) {
                this.expShowAndValid = true;
            }
        },

        traverseAndApply: function(blk, fn, blkName, args) {
            if (blkName) {
                var items = blk[blkName];
                for (var i=0; i<items.length; i++) {
                    args? fn.apply(this, [items.getAt(i)].concat(args)): fn.apply(this, [items.getAt(i)].concat(fn));
                }
            } else {
                fn.apply(this, blk);
            }
        },

        traverse: function(applyFn) {
            for (var i=0; i<this.items.length; i++) {
                this.traverseAndApply(this.items[i], this.traverseAndApply, 'blocks', [applyFn, 'expressions']);
            }
        }		
    },

    getDataPaths: function () {
        return [{
            filter: function (expression) { return expression.expressionType === 'severity'; },
            path:'data.resolveActionInvoc.expressions'
        }];
    },

    getData: function () {
        var severities = [],
            items = this.gluModel.items,
            pluralMap = {
                INPUT: 'INPUTS',
                OUTPUT: 'OUTPUTS',
                FLOW: 'FLOWS',
                PARAM: 'PARAMS',
                PROPERTY: 'PROPERTIES',
                WSDATA: 'WSDATA',
                CONSTANT: 'CONSTANT'
            };

        for (var i = 0; i < items.length; i++) {
            var serveritiesGroup = items[i],
                data = serveritiesGroup.getData(this.gluModel.expressionType);
            
            for (var j = 0; j < data.length; j++) {
                var e = data[j];
                e.leftOperandType = pluralMap[e.leftOperandType] || e.leftOperandType;
                e.rightOperandType = pluralMap[e.rightOperandType] || e.rightOperandType;
            }

            severities.push.apply(severities, data);
        }

        return [severities];
    },

    load: function (expressions) {
        var g = this.gluModel,
			items = g.items,
            singularMap = {
            INPUTS: 'INPUT',
            OUTPUTS: 'OUTPUT',
            FLOWS: 'FLOW',
            PARAMS: 'PARAM',
            PROPERTIES: 'PROPERTY',
            WSDATA: 'WSDATA',
            CONSTANT: 'CONSTANT'
        };

        for (var i = 0; i < items.length; i++) {
            items[i].clear();
        }

        var itemLevelMap = [3, 2, 1, 0],
            orderMap = [[], [] ,[] , []];            

        // generate order plan
        for (var i = 0; i < expressions.length; i++) {
            var e = expressions[i],
                itemIndex = itemLevelMap[e.resultLevel];

            e.leftOperandType = singularMap[e.leftOperandType] || e.leftOperandType;
            e.rightOperandType = singularMap[e.rightOperandType] || e.rightOperandType;               
            orderMap[itemIndex].push(e);
        }

        // eliminate gaps in order and load as we process the expressions.
        for (var i = 0; i < orderMap.length; i++) {
            var t = orderMap[i];
            t.sort(function (a, b) {
                var result = 0;

                if (a.order < b.order) {
                    result = -1;
                } else if (a.order > b.order) {
                    result = 1;
                }

                return result;
            });

            if (t.length > 0) {
                var reducedOrder = 0;
                var lastOrderEncountered = t[0].order;

                for (var j = 0; j < t.length; j++) {
                    var e = t[j];

                    if (e.order > lastOrderEncountered) {
                        lastOrderEncountered = e.order;
                        reducedOrder++;
                    }

                    var itemIndex = itemLevelMap[e.resultLevel];
                    e.isRemoveHidden = false;
                    e.isOrHidden = false;
                    items[itemIndex].load(e, reducedOrder);
                }
            }                
        }

        for (var i = 0; i < items.length; i++) {
            items[i].enableAdd();
        }		
    },

    processDataChange : function(data){
        var severityCategoryCount = 4;
        var items = this.gluModel.items;
        
        for (var i = 0; i < severityCategoryCount; i++) {
            items[i].updateParameters(data);
        }     
    }   
});

Ext.define('RS.actiontaskbuilder.SummaryAndDetailsReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'SummaryAndDetails',
		classList: 'block-model',
		collapsed: true,
		hidden: false,
        title: '',
        summaryRuleCount : 0,
		detailRuleCount : 0,
        header: {
            titlePosition: 0,
            defaults: {
                margin: 0
            },	            
            items: []
        },		
        summaryRuleStore : {
        	mtype : 'store',
        	fields : ['severity','condition','display']
        },
        detailRuleStore : {
        	mtype : 'store',
        	fields : ['severity','condition','display']
        },

        when_rulestore_change : {
			on : ['ruleStoreChanged'],
			action : function(){
				this.set('summaryRuleCount', this.summaryRuleStore.getCount());	
				this.set('detailRuleCount', this.detailRuleStore.getCount());			
			}
		},

		postInit: function (blockModel) {
			this.blockModel = blockModel;	
			this.summaryRuleStore.on('datachanged', function(){
				this.fireEvent('ruleStoreChanged');
			}.bind(this));
			this.detailRuleStore.on('datachanged', function(){
				this.fireEvent('ruleStoreChanged');
			}.bind(this));	
		},
		
		multipleSummaryRule$ : function(){
			return this.summaryRuleCount > 1;
		},
		noSummaryRule$ : function(){
			return this.summaryRuleCount == 0
		},
		multipleDetailRule$ : function(){
			return this.detailRuleCount > 1;
		},
		noDetailRule$ : function(){
			return this.detailRuleCount == 0
		},
		
		when_hidden_changed: { 
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden && this.parentVM.embed) {
					var summaryData = this.parentVM.summaryEdit.getData(),
						detailsData = this.parentVM.detailsEdit.getData();

					var assess = {
						summaryRule: summaryData[0].summaryRule,
						detailRule: detailsData[0].detailRule
					}

					this.parentVM.summaryAndDetailsRead.load(assess);
				}
			}
		}
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.assess'];
	},

	load: function (assess) {
		//Temporary code for now until API is ready
		this.gluModel.summaryRuleStore.removeAll();
		if(assess.summaryRule){
			var data = JSON.parse(assess.summaryRule);
			var summaryRules = Array.isArray(data) ? data : [];
			this.gluModel.summaryRuleStore.loadData(summaryRules);
		}
		this.gluModel.detailRuleStore.removeAll();
		if(assess.detailRule){
			var data = JSON.parse(assess.detailRule);
			var detailRules = Array.isArray(data) ? data : [];
			this.gluModel.detailRuleStore.loadData(detailRules);
		}
	}
});

glu.defModel('RS.actiontaskbuilder.SummaryAndDetailUtil',{		
	isFieldNameEditable: true,		
	htmlEncode : false,
	preview : '',			
	ruleCount : 0,	
	typesStore: {
		mtype : 'store',
		fields : ['text','value'],
		data : [{
			text: 'OUTPUT',
			value: 'OUTPUTS'
		}, {
			text: 'INPUT',
			value: 'INPUTS'
		}, {
			text: 'FLOW',
			value: 'FLOWS'
		}, {
			text: 'PARAM',
			value: 'PARAMS'
		}, {
			text:  'WSDATA',
			value: 'WSDATA'
		}]
	},
 	allParameters : {
		INPUT : [],
		OUTPUT : [],
		FLOW : [],
		WSDATA : []
	},
	defaultData : {
		condition : 'ANY',
		severity : 'ANY',
		text : '',
		fieldName : '',
		fieldType : ''
	},		
	parametersStore: {
		mtype : ['store'],
		fields : ['name']
	},			
	ruleStore : {
		mtype : 'store',
		fields : ['condition','severity','display']
	},
	conditionStore : {
		mtype : 'store',
		fields : ['name'],
		data  : [{
			name : 'ANY',
		},{
			name : 'BAD'
		},{
			name : 'GOOD'
		}]
	},
	severityStore : {
		mtype : 'store',
		fields : ['name'],
		data  : [{
			name : 'ANY',
		},{
			name : 'CRITICAL'
		},{
			name : 'SEVERE'
		},{
			name : 'WARNING'
		},{
			name : 'GOOD'
		}]
	},

	when_rulestore_change : {
		on : ['ruleStoreChanged'],
		action : function(){
			this.set('ruleCount', this.ruleStore.getCount());
			this.notifyDirty();
		}
	},
	render_preview : {
		on : ['htmlEncodeChanged','displayChanged'],
		action : function(){
			this.renderPreview();
		}
	},
	when_field_type_changed : {
		on : ['fieldTypeChanged'],
		action : function(){
			var currentType = this.fieldType;
			this.set('fieldName', '');	
			this.set('fieldNameIsPristine', true);		

			if(currentType == 'OUTPUTS' || currentType == 'INPUTS')				
				this.set('isFieldNameEditable', false);		
			else if(currentType == 'WSDATA' || currentType == 'FLOWS' || currentType == 'PARAMS')				
				this.set('isFieldNameEditable', true);
							
			this.loadParameterStore(this.parametersStore, currentType);				
		}
	},
	conditionIsValid$ : function(){
		return this.condition != '' ? true : this.localize('requiredField');
	},
	severityIsValid$ : function(){
		return this.severity != '' ? true : this.localize('requiredField');
	},
	displayIsValid$ : function(){
		return this.display != '' ? true : this.localize('requiredField');
	},
	multipleRule$ : function(){
		return this.ruleCount > 1;
	},
	noRule$ : function(){
		return this.ruleCount == 0
	},
	insertVariableIsEnabled$ : function(){
		return this.fieldType && this.fieldName;
	},

	renderPreview : function(){
		var preview = this.htmlEncode ? Ext.String.htmlEncode(this.display) : this.display;
		this.fireEvent('renderPreview', preview.replace(/\n/g,'<br >'));
	},
	addRule : function(){
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		this.ruleStore.add({
			condition : this.condition,
			severity : this.severity,
			display : this.display
		})
		this.loadData(this.defaultData);
		this.set('isPristine',true);
	},
	removeRule : function(record){
		this.ruleStore.remove(record);
	},			
	loadParameterStore : function(store, dataType){
		if(dataType == 'INPUTS')
			store.loadData(this.allParameters['INPUT']);
		else if(dataType == 'OUTPUTS')
			store.loadData(this.allParameters['OUTPUT']);
		else if(dataType == 'FLOWS')
			store.loadData(this.allParameters['FLOW']);
		else if(dataType == 'WSDATA')
			store.loadData(this.allParameters['WSDATA']);
		else
			store.removeAll();
	},
	insertVariable: function () {			
		this.set('display', this.display + ' ${' + this.fieldType + '.' + this.fieldName + '}');				
	}
})
Ext.define('RS.actiontaskbuilder.SummaryEditBlockModel',  {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers','SummaryAndDetailUtil'],
		blockModel: null,
		blockModelClass: 'SummaryAndDetails',
		hidden: true,
		collapsed: true,
		editBlock: true,
		stopPropagation: false,
		closeFlag: false,
		title: '',
		header: {
			xtype: 'toolbar',
			margin: 0,
			padding: 0,
			items: []
		},
		fields : ['fieldType','fieldName','condition','severity','display'],
		
		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden) {
					setTimeout(function() {
						if (this.parentVM.summaryAndDetailsEditSectionExpanded) {
							this.parentVM.summaryAndDetailsEditSectionExpanded = null;
							this.set('collapsed', false);
						}
						if (this.parentVM.summaryAndDetailsEditSectionMaximized) {
							this.parentVM.summaryAndDetailsEditSectionMaximized = null;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['SUMMARYDETAILS']);
						}
					}.bind(this), 10);
				}
			}
		},

		postInit: function (blockModel) {
			this.blockModel = blockModel;
				
			this.header.items = [/*{
				xtype: 'component',
				padding: '0 0 0 15',
				cls: 'x-header-text x-panel-header-text-container-default',
				html: this.localize('summaryAndDetailsTitle')
			},*/
			{
	        	xtype: 'container',
	        	autoEl: 'ol',
	        	cls: 'breadcrumb flat',
	        	margin: 0,
	        	padding: 0,
	        	items: [{
		    		xtype: 'component',
		    		autoEl: 'li',
		    		html: this.localize('summaryEditTitle'),
		    		cls: 'active'
		    	},{
		    		xtype: 'component',
		    		autoEl: 'li',
					html: this.localize('detailsEditTitle'),
	    			listeners: {
						afterrender: function (c) {
							c.getEl().on('click', function () {
								this.stopPropagation = true;
								this.jumpTo('DetailsEdit', this.parentVM.detailsEdit.gluModel);
				            }, this);
						}.bind(this)
		    		}
		    	}]
	        }, '->', {
				xtype: 'button',
				tooltip: this.localize('resizeFullscreen'),
				iconCls: 'rs-block-button icon-resize-full',
				isResizeMaxBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeMaxSection(this, this.parentVM.sectionButtonMap['SUMMARYDETAILS']);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				tooltip: this.localize('resizeNormalscreen'),
				iconCls: 'rs-block-button icon-resize-small',
				hidden: true,
				isResizeNormalBtn: true,
				margin: '0 20',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.parentVM.resizeNormalSection(this);
						}, this);
					}.bind(this)
				}
			}, {
				xtype: 'button',
				iconCls: 'rs-block-button icon-chevron-sign-up',
				isCollapseBtn: true,
				margin: '0 5',
				text: '',
				style: {
					opacity: 0.75
				},
				listeners: {
					afterrender: function (c) {
						c.getEl().on('click', function () {
							this.stopPropagation = true;
							this.set('collapsed', true);
						}, this);
					}.bind(this)
				}
			}];
	
			this.ruleStore.on('datachanged', function(){
				this.fireEvent('ruleStoreChanged');
			}.bind(this));
			this.ruleStore.on('update', function(){
				this.fireEvent('ruleStoreChanged');
			}.bind(this));
			this.loadData(this.defaultData);
			this.set('isPristine', true);
		},
		
		jumpTo: function(nextBlockName, nextSection){
			this.parentVM.summaryAndDetailsEditSectionExpanded = !this.collapsed;
			this.parentVM.summaryAndDetailsEditSectionMaximized = this.isSectionMaxSize;
			this.parentVM.collapseAndHideSection(this);
			this.parentVM.showAndExpandThisSection(nextSection);
		},		
		close: function () {
			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		},
		notifyDirty: function(format) {
			this.blockModel.notifyDirty();
		}			
	},

	getDataPaths: function () {
		return ['data.resolveActionInvoc.assess'];
	},

	getData: function () {
		//Temporary code for now until API is ready
		var ruleStore = this.gluModel.ruleStore;
		var summaryRules = [];
		ruleStore.each(function(rule){
			summaryRules.push({
				severity : rule.get('severity'),
				condition : rule.get('condition'),
				display : rule.get('display')
			})
		})
		return [{summaryRule : JSON.stringify(summaryRules)}];
	},

	load: function (data) {
		var data = JSON.parse(data.summaryRule);
		var summaryRules = Array.isArray(data) ? data : [];
		this.gluModel.ruleStore.loadData(summaryRules);
	},		
	processDataChange : function(data){
		var g = this.gluModel;		
		for(var type in data){
			var processedData = [];
			for(var i = 0; i < data[type].length; i++){
				processedData.push({
					name : data[type][i]
				})
			}
			g.allParameters[type] = processedData;			
		}
		//Repopulate already selected type for each field.
		g.loadParameterStore(g.parametersStore, g.fieldType);
	}	
});

Ext.define('RS.actiontaskbuilder.TagsEditBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Tags',
		itemsExhausted: false,
		hidden: true,
		collapsed: true,
		editBlock: true,
		title: '',
		header: {
			titlePosition: 0,
			items: []
		},
		name: null,
		description: '',
		noTagsMessage: '',
		allowedTags: null, // Possible allowed tags that can be added to the action task.
		store: null, // Current tags assigned to the action task
		allTags: [],
		gridIsEmpty: false,

		noDescription: '',
		tagDescriptions: {},

		postInit: function (blockModel) {
			this.blockModel = blockModel;
			this.noDescription = this.localize('noTagDescription');
			this.set('noTagsMessage', this.localize('tagsEmptyText'));

			this.set('allowedTags', new RS.common.data.FlatStore().accessors([
				function (o) { return o.name; },
				function (o) { return o.name; }
			]));

			this.set('store', new RS.common.data.FlatStore({
				fields: ['name', 'description']
			}));

			this.allTags = this.rootVM.allTags;
		},

		tagItemChange: function(combo, records) {
			var tagName = combo.value;
			var selection = this.grid.getSelectionModel().getSelection()[0];
			selection.set('name', tagName);
			selection.set('description', this.tagDescriptions[tagName]);
			this.generateAllowedTags();
			this.notifyDirty();
		},

		recordRemoved: function (record) {
			record.store.remove(record);
			this.set('itemsExhausted', false);
			this.generateAllowedTags();
			this.set('gridIsEmpty', this.store.count() === 0);
			this.notifyDirty();
		},
		validateDescription : function(newVal){
			if(!this.tagDescriptions.hasOwnProperty(newVal)){
				this.set('description','');
				this.set('recordAddedIsEnabled', false);
			}
			else{
				this.set('description',this.tagDescriptions[newVal]);
				this.set('recordAddedIsEnabled', true);
			}
		},
		tagSelected: function (records) {
			var rec = Array.isArray(records)? records[0]: records;
			this.set('description', this.tagDescriptions[rec.get('text')] || '');
			this.set('recordAddedIsEnabled', true);
		},
		recordAdded: function () {
			this.store.add({
				name: this.name,
				description: this.description
			});
			this.set('name', null);
			this.set('description', '');
			this.set('recordAddedIsEnabled',false);
			this.form.isValid();
			this.generateAllowedTags();
			this.hideAddFormOnExhaust();
			this.set('gridIsEmpty', this.store.count() === 0);
			this.notifyDirty();
		},
		recordAddedIsEnabled: false,
		generateAllowedTags: function (context) {
			var remaining = this.allTags.slice(),
				range = this.store.getRange();

			for (var i = 0; i < range.length; i++) {
				var tag = range[i].data;

				for (var j = 0; j < remaining.length; j++) {
					if (tag.name === remaining[j].name) {
						remaining.splice(j, 1);
						break;
					}
				}
			}

			this.allowedTags.loadData(this.allowedTags.shapeData(remaining));
			this.set('name', '');
			this.set('description', '');

			if (this.form) {
				this.form.isValid();
			}
		},

		hideAddFormOnExhaust: function() {
			this.set('itemsExhausted', this.store.getCount() === this.allTags.length);
		},

		grid: null,
		gridRenderCompleted: function (grid) {
			this.grid = grid;
		},

		form: null,
		registerForm: function (form) {
			this.form = form;
			form.isValid();
		},

		updateAllTags: function() {
			this.set('allTags', this.rootVM.allTags);
		},

		isGetAllTagsInProgress: function() {
			return this.rootVM.getAllTagsInProgress;
		},

		processAllTags: function() {
			for (var i = 0; i < this.allTags.length; i++) {
				var tag = this.allTags[i];
				this.tagDescriptions[tag.name] = tag.description;
			}

			var shapedTags = [];

			for (var i = 0; i < this.tags.length; i++) {
				var t = {
					name: '',
					description: ''
				};

				t.name = this.tags[i];
				t.description = this.tagDescriptions[this.tags[i]] || this.noDescription;
				shapedTags.push(t);
			}

			this.store.loadData(shapedTags);
			this.set('gridIsEmpty', this.store.count() === 0);
			this.generateAllowedTags();
			this.hideAddFormOnExhaust();
		},

		notifyDirty: function() {
			this.blockModel.notifyDirty();
		},

		closeFlag: false,
		close: function () {
			if (this.isSectionMaxSize) {
				this.parentVM.resizeNormalSection(this);
			}
			this.closeFlag = true;
			this.set('collapsed', true);
		}
	},

	getData: function () {
		var range = this.gluModel.store.getRange(),
			tags = [];

		for (var i = 0; i < range.length; i++) {
			tags.push(range[i].data.name);
		}

		return [tags];
	},

	getDataPaths: function () {
		return ['data.tags'];
	},

	load: function (tags) {
		this.getAllTags(tags || []);
	},

	getAllTags: function (tags) {
		var g = this.gluModel;

		g.tags = tags;
		g.updateAllTags();

		if (g.allTags.length) {
			g.processAllTags();
		}
		else if (g.isGetAllTagsInProgress()) {
			var id = setInterval(function() {
				if (!g.isGetAllTagsInProgress()) {
					clearTimeout(id); // exit
					g.updateAllTags();
					g.processAllTags();
				}
			}, 100);
		}
	}
});
Ext.define('RS.actiontaskbuilder.TagsReadBlockModel', {
	extend: 'RS.common.blocks.BlockModel',

	modelConfig: {
		mixins: ['QuickAccessHelpers'],
		blockModel: null,
		blockModelClass: 'Tags',
		hidden: false,
		collapsed: true,
		classList: 'block-model',
		title: '',
		header: {
			titlePosition: 0,
			defaults: {
				margin: 0
			},
			items: []
		},
		gridIsEmpty: false,
		store: null,
		tags: [],
		allTags: [],
		noDescription: '',

		postInit: function (blockModel) {
			this.blockModel = blockModel;
			this.noDescription = this.localize('noTagDescription');
			this.set('store', new RS.common.data.FlatStore({
				fields: ['name', 'description']
			}));

			this.allTags = this.rootVM.allTags;
		},		

		when_hidden_changed: {
			on: ['hiddenChanged'],
			action: function() {
				if (!this.hidden && this.parentVM.embed) {
					var tagsData = this.parentVM.tagsEdit.getData();
					this.parentVM.tagsRead.load(tagsData[0]);
				}
			}
		},

		updateAllTags: function() {
			this.set('allTags', this.rootVM.allTags);
		},
	
		isGetAllTagsInProgress: function() {
			return this.rootVM.getAllTagsInProgress;
		},

		processAllTags: function() {
			var tagDescriptions = {};
	
			for (var i = 0; i < this.allTags.length; i++) {
				var tag = this.allTags[i];
				tagDescriptions[tag.name] = tag.description;
			}
	
			var shapedTags = [];
	
			for (var i = 0; i < this.tags.length; i++) {
				var t = {
					name: '',
					description: ''
				};
	
				t.name = this.tags[i];
				t.description = tagDescriptions[this.tags[i]] || this.noDescription;
				shapedTags.push(t);
			}
	
			this.store.loadData(shapedTags);
			this.set('gridIsEmpty', this.store.count() === 0);
		}
	},

	getDataPaths: function () {
		return ['data.tags'];
	},

	load: function (tags) {
		this.getAllTags(tags || []);
	},

	getAllTags: function (tags) {
		var g = this.gluModel,
			classes = ['block-model'];

		g.tags = tags;

		g.set('classList', classes);

		g.updateAllTags();
		if (g.allTags.length) {
			g.processAllTags();
		} 
		else if (g.isGetAllTagsInProgress()) {
			var id = setInterval(function() {
				if (!g.isGetAllTagsInProgress()) {
					clearTimeout(id); // exit 
					g.updateAllTags();
					g.processAllTags();
				}
			}, 100);
		}
	}
});

glu.defModel('RS.actiontaskbuilder.Version', {
	name : '',
	uname: '',
	id : '',
	unamespace: '',
	versionsList: {
		mtype: 'store',
		fields: ['documentName', 'isStable', 'referenced', 'version', 'type', 'comment', 'createdBy', {
			name: 'sysCreatedOn',
			type: 'time',
			format: 'c',
			dateFormat: 'c'
		}],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	compareVersionIsVisible: true,
	rollbackVersionIsVisible: true,
	versionsListSelections: [],
	versionsListColumns: [],
	init: function() {
		this.initPagination();
		this.loadVersion();
		var dateFormat = this.parentVM.userDateFormat || this.rootVM.userDateFormat;
		this.set('versionsListColumns', [{
			header: '~~documentName~~',
			dataIndex: 'documentName',
			width: 132,
			sortable: false
		}, {
			header: '~~versionNumber~~',
			dataIndex: 'version',
			width: 100
		}, {
			header: '~~stableVersion~~',
			dataIndex: 'isStable',
			width: 100,
			renderer: RS.common.grid.booleanRenderer(),
		}, {
			header: '~~referenced~~',
			dataIndex: 'referenced',
			width: 100,
			renderer: RS.common.grid.booleanRenderer(),
		}, {
			header: '~~comment~~',
			flex: 1,
			dataIndex: 'comment',
			sortable: false,
			renderer: function(value, metaData) {
				metaData['tdAttr'] = Ext.String.format('data-qtip="{0}"', Ext.String.htmlEncode(value));
				return Ext.String.htmlEncode(value);
			},
		}, {
			header: '~~sysUpdatedBy~~',
			dataIndex: 'createdBy'
		}, {
			header: '~~createdOn~~',
			dataIndex: 'sysCreatedOn',
			width: 150,
			renderer: function(value) {
				return Ext.Date.format(new Date(value), dateFormat)
			}
		}]);
		if (this.isDetails) {
			this.set('compareVersionIsVisible', false);
			this.set('rollbackVersionIsVisible', false);
		}
	},
	loadVersion: function() {
		this.versionsList.removeAll();
		this.ajax({
			url: '/resolve/service/actiontask/getHistory',
			params: {
				id: this.id,
				docFullName: this.name
			},
			method: 'GET',
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('listVersionsError'), respData.message);
					return;
				}
				this.loadPageRecords(respData.records);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},
	commitVersion: function() {

	},

	// Client-side pagination
	list: [],
	pageList: [],
	currentPage: 1,
	numberPerPage: 20,
	numberOfPages: 1,
	paginationPadding: '0 0 0 0',

	nextDisabled: false,
	previousDisabled: false,
	firstDisabled: false,
	lastDisabled: false,

	initPagination: function() {
		var versionDialogWidth = Math.round(Ext.getBody().getWidth() * 0.8);
		this.set('paginationPadding', Ext.String.format('0 0 0 {0}', Math.round(versionDialogWidth / 2) - 100));
	},

	loadPageRecords: function(records) {
		this.set('list', records);
		this.set('numberOfPages', Math.ceil(this.list.length / this.numberPerPage));
		this.loadList();
	},

	firstPage: function() {
		this.set('currentPage', 1);
		this.loadList();
	},
	lastPage: function() {
		this.set('currentPage', this.numberOfPages);
		this.loadList();
	},
	nextPage: function() {
		this.set('currentPage', this.currentPage += 1);
		this.loadList();
	},
	previousPage: function() {
		this.set('currentPage', this.currentPage -= 1);
		this.loadList();
	},
	loadList: function() {
		var begin = ((this.currentPage - 1) * this.numberPerPage);
		var end = begin + this.numberPerPage;
		this.set('pageList', this.list.slice(begin, end));
		this.versionsList.loadData(this.pageList);
		this.check();
	},
	check: function() {
		this.set('nextDisabled', (!this.numberOfPages || this.currentPage == this.numberOfPages) ? true : false);
		this.set('previousDisabled', this.currentPage == 1 ? true : false);
		this.set('firstDisabled', this.currentPage == 1 ? true : false);
		this.set('lastDisabled', (!this.numberOfPages || this.currentPage == this.numberOfPages) ? true : false);
	},
	paginationText$: function() {
		var max = this.list.length;
		var begin = max ? ((this.currentPage - 1) * this.numberPerPage) + 1 : 0;
		var end = begin + this.numberPerPage - 1;
		if (end > max) {
			end = max;
		}
		return Ext.String.format('{0} - {1} of {2}', begin, end, max);
	},

	viewVersion: function() {
		/* TODO
		var versionNo = this.versionsListSelections[0].get('version');
		this.open({
			mtype: 'RS.actiontaskbuilder.VersionView',
			id: this.id,
			namespace: this.unamespace,
			name: this.uname,
			version: versionNo
		}); 
		*/ 
	},
	viewVersionIsEnabled$: function() {
		return this.versionsListSelections.length == 1;
	},
	compareVersion: function() {
		/* TODO
		this.open({
			mtype: 'RS.pagebuilder.VersionComparison',
			id: this.id,
			namespace: this.unamespace,
			name: this.uname,
			type: this.versionsListSelections[0].get('type'),
			version1: this.versionsListSelections[0].get('version'),
			version2: this.versionsListSelections[1].get('version')
		}); 
		*/ 
	},
	compareVersionIsEnabled$: function() {
		if (this.versionsListSelections.length != 2)
			return false;
		var type1 = this.versionsListSelections[0].get('type');
		var type2 = this.versionsListSelections[1].get('type');
		return type1 == type2;
	},
	rollbackVersion: function() {
		/* TODO
		this.message({
			title: this.localize('rollbackVersion'),
			msg: this.localize('rollbackVersionMsg'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				'yes': this.localize('rollbackVersion'),
				'no': this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.ajax({
					url: '/resolve/service/actiontask/version/rollback',
					params: {
						id: this.id,
						docFullname: this.ufullname,
						version: this.versionsListSelections[0].get('version')
					},
					success: function(resp) {
						var respData = RS.common.parsePayload(resp);
						if (!respData.success) {
							clientVM.displayError(this.localize('rollbackError', {
								msg: respData.message
							}));
							return;
						}
						clientVM.displaySuccess(this.localize('rollbackSuccess'));
						this.parentVM.set('havePersistedWikiSinceLastView', true);					
						this.loadVersion();
						this.parentVM.refresh();
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				});
			}
		}) 
		*/ 
	},
	rollbackVersionIsEnabled$: function() {
		return this.versionsListSelections.length == 1;
	},
	selectVersion: function() {
        if (!this.dumper)
            return
        if (Ext.isFunction(this.dumper))
            this.dumper(this.versionsListSelections[0]);
        else {
            var scope = this.dumper.scope;         
            this.dumper.dump.call(scope, [this.versionsListSelections[0]]);
        }
        this.doClose()
	},
	selectVersionIsEnabled$: function() {
		return this.versionsListSelections.length == 1;
	},
	close: function() {
		this.doClose();
	}
});
glu.defModel('RS.actiontask.ActionTaskPicker', {
    title: '',
    allTaskTabIsPressed: true,
    fromAutomationTabIsPressed : false,
    dumper: null,
    activeTab : 0,
    multiSelection : false, 
    currentDocName : '',
    allTask : {
        mtype : 'AllTask'
    },
    fromAutomation : {
        mtype : 'FromAutomation'
    },
    init : function(){
        this.set('title', this.localize('selectTitle'));
        this.allTask.set('multiSelection', this.multiSelection);    
        this.fromAutomation.set('multiSelection', this.multiSelection);
        this.fromAutomation.set('automationSearch', this.currentDocName ? this.currentDocName : '');   
    },
    dump: function() {
        var actiontaskSelection = this.allTaskTabIsPressed ? this.allTask.actionTaskGridStoreSelections : this.fromAutomation.actionTaskGridStoreSelections;
        if (!this.dumper)
            return
        if (Ext.isFunction(this.dumper))
            this.dumper(actiontaskSelection);
        else {
            var scope = this.dumper.scope || this;
            this.dumper.dump.call(scope, actiontaskSelection);
        }
        this.doClose();
    },  
    cancel: function() {
        this.doClose();
    }, 
    allTaskTab : function(){
       this.displayTab("allTask");
    },
    fromAutomationTab : function(){
        this.displayTab("fromAutomation");
    },
    displayTab : function(tabName){
        this.set('allTaskTabIsPressed', tabName == "allTask");
        this.set('fromAutomationTabIsPressed', tabName == "fromAutomation");
        this.set('activeTab', tabName == "allTask" ? 0 : 1);
    }   
})
glu.defModel('RS.actiontask.AllTask',{
    mixins: ['Main'],
    multiSelection : false,  
    actionTaskGridStore: {
        mtype: 'store',
        sorters: ['uname'],
        fields: ['id', 'ufullName','uname', 'uactive', 'unamespace', 'usummary', 'udescription', 'resolveActionInvoc'].concat(RS.common.grid.getSysFields()),
        remoteSort: true,
        proxy: {
            type: 'ajax',
            url: '/resolve/service/actiontask/list',
            reader: {
                type: 'json',
                root: 'records'
            },
            listeners: {
                exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
            }
        }
    },
    columns: [],
    actionTaskTreeMenuPathColumns: [],
    actionTaskTreeModuleColumns: [],

       init: function() {      
        this.set('columns', [{
            header: '~~name~~',
            dataIndex: 'uname',
            filterable: true,
            width: 250
        }, {
            header: '~~active~~',
            dataIndex: 'uactive',
            filterable: true
        }, {
            header: '~~module~~',
            dataIndex: 'unamespace',
            filterable: true
        }, {
            header: '~~summary~~',
            dataIndex: 'usummary',
            filterable: true,
            renderer: RS.common.grid.plainRenderer(),
            flex: 1
        }].concat(RS.common.grid.getSysColumns()))

        this.set('actionTaskTreeMenuPathColumns', [{
            xtype: 'treecolumn',
            dataIndex: 'name',
            text: this.localize('menuPath'),
            flex: 1
        }])

        this.set('actionTaskTreeModuleColumns', [{
            xtype: 'treecolumn',
            dataIndex: 'name',
            text: this.localize('module'),
            flex: 1
        }])

        this.actionTaskGridStore.on('load', function(store, rec, suc) {
            if (!suc) {
                this.doClose();
            }
        }, this);

        this.set('menuPathSelected', this.actionTaskTreeMenuStore.getRootNode());
        this.set('moduleSelected', this.actionTaskTreeModuleStore.getRootNode());
        this.attachReqParam();
        this.menuPath();
        this.actionTaskGridStore.load();
    },

    actionTaskGridStoreSelections: [],
    selectedTreeNodeId: '',
    searchDelimiter: '',
    activeItem : 0,
   
    menuPath: function() {
        this.set('searchDelimiter', '/');
        this.set('activeItem', 0);
        if (!this.menuPathSelected || this.menuPathSelected.get('name') == 'All')
            with(this.actionTaskTreeMenuStore.getRootNode()) {
                collapse();
                expand();
            }
        this.actionTaskGridStore.load();
    },

    module: function() {
        this.set('searchDelimiter', '.')
        this.set('activeItem', 1);
        if (!this.moduleSelected || this.moduleSelected.get('name') == 'All')
            with(this.actionTaskTreeModuleStore.getRootNode()) {
                collapse();
                expand();
            }
        this.actionTaskGridStore.load();
    },
    editActionTask: function(id) {
        clientVM.handleNavigation({
            modelName: 'RS.actiontask.ActionTask',
            target: '_blank',
            params: {
                id: id
            }
        })
    }
})
glu.defModel('RS.actiontask.FromAutomation',{  
    columns: [],
    multiSelection : false,
    automationSearch : '',
    automationSearchIsValid : true, 
    actionTaskGridStoreSelections: [], 
    prevSelections : [],
    automationStore: {
        mtype: 'store',
        fields: ['ufullname'],
        proxy: {
            type: 'ajax',
            url: '/resolve/service/wikiadmin/listRunbooks',           
            reader: {
                type: 'json',
                root: 'records'
            },
            listeners: {
                exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
            }
        }
    },
    actionTaskGridStore: {
        mtype: 'store',
        sorters: ['uname'],
        fields: ['id','ufullName', 'uname', 'uactive', 'unamespace', 'usummary', 'udescription', 'resolveActionInvoc'].concat(RS.common.grid.getSysFields()),
        remoteSort: true,
        proxy: {
            type: 'ajax',
            url: '/resolve/service/wikiadmin/listABTasks',
            reader: {
                type: 'json',
                root: 'records'
            },
            listeners: {
                exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
            }
        }
    },
    when_automation_changed : {
        on : ['automationSearchChanged'],
        action : function(){
            this.validateAutomation(this.automationSearch);
        }
    },
    init: function() {      
        this.set('columns', [{
            header: '~~name~~',
            dataIndex: 'uname',
            filterable: true,
            width: 250
        }, {
            header: '~~active~~',
            dataIndex: 'uactive',
            filterable: true
        }, {
            header: '~~module~~',
            dataIndex: 'unamespace',
            filterable: true
        }, {
            header: '~~summary~~',
            dataIndex: 'usummary',
            filterable: true,
            renderer: RS.common.grid.plainRenderer(),
            flex: 1
        }].concat(RS.common.grid.getSysColumns()))      
        this.actionTaskGridStore.on('beforeload',function(store,op){
            op.params = op.params || {};
            Ext.apply(op.params, {
                name : this.automationSearch
            })
        },this);
        this.automationStore.on('beforeload', function(store, op) {
            op.params = op.params || {};
            Ext.apply(op.params, {
                filter: Ext.encode([{
                    field: 'ufullname',
                    condition: 'contains',
                    type: 'auto',
                    value: op.params.query || this.automationSearch
                }])
            });          
        }, this);
        this.automationStore.on('load', function() {
            this.validateAutomation(this.automationSearch);          
        }, this)
        this.automationStore.load();
    },
    validateAutomation: function(automation) {
        var r = this.automationStore.findRecord('ufullname', automation, 0, false, false, true);
        if (!r){
            this.set('automationSearchIsValid', this.localize('invalidAutomation'));        
            this.actionTaskGridStore.removeAll();
            //this.actionTaskGridStore.loadData(this.actionTaskGridStoreSelections);           
        }
        else {
            this.set('automationSearchIsValid', true);
            this.actionTaskGridStore.load();
        }  
    },
    editActionTask: function(id) {
        clientVM.handleNavigation({
            modelName: 'RS.actiontask.ActionTask',
            target: '_blank',
            params: {
                id: id
            }
        })
    },  
})
glu.defModel('RS.actiontask.ActionTaskValidation', {
	validateName: function(name) {
		return RS.common.validation.TaskName.test(name);
	},
	validateNamespace: function(namespace) {
		return RS.common.validation.TaskNamespace.test(namespace);
	}
});
glu.defModel('RS.actiontask.BulkOpWarningList', {
	action: '',
	bulkWarning: '',
	msg: '',
	selected: null,
	records: [],
	html: '',
	forceToProceed: '',
	dumper: null,	
	init: function() {
		this.set('bulkWarning', this.localize(this.action + 'BulkWarning'));
		this.set('forceToProceed', this.localize(this.action + 'forceToProceed'), {
			msg: this.msg
		})
		var list = '';
		Ext.each(this.records, function(r) {
			list += Ext.String.format('<li>{0}</li>', r);
		}, this);
		list = '<ul>' + list + '</ul>';
		this.set('html', list);
	},
	selectionChanged: function(selected) {
		this.set('selected', selected.length > 0 ? selected[0] : null);
	},
	proceedBulk: function() {
		this.dumper();
		this.close();
	},
	proceedBulkIsVisible$: function() {
		return this.action == 'copy';
	},
	closeBulkIsVisible$ : function(){
		return this.action == 'copy';
	},
	closeIsVisible$ : function(){
		return this.action != 'copy';
	},
	buttonAlign$ : function(){
		return this.proceedBulkIsVisible ? 'right' : 'center'
	},
	close: function() {
		this.doClose();
	},
	closeBulk : function(){
		this.doClose();
	}
});
glu.defModel('RS.actiontask.CNSDefinition', {
	wait: false,

	id: '',

	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrganizationName: '',

	uname: '',
	unameIsValid$: function() {
		return this.uname && (/^[\w_]+$/.test(this.uname) || /^[\w_][\w_\.\s]*[\w_]$/.test(this.uname)) ? true : this.localize('invalidName');
	},
	umodule: '',
	umoduleIsValid$: function() {
		return this.umodule && (/^[\w_]+$/.test(this.umodule) || /^[\w_][\w_\.\s]*[\w_]$/.test(this.umodule)) ? true : this.localize('invalidModule');
	},
	uprefix: '',
	uprefixIsValid$: function() {
		return this.uprefix && (/^[\w_]+$/.test(this.uprefix) || /^[\w_][\w_\.\s]*[\w_]$/.test(this.uprefix)) ? true : this.localize('invalidPrefix');
	},
	uxpath: '',
	utype: 'LIST',
	udescription: '',

	fields: ['uname', 'umodule', 'utype', 'udescription', 'uprefix', 'uxpath'],

	moduleStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type:'memory'
			// type: 'ajax',
			// url: '/test',
			// reader: {
			// 	type: 'json',
			// 	root: 'records'
			// }
		}
	},

	typeStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	init: function() {
		this.moduleStore.load();
		this.typeStore.add([{
			name: this.localize('LIST'),
			value: 'LIST'
		}, {
			name: this.localize('STRING'),
			value: 'STRING'
		}, {
			name: this.localize('MAP'),
			value: 'MAP'
		}, {
			name: this.localize('LISTMAP'),
			value: 'LISTMAP'
		}, {
			name: this.localize('XML'),
			value: 'XML'
		}]);
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/cns/modules',
			success: function(resp) {
				this.set('wait', false);
				var respData = Ext.JSON.decode(resp.responseText);
				if (!respData.success) {
					this.parentVM.clientVM.displayError(this.localize('GetModulesErr') + '[' + respData.message + ']');
					return;
				}
				Ext.each(respData.records, function(r) {
					this.moduleStore.add({
						name: r,
						value: r
					})
				}, this)
			},
			failure: function(resp) {
				this.set('wait', false);
				this.parentVM.clientVM.displayError(this.localize('ServerError') + '[' + resp.status + ']');
			}
		});
		if (this.id)
			this.loadRecord();
	},

	loadRecord: function() {
		this.set('wait', true)
		this.ajax({
			url: '/resolve/service/cns/get',
			params: {
				id: this.id
			},

			success: function(resp) {
				this.set('wait', false);
				var respData = Ext.JSON.decode(resp.responseText);
				if (!respData.success)
					this.parentVM.clientVM.displayError(this.localize('GetPropertyErr') + '[' + respData.message + ']');
				this.loadData(respData.data);

			},
			failure: function(resp) {
				this.set('wait', false);
				this.parentVM.clientVM.displayError(this.localize('ServerError') + '[' + resp.status + ']');
			}
		})
	},

	userDateFormat$: function() {
		return this.parentVM.clientVM.getUserDefaultDateFormat()
	},

	saveRecord: function() {
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/cns/save',
			jsonData: this.asObject(),
			success: function(resp) {
				this.set('wait', false);
				var respData = Ext.JSON.decode(resp.responseText);
				if (!respData.success)
					this.parentVM.clientVM.displayError(this.localize('SaveCNSDefintionErr') + '[' + respData.message + ']');
				else {
					this.parentVM.clientVM.displaySuccess(this.localize('SaveCNSDefintionSuc'));
					this.loadData(respData.data);
				}
				this.close();
				this.parentVM.store.load();
			},

			failure: function(resp) {
				this.set('wait', false);
				this.parentVM.clientVM.displayError(this.localize('ServerError') + '[' + resp.status + ']');
			}
		})
	},

	save: function(exitAfterSave) {
		this.saveRecord();
	},
	saveIsEnabled$: function() {
		return !this.wait && this.isDirty && this.isValid;
	},
	close: function() {
		this.doClose();
	}
})
glu.defModel('RS.actiontask.CNSDefinitions', {
	displayName: '',

	columns: null,

	stateId: 'rsactiontaskcnsgrid',

	wait: false,

	store: {
		mtype: 'store',

		fields: ['uname', 'utype', 'uvalue', 'umodule', 'udescription'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'uname',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/cns/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	recordsSelections: [],

	activate: function() {
		this.clientVM.setWindowTitle(this.localize('windowTitle'))
		this.store.load();
	},

	init: function() {
		this.set('displayName', this.localize('displayName'));

		this.set('columns', [{
			header: '~~uname~~',
			filterable: true,
			sortable: true,
			dataIndex: 'uname',
			flex: 2
		}, {
			header: '~~umodule~~',
			dataIndex: 'umodule',
			filterable: true,
			sortable: true,
			width: 140
		}, {
			header: '~~udescription~~',
			dataIndex: 'udescription',
			filterable: false,
			sortable: false,
			flex: 3,
			renderer: RS.common.grid.plainRenderer()
		}].concat((function() {
			var cols = RS.common.grid.getSysColumns();
			Ext.each(cols, function(c) {
				if (c.dataIndex == 'sysUpdatedOn') {
					c.hidden = false;
					c.initialShow = true;
				}
			})
			return cols;
		})()));
		this.store.on('beforeload', function() {
			this.set('wait', true)
		}, this)
		this.store.on('load', function(store, rec, suc) {
			this.set('wait', false);
			/*
			if (!suc) {
				this.clientVM.displayError(this.localize('ListRecordsErr'))
			}
			*/
		}, this);
	},

	editRecord: function(id) {
		this.open({
			mtype: 'RS.actiontask.CNSDefinition',
			id: id
		})
	},

	createRecord: function() {
		this.open({
			mtype: 'RS.actiontask.CNSDefinition',
			id: ''
		})
	},

	deleteRecords: function() {
		this.message({
			title: this.localize('DeleteTitle'),
			msg: this.localize(this.recordsSelections.length > 1 ? 'DeletesMsg' : 'DeleteMsg', {
				num: this.recordsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirmDelete'),
				no: this.localize('cancel')
			},

			fn: this.sendDeleteReq
		})
	},

	sendDeleteReq: function(btn) {
		if (btn != 'yes')
			return;

		this.set('wait', true)

		var ids = [];

		Ext.each(this.recordsSelections, function(r) {
			ids.push(r.get('id'));
		})

		this.ajax({
			url: '/resolve/service/cns/delete',
			params: {
				ids: ids,
				all: this.allSelected
			},
			success: function(resp) {
				this.set('wait', false);
				this.handleDeleteSucces(resp);
			},
			failure: function(resp) {
				this.set('wait', false);
				this.clientVM.displayError(this.localize('ServerError') + '[' + resp.status + ']');
			}
		})
	},

	handleDeleteSucces: function(resp) {
		this.set('wait', false);
		var respData = Ext.JSON.decode(resp.responseText);
		if (!respData.success)
			this.clientVM.displayError(this.localize('DeleteErrMsg') + '[' + respData.message + ']')
		else
			this.clientVM.displaySuccess(this.localize('DeleteSucMsg'));
		this.store.load();
	},

	createRecordIsEnabled$: function() {
		return !this.wait;
	},

	deleteRecordsIsEnabled$: function() {
		return this.recordsSelections.length > 0;
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	}
})
glu.defModel('RS.actiontask.Execute', {
	API : {
		getAT : '/resolve/service/actiontask/get',
		submit : '/resolve/service/execute/submit'
	},
	target: null,
	newWorksheet: true,
	activeWorksheet: false,
	debug: false,
	mock: false,
	mockName: '',
	sirProblemId: '',

	fromWiki: false,
	mockEditable$: function() {
		return this.fromWiki
	},

	mockStore: {
		mtype: 'store',
		fields: ['uname'],
		proxy: {
			type: 'memory'
		}
	},

	paramStore: {
		mtype: 'store',
		fields: ['name', 'value', 'order'],
		proxy: {
			type: 'memory'
		}
	},

	paramColumns: [{
		header: '~~name~~',
		dataIndex: 'name',
		filterable: false,
		flex: 1,
		editor: {},
		renderer: RS.common.grid.plainRenderer()
	}, {
		header: '~~value~~',
		dataIndex: 'value',
		filterable: false,
		flex: 1,
		editor: {},
		renderer: RS.common.grid.plainRenderer()
	}],

	//when another module use this view, it can use this to init the vm
	inputsLoader: function() {
		this.parentVM.paramsTabCard.paramsStore.each(function(r) {
			if (r.get('utype') == 'INPUT')
				this.paramStore.add({
					name: r.get('uname'),
					value: r.get('udefaultValue'),
					order: r.get('uorder')
				});
		}, this);
		this.paramStore.sort('order');
		this.parentVM.mockTabCard.mockStore.each(function(r) {
			this.mockStore.add(r);
		}, this);
	},
	embedded: false,
	init: function() {
		this.set('debug', clientVM.executionDebugDefaultMode || false);
		if (this.executeDTO && this.embedded)
			this.embeddedExecution();
		else
			this.inputsLoader(this.paramStore, this.mockStore);
	},

	embeddedExecution: function() {
		this.doClose();
		this.ajax({
			url: this.API['getAT'],
			params: {
				id: this.executeDTO.id,
				name: this.executeDTO.name
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('LoadActionTaskErr'), respData.message);
					return;
				}
				var task = respData.data;
				var params = task.resolveActionInvoc.resolveActionParameters;
				Ext.Object.each(this.executeDTO.inputParams, function(k, v) {
					Ext.each(params, function(param) {
						if (param.utype != 'INPUT')
							return;
						if (param.uname != k)
							return;
						param.udefaultValue = v;
					});
				});
				var mockData = task.resolveActionInvoc.resolveActionTaskMockData;
				if (this.executionWin)
					this.executionWin.doClose();
				this.executionWin = this.open({
					mtype: 'RS.actiontask.Execute',
					// target: button.getEl().id,
					executeDTO: {
						actiontask: task.ufullName,
						actiontaskSysId: task.id,
						// problemId: this.newWorksheet ? 'NEW' : null,
						action: 'TASK'
					},
					inputsLoader: function(paramsStore, mockStore) {
						Ext.each(params, function(param) {
							if (param.utype == 'INPUT')
								paramsStore.add({
									name: param.uname,
									value: param.udefaultValue,
									order: param.uorder
								})
						});
						paramsStore.sort('order');
						if (mockData)
							mockStore.add(mockData);
						this.set('embedded', true);
					}
				});
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	paramsSelections: [],
	addParam: function() {
		var max = 0;
		var reg = /^NewParam(\d*)$/;
		var newParam = 'NewParam';

		this.paramStore.each(function(param) {
			if (!reg.test(param.get('name')))
				return;
			var m = reg.exec(param.get('name'));
			var idx = parseInt(m[1]);
			if (idx > max)
				max = idx;
		}, this);
		newParam += ((max + 1) || ''); 

		this.paramStore.add({
			name: newParam,
			value: ''
		})

		// clear out all previously selected items
		this.paramStore.grid.getSelectionModel().deselectAll();

		// determine row of newly added "NewParam"
		var row;
		for (row = this.paramStore.data.items.length-1; row > 0; row--) {
			if (this.paramStore.data.items[row].data.name == newParam) {
				break;
			}
		}
		// start editing the newly selected item
		this.paramStore.grid.getPlugin().startEditByPosition({
			row: row,
			column: 0
		});
	},

	removeParams: function() {
		this.paramStore.remove(this.paramsSelections);
	},

	mockNameIsEnabled$: function() {
		return this.mock
	},

	mockNameIsValid$: function() {
		return (!this.mock || this.mockName) ? true : this.localize('invalidMockName');
	},

	cancel: function() {
		this.doClose()
	},
	executeDTO: null,
	execute: function() {
		var map = {};
		this.paramStore.each(function(param) {
			if (param.get('name') === 'PROBLEMID' && this.sirProblemId !== '') {
				//Pass sir activity worksheet id when called from playbook
				map[param.get('name')] = this.sirProblemId;
			} else {
				map[param.get('name')] = param.get('value');
			}
		}, this);
		var dto = Ext.apply({
			problemId: this.newWorksheet ? 'NEW' : 'ACTIVE',
			isDebug: this.debug,
			mockName: this.mockName,
			params: Ext.apply(map, {
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			}),			
		}, this.executeDTO);

		if (this.fromWiki) {
			var mockNames = Ext.state.Manager.get('wikiMockNames', []);
			if (Ext.Array.indexOf(mockNames, this.mockName) == -1) {
				mockNames.splice(0, 0, this.mockName)
				Ext.state.Manager.set('wikiMockNames', mockNames.slice(0, 10))
			}
		}

		this.ajax({
			url: this.API['submit'],
			jsonData: dto,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				this.doClose();
				if (response.success) {
					clientVM.updateProblemInfo(response.data.problemId, response.data.problemNumber)
					clientVM.fireEvent('worksheetExecuted', clientVM);
					if (this.activeWorksheet) {
						clientVM.fireEvent('refreshActiveWorksheet');
					}
					clientVM.displaySuccess(this.localize('executeSuccess', {
						type: !dto.wiki ? 'ActionTask' : 'Runbook',
						fullName: !dto.wiki ? dto.actiontask : dto.wiki
					}))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	executeIsEnabled$: function() {
		return this.mockNameIsValid;
	}
});
glu.defModel('RS.actiontask.Main', {

    mock: false,

    reqCount: 0,

    assignedToUsername: '',

    actionTaskGridStore: {
        mtype: 'store',
        fields: ['uname', {
            name: 'uactive',
            type: 'bool'
        }, 'unamespace', 'usummary', {
            name: 'assignedTo.UUserName',
            convert: function(v, r) {
                return r.raw.assignedTo ? r.raw.assignedTo.uuserName : '';
            }
        }].concat(RS.common.grid.getSysFields()),
        remoteSort: true,
        sorters: [{
            property: 'uname',
            direction: 'ASC'
        }],
        proxy: {
            type: 'ajax',
            url: '/resolve/service/actiontask/list',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },

    actionTaskTreeMenuStore: {
        mtype: 'treestore',
        fields: ['id', 'name'],
        proxy: {
            type: 'memory'
        },
        root: {
            name: 'All',
            id: 'root_menu_id',
            expanded: true
        }
    },

    actionTaskTreeModuleStore: {
        mtype: 'treestore',
        fields: ['id', 'name'],
        proxy: {
            type: 'memory'
        },
        root: {
            name: 'All',
            id: 'root_module_id',
            expanded: true
        }
    },

    columns: [],
    actionTaskTreeMenuPathColumns: [],
    actionTaskTreeModuleColumns: [],

    loadTreeLayer: function(node) {
        var path = node.getPath('name').substring(5);
        if (!node.isLeaf() && path)
            path += '/';
        if (this.activeItem == 1)
            path = path.replace(/\//g, '\.');
        var filter = '';
        if (this.assignedToUsername)
            filter = Ext.JSON.encode([{
                "field": "assignedTo.UUserName",
                "type": "auto",
                "condition": "equals",
                "value": this.assignedToUsername
            }])
        this.ajax({
            url: '/resolve/service/actiontask/treelist',
            params: {
                node: path,
                delim: this.activeItem == 0 ? '/' : '\.',
                filter: filter
            },
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    clientVM.displayError(this.localize(this.activeItem == 1 ? 'ListModuleTreeError' : 'ListMenuTreeError') + '[' + respData.message + ']');
                    return;
                }
                node = node || (this.activeItem == 0 ? this.actionTaskTreeMenuStore.getRootNode() : this.actionTaskTreeModuleStore.getRootNode());
                path = node.getPath('name');
                if (respData.records.length == 0)
                    node.set('leaf', true);
                else
                    Ext.each(respData.records, function(r) {
                        delete r.children;
                        r.id = path + '/' + r.id;
                        node.appendChild(r);
                    });
            },
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
        });
    },
    inited: false,
    activate: function() {
        clientVM.setWindowTitle(this.localize('windowTitle'));
        this.set('menuPathSelected', this.menuPathSelected || this.actionTaskTreeMenuStore.getRootNode());

        if (!this.inited) {
            this.menuPath();
            this.set('inited', true);
        }
        else
            this.actionTaskGridStore.load();
    },

    init: function() {
        if (this.mock)
            this.backend = RS.actiontask.createMockBackend(true)
        this.set('columns', [{
            header: '~~name~~',
            dataIndex: 'uname',
            filterable: true,
            width: 250,
            renderer: RS.common.grid.plainRenderer()
        }, {
            header: '~~module~~',
            dataIndex: 'unamespace',
            filterable: true
        }, {
            header: '~~active~~',
            dataIndex: 'uactive',
            filterable: true,
            renderer: RS.common.grid.booleanRenderer(),
            width: 60,
            align : 'center'
        }, {
            header: '~~summary~~',
            dataIndex: 'usummary',
            filterable: true,
            flex: 1,
            renderer: RS.common.grid.plainRenderer()
        }, {
            header: '~~assignedToUsername~~',
            dataIndex: 'assignedTo.UUserName',
            filterable: true,
            hideable: false,
            hidden: false
        }].concat((function(cols) {
            Ext.each(cols, function(c) {
                if (c.dataIndex == 'sysUpdatedOn') {
                    c.initialShow = true;
                    c.hidden = false;
                }
            });
            return cols;
        })(RS.common.grid.getSysColumns())))

        this.set('actionTaskTreeMenuPathColumns', [{
            xtype: 'treecolumn',
            dataIndex: 'name',
            text: 'Menu Path',
            flex: 1
        }])

        this.set('actionTaskTreeModuleColumns', [{
            xtype: 'treecolumn',
            dataIndex: 'name',
            text: 'Module',
            flex: 1
        }])

        this.attachReqParam();
        this.actionTaskGridStore.on('load', function(store, records, success, eOpts) {
            // Update selections
            if (success && records.length > 0) {
                for (var i=0; i<this.actionTasksSelections.length; i++) {
                    var newRecord = store.getById(this.actionTasksSelections[i].get('id'));
                    if (newRecord) {
                        this.actionTasksSelections[i] = newRecord;
                    }
                }
            }
        }, this);
    },

    getNodePath: function() {
        var selected = this.activeItem == 0 ? this.menuPathSelected : this.moduleSelected;
        var path = selected ? selected.getPath('name').substring(5) : '';
        if (path) {
            path += !selected.isLeaf() ? '/' : '';
            if (this.activeItem == 1)
                path = path.replace(/\//g, '\.');
            else
                path = '/' + path;
        } else {
            path = this.activeItem == 0 ? 'root_menu_id' : 'root_module_id'
        }
        return path;
    },
    attachReqParam: function() {
        this.actionTaskGridStore.on('beforeload', function(store, operation, eOpts) {
            operation.params = operation.params || {};
            var path = this.getNodePath();
            Ext.apply(operation.params, {
                delim: this.activeItem == 0 ? '/' : '\.',
                id: path
            });
            this.set('reqCount', this.reqCount + 1);
        }, this)

        this.actionTaskGridStore.on('load', function(store, rec, suc) {
			/*
            if (!suc)
                clientVM.displayError(this.localize('LoadActionTaskErr'));
			*/
            this.set('reqCount', this.reqCount - 1);
        }, this);

        this.actionTaskTreeMenuStore.on('beforeexpand', function(node) {
            node.removeAll();
            this.loadTreeLayer(node);
        }, this);

        this.actionTaskTreeModuleStore.on('beforeexpand', function(node) {
            node.removeAll();
            this.loadTreeLayer(node);
        }, this);
    },

    actionTasksSelections: [],

    allSelected: false,
    selectAllAcrossPages: function(selectAll) {
        this.set('allSelected', selectAll)
    },

    selectionChanged: function() {
        this.selectAllAcrossPages(false)
    },

    createActionTask: function() {
        clientVM.handleNavigation({
            modelName: 'RS.actiontask.ActionTask'
        });
    },

    createActionTaskIsEnabled$: function() {
        return this.reqCount == 0;
    },

    deleteActionTasks: function() {
        this.message({
            title: this.localize('DeleteTitle'),
            msg: this.localize(this.actionTasksSelections.length > 1 ? 'DeletesMsg' : 'DeleteMsg', {
                num: this.allSelected ? this.actionTaskGridStore.getTotalCount() : this.actionTasksSelections.length
            }),
            buttons: Ext.MessageBox.YESNO,
            buttonText: {
                'yes': this.localize('deleteActionTasks'),
                'no': this.localize('cancel')
            },
            scope: this,
            fn: function(btn) {
                if (btn == 'no')
                    return;
                this.sendDeleteActionTasks();
            }
        })
    },

    sendDeleteActionTasks: function(hasBeenValidated) {
        var ids = []
        Ext.Array.forEach(this.actionTasksSelections, function(r) {
            ids.push(r.get('id'));
        })

        this.set('reqCount', this.reqCount + 1);

        this.ajax({
            url: '/resolve/service/actiontask/delete',
            params: {
                ids: ids,
                all: this.allSelected,
                validate: !!!hasBeenValidated
            },
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    clientVM.displayError(this.localize('DeleteErrMsg') + '[' + respData.message + ']')
                    return;
                }
                if (!!!hasBeenValidated) {
                    if (respData.records.length > 0) {
                        this.open({
                            mtype: 'RS.actiontask.ReferenceList',
                            detailScreen: 'RS.wiki.Main',
                            referenceNames: respData.records
                        })
                        return;
                    }
                }
                clientVM.displaySuccess(this.localize('DeleteSucMsg'));
                this.actionTaskGridStore.load()
            },

            failure: function(resp) {
                clientVM.displayFailure(resp);
            },
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
        })
    },

    editDefinition: function(id) {
        clientVM.handleNavigation({
            modelName: 'RS.actiontask.ActionTask',
            params: {
                id: id
            }
        })
    },
    deleteActionTasksIsEnabled$: function() {
        return this.actionTasksSelections.length > 0 && this.reqCount == 0;
    },

    searchDelimiter: '',

    activeItem: 0,

    menuPathSelected: null,

    moduleSelected: null,

    menuPath: function() {
        this.set('searchDelimiter', '/');
        this.set('activeItem', 0);
        if (!this.menuPathSelected || this.menuPathSelected.get('name').indexOf('All') != -1)
            with(this.actionTaskTreeMenuStore.getRootNode()) {
                collapse();
                expand();
            }
        this.actionTaskGridStore.load();
    },

    menuPathIsPressed$: function() {
        return this.activeItem == 0;
    },

    menuPathIsEnabled$: function() {
        return this.reqCount == 0;
    },

    module: function() {
        this.set('searchDelimiter', '.')
        this.set('activeItem', 1);
        if (!this.moduleSelected || this.moduleSelected.get('name').indexOf('All') != -1)
            with(this.actionTaskTreeModuleStore.getRootNode()) {
                collapse();
                expand();
            }
        this.actionTaskGridStore.load();
    },

    moduleIsEnabled$: function() {
        return this.reqCount == 0;
    },

    moduleIsPressed$: function() {
        return this.activeItem == 1;
    },

    menuPathTreeSelectionchange: function(selected, eOpts) {
        if (selected.length > 0)
            this.set('menuPathSelected', selected[0]);
        this.actionTaskGridStore.loadPage(1);
    },

    moduleTreeSelectionchange: function(selected, eOpts) {
        if (selected.length > 0)
            this.set('moduleSelected', selected[0]);
        this.actionTaskGridStore.loadPage(1);
    },

    actionConfirm: function(title, msg, sucMsg, errMsg, params) {
        this.message({
            title: title,
            msg: msg,
            buttons: Ext.MessageBox.YESNO,
            buttonText: {
                yes: this.localize(params.action),
                no: this.localize('cancel')
            },
            scope: this,
            fn: function(btn) {
                if (btn == 'no')
                    return;
                this.sendReq(sucMsg, errMsg, params);
            }
        })
    },

    sendReq: function(sucMsg, errMsg, params) {
        this.set('reqCount', this.reqCount + 1);
        this.ajax({
            url: '/resolve/service/actiontask/actiontaskmanipulation',
            params: params,
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success)
                    clientVM.displayError(errMsg + '[' + respData.message + ']');
                else if (respData.records.length > 0) {
                    this.open({
                        mtype: 'RS.actiontask.BulkOpWarningList',
                        msg: respData.message,
                        records: respData.records,
                        action: params.action,
                        dumper: (function(self) {
                            return function() {
                                self.sendReq(sucMsg, errMsg, Ext.apply(params, {
                                    overwrite: true
                                }));
                            }
                        })(this)
                    });
                } else
                    clientVM.displaySuccess(sucMsg);
                this.actionTaskGridStore.load();
            },
            failure: function(resp) {
                clientVM.displayFailure(resp);
            },
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
        })
    },

    doRename: function(params) {
        var title = this.localize('RenameTitle');
        var msg = this.localize(this.actionTasksSelections.length > 1 ? 'RenamesMsg' : 'RenameMsg', {
            num: this.actionTasksSelections.length
        });
        var sucMsg = this.localize('RenameSuc');
        var errMsg = this.localize('RenameErr');
        this.actionConfirm(title, msg, sucMsg, errMsg, params);
    },

    rename: function() {
        var unamespace = this.actionTasksSelections[0].get('unamespace');
        var uname = this.actionTasksSelections[0].get('uname');
        var ids = [];
        Ext.each(this.actionTasksSelections, function(r) {
            ids.push(r.get('id'));
        }, this);
        var self = this;
        this.open({
            mtype: 'RS.actiontask.MoveOrRenameOrCopy',
            unamespace: unamespace,
            uname: uname,
            action: 'rename',
            bulk: this.actionTasksSelections.length > 1,
            dumper: function(action, unamespace, uname, overwrite) {
                var params = {
                    ids: ids,
                    action: action,
                    module: unamespace,
                    name: uname,
                    overwrite: overwrite
                };
                self.doRename(params);
            }
        });
    },
    renameIsEnabled$: function() {
        return this.reqCount == 0 && this.actionTasksSelections.length > 0
    },

    doCopy: function(params) {
        var title = this.localize('CopyTitle');
        var msg = this.localize(this.actionTasksSelections.length > 1 ? 'CopysMsg' : 'CopyMsg', {
            num: this.actionTasksSelections.length
        });
        var sucMsg = this.localize('CopySuc');
        var errMsg = this.localize('CopyErr');
        this.actionConfirm(title, msg, sucMsg, errMsg, params);
    },

    copy: function() {
        var unamespace = this.actionTasksSelections[0].get('unamespace');
        var uname = this.actionTasksSelections[0].get('uname');
        var ids = [];
        var self = this;
        Ext.each(this.actionTasksSelections, function(r) {
            ids.push(r.get('id'));
        }, this);
        this.open({
            mtype: 'RS.actiontask.MoveOrRenameOrCopy',
            unamespace: unamespace,
            uname: uname,
            action: 'copy',
            bulk: this.actionTasksSelections.length > 1,
            dumper: function(action, unamespace, uname, overwrite) {
                var params = {
                    ids: ids,
                    action: action,
                    module: unamespace,
                    name: uname,
                    overwrite: overwrite
                };
                self.doCopy(params);
            }
        });
    },

    copyIsEnabled$: function() {
        return this.reqCount == 0 && this.actionTasksSelections.length > 0
    },
    index: function() {
        var ids = [];
        Ext.each(this.actionTasksSelections, function(t) {
            ids.push(t.get('id'));
        });
        this.ajax({
            url: '/resolve/service/actiontask/index',
            params: {
                all: false,
                ids: ids
            },
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    clientVM.displayError(this.localize('submitIndexReqError', respData.message));
                    return;
                }

                clientVM.displaySuccess(this.localize('submitIndexReqSuccess'));
            },
            failure: function(resp) {
				clientVM.displayFailure(resp);
			}
        });
    },
    indexIsEnabled$: function() {
        return this.reqCount == 0 && this.actionTasksSelections.length > 0;
    },
    indexAll: function() {
        this.ajax({
            url: '/resolve/service/actiontask/index',
            params: {
                all: true,
                ids: []
            },
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    clientVM.displayError(this.localize('submitIndexReqError', respData.message));
                    return;
                }
                clientVM.displaySuccess(this.localize('submitIndexReqSuccess'));
            },
            failure: function(resp) {
				clientVM.displayFailure(resp);
			}
        })
    },
    executionIsEnabled$: function() {
        return this.actionTasksSelections.length == 1 && this.reqCount == 0;
    },
    execution: function(button) {
        this.set('reqCount', this.reqCount + 1);
        var record = this.actionTasksSelections[0];
        this.ajax({
            url: '/resolve/service/actiontask/get',
            params: {
                id: record.get('id'),
                name: ''
            },
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    clientVM.displayError(this.localize('LoadActionTaskErr'), respData.message);
                    return;
                }
                var task = respData.data;
                var params = task.resolveActionInvoc.resolveActionParameters;
                var mockData = task.resolveActionInvoc.resolveActionTaskMockData;
                if (this.executionWin)
                    this.executionWin.doClose();
                this.executionWin = this.open({
                    mtype: 'RS.actiontask.Execute',
                    target: button.getEl().id,
                    executeDTO: {
                        actiontask: task.ufullName,
                        actiontaskSysId: task.id,
                        // problemId: this.newWorksheet ? 'NEW' : null,
                        action: 'TASK'
                    },
                    inputsLoader: function(paramsStore, mockStore) {
                        Ext.each(params, function(param) {
                            if (param.utype == 'INPUT')
                                paramsStore.add({
                                    name: param.uname,
                                    value: param.udefaultValue,
                                    order: param.uorder
                                })
                        });
                        paramsStore.sort('order');
                        if (mockData)
                            mockStore.add(mockData);
                    }
                });
            },
            failure: function(resp) {
                clientVM.displayFailure(resp);
            },
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
        });
    }
})
glu.defModel('RS.actiontask.MoveOrRenameOrCopy', {
    title: '',
    fields: ['unamespace', 'uname'],
    unamespace: '',
    overwrite: false,
    overwriteIsVisible$: function() {
        return this.action == 'copy';
    },
    unamespaceIsValid$: function() {
        return RS.common.validation.TaskNamespace.test(this.unamespace) ? true : this.localize('invalidNamespace');
    },
    uname: '',
    unameIsVisible$: function() {
        return !this.bulk;
    },
    unameIsValid$: function() {
        return RS.common.validation.TaskName.test(this.uname) ? true : this.localize('invalidName');
    },
    action: '',
    bulk: false,
    comboBoxStore: {
        mtype: 'store',
        fields: ['name']       
    },
    oriNamespace: '',
    oriName:  '',

    init: function() {
        this.set('oriNamespace', this.unamespace);
        this.set('oriName', this.uname);
        this.ajax({
            url: '/resolve/service/common/actiontask/namespaces',
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    clientVM('listNamespaceError' + '[' + resp.message + ']');
                    return;
                }
                Ext.each(respData.records, function(r, index) {
                    this.comboBoxStore.add({
                        name: r
                    });
                    if (this.get('unamespace') === r) {
                        // GVo Note
                        // This is a get-around
                        // solution on the combobox not populating the value.
                        // I have no idea why. According to Sencha docs, we can set the
                        // initial selection by setting the value property but some how it doesn't work in the case.
                        this.comboBoxStore.fireEvent('foundComboSelection', this.comboBoxStore.getAt(index));
                    }
                }, this);
            },
            failure: function(resp) {
                clientVM.displayFailure(resp);
            }
        });
        this.set('title', this.localize(this.action + "Title"));
    },

    dumper: null,
    dump: function() {
        this.dumper(this.action, this.unamespace, this.uname, this.overwrite);
        this.cancel();
    },
    nameChange$: function() {
        return (this.unamespace != this.oriNamespace) || (this.uname != this.oriName);
    },
    dumpIsEnabled$: function() {
        return this.isValid && this.nameChange;
    },
    actionText: '',
    dumpText$: function() {
        return this.actionText || this.localize(this.action);
    },

    cancel: function() {
        this.doClose();
    }
})
glu.defModel('RS.actiontask.PropertyDefinition', {

	wait: false,

	id: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrganizationName: '',

	uname: '',
	unameIsValid$: function() {
		return this.uname && (/^[\w_]+$/.test(this.uname) || /^[\w_][\w_\.\s]*[\w_]$/.test(this.uname)) ? true : this.localize('invalidName');
	},
	umodule: '',
	umoduleIsValid$: function() {
		return this.umodule && (/^[\w_]+$/.test(this.umodule) || /^[\w_][\w_\.\s]*[\w_]$/.test(this.umodule)) ? true : this.localize('invalidModule');
	},
	utype: 'Plain',
	uvalue: '',
	// uvalueIsValid$: function() {
	// 	return !this.uvalue || /^\s*$/.test(this.uvalue) || (/^[\w_]+$/.test(this.uvalue) || /^[\w_][\w_\.\s]*[\w_]$/.test(this.uvalue)) ? true : this.localize('invalidValue');
	// },
	fields: ['uname', 'umodule', 'utype', 'uvalue'],

	moduleStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	typeStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	isEncrypt$: function() {
		return this.utype.toUpperCase() == 'Encrypt'.toUpperCase();
	},

	init: function() {
		this.typeStore.add([{
			name: 'Plain',
			value: 'Plain'
		}, {
			name: 'Encrypt',
			value: 'Encrypt'
		}]);
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/atproperties/modules',
			success: function(resp) {
				this.set('wait', false);
				var respData = Ext.JSON.decode(resp.responseText);
				if (!respData.success) {
					this.parentVM.clientVM.displayError(this.localize('GetModulesErr') + '[' + respData.message + ']');
					return;
				}
				Ext.each(respData.records, function(r) {
					this.moduleStore.add({
						name: r,
						value: r
					})
				}, this)
			},
			failure: function(resp) {
				this.set('wait', false);
				this.parentVM.clientVM.displayError(this.localize('ServerError') + '[' + resp.status + ']');
			}
		})
		if (this.id)
			this.loadRecord();
	},

	loadRecord: function() {
		this.set('wait', true)
		this.ajax({
			url: '/resolve/service/atproperties/get',
			params: {
				id: this.id
			},

			success: function(resp) {
				this.set('wait', false);
				var respData = Ext.JSON.decode(resp.responseText);
				if (!respData.success)
					this.parentVM.clientVM.displayError(this.localize('GetPropertyErr') + '[' + respData.message + ']');
				
				//Handle this "uvalue" specially check RBA-12758
				var uvalue = respData.data.uvalue;
				respData.data.uvalue = uvalue.replace(/\r/g, '\n');
				this.loadData(respData.data);

			},
			failure: function(resp) {
				this.set('wait', false);
				this.parentVM.clientVM.displayError(this.localize('ServerError') + '[' + resp.status + ']');
			}
		})
	},

	userDateFormat$: function() {
		return this.parentVM.clientVM.getUserDefaultDateFormat()
	},

	saveRecord: function() {
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/atproperties/save',
			jsonData: this.asObject(),
			success: function(resp) {
				this.set('wait', false);
				var respData = Ext.JSON.decode(resp.responseText);
				if (!respData.success)
					this.parentVM.clientVM.displayError(this.localize('SavePropertyDefintionErr') + '[' + respData.message + ']');
				else {
					this.parentVM.clientVM.displaySuccess(this.localize('SavePropertyDefintionSuc'));
					this.loadData(respData.data);
				}
				this.close();
				this.parentVM.store.load();
			},

			failure: function(resp) {
				this.set('wait', false);
				this.parentVM.clientVM.displayError(this.localize('ServerError') + '[' + resp.status + ']');
			}
		})
	},

	save: function(exitAfterSave) {
		this.saveRecord();
	},
	saveIsEnabled$: function() {
		return !this.wait && this.isDirty && this.isValid;
	},
	close: function() {
		this.doClose();
	}
})
glu.defModel('RS.actiontask.PropertyDefinitions', {
	displayName: '',

	columns: null,

	stateId: 'rsactiontaskpropertiesgrid',

	wait: false,

	store: {
		mtype: 'store',

		fields: ['uname', 'utype', 'uvalue', 'umodule'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'uname',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/atproperties/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	recordsSelections: [],

	activate: function() {
		this.clientVM.setWindowTitle(this.localize('windowTitle'))
		this.store.load();
	},

	init: function() {
		this.set('displayName', this.localize('displayName'));

		this.set('columns', [{
			header: '~~uname~~',
			filterable: true,
			sortable: true,
			dataIndex: 'uname',
			flex: 3
		}, {
			header: '~~utype~~',
			dataIndex: 'utype',
			filterable: true,
			sortable: true,
			width: 100
		}, {
			header: '~~uvalue~~',
			filterable: false,
			sortable: false,
			dataIndex: 'uvalue',
			flex: 1,
			renderer: function(value, row) {
				var record = row.record;
				var type = record.get('utype');
				if (type.toUpperCase() == 'PLAIN')
					return RS.common.grid.plainRenderer()(value);
				return "*****";
			}
		}, {
			header: '~~umodule~~',
			dataIndex: 'umodule',
			filterable: true,
			sortable: true,
			width: 120
		}].concat((function() {
			var cols = RS.common.grid.getSysColumns();
			Ext.each(cols, function(c) {
				if (c.dataIndex == 'sysUpdatedOn') {
					c.hidden = false;
					c.initialShow = true;
				}
			})
			return cols;
		})()));
		this.store.on('beforeload', function() {
			this.set('wait', true)
		}, this)
		this.store.on('load', function(store, rec, suc) {
			this.set('wait', false);
			/*
			if (!suc) {
				this.clientVM.displayError(this.localize('ListRecordsErr'))
			}
			*/
		}, this);
	},

	editRecord: function(id) {
		this.open({
			mtype: 'RS.actiontask.PropertyDefinition',
			id: id
		})
	},

	createRecord: function() {
		this.open({
			mtype: 'RS.actiontask.PropertyDefinition',
			id: ''
		})
	},

	deleteRecords: function() {
		this.message({
			title: this.localize('DeleteTitle'),
			msg: this.localize(this.recordsSelections.length > 1 ? 'DeletesMsg' : 'DeleteMsg', {
				num: this.recordsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirmDelete'),
				no: this.localize('cancel')
			},

			fn: this.sendDeleteReq
		})
	},

	sendDeleteReq: function(btn) {
		if (btn != 'yes')
			return;

		this.set('wait', true)

		var ids = [];

		Ext.each(this.recordsSelections, function(r) {
			ids.push(r.get('id'));
		})

		this.ajax({
			url: '/resolve/service/atproperties/delete',
			params: {
				ids: ids,
				all: this.allSelected
			},
			success: function(resp) {
				this.set('wait', false);
				this.handleDeleteSucces(resp);
			},
			failure: function(resp) {
				this.set('wait', false);
				this.clientVM.displayError(this.localize('ServerError') + '[' + resp.status + ']');
			}
		})
	},

	handleDeleteSucces: function(resp) {
		this.set('wait', false);
		var respData = Ext.JSON.decode(resp.responseText);
		if (!respData.success)
			this.clientVM.displayError(this.localize('DeleteErrMsg') + '[' + respData.message + ']')
		else
			this.clientVM.displaySuccess(this.localize('DeleteSucMsg'));
		this.store.load();
	},

	createRecordIsEnabled$: function() {
		return !this.wait;
	},

	deleteRecordsIsEnabled$: function() {
		return this.recordsSelections.length > 0;
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	}
})
glu.defModel('RS.actiontask.PropertyDefinitionsPicker', {

	height$: function() {
		return Ext.isIE8m ? 600 : '80%'
	},
	width$: function() {
		return Ext.isIE8m ? 800 : '80%'
	},

	displayName: '',

	columns: null,

	stateId: 'rsactiontaskpropertiesgrid',

	store: {
		mtype: 'store',

		fields: ['uname', 'utype', 'uvalue', 'umodule'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'uname',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/atproperties/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	recordsSelections: [],

	init: function() {
		this.set('displayName', this.localize('displayName'));

		this.set('columns', [{
			header: '~~uname~~',
			filterable: true,
			sortable: true,
			dataIndex: 'uname',
			flex: 3
		}, {
			header: '~~utype~~',
			dataIndex: 'utype',
			filterable: true,
			sortable: true,
			width: 100
		}, {
			header: '~~uvalue~~',
			filterable: false,
			sortable: false,
			dataIndex: 'uvalue',
			flex: 1,
			renderer: function(value, row) {
				var record = row.record;
				var type = record.get('utype');
				if (type.toUpperCase() == 'PLAIN')
					return RS.common.grid.plainRenderer()(value);
				return "*****";
			}
		}, {
			header: '~~umodule~~',
			dataIndex: 'umodule',
			filterable: true,
			sortable: true,
			width: 120
		}].concat((function() {
			var cols = RS.common.grid.getSysColumns();
			Ext.each(cols, function(c) {
				if (c.dataIndex == 'sysUpdatedOn') {
					c.hidden = false;
					c.initialShow = true;
				}
			})
			return cols;
		})()));

		this.store.load()
	},

	select: function() {
		if (Ext.isFunction(this.dumper)) this.dumper.call(this, this.recordsSelections)
		this.doClose()
	},
	selectIsEnabled$: function() {
		return this.recordsSelections.length == 1
	},
	cancel: function() {
		this.doClose()
	}
})
glu.defModel('RS.actiontask.ReferenceList', {
	store: {
		mtype: 'store',
		fields: ['name'],
		proxy: {
			type: 'memory'
		}
	},

	referenceNames: null,
	gotoDetail: true,
	detailScreen: '',
	init: function() {
		Ext.each(this.referenceNames, function(name) {
			this.store.add({
				name: name
			})
		}, this)
	},
	selected: null,
	selectionChanged: function(selected) {
		this.set('selected', selected.length > 0 ? selected[0] : null);
	},
	gotoReference: function() {
		var name = this.selected.get('name');
		clientVM.handleNavigation({
			modelName: this.detailScreen,
			params: {
				name: name
			},
			target: '_blank'
		})
	},

	gotoReferenceIsEnabled$: function() {
		return this.selected;
	},

	gotoReferenceIsVisible$: function() {
		return this.detailScreen;
	},

	deleteRecords: function() {
		this.parentVM.sendDeleteActionTasks(true);
		this.close();
	},

	close: function() {
		this.doClose();
	}
});
glu.defView('RS.actiontaskbuilder.Access', {
	xtype: 'fieldset',
	title: '~~addRole~~',
	margin: '10 0 0 0',
	padding: '5 15 10 15',
	hidden: '@{isDisabled}',
	height: 215,
	items: [{
		xtype: 'form',
		margin: 0,
		padding: 0,

		layout: {
			type: 'vbox',
			align: 'stretch'		
		},		

		defaults: {
			labelWidth: 130,
			maxWidth: 500,
		},
		items: [{
			xtype: 'combo',
			name: 'uname',
			allowBlank: false,
			emptyText: '~~selectRole~~',
			store: '@{rolesStore}',
			value: '@{uname}',				
			queryMode: 'local',
			displayField: 'text',
			valueField: 'value',			
			cls: 'no-input-border combo',
			editable: false,
			listeners: {
				focus: '@{focusRole}'
			}
		}, {
			xtype: 'checkbox',			
			name: 'read',
			uncheckedValue: false,			
			boxLabel: '~~roleReadRight~~',
			margin: '0 0 5 0'
		}, {
			xtype: 'checkbox',
			name: 'write',
			uncheckedValue: false,			
			hideLabel: true,
			boxLabel: '~~roleWriteRight~~',
			margin: '0 0 5 135'
		}, {
			xtype: 'checkbox',
			name: 'execute',
			uncheckedValue: false,		
			hideLabel: true,
			boxLabel: '~~roleExecuteRight~~',
			margin: '0 0 5 135'
		}, {
			xtype: 'checkbox',
			name: 'admin',
			uncheckedValue: false,			
			hideLabel: true,
			boxLabel: '~~roleAdminRight~~',
			margin: '0 0 5 135'
		}, {	
			xtype: 'button',
			text: '~~addRole~~',			
			cls: 'rs-small-btn rs-btn-dark',		
			margin: '0 0 0 135',
			maxWidth: 90,
			name: 'add',
			formBind: true
		}]
	}]
});
glu.defView('RS.actiontaskbuilder.Assessor', {
	xtype: 'codeeditorpanel',
	title: '@{title}',
	cls: '@{classList}',
	hidden: '@{hidden}',
	header: '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	resizable: {
		handles: 's',
		pinned: true
	},
	padding: 0,
	bodyPadding: '10 15 12 15',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},	
	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function () {
				if (panel._vm.isViewOnly) {
					if (panel.collapsed) {
						panel.expand();
					} else {
						panel.collapse();
					}
				}
			});
			
			panel.header.items.getAt(1).on('click', function(c, e) {
				panel._vm.set('isViewOnly', false);
				panel._vm.set('collapsed', false);
				e.stopPropagation();
				return false;
			});
		},
		afterRendered: '@{viewRenderCompleted}',
		contentchange: '@{contentChange}',
		warningmsgshowed: '@{warningMsgShowed}',
		expand: '@{expand}',
		beforecollapse: function (panel) {
			panel._vm.set('isViewOnly', true);
			return this.getCollapsible();
		}
	},
	items: [{
		xtype: 'checkbox',
		disabled: '@{isViewOnly}',
		disabledCls: 'disabled-readable',
		padding: '0 0 7 0',
		checked: '@{onlyCompleted}',
		boxLabel: '~~onlyCompletedLabel~~'
	}, {
		xtype: 'AceEditor',
		parser: 'groovy',
		readOnly: '@{isViewOnly}',
		height: 500,
		name: 'content',		
		flex: 1,		
		listeners: {
			change: function () {
				this.up('codeeditorpanel').fireEvent('contentchange', this, this);
			}
		}
	}],
	warningMsg: '@{warningMsg}',
	dockedItems: [
	{
		xtype: 'toolbar',
		hidden: '@{isViewOnly}',
		dock: 'bottom',
		margin: 0,
		padding: '0 15 10 15',
		items: [{
			xtype: 'label',
			cls: '@{editorClass}',
			margin: 0,
			padding: '0 0 10 0',
			listeners: {
				afterrender: function(label) {
					this.up('codeeditorpanel').fireEvent('warningmsgshowed', this, label);
				}
			}
		},'->', {
			margin: 0,
			name: 'close',		
			cls: 'at-med-btn'
		}]
	}]
});

glu.defView('RS.actiontaskbuilder.AssessorEdit', {
	parentLayout: 'quickAccess',
	xtype: 'codeeditorpanel',
	title: '@{title}',
	cls: '@{classList}',
	hidden: '@{hidden}',
	header : '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	minHeight: '@{minHeight}',
	height: '@{height}',
	resizable: {
		handles: 's',
		pinned: true,
		minHeight: '@{minResize}'
	},
	bodyPadding: '10 15 15 15',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},	
	items: [{
		xtype: 'checkbox',	
		checked: '@{onlyCompleted}',
		boxLabel: '~~onlyCompletedLabel~~'
	}, {
		xtype: 'AceEditor',
		parser: 'groovy',
		height: '@{height}',
		name: 'content',	
		flex: 1,		
		listeners: {
			change: function () {
				this.up('codeeditorpanel').fireEvent('contentchange', this, this);
			}
		}
	},{
		xtype: 'label',
		cls: '@{editorClass}',
		margin : '10 0 0 0',
		listeners: {
			afterrender: function(label) {
				this.up('codeeditorpanel').fireEvent('warningmsgshowed', this, label);
			}
		}
	}],

	dockedItems: [{
		xtype: 'toolbar',		
		padding : '0 15 10',
		dock: 'bottom',
		items: ['->', {			
			name: 'close',
			cls: 'rs-med-btn rs-btn-dark'
		}]
	}],

	listeners: {
		contentchange: '@{contentChange}',
		warningmsgshowed: '@{warningMsgShowed}',
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});	

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	}	
});

glu.defView('RS.actiontaskbuilder.AssessorRead', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: '@{classList}',
	hidden: '@{hidden}',
	header : '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '10 15 12 15',
	minHeight: '@{minHeight}',
	height: '@{height}',
	
	resizable: {
		handles: 's',
		pinned: true,
		minHeight: '@{minResize}'
	},
	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},	

	defaults: {
		margin: '0 0 10 0'
	},

	items: [{
		xtype: 'checkbox',
		disabled: true,
		disabledCls: 'disabled-readable',
		padding: 0,
		checked: '@{onlyCompleted}',
		boxLabel: '~~onlyCompletedLabel~~',
		hidden: '@{contentIsEmpty}'
	}, {
		xtype: 'AceEditor',
		parser: 'groovy',
		readOnly: true,
		height: '@{height}',
		name: 'content',	
		cls: 'editor-disabled',
		flex: 1,
		highlightActiveLine: false, 
		highlightGutterLine: false,
		showCursor: false,
		hidden: '@{contentIsEmpty}',		
	}, {
		xtype: 'displayfield',
		hidden: '@{!contentIsEmpty}',
		margin: 0,
		padding: 0,
		value: '~~sourceCodeSectionEmptyText~~'
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},
		viewRendered: '@{viewRenderCompleted}'
	}	
});

glu.defView('RS.actiontaskbuilder.AssignValuesToOutputsEdit', {
	parentLayout: 'quickAccess',
	cls: 'block-model',
	title: '@{title}',	
	hidden: '@{hidden}',
	header: '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '10 15 15 15',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	
	dockedItems: [{
		xtype: 'toolbar',		
		padding : '0 15 10',
		dock: 'bottom',
		items: ['->', {			
			name: 'close',
			cls: 'rs-med-btn rs-btn-dark'
		}]
	}],

	items: [{
		xtype: 'component',
		hidden: '@{!gridIsEmpty}',
		html: '~~noAssignmentsMessage~~',
		margin: '10 0',	
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		store: '@{mappingStore}',
		hidden: '@{gridIsEmpty}',
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		selModel: {
			selType: 'cellmodel'
		},
		listeners: {
			validateedit: function(editor, context){
				if(context.value == '' || !context.value)
					return false;
				else if(context.value == context.originalValue)
					return;
				else
					return this._vm.validateOnEdit(context);
			},
			beforeedit : function(editor, context){
				var fieldName = context.field;
				if(fieldName == 'outputName')
					var fieldValue = context.record.get('outputType');
				else if(fieldName == 'sourceName')
					var fieldValue = context.record.get('sourceType');
				else
					return true;

				this._vm.loadParameterStore(fieldName, fieldValue);
			},
			removeAssignment: '@{removeAssignment}',
		},
		viewConfig: {
			getRowClass: function (record) {
		        if (record.get('isDuplicateAssignment')) {
		            return 'duplicate-row';
		        } else {
		            return '';
		        }
		    }
		},
		columns: [{
			header: '~~outputType~~',
			dataIndex: 'outputType',
			resizable : false,			
			flex: 1,
			editor: {
				xtype: 'combo',				
				editable: false,			
				queryMode: 'local',
				displayField: 'name',
				valueField: 'name',
				store: '@{outputTypesStore}',
				cls: 'no-input-border combo'				
			}
		}, {
			header: '~~outputName~~',
			dataIndex: 'outputName',
			resizable : false,
			flex: 1,
			renderer: RS.common.grid.plainRenderer(),
			editor: {
				xtype: 'combo',			
				queryMode: 'local',
				displayField: 'name',
				valueField: 'name',
				store: '@{outputParametersStore}',
				cls: 'no-input-border combo'
			}
		}, {
			header: '~~sourceType~~',
			dataIndex: 'sourceType',			
			flex: 1,
			editor: {
				xtype: 'combo',			
				editable: false,			
				queryMode: 'local',
				displayField: 'name',
				valueField: 'name',
				store: '@{sourceTypesStore}',
				cls: 'no-input-border combo'			
			}
		}, {
			header: '~~sourceName~~',
			dataIndex: 'sourceName',
			resizable : false,
			flex: 1,
			renderer: RS.common.grid.plainRenderer(),
			editor: {
				xtype: 'combo',				
				queryMode: 'local',
				displayField: 'name',
				valueField: 'name',
				store: '@{inputParametersStore}',
				cls: 'no-input-border combo'					
			}
		}, {
			xtype: 'actioncolumn',		
			hideable: false,
			align: 'center',		
			width: 45,			
			resizable : false,
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record) {
					this.up('grid').fireEvent('removeAssignment', view, record);
				}
			}]
		}]
	}, {		
	  	xtype : 'infoicon',
	  	iconType : 'warn',
        displayText : '~~duplicateAssignmentMessage~~',
		hidden: '@{!hasDuplicateAssignment}',	
		margin: '8 0 0 0'
	}, {
		xtype: '@{detail}'
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});	

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	}
});

glu.defView('RS.actiontaskbuilder.AssignValuesToOutputsRead', {
	parentLayout: 'quickAccess',
	title: '@{title}',	
	cls: '@{classList}',
	hidden: '@{hidden}',
	header: '@{header}',
	bodyPadding: 15,
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,    
	resizable: false,
	items: [{
		xtype: 'displayfield',
		hidden: '@{hasValues}',
		margin: 0,
		padding: 0,
		value: '~~assignValuesToOutputsEmptyText~~' 
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		hidden: '@{!hasValues}',
		store: '@{valuesStore}',
		columns: [{
			flex: 1,
			header: '~~outputType~~',
			dataIndex: 'outputType',
			renderer : function(v){
				if(v == 'OUTPUTS')
					return 'OUTPUT';
				else if(v == 'FLOWS')
					return 'FLOW';
				else if(v == 'PARAMS')
					return 'PARAM';
				else 
					return v;
			},
		}, {
			flex: 1,
			header: '~~outputName~~',
			dataIndex: 'outputName'
		}, {
			flex: 1,
			header: '~~sourceType~~',
			dataIndex: 'sourceType',
			renderer : function(v){
				if(v == 'INPUTS')
					return 'INPUT';
				else if(v == 'FLOWS')
					return 'FLOW';
				else if(v == 'PARAMS')
					return 'PARAM';
				else 
					return v;
			},
		}, {
			flex: 1,
			header: '~~sourceName~~',
			dataIndex: 'sourceName'
		}]
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);			

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},
		viewRendered: '@{viewRenderCompleted}'
	}
});


glu.defView('RS.actiontaskbuilder.AssignValueToOutputDetail', {
	xtype: 'fieldset',
	title: '~~assignValueToOutput~~',
	margin: '10 0 0 0',
	padding: '5 15',	
	layout: {
		type : 'vbox',
		align : 'stretch'
	},
	defaults: {
		labelWidth: 130,
		maxWidth : 500,
		margin: '4 0'
	},
	items: [{
		xtype: 'combobox',
		name: 'outputType',
		emptyText: '~~selectOutputType~~',
		editable: false,			
		queryMode: 'local',
		displayField: 'name',
		valueField: 'name',
		store: '@{outputTypesStore}'		
	},{
		xtype: 'textfield',	
		name: 'outputName',
		emptyText: '~~enterOutputName~~',
		hidden : '@{!outputNameIsText}'		
	},{
		xtype: 'combobox',	
		name: 'outputName',
		hidden : '@{outputNameIsText}',	
		emptyText: '@{outputNameEmptyText}',
		editable: '@{outputNameIsEditable}',		
		queryMode: 'local',
		displayField: 'name',
		valueField: 'name',			
		store: '@{outputParametersStore}',
	}, {
		xtype: 'combobox',
		name: 'sourceType',
		emptyText: '~~selectSourceType~~',
		editable: false,	
		queryMode: 'local',
		displayField: 'name',
		valueField: 'name',
		store: '@{sourceTypesStore}'	
	}, {
		xtype: 'textfield',
		name: 'sourceName',			
		hidden: '@{!sourceNameIsText}',	
		fieldLabel: '~~sourceValueEdit~~',
		emptyText: '~~enterSourceValue~~'
	}, {
		xtype: 'combobox',
		name: 'sourceName',
		hidden: '@{sourceNameIsText}',	
		fieldLabel: '~~sourceNameEdit~~',
		emptyText: '@{sourceNameEmptyText}',
		editable: '@{sourceNameIsEditable}',		
		queryMode: 'local',
		displayField: 'name',
		valueField: 'name',
		store: '@{inputParametersStore}'		
	}, {			
		xtype: 'button',
		name: 'addAssignment',		
		maxWidth: 140,		
		cls: 'rs-small-btn rs-btn-dark',		
		margin: '4 0 4 135'
	}]
});
Ext.define('RS.actiontaskbuilder.CodeEditorPanel', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.codeeditorpanel',
	margin: 0,
	padding: 0,

	// exporting getter/setter in order for glu binding to work
	config: {
		warningMsg: ''
	}
});
glu.defView('RS.actiontaskbuilder.Commit', {
	title: '~~commitTitle~~',
	height: 320,
	width: 600,
	modal: true,	
	padding : 15,
	cls : 'rs-modal-popup',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'textarea',
		labelAlign: 'top',
		name: 'comment',
		maxLength: 2000,
		enforceMaxLength: true,
		flex: 1
	}],
	buttons: [{
        text : '~~commit~~',
        cls : 'rs-med-btn rs-btn-dark',
        disabled : '@{!commitIsEnabled}',
        handler : '@{commit}',
    },{
        text : '~~cancel~~',
        cls : 'rs-med-btn rs-btn-light',
        handler : '@{cancel}',
    }]
})
glu.defView('RS.actiontaskbuilder.ConditionsEdit', {
	parentLayout: 'quickAccess',
	title: '@{title}',		
	cls: 'block-model subEditModel',
 	hidden: '@{hidden}',
 	header: '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '10 15',
	margin: '0 0 2.5 0',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: '@{items}',
	
	dockedItems: [{
		xtype: 'toolbar',		
		padding : '0 15 10',
		dock: 'bottom',
		items: ['->', {			
			name: 'close',
			cls: 'rs-med-btn rs-btn-dark'
		}]
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});	

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});

			panel.getEl().on('click', function() {
				if (panel._vm.collapsed && !panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	}
});
glu.defView('RS.actiontaskbuilder.ContentEdit', {
	parentLayout: 'quickAccess',
	xtype: 'codeeditorpanel',
	title: '@{title}',
	cls: '@{classList}',
 	hidden: '@{hidden}',
 	header: '@{header}',	
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '10 15 15 15',
	minHeight: '@{minHeight}',
	height: '@{height}',
	resizable: {
		handles: 's',
		pinned: true,
		minHeight: '@{minResize}',
		listeners: {
			resize: function (panel, width, height) {
				this.target.fireEvent('resizesection', this, height);
			}
		}
	},	

	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	defaults: {
		labelWidth: 130,
		margin: '0 0 10 0',
	},

	items: [{
		layout: {
			type: 'hbox'
		},
		items: [{
			labelWidth: 130,
			flex: 1,
			margin: '0 0 0 0',
			xtype: 'textfield',
			name: 'command'
		}, {
			xtype: 'button',
			hidden: '@{!commandIsVisible}',
			cls: 'rs-small-btn rs-btn-dark',
			name: 'addInputFile',
			margin: '0 0 0 10',
			height : '100%',
			padding: '0 10'
		}]
	}, {
		xtype: 'combobox',
		cls: 'no-input-border combo',
		editable: false,
		displayField: 'text',
		valueField: 'value',
		queryMode: 'local',
		name: 'syntax',
		value: '@{editorType}',
		maxWidth: 500,
		store: '@{syntaxStore}'
	}, {
		xtype: 'container',
		flex: '@{editorFlexValue}',
		
		layout: {
			type: 'hbox',
			align: 'stretch'
		},

		listeners: {
			afterlayout: function (panel) {
				if (panel.hidden) {
					this.ownerCt._vm.shrinkPanel();
				} else {
					this.ownerCt._vm.growPanel();
				}
			}
		},

		defaults: {
			margin: 0
		},
		
		items: [{
			xtype: 'textfield',
			labelWidth: 130,
			name:'contentLabel',
			fieldLabel: '@{contentLabel}',
			width: 135,
			hidden: '@{!syntaxIsVisible}'
		}, {
			xtype: 'AceEditor',			
			name: 'content',
			height: '@{editorHeight}',
			parser: '@{editorType}',	
			flex : 1,			
			listeners: {
				change: function () {
					this.up('codeeditorpanel').fireEvent('contentchange', this, this);
				}
			}
		}]
	}],	

	dockedItems: [{
		xtype: 'toolbar',		
		padding : '0 15 10',
		dock: 'bottom',
		items: ['->', {			
			name: 'close',
			cls: 'rs-med-btn rs-btn-dark'
		}]
	}],

	listeners: {
		contentchange: '@{contentChange}',
		resizesection: '@{resizeSection}',
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});	

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	}	
});
glu.defView('RS.actiontaskbuilder.ContentRead', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: 'block-model',
 	header: '@{header}',
 	hidden: '@{hidden}',
 	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '10 15 12 15',
	minHeight: '@{minHeight}',
	height: '@{height}',

	resizable: {
		handles: 's',
		pinned: true,
		minHeight: '@{minResize}'
	},	

	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	defaults: {
		labelWidth: 130,
		margin: '0 0 10 0'
	},

	items: [{
		xtype: 'displayfield',
		name: 'command',
		hidden: '@{!commandIsVisible}'
	}, {
		xtype: 'container',
		hidden: '@{contentIsEmpty}',
		flex: 1,
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		defaults: {
			margin: 0
		},
		items: [{
			xtype: 'textfield',
			labelWidth: 130,
			name:'contentLabel',
			fieldLabel: '@{contentLabel}',
			width: 135,
			hidden: '@{!commandIsVisible}'
		}, {
			xtype: 'AceEditor',
			parser: '@{editorType}',
			readOnly: true,
			height: '@{height}',
			name: 'content',		
			cls: 'editor-disabled',
			flex: 1,
			highlightActiveLine: false, 
			highlightGutterLine: false,
			showCursor: false		
		}]
	}, {
		xtype: 'displayfield',
		hidden: '@{!contentIsEmpty}',
		margin: 0,
		padding: 0,
		value: '@{contentEmptyText}'
	}],	

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},		
		viewRendered: '@{viewRenderCompleted}'
	}
});
glu.defView('RS.actiontaskbuilder.DetailsEdit', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: 'block-model subEditModel',
	hidden: '@{hidden}',
	header: '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: 15,
	margin: '0 0 2.5 0',

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		margin: 0,
		padding: 0
	},
	
	dockedItems: [{
		xtype: 'toolbar',		
		padding : '0 15 10',
		dock: 'bottom',
		items: ['->', {			
			name: 'close',
			cls: 'rs-med-btn rs-btn-dark'
		}]
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});	

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});

			panel.getEl().on('click', function() {
				if (panel._vm.collapsed && !panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	},
	items : [{
		xtype : 'component',
		html : '~~noDetailDefined~~',
		hidden : '@{!noRule}',
		margin: '10 0'
	},{
		xtype : 'component',
		html : '~~detailDisplayRule~~',
		margin : '0 0 10 0',
		hidden : '@{noRule}'
	},{
		xtype : 'grid',
		hidden : '@{noRule}',
		store : '@{ruleStore}',
		cls : 'rs-grid-dark summary-detail-grid',
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		columns : [{
			header : '~~severity~~',
			dataIndex : 'severity',
			align : 'center',
			width : 150,
			editor: {
				xtype: 'combo',				
				editable: false,			
				queryMode: 'local',
				displayField: 'name',
				valueField: 'name',
				store: '@{severityStore}',
				cls: 'no-input-border combo'				
			},
			renderer : function(value, metaData){			
				metaData.tdAttr = 'status=' + value.toUpperCase(); 
				return value;               	
			}
		},{
			header : '~~condition~~',
			dataIndex : 'condition',
			align : 'center',
			width : 150,
			editor: {
				xtype: 'combo',				
				editable: false,			
				queryMode: 'local',
				displayField: 'name',
				valueField: 'name',
				store: '@{conditionStore}',
				cls: 'no-input-border combo'				
			},
			renderer : function(value, metaData){			
				metaData.tdAttr = 'status=' + value.toUpperCase();
				return value;     
			}
		},{
			header : '~~textToDisplay~~',
			dataIndex : 'display',
			editor : 'textarea',
			flex : 1,
			renderer : RS.common.grid.plainRenderer()
		},{
			xtype: 'actioncolumn',		
			hideable: false,
			align: 'center',		
			width: 45,			
			resizable : false,
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record) {
					this.up('grid').fireEvent('removeRule', view, record);
				}
			}]
		}],
		listeners : {
			removeRule : '@{removeRule}'
		}
	},{		
		xtype : 'infoicon',
		displayText : '~~multipleRuleMessage~~',
		hidden: '@{!multipleRule}',
		margin: '8 0 0 0'
	},{
		xtype : 'fieldset',
		title: '~~detailRule~~',
		margin: '20 0 0 0',
		padding: '5 15',	
		layout: {
			type : 'hbox',
			align : 'stretch'
		},		
		items : [{
			xtype : 'container',
			flex : 1,
			layout: {
				type : 'vbox',
				align : 'stretch'
			},
			defaults : {
				labelWidth: 130,			
				margin: '4 0',
				editable : false
			},
			items : [{
				xtype : 'combobox',
				store : '@{severityStore}',
				displayField : 'name',
				valueField : 'name',
				name : 'severity'
			},{
				xtype : 'combobox',
				store : '@{conditionStore}',
				displayField : 'name',
				valueField : 'name',
				name : 'condition'
			},{
				xtype : 'textarea',
				height : 250,
				fieldLabel : '~~textToDisplay~~',
				name : 'display',
				editable : true
			},{
				xtype: 'container',			
				margin: '4 0 4 135',
				layout: {
					type: 'hbox'
				},
				defaults : {
					margin: '0 4 0 0',
					hideLabel : true
				},
				items: [{
					xtype: 'combobox',
					name: 'fieldType',
					emptyText: '~~selectType~~',
					editable: false,
					queryMode: 'local',
					displayField: 'text',
					valueField: 'value',	
					store: '@{typesStore}',
					width : 150
				}, {
					xtype: 'combobox',		
					name: 'fieldName',
					emptyText: '~~fieldNameTextForOthers~~',
					editable: '@{isFieldNameEditable}',
					queryMode: 'local',
					displayField: 'name',
					valueField: 'name',
					store: '@{parametersStore}',			
					flex: 1,
					listeners : {
						focus : function(){
							this.expand();
						}
					}				
				}, {
					xtype: 'button',
					cls : 'rs-small-btn rs-btn-dark',
					height : '100%',
					name :  'insertVariable',
					text : '~~insertVariable~~',
					margin : 0
				}]
			},{
				xtype : 'button',
				name : 'addRule',			
				maxWidth: 140,		
				cls : 'rs-small-btn rs-btn-dark',
				margin: '4 0 4 135'
			}]
		},{
			xtype : 'container',
			hidden : '@{..embed}',	
			flex : 1,
			margin : '44 0 0 50',
			layout : {
				type : 'vbox',
				align : 'stretch'
			},
			defaults : {
				margin : '4 0'
			},
			items : [{
				xtype : 'component',
				html : '~~previewLabel~~'
			},{
				xtype : 'component',			
				html : '@{preview}',
				overflowY : 'auto',
				height : 250,
				autoScroll : true,
				cls : 'summary-detail-preview',
				listeners : {
					afterrender : function(component){
						this._vm.on('renderPreview', function(preview){
							component.update(preview);
						})
					}										
				}			
			},{
				xtype : 'checkbox',
				name : 'htmlEncode',
				hideLabel : true,
				boxLabel : '~~htmlEncodeDetail~~'
			}]
		}]
	}]
});



glu.defView('RS.actiontaskbuilder.Execute', {
	asWindow: {
		width: 400,
		maxHeight: 500,
		title: '~~executeTitle~~',
		cls : 'rs-modal-popup',
		draggable: false,
		target: '@{target}',
		listeners: {
			render: function(panel) {
				if (panel.target) {
					Ext.defer(function() {
						panel.alignTo(panel.target, 'tr-br')
					}, 1)
				}
			}
		}
	},
	buttonAlign: 'left',
	buttons: [{
		name:'execute', 
		cls:'rs-proceed-button'
	},{
		name : 'cancel',
		cls : 'rs-small-btn rs-btn-light'
	}],
	padding : 10,

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'fieldcontainer',
		layout: 'hbox',
		hidden: '@{embedded}',
		items: [{
			xtype: 'radio',
			name: 'worksheetType',
			value: '@{newWorksheet}',
			boxLabel: '~~newWorksheet~~',
			hidden: '@{sirContext}'
		}, {
			xtype: 'radio',
			padding: '0px 0px 0px 10px',
			name: 'worksheetType',
			value: '@{activeWorksheet}',
			boxLabel: '~~activeWorksheet~~',
			hidden: '@{sirContext}'
		}]
	}, {
		xtype: 'checkbox',
		name: 'debug',
		hideLabel: true,
		boxLabel: '~~debug~~'
	}, {
		xtype: 'fieldcontainer',
		layout: 'hbox',
		items: [{
			xtype: 'checkbox',
			name: 'mock',
			hideLabel: true,
			boxLabel: '~~mock~~'
		}, {
			xtype: 'combo',
			padding: '0px 0px 0px 10px',
			flex: 1,
			name: 'mockName',
			store: '@{mockStore}',
			editable: '@{mockEditable}',
			displayField: 'uname',
			valueField: 'uname',
			queryMode: 'local',
			hideLabel: true
		}]
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		title: '~~parameters~~',
		name: 'params',
		margin : '0 0 10 0',
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items :  ['addParam','removeParams']
		}],
		store: '@{paramStore}',
		columns: '@{paramColumns}',
		features: [{
			ftype: 'grouping',
			groupHeaderTpl: [
				'{name:this.formatName}',
				{
					formatName: function(name) {
						if (name) {
							return name+'S';
						} else {
							return 'PARAMS'
						}
					}
				}
			]
		}],
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 2
		}],
		viewConfig: {
			markDirty: false
		},
		flex: 1,
		listeners: {
			render: function(grid) {
				grid.store.grid = grid;
				grid.on('celldblclick', function(comp ,td, cellIndex, record, tr, rowIndex, e, eOpts )  {
					if (!this._vm.editParamsIsEnabled) {
						return false;
					}
					var param = this.store.getAt(rowIndex);
					if (param.get('encrypt') && param.get('value') == '*****') {
						param.set('value', '');
						param.set('origValue', '');
						param.set('modified', true);
					}
				})				
			}
		}
	}]
})
glu.defView('RS.actiontaskbuilder.Expression', {
	xtype: 'container',
	cls: 'expression-container',
	defaults: {
		padding: 5
	},
	items: [{
		xtype: 'container',
		layout: 'hbox',
		cls: 'expression',
		defaults: {
			margin: '0 5 0 0',
			padding: 0,
		},
		items: [{
			xtype: 'displayfield',
			fieldCls: 'expression-field',
			hidden: '@{isExpressionHidden}',
			value: '@{expression}',
			fieldCls: 'expression-text',
			renderer: RS.common.grid.plainRenderer(),
			listeners: {
				render: function (c) {
					c.getEl().on('click', function () {
		                this.fireEvent('edit', this);
		            }, this);
				},
				edit: '@{edit}'
			}
		}, {
			xtype: 'image',
			hidden: '@{isRemoveHidden}',
		    src: '/resolve/css/resources/images/remove.png',
		    height: '21px',
			width: '28px',
			margin: '0 5 0 5',
			style: {
				verticalAlign: 'bottom'
			},
		    listeners: {
		        render: function (c) {
		            c.getEl().on('click', function () {
		                this.fireEvent('remove', this);
		            }, this);
		        },
		        remove: '@{remove}'
		    }
		}, {
			xtype: 'displayfield',
			fieldCls: 'expression-text-bold',
			value: 'OR',
			hidden: '@{isOrHidden}',
			margin: '0 0 0 5'
		}, {
			xtype: 'image',
			hidden: '@{isAddHidden}',
			src: '/resolve/css/resources/images/add.png',
		    height: '21px',
			width: '28px',
			margin: 0,
			style: {
				verticalAlign: 'bottom'
			},
		    listeners: {
		        render: function (c) {
		            c.getEl().on('click', function () {
		                this.fireEvent('add', this);
		            }, this);
		        },
		        add: '@{add}'
		    }
		}]
	}, {
		xtype: 'form',
		cls: 'expression-form',
		hidden: '@{isFormHidden}',
		defaults: {
			width: 400,
			margin: '0 0 10 0',
			padding: 0,
			queryMode: 'local',
			displayField: 'text',
			valueField: 'value'			
		},
		padding: '10 10 0 10',
		margin: 10,
		items: [{
			xtype: 'combobox',
			name: 'category',
			store: '@{categoryStore}',
			editable: false,
			hidden: '@{addMode}'
		}, {
			xtype: 'combobox',
			name: 'criteria',
			fieldLabel: '~~determiner~~',
			store: '@{determinerStore}',
			editable: false
		}, {
			xtype: 'combobox',
			name: 'leftOperandType',
			fieldLabel: '~~operandType~~',
			emptyText: '~~selectType~~',
			store: '@{operandTypeStore}',
			editable: false			
		}, {
			xtype: 'textfield',
			name: 'leftOperandName',
			fieldLabel: '~~operand~~',
			hidden: '@{!leftOperandIsText}',		
			emptyText: '~~enterOperandValue~~',			
		}, {
			xtype: 'combobox',
			name: 'leftOperandName',
			fieldLabel: '~~operand~~',
			hidden: '@{leftOperandIsText}',		
			emptyText: '~~selectOperand~~',
			store: '@{leftOperandStore}',
			editable: '@{leftOperandEditable}',
			displayField: 'name',
			valueField: 'name',	
			listeners : {
				afterrender : function(){
					this.setRawValue(this._vm.leftOperandName);
				}
			}	
		}, {
			xtype: 'combobox',
			name: 'operator',
			valueField : 'value',
			emptyText: '~~selectOperator~~',
			store: '@{operatorStore}',
			editable: false
		}, {
			xtype: 'combobox',
			name: 'rightOperandType',
			fieldLabel: '~~operandType~~',
			emptyText: '~~selectType~~',
			store: '@{operandTypeStore}',
			editable: false,			
		}, {
			xtype: 'textfield',
			name: 'rightOperandName',
			fieldLabel: '~~operand~~',
			hidden: '@{!rightOperandIsText}',		
			emptyText: '~~enterOperandValue~~'			
		}, {
			xtype: 'combobox',
			name: 'rightOperandName',
			fieldLabel: '~~operand~~',
			hidden: '@{rightOperandIsText}',		
			emptyText: '~~selectOperand~~',
			store: '@{rightOperandStore}',
			editable: '@{rightOperandEditable}',
			displayField: 'name',
			valueField: 'name',	
			listeners : {
				afterrender : function(){
					this.setRawValue(this._vm.rightOperandName);
				}
			}			
		}],

		dockedItems: [{
			xtype: 'toolbar',
			dock: 'bottom',
			margin: '0 0 10 0',
			padding: 0,
			items: ['->',{
				name:'done',		
				hidden : '@{addMode}',
				cls: 'rs-small-btn rs-btn-dark',
				itemId: 'doneBtn'
			},{
				name:'done',
				text : '~~add~~',
				hidden : '@{!addMode}',			
				cls: 'rs-small-btn rs-btn-dark',
			},{
				name:'cancel',
				cls: 'rs-small-btn rs-btn-light'
			}]
		}]
	}]
});
glu.defView('RS.actiontaskbuilder.ExpressionBlock', {
	xtype: 'container',
	layout: { 
		type: 'column'
	},
	cls: 'expression-block',
	items: '@{expressions}',
	border: 1,
	shrinkWrap: 3,
	flex: 1,
	style: {
		borderColor: '#aaa',
		borderStyle: 'solid'
	}
});
glu.defView('RS.actiontaskbuilder.ExpressionBlockContainer', {
	xtype: 'fieldset',
	cls: 'expression-block-container-fieldset',
	title: '@{title}',
	border: 2,
	style: '@{style}',
	hidden: '@{isHidden}',
	margin: '0 0 10 0',
	items: [{
		xtype: 'container',
	    layout: {
	    	type: 'hbox',
	    	align: 'stretchmax'
	    },
	    cls: 'expression-block-container-fieldset-inner',
	    defaults: {
	        margin: '5 10 10 10'
	    },
	    items: [{
	        xtype: 'displayfield',
	        fieldStyle: {
	            fontWeight: 'bold'
	        },
	        hidden: '@{isAndHidden}',
	        height: '100%',
	        value: '~~AND~~'
	    }, {
			xtype: 'container',
			flex: 1,
			cls: 'expression-block-container',
			items: '@{blocks}'
		}]
	}],
	listeners : {
		afterrender : function(panel){
			this._vm.on('onClear', function(){
				panel.clearManagedListeners();
			})
		}
	}
});
glu.defView('RS.actiontaskbuilder.GeneralEdit', {
	parentLayout: 'quickAccess',
	xtype: 'form',
	title: '@{title}',
	cls: 'block-model subEditModel',
 	hidden: '@{hidden}',
 	header: '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '10 15 15 15',
	margin: '0 0 2.5 0',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		labelWidth: 130,
		margin: '4 0',
		padding: 0
	},
	generalEditTabContrl: '@{generalEditTabContrl}',
	setGeneralEditTabContrl: function() {
		// action task w/o an id means it is being created
		var hideOrShow = !this._vm.rootVM.id? 'hide': 'show';
		[
			this.down('#generalProperties'),
			this.down('#rolesEdit'),
			this.down('#optionsEdit')
		].forEach(function(tab) {
			// hide (show) General, Roles and Options tab on creating new (opening existing) task.
			tab? tab[hideOrShow].call(tab): tab;
		})

	},
	listeners: {
		afterrender: function (panel) {
			this.fireEvent('setform', this, this.form);
			this.fireEvent('afterRendered', this, this);

			if (!this._vm.isExistingActionTask) {
				this.form.reset();
			}

			this.form.isValid();

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize && !panel._vm.isNewActionTask) {
					panel._vm.close();
				}
			});

			panel.getEl().on('click', function() {
				if (panel._vm.collapsed && !panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});
		},

		setform: '@{setForm}',
		validitychange: '@{validityChange}',
		afterRendered: '@{viewRenderCompleted}'
	},

	dockedItems: [{
		xtype: 'toolbar',
		dock: 'bottom',
		padding : '0 15 10',
		items: ['->', {
			name: 'close',
			hidden: '@{isNewActionTask}',
			formBind: true,
			cls: 'rs-med-btn rs-btn-dark'
		}, {
			text: '~~save~~',
			hidden: '@{!isNewActionTask}',
			disabled: '@{!saveBtnIsEnabled}',
			cls: 'rs-med-btn rs-btn-dark',
			handler : '@{close}'
		}]
	}],

	items: [{
		xtype: 'textfield',
		name: 'namespace',
		hidden: '@{isExistingActionTask}',
		disabled: '@{isExistingActionTask}',	
		listeners: {			
			change: function (x, newValue) {
				this.fireEvent('namespacechangehandler', this, this, newValue);
			},
			namespacechangehandler: '@{namespaceChangeHandler}',
			blur: '@{namespaceBlurHandler}'
		}
	}, {
		xtype: 'displayfield',
		margin: 0,
		padding: 0,
		name: 'namespace',
		hidden: '@{isNewActionTask}',
		htmlEncode : true
	}, {
		xtype: 'textfield',
		name: 'menuPath',
		allowBlank: false,		
		validateOnBlur: false,
		listeners: {
			change: '@{menuPathChangeHandler}',
			blur: function () {
				this.fireEvent('menupathblurhandler', this, this);
			},
			menupathblurhandler: '@{menuPathBlurHandler}'
		}
	}, {
		xtype: 'textfield',
		name: 'name',
		hidden: '@{isExistingActionTask}',
		disabled: '@{isExistingActionTask}',	
	}, {
		xtype: 'displayfield',
		margin: 0,
		padding: 0,
		name: 'name',
		htmlEncode : true,
		hidden: '@{isNewActionTask}'
	}, {
		xtype: 'combobox',
		name: 'type',
		hidden: '@{isTypeComboHidden}',
		disabled: '@{isTypeComboHidden}',
		cls: 'no-input-border combo',
		editable: false,
		forceSelection: true,
		displayField: 'text',
		valueField: 'value',
		queryMode: 'local',
		value : '@{type}',
		maxWidth: 500,
		store: '@{typeStore}'
	}, {
		xtype: 'displayfield',
		name: 'type',
		hidden: '@{isProcessFieldHidden}',
		margin: '0 0 5 0',
		padding: 0
	}, {
		xtype: 'numberfield',
		cls: 'no-input-border combo',
		name: 'timeout',
		maxWidth: 500,
		minValue: 1,
		listeners: {
			blur: '@{notifyDirty}'
		}
	}, {
		xtype: 'fieldcontainer',
		layout: 'hbox',
		items: [{
			xtype: 'triggerfield',
			name: 'assignedToUserName',
			fieldLabel: '~~assignedTo~~',
			labelWidth: 130,
			flex: 1,
			editable: false,
			allowBlank: true,
			cls: 'no-input-border combo',
			triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
			maxWidth: 500,
			onTriggerClick: function() {
				this.fireEvent('handleTriggerClick', this);
			},
			listeners: {
				handleTriggerClick: '@{assignedToClicked}',
				blur: '@{notifyDirty}'
			}
		}, {
			xtype: 'image',
			cls: 'rs-btn-edit',
			margin: '6 0 0 10',
			hidden: '@{jumpToAssignedToIsHidden}',
			listeners: {
				handler: '@{jumpToAssignedTo}',
				render: function(image) {
					image.getEl().on('click', function() {
						image.fireEvent('handler', this);
					}, this);
				}
			}
		}]
	}, {
		xtype: 'textfield',
		name: 'summary',
		allowBlank: true,
		maxLength: 300,
		enforceMaxLength: true,
		listeners: {
			blur: '@{notifyDirty}'
		}
	}, {
		xtype: 'container',

		layout: {
			type: 'hbox',
			align: 'stretch'
		},

		items: [{
			xtype: 'label',
			text: '~~generalDescription~~',
			margin: '0 5 0 0',
			padding: 0,
			width: 130
		}, {
			xtype: 'container',
			margin: 0,
			padding: 0,
			flex: 1,
			layout: 'fit',

			resizable: {
				handles: 's',
				pinned: true
			},

			items: [{
				xtype: 'textarea',
				maxLength: 4000,
				enforceMaxLength: true,
				margin: 0,
				flex: 1,
				name: 'description',
				hideLabel: true,
				allowBlank: true,
				listeners: {
					blur: '@{notifyDirty}'
				}
			}]
		}]
	}, {
		xtype: 'checkbox',
		name: 'active',
		hideLabel: true,
		uncheckedValue: '',
		boxLabel: '~~active~~',
		margin: '0 0 3 135'
	}, {
		xtype: 'checkbox',
		name: 'logResult',
		hideLabel: true,
		uncheckedValue: '',
		boxLabel: '~~logResult~~',
		margin: '0 0 3 135'
	}, {
		xtype: 'checkbox',
		name: 'displayInWorksheet',
		hideLabel: true,
		uncheckedValue: '',
		boxLabel: '~~displayInWorksheet~~',
		margin: '0 0 3 135'
	}]
});

glu.defView('RS.actiontaskbuilder.GeneralRead', {
	parentLayout: 'quickAccess',
	itemId: 'GeneralRead',
	title: '@{title}',
	cls: 'block-model',
	hidden: '@{hidden}',
 	header: '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '10 15 15 15',
	defaults: {
		labelWidth: 130,
		margin: 0,
		padding: 0
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'displayfield',
		name: 'namespace',
		htmlEncode : true
	}, {
		xtype: 'displayfield',
		name: 'name',
		htmlEncode : true
	}, {
		xtype: 'displayfield',
		fieldLabel: '~~type~~',
		name: 'type'
	}, {
		defaults: {
			labelWidth: 130,
		},
		items: [{
		xtype: 'container',
		layout: 'hbox',
		items:[{
			xtype: 'displayfield',
			name: 'assignedToUserName',
			fieldLabel: '~~assignedTo~~',
			labelWidth: 130,
			border: 'none'
		}, {
			xtype: 'image',
			cls: 'rs-btn-edit',
			hidden: '@{jumpToAssignedToIsHidden}',
			listeners: {
				handler: '@{jumpToAssignedTo}',
				render: function(image) {
					image.getEl().on('click', function() {
						image.fireEvent('handler', this);
					}, this);
				}
			}
		}]
	}, {
		xtype: 'displayfield',
		cls: 'summary-displayfield',
		name: 'summary',
		padding: '0 0 5 0',
		renderer: RS.common.grid.plainRenderer()
	}, {
		xtype: 'displayfield',
		cls: 'description-displayfield',
		fieldLabel: '~~description~~',
		name: 'description',
		html: '@{description}',
		htmlEncode : false
	}, {
		xtype: 'checkbox',
		name: 'active',
		margin: '0 0 5 135',
		hideLabel: true,
		readOnly: true,
		boxLabel: '~~active~~'
	}, {
		xtype: 'checkbox',
		name: 'logResult',
		margin: '0 0 5 135',
		hideLabel: true,
		readOnly: true,
		boxLabel: '~~logResult~~'
	}, {
		xtype: 'checkbox',
		name: 'displayInWorksheet',
		margin: '0 0 5 135',
		hideLabel: true,
		readOnly: true,
		boxLabel: '~~displayInWorksheet~~'
		}]
	}, {
		xtype: 'fieldset',
		title : '~~rolesReadTitle~~',
		margin: '0 0 10 0',
		padding: '10 15 15 15',
		items: [{
			xtype: 'checkbox',
			name: 'defaultRoles',
			margin: 0,
			hideLabel: true,
			readOnly: true,
			boxLabel: '~~usesDefaultRoles~~'
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			hidden: '@{hideAccessRights}',
			name: 'right',
			store: '@{accessStore}',
			emptyText: '~~rolesEmptyText~~',
			viewConfig: {
				deferEmptyText: false
			},
			columns: [{
				dataIndex: 'uname',
				filterable: true,
				flex: 5,
				header: '~~roleName~~',
				hideable: false,
				sortable: true
			}, {
				renderer: RS.common.grid.booleanRenderer(),
				align: 'center',
				dataIndex: 'read',
				filterable: true,
				flex: 1,
				header: '~~roleReadRight~~',
				hideable: false,
				sortable: true
			}, {
				renderer: RS.common.grid.booleanRenderer(),
				align: 'center',
				dataIndex: 'write',
				filterable: true,
				flex: 1,
				header: '~~roleWriteRight~~',
				hideable: false,
				sortable: true
			}, {
				renderer: RS.common.grid.booleanRenderer(),
				align: 'center',
				dataIndex: 'execute',
				filterable: true,
				flex: 1,
				header: '~~roleExecuteRight~~',
				hideable: false,
				sortable: true
			}, {
				renderer: RS.common.grid.booleanRenderer(),
				align: 'center',
				dataIndex: 'admin',
				filterable: true,
				flex: 1,
				header: '~~roleAdminRight~~',
				hideable: false,
				sortable: true,
				listeners: {
					afterrender: function (column) {
						if (this.up('grid')._vm.hideAdminColumn) {
							column.hide();
						} else {
							column.show();
						}
					}
				}
			}]
		}]
	}, {
		xtype: 'fieldset',
		title: '~~optionsReadTitle~~',
		margin: 0,
		padding: '10 15 15 15',
		items: [{
			xtype: 'displayfield',
			hidden: '@{hasOptions}',
			margin: 0,
			padding: 0,
			value: '~~optionsEmptyText~~'
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			hidden: '@{!hasOptions}',
			store: '@{optionsStore}',
			emptyText: '~~optionsEmptyText~~',
			viewConfig: {
				deferEmptyText: false
			},
			columns: [{
				dataIndex: 'uname',
				filterable: true,
				flex: 1,
				header: '~~uname~~',
				hideable: false,
				sortable: true
			}, {
				header: '~~uvalue~~',
				dataIndex: 'uvalue',
				filterable: false,
				flex: 2,
				sortable: true,
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~udescription~~',
				dataIndex: 'udescription',
				filterable: false,
				flex: 3,
				sortable: true,
				renderer: RS.common.grid.plainRenderer()
			}]
		}]
	}],

	listeners: {
		afterrender: function(panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	}
});
glu.defView('RS.actiontaskbuilder.GeneralReference',{
	autoScroll : true,
	layout : 'fit',
	store: '@{referencesStore}',
	padding: '10',
	bodyPadding: '10 0 0 0',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{referenceTitle}'
		}]
	}],
	items: [{
		id: 'referencesTab',
		autoScroll: true,
		xtype: 'grid',
		name: 'references',
		store: '@{referencesStore}',
		columns: [{
			header: '~~name~~',
			dataIndex: 'name',
			hideable: false,
			filterable: false,
			sortable: true,
			flex: 1
		}, {
			header: '~~refInContent~~',
			dataIndex: 'refInContent',
			filterable: false,
			width: 100,
			renderer: RS.common.grid.booleanRenderer()
		}, {
			header: '~~refInMain~~',
			dataIndex: 'refInMain',
			filterable: false,
			width: 100,
			renderer: RS.common.grid.booleanRenderer()

		}, {
			header: '~~refInAbort~~',
			dataIndex: 'refInAbort',
			filterable: false,
			width: 100,
			renderer: RS.common.grid.booleanRenderer()
		}],
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTooltip: RS.common.locale.viewColumnTooltip,
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'name'
		},
		listeners: {
			editAction: '@{editReference}'
		}
	}]
});

glu.defView('RS.actiontask.ActionTask',{
	bodyPadding : '@{bodyPadding}',
	dockedItems: [{		
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		padding : '10 15 4 15',
		hidden: '@{isembedded}',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{actionTaskTitle}',
			htmlEncode : false
		},
		{
			xtype: 'tbtext',
			text: '@{versionTxt}'
		},
		{
			xtype: 'tbtext',
			text: '~~readOnlyTxt~~',
			hidden: '@{!readOnlyMode}'
		},
		'->', {
			text: '',
			cls: 'rs-execute-button',
			iconCls: 'rs-social-button icon-play',
			tooltip: '~~executionTooltip~~',
			disabled: '@{!isExecutable}',
			handler: function(button) {
				button.fireEvent('execution', button, button)
			},
			listeners: {
				execution: '@{execution}'
			}
		}, {
			name: 'save',
			iconCls: 'rs-social-button icon-save',
			text: '',
			tooltip: '~~save~~',
			disabled: '@{!saveIsEnabled}',
			hidden: '@{readOnlyMode}'
		}, {
			name: 'edit',
			iconCls: 'rs-social-button icon-pencil',
			text: '',
			tooltip: '~~edit~~',
			disabled: '@{!editIsEnabled}',
			hidden: '@{!readOnlyMode}'
		}, {
			name: 'exitToView',
			iconCls: 'rs-social-button icon-reply-all',
			style: {
				width: '25px'
			},
			text: '',
			tooltip: '~~exitToView~~',
			hidden: '@{readOnlyMode}'
		}, 
		/*
		{
			name: 'expandAll',
			text: '',
			iconCls: 'rs-social-button enlarge120 icon-expand-alt',
			tooltip: '~~expandAll~~',
			hidden: '@{expandAllDisabled}'
		}, {
			name: 'collapseAll',
			text: '',
			iconCls: 'rs-social-button enlarge120 icon-collapse-alt',
			tooltip: '~~collapseAll~~',
			hidden: '@{collapseAllDisabled}'
		}, 
		*/
		{
			text: '',
			iconCls: 'rs-social-button icon-repeat',
			tooltip: '~~reload~~',
			disabled: '@{!saveAsRenameIsEnabled}',
			handler: function() {
				this.fireEvent('reload', this);
			},
			listeners: {
				reload: '@{reload}',
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}, 
	{
		xtype: 'toolbar',
		padding : '4 15',
		dock: 'top',
		cls: 'actionBar actionBar-form',		
		hidden: '@{hideNewActionTaskToolBar}',
		defaults : {
			cls: 'rs-toggle-btn rs-small-btn' 
		},
		items: [{	
			iconCls: 'rs-icon-button icon-file-text',
			name: 'fileOptions',
			cls : 'rs-small-btn rs-btn-light',			
			menu: [{
				name: 'save',
				disabled: '@{!saveIsEnabled}',
				hidden: '@{readOnlyMode}',
			}, {
				xtype: 'menuseparator',
				width: 200
			}, {
				name: 'accessActiontTaskList',
				text: '~~cancel~~'
			}]
		}]
	}, {
		xtype: 'toolbar',
		padding : '4 15',
		dock: 'top',
		cls: 'actionBar actionBar-form',		
		hidden: '@{hideActionTaskToolBar}',
		defaults : {
			cls: 'rs-toggle-btn rs-small-btn' 
		},
		items: [{	
			iconCls: 'rs-icon-button icon-file-text',
			name: 'fileOptions',
			cls : 'rs-small-btn rs-btn-light',			
			menu: [{
				name: 'save',
				disabled: '@{!saveIsEnabled}',
				hidden: '@{readOnlyMode}',
			}, {
				name: 'saveAs',
				disabled: '@{!saveAsRenameIsEnabled}',
				hidden: '@{readOnlyMode}',
			}, {
				xtype: 'menuseparator',
				hidden: '@{readOnlyMode}',
			}, {
				name: 'commit',
				hidden: '@{readOnlyMode}',
			}, {
				name: 'viewVersionList',
				hidden: '@{readOnlyMode}',
			}, {
				name: 'edit',
				disabled: '@{!editIsEnabled}',
				hidden: '@{!readOnlyMode}'
			}, {
				name: 'rename',
				disabled: '@{!saveAsRenameIsEnabled}',
				hidden: '@{!readOnlyMode}',
			}, {
				xtype: 'menuseparator',
				width: 200
			}, {
				name: 'expandAll',
				hidden: '@{expandAllDisabled}'
			}, {
				name: 'collapseAll',
				hidden: '@{collapseAllDisabled}'
			}, {
				xtype: 'menuseparator',
				hidden: '@{expandCollapseSeparatorHidden}'
			},
			/* 
			{
				name: 'reference', 
				disabled: '@{isNew}'
			}, 
			*/
			{
				name: 'sysInfo',
				disabled: '@{isNew}'
			}, {  
				text: '~~toolbarSettings~~',
				disabled: '@{!toolbarSettingsIsEnabled}',
				menu: [{
					name: 'showSectionNone',
					iconCls: '@{iconShowSectionNone}',
				}, {
					name: 'showSectionIconOnly',
					iconCls: '@{iconShowSectionIconOnly}',
				}, {
					name: 'showSectionTextOnly',
					iconCls: '@{iconShowSectionTextOnly}',
				}, {
					name: 'showSectionTextIcon',
					iconCls: '@{iconShowSectionIconAndText}',
				}] 
			}, {
				xtype: 'menuseparator'
			}, {
				name: 'exitToView',
				text: '~~exitToView~~',
				hidden: '@{readOnlyMode}',
				disabled: '@{!exitToViewIsEnabled}',
			}, {
				//iconCls: 'rs-social-menu-button icon-reply-all',
				name: 'accessActiontTaskList',
				text: '~~viewActionTaskList~~'
			}]
		},
		/*
		{
			xtype: 'tbseparator',
			hidden: '@{expandCollapseSeparatorHidden}'
		}, {
			name: 'expandAll',
			iconCls: 'rs-social-button icon-expand-alt',
			hidden: '@{expandAllDisabled}'
		}, {
			name: 'collapseAll',
			iconCls: 'rs-social-button icon-collapse-alt',
			hidden: '@{collapseAllDisabled}'
		},
		*/
		{
			xtype: 'tbseparator',
		}, {
			name: 'allBtn',
			text: '@{allBtnText}',
			iconCls: '@{allBtnIcon}',
			tooltip: '@{allBtnTooltip}',
		}, {
			name: 'generalBtn',
			text: '@{generalBtnText}',
			iconCls: '@{generalBtnIcon}',
			tooltip: '@{generalBtnTooltip}',
		},
		{
			name: 'referenceBtn',
			text: '@{referenceBtnText}',
			iconCls: '@{referenceBtnIcon}',
			tooltip: '@{referenceBtnTooltip}',
		}, 
		{
			name: 'parametersBtn',
			text: '@{parametersBtnText}',
			iconCls: '@{parametersBtnIcon}',
			tooltip: '@{parametersBtnTooltip}',
		}, {
			name: 'preprocessorBtn',
			text: '@{preprocessorBtnText}',
			iconCls: '@{preprocessorBtnIcon}',
			tooltip: '@{preprocessorBtnTooltip}',
		}, {
			name: 'contentBtn',
			text: '@{contentBtnText}',
			iconCls: '@{contentBtnIcon}',
			tooltip: '@{contentBtnTooltip}',
			hidden: '@{contentBtnDisabled}',
		}, {
			name: 'parserBtn',
			text: '@{parserBtnText}',
			iconCls: '@{parserBtnIcon}',
			tooltip: '@{parserBtnTooltip}',
		}, {
			name: 'assessBtn',
			text: '@{assessBtnText}',
			iconCls: '@{assessBtnIcon}',
			tooltip: '@{assessBtnTooltip}',
		}, {
			name: 'outputsBtn',
			text: '@{outputsBtnText}',
			iconCls: '@{outputsBtnIcon}',
			tooltip: '@{outputsBtnTooltip}',
		}, {
			name: 'severityConditionsBtn',
			text: '@{severityConditionsBtnText}',
			iconCls: '@{severityConditionsBtnIcon}',
			tooltip: '@{severityConditionsBtnTooltip}',
		}, {
			name: 'summaryDetailsBtn',
			text: '@{summaryDetailsBtnText}',
			iconCls: '@{summaryDetailsBtnIcon}',
			tooltip: '@{summaryDetailsBtnTooltip}',
		}, {
			name: 'mockBtn',
			text: '@{mockBtnText}',
			iconCls: '@{mockBtnIcon}',
			tooltip: '@{mockBtnTooltip}',
		}, {
			name: 'tagsBtn',
			text: '@{tagsBtnText}',
			iconCls: '@{tagsBtnIcon}',
			tooltip: '@{tagsBtnTooltip}',
		}]
	}],		
	overflowY: 'auto',		
	layout:{
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		margin: '0 0 5 0',
	},
	items: '@{itemList}',
	viewOnly: '@?{..viewOnly}',
	listeners: {
		afterrender: function(view) {
			this.fireEvent('afterRendered', this, view);			
		},	
		afterRendered: '@{afterRendered}'
	}
});

glu.defView('RS.actiontaskbuilder.MockEdit', {
	parentLayout: 'quickAccess',
	title: '@{title}',		
	cls: 'block-model',
 	header: '@{header}',
	hidden: '@{hidden}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: 15,	
	dockedItems: [{
		xtype: 'toolbar',	
		padding : '10 15 0 15',	
		items: [{ 
			name:'closeDetails', 
			cls: 'rs-small-btn rs-btn-dark',		
		}]
	}, {
		xtype: 'toolbar',		
		padding : '0 15 10',
		dock: 'bottom',
		items: ['->', {			
			name: 'close',
			cls: 'rs-med-btn rs-btn-dark'
		}]
	}],	
	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	
	items: [{
		name: 'Mock',
		xtype: 'container',
		items: [{
			xtype: 'displayfield',
			hidden: '@{!gridIsEmpty}',
			value: '~~emptyMockMsg~~'
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			hidden: '@{gridIsEmpty}',
			store: '@{mockStore}',
			listeners: {				
				afterrender: function(grid) {
					grid.fireEvent('rendercompleted', grid, grid);
				},
				rendercompleted: '@{mockRenderCompleted}',
				editmockdetails: '@{editMockDetails}',
				recordremoved: '@{recordRemoved}',
				validateedit: function(editor, context){
					return this._vm.validateNameOnEdit(context, 'uname', 'mockStore');
				}					
			},
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}],
			selModel: {
				selType: 'cellmodel'
			},
			userDateFormat: '@{..userDateFormat}',
			editDetailsTooltip: '@{editDetailsTooltip}',
			removeMockTooltip: '@{removeMockTooltip}',
			columns: [{
				xtype: 'actioncolumn',			
				hideable: false,
				sortable: false,
				align: 'center',			
				width: 45,			
				iconCls: 'rs-fa-details',
				items: [{
					getTip: function(v, meta, record) {
						return this.up('grid').editDetailsTooltip;
					},
					handler: function(view, rowIndex, colIndex, item, e, record ) {
						this.up('grid').fireEvent('editmockdetails', view, record);
					}
				}]
			}, {
				header: '~~name~~',
				dataIndex: 'uname',					
				hideable: false,
				sortable: true,
				flex: 1,
				editor: {
					xtype:'textfield',
					grow: true,
					flex: 1
				},
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~udescription~~',
				dataIndex: 'udescription',
				editor: {
					xtype: 'textfield',
					allowBlank: true
				},
				hideable: false,
				sortable: true,
				flex: 3,
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~sysUpdatedOn~~',
				dataIndex: 'sysUpdatedOn',
				hideable: false,
				sortable: true,
				flex: 1,
				renderer: function(value) {
					if (value) {
						return Ext.Date.format(new Date(value), this.userDateFormat);
					}
				}
			}, {
				header: '~~sysUpdatedBy~~',
				dataIndex: 'sysUpdatedBy',
				hideable: false,
				sortable: true,
				flex: 1,
				renderer: RS.common.grid.plainRenderer()
			}, {
				xtype: 'actioncolumn',			
				hideable: false,
				sortable: false,
				align: 'center',				
				width: 45,
				items: [{
					glyph: 0xF146,
					tooltip : 'Remove',
					handler: function(view, rowIndex, colIndex, item, e, record) {
						this.up('grid').fireEvent('recordremoved', view, record);
					}
				}]
			}]
		}, {
			xtype: 'fieldset',
			title : '~~addMock~~',
			margin: '10 0 5 0',
			padding: 15,
			items: [{
				xtype: 'form',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaults: {
					labelWidth: 130,
					margin: '0 0 10 0'
				},
				items: [{
					xtype: 'textfield',
					name: 'uname',
					allowBlank: false,
					maxWidth: 500
				}, {
					xtype: 'textarea',
					name: 'udescription',
					margin: '0 0 7 0'
				}, {
					xtype: 'button',
					name : 'addMock',						
					cls: 'rs-small-btn rs-btn-dark',							
					margin: '0 0 0 135',
					maxWidth: 100,				
				}]
			}]
		}]
	}, {
		name: 'MockDetails',
		xtype: 'container',
		margin: 0,
		padding: 0,

		layout: {
			type : 'vbox',
			align: 'stretch'
		},

		defaults: {
			labelWidth: 130,
			margin: '0 0 10 0',
			padding: 0			
		},

		items: [{
			xtype: 'displayfield',
			name: 'mockName',
			margin: '0 0 2 0',
			fieldLabel: '~~name~~'
		}, {
			xtype: 'displayfield',
			fieldLabel: '~~mockDescription~~',
			value: '@{mockDescription}',
			hidden: '@{!hasMockDescription}',
			margin: 0,
			renderer: RS.common.grid.plainRenderer()
		}, {
			xtype: 'displayfield',
			fieldLabel: '~~mockDescription~~',
			value: '~~noMockDescription~~',
			hidden: '@{hasMockDescription}',
			margin: 0
		}, {
			xtype: 'displayfield',
			margin: '0 0 2 0',
			fieldLabel: '~~parameters~~',
			value: '@{parametersMesg}',
		}, {
			xtype: 'grid',
			cls: 'rs-grid-dark',
			hidden: '@{parametersGridIsEmpty}',
			store: '@{mockDetailsStore}',
			maxHeight: 300,
			flex: 1,
			margin: '-20 0 5 135',
			listeners: {
				validateedit: function(editor, context){
					return this._vm.validateNameOnEdit(context, 'name', 'mockDetailsStore');
				},		
				beforeedit: function (editor, context) {
					this.fireEvent('populatedatasources', this, context);
				},
				afterrender: function(grid) {
					grid.fireEvent('rendercompleted', grid, grid);
				},
				populatedatasources: '@{populateMockDetailsDataSources}',
				rendercompleted: '@{mockDetailsRenderCompleted}',
				recordparamremoved: '@{recordParamRemoved}'
			},

			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}],

			selModel: {
				selType: 'cellmodel'
			},

			removeMockTooltip: '@{removeMockTooltip}',
			columns: [{
				header: '~~type~~',
				dataIndex: 'type',
				itemId: 'mockDetailsEditTypeCombo',
				hideable: false,
				sortable: true,
				flex: 1,
				editor: {
					xtype: 'combo',
					allowBlank : false,
					editable: false,
					emptyText: '~~selectType~~',
					queryMode: 'local',
					displayField: 'name',
					valueField: 'name',
					store: '@{typeStore}',
					listeners: {
						focus: function () {
							this.fireEvent('mockdetailsedittypefocus');
						},
						select: function (combo, records) {
							this.fireEvent('mockdetailsedittypeselected', this, combo, records);
						},
						mockdetailsedittypefocus: '@{mockDetailsEditTypeFocus}',
						mockdetailsedittypeselected: '@{mockDetailsEditTypesSelected}'
					}
				}
			}, {
				header: '~~name~~',
				dataIndex: 'name',
				itemId: 'mockDetailsEditNameCombo',
				editor: {
					xtype: 'combo',
					allowBlank : false,
					editable: '@{isMockDetailsEditNameEditable}',
					emptyText: '~~enterMockContentName~~',
					queryMode: 'local',
					displayField: 'name',
					valueField: 'name',
					store: '@{remainingMockInputParametersStore}',
					listeners: {
						select: function (combo, records) {
							this.fireEvent('mockdetailseditnameselected', this, combo, records);
						},
						mockdetailseditnameselected: '@{mockDetailsEditNameSelected}'
					}
				},
				hideable: false,
				sortable: true,
				flex: 2,
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~uvalue~~',
				dataIndex: 'value',
				editor: 'textfield',
				hideable: false,
				sortable: true,
				flex: 2,
				renderer: RS.common.grid.plainRenderer()
			}, {
				xtype: 'actioncolumn',			
				hideable: false,
				sortable: false,
				align: 'center',				
				width: 45,
				items: [{
					glyph: 0xF146,
					tooltip : 'Remove',
					handler: function(view, rowIndex, colIndex, item, e, record ) {
						this.up('grid').fireEvent('recordparamremoved', view, record);
					}
				}]
			}]
		}, {
			xtype: 'fieldset',
			title : '~~addParameter~~',
			margin: '0 0 5 135',
			padding: 15,
			items: [{
				xtype: 'form',
				
				layout: {
					type: 'vbox',
					align: 'stretch'
				},

				defaults: {
					labelWidth: 80,
					margin: '0 0 10 0',				
				},

				items: [{
					xtype: 'combo',
					name: 'type',
					maxWidth: 500,
					emptyText: '~~selectType~~',
					store: '@{typeStore}',
					queryMode: 'local',
					displayField: 'name',
					valueField: 'name',
					cls: 'no-input-border combo',
					editable: false,
					listeners: {
						focus: function() {
							this.fireEvent('newmockdetailtypefocus');
						},
						select: function(combo, records) {
							this.fireEvent('mocktypeselected', this, combo, records);
						},
						newmockdetailtypefocus: '@{newMockDetailTypeFocus}',
						mocktypeselected: '@{mockDetailTypeSelected}'
					}
				}, {
					xtype: 'combo',
					itemId: 'mockDetailsNameCombo',
					name: 'name',
					allowBlank: false,
					maxWidth: 500,
					cls: 'no-input-border combo',
					editable: '@{isMockDetailsNameEditable}',
					emptyText: '@{mockContentNameEmptyTxt}',
					queryMode: 'local',
					displayField: 'name',
					valueField: 'name',
					store: '@{remainingMockInputParametersStore}',						
					listeners: {
						focus: function() {
							this.fireEvent('newmockdetailnamefocus');
						},
						newmockdetailnamefocus: '@{newMockDetailNameFocus}'
					}
				}, {
					xtype: 'textfield',
					name: 'value',
					allowBlank: false,
					maxWidth: 500
				}, {
					xtype: 'button',
					name: 'addParams',
					cls: 'rs-small-btn rs-btn-dark',				
					margin: '0 0 0 85',
					maxWidth: 120
				}]
			}]
		}, {
			xtype: 'container',
			margin: '0 0 5 0',			
			hidden: '@{rawIsHidden}',

			layout: {
				type: 'hbox',
				align: 'stretch'
			},

			items: [{
				xtype: 'displayfield',
				hidden: '@{rawIsHidden}',
				labelWidth: 130,
				fieldLabel: '~~raw~~',
				width: 135,
				margin: '10 0 0 0'
			}, {
				xtype: 'container',
				hidden: '@{rawIsHidden}',
				margin: '10 0 0 0',
				padding: 0,
				flex: 1,
				
				layout: {
					type: 'vbox',
					align: 'stretch'
				},

				resizable: {
					handles: 's',
					pinned: true
				},

				items: [{
					xtype: 'AceEditor',
					hidden: '@{rawIsHidden}',
					margin: 0,
					padding: 0,
					name: 'mockRaw',
					parser: 'text',
					height: 300,			
					flex: 1					
				}]
			}]
		}]
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});	

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	}	
});

glu.defView('RS.actiontaskbuilder.MockRead', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: '@{classList}',
 	header: '@{header}',
	hidden: '@{hidden}',
 	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: 15,

	defaults: {
		margin: 0,
		padding: 0
	},

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	
	dockedItems: [{
		xtype: 'toolbar',	
		hidden: '@{!isShowingDetails}',
		padding : '10 15 0 15',
		items: [{ 
			name:'back',
			cls: 'rs-small-btn rs-btn-dark' 
		}]
	}],

	items: [{
		name: 'Mock',
		xtype: 'container',

		defaults: {
			margin: 0,
			padding: 0
		},

		items:[{
			xtype: 'displayfield',
			hidden: '@{hasMockValues}',
			value: '~~emptyMockMsg~~'
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			hidden: '@{!hasMockValues}',
			store: '@{mockStore}',
			maxHeight: 300,
			userDateFormat: '@{..userDateFormat}',
			viewDetailsTooltip: '@{viewDetailsTooltip}',
			listeners: {
				scope: this,
				viewmockdetails: '@{viewMockDetails}',
			},

			defaults: {
				margin: 0,
				padding: 0
			},			

			columns: [{
				xtype: 'actioncolumn',
				tdCls: 'action-cell',
				hideable: false,
				sortable: false,
				align: 'center',
				flex: 0,
				width: 34,			
				iconCls: 'rs-fa-details',
				items: [{
					getTip: function(v, meta, rec) {
						return this.up('grid').viewDetailsTooltip;
					},
					handler: function(view, rowIndex, colIndex, item, e, record ) {
						if (record.get('id') != -1) {
							this.up('grid').fireEvent('viewmockdetails', view, record);
						}
					}
				}]
			}, {
				header: '~~name~~',
				dataIndex: 'uname',
				hideable: false,
				sortable: true,
				flex: 1,
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~udescription~~',
				dataIndex: 'udescription',
				hideable: false,
				sortable: true,
				flex: 3,
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~sysUpdatedOn~~',
				dataIndex: 'sysUpdatedOn',
				hideable: true,
				sortable: true,
				flex: 1,
				renderer: function(value) {
					if (value) {
						return Ext.Date.format(new Date(value), this.userDateFormat);
					}
				}
			}, {
				header: '~~sysUpdatedBy~~',
				dataIndex: 'sysUpdatedBy',
				hideable: true,
				sortable: true,
				flex: 1,
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~sysCreatedOn~~',
				dataIndex: 'sysCreatedOn',
				hideable: true,
				sortable: true,
				hidden: true,
				flex: 1,
				renderer: function(value) {
					if (value) {
						return Ext.Date.format(new Date(value), this.userDateFormat);
					}
				}
			}, {
				header: '~~sysCreatedBy~~',
				dataIndex: 'sysCreatedBy',
				hideable: true,
				sortable: true,
				hidden: true,
				flex: 1,
				renderer: RS.common.grid.plainRenderer()
			}]
		}]
	}, {
		name: 'MockDetails',
		xtype: 'container',
		margin: 0,
		padding: 0,

		defaults: {
			margin: '0 0 10 0',
			padding: 0,
		},

		items: [{
			xtype: 'displayfield',
			fieldLabel: '~~mockName~~',
			value: '@{mockName}',
			margin: 0
		}, {
			xtype: 'displayfield',
			fieldLabel: '~~mockDescription~~',
			value: '@{mockDescription}',
			hidden: '@{!hasMockDescription}',
			margin: 0,
			renderer: RS.common.grid.plainRenderer()
		}, {
			xtype: 'displayfield',
			fieldLabel: '~~mockDescription~~',
			value: '~~noMockDescription~~',
			hidden: '@{hasMockDescription}',
			margin: 0
		}, {
			xtype: 'displayfield',
			fieldStyle: {
				fontWeight: 'bold'
			},
			value: '~~parameters~~',
			margin: 0
		}, {
			xtype: 'container',
			margin: 0,

			defaults: {
				margin: '0 0 10 0',
				padding: 0,
			},

			items: [{
				xtype: 'displayfield',
				hidden: '@{hasMockParamValues}',
				value: '~~emptyMockParamMsg~~',
				margin: 0,
			}, {
				xtype: 'grid',
				cls : 'rs-grid-dark',
				store: '@{mockDetailsStore}',
				hidden: '@{!hasMockParamValues}',
				maxHeight: '@{maxHeight}',
				columns: [{
					header: '~~type~~',
					dataIndex: 'type',
					hideable: false,
					sortable: true,
					flex: 1,
					renderer: RS.common.grid.plainRenderer()
				}, {
					header: '~~name~~',
					dataIndex: 'name',
					hideable: false,
					sortable: true,
					flex: 2,
					renderer: RS.common.grid.plainRenderer()
				}, {
					header: '~~uvalue~~',
					dataIndex: 'value',
					hideable: false,
					sortable: true,
					flex: 2,
					renderer: RS.common.grid.plainRenderer()
				}]
			}, {
				xtype: 'displayfield',
				fieldStyle: {
					fontWeight: 'bold'
				},
				hidden: '@{rawIsHidden}',
				value: '~~raw~~'
			}, {
				xtype: 'container',
				margin: 0,
				padding: 0,
				hidden: '@{rawIsHidden}',

				layout: {
					type: 'vbox',
					align: 'stretch'
				},
	
				resizable: {
					handles: 's',
					pinned: true
				},

				items: [{
					xtype: 'textarea',
					name: 'mockRaw',
					readOnly: true,
					fieldLabel: '',
					flex: 1,
					margin: 0,
					padding: 0
				}]
			}]
		}]
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});			

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},
		viewRendered: '@{viewRenderCompleted}'
	}
});

glu.defView('RS.actiontaskbuilder.OptionEditDetail', {
	xtype: 'fieldset',
	title: '~~addOption~~',
	margin: '10 0 0 0',
	padding: '5 15 10 15',
	items: [{
		xtype: 'form',
		margin: 0,
		padding: 0,

		layout: {
			type: 'vbox',
			align: 'stretch'
		},

		defaults: {
			labelWidth: 130,
			margin: '0 0 10 0'		
		},

		items: [{
			xtype: 'combobox',
			name: 'uname',
			emptyText: '~~selectOption~~',
			store: '@{optionsStore}',
			value: '@{uname}',
			editable: false,
			allowBlank: false,
			queryMode: 'local',
			displayField: 'uname',
			valueField: 'uname',			
			cls: 'no-input-border combo',
			maxWidth: 500,
			listeners: {
				focus: '@{focusOption}',
				select: '@{selectOption}'
			}
		}, {
			xtype: 'textfield',
			name: 'uvalue',
			maxWidth: 500,
		}, {
			xtype: 'textarea',
			name: 'udescription',			
		}, {
			xtype: 'button',
			text: '~~addOption~~',
			cls: 'rs-small-btn rs-btn-dark',		
			margin: '0 0 0 135',
			maxWidth: 90,
			name: 'add',
			formBind: true
		}]
	}]
});
glu.defView('RS.actiontaskbuilder.OptionsEdit', {
	parentLayout: 'quickAccess',
	title: '@{title}',	
	cls: 'block-model subEditModel',
	hidden: '@{hidden}',
	header: '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '10 15 15 15',	
	margin: '0 0 2.5 0',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});	

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});

			panel.getEl().on('click', function() {
				if (panel._vm.collapsed && !panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	},

	dockedItems: [{
		xtype: 'toolbar',	
		dock: 'bottom',
		padding : '0 15 10',			
		items: ['->', { 
			name: 'close', 
			cls: 'rs-med-btn rs-btn-dark'
		}]
	}],

	items: [{
		xtype: 'displayfield',
		hidden: '@{!gridIsEmpty}',
		value: '~~optionsEmptyText~~',
		margin: 0,
		padding: 0
	}, {
		xtype: 'grid',
		disabled: '@{savingNewTask}',
		cls : 'rs-grid-dark',
		hidden: '@{gridIsEmpty}',
		store: '@{optionsStore}',			
		
		listeners: {
			beforeedit: '@{updateAllowedOptions}'
		},
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		viewConfig: {
			getRowClass: function(record, rowIndex, rowParams, store){
				if (record.data.uname === "BLANKVALUES") {
					return "disable-entry"
				}
				else return "";
			}
		},
		selModel: {
			selType: 'cellmodel'
		},
		columns: [{
			dataIndex: 'uname',
			header: '~~uname~~',
			flex: 1,
			resizable: false,
			hideable: false,
			editor: {
				xtype: 'combobox',
				queryMode: 'local',
				displayField: 'uname',
				valueField: 'uname',
				store: '@{allowedOptionsForGridRow}',
				editable: false,
				listeners: {
					select: function () {
						this.fireEvent('selectoption', this, arguments);
					},
					selectoption: '@{selectOption}'
				}
			}
		}, {
			dataIndex: 'uvalue',
			header: '~~uvalue~~',
			editor: 'textfield',
			flex: 2,
			resizable: false,
			hideable: false,
			renderer: RS.common.grid.plainRenderer()
		}, {
			dataIndex: 'udescription',
			header: '~~description~~',
			editor: 'textfield',
			flex: 3,
			resizable: false,
			hideable: false,
			renderer: RS.common.grid.plainRenderer()
		}, {
			xtype: 'actioncolumn',		
			width : 45,
			hideable: false,			
			resizable: false,
			hideable: false,			
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record ) {
					record.store.remove(record);
					view.ownerCt._vm.addOptionToDetail(record);
				}
			}]
		}]
	}, {
		xtype: '@{detail}',
		hidden: '@{..itemsExhausted}',
		disabled: '@{..savingNewTask}',
	}]
});

glu.defView('RS.actiontaskbuilder.ParameterInputDetail', {
	xtype: 'form',	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults : {
		margin : '4 0'
	},
	items: [{
		xtype: 'container',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},		
		items: [{
			xtype: 'textfield',
			labelWidth: 130,
			allowBlank: false,
			validateOnBlur: true,
			name: 'uname',
			margin: '0 10 0 0',
			regex: RS.common.validation.VariableName,
			regexText: '~~validKeyCharacters~~',			
			flex: 2
		}, {
			xtype: 'combobox',
			cls: 'no-input-border combo',
			editable: false,
			hideLabel: true,
			labelWidth: 130,
			displayField: 'text',
			valueField: 'value',
			queryMode: 'local',
			name: 'ugroupName',
			value: '@{ugroupName}',
			store: '@{groupStore}',	
			flex: 1
		}]
	}, {
		xtype: 'container',		
		flex: 1,		
		layout: {
			type: 'hbox',
			align: 'stretchmax'
		},
		items: [{
			xtype: 'displayfield',
			width: 130,
			margin: 0,
			value: '~~parameterDescription~~'
		}, {
			xtype: 'container',
			flex: 1,
			margin: '0 0 6 0',
			padding: 0,
			cls: 'reverse-handle-borders',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},			
			resizable: {
				handles: 's',
				pinned: true
			},
			items: [{
				xtype: 'textarea',
				name: 'udescription',
				rows: 3,
				flex: 1,
				fieldLabel: null,
				labelWidth: 0,
				margin: '0 0 0 5',
				padding: 0
			}]
		}]
	}, {
		xtype: 'container',
		flex: 1,		
		layout: {
			type: 'hbox',
			align: 'stretchmax'
		},
		items: [{
			xtype: 'displayfield',
			width: 130,
			margin: 0,
			value: '~~labelDefaultValue~~'
		}, {
			xtype: 'container',
			flex: 1,
			margin: 0,
			padding: 0,
			cls: 'reverse-handle-borders',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},			
			resizable: {
				handles: 's',
				pinned: true
			},
			items: [{
				xtype: 'textarea',
				name: 'udefaultValue',
				rows: 3,
				flex: 1,
				fieldLabel: null,
				labelWidth: 0,
				margin: '0 0 0 5',
				padding: 0
			}]
		}]
	}, {
		xtype: 'checkbox',
		margin: '4 0 0 135',		
		name: 'uencrypt',
		hideLabel: true,
		boxLabel: '~~encrypt~~'
	}, {
		xtype: 'checkbox',
		margin: '4 0 0 135',		
		name: 'urequired',
		hideLabel: true,
		boxLabel: '~~required~~'
	}, {
		xtype: 'checkbox',
		margin: '4 0 0 135',		
		name: 'uprotected',
		hideLabel: true,
		boxLabel: '~~protected~~'
	},{
		xtype: 'button',
		cls: 'rs-small-btn rs-btn-dark',
		margin: '8 0 4 135',
		maxWidth : 90,
		name: 'addInput',
		text: '@{addInputTitle}',			
		formBind: true,
		listeners: {
			click: function (view) {
				var form = this.up('form').getForm();
				form.findField('ugroupName').resetOriginalValue();

				setTimeout(function () {
					this.reset();
				}.bind(form), 50);
			}
		}
	}]
});
glu.defView('RS.actiontaskbuilder.ParameterOutputDetail', {
	xtype: 'form',	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults : {
		margin : '4 0'
	},
	items: [{
		xtype: 'textfield',
		labelWidth: 130,
		allowBlank: false,
		validateOnBlur: true,
		name: 'uname',
		regex: RS.common.validation.VariableName,
		regexText: '~~validKeyCharacters~~'		
	}, {
		xtype: 'container',
		flex: 1,	
		layout: {
			type: 'hbox',
			align: 'stretchmax'
		},

		items: [{
			xtype: 'displayfield',
			width: 130,
			margin: '0 5 0 0',
			value: '~~parameterDescription~~'
		}, {
			xtype: 'container',
			flex: 1,			
			cls: 'reverse-handle-borders',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},			
			resizable: {
				handles: 's',
				pinned: true
			},
			items: [{
				xtype: 'textarea',
				name: 'udescription',
				rows: 3,
				flex: 1,
				fieldLabel: null,
				labelWidth: 0,
				margin: 0,
				padding: 0
			}]
		}]
	}, {
		xtype: 'container',
		flex: 1,		
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'displayfield',
			width: 130,
			margin: 0,
			value: '~~labelDefaultValue~~'
		}, {
			xtype: 'container',
			flex: 1,			
			cls: 'reverse-handle-borders',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},			
			resizable: {
				handles: 's',
				pinned: true
			},
			items: [{
				xtype: 'textarea',
				name: 'udefaultValue',
				rows: 3,
				flex: 1,
				fieldLabel: null,
				labelWidth: 0,
				margin: '0 0 0 5',
				padding: 0
			}]
		}]
	}, {
		xtype: 'checkbox',
		margin: '4 0 0 135',
		cls: 'no-margin',
		name: 'uencrypt',
		hideLabel: true,
		boxLabel: '~~encrypt~~'
	}, {
		xtype: 'checkbox',
		margin: '4 0 0 135',		
		name: 'urequired',
		hideLabel: true,
		boxLabel: '~~required~~'
	},{
		xtype: 'button',
		cls: 'rs-small-btn rs-btn-dark',
		margin: '8 0 0 135',
		maxWidth : 90,
		name: 'addOutput',
		text: '@{addOutputTitle}',
		formBind: true,
		listeners: {
			click: function (view) {
				setTimeout(function (vm) {
					this.reset();
				}.bind(this.up('form').getForm(), view._vm), 50);
			}
		}
	}]	
});
glu.defView('RS.actiontaskbuilder.ParametersInputEdit', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: 'block-model subEditModel',
 	hidden: '@{hidden}',
 	header: '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: 15,
	margin: '0 0 2.5 0',
	items: [{
		xtype: 'displayfield',
		value: '~~inputParametersEmptyText~~',
		hidden: '@{hasInputParameters}'
	},{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		hidden: '@{!hasInputParameters}',
		store: '@{inputParametersStore}',
		features: [{
			ftype: 'grouping',
			groupField: 'ugroupName',
			groupHeaderTpl: '{name}',
			enableNoGroups: false,
			enableGroupingMenu : false
		}],
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		selModel: {
			selType: 'cellmodel'
		},
		columns: [{
			header: '~~name~~',
			dataIndex: 'uname',
			editor: {
				xtype: 'textfield',
				allowBlank: false,
				regex: RS.common.validation.VariableName,
				regexText: '~~validKeyCharacters~~'
			},
			hideable: false,
			flex: 2,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~description~~',
			dataIndex: 'udescription',
			editor: {
				xtype:'textarea',
				grow: true,
				flex: 1
			},
			hideable: false,
			flex: 4,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~defaultValue~~',
			dataIndex: 'udefaultValue',
			editor: {
				xtype: 'textarea',
				grow: true
			},
			hideable: false,
			flex: 4,
			renderer: RS.common.grid.plainRenderer()
		}, {
			xtype: 'checkcolumn',
			align: 'center',
			header: '~~encrypt~~',
			dataIndex: 'uencrypt',
			filterable: false,
			groupable: false,
			flex: 1,
			stopSelection: false
		}, {
			xtype: 'checkcolumn',
			align: 'center',
			header: '~~required~~',
			dataIndex: 'urequired',
			groupable: false,
			flex: 1,
			stopSelection: false
		}, {
			xtype: 'checkcolumn',
			align: 'center',
			header: '~~protected~~',
			dataIndex: 'uprotected',
			groupable: false,
			flex: 1,
			stopSelection: false
		}, {
			xtype: 'actioncolumn',
			hideable: false,
			sortable: false,
			align: 'center',
			width: 45,
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record) {
					view.ownerCt._vm.removeParameter(record);
				}
			}]
		}],
		listeners : {
			validateedit : function(editor, context){
				if(context.field == "uname"){
					//Find the correct store (nested store because of the grouping feature)
					var store = context.store.store;
					var newVal = context.value;
					var currentRowIdx = context.rowIdx;
					if(store.findExact(context.field, newVal) != currentRowIdx && store.findExact(context.field, newVal) != -1){
						this._vm.displayDuplicateNameMsg();
						return false;
					}
				}
			},
		}
	}, {
		xtype: 'fieldset',
		title : '~~createGroupTitle~~',
		margin: '10 0 0 0',
		padding: '5 15 10 15',
		items: [{
			xtype: 'form',
			layout: 'vbox',
			margin: 0,
			padding: 0,
			items: [{
				xtype: 'textfield',
				labelWidth: 130,
				width: 500,
				margin: '0 0 10 0',
				padding: 0,
				name: 'newGroupName'				
			}, {
				xtype: 'button',
				name: 'addNewGroup',
				cls: 'rs-small-btn rs-btn-dark',
				margin: '0 0 0 135',			
				listeners: {
					click: function () {
			        	setTimeout(function () {
							this.reset();
						}.bind(this.up('form').getForm()), 50);
			        }
			    }
			}]
		}]
	}, {
		xtype: 'fieldset',
		title : '~~createInputTitle~~',
		margin: '10 0 0 0',
		padding: '5 15 10 15',
		items: [{
			xtype: '@{detail}'
		}]
	}],
	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});

			panel.getEl().on('click', function() {
				if (panel._vm.collapsed && !panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	},

	dockedItems: [{
		xtype: 'toolbar',
		dock: 'bottom',
		padding : '0 15 10 15',
		items: ['->', {
			name: 'close',
			cls: 'rs-med-btn rs-btn-dark',
			disabledCls: 'link-button-disabled'
		}]
	}]
});


glu.defView('RS.actiontaskbuilder.ParametersOutputEdit', {
	parentLayout: 'quickAccess',
	title: '@{title}',		
	cls: 'block-model subEditModel',
 	hidden: '@{hidden}',
 	header: '@{header}',
	bodyPadding: 15,
	margin: '0 0 2.5 0',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	items: [{
		xtype: 'displayfield',
		value: '~~outputParametersEmptyText~~',
		hidden: '@{hasOutputParameters}'
	},{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		hidden: '@{!hasOutputParameters}',
		store: '@{outputParametersStore}',
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		selModel: {
			selType: 'cellmodel'
		},
		columns: [{
			header: '~~name~~',
			dataIndex: 'uname',
			editor: {
				xtype: 'textfield',
				allowBlank: false,
				regex: RS.common.validation.VariableName,
				regexText: '~~validKeyCharacters~~'				
			},
			hideable: false,
			flex: 2,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~description~~',
			dataIndex: 'udescription',
			editor: {
				xtype: 'textarea',
				grow: true
			},
			hideable: false,
			flex: 4,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~defaultValue~~',
			dataIndex: 'udefaultValue',
			editor: {
				xtype: 'textarea',
				grow: true
			},
			hideable: false,
			flex: 4,
			renderer: RS.common.grid.plainRenderer()
		}, {
			xtype: 'checkcolumn',
			align: 'center',
			header: '~~encrypt~~',
			dataIndex: 'uencrypt',
			filterable: false,
			groupable: false,
			flex: 1
		}, {
			xtype: 'checkcolumn',
			align: 'center',
			header: '~~required~~',
			dataIndex: 'urequired',
			groupable: false,
			flex: 1
		}, {
			xtype: 'actioncolumn',			
			hideable: false,
			sortable: false,
			align: 'center',		
			width: 45,			
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record) {
					view.ownerCt._vm.removeParameter(record);	
				}
			}]
		}],
		listeners : {
			afterrender : function(grid){
				this._vm.on('refreshGrid', function(){
					grid.getView().refresh();
				});
			},
			validateedit : function(editor, context){
				if(context.field == "uname"){
					//Custom Flatstore
					var store = context.store;
					var newVal = context.value;
					var currentRowIdx = context.rowIdx;
					if(this._vm.validateParserOutput(context.record)){
						if(store.findExact(context.field, newVal) != currentRowIdx && store.findExact(context.field, newVal) != -1){
							this._vm.displayDuplicateNameMsg();
							return false;
						}
					}
					else
						return false;
				}
			},
			edit : function(editor, context){
				if(context.field == "uname"){
					if (this._vm.parentVM.embed && this._vm.rootVM.automation.activeItem && this._vm.rootVM.automation.activeItem.activeItem) {
						var outputParamName = context.originalValue;
						this._vm.rootVM.automation.activeItem.activeItem.checkTargetParamDependency(outputParamName, function() {}, function() {
							context.record.reject();
						}.bind(this));
					}
				}
			}
		}
	}, {
		xtype: 'fieldset',
		title : '~~createOutputTitle~~',
		margin: '10 0 0 0',
		padding: '5 15 10 15',
		items: [{
			xtype: '@{detail}'
		}]
	}],
	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});	

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});

			panel.getEl().on('click', function() {
				if (panel._vm.collapsed && !panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	},

	dockedItems: [{
		xtype: 'toolbar',
		dock: 'bottom',		
		padding : '0 15 10 15',
		items: ['->', { 
			name: 'close', 
			cls: 'rs-med-btn rs-btn-dark',
			disabledCls: 'link-button-disabled'		
		}]
	}]
});


glu.defView('RS.actiontaskbuilder.ParametersRead', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: '@{classList}',
 	header: '@{header}',
	hidden: '@{hidden}',
 	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '0 15 15 15',
	defaults: {
		margin: 0,
		padding: 0
	},
	items: [{
		xtype: 'panel',
		defaults: {
			labelWidth: 130,
			margin: 0,
			padding: 0
		},
		items: [{
			xtype: 'displayfield',
			fieldStyle: {
				fontWeight: 'bold'
			},
			value: '~~inputParam~~'
		}, {
			xtype: 'displayfield',
			value: '~~inputParametersEmptyText~~',
			hidden: '@{hasInputParameters}'
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			hidden: '@{!hasInputParameters}',
			store: '@{inputParametersStore}',
			features: [{
				ftype: 'grouping',
				groupField: 'ugroupName',
				groupHeaderTpl: '{name}',
				enableNoGroups: false,
				enableGroupingMenu : false,
			}],
			columns: [{
				header: '~~name~~',
				dataIndex: 'uname',
				flex: 2,
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~description~~',
				dataIndex: 'udescription',
				flex: 4,
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~defaultValue~~',
				dataIndex: 'udefaultValue',
				flex: 4,
				renderer: RS.common.grid.plainRenderer()
			}, {
				renderer: RS.common.grid.booleanRenderer(),
				align: 'center',
				header: '~~encrypt~~',
				dataIndex: 'uencrypt',
				flex: 1
			}, {
				renderer: RS.common.grid.booleanRenderer(),
				align: 'center',
				header: '~~required~~',
				dataIndex: 'urequired',
				flex: 1
			}, {
				renderer: RS.common.grid.booleanRenderer(),
				align: 'center',
				header: '~~protected~~',
				dataIndex: 'uprotected',
				flex: 1
			}]
		}, {
			xtype: 'displayfield',
			fieldStyle: {
				fontWeight: 'bold'
			},
			value: '~~outputParam~~',
			padding: '10 0 0 0'
		}, {
			xtype: 'displayfield',
			value: '~~outputParametersEmptyText~~',
			hidden: '@{hasOutputParameters}'
		},{
			xtype: 'grid',
			cls : 'rs-grid-dark',
			hidden: '@{!hasOutputParameters}',
			store: '@{outputParametersStore}',
			columns: [{
				header: '~~name~~',
				dataIndex: 'uname',
				flex: 2,
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~description~~',
				dataIndex: 'udescription',
				flex: 4,
				renderer: RS.common.grid.plainRenderer()
			}, {
				header: '~~defaultValue~~',
				dataIndex: 'udefaultValue',
				flex: 4,
				renderer: RS.common.grid.plainRenderer()
			}, {
				renderer: RS.common.grid.booleanRenderer(),
				align: 'center',
				header: '~~encrypt~~',
				dataIndex: 'uencrypt',
				flex: 1
			}, {
				renderer: RS.common.grid.booleanRenderer(),
				align: 'center',
				header: '~~required~~',
				dataIndex: 'urequired',
				flex: 1
			}]
		}]
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},
		viewRendered: '@{viewRenderCompleted}'
	}
});

glu.defView('RS.actiontaskbuilder.MarkupCapture',{
	cls : 'rs-modal-popup',
	title : '~~markupCapture~~',
	padding : 15,
	modal : true,
	width : 600,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		margin : '4 0',
		labelWidth : 120
	},
	items : [{
		xtype : 'textfield',
		name : 'variable',
	},{
		xtype : 'combo',
		name : 'containerType',
		store : '@{containerTypeStore}',
		displayField : 'value',
		valueField : 'value',
		queryMode : 'local',
		editable : false,
	},{
		xtype : '@{patternProcessor}',
		hidden : '@{!..parserTypeIsText}'
	},{
		xtype : 'displayfield',
		name : 'column',
		hidden : '@{!parserTypeIsTable}'
	},{
		xtype : 'textarea',
		name : 'xpath',
		hidden : '@{!parserTypeIsXML}'
	}],
	buttons : [{
		name : 'add',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})
glu.defView("RS.actiontaskbuilder.ParserCode",{
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	items : [{
		xtype : 'checkboxfield',
		name : 'autoGenCheck',
		readOnly : '@{viewOnly}',
		hideLabel : true,		
		boxLabel : '~~autoGenCheck~~',
		listeners : {
			change : '@{checkBoxChange}'
		}
	},{
		xtype: 'AceEditor',
		name: 'codeContent',
		cls: '@{editorCls}',
		parser: 'groovy',
		flex : 1,
		readOnly : '@{codeContentIsReadOnly}',	
		minHeight : 500
	}]
})
glu.defView('RS.actiontaskbuilder.ParserControl',{
	xtype : 'panel',
	layout : 'card',	
	activeItem : '@{activeParserType}',
	items : [{
		xtype : '@{textParser}',
	},{
		xtype : '@{tableParser}'
	},{
		xtype : '@{xmlParser}'
	}]
})
glu.defView('RS.actiontaskbuilder.PatternProcessor', {
	xtype : 'panel',
	items : [{
		xtype : 'component',
		html : '~~patternTableTitle~~',
		margin : '0 0 8 0'
	},{
		xtype : 'component',
		html : '~~patternHintText~~',
		hidden : '@{!patternIsHidden}'
	},{		
		items : '@{patternList}',
		hidden : '@{patternIsHidden}'
	}]
})
glu.defView('RS.actiontaskbuilder.PatternTag',{
	xtype : 'component',
	hidden : '@{hidden}',	
	margin : '0 8 8 0',
	cls : 'text-parser-tag @{selectedCss}',	
	html : '@{displayName}',
	listeners: {
		render: function(panel) {
			panel.getEl().on('click', function() {
				panel.fireEvent('select', panel)
			})
		},
		select: '@{select}'		
	}
})
RS.actiontaskbuilder.parserTextConfig = {
	region : 'center',	
	xtype : 'container',		
	html: '<iframe spellcheck="false" autocomplete="off" frameborder="0" style="width:100%;height:100%;border:1px solid #999;line-height: 0;" src=""></iframe>',
	parser: {	
		iframe: null,
		win: null,
		parserHintElement: null,
		//In Chrome mousedown and mousemove fire simultanuousely use the position to detect actual movement.
		mousePreviousPosition : false,
		hightLightTextFlag: false,

		initParser: function(iframe){		
			var me = this;			
			me.iframe = iframe;

			me.win = iframe.contentWindow;
			me.parserHintElement = '<div class=parserHintText contenteditable="false">PASTE YOUR OUTPUT SAMPLE HERE.</div>';
			var document = this.cdoc();
			var docBody = document.body;	
			docBody.innerHTML = '<pre class="parser-content" contenteditable="true">' + me.parserHintElement + '</pre>';		
			if (docBody && ('spellcheck' in docBody))
				docBody.spellcheck = false;

			//Disable typing on this iframe ( might add feature to allow user to type in future)
			Ext.fly(document).on('keydown', function(e) {
				if (!e.ctrlKey)
					e.preventDefault()
			});

			//Events on parser's sample editor.
			docBody.onpaste = function(e) {									
				var rawData = e && e.clipboardData ? e.clipboardData.getData('text/plain'): window.clipboardData.getData('Text');
				var lines = rawData.split(/\r*\n/);
				var data = lines.join(Ext.is.Windows ? '\r\n': '\n');
				var asPasted = true;			
				me.eventEmitter.fireEvent('sampleChanged', me, data, asPasted);				
				return false;
			}
			
			docBody.onmousedown = function(e) {
				if(e.button == 0){
					me.hightLightTextFlag = false;
					me.mousePreviousPosition = [e.pageX,e.pageY];
					me.deselectAllMarkup();
				}
				else 
					me.mousePreviousPosition = null;
			}

			docBody.onmousemove = function(e) {				
				if(me.mousePreviousPosition && e.pageX != me.mousePreviousPosition[0] && e.pageX != me.mousePreviousPosition[1])
					me.hightLightTextFlag = true;			
				else
					e.hightLightTextFlag = false;
			}
			function patternSelectionHandler(e) {				
				var selection = rangy.getSelection(me.iframe),
					validParent = selection && selection.anchorNode && selection.parentNode;
				
				//Selection outside boundary
				if(validParent && selection.anchorNode.parentNode.nodeType == 1 && selection.anchorNode.parentNode.hasAttribute('unselectable')){
					me.eventEmitter.fireEvent('selectionIsInvalid');
					return;
				}
				//Handle zero length selection
				if(me.isZeroLengthSelection(selection)){					
					return true;
				}

				if(me.hightLightTextFlag){
					//Selection happens inside marked node.
					if(validParent && selection.anchorNode.parentNode.nodeType == 1 && selection.anchorNode.parentNode.hasAttribute('marked')){
						me.eventEmitter.fireEvent('selectionIsInvalid');
						return;
					} else if (selection && selection.rangeCount) {
					    var range = selection.getRangeAt(0);
					    var markedElements = range.getNodes([1], function(node) {
					    	return node.hasAttribute('marked');				    	
					    });

					    if(markedElements.length > 0){
					    	me.eventEmitter.fireEvent('selectionIsInvalid');
					    	return;
					    }
					}

					me.eventEmitter.fireEvent('contentSelectionChanged', me, selection, docBody, false);
				}
				else {
					me.eventEmitter.fireEvent('parserIsDefault');
				}			
			}	
			Ext.fly(document).on('mouseup',function(){
				me.mousePreviousPosition = null;
				patternSelectionHandler();
			});		
			docBody.ondblclick = function(e){
				me.hightLightTextFlag = true;
				patternSelectionHandler();
				e.preventDefault();
			};
			//Disable drag and drop elements
			docBody.ondragstart = function(){
				return false;
			}
			docBody.ondrop = function(){
				return false;
			}			
			me.eventEmitter.fireEvent('parserIsLoaded',me,true);
		},
		cdoc: function(){
			return this.win.document;
		},
		placeHolders: {
			space: '<span marker="space" class="format">&nbsp;</span>',
			new_line:  '<span marker="new_line" class="return"><br></span>'
		},
		convert: function(text, pre, post) {
			if(!text)
				return "";
			var processedText = text.replace(/ /g, this.placeHolders.space).replace(Ext.is.Windows ? (/\r\n/g): (/\n/g), this.placeHolders.new_line);
			return processedText;
		},
		isZeroLengthSelection: function(selection){
			var isZeroLengthSelection = (selection.anchorNode == selection.focusNode && selection.anchorOffset == selection.focusOffset);
			//Check for Line Break.	
			if(isZeroLengthSelection){		
				var validAnchor = selection && selection.anchorNode;

				if(validAnchor && selection.anchorNode.nodeType == 1 && selection.anchorNode.getAttribute('marker') == 'new_line'){
					//Case 1: Selection on a line-break node (Line only contains linebreak.)
					//Make sure this new-line node is not inside a marked node.
					if(selection.anchorNode.parentNode && selection.anchorNode.parentNode.nodeType== 1 && selection.anchorNode.parentNode.hasAttribute('marked')) {
						this.eventEmitter.fireEvent('selectionIsInvalid');						
					} else {
						this.eventEmitter.fireEvent('contentSelectionChanged', this, selection, this.cdoc().body, true);
					}

				} else if(validAnchor && selection.anchorNode.length == selection.anchorOffset){			
					//Case 2: Selection on a node.
					var node = selection.anchorNode.nextSibling;

					if(!node && selection.anchorNode.parentNode) {
						node = selection.anchorNode.parentNode.nextSibling;
					}

					if(node && node.nodeType == 1 && node.getAttribute('marker') == 'new_line'){
						//Make sure this new-line node is not inside a marked node.
						if (node.parentNode) {
							if(node.parentNode.nodeType == 1 && node.parentNode.hasAttribute('marked'))
								this.eventEmitter.fireEvent('selectionIsInvalid');
							else
								this.eventEmitter.fireEvent('contentSelectionChanged', this, selection, this.cdoc().body, true);							
						}
					}
				}else {
					this.eventEmitter.fireEvent('parserIsDefault');
				}
			}			
			return isZeroLengthSelection;
		},
		updateParserView: function(sample,markups){
			//Get pts for each markup
			var pts = [];
			var parseStartPosition = -1;
			var parseEndPosition = -1;
			var hasParseStart = false;
			var hasParseEnd = false;
			Ext.each(markups, function(markup){			
				if(markup.type == 'parseStart'){
					hasParseStart = true;
					parseStartPosition = markup.from + ( markup.included ? 0: markup.length );
					pts.push(parseStartPosition);
				}
				else if(markup.type == 'parseEnd'){
					hasParseEnd = true;
					parseEndPosition = markup.from + ( markup.included ? markup.length: 0 );
					pts.push(parseEndPosition);
				}
				else{
					pts.push(markup.from);
					pts.push(markup.from + markup.length);
				}
			})
			pts = Ext.Array.unique(pts);
			//Add start and end pts
			if(pts.indexOf(0) == -1)
				pts.push(0);
			if (pts.indexOf(sample.length) == -1)
				pts.push(sample.length);
			pts.sort(function(a, b) {
				return a > b ? 1: (a == b ? 0: -1);
			});
			var chopped = [];
			for (var i = 0; i < pts.length - 1; i++){
				if(parseStartPosition == pts[i]){
					chopped.push({
						type: "parseStart"					
					});
				}
				else if(parseEndPosition == pts[i]){
					chopped.push({
						type: "parseEnd"					
					});
				}
				chopped.push({
					text: sample.substring(pts[i], pts[i + 1]),
					from: pts[i],
					to: pts[i + 1],
					actionId: null
				});
			}
		
			//Sort all pieces and add type and id for each chopped piece.
			Ext.each(chopped, function(slice, idx) {
				Ext.each(markups, function(markup) {
					if(markup.type == slice.type && ( markup.type == "parseStart" || markup.type == "parseEnd")){ //Only slice with parseEnd and parseStart has a type at this point						
						slice.actionId = markup.actionId;
					}
					else if (markup.type != "parseStart" && markup.type != "parseEnd" && slice.from >= markup.from && slice.to <= (markup.from + markup.length)) {
						slice.actionId = markup.actionId;
						slice.type = markup.type;	
						slice.action = markup.action || 'default';				
						if(markup.action == 'capture'){
							slice.color = markup.color;
						}
					}
				});
			});
			var done = [];
			var beginParseSection = false ;
			var endParseSection = false;
			Ext.each(chopped, function(slice, idx) {				
				var convertedContent = ((slice.type == 'endOfLine') ? '&crarr;': '') + this.convert(Ext.htmlEncode(slice.text));
				if(slice.type == 'parseStart')
					beginParseSection = true;				
				if((hasParseStart && !beginParseSection) || (hasParseEnd && endParseSection)){
					done.push(Ext.String.format('<span unselectable boundary contenteditable="false">{0}</span>', convertedContent));
				}
				else {
					if (slice.actionId != null){
						if(slice.action == 'capture'){
							done.push(Ext.String.format('<span {1} unselectable marked contenteditable="false" action="capture" type={2} style="background:{3}">{0}</span>', convertedContent, 'actionId="' + slice.actionId + '"', slice.type, slice.color));
						}
						else
							done.push(Ext.String.format('<span {1} unselectable marked contenteditable="false" type={2}>{0}</span>', convertedContent, 'actionId="' + slice.actionId + '"', slice.type))
					}
					else
						done.push(convertedContent);
				if(slice.type == 'parseEnd')
					endParseSection = true;
				}
			},this);
			if(done.length == 0)
				done.push(this.parserHintElement);
			this.cdoc().body.innerHTML ='<pre class="parser-content" contenteditable="true">' + done.join('') +'</pre>';

			//Add Event Handler for each markups
			this.liveMarkup();
		},		
		liveMarkup: function(){
			var me = this;
			Ext.each(Ext.fly(this.cdoc().body).query('span'),function(span){
				if(span.hasAttribute('marked') && span.getAttribute('action') != 'capture'){				
					Ext.fly(span).on('mouseover', function(e) {							
						Ext.fly(span).addCls('markup-over');
					});
					Ext.fly(span).on('mouseout', function(e) {
						Ext.fly(span).removeCls('markup-over');
					});
					Ext.fly(span).on('click', function(e) {	
						span.setAttribute('activated','');
						var actionId = span.getAttribute('actionId');
						me.eventEmitter.fireEvent('markupSelectionChanged', me, actionId);						
					});						
				}
			})
		},
		reselectMarkup : function(actionId){
			Ext.each(Ext.fly(this.cdoc().body).query('span[actionid=' + actionId + ']'),function(markup){
				markup.click();
			})
		},
		deselectAllMarkup : function(){		
			var activatedMarkups = Ext.fly(this.cdoc().body).query('span[marked][activated]');
			if(activatedMarkups.length > 0)
				activatedMarkups[0].removeAttribute('activated');
		}
	},	
	sample: '@{sample}',
	setSample: function(sample){
		this.sample = sample;
		this.parser.updateParserView(this.sample, []);
	},
	markups: '@{markups}',
	setMarkups: function(markups){
		this.markups = markups;
		this.parser.updateParserView(this.sample, markups);
	},	
	listeners: {
		afterrender: function() {					
			var iframe = this.el.query('iframe')[0];
			iframe.src = '/resolve/actiontask/editorstub.jsp?type=text&OWASP_CSRFTOKEN=' + clientVM.getPageToken('actiontask/editorstub.jsp');
			var me = this;			
			me.parser.eventEmitter = this;
			//Initialize Parser Once the frame is ready.	
			if (iframe.attachEvent)
				iframe.attachEvent('onload', function() {
					me.parser.initParser(iframe);
				});
			else if (iframe.addEventListener){
				iframe.addEventListener('load', function() {
					me.parser.initParser(iframe);
				}, false);
			}			
			this._vm.on('reselectMarkup', function(actionId){
				me.parser.reselectMarkup(actionId);
			});
		},
		sampleChanged: '@{updateSample}',		
		contentSelectionChanged: '@{processHighlightSelection}',
		markupSelectionChanged: '@{processMarkupSelection}',
		parserIsDefault: function(){
			this._vm.resetParser();			
		},
		parserIsLoaded: function(){
			this._vm.set('parserReady', true);
		},
		selectionIsInvalid: function(){			
			this._vm.set('selectionIsInvalid', true);
		},
		updateBoundaryFromMarkup: '@{updateBoundaryFromMarkup}',
		beforedestroy : function(){
			//Mark this iframe dom to be collected by GC. 
			//By default events that are registered on #document is marked as not collected by GC (Not sure why Extjs doing that)
			if(this.rendered){
				var document = this.parser.cdoc();
				var iframeDom = Ext.fly(document);
				if(iframeDom && iframeDom.$cache)
					iframeDom.$cache.skipGarbageCollection = false;
			}
		}
	}
}
glu.defView('RS.actiontaskbuilder.PlainTextParser',{
	itemId : 'text',
	layout : 'card',	
	activeItem : '@{activeScreen}',
	dockedItems: [{
		xtype: 'toolbar',
		cls : 'rs-dockedtoolbar',
		defaults: {
			cls: 'rs-small-btn rs-btn-dark'			
		},
		items:['clearSample','clearMarkup','|','showCode','showMarkup']
	}],	
	items : [{	
		//Markup Screen
		xtype : 'container',		
		layout : {
			type : 'vbox',
			align : 'stretch'
		},	
		items:[{
			xtype: 'container',				
			layout : 'border',	
			height : '@{controlHeight}',		
			items: [RS.actiontaskbuilder.parserTextConfig,{	
				//Control Panel
				width : 600,				
				region : '@{controlBorderRegion}',
				margin : '@{controlMargin}',
				xtype : 'container',			
				layout : {
					type : 'vbox',
					align : 'stretch'
				},
				items : [{
					//Control
					xtype : 'fieldset',
					height : 170,
					padding : '5 10',			
					title : '~~parserControl~~',
					layout : {
						type : 'vbox',
						align : 'stretch'
					},
					items : [{
						xtype : 'component',
						html : '~~invalidSelectionText~~',
						hidden : '@{!selectionIsInvalid}'
					},{
						xtype: 'toolbar',
						hidden : '@{selectionIsInvalid}',
						cls : 'rs-dockedtoolbar',
						defaults: {
							cls: 'rs-small-btn rs-btn-dark'					
						},
						items: ['removeMarkup','captureMarkup']
					},{
						xtype : '@{patternProcessor}',
						flex : 1,
						hidden : '@{..selectionIsInvalid}'					
					}]
				},{
					//Boundary
					xtype : 'fieldset',
					margin : 0,	
					flex : 1,		
					padding : '5 10 10 10',		
					title: '~~boundaryTitle~~',			
					layout: {
						type:'vbox',
						align: 'stretch'
					},				
					items: [{
						xtype: 'button',
						name: 'updateBoundary',
						maxWidth : 80,
						cls: 'rs-small-btn rs-btn-dark'
					},{
						xtype : 'fieldcontainer',
						fieldLabel: '~~fromField~~',				
						labelWidth : 100,
						defaults : {
							margin : '1 0'
						},
						layout : {
							type : 'vbox',
							align : 'stretch'
						},
						items: [{
							xtype : 'radiofield',					
							boxLabel: '~~beginningOutputLabel~~',
							value: '@{startOutput}',					
							handler: '@{startOutputIsSelected}'
						},{
							xtype : 'container',					
							layout : 'hbox',					
							items : [{
								xtype : 'radiofield',
								boxLabel: '~~firstOccurrenceLabel~~',
								value: '@{!startOutput}',					
							},{
								xtype: 'textfield',
								name: 'parseStart',						
								hideLabel : true,
								margin : '0 0 0 10',
								flex : 1
							}]
						},{
							xtype: 'checkboxfield',					
							boxLabel: '~~inclusive~~',
							value: '@{startTextIncluded}'
						}]
					},{
						xtype : 'fieldcontainer',
						fieldLabel: '~~toField~~',
						labelWidth : 100,
						defaults : {
							margin : '1 0'
						},
						layout : {
							type : 'vbox',
							align : 'stretch'
						},
						items: [{
							xtype : 'radiofield',					
							boxLabel: '~~endOutputLabel~~',
							value: '@{endOutput}',			
							handler: '@{endOutputIsSelected}'
						},{
							xtype : 'container',					
							layout : 'hbox',
							items : [{
								xtype : 'radiofield',
								boxLabel: '~~firstOccurrenceLabel~~',
								value: '@{!endOutput}',					
							},{
								xtype: 'textfield',
								name: 'parseEnd',						
								hideLabel : true,
								margin : '0 0 0 10',
								flex : 1
							}]
						},{
							xtype: 'checkboxfield',					
							boxLabel: '~~inclusive~~',
							value: '@{endTextIncluded}'
						}]
					},{
						xtype: 'checkboxfield',
						value: '@{block}',
						hideLabel : true,
						margin : '5 0',	
						boxLabel: '~~repeatedBlock~~'				
					}]
				}],
				listeners : {
					afterrender : function(panel){
						panel._vm.on('embeddedLayout', function(isEmbedded){								
							if(isEmbedded){
								panel.setBorderRegion('south');							
							}
							else {
								panel.setBorderRegion('east');							
							}
						});
					}
				}
			}]	
		},{
			xtype: 'component',		
			html: '~~variableTable~~',
			margin : '15 0 8 0',	
		},{
			xtype: 'component',		
			hidden: '@{!variableStoreIsEmpty}',
			html: '~~noVariableText~~'
		},{
			xtype: 'grid',		
			cls: 'rs-grid-dark',						
			store: '@{variableStore}',
			columns: '@{variableColumn}',
			hidden: '@{variableStoreIsEmpty}',
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}],
			selModel: {
				selType: 'cellmodel'
			},
			listeners: {					
				removeVariable: '@{removeVariable}',
				validateedit: function(editor, context){
					if(context.field == "variable"){						
						var store = context.store;
						var newVal = context.value;
						var currentRowIdx = context.rowIdx;
						if(store.findExact(context.field, newVal) != currentRowIdx && store.findExact(context.field, newVal) != -1){
							this._vm.displayDuplicateNameMsg();
							return false;
						}
						else if(!RS.common.validation.VariableName.test(newVal)){
							this._vm.displayInvalidCharacter();
							return false;
						}
					}
				},
				edit: '@{updateVariable}'				
			}
		}]
	},{
		xtype : '@{codeScreen}'		
	}]
})

RS.actiontaskbuilder.parserTableConfig = {	
	region : 'center',	
	xtype : 'container',	
	html: '<iframe spellcheck="false" autocomplete="off" frameborder="0" style="width:100%;height:100%;border:1px solid #999;line-height: 0;" src=""></iframe>',
	parser: {
		iframe: null,
		win: null,	
		parserHintElement: null,
		mousePreviousPosition : false,
		hightLightTextFlag: false,	

		initParser: function (iframe){			
			var me = this;			
			me.iframe = iframe;

			me.win = iframe.contentWindow;
			me.parserHintElement = '<div class=parserHintText contenteditable="false">PASTE YOUR OUTPUT SAMPLE HERE.</div>';	
			var document = this.cdoc();
			var docBody = document.body;
			docBody.innerHTML = '<pre class="parser-content" contenteditable="true">' + me.parserHintElement + '</pre>';							
			if (docBody && ('spellcheck' in docBody))
				docBody.spellcheck = false;

			//Disable typing on this iframe ( might add feature to allow user to type in future)
			Ext.fly(document).on('keydown', function(e) {
				if (!e.ctrlKey)
					e.preventDefault()
			});

			//Events on parser's sample editor.
			docBody.onpaste = function (e) {				
				var rawData = e && e.clipboardData ? e.clipboardData.getData('text/plain'): window.clipboardData.getData('Text');
				var lines = rawData.split(/\r*\n/);
				var data = lines.join(Ext.is.Windows ? '\r\n': '\n');
				var asPasted = true;		
				me.eventEmitter.fireEvent('sampleChanged', me, data, asPasted);	
				return false;
			}
			
			docBody.onmousedown = function(e) {
				if(e.button == 0){
					me.hightLightTextFlag = false;
					me.mousePreviousPosition = [e.pageX,e.pageY];				
				}
				else 
					me.mousePreviousPosition = null;
			}
			
			docBody.onmousemove = function(e) {				
				if(me.mousePreviousPosition && e.pageX != me.mousePreviousPosition[0] && e.pageX != me.mousePreviousPosition[1])
					me.hightLightTextFlag = true;			
				else
					e.hightLightTextFlag = false;
			}

			docBody.onmouseup = function (e) {				
				var selection = rangy.getSelection(me.iframe);

				if (!me.isZeroLengthSelection(selection) && me.hightLightTextFlag) {	
					me.eventEmitter.fireEvent('contentSelectionChanged', me, selection, docBody, false);
				}				
			}

			//Disable drag and drop elements
			docBody.ondragstart = function (e) {
				return false;
			}
			docBody.ondrop = function () {
				return false;
			}
			me.eventEmitter.fireEvent('parserIsLoaded',me,true);	
		},
		cdoc: function(){
			return this.win.document;
		},
		placeHolders: {
			space: '<span marker="space" class="format">&nbsp;</span>',
			new_line:  '<span marker="new_line" class="return"><br></span>'
		},
		convert: function(text, pre, post) {
			if(!text)
				return "";
			var processedText = text.replace(/ /g, this.placeHolders.space).replace(Ext.is.Windows ? (/\r\n/g): (/\n/g), this.placeHolders.new_line);
			return processedText;
		},
		isZeroLengthSelection: function(selection){
			var isZeroLengthSelection = (selection.anchorNode == selection.focusNode && selection.anchorOffset == selection.focusOffset);
			return isZeroLengthSelection;
		},
		//This method will render markups into HTML elements
		updateParserView: function(sample,markups){
			//Get pts for each markup
			var pts = [];
			var parseStartPosition = -1;
			var parseEndPosition = -1;
			var hasParseStart = false;
			var hasParseEnd = false;
			Ext.each(markups, function(markup){			
				if(markup.type == 'parseStart'){
					hasParseStart = true;
					parseStartPosition = markup.from + ( markup.included ? 0: markup.length );
					pts.push(parseStartPosition);
				}
				else if(markup.type == 'parseEnd'){
					hasParseEnd = true;
					parseEndPosition = markup.from + ( markup.included ? markup.length: 0 );
					pts.push(parseEndPosition);
				}
				else{
					pts.push(markup.from);
					pts.push(markup.from + markup.length);
				}
			})
			pts = Ext.Array.unique(pts);
			//Add start and end pts
			if(pts.indexOf(0) == -1)
				pts.push(0);
			if (pts.indexOf(sample.length) == -1)
				pts.push(sample.length);
			pts.sort(function(a, b) {
				return a > b ? 1: (a == b ? 0: -1);
			});
			var chopped = [];
			for (var i = 0; i < pts.length - 1; i++){
				if(parseStartPosition == pts[i]){
					chopped.push({
						type: "parseStart"					
					});
				}
				else if(parseEndPosition == pts[i]){
					chopped.push({
						type: "parseEnd"					
					});
				}
				chopped.push({
					text: sample.substring(pts[i], pts[i + 1]),
					from: pts[i],
					to: pts[i + 1],
					actionId: null
				});
			}
		
			//Sort all pieces and add type and id for each chopped piece.
			Ext.each(chopped, function(slice, idx) {
				Ext.each(markups, function(markup) {
					if(markup.type == slice.type && ( markup.type == "parseStart" || markup.type == "parseEnd")){ //Only slice with parseEnd and parseStart has a type at this point						
						slice.actionId = markup.actionId;
					}
					else if (markup.type != "parseStart" && markup.type != "parseEnd" && slice.from >= markup.from && slice.to <= (markup.from + markup.length)) {
						slice.actionId = markup.actionId;
						slice.type = markup.type;	
						slice.action = markup.action || 'default';				
						if(markup.action == 'capture' || markup.type == 'column'){
							slice.color = markup.color;
						}
						if(markup.type == 'column'){
							slice.col = markup.column;
						}
					}
				});
			});
			var done = [];
			var beginParseSection = false ;
			var endParseSection = false;
			Ext.each(chopped, function(slice, idx) {				
				var convertedContent = this.convert(Ext.htmlEncode(slice.text));
				if(slice.type == 'parseStart')
					beginParseSection = true;				
				if((hasParseStart && !beginParseSection) || (hasParseEnd && endParseSection)){
					done.push(Ext.String.format('<span unselectable boundary contenteditable="false">{0}</span>', convertedContent));
				}
				else {
					if (slice.actionId != null){
						if(slice.action == 'capture'){
							done.push(Ext.String.format('<span {1} marked unselectable contenteditable="false" action="capture" type={2} style="background:{3}" ignore>{0}</span>', convertedContent, 'actionId="' + slice.actionId + '"', slice.type, slice.color));
						}
						else if(slice.type == 'column'){
							done.push(Ext.String.format('<span {1} marked unselectable contenteditable="false" action="default" type={2} col={4} style="border-color:{3};border-width:2px;">{0}</span>', convertedContent, 'actionId="' + slice.actionId + '"', slice.type, slice.color, slice.col));
						}
						else
							done.push(Ext.String.format('<span {1} unselectable contenteditable="false" type={2}>{0}</span>', convertedContent, 'actionId="' + slice.actionId + '"', slice.type))
					}
					else
						done.push(convertedContent);
				if(slice.type == 'parseEnd')
					endParseSection = true;
				}
			},this);
			if(done.length == 0)
				done.push(this.parserHintElement);
			this.cdoc().body.innerHTML ='<pre class="parser-content" contenteditable="true">' + done.join('') +'</pre>';	

			//Add Event Handler for each markups
			this.liveMarkup();
		},
		liveMarkup: function(){
			var me = this;
			Ext.each(Ext.fly(this.cdoc().body).query('span'),function(span){
				if(span.hasAttribute('marked') && !span.hasAttribute('ignore')){					
					Ext.fly(span).on('click', function(e) {						
						var colNo = span.getAttribute('col');
						me.eventEmitter.fireEvent('columnSelectionChanged', this, colNo);						
					});									
				}
			})
		},
		updateColumnSelection: function(colNo){
			//Disable all other activated markup.
			var activatedMarkups = Ext.fly(this.cdoc().body).query('span[marked][activated]');
			for(var i = 0; i < activatedMarkups.length; i++){
				var activatedSpan = activatedMarkups[i];
				activatedSpan.removeAttribute('activated');
			}			
			var columnCells = Ext.fly(this.cdoc().body).query('span[col='+ colNo +']');
			for(var i = 0; i < columnCells.length; i++){
				var currentCell = columnCells[i];
				currentCell.setAttribute('activated','');
			}
		}
	},	
	sample: '@{sample}',
	setSample: function(sample){
		this.sample = sample;
		this.parser.updateParserView(this.sample, []);
	},
	markups: '@{markups}',
	setMarkups: function(markups){
		this.parser.updateParserView(this.sample, markups);
	},	
	listeners: {
		afterrender: function() {					
			var iframe = this.el.query('iframe')[0];
			iframe.src = '/resolve/actiontask/editorstub.jsp?type=table&OWASP_CSRFTOKEN=' + clientVM.getPageToken('actiontask/editorstub.jsp');
			var me = this;
			this._vm.on('updateColumnSelection',function(colNo){
				me.parser.updateColumnSelection(colNo);
			});	
			this._vm.on('updateCaptureColumn',function(markups){
				me.parser.updateParserView(me.sample, markups);
			});					
			me.parser.eventEmitter = this;
			//Initialize Parser Once the frame is ready.	
			if (iframe.attachEvent)
				iframe.attachEvent('onload', function() {
					me.parser.initParser(iframe);
				});
			else if (iframe.addEventListener){
				iframe.addEventListener('load', function() {
					me.parser.initParser(iframe);
				}, false);
			};
		},
		sampleChanged: '@{updateSample}',
		parserIsLoaded: function(){
			this._vm.set('parserReady', true);
		},
		contentSelectionChanged: '@{processHighlightSelection}',
		columnSelectionChanged: '@{processColumnSelection}',	
		beforedestroy : function(){
			//Mark this iframe dom to be collected by GC. 
			//By default events that are registered on #document is marked as not collected by GC (Not sure why Extjs doing that)
			if(this.rendered){
				var document = this.parser.cdoc();
				var iframeDom = Ext.fly(document);
				if(iframeDom && iframeDom.$cache)
					iframeDom.$cache.skipGarbageCollection = false;
			}
		}
	}
};

glu.defView('RS.actiontaskbuilder.TableParser',{
	itemId : 'table',
	layout : 'card',
	activeItem : '@{activeScreen}',
	dockedItems: [{
		xtype: 'toolbar',
		cls : 'rs-dockedtoolbar',
		defaults: {
			cls: 'rs-small-btn rs-btn-dark'		
		},
		items:['clearSample','clearMarkup','|','showCode','showMarkup']
	}],	
	items : [{
		//Markup Screen
		xtype : 'container',		
		layout : {
			type : 'vbox',
			align : 'stretch'
		},	
		items:[{
			xtype: 'container',	
			layout : 'border',	
			height : '@{controlHeight}',		
			items: [RS.actiontaskbuilder.parserTableConfig,{
				//Control
				width : 600,				
				region : '@{controlBorderRegion}',
				margin : '@{controlMargin}',
				xtype : 'container',			
				layout : {
					type : 'vbox',
					align : 'stretch'
				},
				items : [{
					xtype : 'fieldset',
					title : '~~parserControl~~',	
					padding : '5 10',
					height : 170,
					layout: {
						type:'vbox',
						align: 'stretch'
					},
					defaults : {
						labelWidth : 140,
						editable : false	
					},
					items: [{
						xtype : 'toolbar',
						cls : 'rs-dockedtoolbar',
						defaults : {
							cls : 'rs-small-btn rs-btn-dark'
						},
						items : ['applySeparator','captureVariable']
					},{
						xtype: 'combobox',						
						store: '@{columnSeparatorStore}',
						queryMode: 'local',
						displayField: 'display',
						valueField: 'value',
						name:  'columnSeparator'
					},{
						xtype: 'combobox',						
						store: '@{rowSeparatorStore}',
						queryMode: 'local',
						displayField: 'display',
						valueField: 'value',
						fieldLabel:  '~~rowSeparator~~',
						value: '@{rowSeparator}'						
					},{
						xtype: 'combobox',					
						name: 'selectedColumn',
						store: '@{columnDropdownStore}',
						displayField: 'display',
						valueField: 'value',
						queryMode: 'local',									
						cls: 'no-input-border combo',
						tpl: Ext.create('Ext.XTemplate',
							'<tpl for=".">',
							'<div class="x-boundlist-item"><div style="margin-top:5px;">Column {value} </div><div class="column-picker" style="background:{color}"></div></div>',
							'</tpl>'
						)
					}]				
				},{
					//Boundary
					xtype : 'fieldset',
					margin : 0,	
					flex : 1,				
					padding : '5 10 10 10',			
					title: '~~boundaryTitle~~',			
					layout: {
						type:'vbox',
						align: 'stretch'
					},				
					items: [{
						xtype: 'button',
						name: 'updateBoundary',
						maxWidth : 80,
						cls: 'rs-small-btn rs-btn-dark'
					},{
						xtype : 'fieldcontainer',
						fieldLabel: '~~fromField~~',				
						labelWidth : 100,
						defaults : {
							margin : '1 0'
						},
						layout : {
							type : 'vbox',
							align : 'stretch'
						},
						items: [{
							xtype : 'radiofield',					
							boxLabel: '~~beginningOutputLabel~~',
							value: '@{startOutput}',					
							handler: '@{startOutputIsSelected}'
						},{
							xtype : 'container',					
							layout : 'hbox',					
							items : [{
								xtype : 'radiofield',
								boxLabel: '~~firstOccurrenceLabel~~',
								value: '@{!startOutput}',					
							},{
								xtype: 'textfield',
								name: 'parseStart',						
								hideLabel : true,
								margin : '0 0 0 10',
								flex : 1
							}]
						},{
							xtype: 'checkboxfield',					
							boxLabel: '~~inclusive~~',
							value: '@{startTextIncluded}'
						}]
					},{
						xtype : 'fieldcontainer',
						fieldLabel: '~~toField~~',
						labelWidth : 100,
						defaults : {
							margin : '1 0'
						},
						layout : {
							type : 'vbox',
							align : 'stretch'
						},
						items: [{
							xtype : 'radiofield',					
							boxLabel: '~~endOutputLabel~~',
							value: '@{endOutput}',			
							handler: '@{endOutputIsSelected}'
						},{
							xtype : 'container',					
							layout : 'hbox',
							items : [{
								xtype : 'radiofield',
								boxLabel: '~~firstOccurrenceLabel~~',
								value: '@{!endOutput}',					
							},{
								xtype: 'textfield',
								name: 'parseEnd',						
								hideLabel : true,
								margin : '0 0 0 10',
								flex : 1
							}]
						},{
							xtype: 'checkboxfield',					
							boxLabel: '~~inclusive~~',
							value: '@{endTextIncluded}'
						}]
					}]
				}],
				listeners : {
					afterrender : function(panel){
						panel._vm.on('embeddedLayout', function(isEmbedded){								
							if(isEmbedded){
								panel.setBorderRegion('south');							
							}
							else {
								panel.setBorderRegion('east');							
							}
						});
					}
				}
			}]	
		},{
			xtype: 'component',		
			html: '~~variableTable~~',
			margin : '15 0 8 0',	
		},{
			xtype: 'component',		
			hidden: '@{!variableStoreIsEmpty}',
			html: '~~noVariableText~~'
		},{
			xtype: 'grid',	
			cls: 'rs-grid-dark',						
			store: '@{variableStore}',
			columns: '@{variableColumn}',
			hidden: '@{variableStoreIsEmpty}',
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}],
			selModel: {
				selType: 'cellmodel'
			},
			listeners: {					
				removeVariable: '@{removeVariable}',
				validateedit: function(editor, context){
					if(context.field == "variable"){						
						var store = context.store;
						var newVal = context.value;
						var currentRowIdx = context.rowIdx;
						if(store.findExact(context.field, newVal) != currentRowIdx && store.findExact(context.field, newVal) != -1){
							this._vm.displayDuplicateNameMsg();
							return false;
						}
						else if(!RS.common.validation.VariableName.test(newVal)){
							this._vm.displayInvalidCharacter();
							return false;
						}
					}
				}				
			}
		}]
	},{
		xtype : '@{codeScreen}'
	}]
})
glu.defView('RS.actiontaskbuilder.TestControl',{
	flex : 1,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		cls : 'rs-dockedtoolbar',
		defaults : {
			cls: 'rs-small-btn rs-btn-dark',		
		},
		items:['executeTest']
	}],
	items: [{
		xtype: 'tabpanel',
		height : 450,
		flex : 1,
		cls: 'rs-tabpanel-dark',
		activeItem : '@{activeTestSample}',
		defaults : {
			flex : 1
		},
		items: [{
			xtype: 'textarea',
			title: '~~defaultTestSampleTitle~~',
			closable: false,
			value : '@{defaultTestSample}'		
		},{
			iconCls: 'icon-large icon-plus',
			testTabCounter: 1,
			listeners: {
				beforeactivate: function(addTab){
					var parent = addTab.up();
					var numOfTabs = parent.items.length;					
					if(numOfTabs < 8){
						var testSampleNumber = addTab.testTabCounter + 1;
						addTab.testTabCounter++;
						var testSampleTitle = 'Test Sample ' + testSampleNumber; //Use this as id to register it to vm
						parent.insert(numOfTabs - 1,{
							xtype: 'textarea',
							title: testSampleTitle,
							closable : true
						})						
					}
					return false;
				}				
			}
		}],
		listeners : {
			afterrender : function(panel){
				this._vm.registerTestTab(panel);
			}
		}
	},{
		xtype: 'displayfield',
		value: '@{testResultTitle}',
		htmlEncode: false,	
		margin: '10 0 0 0'
	},{
		xtype: 'grid',
		minHeight : 150,
		cls : 'rs-grid-dark',
		store: '@{testResultStore}',
		columns: [{
			header: 'Name',
			dataIndex: 'name',
			width: 200
		}, {
			header: 'Value',
			dataIndex: 'value',
			flex: 1,
			renderer: function(value) {
				return Ext.util.Format.nl2br('<pre>' + Ext.String.htmlEncode(value) + '</pre>')
			}
		}]
	}]	
	
})
RS.actiontaskbuilder.parserXMLConfig = {	
	region : 'center',	
	xtype : 'container',		
	html: '<iframe spellcheck="false" autocomplete="off" frameborder="0" style="width:100%;height:100%;border:1px solid #999;line-height: 0;" src=""></iframe>',
	sample: '@{sample}',
	parser: {
		iframe: null,
		win: null,
		parserHintElement : null,
		initParser: function(iframe){		
			var me = this;			
			me.iframe = iframe;

			me.win = iframe.contentWindow;	
			me.parserHintElement = '<div class=parserHintText contenteditable="false">PASTE YOUR OUTPUT SAMPLE HERE.</div>';	
			var document = this.cdoc();		
			var docBody = document.body;
			docBody.innerHTML = '<pre class="parser-content" contenteditable="true">' + me.parserHintElement + '</pre>';				
			if (docBody && ('spellcheck' in docBody))
				docBody.spellcheck = false;

			//Disable typing on this iframe ( might add feature to allow user to type in future)
			Ext.fly(document).on('keydown', function(e) {
				if (!e.ctrlKey)
					e.preventDefault();
			});

			//Events on parser's sample editor.
			docBody.onpaste = function(e) {
				if(!me.eventEmitter.isViewOnly){
					var rawData = e && e.clipboardData ? e.clipboardData.getData('text/plain'): window.clipboardData.getData('Text');
					//Prettify XML
					rawData = vkbeautify.xml(rawData);
					var lines = rawData.split(/\r*\n/);
					var data = lines.join(Ext.is.Windows ? '\r\n': '\n');
					var asPasted = true;			
					me.eventEmitter.fireEvent('sampleChanged', me, data, asPasted);	
				}
				return false;
			}			
			me.eventEmitter.fireEvent('parserIsLoaded',me,true);		
		},
		cdoc: function () {
			return this.win.document;
		},	
		updateParserView: function(currentSampleDom, forClear){
			var html = forClear ? this.parserHintElement : this.renderNode(currentSampleDom,0);
			this.cdoc().body.innerHTML = '<pre class="parser-content" contenteditable="true">' + html + '</pre>';
			this.liveXml();
		},
		renderNode: function(node, indent) {			
			var id = Ext.data.IdGenerator.get('uuid').generate();
			node.pnid = id;
			if (Ext.isFunction(node.setAttribute))
				node.setAttribute('pnid', id);
			var str = '';
			switch (node.nodeType) {
				case 1:
					if (node.ignore) {
						str = Ext.String.format(
							'<span pnid="{0}" class="xmlSp rootText">{1}</span>',
							id,
							Ext.htmlEncode(Ext.String.format('<{0}>{1}</{0}>', node.tagName, node.innerHTML)));
						break;
					}
					var text = [];
					if (node.tagName != 'resolveparserroot' && node.tagName != 'error')
						text.push(this.preTag(node.tagName, node.attributes, id));
					if (node.tagName != 'error')
						Ext.each(node.childNodes, function(child) {
							text.push(this.renderNode(child, indent + 1));
						}, this)
					else
						text.push(Ext.htmlEncode(node.textContent))
					if (node.tagName != 'resolveparserroot' && node.tagName != 'error')
						text.push(this.postTag(node.tagName, id));
					var textStyle = '';
					if (node.tagName == 'error')
						textStyle = 'style="color:red"';					
					str = Ext.String.format(
						'<span pnid="{0}" class="xmlSp element"{2}>{1}</span>',
						id,
						text.join(''),
						textStyle);
					break;
				case 3:
					str = Ext.htmlEncode(node.textContent)
						.replace(/ /g, '<span ignore class="xmlSp space">&nbsp;</span>')
						.replace(/\n/g, '<span ignore class="xmlSp newline"><br></span>');
					str = Ext.String.format(
						'<span pnid="{0}" class="xmlSp {2}">{1}</span>',
						id,
						str, (node.parentNode.tagName != 'resolveparserroot' && node.parentNode.tagName != 'error' ? 'text': 'rootText'));
					break;
				case 4:
					//DEPRECATED
					str = Ext.htmlEncode(node.textContent)
						.replace(/ /g, '<span class="xmlSp space"></span>')
						.replace(/\n/g, '<span class="xmlSp newline"></span><br>');
					str = Ext.String.format(
						'<span pnid="{0}" class="xmlSp cdata"{2}>{1}</span>',
						id,
						str);
					break;
				case 9:
					var text = [];
					Ext.each(node.childNodes, function(child) {
						text.push(this.renderNode(child, 0));
					}, this)
					str = text.join('');
					break;
			}
			return str;
		},
		preTag: function(tagName, attrs, pnid) {
			var kv = [];
			for (var i = 0; i < attrs.length; i++) {
				if (attrs[i].name == 'pnid')
					continue;
				kv.push(Ext.String.format(
					'<span class="xmlSp attribute"{2}><span class="xmlSp attrName">{0}</span>=<span class="xmlSp attrValue">"{1}"</span></span>',
					attrs[i].name,
					Ext.String.escape(attrs[i].value).replace(/"/g, '\\"')
				));
			}

			return Ext.String.format(
				'<span class="xmlSp tag preTag">{0}</span><span pnid="{4}" class="xmlSp tagName">{1}</span>{3}<span class="xmlSp tag preTag">{2}</span>',
				Ext.htmlEncode('<'),
				tagName,
				Ext.htmlEncode('>'),
				kv.length > 0 ? ' ' + kv.join(' '): '',
				pnid
			);
		},
		postTag: function(tagName, id) {
			return Ext.String.format(
				'<span class="xmlSp tag postTag">{0}</span><span pnid="{3}" class="xmlSp tagName">{1}</span><span class="xmlSp tag postTag">{2}</span>',
				Ext.htmlEncode('</'),
				tagName,
				Ext.htmlEncode('>'),
				id
			);
		},
		isNodeSelectedByXPath: function(node, xpathSelections) {
			return xpathSelections.indexOf(node) != -1
		},
		getSelectedStyle: function(color) {
			return ' highlighted style="background-color:' + color + '"';
		},		
		toggleElement: function(node, on) {
			Ext.fly(node.parentNode)[on ? 'addCls': 'removeCls']('emphasize-xml');
		},
		toggleNonElement: function(node,on){
			Ext.fly(node)[on ? 'addCls': 'removeCls']('emphasize-xml');
		},	
		liveXml: function() {
			var me = this;
			var nodes = Ext.fly(this.cdoc().body).query('.xmlSp');
			Ext.each(nodes, function(node) {
				if(node.hasAttribute('ignore')){
					Ext.fly(node).on('mouseenter', function(event) {
						event.preventDefault();
						event.stopPropagation();
					})
					Ext.fly(node).on('click', function(event) {
						event.preventDefault();
						event.stopPropagation();
					})
					Ext.fly(node).on('leave', function(event) {
						event.preventDefault();
						event.stopPropagation();
					})
				}
				else
				{
					Ext.fly(node).on('mouseenter', function() {
						if(!me.eventEmitter.isViewOnly){
							if (Ext.fly(node).hasCls('tagName'))
								this.toggleElement(node, true);						
							else if (Ext.fly(node).hasCls('text') || Ext.fly(node).hasCls('attribute') || Ext.fly(node).hasCls('cdata'))
								this.toggleNonElement(node, true);
						}
					}, this);
					Ext.fly(node).on('click', function() {
						if(!me.eventEmitter.isViewOnly){
							if (Ext.fly(node).hasCls('tagName'))
								this.eventEmitter.fireEvent('nodeSelectionChanged', node, 'element');
							else if (Ext.fly(node).hasCls('text'))
								this.eventEmitter.fireEvent('nodeSelectionChanged', node, 'text');
							else if (Ext.fly(node).hasCls('attribute'))
								this.eventEmitter.fireEvent('nodeSelectionChanged', node, 'attribute');
							else if (Ext.fly(node).hasCls('cdata'))
								this.eventEmitter.fireEvent('nodeSelectionChanged', node, 'cdata');
						}
					}, this);
					Ext.fly(node).on('mouseleave', function() {
						if(!me.eventEmitter.isViewOnly){
							if (Ext.fly(node).hasCls('tagName'))
								this.toggleElement(node, false);						
							else if (Ext.fly(node).hasCls('text') || Ext.fly(node).hasCls('attribute') || Ext.fly(node).hasCls('cdata'))
								this.toggleNonElement(node, false);
						}
					}, this);
				}			
			}, this);
		}
	},
	listeners: {
		afterrender: function() {					
			var iframe = this.el.query('iframe')[0];
			iframe.src = '/resolve/actiontask/editorstub.jsp?type=xml&OWASP_CSRFTOKEN=' + clientVM.getPageToken('actiontask/editorstub.jsp');
			var me = this;			
			me.parser.eventEmitter = this;
			//Initialize Parser Once the frame is ready.	
			if (iframe.attachEvent)
				iframe.attachEvent('onload', function() {
					me.parser.initParser(iframe);
				});
			else if (iframe.addEventListener){
				iframe.addEventListener('load', function() {
					me.parser.initParser(iframe);
				}, false);
			}
			this._vm.on('renderSample', function(forClear){
				me.parser.updateParserView(this.currentSampleDom, forClear);
			});
			this._vm.on('hightLightSelections', function(xpathSelections, currentColor, scroll) {
				me.parser.highLightSelections(xpathSelections, currentColor, scroll);
			});
		},
		sampleChanged: '@{updateSample}',
		selectXPath: '@{selectXPath}',	
		parserIsLoaded: function(){
			this._vm.set('parserReady', true);
		},
		nodeSelectionChanged: function(node, type) {
			var current = node;
			while (!Ext.fly(current).hasCls('element'))
				current = current.parentNode;
			this.fireEvent('selectXPath', this, type, current.getAttribute('pnid'), node);
		},
		beforedestroy : function(){
			//Mark this iframe dom to be collected by GC. 
			//By default events that are registered on #document is marked as not collected by GC (Not sure why Extjs is doing that)
			if(this.rendered){
				var document = this.parser.cdoc();
				var iframeDom = Ext.fly(document);
				if(iframeDom && iframeDom.$cache)
					iframeDom.$cache.skipGarbageCollection = false;
			}
		}
	}
};

glu.defView('RS.actiontaskbuilder.XMLParser',{
	itemId : 'xml',
	layout : 'card',
	activeItem : '@{activeScreen}',
	dockedItems: [{
		xtype: 'toolbar',
		cls : 'rs-dockedtoolbar',
		defaults: {
			cls: 'rs-small-btn rs-btn-dark'			
		},
		items:['clearSample','|','showCode','showMarkup']
	}],	
	cls: 'xml-parser-wrapper',	
	items : [{
		//Markup Screen
		xtype : 'container',		
		layout : {
			type : 'vbox',
			align : 'stretch'
		},	
		items:[{
			xtype: 'container',	
			layout : 'border',	
			height : '@{controlHeight}',					
			items: [RS.actiontaskbuilder.parserXMLConfig,{
				//Control
				width : 600,	
				region : '@{controlBorderRegion}',
				margin : '@{controlMargin}',
				xtype : 'container',
				layout : {
					type : 'vbox',
					align : 'stretch'
				},
				items : [{
					xtype : 'fieldset',	
					padding : '5 10 10',							
					height : 180,
					title : '~~xpathControl~~',
					layout : {
						type : 'vbox',
						align : 'stretch'
					},				
					items: [{
						xtype: 'toolbar',
						cls : 'rs-dockedtoolbar',
						defaults: {
							cls: 'rs-small-btn rs-btn-dark',				
						},			
						items: ['clearPath','capturePath']		
					},{
						xtype : 'panel',
						augmentedXPath: '@{augmentedXPath}',
						flex : 1,
						autoScroll : true,						
						style: 'border:1px solid #d9d9d9; padding:5px; background: white; width: 100%',		
						setAugmentedXPath: function(augmentedXPath) {
							this.augmentedXPath = augmentedXPath;
							var tokens = [];
							Ext.each(augmentedXPath, function(augmentedToken) {
								tokens.push(this.cookToken(augmentedToken))
							}, this);
							var beautified = '<div class="xpath">&#47;' + tokens.join('') + '</div>';
							if (!this.getContentTarget())
								return;
							this.getContentTarget().dom.innerHTML = beautified;
							this.liveXPath();
						},
						cookPredicates: function(augmentedToken) {
							var predicates = augmentedToken.predicates;
							var pnid = augmentedToken.pnid;
							var tokens = [];
							Ext.each(predicates, function(p) {
								if (p.type == 'text')
									tokens.push(this.cookTextPredicate(p));
								else if (p.type == 'attribute')
									tokens.push(this.cookAttributePredicate(p));
								else if (p.type == 'custom')
									tokens.push(this.cookCustomPredicate(p));
							}, this);
							return Ext.String.format('<span pnid="{1}" ptid="predicates" class="xpath-predicate">&#91;{0}&#93;</span>', tokens.join('&nbsp;' + augmentedToken.op + '&nbsp;'), pnid);
						},
						cookTextPredicate: function(predicate) {
							return Ext.String.format(
								'<span pnid="{2}" ptid="{3}" class="xpath-predicate-text"><span class="xpath-predicate-condition">{0}</span>(., \'{1}\')</span>',
								predicate.condition,
								Ext.htmlEncode(predicate.value),
								predicate.pnid,
								predicate.ptid
							);
						},
						cookAttributePredicate: function(predicate) {
							if (['contains', 'starts-with', 'ends-with'].indexOf(predicate.condition) != -1){
								return Ext.String.format(
									'<span pnid="{3}" ptid="{4}" class="xpath-predicate-attribute">{1}(@{0},{2})</span>',
									predicate.name,
									predicate.condition, (predicate.isNumber ? predicate.value: Ext.htmlEncode('"' + predicate.value + '"')),
									predicate.pnid,
									predicate.ptid
								);
							}
							else 
							return Ext.String.format(
								'<span pnid="{3}" ptid="{4}" class="xpath-predicate-attribute">@{0} {1} {2}</span>',
								predicate.name,
								predicate.condition, (predicate.isNumber ? predicate.value: Ext.htmlEncode('"' + predicate.value + '"')),
								predicate.pnid,
								predicate.ptid
							);
						},
						cookCustomPredicate: function(predicate) {
							return Ext.String.format('<span pnid="{1}" ptid="{2}" class="xpath-predicate-customized">{0}</span>', predicate.value, predicate.pnid, predicate.ptid);
						},
						cookToken: function(augmentedToken) {
							if (augmentedToken.type == 'selector')
								return this.cookSelector(augmentedToken);
							var predicate = augmentedToken.predicates.length == 0 ? '': this.cookPredicates(augmentedToken);
							return Ext.String.format(
								'&#47;<span pnid={2} ptid={3} class="xpath-element">{0}</span>{1}',
								augmentedToken.element,
								predicate,
								augmentedToken.pnid,
								augmentedToken.ptid
							);
						},
						cookSelector: function(augmentedToken) {
							return Ext.String.format('<span ptid={0} ignore class="xpath-selector-text">/{1}</span>', augmentedToken.ptid, augmentedToken.value);
						},
						previousSelectedPathId : null,
						liveXPath: function() {				
							var list = this.getEl().query('*[class*="xpath"]');
							var me = this;
							Ext.each(list, function(li) {
								var path = 	Ext.get(li);
								if(this.previousSelectedPathId == path.dom.getAttribute('pnid'))
									path.dom.setAttribute('selected','');				
								path.on('click', this.showDetail, this);
							}, this);
						},
				
						showDetail: function(evt, dom) {
							evt.stopEvent(); //so that the predicate container span won't receive the click event

							//Already selected or selection is not on node, just return. This will depricate checking for leaf node on VM.
							if(dom.hasAttribute('selected') || dom.className.indexOf('xpath-element') == -1)
								return;
							else{
								var list = this.getEl().query('*[class*="xpath"]');													
								Ext.each(list, function(li) {							
									li.removeAttribute('selected');
								}, this);
								dom.setAttribute('selected','');
								//Save this so we can selected later if this path is updated
								this.previousSelectedPathId = dom.getAttribute('pnid');
							}

							var parentTokenNode = dom;
							var ptid = '';
							while (parentTokenNode.parentNode && !(ptid = parentTokenNode.getAttribute('ptid')))
								parentTokenNode = parentTokenNode.parentNode
							if (!ptid)
								return;							
						
							var pnid = parentTokenNode.getAttribute('pnid');
							var height = Ext.fly(dom).getHeight();
							var x = Ext.fly(dom).getX();
							var y = Ext.fly(dom).getY();
							var parser = this.up().up().down('*[parserType="xml"]');
							this.fireEvent('showDetail', this, pnid, ptid, function(wnd) {
								var bodyHeight = Ext.getBody().getHeight();
								var bodyWidth = Ext.getBody().getWidth();
								if (x + wnd.getWidth() > bodyWidth)
									x = bodyWidth - wnd.getWidth();
								if (y + wnd.getHeight() > bodyHeight)
									y -= wnd.getHeight();
								else
									y += height;
								return [x, y];
							});
						},
						removeToken: function(evt, dom) {
							evt.stopEvent();
							var parentTokenNode = dom;
							var ptid = '';
							while (parentTokenNode.parentNode && !(ptid = parentTokenNode.getAttribute('ptid')))
								parentTokenNode = parentTokenNode.parentNode
							if (!ptid)
								return;
							var pnid = parentTokenNode.getAttribute('pnid');
							this.fireEvent('removeToken', this, pnid, ptid);
						},
						listeners: {
							showDetail: '@{showDetail}',
							removeToken: '@{removeToken}'
						}
					}]						
				},{
					flex : 1,
					margin : 0,				
					xtype : '@{xPathDetail}'				
				}],
				listeners : {
					afterrender : function(panel){
						panel._vm.on('embeddedLayout', function(isEmbedded){								
							if(isEmbedded){
								panel.setBorderRegion('south');							
							}
							else {
								panel.setBorderRegion('east');							
							}
						});
					}
				}
			}]	
		},{
			xtype: 'component',		
			html: '~~variableTable~~',
			margin : '15 0 8 0',	
		},{
			xtype: 'component',		
			hidden: '@{!variableStoreIsEmpty}',
			html: '~~noVariableText~~'
		},{
			xtype: 'grid',		
			cls: 'rs-grid-dark',						
			store: '@{variableStore}',
			columns: '@{variableColumn}',
			hidden: '@{variableStoreIsEmpty}',
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}],
			selModel: {
				selType: 'cellmodel'
			},
			listeners: {					
				removeVariable: '@{removeVariable}',
				validateedit: function(editor, context){
					if(context.field == "variable"){						
						var store = context.store;
						var newVal = context.value;
						var currentRowIdx = context.rowIdx;
						if(store.findExact(context.field, newVal) != currentRowIdx && store.findExact(context.field, newVal) != -1){
							this._vm.displayDuplicateNameMsg();
							return false;
						}
						else if(!RS.common.validation.VariableName.test(newVal)){
							this._vm.displayInvalidCharacter();
							return false;
						}
					}
				}		
			}
		}]
	},{
		xtype : '@{codeScreen}'
	}]
})
glu.defView('RS.actiontaskbuilder.XPathTokenEditor', {
	xtype : 'fieldset',
	title : '@{title}',
	cls : 'xpath-detail',
	padding : 0,
	defaults : {
		margin : '4 0',
		labelWidth : 80		
	},
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	items : [{
		xtype :  'component',
		html : '~~noProperty~~',
		hidden : '@{!noProperty}',
		padding : '0 10'
	},{
		xtype : 'container',		
		flex : 1,
		defaults : {
			padding : '0 10'
		},
		layout : {
			type : 'vbox',
			align : 'stretch'
		},
		hidden : '@{noProperty}',
		items : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			items : [{			
				cls : 'rs-btn-dark rs-small-btn',
				name : 'update', 
			},{			
				cls : 'rs-btn-dark rs-small-btn',
				name : 'removeNode',
			}]
		},{
			xtype : 'textfield',
			name : 'selector',			
			labelWidth : 80
		},{
			xtype : 'component',
		 	html : '~~predicates~~',
		 	margin : '5 0'
		},{
			xtype : 'panel',
			padding : 0,
			flex : 1,
			autoScroll : true,		
			dockedItems : [{
				xtype : 'toolbar',
				padding : '0 10',
				cls : 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-small-btn rs-btn-dark',
				},
				items : [{
					xtype : 'button',
					text : 'Add',
					plain : true,
					menu : ['addTextPredicate', 'addAttrPredicate', 'addCustPredicate']
				},{
					xtype : 'tbseparator'
				},{
					fieldLabel : 'Condition',
					xtype: 'radiofield',
					name: 'operator',
					value: '@{and}',			
					boxLabel: '~~and~~',
					labelWidth : 80
				}, {
					xtype: 'radiofield',
					name: 'operator',
					value: '@{!and}',
					hideLabel: true,
					boxLabel: '~~or~~'
				}]
			}],
			items : [{
				xtype : 'component',
				padding : '0 10',
				html : '~~noPredicate~~',
				hidden : '@{!noPredicate}',
			},{
				xtype : 'container',
				overflowY : 'scroll',
				padding : '0 0 0 10',				
				items : '@{predicates}',			
				hidden : '@{noPredicate}',
			}]
		}]
	}]
});
glu.defView('RS.actiontaskbuilder.XPathTextPredicate', {
	xtype : 'container',
	layout: 'hbox',
	defaults: {
		hideLabel: true,
		labelWidth : 80
	},	
	items: [{
		xtype: 'displayfield',		
		hideLabel: false,
		fieldLabel: '~~text~~'		
	}, {
		xtype: 'combo',	
		name: 'condition',
		displayField: 'name',
		valueField: 'value',
		store: '@{..textConditionStore}',
		width: 120,
		queryMode: 'local',
		margin : '0 5 0 0',
	}, {
		xtype: 'textfield',		
		name: 'value',
		flex: 1,	
		margin : '0 5 0 0',
	}, {
		xtype: 'container',	
		width : 20,	
		items : [{
			xtype : 'component',						
			style: {				
				'color': '#999',
				'padding-left': '3px',
				'padding-top': '3px',
			},			
			autoEl: {				
				cls: 'icon-minus-sign-alt icon-large'
			},
			listeners: {
				render: function() {
					this.getEl().on('mouseover', function() {
						this.setStyle('cursor', 'pointer');
					});
					this.getEl().on('click', function(e) {
						e.stopPropagation();
						this.fireEvent('remove');
					}, this);
				},
				remove: '@{remove}'
			}	
		}]
	}]
});

glu.defView('RS.actiontaskbuilder.XPathAttrPredicate', {
	xtype : 'container',
	layout: 'hbox',
	defaults: {
		hideLabel: true,
		labelWidth : 80
	},	
	items: [{
		xtype: 'displayfield',
		hideLabel: false,	
		fieldLabel: '~~attribute~~'	
	}, {
		xtype: 'combo',		
		name: 'name',
		store: '@{..attributeStore}',
		width: 120,
		displayField: 'name',
		valueField: 'name',
		queryMode: 'local',
		margin : '0 5 0 0',
	}, {
		xtype: 'combo',
		name: 'condition',
		store: '@{..attrConditionStore}',
		width: 120,
		displayField: 'name',
		valueField: 'value',
		queryMode: 'local',
		margin : '0 5 0 0',
	}, {
		xtype: 'textfield',		
		name: 'value',
		flex: 1,
		margin : '0 5 0 0',
	}, {
		xtype: 'container',	
		width : 20,	
		items : [{
			xtype : 'component',						
			style: {				
				'color': '#999',
				'padding-left': '3px',
				'padding-top': '3px',
			},			
			autoEl: {				
				cls: 'icon-minus-sign-alt icon-large'
			},
			listeners: {
				render: function() {
					this.getEl().on('mouseover', function() {
						this.setStyle('cursor', 'pointer');
					});
					this.getEl().on('click', function(e) {
						e.stopPropagation();
						this.fireEvent('remove');
					}, this);
				},
				remove: '@{remove}'
			}	
		}]
	}]
});

glu.defView('RS.actiontaskbuilder.XPathCustomizedPredicate', {
	xtype : 'container',
	layout: 'hbox',
	margin : '0 0 5 0',
	items: [{
		xtype: 'textfield',	
		fieldLabel: '~~customized~~',
		name: 'value',		
		flex: 1,
		labelWidth : 80,
		margin : '0 5 0 0',
	}, {
		xtype: 'container',	
		width : 20,	
		items : [{
			xtype : 'component',								
			style: {				
				'color': '#999',
				'padding-left': '3px',
				'padding-top': '3px',
			},			
			autoEl: {				
				cls: 'icon-minus-sign-alt icon-large'
			},
			listeners: {
				render: function() {
					this.getEl().on('mouseover', function() {
						this.setStyle('cursor', 'pointer');
					});
					this.getEl().on('click', function(e) {
						e.stopPropagation();
						this.fireEvent('remove');
					}, this);
				},
				remove: '@{remove}'
			}	
		}]
	}]
})
glu.defView("RS.actiontaskbuilder.ParserEdit",{
	parentLayout: 'quickAccess',
	title: '@{title}',
	hidden: '@{hidden}',
	cls: '@{classList}',
 	header: '@{header}',
 	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '0 15 10 15',
	layout: 'card',
	activeItem: '@{activeItem}',
	dockedItems:[{		
		xtype: 'toolbar',
		cls : 'rs-dockedtoolbar',
		padding : '5 15 0',
		defaults : {
			cls : 'rs-med-btn rs-btn-dark'
		},
		items: [{
			xtype: 'radiogroup',
			style : 'visibility:@{parserSwitchVisibility}',
			layout: 'hbox',		
			fieldLabel: '~~outputFormat~~',
			labelStyle : 'font-weight:bold',
			cls: 'bold-label rs-dockedtoolbar',
			labelWidth: 130,
			defaultType: 'radio',
			value : '@{parserType}',	
			defaults:{	
				margin: '0 20 0 0',
				name : 'outputFormat'	
			},
			items: [{
				boxLabel: 'Text',
				checked: '@{textParserIsActive}',
				inputValue : 'text'
			},{
				boxLabel: 'Table',
				checked: '@{tableParserIsActive}',
				inputValue : 'table'	
			},{
				boxLabel: 'XML',
				checked: '@{xmlParserIsActive}',
				inputValue : 'xml'
			}]
		},'->',{			
			name: 'markupAndCapture',
			margin : 0,
			iconCls: 'icon-large icon-reply-all rs-icon'		
		},'test']
	},{
		xtype: 'toolbar',	
		dock: 'bottom',	
		padding : '0 15 10 15',
		items: ['->', { 
			name: 'close', 
			cls : 'rs-med-btn rs-btn-dark'
		}]
	}],
	items: [{
		xtype : '@{parserControl}'
	},{
		xtype : '@{testControl}'
	}],	
	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					if (panel._vm.isViewOnly) {
						panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
					} else {
						panel._vm.close();
					}
				}
			});			
		},
		resize : function(panel){			
			if(panel.getWidth() < 1000)
				panel._vm.set('smallResolution', true);
			else 
				panel._vm.set('smallResolution', false);
		},	
		afterRendered: '@{viewRenderCompleted}'
	}
});
RS.actiontaskbuilder.ReadOnlyParserGenericRenderer = {
	height : 480,
	html: '<iframe spellcheck="false" autocomplete="off" frameborder="0" style="background:#fcfaf8;width:100%;height:100%;border:1px solid #999;line-height: 0;" src=""></iframe>',
	dump : '@{dump}',
	parser : {
		parserType : '@{parserType}',
		initParser: function(iframe){		
			var me = this;			
			me.iframe = iframe;
			me.win = iframe.contentWindow;	
			var document = this.cdoc();
			var docBody = document.body;	
			docBody.innerHTML = '<pre class="parser-content" contenteditable="false"></pre>';		
			if (docBody && ('spellcheck' in docBody))
				docBody.spellcheck = false;			
			me.eventEmitter.fireEvent('parserIsLoaded',me,true);
		},
		cdoc: function(){
			return this.win.document;
		},
		placeHolders: {
			space: '<span marker="space" class="format">&nbsp;</span>',
			new_line:  '<span marker="new_line" class="return"><br></span>'
		},
		convert: function(text, pre, post) {
			if(!text)
				return "";
			var processedText = text.replace(/ /g, this.placeHolders.space).replace(Ext.is.Windows ? (/\r\n/g): (/\n/g), this.placeHolders.new_line);
			return processedText;
		},		

		//Text and Table Parser
		renderTextBaseMarkup: function(sample,markups){
			//Get pts for each markup
			var pts = [];
			var parseStartPosition = -1;
			var parseEndPosition = -1;
			var hasParseStart = false;
			var hasParseEnd = false;
			Ext.each(markups, function(markup){			
				if(markup.type == 'parseStart'){
					hasParseStart = true;
					parseStartPosition = markup.from + ( markup.included ? 0: markup.length );
					pts.push(parseStartPosition);
				}
				else if(markup.type == 'parseEnd'){
					hasParseEnd = true;
					parseEndPosition = markup.from + ( markup.included ? markup.length: 0 );
					pts.push(parseEndPosition);
				}
				else{
					pts.push(markup.from);
					pts.push(markup.from + markup.length);
				}
			})
			pts = Ext.Array.unique(pts);
			//Add start and end pts
			if(pts.indexOf(0) == -1)
				pts.push(0);
			if (pts.indexOf(sample.length) == -1)
				pts.push(sample.length);
			pts.sort(function(a, b) {
				return a > b ? 1: (a == b ? 0: -1);
			});
			var chopped = [];
			for (var i = 0; i < pts.length - 1; i++){
				if(parseStartPosition == pts[i]){
					chopped.push({
						type: "parseStart"					
					});
				}
				else if(parseEndPosition == pts[i]){
					chopped.push({
						type: "parseEnd"					
					});
				}
				chopped.push({
					text: sample.substring(pts[i], pts[i + 1]),
					from: pts[i],
					to: pts[i + 1],
					actionId: null
				});
			}
		
			//Sort all pieces and add type and id for each chopped piece.
			Ext.each(chopped, function(slice, idx) {
				Ext.each(markups, function(markup) {
					if(markup.type == slice.type && ( markup.type == "parseStart" || markup.type == "parseEnd")){ //Only slice with parseEnd and parseStart has a type at this point						
						slice.actionId = markup.actionId;
					}
					else if (markup.type != "parseStart" && markup.type != "parseEnd" && slice.from >= markup.from && slice.to <= (markup.from + markup.length)) {
						slice.actionId = markup.actionId;
						slice.type = markup.type;	
						slice.action = markup.action || 'default';				
						if(markup.action == 'capture' || markup.type == 'column'){
							slice.color = markup.color;
						}
						if(markup.type == 'column'){
							slice.col = markup.column;
						}
					}
				});
			});
			var done = [];
			var beginParseSection = false ;
			var endParseSection = false;
			Ext.each(chopped, function(slice, idx) {				
				var convertedContent = this.convert(Ext.htmlEncode(slice.text));
				if(slice.type == 'parseStart')
					beginParseSection = true;				
				if((hasParseStart && !beginParseSection) || (hasParseEnd && endParseSection)){
					done.push(Ext.String.format('<span unselectable boundary contenteditable="false">{0}</span>', convertedContent));
				}
				else {
					if (slice.actionId != null){
						if(slice.action == 'capture'){
							done.push(Ext.String.format('<span {1} marked unselectable contenteditable="false" action="capture" type={2} style="background:{3}" ignore>{0}</span>', convertedContent, 'actionId="' + slice.actionId + '"', slice.type, slice.color));
						}
						else if(slice.type == 'column'){
							done.push(Ext.String.format('<span {1} marked unselectable contenteditable="false" action="default" type={2} col={4} style="border-color:{3};border-width:2px;">{0}</span>', convertedContent, 'actionId="' + slice.actionId + '"', slice.type, slice.color, slice.col));
						}
						else
							done.push(Ext.String.format('<span {1} marked unselectable contenteditable="false" type={2}>{0}</span>', convertedContent, 'actionId="' + slice.actionId + '"', slice.type))
					}
					else
						done.push(convertedContent);
				if(slice.type == 'parseEnd')
					endParseSection = true;
				}
			},this);
			this.cdoc().body.innerHTML ='<pre class="parser-content" contenteditable="false">' + done.join('') +'</pre>';;			
		},

		//XML Parser
		renderXMLBaseMarkup : function(currentSampleDom){
			var html = this.renderNode(currentSampleDom,0);
			this.cdoc().body.innerHTML = '<pre class="parser-content" contenteditable="true">' + html + '</pre>';
		},
		renderNode: function(node, indent) {			
			var id = Ext.data.IdGenerator.get('uuid').generate();
			node.pnid = id;
			if (Ext.isFunction(node.setAttribute))
				node.setAttribute('pnid', id);
			var str = '';
			switch (node.nodeType) {
				case 1:
					if (node.ignore) {
						str = Ext.String.format(
							'<span pnid="{0}" class="xmlSp rootText">{1}</span>',
							id,
							Ext.htmlEncode(Ext.String.format('<{0}>{1}</{0}>', node.tagName, node.innerHTML)));
						break;
					}
					var text = [];
					if (node.tagName != 'resolveparserroot' && node.tagName != 'error')
						text.push(this.preTag(node.tagName, node.attributes, id));
					if (node.tagName != 'error')
						Ext.each(node.childNodes, function(child) {
							text.push(this.renderNode(child, indent + 1));
						}, this)
					else
						text.push(Ext.htmlEncode(node.textContent))
					if (node.tagName != 'resolveparserroot' && node.tagName != 'error')
						text.push(this.postTag(node.tagName, id));
					var textStyle = '';
					if (node.tagName == 'error')
						textStyle = 'style="color:red"';					
					str = Ext.String.format(
						'<span pnid="{0}" class="xmlSp element"{2}>{1}</span>',
						id,
						text.join(''),
						textStyle);
					break;
				case 3:
					str = Ext.htmlEncode(node.textContent)
						.replace(/ /g, '<span ignore class="xmlSp space">&nbsp;</span>')
						.replace(/\n/g, '<span ignore class="xmlSp newline"><br></span>');
					str = Ext.String.format(
						'<span pnid="{0}" class="xmlSp {2}">{1}</span>',
						id,
						str, (node.parentNode.tagName != 'resolveparserroot' && node.parentNode.tagName != 'error' ? 'text': 'rootText'));
					break;
				case 4:
					//DEPRECATED
					str = Ext.htmlEncode(node.textContent)
						.replace(/ /g, '<span class="xmlSp space"></span>')
						.replace(/\n/g, '<span class="xmlSp newline"></span><br>');
					str = Ext.String.format(
						'<span pnid="{0}" class="xmlSp cdata"{2}>{1}</span>',
						id,
						str);
					break;
				case 9:
					var text = [];
					Ext.each(node.childNodes, function(child) {
						text.push(this.renderNode(child, 0));
					}, this)
					str = text.join('');
					break;
			}
			return str;
		},
		preTag: function(tagName, attrs, pnid) {
			var kv = [];
			for (var i = 0; i < attrs.length; i++) {
				if (attrs[i].name == 'pnid')
					continue;
				kv.push(Ext.String.format(
					'<span class="xmlSp attribute"{2}><span class="xmlSp attrName">{0}</span>=<span class="xmlSp attrValue">"{1}"</span></span>',
					attrs[i].name,
					Ext.String.escape(attrs[i].value).replace(/"/g, '\\"')
				));
			}

			return Ext.String.format(
				'<span class="xmlSp tag preTag">{0}</span><span pnid="{4}" class="xmlSp tagName">{1}</span>{3}<span class="xmlSp tag preTag">{2}</span>',
				Ext.htmlEncode('<'),
				tagName,
				Ext.htmlEncode('>'),
				kv.length > 0 ? ' ' + kv.join(' '): '',
				pnid
			);
		},
		postTag: function(tagName, id) {
			return Ext.String.format(
				'<span class="xmlSp tag postTag">{0}</span><span pnid="{3}" class="xmlSp tagName">{1}</span><span class="xmlSp tag postTag">{2}</span>',
				Ext.htmlEncode('</'),
				tagName,
				Ext.htmlEncode('>'),
				id
			);
		},
	},
	listeners: {
		afterrender: function() {		
			var iframe = this.el.query('iframe')[0];
			iframe.src = '/resolve/actiontask/editorstub.jsp?OWASP_CSRFTOKEN=' + clientVM.getPageToken('actiontask/editorstub.jsp');

			var me = this;
			me.parser.eventEmitter = this;
			//Initialize Parser Once the frame is ready.	
			if (iframe.attachEvent)
				iframe.attachEvent('onload', function() {
					me.parser.initParser(iframe);
				});
			else if (iframe.addEventListener){
				iframe.addEventListener('load', function() {
					me.parser.initParser(iframe);
				}, false);
			}	
			this._vm.on('renderMarkup', me.parser.renderTextBaseMarkup, me.parser);
			this._vm.on('renderXML', me.parser.renderXMLBaseMarkup, me.parser);			
		},
		parserIsLoaded: function(){
			this._vm.set('parserReady', true);
		},
		beforedestroy : function(){
			//Mark this iframe dom to be collected by GC. 
			//By default events that are registered on #document is marked as not collected by GC (Not sure why Extjs doing that)
			if(this.rendered){
				var document = this.parser.cdoc();
				var iframeDom = Ext.fly(document);
				if(iframeDom && iframeDom.$cache)
					iframeDom.$cache.skipGarbageCollection = false;
			}
		}
	}
} 
glu.defView('RS.actiontaskbuilder.ParserRead',{
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: '@{classList}',
	hidden: '@{hidden}',
	header : '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,	
	animCollapse: false,
	bodyPadding: '10 15 12 15',
	layout : 'card',
	activeItem : '@{activeWindow}',
	dockedItems : [{
		xtype : 'toolbar',
		hidden : '@{hasNoContent}',
		padding : '5 15 0',
		defaults : {
			cls : 'rs-med-btn rs-btn-dark'
		},
		items : [{
			xtype : 'displayfield',
			fieldLabel : '~~outputFormat~~',
			labelStyle : 'font-weight:bold',
			labelWidth: 130,
			value : '@{parserTypeName}'
		},'->',{			
			name: 'markupAndCapture',
			margin : 0,
			iconCls: 'icon-large icon-reply-all rs-icon'		
		},'test']
	}],
	items : [{
		xtype : 'component',	
		margin : '0 0 10 0',
		html : '~~sourceCodeSectionEmptyText~~'
	},{	
		dockedItems: [{
			xtype: 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults: {
				cls: 'rs-small-btn rs-btn-dark',
				margin: '5 8 0 0'
			},
			items:['showCode','showMarkup']
		}],
		layout : 'card',
		activeItem : '@{activeMarkupScreen}',
		items : [{
			xtype : 'container',
			items : [RS.actiontaskbuilder.ReadOnlyParserGenericRenderer,
			{
				xtype: 'component',		
				html: '~~variableTable~~',
				margin : '15 0 8 0',	
			},{
				xtype: 'component',		
				hidden: '@{!variableStoreIsEmpty}',
				html: '~~noVariableText~~'
			},{
				xtype: 'grid',	
				cls: 'rs-grid-dark',						
				store: '@{variableStore}',
				columns: '@{variableColumn}',
				hidden: '@{variableStoreIsEmpty}',
				listeners : {
					afterrender : function(panel){
						this._vm.on('reconfigureVariableColumn', function(variableColumn){
							panel.reconfigure(undefined, variableColumn);
						})
					}
				}					
			}]
		},{		
			xtype : '@{codeScreen}'
		}]		
	},{
		xtype : '@{testControl}'
	}],	
	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},
		viewRendered: '@{viewRenderCompleted}'
	}	
})
glu.defView('RS.actiontaskbuilder.PreprocessorEdit', {
	parentLayout: 'quickAccess',
	xtype: 'codeeditorpanel',
	title: '@{title}',
	cls: '@{classList}',
	hidden: '@{hidden}',
	header: '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	padding: 0,
	bodyPadding: '10 15 15 15',
	minHeight: '@{minHeight}',
	height: '@{height}',

	resizable: {
		handles: 's',
		pinned: true,
		minHeight: '@{minResize}'
	},
	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	
	defaults: {
		margin: 0,
		padding: 0
	},

	items: [{
		xtype: 'AceEditor',
		parser: 'groovy',
		height: '@{height}',
		name: 'content',	
		flex: 1,		
		listeners: {
			change: function () {
				this.up('codeeditorpanel').fireEvent('contentchange', this, this);
			}
		}
	},{
		xtype: 'label',
		cls: '@{editorClass}',
		margin : '10 0 0 0',
		listeners: {
			afterrender: function(label) {
				this.up('codeeditorpanel').fireEvent('warningmsgshowed', this, label);
			}
		}
	}],

	dockedItems: [{
		xtype: 'toolbar',		
		padding : '0 15 10',
		dock: 'bottom',
		items: ['->', {			
			name: 'close',
			cls: 'rs-med-btn rs-btn-dark'
		}]
	}],

	listeners: {
		contentchange: '@{contentChange}',
		warningmsgshowed: '@{warningMsgShowed}',
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});	

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	}	
});

glu.defView('RS.actiontaskbuilder.PreprocessorRead', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: '@{classList}',
	hidden: '@{hidden}',
	header:'@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '10 15 15 15',
	minHeight: '@{minHeight}',
	height: '@{height}',

	resizable: {
		handles: 's',
		pinned: true,
		minHeight: '@{minResize}'
	},
	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	
	defaults: {
		margin: '0 0 10 0',
		padding: 0
	},

	items: [{
		xtype: 'AceEditor',
		parser: 'groovy',
		readOnly: true,
		height: '@{height}',
		name: 'content',	
		cls: 'editor-disabled',
		flex: 1,
		highlightActiveLine: false, 
		highlightGutterLine: false,
		showCursor: false,
		hidden: '@{contentIsEmpty}'		
	}, {
		xtype: 'displayfield',
		hidden: '@{!contentIsEmpty}',
		margin: 0,
		padding: 0,
		value: '~~sourceCodeSectionEmptyText~~'
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},		
		viewRendered: '@{viewRenderCompleted}'
	}
});

glu.defView('RS.actiontaskbuilder.dummy') //to make sure the ns is init before creating the factory
RS.actiontaskbuilder.views.quickAccessFactory = function(actualView) {
	if (actualView.listeners) {
		Ext.apply(actualView.listeners, {
			show: function(panel) {
				panel.fireEvent('showsectioncallback', panel);
			},
			expand: function(panel) {
				panel.fireEvent('incnumsectionsopen', panel);
			},
			beforeexpand: function(panel) {
				panel.fireEvent('toggleresizemaxbtn', panel);
				return true;
			},
			collapse: function(panel) {
				panel.fireEvent('decnumsectionsopen', panel);
				panel.fireEvent('collapsesectioncallback', panel);
			},
			beforecollapse: function(panel) {
				panel.fireEvent('toggleresizemaxbtn', panel);
				return true;
			},
			showsectioncallback: '@{showSectionCallback}',
			collapsesectioncallback: '@{collapseSectionCallback}',
			incnumsectionsopen: '@{incNumSectionsOpen}',
			decnumsectionsopen: '@{decNumSectionsOpen}',
			toggleresizemaxbtn: '@{toggleResizeMaxBtn}',
		});
	}
	return actualView;
}

glu.defView('RS.actiontaskbuilder.ReferencesRead', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: '@{classList}',
	header: '@{header}',
	hidden: '@{hidden}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: 15,
	items: [{
		xtype: 'displayfield',
		hidden: '@{!gridIsEmpty}',
		margin: 0,
		padding: 0,
		value: '~~referencesEmptyText~~'
	}, {
		id: 'referencesTab',
		autoScroll: true,
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'references',
		store: '@{referencesStore}',
		hidden: '@{gridIsEmpty}',
		columns: [{
			header: '~~name~~',
			dataIndex: 'name',
			hideable: false,
			filterable: false,
			sortable: true,
			flex: 1
		}, {
			header: '~~refInContent~~',
			dataIndex: 'refInContent',
			filterable: false,
			width: 100,
			renderer: RS.common.grid.booleanRenderer()
		}, {
			header: '~~refInMain~~',
			dataIndex: 'refInMain',
			filterable: false,
			width: 100,
			renderer: RS.common.grid.booleanRenderer()

		}, {
			header: '~~refInAbort~~',
			dataIndex: 'refInAbort',
			filterable: false,
			width: 100,
			renderer: RS.common.grid.booleanRenderer()
		}],
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTooltip: RS.common.locale.viewColumnTooltip,
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'name'
		},
		listeners: {
			editAction: '@{editReference}'
		}
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},
		viewRendered: '@{viewRenderCompleted}',
		hello: function() {return null}
	},
});
glu.defView('RS.actiontaskbuilder.RolesEdit', {
	parentLayout: 'quickAccess',
	title: '@{title}',	
	cls: 'block-model subEditModel',
	hidden: '@{hidden}',
	header: '@{header}',
	bodyPadding: '10 15 15 15',
	margin: '0 0 2.5 0',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});	

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});

			panel.getEl().on('click', function() {
				if (panel._vm.collapsed && !panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	},
	
	dockedItems: [{
		xtype: 'toolbar',		
		padding : '0 15 10',
		dock: 'bottom',		
		items: ['->', { 
			name: 'close',
			cls: 'rs-med-btn rs-btn-dark',			
			disabled: '@{!isButtonEnabled}'					
		}]
	}],

	items: [{
		xtype: 'checkbox',
		hideLabel: true,
		disabled: '@{savingNewTask}',
		name: 'defaultRoles',
		boxLabel: '~~defaultRolesText~~',
		listeners: {
			change: '@{defaultRolesModified}'
		}
	}, {
		xtype: 'displayfield',
		hidden: '@{!gridIsEmpty}',
		value: '~~rolesEmptyText~~',
		margin: 0,
		padding: 0
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		hidden: '@{gridIsEmpty}',
		disabled: '@{defaultRolesOrSavingNewTask}',
		itemId: 'rolesGrid',
		store: '@{accessStore}',
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		selModel: {
			selType: 'cellmodel'
		},
		listeners: {
			beforeedit: '@{updateAllowedRolesStore}'
		},
		columns: [{
			dataIndex: 'uname',
			flex: 5,
			header: '~~roleName~~',
			resizable: false,
			hideable: false,
			editor: {
				xtype: 'combo',
				cls: 'no-input-border combo',
				forceSelection: true,
				editable: false,
				store: '@{allowedRolesStoreForGridRow}',
				queryMode: 'local',
				displayField: 'text',
				valueField: 'value',
				width: '80%'		
			}
		}, {
			xtype: 'checkcolumn',
			dataIndex: 'read',
			flex: 1,
			header: '~~roleReadRight~~',
			resizable: false,
			hideable: false
		}, {
			xtype: 'checkcolumn',
			dataIndex: 'write',
			flex: 1,
			header: '~~roleWriteRight~~',
			resizable: false,
			hideable: false
		}, {
			xtype: 'checkcolumn',
			dataIndex: 'execute',
			flex: 1,
			header: '~~roleExecuteRight~~',
			resizable: false,
			hideable: false
		}, {
			xtype: 'checkcolumn',
			dataIndex: 'admin',
			flex: 1,
			header: '~~roleAdminRight~~',
			resizable: false,
			hideable: false,
			listeners: {
				afterrender: function (column) {
					if (this.up('grid')._vm.hideAdminColumn) {
						column.hide();
					} else {
						column.show();
					}
				}
			}
		}, {
			xtype: 'actioncolumn',		
			width : 45,
			resizable: false,
			hideable: false,
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record) {
					record.store.remove(record);
					view.ownerCt._vm.addRoleToDetail(record);
				}
			}]
		}]
	}, {
		xtype: '@{detail}',
		hidden: '@{..hideAddRoleForm}'
	}]
});
glu.defView('RS.actiontaskbuilder.SeveritiesAndConditionsRead', {
	parentLayout: 'quickAccess',
	title: '@{title}',		
	cls: '@{classList}',
 	hidden: '@{hidden}',
 	header: '@{header}',
	bodyPadding: '10 15',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		flex: 1,
		margin : '4 0'
	},
	items: [{
		xtype: 'component',		
		html: '~~severitiesTitle~~'
	}, {
		xtype: 'component',
		html: '~~noSeveritiesMessage~~',
		hidden: '@{isSeveritiesMessageHidden}'
	}, {
		xtype: 'container',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaults: {
			flex: 1
		},
		items: '@{severityItems}'
	}, {
		xtype: 'component',		
		html: '~~conditionsTitle~~'
	}, {
		xtype: 'component',
		html: '~~noConditionsMessage~~',
		hidden: '@{isConditionsMessageHidden}'
	}, {
		xtype: 'container',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaults: {
			flex: 1
		},
		items: '@{conditionItems}'
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},
		viewRendered: '@{viewRenderCompleted}'
	}	
});
glu.defView('RS.actiontaskbuilder.SeveritiesEdit', {
	parentLayout: 'quickAccess',
	title: '@{title}',		
	cls: 'block-model subEditModel',
 	hidden: '@{hidden}',
 	header: '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: '10 15',
	margin: '0 0 2.5 0',
	defaults: {
		margin: '5 0'
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: '@{items}',

	dockedItems: [{
		xtype: 'toolbar',		
		padding : '0 15 10',
		dock: 'bottom',
		items: ['->', {			
			name: 'close',
			cls: 'rs-med-btn rs-btn-dark'
		}]
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});	

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});

			panel.getEl().on('click', function() {
				if (panel._vm.collapsed && !panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	}
});

glu.defView('RS.actiontaskbuilder.SummaryAndDetailsRead', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: 'block-model',
 	header: '@{header}',
	hidden: '@{hidden}',
 	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: 15,
	defaults: {
		margin: '4 0'		
	},
	items: [{
		xtype: 'component',
		html: '~~summary~~',
		margin : '0 0 10 0'	
	},{
		xtype : 'component',
		html : '~~noSummaryDefined~~',
		hidden : '@{!noSummaryRule}'	
	},{
		xtype : 'grid',
		hidden : '@{noSummaryRule}',
		store : '@{summaryRuleStore}',
		cls : 'rs-grid-dark summary-detail-grid',
		columns : [{
			header : '~~severity~~',
			dataIndex : 'severity',
			sortable : false,
			align : 'center',
			width : 150,
			renderer : function(value, metaData){			
				metaData.tdAttr = 'status=' + value.toUpperCase();
				return value;     
			}
		},{
			header : '~~condition~~',
			dataIndex : 'condition',
			sortable : false,
			align : 'center',
			width : 150,
			renderer : function(value, metaData){			
				metaData.tdAttr = 'status=' + value.toUpperCase();
				return value;     
			}
		},{
			header : '~~textToDisplay~~',
			dataIndex : 'display',
			sortable : false,
			editor : 'textarea',
			flex : 1,
			renderer : RS.common.grid.plainRenderer()
		}]
	},{
		xtype: 'container',
		hidden: '@{!multipleSummaryRule}',
		layout: {
			type: 'hbox',
			align: 'middle'
		},
		margin: '10 0 0 0',
		items: [{
            xtype: 'button',
            iconCls: 'icon-info-sign',
            text: '',
            preventDefault: true,
            disabled: true,
		    height: 30,
		    width: 30,
   		    margin: '5 10 0 0',
		    padding: '5 5 5 7',
			style: {
				color: '#00daff',
				backgroundColor: '#d5f9ff',
				border: '1px solid #00daff',
				fontSize: '17px'
			}
        }, {
			xtype: 'component',
			html: '~~multipleRuleMessage~~',			
			flex: 1
		}]
	},{
		xtype: 'component',
		html: '~~details~~',
		margin : '25 0 10 0'	
	},{
		xtype: 'component',	
		html: '~~noDetailDefined~~',
		hidden: '@{!noDetailRule}',
	},{
		xtype : 'grid',
		hidden : '@{noDetailRule}',
		store : '@{detailRuleStore}',
		cls : 'rs-grid-dark summary-detail-grid',
		columns : [{
			header : '~~severity~~',
			dataIndex : 'severity',
			align : 'center',
			sortable : false,
			width : 150,
			renderer : function(value, metaData){			
				metaData.tdAttr = 'status=' + value.toUpperCase();
				return value;     
			}
		},{
			header : '~~condition~~',
			dataIndex : 'condition',
			align : 'center',
			sortable : false,
			width : 150,
			renderer : function(value, metaData){			
				metaData.tdAttr = 'status=' + value.toUpperCase();
				return value;     
			}
		},{
			header : '~~textToDisplay~~',
			dataIndex : 'display',
			editor : 'textarea',
			sortable : false,
			flex : 1,
			renderer : RS.common.grid.plainRenderer()
		}]
	},{
		xtype: 'container',
		hidden: '@{!multipleDetailRule}',
		layout: {
			type: 'hbox',
			align: 'middle'
		},
		margin: '10 0 0 0',
		items: [{
            xtype: 'button',
            iconCls: 'icon-info-sign',
            text: '',
            preventDefault: true,
            disabled: true,
		    height: 30,
		    width: 30,
   		    margin: '5 10 0 0',
		    padding: '5 5 5 7',
			style: {
				color: '#00daff',
				backgroundColor: '#d5f9ff',
				border: '1px solid #00daff',
				fontSize: '17px'
			}
        }, {
			xtype: 'component',
			html: '~~multipleRuleMessage~~',			
			flex: 1
		}]
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},
		viewRendered: '@{viewRenderCompleted}'
	}	
});

glu.defView('RS.actiontaskbuilder.SummaryEdit', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: 'block-model subEditModel',
	hidden: '@{hidden}',
	header: '@{header}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: 15,
	margin: '0 0 2.5 0',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},	
	dockedItems: [{
		xtype: 'toolbar',
		padding : '0 15 10',
		dock: 'bottom',
		items: ['->', {
			name: 'close',
			cls: 'rs-med-btn rs-btn-dark'
		}]
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});

			panel.getEl().on('click', function() {
				if (panel._vm.collapsed && !panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	},
	items : [{
		xtype : 'component',
		html : '~~noSummaryDefined~~',
		hidden : '@{!noRule}',
		margin: '10 0'
	},{
		xtype : 'component',
		html : '~~summaryDisplayRule~~',
		margin : '0 0 8 0',
		hidden : '@{noRule}'
	},{
		xtype : 'grid',
		hidden : '@{noRule}',
		store : '@{ruleStore}',
		cls : 'rs-grid-dark summary-detail-grid',	
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		columns : [{
			header : '~~severity~~',
			dataIndex : 'severity',
			align : 'center',
			width : 150,
			editor: {
				xtype: 'combo',				
				editable: false,			
				queryMode: 'local',
				displayField: 'name',
				valueField: 'name',
				store: '@{severityStore}',
				cls: 'no-input-border combo'				
			},
			renderer : function(value, metaData){			
				metaData.tdAttr = 'status=' + value.toUpperCase(); 
				return value;               	
			}
		},{
			header : '~~condition~~',
			dataIndex : 'condition',
			align : 'center',
			width : 150,
			editor: {
				xtype: 'combo',				
				editable: false,			
				queryMode: 'local',
				displayField: 'name',
				valueField: 'name',
				store: '@{conditionStore}',
				cls: 'no-input-border combo'				
			},
			renderer : function(value, metaData){			
				metaData.tdAttr = 'status=' + value.toUpperCase();
				return value;     
			}
		},{
			header : '~~textToDisplay~~',
			dataIndex : 'display',
			editor : 'textarea',
			flex : 1,
			renderer : RS.common.grid.plainRenderer()
		},{
			xtype: 'actioncolumn',		
			hideable: false,
			align: 'center',		
			width: 45,			
			resizable : false,
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record) {
					this.up('grid').fireEvent('removeRule', view, record);
				}
			}]
		}],
		listeners : {
			removeRule : '@{removeRule}'
		}
	},{		
		xtype : 'infoicon',
		displayText : '~~multipleRuleMessage~~',
		hidden: '@{!multipleRule}',
		margin: '8 0 0 0'
	},{
		xtype : 'fieldset',
		title: '~~summaryRule~~',
		margin: '20 0 0 0',
		padding: '5 15',
		layout : {
			type : 'hbox',
			align : 'stretch'
		},
		items : [{
			xtype : 'container',
			flex : 1,
			layout: {
				type : 'vbox',
				align : 'stretch'
			},
			defaults : {
				labelWidth: 130,			
				margin: '4 0',
				editable : false
			},
			items : [{
				xtype : 'combobox',
				store : '@{severityStore}',
				displayField : 'name',
				valueField : 'name',
				name : 'severity'
			},{
				xtype : 'combobox',
				store : '@{conditionStore}',
				displayField : 'name',
				valueField : 'name',
				name : 'condition'
			},{
				xtype : 'textarea',
				height : 250,
				fieldLabel : '~~textToDisplay~~',
				name : 'display',
				editable : true
			},{
				xtype: 'container',		
				margin: '4 0 4 135',
				layout: {
					type: 'hbox'
				},
				defaults : {
					margin: '0 4 0 0',
					hideLabel : true
				},
				items: [{
					xtype: 'combobox',
					name: 'fieldType',
					emptyText: '~~type~~',
					editable: false,
					queryMode: 'local',
					displayField: 'text',
					valueField: 'value',	
					store: '@{typesStore}',
					width : 100
				}, {
					xtype: 'combobox',		
					name: 'fieldName',
					emptyText : '~~fieldNameTextForOthers~~',			
					editable: '@{isFieldNameEditable}',
					queryMode: 'local',
					displayField: 'name',
					valueField: 'name',
					store: '@{parametersStore}',			
					flex: 1,
					listeners : {
						focus : function(){
							this.expand();
						}
					}		
				}, {
					xtype: 'button',
					cls : 'rs-small-btn rs-btn-dark',
					height : '100%',
					name :  'insertVariable',
					text : '~~insertVariable~~',
					margin : 0
				}]
			},{
				xtype : 'button',
				name : 'addRule',				
				maxWidth: 140,		
				cls : 'rs-small-btn rs-btn-dark',
				margin: '4 0 4 135'
			}]
		},{
			xtype : 'container',
			hidden : '@{..embed}',		
			flex : 1,
			margin : '44 0 0 50',
			layout : {
				type : 'vbox',
				align : 'stretch'
			},
			defaults : {
				margin : '4 0'
			},
			items : [{
				xtype : 'component',
				html : '~~previewLabel~~'
			},{
				xtype : 'component',			
				html : '@{preview}',						
				height : 250,
				autoScroll : true,
				cls : 'summary-detail-preview',
				listeners : {
					afterrender : function(component){
						this._vm.on('renderPreview', function(preview){
							component.update(preview);
						})
					}										
				}			
			},{
				xtype : 'checkbox',
				name : 'htmlEncode',
				hideLabel : true,
				boxLabel : '~~htmlEncodeSummary~~'
			}]
		}]
	}]
});

glu.defView('RS.actiontaskbuilder.TagsEdit', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: 'block-model',
	header: '@{header}',
	hidden: '@{hidden}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: 15,

	dockedItems: [{
		xtype: 'toolbar',		
		padding : '0 15 10',
		dock: 'bottom',
		items: ['->', {			
			name: 'close',
			cls: 'rs-med-btn rs-btn-dark'
		}]
	}],

	items: [{
		xtype: 'displayfield',
		hidden: '@{!gridIsEmpty}',
		value: '@{noTagsMessage}'
	}, {
		xtype: 'grid',
		store: '@{store}',
		cls : 'rs-grid-dark',
		name: 'tags',
		hidden: '@{gridIsEmpty}',
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		selModel: {
			selType: 'cellmodel'
		},
		listeners: {
			scope: this,
			afterrender: function (grid) {
				grid.fireEvent('gridrendercompleted', grid, grid);
			},
			gridrendercompleted: '@{gridRenderCompleted}',
			recordremoved: '@{recordRemoved}',
			recordadded: '@{recordAdded}'
		},
		columns: [{
			header: '~~uname~~',
			dataIndex: 'name',
			filterable: true,
			flex: 2,
			hideable: false,
			sortable: true,
			editor: {
				xtype: 'combobox',
				queryMode: 'local',
				displayField: 'text',
				valueField: 'value',
				store: '@{allowedTags}',
				editable: false,
				listeners: {
					select: function (combo, records) {
						this.fireEvent('tagitemchange', this, combo, records);
					},
					tagitemchange: '@{tagItemChange}'
				}
			}
		}, {
			header: '~~udescription~~',
			dataIndex: 'description',
			filterable: false,
			flex: 4,
			sortable: true
		}, {
			xtype: 'actioncolumn',		
			hideable: false,
			sortable: true,
			align: 'center',			
			width: 45,			
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record ) {
					this.up('grid').fireEvent('recordremoved', view, record);
				}
			}]
		}]
	}, {
		xtype: 'fieldset',
		hidden: '@{itemsExhausted}',
		title : '~~addTag~~',
		margin: '5 0 0 0',
		padding: 10,
		items: [{
			xtype: 'form',

			listeners: {
				afterrender: function (form) {
					form.fireEvent('registerform', this, form);
				},
				registerform: '@{registerForm}'
			},

			layout: {
				type: 'vbox',
				align: 'stretch'
			},

			defaults: {
				labelWidth: 130,
				margin: '0 0 5 0'		
			},

			items: [{
				xtype: 'combobox',
				name: 'name',
				emptyText: '~~selectTag~~',
				allowBlank: false,
				store: '@{allowedTags}',
				queryMode: 'local',
				displayField: 'text',
				valueField: 'value',
				cls: 'no-input-border combo',
				maxWidth: 500,
				editable: true,				
				listeners: {
					select: '@{tagSelected}',
					change: '@{validateDescription}'
				}
			}, {
				xtype: 'displayfield',
				name: 'description',
			}, {
				xtype: 'button',
				name: 'recordAdded',
				text: '~~addTag~~',
				cls: 'rs-small-btn rs-btn-dark',			
				margin: '0 0 0 135',
				maxWidth: 90
			}]
		}]
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('afterRendered', this, this);

			panel.header.el.on('click', function (e) {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
				e.stopPropagation();
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.close();
				}
			});
		},
		afterRendered: '@{viewRenderCompleted}'
	}	
});

glu.defView('RS.actiontaskbuilder.TagsRead', {
	parentLayout: 'quickAccess',
	title: '@{title}',
	cls: '@{classList}',
	header: '@{header}',
	hidden: '@{hidden}',
	collapsed: '@{collapsed}',
	collapsible: true,
	animCollapse: false,
	bodyPadding: 15,
	items: [{
		xtype: 'displayfield',
		hidden: '@{!gridIsEmpty}',
		margin: 0,
		padding: 0,
		value: '~~tagsEmptyText~~'
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		hidden: '@{gridIsEmpty}',
		store: '@{store}',
		columns: [{
			dataIndex: 'name',
			filterable: true,
			flex: 2,
			header: '~~uname~~',
			hideable: false,
			sortable: true
		}, {
			header: '~~udescription~~',
			dataIndex: 'description',
			filterable: false,
			flex: 4,
			sortable: true,
			hideable: false
		}]
	}],

	listeners: {
		afterrender: function (panel) {
			this.fireEvent('viewRendered', this, this);

			panel.header.el.on('click', function () {
				if (!panel._vm.isSectionMaxSize) {
					panel._vm.toggleExpandCollapsePanel();
				}
			});

			panel.header.el.on('dblclick', function () {
				if (panel._vm.isSectionMaxSize) {
					panel._vm.parentVM.resizeNormalSection(panel._vm.gluModel);
				}
			});
		},
		viewRendered: '@{viewRenderCompleted}'
	}	
});

glu.defView('RS.actiontaskbuilder.Version',{
	xtype: 'grid',
	cls : 'rs-grid-dark',
	name: 'versionsList',	
	selected: '@{versionsListSelections}',
	columns: '@{versionsListColumns}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [/*'viewVersion', 'compareVersion', */
		'rollbackVersion',
		'->', {
			iconCls: 'x-tbar-loading',
			handler: '@{loadVersion}',
			tooltip: '~~refresh~~'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'bottom',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		padding: '10 0 5 0',
		items: [{
			xtype: 'toolbar',
			defaults: {
				text: '',
			},
			cls: 'version-pagination',
			padding: '@{paginationPadding}',
			items: [{
				xtype: 'text',
				text: '@{paginationText}',
			}, {
				name: 'firstPage',
				disabled: '@{firstDisabled}',
				cls: 'pagination first-page',
				tooltip: '~~firstPage~~'
			}, {
				name: 'previousPage',
				disabled: '@{previousDisabled}',
				cls: 'pagination previous-page',
				tooltip: '~~previousPage~~'
			}, {
				name: 'nextPage',
				disabled: '@{nextDisabled}',
				cls: 'pagination next-page',
				tooltip: '~~nextPage~~'
			}, {
				name: 'lastPage',
				disabled: '@{lastDisabled}',
				cls: 'pagination last-page',
				tooltip: '~~lastPage~~'
			}]
		}, '->', '->', {	
			name : 'selectVersion',
			xtype: 'button',
			cls: 'rs-med-btn rs-btn-dark',
		}, {
			name : 'close',
			xtype: 'button',
			cls : 'rs-med-btn rs-btn-dark',
		}]
	}],    
	asWindow : {
		title: '~~versions~~',
		modal: true,
		cls : 'rs-modal-popup',
		padding : '15 15 0 15',
		listeners: {
			beforeshow: function() {
				this.setWidth(Math.round(Ext.getBody().getWidth() * 0.8));
				this.setHeight(Math.round(Ext.getBody().getHeight() * 0.8));
			}
		}
	}
});
glu.defView('RS.actiontask.ActionTaskPicker', {
    title: '@{title}',
    padding : 15,
    modal: true,
    cls : 'rs-modal-popup',
    defaults : {
        margin : '0 0 8 0'
    },
    layout : 'card',
    activeItem : '@{activeTab}',
    dockedItems : [{
        xtype : 'toolbar',
        ui: 'display-toolbar',
        defaultButtonUI: 'display-toolbar-button',
        items: ['->', {
            name: 'allTaskTab',
            pressed: '@{allTaskTabIsPressed}'
        }, {
            name: 'fromAutomationTab',
            pressed: '@{fromAutomationTabIsPressed}'
        }]
    }],
    items: [{
        xtype : '@{allTask}'
    },{
        xtype : '@{fromAutomation}'
    }],
    buttons: [{
        name : 'dump',
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'cancel',
        cls : 'rs-med-btn rs-btn-light'
    }],
    listeners: {
        beforeshow: function(win) {
            win.setWidth(Math.round(Ext.getBody().getWidth() * 0.8));
            win.setHeight(Math.round(Ext.getBody().getHeight() * 0.8));
        }
    }
});
glu.defView('RS.actiontask.AllTask',{
    layout: 'border',
    plugins: [{
        ptype: 'searchfilter',
        allowPersistFilter: false,
        hideMenu: true,
        useWindowParams: false
    }, {
        ptype: 'pager'
    }],
    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',
        cls: 'actionBar rs-dockedtoolbar',
        name: 'actionBar',
        items: ['menuPath', {
            name: 'module',
            handler: '@{module}'
        }]
    }],
    items: [{
        layout: 'card',
        region: 'west',
        width: 200,
        split : true,
        maxWidth: 300,
        activeItem: '@{activeItem}',
        items: [{
            xtype: 'treepanel',
            cls : 'rs-grid-dark',
            columns: '@{actionTaskTreeMenuPathColumns}',
            name: 'actionTaskTreeMenuStore',
            listeners: {
                selectionchange: '@{menuPathTreeSelectionchange}'
            }
        },{
            xtype: 'treepanel',
            cls : 'rs-grid-dark',
            columns: '@{actionTaskTreeModuleColumns}',
            name: 'actionTaskTreeModuleStore',
            root: {
                name: 'All',
                id: 'root_module_id',
                expanded: true
            },
            listeners: {
                selectionchange: '@{moduleTreeSelectionchange}'
            }
        }]
    }, {
        //Multi selection (for Result Macro)
        hidden : '@{!multiSelection}',
        region: 'center',
        xtype: 'grid',
        cls : 'rs-grid-dark',
        displayName: '~~actionTaskDisplayName~~',
        name: 'actionTaskGridStore',
        store: '@{actionTaskGridStore}',
        columns: '@{columns}',
        autoScroll: true,
        viewConfig: {
            enableTextSelection: true
        },
        plugins: [{
            ptype: 'resolveexpander',
            rowBodyTpl: new Ext.XTemplate(
                '<div resolveId="resolveRowBody" style="color:#333">{[this.formatDescription(values.udescription)]}{[this.convertJson(values.resolveActionInvoc.resolveActionParameters)]}</div>', {
                    formatDescription: function(desc) {
                        if (!desc)
                            return '';
                        else
                            return '<span class="actiontaskpicker-heading">Description:</span><br/><br/>&nbsp&nbsp&nbsp&nbsp' + Ext.String.htmlEncode(desc) + '<br/><br/>';
                    },
                    convertJson: function(params) {
                        if (!params)
                            return;
                        var inputRows = '<span class="actiontaskpicker-heading">Input Parameters</span>:<br/><br/>';
                        var outputRows = '<span class="actiontaskpicker-heading">Output Parameters</span>:<br/><br/>';
                        Ext.each(params, function(param, idx) {
                            if (!param.utype || param.utype.toLowerCase() == 'input')
                                inputRows += '&nbsp&nbsp&nbsp&nbsp' + param.uname + ' - ' + Ext.String.htmlEncode(param.udescription || '') + '<br/>'
                            else
                                outputRows += '&nbsp&nbsp&nbsp&nbsp' + param.uname + ' - ' + Ext.String.htmlEncode(param.udescription || '') + '<br/>'
                        });
                        return inputRows + '<br/>' + outputRows;
                    }
                }
            )
        }],
        selType:'checkboxmodel',
        selModel : {
            mode : 'simple'
        }
    },{
        //Single selection with option to view detail of task (for Automation Designer)
        hidden : '@{multiSelection}',
        region: 'center',
        xtype: 'grid',
        cls : 'rs-grid-dark',
        displayName: '~~actionTaskDisplayName~~',
        name: 'actionTaskGridStore',
        store: '@{actionTaskGridStore}',
        columns: '@{columns}',
        autoScroll: true,
        viewConfig: {
            enableTextSelection: true
        },
        plugins: [{
            ptype: 'resolveexpander',
            rowBodyTpl: new Ext.XTemplate(
                '<div resolveId="resolveRowBody" style="color:#333">{[this.formatDescription(values.udescription)]}{[this.convertJson(values.resolveActionInvoc.resolveActionParameters)]}</div>', {
                    formatDescription: function(desc) {
                        if (!desc)
                            return '';
                        else
                            return '<span class="actiontaskpicker-heading">Description:</span><br/><br/>&nbsp&nbsp&nbsp&nbsp' + Ext.String.htmlEncode(desc) + '<br/><br/>';
                    },
                    convertJson: function(params) {
                        if (!params)
                            return;
                        var inputRows = '<span class="actiontaskpicker-heading">Input Parameters</span>:<br/><br/>';
                        var outputRows = '<span class="actiontaskpicker-heading">Output Parameters</span>:<br/><br/>';
                        Ext.each(params, function(param, idx) {
                            if (!param.utype || param.utype.toLowerCase() == 'input')
                                inputRows += '&nbsp&nbsp&nbsp&nbsp' + param.uname + ' - ' + Ext.String.htmlEncode(param.udescription || '') + '<br/>'
                            else
                                outputRows += '&nbsp&nbsp&nbsp&nbsp' + param.uname + ' - ' + Ext.String.htmlEncode(param.udescription || '') + '<br/>'
                        });
                        return inputRows + '<br/>' + outputRows;
                    }
                }
            )
        }],
        selModel: {
            selType: 'resolvecheckboxmodel',
            columnTooltip: RS.common.locale.editColumnTooltip,
            columnTarget: '_self',
            columnEventName: 'editAction',
            columnIdField: 'id'
        },
        listeners: {
            editAction: '@{editActionTask}'
        }
    }]
});
glu.defView('RS.actiontask.FromAutomation',{
    padding : '15 0 0 0',
    plugins: [{
        ptype: 'pager'
    }],
    layout : 'fit',
    dockedItems : [{
        xtype : 'toolbar',
        cls : 'actionBar rs-dockedtoolbar',
        name: 'actionBar',
        items : [{
            xtype: 'combo',
            labelStyle : 'font-size: 16px;font-weight: bold;',
            labelWidth : 130,
            width : 700,
            name : 'automationSearch',
            store: '@{automationStore}',
            emptyText : '~~runbookNameEmptyText~~',
            displayField: 'ufullname',
            valueField: 'ufullname',
            queryMode: 'remote',
            typeAhead: false,
            minChars: 1,
            //queryDelay: 500,
            keyDelay : 300,
            //This will fix the issue where value in control in glu is out of sync with what send to server.
            ignoreSetValueOnLoad : true
        }]
    }],
    items : [{
        //Multi selection (for Result Macro)
        hidden : '@{!multiSelection}',
        xtype: 'grid',
        cls : 'rs-grid-dark',
        name: 'actionTaskGridStore',
        store: '@{actionTaskGridStore}',
        columns: '@{columns}',
        autoScroll: true,
        viewConfig: {
            enableTextSelection: true
        },
        plugins: [{
            ptype: 'resolveexpander',
            rowBodyTpl: new Ext.XTemplate(
                '<div resolveId="resolveRowBody" style="color:#333">{[this.formatDescription(values.udescription)]}{[this.convertJson(values.resolveActionInvoc.resolveActionParameters)]}</div>', {
                    formatDescription: function(desc) {
                        if (!desc)
                            return '';
                        else
                            return '<span class="actiontaskpicker-heading">Description:</span><br/><br/>&nbsp&nbsp&nbsp&nbsp' + Ext.String.htmlEncode(desc) + '<br/><br/>';
                    },
                    convertJson: function(params) {
                        if (!params)
                            return;
                        var inputRows = '<span class="actiontaskpicker-heading">Input Parameters</span>:<br/><br/>';
                        var outputRows = '<span class="actiontaskpicker-heading">Output Parameters</span>:<br/><br/>';
                        Ext.each(params, function(param, idx) {
                            if (!param.utype || param.utype.toLowerCase() == 'input')
                                inputRows += '&nbsp&nbsp&nbsp&nbsp' + param.uname + ' - ' + Ext.String.htmlEncode(param.udescription || '') + '<br/>'
                            else
                                outputRows += '&nbsp&nbsp&nbsp&nbsp' + param.uname + ' - ' + Ext.String.htmlEncode(param.udescription || '') + '<br/>'
                        });
                        return inputRows + '<br/>' + outputRows;
                    }
                }
            )
        }],
        selType:'checkboxmodel',
        selModel : {
            mode : 'simple'
        }
    },{
        //Single selection with option to view detail of task (for Automation Designer)
        hidden : '@{multiSelection}',
        region: 'center',
        xtype: 'grid',
        cls : 'rs-grid-dark',
        name: 'actionTaskGridStore',
        store: '@{actionTaskGridStore}',
        columns: '@{columns}',
        autoScroll: true,
        viewConfig: {
            enableTextSelection: true
        },
        plugins: [{
            ptype: 'resolveexpander',
            rowBodyTpl: new Ext.XTemplate(
                '<div resolveId="resolveRowBody" style="color:#333">{[this.formatDescription(values.udescription)]}{[this.convertJson(values.resolveActionInvoc.resolveActionParameters)]}</div>', {
                    formatDescription: function(desc) {
                        if (!desc)
                            return '';
                        else
                            return '<span class="actiontaskpicker-heading">Description:</span><br/><br/>&nbsp&nbsp&nbsp&nbsp' + Ext.String.htmlEncode(desc) + '<br/><br/>';
                    },
                    convertJson: function(params) {
                        if (!params)
                            return;
                        var inputRows = '<span class="actiontaskpicker-heading">Input Parameters</span>:<br/><br/>';
                        var outputRows = '<span class="actiontaskpicker-heading">Output Parameters</span>:<br/><br/>';
                        Ext.each(params, function(param, idx) {
                            if (!param.utype || param.utype.toLowerCase() == 'input')
                                inputRows += '&nbsp&nbsp&nbsp&nbsp' + param.uname + ' - ' + Ext.String.htmlEncode(param.udescription || '') + '<br/>'
                            else
                                outputRows += '&nbsp&nbsp&nbsp&nbsp' + param.uname + ' - ' + Ext.String.htmlEncode(param.udescription || '') + '<br/>'
                        });
                        return inputRows + '<br/>' + outputRows;
                    }
                }
            )
        }],
        selModel: {
            selType: 'resolvecheckboxmodel',
            columnTooltip: RS.common.locale.editColumnTooltip,
            columnTarget: '_self',
            columnEventName: 'editAction',
            columnIdField: 'id'
        },
        listeners: {
            editAction: '@{editActionTask}'
        }
    }]
})
glu.defView('RS.actiontask.BulkOpWarningList', {
	title: '~~bulkWarningTitle~~',
	cls : 'rs-modal-popup',
	width: 550,
	height: 300,
	modal: true,
	autoScroll: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	padding: 15,
	items: [{
			xtype: 'component',
			html: '@{bulkWarning}'
		}, {
			xtype: 'component',
			html: '@{html}'
		}		
	],
	buttonAlign: '@{buttonAlign}',
	buttons: [{
		name :'proceedBulk',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'closeBulk',
		text : '~~close~~',
		cls : 'rs-med-btn rs-btn-light'
	},{
		name : 'close',
		cls : 'rs-med-btn rs-btn-dark'
	}]
});
glu.defView('RS.actiontask.CNSDefinition', {
	padding : 15,
	cls : 'rs-modal-popup',
	title: '~~cns~~',
	modal: true,
	width: 650,
	height: 450,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaultType: 'textfield',
	items: ['uname', {
		xtype: 'combobox',
		queryMode: 'local',
		displayField: 'name',
		valueField: 'value',
		store: '@{moduleStore}',
		name: 'umodule'
	}, 'uprefix', {
		xtype: 'combobox',
		editable: false,
		queryMode: 'local',
		displayField: 'name',
		valueField: 'value',
		store: '@{typeStore}',
		name: 'utype'
	}, 'uxpath', {
		xtype: 'textarea',
		flex: 1,
		name: 'udescription'
	}],	
	buttons: [{
		name :'save',
		cls : 'rs-med-btn rs-btn-dark'
	},{ 
		name : 'close',
		cls : 'rs-med-btn rs-btn-light'
	}]
});
glu.defView('RS.actiontask.CNSDefinitions', {
	padding : 15,
	xtype: 'grid',
	cls : 'rs-grid-dark',
	layout: 'fit',
	stateId: '@{stateId}',
	stateful: true,
	displayName: '@{displayName}',
	name: 'records',
	store: '@{store}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['createRecord', 'deleteRecords']
	}],
	columns: '@{columns}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.editColumnTooltip,
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		glyph : 0xF044
	},

	listeners: {
		editAction: '@{editRecord}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
});
glu.defView('RS.actiontask.Execute', {
	asWindow: {
		width: 400,
		maxHeight: 500,
		title: '~~executeTitle~~',
		cls : 'rs-modal-popup',
		draggable: false,
		target: '@{target}',
		listeners: {
			render: function(panel) {
				if (panel.target) {
					Ext.defer(function() {
						panel.alignTo(panel.target, 'tr-br')
					}, 1)
				}
			}
		}
	},
	buttonAlign: 'left',
	buttons: [{
		name:'execute', 
		cls:'rs-proceed-button'
	},{
		name : 'cancel',
		cls : 'rs-small-btn rs-btn-light'
	}],
	padding :10,

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'fieldcontainer',
		layout: 'hbox',
		hidden: '@{embedded}',
		items: [{
			xtype: 'radio',
			name: 'worksheetType',
			value: '@{newWorksheet}',
			boxLabel: '~~newWorksheet~~'
		}, {
			xtype: 'radio',
			padding: '0px 0px 0px 10px',
			name: 'worksheetType',
			value: '@{activeWorksheet}',
			boxLabel: '~~activeWorksheet~~'
		}]
	}, {
		xtype: 'checkbox',
		name: 'debug',
		hideLabel: true,
		boxLabel: '~~debug~~'
	}, {
		xtype: 'fieldcontainer',
		layout: 'hbox',
		items: [{
			xtype: 'checkbox',
			name: 'mock',
			hideLabel: true,
			boxLabel: '~~mock~~'
		}, {
			xtype: 'combo',
			padding: '0px 0px 0px 10px',
			flex: 1,
			name: 'mockName',
			store: '@{mockStore}',
			editable: '@{mockEditable}',
			displayField: 'uname',
			valueField: 'uname',
			queryMode: 'local',
			hideLabel: true
		}]
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		title: '~~params~~',
		margin : '0 0 10 0',
		name: 'params',
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items :  ['addParam','removeParams']
		}],
		store: '@{paramStore}',
		columns: '@{paramColumns}',
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 2
		}],
		viewConfig: {
			markDirty: false
		},
		flex: 1,
		listeners: {
			render: function(grid) {
				grid.store.grid = grid;
			}
		}
	}]
})
glu.defView('RS.actiontask.Main', {
    padding: 15,
    layout: 'border',
    plugins: [{
        ptype: 'searchfilter',
        allowPersistFilter: false,
        hideMenu: true
    }],

    items: [{
        xtype: 'panel',
        layout: 'card',
        region: 'west',
        width: 300,
        split: true,
        maxWidth: 450,
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            cls: 'actionBar rs-dockedtoolbar',
			defaultButtonUI: 'display-toolbar-button',		
            name: 'actionBar',
            items: ['menuPath', 'module']
        }],
        activeItem: '@{activeItem}',
        items: [{
            xtype: 'treepanel',
            id: 'menuPathTreePanel',
            cls : 'rs-grid-dark',
            viewConfig: {
                markDirty: false
            },
            selected: '@{menuPathSelected}',
            dockedItems: [{
                xtype: 'toolbar',
                style: '!important;padding:0px 0px 2px 0px !important'
            }],
            animate: false,
            columns: '@{actionTaskTreeMenuPathColumns}',
            name: 'actionTaskTreeMenuStore',
            listeners: {
                selectionchange: '@{menuPathTreeSelectionchange}'
            }
        }, {
            xtype: 'treepanel',
            id: 'moduleTreePanel',
            cls : 'rs-grid-dark',
            viewConfig: {
                markDirty: false
            },
            dockedItems: [{
                xtype: 'toolbar',
                style: '!important;padding:0px 0px 2px 0px !important'
            }],
            animate: false,
            columns: '@{actionTaskTreeModuleColumns}',
            name: 'actionTaskTreeModuleStore',

            listeners: {
                selectionchange: '@{moduleTreeSelectionchange}'
            }
        }]
    }, {
        region: 'center',
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            cls: 'actionBar rs-dockedtoolbar',
            name: 'actionBar',
            defaults : {
                cls : 'rs-small-btn rs-btn-light'
            },
            items: ['createActionTask','deleteActionTasks', 'rename', 'copy', {
                text: '~~index~~',
                menu: {
                    xtype: 'menu',
                    items: ['index', 'indexAll']
                }
            }, {
                xtype: 'button',
                iconCls: 'rs-social-button icon-play',
                tooltip: '~~executionTooltip~~',
                disabled: '@{!executionIsEnabled}',
                name: 'execution',
                paddings: {
                    right: 10
                },
                text: '',
                handler: function(button) {
                    button.fireEvent('execution', button, button)
                },
                listeners: {
                    execution: '@{execution}'
                }
            }]
        }],
        xtype: 'grid',
        cls : 'rs-grid-dark',
        plugins: [{
            ptype: 'pager'
        }, {
            init: function(grid) {
                grid.dockedItems.each(function(item) {
                    if (item.name == 'actionBar')
                        this.toolbar = item;
                }, this);
                if (!this.toolbar)
                    return;
                target = null;
                var remove = [];
                this.toolbar.items.each(function(item, idx) {
                    if (item.name == 'execution') {
                        target = item;
                        remove.push(target);
                    }

                })
                this.toolbar.insert(this.toolbar.items.items.length - 2, target.initialConfig);
            }
        }],
        displayName: '~~actionTaskDisplayName~~',
        name: 'actionTasks',
        store: '@{actionTaskGridStore}',
        columns: '@{columns}',
        autoScroll: true,

        viewConfig: {
            enableTextSelection: true
        },
        selModel: {
            selType: 'resolvecheckboxmodel',
            //columnTooltip: RS.common.locale.editColumnTooltip,
			columnTooltip: RS.common.locale.gotoDetailsColumnTooltip,
            columnTarget: '_self',
            columnEventName: 'editAction',
            columnIdField: 'id'
        },
        listeners: {
            editAction: '@{editDefinition}',
            selectAllAcrossPages: '@{selectAllAcrossPages}',
            selectionChange: '@{selectionChanged}'
        }
    }]
});
glu.defView('RS.actiontask.MoveOrRenameOrCopy', {
    title: '@{title}',
    padding : 15,
    modal: true,
    height : 250,   
    width: 600,   
    cls : 'rs-modal-popup',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        allowBlank: false,
        labelWidth: 100
    },
    items: [{
        xtype: 'combo',
        name: 'unamespace',
        itemId: 'unamespaceCombo',
        store: '@{comboBoxStore}',
        displayField: 'name',
        valueField: 'name',
        typeAhead: true,
        queryMode: 'local',
        editable: true	
    }, {
        xtype: 'textfield',
        name: 'uname'
    }, {
        xtype: 'checkbox',
        name: 'overwrite',
        hideLabel: true,
        boxLabel: '~~overwrite~~',
        padding: '0px 0px 0px 110px'
    }],
    buttons : [{
        name: 'dump',
        text: '@{dumpText}',
        cls : 'rs-med-btn rs-btn-dark'
    }, {
        name :'cancel',
        cls : 'rs-med-btn rs-btn-light'
    }],
    listeners: {
        render: function() {
            // GVo Note
            // This is a get-around
            // solution on the combobox not populating the value.
            // I have no idea why. According to Sencha docs, we can set the
            // initial selection by setting the value property but some how it doesn't work in the case.
            var comboBox = this.down('#unamespaceCombo');
            comboBox.getStore().on('foundComboSelection', function(selectedRecord) {
                comboBox.select(selectedRecord);
            });
        }
    }
})
glu.defView('RS.actiontask.PropertyDefinition', {
    title: '~~property~~',
    padding : 15,
    modal: true,
    cls : 'rs-modal-popup',
    width: 600,
    height: 400,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults : {
        margin : '4 0'
    },
    defaultType: 'textfield',
    items: ['uname', {
        xtype: 'combobox',
        queryMode: 'local',
        displayField: 'name',
        valueField: 'value',
        store: '@{moduleStore}',
        name: 'umodule'
    }, {
        xtype: 'combobox',
        editable: false,
        queryMode: 'local',
        displayField: 'name',
        valueField: 'value',
        store: '@{typeStore}',
        name: 'utype'
    }, {
        xtype: 'textarea',
        flex: 1,
        name: 'uvalue',
        hidden: '@{isEncrypt}'
    }, {
        name: 'uvalue',
        hidden: '@{!isEncrypt}',
        inputType: 'password',
		listeners:{
			afterrender:function(cmp){
				cmp.inputEl.set({
					//Prevent autofill from Chrome.
					autocomplete:'new-password'
				});
			}
		}
    }], 
    buttons: [{
        name :'save',
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'close',
        cls : 'rs-med-btn rs-btn-light'
    }]
})
glu.defView('RS.actiontask.PropertyDefinitions', {
	xtype: 'grid',
	cls : 'rs-grid-dark',
	padding : 15,
	layout: 'fit',
	stateId: '@{stateId}',
	stateful: true,
	displayName: '@{displayName}',
	name: 'records',
	store: '@{store}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['createRecord', 'deleteRecords']
	}],
	columns: '@{columns}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		useWindowParams: true,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.editColumnTooltip,
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		glyph : 0xF044
	},

	listeners: {
		editAction: '@{editRecord}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
});
glu.defView('RS.actiontask.PropertyDefinitionsPicker', {
	asWindow: {
		title: '~~PropertyDefinitionsPickerTitle~~',
		height: '@{height}',
		width: '@{width}',
		modal: true
	},
	layout: 'fit',
	buttonAlign: 'left',
	buttons: ['select', 'cancel'],
	items: [{
		xtype: 'grid',
		displayName: '@{displayName}',
		name: 'records',
		store: '@{store}',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: []
		}],
		columns: '@{columns}',
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}]
	}]
});
glu.defView('RS.actiontask.ReferenceList', {
	xtype: 'panel',
	autoScroll: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10px',
	items: [{
		xtype: 'panel',
		html: '~~referenceWarning~~'
	}, {
		region: 'center',
		tbar: [],
		xtype: 'grid',
		padding: '10px 0px 0px 0px',
		plugins: [{
			ptype: 'pager'
		}],
		flex: 1,
		name: 'reference',
		store: '@{store}',
		columns: [{
			header: '~~name~~',
			dataIndex: 'name',
			filterable: false,
			flex: 1
		}],
		minHeight: 200,
		listeners: {
			selectionchange: '@{selectionChanged}'
		}
	}, {
		xtype: 'panel',
		html: '~~stillDelete~~'
	}],

	asWindow: {
		title: '~~reference~~',
		width: 460,
		height: 400,
		modal: true
	},

	buttonAlign: 'left',
	buttons: ['gotoReference', 'deleteRecords', 'close']
});
/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.actiontask').locale = {
	windowTitle: 'ActionTask',
	ServerError: 'Server Error',
	ListRecordsErr: 'Cannot get records from the server.',
	DeleteSucMsg: 'The selected records have been deleted.',
	DeleteErrMsg: 'Cannot delete the selected records.',
	confirmDelete: 'Delete',
	ListModuleTreeError: 'Cannot get modules from the server.',
	ListMenuTreeError: 'Cannot get menu paths from the server.',
	//Fields
	name: 'Name',
	active: 'Active',
	module: 'Namespace',
	summary: 'Summary',
	assignedToUsername: 'Assigned To',
	lastUpdated: 'Last Updated',
	createActionTask: 'New',
	deleteActionTasks: 'Delete',
	menuPath: 'Menu Path',
	module: 'Namespace',
	type: 'Type',

	DeleteActiontaskSuc: 'The records have been deleted.',
	DeleteActiontaskErr: 'Cannot delete the select records.',
	referenceWarning: 'Some of the selected ActionTasks are referenced by the the following wikis:',
	stillDelete: 'Do you still want to delete the selected ActionTasks?',
	gotoReference: 'Show Reference Detail',

	LoadActionTaskErr: 'Cannot get ActionTasks from the server',

	invalidName: 'Name is required and must be in alphanumerics, but may contain single spaces, underscores \'_\' or dash \'-\' between alphanumeric characters.',
	invalidModule: 'Module name is required and must be in alphanumerics, but may contain periods \'.\', single spaces, or underscores \'_\' between alphanumeric characters.',
	//invalidMenuPath: 'Menu Path name is required and must be in alphanumerics, but may contain single spaces or underscores \'_\'. Backslashes \'\\\' are not allowed.',
	invalidTimeout: 'Timeout should be an integer.',

	invalidNamespace: 'A namespace may contain letters, numbers, periods, underscores, and spaces. It must end in a number or letter. The namespace "Resolve" is reserved.',
	invalidMenuPath: 'A menu path may contain letters, numbers, forward slashes, underscores, and spaces. It must end in a number or letter. The menu path "/Resolve" is reserved.',

	move: 'Move',
	MoveTitle: 'Move',
	MoveMsg: 'Are you sure you want to move the selected record?',
	MoveSuc: 'The selected record has been moved.',
	rename: 'Move/Rename',
	RenameTitle: 'Rename',
	RenameMsg: 'Are you sure you want to rename the selected record?',
	RenamesMsg: 'Are you sure you want to rename the selected records?',
	RenameSuc: 'The selected record has been renamed.',
	RenameErr: 'Cannot rename the selected records.[{msg}]',

	copy: 'Copy',
	CopyTitle: 'Copy',
	CopyMsg: 'Are you sure you want to copy the selected record?',
	CopysMsg: 'Are you sure you want to copy the selected records?({num} records selected)',
	CopySuc: 'The selected record has been copied.',
	CopyErr: 'Cannot copy the selected records.[{msg}]',
	edit: 'Edit',
	resizeFullscreen: 'Maximize section',
	resizeNormalscreen: 'Resize back to normal',
	indexAll: 'Index All',
	index: 'Index',
	submitIndexReqError: 'Cannot process the index request:{0}',
	submitIndexReqSuccess: 'Index request submitted.',
	saveAsTitle: 'Save As New ActionTask',
	EditNewActionTaskTitle: 'View New ActionTask',
	EditNewActionTaskMsg: 'Do you want to view the created ActionTask?',
	editNewTask: 'Go to the new ActionTask',
	execute: 'Execute',
	debug: 'Debug',
	mock: 'Mock',
	actionTaskNameId: 'ActionTask Name',
	actionTaskDisplayName: 'ActionTask',
	whySaveIsDisabled: 'There are some required fields not properly filled in the General tab.',

	viewActionTaskList: 'View ActionTask List',

	exitWithoutSaveTitle: 'Exit without saving?',
	exitWithoutSaveBody: 'Changes you have made to the document have not been saved, would you like to save before exiting?',
	dontSave: "Don't Save",

	loadVersionWithoutSaveTitle: 'Load version {0} without saving?',
	loadVersionWithoutSaveBody: 'Changes you have made to the current document have not been saved, would you like to save before opening the new version?',

	confirm: 'Confirm',
	cancel: 'Cancel',
	dump: 'OK',
	ActionTaskPicker: {
		selectTitle: 'Select an ActionTask',
		createTitle: 'Create an ActionTask',
		dump: 'Select',
		all: 'All',
		create: 'Create',
		timeout: 'Timeout',
		duplicateTaskName: 'The ActionTask:{fullName} already exists.',

		remote: 'REMOTE',
		assess: 'ASSESS',
		os: 'OS',
		bash: 'BASH',
		cmd: 'CMD',
		cscript: 'CSCRIPT',
		powershell: 'POWERSHELL',
		allTaskTab : 'All',
		fromAutomationTab : 'From Automation'
	},
	FromAutomation : {
		automationSearch : 'Automation',
		runbookNameEmptyText : 'Enter automation name...',
		invalidAutomation : 'The automation name is invalid or this document is not an automation.'
	},
	//move rename copy
	moveTitle: 'Move',
	renameTitle: 'Rename',
	copyTitle: 'Copy',

	//action  task view
	NewActionTaskTitle: 'New ActionTask',
	ActionTaskTitle: 'ActionTask',
	//rood panel
	generalTab: 'General',
	typeTab: 'Type',
	paramsTab: 'Parameters',
	contentTab: 'Content',
	assessorTab: 'Assess',
	preprocessorTab: 'Preprocess',
	parserTab: 'Parser',
	mockTab: 'Mock',

	//geneal
	back: 'Back',
	save: 'Save',
	saveAs: 'Save As',
	propertiesTab: 'Properties',
	referencesTab: 'Reference',
	rolesTitle: 'Roles',
	uname: 'Name',
	unamespace: 'Namespace',
	umenuPath: 'Menu Path',
	uuserName: 'Assigned To',
	usummary: 'Summary',
	utimeout: 'Timeout (secs)',
	ucommand: 'Command',
	uargs: 'Args',
	udescription: 'Description',
	uactive: 'Active',
	ulogresult: 'Log Raw Result',
	uisHidden: 'Display',
	uisHidden_tooltip: 'Display this action task in the worksheet results always.  If unchecked, then you will only see this action task listed in the worksheet results advanced view.',
	uisDefaultRole: 'Default Role',
	utype: 'Type',
	tags: 'Tags',
	options: 'Options',
	addTags: 'Add',
	removeTags: 'Remove',
	addOption: 'Add',
	removeOptions: 'Remove',
	uvalue: 'Value',
	socialTooltip: 'Social',
	utype: 'Type',
	uinclude: 'Include',
	urequired: 'Required',
	uencrypt: 'Encrypt',
	refInContent: 'Content',
	refInMain: 'Main',
	refInAbort: 'Abort',
	addInput: 'Add Input',
	addOutput: 'Add Output',
	deleteParams: 'Delete',
	tagAsWindowTitle: 'Tags',
	tagDisplayName: 'Tags',
	createTag: 'New',

	uonlyCompleted: 'Only Completed',
	toLocal: 'Copy Reference to Local',
	syntax: 'Syntax',
	local: 'Local',
	reference: 'Reference',
	CopyToLocalTitle: 'Copy Reference',
	CopyToLocalMsg: 'Are you sure you want to create a local copy from reference "{name}"?',
	groovy: 'Groovy',
	golang: 'Go',
	perl: 'Perl',
	powershell: 'Powershell',
	ruby: 'Ruby',
	sql: 'SQL',
	text: 'Text',
	xml: 'XML',
	json: 'JSON',
	revision: 'Revision',
	//param
	inputParam: 'Input Parameter',
	outputParam: 'Output Parameter',
	udefaultValue: 'Default',
	urequired: 'Required',
	uinclude: 'Include',
	uencrypt: 'Encrypt',
	DuplicateParamNameInINPUT: 'Input parameter:{name} already exists.',
	DuplicateParamNameInOUTPUT: 'Output parameter:{name} already exists.',
	invalidGroupCount: 'The group number shouldn\'t be empty',
	//parser
	ParserBuilder: {
		dump: 'Apply',
		example: 'View Example'
	},
	builderDef: 'Definition',
	showDebugger: 'Test',
	showBuilder: 'Build',
	builder: 'Build and Test',
	string: 'String',
	list: 'List',
	map: 'Map',
	addMapRegex: 'Add',
	removeMapRegex: 'Remove',
	listMap: 'List of Map',
	strRegex: 'Regular Expression',
	strSubstring: 'Substring',
	strXPath: 'XPath',
	listRegex: 'Regular Expression',
	listCSV: 'CSV',
	listXPath: 'XPath',
	mapProps: 'Properties',
	mapRegex: 'Regular Expression',
	mapXPath: 'XPath',
	mapURL: 'URL Encoded',
	listMapCSV: 'CSV',
	listMapXPath: 'XPath',
	matchStr: 'Match String',
	exactMatch: 'Exact Match',
	caseSensitive: 'Case Sensitive',
	dotAll: 'Dotall',
	multiline: 'Multi-Line',
	unixLines: 'Unix lines',
	trim: 'Trim',
	group: 'Group',

	block: 'Block',
	start: 'Start',
	includeIndexOfStart: 'Include Index of Start',
	end: 'End',
	separator: 'Separator',
	loop: 'Loop',
	line: 'Line',
	from: 'From',
	to: 'To',
	includeIndexOfEnd: 'Include Index of End',
	xpathExpression: 'Expression',

	startIndexText: 'Start At IndexOf',
	endIndexText: 'End At IndexOf',

	removeBlankEntries: 'Remove Blank Entries',

	delimiter: 'Delimiter',
	join: 'Join',
	space: 'Space',
	vertical: '|',
	comma: ',',
	tab: '\\t',
	carriageReturn: 'Carriage Return(\\r)',
	lineFeed: 'Line Feed(\\n)',
	crlf: 'CR/LF(\\r\\n)',

	fieldSeparator: 'Field Separator',
	rowSeparator: 'Row Separator',
	keyValueSeparator: 'Key-Value Separator',
	propertyLineSeparator: 'Property Row Separator',
	keyCase: 'Key Case',
	upper: 'Upper',
	lower: 'Lower',
	regex: 'Regular Expression',
	keyGroup: 'Key Group',
	valueGroup: 'Value Group',

	firstRow: 'User First Row As Key',
	customize: 'Customize Key Names',
	customizeKeyName: 'Customize Key Names',
	addKeyName: 'New Key',
	removeKeyNames: 'Remove Keys',
	name: 'Name',
	equal: '=',

	custom: 'Custom Parser',

	jsonObj: 'JSON Object',
	jsonArr: 'JSON Array',

	//tips
	matchStrTip: 'Regular expression used to match the input string.',
	exactMatchTip: 'Checked if the entire string matches the given regular expression. Uncheck if a substring of the given string matches the regular expression.',
	dotAllTip: 'In dotall mode, the expression . matches any character, including a line terminator. By default this expression does not match line terminators.',
	caseSensitiveTip: 'The matching will be case sensitive.',
	multilineTip: 'In multiline mode the expressions ^ and $ match just after or just before, respectively, a line terminator or the end of the input sequence. By default these expressions only match at the beginning and the end of the entire input sequence.',
	unixLinesTip: 'In this mode, only the \'\\n\' line terminator is recognized in the behavior of ., ^, and $.',
	groupTip: 'Checked if want to catch specific substring of the given string',
	groupCountTip: 'Regex group number that the target substrings correspond to',
	trimTip: 'True if the captured substring needs to be trimed',
	blockTip: 'True if the parse process specified above need to be repeated.',
	lineTip: 'Extract the content between the specified line numbers for further operation(specified below)',
	fromTip: 'Line number indicating the start line of the extraction.',
	toTip: 'Line number indicating the end line fo the extraction.',
	startTip: 'Start extracting string from the position of the string specified below(after the extraction between lines if line option is checked)',
	startTextTip: 'String indicating the start position of the further extraction',
	endTextTip: 'String indicating the end position of the further extraction',
	endTip: 'Stop extracting string at the position of the string specified below(after the extraction between lines if line option is checked)',
	separatorTip: 'Split the string(or extracted string if the line/start/end option is checked.',
	separatorTextTip: 'Separator used to split the string.',
	loopTip: 'Repeat the extraction with the start text and end text',

	startIndexTextTip: 'String used to decide the start position of the substring extraction.',
	endIndexTextTip: 'String used to decide end position of the substring extraction.',
	includeIndexOfStartTip: 'Include the start string',
	includeIndexOfEndTip: 'Include the end string',

	xpathExpressionTip: 'Expath used to decide elements that need to be extracted.',

	removeBlankEntriesTip: 'Do not include empty group into the result.',

	delimiterTip: 'Delimiter used to split the input string.',
	joinTip: 'Checked if want to join the multiline input before start parsing the input',
	joinerTip: 'New line character that the RAW input current using.',

	keyValueSeparatorTip: 'Separator for the key value pair of the input string.',
	propertyLineSeparatorTip: 'Line separator for the input string.',
	keyCaseTip: 'Check if need to convert property key to uppar/lower case.',
	upperTip: 'Convert the key to upper case.',
	lowerTip: 'Convert the key to lower case.',

	fieldSeparatorTip: 'Separator used to split each line into values for different fields(specified in the key list generated using the \'User First Row As Key\' or \'Customize Key Names\')',
	rowSeparatorTip: 'Separator user to split the input into rows.',

	jsonObjTip: 'Convert the input string into instance of net.sf.json.JSONObject',
	jsonArrTip: 'Convert the input string into instance of net.sf.json.JSONArray',

	invalidLine: 'At least one of Line,Start,End must be checked for block processing.',
	invalidStart: 'At least one of Line,Start,End must be checked for block processing.',
	invalidEnd: 'At least one of Line,Start,End must be checked for block processing.',
	invalidFrom: 'Start line number is required and only non-negative integers are allowed.',
	invalidTo: 'End line number is required and only non-negative integers are allowed.',
	invalidStartText: 'Start text is required.',
	invalidEndText: 'End text is required.',
	invalidSeparatorText: 'The separator is required.',

	invalidStartIndexText: 'The start text is required.',
	invalidEndIndexText: 'The end text is required.',
	invalidListRegexGroupCount: 'The group number shounld only contain digits,separated by \',\'',

	test: 'Test',
	run: 'Run',
	clear: 'Reset',
	raw: 'Raw',
	output: 'Output',

	createMock: 'Add Mock',
	removeMock: 'Remove Mock',

	add: 'Add',
	remove: 'Remove',
	input: 'Inputs',
	flow: 'Flows',
	param: 'Params',
	description: 'Description',

	mockTitle: 'Mock Definition',
	mockContent: 'Mock Content',
	duplicateMockName: 'The mock name should be unique.(Duplicate Name:{name})',
	duplicateOptions: 'The option name should be unique.(Duplicate Name:{name})',
	duplicateMockInput: 'Mock:{mock} has duplicate inputs: {name}',
	duplicateMockFlow: 'Mock:{mock} has duplicate flow: {name}',
	duplicateMockParam: 'Mock:{mock} has duplicate parameter: {name}',
	duplicateParamName: 'The {0} parameter names must be unique (\'{1}\' is an existing name).',
	rawData: 'Raw',

	value: 'Value',

	createRecord: 'New',
	deleteRecords: 'Delete',
	DeleteTitle: 'Delete',
	DeleteMsg: 'Are you sure you want to delete the selected record?',
	DeletesMsg: 'Are you sure you want to delete the selected records?({num} records selected)',

	script: 'Script',
	general: 'General',
	close: 'Close',
	utask: 'Name',

	executionTooltip: 'Execute',
	executeTitle: 'Execute',
	executeSuccess: 'The request for executing {type}:{fullName} has been accepted.',
	newWorksheet: 'New WorkSheet',
	activeWorksheet: 'Current WorkSheet',
	params: 'Params',
	addParam: 'Add',
	removeParams: 'Remove',
	invalidMockName: 'The mock name is required.',

	sysUpdatedOn: 'Updated On',
	SaveErr: 'Cannot save ActionTask.',
	SaveSuc: 'The ActionTask has been saved.',

	execution: 'Execute',
	GetPropertyErr: 'Cannot get the property from the server.',
	GetRevisionErr: 'Cannot get revisions from the server.',
	PreprocessDefinitions: {
		windowTitle: 'Preprocess',
		displayName: 'Preprocess'
	},

	PreprocessDefinition: {
		preprocessDefinition: 'Preprocess',
		preprocess: 'Preprocess',
		GetPreprocessDefintionErr: 'Cannot get the preprocess from the server.',
		SavePreprocessDefintionErr: 'Cannot save the preprocess',
		SavePreprocessDefintionSuc: 'The preprocess has been saved.'
	},

	AssessDefinitions: {
		windowTitle: 'Assess',
		displayName: 'Assess'
	},

	AssessDefinition: {
		assessDefinition: 'Assess',
		assess: 'Assess',
		GetAssessDefintionErr: 'Cannot get the access from the server.',
		SaveAssessDefintionErr: 'Cannot save the access',
		SaveAssessDefintionSuc: 'The access has been saved.'
	},

	ParserDefinitions: {
		windowTitle: 'Parser',
		displayName: 'Parser'
	},

	ParserDefinition: {
		parserDefinition: 'Parser',
		parser: 'Parser',
		GetParserDefintionErr: 'Cannot get the parser from the server.',
		SaveParserDefintionErr: 'Cannot save the parser',
		SaveParserDefintionSuc: 'The parser has been saved.'
	},

	EmptyRegexTitle: 'Regular expression definition list is empty',
	EmptyRegexMsg: 'Please add at least one definition.',

	umodule: 'Module',

	PropertyDefinitions: {
		displayName: 'Properties'
	},

	PropertyDefinitionsPicker: {
		PropertyDefinitionsPickerTitle: 'Select an Action Task Property',
		displayName: 'Properties',
		select: 'Select'
	},

	PropertyDefinition: {
		property: 'Property',
		PLAIN: 'PLAIN',
		ENCRYPT: 'ENCRYPT',
		GetPropertyDefintionErr: 'Cannot get the property from the server.',
		SavePropertyDefintionErr: 'Cannot save the property',
		SavePropertyDefintionSuc: 'The property has been saved.'
	},

	uprefix: 'Prefix',
	uxpath: 'XPath',
	LIST: 'List',
	STRING: 'String',
	MAP: 'Map',
	LISTMAP: 'ListMap',
	XML: 'XML',
	CNSDefinitions: {
		displayName: 'CNS'
	},

	CNSDefinition: {
		cns: 'CNS',
		GetCNSDefintionErr: 'Cannot get the property from the server.',
		SaveCNSDefintionErr: 'Cannot save the property',
		SaveCNSDefintionSuc: 'The property has been saved.',
		invalidPrefix: 'The prefix is required and should only contain alphabets and digits.'
	},

	Option: {
		dump: 'Add',
		invalidName: 'The name should not be empty and should not be "INPUTFILE"'
	},

	ParamsDialog: {
		dumperText: 'OK'
	},

	ReferenceList: {
		deleteRecords: 'Ignore References and Delete'
	},

	overwrite: 'Overwrite',

	BulkOpWarningList: {
		renameBulkWarning: 'Cannot complete the rename operation because the following ActionTasks already exist.',
		copyBulkWarning: 'Cannot complete the copy operation because the following ActionTasks already exist.',
		proceedBulk: 'Overwrite',
		bulkWarningTitle: 'Warning'
	},

	// NAVIGATION
	ActionTaskTitle: 'ActionTask',
	fileOptions: 'File',
	save: 'Save',
	saveAs: 'Save As',
	rename: 'Move/Rename',
	sysInfo: 'System Info',
	sysInformation: 'System Information',
	collaborationOptions: 'Collaboration',
	expandAll: 'Expand All',
	collapseAll: 'Collapse All',

	commit: 'Commit',
	viewVersionList: 'View Version List',
	attention: 'Attention',
	newVersionsAvailabe: 'There are newer versions available. Do you want to commit your version anyway?',
	commitNewVersion: 'Commit New Version',

	edit: 'Edit',
	exitToView: 'Exit Edit Mode',
	readOnlyTxt: '(read-only)',
	versionTxt: '(v.{0})',

	allBtn: 'All',
	generalBtn: 'General',
	referenceBtn: 'Reference',
	parametersBtn: 'Parameters',
	preprocessorBtn: 'Preprocessor',
	contentBtn: 'Content',
	parserBtn: 'Parser',
	assessBtn: 'Assess',
	outputsBtn: 'Assignments',
	severityConditionsBtn: 'Severities & Conditions',
	summaryDetailsBtn: 'Summary & Details',
	mockBtn: 'Mock',
	tagsBtn: 'Tags',

	toolbarSettings: 'Toolbar Settings',
	showSectionNone: 'None',
	showSectionTextOnly: 'Text Only',
	showSectionTextIcon: 'Text and Icon',
	showSectionIconOnly: 'Icon Only',

	//expandAllTip: 'Expand all sections',
	//collapseAllTip: 'Collapse all sections',
	follow: 'Follow',
	viewFollowers: 'View Followers',
	social: 'Collaboration',
	executionTooltip: 'Execute',
	reload: 'Reload',
	follow: 'Follow',
	unfollow: 'Unfollow',
	unamespace: 'Namespace',
	actionTaskFollowed: 'ActionTask followed',
	actionTaskUnfollowed: 'ActionTask unfollowed',
	reloadWithoutSaveTitle: 'Reload without saving?',
	reloadWithoutSaveBody: 'Reload will lose all changes you have made to the ActionTask.<br >Proceed to reload?',
	renameAfterSaveTitle: 'Rename after save?',
	renameAfterSaveBody: 'You have unsaved changes. Do you want to save your changes before renaming?',
	RenameTitle: 'Rename',
	RenameMsg: 'Are you sure you want to rename the ActionTask?',
	RenameSuc: 'The ActionTask has been renamed.',
	RenameErr: 'Cannot rename the ActionTask.',
	exitWithoutSaveTitle: 'Exit without saving?',
	exitWithoutSaveBody: 'Exit without saving will lose all changes you have made to the ActionTask.<br >Proceed to exit?',

	invalidActionTaskTitle: 'Invalid ActionTask',
	saveInvalidActionTaskBody: 'Invalid ActionTask detected. Disregard the invalid changes and save?',
	leaveInvalidActionTaskBody: 'Invalid ActionTask detected. Disregard the invalid changes and continue?',

	generalTitle: 'General',
	generalEditTitle: 'General',
	rolesEditTitle: 'Roles',
	rolesReadTitle: 'Roles',
	tagsReadTitle: 'Tags',
	tagsEditTitle: 'Tags',
	parametersTitle: 'Parameters',
	preprocessorTitle: 'Preprocessor - Code Editor',
	preprocessorReadTitle: 'Preprocessor',
	preprocessorEditTitle: 'Preprocessor',
	contentReadTitle: 'Content',
	contentEditTitle: 'Content',
	contentTitle: 'Content',
	parserReadTitle: 'Parser',
	parserEditTitle: 'Parser',
	assessReadTitle: 'Assess',
	assessEditTitle: 'Assess - Code Editor',
	optionsReadTitle: 'Options',
	optionsEditTitle: 'Options',
	valueToOutputsReadTitle: 'Assign Values to Outputs',
	valueToOutputsEditTitle: 'Assign Values to Outputs',
	inputParametersWizardStepTitle: 'Parameters',
	outputParametersWizardStepTitle: 'Parameters',
	severitiesAndConditionsTitle: 'Severities and Conditions',
	severitiesEditTitle: 'Severities',
	conditionsEditTitle: 'Conditions',
	severitiesTitle: 'Severities',
	summaryAndDetailsTitle: 'Summary and Details',
	summaryEditTitle: 'Summary and Details',
	detailsEditTitle: 'Summary and Details',
	referencesReadTitle: 'References',

	unclassified: 'UNCLASSIFIED',
	noGroupName: 'Not in group',
	wizard: 'Wizard',
	yesButtonText: 'Yes',
	noButtonText: 'No',

	errorTitle: 'Error',
	serverError: 'Server Error: {0}'
}

Ext.namespace('RS.actiontaskbuilder').locale = {
	requestFailed: 'Request Failed',
	done: 'Done',
	add: 'Add',
	clear : 'Clear',
	cancel: 'Cancel',
	edit: 'Edit',
	next: 'Next',
	revert: 'Revert',
	close: 'Close',
	back: 'Back',
	confirm: 'Confirm',
	invalidJSON : 'Invalid JSON format.',
	resizeFullscreen: 'Maximize section',
	resizeNormalscreen: 'Resize back to normal',

	// NAVIGATION
	ActionTaskTitle: 'ActionTask',
	fileOptions: 'File',
	save: 'Save',
	saveAs: 'Save As',
	rename: 'Move/Rename',
	sysInfo: 'System Info',
	sysInformation: 'System Information',
	executionTooltip: 'Execute',
	reload: 'Reload',
	reloadWithoutSaveTitle: 'Reload without saving?',
	reloadWithoutSaveBody: 'Reload will lose all changes you have made to the ActionTask.<br >Proceed to reload?',
	RenameTitle: 'Rename',
	RenameMsg: 'Are you sure you want to rename the ActionTask?',
	RenameSuc: 'The ActionTask has been renamed.',
	RenameErr: 'Cannot rename the ActionTask.',

	serverError: 'Server Error: {0}',
	actionTaskIDMissing: 'ActionTask System ID missing.',

	sourceCodeSectionEmptyText: 'No source code.',

	//GENERAL SECTION
	generalTitle: 'General',
	generalPropertiesEditTitle: 'Properties',
	namespace: 'Namespace',
	name: 'Name',
	type: 'Type',
	timeout: 'Timeout (secs)',
	timeoutLabel: 'Timeout',
	timeoutValue: 'seconds',
	timeoutValueSingular: 'second',
	assignedTo: 'Assigned To',
	summary: 'Summary',
	description: 'Description',
	generalDescription: 'Description:',
	active: 'Active',
	logResult: 'Log raw result',
	displayInWorksheet: 'Display in worksheet',
	noDescription: '(No description)',
	noAssignedUser: '(No user assigned)',
	noSummary: '(No summary)',
	moreDetails: 'More',
	lessDetails: 'Less',
	referenceTitle: 'ActionTask Reference - {0}',
	reference: 'Reference',
	refInContent: 'Content',
	refInMain: 'Main',
	refInAbort: 'Abort',
	menuPath: 'Menu Path',
	activityNotCompleted: 'Activity not completed',
	confirmYes: 'Yes',
	confirmNo: 'No',
	invalidNamespace: 'A namespace may contain letters, numbers, periods, underscores, and spaces. It must end in a number or letter. The namespace "Resolve" is reserved.',
	invalidMenuPath: 'A menu path may contain letters, numbers, forward slashes, underscores, and spaces. It must end in a number or letter. The menu path "/Resolve" is reserved.',
	invalidName: 'Name is required and must be in alphanumerics, but may contain single spaces or underscores \'_\' between alphanumeric characters.',
	addRole: 'Add Role',
	addOption: 'Add Option',

	GeneralEdit: {
		remote: 'REMOTE',
		assess: 'ASSESS',
		os: 'OS',
		bash: 'BASH',
		cmd: 'CMD',
		cscript: 'CSCRIPT',
		powershell: 'POWERSHELL',
		process: 'PROCESS',
		external: 'EXTERNAL',
		namespaceRequired: 'Namespace is required.',
		nameRequired: 'Name is required.',
		timeoutError: 'Timeout cannot be negative.',
		assignedToError: 'Need to assigned a user.'
	},

	//ROLE SECTION
	rolesEditTitle: 'Roles',
	rolesReadTitle: 'Roles',
	usesDefaultRoles: 'Use default roles',
	defaultRolesText: 'Apply Default Roles',
	roleName: 'Name',
	roleReadRight: 'View',
	roleWriteRight: 'Edit',
	roleExecuteRight: 'Execute',
	roleAdminRight: 'Admin',
	roleCommand: 'Command',
	bulkEdit: 'Bulk Edit',
	selectRole: 'Select a role...',
	confirmAddRole: 'Add the role before leaving?',
	rolesEmptyText: 'There are no roles.',
	defaultRolesMessage: 'Default roles will be displayed once the actiontask is saved.',

	Access: {
		read: 'Access',
	},

	//OPTION SECTION
	optionsReadTitle: 'Options',
	optionsEditTitle: 'Options',
	uname: 'Name',
	uvalue: 'Value',
	udescription: 'Description',
	selectOption: 'Select an option...',
	selectTag: 'Select Tag',
	confirmAddOption: 'Add the option before leaving?',
	confirmAddSeverities: 'Add the severities before leaving?',
	confirmAddConditions: 'Add the conditions before leaving?',
	optionsEmptyText: 'There are no options.',

	//TAGS SECTION
	tagsReadTitle: 'Tags',
	addTag: 'Add Tag',
	tagsEditTitle: 'Tags',
	tagsEmptyText: 'There are no tags.',
	confirmAddTag: 'Add the tag before leaving?',
	noTagDescription: '(No description is defined)',

	//PARAMETERS SECTION
	parametersTitle: 'Parameters',
	inputParam: 'INPUTS',
	outputParam: 'OUTPUTS',
	inputParametersEditTitle: 'Inputs',
	outputParametersEditTitle: 'Outputs',
	parameterDescription: 'Description:',
	labelDefaultValue: 'Default:',
	defaultValue: 'Default',
	required: 'Required',
	include: 'Include',
	encrypt: 'Encrypt',
	protected: 'Protected',
	unclassified: 'UNCLASSIFIED',
	noGroupName: 'Not in group',
	inputParametersEmptyText: 'There are no input parameters.',
	outputParametersEmptyText: 'There are no output parameters.',
	invalidGroup : 'Group name may contain single spaces.',

	//REFERENCES SECTION
	referencesTitle: 'References',
	referencesEmptyText: 'There are no references.',

	MockEdit: {
		input: 'INPUT',
		flow: 'FLOW',
		param: 'PARAM',

		closeDetails: 'Back',
	},

	selectType: 'Select Type',

	DuplicateParamNameInINPUT: 'Input parameter:{name} already exists.',
	DuplicateParamNameInOUTPUT: 'Output parameter:{name} already exists.',
	invalidGroupCount: 'The group number shouldn\'t be empty',

	inputParametersWizardStepTitle: 'Input Parameters',
	outputParametersWizardStepTitle: 'Output Parameters',
	invalidParameterTitle: 'Invalid Parameter',
	parameterAlreadyExists: 'That parameter already exists.',
	parserVariableRestrictionTitle : 'Parser Output Parameters', 
	parserVariableRestrictionMsg : 'Output parameters generated from Parser cannot be deleted or renamed from Parameter Section. To rename or delete this parameter, please remove it from Parser Section.',
	preprocess: 'preprocess',
	assess: 'assess',
	editorExceedWarningMsg: 'The {0} script is too long (length: {1}, limit: {2}).',
	editorLengthMsg: 'Characters remaining: {0}.',
	ParametersInputEdit: {
		createInputTitle: 'Create Input',
		createGroupTitle: 'Create Group',
		addInput: 'Add Input',
		duplicateName: 'Name is duplicate.',
		group: 'Group',
		addNewGroup: 'Add Group',
		newGroupName: 'Group Name',
		invalidGroupNameTitle: 'Invalid Group Name',
		duplicateGroupName: 'That group already exists.',
	},

	ParametersOutputEdit: {
		createOutputTitle: 'Create Output',
		addOutput: 'Add Output',
		duplicateName: 'Name is duplicate.',
		outputFromParser : 'This output parameter is generated from Parser.'
	},

	// ASSESS AND PREPROCESSOR SECTION
	preprocessorTitle: 'Preprocessor - Code Editor',
	preprocessorEditTitle: 'Preprocessor - Code Editor',
	assessTitle: 'Assess - Code Editor',
	onlyCompletedLabel: 'Only Completed',

	// ASSIGN VALUE TO OUTPUT SECTION
	valueToOutputsReadTitle: 'Assign Values to Outputs',
	valueToOutputsEditTitle: 'Assign Values to Outputs',
	assignValueToOutput: 'Assign Value to Output',
	addAssignment: 'Add Assignment',
	outputType: 'Output Type',
	outputName: 'Output Name',
	selectOutputType: 'Select output type',
	selectOutput: 'Select output',
	selectOrEnterOutput : 'Select or enter output name',
	enterOutputName: 'Enter output name',
	selectSourceType: 'Select source type',
	selectInput: 'Select input',
	selectOrEnterInput : 'Select or enter source name',
	enterSourceName: 'Enter source name',
	enterSourceValue: 'Enter source value',
	sourceType: 'Source Type',
	sourceName: 'Source Name / Value',
	sourceNameEdit: 'Source Name',
	sourceValueEdit: 'Source Value',
	output: 'OUTPUT',
	input: 'INPUT',
	params: 'PARAMS',
	param: 'PARAM',
	flow: 'FLOW',
	constant: 'CONSTANT',
	wsdata: 'WSDATA',
	confirmAddAssignValue: 'Assign the value to the output before leaving?',
	validKeyCharacters: 'A name must start with a letter. The remaining characters can be letters, numbers, and underscores.',
	assignValuesToOutputsEmptyText: 'There are no values assigned to outputs.',
	invalidAssignmentTitle: 'Invalid Assignment',
	assignmentAlreadyExists: 'That assignment already exists.',
	noAssignmentsMessage: 'There are no values assigned to outputs.',
	emptySourceName: 'Source name should not be empty',
	duplicateAssignmentMessage: '<p>Assignments are executed in the order that they appear in the table above. Duplicates are legal. If the assignment is unintentional then the last assignment will overwrite prior assignments.</p>',

	//MOCK SECTION
	mockTitle: 'Mock Definition',
	mockDetailsTitle: 'Mock Details',
	addParameter: 'Add Parameter',
	parameters: 'Parameters',
	mocks: 'mocks',
	raw: 'Raw',
	sysUpdatedOn: 'Updated On',
	sysUpdatedBy: 'Updated By',
	sysCreatedOn: 'Created On',
	sysCreatedBy: 'Created By',
	editMockDetails: 'Edit details',
	viewMockDetails: 'View details',
	addMock: 'Add',
	removeMock: 'Remove',
	enterMockName: 'Enter mock name',
	enterMockDescription: 'Enter description',
	enterMockContentName: 'Enter mock content name',
	enterMockContentValue: 'Enter mock content value',
	selectMockContentName: 'Select input name',
	duplicateMockName: 'Mock name: <b>{0}</b> already exists.',
	emptyMockName: 'Name should not be empty',
	addMock: 'Add Mock',
	addParams: 'Add Parameters',
	invalidMockTitle: 'Invalid Mock',
	emptyMockMsg: 'There are no mocks.',
	emptyMockParamMsg: 'There are no mock parameters.',
	mockName: 'Name',
	mockDescription: 'Description',
	noMockDescription: '(No description)',

	//CONTENT SECTION
	contentReadTitle: 'Content',
	contentEditTitle: 'Content',
	command: 'Command',
	noCommand: '(No command has been defined)',
	syntax: 'Syntax',
	groovy: 'Groovy',
	golang: 'Go',
	perl: 'Perl',
	powershell: 'Powershell',
	ruby: 'Ruby',
	sql: 'SQL',
	text: 'Text',
	xml: 'XML',
	json: 'JSON',
	titleForRemote: 'Content - Groovy Script',
	titleForCScript: 'Content - CScript',
	titleForPowershell: 'Content - Powershell',
	titleForCommand: 'Command and Input File',
	titleForDefault: 'Content',
	addInputFile: 'Add Input File',

	contentLabel: 'Content',
	inputFileLabel: 'Input File',
	inputFileEmptyText: 'No input file defined.',

	//PARSER SECTION
	text : 'Text',
	table : 'Table',
	xml : 'XML',
	yes: 'Confirm',
	no: 'Cancel',
	clearMarkup: 'Clear Markups',
	clearSample: 'Clear Sample',
	boundaryTitle : 'Parsing Boundary',
	updateBoundary: 'Update',
	fromField: 'From',
	toField: 'To',
	beginningOutputLabel: 'Beginning of Output',
	firstOccurrenceLabel: 'First Occurrence',
	lastOccurrenceLabel: 'Last Occurrence',
	endOutputLabel: 'End of Output',
	markupOutOfBoundaryTitle: 'Out of Boundary Markups',
	markupOutOfBoundaryBody: 'Some markups are outside of this boundary. Update boundary will remove those markups. Proceed?',
	invalidBoundaryInput: 'Boundary cannot contains more than 1 line.',
	repeatedBlock: 'Repeated Block',
	captureMarkup: 'Capture',
	removeMarkup: 'Remove Markup',
	inclusive : 'Inclusive',
	variableTable : '<b>Variable Table:</b>',
	noVariableText: 'There are no variables.',
	parserControl : 'Parser Control',
	showCode : 'Show Code',
	showMarkup : 'Show Markups',
	ignoreNotGreedy: 'Any',
	ignoreRemainder: 'Any (Greedy)',
	spaceOrTab: 'Space or Tab',
	whitespace: 'Whitespace',
	letters: 'Letters',
	lettersAndNumbers: 'Letters and Numbers',
	notLettersOrNumbers: 'Not Letters or Numbers',
	lowerCase: 'Lower Case',
	upperCase: 'Upper Case',
	number: 'Number',
	notNumber: 'Not Number',
	endOfLine: 'Line Break',
	IPv4: 'IPv4',
	MAC: 'MAC',
	decimal: 'Decimal',
	literal: 'Literal',
	orAfterField: 'or after',
	orBeforeField: 'or before',
	overwriteMarkupTitle: 'Overwrite Markups',
	overwriteMarkupBody: 'New reference output will overwrite existing markups and captures. Proceed?',
	outputFormat : 'Output Format',
	markupAndCapture : 'Markup and Capture',
	invalidVariableTitle: 'Invalid Variable',
	duplicateVariableMsg: 'The variable name already exist.',
	test : 'Run Test',	
	noXmlSegFoundError: 'No XML is found in the sample.',
	invalidXmlSyntaxError: 'The format of the sample XML is invalid.',
	autoGenIsUncheckTitle : 'Auto-Generated Code Not Available',
	autoGenIsUncheckMsg : 'Auto-Generated code option is not enabled. Do you want to enable it?<br><b>Note: </b>This will overwrite any existing code.',
	
	TestControl : {
		testResult: '<b>Test Result</b> (For {0})',
		defaultTestSampleTitle : 'Default Test Sample',
		executeTest : 'Execute Test',
		noScriptToExecute : 'There is no script to execute.',
		failedToExecute : 'Failed to execute script.',
		noResultMsg : 'There is no result for this execution',
		executeTestInProgress: 'Executing test code...',
	},	
	PatternProcessor : {
		patternTableTitle : 'Available Patterns:',
		patternHintText: '* There is no pattern for current selection. Highlight text to select its patterns.'
	},
	PlainTextParser : {
		invalidSelectionText: '<i style="color:red"> Selection cannot contain already marked text.</i>',
		clearCaptureTitle : 'Clear Captured Variables',
		clearCaptureBody : 'There are some captured variables. Clear markups will also clear all captured variables. Proceed?'
	},
	TableParser : {
		selectedColumn : 'Select Column',
		columnSeparator: 'Column Separator',
	    comma: 'Comma',
	    space: 'Space',
	    tab: 'Tab',
	    rowSeparator: 'Row Separator',
	    unixLinebreak: 'Unix (LF)',
	    winLinebreak: 'Win (CR\\LF)',
	    trim: 'Trim',
	    tabWidth: 'Tab Width',
	    selectedColumn: 'Select Column',
	    column: 'Column Number',
	    applySeparator: 'Apply Separator',
	    captureVariable: 'Capture'
	},
	XMLParser : {
		xpathControl : 'XPath Control',
		clearPath: 'Clear Path',
		capturePath: 'Capture',
		xpath : 'XPath'		
	},
	ParserCode : {
		autoGenCheck: 'Auto Generated',
		autoGenConfirmTitle: 'Overwrite Code',
		autoGenConfirmBody: 'This action will overwrite current code with auto generated code. Proceed?',
	},
	XPathTokenEditor : {
		update: 'Update',
    	removeNode: 'Remove Node',	
	    predicates: 'Predicates:',
	    selector: 'Selector',
	    addTextPredicate: 'New Text',
	    addAttrPredicate: 'New Attribute',
	    addCustPredicate: 'Custom',
	    and: 'AND',
    	or: 'OR',
    	contains: 'Contains',
    	startsWith: 'Start With',
    	endsWith: 'End With',
    	xpathProperty : 'XPath Property',
    	noPredicate : 'This node has no predicate.',
    	noProperty : 'Select a node from xpath above to display its property.'
	},	
	XPathAttrPredicate : {
		attribute: 'Attribute',
	},
	XPathCustomizedPredicate : {
		customized: 'Custom',
	},
	MarkupCapture : {
		markupCapture : 'Capture Variable',
		variable : 'Variable Name',
		containerType : 'Container Type',
		color : 'Color',
		column : 'Column',
		xpath : 'XPath'
	},

	//CONDITIONS AND SEVERITIES SECTION
	severitiesAndConditionsTitle: 'Severities and Conditions',
	severitiesEditTitle: 'Severities',
	conditionsEditTitle: 'Conditions',
	severitiesTitle: '<b>Severities</b>',
	conditionsTitle: '<b>Conditions</b>',
	criticalTitle: 'Critical',
	severeTitle: 'Severe',
	warningTitle: 'Warning',
	goodTitle: 'Good',
	badTitle: 'Bad',
	noSeveritiesMessage: 'No severities are defined.',
	noConditionsMessage: 'No conditions are defined.',
	requiredField : 'This field is required.',

	SummaryAndDetailsRead: {
		summary: '<b>Summary</b>',	
		details: '<b>Details</b>'
	},
	textToDisplay : 'Text to Display',
	insertVariable : 'Insert Variable',
	htmlEncodeSummary : 'Encode Summary',
	htmlEncodeDetail : 'Encode Detail',
	summaryDisplayRule : 'Summary Display Rules:',
	detailDisplayRule : 'Detail Display Rules:',
	addRule : 'Add Rule',
	summaryRule : 'Define a Summary Display Rule',
	detailRule : 'Define a Detail Display Rule',
	summaryEditTitle: 'Summary',
	condition : 'Condition',
	severity : 'Severity',	
	detailsEditTitle: 'Details',
	noSummaryDefined: 'No summary has been defined.',
	noDetailDefined: 'No detail has been defined.',
	multipleRuleMessage : 'Only one rule can be matched. Rules are executed in the order that they appear in the table above.',

	TextField: {
		invalidName: 'Name is required and must be in alphanumerics, but may contain single spaces or underscores \'_\'.',
		typeInAName: 'Type in a name'
	},

	Execute: {
		executionTooltip: 'Execute',
		executeTitle: 'Execute',
		executeSuccess: 'The request for executing {type}:{fullName} has been accepted.',
		newWorksheet: 'New WorkSheet',
		activeWorksheet: 'Current WorkSheet',
		params: 'Params',
		parameters: 'Parameters',
		addParam: 'Add',
		removeParams: 'Remove',
		invalidMockName: 'The mock name is required.',
		execute: 'Execute',
		debug: 'Debug',
		mock: 'Mock'
	},

	value: 'Value',
	fieldNameTextForOutputs: 'Select an output',
	fieldNameTextForInputs: 'Select an input',
	fieldNameTextForFlows: 'Select or enter a FLOW name',
	fieldNameTextForWSDATA: 'Select or enter a WSDATA name',
	fieldNameTextForOthers: 'Enter a name',
	invalidOutputParametersInFormat: 'There are OUTPUT parameters in the format that do not exist.',
	invalidInputParametersInFormat: 'There are INPUT parameters in the format that do not exist.',

	addField: 'Add Field',
	format: 'Format',
	formatLabel: 'Format:',
	preview: 'Preview',
	previewLabel: '<b>Preview:</b>',
	confirmAddFieldToSummary: 'Do you want to add the field to the summary before leaving?',
	confirmAddFieldToDetails: 'Do you want to add the field to the details before leaving?',
	includeRaw: 'Include Raw',

    //CONDITIONS AND SEVERITIES SECTION    
    AND: 'AND',
    OR: 'OR',
    category: 'Category',
    determiner: 'Determiner',
    operandType: 'Type',
    operand: 'Operand',
    operator: 'Operator',
    selectType: 'Select a type',
    selectOperator: 'Select an operator',
    enterOperand: 'Enter a name',
    selectOperand: 'Select input',
    enterOperandValue: 'Enter a value',

	Commit: {
		commitTitle: 'Commit ActionTask',
		commit: 'Commit',
		cancel: 'Cancel',
		comment: 'Comment',
		commitSuccess: 'Document committed successfully'
	},

	// Versions
	viewVersionList: 'View Version List',
	versions: 'Version List',
	documentName: 'ActionTask Name',
	versionNumber: 'Version #',
	stableVersion: 'Committed',
	referenced: 'Referenced',
	type: 'Type',
	comment: 'Comment',
	createdBy: 'Created By',
	createdOn: 'Created On',
	commitVersion: 'Commit',
	viewVersion: 'View',
	compareVersion: 'Compare',
	rollbackVersion: 'Rollback',
	selectVersion: 'Use Selected',
	refresh: 'Refresh',
	firstPage: 'First Page',
	lastPage: 'Last Page',
	previousPage: 'Previous Page',
	nextPage: 'Next Page',
}

