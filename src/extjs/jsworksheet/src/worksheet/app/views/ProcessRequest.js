glu.defView('RS.worksheet.ProcessRequest', {
	padding : 15,
	autoScroll: true,
	dockedItems: [{
		xtype: 'toolbar',		
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls: 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{processRequestTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',			
		}, 'abort', '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrg}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-btn-icon x-tbar-loading',
			handler: '@{loadProcessRequest}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	layout: {
		type: 'vbox',
		align: 'stretch'
	},	
	items: [{
		style : 'border-top:1px solid silver',
		padding : '8 0',
		defaultType: 'displayfield',
		defaults: {
			labelWidth: 125,
			margin : '4 0'
		},
		items : ['number', {
			xtype: 'container',	
			layout : 'hbox',		
			items: [{
				xtype: 'displayfield',
				labelWidth: 125,		
				name: 'problemNumber'				
			}, {
				xtype: 'image',				
				hidden: '@{!jumpToWorksheetIsVisible}',
				listeners: {
					handler: '@{jumpToWorksheet}',
					render: function(image) {
						image.getEl().on('click', function() {
							image.fireEvent('handler')
						})
					}
				}
			}]
		}, 'wiki', 'timeout', 'status', 'duration']
	}]
})