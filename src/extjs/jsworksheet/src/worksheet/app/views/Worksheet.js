glu.defView('RS.worksheet.Worksheet', {
	bodyPadding: '0 15 15 15',
	layout: 'card',
	activeItem: '@{activeTab}',
	layoutConfig: {
		deferrredRender: false
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		itemId: 'actionBar',
		padding : '15 15 8 15',
		defaultButtonUI: 'display-toolbar-button',
		items: ['->', {
			name: 'generalTab',
			pressed: '@{generalTabIsPressed}'
		}, {
			name: 'resultsTab',
			pressed: '@{resultsTabIsPressed}'
		}, {
			name: 'debugTab',
			pressed: '@{debugTabIsPressed}'
		}, {
			name: 'dataTab',
			pressed: '@{dataTabIsPressed}'
		}]
	}],
	displayName: '@{worksheetTitle}',
	setDisplayName: function(value) {
		this.getPlugin('searchfilter').toolbar.down('#tableMenu').setText(value)
	},
	displayFilter: '@{displayFilter}',
	setDisplayFilter: function(value) {
		this.getPlugin('searchfilter').setDisplayFilter(value)
	},
	plugins: [{
		ptype: 'searchfilter',
		pluginId: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true,
		useWindowParams: true,
		gridName: 'resultsGrid'
	}],
	items: [{
		autoScroll: true,		
		dockedItems: [{
			xtype: 'toolbar',		
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: [{
				name: 'back',
				iconCls: 'icon-large icon-reply-all rs-icon'			
			}, {
				name: 'save',
				listeners: {
					render: function(button) {
						clientVM.updateSaveButtons(button);
					}
				}
			}, 'setActive', '->', {
				iconCls: 'rs-social-button @{socialPressedCls}',
				style: 'left: -1px',
				enableToggle: true,
				pressed: '@{socialIsPressed}',
				tooltip: '~~socialTooltip~~'
			}, {
				xtype: 'followbutton',
				streamId: '@{id}',
				streamType: 'worksheet'
			}, {
				xtype: 'sysinfobutton',
				sysId: '@{id}',
				sys_id: '',
				sysCreated: '@{sysCreatedOn}',
				sysCreatedBy: '@{sysCreatedBy}',
				sysUpdated: '@{sysUpdatedOn}',
				sysUpdatedBy: '@{sysUpdatedBy}',
				sysOrg: '@{sysOrg}',
				dateFormat: '@{userDateFormat}'
			}, {
				iconCls: 'x-btn-icon x-tbar-loading',
				tooltip: '~~refresh~~',
				handler: '@{refresh}',
				listeners: {
					render: function(button) {
						clientVM.updateRefreshButtons(button);
					}
				}
			}]
		}],
		layout: 'border',
		items: [{
			style : 'border-top:1px solid silver',
			padding : '8 0',
			region: 'center',
			autoScroll: true,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				layout: 'column',
				items: [{
					columnWidth: 0.6,
					layout: 'anchor',
					defaultType: 'textfield',
					defaults: {
						anchor: '-30',
						margin : '4 0',
						labelWidth : 130
					},
					items: [{
						xtype: 'displayfield',
						name: 'number',
						cls : 'rs-displayfield-value'
					}, 'alertId', 'reference', 'correlationId', {
						xtype: 'displayfield',
						name: 'sirId',
						cls : 'rs-displayfield-value'
					},{
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [{
							xtype: 'triggerfield',
							flex: 1,
							name: 'assignedToName',
							labelWidth : 130,
							margin: '0px 10px 0px 0px',
							trigger1Cls: Ext.baseCSSPrefix + 'form-search-trigger',
							onTrigger1Click: function() {
								this.fireEvent('handleTriggerClick')
							},
							trigger2Cls: Ext.baseCSSPrefix + 'form-clear-trigger',
							onTrigger2Click: function() {
								this.fireEvent('clearTriggerClick')
							},
							listeners: {
								handleTriggerClick: '@{assignedToClicked}',
								clearTriggerClick: '@{assignedToClickedClear}',
								registerAssignedTo: '@{registerAssignedTo}',
								render: function(field) {
									field.fireEvent('registerAssignedTo', field, field)
								}
							}
						}, {
							xtype: 'image',
							cls: 'rs-btn-edit',
							hidden: '@{!jumpToAssignedToIsVisible}',
							listeners: {
								handler: '@{jumpToAssignedTo}',
								render: function(image) {
									image.getEl().on('click', function() {
										image.fireEvent('handler')
									})
								}
							}
						}]
					}]
				}, {
					columnWidth: 0.4,
					layout: 'anchor',
					defaults: {
						anchor: '100%',
						labelWidth : 130,
						editable : false
					},
					items: [{
						xtype: 'combobox',
						name: 'condition',
						store: '@{conditionStore}',
						queryMode: 'local',
						displayField: 'name',
						valueField: 'value'
					}, {
						xtype: 'combobox',
						name: 'severity',
						store: '@{severityStore}',
						queryMode: 'local',
						displayField: 'name',
						valueField: 'value'
					}]
				}]
			}, {
				xtype: 'textfield',
				name: 'summary',
				labelWidth : 130
			}, {
				xtype: 'textarea',
				name: 'description',
				labelWidth : 130
			}, {
				xtype: 'textarea',
				name: 'workNotes',
				labelWidth : 130
			}, {
				xtype: 'grid',
				cls : 'rs-grid-dark',
				flex: 1,
				store: '@{workNotesDataStore}',
				columns: '@{workNotesColumns}'
			}]
		}, {
			region: 'east',
			split: true,
			width: 480,
			stateId: 'wikiSocialBar',
			stateful: true,
			hidden: '@{!socialIsPressed}',
			layout: 'fit',
			items: [{
				xtype: '@{socialDetail}'
			}]
		}]
	}, {
		layout: 'border',
		dockedItems: [{
			xtype: 'toolbar',		
			name: 'actionBar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'		
			},
			items: [{
				name: 'back',
				iconCls: 'icon-large icon-reply-all rs-icon'				
			}, '|', 'setActive', {
				name: 'showAll',
				cls : 'rs-small-btn rs-toggle-btn',
				enableToggle: true,
				pressed: '@{all}'
			}, {
				iconCls: 'rs-social-button @{socialPressedCls}',
				style: 'left: -1px',
				enableToggle: true,
				pressed: '@{socialIsPressed}',
				tooltip: '~~socialTooltip~~'
			}]
		}],
		socialId: '@{id}',
		setSocialId: function(value) {
			this.getPlugin('pager').setSocialId(value)
		},
		plugins: [{
			ptype: 'pager',
			pluginId: 'pager',
			showSocial: true,
			socialStreamType: 'worksheet'
		}],
		items: [{
			region: 'center',
			xtype: 'grid',
			cls : 'rs-grid-dark',
			viewConfig: {
				enableTextSelection: true
			},
			store: '@{worksheetResults}',
			columns: '@{worksheetColumns}',
			stateId: '@{stateId}',
			stateful: true,
			itemId: 'resultsGrid',
			selModel: {
				selType: 'resolvecheckboxmodel',
				columnTooltip: RS.common.locale.editColumnTooltip,
				columnTarget: '_self',
				columnEventName: 'editAction',
				columnIdField: 'sysId'
			},
			plugins: [{
				ptype: 'resolveexpander',
				rowBodyTpl: new Ext.XTemplate(
					// '<span style="font-weight:bold;padding-left:35px">Queue Name</span>--{esbaddr}<br/>',
					// '<span style="font-weight:bold;padding-left:35px">Duration</span>--{duration}<br/>',
					// '<span style="font-weight:bold;padding-left:35px">Process Request</span>--{processNumber}<br/>',
					// '<span style="font-weight:bold;padding-left:35px">Target</span>--{address}<br/>',
					// '<span style="font-weight:bold;padding-left:35px">Guid</span>--{targetGUID}<br/>',
					'<div resolveId="resolveRowBody" style="color:#333">',
					'<span style="padding-left:10px">Detail:</span><br/>',
					'<span>{[this.doDetai(values)]}</span>',
					'</div>', {
						doDetai: function(detail) {
							return Ext.util.Format.nl2br('<pre style="padding-left:10px;padding-top:10px">' + Ext.String.htmlEncode(detail.detail) + '</pre>')
						}
					}
				)
			}],
			listeners: {
				editAction: '@{editResult}'
			}
		}, {
			region: 'east',
			split: true,
			width: 480,
			stateId: 'wikiSocialBar',
			stateful: true,
			hidden: '@{!socialIsPressed}',
			layout: 'fit',
			items: [{
				xtype: '@{socialDetail}'
			}]
		}]
	}, {
		layout: 'border',
		tbar: {
			xtype: 'toolbar',
			cls: 'actionBar rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: [{
				name: 'back',
				iconCls: 'icon-large icon-reply-all rs-icon'
			}, '->', {
				iconCls: 'rs-social-button @{socialPressedCls}',
				style: 'left: -1px',
				enableToggle: true,
				pressed: '@{socialIsPressed}',
				tooltip: '~~socialTooltip~~'
			}, {
				xtype: 'followbutton',
				streamId: '@{id}',
				streamType: 'worksheet'
			}, {
				xtype: 'sysinfobutton',
				sysId: '@{id}',
				sys_id: '',
				sysCreated: '@{sysCreatedOn}',
				sysCreatedBy: '@{sysCreatedBy}',
				sysUpdated: '@{sysUpdatedOn}',
				sysUpdatedBy: '@{sysUpdatedBy}',
				sysOrg: '@{sysOrg}',
				dateFormat: '@{userDateFormat}'
			}, {
				iconCls: 'x-btn-icon x-tbar-loading',
				tooltip: '~~refresh~~',
				handler: '@{refresh}'
			}]
		},
		items: [{			
			region: 'center',
			xtype: 'textarea',
			readOnly: true,
			labelAlign: 'top',
			hideLabel: true,
			name: 'debug'
		}, {
			region: 'east',
			split: true,
			width: 480,
			stateId: 'wikiSocialBar',
			stateful: true,
			hidden: '@{!socialIsPressed}',
			layout: 'fit',
			items: [{
				xtype: '@{socialDetail}'
			}]
		}]
	}, {
		layout: 'border',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: [{
				name: 'back',
				iconCls: 'icon-large icon-reply-all rs-icon'		
			}, 'addWSData', 'deleteWSData', '->', {
				iconCls: 'rs-social-button @{socialPressedCls}',
				style: 'left: -1px',
				enableToggle: true,
				pressed: '@{socialIsPressed}',
				tooltip: '~~socialTooltip~~'
			}, {
				xtype: 'followbutton',
				streamId: '@{id}',
				streamType: 'worksheet'
			}, {
				xtype: 'sysinfobutton',
				sysId: '@{id}',
				sys_id: '',
				sysCreated: '@{sysCreatedOn}',
				sysCreatedBy: '@{sysCreatedBy}',
				sysUpdated: '@{sysUpdatedOn}',
				sysUpdatedBy: '@{sysUpdatedBy}',
				sysOrg: '@{sysOrg}',
				dateFormat: '@{userDateFormat}'
			}, {
				iconCls: 'x-btn-icon x-tbar-loading',
				tooltip: '~~refresh~~',
				handler: '@{refresh}'
			}]
		}],
		items: [{
			region: 'center',
			xtype: 'grid',
			cls : 'rs-grid-dark',
			name: 'wsData',
			columns: '@{wsColumns}',
			selModel: {
				selType: 'resolvecheckboxmodel',
				columnTooltip: RS.common.locale.editColumnTooltip,
				glyph : 0xF044,
				columnTarget: '_self',
				columnEventName: 'editAction',
				columnIdField: 'name'
			},
			listeners: {
				editAction: '@{editWSData}'
			}
		}, {
			region: 'east',
			split: true,
			width: 480,
			stateId: 'worksheetSocialBar',
			stateful: true,
			hidden: '@{!socialIsPressed}',
			layout: 'fit',
			items: [{
				xtype: '@{socialDetail}'
			}]
		}]
	}],
	listeners: {
		afterrender: function() {
			var splitters = this.query('bordersplitter')
			Ext.Array.forEach(splitters, function(splitter) {
				splitter.setSize(1, 1)
				splitter.getEl().setStyle({
					backgroundColor: '#fbfbfb'
				})
			})
			this.setDisplayFilter(this.displayFilter)
		}
	}
})
