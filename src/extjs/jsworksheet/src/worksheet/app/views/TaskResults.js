glu.defView('RS.worksheet.TaskResults', {
	padding: 15,
	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'grid',
		cls : 'rs-grid-dark',
		border: false,
		stateId: '@{stateId}',
		displayName: '@{displayName}',
		stateful: true,
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}, {
			ptype: 'columnautowidth'
		}],
		store: '@{taskResults}',
		selModel: {
			selType: 'resolvecheckboxmodel',	
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'id'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: [{
				name: 'deleteTaskResult'
			}]
		}],
		columns: '@{columns}',
		viewConfig: {
			enableTextSelection: true
		},
		listeners: {
			editAction: '@{editTaskResult}',
			selectAllAcrossPages: '@{selectAllAcrossPages}',
			selectionChange: '@{selectionChanged}'
		}
	}]
});