glu.defView('RS.worksheet.ListRegistrationDetails', {

	asWindow: {
		title: '~~title~~',
		height: 425,
		width: 450,
		modal: true
	},

	bodyPadding: '10px',
	buttonAlign: 'left',
	buttons: ['close'],

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaultType: 'displayfield',
	items: ['uname',
		'ustatus',
		'uguid',
		'utype',
		'uipaddress',
		'ubuild', 'sysUpdatedOn', 'sysUpdatedBy', 'sysCreatedOn', 'sysCreatedBy'
	]
})