glu.defView('RS.worksheet.WorksheetPicker', {
	title: '~~selectWorksheetTitle~~',
	padding : 10,
	hidden: '@{!showPicker}',		
	width: 650,
	height: 400,
	//ui: '@{ui}',
	cls : 'rs-modal-popup',
	closeAction: 'hide',
	//baseCls: '@{baseCls}',
	draggable: false,
	resizable: false,
	target: '@{target}',
	modal: '@{!clientDialog}',
	listeners: {
		show: function(win) {
			if (win.target) {
				win.alignTo(win.target.getEl().id, 'tr-br')
			}
		},
		beforeclose: '@{beforeClose}'
	},
	layout: 'fit',
	items: [{		
		xtype: 'grid',
		name: 'worksheets',
		cls : 'rs-grid-dark',
		margin : '0 0 10 0',
		flex: 1,
		columns: '@{worksheetsColumns}',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			items: [{
				xtype: 'textfield',
				name: 'searchText',
				hideLabel: true,
				width: 250,
				emptyText: '~~searchWorksheet~~'
			}, {
				iconCls: 'icon-large icon-user rs-icon @{myWorksheetsPressedCls}',
				name: 'myWorksheets',
				text: '',
				tooltip: '@{myWorksheetsTooltip}'
			}]
		}],
		plugins: [{
			ptype: 'pager',
			showSysInfo: false,
			allowAutoRefresh: false
		}, {
			ptype: 'rowexpander',
			pluginId: 'rowExpander',
			rowBodyTpl: new Ext.XTemplate('<span name="replace"></span>')
		}],
		viewConfig: {
			listeners: {
				expandbody: function(rowNode, record, expandRow, eOpts) {
					this.ownerCt.fireEvent('expandbody', this.ownerCt, rowNode, record, expandRow, eOpts)
				}
			}
		},
		listeners: {
			expandbody: '@{expandBody}',
			itemdblclick: '@{dblclick}',
			render: function(grid) {
				var plugin = grid.getPlugin('rowExpander')
				if (plugin)
					grid.on('cellclick', function(view, td, cellIndex, record, tr, rowIndex, e, eOpts) {
						if (cellIndex > 0) {
							plugin.onDblClick(view, record, null, rowIndex, e)
						}
					})
				grid.store.on('beforeload', function() {
					plugin.recordsExpanded = {}
				})
			}
		}
	}],
	buttonAlign: 'left',
	buttons: [{
		name: 'go',
		cls: 'rs-proceed-button rs-med-button',
		iconCls: 'icon-large icon-play rs-client-button'
	}, {
		name : 'select',
		cls: 'rs-med-button rs-btn-light',
	},{
		name : 'newWorksheet',
		cls: 'rs-med-button rs-btn-light',
	},{
		name : 'view',
		cls: 'rs-med-button rs-btn-light',
	},{
		name : 'viewAll',
		cls: 'rs-med-button rs-btn-light',
	},{
		name :  'cancel',
		cls: 'rs-med-button rs-btn-light',
	}]
})
glu.defView('RS.worksheet.WorksheetPicker', 'windowParam', {

	asWindow: {
		title: '~~selectWorksheetTitle~~',
		hidden: '@{!showPicker}',
		width: 600,
		height: 400,
		ui: '@{ui}',
		baseCls: '@{baseCls}',
		draggable: false,
		resizable: false,
		target: '@{target}',
		modal: '@{!clientDialog}',
		listeners: {
			show: function(win) {
				if (win.target) {
					win.alignTo(win.target.getEl().id, 'tr-br')
				}
			},
			beforeclose: '@{beforeClose}'
		}
	},

	layout: 'fit',
	dockedItems: [{
		xtype: 'toolbar',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{tabTitle}'
		}, '->', 'worksheetTab', 'archivedWorksheetTab']
	}],

	items: [{
		xtype: 'grid',
		name: 'worksheets',
		hidden: '@{!worksheetTabIsPressed}',
		flex: 1,
		columns: '@{worksheetsColumns}',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: [{
				xtype: 'textfield',
				name: 'searchText',
				hideLabel: true,
				width: 250,
				emptyText: '~~searchWorksheet~~'
			}, {
				iconCls: 'icon-large icon-user rs-icon @{myWorksheetsPressedCls}',
				name: 'myWorksheets',
				text: '',
				tooltip: '@{myWorksheetsTooltip}'
			}]
		}],
		plugins: [{
			ptype: 'pager',
			showSysInfo: false,
			allowAutoRefresh: false
		}, {
			ptype: 'rowexpander',
			pluginId: 'rowExpander',
			rowBodyTpl: new Ext.XTemplate('<span name="replace"></span>')
		}],
		viewConfig: {
			listeners: {
				expandbody: function(rowNode, record, expandRow, eOpts) {
					this.ownerCt.fireEvent('expandbody', this.ownerCt, rowNode, record, expandRow, eOpts)
				}
			}
		},
		listeners: {
			expandbody: '@{expandBody}',
			itemdblclick: '@{dblclick}',
			render: function(grid) {
				var plugin = grid.getPlugin('rowExpander')
				if (plugin)
					grid.on('cellclick', function(view, td, cellIndex, record, tr, rowIndex, e, eOpts) {
						if (cellIndex > 0) {
							plugin.onDblClick(view, record, null, rowIndex, e)
						}
					})
				grid.store.on('beforeload', function() {
					plugin.recordsExpanded = {}
				})
			}
		}
	}, {
		xtype: 'grid',
		name: 'archived',
		hidden: '@{!archivedWorksheetTabIsPressed}',
		store: '@{archivedWorksheets}',
		columns: [{
			header: '~~number~~',
			flex: 1,
			dataIndex: 'number'
		}, {
			header: '~~reference~~',
			flex: 1,
			dataIndex: 'reference',
			renderer: function(value, metaData, record) {
				return Ext.String.htmlEncode(value) || record.get('correlationId') || record.get('alertId') || ''
			}
		}, {
			text: RS.common.locale.sysUpdated,
			dataIndex: 'sysUpdatedOn',
			filterable: false,
			sortable: true,
			width: 200,
			dateFormat: '@{userDateFormat}'
		}],
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: [{
				xtype: 'textfield',
				name: 'searchText',
				hideLabel: true,
				width: 250,
				emptyText: '~~searchWorksheet~~'
			}, {
				iconCls: 'icon-large icon-user rs-icon @{myWorksheetsPressedCls}',
				name: 'myWorksheets',
				text: '',
				tooltip: '@{myWorksheetsTooltip}'
			}]
		}],
		plugins: [{
			ptype: 'pager',
			showSysInfo: false,
			allowAutoRefresh: false
		}, {
			ptype: 'rowexpander',
			pluginId: 'rowExpander',
			rowBodyTpl: new Ext.XTemplate('<span name="replace"></span>')
		}],
		viewConfig: {
			listeners: {
				expandbody: function(rowNode, record, expandRow, eOpts) {
					this.ownerCt.fireEvent('expandbody', this.ownerCt, rowNode, record, expandRow, eOpts)
				}
			}
		},
		listeners: {
			expandbody: '@{expandBody}',
			itemdblclick: '@{dblclick}',
			render: function(grid) {
				var plugin = grid.getPlugin('rowExpander')
				if (plugin)
					grid.on('cellclick', function(view, td, cellIndex, record, tr, rowIndex, e, eOpts) {
						if (cellIndex > 0) {
							plugin.onDblClick(view, record, null, rowIndex, e)
						}
					})
				grid.store.on('beforeload', function() {
					plugin.recordsExpanded = {}
				})
			}
		}
	}],
	buttonAlign: 'left',
	buttons: [{
		name: 'go',
		iconCls: 'icon-large icon-play rs-client-button'
	}, 'select', 'newWorksheet', 'view', 'viewAll', 'cancel']
})