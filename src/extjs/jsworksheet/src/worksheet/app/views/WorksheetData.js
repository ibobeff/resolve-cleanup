glu.defView('RS.worksheet.WorksheetData', {
	height: 400,
	width: 600,
	padding : 15,
	modal : true,
	cls : 'rs-modal-popup',
	title: '~~worksheetDataTitle~~',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'displayfield',
		name: 'name',
		hidden: '@{!editMode}'
	}, {
		xtype: 'textfield',
		name: 'name',
		hidden: '@{editMode}'
	}, {
		xtype: 'textarea',
		name: 'value',
		flex: 1
	}],
	buttons: [{
		name : 'save',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})