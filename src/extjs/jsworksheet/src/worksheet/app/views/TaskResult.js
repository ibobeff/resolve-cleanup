glu.defView('RS.worksheet.TaskResult', {
	padding: 15,
	layout: 'card',
	activeItem: '@{activeTab}',
	dockedItems: [{
		xtype: 'toolbar',	
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls : 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		items: [{
				xtype: 'tbtext',
				cls: 'rs-display-name',
				text: '@{taskResultTitle}'
			},
			'->', {
				name: 'generalTab',
				pressed: '@{generalTabIsPressed}'
			}, {
				name: 'detailTab',
				pressed: '@{detailTabIsPressed}'
			}, {
				name: 'rawTab',
				pressed: '@{rawTabIsPressed}'
			}
		]
	}, {
		xtype: 'toolbar',		
		cls: 'actionBar rs-dockedtoolbar',
		items: [{
			name : 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',
			cls : 'rs-small-btn rs-btn-light'
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sys_id: '',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrg}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-btn-icon x-tbar-loading',
			handler: '@{loadTaskResult}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	items: [{
		style : 'border-top:1px solid silver',
		padding : '8 0 0 0',
		autoScroll: true,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{	
			layout: 'column',
			items: [{
				layout: 'anchor',
				columnWidth: 0.5,
				defaultType: 'displayfield',
				defaults: {
					anchor: '-20',
					labelWidth: 125
				},
				items: [{
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'displayfield',
						labelWidth: 125,
						name: 'taskName',
						margin: '0px 10px 0px 0px'
					}, {
						xtype: 'image',
						cls: 'rs-btn-edit',
						hidden: '@{!jumpToActionTaskIsVisible}',
						listeners: {
							handler: '@{jumpToActionTask}',
							render: function(image) {
								image.getEl().on('click', function() {
									image.fireEvent('handler')
								})
							}
						}
					}]
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'displayfield',
						labelWidth: 125,
						name: 'problemNumber',
						margin: '0px 10px 0px 0px'
					}, {
						xtype: 'image',
						cls: 'rs-btn-edit',
						hidden: '@{!jumpToWorksheetIsVisible}',
						listeners: {
							handler: '@{jumpToWorksheet}',
							render: function(image) {
								image.getEl().on('click', function() {
									image.fireEvent('handler')
								})
							}
						}
					}]
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'displayfield',
						labelWidth: 125,
						name: 'processNumber',
						margin: '0px 10px 0px 0px'
					}, {
						xtype: 'image',
						cls: 'rs-btn-edit',
						hidden: '@{!jumpToProcessRequestIsVisible}',
						listeners: {
							handler: '@{jumpToProcessRequest}',
							render: function(image) {
								image.getEl().on('click', function() {
									image.fireEvent('handler')
								})
							}
						}
					}]
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'displayfield',
						labelWidth: 125,
						name: 'targetGUID',
						margin: '0px 10px 0px 0px'
					}, {
						xtype: 'image',
						cls: 'rs-btn-edit',
						hidden: '@{!showGUIDDetails}',
						listeners: {
							handler: '@{displayGUIDDetails}',
							render: function(image) {
								image.getEl().on('click', function() {
									image.fireEvent('handler')
								})
							}
						}
					}]
				}, 'address', 'esbAddr']
			}, {
				layout: 'anchor',
				columnWidth: 0.5,
				defaultType: 'displayfield',
				defaults: {
					anchor: '-20',
					labelWidth: 125
				},
				items: ['duration', 'completion', 'condition', 'severity']
			}]
		}, {
			flex: 1,
			minHeight: 300,
			bodyPadding: '10px',
			cls: 'rs-monospace',
			title: '~~summary~~',
			html: '@{summaryDisplay}',
			autoScroll: true
		}]
	}, {
		layout: 'fit',
		items: [{
			autoScroll: true,
			style: 'border: 1px solid #999 !important',
			cls: 'rs-monospace',
			bodyPadding: '10px',
			html: '@{detailDisplay}',
			listeners: {
				afterrender: function(panel, eOpt) {
					clientVM.makeCtrlASelectAllOnView(panel);
				}
			}
		}]
	}, {
		layout: 'fit',
		items: [{
			autoScroll: true,
			style: 'border: 1px solid #999 !important',
			cls: 'rs-monospace',
			bodyPadding: '10px',
			html: '@{rawText}',
			listeners: {
				afterrender: function(panel, eOpt) {
					clientVM.makeCtrlASelectAllOnView(panel);
				}
			}
		}]
	}]
})