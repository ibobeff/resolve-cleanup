glu.defView('RS.worksheet.TaskRequest', {
	padding: '10px',
	bodyPadding: '10px',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{taskRequestTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar',
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-eject rs-icon',
			text: ''
		}, 'abort', '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{created}',
			sysCreatedBy: '@{createdBy}',
			sysUpdated: '@{updated}',
			sysUpdatedBy: '@{updatedBy}',
			sysOrg: '@{sysOrg}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-btn-icon x-tbar-loading',
			handler: '@{loadTaskRequest}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	layout: 'column',
	items: [{
		layout: 'anchor',
		columnWidth: 0.5,
		defaultType: 'displayfield',
		defaults: {
			anchor: '-20',
			labelWidth: 125
		},
		items: ['taskRequest', 'status', 'runbook', {
			xtype: 'fieldcontainer',
			layout: 'hbox',
			items: [{
				xtype: 'displayfield',
				labelWidth: 125,
				name: 'processRequest',
				margin: '0px 10px 0px 0px'
			}, {
				xtype: 'image',
				cls: 'rs-btn-edit',
				hidden: '@{!jumpToProcessRequestIsVisible}',
				listeners: {
					handler: '@{jumpToProcessRequest}',
					render: function(image) {
						image.getEl().on('click', function() {
							image.fireEvent('handler')
						})
					}
				}
			}]
		}, {
			xtype: 'fieldcontainer',
			layout: 'hbox',
			items: [{
				xtype: 'displayfield',
				labelWidth: 125,
				name: 'worksheet',
				margin: '0px 10px 0px 0px'
			}, {
				xtype: 'container',
				autoEl: {
					tag: 'a',
					href: '@{worksheetLink}'
				},
				setHref: function(href) {
					debugger;
				},
				items: [{
					xtype: 'image',
					cls: 'rs-btn-edit',
					hidden: '@{!jumpToWorksheetIsVisible}',
					listeners: {
						handler: '@{jumpToWorksheet}',
						render: function(image) {
							image.getEl().on('click', function() {
								image.fireEvent('handler')
							})
						}
					}
				}]
			}]
		}, {
			xtype: 'fieldcontainer',
			layout: 'hbox',
			items: [{
				xtype: 'displayfield',
				labelWidth: 125,
				name: 'actionTask',
				margin: '0px 10px 0px 0px'
			}, {
				xtype: 'image',
				cls: 'rs-btn-edit',
				hidden: '@{!jumpToActionTaskIsVisible}',
				listeners: {
					handler: '@{jumpToActionTask}',
					render: function(image) {
						image.getEl().on('click', function() {
							image.fireEvent('handler')
						})
					}
				}
			}]
		}]
	}, {
		layout: 'anchor',
		columnWidth: 0.5,
		defaultType: 'displayfield',
		defaults: {
			anchor: '-20',
			labelWidth: 125
		},
		items: ['createdText', 'createdBy', 'lastUpdatedText', 'lastUpdatedBy', 'nodeId', 'duration']
	}]
})