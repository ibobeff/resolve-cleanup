glu.defView('RS.worksheet.TaskRequests', {
	padding: 15,
	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'grid',
		cls : 'rs-grid-dark',
		border: false,
		stateId: '@{stateId}',
		displayName: '@{displayName}',
		stateful: true,
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}, {
			ptype: 'columnautowidth'
		}],
		store: '@{taskRequests}',
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTooltip: RS.common.locale.editColumnTooltip,
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'id'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: ['deleteTaskRequest', 'abortTaskRequest']
		}],
		columns: '@{columns}',
		viewConfig: {
			enableTextSelection: true
		},
		listeners: {
			editAction: '@{editTaskRequest}',
			selectAllAcrossPages: '@{selectAllAcrossPages}',
			selectionChange: '@{selectionChanged}'
		}
	}]
});