glu.defView('RS.worksheet.ProcessRequests', {
	padding: 15,
	layout: 'fit',
	items: [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'grid',
		border: false,
		stateId: '@{stateId}',
		displayName: '@{displayName}',
		stateful: true,
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}, {
			ptype: 'columnautowidth'
		}],
		store: '@{processRequests}',
		selModel: {
			selType: 'resolvecheckboxmodel',	
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'id'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: ['deleteProcessRequest', 'deleteAllProcessRequest','abortProcessRequest','abortAllProcessRequest']
		}],
		columns: '@{columns}',
		viewConfig: {
			enableTextSelection: true
		},
		listeners: {
			editAction: '@{editProcessRequest}'
		}
	}]
});