glu.defModel('RS.worksheet.ListRegistrationDetails', {
	uname: '',
	ustatus: '',
	uguid: '',
	utype: '',
	uipaddress: '',
	ubuild: '',

	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',

	fields: ['uname',
		'ustatus',
		'uguid',
		'utype',
		'uipaddress',
		'ubuild'
	].concat(RS.common.grid.getSysFields()),

	init: function() {
		this.ajax({
			url: '/resolve/service/registration/list',
			params: {
				filter: Ext.encode([{
					field: 'uguid',
					type: 'auto',
					condition: 'equals',
					value: this.guid
				}])
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					if (response.records && response.records.length > 0) {
						this.loadData(response.records[0])
						this.set('sysCreatedOn', Ext.Date.format(Ext.Date.parse(response.records[0].sysCreatedOn, 'time'), clientVM.getUserDefaultDateFormat()))
						this.set('sysUpdatedOn', Ext.Date.format(Ext.Date.parse(response.records[0].sysUpdatedOn, 'time'), clientVM.getUserDefaultDateFormat()))
					}
				} else
					clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	}
})