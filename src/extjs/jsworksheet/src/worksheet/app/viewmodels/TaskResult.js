glu.defModel('RS.worksheet.TaskResult', {

	mock: false,

	activate: function(screen, params) {
		this.resetForm()
		this.set('id', params ? params.id : '')
		if (params && Ext.isDefined(params.activeTab) && Ext.isNumber(Number(params.activeTab))) this.set('activeTab', Number(params.activeTab))
		this.loadTaskResult()
	},

	resetForm: function() {
		this.loadData(this.defaultData)
	},

	fields: ['id',
		'sysId',
		'actionTask',
		'actionTaskFullName',
		'actionTaskId',
		'taskName',
		'taskId',
		'completion',
		'condition',
		'severity',
		'duration',
		'address',
		'summary',
		'updatedBy',
		'targetGUID',
		'summary',
		'raw',
		'esbAddr',
		'executeRequestId',
		'executeRequestNumber',
		'executeResultId',
		'problemId',
		'problemNumber',
		'processId',
		'processNumber',
		'actionTaskName',
		'actionTaskFullName',
		'taskNamespace',
		'detail',
		'timestamp'
	].concat(RS.common.grid.getSysFields()),

	defaultData: {
		id: '',
		sysId: '',
		actionTask: '',
		actionTaskFullName: '',
		taskNamespace: '',
		actionTaskId: '',
		taskName: '',
		taskId: '',
		completion: '',
		condition: '',
		severity: '',
		duration: '',
		address: '',
		summary: '',
		targetGUID: '',
		summary: '',
		raw: '',
		esbAddr: '',
		executeRequestId: '',
		executeRequestNumber: '',
		executeResultId: '',
		problemId: '',
		problemNumber: '',
		processId: '',
		processNumber: '',
		actionTaskName: '',
		actionTaskFullName: '',
		detail: '',
		timestamp: '',

		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrg: ''
	},

	id: '',
	actionTask: '',
	actionTaskId: '',
	actionTaskFullName: '',
	taskNamespace: '',
	taskName: '',
	taskId: '',
	completion: '',
	condition: '',
	severity: '',
	duration: '',
	address: '',
	summary: '',
	summaryDisplay$: function() {
		return this.formatHtmlString(this.summary)
	},
	targetGUID: '',
	summary: '',
	raw: '',
	rawText$: function() {
		return this.formatHtmlString(this.raw)
	},
	esbAddr: '',
	executeRequestId: '',
	executeRequestNumber: '',
	executeResultId: '',
	problemId: '',
	problemNumber: '',
	processId: '',
	processNumber: '',
	actionTaskName: '',
	actionTaskFullName: '',
	detail: '',
	detailDisplay$: function() {
		return this.formatHtmlString(this.detail)
	},

	formatHtmlString: function(value) {
		if (value.indexOf('<!-- DETAIL_TYPE_HTML -->') == 0 || value.indexOf('<!-- UI_TYPE_HTML -->') == 0) {
			return Ext.util.Format.nl2br('<pre>' + clientVM.injectRsclientToken(value) + '</pre>')
		}
		return Ext.util.Format.nl2br('<pre>' + value + '</pre>')
	},
	timestamp: '',

	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrg: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	taskResultTitle$: function() {
		return this.localize('taskResultTitle') + (this.taskName ? ' - ' + this.taskName : '') + (this.taskNamespace ? '#' + this.taskNamespace : '')
	},

	init: function() {
		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)
	},

	loadTaskResult: function() {
		if (this.id)
			this.ajax({
				url: '/resolve/service/taskresult/get',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) this.loadData(response.data)
					else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
	},

	back: function() {
		clientVM.handleNavigationBack()
	},
	backIsVisible$: function() {
		return clientVM.screens.length > 1
	},

	activeTab: 0,

	generalTab: function() {
		this.set('activeTab', 0)
	},
	generalTabIsPressed$: function() {
		return this.activeTab == 0
	},
	detailTab: function() {
		this.set('activeTab', 1)
	},
	detailTabIsPressed$: function() {
		return this.activeTab == 1
	},
	rawTab: function() {
		this.set('activeTab', 2)
	},
	rawTabIsPressed$: function() {
		return this.activeTab == 2
	},

	jumpToWorksheet: function() {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.Worksheet',
			params: {
				id: this.problemId
			}
		})
	},
	jumpToWorksheetIsVisible$: function() {
		return this.problemId
	},

	jumpToActionTask: function() {
		clientVM.handleNavigation({
			modelName: 'RS.actiontask.ActionTask',
			params: {
				id: this.taskId
			}
		})
	},
	jumpToActionTaskIsVisible$: function() {
		return this.taskId
	},
	jumpToProcessRequest: function() {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.ProcessRequest',
			params: {
				id: this.processId
			}
		})
	},
	jumpToProcessRequestIsVisible$: function() {
		return this.processId
	},

	showGUIDDetails$: function() {
		return this.targetGUID
	},
	displayGUIDDetails: function() {
		this.open({
			mtype: 'RS.worksheet.ListRegistrationDetails',
			guid: this.targetGUID
		})
	}
})