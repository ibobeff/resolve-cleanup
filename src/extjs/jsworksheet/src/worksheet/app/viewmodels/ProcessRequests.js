glu.defModel('RS.worksheet.ProcessRequests', {
	mock: false,

	displayName: 'processRequests',
	stateId: 'processRequestList',

	activate: function() {
		//Load the processRequests from the server
		this.processRequests.load();
	},

	processRequests: {
		mtype: 'store',
		remoteSort: true,
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		fields: ['id', 'number', 'status', 'problemNumber', 'problem', 'wiki', 'duration', 'sysOrg'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/processrequest/list',
			reader: {
				type: 'json',
				root: 'records'
			}
		}
	},

	columns: [],

	allSelected: false,
	gridSelections: [],
	processRequestAbortAPI : '/resolve/service/processrequest/abort',
	processRequestDeleteAPI : '/resolve/service/processrequest/delete',

	init: function() {
		//Called when mocking the backend to test the UI
		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)

		this.set('columns', [{
			header: '~~processRequest~~',
			dataIndex: 'number',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.internalLinkRenderer('RS.worksheet.ProcessRequest', 'id', 'id')
		}, {
			header: '~~status~~',
			dataIndex: 'status',
			filterable: true,
			sortable: true
		}, {
			header: '~~problemNumber~~',
			dataIndex: 'problemNumber',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.internalLinkRenderer('RS.worksheet.Worksheet', 'id', 'problem')
		}, {
			header: '~~wiki~~',
			dataIndex: 'wiki',
			filterable: true,
			sortable: true
		}, {
			header: '~~duration~~',
			dataIndex: 'duration',
			filterable: true,
			sortable: true
		},{
			header: '~~organization~~',
			dataIndex: 'sysOrg',
			initialShow: true,
			sortable: false,
			renderer: function(value, metaData, record) {
				if (value) {
					if(!record.store.id2NameMap) {
						record.store.id2NameMap = {};
						clientVM.user.orgs.forEach(function(e) {
							if(e.uname != 'None')
								record.store.id2NameMap[e.id] = e.uname;
						});
					}
					return RS.common.grid.plainRenderer() (record.store.id2NameMap[value]);
				}
				return '';
			}
		}].concat(RS.common.grid.getSysColumns()))

		this.processRequests.on('beforeload', function(store, operation, eOpts) {
			this.addKeywordToSorterProp(operation.sorters);
		}, this)

		this.set('displayName', this.localize('processRequests'))
	},
	addKeywordToSorterProp: function(sorters) {
		for(var i=0; i<sorters.length; i++) {
			var curSorter = sorters[i];
			if (curSorter.property.indexOf('.keyword') == -1) {// iif '.keyword' has not already been appended
				switch (curSorter.property) {
				case 'number':
				case 'status':
				case 'problemNumber':
				case 'wiki':
					curSorter.property += '.keyword';
					break;
				default:
					// do nothing
					break;
				}
			}
		}
	},
	editProcessRequest: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.ProcessRequest',
			params: {
				id: id
			}
		})
	},
	editProcessRequestIsEnabled$: function() {
		return this.gridSelections.length == 1
	},
	findValueForFilter : function(filter){
		var regexPattern = new RegExp(filter + " = '([^']*)'");
		var matches = regexPattern.exec(this.processRequests.lastWhereClause);
		return matches ? matches[1] : null;
	},
	abortProcessRequest: function() {
		var title = this.localize('abortTitle');
		var msg = this.localize(this.gridSelections.length == 1 ? 'abortMessage' : 'abortsMessage', this.gridSelections.length);
		if(this.abortAll){
			title = this.localize('abortAllTitle');
			var status = this.findValueForFilter('status') || '';
			msg = this.localize('abortAllMessage', [this.processRequests.totalCount, status]);
		}
		this.message({
			title: title,
			msg : msg,
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('abortAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyAbortRecords
		})
	},
	abortProcessRequestIsEnabled$: function() {
		return this.gridSelections.length > 0 || this.allSelected
	},
	abortAll : false,
	abortAllProcessRequest : function(){
		this.set('abortAll', true);
		this.abortProcessRequest();
	},
	reallyAbortRecords: function(button) {
		if (button === 'yes') {
			//Abort the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			var status = this.findValueForFilter('status') || 'ALL'
			var ids = [];
			if (this.abortAll) {
				ids.push('All');
			}
			else{
				Ext.each(this.gridSelections, function(selection) {
					ids.push(selection.get('id'))
				})
			}
			this.ajax({
				url: this.processRequestAbortAPI,
				params: {
					ids: ids,
					status : status
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) this.processRequests.load()
					else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
		this.set('abortAll', false);
	},

	deleteProcessRequest: function() {
		//Delete the selected record(s), but first confirm the action
		var title = this.localize('deleteTitle');
		var msg = this.localize(this.gridSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.gridSelections.length);
		if(this.deleteAll){
			title = this.localize('deleteAllTitle');
			var status = this.findValueForFilter('status') || '';
			msg = this.localize('deleteAllMessage', [this.processRequests.totalCount, status]);
		}
		this.message({
			title: title,
			msg: msg,
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	deleteProcessRequestIsEnabled$: function() {
		return this.gridSelections.length > 0 || this.allSelected
	},
	deleteAll : false,
	deleteAllProcessRequest: function() {
		this.set('deleteAll', true);
		this.deleteProcessRequest();
	},
	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			var status = this.findValueForFilter('status') || 'ALL'
			var ids = [];
			if (this.deleteAll) {
				ids.push('All');
			}
			else{
				Ext.each(this.gridSelections, function(selection) {
					ids.push(selection.get('id'))
				})
			}
			this.ajax({
				url: this.processRequestDeleteAPI,
				params: {
					ids: ids.join(','),
					status : status
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) this.processRequests.load()
					else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
		this.set('deleteAll', false);
	}
})