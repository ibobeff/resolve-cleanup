glu.defModel('RS.worksheet.Worksheets', {
	API : {
		deleteWS : '/resolve/service/worksheet/delete',
		setActive : '/resolve/service/worksheet/setActive'
	},
	mock: false,

	activate: function() {
		clientVM.setWindowTitle(this.localize('allWorksheets'))
			// window.name = 'allworksheets'
		this.worksheets.load();
	},

	deactivate: function() {
		// window.name = ''
	},

	displayName: '~~allWorksheets~~',
	stateId: 'worksheetList',

	worksheets: {
		mtype: 'store',
		remoteSort: true,
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		fields: ['id', 'number', 'condition', 'severity', 'summary', 'assignedTo', 'assignedToName', 'sysId', 'reference', 'alertId', 'correlationId', 'sirId',{
			name: 'sysTtl',
			type: 'int'
		}, 'sysOrg'
		].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/worksheet/list',
			reader: {
				type: 'json',
				root: 'records'
			}
		}
	},

	columns: [],

	allSelected: false,
	worksheetsSelections: [],

	init: function() {
		var me = this;
		//Called when mocking the backend to test the UI
		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)

		var worksheetsColumns = [{
			header: '~~number~~',
			dataIndex: 'number',
			filterable: true,
			sortable: true
		}, {
			header: '~~condition~~',
			dataIndex: 'condition',
			filterable: true,
			sortable: true,
			width: 90,
			renderer: RS.common.grid.statusRenderer('rs-worksheet-status')
		}, {
			header: '~~severity~~',
			dataIndex: 'severity',
			filterable: true,
			sortable: true,
			width: 90,
			renderer: RS.common.grid.statusRenderer('rs-worksheet-status')
		}, {
			header: '~~summary~~',
			dataIndex: 'summary',
			filterable: true,
			sortable: true,
			flex: 1,
			//renderer: RS.common.grid.htmlRenderer()
		}, {
			header: '~~assignedToName~~',
			dataIndex: 'assignedToName',
			sortable: true,
			filterable: true
		}, {
			header: '~~assignedTo~~',
			dataIndex: 'assignedTo',
			sortable: true,
			filterable: true,
			hideable: false,
			hidden: true
		}, {
			header: '~~reference~~',
			dataIndex: 'reference',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~alertId~~',
			dataIndex: 'alertId',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~correlationId~~',
			dataIndex: 'correlationId',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.plainRenderer()
		},{
			header: '~~sirId~~',
			dataIndex: 'sirId',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.plainRenderer()
		},{
			header: '~~organization~~',
			dataIndex: 'sysOrg',
			initialShow: true,
			sortable: false,
			renderer: function(value, metaData, record) {
				if (value) {
					if(!record.store.id2NameMap) {
						record.store.id2NameMap = {};
						clientVM.user.orgs.forEach(function(e) {
							if(e.uname != 'None')
								record.store.id2NameMap[e.id] = e.uname;
						});
					}
					return RS.common.grid.plainRenderer() (record.store.id2NameMap[value]);
				}
				return '';
			}
		}].concat(RS.common.grid.getSysColumns());

		Ext.each(this.worksheetsColumns, function(col) {
			col.stateId = 'worksheetscolumn_' + col.dataIndex;
		}, this);


		//correct worksheet columns
		var updatedCol, index = -1;
		Ext.Array.each(worksheetsColumns, function(column, idx) {
			if (column.dataIndex == 'sysUpdatedOn') {
				updatedCol = column
				index = idx
			}
			if (column.dataIndex == 'sysCreatedOn') {
				column.hidden = false;
				column.initialShow = true;
				column.flex = 1;
			}
		})
		if (updatedCol && index > -1) {
			updatedCol.renderer = Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
			updatedCol.hidden = false
			updatedCol.initialShow = true
			worksheetsColumns.splice(index, 1)
			worksheetsColumns.splice(0, 0, updatedCol)
		}
		this.set('columns', worksheetsColumns)

		this.set('displayName', this.localize('allWorksheets'));
		this.worksheets.on('beforeload', function(store, operation){
			operation.params = Ext.apply(operation.params || {}, {
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			});
			var filter = operation.params.filter || [];
			filter.push({
				field: 'sysOrg',
				type: 'terms',
				condition: 'equals',
				value: clientVM.orgId || 'nil'
			});
			operation.params.filter = JSON.stringify(filter);
		});
		clientVM.on('orgchange', this.orgChange.bind(this));
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	creatingWorksheet: false,
	createWorksheet: function() {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.Worksheet'
		})
	},
	createWorksheetIsEnabled$: function() {
		return !this.creatingWorksheet
	},
	editWorksheet: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.Worksheet',
			params: {
				id: id,
				activeTab: 1
			}
		})
	},
	singleRecordSelected$: function() {
		return this.worksheetsSelections.length == 1
	},
	editWorksheetIsEnabled$: function() {
		return this.singleRecordSelected
	},
	deleteWorksheetIsEnabled$: function() {
		return this.worksheetsSelections.length > 0 || this.allSelected
	},
	deleteWorksheet: function() {
		//Delete the selected record(s), but first confirm the action
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.worksheetsSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.worksheetsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			if (this.allSelected) {
				this.ajax({
					url: this.APO['deleteWS'],
					params: {
						ids: '',
						whereClause: this.worksheets.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.worksheets.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.worksheetsSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: this.API['deleteWS'],
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.worksheets.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	},

	setActiveIsEnabled$: function() {
		return this.singleRecordSelected
	},
	setActive: function() {
		var sysTtl = this.worksheetsSelections[0].get('sysTtl');
		//server will inform if the worksheet is old or not.
		//if (sysTtl > 0 && this.worksheetsSelections[0].get('sysCreatedOn') + sysTtl < Ext.Date.format(new Date(), 'time'))
		//	clientVM.displayError(this.localize('oldWorksheet'), 10000)
		//else
		this.message({
			title: this.localize('setActiveTitle'),
			msg: this.localize('setActiveMessage', this.worksheetsSelections[0].get('number')),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('setActiveAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallySetActive
		})
	},
	reallySetActive: function(button) {
		if (button == 'yes') {
			this.ajax({
				url: this.API['setActive'],
				params: {
					id: this.worksheetsSelections[0].get('id'),
					'RESOLVE.ORG_NAME' : clientVM.orgName,
					'RESOLVE.ORG_ID' : clientVM.orgId
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						clientVM.updateProblemInfo(this.worksheetsSelections[0].get('id'), this.worksheetsSelections[0].get('number'))
						clientVM.displayMessage(null, '', {
							msg: this.localize('worksheetSetActiveSuccess', [this.worksheetsSelections[0].get('number'), this.worksheetsSelections[0].get('id')])
						});
						this.worksheets.load()
					} else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
	},

	orgChange: function() {
		this.worksheets.load();
	},

	beforeDestroyComponent : function() {
		clientVM.removeListener('orgchange', this.orgChange);
	}
})