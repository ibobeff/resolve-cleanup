glu.defModel('RS.worksheet.TaskRequests', {
	mock: false,

	displayName: 'taskRequests',
	stateId: 'taskRequestList',

	activate: function() {

	},

	taskRequests: {
		mtype: 'store',
		remoteSort: true,
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		fields: ['id', 'taskRequest', 'status', 'worksheet', 'processRequest', 'actionTask', 'duration'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/taskrequest/list',
			reader: {
				type: 'json',
				root: 'records'
			}
		}
	},

	columns: [],

	allSelected: false,
	gridSelections: [],

	init: function() {
		//Called when mocking the backend to test the UI
		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)

		this.set('columns', [{
			header: '~~taskRequest~~',
			dataIndex: 'taskRequest',
			filterable: true,
			sortable: true
		}, {
			header: '~~status~~',
			dataIndex: 'status',
			filterable: true,
			sortable: true
		}, {
			header: '~~worksheet~~',
			dataIndex: 'worksheet',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.internalLinkRenderer('RS.worksheet.Worksheet', 'id', 'worksheetId')
		}, {
			header: '~~processRequest~~',
			dataIndex: 'processRequest',
			filterable: true,
			sortable: true
		}, {
			header: '~~actionTask~~',
			dataIndex: 'actionTask',
			filterable: true,
			sortable: true
		}, {
			header: '~~duration~~',
			dataIndex: 'duration',
			filterable: true,
			sortable: true
		}].concat(RS.common.grid.getSysColumns()))

		this.set('displayName', this.localize('taskRequests'))

		//Load the taskRequests from the server
		this.taskRequests.load()
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	editTaskRequest: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.TaskRequest',
			params: {
				id: id
			}
		})
	},
	editTaskRequestIsEnabled$: function() {
		return this.gridSelections.length == 1
	},

	abortTaskRequest: function() {
		this.message({
			title: this.localize('abortTitle'),
			msg: this.localize(this.gridSelections.length == 1 ? 'abortMessage' : 'abortsMessage', this.gridSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('abortAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyAbortRecords
		})
	},
	abortTaskRequestIsEnabled$: function() {
		return this.gridSelections.length > 0 || this.allSelected
	},

	reallyAbortRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/taskrequest/abort',
					params: {
						ids: '',
						whereClause: this.taskRequests.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.taskRequests.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.gridSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/taskrequest/abort',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.taskRequests.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	},

	deleteTaskRequest: function() {
		//Delete the selected record(s), but first confirm the action
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.gridSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.gridSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	deleteTaskRequestIsEnabled$: function() {
		return this.gridSelections.length > 0 || this.allSelected
	},

	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/taskrequest/delete',
					params: {
						ids: '',
						whereClause: this.taskRequests.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.taskRequests.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.gridSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/taskrequest/delete',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.taskRequests.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	}
})