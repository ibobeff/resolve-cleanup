glu.defModel('RS.worksheet.ArchivedWorksheet', {
	mock: false,

	activate: function(screen, params) {
		this.initToken();
		clientVM.setWindowTitle(this.localize('archivedWorksheet'))
		this.set('activeTab', 1);
		this.set('all', Ext.state.Manager.get('RS.worksheet.showAllArchived'));
		if (params && Ext.isDefined(params.activeTab) && Ext.isNumber(Number(params.activeTab))) this.set('activeTab', Number(params.activeTab))
		this.resetForm()
		if (!clientVM.fromArchive && !(params || {}).id) {
			history.replaceState({}, '', '/resolve/jsp/rsclient.jsp?' + this.csrftoken + '#RS.worksheet.Worksheet')
			window.location.href = '/resolve/jsp/rsclient.jsp?' + this.csrftoken + '#RS.worksheet.Worksheet';
			return;
		}
		this.set('id', params ? params.id : clientVM.archivedProblemId)
		//if (params && params.setActive === 'true') this.reallySetActive('yes')
		this.loadWorksheet()
		if (params && params.number) this.set('number', params.number)
	},

	initToken: function() {
		this.set('csrftoken', clientVM.rsclientToken);
	},

	socialIsPressed: false,
	socialPressedCls$: function() {
		return this.socialIsPressed ? 'rs-social-button-pressed icon-comment' : 'icon-comment-alt'
	},

	when_socialIsPressed_changes_store_state: {
		on: ['socialIsPressedChanged'],
		action: function() {
			Ext.state.Manager.set('worksheetSocialIsPressed', this.socialIsPressed)
		}
	},

	worksheetTitle$: function() {
		return this.localize('archivedWorksheet') + (this.number ? ' - ' + this.number : '')
	},

	stateId: 'archivedWorksheetResultList',

	activeTab: 0,

	isResolveDev$: function() {
		return hasPermission(['resolve_dev', 'resolve_process'])
	},

	generalTab: function() {
		this.set('activeTab', 0)
	},
	generalTabIsPressed$: function() {
		return this.activeTab == 0
	},
	// generalTabIsVisible$: function() {
	// 	return this.isResolveDev
	// },
	resultsTab: function() {
		this.set('activeTab', 1)
	},
	resultsTabIsPressed$: function() {
		return this.activeTab == 1
	},
	// resultsTabIsVisible$: function() {
	// 	return this.isResolveDev
	// },
	debugTab: function() {
		this.set('activeTab', 2)
	},
	debugTabIsPressed$: function() {
		return this.activeTab == 2
	},
	// debugTabIsVisible$: function() {
	// 	return this.isResolveDev
	// },
	dataTab: function() {
		this.set('activeTab', 3)
	},
	dataTabIsPressed$: function() {
		return this.activeTab == 3
	},
	// dataTabIsVisible$: function() {
	// 	return this.isResolveDev
	// },

	displayFilter$: function() {
		return this.resultsTabIsPressed
	},

	resetForm: function() {
		this.loadData(this.defaultData)
	},


	fields: [
		'number',
		'alertId',
		'reference',
		'correlationId',
		'assignedTo',
		'assignedToName',
		'summary',
		'condition',
		'severity',
		'description',
		'workNotes',
		'debug'
	].concat(RS.common.grid.getSysFields()),
	id: '',
	streamType: 'worksheet',
	number: '',
	alertId: '',
	reference: '',
	correlationId: '',
	assignedTo: '',
	assignedToName: '',
	summary: '',
	condition: '',
	severity: '',
	description: '',
	workNotes: '',
	workNotesData: '',
	debug: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrg: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	defaultData: {
		id: '',
		number: '',
		alertId: '',
		reference: '',
		correlationId: '',
		assignedTo: '',
		assignedToName: '',
		summary: '',
		condition: '',
		severity: '',
		description: '',
		workNotes: '',
		workNotesData: '',
		debug: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrg: ''
	},

	worksheetResults: {
		mtype: 'store',
		remoteSort: true,
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'ASC'
		}],
		fields: ['id',
			'address',
			'completion',
			'condition',
			'esbaddr',
			'severity',
			'executeRequestId',
			'executeRequestNumber',
			'executeResultId',
			'problem',
			'process',
			'processNumber',
			'processId',
			'targetGUID',
			'actionTask',
			'actionTaskName',
			'actionTaskFullName',
			'actionResultId',
			'detail',
			'taskName',
			'taskId',
			'summary', 'sysId', {
				name: 'duration',
				type: 'int'
			}
		].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/worksheet/archiveResults',
			reader: {
				type: 'json',
				root: 'records'
			}
		}
	},

	worksheetColumns: [{
		text: '~~actionTask~~',
		dataIndex: 'taskName',
		// renderer: RS.common.grid.internalLinkRenderer('RS.actiontask.ActionTask', 'id', 'taskId'),
		renderer: function(value, meta, record) {
			if (value)
				return Ext.String.format('<a target="_blank" class="rs-link" href="/resolve/jsp/rsclient.jsp?{0}#RS.actiontask.ActionTask/id={1}">{2}</a>', this.csrftoken, record.get('taskId'), value)
			return ''
		},
		sortable: true,
		filterable: true
	}, {
		text: '~~completion~~',
		dataIndex: 'completion',
		width: 75,
		sortable: true,
		filterable: true
	}, {
		text: '~~condition~~',
		dataIndex: 'condition',
		renderer: RS.common.grid.statusRenderer('rs-worksheet-status'),
		width: 90,
		sortable: true,
		filterable: true
	}, {
		text: '~~severity~~',
		dataIndex: 'severity',
		renderer: RS.common.grid.statusRenderer('rs-worksheet-status'),
		width: 90,
		sortable: true,
		filterable: true
	}, {
		text: '~~summary~~',
		dataIndex: 'summary',
		flex: 1,
		sortable: false,
		filterable: false,
		renderer: function(value) {
			if (value.indexOf('<!-- DETAIL_TYPE_HTML -->') == 0 || value.indexOf('<!-- UI_TYPE_HTML -->') == 0) {
				return Ext.util.Format.nl2br('<pre>' + clientVM.injectRsclientToken(value) + '</pre>')
			}
			return Ext.util.Format.nl2br('<pre>' + Ext.String.htmlEncode(value) + '</pre>')
		}
	}, {
		text: '~~processRequest~~',
		dataIndex: 'processNumber',
		// renderer: RS.common.grid.internalLinkRenderer('RS.worksheet.ProcessRequest', 'id', 'processId'),
		sortable: true,
		filterable: true,
		hidden: true
	}, {
		text: '~~duration~~',
		dataIndex: 'duration',
		width: 60,
		sortable: true,
		filterable: true,
		hidden: true
	}, {
		text: '~~target~~',
		dataIndex: 'address',
		sortable: true,
		filterable: true,
		hidden: true
	}, {
		text: '~~guid~~',
		dataIndex: 'targetGUID',
		sortable: true,
		filterable: true,
		hidden: true
	}, {
		text: '~~detail~~',
		dataIndex: 'detail',
		hidden: true,
		hidable: false,
		sortable: true,
		filterable: true
	}, {
		text: '~~queueName~~',
		dataIndex: 'esbaddr',
		sortable: true,
		filterable: true,
		hidden: true
	}].concat(RS.common.grid.getSysColumns()),

	conditionStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	severityStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	workNotesDataStore: {
		mtype: 'store',
		fields: [{
				type: 'date',
				name: 'createDate',
				format: 'time',
				dateFormat: 'time'
			},
			'userid',
			'worknotesDetail'
		],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	workNotesColumns: [],

	csrftoken: '',

	init: function() {
		this.initToken();

		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)

		if (Ext.isString(this.activeTab)) this.set('activeTab', Number(this.activeTab))

		Ext.Array.forEach(this.worksheetColumns, function(col) {
			if (col.dataIndex == 'sys_id') col.dataIndex = 'sysId'
		})

		this.set('workNotesColumns', [{
			text: '~~sysCreatedOn~~',
			dataIndex: 'createDate',
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat()),
			width: 200
		}, {
			text: '~~sysCreatedBy~~',
			dataIndex: 'userid'
		}, {
			text: '~~note~~',
			dataIndex: 'worknotesDetail',
			flex: 1
		}])

		//correct worksheet columns
		var updatedCol, index = -1;
		Ext.Array.each(this.worksheetColumns, function(column, idx) {
			if (column.dataIndex == 'sysUpdatedOn') {
				updatedCol = column
				index = idx
				return false
			}
		})
		if (updatedCol && index > -1) {
			updatedCol.renderer = Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
			updatedCol.hidden = false
			updatedCol.initialShow = true
			this.worksheetColumns.splice(index, 1)
			this.worksheetColumns.splice(0, 0, updatedCol)
		}

		//Populate combobox stores
		this.conditionStore.add([{
			name: this.localize('good'),
			value: 'GOOD'
		}, {
			name: this.localize('bad'),
			value: 'BAD'
		}, {
			name: this.localize('unknown'),
			value: 'UNKNOWN'
		}])

		this.severityStore.add([{
			name: this.localize('critical'),
			value: 'CRITICAL'
		}, {
			name: this.localize('severe'),
			value: 'SEVERE'
		}, {
			name: this.localize('warning'),
			value: 'WARNING'
		}, {
			name: this.localize('good'),
			value: 'GOOD'
		}])
		this.worksheetResults.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {}
			operation.params.id = this.id;
			operation.params.hidden = !this.all;
		}, this)

		this.set('socialIsPressed', Ext.state.Manager.get('worksheetSocialIsPressed', false))
	},

	loadWorksheet: function() {
		this.workNotesDataStore.removeAll()

		if (this.id) {
			if (this.id == 'ACTIVE') {
				this.ajax({
					url: '/resolve/service/worksheet/getActive',
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.loadData(response.data)
							this.socialDetail.set('initialStreamId', response.data.id)
							this.socialDetail.set('streamParent', response.data.number)
							this.set('workNotesData', this.workNotes)
							this.set('workNotes', '')
							var records = this.workNotesData ? Ext.decode(this.workNotesData) : [] || [];
							this.workNotesDataStore.add(records)

							this.worksheetResults.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				this.worksheetResults.load()

				this.ajax({
					url: '/resolve/service/worksheet/getArchive',
					params: {
						id: this.id
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.loadData(response.data)
							this.socialDetail.set('initialStreamId', response.data.id)
							this.socialDetail.set('streamParent', response.data.number)
							this.set('workNotesData', this.workNotes)
							this.set('workNotes', '')
							var records = this.workNotesData ? Ext.decode(this.workNotesData) : [] || [];
							this.workNotesDataStore.add(records)
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		} else {
			this.ajax({
				url: '/resolve/service/worksheet/getNextNumber',
				method: 'POST',
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success)
						this.set('number', response.data)
					else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})

		}
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.ArchivedWorksheets'
		})
	},
	refresh: function() {
		this.loadWorksheet()
	},

	/* setActiveIsVisible$: function() {
		return this.id
	},
	setActive: function() {
		this.message({
			title: this.localize('setActiveTitle'),
			msg: this.localize('setActiveMessage', this.number),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('setActiveAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallySetActive
		})
	},

	reallySetActive: function(button) {
		if (button == 'yes') {
			clientVM.updateProblemInfo(this.id, this.number, true)
			clientVM.displayMessage(null, '', {
				msg: this.localize('archivedWorksheetSetActiveSuccess', [this.number, this.id])
			});
			this.loadWorksheet()
		}
	}, */

	editResult: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.ArchivedTaskResult',
			params: {
				id: id,
				activeTab: 1
			}
		})
	},

	jumpToAssignedTo: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.user.User',
			params: {
				username: this.assignedTo
			}
		})
	},
	jumpToAssignedToIsVisible$: function() {
		return this.assignedToName
	},

	assignedToClicked: function() {
		this.open({
			mtype: 'RS.user.UserPicker',
			pick: true,
			callback: this.assignedToSelected
		})
	},

	assignedToClickedClear: function() {
		this.set('assignedTo', '')
		this.set('assignedToName', '')
	},

	assignedToControl: null,
	registerAssignedTo: function(assignedToControl) {
		this.set('assignedToControl', assignedToControl)
		this.assignedToControl.triggerEl.item(1).parent().setDisplayed((this.assignedToName || this.assignedTo) ? 'table-cell' : 'none')
	},

	when_assignedTo_changes_show_or_hide_clear_box: {
		on: ['assignedToChanged', 'assignedToNameChanged'],
		action: function() {
			if (this.assignedToControl) {
				this.assignedToControl.triggerEl.item(1).parent().setDisplayed((this.assignedToName || this.assignedTo) ? 'table-cell' : 'none')
				this.assignedToControl.doComponentLayout()
			}
		}
	},

	assignedToSelected: function(records) {
		var record = records[0]
		this.set('assignedTo', record.get('uuserName'))
		this.set('assignedToName', record.get('udisplayName'))
	},

	all: true,

	showAll: function() {
		Ext.state.Manager.set('RS.worksheet.showAllArchived', this.all);
		this.worksheetResults.load();
	},

	hide: function() {
		this.set('all', false);
		Ext.state.Manager.set('RS.worksheet.showAllArchived', this.all);
		this.worksheetResults.load();
	},

	//Child Viewmodels
	socialDetail: {
		mtype: 'RS.social.Main',
		mode: 'embed',
		initialStreamType: 'worksheet'
	}

});