glu.defModel('RS.worksheet.ArchivedWorksheets', {
	mock: false,

	activate: function() {
		clientVM.setWindowTitle(this.localize('allArchivedWorksheets'))
		this.worksheets.load();
	},

	deactivate: function() {
		// window.name = ''
	},

	displayName: '~~allArchivedWorksheets~~',
	stateId: 'rsarchivedworksheetlist',

	worksheets: {
		mtype: 'store',
		remoteSort: true,
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		fields: [
			'id',
			'number',
			'condition',
			'severity',
			'summary',
			'assignedTo',
			'assignedToName',
			'reference',
			'alertId',
			'correlationId',
			'sirId',
			'sysId', {
				name: 'sysTtl',
				type: 'int'
			}, 'sysOrg'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/worksheet/listArchive',
			reader: {
				type: 'json',
				root: 'records'
			}
		}
	},

	columns: [],

	allSelected: false,
	worksheetsSelections: [],

	init: function() {
		//Called when mocking the backend to test the UI
		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)

		var worksheetsColumns = [{
			header: '~~number~~',
			dataIndex: 'number',
			filterable: true,
			sortable: true
		}, {
			header: '~~condition~~',
			dataIndex: 'condition',
			filterable: true,
			sortable: true,
			width: 90,
			renderer: RS.common.grid.statusRenderer('rs-worksheet-status')
		}, {
			header: '~~severity~~',
			dataIndex: 'severity',
			filterable: true,
			sortable: true,
			width: 90,
			renderer: RS.common.grid.statusRenderer('rs-worksheet-status')
		}, {
			header: '~~summary~~',
			dataIndex: 'summary',
			filterable: true,
			sortable: false,
			flex: 1,
			renderer: RS.common.grid.htmlRenderer()
		}, {
			header: '~~assignedTo~~',
			dataIndex: 'assignedTo',
			filterable: true,
			sortable: true
		}, {
			header: '~~assignedToName~~',
			dataIndex: 'assignedToName',
			filterable: true,
			sortable: true
		}, {
			header: '~~reference~~',
			dataIndex: 'reference',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~alertId~~',
			dataIndex: 'alertId',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~correlationId~~',
			dataIndex: 'correlationId',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.plainRenderer()
		},{
			header: '~~sirId~~',
			dataIndex: 'sirId',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.plainRenderer()
		},{
			header: '~~organization~~',
			dataIndex: 'sysOrg',
			initialShow: true,
			sortable: true,
			renderer: function(value, metaData, record) {
				if (value) {
					if(!record.store.id2NameMap) {
						record.store.id2NameMap = {};
						clientVM.user.orgs.forEach(function(e) {
							record.store.id2NameMap[e.id] = e.uname;
						});
					}
					return RS.common.grid.plainRenderer()(record.store.id2NameMap[value]);
				}
				return '';
			}
		}].concat(RS.common.grid.getSysColumns());

		Ext.each(this.worksheetsColumns, function(col) {
			col.stateId = 'worksheetscolumn_' + col.dataIndex;
		}, this);


		//correct worksheet columns
		var updatedCol, index = -1;
		Ext.Array.each(worksheetsColumns, function(column, idx) {
			if (column.dataIndex == 'sysUpdatedOn') {
				updatedCol = column
				index = idx
			}
		})
		if (updatedCol && index > -1) {
			updatedCol.renderer = Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
			updatedCol.hidden = false
			updatedCol.initialShow = true
			worksheetsColumns.splice(index, 1)
			worksheetsColumns.splice(0, 0, updatedCol)
		}
		this.worksheets.on('beforeload', function(store, operation){
			operation.params = Ext.apply(operation.params || {}, {
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			});
		})
		this.set('columns', worksheetsColumns)

		this.set('displayName', this.localize('allArchivedWorksheets'))
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	editWorksheet: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.ArchivedWorksheet',
			params: {
				id: id,
				activeTab: 1
			}
		})
	},
	singleRecordSelected$: function() {
		return this.worksheetsSelections.length == 1
	},
	editWorksheetIsEnabled$: function() {
		return this.singleRecordSelected
	},
});