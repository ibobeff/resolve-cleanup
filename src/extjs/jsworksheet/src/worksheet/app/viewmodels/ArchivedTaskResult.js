glu.defModel('RS.worksheet.ArchivedTaskResult', {
	mixins: ['TaskResult'],
	fields: ['id',
		'sysId',
		'actionTask',
		'actionTaskFullName',
		'actionTaskId',
		'taskName',
		'taskId',
		'completion',
		'condition',
		'severity',
		'duration',
		'address',
		'summary',
		'updatedBy',
		'targetGUID',
		'summary',
		'raw',
		'esbAddr',
		'executeRequestId',
		'executeRequestNumber',
		'executeResultId',
		'problemId',
		'problemNumber',
		'processId',
		'processNumber',
		'actionTaskName',
		'actionTaskFullName',
		'taskNamespace',
		'detail',
		'timestamp'
	].concat(RS.common.grid.getSysFields()),
	loadTaskResult: function() {
		if (this.id)
			this.ajax({
				url: '/resolve/service/taskresult/getArchiveTaskResult',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) this.loadData(response.data)
					else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
	}
})