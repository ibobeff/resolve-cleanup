glu.defModel('RS.worksheet.TaskResults', {
	mock: false,

	displayName: 'taskResults',
	stateId: 'taskResultList',

	activate: function() {},

	taskResults: {
		mtype: 'store',
		remoteSort: true,
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		fields: ['id', 'sysId', 'taskName', 'taskId', 'completion', 'condition', 'severity', 'duration', 'problemNumber', 'problemId', 'processNumber', 'processId', 'summary', 'sysOrg'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/taskresult/list',
			reader: {
				type: 'json',
				root: 'records'
			}
		}
	},

	columns: [],

	allSelected: false,
	gridSelections: [],

	init: function() {
		//Called when mocking the backend to test the UI
		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)

		var taskResultsColumns = [{
			header: '~~actionTask~~',
			dataIndex: 'taskName',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.internalLinkRenderer('RS.actiontask.ActionTask', 'id', 'taskId')
		}, {
			header: '~~completion~~',
			dataIndex: 'completion',
			filterable: true,
			width: 90,
			sortable: true
		}, {
			header: '~~condition~~',
			dataIndex: 'condition',
			filterable: true,
			sortable: true,
			width: 90,
			renderer: RS.common.grid.statusRenderer('rs-worksheet-status')
		}, {
			header: '~~severity~~',
			dataIndex: 'severity',
			filterable: true,
			sortable: true,
			width: 90,
			renderer: RS.common.grid.statusRenderer('rs-worksheet-status')
		}, {
			header: '~~duration~~',
			dataIndex: 'duration',
			filterable: true,
			width: 90,
			sortable: true
		}, {
			header: '~~worksheet~~',
			dataIndex: 'problemNumber',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.internalLinkRenderer('RS.worksheet.Worksheet', 'id', 'problemId')
		}, {
			header: '~~processRequest~~',
			dataIndex: 'processNumber',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.internalLinkRenderer('RS.worksheet.ProcessRequest', 'id', 'processId')
		}, {
			header: '~~summary~~',
			dataIndex: 'summary',
			filterable: false,
			sortable: false,
			flex: 1,
			renderer: function(value) {
				if (value.indexOf('<!-- DETAIL_TYPE_HTML -->') == 0 || value.indexOf('<!-- UI_TYPE_HTML -->') == 0) {
					return Ext.util.Format.nl2br('<pre>' + clientVM.injectRsclientToken(value) + '</pre>')
				}
				return Ext.util.Format.nl2br('<pre>' + Ext.String.htmlEncode(value) + '</pre>')
			}
		},{
			header: '~~organization~~',
			dataIndex: 'sysOrg',
			initialShow: true,
			sortable: false,
			renderer: function(value, metaData, record) {
				if (value) {
					if(!record.store.id2NameMap) {
						record.store.id2NameMap = {};
						clientVM.user.orgs.forEach(function(e) {
							if(e.uname != 'None')
								record.store.id2NameMap[e.id] = e.uname;
						});
					}
					return RS.common.grid.plainRenderer() (record.store.id2NameMap[value]);
				}
				return '';
			}
		}].concat(RS.common.grid.getSysColumns())

		//correct worksheet columns
		var updatedCol, index = -1;
		Ext.Array.each(taskResultsColumns, function(column, idx) {
			if (column.dataIndex == 'sysUpdatedOn') {
				updatedCol = column
				index = idx
			}
		})
		if (updatedCol && index > -1) {
			updatedCol.renderer = Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
			updatedCol.hidden = false
			updatedCol.initialShow = true
			taskResultsColumns.splice(index, 1)
			taskResultsColumns.splice(0, 0, updatedCol)
		}
		this.set('columns', taskResultsColumns)

		this.set('displayName', this.localize('taskResults'))

		this.taskResults.on('beforeload', function(store, operation, eOpts) {
			this.addRawToSorterProp(operation.sorters);
		}, this)

		//Load the taskResults from the server
		this.taskResults.load()
	},

	addRawToSorterProp: function(sorters) {
		// This method is needed after upgrading ES to 5.x. After migration, 'text' field was changed to raw 'keyword'
		// so we need to apply '.raw' to fields that were changed to raw 'keyword'.
		// FYI, sorting on 'GUID' isn't working because the field has been properly migrated. We need to fix sorting
		// on 'GUID' accordingly after the BE correct the migration.
		for(var i=0; i<sorters.length; i++) {
			var curSorter = sorters[i];
			if (curSorter.property.indexOf('.raw') == -1) {// iif '.raw' has not already been appended
				switch (curSorter.property) {
				case 'taskName':
				case 'duration':
				case 'targetGUID':
				case 'sysCreatedBy':
				case 'sysCreatedOn':
				case 'sysId':
				case 'sysUpdatedOn':
				case 'sysUpdatedBy':
				case 'detail':
					// do nothing
					break;
				default:
					curSorter.property += '.raw';
					break;
				}
			}
		}
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	editTaskResult: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.TaskResult',
			params: {
				id: id
			}
		})
	},
	editTaskResultIsEnabled$: function() {
		return this.gridSelections.length == 1
	},

	deleteTaskResult: function() {
		//Delete the selected record(s), but first confirm the action
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.gridSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.gridSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	deleteTaskResultIsEnabled$: function() {
		return this.gridSelections.length > 0 || this.allSelected
	},

	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/taskresult/delete',
					params: {
						ids: '',
						whereClause: this.taskResults.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.taskResults.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.gridSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/taskresult/delete',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.taskResults.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	}
})