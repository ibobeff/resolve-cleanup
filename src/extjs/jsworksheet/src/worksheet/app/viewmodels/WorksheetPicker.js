glu.defModel('RS.worksheet.WorksheetPicker', {
	API : {
		setActive : '/resolve/service/worksheet/setActive',
		newWS : '/resolve/service/worksheet/newWorksheet',
		getSystemProperty : '/resolve/service/sysproperties/getSystemPropertyByName',
		getArchive : '/resolve/service/worksheet/getArchive'
	},
	showPicker: true,

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	when_showPicker_changes_update_worksheet_list: {
		on: ['showPickerChanged'],
		action: function() {
			if (this.showPicker)
				this.worksheets.loadPage(1)
		}
	},

	worksheets: {
		mtype: 'store',
		remoteSort: true,
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		fields: ['id', 'sysId', 'number', 'reference', 'correlationId', 'alertId', 'wsData_dt_dtList',{
			name : 'organization',
			mapping : 'sysOrg',
			convert : function(v, record){
				var orgs = clientVM.userOrganization || [];
				for(var i = 0; i < orgs.length; i++){
					if(v && orgs[i].id == v)
						return orgs[i].uname;
				}
				return "";
			}
		}].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/worksheet/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	worksheetsSelections: [],
	worksheetsColumns: [],

	myWorksheetsIsPressed: false,
	myWorksheets: function() {
		this.set('myWorksheetsIsPressed', !this.myWorksheetsIsPressed)
	},
	myWorksheetsPressedCls$: function() {
		return this.myWorksheetsIsPressed ? 'rs-social-button-pressed' : ''
	},
	myWorksheetsTooltip$: function() {
		return this.myWorksheetsIsPressed ? this.localize('showAllWorksheets') : this.localize('showMyWorksheets')
	},

	searchText: '',
	when_searchText_or_my_worksheets_changes_update_worksheet_grid: {
		on: ['searchTextChanged', 'myWorksheetsIsPressedChanged'],
		action: function() {
			if (!this.openedByWinParam || this.worksheetTabIsPressed)
				this.worksheets.load()
				// if (this.openedByWinParam && this.archivedWorksheetTabIsPressed)
				// 	this.archivedWorksheets.load()
		}
	},

	target: null,

	clientDialog: false,
	afterWorksheetSelected: function(isNewWorksheet) {},
	baseCls$: function() {
		return this.clientDialog && !Ext.isIE8m ? 'x-panel' : 'x-window'
	},
	ui$: function() {
		return this.clientDialog && !Ext.isIE8m ? 'sysinfo-dialog' : 'default'
	},
	prepareFiltersParams: function() {
		var availableFilters = [{
			field: 'alertId',
			type: 'auto',
			condition: 'contains',
			value: this.alertId || this.searchText
		}, {
			field: 'correlationId',
			type: 'auto',
			condition: 'contains',
			value: this.correlationId || this.searchText
		}, {
			field: 'reference',
			type: 'auto',
			condition: 'contains',
			value: this.reference || this.searchText
		}, {
			field: 'sysId',
			type: 'auto',
			condition: 'contains',
			value: this.sysId || this.problemId || this.searchText
		}];
		if (!(this.problemId || this.reference || this.correlationId || this.alertId))
			availableFilters.push({
				field: 'number',
				type: 'auto',
				condition: 'contains',
				value: this.searchText
			})
		var filters = [];
		var urlFilter = [];
		Ext.each(availableFilters, function(availableFilter) {
			if (this[availableFilter.field == 'sysId' ? 'problemId' : availableFilter.field]) {
				availableFilter.condition = 'equals';
				urlFilter.push(availableFilter);
			}
		}, this);

		Ext.each(urlFilter, function(availableFilter) {
			filters.push(availableFilter);
		}, this);

		if (this.myWorksheetsIsPressed) {
			filters.push({
				field: 'assignedTo',
				type: 'auto',
				condition: 'equals',
				value: this.parentVM.user.name
			})
		}

		if (this.searchText) {
			filters.push({
				field: 'number,alertId,correlationId,reference,sysId',
				type: 'string',
				condition: 'fulltext',
				value: this.searchText
			})
		}

		var params = {
			filter: Ext.encode(filters)
		}

		return params;
	},

	init: function() {
		this.set('tabTitle', this.localize('esTitle'));
		this.set('worksheetsColumns', [{
			dataIndex: 'number',
			text: '~~number~~',
			filterable: false,
			sortable: true,
			flex: 2
		}, {
			dataIndex: 'reference',
			text: '~~lookupId~~',
			filterable: false,
			flex: 3,
			renderer: function(value, metaData, record) {
				return Ext.String.htmlEncode(value) || record.get('correlationId') || record.get('alertId') || ''
			}
		}, {
			dataIndex: 'organization',
			text: '~~organization~~',
			filterable: false,
			sortable: false,
			width: 170				
		},{
			text: RS.common.locale.sysUpdated,
			dataIndex: 'sysUpdatedOn',
			filterable: false,
			sortable: true,
			width: 170,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}])


		this.worksheets.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {}
			Ext.apply(operation.params, this.prepareFiltersParams());
		}, this)

		// this.archivedWorksheets.on('beforeload', function(store, operation, eOpts) {
		// 	operation.params = operation.params || {}
		// 	var filters = ;
		// 	Ext.apply(operation.params, this.prepareFiltersParams(function(f) {
		// 		return f != 'sysId'
		// 	}));
		// }, this)

		//this should be refactored...
		//coz more and more dependencies were introduced in to the worksheet / client ui...
		//the orginal asnyc/parallel ajax req should be changed to sequential..
		//or if possible,just move some logic to the backend
		//use cases:
		//if there is a problemid or similar url params... it will fire a req to fetch the ws
		//if only one result comes back..select it and don't show the picker
		//if multiple results come back and there r some exact matches.. only show those exact match
		//if multiple results come back and there r no exact match..show all results
		//if no es is found, check the system properties too see whether need to fetch archived ws by id,
		//	if true, fetch the archived id..if no archived id is found..show picker with the urlparam value in the search textfield
		//	if false, show picker with the urlparam value in the search textfield
		this.worksheets.on('load', function(store, records, success) {
			Ext.Array.forEach(records || [], function(record) {
				record.data.wsData_dt_dtList = []
			})
		}, this)

		if (this.problemId || this.reference || this.correlationId || this.alertId) {
			this.worksheets.on('load', function(store, records, success) {
				if (!success) {
					var raw = store.getProxy().getReader().rawData;
					//clientVM.displayError(this.localize('listWorksheetError', raw.message));
					return;
				}
				var match = [];
				if (records.length == 1)
					match = records;
				else {
					Ext.each(records, function(r) {
						var hit = false;
						Ext.each(['problemId', 'reference', 'correlationId', 'alertId'], function(field) {
							if (this[field])
								hit = hit && (r.get(field) == this[field])
						}, this);
						if (hit)
							match.push(r);
					}, this);
				}
				if (match.length == 1) {
					//Auto select the only record that matched the search results that were passed into the picker from the client
					this.set('worksheetsSelections', match)
					var me = this;
					this.select(function() {
						clientVM.fireEvent('problemIdFetchedFromUrl');
						this.afterWorksheetSelected();
					});
					this.doClose();
				} else {
					if (this.openedByWinParam)
						return;
					this.set('openedByWinParam', true);
					if (records.length == 0) {
						this.ajax({
							url: this.API['getSystemProperty'],
							params: {
								name: 'resultmacro.check.archive'
							},
							success: function(resp) {
								var respData = RS.common.parsePayload(resp);
								if (!respData.success) {
									//clientVM.displayError(this.localize('checkArchiveFlagErr', respData.message));
									clientVM.displayError(respData.message);
									return;
								}
								if (respData.data.uvalue == true || respData.data.uvalue == "true")
									this.getArchiveWS(this.problemId);
								else if (this.autoCreate)
									this.newWorksheet({
										reference: this.reference,
										alertId: this.alertId,
										correlationId: this.correlationId
									});
								else {
									clientVM.open(this);
									this.set('searchText', this.problemId || this.reference || this.correlationId || this.alertId)
									this.set('problemId', '');
									this.set('reference', '');
									this.set('correlationId', '');
									this.set('alertId', '');
								}
							},
							failure: function(resp) {
								clientVM.displayFailure(resp);
							}
						})
					} else {
						clientVM.open(this);
						this.set('searchText', this.problemId || this.reference || this.correlationId || this.alertId)
						this.set('problemId', '');
						this.set('reference', '');
						this.set('correlationId', '');
						this.set('alertId', '');
					}
				}

			}, this, {
				single: true
			})
		}
		this.worksheets.on('beforeload', function(store, operation){
			operation.params = Ext.apply(operation.params || {}, {
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			});
		})
		this.worksheets.load();
	},
	getArchiveWS: function(id) {
		this.ajax({
			url: this.API['getArchive'],
			params: {
				id: id
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success || !respData.data) {
					if (this.autoCreate)
						this.newWorksheet();
					else
						clientVM.displayError(this.localize('archivedWorksheetsNotFound'), respData.message);
					return
				}
				var id = respData.data.id;
				var number = respData.data.number;
				clientVM.displaySuccess(this.localize('archivedWorksheetSelectSuccess', [number, id]));
				clientVM.updateProblemInfo(id, number);
				this.doClose();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	openedByWinParam: false,
	beforeClose: function() {
		this.set('showPicker', false);
		this.set('openedByWinParam', false);
	},
	// archivedWorksheets: {
	// 	mtype: 'store',
	// 	fields: ['id', 'sysId', 'number', 'reference', 'correlationId', 'alertId', 'wsData_dt_dtList'].concat(RS.common.grid.getSysFields()),
	// 	sorters: [{
	// 		property: 'sysUpdatedOn',
	// 		direction: 'DESC'
	// 	}],
	// 	proxy: {
	// 		type: 'ajax',
	// 		url: '/resolve/service/worksheet/listArchive',
	// 		reader: {
	// 			type: 'json',
	// 			root: 'records'
	// 		}
	// 	}
	// },
	tabTitle: '',
	activeItem: 0,
	worksheetTab: function() {
		this.set('tabTitle', this.localize('esTitle'));
		this.set('activeItem', 0);
		this.worksheets.load();
	},
	worksheetTabIsPressed$: function() {
		return this.activeItem == 0;
	},
	// archivedWorksheetTab: function() {
	// 	this.set('tabTitle', this.localize('archivedTitle'));
	// 	this.set('activeItem', 1);
	// 	this.archivedWorksheets.load();
	// },
	// archivedWorksheetTabIsPressed$: function() {
	// 	return this.activeItem == 1;
	// },
	dblclick: function(record, item, index, e, eOpts) {
		this.set('worksheetsSelections', [record])
		this.select()
	},

	go: function() {
		var id = this.worksheetsSelections[0].get('sysId'),
			number = this.worksheetsSelections[0].get('number');

		this.ajax({
			url: '/resolve/service/wsdata/getMap',
			params: {
				problemId: id,
				keyList: ['dt_dtList']
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					var data = Ext.decode(response.data['dt_dtList']) || [],
						record = null;

					Ext.Array.forEach(data, function(record) {
						record.id = Ext.id(),
							record.problemId = id
					})

					//sort by date
					data.sort(function(a, b) {
						if (a.date < b.date) return -1
						if (b.date < a.date) return 1
						return 0
					})

					if (data.length > 0) {
						record = data[data.length - 1]
					}

					this.setActive(id, number, function() {
						if (record) {
							clientVM.handleNavigation({
								modelName: record.type == 'dtviewer' ? 'RS.decisiontree.Main' : 'RS.wiki.Main',
								params: {
									name: record.name,
									problemId: id
								}
							})
						}
					})
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	goIsVisible$: function() {
		return this.activeItem == 0;
	},
	goIsEnabled$: function() {
		return this.worksheetsSelections.length == 1
	},
	select: function(callback) {
		if (!Ext.isFunction(callback)){
			if (clientVM.windowParams.rid) {
				callback = function () {
					clientVM.fireEvent('problemIdFetchedFromUrl');
					this.afterWorksheetSelected();
				}
			} else {
				callback = null;
			}
		}
		var id = this.worksheetsSelections[0].get('sysId'),
			number = this.worksheetsSelections[0].get('number');
		this.setActive(id, number, callback)
	},

	selectIsEnabled$: function() {
		return this.worksheetsSelections.length == 1
	},

	setActive: function(id, number, callback, args) {
		var myArguments = arguments;
		this.ajax({
			url: this.API['setActive'],
			params: {
				id: id,
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displayMessage(null, '', {
						msg: (number ? this.localize('worksheetSetActiveSuccess', [number, id]) : this.localize('newWorksheetActiveSuccess', id))
					});
					clientVM.updateProblemInfo(id, number)
					if (callback) {
						callback.apply(this, Array.prototype.slice.call(myArguments, 3))
					}
					clientVM.fireEvent('problemIdFetchedFromUrl');
					this.set('autoSelected', true)
					this.close()
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	view: function() {
		var id = this.parentVM.problemId || 'ACTIVE'
		if (this.worksheetsSelections.length > 0) {
			id = this.worksheetsSelections[0].get('sysId')
		}
		if (clientVM.fromArchive)
			this.parentVM.handleNavigation({
				modelName: 'RS.worksheet.ArchivedWorksheet',
				target: '_blank',
				params: {
					id: id
				}
			})
		else
			this.parentVM.handleNavigation({
				modelName: 'RS.worksheet.Worksheet',
				target: '_blank',
				params: {
					id: id
				}
			})

		this.close()
	},
	viewAll: function() {
		if (this.activeItem == 0)
			this.parentVM.handleNavigation({
				modelName: 'RS.worksheet.Worksheets',
				target: '_blank'
			})
		else
			this.parentVM.handleNavigation({
				modelName: 'RS.worksheet.ArchivedWorksheets',
				target: '_blank'
			})
		this.close()
	},
	newWorksheet: function() {
		this.ajax({
			url: this.API['newWS'],
			method: 'POST',
			params: {
				reference: this.reference,
				alertId: this.alertId,
				correlationId: this.correlationId,
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.setActive(response.data, undefined, this.afterWorksheetSelected, true)
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	newWorksheetIsVisible$: function() {
		return this.activeItem == 0;
	},
	autoCreate: false,
	autoSelected: false,
	close: function() {
		if (!this.clientDialog && !this.autoSelected) {
			var newestWorksheet = this.worksheets.getCount() > 0 ? this.worksheets.getAt(0) : null;
			this.worksheets.each(function(record) {
				if (record.get('number') == this.problemId) {
					this.set('worksheetsSelections', [record])
					return false
				}
				if (record.get('sysUpdatedOn') > newestWorksheet.get('sysUpdatedOn')) newestWorksheet = record
			}, this)
			if (newestWorksheet && this.worksheetsSelections.length == 0) this.set('worksheetsSelections', [newestWorksheet])

			if (this.worksheetsSelections.length > 0) {
				this.set('autoSelected', true)
				this.select()
				return
			}

			clientVM.displayError(this.localize('noWorksheetSelected'))
		}

		this.set('showPicker', false)

		// if (this.parentVM.prbWin) this.parentVM.prbWin = null
		// this.doClose()
	},
	closeIsVisible$: function() {
		return this.clientDialog
	},

	cancel: function() {
		this.set('showPicker', false)
	},
	cancelIsVisible$: function() {
		return !this.clientDialog
	},

	goIsVisible$: function() {
		return this.clientDialog
	},
	expandBody: function(rowNode, record, expandRow, eOpts) {
		var problemId = record.get('sysId')
			//go get the data for the worksheet
		this.ajax({
			url: '/resolve/service/wsdata/getMap',
			params: {
				problemId: problemId,
				keyList: ['dt_dtList']
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					var data = Ext.decode(response.data['dt_dtList']) || [],
						dateFormat = this.parentVM.getUserDefaultDateFormat(),
						tpl = new Ext.XTemplate(
							'<table style="width:100%">',
							'<tpl for=".">',
							'<tr>',
							'<td>', this.localize('name'), ': {name}</td>',
							'<td>', RS.common.locale.sysUpdatedOn, ': {date:this.formatDate}</td>',
							'<td><span id="{id}"></span></td>',
							'</tr>',
							'</tpl>',
							'</table>', {
								formatDate: function(value) {
									return Ext.Date.format(Ext.Date.parse(value, 'time'), dateFormat) || ''
								}
							}
						),
						replaceNode = Ext.fly(rowNode).query('span[name=replace]')[0];

					Ext.Array.forEach(data, function(record) {
						record.id = Ext.id(),
							record.problemId = problemId
					})

					//sort by date
					data.sort(function(a, b) {
						if (a.date < b.date) return -1
						if (b.date < a.date) return 1
						return 0
					})

					tpl.overwrite(replaceNode, data)

					//render the buttons
					Ext.Array.forEach(data, function(record) {
						Ext.create('Ext.button.Button', {
							text: this.localize('continueWorksheet'),
							iconCls: 'icon-large icon-play-sign rs-worksheet-continue',
							cls : 'rs-small-btn rs-btn-dark',
							renderTo: record.id,
							record: record,
							scope: this,
							handler: record.type == 'dtviewer' ? this.continueDecisionTree : this.continueWiki
						})
					}, this)
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	continueDecisionTree: function(button) {
		//select the worksheet for this dt
		var record = this.worksheets.getAt(this.worksheets.findExact('sysId', button.record.problemId))
		this.set('autoSelected', true)
		this.setActive(record.get('sysId'), record.get('number'), function(r) {
			clientVM.handleNavigation({
				modelName: 'RS.decisiontree.Main',
				params: {
					name: button.record.name,
					problemId: button.record.problemId
				}
			})
		})
	},

	continueWiki: function(button) {
		//select the worksheet for this dt
		var record = this.worksheets.getAt(this.worksheets.findExact('sysId', button.record.problemId))
		this.set('autoSelected', true)
		this.setActive(record.get('sysId'), record.get('number'), function(r) {
			clientVM.handleNavigation({
				modelName: 'RS.wiki.Main',
				params: {
					name: button.record.name
				}
			})
		})
	}
})