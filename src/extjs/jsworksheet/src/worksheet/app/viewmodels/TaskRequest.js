glu.defModel('RS.worksheet.TaskRequest', {

	activate: function(screen, params) {
		this.initToken();
		this.resetForm()
		this.set('id', params ? params.id : '')
		this.loadTaskRequest()
	},

	initToken: function() {
		this.set('csrftoken', clientVM.rsclientToken);
	},

	resetForm: function() {
		this.loadData(this.defaultData)
	},

	fields: [
		'id',
		'created',
		'taskRequest',
		'status',
		'worksheet',
		'worksheetId',
		'processRequest',
		'processRequestId',
		'actionTask',
		'actionTaskId',
		'duration',
		'runbook',
		'createdBy',
		'lastUpdated',
		'lastUpdatedBy',
		'nodeId'
	],

	defaultData: {},

	id: '',
	taskRequest: '',
	status: '',
	worksheet: '',
	worksheetId: '',
	processRequest: '',
	processRequestId: '',
	actionTask: '',
	actionTaskId: '',
	duration: '',
	runbook: '',
	created: '',
	createdText$: function() {
		return this.created ? Ext.Date.format(Ext.Date.parse(this.created, 'c'), clientVM.getUserDefaultDateFormat()) : ''
	},
	createdBy: '',
	updated: '',
	updatedBy: '',
	sysOrg: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},
	lastUpdatedText$: function() {
		return this.lastUpdated ? Ext.Date.format(Ext.Date.parse(this.lastUpdated, 'c'), clientVM.getUserDefaultDateFormat()) : ''
	},
	nodeId: '',

	taskRequestTitle$: function() {
		return this.localize('taskRequestTitle') + (this.taskRequest ? ' - ' + this.taskRequest : '')
	},

	csrftoken: '',

	init: function() {
		this.initToken();
		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)
	},

	loadTaskRequest: function() {
		if (this.id)
			this.ajax({
				url: '/resolve/service/taskrequest/get',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) this.loadData(response.data)
					else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.TaskRequests'
		})
	},
	backIsVisible$: function() {
		return clientVM.screens.length > 1
	},

	abort: function() {
		this.message({
			title: this.localize('abortTitle'),
			msg: this.localize('abortMessage'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('abortAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyAbortRecords
		})
	},

	reallyAbortRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			this.ajax({
				url: '/resolve/service/taskrequest/abort',
				params: {
					taskRequestIds: [this.id]
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) this.loadTaskRequest()
					else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
	},

	worksheetLink$: function() {
		return '/resolve/client/rsclient.jsp?' + this.csrftoken + '&debug=true&mock=true#RS.worksheet.Worksheet/id=' + this.worksheetId
	},
	jumpToWorksheet: function() {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.Worksheet',
			params: {
				id: this.worksheetId
			}
		})
	},
	jumpToWorksheetIsVisible$: function() {
		return this.worksheetId
	},

	jumpToActionTask: function() {
		clientVM.handleNavigation({
			modelName: 'RS.actionTask.ActionTask',
			params: {
				id: this.actionTaskId
			}
		})
	},
	jumpToActionTaskIsVisible$: function() {
		return this.actionTaskId
	},

	jumpToProcessRequest: function() {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.ProcessRequest',
			params: {
				id: this.processRequestId
			}
		})
	},
	jumpToProcessRequestIsVisible$: function() {
		return this.processRequestId
	}
})