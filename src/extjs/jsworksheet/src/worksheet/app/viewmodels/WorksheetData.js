glu.defModel('RS.worksheet.WorksheetData', {
	fields : ['name'],
	id: '',	
	nameIsValid$: function() {
		return this.name ? true : this.localize('nameInvalid')
	},
	value: '',

	editMode: false,

	init: function() {
		if (this.name) this.set('editMode', true)
	},

	cancel: function() {
		this.doClose()
	},

	save: function() {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		this.ajax({
			url: '/resolve/service/wsdata/put',
			params: {
				propertyName: this.name,
				propertyValue: this.value,
				problemId: this.id
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.localize('propertySaved'))
					this.parentVM.loadWorksheet()
					this.doClose()
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	}
})