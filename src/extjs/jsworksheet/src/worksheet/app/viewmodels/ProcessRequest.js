glu.defModel('RS.worksheet.ProcessRequest', {
	mock: false,

	activate: function(screen, params) {
		this.resetForm()
		this.set('id', params ? params.id : '')
		this.loadProcessRequest()
	},

	resetForm: function() {
		this.loadData(this.defaultData)
	},

	isArhive: false,

	fields: [
		'id',
		'number',
		'processRequest',
		'status',
		'problemNumber',
		'worksheetId',
		'wiki',
		'duration',
		'created',
		'timeout'
	].concat(RS.common.grid.getSysFields()),

	defaultData: {},

	id: '',
	number: '',
	processRequest: '',
	status: '',
	problemNumber: '',
	worksheetId: '',
	wiki: '',
	duration: '',
	timeout: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrg: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},
	processRequestTitle$: function() {
		return this.localize('processRequestTitle') + (this.processRequest ? ' - ' + this.processRequest : '')
	},

	init: function() {
		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)
	},

	loadProcessRequest: function() {
		if (this.id)
			this.ajax({
				url: '/resolve/service/processrequest/get',
				params: {
					id: this.id,
					isArhive: this.isArhive
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) this.loadData(response.data)
					else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.ProcessRequests'
		})
	},

	abort: function() {
		this.message({
			title: this.localize('abortTitle'),
			msg: this.localize('abortMessage'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('abortAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyAbortRecords
		})
	},

	reallyAbortRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			this.ajax({
				url: '/resolve/service/processrequest/abort',
				params: {
					id: [this.id]
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) this.loadProcessRequest()
					else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
	},

	stateId: 'processRequestTaskRequestsList',

	jumpToWorksheet: function() {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.Worksheet',
			params: {
				id: this.worksheetId
			}
		})
	},
	jumpToWorksheetIsVisible$: function() {
		return this.worksheetId
	}
})