glu.defModel('RS.worksheet.Worksheet', {
	mock: false,
	API : {
		setActive : '/resolve/service/worksheet/setActive',
		getActive : '/resolve/service/worksheet/getActive',
		saveWS : '/resolve/service/worksheet/save',
		deleteWS : '/resolve/service/wsdata/delete',
		getWSData : '/resolve/service/wsdata/getMap',
		getNextNumber : '/resolve/service/worksheet/getNextNumber'
	},
	activate: function(screen, params) {
 		this.initToken();
		clientVM.setWindowTitle(this.localize('worksheet'))
		this.set('all', Ext.state.Manager.get('RS.worksheet.showAll'));
		this.set('activeTab', 1)
		if (params && Ext.isDefined(params.activeTab) && Ext.isNumber(Number(params.activeTab))) this.set('activeTab', Number(params.activeTab))
		this.resetForm()
		if (clientVM.fromArchive && !(params || {}).id) {
			history.replaceState({}, '', '/resolve/jsp/rsclient.jsp?' + this.csrftoken + '#RS.worksheet.ArchivedWorksheet')
			window.location.href = '/resolve/jsp/rsclient.jsp?' +this.csrftoken + '#RS.worksheet.ArchivedWorksheet';
			return;
		}
		this.set('id', params ? params.id : clientVM.problemId);
		this.processWorksheetPage(this.id);
		if (params && params.setActive === 'true')
			this.reallySetActive('yes');
		else
			this.loadWorksheet();
		if (params && params.number)
			this.set('number', params.number);
		// Reset the grid store on new worksheet
		(!this.id && this.worksheetResults) ? this.worksheetResults.removeAll(): null;
	},

	initToken: function() {
		this.set('csrftoken', clientVM.rsclientToken);
	},

	//This method is used to keep track of current page for each different worksheet.
	worksheetPaginationMap : [],
	processWorksheetPage : function(worksheetID){
		var found = false;
		var wsPage = '';
		for(var i = 0; i < this.worksheetPaginationMap.length; i++){
			var ws = this.worksheetPaginationMap[i];
			if(worksheetID == ws.id){
				found = true;
				wsPage = ws.page;
				break;
			}
		}
		if(found){
			this.worksheetResults.currentPage = wsPage;
		}
		else
		{
			this.worksheetPaginationMap.push({
				id : worksheetID,
				page : 1
			})
			this.worksheetResults.currentPage = 1;
		}
	},
	updateWorksheetPageMap : function(worksheetID, currentPage){
		for(var i = 0; i < this.worksheetPaginationMap.length; i++){
			var ws = this.worksheetPaginationMap[i];
			if(worksheetID == ws.id){
				ws.page = currentPage;
				break;
			}
		}
	},
	socialIsPressed: false,
	socialPressedCls$: function() {
		return this.socialIsPressed ? 'rs-social-button-pressed icon-comment' : 'icon-comment-alt'
	},

	when_socialIsPressed_changes_store_state: {
		on: ['socialIsPressedChanged'],
		action: function() {
			Ext.state.Manager.set('worksheetSocialIsPressed', this.socialIsPressed)
		}
	},

	worksheetTitle$: function() {
		return this.localize('worksheet') + (this.number ? ' - ' + this.number : '')
	},

	stateId: 'worksheetResultList',

	activeTab: 0,

	isResolveDev$: function() {
		return hasPermission(['resolve_dev', 'resolve_process'])
	},

	generalTab: function() {
		this.set('activeTab', 0)
	},
	generalTabIsPressed$: function() {
		return this.activeTab == 0
	},
	// generalTabIsVisible$: function() {
	// 	return this.isResolveDev
	// },
	resultsTab: function() {
		this.set('activeTab', 1)
	},
	resultsTabIsPressed$: function() {
		return this.activeTab == 1
	},
	// resultsTabIsVisible$: function() {
	// 	return this.isResolveDev
	// },
	debugTab: function() {
		this.set('activeTab', 2)
	},
	debugTabIsPressed$: function() {
		return this.activeTab == 2
	},
	// debugTabIsVisible$: function() {
	// 	return this.isResolveDev
	// },
	dataTab: function() {
		this.set('activeTab', 3)
	},
	dataTabIsPressed$: function() {
		return this.activeTab == 3
	},
	// dataTabIsVisible$: function() {
	// 	return this.isResolveDev
	// },

	displayFilter$: function() {
		return this.resultsTabIsPressed
	},

	resetForm: function() {
		this.loadData(this.defaultData)
	},


	fields: ['number', 'alertId', 'reference', 'correlationId', 'sirId','assignedTo', 'assignedToName', 'summary', 'condition', 'severity', 'description', 'workNotes', 'debug'].concat(RS.common.grid.getSysFields()),
	id: '',
	streamType: 'worksheet',
	number: '',
	alertId: '',
	reference: '',
	correlationId: '',
	assignedTo: '',
	assignedToName: '',
	summary: '',
	condition: '',
	severity: '',
	description: '',
	workNotes: '',
	workNotesData: '',
	debug: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrg: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	defaultData: {
		id: '',
		number: '',
		alertId: '',
		reference: '',
		correlationId: '',
		assignedTo: '',
		assignedToName: '',
		summary: '',
		condition: '',
		severity: '',
		description: '',
		workNotes: '',
		workNotesData: '',
		debug: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrg: ''
	},

	worksheetResults: {
		mtype: 'store',
		remoteSort: true,
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'ASC'
		}],
		fields: ['id', 'address',
			'completion',
			'condition',
			'esbaddr',
			'severity',
			'executeRequestId',
			'executeRequestNumber',
			'executeResultId',
			'problem',
			'process',
			'processNumber',
			'processId',
			'targetGUID',
			'actionTask',
			'actionTaskName',
			'actionTaskFullName',
			'actionResultId',
			'detail',
			'taskName',
			'taskId',
			'summary', 'sysId', {
				name: 'duration',
				type: 'int'
			}
		].concat(RS.common.grid.getSysFields()
		),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/worksheet/results',
			reader: {
				type: 'json',
				root: 'records'
			}
		}
	},

	worksheetColumns: [{
		text: '~~actionTask~~',
		dataIndex: 'taskName',
		// renderer: RS.common.grid.linkRenderer('RS.actiontask.ActionTask', 'taskId', '_blank'),
		renderer: function(value, meta, record) {
			if (value)
				return Ext.String.format('<a target="_blank" class="rs-link" href="/resolve/jsp/rsclient.jsp?{0}#RS.actiontask.ActionTask/id={1}">{2}</a>', this.csrftoken, record.get('taskId'), value)
			return ''
		},
		sortable: true,
		filterable: true
	}, {
		text: '~~completion~~',
		dataIndex: 'completion',
		width: 75,
		sortable: true,
		filterable: true
	}, {
		text: '~~condition~~',
		dataIndex: 'condition',
		renderer: RS.common.grid.statusRenderer('rs-worksheet-status'),
		width: 90,
		sortable: true,
		filterable: true
	}, {
		text: '~~severity~~',
		dataIndex: 'severity',
		renderer: RS.common.grid.statusRenderer('rs-worksheet-status'),
		width: 90,
		sortable: true,
		filterable: true
	}, {
		text: '~~summary~~',
		dataIndex: 'summary',
		flex: 1,
		sortable: false,
		filterable: true,
		renderer: function(value) {
			if (value.indexOf('<!-- DETAIL_TYPE_HTML -->') == 0 || value.indexOf('<!-- UI_TYPE_HTML -->') == 0) {
				return Ext.util.Format.nl2br('<pre>' + clientVM.injectRsclientToken(value) + '</pre>')
			}
			return Ext.util.Format.nl2br('<pre>' + Ext.String.htmlEncode(value) + '</pre>')
		}
	}, {
		text: '~~processRequest~~',
		dataIndex: 'processNumber',
		// renderer: RS.common.grid.internalLinkRenderer('RS.worksheet.ProcessRequest', 'id', 'processId'),
		renderer: function(value, meta, record) {
			if (value)
				return Ext.String.format('<a target="_blank" class="rs-link" href="/resolve/jsp/rsclient.jsp?{0}#RS.worksheet.ProcessRequest/id={1}">{2}</a>', this.csrftoken, record.get('processId'), value)
			return ''
		},
		sortable: true,
		filterable: true,
		hidden: true
	}, {
		text: '~~duration~~',
		dataIndex: 'duration',
		width: 60,
		sortable: true,
		filterable: true,
		hidden: true
	}, {
		text: '~~target~~',
		dataIndex: 'address',
		sortable: true,
		filterable: true,
		hidden: true
	}, {
		text: '~~guid~~',
		dataIndex: 'targetGUID',
		sortable: false,
		filterable: true,
		hidden: true
	}, {
		text: '~~detail~~',
		dataIndex: 'detail',
		hidden: true,
		hidable: false,
		sortable: false,
		filterable: true
	}, {
		text: '~~queueName~~',
		dataIndex: 'esbaddr',
		sortable: true,
		filterable: true,
		hidden: true
	}].concat(
			(function () {
				var commonCols = RS.common.grid.getSysColumns();
				var index = -1;

				for(var i = 0; i < commonCols.length; i++) {
					if(commonCols[i].dataIndex === 'sysCreatedOn') {
						index = i;
						break;
					}
				}

				if(index !== -1) {
					commonCols[index].hidden = false;
					commonCols[index].initialShow = true;
					commonCols[index].flex = 1;
				}

				return commonCols;
			})()
			),

	wsColumns: [{
		text: '~~name~~',
		dataIndex: 'name',
		flex: 1
	}, {
		text: '~~value~~',
		dataIndex: 'value',
		flex: 1
	}],

	wsData: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	wsDataSelections: [],

	conditionStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	severityStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	workNotesDataStore: {
		mtype: 'store',
		fields: [{
				type: 'date',
				name: 'createDate',
				format: 'time',
				dateFormat: 'time'
			},
			'userid',
			'worknotesDetail'
		],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	workNotesColumns: [],

	csrftoken: '',

	init: function() {
		this.initToken();

		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)

		if (Ext.isString(this.activeTab)) this.set('activeTab', Number(this.activeTab))

		Ext.Array.forEach(this.worksheetColumns, function(col) {
			if (col.dataIndex == 'sys_id') col.dataIndex = 'sysId'
		})

		this.set('workNotesColumns', [{
			text: '~~sysCreatedOn~~',
			dataIndex: 'createDate',
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat()),
			width: 200
		}, {
			text: '~~sysCreatedBy~~',
			dataIndex: 'userid'
		}, {
			text: '~~note~~',
			dataIndex: 'worknotesDetail',
			flex: 1,
			renderer: function(value) {
				return Ext.util.Format.nl2br('<pre style="word-wrap:break-word;">' + Ext.util.Format.nl2br(Ext.String.htmlEncode(value)) + '</pre>')
			}
		}])

		//correct worksheet columns
		// var updatedCol, index = -1;
		// Ext.Array.each(this.worksheetColumns, function(column, idx) {
		// 		if (column.dataIndex == 'sysUpdatedOn') {
		// 			updatedCol = column
		// 			index = idx
		// 			return false
		// 		}
		// 	})
		// if (updatedCol && index > -1) {
		// 	updatedCol.renderer = Ext.util.Format.dateRenderer('H:i:s.u M j')
		// 	updatedCol.hidden = false
		// 	updatedCol.initialShow = true
		// 	this.worksheetColumns.splice(index, 1)
		// 	this.worksheetColumns.splice(0, 0, updatedCol)
		// }

		this.set('worksheetColumns', Ext.Array.filter(this.worksheetColumns, function(column) {
			return column.dataIndex != 'sysUpdatedOn' && column.dataIndex != 'sysUpdatedBy';
		}));
		//Populate combobox stores
		this.conditionStore.add([{
			name: this.localize('good'),
			value: 'GOOD'
		}, {
			name: this.localize('bad'),
			value: 'BAD'
		}, {
			name: this.localize('unknown'),
			value: 'UNKNOWN'
		}])

		this.severityStore.add([{
			name: this.localize('critical'),
			value: 'CRITICAL'
		}, {
			name: this.localize('severe'),
			value: 'SEVERE'
		}, {
			name: this.localize('warning'),
			value: 'WARNING'
		}, {
			name: this.localize('good'),
			value: 'GOOD'
		}])
		this.worksheetResults.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {}
			operation.params.id = this.id;
			operation.params.hidden = !this.all;
			this.addRawToSorterProp(operation.sorters);
			this.updateWorksheetPageMap(this.id, store.currentPage);
		}, this)
		this.set('socialIsPressed', Ext.state.Manager.get('worksheetSocialIsPressed', false))
	},
	addRawToSorterProp: function(sorters) {
		// This method is needed after upgrading ES to 5.x. After migration, 'text' field was changed to raw 'keyword'
		// so we need to apply '.raw' to fields that were changed to raw 'keyword'.
		// FYI, sorting on 'GUID' isn't working because the field has been properly migrated. We need to fix sorting
		// on 'GUID' accordingly after the BE correct the migration.
		for(var i=0; i<sorters.length; i++) {
			var curSorter = sorters[i];
			if (curSorter.property.indexOf('.raw') == -1) {// iif '.raw' has not already been appended
				switch (curSorter.property) {
				case 'taskName':
				case 'duration':
				case 'targetGUID':
				case 'sysCreatedBy':
				case 'sysCreatedOn':
				case 'sysId':
				case 'sysUpdatedOn':
				case 'sysUpdatedBy':
				case 'detail':
					// do nothing
					break;
				default:
					curSorter.property += '.raw';
					break;
				}
			}
		}
	},
	loadActiveWorksheet: function() {
		this.ajax({
			url : this.API['getActive'],
			method : 'GET',
			params : {
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.loadData(response.data)
					this.socialDetail.set('initialStreamId', response.data.id)
					this.socialDetail.set('streamParent', response.data.number)
					this.set('workNotesData', this.workNotes)
					this.set('workNotes', '')
					var records = this.workNotesData ? Ext.decode(this.workNotesData) : [] || [];
					this.workNotesDataStore.add(records)

					this.worksheetResults.load()

					this.ajax({
						url: this.API['getWSData'],
						params: {
							problemId: this.id
						},
						success: function(r) {
							var response = RS.common.parsePayload(r)
							if (response.success) {
								var data = []
								Ext.Object.each(response.data, function(key) {
									data.push({
										name: key,
										value: response.data[key]
									})
								})
								this.wsData.add(data)
							} else clientVM.displayError(response.message)
						},
						failure: function(resp) {
							clientVM.displayFailure(resp);
						}
					})
				} else clientVM.displayError(response.message)
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	populateWorksheetWithResponseData: function(data, removeAll) {
		if (removeAll) {
			this.wsData.removeAll()
			this.workNotesDataStore.removeAll()
		}
		this.loadData(data)
		this.socialDetail.set('initialStreamId', data.id)
		this.socialDetail.set('streamParent', data.number)
		this.set('workNotesData', this.workNotes)
		this.set('workNotes', '')
		var records = this.workNotesData ? Ext.decode(this.workNotesData) : [] || [];
		this.workNotesDataStore.add(records)
	},

	loadWorksheetWithId: function() {
		this.worksheetResults.load();
		this.ajax({
			url: '/resolve/service/worksheet/getWS',
			params: {
				id: this.id
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.populateWorksheetWithResponseData(response.data)
				} else clientVM.displayError(response.message)
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})

		this.ajax({
			url: '/resolve/service/worksheet/wsdata/getMap',
			params: {
				problemId: this.id
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					var data = []
					Ext.Object.each(response.data, function(key) {
						data.push({
							name: key,
							value: response.data[key]
						})
					})
					this.wsData.add(data)
				} else clientVM.displayError(response.message)
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	getNextNumber: function() {
		this.ajax({
			url: this.API['getNextNumber'],
			method: 'POST',
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success)
					this.set('number', response.data)
				else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		});
	},
	loadWorksheet: function() {
		this.wsData.removeAll()
		this.workNotesDataStore.removeAll()
		//Postpone loading for now until new ProblemId come back.
		if (clientVM.waitClientVMFetchProblemIdFromUrl)
			clientVM.on('problemIdFetchedFromUrl', function() {
				this.set('id', clientVM.problemId);
				this.loadWorksheet();
				return 'discard';
			}, this);
		else if (this.id) {
			if (this.id == 'ACTIVE')
				this.loadActiveWorksheet();
			else
				this.loadWorksheetWithId();
		} else
			this.getNextNumber();
	},

	exitAfterSave: false,
	saving: false,
	save: function() {
		this.set('saving', true)
		var worksheet = Ext.apply({
			sysId: this.id
		}, this.asObject());

		delete worksheet.sys_id
		delete worksheet.sysCreatedOn
		delete worksheet.sysCreatedBy
		delete worksheet.sysUpdatedOn
		delete worksheet.sysUpdatedBy

		worksheet.sysOrg = clientVM.orgId == 'nil' ? '' : clientVM.orgId;

		this.ajax({
			url: this.API['saveWS'],
			jsonData: worksheet,
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.localize('saveMessage'))
					this.set('id', response.data.id)
					//this.loadWorksheet()
					this.populateWorksheetWithResponseData(response.data, true);
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('saving', false);
			}
		})
	},
	saveIsEnabled$: function() {
		return !this.saving
	},
	saveAndExitIsVisible$: function() {
		return clientVM.screens.length > 1
	},
	saveAndExit: function() {
		this.set('exitAfterSave', true)
		this.save()
	},
	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.Worksheets'
		})
	},
	refresh: function() {
		this.loadWorksheet()
	},

	setActiveIsVisible$: function() {
		return this.id && this.id !== 'ACTIVE';
	},
	setActive: function() {
		this.message({
			title: this.localize('setActiveTitle'),
			msg: this.localize('setActiveMessage', this.number),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('setActiveAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallySetActive
		})
	},

	reallySetActive: function(button) {
		if (button == 'yes') {
			this.ajax({
				url: this.API['setActive'],
				params: {
					id: this.id,
					'RESOLVE.ORG_NAME' : clientVM.orgName,
					'RESOLVE.ORG_ID' : clientVM.orgId
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						clientVM.updateProblemInfo(this.id, this.number)
						clientVM.displayMessage(null, '', {
							msg: this.localize('worksheetSetActiveSuccess', [this.number, this.id])
						});
						this.loadWorksheet()
					} else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
	},

	editResult: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.TaskResult',
			params: {
				id: id,
				activeTab: 1
			}
		})
	},

	jumpToAssignedTo: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.user.User',
			params: {
				username: this.assignedTo
			}
		})
	},
	jumpToAssignedToIsVisible$: function() {
		return this.assignedToName
	},

	assignedToClicked: function() {
		this.open({
			mtype: 'RS.user.UserPicker',
			pick: true,
			callback: this.assignedToSelected
		})
	},

	assignedToClickedClear: function() {
		this.set('assignedTo', '')
		this.set('assignedToName', '')
	},

	assignedToControl: null,
	registerAssignedTo: function(assignedToControl) {
		this.set('assignedToControl', assignedToControl)
		this.assignedToControl.triggerEl.item(1).parent().setDisplayed((this.assignedToName || this.assignedTo) ? 'table-cell' : 'none')
	},

	when_assignedTo_changes_show_or_hide_clear_box: {
		on: ['assignedToChanged', 'assignedToNameChanged'],
		action: function() {
			if (this.assignedToControl) {
				this.assignedToControl.triggerEl.item(1).parent().setDisplayed((this.assignedToName || this.assignedTo) ? 'table-cell' : 'none')
				this.assignedToControl.doComponentLayout()
			}
		}
	},

	assignedToSelected: function(records) {
		var record = records[0]
		this.set('assignedTo', record.get('uuserName'))
		this.set('assignedToName', record.get('udisplayName'))
	},

	addWSData: function() {
		this.open({
			mtype: 'RS.worksheet.WorksheetData',
			id: this.id
		})
	},

	editWSData: function(name) {
		var idx = this.wsData.findExact('name', name);
		this.open({
			mtype: 'RS.worksheet.WorksheetData',
			id: this.id,
			name: name,
			value: idx > -1 ? this.wsData.getAt(idx).get('value') : ''
		})
	},

	deleteWSData: function() {
		this.message({
			title: this.localize('confirmWSDataDelete'),
			msg: this.localize(this.wsDataSelections.length === 1 ? 'deleteWSDataMessage' : 'deleteWSDatasMessage'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteWSData'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteWSData
		})
	},
	deleteWSDataIsEnabled$: function() {
		return this.wsDataSelections.length > 0
	},

	reallyDeleteWSData: function(btn) {
		if (btn == 'yes') {
			var selections = [];
			Ext.Array.forEach(this.wsDataSelections, function(selection) {
				selections.push(selection.get('name'))
			})

			this.ajax({
				url: this.API['deleteWS'],
				params: {
					problemId: this.id,
					ids: selections
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						clientVM.displaySuccess(this.localize('wsDataDeleted'))
						this.loadWorksheet()
					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},

	all: true,

	showAll: function() {
		Ext.state.Manager.set('RS.worksheet.showAll', this.all);
		this.worksheetResults.load();
	},

	hide: function() {
		this.set('all', false);
		Ext.state.Manager.set('RS.worksheet.showAll', this.all);
		this.worksheetResults.load();
	},

	//Child Viewmodels
	socialDetail: {
		mtype: 'RS.social.Main',
		mode: 'embed',
		initialStreamType: 'worksheet'
	}

})