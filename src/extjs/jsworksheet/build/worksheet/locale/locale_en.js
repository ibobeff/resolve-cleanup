/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.worksheet').locale = {

	//Worksheet Columns
	created: 'Created',
	number: 'Number',
	condition: 'Condition',
	summary: 'Summary',
	severity: 'Severity',
	reference: 'Reference',
	lookupId: 'Lookup ID',
	alertId: 'Alert ID',
	correlationId: 'Correlation ID',
	sirId : 'SIR ID',
	organization: 'Organization',
	assignedTo: 'Assigned User ID',
	assignedToName: 'Assigned To',

	sysCreatedBy: 'Created By',
	sysCreatedOn: 'Created On',
	note: 'Note',

	//Worksheet Data Columns
	name: 'Name',
	value: 'Value',

	socialTooltip: 'Social',

	//Result grid columns
	lastUpdated: 'Last Updated',
	actionTask: 'Action Task',
	completion: 'Completion',
	condition: 'Condition',
	severity: 'Severity',
	summary: 'Summary',
	processRequest: 'Process Request',
	taskRequest: 'Task Request',
	duration: 'Duration',
	queueName: 'Queue Name',
	target: 'Target',
	guid: 'GUID',
	updatedBy: 'Updated By',
	detail: 'Detail',

	//Task Results Columns
	duration: 'Duration',
	worksheet: 'Worksheet',

	//Process Requests Columns
	status: 'Status',
	runbook: 'Runbook',
	wiki: 'Runbook',
	problemNumber: 'Worksheet',

	cancel: 'Cancel',



	Worksheets: {
		allWorksheets: 'Worksheets',
		oldWorksheet: 'Unable to set worksheet as selected because it has expired',

		//Actions
		createWorksheet: 'New',
		editWorksheet: 'Edit',
		setActive: 'Select',
		deleteWorksheet: 'Delete',

		//Delete Action
		deleteTitle: 'Confirm Deletion',
		deleteMessage: 'Are you sure you want to delete the selected worksheet?<br><font color=\'red\'> NOTE: Incident related worksheets or active worksheets will NOT be deleted.</font>',
		deletesMessage: 'Are you sure you want to delete the {0} selected worksheets?<br><font color=\'red\'> NOTE: Incident related worksheet(s) will NOT be deleted.</font>',
		deleteAction: 'Delete',
		cancel: 'Cancel',

		//Select Action
		setActiveTitle: 'Confirm Select',
		setActiveMessage: 'Are you sure you want to make {0} the selected worksheet?',
		setActiveAction: 'Select',
		worksheetSetActiveSuccess: 'Worksheet {0} is now selected, <a href="#RS.worksheet.Worksheet/id={1}">click to view</a>'
	},

	ArchivedWorksheets: {
		allArchivedWorksheets: 'Archived Worksheets',
		setActive: 'Select',
		setActiveTitle: 'Confirm Select',
		setActiveMessage: 'Are you sure you want to make {0} the selected worksheet?',
		setActiveAction: 'Select',
		archivedWorksheetSetActiveSuccess: 'Worksheet {0} is now selected, <a href="#RS.worksheet.ArchivedWorksheet/id={1}">click to view</a>'
	},

	ArchivedWorksheet: {
		//Conditions
		good: 'Good',
		bad: 'Bad',
		unknown: 'Unknown',

		//Severity
		critical: 'Critical',
		severe: 'Severe',
		warning: 'Warning',

		archivedWorksheet: 'Archived Worksheet',
		setActive: 'Select',
		generalTab: 'General',
		resultsTab: 'Results',
		debugTab: 'Debug',
		dataTab: 'Data',
		showAll: 'Advanced View',
		description: 'Description',
		workNotes: 'Work Notes',
		setActiveTitle: 'Confirm Select',
		setActiveMessage: 'Are you sure you want to make {0} the selected worksheet?',
		setActiveAction: 'Select',
		archivedWorksheetSetActiveSuccess: 'Worksheet {0} is now selected, <a href="#RS.worksheet.ArchivedWorksheet/id={1}">click to view<a>'
	},

	Worksheet: {
		//Tabs
		generalTab: 'General',
		resultsTab: 'Results',
		debugTab: 'Debug',
		dataTab: 'Data',

		//Actions
		save: 'Save',
		saveAs: 'Save As',
		saveAndExit: 'Save & Exit',
		back: 'Back',
		refresh: 'Refresh',
		setActive: 'Select',
		showAll: 'Advanced View',

		createdBy: 'Created By',
		created: 'Created',
		createdText: 'Created',
		description: 'Description',
		workNotes: 'Work Notes',

		xml: 'XML',
		debug: 'Debug',

		//Conditions
		good: 'Good',
		bad: 'Bad',
		unknown: 'Unknown',

		//Severity
		critical: 'Critical',
		severe: 'Severe',
		warning: 'Warning',

		saveTitle: 'Success',
		saveMessage: 'Worksheet saved',

		addWSData: 'Add',

		//Select Action
		setActiveTitle: 'Confirm Select',
		setActiveMessage: 'Are you sure you want to make {0} the active worksheet?',
		setActiveAction: 'Select',
		worksheetSetActiveSuccess: 'Worksheet {0} is now active, <a href="#RS.worksheet.Worksheet/id={1}">click to view<a>',

		confirmWSDataDelete: 'Confirm Data Deletion',
		deleteWSDataMessage: 'Are you sure you want to delete the selected data record?',
		deleteWSDatasMessage: 'Are you sure you want to delete the {0} selected data records?',
		deleteWSData: 'Delete',
		wsDataDeleted: 'Data successfully deleted',
		actionTask: 'Action Task'

	},

	TaskResults: {
		taskResults: 'Task Results',

		//Actions
		deleteTaskResult: 'Delete',
		deleteTitle: 'Delete Task Result',
		deleteMessage: 'Are you sure you want to delete the selected task result?',
		deletesMessage: 'Are you sure you want to delete the {0} selected task results?',
		deleteAction: 'Delete',
		cancel: 'Cancel'
	},

	ArchivedTaskResult: {
		taskResultTitle: 'Task Result',

		actionTask: 'Action Task',
		actionTaskFullName: 'Action Task',
		actionTaskName: 'Action Task',
		taskName: 'Action Task',
		problemNumber: 'Worksheet',
		processNumber: 'Process Request',
		address: 'Target',
		esbAddr: 'Queue Name',
		targetGUID: 'GUID',
		address: 'Target',
		created: 'Created',
		createdText: 'Created',
		createdBy: 'Created By',
		lastUpdated: 'Last Updated',
		lastUpdatedText: 'Last Updated',
		lastUpdatedBy: 'Last Updated By',
		duration: 'Duration',
		summary: 'Summary',
		summaryDisplay: 'Summary',

		// Tabs
		generalTab: 'General',
		detailTab: 'Detail',
		rawTab: 'Raw',

		// Actions
		back: 'Back'

	},

	TaskResult: {
		taskResultTitle: 'Task Result',

		actionTask: 'Action Task',
		actionTaskFullName: 'Action Task',
		actionTaskName: 'Action Task',
		taskName: 'Action Task',
		problemNumber: 'Worksheet',
		processNumber: 'Process Request',
		address: 'Target',
		esbAddr: 'Queue Name',
		targetGUID: 'GUID',
		address: 'Target',
		created: 'Created',
		createdText: 'Created',
		createdBy: 'Created By',
		lastUpdated: 'Last Updated',
		lastUpdatedText: 'Last Updated',
		lastUpdatedBy: 'Last Updated By',
		duration: 'Duration',
		summary: 'Summary:',
		summaryDisplay: 'Summary',

		// Tabs
		generalTab: 'General',
		detailTab: 'Detail',
		rawTab: 'Raw',

		// Actions
		back: 'Back'

	},

	ProcessRequests: {
		processRequests: 'Process Requests',

		//Actions
		deleteProcessRequest: 'Delete',
		deleteAllProcessRequest : 'Delete All',
		deleteTitle: 'Delete Process Request',
		deleteAllTitle: 'Delete All Process Request',
		deleteMessage: 'Are you sure you want to delete the selected process request?',
		deletesMessage: 'Are you sure you want to delete the {0} selected process requests?',
		deleteAllMessage: 'Are you sure you want to delete the {0} <b>{1}</b> process requests?',
		deleteAction: 'Delete',
		cancel: 'Cancel',

		abortProcessRequest: 'Abort',
		abortAllProcessRequest : 'Abort All',
		abortTitle: 'Abort Process Request',
		abortAllTitle: 'Abort All Process Request',
		abortMessage: 'Are you sure you want to abort the selected process request?',
		abortsMessage: 'Are you sure you want to abort the {0} selected process requests?',
		abortAllMessage: 'Are you sure you want to abort the {0} <b>{1}</b> process requests?',
		abortAction: 'Abort',
		cancel: 'Cancel'
	},

	ProcessRequest: {
		processRequestTitle: 'Process Request',

		//Actions
		back: 'Back',
		abort: 'Abort',
		abortTitle: 'Abort Task Request',
		abortMessage: 'Are you sure you want to abort the selected process request?',
		abortsMessage: 'Are you sure you want to abort the {0} selected process requests?',
		abortAction: 'Abort',
		cancel: 'Cancel',

		number: 'Process Request',
		worksheet: 'Worksheet',
		runbook: 'Runbook',
		timeout: 'Timeout (mins)',
		status: 'Status',
		created: 'Created',
		createdText: 'Created',
		createdBy: 'Created By',
		lastUpdated: 'Last Updated',
		lastUpdatedText: 'Last Updated',
		lastUpdatedBy: 'Last Updated By',
		duration: 'Duration',

		taskRequests: 'Task Requests',

		problemNumber: 'Worksheet'

	},

	TaskRequests: {
		taskRequests: 'Task Requests',

		//Actions
		deleteTaskRequest: 'Delete',
		deleteTitle: 'Delete Task Request',
		deleteMessage: 'Are you sure you want to delete the selected task request?',
		deletesMessage: 'Are you sure you want to delete the {0} selected task requests?',
		deleteAction: 'Delete',
		cancel: 'Cancel',

		abortTaskRequest: 'Abort',
		abortTitle: 'Abort Task Request',
		abortMessage: 'Are you sure you want to abort the selected task request?',
		abortsMessage: 'Are you sure you want to abort the {0} selected task requests?',
		abortAction: 'Abort'
	},

	TaskRequest: {
		taskRequestTitle: 'Task Request',
		taskRequest: 'Number',
		status: 'Execute Status',
		runbook: 'Runbook',
		processRequest: 'Action Process',
		worksheet: 'Worksheet',
		actionTask: 'Action Task',
		created: 'Created',
		createdText: 'Created',
		createdBy: 'Created By',
		lastUpdated: 'Last Updated',
		lastUpdatedText: 'Last Updated',
		lastUpdatedBy: 'Last Updated By',
		nodeId: 'Node ID',
		duration: 'Duration',

		//Actions
		back: 'Back',
		refresh: 'Refresh',
		abort: 'Abort',

		abortTitle: 'Abort Task Request',
		abortMessage: 'Are you sure you want to abort the selected task request?',
		abortsMessage: 'Are you sure you want to abort the {0} selected task requests?',
		abortAction: 'Abort',
		cancel: 'Cancel'
	},

	WorksheetPicker: {
		selectWorksheetTitle: 'Select Worksheet',
		go: 'Go',
		select: 'Select',
		continueWorksheet: 'Continue',
		newWorksheet: 'New',
		view: 'View',
		viewAll: 'View All',
		close: 'Close',
		cancel: 'Cancel',

		searchWorksheet: 'Search worksheets...',

		showMyWorksheets: 'Click to show my worksheets',
		showAllWorksheets: 'Click to show all worksheets',
		archivedWorksheetsNotFound: 'Cannot find the specified ws in archived worksheet.',
		worksheetSetActiveSuccess: 'Worksheet {0} is now selected, <a href="#RS.worksheet.Worksheet/id={1}">click to view<a>',
		archivedWorksheetSelectSuccess: 'ArchivedWorksheet {0} is now selected, <a href="#RS.worksheet.ArchivedWorksheet/id={1}">click to view<a>',
		newWorksheetActiveSuccess: 'New worksheet created and is now selected, <a href="#RS.worksheet.Worksheet/id={0}">click to view</a>',

		noWorksheetSelected: 'No worksheet was selected',
		worksheetTab: 'Worksheet',
		archivedWorksheetTab: 'Archived',
		esTitle: 'Worksheet',
		archivedTitle: 'Archived',
		listWorksheetError: 'Cannot fetch worksheets from the server. [{0}]'
	},

	WorksheetData: {
		worksheetDataTitle: 'Worksheet Data',
		save: 'Save',
		cancel: 'Cancel',

		nameInvalid: 'Please provide a property name',

		propertySaved: 'Property saved successfully'
	},

	ListRegistrationDetails: {
		title: 'Registration Details',

		uname: 'Name',
		ustatus: 'Status',
		uguid: 'Guid',
		utype: 'Type',
		uipaddress: 'Reg Address',
		ubuild: 'Build',

		sysUpdatedOn: 'Updated On',
		sysUpdatedBy: 'Updated By',

		close: 'Close'
	}
}
