/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/
glu.defModel('RS.worksheet.ArchivedTaskResult', {
	mixins: ['TaskResult'],
	fields: ['id',
		'sysId',
		'actionTask',
		'actionTaskFullName',
		'actionTaskId',
		'taskName',
		'taskId',
		'completion',
		'condition',
		'severity',
		'duration',
		'address',
		'summary',
		'updatedBy',
		'targetGUID',
		'summary',
		'raw',
		'esbAddr',
		'executeRequestId',
		'executeRequestNumber',
		'executeResultId',
		'problemId',
		'problemNumber',
		'processId',
		'processNumber',
		'actionTaskName',
		'actionTaskFullName',
		'taskNamespace',
		'detail',
		'timestamp'
	].concat(RS.common.grid.getSysFields()),
	loadTaskResult: function() {
		if (this.id)
			this.ajax({
				url: '/resolve/service/taskresult/getArchiveTaskResult',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) this.loadData(response.data)
					else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
	}
})
glu.defModel('RS.worksheet.ArchivedWorksheet', {
	mock: false,

	activate: function(screen, params) {
		this.initToken();
		clientVM.setWindowTitle(this.localize('archivedWorksheet'))
		this.set('activeTab', 1);
		this.set('all', Ext.state.Manager.get('RS.worksheet.showAllArchived'));
		if (params && Ext.isDefined(params.activeTab) && Ext.isNumber(Number(params.activeTab))) this.set('activeTab', Number(params.activeTab))
		this.resetForm()
		if (!clientVM.fromArchive && !(params || {}).id) {
			history.replaceState({}, '', '/resolve/jsp/rsclient.jsp?' + this.csrftoken + '#RS.worksheet.Worksheet')
			window.location.href = '/resolve/jsp/rsclient.jsp?' + this.csrftoken + '#RS.worksheet.Worksheet';
			return;
		}
		this.set('id', params ? params.id : clientVM.archivedProblemId)
		//if (params && params.setActive === 'true') this.reallySetActive('yes')
		this.loadWorksheet()
		if (params && params.number) this.set('number', params.number)
	},

	initToken: function() {
		this.set('csrftoken', clientVM.rsclientToken);
	},

	socialIsPressed: false,
	socialPressedCls$: function() {
		return this.socialIsPressed ? 'rs-social-button-pressed icon-comment' : 'icon-comment-alt'
	},

	when_socialIsPressed_changes_store_state: {
		on: ['socialIsPressedChanged'],
		action: function() {
			Ext.state.Manager.set('worksheetSocialIsPressed', this.socialIsPressed)
		}
	},

	worksheetTitle$: function() {
		return this.localize('archivedWorksheet') + (this.number ? ' - ' + this.number : '')
	},

	stateId: 'archivedWorksheetResultList',

	activeTab: 0,

	isResolveDev$: function() {
		return hasPermission(['resolve_dev', 'resolve_process'])
	},

	generalTab: function() {
		this.set('activeTab', 0)
	},
	generalTabIsPressed$: function() {
		return this.activeTab == 0
	},
	// generalTabIsVisible$: function() {
	// 	return this.isResolveDev
	// },
	resultsTab: function() {
		this.set('activeTab', 1)
	},
	resultsTabIsPressed$: function() {
		return this.activeTab == 1
	},
	// resultsTabIsVisible$: function() {
	// 	return this.isResolveDev
	// },
	debugTab: function() {
		this.set('activeTab', 2)
	},
	debugTabIsPressed$: function() {
		return this.activeTab == 2
	},
	// debugTabIsVisible$: function() {
	// 	return this.isResolveDev
	// },
	dataTab: function() {
		this.set('activeTab', 3)
	},
	dataTabIsPressed$: function() {
		return this.activeTab == 3
	},
	// dataTabIsVisible$: function() {
	// 	return this.isResolveDev
	// },

	displayFilter$: function() {
		return this.resultsTabIsPressed
	},

	resetForm: function() {
		this.loadData(this.defaultData)
	},


	fields: [
		'number',
		'alertId',
		'reference',
		'correlationId',
		'assignedTo',
		'assignedToName',
		'summary',
		'condition',
		'severity',
		'description',
		'workNotes',
		'debug'
	].concat(RS.common.grid.getSysFields()),
	id: '',
	streamType: 'worksheet',
	number: '',
	alertId: '',
	reference: '',
	correlationId: '',
	assignedTo: '',
	assignedToName: '',
	summary: '',
	condition: '',
	severity: '',
	description: '',
	workNotes: '',
	workNotesData: '',
	debug: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrg: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	defaultData: {
		id: '',
		number: '',
		alertId: '',
		reference: '',
		correlationId: '',
		assignedTo: '',
		assignedToName: '',
		summary: '',
		condition: '',
		severity: '',
		description: '',
		workNotes: '',
		workNotesData: '',
		debug: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrg: ''
	},

	worksheetResults: {
		mtype: 'store',
		remoteSort: true,
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'ASC'
		}],
		fields: ['id',
			'address',
			'completion',
			'condition',
			'esbaddr',
			'severity',
			'executeRequestId',
			'executeRequestNumber',
			'executeResultId',
			'problem',
			'process',
			'processNumber',
			'processId',
			'targetGUID',
			'actionTask',
			'actionTaskName',
			'actionTaskFullName',
			'actionResultId',
			'detail',
			'taskName',
			'taskId',
			'summary', 'sysId', {
				name: 'duration',
				type: 'int'
			}
		].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/worksheet/archiveResults',
			reader: {
				type: 'json',
				root: 'records'
			}
		}
	},

	worksheetColumns: [{
		text: '~~actionTask~~',
		dataIndex: 'taskName',
		// renderer: RS.common.grid.internalLinkRenderer('RS.actiontask.ActionTask', 'id', 'taskId'),
		renderer: function(value, meta, record) {
			if (value)
				return Ext.String.format('<a target="_blank" class="rs-link" href="/resolve/jsp/rsclient.jsp?{0}#RS.actiontask.ActionTask/id={1}">{2}</a>', this.csrftoken, record.get('taskId'), value)
			return ''
		},
		sortable: true,
		filterable: true
	}, {
		text: '~~completion~~',
		dataIndex: 'completion',
		width: 75,
		sortable: true,
		filterable: true
	}, {
		text: '~~condition~~',
		dataIndex: 'condition',
		renderer: RS.common.grid.statusRenderer('rs-worksheet-status'),
		width: 90,
		sortable: true,
		filterable: true
	}, {
		text: '~~severity~~',
		dataIndex: 'severity',
		renderer: RS.common.grid.statusRenderer('rs-worksheet-status'),
		width: 90,
		sortable: true,
		filterable: true
	}, {
		text: '~~summary~~',
		dataIndex: 'summary',
		flex: 1,
		sortable: false,
		filterable: false,
		renderer: function(value) {
			if (value.indexOf('<!-- DETAIL_TYPE_HTML -->') == 0 || value.indexOf('<!-- UI_TYPE_HTML -->') == 0) {
				return Ext.util.Format.nl2br('<pre>' + clientVM.injectRsclientToken(value) + '</pre>')
			}
			return Ext.util.Format.nl2br('<pre>' + Ext.String.htmlEncode(value) + '</pre>')
		}
	}, {
		text: '~~processRequest~~',
		dataIndex: 'processNumber',
		// renderer: RS.common.grid.internalLinkRenderer('RS.worksheet.ProcessRequest', 'id', 'processId'),
		sortable: true,
		filterable: true,
		hidden: true
	}, {
		text: '~~duration~~',
		dataIndex: 'duration',
		width: 60,
		sortable: true,
		filterable: true,
		hidden: true
	}, {
		text: '~~target~~',
		dataIndex: 'address',
		sortable: true,
		filterable: true,
		hidden: true
	}, {
		text: '~~guid~~',
		dataIndex: 'targetGUID',
		sortable: true,
		filterable: true,
		hidden: true
	}, {
		text: '~~detail~~',
		dataIndex: 'detail',
		hidden: true,
		hidable: false,
		sortable: true,
		filterable: true
	}, {
		text: '~~queueName~~',
		dataIndex: 'esbaddr',
		sortable: true,
		filterable: true,
		hidden: true
	}].concat(RS.common.grid.getSysColumns()),

	conditionStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	severityStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	workNotesDataStore: {
		mtype: 'store',
		fields: [{
				type: 'date',
				name: 'createDate',
				format: 'time',
				dateFormat: 'time'
			},
			'userid',
			'worknotesDetail'
		],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	workNotesColumns: [],

	csrftoken: '',

	init: function() {
		this.initToken();

		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)

		if (Ext.isString(this.activeTab)) this.set('activeTab', Number(this.activeTab))

		Ext.Array.forEach(this.worksheetColumns, function(col) {
			if (col.dataIndex == 'sys_id') col.dataIndex = 'sysId'
		})

		this.set('workNotesColumns', [{
			text: '~~sysCreatedOn~~',
			dataIndex: 'createDate',
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat()),
			width: 200
		}, {
			text: '~~sysCreatedBy~~',
			dataIndex: 'userid'
		}, {
			text: '~~note~~',
			dataIndex: 'worknotesDetail',
			flex: 1
		}])

		//correct worksheet columns
		var updatedCol, index = -1;
		Ext.Array.each(this.worksheetColumns, function(column, idx) {
			if (column.dataIndex == 'sysUpdatedOn') {
				updatedCol = column
				index = idx
				return false
			}
		})
		if (updatedCol && index > -1) {
			updatedCol.renderer = Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
			updatedCol.hidden = false
			updatedCol.initialShow = true
			this.worksheetColumns.splice(index, 1)
			this.worksheetColumns.splice(0, 0, updatedCol)
		}

		//Populate combobox stores
		this.conditionStore.add([{
			name: this.localize('good'),
			value: 'GOOD'
		}, {
			name: this.localize('bad'),
			value: 'BAD'
		}, {
			name: this.localize('unknown'),
			value: 'UNKNOWN'
		}])

		this.severityStore.add([{
			name: this.localize('critical'),
			value: 'CRITICAL'
		}, {
			name: this.localize('severe'),
			value: 'SEVERE'
		}, {
			name: this.localize('warning'),
			value: 'WARNING'
		}, {
			name: this.localize('good'),
			value: 'GOOD'
		}])
		this.worksheetResults.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {}
			operation.params.id = this.id;
			operation.params.hidden = !this.all;
		}, this)

		this.set('socialIsPressed', Ext.state.Manager.get('worksheetSocialIsPressed', false))
	},

	loadWorksheet: function() {
		this.workNotesDataStore.removeAll()

		if (this.id) {
			if (this.id == 'ACTIVE') {
				this.ajax({
					url: '/resolve/service/worksheet/getActive',
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.loadData(response.data)
							this.socialDetail.set('initialStreamId', response.data.id)
							this.socialDetail.set('streamParent', response.data.number)
							this.set('workNotesData', this.workNotes)
							this.set('workNotes', '')
							var records = this.workNotesData ? Ext.decode(this.workNotesData) : [] || [];
							this.workNotesDataStore.add(records)

							this.worksheetResults.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				this.worksheetResults.load()

				this.ajax({
					url: '/resolve/service/worksheet/getArchive',
					params: {
						id: this.id
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.loadData(response.data)
							this.socialDetail.set('initialStreamId', response.data.id)
							this.socialDetail.set('streamParent', response.data.number)
							this.set('workNotesData', this.workNotes)
							this.set('workNotes', '')
							var records = this.workNotesData ? Ext.decode(this.workNotesData) : [] || [];
							this.workNotesDataStore.add(records)
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		} else {
			this.ajax({
				url: '/resolve/service/worksheet/getNextNumber',
				method: 'POST',
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success)
						this.set('number', response.data)
					else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})

		}
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.ArchivedWorksheets'
		})
	},
	refresh: function() {
		this.loadWorksheet()
	},

	/* setActiveIsVisible$: function() {
		return this.id
	},
	setActive: function() {
		this.message({
			title: this.localize('setActiveTitle'),
			msg: this.localize('setActiveMessage', this.number),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('setActiveAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallySetActive
		})
	},

	reallySetActive: function(button) {
		if (button == 'yes') {
			clientVM.updateProblemInfo(this.id, this.number, true)
			clientVM.displayMessage(null, '', {
				msg: this.localize('archivedWorksheetSetActiveSuccess', [this.number, this.id])
			});
			this.loadWorksheet()
		}
	}, */

	editResult: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.ArchivedTaskResult',
			params: {
				id: id,
				activeTab: 1
			}
		})
	},

	jumpToAssignedTo: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.user.User',
			params: {
				username: this.assignedTo
			}
		})
	},
	jumpToAssignedToIsVisible$: function() {
		return this.assignedToName
	},

	assignedToClicked: function() {
		this.open({
			mtype: 'RS.user.UserPicker',
			pick: true,
			callback: this.assignedToSelected
		})
	},

	assignedToClickedClear: function() {
		this.set('assignedTo', '')
		this.set('assignedToName', '')
	},

	assignedToControl: null,
	registerAssignedTo: function(assignedToControl) {
		this.set('assignedToControl', assignedToControl)
		this.assignedToControl.triggerEl.item(1).parent().setDisplayed((this.assignedToName || this.assignedTo) ? 'table-cell' : 'none')
	},

	when_assignedTo_changes_show_or_hide_clear_box: {
		on: ['assignedToChanged', 'assignedToNameChanged'],
		action: function() {
			if (this.assignedToControl) {
				this.assignedToControl.triggerEl.item(1).parent().setDisplayed((this.assignedToName || this.assignedTo) ? 'table-cell' : 'none')
				this.assignedToControl.doComponentLayout()
			}
		}
	},

	assignedToSelected: function(records) {
		var record = records[0]
		this.set('assignedTo', record.get('uuserName'))
		this.set('assignedToName', record.get('udisplayName'))
	},

	all: true,

	showAll: function() {
		Ext.state.Manager.set('RS.worksheet.showAllArchived', this.all);
		this.worksheetResults.load();
	},

	hide: function() {
		this.set('all', false);
		Ext.state.Manager.set('RS.worksheet.showAllArchived', this.all);
		this.worksheetResults.load();
	},

	//Child Viewmodels
	socialDetail: {
		mtype: 'RS.social.Main',
		mode: 'embed',
		initialStreamType: 'worksheet'
	}

});
glu.defModel('RS.worksheet.ArchivedWorksheets', {
	mock: false,

	activate: function() {
		clientVM.setWindowTitle(this.localize('allArchivedWorksheets'))
		this.worksheets.load();
	},

	deactivate: function() {
		// window.name = ''
	},

	displayName: '~~allArchivedWorksheets~~',
	stateId: 'rsarchivedworksheetlist',

	worksheets: {
		mtype: 'store',
		remoteSort: true,
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		fields: [
			'id',
			'number',
			'condition',
			'severity',
			'summary',
			'assignedTo',
			'assignedToName',
			'reference',
			'alertId',
			'correlationId',
			'sirId',
			'sysId', {
				name: 'sysTtl',
				type: 'int'
			}, 'sysOrg'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/worksheet/listArchive',
			reader: {
				type: 'json',
				root: 'records'
			}
		}
	},

	columns: [],

	allSelected: false,
	worksheetsSelections: [],

	init: function() {
		//Called when mocking the backend to test the UI
		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)

		var worksheetsColumns = [{
			header: '~~number~~',
			dataIndex: 'number',
			filterable: true,
			sortable: true
		}, {
			header: '~~condition~~',
			dataIndex: 'condition',
			filterable: true,
			sortable: true,
			width: 90,
			renderer: RS.common.grid.statusRenderer('rs-worksheet-status')
		}, {
			header: '~~severity~~',
			dataIndex: 'severity',
			filterable: true,
			sortable: true,
			width: 90,
			renderer: RS.common.grid.statusRenderer('rs-worksheet-status')
		}, {
			header: '~~summary~~',
			dataIndex: 'summary',
			filterable: true,
			sortable: false,
			flex: 1,
			renderer: RS.common.grid.htmlRenderer()
		}, {
			header: '~~assignedTo~~',
			dataIndex: 'assignedTo',
			filterable: true,
			sortable: true
		}, {
			header: '~~assignedToName~~',
			dataIndex: 'assignedToName',
			filterable: true,
			sortable: true
		}, {
			header: '~~reference~~',
			dataIndex: 'reference',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~alertId~~',
			dataIndex: 'alertId',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~correlationId~~',
			dataIndex: 'correlationId',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.plainRenderer()
		},{
			header: '~~sirId~~',
			dataIndex: 'sirId',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.plainRenderer()
		},{
			header: '~~organization~~',
			dataIndex: 'sysOrg',
			initialShow: true,
			sortable: true,
			renderer: function(value, metaData, record) {
				if (value) {
					if(!record.store.id2NameMap) {
						record.store.id2NameMap = {};
						clientVM.user.orgs.forEach(function(e) {
							record.store.id2NameMap[e.id] = e.uname;
						});
					}
					return RS.common.grid.plainRenderer()(record.store.id2NameMap[value]);
				}
				return '';
			}
		}].concat(RS.common.grid.getSysColumns());

		Ext.each(this.worksheetsColumns, function(col) {
			col.stateId = 'worksheetscolumn_' + col.dataIndex;
		}, this);


		//correct worksheet columns
		var updatedCol, index = -1;
		Ext.Array.each(worksheetsColumns, function(column, idx) {
			if (column.dataIndex == 'sysUpdatedOn') {
				updatedCol = column
				index = idx
			}
		})
		if (updatedCol && index > -1) {
			updatedCol.renderer = Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
			updatedCol.hidden = false
			updatedCol.initialShow = true
			worksheetsColumns.splice(index, 1)
			worksheetsColumns.splice(0, 0, updatedCol)
		}
		this.worksheets.on('beforeload', function(store, operation){
			operation.params = Ext.apply(operation.params || {}, {
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			});
		})
		this.set('columns', worksheetsColumns)

		this.set('displayName', this.localize('allArchivedWorksheets'))
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	editWorksheet: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.ArchivedWorksheet',
			params: {
				id: id,
				activeTab: 1
			}
		})
	},
	singleRecordSelected$: function() {
		return this.worksheetsSelections.length == 1
	},
	editWorksheetIsEnabled$: function() {
		return this.singleRecordSelected
	},
});
glu.defModel('RS.worksheet.ListRegistrationDetails', {
	uname: '',
	ustatus: '',
	uguid: '',
	utype: '',
	uipaddress: '',
	ubuild: '',

	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',

	fields: ['uname',
		'ustatus',
		'uguid',
		'utype',
		'uipaddress',
		'ubuild'
	].concat(RS.common.grid.getSysFields()),

	init: function() {
		this.ajax({
			url: '/resolve/service/registration/list',
			params: {
				filter: Ext.encode([{
					field: 'uguid',
					type: 'auto',
					condition: 'equals',
					value: this.guid
				}])
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					if (response.records && response.records.length > 0) {
						this.loadData(response.records[0])
						this.set('sysCreatedOn', Ext.Date.format(Ext.Date.parse(response.records[0].sysCreatedOn, 'time'), clientVM.getUserDefaultDateFormat()))
						this.set('sysUpdatedOn', Ext.Date.format(Ext.Date.parse(response.records[0].sysUpdatedOn, 'time'), clientVM.getUserDefaultDateFormat()))
					}
				} else
					clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	}
})
glu.defModel('RS.worksheet.ProcessRequest', {
	mock: false,

	activate: function(screen, params) {
		this.resetForm()
		this.set('id', params ? params.id : '')
		this.loadProcessRequest()
	},

	resetForm: function() {
		this.loadData(this.defaultData)
	},

	isArhive: false,

	fields: [
		'id',
		'number',
		'processRequest',
		'status',
		'problemNumber',
		'worksheetId',
		'wiki',
		'duration',
		'created',
		'timeout'
	].concat(RS.common.grid.getSysFields()),

	defaultData: {},

	id: '',
	number: '',
	processRequest: '',
	status: '',
	problemNumber: '',
	worksheetId: '',
	wiki: '',
	duration: '',
	timeout: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrg: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},
	processRequestTitle$: function() {
		return this.localize('processRequestTitle') + (this.processRequest ? ' - ' + this.processRequest : '')
	},

	init: function() {
		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)
	},

	loadProcessRequest: function() {
		if (this.id)
			this.ajax({
				url: '/resolve/service/processrequest/get',
				params: {
					id: this.id,
					isArhive: this.isArhive
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) this.loadData(response.data)
					else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.ProcessRequests'
		})
	},

	abort: function() {
		this.message({
			title: this.localize('abortTitle'),
			msg: this.localize('abortMessage'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('abortAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyAbortRecords
		})
	},

	reallyAbortRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			this.ajax({
				url: '/resolve/service/processrequest/abort',
				params: {
					id: [this.id]
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) this.loadProcessRequest()
					else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
	},

	stateId: 'processRequestTaskRequestsList',

	jumpToWorksheet: function() {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.Worksheet',
			params: {
				id: this.worksheetId
			}
		})
	},
	jumpToWorksheetIsVisible$: function() {
		return this.worksheetId
	}
})
glu.defModel('RS.worksheet.ProcessRequests', {
	mock: false,

	displayName: 'processRequests',
	stateId: 'processRequestList',

	activate: function() {
		//Load the processRequests from the server
		this.processRequests.load();
	},

	processRequests: {
		mtype: 'store',
		remoteSort: true,
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		fields: ['id', 'number', 'status', 'problemNumber', 'problem', 'wiki', 'duration', 'sysOrg'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/processrequest/list',
			reader: {
				type: 'json',
				root: 'records'
			}
		}
	},

	columns: [],

	allSelected: false,
	gridSelections: [],
	processRequestAbortAPI : '/resolve/service/processrequest/abort',
	processRequestDeleteAPI : '/resolve/service/processrequest/delete',

	init: function() {
		//Called when mocking the backend to test the UI
		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)

		this.set('columns', [{
			header: '~~processRequest~~',
			dataIndex: 'number',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.internalLinkRenderer('RS.worksheet.ProcessRequest', 'id', 'id')
		}, {
			header: '~~status~~',
			dataIndex: 'status',
			filterable: true,
			sortable: true
		}, {
			header: '~~problemNumber~~',
			dataIndex: 'problemNumber',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.internalLinkRenderer('RS.worksheet.Worksheet', 'id', 'problem')
		}, {
			header: '~~wiki~~',
			dataIndex: 'wiki',
			filterable: true,
			sortable: true
		}, {
			header: '~~duration~~',
			dataIndex: 'duration',
			filterable: true,
			sortable: true
		},{
			header: '~~organization~~',
			dataIndex: 'sysOrg',
			initialShow: true,
			sortable: false,
			renderer: function(value, metaData, record) {
				if (value) {
					if(!record.store.id2NameMap) {
						record.store.id2NameMap = {};
						clientVM.user.orgs.forEach(function(e) {
							if(e.uname != 'None')
								record.store.id2NameMap[e.id] = e.uname;
						});
					}
					return RS.common.grid.plainRenderer() (record.store.id2NameMap[value]);
				}
				return '';
			}
		}].concat(RS.common.grid.getSysColumns()))

		this.processRequests.on('beforeload', function(store, operation, eOpts) {
			this.addKeywordToSorterProp(operation.sorters);
		}, this)

		this.set('displayName', this.localize('processRequests'))
	},
	addKeywordToSorterProp: function(sorters) {
		for(var i=0; i<sorters.length; i++) {
			var curSorter = sorters[i];
			if (curSorter.property.indexOf('.keyword') == -1) {// iif '.keyword' has not already been appended
				switch (curSorter.property) {
				case 'number':
				case 'status':
				case 'problemNumber':
				case 'wiki':
					curSorter.property += '.keyword';
					break;
				default:
					// do nothing
					break;
				}
			}
		}
	},
	editProcessRequest: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.ProcessRequest',
			params: {
				id: id
			}
		})
	},
	editProcessRequestIsEnabled$: function() {
		return this.gridSelections.length == 1
	},
	findValueForFilter : function(filter){
		var regexPattern = new RegExp(filter + " = '([^']*)'");
		var matches = regexPattern.exec(this.processRequests.lastWhereClause);
		return matches ? matches[1] : null;
	},
	abortProcessRequest: function() {
		var title = this.localize('abortTitle');
		var msg = this.localize(this.gridSelections.length == 1 ? 'abortMessage' : 'abortsMessage', this.gridSelections.length);
		if(this.abortAll){
			title = this.localize('abortAllTitle');
			var status = this.findValueForFilter('status') || '';
			msg = this.localize('abortAllMessage', [this.processRequests.totalCount, status]);
		}
		this.message({
			title: title,
			msg : msg,
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('abortAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyAbortRecords
		})
	},
	abortProcessRequestIsEnabled$: function() {
		return this.gridSelections.length > 0 || this.allSelected
	},
	abortAll : false,
	abortAllProcessRequest : function(){
		this.set('abortAll', true);
		this.abortProcessRequest();
	},
	reallyAbortRecords: function(button) {
		if (button === 'yes') {
			//Abort the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			var status = this.findValueForFilter('status') || 'ALL'
			var ids = [];
			if (this.abortAll) {
				ids.push('All');
			}
			else{
				Ext.each(this.gridSelections, function(selection) {
					ids.push(selection.get('id'))
				})
			}
			this.ajax({
				url: this.processRequestAbortAPI,
				params: {
					ids: ids,
					status : status
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) this.processRequests.load()
					else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
		this.set('abortAll', false);
	},

	deleteProcessRequest: function() {
		//Delete the selected record(s), but first confirm the action
		var title = this.localize('deleteTitle');
		var msg = this.localize(this.gridSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.gridSelections.length);
		if(this.deleteAll){
			title = this.localize('deleteAllTitle');
			var status = this.findValueForFilter('status') || '';
			msg = this.localize('deleteAllMessage', [this.processRequests.totalCount, status]);
		}
		this.message({
			title: title,
			msg: msg,
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	deleteProcessRequestIsEnabled$: function() {
		return this.gridSelections.length > 0 || this.allSelected
	},
	deleteAll : false,
	deleteAllProcessRequest: function() {
		this.set('deleteAll', true);
		this.deleteProcessRequest();
	},
	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			var status = this.findValueForFilter('status') || 'ALL'
			var ids = [];
			if (this.deleteAll) {
				ids.push('All');
			}
			else{
				Ext.each(this.gridSelections, function(selection) {
					ids.push(selection.get('id'))
				})
			}
			this.ajax({
				url: this.processRequestDeleteAPI,
				params: {
					ids: ids.join(','),
					status : status
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) this.processRequests.load()
					else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
		this.set('deleteAll', false);
	}
})
glu.defModel('RS.worksheet.TaskRequest', {

	activate: function(screen, params) {
		this.initToken();
		this.resetForm()
		this.set('id', params ? params.id : '')
		this.loadTaskRequest()
	},

	initToken: function() {
		this.set('csrftoken', clientVM.rsclientToken);
	},

	resetForm: function() {
		this.loadData(this.defaultData)
	},

	fields: [
		'id',
		'created',
		'taskRequest',
		'status',
		'worksheet',
		'worksheetId',
		'processRequest',
		'processRequestId',
		'actionTask',
		'actionTaskId',
		'duration',
		'runbook',
		'createdBy',
		'lastUpdated',
		'lastUpdatedBy',
		'nodeId'
	],

	defaultData: {},

	id: '',
	taskRequest: '',
	status: '',
	worksheet: '',
	worksheetId: '',
	processRequest: '',
	processRequestId: '',
	actionTask: '',
	actionTaskId: '',
	duration: '',
	runbook: '',
	created: '',
	createdText$: function() {
		return this.created ? Ext.Date.format(Ext.Date.parse(this.created, 'c'), clientVM.getUserDefaultDateFormat()) : ''
	},
	createdBy: '',
	updated: '',
	updatedBy: '',
	sysOrg: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},
	lastUpdatedText$: function() {
		return this.lastUpdated ? Ext.Date.format(Ext.Date.parse(this.lastUpdated, 'c'), clientVM.getUserDefaultDateFormat()) : ''
	},
	nodeId: '',

	taskRequestTitle$: function() {
		return this.localize('taskRequestTitle') + (this.taskRequest ? ' - ' + this.taskRequest : '')
	},

	csrftoken: '',

	init: function() {
		this.initToken();
		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)
	},

	loadTaskRequest: function() {
		if (this.id)
			this.ajax({
				url: '/resolve/service/taskrequest/get',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) this.loadData(response.data)
					else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.TaskRequests'
		})
	},
	backIsVisible$: function() {
		return clientVM.screens.length > 1
	},

	abort: function() {
		this.message({
			title: this.localize('abortTitle'),
			msg: this.localize('abortMessage'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('abortAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyAbortRecords
		})
	},

	reallyAbortRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			this.ajax({
				url: '/resolve/service/taskrequest/abort',
				params: {
					taskRequestIds: [this.id]
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) this.loadTaskRequest()
					else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
	},

	worksheetLink$: function() {
		return '/resolve/client/rsclient.jsp?' + this.csrftoken + '&debug=true&mock=true#RS.worksheet.Worksheet/id=' + this.worksheetId
	},
	jumpToWorksheet: function() {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.Worksheet',
			params: {
				id: this.worksheetId
			}
		})
	},
	jumpToWorksheetIsVisible$: function() {
		return this.worksheetId
	},

	jumpToActionTask: function() {
		clientVM.handleNavigation({
			modelName: 'RS.actionTask.ActionTask',
			params: {
				id: this.actionTaskId
			}
		})
	},
	jumpToActionTaskIsVisible$: function() {
		return this.actionTaskId
	},

	jumpToProcessRequest: function() {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.ProcessRequest',
			params: {
				id: this.processRequestId
			}
		})
	},
	jumpToProcessRequestIsVisible$: function() {
		return this.processRequestId
	}
})
glu.defModel('RS.worksheet.TaskRequests', {
	mock: false,

	displayName: 'taskRequests',
	stateId: 'taskRequestList',

	activate: function() {

	},

	taskRequests: {
		mtype: 'store',
		remoteSort: true,
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		fields: ['id', 'taskRequest', 'status', 'worksheet', 'processRequest', 'actionTask', 'duration'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/taskrequest/list',
			reader: {
				type: 'json',
				root: 'records'
			}
		}
	},

	columns: [],

	allSelected: false,
	gridSelections: [],

	init: function() {
		//Called when mocking the backend to test the UI
		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)

		this.set('columns', [{
			header: '~~taskRequest~~',
			dataIndex: 'taskRequest',
			filterable: true,
			sortable: true
		}, {
			header: '~~status~~',
			dataIndex: 'status',
			filterable: true,
			sortable: true
		}, {
			header: '~~worksheet~~',
			dataIndex: 'worksheet',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.internalLinkRenderer('RS.worksheet.Worksheet', 'id', 'worksheetId')
		}, {
			header: '~~processRequest~~',
			dataIndex: 'processRequest',
			filterable: true,
			sortable: true
		}, {
			header: '~~actionTask~~',
			dataIndex: 'actionTask',
			filterable: true,
			sortable: true
		}, {
			header: '~~duration~~',
			dataIndex: 'duration',
			filterable: true,
			sortable: true
		}].concat(RS.common.grid.getSysColumns()))

		this.set('displayName', this.localize('taskRequests'))

		//Load the taskRequests from the server
		this.taskRequests.load()
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	editTaskRequest: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.TaskRequest',
			params: {
				id: id
			}
		})
	},
	editTaskRequestIsEnabled$: function() {
		return this.gridSelections.length == 1
	},

	abortTaskRequest: function() {
		this.message({
			title: this.localize('abortTitle'),
			msg: this.localize(this.gridSelections.length == 1 ? 'abortMessage' : 'abortsMessage', this.gridSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('abortAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyAbortRecords
		})
	},
	abortTaskRequestIsEnabled$: function() {
		return this.gridSelections.length > 0 || this.allSelected
	},

	reallyAbortRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/taskrequest/abort',
					params: {
						ids: '',
						whereClause: this.taskRequests.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.taskRequests.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.gridSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/taskrequest/abort',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.taskRequests.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	},

	deleteTaskRequest: function() {
		//Delete the selected record(s), but first confirm the action
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.gridSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.gridSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	deleteTaskRequestIsEnabled$: function() {
		return this.gridSelections.length > 0 || this.allSelected
	},

	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/taskrequest/delete',
					params: {
						ids: '',
						whereClause: this.taskRequests.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.taskRequests.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.gridSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/taskrequest/delete',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.taskRequests.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	}
})
glu.defModel('RS.worksheet.TaskResult', {

	mock: false,

	activate: function(screen, params) {
		this.resetForm()
		this.set('id', params ? params.id : '')
		if (params && Ext.isDefined(params.activeTab) && Ext.isNumber(Number(params.activeTab))) this.set('activeTab', Number(params.activeTab))
		this.loadTaskResult()
	},

	resetForm: function() {
		this.loadData(this.defaultData)
	},

	fields: ['id',
		'sysId',
		'actionTask',
		'actionTaskFullName',
		'actionTaskId',
		'taskName',
		'taskId',
		'completion',
		'condition',
		'severity',
		'duration',
		'address',
		'summary',
		'updatedBy',
		'targetGUID',
		'summary',
		'raw',
		'esbAddr',
		'executeRequestId',
		'executeRequestNumber',
		'executeResultId',
		'problemId',
		'problemNumber',
		'processId',
		'processNumber',
		'actionTaskName',
		'actionTaskFullName',
		'taskNamespace',
		'detail',
		'timestamp'
	].concat(RS.common.grid.getSysFields()),

	defaultData: {
		id: '',
		sysId: '',
		actionTask: '',
		actionTaskFullName: '',
		taskNamespace: '',
		actionTaskId: '',
		taskName: '',
		taskId: '',
		completion: '',
		condition: '',
		severity: '',
		duration: '',
		address: '',
		summary: '',
		targetGUID: '',
		summary: '',
		raw: '',
		esbAddr: '',
		executeRequestId: '',
		executeRequestNumber: '',
		executeResultId: '',
		problemId: '',
		problemNumber: '',
		processId: '',
		processNumber: '',
		actionTaskName: '',
		actionTaskFullName: '',
		detail: '',
		timestamp: '',

		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrg: ''
	},

	id: '',
	actionTask: '',
	actionTaskId: '',
	actionTaskFullName: '',
	taskNamespace: '',
	taskName: '',
	taskId: '',
	completion: '',
	condition: '',
	severity: '',
	duration: '',
	address: '',
	summary: '',
	summaryDisplay$: function() {
		return this.formatHtmlString(this.summary)
	},
	targetGUID: '',
	summary: '',
	raw: '',
	rawText$: function() {
		return this.formatHtmlString(this.raw)
	},
	esbAddr: '',
	executeRequestId: '',
	executeRequestNumber: '',
	executeResultId: '',
	problemId: '',
	problemNumber: '',
	processId: '',
	processNumber: '',
	actionTaskName: '',
	actionTaskFullName: '',
	detail: '',
	detailDisplay$: function() {
		return this.formatHtmlString(this.detail)
	},

	formatHtmlString: function(value) {
		if (value.indexOf('<!-- DETAIL_TYPE_HTML -->') == 0 || value.indexOf('<!-- UI_TYPE_HTML -->') == 0) {
			return Ext.util.Format.nl2br('<pre>' + clientVM.injectRsclientToken(value) + '</pre>')
		}
		return Ext.util.Format.nl2br('<pre>' + value + '</pre>')
	},
	timestamp: '',

	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrg: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	taskResultTitle$: function() {
		return this.localize('taskResultTitle') + (this.taskName ? ' - ' + this.taskName : '') + (this.taskNamespace ? '#' + this.taskNamespace : '')
	},

	init: function() {
		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)
	},

	loadTaskResult: function() {
		if (this.id)
			this.ajax({
				url: '/resolve/service/taskresult/get',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) this.loadData(response.data)
					else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
	},

	back: function() {
		clientVM.handleNavigationBack()
	},
	backIsVisible$: function() {
		return clientVM.screens.length > 1
	},

	activeTab: 0,

	generalTab: function() {
		this.set('activeTab', 0)
	},
	generalTabIsPressed$: function() {
		return this.activeTab == 0
	},
	detailTab: function() {
		this.set('activeTab', 1)
	},
	detailTabIsPressed$: function() {
		return this.activeTab == 1
	},
	rawTab: function() {
		this.set('activeTab', 2)
	},
	rawTabIsPressed$: function() {
		return this.activeTab == 2
	},

	jumpToWorksheet: function() {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.Worksheet',
			params: {
				id: this.problemId
			}
		})
	},
	jumpToWorksheetIsVisible$: function() {
		return this.problemId
	},

	jumpToActionTask: function() {
		clientVM.handleNavigation({
			modelName: 'RS.actiontask.ActionTask',
			params: {
				id: this.taskId
			}
		})
	},
	jumpToActionTaskIsVisible$: function() {
		return this.taskId
	},
	jumpToProcessRequest: function() {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.ProcessRequest',
			params: {
				id: this.processId
			}
		})
	},
	jumpToProcessRequestIsVisible$: function() {
		return this.processId
	},

	showGUIDDetails$: function() {
		return this.targetGUID
	},
	displayGUIDDetails: function() {
		this.open({
			mtype: 'RS.worksheet.ListRegistrationDetails',
			guid: this.targetGUID
		})
	}
})
glu.defModel('RS.worksheet.TaskResults', {
	mock: false,

	displayName: 'taskResults',
	stateId: 'taskResultList',

	activate: function() {},

	taskResults: {
		mtype: 'store',
		remoteSort: true,
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		fields: ['id', 'sysId', 'taskName', 'taskId', 'completion', 'condition', 'severity', 'duration', 'problemNumber', 'problemId', 'processNumber', 'processId', 'summary', 'sysOrg'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/taskresult/list',
			reader: {
				type: 'json',
				root: 'records'
			}
		}
	},

	columns: [],

	allSelected: false,
	gridSelections: [],

	init: function() {
		//Called when mocking the backend to test the UI
		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)

		var taskResultsColumns = [{
			header: '~~actionTask~~',
			dataIndex: 'taskName',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.internalLinkRenderer('RS.actiontask.ActionTask', 'id', 'taskId')
		}, {
			header: '~~completion~~',
			dataIndex: 'completion',
			filterable: true,
			width: 90,
			sortable: true
		}, {
			header: '~~condition~~',
			dataIndex: 'condition',
			filterable: true,
			sortable: true,
			width: 90,
			renderer: RS.common.grid.statusRenderer('rs-worksheet-status')
		}, {
			header: '~~severity~~',
			dataIndex: 'severity',
			filterable: true,
			sortable: true,
			width: 90,
			renderer: RS.common.grid.statusRenderer('rs-worksheet-status')
		}, {
			header: '~~duration~~',
			dataIndex: 'duration',
			filterable: true,
			width: 90,
			sortable: true
		}, {
			header: '~~worksheet~~',
			dataIndex: 'problemNumber',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.internalLinkRenderer('RS.worksheet.Worksheet', 'id', 'problemId')
		}, {
			header: '~~processRequest~~',
			dataIndex: 'processNumber',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.internalLinkRenderer('RS.worksheet.ProcessRequest', 'id', 'processId')
		}, {
			header: '~~summary~~',
			dataIndex: 'summary',
			filterable: false,
			sortable: false,
			flex: 1,
			renderer: function(value) {
				if (value.indexOf('<!-- DETAIL_TYPE_HTML -->') == 0 || value.indexOf('<!-- UI_TYPE_HTML -->') == 0) {
					return Ext.util.Format.nl2br('<pre>' + clientVM.injectRsclientToken(value) + '</pre>')
				}
				return Ext.util.Format.nl2br('<pre>' + Ext.String.htmlEncode(value) + '</pre>')
			}
		},{
			header: '~~organization~~',
			dataIndex: 'sysOrg',
			initialShow: true,
			sortable: false,
			renderer: function(value, metaData, record) {
				if (value) {
					if(!record.store.id2NameMap) {
						record.store.id2NameMap = {};
						clientVM.user.orgs.forEach(function(e) {
							if(e.uname != 'None')
								record.store.id2NameMap[e.id] = e.uname;
						});
					}
					return RS.common.grid.plainRenderer() (record.store.id2NameMap[value]);
				}
				return '';
			}
		}].concat(RS.common.grid.getSysColumns())

		//correct worksheet columns
		var updatedCol, index = -1;
		Ext.Array.each(taskResultsColumns, function(column, idx) {
			if (column.dataIndex == 'sysUpdatedOn') {
				updatedCol = column
				index = idx
			}
		})
		if (updatedCol && index > -1) {
			updatedCol.renderer = Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
			updatedCol.hidden = false
			updatedCol.initialShow = true
			taskResultsColumns.splice(index, 1)
			taskResultsColumns.splice(0, 0, updatedCol)
		}
		this.set('columns', taskResultsColumns)

		this.set('displayName', this.localize('taskResults'))

		this.taskResults.on('beforeload', function(store, operation, eOpts) {
			this.addRawToSorterProp(operation.sorters);
		}, this)

		//Load the taskResults from the server
		this.taskResults.load()
	},

	addRawToSorterProp: function(sorters) {
		// This method is needed after upgrading ES to 5.x. After migration, 'text' field was changed to raw 'keyword'
		// so we need to apply '.raw' to fields that were changed to raw 'keyword'.
		// FYI, sorting on 'GUID' isn't working because the field has been properly migrated. We need to fix sorting
		// on 'GUID' accordingly after the BE correct the migration.
		for(var i=0; i<sorters.length; i++) {
			var curSorter = sorters[i];
			if (curSorter.property.indexOf('.raw') == -1) {// iif '.raw' has not already been appended
				switch (curSorter.property) {
				case 'taskName':
				case 'duration':
				case 'targetGUID':
				case 'sysCreatedBy':
				case 'sysCreatedOn':
				case 'sysId':
				case 'sysUpdatedOn':
				case 'sysUpdatedBy':
				case 'detail':
					// do nothing
					break;
				default:
					curSorter.property += '.raw';
					break;
				}
			}
		}
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	editTaskResult: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.TaskResult',
			params: {
				id: id
			}
		})
	},
	editTaskResultIsEnabled$: function() {
		return this.gridSelections.length == 1
	},

	deleteTaskResult: function() {
		//Delete the selected record(s), but first confirm the action
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.gridSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.gridSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	deleteTaskResultIsEnabled$: function() {
		return this.gridSelections.length > 0 || this.allSelected
	},

	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/taskresult/delete',
					params: {
						ids: '',
						whereClause: this.taskResults.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.taskResults.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.gridSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/taskresult/delete',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.taskResults.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	}
})
glu.defModel('RS.worksheet.Worksheet', {
	mock: false,
	API : {
		setActive : '/resolve/service/worksheet/setActive',
		getActive : '/resolve/service/worksheet/getActive',
		saveWS : '/resolve/service/worksheet/save',
		deleteWS : '/resolve/service/wsdata/delete',
		getWSData : '/resolve/service/wsdata/getMap',
		getNextNumber : '/resolve/service/worksheet/getNextNumber'
	},
	activate: function(screen, params) {
 		this.initToken();
		clientVM.setWindowTitle(this.localize('worksheet'))
		this.set('all', Ext.state.Manager.get('RS.worksheet.showAll'));
		this.set('activeTab', 1)
		if (params && Ext.isDefined(params.activeTab) && Ext.isNumber(Number(params.activeTab))) this.set('activeTab', Number(params.activeTab))
		this.resetForm()
		if (clientVM.fromArchive && !(params || {}).id) {
			history.replaceState({}, '', '/resolve/jsp/rsclient.jsp?' + this.csrftoken + '#RS.worksheet.ArchivedWorksheet')
			window.location.href = '/resolve/jsp/rsclient.jsp?' +this.csrftoken + '#RS.worksheet.ArchivedWorksheet';
			return;
		}
		this.set('id', params ? params.id : clientVM.problemId);
		this.processWorksheetPage(this.id);
		if (params && params.setActive === 'true')
			this.reallySetActive('yes');
		else
			this.loadWorksheet();
		if (params && params.number)
			this.set('number', params.number);
		// Reset the grid store on new worksheet
		(!this.id && this.worksheetResults) ? this.worksheetResults.removeAll(): null;
	},

	initToken: function() {
		this.set('csrftoken', clientVM.rsclientToken);
	},

	//This method is used to keep track of current page for each different worksheet.
	worksheetPaginationMap : [],
	processWorksheetPage : function(worksheetID){
		var found = false;
		var wsPage = '';
		for(var i = 0; i < this.worksheetPaginationMap.length; i++){
			var ws = this.worksheetPaginationMap[i];
			if(worksheetID == ws.id){
				found = true;
				wsPage = ws.page;
				break;
			}
		}
		if(found){
			this.worksheetResults.currentPage = wsPage;
		}
		else
		{
			this.worksheetPaginationMap.push({
				id : worksheetID,
				page : 1
			})
			this.worksheetResults.currentPage = 1;
		}
	},
	updateWorksheetPageMap : function(worksheetID, currentPage){
		for(var i = 0; i < this.worksheetPaginationMap.length; i++){
			var ws = this.worksheetPaginationMap[i];
			if(worksheetID == ws.id){
				ws.page = currentPage;
				break;
			}
		}
	},
	socialIsPressed: false,
	socialPressedCls$: function() {
		return this.socialIsPressed ? 'rs-social-button-pressed icon-comment' : 'icon-comment-alt'
	},

	when_socialIsPressed_changes_store_state: {
		on: ['socialIsPressedChanged'],
		action: function() {
			Ext.state.Manager.set('worksheetSocialIsPressed', this.socialIsPressed)
		}
	},

	worksheetTitle$: function() {
		return this.localize('worksheet') + (this.number ? ' - ' + this.number : '')
	},

	stateId: 'worksheetResultList',

	activeTab: 0,

	isResolveDev$: function() {
		return hasPermission(['resolve_dev', 'resolve_process'])
	},

	generalTab: function() {
		this.set('activeTab', 0)
	},
	generalTabIsPressed$: function() {
		return this.activeTab == 0
	},
	// generalTabIsVisible$: function() {
	// 	return this.isResolveDev
	// },
	resultsTab: function() {
		this.set('activeTab', 1)
	},
	resultsTabIsPressed$: function() {
		return this.activeTab == 1
	},
	// resultsTabIsVisible$: function() {
	// 	return this.isResolveDev
	// },
	debugTab: function() {
		this.set('activeTab', 2)
	},
	debugTabIsPressed$: function() {
		return this.activeTab == 2
	},
	// debugTabIsVisible$: function() {
	// 	return this.isResolveDev
	// },
	dataTab: function() {
		this.set('activeTab', 3)
	},
	dataTabIsPressed$: function() {
		return this.activeTab == 3
	},
	// dataTabIsVisible$: function() {
	// 	return this.isResolveDev
	// },

	displayFilter$: function() {
		return this.resultsTabIsPressed
	},

	resetForm: function() {
		this.loadData(this.defaultData)
	},


	fields: ['number', 'alertId', 'reference', 'correlationId', 'sirId','assignedTo', 'assignedToName', 'summary', 'condition', 'severity', 'description', 'workNotes', 'debug'].concat(RS.common.grid.getSysFields()),
	id: '',
	streamType: 'worksheet',
	number: '',
	alertId: '',
	reference: '',
	correlationId: '',
	assignedTo: '',
	assignedToName: '',
	summary: '',
	condition: '',
	severity: '',
	description: '',
	workNotes: '',
	workNotesData: '',
	debug: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrg: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	defaultData: {
		id: '',
		number: '',
		alertId: '',
		reference: '',
		correlationId: '',
		assignedTo: '',
		assignedToName: '',
		summary: '',
		condition: '',
		severity: '',
		description: '',
		workNotes: '',
		workNotesData: '',
		debug: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrg: ''
	},

	worksheetResults: {
		mtype: 'store',
		remoteSort: true,
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'ASC'
		}],
		fields: ['id', 'address',
			'completion',
			'condition',
			'esbaddr',
			'severity',
			'executeRequestId',
			'executeRequestNumber',
			'executeResultId',
			'problem',
			'process',
			'processNumber',
			'processId',
			'targetGUID',
			'actionTask',
			'actionTaskName',
			'actionTaskFullName',
			'actionResultId',
			'detail',
			'taskName',
			'taskId',
			'summary', 'sysId', {
				name: 'duration',
				type: 'int'
			}
		].concat(RS.common.grid.getSysFields()
		),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/worksheet/results',
			reader: {
				type: 'json',
				root: 'records'
			}
		}
	},

	worksheetColumns: [{
		text: '~~actionTask~~',
		dataIndex: 'taskName',
		// renderer: RS.common.grid.linkRenderer('RS.actiontask.ActionTask', 'taskId', '_blank'),
		renderer: function(value, meta, record) {
			if (value)
				return Ext.String.format('<a target="_blank" class="rs-link" href="/resolve/jsp/rsclient.jsp?{0}#RS.actiontask.ActionTask/id={1}">{2}</a>', this.csrftoken, record.get('taskId'), value)
			return ''
		},
		sortable: true,
		filterable: true
	}, {
		text: '~~completion~~',
		dataIndex: 'completion',
		width: 75,
		sortable: true,
		filterable: true
	}, {
		text: '~~condition~~',
		dataIndex: 'condition',
		renderer: RS.common.grid.statusRenderer('rs-worksheet-status'),
		width: 90,
		sortable: true,
		filterable: true
	}, {
		text: '~~severity~~',
		dataIndex: 'severity',
		renderer: RS.common.grid.statusRenderer('rs-worksheet-status'),
		width: 90,
		sortable: true,
		filterable: true
	}, {
		text: '~~summary~~',
		dataIndex: 'summary',
		flex: 1,
		sortable: false,
		filterable: true,
		renderer: function(value) {
			if (value.indexOf('<!-- DETAIL_TYPE_HTML -->') == 0 || value.indexOf('<!-- UI_TYPE_HTML -->') == 0) {
				return Ext.util.Format.nl2br('<pre>' + clientVM.injectRsclientToken(value) + '</pre>')
			}
			return Ext.util.Format.nl2br('<pre>' + Ext.String.htmlEncode(value) + '</pre>')
		}
	}, {
		text: '~~processRequest~~',
		dataIndex: 'processNumber',
		// renderer: RS.common.grid.internalLinkRenderer('RS.worksheet.ProcessRequest', 'id', 'processId'),
		renderer: function(value, meta, record) {
			if (value)
				return Ext.String.format('<a target="_blank" class="rs-link" href="/resolve/jsp/rsclient.jsp?{0}#RS.worksheet.ProcessRequest/id={1}">{2}</a>', this.csrftoken, record.get('processId'), value)
			return ''
		},
		sortable: true,
		filterable: true,
		hidden: true
	}, {
		text: '~~duration~~',
		dataIndex: 'duration',
		width: 60,
		sortable: true,
		filterable: true,
		hidden: true
	}, {
		text: '~~target~~',
		dataIndex: 'address',
		sortable: true,
		filterable: true,
		hidden: true
	}, {
		text: '~~guid~~',
		dataIndex: 'targetGUID',
		sortable: false,
		filterable: true,
		hidden: true
	}, {
		text: '~~detail~~',
		dataIndex: 'detail',
		hidden: true,
		hidable: false,
		sortable: false,
		filterable: true
	}, {
		text: '~~queueName~~',
		dataIndex: 'esbaddr',
		sortable: true,
		filterable: true,
		hidden: true
	}].concat(
			(function () {
				var commonCols = RS.common.grid.getSysColumns();
				var index = -1;

				for(var i = 0; i < commonCols.length; i++) {
					if(commonCols[i].dataIndex === 'sysCreatedOn') {
						index = i;
						break;
					}
				}

				if(index !== -1) {
					commonCols[index].hidden = false;
					commonCols[index].initialShow = true;
					commonCols[index].flex = 1;
				}

				return commonCols;
			})()
			),

	wsColumns: [{
		text: '~~name~~',
		dataIndex: 'name',
		flex: 1
	}, {
		text: '~~value~~',
		dataIndex: 'value',
		flex: 1
	}],

	wsData: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	wsDataSelections: [],

	conditionStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	severityStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	workNotesDataStore: {
		mtype: 'store',
		fields: [{
				type: 'date',
				name: 'createDate',
				format: 'time',
				dateFormat: 'time'
			},
			'userid',
			'worknotesDetail'
		],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	workNotesColumns: [],

	csrftoken: '',

	init: function() {
		this.initToken();

		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)

		if (Ext.isString(this.activeTab)) this.set('activeTab', Number(this.activeTab))

		Ext.Array.forEach(this.worksheetColumns, function(col) {
			if (col.dataIndex == 'sys_id') col.dataIndex = 'sysId'
		})

		this.set('workNotesColumns', [{
			text: '~~sysCreatedOn~~',
			dataIndex: 'createDate',
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat()),
			width: 200
		}, {
			text: '~~sysCreatedBy~~',
			dataIndex: 'userid'
		}, {
			text: '~~note~~',
			dataIndex: 'worknotesDetail',
			flex: 1,
			renderer: function(value) {
				return Ext.util.Format.nl2br('<pre style="word-wrap:break-word;">' + Ext.util.Format.nl2br(Ext.String.htmlEncode(value)) + '</pre>')
			}
		}])

		//correct worksheet columns
		// var updatedCol, index = -1;
		// Ext.Array.each(this.worksheetColumns, function(column, idx) {
		// 		if (column.dataIndex == 'sysUpdatedOn') {
		// 			updatedCol = column
		// 			index = idx
		// 			return false
		// 		}
		// 	})
		// if (updatedCol && index > -1) {
		// 	updatedCol.renderer = Ext.util.Format.dateRenderer('H:i:s.u M j')
		// 	updatedCol.hidden = false
		// 	updatedCol.initialShow = true
		// 	this.worksheetColumns.splice(index, 1)
		// 	this.worksheetColumns.splice(0, 0, updatedCol)
		// }

		this.set('worksheetColumns', Ext.Array.filter(this.worksheetColumns, function(column) {
			return column.dataIndex != 'sysUpdatedOn' && column.dataIndex != 'sysUpdatedBy';
		}));
		//Populate combobox stores
		this.conditionStore.add([{
			name: this.localize('good'),
			value: 'GOOD'
		}, {
			name: this.localize('bad'),
			value: 'BAD'
		}, {
			name: this.localize('unknown'),
			value: 'UNKNOWN'
		}])

		this.severityStore.add([{
			name: this.localize('critical'),
			value: 'CRITICAL'
		}, {
			name: this.localize('severe'),
			value: 'SEVERE'
		}, {
			name: this.localize('warning'),
			value: 'WARNING'
		}, {
			name: this.localize('good'),
			value: 'GOOD'
		}])
		this.worksheetResults.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {}
			operation.params.id = this.id;
			operation.params.hidden = !this.all;
			this.addRawToSorterProp(operation.sorters);
			this.updateWorksheetPageMap(this.id, store.currentPage);
		}, this)
		this.set('socialIsPressed', Ext.state.Manager.get('worksheetSocialIsPressed', false))
	},
	addRawToSorterProp: function(sorters) {
		// This method is needed after upgrading ES to 5.x. After migration, 'text' field was changed to raw 'keyword'
		// so we need to apply '.raw' to fields that were changed to raw 'keyword'.
		// FYI, sorting on 'GUID' isn't working because the field has been properly migrated. We need to fix sorting
		// on 'GUID' accordingly after the BE correct the migration.
		for(var i=0; i<sorters.length; i++) {
			var curSorter = sorters[i];
			if (curSorter.property.indexOf('.raw') == -1) {// iif '.raw' has not already been appended
				switch (curSorter.property) {
				case 'taskName':
				case 'duration':
				case 'targetGUID':
				case 'sysCreatedBy':
				case 'sysCreatedOn':
				case 'sysId':
				case 'sysUpdatedOn':
				case 'sysUpdatedBy':
				case 'detail':
					// do nothing
					break;
				default:
					curSorter.property += '.raw';
					break;
				}
			}
		}
	},
	loadActiveWorksheet: function() {
		this.ajax({
			url : this.API['getActive'],
			method : 'GET',
			params : {
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.loadData(response.data)
					this.socialDetail.set('initialStreamId', response.data.id)
					this.socialDetail.set('streamParent', response.data.number)
					this.set('workNotesData', this.workNotes)
					this.set('workNotes', '')
					var records = this.workNotesData ? Ext.decode(this.workNotesData) : [] || [];
					this.workNotesDataStore.add(records)

					this.worksheetResults.load()

					this.ajax({
						url: this.API['getWSData'],
						params: {
							problemId: this.id
						},
						success: function(r) {
							var response = RS.common.parsePayload(r)
							if (response.success) {
								var data = []
								Ext.Object.each(response.data, function(key) {
									data.push({
										name: key,
										value: response.data[key]
									})
								})
								this.wsData.add(data)
							} else clientVM.displayError(response.message)
						},
						failure: function(resp) {
							clientVM.displayFailure(resp);
						}
					})
				} else clientVM.displayError(response.message)
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	populateWorksheetWithResponseData: function(data, removeAll) {
		if (removeAll) {
			this.wsData.removeAll()
			this.workNotesDataStore.removeAll()
		}
		this.loadData(data)
		this.socialDetail.set('initialStreamId', data.id)
		this.socialDetail.set('streamParent', data.number)
		this.set('workNotesData', this.workNotes)
		this.set('workNotes', '')
		var records = this.workNotesData ? Ext.decode(this.workNotesData) : [] || [];
		this.workNotesDataStore.add(records)
	},

	loadWorksheetWithId: function() {
		this.worksheetResults.load();
		this.ajax({
			url: '/resolve/service/worksheet/getWS',
			params: {
				id: this.id
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.populateWorksheetWithResponseData(response.data)
				} else clientVM.displayError(response.message)
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})

		this.ajax({
			url: '/resolve/service/worksheet/wsdata/getMap',
			params: {
				problemId: this.id
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					var data = []
					Ext.Object.each(response.data, function(key) {
						data.push({
							name: key,
							value: response.data[key]
						})
					})
					this.wsData.add(data)
				} else clientVM.displayError(response.message)
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	getNextNumber: function() {
		this.ajax({
			url: this.API['getNextNumber'],
			method: 'POST',
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success)
					this.set('number', response.data)
				else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		});
	},
	loadWorksheet: function() {
		this.wsData.removeAll()
		this.workNotesDataStore.removeAll()
		//Postpone loading for now until new ProblemId come back.
		if (clientVM.waitClientVMFetchProblemIdFromUrl)
			clientVM.on('problemIdFetchedFromUrl', function() {
				this.set('id', clientVM.problemId);
				this.loadWorksheet();
				return 'discard';
			}, this);
		else if (this.id) {
			if (this.id == 'ACTIVE')
				this.loadActiveWorksheet();
			else
				this.loadWorksheetWithId();
		} else
			this.getNextNumber();
	},

	exitAfterSave: false,
	saving: false,
	save: function() {
		this.set('saving', true)
		var worksheet = Ext.apply({
			sysId: this.id
		}, this.asObject());

		delete worksheet.sys_id
		delete worksheet.sysCreatedOn
		delete worksheet.sysCreatedBy
		delete worksheet.sysUpdatedOn
		delete worksheet.sysUpdatedBy

		worksheet.sysOrg = clientVM.orgId == 'nil' ? '' : clientVM.orgId;

		this.ajax({
			url: this.API['saveWS'],
			jsonData: worksheet,
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.localize('saveMessage'))
					this.set('id', response.data.id)
					//this.loadWorksheet()
					this.populateWorksheetWithResponseData(response.data, true);
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('saving', false);
			}
		})
	},
	saveIsEnabled$: function() {
		return !this.saving
	},
	saveAndExitIsVisible$: function() {
		return clientVM.screens.length > 1
	},
	saveAndExit: function() {
		this.set('exitAfterSave', true)
		this.save()
	},
	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.Worksheets'
		})
	},
	refresh: function() {
		this.loadWorksheet()
	},

	setActiveIsVisible$: function() {
		return this.id && this.id !== 'ACTIVE';
	},
	setActive: function() {
		this.message({
			title: this.localize('setActiveTitle'),
			msg: this.localize('setActiveMessage', this.number),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('setActiveAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallySetActive
		})
	},

	reallySetActive: function(button) {
		if (button == 'yes') {
			this.ajax({
				url: this.API['setActive'],
				params: {
					id: this.id,
					'RESOLVE.ORG_NAME' : clientVM.orgName,
					'RESOLVE.ORG_ID' : clientVM.orgId
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						clientVM.updateProblemInfo(this.id, this.number)
						clientVM.displayMessage(null, '', {
							msg: this.localize('worksheetSetActiveSuccess', [this.number, this.id])
						});
						this.loadWorksheet()
					} else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
	},

	editResult: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.TaskResult',
			params: {
				id: id,
				activeTab: 1
			}
		})
	},

	jumpToAssignedTo: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.user.User',
			params: {
				username: this.assignedTo
			}
		})
	},
	jumpToAssignedToIsVisible$: function() {
		return this.assignedToName
	},

	assignedToClicked: function() {
		this.open({
			mtype: 'RS.user.UserPicker',
			pick: true,
			callback: this.assignedToSelected
		})
	},

	assignedToClickedClear: function() {
		this.set('assignedTo', '')
		this.set('assignedToName', '')
	},

	assignedToControl: null,
	registerAssignedTo: function(assignedToControl) {
		this.set('assignedToControl', assignedToControl)
		this.assignedToControl.triggerEl.item(1).parent().setDisplayed((this.assignedToName || this.assignedTo) ? 'table-cell' : 'none')
	},

	when_assignedTo_changes_show_or_hide_clear_box: {
		on: ['assignedToChanged', 'assignedToNameChanged'],
		action: function() {
			if (this.assignedToControl) {
				this.assignedToControl.triggerEl.item(1).parent().setDisplayed((this.assignedToName || this.assignedTo) ? 'table-cell' : 'none')
				this.assignedToControl.doComponentLayout()
			}
		}
	},

	assignedToSelected: function(records) {
		var record = records[0]
		this.set('assignedTo', record.get('uuserName'))
		this.set('assignedToName', record.get('udisplayName'))
	},

	addWSData: function() {
		this.open({
			mtype: 'RS.worksheet.WorksheetData',
			id: this.id
		})
	},

	editWSData: function(name) {
		var idx = this.wsData.findExact('name', name);
		this.open({
			mtype: 'RS.worksheet.WorksheetData',
			id: this.id,
			name: name,
			value: idx > -1 ? this.wsData.getAt(idx).get('value') : ''
		})
	},

	deleteWSData: function() {
		this.message({
			title: this.localize('confirmWSDataDelete'),
			msg: this.localize(this.wsDataSelections.length === 1 ? 'deleteWSDataMessage' : 'deleteWSDatasMessage'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteWSData'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteWSData
		})
	},
	deleteWSDataIsEnabled$: function() {
		return this.wsDataSelections.length > 0
	},

	reallyDeleteWSData: function(btn) {
		if (btn == 'yes') {
			var selections = [];
			Ext.Array.forEach(this.wsDataSelections, function(selection) {
				selections.push(selection.get('name'))
			})

			this.ajax({
				url: this.API['deleteWS'],
				params: {
					problemId: this.id,
					ids: selections
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						clientVM.displaySuccess(this.localize('wsDataDeleted'))
						this.loadWorksheet()
					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},

	all: true,

	showAll: function() {
		Ext.state.Manager.set('RS.worksheet.showAll', this.all);
		this.worksheetResults.load();
	},

	hide: function() {
		this.set('all', false);
		Ext.state.Manager.set('RS.worksheet.showAll', this.all);
		this.worksheetResults.load();
	},

	//Child Viewmodels
	socialDetail: {
		mtype: 'RS.social.Main',
		mode: 'embed',
		initialStreamType: 'worksheet'
	}

})
glu.defModel('RS.worksheet.WorksheetData', {
	fields : ['name'],
	id: '',	
	nameIsValid$: function() {
		return this.name ? true : this.localize('nameInvalid')
	},
	value: '',

	editMode: false,

	init: function() {
		if (this.name) this.set('editMode', true)
	},

	cancel: function() {
		this.doClose()
	},

	save: function() {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		this.ajax({
			url: '/resolve/service/wsdata/put',
			params: {
				propertyName: this.name,
				propertyValue: this.value,
				problemId: this.id
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.localize('propertySaved'))
					this.parentVM.loadWorksheet()
					this.doClose()
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	}
})
glu.defModel('RS.worksheet.WorksheetPicker', {
	API : {
		setActive : '/resolve/service/worksheet/setActive',
		newWS : '/resolve/service/worksheet/newWorksheet',
		getSystemProperty : '/resolve/service/sysproperties/getSystemPropertyByName',
		getArchive : '/resolve/service/worksheet/getArchive'
	},
	showPicker: true,

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	when_showPicker_changes_update_worksheet_list: {
		on: ['showPickerChanged'],
		action: function() {
			if (this.showPicker)
				this.worksheets.loadPage(1)
		}
	},

	worksheets: {
		mtype: 'store',
		remoteSort: true,
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		fields: ['id', 'sysId', 'number', 'reference', 'correlationId', 'alertId', 'wsData_dt_dtList',{
			name : 'organization',
			mapping : 'sysOrg',
			convert : function(v, record){
				var orgs = clientVM.userOrganization || [];
				for(var i = 0; i < orgs.length; i++){
					if(v && orgs[i].id == v)
						return orgs[i].uname;
				}
				return "";
			}
		}].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/worksheet/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	worksheetsSelections: [],
	worksheetsColumns: [],

	myWorksheetsIsPressed: false,
	myWorksheets: function() {
		this.set('myWorksheetsIsPressed', !this.myWorksheetsIsPressed)
	},
	myWorksheetsPressedCls$: function() {
		return this.myWorksheetsIsPressed ? 'rs-social-button-pressed' : ''
	},
	myWorksheetsTooltip$: function() {
		return this.myWorksheetsIsPressed ? this.localize('showAllWorksheets') : this.localize('showMyWorksheets')
	},

	searchText: '',
	when_searchText_or_my_worksheets_changes_update_worksheet_grid: {
		on: ['searchTextChanged', 'myWorksheetsIsPressedChanged'],
		action: function() {
			if (!this.openedByWinParam || this.worksheetTabIsPressed)
				this.worksheets.load()
				// if (this.openedByWinParam && this.archivedWorksheetTabIsPressed)
				// 	this.archivedWorksheets.load()
		}
	},

	target: null,

	clientDialog: false,
	afterWorksheetSelected: function(isNewWorksheet) {},
	baseCls$: function() {
		return this.clientDialog && !Ext.isIE8m ? 'x-panel' : 'x-window'
	},
	ui$: function() {
		return this.clientDialog && !Ext.isIE8m ? 'sysinfo-dialog' : 'default'
	},
	prepareFiltersParams: function() {
		var availableFilters = [{
			field: 'alertId',
			type: 'auto',
			condition: 'contains',
			value: this.alertId || this.searchText
		}, {
			field: 'correlationId',
			type: 'auto',
			condition: 'contains',
			value: this.correlationId || this.searchText
		}, {
			field: 'reference',
			type: 'auto',
			condition: 'contains',
			value: this.reference || this.searchText
		}, {
			field: 'sysId',
			type: 'auto',
			condition: 'contains',
			value: this.sysId || this.problemId || this.searchText
		}];
		if (!(this.problemId || this.reference || this.correlationId || this.alertId))
			availableFilters.push({
				field: 'number',
				type: 'auto',
				condition: 'contains',
				value: this.searchText
			})
		var filters = [];
		var urlFilter = [];
		Ext.each(availableFilters, function(availableFilter) {
			if (this[availableFilter.field == 'sysId' ? 'problemId' : availableFilter.field]) {
				availableFilter.condition = 'equals';
				urlFilter.push(availableFilter);
			}
		}, this);

		Ext.each(urlFilter, function(availableFilter) {
			filters.push(availableFilter);
		}, this);

		if (this.myWorksheetsIsPressed) {
			filters.push({
				field: 'assignedTo',
				type: 'auto',
				condition: 'equals',
				value: this.parentVM.user.name
			})
		}

		if (this.searchText) {
			filters.push({
				field: 'number,alertId,correlationId,reference,sysId',
				type: 'string',
				condition: 'fulltext',
				value: this.searchText
			})
		}

		var params = {
			filter: Ext.encode(filters)
		}

		return params;
	},

	init: function() {
		this.set('tabTitle', this.localize('esTitle'));
		this.set('worksheetsColumns', [{
			dataIndex: 'number',
			text: '~~number~~',
			filterable: false,
			sortable: true,
			flex: 2
		}, {
			dataIndex: 'reference',
			text: '~~lookupId~~',
			filterable: false,
			flex: 3,
			renderer: function(value, metaData, record) {
				return Ext.String.htmlEncode(value) || record.get('correlationId') || record.get('alertId') || ''
			}
		}, {
			dataIndex: 'organization',
			text: '~~organization~~',
			filterable: false,
			sortable: false,
			width: 170				
		},{
			text: RS.common.locale.sysUpdated,
			dataIndex: 'sysUpdatedOn',
			filterable: false,
			sortable: true,
			width: 170,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}])


		this.worksheets.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {}
			Ext.apply(operation.params, this.prepareFiltersParams());
		}, this)

		// this.archivedWorksheets.on('beforeload', function(store, operation, eOpts) {
		// 	operation.params = operation.params || {}
		// 	var filters = ;
		// 	Ext.apply(operation.params, this.prepareFiltersParams(function(f) {
		// 		return f != 'sysId'
		// 	}));
		// }, this)

		//this should be refactored...
		//coz more and more dependencies were introduced in to the worksheet / client ui...
		//the orginal asnyc/parallel ajax req should be changed to sequential..
		//or if possible,just move some logic to the backend
		//use cases:
		//if there is a problemid or similar url params... it will fire a req to fetch the ws
		//if only one result comes back..select it and don't show the picker
		//if multiple results come back and there r some exact matches.. only show those exact match
		//if multiple results come back and there r no exact match..show all results
		//if no es is found, check the system properties too see whether need to fetch archived ws by id,
		//	if true, fetch the archived id..if no archived id is found..show picker with the urlparam value in the search textfield
		//	if false, show picker with the urlparam value in the search textfield
		this.worksheets.on('load', function(store, records, success) {
			Ext.Array.forEach(records || [], function(record) {
				record.data.wsData_dt_dtList = []
			})
		}, this)

		if (this.problemId || this.reference || this.correlationId || this.alertId) {
			this.worksheets.on('load', function(store, records, success) {
				if (!success) {
					var raw = store.getProxy().getReader().rawData;
					//clientVM.displayError(this.localize('listWorksheetError', raw.message));
					return;
				}
				var match = [];
				if (records.length == 1)
					match = records;
				else {
					Ext.each(records, function(r) {
						var hit = false;
						Ext.each(['problemId', 'reference', 'correlationId', 'alertId'], function(field) {
							if (this[field])
								hit = hit && (r.get(field) == this[field])
						}, this);
						if (hit)
							match.push(r);
					}, this);
				}
				if (match.length == 1) {
					//Auto select the only record that matched the search results that were passed into the picker from the client
					this.set('worksheetsSelections', match)
					var me = this;
					this.select(function() {
						clientVM.fireEvent('problemIdFetchedFromUrl');
						this.afterWorksheetSelected();
					});
					this.doClose();
				} else {
					if (this.openedByWinParam)
						return;
					this.set('openedByWinParam', true);
					if (records.length == 0) {
						this.ajax({
							url: this.API['getSystemProperty'],
							params: {
								name: 'resultmacro.check.archive'
							},
							success: function(resp) {
								var respData = RS.common.parsePayload(resp);
								if (!respData.success) {
									//clientVM.displayError(this.localize('checkArchiveFlagErr', respData.message));
									clientVM.displayError(respData.message);
									return;
								}
								if (respData.data.uvalue == true || respData.data.uvalue == "true")
									this.getArchiveWS(this.problemId);
								else if (this.autoCreate)
									this.newWorksheet({
										reference: this.reference,
										alertId: this.alertId,
										correlationId: this.correlationId
									});
								else {
									clientVM.open(this);
									this.set('searchText', this.problemId || this.reference || this.correlationId || this.alertId)
									this.set('problemId', '');
									this.set('reference', '');
									this.set('correlationId', '');
									this.set('alertId', '');
								}
							},
							failure: function(resp) {
								clientVM.displayFailure(resp);
							}
						})
					} else {
						clientVM.open(this);
						this.set('searchText', this.problemId || this.reference || this.correlationId || this.alertId)
						this.set('problemId', '');
						this.set('reference', '');
						this.set('correlationId', '');
						this.set('alertId', '');
					}
				}

			}, this, {
				single: true
			})
		}
		this.worksheets.on('beforeload', function(store, operation){
			operation.params = Ext.apply(operation.params || {}, {
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			});
		})
		this.worksheets.load();
	},
	getArchiveWS: function(id) {
		this.ajax({
			url: this.API['getArchive'],
			params: {
				id: id
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success || !respData.data) {
					if (this.autoCreate)
						this.newWorksheet();
					else
						clientVM.displayError(this.localize('archivedWorksheetsNotFound'), respData.message);
					return
				}
				var id = respData.data.id;
				var number = respData.data.number;
				clientVM.displaySuccess(this.localize('archivedWorksheetSelectSuccess', [number, id]));
				clientVM.updateProblemInfo(id, number);
				this.doClose();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	openedByWinParam: false,
	beforeClose: function() {
		this.set('showPicker', false);
		this.set('openedByWinParam', false);
	},
	// archivedWorksheets: {
	// 	mtype: 'store',
	// 	fields: ['id', 'sysId', 'number', 'reference', 'correlationId', 'alertId', 'wsData_dt_dtList'].concat(RS.common.grid.getSysFields()),
	// 	sorters: [{
	// 		property: 'sysUpdatedOn',
	// 		direction: 'DESC'
	// 	}],
	// 	proxy: {
	// 		type: 'ajax',
	// 		url: '/resolve/service/worksheet/listArchive',
	// 		reader: {
	// 			type: 'json',
	// 			root: 'records'
	// 		}
	// 	}
	// },
	tabTitle: '',
	activeItem: 0,
	worksheetTab: function() {
		this.set('tabTitle', this.localize('esTitle'));
		this.set('activeItem', 0);
		this.worksheets.load();
	},
	worksheetTabIsPressed$: function() {
		return this.activeItem == 0;
	},
	// archivedWorksheetTab: function() {
	// 	this.set('tabTitle', this.localize('archivedTitle'));
	// 	this.set('activeItem', 1);
	// 	this.archivedWorksheets.load();
	// },
	// archivedWorksheetTabIsPressed$: function() {
	// 	return this.activeItem == 1;
	// },
	dblclick: function(record, item, index, e, eOpts) {
		this.set('worksheetsSelections', [record])
		this.select()
	},

	go: function() {
		var id = this.worksheetsSelections[0].get('sysId'),
			number = this.worksheetsSelections[0].get('number');

		this.ajax({
			url: '/resolve/service/wsdata/getMap',
			params: {
				problemId: id,
				keyList: ['dt_dtList']
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					var data = Ext.decode(response.data['dt_dtList']) || [],
						record = null;

					Ext.Array.forEach(data, function(record) {
						record.id = Ext.id(),
							record.problemId = id
					})

					//sort by date
					data.sort(function(a, b) {
						if (a.date < b.date) return -1
						if (b.date < a.date) return 1
						return 0
					})

					if (data.length > 0) {
						record = data[data.length - 1]
					}

					this.setActive(id, number, function() {
						if (record) {
							clientVM.handleNavigation({
								modelName: record.type == 'dtviewer' ? 'RS.decisiontree.Main' : 'RS.wiki.Main',
								params: {
									name: record.name,
									problemId: id
								}
							})
						}
					})
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	goIsVisible$: function() {
		return this.activeItem == 0;
	},
	goIsEnabled$: function() {
		return this.worksheetsSelections.length == 1
	},
	select: function(callback) {
		if (!Ext.isFunction(callback)){
			if (clientVM.windowParams.rid) {
				callback = function () {
					clientVM.fireEvent('problemIdFetchedFromUrl');
					this.afterWorksheetSelected();
				}
			} else {
				callback = null;
			}
		}
		var id = this.worksheetsSelections[0].get('sysId'),
			number = this.worksheetsSelections[0].get('number');
		this.setActive(id, number, callback)
	},

	selectIsEnabled$: function() {
		return this.worksheetsSelections.length == 1
	},

	setActive: function(id, number, callback, args) {
		var myArguments = arguments;
		this.ajax({
			url: this.API['setActive'],
			params: {
				id: id,
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displayMessage(null, '', {
						msg: (number ? this.localize('worksheetSetActiveSuccess', [number, id]) : this.localize('newWorksheetActiveSuccess', id))
					});
					clientVM.updateProblemInfo(id, number)
					if (callback) {
						callback.apply(this, Array.prototype.slice.call(myArguments, 3))
					}
					clientVM.fireEvent('problemIdFetchedFromUrl');
					this.set('autoSelected', true)
					this.close()
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	view: function() {
		var id = this.parentVM.problemId || 'ACTIVE'
		if (this.worksheetsSelections.length > 0) {
			id = this.worksheetsSelections[0].get('sysId')
		}
		if (clientVM.fromArchive)
			this.parentVM.handleNavigation({
				modelName: 'RS.worksheet.ArchivedWorksheet',
				target: '_blank',
				params: {
					id: id
				}
			})
		else
			this.parentVM.handleNavigation({
				modelName: 'RS.worksheet.Worksheet',
				target: '_blank',
				params: {
					id: id
				}
			})

		this.close()
	},
	viewAll: function() {
		if (this.activeItem == 0)
			this.parentVM.handleNavigation({
				modelName: 'RS.worksheet.Worksheets',
				target: '_blank'
			})
		else
			this.parentVM.handleNavigation({
				modelName: 'RS.worksheet.ArchivedWorksheets',
				target: '_blank'
			})
		this.close()
	},
	newWorksheet: function() {
		this.ajax({
			url: this.API['newWS'],
			method: 'POST',
			params: {
				reference: this.reference,
				alertId: this.alertId,
				correlationId: this.correlationId,
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.setActive(response.data, undefined, this.afterWorksheetSelected, true)
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	newWorksheetIsVisible$: function() {
		return this.activeItem == 0;
	},
	autoCreate: false,
	autoSelected: false,
	close: function() {
		if (!this.clientDialog && !this.autoSelected) {
			var newestWorksheet = this.worksheets.getCount() > 0 ? this.worksheets.getAt(0) : null;
			this.worksheets.each(function(record) {
				if (record.get('number') == this.problemId) {
					this.set('worksheetsSelections', [record])
					return false
				}
				if (record.get('sysUpdatedOn') > newestWorksheet.get('sysUpdatedOn')) newestWorksheet = record
			}, this)
			if (newestWorksheet && this.worksheetsSelections.length == 0) this.set('worksheetsSelections', [newestWorksheet])

			if (this.worksheetsSelections.length > 0) {
				this.set('autoSelected', true)
				this.select()
				return
			}

			clientVM.displayError(this.localize('noWorksheetSelected'))
		}

		this.set('showPicker', false)

		// if (this.parentVM.prbWin) this.parentVM.prbWin = null
		// this.doClose()
	},
	closeIsVisible$: function() {
		return this.clientDialog
	},

	cancel: function() {
		this.set('showPicker', false)
	},
	cancelIsVisible$: function() {
		return !this.clientDialog
	},

	goIsVisible$: function() {
		return this.clientDialog
	},
	expandBody: function(rowNode, record, expandRow, eOpts) {
		var problemId = record.get('sysId')
			//go get the data for the worksheet
		this.ajax({
			url: '/resolve/service/wsdata/getMap',
			params: {
				problemId: problemId,
				keyList: ['dt_dtList']
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					var data = Ext.decode(response.data['dt_dtList']) || [],
						dateFormat = this.parentVM.getUserDefaultDateFormat(),
						tpl = new Ext.XTemplate(
							'<table style="width:100%">',
							'<tpl for=".">',
							'<tr>',
							'<td>', this.localize('name'), ': {name}</td>',
							'<td>', RS.common.locale.sysUpdatedOn, ': {date:this.formatDate}</td>',
							'<td><span id="{id}"></span></td>',
							'</tr>',
							'</tpl>',
							'</table>', {
								formatDate: function(value) {
									return Ext.Date.format(Ext.Date.parse(value, 'time'), dateFormat) || ''
								}
							}
						),
						replaceNode = Ext.fly(rowNode).query('span[name=replace]')[0];

					Ext.Array.forEach(data, function(record) {
						record.id = Ext.id(),
							record.problemId = problemId
					})

					//sort by date
					data.sort(function(a, b) {
						if (a.date < b.date) return -1
						if (b.date < a.date) return 1
						return 0
					})

					tpl.overwrite(replaceNode, data)

					//render the buttons
					Ext.Array.forEach(data, function(record) {
						Ext.create('Ext.button.Button', {
							text: this.localize('continueWorksheet'),
							iconCls: 'icon-large icon-play-sign rs-worksheet-continue',
							cls : 'rs-small-btn rs-btn-dark',
							renderTo: record.id,
							record: record,
							scope: this,
							handler: record.type == 'dtviewer' ? this.continueDecisionTree : this.continueWiki
						})
					}, this)
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	continueDecisionTree: function(button) {
		//select the worksheet for this dt
		var record = this.worksheets.getAt(this.worksheets.findExact('sysId', button.record.problemId))
		this.set('autoSelected', true)
		this.setActive(record.get('sysId'), record.get('number'), function(r) {
			clientVM.handleNavigation({
				modelName: 'RS.decisiontree.Main',
				params: {
					name: button.record.name,
					problemId: button.record.problemId
				}
			})
		})
	},

	continueWiki: function(button) {
		//select the worksheet for this dt
		var record = this.worksheets.getAt(this.worksheets.findExact('sysId', button.record.problemId))
		this.set('autoSelected', true)
		this.setActive(record.get('sysId'), record.get('number'), function(r) {
			clientVM.handleNavigation({
				modelName: 'RS.wiki.Main',
				params: {
					name: button.record.name
				}
			})
		})
	}
})
glu.defModel('RS.worksheet.Worksheets', {
	API : {
		deleteWS : '/resolve/service/worksheet/delete',
		setActive : '/resolve/service/worksheet/setActive'
	},
	mock: false,

	activate: function() {
		clientVM.setWindowTitle(this.localize('allWorksheets'))
			// window.name = 'allworksheets'
		this.worksheets.load();
	},

	deactivate: function() {
		// window.name = ''
	},

	displayName: '~~allWorksheets~~',
	stateId: 'worksheetList',

	worksheets: {
		mtype: 'store',
		remoteSort: true,
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		fields: ['id', 'number', 'condition', 'severity', 'summary', 'assignedTo', 'assignedToName', 'sysId', 'reference', 'alertId', 'correlationId', 'sirId',{
			name: 'sysTtl',
			type: 'int'
		}, 'sysOrg'
		].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/worksheet/list',
			reader: {
				type: 'json',
				root: 'records'
			}
		}
	},

	columns: [],

	allSelected: false,
	worksheetsSelections: [],

	init: function() {
		var me = this;
		//Called when mocking the backend to test the UI
		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)

		var worksheetsColumns = [{
			header: '~~number~~',
			dataIndex: 'number',
			filterable: true,
			sortable: true
		}, {
			header: '~~condition~~',
			dataIndex: 'condition',
			filterable: true,
			sortable: true,
			width: 90,
			renderer: RS.common.grid.statusRenderer('rs-worksheet-status')
		}, {
			header: '~~severity~~',
			dataIndex: 'severity',
			filterable: true,
			sortable: true,
			width: 90,
			renderer: RS.common.grid.statusRenderer('rs-worksheet-status')
		}, {
			header: '~~summary~~',
			dataIndex: 'summary',
			filterable: true,
			sortable: true,
			flex: 1,
			//renderer: RS.common.grid.htmlRenderer()
		}, {
			header: '~~assignedToName~~',
			dataIndex: 'assignedToName',
			sortable: true,
			filterable: true
		}, {
			header: '~~assignedTo~~',
			dataIndex: 'assignedTo',
			sortable: true,
			filterable: true,
			hideable: false,
			hidden: true
		}, {
			header: '~~reference~~',
			dataIndex: 'reference',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~alertId~~',
			dataIndex: 'alertId',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~correlationId~~',
			dataIndex: 'correlationId',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.plainRenderer()
		},{
			header: '~~sirId~~',
			dataIndex: 'sirId',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.plainRenderer()
		},{
			header: '~~organization~~',
			dataIndex: 'sysOrg',
			initialShow: true,
			sortable: false,
			renderer: function(value, metaData, record) {
				if (value) {
					if(!record.store.id2NameMap) {
						record.store.id2NameMap = {};
						clientVM.user.orgs.forEach(function(e) {
							if(e.uname != 'None')
								record.store.id2NameMap[e.id] = e.uname;
						});
					}
					return RS.common.grid.plainRenderer() (record.store.id2NameMap[value]);
				}
				return '';
			}
		}].concat(RS.common.grid.getSysColumns());

		Ext.each(this.worksheetsColumns, function(col) {
			col.stateId = 'worksheetscolumn_' + col.dataIndex;
		}, this);


		//correct worksheet columns
		var updatedCol, index = -1;
		Ext.Array.each(worksheetsColumns, function(column, idx) {
			if (column.dataIndex == 'sysUpdatedOn') {
				updatedCol = column
				index = idx
			}
			if (column.dataIndex == 'sysCreatedOn') {
				column.hidden = false;
				column.initialShow = true;
				column.flex = 1;
			}
		})
		if (updatedCol && index > -1) {
			updatedCol.renderer = Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
			updatedCol.hidden = false
			updatedCol.initialShow = true
			worksheetsColumns.splice(index, 1)
			worksheetsColumns.splice(0, 0, updatedCol)
		}
		this.set('columns', worksheetsColumns)

		this.set('displayName', this.localize('allWorksheets'));
		this.worksheets.on('beforeload', function(store, operation){
			operation.params = Ext.apply(operation.params || {}, {
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			});
			var filter = operation.params.filter || [];
			filter.push({
				field: 'sysOrg',
				type: 'terms',
				condition: 'equals',
				value: clientVM.orgId || 'nil'
			});
			operation.params.filter = JSON.stringify(filter);
		});
		clientVM.on('orgchange', this.orgChange.bind(this));
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	creatingWorksheet: false,
	createWorksheet: function() {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.Worksheet'
		})
	},
	createWorksheetIsEnabled$: function() {
		return !this.creatingWorksheet
	},
	editWorksheet: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.Worksheet',
			params: {
				id: id,
				activeTab: 1
			}
		})
	},
	singleRecordSelected$: function() {
		return this.worksheetsSelections.length == 1
	},
	editWorksheetIsEnabled$: function() {
		return this.singleRecordSelected
	},
	deleteWorksheetIsEnabled$: function() {
		return this.worksheetsSelections.length > 0 || this.allSelected
	},
	deleteWorksheet: function() {
		//Delete the selected record(s), but first confirm the action
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.worksheetsSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.worksheetsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			if (this.allSelected) {
				this.ajax({
					url: this.APO['deleteWS'],
					params: {
						ids: '',
						whereClause: this.worksheets.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.worksheets.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.worksheetsSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: this.API['deleteWS'],
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.worksheets.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	},

	setActiveIsEnabled$: function() {
		return this.singleRecordSelected
	},
	setActive: function() {
		var sysTtl = this.worksheetsSelections[0].get('sysTtl');
		//server will inform if the worksheet is old or not.
		//if (sysTtl > 0 && this.worksheetsSelections[0].get('sysCreatedOn') + sysTtl < Ext.Date.format(new Date(), 'time'))
		//	clientVM.displayError(this.localize('oldWorksheet'), 10000)
		//else
		this.message({
			title: this.localize('setActiveTitle'),
			msg: this.localize('setActiveMessage', this.worksheetsSelections[0].get('number')),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('setActiveAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallySetActive
		})
	},
	reallySetActive: function(button) {
		if (button == 'yes') {
			this.ajax({
				url: this.API['setActive'],
				params: {
					id: this.worksheetsSelections[0].get('id'),
					'RESOLVE.ORG_NAME' : clientVM.orgName,
					'RESOLVE.ORG_ID' : clientVM.orgId
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						clientVM.updateProblemInfo(this.worksheetsSelections[0].get('id'), this.worksheetsSelections[0].get('number'))
						clientVM.displayMessage(null, '', {
							msg: this.localize('worksheetSetActiveSuccess', [this.worksheetsSelections[0].get('number'), this.worksheetsSelections[0].get('id')])
						});
						this.worksheets.load()
					} else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
	},

	orgChange: function() {
		this.worksheets.load();
	},

	beforeDestroyComponent : function() {
		clientVM.removeListener('orgchange', this.orgChange);
	}
})
glu.defView('RS.worksheet.ArchivedTaskResult', {
	padding: '10px',
	bodyPadding: '10px',
	layout: 'card',
	activeItem: '@{activeTab}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
				xtype: 'tbtext',
				cls: 'rs-display-name',
				text: '@{taskResultTitle}'
			},
			'->', {
				name: 'generalTab',
				pressed: '@{generalTabIsPressed}'
			}, {
				name: 'detailTab',
				pressed: '@{detailTabIsPressed}'
			}, {
				name: 'rawTab',
				pressed: '@{rawTabIsPressed}'
			}
		]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar actionBar-form',
		items: ['back', '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sys_id: '',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrg}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-btn-icon x-tbar-loading',
			handler: '@{loadTaskResult}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	items: [{
		autoScroll: true,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			layout: 'column',
			items: [{
				layout: 'anchor',
				columnWidth: 0.5,
				defaultType: 'displayfield',
				defaults: {
					anchor: '-20',
					labelWidth: 125
				},
				items: [{
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'displayfield',
						labelWidth: 125,
						name: 'taskName',
						margin: '0px 10px 0px 0px'
					}, {
						xtype: 'image',
						cls: 'rs-btn-edit',
						hidden: '@{!jumpToActionTaskIsVisible}',
						listeners: {
							handler: '@{jumpToActionTask}',
							render: function(image) {
								image.getEl().on('click', function() {
									image.fireEvent('handler')
								})
							}
						}
					}]
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
							xtype: 'displayfield',
							labelWidth: 125,
							name: 'problemNumber',
							margin: '0px 10px 0px 0px'
						}
						/*, {
												xtype: 'image',
												cls: 'rs-btn-edit',
												hidden: '@{!jumpToWorksheetIsVisible}',
												listeners: {
													handler: '@{jumpToWorksheet}',
													render: function(image) {
														image.getEl().on('click', function() {
															image.fireEvent('handler')
														})
													}
												}
											}*/
					]
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'displayfield',
						labelWidth: 125,
						name: 'processNumber',
						margin: '0px 10px 0px 0px'
					}, {
						xtype: 'image',
						cls: 'rs-btn-edit',
						hidden: '@{!jumpToProcessRequestIsVisible}',
						listeners: {
							handler: '@{jumpToProcessRequest}',
							render: function(image) {
								image.getEl().on('click', function() {
									image.fireEvent('handler')
								})
							}
						}
					}]
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'displayfield',
						labelWidth: 125,
						name: 'targetGUID',
						margin: '0px 10px 0px 0px'
					}, {
						xtype: 'image',
						cls: 'rs-btn-edit',
						hidden: '@{!showGUIDDetails}',
						listeners: {
							handler: '@{displayGUIDDetails}',
							render: function(image) {
								image.getEl().on('click', function() {
									image.fireEvent('handler')
								})
							}
						}
					}]
				}, 'address', 'esbAddr']
			}, {
				layout: 'anchor',
				columnWidth: 0.5,
				defaultType: 'displayfield',
				defaults: {
					anchor: '-20',
					labelWidth: 125
				},
				items: ['duration', 'completion', 'condition', 'severity']
			}]
		}, {
			flex: 1,
			minHeight: 300,
			bodyPadding: '10px',
			cls: 'rs-monospace',
			title: '~~summary~~',
			html: '@{summaryDisplay}',
			autoScroll: true
		}]
	}, {
		layout: 'fit',
		items: [{
			autoScroll: true,
			style: 'border: 1px solid #999 !important',
			cls: 'rs-monospace',
			bodyPadding: '10px',
			html: '@{detailDisplay}'
		}]
	}, {
		layout: 'fit',
		items: [{
			autoScroll: true,
			style: 'border: 1px solid #999 !important',
			cls: 'rs-monospace',
			bodyPadding: '10px',
			html: '@{rawText}'
		}]
	}]
});
glu.defView('RS.worksheet.ArchivedWorksheet', {
	padding: '10px',
	layout: 'card',
	activeItem: '@{activeTab}',
	layoutConfig: {
		deferrredRender: false
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		itemId: 'actionBar',
		defaultButtonUI: 'display-toolbar-button',
		items: ['->', {
			name: 'generalTab',
			pressed: '@{generalTabIsPressed}'
		}, {
			name: 'resultsTab',
			pressed: '@{resultsTabIsPressed}'
		}, {
			name: 'debugTab',
			pressed: '@{debugTabIsPressed}'
		}]
	}],
	displayName: '@{worksheetTitle}',
	setDisplayName: function(value) {
		this.getPlugin('searchfilter').toolbar.down('#tableMenu').setText(value)
	},
	displayFilter: '@{displayFilter}',
	setDisplayFilter: function(value) {
		this.getPlugin('searchfilter').setDisplayFilter(value)
	},
	plugins: [{
		ptype: 'searchfilter',
		pluginId: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true,
		useWindowParams: false,
		gridName: 'resultsGrid'
	}],
	items: [{
		autoScroll: true,
		bodyPadding: '10px',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar actionBar-form',
			items: [{
				name: 'back',
				iconCls: 'icon-large icon-eject rs-icon',
				text: ''
			}, /* 'setActive',*/ '->', {
				iconCls: 'rs-social-button @{socialPressedCls}',
				style: 'left: -1px',
				enableToggle: true,
				pressed: '@{socialIsPressed}',
				tooltip: '~~socialTooltip~~'
			}, {
				xtype: 'followbutton',
				streamId: '@{id}',
				streamType: 'worksheet'
			}, {
				xtype: 'sysinfobutton',
				sysId: '@{id}',
				sys_id: '',
				sysCreated: '@{sysCreatedOn}',
				sysCreatedBy: '@{sysCreatedBy}',
				sysUpdated: '@{sysUpdatedOn}',
				sysUpdatedBy: '@{sysUpdatedBy}',
				sysOrg: '@{sysOrg}',
				dateFormat: '@{userDateFormat}'
			}, {
				iconCls: 'x-btn-icon x-tbar-loading',
				tooltip: '~~refresh~~',
				handler: '@{refresh}',
				listeners: {
					render: function(button) {
						clientVM.updateRefreshButtons(button);
					}
				}
			}]
		}],
		layout: 'border',
		items: [{
			region: 'center',
			autoScroll: true,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				layout: 'column',
				items: [{
					columnWidth: 0.6,
					layout: 'anchor',
					defaultType: 'textfield',
					defaults: {
						anchor: '-30'
					},
					items: [{
						xtype: 'displayfield',
						name: 'number'
					}, 'alertId', 'reference', 'correlationId', {
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [{
							xtype: 'triggerfield',
							flex: 1,
							name: 'assignedToName',
							margin: '0px 10px 0px 0px',
							trigger1Cls: Ext.baseCSSPrefix + 'form-search-trigger',
							onTrigger1Click: function() {
								this.fireEvent('handleTriggerClick')
							},
							trigger2Cls: Ext.baseCSSPrefix + 'form-clear-trigger',
							onTrigger2Click: function() {
								this.fireEvent('clearTriggerClick')
							},
							listeners: {
								handleTriggerClick: '@{assignedToClicked}',
								clearTriggerClick: '@{assignedToClickedClear}',
								registerAssignedTo: '@{registerAssignedTo}',
								render: function(field) {
									field.fireEvent('registerAssignedTo', field, field)
								}
							}
						}, {
							xtype: 'image',
							cls: 'rs-btn-edit',
							hidden: '@{!jumpToAssignedToIsVisible}',
							listeners: {
								handler: '@{jumpToAssignedTo}',
								render: function(image) {
									image.getEl().on('click', function() {
										image.fireEvent('handler')
									})
								}
							}
						}]
					}]
				}, {
					columnWidth: 0.4,
					layout: 'anchor',
					defaults: {
						anchor: '100%'
					},
					items: [{
						xtype: 'combobox',
						name: 'condition',
						store: '@{conditionStore}',
						queryMode: 'local',
						displayField: 'name',
						valueField: 'value'
					}, {
						xtype: 'combobox',
						name: 'severity',
						store: '@{severityStore}',
						queryMode: 'local',
						displayField: 'name',
						valueField: 'value'
					}]
				}]
			}, {
				xtype: 'textfield',
				name: 'summary'
			}, {
				xtype: 'textarea',
				name: 'description'
			}, {
				xtype: 'textarea',
				name: 'workNotes'
			}, {
				xtype: 'grid',
				minHeight: 200,
				store: '@{workNotesDataStore}',
				columns: '@{workNotesColumns}'
			}]
		}, {
			region: 'east',
			split: true,
			width: 480,
			stateId: 'wikiSocialBar',
			stateful: true,
			hidden: '@{!socialIsPressed}',
			layout: 'fit',
			items: [{
				xtype: '@{socialDetail}'
			}]
		}]
	}, {
		layout: 'border',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			name: 'actionBar',
			items: [{
					name: 'back',
					iconCls: 'icon-large icon-eject rs-icon',
					text: ''
				},
				/*'|',
				'setActive',
				{
					name: 'showAll',
					enableToggle: true,
					pressed: '@{all}'
				},*/
				{
					iconCls: 'rs-social-button @{socialPressedCls}',
					style: 'left: -1px',
					enableToggle: true,
					pressed: '@{socialIsPressed}',
					tooltip: '~~socialTooltip~~'
				}
			]
		}],
		socialId: '@{id}',
		setSocialId: function(value) {
			this.getPlugin('pager').setSocialId(value)
		},
		plugins: [{
			ptype: 'pager',
			pluginId: 'pager',
			showSocial: true,
			socialStreamType: 'worksheet'
		}],
		items: [{
			region: 'center',
			xtype: 'grid',
			store: '@{worksheetResults}',
			columns: '@{worksheetColumns}',
			stateId: '@{stateId}',
			stateful: true,
			itemId: 'resultsGrid',
			selModel: {
				selType: 'resolvecheckboxmodel',
				columnTooltip: RS.common.locale.editColumnTooltip,
				columnTarget: '_self',
				columnEventName: 'editAction',
				columnIdField: 'sysId'
			},
			plugins: [{
				ptype: 'resolveexpander',
				rowBodyTpl: new Ext.XTemplate(
					// '<span style="font-weight:bold;padding-left:35px">Queue Name</span>--{esbaddr}<br/>',
					// '<span style="font-weight:bold;padding-left:35px">Duration</span>--{duration}<br/>',
					// '<span style="font-weight:bold;padding-left:35px">Process Request</span>--{processNumber}<br/>',
					// '<span style="font-weight:bold;padding-left:35px">Target</span>--{address}<br/>',
					// '<span style="font-weight:bold;padding-left:35px">Guid</span>--{targetGUID}<br/>',
					'<div resolveId="resolveRowBody" style="color:#333">',
					'<span style="padding-left:10px">Detail:</span><br/>',
					'<span>{[this.doDetai(values)]}</span>',
					'</div>', {
						doDetai: function(detail) {
							return Ext.util.Format.nl2br('<pre style="padding-left:10px;padding-top:10px">' + Ext.String.htmlEncode(detail.detail) + '</pre>')
						}
					}
				)
			}],
			listeners: {
				editAction: '@{editResult}'
			}
		}, {
			region: 'east',
			split: true,
			width: 480,
			stateId: 'wikiSocialBar',
			stateful: true,
			hidden: '@{!socialIsPressed}',
			layout: 'fit',
			items: [{
				xtype: '@{socialDetail}'
			}]
		}]
	}, {
		layout: 'border',
		bodyPadding: '10px',
		tbar: {
			xtype: 'toolbar',
			cls: 'actionBar actionBar-form',
			items: [{
				name: 'back',
				iconCls: 'icon-large icon-eject rs-icon',
				text: ''
			}, '->', {
				iconCls: 'rs-social-button @{socialPressedCls}',
				style: 'left: -1px',
				enableToggle: true,
				pressed: '@{socialIsPressed}',
				tooltip: '~~socialTooltip~~'
			}, {
				xtype: 'followbutton',
				streamId: '@{id}',
				streamType: 'worksheet'
			}, {
				xtype: 'sysinfobutton',
				sysId: '@{id}',
				sys_id: '',
				sysCreated: '@{sysCreatedOn}',
				sysCreatedBy: '@{sysCreatedBy}',
				sysUpdated: '@{sysUpdatedOn}',
				sysUpdatedBy: '@{sysUpdatedBy}',
				sysOrg: '@{sysOrg}',
				dateFormat: '@{userDateFormat}'
			}, {
				iconCls: 'x-btn-icon x-tbar-loading',
				tooltip: '~~refresh~~',
				handler: '@{refresh}'
			}]
		},
		items: [{
			region: 'center',
			xtype: 'textarea',
			readOnly: true,
			labelAlign: 'top',
			hideLabel: true,
			name: 'debug'
		}, {
			region: 'east',
			split: true,
			width: 480,
			stateId: 'wikiSocialBar',
			stateful: true,
			hidden: '@{!socialIsPressed}',
			layout: 'fit',
			items: [{
				xtype: '@{socialDetail}'
			}]
		}]
	}],
	listeners: {
		afterrender: function() {
			var splitters = this.query('bordersplitter')
			Ext.Array.forEach(splitters, function(splitter) {
				splitter.setSize(1, 1)
				splitter.getEl().setStyle({
					backgroundColor: '#fbfbfb'
				})
			})
			this.setDisplayFilter(this.displayFilter)
		}
	}
});
glu.defView('RS.worksheet.ArchivedWorksheets', {
	padding: 15,
	layout: 'fit',
	items: [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'worksheets',
		border: false,
		stateId: '@{stateId}',
		displayName: '@{displayName}',
		stateful: true,
		multiSelect: false,
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}, {
			ptype: 'columnautowidth'
		}],
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTooltip: RS.common.locale.editColumnTooltip,
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'id'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: []
		}],
		columns: '@{columns}',
		viewConfig: {
			enableTextSelection: true
		},
		listeners: {
			// showDetail: '@{showDetails}',
			editAction: '@{editWorksheet}',
			selectAllAcrossPages: '@{selectAllAcrossPages}',
			selectionChange: '@{selectionChanged}'
		}
	}]
});
glu.defView('RS.worksheet.ListRegistrationDetails', {

	asWindow: {
		title: '~~title~~',
		height: 425,
		width: 450,
		modal: true
	},

	bodyPadding: '10px',
	buttonAlign: 'left',
	buttons: ['close'],

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaultType: 'displayfield',
	items: ['uname',
		'ustatus',
		'uguid',
		'utype',
		'uipaddress',
		'ubuild', 'sysUpdatedOn', 'sysUpdatedBy', 'sysCreatedOn', 'sysCreatedBy'
	]
})
glu.defView('RS.worksheet.ProcessRequest', {
	padding : 15,
	autoScroll: true,
	dockedItems: [{
		xtype: 'toolbar',		
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls: 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{processRequestTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',			
		}, 'abort', '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrg}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-btn-icon x-tbar-loading',
			handler: '@{loadProcessRequest}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	layout: {
		type: 'vbox',
		align: 'stretch'
	},	
	items: [{
		style : 'border-top:1px solid silver',
		padding : '8 0',
		defaultType: 'displayfield',
		defaults: {
			labelWidth: 125,
			margin : '4 0'
		},
		items : ['number', {
			xtype: 'container',	
			layout : 'hbox',		
			items: [{
				xtype: 'displayfield',
				labelWidth: 125,		
				name: 'problemNumber'				
			}, {
				xtype: 'image',				
				hidden: '@{!jumpToWorksheetIsVisible}',
				listeners: {
					handler: '@{jumpToWorksheet}',
					render: function(image) {
						image.getEl().on('click', function() {
							image.fireEvent('handler')
						})
					}
				}
			}]
		}, 'wiki', 'timeout', 'status', 'duration']
	}]
})
glu.defView('RS.worksheet.ProcessRequests', {
	padding: 15,
	layout: 'fit',
	items: [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'grid',
		border: false,
		stateId: '@{stateId}',
		displayName: '@{displayName}',
		stateful: true,
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}, {
			ptype: 'columnautowidth'
		}],
		store: '@{processRequests}',
		selModel: {
			selType: 'resolvecheckboxmodel',	
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'id'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: ['deleteProcessRequest', 'deleteAllProcessRequest','abortProcessRequest','abortAllProcessRequest']
		}],
		columns: '@{columns}',
		viewConfig: {
			enableTextSelection: true
		},
		listeners: {
			editAction: '@{editProcessRequest}'
		}
	}]
});
glu.defView('RS.worksheet.TaskRequest', {
	padding: '10px',
	bodyPadding: '10px',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{taskRequestTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar',
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-eject rs-icon',
			text: ''
		}, 'abort', '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{created}',
			sysCreatedBy: '@{createdBy}',
			sysUpdated: '@{updated}',
			sysUpdatedBy: '@{updatedBy}',
			sysOrg: '@{sysOrg}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-btn-icon x-tbar-loading',
			handler: '@{loadTaskRequest}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	layout: 'column',
	items: [{
		layout: 'anchor',
		columnWidth: 0.5,
		defaultType: 'displayfield',
		defaults: {
			anchor: '-20',
			labelWidth: 125
		},
		items: ['taskRequest', 'status', 'runbook', {
			xtype: 'fieldcontainer',
			layout: 'hbox',
			items: [{
				xtype: 'displayfield',
				labelWidth: 125,
				name: 'processRequest',
				margin: '0px 10px 0px 0px'
			}, {
				xtype: 'image',
				cls: 'rs-btn-edit',
				hidden: '@{!jumpToProcessRequestIsVisible}',
				listeners: {
					handler: '@{jumpToProcessRequest}',
					render: function(image) {
						image.getEl().on('click', function() {
							image.fireEvent('handler')
						})
					}
				}
			}]
		}, {
			xtype: 'fieldcontainer',
			layout: 'hbox',
			items: [{
				xtype: 'displayfield',
				labelWidth: 125,
				name: 'worksheet',
				margin: '0px 10px 0px 0px'
			}, {
				xtype: 'container',
				autoEl: {
					tag: 'a',
					href: '@{worksheetLink}'
				},
				setHref: function(href) {
					debugger;
				},
				items: [{
					xtype: 'image',
					cls: 'rs-btn-edit',
					hidden: '@{!jumpToWorksheetIsVisible}',
					listeners: {
						handler: '@{jumpToWorksheet}',
						render: function(image) {
							image.getEl().on('click', function() {
								image.fireEvent('handler')
							})
						}
					}
				}]
			}]
		}, {
			xtype: 'fieldcontainer',
			layout: 'hbox',
			items: [{
				xtype: 'displayfield',
				labelWidth: 125,
				name: 'actionTask',
				margin: '0px 10px 0px 0px'
			}, {
				xtype: 'image',
				cls: 'rs-btn-edit',
				hidden: '@{!jumpToActionTaskIsVisible}',
				listeners: {
					handler: '@{jumpToActionTask}',
					render: function(image) {
						image.getEl().on('click', function() {
							image.fireEvent('handler')
						})
					}
				}
			}]
		}]
	}, {
		layout: 'anchor',
		columnWidth: 0.5,
		defaultType: 'displayfield',
		defaults: {
			anchor: '-20',
			labelWidth: 125
		},
		items: ['createdText', 'createdBy', 'lastUpdatedText', 'lastUpdatedBy', 'nodeId', 'duration']
	}]
})
glu.defView('RS.worksheet.TaskRequests', {
	padding: 15,
	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'grid',
		cls : 'rs-grid-dark',
		border: false,
		stateId: '@{stateId}',
		displayName: '@{displayName}',
		stateful: true,
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}, {
			ptype: 'columnautowidth'
		}],
		store: '@{taskRequests}',
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTooltip: RS.common.locale.editColumnTooltip,
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'id'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: ['deleteTaskRequest', 'abortTaskRequest']
		}],
		columns: '@{columns}',
		viewConfig: {
			enableTextSelection: true
		},
		listeners: {
			editAction: '@{editTaskRequest}',
			selectAllAcrossPages: '@{selectAllAcrossPages}',
			selectionChange: '@{selectionChanged}'
		}
	}]
});
glu.defView('RS.worksheet.TaskResult', {
	padding: 15,
	layout: 'card',
	activeItem: '@{activeTab}',
	dockedItems: [{
		xtype: 'toolbar',	
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls : 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		items: [{
				xtype: 'tbtext',
				cls: 'rs-display-name',
				text: '@{taskResultTitle}'
			},
			'->', {
				name: 'generalTab',
				pressed: '@{generalTabIsPressed}'
			}, {
				name: 'detailTab',
				pressed: '@{detailTabIsPressed}'
			}, {
				name: 'rawTab',
				pressed: '@{rawTabIsPressed}'
			}
		]
	}, {
		xtype: 'toolbar',		
		cls: 'actionBar rs-dockedtoolbar',
		items: [{
			name : 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',
			cls : 'rs-small-btn rs-btn-light'
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sys_id: '',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrg}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-btn-icon x-tbar-loading',
			handler: '@{loadTaskResult}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	items: [{
		style : 'border-top:1px solid silver',
		padding : '8 0 0 0',
		autoScroll: true,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{	
			layout: 'column',
			items: [{
				layout: 'anchor',
				columnWidth: 0.5,
				defaultType: 'displayfield',
				defaults: {
					anchor: '-20',
					labelWidth: 125
				},
				items: [{
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'displayfield',
						labelWidth: 125,
						name: 'taskName',
						margin: '0px 10px 0px 0px'
					}, {
						xtype: 'image',
						cls: 'rs-btn-edit',
						hidden: '@{!jumpToActionTaskIsVisible}',
						listeners: {
							handler: '@{jumpToActionTask}',
							render: function(image) {
								image.getEl().on('click', function() {
									image.fireEvent('handler')
								})
							}
						}
					}]
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'displayfield',
						labelWidth: 125,
						name: 'problemNumber',
						margin: '0px 10px 0px 0px'
					}, {
						xtype: 'image',
						cls: 'rs-btn-edit',
						hidden: '@{!jumpToWorksheetIsVisible}',
						listeners: {
							handler: '@{jumpToWorksheet}',
							render: function(image) {
								image.getEl().on('click', function() {
									image.fireEvent('handler')
								})
							}
						}
					}]
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'displayfield',
						labelWidth: 125,
						name: 'processNumber',
						margin: '0px 10px 0px 0px'
					}, {
						xtype: 'image',
						cls: 'rs-btn-edit',
						hidden: '@{!jumpToProcessRequestIsVisible}',
						listeners: {
							handler: '@{jumpToProcessRequest}',
							render: function(image) {
								image.getEl().on('click', function() {
									image.fireEvent('handler')
								})
							}
						}
					}]
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'displayfield',
						labelWidth: 125,
						name: 'targetGUID',
						margin: '0px 10px 0px 0px'
					}, {
						xtype: 'image',
						cls: 'rs-btn-edit',
						hidden: '@{!showGUIDDetails}',
						listeners: {
							handler: '@{displayGUIDDetails}',
							render: function(image) {
								image.getEl().on('click', function() {
									image.fireEvent('handler')
								})
							}
						}
					}]
				}, 'address', 'esbAddr']
			}, {
				layout: 'anchor',
				columnWidth: 0.5,
				defaultType: 'displayfield',
				defaults: {
					anchor: '-20',
					labelWidth: 125
				},
				items: ['duration', 'completion', 'condition', 'severity']
			}]
		}, {
			flex: 1,
			minHeight: 300,
			bodyPadding: '10px',
			cls: 'rs-monospace',
			title: '~~summary~~',
			html: '@{summaryDisplay}',
			autoScroll: true
		}]
	}, {
		layout: 'fit',
		items: [{
			autoScroll: true,
			style: 'border: 1px solid #999 !important',
			cls: 'rs-monospace',
			bodyPadding: '10px',
			html: '@{detailDisplay}',
			listeners: {
				afterrender: function(panel, eOpt) {
					clientVM.makeCtrlASelectAllOnView(panel);
				}
			}
		}]
	}, {
		layout: 'fit',
		items: [{
			autoScroll: true,
			style: 'border: 1px solid #999 !important',
			cls: 'rs-monospace',
			bodyPadding: '10px',
			html: '@{rawText}',
			listeners: {
				afterrender: function(panel, eOpt) {
					clientVM.makeCtrlASelectAllOnView(panel);
				}
			}
		}]
	}]
})
glu.defView('RS.worksheet.TaskResults', {
	padding: 15,
	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'grid',
		cls : 'rs-grid-dark',
		border: false,
		stateId: '@{stateId}',
		displayName: '@{displayName}',
		stateful: true,
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}, {
			ptype: 'columnautowidth'
		}],
		store: '@{taskResults}',
		selModel: {
			selType: 'resolvecheckboxmodel',	
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'id'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: [{
				name: 'deleteTaskResult'
			}]
		}],
		columns: '@{columns}',
		viewConfig: {
			enableTextSelection: true
		},
		listeners: {
			editAction: '@{editTaskResult}',
			selectAllAcrossPages: '@{selectAllAcrossPages}',
			selectionChange: '@{selectionChanged}'
		}
	}]
});
glu.defView('RS.worksheet.Worksheet', {
	bodyPadding: '0 15 15 15',
	layout: 'card',
	activeItem: '@{activeTab}',
	layoutConfig: {
		deferrredRender: false
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		itemId: 'actionBar',
		padding : '15 15 8 15',
		defaultButtonUI: 'display-toolbar-button',
		items: ['->', {
			name: 'generalTab',
			pressed: '@{generalTabIsPressed}'
		}, {
			name: 'resultsTab',
			pressed: '@{resultsTabIsPressed}'
		}, {
			name: 'debugTab',
			pressed: '@{debugTabIsPressed}'
		}, {
			name: 'dataTab',
			pressed: '@{dataTabIsPressed}'
		}]
	}],
	displayName: '@{worksheetTitle}',
	setDisplayName: function(value) {
		this.getPlugin('searchfilter').toolbar.down('#tableMenu').setText(value)
	},
	displayFilter: '@{displayFilter}',
	setDisplayFilter: function(value) {
		this.getPlugin('searchfilter').setDisplayFilter(value)
	},
	plugins: [{
		ptype: 'searchfilter',
		pluginId: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true,
		useWindowParams: true,
		gridName: 'resultsGrid'
	}],
	items: [{
		autoScroll: true,		
		dockedItems: [{
			xtype: 'toolbar',		
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: [{
				name: 'back',
				iconCls: 'icon-large icon-reply-all rs-icon'			
			}, {
				name: 'save',
				listeners: {
					render: function(button) {
						clientVM.updateSaveButtons(button);
					}
				}
			}, 'setActive', '->', {
				iconCls: 'rs-social-button @{socialPressedCls}',
				style: 'left: -1px',
				enableToggle: true,
				pressed: '@{socialIsPressed}',
				tooltip: '~~socialTooltip~~'
			}, {
				xtype: 'followbutton',
				streamId: '@{id}',
				streamType: 'worksheet'
			}, {
				xtype: 'sysinfobutton',
				sysId: '@{id}',
				sys_id: '',
				sysCreated: '@{sysCreatedOn}',
				sysCreatedBy: '@{sysCreatedBy}',
				sysUpdated: '@{sysUpdatedOn}',
				sysUpdatedBy: '@{sysUpdatedBy}',
				sysOrg: '@{sysOrg}',
				dateFormat: '@{userDateFormat}'
			}, {
				iconCls: 'x-btn-icon x-tbar-loading',
				tooltip: '~~refresh~~',
				handler: '@{refresh}',
				listeners: {
					render: function(button) {
						clientVM.updateRefreshButtons(button);
					}
				}
			}]
		}],
		layout: 'border',
		items: [{
			style : 'border-top:1px solid silver',
			padding : '8 0',
			region: 'center',
			autoScroll: true,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				layout: 'column',
				items: [{
					columnWidth: 0.6,
					layout: 'anchor',
					defaultType: 'textfield',
					defaults: {
						anchor: '-30',
						margin : '4 0',
						labelWidth : 130
					},
					items: [{
						xtype: 'displayfield',
						name: 'number',
						cls : 'rs-displayfield-value'
					}, 'alertId', 'reference', 'correlationId', {
						xtype: 'displayfield',
						name: 'sirId',
						cls : 'rs-displayfield-value'
					},{
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [{
							xtype: 'triggerfield',
							flex: 1,
							name: 'assignedToName',
							labelWidth : 130,
							margin: '0px 10px 0px 0px',
							trigger1Cls: Ext.baseCSSPrefix + 'form-search-trigger',
							onTrigger1Click: function() {
								this.fireEvent('handleTriggerClick')
							},
							trigger2Cls: Ext.baseCSSPrefix + 'form-clear-trigger',
							onTrigger2Click: function() {
								this.fireEvent('clearTriggerClick')
							},
							listeners: {
								handleTriggerClick: '@{assignedToClicked}',
								clearTriggerClick: '@{assignedToClickedClear}',
								registerAssignedTo: '@{registerAssignedTo}',
								render: function(field) {
									field.fireEvent('registerAssignedTo', field, field)
								}
							}
						}, {
							xtype: 'image',
							cls: 'rs-btn-edit',
							hidden: '@{!jumpToAssignedToIsVisible}',
							listeners: {
								handler: '@{jumpToAssignedTo}',
								render: function(image) {
									image.getEl().on('click', function() {
										image.fireEvent('handler')
									})
								}
							}
						}]
					}]
				}, {
					columnWidth: 0.4,
					layout: 'anchor',
					defaults: {
						anchor: '100%',
						labelWidth : 130,
						editable : false
					},
					items: [{
						xtype: 'combobox',
						name: 'condition',
						store: '@{conditionStore}',
						queryMode: 'local',
						displayField: 'name',
						valueField: 'value'
					}, {
						xtype: 'combobox',
						name: 'severity',
						store: '@{severityStore}',
						queryMode: 'local',
						displayField: 'name',
						valueField: 'value'
					}]
				}]
			}, {
				xtype: 'textfield',
				name: 'summary',
				labelWidth : 130
			}, {
				xtype: 'textarea',
				name: 'description',
				labelWidth : 130
			}, {
				xtype: 'textarea',
				name: 'workNotes',
				labelWidth : 130
			}, {
				xtype: 'grid',
				cls : 'rs-grid-dark',
				flex: 1,
				store: '@{workNotesDataStore}',
				columns: '@{workNotesColumns}'
			}]
		}, {
			region: 'east',
			split: true,
			width: 480,
			stateId: 'wikiSocialBar',
			stateful: true,
			hidden: '@{!socialIsPressed}',
			layout: 'fit',
			items: [{
				xtype: '@{socialDetail}'
			}]
		}]
	}, {
		layout: 'border',
		dockedItems: [{
			xtype: 'toolbar',		
			name: 'actionBar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'		
			},
			items: [{
				name: 'back',
				iconCls: 'icon-large icon-reply-all rs-icon'				
			}, '|', 'setActive', {
				name: 'showAll',
				cls : 'rs-small-btn rs-toggle-btn',
				enableToggle: true,
				pressed: '@{all}'
			}, {
				iconCls: 'rs-social-button @{socialPressedCls}',
				style: 'left: -1px',
				enableToggle: true,
				pressed: '@{socialIsPressed}',
				tooltip: '~~socialTooltip~~'
			}]
		}],
		socialId: '@{id}',
		setSocialId: function(value) {
			this.getPlugin('pager').setSocialId(value)
		},
		plugins: [{
			ptype: 'pager',
			pluginId: 'pager',
			showSocial: true,
			socialStreamType: 'worksheet'
		}],
		items: [{
			region: 'center',
			xtype: 'grid',
			cls : 'rs-grid-dark',
			viewConfig: {
				enableTextSelection: true
			},
			store: '@{worksheetResults}',
			columns: '@{worksheetColumns}',
			stateId: '@{stateId}',
			stateful: true,
			itemId: 'resultsGrid',
			selModel: {
				selType: 'resolvecheckboxmodel',
				columnTooltip: RS.common.locale.editColumnTooltip,
				columnTarget: '_self',
				columnEventName: 'editAction',
				columnIdField: 'sysId'
			},
			plugins: [{
				ptype: 'resolveexpander',
				rowBodyTpl: new Ext.XTemplate(
					// '<span style="font-weight:bold;padding-left:35px">Queue Name</span>--{esbaddr}<br/>',
					// '<span style="font-weight:bold;padding-left:35px">Duration</span>--{duration}<br/>',
					// '<span style="font-weight:bold;padding-left:35px">Process Request</span>--{processNumber}<br/>',
					// '<span style="font-weight:bold;padding-left:35px">Target</span>--{address}<br/>',
					// '<span style="font-weight:bold;padding-left:35px">Guid</span>--{targetGUID}<br/>',
					'<div resolveId="resolveRowBody" style="color:#333">',
					'<span style="padding-left:10px">Detail:</span><br/>',
					'<span>{[this.doDetai(values)]}</span>',
					'</div>', {
						doDetai: function(detail) {
							return Ext.util.Format.nl2br('<pre style="padding-left:10px;padding-top:10px">' + Ext.String.htmlEncode(detail.detail) + '</pre>')
						}
					}
				)
			}],
			listeners: {
				editAction: '@{editResult}'
			}
		}, {
			region: 'east',
			split: true,
			width: 480,
			stateId: 'wikiSocialBar',
			stateful: true,
			hidden: '@{!socialIsPressed}',
			layout: 'fit',
			items: [{
				xtype: '@{socialDetail}'
			}]
		}]
	}, {
		layout: 'border',
		tbar: {
			xtype: 'toolbar',
			cls: 'actionBar rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: [{
				name: 'back',
				iconCls: 'icon-large icon-reply-all rs-icon'
			}, '->', {
				iconCls: 'rs-social-button @{socialPressedCls}',
				style: 'left: -1px',
				enableToggle: true,
				pressed: '@{socialIsPressed}',
				tooltip: '~~socialTooltip~~'
			}, {
				xtype: 'followbutton',
				streamId: '@{id}',
				streamType: 'worksheet'
			}, {
				xtype: 'sysinfobutton',
				sysId: '@{id}',
				sys_id: '',
				sysCreated: '@{sysCreatedOn}',
				sysCreatedBy: '@{sysCreatedBy}',
				sysUpdated: '@{sysUpdatedOn}',
				sysUpdatedBy: '@{sysUpdatedBy}',
				sysOrg: '@{sysOrg}',
				dateFormat: '@{userDateFormat}'
			}, {
				iconCls: 'x-btn-icon x-tbar-loading',
				tooltip: '~~refresh~~',
				handler: '@{refresh}'
			}]
		},
		items: [{			
			region: 'center',
			xtype: 'textarea',
			readOnly: true,
			labelAlign: 'top',
			hideLabel: true,
			name: 'debug'
		}, {
			region: 'east',
			split: true,
			width: 480,
			stateId: 'wikiSocialBar',
			stateful: true,
			hidden: '@{!socialIsPressed}',
			layout: 'fit',
			items: [{
				xtype: '@{socialDetail}'
			}]
		}]
	}, {
		layout: 'border',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: [{
				name: 'back',
				iconCls: 'icon-large icon-reply-all rs-icon'		
			}, 'addWSData', 'deleteWSData', '->', {
				iconCls: 'rs-social-button @{socialPressedCls}',
				style: 'left: -1px',
				enableToggle: true,
				pressed: '@{socialIsPressed}',
				tooltip: '~~socialTooltip~~'
			}, {
				xtype: 'followbutton',
				streamId: '@{id}',
				streamType: 'worksheet'
			}, {
				xtype: 'sysinfobutton',
				sysId: '@{id}',
				sys_id: '',
				sysCreated: '@{sysCreatedOn}',
				sysCreatedBy: '@{sysCreatedBy}',
				sysUpdated: '@{sysUpdatedOn}',
				sysUpdatedBy: '@{sysUpdatedBy}',
				sysOrg: '@{sysOrg}',
				dateFormat: '@{userDateFormat}'
			}, {
				iconCls: 'x-btn-icon x-tbar-loading',
				tooltip: '~~refresh~~',
				handler: '@{refresh}'
			}]
		}],
		items: [{
			region: 'center',
			xtype: 'grid',
			cls : 'rs-grid-dark',
			name: 'wsData',
			columns: '@{wsColumns}',
			selModel: {
				selType: 'resolvecheckboxmodel',
				columnTooltip: RS.common.locale.editColumnTooltip,
				glyph : 0xF044,
				columnTarget: '_self',
				columnEventName: 'editAction',
				columnIdField: 'name'
			},
			listeners: {
				editAction: '@{editWSData}'
			}
		}, {
			region: 'east',
			split: true,
			width: 480,
			stateId: 'worksheetSocialBar',
			stateful: true,
			hidden: '@{!socialIsPressed}',
			layout: 'fit',
			items: [{
				xtype: '@{socialDetail}'
			}]
		}]
	}],
	listeners: {
		afterrender: function() {
			var splitters = this.query('bordersplitter')
			Ext.Array.forEach(splitters, function(splitter) {
				splitter.setSize(1, 1)
				splitter.getEl().setStyle({
					backgroundColor: '#fbfbfb'
				})
			})
			this.setDisplayFilter(this.displayFilter)
		}
	}
})

glu.defView('RS.worksheet.WorksheetData', {
	height: 400,
	width: 600,
	padding : 15,
	modal : true,
	cls : 'rs-modal-popup',
	title: '~~worksheetDataTitle~~',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'displayfield',
		name: 'name',
		hidden: '@{!editMode}'
	}, {
		xtype: 'textfield',
		name: 'name',
		hidden: '@{editMode}'
	}, {
		xtype: 'textarea',
		name: 'value',
		flex: 1
	}],
	buttons: [{
		name : 'save',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})
glu.defView('RS.worksheet.WorksheetPicker', {
	title: '~~selectWorksheetTitle~~',
	padding : 10,
	hidden: '@{!showPicker}',		
	width: 650,
	height: 400,
	//ui: '@{ui}',
	cls : 'rs-modal-popup',
	closeAction: 'hide',
	//baseCls: '@{baseCls}',
	draggable: false,
	resizable: false,
	target: '@{target}',
	modal: '@{!clientDialog}',
	listeners: {
		show: function(win) {
			if (win.target) {
				win.alignTo(win.target.getEl().id, 'tr-br')
			}
		},
		beforeclose: '@{beforeClose}'
	},
	layout: 'fit',
	items: [{		
		xtype: 'grid',
		name: 'worksheets',
		cls : 'rs-grid-dark',
		margin : '0 0 10 0',
		flex: 1,
		columns: '@{worksheetsColumns}',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			items: [{
				xtype: 'textfield',
				name: 'searchText',
				hideLabel: true,
				width: 250,
				emptyText: '~~searchWorksheet~~'
			}, {
				iconCls: 'icon-large icon-user rs-icon @{myWorksheetsPressedCls}',
				name: 'myWorksheets',
				text: '',
				tooltip: '@{myWorksheetsTooltip}'
			}]
		}],
		plugins: [{
			ptype: 'pager',
			showSysInfo: false,
			allowAutoRefresh: false
		}, {
			ptype: 'rowexpander',
			pluginId: 'rowExpander',
			rowBodyTpl: new Ext.XTemplate('<span name="replace"></span>')
		}],
		viewConfig: {
			listeners: {
				expandbody: function(rowNode, record, expandRow, eOpts) {
					this.ownerCt.fireEvent('expandbody', this.ownerCt, rowNode, record, expandRow, eOpts)
				}
			}
		},
		listeners: {
			expandbody: '@{expandBody}',
			itemdblclick: '@{dblclick}',
			render: function(grid) {
				var plugin = grid.getPlugin('rowExpander')
				if (plugin)
					grid.on('cellclick', function(view, td, cellIndex, record, tr, rowIndex, e, eOpts) {
						if (cellIndex > 0) {
							plugin.onDblClick(view, record, null, rowIndex, e)
						}
					})
				grid.store.on('beforeload', function() {
					plugin.recordsExpanded = {}
				})
			}
		}
	}],
	buttonAlign: 'left',
	buttons: [{
		name: 'go',
		cls: 'rs-proceed-button rs-med-button',
		iconCls: 'icon-large icon-play rs-client-button'
	}, {
		name : 'select',
		cls: 'rs-med-button rs-btn-light',
	},{
		name : 'newWorksheet',
		cls: 'rs-med-button rs-btn-light',
	},{
		name : 'view',
		cls: 'rs-med-button rs-btn-light',
	},{
		name : 'viewAll',
		cls: 'rs-med-button rs-btn-light',
	},{
		name :  'cancel',
		cls: 'rs-med-button rs-btn-light',
	}]
})
glu.defView('RS.worksheet.WorksheetPicker', 'windowParam', {

	asWindow: {
		title: '~~selectWorksheetTitle~~',
		hidden: '@{!showPicker}',
		width: 600,
		height: 400,
		ui: '@{ui}',
		baseCls: '@{baseCls}',
		draggable: false,
		resizable: false,
		target: '@{target}',
		modal: '@{!clientDialog}',
		listeners: {
			show: function(win) {
				if (win.target) {
					win.alignTo(win.target.getEl().id, 'tr-br')
				}
			},
			beforeclose: '@{beforeClose}'
		}
	},

	layout: 'fit',
	dockedItems: [{
		xtype: 'toolbar',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{tabTitle}'
		}, '->', 'worksheetTab', 'archivedWorksheetTab']
	}],

	items: [{
		xtype: 'grid',
		name: 'worksheets',
		hidden: '@{!worksheetTabIsPressed}',
		flex: 1,
		columns: '@{worksheetsColumns}',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: [{
				xtype: 'textfield',
				name: 'searchText',
				hideLabel: true,
				width: 250,
				emptyText: '~~searchWorksheet~~'
			}, {
				iconCls: 'icon-large icon-user rs-icon @{myWorksheetsPressedCls}',
				name: 'myWorksheets',
				text: '',
				tooltip: '@{myWorksheetsTooltip}'
			}]
		}],
		plugins: [{
			ptype: 'pager',
			showSysInfo: false,
			allowAutoRefresh: false
		}, {
			ptype: 'rowexpander',
			pluginId: 'rowExpander',
			rowBodyTpl: new Ext.XTemplate('<span name="replace"></span>')
		}],
		viewConfig: {
			listeners: {
				expandbody: function(rowNode, record, expandRow, eOpts) {
					this.ownerCt.fireEvent('expandbody', this.ownerCt, rowNode, record, expandRow, eOpts)
				}
			}
		},
		listeners: {
			expandbody: '@{expandBody}',
			itemdblclick: '@{dblclick}',
			render: function(grid) {
				var plugin = grid.getPlugin('rowExpander')
				if (plugin)
					grid.on('cellclick', function(view, td, cellIndex, record, tr, rowIndex, e, eOpts) {
						if (cellIndex > 0) {
							plugin.onDblClick(view, record, null, rowIndex, e)
						}
					})
				grid.store.on('beforeload', function() {
					plugin.recordsExpanded = {}
				})
			}
		}
	}, {
		xtype: 'grid',
		name: 'archived',
		hidden: '@{!archivedWorksheetTabIsPressed}',
		store: '@{archivedWorksheets}',
		columns: [{
			header: '~~number~~',
			flex: 1,
			dataIndex: 'number'
		}, {
			header: '~~reference~~',
			flex: 1,
			dataIndex: 'reference',
			renderer: function(value, metaData, record) {
				return Ext.String.htmlEncode(value) || record.get('correlationId') || record.get('alertId') || ''
			}
		}, {
			text: RS.common.locale.sysUpdated,
			dataIndex: 'sysUpdatedOn',
			filterable: false,
			sortable: true,
			width: 200,
			dateFormat: '@{userDateFormat}'
		}],
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: [{
				xtype: 'textfield',
				name: 'searchText',
				hideLabel: true,
				width: 250,
				emptyText: '~~searchWorksheet~~'
			}, {
				iconCls: 'icon-large icon-user rs-icon @{myWorksheetsPressedCls}',
				name: 'myWorksheets',
				text: '',
				tooltip: '@{myWorksheetsTooltip}'
			}]
		}],
		plugins: [{
			ptype: 'pager',
			showSysInfo: false,
			allowAutoRefresh: false
		}, {
			ptype: 'rowexpander',
			pluginId: 'rowExpander',
			rowBodyTpl: new Ext.XTemplate('<span name="replace"></span>')
		}],
		viewConfig: {
			listeners: {
				expandbody: function(rowNode, record, expandRow, eOpts) {
					this.ownerCt.fireEvent('expandbody', this.ownerCt, rowNode, record, expandRow, eOpts)
				}
			}
		},
		listeners: {
			expandbody: '@{expandBody}',
			itemdblclick: '@{dblclick}',
			render: function(grid) {
				var plugin = grid.getPlugin('rowExpander')
				if (plugin)
					grid.on('cellclick', function(view, td, cellIndex, record, tr, rowIndex, e, eOpts) {
						if (cellIndex > 0) {
							plugin.onDblClick(view, record, null, rowIndex, e)
						}
					})
				grid.store.on('beforeload', function() {
					plugin.recordsExpanded = {}
				})
			}
		}
	}],
	buttonAlign: 'left',
	buttons: [{
		name: 'go',
		iconCls: 'icon-large icon-play rs-client-button'
	}, 'select', 'newWorksheet', 'view', 'viewAll', 'cancel']
})
glu.defView('RS.worksheet.Worksheets', {
	bodyPadding: 15,
	layout: 'fit',
	items: [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'worksheets',
		border: false,
		stateId: '@{stateId}',
		displayName: '@{displayName}',
		stateful: true,
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}, {
			ptype: 'columnautowidth'
		}],
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTooltip: RS.common.locale.editColumnTooltip,
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'id'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: ['createWorksheet', 'setActive', 'deleteWorksheet']
		}],
		columns: '@{columns}',
		viewConfig: {
			enableTextSelection: true
		},
		listeners: {
			// showDetail: '@{showDetails}',
			editAction: '@{editWorksheet}',
			selectAllAcrossPages: '@{selectAllAcrossPages}',
			selectionChange: '@{selectionChanged}',
			beforedestroy : '@{beforeDestroyComponent}'
		}
	}]
});
/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.worksheet').locale = {

	//Worksheet Columns
	created: 'Created',
	number: 'Number',
	condition: 'Condition',
	summary: 'Summary',
	severity: 'Severity',
	reference: 'Reference',
	lookupId: 'Lookup ID',
	alertId: 'Alert ID',
	correlationId: 'Correlation ID',
	sirId : 'SIR ID',
	organization: 'Organization',
	assignedTo: 'Assigned User ID',
	assignedToName: 'Assigned To',

	sysCreatedBy: 'Created By',
	sysCreatedOn: 'Created On',
	note: 'Note',

	//Worksheet Data Columns
	name: 'Name',
	value: 'Value',

	socialTooltip: 'Social',

	//Result grid columns
	lastUpdated: 'Last Updated',
	actionTask: 'Action Task',
	completion: 'Completion',
	condition: 'Condition',
	severity: 'Severity',
	summary: 'Summary',
	processRequest: 'Process Request',
	taskRequest: 'Task Request',
	duration: 'Duration',
	queueName: 'Queue Name',
	target: 'Target',
	guid: 'GUID',
	updatedBy: 'Updated By',
	detail: 'Detail',

	//Task Results Columns
	duration: 'Duration',
	worksheet: 'Worksheet',

	//Process Requests Columns
	status: 'Status',
	runbook: 'Runbook',
	wiki: 'Runbook',
	problemNumber: 'Worksheet',

	cancel: 'Cancel',



	Worksheets: {
		allWorksheets: 'Worksheets',
		oldWorksheet: 'Unable to set worksheet as selected because it has expired',

		//Actions
		createWorksheet: 'New',
		editWorksheet: 'Edit',
		setActive: 'Select',
		deleteWorksheet: 'Delete',

		//Delete Action
		deleteTitle: 'Confirm Deletion',
		deleteMessage: 'Are you sure you want to delete the selected worksheet?<br><font color=\'red\'> NOTE: Incident related worksheets or active worksheets will NOT be deleted.</font>',
		deletesMessage: 'Are you sure you want to delete the {0} selected worksheets?<br><font color=\'red\'> NOTE: Incident related worksheet(s) will NOT be deleted.</font>',
		deleteAction: 'Delete',
		cancel: 'Cancel',

		//Select Action
		setActiveTitle: 'Confirm Select',
		setActiveMessage: 'Are you sure you want to make {0} the selected worksheet?',
		setActiveAction: 'Select',
		worksheetSetActiveSuccess: 'Worksheet {0} is now selected, <a href="#RS.worksheet.Worksheet/id={1}">click to view</a>'
	},

	ArchivedWorksheets: {
		allArchivedWorksheets: 'Archived Worksheets',
		setActive: 'Select',
		setActiveTitle: 'Confirm Select',
		setActiveMessage: 'Are you sure you want to make {0} the selected worksheet?',
		setActiveAction: 'Select',
		archivedWorksheetSetActiveSuccess: 'Worksheet {0} is now selected, <a href="#RS.worksheet.ArchivedWorksheet/id={1}">click to view</a>'
	},

	ArchivedWorksheet: {
		//Conditions
		good: 'Good',
		bad: 'Bad',
		unknown: 'Unknown',

		//Severity
		critical: 'Critical',
		severe: 'Severe',
		warning: 'Warning',

		archivedWorksheet: 'Archived Worksheet',
		setActive: 'Select',
		generalTab: 'General',
		resultsTab: 'Results',
		debugTab: 'Debug',
		dataTab: 'Data',
		showAll: 'Advanced View',
		description: 'Description',
		workNotes: 'Work Notes',
		setActiveTitle: 'Confirm Select',
		setActiveMessage: 'Are you sure you want to make {0} the selected worksheet?',
		setActiveAction: 'Select',
		archivedWorksheetSetActiveSuccess: 'Worksheet {0} is now selected, <a href="#RS.worksheet.ArchivedWorksheet/id={1}">click to view<a>'
	},

	Worksheet: {
		//Tabs
		generalTab: 'General',
		resultsTab: 'Results',
		debugTab: 'Debug',
		dataTab: 'Data',

		//Actions
		save: 'Save',
		saveAs: 'Save As',
		saveAndExit: 'Save & Exit',
		back: 'Back',
		refresh: 'Refresh',
		setActive: 'Select',
		showAll: 'Advanced View',

		createdBy: 'Created By',
		created: 'Created',
		createdText: 'Created',
		description: 'Description',
		workNotes: 'Work Notes',

		xml: 'XML',
		debug: 'Debug',

		//Conditions
		good: 'Good',
		bad: 'Bad',
		unknown: 'Unknown',

		//Severity
		critical: 'Critical',
		severe: 'Severe',
		warning: 'Warning',

		saveTitle: 'Success',
		saveMessage: 'Worksheet saved',

		addWSData: 'Add',

		//Select Action
		setActiveTitle: 'Confirm Select',
		setActiveMessage: 'Are you sure you want to make {0} the active worksheet?',
		setActiveAction: 'Select',
		worksheetSetActiveSuccess: 'Worksheet {0} is now active, <a href="#RS.worksheet.Worksheet/id={1}">click to view<a>',

		confirmWSDataDelete: 'Confirm Data Deletion',
		deleteWSDataMessage: 'Are you sure you want to delete the selected data record?',
		deleteWSDatasMessage: 'Are you sure you want to delete the {0} selected data records?',
		deleteWSData: 'Delete',
		wsDataDeleted: 'Data successfully deleted',
		actionTask: 'Action Task'

	},

	TaskResults: {
		taskResults: 'Task Results',

		//Actions
		deleteTaskResult: 'Delete',
		deleteTitle: 'Delete Task Result',
		deleteMessage: 'Are you sure you want to delete the selected task result?',
		deletesMessage: 'Are you sure you want to delete the {0} selected task results?',
		deleteAction: 'Delete',
		cancel: 'Cancel'
	},

	ArchivedTaskResult: {
		taskResultTitle: 'Task Result',

		actionTask: 'Action Task',
		actionTaskFullName: 'Action Task',
		actionTaskName: 'Action Task',
		taskName: 'Action Task',
		problemNumber: 'Worksheet',
		processNumber: 'Process Request',
		address: 'Target',
		esbAddr: 'Queue Name',
		targetGUID: 'GUID',
		address: 'Target',
		created: 'Created',
		createdText: 'Created',
		createdBy: 'Created By',
		lastUpdated: 'Last Updated',
		lastUpdatedText: 'Last Updated',
		lastUpdatedBy: 'Last Updated By',
		duration: 'Duration',
		summary: 'Summary',
		summaryDisplay: 'Summary',

		// Tabs
		generalTab: 'General',
		detailTab: 'Detail',
		rawTab: 'Raw',

		// Actions
		back: 'Back'

	},

	TaskResult: {
		taskResultTitle: 'Task Result',

		actionTask: 'Action Task',
		actionTaskFullName: 'Action Task',
		actionTaskName: 'Action Task',
		taskName: 'Action Task',
		problemNumber: 'Worksheet',
		processNumber: 'Process Request',
		address: 'Target',
		esbAddr: 'Queue Name',
		targetGUID: 'GUID',
		address: 'Target',
		created: 'Created',
		createdText: 'Created',
		createdBy: 'Created By',
		lastUpdated: 'Last Updated',
		lastUpdatedText: 'Last Updated',
		lastUpdatedBy: 'Last Updated By',
		duration: 'Duration',
		summary: 'Summary:',
		summaryDisplay: 'Summary',

		// Tabs
		generalTab: 'General',
		detailTab: 'Detail',
		rawTab: 'Raw',

		// Actions
		back: 'Back'

	},

	ProcessRequests: {
		processRequests: 'Process Requests',

		//Actions
		deleteProcessRequest: 'Delete',
		deleteAllProcessRequest : 'Delete All',
		deleteTitle: 'Delete Process Request',
		deleteAllTitle: 'Delete All Process Request',
		deleteMessage: 'Are you sure you want to delete the selected process request?',
		deletesMessage: 'Are you sure you want to delete the {0} selected process requests?',
		deleteAllMessage: 'Are you sure you want to delete the {0} <b>{1}</b> process requests?',
		deleteAction: 'Delete',
		cancel: 'Cancel',

		abortProcessRequest: 'Abort',
		abortAllProcessRequest : 'Abort All',
		abortTitle: 'Abort Process Request',
		abortAllTitle: 'Abort All Process Request',
		abortMessage: 'Are you sure you want to abort the selected process request?',
		abortsMessage: 'Are you sure you want to abort the {0} selected process requests?',
		abortAllMessage: 'Are you sure you want to abort the {0} <b>{1}</b> process requests?',
		abortAction: 'Abort',
		cancel: 'Cancel'
	},

	ProcessRequest: {
		processRequestTitle: 'Process Request',

		//Actions
		back: 'Back',
		abort: 'Abort',
		abortTitle: 'Abort Task Request',
		abortMessage: 'Are you sure you want to abort the selected process request?',
		abortsMessage: 'Are you sure you want to abort the {0} selected process requests?',
		abortAction: 'Abort',
		cancel: 'Cancel',

		number: 'Process Request',
		worksheet: 'Worksheet',
		runbook: 'Runbook',
		timeout: 'Timeout (mins)',
		status: 'Status',
		created: 'Created',
		createdText: 'Created',
		createdBy: 'Created By',
		lastUpdated: 'Last Updated',
		lastUpdatedText: 'Last Updated',
		lastUpdatedBy: 'Last Updated By',
		duration: 'Duration',

		taskRequests: 'Task Requests',

		problemNumber: 'Worksheet'

	},

	TaskRequests: {
		taskRequests: 'Task Requests',

		//Actions
		deleteTaskRequest: 'Delete',
		deleteTitle: 'Delete Task Request',
		deleteMessage: 'Are you sure you want to delete the selected task request?',
		deletesMessage: 'Are you sure you want to delete the {0} selected task requests?',
		deleteAction: 'Delete',
		cancel: 'Cancel',

		abortTaskRequest: 'Abort',
		abortTitle: 'Abort Task Request',
		abortMessage: 'Are you sure you want to abort the selected task request?',
		abortsMessage: 'Are you sure you want to abort the {0} selected task requests?',
		abortAction: 'Abort'
	},

	TaskRequest: {
		taskRequestTitle: 'Task Request',
		taskRequest: 'Number',
		status: 'Execute Status',
		runbook: 'Runbook',
		processRequest: 'Action Process',
		worksheet: 'Worksheet',
		actionTask: 'Action Task',
		created: 'Created',
		createdText: 'Created',
		createdBy: 'Created By',
		lastUpdated: 'Last Updated',
		lastUpdatedText: 'Last Updated',
		lastUpdatedBy: 'Last Updated By',
		nodeId: 'Node ID',
		duration: 'Duration',

		//Actions
		back: 'Back',
		refresh: 'Refresh',
		abort: 'Abort',

		abortTitle: 'Abort Task Request',
		abortMessage: 'Are you sure you want to abort the selected task request?',
		abortsMessage: 'Are you sure you want to abort the {0} selected task requests?',
		abortAction: 'Abort',
		cancel: 'Cancel'
	},

	WorksheetPicker: {
		selectWorksheetTitle: 'Select Worksheet',
		go: 'Go',
		select: 'Select',
		continueWorksheet: 'Continue',
		newWorksheet: 'New',
		view: 'View',
		viewAll: 'View All',
		close: 'Close',
		cancel: 'Cancel',

		searchWorksheet: 'Search worksheets...',

		showMyWorksheets: 'Click to show my worksheets',
		showAllWorksheets: 'Click to show all worksheets',
		archivedWorksheetsNotFound: 'Cannot find the specified ws in archived worksheet.',
		worksheetSetActiveSuccess: 'Worksheet {0} is now selected, <a href="#RS.worksheet.Worksheet/id={1}">click to view<a>',
		archivedWorksheetSelectSuccess: 'ArchivedWorksheet {0} is now selected, <a href="#RS.worksheet.ArchivedWorksheet/id={1}">click to view<a>',
		newWorksheetActiveSuccess: 'New worksheet created and is now selected, <a href="#RS.worksheet.Worksheet/id={0}">click to view</a>',

		noWorksheetSelected: 'No worksheet was selected',
		worksheetTab: 'Worksheet',
		archivedWorksheetTab: 'Archived',
		esTitle: 'Worksheet',
		archivedTitle: 'Archived',
		listWorksheetError: 'Cannot fetch worksheets from the server. [{0}]'
	},

	WorksheetData: {
		worksheetDataTitle: 'Worksheet Data',
		save: 'Save',
		cancel: 'Cancel',

		nameInvalid: 'Please provide a property name',

		propertySaved: 'Property saved successfully'
	},

	ListRegistrationDetails: {
		title: 'Registration Details',

		uname: 'Name',
		ustatus: 'Status',
		uguid: 'Guid',
		utype: 'Type',
		uipaddress: 'Reg Address',
		ubuild: 'Build',

		sysUpdatedOn: 'Updated On',
		sysUpdatedBy: 'Updated By',

		close: 'Close'
	}
}

