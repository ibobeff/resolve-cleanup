/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/
glu.defModel('RS.search.AdvanceSearchWindow', {
	fields: ['keyword', 'targetId', 'sysCreatedBy', 'sysUpdatedBy', 'authorUsername', 'commentAuthorUsername', {
		name: 'type',
		type: 'array'
	}, {
		name: 'namespace',
		type: 'array'
	}, {
		name: 'updatedOn',
		type: 'time'
	}, {
		name: 'updatedOnTime',
		type: 'time'
	}, 'updatedOnCondition', {
		name: 'createdOn',
		type: 'time'
	}, 'createdOnCondition', {
		name: 'createdOnTime',
		type: 'time'
	}, {
		name: 'field',
		type: 'array'
	}, {
		name: 'tag',
		type: 'array'
	}],

	defaultData: {
		namespace: [],
		updatedOn: '',
		updatedOnTime: '',
		authorUsername: '',
		commentAuthorUsername: '',
		updatedOnCondition: 'on',
		createdOn: '',
		createdOnTime: '',
		createdOnCondition: 'on',
		field: '',
		tag: [],
		targetId: '',
		type: ''
	},
	targetData: {
		targetId: ''
	},
	sysCreatedBy: '',
	sysCreatedByIsVisible$: function() {
		return this.activeItem != 1;
	},
	sysUpdatedBy: '',
	sysUpdatedByIsVisible$: function() {
		return this.activeItem != 1;
	},
	authorUsername: '',
	authorUsernameIsVisible$: function() {
		return this.activeItem == 1;
	},
	commentAuthorUsername: '',
	commentAuthorUsernameIsVisible$: function() {
		return this.activeItem == 1;
	},
	keyword: '',
	namespace: [],
	namespaceIsVisible$: function() {
		return this.activeItem != 1;
	},
	target: null,
	assembleDateTime: function(date, time) {
		if (!date)
			return null;
		var strDate = Ext.Date.format(date, 'Y-m-d');
		var strTime = Ext.Date.format((time ? time : Ext.Date.clearTime(new Date())), 'H:i:s');
		var strDateTime = strDate + ' ' + strTime;
		var dateTime = Ext.Date.parse(strDateTime, 'Y-m-d H:i:s');
		return dateTime;
	},
	updatedOnTimeObj: null,
	updatedOn: null,
	updatedOnCondition: 'on',
	timeConditionStore: {
		mtype: 'store',
		fields: ['condition'],
		data: [{
			condition: 'before'
		}, {
			condition: 'on'
		}, {
			condition: 'after'
		}]
	},
	updatedOnChanged$: function() {
		this.set('updatedOnTimeObj', this.assembleDateTime(this.updatedOn, this.updatedOnTime));
	},
	updatedOnValidFlag: true,
	setUpdatedOnValid: function(isValid) {
		this.set('updatedOnValidFlag', isValid);
	},
	updatedOnTime: null,
	updatedOnTimeChanged$: function() {
		this.updatedOnChanged$();
	},
	updatedOnTimeValidFlag: true,
	setUpdatedOnTimeValid: function(isValid) {
		this.set('updatedOnTimeValidFlag', isValid);
	},
	createdOnTimeObj: null,
	createdOn: null,
	createdOnCondition: 'on',
	createdOnValidFlag: true,
	createdOnChanged$: function() {
		this.set('createdOnTimeObj', this.assembleDateTime(this.createdOn, this.createdOnTime));
	},
	setCreatedOnValid: function(isValid) {
		this.set('createdOnValidFlag', isValid);
	},
	createdOnTime: null,
	createdOnTimeValidFlag: true,
	createdOnTimeChanged$: function() {
		this.createdOnChanged$();
	},
	setCreatedOnTimeValid: function(isValid) {
		this.set('createdOnTimeValidFlag', isValid);
	},
	targetId: '',
	targetIdChanged$: function() {
		//this.targetIds.clearFilter();
		/*this.targetIds.filter({
			property: 'displayName',
			anyMatch: true,
			value: this.targetId
		});*/
	},
	targetIds: {
		mtype: 'store',
		fields: ['displayName', 'id', 'type'],
		proxy: {
			type: 'memory'
		}
	},
	targetIdIsVisible$: function() {
		return this.activeItem == 1;
	},
	field: [],
	tag: [],
	searchIsEnabled$: function() {
		return this.updatedOnValidFlag && this.updatedOnTimeValidFlag && this.createdOnValidFlag && this.createdOnTimeValidFlag;
	},
	type: '',
	typeIsVisible$: function() {
		return this.activeItem != 1;
	},
	activeItem: 0,
	init: function() {
		switch (this.activeItem) {
			case 0:
				this.typeStore.add([{
					name: 'Document',
					value: 'document'
				}, {
					name: 'Decision Tree',
					value: 'decisiontree'
				}, {
					name: 'Playbook Template',
					value: 'playbook'
				}]);
				break;
			case 1:
				this.typeStore.add([{
					name: 'Post',
					value: 'post'
				}]);
				break;
			case 2:
				this.typeStore.add([{
					name: 'Automation',
					value: 'automation'
				}, {
					name: 'ActionTask',
					value: 'actiontask'
				}]);
				break;
			default:
		}
		if (this.activeItem == 0)
			this.fieldStore.add({
				name: this.localize('attachment'),
				value: 'attachment'
			}, {
				name: this.localize('attachmentContent'),
				value: 'attachmentContent'
			});
		if (this.activeItem != 1)
			this.fieldStore.add([{
					name: this.localize('fullName'),
					value: 'fullName'
				}, {
					name: this.localize('summary'),
					value: 'summary'
				}
				// {
				// 	name: this.localize('sysCreatedBy'),
				// 	value: 'sysCreatedBy'
				// }, {
				// 	name: this.localize('sysUpdatedBy'),
				// 	value: 'sysUpdatedBy'
				// }
			]);
		if (this.activeItem != 2)
			this.fieldStore.add([{
				name: this.localize(this.activeItem != 1 ? 'title' : 'subject'),
				value: 'title'
			}, {
				name: this.localize('content'),
				value: 'content'
			}]);

		if (this.activeItem == 1)
			this.fieldStore.add([{
					name: this.localize('comment'),
					value: 'socialcomment.content'
				}
				// {
				// 	name: this.localize('authorUsername'),
				// 	value: 'authorUsername'
				// }, {
				// 	name: this.localize('commentAuthorUsername'),
				// 	value: 'socialcomment.comment_Author_Us_ern_ame'
				// }
			])
		if (Ext.state.Manager.get('lastSearch' + this.activeItem)) {
			var lastSearch = Ext.state.Manager.get('lastSearch' + this.activeItem);
			var lastFields = [];
			if (lastSearch.field) {
				if (Ext.isArray(lastSearch.field))
					lastFields = lastSearch.field;
				else
					lastFields = lastSearch.field.split(',');
			}
			var validatedFields = [];
			Ext.each(lastFields, function(f) {
				this.fieldStore.each(function(current) {
					if (f == current.get('value'))
						validatedFields.push(f);
				});
			}, this);
			lastSearch.field = validatedFields;
			lastSearch.keyword = this.keyword;
			this.loadData(lastSearch)
		}

		this.tagStore.on('beforeload', function(store, op) {
			op.params = op.params || {};
			var target = '';
			if (typeof this.tag == 'object')
				target = this.tag && this.tag.length > 0 ? this.tag[0] : '';
			else {
				target = this.tag.split(',');
				target = target[target.length - 1]
			}
			op.params.filter = Ext.encode([{
				"field": "name",
				"type": "auto",
				"condition": "contains",
				"value": target
			}]);
		}, this);

		this.namespaceStore.on('beforeload', function(store, op) {
			op.params = op.params || {};
			var target = '';
			if (typeof this.namespace == 'object')
				target = this.namespace && this.namespace.length > 0 ? this.namespace[0] : '';
			else {
				target = this.namespace.split(',');
				target = target[target.length - 1];
			}
			op.params.filter = Ext.encode([{
				"field": "unamespace",
				"type": "auto",
				"condition": "contains",
				"value": target
			}]);
		}, this);

		this.ajax({
			url: '/resolve/service/social/getUserStreams',
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('getTargetIdError', {
						msg: respData.message
					}));
					return;
				}
				Ext.each(respData.records, function(r) {
					if (!r.type || r.type.toLowerCase() == 'namespace')
						return;
					this.targetIds.add(r);
				}, this);
				this.targetIds.sort('displayName', 'ASC');
				var targetId = this.targetId;
				this.set('targetId', null);
				this.set('targetId', targetId.split(','));
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	namespaceStore: {
		mtype: 'store',
		fields: ['unamespace'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/namespaces',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			},
			limitParam: null
		}
	},

	fieldStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	tagStore: {
		mtype: 'store',
		fields: ['name'],
		sorters: [{
			property: 'name',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/tag/listTags',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	typeStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	collectFilters: function() {
		var filters = [];
		var fields = [];
		Ext.each(this.field, function(f) {
			fields.push(f);
		});

		filters.push({
			field: Ext.isArray(fields) ? fields.join(',') : fields,
			type: 'auto',
			condition: 'contains',
			value: this.keyword
		});

		Ext.each(['sysCreatedBy', 'sysUpdatedBy', 'authorUsername', 'commentAuthorUsername'], function(field) {
			if (this[field])
				filters.push({
					field: (field != 'commentAuthorUsername' ? field : 'socialcomment.comment_Author_Us_ern_ame'),
					type: 'auto',
					condition: 'contains',
					value: this[field]
				});
		}, this);

		if (this.type && this.type.length > 0) {
			if (Ext.isArray(this.type) && this.type.length > 0)
				filters.push({
					field: 'type',
					type: 'auto',
					condition: 'contains',
					value: this.type.join(',')
				})
			else
				filters.push({
					field: 'type',
					type: 'auto',
					condition: 'contains',
					value: this.type
				})
		}
		if (this.namespace && this.namespace.length > 0)
			filters.push({
				field: 'namespace',
				type: 'auto',
				condition: 'equals',
				value: Ext.isArray(this.namespace) ? this.namespace.join(',') : this.namespace
			});
		if (this.updatedOnTimeObj)
			filters.push({
				field: 'sysUpdatedOn',
				type: 'date',
				condition: this.updatedOnCondition,
				value: Ext.Date.format(this.updatedOnTimeObj, 'c')
			});
		if (this.createdOnTimeObj)
			filters.push({
				field: 'sysCreatedOn',
				type: 'date',
				condition: this.createdOnCondition,
				value: Ext.Date.format(this.createdOnTimeObj, 'c')
			});
		if (this.tag && this.tag.length > 0)
			filters.push({
				field: 'tag',
				type: 'auto',
				condition: 'equals',
				value: Ext.isArray(this.tag) ? this.tag.join(',') : this.tag
			});
		if (this.targetId && this.targetId.length > 0) {
			var tids = this.targetId;
			var displayFields = [];
			if (!Ext.isArray(tids)) {
				tids = tids.split();
			}
			Ext.each(tids, function(id) {
				if (id)
					displayFields.push(this.targetIds.getAt(this.targetIds.findExact('id', id)).get('displayName'));
			}, this);
			if (displayFields.length > 0)
				filters.push({
					field: 'targetId',
					type: 'auto',
					condition: 'contains',
					value: Ext.isArray(this.targetId) ? this.targetId.join(',') : this.targetId,
					displayFields: displayFields
				});
		}
		return filters;
	},

	search: function() {
		this.parentVM.set('documentKeyword', this.keyword);
		this.parentVM.set('socialKeyword', this.keyword);
		this.parentVM.set('automationKeyword', this.keyword);
		this.parentVM.searchNow({}, this.collectFilters());
		Ext.state.Manager.set('lastSearch' + this.activeItem, this.asObject());
		this.cancel();
	},
	resetInputs: function() {
		this.loadData(this.defaultData);
		Ext.state.Manager.set('lastSearch' + this.activeItem, this.asObject());
		this.namespaceStore.load();
	},
	cancel: function() {
		this.doClose();
	}
});
glu.defModel('RS.search.AutomationRenderer', {
	mixins: ['Renderer'],
	init: function() {
		this.set('fullNameLink', {
			tag: 'a',
			href:(
				this.type=='actiontask'?
				Ext.String.format('#RS.actiontask.ActionTask/id={0}',this.id):
				Ext.String.format('#RS.wiki.Main/name={0}',this.fullName)
			),
			target: '_blank',
			cls: "rs-link",
			html: this.fullName
		})
	},

	numberOfReviewsIsVisible$: function() {
		return this.type != 'actiontask';
	},

	jumpToEdit: function() {
		clientVM.handleNavigation({
			modelName: (this.type == 'actiontask' ? 'RS.actiontask.ActionTask' : 'RS.wiki.Main'),
			params: {
				id: this.id,
				name : this.fullName
			}
		})
	}
});
glu.defModel('RS.search.DocumentRenderer', {
	mixins: ['Renderer'],

	init: function() {
		if (this.type == 'playbook') {
			this.dest = 'RS.incident.PlaybookTemplateRead';
		}
		this.set('fullNameLink', {
			tag: 'a',
			href: Ext.String.format('#{0}/name={1}', this.dest, this.fullName),
			target: '_blank',
			cls: "rs-link",
			html: this.fullName
		});

	},

	jumpToEdit: function() {
		clientVM.handleNavigation({
			modelName: this.dest,
			params: {
				name: this.fullName
			}
		});
	},

	nothingFound$: function() {
		return !this.id;
	}
});
glu.defModel('RS.search.Home', {
	historyStore: {
		mtype: 'store',
		fields: ['query', 'type'].concat(RS.common.grid.getSysFields()),
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		remoteSort: true,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/getUserQuery',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			},
			extraParams: {
				limit: 20
			}
		}
	},

	topQueryStore: {
		mtype: 'store',
		fields: ['query', 'selectCount', 'type'],
		remoteSort: true,
		sorters: [{
			property: 'selectCount',
			direction: 'DESC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/getTopQueries',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			},
			extraParams: {
				limit: 20
			}
		}
	},

	historyColumns: null,

	topQueryColumns: [{
		header: '~~query~~',
		dataIndex: 'query',
		flex: 1
	}, {
		header: '~~selectCount~~',
		dataIndex: 'selectCount'
	}],

	activate: function() {
		// clientVM.setWindowTitle(this.localize('windowTitle'));
		this.historyStore.load();
		this.topQueryStore.load();
	},
	init: function() {
		this.topQueryStore.on('beforeload', function(store, op) {
			var params = op.params || {};
			Ext.applyIf({
				start: 1,
				limit: 5
			});
			op.params = params;
		}, this);
		me = this;
		this.set('historyColumns', [{
			header: '~~query~~',
			dataIndex: 'query'
		}].concat((function(cols) {
			for (var idx = 0; idx < cols.length; idx++) {
				if (cols[idx].dataIndex == 'sysUpdatedOn') {
					cols[idx].hidden = false;
					cols[idx].flex = 1;
					cols[idx].text = me.localize('lastSearchedOn')
					return [cols[idx]];
				}
			}
		})(RS.common.grid.getSysColumns())));

		this.activate();

	},
	toggleUpHomeButton: function() {
		this.parentVM.set('homeWindow', null);
	},
	search: function(record) {
		switch (this.parentVM.activeItem) {
			case 0:
				this.parentVM.set('documentKeyword', record.get('query'))
				break;
			case 1:
				this.parentVM.set('socialKeyword', record.get('query'))
				break;
			default:
				this.parentVM.set('automationKeyword', record.get('query'))
		}
		this.parentVM.searchNow();
		this.doClose();
	}
})
glu.defModel('RS.search.Main', {
	mixins: ['pageable'],
	activeItem: 0,
	homeText: '',
	docExpanded: false,
	autoExpanded: false,
	displaySocial: true,
	displayAutomation: true,
	reqCount: 0,

	hideSocial: function() {
		this.set('displaySocial', false);
	},

	hideAutomation: function() {
		this.set('displayAutomation', false);
	},

	expanded$: function() {
		var isDocExpanded = this.activeItem === 0 && this.docExpanded;
		var isAutoExpanded = this.activeItem === 2 && this.autoExpanded;
		return isDocExpanded || isAutoExpanded;
	},

	toggleDetail: function(expand) {
		this[expand ? 'expand' : 'collapse']();
	},

	expand: function() {
		this.set(this.activeItem === 0 ? 'docExpanded' : 'autoExpanded', true);
	},

	collapse: function() {
		this.set(this.activeItem === 0 ? 'docExpanded' : 'autoExpanded', false);
	},

	showPreview$: function() {
		return this.activeItem === 0;
	},

	previewCollapsed: true,
	displayPostProfilePicutres: true,
	displayCommentProfilePictures: true,
	wait: false,

	documentTabIsPressed$: function() {
		return this.activeItem === 0;
	},

	socialTabIsPressed$: function() {
		return this.activeItem === 1;
	},

	automationTabIsPressed$: function() {
		return this.activeItem === 2;
	},

	documentKeywordIsEnabled$: function() {
		return this.triggerSearchIsEnabled;
	},

	socialKeywordIsEnabled$: function() {
		return this.triggerSearchIsEnabled;
	},

	automationKeywordIsEnabled$: function() {
		return this.triggerSearchIsEnabled;
	},

	triggerSearch: function() {
		if (this.triggerSearchIsEnabled) {
			this.searchNow();
		}
	},

	triggerSearchIsEnabled$: function() {
		return this.reqCount === 0;
	},

	documentTab: function() {
		this.set('activeItem', 0);
		this.resetPageInfo();
		this.documentStore.load();
	},

	socialTab: function() {
		this.set('activeItem', 1);
		this.resetPageInfo();
		this.postStore.load();
	},

	socialTabIsVisible$: function() {
		return this.displaySocial;
	},

	automationTab: function() {
		this.set('activeItem', 2);
		this.resetPageInfo();
		this.automationStore.load();
	},

	automationTabIsVisible$: function() {
		return hasPermission(['resolve_dev', 'resolve_process']) && this.displayAutomation;
	},

	homeWindow: null,

	gotoSearchHomeIsPressed$: function() {
		return this.homeWindow;
	},

	documentStore: {
		mtype: 'store',
		fields: [
			'name',
			'fullName',
			'namespace',
			'title',
			'summary',
			'content',
			'processModel',
			'abortModel',
			'finalModel',
			'decisionTree',

			'locked',
			'deleted',
			'hidden',
			'active',
			'type'
		].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/searchWikis',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	documentsColumns: [{
		text: '~~name~~',
		dataIndex: 'name'
	}, {
		text: '~~fullName~~',
		dataIndex: 'fullName',
		flex: 1
	}, {
		text: '~~namespace~~',
		dataIndex: 'namespace'
	}, {
		text: '~~title~~',
		dataIndex: 'title'
	}, {
		text: '~~summary~~',
		dataIndex: 'summary'
	}, {
		text: '~~content~~',
		dataIndex: 'content'
	}, {
		text: '~~processModel~~',
		dataIndex: 'processModel'
	}, {
		text: '~~abortModel~~',
		dataIndex: 'abortModel'
	}, {
		text: '~~finalModel~~',
		dataIndex: 'finalModel'
	}, {
		text: '~~decisionTree~~',
		dataIndex: 'decisionTree'
	}, {
		text: '~~locked~~',
		dataIndex: 'locked',
		renderer: RS.common.grid.booleanRenderer(),
		width: 50
	}, {
		text: '~~deleted~~',
		dataIndex: 'deleted',
		renderer: RS.common.grid.booleanRenderer(),
		width: 50
	}, {
		text: '~~hidden~~',
		dataIndex: 'hidden',
		renderer: RS.common.grid.booleanRenderer(),
		width: 50
	}, {
		text: '~~active~~',
		dataIndex: 'active',
		renderer: RS.common.grid.booleanRenderer(),
		width: 50
	}],

	documentKeyword: '',

	documentKeywordIsVisible$: function() {
		return this.activeItem === 0;
	},

	documentSuggestions: {
		mtype: 'store',
		fields: ['suggestion'],
		pageSize: 5,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/getDocumentSuggestions',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	documents: {
		mtype: 'list'
	},

	socialKeyword: '',

	socialKeywordIsVisible$: function() {
		return this.activeItem === 1;
	},

	socialSuggestions: {
		mtype: 'store',
		fields: ['suggestion'],
		pageSize: 5,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/getSocialSuggestions',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	automationKeyword: '',

	automationKeywordIsVisible$: function() {
		return this.activeItem === 2;
	},

	automationSuggestions: {
		mtype: 'store',
		fields: ['suggestion'],
		pageSize: 5,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/getAutomationSuggestions',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('windowTitle'));

		if (params) {
			if (params.search) {
				this.set('documentKeyword', params.search);
				this.set('socialKeyword', params.search);
				this.set('automationKeyword', params.search);
			}

			if (params.activeItem) {
				try {
					this.set('activeItem', parseInt(params.activeItem));
				} catch (e) {
					this.set('activeItem', 0);
				}
			}

			this.searchNow(params);
		}
	},

	clearFilter: function() {
		this.set('documentKeyword', '');
		this.set('socialKeyword', '');
		this.set('automationKeyword', '');
		this.searchNow({}, []);
	},

	refresh: function() {
		this.searchNow({}, this.filterItems);
	},

	searchNow: function(params, filter) {
		var keyword = '';

		switch (this.activeItem) {
			case 0:
				keyword = this.documentKeyword;
				this.generateQuery(Ext.apply({
					searchTerm: keyword
				}, params), filter);
				this.documentStore.load();
				break;
			case 1:
				keyword = this.socialKeyword;
				this.generateQuery(Ext.apply({
					searchTerm: keyword
				}, params), filter);
				this.postStore.load();
				break;
			case 2:
				keyword = this.automationKeyword;
				this.generateQuery(Ext.apply({
					searchTerm: keyword
				}, params), filter);
				this.automationStore.load();
				break;
		}

		this.set('documentKeyword', keyword);
		this.set('socialKeyword', keyword);
		this.set('automationKeyword', keyword);
	},

	generateQuery: function(params, extraFilters) {
		var filters = [];
		filters.push({
			field: '',
			type: 'auto',
			condition: 'contains',
			value: params.searchTerm
		});

		if (params && params.tag) {
			filters.push({
				field: 'tag',
				type: 'auto',
				condition: 'equals',
				value: Ext.isArray
				(params.tag) ? params.tag.join(',') : params.tag
			});
		}

		if (extraFilters) {
			filters = extraFilters;
		}

		this.set('filterItems', filters);
		this.set('query', { filter: Ext.encode(filters) });
	},

	query: null,
	filterItems: [],

	advanceSearch: function(target) {
		var k = this.automationKeyword;

		if(this.activeItem === 0) {
			k = this.documentKeyword;
		} else if(this.activeItem === 1) {
			k = this.socialKeyword;
		}

		this.open({
			mtype: 'RS.search.AdvanceSearchWindow',
			activeItem: this.activeItem,
			target: target.getEl().id,
			keyword: k
		});
	},

	init: function() {
		if (this.mock) {
			this.backend = RS.search.createMockBackend(true);
		}

		this.set('homeText', this.localize('gotoSearchHome'));
		this.documentSuggestions.on('beforeload', function(store, operation) {
			this.set('reqCount', this.reqCount + 1);
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				searchTerm: this.documentKeyword
			});
			operation.params.query = {};
		}, this);

		this.socialSuggestions.on('beforeload', function(store, operation) {
			this.set('reqCount', this.reqCount + 1);
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				searchTerm: this.socialKeyword
			});
			operation.params.query = {};
		}, this);

		this.automationSuggestions.on('beforeload', function(store, operation) {
			this.set('reqCount', this.reqCount + 1);
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				searchTerm: this.automationKeyword
			});
			operation.params.query = {};
		}, this);

		this.automationStore.on('beforeload', function(store, operation) {
			this.set('reqCount', this.reqCount + 1);
			operation.params = operation.params || {};
			var nextPageReqParam = this.nextPageReqParam();
			nextPageReqParam.problemId = clientVM.problemId;
			Ext.apply(operation.params, (this.query ? this.query : {}), nextPageReqParam);
			this.set('selectedAutomation', null);
		}, this)

		this.documentStore.on('beforeload', function(store, operation) {
			this.set('reqCount', this.reqCount + 1);
			operation.params = operation.params || {};
			var nextPageReqParam = this.nextPageReqParam();
			nextPageReqParam.problemId = clientVM.problemId;
			Ext.apply(operation.params, (this.query ? this.query : {}), nextPageReqParam);
			this.set('selectedDocument', null);
			this.fireEvent('resetFocus');
		}, this);

		this.postStore.on('beforeload', function(store, operation) {
			this.set('reqCount', this.reqCount + 1);
			operation.params = operation.params || {};
			var nextPageReqParam = this.nextPageReqParam();
			nextPageReqParam.streamId = 'all';
			nextPageReqParam.unreadOnly = false;
			nextPageReqParam.problemId = clientVM.problemId;
			Ext.apply(operation.params, (this.query ? this.query : {}), nextPageReqParam);
		}, this)

		this.documentSuggestions.on('load', function() {
			this.set('reqCount', this.reqCount - 1);
			this.fireEvent('resetFocus');
		}, this);

		this.socialSuggestions.on('load', function() {
			this.set('reqCount', this.reqCount - 1);
			this.fireEvent('resetFocus');
		}, this);

		this.automationSuggestions.on('load', function() {
			this.set('reqCount', this.reqCount - 1);
			this.fireEvent('resetFocus');
		}, this);

		this.documentStore.on('load', function(store, records) {
			this.set('reqCount', this.reqCount - 1);
			this.populateList(records, this.documents, {
				mtype: 'RS.search.DocumentRenderer',
				dest: 'RS.wiki.Main',
				collapsed: !this.docExpanded
			});
			this.set('total', this.documentStore.getTotalCount());
			this.computePageInfo();
			this.fireEvent('resetFocus');
		}, this);

		this.postStore.on('load', function(store, records) {
			this.set('reqCount', this.reqCount - 1);
			this.populateList(records, this.posts, {
				mtype: 'RS.social.Post',
				dest: 'RS.actiontask.ActionTask',
				showMove$: function() {
					return false;
				},
				showShareLink$: function() {
					return false;
				}
			});
			this.set('total', this.postStore.getTotalCount());
			this.computePageInfo();
			this.fireEvent('resetFocus');
		}, this);

		this.automationStore.on('load', function(store, records) {
			this.set('reqCount', this.reqCount - 1);
			this.populateList(records, this.automations, {
				mtype: 'RS.search.AutomationRenderer',
				dest: 'RS.actiontask.ActionTask',
				collapsed: !this.autoExpanded
			});
			this.set('total', this.automationStore.getTotalCount());
			this.computePageInfo();
			this.fireEvent('resetFocus');
		}, this);
	},

	gotoSearchHome: function(button) {
		if (this.homeWindow) {
			this.homeWindow.doClose();
			this.set('homeWindow', null);
		} else {
			this.set('homeWindow', this.open({
				mtype: 'RS.search.Home',
				target: button.getEl().id
			}));
		}
	},

	populateList: function(records, list, config) {
		list.removeAll();

		if (records && records.length) {
			for(var i = 0; i < records.length; i++) {
				list.add(this.model(Ext.apply(Ext.apply({}, config), records[i].raw)));
			}
		} else {
			list.add(this.model({
				mtype: 'RS.search.NothingFoundRenderer'
			}));
		}
	},

	searchDocument: function() {
		this.documentStore.load();
	},

	updatePreview: function(record) {
		var fullName = record.get('fullName');
		url = '/resolve/service/wiki/view/' + fullName.replace(/[.]/g, '/');
		this.set('iframe', url);
	},

	iframe: '',

	postStore: {
		mtype: 'store',
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		fields: ['id', 'content', 'title', 'author', 'sysUpdatedOn'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/searchPosts',
			reader: {
				type: 'json',
				root: 'records',
				totalProperty: 'total'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	posts: {
		mtype: 'list'
	},

	when_pageNumber_changes_reload_posts: {
		on: ['pageNumberChanged'],
		action: function() {
			if (this.activeItem === 0)
				this.documentStore.load();
			else if (this.activeItem === 1)
				this.postStore.load();
			else
				this.automationStore.load();
		}
	},

	automationColumns: [{
		text: '~~name~~',
		dataIndex: 'name'
	}, {
		text: '~~fullName~~',
		dataIndex: 'fullName'
	}, {
		text: '~~namespace~~',
		dataIndex: 'namespace'
	}, {
		text: '~~deleted~~',
		dataIndex: 'deleted',
		renderer: RS.common.grid.booleanRenderer(),
		width: 60
	}, {
		text: '~~hidden~~',
		dataIndex: 'hidden',
		renderer: RS.common.grid.booleanRenderer(),
		width: 60
	}, {
		text: '~~active~~',
		dataIndex: 'active',
		renderer: RS.common.grid.booleanRenderer(),
		width: 60
	}, {
		text: '~~summary~~',
		dataIndex: 'summary',
		flex: 1
	}],

	automationStore: {
		mtype: 'store',
		fields: [
			'name',
			'fullName',
			'namespace',
			'summary',
			'deleted',
			'hidden',
			'active',
			'type'
		].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/searchAutomations',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	automations: {
		mtype: 'list'
	},

	documentsSelectionChange: function(renderer) {
		if (this.selectedDocument) {
			this.selectedDocument.removeCls('render-selected');
		}

		renderer.addCls('render-selected');
		this.set('selectedDocument', renderer);
	},

	selectedDocument: null,

	automationsSelectionChange: function(renderer) {
		if (this.selectedAutomation) {
			this.selectedAutomation.removeCls('render-selected');
		}

		renderer.addCls('render-selected');
		this.set('selectedAutomation', renderer);
	},

	selectedAutomation: null,

	cleanIframes: function() {
		//Clean up anything like leftover iframes and links
		var frame = document.getElementById('searchresult-iframe');
		if(frame && frame.contentWindow) {
			var event = frame.contentWindow.document.createEvent("Event");
			event.initEvent('beforedestroy', true, false);
			frame.contentWindow.document.dispatchEvent(event);		
		}
	},

	preview: function() {
		var url = null;
		this.cleanIframes();

		if (this.activeItem === 0) {
			if (this.selectedDocument) {
				var rec = this.documentStore.getById(this.selectedDocument.recordId);
				if (rec.get('type') == 'playbook') {
					url = Ext.String.format('/resolve/jsp/rsclient.jsp?displayClient=false&{0}#RS.incident.PlaybookTemplateSearchPreview/name={1}', clientVM.rsclientToken, rec.get('fullName'));
				} else {
					url = Ext.String.format('/resolve/service/wiki/view?wiki={0}&OWASP_CSRFTOKEN={1}', rec.get('fullName'), clientVM.getPageToken('service/wiki/view'));
				}
			}
		} else if (this.activeItem === 2) {
			if (this.selectedAutomation) {
				var rec = this.automationStore.getById(this.selectedAutomation.recordId);

				if (rec.get('type').toLowerCase() === 'actiontask') {
					url = Ext.String.format('/resolve/jsp/rsclient.jsp?displayClient=false&{0}#RS.actiontask.ActionTask/id={1}&preview=true', clientVM.rsclientToken, this.selectedAutomation.recordId);
				} else {
					url = Ext.String.format('/resolve/service/wiki/view?wiki={0}&OWASP_CSRFTOKEN={1}', rec.get('fullName'), clientVM.getPageToken('service/wiki/view'));
				}
			}
		}

		if (url !== null) {
			//console.log('setting iframe to ', url);
			this.set('iframe', url);
			this.setPreviewMode(0);
		}
	},

	setPreviewMode: function(counter) {
		if (counter < 20) {
			setTimeout(function(){
				var iframe = document.getElementById('searchresult-iframe');
				var iframeDocBody = iframe.contentDocument.body;
				if (iframeDocBody && iframeDocBody.innerHTML) {
					iframeDocBody.className += ' preview';
				} else {
					this.setPreviewMode(++counter);
				}
			}.bind(this), 200)
		}
	},

	localRendered$: function() {
		return this.activeItem !== 1;
	},

	isPreviewable$: function() {
		return this.activeItem === 0 || this.activeItem === 2;
	},

	test: function() {
		this.open({
			mtype: 'RS.search.Test'
		})
	},

	localizer$: function() {
		return {
			doLocalize: function(text) {
				return this.localize(text);
			}.bind(this)
		};
	},

	removeFilter: function(removedFilter) {
		this.set('filterItems', Ext.Array.filter(this.filterItems, function(item) {
			return item.field !== removedFilter;
		}));

		this.refresh();
	}
});

glu.defModel('RS.search.NothingFoundRenderer', {

});
glu.mreg('pageable', {
	pageNumber: 1,
	pageSize: 15,
	total: 0,
	pageInfo: '',
	defaultPageInfo: {
		pageNumber: 1,
		pageSize: 15,
		total: 0,
		pageInfo: ''
	},
	computePageInfo: function() {
		if (this.total > 0) {
			var from = (this.pageNumber - 1) * this.pageSize + 1;
			var to = this.pageNumber * this.pageSize > this.total ? this.total : this.pageNumber * this.pageSize;
			this.set('pageInfo', Ext.String.format('{0} - {1} of {2}', from, to, this.total));
		} else
			this.set('pageInfo', 'None');
	},
	resetPageInfo: function() {
		with(this.defaultPageInfo) {
			this.set('pageNumber', pageNumber);
			this.set('pageSize', pageSize);
			this.set('total', total);
			this.set('pageInfo', pageInfo);
		}
	},
	previousPage: function() {
		this.set('pageNumber', this.pageNumber - 1)
	},
	previousPageIsEnabled$: function() {
		return this.pageNumber > 1
	},
	nextPage: function() {
		this.set('pageNumber', this.pageNumber + 1)
	},
	nextPageIsEnabled$: function() {
		return this.pageNumber * this.pageSize < this.total;
	},
	nextPageReqParam: function() {
		return {
			page: this.pageNumber,
			start: (this.pageNumber - 1) * this.pageSize,
			limit: this.pageSize
		}
	},
	firstPage: function() {
		this.set('pageNumber', 1);
	},
	firstPageIsEnabled$: function() {
		return this.previousPageIsEnabled;
	},
	lastPage: function() {
		this.set('pageNumber', Math.ceil(this.total / this.pageSize));
	},
	lastPageIsEnabled$: function() {
		return this.nextPageIsEnabled;
	},
	makeStorePageable: function(store) {
		store.on('beforeload', function(store, op) {
			var params = op.params || {};
			params = Ext.applyIf(params, this.nextPageReqParam());
		});
	}
});
glu.defModel('RS.search.Renderer', {
	id: '',
	title: '',
	name: '',
	dest: '',
	type: '',
	fullName: '',
	fullNameLink: '',
	rawSummary: '',
	summary: '',
	tags: [],
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	rating: 0,
	numberOfReviews: 0,
	collapsed: false,
	updated$: function() {
		return this.updatedOn + ' ' + this.sysUpdatedBy;
	},
	doctypeMap: {
		document : 'Document',
		automation : 'Automation',
		decisiontree : 'Decision Tree',
		actiontask : 'ActionTask',
		playbook : 'Playbook Template',
	},
	doctype$: function() {
		if (this.type == 'playbook' || this.type == 'actiontask') {
			return this.doctypeMap[this.type];
		} else {
			var doctypes = '';
			var types = this.type.split(', ');
			for (var i=0; i<types.length; i++) {
				var doctype = this.doctypeMap[types[i]];
				if (doctypes) {
					doctypes += ', ';
				}
				if (doctype) {
					doctypes += doctype;
				} else {
					doctypes += types[i];
				}
			}
			return doctypes;
		}
	},
	displaySummary$: function() {
		if (!this.summary)
			return;
		var doc = document.createElement('div');
		var result = this.summary;
		doc.innerHTML = result;
		if (doc.innerHTML == result && result.indexOf('<') != -1)
			return result
		var lines = result.split('\n');
		this.set('lineNum', lines.length);
		var sliced = lines.slice(0, lines.length > 3 ? 3 : lines.length).join('<br/>');
		var result = sliced;
		return result;
	},
	displayDetailedSummary$: function() {
		if (!this.summary)
			return;
		var doc = document.createElement('div');
		var result = this.summary;
		doc.innerHTML = this.summary;
		if (doc.innerHTML !== this.summary)
			result = Ext.String.htmlEncode(this.summary);
		result = Ext.util.Format.nl2br(result);
		return result;
	},
	tagsIsVisible$: function() {
		return this.tags.length > 0;
	},
	updatedOn$: function() {
		return Ext.Date.format(new Date(this.sysUpdatedOn), clientVM.getUserDefaultDateFormat());
	},
	socialReview: function() {
		clientVM.handleNavigation({
			modelName: 'RS.social.Main',
			params: {
				streamId: this.id,
				streamParent: this.fullName
			}
		});
	},
	detailed: false,
	lineNum: 1,
	showDetail: function() {
		this.set('detailed', !this.detailed)
	},
	canShowDetail$: function() {
		return this.rawSummary && this.lineNum > 3;
	},
	moreOrLess$: function() {
		return this.detailed ? this.localize('less') : this.localize('more');
	},
	allTags: false,
	moreOrLessTags$: function() {
		return this.allTags ? this.localize('less') : this.localize('showAllTags');
	},
	showTags$: function() {
		var len = this.tags.length < 6 || this.allTags ? this.tags.length : 6;
		return (this.tags.slice(0, len)).join(', ');
	},
	showTagsIsVisible$: function() {
		return !!this.showTags
	},
	showAllTagsIsVisible$: function() {
		return this.tags && this.tags.length > 6;
	},
	showAllTags: function() {
		this.set('allTags', !this.allTags);
	}
});
// glu.defModel('RS.search.Search', {

// 	mock: false,

// 	//define URL parameters for search
// 	keyword: '',

// 	state: '',
// 	stateId: 'searchdataList',
// 	displayName: 'Search Home',

// 	//define the selection field
// 	typecombo: '',
// 	typecomboS: '',
// 	typecomboA: '',

// 	incombo: '',
// 	timecombo: '',
// 	nscombo: '',
// 	tagcombo: '',
// 	center: '',
// 	east: '',
// 	eastHidden: true,
// 	didyoumeanHidden: true,
// 	didyoumean: '',
// 	search: '',
// 	searchtag: '',
// 	searchcombo: '',
// 	presearch: '',
// 	issuggested: 'false',
// 	preactivetype: '-1',
// 	issuggestedid: '',

// 	//hide params
// 	hideDType: false,
// 	hideSType: true,
// 	hideAType: true,
// 	hideADType: true,
// 	addingNSS: false,
// 	addingTags: false,
// 	hideNS: false,

// 	documentTabIsPressed: false,
// 	socialTabIsPressed: false,
// 	automationTabIsPressed: false,

// 	searchdataStore: {
// 		mtype: 'store',
// 		fields: ['u_component_sys_id', 'u_component_parent_name', 'u_component_updated_by', 'rating', 'views', 'u_component_type_display', 'u_component_description', 'expanderDesc', {
// 			name: 'u_component_updated_on',
// 			type: 'date',
// 			format: 'time',
// 			dateFormat: 'time'
// 		}],
// 		remoteSort: true,
// 		sorters: [{
// 			property: 'u_component_parent_name',
// 			direction: 'ASC'
// 		}],
// 		proxy: {
// 			type: 'ajax',
// 			url: '/resolve/service/search/listdata',
// 			reader: {
// 				type: 'json',
// 				root: 'records'
// 			}
// 		}
// 	},

// 	mysearchStore: {
// 		mtype: 'store',
// 		fields: ['name', {
// 			name: 'value',
// 			type: 'auto'
// 		}],
// 		remoteSort: true,
// 		proxy: {
// 			type: 'ajax',
// 			url: '/resolve/service/search/getAutoCompletion',
// 			reader: {
// 				type: 'json',
// 				root: 'records'
// 			}
// 		}
// 	},

// 	columns: null,

// 	searchSelections: [],

// 	activate: function(screen, params) {
// 		window.document.title = this.localize('windowTitle');

// 		if (this.preactivetype == "0")
// 			this.activeTab = 0;
// 		else if (this.preactivetype == "1")
// 			this.activeTab = 1;
// 		else if (this.preactivetype == "2")
// 			this.activeTab = 2;

// 		if (params != null && params.search != null) {
// 			this.search = params.search;
// 			this.searchtag = params.searchtag;
// 		}

// 		this.loadRecs();

// 		this.presearch = this.search;
// 		this.set('presearch', this.search);
// 		this.set('activeTab', Number(this.activeTab));
// 		this.issuggested = 'false';
// 		this.search = '';
// 		this.preactivetype = this.activeTab;
// 		this.didyoumeanHidden = true;
// 		this.searchcombo = '';

// 		this.set('issuggested', 'false');
// 		this.set('search', '');
// 		this.set('preactivetype', this.activeTab);
// 		this.set('didyoumeanHidden', true);
// 		this.set('searchcombo', this.presearch);

// 	},
// 	allSelected: false,
// 	gridSelections: [],
// 	activeTab: 0,
// 	reset: false,
// 	searchHomeTab: function() {

// 		clientVM.handleNavigation({
// 			modelName: 'RS.search.SearchHome/preactivetype=' + this.activeTab
// 		})

// 		this.closePreview();
// 	},

// 	documentTab: function() {

// 		this.set('activeTab', 0);
// 		this.set('hideNS', false);
// 		this.set('preactivetype', 0);
// 		this.closePreview();
// 	},
// 	documentTabIsPressed$: function() {
// 		return this.activeTab == 0
// 	},

// 	socialTab: function() {
// 		this.set('activeTab', 1);
// 		this.set('hideNS', true);
// 		this.set('preactivetype', 1);
// 		this.closePreview();
// 	},
// 	socialTabIsPressed$: function() {
// 		return this.activeTab == 1
// 	},
// 	automationTab: function() {

// 		this.set('activeTab', 2);
// 		this.set('hideNS', false);
// 		this.set('preactivetype', 2);
// 		this.closePreview();
// 	},
// 	automationTabIsPressed$: function() {

// 		return this.activeTab == 2
// 	},

// 	resetData: function() {

// 		this.set('typecombo', []);
// 		this.set('typecomboS', []);
// 		this.set('typecomboA', []);

// 		this.set('incombo', []);
// 		this.set('timecombo', []);
// 		this.set('nscombo', []);
// 		this.set('tagcombo', []);

// 		this.issuggested = 'false';
// 		this.search = '';
// 		this.preactivetype = '-1';
// 		this.didyoumeanHidden = true;
// 		this.searchcombo = '';

// 		this.set('issuggested', 'false');
// 		this.set('search', '');
// 		this.set('preactivetype', '-1');
// 		this.set('didyoumeanHidden', true);
// 		this.set('searchcombo', '');
// 	},


// 	init: function() {

// 		//Called when mocking the backend to test the UI
// 		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)

// 		if (Ext.isString(this.activeTab)) this.set('activeTab', Number(this.activeTab))

// 		if (this.searchtag != null && this.searchtag != '') {
// 			this.tagcombo = [];
// 			this.tagcombo.push(this.searchtag);
// 			this.set('tagcombo', this.tagcombo);
// 			this.activeTab = 1;
// 			this.set('activeTab', 1);

// 		}

// 		this.searchdataStore.on('beforeload', function(store, operation, eOpts) {
// 			operation.params = operation.params || {};
// 			this.set('state', 'wait');
// 			Ext.apply(operation.params, {
// 				searchtypes: this.typecombo,
// 				searchtypesS: this.typecomboS,
// 				searchtypesA: this.typecomboA,
// 				searchin: this.incombo,
// 				searchtime: this.timecombo,
// 				searchnamespace: this.nscombo,
// 				searchtag: this.tagcombo,
// 				searchkey: this.searchcombo,
// 				search: this.search,
// 				activetype: this.activeTab,
// 				suggested: this.issuggested,
// 				issuggestedid: this.issuggestedid,
// 			});
// 		}, this);

// 		this.searchdataStore.on('load', function(recs, op, suc) {
// 			this.set('state', 'ready');
// 			if (!suc) {
// 				clientVM.displayError(this.localize('SearchErr'));
// 				return;
// 			}
// 		}, this);

// 		this.mysearchStore.on('beforeload', function(store, operation, eOpts) {
// 			operation.params = operation.params || {};

// 			Ext.apply(operation.params, {
// 				searchkey: this.searchcombo,
// 				activetype: this.activeTab,
// 			});
// 		}, this)

// 		this.selectedTags.on('lengthchanged', function() {
// 			var t = [];
// 			this.selectedTags.foreach(function(tag) {
// 				t.push(tag.id)
// 			})
// 			this.set('tagcombo', t)
// 		}, this)

// 		this.selectedNS.on('lengthchanged', function() {
// 			var t = [];
// 			this.selectedNS.foreach(function(namespace) {
// 				t.push(namespace.id)
// 			})
// 			this.set('nscombo', t)
// 		}, this)

// 		this.set('columns', [
// 			// {
// 			// 	header: 'ID',
// 			// 	dataIndex: 'u_component_sys_id',
// 			// 	hidden: true,
// 			// 	autoWidth: true,
// 			// 	sortable: false
// 			// },
// 			{
// 				header: 'Name',
// 				dataIndex: 'u_component_parent_name',
// 				filterable: true,
// 				sortable: false,
// 				flex: 1,
// 				renderer: function(value, record) {

// 					var fullname = record.record.raw.u_component_parent_name_original,
// 						namespace = '',
// 						docName = '',
// 						url = '',
// 						docType = record.record.raw.u_component_type,
// 						sys_ID = record.record.raw.u_component_sys_id;

// 					if (Ext.Array.indexOf(['WikiDocument', 'Runbook', 'Attachment', 'Decisiontree'], docType) > -1) {
// 						if (fullname.indexOf('.') != -1) {
// 							var index = fullname.indexOf('.');
// 							namespace = fullname.substring(0, index);
// 							docName = fullname.substring(index + 1, fullname.length);
// 						}

// 						url = Ext.String.format('/resolve/jsp/rsclient.jsp?#RS.wiki.Main/name={0}', namespace + '.' + docName)
// 					} else if (docType != null && docType == 'ActionTask') {
// 						url = Ext.String.format('/resolve/jsp/rsclient.jsp?#RS.actiontask.Main/id=', sys_ID)
// 					} else if (docType != null && docType == 'POST') {
// 						url = Ext.String.format('/resolve/jsp/rsclient.jsp#RS.social.Main/postId={0}', sys_ID)
// 					}

// 					return Ext.String.format('<a class="search-link" id="{0}" href="{1}" target="_blank" onClick="updateViewCount(this.id);">{2}</a>', sys_ID, url, value)
// 				}
// 			}, {
// 				header: 'User',
// 				dataIndex: 'u_component_updated_by',
// 				filterable: true,
// 				sortable: true,
// 				flex: 1

// 			}, {
// 				header: 'Rating',
// 				dataIndex: 'rating',
// 				filterable: true,
// 				sortable: true,
// 				width: 80
// 			}, {
// 				header: 'Views',
// 				dataIndex: 'views',
// 				filterable: true,
// 				sortable: true,
// 				width: 80
// 			}, {
// 				header: 'Type',
// 				dataIndex: 'u_component_type_display',
// 				filterable: true,
// 				sortable: false,
// 				flex: 1
// 			}, {
// 				header: 'Updated',
// 				dataIndex: 'u_component_updated_on',
// 				filterable: true,
// 				sortable: true,
// 				flex: 1,
// 				renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
// 			}
// 		]);

// 	},

// 	when_activeTab_changes_populate_store: {
// 		on: ['activeTabChanged'],
// 		action: function() {

// 			switch (this.activeTab) {
// 				case 0:

// 					this.inStore.removeAll();
// 					this.inStore.add({
// 						"name": "Title",
// 						"value": "Title"
// 					});
// 					this.inStore.add({
// 						"name": "Contents",
// 						"value": "Contents"
// 					});
// 					this.inStore.add({
// 						"name": "User",
// 						"value": "User"
// 					});

// 					this.set('hideDType', false);
// 					this.set('hideSType', true);
// 					this.set('hideAType', true);
// 					this.set('hideADType', true);

// 					this.searchtag = '';
// 					this.set('searchtag', '');
// 					this.set('tagcombo', []);

// 					break;
// 				case 1:

// 					this.inStore.removeAll();
// 					this.inStore.add({
// 						"name": "Title",
// 						"value": "Subject"
// 					});
// 					this.inStore.add({
// 						"name": "Contents",
// 						"value": "Contents"
// 					});
// 					this.inStore.add({
// 						"name": "User",
// 						"value": "User"
// 					});

// 					this.set('hideDType', true);
// 					this.set('hideSType', false);
// 					this.set('hideAType', true);
// 					this.set('hideADType', true);

// 					break;
// 				case 2:

// 					this.inStore.removeAll();
// 					this.inStore.add({
// 						"name": "Title",
// 						"value": "Title"
// 					});
// 					this.inStore.add({
// 						"name": "Contents",
// 						"value": "Contents"
// 					});
// 					this.inStore.add({
// 						"name": "User",
// 						"value": "User"
// 					});

// 					this.set('hideDType', true);
// 					this.set('hideSType', true);
// 					this.set('hideADType', true);
// 					this.set('hideAType', false);

// 					this.searchtag = '';
// 					this.set('searchtag', '');
// 					this.set('tagcombo', []);

// 					break;
// 				case 3:

// 					this.inStore.removeAll();
// 					this.inStore.add({
// 						"name": "Title",
// 						"value": "Title"
// 					});
// 					this.inStore.add({
// 						"name": "Contents",
// 						"value": "Contents"
// 					});
// 					this.inStore.add({
// 						"name": "User",
// 						"value": "User"
// 					});


// 					this.set('hideDType', true);
// 					this.set('hideSType', true);
// 					this.set('hideAType', true);
// 					this.set('hideADType', false);

// 					break;
// 				default:
// 					break;

// 			}

// 			this.loadRecs();

// 		}
// 	},


// 	findSuggestions: function() {

// 		this.ajax({
// 			url: '/resolve/service/search/getSuggestions',
// 			params: {
// 				key: this.searchcombo,
// 				urlkey: this.search,
// 				activetype: this.activeTab,
// 				suggested: this.issuggested,
// 			},
// 			scope: this,
// 			success: function(r) {
// 				var response = RS.common.parsePayload(r)
// 				if (response.success) {

// 					if (response != null && response.records != null && response.records.length == 1) {
// 						var suggestionArray = response.records;
// 						var sug = suggestionArray[0];

// 						var didYouMean = sug.DIDYOUMEAN;
// 						var sugString = sug.SUGGESTION;
// 						var suggestiononly = sug.SUGGESTIONONLY;

// 						if (didYouMean != null) {
// 							if (didYouMean == 'true' && suggestiononly != '') // && suggestiononly != this.presearch)
// 							{
// 								this.didyoumeanHidden = false;
// 								this.didyoumean = sugString;

// 								this.set('didyoumeanHidden', false);
// 								this.set('didyoumean', sugString);

// 								this.search = '';
// 								this.set('search', '');
// 								this.issuggested = 'false';
// 								this.set('issuggested', 'false');

// 							} else {
// 								this.set('didyoumeanHidden', true);
// 							}
// 						}
// 					}
// 				} else {
// 					clientVM.displayError(response.message)
// 				}
// 			}
// 		});
// 	},

// 	loadRecs: function() {

// 		var testdata = this.searchcombo;
// 		this.searchdataStore.sorters.clear();

// 		this.set('state', 'wait');
// 		this.searchdataStore.load();

// 	},


// 	loadQuery: function() {

// 		if (this.searchcombo != null) {
// 			this.set('state', 'wait');
// 			this.mysearchStore.load({
// 				scope: this,
// 				callback: function(recs, op, suc) {

// 					if (!suc) {
// 						clientVM.displayError(this.localize('Search'));
// 						return;
// 					}

// 					this.set('state', 'ready');
// 				}
// 			});
// 		}
// 	},


// 	handleAjaxErr: function(err) {
// 		var title = null;
// 		var msg = null;
// 		if (err.status) {
// 			title = 'ServerErrTitle';
// 			msg = 'ServerErrMsg';
// 		} else {
// 			title = err.title;
// 			msg = err.msg;
// 		}

// 		this.message({
// 			title: this.localize(title),
// 			msg: this.localize(msg),
// 			button: Ext.MessageBox.YES,
// 			buttonText: {
// 				yes: this.localize('confirmErr')
// 			}
// 		});
// 	},


// 	selectAllAcrossPages: function(selectAll) {
// 		this.set('allSelected', selectAll);

// 	},
// 	selectionChanged: function() {

// 	},


// 	closePreview: function() {
// 		this.eastHidden = true;
// 		this.eastCollpased = true;
// 		this.set('eastHidden', true);
// 		this.set('eastCollpased', true);
// 	},


// 	eastCollpased: true,

// 	activeItem: 0,


// 	tag: function() {
// 		this.set('creatingWorksheet', true)
// 		this.ajax({
// 			url: '/resolve/service/worksheet/newWorksheet',
// 			method: 'POST',
// 			success: function(r) {
// 				var response = RS.common.parsePayload(r)
// 				if (response.success)
// 					clientVM.handleNavigation({
// 						modelName: 'RS.worksheet.Worksheet',
// 						params: {
// 							id: response.data
// 						}
// 					})
// 				else clientVM.displayError(response.message)
// 			},
// 			failure: function(r) {
// 				this.set('creatingWorksheet', false)
// 				clientVM.displayFailure(r)
// 			}
// 		})
// 	},


// 	typeStore: {
// 		mtype: 'store',
// 		fields: ['name', {
// 			name: 'value',
// 			type: 'auto'
// 		}],
// 		data: [{
// 			"name": "Document",
// 			"value": "Document"
// 		}, {
// 			"name": "DecisionTree",
// 			"value": "DecisionTree"
// 		}, {
// 			"name": "Attachment",
// 			"value": "Attachment"
// 		}, ],
// 		proxy: {
// 			type: 'memory',
// 			reader: {
// 				type: 'json'
// 			}
// 		}
// 	},

// 	typeStoreS: {
// 		mtype: 'store',
// 		fields: ['name', {
// 			name: 'value',
// 			type: 'auto'
// 		}],
// 		data: [{
// 			"name": "Post",
// 			"value": "POST"
// 		}, {
// 			"name": "Notification",
// 			"value": "NOTIFICATION"
// 		}, ],
// 		proxy: {
// 			type: 'memory',
// 			reader: {
// 				type: 'json'
// 			}
// 		}
// 	},

// 	typeStoreA: {
// 		mtype: 'store',
// 		fields: ['name', {
// 			name: 'value',
// 			type: 'auto'
// 		}],
// 		data: [{
// 			"name": "Runbook",
// 			"value": "Runbook"
// 		}, {
// 			"name": "ActionTask",
// 			"value": "ActionTask"
// 		}, ],
// 		proxy: {
// 			type: 'memory',
// 			reader: {
// 				type: 'json'
// 			}
// 		}
// 	},


// 	inStore: {
// 		mtype: 'store',
// 		fields: ['name', {
// 			name: 'value',
// 			type: 'auto'
// 		}],
// 		data: [{
// 			"name": "Title",
// 			"value": "Title"
// 		}, {
// 			"name": "Contents",
// 			"value": "Contents"
// 		}, {
// 			"name": "User",
// 			"value": "User"
// 		}, ],
// 		proxy: {
// 			type: 'memory',
// 			reader: {
// 				type: 'json'
// 			}
// 		}
// 	},

// 	timeStore: {
// 		mtype: 'store',
// 		fields: ['name', {
// 			name: 'value',
// 			type: 'auto'
// 		}],
// 		data: [{
// 			"name": "Today",
// 			"value": "Today"
// 		}, {
// 			"name": "Week",
// 			"value": "Week"
// 		}, {
// 			"name": "Month",
// 			"value": "Month"
// 		}, {
// 			"name": "Older",
// 			"value": "Older"
// 		}, ],
// 		proxy: {
// 			type: 'memory',
// 			reader: {
// 				type: 'json'
// 			}
// 		}
// 	},

// 	selectedTags: {
// 		mtype: 'list'
// 	},

// 	selectedNS: {
// 		mtype: 'list'
// 	},

// 	namespaceStore: {
// 		mtype: 'store',
// 		fields: ['name'],
// 		mixins: [{
// 			type: 'liststoreadapter',
// 			attachTo: 'selectedNS'
// 		}],
// 		data: [{
// 			"name": "Add Namespace",
// 			"id": "Add Namespace"
// 		}],
// 		proxy: {
// 			type: 'memory',
// 			reader: {
// 				type: 'json'
// 			}
// 		}
// 	},

// 	tagStore: {
// 		mtype: 'store',
// 		fields: ['name'],
// 		mixins: [{
// 			type: 'liststoreadapter',
// 			attachTo: 'selectedTags'
// 		}],
// 		data: [{
// 			"name": "Add Tag",
// 			"id": "Add Tag"
// 		}],
// 		proxy: {
// 			type: 'memory',
// 			reader: {
// 				type: 'json'
// 			}
// 		}
// 	},



// 	searchkeys: {
// 		mtype: 'list'
// 	},


// 	searchStore: {
// 		mtype: 'store',
// 		fields: ['name', 'value'],
// 		mixins: [{
// 			type: 'liststoreadapter',
// 			attachTo: 'searchkeys'
// 		}],
// 		proxy: {
// 			type: 'memory',
// 			reader: {
// 				type: 'json'
// 			}
// 		}
// 	},


// 	singleRecordSelected$: function() {
// 		return this.gridSelections.length == 1
// 	},


// 	when_typecombo_changes_update_name: {
// 		on: ['typecomboChanged'],
// 		action: function() {
// 			if (this.typecombo.length == 0) {
// 				this.typecombo = '';
// 				this.set('typecombo', []);
// 			}
// 			this.loadRecs();
// 		}
// 	},

// 	when_typecomboA_changes_update_name: {
// 		on: ['typecomboAChanged'],
// 		action: function() {
// 			if (this.typecomboA.length == 0) {
// 				this.typecomboA = '';
// 				this.set('typecomboA', []);
// 			}
// 			this.loadRecs();
// 		}
// 	},


// 	when_typecomboS_changes_update_name: {
// 		on: ['typecomboSChanged'],
// 		action: function() {

// 			if (this.typecomboS.length == 0) {
// 				this.typecomboS = '';
// 				this.set('typecomboS', []);
// 			}

// 			this.loadRecs();
// 		}
// 	},

// 	when_incombo_changes_update_name: {
// 		on: ['incomboChanged'],
// 		action: function() {

// 			if (this.incombo.length == 0) {
// 				this.incombo = '';
// 				this.set('incombo', []);
// 			}

// 			this.loadRecs();
// 		}
// 	},


// 	when_timecombo_changes_update_name: {
// 		on: ['timecomboChanged'],
// 		action: function() {

// 			if (this.timecombo.length == 0) {
// 				this.timecombo = '';
// 				this.set('timecombo', []);
// 			}

// 			this.loadRecs();
// 		}
// 	},

// 	when_nscombo_changes_update_name: {
// 		on: ['nscomboChanged'],
// 		action: function() {

// 			var selectedNS = this.nscombo;

// 			if (this.nscombo.length == 0) {
// 				this.nscombo = '';
// 				this.set('nscombo', []);
// 				this.loadRecs();
// 			} else {
// 				if (selectedNS != '' && (Ext.Array.indexOf(selectedNS, 'Add Namespace') != -1)) {
// 					for (var i = selectedNS.length - 1; i >= 0; i--) {
// 						if (selectedNS[i] === 'Add Namespace') {
// 							selectedNS.splice(i, 1);
// 						}
// 					}

// 					this.nscombo = this.selectedNS;
// 					this.set('nscombo', this.selectedNS);

// 					this.addingNSS = true;
// 					this.addMoreNamespaces();

// 				} else if (!this.addingNSS) {
// 					this.loadRecs();
// 				}

// 			}
// 		}
// 	},

// 	addMoreNamespaces: function() {

// 		this.open({
// 			mtype: 'RS.search.SearchNameSpacePicker',
// 			callback: 'addNamespace'
// 		});

// 	},

// 	addNamespace: function(newNamespaces) {
// 		var nsName = '',
// 			nsId = ''
// 		Ext.Array.forEach(newNamespaces, function(namespace) {
// 			nsName = namespace.data.name
// 			nsId = namespace.data.id || namespace.data.name

// 			var existed = false;
// 			this.selectedNS.foreach(function(namespace) {

// 				if (namespace.id == nsId) {
// 					existed = true;
// 					return false;
// 				}
// 			})

// 			if (!existed) {
// 				this.selectedNS.add({
// 					name: nsName,
// 					id: nsId
// 				})
// 			}
// 		}, this)

// 		this.addingNSS = false;
// 		this.set('nscombo', []);
// 	},


// 	when_tagcombo_changes_update_name: {
// 		on: ['tagcomboChanged'],
// 		action: function() {

// 			var selectedtag = this.tagcombo;

// 			if (this.tagcombo.length == 0) {
// 				this.tagcombo = '';
// 				this.set('tagcombo', []);
// 				this.loadRecs();
// 			} else {
// 				if (selectedtag != '' && (Ext.Array.indexOf(selectedtag, 'Add Tag') != -1)) {
// 					this.addingTags = true;
// 					this.addMoreTages();
// 				} else if (!this.addingTags) {
// 					this.loadRecs();
// 				}
// 			}
// 		}
// 	},


// 	addMoreTages: function() {

// 		this.open({
// 			mtype: 'RS.search.SearchTagPicker',
// 			callback: 'addTags'
// 		});

// 	},

// 	addTags: function(newTags) {

// 		var testd = this.search;

// 		var tagName = '',
// 			tagId = ''
// 		Ext.Array.forEach(newTags, function(tag) {
// 			tagName = tag.data.name
// 			tagId = tag.data.id || tag.data.name

// 			var existed = false;
// 			this.selectedTags.foreach(function(tag) {

// 				if (tag.id == tagId) {
// 					existed = true;
// 					return false;
// 				}
// 			})

// 			if (!existed) {
// 				this.selectedTags.add({
// 					name: tagName,
// 					id: tagId
// 				})
// 			}
// 		}, this)

// 		this.addingTags = false;
// 		this.set('tagcombo', []);
// 	},

// 	when_searchcombo_changes_update_name: {
// 		on: ['searchcomboChanged'],
// 		action: function() {

// 			this.didyoumeanHidden = false;
// 			this.set('didyoumeanHidden', false);

// 			this.loadRecs();
// 			this.loadQuery();
// 			this.findSuggestions();
// 		}
// 	},


// 	searchButtonTriggered: function() {

// 		this.didyoumeanHidden = false;
// 		this.set('didyoumeanHidden', false);

// 		if (this.searchcombo == null || this.searchcombo == '') {
// 			this.searchHomeTab();
// 		} else {
// 			this.loadRecs();
// 			this.findSuggestions();
// 		}
// 	},

// 	reallyDeleteRecords: function(button) {
// 		if (button === 'yes') {
// 			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
// 			if (this.allSelected) {
// 				this.ajax({
// 					url: '/resolve/service/worksheet/delete',
// 					params: {
// 						ids: '',
// 						whereClause: this.worksheets.lastWhereClause || '1=1'
// 					},
// 					scope: this,
// 					success: function(r) {
// 						var response = RS.common.parsePayload(r)
// 						if (response.success) this.worksheets.load()
// 						else clientVM.displayError(response.message)
// 					}
// 				})
// 			} else {
// 				var ids = []
// 				Ext.each(this.gridSelections, function(selection) {
// 					ids.push(selection.get('id'))
// 				})
// 				this.ajax({
// 					url: '/resolve/service/worksheet/delete',
// 					params: {
// 						ids: ids.join(',')
// 					},
// 					scope: this,
// 					success: function(r) {
// 						var response = RS.common.parsePayload(r)
// 						if (response.success) this.worksheets.load()
// 						else clientVM.displayError(response.message)
// 					}
// 				})
// 			}
// 		}
// 	},

// 	setActiveIsEnabled$: function() {
// 		return true; //this.singleRecordSelected
// 	}
// })
glu.defModel('RS.search.SearchHome', {
	API : {
		newWS : '/resolve/service/worksheet/newWorksheet',
		deleteWS : '/resolve/service/worksheet/delete',
		setActive : '/resolve/service/worksheet/setActive'
	},
	mock: false,
	state: '',
	stateId: 'searchdataList',
	searchcombo: '',
	displayName: 'Search Home',
	preactivetype: '0',

	searchdata: {
		mtype: 'store',
		fields: ['query', 'queryCount', 'u_component_type'],
		remoteSort: false,
		sorters: [{
			property: 'queryCount',
			direction: 'DESC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/getTopViewedResults',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	searchdataofme: {
		mtype: 'store',
		fields: ['query', {
			name: 'u_component_updated_on',
			type: 'date', //'types.VELATLONG',
			format: 'time',
			dateFormat: 'time'
		}],
		remoteSort: false,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/getQueryofme',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	searchdataofall: {
		mtype: 'store',
		fields: ['query', 'queryCount'],
		remoteSort: false,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/getQueryofall',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	columns: null,
	columnsofme: null,
	columnsofall: null,

	searchSelections: [],
	activate: function(screen, params) {
		window.document.title = this.localize('windowTitle');
		this.loadRecs();

		this.set('activeTab', Number(0)) //Number(params.activeTab))
	},


	myRecentSearch: 'My Recent Search',


	allSelected: false,
	gridSelections: [],

	activeTab: 0,

	searchTab: function() {

		this.loadSearchPage();
		//this.searchcombo = '';
		//this.set('searchcombo', '');
	},
	searchTabIsPressed$: function() {

		return; //this.activeTab == 0
	},

	loadSearchPage: function() {

		clientVM.handleNavigation({
			modelName: 'RS.search.Search/search=' + this.searchcombo + '&preactivetype=' + this.preactivetype
		})
	},

	searchButtonTriggered: function() {

		this.loadSearchPage();
		//this.searchcombo = '';
		//	this.set('searchcombo', '');
	},

	documentTab: function() {
		this.set('activeTab', 0)
	},
	documentTabIsPressed$: function() {
		return this.activeTab == 0
	},
	socialTab: function() {
		this.set('activeTab', 1)
	},
	socialTabIsPressed$: function() {
		return this.activeTab == 1
	},
	automationTab: function() {
		this.set('activeTab', 2)
	},
	automationTabIsPressed$: function() {
		return this.activeTab == 2
	},


	init: function() {

		//Called when mocking the backend to test the UI
		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)

		if (Ext.isString(this.activeTab)) this.set('activeTab', Number(this.activeTab))

		this.set('searchcombo', this.searchcombo)

		this.set('columns', [{
				header: 'Top Viewed Results',
				dataIndex: 'query',
				filterable: true,
				sortable: true,
				flex: 1,
				renderer: function(value, record) {

					var fullname = record.record.raw.query;
					var namespace = '';
					var docName = '';
					var url = '';
					var docType = record.record.raw.u_component_type;
					var sys_ID = record.record.raw.u_component_sys_id;

					if (docType != null && docType == 'WikiDocument' || docType == 'Runbook' || docType == 'Attachment' || docType == 'Decisiontree') {
						if (fullname.indexOf(".") != -1) {
							var index = fullname.indexOf(".");
							namespace = fullname.substring(0, index);
							docName = fullname.substring(index + 1, fullname.length);
						}

						url += "/resolve/jsp/rsclient.jsp?#RS.client.URLScreen/location=" + "%2Fresolve%2Fjsp%2Frswiki.jsp%3Fwiki%3D" + namespace + "." + docName;
					} else if (docType != null && docType == 'ActionTask') {
						//var sys_ID = record.record.raw.u_component_sys_id;
						url += "/resolve/jsp/rsclient.jsp?#RS.client.URLScreen/location=" + "%2Fresolve%2Fjsp%2Frsactiontask.jsp%3Fmain%3Dactiontask%26type%3Dform%26sys_id%3D" + sys_ID;

					} else if (docType != null && docType == 'POST') {
						//var sys_ID = record.record.raw.u_component_sys_id;
						url += '/resolve/jsp/rsclient.jsp#RS.social.Main/postId=/' + sys_ID;
					}

					return '<a style="text-decoration: none !important;" href="' + url + '" target="_blank"><font color=#385F95>' + value + '</font></a>';
				}
			}, {
				header: 'Search View Count',
				dataIndex: 'queryCount',
				filterable: true,
				sortable: true,
				width: 80
			}, {
				header: 'Type',
				dataIndex: 'u_component_type',
				filterable: true,
				sortable: true,
				flex: 1
			}

		]);

		this.set('columnsofme', [{
				header: 'Search Query',
				dataIndex: 'query',
				filterable: true,
				sortable: true,
				flex: 1,
				renderer: function(value, record) {
					return '<font color=#385F95>' + value + '</font>';
				}
			}, {
				header: 'Searched On',
				dataIndex: 'u_component_updated_on',
				filterable: true,
				sortable: true,
				flex: 1
			}

		]);

		this.set('columnsofall', [{
				header: 'Search Query',
				dataIndex: 'query',
				filterable: true,
				sortable: true,
				flex: 1
			}, {
				header: 'Approx Count',
				dataIndex: 'queryCount',
				filterable: true,
				sortable: true,
				flex: 1
			}

		]);

	},

	when_activeTab_changes_populate_store: {
		on: ['activeTabChanged'],
		action: function() {

			switch (this.activeTab) {
				case 0:
					this.typeStore.removeAll();
					this.typeStore.add({
						"name": "Document",
						"value": "Document"
					});
					this.typeStore.add({
						"name": "DecisionTree",
						"value": "DecisionTree"
					});
					this.typeStore.add({
						"name": "Attachment",
						"value": "Attachment"
					});
					break;
				case 1:
					this.typeStore.removeAll();
					this.typeStore.add({
						"name": "Post",
						"value": "Post"
					});
					this.typeStore.add({
						"name": "Notification",
						"value": "Notification"
					});
					break;
				case 2:
					this.typeStore.removeAll();
					this.typeStore.add({
						"name": "Runbook",
						"value": "Runbook"
					});
					this.typeStore.add({
						"name": "Actiontask",
						"value": "Actiontask"
					});
					break;
				default:
					break;

			}

		}
	},

	opensearch: function(record, item, index, e, eOpts) {
		var keyLocal = record.raw.query;
		clientVM.handleNavigation({
			modelName: 'RS.search.Search/urlkeyword=' + keyLocal,
			target: '_self'
		})
		this.searchcombo = '';
		this.set('searchcombo', '');
	},

	loadRecs: function() {
		this.set('state', 'wait');

		this.searchdataofme.load({
			scope: this,
			callback: function(recs, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('Search'));
					return;
				}

				this.set('state', 'ready');
			}
		});

		this.searchdataofall.load({
			scope: this,
			callback: function(recs, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('Search'));
					return;
				}

				this.set('state', 'ready');
			}
		});

		this.searchdata.load({
			scope: this,
			callback: function(recs, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('Search'));
					return;
				}

				this.set('state', 'ready');
			}
		});
	},

	loadRecsofme: function() {
		this.set('state', 'wait');

		this.searchdataofme.load({
			scope: this,
			callback: function(recs, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('Search'));
					return;
				}

				this.set('state', 'ready');
			}
		});
	},

	loadRecsofall: function() {
		this.set('state', 'wait');

		this.searchdataofall.load({
			scope: this,
			callback: function(recs, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('Search'));
					return;
				}

				this.set('state', 'ready');
			}
		});
	},

	handleAjaxErr: function(err) {
		var title = null;
		var msg = null;
		if (err.status) {
			title = 'ServerErrTitle';
			msg = 'ServerErrMsg';
		} else {
			title = err.title;
			msg = err.msg;
		}

		this.message({
			title: this.localize(title),
			msg: this.localize(msg),
			button: Ext.MessageBox.YES,
			buttonText: {
				yes: this.localize('confirmErr')
			}
		});
	},

	when_searchcombo_changes_update_name: {
		on: ['searchcomboChanged'],
		action: function() {
			this.loadSearchPage();
			//this.searchcombo = '';
			//this.set('searchcombo', '');
		}
	},

	resetData: function() {
		this.typeStore.removeAll();

	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	namespace: function() {
		this.set('creatingWorksheet', true)
		this.ajax({
			url: this.API['newWS'],
			method: 'POST',
			scope: this,
			params : {
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success)
					clientVM.handleNavigation({
						modelName: 'RS.worksheet.Worksheet',
						params: {
							id: response.data
						}
					})
				else clientVM.displayError(response.message)
			},
			failure: function(r) {
				this.set('creatingWorksheet', false)
				clientVM.displayFailure(r);
			}
		})
	},

	tag: function() {
		this.set('creatingWorksheet', true)
		this.ajax({
			url: this.API['newWS'],
			method: 'POST',
			scope: this,
			params : {
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success)
					clientVM.handleNavigation({
						modelName: 'RS.worksheet.Worksheet',
						params: {
							id: response.data
						}
					})
				else clientVM.displayError(response.message)
			},
			failure: function(r) {
				this.set('creatingWorksheet', false)
				clientVM.displayFailure(r);
			}
		})
	},

	creatingWorksheet: false,
	searchType: function() {
		this.set('creatingWorksheet', true)

		this.ajax({
			url: this.API['newWS'],
			method: 'POST',
			scope: this,
			params : {
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			},	
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success)
					clientVM.handleNavigation({
						modelName: 'RS.worksheet.Worksheet',
						params: {
							id: response.data
						}
					})
				else clientVM.displayError(response.message)
			},
			failure: function(r) {
				this.set('creatingWorksheet', false)
				clientVM.displayFailure(r);
			}
		})
	},

	searchTypeIsEnabled$: function() {
		return !this.creatingWorksheet
	},


	typeStore: {
		mtype: 'store',
		fields: ['name', {
			name: 'value',
			type: 'auto'
		}],
		data: [{
			"name": "Document",
			"value": "Document"
		}, {
			"name": "DecisionTree",
			"value": "DecisionTree"
		}, {
			"name": "Attachment",
			"value": "Attachment"
		}, ],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	inStore: {
		mtype: 'store',
		fields: ['name', {
			name: 'value',
			type: 'auto'
		}],
		data: [{
			"name": "Title",
			"value": "Title"
		}, {
			"name": "Contents",
			"value": "Contents"
		}, {
			"name": "User",
			"value": "User"
		}, ],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	timeStore: {
		mtype: 'store',
		fields: ['name', {
			name: 'value',
			type: 'auto'
		}],
		data: [{
			"name": "Today",
			"value": "Today"
		}, {
			"name": "Week",
			"value": "Week"
		}, {
			"name": "Month",
			"value": "Month"
		}, {
			"name": "Older",
			"value": "Older"
		}, ],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	namespaceStore: {
		mtype: 'store',
		fields: ['name', {
			name: 'value',
			type: 'auto'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/listNamespaces',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	tagStore: {
		mtype: 'store',
		fields: ['name', {
			name: 'value',
			type: 'auto'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/listTags',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},


	createCatalog: function() {
		clientVM.handleNavigation({
			modelName: 'RS.catalog.Catalog'
		})
	},
	createTrainingCatalog: function() {
		clientVM.handleNavigation({
			modelName: 'RS.catalog.Catalog',
			params: {
				catalogType: 'training'
			}
		})
	},
	editWorksheet: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.Worksheet',
			params: {
				id: id,
				activeTab: 1
			}
		})
	},
	singleRecordSelected$: function() {
		return this.gridSelections.length == 1
	},
	editWorksheetIsEnabled$: function() {
		return this.singleRecordSelected
	},
	deleteWorksheetIsEnabled$: function() {
		return this.gridSelections.length > 0 || this.allSelected
	},
	searchTime: function() {
		//Delete the selected record(s), but first confirm the action
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.gridSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.gridSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			if (this.allSelected) {
				this.ajax({
					url: this.API['deleteWS'],
					params: {
						ids: '',
						whereClause: this.worksheets.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.worksheets.load()
						else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			} else {
				var ids = []
				Ext.each(this.gridSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: this.API['deleteWS'],
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.worksheets.load()
						else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}
	},

	setActiveIsEnabled$: function() {
		return this.singleRecordSelected
	},
	searchIn: function() {
		this.message({
			title: this.localize('setActiveTitle'),
			msg: this.localize('setActiveMessage', this.gridSelections[0].get('number')),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('setActiveAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallySetActive
		})
	},
	reallySetActive: function(button) {
		if (button == 'yes') {
			this.ajax({
				url: this.API['setActive'],
				params: {
					worksheetId: this.gridSelections[0].get('id'),
					'RESOLVE.ORG_NAME' : clientVM.orgName,
					'RESOLVE.ORG_ID' : clientVM.orgId
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) this.worksheets.load()
					else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	}
})
glu.defView('RS.search.AdvanceSearchWindow', {
	title: '~~advanceSearchTitle~~',
	bodyPadding: '10px',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaultType: 'textfield',
	defaults: {
		labelWidth: 120
	},
	items: ['keyword', {
			xtype: 'combo',
			name: 'type',
			store: '@{typeStore}',
			displayField: 'name',
			valueField: 'value',
			queryMode: 'local',
			multiSelect: true,
			editable: false
		}, {
			xtype: 'combo',
			name: 'namespace',
			store: '@{namespaceStore}',
			displayField: 'unamespace',
			valueField: 'unamespace',
			typeAhead: false,
			multiSelect: true,
			minChars: 0
		},

		// {
		// 	xtype: 'combo',
		// 	name: 'sysUpdatedBy',
		// 	store: '@{sysUpdatedByStore}',
		// 	displayField: 'udisplayName',
		// 	valueField: 'uuserName',
		// 	typeAhead: false,
		// 	multiSelect: true,
		// 	minChars: 0
		// }, {
		// 	xtype: 'combo',
		// 	name: 'sysCreatedBy',
		// 	store: '@{sysCreatedByStore}',
		// 	displayField: 'udisplayName',
		// 	valueField: 'uuserName',
		// 	typeAhead: false,
		// 	multiSelect: true,
		// 	minChars: 0
		// }, {
		// 	xtype: 'combo',
		// 	name: 'authorUsername',
		// 	store: '@{authorUsernameStore}',
		// 	displayField: 'udisplayName',
		// 	valueField: 'uuserName',
		// 	typeAhead: false,
		// 	multiSelect: true,
		// 	minChars: 0
		// }, {
		// 	xtype: 'combo',
		// 	name: 'commentAuthorUsername',
		// 	store: '@{commentAuthorUsernameStore}',
		// 	displayField: 'udisplayName',
		// 	valueField: 'uuserName',
		// 	typeAhead: false,
		// 	multiSelect: true,
		// 	minChars: 0
		// },
		'authorUsername', 'commentAuthorUsername', 'sysUpdatedBy', {
			xtype: 'fieldcontainer',
			layout: 'hbox',
			items: [{
				xtype: 'combo',
				width: 210,
				fieldLabel: '~~updatedOn~~',
				labelWidth: 120,
				editable: false,
				name: 'updatedOnCondition',
				queryMode: 'local',
				displayField: 'condition',
				valueField: 'condition',
				store: '@{timeConditionStore}'
			}, {
				xtype: 'datefield',
				padding: '0px 0px 0px 5px',
				hideLabel: true,
				flex: 3,
				name: 'updatedOn',
				format: 'm/d/Y',
				maxValue: new Date(),
				listeners: {
					change: function(datefield, oldDate, newDate) {
						datefield.fireEvent('updatedOnChagned', datefield, oldDate, newDate);
					},
					validitychange: '@{setUpdatedOnValid}'
				}
			}, {
				xtype: 'timefield',
				flex: 2,
				hideLabel: true,
				padding: '0px 0px 0px 5px',
				name: 'updatedOnTime',
				listeners: {
					change: function(datefield, oldDate, newDate) {
						datefield.fireEvent('updatedOnTimeChanged', datefield, oldDate, newDate);
					},
					validitychange: '@{setUpdatedOnTimeValid}'
				}
			}]
		}, 'sysCreatedBy', {
			xtype: 'fieldcontainer',
			layout: 'hbox',
			items: [{
				xtype: 'combo',
				width: 210,
				fieldLabel: '~~createdOn~~',
				labelWidth: 120,
				editable: false,
				name: 'createdOnCondition',
				queryMode: 'local',
				displayField: 'condition',
				valueField: 'condition',
				store: '@{timeConditionStore}'
			}, {
				xtype: 'datefield',
				padding: '0px 0px 0px 5px',
				hideLabel: true,
				flex: 3,
				name: 'createdOn',
				format: 'm/d/Y',
				maxValue: new Date(),
				listeners: {
					change: function(datefield, oldDate, newDate) {
						datefield.fireEvent('createdOnChanged', datefield, oldDate, newDate);
					},
					validitychange: '@{setCreatedOnValid}'
				}
			}, {
				xtype: 'timefield',
				flex: 2,
				padding: '0px 0px 0px 5px',
				name: 'createdOnTime',
				hideLabel: true,
				listeners: {
					change: function(datefield, oldDate, newDate) {
						datefield.fireEvent('createdOnTimeChanged', datefield, oldDate, newDate);
					},
					validitychange: '@{setCreatedOnTimeValid}'
				}
			}]
		}, {
			xtype: 'combo',
			name: 'field',
			store: '@{fieldStore}',
			queryMode: 'local',
			displayField: 'name',
			valueField: 'value',
			multiSelect: true,
			editable: false
		}, {
			xtype: 'combo',
			name: 'targetId',
			store: '@{targetIds}',
			displayField: 'displayName',
			valueField: 'id',
			multiSelect: true,
			queryMode: 'local'
		}, {
			xtype: 'combo',
			name: 'tag',
			store: '@{tagStore}',
			displayField: 'name',
			valueField: 'name',
			multiSelect: true,
			minChars: 0,
			pageSize: 10
		}
	],
	buttonAlign: 'left',
	modal: true,
	buttons: ['search', 'resetInputs', 'cancel'],

	baseCls: Ext.isIE8m ? 'x-window' : 'x-panel',
	ui: Ext.isIE8m ? 'default' : 'sysinfo-dialog',
	draggable: false,
	target: '@{target}',

	listeners: {
		render: function(panel) {
			panel.setWidth(Ext.isIE8m ? 550 : Ext.get(panel.target).getWidth());
			if (panel.target) {
				Ext.defer(function() {
					panel.alignTo(panel.target)
				}, 1)
			}
		}
	}
});
glu.defView('RS.search.AutomationRenderer', {
	xtype: 'panel',
	overCls: 'render-hover',
	width: '100%',
	minWidth: 500,
	cls: 'render-body',
	style: 'border-bottom: 1px solid #eee !important;',
	bodyPadding: '5px',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'fieldcontainer',
		layout: 'vbox',
		items: [{
			xtype: 'tbtext',
			cls: 'render-title',
			overCls: 'render-title-hover',
			text: '@{fullName}',
			listeners: {
				handler: '@{jumpToEdit}',
				render: function(image) {
					image.getEl().on('click', function() {
						image.fireEvent('handler')
					})
				}
			}
		}, {
			xtype: 'component',
			autoEl: '@{fullNameLink}',
			margins: {
				left: 5
			}
		}]
	}, {
		xtype: 'panel',
		margins: {
			left: 5
		},
		collapsible: true,
		collapsed: '@{collapsed}',
		animCollapse: false,
		name: 'detail',
		hideCollapseTool: true,
		header: false,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			hidden: '@{detailed}',
			html: '@{displaySummary}',
			style: 'font-family:Arial;overflow-wrap: break-word;'
		}, {
			hideLabel: true,
			hidden: '@{!detailed}',
			html: '@{displayDetailedSummary}',
			style: 'font-family:Arial;overflow-wrap: break-word;'
		}, {
			xtype: 'fieldcontainer',
			items: [{
				xtype: 'displayfield',
				overCls: 'render-title-hover',
				style: 'color:#999;cursor:pointer;width: 100!important',
				fieldStyle: 'color:#999;cursor:pointer;',
				value: '@{moreOrLess}',
				hidden: '@{!canShowDetail}',
				listeners: {
					handler: '@{showDetail}',
					render: function(field) {
						field.getEl().on('click', function() {
							field.fireEvent('handler')
						});
					}
				}
			}]
		}, {
			xtype: 'fieldcontainer',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'displayfield',
				name: 'showTags',
				fieldLabel: '~~tags~~',
				labelWidth: 90,
				fieldStyle: 'overflow-wrap: break-word;',
				cls: 'render-info'
			}, {
				xtype: 'tbtext',
				padding: '0 0 0 90px',
				overCls: 'render-title-hover',
				style: 'color:#999;cursor:pointer;width: 100!important',
				fieldStyle: 'color:#999;cursor:pointer;',
				text: '@{moreOrLessTags}',
				hidden: '@{!showAllTagsIsVisible}',
				listeners: {
					handler: '@{showAllTags}',
					render: function(field) {
						field.getEl().on('click', function() {
							field.fireEvent('handler')
						});
					}
				}
			}]
		}, {
			xtype: 'displayfield',
			labelWidth: 90,
			name: 'doctype',
			cls: 'render-info'
		}, {
			xtype: 'displayfield',
			name: 'updated',
			labelWidth: 90,
			cls: 'render-info'
		}, {
			xtype: 'fieldcontainer',
			layout: 'hbox',
			hidden: '@{!numberOfReviewsIsVisible}',
			items: [{
				xtype: 'component',
				rating: '@{rating}',
				cls: 'rating'
			}, {
				xtype: 'tbtext',
				padding: '0px 0px 0px 10px',
				overCls: 'render-title-hover',
				style: 'color:#999;cursor:pointer;',
				text: '(@{numberOfReviews} reviews)',
				name: 'numberOfReviews',
				listeners: {
					handler: '@{socialReview}',
					render: function(image) {
						image.getEl().on('click', function() {
							image.fireEvent('handler')
						});
					}
				}
			}]
		}]
	}],
	recordId: '@{id}',
	listeners: {
		render: function(c) {
			var me = this;
			c.body.on('click', function() {
				me.ownerCt.fireEvent('autoselchange', me, me.recordId);
			});
			var comp = this.down('component[cls="rating"]');
			var rating = comp.rating;
			var floor = Math.floor(rating);
			var icons = '';
			for (var i = 0; i < floor; i++)
				icons += '<i class="icon-star"></i>'

			var r = rating - floor;
			if (r > 0.25) {
				icons += '<i class="icon-star-half-empty"></i>'
				floor++;
			}

			for (var i = floor; i < 5; i++)
				icons += '<i class="icon-star-empty"></i>'
			Ext.get(comp.id).update(Ext.String.format('Ratings:{0}', icons));
		}
	}
});
glu.defView('RS.search.DocumentRenderer', {
	xtype: 'panel',
	overCls: 'render-hover',
	style: 'border-bottom: 1px solid #eee !important;',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	width: '100%',
	bodyPadding: '3px',
	minWidth: 500,
	items: [{
		xtype: 'panel',
		layout: {
			type: 'vbox',
			//align: 'stretch'
		},
		items: [{
			xtype: 'tbtext',
			cls: 'render-title',
			overCls: 'render-title-hover',
			text: '@{title}',
			htmlEncode : false,
			listeners: {
				handler: '@{jumpToEdit}',
				render: function(image) {
					image.getEl().on('click', function() {
						image.fireEvent('handler')
					});
				}
			}
		}, {
			xtype: 'component',
			autoEl: '@{fullNameLink}',
			margins: {
				left: 5
			}
		}, {
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			margins: {
				left: 5
			},
			hidden: '@{!collapsed}',
			items: [{
				hidden: '@{detailed}',
				html: '@{displaySummary}',
				style: 'font-family:Arial;overflow-wrap: break-word;'
			}, {
				hideLabel: true,
				hidden: '@{!detailed}',
				html: '@{displayDetailedSummary}',
				style: 'font-family:Arial;overflow-wrap: break-word;'
			}, {
				html: '<span style="color:#999;cursor:pointer;width: 100!important">@{moreOrLess}</span>',
				hidden: '@{!canShowDetail}',
				listeners: {
					handler: '@{showDetail}',
					render: function(field) {
						field.getEl().on('click', function() {
							field.fireEvent('handler')
						});
						var renderer = field.getEl().down('span[style^="color:#999"]');
						if (renderer)
							renderer.addClsOnOver('render-title-hover')
					}
				}
			}, {
				xtype: 'displayfield',
				name: 'updated',
				labelWidth: 90,
				cls: 'render-info'
			}]
		}]
	}, {
		xtype: 'panel',
		collapsible: true,
		collapsed: '@{collapsed}',
		animCollapse: false,
		name: 'detail',
		hideCollapseTool: true,
		header: false,
		margins: {
			left: 5
		},
		padding: '0 10px 0px 0px',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			hidden: '@{detailed}',
			html: '@{displaySummary}',
			style: 'font-family:Arial;overflow-wrap: break-word;'
		}, {
			hideLabel: true,
			hidden: '@{!detailed}',
			html: '@{displayDetailedSummary}',
			style: 'font-family:Arial;overflow-wrap: break-word;'
		}, {
			xtype: 'displayfield',
			overCls: 'render-title-hover',
			style: 'color:#999;cursor:pointer;width: 100!important',
			fieldStyle: 'color:#999;cursor:pointer;',
			value: '@{moreOrLess}',
			hidden: '@{!canShowDetail}',
			listeners: {
				handler: '@{showDetail}',
				render: function(field) {
					field.getEl().on('click', function() {
						field.fireEvent('handler')
					});
				}
			}
		}, {
			xtype: 'displayfield',
			name: 'showTags',
			fieldLabel: '~~tags~~',
			labelWidth: 90,
			fieldStyle: 'overflow-wrap: break-word;',
			cls: 'render-info'
		}, {
			xtype: 'tbtext',
			padding: '0 0 0 90px',
			overCls: 'render-title-hover',
			style: 'color:#999;cursor:pointer;width: 100!important',
			fieldStyle: 'color:#999;cursor:pointer;',
			text: '@{moreOrLessTags}',
			hidden: '@{!showAllTagsIsVisible}',
			listeners: {
				handler: '@{showAllTags}',
				render: function(field) {
					field.getEl().on('click', function() {
						field.fireEvent('handler')
					});
				}
			}
		}, {
			xtype: 'displayfield',
			labelWidth: 90,
			name: 'doctype',
			cls: 'render-info'
		}, {
			xtype: 'displayfield',
			name: 'updated',
			labelWidth: 90,
			cls: 'render-info'
		}, {
			xtype: 'fieldcontainer',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'component',
				rating: '@{rating}',
				cls: 'rating'
			}, {
				xtype: 'tbtext',
				padding: '0px 0px 0px 10px',
				overCls: 'render-title-hover',
				style: 'color:#999;cursor:pointer;',
				text: '(@{numberOfReviews} reviews)',
				listeners: {
					handler: '@{socialReview}',
					render: function(image) {
						image.getEl().on('click', function() {
							image.fireEvent('handler')
						});
					}
				}

			}]
		}]
	}],
	recordId: '@{id}',
	listeners: {
		render: function(c) {
			var me = this;
			c.body.on('click', function() {
				me.ownerCt.fireEvent('docrendererselectionchange', me, me.recordId);
			});
			var comp = this.down('component[cls="rating"]');
			var rating = comp.rating;
			var floor = Math.floor(rating);
			var icons = '';
			for (var i = 0; i < floor; i++)
				icons += '<i class="icon-star"></i>'

			var r = rating - floor;
			if (r > 0.25) {
				icons += '<i class="icon-star-half-empty" style="color:rgb(246, 171, 0)"></i>'
				floor++;
			}
			for (var i = floor; i < 5; i++)
				icons += '<i class="icon-star-empty"></i>'
			Ext.get(comp.id).update(Ext.String.format('<span style="color:#999 !important;padding-right:40px">Ratings:</span>{0}', icons));
		}
	}
});
glu.defView('RS.search.Home', {
	title: '~~searchHome~~',
	baseCls: Ext.isIE8m ? 'x-window' : 'x-panel',
	ui: Ext.isIE8m ? 'default' : 'sysinfo-dialog',
	draggable: false,
	target: '@{target}',
	listeners: {
		render: function(panel) {
			if (panel.target) {
				Ext.defer(function() {
					panel.alignTo(panel.target, 'tl-bl')
				}, 1)
			}
		},

		close: '@{toggleUpHomeButton}'
	},

	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	bodyPadding: '10px',
	defaultType: 'grid',
	defaults: {
		maximunHeight: 400,
		flex: 1,
		cls : 'rs-grid-dark'
	},
	items: [{
		title: '~~topHistory~~',
		name: 'history',
		store: '@{historyStore}',
		columns: '@{historyColumns}',
		margins: {
			right: 5
		},
		listeners: {
			search: '@{search}',
			itemclick: function(item, record) {
				this.fireEvent('search', item, record);
			}
		}
	}, {
		title: '~~topQuery~~',
		name: 'topQuery',
		store: '@{topQueryStore}',
		columns: '@{topQueryColumns}',
		margins: {
			left: 5
		},
		listeners: {
			search: '@{search}',
			itemclick: function(item, record) {
				this.fireEvent('search', item, record);
			}
		}
	}],
	width: 700
})
glu.defView('RS.search.Main', {
	layout: 'border',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			htmlEncode : false,
			text: '@{homeText}&nbsp&nbsp<i class="icon-caret-down" style="color:rgb(153, 153, 153)"></i>&nbsp',
			cls: 'rs-display-name',
			pressed: '@{gotoSearchHomeIsPressed}',
			handler: function(button) {
				button.fireEvent('gotoSearchHome', button, button)
			},
			listeners: {
				gotoSearchHome: '@{gotoSearchHome}'
			}
		}, {
			xtype: 'combobox',
			store: '@{documentSuggestions}',
			displayField: 'suggestion',
			valueField: 'suggestion',
			trigger2Cls: 'x-form-search-trigger',
			name: 'documentKeyword',
			hideLabel: true,
			emptyText: '~~emptySearch~~',
			width: 520,
			queryMode: 'remote',
			keyDelay:  0,
			minChars: 0,
			autoSelect: false,
			listConfig: {
				loadMask: false
			},
			listeners: {
				specialKey: function(field, e) {
					var key = e.getKey();

					if (key === e.ENTER) {
						field.fireEvent('search', field)
						this.fireEvent('resetFocus');
					} else if (key === e.END) {
						var value = field.value;
						field.value = '';
						field.focus();
						field.value = value;
					} else if (key === e.HOME) {
						field.focus();
					}
				},
				render: function (field) {
					this._vm.on('resetFocus', function () {
						field.focus();
					});
				},
				search: '@{triggerSearch}',
				advanceSearch: '@{advanceSearch}'
			},
			onTriggerClick: function() {
				this.fireEvent('advanceSearch', this, this);
			},
			onTrigger2Click: function() {
				this.fireEvent('search');
			}
		}, {
			xtype: 'combobox',
			store: '@{socialSuggestions}',
			displayField: 'suggestion',
			valueField: 'suggestion',
			trigger2Cls: 'x-form-search-trigger',
			name: 'socialKeyword',
			hideLabel: true,
			emptyText: '~~emptySearch~~',
			width: 520,
			queryMode: 'remote',
			minChars: 2,
			typeAheadDelay: 1,
			autoSelect: false,
			listConfig: {
				loadMask: false
			},
			listeners: {
				specialKey: function(field, e) {
					var key = e.getKey();

					if (key === e.ENTER) {
						field.fireEvent('search', field);
						this.fireEvent('resetFocus');
					} else if (key === e.END) {
						var value = field.value;
						field.value = '';
						field.focus();
						field.value = value;
					} else if (key === e.HOME) {
						field.focus();
					}
				},
				render: function (field) {
					this._vm.on('resetFocus', function () {
						field.focus();
					});
				},
				search: '@{searchNow}',
				advanceSearch: '@{advanceSearch}'
			},
			onTriggerClick: function() {
				this.fireEvent('advanceSearch', this, this);
			},
			onTrigger2Click: function() {
				this.fireEvent('search');
			}
		}, {
			xtype: 'combobox',
			store: '@{automationSuggestions}',
			displayField: 'suggestion',
			valueField: 'suggestion',
			trigger2Cls: 'x-form-search-trigger',
			name: 'automationKeyword',
			hideLabel: true,
			emptyText: '~~emptySearch~~',
			width: 520,
			queryMode: 'remote',
			minChars: 2,
			typeAheadDelay: 1,
			autoSelect: false,
			listConfig: {
				loadMask: false
			},
			listeners: {
				specialKey: function(field, e) {
					var key = e.getKey();

					if (key === e.ENTER) {
						field.fireEvent('search', field);
						this.fireEvent('resetFocus');
					} else if (key === e.END) {
						var value = field.value;
						field.value = '';
						field.focus();
						field.value = value;
					} else if (key === e.HOME) {
						field.focus();
					}
				},
				render: function (field) {
					this._vm.on('resetFocus', function () {
						field.focus();
					});
				},
				search: '@{searchNow}',
				advanceSearch: '@{advanceSearch}'
			},
			onTriggerClick: function() {
				this.fireEvent('advanceSearch', this, this);
			},
			onTrigger2Click: function() {
				this.fireEvent('search');
			}
		}, '->', 'documentTab', 'socialTab', 'automationTab']
	}, {
		xtype: 'container',
		items: [{
			xtype: 'filterbar',
			filterItems: '@{filterItems}',
			localizer: '@{localizer}',
			allowPersistFilter: false,
			setFilterItems: function(filterItems) {
				this.clearFilter(null, false);

				var map = {
					'sysCreatedBy': true,
					'sysCreatedOn': true,
					'sysUpdatedBy': true,
					'sysUpdatedOn': true,
					'type': true,
					'namespace': true,
					'targetId': true,
					'tag': true
				};

				Ext.each(filterItems, function(filter) {
					if (!filter.field) {
						return;
					}

					if (!map[filter.field]) {
						var fields = filter.field.split(',')
						var fieldText = [];
						Ext.each(fields, function(field) {
							fieldText.push(this.localizer.doLocalize(field));
						}, this);
						this.addFilter(Ext.String.format('In {0}', fieldText.join(',')), filter.field)
						return;
					}

					if (filter.field === 'tag') {
						this.addFilter('Tagged by ' + filter.value, filter.field);
						return;
					}

					if (filter.field === 'targetId') {
						this.addFilter('With Target ID:' + filter.displayFields)
						return;
					}

					if (!!filter.value) {
						this.addFilter(Ext.String.format('{0} {1} {2}', this.localizer.doLocalize(filter.field), filter.condition, filter.value), filter.field);
					}
				}, this)
			},

			listeners: {
				clearFilter: '@{clearFilter}',
				remove: function(filterBar, removedItem) {
					if (!this.clearingFilters) {
						this.fireEvent('removeFilter', this, removedItem.internalValue);
					}
				},
				removeFilter: '@{removeFilter}'
			}
		}]
	}, {
		xtype: 'toolbar',
		cls: 'actionBar-form',
		name: 'actionBar',
		items: [{
			text: '~~expand~~',
			hidden: '@{!localRendered}',
			itemId: 'expand',
			pressed: '@{expanded}',
			handler: function() {
				var panel = this.up().up().items.getAt(0);
				var active = panel.getLayout().getActiveItem();
				Ext.each(active.items.items, function(renderer) {
					renderer.down('panel[name="detail"]')[!this.pressed ? 'expand' : 'collapse']();
				}, this);
				this.fireEvent('toggleDetail', this, !this.pressed);
			},
			listeners: {
				'toggleDetail': '@{toggleDetail}'
			}
		}, {
			text: '~~preview~~',
			itemId: 'searchpreviewbutton',
			hidden: '@{!isPreviewable}',
			enableToggle: true,
			handler: function() {
				var preview = this.up().up().up().down('#preview');

				if (this.pressed) {
					preview.expand();
					this.fireEvent('preview');
				} else {
					preview.collapse();
				}
			},
			listeners: {
				preview: '@{preview}'
			}
		}, '->', {
			xtype: 'tbtext',
			text: '@{pageInfo}'
		}, {
			iconCls: 'x-tbar-page-first',
			name: 'firstPage',
			tooltip: '~~firstPage~~',
			text: ''
		}, {
			iconCls: 'x-tbar-page-prev',
			name: 'previousPage',
			tooltip: '~~previousPage~~',
			text: ''
		}, {
			iconCls: 'x-tbar-page-next',
			name: 'nextPage',
			tooltip: '~~nextPage~~',
			text: ''
		}, {
			iconCls: 'x-tbar-page-last',
			name: 'lastPage',
			tooltip: '~~lastPage~~',
			text: ''
		}, {
			xtype: 'tbseparator'
		}, {
			name: 'refresh',
			text: '',
			iconCls: 'icon-repeat social-post-toolbar',
			tooltip: '~~refreshTooltip~~'
		}]
	}],

	items: [{
		region: 'center',
		layout: 'card',
		activeItem: '@{activeItem}',
		items: [{
			xtype: 'panel',
			autoScroll: true,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: '@{documents}',
			listeners: {
				documentsSelectionChange: '@{documentsSelectionChange}',
				preview: '@{preview}',
				docrendererselectionchange: function(renderer, id) {
					this.fireEvent('documentsSelectionChange', renderer, renderer);

					if (!this.up().up().up().up().down('#preview').getCollapsed()) {
						this.fireEvent('preview');
					}
				},
				activate: function() {
					var parent = this.up().up().up().up();
					var preview = parent.down('#preview');
					var btn = parent.down('#searchpreviewbutton');

					if (btn.pressed) {
						preview.expand();
					} else {
						preview.collapse();
					}
				}
			}
		}, {
			xtype: 'panel',
			autoScroll: true,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: '@{posts}',
			listeners: {
				activate: function() {
					var preview = this.up().up().up().up().down('#preview');
					preview.collapse();
				}
			}
		}, {
			xtype: 'panel',
			autoScroll: true,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: '@{automations}',
			listeners: {
				automationsSelectionChange: '@{automationsSelectionChange}',
				preview: '@{preview}',
				autoselchange: function(renderer, id) {
					this.fireEvent('automationsSelectionChange', renderer, renderer);

					if (!this.up().up().up().up().down('#preview').getCollapsed()) {
						this.fireEvent('preview');
					}
				},
				activate: function() {
					var parent = this.up().up().up().up();
					var preview = parent.down('#preview');
					var btn = parent.down('#searchpreviewbutton');

					if (btn.pressed) {
						preview.expand();
					} else {
						preview.collapse();
					}
				}
			}
		}]
	}, {
		region: 'east',
		itemId: 'preview',
		collapsed: '@{previewCollapsed}',
		collapsible: true,
		animCollapse: false,
		padding: '20px 0px 0px 0px',
		hideCollapseTool: true,
		collapseDirection: 'right',
		split: true,
		border: false,
		width: '50%',
		collapseMode: 'mini',
		html: '<iframe id="searchresult-iframe" src="@{iframe}" frameborder="0" width="99%" height="100%"></iframe>',
		preventHeader: true,
		listeners: {
			render: function() {
				//this.hide(); 
			},
			beforeexpand: function() {
				// this.show()
			},
			collapse: function() {
				// this.hide()
			}
		}
	}]
});

glu.defView('RS.search.NothingFoundRenderer', {
	xtype: 'panel',
	html: '~~nothingFound~~'
});
// glu.defView('RS.search.Main', {
// 	padding: '10px',
// 	layout: 'border',
// 	id: 'mysearchpage',
// 	dockedItems: [{
// 		xtype: 'toolbar',
// 		dock: 'top',
// 		ui: 'display-toolbar',
// 		defaultButtonUI: 'display-toolbar-button',
// 		items: [{
// 			name: 'goToSearchHome',
// 			cls: 'rs-display-name'
// 		}, {
// 			xtype: 'combobox',
// 			store: '@{documentSuggestions}',
// 			displayField: 'suggestion',
// 			valueField: 'suggestion',
// 			triggerCls: 'x-form-search-trigger',
// 			name: 'documentKeyword',
// 			hideLabel: true,
// 			emptyText: '~~emptySearch~~',
// 			width: 520,
// 			queryMode: 'remote',
// 			minChars: 2,
// 			typeAheadDelay: 1,
// 			autoSelect: false,
// 			listeners: {
// 				specialKey: function(field, e) {
// 					if (e.getKey() == e.ENTER) field.fireEvent('search', field)
// 				},
// 				search: '@{searchNow}'
// 			}
// 		}, {
// 			xtype: 'combobox',
// 			store: '@{socialSuggestions}',
// 			displayField: 'suggestion',
// 			valueField: 'suggestion',
// 			triggerCls: 'x-form-search-trigger',
// 			name: 'socialKeyword',
// 			hideLabel: true,
// 			emptyText: '~~emptySearch~~',
// 			width: 520,
// 			queryMode: 'remote',
// 			minChars: 2,
// 			typeAheadDelay: 1,
// 			autoSelect: false,
// 			listeners: {
// 				specialKey: function(field, e) {
// 					if (e.getKey() == e.ENTER) field.fireEvent('search', field)
// 				},
// 				search: '@{searchNow}'
// 			}
// 		}, {
// 			xtype: 'combobox',
// 			store: '@{automationSuggestions}',
// 			displayField: 'suggestion',
// 			valueField: 'suggestion',
// 			triggerCls: 'x-form-search-trigger',
// 			name: 'automationKeyword',
// 			hideLabel: true,
// 			emptyText: '~~emptySearch~~',
// 			width: 520,
// 			queryMode: 'remote',
// 			minChars: 2,
// 			typeAheadDelay: 1,
// 			autoSelect: false,
// 			listeners: {
// 				specialKey: function(field, e) {
// 					if (e.getKey() == e.ENTER) field.fireEvent('search', field)
// 				},
// 				search: '@{searchNow}'
// 			}
// 		}, '->', 'documentTab', 'socialTab', 'automationTab']
// 	}],

// 	items: [{
// 		region: 'center',
// 		layout: 'card',
// 		activeItem: '@{activeItem}',
// 		items: [{
// 			xtype: 'grid',
// 			name: 'documents',
// 			columns: '@{documentsColumns}',
// 			plugins: [{
// 				ptype: 'pager'
// 			}, {
// 				ptype: 'resolveexpander',
// 				resolveAfterExpand: function() {
// 					var preview = this.grid.ownerCt.ownerCt.down('#preview');
// 					preview.collapse();
// 				},
// 				resolveAfterCollapse: function() {
// 					var preview = this.grid.ownerCt.ownerCt.down('#preview');
// 					preview.collapse();
// 				},

// 				getHeaderConfig: function() {
// 					var me = this;

// 					return {
// 						width: 24,
// 						lockable: false,
// 						sortable: false,
// 						resizable: false,
// 						draggable: false,
// 						hideable: false,
// 						menuDisabled: true,
// 						tdCls: Ext.baseCSSPrefix + 'grid-cell-special',
// 						innerCls: Ext.baseCSSPrefix + 'grid-cell-inner-row-expander',
// 						renderer: function(value, metadata) {
// 							if (!me.grid.ownerLockable) {
// 								metadata.tdAttr += ' rowspan="2"';
// 							}
// 							return '<div class="icon-eye-open"></div>';
// 						},
// 						processEvent: function(type, view, cell, rowIndex, cellIndex, e, record) {
// 							if (type == "mousedown" && e.getTarget('.icon-eye-open')) {
// 								view.ownerCt.fireEvent('preview', view, record);
// 								var preview = view.ownerCt.ownerCt.ownerCt.down('#preview')
// 								preview.expand();
// 								return me.selectRowOnExpand;
// 							}
// 						}
// 					};
// 				}
// 			}],
// 			dockedItems: [{
// 				xtype: 'toolbar',
// 				dock: 'top',
// 				cls: 'actionBar',
// 				name: 'actionBar',
// 				items: []
// 			}],
// 			viewConfig: {
// 				enableTextSelection: true,
// 				listeners: {
// 					expandbody: function() {
// 						Ext.each(this.ownerCt.plugins, function(p) {
// 							if (p.ptype != 'resolveexpander')
// 								return;
// 							if (p.batchToggling)
// 								return;
// 							p.replaceHeaderNoFurtherAction();
// 						})
// 					},

// 					collapsebody: function() {
// 						Ext.each(this.ownerCt.plugins, function(p) {
// 							if (p.ptype != 'resolveexpander')
// 								return;
// 							if (p.batchToggling)
// 								return;
// 							p.replaceHeaderNoFurtherAction();
// 						})
// 					}
// 				}
// 			},
// 			listeners: {
// 				render: function(grid) {
// 					grid.getStore().on("load", function() {
// 						Ext.each(grid.plugins, function(p) {
// 							if (p.ptype != 'resolveexpander')
// 								return;
// 							p.gridLoaded();
// 							p.collapseAll();
// 							p.replaceHeaderNoFurtherAction();

// 							grid.on('cellclick', function(view, td, cellIndex, record, tr, rowIndex, e, eOpts) {
// 								if (cellIndex > 0) {
// 									p.onDblClick(view, record, null, rowIndex, e)
// 								}
// 							})
// 						})
// 					});
// 				},

// 				preview: '@{updatePreview}'
// 			}
// 		}, {
// 			xtype: 'panel',
// 			layout: {
// 				type: 'vbox',
// 				align: 'stretch'
// 			},
// 			dockedItems: [{
// 				xtype: 'toolbar',
// 				items: ['->', {
// 					xtype: 'tbtext',
// 					text: '@{pageInfo}'
// 				}, {
// 					iconCls: 'icon-chevron-left',
// 					name: 'previousPage',
// 					tooltip: '~~previousPage~~',
// 					text: ''
// 				}, {
// 					iconCls: 'icon-chevron-right',
// 					name: 'nextPage',
// 					tooltip: '~~nextPage~~',
// 					text: ''
// 				}, {
// 					xtype: 'tbseparator'
// 				}, {
// 					name: 'refreshPosts',
// 					text: '',
// 					// hidden: '@{!showPostToolbarButtons}',
// 					iconCls: 'icon-repeat social-post-toolbar',
// 					tooltip: '~~refreshPostsTooltip~~'
// 				}]
// 			}],
// 			items: '@{posts}'
// 		}, {
// 			xtype: 'grid',
// 			name: 'automation',
// 			store: '@{automationStore}',
// 			columns: '@{automationColumns}',
// 			plugins: [{
// 				ptype: 'pager'
// 			}],
// 			dockedItems: [{
// 				xtype: 'toolbar',
// 				dock: 'top',
// 				cls: 'actionBar',
// 				name: 'actionBar',
// 				items: []
// 			}]
// 		}]
// 	}, {
// 		region: 'east',
// 		id: 'preview',
// 		collapsed: '@{previewCollapsed}',
// 		collapsible: true,
// 		hidden: '@{!showPreview}',
// 		// hideCollapseTool: true,
// 		collapseDirection: 'right',
// 		split: true,
// 		border: false,
// 		width: '40%',
// 		html: '@{iframe}',
// 		hiddenOnCollapse: true,
// 		// preventHeader: true,
// 		listeners: {
// 			beforeexpand: function() {
// 				this.show()
// 			},
// 			collapse: function() {
// 				this.hide()
// 			}
// 		}
// 	}]
// })
glu.defView('RS.search.SearchHome', {
	layout: 'border',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'title-toolbar',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		enableOverflow: true,
		items: [{
			name: 'searchTab',
			pressed: '@{searchTabIsPressed}'
		}, {
			xtype: 'trigger',
			triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
			name: 'searchcombo',
			allowBlank: true,
			width: 520,
			margin: '5 0 0 20',
			style: 'margin-left: 1px',
			enableKeyEvents: true,
			hideLabel: true,
			keyDelay: 500,
			onTriggerClick: function() {
				this.fireEvent('searchTrigger');
			},
			listeners: {
				searchTrigger: '@{searchButtonTriggered}'
			}
		}]
	}],

	items: [{
			region: 'west',
			collapsible: false,
			defaults: {
				padding: '10px 20px 10px 10px'
			},
			flex: 1,
			items: [{
				title: 'My Recent Search',
				titleAlign: 'center',
				cls: 'rs-display-name',
				store: '@{searchdataofme}',
				xtype: 'grid',
				columns: '@{columnsofme}',
				bolder: false,
				split: true,
				listeners: {
					itemdblclick: '@{opensearch}'
				}
			}]
		}, {
			region: 'center',
			xtype: 'panel',
			collapsible: false,
			defaults: {
				padding: '10px 20px 10px 10px'
			},
			flex: 1,

			items: [{
				title: 'Top Search',
				titleAlign: 'center',
				store: '@{searchdataofall}',
				xtype: 'grid',
				columns: '@{columnsofall}',
				bolder: false,
				split: true,
				listeners: {
					itemdblclick: '@{opensearch}'
				}
			}]
		}, {
			region: 'east',
			xtype: 'panel',
			collapsible: false,
			defaults: {
				padding: '10px 20px 10px 10px'
			},
			flex: 1,

			items: [{
				title: 'Top Viewed Results',
				titleAlign: 'center',
				name: 'searchdata',
				xtype: 'grid',
				columns: '@{columns}',
				bolder: false,
				split: true
			}]
		}
	]
});
/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.search').locale = {
	back: 'Back',
	searchHome: 'Search',

	name: 'Name',
	fullName: 'Full Name',
	namespace: 'Namespace',
	title: 'Title',
	summary: 'Summary',
	content: 'Content',
	processModel: 'Process Model',
	abortModel: 'Abort Model',
	finalModel: 'Final Model',
	decisionTree: 'Decision Tree',
	locked: 'Locked',
	deleted: 'Deleted',
	hidden: 'Hidden',
	active: 'Active',

	tags: 'Tags',
	type: 'Type',
	doctype: 'Type',

	query: 'Query',
	selectCount: 'Total',
	rating: 'Rating',

	windowTitle: 'Search',

	emptySearch: 'Input the search keywords...',
	detail: 'Detail',
	'socialcomment.comment_Author_Us_ern_ame': 'Comment Author',
	'socialcomment.content': 'Comment',

	Main: {
		gotoSearchHome: 'Search',

		documentTab: 'Document',
		socialTab: 'Social',
		automationTab: 'Automation',
		firstPage: 'First Page',
		previousPage: 'Previous Page',
		nextPage: 'Next Page',
		lastPage: 'Last Page',
		refreshTooltip: 'Refresh',
		expand: 'Detail',
		collapse: 'Collapse',
		preview: 'Preview',
		//Copy from ad search
		attachment: 'Attachment',
		attachmentContent: 'Attachment Content',
		keyword: 'Keyword',
		createdOn: 'Created On',
		field: 'Fields',
		search: 'Search',
		cancel: 'Cancel',
		tag: 'Tags',
		resetInputs: 'Reset',
		sysCreatedBy: 'Created By',
		sysUpdatedBy: 'Updated By',
		authorUsername: 'Post Author',
		postTarget: 'Post Target',
		targetId: 'Target ID',
		comment: 'Comment',
		commentAuthorUsername: 'Comment Author',
		sysUpdatedOn: 'Updated',
		sysCreatedOn: 'Created'
	},
	more: 'More...',
	less: 'Less...',
	showAllTags: 'Show All Tags...',
	nothingFound: 'Nothing found.',
	lastSearchedOn: 'Last Searched On',
	updatedOn: 'Updated On',
	sysUpdatedBy: 'Updated By',
	updated: 'Updated',
	Home: {
		topHistory: 'My Search History',
		topQuery: 'Trending Queries'
	},
	subject: 'Subject',
	AdvanceSearchWindow: {
		advanceSearchTitle: 'Advanced Search',
		attachment: 'Attachment',
		attachmentContent: 'Attachment Content',
		keyword: 'Keyword',
		createdOn: 'Created On',
		field: 'Fields',
		search: 'Search',
		cancel: 'Cancel',
		tag: 'Tags',
		resetInputs: 'Reset',
		sysCreatedBy: 'Created By',
		sysUpdatedBy: 'Updated By',
		authorUsername: 'Post Author',
		postTarget: 'Post Target',
		targetId: 'Target ID',
		comment: 'Comment',
		commentAuthorUsername: 'Comment Author'
	}
}
