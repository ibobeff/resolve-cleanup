<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.resolve.util.JspUtils" %>
<%@page import="org.owasp.esapi.ESAPI"%>
<%
    String ver = ESAPI.encoder().encodeForURL(JspUtils.getResolveVersion());

    boolean debug = false;
    String d = request.getParameter("debug");
    if( d != null && d.indexOf("t") > -1){
        debug = true;
    }

    boolean debugSearch = false;
    String dT = request.getParameter("debugSearch");
    if( dT != null && dT.indexOf("t") > -1){
        debugSearch = true;
    }

    boolean mock = false;
    String m = request.getParameter("mock");
    if( m != null && m.indexOf("t") > -1){
        mock = true;
    }
%>

<%
    if( debug || debugSearch ){
%>
<link rel="stylesheet" type="text/css" href="/resolve/search/css/search-all.css?_v=<%=ver%>"/>
<script type="text/javascript" src="/resolve/search/js/search-classes.js?_v=<%=ver%>"></script>
<%
    }else{
%>
<link rel="stylesheet" type="text/css" href="/resolve/search/css/search-all.css?_v=<%=ver%>"/>
<script type="text/javascript" src="/resolve/search/js/search-classes.js?_v=<%=ver%>"></script>
<%
    }
%>

