/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.search').locale = {
	back: 'Back',
	searchHome: 'Search',

	name: 'Name',
	fullName: 'Full Name',
	namespace: 'Namespace',
	title: 'Title',
	summary: 'Summary',
	content: 'Content',
	processModel: 'Process Model',
	abortModel: 'Abort Model',
	finalModel: 'Final Model',
	decisionTree: 'Decision Tree',
	locked: 'Locked',
	deleted: 'Deleted',
	hidden: 'Hidden',
	active: 'Active',

	tags: 'Tags',
	type: 'Type',
	doctype: 'Type',

	query: 'Query',
	selectCount: 'Total',
	rating: 'Rating',

	windowTitle: 'Search',

	emptySearch: 'Input the search keywords...',
	detail: 'Detail',
	'socialcomment.comment_Author_Us_ern_ame': 'Comment Author',
	'socialcomment.content': 'Comment',

	Main: {
		gotoSearchHome: 'Search',

		documentTab: 'Document',
		socialTab: 'Social',
		automationTab: 'Automation',
		firstPage: 'First Page',
		previousPage: 'Previous Page',
		nextPage: 'Next Page',
		lastPage: 'Last Page',
		refreshTooltip: 'Refresh',
		expand: 'Detail',
		collapse: 'Collapse',
		preview: 'Preview',
		//Copy from ad search
		attachment: 'Attachment',
		attachmentContent: 'Attachment Content',
		keyword: 'Keyword',
		createdOn: 'Created On',
		field: 'Fields',
		search: 'Search',
		cancel: 'Cancel',
		tag: 'Tags',
		resetInputs: 'Reset',
		sysCreatedBy: 'Created By',
		sysUpdatedBy: 'Updated By',
		authorUsername: 'Post Author',
		postTarget: 'Post Target',
		targetId: 'Target ID',
		comment: 'Comment',
		commentAuthorUsername: 'Comment Author',
		sysUpdatedOn: 'Updated',
		sysCreatedOn: 'Created'
	},
	more: 'More...',
	less: 'Less...',
	showAllTags: 'Show All Tags...',
	nothingFound: 'Nothing found.',
	lastSearchedOn: 'Last Searched On',
	updatedOn: 'Updated On',
	sysUpdatedBy: 'Updated By',
	updated: 'Updated',
	Home: {
		topHistory: 'My Search History',
		topQuery: 'Trending Queries'
	},
	subject: 'Subject',
	AdvanceSearchWindow: {
		advanceSearchTitle: 'Advanced Search',
		attachment: 'Attachment',
		attachmentContent: 'Attachment Content',
		keyword: 'Keyword',
		createdOn: 'Created On',
		field: 'Fields',
		search: 'Search',
		cancel: 'Cancel',
		tag: 'Tags',
		resetInputs: 'Reset',
		sysCreatedBy: 'Created By',
		sysUpdatedBy: 'Updated By',
		authorUsername: 'Post Author',
		postTarget: 'Post Target',
		targetId: 'Target ID',
		comment: 'Comment',
		commentAuthorUsername: 'Comment Author'
	}
}