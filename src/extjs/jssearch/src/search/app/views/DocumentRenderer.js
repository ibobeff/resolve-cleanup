glu.defView('RS.search.DocumentRenderer', {
	xtype: 'panel',
	overCls: 'render-hover',
	style: 'border-bottom: 1px solid #eee !important;',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	width: '100%',
	bodyPadding: '3px',
	minWidth: 500,
	items: [{
		xtype: 'panel',
		layout: {
			type: 'vbox',
			//align: 'stretch'
		},
		items: [{
			xtype: 'tbtext',
			cls: 'render-title',
			overCls: 'render-title-hover',
			text: '@{title}',
			htmlEncode : false,
			listeners: {
				handler: '@{jumpToEdit}',
				render: function(image) {
					image.getEl().on('click', function() {
						image.fireEvent('handler')
					});
				}
			}
		}, {
			xtype: 'component',
			autoEl: '@{fullNameLink}',
			margins: {
				left: 5
			}
		}, {
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			margins: {
				left: 5
			},
			hidden: '@{!collapsed}',
			items: [{
				hidden: '@{detailed}',
				html: '@{displaySummary}',
				style: 'font-family:Arial;overflow-wrap: break-word;'
			}, {
				hideLabel: true,
				hidden: '@{!detailed}',
				html: '@{displayDetailedSummary}',
				style: 'font-family:Arial;overflow-wrap: break-word;'
			}, {
				html: '<span style="color:#999;cursor:pointer;width: 100!important">@{moreOrLess}</span>',
				hidden: '@{!canShowDetail}',
				listeners: {
					handler: '@{showDetail}',
					render: function(field) {
						field.getEl().on('click', function() {
							field.fireEvent('handler')
						});
						var renderer = field.getEl().down('span[style^="color:#999"]');
						if (renderer)
							renderer.addClsOnOver('render-title-hover')
					}
				}
			}, {
				xtype: 'displayfield',
				name: 'updated',
				labelWidth: 90,
				cls: 'render-info'
			}]
		}]
	}, {
		xtype: 'panel',
		collapsible: true,
		collapsed: '@{collapsed}',
		animCollapse: false,
		name: 'detail',
		hideCollapseTool: true,
		header: false,
		margins: {
			left: 5
		},
		padding: '0 10px 0px 0px',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			hidden: '@{detailed}',
			html: '@{displaySummary}',
			style: 'font-family:Arial;overflow-wrap: break-word;'
		}, {
			hideLabel: true,
			hidden: '@{!detailed}',
			html: '@{displayDetailedSummary}',
			style: 'font-family:Arial;overflow-wrap: break-word;'
		}, {
			xtype: 'displayfield',
			overCls: 'render-title-hover',
			style: 'color:#999;cursor:pointer;width: 100!important',
			fieldStyle: 'color:#999;cursor:pointer;',
			value: '@{moreOrLess}',
			hidden: '@{!canShowDetail}',
			listeners: {
				handler: '@{showDetail}',
				render: function(field) {
					field.getEl().on('click', function() {
						field.fireEvent('handler')
					});
				}
			}
		}, {
			xtype: 'displayfield',
			name: 'showTags',
			fieldLabel: '~~tags~~',
			labelWidth: 90,
			fieldStyle: 'overflow-wrap: break-word;',
			cls: 'render-info'
		}, {
			xtype: 'tbtext',
			padding: '0 0 0 90px',
			overCls: 'render-title-hover',
			style: 'color:#999;cursor:pointer;width: 100!important',
			fieldStyle: 'color:#999;cursor:pointer;',
			text: '@{moreOrLessTags}',
			hidden: '@{!showAllTagsIsVisible}',
			listeners: {
				handler: '@{showAllTags}',
				render: function(field) {
					field.getEl().on('click', function() {
						field.fireEvent('handler')
					});
				}
			}
		}, {
			xtype: 'displayfield',
			labelWidth: 90,
			name: 'doctype',
			cls: 'render-info'
		}, {
			xtype: 'displayfield',
			name: 'updated',
			labelWidth: 90,
			cls: 'render-info'
		}, {
			xtype: 'fieldcontainer',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'component',
				rating: '@{rating}',
				cls: 'rating'
			}, {
				xtype: 'tbtext',
				padding: '0px 0px 0px 10px',
				overCls: 'render-title-hover',
				style: 'color:#999;cursor:pointer;',
				text: '(@{numberOfReviews} reviews)',
				listeners: {
					handler: '@{socialReview}',
					render: function(image) {
						image.getEl().on('click', function() {
							image.fireEvent('handler')
						});
					}
				}

			}]
		}]
	}],
	recordId: '@{id}',
	listeners: {
		render: function(c) {
			var me = this;
			c.body.on('click', function() {
				me.ownerCt.fireEvent('docrendererselectionchange', me, me.recordId);
			});
			var comp = this.down('component[cls="rating"]');
			var rating = comp.rating;
			var floor = Math.floor(rating);
			var icons = '';
			for (var i = 0; i < floor; i++)
				icons += '<i class="icon-star"></i>'

			var r = rating - floor;
			if (r > 0.25) {
				icons += '<i class="icon-star-half-empty" style="color:rgb(246, 171, 0)"></i>'
				floor++;
			}
			for (var i = floor; i < 5; i++)
				icons += '<i class="icon-star-empty"></i>'
			Ext.get(comp.id).update(Ext.String.format('<span style="color:#999 !important;padding-right:40px">Ratings:</span>{0}', icons));
		}
	}
});