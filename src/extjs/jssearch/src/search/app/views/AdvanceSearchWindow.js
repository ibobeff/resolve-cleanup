glu.defView('RS.search.AdvanceSearchWindow', {
	title: '~~advanceSearchTitle~~',
	bodyPadding: '10px',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaultType: 'textfield',
	defaults: {
		labelWidth: 120
	},
	items: ['keyword', {
			xtype: 'combo',
			name: 'type',
			store: '@{typeStore}',
			displayField: 'name',
			valueField: 'value',
			queryMode: 'local',
			multiSelect: true,
			editable: false
		}, {
			xtype: 'combo',
			name: 'namespace',
			store: '@{namespaceStore}',
			displayField: 'unamespace',
			valueField: 'unamespace',
			typeAhead: false,
			multiSelect: true,
			minChars: 0
		},

		// {
		// 	xtype: 'combo',
		// 	name: 'sysUpdatedBy',
		// 	store: '@{sysUpdatedByStore}',
		// 	displayField: 'udisplayName',
		// 	valueField: 'uuserName',
		// 	typeAhead: false,
		// 	multiSelect: true,
		// 	minChars: 0
		// }, {
		// 	xtype: 'combo',
		// 	name: 'sysCreatedBy',
		// 	store: '@{sysCreatedByStore}',
		// 	displayField: 'udisplayName',
		// 	valueField: 'uuserName',
		// 	typeAhead: false,
		// 	multiSelect: true,
		// 	minChars: 0
		// }, {
		// 	xtype: 'combo',
		// 	name: 'authorUsername',
		// 	store: '@{authorUsernameStore}',
		// 	displayField: 'udisplayName',
		// 	valueField: 'uuserName',
		// 	typeAhead: false,
		// 	multiSelect: true,
		// 	minChars: 0
		// }, {
		// 	xtype: 'combo',
		// 	name: 'commentAuthorUsername',
		// 	store: '@{commentAuthorUsernameStore}',
		// 	displayField: 'udisplayName',
		// 	valueField: 'uuserName',
		// 	typeAhead: false,
		// 	multiSelect: true,
		// 	minChars: 0
		// },
		'authorUsername', 'commentAuthorUsername', 'sysUpdatedBy', {
			xtype: 'fieldcontainer',
			layout: 'hbox',
			items: [{
				xtype: 'combo',
				width: 210,
				fieldLabel: '~~updatedOn~~',
				labelWidth: 120,
				editable: false,
				name: 'updatedOnCondition',
				queryMode: 'local',
				displayField: 'condition',
				valueField: 'condition',
				store: '@{timeConditionStore}'
			}, {
				xtype: 'datefield',
				padding: '0px 0px 0px 5px',
				hideLabel: true,
				flex: 3,
				name: 'updatedOn',
				format: 'm/d/Y',
				maxValue: new Date(),
				listeners: {
					change: function(datefield, oldDate, newDate) {
						datefield.fireEvent('updatedOnChagned', datefield, oldDate, newDate);
					},
					validitychange: '@{setUpdatedOnValid}'
				}
			}, {
				xtype: 'timefield',
				flex: 2,
				hideLabel: true,
				padding: '0px 0px 0px 5px',
				name: 'updatedOnTime',
				listeners: {
					change: function(datefield, oldDate, newDate) {
						datefield.fireEvent('updatedOnTimeChanged', datefield, oldDate, newDate);
					},
					validitychange: '@{setUpdatedOnTimeValid}'
				}
			}]
		}, 'sysCreatedBy', {
			xtype: 'fieldcontainer',
			layout: 'hbox',
			items: [{
				xtype: 'combo',
				width: 210,
				fieldLabel: '~~createdOn~~',
				labelWidth: 120,
				editable: false,
				name: 'createdOnCondition',
				queryMode: 'local',
				displayField: 'condition',
				valueField: 'condition',
				store: '@{timeConditionStore}'
			}, {
				xtype: 'datefield',
				padding: '0px 0px 0px 5px',
				hideLabel: true,
				flex: 3,
				name: 'createdOn',
				format: 'm/d/Y',
				maxValue: new Date(),
				listeners: {
					change: function(datefield, oldDate, newDate) {
						datefield.fireEvent('createdOnChanged', datefield, oldDate, newDate);
					},
					validitychange: '@{setCreatedOnValid}'
				}
			}, {
				xtype: 'timefield',
				flex: 2,
				padding: '0px 0px 0px 5px',
				name: 'createdOnTime',
				hideLabel: true,
				listeners: {
					change: function(datefield, oldDate, newDate) {
						datefield.fireEvent('createdOnTimeChanged', datefield, oldDate, newDate);
					},
					validitychange: '@{setCreatedOnTimeValid}'
				}
			}]
		}, {
			xtype: 'combo',
			name: 'field',
			store: '@{fieldStore}',
			queryMode: 'local',
			displayField: 'name',
			valueField: 'value',
			multiSelect: true,
			editable: false
		}, {
			xtype: 'combo',
			name: 'targetId',
			store: '@{targetIds}',
			displayField: 'displayName',
			valueField: 'id',
			multiSelect: true,
			queryMode: 'local'
		}, {
			xtype: 'combo',
			name: 'tag',
			store: '@{tagStore}',
			displayField: 'name',
			valueField: 'name',
			multiSelect: true,
			minChars: 0,
			pageSize: 10
		}
	],
	buttonAlign: 'left',
	modal: true,
	buttons: ['search', 'resetInputs', 'cancel'],

	baseCls: Ext.isIE8m ? 'x-window' : 'x-panel',
	ui: Ext.isIE8m ? 'default' : 'sysinfo-dialog',
	draggable: false,
	target: '@{target}',

	listeners: {
		render: function(panel) {
			panel.setWidth(Ext.isIE8m ? 550 : Ext.get(panel.target).getWidth());
			if (panel.target) {
				Ext.defer(function() {
					panel.alignTo(panel.target)
				}, 1)
			}
		}
	}
});