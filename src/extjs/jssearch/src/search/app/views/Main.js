glu.defView('RS.search.Main', {
	layout: 'border',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			htmlEncode : false,
			text: '@{homeText}&nbsp&nbsp<i class="icon-caret-down" style="color:rgb(153, 153, 153)"></i>&nbsp',
			cls: 'rs-display-name',
			pressed: '@{gotoSearchHomeIsPressed}',
			handler: function(button) {
				button.fireEvent('gotoSearchHome', button, button)
			},
			listeners: {
				gotoSearchHome: '@{gotoSearchHome}'
			}
		}, {
			xtype: 'combobox',
			store: '@{documentSuggestions}',
			displayField: 'suggestion',
			valueField: 'suggestion',
			trigger2Cls: 'x-form-search-trigger',
			name: 'documentKeyword',
			hideLabel: true,
			emptyText: '~~emptySearch~~',
			width: 520,
			queryMode: 'remote',
			keyDelay:  0,
			minChars: 0,
			autoSelect: false,
			listConfig: {
				loadMask: false
			},
			listeners: {
				specialKey: function(field, e) {
					var key = e.getKey();

					if (key === e.ENTER) {
						field.fireEvent('search', field)
						this.fireEvent('resetFocus');
					} else if (key === e.END) {
						var value = field.value;
						field.value = '';
						field.focus();
						field.value = value;
					} else if (key === e.HOME) {
						field.focus();
					}
				},
				render: function (field) {
					this._vm.on('resetFocus', function () {
						field.focus();
					});
				},
				search: '@{triggerSearch}',
				advanceSearch: '@{advanceSearch}'
			},
			onTriggerClick: function() {
				this.fireEvent('advanceSearch', this, this);
			},
			onTrigger2Click: function() {
				this.fireEvent('search');
			}
		}, {
			xtype: 'combobox',
			store: '@{socialSuggestions}',
			displayField: 'suggestion',
			valueField: 'suggestion',
			trigger2Cls: 'x-form-search-trigger',
			name: 'socialKeyword',
			hideLabel: true,
			emptyText: '~~emptySearch~~',
			width: 520,
			queryMode: 'remote',
			minChars: 2,
			typeAheadDelay: 1,
			autoSelect: false,
			listConfig: {
				loadMask: false
			},
			listeners: {
				specialKey: function(field, e) {
					var key = e.getKey();

					if (key === e.ENTER) {
						field.fireEvent('search', field);
						this.fireEvent('resetFocus');
					} else if (key === e.END) {
						var value = field.value;
						field.value = '';
						field.focus();
						field.value = value;
					} else if (key === e.HOME) {
						field.focus();
					}
				},
				render: function (field) {
					this._vm.on('resetFocus', function () {
						field.focus();
					});
				},
				search: '@{searchNow}',
				advanceSearch: '@{advanceSearch}'
			},
			onTriggerClick: function() {
				this.fireEvent('advanceSearch', this, this);
			},
			onTrigger2Click: function() {
				this.fireEvent('search');
			}
		}, {
			xtype: 'combobox',
			store: '@{automationSuggestions}',
			displayField: 'suggestion',
			valueField: 'suggestion',
			trigger2Cls: 'x-form-search-trigger',
			name: 'automationKeyword',
			hideLabel: true,
			emptyText: '~~emptySearch~~',
			width: 520,
			queryMode: 'remote',
			minChars: 2,
			typeAheadDelay: 1,
			autoSelect: false,
			listConfig: {
				loadMask: false
			},
			listeners: {
				specialKey: function(field, e) {
					var key = e.getKey();

					if (key === e.ENTER) {
						field.fireEvent('search', field);
						this.fireEvent('resetFocus');
					} else if (key === e.END) {
						var value = field.value;
						field.value = '';
						field.focus();
						field.value = value;
					} else if (key === e.HOME) {
						field.focus();
					}
				},
				render: function (field) {
					this._vm.on('resetFocus', function () {
						field.focus();
					});
				},
				search: '@{searchNow}',
				advanceSearch: '@{advanceSearch}'
			},
			onTriggerClick: function() {
				this.fireEvent('advanceSearch', this, this);
			},
			onTrigger2Click: function() {
				this.fireEvent('search');
			}
		}, '->', 'documentTab', 'socialTab', 'automationTab']
	}, {
		xtype: 'container',
		items: [{
			xtype: 'filterbar',
			filterItems: '@{filterItems}',
			localizer: '@{localizer}',
			allowPersistFilter: false,
			setFilterItems: function(filterItems) {
				this.clearFilter(null, false);

				var map = {
					'sysCreatedBy': true,
					'sysCreatedOn': true,
					'sysUpdatedBy': true,
					'sysUpdatedOn': true,
					'type': true,
					'namespace': true,
					'targetId': true,
					'tag': true
				};

				Ext.each(filterItems, function(filter) {
					if (!filter.field) {
						return;
					}

					if (!map[filter.field]) {
						var fields = filter.field.split(',')
						var fieldText = [];
						Ext.each(fields, function(field) {
							fieldText.push(this.localizer.doLocalize(field));
						}, this);
						this.addFilter(Ext.String.format('In {0}', fieldText.join(',')), filter.field)
						return;
					}

					if (filter.field === 'tag') {
						this.addFilter('Tagged by ' + filter.value, filter.field);
						return;
					}

					if (filter.field === 'targetId') {
						this.addFilter('With Target ID:' + filter.displayFields)
						return;
					}

					if (!!filter.value) {
						this.addFilter(Ext.String.format('{0} {1} {2}', this.localizer.doLocalize(filter.field), filter.condition, filter.value), filter.field);
					}
				}, this)
			},

			listeners: {
				clearFilter: '@{clearFilter}',
				remove: function(filterBar, removedItem) {
					if (!this.clearingFilters) {
						this.fireEvent('removeFilter', this, removedItem.internalValue);
					}
				},
				removeFilter: '@{removeFilter}'
			}
		}]
	}, {
		xtype: 'toolbar',
		cls: 'actionBar-form',
		name: 'actionBar',
		items: [{
			text: '~~expand~~',
			hidden: '@{!localRendered}',
			itemId: 'expand',
			pressed: '@{expanded}',
			handler: function() {
				var panel = this.up().up().items.getAt(0);
				var active = panel.getLayout().getActiveItem();
				Ext.each(active.items.items, function(renderer) {
					renderer.down('panel[name="detail"]')[!this.pressed ? 'expand' : 'collapse']();
				}, this);
				this.fireEvent('toggleDetail', this, !this.pressed);
			},
			listeners: {
				'toggleDetail': '@{toggleDetail}'
			}
		}, {
			text: '~~preview~~',
			itemId: 'searchpreviewbutton',
			hidden: '@{!isPreviewable}',
			enableToggle: true,
			handler: function() {
				var preview = this.up().up().up().down('#preview');

				if (this.pressed) {
					preview.expand();
					this.fireEvent('preview');
				} else {
					preview.collapse();
				}
			},
			listeners: {
				preview: '@{preview}'
			}
		}, '->', {
			xtype: 'tbtext',
			text: '@{pageInfo}'
		}, {
			iconCls: 'x-tbar-page-first',
			name: 'firstPage',
			tooltip: '~~firstPage~~',
			text: ''
		}, {
			iconCls: 'x-tbar-page-prev',
			name: 'previousPage',
			tooltip: '~~previousPage~~',
			text: ''
		}, {
			iconCls: 'x-tbar-page-next',
			name: 'nextPage',
			tooltip: '~~nextPage~~',
			text: ''
		}, {
			iconCls: 'x-tbar-page-last',
			name: 'lastPage',
			tooltip: '~~lastPage~~',
			text: ''
		}, {
			xtype: 'tbseparator'
		}, {
			name: 'refresh',
			text: '',
			iconCls: 'icon-repeat social-post-toolbar',
			tooltip: '~~refreshTooltip~~'
		}]
	}],

	items: [{
		region: 'center',
		layout: 'card',
		activeItem: '@{activeItem}',
		items: [{
			xtype: 'panel',
			autoScroll: true,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: '@{documents}',
			listeners: {
				documentsSelectionChange: '@{documentsSelectionChange}',
				preview: '@{preview}',
				docrendererselectionchange: function(renderer, id) {
					this.fireEvent('documentsSelectionChange', renderer, renderer);

					if (!this.up().up().up().up().down('#preview').getCollapsed()) {
						this.fireEvent('preview');
					}
				},
				activate: function() {
					var parent = this.up().up().up().up();
					var preview = parent.down('#preview');
					var btn = parent.down('#searchpreviewbutton');

					if (btn.pressed) {
						preview.expand();
					} else {
						preview.collapse();
					}
				}
			}
		}, {
			xtype: 'panel',
			autoScroll: true,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: '@{posts}',
			listeners: {
				activate: function() {
					var preview = this.up().up().up().up().down('#preview');
					preview.collapse();
				}
			}
		}, {
			xtype: 'panel',
			autoScroll: true,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: '@{automations}',
			listeners: {
				automationsSelectionChange: '@{automationsSelectionChange}',
				preview: '@{preview}',
				autoselchange: function(renderer, id) {
					this.fireEvent('automationsSelectionChange', renderer, renderer);

					if (!this.up().up().up().up().down('#preview').getCollapsed()) {
						this.fireEvent('preview');
					}
				},
				activate: function() {
					var parent = this.up().up().up().up();
					var preview = parent.down('#preview');
					var btn = parent.down('#searchpreviewbutton');

					if (btn.pressed) {
						preview.expand();
					} else {
						preview.collapse();
					}
				}
			}
		}]
	}, {
		region: 'east',
		itemId: 'preview',
		collapsed: '@{previewCollapsed}',
		collapsible: true,
		animCollapse: false,
		padding: '20px 0px 0px 0px',
		hideCollapseTool: true,
		collapseDirection: 'right',
		split: true,
		border: false,
		width: '50%',
		collapseMode: 'mini',
		html: '<iframe id="searchresult-iframe" src="@{iframe}" frameborder="0" width="99%" height="100%"></iframe>',
		preventHeader: true,
		listeners: {
			render: function() {
				//this.hide(); 
			},
			beforeexpand: function() {
				// this.show()
			},
			collapse: function() {
				// this.hide()
			}
		}
	}]
});

glu.defView('RS.search.NothingFoundRenderer', {
	xtype: 'panel',
	html: '~~nothingFound~~'
});