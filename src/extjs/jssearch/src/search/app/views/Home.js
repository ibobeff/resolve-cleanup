glu.defView('RS.search.Home', {
	title: '~~searchHome~~',
	baseCls: Ext.isIE8m ? 'x-window' : 'x-panel',
	ui: Ext.isIE8m ? 'default' : 'sysinfo-dialog',
	draggable: false,
	target: '@{target}',
	listeners: {
		render: function(panel) {
			if (panel.target) {
				Ext.defer(function() {
					panel.alignTo(panel.target, 'tl-bl')
				}, 1)
			}
		},

		close: '@{toggleUpHomeButton}'
	},

	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	bodyPadding: '10px',
	defaultType: 'grid',
	defaults: {
		maximunHeight: 400,
		flex: 1,
		cls : 'rs-grid-dark'
	},
	items: [{
		title: '~~topHistory~~',
		name: 'history',
		store: '@{historyStore}',
		columns: '@{historyColumns}',
		margins: {
			right: 5
		},
		listeners: {
			search: '@{search}',
			itemclick: function(item, record) {
				this.fireEvent('search', item, record);
			}
		}
	}, {
		title: '~~topQuery~~',
		name: 'topQuery',
		store: '@{topQueryStore}',
		columns: '@{topQueryColumns}',
		margins: {
			left: 5
		},
		listeners: {
			search: '@{search}',
			itemclick: function(item, record) {
				this.fireEvent('search', item, record);
			}
		}
	}],
	width: 700
})