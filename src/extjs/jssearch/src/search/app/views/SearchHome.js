glu.defView('RS.search.SearchHome', {
	layout: 'border',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'title-toolbar',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		enableOverflow: true,
		items: [{
			name: 'searchTab',
			pressed: '@{searchTabIsPressed}'
		}, {
			xtype: 'trigger',
			triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
			name: 'searchcombo',
			allowBlank: true,
			width: 520,
			margin: '5 0 0 20',
			style: 'margin-left: 1px',
			enableKeyEvents: true,
			hideLabel: true,
			keyDelay: 500,
			onTriggerClick: function() {
				this.fireEvent('searchTrigger');
			},
			listeners: {
				searchTrigger: '@{searchButtonTriggered}'
			}
		}]
	}],

	items: [{
			region: 'west',
			collapsible: false,
			defaults: {
				padding: '10px 20px 10px 10px'
			},
			flex: 1,
			items: [{
				title: 'My Recent Search',
				titleAlign: 'center',
				cls: 'rs-display-name',
				store: '@{searchdataofme}',
				xtype: 'grid',
				columns: '@{columnsofme}',
				bolder: false,
				split: true,
				listeners: {
					itemdblclick: '@{opensearch}'
				}
			}]
		}, {
			region: 'center',
			xtype: 'panel',
			collapsible: false,
			defaults: {
				padding: '10px 20px 10px 10px'
			},
			flex: 1,

			items: [{
				title: 'Top Search',
				titleAlign: 'center',
				store: '@{searchdataofall}',
				xtype: 'grid',
				columns: '@{columnsofall}',
				bolder: false,
				split: true,
				listeners: {
					itemdblclick: '@{opensearch}'
				}
			}]
		}, {
			region: 'east',
			xtype: 'panel',
			collapsible: false,
			defaults: {
				padding: '10px 20px 10px 10px'
			},
			flex: 1,

			items: [{
				title: 'Top Viewed Results',
				titleAlign: 'center',
				name: 'searchdata',
				xtype: 'grid',
				columns: '@{columns}',
				bolder: false,
				split: true
			}]
		}
	]
});