// glu.defView('RS.search.Main', {
// 	padding: '10px',
// 	layout: 'border',
// 	id: 'mysearchpage',
// 	dockedItems: [{
// 		xtype: 'toolbar',
// 		dock: 'top',
// 		ui: 'display-toolbar',
// 		defaultButtonUI: 'display-toolbar-button',
// 		items: [{
// 			name: 'goToSearchHome',
// 			cls: 'rs-display-name'
// 		}, {
// 			xtype: 'combobox',
// 			store: '@{documentSuggestions}',
// 			displayField: 'suggestion',
// 			valueField: 'suggestion',
// 			triggerCls: 'x-form-search-trigger',
// 			name: 'documentKeyword',
// 			hideLabel: true,
// 			emptyText: '~~emptySearch~~',
// 			width: 520,
// 			queryMode: 'remote',
// 			minChars: 2,
// 			typeAheadDelay: 1,
// 			autoSelect: false,
// 			listeners: {
// 				specialKey: function(field, e) {
// 					if (e.getKey() == e.ENTER) field.fireEvent('search', field)
// 				},
// 				search: '@{searchNow}'
// 			}
// 		}, {
// 			xtype: 'combobox',
// 			store: '@{socialSuggestions}',
// 			displayField: 'suggestion',
// 			valueField: 'suggestion',
// 			triggerCls: 'x-form-search-trigger',
// 			name: 'socialKeyword',
// 			hideLabel: true,
// 			emptyText: '~~emptySearch~~',
// 			width: 520,
// 			queryMode: 'remote',
// 			minChars: 2,
// 			typeAheadDelay: 1,
// 			autoSelect: false,
// 			listeners: {
// 				specialKey: function(field, e) {
// 					if (e.getKey() == e.ENTER) field.fireEvent('search', field)
// 				},
// 				search: '@{searchNow}'
// 			}
// 		}, {
// 			xtype: 'combobox',
// 			store: '@{automationSuggestions}',
// 			displayField: 'suggestion',
// 			valueField: 'suggestion',
// 			triggerCls: 'x-form-search-trigger',
// 			name: 'automationKeyword',
// 			hideLabel: true,
// 			emptyText: '~~emptySearch~~',
// 			width: 520,
// 			queryMode: 'remote',
// 			minChars: 2,
// 			typeAheadDelay: 1,
// 			autoSelect: false,
// 			listeners: {
// 				specialKey: function(field, e) {
// 					if (e.getKey() == e.ENTER) field.fireEvent('search', field)
// 				},
// 				search: '@{searchNow}'
// 			}
// 		}, '->', 'documentTab', 'socialTab', 'automationTab']
// 	}],

// 	items: [{
// 		region: 'center',
// 		layout: 'card',
// 		activeItem: '@{activeItem}',
// 		items: [{
// 			xtype: 'grid',
// 			name: 'documents',
// 			columns: '@{documentsColumns}',
// 			plugins: [{
// 				ptype: 'pager'
// 			}, {
// 				ptype: 'resolveexpander',
// 				resolveAfterExpand: function() {
// 					var preview = this.grid.ownerCt.ownerCt.down('#preview');
// 					preview.collapse();
// 				},
// 				resolveAfterCollapse: function() {
// 					var preview = this.grid.ownerCt.ownerCt.down('#preview');
// 					preview.collapse();
// 				},

// 				getHeaderConfig: function() {
// 					var me = this;

// 					return {
// 						width: 24,
// 						lockable: false,
// 						sortable: false,
// 						resizable: false,
// 						draggable: false,
// 						hideable: false,
// 						menuDisabled: true,
// 						tdCls: Ext.baseCSSPrefix + 'grid-cell-special',
// 						innerCls: Ext.baseCSSPrefix + 'grid-cell-inner-row-expander',
// 						renderer: function(value, metadata) {
// 							if (!me.grid.ownerLockable) {
// 								metadata.tdAttr += ' rowspan="2"';
// 							}
// 							return '<div class="icon-eye-open"></div>';
// 						},
// 						processEvent: function(type, view, cell, rowIndex, cellIndex, e, record) {
// 							if (type == "mousedown" && e.getTarget('.icon-eye-open')) {
// 								view.ownerCt.fireEvent('preview', view, record);
// 								var preview = view.ownerCt.ownerCt.ownerCt.down('#preview')
// 								preview.expand();
// 								return me.selectRowOnExpand;
// 							}
// 						}
// 					};
// 				}
// 			}],
// 			dockedItems: [{
// 				xtype: 'toolbar',
// 				dock: 'top',
// 				cls: 'actionBar',
// 				name: 'actionBar',
// 				items: []
// 			}],
// 			viewConfig: {
// 				enableTextSelection: true,
// 				listeners: {
// 					expandbody: function() {
// 						Ext.each(this.ownerCt.plugins, function(p) {
// 							if (p.ptype != 'resolveexpander')
// 								return;
// 							if (p.batchToggling)
// 								return;
// 							p.replaceHeaderNoFurtherAction();
// 						})
// 					},

// 					collapsebody: function() {
// 						Ext.each(this.ownerCt.plugins, function(p) {
// 							if (p.ptype != 'resolveexpander')
// 								return;
// 							if (p.batchToggling)
// 								return;
// 							p.replaceHeaderNoFurtherAction();
// 						})
// 					}
// 				}
// 			},
// 			listeners: {
// 				render: function(grid) {
// 					grid.getStore().on("load", function() {
// 						Ext.each(grid.plugins, function(p) {
// 							if (p.ptype != 'resolveexpander')
// 								return;
// 							p.gridLoaded();
// 							p.collapseAll();
// 							p.replaceHeaderNoFurtherAction();

// 							grid.on('cellclick', function(view, td, cellIndex, record, tr, rowIndex, e, eOpts) {
// 								if (cellIndex > 0) {
// 									p.onDblClick(view, record, null, rowIndex, e)
// 								}
// 							})
// 						})
// 					});
// 				},

// 				preview: '@{updatePreview}'
// 			}
// 		}, {
// 			xtype: 'panel',
// 			layout: {
// 				type: 'vbox',
// 				align: 'stretch'
// 			},
// 			dockedItems: [{
// 				xtype: 'toolbar',
// 				items: ['->', {
// 					xtype: 'tbtext',
// 					text: '@{pageInfo}'
// 				}, {
// 					iconCls: 'icon-chevron-left',
// 					name: 'previousPage',
// 					tooltip: '~~previousPage~~',
// 					text: ''
// 				}, {
// 					iconCls: 'icon-chevron-right',
// 					name: 'nextPage',
// 					tooltip: '~~nextPage~~',
// 					text: ''
// 				}, {
// 					xtype: 'tbseparator'
// 				}, {
// 					name: 'refreshPosts',
// 					text: '',
// 					// hidden: '@{!showPostToolbarButtons}',
// 					iconCls: 'icon-repeat social-post-toolbar',
// 					tooltip: '~~refreshPostsTooltip~~'
// 				}]
// 			}],
// 			items: '@{posts}'
// 		}, {
// 			xtype: 'grid',
// 			name: 'automation',
// 			store: '@{automationStore}',
// 			columns: '@{automationColumns}',
// 			plugins: [{
// 				ptype: 'pager'
// 			}],
// 			dockedItems: [{
// 				xtype: 'toolbar',
// 				dock: 'top',
// 				cls: 'actionBar',
// 				name: 'actionBar',
// 				items: []
// 			}]
// 		}]
// 	}, {
// 		region: 'east',
// 		id: 'preview',
// 		collapsed: '@{previewCollapsed}',
// 		collapsible: true,
// 		hidden: '@{!showPreview}',
// 		// hideCollapseTool: true,
// 		collapseDirection: 'right',
// 		split: true,
// 		border: false,
// 		width: '40%',
// 		html: '@{iframe}',
// 		hiddenOnCollapse: true,
// 		// preventHeader: true,
// 		listeners: {
// 			beforeexpand: function() {
// 				this.show()
// 			},
// 			collapse: function() {
// 				this.hide()
// 			}
// 		}
// 	}]
// })