// glu.defModel('RS.search.Search', {

// 	mock: false,

// 	//define URL parameters for search
// 	keyword: '',

// 	state: '',
// 	stateId: 'searchdataList',
// 	displayName: 'Search Home',

// 	//define the selection field
// 	typecombo: '',
// 	typecomboS: '',
// 	typecomboA: '',

// 	incombo: '',
// 	timecombo: '',
// 	nscombo: '',
// 	tagcombo: '',
// 	center: '',
// 	east: '',
// 	eastHidden: true,
// 	didyoumeanHidden: true,
// 	didyoumean: '',
// 	search: '',
// 	searchtag: '',
// 	searchcombo: '',
// 	presearch: '',
// 	issuggested: 'false',
// 	preactivetype: '-1',
// 	issuggestedid: '',

// 	//hide params
// 	hideDType: false,
// 	hideSType: true,
// 	hideAType: true,
// 	hideADType: true,
// 	addingNSS: false,
// 	addingTags: false,
// 	hideNS: false,

// 	documentTabIsPressed: false,
// 	socialTabIsPressed: false,
// 	automationTabIsPressed: false,

// 	searchdataStore: {
// 		mtype: 'store',
// 		fields: ['u_component_sys_id', 'u_component_parent_name', 'u_component_updated_by', 'rating', 'views', 'u_component_type_display', 'u_component_description', 'expanderDesc', {
// 			name: 'u_component_updated_on',
// 			type: 'date',
// 			format: 'time',
// 			dateFormat: 'time'
// 		}],
// 		remoteSort: true,
// 		sorters: [{
// 			property: 'u_component_parent_name',
// 			direction: 'ASC'
// 		}],
// 		proxy: {
// 			type: 'ajax',
// 			url: '/resolve/service/search/listdata',
// 			reader: {
// 				type: 'json',
// 				root: 'records'
// 			}
// 		}
// 	},

// 	mysearchStore: {
// 		mtype: 'store',
// 		fields: ['name', {
// 			name: 'value',
// 			type: 'auto'
// 		}],
// 		remoteSort: true,
// 		proxy: {
// 			type: 'ajax',
// 			url: '/resolve/service/search/getAutoCompletion',
// 			reader: {
// 				type: 'json',
// 				root: 'records'
// 			}
// 		}
// 	},

// 	columns: null,

// 	searchSelections: [],

// 	activate: function(screen, params) {
// 		window.document.title = this.localize('windowTitle');

// 		if (this.preactivetype == "0")
// 			this.activeTab = 0;
// 		else if (this.preactivetype == "1")
// 			this.activeTab = 1;
// 		else if (this.preactivetype == "2")
// 			this.activeTab = 2;

// 		if (params != null && params.search != null) {
// 			this.search = params.search;
// 			this.searchtag = params.searchtag;
// 		}

// 		this.loadRecs();

// 		this.presearch = this.search;
// 		this.set('presearch', this.search);
// 		this.set('activeTab', Number(this.activeTab));
// 		this.issuggested = 'false';
// 		this.search = '';
// 		this.preactivetype = this.activeTab;
// 		this.didyoumeanHidden = true;
// 		this.searchcombo = '';

// 		this.set('issuggested', 'false');
// 		this.set('search', '');
// 		this.set('preactivetype', this.activeTab);
// 		this.set('didyoumeanHidden', true);
// 		this.set('searchcombo', this.presearch);

// 	},
// 	allSelected: false,
// 	gridSelections: [],
// 	activeTab: 0,
// 	reset: false,
// 	searchHomeTab: function() {

// 		clientVM.handleNavigation({
// 			modelName: 'RS.search.SearchHome/preactivetype=' + this.activeTab
// 		})

// 		this.closePreview();
// 	},

// 	documentTab: function() {

// 		this.set('activeTab', 0);
// 		this.set('hideNS', false);
// 		this.set('preactivetype', 0);
// 		this.closePreview();
// 	},
// 	documentTabIsPressed$: function() {
// 		return this.activeTab == 0
// 	},

// 	socialTab: function() {
// 		this.set('activeTab', 1);
// 		this.set('hideNS', true);
// 		this.set('preactivetype', 1);
// 		this.closePreview();
// 	},
// 	socialTabIsPressed$: function() {
// 		return this.activeTab == 1
// 	},
// 	automationTab: function() {

// 		this.set('activeTab', 2);
// 		this.set('hideNS', false);
// 		this.set('preactivetype', 2);
// 		this.closePreview();
// 	},
// 	automationTabIsPressed$: function() {

// 		return this.activeTab == 2
// 	},

// 	resetData: function() {

// 		this.set('typecombo', []);
// 		this.set('typecomboS', []);
// 		this.set('typecomboA', []);

// 		this.set('incombo', []);
// 		this.set('timecombo', []);
// 		this.set('nscombo', []);
// 		this.set('tagcombo', []);

// 		this.issuggested = 'false';
// 		this.search = '';
// 		this.preactivetype = '-1';
// 		this.didyoumeanHidden = true;
// 		this.searchcombo = '';

// 		this.set('issuggested', 'false');
// 		this.set('search', '');
// 		this.set('preactivetype', '-1');
// 		this.set('didyoumeanHidden', true);
// 		this.set('searchcombo', '');
// 	},


// 	init: function() {

// 		//Called when mocking the backend to test the UI
// 		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)

// 		if (Ext.isString(this.activeTab)) this.set('activeTab', Number(this.activeTab))

// 		if (this.searchtag != null && this.searchtag != '') {
// 			this.tagcombo = [];
// 			this.tagcombo.push(this.searchtag);
// 			this.set('tagcombo', this.tagcombo);
// 			this.activeTab = 1;
// 			this.set('activeTab', 1);

// 		}

// 		this.searchdataStore.on('beforeload', function(store, operation, eOpts) {
// 			operation.params = operation.params || {};
// 			this.set('state', 'wait');
// 			Ext.apply(operation.params, {
// 				searchtypes: this.typecombo,
// 				searchtypesS: this.typecomboS,
// 				searchtypesA: this.typecomboA,
// 				searchin: this.incombo,
// 				searchtime: this.timecombo,
// 				searchnamespace: this.nscombo,
// 				searchtag: this.tagcombo,
// 				searchkey: this.searchcombo,
// 				search: this.search,
// 				activetype: this.activeTab,
// 				suggested: this.issuggested,
// 				issuggestedid: this.issuggestedid,
// 			});
// 		}, this);

// 		this.searchdataStore.on('load', function(recs, op, suc) {
// 			this.set('state', 'ready');
// 			if (!suc) {
// 				clientVM.displayError(this.localize('SearchErr'));
// 				return;
// 			}
// 		}, this);

// 		this.mysearchStore.on('beforeload', function(store, operation, eOpts) {
// 			operation.params = operation.params || {};

// 			Ext.apply(operation.params, {
// 				searchkey: this.searchcombo,
// 				activetype: this.activeTab,
// 			});
// 		}, this)

// 		this.selectedTags.on('lengthchanged', function() {
// 			var t = [];
// 			this.selectedTags.foreach(function(tag) {
// 				t.push(tag.id)
// 			})
// 			this.set('tagcombo', t)
// 		}, this)

// 		this.selectedNS.on('lengthchanged', function() {
// 			var t = [];
// 			this.selectedNS.foreach(function(namespace) {
// 				t.push(namespace.id)
// 			})
// 			this.set('nscombo', t)
// 		}, this)

// 		this.set('columns', [
// 			// {
// 			// 	header: 'ID',
// 			// 	dataIndex: 'u_component_sys_id',
// 			// 	hidden: true,
// 			// 	autoWidth: true,
// 			// 	sortable: false
// 			// },
// 			{
// 				header: 'Name',
// 				dataIndex: 'u_component_parent_name',
// 				filterable: true,
// 				sortable: false,
// 				flex: 1,
// 				renderer: function(value, record) {

// 					var fullname = record.record.raw.u_component_parent_name_original,
// 						namespace = '',
// 						docName = '',
// 						url = '',
// 						docType = record.record.raw.u_component_type,
// 						sys_ID = record.record.raw.u_component_sys_id;

// 					if (Ext.Array.indexOf(['WikiDocument', 'Runbook', 'Attachment', 'Decisiontree'], docType) > -1) {
// 						if (fullname.indexOf('.') != -1) {
// 							var index = fullname.indexOf('.');
// 							namespace = fullname.substring(0, index);
// 							docName = fullname.substring(index + 1, fullname.length);
// 						}

// 						url = Ext.String.format('/resolve/jsp/rsclient.jsp?#RS.wiki.Main/name={0}', namespace + '.' + docName)
// 					} else if (docType != null && docType == 'ActionTask') {
// 						url = Ext.String.format('/resolve/jsp/rsclient.jsp?#RS.actiontask.Main/id=', sys_ID)
// 					} else if (docType != null && docType == 'POST') {
// 						url = Ext.String.format('/resolve/jsp/rsclient.jsp#RS.social.Main/postId={0}', sys_ID)
// 					}

// 					return Ext.String.format('<a class="search-link" id="{0}" href="{1}" target="_blank" onClick="updateViewCount(this.id);">{2}</a>', sys_ID, url, value)
// 				}
// 			}, {
// 				header: 'User',
// 				dataIndex: 'u_component_updated_by',
// 				filterable: true,
// 				sortable: true,
// 				flex: 1

// 			}, {
// 				header: 'Rating',
// 				dataIndex: 'rating',
// 				filterable: true,
// 				sortable: true,
// 				width: 80
// 			}, {
// 				header: 'Views',
// 				dataIndex: 'views',
// 				filterable: true,
// 				sortable: true,
// 				width: 80
// 			}, {
// 				header: 'Type',
// 				dataIndex: 'u_component_type_display',
// 				filterable: true,
// 				sortable: false,
// 				flex: 1
// 			}, {
// 				header: 'Updated',
// 				dataIndex: 'u_component_updated_on',
// 				filterable: true,
// 				sortable: true,
// 				flex: 1,
// 				renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
// 			}
// 		]);

// 	},

// 	when_activeTab_changes_populate_store: {
// 		on: ['activeTabChanged'],
// 		action: function() {

// 			switch (this.activeTab) {
// 				case 0:

// 					this.inStore.removeAll();
// 					this.inStore.add({
// 						"name": "Title",
// 						"value": "Title"
// 					});
// 					this.inStore.add({
// 						"name": "Contents",
// 						"value": "Contents"
// 					});
// 					this.inStore.add({
// 						"name": "User",
// 						"value": "User"
// 					});

// 					this.set('hideDType', false);
// 					this.set('hideSType', true);
// 					this.set('hideAType', true);
// 					this.set('hideADType', true);

// 					this.searchtag = '';
// 					this.set('searchtag', '');
// 					this.set('tagcombo', []);

// 					break;
// 				case 1:

// 					this.inStore.removeAll();
// 					this.inStore.add({
// 						"name": "Title",
// 						"value": "Subject"
// 					});
// 					this.inStore.add({
// 						"name": "Contents",
// 						"value": "Contents"
// 					});
// 					this.inStore.add({
// 						"name": "User",
// 						"value": "User"
// 					});

// 					this.set('hideDType', true);
// 					this.set('hideSType', false);
// 					this.set('hideAType', true);
// 					this.set('hideADType', true);

// 					break;
// 				case 2:

// 					this.inStore.removeAll();
// 					this.inStore.add({
// 						"name": "Title",
// 						"value": "Title"
// 					});
// 					this.inStore.add({
// 						"name": "Contents",
// 						"value": "Contents"
// 					});
// 					this.inStore.add({
// 						"name": "User",
// 						"value": "User"
// 					});

// 					this.set('hideDType', true);
// 					this.set('hideSType', true);
// 					this.set('hideADType', true);
// 					this.set('hideAType', false);

// 					this.searchtag = '';
// 					this.set('searchtag', '');
// 					this.set('tagcombo', []);

// 					break;
// 				case 3:

// 					this.inStore.removeAll();
// 					this.inStore.add({
// 						"name": "Title",
// 						"value": "Title"
// 					});
// 					this.inStore.add({
// 						"name": "Contents",
// 						"value": "Contents"
// 					});
// 					this.inStore.add({
// 						"name": "User",
// 						"value": "User"
// 					});


// 					this.set('hideDType', true);
// 					this.set('hideSType', true);
// 					this.set('hideAType', true);
// 					this.set('hideADType', false);

// 					break;
// 				default:
// 					break;

// 			}

// 			this.loadRecs();

// 		}
// 	},


// 	findSuggestions: function() {

// 		this.ajax({
// 			url: '/resolve/service/search/getSuggestions',
// 			params: {
// 				key: this.searchcombo,
// 				urlkey: this.search,
// 				activetype: this.activeTab,
// 				suggested: this.issuggested,
// 			},
// 			scope: this,
// 			success: function(r) {
// 				var response = RS.common.parsePayload(r)
// 				if (response.success) {

// 					if (response != null && response.records != null && response.records.length == 1) {
// 						var suggestionArray = response.records;
// 						var sug = suggestionArray[0];

// 						var didYouMean = sug.DIDYOUMEAN;
// 						var sugString = sug.SUGGESTION;
// 						var suggestiononly = sug.SUGGESTIONONLY;

// 						if (didYouMean != null) {
// 							if (didYouMean == 'true' && suggestiononly != '') // && suggestiononly != this.presearch)
// 							{
// 								this.didyoumeanHidden = false;
// 								this.didyoumean = sugString;

// 								this.set('didyoumeanHidden', false);
// 								this.set('didyoumean', sugString);

// 								this.search = '';
// 								this.set('search', '');
// 								this.issuggested = 'false';
// 								this.set('issuggested', 'false');

// 							} else {
// 								this.set('didyoumeanHidden', true);
// 							}
// 						}
// 					}
// 				} else {
// 					clientVM.displayError(response.message)
// 				}
// 			}
// 		});
// 	},

// 	loadRecs: function() {

// 		var testdata = this.searchcombo;
// 		this.searchdataStore.sorters.clear();

// 		this.set('state', 'wait');
// 		this.searchdataStore.load();

// 	},


// 	loadQuery: function() {

// 		if (this.searchcombo != null) {
// 			this.set('state', 'wait');
// 			this.mysearchStore.load({
// 				scope: this,
// 				callback: function(recs, op, suc) {

// 					if (!suc) {
// 						clientVM.displayError(this.localize('Search'));
// 						return;
// 					}

// 					this.set('state', 'ready');
// 				}
// 			});
// 		}
// 	},


// 	handleAjaxErr: function(err) {
// 		var title = null;
// 		var msg = null;
// 		if (err.status) {
// 			title = 'ServerErrTitle';
// 			msg = 'ServerErrMsg';
// 		} else {
// 			title = err.title;
// 			msg = err.msg;
// 		}

// 		this.message({
// 			title: this.localize(title),
// 			msg: this.localize(msg),
// 			button: Ext.MessageBox.YES,
// 			buttonText: {
// 				yes: this.localize('confirmErr')
// 			}
// 		});
// 	},


// 	selectAllAcrossPages: function(selectAll) {
// 		this.set('allSelected', selectAll);

// 	},
// 	selectionChanged: function() {

// 	},


// 	closePreview: function() {
// 		this.eastHidden = true;
// 		this.eastCollpased = true;
// 		this.set('eastHidden', true);
// 		this.set('eastCollpased', true);
// 	},


// 	eastCollpased: true,

// 	activeItem: 0,


// 	tag: function() {
// 		this.set('creatingWorksheet', true)
// 		this.ajax({
// 			url: '/resolve/service/worksheet/newWorksheet',
// 			method: 'POST',
// 			success: function(r) {
// 				var response = RS.common.parsePayload(r)
// 				if (response.success)
// 					clientVM.handleNavigation({
// 						modelName: 'RS.worksheet.Worksheet',
// 						params: {
// 							id: response.data
// 						}
// 					})
// 				else clientVM.displayError(response.message)
// 			},
// 			failure: function(r) {
// 				this.set('creatingWorksheet', false)
// 				clientVM.displayFailure(r)
// 			}
// 		})
// 	},


// 	typeStore: {
// 		mtype: 'store',
// 		fields: ['name', {
// 			name: 'value',
// 			type: 'auto'
// 		}],
// 		data: [{
// 			"name": "Document",
// 			"value": "Document"
// 		}, {
// 			"name": "DecisionTree",
// 			"value": "DecisionTree"
// 		}, {
// 			"name": "Attachment",
// 			"value": "Attachment"
// 		}, ],
// 		proxy: {
// 			type: 'memory',
// 			reader: {
// 				type: 'json'
// 			}
// 		}
// 	},

// 	typeStoreS: {
// 		mtype: 'store',
// 		fields: ['name', {
// 			name: 'value',
// 			type: 'auto'
// 		}],
// 		data: [{
// 			"name": "Post",
// 			"value": "POST"
// 		}, {
// 			"name": "Notification",
// 			"value": "NOTIFICATION"
// 		}, ],
// 		proxy: {
// 			type: 'memory',
// 			reader: {
// 				type: 'json'
// 			}
// 		}
// 	},

// 	typeStoreA: {
// 		mtype: 'store',
// 		fields: ['name', {
// 			name: 'value',
// 			type: 'auto'
// 		}],
// 		data: [{
// 			"name": "Runbook",
// 			"value": "Runbook"
// 		}, {
// 			"name": "ActionTask",
// 			"value": "ActionTask"
// 		}, ],
// 		proxy: {
// 			type: 'memory',
// 			reader: {
// 				type: 'json'
// 			}
// 		}
// 	},


// 	inStore: {
// 		mtype: 'store',
// 		fields: ['name', {
// 			name: 'value',
// 			type: 'auto'
// 		}],
// 		data: [{
// 			"name": "Title",
// 			"value": "Title"
// 		}, {
// 			"name": "Contents",
// 			"value": "Contents"
// 		}, {
// 			"name": "User",
// 			"value": "User"
// 		}, ],
// 		proxy: {
// 			type: 'memory',
// 			reader: {
// 				type: 'json'
// 			}
// 		}
// 	},

// 	timeStore: {
// 		mtype: 'store',
// 		fields: ['name', {
// 			name: 'value',
// 			type: 'auto'
// 		}],
// 		data: [{
// 			"name": "Today",
// 			"value": "Today"
// 		}, {
// 			"name": "Week",
// 			"value": "Week"
// 		}, {
// 			"name": "Month",
// 			"value": "Month"
// 		}, {
// 			"name": "Older",
// 			"value": "Older"
// 		}, ],
// 		proxy: {
// 			type: 'memory',
// 			reader: {
// 				type: 'json'
// 			}
// 		}
// 	},

// 	selectedTags: {
// 		mtype: 'list'
// 	},

// 	selectedNS: {
// 		mtype: 'list'
// 	},

// 	namespaceStore: {
// 		mtype: 'store',
// 		fields: ['name'],
// 		mixins: [{
// 			type: 'liststoreadapter',
// 			attachTo: 'selectedNS'
// 		}],
// 		data: [{
// 			"name": "Add Namespace",
// 			"id": "Add Namespace"
// 		}],
// 		proxy: {
// 			type: 'memory',
// 			reader: {
// 				type: 'json'
// 			}
// 		}
// 	},

// 	tagStore: {
// 		mtype: 'store',
// 		fields: ['name'],
// 		mixins: [{
// 			type: 'liststoreadapter',
// 			attachTo: 'selectedTags'
// 		}],
// 		data: [{
// 			"name": "Add Tag",
// 			"id": "Add Tag"
// 		}],
// 		proxy: {
// 			type: 'memory',
// 			reader: {
// 				type: 'json'
// 			}
// 		}
// 	},



// 	searchkeys: {
// 		mtype: 'list'
// 	},


// 	searchStore: {
// 		mtype: 'store',
// 		fields: ['name', 'value'],
// 		mixins: [{
// 			type: 'liststoreadapter',
// 			attachTo: 'searchkeys'
// 		}],
// 		proxy: {
// 			type: 'memory',
// 			reader: {
// 				type: 'json'
// 			}
// 		}
// 	},


// 	singleRecordSelected$: function() {
// 		return this.gridSelections.length == 1
// 	},


// 	when_typecombo_changes_update_name: {
// 		on: ['typecomboChanged'],
// 		action: function() {
// 			if (this.typecombo.length == 0) {
// 				this.typecombo = '';
// 				this.set('typecombo', []);
// 			}
// 			this.loadRecs();
// 		}
// 	},

// 	when_typecomboA_changes_update_name: {
// 		on: ['typecomboAChanged'],
// 		action: function() {
// 			if (this.typecomboA.length == 0) {
// 				this.typecomboA = '';
// 				this.set('typecomboA', []);
// 			}
// 			this.loadRecs();
// 		}
// 	},


// 	when_typecomboS_changes_update_name: {
// 		on: ['typecomboSChanged'],
// 		action: function() {

// 			if (this.typecomboS.length == 0) {
// 				this.typecomboS = '';
// 				this.set('typecomboS', []);
// 			}

// 			this.loadRecs();
// 		}
// 	},

// 	when_incombo_changes_update_name: {
// 		on: ['incomboChanged'],
// 		action: function() {

// 			if (this.incombo.length == 0) {
// 				this.incombo = '';
// 				this.set('incombo', []);
// 			}

// 			this.loadRecs();
// 		}
// 	},


// 	when_timecombo_changes_update_name: {
// 		on: ['timecomboChanged'],
// 		action: function() {

// 			if (this.timecombo.length == 0) {
// 				this.timecombo = '';
// 				this.set('timecombo', []);
// 			}

// 			this.loadRecs();
// 		}
// 	},

// 	when_nscombo_changes_update_name: {
// 		on: ['nscomboChanged'],
// 		action: function() {

// 			var selectedNS = this.nscombo;

// 			if (this.nscombo.length == 0) {
// 				this.nscombo = '';
// 				this.set('nscombo', []);
// 				this.loadRecs();
// 			} else {
// 				if (selectedNS != '' && (Ext.Array.indexOf(selectedNS, 'Add Namespace') != -1)) {
// 					for (var i = selectedNS.length - 1; i >= 0; i--) {
// 						if (selectedNS[i] === 'Add Namespace') {
// 							selectedNS.splice(i, 1);
// 						}
// 					}

// 					this.nscombo = this.selectedNS;
// 					this.set('nscombo', this.selectedNS);

// 					this.addingNSS = true;
// 					this.addMoreNamespaces();

// 				} else if (!this.addingNSS) {
// 					this.loadRecs();
// 				}

// 			}
// 		}
// 	},

// 	addMoreNamespaces: function() {

// 		this.open({
// 			mtype: 'RS.search.SearchNameSpacePicker',
// 			callback: 'addNamespace'
// 		});

// 	},

// 	addNamespace: function(newNamespaces) {
// 		var nsName = '',
// 			nsId = ''
// 		Ext.Array.forEach(newNamespaces, function(namespace) {
// 			nsName = namespace.data.name
// 			nsId = namespace.data.id || namespace.data.name

// 			var existed = false;
// 			this.selectedNS.foreach(function(namespace) {

// 				if (namespace.id == nsId) {
// 					existed = true;
// 					return false;
// 				}
// 			})

// 			if (!existed) {
// 				this.selectedNS.add({
// 					name: nsName,
// 					id: nsId
// 				})
// 			}
// 		}, this)

// 		this.addingNSS = false;
// 		this.set('nscombo', []);
// 	},


// 	when_tagcombo_changes_update_name: {
// 		on: ['tagcomboChanged'],
// 		action: function() {

// 			var selectedtag = this.tagcombo;

// 			if (this.tagcombo.length == 0) {
// 				this.tagcombo = '';
// 				this.set('tagcombo', []);
// 				this.loadRecs();
// 			} else {
// 				if (selectedtag != '' && (Ext.Array.indexOf(selectedtag, 'Add Tag') != -1)) {
// 					this.addingTags = true;
// 					this.addMoreTages();
// 				} else if (!this.addingTags) {
// 					this.loadRecs();
// 				}
// 			}
// 		}
// 	},


// 	addMoreTages: function() {

// 		this.open({
// 			mtype: 'RS.search.SearchTagPicker',
// 			callback: 'addTags'
// 		});

// 	},

// 	addTags: function(newTags) {

// 		var testd = this.search;

// 		var tagName = '',
// 			tagId = ''
// 		Ext.Array.forEach(newTags, function(tag) {
// 			tagName = tag.data.name
// 			tagId = tag.data.id || tag.data.name

// 			var existed = false;
// 			this.selectedTags.foreach(function(tag) {

// 				if (tag.id == tagId) {
// 					existed = true;
// 					return false;
// 				}
// 			})

// 			if (!existed) {
// 				this.selectedTags.add({
// 					name: tagName,
// 					id: tagId
// 				})
// 			}
// 		}, this)

// 		this.addingTags = false;
// 		this.set('tagcombo', []);
// 	},

// 	when_searchcombo_changes_update_name: {
// 		on: ['searchcomboChanged'],
// 		action: function() {

// 			this.didyoumeanHidden = false;
// 			this.set('didyoumeanHidden', false);

// 			this.loadRecs();
// 			this.loadQuery();
// 			this.findSuggestions();
// 		}
// 	},


// 	searchButtonTriggered: function() {

// 		this.didyoumeanHidden = false;
// 		this.set('didyoumeanHidden', false);

// 		if (this.searchcombo == null || this.searchcombo == '') {
// 			this.searchHomeTab();
// 		} else {
// 			this.loadRecs();
// 			this.findSuggestions();
// 		}
// 	},

// 	reallyDeleteRecords: function(button) {
// 		if (button === 'yes') {
// 			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
// 			if (this.allSelected) {
// 				this.ajax({
// 					url: '/resolve/service/worksheet/delete',
// 					params: {
// 						ids: '',
// 						whereClause: this.worksheets.lastWhereClause || '1=1'
// 					},
// 					scope: this,
// 					success: function(r) {
// 						var response = RS.common.parsePayload(r)
// 						if (response.success) this.worksheets.load()
// 						else clientVM.displayError(response.message)
// 					}
// 				})
// 			} else {
// 				var ids = []
// 				Ext.each(this.gridSelections, function(selection) {
// 					ids.push(selection.get('id'))
// 				})
// 				this.ajax({
// 					url: '/resolve/service/worksheet/delete',
// 					params: {
// 						ids: ids.join(',')
// 					},
// 					scope: this,
// 					success: function(r) {
// 						var response = RS.common.parsePayload(r)
// 						if (response.success) this.worksheets.load()
// 						else clientVM.displayError(response.message)
// 					}
// 				})
// 			}
// 		}
// 	},

// 	setActiveIsEnabled$: function() {
// 		return true; //this.singleRecordSelected
// 	}
// })