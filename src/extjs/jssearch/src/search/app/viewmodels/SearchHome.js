glu.defModel('RS.search.SearchHome', {
	API : {
		newWS : '/resolve/service/worksheet/newWorksheet',
		deleteWS : '/resolve/service/worksheet/delete',
		setActive : '/resolve/service/worksheet/setActive'
	},
	mock: false,
	state: '',
	stateId: 'searchdataList',
	searchcombo: '',
	displayName: 'Search Home',
	preactivetype: '0',

	searchdata: {
		mtype: 'store',
		fields: ['query', 'queryCount', 'u_component_type'],
		remoteSort: false,
		sorters: [{
			property: 'queryCount',
			direction: 'DESC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/getTopViewedResults',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	searchdataofme: {
		mtype: 'store',
		fields: ['query', {
			name: 'u_component_updated_on',
			type: 'date', //'types.VELATLONG',
			format: 'time',
			dateFormat: 'time'
		}],
		remoteSort: false,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/getQueryofme',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	searchdataofall: {
		mtype: 'store',
		fields: ['query', 'queryCount'],
		remoteSort: false,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/getQueryofall',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	columns: null,
	columnsofme: null,
	columnsofall: null,

	searchSelections: [],
	activate: function(screen, params) {
		window.document.title = this.localize('windowTitle');
		this.loadRecs();

		this.set('activeTab', Number(0)) //Number(params.activeTab))
	},


	myRecentSearch: 'My Recent Search',


	allSelected: false,
	gridSelections: [],

	activeTab: 0,

	searchTab: function() {

		this.loadSearchPage();
		//this.searchcombo = '';
		//this.set('searchcombo', '');
	},
	searchTabIsPressed$: function() {

		return; //this.activeTab == 0
	},

	loadSearchPage: function() {

		clientVM.handleNavigation({
			modelName: 'RS.search.Search/search=' + this.searchcombo + '&preactivetype=' + this.preactivetype
		})
	},

	searchButtonTriggered: function() {

		this.loadSearchPage();
		//this.searchcombo = '';
		//	this.set('searchcombo', '');
	},

	documentTab: function() {
		this.set('activeTab', 0)
	},
	documentTabIsPressed$: function() {
		return this.activeTab == 0
	},
	socialTab: function() {
		this.set('activeTab', 1)
	},
	socialTabIsPressed$: function() {
		return this.activeTab == 1
	},
	automationTab: function() {
		this.set('activeTab', 2)
	},
	automationTabIsPressed$: function() {
		return this.activeTab == 2
	},


	init: function() {

		//Called when mocking the backend to test the UI
		if (this.mock) this.backend = RS.worksheet.createMockBackend(true)

		if (Ext.isString(this.activeTab)) this.set('activeTab', Number(this.activeTab))

		this.set('searchcombo', this.searchcombo)

		this.set('columns', [{
				header: 'Top Viewed Results',
				dataIndex: 'query',
				filterable: true,
				sortable: true,
				flex: 1,
				renderer: function(value, record) {

					var fullname = record.record.raw.query;
					var namespace = '';
					var docName = '';
					var url = '';
					var docType = record.record.raw.u_component_type;
					var sys_ID = record.record.raw.u_component_sys_id;

					if (docType != null && docType == 'WikiDocument' || docType == 'Runbook' || docType == 'Attachment' || docType == 'Decisiontree') {
						if (fullname.indexOf(".") != -1) {
							var index = fullname.indexOf(".");
							namespace = fullname.substring(0, index);
							docName = fullname.substring(index + 1, fullname.length);
						}

						url += "/resolve/jsp/rsclient.jsp?#RS.client.URLScreen/location=" + "%2Fresolve%2Fjsp%2Frswiki.jsp%3Fwiki%3D" + namespace + "." + docName;
					} else if (docType != null && docType == 'ActionTask') {
						//var sys_ID = record.record.raw.u_component_sys_id;
						url += "/resolve/jsp/rsclient.jsp?#RS.client.URLScreen/location=" + "%2Fresolve%2Fjsp%2Frsactiontask.jsp%3Fmain%3Dactiontask%26type%3Dform%26sys_id%3D" + sys_ID;

					} else if (docType != null && docType == 'POST') {
						//var sys_ID = record.record.raw.u_component_sys_id;
						url += '/resolve/jsp/rsclient.jsp#RS.social.Main/postId=/' + sys_ID;
					}

					return '<a style="text-decoration: none !important;" href="' + url + '" target="_blank"><font color=#385F95>' + value + '</font></a>';
				}
			}, {
				header: 'Search View Count',
				dataIndex: 'queryCount',
				filterable: true,
				sortable: true,
				width: 80
			}, {
				header: 'Type',
				dataIndex: 'u_component_type',
				filterable: true,
				sortable: true,
				flex: 1
			}

		]);

		this.set('columnsofme', [{
				header: 'Search Query',
				dataIndex: 'query',
				filterable: true,
				sortable: true,
				flex: 1,
				renderer: function(value, record) {
					return '<font color=#385F95>' + value + '</font>';
				}
			}, {
				header: 'Searched On',
				dataIndex: 'u_component_updated_on',
				filterable: true,
				sortable: true,
				flex: 1
			}

		]);

		this.set('columnsofall', [{
				header: 'Search Query',
				dataIndex: 'query',
				filterable: true,
				sortable: true,
				flex: 1
			}, {
				header: 'Approx Count',
				dataIndex: 'queryCount',
				filterable: true,
				sortable: true,
				flex: 1
			}

		]);

	},

	when_activeTab_changes_populate_store: {
		on: ['activeTabChanged'],
		action: function() {

			switch (this.activeTab) {
				case 0:
					this.typeStore.removeAll();
					this.typeStore.add({
						"name": "Document",
						"value": "Document"
					});
					this.typeStore.add({
						"name": "DecisionTree",
						"value": "DecisionTree"
					});
					this.typeStore.add({
						"name": "Attachment",
						"value": "Attachment"
					});
					break;
				case 1:
					this.typeStore.removeAll();
					this.typeStore.add({
						"name": "Post",
						"value": "Post"
					});
					this.typeStore.add({
						"name": "Notification",
						"value": "Notification"
					});
					break;
				case 2:
					this.typeStore.removeAll();
					this.typeStore.add({
						"name": "Runbook",
						"value": "Runbook"
					});
					this.typeStore.add({
						"name": "Actiontask",
						"value": "Actiontask"
					});
					break;
				default:
					break;

			}

		}
	},

	opensearch: function(record, item, index, e, eOpts) {
		var keyLocal = record.raw.query;
		clientVM.handleNavigation({
			modelName: 'RS.search.Search/urlkeyword=' + keyLocal,
			target: '_self'
		})
		this.searchcombo = '';
		this.set('searchcombo', '');
	},

	loadRecs: function() {
		this.set('state', 'wait');

		this.searchdataofme.load({
			scope: this,
			callback: function(recs, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('Search'));
					return;
				}

				this.set('state', 'ready');
			}
		});

		this.searchdataofall.load({
			scope: this,
			callback: function(recs, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('Search'));
					return;
				}

				this.set('state', 'ready');
			}
		});

		this.searchdata.load({
			scope: this,
			callback: function(recs, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('Search'));
					return;
				}

				this.set('state', 'ready');
			}
		});
	},

	loadRecsofme: function() {
		this.set('state', 'wait');

		this.searchdataofme.load({
			scope: this,
			callback: function(recs, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('Search'));
					return;
				}

				this.set('state', 'ready');
			}
		});
	},

	loadRecsofall: function() {
		this.set('state', 'wait');

		this.searchdataofall.load({
			scope: this,
			callback: function(recs, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('Search'));
					return;
				}

				this.set('state', 'ready');
			}
		});
	},

	handleAjaxErr: function(err) {
		var title = null;
		var msg = null;
		if (err.status) {
			title = 'ServerErrTitle';
			msg = 'ServerErrMsg';
		} else {
			title = err.title;
			msg = err.msg;
		}

		this.message({
			title: this.localize(title),
			msg: this.localize(msg),
			button: Ext.MessageBox.YES,
			buttonText: {
				yes: this.localize('confirmErr')
			}
		});
	},

	when_searchcombo_changes_update_name: {
		on: ['searchcomboChanged'],
		action: function() {
			this.loadSearchPage();
			//this.searchcombo = '';
			//this.set('searchcombo', '');
		}
	},

	resetData: function() {
		this.typeStore.removeAll();

	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	namespace: function() {
		this.set('creatingWorksheet', true)
		this.ajax({
			url: this.API['newWS'],
			method: 'POST',
			scope: this,
			params : {
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success)
					clientVM.handleNavigation({
						modelName: 'RS.worksheet.Worksheet',
						params: {
							id: response.data
						}
					})
				else clientVM.displayError(response.message)
			},
			failure: function(r) {
				this.set('creatingWorksheet', false)
				clientVM.displayFailure(r);
			}
		})
	},

	tag: function() {
		this.set('creatingWorksheet', true)
		this.ajax({
			url: this.API['newWS'],
			method: 'POST',
			scope: this,
			params : {
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success)
					clientVM.handleNavigation({
						modelName: 'RS.worksheet.Worksheet',
						params: {
							id: response.data
						}
					})
				else clientVM.displayError(response.message)
			},
			failure: function(r) {
				this.set('creatingWorksheet', false)
				clientVM.displayFailure(r);
			}
		})
	},

	creatingWorksheet: false,
	searchType: function() {
		this.set('creatingWorksheet', true)

		this.ajax({
			url: this.API['newWS'],
			method: 'POST',
			scope: this,
			params : {
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			},	
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success)
					clientVM.handleNavigation({
						modelName: 'RS.worksheet.Worksheet',
						params: {
							id: response.data
						}
					})
				else clientVM.displayError(response.message)
			},
			failure: function(r) {
				this.set('creatingWorksheet', false)
				clientVM.displayFailure(r);
			}
		})
	},

	searchTypeIsEnabled$: function() {
		return !this.creatingWorksheet
	},


	typeStore: {
		mtype: 'store',
		fields: ['name', {
			name: 'value',
			type: 'auto'
		}],
		data: [{
			"name": "Document",
			"value": "Document"
		}, {
			"name": "DecisionTree",
			"value": "DecisionTree"
		}, {
			"name": "Attachment",
			"value": "Attachment"
		}, ],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	inStore: {
		mtype: 'store',
		fields: ['name', {
			name: 'value',
			type: 'auto'
		}],
		data: [{
			"name": "Title",
			"value": "Title"
		}, {
			"name": "Contents",
			"value": "Contents"
		}, {
			"name": "User",
			"value": "User"
		}, ],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	timeStore: {
		mtype: 'store',
		fields: ['name', {
			name: 'value',
			type: 'auto'
		}],
		data: [{
			"name": "Today",
			"value": "Today"
		}, {
			"name": "Week",
			"value": "Week"
		}, {
			"name": "Month",
			"value": "Month"
		}, {
			"name": "Older",
			"value": "Older"
		}, ],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	namespaceStore: {
		mtype: 'store',
		fields: ['name', {
			name: 'value',
			type: 'auto'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/listNamespaces',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	tagStore: {
		mtype: 'store',
		fields: ['name', {
			name: 'value',
			type: 'auto'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/listTags',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},


	createCatalog: function() {
		clientVM.handleNavigation({
			modelName: 'RS.catalog.Catalog'
		})
	},
	createTrainingCatalog: function() {
		clientVM.handleNavigation({
			modelName: 'RS.catalog.Catalog',
			params: {
				catalogType: 'training'
			}
		})
	},
	editWorksheet: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.worksheet.Worksheet',
			params: {
				id: id,
				activeTab: 1
			}
		})
	},
	singleRecordSelected$: function() {
		return this.gridSelections.length == 1
	},
	editWorksheetIsEnabled$: function() {
		return this.singleRecordSelected
	},
	deleteWorksheetIsEnabled$: function() {
		return this.gridSelections.length > 0 || this.allSelected
	},
	searchTime: function() {
		//Delete the selected record(s), but first confirm the action
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.gridSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.gridSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			if (this.allSelected) {
				this.ajax({
					url: this.API['deleteWS'],
					params: {
						ids: '',
						whereClause: this.worksheets.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.worksheets.load()
						else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			} else {
				var ids = []
				Ext.each(this.gridSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: this.API['deleteWS'],
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.worksheets.load()
						else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}
	},

	setActiveIsEnabled$: function() {
		return this.singleRecordSelected
	},
	searchIn: function() {
		this.message({
			title: this.localize('setActiveTitle'),
			msg: this.localize('setActiveMessage', this.gridSelections[0].get('number')),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('setActiveAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallySetActive
		})
	},
	reallySetActive: function(button) {
		if (button == 'yes') {
			this.ajax({
				url: this.API['setActive'],
				params: {
					worksheetId: this.gridSelections[0].get('id'),
					'RESOLVE.ORG_NAME' : clientVM.orgName,
					'RESOLVE.ORG_ID' : clientVM.orgId
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) this.worksheets.load()
					else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	}
})