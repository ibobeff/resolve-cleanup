glu.defModel('RS.search.Main', {
	mixins: ['pageable'],
	activeItem: 0,
	homeText: '',
	docExpanded: false,
	autoExpanded: false,
	displaySocial: true,
	displayAutomation: true,
	reqCount: 0,

	hideSocial: function() {
		this.set('displaySocial', false);
	},

	hideAutomation: function() {
		this.set('displayAutomation', false);
	},

	expanded$: function() {
		var isDocExpanded = this.activeItem === 0 && this.docExpanded;
		var isAutoExpanded = this.activeItem === 2 && this.autoExpanded;
		return isDocExpanded || isAutoExpanded;
	},

	toggleDetail: function(expand) {
		this[expand ? 'expand' : 'collapse']();
	},

	expand: function() {
		this.set(this.activeItem === 0 ? 'docExpanded' : 'autoExpanded', true);
	},

	collapse: function() {
		this.set(this.activeItem === 0 ? 'docExpanded' : 'autoExpanded', false);
	},

	showPreview$: function() {
		return this.activeItem === 0;
	},

	previewCollapsed: true,
	displayPostProfilePicutres: true,
	displayCommentProfilePictures: true,
	wait: false,

	documentTabIsPressed$: function() {
		return this.activeItem === 0;
	},

	socialTabIsPressed$: function() {
		return this.activeItem === 1;
	},

	automationTabIsPressed$: function() {
		return this.activeItem === 2;
	},

	documentKeywordIsEnabled$: function() {
		return this.triggerSearchIsEnabled;
	},

	socialKeywordIsEnabled$: function() {
		return this.triggerSearchIsEnabled;
	},

	automationKeywordIsEnabled$: function() {
		return this.triggerSearchIsEnabled;
	},

	triggerSearch: function() {
		if (this.triggerSearchIsEnabled) {
			this.searchNow();
		}
	},

	triggerSearchIsEnabled$: function() {
		return this.reqCount === 0;
	},

	documentTab: function() {
		this.set('activeItem', 0);
		this.resetPageInfo();
		this.documentStore.load();
	},

	socialTab: function() {
		this.set('activeItem', 1);
		this.resetPageInfo();
		this.postStore.load();
	},

	socialTabIsVisible$: function() {
		return this.displaySocial;
	},

	automationTab: function() {
		this.set('activeItem', 2);
		this.resetPageInfo();
		this.automationStore.load();
	},

	automationTabIsVisible$: function() {
		return hasPermission(['resolve_dev', 'resolve_process']) && this.displayAutomation;
	},

	homeWindow: null,

	gotoSearchHomeIsPressed$: function() {
		return this.homeWindow;
	},

	documentStore: {
		mtype: 'store',
		fields: [
			'name',
			'fullName',
			'namespace',
			'title',
			'summary',
			'content',
			'processModel',
			'abortModel',
			'finalModel',
			'decisionTree',

			'locked',
			'deleted',
			'hidden',
			'active',
			'type'
		].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/searchWikis',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	documentsColumns: [{
		text: '~~name~~',
		dataIndex: 'name'
	}, {
		text: '~~fullName~~',
		dataIndex: 'fullName',
		flex: 1
	}, {
		text: '~~namespace~~',
		dataIndex: 'namespace'
	}, {
		text: '~~title~~',
		dataIndex: 'title'
	}, {
		text: '~~summary~~',
		dataIndex: 'summary'
	}, {
		text: '~~content~~',
		dataIndex: 'content'
	}, {
		text: '~~processModel~~',
		dataIndex: 'processModel'
	}, {
		text: '~~abortModel~~',
		dataIndex: 'abortModel'
	}, {
		text: '~~finalModel~~',
		dataIndex: 'finalModel'
	}, {
		text: '~~decisionTree~~',
		dataIndex: 'decisionTree'
	}, {
		text: '~~locked~~',
		dataIndex: 'locked',
		renderer: RS.common.grid.booleanRenderer(),
		width: 50
	}, {
		text: '~~deleted~~',
		dataIndex: 'deleted',
		renderer: RS.common.grid.booleanRenderer(),
		width: 50
	}, {
		text: '~~hidden~~',
		dataIndex: 'hidden',
		renderer: RS.common.grid.booleanRenderer(),
		width: 50
	}, {
		text: '~~active~~',
		dataIndex: 'active',
		renderer: RS.common.grid.booleanRenderer(),
		width: 50
	}],

	documentKeyword: '',

	documentKeywordIsVisible$: function() {
		return this.activeItem === 0;
	},

	documentSuggestions: {
		mtype: 'store',
		fields: ['suggestion'],
		pageSize: 5,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/getDocumentSuggestions',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	documents: {
		mtype: 'list'
	},

	socialKeyword: '',

	socialKeywordIsVisible$: function() {
		return this.activeItem === 1;
	},

	socialSuggestions: {
		mtype: 'store',
		fields: ['suggestion'],
		pageSize: 5,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/getSocialSuggestions',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	automationKeyword: '',

	automationKeywordIsVisible$: function() {
		return this.activeItem === 2;
	},

	automationSuggestions: {
		mtype: 'store',
		fields: ['suggestion'],
		pageSize: 5,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/getAutomationSuggestions',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('windowTitle'));

		if (params) {
			if (params.search) {
				this.set('documentKeyword', params.search);
				this.set('socialKeyword', params.search);
				this.set('automationKeyword', params.search);
			}

			if (params.activeItem) {
				try {
					this.set('activeItem', parseInt(params.activeItem));
				} catch (e) {
					this.set('activeItem', 0);
				}
			}

			this.searchNow(params);
		}
	},

	clearFilter: function() {
		this.set('documentKeyword', '');
		this.set('socialKeyword', '');
		this.set('automationKeyword', '');
		this.searchNow({}, []);
	},

	refresh: function() {
		this.searchNow({}, this.filterItems);
	},

	searchNow: function(params, filter) {
		var keyword = '';

		switch (this.activeItem) {
			case 0:
				keyword = this.documentKeyword;
				this.generateQuery(Ext.apply({
					searchTerm: keyword
				}, params), filter);
				this.documentStore.load();
				break;
			case 1:
				keyword = this.socialKeyword;
				this.generateQuery(Ext.apply({
					searchTerm: keyword
				}, params), filter);
				this.postStore.load();
				break;
			case 2:
				keyword = this.automationKeyword;
				this.generateQuery(Ext.apply({
					searchTerm: keyword
				}, params), filter);
				this.automationStore.load();
				break;
		}

		this.set('documentKeyword', keyword);
		this.set('socialKeyword', keyword);
		this.set('automationKeyword', keyword);
	},

	generateQuery: function(params, extraFilters) {
		var filters = [];
		filters.push({
			field: '',
			type: 'auto',
			condition: 'contains',
			value: params.searchTerm
		});

		if (params && params.tag) {
			filters.push({
				field: 'tag',
				type: 'auto',
				condition: 'equals',
				value: Ext.isArray
				(params.tag) ? params.tag.join(',') : params.tag
			});
		}

		if (extraFilters) {
			filters = extraFilters;
		}

		this.set('filterItems', filters);
		this.set('query', { filter: Ext.encode(filters) });
	},

	query: null,
	filterItems: [],

	advanceSearch: function(target) {
		var k = this.automationKeyword;

		if(this.activeItem === 0) {
			k = this.documentKeyword;
		} else if(this.activeItem === 1) {
			k = this.socialKeyword;
		}

		this.open({
			mtype: 'RS.search.AdvanceSearchWindow',
			activeItem: this.activeItem,
			target: target.getEl().id,
			keyword: k
		});
	},

	init: function() {
		if (this.mock) {
			this.backend = RS.search.createMockBackend(true);
		}

		this.set('homeText', this.localize('gotoSearchHome'));
		this.documentSuggestions.on('beforeload', function(store, operation) {
			this.set('reqCount', this.reqCount + 1);
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				searchTerm: this.documentKeyword
			});
			operation.params.query = {};
		}, this);

		this.socialSuggestions.on('beforeload', function(store, operation) {
			this.set('reqCount', this.reqCount + 1);
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				searchTerm: this.socialKeyword
			});
			operation.params.query = {};
		}, this);

		this.automationSuggestions.on('beforeload', function(store, operation) {
			this.set('reqCount', this.reqCount + 1);
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				searchTerm: this.automationKeyword
			});
			operation.params.query = {};
		}, this);

		this.automationStore.on('beforeload', function(store, operation) {
			this.set('reqCount', this.reqCount + 1);
			operation.params = operation.params || {};
			var nextPageReqParam = this.nextPageReqParam();
			nextPageReqParam.problemId = clientVM.problemId;
			Ext.apply(operation.params, (this.query ? this.query : {}), nextPageReqParam);
			this.set('selectedAutomation', null);
		}, this)

		this.documentStore.on('beforeload', function(store, operation) {
			this.set('reqCount', this.reqCount + 1);
			operation.params = operation.params || {};
			var nextPageReqParam = this.nextPageReqParam();
			nextPageReqParam.problemId = clientVM.problemId;
			Ext.apply(operation.params, (this.query ? this.query : {}), nextPageReqParam);
			this.set('selectedDocument', null);
			this.fireEvent('resetFocus');
		}, this);

		this.postStore.on('beforeload', function(store, operation) {
			this.set('reqCount', this.reqCount + 1);
			operation.params = operation.params || {};
			var nextPageReqParam = this.nextPageReqParam();
			nextPageReqParam.streamId = 'all';
			nextPageReqParam.unreadOnly = false;
			nextPageReqParam.problemId = clientVM.problemId;
			Ext.apply(operation.params, (this.query ? this.query : {}), nextPageReqParam);
		}, this)

		this.documentSuggestions.on('load', function() {
			this.set('reqCount', this.reqCount - 1);
			this.fireEvent('resetFocus');
		}, this);

		this.socialSuggestions.on('load', function() {
			this.set('reqCount', this.reqCount - 1);
			this.fireEvent('resetFocus');
		}, this);

		this.automationSuggestions.on('load', function() {
			this.set('reqCount', this.reqCount - 1);
			this.fireEvent('resetFocus');
		}, this);

		this.documentStore.on('load', function(store, records) {
			this.set('reqCount', this.reqCount - 1);
			this.populateList(records, this.documents, {
				mtype: 'RS.search.DocumentRenderer',
				dest: 'RS.wiki.Main',
				collapsed: !this.docExpanded
			});
			this.set('total', this.documentStore.getTotalCount());
			this.computePageInfo();
			this.fireEvent('resetFocus');
		}, this);

		this.postStore.on('load', function(store, records) {
			this.set('reqCount', this.reqCount - 1);
			this.populateList(records, this.posts, {
				mtype: 'RS.social.Post',
				dest: 'RS.actiontask.ActionTask',
				showMove$: function() {
					return false;
				},
				showShareLink$: function() {
					return false;
				}
			});
			this.set('total', this.postStore.getTotalCount());
			this.computePageInfo();
			this.fireEvent('resetFocus');
		}, this);

		this.automationStore.on('load', function(store, records) {
			this.set('reqCount', this.reqCount - 1);
			this.populateList(records, this.automations, {
				mtype: 'RS.search.AutomationRenderer',
				dest: 'RS.actiontask.ActionTask',
				collapsed: !this.autoExpanded
			});
			this.set('total', this.automationStore.getTotalCount());
			this.computePageInfo();
			this.fireEvent('resetFocus');
		}, this);
	},

	gotoSearchHome: function(button) {
		if (this.homeWindow) {
			this.homeWindow.doClose();
			this.set('homeWindow', null);
		} else {
			this.set('homeWindow', this.open({
				mtype: 'RS.search.Home',
				target: button.getEl().id
			}));
		}
	},

	populateList: function(records, list, config) {
		list.removeAll();

		if (records && records.length) {
			for(var i = 0; i < records.length; i++) {
				list.add(this.model(Ext.apply(Ext.apply({}, config), records[i].raw)));
			}
		} else {
			list.add(this.model({
				mtype: 'RS.search.NothingFoundRenderer'
			}));
		}
	},

	searchDocument: function() {
		this.documentStore.load();
	},

	updatePreview: function(record) {
		var fullName = record.get('fullName');
		url = '/resolve/service/wiki/view/' + fullName.replace(/[.]/g, '/');
		this.set('iframe', url);
	},

	iframe: '',

	postStore: {
		mtype: 'store',
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		fields: ['id', 'content', 'title', 'author', 'sysUpdatedOn'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/searchPosts',
			reader: {
				type: 'json',
				root: 'records',
				totalProperty: 'total'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	posts: {
		mtype: 'list'
	},

	when_pageNumber_changes_reload_posts: {
		on: ['pageNumberChanged'],
		action: function() {
			if (this.activeItem === 0)
				this.documentStore.load();
			else if (this.activeItem === 1)
				this.postStore.load();
			else
				this.automationStore.load();
		}
	},

	automationColumns: [{
		text: '~~name~~',
		dataIndex: 'name'
	}, {
		text: '~~fullName~~',
		dataIndex: 'fullName'
	}, {
		text: '~~namespace~~',
		dataIndex: 'namespace'
	}, {
		text: '~~deleted~~',
		dataIndex: 'deleted',
		renderer: RS.common.grid.booleanRenderer(),
		width: 60
	}, {
		text: '~~hidden~~',
		dataIndex: 'hidden',
		renderer: RS.common.grid.booleanRenderer(),
		width: 60
	}, {
		text: '~~active~~',
		dataIndex: 'active',
		renderer: RS.common.grid.booleanRenderer(),
		width: 60
	}, {
		text: '~~summary~~',
		dataIndex: 'summary',
		flex: 1
	}],

	automationStore: {
		mtype: 'store',
		fields: [
			'name',
			'fullName',
			'namespace',
			'summary',
			'deleted',
			'hidden',
			'active',
			'type'
		].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/searchAutomations',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	automations: {
		mtype: 'list'
	},

	documentsSelectionChange: function(renderer) {
		if (this.selectedDocument) {
			this.selectedDocument.removeCls('render-selected');
		}

		renderer.addCls('render-selected');
		this.set('selectedDocument', renderer);
	},

	selectedDocument: null,

	automationsSelectionChange: function(renderer) {
		if (this.selectedAutomation) {
			this.selectedAutomation.removeCls('render-selected');
		}

		renderer.addCls('render-selected');
		this.set('selectedAutomation', renderer);
	},

	selectedAutomation: null,

	cleanIframes: function() {
		//Clean up anything like leftover iframes and links
		var frame = document.getElementById('searchresult-iframe');
		if(frame && frame.contentWindow) {
			var event = frame.contentWindow.document.createEvent("Event");
			event.initEvent('beforedestroy', true, false);
			frame.contentWindow.document.dispatchEvent(event);		
		}
	},

	preview: function() {
		var url = null;
		this.cleanIframes();

		if (this.activeItem === 0) {
			if (this.selectedDocument) {
				var rec = this.documentStore.getById(this.selectedDocument.recordId);
				if (rec.get('type') == 'playbook') {
					url = Ext.String.format('/resolve/jsp/rsclient.jsp?displayClient=false&{0}#RS.incident.PlaybookTemplateSearchPreview/name={1}', clientVM.rsclientToken, rec.get('fullName'));
				} else {
					url = Ext.String.format('/resolve/service/wiki/view?wiki={0}&{1}={2}', rec.get('fullName'), clientVM.CSRFTOKEN_NAME, clientVM.getPageToken('service/wiki/view'));
				}
			}
		} else if (this.activeItem === 2) {
			if (this.selectedAutomation) {
				var rec = this.automationStore.getById(this.selectedAutomation.recordId);

				if (rec.get('type').toLowerCase() === 'actiontask') {
					url = Ext.String.format('/resolve/jsp/rsclient.jsp?displayClient=false&{0}#RS.actiontask.ActionTask/id={1}&preview=true', clientVM.rsclientToken, this.selectedAutomation.recordId);
				} else {
					url = Ext.String.format('/resolve/service/wiki/view?wiki={0}&{1}={2}', rec.get('fullName'), clientVM.CSRFTOKEN_NAME, clientVM.getPageToken('service/wiki/view'));
				}
			}
		}

		if (url !== null) {
			//console.log('setting iframe to ', url);
			this.set('iframe', url);
			this.setPreviewMode(0);
		}
	},

	setPreviewMode: function(counter) {
		if (counter < 20) {
			setTimeout(function(){
				var iframe = document.getElementById('searchresult-iframe');
				var iframeDocBody = iframe.contentDocument.body;
				if (iframeDocBody && iframeDocBody.innerHTML) {
					iframeDocBody.className += ' preview';
				} else {
					this.setPreviewMode(++counter);
				}
			}.bind(this), 200)
		}
	},

	localRendered$: function() {
		return this.activeItem !== 1;
	},

	isPreviewable$: function() {
		return this.activeItem === 0 || this.activeItem === 2;
	},

	test: function() {
		this.open({
			mtype: 'RS.search.Test'
		})
	},

	localizer$: function() {
		return {
			doLocalize: function(text) {
				return this.localize(text);
			}.bind(this)
		};
	},

	removeFilter: function(removedFilter) {
		this.set('filterItems', Ext.Array.filter(this.filterItems, function(item) {
			return item.field !== removedFilter;
		}));

		this.refresh();
	}
});

glu.defModel('RS.search.NothingFoundRenderer', {

});