glu.defModel('RS.search.Home', {
	historyStore: {
		mtype: 'store',
		fields: ['query', 'type'].concat(RS.common.grid.getSysFields()),
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		remoteSort: true,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/getUserQuery',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			},
			extraParams: {
				limit: 20
			}
		}
	},

	topQueryStore: {
		mtype: 'store',
		fields: ['query', 'selectCount', 'type'],
		remoteSort: true,
		sorters: [{
			property: 'selectCount',
			direction: 'DESC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/getTopQueries',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			},
			extraParams: {
				limit: 20
			}
		}
	},

	historyColumns: null,

	topQueryColumns: [{
		header: '~~query~~',
		dataIndex: 'query',
		flex: 1
	}, {
		header: '~~selectCount~~',
		dataIndex: 'selectCount'
	}],

	activate: function() {
		// clientVM.setWindowTitle(this.localize('windowTitle'));
		this.historyStore.load();
		this.topQueryStore.load();
	},
	init: function() {
		this.topQueryStore.on('beforeload', function(store, op) {
			var params = op.params || {};
			Ext.applyIf({
				start: 1,
				limit: 5
			});
			op.params = params;
		}, this);
		me = this;
		this.set('historyColumns', [{
			header: '~~query~~',
			dataIndex: 'query'
		}].concat((function(cols) {
			for (var idx = 0; idx < cols.length; idx++) {
				if (cols[idx].dataIndex == 'sysUpdatedOn') {
					cols[idx].hidden = false;
					cols[idx].flex = 1;
					cols[idx].text = me.localize('lastSearchedOn')
					return [cols[idx]];
				}
			}
		})(RS.common.grid.getSysColumns())));

		this.activate();

	},
	toggleUpHomeButton: function() {
		this.parentVM.set('homeWindow', null);
	},
	search: function(record) {
		switch (this.parentVM.activeItem) {
			case 0:
				this.parentVM.set('documentKeyword', record.get('query'))
				break;
			case 1:
				this.parentVM.set('socialKeyword', record.get('query'))
				break;
			default:
				this.parentVM.set('automationKeyword', record.get('query'))
		}
		this.parentVM.searchNow();
		this.doClose();
	}
})