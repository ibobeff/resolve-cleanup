glu.defModel('RS.search.AutomationRenderer', {
	mixins: ['Renderer'],
	init: function() {
		this.set('fullNameLink', {
			tag: 'a',
			href:(
				this.type=='actiontask'?
				Ext.String.format('#RS.actiontask.ActionTask/id={0}',this.id):
				Ext.String.format('#RS.wiki.Main/name={0}',this.fullName)
			),
			target: '_blank',
			cls: "rs-link",
			html: this.fullName
		})
	},

	numberOfReviewsIsVisible$: function() {
		return this.type != 'actiontask';
	},

	jumpToEdit: function() {
		clientVM.handleNavigation({
			modelName: (this.type == 'actiontask' ? 'RS.actiontask.ActionTask' : 'RS.wiki.Main'),
			params: {
				id: this.id,
				name : this.fullName
			}
		})
	}
});