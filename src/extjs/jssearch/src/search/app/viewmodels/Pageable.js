glu.mreg('pageable', {
	pageNumber: 1,
	pageSize: 15,
	total: 0,
	pageInfo: '',
	defaultPageInfo: {
		pageNumber: 1,
		pageSize: 15,
		total: 0,
		pageInfo: ''
	},
	computePageInfo: function() {
		if (this.total > 0) {
			var from = (this.pageNumber - 1) * this.pageSize + 1;
			var to = this.pageNumber * this.pageSize > this.total ? this.total : this.pageNumber * this.pageSize;
			this.set('pageInfo', Ext.String.format('{0} - {1} of {2}', from, to, this.total));
		} else
			this.set('pageInfo', 'None');
	},
	resetPageInfo: function() {
		with(this.defaultPageInfo) {
			this.set('pageNumber', pageNumber);
			this.set('pageSize', pageSize);
			this.set('total', total);
			this.set('pageInfo', pageInfo);
		}
	},
	previousPage: function() {
		this.set('pageNumber', this.pageNumber - 1)
	},
	previousPageIsEnabled$: function() {
		return this.pageNumber > 1
	},
	nextPage: function() {
		this.set('pageNumber', this.pageNumber + 1)
	},
	nextPageIsEnabled$: function() {
		return this.pageNumber * this.pageSize < this.total;
	},
	nextPageReqParam: function() {
		return {
			page: this.pageNumber,
			start: (this.pageNumber - 1) * this.pageSize,
			limit: this.pageSize
		}
	},
	firstPage: function() {
		this.set('pageNumber', 1);
	},
	firstPageIsEnabled$: function() {
		return this.previousPageIsEnabled;
	},
	lastPage: function() {
		this.set('pageNumber', Math.ceil(this.total / this.pageSize));
	},
	lastPageIsEnabled$: function() {
		return this.nextPageIsEnabled;
	},
	makeStorePageable: function(store) {
		store.on('beforeload', function(store, op) {
			var params = op.params || {};
			params = Ext.applyIf(params, this.nextPageReqParam());
		});
	}
});