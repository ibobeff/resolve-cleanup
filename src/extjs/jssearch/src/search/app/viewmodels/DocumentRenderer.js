glu.defModel('RS.search.DocumentRenderer', {
	mixins: ['Renderer'],

	init: function() {
		if (this.type == 'playbook') {
			this.dest = 'RS.incident.PlaybookTemplateRead';
		}
		this.set('fullNameLink', {
			tag: 'a',
			href: Ext.String.format('#{0}/name={1}', this.dest, this.fullName),
			target: '_blank',
			cls: "rs-link",
			html: this.fullName
		});

	},

	jumpToEdit: function() {
		clientVM.handleNavigation({
			modelName: this.dest,
			params: {
				name: this.fullName
			}
		});
	},

	nothingFound$: function() {
		return !this.id;
	}
});