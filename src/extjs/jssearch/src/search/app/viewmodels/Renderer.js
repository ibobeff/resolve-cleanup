glu.defModel('RS.search.Renderer', {
	id: '',
	title: '',
	name: '',
	dest: '',
	type: '',
	fullName: '',
	fullNameLink: '',
	rawSummary: '',
	summary: '',
	tags: [],
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	rating: 0,
	numberOfReviews: 0,
	collapsed: false,
	updated$: function() {
		return this.updatedOn + ' ' + this.sysUpdatedBy;
	},
	doctypeMap: {
		document : 'Document',
		automation : 'Automation',
		decisiontree : 'Decision Tree',
		actiontask : 'ActionTask',
		playbook : 'Playbook Template',
	},
	doctype$: function() {
		if (this.type == 'playbook' || this.type == 'actiontask') {
			return this.doctypeMap[this.type];
		} else {
			var doctypes = '';
			var types = this.type.split(', ');
			for (var i=0; i<types.length; i++) {
				var doctype = this.doctypeMap[types[i]];
				if (doctypes) {
					doctypes += ', ';
				}
				if (doctype) {
					doctypes += doctype;
				} else {
					doctypes += types[i];
				}
			}
			return doctypes;
		}
	},
	displaySummary$: function() {
		if (!this.summary)
			return;
		var doc = document.createElement('div');
		var result = this.summary;
		doc.innerHTML = result;
		if (doc.innerHTML == result && result.indexOf('<') != -1)
			return result
		var lines = result.split('\n');
		this.set('lineNum', lines.length);
		var sliced = lines.slice(0, lines.length > 3 ? 3 : lines.length).join('<br/>');
		var result = sliced;
		return result;
	},
	displayDetailedSummary$: function() {
		if (!this.summary)
			return;
		var doc = document.createElement('div');
		var result = this.summary;
		doc.innerHTML = this.summary;
		if (doc.innerHTML !== this.summary)
			result = Ext.String.htmlEncode(this.summary);
		result = Ext.util.Format.nl2br(result);
		return result;
	},
	tagsIsVisible$: function() {
		return this.tags.length > 0;
	},
	updatedOn$: function() {
		return Ext.Date.format(new Date(this.sysUpdatedOn), clientVM.getUserDefaultDateFormat());
	},
	socialReview: function() {
		clientVM.handleNavigation({
			modelName: 'RS.social.Main',
			params: {
				streamId: this.id,
				streamParent: this.fullName
			}
		});
	},
	detailed: false,
	lineNum: 1,
	showDetail: function() {
		this.set('detailed', !this.detailed)
	},
	canShowDetail$: function() {
		return this.rawSummary && this.lineNum > 3;
	},
	moreOrLess$: function() {
		return this.detailed ? this.localize('less') : this.localize('more');
	},
	allTags: false,
	moreOrLessTags$: function() {
		return this.allTags ? this.localize('less') : this.localize('showAllTags');
	},
	showTags$: function() {
		var len = this.tags.length < 6 || this.allTags ? this.tags.length : 6;
		return (this.tags.slice(0, len)).join(', ');
	},
	showTagsIsVisible$: function() {
		return !!this.showTags
	},
	showAllTagsIsVisible$: function() {
		return this.tags && this.tags.length > 6;
	},
	showAllTags: function() {
		this.set('allTags', !this.allTags);
	}
});