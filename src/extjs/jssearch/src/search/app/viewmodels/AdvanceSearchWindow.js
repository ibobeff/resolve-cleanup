glu.defModel('RS.search.AdvanceSearchWindow', {
	fields: ['keyword', 'targetId', 'sysCreatedBy', 'sysUpdatedBy', 'authorUsername', 'commentAuthorUsername', {
		name: 'type',
		type: 'array'
	}, {
		name: 'namespace',
		type: 'array'
	}, {
		name: 'updatedOn',
		type: 'time'
	}, {
		name: 'updatedOnTime',
		type: 'time'
	}, 'updatedOnCondition', {
		name: 'createdOn',
		type: 'time'
	}, 'createdOnCondition', {
		name: 'createdOnTime',
		type: 'time'
	}, {
		name: 'field',
		type: 'array'
	}, {
		name: 'tag',
		type: 'array'
	}],

	defaultData: {
		namespace: [],
		updatedOn: '',
		updatedOnTime: '',
		authorUsername: '',
		commentAuthorUsername: '',
		updatedOnCondition: 'on',
		createdOn: '',
		createdOnTime: '',
		createdOnCondition: 'on',
		field: '',
		tag: [],
		targetId: '',
		type: ''
	},
	targetData: {
		targetId: ''
	},
	sysCreatedBy: '',
	sysCreatedByIsVisible$: function() {
		return this.activeItem != 1;
	},
	sysUpdatedBy: '',
	sysUpdatedByIsVisible$: function() {
		return this.activeItem != 1;
	},
	authorUsername: '',
	authorUsernameIsVisible$: function() {
		return this.activeItem == 1;
	},
	commentAuthorUsername: '',
	commentAuthorUsernameIsVisible$: function() {
		return this.activeItem == 1;
	},
	keyword: '',
	namespace: [],
	namespaceIsVisible$: function() {
		return this.activeItem != 1;
	},
	target: null,
	assembleDateTime: function(date, time) {
		if (!date)
			return null;
		var strDate = Ext.Date.format(date, 'Y-m-d');
		var strTime = Ext.Date.format((time ? time : Ext.Date.clearTime(new Date())), 'H:i:s');
		var strDateTime = strDate + ' ' + strTime;
		var dateTime = Ext.Date.parse(strDateTime, 'Y-m-d H:i:s');
		return dateTime;
	},
	updatedOnTimeObj: null,
	updatedOn: null,
	updatedOnCondition: 'on',
	timeConditionStore: {
		mtype: 'store',
		fields: ['condition'],
		data: [{
			condition: 'before'
		}, {
			condition: 'on'
		}, {
			condition: 'after'
		}]
	},
	updatedOnChanged$: function() {
		this.set('updatedOnTimeObj', this.assembleDateTime(this.updatedOn, this.updatedOnTime));
	},
	updatedOnValidFlag: true,
	setUpdatedOnValid: function(isValid) {
		this.set('updatedOnValidFlag', isValid);
	},
	updatedOnTime: null,
	updatedOnTimeChanged$: function() {
		this.updatedOnChanged$();
	},
	updatedOnTimeValidFlag: true,
	setUpdatedOnTimeValid: function(isValid) {
		this.set('updatedOnTimeValidFlag', isValid);
	},
	createdOnTimeObj: null,
	createdOn: null,
	createdOnCondition: 'on',
	createdOnValidFlag: true,
	createdOnChanged$: function() {
		this.set('createdOnTimeObj', this.assembleDateTime(this.createdOn, this.createdOnTime));
	},
	setCreatedOnValid: function(isValid) {
		this.set('createdOnValidFlag', isValid);
	},
	createdOnTime: null,
	createdOnTimeValidFlag: true,
	createdOnTimeChanged$: function() {
		this.createdOnChanged$();
	},
	setCreatedOnTimeValid: function(isValid) {
		this.set('createdOnTimeValidFlag', isValid);
	},
	targetId: '',
	targetIdChanged$: function() {
		//this.targetIds.clearFilter();
		/*this.targetIds.filter({
			property: 'displayName',
			anyMatch: true,
			value: this.targetId
		});*/
	},
	targetIds: {
		mtype: 'store',
		fields: ['displayName', 'id', 'type'],
		proxy: {
			type: 'memory'
		}
	},
	targetIdIsVisible$: function() {
		return this.activeItem == 1;
	},
	field: [],
	tag: [],
	searchIsEnabled$: function() {
		return this.updatedOnValidFlag && this.updatedOnTimeValidFlag && this.createdOnValidFlag && this.createdOnTimeValidFlag;
	},
	type: '',
	typeIsVisible$: function() {
		return this.activeItem != 1;
	},
	activeItem: 0,
	init: function() {
		switch (this.activeItem) {
			case 0:
				this.typeStore.add([{
					name: 'Document',
					value: 'document'
				}, {
					name: 'Decision Tree',
					value: 'decisiontree'
				}, {
					name: 'Playbook Template',
					value: 'playbook'
				}]);
				break;
			case 1:
				this.typeStore.add([{
					name: 'Post',
					value: 'post'
				}]);
				break;
			case 2:
				this.typeStore.add([{
					name: 'Automation',
					value: 'automation'
				}, {
					name: 'ActionTask',
					value: 'actiontask'
				}]);
				break;
			default:
		}
		if (this.activeItem == 0)
			this.fieldStore.add({
				name: this.localize('attachment'),
				value: 'attachment'
			}, {
				name: this.localize('attachmentContent'),
				value: 'attachmentContent'
			});
		if (this.activeItem != 1)
			this.fieldStore.add([{
					name: this.localize('fullName'),
					value: 'fullName'
				}, {
					name: this.localize('summary'),
					value: 'summary'
				}
				// {
				// 	name: this.localize('sysCreatedBy'),
				// 	value: 'sysCreatedBy'
				// }, {
				// 	name: this.localize('sysUpdatedBy'),
				// 	value: 'sysUpdatedBy'
				// }
			]);
		if (this.activeItem != 2)
			this.fieldStore.add([{
				name: this.localize(this.activeItem != 1 ? 'title' : 'subject'),
				value: 'title'
			}, {
				name: this.localize('content'),
				value: 'content'
			}]);

		if (this.activeItem == 1)
			this.fieldStore.add([{
					name: this.localize('comment'),
					value: 'socialcomment.content'
				}
				// {
				// 	name: this.localize('authorUsername'),
				// 	value: 'authorUsername'
				// }, {
				// 	name: this.localize('commentAuthorUsername'),
				// 	value: 'socialcomment.comment_Author_Us_ern_ame'
				// }
			])
		if (Ext.state.Manager.get('lastSearch' + this.activeItem)) {
			var lastSearch = Ext.state.Manager.get('lastSearch' + this.activeItem);
			var lastFields = [];
			if (lastSearch.field) {
				if (Ext.isArray(lastSearch.field))
					lastFields = lastSearch.field;
				else
					lastFields = lastSearch.field.split(',');
			}
			var validatedFields = [];
			Ext.each(lastFields, function(f) {
				this.fieldStore.each(function(current) {
					if (f == current.get('value'))
						validatedFields.push(f);
				});
			}, this);
			lastSearch.field = validatedFields;
			lastSearch.keyword = this.keyword;
			this.loadData(lastSearch)
		}

		this.tagStore.on('beforeload', function(store, op) {
			op.params = op.params || {};
			var target = '';
			if (typeof this.tag == 'object')
				target = this.tag && this.tag.length > 0 ? this.tag[0] : '';
			else {
				target = this.tag.split(',');
				target = target[target.length - 1]
			}
			op.params.filter = Ext.encode([{
				"field": "name",
				"type": "auto",
				"condition": "contains",
				"value": target
			}]);
		}, this);

		this.namespaceStore.on('beforeload', function(store, op) {
			op.params = op.params || {};
			var target = '';
			if (typeof this.namespace == 'object')
				target = this.namespace && this.namespace.length > 0 ? this.namespace[0] : '';
			else {
				target = this.namespace.split(',');
				target = target[target.length - 1];
			}
			op.params.filter = Ext.encode([{
				"field": "unamespace",
				"type": "auto",
				"condition": "contains",
				"value": target
			}]);
		}, this);

		this.ajax({
			url: '/resolve/service/social/getUserStreams',
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('getTargetIdError', {
						msg: respData.message
					}));
					return;
				}
				Ext.each(respData.records, function(r) {
					if (!r.type || r.type.toLowerCase() == 'namespace')
						return;
					this.targetIds.add(r);
				}, this);
				this.targetIds.sort('displayName', 'ASC');
				var targetId = this.targetId;
				this.set('targetId', null);
				this.set('targetId', targetId.split(','));
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	namespaceStore: {
		mtype: 'store',
		fields: ['unamespace'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/search/namespaces',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			},
			limitParam: null
		}
	},

	fieldStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	tagStore: {
		mtype: 'store',
		fields: ['name'],
		sorters: [{
			property: 'name',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/tag/listTags',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	typeStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	collectFilters: function() {
		var filters = [];
		var fields = [];
		Ext.each(this.field, function(f) {
			fields.push(f);
		});

		filters.push({
			field: Ext.isArray(fields) ? fields.join(',') : fields,
			type: 'auto',
			condition: 'contains',
			value: this.keyword
		});

		Ext.each(['sysCreatedBy', 'sysUpdatedBy', 'authorUsername', 'commentAuthorUsername'], function(field) {
			if (this[field])
				filters.push({
					field: (field != 'commentAuthorUsername' ? field : 'socialcomment.comment_Author_Us_ern_ame'),
					type: 'auto',
					condition: 'contains',
					value: this[field]
				});
		}, this);

		if (this.type && this.type.length > 0) {
			if (Ext.isArray(this.type) && this.type.length > 0)
				filters.push({
					field: 'type',
					type: 'auto',
					condition: 'contains',
					value: this.type.join(',')
				})
			else
				filters.push({
					field: 'type',
					type: 'auto',
					condition: 'contains',
					value: this.type
				})
		}
		if (this.namespace && this.namespace.length > 0)
			filters.push({
				field: 'namespace',
				type: 'auto',
				condition: 'equals',
				value: Ext.isArray(this.namespace) ? this.namespace.join(',') : this.namespace
			});
		if (this.updatedOnTimeObj)
			filters.push({
				field: 'sysUpdatedOn',
				type: 'date',
				condition: this.updatedOnCondition,
				value: Ext.Date.format(this.updatedOnTimeObj, 'c')
			});
		if (this.createdOnTimeObj)
			filters.push({
				field: 'sysCreatedOn',
				type: 'date',
				condition: this.createdOnCondition,
				value: Ext.Date.format(this.createdOnTimeObj, 'c')
			});
		if (this.tag && this.tag.length > 0)
			filters.push({
				field: 'tag',
				type: 'auto',
				condition: 'equals',
				value: Ext.isArray(this.tag) ? this.tag.join(',') : this.tag
			});
		if (this.targetId && this.targetId.length > 0) {
			var tids = this.targetId;
			var displayFields = [];
			if (!Ext.isArray(tids)) {
				tids = tids.split();
			}
			Ext.each(tids, function(id) {
				if (id)
					displayFields.push(this.targetIds.getAt(this.targetIds.findExact('id', id)).get('displayName'));
			}, this);
			if (displayFields.length > 0)
				filters.push({
					field: 'targetId',
					type: 'auto',
					condition: 'contains',
					value: Ext.isArray(this.targetId) ? this.targetId.join(',') : this.targetId,
					displayFields: displayFields
				});
		}
		return filters;
	},

	search: function() {
		this.parentVM.set('documentKeyword', this.keyword);
		this.parentVM.set('socialKeyword', this.keyword);
		this.parentVM.set('automationKeyword', this.keyword);
		this.parentVM.searchNow({}, this.collectFilters());
		Ext.state.Manager.set('lastSearch' + this.activeItem, this.asObject());
		this.cancel();
	},
	resetInputs: function() {
		this.loadData(this.defaultData);
		Ext.state.Manager.set('lastSearch' + this.activeItem, this.asObject());
		this.namespaceStore.load();
	},
	cancel: function() {
		this.doClose();
	}
});