/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/
glu.defModel('RS.resolutionrouting.Bulk',{
	mixins: ['FileUploadManager'],
	api: {
		upload: '/resolve/service/resolutionrouting/rule/csv/import/upload',
		list: ''
	},

	allowedExtensions: ['csv'],
	fineUploaderTemplateHidden: false,
	checkForDuplicates: false,
	allowMultipleFiles: false,
	uploaderAdditionalClass: 'resolutionrouting',

	fileOnSubmittedCallback: function(manager, filename) {
		this.uploader.setParams({
			override: this.override,
			forceImport: this.forceImport
		});
	},

	fileOnUploadCallback: function(manager, filename) {
		this.set('wait', true);
	},

	fileOnCompleteCallback: function(manager, filename, response, xhr) {
        this.set('wait', false);

		this.importStart(manager, response.data);
	},

	title: '',
	bulkType: 0,
	TYPE_IMPORT: 0,
	TYPE_EXPORT: 1,
	TYPE_ACTIVATE: 2,
	TYPE_DEACTIVATE: 3,
	wait: false,
	status: null,
	busyText: '',
	progress: 0.0,
	keepPolling: false,
	override: false,
	forceImport: false,
	otherUsersAreDoingBulk: false,
	currentTid: '',
	overrideIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT && !this.wait;
	},
	forceImportIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT && !this.wait;
	},
	init: function() {
		this.set('dragHereText', this.localize('dropArea'));
		switch (this.bulkType) {
			case this.TYPE_EXPORT:
				this.set('title', this.localize('exportTitle'));
				this.set('fineUploaderTemplateHidden', true);
				break;
			case this.TYPE_IMPORT:
				this.set('title', this.localize('importTitle'));
				this.set('fineUploaderTemplateHidden', false);
				break;
			case this.TYPE_ACTIVATE:
				this.set('title', this.localize('activationTitle'));
				this.set('fineUploaderTemplateHidden', true);
				break;
			case this.TYPE_DEACTIVATE:
				this.set('title', this.localize('deactivationTitle'));
				this.set('fineUploaderTemplateHidden', true);
		}
		if (this.bulkType == this.TYPE_EXPORT)
			this.exportCSV();
		else if (this.bulkType == this.TYPE_ACTIVATE)
			this.activateMapping();
		else if (this.bulkType == this.TYPE_DEACTIVATE)
			this.deactivateMapping();

	},
	importCSV: function() {},
	importCSVIsEnabled$: function() {
		return !this.wait;
	},
	importCSVIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT;
	},
	beforeImport: function(nextAction) {
		this.ajax({
			url: '/resolve/service/resolutionrouting/csv/import/before',
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('checkBeforeImportError', respData.message));
					return;
				}
				nextAction.call(this, respData);
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	importStart: function(uploader, respData) {
		this.set('currentTid', respData.TASK_ID)
		if (respData.EVT_TYPE) {
			this.set('previousOp', 'importStart');
			this.set('opOnCurrentNode', respData.EVT_IS_CURRENT_NODE == 'true');
			clientVM.displayMessage(
				this.localize('bulkOpInProgress'),
				this.localize(
					respData.EVT_TYPE, [
						respData.EVT_USERNAME,
						respData.EVT_RSVIEW_ID,
						this.opOnCurrentNode ? this.localize('currentNode') : this.localize('otherNode')
					]
				)
			);
			if (!this.opOnCurrentNode) {
				this.close();
				return;
			}
			this.adjustPollingMode(respData.EVT_TYPE);
			return;
		}
		this.set('wait', true);
		this.set('keepPolling', true);
		this.set('taskStatus', []);
		this.pollBulk();
	},
	processedStatus$: function() {
		var lines = [];
		Ext.each(this.taskStatus, function(status) {
			if (status.finished) {
				if (this.type != this.TYPE_IMPORT)
					lines.push('Finished.');
				return;
			}
			var color = 'red';
			if (status.state == 'success')
				color = 'green';
			if (status.state.toLowerCase() == 'global') {
				lines.push(Ext.String.format('<pre style="color:red;">Global:{0}', status.detail));
				return;
			}
			if (this.bulkType == this.TYPE_IMPORT) {
				lines.push(Ext.String.format('<pre style="color:{0};">{1}[{2}]:{3}{4}</pre>', color, status.state, status.line_no, status.line, (status.detail ? '<br/>' + status.detail : '')));
				return;
			}
			lines.push(Ext.String.format('<pre style="color:{0};">{1}:{2}, {3}, {4}{5}</pre>', color, status.state, status.id, status.rid, status.schema, (status.detail ? '<br/>' + status.detail : '')));
		}, this);
		return lines.join('<br/>')
	},
	adjustPollingMode: function(evtType) {
		this.set('keepPolling', true);
		this.set('wait', true);
		switch (evtType) {
			case 'EVT_IMPORT':
				this.set('bulkType', this.TYPE_IMPORT);
				this.set('title', this.localize('importTitle'));
				break;
			case 'EVT_EXPORT':
				this.set('bulkType', this.TYPE_EXPORT);
				this.set('title', this.localize('exportTitle'));
				break;
			case 'EVT_ACTIVATION':
				this.set('bulkType', this.TYPE_ACTIVATE);
				this.set('title', this.localize('activationTitle'));
				break;
			case 'EVT_DEACTIVATION':
				this.set('bulkType', this.TYPE_DEACTIVATE);
				this.set('title', this.localize('deactivationTitle'));
				break;
		}
		this.pollBulk();
	},
	opOnCurrentNode: true,
	previousOp: '',
	activateMapping: function(force) {
		this.asyncAction({
			url: '/resolve/service/resolutionrouting/rule/activation/' + (this.parentVM.selectAll ? 'query' : 'id'),
			params: {
				isActivated: true,
				force: force === true
			},
			submitReqError: 'submitExportReqError',
			onRequestSubmitted: function(respData) {
				this.set('currentTid', respData.data.TASK_ID);
				if (respData.data.EVT_TYPE) {
					this.set('previousOp', 'activateMapping');
					this.set('opOnCurrentNode', respData.data.EVT_IS_CURRENT_NODE == 'true');
					clientVM.displayMessage(
						this.localize('bulkOpInProgress'),
						this.localize(
							respData.data.EVT_TYPE, [
								respData.data.EVT_USERNAME,
								respData.data.EVT_RSVIEW_ID,
								this.opOnCurrentNode ? this.localize('currentNode') : this.localize('otherNode')
							]
						)
					);
					if (!respData.data || !this.opOnCurrentNode) {
						this.close();
						return;
					}
					this.adjustPollingMode(respData.data.EVT_TYPE);
					return;
				}
				this.set('wait', true);
				this.set('keepPolling', true);
				this.pollBulk();
			}
		});
	},
	deactivateMapping: function(force) {
		this.asyncAction({
			url: '/resolve/service/resolutionrouting/rule/activation/' + (this.parentVM.selectAll ? 'query' : 'id'),
			params: {
				isActivated: false,
				force: force === true
			},
			submitReqError: 'submitExportReqError',
			onRequestSubmitted: function(respData) {
				this.set('currentTid', respData.data.TASK_ID);
				if (respData.data.EVT_TYPE) {
					this.set('previousOp', 'deactivateMapping');
					this.set('opOnCurrentNode', respData.data.EVT_IS_CURRENT_NODE == 'true');
					clientVM.displayMessage(
						this.localize('bulkOpInProgress'),
						this.localize(
							respData.data.EVT_TYPE, [
								respData.data.EVT_USERNAME,
								respData.data.EVT_RSVIEW_ID,
								this.opOnCurrentNode ? this.localize('currentNode') : this.localize('otherNode')
							]
						)
					);
					if (!respData.data || !this.opOnCurrentNode) {
						this.close();
						return;
					}
					this.adjustPollingMode(respData.data.EVT_TYPE);
					return;
				}
				this.set('wait', true);
				this.set('keepPolling', true);
				this.pollBulk();
			}
		});
	},
	exportCSV: function(force) {
		this.asyncAction({
			url: '/resolve/service/resolutionrouting/rule/csv/export/init/' + (this.parentVM.selectAll ? 'query' : 'id'),
			params: {
				force: force === true
			},
			submitReqError: 'submitExportReqError',
			onRequestSubmitted: function(respData) {
				this.set('currentTid', respData.data.TASK_ID)
				if (respData.data.EVT_TYPE) {
					this.set('previousOp', 'exportCSV');
					this.set('opOnCurrentNode', respData.data.EVT_IS_CURRENT_NODE == 'true');
					clientVM.displayMessage(
						this.localize('bulkOpInProgress'),
						this.localize(
							respData.data.EVT_TYPE, [
								respData.data.EVT_USERNAME,
								respData.data.EVT_RSVIEW_ID,
								this.opOnCurrentNode ? this.localize('currentNode') : this.localize('otherNode')
							]
						)
					);
					if (!respData.data || !this.opOnCurrentNode) {
						this.close();
						return;
					}
					this.adjustPollingMode(respData.data.EVT_TYPE);
					return
				}
				this.set('keepPolling', true);
				this.downloadCSV(this.currentTid);
			}
		}, true);
	},
	asyncAction: function(config, canRedo) {
		var url = config.url;
		var ids = [];
		this.set('keepPolling', true);
		Ext.each(this.parentVM.rulesSelections, function(rule) {
			ids.push(rule.get('id'));
		});
		var filters = this.parentVM.ruleStore.commonSearchFilter.getWhereFilter();
		var modulePath = '';
		if (this.parentVM.selectedModule)
			modulePath = this.parentVM.selectedModule.getPath('module', '.').substring(5) + (this.parentVM.selectedModule.isLeaf() ? '' : '.')
		if (modulePath && modulePath != '.')
			filters.push({
				field: 'module',
				type: 'auto',
				condition: 'equals',
				value: modulePath
			});
		var params = Ext.applyIf((this.parentVM.selectAll ? {
			filter: Ext.encode(filters)
		} : {
			ids: ids
		}), config.params);
		this.ajax({
			url: url,
			params: params,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					this.set('wait', false);
					clientVM.displayError(this.localize(config.submitReqError, respData.message));
					return;
				}
				config.onRequestSubmitted.call(this, respData);
			},
			failure: function(f) {
				clientVM.displayFailure(r);
			}
		});
	},
	progressIsVisible$: function() {
		return this.bulkType != this.TYPE_IMPORT || this.wait;
	},
	checkExportFinish: function(data) {
		var finished = false;
		Ext.each(data.status, function(st) {
			if (st.finished)
				finished = true;
		})
		if (data.hasGone)
			finished = true;
		return data.percent == 1.0 || finished;
	},
	pollBulk: function() {
		if (!this.keepPolling)
			return;
		this.ajax({
			url: '/resolve/service/resolutionrouting/rule/bulk/poll',
			params: {
				tid: this.currentTid
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('bulkPollError', respData.message));
					return;
				}
				if (respData.data.percent != 1 && !respData.data.hasGone) {
					this.set('progress', respData.data.percent);
					this.set('taskStatus', respData.data.status || []);
					var me = this;
					setTimeout(function() {
						if (me.keepPolling)
							me.pollBulk();
					}, 1000);
					return;
				}
				this.set('progress', 1);
				this.set('taskStatus', respData.data.status || this.taskStatus);
				this.set('previousOp', '');
				if (this.bulkType != this.TYPE_EXPORT) {
					this.parentVM.set('rulesSelections', []);
					this.parentVM.ruleStore.load();
					this.parentVM.expandToNode(this.parentVM.selectedModule);
				}
				this.set('fineUploaderTemplateHidden', true);
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	downloadCSV: function() {
		this.set('wait', true);
        this.pollBulk();
        //downloadFile('/resolve/service/resolutionrouting/rule/csv/export/download?tid=' + this.currentTid);
        var me = this;

        clientVM.getCSRFToken_ForURI('/resolve/service/resolutionrouting/rule/csv/export/download', function(data){
            var csrftoken = '&' + data[0] + '=' + data[1];
            downloadFile('/resolve/service/resolutionrouting/rule/csv/export/download?tid=' + me.currentTid + csrftoken);
        })
	},
	exportCSVIsEnabled$: function() {
		return !this.wait;
	},
	browse: function() {
	},
	browseIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT && !this.wait;
	},
	exportCSVIsVisible$: function() {
		return this.type != this.TYPE_IMPORT;
	},
	dropAreaIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT && !this.wait;
	},
	close: function() {
		this.set('keepPolling', false);
		this.doClose();
	},
	taskStatus: [],
	statusDisplayIsVisible$: function() {
		return this.taskStatus && this.taskStatus.length > 1;
	},
	uploadError: function(resp) {
		var respData = Ext.decode(resp.response);
		clientVM.displayError(respData.message || this.localize('fileUploadError', resp.message));
		this.set('keepPolling', true);
		this.pollBulk();
	},
	abort: function() {
		this.abortBulk(this.currentTid);
	},
	abortBulk: function(tid) {
		this.ajax({
			url: '/resolve/service/resolutionrouting/rule/bulk/abort',
			params: {
				tid: tid
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('cannotAbortProcess', respData.message));
					return;
				}
				this.set('currentTid', '');
				this.set('keepPolling', false);
				this.set('previousOp', '');
				clientVM.displaySuccess(this.localize('abortSuccess'));
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	abortIsVisible$: function() {
		return this.wait && this.opOnCurrentNode;
	},
	overrideLock: function() {
		this[this.previousOp](true);
	},
	overrideLockIsVisible$: function() {
		return this.wait && this.bulkType != this.TYPE_IMPORT && this.previousOp;
	},
	backToImport: function() {
		this.set('bulkType', this.TYPE_IMPORT);
		this.set('wait', false);
		this.set('keepPolling', false);
		this.set('currentTid', '');
		this.set('previousOp', '');
	},
	backToImportIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT && this.wait && this.previousOp;
	}
});

glu.defModel('RS.resolutionrouting.FileUploadManagerFactory', function(cfg) {
	Ext.applyIf(cfg, RS.common.viewmodels.FileUploadManager);
	return cfg;
});

/*
glu.defModel('RS.resolutionrouting.Bulk', {
	title: '',
	bulkType: 0,
	TYPE_IMPORT: 0,
	TYPE_EXPORT: 1,
	TYPE_ACTIVATE: 2,
	TYPE_DEACTIVATE: 3,
	wait: false,
	status: null,
	busyText: '',
	progress: 0.0,
	keepPolling: false,
	override: false,
	forceImport: false,
	otherUsersAreDoingBulk: false,
	currentTid: '',
	overrideIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT && !this.wait;
	},
	forceImportIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT && !this.wait;
	},
	init: function() {
		switch (this.bulkType) {
			case this.TYPE_EXPORT:
				this.set('title', this.localize('exportTitle'));
				break;
			case this.TYPE_IMPORT:
				this.set('title', this.localize('importTitle'));
				break;
			case this.TYPE_ACTIVATE:
				this.set('title', this.localize('activationTitle'));
				break;
			case this.TYPE_DEACTIVATE:
				this.set('title', this.localize('deactivationTitle'));
		}
		if (this.bulkType == this.TYPE_EXPORT)
			this.exportCSV();
		else if (this.bulkType == this.TYPE_ACTIVATE)
			this.activateMapping();
		else if (this.bulkType == this.TYPE_DEACTIVATE)
			this.deactivateMapping();

	},
	importCSV: function() {},
	importCSVIsEnabled$: function() {
		return !this.wait;
	},
	importCSVIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT;
	},
	beforeImport: function(nextAction) {
		this.ajax({
			url: '/resolve/service/resolutionrouting/csv/import/before',
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('checkBeforeImportError', respData.message));
					return;
				}
				nextAction.call(this, respData);
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	importStart: function(uploader, respData) {
		this.set('currentTid', respData.TASK_ID)
		if (respData.EVT_TYPE) {
			this.set('previousOp', 'importStart');
			this.set('opOnCurrentNode', respData.EVT_IS_CURRENT_NODE == 'true');
			clientVM.displayMessage(
				this.localize('bulkOpInProgress'),
				this.localize(
					respData.EVT_TYPE, [
						respData.EVT_USERNAME,
						respData.EVT_RSVIEW_ID,
						this.opOnCurrentNode ? this.localize('currentNode') : this.localize('otherNode')
					]
				)
			);
			if (!this.opOnCurrentNode) {
				this.close();
				return;
			}
			this.adjustPollingMode(respData.EVT_TYPE);
			return;
		}
		this.set('wait', true);
		this.set('keepPolling', true);
		this.set('taskStatus', []);
		this.pollBulk();
	},
	processedStatus$: function() {
		var lines = [];
		Ext.each(this.taskStatus, function(status) {
			if (status.finished) {
				if (this.type != this.TYPE_IMPORT)
					lines.push('Finished.');
				return;
			}
			var color = 'red';
			if (status.state == 'success')
				color = 'green';
			if (status.state.toLowerCase() == 'global') {
				lines.push(Ext.String.format('<pre style="color:red;">Global:{0}', status.detail));
				return;
			}
			if (this.bulkType == this.TYPE_IMPORT) {
				lines.push(Ext.String.format('<pre style="color:{0};">{1}[{2}]:{3}{4}</pre>', color, status.state, status.line_no, status.line, (status.detail ? '<br/>' + status.detail : '')));
				return;
			}
			lines.push(Ext.String.format('<pre style="color:{0};">{1}:{2}, {3}, {4}{5}</pre>', color, status.state, status.id, status.rid, status.schema, (status.detail ? '<br/>' + status.detail : '')));
		}, this);
		return lines.join('<br/>')
	},
	adjustPollingMode: function(evtType) {
		this.set('keepPolling', true);
		this.set('wait', true);
		switch (evtType) {
			case 'EVT_IMPORT':
				this.set('bulkType', this.TYPE_IMPORT);
				this.set('title', this.localize('importTitle'));
				break;
			case 'EVT_EXPORT':
				this.set('bulkType', this.TYPE_EXPORT);
				this.set('title', this.localize('exportTitle'));
				break;
			case 'EVT_ACTIVATION':
				this.set('bulkType', this.TYPE_ACTIVATE);
				this.set('title', this.localize('activationTitle'));
				break;
			case 'EVT_DEACTIVATION':
				this.set('bulkType', this.TYPE_DEACTIVATE);
				this.set('title', this.localize('deactivationTitle'));
				break;
		}
		this.pollBulk();
	},
	opOnCurrentNode: true,
	previousOp: '',
	activateMapping: function(force) {
		this.asyncAction({
			url: '/resolve/service/resolutionrouting/rule/activation/' + (this.parentVM.selectAll ? 'query' : 'id'),
			params: {
				isActivated: true,
				force: force === true
			},
			submitReqError: 'submitExportReqError',
			onRequestSubmitted: function(respData) {
				this.set('currentTid', respData.data.TASK_ID);
				if (respData.data.EVT_TYPE) {
					this.set('previousOp', 'activateMapping');
					this.set('opOnCurrentNode', respData.data.EVT_IS_CURRENT_NODE == 'true');
					clientVM.displayMessage(
						this.localize('bulkOpInProgress'),
						this.localize(
							respData.data.EVT_TYPE, [
								respData.data.EVT_USERNAME,
								respData.data.EVT_RSVIEW_ID,
								this.opOnCurrentNode ? this.localize('currentNode') : this.localize('otherNode')
							]
						)
					);
					if (!respData.data || !this.opOnCurrentNode) {
						this.close();
						return;
					}
					this.adjustPollingMode(respData.data.EVT_TYPE);
					return;
				}
				this.set('wait', true);
				this.set('keepPolling', true);
				this.pollBulk();
			}
		});
	},
	deactivateMapping: function(force) {
		this.asyncAction({
			url: '/resolve/service/resolutionrouting/rule/activation/' + (this.parentVM.selectAll ? 'query' : 'id'),
			params: {
				isActivated: false,
				force: force === true
			},
			submitReqError: 'submitExportReqError',
			onRequestSubmitted: function(respData) {
				this.set('currentTid', respData.data.TASK_ID);
				if (respData.data.EVT_TYPE) {
					this.set('previousOp', 'deactivateMapping');
					this.set('opOnCurrentNode', respData.data.EVT_IS_CURRENT_NODE == 'true');
					clientVM.displayMessage(
						this.localize('bulkOpInProgress'),
						this.localize(
							respData.data.EVT_TYPE, [
								respData.data.EVT_USERNAME,
								respData.data.EVT_RSVIEW_ID,
								this.opOnCurrentNode ? this.localize('currentNode') : this.localize('otherNode')
							]
						)
					);
					if (!respData.data || !this.opOnCurrentNode) {
						this.close();
						return;
					}
					this.adjustPollingMode(respData.data.EVT_TYPE);
					return;
				}
				this.set('wait', true);
				this.set('keepPolling', true);
				this.pollBulk();
			}
		});
	},
	exportCSV: function(force) {
		this.asyncAction({
			url: '/resolve/service/resolutionrouting/rule/csv/export/init/' + (this.parentVM.selectAll ? 'query' : 'id'),
			params: {
				force: force === true
			},
			submitReqError: 'submitExportReqError',
			onRequestSubmitted: function(respData) {
				this.set('currentTid', respData.data.TASK_ID)
				if (respData.data.EVT_TYPE) {
					this.set('previousOp', 'exportCSV');
					this.set('opOnCurrentNode', respData.data.EVT_IS_CURRENT_NODE == 'true');
					clientVM.displayMessage(
						this.localize('bulkOpInProgress'),
						this.localize(
							respData.data.EVT_TYPE, [
								respData.data.EVT_USERNAME,
								respData.data.EVT_RSVIEW_ID,
								this.opOnCurrentNode ? this.localize('currentNode') : this.localize('otherNode')
							]
						)
					);
					if (!respData.data || !this.opOnCurrentNode) {
						this.close();
						return;
					}
					this.adjustPollingMode(respData.data.EVT_TYPE);
					return
				}
				this.set('keepPolling', true);
				this.downloadCSV(this.currentTid);
			}
		}, true);
	},
	asyncAction: function(config, canRedo) {
		var url = config.url;
		var ids = [];
		this.set('keepPolling', true);
		Ext.each(this.parentVM.rulesSelections, function(rule) {
			ids.push(rule.get('id'));
		});
		var filters = this.parentVM.ruleStore.commonSearchFilter.getWhereFilter();
		var modulePath = '';
		if (this.parentVM.selectedModule)
			modulePath = this.parentVM.selectedModule.getPath('module', '.').substring(5) + (this.parentVM.selectedModule.isLeaf() ? '' : '.')
		if (modulePath && modulePath != '.')
			filters.push({
				field: 'module',
				type: 'auto',
				condition: 'equals',
				value: modulePath
			});
		var params = Ext.applyIf((this.parentVM.selectAll ? {
			filter: Ext.encode(filters)
		} : {
			ids: ids
		}), config.params);
		this.ajax({
			url: url,
			params: params,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					this.set('wait', false);
					clientVM.displayError(this.localize(config.submitReqError, respData.message));
					return;
				}
				config.onRequestSubmitted.call(this, respData);
			},
			failure: function(f) {
				clientVM.displayFailure(r);
			}
		});
	},
	progressIsVisible$: function() {
		return this.bulkType != this.TYPE_IMPORT || this.wait;
	},
	checkExportFinish: function(data) {
		var finished = false;
		Ext.each(data.status, function(st) {
			if (st.finished)
				finished = true;
		})
		if (data.hasGone)
			finished = true;
		return data.percent == 1.0 || finished;
	},
	pollBulk: function() {
		if (!this.keepPolling)
			return;
		this.ajax({
			url: '/resolve/service/resolutionrouting/rule/bulk/poll',
			params: {
				tid: this.currentTid
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('bulkPollError', respData.message));
					return;
				}
				if (respData.data.percent != 1 && !respData.data.hasGone) {
					this.set('progress', respData.data.percent);
					this.set('taskStatus', respData.data.status || []);
					var me = this;
					setTimeout(function() {
						if (me.keepPolling)
							me.pollBulk();
					}, 1000);
					return;
				}
				this.set('progress', 1);
				this.set('taskStatus', respData.data.status || this.taskStatus);
				this.set('previousOp', '');
				if (this.bulkType != this.TYPE_EXPORT) {
					this.parentVM.set('rulesSelections', []);
					this.parentVM.ruleStore.load();
					this.parentVM.expandToNode(this.parentVM.selectedModule);
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	downloadCSV: function() {
		this.set('wait', true);
		this.pollBulk();
		downloadFile('/resolve/service/resolutionrouting/rule/csv/export/download?tid=' + this.currentTid);
	},
	exportCSVIsEnabled$: function() {
		return !this.wait;
	},
	browseIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT && !this.wait;
	},
	exportCSVIsVisible$: function() {
		return this.type != this.TYPE_IMPORT;
	},
	dropAreaIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT && !this.wait;
	},
	close: function() {
		this.set('keepPolling', false);
		this.doClose();
	},
	taskStatus: [],
	statusDisplayIsVisible$: function() {
		return this.taskStatus && this.taskStatus.length > 1;
	},
	uploadError: function(resp) {
		var respData = Ext.decode(resp.response);
		clientVM.displayError(respData.message || this.localize('fileUploadError', resp.message));
		this.set('keepPolling', true);
		this.pollBulk();
	},
	abort: function() {
		this.abortBulk(this.currentTid);
	},
	abortBulk: function(tid) {
		this.ajax({
			url: '/resolve/service/resolutionrouting/rule/bulk/abort',
			params: {
				tid: tid
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('cannotAbortProcess', respData.message));
					return;
				}
				this.set('currentTid', '');
				this.set('keepPolling', false);
				this.set('previousOp', '');
				clientVM.displaySuccess(this.localize('abortSuccess'));
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	abortIsVisible$: function() {
		return this.wait && this.opOnCurrentNode;
	},
	overrideLock: function() {
		this[this.previousOp](true);
	},
	overrideLockIsVisible$: function() {
		return this.wait && this.bulkType != this.TYPE_IMPORT && this.previousOp;
	},
	backToImport: function() {
		this.set('bulkType', this.TYPE_IMPORT);
		this.set('wait', false);
		this.set('keepPolling', false);
		this.set('currentTid', '');
		this.set('previousOp', '');
	},
	backToImportIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT && this.wait && this.previousOp;
	}
});
*/

glu.defModel('RS.resolutionrouting.MappingDefinition', {
	store: {
		mtype: 'store',
		fields: ['id', 'name', 'value', {
			name: 'valid',
			value: true
		}]
	},
	fieldIsValid: true,
	fieldIsEmpty : false,
	init: function() {
		this.store.on('datachanged', function(store) {
			this.verifyFields();
		}, this);
	},
	resetForm: function() {
		this.store.removeAll();
	},
	verifyFields: function() {
		this.set('fieldIsValid', true);
		this.set('fieldIsEmpty', false);
		this.store.each(function(r) {
			if (!r.get('value'))
				this.set('fieldIsEmpty', true);
		}, this);
	},
	loadDefinition: function(orderedFields) {
		this.store.removeAll();
		this.store.add(orderedFields);
	},
	fieldsIsEnabled: false,
	fieldsSelections: [],
	reloadSchemaField: function() {
		this.parentVM.confirmReloadMappingFields();
	},
	dump: function() {
		var ridFields = [];
		this.store.each(function(item) {
			var data = Ext.apply({}, item.data);
			delete data.valid;
			ridFields.push(data);
		}, this);
		this.parentVM.set('ridFields', ridFields);
		this.cancel();
	},
	addField: function() {
		var max = 0;
		this.store.each(function(field) {
			if (/^New Field\d*/g.test(field.get('name'))) {
				var strIdx = field.get('name').substring('New Field'.length);
				var idx = 0;
				if (strIdx != '')
					idx = parseInt(strIdx);
				if (idx > max)
					max = idx;
			}
		});
		this.store.add({
			field: 'New Field' + (max + 1),
			value: 'New Value'
		});
	},
	removeFields: function() {
		this.store.remove(this.fieldsSelections);
	},
	removeFieldsIsEnabled$: function() {
		return this.fieldsSelections.length > 0;
	},
	cancel: function() {
		this.doClose();
	}
});
glu.defModel('RS.resolutionrouting.Rule', {
	fields: ['id',
		'schema',
		'automation',
		'wiki',
		'runbook',
		'module',
		'eventId', {
			name: 'active',
			type: 'bool'
		}, {
			name: 'newWorksheetOnly',
			type: 'bool'
		}
	],	
	ridFields: [],
	foo : '',
	moduleIsValid$: function() {
		if (/^\s*$/.test(this.module))
			return this.localize('invalidModule');
		var splits = this.module.split('\.');
		var current = '';
		for (var i = 0; i < splits.length; i++)
			if (/^[_a-zA-Z0-9]+$/.test(splits[i]) || /^[_a-zA-Z0-9]+[ _a-zA-Z0-9]*[_a-zA-Z0-9]+$/.test(splits[i]))
				return true;
		return this.localize('invalidModule');
	},
	wikiIsValid$: function() {
		return this.createInvestigationRecord || !/^\s*$/.test(this.runbook) || !/^\s*$/.test(this.wiki) || !/^\s*$/.test(this.automation) ? true : this.localize('invalidWiki');
	},
	runbookIsValid$: function() {
		return this.createInvestigationRecord || !/^\s*$/.test(this.runbook) || !/^\s*$/.test(this.wiki) || !/^\s*$/.test(this.automation) ? true : this.localize('invalidRunbook');
	},
	automationIsValid$: function() {
		return this.createInvestigationRecord || !/^\s*$/.test(this.runbook) || !/^\s*$/.test(this.wiki) || !/^\s*$/.test(this.automation) ? true : this.localize('invalidAutomation');
	},
	newWorksheetOnlyIsEnabled$: function() {
		return this.automationIsValid && !/^\s*$/.test(this.automation);
	},
	// eventIdIsValid$: function() {
	// 	return /^\s*$/.test(this.eventId) || !/^\s*$/.test(this.automation) || !/^s*$/.test(this.runbook) ? true : this.localize('invalidEventId');
	// },
	// eventIdIsEnabled$: function() {
	// 	return this.eventIdIsEnabled;
	// },
	isSchemaExist: false,
	schemaIsValid$: function() {
		return this.isSchemaExist ? true : this.localize('invalidSchema');
	},
	isSchemaExistChanged$: function() {
		this.mappingDefinition.set('fieldsIsEnabled', this.isSchemaExist);
	},
	mappingFieldIsValid: false,
	mappingDefinition: {
		mtype: 'RS.resolutionrouting.MappingDefinition'
	},
	moduleStore: {
		mtype: 'store',
		fields: [{
			name: 'name',
			convert: function(value, r) {
				return r.raw
			}
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/resolutionrouting/rule/module/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	schemaStore: {
		mtype: 'store',
		fields: ['id', 'name'],
		remoteSort: true,
		sorters: [{
			property: 'order',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/resolutionrouting/schema/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	wait: false,	
	init: function() {	
		this.moduleStore.on('beforeload', function(store, op) {
			op.params = op.params || {};
			Ext.applyIf(op.params, {
				filter: Ext.encode([{
					field: 'module',
					type: 'auto',
					condition: 'contains',
					value: this.module
				}])
			});
		}, this);
		this.schemaStore.on('beforeload', function(store, op) {
			var record = store.findRecord('id', this.schema);
			var queryStr = record ? record.get('name') : "";		
			Ext.applyIf(op.params, {
				filter: Ext.encode([{
					field: 'name',
					type: 'auto',
					condition: 'contains',
					value: (op.params || {}).query || queryStr
				}])
			})
		}, this);
		this.schemaStore.on('datachanged', function() {
			if (!this.schema)
				return;
			this.checkSchema();
		}, this);
		this.set('active', true);
		this.set('newWorksheetOnly', true);
		this.schemaStore.load({
			scope: this,
			callback: function() {
				if (this.id)
					this.loadRule();
			}
		});
		var me = this;
		this.mappingDefinition.on('fieldIsValidChanged', function() {
			me.set('mappingFieldIsValid', this.fieldIsValid);
		})
		this.ownerStore.load();

		this.ajax({
			url : '/resolve/service/sysproperties/getSystemPropertyByName',
			params : {
				name : 'app.security.types'
			},
			method : 'POST',
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				if(response.success && response.data){
					this.investigationTypeStore.loadData(response.data.uvalue.split(','));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	schemaId: '',
	checkSchema: function() {
		//this.set('schemaId', '');
		var isSchemaExist = (this.schemaStore.findBy(function(s) {
			if (this.schema == s.get('name') || this.schema == s.get('id')) {
				this.set('schemaId', s.get('id'));
				return true;
			}
		}, this) != -1)

		if (!isSchemaExist && this.schema && this.schemaId) {
			this.checkSchemaById(this.schemaId);
		} else {
			this.set('isSchemaExist', isSchemaExist);
		}
	},
	checkSchemaById: function(id) {
		this.ajax({
			url: '/resolve/service/resolutionrouting/schema/get',
			params: {
				id: id
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					this.set('isSchemaExist', false);
					clientVM.displayError(this.localize('getSchemaError', respData.message));
					return;
				}

				this.set('schemaId', id);
				this.set('isSchemaExist', true);
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	displaySchema$: function() {
		return this.schema && this.schema.name || '';
	},
	alignMappingFieldsWithSchema: function(ridFields, schemaFields) {
		var ordered = [];
		Ext.each(schemaFields, function(schemaField) {
			var found = null;
			Ext.each(ridFields, function(ridField) {
				if (ridField.name == schemaField.name)
					found = ridField;
			}, this);
			if (found == null) {
				clientVM.displayError(this.localize('schemaMappingInsistent'));
				return false;
			}
			ordered.push(found);
		}, this);
		return ordered;
	},
	fromJson: function(json) {
		this.loadData(json);
		this.set('updatingSchema', true);
		this.set('schema', json.schema && json.schema.name || '');
		this.set('schemaId', json.schema && json.schema.id || '');
		this.set('updatingSchema', false);
		this.schemaStore.load({
			callback: function() {
				if (this.schema) {
					var schemaData = this.schemaStore.findRecord('id', json.schema.id);
					var schemaFields = [];
					if (schemaData) {
						schemaFields = Ext.decode(schemaData.raw.jsonFields);
					} else {
						schemaFields = Ext.decode(json.schema.jsonFields);
					}
					this.set('ridFields', this.alignMappingFieldsWithSchema(json.ridFields, schemaFields));
				}
				this.mappingDefinition.loadDefinition(this.ridFields);
			},
			scope: this
		});

		this.set('createInvestigationRecord', json.sir);
		this.set('investigationTitle', json.sirTitle);
		this.set('owner', json.sirOwner);
		this.set('investigationType', json.sirType);
		this.set('playbook', json.sirPlaybook);
		this.set('severity', json.sirSeverity);
		this.set('sirSource', json.sirSource);
	},
	gatherData: function() {
		this.mappingDefinition.dump();
		var data = this.asObject();
		data['schema'] = {
			id: this.schemaId
		};
		data['ridFields'] = this.ridFields;

		//For SIR
		if(this.createInvestigationRecord){
			data = Ext.apply(data,{
				sir : true,
				sirSource : this.sirSource,
				sirTitle : this.investigationTitle,
				sirOwner : this.owner,
				sirType : this.investigationType,
				sirPlaybook : this.playbook,
				sirSeverity : this.severity
			})
		}
		return data;
	},
	updateFields: function() {
		var schema = this.schemaStore.getById(this.schema);
		var metaFields = [];
		try {
			Ext.each(Ext.decode(schema.raw.jsonFields), function(field) {
				metaFields.push(Ext.apply({
					valid: true
				}, field));
			});
			metaFields.sort(function(a, b) {
				return a.order > b.order ? 1 : -1;
			});
			this.mappingDefinition.loadDefinition(metaFields);
		} catch (e) {}
	},
	updatingSchema: false,
	confirmReloadMappingFields: function(previous) {
		this.message({
			title: this.localize('schemaChangeTitle'),
			msg: this.localize('schemaChangeMsg'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('discardCurrentFields'),
				no: this.localize('cancel')
			},
			fn: function(btn) {
				if (btn != 'yes') {
					this.set('updatingSchema', true);
					this.set('schema', previous);
					this.set('updatingSchema', false);
					return;
				}
				this.updateFields();
			}
		});
	},
	when_schema_changed: {
		on: ['schemaChanged'],
		action: function(current, previous) {
			this.checkSchema();
			if (!this.isSchemaExist)
				return;
			if (this.updatingSchema || this.wait)
				return;
			if (this.mappingDefinition.store.getCount() != 0)
				this.confirmReloadMappingFields(previous);
			else
				this.updateFields();
		}
	},
	loadRule: function() {
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/resolutionrouting/rule/get',
			params: {
				id: this.id
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('getRuleError', respData.message));
					return;
				}
				this.fromJson(respData.data);
			},
			failure: function(r) {
				this.set('updatingSchema', false);
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},
	defineMapping: function() {
		this.open({
			mtype: 'RS.resolutionrouting.MappingDefinition'
		});
	},
	openPageSearch: function() {
		this.open({
			mtype : 'RS.common.ContentPicker',
			contentType : 'page',
			dumper : {
				dump : function(selection) {
					this.set('wiki', selection.get('ufullname'));
				},
				scope : this
			}
		})
	},
	openAutomationSearch: function(fieldName) {
		this.open({
			mtype : 'RS.common.ContentPicker',
			contentType : 'automation',
			dumper : {
				dump : function(selection) {
					this.set(fieldName, selection.get('ufullname'));
				},
				scope : this
			}
		})
	},
	openPlaybookSearch : function(){
		this.open({
			mtype : 'RS.common.ContentPicker',
			contentType : 'playbook',
			dumper : {
				dump : function(selection) {
					this.set('playbook', selection.get('ufullname'));
				},
				scope : this
			}
		})
	},
	dumper: function() {},
	newRule: function() {
		this.mappingDefinition.resetForm();
		this.loadData({
			module: '',
			schema: '',
			wiki: '',
			automation: '',
			runbook: '',
			eventId: '',
			active: true
		});
		this.set('id', '');
	},
	save: function() {
		if(!this.isValid || !this.mappingDefinition.fieldIsValid || this.mappingDefinition.fieldIsEmpty){
			this.set('isPristine', false);
			return ;
		}
		var data = this.gatherData();
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/resolutionrouting/rule/save',
			jsonData: data,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('saveRuleError', respData.message));
					return;
				}
				this.fromJson(respData.data);
				this.dumper(respData, !!this.id);
				clientVM.displaySuccess(this.localize('saveRuleSuccess'));
				this.doClose();
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},
	saveIsEnabled$: function() {
		return !this.wait && this.isValid && this.mappingDefinition.fieldIsValid && !this.mappingDefinition.fieldIsEmpty;
	},
	getCurrentIdx: function(id) {
		if (!id)
			return -1;
		var r = this.parentVM.ruleStore.getById(this.id);
		if (!r)
			return -1;
		return r.store.indexOf(r);
	},
	previousRule: function() {
		var idx = this.getCurrentIdx(this.id);
		var previousRule = this.parentVM.ruleStore.getAt(idx - 1);
		this.set('id', previousRule.get('id'));
		this.loadRule();
	},
	previousRuleIsEnabled$: function() {
		var idx = this.getCurrentIdx(this.id);
		return idx > 0;
	},
	nextRule: function() {
		var idx = 0;
		if (this.id)
			idx = this.getCurrentIdx(this.id);
		var nextRule = this.parentVM.ruleStore.getAt(idx + 1);
		this.set('id', nextRule.get('id'));
		this.loadRule();
	},
	/*nextRuleIsEnabled$: function() {
		var idx = this.getCurrentIdx(this.id);
		return !idx || idx < this.parentVM.ruleStore.getCount() - 1;
	},*/
	close: function() {
		this.doClose();
	},
	jumpToSchema: function() {
		var schema = this.schemaStore.getById(this.schema);
		if(!schema)//Schema sometimes use name sometimes use ID !!!!!!!
			schema = this.schemaStore.findRecord('name', this.schema);
		clientVM.handleNavigation({
			modelName: 'RS.resolutionrouting.Schema',
			params: {
				id: schema ? schema.get('id') : this.schemaId
			},
			target: '_blank'
		});
	},
	jumpToSchemaIsVisible$: function() {
		return this.schemaIsValid === true;
	},

	//SIR
	createInvestigationRecord : false,
	investigationTitle : '',
	owner : '',
	investigationType : '',
	playbook : '',
	sirSource : '',
	severity : 'Critical',
	investigationTypeStore : {
		mtype : 'store',
		fields : [{
			name : 'name',
			convert : function(value, record){
				return record.raw;
			}
		}]
	},
	ownerStore : {
		mtype : 'store',
		fields : ['userId'],
		proxy : {
			url : '/resolve/service/playbook/securityIncident/getSecurityGroup',
			type: 'ajax',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	severityStore : {
		mtype : 'store',
		fields : [{
			name : 'name',
			convert : function(value, record){
				return record.raw;
			}
		}],
		data : ['Critical', 'High', 'Medium', 'Low']
	},
	/*investigationTitleIsValid$ : function(){
		return !this.createInvestigationRecord || this.investigationTitle ? true : this.localize('invalidField');
	},
	investigationTypeIsValid$ : function(){
		return !this.createInvestigationRecord || this.investigationType ? true : this.localize('invalidField');
	},
	sirSourceIsValid$ : function(){
		return !this.createInvestigationRecord || this.sirSource ? true : this.localize('invalidField');
	}*/
});
glu.defModel('RS.resolutionrouting.Rules', {

	mock: false,

	treeStore: {
		mtype: 'treestore',
		fields: ['module'],
		root: {
			module: 'All'
		}
	},
	schemaStore: {
		mtype: 'store',
		fields: ['id', 'name', 'order'],
		remoteSort: true,
		sorters: [{
			property: 'order',
			order: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/resolutionrouting/schema/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	ruleStore: {
		mtype: 'store',
		fields: ['rid', {
			name: 'schema',
			type: 'object'
		}, {
			name: 'schema.name',
			convert: function(v, r) {
				//v will not be empty when edited
				return v || (r.raw.schema && r.raw.schema.name ? r.raw.schema.name : 'UNDEFINED');
			}
		}, 'wiki', 'runbook', 'automation', 'eventId', 'module', {
			name: 'active',
			type: 'boolean'
		}].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/resolutionrouting/rule/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	activate: function() {
		window.document.title = this.localize('windowTitle');
		this.treeStore.load();
		this.ruleStore.load();
	},
	rulesSelections: [],
	selectedModule: null,
	reqCount: 0,
	init: function() {
		if (this.mock) this.backend = RS.resolutionrouting.createMockBackend(true);

		this.ruleStore.on('beforeload', function(store, op) {
			this.set('reqCount', this.reqCount + 1);
			op.params = op.params || {};
			if (this.selectedModule && this.selectedModule.get('module') != 'All') {
				Ext.apply(op.params, {
					filter: Ext.encode([{
						field: 'module',
						type: 'auto',
						condition: (this.selectedModule.isLeaf() ? 'equals' : 'startsWith'),
						value: this.selectedModule.getPath('module', '.').substring(5) //+ (this.selectedModule.isLeaf() ? '' : '.')
					}, {
						field: 'schema.sysOrg',
						type: 'auto',
						condition: 'equals',
						value: clientVM.orgId || 'nil'
					}])
				});
			} else {
				Ext.apply(op.params, {
					filter: Ext.encode([{
						field: 'schema.sysOrg',
						type: 'auto',
						condition: 'equals',
						value: clientVM.orgId || 'nil'
					}])
				});
			}
		}, this);
		this.set('selectedModule', this.treeStore.getRootNode());
		this.ruleStore.on('load', function() {
			this.set('reqCount', this.reqCount - 1);
		}, this);
		this.ruleStore.on('commonSearchFilterInited', function(commonFilter) {
			this.ruleStore.commonSearchFilter = commonFilter;
		}, this);
		this.treeStore.on('expand', function(node) {
			if (!this.expandingToNode)
				this.expandModuleNode(node);
		}, this);
		this.treeStore.getRootNode().expand();
		clientVM.on('orgchange', this.orgChange.bind(this));
	},
	when_selected_module_changed: {
		on: ['selectedModuleChanged'],
		action: function() {
			if (this.selectedModule) {
				this.set('rulesSelections', []);
				this.ruleStore.load();
			}
		}
	},
	expandModuleNode: function(node, nextAction) {
		this.set('reqCount', this.reqCount + 1);
		var path = node.getPath('module', '.').substring(5);
		params = {
			filter: [{
				field: 'schema.sysOrg',
				type: 'auto',
				condition: 'equals',
				value: clientVM.orgId || 'nil'
			}]
		};
		if (path) {
			params.filter.push({
				field: 'module',
				type: 'auto',
				condition: 'startsWith',
				value: path
			});
		}
		params.filter = Ext.encode(params.filter);
		this.ajax({
			url: '/resolve/service/resolutionrouting/rule/module/expand',
			params: params,
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('expandModuleError', respData.message));
					return;
				}
				node.removeAll();
				Ext.Object.each(respData.data, function(k, v) {
					node.appendChild({
						id: Ext.data.IdGenerator.get('uuid').generate(),
						module: k,
						leaf: v
					});
				}, this)
				if (nextAction) nextAction.call(this);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
		});
	},
	deploy: function() {
		// var records = [];
		// this.ruleStore.each(function(r) {
		// 	if (!r.dirty)
		// 		return
		// 	var schema = this.schemaStore.findRecord('name', r.get('schema.name'));
		// 	if (!schema) {
		// 		clientVM.displayError(this.localize('cannotFindSchemaInStore'));
		// 		return;
		// 	}
		// 	r.data.schema = {
		// 		id: schema.get('id')
		// 	};
		// 	delete r.data['schema.name'];
		// 	records.push(r.data);
		// }, this);
		this.set('reqCount', this.reqCount + 1);
		this.ajax({
			url: '/resolve/service/resolutionrouting/rule/deploy',
			// jsonData: records,
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('saveRuleError', respData.message));
					return;
				}
				clientVM.displaySuccess(this.localize('deploySuccess'));
				this.ruleStore.load();
				this.expandToNode(this.selectedModule);
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
		})
	},
	saveRulesIsEnabled$: function() {
		return this.reqCount == 0;
	},
	addRule: function() {
		var me = this;
		var modulePath = '';
		if (this.selectedModule)
			modulePath = this.selectedModule.getPath('module', '.').substring(5) + (this.selectedModule.isLeaf() ? '' : '.')
		if (modulePath == '.')
			modulePath = '';
		this.open({
			mtype: 'RS.resolutionrouting.Rule',
			module: modulePath,
			dumper: function(respData) {
				var savedRecord = respData.data;
				var record = me.ruleStore.getById(savedRecord.id);
				if (!record) {
					me.ruleStore.add(savedRecord);
					me.expandToNode(me.selectedModule);
					return;
				}
				Ext.Object.each(record.data, function(k, v) {
					record.set(k, savedRecord[k]);
				});
				record.commit();
				me.expandToNode(me.selectedModule);
			}
		});
	},
	addRuleIsEnabled$: function() {
		return this.reqCount == 0;
	},
	editRule: function(id) {
		var me = this;
		this.open({
			mtype: 'RS.resolutionrouting.Rule',
			id: id,
			dumper: function(respData) {
				var savedRecord = respData.data;
				var record = me.ruleStore.getById(savedRecord.id);
				if (!record) {
					me.ruleStore.add(savedRecord);
					return;
				}
				Ext.Object.each(record.data, function(k, v) {
					record.set(k, savedRecord[k]);
				});
				record.commit();
				me.expandToNode(me.selectedModule);
			}
		});
	},
	deleteRulesById: function() {
		var ids = [];
		Ext.each(this.rulesSelections, function(r) {
			ids.push(r.get('id'));
		}, this);
		this.message({
			title: this.localize('deleteRules'),
			msg: this.localize(this.rulesSelections.length > 0 ? 'deleteRulesMsg' : 'deleteRuleMsg'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteRules'),
				no: this.localize('cancel')
			},
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.set('reqCount', this.reqCount + 1);
				this.ajax({
					url: '/resolve/service/resolutionrouting/rule/delete/id',
					params: {
						ids: ids
					},
					scope: this,
					success: function(resp) {
						var respData = RS.common.parsePayload(resp);
						if (!respData.success) {
							clientVM.displayError(this.localize('deleteRulesError', respData.message));
							return;
						}
						this.ruleStore.load();
						clientVM.displaySuccess(this.localize('deleteRuleSuccess'));
						this.expandToNode(this.selectedModule);
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					},
					callback: function() {
						this.set('reqCount', this.reqCount - 1);
					}
				});
			}
		})
	},
	deleteRulesByQuery: function() {
		this.message({
			title: this.localize('deleteRules'),
			msg: this.localize('deleteRulesThroughoutPages'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteRules'),
				no: this.localize('cancel')
			},
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.set('reqCount', this.reqCount + 1);
				this.ajax({
					url: '/resolve/service/resolutionrouting/rule/delete/query',
					params: {
						filter: Ext.encode(this.ruleStore.commonSearchFilter.getWhereFilter())
					},
					scope: this,
					success: function(resp) {
						var respData = RS.common.parsePayload(resp);
						if (!respData.success) {
							clientVM.displayError(this.localize('deleteRulesError', respData.message));
							return;
						}
						this.ruleStore.load();
						clientVM.displaySuccess(this.localize('deleteRuleSuccess'));
						this.expandToNode(this.selectedModule);
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					},
					callback: function() {
						this.set('reqCount', this.reqCount - 1);
					}
				});
			}
		})

	},
	deleteRules: function() {
		if (!this.selectAll)
			this.deleteRulesById();
		else
			this.deleteRulesByQuery();
	},
	deleteRulesIsEnabled$: function() {
		return this.reqCount == 0 && this.rulesSelections.length > 0;
	},
	expandingToNode: false,
	expandToNode: function(node) {
		node = node || this.treeStore.getRootNode();
		this.set('expandingToNode', true);
		var paths = node.getPath('module').substring(5).split(/\//);
		var current = 0;
		this.treeStore.getRootNode().removeAll();
		var me = this;

		function recursiveExpand(currentNode) {
			me.expandModuleNode(currentNode, function() {
				current++;
				if (current > paths.length - 1)
					return;
				var currentPath = paths.slice(0, current).join('\/');
				//can be more efficient
				var end = true;
				me.treeStore.getRootNode().eachChild(function(node) {
					if (currentPath == node.getPath('module').substring(5)) {
						end = false;
						node.expand();
						recursiveExpand(node);
					}
				});
				if (end)
					me.set('expandingToNode', false);
			});
		}

		recursiveExpand(this.treeStore.getRootNode());
	},
	importCSV: function() {
		this.open({
			mtype: 'RS.resolutionrouting.Bulk',
			bulkType: 0
		});
	},
	// TYPE_IMPORT: 0,
	// TYPE_EXPORT: 1,
	// TYPE_ACTIVATE: 2,
	// TYPE_DEACTIVATE: 3,
	exportCSV: function() {
		this.open({
			mtype: 'RS.resolutionrouting.Bulk',
			bulkType: 1
		});
	},
	exportCSVIsEnabled$: function() {
		return this.rulesSelections.length > 0;
	},
	recordEdit: function(record) {
		var data = Ext.apply({}, record.data);
		delete data['schema.name'];
		this.set('reqCount', this.reqCount + 1);
		this.ajax({
			url: '/resolve/service/resolutionrouting/rule/save',
			jsonData: data,
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp)
				if (!respData.success) {
					clientVM.displayError(this.localize('saveRuleError', respData.message));
					return;
				}
				record.commit();
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
		})
	},
	selectAll: false,
	pageSelections: [],
	selectAllAcrossPages: function(filters) {
		this.set('pageSelections', filters);
		this.set('selectAll', true);
	},
	selectionChanged: function() {
		this.set('selectAll', false);
		this.set('pageSelections', []);
	},
	activateMapping: function() {
		if (this.selectAll)
			this.toggleMappingActivationsByQuery(true);
		else
			this.toggleMappingActivationsById(true);
	},
	activateMappingIsEnabled$: function() {
		return this.rulesSelections.length > 0;
	},
	deactivateMapping: function() {
		if (this.selectAll)
			this.toggleMappingActivationsByQuery(false);
		else
			this.toggleMappingActivationsById(false);
	},
	deactivateMappingIsEnabled$: function() {
		return this.rulesSelections.length > 0;
	},
	toggleMappingActivationsById: function(activate) {
		this.open({
			mtype: 'RS.resolutionrouting.Bulk',
			bulkType: activate ? 2 : 3
		});
	},
	toggleMappingActivationsByQuery: function(activate) {
		this.open({
			mtype: 'RS.resolutionrouting.Bulk',
			bulkType: activate ? 2 : 3
		});
	},

	orgChange: function() {
		this.treeStore.load();
		this.ruleStore.load();
	},

	beforeDestroyComponent : function() {
		clientVM.removeListener('orgchange', this.orgChange);
	}
});
glu.defModel('RS.resolutionrouting.Schema', {
	wait: false,
	fields: ['id', 'name', 'description', {
		name: 'order',
		type: 'int'
	}].concat(RS.common.grid.getSysFields()),
	accessRights: {
		mtype: 'RS.common.AccessRights',
		rightsShown: ['read', 'write'],
		title: '',
		defaultRolesIsVisible: false,
		local: true
	},
	invalidTextIsHidden : true,
	invalidFieldsMsg: '',
	orgStore : {
		mtype : 'store',
		fields : ['id', 'uname']
	},
	maxOrder: 0,
	orderIsValid$: function() {
		return this.order >= 0 && this.order <= this.maxOrder;
	},
	init: function() {
		var names = {};
		this.set('invalidFieldsMsg', this.localize('noFieldDefinedMsg'));
		this.fieldStore.on('update', function(store, record, operation, modifiedFieldNames, eOpts) {
			if (this.fieldStore.getCount() == 0) {
				this.set('invalidFieldsMsg', this.localize('noFieldDefinedMsg'));
				return;
			}
			this.set('invalidFieldsMsg', '');
			names = {};
			this.fieldStore.each(function(field) {
				if (names[field.get('name')]) {
					this.set('invalidFieldsMsg', this.localize('duplicateFieldMsg', field.get('name')));
					return;
				}
				names[field.get('name')] = true;
			}, this);
		}, this);

		this.fieldStore.on('datachanged', function() {
			if (this.fieldStore.getCount() == 0) {
				this.set('invalidFieldsMsg', this.localize('noFieldDefinedMsg'));
				return;
			}
			this.set('invalidFieldsMsg', '');
			names = {};
			this.fieldStore.each(function(field) {
				if (names[field.get('name')]) {
					this.set('invalidFieldsMsg', this.localize('duplicateFieldMsg', field.get('name')));
					return;
				}
				names[field.get('name')] = true;
			}, this);
		}, this);
	},
	nameIsValid$: function() {
		return /^[_a-zA-Z0-9]+$/.test(this.name) ? true : this.localize('invalidName');
	},
	fieldStore: {
		mtype: 'store',
		fields: ['name', 'source', 'regex']
	},
	fieldsSelections: [],
	queueStore: {
		mtype: 'store',
		fields: ['queue']
	},
	queuesSelections: [],
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},
	defaultData: {
		id: '',
		name: '',
		order: null,
		description: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: ''
	},
	resetForm: function() {
		this.loadData(this.defaultData);
		this.fieldStore.removeAll();
		this.queueStore.removeAll();
		this.set('isPristine', true);
		this.set('invalidTextIsHidden', true);
	},
	activate: function(screens, params) {
		this.resetForm();
		this.set('id', params && params.id || '');
		if (this.id) {
			this.loadSchema();
			this.getNextOrder();
		} else {
			this.getNextOrder(true);
		}
		this.orgStore.loadData(user.orgs || []);
	},
	getNextOrder: function(isNew) {
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/resolutionrouting/schema/order/next',
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('getNextOrderError'), respData.message);
					return;
				}

				if (isNew) {
					this.set('order', respData.data);
					this.set('maxOrder', parseInt(this.order));
				} else {
					this.set('maxOrder', parseInt(respData.data) - 1);
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},
	loadSchema: function() {
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/resolutionrouting/schema/get',
			params: {
				id: this.id
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('getSchemaError', respData.message));
					return;
				}
				this.fromJson(respData.data);
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},
	refresh: function() {
		if (this.id)
			this.loadSchema();
	},
	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.resolutionrouting.Schemas'
		});
	},
	save: function(exitAfterSave) {
		if(!this.isValid || this.invalidFieldsMsg){
			this.set('isPristine', false);
			this.set('invalidTextIsHidden', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveSchema, this, [exitAfterSave])
		this.task.delay(500)
	},
	saveIsEnabled$: function() {
		return !this.wait && this.isValid;
	},
	saveAndExit: function() {
		if(!this.isValid || this.invalidFieldsMsg){
			this.set('isPristine', false);
			this.set('invalidTextIsHidden', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.save(true)
	},
	toJson: function() {
		var fields = [];
		this.fieldStore.each(function(f, idx) {
			fields.push(Ext.apply(f.data, {
				order: idx
			}));
		});

		var queues = [];
		this.queueStore.each(function(g) {
			queues.push(g.get('queue'));
		});
		var json = this.asObject();
		json.jsonFields = Ext.encode(fields);
		json.gatewayQueues = Ext.encode(queues);
		return json;
	},
	fromJson: function(json) {
		this.resetForm();
		this.loadData(json);
		try {
			Ext.each(Ext.decode(json.jsonFields), function(field) {
				this.fieldStore.add(field);
			}, this);
		} catch (e) {}

		this.fieldStore.sort({
			property: 'order',
			direction: 'ASC'
		});
		try {
			Ext.each(Ext.decode(json.gatewayQueues), function(queue) {
				this.queueStore.add({
					queue: queue
				});
			}, this);
		} catch (e) {}
	},
	saveSchema: function(exitAfterSave) {
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/resolutionrouting/schema/save',
			jsonData: this.toJson(),
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('saveSchemaError', respData.message));
					return;
				}
				clientVM.displaySuccess(this.localize('saveSchemaSuccess'));
				if (exitAfterSave === true) {
					clientVM.handleNavigation({
						modelName: 'RS.resolutionrouting.Schemas'
					});
					return;
				}
				if (!this.id) {
					clientVM.handleNavigation({
						modelName: 'RS.resolutionrouting.Schema',
						params: {
							id: respData.data.id
						}
					});
				}
				this.fromJson(respData.data);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},
	addField: function() {
		var max = 0;
		this.fieldStore.each(function(field) {
			if (/^New Field\d*/g.test(field.get('name'))) {
				var strIdx = field.get('name').substring('New Field'.length);
				var idx = 0;
				if (strIdx != '')
					idx = parseInt(strIdx);
				if (idx > max)
					max = idx;
			}
		});
		this.fieldStore.add({
			name: 'New Field' + (max + 1),
			source: '',
			regex: ''
		});
	},
	fieldNameChanged: function(name) {
		var record = this.fieldStore.findRecord('name', name);
		if (!record || record.get('source'))
			return;
		record.set('source', name);
	},
	removeFields: function() {
		this.fieldStore.remove(this.fieldsSelections);
	},
	removeFieldsIsEnabled$: function() {
		return !this.wait && this.fieldsSelections.length > 0;
	},
	addGateway: function() {
		var me = this;
		this.open({
			mtype: 'RS.resolutionrouting.QueuePicker',
			dumper: function() {
				if (!this.queue || me.queueStore.find('queue', this.queue) != -1)
					return;
				me.queueStore.add({
					queue: this.queue
				});
			}
		});
	},
	removeGateways: function() {
		this.queueStore.remove(this.queuesSelections);
	},
	removeGatewaysIsEnabled$: function() {
		return !this.wait && this.queuesSelections.length > 0;
	},
	cancel: function() {
		this.doClose();
	}
});

glu.defModel('RS.resolutionrouting.QueuePicker', {
	queueStore: {
		mtype: 'store',
		fields: ['gateway', 'queue']
	},
	queue: '',
	queueIsValid$: function() {
		return !/^\s*$/.test(this.queue) ? true : this.localize('invalidQueueName');
	},
	loadQueues: function() {
		this.ajax({
			url: '/resolve/service/gateway/listQueues',
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					//clientVM.displayError(this.localize('listQueueError', respData.message));
					clientVM.displayError(respData.message);
					return;
				}
				Ext.Object.each(respData.data, function(k, v) {
					Ext.each(v, function(queue) {
						this.queueStore.add({
							gateway: k,
							queue: queue
						});
					}, this);
				}, this);

				if (this.queueStore.getCount() == 0)
					clientVM.displayMessage(this.localize('noGatewayFound'), this.localize('noGatewayFoundMsg'));
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	init: function() {
		this.loadQueues();
	},
	dumper: function() {},
	dump: function() {
		this.dumper();
		this.close();
	},
	dumpIsEnabled$: function() {
		return this.queueIsValid === true;
	},
	close: function() {
		this.doClose();
	}
});
glu.defModel('RS.resolutionrouting.Schemas', {
	store: {
		mtype: 'store',
		fields: ['name', 'description', {
			name: 'order',
			type: 'int'
		}, 'jsonFields', 'gatewayQueues', {
			name: 'order',
			type: 'int'
		}].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'tableData.order',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/resolutionrouting/schema/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	schemasSelections: [],
	wait: false,
	activate: function() {
		this.store.load();
	},
	init: function() {
		var me = this;
		if (this.mock) this.backend = RS.resolutionrouting.createMockBackend(true);

		this.store.on('load', function() {
		//	var respData = this.store.proxy.reader.rawData;
		//	if (!respData.success)
		//		clientVM.displayError(this.localize('listSchemaError', respData.message));
		}, this);
		this.store.on('beforeload', function(store, operation){
			operation.params = operation.params || {};
			var filter = operation.params.filter || [];
			filter.push({
				field: 'sysOrg',
				type: 'auto',
				condition: 'equals',
				value: clientVM.orgId || 'nil'
			});
			operation.params.filter = JSON.stringify(filter);
		});
		clientVM.on('orgchange', this.orgChange.bind(this));
	},
	createSchema: function() {
		clientVM.handleNavigation({
			modelName: 'RS.resolutionrouting.Schema'
		});
	},
	createIsEnabled$: function() {
		return !this.wait;
	},
	deleteSchemas: function() {
		var ids = [];
		Ext.each(this.schemasSelections, function(schema) {
			ids.push(schema.get('id'));
		}, this);
		this.message({
			title: this.localize('deleteSchemas'),
			msg: this.localize(this.schemasSelections.length > 1 ? 'deleteSchemasMsg' : 'deleteSchemaMsg'),
			buttonText: {
				yes: this.localize('deleteSchemas'),
				no: this.localize('cancel')
			},
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.ajax({
					url: '/resolve/service/resolutionrouting/schema/delete',
					params: {
						ids: ids
					},
					success: function(resp) {
						var respData = RS.common.parsePayload(resp);
						if (!respData.success) {
							clientVM.displayError(this.localize('deleteSchemaError', respData.message));
							return;
						}
						this.store.load();
						clientVM.displaySuccess(this.localize('deleteSchemaSuccess'));
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				});
			}
		})
	},
	deleteSchemasIsEnabled$: function() {
		return !this.wait && this.schemasSelections.length > 0;
	},
	editSchema: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.resolutionrouting.Schema',
			params: {
				id: id
			}
		});
	},
	redoOrder: function() {
		var records = [];
		var direction = this.store.sorters.get(0).direction;
		var pageSize = this.store.pageSize;
		var currentPage = this.store.currentPage;
		this.store.each(function(item, idx) {
			var ascIdx = (currentPage - 1) * 50 + idx;
			var descIdx = this.store.totalCount - ascIdx - 1;
			var theIdx = direction == 'ASC' ? ascIdx : descIdx;
			item.set('order', theIdx);
			item.commit();
			records.push(Ext.apply(item.raw, {
				order: theIdx
			}));
		}, this);
		this.ajax({
			url: '/resolve/service/resolutionrouting/schema/reorder',
			scope: this,
			jsonData: records,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('reorderError', respData.message));
					return;
				}
				this.store.reload();
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	orgChange: function() {
		this.store.load();
	},

	beforeDestroyComponent : function() {
		clientVM.removeListener('orgchange', this.orgChange);
	}
});
glu.defModel('RS.resolutionrouting.WikiPicker', {
	mock: false,
	title$: function() {
		return this.isRunbook ? this.localize('runbook') : this.localize('wiki');
	},
	runbookGrid: null,
	isRunbook: false,
	runbookTree: {
		mtype: 'treestore',
		fields: ['id', 'unamespace'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/nsadmin/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	moduleColumns: [],
	namespace: '',
	namespaceIsValid$: function() {
		if (/^\s*$/.test(this.namespace))
			return this.localize('invalidNamespaceConvention');
		if (this.builderOnly) {
			var splittedFullName = this.parentVM.parentVM.parentVM.name.split('\.');
			if (splittedFullName[0] == this.namespace && splittedFullName[1] == this.name)
				return this.localize('cannotUseCurrentWiki');
		}
		return true;
	},
	name: '',
	nameIsValid$: function() {
		if (/^\s*$/.test(this.name))
			return this.localize('invalidNameConvention');
		if (this.builderOnly) {
			var splittedFullName = this.parentVM.parentVM.parentVM.name.split('\.');
			if (splittedFullName[0] == this.namespace && splittedFullName[1] == this.name)
				return this.localize('cannotUseCurrentWiki');
		}
		return true;
	},
	appendExtraParams: function() {
		this.runbookGrid.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {};
			var filter = [];
			if (this.selectedNamespace && this.selectedNamespace.toLowerCase() != 'all')
				filter.push({
					field: 'unamespace',
					type: 'auto',
					condition: 'equals',
					value: this.selectedNamespace
				});
			if (this.isRunbook) {
				filter.push({
					field: "uhasActiveModel",
					type: "bool",
					condition: "equals",
					value: true
				});
			}
			Ext.apply(operation.params, {
				filter: Ext.encode(filter)
			})
		}, this);
	},

	init: function() {
		if (this.mock)
			this.backend = RS.actiontask.createMockBackend(true)
		var store = Ext.create('Ext.data.Store', {
			mtype: 'store',
			sorters: ['ufullname'],
			fields: ['id', 'ufullname', 'usummary', 'uresolutionBuilderId', 'uisRoot'].concat(RS.common.grid.getSysFields()),
			remoteSort: true,
			proxy: {
				type: 'ajax',
				url: (!this.runbookOnly && !this.builderOnly) ? '/resolve/service/wikiadmin/list' : '/resolve/service/wikiadmin/listRunbooks',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});
		this.set('runbookGrid', store);
		this.appendExtraParams();
		this.set('title', this.title || this.localize('title'));

		this.set('moduleColumns', [{
			xtype: 'treecolumn',
			dataIndex: 'unamespace',
			text: this.localize('namespace'),
			flex: 1
		}])

		this.runbookTree.on('load', function(store, node, records) {
			Ext.Array.forEach(records || [], function(record) {
				record.set({
					leaf: true
				})
			})
		})

		this.runbookTree.setRootNode({
			unamespace: this.localize('all'),
			expanded: true
		})

		this.runbookGrid.load()
	},

	selected: null,
	selectedNamespace: '',

	searchDelimiter: '',

	when_selected_menu_node_id_changes_update_grid: {
		on: ['selectedNamespaceChanged'],
		action: function() {
			this.runbookGrid.load()
		}
	},

	menuPathTreeSelectionchange: function(selected, eOpts) {
		if (selected.length > 0) {
			var nodeId = selected[0].get('unamespace')
			this.set('selectedNamespace', nodeId)
		}
	},

	dumper: null,
	dump: function() {
		if (Ext.isFunction(this.dumper))
			this.dumper(this.selected);
		this.doClose()
	},
	dumpIsEnabled$: function() {
		return !!this.selected
	},
	cancel: function() {
		if (this.activeItem == 1)
			this.selectExistingRunbook()
		else
			this.doClose()
	},
	editRunbook: function(name) {
		if (this.runbookOnly)
			clientVM.handleNavigation({
				modelName: 'RS.wiki.Main',
				target: '_blank',
				params: {
					activeTab: (this.runbookOnly ? 2 : 0),
					name: name
				}
			})
		else if (this.builderOnly) {
			var idx = this.runbookGrid.findExact('ufullname', name);
			var r = this.runbookGrid.getAt(idx);
			clientVM.handleNavigation({
				modelName: 'RS.wiki.resolutionbuilder.AutomationMeta',
				target: '_blank',
				params: {
					id: r.get('uresolutionBuilderId')
				}
			});
		}
	}
});
glu.defView('RS.resolutionrouting.Bulk', {	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	items: [{
		xtype: 'progressbar',
		progress: '@{progress}',
		hidden: '@{!progressIsVisible}',
		toFixed: function(value, precision) {
			var precision = precision || 0,
				power = Math.pow(10, precision),
				absValue = Math.abs(Math.round(value * power)),
				result = (value < 0 ? '-' : '') + String(Math.floor(absValue / power));

			if (precision > 0) {
				var fraction = String(absValue % power),
					padding = new Array(Math.max(precision - fraction.length, 0) + 1).join('0');
				result += '.' + padding + fraction;
			}
			return result;
		},
		setProgress: function(progress) {
			this.progress = progress;
			if (this.progress)
				this.updateProgress(progress, this.toFixed(progress * 100, 2) + '%');
			else
				this.updateProgress(0, '');
		}
	}, {
		html: '@{busyText}'
	}, {
		xtype: 'fieldcontainer',
		layout: 'vbox',
		disabled: '@{fineUploaderTemplateHidden}',
		items: [{
			xtype: 'checkbox',
			name: 'override',
			hideLabel: true,
			boxLabel: '~~override~~'
		}, {
			xtype: 'checkbox',
			name: 'forceImport',
			hideLabel: true,		
			boxLabel: '~~forceImport~~'
		}]
	}, {
		itemId: 'fineUploaderTemplate',
		html: '@{fineUploaderTemplate}',
		height: '@{fineUploaderTemplateHeight}',
		hidden: '@{fineUploaderTemplateHidden}',
	}, {
		id: 'attachments_grid',
		hidden: true
	}, {
		hidden: '@{!statusDisplayIsVisible}',
		autoScroll: true,
		html: '@{processedStatus}',
		flex: 1
	}],

	asWindow: {
		title: '@{title}',
		modal: true,
		width: 600,
		height: 400,
		padding : 15,
		cls : 'rs-modal-popup',		
		closable: true,
		buttons: [{
			name: 'browse',
			itemId: 'browse',
			id: 'upload_button',
			cls : 'rs-med-btn rs-btn-dark',
			preventDefault: false,
			hidden: '@{!browseIsVisible}',
			disabled: '@{fineUploaderTemplateHidden}'
		},{
			name : 'abort',
			cls : 'rs-med-btn rs-btn-dark'
		},{
			name : 'overrideLock', 
			cls : 'rs-med-btn rs-btn-dark'
		},{	
			name : 'backToImport', 
			cls : 'rs-med-btn rs-btn-dark'
		},{
			name :'close',
			cls : 'rs-med-btn rs-btn-light'
		}]
	}
});

/*
glu.defView('RS.resolutionrouting.Bulk', {	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	override: '@{override}',
	forceImport: '@{forceImport}',
	setOverride: function(override) {
		this.uploader.multipart_params['override'] = override;
	},
	setForceImport: function(forceImport) {
		this.uploader.multipart_params['forceImport'] = forceImport;
	},
	getConfig: function() {
		return {
			autoStart: true,
			url: '/resolve/service/resolutionrouting/rule/csv/import/upload',
			filters: [{
				title: "CSV files",
				extensions: "csv"
			}],
			browse_button: this.getBrowseButton(),
			drop_element: this.getDropArea(),
			container: this.getContainer(),
			chunk_size: null,
			runtimes: "html5,flash,silverlight,html4",
			flash_swf_url: '/resolve/js/plupload/Moxie.swf',
			unique_names: true,
			multipart: true,
			multipart_params: {
				test: 'test'
			},
			multi_selection: false,
			required_features: null,
			ownerCt: this
		}
	},
	getBrowseButton: function() {
		return this.up().down('#browse').btnEl.dom.id;
	},
	getDropArea: function() {
		return this.down('#dropzone').getEl().dom.id;
	},
	getContainer: function() {
		var btn = this.up().down('#browse');
		return btn.ownerCt.id;
	},
	items: [{
		xtype: 'progressbar',
		progress: '@{progress}',
		hidden: '@{!progressIsVisible}',
		toFixed: function(value, precision) {
			var precision = precision || 0,
				power = Math.pow(10, precision),
				absValue = Math.abs(Math.round(value * power)),
				result = (value < 0 ? '-' : '') + String(Math.floor(absValue / power));

			if (precision > 0) {
				var fraction = String(absValue % power),
					padding = new Array(Math.max(precision - fraction.length, 0) + 1).join('0');
				result += '.' + padding + fraction;
			}
			return result;
		},
		setProgress: function(progress) {
			this.progress = progress;
			if (this.progress)
				this.updateProgress(progress, this.toFixed(progress * 100, 2) + '%');
			else
				this.updateProgress(0, '');
		}
	}, {
		html: '@{busyText}'
	}, {
		xtype: 'fieldcontainer',
		layout: 'vbox',
		items: [{
			xtype: 'checkbox',
			name: 'override',
			hideLabel: true,
			boxLabel: '~~override~~'
		}, {
			xtype: 'checkbox',
			name: 'forceImport',
			hideLabel: true,		
			boxLabel: '~~forceImport~~'
		}]
	}, {
		xtype: 'form',
		itemId: 'dropzone',
		style: 'border: 2px dashed #ccc;margin: 5px 0px 10px 0px',
		hidden: '@{!dropAreaIsVisible}',
		flex: 1,
		layout: {
			type: 'vbox',
			pack: 'center',
			align: 'center'
		},
		items: [{
			xtype: 'label',
			text: '~~dropArea~~',
			style: {
				'font-size': '30px',
				'color': '#CCCCCC'
			}
		}]
	}, {
		hidden: '@{!statusDisplayIsVisible}',
		autoScroll: true,
		html: '@{processedStatus}',
		flex: 1
	}],
	uploaderListeners: {
		uploaderror: function(uplaoder, data) {
			this.owner.fireEvent('uploadError', this.owner, data);
		},
		fileuploaded: function(uploader, data) {
			this.owner.fireEvent('fileUploaded', this.owner, uploader, data);
		},
		uploadprogress: function(uploader, file, name, size, percent) {
			this.owner.percent = file.percent;
		}
	},
	listeners: {
		afterrender: function() {
			this.myUploader = Ext.create('RS.common.SinglePlupUpload', this);
			this.uploader = this.getConfig();
			this.myUploader.initialize(this);
		},
		fileUploaded: '@{importStart}',
		uploadError: '@{uploadError}'
	},
	asWindow: {
		title: '@{title}',
		modal: true,
		width: 600,
		height: 400,
		padding : 15,
		cls : 'rs-modal-popup',		
		buttons: [{
			itemId: 'browse',
			text: '~~browse~~',
			cls : 'rs-med-btn rs-btn-dark',
			hidden: '@{!browseIsVisible}',
			listeners: {
				hide: function() {
					var fileField = this.up().el.query('.moxie-shim')[0];
					if (fileField)
						fileField.style.display = 'none'
				},
				show: function() {
					var fileField = this.up().el.query('.moxie-shim')[0];
					if (fileField)
						fileField.style.display = 'inline'
				}
			}
		}, 
		{
			name : 'abort',
			cls : 'rs-med-btn rs-btn-dark'
		},{
			name : 'overrideLock', 
			cls : 'rs-med-btn rs-btn-dark'
		},{	
			name : 'backToImport', 
			cls : 'rs-med-btn rs-btn-dark'
		},{
			name :'close',
			cls : 'rs-med-btn rs-btn-light'
		}]
	}
});
*/
glu.defView('RS.resolutionrouting.MappingDefinition', {
	layout: {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		margin : '4 0'
	},
	items: [{
		xtype: 'grid',
		flex : 1,
		disabled: '@{!fieldsIsEnabled}',
		name: 'mapping',
		store: '@{store}',	
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		columns: [{
			header: '~~name~~',
			dataIndex: 'name',
			flex: 1
		}, {
			header: '~~value~~',
			dataIndex: 'value',
			flex: 4,
			editor: {
				xtype: 'textfield',
				allowBlank: false
			},
			renderer: Ext.String.htmlEncode
		}],
		listeners: {
			edit: '@{verifyFields}'
		}
	},{
		xtype: 'tbtext',
		style: 'color:red !important',
		htmlEncode : false,
		text: '~~fieldInvalidMsg~~',
		hidden: '@{fieldIsValid}'
	},{
		xtype: 'tbtext',
		htmlEncode : false,
		style: 'color:red !important',
		text: '~~fieldIsEmptyMsg~~',
		hidden: '@{!fieldIsEmpty}'
	}]
});
glu.defView('RS.resolutionrouting.Rule', {
	title: '~~rule~~',
	bodyPadding : 15,
	modal: true,
	cls : 'rs-modal-popup',
	autoScroll: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		labelWidth: 120,
		margin : '4 0'
	},
	items: [{
		xtype: 'combo',
		name: 'module',
		minChars: 2,
		queryDelay: 500,
		store: '@{moduleStore}',
		displayField: 'name',
		valueField: 'name',
		margin : '4 15',
		labelStyle : 'font-weight:bold'
	}, {
		xtype: 'checkboxfield',
		hideLabel: true,
		boxLabel: '~~active~~',
		name: 'active',
		margin: '0 0 0 140'
	}, {
		xtype : 'fieldset',
		margin : '10 0 4 0',
		padding : '5 15 10',
		title : '~~integrationTitle~~',
		items : [{
			xtype : 'checkboxfield',
			name : 'createInvestigationRecord',
			hideLabel : true,
			boxLabel : '~~sirTitle~~',
		},{
			//SIR
			xtype : 'container',
			margin : '0 0 10 0',
			layout : {
				type :'vbox',
				align : 'stretch'
			},
			defaults : {
				labelWidth: 120,
				margin : '4 0'
			},
			hidden : '@{!createInvestigationRecord}',
			items : [{
				xtype : 'textfield',
				name : 'sirSource',
			},{
				xtype : 'textfield',
				name : 'investigationTitle'
			},{
				xtype : 'combobox',
				name : 'investigationType',
				store : '@{investigationTypeStore}',
				displayField : 'name',
				valueField : 'name',
				queryMode : 'local'
			},{
				xtype: 'triggerfield',
				name: 'playbook',
				triggerCls: 'x-form-search-trigger',
				onTriggerClick: function() {
					this.fireEvent('openPlaybookSearch');
				},
				listeners: {
					openPlaybookSearch: '@{openPlaybookSearch}'
				}
			},{
				xtype : 'combobox',
				store : '@{severityStore}',
				name : 'severity',
				displayField : 'name',
				valueField : 'name',
				queryMode : 'local'
			},{
				xtype : 'combobox',
				store : '@{ownerStore}',
				name : 'owner',
				displayField : 'userId',
				valueField : 'userId',
				editable : false,
				queryMode : 'local'
			},{
				xtype : 'component',
				margin : '10 0 10 125',
				html : '~~sirUIFlag~~'
			}]
		},{
			xtype : 'tabpanel',	
			cls : 'rs-tabpanel-dark',
			foo : '@{foo}',
			layout : {
				align : 'stretch'
			},
			items : [{
				title : '~~uiTitle~~',
				itemId : 'uiDisplay',
				layout : {
					type :'vbox',
					align : 'stretch'
				},
				defaults : {
					labelWidth: 120,
					margin : '4 0'
				},
				items : [{
					xtype: 'triggerfield',
					name: 'wiki',
					triggerCls: 'x-form-search-trigger',
					onTriggerClick: function() {
						this.fireEvent('openPageSearch');
					},
					listeners: {
						openPageSearch: '@{openPageSearch}'
					}
				},{
					xtype: 'triggerfield',
					name: 'automation',
					triggerCls: 'x-form-search-trigger',
					onTriggerClick: function() {
						this.fireEvent('openAutomationSearch',this, 'automation');
					},
					listeners: {
						openAutomationSearch: '@{openAutomationSearch}'
					}
				},{
					xtype: 'checkboxfield',
					hideLabel: true,
					boxLabel: '~~newWorksheetOnly~~',
					name: 'newWorksheetOnly',
					margin: '0 0 0 125'
				}]
			},{
				title : '~~gateway~~',
				layout : {
					type : 'vbox',
					align : 'stretch'
				},
				defaults : {
					margin : '4 0',
					labelWidth : 120
				},
				items : [{
					xtype: 'triggerfield',
					name: 'runbook',
					triggerCls: 'x-form-search-trigger',
					onTriggerClick: function() {
						this.fireEvent('openAutomationSearch',this, 'runbook');
					},
					listeners: {
						openAutomationSearch: '@{openAutomationSearch}'
					}
				},{
					xtype: 'textfield',
					name: 'eventId'
				}]
			}]			
		}]
	},{
		xtype: 'fieldcontainer',
		layout: 'hbox',
		margin : '10 0 4 15',
		items: [{
			xtype: 'combo',
			name: 'schema',
			labelWidth: 120,
			minChars: 1,			
			flex: 1,
			store: '@{schemaStore}',
			pageSize: 30,
			displayField: 'name',
			valueField: 'id',
			margin : '0 5 0 0',
			labelStyle : 'font-weight:bold'
		}, {
			xtype: 'image',
			cls: 'rs-btn-edit',
			hidden: '@{!jumpToSchemaIsVisible}',
			listeners: {
				handler: '@{jumpToSchema}',
				render: function(image) {
					image.getEl().on('click', function() {
						image.fireEvent('handler')
					})
				}
			}
		}]
	},{
		xtype: '@{mappingDefinition}',
		cls : 'rs-grid-dark',
		minHeight: 200,
		flex: 1,
		margin : {
			bottom : 10
		}
	}],
	listeners: {
		beforerender: function() {
			this.setWidth(Math.round(Ext.getBody().getWidth() * 0.6));
		},
		resize: function(panel) {			
			panel.center();				
		},
		beforeshow: function(win) {			
			win.setHeight(Math.round(Ext.getBody().getHeight() * 0.9));
		},
	},	
	dockedItems : [{
		xtype : 'toolbar',
		dock : 'bottom',
		style : 'border-top: 1px solid #b5b8c8 !important;',
		padding : '8 15',
		items : ['->',{
			name : 'save',
			cls : 'rs-med-btn rs-btn-dark'
		},
		//Working but disable for now to simplify UI.
		/*{
			name : 'previousRule',
			cls : 'rs-med-btn rs-btn-dark'
		},{
			name : 'nextRule',
			cls : 'rs-med-btn rs-btn-dark'
		},{
			name : 'newRule',
			cls : 'rs-med-btn rs-btn-dark'
		},
		*/{
			name : 'close',
			cls : 'rs-med-btn rs-btn-light'
		}]
	}]
});
glu.defView('RS.resolutionrouting.Rules', {
	layout: 'border',
	padding : 15,
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true,
		useWindowParams: false
	}],
	items: [{
		padding : '32 0 0 0',
		region: 'west',
		xtype: 'treepanel',
		cls : 'rs-grid-dark',
		store: '@{treeStore}',
		columns: [{
			header: '~~module~~',
			xtype: 'treecolumn',
			dataIndex: 'module',
			flex: 1
		}],
		selected: '@{selectedModule}',
		width: 300,
		split: true,
		listeners: {
			afterrender: function() {
				this.selectPath(this.getRootNode().getPath());
			}
		}
	}, {
		region: 'center',
		displayName: '~~resolutionRouting~~',
		stateId: 'rsresolutionroutingmain',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		dockedItems: [{
			xtype: 'toolbar',
			name: 'actionBar',
			cls: 'actionBar rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: ['addRule', 'deleteRules', '-', 'deploy', {
				text: '~~bulk~~',
				menu: {
					xtype: 'menu',
					items: ['importCSV', 'exportCSV', '-', 'activateMapping', 'deactivateMapping']
				}
			}]
		}],
		viewConfig: {
			enableTextSelection: true
		},
		name: 'rules',
		store: '@{ruleStore}',
		enableColumnMove: true,
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}, {
			ptype: 'pager'
		}, {
			ptype: 'rowexpander',
			rowBodyTpl: new Ext.XTemplate(
				'<div resolveId="resolveRowBody" style="color:#333">{[this.formatFields(values)]}</div>', {
					formatFields: function(values) {
						var schema = values.schema;
						if (!schema)
							return '';
						var rid = Ext.String.htmlEncode(values.rid);
						var list = [];
						try {
							var fields = Ext.decode(schema.jsonFields);
							Ext.each(fields.sort(function(a, b) {
								return a.order > b.order ? 1 : -1;
							}), function(field) {
								list.push(field.name + (field.regex ? '(' + field.regex + ')' : ''));
							})

							var rids = rid.split('§');
							var strs = [];
							for (var i = 0; i < rids.length; i++) {
								strs.push('<tr><td>' + Ext.String.htmlEncode(list[i]) + ':</td><td>' + Ext.String.htmlEncode(rids[i]) + '</td></tr>');
							}
						} catch (e) {
							return ''
						}
						return '<table style="padding:0 0 0 62px">' + strs.join('') + '</table>';
					}
				}
			)
		}],
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTooltip: RS.common.locale.editColumnTooltip,
			columnTarget: '_self',
			columnEventName: 'editAction',
			glyph : 0xf044,
			cellNavigation: true,
			canNavigateToCol: function(col, viewDomIndex) {
				return col.dataIndex && col.dataIndex != 'rid';
			},
			columnIdField: 'id',
			canSelectEntireTable: true
		},
		multiSelect: true,
		columns: [{
			header: '~~rid~~',
			dataIndex: 'rid',
			filterable: true,
			sortable: true,
			flex: 3
		}, {
			header: '~~schema~~',
			dataIndex: 'schema.name',
			filterable: true,
			sortable: true
		}, {
			header: '~~wiki~~',
			dataIndex: 'wiki',
			filterable: true,
			sortable: true,
			flex: 1,
			editor: {
				xtype: 'textfield'
			}
		},{
			header: '~~automation~~',
			dataIndex: 'automation',
			hidden: true,
			filterable: true,
			sortable: true,
			flex: 1,
			editor: {
				xtype: 'textfield'
			}
		},{
			header: '~~gwautomation~~',
			dataIndex: 'runbook',
			filterable: true,
			sortable: true,
			flex: 1,
			editor: {
				xtype: 'textfield'
			}
		},{
			header: '~~eventId~~',
			dataIndex: 'eventId',
			hidden: true,
			filterable: true,
			sortable: true,
			flex: 1,
			editor: {
				xtype: 'textfield'
			}
		}, {
			header: '~~active~~',
			dataIndex: 'active',
			filterable: true,
			sortable: true,
			width: 80,
			align : 'center',
			renderer: RS.common.grid.booleanRenderer(),
			editor: {
				xtype: 'combo',
				editable: false,
				displayField: 'name',
				valueField: 'value',
				store: {
					xtype: 'store',
					fields: ['name', 'value'],
					data: [{
						name: 'TRUE',
						value: true
					}, {
						name: 'FALSE',
						value: false
					}]
				}
			}
		}].concat(RS.common.grid.getSysColumns()),
		editCell: function() {
			this.plugins[0].startEditByPosition({
				row: this.getSelectionModel().currentCell.row,
				column: this.getSelectionModel().currentCell.column
			});
		},
		editing: false,
		listeners: {
			editAction: '@{editRule}',
			render: function() {
				this.getEl().on('click', function(evt) {
					var row = evt.getTarget('tr[role="row"]');
					var cell = evt.getTarget('td[role="gridcell"]');
					if (!row || this.currentCell == cell || this.editing)
						return;
					var colIdx = 0;
					Ext.each(row.childNodes, function(child, idx) {
						if (child == cell)
							colIdx = idx; //this is for the edit column and row expander column and the rid column
					});
					if (colIdx < 3) {
						if (this.currentCell)
							Ext.fly(this.currentCell).removeCls('highlightCell');
						this.currentCell = null;
						return;
					}
					var rowIdx = parseInt(row.getAttribute('data-recordindex'));
					this.getSelectionModel().currentCell.row = rowIdx;
					this.getSelectionModel().currentCell.column = colIdx;
					this.fireEvent('navigatecell', this.getSelectionModel());
				}, this);

				this.getEl().on('keyup', function(evt) {
					if (evt.F2 == evt.getCharCode()) {
						this.editCell();
					}
				}, this);
			},
			edit: function(editor, eOpts) {
				this.fireEvent('recordEdit', this, eOpts.record);
			},
			recordEdit: '@{recordEdit}',
			beforeedit: function(editor, eOpt) {

				if (event && (event.ctrlKey || event.shiftKey))
					return false;
				this.fireEvent('navigatecell', this.getSelectionModel());
				if (eOpt.column.dataIndex == 'rid') {
					// if (event.type != 'click')
					// 	editor.view.panel.fireEvent('editAction', editor.view.panel, eOpt.record.get('id'));
					return false;
				}
				this.getSelectionModel().currentCell = {
					row: eOpt.rowIdx,
					column: eOpt.colIdx
				};
			},
			navigatecell: function(sm) {
				var rowIdx = sm.currentCell.row;
				var colIdx = sm.currentCell.column;
				var r = this.view.getRecord(rowIdx);
				var cell = this.view.getCell(r, this.view.getGridColumns()[colIdx]).dom
				if (this.currentCell == cell)
					return;
				if (this.currentCell)
					Ext.fly(this.currentCell).removeCls('highlightCell');
				this.currentCell = cell;
				Ext.fly(cell).addCls('highlightCell');
			},
			selectAllAcrossPages: function() {
				var filters = this.up().plugins[0].getWhereFilter();
				this.fireEvent('selectAll', this, filters);
			},
			selectAll: '@{selectAllAcrossPages}',
			selectionChange: '@{selectionChanged}'
		}
	}],
	listeners: {
		beforedestroy : '@{beforeDestroyComponent}'
	}
});
glu.defView('RS.resolutionrouting.Schema', {
	padding : 15,
	autoScroll: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		cls : 'rs-dockedtoolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '~~schema~~'
		}],
		margin : '0 0 15 0'
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'		
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}'
		}]
	}],
	defaults : {
		labelWidth : 120
	},
	items: [{
		layout : {
			type : 'vbox',
			align : 'stretch'
		},
		defaults : {
			margin : '4 0'
		},
		style : 'border-top:1px solid silver',
		padding : '8 0',
		items : [{
			xtype: 'textfield',
			name: 'name'
		},{
			xtype: 'textarea',
			name: 'description',
			height : 150
		},{
			xtype: 'numberfield',
			name: 'order',
			minValue: 0,
			maxValue: '@{maxOrder}',
			maxWidth : 600
		},{
			xtype : 'combo',
			store : '@{orgStore}',
			displayField : 'uname',
			valueField : 'id',
			editable : false,
			name : 'sysOrg',
			maxWidth : 600,
			queryMode : 'local'
		}]
	},{
		xtype: 'panel',
		flex : 1,
		layout: {
			type : 'hbox',
			align : 'stretch'
		},
		autoScroll: true,
		defaults: {
			flex: 1
		},
		items: [{
			title: '~~fields~~',		
			xtype: 'grid',
			cls : 'rs-grid-dark',
			margin : {
				right : 5
			},
			name: 'fields',
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}],
			viewConfig: {
				plugins: [{
					ptype: 'gridviewdragdrop'
				}]
			},
			dockedItems: [{
				xtype: 'toolbar',
				cls : 'rs-dockedtoolbar',
				defaults :{
					cls : 'rs-small-btn rs-btn-light'
				},
				items: ['addField', 'removeFields']
			}],
			store: '@{fieldStore}',			
			columns: [{
				header: '~~name~~',
				dataIndex: 'name',
				minWidth: 200,
				editor: {
					xtype: 'textfield',
					allowBlank: false
				}
			}, {
				header: '~~source~~',
				dataIndex: 'source',
				flex: 1,
				editor: {
					xtype: 'textfield'
				}
			}, {
				header: '~~regex~~',
				dataIndex: 'regex',
				flex: 1,
				editor: {
					xtype: 'textfield'
				}
			}]
		}, {
			title: '~~queues~~',
			xtype: 'grid',
			cls : 'rs-grid-dark',
			name: 'queues',
			dockedItems: [{
				xtype: 'toolbar',
				cls : 'rs-dockedtoolbar',
				defaults :{
					cls : 'rs-small-btn rs-btn-light'
				},
				items: ['addGateway', 'removeGateways']
			}],
			store: '@{queueStore}',
			columns: [{
				header: '~~queue~~',
				dataIndex: 'queue',
				flex: 1
			}],
			margin : {
				left : 5
			}
		}]
	},{
		xtype: 'component',
		margin : '4 0',
		html: '@{invalidFieldsMsg}',
		hidden : '@{invalidTextIsHidden}'
	}]
});

glu.defView('RS.resolutionrouting.QueuePicker', {
	padding : 15,
	cls : 'rs-modal-popup',
	title: '~~queue~~',
	width: 600,
	modal: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'combo',
		name: 'queue',
		editable: false,
		emptyText: '~~selectQueue~~',
		displayField: 'queue',
		valueField: 'queue',
		queryMode: 'local',
		store: '@{queueStore}'
	}],
	buttons: [{
		name : 'dump',
		cls : 'rs-med-btn rs-btn-dark'
	},{ 
		name : 'close',
		cls : 'rs-med-btn rs-btn-light'
	}]
});

glu.defView('RS.resolutionrouting.Schemas', {
	layout: 'fit',
	padding : 15,
	items: [{
		disabled: '@{wait}',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		displayName: '~~resolutionRoutingSchema~~',
		dockedItems: [{
			xtype: 'toolbar',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: ['createSchema', 'deleteSchemas']
		}],
		name: 'schemas',
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true,
			useWindowParams: false
		}, {
			ptype: 'pager'
		}],
		viewConfig: {
			plugins: [{
				ptype: 'gridviewdragdrop',
				containerScroll: true
			}],
			listeners: {
				drop: function() {
					this.panel.fireEvent('reorder');
				}
			}
		},
		stateId: 'rsresolutionroutingschema',
		store: '@{store}',
		columns: [{
			header: '~~name~~',
			dataIndex: 'name',
			filterable: true,
			sortable: false
		}, {
			header: '~~fields~~',
			dataIndex: 'jsonFields',
			flex: 1,
			filterable: true,
			sortable: false,
			renderer: function(value) {
				var fields = [];
				try {
					Ext.each(Ext.decode(value), function(f) {
						fields.push(Ext.util.Format.htmlEncode(f.name));
					});
				} catch (e) {}
				return fields.join(',');
			}
		}, {
			header: '~~order~~',
			dataIndex: 'order',
			width: 90,
			filterable: false,
			sortable: true,
			hidden: true
		}, {
			header: '~~gatewayQueues~~',
			dataIndex: 'gatewayQueues',
			flex: 1,
			filterable: true,
			sortable: false,
			renderer: function(value) {
				try {
					return Ext.decode(value).join(',');
				} catch (e) {}
				return '';
			}
		}, {
			header: '~~description~~',
			dataIndex: 'description',
			filterable: true,
			sortable: false,
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		},{
			header: '~~organization~~',
			dataIndex: 'sysOrg',
			initialShow: true,
			sortable: true,
			renderer: function(value, metaData, record) {
				if (value) {
					if(!record.store.id2NameMap) {
						record.store.id2NameMap = {};
						clientVM.user.orgs.forEach(function(e) {
							if(e.uname != 'None')
								record.store.id2NameMap[e.id] = e.uname;
						});
					}
					return RS.common.grid.plainRenderer() (record.store.id2NameMap[value]);
				}
				return '';
			}
		}].concat(RS.common.grid.getSysColumns()),
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'id'
		},
		listeners: {
			editAction: '@{editSchema}',
			reorder: '@{redoOrder}',
			beforedestroy : '@{beforeDestroyComponent}'
		}
	}]
});
glu.defView('RS.resolutionrouting.WikiPicker', {
	layout: 'border',
	padding : 15,		
	title: '@{title}',
	cls : 'rs-modal-popup',
	modal: true,
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}],
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockectoolbar',
		name: 'actionBar',
		items: []
	}],
	items: [{
		region: 'west',
		width: 200,
		maxWidth: 300,
		split : true,
		xtype: 'treepanel',
		cls : 'rs-grid-dark',
		columns: '@{moduleColumns}',
		name: 'runbookTree',
		listeners: {
			selectionchange: '@{menuPathTreeSelectionchange}'
		},
		margin : {
			bottom : 10
		}
	}, {
		region: 'center',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		displayName: '@{title}',
		name: 'runbookGrid',
		columns: [{
			header: '~~name~~',
			dataIndex: 'ufullname',
			filterable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()),
		multiSelect: false,
		selected: '@{selected}',
		autoScroll: true,
		viewConfig: {
			enableTextSelection: true
		},
		plugins: [{
			ptype: 'resolveexpander',
			pluginId: 'rowExpander',
			rowBodyTpl: new Ext.XTemplate('<div resolveId="resolveRowBody" style="color:#333"><span style="font-weight:bold">Summary:</span><br/><br/>&nbsp&nbsp&nbsp&nbsp{usummary:htmlEncode}</div>')
		}],		
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTooltip: RS.common.locale.editColumnTooltip,
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'ufullname'
		},
		listeners: {
			editAction: '@{editRunbook}'
		},
		margin : {
			bottom : 10
		}
	}],
	buttons: [{
		name : 'dump',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}],	
	listeners: {
		beforerender: function() {
			clientVM.setPopupSizeToEightyPercent(this);
		}
	}
});
/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.resolutionrouting').locale = {
	windowTitle: 'Resolution Routing',
	name: 'Name',
	description: 'Description',
	fields: 'Fields',
	regex: 'Extract Regex (optional)',
	source: 'Source (optional)',
	queues: 'Gateways',
	gatewayQueues: 'Gateway',
	organization: 'Organization',
	order: 'Order',
	queue: 'Gateway',
	saveSchemaSuccess: 'Schema has been saved.',
	saveSchemaError: 'Cannot save schema:[{0}]',
	rid: 'RID',
	ridDisplay: 'RID',
	schema: 'Schema',
	wiki: 'Page',
	runbook : 'Automation',
	gwautomation: 'GW Automation',
	automation : 'UI Automation',
	eventId: 'Event ID',
	active: 'Active',
	namespace: 'Namespace',
	module: 'Module',
	invalidField : 'This field is required.',
	Rule: {
		uiTitle : 'UI Display',
		gateway : 'Gateway',
		GatewayUI: 'Gateway/UI',
		sirTitle : 'Security Incident Response',
		sirSource : 'Source',

		automation : 'Automation',
		integrationTitle : '<b>Integration Behavior</b>',

		//SIR
		createInvestigationRecord : 'Create Investigation Record',
		investigationTitle : 'Title',
		investigationType : 'Type',
		playbook : 'Playbook',
		owner : 'Owner',
		severity : 'Severity',
		sirAutomation : 'Automation',

		newWorksheetOnly: 'Execute on New Worksheet Only',
		newRule: 'New',
		previousRule: 'Previous',
		nextRule: 'Next',
		rule: 'Mapping',
		module: 'Module',
		gatewayScript: 'Gateway Script',
		save: 'Save',
		saveRuleError: 'Cannot save mapping:{0}',
		getRuleError: 'Cannot get mapping from the server:{0}',
		saveRuleSuccess: 'Rules have been saved.',
		close: 'Close',
		schemaChangeTitle: 'Warning',
		schemaChangeMsg: 'Are you sure you want to change the schema and override current rid fields?',
		discardCurrentFields: 'Override',
		cancel: 'Cancel',
		invalidModule: 'Module is required. It should only contain alphabets and underscore.',
		invalidSchema: 'Schema is required.',
		invalidWiki: 'For UI Display, UI Automation and GW Automation, at least one of them need to be specified.',
		invalidRunbook: 'For UI Display, UI Automation and GW Automation, at least one of them need to be specified.',
		invalidAutomation: 'For UI Display, UI Automation and GW Automation, at least one of them need to be specified.',
		schemaMappingInsistent: 'The fields in the current mapping defintion are not consistent with the schema definition.',
		sirUIFlag : '<b>Note:</b> To be able to redirect to an SIR record, a parameter <b>sir = true</b> needs to be added in the URL.'

	},

	Rules: {
		resolutionRouting: 'RID Mapping',
		deploy: 'Sync',
		addRule: 'Add',
		deleteRules: 'Delete',
		deleteRulesError: 'Cannot delete the selected mappings.{0}',
		deleteRulesMsg: 'Are you sure you want to delete the selected records?',
		deleteRuleMsg: 'Are you sure you want to delete the selected record?',
		deploySuccess: 'Active mapping have been deployed',
		saveRuleError: 'Cannot save mappings:{0}',
		expandModuleError: 'Cannot get sub module from the server:{0}',
		deleteRuleSuccess: 'Rules have been deleted.',
		deleteRulesThroughoutPages: 'Are you sure you want to delete all the mappings across all the pages?',
		cancel: 'Cancel',
		bulk: 'Bulk',
		importCSV: 'Import CSV',
		exportCSV: 'Export CSV',
		exportPollingError: 'Cannot poll the export task status.{0}',
		activateMapping: 'Activate',
		deactivateMapping: 'Deactivate'
	},
	Schemas: {
		listSchemaError: 'Cannot get schemas from thes server:{0}',
		deleteSchemasMsg: 'Are you sure you want to delete the selected schemas?',
		deleteSchemaMsg: 'Are you sure you want to delete the selected schema?',
		deleteSchemaSuccess: 'The selected schemas have been deleted.',
		deleteSchemaError: 'Cannot delete the selected schemas:{0}',
		createSchema: 'New',
		deleteSchemas: 'Delete',
		resolutionRoutingSchema: 'Schemas',
		cancel: 'Cancel',
		reorderError: 'Error ordering schemas. [{0}]'
	},
	Schema: {
		schema: 'Schema',
		back: 'Back',
		save: 'Save',
		addField: 'Add',
		removeFields: 'Remove',
		queues: 'Gateways',
		addGateway: 'Add',
		removeGateways: 'Remove',
		sysOrg : 'Organization',
		getSchemaError: 'Cannot get schema from the server:{0}',
		getNextOrderError: 'Cannot get schema order from the server.',
		invalidName: 'Schema name is required and should only contain alphanumerics and underscore',
		duplicateFieldMsg: '<i style="color:red">*Duplicate field name "{0}" detected current schema definitions.</i>',
		noFieldDefinedMsg: '<i style="color:red">*The schema should contain at least one field.</i>'
	},
	QueuePicker: {
		dump: 'Add',
		selectQueue: 'Please select a gateway..',
		close: 'Close',
		noGatewayFound: 'No Gateway Available',
		noGatewayFoundMsg: 'Cannot get any available gateway from the server.',
		invalidQueueName: 'Please pick a gateway.'
	},
	WikiPicker: {
		all: 'All',
		runbook: 'Automation',
		wiki: 'Page',
		dump: 'Select',
		cancel: 'Close'
	},
	name: 'Name',
	value: 'Value',
	MappingDefinition: {
		mappingDefinition: 'Mapping Definition',
		addField: 'Add',
		removeFields: 'Remove',
		dump: 'OK',
		cancel: 'Close',
		fieldIsEmptyMsg: '<i>*Field value should not be empty.</i>',
		fieldInvalidMsg: '<i>*Field value cannot contain "|" symbol.</i>'
	},
	Bulk: {
		bulkPollError: 'Error tracking the bulk operation status.{0}',
		importCSV: 'Import',
		abort: 'Abort',
		abortSuccess: 'Process aborted.',
		override: 'Override Existing Mapping',
		forceImport: 'Override Lock',
		browse: 'Browse',
		close: 'Close',
		exportCSV: 'Export',
		dragHere: 'Drop the file in to the area for uploading..',
		dropArea: 'Drop the file here.',
		submitExportReqError: 'Cannot start the export task. {0}',
		fileUploadError: 'Cannot upload file. {0}',
		bulkOpInProgress: 'Bulk Operation In Progress',
		EXPORT: 'User {0} is currently exporting the csv file on RSVIEW[{1}]({2}).',
		IMPORT: 'User {0} is currently importing the csv file on RSVIEW[{1}]({2})..',
		ACTIVATION: 'User {0} is activating mappings on RSVIEW[{1}]({2})..',
		DEACTIVATION: 'User {0} is deactivation mappings on RSVIEW[{1}]({2})..',
		currentNode: 'Current Node',
		otherNode: 'Another Node',
		stopCurrent: 'Stop Current Task',
		importTitle: 'Importing CSV',
		exportTitle: 'Exporting CSV',
		activationTitle: 'Activating Mapping',
		deactivationTitle: 'Deactivating Mapping',
		no: 'Cancel',
		backToImport: 'Back',
		overrideLock: 'Override',
		checkBeforeImportError: 'Cannot check current bulk op status. {0}',
		cannotAbortProcess: 'Error aborting process.{0}'
	}
}
