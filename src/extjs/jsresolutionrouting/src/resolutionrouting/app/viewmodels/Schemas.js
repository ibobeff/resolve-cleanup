glu.defModel('RS.resolutionrouting.Schemas', {
	store: {
		mtype: 'store',
		fields: ['name', 'description', {
			name: 'order',
			type: 'int'
		}, 'jsonFields', 'gatewayQueues', {
			name: 'order',
			type: 'int'
		}].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'tableData.order',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/resolutionrouting/schema/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	schemasSelections: [],
	wait: false,
	activate: function() {
		this.store.load();
	},
	init: function() {
		var me = this;
		if (this.mock) this.backend = RS.resolutionrouting.createMockBackend(true);

		this.store.on('load', function() {
		//	var respData = this.store.proxy.reader.rawData;
		//	if (!respData.success)
		//		clientVM.displayError(this.localize('listSchemaError', respData.message));
		}, this);
		this.store.on('beforeload', function(store, operation){
			operation.params = operation.params || {};
			var filter = operation.params.filter || [];
			filter.push({
				field: 'sysOrg',
				type: 'auto',
				condition: 'equals',
				value: clientVM.orgId || 'nil'
			});
			operation.params.filter = JSON.stringify(filter);
		});
		clientVM.on('orgchange', this.orgChange.bind(this));
	},
	createSchema: function() {
		clientVM.handleNavigation({
			modelName: 'RS.resolutionrouting.Schema'
		});
	},
	createIsEnabled$: function() {
		return !this.wait;
	},
	deleteSchemas: function() {
		var ids = [];
		Ext.each(this.schemasSelections, function(schema) {
			ids.push(schema.get('id'));
		}, this);
		this.message({
			title: this.localize('deleteSchemas'),
			msg: this.localize(this.schemasSelections.length > 1 ? 'deleteSchemasMsg' : 'deleteSchemaMsg'),
			buttonText: {
				yes: this.localize('deleteSchemas'),
				no: this.localize('cancel')
			},
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.ajax({
					url: '/resolve/service/resolutionrouting/schema/delete',
					params: {
						ids: ids
					},
					success: function(resp) {
						var respData = RS.common.parsePayload(resp);
						if (!respData.success) {
							clientVM.displayError(this.localize('deleteSchemaError', respData.message));
							return;
						}
						this.store.load();
						clientVM.displaySuccess(this.localize('deleteSchemaSuccess'));
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				});
			}
		})
	},
	deleteSchemasIsEnabled$: function() {
		return !this.wait && this.schemasSelections.length > 0;
	},
	editSchema: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.resolutionrouting.Schema',
			params: {
				id: id
			}
		});
	},
	redoOrder: function() {
		var records = [];
		var direction = this.store.sorters.get(0).direction;
		var pageSize = this.store.pageSize;
		var currentPage = this.store.currentPage;
		this.store.each(function(item, idx) {
			var ascIdx = (currentPage - 1) * 50 + idx;
			var descIdx = this.store.totalCount - ascIdx - 1;
			var theIdx = direction == 'ASC' ? ascIdx : descIdx;
			item.set('order', theIdx);
			item.commit();
			records.push(Ext.apply(item.raw, {
				order: theIdx
			}));
		}, this);
		this.ajax({
			url: '/resolve/service/resolutionrouting/schema/reorder',
			scope: this,
			jsonData: records,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('reorderError', respData.message));
					return;
				}
				this.store.reload();
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	orgChange: function() {
		this.store.load();
	},

	beforeDestroyComponent : function() {
		clientVM.removeListener('orgchange', this.orgChange);
	}
});