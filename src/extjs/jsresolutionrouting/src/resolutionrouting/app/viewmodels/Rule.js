glu.defModel('RS.resolutionrouting.Rule', {
	fields: ['id',
		'schema',
		'automation',
		'wiki',
		'runbook',
		'module',
		'eventId', {
			name: 'active',
			type: 'bool'
		}, {
			name: 'newWorksheetOnly',
			type: 'bool'
		}
	],	
	ridFields: [],
	foo : '',
	moduleIsValid$: function() {
		if (/^\s*$/.test(this.module))
			return this.localize('invalidModule');
		var splits = this.module.split('\.');
		var current = '';
		for (var i = 0; i < splits.length; i++)
			if (/^[_a-zA-Z0-9]+$/.test(splits[i]) || /^[_a-zA-Z0-9]+[ _a-zA-Z0-9]*[_a-zA-Z0-9]+$/.test(splits[i]))
				return true;
		return this.localize('invalidModule');
	},
	wikiIsValid$: function() {
		return this.createInvestigationRecord || !/^\s*$/.test(this.runbook) || !/^\s*$/.test(this.wiki) || !/^\s*$/.test(this.automation) ? true : this.localize('invalidWiki');
	},
	runbookIsValid$: function() {
		return this.createInvestigationRecord || !/^\s*$/.test(this.runbook) || !/^\s*$/.test(this.wiki) || !/^\s*$/.test(this.automation) ? true : this.localize('invalidRunbook');
	},
	automationIsValid$: function() {
		return this.createInvestigationRecord || !/^\s*$/.test(this.runbook) || !/^\s*$/.test(this.wiki) || !/^\s*$/.test(this.automation) ? true : this.localize('invalidAutomation');
	},
	newWorksheetOnlyIsEnabled$: function() {
		return this.automationIsValid && !/^\s*$/.test(this.automation);
	},
	// eventIdIsValid$: function() {
	// 	return /^\s*$/.test(this.eventId) || !/^\s*$/.test(this.automation) || !/^s*$/.test(this.runbook) ? true : this.localize('invalidEventId');
	// },
	// eventIdIsEnabled$: function() {
	// 	return this.eventIdIsEnabled;
	// },
	isSchemaExist: false,
	schemaIsValid$: function() {
		return this.isSchemaExist ? true : this.localize('invalidSchema');
	},
	isSchemaExistChanged$: function() {
		this.mappingDefinition.set('fieldsIsEnabled', this.isSchemaExist);
	},
	mappingFieldIsValid: false,
	mappingDefinition: {
		mtype: 'RS.resolutionrouting.MappingDefinition'
	},
	moduleStore: {
		mtype: 'store',
		fields: [{
			name: 'name',
			convert: function(value, r) {
				return r.raw
			}
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/resolutionrouting/rule/module/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	schemaStore: {
		mtype: 'store',
		fields: ['id', 'name'],
		remoteSort: true,
		sorters: [{
			property: 'order',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/resolutionrouting/schema/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	wait: false,	
	init: function() {	
		this.moduleStore.on('beforeload', function(store, op) {
			op.params = op.params || {};
			Ext.applyIf(op.params, {
				filter: Ext.encode([{
					field: 'module',
					type: 'auto',
					condition: 'contains',
					value: this.module
				}])
			});
		}, this);
		this.schemaStore.on('beforeload', function(store, op) {
			var record = store.findRecord('id', this.schema);
			var queryStr = record ? record.get('name') : "";		
			Ext.applyIf(op.params, {
				filter: Ext.encode([{
					field: 'name',
					type: 'auto',
					condition: 'contains',
					value: (op.params || {}).query || queryStr
				}])
			})
		}, this);
		this.schemaStore.on('datachanged', function() {
			if (!this.schema)
				return;
			this.checkSchema();
		}, this);
		this.set('active', true);
		this.set('newWorksheetOnly', true);
		this.schemaStore.load({
			scope: this,
			callback: function() {
				if (this.id)
					this.loadRule();
			}
		});
		var me = this;
		this.mappingDefinition.on('fieldIsValidChanged', function() {
			me.set('mappingFieldIsValid', this.fieldIsValid);
		})
		this.ownerStore.load();

		this.ajax({
			url : '/resolve/service/sysproperties/getSystemPropertyByName',
			params : {
				name : 'app.security.types'
			},
			method : 'POST',
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				if(response.success && response.data){
					this.investigationTypeStore.loadData(response.data.uvalue.split(','));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	schemaId: '',
	checkSchema: function() {
		//this.set('schemaId', '');
		var isSchemaExist = (this.schemaStore.findBy(function(s) {
			if (this.schema == s.get('name') || this.schema == s.get('id')) {
				this.set('schemaId', s.get('id'));
				return true;
			}
		}, this) != -1)

		if (!isSchemaExist && this.schema && this.schemaId) {
			this.checkSchemaById(this.schemaId);
		} else {
			this.set('isSchemaExist', isSchemaExist);
		}
	},
	checkSchemaById: function(id) {
		this.ajax({
			url: '/resolve/service/resolutionrouting/schema/get',
			params: {
				id: id
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					this.set('isSchemaExist', false);
					clientVM.displayError(this.localize('getSchemaError', respData.message));
					return;
				}

				this.set('schemaId', id);
				this.set('isSchemaExist', true);
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	displaySchema$: function() {
		return this.schema && this.schema.name || '';
	},
	alignMappingFieldsWithSchema: function(ridFields, schemaFields) {
		var ordered = [];
		Ext.each(schemaFields, function(schemaField) {
			var found = null;
			Ext.each(ridFields, function(ridField) {
				if (ridField.name == schemaField.name)
					found = ridField;
			}, this);
			if (found == null) {
				clientVM.displayError(this.localize('schemaMappingInsistent'));
				return false;
			}
			ordered.push(found);
		}, this);
		return ordered;
	},
	fromJson: function(json) {
		this.loadData(json);
		this.set('updatingSchema', true);
		this.set('schema', json.schema && json.schema.name || '');
		this.set('schemaId', json.schema && json.schema.id || '');
		this.set('updatingSchema', false);
		this.schemaStore.load({
			callback: function() {
				if (this.schema) {
					var schemaData = this.schemaStore.findRecord('id', json.schema.id);
					var schemaFields = [];
					if (schemaData) {
						schemaFields = Ext.decode(schemaData.raw.jsonFields);
					} else {
						schemaFields = Ext.decode(json.schema.jsonFields);
					}
					this.set('ridFields', this.alignMappingFieldsWithSchema(json.ridFields, schemaFields));
				}
				this.mappingDefinition.loadDefinition(this.ridFields);
			},
			scope: this
		});

		this.set('createInvestigationRecord', json.sir);
		this.set('investigationTitle', json.sirTitle);
		this.set('owner', json.sirOwner);
		this.set('investigationType', json.sirType);
		this.set('playbook', json.sirPlaybook);
		this.set('severity', json.sirSeverity);
		this.set('sirSource', json.sirSource);
	},
	gatherData: function() {
		this.mappingDefinition.dump();
		var data = this.asObject();
		data['schema'] = {
			id: this.schemaId
		};
		data['ridFields'] = this.ridFields;

		//For SIR
		if(this.createInvestigationRecord){
			data = Ext.apply(data,{
				sir : true,
				sirSource : this.sirSource,
				sirTitle : this.investigationTitle,
				sirOwner : this.owner,
				sirType : this.investigationType,
				sirPlaybook : this.playbook,
				sirSeverity : this.severity
			})
		}
		return data;
	},
	updateFields: function() {
		var schema = this.schemaStore.getById(this.schema);
		var metaFields = [];
		try {
			Ext.each(Ext.decode(schema.raw.jsonFields), function(field) {
				metaFields.push(Ext.apply({
					valid: true
				}, field));
			});
			metaFields.sort(function(a, b) {
				return a.order > b.order ? 1 : -1;
			});
			this.mappingDefinition.loadDefinition(metaFields);
		} catch (e) {}
	},
	updatingSchema: false,
	confirmReloadMappingFields: function(previous) {
		this.message({
			title: this.localize('schemaChangeTitle'),
			msg: this.localize('schemaChangeMsg'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('discardCurrentFields'),
				no: this.localize('cancel')
			},
			fn: function(btn) {
				if (btn != 'yes') {
					this.set('updatingSchema', true);
					this.set('schema', previous);
					this.set('updatingSchema', false);
					return;
				}
				this.updateFields();
			}
		});
	},
	when_schema_changed: {
		on: ['schemaChanged'],
		action: function(current, previous) {
			this.checkSchema();
			if (!this.isSchemaExist)
				return;
			if (this.updatingSchema || this.wait)
				return;
			if (this.mappingDefinition.store.getCount() != 0)
				this.confirmReloadMappingFields(previous);
			else
				this.updateFields();
		}
	},
	loadRule: function() {
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/resolutionrouting/rule/get',
			params: {
				id: this.id
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('getRuleError', respData.message));
					return;
				}
				this.fromJson(respData.data);
			},
			failure: function(r) {
				this.set('updatingSchema', false);
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},
	defineMapping: function() {
		this.open({
			mtype: 'RS.resolutionrouting.MappingDefinition'
		});
	},
	openPageSearch: function() {
		this.open({
			mtype : 'RS.common.ContentPicker',
			contentType : 'page',
			dumper : {
				dump : function(selection) {
					this.set('wiki', selection.get('ufullname'));
				},
				scope : this
			}
		})
	},
	openAutomationSearch: function(fieldName) {
		this.open({
			mtype : 'RS.common.ContentPicker',
			contentType : 'automation',
			dumper : {
				dump : function(selection) {
					this.set(fieldName, selection.get('ufullname'));
				},
				scope : this
			}
		})
	},
	openPlaybookSearch : function(){
		this.open({
			mtype : 'RS.common.ContentPicker',
			contentType : 'playbook',
			dumper : {
				dump : function(selection) {
					this.set('playbook', selection.get('ufullname'));
				},
				scope : this
			}
		})
	},
	dumper: function() {},
	newRule: function() {
		this.mappingDefinition.resetForm();
		this.loadData({
			module: '',
			schema: '',
			wiki: '',
			automation: '',
			runbook: '',
			eventId: '',
			active: true
		});
		this.set('id', '');
	},
	save: function() {
		if(!this.isValid || !this.mappingDefinition.fieldIsValid || this.mappingDefinition.fieldIsEmpty){
			this.set('isPristine', false);
			return ;
		}
		var data = this.gatherData();
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/resolutionrouting/rule/save',
			jsonData: data,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('saveRuleError', respData.message));
					return;
				}
				this.fromJson(respData.data);
				this.dumper(respData, !!this.id);
				clientVM.displaySuccess(this.localize('saveRuleSuccess'));
				this.doClose();
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},
	saveIsEnabled$: function() {
		return !this.wait && this.isValid && this.mappingDefinition.fieldIsValid && !this.mappingDefinition.fieldIsEmpty;
	},
	getCurrentIdx: function(id) {
		if (!id)
			return -1;
		var r = this.parentVM.ruleStore.getById(this.id);
		if (!r)
			return -1;
		return r.store.indexOf(r);
	},
	previousRule: function() {
		var idx = this.getCurrentIdx(this.id);
		var previousRule = this.parentVM.ruleStore.getAt(idx - 1);
		this.set('id', previousRule.get('id'));
		this.loadRule();
	},
	previousRuleIsEnabled$: function() {
		var idx = this.getCurrentIdx(this.id);
		return idx > 0;
	},
	nextRule: function() {
		var idx = 0;
		if (this.id)
			idx = this.getCurrentIdx(this.id);
		var nextRule = this.parentVM.ruleStore.getAt(idx + 1);
		this.set('id', nextRule.get('id'));
		this.loadRule();
	},
	/*nextRuleIsEnabled$: function() {
		var idx = this.getCurrentIdx(this.id);
		return !idx || idx < this.parentVM.ruleStore.getCount() - 1;
	},*/
	close: function() {
		this.doClose();
	},
	jumpToSchema: function() {
		var schema = this.schemaStore.getById(this.schema);
		if(!schema)//Schema sometimes use name sometimes use ID !!!!!!!
			schema = this.schemaStore.findRecord('name', this.schema);
		clientVM.handleNavigation({
			modelName: 'RS.resolutionrouting.Schema',
			params: {
				id: schema ? schema.get('id') : this.schemaId
			},
			target: '_blank'
		});
	},
	jumpToSchemaIsVisible$: function() {
		return this.schemaIsValid === true;
	},

	//SIR
	createInvestigationRecord : false,
	investigationTitle : '',
	owner : '',
	investigationType : '',
	playbook : '',
	sirSource : '',
	severity : 'Critical',
	investigationTypeStore : {
		mtype : 'store',
		fields : [{
			name : 'name',
			convert : function(value, record){
				return record.raw;
			}
		}]
	},
	ownerStore : {
		mtype : 'store',
		fields : ['userId'],
		proxy : {
			url : '/resolve/service/playbook/securityIncident/getSecurityGroup',
			type: 'ajax',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	severityStore : {
		mtype : 'store',
		fields : [{
			name : 'name',
			convert : function(value, record){
				return record.raw;
			}
		}],
		data : ['Critical', 'High', 'Medium', 'Low']
	},
	/*investigationTitleIsValid$ : function(){
		return !this.createInvestigationRecord || this.investigationTitle ? true : this.localize('invalidField');
	},
	investigationTypeIsValid$ : function(){
		return !this.createInvestigationRecord || this.investigationType ? true : this.localize('invalidField');
	},
	sirSourceIsValid$ : function(){
		return !this.createInvestigationRecord || this.sirSource ? true : this.localize('invalidField');
	}*/
});