glu.defModel('RS.resolutionrouting.Schema', {
	wait: false,
	fields: ['id', 'name', 'description', {
		name: 'order',
		type: 'int'
	}].concat(RS.common.grid.getSysFields()),
	accessRights: {
		mtype: 'RS.common.AccessRights',
		rightsShown: ['read', 'write'],
		title: '',
		defaultRolesIsVisible: false,
		local: true
	},
	invalidTextIsHidden : true,
	invalidFieldsMsg: '',
	orgStore : {
		mtype : 'store',
		fields : ['id', 'uname']
	},
	maxOrder: 0,
	orderIsValid$: function() {
		return this.order >= 0 && this.order <= this.maxOrder;
	},
	init: function() {
		var names = {};
		this.set('invalidFieldsMsg', this.localize('noFieldDefinedMsg'));
		this.fieldStore.on('update', function(store, record, operation, modifiedFieldNames, eOpts) {
			if (this.fieldStore.getCount() == 0) {
				this.set('invalidFieldsMsg', this.localize('noFieldDefinedMsg'));
				return;
			}
			this.set('invalidFieldsMsg', '');
			names = {};
			this.fieldStore.each(function(field) {
				if (names[field.get('name')]) {
					this.set('invalidFieldsMsg', this.localize('duplicateFieldMsg', field.get('name')));
					return;
				}
				names[field.get('name')] = true;
			}, this);
		}, this);

		this.fieldStore.on('datachanged', function() {
			if (this.fieldStore.getCount() == 0) {
				this.set('invalidFieldsMsg', this.localize('noFieldDefinedMsg'));
				return;
			}
			this.set('invalidFieldsMsg', '');
			names = {};
			this.fieldStore.each(function(field) {
				if (names[field.get('name')]) {
					this.set('invalidFieldsMsg', this.localize('duplicateFieldMsg', field.get('name')));
					return;
				}
				names[field.get('name')] = true;
			}, this);
		}, this);
	},
	nameIsValid$: function() {
		return /^[_a-zA-Z0-9]+$/.test(this.name) ? true : this.localize('invalidName');
	},
	fieldStore: {
		mtype: 'store',
		fields: ['name', 'source', 'regex']
	},
	fieldsSelections: [],
	queueStore: {
		mtype: 'store',
		fields: ['queue']
	},
	queuesSelections: [],
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},
	defaultData: {
		id: '',
		name: '',
		order: null,
		description: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: ''
	},
	resetForm: function() {
		this.loadData(this.defaultData);
		this.fieldStore.removeAll();
		this.queueStore.removeAll();
		this.set('isPristine', true);
		this.set('invalidTextIsHidden', true);
	},
	activate: function(screens, params) {
		this.resetForm();
		this.set('id', params && params.id || '');
		if (this.id) {
			this.loadSchema();
			this.getNextOrder();
		} else {
			this.getNextOrder(true);
		}
		this.orgStore.loadData(user.orgs || []);
	},
	getNextOrder: function(isNew) {
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/resolutionrouting/schema/order/next',
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('getNextOrderError'), respData.message);
					return;
				}

				if (isNew) {
					this.set('order', respData.data);
					this.set('maxOrder', parseInt(this.order));
				} else {
					this.set('maxOrder', parseInt(respData.data) - 1);
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},
	loadSchema: function() {
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/resolutionrouting/schema/get',
			params: {
				id: this.id
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('getSchemaError', respData.message));
					return;
				}
				this.fromJson(respData.data);
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},
	refresh: function() {
		if (this.id)
			this.loadSchema();
	},
	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.resolutionrouting.Schemas'
		});
	},
	save: function(exitAfterSave) {
		if(!this.isValid || this.invalidFieldsMsg){
			this.set('isPristine', false);
			this.set('invalidTextIsHidden', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveSchema, this, [exitAfterSave])
		this.task.delay(500)
	},
	saveIsEnabled$: function() {
		return !this.wait && this.isValid;
	},
	saveAndExit: function() {
		if(!this.isValid || this.invalidFieldsMsg){
			this.set('isPristine', false);
			this.set('invalidTextIsHidden', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.save(true)
	},
	toJson: function() {
		var fields = [];
		this.fieldStore.each(function(f, idx) {
			fields.push(Ext.apply(f.data, {
				order: idx
			}));
		});

		var queues = [];
		this.queueStore.each(function(g) {
			queues.push(g.get('queue'));
		});
		var json = this.asObject();
		json.jsonFields = Ext.encode(fields);
		json.gatewayQueues = Ext.encode(queues);
		return json;
	},
	fromJson: function(json) {
		this.resetForm();
		this.loadData(json);
		try {
			Ext.each(Ext.decode(json.jsonFields), function(field) {
				this.fieldStore.add(field);
			}, this);
		} catch (e) {}

		this.fieldStore.sort({
			property: 'order',
			direction: 'ASC'
		});
		try {
			Ext.each(Ext.decode(json.gatewayQueues), function(queue) {
				this.queueStore.add({
					queue: queue
				});
			}, this);
		} catch (e) {}
	},
	saveSchema: function(exitAfterSave) {
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/resolutionrouting/schema/save',
			jsonData: this.toJson(),
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('saveSchemaError', respData.message));
					return;
				}
				clientVM.displaySuccess(this.localize('saveSchemaSuccess'));
				if (exitAfterSave === true) {
					clientVM.handleNavigation({
						modelName: 'RS.resolutionrouting.Schemas'
					});
					return;
				}
				if (!this.id) {
					clientVM.handleNavigation({
						modelName: 'RS.resolutionrouting.Schema',
						params: {
							id: respData.data.id
						}
					});
				}
				this.fromJson(respData.data);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},
	addField: function() {
		var max = 0;
		this.fieldStore.each(function(field) {
			if (/^New Field\d*/g.test(field.get('name'))) {
				var strIdx = field.get('name').substring('New Field'.length);
				var idx = 0;
				if (strIdx != '')
					idx = parseInt(strIdx);
				if (idx > max)
					max = idx;
			}
		});
		this.fieldStore.add({
			name: 'New Field' + (max + 1),
			source: '',
			regex: ''
		});
	},
	fieldNameChanged: function(name) {
		var record = this.fieldStore.findRecord('name', name);
		if (!record || record.get('source'))
			return;
		record.set('source', name);
	},
	removeFields: function() {
		this.fieldStore.remove(this.fieldsSelections);
	},
	removeFieldsIsEnabled$: function() {
		return !this.wait && this.fieldsSelections.length > 0;
	},
	addGateway: function() {
		var me = this;
		this.open({
			mtype: 'RS.resolutionrouting.QueuePicker',
			dumper: function() {
				if (!this.queue || me.queueStore.find('queue', this.queue) != -1)
					return;
				me.queueStore.add({
					queue: this.queue
				});
			}
		});
	},
	removeGateways: function() {
		this.queueStore.remove(this.queuesSelections);
	},
	removeGatewaysIsEnabled$: function() {
		return !this.wait && this.queuesSelections.length > 0;
	},
	cancel: function() {
		this.doClose();
	}
});

glu.defModel('RS.resolutionrouting.QueuePicker', {
	queueStore: {
		mtype: 'store',
		fields: ['gateway', 'queue']
	},
	queue: '',
	queueIsValid$: function() {
		return !/^\s*$/.test(this.queue) ? true : this.localize('invalidQueueName');
	},
	loadQueues: function() {
		this.ajax({
			url: '/resolve/service/gateway/listQueues',
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					//clientVM.displayError(this.localize('listQueueError', respData.message));
					clientVM.displayError(respData.message);
					return;
				}
				Ext.Object.each(respData.data, function(k, v) {
					Ext.each(v, function(queue) {
						this.queueStore.add({
							gateway: k,
							queue: queue
						});
					}, this);
				}, this);

				if (this.queueStore.getCount() == 0)
					clientVM.displayMessage(this.localize('noGatewayFound'), this.localize('noGatewayFoundMsg'));
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	init: function() {
		this.loadQueues();
	},
	dumper: function() {},
	dump: function() {
		this.dumper();
		this.close();
	},
	dumpIsEnabled$: function() {
		return this.queueIsValid === true;
	},
	close: function() {
		this.doClose();
	}
});