glu.defModel('RS.resolutionrouting.MappingDefinition', {
	store: {
		mtype: 'store',
		fields: ['id', 'name', 'value', {
			name: 'valid',
			value: true
		}]
	},
	fieldIsValid: true,
	fieldIsEmpty : false,
	init: function() {
		this.store.on('datachanged', function(store) {
			this.verifyFields();
		}, this);
	},
	resetForm: function() {
		this.store.removeAll();
	},
	verifyFields: function() {
		this.set('fieldIsValid', true);
		this.set('fieldIsEmpty', false);
		this.store.each(function(r) {
			if (!r.get('value'))
				this.set('fieldIsEmpty', true);
		}, this);
	},
	loadDefinition: function(orderedFields) {
		this.store.removeAll();
		this.store.add(orderedFields);
	},
	fieldsIsEnabled: false,
	fieldsSelections: [],
	reloadSchemaField: function() {
		this.parentVM.confirmReloadMappingFields();
	},
	dump: function() {
		var ridFields = [];
		this.store.each(function(item) {
			var data = Ext.apply({}, item.data);
			delete data.valid;
			ridFields.push(data);
		}, this);
		this.parentVM.set('ridFields', ridFields);
		this.cancel();
	},
	addField: function() {
		var max = 0;
		this.store.each(function(field) {
			if (/^New Field\d*/g.test(field.get('name'))) {
				var strIdx = field.get('name').substring('New Field'.length);
				var idx = 0;
				if (strIdx != '')
					idx = parseInt(strIdx);
				if (idx > max)
					max = idx;
			}
		});
		this.store.add({
			field: 'New Field' + (max + 1),
			value: 'New Value'
		});
	},
	removeFields: function() {
		this.store.remove(this.fieldsSelections);
	},
	removeFieldsIsEnabled$: function() {
		return this.fieldsSelections.length > 0;
	},
	cancel: function() {
		this.doClose();
	}
});