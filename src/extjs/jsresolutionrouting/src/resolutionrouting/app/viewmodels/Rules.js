glu.defModel('RS.resolutionrouting.Rules', {

	mock: false,

	treeStore: {
		mtype: 'treestore',
		fields: ['module'],
		root: {
			module: 'All'
		}
	},
	schemaStore: {
		mtype: 'store',
		fields: ['id', 'name', 'order'],
		remoteSort: true,
		sorters: [{
			property: 'order',
			order: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/resolutionrouting/schema/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	ruleStore: {
		mtype: 'store',
		fields: ['rid', {
			name: 'schema',
			type: 'object'
		}, {
			name: 'schema.name',
			convert: function(v, r) {
				//v will not be empty when edited
				return v || (r.raw.schema && r.raw.schema.name ? r.raw.schema.name : 'UNDEFINED');
			}
		}, 'wiki', 'runbook', 'automation', 'eventId', 'module', {
			name: 'active',
			type: 'boolean'
		}].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/resolutionrouting/rule/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	activate: function() {
		window.document.title = this.localize('windowTitle');
		this.treeStore.load();
		this.ruleStore.load();
	},
	rulesSelections: [],
	selectedModule: null,
	reqCount: 0,
	init: function() {
		if (this.mock) this.backend = RS.resolutionrouting.createMockBackend(true);

		this.ruleStore.on('beforeload', function(store, op) {
			this.set('reqCount', this.reqCount + 1);
			op.params = op.params || {};
			if (this.selectedModule && this.selectedModule.get('module') != 'All') {
				Ext.apply(op.params, {
					filter: Ext.encode([{
						field: 'module',
						type: 'auto',
						condition: (this.selectedModule.isLeaf() ? 'equals' : 'startsWith'),
						value: this.selectedModule.getPath('module', '.').substring(5) //+ (this.selectedModule.isLeaf() ? '' : '.')
					}, {
						field: 'schema.sysOrg',
						type: 'auto',
						condition: 'equals',
						value: clientVM.orgId || 'nil'
					}])
				});
			} else {
				Ext.apply(op.params, {
					filter: Ext.encode([{
						field: 'schema.sysOrg',
						type: 'auto',
						condition: 'equals',
						value: clientVM.orgId || 'nil'
					}])
				});
			}
		}, this);
		this.set('selectedModule', this.treeStore.getRootNode());
		this.ruleStore.on('load', function() {
			this.set('reqCount', this.reqCount - 1);
		}, this);
		this.ruleStore.on('commonSearchFilterInited', function(commonFilter) {
			this.ruleStore.commonSearchFilter = commonFilter;
		}, this);
		this.treeStore.on('expand', function(node) {
			if (!this.expandingToNode)
				this.expandModuleNode(node);
		}, this);
		this.treeStore.getRootNode().expand();
		clientVM.on('orgchange', this.orgChange.bind(this));
	},
	when_selected_module_changed: {
		on: ['selectedModuleChanged'],
		action: function() {
			if (this.selectedModule) {
				this.set('rulesSelections', []);
				this.ruleStore.load();
			}
		}
	},
	expandModuleNode: function(node, nextAction) {
		this.set('reqCount', this.reqCount + 1);
		var path = node.getPath('module', '.').substring(5);
		params = {
			filter: [{
				field: 'schema.sysOrg',
				type: 'auto',
				condition: 'equals',
				value: clientVM.orgId || 'nil'
			}]
		};
		if (path) {
			params.filter.push({
				field: 'module',
				type: 'auto',
				condition: 'startsWith',
				value: path
			});
		}
		params.filter = Ext.encode(params.filter);
		this.ajax({
			url: '/resolve/service/resolutionrouting/rule/module/expand',
			params: params,
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('expandModuleError', respData.message));
					return;
				}
				node.removeAll();
				Ext.Object.each(respData.data, function(k, v) {
					node.appendChild({
						id: Ext.data.IdGenerator.get('uuid').generate(),
						module: k,
						leaf: v
					});
				}, this)
				if (nextAction) nextAction.call(this);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
		});
	},
	deploy: function() {
		// var records = [];
		// this.ruleStore.each(function(r) {
		// 	if (!r.dirty)
		// 		return
		// 	var schema = this.schemaStore.findRecord('name', r.get('schema.name'));
		// 	if (!schema) {
		// 		clientVM.displayError(this.localize('cannotFindSchemaInStore'));
		// 		return;
		// 	}
		// 	r.data.schema = {
		// 		id: schema.get('id')
		// 	};
		// 	delete r.data['schema.name'];
		// 	records.push(r.data);
		// }, this);
		this.set('reqCount', this.reqCount + 1);
		this.ajax({
			url: '/resolve/service/resolutionrouting/rule/deploy',
			// jsonData: records,
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('saveRuleError', respData.message));
					return;
				}
				clientVM.displaySuccess(this.localize('deploySuccess'));
				this.ruleStore.load();
				this.expandToNode(this.selectedModule);
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
		})
	},
	saveRulesIsEnabled$: function() {
		return this.reqCount == 0;
	},
	addRule: function() {
		var me = this;
		var modulePath = '';
		if (this.selectedModule)
			modulePath = this.selectedModule.getPath('module', '.').substring(5) + (this.selectedModule.isLeaf() ? '' : '.')
		if (modulePath == '.')
			modulePath = '';
		this.open({
			mtype: 'RS.resolutionrouting.Rule',
			module: modulePath,
			dumper: function(respData) {
				var savedRecord = respData.data;
				var record = me.ruleStore.getById(savedRecord.id);
				if (!record) {
					me.ruleStore.add(savedRecord);
					me.expandToNode(me.selectedModule);
					return;
				}
				Ext.Object.each(record.data, function(k, v) {
					record.set(k, savedRecord[k]);
				});
				record.commit();
				me.expandToNode(me.selectedModule);
			}
		});
	},
	addRuleIsEnabled$: function() {
		return this.reqCount == 0;
	},
	editRule: function(id) {
		var me = this;
		this.open({
			mtype: 'RS.resolutionrouting.Rule',
			id: id,
			dumper: function(respData) {
				var savedRecord = respData.data;
				var record = me.ruleStore.getById(savedRecord.id);
				if (!record) {
					me.ruleStore.add(savedRecord);
					return;
				}
				Ext.Object.each(record.data, function(k, v) {
					record.set(k, savedRecord[k]);
				});
				record.commit();
				me.expandToNode(me.selectedModule);
			}
		});
	},
	deleteRulesById: function() {
		var ids = [];
		Ext.each(this.rulesSelections, function(r) {
			ids.push(r.get('id'));
		}, this);
		this.message({
			title: this.localize('deleteRules'),
			msg: this.localize(this.rulesSelections.length > 0 ? 'deleteRulesMsg' : 'deleteRuleMsg'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteRules'),
				no: this.localize('cancel')
			},
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.set('reqCount', this.reqCount + 1);
				this.ajax({
					url: '/resolve/service/resolutionrouting/rule/delete/id',
					params: {
						ids: ids
					},
					scope: this,
					success: function(resp) {
						var respData = RS.common.parsePayload(resp);
						if (!respData.success) {
							clientVM.displayError(this.localize('deleteRulesError', respData.message));
							return;
						}
						this.ruleStore.load();
						clientVM.displaySuccess(this.localize('deleteRuleSuccess'));
						this.expandToNode(this.selectedModule);
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					},
					callback: function() {
						this.set('reqCount', this.reqCount - 1);
					}
				});
			}
		})
	},
	deleteRulesByQuery: function() {
		this.message({
			title: this.localize('deleteRules'),
			msg: this.localize('deleteRulesThroughoutPages'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteRules'),
				no: this.localize('cancel')
			},
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.set('reqCount', this.reqCount + 1);
				this.ajax({
					url: '/resolve/service/resolutionrouting/rule/delete/query',
					params: {
						filter: Ext.encode(this.ruleStore.commonSearchFilter.getWhereFilter())
					},
					scope: this,
					success: function(resp) {
						var respData = RS.common.parsePayload(resp);
						if (!respData.success) {
							clientVM.displayError(this.localize('deleteRulesError', respData.message));
							return;
						}
						this.ruleStore.load();
						clientVM.displaySuccess(this.localize('deleteRuleSuccess'));
						this.expandToNode(this.selectedModule);
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					},
					callback: function() {
						this.set('reqCount', this.reqCount - 1);
					}
				});
			}
		})

	},
	deleteRules: function() {
		if (!this.selectAll)
			this.deleteRulesById();
		else
			this.deleteRulesByQuery();
	},
	deleteRulesIsEnabled$: function() {
		return this.reqCount == 0 && this.rulesSelections.length > 0;
	},
	expandingToNode: false,
	expandToNode: function(node) {
		node = node || this.treeStore.getRootNode();
		this.set('expandingToNode', true);
		var paths = node.getPath('module').substring(5).split(/\//);
		var current = 0;
		this.treeStore.getRootNode().removeAll();
		var me = this;

		function recursiveExpand(currentNode) {
			me.expandModuleNode(currentNode, function() {
				current++;
				if (current > paths.length - 1)
					return;
				var currentPath = paths.slice(0, current).join('\/');
				//can be more efficient
				var end = true;
				me.treeStore.getRootNode().eachChild(function(node) {
					if (currentPath == node.getPath('module').substring(5)) {
						end = false;
						node.expand();
						recursiveExpand(node);
					}
				});
				if (end)
					me.set('expandingToNode', false);
			});
		}

		recursiveExpand(this.treeStore.getRootNode());
	},
	importCSV: function() {
		this.open({
			mtype: 'RS.resolutionrouting.Bulk',
			bulkType: 0
		});
	},
	// TYPE_IMPORT: 0,
	// TYPE_EXPORT: 1,
	// TYPE_ACTIVATE: 2,
	// TYPE_DEACTIVATE: 3,
	exportCSV: function() {
		this.open({
			mtype: 'RS.resolutionrouting.Bulk',
			bulkType: 1
		});
	},
	exportCSVIsEnabled$: function() {
		return this.rulesSelections.length > 0;
	},
	recordEdit: function(record) {
		var data = Ext.apply({}, record.data);
		delete data['schema.name'];
		this.set('reqCount', this.reqCount + 1);
		this.ajax({
			url: '/resolve/service/resolutionrouting/rule/save',
			jsonData: data,
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp)
				if (!respData.success) {
					clientVM.displayError(this.localize('saveRuleError', respData.message));
					return;
				}
				record.commit();
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
		})
	},
	selectAll: false,
	pageSelections: [],
	selectAllAcrossPages: function(filters) {
		this.set('pageSelections', filters);
		this.set('selectAll', true);
	},
	selectionChanged: function() {
		this.set('selectAll', false);
		this.set('pageSelections', []);
	},
	activateMapping: function() {
		if (this.selectAll)
			this.toggleMappingActivationsByQuery(true);
		else
			this.toggleMappingActivationsById(true);
	},
	activateMappingIsEnabled$: function() {
		return this.rulesSelections.length > 0;
	},
	deactivateMapping: function() {
		if (this.selectAll)
			this.toggleMappingActivationsByQuery(false);
		else
			this.toggleMappingActivationsById(false);
	},
	deactivateMappingIsEnabled$: function() {
		return this.rulesSelections.length > 0;
	},
	toggleMappingActivationsById: function(activate) {
		this.open({
			mtype: 'RS.resolutionrouting.Bulk',
			bulkType: activate ? 2 : 3
		});
	},
	toggleMappingActivationsByQuery: function(activate) {
		this.open({
			mtype: 'RS.resolutionrouting.Bulk',
			bulkType: activate ? 2 : 3
		});
	},

	orgChange: function() {
		this.treeStore.load();
		this.ruleStore.load();
	},

	beforeDestroyComponent : function() {
		clientVM.removeListener('orgchange', this.orgChange);
	}
});