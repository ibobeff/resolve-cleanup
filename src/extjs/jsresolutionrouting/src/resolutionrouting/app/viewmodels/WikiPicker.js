glu.defModel('RS.resolutionrouting.WikiPicker', {
	mock: false,
	title$: function() {
		return this.isRunbook ? this.localize('runbook') : this.localize('wiki');
	},
	runbookGrid: null,
	isRunbook: false,
	runbookTree: {
		mtype: 'treestore',
		fields: ['id', 'unamespace'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/nsadmin/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	moduleColumns: [],
	namespace: '',
	namespaceIsValid$: function() {
		if (/^\s*$/.test(this.namespace))
			return this.localize('invalidNamespaceConvention');
		if (this.builderOnly) {
			var splittedFullName = this.parentVM.parentVM.parentVM.name.split('\.');
			if (splittedFullName[0] == this.namespace && splittedFullName[1] == this.name)
				return this.localize('cannotUseCurrentWiki');
		}
		return true;
	},
	name: '',
	nameIsValid$: function() {
		if (/^\s*$/.test(this.name))
			return this.localize('invalidNameConvention');
		if (this.builderOnly) {
			var splittedFullName = this.parentVM.parentVM.parentVM.name.split('\.');
			if (splittedFullName[0] == this.namespace && splittedFullName[1] == this.name)
				return this.localize('cannotUseCurrentWiki');
		}
		return true;
	},
	appendExtraParams: function() {
		this.runbookGrid.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {};
			var filter = [];
			if (this.selectedNamespace && this.selectedNamespace.toLowerCase() != 'all')
				filter.push({
					field: 'unamespace',
					type: 'auto',
					condition: 'equals',
					value: this.selectedNamespace
				});
			if (this.isRunbook) {
				filter.push({
					field: "uhasActiveModel",
					type: "bool",
					condition: "equals",
					value: true
				});
			}
			Ext.apply(operation.params, {
				filter: Ext.encode(filter)
			})
		}, this);
	},

	init: function() {
		if (this.mock)
			this.backend = RS.actiontask.createMockBackend(true)
		var store = Ext.create('Ext.data.Store', {
			mtype: 'store',
			sorters: ['ufullname'],
			fields: ['id', 'ufullname', 'usummary', 'uresolutionBuilderId', 'uisRoot'].concat(RS.common.grid.getSysFields()),
			remoteSort: true,
			proxy: {
				type: 'ajax',
				url: (!this.runbookOnly && !this.builderOnly) ? '/resolve/service/wikiadmin/list' : '/resolve/service/wikiadmin/listRunbooks',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});
		this.set('runbookGrid', store);
		this.appendExtraParams();
		this.set('title', this.title || this.localize('title'));

		this.set('moduleColumns', [{
			xtype: 'treecolumn',
			dataIndex: 'unamespace',
			text: this.localize('namespace'),
			flex: 1
		}])

		this.runbookTree.on('load', function(store, node, records) {
			Ext.Array.forEach(records || [], function(record) {
				record.set({
					leaf: true
				})
			})
		})

		this.runbookTree.setRootNode({
			unamespace: this.localize('all'),
			expanded: true
		})

		this.runbookGrid.load()
	},

	selected: null,
	selectedNamespace: '',

	searchDelimiter: '',

	when_selected_menu_node_id_changes_update_grid: {
		on: ['selectedNamespaceChanged'],
		action: function() {
			this.runbookGrid.load()
		}
	},

	menuPathTreeSelectionchange: function(selected, eOpts) {
		if (selected.length > 0) {
			var nodeId = selected[0].get('unamespace')
			this.set('selectedNamespace', nodeId)
		}
	},

	dumper: null,
	dump: function() {
		if (Ext.isFunction(this.dumper))
			this.dumper(this.selected);
		this.doClose()
	},
	dumpIsEnabled$: function() {
		return !!this.selected
	},
	cancel: function() {
		if (this.activeItem == 1)
			this.selectExistingRunbook()
		else
			this.doClose()
	},
	editRunbook: function(name) {
		if (this.runbookOnly)
			clientVM.handleNavigation({
				modelName: 'RS.wiki.Main',
				target: '_blank',
				params: {
					activeTab: (this.runbookOnly ? 2 : 0),
					name: name
				}
			})
		else if (this.builderOnly) {
			var idx = this.runbookGrid.findExact('ufullname', name);
			var r = this.runbookGrid.getAt(idx);
			clientVM.handleNavigation({
				modelName: 'RS.wiki.resolutionbuilder.AutomationMeta',
				target: '_blank',
				params: {
					id: r.get('uresolutionBuilderId')
				}
			});
		}
	}
});