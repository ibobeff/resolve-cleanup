glu.defModel('RS.resolutionrouting.Bulk',{
	mixins: ['FileUploadManager'],
	api: {
		upload: '/resolve/service/resolutionrouting/rule/csv/import/upload',
		list: ''
	},

	allowedExtensions: ['csv'],
	fineUploaderTemplateHidden: false,
	checkForDuplicates: false,
	allowMultipleFiles: false,
	uploaderAdditionalClass: 'resolutionrouting',

	fileOnSubmittedCallback: function(manager, filename) {
		this.uploader.setParams({
			override: this.override,
			forceImport: this.forceImport
		});
	},

	fileOnUploadCallback: function(manager, filename) {
		this.set('wait', true);
	},

	fileOnCompleteCallback: function(manager, filename, response, xhr) {
        this.set('wait', false);

		this.importStart(manager, response.data);
	},

	title: '',
	bulkType: 0,
	TYPE_IMPORT: 0,
	TYPE_EXPORT: 1,
	TYPE_ACTIVATE: 2,
	TYPE_DEACTIVATE: 3,
	wait: false,
	status: null,
	busyText: '',
	progress: 0.0,
	keepPolling: false,
	override: false,
	forceImport: false,
	otherUsersAreDoingBulk: false,
	currentTid: '',
	overrideIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT && !this.wait;
	},
	forceImportIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT && !this.wait;
	},
	init: function() {
		this.set('dragHereText', this.localize('dropArea'));
		switch (this.bulkType) {
			case this.TYPE_EXPORT:
				this.set('title', this.localize('exportTitle'));
				this.set('fineUploaderTemplateHidden', true);
				break;
			case this.TYPE_IMPORT:
				this.set('title', this.localize('importTitle'));
				this.set('fineUploaderTemplateHidden', false);
				break;
			case this.TYPE_ACTIVATE:
				this.set('title', this.localize('activationTitle'));
				this.set('fineUploaderTemplateHidden', true);
				break;
			case this.TYPE_DEACTIVATE:
				this.set('title', this.localize('deactivationTitle'));
				this.set('fineUploaderTemplateHidden', true);
		}
		if (this.bulkType == this.TYPE_EXPORT)
			this.exportCSV();
		else if (this.bulkType == this.TYPE_ACTIVATE)
			this.activateMapping();
		else if (this.bulkType == this.TYPE_DEACTIVATE)
			this.deactivateMapping();

	},
	importCSV: function() {},
	importCSVIsEnabled$: function() {
		return !this.wait;
	},
	importCSVIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT;
	},
	beforeImport: function(nextAction) {
		this.ajax({
			url: '/resolve/service/resolutionrouting/csv/import/before',
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('checkBeforeImportError', respData.message));
					return;
				}
				nextAction.call(this, respData);
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	importStart: function(uploader, respData) {
		this.set('currentTid', respData.TASK_ID)
		if (respData.EVT_TYPE) {
			this.set('previousOp', 'importStart');
			this.set('opOnCurrentNode', respData.EVT_IS_CURRENT_NODE == 'true');
			clientVM.displayMessage(
				this.localize('bulkOpInProgress'),
				this.localize(
					respData.EVT_TYPE, [
						respData.EVT_USERNAME,
						respData.EVT_RSVIEW_ID,
						this.opOnCurrentNode ? this.localize('currentNode') : this.localize('otherNode')
					]
				)
			);
			if (!this.opOnCurrentNode) {
				this.close();
				return;
			}
			this.adjustPollingMode(respData.EVT_TYPE);
			return;
		}
		this.set('wait', true);
		this.set('keepPolling', true);
		this.set('taskStatus', []);
		this.pollBulk();
	},
	processedStatus$: function() {
		var lines = [];
		Ext.each(this.taskStatus, function(status) {
			if (status.finished) {
				if (this.type != this.TYPE_IMPORT)
					lines.push('Finished.');
				return;
			}
			var color = 'red';
			if (status.state == 'success')
				color = 'green';
			if (status.state.toLowerCase() == 'global') {
				lines.push(Ext.String.format('<pre style="color:red;">Global:{0}', status.detail));
				return;
			}
			if (this.bulkType == this.TYPE_IMPORT) {
				lines.push(Ext.String.format('<pre style="color:{0};">{1}[{2}]:{3}{4}</pre>', color, status.state, status.line_no, status.line, (status.detail ? '<br/>' + status.detail : '')));
				return;
			}
			lines.push(Ext.String.format('<pre style="color:{0};">{1}:{2}, {3}, {4}{5}</pre>', color, status.state, status.id, status.rid, status.schema, (status.detail ? '<br/>' + status.detail : '')));
		}, this);
		return lines.join('<br/>')
	},
	adjustPollingMode: function(evtType) {
		this.set('keepPolling', true);
		this.set('wait', true);
		switch (evtType) {
			case 'EVT_IMPORT':
				this.set('bulkType', this.TYPE_IMPORT);
				this.set('title', this.localize('importTitle'));
				break;
			case 'EVT_EXPORT':
				this.set('bulkType', this.TYPE_EXPORT);
				this.set('title', this.localize('exportTitle'));
				break;
			case 'EVT_ACTIVATION':
				this.set('bulkType', this.TYPE_ACTIVATE);
				this.set('title', this.localize('activationTitle'));
				break;
			case 'EVT_DEACTIVATION':
				this.set('bulkType', this.TYPE_DEACTIVATE);
				this.set('title', this.localize('deactivationTitle'));
				break;
		}
		this.pollBulk();
	},
	opOnCurrentNode: true,
	previousOp: '',
	activateMapping: function(force) {
		this.asyncAction({
			url: '/resolve/service/resolutionrouting/rule/activation/' + (this.parentVM.selectAll ? 'query' : 'id'),
			params: {
				isActivated: true,
				force: force === true
			},
			submitReqError: 'submitExportReqError',
			onRequestSubmitted: function(respData) {
				this.set('currentTid', respData.data.TASK_ID);
				if (respData.data.EVT_TYPE) {
					this.set('previousOp', 'activateMapping');
					this.set('opOnCurrentNode', respData.data.EVT_IS_CURRENT_NODE == 'true');
					clientVM.displayMessage(
						this.localize('bulkOpInProgress'),
						this.localize(
							respData.data.EVT_TYPE, [
								respData.data.EVT_USERNAME,
								respData.data.EVT_RSVIEW_ID,
								this.opOnCurrentNode ? this.localize('currentNode') : this.localize('otherNode')
							]
						)
					);
					if (!respData.data || !this.opOnCurrentNode) {
						this.close();
						return;
					}
					this.adjustPollingMode(respData.data.EVT_TYPE);
					return;
				}
				this.set('wait', true);
				this.set('keepPolling', true);
				this.pollBulk();
			}
		});
	},
	deactivateMapping: function(force) {
		this.asyncAction({
			url: '/resolve/service/resolutionrouting/rule/activation/' + (this.parentVM.selectAll ? 'query' : 'id'),
			params: {
				isActivated: false,
				force: force === true
			},
			submitReqError: 'submitExportReqError',
			onRequestSubmitted: function(respData) {
				this.set('currentTid', respData.data.TASK_ID);
				if (respData.data.EVT_TYPE) {
					this.set('previousOp', 'deactivateMapping');
					this.set('opOnCurrentNode', respData.data.EVT_IS_CURRENT_NODE == 'true');
					clientVM.displayMessage(
						this.localize('bulkOpInProgress'),
						this.localize(
							respData.data.EVT_TYPE, [
								respData.data.EVT_USERNAME,
								respData.data.EVT_RSVIEW_ID,
								this.opOnCurrentNode ? this.localize('currentNode') : this.localize('otherNode')
							]
						)
					);
					if (!respData.data || !this.opOnCurrentNode) {
						this.close();
						return;
					}
					this.adjustPollingMode(respData.data.EVT_TYPE);
					return;
				}
				this.set('wait', true);
				this.set('keepPolling', true);
				this.pollBulk();
			}
		});
	},
	exportCSV: function(force) {
		this.asyncAction({
			url: '/resolve/service/resolutionrouting/rule/csv/export/init/' + (this.parentVM.selectAll ? 'query' : 'id'),
			params: {
				force: force === true
			},
			submitReqError: 'submitExportReqError',
			onRequestSubmitted: function(respData) {
				this.set('currentTid', respData.data.TASK_ID)
				if (respData.data.EVT_TYPE) {
					this.set('previousOp', 'exportCSV');
					this.set('opOnCurrentNode', respData.data.EVT_IS_CURRENT_NODE == 'true');
					clientVM.displayMessage(
						this.localize('bulkOpInProgress'),
						this.localize(
							respData.data.EVT_TYPE, [
								respData.data.EVT_USERNAME,
								respData.data.EVT_RSVIEW_ID,
								this.opOnCurrentNode ? this.localize('currentNode') : this.localize('otherNode')
							]
						)
					);
					if (!respData.data || !this.opOnCurrentNode) {
						this.close();
						return;
					}
					this.adjustPollingMode(respData.data.EVT_TYPE);
					return
				}
				this.set('keepPolling', true);
				this.downloadCSV(this.currentTid);
			}
		}, true);
	},
	asyncAction: function(config, canRedo) {
		var url = config.url;
		var ids = [];
		this.set('keepPolling', true);
		Ext.each(this.parentVM.rulesSelections, function(rule) {
			ids.push(rule.get('id'));
		});
		var filters = this.parentVM.ruleStore.commonSearchFilter.getWhereFilter();
		var modulePath = '';
		if (this.parentVM.selectedModule)
			modulePath = this.parentVM.selectedModule.getPath('module', '.').substring(5) + (this.parentVM.selectedModule.isLeaf() ? '' : '.')
		if (modulePath && modulePath != '.')
			filters.push({
				field: 'module',
				type: 'auto',
				condition: 'equals',
				value: modulePath
			});
		var params = Ext.applyIf((this.parentVM.selectAll ? {
			filter: Ext.encode(filters)
		} : {
			ids: ids
		}), config.params);
		this.ajax({
			url: url,
			params: params,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					this.set('wait', false);
					clientVM.displayError(this.localize(config.submitReqError, respData.message));
					return;
				}
				config.onRequestSubmitted.call(this, respData);
			},
			failure: function(f) {
				clientVM.displayFailure(r);
			}
		});
	},
	progressIsVisible$: function() {
		return this.bulkType != this.TYPE_IMPORT || this.wait;
	},
	checkExportFinish: function(data) {
		var finished = false;
		Ext.each(data.status, function(st) {
			if (st.finished)
				finished = true;
		})
		if (data.hasGone)
			finished = true;
		return data.percent == 1.0 || finished;
	},
	pollBulk: function() {
		if (!this.keepPolling)
			return;
		this.ajax({
			url: '/resolve/service/resolutionrouting/rule/bulk/poll',
			params: {
				tid: this.currentTid
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('bulkPollError', respData.message));
					return;
				}
				if (respData.data.percent != 1 && !respData.data.hasGone) {
					this.set('progress', respData.data.percent);
					this.set('taskStatus', respData.data.status || []);
					var me = this;
					setTimeout(function() {
						if (me.keepPolling)
							me.pollBulk();
					}, 1000);
					return;
				}
				this.set('progress', 1);
				this.set('taskStatus', respData.data.status || this.taskStatus);
				this.set('previousOp', '');
				if (this.bulkType != this.TYPE_EXPORT) {
					this.parentVM.set('rulesSelections', []);
					this.parentVM.ruleStore.load();
					this.parentVM.expandToNode(this.parentVM.selectedModule);
				}
				this.set('fineUploaderTemplateHidden', true);
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	downloadCSV: function() {
		this.set('wait', true);
        this.pollBulk();
        //downloadFile('/resolve/service/resolutionrouting/rule/csv/export/download?tid=' + this.currentTid);
        var me = this;

        clientVM.getCSRFToken_ForURI('/resolve/service/resolutionrouting/rule/csv/export/download', function(data){
            var csrftoken = '&' + data[0] + '=' + data[1];
            downloadFile('/resolve/service/resolutionrouting/rule/csv/export/download?tid=' + me.currentTid + csrftoken);
        })
	},
	exportCSVIsEnabled$: function() {
		return !this.wait;
	},
	browse: function() {
	},
	browseIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT && !this.wait;
	},
	exportCSVIsVisible$: function() {
		return this.type != this.TYPE_IMPORT;
	},
	dropAreaIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT && !this.wait;
	},
	close: function() {
		this.set('keepPolling', false);
		this.doClose();
	},
	taskStatus: [],
	statusDisplayIsVisible$: function() {
		return this.taskStatus && this.taskStatus.length > 1;
	},
	uploadError: function(resp) {
		var respData = Ext.decode(resp.response);
		clientVM.displayError(respData.message || this.localize('fileUploadError', resp.message));
		this.set('keepPolling', true);
		this.pollBulk();
	},
	abort: function() {
		this.abortBulk(this.currentTid);
	},
	abortBulk: function(tid) {
		this.ajax({
			url: '/resolve/service/resolutionrouting/rule/bulk/abort',
			params: {
				tid: tid
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('cannotAbortProcess', respData.message));
					return;
				}
				this.set('currentTid', '');
				this.set('keepPolling', false);
				this.set('previousOp', '');
				clientVM.displaySuccess(this.localize('abortSuccess'));
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	abortIsVisible$: function() {
		return this.wait && this.opOnCurrentNode;
	},
	overrideLock: function() {
		this[this.previousOp](true);
	},
	overrideLockIsVisible$: function() {
		return this.wait && this.bulkType != this.TYPE_IMPORT && this.previousOp;
	},
	backToImport: function() {
		this.set('bulkType', this.TYPE_IMPORT);
		this.set('wait', false);
		this.set('keepPolling', false);
		this.set('currentTid', '');
		this.set('previousOp', '');
	},
	backToImportIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT && this.wait && this.previousOp;
	}
});

glu.defModel('RS.resolutionrouting.FileUploadManagerFactory', function(cfg) {
	Ext.applyIf(cfg, RS.common.viewmodels.FileUploadManager);
	return cfg;
});

/*
glu.defModel('RS.resolutionrouting.Bulk', {
	title: '',
	bulkType: 0,
	TYPE_IMPORT: 0,
	TYPE_EXPORT: 1,
	TYPE_ACTIVATE: 2,
	TYPE_DEACTIVATE: 3,
	wait: false,
	status: null,
	busyText: '',
	progress: 0.0,
	keepPolling: false,
	override: false,
	forceImport: false,
	otherUsersAreDoingBulk: false,
	currentTid: '',
	overrideIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT && !this.wait;
	},
	forceImportIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT && !this.wait;
	},
	init: function() {
		switch (this.bulkType) {
			case this.TYPE_EXPORT:
				this.set('title', this.localize('exportTitle'));
				break;
			case this.TYPE_IMPORT:
				this.set('title', this.localize('importTitle'));
				break;
			case this.TYPE_ACTIVATE:
				this.set('title', this.localize('activationTitle'));
				break;
			case this.TYPE_DEACTIVATE:
				this.set('title', this.localize('deactivationTitle'));
		}
		if (this.bulkType == this.TYPE_EXPORT)
			this.exportCSV();
		else if (this.bulkType == this.TYPE_ACTIVATE)
			this.activateMapping();
		else if (this.bulkType == this.TYPE_DEACTIVATE)
			this.deactivateMapping();

	},
	importCSV: function() {},
	importCSVIsEnabled$: function() {
		return !this.wait;
	},
	importCSVIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT;
	},
	beforeImport: function(nextAction) {
		this.ajax({
			url: '/resolve/service/resolutionrouting/csv/import/before',
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('checkBeforeImportError', respData.message));
					return;
				}
				nextAction.call(this, respData);
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	importStart: function(uploader, respData) {
		this.set('currentTid', respData.TASK_ID)
		if (respData.EVT_TYPE) {
			this.set('previousOp', 'importStart');
			this.set('opOnCurrentNode', respData.EVT_IS_CURRENT_NODE == 'true');
			clientVM.displayMessage(
				this.localize('bulkOpInProgress'),
				this.localize(
					respData.EVT_TYPE, [
						respData.EVT_USERNAME,
						respData.EVT_RSVIEW_ID,
						this.opOnCurrentNode ? this.localize('currentNode') : this.localize('otherNode')
					]
				)
			);
			if (!this.opOnCurrentNode) {
				this.close();
				return;
			}
			this.adjustPollingMode(respData.EVT_TYPE);
			return;
		}
		this.set('wait', true);
		this.set('keepPolling', true);
		this.set('taskStatus', []);
		this.pollBulk();
	},
	processedStatus$: function() {
		var lines = [];
		Ext.each(this.taskStatus, function(status) {
			if (status.finished) {
				if (this.type != this.TYPE_IMPORT)
					lines.push('Finished.');
				return;
			}
			var color = 'red';
			if (status.state == 'success')
				color = 'green';
			if (status.state.toLowerCase() == 'global') {
				lines.push(Ext.String.format('<pre style="color:red;">Global:{0}', status.detail));
				return;
			}
			if (this.bulkType == this.TYPE_IMPORT) {
				lines.push(Ext.String.format('<pre style="color:{0};">{1}[{2}]:{3}{4}</pre>', color, status.state, status.line_no, status.line, (status.detail ? '<br/>' + status.detail : '')));
				return;
			}
			lines.push(Ext.String.format('<pre style="color:{0};">{1}:{2}, {3}, {4}{5}</pre>', color, status.state, status.id, status.rid, status.schema, (status.detail ? '<br/>' + status.detail : '')));
		}, this);
		return lines.join('<br/>')
	},
	adjustPollingMode: function(evtType) {
		this.set('keepPolling', true);
		this.set('wait', true);
		switch (evtType) {
			case 'EVT_IMPORT':
				this.set('bulkType', this.TYPE_IMPORT);
				this.set('title', this.localize('importTitle'));
				break;
			case 'EVT_EXPORT':
				this.set('bulkType', this.TYPE_EXPORT);
				this.set('title', this.localize('exportTitle'));
				break;
			case 'EVT_ACTIVATION':
				this.set('bulkType', this.TYPE_ACTIVATE);
				this.set('title', this.localize('activationTitle'));
				break;
			case 'EVT_DEACTIVATION':
				this.set('bulkType', this.TYPE_DEACTIVATE);
				this.set('title', this.localize('deactivationTitle'));
				break;
		}
		this.pollBulk();
	},
	opOnCurrentNode: true,
	previousOp: '',
	activateMapping: function(force) {
		this.asyncAction({
			url: '/resolve/service/resolutionrouting/rule/activation/' + (this.parentVM.selectAll ? 'query' : 'id'),
			params: {
				isActivated: true,
				force: force === true
			},
			submitReqError: 'submitExportReqError',
			onRequestSubmitted: function(respData) {
				this.set('currentTid', respData.data.TASK_ID);
				if (respData.data.EVT_TYPE) {
					this.set('previousOp', 'activateMapping');
					this.set('opOnCurrentNode', respData.data.EVT_IS_CURRENT_NODE == 'true');
					clientVM.displayMessage(
						this.localize('bulkOpInProgress'),
						this.localize(
							respData.data.EVT_TYPE, [
								respData.data.EVT_USERNAME,
								respData.data.EVT_RSVIEW_ID,
								this.opOnCurrentNode ? this.localize('currentNode') : this.localize('otherNode')
							]
						)
					);
					if (!respData.data || !this.opOnCurrentNode) {
						this.close();
						return;
					}
					this.adjustPollingMode(respData.data.EVT_TYPE);
					return;
				}
				this.set('wait', true);
				this.set('keepPolling', true);
				this.pollBulk();
			}
		});
	},
	deactivateMapping: function(force) {
		this.asyncAction({
			url: '/resolve/service/resolutionrouting/rule/activation/' + (this.parentVM.selectAll ? 'query' : 'id'),
			params: {
				isActivated: false,
				force: force === true
			},
			submitReqError: 'submitExportReqError',
			onRequestSubmitted: function(respData) {
				this.set('currentTid', respData.data.TASK_ID);
				if (respData.data.EVT_TYPE) {
					this.set('previousOp', 'deactivateMapping');
					this.set('opOnCurrentNode', respData.data.EVT_IS_CURRENT_NODE == 'true');
					clientVM.displayMessage(
						this.localize('bulkOpInProgress'),
						this.localize(
							respData.data.EVT_TYPE, [
								respData.data.EVT_USERNAME,
								respData.data.EVT_RSVIEW_ID,
								this.opOnCurrentNode ? this.localize('currentNode') : this.localize('otherNode')
							]
						)
					);
					if (!respData.data || !this.opOnCurrentNode) {
						this.close();
						return;
					}
					this.adjustPollingMode(respData.data.EVT_TYPE);
					return;
				}
				this.set('wait', true);
				this.set('keepPolling', true);
				this.pollBulk();
			}
		});
	},
	exportCSV: function(force) {
		this.asyncAction({
			url: '/resolve/service/resolutionrouting/rule/csv/export/init/' + (this.parentVM.selectAll ? 'query' : 'id'),
			params: {
				force: force === true
			},
			submitReqError: 'submitExportReqError',
			onRequestSubmitted: function(respData) {
				this.set('currentTid', respData.data.TASK_ID)
				if (respData.data.EVT_TYPE) {
					this.set('previousOp', 'exportCSV');
					this.set('opOnCurrentNode', respData.data.EVT_IS_CURRENT_NODE == 'true');
					clientVM.displayMessage(
						this.localize('bulkOpInProgress'),
						this.localize(
							respData.data.EVT_TYPE, [
								respData.data.EVT_USERNAME,
								respData.data.EVT_RSVIEW_ID,
								this.opOnCurrentNode ? this.localize('currentNode') : this.localize('otherNode')
							]
						)
					);
					if (!respData.data || !this.opOnCurrentNode) {
						this.close();
						return;
					}
					this.adjustPollingMode(respData.data.EVT_TYPE);
					return
				}
				this.set('keepPolling', true);
				this.downloadCSV(this.currentTid);
			}
		}, true);
	},
	asyncAction: function(config, canRedo) {
		var url = config.url;
		var ids = [];
		this.set('keepPolling', true);
		Ext.each(this.parentVM.rulesSelections, function(rule) {
			ids.push(rule.get('id'));
		});
		var filters = this.parentVM.ruleStore.commonSearchFilter.getWhereFilter();
		var modulePath = '';
		if (this.parentVM.selectedModule)
			modulePath = this.parentVM.selectedModule.getPath('module', '.').substring(5) + (this.parentVM.selectedModule.isLeaf() ? '' : '.')
		if (modulePath && modulePath != '.')
			filters.push({
				field: 'module',
				type: 'auto',
				condition: 'equals',
				value: modulePath
			});
		var params = Ext.applyIf((this.parentVM.selectAll ? {
			filter: Ext.encode(filters)
		} : {
			ids: ids
		}), config.params);
		this.ajax({
			url: url,
			params: params,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					this.set('wait', false);
					clientVM.displayError(this.localize(config.submitReqError, respData.message));
					return;
				}
				config.onRequestSubmitted.call(this, respData);
			},
			failure: function(f) {
				clientVM.displayFailure(r);
			}
		});
	},
	progressIsVisible$: function() {
		return this.bulkType != this.TYPE_IMPORT || this.wait;
	},
	checkExportFinish: function(data) {
		var finished = false;
		Ext.each(data.status, function(st) {
			if (st.finished)
				finished = true;
		})
		if (data.hasGone)
			finished = true;
		return data.percent == 1.0 || finished;
	},
	pollBulk: function() {
		if (!this.keepPolling)
			return;
		this.ajax({
			url: '/resolve/service/resolutionrouting/rule/bulk/poll',
			params: {
				tid: this.currentTid
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('bulkPollError', respData.message));
					return;
				}
				if (respData.data.percent != 1 && !respData.data.hasGone) {
					this.set('progress', respData.data.percent);
					this.set('taskStatus', respData.data.status || []);
					var me = this;
					setTimeout(function() {
						if (me.keepPolling)
							me.pollBulk();
					}, 1000);
					return;
				}
				this.set('progress', 1);
				this.set('taskStatus', respData.data.status || this.taskStatus);
				this.set('previousOp', '');
				if (this.bulkType != this.TYPE_EXPORT) {
					this.parentVM.set('rulesSelections', []);
					this.parentVM.ruleStore.load();
					this.parentVM.expandToNode(this.parentVM.selectedModule);
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	downloadCSV: function() {
		this.set('wait', true);
		this.pollBulk();
		downloadFile('/resolve/service/resolutionrouting/rule/csv/export/download?tid=' + this.currentTid);
	},
	exportCSVIsEnabled$: function() {
		return !this.wait;
	},
	browseIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT && !this.wait;
	},
	exportCSVIsVisible$: function() {
		return this.type != this.TYPE_IMPORT;
	},
	dropAreaIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT && !this.wait;
	},
	close: function() {
		this.set('keepPolling', false);
		this.doClose();
	},
	taskStatus: [],
	statusDisplayIsVisible$: function() {
		return this.taskStatus && this.taskStatus.length > 1;
	},
	uploadError: function(resp) {
		var respData = Ext.decode(resp.response);
		clientVM.displayError(respData.message || this.localize('fileUploadError', resp.message));
		this.set('keepPolling', true);
		this.pollBulk();
	},
	abort: function() {
		this.abortBulk(this.currentTid);
	},
	abortBulk: function(tid) {
		this.ajax({
			url: '/resolve/service/resolutionrouting/rule/bulk/abort',
			params: {
				tid: tid
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('cannotAbortProcess', respData.message));
					return;
				}
				this.set('currentTid', '');
				this.set('keepPolling', false);
				this.set('previousOp', '');
				clientVM.displaySuccess(this.localize('abortSuccess'));
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	abortIsVisible$: function() {
		return this.wait && this.opOnCurrentNode;
	},
	overrideLock: function() {
		this[this.previousOp](true);
	},
	overrideLockIsVisible$: function() {
		return this.wait && this.bulkType != this.TYPE_IMPORT && this.previousOp;
	},
	backToImport: function() {
		this.set('bulkType', this.TYPE_IMPORT);
		this.set('wait', false);
		this.set('keepPolling', false);
		this.set('currentTid', '');
		this.set('previousOp', '');
	},
	backToImportIsVisible$: function() {
		return this.bulkType == this.TYPE_IMPORT && this.wait && this.previousOp;
	}
});
*/
