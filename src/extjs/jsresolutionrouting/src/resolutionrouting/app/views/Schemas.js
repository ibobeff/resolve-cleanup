glu.defView('RS.resolutionrouting.Schemas', {
	layout: 'fit',
	padding : 15,
	items: [{
		disabled: '@{wait}',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		displayName: '~~resolutionRoutingSchema~~',
		dockedItems: [{
			xtype: 'toolbar',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: ['createSchema', 'deleteSchemas']
		}],
		name: 'schemas',
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true,
			useWindowParams: false
		}, {
			ptype: 'pager'
		}],
		viewConfig: {
			plugins: [{
				ptype: 'gridviewdragdrop',
				containerScroll: true
			}],
			listeners: {
				drop: function() {
					this.panel.fireEvent('reorder');
				}
			}
		},
		stateId: 'rsresolutionroutingschema',
		store: '@{store}',
		columns: [{
			header: '~~name~~',
			dataIndex: 'name',
			filterable: true,
			sortable: false
		}, {
			header: '~~fields~~',
			dataIndex: 'jsonFields',
			flex: 1,
			filterable: true,
			sortable: false,
			renderer: function(value) {
				var fields = [];
				try {
					Ext.each(Ext.decode(value), function(f) {
						fields.push(Ext.util.Format.htmlEncode(f.name));
					});
				} catch (e) {}
				return fields.join(',');
			}
		}, {
			header: '~~order~~',
			dataIndex: 'order',
			width: 90,
			filterable: false,
			sortable: true,
			hidden: true
		}, {
			header: '~~gatewayQueues~~',
			dataIndex: 'gatewayQueues',
			flex: 1,
			filterable: true,
			sortable: false,
			renderer: function(value) {
				try {
					return Ext.decode(value).join(',');
				} catch (e) {}
				return '';
			}
		}, {
			header: '~~description~~',
			dataIndex: 'description',
			filterable: true,
			sortable: false,
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		},{
			header: '~~organization~~',
			dataIndex: 'sysOrg',
			initialShow: true,
			sortable: true,
			renderer: function(value, metaData, record) {
				if (value) {
					if(!record.store.id2NameMap) {
						record.store.id2NameMap = {};
						clientVM.user.orgs.forEach(function(e) {
							if(e.uname != 'None')
								record.store.id2NameMap[e.id] = e.uname;
						});
					}
					return RS.common.grid.plainRenderer() (record.store.id2NameMap[value]);
				}
				return '';
			}
		}].concat(RS.common.grid.getSysColumns()),
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'id'
		},
		listeners: {
			editAction: '@{editSchema}',
			reorder: '@{redoOrder}',
			beforedestroy : '@{beforeDestroyComponent}'
		}
	}]
});