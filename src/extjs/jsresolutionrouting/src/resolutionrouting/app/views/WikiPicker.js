glu.defView('RS.resolutionrouting.WikiPicker', {
	layout: 'border',
	padding : 15,		
	title: '@{title}',
	cls : 'rs-modal-popup',
	modal: true,
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}],
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockectoolbar',
		name: 'actionBar',
		items: []
	}],
	items: [{
		region: 'west',
		width: 200,
		maxWidth: 300,
		split : true,
		xtype: 'treepanel',
		cls : 'rs-grid-dark',
		columns: '@{moduleColumns}',
		name: 'runbookTree',
		listeners: {
			selectionchange: '@{menuPathTreeSelectionchange}'
		},
		margin : {
			bottom : 10
		}
	}, {
		region: 'center',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		displayName: '@{title}',
		name: 'runbookGrid',
		columns: [{
			header: '~~name~~',
			dataIndex: 'ufullname',
			filterable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()),
		multiSelect: false,
		selected: '@{selected}',
		autoScroll: true,
		viewConfig: {
			enableTextSelection: true
		},
		plugins: [{
			ptype: 'resolveexpander',
			pluginId: 'rowExpander',
			rowBodyTpl: new Ext.XTemplate('<div resolveId="resolveRowBody" style="color:#333"><span style="font-weight:bold">Summary:</span><br/><br/>&nbsp&nbsp&nbsp&nbsp{usummary:htmlEncode}</div>')
		}],		
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTooltip: RS.common.locale.editColumnTooltip,
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'ufullname'
		},
		listeners: {
			editAction: '@{editRunbook}'
		},
		margin : {
			bottom : 10
		}
	}],
	buttons: [{
		name : 'dump',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}],	
	listeners: {
		beforerender: function() {
			clientVM.setPopupSizeToEightyPercent(this);
		}
	}
});