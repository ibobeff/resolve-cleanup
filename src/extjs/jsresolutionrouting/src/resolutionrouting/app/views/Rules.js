glu.defView('RS.resolutionrouting.Rules', {
	layout: 'border',
	padding : 15,
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true,
		useWindowParams: false
	}],
	items: [{
		padding : '32 0 0 0',
		region: 'west',
		xtype: 'treepanel',
		cls : 'rs-grid-dark',
		store: '@{treeStore}',
		columns: [{
			header: '~~module~~',
			xtype: 'treecolumn',
			dataIndex: 'module',
			flex: 1
		}],
		selected: '@{selectedModule}',
		width: 300,
		split: true,
		listeners: {
			afterrender: function() {
				this.selectPath(this.getRootNode().getPath());
			}
		}
	}, {
		region: 'center',
		displayName: '~~resolutionRouting~~',
		stateId: 'rsresolutionroutingmain',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		dockedItems: [{
			xtype: 'toolbar',
			name: 'actionBar',
			cls: 'actionBar rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: ['addRule', 'deleteRules', '-', 'deploy', {
				text: '~~bulk~~',
				menu: {
					xtype: 'menu',
					items: ['importCSV', 'exportCSV', '-', 'activateMapping', 'deactivateMapping']
				}
			}]
		}],
		viewConfig: {
			enableTextSelection: true
		},
		name: 'rules',
		store: '@{ruleStore}',
		enableColumnMove: true,
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}, {
			ptype: 'pager'
		}, {
			ptype: 'rowexpander',
			rowBodyTpl: new Ext.XTemplate(
				'<div resolveId="resolveRowBody" style="color:#333">{[this.formatFields(values)]}</div>', {
					formatFields: function(values) {
						var schema = values.schema;
						if (!schema)
							return '';
						var rid = Ext.String.htmlEncode(values.rid);
						var list = [];
						try {
							var fields = Ext.decode(schema.jsonFields);
							Ext.each(fields.sort(function(a, b) {
								return a.order > b.order ? 1 : -1;
							}), function(field) {
								list.push(field.name + (field.regex ? '(' + field.regex + ')' : ''));
							})

							var rids = rid.split('§');
							var strs = [];
							for (var i = 0; i < rids.length; i++) {
								strs.push('<tr><td>' + Ext.String.htmlEncode(list[i]) + ':</td><td>' + Ext.String.htmlEncode(rids[i]) + '</td></tr>');
							}
						} catch (e) {
							return ''
						}
						return '<table style="padding:0 0 0 62px">' + strs.join('') + '</table>';
					}
				}
			)
		}],
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTooltip: RS.common.locale.editColumnTooltip,
			columnTarget: '_self',
			columnEventName: 'editAction',
			glyph : 0xf044,
			cellNavigation: true,
			canNavigateToCol: function(col, viewDomIndex) {
				return col.dataIndex && col.dataIndex != 'rid';
			},
			columnIdField: 'id',
			canSelectEntireTable: true
		},
		multiSelect: true,
		columns: [{
			header: '~~rid~~',
			dataIndex: 'rid',
			filterable: true,
			sortable: true,
			flex: 3
		}, {
			header: '~~schema~~',
			dataIndex: 'schema.name',
			filterable: true,
			sortable: true
		}, {
			header: '~~wiki~~',
			dataIndex: 'wiki',
			filterable: true,
			sortable: true,
			flex: 1,
			editor: {
				xtype: 'textfield'
			}
		},{
			header: '~~automation~~',
			dataIndex: 'automation',
			hidden: true,
			filterable: true,
			sortable: true,
			flex: 1,
			editor: {
				xtype: 'textfield'
			}
		},{
			header: '~~gwautomation~~',
			dataIndex: 'runbook',
			filterable: true,
			sortable: true,
			flex: 1,
			editor: {
				xtype: 'textfield'
			}
		},{
			header: '~~eventId~~',
			dataIndex: 'eventId',
			hidden: true,
			filterable: true,
			sortable: true,
			flex: 1,
			editor: {
				xtype: 'textfield'
			}
		}, {
			header: '~~active~~',
			dataIndex: 'active',
			filterable: true,
			sortable: true,
			width: 80,
			align : 'center',
			renderer: RS.common.grid.booleanRenderer(),
			editor: {
				xtype: 'combo',
				editable: false,
				displayField: 'name',
				valueField: 'value',
				store: {
					xtype: 'store',
					fields: ['name', 'value'],
					data: [{
						name: 'TRUE',
						value: true
					}, {
						name: 'FALSE',
						value: false
					}]
				}
			}
		}].concat(RS.common.grid.getSysColumns()),
		editCell: function() {
			this.plugins[0].startEditByPosition({
				row: this.getSelectionModel().currentCell.row,
				column: this.getSelectionModel().currentCell.column
			});
		},
		editing: false,
		listeners: {
			editAction: '@{editRule}',
			render: function() {
				this.getEl().on('click', function(evt) {
					var row = evt.getTarget('tr[role="row"]');
					var cell = evt.getTarget('td[role="gridcell"]');
					if (!row || this.currentCell == cell || this.editing)
						return;
					var colIdx = 0;
					Ext.each(row.childNodes, function(child, idx) {
						if (child == cell)
							colIdx = idx; //this is for the edit column and row expander column and the rid column
					});
					if (colIdx < 3) {
						if (this.currentCell)
							Ext.fly(this.currentCell).removeCls('highlightCell');
						this.currentCell = null;
						return;
					}
					var rowIdx = parseInt(row.getAttribute('data-recordindex'));
					this.getSelectionModel().currentCell.row = rowIdx;
					this.getSelectionModel().currentCell.column = colIdx;
					this.fireEvent('navigatecell', this.getSelectionModel());
				}, this);

				this.getEl().on('keyup', function(evt) {
					if (evt.F2 == evt.getCharCode()) {
						this.editCell();
					}
				}, this);
			},
			edit: function(editor, eOpts) {
				this.fireEvent('recordEdit', this, eOpts.record);
			},
			recordEdit: '@{recordEdit}',
			beforeedit: function(editor, eOpt) {

				if (event && (event.ctrlKey || event.shiftKey))
					return false;
				this.fireEvent('navigatecell', this.getSelectionModel());
				if (eOpt.column.dataIndex == 'rid') {
					// if (event.type != 'click')
					// 	editor.view.panel.fireEvent('editAction', editor.view.panel, eOpt.record.get('id'));
					return false;
				}
				this.getSelectionModel().currentCell = {
					row: eOpt.rowIdx,
					column: eOpt.colIdx
				};
			},
			navigatecell: function(sm) {
				var rowIdx = sm.currentCell.row;
				var colIdx = sm.currentCell.column;
				var r = this.view.getRecord(rowIdx);
				var cell = this.view.getCell(r, this.view.getGridColumns()[colIdx]).dom
				if (this.currentCell == cell)
					return;
				if (this.currentCell)
					Ext.fly(this.currentCell).removeCls('highlightCell');
				this.currentCell = cell;
				Ext.fly(cell).addCls('highlightCell');
			},
			selectAllAcrossPages: function() {
				var filters = this.up().plugins[0].getWhereFilter();
				this.fireEvent('selectAll', this, filters);
			},
			selectAll: '@{selectAllAcrossPages}',
			selectionChange: '@{selectionChanged}'
		}
	}],
	listeners: {
		beforedestroy : '@{beforeDestroyComponent}'
	}
});