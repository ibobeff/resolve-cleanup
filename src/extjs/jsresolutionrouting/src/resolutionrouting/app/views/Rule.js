glu.defView('RS.resolutionrouting.Rule', {
	title: '~~rule~~',
	bodyPadding : 15,
	modal: true,
	cls : 'rs-modal-popup',
	autoScroll: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		labelWidth: 120,
		margin : '4 0'
	},
	items: [{
		xtype: 'combo',
		name: 'module',
		minChars: 2,
		queryDelay: 500,
		store: '@{moduleStore}',
		displayField: 'name',
		valueField: 'name',
		margin : '4 15',
		labelStyle : 'font-weight:bold'
	}, {
		xtype: 'checkboxfield',
		hideLabel: true,
		boxLabel: '~~active~~',
		name: 'active',
		margin: '0 0 0 140'
	}, {
		xtype : 'fieldset',
		margin : '10 0 4 0',
		padding : '5 15 10',
		title : '~~integrationTitle~~',
		items : [{
			xtype : 'checkboxfield',
			name : 'createInvestigationRecord',
			hideLabel : true,
			boxLabel : '~~sirTitle~~',
		},{
			//SIR
			xtype : 'container',
			margin : '0 0 10 0',
			layout : {
				type :'vbox',
				align : 'stretch'
			},
			defaults : {
				labelWidth: 120,
				margin : '4 0'
			},
			hidden : '@{!createInvestigationRecord}',
			items : [{
				xtype : 'textfield',
				name : 'sirSource',
			},{
				xtype : 'textfield',
				name : 'investigationTitle'
			},{
				xtype : 'combobox',
				name : 'investigationType',
				store : '@{investigationTypeStore}',
				displayField : 'name',
				valueField : 'name',
				queryMode : 'local'
			},{
				xtype: 'triggerfield',
				name: 'playbook',
				triggerCls: 'x-form-search-trigger',
				onTriggerClick: function() {
					this.fireEvent('openPlaybookSearch');
				},
				listeners: {
					openPlaybookSearch: '@{openPlaybookSearch}'
				}
			},{
				xtype : 'combobox',
				store : '@{severityStore}',
				name : 'severity',
				displayField : 'name',
				valueField : 'name',
				queryMode : 'local'
			},{
				xtype : 'combobox',
				store : '@{ownerStore}',
				name : 'owner',
				displayField : 'userId',
				valueField : 'userId',
				editable : false,
				queryMode : 'local'
			},{
				xtype : 'component',
				margin : '10 0 10 125',
				html : '~~sirUIFlag~~'
			}]
		},{
			xtype : 'tabpanel',	
			cls : 'rs-tabpanel-dark',
			foo : '@{foo}',
			layout : {
				align : 'stretch'
			},
			items : [{
				title : '~~uiTitle~~',
				itemId : 'uiDisplay',
				layout : {
					type :'vbox',
					align : 'stretch'
				},
				defaults : {
					labelWidth: 120,
					margin : '4 0'
				},
				items : [{
					xtype: 'triggerfield',
					name: 'wiki',
					triggerCls: 'x-form-search-trigger',
					onTriggerClick: function() {
						this.fireEvent('openPageSearch');
					},
					listeners: {
						openPageSearch: '@{openPageSearch}'
					}
				},{
					xtype: 'triggerfield',
					name: 'automation',
					triggerCls: 'x-form-search-trigger',
					onTriggerClick: function() {
						this.fireEvent('openAutomationSearch',this, 'automation');
					},
					listeners: {
						openAutomationSearch: '@{openAutomationSearch}'
					}
				},{
					xtype: 'checkboxfield',
					hideLabel: true,
					boxLabel: '~~newWorksheetOnly~~',
					name: 'newWorksheetOnly',
					margin: '0 0 0 125'
				}]
			},{
				title : '~~gateway~~',
				layout : {
					type : 'vbox',
					align : 'stretch'
				},
				defaults : {
					margin : '4 0',
					labelWidth : 120
				},
				items : [{
					xtype: 'triggerfield',
					name: 'runbook',
					triggerCls: 'x-form-search-trigger',
					onTriggerClick: function() {
						this.fireEvent('openAutomationSearch',this, 'runbook');
					},
					listeners: {
						openAutomationSearch: '@{openAutomationSearch}'
					}
				},{
					xtype: 'textfield',
					name: 'eventId'
				}]
			}]			
		}]
	},{
		xtype: 'fieldcontainer',
		layout: 'hbox',
		margin : '10 0 4 15',
		items: [{
			xtype: 'combo',
			name: 'schema',
			labelWidth: 120,
			minChars: 1,			
			flex: 1,
			store: '@{schemaStore}',
			pageSize: 30,
			displayField: 'name',
			valueField: 'id',
			margin : '0 5 0 0',
			labelStyle : 'font-weight:bold'
		}, {
			xtype: 'image',
			cls: 'rs-btn-edit',
			hidden: '@{!jumpToSchemaIsVisible}',
			listeners: {
				handler: '@{jumpToSchema}',
				render: function(image) {
					image.getEl().on('click', function() {
						image.fireEvent('handler')
					})
				}
			}
		}]
	},{
		xtype: '@{mappingDefinition}',
		cls : 'rs-grid-dark',
		minHeight: 200,
		flex: 1,
		margin : {
			bottom : 10
		}
	}],
	listeners: {
		beforerender: function() {
			this.setWidth(Math.round(Ext.getBody().getWidth() * 0.6));
		},
		resize: function(panel) {			
			panel.center();				
		},
		beforeshow: function(win) {			
			win.setHeight(Math.round(Ext.getBody().getHeight() * 0.9));
		},
	},	
	dockedItems : [{
		xtype : 'toolbar',
		dock : 'bottom',
		style : 'border-top: 1px solid #b5b8c8 !important;',
		padding : '8 15',
		items : ['->',{
			name : 'save',
			cls : 'rs-med-btn rs-btn-dark'
		},
		//Working but disable for now to simplify UI.
		/*{
			name : 'previousRule',
			cls : 'rs-med-btn rs-btn-dark'
		},{
			name : 'nextRule',
			cls : 'rs-med-btn rs-btn-dark'
		},{
			name : 'newRule',
			cls : 'rs-med-btn rs-btn-dark'
		},
		*/{
			name : 'close',
			cls : 'rs-med-btn rs-btn-light'
		}]
	}]
});