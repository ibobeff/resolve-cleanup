glu.defView('RS.resolutionrouting.Bulk', {	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	items: [{
		xtype: 'progressbar',
		progress: '@{progress}',
		hidden: '@{!progressIsVisible}',
		toFixed: function(value, precision) {
			var precision = precision || 0,
				power = Math.pow(10, precision),
				absValue = Math.abs(Math.round(value * power)),
				result = (value < 0 ? '-' : '') + String(Math.floor(absValue / power));

			if (precision > 0) {
				var fraction = String(absValue % power),
					padding = new Array(Math.max(precision - fraction.length, 0) + 1).join('0');
				result += '.' + padding + fraction;
			}
			return result;
		},
		setProgress: function(progress) {
			this.progress = progress;
			if (this.progress)
				this.updateProgress(progress, this.toFixed(progress * 100, 2) + '%');
			else
				this.updateProgress(0, '');
		}
	}, {
		html: '@{busyText}'
	}, {
		xtype: 'fieldcontainer',
		layout: 'vbox',
		disabled: '@{fineUploaderTemplateHidden}',
		items: [{
			xtype: 'checkbox',
			name: 'override',
			hideLabel: true,
			boxLabel: '~~override~~'
		}, {
			xtype: 'checkbox',
			name: 'forceImport',
			hideLabel: true,		
			boxLabel: '~~forceImport~~'
		}]
	}, {
		itemId: 'fineUploaderTemplate',
		html: '@{fineUploaderTemplate}',
		height: '@{fineUploaderTemplateHeight}',
		hidden: '@{fineUploaderTemplateHidden}',
	}, {
		id: 'attachments_grid',
		hidden: true
	}, {
		hidden: '@{!statusDisplayIsVisible}',
		autoScroll: true,
		html: '@{processedStatus}',
		flex: 1
	}],

	asWindow: {
		title: '@{title}',
		modal: true,
		width: 600,
		height: 400,
		padding : 15,
		cls : 'rs-modal-popup',		
		closable: true,
		buttons: [{
			name: 'browse',
			itemId: 'browse',
			id: 'upload_button',
			cls : 'rs-med-btn rs-btn-dark',
			preventDefault: false,
			hidden: '@{!browseIsVisible}',
			disabled: '@{fineUploaderTemplateHidden}'
		},{
			name : 'abort',
			cls : 'rs-med-btn rs-btn-dark'
		},{
			name : 'overrideLock', 
			cls : 'rs-med-btn rs-btn-dark'
		},{	
			name : 'backToImport', 
			cls : 'rs-med-btn rs-btn-dark'
		},{
			name :'close',
			cls : 'rs-med-btn rs-btn-light'
		}]
	}
});

/*
glu.defView('RS.resolutionrouting.Bulk', {	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	override: '@{override}',
	forceImport: '@{forceImport}',
	setOverride: function(override) {
		this.uploader.multipart_params['override'] = override;
	},
	setForceImport: function(forceImport) {
		this.uploader.multipart_params['forceImport'] = forceImport;
	},
	getConfig: function() {
		return {
			autoStart: true,
			url: '/resolve/service/resolutionrouting/rule/csv/import/upload',
			filters: [{
				title: "CSV files",
				extensions: "csv"
			}],
			browse_button: this.getBrowseButton(),
			drop_element: this.getDropArea(),
			container: this.getContainer(),
			chunk_size: null,
			runtimes: "html5,flash,silverlight,html4",
			flash_swf_url: '/resolve/js/plupload/Moxie.swf',
			unique_names: true,
			multipart: true,
			multipart_params: {
				test: 'test'
			},
			multi_selection: false,
			required_features: null,
			ownerCt: this
		}
	},
	getBrowseButton: function() {
		return this.up().down('#browse').btnEl.dom.id;
	},
	getDropArea: function() {
		return this.down('#dropzone').getEl().dom.id;
	},
	getContainer: function() {
		var btn = this.up().down('#browse');
		return btn.ownerCt.id;
	},
	items: [{
		xtype: 'progressbar',
		progress: '@{progress}',
		hidden: '@{!progressIsVisible}',
		toFixed: function(value, precision) {
			var precision = precision || 0,
				power = Math.pow(10, precision),
				absValue = Math.abs(Math.round(value * power)),
				result = (value < 0 ? '-' : '') + String(Math.floor(absValue / power));

			if (precision > 0) {
				var fraction = String(absValue % power),
					padding = new Array(Math.max(precision - fraction.length, 0) + 1).join('0');
				result += '.' + padding + fraction;
			}
			return result;
		},
		setProgress: function(progress) {
			this.progress = progress;
			if (this.progress)
				this.updateProgress(progress, this.toFixed(progress * 100, 2) + '%');
			else
				this.updateProgress(0, '');
		}
	}, {
		html: '@{busyText}'
	}, {
		xtype: 'fieldcontainer',
		layout: 'vbox',
		items: [{
			xtype: 'checkbox',
			name: 'override',
			hideLabel: true,
			boxLabel: '~~override~~'
		}, {
			xtype: 'checkbox',
			name: 'forceImport',
			hideLabel: true,		
			boxLabel: '~~forceImport~~'
		}]
	}, {
		xtype: 'form',
		itemId: 'dropzone',
		style: 'border: 2px dashed #ccc;margin: 5px 0px 10px 0px',
		hidden: '@{!dropAreaIsVisible}',
		flex: 1,
		layout: {
			type: 'vbox',
			pack: 'center',
			align: 'center'
		},
		items: [{
			xtype: 'label',
			text: '~~dropArea~~',
			style: {
				'font-size': '30px',
				'color': '#CCCCCC'
			}
		}]
	}, {
		hidden: '@{!statusDisplayIsVisible}',
		autoScroll: true,
		html: '@{processedStatus}',
		flex: 1
	}],
	uploaderListeners: {
		uploaderror: function(uplaoder, data) {
			this.owner.fireEvent('uploadError', this.owner, data);
		},
		fileuploaded: function(uploader, data) {
			this.owner.fireEvent('fileUploaded', this.owner, uploader, data);
		},
		uploadprogress: function(uploader, file, name, size, percent) {
			this.owner.percent = file.percent;
		}
	},
	listeners: {
		afterrender: function() {
			this.myUploader = Ext.create('RS.common.SinglePlupUpload', this);
			this.uploader = this.getConfig();
			this.myUploader.initialize(this);
		},
		fileUploaded: '@{importStart}',
		uploadError: '@{uploadError}'
	},
	asWindow: {
		title: '@{title}',
		modal: true,
		width: 600,
		height: 400,
		padding : 15,
		cls : 'rs-modal-popup',		
		buttons: [{
			itemId: 'browse',
			text: '~~browse~~',
			cls : 'rs-med-btn rs-btn-dark',
			hidden: '@{!browseIsVisible}',
			listeners: {
				hide: function() {
					var fileField = this.up().el.query('.moxie-shim')[0];
					if (fileField)
						fileField.style.display = 'none'
				},
				show: function() {
					var fileField = this.up().el.query('.moxie-shim')[0];
					if (fileField)
						fileField.style.display = 'inline'
				}
			}
		}, 
		{
			name : 'abort',
			cls : 'rs-med-btn rs-btn-dark'
		},{
			name : 'overrideLock', 
			cls : 'rs-med-btn rs-btn-dark'
		},{	
			name : 'backToImport', 
			cls : 'rs-med-btn rs-btn-dark'
		},{
			name :'close',
			cls : 'rs-med-btn rs-btn-light'
		}]
	}
});
*/