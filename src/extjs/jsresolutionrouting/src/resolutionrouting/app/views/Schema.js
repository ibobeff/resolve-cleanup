glu.defView('RS.resolutionrouting.Schema', {
	padding : 15,
	autoScroll: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		cls : 'rs-dockedtoolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '~~schema~~'
		}],
		margin : '0 0 15 0'
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'		
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}'
		}]
	}],
	defaults : {
		labelWidth : 120
	},
	items: [{
		layout : {
			type : 'vbox',
			align : 'stretch'
		},
		defaults : {
			margin : '4 0'
		},
		style : 'border-top:1px solid silver',
		padding : '8 0',
		items : [{
			xtype: 'textfield',
			name: 'name'
		},{
			xtype: 'textarea',
			name: 'description',
			height : 150
		},{
			xtype: 'numberfield',
			name: 'order',
			minValue: 0,
			maxValue: '@{maxOrder}',
			maxWidth : 600
		},{
			xtype : 'combo',
			store : '@{orgStore}',
			displayField : 'uname',
			valueField : 'id',
			editable : false,
			name : 'sysOrg',
			maxWidth : 600,
			queryMode : 'local'
		}]
	},{
		xtype: 'panel',
		flex : 1,
		layout: {
			type : 'hbox',
			align : 'stretch'
		},
		autoScroll: true,
		defaults: {
			flex: 1
		},
		items: [{
			title: '~~fields~~',		
			xtype: 'grid',
			cls : 'rs-grid-dark',
			margin : {
				right : 5
			},
			name: 'fields',
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}],
			viewConfig: {
				plugins: [{
					ptype: 'gridviewdragdrop'
				}]
			},
			dockedItems: [{
				xtype: 'toolbar',
				cls : 'rs-dockedtoolbar',
				defaults :{
					cls : 'rs-small-btn rs-btn-light'
				},
				items: ['addField', 'removeFields']
			}],
			store: '@{fieldStore}',			
			columns: [{
				header: '~~name~~',
				dataIndex: 'name',
				minWidth: 200,
				editor: {
					xtype: 'textfield',
					allowBlank: false
				}
			}, {
				header: '~~source~~',
				dataIndex: 'source',
				flex: 1,
				editor: {
					xtype: 'textfield'
				}
			}, {
				header: '~~regex~~',
				dataIndex: 'regex',
				flex: 1,
				editor: {
					xtype: 'textfield'
				}
			}]
		}, {
			title: '~~queues~~',
			xtype: 'grid',
			cls : 'rs-grid-dark',
			name: 'queues',
			dockedItems: [{
				xtype: 'toolbar',
				cls : 'rs-dockedtoolbar',
				defaults :{
					cls : 'rs-small-btn rs-btn-light'
				},
				items: ['addGateway', 'removeGateways']
			}],
			store: '@{queueStore}',
			columns: [{
				header: '~~queue~~',
				dataIndex: 'queue',
				flex: 1
			}],
			margin : {
				left : 5
			}
		}]
	},{
		xtype: 'component',
		margin : '4 0',
		html: '@{invalidFieldsMsg}',
		hidden : '@{invalidTextIsHidden}'
	}]
});

glu.defView('RS.resolutionrouting.QueuePicker', {
	padding : 15,
	cls : 'rs-modal-popup',
	title: '~~queue~~',
	width: 600,
	modal: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'combo',
		name: 'queue',
		editable: false,
		emptyText: '~~selectQueue~~',
		displayField: 'queue',
		valueField: 'queue',
		queryMode: 'local',
		store: '@{queueStore}'
	}],
	buttons: [{
		name : 'dump',
		cls : 'rs-med-btn rs-btn-dark'
	},{ 
		name : 'close',
		cls : 'rs-med-btn rs-btn-light'
	}]
});
