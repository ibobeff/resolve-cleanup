glu.defView('RS.resolutionrouting.MappingDefinition', {
	layout: {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		margin : '4 0'
	},
	items: [{
		xtype: 'grid',
		flex : 1,
		disabled: '@{!fieldsIsEnabled}',
		name: 'mapping',
		store: '@{store}',	
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		columns: [{
			header: '~~name~~',
			dataIndex: 'name',
			flex: 1
		}, {
			header: '~~value~~',
			dataIndex: 'value',
			flex: 4,
			editor: {
				xtype: 'textfield',
				allowBlank: false
			},
			renderer: Ext.String.htmlEncode
		}],
		listeners: {
			edit: '@{verifyFields}'
		}
	},{
		xtype: 'tbtext',
		style: 'color:red !important',
		htmlEncode : false,
		text: '~~fieldInvalidMsg~~',
		hidden: '@{fieldIsValid}'
	},{
		xtype: 'tbtext',
		htmlEncode : false,
		style: 'color:red !important',
		text: '~~fieldIsEmptyMsg~~',
		hidden: '@{!fieldIsEmpty}'
	}]
});