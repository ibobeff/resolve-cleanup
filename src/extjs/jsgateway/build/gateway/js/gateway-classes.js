/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/
glu.defModel('RS.gateway.AMQPFilter', {
	uquery: '',
	modelName: 'AmqpFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'amqp',
	fields: RS.gateway.commonFields.concat(['utargetQueue', {
		name: 'uexchange',
		type: 'bool'
	}]),
	// uqueryIsValid$:function(){
	// 	return this.uquery&&this.uquery!=''?true:this.localize('invalidQuery');
	// },

	init: function() {
		this.initBase();
		this.set('title', this.localize('amqpFilterTitle'));
	}
});
RS.gateway.amqp = {
	modelName: 'amqp',
	filterViewName: 'RS.gateway.AMQPFilter',
	requestParams: {
		modelName: 'AmqpFilter',
		queueName: 'AMQP',
		gatewayName: 'AMQP',
		gatewayClass: 'MAmqp',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~utargetQueue~~',
		dataIndex: 'utargetQueue',
		filterable: true,
		flex: 1
	}, {
		header: '~~uexchange~~',
		dataIndex: 'uexchange',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'utargetQueue',
		type: 'string'
	}, {
		name: 'uexchange',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('amqpFilterDisplayName'));
		this.set('title', this.localize('amqpTitle'));
	}
};

glu.defModel('RS.gateway.AMQPMain', {
	mixins: ['Main'],
	gatewaytype: 'amqp'
});
glu.defModel('RS.gateway.BaseFilter', {
	uniqueIdField: 'uname',
	uname: '',
	uinterval: undefined,
	uorder: 1,
	uactive: true,
	ueventEventId: '',
	urunbook: '',
	uscript: '',
	modelName: '',

	id: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrganizationName: '',

	defaultData: {
		uname: '',
		uinterval: undefined,
		uorder: 1,
		uactive: true,
		ueventEventId: '',
		urunbook: '',
		uscript: '',

		id: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrganizationName: ''
	},

	title: '',

	wait: false,
	API : {
		getDefaultInterval : '/resolve/service/gateway/getDefaultInterval',
		getAuthenticationType: '/resolve/service/gateway/getHttpAuthType',
		getFilter : '/resolve/service/gateway/getFilter',
		saveFilter : '/resolve/service/gateway/saveFilter'
	},	
	unameIsValid$: function() {
		if (this.ignoreName)
			return true;
        return RS.common.validation.TaskName.test(this.uname) ? true : this.localize('invalidName');
    },
	unameIsReadOnly$: function() {
		return !!this.id;
	},

	uorderIsValid$: function() {
		return /^\d*$/.test(this.uorder) ? true : this.localize('invalidOrder');
	},

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	activateBase: function(params) {
		window.document.title = this.localize('windowTitle');
		this.resetForm()
		this.set('id', params && params.id != null ? params.id : '');
		this.set('uniqueIdField', params.uniqueIdField ? params.uniqueIdField : 'uname');
		this.set('wait', false);
		if (this.id != '') {
			this.loadFilter();
			return;
		}
	},

	activate: function(screen, params) {
		this.loadDefaultValue(params);		
	},

	initBase: function() {
		this.set('fields', this.fields.concat(this.commonFields));	
	},
	loadDefaultValue : function(params){
		
		var self = this;
		//For now, just need for interval, might need to enhance this method in future.
		this.ajax({
			url : this.API['getDefaultInterval'],
			method : 'GET',
			params : {
				gatewayName : this.gatewaytype
			},
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				this.defaultData['uinterval'] = response.data ? response.data : 10;
				this.ajax({
					url : this.API['getAuthenticationType'],
					method : 'GET',
					params : {
						gatewayName : this.gatewaytype
					},
					success : function(resp){
						var response = RS.common.parsePayload(resp);						
						this.defaultData['uhttpAuthType'] = response.data ? response.data : "";
					    self._uhttpAuthType = response.data ? response.data : "";
						this.activateBase(params);
					}
				})
			}
		});
	},
	uhttpAuthType: null,
	loadStaticData: function() {
		this.set('uhttpAuthType',   this._uhttpAuthType );
	},
	loadFilter: function() {
		this.set('wait', true);
		this.ajax({
			url: this.API['getFilter'],
			method: 'GET',
			params: {
				id: this.id,
				modelName: this.modelName
			},
			scope: this,
			success: function(resp) {
				this.handleLoadFilterReqSuccess(resp);
			},
			failure: function(resp) {
				this.handleLoadFilterReqSuccess(resp);
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	handleLoadFilterReqSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);		
		if (!respData.success) {
			clientVM.displayError(this.localize('GetFilterErrMsg') + '[' + respData.message + ']');
			return;
		}
		if(this.SDK && this.encryptFields.length > 0) {
			for(var i = 0; i < this.encryptFields.length; i += 1) {
				var fieldName = this.encryptFields[i];
				var fieldData = JSON.parse(respData.data[fieldName]);
				respData.data["is" + fieldName +"Encrypted"] = fieldData.encrypt;			
				if(fieldData.hasOwnProperty("label")) {
					respData.data[fieldName + "Label"] = fieldData.label;
				}
				respData.data[fieldName] = fieldData.value;
			}
		}
		this.loadData(respData.data);
		this.loadStaticData();
	},

	save: function(exitAfterSave) {
		if (this.task) this.task.cancel();
		this.task = new Ext.util.DelayedTask(this.saveFilter, this, [exitAfterSave]);
		this.task.delay(500);
	},

	saveIsEnabled$: function() {
		return this.state != 'wait' && this.isValid;
	},

	saveAndExit: function() {
		if (this.task) this.task.cancel()
		this.save(true)
	},

	saveAs: function() {
		this.open({
			mtype: 'RS.gateway.SaveAs',
			uniqueIdField: this.uniqueIdField,
			modal: true,
			dumper: (function(self) {
				return function() {
					self.saveFilter(false, this.name, true);
				}
			})(this)
		});
	},

	saveAsIsEnabled$: function() {
		return this.state != 'wait' && this.isValid;
	},

	saveAsIsVisible$: function() {
		return !!this.id;
	},

	respErr: function(resp) {
		clientVM.displayFailure(resp);
	},

	saveFilter: function(exitAfterSave, newName, newWindow) {
		var filter = this.asObject();
		//Trim all the extra spaces before save.
		for(var k in filter){
			if(typeof filter[k] == "string")
				filter[k] = filter[k].trim();
			if(this.SDK && this.encryptFields.indexOf(k) != -1) {
				filter[k] = {
					label:  filter[k + "Label"],
					value: filter[k],
					encrypt: filter["is" + k + "Encrypted"]
				}
				delete filter["is" + k + "Encrypted"];
				delete filter[k + "Label"];
			}
		}
		filter.modelName = this.modelName;
		if (newName) {
			filter.id = '';
			filter[this.uniqueIdField] = newName;
		}
		this.set('wait', true);
		this.ajax({
			url: this.API['saveFilter'],
			jsonData: filter,
			success: function(resp) {
				this.set('wait', false);
				this.handleSaveSuccess(resp, newWindow);
				if (exitAfterSave === true)
					this.back();
			},
			failure: this.respErr
		});
	},

	handleSaveSuccess: function(resp, newWindow) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('SaveErrMsg') + '[' + respData.message + ']');
			return;
		}
		if (newWindow) {
			clientVM.handleNavigation({
				modelName: 'RS.gateway.' + this.viewmodelName,
				params: {
					name: this.gatewaytype,
					id: respData.data.id
				},
				target: '_blank'
			})
			this.loadFilter();
		} else {
			if(this.SDK && this.encryptFields.length > 0) {
				for(var i = 0; i < this.encryptFields.length; i += 1) {
					var fieldName = this.encryptFields[i];
					var fieldData = JSON.parse(respData.data[fieldName]);
					respData.data["is" + fieldName +"Encrypted"] = fieldData.encrypt;			
					if(fieldData.hasOwnProperty("label")) {
						respData.data[fieldName + "Label"] = fieldData.label;
					}
					respData.data[fieldName] = fieldData.value;
				}
			}
			this.loadData(respData.data);		
		}
		clientVM.displaySuccess(this.localize('SaveSucMsg'));
		this.loadStaticData();

	},

	back: function() {
		var config = {
			modelName: 'RS.gateway.' + this.viewmodelName.substring(0, this.viewmodelName.length - 6) + 'Main'
		}
		if(this.SDK == true){
			Ext.apply(config, {
				modelName: 'RS.gateway.Custom',
				params : {
					name : this.gatewaytype
				}
			})
		}
		clientVM.handleNavigation(config);
	},

	resetForm: function() {
		this.set('uniqueIdField', 'uname');
		this.loadData(this.defaultData);
	},

	refresh: function() {
		if (this.id == null || this.id == '')
			return;
		this.loadFilter();
	}
});
glu.defModel('RS.gateway.CASpectrumFilter', {
	uurlQuery: '',
	uxmlQuery:'',
	modelName: 'CASpectrumFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'CASpectrum',
	objectStore : {
		mtype :'store',
		proxy: {
			type: 'memory'
		},
		fields : ['name'],
		data : [{
			name : "alarms"
		},{
			name : "models"
		},
		{
			name : "devices"
		},
		{
			name : "subscription"
		}]
	},
	uobjectIsValid$: function(){
		return this.uobject && this.uobject != '' ? true : this.localize('invalidObject');
	},
	fields: RS.gateway.commonFields.concat(['uobject', 'uurlQuery', 'uxmlQuery']),
	init: function() {
		this.initBase();
		this.set('title', this.localize('CASpectrumFilterTitle'));
	}
});
RS.gateway.CASpectrum = {
	modelName: 'CASpectrum',
	filterViewName: 'RS.gateway.CASpectrumFilter',
	requestParams: {
		modelName: 'CASpectrumFilter',
		queueName: 'CASPECTRUM',
		gatewayName: 'CASPECTRUM',
		gatewayClass: 'MCASpectrum',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uobject~~',
		dataIndex: 'uobject',
		filterable: true,
		flex: 1
	}, {
		header: '~~uurlQuery~~',
		dataIndex: 'uurlQuery',
		filterable: true,
		flex: 1,
		renderer: Ext.String.htmlEncode
	},
	{
		header: '~~uxmlQuery~~',
		dataIndex: 'uxmlQuery',
		filterable: true,
		flex: 1,
	 	renderer: Ext.String.htmlEncode
	}],

	additionalStoreFields: [{
		name: 'uobject',
		type: 'string'
	}, {
		name: 'uurlQuery',
		type: 'string'
	},
	{
		name: 'uxmlQuery',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('CASpectrumFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('CASpectrumGatewayDisplayName'));
		this.set('title', this.localize('CASpectrumTitle'));
	}
};

glu.defModel('RS.gateway.CASpectrumMain', {
	mixins: ['Main'],
	gatewaytype: 'CASpectrum'
});
glu.defModel('RS.gateway.Custom', {
	activate : function(screen, params){
		if(this.viewIsReady){
			if(this.name){
				if(this.name != params.name){ //re-render this screen if new gateway is loaded.
					this.set("name", params.name);
					this.loadMainView(params.name);
				}
				else
					this.subVM.activate();
			}
			else
				clientVM.displayError("A gateway name is needed. Ex: RS.gateway.Custom/name=NAME", 10000)
		}
	},
	subVM : null,
	panel : null,
	viewIsReady : false,
	config : {},
	encryptFields: [],
	init : function() {
		var me = this;
		this.on('viewIsReady', function(){
			me.viewIsReady = true;
			if(this.name)
				me.loadMainView(this.name);
			else
				clientVM.displayError("A gateway name is needed. Ex: RS.gateway.Custom/name=NAME", 10000)
		});
	},
	loadMainView : function(gatewayName){
		this.fireEvent('reloadingView');
		this.buildViewConfig(gatewayName);
		this.loadCustomField(gatewayName);
	},

	buildViewConfig : function(gatewayName){
		var newConfig = {
			modelName: gatewayName,
			gatewaytype: gatewayName,
			filterViewName: 'RS.gateway.CustomFilter',
			requestParams: {
				modelName: gatewayName + "Filter",
				queueName: gatewayName.toUpperCase(),
				gatewayName: gatewayName.toUpperCase(),
				gatewayClass: 'M' + gatewayName,
				getterMethod: 'getFilters',
				clearAndSetMethod: 'clearAndSetFilters',
				undeployFilterMethod: 'undeployFilters',
				deleteFilterNames: null
			},
			config : function(){}
		}
		Ext.apply(this.config, newConfig);
	},

	loadCustomField : function(gatewayName){
		var me = this;
		Ext.Ajax.request({
			url : "/resolve/service/gateway/getCustomSDKFields",
			method : "GET",
			params : {
				customGatewayName : gatewayName
			},
			success : function(r){
				var response = RS.common.parsePayload(r);
				if (response.success) {
					var records = response.records;
					var additionalColumns = []
					var additionalStoreFields = [];
					if(records && records.length > 0){
						for(var i = 0 ; i < records.length; i++){
							var newField = records[i];
							var rawFieldParameter = newField.NAME;
							var fieldParameter = 'u' + rawFieldParameter;

							//Build addtional column and field for new custom gateway.
							if (newField.TYPE === "textenc" || newField.TYPE === "encrypt") {
								me.encryptFields.push(fieldParameter);
							}
							var newAdditionalColumm = {
								header: newField.DISPLAYNAME,
								dataIndex: fieldParameter,
								filterable: true,
								flex: 1,
								renderer: function(value, metaData, record){
									var dataIndexName = metaData.column.dataIndex;
									var rawRecord = record.raw;
									var data = (rawRecord.attrs && rawRecord.attrs[dataIndexName])? rawRecord.attrs[dataIndexName] : rawRecord[dataIndexName];
									if (data && me.encryptFields.indexOf(dataIndexName) !== -1) {
										data = JSON.parse(data);
										data = data.encrypt ? "******" : data.value;
										if (data.label){
											metaData.column.setText(data.label);
										}
									}
									return data;
								}
							}												
							if(newField.TYPE == 'textarea') {
								newAdditionalColumm.renderer = function(value, metaData, record) {
									var dataIndexName = metaData.column.dataIndex;
									var rawRecord = record.raw;
									var dataIndexValue = (rawRecord.attrs && rawRecord.attrs[dataIndexName])? rawRecord.attrs[dataIndexName] : rawRecord[dataIndexName];
									return Ext.String.htmlEncode(dataIndexValue);
								}
							}
							additionalColumns.push(newAdditionalColumm);

							var type = newField.TYPE == 'checkbox' ? 'bool' : newField.TYPE;
							type = ( type == 'string' || type == 'textarea') ? "string" : type;
							var newStoreField = {
								name: fieldParameter,
								type: type
							}
							
							additionalStoreFields.push(newStoreField);
						}
					}
					//Apply to config
					me.config["additionalColumns"] = additionalColumns;
					me.config["additionalStoreFields"] = additionalStoreFields;
					me.config["filterDisplayName"] = response.data.menuTitle + " Filter";
					me.activateGatewayView();
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	activateGatewayView : function(){
		var viewmodel = glu.model(Ext.apply({
			mtype : "RS.gateway.Main",
			clientVM: this.clientVM,
			screenVM: this.screenVM,
			SDK : true
		}, this.config));
		this.subVM = viewmodel;
		var view = glu.createViewmodelAndView(viewmodel);
		this.fireEvent('updateView', view);
		viewmodel.activate();
	}
});

glu.defModel("RS.gateway.CustomFilter",{
	template_mtype : "RS.gateway.CustomFilter",
	activate : function(screen, params){
		if(this.viewIsReady){
			if(this.name != params.name){ //re-render this screen if new gateway is loaded.
				this.set("name", params.name);
				this.loadCustomFilter(this.name);
			}
			else
				this.subVM.activate(screen, params);
		}
	},
	panel : "panel",
	viewIsReady : false,	
	subVM : null,
	init : function() {
		var me = this;
		this.on('viewIsReady', function(){
			me.viewIsReady = true;
			me.loadCustomFilter(this.name);
		});
	},
	
	loadCustomFilter : function(filterName){
		var me = this;
		Ext.Ajax.request({
			url : "/resolve/service/gateway/getCustomSDKFields",
			method : "GET",
			params : {
				customGatewayName : filterName
			},
			success : function(r){
				var response = RS.common.parsePayload(r);
				if (response.success) {
					var records = response.records;
					var data = response.data;
					if(records && records.length > 0){						
						me.defineViewModelConfig(data, records);
						me.defineViewConfig(records);
						me.activateFilterView();
					}
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	activateFilterView : function(){
		//Initialize model and build view
		var viewmodel = glu.model(Ext.apply({
			mtype : this.template_mtype,
			clientVM : this.clientVM,
			screenVM : this.screenVM
		}, this.params));
		var view = glu.createViewmodelAndView(viewmodel);
		this.subVM = viewmodel;	
		this.fireEvent('updateView', view);
	},

	defineViewModelConfig : function(data, records){
		var me = this;
		var config = {
			fields : RS.gateway.SDKcommonFields.concat([]),
			encryptFields: [],		
			gatewaytype: data.implPrefixes,
			mixins: ['BaseFilter'],
			modelName : data.implPrefixes + "Filter",
			defaultData : {},		
			SDK : true,
			init: function() {
				this.initBase();
				this.set('title', data.menuTitle + " Filter"); 
			}
		};

		for(var i = 0; i < records.length; i++){
			var field = records[i];
			var fieldType = field.TYPE;
			var rawFieldParameter = field.NAME;
			var fieldParameter = 'u' + rawFieldParameter;			
			config["fields"].push(fieldParameter);
			config["defaultData"][fieldParameter] = field.DEFAULTVALUE;
			if (fieldType == "encrypt") {
				config["fields"].push({ name: "is" + fieldParameter + "Encrypted", type: 'bool' });
				config["encryptFields"].push(fieldParameter);
				config["defaultData"]["is" + fieldParameter + "Encrypted"] = false;		
			} else if (fieldType == "textenc") {
				config["fields"].push({ name: "is" + fieldParameter + "Encrypted", type: 'bool' });
				config["encryptFields"].push(fieldParameter);
				config["defaultData"]["is" + fieldParameter + "Encrypted"] = false;
				config["fields"].push(fieldParameter + "Label");
				config["defaultData"][fieldParameter + "Label"] = "";			
			}

			if(fieldType == "textarea")
				Ext.apply(config, this.getTextareaVM(field));
			else if(fieldType == "text")
				Ext.apply(config, this.getTextfieldVM(field));
			else if(fieldType == "list")
				Ext.apply(config, this.getComboboxVM(field));
			else if(fieldType == "checkbox")
				Ext.apply(config, this.getCheckboxVM(field));
			else if(fieldType == "encrypt")
				Ext.apply(config, this.getEncryptVM(field));
			else if(fieldType == "textenc")
				Ext.apply(config, this.getTextEncryptVM(field));
		}

		glu.defModel(this.template_mtype, config);
	},

	getTextfieldVM : function(field){
		var config = {};
		// var fieldParameter = field.NAME;
		// if(!field.REQUIRE){
		// 	config[ fieldParameter + "IsValid$" ] = function(){
				
		// 		return this.testField && this.testField != '' ? true  : this.localize('invalidField');
		// 	}
		// }
		return config;
	},

	getTextareaVM : function(field){
		var config = {};
		// var fieldParameter = field.NAME;
		// if(field.REQUIRE){
		// 	config[ fieldParameter+ "IsValid$" ] = function(){
		// 		return this.fieldParameter && this.fieldParameter != '' ? true : this.localize('invalidField');
		// 	}
		// }
		return config;
	},

	getComboboxVM : function(field){
		var config = {};
		var rawFieldParameter = field.NAME;
		var fieldParameter = 'u' + rawFieldParameter;
		// if(field.REQUIRE){
		// 	config[ fieldParameter+ "IsValid$" ] = function(){
		// 		return this[fieldParameter] && this[fieldParameter] != '' ? true : this.localize('invalidField');
		// 	}
		// }

		//Add store for combo box values
		var valueSet = field.VALUES.split(",");
		var dataSet = []
		for(var i = 0; i < valueSet.length; i++){
			dataSet.push({
				value : valueSet[i]
			})
		}
		config[fieldParameter + "_store"] = {
			mtype : 'store',
			proxy : {
				type : 'memory'
			},
			fields : ['value'],
			data : dataSet
		}
		return config;
	},


	getCheckboxVM : function(field){
		var config = {};
		// var fieldParameter = field.NAME;
		// config[fieldParameter] = this.DEFAULTVALUE;
		// if(field.REQUIRE){
		// 	config[ fieldParameter+ "IsValid$" ] = function(){
		// 		return this.fieldParameter && this.fieldParameter != '' ? true : this.localize('invalidField');
		// 	}
		// }
		return config;
	},

	getEncryptVM: function() {
		var config = {};
		return config;
	},

	getTextEncryptVM: function() {
		var config = {};
		return config;
	},

	defineViewConfig : function(records){
		var standardViewConfig = {
			xtype: 'panel',
			style : 'border-top :1px solid #cccccc;',			
			layout: {
				type: 'vbox',
				align: 'stretch'
			}			
		}
		var additionalSettingViewConfig = {
			xtype: 'panel',
			title: '~~additionalSettingTitle~~',
			layout: {
				type: 'vbox',
				align: 'stretch'
			}
		};

		var leftViewContainer = [];
		var rightViewContainer = [];
		var fullViewContainer = [];
		var leftAdditionalSettingViewContainer = [];
		var rightAdditionalSettingViewContainer = [];
		var fullAdditionalSettingViewContainer = [];
		for(var i = 0; i < records.length; i++){
			var field = records[i];
			var fieldType = field.TYPE;
			var fieldConfig = {};

			//Construct view config.
			if(fieldType == "textarea")
				fieldConfig = this.getTextareaView(field);
			else if(fieldType == "text")
				fieldConfig = this.getTextfieldView(field);
			else if(fieldType == "list")
				fieldConfig = this.getComboboxView(field);
			else if(fieldType == "checkbox")
				fieldConfig = this.getCheckboxView(field);
			else if(fieldType == "encrypt")
				fieldConfig = this.getEncryptView(field);
			else if(fieldType == "textenc")
				fieldConfig = this.getTextEncryptView(field);

			//Check for standard field.
			if(field.STANDARD == "TRUE"){
				if(field.POSITION && field.POSITION.toLowerCase() == "left")
					leftViewContainer.push(fieldConfig);
				else if(field.POSITION && field.POSITION.toLowerCase() == "right")
					rightViewContainer.push(fieldConfig);
				else
					fullViewContainer.push(fieldConfig);
			}
			else {
				if(field.POSITION && field.POSITION.toLowerCase() == "left")
					leftAdditionalSettingViewContainer.push(fieldConfig);
				else if(field.POSITION && field.POSITION.toLowerCase() == "right")
					rightAdditionalSettingViewContainer.push(fieldConfig);
				else
					fullAdditionalSettingViewContainer.push(fieldConfig);
			}		
			
		}
		standardViewConfig["items"] = this.layoutFromConfig(leftViewContainer, rightViewContainer, fullViewContainer);
		additionalSettingViewConfig["items"] = this.layoutFromConfig(leftAdditionalSettingViewContainer, rightAdditionalSettingViewContainer, fullAdditionalSettingViewContainer);

		glu.defView(this.template_mtype, RS.gateway.buildSDKGatewayViewConfig(standardViewConfig, additionalSettingViewConfig));
	},

	getTextfieldView : function(field){
		var rawFieldParameter = field.NAME;
		var fieldParameter = 'u' + rawFieldParameter;
		var config = {
			fieldLabel : field.DISPLAYNAME,
			value : "@{" + fieldParameter + "}",
			name : fieldParameter,
			xtype : 'textfield',
		};
		return config;
	},

	getTextareaView : function(field){
		var rawFieldParameter = field.NAME;
		var fieldParameter = 'u' + rawFieldParameter;
		var config = {
			fieldLabel : field.DISPLAYNAME,
			value : "@{" + fieldParameter + "}",
			xtype : 'textareafield',
			name : fieldParameter,
			growMin : 200,
			growMax : 400,
			grow : true
		};
		return config;
	},

	getComboboxView : function(field){
		var rawFieldParameter = field.NAME;
		var fieldParameter = 'u' + rawFieldParameter;
		var config = {
			xtype: 'combobox',
			fieldLabel : field.DISPLAYNAME,
			value : "@{" + fieldParameter + "}",
			queryMode : 'local',
			store : '@{'+ fieldParameter + '_store}',
			displayField : 'value',
			name : fieldParameter,
			editable: false
		};
		return config;
	},


	getCheckboxView : function(field){
		var rawFieldParameter = field.NAME;
		var fieldParameter = 'u' + rawFieldParameter;
		var config = {
			fieldLabel : field.DISPLAYNAME,
			value : "@{" + fieldParameter + "}",
			xtype : 'checkbox'
		};
		return config;
	},

	getEncryptView : function(field){
		var me = this;
		var rawFieldParameter = field.NAME;
		var fieldParameter = 'u' + rawFieldParameter;
		var config = {
			layout : {
				type : 'hbox',
				align : 'stretch'
			},
			items : [{
				labelWidth: 130,
				fieldLabel : field.DISPLAYNAME,
				value : "@{" + fieldParameter + "}",
				name : fieldParameter,
				xtype : 'textfield',
				flex: 1,
				hidden: "@{is" + fieldParameter + "Encrypted}",
			},{
				labelWidth: 130,
				fieldLabel : field.DISPLAYNAME,
				value : "@{" + fieldParameter + "}",
				name : fieldParameter,
				xtype : 'textfield',
				inputType: 'password',
				flex: 1,
				hidden: "@{!is" + fieldParameter + "Encrypted}",
			},{
				boxLabel : "Encrypt",
				value : "@{is" + fieldParameter + "Encrypted}",
				xtype : 'checkbox',
				margin: "0 0 0 10px",
				id: fieldParameter,
				listeners: {
					change: function(ele, newVal, oldVal) {					
						if (oldVal && !newVal) {
							this._vm.set(this.id, "");									
						}
					}
				}
			}]
		};
		return config;
	},

	getTextEncryptView : function(field){
		var rawFieldParameter = field.NAME;
		var fieldParameter = 'u' + rawFieldParameter;
		var config = {
			layout : {
				type : 'hbox',
				align : 'stretch'
			},
			items : [{				
				hideLabel: true,
				value : "@{" + fieldParameter + "Label}",
				name : fieldParameter + "Label",
				xtype : 'textfield',
				width: 125,
			},{
				hideLabel: true,
				value : "@{" + fieldParameter + "}",
				name : fieldParameter,
				xtype : 'textfield',
				flex: 1,
				margin: "0 0 0 10px",
				hidden: "@{is" + fieldParameter + "Encrypted}",
			},{
				hideLabel: true,			
				value : "@{" + fieldParameter + "}",
				name : fieldParameter,
				xtype : 'textfield',
				inputType: 'password',
				flex: 1,
				margin: "0 0 0 10px",
				hidden: "@{!is" + fieldParameter + "Encrypted}",
			},{
				boxLabel : "Encrypt",
				value : "@{is" + fieldParameter + "Encrypted}",
				xtype : 'checkbox',
				margin: "0 0 0 10px",
				id: fieldParameter,
				listeners: {
					change: function(ele, newVal, oldVal) {					
						if (oldVal && !newVal) {
							this._vm.set(this.id, "");									
						}
					}
				}
			}]
		};
		return config;
	},

	layoutFromConfig : function(leftView, rightView, fullView){
		var items = [];
		var l = 0;
		var r = 0;
		var f = 0;

		for(;l < leftView.length; l++){
			var rightItem = {};
			if(r < rightView.length){
				rightItem = Ext.apply(rightItem, rightView[r]);
				r++;
			}
			items.push({
				layout : {
					type : 'hbox'				
				},
				defaults : {
					padding: '10px 10px 0px 10px',
					labelWidth : 130,
					flex : 1
				},
				items : [leftView[l], rightItem]
			})
		}

		for(; r < rightView.length; r++){
			items.push({
				layout : {
					type : 'hbox'
		
				},
				defaults : {
					padding: '10px 10px 0px 10px',
					labelWidth : 130,
					flex : 1
				},
				items : [{},rightView[r]]
			})
		}

		for(; f < fullView.length; f++){
			items.push(Ext.apply({
				padding: '10px 10px 0px 10px',
				labelWidth : 130,
				flex : 1
			},fullView[f]));
		}
		return items;
	}
})
glu.defModel('RS.gateway.DatabaseConnectionPool', {
	ignoreName: true,
	upoolName: '',
	upoolNameIsValid$: function() {
		return this.upoolName ? true : this.localize('invalidName');
	},
	udriverClass: '',
	u_db_us_ern_ame: '',
	u_db_pas_sword: '',
	udbconnectURI: '',
	udbmaxConn: '',
	modelName: 'DatabaseConnectionPool',
	mixins: ['BaseFilter'],
	gatewaytype: 'databaseConnectionPool',
	fields: ['upoolName', 'udriverClass', 'u_db_us_ern_ame', 'u_db_pas_sword', 'udbconnectURI', 'udbmaxConn'],
	driverStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	init: function() {
		this.initBase();
		this.set('title', this.localize('databaseConnectionPoolTitle'));
		this.loadDriver();
		this.activate(screen, {
			id: this.id,
			uniqueIdField: this.uniqueIdField
		});
	},
	userDateFormat$: function() {},
	udbmaxConnIsValid$: function() {
		var test = /^[0-9]*$/;
		return test.test(this.udbmaxConn) ? true : this.localize('invalidMaxConn');
	},
	handleSaveSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(this.localize('SaveErrMsg') + '[' + respData.message + ']');
		else {
			clientVM.displaySuccess(this.localize('SaveSucMsg'));
			this.cancel();
		}
		this.parentVM.loadFilters();
		this.parentVM.loadDeployedFilters();
	},

	respErr: function(resp) {
		clientVM.displayFailure(resp);
	},
	loadDriver: function() {
		this.ajax({
			url: '/resolve/service/gateway/getDatabaseDrivers',
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('loadDriverErrMsg'), respData.message);
					return;
				}
				var drivers = respData.data;
				for (db in drivers)
					this.driverStore.add({
						name: drivers[db],
						value: drivers[db]
					});
				this.set('udriverClass', this.driverStore.first().get('value'));
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	cancel: function() {
		this.doClose();
	},
	showGenerator: function() {
		this.set('generatorIsHidden', false);
	},

	dbType: '',
	//not smart...will revisit this
	dbTypeStore: {
		mtype: 'store',
		fields: ['name', 'value', 'driver'],
		proxy: {
			type: 'memory'
		},
		data: [{
			name: 'MySQL',
			driver: 'org.mariadb.jdbc.Driver',
			value: 'mysql'
		}, {
			name: 'Oracle-SID',
			driver: "oracle.jdbc.OracleDriver",
			value: 'oraclesid'
		}, {
			name: 'Oracle-Service',
			driver: "oracle.jdbc.OracleDriver",
			value: 'oracleservice'
		}, {
			name: 'DB2',
			driver: 'org.mariadb.jdbc.Driver',
			value: 'db2'
		}, {
			name: 'MSSQL',
			driver: 'com.microsoft.sqlserver.jdbc.SQLServerDriver',
			value: 'mssql'
		}, {
			name: 'PostgreSQL',
			driver: 'org.postgresql.Driver',
			value: 'postgresql'
		}]
	},
	ipAddress: '',
	port: '',
	dbName: '',
	dbNameIsVisible$: function() {
		return this.dbType == 'mysql' || this.dbType == 'db2' || this.dbType == 'mssql' || this.dbType == 'postgresql';
	},
	sid: '',
	sidIsVisible$: function() {
		return this.dbType == 'oraclesid';
	},
	service: '',
	serviceIsVisible$: function() {
		return this.dbType == 'oracleservice';
	},
	howToGetURI: '',
	dbInfo: false,
	custom: true,
	generateURI: function() {
		var url = '';
		switch (this.dbType) {
			case 'mysql':
				url = 'jdbc:mysql://' + this.ipAddress + ':' + this.port + '/' + this.dbName;
				break;
			case 'oraclesid':
				url = 'jdbc:oracle:thin:@' + this.ipAddress + ':' + this.port + ':' + this.sid;
				break;
			case 'oracleservice':
				url = 'jdbc:oracle:thin:@//' + this.ipAddress + ':' + this.port + '/' + this.service;
				break;
			case 'db2':
				url = 'jdbc:db2://' + this.ipAddress + ':' + this.port + '/' + this.dbName;
				break;
			case 'mssql':
				url = 'jdbc:microsoft:sqlserver://' + this.ipAddress + ':' + this.port + ';' + 'databaseName=' + this.dbName;
				break;
			case 'postgresql':
				url = 'jdbc:postgresql://' + this.ipAddress + ':' + this.port + '/' + this.dbName;
				break;
		}
		this.set('udriverClass', this.dbTypeStore.getAt(this.dbTypeStore.findExact('value', this.dbType)).get('driver'));
		this.set('udbconnectURI', url);
	},
	dbTypeChanged$: function() {
		this.set('ipAddress', '');
		this.set('port', '');
		this.set('dbName', '');
		this.set('sid', '');
		this.set('service', '');
		switch (this.dbType) {
			case 'mysql':
				this.set('port', 3306);
				break;
			case 'oraclesid':
				this.set('port', 1521);
				break;
			case 'oracleservice':
				this.set('port', 1521)
				break;
			case 'db2':
				this.set('port', 50000);
				break;
			case 'mssql':
				this.set('port', 1433);
				break;
			case 'postgresql':
				this.set('port', 5432);
				break;
		}
	},

	udriverClassIsVisible$: function() {
		return this.custom;
	}
});
RS.gateway.databaseConnectionPool = {
	modelName: 'databaseConnectionPool',
	uniqueIdField: 'upoolName',
	filterViewName: 'RS.gateway.DatabaseConnectionPool',
	requestParams: {
		modelName: 'DatabaseConnectionPool',
		queueName: 'DATABASE',
		gatewayName: 'DATABASE',
		gatewayClass: 'MDatabase',
		getterMethod: 'getPools',
		clearAndSetMethod: 'clearAndSetPools',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},

	concatColumns: function() {
		this.set('filterColumns', [{
			header: '~~upoolName~~',
			dataIndex: 'upoolName',
			filterable: true,
			flex: 1
		}, {
			header: '~~udriverClass~~',
			dataIndex: 'udriverClass',
			filterable: true,
			flex: 1
		}, {
			header: '~~u_db_us_ern_ame~~',
			dataIndex: 'u_db_us_ern_ame',
			filterable: true,
			flex: 1
		}, {
			header: '~~udbconnectURI~~',
			dataIndex: 'udbconnectURI',
			filterable: true,
			flex: 1
		}, {
			header: '~~udbmaxConn~~',
			dataIndex: 'udbmaxConn',
			filterable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()));
	},

	config: function() {
		this.set('filterDisplayName', this.localize('databaseConnectionPoolDisplayName'));
		this.set('title', this.localize('databaseConnectionPoolTitle'));
	},

	getStoreFields: function() {
		if (!this.gatewaytype)
			alert('Need to specify a gatewaytype!');
		var fields = [
			'id',
			'upoolName',
			'udriverClass',
			'u_db_us_ern_ame',
			'udbconnectURI',
			'udbmaxConn'
		].concat(RS.common.grid.getSysFields());
		return fields;
	},

	createFilter: function() {
		this.open({
			mtype: this.filterViewName
		});
	},
	editFilter: function(id) {
		this.open({
			mtype: this.filterViewName,
			uniqueIdField: this.uniqueIdField,
			gatewaytype: this.gatewaytype,
			id: id
		});
	}
};

glu.defModel('RS.gateway.DatabaseConnectionPoolMain', {
	mixins: ['Main'],
	gatewaytype: 'databaseConnectionPool'
});
glu.defModel('RS.gateway.DatabaseFilter', {
	usql: '',
	uupdateSql: '',
	upool: '',
	ulastValue: '',
	ulastValueColumn: '',
	uprefix: '',
	ulastValueQuote: '',
	modelName: 'DatabaseFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'database',
	fields: RS.gateway.commonFields.concat([
		'usql',
		'uupdateSql',
		'upool',
		'ulastValue',
		'ulastValueColumn',
		'uprefix',
		'ulastValueQuote'
	]),
	poolStore: {
		mtype: 'store',
		fields: ['upoolName'],
		proxy: {
			type: 'memory'
		}
	},
	init: function() {
		this.initBase();
		this.set('title', this.localize('databaseFilterTitle'));
		this.loadPool();
	},

	loadPool: function() {
		this.ajax({
			url: '/resolve/service/gateway/listFilters',
			method: 'GET',
			params: {
				modelName: 'DatabaseConnectionPool',
				start: 0,
				limit: 50
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				var records = respData.records;
				Ext.each(records, function(r) {
					this.poolStore.add({
						upoolName: r.upoolName
					});
				}, this)
				// this.poolStore.add(records);
			},
			failure: function(resp) {
				clientVM.displayError(this.localize('ServerErr'));
			}
		})
	}
});
RS.gateway.database = {
	modelName: 'database',
	filterViewName: 'RS.gateway.DatabaseFilter',
	poolStore: null,
	requestParams: {
		modelName: 'DatabaseFilter',
		queueName: 'DATABASE',
		gatewayName: 'DB',
		gatewayClass: 'MDatabase',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~upool~~',
		dataIndex: 'upool',
		filterable: true,
		flex: 1
	}, {
		header: '~~usql~~',
		dataIndex: 'usql',
		filterable: true,
		flex: 1
	}, {
		header: '~~uupdateSql~~',
		dataIndex: 'uupdateSql',
		filterable: true,
		flex: 1
	}, {
		header: '~~ulastValue~~',
		dataIndex: 'ulastValue',
		filterable: true,
		flex: 1
	}, {
		header: '~~ulastValueColumn~~',
		dataIndex: 'ulastValueColumn',
		filterable: true,
		flex: 1
	}, {
		header: '~~uprefix~~',
		dataIndex: 'uprefix',
		filterable: true,
		flex: 1
	}, {
		header: '~~ulastValueQuote~~',
		dataIndex: 'ulastValueQuote',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'upool',
		type: 'string'
	}, {
		name: 'usql',
		type: 'string'
	}, {
		name: 'uupdateSql',
		type: 'string'
	}, {
		name: 'ulastValue',
		type: 'string'
	}, {
		name: 'ulastValueColumn',
		type: 'string'
	}, {
		name: 'uprefix',
		type: 'string'
	}, {
		name: 'ulastValueQuote',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('databaseFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('databaseGatewayDisplayName'));
		this.set('title', this.localize('databaseTitle'));
	}
};

glu.defModel('RS.gateway.DatabaseMain', {
	mixins: ['Main'],
	gatewaytype: 'database'
});
glu.defModel('RS.gateway.EmailAddress', {
	ignoreName: true,
	uemailAddress: '',
	uemailAddressIsValid$: function() {
		return this.uemailAddress ? true : this.localize('invalidAddress');
	},
	uemailPassword: '',
	modelName: 'EmailAddress',
	mixins: ['BaseFilter'],
	gatewaytype: 'emailAddress',
	fields: RS.gateway.commonFields.concat(['uemailAddress', 'uemailPassword', 'uniqueId']),

	init: function() {
		this.initBase();
		this.set('title', this.localize('emailAddressTitle'));
		this.activate(screen, {
			id: this.id,
			uniqueIdField: this.uniqueIdField
		});
	},

	userDateFormat$: function() {},

	handleSaveSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(this.localize('SaveErrMsg') + '[' + respData.message + ']');
		else {
			clientVM.displaySuccess(this.localize('SaveSucMsg'));
			this.cancel();
		}

		this.parentVM.loadFilters();
		this.parentVM.loadDeployedFilters();
	},

	respErr: function(resp) {
		clientVM.displayFailure(resp);
	},

	cancel: function() {
		this.doClose();
	}
});
RS.gateway.emailAddress = {
	modelName: 'emailAddress',
	uniqueIdField: 'uemailAddress',
	filterViewName: 'RS.gateway.EmailAddress',
	requestParams: {
		modelName: 'EmailAddress',
		queueName: 'EMAIL',
		gatewayName: 'EMAIL',
		gatewayClass: 'MEmail',
		getterMethod: 'getEmailAddresses',
		clearAndSetMethod: 'clearAndSetEmailAddresses',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},

	getStoreFields: function() {
		if (!this.gatewaytype)
			alert('Need to specify a gatewaytype!');
		var fields = [
			'id',
			'uqueue',
			'uemailAddress',
            {
                name: 'changed',
                type: 'bool',
                defaultValue: false
            }
		].concat(RS.common.grid.getSysFields());
		return fields;
	},
	concatColumns: function() {
		this.set('filterColumns', [{
			header: '~~uemailAddress~~',
			dataIndex: 'uemailAddress',
			filterable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()));
	},
	config: function() {
		this.set('filterDisplayName', this.localize('emailAddressDisplayName'));
		this.set('title', this.localize('emailAddressTitle'));
	},

	createFilter: function() {
		this.open({
			mtype: this.filterViewName
		});
	},
	editFilter: function(id) {
		this.open({
			mtype: this.filterViewName,
			uniqueIdField: this.uniqueIdField,
			gatewaytype: this.gatewaytype,
			id: id
		});
	}
};

glu.defModel('RS.gateway.EmailAddressMain', {
	mixins: ['Main'],
	gatewaytype: 'emailAddress'
});
glu.defModel('RS.gateway.EmailFilter', {
	uquery: '',
	modelName: 'EmailFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'email',
	fields: RS.gateway.commonFields.concat(['uquery']),
	// uqueryIsValid$:function(){
	// 	return this.uquery&&this.uquery!=''?true:this.localize('invalidQuery');
	// },

	init: function() {
		this.initBase();
		this.set('title', this.localize('emailFilterTitle'));
	}
});
RS.gateway.email = {
	modelName: 'email',
	filterViewName: 'RS.gateway.EmailFilter',
	requestParams: {
		modelName: 'EmailFilter',
		queueName: 'EMAIL',
		gatewayName: 'EMAIL',
		gatewayClass: 'MEmail',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uquery~~',
		dataIndex: 'uquery',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'uquery',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('emailFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('emailGatewayDisplayName'));
		this.set('title', this.localize('emailTitle'));
	}
};

glu.defModel('RS.gateway.EmailMain', {
	mixins: ['Main'],
	gatewaytype: 'email'
});
glu.defModel('RS.gateway.EWSAddress', {
	ignoreName: true,
	uewsaddress: '',
	uewsaddressIsValid$: function() {
		return this.uewsaddress ? true : this.localize('invalidAddress');
	},
	uewspassword: '',
	modelName: 'EWSAddress',
	mixins: ['BaseFilter'],
	gatewaytype: 'ewsAddress',
	fields: RS.gateway.commonFields.concat(['uewsaddress', 'uewspassword']),

	init: function() {
		this.initBase();
		this.set('title', this.localize('ewsAddressTitle'));
		this.activate(screen, {
			id: this.id,
			uniqueIdField: this.uniqueIdField
		});
	},

	userDateFormat$: function() {},

	handleSaveSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(this.localize('SaveErrMsg') + '[' + respData.message + ']');
		else {
			clientVM.displaySuccess(this.localize('SaveSucMsg'));
			this.cancel();
		}

		this.parentVM.loadFilters();
		this.parentVM.loadDeployedFilters();
	},

	respErr: function(resp) {
		clientVM.displayFailure(resp);
	},

	cancel: function() {
		this.doClose();
	}
});
RS.gateway.ewsAddress = {
	modelName: 'ewsAddress',
	filterViewName: 'RS.gateway.EWSAddress',
	uniqueIdField: 'uewsaddress',
	requestParams: {
		modelName: 'EWSAddress',
		queueName: 'EWS',
		gatewayName: 'EWS',
		gatewayClass: 'MEWS',
		getterMethod: 'getEWSAddress',
		clearAndSetMethod: 'clearAndSetEWSAddresses',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	getStoreFields: function() {
		if (!this.gatewaytype)
			alert('Need to specify a gatewaytype!');
		var fields = [
			'id',
			'uewsaddress',
            'uqueue',
            { 
                name: 'changed',
                type: 'bool',
                defaultValue: false
            }
		].concat(RS.common.grid.getSysFields());
		return fields;
	},
	concatColumns: function() {
		this.set('filterColumns', [{
			header: '~~uewsaddress~~',
			dataIndex: 'uewsaddress',
			filterable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()));
	},
	config: function() {
		this.set('filterDisplayName', this.localize('ewsAddressFilterDisplayName'));
		this.set('title', this.localize('ewsAddressTitle'));
	},
	createFilter: function() {
		this.open({
			mtype: this.filterViewName
		});
	},
	editFilter: function(id) {
		this.open({
			mtype: this.filterViewName,
			uniqueIdField: this.uniqueIdField,
			gatewaytype: this.gatewaytype,
			id: id
		});
	}
};

glu.defModel('RS.gateway.EWSAddressMain', {
	mixins: ['Main'],
	gatewaytype: 'ewsAddress'
});
glu.defModel('RS.gateway.EWSFilter', {
	uquery: '',
	uincludeAttachment: '',
	ufolderNames: '',
    uonlyNew: '',
	modelName: 'EWSFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'ews',
	fields: RS.gateway.commonFields.concat(['uquery', {
		name: 'uincludeAttachment',
		type: 'bool'
	},'ufolderNames',{
		name: 'umarkAsRead',
		type: 'bool'
	},{
		name: 'uonlyNew',
		type: 'bool',
        defaultValue: true,
        convert: function(val) {
            if (val == null)
                return true;
            else
                return val;
        }
    }]),
    defaultData : {
        uonlyNew : true
    },
	// uqueryIsValid$:function(){
	// 	return this.uquery&&this.uquery!=''?true:this.localize('invalidQuery');
	// },

	// uincludeAttachmentIsValid$:function(){
	// 	return this.uincludeAttachment&&this.uincludeAttachment!=''?true:this.localize('invalidQuery');
	// },

	init: function() {
		this.initBase();
		this.set('title', this.localize('ewsFilterTitle'));
	}
});
RS.gateway.ews = {
	modelName: 'ews',
	filterViewName: 'RS.gateway.EWSFilter',
	requestParams: {
		modelName: 'EWSFilter',
		queueName: 'EWS',
		gatewayName: 'EWS',
		gatewayClass: 'MEWS',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uquery~~',
		dataIndex: 'uquery',
		filterable: true,
		flex: 1
	}, {
		header: '~~uincludeAttachment~~',
		dataIndex: 'uincludeAttachment',
		filterable: true,
		flex: 1
	},	{
		header: '~~uonlyNew~~',
		dataIndex: 'uonlyNew',
		filterable: true,
		flex: 1
	},	{
		 header: '~~ufolderNames~~',
		 dataIndex: 'ufolderNames',
		 filterable: true,
		 flex: 1
	},	{
		 header: '~~umarkAsRead~~',
		 dataIndex: 'umarkAsRead',
		 filterable: true,
		 flex: 1
	}],

	additionalStoreFields: [{
		name: 'uquery',
		type: 'string'
	}, {
		name: 'uincludeAttachment',
		type: 'string'
	}, {
		name: 'ufolderNames',
	}, {
		name: 'umarkAsRead',
		type: 'string'
	},{
		name: 'uonlyNew',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('ewsFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('ewsGatewayDisplayName'));
		this.set('title', this.localize('ewsTitle'));
	}
};

glu.defModel('RS.gateway.EWSMain', {
	mixins: ['Main'],
	gatewaytype: 'ews'
});
glu.defModel('RS.gateway.ExchangeFilter', {
	usubject: '',
	ucontent: '',
	usendername: '',
	usenderaddress: '',
	modelName: 'ExchangeserverFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'exchange',
	fields: RS.gateway.commonFields.concat(['usubject', 'ucontent', 'usendername', 'usenderaddress']),
	// usubjectIsValid$:function(){
	// 	return this.usubject&&this.usubject!=''?true:this.localize('invalidQuery');
	// },
	// ucontentIsValid$:function(){
	// 	return this.ucontent&&this.ucontent!=''?true:this.localize('invalidQuery');
	// },
	// usendernameIsValid$:function(){
	// 	return this.usendername&&this.usendername!=''?true:this.localize('invalidQuery');
	// },
	// usenderaddressIsValid$:function(){
	// 	return this.usenderaddress&&this.usenderaddress!=''?true:this.localize('invalidQuery');
	// },
	init: function() {
		this.initBase();
		this.set('title', this.localize('exchangeFilterTitle'));
	}
});
RS.gateway.exchange = {
	modelName: 'exchange',
	filterViewName: 'RS.gateway.ExchangeFilter',
	requestParams: {
		modelName: 'ExchangeserverFilter',
		queueName: 'EXCHANGE',
		gatewayName: 'EXCHANGE',
		gatewayClass: 'MExchange',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~usubject~~',
		dataIndex: 'usubject',
		filterable: true,
		flex: 1
	}, {
		header: '~~ucontent~~',
		dataIndex: 'ucontent',
		filterable: true,
		flex: 1
	}, {
		header: '~~usendername~~',
		dataIndex: 'usendername',
		filterable: true,
		flex: 1
	}, {
		header: '~~usenderaddress~~',
		dataIndex: 'usenderaddress',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'usubject',
		type: 'string'
	}, {
		name: 'ucontent',
		type: 'string'
	}, {
		name: 'usendername',
		type: 'string'
	}, {
		name: 'usenderaddress',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('exchangeFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('exchangeGatewayDisplayName'));
		this.set('title', this.localize('exchangeTitle'));
	}
};

glu.defModel('RS.gateway.ExchangeMain', {
	mixins: ['Main'],
	gatewaytype: 'exchange'
});
glu.defModel('RS.gateway.Filters', {
	title: 'Add Filters',
	// modelName:'EmailFilter',

	filtersSelections: [],

	init: function() {},

	addFilters: function() {
		this.parentVM.addFilterFromWindow(this.filtersSelections);
		this.cancel();
	},

	cancel: function() {
		this.doClose();
	}
});
glu.defModel('RS.gateway.Gateways', {
	title: 'Gateways',
	gatewayStore: {
		mtype: 'store',
		fields: ['uqueue'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/gateway/listGateways',
			reader: {
				type: 'array',
				root: 'records',
				successProperty: 'success'
			}
			/* handled below in handleAjaxException()
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
			*/
		}
	},

	columns: [{
		header: '~~Gateway~~',
		dataIndex: 'uqueue',
		filterable: true,
		flex: 1
	}],

	state: '',

	gatewaysSelections: [],

	activate: function() {

	},

	init: function() {
		this.loadGateways();
	},

	deleteGateways: function() {
		this.message({
			title: this.localize('deleteGatewaysTitle'),
			msg: this.localize(this.gatewaysSelections.length > 1 ? 'deleteGatewaysMsg' : 'deleteGatewayMsg', {
				num: this.gatewaysSelections.length
			}),
			button: Ext.MessageBox.YES,
			buttonText: {
				yes: this.localize('confirmDeleteGateway'),
				no: this.localize('cancel')
			},
			fn: this.sendDeleteGatewayReq
		});
	},

	sendDeleteGatewayReq: function(btn) {
		if (btn == 'no') {
			return;
		}
		var ids = [];

		Ext.each(this.gatewaysSelections,function(r){
			ids.push(r.get('id'));
		});

		this.ajax({
			url: '/resolve/service/gateway/deleteGateways',
			params: {
				modelName: 'EmailFilter',
				ids: ids
			},
			scope: this,
			success: function(resp) {
				this.set('state', 'ready');
				this.handleDeleteSuccess(resp);
			},
			failure: function(resp) {
				var error = {};
				error.status = resp.status;
				this.handleAjaxException(error);
			}
		});
	},

	handleDeleteSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			var error = {};
			error.title = 'DeleteGatewaysErrTitle';
			error.msg = 'DeleteGatewaysErrMsg';
			error.ps = respData.message;
			var next = (function(obj) {
				return function() {
					obj.loadGateways()
				}
			})(this);
			this.handleAjaxException(error, next);
			return;
		}
		this.loadGateways();
	},

	deleteGatewaysIsEnabled$: function() {
		return this.state != 'wait';
	},

	loadGateways: function() {
		this.set('state', 'wait');
		this.gatewayStore.load({
			scope: this,
			params: {
				modelName: 'EmailFilter'
			},
			callback: function(records, op, suc) {
				if (!suc) {
					var e = {};
					if (op.error) {
						e.status = op.error.status;
					}
					e.title = 'ListGatewaysErrTitle';
					e.msg = 'ListGatewaysErrMsg';

					this.handleAjaxException(e);
				}
				this.set('state', 'ready');
			}
		});
	},

	handleAjaxException: function(error, next) {
		var title = null;
		var msg = null;
		if (error.status) {
			title = this.localize('ServerErrTitle');
			msg = this.localize('ServerErrMsg') + '[' + error.status + ']';
		} else {
			title = this.localize(error.title);
			msg = this.localize(error.msg);
		}

		this.message({
			title: title,
			msg: msg,
			buttons: Ext.MessageBox.YES,
			buttonText: {
				yes: this.localize('confirmAjaxException')
			},
			scope: this,
			fn: function() {
				if (next)
					next();
			}
		});
	},

	cancel: function() {
		this.doClose();
	}
});
glu.defModel('RS.gateway.HPOMFilter', {
	uquery: '',
	modelName: 'HPOMFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'hpom',
	objectStore: {
		mtype: 'store',
		fields: ['uobject'],
		proxy: {
			type: 'memory'
		}
	},
	fields: RS.gateway.commonFields.concat(['uobject', 'uquery']),

	activate: function(screen, params) {
		this.activateBase(params);
		this.loadObjects();
	},

	init: function() {
		this.initBase();
		this.set('title', this.localize('hpomFilterTitle'));
	},

	loadObjects: function() {
		this.ajax({
			url: '/resolve/service/gateway/listObjectNames',
			params: {
				gatewayName: 'HPOM'
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('LoadObjErr') + '[' + respData.message + ']');
					return;
				}
				var objs = respData.records || {};
				this.objectStore.removeAll();
				Ext.each(objs, function(o) {
					this.objectStore.add({
						uobject: o
					});
				}, this);
				if (!this.uobject)
					this.set('uobject', this.objectStore.first().get('uobject'));
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	}
});
RS.gateway.hpom = {
	modelName: 'hpom',
	filterViewName: 'RS.gateway.HPOMFilter',
	requestParams: {
		modelName: 'HPOMFilter',
		queueName: 'HPOM',
		gatewayName: 'HPOM',
		gatewayClass: 'MHPOM',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uobject~~',
		dataIndex: 'uobject',
		filterable: true,
		flex: 1
	}, {
		header: '~~uquery~~',
		dataIndex: 'uquery',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'uobject',
		type: 'string'
	}, {
		name: 'uquery',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('hpomFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('hpomGatewayDisplayName'));
		this.set('title', this.localize('hpomTitle'));
	}
};

glu.defModel('RS.gateway.HPOMMain', {
	mixins: ['Main'],
	gatewaytype: 'hpom'
});
glu.defModel('RS.gateway.HPSMFilter', {
	uquery: '',
	modelName: 'HPSMFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'hpom',
	objectStore: {
		mtype: 'store',
		fields: ['uobject'],
		proxy: {
			type: 'memory'
		}
	},
	fields: RS.gateway.commonFields.concat(['uobject', 'uquery']),
	activate: function(screen, params) {
		this.activateBase(params);
		this.loadObjects();
	},
	init: function() {
		this.initBase();
		this.set('title', this.localize('hpsmFilterTitle'));
	},
	loadObjects: function() {
		this.ajax({
			url: '/resolve/service/gateway/listObjectNames',
			params: {
				gatewayName: 'HPSM'
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('LoadObjErr') + '[' + respData.message + ']');
					return;
				}
				var objs = respData.records || {};
				this.objectStore.removeAll();
				Ext.each(objs, function(o) {
					this.objectStore.add({
						uobject: o
					});
				}, this);
				if (!this.uobject)
					this.set('uobject', this.objectStore.first().get('uobject'));
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	}
});
RS.gateway.hpsm = {
	modelName: 'hpsm',
	filterViewName: 'RS.gateway.HPSMFilter',
	requestParams: {
		modelName: 'HPSMFilter',
		queueName: 'HPSM',
		gatewayName: 'HPSM',
		gatewayClass: 'MHPSM',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uobject~~',
		dataIndex: 'uobject',
		filterable: true,
		flex: 1
	}, {
		header: '~~uquery~~',
		dataIndex: 'uquery',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'uobject',
		type: 'string'
	}, {
		name: 'uquery',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('hpsmFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('hpsmGatewayDisplayName'));
		this.set('title', this.localize('hpsmTitle'));
	}
};

glu.defModel('RS.gateway.HPSMMain', {
	mixins: ['Main'],
	gatewaytype: 'hpsm'
});
glu.defModel('RS.gateway.HTTPFilter', {
	// MAX_N_FILTERED_RESPONSE_CHAR: 10000,
	DEFAULT: 1,
	GATEWAY: 2,
	WORKSHEET: 3,
	RUNBOOK: 4,
	FILTERED_RESPONSE: 5,
	CDEFAULT: 'default',
	CJSON: 'application/json',
	uport: '',
	modelName: 'HTTPFilter',

	mixins: ['BaseFilter'],
	gatewaytype: 'http',
	fields: RS.gateway.commonFields.concat(['uuri', 'uport', 'uallowedIP', 'uhttpAuthType',
		{
		name: 'ussl',
		type: 'bool'
		}, 'u_bas_icAu_thUs_ername', 'u_bas_icAu_thPas_sword' 
		,{
			name : 'ublocking'
		}, 'ucontentType']),
	blockStore : null,
	contentTypeStore: {
		mtype: 'store',
		fields: ['value', 'display'],
		proxy: 'memory'
	},
	defaultData : {
		ublocking : 1,
		ucontentType: 'application/json'
	},
	unpwHidden$: function() { return this.uhttpAuthType != "BASIC"},
	contentCbTypeDisabled$: function () {
		// The content type combo is disabled on "Default", "Worksheet Id" or "Execution Complete" 
		if (this.ublocking === this.DEFAULT || this.ublocking === this.WORKSHEET || this.ublocking === this.RUNBOOK || this.ublocking === this.FILTERED_RESPONSE) {
			this.set('ucontentType', this.CJSON);
			return true;
		} else if (this.ublocking === this.GATEWAY) {
			this.set('ucontentType', this.CDEFAULT);
		}
		return false;
		
	},
	//filteredResponseSelected$: function () {
		// Filtered Response only show if "Filtered Response" is selected
	//	return this.ublocking === this.FILTERED_RESPONSE;
	//},
	// uresponseFilterIsValid$: function() {
	// 	return !this.filteredResponseSelected$() || this.uresponsefilter && this.uresponsefilter.length <= this.MAX_N_FILTERED_RESPONSE_CHAR;
	// },

	init: function() {
		this.initBase();
		this.contentTypeStore.loadData([
			{value: this.CDEFAULT, display: this.localize('contentTypeDefault') },
			{value: this.CJSON, display: this.localize('contentTypeJSON')}
		]);
		var blockStore = Ext.create('Ext.data.Store',{
			fields : ['value', 'display'],
			proxy : {
				type : 'memory'
			},
			data : [
			{
				value : this.DEFAULT,
				display : this.localize('defaultBlockCall')
			},{
				value : this.GATEWAY,
				display : this.localize('gatewayBlockCall')
			},{
				value : this.WORKSHEET,
				display : this.localize('worksheetBlockCall')
			},{
				value : this.RUNBOOK,
				display : this.localize('runbookBlockCall')
			}, {
				value : this.FILTERED_RESPONSE,
				display : this.localize('filteredResponseBlockCall')
			}]
		});
		this.set('blockStore', blockStore);
		this.set('title', this.localize('httpFilterTitle'));
	}
});

RS.gateway.http = {
	modelName: 'http',
	filterViewName: 'RS.gateway.HTTPFilter',
	requestParams: {
		modelName: 'HTTPFilter',
		queueName: 'HTTP',
		gatewayName: 'HTTP',
		gatewayClass: 'MHttp',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
        header: '~~uuri~~',
        dataIndex: 'uuri',
        filterable: true,
        renderer: RS.common.grid.plainRenderer(),
        flex: 2
    },{
		header: '~~uport~~',
		dataIndex: 'uport',
		filterable: true,
		flex: 1
	},{
        header: '~~ussl~~',
        dataIndex: 'ussl',
		renderer: RS.common.grid.booleanRenderer(),
        filterable: true,
        flex: 1
/*    },{
        header: '~~uallowedIP~~',
        dataIndex: 'uallowedIP',
        filterable: true,
        renderer: RS.common.grid.plainRenderer(),
        flex: 1 */
    },{
    	header: '~~ublocking~~',
        dataIndex: 'ublocking',
		renderer: RS.common.grid.booleanRenderer(),
        filterable: true,
        flex: 1
    }],

	additionalStoreFields: [{
        name: 'uuri',
        type: 'string'
	},{
        name: 'uport',
        type: 'string'
    },{
        name: 'ussl',
        type: 'string'
    },{
    	name : 'ublocking',
    	type : 'bool',
        convert : function(val){
            return val == 1 ? false : true;
        }
    },{
        name: 'uallowedIP',
        type: 'string'
    },{
        name: 'u_bas_icAu_thPas_sword',
        type: 'string'
    },{
        name: 'u_bas_icAu_thUs_ername',
        type: 'string'
    }],
	config: function() {
		this.set('filterDisplayName', this.localize('httpFilterDisplayName'));
		this.set('title', this.localize('httpTitle'));
	}
};

glu.defModel('RS.gateway.HTTPMain', {
	mixins: ['Main'],
	gatewaytype: 'http'
});
glu.defModel('RS.gateway.Main', {
	/*-------------Gateway specific fields start----------------*/
	modelName: null,
	filterViewName: null,
	uniqueIdField: 'uname',
	requestParams: null,
	additionalColumns: null,
	additionalStoreFields: null,
	config: null,
	/*-------------Gateway specific fields end----------------*/
	renderer: function(value, row, record, extraRenderer) {
		/*if (record.get('isDeleted') && record.get('uqueue') != '<|Default|Queue|>')
			row.style = 'background-color:red !important';
		if (record.get('uqueue') === '<|Default|Queue|>' && !record.get('isMaster'))
			row.style = 'background-color:#25E01B !important';
		if (record.get('uqueue') != '<|Default|Queue|>' && record.get('changed') && !record.get('isDeleted'))
			row.style = 'background-color:#FFEE00 !important';
		return (extraRenderer ? extraRenderer(value) : value);*/
	},

	chainRenderer: function(extraRenderer) {
		return function(value, row, record) {
			/*if (record.get('isDeleted') && record.get('uqueue') != '<|Default|Queue|>')
				row.style = 'background-color:red !important';
			if (record.get('uqueue') === '<|Default|Queue|>' && !record.get('isMaster'))
				row.style = 'background-color:#25E01B !important';
			if (record.get('uqueue') != '<|Default|Queue|>' && record.get('changed') && !record.get('isDeleted'))
				row.style = 'background-color:#FFEE00 !important';
			return (extraRenderer ? extraRenderer(value) : value);*/
		}
	},

	reqCount: 0,

	mock: false,

	gatewaytype: '',

	title: '',

	id: '',

	filterStateId: 'gatewayFilters',

	gatewayStateId: 'gatewayQueues',

	filterDisplayName: '',

	gatewayDisplayName: '',

	filterFieldSetTitle: '',

	gatewayFieldSetTitle: '',

	filters: null,

	gatewaysStore: {
		mtype: 'store',
		fields: ['uqueue'],
		proxy: {
			type: 'memory'
		}
	},
	gateway: null,
	deployedFilters: null,
	filtersSelections: [],
	deployedFiltersSelections: [],
	requestParams: null,
	filterColumns: null,
	deployedColumns: null,

	activate: function(screen, params) { //Rs client will trigger this event when this gateway comes into view.
		window.document.title = this.localize('windowTitle');
		this.loadFilters();
		this.loadGateways();
		this.loadDeployedFilters();
	},

	isDeployedFiltersEmpty: true,

	init: function() {
		if (this.mock) this.backend = RS.gateway.createMockBackend(true);

		//Apply Gateway config into main config model. For SDK those attributes are already applied.
		if(!this.SDK){
			for (attr in RS.gateway[this.gatewaytype]) {
				this[attr] = RS.gateway[this.gatewaytype][attr];
			}
		}
		this.set('filterFieldSetTitle', this.localize('filterFieldSetTitle'));
		this.set('gatewayFieldSetTitle', this.localize('gatewayFieldSetTitle'));

		//Setting up store for filters and columns (grid)
		this.initFilterStore();
		this.initDeployedFilterStore();
		this.initFilterColumns();
		this.initDeployedFilterColumns(); //Not sure why we need this?

		//Setting culumns for deployment section using columns from filters.
		var dc = [];
		Ext.each(this.filterColumns, function(f) {
			var c = {};
			Ext.apply(c, f);
			if (f.dataIndex == 'sysCreatedOn' || f.dataIndex == 'sysUpdatedOn')
				c.renderer = (function(self) {
					return function(value, row, record) {
						var v = Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())(value);
						//self.renderer(v, row, record);
						return v;
					}
				})(this)		
			dc.push(c);
		}, this);
		this.set('deployedColumns', dc);

		//Call config function of this gateway. Mostly apply display name of the gateway to appropriate field.
		this.config();

		//Setting up params for filter and deployed filter before load.
		this.deployedFilters.on('beforeload', function(store, operations) {
			operations.params = operations.params || {};
			Ext.apply(operations.params, this.requestParams);
			operations.params['queueName'] = this.gateway;
		}, this);

		this.filters.on('beforeload', function() {
			this.set('reqCount', this.reqCount + 1);
		}, this);

		//Handle call back data.
		this.filters.on('load', function(records, op, suc) {
			this.set('reqCount', this.reqCount - 1);
			if (!suc) {
				//clientVM.displayError(this.localize('ListFiltersErrMsg'));
				return;
			}

		}, this)
		this.deployedFilters.on('datachanged', function() {
			this.set('numOfDeployedFilters', this.deployedFilters.getCount());
		}, this);
	},
	getStoreFields: function() {
		if (!this.additionalStoreFields)
			alert('Need to specify a gatewaytype!');
		var fields = [{
			name: 'id',
			type: 'string'
		}, {
			name: 'uqueue',
			type: 'string'
		}, {
			name: 'uniqueId',
			type: 'string'
		}, {
			name: 'changed',
			type: 'bool',
			defaultValue: false
		}, {
			name: 'isDeleted',
			type: 'bool',
			defaultValue: false
		}, {
			name: 'isMaster',
			type: 'bool',
			defaultValue: true
		}].concat(this.additionalStoreFields).concat(RS.common.grid.getSysFields());
		if(!this.SDK){
			fields = fields.concat([{
				name: 'uname',
				type: 'string'
			}, {
				name: 'uactive',
				type: 'bool'
			}, {
				name: 'uorder',
				type: 'int'
			}, {
				name: 'uinterval',
				type: 'int'
			}, {
				name: 'ueventEventId',
				type: 'string'
			}, {
				name: 'urunbook',
				type: 'string'
			}])
		}
		return fields;
	},

	initFilterStore: function() {
		var fields = this.getStoreFields();
		var filters = Ext.create('Ext.data.Store', {
			fields: fields,
			proxy: {
				type: 'ajax',
				extraParams: this.requestParams,
				url: '/resolve/service/gateway/listFilters',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});
		this.set('filters', filters);
	},

	initDeployedFilterStore: function() {
		var fields = this.getStoreFields();
		var deployedFilters = Ext.create('Ext.data.Store', {
			fields: fields,
			proxy: {
				type: 'ajax',
				extraParams: this.requestParams,
				url: '/resolve/service/gateway/listDeployedFilters',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});

		this.set('deployedFilters', deployedFilters);
	},

	concatColumns: function() {
		//Concatinate all defauls column + sys column + additional columns for this gateway
		var filterColumns = this.SDK ? [] : [{
			header: '~~uname~~',
			dataIndex: 'uname',
			filterable: true,
			flex: 1
		}, {
			header: '~~uactive~~',
			dataIndex: 'uactive',
			filterable: true,
			width: 70,
			align : 'center'
		}, {
			header: '~~uorder~~',
			dataIndex: 'uorder',
			filterable: true,
			width: 70,
			align : 'center'
		}, {
			header: '~~uinterval~~',
			dataIndex: 'uinterval',
			filterable: true,
			width: 100,
			align : 'center'
		}, {
			header: '~~ueventEventId~~',
			dataIndex: 'ueventEventId',
			filterable: true,
			flex: 1
		}, {
			header: '~~urunbook~~',
			dataIndex: 'urunbook',
			filterable: true,
			flex: 2
		}];
		this.set('filterColumns', filterColumns.concat(this.additionalColumns).concat(RS.common.grid.getSysColumns()));
	},

	initFilterColumns: function() {
		this.concatColumns();
		Ext.each(this.filterColumns, function(f) {
			Ext.each(this.getStoreFields(), function(sf) {
				if (f.dataIndex == sf.name)
					switch (sf.type) {
						case 'date':
							f.renderer = Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat());
							break;
						case 'bool':
							f.renderer = RS.common.grid.booleanRenderer()
							break;
					}
			}, this);
		}, this);
	},

	initDeployedFilterColumns: function() {
		Ext.each(this.deployedColumns, function(f) {
			Ext.each(this.getStoreFields(), function(sf) {
				if (f.dataIndex == sf.name)
					switch (sf.type) {
						case 'date':
							f.renderer = (function(self) {
								return function(value, row, record) {
									var v = Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())(value);
									//v = self.renderer(v, row, record);
									return v;
								}
							})(this);
							break;
						case 'bool':
							f.renderer = (function(self) {
								return function(value, row, record) {
									var v = RS.common.grid.booleanRenderer()(value);
									//v = self.renderer(v, row, record);
									return v;
								}
							})(this);
							break;
					}
			}, this);
		}, this);
	},

	loadFilters: function() {
		// this.set('reqCount',this.reqCount + 1);
		this.filters.load();
	},

	createFilter: function() {
		var config = {	modelName: this.filterViewName };
		if(this.SDK == true){
			Ext.apply(config,{
				params : {
					name : this.gatewaytype
				}
			})
		};
		clientVM.handleNavigation(config);
	},

	deleteFilters: function() {
		this.message({
			title: this.localize('deleteFiltersTitle'),
			msg: this.localize(this.filtersSelections.length > 1 ? 'deleteFilterRecs' : 'deleteFilterRec', {
				num: this.allSelected ? this.filters.getTotalCount() : this.filtersSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirmDeleteFilters'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.sendDeleteFiltersReq
		});
	},

	createFilterIsEnabled$: function() {
		return this.reqCount == 0;
	},

	whyUCannotDeleteIt: null,
	/*deleteFiltersIsEnabled$: function() {
		var uniqueIds = [];
		Ext.each(this.filtersSelections, function(f) {
			if (-1 != this.deployedFilters.find(this.uniqueIdField, f.get(this.uniqueIdField)))
				uniqueIds.push(f.get(this.uniqueIdField));
		}, this);
		if (uniqueIds.length > 0)
			this.set('whyUCannotDeleteIt', this.localize('whyUCannotDeleteIt', {
				names: uniqueIds
			}));
		else
			this.set('whyUCannotDeleteIt', null);
		return this.reqCount == 0 && this.filtersSelections.length > 0 && !this.whyUCannotDeleteIt;
	},*/

	sendDeleteFiltersReq: function(btn) {
		if (btn === 'no')
			return;
		var str = [];
		this.set('reqCount', this.reqCount + 1);
		Ext.each(this.filtersSelections, function(r) {
			str.push(r.get('id'));
		});
		var params = {};
		Ext.apply(params, this.requestParams);
		params.ids = str;
		params.all = this.allSelected;
		this.ajax({
			url: '/resolve/service/gateway/deleteFilters',
			params: params,
			scope: this,
			success: function(resp) {
				this.handleDeleteFiltersSuccess(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
		});
	},

	handleDeleteFiltersSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(respData.message);
		else
			clientVM.displaySuccess(this.localize('DeleteFilterSucMsg'));
		this.loadFilters();
		this.loadDeployedFilters();
	},

	editFilter: function(id) {
		var config = {	modelName: this.filterViewName };
		if(this.SDK == true){
			Ext.apply(config,{
				params : {
					name : this.gatewaytype,
					id : id
				}
			})
		}
		else
		{
			Ext.apply(config,{
				params: {
					gatewaytype: this.gatewaytype,
					id: id
				}
			})
		}
		clientVM.handleNavigation(config);
	},
	/*------------------------------------------------gateway impl start-------------------------------*/
	loadGateways: function() {
		this.set('reqCount', this.reqCount + 1);
		this.ajax({
			url: '/resolve/service/gateway/listGateways',
			method: 'GET',
			params: this.requestParams,
			success: function(resp) {
				this.handleLoadGatewaysSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
		});
		this.deployedFilters.removeAll();
		this.set('deployedFiltersSelections', []);
	},

	handleLoadGatewaysSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('LoadGatewaysErrMsg') + '[' + respData.message + ']');
			return;
		}
		var queues = respData.records;
		this.gatewaysStore.loadData([], false);
		for (var idx = 0; idx < queues.length; idx++) {
			this.gatewaysStore.add({
				uqueue: queues[idx]
			});
		}

		if (queues.length > 0)
			this.set('gateway', queues[0]);
	},

	loadDeployedFilters: function() {
		if (!this.gateway)
			return;
		this.set('reqCount', this.reqCount + 1);
		this.deployedFilters.load({
			scope: this,
			callback: function(records, op, suc) {
				this.set('reqCount', this.reqCount - 1);
				if (!suc) {
					//clientVM.displayError(this.localize('ListDeployedFiltersErrMsg'));
					return;
				}
			}
		});
	},

	// updateDeployedFilters:function(){
	// 	this.message({
	// 		title:this.localize('updateTitle'),
	// 		msg:this.localize('updateMsg'),
	// 		buttons:Ext.MessageBox.YESNO,
	// 		buttonText:{
	// 			yes:this.localize('confirmUpdate'),
	// 			no:this.localize('cancel')
	// 		},
	// 		scope:this,
	// 		fn:this.sendUpdateFiltersReq
	// 	})
	// },

	// sendUpdateFiltersReq:function(btn){
	// 	if(btn==='no')return;

	// 	this.set('reqCount',this.reqCount + 1);
	// 	var ids = [];
	// 	for(idx in this.deployedFiltersSelections){
	// 		ids.push(this.deployedFiltersSelections[idx].data.id);
	// 	}

	// 	this.ajax({
	// 		url:'/resolve/service/gateway/updateDeployedFilters',
	// 		jsonData:ids,
	// 		success:function(resp){
	// 			this.set('reqCount',this.reqCount - 1);
	// 			this.handleUpdateReqSuccess(resp);
	// 		},
	// 		failure:function(resp){
	// 			this.set('reqCount',this.reqCount - 1);
	// 			clientVM.displayFailure(resp);
	// 		}
	// 	});
	// },

	handleUpdateReqSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('updateFailureMsg') + '[' + respData.message + ']');
			return;
		}
		this.loadDeployedFilters();
	},

	undeployFilters: function() {
		this.message({
			title: this.localize('UndeployFilters'),
			msg: this.localize(this.deployedFiltersSelections.length > 0 ? 'UndeployFiltersMsg' : 'UndeployFilterMsg', {
				num: this.deployedFiltersSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirmUndeploy'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.sendUndeployFiltersReq
		})
	},

	sendUndeployFiltersReq: function(btn) {
		if (btn === 'no')
			return;

		var names = [];

		for (var idx = 0; this.deployedFiltersSelections.length; idx++)
			names.push(this.deployedFiltersSelections[idx].get(this.uniqueIdField));

		var params = {};
		Ext.apply(params, this.requestParams);
		params.filterNames = names;

		this.set('reqCount', this.reqCount + 1);
		this.ajax({
			url: '/resolve/service/gateway/undeployFilters',
			params: params,
			scope: this,
			success: function(resp) {
				this.handleUndeployFiltersSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
		})
	},

	handleUndeployFiltersSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(this.localize('UndeployFilterErrMsg') + '[' + respData.message + ']');
		else
			clientVM.displaySuccess(this.localize('UndeployFilterSucMsg'));
		this.loadDeployedFilters();
	},

	showFilterList: function() {
		this.open({
			mtype: 'RS.gateway.Filters'
		});
	},

	addFiltersToGateway: function(data, dropRec, dropPosition, dropHandlers) {
		if (this.reqCount > 0) {
			dropHandlers.cancelDrop();
			return;
		}

		if (!this.gateway) {
			clientVM.displayError(this.localize('NullGatewayError'));
			dropHandlers.cancelDrop();
			return;
		}

		var records = this.duplicationFilter(data.records);
		data.records = records;
	},

	addFilterFromWindow: function(records) {
		if (this.reqCount > 0)
			return;

		if (!this.gateway) {
			clientVM.displayError(this.localize('NullGatewayError'));
			return;
		}
		records = this.duplicationFilter(records);
		this.deployedFilters.add(records);
	},

	duplicationFilter: function(records) {
		var result = [];
		Ext.each(records, function(r, index) {
			var dup = false;
			this.deployedFilters.each(function(sr) {
				if (sr.get(this.uniqueIdField) === r.get(this.uniqueIdField))
					dup = true;
			}, this);
			if (!dup) {
				r.set('isMaster', false);
				result.push(r);
			}

		}, this);
		return result;
	},

	handleAddFiltersToGatewaySuccessReq: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('AddFiltersToGatewayErrMsg') + '[' + respData.message + ']');
			this.loadDeployedFilters();
			return;
		}

	},

	deployFilters: function() {
		this.message({
			title: this.localize('DeployTitle'),
			msg: this.localize('DeploysMsg'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirmDeploy'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.sendDeployFiltersReq
		});
	},

	sendDeployFiltersReq: function(btn) {
		if (btn === 'no')
			return;

		var names = null;
		var deleteFilterNames = null;
		this.deployedFilters.each(function(r) {
			if (r.get('isDeleted')) {
				if (!deleteFilterNames)
					deleteFilterNames = [];
				deleteFilterNames.push(r.get(this.uniqueIdField));
			} else {
				if (!names)
					names = [];
				names.push(r.get(this.uniqueIdField));
			}

		}, this);
		var p = {
			filterNames: names,
			deleteFilterNames: deleteFilterNames
		};
		Ext.applyIf(p, this.requestParams);
		p.queueName = this.gateway || p.queueName;
		this.set('reqCount', this.reqCount + 1);
		this.ajax({
			url: '/resolve/service/gateway/deployFilters',
			params: p,
			scope: this,
			success: function(resp) {
				this.handleDeploySucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
		});
	},

	handleDeploySucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(this.localize('DeployErrMsg') + '[' + respData.message + ']');
		else
			clientVM.displaySuccess(this.localize('DeploySucMsg'));
		var records = respData.records;
		//this.set('filtersSelections', []); commented out because UI does not deselect an item after it is successfully deployed
		this.deployedFilters.removeAll();
		if (records != null)
			this.deployedFilters.add(records);
	},

	markDeletedDeployedFilters: function() {
		Ext.each(this.deployedFiltersSelections, function(r) {
			if (r.get('uqueue') === '<|Default|Queue|>')
				this.deployedFilters.remove(r);
			else
				r.set('isDeleted', true);
		}, this);
	},

	markDeletedDeployedFiltersIsEnabled$: function() {
		return this.reqCount == 0 && this.deployedFiltersSelections.length > 0;
	},

	gatewayIsEnabled$: function() {
		return this.reqCount == 0;
	},

	showFilterListIsEnabled$: function() {
		return this.reqCount == 0 && this.gateway != null;
	},

	undeployFiltersIsEnabled$: function() {
		return this.reqCount == 0 && this.gateway != null && this.deployedFiltersSelections.length > 0;
	},

	numOfDeployedFilters: 0,

	deployFiltersIsEnabled$: function() {
		return this.reqCount == 0 && this.gateway != null && this.numOfDeployedFilters > 0;
	},

	when_gateway_changed: {
		on: ['gatewayChanged'],
		action: function() {
			this.loadDeployedFilters();
		}
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},

	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	}
});
glu.defModel('RS.gateway.NetcoolFilter', {
	usql: '',
	modelName: 'NetcoolFilter',
	gatewaytype: 'netcool',
	mixins: ['BaseFilter'],
	fields: RS.gateway.commonFields.concat(['usql']),
	// usqlIsValid$:function(){
	// 	return this.usql&&this.usql!=''?true:this.localize('invalidSql');
	// },

	init: function() {
		this.initBase();
		this.set('title', this.localize('netcoolFilterTitle'));
	}
});
RS.gateway.netcool = {
	modelName: 'netcool',
	filterViewName: 'RS.gateway.NetcoolFilter',
	requestParams: {
		modelName: 'NetcoolFilter',
		queueName: 'NETCOOL',
		gatewayName: 'NETCOOL',
		gatewayClass: 'MNetcool',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~usql~~',
		dataIndex: 'usql',
		filterable: true,
		flex: 1,
		renderer: this.renderer
	}],

	additionalStoreFields: [{
		name: 'usql',
		type: 'string'
	}, ],
	config: function() {
		this.set('filterDisplayName', this.localize('netcoolFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('netcoolGatewayDisplayName'));
		this.set('title', this.localize('netcoolTitle'));
	}
};

glu.defModel('RS.gateway.NetcoolMain', {
	mixins: ['Main'],
	gatewaytype: 'netcool'
});
glu.defModel('RS.gateway.RemedyxFilter', {
	uformName: '',
	uquery: '',
	ulastValueField: '',
	ignoreName: true,
	modelName: 'RemedyxFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'remedyx',
	fields: RS.gateway.commonFields.concat(['uformName', 'uquery', 'ulastValueField', 'ulimit']),
	// uqueryIsValid$:function(){
	// 	return this.uquery&&this.uquery!=''?true:this.localize('invalidQuery');
	// },

	init: function() {
		this.initBase();
		this.set('title', this.localize('remedyxFilterTitle'));
	}
});
glu.defModel('RS.gateway.RemedyxForm', {
	uniqueIdField: 'uname',
	uname: '',
	ignoreName: true,
	ufieldList: '',
	modelName: 'RemedyxForm',
	mixins: ['BaseFilter'],
	gatewaytype: 'remedyxForm',
	fields: ['uname', 'ufieldList'],
	// uqueryIsValid$:function(){
	// 	return this.uquery&&this.uquery!=''?true:this.localize('invalidQuery');
	// },

	init: function() {
		this.initBase();
		this.set('title', this.localize('remedyxFormTitle'));
		this.activate(screen, {
			id: this.id,
			uniqueIdField: this.uniqueIdField
		});
	},
	userDateFormat$: function() {},
	handleSaveSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(this.localize('SaveErrMsg') + '[' + respData.message + ']');
		else {
			clientVM.displaySuccess(this.localize('SaveSucMsg'));
			this.cancel();
		}
		this.parentVM.loadFilters();
		this.parentVM.loadDeployedFilters();
	},

	respErr: function(resp) {
		clientVM.displayFailure(resp);
	},
	cancel: function() {
		this.doClose();
	}
});
RS.gateway.remedyxForm = {
	modelName: 'remedyxForm',
	filterViewName: 'RS.gateway.RemedyxForm',
	requestParams: {
		modelName: 'RemedyxForm',
		queueName: 'REMEDYX',
		gatewayName: 'REMEDYX',
		gatewayClass: 'MRemedyx',
		getterMethod: 'getForms',
		clearAndSetMethod: 'clearAndSetForms',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},

	getStoreFields: function() {
		if (!this.gatewaytype)
			alert('Need to specify a gatewaytype!');
		var fields = [
			'id',
			'uname',
			'ufieldList'
		].concat(RS.common.grid.getSysFields());
		return fields;
	},
	concatColumns: function() {
		this.set('filterColumns', [{
			header: '~~uname~~',
			dataIndex: 'uname',
			filterable: true,
			flex: 1
		}, {
			header: '~~ufieldList~~',
			dataIndex: 'ufieldList',
			filterable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()));
	},
	config: function() {
		this.set('filterDisplayName', this.localize('remedyxFormDisplayName'));
		this.set('title', this.localize('remedyxFormTitle'));
	},
	createFilter: function() {
		this.open({
			mtype: this.filterViewName
		});
	},
	editFilter: function(id) {
		this.open({
			mtype: this.filterViewName,
			gatewaytype: this.gatewaytype,
			id: id
		});
	}
};

glu.defModel('RS.gateway.RemedyxFormMain', {
	mixins: ['Main'],
	gatewaytype: 'remedyxForm'
});
RS.gateway.remedyx = {
	modelName: 'remedyx',
	filterViewName: 'RS.gateway.RemedyxFilter',
	requestParams: {
		modelName: 'RemedyxFilter',
		queueName: 'REMEDYX',
		gatewayName: 'REMEDYX',
		gatewayClass: 'MRemedyx',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uformName~~',
		dataIndex: 'uformName',
		filterable: true,
		flex: 1
	}, {
		header: '~~uquery~~',
		dataIndex: 'uquery',
		filterable: true,
		flex: 1
	}, {
		header: '~~ulimit~~',
		dataIndex: 'ulimit',
		filterable: true,
		flex: 1
	}, {
		header: '~~ulastValueField~~',
		dataIndex: 'ulastValueField',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'uformName',
		type: 'string'
	}, {
		name: 'uquery',
		type: 'string'
	}, {
		name: 'ulastValueField',
		type: 'string'
	}, {
		name: 'ulimit',
		type: 'integer'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('remedyxFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('remedyxGatewayDisplayName'));
		this.set('title', this.localize('remedyxTitle'));
	}
};

glu.defModel('RS.gateway.RemedyxMain', {
	mixins: ['Main'],
	gatewaytype: 'remedyx'
});
glu.defModel('RS.gateway.SalesforceFilter', {
	uquery: '',
	modelName: 'SalesforceFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'salesforce',
	fields: RS.gateway.commonFields.concat(['uobject', 'uquery']),
	objectStore: {
		mtype: 'store',
		fields: ['uobject'],
		proxy: {
			type: 'memory'
		}
	},
	activate: function(screen, params) {
		this.activateBase(params);
		this.loadObjects();
	},
	init: function() {
		this.initBase();
		this.set('title', this.localize('salesforceFilterTitle'));
	},

	loadObjects: function() {
		this.ajax({
			url: '/resolve/service/gateway/listObjectNames',
			params: {
				gatewayName: 'SALESFORCE'
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('LoadObjErr') + '[' + respData.message + ']');
					return;
				}
				var objs = respData.records || {};
				this.objectStore.removeAll();
				Ext.each(objs, function(o) {
					this.objectStore.add({
						uobject: o
					});
				}, this);
				if (!this.uobject)
					this.set('uobject', this.objectStore.first().get('uobject'));
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	}
});
RS.gateway.salesforce = {
	modelName: 'salesforce',
	filterViewName: 'RS.gateway.SalesforceFilter',
	requestParams: {
		modelName: 'SalesforceFilter',
		queueName: 'SALESFORCE',
		gatewayName: 'SALESFORCE',
		gatewayClass: 'MSalesforce',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uobject~~',
		dataIndex: 'uobject',
		filterable: true,
		flex: 1
	}, {
		header: '~~uquery~~',
		dataIndex: 'uquery',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'uobject',
		type: 'string'
	}, {
		name: 'uquery',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('salesforceFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('salesforceGatewayDisplayName'));
		this.set('title', this.localize('salesforceTitle'));
	}
};

glu.defModel('RS.gateway.SalesforceMain', {
	mixins: ['Main'],
	gatewaytype: 'salesforce'
});
glu.defModel('RS.gateway.SaveAs', {
	name: '',
	nameIsValid$: function() {
		var me = this;
		var obj = {
			localize: function(text) {
				return me.parentVM.localize(text);
			}
		};
		obj[this.uniqueIdField] = this.name;
		var valid = this.parentVM[this.uniqueIdField + 'IsValid$'].apply(obj);
		return valid;
	},
	label: '',
	uniqueIdField: '',
	title: '',
	msg: '',
	dumper: null,
	init: function() {
		this.set('msg', this.localize('saveAsNewFilterMsg', {
			name: this.localize(this.uniqueIdField)
		}));
		this.set('label', this.localize(this.uniqueIdField));
		this.set('title', this.localize('saveAsNewFilterTitle'));
	},
	save: function() {
		this.dumper();
		this.close();
	},
	close: function() {
		this.doClose();
	}
});
glu.defModel('RS.gateway.ServiceNowFilter', {
	uquery: '',
	modelName: 'ServiceNowFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'serviceNow',
	fields: RS.gateway.commonFields.concat(['uobject', 'uquery']),
	// uqueryIsValid$:function(){
	// 	return this.uquery&&this.uquery!=''?true:this.localize('invalidQuery');
	// },

	init: function() {
		this.initBase();
		this.set('title', this.localize('serviceNowFilterTitle'));
	}
});
RS.gateway.serviceNow = {
	modelName: 'serviceNow',
	filterViewName: 'RS.gateway.ServiceNowFilter',
	requestParams: {
		modelName: 'ServiceNowFilter',
		queueName: 'SERVICENOW',
		gatewayName: 'SERVICENOW',
		gatewayClass: 'MServiceNow',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uobject~~',
		dataIndex: 'uobject',
		filterable: true,
		flex: 1
	}, {
		header: '~~uquery~~',
		dataIndex: 'uquery',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'uobject',
		type: 'string'
	}, {
		name: 'uquery',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('serviceNowFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('serviceNowGatewayDisplayName'));
		this.set('title', this.localize('serviceNowTitle'));
	}
};

glu.defModel('RS.gateway.ServiceNowMain', {
	mixins: ['Main'],
	gatewaytype: 'serviceNow'
});
glu.defModel('RS.gateway.SNMPFilter', {
	utrapReceiver: '',
	usnmpTrapOid: '',
	usnmpVersion: '',
	uipAddresses: '',
	ureadCommunity: '',
	utimeout: '',
	uretries: '',
	uregex: '',
	uoid: '',
	ucomparator: '',
	uvalue: '',

	modelName: 'SNMPFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'snmp',
	fields: RS.gateway.commonFields.concat([{
			name: 'utrapReceiver',
			type: 'bool'
		},
		'usnmpTrapOid',
		'usnmpVersion',
		'uipAddresses',
		'ureadCommunity',
		'utimeout',
		'uretries',
		'uregex',
		'uoid',
		'ucomparator',
		'uvalue'
	]),

	init: function() {
		this.initBase();
		this.set('title', this.localize('snmpFilterTitle'));
	},
	usnmpVersionIsValid$: function() {
		var test = /^[0-9]*$/;
		return test.test(this.usnmpVersion) ? true : this.localize('invalidVersion');
	},

	utimeoutIsValid$: function() {
		var test = /^[0-9]*$/;
		return test.test(this.utimeout) ? true : this.localize('invalidTimeout');
	},

	uretriesIsValid$: function() {
		var test = /^[0-9]*$/;
		return test.test(this.uretries) ? true : this.localize('invalidTimeout');
	},

	uvalueIsValid$: function() {
		var test = /^[0-9]*$/;
		return test.test(this.uvalue) ? true : this.localize('invalidTimeout');
	}
});
RS.gateway.snmp = {
	modelName: 'snmp',
	filterViewName: 'RS.gateway.SNMPFilter',
	requestParams: {
		modelName: 'SNMPFilter',
		queueName: 'SNMP',
		gatewayName: 'SNMP',
		gatewayClass: 'MSNMP',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~utrapReceiver~~',
		dataIndex: 'utrapReceiver',
		filterable: true,
		flex: 1
	}, {
		header: '~~usnmpTrapOid~~',
		dataIndex: 'usnmpTrapOid',
		filterable: true,
		flex: 1
	}, {
		header: '~~usnmpVersion~~',
		dataIndex: 'usnmpVersion',
		filterable: true,
		flex: 1
	}, {
		header: '~~uipAddresses~~',
		dataIndex: 'uipAddresses',
		filterable: true,
		flex: 1
	}, {
		header: '~~ureadCommunity~~',
		dataIndex: 'ureadCommunity',
		filterable: true,
		flex: 1
	}, {
		header: '~~utimeout~~',
		dataIndex: 'utimeout',
		filterable: true,
		flex: 1
	}, {
		header: '~~uretries~~',
		dataIndex: 'uretries',
		filterable: true,
		flex: 1
	}, {
		header: '~~uoid~~',
		dataIndex: 'uoid',
		filterable: true,
		flex: 1
	}, {
		header: '~~uregex~~',
		dataIndex: 'uregex',
		filterable: true,
		flex: 1
	}, {
		header: '~~ucomparator~~',
		dataIndex: 'ucomparator',
		filterable: true,
		flex: 1
	}, {
		header: '~~uvalue~~',
		dataIndex: 'uvalue',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'utrapReceiver',
		type: 'bool'
	}, {
		name: 'usnmpTrapOid',
		type: 'string'
	}, {
		name: 'usnmpVersion',
		type: 'integer'
	}, {
		name: 'uipAddress',
		type: 'string'
	}, {
		name: 'ureadCommunity',
		type: 'string'
	}, {
		name: 'utimeout',
		type: 'long'
	}, {
		name: 'uretries',
		type: 'long'
	}, {
		name: 'uoid',
		type: 'string'
	}, {
		name: 'uregex',
		type: 'string'
	}, {
		name: 'ucomparator',
		type: 'string'
	}, {
		name: 'uvalue',
		type: 'long'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('snmpFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('snmpGatewayDisplayName'));
		this.set('title', this.localize('snmpTitle'));
	}
};

glu.defModel('RS.gateway.SNMPMain', {
	mixins: ['Main'],
	gatewaytype: 'snmp'
});
glu.defModel('RS.gateway.SSHPool', {
	ignoreName: true,
	usubnetMask: '',
	utimeout: '',
	umaxConn: '',
	modelName: 'SSHPool',
	mixins: ['BaseFilter'],
	gatewaytype: 'databaseConnectionPool',
	fields: ['upoolName', 'usubnetMask', 'utimeout', 'umaxConn'],
	driverStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	init: function() {
		this.initBase();
		this.set('title', this.localize('sshPoolTitle'));
		this.activate(screen, {
			id: this.id,
			uniqueIdField: this.uniqueIdField
		});
	},
	userDateFormat$: function() {},
	usubnetMaskIsValid$: function() {
	var test = /^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}(\/[0-9]{1,2})*$/;
		return test.test(this.usubnetMask) ? true : this.localize('invalidSubnetMask');
	},
	umaxConnIsValid$: function() {
		var test = /^[0-9]*$/;
		return test.test(this.umaxConn) ? true : this.localize('invalidMaxConn');
	},
	utimeoutIsValid$: function() {
		var test = /^[0-9]*$/;
		return test.test(this.utimeout) ? true : this.localize('invalidMaxConn');
	},
	handleSaveSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(this.localize('SaveErrMsg') + '[' + respData.message + ']');
		else {
			clientVM.displaySuccess(this.localize('SaveSucMsg'));
			this.cancel();
		}
		this.parentVM.loadFilters();
		this.parentVM.loadDeployedFilters();
	},

	respErr: function(resp) {
		clientVM.displayFailure(resp);
	},
	cancel: function() {
		this.doClose();
	}
});
RS.gateway.sshPool = {
	modelName: 'SSHPool',
	uniqueIdField: 'usubnetMask',
	filterViewName: 'RS.gateway.SSHPool',
	requestParams: {
		modelName: 'SSHPool',
		queueName: 'SSH',
		gatewayName: 'SSH',
		gatewayClass: 'MSSH',
		getterMethod: 'getPools',
		clearAndSetMethod: 'clearAndSetPools',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},

	concatColumns: function() {
		this.set('filterColumns', [{
			header: '~~usubnetMask~~',
			dataIndex: 'usubnetMask',
			filterable: true,
			flex: 1
		}, {
			header: '~~umaxConn~~',
			dataIndex: 'umaxConn',
			filterable: true,
			flex: 1
		}, {
			header: '~~utimeout~~',
			dataIndex: 'utimeout',
			filterable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()));
	},

	config: function() {
		this.set('filterDisplayName', this.localize('sshPoolDisplayName'));
		this.set('title', this.localize('sshPoolTitle'));
	},

	getStoreFields: function() {
		if (!this.gatewaytype)
			alert('Need to specify a gatewaytype!');
		var fields = [
			'id',
			'usubnetMask',
			'umaxConn',
			'utimeout'
		].concat(RS.common.grid.getSysFields());
		return fields;
	},

	createFilter: function() {
		this.open({
			mtype: this.filterViewName
		});
	},
	editFilter: function(id) {
		this.open({
			mtype: this.filterViewName,
			uniqueIdField: this.uniqueIdField,
			gatewaytype: this.gatewaytype,
			id: id
		});
	}
};

glu.defModel('RS.gateway.SSHPoolMain', {
	mixins: ['Main'],
	gatewaytype: 'sshPool'
});
glu.defModel('RS.gateway.TCPFilter', {
	uport: '',
	modelName: 'TCPFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'tcp',
	fields: RS.gateway.commonFields.concat(['uport', 'ubeginSeparator', 'uendSeparator',
	    {
            name: 'uincludeBeginSeparator',
            type: 'bool'
        },{
            name: 'uincludeEndSeparator',
            type: 'bool'
        }]),
	init: function() {
		this.initBase();
		this.set('title', this.localize('tcpFilterTitle'));
	}
});
RS.gateway.tcp = {
	modelName: 'tcp',
	filterViewName: 'RS.gateway.TCPFilter',
	requestParams: {
		modelName: 'TCPFilter',
		queueName: 'TCP',
		gatewayName: 'TCP',
		gatewayClass: 'MTCP',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uport~~',
		dataIndex: 'uport',
		filterable: true,
		flex: 1
	},{
        header: '~~ubeginSeparator~~',
        dataIndex: 'ubeginSeparator',
        filterable: true,
        renderer: RS.common.grid.plainRenderer(),
        flex: 1
    },{
        header: '~~uincludeBeginSeparator~~',
        dataIndex: 'uincludeBeginSeparator',
		renderer: RS.common.grid.booleanRenderer(),
        filterable: true,
        flex: 1
    },{
        header: '~~uendSeparator~~',
        dataIndex: 'uendSeparator',
        filterable: true,
        renderer: RS.common.grid.plainRenderer(),
        flex: 1
    },{
        header: '~~uincludeEndSeparator~~',
        dataIndex: 'uincludeEndSeparator',
		renderer: RS.common.grid.booleanRenderer(),
        filterable: true,
        flex: 1
    }],

	additionalStoreFields: [{
		name: 'uport',
		type: 'string'
	},{
        name: 'ubeginSeparator',
        type: 'string'
    },{
        name: 'uincludeBeginSeparator',
        type: 'string'
    },{
        name: 'uendSeparator',
        type: 'string'
    },{
        name: 'uincludeEndSeparator',
        type: 'string'
    }],
	config: function() {
		this.set('filterDisplayName', this.localize('tcpFilterDisplayName'));
		this.set('title', this.localize('tcpTitle'));
	}
};

glu.defModel('RS.gateway.TCPMain', {
	mixins: ['Main'],
	gatewaytype: 'tcp'
});
glu.defModel('RS.gateway.TelnetPool', {
	ignoreName: true,
	usubnetMask: '',
	utimeout: '',
	umaxConn: '',
	modelName: 'TelnetPool',
	mixins: ['BaseFilter'],
	gatewaytype: 'databaseConnectionPool',
	fields: ['upoolName', 'usubnetMask', 'utimeout', 'umaxConn'],
	driverStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	init: function() {
		this.initBase();
		this.set('title', this.localize('telnetPoolTitle'));
		this.activate(screen, {
			id: this.id,
			uniqueIdField: this.uniqueIdField
		});
	},
	userDateFormat$: function() {},
	usubnetMaskIsValid$: function() {
		var test = /^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/;
		return test.test(this.usubnetMask) ? true : this.localize('invalidSubnetMask');
	},
	umaxConnIsValid$: function() {
		var test = /^[0-9]*$/;
		return test.test(this.umaxConn) ? true : this.localize('invalidMaxConn');
	},
	utimeoutIsValid$: function() {
		var test = /^[0-9]*$/;
		return test.test(this.utimeout) ? true : this.localize('invalidMaxConn');
	},
	handleSaveSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(this.localize('SaveErrMsg') + '[' + respData.message + ']');
		else {
			clientVM.displaySuccess(this.localize('SaveSucMsg'));
			this.cancel();
		}
		this.parentVM.loadFilters();
		this.parentVM.loadDeployedFilters();
	},

	respErr: function(resp) {
		clientVM.displayFailure(resp);
	},
	cancel: function() {
		this.doClose();
	}
});
RS.gateway.telnetPool = {
	modelName: 'telnetPool',
	uniqueIdField: 'usubnetMask',
	filterViewName: 'RS.gateway.TelnetPool',
	requestParams: {
		modelName: 'TelnetPool',
		queueName: 'TELNET',
		gatewayName: 'TELNET',
		gatewayClass: 'MTelnet',
		getterMethod: 'getPools',
		clearAndSetMethod: 'clearAndSetPools',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},

	concatColumns: function() {
		this.set('filterColumns', [{
			header: '~~usubnetMask~~',
			dataIndex: 'usubnetMask',
			filterable: true,
			flex: 1
		}, {
			header: '~~umaxConn~~',
			dataIndex: 'umaxConn',
			filterable: true,
			flex: 1
		}, {
			header: '~~utimeout~~',
			dataIndex: 'utimeout',
			filterable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()));
	},

	config: function() {
		this.set('filterDisplayName', this.localize('telnetPoolDisplayName'));
		this.set('title', this.localize('telnetPoolTitle'));
	},

	getStoreFields: function() {
		if (!this.gatewaytype)
			alert('Need to specify a gatewaytype!');
		var fields = [
			'id',
			'usubnetMask',
			'umaxConn',
			'utimeout'
		].concat(RS.common.grid.getSysFields());
		return fields;
	},

	createFilter: function() {
		this.open({
			mtype: this.filterViewName
		});
	},
	editFilter: function(id) {
		this.open({
			mtype: this.filterViewName,
			uniqueIdField: this.uniqueIdField,
			gatewaytype: this.gatewaytype,
			id: id
		});
	}
};

glu.defModel('RS.gateway.TelnetPoolMain', {
	mixins: ['Main'],
	gatewaytype: 'telnetPool'
});
glu.defModel('RS.gateway.TIBCOBespokeFilter', {
	uregex: '',
	modelName: 'TIBCOBespokeFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'tibcoBespoke',
	fields: RS.gateway.commonFields.concat(['uregex','ubusUri','utopic','ucertifiedMessaging','uxmlQuery','uunescapeXml','uprocessAsXml','urequireReply','ureplyTimeout']),

	defaultData:
	{
		uprocessAsXml: true,
		ureplyTimeout: '60000'
	},

	init: function() {
		this.initBase();
		this.set('title', this.localize('tibcoBespokeFilterTitle'));
	}
});
RS.gateway.tibcoBespoke = {
	modelName: 'tibcoBespoke',
	filterViewName: 'RS.gateway.TIBCOBespokeFilter',
	requestParams: {
		modelName: 'TIBCOBespokeFilter',
		queueName: 'TIBCOBESPOKE',
		gatewayName: 'TIBCOBESPOKE',
		gatewayClass: 'MTIBCOBespoke',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uregex~~',
		dataIndex: 'uregex',
		filterable: true,
		hidden: true,
		flex: 1
	},{
		header: '~~ubusUri~~',
		dataIndex: 'ubusUri',
		filterable: true,
		flex: 1
	},{
		header: '~~utopic~~',
		dataIndex: 'utopic',
		filterable: true,
		flex: 1
	},{
		header: '~~ucertifiedMessaging~~',
		dataIndex: 'ucertifiedMessaging',
		filterable: true,
		flex: 1
	},{
		header: '~~uunescapeXml~~',
		dataIndex: 'uunescapeXml',
		filterable: true,
		hidden: true,
		flex: 1
	},{
		header: '~~uprocessAsXml~~',
		dataIndex: 'uprocessAsXml',
		filterable: true,
		hidden: true,
		flex: 1
	},{
		header: '~~urequireReply~~',
		dataIndex: 'urequireReply',
		filterable: true,
		flex: 1
	},{
		header: '~~ureplyTimeout~~',
		dataIndex: 'ureplyTimeout',
		hidden: true,
		filterable: true,
		flex: 1
	},{
		header: '~~uxmlQuery~~',
		dataIndex: 'uxmlQuery',
		filterable: true,
		flex: 1
	}],
	additionalStoreFields: [{
		name: 'uxmlQuery',
		type: 'string'
	},{
		name: 'uregex',
		type: 'string'
	},{
		name: 'ubusUri',
		type: 'string',
	},{
		name: 'utopic',
		type: 'string'
	},{
		name: 'ucertifiedMessaging',
		type: 'boolean'
	},{
		name: 'uunescapeXml',
		type: 'boolean'
	},{
		name: 'uprocessAsXml',
		type: 'boolean'
	},{
		name: 'urequireReply',
		type: 'boolean'
	},{
		name: 'ureplyTimeout',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('tibcoBespokeFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('tibcoBespokeGatewayDisplayName'));
		this.set('title', this.localize('tibcoBespokeTitle'));
	}
};

glu.defModel('RS.gateway.TIBCOBespokeMain', {
	mixins: ['Main'],
	gatewaytype: 'tibcoBespoke'
});
glu.defModel('RS.gateway.TSRMFilter', {
	uobject: '',
	uquery: '',
	modelName: 'TSRMFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'tsrm',
	fields: RS.gateway.commonFields.concat(['uquery', 'uobject']),
	objectStore: {
		mtype: 'store',
		fields: ['uobject'],
		proxy: {
			type: 'memory'
		}
	},
	activate: function(screen, params) {
		this.activateBase(params);
		this.loadObjects();
	},
	init: function() {
		this.initBase();
		this.set('title', this.localize('tsrmFilterTitle'));
	},

	loadObjects: function() {
		this.ajax({
			url: '/resolve/service/gateway/listObjectNames',
			params: {
				gatewayName: 'TSRM'
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('LoadObjErr') + '[' + respData.message + ']');
					return;
				}
				var objs = respData.records || {};
				this.objectStore.removeAll();
				Ext.each(objs, function(o) {
					this.objectStore.add({
						uobject: o
					});
				}, this);
				if (this.uobject)
					this.set('uobject', this.objectStore.first().get('uobject'));
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	}
});
RS.gateway.tsrm = {
	modelName: 'tsrm',
	filterViewName: 'RS.gateway.TSRMFilter',
	requestParams: {
		modelName: 'TSRMFilter',
		queueName: 'TSRM',
		gatewayName: 'TSRM',
		gatewayClass: 'MTSRM',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},

	additionalColumns: [{
		header: '~~uobject~~',
		dataIndex: 'uobject',
		filterable: true,
		flex: 1
	}, {
		header: '~~uquery~~',
		dataIndex: 'uquery',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'uobject',
		type: 'string'
	}, {
		name: 'uquery',
		type: 'string'
	}],

	config: function() {
		this.set('filterDisplayName', this.localize('tsrmFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('tsrmGatewayDisplayName'));
		this.set('title', this.localize('tsrmTitle'));
	}
};

glu.defModel('RS.gateway.TSRMMain', {
	mixins: ['Main'],
	gatewaytype: 'tsrm'
});
glu.defModel('RS.gateway.XMPPAddress', {
	ignoreName: true,
	uxmppaddress: '',
	uxmppaddressIsValid$: function() {
		return this.uxmppaddress ? true : this.localize('invalidAddress');
	},
	uxmpppassword: '',
	modelName: 'XMPPAddress',
	mixins: ['BaseFilter'],
	gatewaytype: 'xmppAddress',
	fields: ['uxmppaddress', 'uxmpppassword', 'uniqueId'],

	init: function() {
		this.initBase();
		this.set('title', this.localize('xmppAddressTitle'));
		this.activate(screen, {
			id: this.id,
			uniqueIdField: this.uniqueIdField
		});
	},

	userDateFormat$: function() {},

	handleSaveSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(this.localize('SaveErrMsg') + '[' + respData.message + ']');
		else {
			clientVM.displaySuccess(this.localize('SaveSucMsg'));
			this.cancel();
		}

		this.parentVM.loadFilters();
		this.parentVM.loadDeployedFilters();
	},

	respErr: function(resp) {
		clientVM.displayFailure(resp);
	},

	cancel: function() {
		this.doClose();
	}
});
RS.gateway.xmppAddress = {
	modelName: 'xmppAddress',
	uniqueIdField: 'uxmppaddress',
	filterViewName: 'RS.gateway.XMPPAddress',
	requestParams: {
		modelName: 'XMPPAddress',
		queueName: 'XMPP',
		gatewayName: 'XMPP',
		gatewayClass: 'MXMPP',
		getterMethod: 'getXMPPAddresses',
		clearAndSetMethod: 'clearAndSetXMPPAddresses',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},

	getStoreFields: function() {
		if (!this.gatewaytype)
			alert('Need to specify a gatewaytype!');
		var fields = [
			'id',
			'uxmppaddress'
		].concat(RS.common.grid.getSysFields());
		return fields;
	},
	concatColumns: function() {
		this.set('filterColumns', [{
			header: '~~uxmppaddress~~',
			dataIndex: 'uxmppaddress',
			filterable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()));
	},
	config: function() {
		this.set('filterDisplayName', this.localize('xmppAddressDisplayName'));
		this.set('title', this.localize('xmppAddressTitle'));
	},

	createFilter: function() {
		this.open({
			mtype: this.filterViewName
		});
	},
	editFilter: function(id) {
		this.open({
			mtype: this.filterViewName,
			uniqueIdField: this.uniqueIdField,
			gatewaytype: this.gatewaytype,
			id: id
		});
	}
};

glu.defModel('RS.gateway.XMPPAddressMain', {
	mixins: ['Main'],
	gatewaytype: 'xmppAddress'
});
glu.defModel('RS.gateway.XMPPFilter', {
	uregex: '',
	modelName: 'XMPPFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'xmpp',
	fields: RS.gateway.commonFields.concat(['uregex']),
	// uregexIsValid$:function(){
	// 	return this.uregex&&this.uregex!=''?true:this.localize('invalidQuery');
	// },

	init: function() {
		this.initBase();
		this.set('title', this.localize('xmppFilterTitle'));
	}
});
RS.gateway.xmpp = {
	modelName: 'xmpp',
	filterViewName: 'RS.gateway.XMPPFilter',
	requestParams: {
		modelName: 'XMPPFilter',
		queueName: 'XMPP',
		gatewayName: 'XMPP',
		gatewayClass: 'MXMPP',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uregex~~',
		dataIndex: 'uregex',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'uregex',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('xmppFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('xmppGatewayDisplayName'));
		this.set('title', this.localize('xmppTitle'));
	}
};

glu.defModel('RS.gateway.XMPPMain', {
	mixins: ['Main'],
	gatewaytype: 'xmpp'
});
glu.defView('RS.gateway.AMQPFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'textfield',
				labelWidth: 110,
				name: 'utargetQueue',
				padding: '0 10 0 0',
				flex: 1,
				fieldLabel: '~~utargetQueue~~'
			}, {
				xtype: 'checkboxfield',
				name: 'uexchange',
				boxLabel: '~~uexchange~~',
				hideLabel: true
			}]
		})
	}
);
glu.defView('RS.gateway.CASpectrumFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			bodyPadding : '10 0 0 10',
			defaults: {
				flex: 1,
				labelWidth : 130,
				margin : '4 0'
			},
			items: [
			{
				layout : {
					type : 'hbox',
					align : 'stretch'
				},
				defaults : {
					labelWidth : 130
				},
				items : [
				{
					name: 'uobject',
					flex : 1,
					xtype: 'combobox',
					labelField : 'Object',
					queryMode : 'local',
					displayField : 'name',
					store : '@{objectStore}',				
					editable: false
				},{
					name: 'uurlQuery',
					flex : 1,
					xtype : 'textfield',					
					margin : '0 0 0 10'
				}]
			},{
				name: 'uxmlQuery',
				xtype : 'textareafield',
				height : 200			
			}]
		})
	}
);
glu.defView('RS.gateway.Custom', {
	autoScroll: true,
	layout: 'fit',
	panel : '@{panel}',
	listeners:{
		render:function(){
			var me = this;
			me._vm.on('reloadingView',function(view){
				me.removeAll();
				me.doLayout();
			}, me),
			me._vm.on('updateView',function(view){
				me.add(view);
			}, me),
			me._vm.fireEvent("viewIsReady");
		}
	}
});
glu.defView("RS.gateway.CustomFilter",{
	layout: 'fit',
	panel: '@{panel}',
	listeners:{
		render:function(){
			var me = this;
			this._vm.on('updateView',function(view){
				this.removeAll();
				this.add(view);
				this.doLayout();
			}, me),
			this._vm.fireEvent("viewIsReady");
		}
	}
})




glu.defView('RS.gateway.DatabaseConnectionPool', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10px',
	defaultType: 'textfield',
	defaults: {
		labelWidth: 150
	},
	items: [{
		name: 'upoolName',
		readOnly: '@{unameIsReadOnly}'
	}, 'u_db_us_ern_ame', {
		name: 'u_db_pas_sword',
		inputType: 'password'
	}, 'udbmaxConn', {
		xtype: 'form',
		layout: 'hbox',
		defaultType: 'radiofield',
		items: [{
			xtype: 'displayfield',
			name: 'howToGetURI',
			labelWidth: 150
		}, {
			name: 'generateType',
			margin: '0px 0px 0px 20px',
			boxLabel: 'dbInfo',
			value: '@{dbInfo}'
		}, {
			name: 'generateType',
			margin: '0px 0px 0px 20px',
			boxLabel: 'custom',
			value: '@{custom}'
		}]
	}, {
		name: 'udriverClass',
		xtype: 'combobox',
		editable: false,
		queryMode: 'local',
		displayField: 'name',
		valueField: 'value',
		store: '@{driverStore}'
	}, 'udbconnectURI', {
		xtype: 'form',
		hidden: '@{custom}',
		html: '<hr/>'
	}, {
		xtype: 'form',
		hidden: '@{custom}',
		defaultType: 'textfield',
		defaults: {
			labelWidth: 150
		},
		items: [{
			name: 'dbType',
			xtype: 'combobox',
			editable: false,
			queryMode: 'local',
			displayField: 'name',
			valueField: 'value',
			store: '@{dbTypeStore}'
		}, 'ipAddress', 'port', 'dbName', 'sid', 'service', {
			xtype: 'form',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			defaultType: 'button',
			defaults: {
				margin: '0px 0px 0px 5px'
			},
			items: [{
				xtype: 'form',
				flex: 1
			}, 'generateURI']
		}]
	}],
	buttonAlign: 'left',
	buttons: ['save', 'saveAs', 'cancel'],
	asWindow: {
		modal: true,
		title: '@{title}',
		width: 700
	}
});
glu.defView('RS.gateway.DatabaseFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			// title:'~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				flex: 1,
				labelWidth: 150
			},
			items: [{
				xtype: 'container',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				defaults: {
					flex: 1
				},
				items: [{
					xtype: 'container',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					defaults: {
						labelWidth: 150
					},
					padding: '0px 10px 0px 0px',
					defaultType: 'textfield',
					items: ['ulastValueColumn', 'ulastValue']
				}, {
					xtype: 'container',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					padding: '0px 0px 0px 10px',
					defaultType: 'textfield',
					items: ['uprefix', {
						xtype: 'checkbox',
						hideLabel: true,
						boxLabel: '~~ulastValueQuote~~',
						padding: '0px 0px 0px 110px',
						name: 'ulastValueQuote'
					}]
				}]
			}, {
				xtype: 'combo',
				queryMode: 'local',
				valueField: 'upoolName',
				displayField: 'upoolName',
				name: 'upool',
				store: '@{poolStore}'
			}, {
				xtype: 'textarea',
				name: 'uupdateSql'
			}, {
				xtype: 'textarea',
				name: 'usql'
			}]
		})
	}
);
glu.defView('RS.gateway.EmailAddress', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10px',
	defaultType: 'textfield',
	defaults: {
		labelWidth: 150
	},
	items: [{
		name: 'uemailAddress',
		readOnly: '@{unameIsReadOnly}'
	}, {
		name: 'uemailPassword',
		inputType: 'password'
	}],
	buttonAlign: 'left',
	buttons: ['save', 'saveAs', 'cancel'],
	asWindow: {
		title: '@{title}',
		width: 500,
		modal: true
	}
});
glu.defView('RS.gateway.EmailFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'textfield',
				labelWidth: 150,
				name: 'uquery',
				fieldLabel: '~~uquery~~'
			}]
		})
	}
);
glu.defView('RS.gateway.EWSAddress', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10px',
	defaultType: 'textfield',
	defaults: {
		labelWidth: 150
	},
	items: ['uewsaddress', {
		name: 'uewspassword',
		fieldLabel: '~~uewsp_assword~~',
		inputType: 'password'
	}],
	buttonAlign: 'left',
	buttons: ['save', 'saveAs', 'cancel'],
	asWindow: {
		title: '@{title}',
		width: 500
	}
});
glu.defView('RS.gateway.EWSFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items:[{
				flex: 1,
				xtype: 'panel',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaults: {
					layout: {
						type: 'hbox',
						align: 'stretch'
					}
				},
				items:[{
					xtype: 'panel',
					padding: '0px 10px 0px 0px',
					items: [{
						flex: 1,
						xtype: 'textfield',
						labelWidth: 150,
						name: 'uquery',
						fieldLabel: '~~uquery~~'
					}]
				}, {
					xtype: 'panel',
					padding: '10px 10px 0px 0px',
					items: [{
						flex: 1,
						xtype: 'textfield',
						labelWidth: 150,
						name: 'ufolderNames',
						fieldLabel: '~~ufolderNames~~',
					}]
				},{ xtype: 'panel',
					padding: '10px 0px 0px 0px',
					items: [{
							flex: 1,
							xtype: 'checkbox',
							name: 'uincludeAttachment',
							padding: '0px 0px 0px 10px',
							hideLabel: true,
							boxLabel: '~~uincludeAttachment~~'
						}, {
							flex: 1,
							xtype: 'checkbox',
							name: 'uonlyNew',
							padding: '0px 0px 0px 10px',
							hideLabel: true,
							checked: true,
							boxLabel: '~~uonlyNew~~'
						}, {
							flex: 1,
							xtype: 'checkbox',
							name: 'umarkAsRead',
							padding: '0px 0px 0px 10px',
							hideLabel: true,
							checked: true,
							boxLabel: '~~umarkAsRead~~'
						}]	
				}]
			}]
		})
	}
);
glu.defView('RS.gateway.ExchangeFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaultType: 'textarea',
			defaults: {
				labelWidth: 150
			},
			items: ['usubject', 'ucontent', 'usendername', 'usenderaddress']
		})
	}
);
glu.defView('RS.gateway.Filters', {
	autoScroll: true,
	bodyPadding: '10px',
	buttonAlign: 'left',
	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'filters',
		displayName: '@{..filterDisplayName}',
		columns: '@{..filterColumns}',
		store: '@{..filters}',
		stateId: '@{..filterStateId}',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: []
		}],
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}, {
			ptype: 'columnautowidth'
		}],
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTooltip: RS.common.locale.editColumnTooltip,
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'id'
		},
		listeners: {
			editAction: function() {}
		}
	}],
	buttons: ['addFilters', 'cancel'],
	asWindow: {
		title: '@{title}',
		modal: true,
		listeners: {
			beforeshow: function() {
				this.setWidth(Math.round(Ext.getBody().getWidth() * 0.9));
				this.setHeight(Math.round(Ext.getBody().getHeight() * 0.6));
			}
		}
	}
});
glu.defView('RS.gateway.Gateways', {
	autoScroll: true,
	bodyPadding: '10px',
	buttonAlign: 'left',
	items: [{
		xtype: 'grid',
		name: 'gateways',
		columns: '@{columns}',
		store: '@{gatewayStore}'
	}],
	buttons: ['deleteGateways', 'cancel'],
	asWindow: {
		width: 500,
		height: 400,
		title: '@{title}'
	}
});
RS.gateway.buildGatewayViewConfig = function(additional) {
	return {
		autoScroll: true,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		padding: 15,
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			ui: 'display-toolbar',
			cls : 'rs-dockedtoolbar',
			defaultButtonUI: 'display-toolbar-button',
			items: [{
				xtype: 'tbtext',
				cls: 'rs-display-name',
				text: '@{title}'
			}]
		}, {
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: [{
				name: 'back',
				iconCls: 'icon-large icon-reply-all rs-icon'
			}, {
				name: 'save',
				listeners: {
					doubleClickHandler: '@{saveAndExit}',
					render: function(button) {
						button.getEl().on('dblclick', function() {
							button.fireEvent('doubleClickHandler')
						});
						clientVM.updateSaveButtons(button);
					}
				}
			}, 'saveAs', '->', {
				xtype: 'sysinfobutton',
				sysId: '@{id}',
				sysCreated: '@{sysCreatedOn}',
				sysCreatedBy: '@{sysCreatedBy}',
				sysUpdated: '@{sysUpdatedOn}',
				sysUpdatedBy: '@{sysUpdatedBy}',
				sysOrg: '@{sysOrganizationName}',
				dateFormat: '@{userDateFormat}'
			}, {
				iconCls: 'x-tbar-loading',
				handler: '@{refresh}'
			}]
		}],
		items: [{
			xtype: 'panel',
			style : 'border-top :1px solid #cccccc;',
			padding : '10 0',
			// title:'~~generalSettingTitle~~',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			defaults: {
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				margin : '0 0 0 10',
			},
			items: [{
				xtype: 'panel',			
				defaultType: 'textfield',
				flex: 1,
				defaults: {
					labelWidth: 130
				},
				items: [{
					name: 'uname',
					readOnly: '@{unameIsReadOnly}',
					maxLength: 255,
					enforceMaxLength: true
				}, {
					name: 'uorder'
				}, {
					xtype: 'checkboxfield',
					name: 'uactive',
					hideLabel: true,
					padding: '0px 0px 0px 135px',
					boxLabel: '~~uactive~~',
					fieldLabel: '~~uactive~~'
				}]
			}, {
				xtype: 'panel',			
				flex: 1,				
				defaultType: 'textfield',
				defaults: {
					labelWidth: 130
				},	
				items: [{
					name: 'uinterval'
				}, {
					name: 'urunbook'
				}, {
					name: 'ueventEventId'
				}]
			}]
		},
		additional,
		{
			xtype: 'AceEditor',
			flex: 1,
			minHeight: 300,
			width: '100%',
			name: 'uscript',
			parser: 'groovy',
			title: 'Script'
		}]
	}
}

//FOR SDK
RS.gateway.buildSDKGatewayViewConfig = function(standardViewConfig, additionalSettingViewConfig) {
	return {
		autoScroll: true,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		padding: 15,
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			ui: 'display-toolbar',
			cls : 'rs-dockedtoolbar',
			defaultButtonUI: 'display-toolbar-button',
			items: [{
				xtype: 'tbtext',
				cls: 'rs-display-name',
				text: '@{title}'
			}]
		}, {
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: [{
				name: 'back',
				iconCls: 'icon-large icon-reply-all rs-icon'
			}, {
				name: 'save',
				listeners: {
					doubleClickHandler: '@{saveAndExit}',
					render: function(button) {
						button.getEl().on('dblclick', function() {
							button.fireEvent('doubleClickHandler')
						});
						clientVM.updateSaveButtons(button);
					}
				}
			}, 'saveAs', '->', {
				xtype: 'sysinfobutton',
				sysId: '@{id}',
				sysCreated: '@{sysCreatedOn}',
				sysCreatedBy: '@{sysCreatedBy}',
				sysUpdated: '@{sysUpdatedOn}',
				sysUpdatedBy: '@{sysUpdatedBy}',
				sysOrg: '@{sysOrganizationName}',
				dateFormat: '@{userDateFormat}'
			}, {
				iconCls: 'x-tbar-loading',
				handler: '@{refresh}'
			}]
		}],
		items: [
		standardViewConfig,
		additionalSettingViewConfig,
		{
			xtype: 'AceEditor',
			flex: 1,
			minHeight: 300,
			width: '100%',
			name: 'uscript',
			parser: 'groovy',
			title: 'Script'
		}]
	}
}

glu.defView('RS.gateway.HPOMFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			defaults: {
				layout: {
					type: 'vbox',
					align: 'stretch'
				}
			},
			items: [{
				flex: 1,
				padding: '0px 10px 0px 0px',
				items: [{
					labelWidth: 150,
					xtype: 'combo',
					queryMode: 'local',
					valueField: 'uobject',
					displayField: 'uobject',
					name: 'uobject',
					store: '@{objectStore}'
				}]
			}, {
				flex: 1,
				padding: '0px 0px 0px 10px',
				defaultType: 'textfield',
				items: ['uquery']
			}]
		})
	}
);
glu.defView('RS.gateway.HPSMFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			defaults: {
				layout: {
					type: 'vbox',
					align: 'stretch'
				}
			},
			items: [{
				flex: 1,
				padding: '0px 10px 0px 0px',
				items: [{
					labelWidth: 150,
					xtype: 'combo',
					queryMode: 'local',
					valueField: 'uobject',
					displayField: 'uobject',
					name: 'uobject',
					store: '@{objectStore}'
				}]
			}, {
				flex: 1,
				padding: '0px 0px 0px 10px',
				defaultType: 'textfield',
				items: ['uquery']
			}]
		})
	}
);
glu.defView('RS.gateway.HTTPFilter',
    function() {        
        return RS.gateway.buildGatewayViewConfig({
            title: '~~additionalSettingTitle~~',
            bodyPadding: '10px 0px 0px 10px',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            defaults : {
                margin : '0 0 5 0'
            },
            items: [
            {            
                layout  : {
                    type : 'hbox',
                    align : 'stretch'
                },
                defaults : {
                    labelWidth : 130,
                    flex : 1
                },
                items : [
                {
                    xtype: 'textfield',                     
                    name: 'uuri'
                },{
                    xtype: 'textfield',                   
                    name: 'uport',  
                     
                    margin : '0 0 0 15px'
                }]
            },{
            
                layout  : {
                    type : 'hbox',
                    align : 'stretch'
                },
                defaults : {
                    labelWidth : 130,
                    flex : 1
                },

                items : [
                
                {
                    xtype: 'textfield',
                    name: 'u_bas_icAu_thUs_ername',
                   hidden: '@{unpwHidden}',
                     
                 },
                 {
                    xtype: 'textfield',
                    name: 'u_bas_icAu_thPas_sword',
                    margin : '0 0 0 15px',
                    hidden: '@{unpwHidden}',
                    inputType: 'password'
                 }   
                 ]
             
            },{
                xtype: 'checkboxfield',
                name: 'ussl',
                labelWidth : 130
/*            },{
                xtype: 'textfield',
                labelWidth:120,
                name: 'uallowedIP' */
            },
            {
                layout : 'hbox',
                items : [
                {
                    xtype : 'combobox',
                    flex : 38,
                    value : '@{ublocking}',
                    valueField : 'value',  
                    fieldLabel : 'Block Call',
                    displayField : 'display',
                    store : '@{blockStore}',
                    queryMode : 'local',
                    editable : false,
                    labelWidth : 130,
                }, {
                    xtype : 'button',
                    flex: 1,
                    cls : 'x-btn-system-info-button-small-toolbar',
                    text : '',
                    tooltip : '~~blockCallTooltip~~', 
                    dismissDelay : 0,                  
                    iconCls: 'rs-icon-button icon-info-sign',
                    padding : '5 0 0 6'                
                },  {
                    xtype: 'combobox',
                    flex : 40,
                    cls: 'rs-http-gateway-content-type',
                    fieldLabel : '~~contentType~~',
                    value: '@{ucontentType}',
                    valueField : 'value',
                    displayField : 'display',
                    queryMode : 'local',
                    editable : false,
                    disabled: '@{contentCbTypeDisabled}',
                    disabledCls: 'rs-http-gateway-content-type-disabled',
                    store: '@{contentTypeStore}',
                    margin: '0 0 0 30',
                    labelWidth : 130
                }]
            }]
        })
    }
);
// glu.defView('RS.gateway.Main', );
RS.gateway.mainViewConfig = {
	padding: 15,	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		flex: 1,
		xtype: 'grid',
		name: 'filters',
		minHeight: 300,
		cls: 'rs-section-title rs-grid-dark',
		displayName: '@{filterDisplayName}',
		columns: '@{filterColumns}',
		store: '@{filters}',
		stateId: '@{filterStateId}',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: ['createFilter', {
				name: 'deleteFilters',
				tooltip: '@{whyUCannotDeleteIt}'
			}]
		}],
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}, {
			ptype: 'columnautowidth'
		}],
		dragText: '~~dragText~~',
		viewConfig: {
			allowCopy: true,
			copy: true,
			plugins: {
				ptype: 'gridviewdragdrop',
				// dragText: 'Drop it to the gateway grid to add this filter to the gateway',
				enableDrop: false
			},
			listeners: {
				render: function(view) {
					this.plugins[0].dragText = view.panel.dragText;
				}
			}
		},
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTooltip: RS.common.locale.editColumnTooltip,
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'id'
		},
		listeners: {
			editAction: '@{editFilter}',
			selectAllAcrossPages: '@{selectAllAcrossPages}',
			selectionChange: '@{selectionChanged}'
		}
	}, {
		flex: 1,
		xtype: 'panel',
		minHeight: 400,
		style : 'border-top :1px solid #cccccc;',
		margin : '10 0 0 0',
		padding : '10 0 0 0',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			ui: 'display-toolbar',
			cls: 'rs-section-title',
			defaultButtonUI: 'display-toolbar-button',
			items: [{
				xtype: 'tbtext',
				cls: 'rs-display-name',
				text: '@{gatewayFieldSetTitle}'
			}]
		}],
		defaults: {
			labelWidth: 150
		},
		layout : {
			type : 'vbox',
			align : 'stretch'
		},
		items: [
		{
			xtype: 'grid',
			cls : 'rs-grid-dark deployed-filters',
			flex : 1,
			name: 'deployedFilters',
			store: '@{deployedFilters}',
			columns: '@{deployedColumns}',
			stateId: '@{gatewayStateId}',
			dockedItems: [{
				xtype: 'toolbar',
				cls: 'actionBar rs-dockedtoolbar',
				name: 'actionBar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items: ['showFilterList', 'markDeletedDeployedFilters', /*'undeployFilters',*/ 'deployFilters' , 
				{
					xtype: 'tbseparator'
				},{
					xtype: 'combo',
					margin : '8 0',
					queryMode: 'local',
					fieldLabel: '~~gateways~~',
					labelWidth : 150,
					valueField: 'uqueue',
					displayField: 'uqueue',
					width: 600,		
					name: 'gateway',
					store: '@{gatewaysStore}'
				}]
			}],
			plugins: [{
				ptype: 'pager',
				pageable: false
			}],
			selModel: {
				selType: 'checkboxmodel',
				mode: 'MULTI'
			},
			viewConfig: {
				plugins: {
					ptype: 'gridviewdragdrop',
					enableDrag: false
				},
				markDirty: false,
				listeners: {
					beforedrop: function(node, data, dropRec, dropPosition, dropHandlers) {
						this.ownerCt.fireEvent('beforeDropRec', node, data, dropRec, dropPosition, dropHandlers);
					}
				},
				getRowClass: function(record, index, rowParams){
				    if (record.get('isDeleted') && record.get('uqueue') != '<|Default|Queue|>')
						return 'deleteClass'
					if (record.get('uqueue') === '<|Default|Queue|>' && !record.get('isMaster'))
						return 'newClass'
					if (record.get('uqueue') != '<|Default|Queue|>' && record.get('changed') && !record.get('isDeleted'))
						return 'changeClass'
			    }
			},
			listeners: {
				beforeDropRec: '@{addFiltersToGateway}'
			}
		}]
	}]
};
glu.defView('RS.gateway.Main', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.DatabaseConnectionPoolMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.DatabaseMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.EmailAddressMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.EmailMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.EWSAddressMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.EWSMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.ExchangeMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.HPOMMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.HPSMMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.NetcoolMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.RemedyxFormMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.RemedyxMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.SalesforceMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.ServiceNowMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.SNMPMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.SSHPoolMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.TelnetPoolMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.TSRMMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.NetcoolMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.XMPPMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.XMPPAddressMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.AMQPMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.HTTPMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.TCPMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.TIBCOBespokeMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.CASpectrumMain', RS.gateway.mainViewConfig);

glu.defView('RS.gateway.MessageFilter', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10px',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{title}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar actionBar-form',
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-eject rs-icon',
			text: ''
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}'
		}]
	}],
	items: [{
		xtype: 'fieldset',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},

		title: '~~generalSettingTitle~~',
		items: [{
			xtype: 'form',
			bodyPadding: '10px',
			flex: 1,
			defaultType: 'textfield',
			defaults: {
				labelWidth: 150
			},
			items: [{
				name: 'uname'
			}, {
				name: 'uorder'
			}, {
				name: 'ueventEventId'
			}]
		}, {
			xtype: 'form',
			bodyPadding: '10px',
			flex: 1,
			defaultType: 'textfield',
			items: [{
				name: 'uinterval'
			}, {
				name: 'urunbook'
			}, {
				xtype: 'checkboxfield',
				name: 'uactive',
				fieldLabel: '~~uactive~~'
			}]
		}]
	}, {
		xtype: 'fieldset',
		title: '~~additionalSettingTitle~~',
		padding: '10px 20px 10px 20px',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'textfield',
			labelWith: 150,
			name: 'uquery',
			fieldLabel: '~~uquery~~'
		}]
	}, {
		xtype: 'AceEditor',
		flex: 1,
		width: '100%',
		name: 'uscript',
		parser: 'groovy',
		title: 'Script'
	}]
});

glu.defView('RS.gateway.NetcoolFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'textfield',
				labelWidth: 150,
				name: 'usql',
				fieldLabel: '~~usql~~'
			}]
		})
	}
);
glu.defView('RS.gateway.RemedyxFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			// xtype:'fieldset',
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaultType: 'textfield',
			defaults: {
				labelWidth: 150
			},
			items: ['uformName', 'uquery', 'ulastValueField', 'ulimit']
		})
	}
);
glu.defView('RS.gateway.RemedyxForm', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10px',
	defaultType: 'textfield',
	defaults: {
		labelWidth: 150
	},
	items: ['uname', 'ufieldList'],
	buttonAlign: 'left',
	buttons: ['save', 'saveAs', 'cancel'],
	asWindow: {
		title: '@{title}',
		width: 600
	}
});
glu.defView('RS.gateway.SalesforceFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			// xtype:'fieldset',
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			defaults: {
				layout: {
					type: 'vbox',
					align: 'stretch'
				}
			},
			items: [{
				flex: 1,
				padding: '0px 10px 0px 0px',
				items: [{
					labelWidth: 150,
					xtype: 'combo',
					queryMode: 'local',
					valueField: 'uobject',
					displayField: 'uobject',
					name: 'uobject',
					store: '@{objectStore}'
				}]
			}, {
				flex: 1,
				padding: '0px 0px 0px 10px',
				defaultType: 'textfield',
				items: ['uquery']
			}]
		})
	}
);
glu.defView('RS.gateway.SaveAs', {
	title: '@{title}',
	xtype: 'panel',
	modal: true,
	bodyPadding: '10px',
	items: [{
		xtype: 'displayfield',
		name: 'msg',
		hideLabel: true
	}, {
		xtype: 'textfield',
		fieldLabel: '@{label}',
		name: 'name'
	}],
	buttonAlign: 'left',
	buttons: ['save', 'close']
});
glu.defView('RS.gateway.ServiceNowFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			defaults: {
				flex: 1
			},
			defaultType: 'textfield',
			items: [{
				name: 'uobject',
				labelWidth: 150,
				padding: '0px 10px 0px 0px'
			}, {
				name: 'uquery',
				padding: '0px 10px 0px 10px'
			}]
		})
	}
);
glu.defView('RS.gateway.SNMPFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			defaults: {
				flex: 1
			},
			items: [{
				xtype: 'container',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				padding: '0px 10px 0px 0px',
				defaultType: 'textfield',
				defaults: {
					labelWidth: 150
				},
				items: [
					'usnmpTrapOid',
					'usnmpVersion',
					'uipAddresses',
					'ureadCommunity', 'ucomparator', {
						xtype: 'checkbox',
						hideLabel: true,
						boxLabel: '~~utrapReceiver~~',
						padding: '0px 0px 0px 160px',
						name: 'utrapReceiver'
					}
				]
			}, {
				xtype: 'container',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				padding: '0px 0px 0px 10px',
				defaultType: 'textfield',
				items: ['utimeout',
					'uretries',
					'uregex',
					'uoid',

					'uvalue'
				]
			}]
		})
	}
);
glu.defView('RS.gateway.SSHPool', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10px',
	defaultType: 'textfield',
	defaults: {
		labelWidth: 150
	},
	items: ['usubnetMask', 'umaxConn', 'utimeout'],
	buttonAlign: 'left',
	buttons: ['save', 'saveAs', 'cancel'],
	asWindow: {
		title: '@{title}',
		width: 600
	}
});
glu.defView('RS.gateway.TCPFilter',
    function() {
        return RS.gateway.buildGatewayViewConfig({
            title: '~~additionalSettingTitle~~',
            bodyPadding: '10px 0px 0px 10px',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [{
                xtype: 'textfield',
                labelWidth:120,
                name: 'uport'
            },{
                xtype:'fieldcontainer',
                layout:{
                    type:'hbox',
                    align:'stretch'
                },  
                items:[{
                    flex:1,
                    padding:'0 10 0 0',
                    xtype:'fieldcontainer',
                    layout:{
                        type:'vbox',
                        align:'stretch'
                    },
                    items:[{
                        labelWidth:120,
                        xtype: 'textfield',
                        name: 'ubeginSeparator'
                    },{
                        labelWidth:120,
                        xtype: 'textfield',
                        name: 'uendSeparator'
                    }]
                },{
                    xtype:'fieldcontainer',
                    layout:{
                        type:'vbox',
                        align:'stretch'
                    },
                    items:[{
                        xtype: 'checkboxfield',
                        name: 'uincludeBeginSeparator',
                        boxLabel: '~~uincludeBeginSeparator~~',
                        hideLabel: true
                    }, {
                        xtype: 'checkboxfield',
                        name: 'uincludeEndSeparator',
                        boxLabel: '~~uincludeEndSeparator~~',
                        hideLabel: true
                    }]
                }]
            }]
        })
    }
);
glu.defView('RS.gateway.TelnetPool', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10px',
	defaultType: 'textfield',
	defaults: {
		labelWidth: 150
	},
	items: ['usubnetMask', 'umaxConn', 'utimeout'],
	buttonAlign: 'left',
	buttons: ['save', 'saveAs', 'cancel'],
	asWindow: {
		title: '@{title}',
		width: 600
	}
});
glu.defView('RS.gateway.TIBCOBespokeFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'container',
				layout: 'hbox',
				items: [{
					xtype: 'checkbox',
					name: 'ucertifiedMessaging',
					boxLabel: '~~ucertifiedMessaging~~',
					margin: '0 10 0 0',
					hideLabel: true
				}, {
					xtype: 'checkbox',
					name: 'uprocessAsXml',
					boxLabel: '~~uprocessAsXml~~',
					margin: '0 10 0 0',
					hideLabel: true
				}, {
					xtype: 'checkbox',
					name: 'uunescapeXml',
					boxLabel: '~~uunescapeXml~~',
					margin: '0 10 0 0',
					hideLabel: true
				}]
			}, {
				xtype: 'container',
				layout: 'hbox',
				margin: '0 0 10 0',
				items: [{
					xtype: 'checkbox',
					align: 'left',
					name: 'urequireReply',
					boxLabel: '~~urequireReply~~',
					hideLabel: true,
					margin: '0 20 0 0',
					labelWidth: 150,
					listeners: {
						change: function( checkbox, newValue, oldValue, eOpts)
						{
							var timeoutField = Ext.getCmp('ureplyTimeout');
							if(newValue)
							{
								timeoutField.show();
							}
							else
							{
								timeoutField.hide();
							}
						}
					}
				}, {
					xtype: 'textfield',
					align: 'right',
					name: 'ureplyTimeout',
					id: 'ureplyTimeout',
					fieldLabel: '~~ureplyTimeout~~',
					hidden: true,
					labelWidth: 175
				}]
			}, {
				xtype: 'textfield',
				name: 'uregex',
				fieldLabel: '~~uregex~~',
				hidden: true,			
				labelWidth: 150
			}, {
				xtype: 'textfield',
				name: 'utopic',
				fieldLabel: '~~utopic~~',
				emptyText: 'Q.PROBH17.PROBH.ALARMT.NOTI.NETWMR18',
				labelWidth: 150
			}, {
				xtype: 'textfield',
				name: 'ubusUri',
				fieldLabel: '~~ubusUri~~',
				emptyText: 'rvd://239.85.2.3:8523/omahft2:8523',
				labelWidth: 150
			}, {
				xtype: 'label',
				text: '~~uxmlQuery~~',
				margin: '10 0 10 0'
			}, {
				xtype: 'textareafield',
				name: 'uxmlQuery',
				fieldLabel: '',
				emptyText: 'tag1.subtag1 contains \'tagValue\' && tag1.subtag2 >= 0',
				labelWidth: 0
			}]
		})
	}
);
glu.defView('RS.gateway.TSRMFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			// padding:'10px 20px 10px 20px',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'combo',
				queryMode: 'local',
				valueField: 'uobject',
				displayField: 'uobject',
				name: 'uobject',
				store: '@{objectStore}'
			}, {
				xtype: 'textfield',
				labelWith: 150,
				name: 'uquery',
				fieldLabel: '~~uquery~~'
			}]
		})
	}
);
glu.defView('RS.gateway.XMPPAddress', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10px',
	defaultType: 'textfield',
	defaults: {
		labelWidth: 150
	},
	items: ['uxmppaddress', {
		name: 'uxmpppassword',
		inputType: 'password'
	}],
	buttonAlign: 'left',
	buttons: ['save', 'saveAs', 'cancel'],
	asWindow: {
		title: '@{title}',
		width: 500
	}
});
glu.defView('RS.gateway.XMPPFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'textfield',
				labelWith: 150,
				name: 'uregex',
				fieldLabel: '~~uregex~~'
			}]
		})
	}
);
/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.gateway').locale = {
	windowTitle: 'Gateway Project',
	uname: 'Name',
	uactive: 'Active',
	uorder: 'Order',
	uinterval: 'Interval',
	ueventEventId: 'Resolve Event ID',
	urunbook: 'Runbook',
	uquery: 'Query',
	ufolderNames: 'Folders to query',
	uonlyNew: 'Process Only New Emails',
	umarkAsRead: 'Mark retrieved emails as read',
	uscript: 'Script',
	usql: 'SQL',
	upool: 'Pool',
	uupdateSql: 'Update SQL',
	ulastValue: 'Last Value',
	ulastValueColumn: 'Last Value Column',
	uprefix: 'Prefix',
	ulastValueQuote: 'Last Value Quote',
	usubject: 'Subject',
	ucontent: 'Content',
	usendername: 'Sender Name',
	usenderaddress: 'Sender Address',
	uobject: 'Object',
	uincludeAttachment: 'Include Attachment',
	uformName: 'Form Name',
	ulastValueField: 'Last Value Field',
	utrapReceiver: 'Trap Receiver',
	usnmpTrapOid: 'Trap OID',
	usnmpVersion: 'Version',
	uipAddresses: 'IP Address',
	ureadCommunity: 'Read Community',
	utimeout: 'Timeout',
	uretries: 'Retries',
	uregex: 'Regex',
	ucomparator: 'Comparator',
	uvalue: 'Value',
	uoid: 'OID',
	uqueue: 'Queue',
	uemailAddress: 'Email Address',
	uemailPassword: 'Password',
	upoolName: 'Pool Name',
	udriverClass: 'Driver Class',
	u_db_us_ern_ame: 'Username',
	u_db_pas_sword: 'Password',
	udbconnectURI: 'Connection URI',
	udbmaxConn: 'Max Conn',
	usubnetMask: 'Subnet Mask',
	umaxConn: 'Max Conn',
	ufieldList: 'Field List',
    ulimit: 'Results Limit',
	uewsaddress: 'EWS Address',
	uewsp_assword: 'EWS Password',
	uxmppaddress: 'XMPP Address',
	uxmpppassword: 'XMPP Password',
	utargetQueue: 'Queue/Exchange',
	uexchange: 'Exchange',
	uport: 'Port',
	ussl: 'SSL',
	u_bas_icAu_thUs_ername: 'Username',
	u_bas_icAu_thPas_sword: 'Password',

	uuri: 'URI',
	ublocking : 'Block Call',
	ubeginSeparator: 'Begin Separator',
	uendSeparator: 'End Separator',
	uincludeBeginSeparator: 'Include Begin Separator',
	uincludeEndSeparator: 'Include End Separator',
	utopic: 'Topic',
	ubusUri: 'Bus URI',
	ucertifiedMessaging: 'Certified Messaging',
	uxmlQuery: 'Xml Query',
	uunescapeXml: 'Unescape Xml',
	uprocessAsXml: 'Process As Xml',
	urequireReply: 'Save message for response?',
	ureplyTimeout: 'Message expiration (ms)',
	uurlQuery : 'URL Query',
	uxmlQuery : 'XML Query',

	Gateway: 'Gateway',
	cancel: 'Cancel',
	back: 'Back',
	ServerErrMsg: 'The server currently cannot handle the request.',
	sysCreatedOn: 'Created On',
	sysCreatedBy: 'Created By',
	sysUpdatedOn: 'Updated On',
	sysUpdatedBy: 'Updated By',
	sysOrganizationName: 'Organizaion',
	sysId: 'Sys ID',

	//for main
	emailFilterFormTitle: 'Email Filter',
	emailFilterDisplayName: 'Email Gateway Filter',
	databaseFilterDisplayName: 'Database Gateway Filter',
	exchangeFilterDisplayName: 'Exchange Gateway Filter',
	tsrmFilterDisplayName: 'TSRM Gateway Filter',
	netcoolFilterDisplayName: 'Netcool Gateway Filter',
	ewsFilterDisplayName: 'EWS Gateway Filter',
	remedyxFilterDisplayName: 'Remedyx Gateway Filter',
	salesforceFilterDisplayName: 'Salesforce Gateway Filter',
	hpomFilterDisplayName: 'HPOM Gateway Filter',
	hpsmFilterDisplayName: 'HPSM Gateway Filter',
	serviceNowFilterDisplayName: 'ServiceNow Gateway Filter',
	snmpFilterDisplayName: 'SNMP Gateway Filter',
	xmppFilterDisplayName: 'XMPP Gateway Filter',
	amqpFilterDisplayName: 'AMQP Gateway Filter',
	httpFilterDisplayName: 'HTTP Gateway Filter',
	tcpFilterDisplayName: 'TCP Gateway Filter',
	CASpectrumFilterDisplayName : 'CA Spectrum Gateway Filter',

	emailAddressDisplayName: 'Email Address',
	databaseConnectionPoolDisplayName: 'Database Connection Pool',
	telnetPoolDisplayName: 'Telnet Connection Pool',
	remedyxFormDisplayName: 'Remedyx Form',
	ewsAddressFilterDisplayName: 'EWS Address',
	sshPoolDisplayName: 'SSH Connection Pool',
	xmppAddressDisplayName: 'XMPP Address',
	tibcoBespokeFilterDisplayName: 'TIBCO Filter',

	ListFiltersErrMsg: 'Cannot get filters from the server.',
	createFilter: 'New',
	deleteFilters: 'Delete',
	filterFieldSetTitle: 'Filters',
	gatewayFieldSetTitle: 'Deployment Information',
	emailTitle: 'Email Gateway',
	gateways: 'Available Gateways',
	showGatewayList: 'Delete Gateway',
	showFilterList: 'Add',
	updateDeployedFilters: 'Update Filters',
	deployFilters: 'Deploy All',
	AddFiltersToGatewayErrMsg: 'Filters {name} is already added to the gateway.',
	ListGatewaysErrMsg: 'Cannot get getways from server',
	deleteFiltersTitle: 'Delete Filters',
	deleteFilterRecs: 'Are you sure you want to delete the selected filter?({num} items selected)',
	deleteFilterRec: 'Are you sure  you want to delete the selected filter?',
	confirmDeleteFilters: 'Delete',
	DeleteFilterSucMsg: 'The selected filters have been deleted',
	ListDeployedFiltersErrMsg: 'Cannot get deployed filters from the server.',
	DeployTitle: 'Deploy Filters',
	DeploysMsg: 'Are you sure you want to deploy all the filters?',
	confirmDeploy: 'Deploy',
	LoadGatewaysErrMsg: 'Cannot get gateways from the server.',
	NullGatewayError: 'Please select a gateway first.',
	DeployedFilterDuplication: 'Filter {name} has already been added to the gateway.',
	DeploySucMsg: 'The filters have been deployed.',
	DeployErrMsg: 'Cannot deploy the filters',
	undeployFilters: 'Undeploy Filters',
	UndeployFilters: 'Undeploy Filters',
	UndeployFilterMsg: 'Are you sure you want to undeploy the selected item? ',
	UndeployFiltersMsg: 'Are you sure you want to undeploy the selected items?({num} item selected)',
	UndeployFilterErrMsg: 'Cannot deploy the selected filters.',
	UndeployFilterSucMsg: 'The selected filters have been undeployed.',
	confirmUndeploy: 'Undeploy',
	markDeletedDeployedFilters: 'Delete',
	whyUCannotDeleteIt: 'Items:[{names}]  have been deployed',



	save: 'Save',
	saveAs: 'Save As',
	generalSettingTitle: 'General Setting',
	additionalSettingTitle: 'Additional Setting',
	SaveSucMsg: 'The filter has been saved.',
	SaveErrMsg: 'Cannot save the filter.',
	invalidName: 'Name is required and must be in alphanumerics, but may contain single spaces or underscores \'_\' between alphanumeric characters.',
	invalidObject: 'The object cannot be empty.',
	invalidOrder: 'The order should be an integer.',
	ServerErrMsg: 'Server Error',
	ServerErrTitle: 'Server cannot handle the request.',
	GetFilterErrMsg: 'Cannot get the filter from the server.',
	uscript: 'Script',
	invalidAddress: 'The address is required.',
	invalidField: 'The field cannot be empty.',

	dragText: 'Drop it to the gateway grid to add this filter to the gateway.',
	MessageFilter: {
		invalidQuery: 'The query cannot be empty'
	},

	Filters: {
		addFilters: 'Add Filters To Gateway',
		cancel: 'Cancel'
	},

	BaseFilter: {
		invalidName: 'The name cannot be empty.'
	},

	Gateways: {
		deleteGateways: 'Delete',
		cancel: 'Cancel',
		deleteGatewaysTitle: 'Delete Gateways',
		deleteGatewaysMsg: 'Are you sure you want to delete the selected gateways?({num} items selected)',
		deleteGatewayMsg: 'Are you sure you want to delete the selected gateways?',
		confirmDeleteGateway: 'Delete',
		ServerErrTitle: 'Server Error',
		ServerErrMsg: 'The server currently cannot handle the request.',
		confirmAjaxException: 'OK',
		ListGatewaysErrTitle: 'Get Gateways Error',
		ListGatewaysErrMsg: 'Cannot get getways from server'
	},

	EmailFilter: {
		emailFilterTitle: 'Email Filter',
		invalidQuery: 'The query cannot be empty'
	},

	NetcoolFilter: {
		netcoolFilterTitle: 'Netcool Filter'
	},

	DatabaseFilter: {
		databaseFilterTitle: 'Database Filter',
		invalidMaxConn: 'the max conn should be a integer'
	},

	ExchangeFilter: {
		exchangeFilterTitle: 'Exchange Filter'
	},

	TSRMFilter: {
		tsrmFilterTitle: 'TSRM Filter'
	},

	EWSFilter: {
		ewsFilterTitle: 'EWS Filter'
	},

	RemedyxFilter: {
		remedyxFilterTitle: 'Remedyx Filter'
	},

	SalesforceFilter: {
		salesforceFilterTitle: 'Salesforce Filter'
	},

	HPOMFilter: {
		hpomFilterTitle: 'HPOM Filter'
	},

	HPSMFilter: {
		hpsmFilterTitle: 'HPSM Filter'
	},

	ServiceNowFilter: {
		serviceNowFilterTitle: 'ServiceNow Filter'
	},

	CASpectrumFilter: {
		CASpectrumFilterTitle: 'CA Spectrum Filter'
	},

	SNMPFilter: {
		snmpFilterTitle: 'SNMP Filter',
		invalidTimeout: 'Timeout should be a integer.',
		invalidVersion: 'Version should be a integer.',
	},

	XMPPFilter: {
		xmppFilterTitle: 'XMPP Filter'
	},

	TIBCOBespokeFilter: {
		tibcoBespokeFilterTitle: 'TIBCO Filter'
	},

	AMQPFilter: {
		amqpFilterTitle: 'AMQP Filter'
	},

	HTTPFilter: {
		httpFilterTitle: 'HTTP Filter',
		defaultBlockCall : 'Default',
		gatewayBlockCall : 'Gateway Script',
		worksheetBlockCall : 'Worksheet ID',
		runbookBlockCall : 'Execution Complete',
		blockCallTooltip :  '<b>Default</b> : Return immediately after receiving the message.<br>' +
							'<b>Gateway Script</b> : Return after execution of gateway script. The gateway script can set the additional response.<br>' +
							'<b>Worksheet ID</b> :  Return after getting the worksheet ID. If gateway script is executed, gateway script can set additional response.<br>' +
							'<b>Execution Complete</b> : Return after the runbook complete execution, return worksheet ID, and execution condition and severity<br>' +
							'<b>Filtered Response</b> : Returns after the Runbook execution, returns the RESULT.detail from "httpfilter_response" action task of the ' +
							'  Runbook executed, If the "httpfilter_response" is not part of the Runbook please include it',
		filteredResponseBlockCall: 'Filtered Response',
		contentType: 'Content Type',
		contentTypeJSON: 'application/json',
		contentTypeDefault: 'Default',
		requestFilter: 'Response Filter'
	},

	TCPFilter: {
		tcpFilterTitle: 'TCP Filter'
	},

	EmailAddress: {
		emailAddressTitle: 'Email Address'
	},

	EmailAddressMain: {
		dragText: 'Drop it to the gateway grid to add this address to the gateway.',
		DeploySucMsg: 'The addresses have been deployed.',
		DeploysMsg: 'Are you sure you want to deploy all the addresses?'
	},

	DatabaseConnectionPool: {
		databaseConnectionPoolTitle: 'Database Connection Pool',
		showGenerator: 'URI Generator',
		generateURI: 'Generate URI ',
		dbType: 'Database Type',
		ipAddress: 'Host/IP Address',
		port: 'Port',
		dbName: 'Database Name',
		hideGenerator: 'Hide',
		howToGetURI: 'Get URI from',
		sid: 'SID',
		service: 'Service'
	},

	DatabaseConnectionPoolMain: {
		dragText: 'Drop it to the gateway grid to add this connection to the gateway.',
		DeploySucMsg: 'The connections have been deployed.',
		DeploysMsg: 'Are you sure you want to deploy all the connections?'
	},

	TelnetPool: {
		telnetPoolTitle: 'Telnet Pool',
		invalidSubnetMask: 'Invalid Subnet Mask format.example:255.255.255.0',
		invalidMaxConn: 'Max connection should be an integer',
		invalidTimeout: 'Timeout should be a integer.'
	},

	TelnetPoolMain: {
		dragText: 'Drop it to the gateway grid to add this connection to the gateway.',
		DeploySucMsg: 'The connections have been deployed.',
		DeploysMsg: 'Are you sure you want to deploy all the connection?'
	},

	RemedyxForm: {
		remedyxFormTitle: 'Remedyx Form',
		ufieldList: 'Field List'
	},

	RemedyxFormMain: {
		dragText: 'Drop it to the gateway grid to add this form to the gateway.',
		DeploySucMsg: 'The forms have been deployed.',
		DeploysMsg: 'Are you sure you want to deploy all the connections?'
	},

	EWSAddress: {
		ewsAddressTitle: 'EWS Address'
	},

	EWSAddressMain: {
		dragText: 'Drop it to the gateway grid to add this address to the gateway.',
		DeploySucMsg: 'The addresses have been deployed.',
		DeploysMsg: 'Are you sure you want to deploy all the connections?'
	},

	SSHPool: {
		sshPoolTitle: 'SSH Pool',
		invalidSubnetMask: 'Invalid Subnet Mask format.example:255.255.255.0',
		invalidMaxConn: 'Max connection should be an integer',
		invalidTimeout: 'Timeout should be a integer.'
	},

	SSHPoolMain: {
		dragText: 'Drop it to the gateway grid to add this connection to the gateway.',
		DeploySucMsg: 'The connections have been deployed.',
		DeploysMsg: 'Are you sure you want to deploy all the connections?'
	},

	XMPPAddress: {
		xmppAddressTitle: 'XMPP Address'
	},

	XMPPAddressMain: {
		dragText: 'Drop it to the gateway grid to add this address to the gateway.',
		DeploySucMsg: 'The addresses have been deployed.',
		DeploysMsg: 'Are you sure you want to deploy all the connections?'
	},
	name: 'Name',
	SaveAs: {
		saveAsNewFilterTitle: 'Save As New',
		saveAsNewFilterMsg: 'Enter the new {name}.',
		close: 'Close'
	}
}
