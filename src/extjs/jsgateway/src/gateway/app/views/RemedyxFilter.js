glu.defView('RS.gateway.RemedyxFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			// xtype:'fieldset',
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaultType: 'textfield',
			defaults: {
				labelWidth: 150
			},
			items: ['uformName', 'uquery', 'ulastValueField', 'ulimit']
		})
	}
);