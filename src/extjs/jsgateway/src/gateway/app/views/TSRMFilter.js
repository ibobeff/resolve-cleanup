glu.defView('RS.gateway.TSRMFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			// padding:'10px 20px 10px 20px',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'combo',
				queryMode: 'local',
				valueField: 'uobject',
				displayField: 'uobject',
				name: 'uobject',
				store: '@{objectStore}'
			}, {
				xtype: 'textfield',
				labelWith: 150,
				name: 'uquery',
				fieldLabel: '~~uquery~~'
			}]
		})
	}
);