glu.defView('RS.gateway.EmailAddress', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10px',
	defaultType: 'textfield',
	defaults: {
		labelWidth: 150
	},
	items: [{
		name: 'uemailAddress',
		readOnly: '@{unameIsReadOnly}'
	}, {
		name: 'uemailPassword',
		inputType: 'password'
	}],
	buttonAlign: 'left',
	buttons: ['save', 'saveAs', 'cancel'],
	asWindow: {
		title: '@{title}',
		width: 500,
		modal: true
	}
});