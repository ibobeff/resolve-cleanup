glu.defView('RS.gateway.NetcoolFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'textfield',
				labelWidth: 150,
				name: 'usql',
				fieldLabel: '~~usql~~'
			}]
		})
	}
);