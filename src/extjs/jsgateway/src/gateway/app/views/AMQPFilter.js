glu.defView('RS.gateway.AMQPFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'textfield',
				labelWidth: 110,
				name: 'utargetQueue',
				padding: '0 10 0 0',
				flex: 1,
				fieldLabel: '~~utargetQueue~~'
			}, {
				xtype: 'checkboxfield',
				name: 'uexchange',
				boxLabel: '~~uexchange~~',
				hideLabel: true
			}]
		})
	}
);