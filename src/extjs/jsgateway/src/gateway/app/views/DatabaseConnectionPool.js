glu.defView('RS.gateway.DatabaseConnectionPool', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10px',
	defaultType: 'textfield',
	defaults: {
		labelWidth: 150
	},
	items: [{
		name: 'upoolName',
		readOnly: '@{unameIsReadOnly}'
	}, 'u_db_us_ern_ame', {
		name: 'u_db_pas_sword',
		inputType: 'password'
	}, 'udbmaxConn', {
		xtype: 'form',
		layout: 'hbox',
		defaultType: 'radiofield',
		items: [{
			xtype: 'displayfield',
			name: 'howToGetURI',
			labelWidth: 150
		}, {
			name: 'generateType',
			margin: '0px 0px 0px 20px',
			boxLabel: 'dbInfo',
			value: '@{dbInfo}'
		}, {
			name: 'generateType',
			margin: '0px 0px 0px 20px',
			boxLabel: 'custom',
			value: '@{custom}'
		}]
	}, {
		name: 'udriverClass',
		xtype: 'combobox',
		editable: false,
		queryMode: 'local',
		displayField: 'name',
		valueField: 'value',
		store: '@{driverStore}'
	}, 'udbconnectURI', {
		xtype: 'form',
		hidden: '@{custom}',
		html: '<hr/>'
	}, {
		xtype: 'form',
		hidden: '@{custom}',
		defaultType: 'textfield',
		defaults: {
			labelWidth: 150
		},
		items: [{
			name: 'dbType',
			xtype: 'combobox',
			editable: false,
			queryMode: 'local',
			displayField: 'name',
			valueField: 'value',
			store: '@{dbTypeStore}'
		}, 'ipAddress', 'port', 'dbName', 'sid', 'service', {
			xtype: 'form',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			defaultType: 'button',
			defaults: {
				margin: '0px 0px 0px 5px'
			},
			items: [{
				xtype: 'form',
				flex: 1
			}, 'generateURI']
		}]
	}],
	buttonAlign: 'left',
	buttons: ['save', 'saveAs', 'cancel'],
	asWindow: {
		modal: true,
		title: '@{title}',
		width: 700
	}
});