glu.defView('RS.gateway.Gateways', {
	autoScroll: true,
	bodyPadding: '10px',
	buttonAlign: 'left',
	items: [{
		xtype: 'grid',
		name: 'gateways',
		columns: '@{columns}',
		store: '@{gatewayStore}'
	}],
	buttons: ['deleteGateways', 'cancel'],
	asWindow: {
		width: 500,
		height: 400,
		title: '@{title}'
	}
});