glu.defView('RS.gateway.EWSAddress', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10px',
	defaultType: 'textfield',
	defaults: {
		labelWidth: 150
	},
	items: ['uewsaddress', {
		name: 'uewspassword',
		fieldLabel: '~~uewsp_assword~~',
		inputType: 'password'
	}],
	buttonAlign: 'left',
	buttons: ['save', 'saveAs', 'cancel'],
	asWindow: {
		title: '@{title}',
		width: 500
	}
});