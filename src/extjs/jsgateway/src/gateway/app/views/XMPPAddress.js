glu.defView('RS.gateway.XMPPAddress', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10px',
	defaultType: 'textfield',
	defaults: {
		labelWidth: 150
	},
	items: ['uxmppaddress', {
		name: 'uxmpppassword',
		inputType: 'password'
	}],
	buttonAlign: 'left',
	buttons: ['save', 'saveAs', 'cancel'],
	asWindow: {
		title: '@{title}',
		width: 500
	}
});