glu.defView('RS.gateway.SNMPFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			defaults: {
				flex: 1
			},
			items: [{
				xtype: 'container',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				padding: '0px 10px 0px 0px',
				defaultType: 'textfield',
				defaults: {
					labelWidth: 150
				},
				items: [
					'usnmpTrapOid',
					'usnmpVersion',
					'uipAddresses',
					'ureadCommunity', 'ucomparator', {
						xtype: 'checkbox',
						hideLabel: true,
						boxLabel: '~~utrapReceiver~~',
						padding: '0px 0px 0px 160px',
						name: 'utrapReceiver'
					}
				]
			}, {
				xtype: 'container',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				padding: '0px 0px 0px 10px',
				defaultType: 'textfield',
				items: ['utimeout',
					'uretries',
					'uregex',
					'uoid',

					'uvalue'
				]
			}]
		})
	}
);