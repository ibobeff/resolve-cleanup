glu.defView("RS.gateway.CustomFilter",{
	layout: 'fit',
	panel: '@{panel}',
	listeners:{
		render:function(){
			var me = this;
			this._vm.on('updateView',function(view){
				this.removeAll();
				this.add(view);
				this.doLayout();
			}, me),
			this._vm.fireEvent("viewIsReady");
		}
	}
})



