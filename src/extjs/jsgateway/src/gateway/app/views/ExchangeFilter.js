glu.defView('RS.gateway.ExchangeFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaultType: 'textarea',
			defaults: {
				labelWidth: 150
			},
			items: ['usubject', 'ucontent', 'usendername', 'usenderaddress']
		})
	}
);