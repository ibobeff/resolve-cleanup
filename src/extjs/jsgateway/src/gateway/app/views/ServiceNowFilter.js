glu.defView('RS.gateway.ServiceNowFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			defaults: {
				flex: 1
			},
			defaultType: 'textfield',
			items: [{
				name: 'uobject',
				labelWidth: 150,
				padding: '0px 10px 0px 0px'
			}, {
				name: 'uquery',
				padding: '0px 10px 0px 10px'
			}]
		})
	}
);