glu.defView('RS.gateway.Filters', {
	autoScroll: true,
	bodyPadding: '10px',
	buttonAlign: 'left',
	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'filters',
		displayName: '@{..filterDisplayName}',
		columns: '@{..filterColumns}',
		store: '@{..filters}',
		stateId: '@{..filterStateId}',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: []
		}],
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}, {
			ptype: 'columnautowidth'
		}],
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTooltip: RS.common.locale.editColumnTooltip,
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'id'
		},
		listeners: {
			editAction: function() {}
		}
	}],
	buttons: ['addFilters', 'cancel'],
	asWindow: {
		title: '@{title}',
		modal: true,
		listeners: {
			beforeshow: function() {
				this.setWidth(Math.round(Ext.getBody().getWidth() * 0.9));
				this.setHeight(Math.round(Ext.getBody().getHeight() * 0.6));
			}
		}
	}
});