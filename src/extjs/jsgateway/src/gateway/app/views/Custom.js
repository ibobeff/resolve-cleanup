glu.defView('RS.gateway.Custom', {
	autoScroll: true,
	layout: 'fit',
	panel : '@{panel}',
	listeners:{
		render:function(){
			var me = this;
			me._vm.on('reloadingView',function(view){
				me.removeAll();
				me.doLayout();
			}, me),
			me._vm.on('updateView',function(view){
				me.add(view);
			}, me),
			me._vm.fireEvent("viewIsReady");
		}
	}
});