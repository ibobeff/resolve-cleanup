// glu.defView('RS.gateway.Main', );
RS.gateway.mainViewConfig = {
	padding: 15,	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		flex: 1,
		xtype: 'grid',
		name: 'filters',
		minHeight: 300,
		cls: 'rs-section-title rs-grid-dark',
		displayName: '@{filterDisplayName}',
		columns: '@{filterColumns}',
		store: '@{filters}',
		stateId: '@{filterStateId}',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: ['createFilter', {
				name: 'deleteFilters',
				tooltip: '@{whyUCannotDeleteIt}'
			}]
		}],
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}, {
			ptype: 'columnautowidth'
		}],
		dragText: '~~dragText~~',
		viewConfig: {
			allowCopy: true,
			copy: true,
			plugins: {
				ptype: 'gridviewdragdrop',
				// dragText: 'Drop it to the gateway grid to add this filter to the gateway',
				enableDrop: false
			},
			listeners: {
				render: function(view) {
					this.plugins[0].dragText = view.panel.dragText;
				}
			}
		},
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTooltip: RS.common.locale.editColumnTooltip,
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'id'
		},
		listeners: {
			editAction: '@{editFilter}',
			selectAllAcrossPages: '@{selectAllAcrossPages}',
			selectionChange: '@{selectionChanged}'
		}
	}, {
		flex: 1,
		xtype: 'panel',
		minHeight: 400,
		style : 'border-top :1px solid #cccccc;',
		margin : '10 0 0 0',
		padding : '10 0 0 0',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			ui: 'display-toolbar',
			cls: 'rs-section-title',
			defaultButtonUI: 'display-toolbar-button',
			items: [{
				xtype: 'tbtext',
				cls: 'rs-display-name',
				text: '@{gatewayFieldSetTitle}'
			}]
		}],
		defaults: {
			labelWidth: 150
		},
		layout : {
			type : 'vbox',
			align : 'stretch'
		},
		items: [
		{
			xtype: 'grid',
			cls : 'rs-grid-dark deployed-filters',
			flex : 1,
			name: 'deployedFilters',
			store: '@{deployedFilters}',
			columns: '@{deployedColumns}',
			stateId: '@{gatewayStateId}',
			dockedItems: [{
				xtype: 'toolbar',
				cls: 'actionBar rs-dockedtoolbar',
				name: 'actionBar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items: ['showFilterList', 'markDeletedDeployedFilters', /*'undeployFilters',*/ 'deployFilters' , 
				{
					xtype: 'tbseparator'
				},{
					xtype: 'combo',
					margin : '8 0',
					queryMode: 'local',
					fieldLabel: '~~gateways~~',
					labelWidth : 150,
					valueField: 'uqueue',
					displayField: 'uqueue',
					width: 600,		
					name: 'gateway',
					store: '@{gatewaysStore}'
				}]
			}],
			plugins: [{
				ptype: 'pager',
				pageable: false
			}],
			selModel: {
				selType: 'checkboxmodel',
				mode: 'MULTI'
			},
			viewConfig: {
				plugins: {
					ptype: 'gridviewdragdrop',
					enableDrag: false
				},
				markDirty: false,
				listeners: {
					beforedrop: function(node, data, dropRec, dropPosition, dropHandlers) {
						this.ownerCt.fireEvent('beforeDropRec', node, data, dropRec, dropPosition, dropHandlers);
					}
				},
				getRowClass: function(record, index, rowParams){
				    if (record.get('isDeleted') && record.get('uqueue') != '<|Default|Queue|>')
						return 'deleteClass'
					if (record.get('uqueue') === '<|Default|Queue|>' && !record.get('isMaster'))
						return 'newClass'
					if (record.get('uqueue') != '<|Default|Queue|>' && record.get('changed') && !record.get('isDeleted'))
						return 'changeClass'
			    }
			},
			listeners: {
				beforeDropRec: '@{addFiltersToGateway}'
			}
		}]
	}]
};
glu.defView('RS.gateway.Main', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.DatabaseConnectionPoolMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.DatabaseMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.EmailAddressMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.EmailMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.EWSAddressMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.EWSMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.ExchangeMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.HPOMMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.HPSMMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.NetcoolMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.RemedyxFormMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.RemedyxMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.SalesforceMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.ServiceNowMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.SNMPMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.SSHPoolMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.TelnetPoolMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.TSRMMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.NetcoolMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.XMPPMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.XMPPAddressMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.AMQPMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.HTTPMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.TCPMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.TIBCOBespokeMain', RS.gateway.mainViewConfig);
glu.defView('RS.gateway.CASpectrumMain', RS.gateway.mainViewConfig);
