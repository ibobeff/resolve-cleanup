glu.defView('RS.gateway.TelnetPool', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10px',
	defaultType: 'textfield',
	defaults: {
		labelWidth: 150
	},
	items: ['usubnetMask', 'umaxConn', 'utimeout'],
	buttonAlign: 'left',
	buttons: ['save', 'saveAs', 'cancel'],
	asWindow: {
		title: '@{title}',
		width: 600
	}
});