glu.defView('RS.gateway.TCPFilter',
    function() {
        return RS.gateway.buildGatewayViewConfig({
            title: '~~additionalSettingTitle~~',
            bodyPadding: '10px 0px 0px 10px',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [{
                xtype: 'textfield',
                labelWidth:120,
                name: 'uport'
            },{
                xtype:'fieldcontainer',
                layout:{
                    type:'hbox',
                    align:'stretch'
                },  
                items:[{
                    flex:1,
                    padding:'0 10 0 0',
                    xtype:'fieldcontainer',
                    layout:{
                        type:'vbox',
                        align:'stretch'
                    },
                    items:[{
                        labelWidth:120,
                        xtype: 'textfield',
                        name: 'ubeginSeparator'
                    },{
                        labelWidth:120,
                        xtype: 'textfield',
                        name: 'uendSeparator'
                    }]
                },{
                    xtype:'fieldcontainer',
                    layout:{
                        type:'vbox',
                        align:'stretch'
                    },
                    items:[{
                        xtype: 'checkboxfield',
                        name: 'uincludeBeginSeparator',
                        boxLabel: '~~uincludeBeginSeparator~~',
                        hideLabel: true
                    }, {
                        xtype: 'checkboxfield',
                        name: 'uincludeEndSeparator',
                        boxLabel: '~~uincludeEndSeparator~~',
                        hideLabel: true
                    }]
                }]
            }]
        })
    }
);