glu.defView('RS.gateway.HTTPFilter',
    function() {        
        return RS.gateway.buildGatewayViewConfig({
            title: '~~additionalSettingTitle~~',
            bodyPadding: '10px 0px 0px 10px',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            defaults : {
                margin : '0 0 5 0'
            },
            items: [
            {            
                layout  : {
                    type : 'hbox',
                    align : 'stretch'
                },
                defaults : {
                    labelWidth : 130,
                    flex : 1
                },
                items : [
                {
                    xtype: 'textfield',                     
                    name: 'uuri'
                },{
                    xtype: 'textfield',                   
                    name: 'uport',  
                     
                    margin : '0 0 0 15px'
                }]
            },{
            
                layout  : {
                    type : 'hbox',
                    align : 'stretch'
                },
                defaults : {
                    labelWidth : 130,
                    flex : 1
                },

                items : [
                
                {
                    xtype: 'textfield',
                    name: 'u_bas_icAu_thUs_ername',
                   hidden: '@{unpwHidden}',
                     
                 },
                 {
                    xtype: 'textfield',
                    name: 'u_bas_icAu_thPas_sword',
                    margin : '0 0 0 15px',
                    hidden: '@{unpwHidden}',
                    inputType: 'password'
                 }   
                 ]
             
            },{
                xtype: 'checkboxfield',
                name: 'ussl',
                labelWidth : 130
/*            },{
                xtype: 'textfield',
                labelWidth:120,
                name: 'uallowedIP' */
            },
            {
                layout : 'hbox',
                items : [
                {
                    xtype : 'combobox',
                    flex : 38,
                    value : '@{ublocking}',
                    valueField : 'value',  
                    fieldLabel : 'Block Call',
                    displayField : 'display',
                    store : '@{blockStore}',
                    queryMode : 'local',
                    editable : false,
                    labelWidth : 130,
                }, {
                    xtype : 'button',
                    flex: 1,
                    cls : 'x-btn-system-info-button-small-toolbar',
                    text : '',
                    tooltip : '~~blockCallTooltip~~', 
                    dismissDelay : 0,                  
                    iconCls: 'rs-icon-button icon-info-sign',
                    padding : '5 0 0 6'                
                },  {
                    xtype: 'combobox',
                    flex : 40,
                    cls: 'rs-http-gateway-content-type',
                    fieldLabel : '~~contentType~~',
                    value: '@{ucontentType}',
                    valueField : 'value',
                    displayField : 'display',
                    queryMode : 'local',
                    editable : false,
                    disabled: '@{contentCbTypeDisabled}',
                    disabledCls: 'rs-http-gateway-content-type-disabled',
                    store: '@{contentTypeStore}',
                    margin: '0 0 0 30',
                    labelWidth : 130
                }]
            }]
        })
    }
);