glu.defView('RS.gateway.SaveAs', {
	title: '@{title}',
	xtype: 'panel',
	modal: true,
	bodyPadding: '10px',
	items: [{
		xtype: 'displayfield',
		name: 'msg',
		hideLabel: true
	}, {
		xtype: 'textfield',
		fieldLabel: '@{label}',
		name: 'name'
	}],
	buttonAlign: 'left',
	buttons: ['save', 'close']
});