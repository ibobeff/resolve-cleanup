glu.defView('RS.gateway.EWSFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items:[{
				flex: 1,
				xtype: 'panel',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaults: {
					layout: {
						type: 'hbox',
						align: 'stretch'
					}
				},
				items:[{
					xtype: 'panel',
					padding: '0px 10px 0px 0px',
					items: [{
						flex: 1,
						xtype: 'textfield',
						labelWidth: 150,
						name: 'uquery',
						fieldLabel: '~~uquery~~'
					}]
				}, {
					xtype: 'panel',
					padding: '10px 10px 0px 0px',
					items: [{
						flex: 1,
						xtype: 'textfield',
						labelWidth: 150,
						name: 'ufolderNames',
						fieldLabel: '~~ufolderNames~~',
					}]
				},{ xtype: 'panel',
					padding: '10px 0px 0px 0px',
					items: [{
							flex: 1,
							xtype: 'checkbox',
							name: 'uincludeAttachment',
							padding: '0px 0px 0px 10px',
							hideLabel: true,
							boxLabel: '~~uincludeAttachment~~'
						}, {
							flex: 1,
							xtype: 'checkbox',
							name: 'uonlyNew',
							padding: '0px 0px 0px 10px',
							hideLabel: true,
							checked: true,
							boxLabel: '~~uonlyNew~~'
						}, {
							flex: 1,
							xtype: 'checkbox',
							name: 'umarkAsRead',
							padding: '0px 0px 0px 10px',
							hideLabel: true,
							checked: true,
							boxLabel: '~~umarkAsRead~~'
						}]	
				}]
			}]
		})
	}
);