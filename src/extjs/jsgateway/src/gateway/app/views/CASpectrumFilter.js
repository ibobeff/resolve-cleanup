glu.defView('RS.gateway.CASpectrumFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			bodyPadding : '10 0 0 10',
			defaults: {
				flex: 1,
				labelWidth : 130,
				margin : '4 0'
			},
			items: [
			{
				layout : {
					type : 'hbox',
					align : 'stretch'
				},
				defaults : {
					labelWidth : 130
				},
				items : [
				{
					name: 'uobject',
					flex : 1,
					xtype: 'combobox',
					labelField : 'Object',
					queryMode : 'local',
					displayField : 'name',
					store : '@{objectStore}',				
					editable: false
				},{
					name: 'uurlQuery',
					flex : 1,
					xtype : 'textfield',					
					margin : '0 0 0 10'
				}]
			},{
				name: 'uxmlQuery',
				xtype : 'textareafield',
				height : 200			
			}]
		})
	}
);