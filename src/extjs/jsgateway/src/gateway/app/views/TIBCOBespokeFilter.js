glu.defView('RS.gateway.TIBCOBespokeFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'container',
				layout: 'hbox',
				items: [{
					xtype: 'checkbox',
					name: 'ucertifiedMessaging',
					boxLabel: '~~ucertifiedMessaging~~',
					margin: '0 10 0 0',
					hideLabel: true
				}, {
					xtype: 'checkbox',
					name: 'uprocessAsXml',
					boxLabel: '~~uprocessAsXml~~',
					margin: '0 10 0 0',
					hideLabel: true
				}, {
					xtype: 'checkbox',
					name: 'uunescapeXml',
					boxLabel: '~~uunescapeXml~~',
					margin: '0 10 0 0',
					hideLabel: true
				}]
			}, {
				xtype: 'container',
				layout: 'hbox',
				margin: '0 0 10 0',
				items: [{
					xtype: 'checkbox',
					align: 'left',
					name: 'urequireReply',
					boxLabel: '~~urequireReply~~',
					hideLabel: true,
					margin: '0 20 0 0',
					labelWidth: 150,
					listeners: {
						change: function( checkbox, newValue, oldValue, eOpts)
						{
							var timeoutField = Ext.getCmp('ureplyTimeout');
							if(newValue)
							{
								timeoutField.show();
							}
							else
							{
								timeoutField.hide();
							}
						}
					}
				}, {
					xtype: 'textfield',
					align: 'right',
					name: 'ureplyTimeout',
					id: 'ureplyTimeout',
					fieldLabel: '~~ureplyTimeout~~',
					hidden: true,
					labelWidth: 175
				}]
			}, {
				xtype: 'textfield',
				name: 'uregex',
				fieldLabel: '~~uregex~~',
				hidden: true,			
				labelWidth: 150
			}, {
				xtype: 'textfield',
				name: 'utopic',
				fieldLabel: '~~utopic~~',
				emptyText: 'Q.PROBH17.PROBH.ALARMT.NOTI.NETWMR18',
				labelWidth: 150
			}, {
				xtype: 'textfield',
				name: 'ubusUri',
				fieldLabel: '~~ubusUri~~',
				emptyText: 'rvd://239.85.2.3:8523/omahft2:8523',
				labelWidth: 150
			}, {
				xtype: 'label',
				text: '~~uxmlQuery~~',
				margin: '10 0 10 0'
			}, {
				xtype: 'textareafield',
				name: 'uxmlQuery',
				fieldLabel: '',
				emptyText: 'tag1.subtag1 contains \'tagValue\' && tag1.subtag2 >= 0',
				labelWidth: 0
			}]
		})
	}
);