glu.defView('RS.gateway.SalesforceFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			// xtype:'fieldset',
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			defaults: {
				layout: {
					type: 'vbox',
					align: 'stretch'
				}
			},
			items: [{
				flex: 1,
				padding: '0px 10px 0px 0px',
				items: [{
					labelWidth: 150,
					xtype: 'combo',
					queryMode: 'local',
					valueField: 'uobject',
					displayField: 'uobject',
					name: 'uobject',
					store: '@{objectStore}'
				}]
			}, {
				flex: 1,
				padding: '0px 0px 0px 10px',
				defaultType: 'textfield',
				items: ['uquery']
			}]
		})
	}
);