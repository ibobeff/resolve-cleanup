glu.defView('RS.gateway.MessageFilter', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10px',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{title}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar actionBar-form',
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-eject rs-icon',
			text: ''
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}'
		}]
	}],
	items: [{
		xtype: 'fieldset',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},

		title: '~~generalSettingTitle~~',
		items: [{
			xtype: 'form',
			bodyPadding: '10px',
			flex: 1,
			defaultType: 'textfield',
			defaults: {
				labelWidth: 150
			},
			items: [{
				name: 'uname'
			}, {
				name: 'uorder'
			}, {
				name: 'ueventEventId'
			}]
		}, {
			xtype: 'form',
			bodyPadding: '10px',
			flex: 1,
			defaultType: 'textfield',
			items: [{
				name: 'uinterval'
			}, {
				name: 'urunbook'
			}, {
				xtype: 'checkboxfield',
				name: 'uactive',
				fieldLabel: '~~uactive~~'
			}]
		}]
	}, {
		xtype: 'fieldset',
		title: '~~additionalSettingTitle~~',
		padding: '10px 20px 10px 20px',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'textfield',
			labelWith: 150,
			name: 'uquery',
			fieldLabel: '~~uquery~~'
		}]
	}, {
		xtype: 'AceEditor',
		flex: 1,
		width: '100%',
		name: 'uscript',
		parser: 'groovy',
		title: 'Script'
	}]
});
