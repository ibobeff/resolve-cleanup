glu.defView('RS.gateway.DatabaseFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			// title:'~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				flex: 1,
				labelWidth: 150
			},
			items: [{
				xtype: 'container',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				defaults: {
					flex: 1
				},
				items: [{
					xtype: 'container',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					defaults: {
						labelWidth: 150
					},
					padding: '0px 10px 0px 0px',
					defaultType: 'textfield',
					items: ['ulastValueColumn', 'ulastValue']
				}, {
					xtype: 'container',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					padding: '0px 0px 0px 10px',
					defaultType: 'textfield',
					items: ['uprefix', {
						xtype: 'checkbox',
						hideLabel: true,
						boxLabel: '~~ulastValueQuote~~',
						padding: '0px 0px 0px 110px',
						name: 'ulastValueQuote'
					}]
				}]
			}, {
				xtype: 'combo',
				queryMode: 'local',
				valueField: 'upoolName',
				displayField: 'upoolName',
				name: 'upool',
				store: '@{poolStore}'
			}, {
				xtype: 'textarea',
				name: 'uupdateSql'
			}, {
				xtype: 'textarea',
				name: 'usql'
			}]
		})
	}
);