glu.defView('RS.gateway.XMPPFilter',
	function() {
		return RS.gateway.buildGatewayViewConfig({
			title: '~~additionalSettingTitle~~',
			bodyPadding: '10px 0px 0px 10px',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'textfield',
				labelWith: 150,
				name: 'uregex',
				fieldLabel: '~~uregex~~'
			}]
		})
	}
);