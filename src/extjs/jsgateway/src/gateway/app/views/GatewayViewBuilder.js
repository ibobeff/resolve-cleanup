RS.gateway.buildGatewayViewConfig = function(additional) {
	return {
		autoScroll: true,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		padding: 15,
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			ui: 'display-toolbar',
			cls : 'rs-dockedtoolbar',
			defaultButtonUI: 'display-toolbar-button',
			items: [{
				xtype: 'tbtext',
				cls: 'rs-display-name',
				text: '@{title}'
			}]
		}, {
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: [{
				name: 'back',
				iconCls: 'icon-large icon-reply-all rs-icon'
			}, {
				name: 'save',
				listeners: {
					doubleClickHandler: '@{saveAndExit}',
					render: function(button) {
						button.getEl().on('dblclick', function() {
							button.fireEvent('doubleClickHandler')
						});
						clientVM.updateSaveButtons(button);
					}
				}
			}, 'saveAs', '->', {
				xtype: 'sysinfobutton',
				sysId: '@{id}',
				sysCreated: '@{sysCreatedOn}',
				sysCreatedBy: '@{sysCreatedBy}',
				sysUpdated: '@{sysUpdatedOn}',
				sysUpdatedBy: '@{sysUpdatedBy}',
				sysOrg: '@{sysOrganizationName}',
				dateFormat: '@{userDateFormat}'
			}, {
				iconCls: 'x-tbar-loading',
				handler: '@{refresh}'
			}]
		}],
		items: [{
			xtype: 'panel',
			style : 'border-top :1px solid #cccccc;',
			padding : '10 0',
			// title:'~~generalSettingTitle~~',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			defaults: {
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				margin : '0 0 0 10',
			},
			items: [{
				xtype: 'panel',			
				defaultType: 'textfield',
				flex: 1,
				defaults: {
					labelWidth: 130
				},
				items: [{
					name: 'uname',
					readOnly: '@{unameIsReadOnly}',
					maxLength: 255,
					enforceMaxLength: true
				}, {
					name: 'uorder'
				}, {
					xtype: 'checkboxfield',
					name: 'uactive',
					hideLabel: true,
					padding: '0px 0px 0px 135px',
					boxLabel: '~~uactive~~',
					fieldLabel: '~~uactive~~'
				}]
			}, {
				xtype: 'panel',			
				flex: 1,				
				defaultType: 'textfield',
				defaults: {
					labelWidth: 130
				},	
				items: [{
					name: 'uinterval'
				}, {
					name: 'urunbook'
				}, {
					name: 'ueventEventId'
				}]
			}]
		},
		additional,
		{
			xtype: 'AceEditor',
			flex: 1,
			minHeight: 300,
			width: '100%',
			name: 'uscript',
			parser: 'groovy',
			title: 'Script'
		}]
	}
}

//FOR SDK
RS.gateway.buildSDKGatewayViewConfig = function(standardViewConfig, additionalSettingViewConfig) {
	return {
		autoScroll: true,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		padding: 15,
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			ui: 'display-toolbar',
			cls : 'rs-dockedtoolbar',
			defaultButtonUI: 'display-toolbar-button',
			items: [{
				xtype: 'tbtext',
				cls: 'rs-display-name',
				text: '@{title}'
			}]
		}, {
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: [{
				name: 'back',
				iconCls: 'icon-large icon-reply-all rs-icon'
			}, {
				name: 'save',
				listeners: {
					doubleClickHandler: '@{saveAndExit}',
					render: function(button) {
						button.getEl().on('dblclick', function() {
							button.fireEvent('doubleClickHandler')
						});
						clientVM.updateSaveButtons(button);
					}
				}
			}, 'saveAs', '->', {
				xtype: 'sysinfobutton',
				sysId: '@{id}',
				sysCreated: '@{sysCreatedOn}',
				sysCreatedBy: '@{sysCreatedBy}',
				sysUpdated: '@{sysUpdatedOn}',
				sysUpdatedBy: '@{sysUpdatedBy}',
				sysOrg: '@{sysOrganizationName}',
				dateFormat: '@{userDateFormat}'
			}, {
				iconCls: 'x-tbar-loading',
				handler: '@{refresh}'
			}]
		}],
		items: [
		standardViewConfig,
		additionalSettingViewConfig,
		{
			xtype: 'AceEditor',
			flex: 1,
			minHeight: 300,
			width: '100%',
			name: 'uscript',
			parser: 'groovy',
			title: 'Script'
		}]
	}
}
