/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.gateway').locale = {
	windowTitle: 'Gateway Project',
	uname: 'Name',
	uactive: 'Active',
	uorder: 'Order',
	uinterval: 'Interval',
	ueventEventId: 'Resolve Event ID',
	urunbook: 'Runbook',
	uquery: 'Query',
	ufolderNames: 'Folders to query',
	uonlyNew: 'Process Only New Emails',
	umarkAsRead: 'Mark retrieved emails as read',
	uscript: 'Script',
	usql: 'SQL',
	upool: 'Pool',
	uupdateSql: 'Update SQL',
	ulastValue: 'Last Value',
	ulastValueColumn: 'Last Value Column',
	uprefix: 'Prefix',
	ulastValueQuote: 'Last Value Quote',
	usubject: 'Subject',
	ucontent: 'Content',
	usendername: 'Sender Name',
	usenderaddress: 'Sender Address',
	uobject: 'Object',
	uincludeAttachment: 'Include Attachment',
	uformName: 'Form Name',
	ulastValueField: 'Last Value Field',
	utrapReceiver: 'Trap Receiver',
	usnmpTrapOid: 'Trap OID',
	usnmpVersion: 'Version',
	uipAddresses: 'IP Address',
	ureadCommunity: 'Read Community',
	utimeout: 'Timeout',
	uretries: 'Retries',
	uregex: 'Regex',
	ucomparator: 'Comparator',
	uvalue: 'Value',
	uoid: 'OID',
	uqueue: 'Queue',
	uemailAddress: 'Email Address',
	uemailPassword: 'Password',
	upoolName: 'Pool Name',
	udriverClass: 'Driver Class',
	u_db_us_ern_ame: 'Username',
	u_db_pas_sword: 'Password',
	udbconnectURI: 'Connection URI',
	udbmaxConn: 'Max Conn',
	usubnetMask: 'Subnet Mask',
	umaxConn: 'Max Conn',
	ufieldList: 'Field List',
    ulimit: 'Results Limit',
	uewsaddress: 'EWS Address',
	uewsp_assword: 'EWS Password',
	uxmppaddress: 'XMPP Address',
	uxmpppassword: 'XMPP Password',
	utargetQueue: 'Queue/Exchange',
	uexchange: 'Exchange',
	uport: 'Port',
	ussl: 'SSL',
	u_bas_icAu_thUs_ername: 'Username',
	u_bas_icAu_thPas_sword: 'Password',

	uuri: 'URI',
	ublocking : 'Block Call',
	ubeginSeparator: 'Begin Separator',
	uendSeparator: 'End Separator',
	uincludeBeginSeparator: 'Include Begin Separator',
	uincludeEndSeparator: 'Include End Separator',
	utopic: 'Topic',
	ubusUri: 'Bus URI',
	ucertifiedMessaging: 'Certified Messaging',
	uxmlQuery: 'Xml Query',
	uunescapeXml: 'Unescape Xml',
	uprocessAsXml: 'Process As Xml',
	urequireReply: 'Save message for response?',
	ureplyTimeout: 'Message expiration (ms)',
	uurlQuery : 'URL Query',
	uxmlQuery : 'XML Query',

	Gateway: 'Gateway',
	cancel: 'Cancel',
	back: 'Back',
	ServerErrMsg: 'The server currently cannot handle the request.',
	sysCreatedOn: 'Created On',
	sysCreatedBy: 'Created By',
	sysUpdatedOn: 'Updated On',
	sysUpdatedBy: 'Updated By',
	sysOrganizationName: 'Organizaion',
	sysId: 'Sys ID',

	//for main
	emailFilterFormTitle: 'Email Filter',
	emailFilterDisplayName: 'Email Gateway Filter',
	databaseFilterDisplayName: 'Database Gateway Filter',
	exchangeFilterDisplayName: 'Exchange Gateway Filter',
	tsrmFilterDisplayName: 'TSRM Gateway Filter',
	netcoolFilterDisplayName: 'Netcool Gateway Filter',
	ewsFilterDisplayName: 'EWS Gateway Filter',
	remedyxFilterDisplayName: 'Remedyx Gateway Filter',
	salesforceFilterDisplayName: 'Salesforce Gateway Filter',
	hpomFilterDisplayName: 'HPOM Gateway Filter',
	hpsmFilterDisplayName: 'HPSM Gateway Filter',
	serviceNowFilterDisplayName: 'ServiceNow Gateway Filter',
	snmpFilterDisplayName: 'SNMP Gateway Filter',
	xmppFilterDisplayName: 'XMPP Gateway Filter',
	amqpFilterDisplayName: 'AMQP Gateway Filter',
	httpFilterDisplayName: 'HTTP Gateway Filter',
	tcpFilterDisplayName: 'TCP Gateway Filter',
	CASpectrumFilterDisplayName : 'CA Spectrum Gateway Filter',

	emailAddressDisplayName: 'Email Address',
	databaseConnectionPoolDisplayName: 'Database Connection Pool',
	telnetPoolDisplayName: 'Telnet Connection Pool',
	remedyxFormDisplayName: 'Remedyx Form',
	ewsAddressFilterDisplayName: 'EWS Address',
	sshPoolDisplayName: 'SSH Connection Pool',
	xmppAddressDisplayName: 'XMPP Address',
	tibcoBespokeFilterDisplayName: 'TIBCO Filter',

	ListFiltersErrMsg: 'Cannot get filters from the server.',
	createFilter: 'New',
	deleteFilters: 'Delete',
	filterFieldSetTitle: 'Filters',
	gatewayFieldSetTitle: 'Deployment Information',
	emailTitle: 'Email Gateway',
	gateways: 'Available Gateways',
	showGatewayList: 'Delete Gateway',
	showFilterList: 'Add',
	updateDeployedFilters: 'Update Filters',
	deployFilters: 'Deploy All',
	AddFiltersToGatewayErrMsg: 'Filters {name} is already added to the gateway.',
	ListGatewaysErrMsg: 'Cannot get getways from server',
	deleteFiltersTitle: 'Delete Filters',
	deleteFilterRecs: 'Are you sure you want to delete the selected filter?({num} items selected)',
	deleteFilterRec: 'Are you sure  you want to delete the selected filter?',
	confirmDeleteFilters: 'Delete',
	DeleteFilterSucMsg: 'The selected filters have been deleted',
	ListDeployedFiltersErrMsg: 'Cannot get deployed filters from the server.',
	DeployTitle: 'Deploy Filters',
	DeploysMsg: 'Are you sure you want to deploy all the filters?',
	confirmDeploy: 'Deploy',
	LoadGatewaysErrMsg: 'Cannot get gateways from the server.',
	NullGatewayError: 'Please select a gateway first.',
	DeployedFilterDuplication: 'Filter {name} has already been added to the gateway.',
	DeploySucMsg: 'The filters have been deployed.',
	DeployErrMsg: 'Cannot deploy the filters',
	undeployFilters: 'Undeploy Filters',
	UndeployFilters: 'Undeploy Filters',
	UndeployFilterMsg: 'Are you sure you want to undeploy the selected item? ',
	UndeployFiltersMsg: 'Are you sure you want to undeploy the selected items?({num} item selected)',
	UndeployFilterErrMsg: 'Cannot deploy the selected filters.',
	UndeployFilterSucMsg: 'The selected filters have been undeployed.',
	confirmUndeploy: 'Undeploy',
	markDeletedDeployedFilters: 'Delete',
	whyUCannotDeleteIt: 'Items:[{names}]  have been deployed',



	save: 'Save',
	saveAs: 'Save As',
	generalSettingTitle: 'General Setting',
	additionalSettingTitle: 'Additional Setting',
	SaveSucMsg: 'The filter has been saved.',
	SaveErrMsg: 'Cannot save the filter.',
	invalidName: 'Name is required and must be in alphanumerics, but may contain single spaces or underscores \'_\' between alphanumeric characters.',
	invalidObject: 'The object cannot be empty.',
	invalidOrder: 'The order should be an integer.',
	ServerErrMsg: 'Server Error',
	ServerErrTitle: 'Server cannot handle the request.',
	GetFilterErrMsg: 'Cannot get the filter from the server.',
	uscript: 'Script',
	invalidAddress: 'The address is required.',
	invalidField: 'The field cannot be empty.',

	dragText: 'Drop it to the gateway grid to add this filter to the gateway.',
	MessageFilter: {
		invalidQuery: 'The query cannot be empty'
	},

	Filters: {
		addFilters: 'Add Filters To Gateway',
		cancel: 'Cancel'
	},

	BaseFilter: {
		invalidName: 'The name cannot be empty.'
	},

	Gateways: {
		deleteGateways: 'Delete',
		cancel: 'Cancel',
		deleteGatewaysTitle: 'Delete Gateways',
		deleteGatewaysMsg: 'Are you sure you want to delete the selected gateways?({num} items selected)',
		deleteGatewayMsg: 'Are you sure you want to delete the selected gateways?',
		confirmDeleteGateway: 'Delete',
		ServerErrTitle: 'Server Error',
		ServerErrMsg: 'The server currently cannot handle the request.',
		confirmAjaxException: 'OK',
		ListGatewaysErrTitle: 'Get Gateways Error',
		ListGatewaysErrMsg: 'Cannot get getways from server'
	},

	EmailFilter: {
		emailFilterTitle: 'Email Filter',
		invalidQuery: 'The query cannot be empty'
	},

	NetcoolFilter: {
		netcoolFilterTitle: 'Netcool Filter'
	},

	DatabaseFilter: {
		databaseFilterTitle: 'Database Filter',
		invalidMaxConn: 'the max conn should be a integer'
	},

	ExchangeFilter: {
		exchangeFilterTitle: 'Exchange Filter'
	},

	TSRMFilter: {
		tsrmFilterTitle: 'TSRM Filter'
	},

	EWSFilter: {
		ewsFilterTitle: 'EWS Filter'
	},

	RemedyxFilter: {
		remedyxFilterTitle: 'Remedyx Filter'
	},

	SalesforceFilter: {
		salesforceFilterTitle: 'Salesforce Filter'
	},

	HPOMFilter: {
		hpomFilterTitle: 'HPOM Filter'
	},

	HPSMFilter: {
		hpsmFilterTitle: 'HPSM Filter'
	},

	ServiceNowFilter: {
		serviceNowFilterTitle: 'ServiceNow Filter'
	},

	CASpectrumFilter: {
		CASpectrumFilterTitle: 'CA Spectrum Filter'
	},

	SNMPFilter: {
		snmpFilterTitle: 'SNMP Filter',
		invalidTimeout: 'Timeout should be a integer.',
		invalidVersion: 'Version should be a integer.',
	},

	XMPPFilter: {
		xmppFilterTitle: 'XMPP Filter'
	},

	TIBCOBespokeFilter: {
		tibcoBespokeFilterTitle: 'TIBCO Filter'
	},

	AMQPFilter: {
		amqpFilterTitle: 'AMQP Filter'
	},

	HTTPFilter: {
		httpFilterTitle: 'HTTP Filter',
		defaultBlockCall : 'Default',
		gatewayBlockCall : 'Gateway Script',
		worksheetBlockCall : 'Worksheet ID',
		runbookBlockCall : 'Execution Complete',
		blockCallTooltip :  '<b>Default</b> : Return immediately after receiving the message.<br>' +
							'<b>Gateway Script</b> : Return after execution of gateway script. The gateway script can set the additional response.<br>' +
							'<b>Worksheet ID</b> :  Return after getting the worksheet ID. If gateway script is executed, gateway script can set additional response.<br>' +
							'<b>Execution Complete</b> : Return after the runbook complete execution, return worksheet ID, and execution condition and severity<br>' +
							'<b>Filtered Response</b> : Returns after the Runbook execution, returns the RESULT.detail from "httpfilter_response" action task of the ' +
							'  Runbook executed, If the "httpfilter_response" is not part of the Runbook please include it',
		filteredResponseBlockCall: 'Filtered Response',
		contentType: 'Content Type',
		contentTypeJSON: 'application/json',
		contentTypeDefault: 'Default',
		requestFilter: 'Response Filter'
	},

	TCPFilter: {
		tcpFilterTitle: 'TCP Filter'
	},

	EmailAddress: {
		emailAddressTitle: 'Email Address'
	},

	EmailAddressMain: {
		dragText: 'Drop it to the gateway grid to add this address to the gateway.',
		DeploySucMsg: 'The addresses have been deployed.',
		DeploysMsg: 'Are you sure you want to deploy all the addresses?'
	},

	DatabaseConnectionPool: {
		databaseConnectionPoolTitle: 'Database Connection Pool',
		showGenerator: 'URI Generator',
		generateURI: 'Generate URI ',
		dbType: 'Database Type',
		ipAddress: 'Host/IP Address',
		port: 'Port',
		dbName: 'Database Name',
		hideGenerator: 'Hide',
		howToGetURI: 'Get URI from',
		sid: 'SID',
		service: 'Service'
	},

	DatabaseConnectionPoolMain: {
		dragText: 'Drop it to the gateway grid to add this connection to the gateway.',
		DeploySucMsg: 'The connections have been deployed.',
		DeploysMsg: 'Are you sure you want to deploy all the connections?'
	},

	TelnetPool: {
		telnetPoolTitle: 'Telnet Pool',
		invalidSubnetMask: 'Invalid Subnet Mask format.example:255.255.255.0',
		invalidMaxConn: 'Max connection should be an integer',
		invalidTimeout: 'Timeout should be a integer.'
	},

	TelnetPoolMain: {
		dragText: 'Drop it to the gateway grid to add this connection to the gateway.',
		DeploySucMsg: 'The connections have been deployed.',
		DeploysMsg: 'Are you sure you want to deploy all the connection?'
	},

	RemedyxForm: {
		remedyxFormTitle: 'Remedyx Form',
		ufieldList: 'Field List'
	},

	RemedyxFormMain: {
		dragText: 'Drop it to the gateway grid to add this form to the gateway.',
		DeploySucMsg: 'The forms have been deployed.',
		DeploysMsg: 'Are you sure you want to deploy all the connections?'
	},

	EWSAddress: {
		ewsAddressTitle: 'EWS Address'
	},

	EWSAddressMain: {
		dragText: 'Drop it to the gateway grid to add this address to the gateway.',
		DeploySucMsg: 'The addresses have been deployed.',
		DeploysMsg: 'Are you sure you want to deploy all the connections?'
	},

	SSHPool: {
		sshPoolTitle: 'SSH Pool',
		invalidSubnetMask: 'Invalid Subnet Mask format.example:255.255.255.0',
		invalidMaxConn: 'Max connection should be an integer',
		invalidTimeout: 'Timeout should be a integer.'
	},

	SSHPoolMain: {
		dragText: 'Drop it to the gateway grid to add this connection to the gateway.',
		DeploySucMsg: 'The connections have been deployed.',
		DeploysMsg: 'Are you sure you want to deploy all the connections?'
	},

	XMPPAddress: {
		xmppAddressTitle: 'XMPP Address'
	},

	XMPPAddressMain: {
		dragText: 'Drop it to the gateway grid to add this address to the gateway.',
		DeploySucMsg: 'The addresses have been deployed.',
		DeploysMsg: 'Are you sure you want to deploy all the connections?'
	},
	name: 'Name',
	SaveAs: {
		saveAsNewFilterTitle: 'Save As New',
		saveAsNewFilterMsg: 'Enter the new {name}.',
		close: 'Close'
	}
}