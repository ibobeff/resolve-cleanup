RS.gateway.serviceNow = {
	modelName: 'serviceNow',
	filterViewName: 'RS.gateway.ServiceNowFilter',
	requestParams: {
		modelName: 'ServiceNowFilter',
		queueName: 'SERVICENOW',
		gatewayName: 'SERVICENOW',
		gatewayClass: 'MServiceNow',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uobject~~',
		dataIndex: 'uobject',
		filterable: true,
		flex: 1
	}, {
		header: '~~uquery~~',
		dataIndex: 'uquery',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'uobject',
		type: 'string'
	}, {
		name: 'uquery',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('serviceNowFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('serviceNowGatewayDisplayName'));
		this.set('title', this.localize('serviceNowTitle'));
	}
};

glu.defModel('RS.gateway.ServiceNowMain', {
	mixins: ['Main'],
	gatewaytype: 'serviceNow'
});