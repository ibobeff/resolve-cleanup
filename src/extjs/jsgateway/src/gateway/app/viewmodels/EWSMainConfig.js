RS.gateway.ews = {
	modelName: 'ews',
	filterViewName: 'RS.gateway.EWSFilter',
	requestParams: {
		modelName: 'EWSFilter',
		queueName: 'EWS',
		gatewayName: 'EWS',
		gatewayClass: 'MEWS',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uquery~~',
		dataIndex: 'uquery',
		filterable: true,
		flex: 1
	}, {
		header: '~~uincludeAttachment~~',
		dataIndex: 'uincludeAttachment',
		filterable: true,
		flex: 1
	},	{
		header: '~~uonlyNew~~',
		dataIndex: 'uonlyNew',
		filterable: true,
		flex: 1
	},	{
		 header: '~~ufolderNames~~',
		 dataIndex: 'ufolderNames',
		 filterable: true,
		 flex: 1
	},	{
		 header: '~~umarkAsRead~~',
		 dataIndex: 'umarkAsRead',
		 filterable: true,
		 flex: 1
	}],

	additionalStoreFields: [{
		name: 'uquery',
		type: 'string'
	}, {
		name: 'uincludeAttachment',
		type: 'string'
	}, {
		name: 'ufolderNames',
	}, {
		name: 'umarkAsRead',
		type: 'string'
	},{
		name: 'uonlyNew',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('ewsFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('ewsGatewayDisplayName'));
		this.set('title', this.localize('ewsTitle'));
	}
};

glu.defModel('RS.gateway.EWSMain', {
	mixins: ['Main'],
	gatewaytype: 'ews'
});