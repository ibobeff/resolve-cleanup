glu.defModel('RS.gateway.TSRMFilter', {
	uobject: '',
	uquery: '',
	modelName: 'TSRMFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'tsrm',
	fields: RS.gateway.commonFields.concat(['uquery', 'uobject']),
	objectStore: {
		mtype: 'store',
		fields: ['uobject'],
		proxy: {
			type: 'memory'
		}
	},
	activate: function(screen, params) {
		this.activateBase(params);
		this.loadObjects();
	},
	init: function() {
		this.initBase();
		this.set('title', this.localize('tsrmFilterTitle'));
	},

	loadObjects: function() {
		this.ajax({
			url: '/resolve/service/gateway/listObjectNames',
			params: {
				gatewayName: 'TSRM'
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('LoadObjErr') + '[' + respData.message + ']');
					return;
				}
				var objs = respData.records || {};
				this.objectStore.removeAll();
				Ext.each(objs, function(o) {
					this.objectStore.add({
						uobject: o
					});
				}, this);
				if (this.uobject)
					this.set('uobject', this.objectStore.first().get('uobject'));
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	}
});