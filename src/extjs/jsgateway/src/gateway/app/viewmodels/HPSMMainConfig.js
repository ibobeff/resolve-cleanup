RS.gateway.hpsm = {
	modelName: 'hpsm',
	filterViewName: 'RS.gateway.HPSMFilter',
	requestParams: {
		modelName: 'HPSMFilter',
		queueName: 'HPSM',
		gatewayName: 'HPSM',
		gatewayClass: 'MHPSM',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uobject~~',
		dataIndex: 'uobject',
		filterable: true,
		flex: 1
	}, {
		header: '~~uquery~~',
		dataIndex: 'uquery',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'uobject',
		type: 'string'
	}, {
		name: 'uquery',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('hpsmFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('hpsmGatewayDisplayName'));
		this.set('title', this.localize('hpsmTitle'));
	}
};

glu.defModel('RS.gateway.HPSMMain', {
	mixins: ['Main'],
	gatewaytype: 'hpsm'
});