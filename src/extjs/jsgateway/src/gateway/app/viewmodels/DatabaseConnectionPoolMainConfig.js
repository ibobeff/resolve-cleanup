RS.gateway.databaseConnectionPool = {
	modelName: 'databaseConnectionPool',
	uniqueIdField: 'upoolName',
	filterViewName: 'RS.gateway.DatabaseConnectionPool',
	requestParams: {
		modelName: 'DatabaseConnectionPool',
		queueName: 'DATABASE',
		gatewayName: 'DATABASE',
		gatewayClass: 'MDatabase',
		getterMethod: 'getPools',
		clearAndSetMethod: 'clearAndSetPools',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},

	concatColumns: function() {
		this.set('filterColumns', [{
			header: '~~upoolName~~',
			dataIndex: 'upoolName',
			filterable: true,
			flex: 1
		}, {
			header: '~~udriverClass~~',
			dataIndex: 'udriverClass',
			filterable: true,
			flex: 1
		}, {
			header: '~~u_db_us_ern_ame~~',
			dataIndex: 'u_db_us_ern_ame',
			filterable: true,
			flex: 1
		}, {
			header: '~~udbconnectURI~~',
			dataIndex: 'udbconnectURI',
			filterable: true,
			flex: 1
		}, {
			header: '~~udbmaxConn~~',
			dataIndex: 'udbmaxConn',
			filterable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()));
	},

	config: function() {
		this.set('filterDisplayName', this.localize('databaseConnectionPoolDisplayName'));
		this.set('title', this.localize('databaseConnectionPoolTitle'));
	},

	getStoreFields: function() {
		if (!this.gatewaytype)
			alert('Need to specify a gatewaytype!');
		var fields = [
			'id',
			'upoolName',
			'udriverClass',
			'u_db_us_ern_ame',
			'udbconnectURI',
			'udbmaxConn'
		].concat(RS.common.grid.getSysFields());
		return fields;
	},

	createFilter: function() {
		this.open({
			mtype: this.filterViewName
		});
	},
	editFilter: function(id) {
		this.open({
			mtype: this.filterViewName,
			uniqueIdField: this.uniqueIdField,
			gatewaytype: this.gatewaytype,
			id: id
		});
	}
};

glu.defModel('RS.gateway.DatabaseConnectionPoolMain', {
	mixins: ['Main'],
	gatewaytype: 'databaseConnectionPool'
});