glu.defModel('RS.gateway.Main', {
	/*-------------Gateway specific fields start----------------*/
	modelName: null,
	filterViewName: null,
	uniqueIdField: 'uname',
	requestParams: null,
	additionalColumns: null,
	additionalStoreFields: null,
	config: null,
	/*-------------Gateway specific fields end----------------*/
	renderer: function(value, row, record, extraRenderer) {
		/*if (record.get('isDeleted') && record.get('uqueue') != '<|Default|Queue|>')
			row.style = 'background-color:red !important';
		if (record.get('uqueue') === '<|Default|Queue|>' && !record.get('isMaster'))
			row.style = 'background-color:#25E01B !important';
		if (record.get('uqueue') != '<|Default|Queue|>' && record.get('changed') && !record.get('isDeleted'))
			row.style = 'background-color:#FFEE00 !important';
		return (extraRenderer ? extraRenderer(value) : value);*/
	},

	chainRenderer: function(extraRenderer) {
		return function(value, row, record) {
			/*if (record.get('isDeleted') && record.get('uqueue') != '<|Default|Queue|>')
				row.style = 'background-color:red !important';
			if (record.get('uqueue') === '<|Default|Queue|>' && !record.get('isMaster'))
				row.style = 'background-color:#25E01B !important';
			if (record.get('uqueue') != '<|Default|Queue|>' && record.get('changed') && !record.get('isDeleted'))
				row.style = 'background-color:#FFEE00 !important';
			return (extraRenderer ? extraRenderer(value) : value);*/
		}
	},

	reqCount: 0,

	mock: false,

	gatewaytype: '',

	title: '',

	id: '',

	filterStateId: 'gatewayFilters',

	gatewayStateId: 'gatewayQueues',

	filterDisplayName: '',

	gatewayDisplayName: '',

	filterFieldSetTitle: '',

	gatewayFieldSetTitle: '',

	filters: null,

	gatewaysStore: {
		mtype: 'store',
		fields: ['uqueue'],
		proxy: {
			type: 'memory'
		}
	},
	gateway: null,
	deployedFilters: null,
	filtersSelections: [],
	deployedFiltersSelections: [],
	requestParams: null,
	filterColumns: null,
	deployedColumns: null,

	activate: function(screen, params) { //Rs client will trigger this event when this gateway comes into view.
		window.document.title = this.localize('windowTitle');
		this.loadFilters();
		this.loadGateways();
		this.loadDeployedFilters();
	},

	isDeployedFiltersEmpty: true,

	init: function() {
		if (this.mock) this.backend = RS.gateway.createMockBackend(true);

		//Apply Gateway config into main config model. For SDK those attributes are already applied.
		if(!this.SDK){
			for (attr in RS.gateway[this.gatewaytype]) {
				this[attr] = RS.gateway[this.gatewaytype][attr];
			}
		}
		this.set('filterFieldSetTitle', this.localize('filterFieldSetTitle'));
		this.set('gatewayFieldSetTitle', this.localize('gatewayFieldSetTitle'));

		//Setting up store for filters and columns (grid)
		this.initFilterStore();
		this.initDeployedFilterStore();
		this.initFilterColumns();
		this.initDeployedFilterColumns(); //Not sure why we need this?

		//Setting culumns for deployment section using columns from filters.
		var dc = [];
		Ext.each(this.filterColumns, function(f) {
			var c = {};
			Ext.apply(c, f);
			if (f.dataIndex == 'sysCreatedOn' || f.dataIndex == 'sysUpdatedOn')
				c.renderer = (function(self) {
					return function(value, row, record) {
						var v = Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())(value);
						//self.renderer(v, row, record);
						return v;
					}
				})(this)		
			dc.push(c);
		}, this);
		this.set('deployedColumns', dc);

		//Call config function of this gateway. Mostly apply display name of the gateway to appropriate field.
		this.config();

		//Setting up params for filter and deployed filter before load.
		this.deployedFilters.on('beforeload', function(store, operations) {
			operations.params = operations.params || {};
			Ext.apply(operations.params, this.requestParams);
			operations.params['queueName'] = this.gateway;
		}, this);

		this.filters.on('beforeload', function() {
			this.set('reqCount', this.reqCount + 1);
		}, this);

		//Handle call back data.
		this.filters.on('load', function(records, op, suc) {
			this.set('reqCount', this.reqCount - 1);
			if (!suc) {
				//clientVM.displayError(this.localize('ListFiltersErrMsg'));
				return;
			}

		}, this)
		this.deployedFilters.on('datachanged', function() {
			this.set('numOfDeployedFilters', this.deployedFilters.getCount());
		}, this);
	},
	getStoreFields: function() {
		if (!this.additionalStoreFields)
			alert('Need to specify a gatewaytype!');
		var fields = [{
			name: 'id',
			type: 'string'
		}, {
			name: 'uqueue',
			type: 'string'
		}, {
			name: 'uniqueId',
			type: 'string'
		}, {
			name: 'changed',
			type: 'bool',
			defaultValue: false
		}, {
			name: 'isDeleted',
			type: 'bool',
			defaultValue: false
		}, {
			name: 'isMaster',
			type: 'bool',
			defaultValue: true
		}].concat(this.additionalStoreFields).concat(RS.common.grid.getSysFields());
		if(!this.SDK){
			fields = fields.concat([{
				name: 'uname',
				type: 'string'
			}, {
				name: 'uactive',
				type: 'bool'
			}, {
				name: 'uorder',
				type: 'int'
			}, {
				name: 'uinterval',
				type: 'int'
			}, {
				name: 'ueventEventId',
				type: 'string'
			}, {
				name: 'urunbook',
				type: 'string'
			}])
		}
		return fields;
	},

	initFilterStore: function() {
		var fields = this.getStoreFields();
		var filters = Ext.create('Ext.data.Store', {
			fields: fields,
			proxy: {
				type: 'ajax',
				extraParams: this.requestParams,
				url: '/resolve/service/gateway/listFilters',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});
		this.set('filters', filters);
	},

	initDeployedFilterStore: function() {
		var fields = this.getStoreFields();
		var deployedFilters = Ext.create('Ext.data.Store', {
			fields: fields,
			proxy: {
				type: 'ajax',
				extraParams: this.requestParams,
				url: '/resolve/service/gateway/listDeployedFilters',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});

		this.set('deployedFilters', deployedFilters);
	},

	concatColumns: function() {
		//Concatinate all defauls column + sys column + additional columns for this gateway
		var filterColumns = this.SDK ? [] : [{
			header: '~~uname~~',
			dataIndex: 'uname',
			filterable: true,
			flex: 1
		}, {
			header: '~~uactive~~',
			dataIndex: 'uactive',
			filterable: true,
			width: 70,
			align : 'center'
		}, {
			header: '~~uorder~~',
			dataIndex: 'uorder',
			filterable: true,
			width: 70,
			align : 'center'
		}, {
			header: '~~uinterval~~',
			dataIndex: 'uinterval',
			filterable: true,
			width: 100,
			align : 'center'
		}, {
			header: '~~ueventEventId~~',
			dataIndex: 'ueventEventId',
			filterable: true,
			flex: 1
		}, {
			header: '~~urunbook~~',
			dataIndex: 'urunbook',
			filterable: true,
			flex: 2
		}];
		this.set('filterColumns', filterColumns.concat(this.additionalColumns).concat(RS.common.grid.getSysColumns()));
	},

	initFilterColumns: function() {
		this.concatColumns();
		Ext.each(this.filterColumns, function(f) {
			Ext.each(this.getStoreFields(), function(sf) {
				if (f.dataIndex == sf.name)
					switch (sf.type) {
						case 'date':
							f.renderer = Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat());
							break;
						case 'bool':
							f.renderer = RS.common.grid.booleanRenderer()
							break;
					}
			}, this);
		}, this);
	},

	initDeployedFilterColumns: function() {
		Ext.each(this.deployedColumns, function(f) {
			Ext.each(this.getStoreFields(), function(sf) {
				if (f.dataIndex == sf.name)
					switch (sf.type) {
						case 'date':
							f.renderer = (function(self) {
								return function(value, row, record) {
									var v = Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())(value);
									//v = self.renderer(v, row, record);
									return v;
								}
							})(this);
							break;
						case 'bool':
							f.renderer = (function(self) {
								return function(value, row, record) {
									var v = RS.common.grid.booleanRenderer()(value);
									//v = self.renderer(v, row, record);
									return v;
								}
							})(this);
							break;
					}
			}, this);
		}, this);
	},

	loadFilters: function() {
		// this.set('reqCount',this.reqCount + 1);
		this.filters.load();
	},

	createFilter: function() {
		var config = {	modelName: this.filterViewName };
		if(this.SDK == true){
			Ext.apply(config,{
				params : {
					name : this.gatewaytype
				}
			})
		};
		clientVM.handleNavigation(config);
	},

	deleteFilters: function() {
		this.message({
			title: this.localize('deleteFiltersTitle'),
			msg: this.localize(this.filtersSelections.length > 1 ? 'deleteFilterRecs' : 'deleteFilterRec', {
				num: this.allSelected ? this.filters.getTotalCount() : this.filtersSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirmDeleteFilters'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.sendDeleteFiltersReq
		});
	},

	createFilterIsEnabled$: function() {
		return this.reqCount == 0;
	},

	whyUCannotDeleteIt: null,
	/*deleteFiltersIsEnabled$: function() {
		var uniqueIds = [];
		Ext.each(this.filtersSelections, function(f) {
			if (-1 != this.deployedFilters.find(this.uniqueIdField, f.get(this.uniqueIdField)))
				uniqueIds.push(f.get(this.uniqueIdField));
		}, this);
		if (uniqueIds.length > 0)
			this.set('whyUCannotDeleteIt', this.localize('whyUCannotDeleteIt', {
				names: uniqueIds
			}));
		else
			this.set('whyUCannotDeleteIt', null);
		return this.reqCount == 0 && this.filtersSelections.length > 0 && !this.whyUCannotDeleteIt;
	},*/

	sendDeleteFiltersReq: function(btn) {
		if (btn === 'no')
			return;
		var str = [];
		this.set('reqCount', this.reqCount + 1);
		Ext.each(this.filtersSelections, function(r) {
			str.push(r.get('id'));
		});
		var params = {};
		Ext.apply(params, this.requestParams);
		params.ids = str;
		params.all = this.allSelected;
		this.ajax({
			url: '/resolve/service/gateway/deleteFilters',
			params: params,
			scope: this,
			success: function(resp) {
				this.handleDeleteFiltersSuccess(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
		});
	},

	handleDeleteFiltersSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(respData.message);
		else
			clientVM.displaySuccess(this.localize('DeleteFilterSucMsg'));
		this.loadFilters();
		this.loadDeployedFilters();
	},

	editFilter: function(id) {
		var config = {	modelName: this.filterViewName };
		if(this.SDK == true){
			Ext.apply(config,{
				params : {
					name : this.gatewaytype,
					id : id
				}
			})
		}
		else
		{
			Ext.apply(config,{
				params: {
					gatewaytype: this.gatewaytype,
					id: id
				}
			})
		}
		clientVM.handleNavigation(config);
	},
	/*------------------------------------------------gateway impl start-------------------------------*/
	loadGateways: function() {
		this.set('reqCount', this.reqCount + 1);
		this.ajax({
			url: '/resolve/service/gateway/listGateways',
			method: 'GET',
			params: this.requestParams,
			success: function(resp) {
				this.handleLoadGatewaysSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
		});
		this.deployedFilters.removeAll();
		this.set('deployedFiltersSelections', []);
	},

	handleLoadGatewaysSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('LoadGatewaysErrMsg') + '[' + respData.message + ']');
			return;
		}
		var queues = respData.records;
		this.gatewaysStore.loadData([], false);
		for (var idx = 0; idx < queues.length; idx++) {
			this.gatewaysStore.add({
				uqueue: queues[idx]
			});
		}

		if (queues.length > 0)
			this.set('gateway', queues[0]);
	},

	loadDeployedFilters: function() {
		if (!this.gateway)
			return;
		this.set('reqCount', this.reqCount + 1);
		this.deployedFilters.load({
			scope: this,
			callback: function(records, op, suc) {
				this.set('reqCount', this.reqCount - 1);
				if (!suc) {
					//clientVM.displayError(this.localize('ListDeployedFiltersErrMsg'));
					return;
				}
			}
		});
	},

	// updateDeployedFilters:function(){
	// 	this.message({
	// 		title:this.localize('updateTitle'),
	// 		msg:this.localize('updateMsg'),
	// 		buttons:Ext.MessageBox.YESNO,
	// 		buttonText:{
	// 			yes:this.localize('confirmUpdate'),
	// 			no:this.localize('cancel')
	// 		},
	// 		scope:this,
	// 		fn:this.sendUpdateFiltersReq
	// 	})
	// },

	// sendUpdateFiltersReq:function(btn){
	// 	if(btn==='no')return;

	// 	this.set('reqCount',this.reqCount + 1);
	// 	var ids = [];
	// 	for(idx in this.deployedFiltersSelections){
	// 		ids.push(this.deployedFiltersSelections[idx].data.id);
	// 	}

	// 	this.ajax({
	// 		url:'/resolve/service/gateway/updateDeployedFilters',
	// 		jsonData:ids,
	// 		success:function(resp){
	// 			this.set('reqCount',this.reqCount - 1);
	// 			this.handleUpdateReqSuccess(resp);
	// 		},
	// 		failure:function(resp){
	// 			this.set('reqCount',this.reqCount - 1);
	// 			clientVM.displayFailure(resp);
	// 		}
	// 	});
	// },

	handleUpdateReqSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('updateFailureMsg') + '[' + respData.message + ']');
			return;
		}
		this.loadDeployedFilters();
	},

	undeployFilters: function() {
		this.message({
			title: this.localize('UndeployFilters'),
			msg: this.localize(this.deployedFiltersSelections.length > 0 ? 'UndeployFiltersMsg' : 'UndeployFilterMsg', {
				num: this.deployedFiltersSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirmUndeploy'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.sendUndeployFiltersReq
		})
	},

	sendUndeployFiltersReq: function(btn) {
		if (btn === 'no')
			return;

		var names = [];

		for (var idx = 0; this.deployedFiltersSelections.length; idx++)
			names.push(this.deployedFiltersSelections[idx].get(this.uniqueIdField));

		var params = {};
		Ext.apply(params, this.requestParams);
		params.filterNames = names;

		this.set('reqCount', this.reqCount + 1);
		this.ajax({
			url: '/resolve/service/gateway/undeployFilters',
			params: params,
			scope: this,
			success: function(resp) {
				this.handleUndeployFiltersSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
		})
	},

	handleUndeployFiltersSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(this.localize('UndeployFilterErrMsg') + '[' + respData.message + ']');
		else
			clientVM.displaySuccess(this.localize('UndeployFilterSucMsg'));
		this.loadDeployedFilters();
	},

	showFilterList: function() {
		this.open({
			mtype: 'RS.gateway.Filters'
		});
	},

	addFiltersToGateway: function(data, dropRec, dropPosition, dropHandlers) {
		if (this.reqCount > 0) {
			dropHandlers.cancelDrop();
			return;
		}

		if (!this.gateway) {
			clientVM.displayError(this.localize('NullGatewayError'));
			dropHandlers.cancelDrop();
			return;
		}

		var records = this.duplicationFilter(data.records);
		data.records = records;
	},

	addFilterFromWindow: function(records) {
		if (this.reqCount > 0)
			return;

		if (!this.gateway) {
			clientVM.displayError(this.localize('NullGatewayError'));
			return;
		}
		records = this.duplicationFilter(records);
		this.deployedFilters.add(records);
	},

	duplicationFilter: function(records) {
		var result = [];
		Ext.each(records, function(r, index) {
			var dup = false;
			this.deployedFilters.each(function(sr) {
				if (sr.get(this.uniqueIdField) === r.get(this.uniqueIdField))
					dup = true;
			}, this);
			if (!dup) {
				r.set('isMaster', false);
				result.push(r);
			}

		}, this);
		return result;
	},

	handleAddFiltersToGatewaySuccessReq: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('AddFiltersToGatewayErrMsg') + '[' + respData.message + ']');
			this.loadDeployedFilters();
			return;
		}

	},

	deployFilters: function() {
		this.message({
			title: this.localize('DeployTitle'),
			msg: this.localize('DeploysMsg'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirmDeploy'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.sendDeployFiltersReq
		});
	},

	sendDeployFiltersReq: function(btn) {
		if (btn === 'no')
			return;

		var names = null;
		var deleteFilterNames = null;
		this.deployedFilters.each(function(r) {
			if (r.get('isDeleted')) {
				if (!deleteFilterNames)
					deleteFilterNames = [];
				deleteFilterNames.push(r.get(this.uniqueIdField));
			} else {
				if (!names)
					names = [];
				names.push(r.get(this.uniqueIdField));
			}

		}, this);
		var p = {
			filterNames: names,
			deleteFilterNames: deleteFilterNames
		};
		Ext.applyIf(p, this.requestParams);
		p.queueName = this.gateway || p.queueName;
		this.set('reqCount', this.reqCount + 1);
		this.ajax({
			url: '/resolve/service/gateway/deployFilters',
			params: p,
			scope: this,
			success: function(resp) {
				this.handleDeploySucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
		});
	},

	handleDeploySucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(this.localize('DeployErrMsg') + '[' + respData.message + ']');
		else
			clientVM.displaySuccess(this.localize('DeploySucMsg'));
		var records = respData.records;
		//this.set('filtersSelections', []); commented out because UI does not deselect an item after it is successfully deployed
		this.deployedFilters.removeAll();
		if (records != null)
			this.deployedFilters.add(records);
	},

	markDeletedDeployedFilters: function() {
		Ext.each(this.deployedFiltersSelections, function(r) {
			if (r.get('uqueue') === '<|Default|Queue|>')
				this.deployedFilters.remove(r);
			else
				r.set('isDeleted', true);
		}, this);
	},

	markDeletedDeployedFiltersIsEnabled$: function() {
		return this.reqCount == 0 && this.deployedFiltersSelections.length > 0;
	},

	gatewayIsEnabled$: function() {
		return this.reqCount == 0;
	},

	showFilterListIsEnabled$: function() {
		return this.reqCount == 0 && this.gateway != null;
	},

	undeployFiltersIsEnabled$: function() {
		return this.reqCount == 0 && this.gateway != null && this.deployedFiltersSelections.length > 0;
	},

	numOfDeployedFilters: 0,

	deployFiltersIsEnabled$: function() {
		return this.reqCount == 0 && this.gateway != null && this.numOfDeployedFilters > 0;
	},

	when_gateway_changed: {
		on: ['gatewayChanged'],
		action: function() {
			this.loadDeployedFilters();
		}
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},

	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	}
});