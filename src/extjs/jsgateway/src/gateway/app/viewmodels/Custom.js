glu.defModel('RS.gateway.Custom', {
	activate : function(screen, params){
		if(this.viewIsReady){
			if(this.name){
				if(this.name != params.name){ //re-render this screen if new gateway is loaded.
					this.set("name", params.name);
					this.loadMainView(params.name);
				}
				else
					this.subVM.activate();
			}
			else
				clientVM.displayError("A gateway name is needed. Ex: RS.gateway.Custom/name=NAME", 10000)
		}
	},
	subVM : null,
	panel : null,
	viewIsReady : false,
	config : {},
	encryptFields: [],
	init : function() {
		var me = this;
		this.on('viewIsReady', function(){
			me.viewIsReady = true;
			if(this.name)
				me.loadMainView(this.name);
			else
				clientVM.displayError("A gateway name is needed. Ex: RS.gateway.Custom/name=NAME", 10000)
		});
	},
	loadMainView : function(gatewayName){
		this.fireEvent('reloadingView');
		this.buildViewConfig(gatewayName);
		this.loadCustomField(gatewayName);
	},

	buildViewConfig : function(gatewayName){
		var newConfig = {
			modelName: gatewayName,
			gatewaytype: gatewayName,
			filterViewName: 'RS.gateway.CustomFilter',
			requestParams: {
				modelName: gatewayName + "Filter",
				queueName: gatewayName.toUpperCase(),
				gatewayName: gatewayName.toUpperCase(),
				gatewayClass: 'M' + gatewayName,
				getterMethod: 'getFilters',
				clearAndSetMethod: 'clearAndSetFilters',
				undeployFilterMethod: 'undeployFilters',
				deleteFilterNames: null
			},
			config : function(){}
		}
		Ext.apply(this.config, newConfig);
	},

	loadCustomField : function(gatewayName){
		var me = this;
		Ext.Ajax.request({
			url : "/resolve/service/gateway/getCustomSDKFields",
			method : "GET",
			params : {
				customGatewayName : gatewayName
			},
			success : function(r){
				var response = RS.common.parsePayload(r);
				if (response.success) {
					var records = response.records;
					var additionalColumns = []
					var additionalStoreFields = [];
					if(records && records.length > 0){
						for(var i = 0 ; i < records.length; i++){
							var newField = records[i];
							var rawFieldParameter = newField.NAME;
							var fieldParameter = 'u' + rawFieldParameter;

							//Build addtional column and field for new custom gateway.
							if (newField.TYPE === "textenc" || newField.TYPE === "encrypt") {
								me.encryptFields.push(fieldParameter);
							}
							var newAdditionalColumm = {
								header: newField.DISPLAYNAME,
								dataIndex: fieldParameter,
								filterable: true,
								flex: 1,
								renderer: function(value, metaData, record){
									var dataIndexName = metaData.column.dataIndex;
									var rawRecord = record.raw;
									var data = (rawRecord.attrs && rawRecord.attrs[dataIndexName])? rawRecord.attrs[dataIndexName] : rawRecord[dataIndexName];
									if (data && me.encryptFields.indexOf(dataIndexName) !== -1) {
										data = JSON.parse(data);
										data = data.encrypt ? "******" : data.value;
										if (data.label){
											metaData.column.setText(data.label);
										}
									}
									return data;
								}
							}												
							if(newField.TYPE == 'textarea') {
								newAdditionalColumm.renderer = function(value, metaData, record) {
									var dataIndexName = metaData.column.dataIndex;
									var rawRecord = record.raw;
									var dataIndexValue = (rawRecord.attrs && rawRecord.attrs[dataIndexName])? rawRecord.attrs[dataIndexName] : rawRecord[dataIndexName];
									return Ext.String.htmlEncode(dataIndexValue);
								}
							}
							additionalColumns.push(newAdditionalColumm);

							var type = newField.TYPE == 'checkbox' ? 'bool' : newField.TYPE;
							type = ( type == 'string' || type == 'textarea') ? "string" : type;
							var newStoreField = {
								name: fieldParameter,
								type: type
							}
							
							additionalStoreFields.push(newStoreField);
						}
					}
					//Apply to config
					me.config["additionalColumns"] = additionalColumns;
					me.config["additionalStoreFields"] = additionalStoreFields;
					me.config["filterDisplayName"] = response.data.menuTitle + " Filter";
					me.activateGatewayView();
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	activateGatewayView : function(){
		var viewmodel = glu.model(Ext.apply({
			mtype : "RS.gateway.Main",
			clientVM: this.clientVM,
			screenVM: this.screenVM,
			SDK : true
		}, this.config));
		this.subVM = viewmodel;
		var view = glu.createViewmodelAndView(viewmodel);
		this.fireEvent('updateView', view);
		viewmodel.activate();
	}
});
