glu.defModel('RS.gateway.EWSAddress', {
	ignoreName: true,
	uewsaddress: '',
	uewsaddressIsValid$: function() {
		return this.uewsaddress ? true : this.localize('invalidAddress');
	},
	uewspassword: '',
	modelName: 'EWSAddress',
	mixins: ['BaseFilter'],
	gatewaytype: 'ewsAddress',
	fields: RS.gateway.commonFields.concat(['uewsaddress', 'uewspassword']),

	init: function() {
		this.initBase();
		this.set('title', this.localize('ewsAddressTitle'));
		this.activate(screen, {
			id: this.id,
			uniqueIdField: this.uniqueIdField
		});
	},

	userDateFormat$: function() {},

	handleSaveSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(this.localize('SaveErrMsg') + '[' + respData.message + ']');
		else {
			clientVM.displaySuccess(this.localize('SaveSucMsg'));
			this.cancel();
		}

		this.parentVM.loadFilters();
		this.parentVM.loadDeployedFilters();
	},

	respErr: function(resp) {
		clientVM.displayFailure(resp);
	},

	cancel: function() {
		this.doClose();
	}
});