glu.defModel('RS.gateway.CASpectrumFilter', {
	uurlQuery: '',
	uxmlQuery:'',
	modelName: 'CASpectrumFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'CASpectrum',
	objectStore : {
		mtype :'store',
		proxy: {
			type: 'memory'
		},
		fields : ['name'],
		data : [{
			name : "alarms"
		},{
			name : "models"
		},
		{
			name : "devices"
		},
		{
			name : "subscription"
		}]
	},
	uobjectIsValid$: function(){
		return this.uobject && this.uobject != '' ? true : this.localize('invalidObject');
	},
	fields: RS.gateway.commonFields.concat(['uobject', 'uurlQuery', 'uxmlQuery']),
	init: function() {
		this.initBase();
		this.set('title', this.localize('CASpectrumFilterTitle'));
	}
});