glu.defModel('RS.gateway.Gateways', {
	title: 'Gateways',
	gatewayStore: {
		mtype: 'store',
		fields: ['uqueue'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/gateway/listGateways',
			reader: {
				type: 'array',
				root: 'records',
				successProperty: 'success'
			}
			/* handled below in handleAjaxException()
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
			*/
		}
	},

	columns: [{
		header: '~~Gateway~~',
		dataIndex: 'uqueue',
		filterable: true,
		flex: 1
	}],

	state: '',

	gatewaysSelections: [],

	activate: function() {

	},

	init: function() {
		this.loadGateways();
	},

	deleteGateways: function() {
		this.message({
			title: this.localize('deleteGatewaysTitle'),
			msg: this.localize(this.gatewaysSelections.length > 1 ? 'deleteGatewaysMsg' : 'deleteGatewayMsg', {
				num: this.gatewaysSelections.length
			}),
			button: Ext.MessageBox.YES,
			buttonText: {
				yes: this.localize('confirmDeleteGateway'),
				no: this.localize('cancel')
			},
			fn: this.sendDeleteGatewayReq
		});
	},

	sendDeleteGatewayReq: function(btn) {
		if (btn == 'no') {
			return;
		}
		var ids = [];

		Ext.each(this.gatewaysSelections,function(r){
			ids.push(r.get('id'));
		});

		this.ajax({
			url: '/resolve/service/gateway/deleteGateways',
			params: {
				modelName: 'EmailFilter',
				ids: ids
			},
			scope: this,
			success: function(resp) {
				this.set('state', 'ready');
				this.handleDeleteSuccess(resp);
			},
			failure: function(resp) {
				var error = {};
				error.status = resp.status;
				this.handleAjaxException(error);
			}
		});
	},

	handleDeleteSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			var error = {};
			error.title = 'DeleteGatewaysErrTitle';
			error.msg = 'DeleteGatewaysErrMsg';
			error.ps = respData.message;
			var next = (function(obj) {
				return function() {
					obj.loadGateways()
				}
			})(this);
			this.handleAjaxException(error, next);
			return;
		}
		this.loadGateways();
	},

	deleteGatewaysIsEnabled$: function() {
		return this.state != 'wait';
	},

	loadGateways: function() {
		this.set('state', 'wait');
		this.gatewayStore.load({
			scope: this,
			params: {
				modelName: 'EmailFilter'
			},
			callback: function(records, op, suc) {
				if (!suc) {
					var e = {};
					if (op.error) {
						e.status = op.error.status;
					}
					e.title = 'ListGatewaysErrTitle';
					e.msg = 'ListGatewaysErrMsg';

					this.handleAjaxException(e);
				}
				this.set('state', 'ready');
			}
		});
	},

	handleAjaxException: function(error, next) {
		var title = null;
		var msg = null;
		if (error.status) {
			title = this.localize('ServerErrTitle');
			msg = this.localize('ServerErrMsg') + '[' + error.status + ']';
		} else {
			title = this.localize(error.title);
			msg = this.localize(error.msg);
		}

		this.message({
			title: title,
			msg: msg,
			buttons: Ext.MessageBox.YES,
			buttonText: {
				yes: this.localize('confirmAjaxException')
			},
			scope: this,
			fn: function() {
				if (next)
					next();
			}
		});
	},

	cancel: function() {
		this.doClose();
	}
});