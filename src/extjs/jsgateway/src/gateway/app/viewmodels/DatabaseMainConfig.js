RS.gateway.database = {
	modelName: 'database',
	filterViewName: 'RS.gateway.DatabaseFilter',
	poolStore: null,
	requestParams: {
		modelName: 'DatabaseFilter',
		queueName: 'DATABASE',
		gatewayName: 'DB',
		gatewayClass: 'MDatabase',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~upool~~',
		dataIndex: 'upool',
		filterable: true,
		flex: 1
	}, {
		header: '~~usql~~',
		dataIndex: 'usql',
		filterable: true,
		flex: 1
	}, {
		header: '~~uupdateSql~~',
		dataIndex: 'uupdateSql',
		filterable: true,
		flex: 1
	}, {
		header: '~~ulastValue~~',
		dataIndex: 'ulastValue',
		filterable: true,
		flex: 1
	}, {
		header: '~~ulastValueColumn~~',
		dataIndex: 'ulastValueColumn',
		filterable: true,
		flex: 1
	}, {
		header: '~~uprefix~~',
		dataIndex: 'uprefix',
		filterable: true,
		flex: 1
	}, {
		header: '~~ulastValueQuote~~',
		dataIndex: 'ulastValueQuote',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'upool',
		type: 'string'
	}, {
		name: 'usql',
		type: 'string'
	}, {
		name: 'uupdateSql',
		type: 'string'
	}, {
		name: 'ulastValue',
		type: 'string'
	}, {
		name: 'ulastValueColumn',
		type: 'string'
	}, {
		name: 'uprefix',
		type: 'string'
	}, {
		name: 'ulastValueQuote',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('databaseFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('databaseGatewayDisplayName'));
		this.set('title', this.localize('databaseTitle'));
	}
};

glu.defModel('RS.gateway.DatabaseMain', {
	mixins: ['Main'],
	gatewaytype: 'database'
});