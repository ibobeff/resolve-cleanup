RS.gateway.sshPool = {
	modelName: 'SSHPool',
	uniqueIdField: 'usubnetMask',
	filterViewName: 'RS.gateway.SSHPool',
	requestParams: {
		modelName: 'SSHPool',
		queueName: 'SSH',
		gatewayName: 'SSH',
		gatewayClass: 'MSSH',
		getterMethod: 'getPools',
		clearAndSetMethod: 'clearAndSetPools',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},

	concatColumns: function() {
		this.set('filterColumns', [{
			header: '~~usubnetMask~~',
			dataIndex: 'usubnetMask',
			filterable: true,
			flex: 1
		}, {
			header: '~~umaxConn~~',
			dataIndex: 'umaxConn',
			filterable: true,
			flex: 1
		}, {
			header: '~~utimeout~~',
			dataIndex: 'utimeout',
			filterable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()));
	},

	config: function() {
		this.set('filterDisplayName', this.localize('sshPoolDisplayName'));
		this.set('title', this.localize('sshPoolTitle'));
	},

	getStoreFields: function() {
		if (!this.gatewaytype)
			alert('Need to specify a gatewaytype!');
		var fields = [
			'id',
			'usubnetMask',
			'umaxConn',
			'utimeout'
		].concat(RS.common.grid.getSysFields());
		return fields;
	},

	createFilter: function() {
		this.open({
			mtype: this.filterViewName
		});
	},
	editFilter: function(id) {
		this.open({
			mtype: this.filterViewName,
			uniqueIdField: this.uniqueIdField,
			gatewaytype: this.gatewaytype,
			id: id
		});
	}
};

glu.defModel('RS.gateway.SSHPoolMain', {
	mixins: ['Main'],
	gatewaytype: 'sshPool'
});