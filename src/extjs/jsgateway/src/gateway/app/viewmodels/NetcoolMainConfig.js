RS.gateway.netcool = {
	modelName: 'netcool',
	filterViewName: 'RS.gateway.NetcoolFilter',
	requestParams: {
		modelName: 'NetcoolFilter',
		queueName: 'NETCOOL',
		gatewayName: 'NETCOOL',
		gatewayClass: 'MNetcool',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~usql~~',
		dataIndex: 'usql',
		filterable: true,
		flex: 1,
		renderer: this.renderer
	}],

	additionalStoreFields: [{
		name: 'usql',
		type: 'string'
	}, ],
	config: function() {
		this.set('filterDisplayName', this.localize('netcoolFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('netcoolGatewayDisplayName'));
		this.set('title', this.localize('netcoolTitle'));
	}
};

glu.defModel('RS.gateway.NetcoolMain', {
	mixins: ['Main'],
	gatewaytype: 'netcool'
});