RS.gateway.hpom = {
	modelName: 'hpom',
	filterViewName: 'RS.gateway.HPOMFilter',
	requestParams: {
		modelName: 'HPOMFilter',
		queueName: 'HPOM',
		gatewayName: 'HPOM',
		gatewayClass: 'MHPOM',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uobject~~',
		dataIndex: 'uobject',
		filterable: true,
		flex: 1
	}, {
		header: '~~uquery~~',
		dataIndex: 'uquery',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'uobject',
		type: 'string'
	}, {
		name: 'uquery',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('hpomFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('hpomGatewayDisplayName'));
		this.set('title', this.localize('hpomTitle'));
	}
};

glu.defModel('RS.gateway.HPOMMain', {
	mixins: ['Main'],
	gatewaytype: 'hpom'
});