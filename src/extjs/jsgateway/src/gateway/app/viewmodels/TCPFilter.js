glu.defModel('RS.gateway.TCPFilter', {
	uport: '',
	modelName: 'TCPFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'tcp',
	fields: RS.gateway.commonFields.concat(['uport', 'ubeginSeparator', 'uendSeparator',
	    {
            name: 'uincludeBeginSeparator',
            type: 'bool'
        },{
            name: 'uincludeEndSeparator',
            type: 'bool'
        }]),
	init: function() {
		this.initBase();
		this.set('title', this.localize('tcpFilterTitle'));
	}
});