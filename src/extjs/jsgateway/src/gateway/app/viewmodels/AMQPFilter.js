glu.defModel('RS.gateway.AMQPFilter', {
	uquery: '',
	modelName: 'AmqpFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'amqp',
	fields: RS.gateway.commonFields.concat(['utargetQueue', {
		name: 'uexchange',
		type: 'bool'
	}]),
	// uqueryIsValid$:function(){
	// 	return this.uquery&&this.uquery!=''?true:this.localize('invalidQuery');
	// },

	init: function() {
		this.initBase();
		this.set('title', this.localize('amqpFilterTitle'));
	}
});