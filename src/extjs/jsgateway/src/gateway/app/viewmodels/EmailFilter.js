glu.defModel('RS.gateway.EmailFilter', {
	uquery: '',
	modelName: 'EmailFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'email',
	fields: RS.gateway.commonFields.concat(['uquery']),
	// uqueryIsValid$:function(){
	// 	return this.uquery&&this.uquery!=''?true:this.localize('invalidQuery');
	// },

	init: function() {
		this.initBase();
		this.set('title', this.localize('emailFilterTitle'));
	}
});