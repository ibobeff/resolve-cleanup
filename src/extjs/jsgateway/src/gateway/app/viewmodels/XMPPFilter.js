glu.defModel('RS.gateway.XMPPFilter', {
	uregex: '',
	modelName: 'XMPPFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'xmpp',
	fields: RS.gateway.commonFields.concat(['uregex']),
	// uregexIsValid$:function(){
	// 	return this.uregex&&this.uregex!=''?true:this.localize('invalidQuery');
	// },

	init: function() {
		this.initBase();
		this.set('title', this.localize('xmppFilterTitle'));
	}
});