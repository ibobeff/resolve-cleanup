glu.defModel('RS.gateway.EmailAddress', {
	ignoreName: true,
	uemailAddress: '',
	uemailAddressIsValid$: function() {
		return this.uemailAddress ? true : this.localize('invalidAddress');
	},
	uemailPassword: '',
	modelName: 'EmailAddress',
	mixins: ['BaseFilter'],
	gatewaytype: 'emailAddress',
	fields: RS.gateway.commonFields.concat(['uemailAddress', 'uemailPassword', 'uniqueId']),

	init: function() {
		this.initBase();
		this.set('title', this.localize('emailAddressTitle'));
		this.activate(screen, {
			id: this.id,
			uniqueIdField: this.uniqueIdField
		});
	},

	userDateFormat$: function() {},

	handleSaveSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(this.localize('SaveErrMsg') + '[' + respData.message + ']');
		else {
			clientVM.displaySuccess(this.localize('SaveSucMsg'));
			this.cancel();
		}

		this.parentVM.loadFilters();
		this.parentVM.loadDeployedFilters();
	},

	respErr: function(resp) {
		clientVM.displayFailure(resp);
	},

	cancel: function() {
		this.doClose();
	}
});