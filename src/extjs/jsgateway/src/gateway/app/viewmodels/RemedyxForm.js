glu.defModel('RS.gateway.RemedyxForm', {
	uniqueIdField: 'uname',
	uname: '',
	ignoreName: true,
	ufieldList: '',
	modelName: 'RemedyxForm',
	mixins: ['BaseFilter'],
	gatewaytype: 'remedyxForm',
	fields: ['uname', 'ufieldList'],
	// uqueryIsValid$:function(){
	// 	return this.uquery&&this.uquery!=''?true:this.localize('invalidQuery');
	// },

	init: function() {
		this.initBase();
		this.set('title', this.localize('remedyxFormTitle'));
		this.activate(screen, {
			id: this.id,
			uniqueIdField: this.uniqueIdField
		});
	},
	userDateFormat$: function() {},
	handleSaveSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(this.localize('SaveErrMsg') + '[' + respData.message + ']');
		else {
			clientVM.displaySuccess(this.localize('SaveSucMsg'));
			this.cancel();
		}
		this.parentVM.loadFilters();
		this.parentVM.loadDeployedFilters();
	},

	respErr: function(resp) {
		clientVM.displayFailure(resp);
	},
	cancel: function() {
		this.doClose();
	}
});