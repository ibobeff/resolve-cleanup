RS.gateway.tcp = {
	modelName: 'tcp',
	filterViewName: 'RS.gateway.TCPFilter',
	requestParams: {
		modelName: 'TCPFilter',
		queueName: 'TCP',
		gatewayName: 'TCP',
		gatewayClass: 'MTCP',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uport~~',
		dataIndex: 'uport',
		filterable: true,
		flex: 1
	},{
        header: '~~ubeginSeparator~~',
        dataIndex: 'ubeginSeparator',
        filterable: true,
        renderer: RS.common.grid.plainRenderer(),
        flex: 1
    },{
        header: '~~uincludeBeginSeparator~~',
        dataIndex: 'uincludeBeginSeparator',
		renderer: RS.common.grid.booleanRenderer(),
        filterable: true,
        flex: 1
    },{
        header: '~~uendSeparator~~',
        dataIndex: 'uendSeparator',
        filterable: true,
        renderer: RS.common.grid.plainRenderer(),
        flex: 1
    },{
        header: '~~uincludeEndSeparator~~',
        dataIndex: 'uincludeEndSeparator',
		renderer: RS.common.grid.booleanRenderer(),
        filterable: true,
        flex: 1
    }],

	additionalStoreFields: [{
		name: 'uport',
		type: 'string'
	},{
        name: 'ubeginSeparator',
        type: 'string'
    },{
        name: 'uincludeBeginSeparator',
        type: 'string'
    },{
        name: 'uendSeparator',
        type: 'string'
    },{
        name: 'uincludeEndSeparator',
        type: 'string'
    }],
	config: function() {
		this.set('filterDisplayName', this.localize('tcpFilterDisplayName'));
		this.set('title', this.localize('tcpTitle'));
	}
};

glu.defModel('RS.gateway.TCPMain', {
	mixins: ['Main'],
	gatewaytype: 'tcp'
});