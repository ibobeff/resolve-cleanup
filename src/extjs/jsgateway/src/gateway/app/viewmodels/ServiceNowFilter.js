glu.defModel('RS.gateway.ServiceNowFilter', {
	uquery: '',
	modelName: 'ServiceNowFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'serviceNow',
	fields: RS.gateway.commonFields.concat(['uobject', 'uquery']),
	// uqueryIsValid$:function(){
	// 	return this.uquery&&this.uquery!=''?true:this.localize('invalidQuery');
	// },

	init: function() {
		this.initBase();
		this.set('title', this.localize('serviceNowFilterTitle'));
	}
});