glu.defModel('RS.gateway.BaseFilter', {
	uniqueIdField: 'uname',
	uname: '',
	uinterval: undefined,
	uorder: 1,
	uactive: true,
	ueventEventId: '',
	urunbook: '',
	uscript: '',
	modelName: '',

	id: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrganizationName: '',

	defaultData: {
		uname: '',
		uinterval: undefined,
		uorder: 1,
		uactive: true,
		ueventEventId: '',
		urunbook: '',
		uscript: '',

		id: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrganizationName: ''
	},

	title: '',

	wait: false,
	API : {
		getDefaultInterval : '/resolve/service/gateway/getDefaultInterval',
		getAuthenticationType: '/resolve/service/gateway/getHttpAuthType',
		getFilter : '/resolve/service/gateway/getFilter',
		saveFilter : '/resolve/service/gateway/saveFilter'
	},	
	unameIsValid$: function() {
		if (this.ignoreName)
			return true;
        return RS.common.validation.TaskName.test(this.uname) ? true : this.localize('invalidName');
    },
	unameIsReadOnly$: function() {
		return !!this.id;
	},

	uorderIsValid$: function() {
		return /^\d*$/.test(this.uorder) ? true : this.localize('invalidOrder');
	},

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	activateBase: function(params) {
		window.document.title = this.localize('windowTitle');
		this.resetForm()
		this.set('id', params && params.id != null ? params.id : '');
		this.set('uniqueIdField', params.uniqueIdField ? params.uniqueIdField : 'uname');
		this.set('wait', false);
		if (this.id != '') {
			this.loadFilter();
			return;
		}
	},

	activate: function(screen, params) {
		this.loadDefaultValue(params);		
	},

	initBase: function() {
		this.set('fields', this.fields.concat(this.commonFields));	
	},
	loadDefaultValue : function(params){
		
		var self = this;
		//For now, just need for interval, might need to enhance this method in future.
		this.ajax({
			url : this.API['getDefaultInterval'],
			method : 'GET',
			params : {
				gatewayName : this.gatewaytype
			},
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				this.defaultData['uinterval'] = response.data ? response.data : 10;
				this.ajax({
					url : this.API['getAuthenticationType'],
					method : 'GET',
					params : {
						gatewayName : this.gatewaytype
					},
					success : function(resp){
						var response = RS.common.parsePayload(resp);						
						this.defaultData['uhttpAuthType'] = response.data ? response.data : "";
					    self._uhttpAuthType = response.data ? response.data : "";
						this.activateBase(params);
					}
				})
			}
		});
	},
	uhttpAuthType: null,
	loadStaticData: function() {
		this.set('uhttpAuthType',   this._uhttpAuthType );
	},
	loadFilter: function() {
		this.set('wait', true);
		this.ajax({
			url: this.API['getFilter'],
			method: 'GET',
			params: {
				id: this.id,
				modelName: this.modelName
			},
			scope: this,
			success: function(resp) {
				this.handleLoadFilterReqSuccess(resp);
			},
			failure: function(resp) {
				this.handleLoadFilterReqSuccess(resp);
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	handleLoadFilterReqSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);		
		if (!respData.success) {
			clientVM.displayError(this.localize('GetFilterErrMsg') + '[' + respData.message + ']');
			return;
		}
		if(this.SDK && this.encryptFields.length > 0) {
			for(var i = 0; i < this.encryptFields.length; i += 1) {
				var fieldName = this.encryptFields[i];
				var fieldData = JSON.parse(respData.data[fieldName]);
				respData.data["is" + fieldName +"Encrypted"] = fieldData.encrypt;			
				if(fieldData.hasOwnProperty("label")) {
					respData.data[fieldName + "Label"] = fieldData.label;
				}
				respData.data[fieldName] = fieldData.value;
			}
		}
		this.loadData(respData.data);
		this.loadStaticData();
	},

	save: function(exitAfterSave) {
		if (this.task) this.task.cancel();
		this.task = new Ext.util.DelayedTask(this.saveFilter, this, [exitAfterSave]);
		this.task.delay(500);
	},

	saveIsEnabled$: function() {
		return this.state != 'wait' && this.isValid;
	},

	saveAndExit: function() {
		if (this.task) this.task.cancel()
		this.save(true)
	},

	saveAs: function() {
		this.open({
			mtype: 'RS.gateway.SaveAs',
			uniqueIdField: this.uniqueIdField,
			modal: true,
			dumper: (function(self) {
				return function() {
					self.saveFilter(false, this.name, true);
				}
			})(this)
		});
	},

	saveAsIsEnabled$: function() {
		return this.state != 'wait' && this.isValid;
	},

	saveAsIsVisible$: function() {
		return !!this.id;
	},

	respErr: function(resp) {
		clientVM.displayFailure(resp);
	},

	saveFilter: function(exitAfterSave, newName, newWindow) {
		var filter = this.asObject();
		//Trim all the extra spaces before save.
		for(var k in filter){
			if(typeof filter[k] == "string")
				filter[k] = filter[k].trim();
			if(this.SDK && this.encryptFields.indexOf(k) != -1) {
				filter[k] = {
					label:  filter[k + "Label"],
					value: filter[k],
					encrypt: filter["is" + k + "Encrypted"]
				}
				delete filter["is" + k + "Encrypted"];
				delete filter[k + "Label"];
			}
		}
		filter.modelName = this.modelName;
		if (newName) {
			filter.id = '';
			filter[this.uniqueIdField] = newName;
		}
		this.set('wait', true);
		this.ajax({
			url: this.API['saveFilter'],
			jsonData: filter,
			success: function(resp) {
				this.set('wait', false);
				this.handleSaveSuccess(resp, newWindow);
				if (exitAfterSave === true)
					this.back();
			},
			failure: this.respErr
		});
	},

	handleSaveSuccess: function(resp, newWindow) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('SaveErrMsg') + '[' + respData.message + ']');
			return;
		}
		if (newWindow) {
			clientVM.handleNavigation({
				modelName: 'RS.gateway.' + this.viewmodelName,
				params: {
					name: this.gatewaytype,
					id: respData.data.id
				},
				target: '_blank'
			})
			this.loadFilter();
		} else {
			if(this.SDK && this.encryptFields.length > 0) {
				for(var i = 0; i < this.encryptFields.length; i += 1) {
					var fieldName = this.encryptFields[i];
					var fieldData = JSON.parse(respData.data[fieldName]);
					respData.data["is" + fieldName +"Encrypted"] = fieldData.encrypt;			
					if(fieldData.hasOwnProperty("label")) {
						respData.data[fieldName + "Label"] = fieldData.label;
					}
					respData.data[fieldName] = fieldData.value;
				}
			}
			this.loadData(respData.data);		
		}
		clientVM.displaySuccess(this.localize('SaveSucMsg'));
		this.loadStaticData();

	},

	back: function() {
		var config = {
			modelName: 'RS.gateway.' + this.viewmodelName.substring(0, this.viewmodelName.length - 6) + 'Main'
		}
		if(this.SDK == true){
			Ext.apply(config, {
				modelName: 'RS.gateway.Custom',
				params : {
					name : this.gatewaytype
				}
			})
		}
		clientVM.handleNavigation(config);
	},

	resetForm: function() {
		this.set('uniqueIdField', 'uname');
		this.loadData(this.defaultData);
	},

	refresh: function() {
		if (this.id == null || this.id == '')
			return;
		this.loadFilter();
	}
});