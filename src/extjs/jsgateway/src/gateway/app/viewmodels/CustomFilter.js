glu.defModel("RS.gateway.CustomFilter",{
	template_mtype : "RS.gateway.CustomFilter",
	activate : function(screen, params){
		if(this.viewIsReady){
			if(this.name != params.name){ //re-render this screen if new gateway is loaded.
				this.set("name", params.name);
				this.loadCustomFilter(this.name);
			}
			else
				this.subVM.activate(screen, params);
		}
	},
	panel : "panel",
	viewIsReady : false,	
	subVM : null,
	init : function() {
		var me = this;
		this.on('viewIsReady', function(){
			me.viewIsReady = true;
			me.loadCustomFilter(this.name);
		});
	},
	
	loadCustomFilter : function(filterName){
		var me = this;
		Ext.Ajax.request({
			url : "/resolve/service/gateway/getCustomSDKFields",
			method : "GET",
			params : {
				customGatewayName : filterName
			},
			success : function(r){
				var response = RS.common.parsePayload(r);
				if (response.success) {
					var records = response.records;
					var data = response.data;
					if(records && records.length > 0){						
						me.defineViewModelConfig(data, records);
						me.defineViewConfig(records);
						me.activateFilterView();
					}
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	activateFilterView : function(){
		//Initialize model and build view
		var viewmodel = glu.model(Ext.apply({
			mtype : this.template_mtype,
			clientVM : this.clientVM,
			screenVM : this.screenVM
		}, this.params));
		var view = glu.createViewmodelAndView(viewmodel);
		this.subVM = viewmodel;	
		this.fireEvent('updateView', view);
	},

	defineViewModelConfig : function(data, records){
		var me = this;
		var config = {
			fields : RS.gateway.SDKcommonFields.concat([]),
			encryptFields: [],		
			gatewaytype: data.implPrefixes,
			mixins: ['BaseFilter'],
			modelName : data.implPrefixes + "Filter",
			defaultData : {},		
			SDK : true,
			init: function() {
				this.initBase();
				this.set('title', data.menuTitle + " Filter"); 
			}
		};

		for(var i = 0; i < records.length; i++){
			var field = records[i];
			var fieldType = field.TYPE;
			var rawFieldParameter = field.NAME;
			var fieldParameter = 'u' + rawFieldParameter;			
			config["fields"].push(fieldParameter);
			config["defaultData"][fieldParameter] = field.DEFAULTVALUE;
			if (fieldType == "encrypt") {
				config["fields"].push({ name: "is" + fieldParameter + "Encrypted", type: 'bool' });
				config["encryptFields"].push(fieldParameter);
				config["defaultData"]["is" + fieldParameter + "Encrypted"] = false;		
			} else if (fieldType == "textenc") {
				config["fields"].push({ name: "is" + fieldParameter + "Encrypted", type: 'bool' });
				config["encryptFields"].push(fieldParameter);
				config["defaultData"]["is" + fieldParameter + "Encrypted"] = false;
				config["fields"].push(fieldParameter + "Label");
				config["defaultData"][fieldParameter + "Label"] = "";			
			}

			if(fieldType == "textarea")
				Ext.apply(config, this.getTextareaVM(field));
			else if(fieldType == "text")
				Ext.apply(config, this.getTextfieldVM(field));
			else if(fieldType == "list")
				Ext.apply(config, this.getComboboxVM(field));
			else if(fieldType == "checkbox")
				Ext.apply(config, this.getCheckboxVM(field));
			else if(fieldType == "encrypt")
				Ext.apply(config, this.getEncryptVM(field));
			else if(fieldType == "textenc")
				Ext.apply(config, this.getTextEncryptVM(field));
		}

		glu.defModel(this.template_mtype, config);
	},

	getTextfieldVM : function(field){
		var config = {};
		// var fieldParameter = field.NAME;
		// if(!field.REQUIRE){
		// 	config[ fieldParameter + "IsValid$" ] = function(){
				
		// 		return this.testField && this.testField != '' ? true  : this.localize('invalidField');
		// 	}
		// }
		return config;
	},

	getTextareaVM : function(field){
		var config = {};
		// var fieldParameter = field.NAME;
		// if(field.REQUIRE){
		// 	config[ fieldParameter+ "IsValid$" ] = function(){
		// 		return this.fieldParameter && this.fieldParameter != '' ? true : this.localize('invalidField');
		// 	}
		// }
		return config;
	},

	getComboboxVM : function(field){
		var config = {};
		var rawFieldParameter = field.NAME;
		var fieldParameter = 'u' + rawFieldParameter;
		// if(field.REQUIRE){
		// 	config[ fieldParameter+ "IsValid$" ] = function(){
		// 		return this[fieldParameter] && this[fieldParameter] != '' ? true : this.localize('invalidField');
		// 	}
		// }

		//Add store for combo box values
		var valueSet = field.VALUES.split(",");
		var dataSet = []
		for(var i = 0; i < valueSet.length; i++){
			dataSet.push({
				value : valueSet[i]
			})
		}
		config[fieldParameter + "_store"] = {
			mtype : 'store',
			proxy : {
				type : 'memory'
			},
			fields : ['value'],
			data : dataSet
		}
		return config;
	},


	getCheckboxVM : function(field){
		var config = {};
		// var fieldParameter = field.NAME;
		// config[fieldParameter] = this.DEFAULTVALUE;
		// if(field.REQUIRE){
		// 	config[ fieldParameter+ "IsValid$" ] = function(){
		// 		return this.fieldParameter && this.fieldParameter != '' ? true : this.localize('invalidField');
		// 	}
		// }
		return config;
	},

	getEncryptVM: function() {
		var config = {};
		return config;
	},

	getTextEncryptVM: function() {
		var config = {};
		return config;
	},

	defineViewConfig : function(records){
		var standardViewConfig = {
			xtype: 'panel',
			style : 'border-top :1px solid #cccccc;',			
			layout: {
				type: 'vbox',
				align: 'stretch'
			}			
		}
		var additionalSettingViewConfig = {
			xtype: 'panel',
			title: '~~additionalSettingTitle~~',
			layout: {
				type: 'vbox',
				align: 'stretch'
			}
		};

		var leftViewContainer = [];
		var rightViewContainer = [];
		var fullViewContainer = [];
		var leftAdditionalSettingViewContainer = [];
		var rightAdditionalSettingViewContainer = [];
		var fullAdditionalSettingViewContainer = [];
		for(var i = 0; i < records.length; i++){
			var field = records[i];
			var fieldType = field.TYPE;
			var fieldConfig = {};

			//Construct view config.
			if(fieldType == "textarea")
				fieldConfig = this.getTextareaView(field);
			else if(fieldType == "text")
				fieldConfig = this.getTextfieldView(field);
			else if(fieldType == "list")
				fieldConfig = this.getComboboxView(field);
			else if(fieldType == "checkbox")
				fieldConfig = this.getCheckboxView(field);
			else if(fieldType == "encrypt")
				fieldConfig = this.getEncryptView(field);
			else if(fieldType == "textenc")
				fieldConfig = this.getTextEncryptView(field);

			//Check for standard field.
			if(field.STANDARD == "TRUE"){
				if(field.POSITION && field.POSITION.toLowerCase() == "left")
					leftViewContainer.push(fieldConfig);
				else if(field.POSITION && field.POSITION.toLowerCase() == "right")
					rightViewContainer.push(fieldConfig);
				else
					fullViewContainer.push(fieldConfig);
			}
			else {
				if(field.POSITION && field.POSITION.toLowerCase() == "left")
					leftAdditionalSettingViewContainer.push(fieldConfig);
				else if(field.POSITION && field.POSITION.toLowerCase() == "right")
					rightAdditionalSettingViewContainer.push(fieldConfig);
				else
					fullAdditionalSettingViewContainer.push(fieldConfig);
			}		
			
		}
		standardViewConfig["items"] = this.layoutFromConfig(leftViewContainer, rightViewContainer, fullViewContainer);
		additionalSettingViewConfig["items"] = this.layoutFromConfig(leftAdditionalSettingViewContainer, rightAdditionalSettingViewContainer, fullAdditionalSettingViewContainer);

		glu.defView(this.template_mtype, RS.gateway.buildSDKGatewayViewConfig(standardViewConfig, additionalSettingViewConfig));
	},

	getTextfieldView : function(field){
		var rawFieldParameter = field.NAME;
		var fieldParameter = 'u' + rawFieldParameter;
		var config = {
			fieldLabel : field.DISPLAYNAME,
			value : "@{" + fieldParameter + "}",
			name : fieldParameter,
			xtype : 'textfield',
		};
		return config;
	},

	getTextareaView : function(field){
		var rawFieldParameter = field.NAME;
		var fieldParameter = 'u' + rawFieldParameter;
		var config = {
			fieldLabel : field.DISPLAYNAME,
			value : "@{" + fieldParameter + "}",
			xtype : 'textareafield',
			name : fieldParameter,
			growMin : 200,
			growMax : 400,
			grow : true
		};
		return config;
	},

	getComboboxView : function(field){
		var rawFieldParameter = field.NAME;
		var fieldParameter = 'u' + rawFieldParameter;
		var config = {
			xtype: 'combobox',
			fieldLabel : field.DISPLAYNAME,
			value : "@{" + fieldParameter + "}",
			queryMode : 'local',
			store : '@{'+ fieldParameter + '_store}',
			displayField : 'value',
			name : fieldParameter,
			editable: false
		};
		return config;
	},


	getCheckboxView : function(field){
		var rawFieldParameter = field.NAME;
		var fieldParameter = 'u' + rawFieldParameter;
		var config = {
			fieldLabel : field.DISPLAYNAME,
			value : "@{" + fieldParameter + "}",
			xtype : 'checkbox'
		};
		return config;
	},

	getEncryptView : function(field){
		var me = this;
		var rawFieldParameter = field.NAME;
		var fieldParameter = 'u' + rawFieldParameter;
		var config = {
			layout : {
				type : 'hbox',
				align : 'stretch'
			},
			items : [{
				labelWidth: 130,
				fieldLabel : field.DISPLAYNAME,
				value : "@{" + fieldParameter + "}",
				name : fieldParameter,
				xtype : 'textfield',
				flex: 1,
				hidden: "@{is" + fieldParameter + "Encrypted}",
			},{
				labelWidth: 130,
				fieldLabel : field.DISPLAYNAME,
				value : "@{" + fieldParameter + "}",
				name : fieldParameter,
				xtype : 'textfield',
				inputType: 'password',
				flex: 1,
				hidden: "@{!is" + fieldParameter + "Encrypted}",
			},{
				boxLabel : "Encrypt",
				value : "@{is" + fieldParameter + "Encrypted}",
				xtype : 'checkbox',
				margin: "0 0 0 10px",
				id: fieldParameter,
				listeners: {
					change: function(ele, newVal, oldVal) {					
						if (oldVal && !newVal) {
							this._vm.set(this.id, "");									
						}
					}
				}
			}]
		};
		return config;
	},

	getTextEncryptView : function(field){
		var rawFieldParameter = field.NAME;
		var fieldParameter = 'u' + rawFieldParameter;
		var config = {
			layout : {
				type : 'hbox',
				align : 'stretch'
			},
			items : [{				
				hideLabel: true,
				value : "@{" + fieldParameter + "Label}",
				name : fieldParameter + "Label",
				xtype : 'textfield',
				width: 125,
			},{
				hideLabel: true,
				value : "@{" + fieldParameter + "}",
				name : fieldParameter,
				xtype : 'textfield',
				flex: 1,
				margin: "0 0 0 10px",
				hidden: "@{is" + fieldParameter + "Encrypted}",
			},{
				hideLabel: true,			
				value : "@{" + fieldParameter + "}",
				name : fieldParameter,
				xtype : 'textfield',
				inputType: 'password',
				flex: 1,
				margin: "0 0 0 10px",
				hidden: "@{!is" + fieldParameter + "Encrypted}",
			},{
				boxLabel : "Encrypt",
				value : "@{is" + fieldParameter + "Encrypted}",
				xtype : 'checkbox',
				margin: "0 0 0 10px",
				id: fieldParameter,
				listeners: {
					change: function(ele, newVal, oldVal) {					
						if (oldVal && !newVal) {
							this._vm.set(this.id, "");									
						}
					}
				}
			}]
		};
		return config;
	},

	layoutFromConfig : function(leftView, rightView, fullView){
		var items = [];
		var l = 0;
		var r = 0;
		var f = 0;

		for(;l < leftView.length; l++){
			var rightItem = {};
			if(r < rightView.length){
				rightItem = Ext.apply(rightItem, rightView[r]);
				r++;
			}
			items.push({
				layout : {
					type : 'hbox'				
				},
				defaults : {
					padding: '10px 10px 0px 10px',
					labelWidth : 130,
					flex : 1
				},
				items : [leftView[l], rightItem]
			})
		}

		for(; r < rightView.length; r++){
			items.push({
				layout : {
					type : 'hbox'
		
				},
				defaults : {
					padding: '10px 10px 0px 10px',
					labelWidth : 130,
					flex : 1
				},
				items : [{},rightView[r]]
			})
		}

		for(; f < fullView.length; f++){
			items.push(Ext.apply({
				padding: '10px 10px 0px 10px',
				labelWidth : 130,
				flex : 1
			},fullView[f]));
		}
		return items;
	}
})