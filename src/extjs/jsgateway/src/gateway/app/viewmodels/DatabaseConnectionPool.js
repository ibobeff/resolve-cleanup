glu.defModel('RS.gateway.DatabaseConnectionPool', {
	ignoreName: true,
	upoolName: '',
	upoolNameIsValid$: function() {
		return this.upoolName ? true : this.localize('invalidName');
	},
	udriverClass: '',
	u_db_us_ern_ame: '',
	u_db_pas_sword: '',
	udbconnectURI: '',
	udbmaxConn: '',
	modelName: 'DatabaseConnectionPool',
	mixins: ['BaseFilter'],
	gatewaytype: 'databaseConnectionPool',
	fields: ['upoolName', 'udriverClass', 'u_db_us_ern_ame', 'u_db_pas_sword', 'udbconnectURI', 'udbmaxConn'],
	driverStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	init: function() {
		this.initBase();
		this.set('title', this.localize('databaseConnectionPoolTitle'));
		this.loadDriver();
		this.activate(screen, {
			id: this.id,
			uniqueIdField: this.uniqueIdField
		});
	},
	userDateFormat$: function() {},
	udbmaxConnIsValid$: function() {
		var test = /^[0-9]*$/;
		return test.test(this.udbmaxConn) ? true : this.localize('invalidMaxConn');
	},
	handleSaveSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(this.localize('SaveErrMsg') + '[' + respData.message + ']');
		else {
			clientVM.displaySuccess(this.localize('SaveSucMsg'));
			this.cancel();
		}
		this.parentVM.loadFilters();
		this.parentVM.loadDeployedFilters();
	},

	respErr: function(resp) {
		clientVM.displayFailure(resp);
	},
	loadDriver: function() {
		this.ajax({
			url: '/resolve/service/gateway/getDatabaseDrivers',
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('loadDriverErrMsg'), respData.message);
					return;
				}
				var drivers = respData.data;
				for (db in drivers)
					this.driverStore.add({
						name: drivers[db],
						value: drivers[db]
					});
				this.set('udriverClass', this.driverStore.first().get('value'));
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	cancel: function() {
		this.doClose();
	},
	showGenerator: function() {
		this.set('generatorIsHidden', false);
	},

	dbType: '',
	//not smart...will revisit this
	dbTypeStore: {
		mtype: 'store',
		fields: ['name', 'value', 'driver'],
		proxy: {
			type: 'memory'
		},
		data: [{
			name: 'MySQL',
			driver: 'org.mariadb.jdbc.Driver',
			value: 'mysql'
		}, {
			name: 'Oracle-SID',
			driver: "oracle.jdbc.OracleDriver",
			value: 'oraclesid'
		}, {
			name: 'Oracle-Service',
			driver: "oracle.jdbc.OracleDriver",
			value: 'oracleservice'
		}, {
			name: 'DB2',
			driver: 'org.mariadb.jdbc.Driver',
			value: 'db2'
		}, {
			name: 'MSSQL',
			driver: 'com.microsoft.sqlserver.jdbc.SQLServerDriver',
			value: 'mssql'
		}, {
			name: 'PostgreSQL',
			driver: 'org.postgresql.Driver',
			value: 'postgresql'
		}]
	},
	ipAddress: '',
	port: '',
	dbName: '',
	dbNameIsVisible$: function() {
		return this.dbType == 'mysql' || this.dbType == 'db2' || this.dbType == 'mssql' || this.dbType == 'postgresql';
	},
	sid: '',
	sidIsVisible$: function() {
		return this.dbType == 'oraclesid';
	},
	service: '',
	serviceIsVisible$: function() {
		return this.dbType == 'oracleservice';
	},
	howToGetURI: '',
	dbInfo: false,
	custom: true,
	generateURI: function() {
		var url = '';
		switch (this.dbType) {
			case 'mysql':
				url = 'jdbc:mysql://' + this.ipAddress + ':' + this.port + '/' + this.dbName;
				break;
			case 'oraclesid':
				url = 'jdbc:oracle:thin:@' + this.ipAddress + ':' + this.port + ':' + this.sid;
				break;
			case 'oracleservice':
				url = 'jdbc:oracle:thin:@//' + this.ipAddress + ':' + this.port + '/' + this.service;
				break;
			case 'db2':
				url = 'jdbc:db2://' + this.ipAddress + ':' + this.port + '/' + this.dbName;
				break;
			case 'mssql':
				url = 'jdbc:microsoft:sqlserver://' + this.ipAddress + ':' + this.port + ';' + 'databaseName=' + this.dbName;
				break;
			case 'postgresql':
				url = 'jdbc:postgresql://' + this.ipAddress + ':' + this.port + '/' + this.dbName;
				break;
		}
		this.set('udriverClass', this.dbTypeStore.getAt(this.dbTypeStore.findExact('value', this.dbType)).get('driver'));
		this.set('udbconnectURI', url);
	},
	dbTypeChanged$: function() {
		this.set('ipAddress', '');
		this.set('port', '');
		this.set('dbName', '');
		this.set('sid', '');
		this.set('service', '');
		switch (this.dbType) {
			case 'mysql':
				this.set('port', 3306);
				break;
			case 'oraclesid':
				this.set('port', 1521);
				break;
			case 'oracleservice':
				this.set('port', 1521)
				break;
			case 'db2':
				this.set('port', 50000);
				break;
			case 'mssql':
				this.set('port', 1433);
				break;
			case 'postgresql':
				this.set('port', 5432);
				break;
		}
	},

	udriverClassIsVisible$: function() {
		return this.custom;
	}
});