glu.defModel('RS.gateway.HTTPFilter', {
	// MAX_N_FILTERED_RESPONSE_CHAR: 10000,
	DEFAULT: 1,
	GATEWAY: 2,
	WORKSHEET: 3,
	RUNBOOK: 4,
	FILTERED_RESPONSE: 5,
	CDEFAULT: 'default',
	CJSON: 'application/json',
	uport: '',
	modelName: 'HTTPFilter',

	mixins: ['BaseFilter'],
	gatewaytype: 'http',
	fields: RS.gateway.commonFields.concat(['uuri', 'uport', 'uallowedIP', 'uhttpAuthType',
		{
		name: 'ussl',
		type: 'bool'
		}, 'u_bas_icAu_thUs_ername', 'u_bas_icAu_thPas_sword' 
		,{
			name : 'ublocking'
		}, 'ucontentType']),
	blockStore : null,
	contentTypeStore: {
		mtype: 'store',
		fields: ['value', 'display'],
		proxy: 'memory'
	},
	defaultData : {
		ublocking : 1,
		ucontentType: 'application/json'
	},
	unpwHidden$: function() { return this.uhttpAuthType != "BASIC"},
	contentCbTypeDisabled$: function () {
		// The content type combo is disabled on "Default", "Worksheet Id" or "Execution Complete" 
		if (this.ublocking === this.DEFAULT || this.ublocking === this.WORKSHEET || this.ublocking === this.RUNBOOK || this.ublocking === this.FILTERED_RESPONSE) {
			this.set('ucontentType', this.CJSON);
			return true;
		} else if (this.ublocking === this.GATEWAY) {
			this.set('ucontentType', this.CDEFAULT);
		}
		return false;
		
	},
	//filteredResponseSelected$: function () {
		// Filtered Response only show if "Filtered Response" is selected
	//	return this.ublocking === this.FILTERED_RESPONSE;
	//},
	// uresponseFilterIsValid$: function() {
	// 	return !this.filteredResponseSelected$() || this.uresponsefilter && this.uresponsefilter.length <= this.MAX_N_FILTERED_RESPONSE_CHAR;
	// },

	init: function() {
		this.initBase();
		this.contentTypeStore.loadData([
			{value: this.CDEFAULT, display: this.localize('contentTypeDefault') },
			{value: this.CJSON, display: this.localize('contentTypeJSON')}
		]);
		var blockStore = Ext.create('Ext.data.Store',{
			fields : ['value', 'display'],
			proxy : {
				type : 'memory'
			},
			data : [
			{
				value : this.DEFAULT,
				display : this.localize('defaultBlockCall')
			},{
				value : this.GATEWAY,
				display : this.localize('gatewayBlockCall')
			},{
				value : this.WORKSHEET,
				display : this.localize('worksheetBlockCall')
			},{
				value : this.RUNBOOK,
				display : this.localize('runbookBlockCall')
			}, {
				value : this.FILTERED_RESPONSE,
				display : this.localize('filteredResponseBlockCall')
			}]
		});
		this.set('blockStore', blockStore);
		this.set('title', this.localize('httpFilterTitle'));
	}
});
