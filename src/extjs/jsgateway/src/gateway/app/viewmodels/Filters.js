glu.defModel('RS.gateway.Filters', {
	title: 'Add Filters',
	// modelName:'EmailFilter',

	filtersSelections: [],

	init: function() {},

	addFilters: function() {
		this.parentVM.addFilterFromWindow(this.filtersSelections);
		this.cancel();
	},

	cancel: function() {
		this.doClose();
	}
});