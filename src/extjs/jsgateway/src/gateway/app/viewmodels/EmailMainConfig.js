RS.gateway.email = {
	modelName: 'email',
	filterViewName: 'RS.gateway.EmailFilter',
	requestParams: {
		modelName: 'EmailFilter',
		queueName: 'EMAIL',
		gatewayName: 'EMAIL',
		gatewayClass: 'MEmail',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uquery~~',
		dataIndex: 'uquery',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'uquery',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('emailFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('emailGatewayDisplayName'));
		this.set('title', this.localize('emailTitle'));
	}
};

glu.defModel('RS.gateway.EmailMain', {
	mixins: ['Main'],
	gatewaytype: 'email'
});