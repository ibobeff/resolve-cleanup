RS.gateway.CASpectrum = {
	modelName: 'CASpectrum',
	filterViewName: 'RS.gateway.CASpectrumFilter',
	requestParams: {
		modelName: 'CASpectrumFilter',
		queueName: 'CASPECTRUM',
		gatewayName: 'CASPECTRUM',
		gatewayClass: 'MCASpectrum',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uobject~~',
		dataIndex: 'uobject',
		filterable: true,
		flex: 1
	}, {
		header: '~~uurlQuery~~',
		dataIndex: 'uurlQuery',
		filterable: true,
		flex: 1,
		renderer: Ext.String.htmlEncode
	},
	{
		header: '~~uxmlQuery~~',
		dataIndex: 'uxmlQuery',
		filterable: true,
		flex: 1,
	 	renderer: Ext.String.htmlEncode
	}],

	additionalStoreFields: [{
		name: 'uobject',
		type: 'string'
	}, {
		name: 'uurlQuery',
		type: 'string'
	},
	{
		name: 'uxmlQuery',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('CASpectrumFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('CASpectrumGatewayDisplayName'));
		this.set('title', this.localize('CASpectrumTitle'));
	}
};

glu.defModel('RS.gateway.CASpectrumMain', {
	mixins: ['Main'],
	gatewaytype: 'CASpectrum'
});