glu.defModel('RS.gateway.ExchangeFilter', {
	usubject: '',
	ucontent: '',
	usendername: '',
	usenderaddress: '',
	modelName: 'ExchangeserverFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'exchange',
	fields: RS.gateway.commonFields.concat(['usubject', 'ucontent', 'usendername', 'usenderaddress']),
	// usubjectIsValid$:function(){
	// 	return this.usubject&&this.usubject!=''?true:this.localize('invalidQuery');
	// },
	// ucontentIsValid$:function(){
	// 	return this.ucontent&&this.ucontent!=''?true:this.localize('invalidQuery');
	// },
	// usendernameIsValid$:function(){
	// 	return this.usendername&&this.usendername!=''?true:this.localize('invalidQuery');
	// },
	// usenderaddressIsValid$:function(){
	// 	return this.usenderaddress&&this.usenderaddress!=''?true:this.localize('invalidQuery');
	// },
	init: function() {
		this.initBase();
		this.set('title', this.localize('exchangeFilterTitle'));
	}
});