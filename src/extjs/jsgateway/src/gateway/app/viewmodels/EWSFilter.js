glu.defModel('RS.gateway.EWSFilter', {
	uquery: '',
	uincludeAttachment: '',
	ufolderNames: '',
    uonlyNew: '',
	modelName: 'EWSFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'ews',
	fields: RS.gateway.commonFields.concat(['uquery', {
		name: 'uincludeAttachment',
		type: 'bool'
	},'ufolderNames',{
		name: 'umarkAsRead',
		type: 'bool'
	},{
		name: 'uonlyNew',
		type: 'bool',
        defaultValue: true,
        convert: function(val) {
            if (val == null)
                return true;
            else
                return val;
        }
    }]),
    defaultData : {
        uonlyNew : true
    },
	// uqueryIsValid$:function(){
	// 	return this.uquery&&this.uquery!=''?true:this.localize('invalidQuery');
	// },

	// uincludeAttachmentIsValid$:function(){
	// 	return this.uincludeAttachment&&this.uincludeAttachment!=''?true:this.localize('invalidQuery');
	// },

	init: function() {
		this.initBase();
		this.set('title', this.localize('ewsFilterTitle'));
	}
});