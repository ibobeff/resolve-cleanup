RS.gateway.telnetPool = {
	modelName: 'telnetPool',
	uniqueIdField: 'usubnetMask',
	filterViewName: 'RS.gateway.TelnetPool',
	requestParams: {
		modelName: 'TelnetPool',
		queueName: 'TELNET',
		gatewayName: 'TELNET',
		gatewayClass: 'MTelnet',
		getterMethod: 'getPools',
		clearAndSetMethod: 'clearAndSetPools',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},

	concatColumns: function() {
		this.set('filterColumns', [{
			header: '~~usubnetMask~~',
			dataIndex: 'usubnetMask',
			filterable: true,
			flex: 1
		}, {
			header: '~~umaxConn~~',
			dataIndex: 'umaxConn',
			filterable: true,
			flex: 1
		}, {
			header: '~~utimeout~~',
			dataIndex: 'utimeout',
			filterable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()));
	},

	config: function() {
		this.set('filterDisplayName', this.localize('telnetPoolDisplayName'));
		this.set('title', this.localize('telnetPoolTitle'));
	},

	getStoreFields: function() {
		if (!this.gatewaytype)
			alert('Need to specify a gatewaytype!');
		var fields = [
			'id',
			'usubnetMask',
			'umaxConn',
			'utimeout'
		].concat(RS.common.grid.getSysFields());
		return fields;
	},

	createFilter: function() {
		this.open({
			mtype: this.filterViewName
		});
	},
	editFilter: function(id) {
		this.open({
			mtype: this.filterViewName,
			uniqueIdField: this.uniqueIdField,
			gatewaytype: this.gatewaytype,
			id: id
		});
	}
};

glu.defModel('RS.gateway.TelnetPoolMain', {
	mixins: ['Main'],
	gatewaytype: 'telnetPool'
});