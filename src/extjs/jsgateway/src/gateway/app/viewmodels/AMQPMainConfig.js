RS.gateway.amqp = {
	modelName: 'amqp',
	filterViewName: 'RS.gateway.AMQPFilter',
	requestParams: {
		modelName: 'AmqpFilter',
		queueName: 'AMQP',
		gatewayName: 'AMQP',
		gatewayClass: 'MAmqp',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~utargetQueue~~',
		dataIndex: 'utargetQueue',
		filterable: true,
		flex: 1
	}, {
		header: '~~uexchange~~',
		dataIndex: 'uexchange',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'utargetQueue',
		type: 'string'
	}, {
		name: 'uexchange',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('amqpFilterDisplayName'));
		this.set('title', this.localize('amqpTitle'));
	}
};

glu.defModel('RS.gateway.AMQPMain', {
	mixins: ['Main'],
	gatewaytype: 'amqp'
});