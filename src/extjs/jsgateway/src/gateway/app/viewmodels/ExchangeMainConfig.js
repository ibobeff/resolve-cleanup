RS.gateway.exchange = {
	modelName: 'exchange',
	filterViewName: 'RS.gateway.ExchangeFilter',
	requestParams: {
		modelName: 'ExchangeserverFilter',
		queueName: 'EXCHANGE',
		gatewayName: 'EXCHANGE',
		gatewayClass: 'MExchange',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~usubject~~',
		dataIndex: 'usubject',
		filterable: true,
		flex: 1
	}, {
		header: '~~ucontent~~',
		dataIndex: 'ucontent',
		filterable: true,
		flex: 1
	}, {
		header: '~~usendername~~',
		dataIndex: 'usendername',
		filterable: true,
		flex: 1
	}, {
		header: '~~usenderaddress~~',
		dataIndex: 'usenderaddress',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'usubject',
		type: 'string'
	}, {
		name: 'ucontent',
		type: 'string'
	}, {
		name: 'usendername',
		type: 'string'
	}, {
		name: 'usenderaddress',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('exchangeFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('exchangeGatewayDisplayName'));
		this.set('title', this.localize('exchangeTitle'));
	}
};

glu.defModel('RS.gateway.ExchangeMain', {
	mixins: ['Main'],
	gatewaytype: 'exchange'
});