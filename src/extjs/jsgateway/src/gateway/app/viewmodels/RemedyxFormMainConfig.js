RS.gateway.remedyxForm = {
	modelName: 'remedyxForm',
	filterViewName: 'RS.gateway.RemedyxForm',
	requestParams: {
		modelName: 'RemedyxForm',
		queueName: 'REMEDYX',
		gatewayName: 'REMEDYX',
		gatewayClass: 'MRemedyx',
		getterMethod: 'getForms',
		clearAndSetMethod: 'clearAndSetForms',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},

	getStoreFields: function() {
		if (!this.gatewaytype)
			alert('Need to specify a gatewaytype!');
		var fields = [
			'id',
			'uname',
			'ufieldList'
		].concat(RS.common.grid.getSysFields());
		return fields;
	},
	concatColumns: function() {
		this.set('filterColumns', [{
			header: '~~uname~~',
			dataIndex: 'uname',
			filterable: true,
			flex: 1
		}, {
			header: '~~ufieldList~~',
			dataIndex: 'ufieldList',
			filterable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()));
	},
	config: function() {
		this.set('filterDisplayName', this.localize('remedyxFormDisplayName'));
		this.set('title', this.localize('remedyxFormTitle'));
	},
	createFilter: function() {
		this.open({
			mtype: this.filterViewName
		});
	},
	editFilter: function(id) {
		this.open({
			mtype: this.filterViewName,
			gatewaytype: this.gatewaytype,
			id: id
		});
	}
};

glu.defModel('RS.gateway.RemedyxFormMain', {
	mixins: ['Main'],
	gatewaytype: 'remedyxForm'
});