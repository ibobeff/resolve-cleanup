glu.defModel('RS.gateway.RemedyxFilter', {
	uformName: '',
	uquery: '',
	ulastValueField: '',
	ignoreName: true,
	modelName: 'RemedyxFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'remedyx',
	fields: RS.gateway.commonFields.concat(['uformName', 'uquery', 'ulastValueField', 'ulimit']),
	// uqueryIsValid$:function(){
	// 	return this.uquery&&this.uquery!=''?true:this.localize('invalidQuery');
	// },

	init: function() {
		this.initBase();
		this.set('title', this.localize('remedyxFilterTitle'));
	}
});