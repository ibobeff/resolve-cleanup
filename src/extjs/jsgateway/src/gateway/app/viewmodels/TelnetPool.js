glu.defModel('RS.gateway.TelnetPool', {
	ignoreName: true,
	usubnetMask: '',
	utimeout: '',
	umaxConn: '',
	modelName: 'TelnetPool',
	mixins: ['BaseFilter'],
	gatewaytype: 'databaseConnectionPool',
	fields: ['upoolName', 'usubnetMask', 'utimeout', 'umaxConn'],
	driverStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	init: function() {
		this.initBase();
		this.set('title', this.localize('telnetPoolTitle'));
		this.activate(screen, {
			id: this.id,
			uniqueIdField: this.uniqueIdField
		});
	},
	userDateFormat$: function() {},
	usubnetMaskIsValid$: function() {
		var test = /^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/;
		return test.test(this.usubnetMask) ? true : this.localize('invalidSubnetMask');
	},
	umaxConnIsValid$: function() {
		var test = /^[0-9]*$/;
		return test.test(this.umaxConn) ? true : this.localize('invalidMaxConn');
	},
	utimeoutIsValid$: function() {
		var test = /^[0-9]*$/;
		return test.test(this.utimeout) ? true : this.localize('invalidMaxConn');
	},
	handleSaveSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(this.localize('SaveErrMsg') + '[' + respData.message + ']');
		else {
			clientVM.displaySuccess(this.localize('SaveSucMsg'));
			this.cancel();
		}
		this.parentVM.loadFilters();
		this.parentVM.loadDeployedFilters();
	},

	respErr: function(resp) {
		clientVM.displayFailure(resp);
	},
	cancel: function() {
		this.doClose();
	}
});