RS.gateway.emailAddress = {
	modelName: 'emailAddress',
	uniqueIdField: 'uemailAddress',
	filterViewName: 'RS.gateway.EmailAddress',
	requestParams: {
		modelName: 'EmailAddress',
		queueName: 'EMAIL',
		gatewayName: 'EMAIL',
		gatewayClass: 'MEmail',
		getterMethod: 'getEmailAddresses',
		clearAndSetMethod: 'clearAndSetEmailAddresses',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},

	getStoreFields: function() {
		if (!this.gatewaytype)
			alert('Need to specify a gatewaytype!');
		var fields = [
			'id',
			'uqueue',
			'uemailAddress',
            {
                name: 'changed',
                type: 'bool',
                defaultValue: false
            }
		].concat(RS.common.grid.getSysFields());
		return fields;
	},
	concatColumns: function() {
		this.set('filterColumns', [{
			header: '~~uemailAddress~~',
			dataIndex: 'uemailAddress',
			filterable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()));
	},
	config: function() {
		this.set('filterDisplayName', this.localize('emailAddressDisplayName'));
		this.set('title', this.localize('emailAddressTitle'));
	},

	createFilter: function() {
		this.open({
			mtype: this.filterViewName
		});
	},
	editFilter: function(id) {
		this.open({
			mtype: this.filterViewName,
			uniqueIdField: this.uniqueIdField,
			gatewaytype: this.gatewaytype,
			id: id
		});
	}
};

glu.defModel('RS.gateway.EmailAddressMain', {
	mixins: ['Main'],
	gatewaytype: 'emailAddress'
});