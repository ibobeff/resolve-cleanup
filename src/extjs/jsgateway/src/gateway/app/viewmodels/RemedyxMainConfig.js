RS.gateway.remedyx = {
	modelName: 'remedyx',
	filterViewName: 'RS.gateway.RemedyxFilter',
	requestParams: {
		modelName: 'RemedyxFilter',
		queueName: 'REMEDYX',
		gatewayName: 'REMEDYX',
		gatewayClass: 'MRemedyx',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uformName~~',
		dataIndex: 'uformName',
		filterable: true,
		flex: 1
	}, {
		header: '~~uquery~~',
		dataIndex: 'uquery',
		filterable: true,
		flex: 1
	}, {
		header: '~~ulimit~~',
		dataIndex: 'ulimit',
		filterable: true,
		flex: 1
	}, {
		header: '~~ulastValueField~~',
		dataIndex: 'ulastValueField',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'uformName',
		type: 'string'
	}, {
		name: 'uquery',
		type: 'string'
	}, {
		name: 'ulastValueField',
		type: 'string'
	}, {
		name: 'ulimit',
		type: 'integer'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('remedyxFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('remedyxGatewayDisplayName'));
		this.set('title', this.localize('remedyxTitle'));
	}
};

glu.defModel('RS.gateway.RemedyxMain', {
	mixins: ['Main'],
	gatewaytype: 'remedyx'
});