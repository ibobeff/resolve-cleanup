glu.defModel('RS.gateway.NetcoolFilter', {
	usql: '',
	modelName: 'NetcoolFilter',
	gatewaytype: 'netcool',
	mixins: ['BaseFilter'],
	fields: RS.gateway.commonFields.concat(['usql']),
	// usqlIsValid$:function(){
	// 	return this.usql&&this.usql!=''?true:this.localize('invalidSql');
	// },

	init: function() {
		this.initBase();
		this.set('title', this.localize('netcoolFilterTitle'));
	}
});