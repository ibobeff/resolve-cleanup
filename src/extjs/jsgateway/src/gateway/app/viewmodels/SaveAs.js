glu.defModel('RS.gateway.SaveAs', {
	name: '',
	nameIsValid$: function() {
		var me = this;
		var obj = {
			localize: function(text) {
				return me.parentVM.localize(text);
			}
		};
		obj[this.uniqueIdField] = this.name;
		var valid = this.parentVM[this.uniqueIdField + 'IsValid$'].apply(obj);
		return valid;
	},
	label: '',
	uniqueIdField: '',
	title: '',
	msg: '',
	dumper: null,
	init: function() {
		this.set('msg', this.localize('saveAsNewFilterMsg', {
			name: this.localize(this.uniqueIdField)
		}));
		this.set('label', this.localize(this.uniqueIdField));
		this.set('title', this.localize('saveAsNewFilterTitle'));
	},
	save: function() {
		this.dumper();
		this.close();
	},
	close: function() {
		this.doClose();
	}
});