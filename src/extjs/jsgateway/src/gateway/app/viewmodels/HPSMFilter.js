glu.defModel('RS.gateway.HPSMFilter', {
	uquery: '',
	modelName: 'HPSMFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'hpom',
	objectStore: {
		mtype: 'store',
		fields: ['uobject'],
		proxy: {
			type: 'memory'
		}
	},
	fields: RS.gateway.commonFields.concat(['uobject', 'uquery']),
	activate: function(screen, params) {
		this.activateBase(params);
		this.loadObjects();
	},
	init: function() {
		this.initBase();
		this.set('title', this.localize('hpsmFilterTitle'));
	},
	loadObjects: function() {
		this.ajax({
			url: '/resolve/service/gateway/listObjectNames',
			params: {
				gatewayName: 'HPSM'
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('LoadObjErr') + '[' + respData.message + ']');
					return;
				}
				var objs = respData.records || {};
				this.objectStore.removeAll();
				Ext.each(objs, function(o) {
					this.objectStore.add({
						uobject: o
					});
				}, this);
				if (!this.uobject)
					this.set('uobject', this.objectStore.first().get('uobject'));
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	}
});