RS.gateway.xmppAddress = {
	modelName: 'xmppAddress',
	uniqueIdField: 'uxmppaddress',
	filterViewName: 'RS.gateway.XMPPAddress',
	requestParams: {
		modelName: 'XMPPAddress',
		queueName: 'XMPP',
		gatewayName: 'XMPP',
		gatewayClass: 'MXMPP',
		getterMethod: 'getXMPPAddresses',
		clearAndSetMethod: 'clearAndSetXMPPAddresses',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},

	getStoreFields: function() {
		if (!this.gatewaytype)
			alert('Need to specify a gatewaytype!');
		var fields = [
			'id',
			'uxmppaddress'
		].concat(RS.common.grid.getSysFields());
		return fields;
	},
	concatColumns: function() {
		this.set('filterColumns', [{
			header: '~~uxmppaddress~~',
			dataIndex: 'uxmppaddress',
			filterable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()));
	},
	config: function() {
		this.set('filterDisplayName', this.localize('xmppAddressDisplayName'));
		this.set('title', this.localize('xmppAddressTitle'));
	},

	createFilter: function() {
		this.open({
			mtype: this.filterViewName
		});
	},
	editFilter: function(id) {
		this.open({
			mtype: this.filterViewName,
			uniqueIdField: this.uniqueIdField,
			gatewaytype: this.gatewaytype,
			id: id
		});
	}
};

glu.defModel('RS.gateway.XMPPAddressMain', {
	mixins: ['Main'],
	gatewaytype: 'xmppAddress'
});