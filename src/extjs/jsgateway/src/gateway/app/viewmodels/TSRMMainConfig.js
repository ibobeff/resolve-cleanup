RS.gateway.tsrm = {
	modelName: 'tsrm',
	filterViewName: 'RS.gateway.TSRMFilter',
	requestParams: {
		modelName: 'TSRMFilter',
		queueName: 'TSRM',
		gatewayName: 'TSRM',
		gatewayClass: 'MTSRM',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},

	additionalColumns: [{
		header: '~~uobject~~',
		dataIndex: 'uobject',
		filterable: true,
		flex: 1
	}, {
		header: '~~uquery~~',
		dataIndex: 'uquery',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'uobject',
		type: 'string'
	}, {
		name: 'uquery',
		type: 'string'
	}],

	config: function() {
		this.set('filterDisplayName', this.localize('tsrmFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('tsrmGatewayDisplayName'));
		this.set('title', this.localize('tsrmTitle'));
	}
};

glu.defModel('RS.gateway.TSRMMain', {
	mixins: ['Main'],
	gatewaytype: 'tsrm'
});