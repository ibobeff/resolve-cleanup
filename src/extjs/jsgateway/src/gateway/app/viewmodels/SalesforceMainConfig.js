RS.gateway.salesforce = {
	modelName: 'salesforce',
	filterViewName: 'RS.gateway.SalesforceFilter',
	requestParams: {
		modelName: 'SalesforceFilter',
		queueName: 'SALESFORCE',
		gatewayName: 'SALESFORCE',
		gatewayClass: 'MSalesforce',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uobject~~',
		dataIndex: 'uobject',
		filterable: true,
		flex: 1
	}, {
		header: '~~uquery~~',
		dataIndex: 'uquery',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'uobject',
		type: 'string'
	}, {
		name: 'uquery',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('salesforceFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('salesforceGatewayDisplayName'));
		this.set('title', this.localize('salesforceTitle'));
	}
};

glu.defModel('RS.gateway.SalesforceMain', {
	mixins: ['Main'],
	gatewaytype: 'salesforce'
});