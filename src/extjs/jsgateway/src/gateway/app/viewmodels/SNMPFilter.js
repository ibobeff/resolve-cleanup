glu.defModel('RS.gateway.SNMPFilter', {
	utrapReceiver: '',
	usnmpTrapOid: '',
	usnmpVersion: '',
	uipAddresses: '',
	ureadCommunity: '',
	utimeout: '',
	uretries: '',
	uregex: '',
	uoid: '',
	ucomparator: '',
	uvalue: '',

	modelName: 'SNMPFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'snmp',
	fields: RS.gateway.commonFields.concat([{
			name: 'utrapReceiver',
			type: 'bool'
		},
		'usnmpTrapOid',
		'usnmpVersion',
		'uipAddresses',
		'ureadCommunity',
		'utimeout',
		'uretries',
		'uregex',
		'uoid',
		'ucomparator',
		'uvalue'
	]),

	init: function() {
		this.initBase();
		this.set('title', this.localize('snmpFilterTitle'));
	},
	usnmpVersionIsValid$: function() {
		var test = /^[0-9]*$/;
		return test.test(this.usnmpVersion) ? true : this.localize('invalidVersion');
	},

	utimeoutIsValid$: function() {
		var test = /^[0-9]*$/;
		return test.test(this.utimeout) ? true : this.localize('invalidTimeout');
	},

	uretriesIsValid$: function() {
		var test = /^[0-9]*$/;
		return test.test(this.uretries) ? true : this.localize('invalidTimeout');
	},

	uvalueIsValid$: function() {
		var test = /^[0-9]*$/;
		return test.test(this.uvalue) ? true : this.localize('invalidTimeout');
	}
});