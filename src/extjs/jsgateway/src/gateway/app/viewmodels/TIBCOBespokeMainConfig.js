RS.gateway.tibcoBespoke = {
	modelName: 'tibcoBespoke',
	filterViewName: 'RS.gateway.TIBCOBespokeFilter',
	requestParams: {
		modelName: 'TIBCOBespokeFilter',
		queueName: 'TIBCOBESPOKE',
		gatewayName: 'TIBCOBESPOKE',
		gatewayClass: 'MTIBCOBespoke',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uregex~~',
		dataIndex: 'uregex',
		filterable: true,
		hidden: true,
		flex: 1
	},{
		header: '~~ubusUri~~',
		dataIndex: 'ubusUri',
		filterable: true,
		flex: 1
	},{
		header: '~~utopic~~',
		dataIndex: 'utopic',
		filterable: true,
		flex: 1
	},{
		header: '~~ucertifiedMessaging~~',
		dataIndex: 'ucertifiedMessaging',
		filterable: true,
		flex: 1
	},{
		header: '~~uunescapeXml~~',
		dataIndex: 'uunescapeXml',
		filterable: true,
		hidden: true,
		flex: 1
	},{
		header: '~~uprocessAsXml~~',
		dataIndex: 'uprocessAsXml',
		filterable: true,
		hidden: true,
		flex: 1
	},{
		header: '~~urequireReply~~',
		dataIndex: 'urequireReply',
		filterable: true,
		flex: 1
	},{
		header: '~~ureplyTimeout~~',
		dataIndex: 'ureplyTimeout',
		hidden: true,
		filterable: true,
		flex: 1
	},{
		header: '~~uxmlQuery~~',
		dataIndex: 'uxmlQuery',
		filterable: true,
		flex: 1
	}],
	additionalStoreFields: [{
		name: 'uxmlQuery',
		type: 'string'
	},{
		name: 'uregex',
		type: 'string'
	},{
		name: 'ubusUri',
		type: 'string',
	},{
		name: 'utopic',
		type: 'string'
	},{
		name: 'ucertifiedMessaging',
		type: 'boolean'
	},{
		name: 'uunescapeXml',
		type: 'boolean'
	},{
		name: 'uprocessAsXml',
		type: 'boolean'
	},{
		name: 'urequireReply',
		type: 'boolean'
	},{
		name: 'ureplyTimeout',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('tibcoBespokeFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('tibcoBespokeGatewayDisplayName'));
		this.set('title', this.localize('tibcoBespokeTitle'));
	}
};

glu.defModel('RS.gateway.TIBCOBespokeMain', {
	mixins: ['Main'],
	gatewaytype: 'tibcoBespoke'
});