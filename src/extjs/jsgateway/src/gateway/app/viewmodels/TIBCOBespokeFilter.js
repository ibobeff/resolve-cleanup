glu.defModel('RS.gateway.TIBCOBespokeFilter', {
	uregex: '',
	modelName: 'TIBCOBespokeFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'tibcoBespoke',
	fields: RS.gateway.commonFields.concat(['uregex','ubusUri','utopic','ucertifiedMessaging','uxmlQuery','uunescapeXml','uprocessAsXml','urequireReply','ureplyTimeout']),

	defaultData:
	{
		uprocessAsXml: true,
		ureplyTimeout: '60000'
	},

	init: function() {
		this.initBase();
		this.set('title', this.localize('tibcoBespokeFilterTitle'));
	}
});