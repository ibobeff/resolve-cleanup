RS.gateway.http = {
	modelName: 'http',
	filterViewName: 'RS.gateway.HTTPFilter',
	requestParams: {
		modelName: 'HTTPFilter',
		queueName: 'HTTP',
		gatewayName: 'HTTP',
		gatewayClass: 'MHttp',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
        header: '~~uuri~~',
        dataIndex: 'uuri',
        filterable: true,
        renderer: RS.common.grid.plainRenderer(),
        flex: 2
    },{
		header: '~~uport~~',
		dataIndex: 'uport',
		filterable: true,
		flex: 1
	},{
        header: '~~ussl~~',
        dataIndex: 'ussl',
		renderer: RS.common.grid.booleanRenderer(),
        filterable: true,
        flex: 1
/*    },{
        header: '~~uallowedIP~~',
        dataIndex: 'uallowedIP',
        filterable: true,
        renderer: RS.common.grid.plainRenderer(),
        flex: 1 */
    },{
    	header: '~~ublocking~~',
        dataIndex: 'ublocking',
		renderer: RS.common.grid.booleanRenderer(),
        filterable: true,
        flex: 1
    }],

	additionalStoreFields: [{
        name: 'uuri',
        type: 'string'
	},{
        name: 'uport',
        type: 'string'
    },{
        name: 'ussl',
        type: 'string'
    },{
    	name : 'ublocking',
    	type : 'bool',
        convert : function(val){
            return val == 1 ? false : true;
        }
    },{
        name: 'uallowedIP',
        type: 'string'
    },{
        name: 'u_bas_icAu_thPas_sword',
        type: 'string'
    },{
        name: 'u_bas_icAu_thUs_ername',
        type: 'string'
    }],
	config: function() {
		this.set('filterDisplayName', this.localize('httpFilterDisplayName'));
		this.set('title', this.localize('httpTitle'));
	}
};

glu.defModel('RS.gateway.HTTPMain', {
	mixins: ['Main'],
	gatewaytype: 'http'
});