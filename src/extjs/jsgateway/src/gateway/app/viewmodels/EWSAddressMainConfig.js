RS.gateway.ewsAddress = {
	modelName: 'ewsAddress',
	filterViewName: 'RS.gateway.EWSAddress',
	uniqueIdField: 'uewsaddress',
	requestParams: {
		modelName: 'EWSAddress',
		queueName: 'EWS',
		gatewayName: 'EWS',
		gatewayClass: 'MEWS',
		getterMethod: 'getEWSAddress',
		clearAndSetMethod: 'clearAndSetEWSAddresses',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	getStoreFields: function() {
		if (!this.gatewaytype)
			alert('Need to specify a gatewaytype!');
		var fields = [
			'id',
			'uewsaddress',
            'uqueue',
            { 
                name: 'changed',
                type: 'bool',
                defaultValue: false
            }
		].concat(RS.common.grid.getSysFields());
		return fields;
	},
	concatColumns: function() {
		this.set('filterColumns', [{
			header: '~~uewsaddress~~',
			dataIndex: 'uewsaddress',
			filterable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()));
	},
	config: function() {
		this.set('filterDisplayName', this.localize('ewsAddressFilterDisplayName'));
		this.set('title', this.localize('ewsAddressTitle'));
	},
	createFilter: function() {
		this.open({
			mtype: this.filterViewName
		});
	},
	editFilter: function(id) {
		this.open({
			mtype: this.filterViewName,
			uniqueIdField: this.uniqueIdField,
			gatewaytype: this.gatewaytype,
			id: id
		});
	}
};

glu.defModel('RS.gateway.EWSAddressMain', {
	mixins: ['Main'],
	gatewaytype: 'ewsAddress'
});