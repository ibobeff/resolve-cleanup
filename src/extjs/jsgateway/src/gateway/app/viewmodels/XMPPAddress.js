glu.defModel('RS.gateway.XMPPAddress', {
	ignoreName: true,
	uxmppaddress: '',
	uxmppaddressIsValid$: function() {
		return this.uxmppaddress ? true : this.localize('invalidAddress');
	},
	uxmpppassword: '',
	modelName: 'XMPPAddress',
	mixins: ['BaseFilter'],
	gatewaytype: 'xmppAddress',
	fields: ['uxmppaddress', 'uxmpppassword', 'uniqueId'],

	init: function() {
		this.initBase();
		this.set('title', this.localize('xmppAddressTitle'));
		this.activate(screen, {
			id: this.id,
			uniqueIdField: this.uniqueIdField
		});
	},

	userDateFormat$: function() {},

	handleSaveSuccess: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(this.localize('SaveErrMsg') + '[' + respData.message + ']');
		else {
			clientVM.displaySuccess(this.localize('SaveSucMsg'));
			this.cancel();
		}

		this.parentVM.loadFilters();
		this.parentVM.loadDeployedFilters();
	},

	respErr: function(resp) {
		clientVM.displayFailure(resp);
	},

	cancel: function() {
		this.doClose();
	}
});