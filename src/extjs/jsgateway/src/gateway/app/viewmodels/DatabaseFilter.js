glu.defModel('RS.gateway.DatabaseFilter', {
	usql: '',
	uupdateSql: '',
	upool: '',
	ulastValue: '',
	ulastValueColumn: '',
	uprefix: '',
	ulastValueQuote: '',
	modelName: 'DatabaseFilter',
	mixins: ['BaseFilter'],
	gatewaytype: 'database',
	fields: RS.gateway.commonFields.concat([
		'usql',
		'uupdateSql',
		'upool',
		'ulastValue',
		'ulastValueColumn',
		'uprefix',
		'ulastValueQuote'
	]),
	poolStore: {
		mtype: 'store',
		fields: ['upoolName'],
		proxy: {
			type: 'memory'
		}
	},
	init: function() {
		this.initBase();
		this.set('title', this.localize('databaseFilterTitle'));
		this.loadPool();
	},

	loadPool: function() {
		this.ajax({
			url: '/resolve/service/gateway/listFilters',
			method: 'GET',
			params: {
				modelName: 'DatabaseConnectionPool',
				start: 0,
				limit: 50
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				var records = respData.records;
				Ext.each(records, function(r) {
					this.poolStore.add({
						upoolName: r.upoolName
					});
				}, this)
				// this.poolStore.add(records);
			},
			failure: function(resp) {
				clientVM.displayError(this.localize('ServerErr'));
			}
		})
	}
});