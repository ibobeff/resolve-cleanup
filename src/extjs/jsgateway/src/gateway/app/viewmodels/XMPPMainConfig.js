RS.gateway.xmpp = {
	modelName: 'xmpp',
	filterViewName: 'RS.gateway.XMPPFilter',
	requestParams: {
		modelName: 'XMPPFilter',
		queueName: 'XMPP',
		gatewayName: 'XMPP',
		gatewayClass: 'MXMPP',
		getterMethod: 'getFilters',
		clearAndSetMethod: 'clearAndSetFilters',
		undeployFilterMethod: 'undeployFilters',
		deleteFilterNames: null
	},
	additionalColumns: [{
		header: '~~uregex~~',
		dataIndex: 'uregex',
		filterable: true,
		flex: 1
	}],

	additionalStoreFields: [{
		name: 'uregex',
		type: 'string'
	}],
	config: function() {
		this.set('filterDisplayName', this.localize('xmppFilterDisplayName'));
		this.set('gatewayDisplayName', this.localize('xmppGatewayDisplayName'));
		this.set('title', this.localize('xmppTitle'));
	}
};

glu.defModel('RS.gateway.XMPPMain', {
	mixins: ['Main'],
	gatewaytype: 'xmpp'
});