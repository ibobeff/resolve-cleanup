<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.resolve.util.JspUtils" %>
<%@page import="org.owasp.esapi.ESAPI"%>
<%
    String ver = ESAPI.encoder().encodeForURL(JspUtils.getResolveVersion());

    boolean debug = false;
    String d = request.getParameter("debug");
    if( d != null && d.indexOf("t") > -1){
        debug = true;
    }

    boolean debugGateway = false;
    String dT = request.getParameter("debugGateway");
    if( dT != null && dT.indexOf("t") > -1){
        debugGateway = true;
    }

    boolean mock = false;
    String m = request.getParameter("mock");
    if( m != null && m.indexOf("t") > -1){
        mock = true;
    }
%>

<script type="text/javascript">
glu.ns('RS.gateway');
RS.gateway.commonFields = ['uname', 'uinterval', 'uorder', 'uactive', 'ueventEventId', 'urunbook', 'uscript', 'id', {
	name: 'sysCreatedOn',
	type: 'long'
}, {
	name: 'sysUpdatedOn',
	type: 'long'
}, 'sysCreatedBy', 'sysCreatedOn', 'sysUpdatedBy', 'sysOrganizationName'];
RS.gateway.SDKcommonFields = ['uscript', 'id', {
    name: 'sysCreatedOn',
    type: 'long'
}, {
    name: 'sysUpdatedOn',
    type: 'long'
}, 'sysCreatedBy', 'sysCreatedOn', 'sysUpdatedBy', 'sysOrganizationName'];
</script>
<%
    if( debug || debugGateway ){
%>
<link rel="stylesheet" type="text/css" href="/resolve/gateway/css/gateway-all.css?_v=<%=ver%>"/>
<script type="text/javascript" src="/resolve/gateway/js/gateway-classes.js?_v=<%=ver%>"></script>
<%
    }else{
%>
<link rel="stylesheet" type="text/css" href="/resolve/gateway/css/gateway-all.css?_v=<%=ver%>"/>
<script type="text/javascript" src="/resolve/gateway/js/gateway-classes.js?_v=<%=ver%>"></script>
<%
    }
%>