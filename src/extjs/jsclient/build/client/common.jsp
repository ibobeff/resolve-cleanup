<!-- ******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
********************************************************************************* -->
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="java.io.*" %> 
<%@page import="java.util.*" %> 
<%@page import="com.resolve.util.JspUtils" %>
<%@page import="org.owasp.esapi.ESAPI"%>
<%
    String ver = ESAPI.encoder().encodeForURL(JspUtils.getResolveVersion());

    boolean debug = false;
    String d = request.getParameter("debug");
    if( d != null && d.indexOf("t") > -1){
        debug = true;
    }

    boolean mock = false;
    String m = request.getParameter("mock");
    if( m != null && m.indexOf("t") > -1){
        mock = true;
    }

    boolean test = false;
    boolean glujsLog = false;
    Map<String, String[]> parameters = request.getParameterMap();
    for(String parameter : parameters.keySet()) {
        if(parameter.toLowerCase().startsWith("debug")) {
            glujsLog = true;
            String testString = request.getParameter(parameter);
            if( testString != null && testString.indexOf("t") > -1){
                test = true;
            }
        }
    }
%>

<!-- esapi4js dependencies -->
<script type="text/javascript" language="JavaScript" src="/resolve/js/esapi4js/lib/log4js.js?_v=<%=ver%>"></script>
<!-- esapi4js i18n resources -->
<script type="text/javascript" language="JavaScript" src="/resolve/js/esapi4js/resources/i18n/ESAPI_Standard_en_US.properties.js?_v=<%=ver%>"></script>
<!-- esapi4js core -->
<script type="text/javascript" language="JavaScript" src="/resolve/js/esapi4js/esapi-compressed.js?_v=<%=ver%>"></script>
<!-- esapi4js configuration -->
<script type="text/javascript" language="JavaScript" src="/resolve/js/esapi4js/resources/Base.esapi.properties.js?_v=<%=ver%>"></script>

<script type="text/javascript" language="JavaScript">
    // Set any custom configuration options here or in an external js file that gets sourced in above.
    Base.esapi.properties.logging['ApplicationLogger'] = {
        Level: org.owasp.esapi.Logger.OFF,
        Appenders: [ new Log4js.ConsoleAppender() ],
        LogUrl: true,
        LogApplicationName: true,
        EncodingRequired: true
    };
    Base.esapi.properties.application.Name = "Resolve";
    org.owasp.esapi.ESAPI.initialize();
</script>

<!-- Extjs Core Library -->
<%
    if( debug || test ){
%>
<link type="text/css" rel="stylesheet" href="/resolve/css/diffview.css?_v=<%=ver%>"/>
<!--<link rel="stylesheet" type="text/css" href="/resolve/css/resources/ResolveTheme-all-debug.css"/>-->
<link rel="stylesheet" type="text/css" href="/resolve/css/resources/ResolveTheme-all-debug_01.css?_v=<%=ver%>"/>
<link rel="stylesheet" type="text/css" href="/resolve/css/resources/ResolveTheme-all-debug_02.css?_v=<%=ver%>"/>
<link rel="stylesheet" type="text/css" href="/resolve/css/font-awesome.min.css?_v=<%=ver%>"/>
<script type="text/javascript" src="/resolve/js/extjs4/ext-all-debug-w-comments.js?_v=<%=ver%>"></script>
<script type="text/javascript" src="/resolve/js/extjs4/ext-theme-neptune.js?_v=<%=ver%>"></script>
<%
    }else{
%>
<link type="text/css" rel="stylesheet" href="/resolve/css/diffview.css?_v=<%=ver%>"/>
<!--<link rel="stylesheet" type="text/css" href="/resolve/css/resources/ResolveTheme-all.css"/>-->
<link rel="stylesheet" type="text/css" href="/resolve/css/resources/ResolveTheme-all-debug_01.css?_v=<%=ver%>"/>
<link rel="stylesheet" type="text/css" href="/resolve/css/resources/ResolveTheme-all-debug_02.css?_v=<%=ver%>"/>
<link rel="stylesheet" type="text/css" href="/resolve/css/font-awesome.min.css?_v=<%=ver%>"/>
<script type="text/javascript" src="/resolve/js/extjs4/ext-all.js?_v=<%=ver%>"></script>
<script type="text/javascript" src="/resolve/js/extjs4/ext-theme-neptune.js?_v=<%=ver%>"></script>
<%
    }
%>

<!-- Resolve Library -->
<script type="text/javascript" src="/resolve/js/resolve.js?_v=<%=ver%>"></script>
<script type="text/javascript" src="/resolve/js/Menu_Sections.js?_v=<%=ver%>"></script>

<!-- Status Bar -->
<link rel="stylesheet" type="text/css" href="/resolve/js/extjs4/ux/statusbar/css/statusbar.css?_v=<%=ver%>" />

<!-- HtmlEditor plugins -->
<script type="text/javascript" src="/resolve/js/extjs4/ux/htmleditor/Ext.ux.form.HtmlEditor.MidasCommand.js?_v=<%=ver%>"></script>
<script type="text/javascript" src="/resolve/js/extjs4/ux/htmleditor/Ext.ux.form.HtmlEditor.IndentOutdent.js?_v=<%=ver%>"></script>
<script type="text/javascript" src="/resolve/js/extjs4/ux/htmleditor/Ext.ux.form.HtmlEditor.Word.js?_v=<%=ver%>"></script>

<!-- GluJs Library --> 
<!--log switch-->
<%
     if( glujsLog ){
        String gluLogLevel = "off";
        if(d.equals("1")){
            gluLogLevel = "info";
        } else if(d.equals("2")) {
            gluLogLevel = "warn";
        } else if(d.equals("3")) {
            gluLogLevel = "error";
        } else if(d.equals("4")) {
            gluLogLevel = "debug";
        } 
        out.write("<script type='text/javascript'>" +
                        "glu={" + 
                            "logLevel:'" + gluLogLevel + "'" +
                        " };" + 
                    "</script>");
    }else {
%>
<script type="text/javascript">
    glu={
        logLevel:'error'
    };
</script>
<%
    }
%>
<script type="text/javascript" src="/resolve/js/glujs/glu-extjs-4.js?_v=<%=ver%>"></script>
<script type="text/javascript" src="/resolve/js/glujs/glu.js?_v=<%=ver%>"></script>

<!-- Plupload Library -->
<!--<script type="text/javascript" src="/resolve/js/plupload/plupload.full.min.js?_v=<%=ver%>"></script>-->

<!-- Fine-Uploader Library -->
<link rel="stylesheet" type="text/css" href="/resolve/js/fine-uploader/fine-uploader-new.css?_v=<%=ver%>"/>
<script type="text/javascript" src="/resolve/js/fine-uploader/fine-uploader.min.js?_v=<%=ver%>"></script>

<!-- Resolve Common and Extjs Extensions -->
<link rel="stylesheet" type="text/css" href="/resolve/css/common-all.css?_v=<%=ver%>"/>
<script type="text/javascript" src="/resolve/js/common-classes.js?_v=<%=ver%>"></script>

<!-- difflib -->
<script language="javascript" type="text/javascript" src="/resolve/js/diffview.js?_v=<%=ver%>"></script>
<script language="javascript" type="text/javascript" src="/resolve/js/difflib.js?_v=<%=ver%>"></script>


<!-- Ace Editor -->
<script type="text/javascript" src="/resolve/js/ace/ace-min.js?_v=<%=ver%>" charset="utf-8"></script>

<!-- Rangy -->
<script type="text/javascript" src="/resolve/js/rangy/rangy-core.js?_v=<%=ver%>"></script>
<!-- Glu Test Framework -->
<%
    if( test ){
%>
<script type="text/javascript" src="/resolve/js/glujs/glu-test.js?_v=<%=ver%>"></script>
<%
    }
%>

<!--HTML2Canvas-->
<script type="text/javascript" src="/resolve/js/saveSvgAsPng.js?_v=<%=ver%>"></script>

