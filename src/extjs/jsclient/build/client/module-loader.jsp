<%@page import="java.io.*" %> 
<%@page import="java.util.*" %> 
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Resolve Modules -->
<%
    String root = request.getSession().getServletContext().getRealPath("/");
    File directory = new File(root);
    File[] list = directory.listFiles();
    for( int i = 0; i < list.length; i++ ){
        if( list[i].isDirectory() ){
            File[] subList = list[i].listFiles();
            for( int j = 0; j < subList.length; j++ ){
                if( subList[j].getName().equals("common.jsp") ){
                    String path = "/" + subList[j].getPath().substring(root.length());
                    %>
                        <jsp:include page="<%=path%>" flush="true"/>
                    <%
                }
            }
        }
    }
    for( int i = 0; i < list.length; i++ ){
        if( list[i].isDirectory() ){
            File[] subList = list[i].listFiles();
            for( int j = 0; j < subList.length; j++ ){
                if( subList[j].getName().startsWith("loader") && subList[j].getName().endsWith(".jsp") ){
                    String path = "/" + subList[j].getPath().substring(root.length());
                    %>
                        <jsp:include page="<%=path%>" flush="true"/>
                    <%
                }
            }
        }
    }
%>