function hasPermission(roleConfig) {
	if (!roleConfig) roleConfig = []
	if (Ext.isString(roleConfig)) roleConfig = roleConfig.split(',')
	if (!Ext.isArray(roleConfig)) roleConfig = Ext.Array.from(roleConfig)

	var roles = roleConfig,
		hasRole = roles.length == 0 ? true : false,
		containsRole = false,
		roleSplit, user = clientVM.user;

	if (user.name == 'resolve.maint') return true

	if (user && user.roles) {
		if (Ext.Array.indexOf(user.roles, 'admin') > -1) return true //admin has access to everything

		Ext.Array.each(roles, function(role) {
			containsRole = false
			roleSplit = role.split('&')
			Ext.Array.each(roleSplit, function(roleSplitItem) {
				if (Ext.Array.indexOf(user.roles, Ext.String.trim(roleSplitItem)) > -1) {
					containsRole = true
					return false
				}
			})

			if (containsRole) {
				hasRole = true
				return false
			}
		}, this)

	}
	return hasRole
}

function isPublicUser() {
	return user && user.name == 'public'
}

//Adapter to check permissions of controls
glu.regAdapter('auth', {
	beforeCreate: function(config) {
		if (config.roles) {
			if (config.roleHide && !config.hidden && !hasPermission(config.roles)) config.hidden = true

			// if (config.roleDisplay && !this.hasPermission(config.roles)) config.xtype = 'displayfield'
		}
	}
})

//Register adapter as global plugin
glu.plugin('auth')