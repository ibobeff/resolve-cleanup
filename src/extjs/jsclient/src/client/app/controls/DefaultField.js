//Adapter to setup default field properties
glu.regAdapter('defaultfield', {
	beforeCreate: function(config) {
		Ext.applyIf(config, {
			msgTarget: 'side'
		})
	}
})

//Register adapter as global plugin
glu.plugin('defaultfield')