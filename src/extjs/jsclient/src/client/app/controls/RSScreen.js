Ext.define('RS.client.RSScreen', {
	extend: 'Ext.Panel',
	alias: 'widget.rsscreen',

	layout: 'fit',
	border: false,

	onRender: function() {
		this.callParent(arguments)
		var vm = glu.model(Ext.apply({
			mock: this.mock,
			mtype: this.modelName,
			clientVM: this.clientVM,
			screenVM: this.screenVM,
			registerClientHandlers: function() {
				if (this.activate) this.screenVM.on('activate', this.activate, this)
				if (this.deactivate) this.screenVM.on('deactivate', this.deactivate, this)
				if (this.checkNavigateAway) this.screenVM.on('checkNavigateAway', this.checkNavigateAway, this)
			}
		}, this.params)),
			view = glu.createViewmodelAndView(vm)
			vm.registerClientHandlers()
			this.add(view);
	}
})