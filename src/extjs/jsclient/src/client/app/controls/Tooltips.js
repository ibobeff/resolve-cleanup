//Adapter to setup default field properties
glu.regAdapter('tooltipfield', {
	afterCreate: function(control, vm) {
		if (control.name && vm && vm.localize) {
			var translation = vm.localize(control.name + '_tooltip')
			if (translation.indexOf('~~') == -1) {
				control.on('render', function() {
					Ext.create('Ext.tip.ToolTip', {
						target: control.getEl(),
						html: translation
					})
				})
			}
		}
	}
})

//Register adapter as global plugin
glu.plugin('tooltipfield')