glu.defView('RS.client.MenuSection', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	cls: 'rs-menu-section',
	bodyCls: 'rs-menu-section-body',
	sectionColor: '@{color}',
	sectionTextColor: '@{textColor}',
	defaultHeight: 170,
	defaultInnerHeight: 157,
	height: 170,
	width: 280,
	minSectionWidth: 280,
	displayItemsHeight: 0,
	items: '@{displayItems}',
	ready: false,
	listeners: {
		add: function(panel) {
			panel.removeCls('rs-menu-section-hover')
			panel.removeBodyCls('rs-menu-section-body-hover')
			if (panel.getEl())
				Ext.each(panel.getEl().query('.rs-menu-section-header-hover'), function(item) {
					Ext.fly(item).removeCls('rs-menu-section-header-hover');
				})
			this.showArrow()
		},
		remove: function() {
			this.showArrow()
		},
		render: function(panel) {
			if (this.sectionColor) this.setBodyStyle('background-color', '#' + this.sectionColor)

			//Calculate appropriate width for box based on the widest child
			// var divs = Ext.fly(this.getEl().query('.x-box-target')[0]).query('.x-panel'),
			// 	width = this.minSectionWidth;
			// Ext.each(divs, function(div) {
			// 	width = Math.max(width, Ext.fly(div).getWidth() + Ext.fly(div).child('div').child('span').child('div').child('span').getPadding('lr') + Ext.fly(div).getPadding('lr'))
			// })
			// this.setWidth(width)

			//determine whether to show the arrow on render
			this.showArrow()

			panel.getEl().on('mouseover', function() {
				this.addCls('rs-menu-section-hover')
				this.addBodyCls('rs-menu-section-body-hover')

				var arrows = this.getEl().query('.rs-menu-section-arrow')
				Ext.each(arrows, function(arrow) {
					Ext.fly(arrow).addCls('rs-menu-section-arrow-hover')
				})

				var headers = this.getEl().query('.rs-menu-section-header')
				Ext.each(headers, function(header) {
					Ext.fly(header).addCls('rs-menu-section-header-hover')
				})

				var groups = this.getEl().query('.rs-menu-section-group')
				Ext.each(groups, function(group) {
					Ext.fly(group).addCls('rs-menu-section-group-hover')
				})

				var texts = this.getEl().query('.rs-menu-section-text')
				Ext.each(texts, function(text) {
					Ext.fly(text).addCls('rs-menu-section-text-hover')
				})

				var bullets = this.getEl().query('.rs-menu-section-bullet')
				Ext.each(bullets, function(bullet) {
					Ext.fly(bullet).addCls('rs-menu-section-bullet-hover')
				})

				var height = this.displayItemsHeight;
				if (!height) {
					var divs = Ext.fly(this.getEl().query('.x-box-target')[0]).query('.x-panel');
					Ext.each(divs, function(div) {
						height += Ext.fly(div).getHeight()
					})
					height += divs.length * 2
					this.displayItemsHeight = height;
				}
				this.defaultInnerHeight = Math.min(this.getEl().child('div').child('div').getHeight(), this.defaultInnerHeight)
				this.getEl().child('div').child('div').setHeight(Math.max(this.displayItemsHeight, this.defaultInnerHeight))
				this.getEl().child('div').setHeight(Math.max(this.displayItemsHeight, this.defaultHeight))
			}, panel)
			panel.getEl().on('mouseout', function() {
				this.getEl().child('div').child('div').setHeight(this.defaultInnerHeight)
				this.getEl().child('div').setHeight(this.defaultHeight)
				this.removeCls('rs-menu-section-hover')
				this.removeBodyCls('rs-menu-section-body-hover')

				var arrows = this.getEl().query('.rs-menu-section-arrow')
				Ext.each(arrows, function(arrow) {
					Ext.fly(arrow).removeCls('rs-menu-section-arrow-hover')
				})

				var headers = this.getEl().query('.rs-menu-section-header')
				Ext.each(headers, function(header) {
					Ext.fly(header).removeCls('rs-menu-section-header-hover')
				})

				var groups = this.getEl().query('.rs-menu-section-group')
				Ext.each(groups, function(group) {
					Ext.fly(group).removeCls('rs-menu-section-group-hover')
				})

				var texts = this.getEl().query('.rs-menu-section-text')
				Ext.each(texts, function(text) {
					Ext.fly(text).removeCls('rs-menu-section-text-hover')
				})

				var bullets = this.getEl().query('.rs-menu-section-bullet')
				Ext.each(bullets, function(bullet) {
					Ext.fly(bullet).removeCls('rs-menu-section-bullet-hover')
				})

				if (this.sectionColor) this.setBodyStyle('background-color', '#' + this.sectionColor)
				else this.setBodyStyle('background-color', 'transparent')
			}, panel)

			this.ready = true
			Ext.defer(function() {
				this.showArrow()
			}, 1, this)
		},
		added: function() {
			this.showArrow()
		},
		removed: function() {
			this.showArrow()
		}
	},
	showArrow: function() {
		if (this.ready) {
			if (this.items.length > 7) {
				if (!this.showingArrow) {
					var els = this.getEl().query('.x-box-inner'),
						config = {
							tag: 'div',
							cls: 'rs-menu-section-arrow'
						}
					if (this.sectionTextColor) config.style = 'border-top-color:' + '#' + this.sectionTextColor
					if (els.length > 0) Ext.fly(els[0]).insertFirst(config)
					this.showingArrow = true
				}
			} else {
				var els = this.getEl();
				if (els) {
					var arrows = els.query('.rs-menu-section-arrow')
					Ext.each(arrows, function(arrow) {
						Ext.fly(arrow).remove()
					})
				}
				this.showingArrow = false
			}
		}
	},
	hexToRGBA: function(c, alpha) {
		var color = c.charAt(0) == '#' ? c.substring(1, 7) : c,
			r = parseInt(color.substring(0, 2), 16),
			g = parseInt(color.substring(2, 4), 16),
			b = parseInt(color.substring(4, 6), 16);

		return Ext.String.format('rgba({0},{1},{2},{3})', r, g, b, alpha || 1)
	}
})

glu.defView('RS.client.MenuSectionItem', {
	bodyStyle: 'background-color: transparent;',
	style: '@{displayStyle}',
	textChange: '@{displayText}',
	setTextChange: function() {
		if (this.setupClick) Ext.defer(function() {
			this.setupOnClick()
		}, 1, this)
	},
	html: '@{displayText}',
	setupClick: '@{setupClick}',
	setupOnClick: function() {
		var texts = this.body.el.query('span[class*=rs-menu-section-text]');
		Ext.Array.forEach(texts, function(text) {
			Ext.fly(text).on('click', function() {
				this.fireEvent('menuClick', this)
			}, this)
		}, this)
	},
	listeners: {
		menuclick: '@{menuItemClicked}',
		render: function(pnl) {
			if (pnl.setupClick)
				pnl.setupOnClick()
		}
	}
})

glu.defView('RS.client.MenuSectionPadding', {
	height: 155,
	width: 280
})