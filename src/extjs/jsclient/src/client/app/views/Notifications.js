glu.defView('RS.client.Notifications', {
	asWindow: {
		target: '@{target}',
		width: 350,
		height: 155,
		baseCls: Ext.isIE8m ? 'x-window' : 'x-panel',
		ui: Ext.isIE8m ? 'default' : 'sysinfo-dialog',
		title: '~~notifications~~',
		header: false,
		draggable: false,
		listeners: {
			render: function(panel) {
				if (panel.target) {
					Ext.defer(function() {
						panel.alignTo(panel.target, 'tr-br')
					}, 1)
				}
			}
		}
	},

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	autoScroll: true,
	items: '@{notifications}',
	buttonAlign: 'left',
	buttons: ['dismissAll', 'close']
});

glu.defView('RS.client.Notification', {
	title: '@{title}',
	ui: 'sysinfo-dialog',
	bodyPadding: '10px',
	html: '@{messageText}',
	gotoText: '@{gotoText}',
	dismissText: '@{dismissText}',
	tools: [{
		type: 'left',
		itemId: 'right',
		handler: function(tool) {
			this.up('panel').fireEvent('goTo', this)
		}
	}, {
		type: 'close',
		itemId: 'close',
		handler: function(tool) {
			this.up('panel').fireEvent('dismiss', this)
		}
	}],
	listeners: {
		render: function(panel) {
			//Setup tooltips for buttons
			panel.down('#right').on('render', function() {
				Ext.create('Ext.tip.ToolTip', {
					target: panel.down('#right').getEl(),
					html: panel.gotoText
				})
			})

			panel.down('#close').on('render', function() {
				Ext.create('Ext.tip.ToolTip', {
					target: panel.down('#close').getEl(),
					html: panel.dismissText
				})
			})
		},
		goTo: '@{goTo}',
		dismiss: '@{dismiss}'
	}
})