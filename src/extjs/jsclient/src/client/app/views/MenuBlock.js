glu.defView('RS.client.MenuBlock', {
	xtype: 'panel',
	hidden: '@{hidden}',
	cls: 'rs-client-menublock',
	width: 300,
	margin: '0 20 20 20',
	dock: 'top',
	dockedItems: [{
		xtype: 'toolbar',
		layout: 'hbox',
		items: [{
			xtype: 'button',
			//width: 35,
			cls: 'rs-client-menuglyph',
			glyph: '@{glyph}',
			scale: 'small',
			style: {
				color: 'black'
			},
			iconCls: 'rs-client-iconmedium'
		}, {
			xtype: 'label',
			text: '@{text}',
			style: {
				color: '#66cc33'
			},
			cls: 'rs-client-menublocktitle'
		}]
	}],
	bodyStyle: {
		paddingLeft: '32px',
	},
	items: '@{menuLinks}'
});

glu.defView('RS.client.MenuGroup', {
	xtype: 'component',
	cls: 'rs-client-menugroup',
	html: '@{text}'
});

glu.defView('RS.client.MenuLink', {
	xtype: 'container',
	items: [{
		xtype: 'component',
		cls: 'rs-client-menulink',
		html: '@{text}',
		listeners: {
			afterrender: function(link) {
				link.getEl().on('click', this._vm.handleClick.bind(this._vm))
			}
		}
	}]
});