glu.defView('RS.client.Menu', {
	hidden : '@{hidden}',
	overflowX: true,
	height: 'inherit',
	dockedItems: [{
		xtype: 'toolbar',
		layout: {
			pack: 'center'
		},
		items: [/*{
			xtype: 'tbtext',
			cls: 'rs-client-displayname',
			text: 'Main Menu'
		},*/ {
			xtype: 'combobox',
			editable: false,
			fieldLabel: '~~menuSet~~',
			labelStyle: 'text-align:right',
			store: '@{menuSetsStore}',
			queryMode: 'local',
			displayField: 'name',
			valueField: 'menuSetLink',
			value: '@{selectedMenuSet}',
			listeners: {
				change: '@{changeMenuSet}'
			}
		}, {
			xtype: 'button',
			cls: 'rs-client-historybutton',
			glyph: 0xf017,
			scale: 'medium',
			arrowAlign: 'right',
			menu: {
				xtype: 'menu',
				items: '@{historyState}'
			}
		}]
	}],
	items: '@{menuItems}',
	bodyCls: 'rs-client-menucontainer',
	listeners: {
		afterrender: function(panel) {
			panel.body.on('mousewheel', function(e){
				e.preventDefault();
				movement = e.getWheelDeltas();

				if (movement.y < 0) {
					this.scrollTo('left', (this.getScroll().left + 150));
				} else {
					this.scrollTo('left', (this.getScroll().left - 150));
				}
			});
		}
	}
});

glu.defView('RS.client.HistoryLink', {
	xtype: 'menuitem',
	cls: 'rs-client-historylink',
	text: '@{text}',
	listeners: {
		click: '@{handleClick}'
	}
});