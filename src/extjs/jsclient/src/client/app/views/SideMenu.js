glu.defView('RS.client.SideMenu', {
	layout: 'fit', 
	style : 'border-right:1px solid #cccccc',
	xtype: 'treepanel',
	hideHeaders: true,
	animCollapse : false,
	header: false,
	lines: false,
	cls: 'side-menu',
	title: '~~sideMenu~~',
	store: '@{menuSections}',
	rootVisible: false,
	viewConfig: {
		toggleOnDblClick: false
	},
	dockedItems: [{
		cls: 'side-menu-tb',
		xtype: 'toolbar',
		padding : 5,
		items: [{
			xtype: 'combo',
			minWidth : 180,
			flex: 1,
			name: 'currentMenuSetLink',
			hideLabel: true,
			store: '@{menuSets}',
			queryMode: 'local',
			editable: false,
			displayField: 'name',
			valueField: 'menuSetLink',
			listeners: {
				change: function() {
					delete this.up().up().currentExpanded;
				}
			}
		},'->',{
			xtype: 'tool',
			type: 'left',
			handler: function() {
				this.up().up().collapse()
			}
		}]
	}],
	columns: [{
		flex: 1,
		xtype: 'treecolumn',
		dataIndex: 'text',
		text: 'Menu',
		renderer: function(value, meta, record, rowIdx) {
			if (!record.isLeaf()) {
				if (record.getPath() == '/root/') {
					if (rowIdx == 0)
						meta.tdCls = 'side-menu-header-first';
					else
						meta.tdCls = 'side-menu-header';
					if (record.isExpanded())
						meta.style = 'border-bottom:1px solid silver';
					var cls = record.isExpanded() ? 'icon-caret-down' : 'icon-caret-right';
					return Ext.String.format('<div style="width:15px;padding-top:5px;padding-bottom:5px;display:inline-block"><span class="{1}"></span></div>{0}', value, cls);
				}
				meta.tdCls = 'side-menu-group';
				return value;
			}
			meta.tdCls = 'side-menu-item';
			return value;
		}
	}],
	listeners: {
		afteritemexpand: function(node, index, nodeDom) {
			this.clientSideMenuItemExpanding = false;
		},
		beforeitemexpand: function(node) {
			if (this.clientSideMenuItemExpanding) {
				event.stopPropagation();
				return
			}
			this.clientSideMenuItemExpanding = true;
		},
		beforeitemmouseenter: function(view, node) {
			if (!node.isLeaf() && node.getPath() != '/root/')
				return false;
		},
		itemclick: function(view, node, nodeDom) {
			if (this.clientSideMenuItemExpanding || (!node.isLeaf() && node.getPath() != '/root/')) {
				return false;
			}
			if (node.isLeaf());
			// some functionality to open the leaf(document) in a tabpanel
			else if (node.isExpanded())
				node.collapse();
			else
				node.expand();
			this.fireEvent('menuItemClick', node, node)
		},
		menuItemClick: '@{menuItemClick}'
	}
})