glu.defView('RS.client.UserInfo', {
	target: '@{target}',
	width: 350,
	height: 155,		
	cls : 'rs-modal-popup',
	componentCls : 'user-info',
	header: false,
	title: '@{userTitle}',
	draggable: false,
	resizable: false,
	listeners: {
		render: function(panel) {
			if (panel.target) {
				Ext.defer(function() {
					panel.alignTo(panel.target, 'tr-br')
				}, 1)
			}
		},
		beforedestroy: '@{beforeDestroyComponent}'
	},
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	padding : 5,
	defaults: {
		labelWidth: 120,
		margin : '0 0 5 0'
	},
	items: [{
		xtype: 'image',
		src: '@{profilePicUrl}',
		maxWidth: 100,
		maxHeight: 100,
		width: 100
	}, {
		flex: 1,
		padding: '0px 0px 0px 20px',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaultType: 'displayfield',
		defaults: {
			hideLabel: true
		},
		items: [{
			name: 'fullname',
			fieldCls: 'userInfo-title'
		}, {
			name: 'name',
			fieldCls: 'userInfo-name'
		}]
	}],
	// html: '@{html}',
	buttons: ['->', {
		name: 'settings',	
		iconCls: 'icon-cog icon-large rs-client-button',
		cls : 'rs-small-btn rs-btn-dark'
	}, {
		name: 'logout',		
		iconCls: 'icon-signout icon-large rs-client-button',
		cls : 'rs-small-btn rs-btn-dark'
	}]
});