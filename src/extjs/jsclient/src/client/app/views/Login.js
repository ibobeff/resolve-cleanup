glu.defView('RS.client.Login', {
	width: 480,	
	height : 200,
	modal: true,
	cls : 'rs-modal-popup',
	title: '~~authenticateUser~~',	
	padding : 15,
	closable: false,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		margin : '4 0',
		labelWidth : 120
	},
	items: [{
		xtype: 'textfield',
		itemId: 'username',
		id: 'resolveSessionTimeoutUsername',
		name: 'username',
		fieldLabel: '~~u_sername~~',
		keyDelay: 1,
		listeners: {
			specialkey: '@{specialKey}',
			loginwindowshow: '@{loginWindowShow}'
		}
	}, {
		xtype: 'textfield',
		itemId: 'password',
		id: 'resolveSessionTimeoutPassword',
		inputType: 'password',
		name: 'password',
		fieldLabel: '~~p_assword~~',
		keyDelay: 1,
		listeners: {
			specialkey: '@{specialKey}'
		}
	},{
		xtype: 'tbtext',
		margin : '0 0 0 125',
		text: '@{badLoginHtml}',
		htmlEncode : false,
		hidden: '@{!badLoginIsVisible}'
	}],		
	buttons: [{
		name : 'authenticateUser',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}],
	listeners: {
		show: function(w) {
			//Focus the username field when the dialog is displayed so the user can just come back and start typing
			Ext.defer(function() {
				var username = w.down('#username'),
					password = w.down('#password');
				if (username && password) {
					username.focus();
					username.fireEvent('loginwindowshow', this, username, password);
				}
			}, 100)
		}
	}
})