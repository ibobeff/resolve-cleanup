glu.defView('RS.client.ContactUs', {
	asWindow: {
		title: '~~contactUs~~',
		height: 330,
		width: 600,
		cls : 'rs-modal-popup contact-us',
		padding : 15,
		modal: true,
		draggable: false,
		resizable: false,
	},
	layout: {
		type: 'hbox',
		pack: 'center',
	},
	margin : '30 0 0 0',
	items: [{
		layout: {
			type: 'vbox',
			align: 'center'
		},
		defaults : {
			margin : '5 0'
		},
		items: [{
			html : '~~getInTouchWithResolve~~',
		}, {
			html : '~~supportPortal~~',
		}, {
			html : '~~usPhone~~',
		}, {
			html : '~~ukPhone~~',
		}]
	}],
	buttons: [{
		name :'close',			
		cls : 'rs-med-btn rs-btn-dark'
	}]
});
