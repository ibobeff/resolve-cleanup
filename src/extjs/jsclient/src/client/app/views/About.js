glu.defView('RS.client.About', {
	cls : 'rs-modal-popup',
	resizable: false,
	draggable: false,
	modal: true,
	title: '~~aboutTitle~~',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		layout: {
			type: 'hbox',
			align: 'middle'
		},
		items: [/*{
			flex: 1,
			html: '@{versionInfo}'
		}, */{
			xtype: 'image',
			width: 615,
			height: 176,
			src: '/resolve/images/logo-about.png'
		}]
	}, {
		flex: 1,
		style: 'border-top: 1px solid #bfbfbf !important',
		bodyStyle: 'background-color: #f0f0f0 !important;',
		bodyPadding: 10,
		html: '@{versionInfo}',
		
	}],
	buttons: {
		xtype: 'toolbar',
		style: 'background-color: #f0f0f0 !important;border-top:0px !important',
		padding : 10,
		items: [{
			name :'ok',			
			cls : 'rs-med-btn rs-btn-dark'
		}]
	}
});
