glu.defView('RS.client.Screen', {
	border: false,
	layout: 'fit',
	items: [{
		xtype: 'rsscreen',
		mock: '@{mock}',
		modelName: '@{modelName}',
		params: '@{params}',
		clientVM: '@{clientVM}',
		screenVM: '@{screenVM}'
	}]
})