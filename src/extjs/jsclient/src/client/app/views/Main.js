glu.defView('RS.client.Main', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	style : 'visibility : @{visibility}',
	items: [{
		cls: 'rs-header-top',
		border: false,
		bodyBorder: false,
		height: '@{bannerHeight}',
		hidden: '@{!bannerIsVisible}',
		layout: {
			type: 'hbox',
			align: 'middle'
		},
		items: [{
			cls: 'rs-logo',
			xtype: 'image',
			src: '@{logo}',
			height: '@{logoHeight}',
			width: '@{logoWidth}',
			hidden: '@{!logoIsVisible}',
			listeners: {
				render: function(img) {
					img.getEl().on('load', function() {
						img.up('panel').doLayout()
						img.up('panel').doComponentLayout()
					})
				}
			}
		}, {
			xtype: 'panel',
			flex: 1,
			padding: '0 0 0 20',
			hidden: '@{!bannerTextIsVisible}',
			html: '@{bannerDisplayText}'
		}, {
			xtype: 'panel',
			flex: 1,
			hidden: '@{!bannerURLIsVisible}',
			html: '@{bannerURL}'
		}, {
			cls: 'rs-logo',
			xtype: 'image',
			src: '@{rightLogo}',
			height: '@{rightLogoHeight}',
			width: '@{rightLogoWidth}',
			hidden: '@{!rightLogoIsVisible}',
			listeners: {
				render: function(img) {
					img.getEl().on('load', function() {
						img.up('panel').doLayout()
						img.up('panel').doComponentLayout()
					})
				}
			}
		}]
	}, {
		border: false,
		hidden: '@{!toolbarIsVisible}',
		xtype: 'toolbar',
		overflowX: 'auto',
		enableOverflow: false,
		ui: 'client-toolbar',
		height: 60,
		defaults: {
			defaultButtonUI: 'client-toolbar-button'
		},
		items: [{
			xtype: 'toolbar',
			itemId: 'leftToolbar',
			cls: 'rs-header-toolbar leftToolbar',
			items: '@{lefttoolbarItems}',
			enableOverflow: false,
			itemTemplate: function(config) {
				return config
			}
		}, {
			xtype: 'toolbar',
			itemId: 'middleToolbar',
			cls: 'rs-header-toolbar leftToolbar',
			items: '@{middletoolbarItems}',
			listeners: {
				afterrender: function ( toolbar ) {

					var getMyWidth = function() {
						var leftToolbarMinWidth = this.up().down('#leftToolbar').getWidth(),
							rightTollbarMinWidth = this.up().down('#rightToolbar').getWidth(),
							padding = 40;
						return (Ext.getBody().getViewSize().width - leftToolbarMinWidth - rightTollbarMinWidth - padding);
					};

					toolbar.afterLayoutHander = function() {
						this.un('afterlayout', this.afterLayoutHander);
						this.setWidth(getMyWidth.call(this));
						this.on('afterlayout', this.afterLayoutHander);
					}

					Ext.EventManager.onWindowResize(function(width) {
						this.un('afterlayout', this.afterLayoutHander);
						this.setWidth(getMyWidth.call(this));
					}, toolbar);

					toolbar.on('afterlayout', toolbar.afterLayoutHander, toolbar);

				}
			},
			enableOverflow: true,
			minWidth: 45,
			itemTemplate: function(config) {
				return config
			}
		}, '->', {
			xtype: 'toolbar',
			itemId: 'rightToolbar',
			cls: 'rs-header-toolbar',
			enableOverflow: false,
			items: '@{righttoolbarItems}',
			minWidth: 444,
			itemTemplate: function(config) {
				return config
			}
		}]
	}, {
		hidden: '@{licenseMsgHidden}',
		xtype: 'toolbar',
		layout: {
			type: 'hbox',
			align: 'middle',
			pack: 'center',
		},
		cls: '@{licenseMsgCls}',
		height: 60,
		items: [{
			xtype: 'label',
			text: '@{licenseMsg}',
			cls: 'licenseMsgText',
		}, {
			name: 'dismissLicenseMsg',
			hidden: '@{dismissLicenseMsgHidden}',
			cls: 'rs-small-btn rs-btn-light rs-btn-license',
			text: '~~dismiss~~',
		}, {
			name: 'contactUs',
			cls: 'rs-small-btn rs-btn-light rs-btn-license',
			text: '~~contactUs~~',
		}]
	}, {
		flex: 1,
		layout: 'border',
		items: [{
			hidden: '@{!sideMenuIsVisible}',
			region: 'west',
			width: 290,
			split: true,
			collapsible: true,
			xtype: '@{sideMenu}'
		}, {
			region: 'center',
			flex: 1,
			layout: 'card',
			activeItem: '@{activeScreen}',
			items: '@{screens}'
		}]
	}],
	listeners: {
		render: function() {
			this.getEl().on('click', function() {
				this.fireEvent('checkauthentication', this);
			}, this);

			//
			// We hijack Ctrl-S/Ctrl-R hotkey globally ans use it to save the pages that have the 'save/reload' button.
			//
			Ext.create('Ext.util.KeyMap', Ext.getBody(), {
				scope: this,
				key: Ext.EventObject.S, // Ctrl-S for save
				ctrl: true,
				fn: function(keyCode, e) {
					e.preventDefault();
					clientVM.clickActiveScreenSaveButton();
				}
			});
			Ext.create('Ext.util.KeyMap', Ext.getBody(), {
				scope: this,
				key: Ext.EventObject.R, // Ctrl-R for reload
				ctrl: true,
				fn: function(keyCode, e) {
					e.preventDefault();
					clientVM.clickActiveScreenRefreshButton();
				}
			});
		},
		checkauthentication: '@{checkAuthentication}',
		beforedestroy: '@{beforeDestroyComponent}'
	}
});
