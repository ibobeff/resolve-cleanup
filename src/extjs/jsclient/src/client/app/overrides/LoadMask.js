Ext.override(Ext.LoadMask, {
	setZIndex: function(index) {
		var me = this,
			owner = me.activeOwner;

		if (owner) {
			// it seems silly to add 1 to have it subtracted in the call below,
			// but this allows the x-mask el to have the correct z-index (same as the component)
			// so instead of directly changing the zIndexStack just get the z-index of the owner comp
			var style = owner.el.getStyle('zIndex');
			index = parseInt(Ext.isNumber(style) ? style : 0, 10) + 1;
		}

		me.getMaskEl().setStyle('zIndex', index - 1);
		return me.mixins.floating.setZIndex.apply(me, arguments);
	}
})