Ext.override('Ext.dom.Layer', {
	setZIndex: function(zindex) {
		var me = this;
		zindex = Ext.isNumber(zindex) ? zindex : 0;
		me.zindex = zindex;
		if (me.getShim()) {
			me.shim.setStyle('z-index', zindex++);
		}
		if (me.shadow) {
			me.shadow.setZIndex(zindex++);
		}
		return me.setStyle('z-index', zindex);
	}
})