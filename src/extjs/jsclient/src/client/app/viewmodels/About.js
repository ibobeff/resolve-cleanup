glu.defModel('RS.client.About', {
	version: '',
	build: '',
	year: '',
	fields: ['version', 'build', 'year'],

	versionInfo$: function() {
		return ('Build Version: ' + this.version + this.getBuildVer() + 
			'<br/>Copyright &copy; 2006-' + this.year + ' Resolve Systems, LLC.  All Rights Reserved.<br/>' + 
			'U.S. Patent No. 8,533,608'
		)
	},

	getBuildVer: function() {
		if (this.build) {
			return ' (' + this.build + ')';
		} else {
			return '';
		}
	},

	init: function() {
		this.ajax({
			url: '/resolve/service/client/about',
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success)
					this.loadData(response.data)
			},
			failure: function(resp) {
				this.displayFailure(resp);
			}
		})
	},

	ok: function() {
		this.doClose()
	}
})