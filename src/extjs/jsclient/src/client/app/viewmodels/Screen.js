glu.defModel('RS.client.Screen', {
	modelName: '',
	params: {},

	lastActivated: new Date(),

	mock: false,

	init: function() {
		this.clientVM = this.parentVM
	},
	screenVM$: function() {
		return this
	},
	activate: function(params, asNew) {
		params = this.encodeWindowParam(params);
		this.set('lastActivated', new Date());
		this.set('params', params);
		//New Screen is not cached, delay before activate to make sure it rendered properly.
		if(asNew){
			setTimeout(function(){
				this.fireEvent('activate', this, params);
			}.bind(this), 500);	
		}
		else {
			this.fireEvent('activate', this, params);
		}
	},
	deactivate: function() {
		this.fireEvent('deactivate', this)
	},
	checkNavigateAway: function() {
		return this.fireEvent('checkNavigateAway', this)
	},
	encodeWindowParam : function(params){
		for(var p in params){
			var param = params[p];
			if(Array.isArray(param)){
				param.forEach(function(param){
					param = RS.common.encodeForHTML(param);
				})
			}
			else
				param = RS.common.encodeForHTML(param);
		}
		return params;
	}
})