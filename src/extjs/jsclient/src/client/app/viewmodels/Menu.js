glu.defModel('RS.client.Menu', {
	xtype: 'panel',
	hidden: false,
	menuItems: {
		mtype: 'list',
		autoParent: true
	},

	selectedMenuSet: '',

	changeMenuSet: function(newSet) {
		this.set('selectedMenuSet', newSet);
		Ext.state.Manager.set('userMenuSet', newSet);
		this.filterMenuItems();
	},

	menuSetsStore: {
		mtype: 'store',
		fields: [
			'name',
			'applications',
			{
				name: 'menuSetLink',
				convert: function(val) {
					return (val === '') ? '-1' : val
				}
			},
			'roles',
			'sequence'
		],
		proxy: {
			type: 'memory'
		}
	},

	historyState: {
		mtype: 'list',
		autoParent: true
	},

	retrieveHistory: function() {
		this.historyState.removeAll();
		//Get last 10 links
		var recent = JSON.parse(Ext.state.Manager.get(clientVM.username + 'UserMenuRecentlyUsed', '[]') || '[]').slice(0, 10);
		recent.forEach(function(item) {
			this.historyState.add({
				mtype: 'RS.client.HistoryLink',
				text: item.text,
				menuId: item.menuId,
				modelName: item.modelName,
				params: item.params

			})
		}, this);
	},

	handleNavigation: function(vm, screenConfig) {
		var recent = JSON.parse(Ext.state.Manager.get(clientVM.username + 'UserMenuRecentlyUsed', '[]') || '[]')
		recent = recent.filter(function(item) {
			return screenConfig.menuId !== item.menuId
		});
		if (screenConfig.menuId) {
			recent.unshift({
				menuId: screenConfig.menuId,
				text: screenConfig.text,
				modelName: screenConfig.modelName,
				params: screenConfig.params
			});
			Ext.state.Manager.set(clientVM.username + 'UserMenuRecentlyUsed', JSON.stringify(recent));
		}
	},	

	filterMenuItems: function() {
		this.menuItems.each(function(item) {
			item.toggleVisibility(this.selectedMenuSet, this.selectedMenuSet === '-1');
		}, this)
	},

	processMenuItem: function(menuItem) {
		if (!menuItem.modelName) {
			var location = menuItem.location;
			if (location && location.indexOf('/resolve/jsp/rswiki.jsp?wiki=RS.') === 0) {
				//Wiki RS type
				var splitLocation = location.split('wiki=')[1].split('/');
				menuItem.modelName = splitLocation[0];
				menuItem.params = (splitLocation[1]) ? Ext.Object.fromQueryString(splitLocation[1]) : {};

			} else if (location && location.indexOf('/resolve/jsp/rswiki.jsp?wiki=') === 0) {
				//Wiki non-RS type
				menuItem.modelName = 'RS.wiki.Main';
				menuItem.params = {
					name: location.split('wiki=')[1]
				}
			} else if (location && location.indexOf('RS.') === 0) {
				//Plain namespace in location
				var splitLocation = location.split('/');
				menuItem.modelName = splitLocation[0];
				menuItem.params = (splitLocation[1]) ? Ext.Object.fromQueryString(splitLocation[1]) : {};
			} else if (location && location.indexOf('/resolve/') === 0) {
				if (location.indexOf('/resolve/sir/index.html') === 0 && !clientVM.displayClient) {
					// React UI specific locations
					location = '/resolve/sir/index.html?displayClient=false' + location.split('/resolve/sir/index.html')[1];
				}
				const safeLocation = RS.common.encodeForURL(location);
				menuItem.modelName = 'RS.client.URLScreen';
				menuItem.params = {
					location: RS.common.decodeFromURL(safeLocation)
				}
			} else {
				//Etc.
				var safeLocation = RS.common.encodeForURL(location);
				menuItem.modelName = 'RS.wiki.Main';
				menuItem.params = {
					location: RS.common.decodeFromURL(safeLocation)
				}
			}
			delete menuItem.location
		}
		else if (menuItem.modelName == 'RS.wiki.Main' && menuItem.params && menuItem.params.location) {
			menuItem.modelName = 'RS.client.URLScreen';
		}
	},

	generateSections: function(menuSections) {
		Ext.Array.forEach(menuSections || [], function(item) {
			if (item.items) {
				//Process legacy URL scheme
				item.items.forEach(function(menuItem) {
					this.processMenuItem(menuItem)
				}, this);
	
				this.menuItems.add({
					mtype: 'RS.client.MenuBlock',
					glyph:  this.menuGlyphs[item.menuId] || 0xf105,//hardcode menuglpyhs
					color: item.color,
					group: item.group,
					items: item.items,
					location: item.location,
					menuId: item.menuId,
					modelName: item.modelName,
					order: item.order,
					sets: item.sets || [],
					text: item.text,
					textColor: item.textColor,
				});
			}

		}, this)
	},

	createMenu: function() {
		this.menuItems.removeAll();
		this.generateSections(clientVM.menuSections);
		this.filterMenuItems();
		this.retrieveHistory();
	},

	activate: function(screens, params) {
		this.menuSetsStore.loadData(clientVM.menuSets);
		if (params && params.menuSet) {
			var menuSet = clientVM.menuSets.filter(function(menuSet) {return menuSet.name === params.menuSet})[0] || {};
			this.set('selectedMenuSet', (menuSet.name && (menuSet.name !== 'All')) ? menuSet.menuSetLink : '-1'); //set 'all' if param is invalid
		} else {
			this.set('selectedMenuSet', (Ext.state.Manager.get('userMenuSet') === -1 || Ext.state.Manager.get('userMenuSet') === undefined) ? '-1' : Ext.state.Manager.get('userMenuSet'));
		}

		this.createMenu();
	},

	init: function() {
		clientVM.on('menuSetsChanged', this.createMenu, this)
		clientVM.on('menuSectionsChanged', this.createMenu, this)
		clientVM.on('beforeScreenChanged', this.handleNavigation, this)
		//clientVM.on('userSettingsChanged', this.createMenu, this)
	},

	menuGlyphs: {
		resolve_runbook: 0xf02d,
		'8a9494a959f1499f0159f1f76adc001a': 0xf132,
		'Content Request': 0xf0f6,
		'Content Management': 0xf0c5,
		resolve_problem: 0xf03a,
		resolve_request_history: 0xf017,
		system_user_interface: 0xf0e8,
		resolve_admin_action: 0xf144,
		'Social Administration': 0xf086,
		resolve_admin_wiki: 0xf15c,
		'8a9482e549ca1db40149ca33cc7e013a': 0xf080,
		'Reports Administration': 0xf080,
		'user admin': 0xf0c0,
		'Table Administration': 0xf0ce,
		'8a9482e64c5cb9cb014c5cc37e1b0066': 0xf126,
		'8a9482e641dbe8bc0141dd0f40560006': 0xf00a,
		resolve_admin_status: 0xf085,
		resolve_admin_impex: 0xf0ec,
		'System Logs': 0xf044,
		resolve_help: 0xf059,
		'8a9482ba661e46810166219065830270': 0xf0ad,	// Development Tools
	}
});

glu.defModel('RS.client.HistoryLink', {
	menuId: '',
	text: '',
	modelName: '',
	params: {},

	handleClick: function() {
		clientVM.handleNavigation({
			menuId: this.menuId,
			text: this.text,
			modelName: this.modelName,
			params: this.params
		})
	},

	init: function() {
	}
});

