glu.defModel('RS.client.Login', {
	username: '',
	usernameIsValid: true,
	password: '',
	passwordIsValid: true,
	badLoginIsVisible: false,

	badLoginText: '',
	badLoginHtml$: function() {
		return Ext.String.format('<font style="color:red">{0}</font>', this.badLoginText)
	},

	submittedUsername: '',

	pollIntervalId: null,

	refreshAuthentication: function() {
		clearInterval(this.pollIntervalId);

		var token_pair = clientVM.getCSRFToken();
		token_pair = token_pair.split(":");
		clientVM.updateCSRFToken(token_pair);

		this.parentVM.set('loginOpen', false)
		this.parentVM.set('authenticated', true);
		this.doClose()
	},

	init: function() {
		this.parentVM.set('pollEnabled', false);
		this.set('badLoginText', this.localize('badLoginText'))

		clearInterval(this.pollIntervalId);
		this.pollIntervalId = setInterval(function(){
			if (clientVM.getCSRFTokenFlag()) {
				this.refreshAuthentication();
			}
		}.bind(this), 1000);
	},

	authenticateUser: function() {
		this.set('username', document.getElementById('resolveSessionTimeoutUsername').getElementsByTagName('input')[0].value);
		this.set('password', document.getElementById('resolveSessionTimeoutPassword').getElementsByTagName('input')[0].value);

		if (this.username.length == 0 || this.password.length == 0) {
			this.set('badLoginIsVisible', true)
			this.set('badLoginText', this.localize('emptyUserNameOrPassword'));
		}
		else {
			this.set('submittedUsername', this.username)
			this.ajax({
				url: '/resolve/service/client/login',
				params: {
					username: this.username,
					password: this.password
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) {
						clearInterval(this.pollIntervalId);
						clientVM.updateCSRFToken(response.data);
						clientVM.setCSRFTokenFlag();
						clientVM.removeLogoutFlag();
						clientVM.refreshPageTokens();
						this.parentVM.checkUser(this.submittedUsername)
						this.parentVM.set('loginOpen', false)
						this.parentVM.set('authenticated', true);
						this.doClose()
					} else {
						this.set('badLoginIsVisible', true)
						this.parentVM.set('authenticated', false);
						this.set('password', '')
						if (response.message) {
							this.set('badLoginText', response.message);
						} else {
							this.set('badLoginText', this.localize('badLoginText'));
						}
					}
				},
				failure: function() {
					this.set('badLoginText', this.localize('unableToConnect'))
				}
			})
		}
	},

	cancel: function() {
		this.parentVM.set('loginOpen', false)
		this.doClose()
		location.href = '/';
	},

	specialKey: function(e) {
		if (e.getKey() == e.ENTER) this.authenticateUser()
	},

	loginWindowShow: function(username, password) {
		this.set('username', username.getValue());
		this.set('password', password.getValue());
	}
})