glu.defModel('RS.client.Menu0', {
	selectThatSetWhenAllSetsLoaded: '',
	hidden : false,
	activate: function(screens, params) {
		clientVM.restoreDefaultWindowTitle()
		if (params.menuSet) {
			this.set('selectThatSetWhenAllSetsLoaded', params.menuSet);
			this.selectThePredefinedSet();
		}
	},

	selectThePredefinedSet: function() {
		if (clientVM.menuSets.length > 0) {
			var menuSetsMenu = this.menuDisplaySections.getAt(0),
				menuSetsMenuDisplayItems = menuSetsMenu.displayItems
			menuSetsMenuDisplayItems.forEach(function(item) {
				if (item.text == this.selectThatSetWhenAllSetsLoaded) {
					this.set('selectThatSetWhenAllSetsLoaded', '');
					this.set('lastSet', item.menuId);
				}
			}, this)
		}
	},

	lastSet: '',
	when_last_set_changes_process_new_menus_and_update_state: {
		on: ['lastSetChanged'],
		action: function() {
			Ext.state.Manager.set('userMenuSet', this.lastSet)
			this.processSections()
		}
	},

	menuDisplaySections: {
		mtype: 'list',
		autoParent: true
	},

	recentlyUsed: {
		mtype: 'list',
		autoParent: true
	},
	recentlyUsedSection: null,
	maxRecentlyUsed: 10,
	maxRecentUser: 5,

	init: function() {
		var recent = Ext.state.Manager.get(clientVM.username + 'UserMenuRecentlyUsed', '[]') || '[]',
			recentlyUsedState = Ext.decode(recent);

		this.set('maxRecentlyUsed', Ext.state.Manager.get('maxRecentlyUsed', 10))
		this.set('maxRecentUser', Ext.state.Manager.get('maxRecentUser', 5))
			//Add in the Menu Sets display Section
		this.menuDisplaySections.add(this.model({
			mtype: 'MenuSection',
			text: this.localize('MenuSets'),
			id: '-1',
			items: [{
				text: this.localize('All'),
				id: '-1'
			}]
		}))

		//Add in the recently used display section
		this.recentlyUsedSection = this.model({
			mtype: 'MenuSection',
			text: this.localize('RecentlyUsed'),
			id: '-2',
			displayItems: this.recentlyUsed
		})
		this.menuDisplaySections.add(this.recentlyUsedSection)

		//Restore recently used from user preferences
		if (recentlyUsedState && Ext.isArray(recentlyUsedState)) {
			Ext.each(recentlyUsedState, function(stateItem) {
				this.recentlyUsed.add(this.recentlyUsedSection.model(Ext.apply(stateItem, {
					mtype: 'MenuSectionItem'
				})))
			}, this)
		}

		//Restore lastSet menuSet from state
		this.set('lastSet', Ext.state.Manager.get('userMenuSet', -1))

		clientVM.on('menuSetsChanged', this.processMenuSets, this)
		clientVM.on('menuSectionsChanged', this.processSections, this)
		clientVM.on('beforeScreenChanged', this.processScreenChange, this)
		clientVM.on('userSettingsChanged', this.activateMenu, this)

		this.processMenuSets()
		this.processSections()
	},
	activateMenu : function(){
		var showSideMenu = clientVM.userSettings.sideMenuDisplay;
		this.set('hidden', showSideMenu == true);
	},
	processMenuSets: function() {
		var menuSetsMenu = this.menuDisplaySections.getAt(0),
			menuSetsMenuDisplayItems = menuSetsMenu.displayItems

		while (menuSetsMenuDisplayItems.length > 1) menuSetsMenuDisplayItems.removeAt(1)
		Ext.each(clientVM.menuSets, function(menuSet) {
			var text = menuSet.text || menuSet.name;
			var menuId = menuSet.menuId || menuSet.menuSetID || menuSet.menuSetLink || '-1';
			menuSetsMenuDisplayItems.add({
				mtype: 'MenuSectionItem',
				text: text,
				menuId: menuId
			})
		}, this)
		this.selectThePredefinedSet();
	},

	processSections: function() {
		if (!this.lastSet && clientVM.menuSets && clientVM.menuSets.length > 1) {
			this.set('lastSet', clientVM.menuSets[1].menuId)
			return
		}

		while (this.menuDisplaySections.length > 2) this.menuDisplaySections.removeAt(2)

		Ext.Array.forEach(clientVM.menuSections, function(section) {
			if (section.sets && (Ext.Array.indexOf(section.sets, this.lastSet) > -1) || this.lastSet == -1) {
				this.menuDisplaySections.add(this.model({
					mtype: 'MenuSection',
					id: section.id,
					text: section.text,
					items: section.items,
					color: section.color,
					textColor: section.textColor
				}))
			}
		}, this)

		var count = 0
		while (count < 4) {
			this.menuDisplaySections.add(this.model({
				mtype: 'MenuSectionPadding'
			}))
			count++;
		}

		this.syncSectionsWithCache()
	},

	syncSectionsWithCache: function() {
		var recent = Ext.state.Manager.get(clientVM.username + 'UserMenuRecentlyUsed', '[]') || '[]',
			recentlyUsedState = Ext.decode(recent);

		Ext.Array.forEach(clientVM.menuSections, function(section) {
			Ext.Array.forEach(section.items || [], function(item) {
				var menuItem = clientVM.processMenuItem(item)
				if (menuItem) {
					Ext.Array.each(recentlyUsedState, function(recentItem) {
						if (item.menuId == recentItem.menuId) {
							recentItem.text = menuItem.text
							recentItem.modelName = menuItem.modelName
							recentItem.location = menuItem.location
							recentItem.params = menuItem.params
							return false
						}
					}, this)

					this.recentlyUsed.forEach(function(recentMenu) {
						if (item.menuId == recentMenu.menuId) {
							recentMenu.set('modelName', menuItem.modelName)
							recentMenu.set('params', menuItem.params)
							recentMenu.set('location', menuItem.location)
							recentMenu.set('text', menuItem.text)
						}
					}, this)
				}
			}, this)
		}, this)
		var recentUsersStr = Ext.state.Manager.get('recentUsers', '[]');
		var recentUsers = Ext.decode(recentUsersStr);
		recentUsers = Ext.Array.remove(recentUsers, clientVM.username);
		recentUsers.push(clientVM.username);
		if (recentUsers.length > this.maxRecentUser) {
			var oldest = recentUsers.shift();
			Ext.state.Manager.clear(oldest + 'UserMenuRecentlyUsed');
		}
		Ext.state.Manager.set('recentUsers', Ext.encode(recentUsers));
		Ext.state.Manager.set(clientVM.username + 'UserMenuRecentlyUsed', Ext.encode(recentlyUsedState))
	},

	selectMenuSet: function(setId) {
		this.set('lastSet', setId)
	},

	processScreenChange: function(vm, screenConfig) {
		if (screenConfig.id == '-1') return
			// handle the recently used menu items list
		var recent = Ext.state.Manager.get(clientVM.username + 'UserMenuRecentlyUsed', '[]') || '[]',
			recentlyUsedState = Ext.decode(recent),
			found = false,
			i = 0,
			max = this.maxRecentlyUsed,
			len = 0,
			mostRecent = null;

		Ext.Array.each(recentlyUsedState, function(recent) {
			if (recent.menuId == screenConfig.id) {
				found = true
				mostRecent = recent
				return false
			}
		})

		if (!found && screenConfig.id) {
			recentlyUsedState.splice(0, 0, {
				menuId: screenConfig.id,
				text: screenConfig.text,
				modelName: screenConfig.modelName,
				params: screenConfig.params
			})
		} else if (found && mostRecent) {
			recentlyUsedState.splice(recentlyUsedState.indexOf(mostRecent), 1)
			recentlyUsedState.splice(0, 0, mostRecent)
			recentlyUsedState = recentlyUsedState.slice(0, this.maxRecentlyUsed)
		}

		//Persist the current recently used information to state
		var recentUsersStr = Ext.state.Manager.get('recentUsers', '[]');
		var recentUsers = Ext.decode(recentUsersStr);
		recentUsers = Ext.Array.remove(recentUsers, clientVM.username);
		recentUsers.push(clientVM.username);
		if (recentUsers.length > this.maxRecentUser) {
			var oldest = recentUsers.shift();
			Ext.state.Manager.clear(oldest + 'UserMenuRecentlyUsed');
		}
		Ext.state.Manager.set('recentUsers', Ext.encode(recentUsers));
		Ext.state.Manager.set(clientVM.username + 'UserMenuRecentlyUsed', Ext.encode(recentlyUsedState))

		//Clear existing list of recently used items (except the header)
		while (this.recentlyUsed.length > 1) this.recentlyUsed.removeAt(1)

		//Add in the recently used items to be displayed to the user
		len = recentlyUsedState.length
		for (; i < len && i < len && i < max; i++) {
			this.recentlyUsed.add(this.recentlyUsedSection.model(Ext.apply({
				mtype: 'MenuSectionItem'
			}, recentlyUsedState[i])))
		}
	},

	handleMenuClick: function(menuItem) {
		if (clientVM.processMenuItem(menuItem)) {
			clientVM.handleNavigationClick(menuItem)
			return
		}

		//process menu set click
		this.set('lastSet', menuItem.menuId)
	}
})