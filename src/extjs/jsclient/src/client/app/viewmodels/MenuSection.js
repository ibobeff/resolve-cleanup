glu.defModel('RS.client.MenuSection', {

	fields: ['id', 'text', 'color', 'textColor'],

	id: '',
	text: '',
	color: '',
	textColor: '',
	sets: [],
	items: [],

	displayItems: {
		mtype: 'list',
		autoParent: true
	},

	init: function() {
		this.displayItems.insert(0, {
			menuId: this.id,
			mtype: 'MenuSectionItem',
			text: this.text,
			isHeader: true
		})

		//gather groups for items
		var groups = [];
		Ext.each(this.items, function(item) {
			var found = false;
			Ext.each(groups, function(group) {
				if (group.text == item.group) {
					found = true
					group.items.push(item)
				}
			})
			if (!found) groups.push({
				text: item.group,
				items: [item]
			})
		})

		groups.sort(function(a, b) {
			if (a.order < b.order) return -1
			if (a.order > b.order) return 1
			return 0
		})

		Ext.each(groups, function(group) {
			if (group.text) this.displayItems.add(this.model(Ext.apply(group, {
				mtype: 'MenuSectionItem',
				isGroup: true
			})))

			group.items.sort(function(a, b) {
				if (a.order < b.order) return -1
				if (a.order > b.order) return 1
				return 0
			})

			Ext.each(group.items, function(item) {
				this.displayItems.add(this.model(Ext.apply(item, {
					mtype: 'MenuSectionItem',
					isUnderGroup: group.text
				})))
			}, this)
		}, this)
	}
})

glu.defModel('RS.client.MenuSectionItem', {
	menuId: '',
	modelName: '',
	location: '',
	params: '',
	isHeader: false,
	isGroup: false,
	isUnderGroup: false,
	group: '',
	text: '',
	setupClick$: function() {
		return !this.isHeader && !this.isGroup
	},
	displayText$: function() {
		var cls = 'rs-menu-section-text '
		if (this.isHeader) cls = ' rs-menu-section-header'
		if (this.isGroup) cls = ' rs-menu-section-group'
		if (this.isUnderGroup) cls += ' rs-menu-section-subtext'

		if (this.parentVM.id == '-1') {
			if (this.parentVM.parentVM.lastSet == this.menuId) {
				cls += ' rs-menu-section-text-bold'
			}
		}

		return Ext.String.format('<span class="{1}" {2}>{0}</span>', this.setupClick ? '<span class="{1} rs-menu-section-bullet" {2}>• </span>' + Ext.String.htmlEncode(this.text) : Ext.String.htmlEncode(this.text), cls, this.parentVM.textColor ? Ext.String.format('style="color:#{0}"', this.parentVM.textColor) : '')
	},
	displayStyle$: function() {
		return this.isHeader ? 'text-align: center; padding: 10px' : 'padding: 2px'
	},
	menuItemClicked: function() {
		this.rootVM.handleMenuClick(this)
	}
})

glu.defModel('RS.client.MenuSectionPadding', {})