glu.defModel('RS.client.SideMenu', {
	currentMenuSetLink: 'All',
	sideMenuIsVisible: false,
	menuSections: {
		mtype: 'treestore',
		fields: ['menuId', 'text'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'root',
				idProperty: 'menuId'
			}
		},
		root: {
			name: 'Root',
			expanded: true
		}
	},
	menuSets: {
		mtype: 'store',
		fields: ['name', 'menuSetLink']
	},
	menu: null,
	mapping: {},
	init : function(){
		clientVM.on('userSettingsChanged', this.activateMenu, this);
	},		
	activateMenu : function(){
		var showSideMenu = clientVM.userSettings.sideMenuDisplay;
		this.set('sideMenuIsVisible', showSideMenu == true);
	},	
	updateMenu: function(menu) {
		this.set('menu', menu);

		Ext.each(menu.sets, function(set) {
			this.menuSets.add(set);
		}, this);
		this.set('mapping', {
			'': menu.sections
		});
		Ext.each(menu.sets, function(set) {
			Ext.each(menu.sections, function(section) {
				Ext.each(section.sets, function(linkId) {
					if (set.menuSetLink != linkId)
						return;
					if (!this.mapping[set.menuSetLink])
						this.mapping[set.menuSetLink] = []
					this.mapping[set.menuSetLink].push(section)
				}, this);
			}, this);
		}, this);
		this.set('currentMenuSetLink', Ext.state.Manager.get(clientVM.username + 'SideMenuSet', 'All'));
		this.buildTree();
	},
	when_currentMenuSetLink_changed: {
		on: ['currentMenuSetLinkChanged'],
		action: function() {
			Ext.state.Manager.set(clientVM.username + 'SideMenuSet', this.currentMenuSetLink);
			this.buildTree(this.currentMenuSetLink);
		}
	},
	buildTree: function() {
		var currentMenuSetLink = this.currentMenuSetLink == 'All' ? '' : this.currentMenuSetLink;
		var root = this.menuSections.getRootNode();
		root.removeAll();
		var sections = this.mapping[currentMenuSetLink];
		Ext.each(sections, function(section) {
			var node = root.appendChild(section);
			Ext.each(section.items, function(item) {
				if (item.group) {
					var g = node.findChild('text', item.group);
					if (!g)
						g = node.appendChild({
							menuId: Ext.data.IdGenerator.get('uuid').generate(),
							text: item.group,
							expanded: true,
							expandable: false
						});
					g.appendChild(Ext.apply({
						leaf: true
					}, item));
				} else
					node.appendChild(Ext.apply({
						leaf: true
					}, item));
			});
		});
	},
	menuItemClick: function(record) {
		if (!record.isLeaf())
			return;
		if (this.parentVM.processMenuItem(record.raw)) {
			this.parentVM.handleNavigationClick(record.raw)
			return
		}
	}
});