glu.defModel('RS.client.MenuBlock', {
	hidden: false,
	text: null, //Section Title
	menuId: null,
	sets: [], //SetIds that item belongs to
	items: [],
	glyph: '',

	color: null,
	group: null,
	location: null,
	modelName: null,
	order: null,
	textColor: null,

	menuLinks: {
		mtype: 'list',
	},

	toggleVisibility: function(currentMenuLink, isAll) {
		if (isAll) {
			this.set('hidden', false);
		}	else {
			this.set('hidden', (this.sets.indexOf(currentMenuLink) !== -1) ? false : true);
		}
	},

	generateItems$: function() {
		var currentSubGroup = '';
		this.items.map(function(link) {
			if (link.group !== '' && currentSubGroup !== link.group) {
				currentSubGroup = link.group;
				this.menuLinks.add({mtype: 'RS.client.MenuGroup', 
					text: Ext.util.Format.htmlEncode(link.group)
				});
			}
			this.menuLinks.add({mtype: 'RS.client.MenuLink',
				text: Ext.util.Format.htmlEncode(link.text),
				modelName: link.modelName,
				params: link.params,
				menuId: link.menuId
			});
		}, this);
	}
});

glu.defModel('RS.client.MenuGroup', {
	text: '',
});

glu.defModel('RS.client.MenuLink', {
	text: '',
	modelName: '',
	params: {},
	menuId: '',

	handleClick: function() {
		clientVM.handleNavigation({
			menuId: this.menuId,
			text: this.text,
			modelName: this.modelName,
			params: this.params
		})
	}
});