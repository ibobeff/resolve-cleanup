glu.defModel('RS.client.UserInfo', {
	target: null,
	logoutIsEnabled: true,
	csrftoken: '',
	showUserInfo: false,

	user$: function() {
		return clientVM.user
	},

	userTitle$: function() {
		return this.user.fullName
	},

	profilePicUrl$: function() {
		return this.csrftoken !== '' ? Ext.String.format('/resolve/service/user/downloadProfile?username={0}&{1}', this.user.name, this.csrftoken) : '';
	},

	fullname$: function() {
		return this.user.fullName
	},
	name$: function() {
		return this.user.name
	},

	html$: function() {
		return Ext.String.format('<table><tr><td></td></tr></table>')
	},
	
	init: function() {
		this.initToken();
		if (clientVM.adfsEnabled === 'true' || clientVM.adfsEnabled === true) {
			this.set('logoutIsEnabled', false);
		} else {
			this.set('logoutIsEnabled', true);
		}

		setTimeout(function(){
			this.set('showUserInfo', true);
		}.bind(this), 100);

		window.addEventListener('click', this.checkClickEvents);
	},

	activate: function() {
		this.initToken();
	},

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/user/downloadProfile', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this))
	},

	checkClickEvents: function(e) {
		// close userInfoWin if clicked outside of the modal
		if (clientVM.userInfoWin && clientVM.userInfoWin.showUserInfo) {
			var hideUserInfoWin = true;
			for (var i=0; i < e.path.length; i++) {
				var path = e.path[i];
				if (path.nodeName == '#document') {
					// reach outter most parent without finding the userInfoWin, so hideUserInfoWin is true
					break;
				} else if (path.classList.contains('user-info') || path.classList.contains('navigation-menu')) {
					// found userInfoWin so hideUserInfoWin is false
					hideUserInfoWin = false;
				}
			}
			if (hideUserInfoWin) {
				clientVM.userInfoWin.doClose();
				clientVM.userInfoWin = null
			}
		}
	},

	beforeDestroyComponent: function() {
		window.removeEventListener('click', this.checkClickEvents);
	},

	settings: function() {
		this.parentVM.handleNavigation({
			modelName: 'RS.user.User',
			params: {
				username: this.user.name
			}
		})
		this.doClose()
	},
	logout: function() {
		this.parentVM.logout()
		this.doClose()
	},

	closeClicked: function() {
		this.doClose()
	}
})
