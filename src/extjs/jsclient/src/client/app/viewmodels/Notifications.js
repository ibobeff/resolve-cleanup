glu.defModel('RS.client.Notifications', {
	target: null,

	notifications: {
		mtype: 'list'
	},

	init: function() {
		this.notifications.add(this.model({
			mtype: 'RS.client.Notification',
			title: this.localize('social'),
			unreadMessages: this.parentVM.user.notificationUnreadCount || 0,
			gotoText: this.localize('goToSocial'),
			dismissText: this.localize('dismissSocial'),
			unreadText: this.localize('socialText'),
			type: 'social'
		}))
	},

	closeClicked: function() {
		this.doClose()
	},

	dismissAll: function() {
		//Change next poll to use "now" as current highest point in time for new notifications
		Ext.state.Manager.set('notificationIgnore', Ext.Date.format(new Date(), 'time'))

		this.doClose()
	},

	goTo: function(type) {
		this.parentVM.handleNavigation({
			modelName: RS.client.NotificationMap[type]
		})
		this.doClose()
	}
})

Ext.namespace('RS.client.NotificationMap')
Ext.applyIf(RS.client.NotificationMap, {
	social: 'RS.social.Main'
})

glu.defModel('RS.client.Notification', {
	title: '',
	unreadMessages: 0,

	gotoText: '',
	dismissText: '',

	unreadText: '',

	type: '',

	messageText$: function() {
		switch (this.unreadMessages) {
			case 0:
				return this.localize('noUnreadMessages', [this.unreadMessages, this.unreadText])
			case 1:
				return this.localize('unreadMessage', [this.unreadMessages, this.unreadText])
			default:
				return this.localize('unreadMessages', [this.unreadMessages, this.unreadText])
		}
	},

	init: function() {},

	goTo: function() {
		this.parentVM.goTo(this.type)
	},
	dismiss: function() {
		//TODO: Clear the individual type, but for now its the same as dismiss all until we can get something other than social
		this.parentVM.dismissAll()
	}
})
