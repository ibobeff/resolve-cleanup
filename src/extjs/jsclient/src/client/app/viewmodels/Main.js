glu.defModel('RS.client.Main', {
	API : {
		submit : '/resolve/service/execute/submit',
		routingLookup : '/resolve/service/resolutionrouting/routing/lookup',
		getSystemProperty : '/resolve/service/sysproperties/getSystemPropertyByName',
		getSystemProperties : '/resolve/service/sysproperties/getAllProperties',
		wikiLookNotBlocked : '/resolve/service/wikilookupnotblocked/match',
		getArchiveWS : '/resolve/service/worksheet/getArchive',
		getWS : '/resolve/service/worksheet/get',
		listWS : '/resolve/service/worksheet/list',
		getActiveWS : '/resolve/service/worksheet/getActive',
		newWS : '/resolve/service/worksheet/newWorksheet',
		poll : '/resolve/service/client/poll',
		getUser : '/resolve/service/client/getUser',
		getCatalogNames : '/resolve/service/catalog/names',
		getPlaybookTemplates : '/resolve/service/wikiadmin/listPlaybookTemplates',
		getLicenseSummary : '/resolve/service/license/summary',
		getUsageSummaryV2 : '/resolve/service/saas/usagesummaryv2'
	},
	visibility : 'hidden',
	mock: false,
	pageTokens: [],
	CSRFTOKEN_NAME: '',
	rsclientToken: '',
	isAdmin: false,
	displayClient: true,
	mockScreen: false,
	backend: null,
	currentNotificationLevel : null,
	notificationLevel : {
		'info' : 0,
		'warn' : 1,
		'error' : 2,
		'critical' : 3,
		'none' : 4
	},
	serverErrorTxt: '',
	errorTitle: '',
	userOrganization : {},
	sideMenu: {
		mtype: 'SideMenu'
	},
	actionTaskPropertyStore : {
		mtype : 'store',
		fields : ['uname','uvalue'],
		proxy : {
			type : 'ajax',
			url :'/resolve/service/atproperties/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		},
		pageSize : -1
	},
	getActionTaskPropertyStore : function(){
		return this.actionTaskPropertyStore;
	},
	routes: {},
	defaultWindowTitle: '',
	restoreDefaultWindowTitle: function() {
		window.document.title = this.defaultWindowTitle
	},
	problemId: '',
	archivedProblemId: '',
	fromArchive: false,
	problemNumber: '',

	orgId: '',
	orgName : '',
	logo: '',
	logoHidden: false,
	logoHeight: 0,
	logoWidth: 0,
	rightLogo: '',
	rightLogoHidden: false,
	rightLogoHeight: 0,
	rightLogoWidth: 0,

	toolbarLogo: '',
	toolbarLogoHidden: false,
	toolbarLogoHeight: 0,
	toolbarLogoWidth: 0,
	toolbarRightLogo: '',
	toolbarRightLogoHidden: false,
	toolbarRightLogoHeight: 0,
	toolbarRightLogoWidth: 0,


	when_problemNumber_changes_update_browser_title: {
		on: ['problemNumberChanged'],
		action: function() {
			this.setWindowTitle(this.lastTitle)
		}
	},

	bannerHeight$: function() {
		return Math.max(this.logoHeight, this.rightLogoHeight);
	},

	getWindowHeight: function () {
		var w = window,
			e = document.documentElement,
			g = document.getElementsByTagName('body')[0];
		return w.innerHeight || e.clientHeight || g.clientHeight;
	},

	getWindowWidth: function () {
		var w = window,
			e = document.documentElement,
			g = document.getElementsByTagName('body')[0];
		return w.innerWidth || e.clientWidth || g.clientWidth;
	},

	logoIsVisible$: function() {
		return (!this.logoHidden && !this.bannerURL.length);
	},

	rightLogoIsVisible$: function() {
		return (!this.rightLogoHidden && !this.bannerURL.length);
	},

	setLogoSize: function() {
		var me = this.me,
			img = this.image,
			logoInfo =  this.logoInfo,
			logoType = this.logo,
			heightName = logoType + 'Height',
			widthName = logoType + 'Width',
			logoHeight = parseInt(logoInfo[heightName]),
			logoWidth = parseInt(logoInfo[widthName]);

		if (isNaN(logoHeight))
			logoHeight = 'auto';
		if (isNaN(logoWidth))
			logoWidth = 'auto';

		if(typeof(logoHeight) == 'string')
			logoHeight = logoHeight.toLowerCase();
		if(typeof(logoWidth) == 'string')
			logoWidth = logoWidth.toLowerCase();

		if (logoHeight == 'auto' && logoWidth == 'auto') {
			me.set(heightName, img.naturalHeight);
			me.set(widthName, img.naturalWidth );
		} else if (logoHeight == 'auto' && logoWidth != 'auto') {
			me.set(heightName, (logoWidth/img.naturalWidth) * img.naturalHeight);
			me.set(widthName, logoWidth );
		} else if (logoHeight != 'auto' && logoWidth == 'auto') {
			me.set(heightName,logoHeight );
			me.set(widthName, (logoHeight/img.naturalHeight) * img.naturalWidth);
		} else {
			me.set(heightName, logoHeight);
			me.set(widthName, logoWidth);
		}
	},

	supportedImageTypes: ['gif', 'png', 'jpg', 'jpeg'],

	computeLogoSize: function(logoInfo, logo) {
		if (!logoInfo[logo].trim())
			return;

		var image = new Image();
		var imageSrc = '/resolve/images/' + logoInfo[logo];
		// check for supported image types
		var split = imageSrc.split('.');
		var extension = split[split.length - 1].toLowerCase();
		if (Ext.Array.indexOf(this.supportedImageTypes, extension) == -1) {
			// only check for internally linked image
			if (imageSrc.indexOf('/resolve/') === 0 && imageSrc.indexOf(this.CSRFTOKEN_NAME) === -1) {
				this.getCSRFToken_ForURI(imageSrc, function(data, uri) {
					var csrftoken = '?' + data[0] + '=' + data[1];
					var tokenizedData = uri + csrftoken;
					imageSrc = tokenizedData;
				}); 
			}
		}

		image.src = imageSrc;
		image.addEventListener('load', this.setLogoSize.bind({
			image: image,
			me: this,
			logoInfo: logoInfo,
			logo: logo
		}));
	},

	getCSRFTokenForLogo: function(logo) {
		if (logo) {
			logo = '/resolve/images/' + logo;
			// check for supported image types
			var split = logo.split('.');
			var extension = split[split.length - 1].toLowerCase();
			if (Ext.Array.indexOf(this.supportedImageTypes, extension) == -1) {
				// only check for internally linked image
				if (logo.indexOf('/resolve/') === 0 && logo.indexOf(this.CSRFTOKEN_NAME) === -1) {
					this.getCSRFToken_ForURI(logo, function(data, uri) {
						var csrftoken = '?' + data[0] + '=' + data[1];
						var tokenizedData = uri + csrftoken;
						logo = tokenizedData;
					}); 
				}
			}
			return logo;
		} else {
			return '';
		}
	},

	bindLogosProperties: function() {
		if (!this.resolveLogoInfo) {
			return;
		}
		var logoInfo = Ext.JSON.decode(this.resolveLogoInfo);

		this.computeLogoSize(logoInfo, 'logo');
		this.computeLogoSize(logoInfo, 'rightLogo');
		this.set('logo', this.getCSRFTokenForLogo(logoInfo.logo));
		this.set('logoHidden', (logoInfo.logoHidden.toLowerCase() == 'true')? true: false );
		this.set('rightLogo', this.getCSRFTokenForLogo(logoInfo.rightLogo));
		this.set('rightLogoHidden', (logoInfo.rightLogoHidden.toLowerCase() == 'true')? true: false );
	},

	bindToolbarLogosProperties: function() {
		if (!this.resolveToolbarLogoInfo) {
			return;
		}
		var toolbarLogoInfo = Ext.JSON.decode(this.resolveToolbarLogoInfo);

		this.computeLogoSize(toolbarLogoInfo, 'toolbarLogo');
		this.computeLogoSize(toolbarLogoInfo, 'toolbarRightLogo');
		this.set('toolbarLogo', this.getCSRFTokenForLogo(toolbarLogoInfo.toolbarLogo));
		this.set('toolbarLogoHidden', (toolbarLogoInfo.toolbarLogoHidden.toLowerCase() == 'true')? true: false);
		this.set('toolbarRightLogo', this.getCSRFTokenForLogo(toolbarLogoInfo.toolbarRightLogo));
		this.set('toolbarRightLogoHidden', (toolbarLogoInfo.toolbarRightLogoHidden.toLowerCase() == 'true')? true: false);
	},

	init: function() {
		window.clientVM = this;  
		this.initCSRFToken();
		this.removeLogoutFlag();
		this.bindLogosProperties();
		this.bindToolbarLogosProperties();
		if(this.currentNotificationLevel && this.notificationLevel.hasOwnProperty(this.currentNotificationLevel.toLowerCase()))
			this.currentNotificationLevel = this.notificationLevel[this.currentNotificationLevel.toLowerCase()];
		else
			this.currentNotificationLevel = this.notificationLevel['info'];
		Ext.fly(document).on('keyup', function(evt) {
			if (evt.ctrlKey && evt.getCharCode() == evt.S) {
				evt.stopEvent();
				glu.log.info('Ctrl+S pressed! clientVM stop the default behavior!')
			}
		})
		Ext.fly(document).on('keydown', function(evt) {
			if (evt.ctrlKey && evt.getCharCode() == evt.S) {
				evt.stopEvent();
				glu.log.info('Ctrl+S pressed! clientVM stop the default behavior!')
			}
		})
		Ext.fly(document).on('keypress', function(evt) {
			if (evt.ctrlKey && evt.getCharCode() == evt.S) {
				evt.stopEvent();
				glu.log.info('Ctrl+S pressed! clientVM stop the default behavior!')
			}
		})
		window.onbeforeunload = this.closeChildWindows
		//Called when mocking the backend to test the UI

		if (this.mock) {
			this.backend = RS.client.createMockBackend(true)
		}

		//Restore banner display state
		var displayBanner = Ext.state.Manager.get('bannerIsVisible', true)
		this.set('bannerIsVisible', Ext.isBoolean(displayBanner) ? displayBanner : true)
		this.set('defaultWindowTitle', window.document.title)

		//Setup routes for history management
		this.routes = {
			':modelName/:params/:sticky': this.goToScreen,
			':modelName/:params': this.goToScreen,
			':modelName': this.goToScreen
		}

		//Initialize translated strings
		this.set('bannerText', this.localize('RunbookAutomation'));

		//Parse out the variables on the window to determine if we need to do anything with the problemId or redirection
		var windowParams = {};
		try {
			windowParams = Ext.Object.fromQueryString(window.location.search || '')
		} catch (e) {
			clientVM.displayError(e);
		}
		this.set('windowParams', windowParams);

		//THIS APPLIES SPECIALLY FOR SERVICENOW.
		if(windowParams.hasOwnProperty('SSOTYPE')){
			var type = windowParams['SSOTYPE'];
		}
		//See if windowParams contains anything about hiding the client, if so then hide it
		if (this.windowParams) {
			if (this.windowParams.displayClient === false || this.windowParams.displayClient === 'false') {
				this.set('toolbarIsVisible', false);
				this.set('bannerIsVisible', false);
				this.set('displayClient', false);
			}
			if (this.windowParams[this.CSRFTOKEN_NAME]) {
				this.rsclientToken = this.CSRFTOKEN_NAME+'='+this.windowParams[this.CSRFTOKEN_NAME];
			}
		}

		//Initialize History Management
		Ext.History.add = function(hash) {
			var me = this,
				win = me.useTopWindow ? getResolveRoot() : window;
			try {
				win.location.hash = hash;
			} catch (e) {
				// IE can give Access Denied (esp. in popup windows)
			}
		}
		Ext.ux.Router.init(this);

		if (this.redirectFromLegacyUrl(windowParams)) {
			this.set('redirectStarted', true);
		} else {
			//load the search store with records from previous searches
			var searchHistory = Ext.state.Manager.get('searchHistory', [])
			this.searchStore.add(searchHistory)
		}
		//Get all system properties needed
		this.getSystemProperty();

		//Go load the user to display
		this.getUser(true);

		//Load actiontask property
		this.actionTaskPropertyStore.load();

		//Set any flag indicate waiting for problemId to false when new id come back
		this.on('problemIdFetchedFromUrl', function(){
			this.set('waitClientVMFetchProblemIdFromUrl', false);
		})

		this.set('serverErrorTxt', this.localize('serverError'));
		this.set('errorTitle', this.localize('error'));
		this.set('pollErrCount', 0);

		this.getFileUploadMaxSize();

		window.addEventListener('storage', this.checkStorageEvents);
	},

	checkLoginOpen: function() {
		if (this.loginOpen) {
			this.loginDialog.doClose()
			this.set('loginOpen', false);
			this.set('authenticated', true);
			this.getUser();
		}
	},

	checkStorageEvents: function(e){
		switch (e.key) {
		case 'csrftoken-flag':
			if (localStorage.getItem('csrftoken-flag')) {
				clientVM.refreshPageTokens();
				clientVM.checkLoginOpen();
			}
			break;
		case 'logout-flag':
			if (localStorage.getItem('logout-flag')) {
				clientVM.displayAuthenticateUserDialog()
			}
			break;
		case 'update-license':
		case 'expiration-banner':
			clientVM.checkLicenseBanner();
			break;
		default:
			break;
		}
    },

	initPageTokens: function(isRefresh) {
		// get session token
		var token_pair = this.getCSRFToken();
		token_pair = token_pair.split(":");
		this.updateCSRFToken(token_pair);

		// get rsclient.jsp token
		this.getCSRFToken_ForURI('/resolve/jsp/rsclient.jsp', function(token_pair) {
			this.rsclientToken = token_pair[0] + '=' + token_pair[1]; 
			this.set('CSRFTOKEN_NAME', token_pair[0]); // CSRF Token name is configurable in csrfguard.properties
		}.bind(this), null, null, true);

		if (!isRefresh) {
			// get wiki/view token
			this.getCSRFToken_ForURI('/resolve/service/wiki/view', function(token_pair) {
				this.setPageToken('/resolve/service/wiki/view', token_pair[1]);
			}.bind(this));
		}
	},

	refreshPageTokens: function() {
		// refresh session and rsclientToken
		this.initPageTokens(true);

		// refresh wiki/view and any other dynamically generated tokens
		Ext.Object.each(this.pageTokens, function(key, value) {
			this.refreshPageToken('/resolve/'+key);
		}.bind(this));
	},

	refreshPageToken: function(uri) {
		this.getCSRFToken_ForURI(uri, function(token_pair, uri) {
			this.setPageToken(uri, token_pair[1]);
		}.bind(this), null, null, false, true);
	},

	setPageToken: function(page, token) {
		this.pageTokens[page.replace('/resolve/', '')] = token;
	},

	clearPageTokens: function() {
		this.pageTokens = [];
	},

	getPageToken: function(page) {
		if (this.pageTokens && this.pageTokens[page]) {
			return this.pageTokens[page];
		}
		return '';
	},

	getSystemProperty : function(){
		//Get orginization label
		this.ajax({
			url: this.API['getSystemProperties'],
			params: {
				filter: Ext.encode([{"field":"uname","type":"auto","condition":"contains","value":"menu.organization.label|sso.adfs.enabled"}])
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(respData.message);
					return;
				}
				var org = respData.records.filter(function(r) { return r.uname === 'menu.organization.label'})[0];
				var adfsEnabled = respData.records.filter(function(r) { return r.uname === 'sso.adfs.enabled'})[0];
				clientVM.orgLabel = org.uvalue || this.localize('organization');
				clientVM.adfsEnabled = adfsEnabled.uvalue;
			},
			failure: function(resp) {
				this.displayFailure(resp);
			}
		})

		//Get default execution debug mode.
		this.ajax({
			url: this.API['getSystemProperty'],
			params: {
				name: 'automation.execution.debugmode'
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(respData.message);
					return;
				}
				clientVM.executionDebugDefaultMode = respData.data.uvalue === "true";
			},
			failure: function(resp) {
				this.displayFailure(resp);
			}
		})
	},
	updateProblemInfo: function(problemId, problemNumber, fromArchive) {
		//only have the problemId, so go get the problemNumber from the server
		// If we are in SIR context, all problemID update should always use SIR_PROBLEMID
		this.set('problemId', clientVM.SIR_PROBLEMID || problemId)
		this.set('fromArchive', !!fromArchive);

		if (this.fromArchive) {
			this.set('archivedProblemId', problemId);
		}

		if (problemNumber) {
			this.set('problemNumber', problemNumber)
		} else {
			this.ajax({
				url: fromArchive ? this.API['getArchiveWS'] : this.API['getWS'],
				params: {
					id: fromArchive ? this.archivedProblemId : this.problemId
				},
				scope: this,
				success: function (r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						if (response.data) {
							this.set('problemNumber', response.data.number);
						} else {
							this.displayError(this.localize('worksheetNotFound'));
						}
					} else {
						this.displayError(response.message);
					}
				},
				failure: function (r) {
					this.displayFailure(r)
				}
			});
		}
	},
	lastTitle: '',
	setWindowTitle: function(title) {
		this.set('lastTitle', title)
		window.document.title = (this.problemNumber ? '[' + this.problemNumber + '] - ' : '') + title + (this.defaultWindowTitle && !title ? ' - ' + this.defaultWindowTitle : '')
	},

	closeChildWindows: function() {
		Ext.Array.forEach(clientVM.childWindows, function(win) {
			if (win && win.close)
				win.close()
		})
	},

	windowHash: '',
	updateWindowHash: function() {
		this.set('windowHash', window.location.hash);
	},

	windowParams: {},
	doWikiLookup: function(params) {
		params.lookup = params.lookup || history.state && history.state.lookup;
		var query = this.removeParamAndEncode(params, ['lookup', this.CSRFTOKEN_NAME])
		this.ajax({
			url: this.API['wikiLookNotBlocked'],
			params: {
				lookup: params.lookup
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					this.displayError(this.localize('lookupError'), respData.message);
					return;
				}
				var wikiName = respData.data;
				var path;
				if (window.location.pathname.indexOf('rsclient.jsp') != -1) {
					path = Ext.String.format('/resolve/jsp/rsclient.jsp?{0}{1}#RS.wiki.Main/name={2}', clientVM.rsclientToken, (query ? '&' + query : ''), wikiName)
					if (!Ext.isIE9m)
						history.replaceState({
							lookup: params.lookup
						}, '', path);
				} else {
					path = Ext.String.format('/resolve/service/wiki/view?wiki={0}&{1}={2}{3}', wikiName, this.CSRFTOKEN_NAME, clientVM.getPageToken('service/wiki/view'), (query ? '&' + query : ''));
					if (!Ext.isIE9m) {
						history.replaceState({}, '', path);
					}
				}

				//Manually fire hashchage event since history.replaceState won't fire hashchange event.
				var event = document.createEvent("Event");
				event.initEvent('hashchange', true, false);
				window.dispatchEvent(event);
			},
			failure: function(resp) {
				this.displayFailure(resp);
			}
		})
	},
	executeRRAutomation: function(params, automation) {
		this.ajax({
			url: this.API['submit'],
			jsonData: {
				isDebug: false,
				mockName: '',
				problemId: clientVM.problemId,
				wiki: automation,
				params: Ext.apply(params, {
					'RESOLVE.ORG_NAME' : clientVM.orgName,
					'RESOLVE.ORG_ID' : clientVM.orgId
				}),
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					//clientVM.displayError(this.localize('ridMappingUIAutomationError', respData.message));
					this.displayError(respData.message);
					return;
				}
				this.displaySuccess(this.localize('rrAutomationExeReqSubmitSuccess', automation));
				this.updateProblemInfo(respData.data.problemId, respData.data.problemNumber);
			},
			failure: function(resp) {
				this.displayFailure(resp);
			}
		})
	},

	actionAfterWorksheetLoad: function(isNewWorksheet) {},

	doRidLookup: function(params, isNewWorksheet) {
		var query = this.removeParamAndEncode(params, ['rid', this.CSRFTOKEN_NAME]);
		var newParams = Ext.apply({}, params);
		delete newParams.rid;

		function runRIDLookup(){
			this.ajax({
				url: this.API['routingLookup'],
				params: {
					queryString: query,
					problemId : clientVM.problemId
				},
				scope: this,
				success: function(resp) {
					var respData = RS.common.parsePayload(resp);

					if (respData.success) {
						if (respData.data && respData.data.automation && (respData.data.newWorksheetOnly && isNewWorksheet || !respData.data.newWorksheetOnly)) {
							if (clientVM.windowParams.org && !(clientVM.windowParams.org.toLowerCase() == clientVM.orgName.toLowerCase() || clientVM.windowParams.org == clientVM.orgId))
								this.displayError(this.localize('mismatchedOrgOnExecution'));
							else
								this.executeRRAutomation(newParams, respData.data.automation);
						}

						if (!respData.data && newParams.lookup)//|| !respData.data.wiki) {
							this.doWikiLookup(newParams);
						else {
							//Check for SIR. SIR has higher priority.
							var sirUI = params['sir'] == "true";
							query = this.removeParamAndEncode(params, ['rid', 'lookup', 'sir', this.CSRFTOKEN_NAME]);
							if(respData.data.sir && sirUI){
								//SIR has enough information. Redirect to incident else redirect to dashboard.
								if(/*respData.data.sirOwner */ respData.data.sirId && respData.data.sirType && respData.data.sirSeverity && respData.data.sirPlaybook)
									var modelInfo = "RS.incident.Playbook/sir=" +  respData.data.sirId;
								else
									modelInfo = "RS.incident.Dashboard";
							}
							else if(respData.data.wiki){
								var regexDisplayMatch = /.*\?UDisplayMode=(.*$)/;
								var docInfo = respData.data.wiki;
								var docName = docInfo.replace(/\?.*/,'');
								var matchedDisplayComponent = docInfo.match(regexDisplayMatch) ? docInfo.match(regexDisplayMatch)[1] : null;
								if(matchedDisplayComponent){
									if(matchedDisplayComponent.toLowerCase() == "decisiontree")
										modelInfo = "RS.decisiontree.Main/name=" + docName;
									else
										modelInfo = "RS.wiki.Main/name=" + docName;
								}
								else
									modelInfo = "RS.wiki.Main/name=" + docName;
							}
							else {
								this.doWikiLookup(newParams);
								return;
							}

							var path = Ext.String.format('/resolve/jsp/rsclient.jsp?{0}{1}#{2}', clientVM.rsclientToken, (query ? '&' + query : ''), modelInfo);

							if (!Ext.isIE9m) {
								history.replaceState({}, '', path);
							}
							//Manually fire hashchage event since history.replaceState won't fire hashchange event.
							var event = document.createEvent("Event");
							event.initEvent('hashchange', true, false);
							window.dispatchEvent(event);
						}
					} else {
						clientVM.displayMessage(this.localize('mappingNotFound'), this.localize('resolutionRoutingLookupError', respData.message), {
							duration: 1000
						});

						if (newParams.lookup) {
							this.doWikiLookup(newParams);
						}

						setTimeout(function() {
							window.location.href = Ext.String.format('/resolve/jsp/rsclient.jsp?{0}{1}#{2}', clientVM.rsclientToken, (query ? '&' + query : ''), window.location.hash);
							//Manually fire hashchage event since history.replaceState won't fire hashchange event.
							var event = document.createEvent("Event");
							event.initEvent('hashchange', true, false);
							window.dispatchEvent(event);
						}, 500);
					}
				},
				failure: function(resp) {
					this.displayFailure(resp);
				}
			})
		}
		//Redirect SIR if possible
		if(params.sir == 'true' && params.sirId != null){
			var sirID = params.sirId;
			query = this.removeParamAndEncode(params, ['rid', 'lookup', 'sir', 'sirId', this.CSRFTOKEN_NAME]);
			var filter = [{
				field: 'sirId',
				type: 'auto',
				condition: 'equals',
				value:params.sirId
			}]
			this.ajax({
				url : this.API['listWS'],
				method: 'GET',
				params : {
					'RESOLVE.ORG_NAME' : clientVM.orgName,
					'RESOLVE.ORG_ID' : clientVM.orgId,
					filter: JSON.stringify(filter)
				},
				success: function(resp){
					var response = RS.common.parsePayload(resp);
					var records = response.records;
					if(records && records.length > 0){						
						var path = Ext.String.format('/resolve/jsp/rsclient.jsp?{0}{1}#RS.incident.Playbook/sir={1}', clientVM.rsclientToken, (query ? '&' + query : ''), params.sirId);
						if (!Ext.isIE9m) {
							history.replaceState({}, '', path);
						}
						//Manually fire hashchage event since history.replaceState won't fire hashchange event.
						var event = document.createEvent("Event");
						event.initEvent('hashchange', true, false);
						window.dispatchEvent(event);
					}
					else
						runRIDLookup.call(this);
				}
			})
		}
		else {
			runRIDLookup.call(this);
		}
		
	},
	removeParamAndEncode: function(params, names) {
		var removeParamList = Array.isArray(names) ? names : [names];
		var trimmed = Ext.apply({}, params);
		var trimmedFields = [];

		Ext.Object.each(trimmed, function(k, v) {
			if (!v || removeParamList.indexOf(k) != -1)
				trimmedFields.push(k);
		});

		Ext.each(trimmedFields, function(name) {
			delete trimmed[name];
		});

		return Ext.Object.toQueryString(trimmed);
	},
	removeParam: function(params, names) {
		var removeParamList = Array.isArray(names) ? names : [names];
		var trimmed = Ext.apply({}, params);
		var trimmedFields = [];

		Ext.Object.each(trimmed, function(k, v) {
			if (!v || removeParamList.indexOf(k) != -1)
				trimmedFields.push(k);
		});

		Ext.each(trimmedFields, function(name) {
			delete trimmed[name];
		});

		return trimmed;
	},
	redirectStarted: false,

	gotoWinLocationParamsWiki: function(params) {
		var safeQueryString = RS.common.encodeForURL(this.removeParamAndEncode(params, ['wiki', this.CSRFTOKEN_NAME])),
			safeWikiName = RS.common.encodeForURL(params.wiki);
		var safeURL = Ext.String.format('/resolve/jsp/rsclient.jsp?{0}#RS.wiki.Main/name={1}', (safeQueryString ? safeQueryString : ''), safeWikiName);
		window.location.replace(RS.common.decodeFromURL(safeURL));
	},

	gotoWinLocationParamsUrl: function(params) {
		const urlData = params.url.split('#');
		const uriData = urlData[0].split('?');
		if (uriData[0] == '/' || uriData[0].toLowerCase() == '/resolve' || uriData[0].toLowerCase() == '/resolve/') {
			// if param.url's base uri is root UI ("/" or "/resolve"), don't redirect to RS.client.URLScreen/location, just navigate to RS.client.Menu
			clientVM.handleNavigation({
				modelName: 'RS.client.Menu',
			});
		} else if (params.url != 'menu') {
			var safeQueryString = RS.common.encodeForURL(this.removeParamAndEncode(params, ['url', this.CSRFTOKEN_NAME])),
				safeLocaton = RS.common.encodeForURL(params.url);
			var safeURL = Ext.String.format('/resolve/jsp/rsclient.jsp?{0}#RS.client.URLScreen/location={1}', (safeQueryString ? safeQueryString : ''), safeLocaton);
			window.location.replace(RS.common.decodeFromURL(safeURL));
		} else {
			var safeURL = '/resolve/jsp/rsclient.jsp#RS.client.Menu';
			window.location.replace(RS.common.decodeFromURL(safeURL));
		}
	},

	redirectFromLegacyUrl: function(params) {
		if (typeof window.location.origin === 'undefined') {
			// origin is a read only property so this only happens in IE which does not have an origin property.
			// Since IE doesn't have the property we can create our own and write to it.
			window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
		}

		var result = false;

		if (params) {
			var encodedParameters = '';

			if (params.wiki) {
				// try/catch for IE not supporting window.frameElement
				try {
					// for non-IE, check "external" frameElement name
					if (window.frameElement && window.frameElement.name && window.frameElement.name === 'external') {
						// don't replace window location if frameElement name is external, e.g. Doc.Wiki Help in external popup
					} else {
						this.gotoWinLocationParamsWiki(params);
					}
				} catch (err) {
					// for IE, ignore "external" frameElement name checking
					this.gotoWinLocationParamsWiki(params);
				}
			} else if (params.rid == 'true' || (params.sir == 'true' && params.sirId != null)) {
				this.actionAfterWorksheetLoad = function(isNewWorksheet) {
					this.doRidLookup(params, isNewWorksheet);
				};

				result = true;
			} else if (params.lookup || (typeof(history.state) != 'unknown' && history.state && history.state.lookup)) {
				this.doWikiLookup(params);
			} else if (params.url) {
				// try/catch for IE not supporting window.frameElement
				try {
					// for non-IE, check "external" frameElement name
					if (window.frameElement && window.frameElement.name && window.frameElement.name === 'external') {
						// don't replace window location if frameElement name is external, e.g. Doc.Wiki Help in external popup
					} else {
						this.gotoWinLocationParamsUrl(params);
					}
				} catch (err) {
					// for IE, ignore "external" frameElement name checking
					this.gotoWinLocationParamsUrl(params);
				}
				
			} else if (params.model) {
				var safeQueryString = RS.common.encodeForURL(this.removeParamAndEncode(params, ['model', this.CSRFTOKEN_NAME])),
					safeModel = RS.common.encodeForURL(params.model);
				var safeURL = Ext.String.format('/resolve/jsp/rsclient.jsp?{0}#RS.wiki.Main/name={1}&automationMode=0&activeTab=2', (safeQueryString ?  safeQueryString : ''), safeModel);
				window.location.replace(RS.common.decodeFromURL(safeURL));
			}
		}

		return result;
	},

	/* Banner */
	bannerIsVisible: true,
	when_banner_visibility_changes_update_state: {
		on: ['bannerIsVisibleChanged'],
		action: function() {
			//only store if the toolbar is visible, otherwise we're trying to hide the whole client from display
			if (this.toolbarIsVisible)
				Ext.state.Manager.set('bannerIsVisible', this.bannerIsVisible)
		}
	},

	toolbarIsVisible: true,

	bannerURL: '',
	bannerURLIsVisible$: function() {
		return this.bannerURL.length > 0
	},

	// bannerLogoIsLoaded: false,
	// bannerLogoIsLoaded: function() {
	// 	Ext.defer(function() {
	// 		this.set('bannerLogoIsLoaded', true)
	// 	}, 1, this)
	// },

	bannerText: '',
	bannerDisplayText$: function() {
		return Ext.String.format('<font style="font-size:28px;color:#C0C0C0">{0}</font>', this.bannerText)
	},
	bannerTextIsVisible$: function() {
		return !this.bannerURLIsVisible /*&& this.bannerLogoIsLoaded*/
	},

	displayPRB: true,

	/* Polling */
	poller: null,
	polling: false,
	pollInterval: 60000,
	pollEnabled: true,
	pollErrCount: 0,
	poll: function() {
		if (!this.polling && this.pollEnabled && !this.getLogoutFlag()) {
			this.set('polling', true);
			var user = this.get('user');
			var ssoType = user['source'] || "{}";
			this.ajax({
				url: this.API['poll'],
				params: {
					notificationsSince: Math.max(Ext.Date.format(Ext.Date.subtract(new Date(), Ext.Date.HOUR, 1), 'time'), Ext.state.Manager.get('notificationIgnore', 0)),
					USource : ssoType
				},
				success: this.updateFromPoll,
				failure: function(resp) {
					this.set('pollErrCount', this.pollErrCount +1);
					if (this.pollErrCount == 3) {
						this.displayError(this.serverErrorTxt + ' : ' + (resp.statusText || this.localize('serverCommunicationFailure')));
					}
				},
				callback: function() {
					this.set('polling', false)
				},
				scope: this
			})
		}
	},
	updateFromPoll: function(r) {
		this.set('pollErrCount', 0);
		try {
			var response = RS.common.parsePayload(r)
			if (response && response.success) {
				if (this.loginOpen) {
					this.loginDialog.doClose()
					this.set('loginOpen', false)
					this.set('authenticated', true)
				}
				if (response.data.reloadMenu) this.getUser()
			} else {
				this.set('authenticated', false)
				if(response.data && response.data['redirectUrl']){
					window.location.assign(response.data['redirectUrl'])
				} else {
					this.displayAuthenticateUserDialog()
				}
			}
		} catch (e) {
			this.displayAuthenticateUserDialog()
			this.set('authenticated', false)
		}
	},

	ONE_DAY: 1000*60*60*24,
	CURRENT_DATE: new Date().getTime(),

	/* Usage*/
	usageSummary: null,
	getUsageSummary: function(onComplete) {
		// UsagesummaryV2 is only for admin user
		if (clientVM.isAdmin) {
			if (this.currentInterval && this.currentInterval.termStart && this.currentInterval.termEnd) {
				var startDate = this.currentInterval.termStart;
				var endDate = this.currentInterval.termEnd;
				this.ajax({
					url: this.API['getUsageSummaryV2'],
					method: 'GET',
					params: {
						startDate: startDate,
						endDate: endDate
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r);
						if (response.success) {
							const chartData = [];
							const records = response.records;
							const now = Date.now();
							var totalEvents = 0;
							var totalIncidents = 0;
					
							for (var i = 0; i < records.length; i += 1) {
								const keys = Object.keys(records[i]);
								if (keys.length) {
									const timestamp = keys[0];
									const eDate = new Date(timestamp).getTime();
									if (eDate <= now) {
										const events = records[i][timestamp].All.Event || 0;
										const incidents = records[i][timestamp].All.Incident || 0;
						
										totalEvents += events;
										totalIncidents += incidents;
						
										chartData.push({
											rawdate: eDate,
											events: events,
											incidents: incidents
										});
									}
								}  
							}
	
							const Duration = Math.round((endDate - startDate)/this.ONE_DAY);
							const chartRecord = {
								start: startDate,
								dnd: endDate,
								duration: Duration, 
							};
					
							var usageSummaryData = {
								//data: chartData,
								record: chartRecord,
								events: totalEvents,
								incidents: totalIncidents,
							}
							this.set('usageSummary', usageSummaryData);
						} else {
							this.displayError(response.message);
						}
					},
					failure: function(resp) {
						this.displayFailure(resp);
					},
					callback: function() {
						if (Ext.isFunction(onComplete)) {
							onComplete();
						}
					}
				})
			} else {
				// if no intervals just call onComplete
				if (Ext.isFunction(onComplete)) {
					onComplete();
				}
			}
		}
	},

	quotaOverageData: null,
	calculateQuotaOverageData: function() {
		var eventsDailyUsage = 0;
		var eventsOverageDate = 0;
		var incidentsDailyUsage = 0;
		var incidentsOverageDate = 0;
		var daysLeftBeforeOverage = -1;  

		const termStart = this.currentInterval.termStart;
		const termEnd = this.currentInterval.termEnd;
		const eventQuota = this.currentInterval.eventQuota;
		const incidentQuota = this.currentInterval.incidentQuota;

		if (termStart && termEnd) {
			const usageDay = Math.round((this.CURRENT_DATE - termStart)/this.ONE_DAY);
			const termDuration = Math.round((termEnd - termStart)/this.ONE_DAY)
			const termDaysLeft = termDuration - usageDay;
			const events = this.usageSummary.events;
			const incidents = this.usageSummary.incidents;

			var daysLeftBeforeEventOverage = -1;
			var daysLeftBeforeIncidentOverage = -1;
		  
			if (eventQuota > 0 && events && usageDay && termDaysLeft >= 0) {
				eventsDailyUsage = Math.round((events / usageDay) * 10) / 10; // 1 decimal place
				const estimatedEventsRemaining = eventQuota - events - (Math.round(eventsDailyUsage * termDaysLeft));
				if (estimatedEventsRemaining < 0) {
					const eventsEntitlementRemaining = eventQuota - events;
					if (eventsEntitlementRemaining > 0) {
						if (eventsDailyUsage) {
							daysLeftBeforeEventOverage = Math.round(eventsEntitlementRemaining / eventsDailyUsage);  
						} else {
							daysLeftBeforeEventOverage = 0;
						}
						eventsOverageDate = this.CURRENT_DATE + (daysLeftBeforeEventOverage * this.ONE_DAY);
					} else {
						eventsOverageDate = this.CURRENT_DATE - this.ONE_DAY;
					}
				}
			}
		  
			if (incidentQuota > 0 && incidents && usageDay && termDaysLeft >= 0) {
				incidentsDailyUsage = Math.round((incidents / usageDay) * 10) / 10; // 1 decimal place
				const estimatedIncidentsRemaining = incidentQuota - incidents - (Math.round(incidentsDailyUsage * termDaysLeft));
				if (estimatedIncidentsRemaining < 0) {
					const incidentsEntitlementRemaining = incidentQuota - incidents;
					if (incidentsEntitlementRemaining > 0) {
						if (incidentsDailyUsage) {
							daysLeftBeforeIncidentOverage = Math.round(incidentsEntitlementRemaining / incidentsDailyUsage);  
						} else {
							daysLeftBeforeIncidentOverage = 0;
						}
						incidentsOverageDate = this.CURRENT_DATE + (daysLeftBeforeIncidentOverage * this.ONE_DAY);
					} else {
						incidentsOverageDate = this.CURRENT_DATE - this.ONE_DAY;
					}
				}
			}
		  
			if (daysLeftBeforeEventOverage >= 0 && daysLeftBeforeEventOverage <= daysLeftBeforeIncidentOverage) {
				daysLeftBeforeOverage = daysLeftBeforeEventOverage;
			} else {
				daysLeftBeforeOverage = daysLeftBeforeIncidentOverage;
			}
		}

		this.set('quotaOverageData', {
			eventsDailyUsage: eventsDailyUsage,
			eventsOverageDate: eventsOverageDate,
			incidentsDailyUsage: incidentsDailyUsage,
			incidentsOverageDate: incidentsOverageDate,
			daysLeftBeforeOverage: daysLeftBeforeOverage
		});
	},

	/* License */
	license: null,
	licenseMsg: '',
	licenseMsgCls: '',
	licenseDaysLeft: 0,
	displayLicenseMsg: false,
	checkLicenseBanner: function() {
		// License expiring message is only for admin user
		if (clientVM.isAdmin) {
			this.ajax({
				url: this.API['getLicenseSummary'],
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) {
						if (response.data) {
							this.set('license', response.data);
							this.calculateCurrentInterval();
							this.getUsageSummary(function() {
								this.calculateQuotaOverageData();
								this.processLicenseBannerDisplay();
							}.bind(this));
						}
					} else {
						this.displayError(response.message);
					}
				},
				failure: function(resp) {
					this.displayFailure(resp);
				}
			})
		}
	},

	licenseMsgHidden$: function() {
		var isRsFrame = false;
		// try/catch for IE not supporting window.frameElement
		try {
			isRsFrame = (window.frameElement && window.frameElement.getAttribute('class') == 'rs_iframe');
		} catch (err) {
			isRsFrame = false;
		}
		return !this.displayLicenseMsg || isRsFrame;
	},

	dismissLicenseMsgHidden$: function() {
		const daysLeftBeforeOverage = this.quotaOverageData && this.quotaOverageData.daysLeftBeforeOverage;
		const usageOverageUnder30days = daysLeftBeforeOverage && daysLeftBeforeOverage >= 0 && daysLeftBeforeOverage <= 30;
		return this.licenseDaysLeft <= 30 || usageOverageUnder30days || this.generateLicenseMessageCls != '';
	},

	contactUs: function() {
		this.open({
			mtype: 'RS.client.ContactUs',
		})
	},

	dismissLicenseMsg: function() {
		const daysLeftBeforeOverage = clientVM.quotaOverageData.daysLeftBeforeOverage;
		var dismissed = this.licenseDaysLeft;
		if (daysLeftBeforeOverage !== -1 && daysLeftBeforeOverage < dismissed) {
		  dismissed = daysLeftBeforeOverage;
		}
		localStorage.setItem('expiration-banner', dismissed);
		this.checkLicenseBanner();
	},

	updateLicenseBanner: function() {
		// when add/delete license, clear expiration-banner and checkLicenseBanner()
		localStorage.removeItem('expiration-banner');
		setTimeout(function(){
			clientVM.checkLicenseBanner();
		}, 200);
		// also set localStorage flag so other tabs can checkLicenseBanner()
		localStorage.setItem('update-license', true);
		setTimeout(function(){
			localStorage.removeItem('update-license');
		}, 5000);
	},

	currentInterval: null,
	calculateCurrentInterval: function() {
		var termStart = 0;
		var termEnd = 0;
		var eventQuota = 0;
		var incidentQuota = 0;
		var intervals = this.license.intervals;
		
		if (intervals) {
			for (var i = 0; i < intervals.length; i += 1) {
				if (this.CURRENT_DATE >= intervals[i].startDate && this.CURRENT_DATE <= intervals[i].endDate) {
					termStart = intervals[i].startDate;
					termEnd = intervals[i].endDate
					eventQuota = intervals[i].eventCount;
					incidentQuota = intervals[i].incidentCount;
					break;
				}
			}
		}

		this.set('currentInterval', {
			termStart: termStart,
			termEnd: termEnd,
			eventQuota: eventQuota,
			incidentQuota: incidentQuota
		});
	},

	displayBanner: function(daysLeft, dismissed, daysLeftBeforeOverage) {
		// License expiring message is only for admin user
		if (clientVM.isAdmin) {
			// daysLeftBeforeOverage = -1 means not applicable
			const usageOverageUnder30days = daysLeftBeforeOverage >= 0 && daysLeftBeforeOverage <= 30;
			const usageOverageUnder60days = daysLeftBeforeOverage >= 0 && daysLeftBeforeOverage <= 60;
			const noUsageOverage = daysLeftBeforeOverage === -1 || daysLeftBeforeOverage > 90;
			
			// if over 90 days, don't display banner
			if (daysLeft > 90 && noUsageOverage) {
				return false;
			}
	
			// if under 60 days but was dismissed over 60, then display banner
			else if ((daysLeft <= 60 || usageOverageUnder60days) && (dismissed && parseInt(dismissed) > 60)) {
				return true;
			}
	
			// if under 30 days always display banner
			else if (this.license.expired || daysLeft <= 30 || usageOverageUnder30days) {
				return true;
			}
	
			return dismissed === null;
		}
		return false;
	},

	generateLicenseMessageCls: '',
	generateLicenseMessage: function(daysLeft, expired, eventsOverageDate, incidentsOverageDate) {
		var licenseMsg = '';

		if (expired || daysLeft <= 0) {
			licenseMsg += this.localize('licenseExpired');
		} else if (daysLeft === 1) {
			licenseMsg += this.localize('licenseExpiredInOneDay');
		} else if (daysLeft <= 90) {
			licenseMsg += this.localize('licenseExpiredInDays', daysLeft);
		}

		if ((eventsOverageDate && eventsOverageDate <= this.CURRENT_DATE) || (incidentsOverageDate && incidentsOverageDate <= this.CURRENT_DATE)) {
			licenseMsg += this.localize('entitlementExceeded');
			this.set('generateLicenseMessageCls', 'license-critical');
		} else if (eventsOverageDate && incidentsOverageDate && eventsOverageDate < incidentsOverageDate) {
			licenseMsg += this.localize('entitlementWillExceedOn', Ext.Date.format(new Date(eventsOverageDate), 'M j, Y'));
		} else if (incidentsOverageDate) {
			licenseMsg += this.localize('entitlementWillExceedOn', Ext.Date.format(new Date(incidentsOverageDate), 'M j, Y'));
		} else if (eventsOverageDate) {
			licenseMsg += this.localize('entitlementWillExceedOn', Ext.Date.format(new Date(eventsOverageDate), 'M j, Y'));
		}

		return licenseMsg.trim();
	},

	processLicenseBannerDisplay: function() {
		const dismissed = localStorage.getItem('expiration-banner');
		const daysLeft = this.license.daysLeft;
		const expired = this.license.expired;
		const eventsOverageDate = clientVM.quotaOverageData.eventsOverageDate;
		const incidentsOverageDate = clientVM.quotaOverageData.incidentsOverageDate;
		const daysLeftBeforeOverage = clientVM.quotaOverageData.daysLeftBeforeOverage;

		if (this.displayBanner(daysLeft, dismissed, daysLeftBeforeOverage)) {
			this.set('generateLicenseMessageCls', '');
			var licenseMsg = this.generateLicenseMessage(daysLeft, expired, eventsOverageDate, incidentsOverageDate);
			var licenseMsgCls = 'licenseMsg';

			if (daysLeft <= 30 || (daysLeftBeforeOverage !== -1 && daysLeftBeforeOverage <= 30) || this.generateLicenseMessageCls) {
				licenseMsgCls += ' license-critical';
			} else {
				licenseMsgCls += ' license-warning';
			}

			this.set('licenseMsg', licenseMsg);
			this.set('licenseMsgCls', licenseMsgCls);
			this.set('licenseDaysLeft', daysLeft);
			this.set('displayLicenseMsg', true);
		} else {
			this.set('displayLicenseMsg', false);
		}
	},

	/* User */
	username: '',
	user: null,
	when_user_changes_refresh_user_components: {
		on: ['userChanged'],
		action: function() {
			this.updateUser()
		}
	},
	userDefaultDateFormat: 'Y-m-d G:i:s',
	getUserDefaultDateFormat: function() {
		return this.userDefaultDateFormat;
	},
	when_defaultDateFormatChanged_persist_in_local_storage: {
		on: ['userDefaultDateFormatChanged'],
		action: function() {
			Ext.state.Manager.set('userDefaultDateFormat', this.userDefaultDateFormat)
		}
	},
	organization: null,
	isRootUser$: function() {
		return this.organization == null
	},

	checkUser: function(username) {
		if (!this.user || this.user.name != username) {
			this.screens.removeAll()
			this.getUser(true)
		}
	},

	getUser: function() {
		this.ajax({
			url: this.API['getUser'],
			params: {
				notificationsSince: Math.max(Ext.Date.format(Ext.Date.subtract(new Date(), Ext.Date.HOUR, 1), 'time'), Ext.state.Manager.get('notificationIgnore', 0))
			},
			scope: this,
			success: this.loadUser,
			failure: function(resp) {
				this.displayFailure(resp);
			}
		})
	},

	loadUser: function(r) {
		//This will allow everything needed to display loaded properly before rendering.
		setTimeout(function(){
			this.set('visibility','visible');
		}.bind(this),500);
		var response = RS.common.parsePayload(r)
		if (response.success) {
			if (response.data.upasswordNeedsReset){
				this.logout();
			}
			else {
				if (response.data) this.set('user', response.data)
				//Start the poller to get updates on the client
				if (!this.runner) this.runner = new Ext.util.TaskRunner()
				if (!this.poller) this.poller = this.runner.newTask({
					run: this.poll,
					scope: this,
					interval: this.pollInterval
				})
				this.poller.start()

				//Render Navigation Menu
				this.generateNavigationMenu();

				// resolve.maint is an admin user, too
				if (response.data.name == 'resolve.maint' || (response.data.roles && Ext.Array.indexOf(response.data.roles, 'admin') > -1)) {
					this.set('isAdmin', true);
				}

				//Retrieve license info and check for expiration
				this.checkLicenseBanner();
			}
		} else this.displayError(response.message)
	},

	waitClientVMFetchProblemIdFromUrl : false,
	userSettings : {},
	updateUser: function() {
		window.user = this.user;
		this.windowParams.problemId = this.windowParams.PROBLEMID || this.windowParams.problemId;
		this.windowParams.reference = this.windowParams.REFERENCE || this.windowParams.reference;
		this.windowParams.alertId = this.windowParams.ALERTID || this.windowParams.alertId ||  this.windowParams.alertid;
		this.windowParams.correlationId = this.windowParams.CORRELATIONID || this.windowParams.correlationId || this.windowParams.correlationid;
		this.windowParams.autoCreate = this.windowParams.AUTOCREATE || this.windowParams.autoCreate;
		
		if (this.user.problemId) {
			this.updateProblemInfo(this.user.problemId, this.user.problemNumber)
		}		

		var hasID = this.windowParams.problemId || this.windowParams.reference || this.windowParams.correlationId || this.windowParams.alertId;

		if (this.windowParams && hasID) {
			//Set this to true to wait for new ProblemId before processing it.
			this.set('waitClientVMFetchProblemIdFromUrl', true);
			var problemID = this.windowParams.problemId;
			var problemIDStatus = null;

			if(problemID) {
				problemIDStatus = problemID.toLowerCase();
			}

			if (problemIDStatus === 'new') {
				setTimeout(this.startNewWorksheet.bind(this),0);
			} else if (problemIDStatus === 'active') {
				//do nothing because we'll automatically select the active from the backend
				this.set('waitClientVMFetchProblemIdFromUrl', false);
				this.actionAfterWorksheetLoad();
			} else {
				this.model({
					mtype: 'RS.worksheet.WorksheetPicker',
					clientDialog: false,
					problemId: problemID,
					reference: this.windowParams.reference,
					correlationId: this.windowParams.correlationId,
					alertId: this.windowParams.alertId,
					autoCreate: this.windowParams.autoCreate,
					afterWorksheetSelected: function (isNewWorksheet) {
						this.actionAfterWorksheetLoad(isNewWorksheet);
					}.bind(this)
				});
			}
		} else {
			this.actionAfterWorksheetLoad();
		}

		if (this.windowParams && this.windowParams.catalog) {
			this.loadNodeInCatalog(this.windowParams.catalog);
		} else if (this.windowParams.url) {
			this.navigateToLocation(this.windowParams.url);
		} else if (this.restoreScreen) {
			this.goToScreen(this.restoreScreen);
			this.restoreScreen = null;
		}

		if(this.windowParams) {
			if (this.windowParams.addDocument === 'true' || this.windowParams.documentName) {
				this.open({
					mtype: 'RS.wiki.AddDocument',
					name: this.windowParams.documentName || ''
				})
			}
		}

		//Determine banner and update accordingly
		var user = this.user;

		if (user.name) {
			this.set('username', user.name);
		}

		if (user.bannerTitle) {
			this.set('bannerText', user.bannerTitle);
		}

		if (user.bannerURL) {
			if (user.bannerURL.indexOf('/') == -1) {
				user.bannerURL = '//' + user.bannerURL;
			}
			if (user.bannerURL.indexOf('/resolve/') === 0 && user.bannerURL.indexOf(this.CSRFTOKEN_NAME) === -1) {
				this.getCSRFToken_ForURI(user.bannerURL, function(data, uri) {
					var csrftoken = '?' + data[0] + '=' + data[1];
					var tokenizedData = uri + csrftoken;
					user.bannerURL = tokenizedData;
					this.set('bannerURL', Ext.String.format('<iframe class="bannerURL rs_iframe" src="{0}" style="height:100%;width:100%;border:0px"></iframe>', user.bannerURL));
				}.bind(this)); 
			} else {
				this.set('bannerURL', Ext.String.format('<iframe class="bannerURL rs_iframe" src="{0}" style="height:100%;width:100%;border:0px"></iframe>', user.bannerURL));
			}
		}

		if (user.defaultDateFormat) {
			this.set('userDefaultDateFormat', user.defaultDateFormat);
		}

		if (user.organization) {
			this.set('organization', user.organization);
		}

		this.set('displayPRB', user.displayPRB);

		//If we already have a screen, its because we restored it from the history token and don't need to load an initial screen
		if (this.screens.length == 0 && !this.redirectStarted) {
			//Start initial screen for user
			this.goToUserHome();
		}

		//Update user's setting
		try {
			this.set('userSettings', user.userSettings ? JSON.parse(user.userSettings) : {});
			this.processUserSetting(this.userSettings);
		}catch (e) {
			clientVM.displayError(this.localize('invalidUserSettings', this.username));
		}

		//Update user's org
		this.set('userOrganization', user.orgs || []);

		//Determine toolbar and update accordingly
		if (user.toolbar === 'default') {
			user.toolbar = this.getDefaultToolbar();
		}

		if (user.toolbar && Ext.isString(user.toolbar)) {
			user.toolbar = this.parseToolbarConfiguration(user.toolbar);
		}

		this.buildUserToolbar();

		//If user has a menu, then update it
		if (user.menu) {
			var menu = user.menu;
			//order the menu sets
			menu.sets.sort(function(a, b) {
				if (a.sequence < b.sequence) {
					return -1;
				}

				if (a.sequence > b.sequence) {
					return 1;
				}

				return 0;
			})
			this.set('menuSets', menu.sets);

			if (menu.sections) {
				menu.sections.sort(function(a, b) {
					if (a.order < b.order) {
						return -1;
					}

					if (a.order > b.order) {
						return 1;
					}

					return 0;
				})
				this.set('menuSections', menu.sections);
			}

			this.sideMenu.updateMenu(menu);
		}

		//
	},
	processUserSetting : function(setting){
		Ext.state.Manager.set('userDefaultDateFormat', setting['userDefaultDateFormat']);
		Ext.state.Manager.set('autoRefreshEnabled', setting['autoRefreshEnabled']);
		Ext.state.Manager.set('autoRefreshInterval', setting['autoRefreshInterval']);
		Ext.state.Manager.set('aceKeybinding', setting['aceKeybinding']);
		Ext.state.Manager.set('maxRecentlyUsed', setting['maxRecentlyUsed']);
		Ext.state.Manager.set('bannerIsVisible', setting['bannerIsVisible']);
	},
	startNewWorksheet: function () {
		this.ajax({
			url: this.API['newWS'],
			method: 'POST',
			scope: this,
			params: {
				reference: this.windowParams.reference,
				alertId: this.windowParams.alertId,
				correlationId: this.windowParams.correlationId,
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			},
			success: function(r) {
				var response = RS.common.parsePayload(r);

				if (response.success) {
					if (response.data) {
						this.updateProblemInfo(response.data);
					}

					this.displaySuccess(this.localize('newWorksheetCreated', response.data));
					this.fireEvent('problemIdFetchedFromUrl');
					this.actionAfterWorksheetLoad(true);
				} else {
					this.displayError(response.message);
				}
			},
			failure: function(resp) {
				this.displayFailure(resp);
			}
		});
	},

	loadNodeInCatalog: function (catalog) {
		this.ajax({
			url: this.API['getCatalogNames'],
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);

				if (response.success) {
					var catalogName = catalog.substring(catalog.lastIndexOf('/') + 1),
						catalogId = null,
						catalogType = null,
						records = response.records;

					for(var i = 0; i < records.length; i++) {
						var record = records[i];

						if(record.name === catalogName) {
							catalogId = record.id;
							catalogType = record.catalogType;
						}
					}

					if (catalogId) {
						this.handleNavigation({
							modelName: catalogType === 'training' ? 'RS.catalog.TrainingViewer' : 'RS.catalog.CatalogViewer',
							params: {
								id: catalogId,
								activeNode: catalog
							}
						});
					}
				} else {
					this.displayError(response.message);
				}
			},
			failure: function(r) {
				this.displayFailure(r);
			}
		})
	},

	navigateToLocation: function (url) {
		this.handleNavigation({
			modelName: 'RS.client.URLScreen',
			params: { location: url }
		});
	},

	buildUserToolbar: function() {
		//remove all toolbar items if there were any previously
		this.lefttoolbarItems.removeAll();
		this.middletoolbarItems.removeAll();
		this.righttoolbarItems.removeAll();

		// Add the toolbar left logo
		if(this.toolbarLogo !== '') {
			this.lefttoolbarItems.add({
				xtype: 'image',
				src: this.toolbarLogo,
				height: '@{..toolbarLogoHeight}',
				width: '@{..toolbarLogoWidth}',
				hidden: this.toolbarLogoHidden,
				padding: '0'
			});
		}

		if (this.user.toolbar && Ext.isArray(this.user.toolbar)) {
			//Add each item to the toolbar based on the user configuration
			var menuItemFound = false;
			var sideMenuDisplay =  clientVM.userSettings.sideMenuDisplay == true;
			Ext.each(this.user.toolbar, function(item) {

				// GVo Note RBA-16204
				// buttons on the toolbar will become menuitem if they don't fit the browser's width
				// and in such cases, the menuitem texts are not encoded. This exploit XSS attack. The
				// best way to fix this is to turn off 'htmlEncode', by default overridden to true, and
				// to encode the text before passing the the button configuration.
				if (item.text) {
					item.text = Ext.htmlEncode(item.text);
				}
				var temp = Ext.applyIf(item, {
					xtype: 'button',
					htmlEncode: false // to prevent double encoding
				})
				// End GVo Note

				if (!temp.menu) {
					Ext.apply(temp, {
						scope: this,
						handler: this.handleNavigationClick,
						componentCls: 'toolbar-nav-menu-item',
						listeners: {
							scope: this,
							render: this.handleMenuButtonRender
						}
					});
				} else {
					Ext.Array.forEach(temp.menu.items, function(menuItem) {
						Ext.apply(menuItem, {
							componentCls: 'toolbar-nav-menu-item',
							scope: this,
							handler: this.handleNavigationClick
						})
					}, this)
					temp.menu.closeHandler = Ext.emptyFn,
					temp.componentCls = 'toolbar-nav-menu-item',
					temp.listeners = {
						scope: this,
						render: this.handleMenuButtonRender
					}
				}
				if (!menuItemFound && (temp.modelName == 'RS.client.Menu' || temp.query == 'RS.client.Menu')) {
					if(!sideMenuDisplay) {
						/* Don't render 'RS.client.Menu' on lefttoolbarItems since we will have navigation menu; render on middletoolbarItems
						this.lefttoolbarItems.add(temp);
						*/
						this.middletoolbarItems.add(temp);
					}
					menuItemFound = true;
				} else {
					this.middletoolbarItems.add(temp);
				}
			}, this)
		}

		this.righttoolbarItems.add({xtype: 'tbfill'});

		//Add organization for this user
		this.righttoolbarItems = this.addOrganizationMenu(this.righttoolbarItems);

		//Add in the user dropdown menu
		this.righttoolbarItems.add({
			xtype: 'button',
			text: this.user.name,
			componentCls: 'right-toolbar-item',
			handler: this.showUserInfo,
			scope: this
		})

		/*
		//Add in the social notification icon
		this.righttoolbarItems.add({
			xtype: 'button',
			itemId: 'notificationFlag',
			text: ' ',
			iconCls: this.user.notificationUnreadCount > 0 ? 'icon-large icon-envelope social-attention' : 'icon-large icon-ok rs-client-button',
			tooltip: this.user.notificationUnreadCount > 0 ? (this.user.notificationUnreadCount == 1 ? this.localize('notificationMessage', this.user.notificationUnreadCount) : this.localize('notificationMessages', this.user.notificationUnreadCount)) : this.localize('notificationNoneMessage'),
			scope: this,
			handler: this.handleNotificationClick
		})
		*/

		//Global search
		this.righttoolbarItems.add({
			xtype: 'combobox',
			width: 225,
			hideLabel: true,
			autoSelect: false,
			name: '..searchText',
			componentCls: 'right-toolbar-item',
			emptyText: this.localize('searchText'),
			store: '@{..searchStore}',
			queryMode: 'local',
			hideTrigger: true,
			displayField: 'display',
			autoSelect: false,
			valueField: 'id',
			listeners: {
				specialkey: '@{..searchSpecialKey}',
				afterrender: function(field) {
					field.focus();
				}
			}
		})

		this.righttoolbarItems.add({
			xtype: 'button',
			iconCls: 'icon-larger icon-search rs-client-button',
			componentCls: 'right-toolbar-item',
			tooltip: this.localize('searchText'),
			overCls: 'tool-over',
			scope: this,
			handler: function() {
				this.performSearch()
			}
		})
		this.righttoolbarItems.add({
			xtype: 'button',
			iconCls: 'icon-larger icon-play-sign rs-client-button',
			componentCls: 'right-toolbar-item',
			tooltip: this.localize('goText'),
			overCls: 'tool-over',
			scope: this,
			handler: function() {
				this.goToSearch(Ext.EventObject.ctrlKey || Ext.EventObject.shiftKey)
			}
		})

		//Add in the prb information
		this.righttoolbarItems.add({
			xtype: 'button',
			iconCls: 'icon-larger icon-th-list rs-client-button ' + (Ext.isGecko ? 'rs-icon-firefox' : ''),
			componentCls: 'right-toolbar-item',
			hidden: '@{!..displayPRB}',
			scope: this,
			handler: this.selectPRB,
			tooltip: '@{..prbTooltip}',
			listeners: {
				scope: this,
				afterrender: function(btn) {
					btn.getEl().on('dblclick', function() {
						this.goToActiveWorksheet()
					}, this)
				}
			}
		});

		// Add the toolbar right logo
		if(this.toolbarRightLogo !== '') {
			this.righttoolbarItems.add({
				xtype: 'image',
				src: this.toolbarRightLogo,
				height: '@{..toolbarRightLogoHeight}',
				width: '@{..toolbarRightLogoWidth}',
				hidden: this.toolbarRightLogoHidden
			});
		}
	},
	addOrganizationMenu : function(toolbar){
		var orgList = clientVM.userOrganization;
		var orgLabel = clientVM.orgLabel;
		var persistOrgInfo = Ext.state.Manager.get('orgInfo', false);
		var persistOrgInfoStillAlive = false;
		if(clientVM.windowParams.org)
			var selectedOrg = this.getOrgInfo(clientVM.windowParams.org);
		if (persistOrgInfo) {
			var persistOrgName = persistOrgInfo.split(':')[1];
			if (persistOrgName.toLowerCase() != 'none') {
				// Making sure that selected org has not been deleted
				persistOrgInfoStillAlive = this.getOrgInfo(persistOrgName);
			} else {
				persistOrgInfoStillAlive = true;
			}
		}
		if(orgList.length > 0){
			var orgItems = [];
			var defaultOrgName = null;
			for(var i = 0; i < orgList.length; i++){
				var name = Ext.String.htmlEncode(orgList[i].uname);
				orgItems.push({
					text : name,
					orgId : orgList[i].id,
					orgName : name,
					componentCls: 'toolbar-nav-menu-item',
				});
				if(!selectedOrg && !persistOrgInfo && orgList[i].isDefaultOrg){
					defaultOrgName = name;
					clientVM.set('orgId', orgList[i].id);
					clientVM.set('orgName', name);

					//Switch active worksheet for default Org
					this.updateActiveWSForOrg(orgList[i].id);
				}
			}
			if (selectedOrg){
				defaultOrgName = selectedOrg.uname;
				clientVM.set('orgId', selectedOrg.id);
				clientVM.set('orgName', selectedOrg.uname);

				//Switch active worksheet for default Org
				this.updateActiveWSForOrg(selectedOrg.id);
				// Persist selected org to ensure all tabs having the same org
				Ext.state.Manager.set('orgInfo', clientVM.orgId + ':' + clientVM.orgName);
			} else if (persistOrgInfo && persistOrgInfoStillAlive) { // Making sure that selected org has been deleted
				var idName = persistOrgInfo.split(':');
				var persistOrgName = idName[1];
				var persistOrgId =  (persistOrgName.toLowerCase() == 'none')? 'nil': idName[0];
				defaultOrgName = persistOrgName;
				clientVM.set('orgId', persistOrgId);
				clientVM.set('orgName', persistOrgName);

				//Switch active worksheet for default Org
				this.updateActiveWSForOrg(persistOrgId);
			}
			//Pick the first item as default.
			if(!defaultOrgName){
				defaultOrgName = orgItems[0].text;
				clientVM.set('orgId', orgList[0].id);
				clientVM.set('orgName', defaultOrgName);

				//Switch active worksheet for default Org
				this.updateActiveWSForOrg(orgList[0].id);
			}
			if(orgLabel){
				toolbar.add({
					xtype : 'component',
					cls : 'org-wrapper-bracket',
					html : orgLabel + ': ',
				})
			}
			toolbar.add({
				xtype : 'button',
				htmlEncode: false,
				cls : 'org-display-name',
				componentCls: 'right-toolbar-item',
				padding : 0,
				marginLeft : 0,
				itemId: 'orgSelection',
				text: defaultOrgName,
				menu: {
					plain : true,
					xtype : 'menu',
					componentCls: 'orgSelection toolbar-nav-menu',
					minWidth : 200,
					closeHandler : Ext.emptyFn,
					defaults : {
						handler : this.orgChangeHandler.bind(this)
					},
					items: orgItems
				}
			})
		}
		return toolbar;
	},
	orgChangeHandler :function(comboBoxBtn){
		this.message({
			title : this.localize('orgChangeTitle',clientVM.orgLabel),
			msg : this.localize('orgChangeMsg', [clientVM.orgLabel, comboBoxBtn.orgName]),
			buttons: Ext.MessageBox.OK,
			buttonText: {
				ok: this.localize('ok'),
				cancel : this.localize('cancel')
			},
			scope: this,
			fn: function(btn){
				if(btn == 'ok'){
					clientVM.set('orgId', comboBoxBtn.orgId || 'nil');
					clientVM.set('orgName', comboBoxBtn.orgName);
					Ext.state.Manager.set('orgInfo', clientVM.orgId + ':' + clientVM.orgName);
					var b = comboBoxBtn.up('#orgSelection');
					b.setText(comboBoxBtn.text);
					this.updateActiveWSForOrg(comboBoxBtn.orgId);
					this.fireEvent('orgchange');
				}
			}
		})
	},
	getOrgInfo : function(org){
		for(var i = 0; i < (clientVM.userOrganization || []).length; i++){
			var currentOrg = clientVM.userOrganization[i];
			if(currentOrg.uname.toLowerCase() == org.toLowerCase() || currentOrg.id == org)
				return currentOrg;
		}
		return null;
	},
	updateActiveWSForOrg : function(orgId){
		this.ajax({
			url : this.API['getActiveWS'],
			method : 'GET',
			params : {
				'RESOLVE.ORG_ID' : orgId
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					this.displayError(respData.message);
					return;
				}
				if(respData.data){
					this.updateProblemInfo(respData.data.id, respData.data.number);				
				}
			},
			failure: function(resp) {
				this.displayFailure(resp);
			}
		})
	},
	updateUserToolbar: function() {
		var index = -1,
			item = null;

		this.toolbarItems.foreach(function(itm, idx) {
			if (itm.itemId == 'notificationFlag') {
				index = idx
			}
		})

		if (index > -1) {
			item = this.toolbarItems.removeAt(index)
			if (this.user.notificationUnreadCount > 0) {
				item.tooltip = this.user.notificationUnreadCount == 1 ? this.localize('notificationMessage', this.user.notificationUnreadCount) : this.localize('notificationMessages', this.user.notificationUnreadCount)
				item.iconCls = 'icon-large icon-envelope social-attention'
			} else {
				item.tooltip = this.localize('notificationNoneMessage')
				item.iconCls = 'icon-large icon-ok rs-client-button'
			}
			this.toolbarItems.insert(index, item)
		}
	},

	notificationWin: null,
	handleNotificationClick: function(button) {
		if (this.notificationWin) {
			this.notificationWin.doClose()
			this.notificationWin = null
		} else {
			this.notificationWin = this.open({
				mtype: 'RS.client.Notifications',
				target: button.getEl().id
			})
			this.notificationWin.on('closed', function() {
				this.notificationWin = null
			}, this)
		}
	},

	goToMain: function() {
		var config = {
			modelName: 'RS.client.Menu'
		}
		var screenToken = Ext.ux.Router.routes[0].routeMatcher.stringify(config);
		var screenConfig = Ext.ux.Router.routes[0].routeMatcher.parse(screenToken);
		this.goToScreen(screenConfig);
	},

	goToUserHome: function() {
		var config = this.getUserHomeScreenConfiguration(),
			screenToken = Ext.ux.Router.routes[0].routeMatcher.stringify(config),
			screenConfig = Ext.ux.Router.routes[0].routeMatcher.parse(screenToken);
		this.goToScreen(screenConfig)
	},

	getUserHomeScreenConfiguration: function() {
		var config = {}
		switch (this.user.startPage) {
			case 'menu':
				config = {
					modelName: 'RS.client.Menu'
				}
				break;
			case 'sirDashboard':
				config = {
					modelName: 'RS.incident.Dashboard'
				}
				break;
			case 'wiki':
				config = {
					modelName: 'RS.wiki.Main',
					params: Ext.Object.toQueryString({
						name: this.user.homePage
					})
				}
				break;
			case 'social':
			default:
				config = {
					modelName: 'RS.social.Main'
				}
				break;
		}
		if (this.user.name == 'resolve.maint')
			config = {
				modelName: 'RS.client.Menu'
			};
		return config;
	},

	userInfoWin: null,
	showUserInfo: function(button) {
		if (this.userInfoWin) {
			this.userInfoWin.doClose()
			this.userInfoWin = null
		} else {
			this.userInfoWin = this.open({
				mtype: 'RS.client.UserInfo',
				target: button.getEl().id
			})
			this.userInfoWin.on('closed', function() {
				this.userInfoWin = null
			}, this)
		}
	},

	prbTooltip$: function() {
		return this.problemNumber ? this.localize('activeWorksheetText') + ': ' + this.problemNumber : this.localize('worksheetsText')
	},

	prbWin: null,
	selectPRB: function(button, clientDialog) {
		if (!this.prbWin) {
			this.prbWin = this.open({
				mtype: 'RS.worksheet.WorksheetPicker',
				clientDialog: true,
				target: button,
				clientDialog: clientDialog !== false
			})
		} else
			this.prbWin.set('showPicker', !this.prbWin.showPicker)
	},
	closePrb: function() {
		if (this.prbWin) {
			this.prbWin.set('showPicker', false)
		}
	},
	goToActiveWorksheet: function() {
		if (this.user && this.user.roles && (Ext.Array.indexOf(this.user.roles, 'resolve_dev') > -1 || Ext.Array.indexOf(this.user.roles, 'admin') > -1 || this.user.name == 'resolve.maint')) {
			if (this.fromArchive)
				this.handleNavigation({
					modelName: 'RS.worksheet.ArchivedWorksheet',
					params: {
						id: this.archivedProblemId,
						activeTab: 1,
						setActive: true
					},
					target: '_blank'
				})
			else
				this.handleNavigation({
					modelName: 'RS.worksheet.Worksheet',
					params: {
						id: this.problemId || 'ACTIVE',
						activeTab: 1
					},
					target: '_blank'
				})
			this.closePrb()
		}
	},

	authenticated: true,
	checkAuthentication: function() {
		if (!this.authenticated)
			this.displayAuthenticateUserDialog();
	},
	loginOpen: false,
	loginDialog: null,
	displayAuthenticateUserDialog: function() {
		//
		// Currently, on a session timeout, we suppress the login popup and force a reload of the current page so that it will go to
		// ADFS login. This could potentially cause loosing unsaved data (i.e. action tasks) for all tabs having the same session.
		// To prevent loosing unsaved data on session timeout and re-login with ADFS, we can enhance the feature by launching ADFS 
		// re-login screen with:
		// 1. A new tab
		// 2. A popup dialog
		// After re-login in from 1 OR 2, other tabs will be notified and renew with the new session so that the views on those tabs
		// are intact and no unsaved data will be lost.
		//
		if (clientVM.adfsEnabled === 'true' || clientVM.adfsEnabled === true) {
			window.location.reload();
		} else if (!this.loginOpen) {
			this.set('loginOpen', true)
			this.loginDialog = this.open({
				mtype: 'Login'
			})
		}
	},

	when_authenticated_changes_update_child_windows: {
		on: ['authenticatedChanged'],
		action: function() {
			Ext.Array.forEach(this.childWindows, function(win) {
				if (win.mainVM && Ext.isDefined(win.mainVM.authenticated)) win.mainVM.set('authenticated', !this.loginOpen)
			})
		}
	},
	childWindows: [],

	logout: function() {
		var windowLocationSearch = '';
		var queryParams = {};
		Ext.Object.each(clientVM.windowParams, function(k, v) {
			if (v) {
				if (k.toUpperCase() != clientVM.CSRFTOKEN_NAME)
					queryParams[k] = v;
			}
		});
		if (queryParams && Object.keys(queryParams).length) {
			windowLocationSearch = '?' + Ext.Object.toQueryString(queryParams);
		}
		this.setLogoutFlag();
		this.removeCSRFTokenFlag();
		if (this.poller) {
			this.poller.stop();
		}
		window['location'] = '/resolve/service/logout' + windowLocationSearch.replace(/javascript\:/g, '');
		Ext.state.Manager.clear('orgInfo');
	},

	/* Toolbar */
	lefttoolbarItems: {
		mtype: 'list'
	},

	middletoolbarItems: {
		mtype: 'list'
	},

	righttoolbarItems: {
		mtype: 'list'
	},

	handleMenuButtonRender: function(button) {
		button.getEl().on('dblclick', function() {
			if (this.navTask) this.navTask.cancel()
			if (button.modelName) {
				var safeUrl = '';
				var queryParams = {};
				Ext.Object.each(clientVM.windowParams, function(k, v) {
					if (v) {
						if (k.toUpperCase() != clientVM.CSRFTOKEN_NAME)
							queryParams[k] = RS.common.encodeForURL(v);
					}
				})
				if (clientVM.fromArchive) {
					Ext.Object.each(queryParams, function(k, v) {
						if (k.toLowerCase() == 'problemid')
							delete queryParams[k]
					})
					queryParams['PROBLEMID'] = clientVM.archivedProblemId
				}
				if (queryParams && Object.keys(queryParams).length) {
					safeUrl += RS.common.encodeForURL('?' + Ext.Object.toQueryString(queryParams));
				}
				safeUrl += RS.common.encodeForURL('#' + button.modelName + '/');
				if (button.params) {
					safeUrl += RS.common.encodeForURL('/' + Ext.Object.toQueryString(button.params));
				}
				clientVM.handleWindowOpen(safeUrl);
			}
		}, this)
	},

	getDefaultToolbar: function() {
		return [{
			text: this.localize('mainMenuText'),
			itemId: 'mainmenuToolbarButton',
			cls: '',
			modelName: 'RS.client.Menu'
		},
			{
				text: this.localize('socialText'),
				itemId: 'socialToolbarButton',
				cls: '',
				modelName: 'RS.social.Main'
			},
			{
				text: this.localize('documentText'),
				menu: {
					xtype: 'menu',
					items: [{
						text: this.localize('defaultDocumentText'),
						modelName: 'RS.wiki.Main',
						params: {
							name: 'HOMEPAGE'
						}
					}, {
						text: this.localize('addDocumentText'),
						modelName: 'RS.wiki.AddDocument',
						action: 'WINDOW'
					}, {
						text: this.localize('listDocuments'),
						modelName: 'RS.wiki.ListDocuments',
						action: 'WINDOW'
					}, {
						text: this.localize('history'),
						modelName: 'RS.wiki.History',
						action: 'WINDOW'
					}]
				}
			}
		]
	},

	parseToolbarConfiguration: function(toolbarConfig) {
		try {
			var toolbar = Ext.decode(toolbarConfig),
				toolbarItems = this.massageToolbarData(toolbar.children);
			//if (toolbarItems.length == 0) toolbarItems = this.getDefaultToolbar()
			return toolbarItems
		} catch (e) {
			return this.parseLegacyConfiguration(toolbarConfig)
		}
	},

	massageToolbarData: function(toolbarItems) {
		if (toolbarItems)
			Ext.Array.forEach(toolbarItems, function(item) {
				item.text =  item.name
				delete item.name
				item.componentCls = 'toolbar-nav-menu-item'
				item.hidden = !item.active
				item.roleHide = true
				item.handler = this.handleNavigationClick
				item.scope = this
				item.tooltip = Ext.String.htmlEncode(item.tooltip);
				switch (item.openAs) {
					case 'tab':
						item.target = '_blank'
						break;
					case 'window':
						item.action = 'WINDOW'
						break;
					default:
						break;
				}

				if (item.query) {
					item.location = item.query
					item = this.processMenuItem(item)
					/* Don't render 'RS.client.Menu' as icon since we will have navigation menu
					if (item.query == 'RS.client.Menu') {
						item.iconCls = 'icon-large icon-th rs-client-button';
						item.text = !item.firstLayer ? item.text : ''
					}
					*/
				} else if (item.children && item.children.length > 0) {
					item.menu = {
						xtype: 'menu',
						componentCls: 'toolbar-nav-menu',
					}
					item.menu.items = this.massageToolbarData(item.children)
					item.menu.closeHandler = Ext.emptyFn
					var allHidden = true;
					Ext.Array.forEach(item.menu.items, function(item) {
						item.closeHandler = Ext.emptyFn
					})
					Ext.Array.each(item.menu.items, function(item) {
						if (!item.hidden) {
							allHidden = false
							return false
						}
					})
					if (allHidden)
						item.hidden = true
				} else
					item.hidden = true

			}, this)
		return toolbarItems
	},

	parseLegacyConfiguration: function(toolbarConfig) {
		var toolbar = [],
			toolbarItems = toolbarConfig.replace(/;\]/g, ']').split(';'),
			toolbarItem, menuItem, toolbarMenu, itemConfigs, startedMenu = false,
			text;

		Ext.Array.forEach(toolbarItems, function(item) {
			if (item) {
				itemConfigs = item.split('[')
				itemConfig = itemConfigs[0].split('=')
				text = Ext.String.trim(itemConfig[0])
				itemConfig.splice(0, 1)
				if (!startedMenu) {
					toolbarItem = {
						text: text,
						componentCls: 'toolbar-nav-menu-item',
						location: itemConfig.join('=')
					}
					if (itemConfigs.length > 1) {
						var t = itemConfigs[1]
						if (t.indexOf(']') > -1) t = t.substring(0, itemConfigs[1].length - 1)
						else startedMenu = true
						var temp = t.split('=')
						text = Ext.String.trim(temp[0])
						temp.splice(0, 1)
						var menuItem = {
							text: text,
							componentCls: 'toolbar-nav-menu-item',
							location: temp.join('=')
						}
						if (!toolbarItem.menu) toolbarItem.menu = {
							xtype: 'menu',
							componentCls: 'toolbar-nav-menu',
							items: []
						}
						this.convertOldMenuItem(menuItem)
						toolbarItem.menu.items.push(menuItem)
					}
					this.convertOldMenuItem(toolbarItem)
					toolbar.push(toolbarItem)
				} else {
					var menuItem = {
						text: text,
						componentCls: 'toolbar-nav-menu-item',
						location: itemConfig.join('=')
					}
					if (menuItem.location.indexOf(']') > -1) {
						menuItem.location = menuItem.location.substring(0, menuItem.location.length - 1)
						startedMenu = false
					}

					if (!toolbarItem.menu) toolbarItem.menu = {
						xtype: 'menu',
						componentCls: 'toolbar-nav-menu',
						items: []
					}
					this.convertOldMenuItem(menuItem)
					toolbarItem.menu.items.push(menuItem)
				}
			}
		}, this)

		return toolbar
	},

	convertOldMenuItem: function(item) {
		//convert the old locations to new equivalents
		var location = item.location;
		if (location.indexOf('rswiki.jsp') > -1) {
			item.location = 'RS.wiki.Main/name=' + location.split('=')[1]
		}

		if (location.indexOf('rsworksheet.jsp') > -1) {
			if (location.indexOf('ACTIVE') > -1)
				item.location = 'RS.worksheet.Worksheet/id=ACTIVE'
			else
				item.location = 'RS.worksheet.Worksheets'
		}

		if (location.indexOf('resolve/service/menu') > -1)
			item.location = 'RS.client.Menu'

		if (location.indexOf('/resolve/social/rssocial.jsp') > -1)
			item.location = 'RS.social.Main'

		if (location.indexOf('rsclient.jsp') > -1) {
			item.location = 'RS.client.Menu'
			item.target = '_blank'
		}

		if (location.indexOf('ADDDOCUMENT') > -1) {
			item.location = 'RS.wiki.AddDocument'
			item.action = 'WINDOW'
		}
	},

	/* Navigation Menu */
	currentNavigationMenuSetId: 'all',
	userMenuItems: null,

	generateNavigationMenu: function() {
		this.flattenUserMenuSection();

		//remove left toolbar items if there were any previously
		this.lefttoolbarItems.removeAll();

		// Add the toolbar left logo
		if(this.toolbarLogo !== '') {
			this.lefttoolbarItems.add({
				xtype: 'image',
				src: this.toolbarLogo,
				height: '@{..toolbarLogoHeight}',
				width: '@{..toolbarLogoWidth}',
				hidden: this.toolbarLogoHidden,
				padding: '0'
			});
		}

		this.lefttoolbarItems.add({
			xtype: 'button',
			tooltip: this.localize('menuText'),
			iconCls: 'icon-larger icon-th rs-client-button',
			arrowCls: '',
			menuId: '-1',
			modelName: 'RS.client.Menu',
			listeners: {
				scope: this,
				render: this.handleMenuButtonRender
			},
			menu: {
				xtype: 'menu',
				componentCls: 'toolbar-nav-menu top-nav-menu',
				closeHandler : Ext.emptyFn,
				items: this.generateNavigationMenuItems(this.currentNavigationMenuSetId)
			}
		});
	},

	createHrefDataForNavItem: function(location, params) {
		var queryString = '';
		if (params) {
			for (var key in params) {
				if (key !== 'location') {
					if (queryString) {
						queryString += '&' + key + '=' + params[key]
					} else {
						queryString = key + '=' + params[key]
					}
				}
			}
		}
		var loc = location || '';
		if (loc.startsWith('/resolve/jsp/rswiki.jsp?wiki=')) {
			loc = loc.split('/resolve/jsp/rswiki.jsp?wiki=')[1];
		}
		if (loc.startsWith('RS.')) {
			loc = '/resolve/jsp/rsclient.jsp?#'+loc;
		} else if (loc.startsWith('/resolve/customtable/customtable.jsp')) {
			loc = '/resolve/jsp/rsclient.jsp?#RS.client.URLScreen/location=' + loc;
		} else if (loc && !loc.startsWith('/resolve/')) {
			loc = '/resolve/jsp/rsclient.jsp?#RS.wiki.Main/name=' + loc;
		}
		var paramsLoc = params? (params.location || '') : '';
		if (paramsLoc.startsWith('/resolve/customtable/customtable.jsp')) {
			paramsLoc = '/resolve/jsp/rsclient.jsp?#RS.client.URLScreen/location=' + paramsLoc;
		}
		return {
			loc: loc,
			paramsLoc: paramsLoc,
			queryString: queryString
		};
	},

	generateNavigationMenuSectionItems: function(items) {
		var menuSectionItems = [];
		for (var i=0; i < items.length; i++) {
			var menuSectionItem = items[i];
			if (menuSectionItem.items && menuSectionItem.items.length) {
				menuSectionItems.push({
					text: menuSectionItem.name,
					componentCls: 'toolbar-nav-menu-item',
					menu: {
						xtype: 'menu',
						componentCls: 'toolbar-nav-menu',
						closeHandler : Ext.emptyFn,
						items: this.generateNavigationMenuSectionItems(menuSectionItem.items)
					}
				});
			}
			else if (this.userMenuItems[menuSectionItem.id] || menuSectionItem.link) {
				var location = '';
				var modelName = '';
				var params = '';
				if (this.userMenuItems[menuSectionItem.id]) {
					location = this.userMenuItems[menuSectionItem.id].location;
					modelName = this.userMenuItems[menuSectionItem.id].modelName;
					params = this.userMenuItems[menuSectionItem.id].params;
				} else if (menuSectionItem.link) {
					location = menuSectionItem.link;
				}
				var hrefData = this.createHrefDataForNavItem(location, params);
				var loc = hrefData.loc;
				var paramsLoc = hrefData.paramsLoc;
				var queryString = hrefData.queryString;
				var href = loc || paramsLoc || '/resolve/jsp/rsclient.jsp?#'+modelName+'/'+queryString;

				// For menu item links, only use the hash part of href so open as Window (e.g. About page) will work correctly
				if (href.startsWith('/resolve/jsp/rsclient.jsp?#')) {
					var hashParts = href.split('#');
					href = '#' + hashParts[1];
				}

				menuSectionItems.push({
					scope: this,
					text: menuSectionItem.name,
					location: location,
					modelName: modelName,
					href: href,
					params: params,
					handler: this.handleNavigationClick,
					componentCls: 'toolbar-nav-menu-item',
					listeners: {
						scope: this,
						render: this.handleMenuButtonRender
					}
				});
			}
		}
		return menuSectionItems;
	},

	generateDynamicNavigationMenuSectionItems: function(items) {
		var menuSectionItems = [];
		if (!items) {
			return [];
		}
		for (var i=0; i < items.length; i++) {
			var menuSectionItem = items[i];
			if (menuSectionItem.items && menuSectionItem.items.length) {
				menuSectionItems.push({
					text: menuSectionItem.name,
					componentCls: 'toolbar-nav-menu-item',
					menu: {
						xtype: 'menu',
						componentCls: 'toolbar-nav-menu',
						closeHandler : Ext.emptyFn,
						items: this.generateDynamicNavigationMenuSectionItems(menuSectionItem.items)
					}
				});
			}
			else {
				var id = menuSectionItem.menuId;
				var location = this.userMenuItems[id].location;
				var params = this.userMenuItems[id].params;
				var modelName = this.userMenuItems[id].modelName;
				var hrefData = this.createHrefDataForNavItem(location, params);
				var loc = hrefData.loc;
				var paramsLoc = hrefData.paramsLoc;
				var queryString = hrefData.queryString;
				var href = loc || paramsLoc || '/resolve/jsp/rsclient.jsp?#'+modelName+'/'+queryString;

				// For menu item links, only use the hash part of href so open as Window (e.g. About page) will work correctly
				if (href.startsWith('/resolve/jsp/rsclient.jsp?#')) {
					var hashParts = href.split('#');
					href = '#' + hashParts[1];
				}

				if (this.userMenuItems[id]) {
					menuSectionItems.push({
						scope: this,
						text: menuSectionItem.text,
						location: location,
						href: href,
						modelName: modelName,
						params: params,
						handler: this.handleNavigationClick,
						componentCls: 'toolbar-nav-menu-item',
						listeners: {
							scope: this,
							render: this.handleMenuButtonRender
						}
					});
				}
			}
		}
		return menuSectionItems;
	},

	renderDynamicMenuSection: function(items, menuName, menuId) {
		if (clientVM.user && clientVM.user.menu) {
			var userMenu = clientVM.user.menu;
			if (userMenu && userMenu.sections) {
				for (var i=0; i < userMenu.sections.length; i++) {
					var section = userMenu.sections[i];
					if (menuName == section.text || menuId == section.menuId) {
						var sectionItems = this.generateDynamicNavigationMenuSectionItems(section.items);
						if (sectionItems.length) {
							items.push({
								text: menuName,
								componentCls: 'toolbar-nav-menu-item',
								menu: {
									xtype: 'menu',
									componentCls: 'toolbar-nav-menu top-nav-submenu',
									closeHandler : Ext.emptyFn,
									items: sectionItems
								}
							});
						}
						break;
					}
				}
			}
		}
	},

	renderMenuSection: function(menuSection, items) {
		if (menuSection.name === 'Gateway Administration') {
			this.renderDynamicMenuSection(items, menuSection.name);
		} else if (menuSection.items && menuSection.items.length) {
			var sectionItems = this.generateNavigationMenuSectionItems(menuSection.items);
			if (sectionItems.length) {
				items.push({
					text: menuSection.name,
					componentCls: 'toolbar-nav-menu-item',
					menu: {
						xtype: 'menu',
						componentCls: 'toolbar-nav-menu top-nav-submenu',
						closeHandler : Ext.emptyFn,
						items: sectionItems
					}
				});
			}
		} else if (this.userMenuItems[menuSection.id] || menuSection.link) {
			var location = '';
			var modelName = '';
			var params = '';
			if (this.userMenuItems[menuSection.id]) {
				location = this.userMenuItems[menuSection.id].location;
				modelName = this.userMenuItems[menuSection.id].modelName;
				params = this.userMenuItems[menuSection.id].params;
			} else if (menuSection.link) {
				location = menuSection.link;
			}
			items.push({
				text: menuSection.name,
				location: location,
				modelName: modelName,
				params: params,
				componentCls: 'toolbar-nav-menu-item',
				scope: this,
				handler: this.handleNavigationClick,
				listeners: {
					scope: this,
					render: this.handleMenuButtonRender
				}
		   });
		}
	},

	generateNavigationMenuAllSectionsItems: function() {
		// menuSets and menuSections are global variables from Menu_Sections.js
		var items = [];
		if (typeof(menuSections) != 'undefined') {
			for (var key in menuSections) {
				if (menuSections.hasOwnProperty(key)) {
					var menuSection = menuSections[key];
					this.renderMenuSection(menuSection, items);
				}
			}
		}
		return items;
	},

	generateNavigationMenuCurrentMenuSetItems: function(currentMenuSetItems) {
		// menuSets and menuSections are global variables from Menu_Sections.js
		var items = [];
		if (typeof(menuSections) != 'undefined') {
			for (var i = 0; i < currentMenuSetItems.length; i++) {
				var menuSection = menuSections[currentMenuSetItems[i]];
				this.renderMenuSection(menuSection, items);
			}
		}
		return items;
	},

	flattenUserMenuSection: function() {
		var userMenuItems = {};
		if (clientVM.user && clientVM.user.menu) {
			var userMenu = clientVM.user.menu;
			if (userMenu.sections) {
				for (var i = 0; i < userMenu.sections.length; i++) {
					var section = userMenu.sections[i];
					if (section.items) {
						for (var j = 0; j < section.items.length; j++) {
							var item = section.items[j];
							var key = item.menuId;
							userMenuItems[key] = {
								location: item.location,
								modelName: item.modelName,
								params: item.params
							};
						}
					}
				}
			}
		}
		this.set('userMenuItems', userMenuItems);
	},

	generateNavigationMenuItems: function(currentMenuSetId) {
		// menuSets and menuSections are global variables from Menu_Sections.js
		if (currentMenuSetId && (typeof(menuSets) != 'undefined')) {
			for (var i = 0; i < menuSets.length; i++) {
				// verify the currentMenuSet exist in "menuSets" then generate that menuSet
				if (menuSets[i].id == currentMenuSetId) {
					return this.generateNavigationMenuCurrentMenuSetItems(menuSets[i].items);
				}
			}
		}
		// just generate all sections if currentMenuSet or menuSets is not given/defined
		return this.generateNavigationMenuAllSectionsItems();
	},

	/* Navigation */
	handleNavigationClick: function(button) {
		if (!this.navTask)
			this.set('navTask', new Ext.util.DelayedTask(Ext.emptyFn, this))

		button = this.processMenuItem(button)
		if (!button) return

		//Three cases, either the button has a location that we need to open in the screen area
		//Or we have a modelName that we need to add to the screens and let glu deal with displaying it
		//Or we have an action that we need to perform (like create a new tab) which will be a finite set
		if (button.params && button.params.action && button.params.action.toLowerCase() == 'window') button.action = 'WINDOW'
		if (button.action) {
			switch (button.action) {
				case 'NEW':
					clientVM.handleWindowOpen(RS.common.encodeForURL('/'))
					break;
				case 'WINDOW':
					this.open({
						mtype: button.modelName
					})
					break;
				case 'POST':
					this.ajax({
						url: button.location,
						method: 'POST',
						scope: this,
						success: function(r) {
							var response = RS.common.parsePayload(r)
							if (response.success) this.displaySuccess(response.message)
							else this.displayError(response.message)
						},
						failure: function(resp) {
							this.displayFailure(resp);
						}
					})
					break;
			}
		} else if (button.modelName) {
			this.navTask.delay(200, function() {
				this.handleNavigation({
					modelName: button.modelName,
					params: button.params || {},
					target: button.target,
					id: button.menuId,
					text: button.text
				})
			})
		} else if (button.location) {
			this.navTask.delay(200, function() {
				this.handleNavigation({
					modelName: 'RS.client.URLScreen',
					params: Ext.apply(button.params || {}, {
						location: button.location
					}),
					text: button.text,
					id: button.menuId,
					target: button.target
				})
			})
		} else {
			this.displayError(this.localize('UnknownActionBody'), 5000, this.localize('UnknownActionTitle'));
		}
	},

	navTask: null,

	/* Menu */
	menuSections: [],

	menuSets: [],

	/* Screen */
	activeScreen: -1,
	screens: {
		mtype: 'activatorlist',
		focusProperty: 'activeScreen'
	},

	/* Navigation */
	handleNavigation: function(screenConfig) {
		if (!this.activeScreen.checkNavigateAway || this.activeScreen.checkNavigateAway() !== false) {

			if (screenConfig.params && screenConfig.params.action) {
				if (screenConfig.params.action.toLowerCase() == 'window') {
					this.open({
						mtype: screenConfig.modelName
					})
					return;
				}
			}

			Ext.applyIf(screenConfig, { params: {} });

			if (Ext.isObject(screenConfig.params)) {
				// special handing for React UI
				if (screenConfig.params.location && screenConfig.params.location.indexOf('/resolve/sir/index.html') !== -1) {
					window.location = screenConfig.params.location;
					return;
				}
				screenConfig.params = Ext.Object.toQueryString(screenConfig.params);
			}

			var screenHistoryToken = Ext.ux.Router.routes[Ext.isDefined(screenConfig.sticky) ? 0 : 1].routeMatcher.stringify(screenConfig);

			if (screenConfig.target == '_blank') {
				//Open in a new window instead of adding it to the screen here
				clientVM.handleWindowOpen(RS.common.encodeForURL('#' + screenHistoryToken));
			} else {
				if (this.fireEvent('beforeScreenChanged', this, screenConfig) !== false) {

					//Add the new screen to the history and let the app respond appropriately
					if (glu.testMode) {
						this.goToScreen(screenConfig);
					} else {
						Ext.ux.Router.redirect(screenHistoryToken, false);
					}
				}
			}
		}
	},
	handleNavigationBack: function() {
		if (this.screens.length > 1) {
			if (!this.activeScreen.checkNavigateAway || this.activeScreen.checkNavigateAway() !== false) Ext.History.back()
		} else {
			Ext.defer(function() {
				this.goToMain()
			}, 100, this)
		}
	},
	handleNavigationBackOrExit: function() {
		if (this.screens.length > 1) {
			if (!this.activeScreen.checkNavigateAway || this.activeScreen.checkNavigateAway() !== false) Ext.History.back()
		} else {
			window.close()
			Ext.defer(function() {
				this.goToUserHome()
			}, 100, this)
		}
	},
	goToScreen: function(screenConfig) {
		if (screenConfig.modelName && screenConfig.modelName.indexOf(this.CSRFTOKEN_NAME) > -1) {
			screenConfig = this.removeCSRFToken(screenConfig);
		}

		if (!this.user) {
			this.restoreScreen = screenConfig;
		} else if (screenConfig.modelName) {
			var screen = null;

			if (screenConfig.modelName === 'RS.incident.Dashboard') {
				if (!clientVM.SIR_PROBLEMID) {
					window.location.href = '/resolve/sir/index.html#/securityoperations/dashboard';
				}
			}
			else if (screenConfig.modelName === 'RS.incident.Playbook') {
				var sirNumber = screenConfig.params.split('=')[1];
				if (!clientVM.SIR_PROBLEMID) {
					window.location.href = '/resolve/sir/index.html#/securityoperations/incident/' + sirNumber + '/playbook';
				}
			}
			else if (Ext.isString(screenConfig.params)) {
				var params = Ext.Object.fromQueryString(screenConfig.params);
				if (screenConfig.modelName && params.action && params.action.toLowerCase() == 'window') {
					var viewmodel = {
						mtype: screenConfig.modelName,
						modal: true
					}
					Ext.apply(viewmodel, params);
					delete viewmodel.action;
					this.open(viewmodel);
					return;
				}
			}

			for(var i = 0; i < this.screens.length; i++) {
				var s = this.screens.getAt(i);
				var canCleanup = ['RS.decisiontree.Main','RS.wiki.Main','RS.formbuilder.Form','RS.incident.PlaybookTemplate'].indexOf(s.modelName) != -1;

				if (canCleanup && typeof s.deactivate === 'function') {
					s.deactivate();
					setTimeout(function(){
						this.screens.remove(s)
					}.bind(this),500); //Give it enough time for the component to do pre destroy clean up work before actually remove it.
					this.removeKeyMapButtons(s.modelName);
				}
				else if(s.modelName === screenConfig.modelName) {
					screen = s;
				}
			}

			if (Ext.isString(screenConfig.params)) {
				screenConfig.params = Ext.Object.fromQueryString(screenConfig.params);
			}
			var asNew = false;
			if (!screen) {
				screen = this.model(Ext.apply({
					mtype: 'Screen',
					mock: this.mockScreen
				}, screenConfig));
				this.screens.add(screen);
				asNew = true;
			}

			this.set('activeScreen', screen)
			screen.sticky = !!screenConfig.sticky;

			//call activate on the screen to allow the screen to change things like document title and window name'
			if (typeof screen.activate === 'function') {
				screen.activate(screenConfig.params, asNew);
			}

			this.removeOldScreens();
			this.updateWindowHash();
		}
	},

	handleWindowOpen: function(safeUrl, target) {
		var url = RS.common.decodeFromURL(safeUrl);
		var urlParts = url.split('?');
		if (urlParts.length > 1) {
			url = urlParts[1];
		}

		var hashParts = url.split('#');
		url = hashParts[0];
		var hashPart = '';
		if (hashParts.length > 1) {
			hashPart = '#' + hashParts[1];
		}
		url = RS.common.encodeForURL(url);
		window.open(Ext.String.format('/resolve/jsp/rsclient.jsp?{0}&{1}{2}', clientVM.rsclientToken, url, hashPart), (target ? target : ''));
	},

	removeOldScreens: function () {
		if(this.screens.length > 10) {
			var oldScreens = [];

			for (var i = 0; i < this.screens.length; i++) {
				oldScreens.push({
					screen: this.screens.getAt(i),
					screenIndex: i
				});
			}

			oldScreens.sort(function (a, b) {
				var result = 0;

				if (a.screen.lastActivated > b.screen.lastActivated) {
					result = -1;
				} else if (a.screen.lastActivated < b.screen.lastActivated) {
					result = 1;
				}
				return result;
			});

			for(var i = 10; i < oldScreens.length; i++) {
				var old = oldScreens[i];
				this.screens.removeAt(old.screenIndex);
				this.removeKeyMapButtons(old.screen.modelName);
			}
		}
	},

	toggleBanner: function(item, pressed) {
		this.set('bannerIsVisible', !pressed)
	},

	searchStore: {
		mtype: 'store',
		fields: ['id',{
			name : 'display',
			convert : function(val, r){
				return Ext.String.htmlEncode(r.get('id'));
			}
		}],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	searchText: '',
	searchTextIsValid: function(searchCriteria) {
		if (!Ext.isDefined(searchCriteria)) searchCriteria = this.searchText

		if (!searchCriteria) return this.localize('searchTextInvalidBlank', searchCriteria)

		//Validate search text to be alphanumeric _ - or .
		if (/[^a-zA-Z0-9_ \-.]+/.test(searchCriteria)) return this.localize('searchTextInvalidSpecialChars', searchCriteria)

		//Validate Wiki Format letters.letters
		if (!/[\w]+[.]{1}[\w]+/.test(searchCriteria)) return this.localize('searchTextInvalidWikiFormat', searchCriteria)

		//Validate not more than 2 .'s in the name
		if (searchCriteria.split('.').length > 2) return this.localize('searchTextInvalidWikiFormat', searchCriteria)

		return true;
	},
	searchSpecialKey: function(e) {
		if (e.getKey() === e.ENTER) {
			Ext.defer(function() {
				if (e.getKey() === e.ENTER) {
					if(e.ctrlKey) {
						this.goToSearch(true);
					} else {
						this.performSearch(e.shiftKey);
					}
				}
			}, 500, this);
		}
	},
	performSearch: function(create) {
		if (this.searchText) {
			this.addToSearchHistory(this.searchText)

			//Process internal resolve commands
			if (this.searchText.indexOf('rs:') == 0) {
				var searchUrl = '',
					searchString = this.searchText.substring(3);
				if (searchString.indexOf('www') == 0) {
					searchUrl = searchString
					create = true
				} else if (searchString.indexOf('/') == 0) {
					searchUrl = searchString.indexOf('/resolve') === 0 ? searchString : '/resolve' + searchString
				} else if (searchString.indexOf('.jsp') > -1) {
					searchUrl = searchString
				} else {
					// searchUrl = '/resolve/jsp/rswiki.jsp?wiki=' + searchString
					this.handleNavigation({
						modelName: 'RS.wiki.Main',
						params: Ext.Object.toQueryString({
							name: Ext.String.trim(searchString)
						}),
						target: create ? '_blank' : ''
					})
					return
				}

				this.handleNavigation({
					modelName: 'RS.client.URLScreen',
					params: {
						location: searchUrl
					},
					target: create ? '_blank' : ''
				})
				return
			}

			// Run the search command
			this.handleNavigation({
				modelName: 'RS.search.Main',
				params: {
					search: this.searchText
				},
				target: create ? '_blank' : ''
			})
		} else {
			var isValid = this.searchTextIsValid()
			this.displayError(isValid, 5000, this.localize('searchTextInvalidTitle'))
		}
	},

	goToSearch: function(create) {
		// Go straight to the wiki document with the provided search criteria
		var isValid = this.searchTextIsValid();
		if (isValid === true) {
			this.addToSearchHistory(this.searchText)
			this.handleNavigation({
				modelName: 'RS.wiki.Main',
				params: {
					name: Ext.String.trim(this.searchText)
				},
				target: create ? '_blank' : ''
			})
		} else {
			this.displayError(isValid, 5000, this.localize('searchTextInvalidTitle'))
		}
	},

	addToSearchHistory: function(token) {
		var searchHistory = [];

		this.searchStore.clearFilter()

		if (this.searchStore.getById(token))
			this.searchStore.remove(this.searchStore.getById(token))

		this.searchStore.insert(0, {
			id: token
		})

		this.searchStore.each(function(record) {
			searchHistory.push({
				id: record.get('id')
			})
		})

		while (this.searchStore.getCount() > 8) {
			this.searchStore.removeAt(7)
		}

		Ext.state.Manager.set('searchHistory', searchHistory.slice(0, 8))
	},

	addToWikiHistory: function(wiki) {
		if (!wiki) return
		var wikiHistory = Ext.state.Manager.get('wikiHistory', [])

		if (Ext.isString(wikiHistory)) wikiHistory = Ext.decode(wikiHistory)

		var contains = false;
		Ext.Array.forEach(wikiHistory, function(w) {
			if (wiki.id == w.id) {
				w.lastViewed = Ext.Date.format(new Date(), 'time')
				contains = true
			}
		})

		if (!contains) {
			wikiHistory.push({
				id: wiki.id,
				title: wiki.utitle,
				fullName: wiki.unamespace + '.' + wiki.uname,
				lastViewed: Ext.Date.format(new Date(), 'time')
			})
		}

		Ext.state.Manager.set('wikiHistory', wikiHistory.slice(0, 50))
	},

	redirectOnUnauthorized: function() {
		// close any opened dialog window before redirecting
		Ext.WindowMgr.each(function(win) {
			if (win.xtype == 'window' && win.isVisible()) {
				win.close();
			}
		})

		Ext.defer(function() {
			this.goToMain()
		}, 100, this)
	},

	displayFailure: function(resp) {
		if(this.currentNotificationLevel <= this.notificationLevel['error']){
			var msg = '';
			if (resp) {
				if (resp.status == 0) {
					// status=0 if ajax call was cancelled or access denied due to cross-site scripting
					return;
				}
				msg = this.serverErrorTxt + ' : ' + resp.status;
				switch (resp.status) {
				case 400:
				case 401:
				case 403:
				case 404:
				case 500:
				case 502:
					msg += ' ' + (resp.statusText || this.localize('http_'+resp.status));
					break;
				default:
					msg += ' ' + (resp.statusText || this.localize('serverCommunicationFailure'));
					break;
				}
			} else {
				msg = this.localize('failure');
			}
			RS.UserMessage.msg({
				success: false,
				title: this.errorTitle,
				msg: Ext.htmlEncode(msg),
				duration: 0,
				closeable: true
			})
		}
		// If Forbidden, set invalid flag so polling will be suspended
		if (resp && resp.status === 403) {
			this.setLogoutFlag();
			this.removeCSRFTokenFlag();
		}
	},

	isUnauthorized: function(errMsg) {
		// check for unauthorization: "has no rights", "not have rights / permission"
		return isUnauthorized = ((errMsg.indexOf('has no') != -1 && errMsg.toLowerCase().indexOf('right') != -1) ||
			(errMsg.indexOf('does not have') != -1 && errMsg.toLowerCase().indexOf('right') != -1) ||
			(errMsg.indexOf('need to have') != -1 && errMsg.toLowerCase().indexOf('role for this operation') != -1) ||
			(errMsg.indexOf('not have') != -1 && (errMsg.toLowerCase().indexOf('right') != -1 || errMsg.indexOf('permission') != -1)));
	},

	displayUnauthorized: function() {
		this.message({
			title: this.localize('Unauthorized'),
			msg: this.localize('UnauthorizedMessage'),
			buttons: Ext.MessageBox.OK,
			buttonText: {
				ok: this.localize('ok'),
			},
			scope: this,
			fn: this.redirectOnUnauthorized
		})
	},

	// parameters: exception, server response, operation
	displayExceptionError: function(e, resp, op) {
		var respData = RS.common.parsePayload(resp);

		if (respData && respData.errCode && respData.errCode == 'SESSION_TIMEOUT') {
			this.displayAuthenticateUserDialog();
		}
		else {
			var title = this.errorTitle,
				duration = 0,
				errMsg = '';

			if (respData && respData.message && respData.message.indexOf('decode an invalid JSON String') == -1) {
				errMsg = respData.message;
			}

			if (errMsg) {
				errMsg += ' ';
			}

			if ((!errMsg) || (respData && respData.errCode == 'INVALID_SERVER_RESPONSE')) {
				if (op.error) {
					if (Ext.isObject(op.error)) {
						for (var key in op.error) {
							if (key == 'status' && op.error[key] == 0) {
								// status=0 if ajax call was cancelled or access denied due to cross-site scripting
								return;
							} else if (key == 'statusText') {
								switch (op.error['status']) {
								case 400:
								case 401:
								case 403:
								case 404:
								case 500:
								case 502:
									errMsg += ' ' + (op.error[key] || this.localize('http_' + op.error['status']));
									break;
								default:
									errMsg += ' ' + (op.error[key] || this.localize('serverCommunicationFailure'));
									break;
								}
							} else {
								errMsg += op.error[key] + ' ';
							}
						}
					} else if (Ext.isString(op.error)) {
						errMsg = op.error;
					}
				}
			}

			// server return no message, so set a default error message
			if (!errMsg) {
				errMsg = this.serverErrorTxt;

				// prefix with the api command
				var urlparts = e.url.split('/resolve/service/');
				if (urlparts.length > 1) {
					errMsg = urlparts[1] + ' : ' + errMsg;
				}
			}

			if (this.isUnauthorized(errMsg)){
				this.displayUnauthorized();
			}
			else if (this.currentNotificationLevel <= this.notificationLevel['error']) {
				RS.UserMessage.msg({
					success: false,
					title: title,
					msg: Ext.htmlEncode(errMsg),
					closeable: true,
					duration: duration
				})
			}
		}
		// If Forbidden, set invalid flag so polling will be suspended
		if (resp && resp.status === 403) {
			this.setLogoutFlag();
			this.removeCSRFTokenFlag();
		}
	},

	// parameters: message, duration or serverErrorMessage, title, redirectOnUnauthorized
	displayError: function() {
		var message = arguments[0],	// message is required parameter
			duration,
			serverErrMsg,
			title,
			redirectOnUnauthorized = true;

		if (arguments[1] && typeof(arguments[1]) == 'string') {
			serverErrMsg = arguments[1];
		}
		else if (arguments[1] && typeof(arguments[1]) == 'number') {
			duration = arguments[1];
		}

		if (arguments[2] && typeof(arguments[2]) == 'string') {
			title = arguments[2];
		}

		if (typeof(arguments[3]) == 'boolean') {
			redirectOnUnauthorized = arguments[3];
		}

		var errMsg = serverErrMsg ? serverErrMsg : message;

		if (redirectOnUnauthorized && this.isUnauthorized(errMsg)){
			this.displayUnauthorized();
		}
		else if (errMsg.indexOf('Session Timeout') != -1) {
			this.displayAuthenticateUserDialog();
		}
		else if(this.currentNotificationLevel <= this.notificationLevel['error']){
			RS.UserMessage.msg({
				success: false,
				title: title || this.errorTitle,
				msg: Ext.htmlEncode(errMsg),
				closeable: true,
				duration: duration || 0
			})
		}
	},
	displaySuccess: function(message, duration, title) {
		if(this.currentNotificationLevel <= this.notificationLevel['info']){
			RS.UserMessage.msg({
				success: true,
				title: title || this.localize('success'),
				msg: Ext.htmlEncode(message),
				duration: duration || 1000,
				closeable: true
			})
		}
	},
	displayMessage: function(title, message, config) {
		return RS.UserMessage.msg(Ext.applyIf(config || {}, {
			success: 2,
			title: title || this.localize('success'),
			msg: Ext.htmlEncode(message),
			duration: 5000,
			closeable: true
		}))
	},

	/* Menu processing */
	processMenuItem: function(menuItem) {
		if (menuItem.modelName || menuItem.location) {
			//forward to rsclient
			//Correct the location if its meant to be a model in the location (which can happen from the menu definition page)
			if (menuItem.location && (menuItem.location.indexOf('/resolve/jsp/rswiki.jsp?wiki=RS.') == 0 || menuItem.location.indexOf('RS.') == 0)) {
				menuItem.modelName = menuItem.location.substring(menuItem.location.indexOf('RS.'))
				var modelSplit = menuItem.modelName.split('/')
				menuItem.modelName = modelSplit[0]
				if (modelSplit.length > 1) {
					var params = Ext.Object.fromQueryString(modelSplit[1])
					menuItem.params = params
				}
				delete menuItem.location
			} else if (menuItem.location && menuItem.location.indexOf('/resolve/jsp/rswiki.jsp?wiki=') == 0) {
				menuItem.modelName = 'RS.wiki.Main'
				menuItem.params = {
					name: menuItem.location.substring('/resolve/jsp/rswiki.jsp?wiki='.length)
				}
			}
			return menuItem
		}
		return null
	},

	saveButtons: {},
	updateSaveButtons: function(saveButton) {
		this.saveButtons[this.activeScreen.modelName] = saveButton;
	},

	getActiveScreenSaveButton: function() {
		return this.saveButtons[this.activeScreen.modelName];
	},

	refreshButtons: {},
	updateRefreshButtons: function(refreshButton, screenName) {
		if (!!screenName) {
			this.refreshButtons[screenName] = refreshButton;
		} else {
			this.refreshButtons[this.activeScreen.modelName] = refreshButton;
		}
	},

	removeKeyMapButtons: function(screenName) {
		delete this.saveButtons[screenName];
		delete this.refreshButtons[screenName];
	},

	getActiveScreenRefreshButton: function() {
		return this.refreshButtons[this.activeScreen.modelName];
	},

	clickButton: function(button) {
		if (!!button) {
			// Programmatically click the button
			button.getEl().dom.click();
		}
	},

	clickActiveScreenSaveButton: function() {
		this.clickButton(this.getActiveScreenSaveButton());

	},

	clickActiveScreenRefreshButton: function() {
		this.clickButton(this.getActiveScreenRefreshButton());
	},

	setPopupSizeToEightyPercent: function(popupWin) {
		popupWin.setWidth(Math.round(Ext.getBody().getWidth() * 0.8));
		popupWin.setHeight(Math.round(Ext.getBody().getHeight() * 0.8));
	},

	makeCtrlASelectAllOnView: function(view) {
		view.getEl().set({contentEditable: true});
		view.getEl().on('keydown', function(e, t, eOpts) {
			// Only allow ctrl-A and ctrl-C to trigger default action
			if (!e.ctrlKey || (e.getCharCode() != Ext.EventObject.A && e.getCharCode() != Ext.EventObject.C)) {
				e.preventDefault();
			}
		});
	},

	getResultMacroLimit : function(){
		this.ajax({
			url: this.API['getSystemProperty'],
			params: {
				name: 'resultmacro.records.limit'
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					//clientVM.displayError(this.localize('getSysPropError', respData.message));
					this.displayError(respData.message);
					return;
				}
				if (respData.data) {
					clientVM['resultMacroLimit'] = parseInt(respData.data.uvalue);
				}
			},
			failure: function(resp) {
				this.displayFailure(resp);
			}
		});
	},

	getFileUploadMaxSize: function(onComplete) {
		this.ajax({
			url : '/resolve/service/sysproperties/getSystemPropertyByName',
			params : {
				name: 'fileupload.max.size'
			},
			scope : this,
			success : function(resp){
				var respData = RS.common.parsePayload(resp);
				if(respData.success){
					if(respData.data && respData.data.uvalue){
						clientVM['fileuploadMaxSize'] = respData.data.uvalue;
					}
				}
				else {
					clientVM.displayError(respData.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				if (typeof onComplete === 'function') {
					onComplete.call(this);
				}
			}
		})
	},

	viewResult: function(id) {
		this.open({
			mtype: 'RS.wiki.ResultViewer',
			id: id
		});
	},

	initCSRFToken: function() {
		// clear csrftoken-flag upon init
		this.getCSRFTokenFlag();
		this.initPageTokens();
		var removeParams = ['urlData']; // urlData is used be FE for redirecting to original bookmarked URL
		/* don't remove CSRFTOKEN_NAME, yet
		if (window.location.pathname.indexOf('/resolve/jsp/rsclient.jsp') === 0) {
			// only remove CSRFToken from URL if uri is rsclient.jsp
			removeParams.push(this.CSRFTOKEN_NAME);
		}
		*/
		this.removeParamsFromURL(removeParams);
	},

	getCSRFToken: function() {
		var xhr = window.XMLHttpRequest ? new window.XMLHttpRequest : new window.ActiveXObject("Microsoft.XMLHTTP");
		var csrfToken = {};
		xhr.open("POST", "/resolve/JavaScriptServlet", false);
		xhr.setRequestHeader("FETCH-CSRF-TOKEN", "1");
		xhr.send(null);

		var token_pair = xhr.responseText;

		//var temp_token_pair = token_pair.split(":");
		//var token_name = temp_token_pair[0];
		//var token_value = temp_token_pair[1];

		return token_pair;
	},

	getCSRFToken_ForURI: function(uri, onSuccess, onFailure, onComplete, ignoreSave, forceRenew) {
		var token = '';
		uri = RS.common.encodeForURL(RS.common.decodeFromURL(uri));
		if (!forceRenew) {
			token = this.getPageToken(uri.replace('/resolve/', ''));
		}
		if (token) {
			if (Ext.isFunction(onSuccess)) {
				onSuccess([this.CSRFTOKEN_NAME, token], uri);
			}
			if (Ext.isFunction(onComplete)) {
				onComplete();
			}
		} else {
			this.ajax({
				url : '/resolve/service/csrf/getCSRFTokenForPage',
				params : {
					uri: uri
				},
				async : false,
				scope: this,
				success : function(resp){
					var respData = RS.common.parsePayload(resp);
					if(respData.success){
						if (!ignoreSave) {
							this.setPageToken(uri, respData.data[1]);
						}
						if (Ext.isFunction(onSuccess)) {
							onSuccess(respData.data, uri);
						}
					}
					else {
						if (Ext.isFunction(onFailure)) {
							onFailure();
						} else {
							this.displayError(respData.message);
						}
					}
				},
				failure: function(resp) {
					if (Ext.isFunction(onFailure)) {
						onFailure();
					} else {
						this.displayFailure(resp);
					}
				},
				callback: function() {
					if (Ext.isFunction(onComplete)) {
						onComplete();
					}
				}
			})
		}
	},

	getCSRFTokenFlag: function() {
		if (localStorage.getItem('csrftoken-flag')) {
			this.clearCSRFTokenFlag();
			return true;
		}
		return false;
	},

	clearCSRFTokenFlag: function() {
		setTimeout(function() {
			this.removeCSRFTokenFlag();
		}.bind(this), 100)	// 100 miliseconds delay before clearing the localStorage csrftoken-flag
	},

	removeCSRFTokenFlag: function() {
		localStorage.removeItem('csrftoken-flag');
	},

	setCSRFTokenFlag: function() {
		localStorage.setItem('csrftoken-flag', true);
		this.clearCSRFTokenFlag();
	},

	getLogoutFlag: function() {
		if (localStorage.getItem('logout-flag')) {
			return true;
		}
		return false;
	},

	setLogoutFlag: function() {
		localStorage.setItem('logout-flag', true);
	},

	removeLogoutFlag: function() {
		localStorage.removeItem('logout-flag');
	},

	updateCSRFToken: function(csrftoken) {
		var token_name = csrftoken[0];
		var token_value = csrftoken[1];
		this.set('pollEnabled', true);

		XMLHttpRequest.prototype.onsend = function(data) {
			this.setRequestHeader("X-Requested-With", "XMLHttpRequest")
			this.setRequestHeader(token_name, token_value);
		};
	},

	removeCSRFToken: function(screenConfig) {
		var modelName = '';
		var queryParams = {};
		var modelParams = Ext.Object.fromQueryString(screenConfig.modelName);
		Ext.Object.each(modelParams, function(k, v) {
			if (v) {
				if (k.toUpperCase() != clientVM.CSRFTOKEN_NAME)
					queryParams[k] = v;
			} else {
				modelName += k;
			}
		})
		if (queryParams && Object.keys(queryParams).length) {
			modelName += Ext.Object.toQueryString(queryParams);
		}
		screenConfig.modelName = modelName;
		return screenConfig;
	},

	beforeDestroyComponent: function() {
		window.removeEventListener('storage', this.checkStorageEvents);
	},

	injectRsclientToken: function(link) {
		if (link.indexOf('/resolve/jsp/rsclient.jsp') === 0) {
			const urlData = link.split('#');
			const uriData = urlData[0].split('?');
			var tokenizedData = uriData[0] + '?' + this.rsclientToken;
			if (uriData.length > 1) {
				tokenizedData += '&' + uriData[1];
			}
			if (urlData.length > 1) {
				tokenizedData += '#' + urlData[1];
			}
			return tokenizedData;
		}
		return link;
	},

	injectCSRFPageTokenToElement: function(link, element, attribute) {
		if (link.indexOf('/resolve/jsp/rsclient.jsp') === 0) {
			element.setAttribute(attribute, this.injectRsclientToken(link));
		} else if (link.indexOf('/resolve/') === 0 && link.indexOf(this.CSRFTOKEN_NAME) === -1) {
			const urlData = link.split('#');
			const uriData = urlData[0].split('?');
			clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
				var csrftoken = '?' + data[0] + '=' + data[1];
				var tokenizedData = uri + csrftoken;
				if (uriData.length > 1) {
					tokenizedData += '&' + uriData[1];
				}
				if (urlData.length > 1) {
					tokenizedData += '#' + urlData[1];
				}
				element.setAttribute(attribute, tokenizedData);
			});
		}
	},

	injectCSRFPageTokenToForm: function(action, form) {
		if (action.indexOf('/resolve/service') === 0) {
			const urlData = action.split('#');
			const uriData = urlData[0].split('?');
			clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
				var input = document.createElement('input');
				input.setAttribute('type', 'hidden');
				input.setAttribute('name', data[0]);
				input.setAttribute('value', data[1]);
				form.appendChild(input);
			});
		}
	},

	injectCSRFPageTokenToImage: function(link, element, attribute) {
		if (link.indexOf('/resolve/service/wiki/download') === 0 && link.indexOf(this.CSRFTOKEN_NAME) === -1) {
			const urlData = link.split('#');
			const uriData = urlData[0].split('?');
			clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
				var csrftoken = '?' + data[0] + '=' + data[1];
				var tokenizedData = uri + csrftoken;
				if (uriData.length > 1) {
					tokenizedData += '&' + uriData[1];
				}
				if (urlData.length > 1) {
					tokenizedData += '#' + urlData[1];
				}
				element.setAttribute(attribute, tokenizedData);
			});
		}
	},

	injectCSRFPageTokenToCssStyleImage: function(style, element, property, pos) {
		if (style.indexOf('/resolve/service/wiki/download', pos) !== -1 && style.indexOf(this.CSRFTOKEN_NAME) === -1) { 
			var cssStyleImageRegEx = /\(([^)]+)\)/g;
			var match = cssStyleImageRegEx.exec(style);
			if (match != null) {
				var link = match[1];
				const urlData = link.split('#');
				const uriData = urlData[0].split('?');
				clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
					var csrftoken = '?' + data[0] + '=' + data[1];
					var tokenizedData = uri + csrftoken;
					if (uriData.length > 1) {
						tokenizedData += '&' + uriData[1];
					}
					if (urlData.length > 1) {
						tokenizedData += '#' + urlData[1];
					}
					element.style.setProperty(property, 'url("'+tokenizedData+'")');
					
					// if element.style still contains "_<property>-data", then rename to "<property>-data" so the cleanup code doesn't load the data
					if (element.style.cssText.indexOf('_' + property + '-data') !== -1) {
						element.style.cssText = element.style.cssText.replace('_' + property + '-data', property + '-data');
					}
				}); 
			}
		}
	},

	injectCSRFPageTokenToScriptImage: function(script, element, property, pos) {
		if (script.indexOf('/resolve/service/wiki/download', pos) !== -1) {
			var updated = false;
			var scriptList = script.split('_' + property + '-data');
			for (var i=0; i < scriptList.length; i++) {
				var scriptContent = scriptList[i];
				if (scriptContent.indexOf('/resolve/service/wiki/download') !== -1) {
					var scriptImageRegEx = /\(([^)]+)\)/g;
					var match = scriptImageRegEx.exec(scriptContent);
					if (match != null) {
						var link = match[1];
						const urlData = link.split('#');
						const uriData = urlData[0].split('?');
						clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
							var csrftoken = '?' + data[0] + '=' + data[1];
							var tokenizedData = uri + csrftoken;
							if (uriData.length > 1) {
								tokenizedData += '&' + uriData[1];
							}
							if (urlData.length > 1) {
								tokenizedData += '#' + urlData[1];
							}
							script = script.replace(link, tokenizedData);
							script = script.replace('_' + property + '-data', property);
							updated = true;
						}); 
					}
				}
			}
			if (updated) {
				var parent = element.parentElement;
				if (parent) {
					parent.removeChild(element);
					var scriptElement = document.createElement('script');
					scriptElement.setAttribute('type', 'text/javascript');
					scriptElement.innerHTML = script;
					parent.appendChild(scriptElement);
				}
			}
		}
	},

	injectCSRFPageTokenToScriptSrc: function(script, element, property, pos) {
		if (script.indexOf('/resolve/service/wiki/view', pos) !== -1) {
			var updated = false;
			var scriptList = script.split('["_' + property + '-data"]');
			for (var i=0; i < scriptList.length; i++) {
				var scriptContent = scriptList[i];
				if (scriptContent.indexOf('/resolve/service/wiki/view') !== -1) {
					var scriptSrcRegEx = /\s*=\s*["']+(.*)["']+/g;
					var match = scriptSrcRegEx.exec(scriptContent);
					if (match != null) {
						var link = match[1];
						const urlData = link.split('#');
						const uriData = urlData[0].split('?');
						clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
							var csrftoken = '?' + data[0] + '=' + data[1];
							var tokenizedData = uri + csrftoken;
							if (uriData.length > 1) {
								tokenizedData += '&' + uriData[1];
							}
							if (urlData.length > 1) {
								tokenizedData += '#' + urlData[1];
							}
							script = script.replace(link, tokenizedData);
							script = script.replace('["_' + property + '-data"]', '.' + property);
							updated = true;
						}); 
					}
				}
			}
			if (updated) {
				var parent = element.parentElement;
				if (parent) {
					parent.removeChild(element);
					var scriptElement = document.createElement('script');
					scriptElement.setAttribute('type', 'text/javascript');
					script = script.replace('Ext.onReady', '');
					scriptElement.innerHTML = script;
					parent.appendChild(scriptElement);
				}
			}
		}
	},

	injectCSRFTokensToWiki: function(wikiBody, isDT) {
		if (clientVM.SIR_PROBLEMID && !isDT) {
			return;
		} else if (!wikiBody) {
			var iframe = document.getElementById('wiki_frame') || document.getElementById('playbook-wikiview'); 
			if (iframe) {
				wikiBody = iframe.contentDocument.getElementsByClassName('wikiBody')[0];
			}
		}

		if (wikiBody) {
			// check HTML anchor href links
			var anchors = wikiBody.getElementsByTagName("a");
			for (var i=0; i < anchors.length; i++) {
				var element = anchors[i];
				var link = element.getAttribute('href');
				if (link) {
					clientVM.injectCSRFPageTokenToElement(link, element, 'href');
				}
			}
			if (isDT) {
				// check HTML form actions
				var forms = wikiBody.querySelectorAll('form');
				for (var i=0; i < forms.length; i++) {
					var form = forms[i];
					var action = form.getAttribute('action');
					if (action) {
						clientVM.injectCSRFPageTokenToForm(action, form);
					}
				}
			}
			// check HTML iframe src links
			var iframes = wikiBody.getElementsByTagName("iframe");
			for (var i=0; i < iframes.length; i++) {
				var element = iframes[i];
				var link = element.getAttribute('src');
				if (link) {
					clientVM.injectCSRFPageTokenToElement(link, element, 'src');
				}
			}
			// check HTML img src links
			var srcs = wikiBody.querySelectorAll('[_src-data]');
			for (var i=0; i < srcs.length; i++) {
				var element = srcs[i];
				var link = element.getAttribute('_src-data');
				if (link) {
					element.removeAttribute('_src-data');
					clientVM.injectCSRFPageTokenToImage(link, element, 'src');
				}
			}
			// check HTML background image links
			var backgrounds = wikiBody.querySelectorAll('[_background-data]');
			for (var i=0; i < backgrounds.length; i++) {
				var element = backgrounds[i];
				var link = element.getAttribute('_background-data');
				if (link) {
					element.removeAttribute('_background-data');
					clientVM.injectCSRFPageTokenToImage(link, element, 'background');
				}
			}
			// check CSS style background-image:url links
			var styles = wikiBody.querySelectorAll('[style]');
			for (var i=0; i < styles.length; i++) {
				var element = styles[i];
				var style = element.getAttribute('style');
				if (style && style.indexOf('_background-image-data') !== -1) {
					clientVM.injectCSRFPageTokenToCssStyleImage(style, element, 'background-image', style.indexOf('_background-image-data'));
				}
			}
			// check script function for background images manipulations
			var scripts = wikiBody.querySelectorAll('script');
			for (var i=0; i < scripts.length; i++) {
				var element = scripts[i];
				var script = element.innerHTML;
				if (script && script.indexOf('_background-image-data') !== -1) {
					clientVM.injectCSRFPageTokenToScriptImage(script, element, 'background-image', script.indexOf('_background-image-data'));
				}
			}
			// check script function for src links
			scripts = wikiBody.querySelectorAll('script'); // need to call querySelectorAll() again in case scripts have been overwritten
			for (var i=0; i < scripts.length; i++) {
				var element = scripts[i];
				var script = element.innerHTML;
				if (script && script.indexOf('["_src-data"]') !== -1) {
					clientVM.injectCSRFPageTokenToScriptSrc(script, element, 'src', script.indexOf('["_src-data"]'));
				}
			}

			// clean some _<property/attribute>-data changes that were modified in WikiVelocityRenderer but not updated above
			var innerHTML = wikiBody.innerHTML;
			innerHTML = innerHTML.replace(/\["_src-data"\]/g, '.src');
			innerHTML = innerHTML.replace(/"_src-data"/g, '.src');
			innerHTML = innerHTML.replace(/_src-data/g, 'src');
			innerHTML = innerHTML.replace(/_background-image-data/g, 'background-image');
			innerHTML = innerHTML.replace(/_background-data/g, 'background');
			if (wikiBody.innerHTML != innerHTML) {
				wikiBody.innerHTML = innerHTML; 
			}
		}
		return wikiBody;
	},

	removeURLHashVersion: function() {
		var hash = Ext.Object.fromQueryString(window.location.hash || '');
		delete hash.version;
		if (window.history.replaceState) {
			//prevents browser from storing history with each change:
			var hrefParts = window.location.href.split('#');
			var url = hrefParts[0];
			window.history.replaceState({}, '', hrefParts[0] + decodeURIComponent(this.hashToQueryString(hash)));
		} else {
			window.location.hash = decodeURIComponent(this.hashToQueryString(hash));
		}
	},
	updateURLHashVersion: function(version) {
		var hash = Ext.Object.fromQueryString(window.location.hash || '');
		hash.version = version;
		if (window.history.replaceState) {
			//prevents browser from storing history with each change:
			var hrefParts = window.location.href.split('#');
			var url = hrefParts[0];
			window.history.replaceState({}, '', hrefParts[0] + decodeURIComponent(this.hashToQueryString(hash)));
		} else {
			window.location.hash = decodeURIComponent(this.hashToQueryString(hash));
		}
	},

	hashToQueryString: function(hash) {
		var queryString = Object.keys(hash).map(function(key) {
			if (hash[key]) {
				return key + '=' + hash[key];
			} 
			return key;
		}).join('&');
		return queryString;
	},

	removeParamsFromURL: function(params) {
		// check for pages created in iframe using srcdoc attribute 
		if (window.location.origin === 'null') { // Chrome, Firefox, and MS Edge returns origin = null
			return;
		}
		try {
			var search = Ext.Object.fromQueryString(window.location.search || '');
			for (var i=0; i<params.length; i++) {
				var param = params[i];
				if (search[param]) {
					delete search[param];
				}
			}
			if (window.history.replaceState) {
				//prevents browser from storing history with each change:
				var decodedSearch = '';
				if (Object.keys(search).length) {
					decodedSearch = '?' + decodeURIComponent(Ext.Object.toQueryString(search));
				}
				var url = window.location.origin + window.location.pathname + decodedSearch + window.location.hash;
				window.history.replaceState({}, '', url);
			} else {
				window.location.search = decodeURIComponent(Ext.Object.toQueryString(search));
			}
		} catch (e) {
			// for IE, ignore URL manipulations
		}
	},
})
