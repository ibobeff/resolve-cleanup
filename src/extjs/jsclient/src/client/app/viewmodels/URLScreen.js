glu.defModel('RS.client.URLScreen', {
	urlLocation: '',
	activate: function(screen, params) {
		var tokens = params.location.split('#');
		var url = tokens[0];
		var hash = '';
		if (tokens[1]) {
			hash = '#' + tokens[1];
		}
		var me = this;
		var urlparts = url.split('?');
		if (urlparts[0] == '/' || urlparts[0].toLowerCase() == '/resolve' || urlparts[0].toLowerCase() == '/resolve/') {
			// if url's base uri is "/" (root UI), don't redirect to RS.client.URLScreen/location, just navigate to RS.client.Menu
			clientVM.handleNavigation({
				modelName: 'RS.client.Menu',
			});
		} else {
			clientVM.getCSRFToken_ForURI(urlparts[0], function(data) {
				var csrftoken = '?' + data[0] + '=' + data[1];
				var newParams = urlparts[0] + csrftoken;
				if (urlparts.length > 1) {
					newParams += '&' + urlparts[1];
				}
				newParams += hash;
				var newParams = Ext.String.format('<iframe class="rs_iframe" src="{0}" style="width:100%;height:100%;border:0"></iframe>', newParams);
				me.set('urlLocation', newParams);
			});
		}
	}
})