/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.client').locale = {
	RunbookAutomation: ' ',
	logout: 'Logout',
	settings: 'Settings',

	Social: 'Social',
	Menu: 'Menu',
	menuSet: 'Menu Set',

	UnknownActionTitle: 'Unknown button clicked',
	UnknownActionBody: 'A button without a proper configuration was passed to the navigation handler',

	searchTextInvalidTitle: 'Invalid Search',
	searchTextInvalidWikiFormat: 'Invalid wiki document name format {0}. Use: Namespace.Document Name (e.g., Network.Health Check)',
	searchTextInvalidSpecialChars: "Special character's are not allowed in wiki document name except '-', '_' and '.'",
	searchTextInvalidBlank: 'No search criteria provided',

	http_400: 'Bad Request',
	http_401: 'Unauthorized',
	http_403: 'Forbidden',
	http_404: 'Not Found',
	http_405: 'Method Not Allowed',
	http_500: 'Internal Server Error',
	http_502: 'Bad Gateway',

	Unauthorized: 'Unauthorized',
	UnauthorizedMessage: 'You are not authorized. Please contact your system administrator.',
	ok: 'Ok',
	close: 'Close',
	cancel: 'Cancel',
	error: 'Error',
	success: 'Success',
	failure: 'An error has occurred on the server, please contact your administrator for assistance',
	serverError: 'Server Error',
	serverCommunicationFailure: 'Communication failure',

	worksheetNotFound: 'Worksheet not found',

	help: 'Context Help',

	notificationMessage: 'You have {0} unread notification',
	notificationMessages: 'You have {0} unread notifications',
	notificationNoneMessage: 'You have no new notifications',

	resolutionRoutingLookupError: 'The url params does not match with any RID.',
	mappingNotFound: 'No Mapping Found',
	Login: {
		u_sername: 'Username',
		p_assword: 'Password',
		authenticateUser: 'Log in',
		badLoginText: 'Incorrect username or password',
		unableToConnect: 'Unable to connect to server',
		emptyUserNameOrPassword: 'Empty username or password'
	},

	Menu: {
		MenuSets: 'Menu Sets',
		RecentlyUsed: 'History',
		All: 'All'
	},

	SideMenu: {
		sideMenu: 'Menu'
	},

	//Default Toolbar items
	organization : 'Organization',
	newText: 'New',
	menuText: 'Menu',
	mainMenuText: 'Main Menu',
	socialText: 'Social',
	worksheetText: 'Worksheet',
	worksheetsText: 'Worksheets',
	activeWorksheetText: 'Active Worksheet',
	newWorksheetText: 'New Worksheet',
	allWorksheetsText: 'All Worksheets',
	documentText: 'Document',
	defaultDocumentText: 'Homepage',
	addDocumentText: 'Add Document',
	history: 'History',
	listDocuments: 'List Documents',
	newWorksheetCreated: 'Created new worksheet.<a href="#RS.worksheet.Worksheet/id={0}">Click to view.</a>',
	About: {
		aboutTitle: 'About',
		ok: 'OK'
	},
	searchText: 'Search',
	goText: 'Go',

	//Org
	orgChangeTitle : 'Confirm Switching {0}',
	orgChangeMsg : 'Are you sure you want to switch {0}? Selected {0}: <b>{1}</b>',
	mismatchedOrgOnExecution : 'Execution is rejected due to mismatched org info. Please check the URL to make sure the org is correct.',

	Notifications: {
		notifications: 'Notifications',
		dismissAll: 'Dismiss All',
		close: 'Close',

		social: 'Social',
		socialText: 'social',
		goToSocial: 'Go to social',
		dismissSocial: 'Dismiss social messages'
	},

	Notification: {
		unreadMessage: 'You have {0} unread {1} message',
		unreadMessages: 'You have {0} unread {1} messages',
		noUnreadMessages: 'You have no unread {1} messages'

	},

	rrAutomationExeReqSubmitSuccess: 'Execution request for runbook:{0} has been submitted.',

	selectRole: 'Select Role',
	selectOption: 'Select Option',
	selectTag: 'Select Tag',
	selectType: 'Select Type...',
	invalidName: 'Name is required and must be in alphanumerics, but may contain single spaces or underscores \'_\'.',
	duplicateName: 'Duplicated name found',

	invalidUserSettings: 'Invalid user settings. <br><br>Please goto the User\'s page, select user <b>{0}</b>, and click <i>Save</i> to correct any invalid user settings.',

	licenseExpired: 'License expired. ',
	licenseExpiredInOneDay: 'License will expire in 1 day. ',
	licenseExpiredInDays: 'License will expire in {0} days. ',
	entitlementExceeded: 'Entitlement exceeded.',
	entitlementWillExceedOn: 'Entitlement will exceed on {0}.',
	dismiss: 'Hide',
	contactUs: 'Contact Us',
	getInTouchWithResolve: '<h2>Get in touch with Resolve Systems</h2>',
	supportPortal: '<b>Resolve Support:</b> <a target="_blank" href=https://resolvesystems.force.com/Support>Support Portal</a>',
	usPhone: '<b>US & Canada:</b> 949.325.0120 Option 2',
	ukPhone: '<b>UK:</b> +44 08082 341 079',
}