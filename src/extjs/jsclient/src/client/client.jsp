<!-- ******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
********************************************************************************* -->

<%@page import="com.resolve.util.JspUtils" %>
<%@page import="java.io.*" %> 
<%@page import="java.util.*" %> 
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
    boolean debug = false;
    String d = request.getParameter("debug");
    if( d != null && d.indexOf("t") > -1){
        debug = true;
    }

    boolean mock = false;
    String m = request.getParameter("mock");
    if( m != null && m.indexOf("t") > -1){
        mock = true;
    }

    boolean mockScreen = false;
    String ms = request.getParameter("mockScreen");
    if( ms != null && ms.indexOf("t") > -1){
        mockScreen = true;
    }
%>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <link rel="SHORTCUT ICON" href="/resolve/favicon.ico"/>

    <jsp:include page="/client/module-loader.jsp" flush="true"/>
</head>

<body>
</body>
</html>