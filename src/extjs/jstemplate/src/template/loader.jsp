<%@page import="com.resolve.util.JspUtils" %>
<%@page import="org.owasp.esapi.ESAPI"%>
<%
    String ver = ESAPI.encoder().encodeForURL(JspUtils.getResolveVersion());

    boolean debug = false;
    String d = request.getParameter("debug");
    if( d != null && d.indexOf("t") > -1){
        debug = true;
    }

    boolean debugTemplate = false;
    String dT = request.getParameter("debugTemplate");
    if( dT != null && dT.indexOf("t") > -1){
        debugTemplate = true;
    }

    boolean mock = false;
    String m = request.getParameter("mock");
    if( m != null && m.indexOf("t") > -1){
        mock = true;
    }
%>

<%
    if( debug || debugTemplate ){
%>
<script type="text/javascript" src="/resolve/template/js/template-classes.js?_v=<%=ver%>"></script>
<%
    }else{
%>
<script type="text/javascript" src="/resolve/template/js/template-classes.js?_v=<%=ver%>"></script>
<%
    }
%>

