glu.defView('RS.template.Grid', {
	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'grid',
		border: false,
		stateId: '@{stateId}',
		displayName: '@{displayName}',
		stateful: true,
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}],
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTooltip: RS.common.locale.editColumnTooltip,
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'id',
			childLink: 'RS.template.SubRecord',
			childLinkIdProperty: 'id'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: ['createRecord', 'deleteRecord']
		}],
		columns: '@{columns}',
		viewConfig: {
			enableTextSelection: true
		},
		listeners: {
			editAction: '@{editCatalog}',
			selectAllAcrossPages: '@{selectAllAcrossPages}',
			selectionChange: '@{selectionChanged}'
		}
	}]
});