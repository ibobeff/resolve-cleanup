glu.defModel('RS.template.Main', {

	mock: false,

	activate: function() {
		window.document.title = this.localize('windowTitle')
	},

	init: function() {
		if (this.mock) this.backend = RS.template.createMockBackend(true)
	}
})