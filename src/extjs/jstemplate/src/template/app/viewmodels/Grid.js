glu.defModel('RS.template.Grid', {

	mock: false,

	activate: function() {
		window.document.title = this.localize('windowTitle')
		this.grid.load()
	},

	displayName: '~~Template Grid~~',
	stateId: 'DEFINITELYCHANGEMEtemplateGridUniqueIdForState',

	grid: {
		mtype: 'store',
		fields: ['id', 'name'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/template/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	columns: [{
		header: '~~id~~',
		dataIndex: 'id',
		hidden: true,
		visible: false,
		autoWidth: true,
		sortable: false
	}, {
		header: '~~name~~',
		dataIndex: 'name',
		filterable: true,
		sortable: true,
		flex: 1
	}],

	allSelected: false,
	gridSelections: [],

	init: function() {
		//Called when mocking the backend to test the UI
		if (this.mock) this.backend = RS.template.createMockBackend(true)

		this.set('displayName', this.localize('templateGrid'))

		//initially load the grid from the server
		this.grid.load()
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createRecord: function() {
		clientVM.handleNavigation({
			modelName: 'RS.template.SubRecord'
		})
	},

	editRecord: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.template.SubRecord',
			params: {
				id: id
			}
		})
	},

	singleRecordSelected$: function() {
		return this.gridSelections.length == 1
	},
	editRecordIsEnabled$: function() {
		return this.singleRecordSelected
	},
	deleteRecordIsEnabled$: function() {
		return this.gridSelections.length > 0
	},
	deleteRecord: function() {
		//Delete the selected record(s), but first confirm the action
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.gridSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.gridSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/template/delete',
					params: {
						ids: '',
						whereClause: this.catalogs.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.catalogs.load()
						else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			} else {
				var ids = []
				Ext.each(this.gridSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/template/delete',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.catalogs.load()
						else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}
	}
})