<!-- ******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
********************************************************************************* -->

<%@page import="com.resolve.util.JspUtils" %>
<%@page import="java.io.*" %> 
<%@page import="java.util.*" %> 
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
    boolean debug = false;
    String d = request.getParameter("debug");
    if( d != null && d.indexOf("t") > -1){
        debug = true;
    }

    boolean mock = false;
    String m = request.getParameter("mock");
    if( m != null && m.indexOf("t") > -1){
        mock = true;
    }

    boolean mockScreen = false;
    String ms = request.getParameter("mockScreen");
    if( ms != null && ms.indexOf("t") > -1){
        mockScreen = true;
    }
%>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <link rel="SHORTCUT ICON" href="/resolve/favicon.ico"/>

    <jsp:include page="/client/module-loader.jsp" flush="true"/>

    <script type="text/javascript">
      var wikiDocumentName = '',
          problemId = '';
      
      //default to empty problemId if none is passed in
      if( problemId && problemId.indexOf('$') > -1 )problemId = '';
      
      Ext.onReady(function() {
            //Initialize State Management
            if (Ext.supports.LocalStorage) Ext.state.Manager.setProvider(new Ext.state.LocalStorageProvider())
            else Ext.state.Manager.setProvider(new Ext.state.CookieProvider())

            //Initialize extjs tooltips
            Ext.tip.QuickTipManager.init()

            //Configure glu for optimal performance
            glu.asyncLayouts = true
            
            //Configure bridge to client if one exists
            clientVM = getResolveRoot().clientVM;
            
            //If no client is configured, then this is being loaded outside of the rsclient.jsp (not recommended, but possible)
            //If that's the case then we need to configure the client with a stub that will act as a message buffer for us
            if( !clientVM ){
                clientVM = glu.model({mtype: 'RS.client.Main'}) //also happens to be the client that would have normally been loaded through rsclient.jsp
                clientVM.init()
            }

            //Register event listeners on clientVM for problemId change event
            if (window['clientVM']) {
                clientVM.on('problemIdChanged', function() {
                    startTask(5000)
                })
                clientVM.on('worksheetExecuted', function() {
                    startTask(5000)
                })
            }
        })
    </script>
</head>

<body>
</body>
</html>