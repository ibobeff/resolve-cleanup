<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.resolve.util.JspUtils" %>
<%@page import="org.owasp.esapi.ESAPI"%>
<%
    String ver = ESAPI.encoder().encodeForURL(JspUtils.getResolveVersion());

    boolean debug = false;
    String d = request.getParameter("debug");
    if( d != null && d.indexOf("t") > -1){
        debug = true;
    }

    boolean debugWiki = false;
    String dW = request.getParameter("debugWiki");
    if( dW != null && dW.indexOf("t") > -1){
        debugWiki = true;
    }

    boolean mock = false;
    String m = request.getParameter("mock");
    if( m != null && m.indexOf("t") > -1){
        mock = true;
    }
%>
<script type="text/javascript">
    mxBasePath = '/resolve/jsp/model/mxBase';
    Ext.BLANK_IMAGE_URL='/resolve/images/s.gif';
</script>
    
<script type="text/javascript" src="/resolve/jsp/model/mxBase/js/mxClient-min.js?_v=<%=ver%>"></script>
<link rel="stylesheet" type="text/css" href="/resolve/jsp/model/css/grapheditor.css?_v=<%=ver%>" />
<style type="text/css">
    .diff{
        width:100%;
    }
    .diff tbody th{
        width:5%;
    }
    .diff tbody td{
        width:45%;
    }
    .draggedTaskOnTheTopOfIf{
        border-top-color: black;
    }
    .draggedTaskOnTheTop:first-child{
        border-top: 3px solid black !important;
        border-bottom: none;
    }
    .draggedTaskAtTheBottom{
        border-bottom: 3px solid black !important;
        border-top: none;
    }
</style>

<%
    if( debug || debugWiki ){
%>
<link rel="stylesheet" type="text/css" href="/resolve/wiki/css/wiki-all.css?_v=<%=ver%>"/>
<script type="text/javascript" src="/resolve/wiki/js/wiki-classes.js?_v=<%=ver%>"></script>
<%
    }else{
%>
<link rel="stylesheet" type="text/css" href="/resolve/wiki/css/wiki-all.css?_v=<%=ver%>"/>
<script type="text/javascript" src="/resolve/wiki/js/wiki-classes.js?_v=<%=ver%>"></script>
<%
    }
%>