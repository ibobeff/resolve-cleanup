<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title id="page-title">Resolve Template Project</title>
    <link rel="stylesheet" type="text/css" href="/resolve/css/font-awesome.min.css"/>
    <style>

        body{
            height:100%;
            width:100%;
            color:#777 !important;
            font-family: 'lucida console' !important;
            line-height: 1.5;
            font-size: 13px;
            margin:0;
        }
        .format{
            color:#ccc;
            z-index: 0;
        }
        .return{
            color:#ccc;
        }
        .literal{
            color:#000;
        }
        .literal .format{
            color:#000;
        }
        .capture{
        }
        .emphasize-left{
            border-left:1px solid #99CCFF !important;
        }
        .emphasize-center{
            background-color: #1F6DFF !important;
            color:white;
        }
        .emphasize-right{
            border-right:1px solid #99CCFF !important;
        }
        .emphasize-right .return:last-of-type{
            border-right: 1px solid #99CCFF;
        }
        .mark{
            font-weight: bold;
        }
        .beginParse{
            color: #C674FC;   
            background-color: #FDF0FF;
            border-left: 4px solid #C674FC;
        }
        .beginParse-mixed{
            color: #C674FC;   
            background-color: #FDF0FF;
        }
        .endParse{
            color: #C674FC;   
            background-color: #FDF0FF;
            border-right: 4px solid #C674FC;
        }
        .endParse .return:last-of-type{
            color: #C674FC !important;
            background-color: #FDF0FF !important;
            border-right: 4px solid #C674FC !important;
        }
        .type{
            border-bottom: 1px solid #FF6600;
        }
        .parseFromEndOfLine{
            color: #C674FC;
            font-size: small;
        }
        .stopParseAtStartOfLine{
            color: #C674FC;   
            font-size: small;
        }
        .beginOfLine::after{
            content: '{';
            color:#FF6600;
            font-weight: bold;
        }
        .endOfLine::after{
            content: '}';
            color:#FF6600;
            font-weight: bold;
        }
        .column{
            background-color: #ADD6F7;
        }
        .overlap-left{
            border-left:2px dashed #99CCFF !important;
        }
        .overlap-center{
            border-top: 2px dashed #99CCFF !important;
            border-bottom: 2px dashed #99CCFF !important;
        }
        .overlap-right{
            border-right:2px dashed #99CCFF !important;
        }
        .overlap-right .return:last-child{
            border-right: 2px dashed #99CCFF;
        }
        .tag{
            color:#33CC8F;
        }
        .tagName{
            color:#33CC8F;
        }
        .attribute{
            color:black;
        }
        .attrName{
            color:#CF37ED;
        }
        .attrValue{
            color:#0095FF;
        }
        .space::after{
            color:#ccc;
            content:'.';
        }
        .text{
            color:black;
        }
        .cdata{
            color:black;
        }
        .cdata::before{
            color:#999;
            content: '<![CDATA['
        }
        .cdata::after{
            color:#999;
            content: ']]>';
        }
        .newline::before{
            color:#ccc;
            content:'<';
        }

        .emphasize-xml{
            background-color:#1F6DFF !important;
            color:white !important;
        }
        .emphasize-xml * {
            color:white !important;   
        }
        .rootText{
            color:#999;
            text-decoration: line-through;
        }
    </style>
</head>
<body></body>
</html>
