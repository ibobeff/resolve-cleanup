<!-- ******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
********************************************************************************* -->

<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="org.owasp.esapi.ESAPI"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.resolve.util.JspUtils" %>
<%
    String ver = ESAPI.encoder().encodeForURL(JspUtils.getResolveVersion());

    boolean debug = false;
    String d = request.getParameter("debug");
    if( d != null && d.indexOf("t") > -1){
        debug = true;
    }

    boolean mock = false;
    String m = request.getParameter("mock");
    if( m != null && m.indexOf("t") > -1){
        mock = true;
    }

    String selectHelp = request.getParameter("selectHelp");
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title id="page-title">Wiki Help</title>

    <jsp:include page="../client/common.jsp" flush="true" />
    <jsp:include page="loader.jsp" flush="true" />
    
	<script src="/resolve/JavaScriptServlet?_v=<%=ver%>"></script>
    <script type="text/javascript">
        Ext.onReady(function() {
            Ext.tip.QuickTipManager.init();
            glu.viewport({
                mtype : 'RS.wiki.SourceHelp',
                mock: <%=mock%>,
                selectHelp: '<%=ESAPI.encoder().decodeForHTML(ESAPI.encoder().encodeForHTML(selectHelp))%>'
            });
        });
    </script>
    
</head>
    <body>
    </body>
</html>
