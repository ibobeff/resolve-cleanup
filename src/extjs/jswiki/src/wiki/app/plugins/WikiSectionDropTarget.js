/**
 * Plugin that sets up a DropZone for dragged items to be dropped on
 * Provides a mask to indicate where the component will be added when dropped on the panel
 */
Ext.define('RS.wiki.plugins.WikiSectionDropTarget', {

	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.wikisectiondroptarget',

	ddGroup: 'wikiSections',
	targetCls: 'sectionContainer',

	init: function(panel) {
		panel.on('render', this.onPanelRender, this, {
			single: true
		});
	},

	onPanelRender: function(panel) {
		var me = this;

		new Ext.dd.DropTarget(panel.getEl(), {
			ddGroup: me.ddGroup,

			//Gets called continuously as the mouse moves, so as we get to a new node, we move the proxy to that new position
			notifyOver: function(dd, e, data) {
				var node = e.getTarget('div[class*=' + me.targetCls + ']');
				if (node) {
					try {
						data.proxyPanel.show();
					} catch (e) {}
					data.nextSibling = node.nextSibling;
					data.proxyPanel.moveProxy(node.parentNode, node.nextSibling);
				}
				return Ext.dd.DropZone.prototype.dropAllowed;
			},

			//Gets called when the item is dropped.  Calculates the indexes for start, end, or a new item and passes that to the
			//viewmodel for processing by firing the itemdropped event that the viewmodel is hooked into
			notifyDrop: function(source, e, data) {
				var i, controlsPanel = panel,
					dropIndex = controlsPanel ? controlsPanel.items.length : 0,
					itemLen = dropIndex,
					sourceIndex = -1,
					controlConfig;

				for (i = 0; i < itemLen; i++) {
					if (controlsPanel.items.items[i].id == data.sourceEl.id) sourceIndex = i;
					if (data.nextSibling && data.nextSibling.nextSibling && data.nextSibling.nextSibling.id == controlsPanel.items.items[i].id)
						dropIndex = i;
				}

				data.proxyPanel.hide();

				if (sourceIndex == -1)
					controlConfig = Ext.getCmp(data.sourceEl.id).value;

				if ((sourceIndex >= 0 || controlConfig) && dropIndex >= 0) panel.fireEvent('itemDropped', panel, dropIndex, sourceIndex, controlConfig);

				return true;
			}
		});
	}
});