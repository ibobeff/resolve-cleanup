/**
 * Plugin that makes sections within the wiki builder dragable
 * Any component that has the css class sectionContainer (like all the fields have) will be reorderable in the wiki builder
 */
Ext.define('RS.wiki.plugins.WikiSectionDragZone', {

	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.wikisectiondragzone',

	ddGroup: 'wikiSections',
	targetCls: '.sectionContainer',

	init: function(panel) {
		panel.on('render', this.onPanelRender, this, {
			single: true
		});
	},

	onPanelRender: function(panel) {
		var me = this;
		new Ext.dd.DragZone(panel.getEl(), {
			ddGroup: me.ddGroup,

			//Gets the drag data information by finding the drag targets based on their css class
			getDragData: function(e) {
				var sourceEl = e.getTarget(me.targetCls, 9);
				if (sourceEl) {
					var d = sourceEl.cloneNode(true);
					d.id = Ext.id();
					return {
						ddel: d,
						sourceEl: sourceEl,
						proxyPanel: new Ext.panel.Proxy(Ext.getCmp(sourceEl.id), {}),
						repairXY: Ext.fly(sourceEl).getXY()
					}
				}
			},

			//Provide coordinates for the proxy to slide back to on failed drag.
			//This is the original XY coordinates of the draggable element captured
			//in the getDragData method.
			getRepairXY: function() {
				return this.dragData.repairXY;
			}
		});
	}
});