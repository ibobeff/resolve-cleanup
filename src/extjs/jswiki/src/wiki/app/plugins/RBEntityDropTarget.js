/**
 * Plugin that sets up a DropZone for dragged items to be dropped on
 * Provides a mask to indicate where the component will be added when dropped on the panel
 */
Ext.define('RS.wiki.plugins.RBEntityDropTarget', {

	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.rbentitydroptarget',

	init: function(panel) {
		panel.on('render', this.onPanelRender);
	},

	onPanelRender: function(panel) {
		var me = this;

		panel.on('mouseleave', function() {
			this.notifyWatcherDDLeave()
		}, this);

		Ext.create('Ext.dd.DropZone', panel.el, {
			ddGroup: 'abEntities',
			//      If the mouse is over a target node, return that node. This is
			//      provided as the "target" parameter in all "onNodeXXXX" node event handling functions
			getTargetFromEvent: function(e) {
				return e.target;
			},

			//      While over a target node, return the default drop allowed class which
			//      places a "tick" icon into the drag proxy.
			lastEl: null,
			compute: function(e, dragWatcherEl) {
				if (this.lastEl && (this.lastEl != dragWatcherEl)) {
					this.lastEl.abCmp.fireEvent('taskLeave');
				}
				this.lastEl = dragWatcherEl;

				var dragWatcher = dragWatcherEl.abCmp;
				var top = dragWatcher.getY();
				var mid = dragWatcher.getY() + dragWatcher.getHeight() / 2;
				var bottom = dragWatcher.getY() + dragWatcher.getHeight();

				if (e.getY() >= top && e.getY() < mid) {
					dragWatcher.fireEvent('taskUpperHalf');
					return;
				}
				if (e.getY() >= mid && e.getY() <= bottom) {
					dragWatcher.fireEvent('taskLowerHalf');
					return;
				}
			},
			onNodeOver: function(target, dd, e, data) {
				if (isNaN(e.getY()))
					return;
				var isAllowed = Ext.dd.DropZone.prototype.dropNotAllowed;
				// the order matters
				var queue = [{
					selector: '.taskEntry',
					isAllowed: 'dropAllowed'
				}, {
					selector: '.runbookEntry',
					isAllowed: 'dropAllowed'
				}, {
					selector: '.conditionEntry',
					isAllowed: 'dropAllowed'
				}, {
					selector: '.indicator',
					isAllowed: 'dropAllowed'
				}, {
					selector: '.ifEntry',
					isAllowed: 'dropAllowed'
				}, {
					selector: '.connectionEntry',
					isAllowed: 'dropAllowed'
				}, {
					selector: '.loopEntry',
					isAllowed: 'dropNotAllowed'
				}];
				var t = null;
				for (var i = 0; i < queue.length; i++) {
					t = e.getTarget(queue[i].selector, panel.getEl().parent());
					if (!Ext.isEmpty(t)) {
						this.compute(e, t);
						//console.info(queue[i].selector + '__' + e.getTarget());
						return Ext.dd.DropZone.prototype[queue[i].isAllowed];
					}
				}
				//console.info(e.getTarget());
				return isAllowed;
			},

			onNodeDrop: function(target, dd, e, data) {
				var queue = ['.conditionEntry', '.connectionEntry'];
				var t = null;
				for (var i = 0; i < queue.length; i++) {
					t = e.getTarget(queue[i], panel.getEl().parent());
					if (!Ext.isEmpty(t)) {
						t.abCmp.fireEvent('accept', t.abCmp, data);
						break;
					}
				}
				this.notifyWatcherDDLeave();
			},

			notifyWatcherDDLeave: function() {
				var watchers = panel.query('*[watch]');
				for (var i = 0; i < watchers.length; i++) {
					watchers[i].fireEvent('taskLeave');
					delete watchers[i].upperHalf;
				}
			}
		});
	}
});