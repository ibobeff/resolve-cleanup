Ext.define('RS.wiki.plugins.AutomationModelDragZone', {

	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.automationmodeldragzone',
	graph: null,
	editorPanel: null,
	init: function(panel) {
		if (!panel.customImg) {
			panel.on('afterrender', this.onPanelRender, this);
			panel.on('beforedestroy', this.onBeforePanelDestroy, this);
			panel.on('grapheditorready', function (dragzonePanel, graph, editorPanel) {
				this.graph = graph;
				this.editorPanel = editorPanel;
			}, this);
		}
	},

	onPanelRender: function(panel, graph) {
		var me = this;
		var dragPreview = document.createElement('div');
		dragPreview.style.border = 'dashed black 1px';
		dragPreview.style.width = panel.elementWidth + 'px';
		dragPreview.style.height = panel.elementHeight + 'px';
		if(!this.editorPanel.viewOnly) {
			var dragSource = mxUtils.makeDraggable(panel.getEl().dom, this.graph, function(graph, evt, target) {
					me.editorPanel.fireEvent('elementDrop', panel, evt, target);
				}, dragPreview, -panel.elementWidth / 2, -panel.elementHeight / 2,
				this.graph.autoscroll, false);
		}
	},
	onBeforePanelDestroy : function(panel){
	//mxUtils.makeDraggable  attachs mxEvent behind the scene on this element		
		if(this.graph)
			mxEvent.removeAllListeners(panel.getEl().dom);
	}	
});

Ext.define('RS.wiki.plugins.AutomationConnectionDragZone', {

	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.automationconnectiondragzone',
	graph: null,
	editorPanel: null,
	init: function(panel) {
		panel.on('afterrender', this.onPanelRender, this);
		panel.on('beforedestroy', this.onBeforePanelDestroy, this);
		panel.on('grapheditorready', function(dragzonePanel, graph, editorPanel) {
			this.graph = graph;
			this.editorPanel = editorPanel;
		}, this)
	},

	onPanelRender: function(panel, graph) {
		var me = this;
		var dragPreview = document.createElement('div');
		dragPreview.style.border = 'dashed black 1px';
		dragPreview.style.width = panel.elementWidth + 'px';
		dragPreview.style.height = panel.elementHeight + 'px';
		if(!this.editorPanel.viewOnly) {
			var dragSource = mxUtils.makeDraggable(panel.getEl().dom, this.graph, function(graph, evt, target) {
					me.editorPanel.fireEvent('connectionDrop', panel, panel.connectionType, evt, graph);
				}, dragPreview, -panel.elementWidth / 2, -panel.elementHeight / 2,
			this.graph.autoscroll, false);
		}
	},
	onBeforePanelDestroy : function(panel){
		//mxUtils.makeDraggable  attachs mxEvent behind the scene on this element
		if(this.graph)
			mxEvent.removeAllListeners(panel.getEl().dom);
	}	
});