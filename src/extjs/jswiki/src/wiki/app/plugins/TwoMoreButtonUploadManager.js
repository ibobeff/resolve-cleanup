/*
Ext.define('RS.wiki.JustTwoMoreButtonUploadManager', {
	extend: 'RS.formbuilder.viewer.desktop.fields.UploadManager',
	alias: 'widget.twomorebuttonuploadmanager',
	initComponent: function() {
		this.callParent();
		Ext.each(this.columns, function(cols) {
			if (cols.dataIndex == 'name') {
				cols.renderer = function(value, metaData, record) {
					var name = record.get(me.nameColumn) || record.get('name')
					if (record.raw.global)
						name += ' <span style="font-weight:600;">(Global)</span>';
					return name;
				}
			}
		});
		var me = this;
		var toolbar = this.down('button[name="uploadFile"]').ownerCt;
		if (this.isEditMode) {
			toolbar.add([
				{
					text: RS.wiki.locale.Main.renameAttachment || '~~renameAttachment~~',
					name: 'renameAttachment',
					tooltip: RS.wiki.locale.Main.renameAttachmentTooltip || '~~renameAttachmentTooltip~~',
					handler: function() {
						me.fireEvent('renameAttachment', this, me.getSelectionModel().getSelection()[0], me);
					}
				}, {
					text: RS.wiki.locale.Main.referGlobalAttachments || '~~referGlobalAttachments~~',
					name: 'refereGlobal',
					tooltip: RS.wiki.locale.Main.referGlobalAttachmentsTooltip || '~~referGlobalAttachmentsTooltip~~',
					handler: function() {
						me.fireEvent('referGlobalAttachments', this, me);
					}
				}
			]);
			this.on({
				render: function() {
					this.down('button[name="renameAttachment"]').disable();
					// this.down('button[name="markAsGlobal"]').disable();
				},
				selectionchange: function(selectionModel, selections, eOpts) {
					if (selections.length == 1) {
						this.down('button[name="renameAttachment"]').enable();
					}
					// if (selections.length > 0) {
					// 	this.down('button[name="markAsGlobal"]').enable();
					// }
					if (selections.length == 0) {
						this.down('button[name="renameAttachment"]').disable();
						// this.down('button[name="markAsGlobal"]').disable();
					}
				}
			});
		}
		toolbar.add('->', {
			iconCls: 'x-tbar-loading',
			tooltip: RS.wiki.locale.Main.refresh || '~~refresh~~',
			handler: function() {
				var manager = this.up().up();
				manager.loadFileUploads(true);
				this.fireEvent('refresh', this);
			}
		});
	},

	loadFileUploads: function(forced) {
		this.getSelectionModel().deselectAll();
		if (this.documentName && this.documentName.length > 1) {
			if (!!forced || this.documentName.toLowerCase() != this.lastDocumentName) {
				this.lastDocumentName = this.documentName.toLowerCase()
				Ext.Ajax.request({
					url: this.api && this.api.list? this.api.list: '/resolve/service/wiki/getAttachments',
					params: {
						docSysId: '',
						docFullName: this.documentName
					},
					success: this.loadFiles,
					scope: this
				})
			}
		} else {
			Ext.Ajax.request({
				url: '/resolve/service/dbapi/getRecordData',
				params: {
					sqlQuery: Ext.String.format("select * from {0} where {1}='{2}'", this.fileUploadTableName, this.referenceColumnName, this.recordId),
					type: 'table',
					useSql: true
				},
				success: this.loadFiles,
				scope: this
			})
		}
	}
});
*/
