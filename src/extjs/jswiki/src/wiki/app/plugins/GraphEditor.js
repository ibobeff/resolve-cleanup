Ext.define('RS.wiki.plugins.GraphEditor', {

	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.resolvegrapheditor',
	graph: null,
	fontFamily: 'Helvetica',
	fontSize: 11,
	updatingProperty: false,
	init: function(panel) {
		panel.on('afterrender', this.onPanelRender, this);
		panel.on('beforedestroy', this.onBeforePanelDestroy, this);
		panel.on('elementDrop', function(elementPanel, evt, target) {
			var name = elementPanel.elementType;
			if (['straight', 'horizontal', 'vertical'].indexOf(name) != -1) {
				this.addEdge(elementPanel, evt, target);
			} else {
				this.addVertex(elementPanel, evt, target, panel);
			}
		}, this);
		panel.on('modelXmlChanged', function() {
			if (panel.modelXml.shouldUpdateModel)
				this.loadXml(panel.modelXml.xml);
		}, this);
		panel.on('cellStyleFormatChanged', function(cellStyleFormat) {
			if (cellStyleFormat.fontFamily) {
				this.fontFamily = cellStyleFormat.fontFamily;
			}
			if (cellStyleFormat.fontSize) {
				this.fontSize = cellStyleFormat.fontSize;
			}
		}, this);
		mxClient.IS_SVG = true;
		panel.on('taskdependencychanged', function(cell, dependency) {
			var graph = this.graph;
			var styleVar = cell.style;
			var mergeSelect = cell.getValue().getAttribute('merge');
			var styleVarUpdate = '';
			switch (dependency) {
				case 'none':
					if (styleVar.indexOf('dashed=1') > 0) {
						styleVarUpdate = styleVar.substring(0, styleVar.indexOf('dashed=1'));
						styleVarUpdate += styleVar.substring(styleVar.indexOf('dashed=1') + 'dashed=1'.length, styleVar.length);
						if (styleVarUpdate != '' && styleVarUpdate.length > 0) {
							cell.setStyle(styleVarUpdate);
							graph.refresh();
						}
					}
					cell.getValue().removeAttribute('merge');
					break;
				case 'all':
					if (styleVar.indexOf('dashed=1') > 0) {
						styleVarUpdate = styleVar.substring(0, styleVar.indexOf('dashed=1'));
						styleVarUpdate += styleVar.substring(styleVar.indexOf('dashed=1') + 'dashed=1'.length, styleVar.length);
						if (styleVarUpdate != '' && styleVarUpdate.length > 0) {
							cell.setStyle(styleVarUpdate);
							graph.refresh();
						}
					}
					cell.setAttribute('merge', 'merge = ALL');
					break;
				case 'any':
					if (styleVar.indexOf('dashed=1') < 0) {
						var lastChar = styleVar.substring(styleVar.length - 1, styleVar.length);
						if (lastChar != ';') {
							styleVarUpdate = styleVar + ';dashed=1';
						} else {
							styleVarUpdate = styleVar + 'dashed=1';
						}
						if (styleVarUpdate != '' && styleVarUpdate.length > 0) {
							cell.setStyle(styleVarUpdate);
							graph.refresh();
						}
					}
					cell.setAttribute('merge', 'merge = ANY');
					break;
				case 'targets':
					if (styleVar.indexOf('dashed=1') > 0) {
						styleVarUpdate = styleVar.substring(0, styleVar.indexOf('dashed=1'));
						styleVarUpdate += styleVar.substring(styleVar.indexOf('dashed=1') + 'dashed=1'.length, styleVar.length);
						if (styleVarUpdate != '' && styleVarUpdate.length > 0) {
							cell.setStyle(styleVarUpdate);
							graph.refresh();
						}
					}
					cell.setAttribute('merge', 'merge = TARGETS');
					break;
			}
			graph.getModel().fireEvent(new mxEventObject(mxEvent.CHANGE, "edit", {
				changes: []
			}));
		});
		panel.on('edgeexecutiondependencychanged', function(executionDependency) {
			var graph = this.graph;
			switch (executionDependency) {
				case 'required':
					graph.setCellStyles(mxConstants.STYLE_DASHED);
					break;
				case 'notRequired':
					graph.setCellStyles(mxConstants.STYLE_DASHED, true);
					break;
			}
			graph.getModel().fireEvent(new mxEventObject(mxEvent.CHANGE, "edit", {
				changes: []
			}));
		});
		//if u use setCellStyles...it will just append the style after the current style string...but the css rule 'vertical' seems to have effect on line color...u need to put it in front of the color rule...suprise~~
		panel.on('edgeconditiondependencychanged', function(conditionDependency) {
			var graph = this.graph;
			switch (conditionDependency) {
				case 'good':
					graph.setCellStyles('strokeColor', 'green');
					break;
				case 'bad':
					graph.setCellStyles('strokeColor', 'red');
					break;
				case 'none':
					graph.setCellStyles('strokeColor', 'black');
					break;
			}
			graph.getModel().fireEvent(new mxEventObject(mxEvent.CHANGE, "edit", {
				changes: []
			}));
		});
		panel.on('connectorchanged', function(connector) {
			var graph = this.graph;
			var styles = this.graph.getSelectionCell().getStyle().split(';');
			styles = Ext.Array.filter(styles, function(item, idx) {
				return ['noEdgeStyle=1', 'straight', 'vertical', 'horizontal'].indexOf(item) == -1;
			});
			switch (connector) {
				case 'straight':
					styles.push('straight');
					styles.push('noEdgeStyle=1');
					break;
				case 'horizontal':
					styles.push('horizontal');
					break;
				case 'vertical':
					styles.push('vertical');
					break;
			}
			this.graph.getSelectionCell().setStyle(styles.join(';'));
			graph.getModel().fireEvent(new mxEventObject(mxEvent.CHANGE, "edit", {
				changes: []
			}));
			this.graph.refresh();
		});
		panel.on('dtselecttypechanged', function(cell, selectType) {
			var graph = this.graph;
			switch (selectType.toLowerCase()) {
				case 'radio':
					cell.setAttribute('select', selectType.toUpperCase());
					break;
				case 'dropdown':
					cell.setAttribute('select', selectType.toUpperCase());
					break;
			}
			graph.getModel().fireEvent(new mxEventObject(mxEvent.CHANGE, "edit", {
				changes: []
			}));
			this.graph.refresh();
		});
		panel.on('dtrecommendationchanged', function(cell, text){
			this.updatingProperty = true;
			var elt = cell.value.cloneNode(true);
			elt.setAttribute('recommendation', text);
			this.graph.getModel().setValue(cell, elt);
			this.updatingProperty = false;
		},this)
		panel.on('celllabelchanged', function(cell, label) {
			this.updatingProperty = true;
			var elt = cell.value.cloneNode(true);
			elt.setAttribute('label', label);
			this.graph.getModel().setValue(cell, elt);
			this.updatingProperty = false;
		}, this);
		panel.on('cellversionchanged', function(cell, version) {
			this.updatingProperty = true;
			var elt = cell.value.cloneNode(true);
			if (version == 'latest') {
				elt.removeAttribute('version');
			} else {
				elt.setAttribute('version', version);
			}
			this.graph.getModel().setValue(cell, elt);
			this.updatingProperty = false;
		}, this);
		panel.on('cdatachanged', function(cell) {
			this.updatingProperty = true;
			var elt = cell.value.cloneNode(true);
			this.graph.getModel().setValue(cell, elt);
			this.updatingProperty = false;
		}, this);
		panel.on('elementpropertychanged', function(cell, property, dataStr) {
			var valueXml = cell.getValue();
			valueXml.setAttribute(property, dataStr);
			this.graph.getModel().fireEvent(new mxEventObject(mxEvent.CHANGE, "edit", {
				changes: ['resolvemodeledit']
			}));
		}, this);
		panel.on('dtanswerorderchanged', function(cell, order) {
			cell.setAttribute('order', order);
			this.graph.getModel().fireEvent(new mxEventObject(mxEvent.CHANGE, "edit", {
				changes: ['resolvemodeledit']
			}));
		}, this);
		// panel.on('entityexistence', function(cell, existence) {

		// 	if (existence)
		// 		cell.setStyle('strokeColor=#A9A9A9');
		// 	else
		// 		cell.setStyle('strokeColor=red');
		// 	panel.graph.refresh();
		// }, this);
	},
	onPanelRender: function(panel) {
		var me = this;
		// Disables browser context menu
		mxEvent.disableContextMenu(panel.getEl().dom);

		// Makes the connection are smaller
		mxConstants.DEFAULT_HOTSPOT = 0.3;
		// Creates the graph and loads the default stylesheet
		var graph = new mxGraph();
		mxGraph.prototype.convertValueToString = function(cell) {
			var value = this.model.getValue(cell);
			if (value != null) {
				if (mxUtils.isNode(value)) {
					var lableValue = cell.getAttribute('label');
					if (lableValue != null) {
						return lableValue;
					} else {
						if (value.nodeName != 'Edge') {
							return value.nodeName;
						}
					}
				} else if (typeof(value.toString) == 'function') {
					return value.toString();
				}
			}
			return '';
		};
		graph.panningHandler.ignoreCell = true;
		graph.useScrollbarsForPanning = false;
		graph.labelChanged = function(cell, newValue, trigger) {
			if (cell != null && newValue != null) {
				var elt = cell.value.cloneNode(true);
				elt.setAttribute('label', newValue);
				graph.model.setValue(cell, elt);
				panel.fireEvent('labelChangedInline', panel, cell);
			}
		};
		// Creates the command history (undo/redo)
		var history = new mxUndoManager();
		panel.graph = graph;
		panel.history = this.history = history;
		// Installs the command history after the initial graph
		// has been created
		this.undoEventHandler = function(sender, evt) {
			if (!this.updatingProperty) {
				history.undoableEditHappened(evt.getProperty('edit'));
			}
		}.bind(this),
		graph.getModel().addListener(mxEvent.UNDO, this.undoEventHandler);
		graph.getView().addListener(mxEvent.UNDO, this.undoEventHandler);

		this.modelChangeEventHandler = function(model, evt) {
			var enc = new mxCodec(mxUtils.createXmlDocument());
			var node = enc.encode(graph.getModel());
			var cdatas = {};

			function prettyXml(node, tab, indent) {
				var result = [];
				if (node != null) {
					tab = tab || '  ';
					indent = indent || '';
					if (node.nodeType == mxConstants.NODETYPE_TEXT) {
						result.push(indent + Ext.String.trim(node.nodeValue) + '\n');
					} else if (node.nodeType == mxConstants.NODETYPE_CDATA) {
						result.push(indent + Ext.String.trim('<![CDATA[' + node.nodeValue + ']]>') + '\n');
					} else {
						result.push(indent + '<' + node.nodeName);

						var attrs = node.attributes;
						if (attrs != null) {
							for (var i = 0; i < attrs.length; i++) {
								var val = mxUtils.htmlEntities(attrs[i].nodeValue);
								result.push(' ' + attrs[i].nodeName + '="' + val + '"');
							}
						}


						var tmp = node.firstChild;

						if (tmp != null) {
							result.push('>\n');
							while (tmp != null) {
								if (!/^\s*$/g.test(tmp.nodeValue))
									result.push(prettyXml(tmp, tab, indent + tab));
								tmp = tmp.nextSibling;
							}
							result.push(indent + '</' + node.nodeName + '>\n');
						} else {
							result.push('/>\n');
						}
					}
				}
				return result.join('');
			}
			
			// By default, a prettyXml node will contain:
			//  <mxGraphModel>
			//    <root>
			//	    <mxCell id="0"/>
			//	    <mxCell id="1" parent="0"/>
			//    </root>
			//  </mxGraphModel>
			var pretty = prettyXml(node);
			if ((!Object.keys(panel._vm.modelXml).length) || (panel._vm.modelXml.xml != '') ||
				(encodeURI(pretty) != '%3CmxGraphModel%3E%0A%20%20%3Croot%3E%0A%20%20%20%20%3CmxCell%20id=%220%22/%3E%0A%20%20%20%20%3CmxCell%20id=%221%22%20parent=%220%22/%3E%0A%20%20%3C/root%3E%0A%3C/mxGraphModel%3E%0A'))
			{
				panel.fireEvent('modelchanged', panel, pretty);
			} else {
				// if prettyXml node contains the default model and the modelXml's xml is blank, just render blank xml
				panel.fireEvent('modelchanged', panel, '');
			}
		},
		graph.getModel().addListener(mxEvent.CHANGE, this.modelChangeEventHandler);

		this.modelSplitEdgeEventHandler = function(model, evt) {
			panel.fireEvent('splitEdge');
		}
		graph.addListener(mxEvent.SPLIT_EDGE, this.modelSplitEdgeEventHandler);

		this.cellSelectionChangeEventHandler = function(sender, evt) {
			!graph.skipNotifyCellSelectionChange? panel.fireEvent('cellselectionchange', graph, sender.cells): undefined;
			panel.fireEvent('updateControls');
		},
		graph.getSelectionModel().addListener(mxEvent.CHANGE, this.cellSelectionChangeEventHandler);
		graph.getTooltip = function(cellstate, svgElement, x, y) {
			return cellstate.cell;
		};
		var show = graph.tooltipHandler.show;
		var task;
		graph.tooltipHandler.show = function(cell, x, y) {
			if (task)
				task.cancel();
			task = new Ext.util.DelayedTask(function() {
				var me = this;
				panel.fireEvent('beforeshowcelltooltip', panel, cell, function(tip) {
					show.call(me, (cell.id ? '[' + cell.id + '] ' : '') + Ext.String.htmlEncode(tip || cell.getAttribute('label') || ''), x, y);
				});
			}, this);
			task.delay(500);
		};
		// Keeps the selection in sync with the history
		var undoHandler = function(sender, evt) {
			graph.skipNotifyCellSelectionChange = true;
			var changes = evt.getProperty('edit').changes;
			graph.setSelectionCells(graph.getSelectionCellsForChanges(changes));
			graph.skipNotifyCellSelectionChange = false;
		};
		history.addListener(mxEvent.UNDO, undoHandler);
		history.addListener(mxEvent.REDO, undoHandler);
		mxGraphHandler.prototype.guidesEnabled = true;
		mxConnectionHandler.prototype.connectImage = new mxImage('/resolve/jsp/model/images/connector.gif', 16, 16);
		var isSplitTarget = mxGraph.prototype.isSplitTarget
		graph.isSplitTarget = function(target, cells, evt) {
			var flag = isSplitTarget.call(this, target, cells, evt);
			if (!flag)
				return false;
			var hasImage = false;
			var hasContainer = false;
			Ext.each(cells, function(c) {
				if (!c.getValue())
					return
				if (c.getValue().tagName.toLowerCase() == 'image')
					hasImage = true;
				if (c.getValue().tagName.toLowerCase() == 'container')
					hasContainer = true;
			});
			var sourceType = target.source ? target.source.getValue().tagName.toLowerCase() : '';
			return !hasContainer && (sourceType == 'image' && hasImage || sourceType != 'image' && !hasImage)

		}
		mxConnectionHandler.prototype.isConnectableCell = function(cell) {
			if (!cell)
				return false;
			var value = cell.getValue();
			if (!value)
				return false
			if (['task', 'start', 'end', 'subprocess', 'document', 'precondition', 'event', 'question', 'image'].indexOf(value.tagName.toLowerCase()) != -1)
				return true;
			return false;
		}
		mxConnectionHandler.prototype.validateConnection = function(source, target) {
			if (!source || !target)
				return null
			var sourceValue = source.getValue();
			var targetValue = target.getValue();
			if (!sourceValue || !targetValue)
				return null;
			if (source == target && (['document', 'question'].indexOf(sourceValue.tagName.toLowerCase()) != -1))
				return 'Document/Question cannot connect to itself.';

			if (sourceValue.tagName.toLowerCase() == 'image' && targetValue.tagName.toLowerCase() != 'image')
				return 'Image cannot connect to non-image element.';
			if (sourceValue.tagName.toLowerCase() != 'image' && targetValue.tagName.toLowerCase() == 'image')
				return 'Image cannot connect to non-image element.';
		}
		graph.connectionHandler.createTargetVertex = function(event, source) {
			var next = me.getNextCell(source);
			var config = {};
			if (next instanceof mxCell) {
				config.actionObject = next.getValue();
				config.cellStyle = next.getStyle();
				config.width = next.getGeometry().width;
				config.height = next.getGeometry().height;
			} else
				config = me.getCellConfig(me.getNextCell(source));
			var pt = graph.getPointForEvent(event);
			var parent = graph.getDefaultParent();
			var cell = graph.insertVertex(parent, null, config.actionObject, pt.x - config.width / 2 + 5, pt.y - config.height / 2, config.width, config.height, config.cellStyle);
			return cell;
		};
		graph.connectionHandler.factoryMethod = function() {
			var resolveEdge = new mxCell('');
			resolveEdge.setEdge(true);
			var geo = new mxGeometry();
			geo.relative = true;
			resolveEdge.setGeometry(geo);
			resolveEdge.setStyle('straight;noEdgeStyle=1');

			var edgeObject = mxUtils.createXmlDocument().createElement("Edge");
			edgeObject.setAttribute('label', "");
			edgeObject.setAttribute('description', "");
			resolveEdge.setValue(edgeObject);
			resolveEdge.setAttribute('order', 99999);
			return resolveEdge;
		};
		graph.init(panel.body.el.dom);
		// Initializes the graph as the DOM for the panel has now been created
		graph.setConnectable(true);
		graph.setDropEnabled(true);
		graph.setPanning(true);
		graph.setTooltips(true);

		graph.connectionHandler.setCreateTarget(true);
		// Sets the cursor
		graph.container.style.cursor = 'default';
		// Loads the default stylesheet into the graph
		var node = mxUtils.load('/resolve/jsp/model/resources/default-style.xml').getDocumentElement();
		var dec = new mxCodec(node.ownerDocument);
		dec.decode(node, graph.getStylesheet());
		// Sets the style to be used when an elbow edge is double clicked
		graph.alternateEdgeStyle = 'vertical';
		var rubberband = new mxRubberband(graph);
		graph.panningHandler.popup = mxUtils.bind(this, function(x, y, cell, evt) {
			panel.fireEvent('showelementmenu', graph, x, y, cell, evt);
		});

		if (panel.viewOnly) {
			graph.setCellsEditable(false);
			graph.setCellsMovable(false);
			graph.setCellsResizable(false);
			graph.setConnectable(false);
			graph.setCellsDisconnectable(false);
		} else {
			if (mxClient.IS_NS)
			{
				// mxGraph Known Issues
				// https://jgraph.github.io/mxgraph/docs/known-issues.html
				mxEvent.addListener(graph.container, 'mousedown', function()
				{
					if (!graph.isEditing())
					{
						graph.container.setAttribute('tabindex', '-1');
						graph.container.focus();
					}
				});
			}

			// Handles keystroke events
			var graphKeyHandler =  new mxKeyHandler(graph);
			this.graphKeyHandler = graphKeyHandler;
			// // Ignores enter keystroke. Remove this line if you want the
			// // enter keystroke to stop editing
			// graphKeyHandler.enter = function() {};

			// graphKeyHandler.bindKey(8, function() {
			// 	graph.foldCells(true);
			// });

			// graphKeyHandler.bindKey(13, function() {
			// 	graph.foldCells(false);
			// });

			// graphKeyHandler.bindKey(33, function() {
			// 	graph.exitGroup();
			// });

			// graphKeyHandler.bindKey(34, function() {
			// 	graph.enterGroup();
			// });

			// graphKeyHandler.bindKey(36, function() {
			// 	graph.home();
			// });

			// graphKeyHandler.bindKey(35, function() {
			// 	graph.refresh();
			// });

			// graphKeyHandler.bindKey(37, function() {
			// 	graph.selectPreviousCell();
			// });

			// graphKeyHandler.bindKey(38, function() {
			// 	graph.selectParentCell();
			// });

			// graphKeyHandler.bindKey(39, function() {
			// 	graph.selectNextCell();
			// });

			// graphKeyHandler.bindKey(40, function() {
			// 	graph.selectChildCell();
			// });

			graphKeyHandler.bindKey(46, function() {
				panel.fireEvent('removeclicked', this, graph);
				/*
				graph.removeCells();
				panel.fireEvent('updateControls');
				*/
			});

			graphKeyHandler.bindKey(107, function() {
				graph.zoomIn();
			});

			 graphKeyHandler.bindKey(109, function() {
			 	graph.zoomOut();
			 });

			// graphKeyHandler.bindKey(113, function() {
			// 	graph.startEditingAtCell();
			// });

			graphKeyHandler.bindControlKey(65, function() {
				graph.selectAll();
			});

			graphKeyHandler.bindControlKey(89, function() {
				if (history.canRedo()) {
					graph.skipNotifyCellSelectionChange = true;
					history.redo();
					graph.skipNotifyCellSelectionChange = false;
					panel.fireEvent('updateControls');
				}
			});

			graphKeyHandler.bindControlKey(90, function() {
				if (history.canUndo()) {
					graph.skipNotifyCellSelectionChange = true;
					history.undo();
					panel.fireEvent('updateControls');
					graph.skipNotifyCellSelectionChange = false;
				}
			});

			graphKeyHandler.bindControlKey(88, function() {
				graph.skipNotifyCellSelectionChange = true;
				mxClipboard.cut(graph);
				graph.skipNotifyCellSelectionChange = false;
				panel.fireEvent('updateControls');
			});

			graphKeyHandler.bindControlKey(67, function() {
				mxClipboard.copy(graph);
				panel.fireEvent('updateControls');
			});

			graphKeyHandler.bindControlKey(86, function() {
				if (mxClipboard.isEmpty() == false) {
					graph.skipNotifyCellSelectionChange = true;
					mxClipboard.paste(graph);
					graph.skipNotifyCellSelectionChange = false;
					panel.fireEvent('updateControls');
				}
			});

			// graphKeyHandler.bindControlKey(71, function() {
			// 	graph.setSelectionCell(graph.groupCells(null, 20));
			// });

			// graphKeyHandler.bindControlKey(85, function() {
			// 	graph.setSelectionCells(graph.ungroupCells());
			// });

			// graphKeyHandler.bindControlKey(97, function() {
			// 	debugger;
			// });
		}
		graph.cellEditor.getEditorBounds = function(state) {
			var result = mxCellEditor.prototype.getEditorBounds.apply(this, arguments);

			var maxWidth = Math.round(state.getCenterX() * 2);
			var maxHeight = Math.round(state.getCenterY() * 2);

			var value = state.cell.getValue();
			var tagName = value.tagName.toLowerCase();
			switch (tagName) {
				case 'task':
				case 'subprocess':
				case 'text':
					result.width = state.cell.geometry.width;
					result.height = state.cell.geometry.height;
					result.x = state.cell.geometry.x;
					result.y = state.cell.geometry.y;
					break;
				case 'container':
					result.width = state.cell.geometry.width;
					result.height = 30;
					result.x = state.cell.geometry.x;
					result.y = state.cell.geometry.y;
					break;
				case 'edge':
					result.x = state.getCenterX();
					result.y = Math.round(state.getCenterY() - result.height / 2);
					result.width = state.getCenterX();
					//break;
				default:
					if (result.width < 100) {
						result.width = 100;
					}
					result.height = 30;
					break;
			}

			// correction for EditorBound beyond canvas	size
			if (result.x < 0) result.x = 0;
			if (result.y < 0) result.y = 0;
			if (result.width > maxWidth) result.width = maxWidth;
			if (result.height > maxHeight) result.height = maxHeight;

			return result;
		};
		panel.on('resize', function() {
			graph.sizeDidChange();
		});
		this.graph = graph;
		panel.fireEvent('grapheditorready', panel, graph);
	},
	onBeforePanelDestroy : function(panel){
		var graph = panel.graph;
		if(graph){
			mxEvent.removeAllListeners(graph.container);
			graph.labelChanged = null;
			graph.tooltipHandler.show = null;
			graph.isSplitTarget = null;
			mxConnectionHandler.prototype.isConnectableCell = null;
			mxConnectionHandler.prototype.validateConnection = null;
			graph.connectionHandler.createTargetVertex = null;
			graph.connectionHandler.factoryMethod  = null;
			graph.panningHandler.popup = null;
			graph.cellEditor.getEditorBounds = null;
			graph.getTooltip = null;
			mxGraph.prototype.convertValueToString = null;
			graph.getModel().removeListener(this.undoEventHandler);
			graph.getView().removeListener(this.undoEventHandler);
			graph.getModel().removeListener(this.modelChangeEventHandler);
			graph.removeListener(this.modelSplitEdgeEventHandler);
			graph.getSelectionModel().removeListener(this.cellSelectionChangeEventHandler);
			if(!panel.viewOnly){
				this.graphKeyHandler.controlKeys = null;
				this.graphKeyHandler.normalKeys = null;
			}
		}
	},
	undoEventHandler : null,
	modelChangeEventHandler : null,
	modelSplitEdgeEventHandler : null,
	cellSelectionChangeEventHandler : null,
	graphKeyHandler : null,
	loadXml: function(modelXml) {
		if (!modelXml && this.graph)
			this.graph.getModel().clear();
		if (!modelXml || !this.graph || this.updatingProperty)
			return;
		this.graph.getModel().clear();
		var doc = mxUtils.parseXml(modelXml.replace(/\n/g, ''));
		var root = null;

		function getRoot(node) {
			if (node.tagName && node.tagName.toLowerCase() == 'root') {
				root = node;
				return;
			}

			Ext.each(node.childNodes, function(child) {
				getRoot(child);
			});
		}

		getRoot(doc);

		var list = [];
		Ext.each((root || {
			childNodes: []
		}).childNodes, function(child) {
			if (child.tagName && child.tagName.toLowerCase() == 'edge')
				list.push(child)
		})
		Ext.each(list, function(child) {
			root.removeChild(child);
			root.appendChild(child);
		});

		var dec = new mxCodec(doc.ownerDocument);
		dec.decode(doc.documentElement, this.graph.getModel());
		//this.history.clear();
	},
	addEdge: function(elementPanel, event) {
		var name = elementPanel.elementType;
		var cellStyle = null;
		var edgeObject = mxUtils.createXmlDocument().createElement("Edge");
		edgeObject.setAttribute('label', "");
		edgeObject.setAttribute('description', "");
		switch (name) {
			case 'straight':
				cellStyle = 'straight;noEdgeStyle=1';
				break;
			case 'horizontal':
				cellStyle = 'horizontal';
				break;
			case 'vertical':
				cellStyle = 'vertical';
				break;
			default:
				throw 'Unknown element!';
		}
		var graph = this.cmp.graph;
		var parent = graph.getDefaultParent();
		var from = graph.getPointForEvent(event);
		from.x = from.x + 50;
		from.y = from.y - 50;
		var to = graph.getPointForEvent(event);
		to.x = to.x - 50;
		to.y = to.y + 50;
		var edge = graph.createEdge(parent, null, edgeObject, null, null, cellStyle);
		edge.setAttribute('order', 99999);
		var geo = edge.getGeometry();

		geo.setTerminalPoint(from, true);
		geo.setTerminalPoint(to);
		graph.addEdge(edge);
	},
	getCellConfig: function(name, icon) {
		var actionObject = null;
		var cellStyle = null;
		var width = 100;
		var height = 40;
		var connectable = true;
		switch (name) {
			case 'start':
				actionObject = mxUtils.createXmlDocument().createElement("Start");
				actionObject.setAttribute('label', "Start");
				actionObject.setAttribute('description', "");
				actionObject.setAttribute('merge', 'merge = ALL');
				cellStyle = "symbol;image=/resolve/jsp/model/images/symbols/start.png";
				actionObject.innerHTML = '<name>start#resolve</name><params><inputs/><outputs/></params>';
				break;
			case 'end':
				actionObject = mxUtils.createXmlDocument().createElement("End");
				actionObject.setAttribute('label', "End");
				actionObject.setAttribute('description', "");
				actionObject.setAttribute('merge', 'merge = ALL');
				cellStyle = "symbol;image=/resolve/jsp/model/images/symbols/end.png";
				actionObject.innerHTML = '<name>end#resolve</name><params><inputs/><outputs/></params>';
				break;
			case 'event':
				actionObject = mxUtils.createXmlDocument().createElement("Event");
				actionObject.setAttribute('label', "Event");
				actionObject.setAttribute('description', "");
				actionObject.setAttribute('merge', 'merge = ALL');
				cellStyle = "symbol;image=/resolve/jsp/model/images/symbols/cancel_end.png";
				actionObject.innerHTML = '<name>event#resolve</name><params><inputs/><outputs/></params>';
				break;
			case 'text':
				actionObject = mxUtils.createXmlDocument().createElement("Text");
				actionObject.setAttribute('label', "Text");
				actionObject.setAttribute('description', "");
				cellStyle = "labelBackgroundColor=none;strokeColor=none;fillColor=none;gradientColor=white;dashed=1";
				break;
			case 'runbook':
				actionObject = mxUtils.createXmlDocument().createElement("Subprocess");
				actionObject.setAttribute('label', 'Runbook');
				actionObject.setAttribute('description', "");
				cellStyle = "labelBackgroundColor=none;strokeColor=#A9A9A9;fillColor=#ADD8E6;gradientColor=white";
				break;
			case 'task':
				actionObject = mxUtils.createXmlDocument().createElement('Task');
				actionObject.setAttribute('label', 'Task');
				actionObject.setAttribute('description', "");
				actionObject.setAttribute('tooltip', "");
				actionObject.setAttribute('href', "");
				actionObject.setAttribute('merge', 'merge = ALL');
				cellStyle = "rounded;labelBackgroundColor=none;strokeColor=#A9A9A9;fillColor=#D3D3D3;gradientColor=white";
				break;
			case 'document':
				actionObject = mxUtils.createXmlDocument().createElement("Document");
				actionObject.setAttribute('label', 'Document');
				actionObject.setAttribute('description', "");
				cellStyle = "labelBackgroundColor=none;strokeColor=#A9A9A9;fillColor=#66CC33;gradientColor=white";
				break;
			case 'precondition':
				actionObject = mxUtils.createXmlDocument().createElement("Precondition");
				actionObject.setAttribute('label', "Precondition");
				actionObject.setAttribute('description', "");
				actionObject.setAttribute('merge', 'merge = ALL');
				cellStyle = "rhombus;labelBackgroundColor=none;strokeColor=#FFA500;fillColor=#FFA500;gradientColor=white";
				break;
			case 'question':
				actionObject = mxUtils.createXmlDocument().createElement("Question");
				actionObject.setAttribute('label', "Question");
				actionObject.setAttribute('description', "");
				cellStyle = "rhombus;labelBackgroundColor=none;strokeColor=#FFA500;fillColor=#FFA500;gradientColor=white";
				width = 30;
				height = 30;
				break;
			case 'root':
				actionObject = mxUtils.createXmlDocument().createElement("Start");
				actionObject.setAttribute('label', "Root");
				actionObject.setAttribute('description', "");
				cellStyle = "symbol;image=/resolve/jsp/model/images/symbols/start.png";
				break;
			case 'container':
				actionObject = mxUtils.createXmlDocument().createElement("Container");
				actionObject.setAttribute('label', "Container");
				actionObject.setAttribute('description', "");
				cellStyle = "swimlane;labelBackgroundColor=none;strokeColor=#A9A9A9;fillColor=#357EC7;gradientColor=white;fontColor=#FFFFFF";
				break;
			case 'text':
				actionObject = mxUtils.createXmlDocument().createElement("Text");
				actionObject.setAttribute('label', "Text");
				actionObject.setAttribute('description', "");
				cellStyle = "labelBackgroundColor=none;strokeColor=none;fillColor=none;gradientColor=white;dashed=1";
				break;
				//images
			default:
				actionObject = mxUtils.createXmlDocument().createElement('Image');
				actionObject.setAttribute('label', name)
				actionObject.setAttribute('description', "");
				actionObject.setAttribute('noProperty', true);
				actionObject.setAttribute('isImage', true);
				actionObject.setAttribute('tooltip', "");
				actionObject.setAttribute('href', "");
				cellStyle = "labelBackgroundColor=none;image;image=" + icon;
				connectable = false;
		}
		return {
			actionObject: actionObject,
			cellStyle: cellStyle+";fontFamily="+this.fontFamily+";fontSize="+this.fontSize,
			height: height,
			width: width,
			connectable: connectable
		}
	},
	addVertex: function(elementPanel, evt, target, panel) {
		panel.fireEvent('addVertex');
		var name = elementPanel.elementType;
		var config = this.getCellConfig(name, elementPanel.elementImg);
		var graph = this.cmp.graph;
		var cellStyle = config.cellStyle;
		var targetValue = target ? target.getValue() : '';
		if (targetValue && (targetValue.tagName.toLowerCase() == 'task' || targetValue.tagName.toLowerCase() == 'subprocess' || targetValue.tagName.toLowerCase() == 'document') && config.actionObject.getAttribute('isImage')) {
			cellStyle += ';rounded;strokeColor=#A9A9A9;fillColor=white'
			target.setStyle(cellStyle);
			target.setAttribute('customIcon', true);
			target.geometry.x = target.geometry.x + (target.geometry.width-40)/2;
			target.geometry.y = target.geometry.y + (target.geometry.height-40)/2;
			target.geometry.width = 40;
			target.geometry.height = 40;
			graph.refresh();
			graph.getModel().fireEvent(new mxEventObject(mxEvent.CHANGE, "edit", {
				changes: []
			}));
			return;
		}
		var actionObject = config.actionObject;
		var parent = graph.getDefaultParent();
		var pt = graph.getPointForEvent(evt);
		var width = elementPanel.elementWidth || config.width;
		var height = elementPanel.elementHeight || config.height;
		var cell = graph.insertVertex(parent, null, actionObject, pt.x - width / 2, pt.y - height / 2, width, height, cellStyle);
		var cellType = cell.getValue().tagName.toLowerCase();
		if (graph.isSplitEnabled() && target && graph.isSplitTarget(target, [cell], evt)) {
			var sourceType = target.source ? target.source.getValue().tagName.toLowerCase() : '';
			//we don't need to check targetType coz previous condition has already garantee that image and non-image cannot be connected together
			// var targetType = target.target.tagName.toLowerCase();
			if (cellType != 'container' && (sourceType == 'image' && cellType == 'image' || sourceType != 'image' && cellType != 'image')) {
				graph.splitEdge(target, [cell], null);
				// var sourcePt = target.source.geometry.getPoint();
				// var targetPt = target.target.geometry.getPoint();
				// var geo = cell.getGeometry();
				// var center = sourcePt.x - (sourcePt.y - pt.y - geo.height / 2) * (sourcePt.x - targetPt.x) / (sourcePt.y - targetPt.y);
				// geo.x = geo.x + (center - geo.x - geo.width / 2 + 5);
				graph.refresh();
				graph.scrollCellToVisible(cell);
			}
		}
		graph.setSelectionCells([cell]);
		if (targetValue && targetValue.tagName.toLowerCase() == 'container')
			graph.cellsAdded([cell], target, graph.model.getChildCount(target), null, null, true)
	}
});
