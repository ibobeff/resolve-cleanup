/**
 * Plugin that makes sections within the wiki builder dragable
 * Any component that has the css class sectionContainer (like all the fields have) will be reorderable in the wiki builder
 */
Ext.define('RS.wiki.plugins.RBEntityDragZone', {

	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.rbentitydragzone',

	init: function(panel) {
		panel.on('render', this.onPanelRender);
	},

	onPanelRender: function(panel) {
		var me = this;
		Ext.create('Ext.dd.DragZone', panel.getEl(), {
			ddGroup: 'abEntities',
			//Gets the drag data information by finding the drag targets based on their css class
			getDragData: function(e) {
				var sourceEl = e.getTarget('#' + panel.id);
				if (sourceEl) {
					var d = sourceEl.cloneNode(true);
					d.id = Ext.id();
					return {
						ddel: d,
						sourceEl: sourceEl,
						repairXY: Ext.fly(sourceEl).getXY(),
						panel: panel
					}
				}
			},
			//Provide coordinates for the proxy to slide back to on failed drag.
			//This is the original XY coordinates of the draggable element captured
			//in the getDragData method.
			getRepairXY: function() {
				return this.dragData.repairXY;
			}
		});
	}
});