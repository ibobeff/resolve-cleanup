glu.defModel('RS.wiki.SectionPicker', {

	filter: '',

	when_filter_changes_update_section_store: {
		on: ['filterChanged'],
		action: function() {
			var me = this;
			me.sections.filter([{
				fn: function(record) {
					return !me.filter || record.get('name').toLowerCase().indexOf(me.filter.toLowerCase()) == 0
				}
			}])
		}
	},

	index: -1,

	sections: {
		mtype: 'store',
		sorters: ['name'],
		proxy: {
			type: 'memory'
		},
		fields: ['name', 'type']
	},

	init: function() {
		this.sections.loadData([{
			name: this.localize('Image'),
			type: 'image'
		}, {
			name: this.localize('Result'),
			type: 'result'
		}, {
			name: this.localize('Model'),
			type: 'model'
		}, {
			name: this.localize('Form'),
			type: 'form'
		}, {
			name: this.localize('WYSIWYG'),
			type: 'wysiwyg'
		}, {
			name: this.localize('Groovy'),
			type: 'groovy'
		}, {
			name: this.localize('Javascript'),
			type: 'javascript'
		}, {
			name: this.localize('HTML'),
			type: 'html'
		}, {
			name: this.localize('Source'),
			type: 'source'
		}, {
			name: this.localize('ActionTask'),
			type: 'actiontask'
		}, {
			name: this.localize('Automation'),
			type: 'automation'
		}, {
			name: this.localize('InfoBar'),
			type: 'infobar'
		}, {
			name: this.localize('Encrypt'),
			type: 'encrypt'
		}, {
			name: this.localize('Table'),
			type: 'table'
		}, {
			name: this.localize('XTable'),
			type: 'xtable'
		}, {
			name: this.localize('Code'),
			type: 'code'
		}, {
			name: this.localize('WikiDoc'),
			type: 'wikidoc'
		}, {
			name: this.localize('Detail'),
			type: 'detail'
		}, {
			name: this.localize('Progress'),
			type: 'progress'
		}, {
			name: this.localize('SocialLink'),
			type: 'sociallink'
		}])
	},

	sectionSelections: [],
	sectionSelected: function(selected) {
		this.set('sectionSelections', selected)
	},
	addComponent: function() {
		var parentVM = null;
		switch (this.parentVM.viewmodelName) {
			case 'AddTab':
				parentVM = this.parentVM.parentVM.parentVM;
				break;
			case 'Main':
				parentVM = this.parentVM;
				break;
			default:
				parentVM = this.parentVM.parentVM;
		}
		if (this.sectionSelections[0].get('type') != 'automation') {
			this.parentVM.reallyAddSection({
				types: [this.sectionSelections[0].get('type')]
			}, this.index)
			this.doClose();
			return;
		}
		//need to come back and figure out a more elegant way
		if (this.sectionSelections[0].get('type') == 'automation') {
			parentVM.reallyAddSection({
				types: ['automation']
			}, this.index);
		}
		this.doClose()
	},
	addSectionIsEnabled$: function() {
		return this.sectionSelections.length > 0;
	},
	cancel: function() {
		this.doClose()
	}
})