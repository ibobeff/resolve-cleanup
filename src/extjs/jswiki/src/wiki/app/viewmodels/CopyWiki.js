glu.defModel('RS.wiki.CopyWiki', {
	isReplace: false,

	windowTitle$: function() {
		if (this.isReplace) {
			return this.localize('replaceContent');
		} else {
			return this.localize('copy');
		}
	},

	id: '',
	overwrite: false,
	overwriteIsVisible: true,
	skip: true,
	skipIsVisible: true,
	update: false,
	updateIsVisible: true,
	validationRegex : /^[0-9a-zA-Z_-]+$/,

	replace: function() {
		this.copy()
	},
	replaceIsVisible$: function() {
		return !this.copyIsVisible
	},
	replaceIsEnabled$: function(){
		return this.namespace && this.name && this.namespaceIsValid == true && this.nameIsValid == true;
	},

	name: '',
	nameIsValid$: function() {
		return /^[\w|\-]+[\w|\-| ]*$/.test(this.name) ? true : this.localize('invalidName');
	},
	namespace: '',
	namespaceIsValid$: function(){
		return this.validationRegex.test(this.namespace) ? true : this.localize('invalidNamespace');
	},

	copy: function() {
		var choice = 'update'
		if (this.skip) choice = 'skip'
		if (this.overwrite) choice = 'overwrite'

		this.ajax({
			url: '/resolve/service/wikiadmin/moveRenameCopy',
			params: {
				ids: [this.id],
				action: 'copy',
				namespace: this.namespace,
				filename: this.name,
				choice: choice
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.message({
						title: this.localize('copy'),
						msg: this.localize('askForNavigation'),
						buttons: Ext.MessageBox.YESNO,
						buttonText: {
							'yes': this.localize('gotoWiki'),
							'no': this.localize('no')
						},
						scope: this,
						fn: function(btn) {
							if (btn == 'no')
								return;
							clientVM.handleNavigation({
								modelName: 'RS.wiki.Main',
								params: {
									name: this.namespace + '.' + this.name
								}
							});
						}
					})
					clientVM.displaySuccess(this.localize('wikiCopied'))
					this.doClose()
				} else
				clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	copyIsEnabled$: function(){
		return this.namespace && this.name && this.namespaceIsValid == true && this.nameIsValid == true;
	},
	copyIsVisible$: function() {
		return !this.isReplace
	},
	copySaveTxt$: function() {
		return this.localize('copy');
	},
	cancel: function() {
		this.doClose()
	}
})