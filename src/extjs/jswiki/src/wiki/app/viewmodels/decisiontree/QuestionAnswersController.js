glu.defModel('RS.decisiontree.QuestionAnswersController',{
	region : 'north',
	answerType : 'dropdown',
	selectedAnswer : '',
	question : '',
	recommendationText : '',
	questionDisplayText : '',
	answerDisplayText : '',
	recommendationDisplayText : '',
	questionVariables : [],
	recommendationTextVariables: [],
	isReselected : false,
	pageSuspended : false,
	answerList : {
		mtype : 'list',
		autoParent : true
	},
	endOfDt : false,
	API : {
		submitExecution : '/resolve/service/execute/submitSync',
		getWiki : '/resolve/service/wiki/get',
		getActionTask : '/resolve/service/actiontask/get',
		processAnswerParam : '/resolve/service/dt/processAnswerParams',
		getWSData : '/resolve/service/wsdata/getMap'
	},
	CONST : {
		defaultRecommendationText : '( Recommended )'
	},
	//Make sure default here matches what defined in ViewSetting
	defaultTheme : {
		questionFontColor: '000000',
		questionFont: 'Verdana',
		questionFontSize: '13',
		answerFontColor: '000000',
		answerFont: 'Verdana',
		answerFontSize: '10',
		recommendationFontColor: '41a510',
		recommendationFont: 'Verdana',
		recommendationFontSize: '10',
		confirmationFontColor: 'FFFFFF',
		confirmationBGColor: '41a510',
		confirmationFont: 'Verdana',
		confirmationFontSize: '10',
		buttonText: 'Next',
		autoConfirm : false,
		verticalAnswerLayout : false
	},
	//View Settings
	fields : ['questionFont','questionFontColor','questionFontSize','recommendationFontColor','recommendationFont','recommendationFontSize','answerFontColor','answerFont','answerFontSize','confirmationFontColor','confirmationBGColor','confirmationFont','confirmationFontSize','buttonText', {
		name : 'autoConfirm',
		type : 'bool'
	},{
		name : 'verticalAnswerLayout',
		type : 'bool'
	}],
	PROPERTY : {},
	init : function(){
		this.loadData(this.defaultTheme);
		var propertyStore = clientVM.getActionTaskPropertyStore();
		var properties = propertyStore.getRange();
		for(var i = 0; i < properties.length; i++){
			var dataObj = properties[i];
			this.PROPERTY[dataObj.data.uname] = dataObj.data.uvalue;
		}
	},

	returnQuestionStyle$: function() {
		return (
			'font-family:' + this.questionFont + ';' +
			'font-size:' + this.questionFontSize + 'pt;' +
			'color:#' + this.questionFontColor + ';'
		)
	},
	returnConfirmationStyle$: function() {
		return (
			'min-width : 80px; border:1px solid #' + this.confirmationBGColor + ';' +
			'background-color:#' + this.confirmationBGColor + ';'
		)
	},
	returnText$: function() {
		return (
			'<span style="' +
			'font-family:' + this.confirmationFont + ';' +
			'font-size:' + this.confirmationFontSize+ 'pt;' +
			'color:#' + this.confirmationFontColor + ';' + '">' +
			Ext.String.htmlEncode(this.buttonText) + '</span>'
		)
	},
	confirmationBtnIsHidden$ : function(){
		return this.autoConfirm || this.endOfDt;
	},
	submitBtnIsDisabled$ : function(){
		return !this.selectedAnswer ||  this.selectedAnswer == '';
	},

	//Rendering Question and Answer.
	resetQuestion : function(){
		this.answerList.removeAll();
		this.set('pageSuspended', false);
		this.set('selectedAnswer', '');
		this.set('recommendationText', '');
		this.set('answerDisplayText', '');
		this.set('executionStacks',[]);
		this.set('executionMap',null);
		this.set('isAutoAnswered',false);
		this.set('isReselected', false);
		this.resetAutoMatching();
	},
	getQuestionDisplayText : function(){
		return this.questionDisplayText;
	},
	updateQuestion : function(questionInfo, isReselected){	
		this.set('isReselected', isReselected);

		//Update question
		var wsdataRegEx = /WSDATA\.([a-zA-Z_][a-zA-Z_0-9]*)/g;
		var question = questionInfo['question'];
		var recommendationText = questionInfo['recommendationText'];
		var questionVariables = [];
		var questionDisplayText = question.replace(wsdataRegEx, '');
		var m = null;
		while ((m = wsdataRegEx.exec(question)) !== null) {
		    // This is necessary to avoid infinite loops with zero-width matches
		    if (m.index === wsdataRegEx.lastIndex) {
		        wsdataRegEx.lastIndex++;
		    }
		    // The result can be accessed through the `m`-variable.
		    m.forEach(function(match, groupIndex){
		     	if(groupIndex == 1)
		     		questionVariables.push(match);
		    });
		}
		this.set('question', question);
		this.set('questionVariables', questionVariables);
		this.set('questionDisplayText', questionDisplayText || 'End');
		this.set('endOfDt', !question);

		//Update Recommendation	
		var wsdataRegEx = /WSDATA\.([a-zA-Z_][a-zA-Z_0-9]*)/g;	
		var recommendationText = questionInfo['recommendationText'];
		var recommendationTextVariables = [];
		var recommendationDisplayText = recommendationText.replace(wsdataRegEx, '');	
		var m = null;
		while ((m = wsdataRegEx.exec(recommendationText)) !== null) {
		    // This is necessary to avoid infinite loops with zero-width matches
		    if (m.index === wsdataRegEx.lastIndex) {
		        wsdataRegEx.lastIndex++;
		    }
		    // The result can be accessed through the `m`-variable.
		    m.forEach(function(match, groupIndex){
		     	if(groupIndex == 1)
		     		recommendationTextVariables.push(match);
		    });
		}
		this.set('recommendationText', recommendationText);
		this.set('recommendationTextVariables', recommendationTextVariables);
		this.set('recommendationDisplayText', recommendationDisplayText);		

		//Update Answer
		this.set('answerType', questionInfo['answerType']);
		this.updateAnswerList( questionInfo['answerList']);	
	},

	updateAnswerList : function(newAnswerList) {
		var executionRegex = /EXECUTION_LIST=\[([^\]\[]*)\]/;
		var expressionRegex = /EXPRESSION=(\[.*\])/;
		var wsdataRegEx = /WSDATA\.([a-zA-Z_][a-zA-Z_0-9]*)/g;
		Ext.Array.each(newAnswerList, function(answer) {

			//Parse execution
			var matchedList = answer.answerInfo.match(executionRegex);
			var executionList = [];
			if(matchedList){				
				executionList = matchedList[1].split(', ');
			}
			if(executionList.length > 0)
				this.updateExecutionMap(answer.answerInfo, executionList);

			//Parse expression
			var matchedList = answer.answerInfo.match(expressionRegex);
			if(matchedList){
				var expressionArray =  matchedList[1] ? Ext.decode(matchedList[1]) : null;
				if(expressionArray)
					this.parseExpression(answer.answerInfo, answer.answerText, expressionArray);
			}

			//Parse answer text			
			var m = null;
			var answerVariables = []; 
			var answerDisplayText = answer.answerText.replace(wsdataRegEx,'');
			while ((m = wsdataRegEx.exec(answer.answerText)) !== null) {
			    // This is necessary to avoid infinite loops with zero-width matches
			    if (m.index === wsdataRegEx.lastIndex) {
			        wsdataRegEx.lastIndex++;
			    }
			    // The result can be accessed through the `m`-variable.
			    m.forEach(function(match, groupIndex){
			     	if(groupIndex == 1)
			     		answerVariables.push(match);
			    });
			}
			this.answerVariables = answerVariables;
			if(answer.selected){				
				this.set('selectedAnswer', answer.answerInfo);
				this.set('answerDisplayText', answerDisplayText);
			}
		}, this);
		//Render page if there no evaluation needed.
		if(this.isReselected || !this.canBeEvaluated)
			this.parentVM.renderPage();	
		else
			this.set('pageSuspended', true);
	},	

	// Need to override this method
	suggestAnswers: function(matchedAnswers, recommendationDisplayText) {

	},
	// Need to override this method
	selectAnswer: function(matchedAnswer) {

	},

	//Evaluate Auto Answer Logic
	isAutoAnswered : false,
	isAutoAnswerDisabled : false,
	processMatchedAnswers : function(matchedAnswers){		
		if(matchedAnswers.length == 0){
			//Try evaluation again after a brief delay
			this.startEvaluationTask(this.parentVM.problemId);

			//No match just render the suspended page
			if(this.pageSuspended){
				this.parentVM.renderPage();
				this.set('pageSuspended', false);
			}
		}
		//Only 1 matched answer, just select/recommend it
		else if(matchedAnswers.length == 1){
			if(!this.isAutoAnswerDisabled){
				this.selectAnswer(matchedAnswers[0]);
				// this.handleNavigation();	
			}
			else
				this.suggestAnswers(matchedAnswers, this.recommendationText);
		}
		//Multiple matched answers, display recommendation only.
		else if(matchedAnswers.length > 1){
			this.suggestAnswers(matchedAnswers, this.recommendationText);

			//Display suspended page.
			this.parentVM.renderPage();
		}
	},

	//Execution Logic
	progressBar : {
		mtype : 'ExecutionProgress',
	},
	executionStacks : [],
	executionMap : null,
	executionVariableList : [],
	updateExecutionVariableList : function(dtVariables){
		this.set('executionVariableList', dtVariables);
	},
	updateExecutionMap : function(answerInfo, executionList){
		var executionMap = this.executionMap || {};
		executionMap[answerInfo] = executionList;
		this.set('executionMap', executionMap);
	},
	currentRequest : null,
	abortExecution : function(abortedFromError){
		//Cancel current request. Note : this does not actually cancel the execution process in the server.
		if(this.currentRequest)
			Ext.Ajax.abort(this.currentRequest);
		//Clear execution stack then display current page.
		this.set('executionStacks',[]);
		this.set('executionMap',null);
		this.progressBar.doClose();
		if(!abortedFromError)
			this.handleNavigation();
	},
	doExecution : function(){
		if(this.executionStacks.length > 0){
			var executionInfo = JSON.parse(this.executionStacks[0]);
			var executionName = executionInfo['name'];
			var paramList = executionInfo['params'];
			var executionType = (executionName.indexOf('#') != -1) ? 'TASK' : 'EXECUTEPROCESS';//Weak logic rely on # symbol to identify task or rb.
			this.progressBar.updateProgress(executionName);

			this.getParamValueThenExecute(paramList, executionType, executionName, submitRequest);
			function submitRequest(params){
			var contextParams = {
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId == 'nil' ? '' : clientVM.orgId
			}
        if(clientVM.SIR_SELECTED_ACTIVITY) {
          contextParams = Ext.apply(contextParams, {
            activityId: clientVM.SIR_SELECTED_ACTIVITY.id,
            activityName: clientVM.SIR_SELECTED_ACTIVITY.activityName,
            incidentId: clientVM.SIR_SELECTED_ACTIVITY.incidentId,
            phase: clientVM.SIR_SELECTED_ACTIVITY.phase
          })
        }
        var payload = {
          action: executionType,
          problemId : this.parentVM.problemId,
          params: Ext.apply(params, contextParams)
        };
        payload[(executionType == 'TASK') ? 'actiontask' : 'wiki'] = executionName;
				this.currentRequest = Ext.Ajax.request({
					scope : this,
					url: this.API['submitExecution'],
					timeout : 300000, // 5 min timeout
					jsonData : {
						executionList : [payload],
						dtVariables : this.executionVariableList,						
					},
					//jsonData : payload,
					success : function(r){
						var response = RS.common.parsePayload(r);
						if(!response[0].success){
							clientVM.displayError(response.message);
							this.abortExecution(true);
							return;
						}
						this.doExecution();
					},
					failure : function(r){
						clientVM.displayFailure(r);
						this.abortExecution(true);
					}
				})
				//Remove this task from call stacks
				this.executionStacks.splice(0,1);
			}
		}
		else {
			this.progressBar.updateProgress();
			this.set('executionMap',null);
			this.parentVM.navigateToNextPage(this.selectedAnswer, this.answerDisplayText ,this.isAutoAnswered);	
		}
	},	
	getParamValueThenExecute : function(paramList, executionType, executionName, callback){
		this.ajax({
			url : this.API['getWSData'],
			params: {
				problemId: this.parentVM.problemId
			},
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if(!response.success){
					clientVM.displayError(response.message);
					return;
				}
				if (response.success) {					
					var WSDATA = response.data;
					var PROPERTY = this.PROPERTY;
					for(var param in paramList){
						var value = paramList[param];
						var propertyPattern = /PROPERTY{([a-zA-Z0-9_\-]*)}/g;
						var wsdataPattern = /WSDATA{([a-zA-Z0-9_\-]*)}/g;
						var m = null;
						while ((m = propertyPattern.exec(value)) !== null) {					   
						    if (m.index === propertyPattern.lastIndex) {
						        propertyPattern.lastIndex++;
						    }					 
						    m.forEach(function(match, groupIndex){
						        if(groupIndex == 1)
						        	paramList[param] = PROPERTY[match] || '';
						    });
						}
						var n;
						while ((n = wsdataPattern.exec(value)) !== null) {					   
						    if (n.index === wsdataPattern.lastIndex) {
						        wsdataPattern.lastIndex++;
						    }					 
						    n.forEach(function(match, groupIndex){
						        if(groupIndex == 1)
						        	paramList[param] = WSDATA[match] || '';						        
						    });
						}
					}
					this.getDefaultParameters(paramList, executionType, executionName, callback);
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	getDefaultParameters : function(paramWithValue, executionType,executionName, callback){
		var parameters = {};
		this.ajax({
			url: executionType == 'TASK' ? this.API['getActionTask'] : this.API['getWiki'],
			params : {
				id : '',
				name : executionName
			},
			scope: this,
			success : function(r){
				var response = RS.common.parsePayload(r);
				if(!response.success){
					clientVM.displayError(response.message);
					this.abortExecution(true);
					return;
				}
				var data = response.data;
				if(executionType == 'TASK'){
					var paramList = data['resolveActionInvoc']['resolveActionParameters'];
					for(var i = 0; i < paramList.length; i++){
						if(paramList[i].utype == 'INPUT'){
							var name = paramList[i].uname;
							var defaultValue = paramList[i].udefaultValue;
							parameters[name] = defaultValue;
						}
					}
				}
				else {
					var parameterData = data['uwikiParameters'];
					var paramList = parameterData ? Ext.decode(parameterData) : [];
					for(var i = 0; i < paramList.length; i++){
						var name = paramList[i].name;
						//NOT SURE WHY SOURCE TYPE DOES NOT MATTER FOR THIS.
						var defaultValue = paramList[i].sourceName;
						parameters[name] = defaultValue;
					}
				}
				callback.apply(this, [Ext.apply(parameters, paramWithValue)]);
			},
			failure : function(r){
				clientVM.displayFailure(r);
				this.abortExecution(true);
			}
		})
	},
	handleNavigation : function() {
		//
		// INI-5 clicking the button multple times can cause duplication for navigation items
		// so disabled the button immediately once the button is clicked. The button will be
		// enabled upon valid answer selection for the question.
		//
		this.set('submitBtnIsDisabled', true);

		//Save any Additional Data first.
		this.saveAdditionalData();
		
		//Execute any task first before navigate to next page.
		if(this.executionMap && this.executionMap.hasOwnProperty(this.selectedAnswer)){
			this.set('executionStacks', this.executionMap[this.selectedAnswer]);
			this.progressBar.initProgress(this.executionStacks.length);
			this.open(this.progressBar);		
			this.doExecution();
		}
		else
			this.parentVM.navigateToNextPage(this.selectedAnswer, this.answerDisplayText ,this.isAutoAnswered);		
	},
	saveAdditionalData : function(){
		var params = Ext.urlDecode(this.selectedAnswer);
		this.ajax({
			url: this.API['processAnswerParam'],
			params: {
				dtFullName: this.parentVM.dtName,
				nodeId: params.NODE_ID,
				problemId: this.parentVM.problemId
			},
			success: function(resp) {
				var response = RS.common.parsePayload(resp)
				if (!response.success) {
					clientVM.displayError(response.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})			
	},
	returnRecommendationStyle: function(recommendationText) {
		return(
			'<span style="font-family:' + this.recommendationFont + ';' +
			'font-size:' + this.recommendationFontSize + 'pt;font-weight:normal;' +
			'color:#' + this.recommendationFontColor + ';' +
			'height:' + (this.recommendationFontSize * 1.5) + 'pt;"> ' + //Adjust height according to font size. 1em ~= 1.33 so 1.5 for padding
			recommendationText + '</span>'
		)
	}
});
