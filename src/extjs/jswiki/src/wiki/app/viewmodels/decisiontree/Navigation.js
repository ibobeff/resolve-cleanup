glu.defModel('RS.decisiontree.Navigation',{	
	mixins : ['AutoMatchingLogic', 'QuestionAnswersController'],
	region : 'west',
	regionPadding : '0 0 0 0',
	navigationEntryList : {
		mtype : 'list',
		autoParent : true
	},
	fields : [
		'navigationFontColor', 'navigationBGColor', 'navigationFont', 'navigationFontSize',
		'questionFont','questionFontColor','questionFontSize','recommendationFontColor','recommendationFont',
		'recommendationFontSize','answerFontColor','answerFont','answerFontSize','confirmationFontColor',
		'confirmationBGColor','confirmationFont','confirmationFontSize','buttonText', {
		name : 'autoConfirm',
		type : 'bool'
	},{
		name : 'verticalAnswerLayout',
		type : 'bool'
	}],
	defaultTheme: {
		navigationFontColor: 'FFFFFF',
		navigationBGColor: '41a510',
		navigationFont: 'Verdana',
		navigationFontSize: '13',
	},

	updateRegion : function(region){	
		this.fireEvent('updateRegion',region);	
	},

	collapsed: false,

	init : function(){		
		this.loadData(this.defaultTheme);
		this.updateTheme();
		if (window.mobile) {
			this.set('collapsed', true);
		}
	},

	updateTheme: function(data) {
		if(data) {
			this.loadData(data);
		}

		//Remove existing theme
		Ext.util.CSS.removeStyleSheet('navigationSelected');
		Ext.util.CSS.removeStyleSheet('navigationHover');

		//Selected
		Ext.util.CSS.createStyleSheet('.qa-selected {background:#' + this.navigationBGColor + ';}','navigationSelected');
		
		//Hover
		var RBGColor = RS.common.hexToRgb(this.navigationBGColor);
		var navColorLightened = 'rgba(' + RBGColor.r + ',' + RBGColor.g + ',' + RBGColor.b + ',0.6)';
		Ext.util.CSS.createStyleSheet('.decision-tree-qa-container:hover .decision-tree-qa {background:' + navColorLightened + ';}','navigationHover');			
	},

	selectedEntry : null,
	rerenderDisplayText : function(newQuestionDisplay, WSDATA) {
		if(this.selectedEntry){
			//Ignore those fields to avoid polluted wsdata set.
			delete WSDATA['dt_dtList'];
			delete WSDATA['dt_qa'];
			delete WSDATA['answer_params_map'];
			delete WSDATA['dt_metric']; 

			//Rerender Answers
			this.selectedEntry.rerenderDisplayText(WSDATA);
			this.selectedEntry.set('wsdata', WSDATA);
		}
	},
	resetNavigation : function(){
		this.navigationEntryList.removeAll();
		this.set('selectedEntry',null);
	},	
	addNewEntry : function(newQuestion) {	
		var indexOfSelectedEntry = this.navigationEntryList.indexOf(this.selectedEntry);
		if(indexOfSelectedEntry == -1)
			this.reallyAddNewEntry(newQuestion);
		else {
			for(var i = this.navigationEntryList.getCount() - 1; i > indexOfSelectedEntry; i--){
				this.navigationEntryList.removeAt(i);
			}
			this.selectedEntry.set('selected',false);
			this.reallyAddNewEntry(newQuestion);
		}		
	},

	reallyAddNewEntry : function(newQuestion){		
		var selectedEntry = this.model({
			mtype : 'NavigationEntry',
			question : newQuestion['question'],
			wsdata : newQuestion['wsdata'],
			questionDisplayText : newQuestion['questionDisplayText'],		
			selected : true	
		})
		this.navigationEntryList.add(selectedEntry);	
		this.set('selectedEntry', selectedEntry);
	},
	updateSelectedEntryAnswer : function(answerInfo, answerText, isAutoAnswer) {
		this.selectedEntry.origAnswerText = answerText;
		//Add a text to indicate this answer is auto answered, for now+		var answerText = answerText;
		if (isAutoAnswer) {				
			answerText = answerText + '<image class="dt-auto-answer-icon" title="'+this.localize('autoAnswerDesc')+'" src="/resolve/images/auto-answer.png">'
		}
		this.selectedEntry.set('answerInfo', answerInfo);
		this.selectedEntry.set('answerText', answerText);
	},
	updateSelectedEntryPreviousAnswer : function(prevAnswer){
		this.selectedEntry.set('prevAnswer', prevAnswer);
	},
	getSelectedEntryAnswerInfo : function(){
		return this.selectedEntry ? this.selectedEntry.answerInfo : null;
	},
	getSelectedEntryPrevAnswer : function(){
		return this.selectedEntry.prevAnswer;
	},
	selectEntry : function(selectedEntry){
		if(this.selectedEntry == selectedEntry)
			return;
		this.navigationEntryList.each(function(entry){
			if(entry != selectedEntry)
				entry.set('selected',false);
			else 
				entry.set('selected',true);			
		});
		this.set('selectedEntry', selectedEntry);
		//Load this question again.
		this.parentVM.reselectQuestion(selectedEntry);
	},	
	getSerializeQuestions : function(){
		var serializedQuestions = [];
		this.navigationEntryList.each(function(entry){
			serializedQuestions.push(entry.asObject());
		})
		return serializedQuestions;
	},
	
	// Need to override this method
	suggestAnswers: function(matchedAnswers, recommendationDisplayText) {
		this.selectedEntry.suggestAnswers(matchedAnswers, recommendationDisplayText);
	},
	// Need to override this method
	selectAnswer: function(matchedAnswer) {
		this.selectedEntry.autoAnswer(matchedAnswer);
	},
});

glu.defModel('RS.decisiontree.NavigationEntry', {
	fields: ['question', 'questionDisplayText', 'answerInfo', 'answerText', {
	name : 'wsdata',
	type : 'auto' }],
	prevAnswer : null,
	answerText: '<span class="dt-answer-is-required-text">Answer required</span>',
	
	suggestAnswers: function(matchedAnswers, recommendedAnswerText) {
		var navigation = this.parentVM;
		var wsData = navigation.selectedEntry['wsdata'];
		// Reccomendation
		for(var i = 0; i < navigation.recommendationTextVariables.length; i++) {
			var variable = navigation.recommendationTextVariables[i];
			recommendedAnswerText = recommendedAnswerText.replace(new RegExp('WSDATA\\.' + variable, 'g'), wsData[variable] || '');
		}
		for (var i=0; i<this.answers.length; i++) {
			var answer = this.answers.getAt(i);
			for (var j=0; j<matchedAnswers.length; j++) {
				var matchedAnswer = matchedAnswers[j];
				if (answer.answerInfo == matchedAnswer) {
					answer.set('answeredIconVisibility', 'hidden');
					answer.set('recommendedAnswerIcon', 'visible');
					answer.set('recommendedAnswerText', recommendedAnswerText);
					answer.recommended = true;
					answer.savedRecommendedAnswerText = recommendedAnswerText;
					break;
				}
			}
		}
	},

	autoAnswer: function(matchedAnswer) {
		this.parentVM.set('isAutoAnswered', true);
		for (var i=0; i<this.answers.length; i++) {
			var answer = this.answers.getAt(i);
			if (answer.answerInfo == matchedAnswer) {
				answer.answerSelected();
				break;
			}
		}
	},

	displayQuestion$ : function() {
		return this.question ? this.questionDisplayText : 'End';
	},
	displayAnswer$ : function() {
		return this.answerText;
	},
	answerIsHidden$ : function() {
		return this.isEnd$() || this.answerText == "" || this.selected;
	},
	selected: false,
	selectedCss$: function() {
		return this.selected ? 'selected' : 'unselected';
	},
	containerCls$ : function() {
		return 'decision-tree-qa-container' + (this.selected ? '-selected' : '');
	},
	select: function() {
		this.parentVM.selectEntry(this);
	},
	isEnd$: function() {
		return !this.question;
	},
	hideAnswers$: function() {
		return !this.selected;
	},
	answers: {
		mtype: 'list',
		autoParent : true
	},

	populateAnswers: function(questionInfo) {
		var answers = questionInfo.answerList;
		var answersLength = this.answers.length;
		var recommendedText = questionInfo.recommendationText;
		for (var i=0; i<answers.length; i++) {
			var rec = answers[i];
			var answer = rec.answerText;
			var answerSelected = rec.selected;
			var isRecommendedAnswer = false;
			if (answersLength > 0) {
				var a = this.answers.getAt(i);
				a.set('answer', answer);
				a.set('answeredIconVisibility', answerSelected ? 'visible' : 'hidden');
				if (answerSelected) {
					a.set('recommendedAnswerIcon', 'hidden');
					a.set('recommendedAnswerText', '');
				}
				
			} else {
				var model = this.model({
					mtype: 'Answer',
					answer: answer,
					selected: answerSelected,
					answerInfo: rec.answerInfo,
					answeredIconVisibility: (!isRecommendedAnswer && answerSelected) ? 'visible' : 'hidden',
					recommendedAnswerIcon: isRecommendedAnswer ? 'visible' : 'hidden'
				});
				this.answers.add(model);
				model.answerModel = rec;
			}
		}
	},

	rerenderDisplayText : function(WSDATA) {
		var questionStr = this.question;
		var navigation = this.parentVM;
		for(var i = 0; i < navigation.questionVariables.length; i++){
			var variable = navigation.questionVariables[i];
			questionStr = questionStr.replace(new RegExp('WSDATA\\.' + variable, 'g'), WSDATA[variable] || ''); //need double slash for new RegExp and symbols
		}
		this.set('questionDisplayText', questionStr);

		for (var i=0; i<this.answers.length; i++) {
			var answer = this.answers.getAt(i);
			var answerStr = Ext.String.htmlDecode(answer.answer);
			var answerVariables = navigation.answerVariables || [];
			for(var j = 0; j < answerVariables.length; j++) {
				var variable = answerVariables[j];
				answerStr = answerStr.replace(new RegExp('WSDATA\\.' + variable, 'g'), WSDATA[variable] || '');
			}
			answer.set('answer', answerStr);		
		}
	},
});

glu.defModel('RS.decisiontree.Answer', {
	recommended: false,
	savedRecommendedAnswerText: '',
	recommendedAnswerText: '',
	answerModel: null,
	answer: '',
	answerInfo: '',
	recommendedAnswerIcon: 'hidden',
	answeredIconVisibility: false,
	recommendedText: '',
	recommendedTextCls: '',
	answerSelected: function() {
		var navigationPanel = this.rootVM.navigationPanel;
		this.parentVM.answers.forEach(function(answer) {
			answer.set('answeredIconVisibility', 'hidden');
			if (answer.recommended) {
				answer.set('recommendedAnswerIcon', 'visble');
				answer.set('recommendedAnswerText', this.savedRecommendedAnswerText);
			}
		});
		this.parentVM.set('answerText', '');
		this.set('answeredIconVisibility', 'visible');
		this.set('recommendedAnswerIcon', 'hidden');
		navigationPanel.set('selectedAnswer', this.answerInfo);
		navigationPanel.set('answerDisplayText', this.answer);
		navigationPanel.handleNavigation();
	},
	answerIconActiveItem$: function() {
		return (this.recommendedAnswerIcon === 'visible' && this.answeredIconVisibility === 'hidden') ? 0 : 1;
	},
	getRecommendationTextWithStyle: function(recommendedAnswerText) {
		return this.parentVM.parentVM.returnRecommendationStyle(recommendedAnswerText? recommendedAnswerText : this.recommendedAnswerText);
	}
});
