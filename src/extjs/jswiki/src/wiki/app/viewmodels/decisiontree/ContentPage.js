glu.defModel('RS.decisiontree.ContentPage', {
	masked: false,
	content : '',
	regionPadding : '0 0 0 0',
	updatePadding : function(questionOnTop){		
		this.fireEvent('updatePadding',questionOnTop);
	},
	updateContent : function(pageContent){
		var newHTML = pageContent['html'];
		var scripts = pageContent['scripts'];
		wikiDocumentName = pageContent['pageName']; //This supports CURRENT_WIKI option for form inside wiki

		// IE does not support Event constructor "new Event('beforedestroy')" so we have to use
		// createEvent/initEvent. Ref: https://developer.mozilla.org/en-US/docs/Web/API/Event/Event
		addLoadEvents = [];
		var event = document.createEvent("Event");
		event.initEvent('beforedestroy', true, false);
		document.dispatchEvent(event);

		// create virtual dom with newHTML content and inject csrftokens as necessary
		var doc = new DOMParser().parseFromString(newHTML.innerHTML, 'text/html');
		var dtWikiBody = doc.getElementsByTagName('body')[0];
		if (dtWikiBody) {
			dtWikiBody = clientVM.injectCSRFTokensToWiki(dtWikiBody, true);
		}

		//Complete clear previous content.		
		this.set('content', null);
		this.set('content', '<div class="wikiBody x-body x-webkit x-chrome">' + dtWikiBody.innerHTML + '</div>');

		if (this.setTimeoutId) {
			clearTimeout(this.setTimeoutId);
		}
		
		//Push this to end of event queue.
		this.setTimeoutId = setTimeout(function(){			
			window.eval(scripts);
			//Go through the load events configured by the users and execute them
            Ext.Array.forEach(addLoadEvents, function(evt){
                if( Ext.isFunction(evt) ) evt()
                else if( Ext.isString(evt) && Ext.isFunction(window[evt])) window[evt]()
            })
		},0);		
	},
	init : function(){
		this.addGlobalEventListerners();
	},
	beforeDestroyComponent : function(){
		this.removeGlobalEventListerners();
	},
	addGlobalEventListerners : function(){	
        clientVM.on('problemIdChanged', this.handleProblemIdChangedHandler, this);
	},
	removeGlobalEventListerners: function(){
		clientVM.removeListener('problemIdChanged', this.handleProblemIdChangedHandler);
	},
	handleProblemIdChangedHandler: function() {
        if(startTask)
            return startTask(5000)
        //console.log('freed script stuff');
        return 'discard'
    }
})
