glu.defModel('RS.decisiontree.AutoMatchingLogic',{
	EVALUATION_INTERVAL : 3000,
	AUTO_MATCHING_API : {
		getWSData : '/resolve/service/wsdata/getMap',
	},
	variableList : {},
	autoMatchingList : [],
	WSDATARegex : /WSDATA\{(.*)\}/,	
	resetAutoMatching : function(){
		this.set('variableList', {});
		this.set('autoMatchingList', []);
		if(this.evaluationTask)
			clearInterval(this.evaluationTask);
	},
	parseExpression : function(answerInfo, answerText, expressionArray){
		for(var i = 0; i < expressionArray.length; i++){
			var expression = expressionArray[i];
			var variableName = expression.variableName;
			this.variableList[variableName] = null;
			var matcher = expression.operand.match(this.WSDATARegex);
			if(matcher)
				this.variableList[matcher[1]] = null;
		}		
		this.autoMatchingList.push({
			answerText : answerText,
			answerInfo : answerInfo,
			expressionArray : expressionArray
		});
		this.fireEvent('autoMatchingListChanged');
	},
	canBeEvaluated$ : function(){
		return !this.isAutoAnswerDisabled && this.autoMatchingList.length != 0;			
	},
	startEvaluationTask : function(problemId){
		if(!this.canBeEvaluated)
			return;
		if(this.evaluationTask)
			clearTimeout(this.evaluationTask);
		this.evaluationTask = setTimeout(function(){
			this.ajax({
				url : this.AUTO_MATCHING_API['getWSData'],
				params: {
					problemId: problemId
				},
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if(!response.success){
						clientVM.displayError(response.message);
						return;
					}
					if (response.success) {
						var WSDATA = response.data;
						//Might need to do dynamic rendering later on here.
						
						//Run evaluation
						this.startEvaluation(WSDATA);		
					}
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}.bind(this),
		this.EVALUATION_INTERVAL);
	},
	startEvaluation : function(WSDATA){			
		for(var k in WSDATA){
			if(this.variableList.hasOwnProperty(k)){
				var data = WSDATA[k];						
				this.variableList[k] = data;						
			}
		}
		this.evaluateExpression();			
	},
	evaluateExpression : function(){	
		var matchedAnswers = [];
		var operatorLookup = {
			'=' : function(a,b){ return a == b},
			'>=' : function(a,b){ return a >= b},
			'<=' : function(a,b){ return a <= b},
			'!=' : function(a,b){ return a != b},
			'<' : function(a,b){ return a < b},
			'>' : function(a,b){ return a > b}
		}	
		for(var i = 0; i < this.autoMatchingList.length; i++){
			var expression = this.autoMatchingList[i];
			var answerInfo = expression.answerInfo;
			var expressionArray = expression.expressionArray || [];
			var matched = false;	
			for(var j = 0; j < expressionArray.length; j++){
				var variableName = expressionArray[j].variableName;
				var variableData = this.variableList[variableName];
				var operator = expressionArray[j].operator;
				var operand = expressionArray[j].operand;
				var matcher = operand.match(this.WSDATARegex);
				if(matcher)
					operand = this.variableList[matcher[1]];
				if(variableData && operand && operatorLookup[operator](variableData, operand))
					matched = true;
				else {
					matched = false;
					break;
				}					
			}	
			if(matched)
				matchedAnswers.push(answerInfo);
		}
		this.processMatchedAnswers(matchedAnswers);	
	}
})
