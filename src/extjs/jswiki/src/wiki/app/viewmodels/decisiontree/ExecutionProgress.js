glu.defModel('RS.decisiontree.ExecutionProgress',{
	taskCount : 0,
	taskRemanining : 0,
	progress : -1,
	currentTaskName : '',
	executionType : 'ActionTask',
	progressText$ : function(){
		return 'Executing ' + this.executionType + ' <b>' + this.currentTaskName + '</b> ...';
	},
	initProgress : function(taskCount){
		this.set('currentTaskName', null);
		this.set('taskCount', taskCount);
		this.set('taskRemanining', taskCount);
	},
	updateProgress  : function(taskName){
		if(taskName){
			this.set('currentTaskName', taskName);
			this.set('executionType' , (taskName.indexOf('#') != -1) ? 'ActionTask' : 'Automation');
		}		
		this.set('taskRemanining', --this.taskRemanining);
		var progress = (this.taskCount - this.taskRemanining - 1) / this.taskCount;
		this.set('progress',progress);
		if(progress == 1){
			this.set('progressText',this.localize('executionCompleted'));
			this.doClose();
		}
	},
	abort : function(){
		this.parentVM.abortExecution();
		this.doClose();
	}
})