glu.defModel('RS.wiki.DecisionTreeViewer', {

	name: '',

	decisionTreeLoaded: false,
	pinned: true,
	displayQuestions: true,

	active: false,
	wait: false,

	questions: {
		mtype: 'list',
		autoParent: true
	},
	autoNavigate : false,
	radioIsVerticalLayout : false,
	socialIsPressed: false,
	showAnswerOnHistory : false,
	showQuestionOnBottom : false,
	northQuestionIsHidden$ : function(){
		return (!this.displayQuestion || this.showQuestionOnBottom);
	},
	southQuestionIsHidden$ : function(){
		return (!this.displayQuestion || !this.showQuestionOnBottom);
	},	
	socialPressedCls$: function() {
		return this.socialIsPressed ? 'rs-social-button-pressed icon-comment' : 'icon-comment-alt'
	},
	socialToggled: function(pressed) {
		this.set('socialIsPressed', pressed)
	},
	showSocial$: function() {
		return !isPublicUser()
	},

	socialDetail: {
		mtype: 'RS.social.Main',
		mode: 'embed',
		initialStreamType: 'worksheet'
	},

	activate: function(screen, params) {
		this.set('active', true)
		this.set('name', params ? params.name : '')
		var oldProblemId = this.problemId
		this.set('problemId', params ? params.problemId || clientVM.problemId : clientVM.problemId)
		clientVM.setWindowTitle(this.name || this.localize('decisionTreeViewerTitle'))
			// this.set('metricData', '');
			//If we don't have a problemId and don't have a user (which has the active problemId in it) then ask the user to set one
		if (!this.problemId && ! clientVM.user) {
			clientVM.selectPRB(null, false)
		}

		if (oldProblemId == this.problemId)
			this.loadWSData()
		this.fireEvent("updateRadioFieldLayout");
	},

	deactivate: function() {
		this.set('active', false)
	},
	dummy : '',
	init: function() {
		this.set('problemId', this.problemId || clientVM.problemId) //pull id from the client if not directly passed
		this.ajax({
			url : '/resolve/service/sysproperties/getAllProperties',
			params : {
				filter: Ext.encode([{"field":"uname","type":"auto","condition":"contains","value":"dt.option"}])
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					if (response.records) {
						Ext.Array.each(response.records, function(record){
							var propertyName = record.uname;
							if(propertyName == "dt.option.radio.vertical")
								this.set('radioIsVerticalLayout', record.uvalue == 'true');
							if(propertyName == "dt.option.autoNavigate")
								this.set('autoNavigate', record.uvalue == 'true');	
							if(propertyName == "dt.option.history.showAnswer")
								this.set('showAnswerOnHistory', record.uvalue == 'true');
							if(propertyName == "dt.option.showQuestionOnBottom"){
								this.set('showQuestionOnBottom', record.uvalue == 'true');
								setTimeout(function(){
									//Hack to remove region
									this.fireEvent('removeRegion', (this.showQuestionOnBottom ? 'northRegion' : 'southRegion'));
								}.bind(this),500);
							}
						}, this)				
					}
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
		clientVM.on('problemIdChanged', this.problemIdChangedHandler,this);
		clientVM.on('dtSuggestionAnswerChanged', this.updateSuggestionAnswer, this);
		clientVM.on('dtActivateAnswer', this.autoActivateAnswer,this);
		clientVM.on('userChanged', function() {
			if (!this.problemId && ! clientVM.problemId) clientVM.selectPRB(null, false)
		}, this)

		this.screenVM.on('sociallinkclicked', this.socialLinkClicked, this);
	},
	verticalRGIsHidden$ : function(){
		return !this.isRadioQuestion || !this.radioIsVerticalLayout;
	},
	horizontalRGIsHidden$ : function(){
		return !this.isRadioQuestion || this.radioIsVerticalLayout;
	},
	onComponentDestroy : function(){
		clientVM.removeListener('problemIdChanged',this.problemIdChangedHandler);
		clientVM.removeListener('dtSuggestionAnswerChanged', this.updateSuggestionAnswer);
		clientVM.removeListener('dtActivateAnswer', this.autoActivateAnswer,this);
	},
	problemIdChangedHandler : function() {
		Ext.defer(function() {
			this.set('problemId', clientVM.problemId)
			this.set('problemNumber', clientVM.problemNumber)
			this.updateSocial()
		}, 100, this)		
	},
	answerIsSelected : false,
	autoNavigateHandler : function(answer){
		this.set('answerIsSelected', true);
		if(this.autoNavigate){
			this.comboAnswer = answer;
			this.submit();
		}
	},
	autoActivateAnswer : function(nextAnswer){		
		var verifiedAns = null;	
		if (this.isRadioQuestion){
			this.radioAnswers.forEach(function(answer) {
				if (answer.displayAnswer == nextAnswer) {
					verifiedAns = answer.value;
					answer.checked = true;
					return;
				}
			})
		}
		if (this.isComboQuestion) {			
			this.comboAnswers.each(function(answer) {
				if (answer.get('displayAnswer') == nextAnswer) {
					verifiedAns = answer.get('value');
					return;
				}
			})
		}
		if(verifiedAns){
			this.set('answerIsSelected', true);
			this.comboAnswer = verifiedAns;
			this.submit();
		}
		
	},
	verticalRadioAnswers: {
		mtype: 'list'
	},
	horizontalRadioAnswers: {
		mtype: 'list'
	},
	when_radio_answer_update : {
		on : ['updateRadioAnswer'],
		action : function(){
			if(!this.horizontalRGIsHidden){
				this.horizontalRadioAnswers.removeAll();
				this.radioAnswers.forEach(function(answer){
					this.horizontalRadioAnswers.add(answer);
				},this)	
			}
			else
			{
				this.verticalRadioAnswers.removeAll();
				this.radioAnswers.forEach(function(answer){
					this.verticalRadioAnswers.add(answer);
				},this)	
			}
		}
	},
	updateSuggestionAnswer : function(suggestedAnswer, displayText){
		if (this.isComboQuestion){
			var newComboAnswers = [];		
			this.comboAnswers.each(function(answer){
				newComboAnswers.push({
					name: answer.get('name'),
					value: answer.get('value'),
					isSuggested : (answer.get('name') == suggestedAnswer),
					displayText : displayText || 'Suggestion'
				})		
			},this)
			this.comboAnswers.removeAll();
			this.comboAnswers.loadData(newComboAnswers);
			var currentAnswer = this.comboAnswer;
			this.set('comboAnswer','');
			this.set('comboAnswer',currentAnswer);
		}		
		//For radio Type
		if (this.isRadioQuestion){
			var newRadioAnswers = [];
			var displayText = displayText || 'Suggestion';
			this.radioAnswers.forEach(function(answer){
				newRadioAnswers.push({
					mtype: 'viewmodel',
					name: answer.get('name'),
					value: answer.get('value'),
					checked: answer.get('checked'),
					displayAnswer : answer.get('name') + (answer.get('name') == suggestedAnswer ? ' ( ' + displayText + ' )' : '' )
				})
			},this)
			this.radioAnswers.removeAll();
			Ext.Array.each(newRadioAnswers,function(answer){
				this.radioAnswers.add(answer);
			},this)	
			this.fireEvent('updateRadioAnswer');
		}
	},
	socialLinkClicked: function(vm) {
		this.set('socialIsPressed', true)
		this.socialDetail.switchToPostMessage()

		var newTo = [],
			currentTo = this.socialDetail.toHidden,
			split = vm.to.split('&&');

		Ext.Array.forEach(currentTo, function(current) {
			newTo.push(current)
		})

		Ext.Array.forEach(split, function(add) {
			var addParams = Ext.Object.fromQueryString(add)
			newTo.push(this.socialDetail.streamToStore.createModel({
				id: addParams.id,
				name: addParams.name,
				type: addParams.type
			}))
		}, this)

		this.socialDetail.set('toHidden', newTo)
	},

	displayFeedback: false,
	questionDisplayItem$: function() {
		return this.displayFeedback ? 1 : 0
	},
	displayQuestion$: function() {
		return this.decisionTreeLoaded
	},

	showQuestionSubmit$: function() {
		return !this.displayFeedback
	},

	documentFrame: null,

	isComboQuestion: false,
	isRadioQuestion: false,

	showRadioAnswers$: function() {
		return this.isRadioQuestion
	},

	height$: function() {
		return this.isComboQuestion || this.isRadioQuestion ? 125 : 36
	},

	activeQuestion: null,
	when_activeQuestionChanges_loadWikiDocumentMetaInformation: {
		on: 'activeQuestionChanged',
		action: function() {
			this.ajax({
				url: '/resolve/service/wiki/get',
				params: {
					id: '',
					name: this.activeQuestion.value.split('?')[0]
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						if (response.data && response.data.accessRights) {
							//Check edit rights to the wiki document to display the edit wiki document icon
							this.set('editWikiIsVisible', hasPermission(response.data.accessRights.uwriteAccess))
							if (response.data.ufullname == this.name)
								this.set('uisRequestSubmission', response.data.uisRequestSubmission);
						} else
							this.set('editWikiIsVisible', false)
					} else {
						clientVM.displayError(response.message);
					}
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
	},
	currentQuestion$: function() {
		return this.activeQuestion ? this.activeQuestion.question : ''
	},
	comboAnswer: '',
	comboAnswerIsVisible$: function() {
		return this.isComboQuestion
	},
	comboAnswers: {
		mtype: 'store',
		fields: ['isSuggested','name','displayText',{
			name : 'displayAnswer',
			convert : function(v, record){
				var isSuggested = record.get('isSuggested') || false;
				var name = record.get('name');
				var displayText = record.get('displayText');
				return name + ( isSuggested ? ' ( ' + displayText + ' )' : '' );
			}
		}, 'value'],
		proxy: {
			type: 'memory'
		}
	},

	radioAnswers: {
		mtype: 'list'
	},

	problemNumber: '',
	problemId: '',
	task: null,

	when_problemId_changes_pull_wsdata: {
		on: ['problemIdChanged'],
		action: function() {
			if (!this.task)
				this.set('task', new Ext.util.DelayedTask(function() {
					this.loadWSData()
				}, this))
			this.task.delay(100)
			Ext.defer(function() {
				this.updateSocial()
			}, 100, this)
		}
	},

	updateSocial: function() {
		this.socialDetail.set('initialStreamId', this.problemId || clientVM.problemId)
		this.socialDetail.set('streamParent', this.problemNumber || clientVM.problemNumber)
	},

	metricData: '',

	dt_qa: {},
	dt_dtList: [],

	loadWSData: function() {
		if (this.problemId) {
			this.set('dt_qa', {})
			this.set('dt_dtList', [])
			this.set('wait', true);
			this.ajax({
				url: '/resolve/service/wsdata/getMap',
				params: {
					problemId: this.problemId
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						if (response.data && response.data.dt_qa)
							this.set('dt_qa', Ext.decode(response.data.dt_qa))

						if (response.data && response.data.dt_dtList)
							this.set('dt_dtList', Ext.isString(response.data.dt_dtList) ? Ext.decode(response.data.dt_dtList) : response.data.dt_dtList)

						this.processDTQA()
						this.saveDTList()
					} else {
						clientVM.displayError(response.message);
						clientVM.selectPRB(null, false)
					}
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				},
				callback: function() {
					setTimeout(function(){
						this.set('wait', false);
					}.bind(this),2000);
				}
			})
		}
		this.updateSocial()
	},

	processDTQA: function() {
		this.questions.removeAll()
		if (this.dt_qa[this.name]) {
			var questions = this.dt_qa[this.name]
			Ext.Array.forEach(questions, function(question) {
				this.questions.add(Ext.apply({
					mtype: 'RS.wiki.DecisionTreeQuestion',
					showAnswerOnHistory : this.showAnswerOnHistory
				}, question))
			}, this)

			if (this.questions.length > 0)
				this.selectQuestion(this.questions.getAt(this.questions.length - 1))
				// this.includeWikiDocument(this.questions.getAt(this.questions.length - 1).value, true)
			this.set('decisionTreeLoaded', true)
		} else if (this.name) {
			this.includeWikiDocument(this.name, true, false)
		}
	},

	saveDTList: function() {
		if (this.problemId && this.active) {
			//populate the dt_information with this decision tree
			var contains = false;
			Ext.Array.forEach(this.dt_dtList, function(item) {
				if (item.name == this.name) {
					contains = true
					item.date = Ext.Date.format(new Date(), 'time')
				}
			}, this)

			if (!contains) {
				this.dt_dtList.push({
					name: this.name,
					date: Ext.Date.format(new Date(), 'time'),
					type: 'dtviewer'
				})
			}

			this.ajax({
				url: '/resolve/service/wsdata/put',
				params: {
					problemId: this.problemId,
					propertyName: 'dt_dtList',
					propertyValue: Ext.encode(this.dt_dtList)
				},
				success: function(resp) {
					var response = RS.common.parsePayload(resp)
					if (!response.success) {
						clientVM.displayError(response.message);
					}
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
	},

	parseResponse: function(responseHTML, value, select) {
		if (responseHTML == '1') 
			responseHTML = Ext.String.format('<div style="padding: 10px;font-size: 16px">{0}</div>', value);
		var decodedHTML = RS.common.decodeHTML(responseHTML);
		var tempEl = document.createElement('div'),
			scripts = '',
			cleaned = decodedHTML.replace(/<script[^>]*>([\s\S]*?)<\/script>/gi, function() {
				scripts += arguments[1] + '\n';
				return '';
			});

		tempEl.innerHTML = cleaned

		//Parse out metric data
		var metrics = Ext.fly(tempEl).query('div[name=data]')
		Ext.Array.forEach(metrics, function(metric) {
			this.set('metricData', metric.innerHTML)
		}, this)

		//Determine if there is a question, if there is then add it to the south region and remove it from the tempEl
		var forms = Ext.fly(tempEl).query('form[name=WikiDecisionForm]');

		this.set('isComboQuestion', false)
		this.set('isRadioQuestion', false)

		if (forms.length > 0) {
			//clear existing radio answers and combobox answers
			this.radioAnswers.removeAll()
			this.comboAnswers.removeAll()
		}

		Ext.Array.forEach(forms, function(form) {
			//We have a question so get the radio/combobox from the form and display it pretty instead of weak and old
			var radios = Ext.fly(form).query('input[type=radio]')
			Ext.Array.forEach(radios, function(radio) {
				var labels = Ext.fly(form).query('label[id=' + radio.id + '_label]'),
					label = '',
					contains = false;

				Ext.Array.forEach(labels, function(l) {
					for (var i = 0; i < l.childNodes.length; i++) {
						if (l.childNodes[i].wholeText || l.childNodes[i].nodeValue)
							label = l.childNodes[1].wholeText || l.childNodes[1].nodeValue
					}
				})

				this.set('isRadioQuestion', true)
				this.radioAnswers.forEach(function(a) {
					if (a.value == radio.value && a.name == label) contains = true
				})
				if (!contains)
					this.radioAnswers.add({
						mtype: 'viewmodel',
						name: label,
						value: radio.value,
						displayAnswer : label,
						checked: false
					})
			}, this)
			this.fireEvent('updateRadioAnswer');
			var options = Ext.fly(form).query('option')
			Ext.Array.forEach(options, function(option) {
				this.set('isComboQuestion', true)
				var contains = false;
				this.comboAnswers.each(function(a) {
					if (a.get('value') == option.value && a.get('name') == option.innerHTML) contains = true
				})
				if (!contains)
					this.comboAnswers.add({
						name: option.innerHTML,
						value: option.value,						
						isSuggested : false
					})
			}, this)

			var questions = Ext.fly(form).query('legend')
			Ext.Array.forEach(questions, function(question) {
				var questionText = question.innerHTML.replace(/<b>/gi, '').replace(/<\/b>/gi, '')
				var q = this.model({
					mtype: 'RS.wiki.DecisionTreeQuestion',
					question: questionText,
					value: value,
					selected: false,
					showAnswerOnHistory : this.showAnswerOnHistory
				})

				var contains = false
				this.questions.forEach(function(temp) {
					if (temp.question == q.question && temp.value == q.value) {
						contains = true
						q = temp
					}
				})

				if (!contains) {
					this.questions.add(q)
					this.saveDTQAData()
				}

				if (select)
					this.selectQuestion(q, false)
			}, this)

			Ext.fly(form).remove()
		}, this)

		var index = this.questions.indexOf(this.activeQuestion),
			val = '';
		var selected = false;
		if (index != this.questions.length - 1 && index > -1 && index < this.questions.length - 1) 
			val = this.questions.getAt(index + 1).value

		if (this.isRadioQuestion) {
			
			this.radioAnswers.forEach(function(answer) {
				if (answer.value == val) {
					answer.set('checked', true)
					selected = true
				}
			})
	
			//if (!selected && this.radioAnswers.length > 0) this.radioAnswers.getAt(0).set('checked', true)
		}

		if (this.isComboQuestion) {
			this.set('comboAnswer', '');
			this.comboAnswers.each(function(answer, idx) {
				if (answer.get('value') == val ){
					this.set('comboAnswer', answer.get('value'));
					selected = true;
				}
			}, this)
		}
		this.set('answerIsSelected', selected);
		if (forms.length == 0) {
			if (select) {
				var end = this.model({
					mtype: 'RS.wiki.DecisionTreeQuestion',
					showAnswerOnHistory : this.showAnswerOnHistory,
					question: this.localize('end'),
					value: value,
					selected: false
				})
				this.questions.add(end)
				this.selectQuestion(end, false)
				this.saveDTQAData()
			}
			this.set('displayFeedback', true)
		} else {
			this.set('displayFeedback', false)
		}	
		Ext.fly(tempEl).remove();

		//Render Content page.
		this.updateContent(tempEl,scripts);
		this.set('decisionTreeLoaded',true);
	},
	content : '',
	setTimeoutId: null,
	updateContent : function(newHTML, scripts){
		// IE does not support Event constructor "new Event('beforedestroy')" so we have to use
		// createEvent/initEvent. Ref: https://developer.mozilla.org/en-US/docs/Web/API/Event/Event
		addLoadEvents = [];
		var event = document.createEvent("Event");
		event.initEvent('beforedestroy', true, false);
		document.dispatchEvent(event);

		//Complete clear previous content.
		this.set('content', null);
		this.set('content', '<div class="wikiBody x-body x-webkit x-chrome">' + newHTML.innerHTML + '</div>');
		var jsFunctions = new Function(scripts);

		if (this.setTimeoutId) {
			clearTimeout(this.setTimeoutId);
		}

		//Push this to end of event queue.
		this.setTimeoutId = setTimeout(function(){
			jsFunctions();
			//Go through the load events configured by the users and execute them
			Ext.Array.forEach(addLoadEvents, function(evt){
				if( Ext.isFunction(evt) ) evt()
				else if( Ext.isString(evt) && Ext.isFunction(window[evt])) window[evt]()
			})
		},1000);
	},
	submitIsEnabled$ : function(){
		return this.answerIsSelected;
	},
	submit: function() {
		var value = '';
		var answerText = '';
		if (this.isRadioQuestion)
			this.radioAnswers.forEach(function(answer) {
				if (answer.checked) {
					value = answer.value;
					answerText = answer.name;
				}
			})

		if (this.isComboQuestion) {
			value = this.comboAnswer
			this.comboAnswers.each(function(answer) {
				if (answer.get('value') == value)
					answerText = answer.get('name');
			})
		}
		this.activeQuestion.set('answerText', answerText);
		// this.activeQuestion.set('value', value);
		//if we're changing the answer from a past question remove future questions if any exist
		var index = this.questions.indexOf(this.activeQuestion)
		if (index != this.questions.length - 1 && index > -1) {
			while (this.questions.length - 1 > index)
				this.questions.removeAt(this.questions.length - 1)
		}
		//Update WSData as user navigate to next page/question.
		var isSubmitted = true;
		this.includeWikiDocument(value, true, false, isSubmitted);
	},

	includeWikiDocument: function(value, select, selectQuestion, isSubmitted) {
		var params = Ext.applyIf({
			docFullName: value,
			metricData: this.metricData,
			PROBLEMID: this.problemId,
			decisiontreeviewer: true,
			selectQuestion: selectQuestion,
			isSubmitted : isSubmitted
		}, Ext.Object.fromQueryString(window.location.search))
		this.ajax({
			url: '/resolve/service/wiki/include',
			params: params,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.parseResponse(r.responseText, value, select)
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	saveDTQAData: function() {
		if (this.problemId && this.active) {
			this.dt_qa[this.name] = this.serializeQuestions()

			//persist the current state to wsdata
			this.ajax({
				url: '/resolve/service/wsdata/put',
				params: {
					problemId: this.problemId,
					propertyName: 'dt_qa',
					propertyValue: Ext.encode(this.dt_qa)
				},
				success: function(resp) {
					var response = RS.common.parsePayload(resp)
					if (!response.success) {
						clientVM.displayError(response.message);
					}
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
	},

	serializeQuestions: function() {
		var serializedQuestions = []
		this.questions.forEach(function(question) {
			serializedQuestions.push(question.asObject())
		})
		return serializedQuestions
	},

	selectQuestion: function(question, include) {
		this.questions.forEach(function(q) {
			q.set('selected', q == question)
		})

		this.set('activeQuestion', question)

		if (include !== false){
			//Dont update WSData as user chose different questions.
			var isSubmitted = false;
			this.includeWikiDocument(this.activeQuestion.value, false, true, isSubmitted);
		}
	},

	previousQuestion: function() {
		if (this.activeQuestion) {
			var index = this.questions.indexOf(this.activeQuestion)
			if (index > 0) this.selectQuestion(this.questions.getAt(index - 1))
		}
	},
	previousQuestionIsEnabled$: function() {
		return this.questions.indexOf(this.activeQuestion) > 0
	},
	nextQuestion: function() {
		if (this.activeQuestion) {
			var index = this.questions.indexOf(this.activeQuestion)
			if (index + 1 < this.questions.length) this.selectQuestion(this.questions.getAt(index + 1))
		}
	},
	nextQuestionIsEnabled$: function() {
		return !this.displayFeedback
	},

	collapseIfNeeded: function() {
		if (!this.pinned) this.set('displayQuestions', false)
	},

	expandIfNeeded: function() {
		this.set('displayQuestions', true)
	},

	pinToggled: function(pinned) {
		this.set('pinned', pinned)
	},

	useful: true,
	rating: 3,
	comment: '',
	submittingFeedback: false,
	submitFeedback: function() {
		this.set('submittingFeedback', true)
		this.ajax({
			url: '/resolve/service/wiki/submitSurvey',
			params: {
				docFullName: this.name,
				useful: this.useful,
				rating: this.rating,
				comment: this.comment
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) clientVM.displaySuccess(this.localize('feedbackSaved'))
				else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('submittingFeedback', false);
			}

		})
	},
	submitFeedbackIsEnabled$: function() {
		return !this.submittingFeedback && this.rating > 0
	},

	refreshQuestions: function() {
		this.loadWSData()
	},
	refreshQuestionsIsEnabled$: function() {
		return !this.wait;
	},
	refreshWiki: function() {
		if (!this.wait) {
			this.loadWSData()
		}
	},
	feedbackTooltip$: function() {
		var date = this.ulastReviewedOn ? Ext.Date.format(new Date(this.ulastReviewedOn), 'Y-m-d G:i:s') : this.localize('never'),
			reviewer = this.ulastReviewedBy || this.localize('noone');
		return this.localize('lastReviewedText', [date, reviewer])
	},
	uisRequestSubmission: false,
	feedbackWin: null,
	feedback: function(button) {
		if (this.feedbackWin) {
			this.feedbackWin.doClose()
			this.feedbackWin = null
		} else {
			this.feedbackWin = this.open({
				mtype: 'RS.wiki.Feedback',
				target: button.getEl().id,
				markForReview: this.uisRequestSubmission
			})
			this.feedbackWin.on('closed', function() {
				this.feedbackWin = null
			}, this)
		}
	},
	feedbackCls$: function() {
		return this.uisRequestSubmission ? 'icon-flag rs-wiki-flagged' : 'icon-flag-alt'
	},

	editWiki: function() {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			params: {
				name: (this.activeQuestion.value || '').split('?')[0],
				isEditMode: true
			},
			target: '_blank'
		})
	},
	editWikiIsVisible: false

})

glu.defModel('RS.wiki.DecisionTreeQuestion', {
	fields: ['question', 'value', 'answerText'],
	question: '',
	value: '',
	answerText: '',
	showAnswerOnHistory : false,
	answer$: function() {
		if (!this.answerText)
			return '';
		return this.localize('answer', this.answerText);
	},
	displayQuestion$ : function(){
		return (!this.showAnswerOnHistory || this.question == "End" ) ? this.question : ('<b>Q:&nbsp;' + this.question + '</b>');
	},
	displayAnswer$ : function(){
		return 'A:&nbsp;' + this.answerText;
	},
	answerIsHidden$ : function(){
		return this.question == "End" || !this.showAnswerOnHistory || this.answerText == "";
	},
	selected: false,
	selectedCss$: function() {
		return this.selected ? 'selected' : 'unselected';
	},
	containerCls$ : function(){
		return (!this.showAnswerOnHistory ? '' : 'decision-tree-qa-container') + (this.selected ? '-selected' : '');
	},
	select: function() {
		this.parentVM.selectQuestion(this)
	}
})
function suggestionAnswer(answer, displayText){
	if(clientVM)
		clientVM.fireEvent('dtSuggestionAnswerChanged',answer, displayText);
}
function activateAnswer(answer){
	if(clientVM)
		clientVM.fireEvent('dtActivateAnswer',answer);
}
