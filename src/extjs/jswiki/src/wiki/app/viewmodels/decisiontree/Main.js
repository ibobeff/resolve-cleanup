glu.defModel('RS.decisiontree.Main',{
	masked: false,
	componentVisibility : 'hidden',
	contentPage : {
		mtype : 'ContentPage',
	},
	navigationPanel : {
		mtype : 'Navigation'
	},

	wait: false,
	isAutoAnswerDisabled : true,
	dtName : '',
	metricData : '',
	problemId : '',
	sirContext : false,
	pageInfo : null,
	prevAnswer : null, //prevAnswer work as a link to QA
	dt_qa : {},
	dt_dtList : [],
	
	API : {
		getWiki : '/resolve/service/wiki/get',
		getWSData : '/resolve/service/wsdata/getMap',
		updateWSData : '/resolve/service/wsdata/put',
		getPageInfo : '/resolve/service/wiki/include',
		newWS : '/resolve/service/worksheet/newWorksheet',
		setActiveWS : '/resolve/service/worksheet/setActive',
		resetDTData: '/resolve/service/playbook/artifact/resetDTData'
	},

	//Initialization
	activate : function(screen, params){
		this.set('componentVisibility','hidden');
		this.set('dtName', params ? params.name : '');
		this.set('problemId', params && params.problemId ? params.problemId : clientVM.problemId);

		// In SIR this will be entry point for legacy app
		if (params.SIR_PROBLEMID) {
			clientVM.SIR_PROBLEMID = params.SIR_PROBLEMID;
			this.set('problemId', params.SIR_PROBLEMID);
			this.set('sirContext', true); // sirContent always true if SIR_PROBLEMID exist
		}
		if (params.SIR_SELECTED_ACTIVITY) {
			clientVM.SIR_SELECTED_ACTIVITY = JSON.parse(params.SIR_SELECTED_ACTIVITY);			
		}

		this.resetDecisionTree();
		//Load Theme first then update DT based on history
		this.loadDtOption(this.getHistory);
	},
	deactivate : function(){
		this.navigationPanel.resetQuestion();
	},
	loadDtOption : function(callback){
		this.ajax({
			url : this.API['getWiki'],
			params : {
				id : '',
				name : this.dtName
			},
			success : function(r){
				var response = RS.common.parsePayload(r);
				if(!response.success){
					clientVM.displayError(response.message);
					return;
				}
				var pageData = response.data;
				this.set('hasPermissionToEdit', hasPermission(response.data.accessRights.uwriteAccess));
				this.set('uisRequestSubmission', pageData.uisRequestSubmission);
				var dtOptions = ( pageData && pageData.udtoptions && pageData.udtoptions.toLowerCase() != 'undefined') ? Ext.decode(pageData.udtoptions) : null;
				if(dtOptions && dtOptions.dtVariables)
					this.navigationPanel.updateExecutionVariableList(dtOptions.dtVariables);
				if(dtOptions && dtOptions.dtLayout)
					this.updateTheme(dtOptions.dtLayout);
				callback.apply(this);
				setTimeout(function(){
					this.set('componentVisibility','visible');
				}.bind(this),500)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	updateTheme : function(data){
		if(!data)
			return;
		var questionOnTop = data.questionOnTop;
		var navigationOnLeft = 'west'; // data.navOnLeft;
		this.set('isAutoAnswerDisabled', !data.isAutoAnswerDisabled);
		this.contentPage.updatePadding(questionOnTop);
		this.navigationPanel.updateRegion(navigationOnLeft ? 'west' : 'east');
		this.navigationPanel.updateTheme(data);
	},
	resetDecisionTree : function(){
		this.set('prevAnswer', this.dtName);
		this.set('dt_qa',{});
		this.set('dt_dtList',[]);
		this.navigationPanel.resetQuestion();
		this.navigationPanel.resetNavigation();
	},

	//History related Logic
	getHistory : function(){
		if (this.problemId) {
			this.set('dt_qa', {})
			this.set('dt_dtList', [])
			this.ajax({
				url: this.API['getWSData'],
				params: {
					problemId: this.problemId
				},
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if(!response.success){
						clientVM.displayError(response.message);
						return;
					}
					if (response.success) {
						if (response.data && response.data.dt_qa)
							this.set('dt_qa', Ext.decode(response.data.dt_qa));

						if (response.data && response.data.dt_dtList)
							this.set('dt_dtList', Ext.isString(response.data.dt_dtList) ? Ext.decode(response.data.dt_dtList) : response.data.dt_dtList)

						this.processHistory();
						this.saveDecisionTreeInfoIntoWS();
					}
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},
	processHistory : function(){
		//Make sure to use the correct entry for this dt
		if(this.dt_qa[this.dtName]){
			var historyInfo = this.dt_qa[this.dtName];
			Ext.Array.each(historyInfo,function(historyEntry){
				this.navigationPanel.addNewEntry({
					question : historyEntry.question,
					questionDisplayText : historyEntry.questionDisplayText,
					wsdata : historyEntry.wsdata
				});
				this.navigationPanel.updateSelectedEntryAnswer(historyEntry.answerInfo,historyEntry.answerText);
				this.navigationPanel.updateSelectedEntryPreviousAnswer(this.prevAnswer);
				if(historyEntry.answerInfo)
					this.set('prevAnswer', historyEntry.answerInfo);
			},this)
			//Reselect last question on the list.
			if(historyInfo.length > 0)
				this.reselectQuestion(this.navigationPanel.selectedEntry);
		}
		else {
			//If there is no entry for this dt then just start as new.
			var callback = this.updateDecisionView;
			var isReselected = true;
			this.getCurrentPageInfo(this.dtName, isReselected, callback);
		}
	},
	saveDecisionTreeInfoIntoWS : function(){
		if (this.problemId) {
			//populate the dt_information with this decision tree
			var contains = false;
			Ext.Array.forEach(this.dt_dtList, function(item) {
				if (item.name == this.dtName) {
					contains = true
					item.date = Ext.Date.format(new Date(), 'time')
				}
			}, this)

			if (!contains) {
				this.dt_dtList.push({
					name: this.dtName,
					date: Ext.Date.format(new Date(), 'time'),
					type: 'dtviewer'
				})
			}

			this.ajax({
				url: this.API['updateWSData'],
				params: {
					problemId: this.problemId,
					propertyName: 'dt_dtList',
					propertyValue: Ext.encode(this.dt_dtList)
				},
				success : function(r){
					var response = RS.common.parsePayload(r);
					if(!response.success){
						clientVM.displayError(response.message);
						return;
					}
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},
	saveHistoryIntoWS : function(){
		if (this.problemId) {
			this.dt_qa[this.dtName] = this.navigationPanel.getSerializeQuestions();			

			//persist the current state to wsdata
			this.ajax({
				url: this.API['updateWSData'],
				params: {
					problemId: this.problemId,
					propertyName: 'dt_qa',
					propertyValue: Ext.encode(this.dt_qa)
				},
				success : function(r){
					var response = RS.common.parsePayload(r);
					if(!response.success){
						clientVM.displayError(response.message);
						return;
					}
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},

	//Processing Logic
	when_auto_answer_changed : {
		on : ['isAutoAnswerDisabledChanged'],
		action : function(){
			this.navigationPanel.set('isAutoAnswerDisabled', !this.isAutoAnswerDisabled);
		}
	},

	getCurrentPageInfo : function(value, isReselected, callback){
		var urlParams = Ext.Object.fromQueryString(window.location.search || '');
		var params = Ext.applyIf({
			docFullName: value,
			metricData: this.metricData,
			PROBLEMID: clientVM.problemId,
			decisiontreeviewer: true,
			selectQuestion: isReselected,
			renderDtQaAsData: true
		}, urlParams);
		this.set('masked', true);
		this.ajax({
			url: this.API['getPageInfo'],
			params: params,
			success: function(r) {
				// Note: API "/resolve/service/wiki/include" returns encoded data in r.responseText so doesn't require call to RS.common.parsePayload()
				var pageInfo = this.processCurrentPageInfo(r.responseText);
				if(Ext.isFunction(callback))
					callback.apply(this,[pageInfo]);
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('masked', false);
			}
		})
	},

	getQAData: function(data) {
		var qaInfo = data.replace(/<div class="x-hidden" name="[\d]+"><\/div>/g, '')
					.replace('"answers":{"', '"answers":[{"')
					.replace(/(EXECUTION_LIST=\[[\s\S]*?\])/g, function(imatch, ip1) {
						return ip1.replace(/"/g, '\\"');
					})
					.replace(/(EXPRESSION=\[[\s\S]*?\])/g, function(imatch2, ip2) {
						return ip2.replace(/"/g, '\\"');
					});
		qaInfo = JSON.parse(qaInfo);
		return qaInfo;
	},

	processCurrentPageInfo: function(responseHTML){
		this.navigationPanel.resetQuestion();
		var containsVar = false;
		var tempEl = document.createElement('div');
		var decodedHTML = RS.common.decodeHTML(responseHTML);
		var currentSelectedAnswer = this.navigationPanel.getSelectedEntryAnswerInfo();
		var currentPage = currentSelectedAnswer ? currentSelectedAnswer.split('?')[0] : this.dtName;
		var	scripts = '';
		var	cleaned = decodedHTML.replace(/<script[^>]*>([\s\S]*?)<\/script>/gi, function() {
			scripts += arguments[1] + '\n';
			return '';
		});
		var qaInfo = null;
		var vm = this;
		var regEx = /(<meta id='RSDecisionTreeQuestionAndAnswersInfo' data-DT-question-answers=([\s\S]+?)><\/meta>)/gi;
		var results = null;
		var qas = [];
		while ((results = regEx.exec(cleaned)) !== null) {
			var qa = this.getQAData(results[2]);
			cleaned = cleaned.replace(results[1], '');
			regEx.lastIndex = 0; // Reset regEx
			qas.push(qa);
		}
		// Merge questions and answers from different sources.
		var qaInfo = qas[0] || {};
		for (var i=1; i<qas.length; i++) {
			qaInfo.answers = qaInfo.answers.concat(qas[i].answers);
		}
		if (qas.length) {
			var lastQqInfo = qas[qas.length-1];
			qaInfo.question = lastQqInfo.question;
			qaInfo.recommendationText = lastQqInfo.recommendationText;
			qaInfo.metricData = lastQqInfo.metricData;
		}
		var answers = (qaInfo && qaInfo.answers) || [];
		vm.set('metricData', RS.common.encodeForURL(qaInfo.metricData)); //For Backend
		answers.forEach(function(answer) {
			answer.answerText = RS.common.decodeHTML(answer.answerText);
			answer.answerInfo = RS.common.decodeHTML(answer.answerInfo);
			(answer.answerInfo == currentSelectedAnswer) ? answer.selected = true : answer.selected = false;
		});
		tempEl.innerHTML = cleaned
		return {		
			questionInfo : {
			 	question : (qaInfo && qaInfo.question) || '',
			 	recommendationText : (qaInfo && qaInfo.recommendationText) || '',
			 	answerList : answers
		 	},			
			pageContent : {
				html : tempEl,
				scripts : scripts,
				pageName : currentPage
			}
		}
	},
	updateDecisionView : function(pageInfo,isReselected){
		if(pageInfo){
			//Save page for now and decide how to display it later.
			this.set('pageInfo', pageInfo);

			//Render everything first then replace once data is ready.
			this.navigationPanel.updateQuestion(pageInfo['questionInfo'], isReselected);
			//Reselect Question : ignore update navigation and execution.
			if(!isReselected) {
				this.navigationPanel.addNewEntry(Ext.apply(pageInfo['questionInfo'], { questionDisplayText : this.navigationPanel.getQuestionDisplayText()}));
				this.navigationPanel.updateSelectedEntryPreviousAnswer(this.prevAnswer);
				this.navigationPanel.selectedEntry.populateAnswers(pageInfo['questionInfo']);
				//Check for WSDATA variables
				this.ajax({
					url: this.API['getWSData'],
					params: {
						problemId: this.problemId
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r);
						if(!response.success){
							clientVM.displayError(response.message);
							return;
						}
						if (response.success) {
							var WSDATA = response.data;
							this.navigationPanel.rerenderDisplayText(this.navigationPanel.getQuestionDisplayText(), WSDATA);
							this.saveHistoryIntoWS();

							//Initial Auto Matching Process.
							this.navigationPanel.startEvaluation(WSDATA);
						}						
					},
					failure : function(){
						clientVM.displayError(this.localize('failedToLoadMsg'));
					}
				});
			} else {
				var selectedQuestion = this.navigationPanel.selectedEntry;
				selectedQuestion.populateAnswers(pageInfo['questionInfo']);
				this.navigationPanel.rerenderDisplayText(this.navigationPanel.getQuestionDisplayText(), selectedQuestion['wsdata']);
			}
		}
	},
	//Question Box will control when page rendered, since execution and auto answer logic is in question box.
	renderPage : function(){
		if(this.pageInfo)
			this.contentPage.updateContent(this.pageInfo['pageContent']);		
	},

	//Navigation Logic
	navigateToNextPage : function(answer, answerText, isAutoAnswer){
		//Update History/Navigation with selected answer then save to WS
		this.navigationPanel.updateSelectedEntryAnswer(answer, answerText, isAutoAnswer);
		this.saveHistoryIntoWS();

		//Previous answer will be used to navigate back to this question when that entry is selected from navigation.
		this.set('prevAnswer', answer);
		var callback = this.updateDecisionView;
		var isReselected = false;
		this.getCurrentPageInfo(answer, isReselected, callback);
	},
	reselectQuestion : function(selectedEntry){
		var isReselected = true;
		this.getCurrentPageInfo(selectedEntry.prevAnswer, isReselected, function(pageInfo){
			this.updateDecisionView(pageInfo, isReselected);
		});
	},


	//Decision Tree's edit, restart, etc.. Logic
	restartDecisionTree : function(){
		this.message({
			title: this.localize('newExecutionTitle'),
			msg: this.localize('confirmNewExecutionMsg'),
			buttonText: {
				ok : this.localize('confirm'),
				no : this.localize('cancel')
			},
			fn: function(btn) {
				if (btn == 'ok'){
					if(!this.sirContext){
						this.ajax({
							url: this.API['newWS'],
							method: 'POST',
							params : {
								'RESOLVE.ORG_NAME' : clientVM.orgName,
								'RESOLVE.ORG_ID' : clientVM.orgId
							},		
							success: function(r) {
								var response = RS.common.parsePayload(r);
								if(response.success)
									this.setActive(response.data);
								else
									clientVM.displayError(response.message);
							},
							failure: function(resp) {
								clientVM.displayFailure(resp);
							}
						})
					}
					else { //DEPRECATED
						this.ajax({
							url : this.API['resetDTData'],
							method: 'POST',								
							params: {
								problemId: this.problemId,
								dtFullName: this.dtName
							},				
							success: function(r) {
								var response = RS.common.parsePayload(r)
								if (response.success) {									
									this.resetDecisionTree();
									this.processHistory();
									clientVM.displaySuccess(this.localize('resetDTDataSucceeded'));									
								} else
									clientVM.displayError(response.message);
							},
							failure: function(resp) {
								clientVM.displayFailure(resp);
							}
						})
					}					
				}
			}
		});
	},
	setActive: function(id, number, callback) {
		this.ajax({
			url: this.API['setActiveWS'],
			params: {
				id: id,
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displayMessage(null, '', {
						msg: (number ? this.localize('worksheetSetActiveSuccess', [number, id]) : this.localize('newWorksheetActiveSuccess', id))
					});
					clientVM.updateProblemInfo(id, number);
					this.set('problemId', clientVM.problemId);

					this.resetDecisionTree();
					this.getHistory();
				} else
				clientVM.displayError(response.message);
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	hasPermissionToEdit : true,
	editDecisionTreeIsVisible$ : function(){
		return this.hasPermissionToEdit;
	},
	editDecisionTree: function() {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			params: {
				name: this.dtName,
				isEditMode: true,
				activeTab :3
			},
			target: '_blank'
		})
	},
	uisRequestSubmission : false,
	addFeedback : function(){
		this.open({
			mtype: 'RS.wiki.Feedback',
			markForReview: this.uisRequestSubmission
		})
	},
	viewCurrentPage : function(){
		var pageName = this.navigationPanel.getSelectedEntryPrevAnswer().split('?')[0];
		if(pageName && pageName.indexOf('_Decision') != -1){
			return;
		}
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			params: {
				name: pageName,
				isEditMode: true,
				activeTab :0
			},
			target: '_blank'
		})
	},
	refreshCurrentPage : function(){
		this.set('wait', true);
		this.renderPage();
		setTimeout(function(){
			this.set('wait', false);
		}.bind(this),2000);
	},
	refreshCurrentPageIsEnabled$ : function() {
		return this.wait == false;
	}
})
