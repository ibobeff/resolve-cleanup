glu.defModel('RS.decisiontree.DocumentPicker', {	
	dumper: null,
	runbookOnly: false,
	builderOnly: false,
	moduleColumns : [],
	runbookGrid : null,
	runbookSelected: null,
	selectedNamespace: '',
	runbookTree: {
        mtype: 'treestore',
        fields: ['id', 'unamespace'],

        proxy: {
            type: 'ajax',
            url: '/resolve/service/nsadmin/list',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },
	init : function(){
		var store = Ext.create('Ext.data.Store', {
            mtype: 'store',
            sorters: ['ufullname'],
            fields: ['id', 'ufullname', 'usummary', 'uresolutionBuilderId', 'uhasResolutionBuilder', {
                name: 'uhasActiveModel',
                type: 'bool'
            }, {
                name: 'uisRoot',
                type: 'bool'
            }].concat(RS.common.grid.getSysFields()),
            remoteSort: true,
            proxy: {
                type: 'ajax',
                url: (!this.runbookOnly && !this.builderOnly) ? '/resolve/service/wiki/list' : '/resolve/service/wikiadmin/listRunbooks',
                reader: {
                    type: 'json',
                    root: 'records'
                },
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
            }
        });
        this.set('runbookGrid', store);
        this.appendExtraParams();     
       

        this.set('moduleColumns', [{
            xtype: 'treecolumn',
            dataIndex: 'unamespace',
            text: this.localize('namespace'),
            flex: 1
        }])

        this.runbookTree.on('load', function(store, node, records) {
            Ext.Array.forEach(records || [], function(record) {
                record.set({
                    leaf: true
                })
            })
        })

        this.runbookTree.setRootNode({
            unamespace: this.localize('all'),
            expanded: true
        })

        this.runbookGrid.load()
	},
    menuPathTreeSelectionchange: function(selected, eOpts) {
        if (selected.length > 0) {
            var nodeId = selected[0].get('unamespace')
            this.set('selectedNamespace', nodeId)
        }
    },
    when_selected_menu_node_id_changes_update_grid: {
        on: ['selectedNamespaceChanged'],
        action: function() {
            this.runbookGrid.load()
        }
    },
	appendExtraParams: function() {
		this.runbookGrid.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {};
			var filter = [];
			if (this.selectedNamespace && this.selectedNamespace.toLowerCase() != 'all')
				filter.push({
					field: 'unamespace',
					type: 'auto',
					condition: 'equals',
					value: this.selectedNamespace
				});
			Ext.apply(operation.params, {
				filter: Ext.encode(filter)
			})
		}, this);
	},
	editRunbook: function(name) {
        clientVM.handleNavigation({
            modelName: 'RS.wiki.Main',
            target: '_blank',
            params: {           
                name: name
            }
        })
    },
	select: function() {
		if (this.dumper)
			this.dumper.dump.call(this.dumper.scope, [this.runbookSelected]);
		this.cancel();
	},
	selectIsEnabled$: function() {
		return !!this.runbookSelected;
	},	
	cancel: function() {
		if (this.activeItem == 1)
			this.set('activeItem', 0)
		else
			this.doClose();
	},
	//TODO: REMOVE
	/*
	newDocument: function() {
		this.set('activeItem', 1);
	},
	newDocumentIsVisible$: function() {
		return this.activeItem == 0;
	},
	createDocument: function() {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			params: {
				name: this.namespace + '.' + this.name
			},
			target: '_blank'
		});
		if (this.dumper)
			this.dumper.dump.call(this.dumper.scope, [{
				ufullname: this.namespace + '.' + this.name,
				get: function(name) {
					return this[name]
				}
			}]);
		this.doClose();
	},
	createDocumentIsVisible$: function() {
		return this.activeItem == 1;
	}*/
});