glu.defModel('RS.decisiontree.History',{
	historyEntryList: {
		mtype: 'list',
		autoParent: true
	},
	selectedHistoryEntry : null,
	addNewEntry : function(entry){
		this.historyEntryList.add({
			mtype : 'HistoryEntry',
			question : entry['question'],
			answerText : entry['answerText'],
			answer : entry['answer'],
			showAnswerOnHistory : true
		})
	},
	clearHistory : function(){
		this.historyEntryList.removeAll();
	},
	selectHistoryEntry : function(historyEntry){
		this.set('selectedHistoryEntry', historyEntry);
		//SHOULD DO THIS ON PARENT
		this.parentVM.getCurrentPageInfo(historyEntry.answer);
	}
})