glu.defModel('RS.decisiontree.ViewSettings', {
	autoConfirm: false,
	fields : ['recommendationFont','recommendationFontSize','recommendationFontColor','questionFont','questionFontColor','questionFontSize','answerFontColor','answerFont','answerFontSize','confirmationFontColor','confirmationBGColor','confirmationFont','confirmationFontSize','buttonText', 'navigationFontColor', 'navigationBGColor', 'navigationFontSize', 'navigationFont', {
		name : 'autoConfirm',
		type : 'bool'
	},{
		name :  'questionOnTop',
		type : 'bool'
	}, {
		name : 'navOnLeft',
		type: 'bool'
	},{
		name : 'verticalAnswerLayout',
		type : 'bool'
	},{
		name : 'isAutoAnswerDisabled',
		type : 'bool'
	}],	
	init : function(){		
		var dtLayout = this.rootVM.dtLayout	
		if(dtLayout)
			this.loadData(dtLayout);
		else
			this.loadData(this.defaultTheme);
	},
	defaultTheme : {},
	fonts: {
		mtype: 'store',
		fields: ['display', 'value'],
		data: [
			{display: 'Arial', value: 'Arial'},
			{display: 'Impact', value: 'Impact'},
			{display: 'Helvetica', value: 'Helvetica'},
			{display: 'Verdana', value: 'Verdana'},
			{display: 'Times New Roman', value: 'Times New Roman'},
			{display: 'Georgia', value: 'Georgia'},
			{display: 'Courier New', value: 'Courier New'}
		]
	},

	fontSizes: {
		mtype: 'store',
		fields: ['display', 'value'],
		data: [
			{display: '6pt', value: '6'},
			{display: '8pt', value: '8'},		
			{display: '10pt', value: '10'},		
			{display: '13pt', value: '13'},		
			{display: '18pt', value: '18'},
			{display: '24pt', value: '24'},
			{display: '30pt', value: '30'},
			{display: '36pt', value: '36'},
			{display: '48pt', value: '48'},
			{display: '60pt', value: '60'},
		]
	},

	updateQuestionFontColor: function(color) {
		this.set('questionFontColor', color);
	},
	updateRecommendationFontColor: function(color) {
		this.set('recommendationFontColor', color);
	},

	updateAnswerFontColor: function(color) {
		this.set('answerFontColor', color);
	},

	updateConfirmationFontColor: function(color) {
		this.set('confirmationFontColor', color);
	},

	updateConfirmationBGColor: function(color) {
		this.set('confirmationBGColor', color);
	},

	updateNavigationBGColor: function(color) {
		this.set('navigationBGColor', color);
	},


	returnQuestionSwatch$: function() {
		return '<div style="height:20px;width:20px;background-color:#' + this.questionFontColor + '"></div>'
	},
	returnAnswerSwatch$: function() {
		return '<div style="height:20px;width:20px;background-color:#' + this.answerFontColor + '"></div>'
	},
	returnRecommendationSwatch$: function() {
		return '<div style="height:20px;width:20px;background-color:#' + this.recommendationFontColor + '"></div>'
	},
	returnConfirmationSwatch$: function() {
		return '<div style="height:20px;width:20px;background-color:#' + this.confirmationFontColor + '"></div>'
	},

	returnConfirmationBGSwatch$: function() {
		return '<div style="height:20px;width:20px;background-color:#' + this.confirmationBGColor + '"></div>'
	},

	returnNavigationBGSwatch$: function() {
		return '<div style="height:20px;width:20px;background-color:#' + this.navigationBGColor + '"></div>'
	},

	restoreDefault : function(){
		this.loadData(this.defaultTheme);
	},
	confirm : function(){
		var data = this.asObject();		
		this.rootVM.updateDTTheme(data);
		this.doClose();
	},
	close: function() {
		this.doClose();
	}
});