glu.defModel('RS.wiki.Templates', {
	mock: false,

	activate: function() {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.loadTemplates();
	},

	displayName: '~~wikiLookups~~',
	stateId: 'wikitemplates',
	state: '',
	store: {
		mtype: 'store',
		fields: ['id', 'uname', 'uwikiDocName', 'uformName', 'udescription'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wikitemplate/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	columns: [{
		header: '~~uname~~',
		dataIndex: 'uname',
		filterable: true,
		sortable: true,
		flex: 1,
		renderer: RS.common.grid.plainRenderer()
	}, {
		header: '~~uwikiDocName~~',
		dataIndex: 'uwikiDocName',
		filterable: true,
		sortable: true,
		flex: 1,
		renderer: RS.common.grid.linkRenderer('RS.wiki.Main', 'name', '_blank')
	}, {
		header: '~~uformName~~',
		dataIndex: 'uformName',
		filterable: true,
		sortable: true,
		flex: 1,
		renderer: RS.common.grid.linkRenderer('RS.formbuilder.Main', 'name', '_blank')
	}, {
		header: '~~udescription~~',
		dataIndex: 'udescription',
		filterable: false,
		sortable: false,
		flex: 1,
		renderer: RS.common.grid.plainRenderer()
	}].concat(RS.common.grid.getSysColumns()),

	templatesSelections: [],

	init: function() {
		if (this.mock) this.backend = RS.wiki.createMockBackend(true)
		this.set('displayName', this.localize('Templates'));
	},

	loadTemplates: function() {
		this.set('state', 'wait');
		this.store.load({
			scope: this,
			callback: function(rec, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('ListTemplateErr'));
					return;
				}
				this.set('state', 'ready');
			}
		});
	},

	createTemplate: function() {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Template'
		});
	},

	editTemplate: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Template',
			params: {
				id: id
			}
		});
	},

	deleteTemplates: function() {
		this.message({
			title: this.localize('DeleteTitle'),
			msg: this.localize(this.templatesSelections.length > 1 ? 'DeletesMsg' : 'DeleteMsg', {
				num: this.templatesSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirmDelete'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.sendDeleteReq
		});
	},

	sendDeleteReq: function(btn) {
		if (btn == 'no')
			return;

		this.set('state', 'wait');

		var ids = [];

		Ext.each(this.templatesSelections, function(r) {
			ids.push(r.get('id'));
		});

		this.ajax({
			url: '/resolve/service/wikitemplate/delete',
			jsonData: ids,
			scope: this,
			success: function(resp) {
				this.handleDeleteSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	handleDeleteSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('DeleteErrMsg') + '[' + respData.message + ']');
		} else
			clientVM.displaySuccess(this.localize('DeleteSucMsg'));

		this.loadTemplates();
	},

	rename: function() {
		this.open({
			mtype: 'RS.wiki.RenameTemplate',
			oldName: this.templatesSelections[0].get('uname'),
			action: 'rename'
		});
	},

	renameIsEnabled$: function() {
		return this.state != 'wait' && this.templatesSelections.length === 1;
	},

	copy: function() {
		this.open({
			mtype: 'RS.wiki.RenameTemplate',
			oldName: this.templatesSelections[0].get('uname'),
			action: 'copy'
		});
	},

	copyIsEnabled$: function() {
		return this.state != 'wait' && this.templatesSelections.length === 1;
	},

	deleteTemplatesIsEnabled$: function() {
		return this.state == 'itemSelected';
	},

	createTemplateIsEnabled$: function() {
		return this.state != 'wait';
	},

	when_templatesSelection_changed: {
		on: ['templatesSelectionsChanged'],
		action: function() {
			if (this.templatesSelections.length > 0)
				this.set('state', 'itemSelected');
			else
				this.set('state', 'ready');
		}
	}
});