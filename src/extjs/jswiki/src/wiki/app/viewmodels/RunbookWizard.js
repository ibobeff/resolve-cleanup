glu.defModel('RS.wiki.RunbookWizard', {
	index: -1,

	searchText: '',
	when_searchText_changes_update_runbooks_grid: {
		on: ['searchTextChanged'],
		action: function() {
			this.runbooks.load()
		}
	},

	runbooks: {
		mtype: 'store',
		fields: ['id', 'ufullname'],
		sorters: [{
			property: 'ufullname',
			direction: 'ASC'
		}],
		remoteSort: true,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wikiadmin/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	runbooksSelections: [],
	runbooksColumns: [{
		header: '~~ufullname~~',
		dataIndex: 'ufullname',
		filterable: true,
		flex: 1
	}],

	init: function() {
		this.runbooks.on('beforeload', function(store, operation) {
			operation.params = operation.params || {}

			if (this.searchText) {
				operation.params.filter = Ext.encode([{
					field: 'ufullname',
					type: 'auto',
					condition: 'contains',
					value: this.searchText
				}])
			}
		}, this)

		this.runbooks.load()
	},

	create: function() {
		this.parentVM.set('name', 'RBF_' + this.runbooksSelections[0].get('ufullname').replace(/\./gi, '_').replace(/ /gi, '_').toUpperCase())
		clientVM.handleNavigation({
			modelName: 'RS.formbuilder.Main',
			params: {
				name: this.parentVM.name
			},
			target: '_blank'
		})
		this.doClose()
	},
	createIsEnabled$: function() {
		return this.runbooksSelections.length == 1
	},
	cancel: function() {
		this.doClose()
	}
})