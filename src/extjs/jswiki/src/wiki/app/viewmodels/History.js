glu.defModel('RS.wiki.History', {
	store: {
		mtype: 'store',
		fields: ['fullName', 'last'],
		proxy: {
			type: 'memory'
		}
	},
	list: {
		mtype: 'list'
	},
	keyword: '',
	searchNow: function() {
		var wikiHistory = Ext.state.Manager.get('wikiHistory');
		var me = this;
		this.populateList(wikiHistory, this.list, {
			mtype: 'RS.wiki.HistoryRenderer'
		}, function(historyItem) {
			if (historyItem.fullName && historyItem.fullName.toLowerCase().indexOf(me.keyword.toLowerCase()) != -1)
				return true;
			return false;
		});
	},
	init: function() {
		var wikiHistory = Ext.state.Manager.get('wikiHistory');
		this.populateList(wikiHistory, this.list, {
			mtype: 'RS.wiki.HistoryRenderer'
		});
	},
	populateList: function(records, list, config, filter) {
		list.removeAll();
		var newItem = null;
		Ext.each(records, function(r) {
			if (filter && !filter(r))
				return;
			newItem = this.model(Ext.apply(Ext.apply({}, config), r));
			list.add(newItem);
		}, this)
	},
	selected: null,
	selectionChange: function(id) {
		this.set('selected', id);
	},
	cancel: function() {
		this.doClose();
	},
	go: function() {
		//double click should automatically go to the document as well
		this.parentVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			params: {
				name: this.selected
			}
		});
		this.doClose();
	},
	goIsEnabled$: function() {
		return this.selected;
	}
});
glu.defModel('RS.wiki.HistoryRenderer', {
	fullName: '',
	id: '',
	lastViewed: '',
	lastViewedDate$: function() {
		var formater = clientVM.getUserDefaultDateFormat();
		return Ext.Date.format(new Date(parseInt(this.lastViewed)), formater);
	},
	title: '',
	init: function() {
		if (!this.title)
			this.set('title', this.fullName);
	},
	gotoWiki: function() {
		this.parentVM.parentVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			params: {
				id: this.id
			}
		});
		this.parentVM.doClose();
	}
});