glu.defModel('RS.wiki.Viewer', {
	content: '',
	items: [],

	itemList: {
		mtype: 'list',
		autoParent: true
	},

	init: function() {
		clientVM = window.top.clientVM

		try {
			var decoded = Ext.decode('[' + this.content + ']')
			Ext.Array.forEach(decoded, function(decode) {
				this.itemList.add(Ext.apply({
					mtype: 'viewmodel'
				}, decode))
			}, this)
		} catch (e) {
			//this is bad... it means the wiki isn't rendering valid json

		}
		// Ext.Array.forEach(this.items || [], function(item) {
		// 	this.itemList.add(this.model({
		// 		mtype: 'RS.wiki.Section'
		// 	}))
		// }, this)
	}
})