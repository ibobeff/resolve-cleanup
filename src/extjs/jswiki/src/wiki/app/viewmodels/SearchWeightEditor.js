glu.defModel('RS.wiki.SearchWeightEditor',{
	ids:[],
	weight:0,
	wait:false,
	dumper:null,
	weightIsValid$:function(){
		var digitTest = /^\d(\.\d+)*$/;
		return digitTest.test(this.weight)?true:this.localize('invalidWeight');
	},

	addSearchWeight:function(){
		this.dumper({weight:this.weight});
		this.doClose();
	},

	addSearchWeightIsEnabled$:function(){
		return !this.wait&&this.isValid;
	},

	cancel:function(){
		this.doClose();
	}
});