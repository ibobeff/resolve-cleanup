/*
glu.defModel('RS.wiki.MoveOrRename', {
	moveRenameCopyTitle: '',
	action: '',
	wait: false,
	namespace: '',


	namespaceIsValid$: function() {
		return this.namespace ? true : this.localize('invalidNamespace');
	},

	namespaceIsEnabled$: function() {
		return !this.wait;
	},

	namespaceIsVisible$: function() {
		return this.parentVM.viewmodelName != 'AttachmentsAdmin';
	},

	filename: '',

	filenameIsVisible$: function() {
		return this.parentVM.recordsSelections.length === 1 && this.parentVM.viewmodelName != 'NamespaceAdmin';
	},

	filenameIsValid$: function() {
		return this.filename && !/^\s+.*$/.test(this.filename) ||(this.parentVM.recordsSelections&&this.parentVM.recordsSelections.length) > 1 ? true : this.localize('invalidFilename');
	},

	store: {
		mtype: 'store',
		fields: ['unamespace'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wiki/namespace/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			},
			extraParams: {
				limit: -1
			}
		}
	},
	overwrite: false,
	skip: true,
	update: false,
	title: '',
	activate: function() {},

	dumper: null,

	init: function() {
		this.set('moveRenameCopyTitle', this.localize(this.moveRenameCopyTitle));
		this.store.on('beforeload', function() {
			this.set('wait', true);
		}, this);

		this.store.on('load', function(store, records, suc) {
			if (!suc)
				clientVM.displayError(this.localize('ListNamespaceErrMsg'));
			this.set('wait', false);
		}, this);
		this.loadNamespace();
	},

	loadNamespace: function() {
		this.store.load();
	},

	confirmAction: function() {
		this.dumper({
			action: this.action,
			namespace: this.namespace,
			filename: this.filename,
			choice: this.overwrite ? 'overwrite' : this.skip ? 'skip' : 'update'
			// overwrite:this.overwrite,
			// skip:this.skip,
			// update:this.update
		});
		this.doClose();
	},

	confirmActionIsEnabled$: function() {
		return !this.wait;
	},

	updateIsVisible$: function() {
		return this.action == 'copy';
	},

	close: function() {
		this.doClose();
	}
});
*/