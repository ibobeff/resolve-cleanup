glu.defModel('RS.wiki.resolutionbuilder.PlainTextManualMarkupParser', {
	mixins: ['BaseParser', 'PlainTextParser'],
	init: function() {
		this.variableStore.on('update', function(store, variable) {
			for (var idx = 0; idx < this.markups.length; idx++)
				if (variable.get('vid') == this.markups[idx].actionId) {
					this.markups[idx].variable = variable.get('name');
					this.markups[idx].flow = variable.get('flow');
					this.markups[idx].wsdata = variable.get('wsdata');
					this.markups[idx].wsdata = variable.get('output');
					return;
				}
		}, this);
	},
	isCoveredByCurrentType: function(from, to) {
		var typeSegs = [];
		Ext.each(this.markups, function(m) {
			if (m.action == 'type' || m.action == 'literal')
				typeSegs.push({
					from: m.from,
					to: m.from + m.length
				})
		}, this);
		if (typeSegs.length == 0)
			return false;
		typeSegs.sort(function(a, b) {
			if (a.from > b.from)
				return 1;
			else if (a.from == b.from)
				return 0;
			else
				return -1;
		});

		var merged = [];
		var currentSeg = null;
		Ext.each(typeSegs, function(seg, idx) {
			if (!currentSeg)
				currentSeg = {
					from: seg.from,
					to: seg.to
				};
			if (seg.from > currentSeg.to) {
				merged.push(currentSeg);
				currentSeg = seg;
				return;
			}
			if (seg.to > currentSeg.to)
				currentSeg.to = seg.to;
		}, this);
		merged.push(currentSeg)
		for (var i = 0; i < merged.length; i++)
			if (merged[i].from <= from && merged[i].to >= to)
				return true;

		return false;
	},
	markCapture: function(markup) {
		var me = this;
		this.open({
			mtype: 'RS.wiki.resolutionbuilder.Capture',
			text: this.sample.substring(markup.from, markup.from + markup.length),
			alreadyMarkedUp: this.isCoveredByCurrentType(markup.from, markup.from + markup.length),
			dumper: function() {
				markup.variable = this.name;
				markup.flow = false;
				markup.wsdata = false;
				markup.output = true;
				me.overrideSameTypeMarkup(markup);
				if (this.type == 'useCurrentTypeMarkups')
					return;
				var typeMarkup = Ext.apply({}, markup);
				delete typeMarkup.inlineStyle;
				typeMarkup.action = 'type';
				typeMarkup.type = this.type;
				typeMarkup.actionId = Ext.data.IdGenerator.get('uuid').generate();
				me.overrideSameTypeMarkup(typeMarkup);
			}
		});
	},
	markBeginOfLine: function(markup) {
		for (var i = 0; i < this.markups.length; i++) {
			if (this.markups[i].type == 'beginOfLine') {
				Ext.Array.erase(this.markups, i, 1);
				break;
			}
		}
		this.overrideSameTypeMarkup(markup);
	},
	markEndOfLine: function(markup) {
		for (var i = 0; i < this.markups.length; i++) {
			if (this.markups[i].type == 'endOfLine') {
				Ext.Array.erase(this.markups, i, 1);
				break;
			}
		}
		this.overrideSameTypeMarkup(markup);
	},
	updateVariableBasedOnCurrentMarkups: function() {
		this.variableStore.removeAll();
		Ext.each(this.markups, function(markup) {
			if (markup.action != 'capture')
				return;
			this.variableStore.add({
				vid: markup.actionId,
				name: markup.variable,
				color: markup.inlineStyle && markup.inlineStyle.backgroundColor || '',
				flow: !!markup.flow,
				wsdata: !!markup.wsdata,
				output: !!markup.output
			})
		}, this);
	},
	whenSampleUpdate: function() {
		this.parentVM.tableMarkupParser.set('sample', this.sample);
		this.parentVM.tableMarkupParser.clearParserConfig();
	},
	when_markups_changed: {
		on: ['markupsChanged'],
		action: function() {
			this.updateVariableBasedOnCurrentMarkups();
		}
	},
	whenVariableRemove: function() {
		this.variableStore.remove(this.variableSelections);
		var newMarkups = [];
		Ext.Array.forEach(this.markups, function(markup) {
			if (markup.variable != this.variablesSelections[0].get('name'))
				newMarkups.push(markup);
		}, this)
		this.set('markups', newMarkups);
	},
	whenGetParserConfig: function(type) {
		return {
			options: {
				repeat: this.repeat
			}
		};
	},
	clearParserConfig: function() {
		this.set('markups', []);
		this.variableStore.removeAll();
		this.placeholderStore.removeAll();
	},
	setParserConfig: function(config, type) {
		this.clearParserConfig();
		if (!config)
			return;
		this.set('markups', config.markups || []);
		this.set('repeat', config.option && config.option.repeat ? true : false);
		this.placeholderStore.add(config.placeholders || []);
		Ext.each(config.placeholders, function(ph) {
			this.variableStore.add(Ext.applyIf({
				placeholder: true
			}, ph));
		}, this)
	}
});