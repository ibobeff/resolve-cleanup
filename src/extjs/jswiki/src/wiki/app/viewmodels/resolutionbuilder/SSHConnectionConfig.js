glu.defModel('RS.wiki.resolutionbuilder.SSHConnectionConfig', {
	optionVM: null,
	host: '',
	updateHostVariables: function() {
		this.parentVM.parentVM.parentVM.getRoot().updateGlobalVariableStore(this.hostSource, false, this.parentVM.parentVM);
	},
	port: 22,
	updatePortVariables: function() {
		this.parentVM.parentVM.parentVM.getRoot().updateGlobalVariableStore(this.portSource, false, this.parentVM.parentVM);
	},
	username: '',
	updateUsernameVariables: function() {
		this.parentVM.parentVM.parentVM.getRoot().updateGlobalVariableStore(this.usernameSource, false, this.parentVM.parentVM);
	},
	password: '',
	passwordIsVisible$: function() {
		return this.usernameAndPassword;
	},
	updatePasswordVariables: function() {
		this.parentVM.parentVM.parentVM.getRoot().updateGlobalVariableStore(this.passwordSource, false, this.parentVM.parentVM);
	},
	hostSource: 'CONSTANT',
	hostSourceIsConst$: function() {
		return this.hostSource == 'CONSTANT';
	},
	hostSourceIsWSDATAOrFlow$: function() {
		return this.hostSource == 'FLOW' || this.hostSource == 'WSDATA';
	},
	hostSourceIsParam$: function() {
		return this.hostSource == 'PARAM';
	},
	hostSourceIsProperty$: function() {
		return this.hostSource == 'PROPERTY';
	},
	portSource: 'CONSTANT',
	portSourceIsConst$: function() {
		return this.portSource == 'CONSTANT';
	},
	portSourceIsWSDATAOrFlow$: function() {
		return this.portSource == 'FLOW' || this.portSource == 'WSDATA';
	},
	portSourceIsParam$: function() {
		return this.portSource == 'PARAM';
	},
	portSourceIsProperty$: function() {
		return this.portSource == 'PROPERTY';
	},
	usernameSource: undefined,
	usernameSourceIsConst$: function() {
		return this.usernameSource == 'CONSTANT';
	},
	usernameSourceIsWSDATAOrFlow$: function() {
		return this.usernameSource == 'FLOW' || this.usernameSource == 'WSDATA';
	},
	usernameSourceIsParam$: function() {
		return this.usernameSource == 'PARAM';
	},
	usernameSourceIsProperty$: function() {
		return this.usernameSource == 'PROPERTY';
	},
	passwordSource: undefined,
	passwordSourceIsConst$: function() {
		return this.passwordSource == 'CONSTANT';
	},
	passwordSourceIsVisible$: function() {
		return this.usernameAndPassword;
	},
	passwordSourceIsWSDATAOrFlow$: function() {
		return this.passwordSource == 'FLOW' || this.passwordSource == 'WSDATA';
	},
	passwordSourceIsParam$: function() {
		return this.passwordSource == 'PARAM';
	},
	passwordSourceIsProperty$: function() {
		return this.passwordSource == 'PROPERTY';
	},
	authType: 'usernameAndPassword',
	authTypeSource: 'CONSTANT',
	file: '',
	keyfile: '',
	updateKeyfileVariables: function() {
		this.parentVM.parentVM.parentVM.getRoot().updateGlobalVariableStore(this.keyfileSource, false, this.parentVM.parentVM);
	},
	keyfileIsVisible$: function() {
		return !this.usernameAndPassword;
	},
	keyfileSource: 'CONSTANT',
	keyfileSourceIsConst$: function() {
		return this.keyfileSource == 'CONSTANT';
	},
	keyfileSourceIsVisible$: function() {
		return !this.usernameAndPassword;
	},
	keyfileSourceIsWSDATAOrFlow$: function() {
		return this.keyfileSource == 'FLOW' || this.keyfileSource == 'WSDATA';
	},
	keyfileSourceIsParam$: function() {
		return this.keyfileSource == 'PARAM';
	},
	keyfileSourceIsProperty$: function() {
		return this.keyfileSource == 'PROPERTY';
	},
	passphrase: '',
	updatePassphraseVariables: function() {
		this.parentVM.parentVM.parentVM.getRoot().updateGlobalVariableStore(this.passphraseSource, false, this.parentVM.parentVM);
	},
	passphraseIsVisible$: function() {
		return !this.usernameAndPassword;
	},
	passphraseSource: 'CONSTANT',
	passphraseSourceIsConst$: function() {
		return this.passphraseSource == 'CONSTANT';
	},
	passphraseSourceIsVisible$: function() {
		return !this.usernameAndPassword;
	},
	passphraseSourceIsWSDATAOrFlow$: function() {
		return this.passphraseSource == 'FLOW' || this.passphraseSource == 'WSDATA';
	},
	passphraseSourceIsParam$: function() {
		return this.passphraseSource == 'PARAM';
	},
	passphraseSourceIsProperty$: function() {
		return this.passphraseSource == 'PROPERTY';
	},
	usernameAndPassword: true,
	keyFileNameAndPassphrase: false,
	authTypeChanged$: function() {
		if (this.usernameAndPassword)
			this.set('authType', 'usernameAndPassword');
		else
			this.set('authType', 'keyFileNameAndPassphrase');
	},
	sourceStore$: function() {
		return this.parentVM.parentVM.getRoot().parentVM.sourceStore;
	},
	when_password_source_changed: {
		on: ['passwordSourceChanged'],
		action: function(newValue, oldValue) {
			if (oldValue == 'CONSTANT' || newValue == 'CONSTANT')
				this.set('password', '');
		}
	},
	when_any_source_changed: {
		on: ['hostChanged', 'portChanged', 'usernameChanged', 'passwordChanged', 'keyfileChanged', 'passphraseChanged'],
		action: function(newValue, oldValue, bindingInfo) {
			this.propertyStore.keyword = this[bindingInfo.modelPropName];
		}
	},
	activate: function() {
		if (this.parentVM.parentVM.type != 'ssh')
			return;
		this.loadParams();
	},
	loadParams: function() {
		Ext.each(this.parentVM.parentVM.params, function(param) {
			if (['host', 'port', 'username', 'password', 'authType', 'keyfile', 'passphrase'].indexOf(param.name) == -1) {
				this.optionVM.set(param.name, param.sourceName);
				if (this.optionVM[param.name + 'Source'])
					this.optionVM.set(param.name + 'Source', param.source);
				return;
			}
			if (param.name == 'authType') {
				this.set('keyFileNameAndPassphrase', param.sourceName == 'keyFileNameAndPassphrase');
				this.set('usernameAndPassword', param.sourceName != 'keyFileNameAndPassphrase');
				return;
			}
			//update of source may wipe out the sourceName...so need to update it first
			this.set(param.name + 'Source', param.source);
			this.set(param.name, param.sourceName);
		}, this)
	},
	getAsParams: function() {
		var name = '';
		var params = [];
		for (key in this) {
			if (key.indexOf('Source') == -1 || key.indexOf('Source') != key.length - 6)
				continue;
			name = key.substring(0, key.length - 6);
			params.push({
				name: name,
				sourceName: this[name],
				source: this[key]
			});
		}
		var options = this.optionVM.asObject();
		Ext.Object.each(options, function(k, v) {
			if (k == 'id')
				return;
			if (k == 'queueName')
				params.push({
					name: 'queueName',
					sourceName: v,
					source: options['queueNameSource']
				})
			else
				params.push({
					name: k,
					sourceName: v,
					source: 'CONSTANT'
				});
		}, this)
		return params;
	},
	paramStore$: function() {
		return this.parentVM.parentVM.parentVM.getRoot().paramStore;
	},
	propertyStore$: function() {
		return this.parentVM.parentVM.parentVM.getRoot().propertyStore;
	},
	globalVariableStore$: function() {
		return this.parentVM.parentVM.parentVM.getRoot().globalVariableStore;
	},
	init: function() {
		this.set('passwordSource', 'CONSTANT');
		this.set('usernameSource', 'CONSTANT');
	}
});

glu.defModel('RS.wiki.resolutionbuilder.SSHConnectionConfigOption', {
	fields: ['prompt', {
		name: 'timeout',
		type: 'int'
	}, 'queueName', 'queueNameSource'],
	prompt: '$',
	timeout: 30,
	queueName: '',
	queueNameSource: 'CONSTANT',
	queueNameVariableStore: {
		mtype: 'store',
		fields: ['name', 'task', 'color']
	},
	queueNameSourceChanged$: function() {
		this.set('queueName', '');
	},
	queueNameSourceIsConst$: function() {
		return this.queueNameSource == 'CONSTANT';
	},
	queueNameSourceIsWSDATAOrFlow$: function() {
		return this.queueNameSource == 'WSDATA' || this.queueNameSource == 'FLOW';
	},
	queueNameSourceIsProperty$: function() {
		return this.queueNameSource == 'PROPERTY';
	},
	queueNameSourceIsParam$: function() {
		return this.queueNameSource == 'PARAM';
	},
	updateQueueNameVariables: function() {
		this.parentVM.parentVM.parentVM.getRoot().updateGlobalVariableStore(this.queueNameSource, true);
	},
	sourceStore$: function() {
		return this.parentVM.parentVM.getRoot().parentVM.sourceStore;
	},
	paramStore$: function() {
		return this.parentVM.parentVM.parentVM.getRoot().paramStore;
	},
	propertyStore$: function() {
		return this.parentVM.parentVM.parentVM.getRoot().propertyStore;		
	},
	globalVariableStore$: function() {
		return this.parentVM.parentVM.parentVM.getRoot().globalVariableStore;
	}
});