glu.defModel('RS.wiki.resolutionbuilder.BaseParser', {
	initMixin: function() {
		this.variableStore.on('update', function(store, variable) {
			this.placeholderStore.each(function(ph) {
				if (variable.get('vid') == ph.get('vid')) {
					ph.set('name', variable.get('name'));
					ph.set('flow', variable.get('flow'));
					ph.set('wsdata', variable.get('wsdata'));
					ph.set('output', variable.get('output'));
				}
			}, this);
		}, this);
	},
	getCurrentSysNewLine: function(escape) {
		return Ext.is.Windows ? (escape ? '\\r\\n' : '\r\n') : (escape ? '\\n' : '\n');
	},
	randomColor: {
		// color: function() {
		// 	var s = '';
		// 	var a = '';
		// 	for (var i = 0; i < 3; i++) {
		// 		a = Math.floor((Math.random() * 0.3 + 0.7) * 0xff).toString(16);
		// 		s += a;
		// 		console.log(a);
		// 	}
		// 	console.log(s);
		// 	return s;
		// },
		color: function() {
			var h = Math.floor(Math.random() * 360);
			var s = Math.floor((0.5 + 0.5 * Math.random()) * 100);
			var l = Math.floor((0.4 + 0.6 * Math.random()) * 100);
			return Ext.String.format('hsl({0},{1}%,{2}%)', h, s, l);
		}
	},
	parserIsDirty: false,
	thatSpecChar: String.fromCharCode(167),
	parserAutoGenEnabled: true,
	turnOnAutoGen: function() {
		if (!this.parserAutoGenEnabled)
			return;
		var me = this;
		this.message({
			title: this.localize('warning'),
			msg: this.localize('overrideCustomizedCode'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('override'),
				no: this.localize('cancel')
			},
			fn: function(btn) {
				if (btn == 'yes')
					return;
				this.set('parserAutoGenEnabled', false);
			}
		});
	},
	turnOffAutoGen: function() {
		this.message({
			title: this.localize('readOnly'),
			msg: this.localize('turnOffAutoGenMsg'),
			buttons: Ext.MessageBox.OK
		});
		// this.set('parserAutoGenEnabled', false);
	},
	testData: '',
	sample: 'Output Sample',
	code: '',
	testResultStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},
	markedVariableStore: {
		mtype: 'store',
		fields: ['vid', 'name', 'color', 'flow', 'wsdata', 'output']
	},
	placeholderStore: {
		mtype: 'store',
		fields: ['vid', 'name', 'color', 'flow', 'wsdata', 'output']
	},
	variableStore: {
		mtype: 'store',
		fields: ['vid', 'name', 'color', 'flow', 'wsdata', 'output',{
			name: 'placeholder',
			type: 'bool',
			defaultValue: false
		}]
	},
	variablesSelections: [],
	addPlaceHolder: function() {
		var max = -1;
		this.variableStore.each(function(ph) {
			if (!ph.get('placeholder'))
				return;
			var n = parseInt(ph.get('name').substring(11));
			if (n > max)
				max = n;
		}, this);
		max++;
		var newRecord = {
			vid: Ext.data.IdGenerator.get('uuid').generate(),
			name: 'placeholder' + max,
			color: 'white',
			flow: false,
			wsdata: false,
			output : true,
			placeholder: true
		};
		this.variableStore.add(newRecord);
		this.placeholderStore.add(newRecord)
	},
	whenSampleUpdated: function() {},
	sampleChanged: function(sample, dontLetParentUpdateAgain) {
		if (sample.length > 25000) {
			clientVM.displayError(this.localize('sampleTooLong'));
			return;
		}
		this.set('sample', sample);
		if (dontLetParentUpdateAgain !== true)
			this.parentVM.parsers.forEach(function(parser) {
				if (parser != this)
					parser.sampleChanged(sample, true);
			}, this);
		if (!this.testData)
			this.set('testData', this.sample);
		this.whenSampleUpdated();
		this.clearParserConfig();
	},
	sampleTooLong: function() {
		clientVM.displayError(this.localize('sampleTooLong'));
	},
	whenVariableRemove: function() {},
	removeVariables: function() {
		this.whenVariableRemove();
		var removedPhs = [];
		this.placeholderStore.each(function(item) {
			Ext.each(this.variablesSelections, function(v) {
				if (v.get('vid') == item.get('vid'))
					removedPhs.push(item);
			}, this)
		}, this)
		this.placeholderStore.remove(removedPhs);
		this.variableStore.remove(this.variablesSelections);
	},
	removeVariablesIsEnables$: function() {
		return this.variablesSelections.length == 0;
	},
	activeItem: 0,
	templateTab: function() {
		this.set('activeItem', 0);
	},
	templateTabIsPressed$: function() {
		return this.activeItem == 0;
	},
	codeTab: function() {
		var me = this;
		if (!/^\s*$/.test(this.sample) && this.parserAutoGenEnabled)
			this.parentVM.parentVM.parentVM.parentVM.defaultActionAfterSave(function() {
				me.set('activeItem', 1);
			});
		else
			this.set('activeItem', 1);
	},
	codeTabIsPressed$: function() {
		return this.activeItem == 1;
	},
	testTab: function() {
		var me = this;
		if (!/^\s*$/.test(this.sample) && this.parserAutoGenEnabled)
			this.parentVM.parentVM.parentVM.parentVM.defaultActionAfterSave(function() {
				me.set('activeItem', 2);
			});
		else
			this.set('activeItem', 2);
	},
	testTabIsPressed$: function() {
		return this.activeItem == 2;
	},
	setData: function(data) {
		var config = Ext.decode(data.parserConfig) || {};
		this.setParserConfig(config);
		this.set('sample', data.sampleOutput || '');
		this.set('testData', data.sampleOutput || '');
		this.set('repeat', data.repeat);
		this.set('code', data.parserScript);
		this.set('parserIsDirty', false);
		this.testResultStore.removeAll();
		this.set('parserAutoGenEnabled', data.parserAutoGenEnabled);
		this.parentVM.parsers.forEach(function(parser) {
			if (parser != this)
				parser.sampleChanged(this.sample, true);
		}, this);
	},
	afterSetData: function() {},
	getData: function() {
		return {
			parserConfig: Ext.encode(this.getParserConfig()) + ' ',
			sampleOutput: this.sample,
			parserAutoGenEnabled: this.parserAutoGenEnabled,
			testData: this.testData,
			repeat: this.repeat,
			parserScript: this.code
		}
	},
	resetParser: function() {},
	collectTestData: function() {
		return {};
	},
	pollTest: function(indicator) {
		this.ajax({
			url: '/resolve/service/resolutionbuilder/testCode/poll',
			scope: this,
			success: function(resp) {

				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('pollTestError', respData.message));
					this.parentVM.parentVM.parentVM.parentVM.set('masked', false);
					return;
				}
				var me = this;
				if (!respData.data) {
					this.parentVM.parentVM.parentVM.parentVM.set('masked', false);
					return;
				}
				if (respData.data.stage != 'DONE') {
					setTimeout(function() {
						me.pollTest(indicator);
					});
					return;
				}
				this.parentVM.parentVM.parentVM.parentVM.set('masked', false);
				var result = respData.data.result;
				this.testResultStore.removeAll();
				Ext.Object.each(result, function(k, v) {
					this.testResultStore.add({
						name: k,
						value: v
					});
				}, this);
				indicator.fadeOut({
					remove: true
				});
			},
			failure: function(r) {
				clientVM.displayFailure(r);
				this.parentVM.parentVM.parentVM.parentVM.set('masked', false);
			}
		})
	},
	executeTest: function() {
		var data = this.collectTestData()
		this.ajax({
			url: '/resolve/service/resolutionbuilder/testCode',
			params: {
				code: this.code,
				testData: this.testData
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(respData.message)
					return;
				}
				var indicator = clientVM.displayMessage(this.localize('executeTest'), this.localize('executeTestInProgress'), {
					duration: -1
				});
				this.parentVM.parentVM.parentVM.parentVM.set('masked', true);
				this.pollTest(indicator);
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	markBoundaryError: function() {
		clientVM.displayError(this.localize('markBoundaryErrMsg'));
	}
});