glu.defModel('RS.wiki.resolutionbuilder.Assessor', {
	assessorIsDirty: false,
	init: function() {
		this.conditionRules.on('conditionsIsDirtyChanged', function() {
			this.set('assessorIsDirty', this.assessorIsDirty || this.conditionRules.conditionsIsDirty);
		}, this);
		this.severityRules.on('severitiesIsDirtyChanged', function() {
			this.set('assessorIsDirty', this.assessorIsDirty || this.severityRules.severitiesIsDirty);
		}, this);
	},
	conditionRules: {
		mtype: 'AssessConditionRules'
	},
	severityRules: {
		mtype: 'AssessSeverityRules'
	},
	activeTab: 0,
	assessorScript: '',
	assessorAutoGenEnabled: false,
	turnOnAutoGen: function() {
		if (!this.assessorAutoGenEnabled)
			return;
		var me = this;
		this.message({
			title: this.localize('warning'),
			msg: this.localize('overrideCustomizedCode'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('override'),
				no: this.localize('cancel')
			},
			fn: function(btn) {
				if (btn == 'yes')
					return;
				this.set('assessorAutoGenEnabled', false);
			},
			listeners: {
				close: function() {
					me.set('assessorAutoGenEnabled', false);
				}
			}
		});
	},
	turnOffAutoGen: function() {
		this.message({
			title: this.localize('readOnly'),
			msg: this.localize('turnOffAutoGenMsg'),
			buttons: Ext.MessageBox.OK
		});
		// this.set('assessorAutoGenEnabled', false);
	},
	fields: ['assessorScript', {
		name: 'assessorAutoGenEnabled',
		type: 'bool'
	}],
	setData: function(taskData) {
		this.loadData(taskData);
		this.conditionRules.loadConditions(taskData.conditions);
		if (!taskData.continuation || taskData.continuation.toLowerCase() == 'always') {
			this.conditionRules.set('alwaysContinue', true);
			this.conditionRules.set('stopOnBad', false);
		} else {
			this.conditionRules.set('alwaysContinue', false);
			this.conditionRules.set('stopOnBad', true);
		}

		this.conditionRules.set('goodDefaultCondition', taskData.defaultAssess.toLowerCase() == 'good')
		this.conditionRules.set('badDefaultCondition', taskData.defaultAssess.toLowerCase() == 'bad')

		this.severityRules.set('goodDefaultSeverity', taskData.defaultSeverity.toLowerCase() == 'good')
		this.severityRules.set('warningDefaultSeverity', taskData.defaultSeverity.toLowerCase() == 'warning')
		this.severityRules.set('severeDefaultSeverity', taskData.defaultSeverity.toLowerCase() == 'severe')
		this.severityRules.set('criticalDefaultSeverity', taskData.defaultSeverity.toLowerCase() == 'critical')
		this.severityRules.loadSeverities(taskData.severities);
	},
	getData: function() {
		var assessData = this.asObject();
		Ext.apply(assessData, this.conditionRules.getAssessConditions());
		Ext.apply(assessData, this.severityRules.getAssessSeverities());
		return assessData;
	},
	assessDefinition: function() {
		this.set('activeTab', 0);
	},
	assessDefinitionIsPressed$: function() {
		return this.activeTab == 0;
	},
	assessCode: function() {
		var me = this;
		if (this.assessorAutoGenEnabled && (this.conditionRules.conditions.length != 0 || this.severityRules.conditions.length))
			this.parentVM.parentVM.parentVM.defaultActionAfterSave(function() {
				me.set('activeTab', 1);
			});
		else
			this.set('activeTab', 1);
		// this.set('activeTab', 1);
	},
	assessCodeIsPressed$: function() {
		return this.activeTab == 1;
	},
	resetAssessor: function() {
		this.set('assessorAutoGenEnabled', false);
		this.set('assessorScript', '');
		this.assessDefinition();
		this.conditionRules.resetAssessConditionRules();
		this.severityRules.resetAssessSeverityRules();
	}
});