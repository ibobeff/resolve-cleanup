glu.defModel('RS.wiki.resolutionbuilder.LoopEntryConfig', {
	mixins: ['VariableAware'],
	fields: ['item', 'itemSource'],
	item: '',
	itemSource: '',
	variables: ['item', 'itemSource'],
	defaultData: {
		itemSource: 'FLOW'
	},
	fixed: true,
	list: false,
	itemDelimiter: 'comma',
	delimiterStore: null,
	initConfig: function() {},
	init: function() {
		this.initConfig();
		this.set('fixed', this.count > 0);
		this.set('list', !this.fixed);
		this.set('delimiterStore', Ext.create('Ext.data.Store', {
			fields: ['name', 'value'],
			data: [{
				name: this.localize('comma'),
				value: 'comma'
			}, {
				name: this.localize('space'),
				value: 'space'
			}]
		}));
		this.updateVariables();
	},
	itemChanged$: function() {
		this.propertyStore.keyword = this.item;
	},
	updateVariables: function() {
		this.updateGlobalVariableStore(this.itemSource, false, this.parentVM);
	},
	itemIsEnabled$: function() {
		return !this.fixed;
	},
	itemSourceIsEnabled$: function() {
		return !this.fixed;
	},
	count: '',
	countIsEnabled$: function() {
		return this.fixed;
	},
	sourceStore$: function() {
		return Ext.create('Ext.data.Store', {
			mtype: 'store',
			fields: ['name', 'value'],
			data: [{
				name: 'PARAM',
				value: 'PARAM'
			}, {
				name: 'FLOW',
				value: 'FLOW'
			}, {
				name: 'WSDATA',
				value: 'WSDATA'
			}, {
				name: 'PROPERTY',
				value: 'PROPERTY'
			}]
		});
	},
	dumper: null,
	update: function() {
		this.dumper();
		this.cancel();
	},
	cancel: function() {
		this.doClose();
	}
});