glu.defModel('RS.wiki.resolutionbuilder.RunbookEntry', {
	mixins: ['EntryAccessor', 'Entry', 'DragWatcher'],
	entityType: 'subrunbook',
	id: '',
	name: '',
	fields: ['id', 'name', {
		name: 'order',
		type: 'float'
	}, 'refRunbookParams'],
	margin: '10 30 10 30',
	defaultData: {
		name: 'NewRunbook',
	},
	getData: function() {
		//hack... we need a real vo.
		var template = {
			"command": "",
			"queueName": "RSREMOTE",
			"queueNameSource": "CONSTANT",
			"promptSource": "CONSTANT",
			"promptSourceName": "",
			"parserType": "text",
			"sampleOutput": "",
			"repeat": false,
			"description": "",
			"summary": "${RAW}",
			"detail": "${RAW}",
			"defaultAssess": "GOOD",
			"defaultSeverity": "GOOD",
			"continuation": "ALWAYS",
			"assessorScript": "",
			"parserScript": "",
			"parserAutoGenEnabled": true,
			"assessorAutoGenEnabled": true,
			"expectTimeout": 0,
			"inputfile": '',
			"timeout": 0,
			"encodedCommand": "",
			"conditions": [],
			"severities": [],
			"variables": [],
			"parserConfig": "{\"markups\":[],\"placeholders\":[],\"isInLoop\":false,\"options\":{\"repeat\":false}} "
		}
		var obj = this.asObject();
		obj.entityType = RS.wiki.resolutionbuilder.viewmodels.RunbookEntry.entityType;
		Ext.applyIf(obj, template);
		return obj;
	},
	setData: function(data) {
		this.loadData(data);
	},
	remove: function() {
		this.deleteEntry(function() {
			this.removeFromParent();
		}, this)
	}
});