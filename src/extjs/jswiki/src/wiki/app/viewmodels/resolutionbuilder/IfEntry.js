glu.defModel('RS.wiki.resolutionbuilder.IfEntry', {
	entityType: 'if',
	mixins: ['EntryAccessor', 'Entry', 'EntryContainer', 'DragWatcher'],
	id: '',
	connectionType: '',
	fields: ['id', 'entityType', {
		name: 'order',
		type: 'float'
	}],
	defaultData: {
		entityType: 'if'
	},

	init: function () {
		this.set('connectionType', this.parentVM.type);
	},

	addCondition: function() {
		this.insertAndSave(RS.wiki.resolutionbuilder.viewmodels.ConditionEntry.entityType, 'RS.wiki.resolutionbuilder.ConditionEntry');
	},
	remove: function() {
		this.deleteEntry(function() {
			this.removeFromParentAndUnselectChild();
		}, this)
	}
});