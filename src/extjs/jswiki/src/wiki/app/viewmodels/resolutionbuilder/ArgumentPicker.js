glu.defModel('RS.wiki.resolutionbuilder.ArgumentPicker', {
	mixins: ['VariableAware'],
	addRaw: false,
	excludeSelf: false,
	variables: ['variable'],
	defaultData: {
		variableSource: 'FLOWS'
	},
	variableChanged$: function() {
		this.propertyStore.keyword = this.variable;
	},
	variableSourceStore: null,
	init: function() {
		var store = null;
		if (Ext.isFunction(this.storeBuilder)) {
			store = this.storeBuilder();
			this.set('variableSource', 'FLOWS')
		} else if (this.addRaw) {
			store = Ext.create('Ext.data.Store', {
				fields: ['name', 'value'],
				data: [{
					name: 'FLOWS',
					value: 'FLOWS'
				}, {
					name: 'PARAMS',
					value: 'PARAMS'
				}, {
					name: 'WSDATA',
					value: 'WSDATA'
				}, {
					name: 'PROPERTIES',
					value: 'PROPERTIES'
				}, {
					name: 'RAW',
					value: 'RAW'
				}]
			});
		} else {
			this.set('variableSource', 'PARAM');
			store = this.sourceStore;
		}
		this.set('variableSourceStore', store);
		this.updateVariables();
	},
	dumper: null,
	select: function() {
		this.dumper();
		this.cancel();
	},
	cancel: function() {
		this.doClose();
	},
	updateVariables: function() {
		var typeName = '';
		if (this.variableSource == 'FLOWS')
			typeName = 'FLOW';
		else if (this.variableSource == 'PROPERTIES')
			typeName = 'PROPERTIES';
		else
			typeName = this.variableSource;
		this.updateGlobalVariableStore(typeName, this.excludeSelf);
	}
});