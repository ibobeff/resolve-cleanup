glu.defModel('RS.wiki.resolutionbuilder.XmlParser', {	
	mixins: ['BaseParser'],
	oldSample: '',
	validXml: false,
	markups: [],
	currentColor: '#000000',
	parserInited: false,
	afterSetData: function() {
		this.whenSampleUpdated();
	},
	whenSampleUpdated: function(forClear) {
		this.set('parserInited', true);
		this.set('xpathSelections', []);
		this.set('currentColor', this.randomColor.color());
		this.set('oldSample', this.sample);
		var clean = this.sample.replace(/\r*\n/g, '\n');
		var doc = null;
		var err = false;
		var dp = new DOMParser();
		this.set('validXml', true);
		try{
			doc = dp.parseFromString('<resolveparserroot>' + clean + '</resolveparserroot>', 'text/xml');
		}
		catch(e){
			//For IE invalid xml just return.
			return;
		}
		this.set('validXml', doc.getElementsByTagName('parsererror').length == 0);
		this.set('currentSampleDom', doc);
		
		if (!this.validXml) {
			if (this.parentVM.activeItem == this)
				clientVM.displayError(this.localize('invalidXmlSyntaxError'));
			doc = dp.parseFromString('<error>' + Ext.htmlEncode(clean) + '</error>', 'text/xml');			
			this.set('currentSampleDom', doc)
			this.fireEvent('renderSample');
			return;
		}

		var xmlCount = 0;
		Ext.each(doc.childNodes[0].childNodes, function(node) {
			if (node.nodeType == 1) {
				xmlCount++;
				if (xmlCount > 1)
					node.ignore = true;
			}
		});
		this.set('validXml', xmlCount > 0);
		if (!this.validXml) {
			if (!forClear && this.parentVM.activeItem == this)
				clientVM.displayError(this.localize('noXmlSegFoundError'));
			try{
				doc = dp.parseFromString('<error>' + clean + '</error>', 'text/xml');
			}
			catch(e){
				//For IE invalid xml just return.
				return;
			}			
			this.set('currentSampleDom', doc)
			this.fireEvent('renderSample');
			return;
		}

		this.fireEvent('renderSample');
	},
	checkBoundary: function(from) {
		this.set('endParseIsEnabled', true);
		this.set('stopParseAtStartOfLineIsEnabled', true);
		this.set('beginParseIsEnabled', true);
		this.set('parseFromEndOfLineIsEnabled', true);
		for (var i = 0; i < this.markups.length; i++) {
			if ((this.markups[i].action == 'beginParse' || this.markups[i].action == 'parseFromEndOfLine') && from <= this.markups[i].from) {
				this.set('endParseIsEnabled', false);
				this.set('stopParseAtStartOfLineIsEnabled', false);
			}
			if ((this.markups[i].action == 'endParse' || this.markups[i].action == 'stopParseAtStartOfLine') && from >= this.markups[i].from) {
				this.set('beginParseIsEnabled', false);
				this.set('parseFromEndOfLineIsEnabled', false);
			}
		}
	},
	beginParseIsEnabled: true,
	parseFromEndOfLineIsEnabled: true,
	endParseIsEnabled: true,
	stopParseAtStartOfLineIsEnabled: true,
	repeat: false,
	markups: [],
	type: 'xml',
	clearParserConfig: function() {
		this.set('augmentedXPath', []);
		this.variableStore.removeAll();
		this.placeholderStore.removeAll();
	},
	setParserConfig: function(config) {
		this.clearParserConfig();
		if (!config)
			return;
		this.variableStore.add(config.variables || []);
		this.placeholderStore.add(config.placeholders || []);
		Ext.each(config.placeholders, function(ph) {
			this.variableStore.add(Ext.applyIf({
				placeholder: true
			}, ph));
		}, this)
	},
	getParserConfig: function() {
		var variables = [];
		this.variableStore.each(function(r) {
			variables.push(Ext.apply(r.data, {
				xpath: r.raw.xpath
			}));
		}, this);
		return {
			type: 'xml',
			sample: this.sample,
			variables: variables
		};
	},
	clearSample: function() {
		this.set('sample', '')
		this.whenSampleUpdated(true)
	},
	applyPredicate: false,
	leftOperand: '',
	leftOperandStore: {
		mtype: 'store',
		fields: ['name']
	},
	comparison: '',
	comparisonStore: {
		mtype: 'store',
		fields: ['op']
	},
	rightOperand: '',
	rightOperandStore: {
		mtype: 'store',
		fields: ['value']
	},
	supportActiveXObject: ((Object.getOwnPropertyDescriptor && Object.getOwnPropertyDescriptor(window, "ActiveXObject")) || ("ActiveXObject" in window)),
	init: function() {
		Ext.each(['=', '>', '<', '>=', '<=', '!='], function(op) {
			this.comparisonStore.add({
				op: op
			})
		}, this);
		var doc = null;
		var dp = new DOMParser();
		try{
			doc = dp.parseFromString('<resolveparserroot></resolveparserroot>', 'text/xml');	
		}
		catch(e){
			//For IE invalid xml just return.
			return;
		}	
		this.set('currentSampleDom', doc);
	},
	editting: false,
	editXPath: function() {
		this.set('editting', !this.editting);
	},
	editXPathIsPressed$: function() {
		return this.editting;
	},
	showEdit: true,
	manuallyEditted: false,
	editXPathIsVisible$: function() {
		return this.showEdit;
	},
	selectionToManual: function() {
		this.set('manuallyEditted', true);
		this.set('showEdit', false);
	},
	currentSampleDom: null,
	sampleDomChanged: function(currentSampleDom) {
		this.set('currentSampleDom', currentSampleDom);
	},
	addXPath: function() {
		var me = this;
		this.open({
			mtype: 'RS.wiki.resolutionbuilder.XPathVarNamer',
			dump: function() {
				if (me.variableStore.find('name', this.name) != -1) {
					clientVM.displayError(this.localize('xpathVarAlreadyExist'));
					return;
				}
				var records = me.variableStore.add({
					vid: this.name,
					name: this.name,
					color: me.currentColor,
					xpath: me.textXPath,
					flow: false,
					wsdata: false,
					output : true
				});
				this.cancel();
			}
		})
	},
	addXPathIsVisible$: function() {
		return this.variablesSelections.length != 1 || !this.variablesSelections[0].get('placeholder');
	},
	updateXPath: function() {
		this.variablesSelections[0].raw.xpath = this.textXPath;
	},
	updateXPathIsVisible$: function() {
		return this.variablesSelections.length == 1 && !this.variablesSelections[0].get('placeholder');
	},
	augmentedXPath: {},
	xpath$: function() {
		var tokens = [];
		Ext.each(this.augmentedXPath, function(augmentedToken) {
			tokens.push(this.serializeXPathToken(augmentedToken));
		}, this);
		return '//' + tokens.join('\/');
	},
	textXPath: '',
	when_textXPath_changed_in_edit_mode: {
		on: ['textXPathChanged'],
		action: function() {
			if (this.variablesSelections.length != 1 || this.variablesSelections[0].get('placeholder'))
				this.set('currentColor', this.randomColor.color());
			// if (!this.editting)
			this.xpathQuery();
		}
	},
	serializeXPathToken: function(augmentedToken) {
		if (augmentedToken.type == 'selector')
			return this.serializeSelector(augmentedToken);

		var predicates = [];
		Ext.each(augmentedToken.predicates, function(p) {
			if (p.type == 'text')
				predicates.push(this.serializeTextPredicate(p));
			else
				predicates.push(this.serializeAttributePredicate(p));
		}, this);
		var token = augmentedToken.element;
		if (predicates.length > 0)
			token += '[' + predicates.join(' ' + augmentedToken.op + ' ') + ']'
		return token;
	},
	serializeSelector: function(token) {
		if (token.selection == 'attribute')
			return '@' + token.name;
		return 'text()';
	},
	serializeTextPredicate: function(predicate) {
		if (['contains', 'starts-with', 'ends-with'].indexOf(predicate.condition) == -1)
			return '';
		return Ext.String.format('{0}(., \'{1}\')', predicate.condition, predicate.originValue);
	},
	serializeAttributePredicate: function(predicate) {
		if (['=', '!=', '>', '<', '>=', '<='].indexOf(predicate.condition) == -1)
			return '';
		return Ext.String.format('@{0} {1} {2}', predicate.name, predicate.condition, predicate.isNumber ? predicate.value : '"' + predicate.value + '"');
	},
	confirmSaveXPathChange: function(nextAction) {
		this.message({
			title: this.localize('saveXPathChange'),
			msg: this.localize('confirmSaveXPathChange'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('yes'),
				no: this.localize('no')
			},
			scope: this,
			fn: function(btn) {
				nextAction.call(this, btn);
			}
		});
	},
	variablesSelections: [],
	selectXPath: function(type, containerElementPnid, rendererNode) {
		if (this.editting && this.manuallyEditted) {
			this.confirmSaveXPathChange(function(btn) {
				if (btn == 'yes')
					this.variablesSelections[0].raw.xpath = this.textXPath;
				this.doSelectXPath(type, containerElementPnid, rendererNode);
			})
			return;
		}
		this.doSelectXPath(type, containerElementPnid, rendererNode);
	},
	doSelectXPath: function(type, containerElementPnid, rendererNode) {
		this.set('manuallyEditted', false);
		this.set('editting', false);
		this.set('variablesSelections', []);
		this.set('currentColor', this.randomColor.color());
		var element = Ext.fly(this.currentSampleDom).query('*[pnid="' + containerElementPnid + '"]')[0];
		var current = element;
		var nodes = [current];
		while (current.parentNode && current.parentNode.tagName.toLowerCase() != 'resolveparserroot') {
			current = current.parentNode;
			if (current.nodeType != 1)
				continue;
			nodes.push(current);
		}
		nodes.reverse();
		var tokens = [];
		var augmentedXPath = [];
		var predicates = [];
		Ext.each(nodes, function(node) {
			tokens.push(node.tagName)
			augmentedXPath.push({
				type: 'element',
				element: node.tagName,
				ptid: Ext.data.IdGenerator.get('uuid').generate(),
				pnid: node.pnid,
				op: 'and',
				predicates: []
			});
		});
		var xpath = '//' + tokens.join('/');
		var selectedNode = null;
		Ext.each(element.childNodes, function(node) {
			if (node.pnid == rendererNode.getAttribute('pnid'))
				selectedNode = node;
		}, this);
		if (type == 'text' || type == 'cdata') {
			augmentedXPath[augmentedXPath.length - 1].predicates = [{
				type: 'text',
				pnid: augmentedXPath[augmentedXPath.length - 1].pnid,
				ptid: Ext.data.IdGenerator.get('uuid').generate(),
				condition: 'contains',
				originValue: selectedNode.textContent,
				value: Ext.String.escape(selectedNode.textContent).replace(/\n/g, '\\n').replace(/"/g, '\\"')
			}];
			augmentedXPath.push({
				type: 'selector',
				ptid: Ext.data.IdGenerator.get('uuid').generate(),
				selection: type
			});
		} else if (type == 'attribute') {
			var trimmer = /^\s*(.+)\s*=\s*"(.*)"\s*$/
			var groups = trimmer.exec(rendererNode.textContent);
			var name = groups[1];
			var value = groups[2];
			var isNumberTest = /^[0-9]+([.][0-9]+)?$/
			var isNumber = isNumberTest.test(value);
			augmentedXPath[augmentedXPath.length - 1].predicates = [{
				type: 'attribute',
				pnid: augmentedXPath[augmentedXPath.length - 1].pnid,
				ptid: Ext.data.IdGenerator.get('uuid').generate(),
				name: name,
				condition: '=',
				isNumber: isNumber,
				value: value
			}];
			augmentedXPath.push({
				type: 'selector',
				selection: type,
				ptid: Ext.data.IdGenerator.get('uuid').generate(),
				name: name
			});
		}

		this.set('augmentedXPath', augmentedXPath);
		this.set('textXPath', this.xpath);
		this.xpathQuery();
		if (this.detailWin)
			this.detailWin.doClose();
	},
	xpathSelections: [],
	invalidXPath: false,
	textXPathIsValid$: function() {
		return this.invalidXPath ? this.localize('invalidXPath') : true;
	},
	xpathQuery: function(scroll) {
		var currentSampleDom = this.currentSampleDom;
		var nsResolver = currentSampleDom.createNSResolver(
			currentSampleDom.ownerDocument == null ? currentSampleDom.documentElement : currentSampleDom.ownerDocument.documentElement);
		var newSelections = [];

		try {
			var iterator = currentSampleDom.evaluate(this.textXPath, currentSampleDom, nsResolver, XPathResult.ANY_TYPE, null);
			var thisNode = iterator.iterateNext();
			while (thisNode) {
				newSelections.push(thisNode);
				thisNode = iterator.iterateNext();
			}
			this.set('xpathSelections', newSelections);
			this.set('invalidXPath', false);
		} catch (e) {
			this.set('invalidXPath', true);
			this.set('xpathSelections', []);
			glu.log.error('Error query xml.');
		}
		this.fireEvent('highLightSelections', this.xpathSelections, this.currentColor, scroll);
	},
	when_variablesSelections_changed: {
		on: ['variablesSelectionsChanged'],
		action: function(newSelections, oldSelections) {
			this.set('showEdit', false);
			this.set('editting', true);
			var previousTextXPath = this.textXPath;
			if (this.manuallyEditted && oldSelections.length == 1) {
				this.confirmSaveXPathChange(function(btn) {
					this.set('manuallyEditted', false);
					if (this.variablesSelections.length != 1 || this.variablesSelections[0].get('placeholder'))
						return;
					this.set('currentColor', this.variablesSelections[0].raw.color);
					this.fireEvent('xpathVariableSelectionChanged', this.variablesSelections[0].raw.xpath);
					if (btn == 'no')
						return;
					oldSelections[0].raw.xpath = previousTextXPath;
				});
				return;
			}
			if (this.variablesSelections.length != 1 || this.variablesSelections[0].get('placeholder')) {
				this.set('manuallyEditted', false);
				return;
			}
			this.set('currentColor', this.variablesSelections[0].raw.color);
			this.fireEvent('xpathVariableSelectionChanged', this.variablesSelections[0].raw.xpath);
		}
	},
	removeToken: function(pnid, ptid) {
		var newPath = [];
		Ext.each(this.augmentedXPath, function(token) {
			if (token.ptid == ptid)
				return;
			newPath.push(token);
			if (ptid == 'predicates') {
				token.predicates = [];
				return;
			}
			if (token.predicates)
				token.predicates = Ext.Array.filter(token.predicates, function(p) {
					return p.ptid != ptid;
				});
		}, this);
		this.set('augmentedXPath', Ext.decode(Ext.encode(newPath)));
		this.set('textXPath', this.xpath);
		if (this.detailWin)
			this.detailWin.doClose();
	},
	showDetail: function(pnid, ptid, wndPosAdjustFn) {
		var targetToken = null;
		Ext.each(this.augmentedXPath, function(token) {
			if (pnid == token.pnid) {
				targetToken = token;
				return;
			}
		}, this);
		if (this.detailWin)
			this.detailWin.doClose();

		this.detailWin = this.open({
			mtype: 'RS.wiki.resolutionbuilder.XPathTokenEditor',
			augmentedXPathToken: targetToken,
			currentSampleDom: this.currentSampleDom,
			computeOffset: wndPosAdjustFn
		});
	},
	contentPasted: function(value) {
		this.selectionToManual();
		this.set('textXPath', value);
	},
	codeTabIsEnabled$: function() {
		return this.validXml;
	},
	testTabIsEnabled$: function() {
		return this.validXml;
	},
	editXPathIsEnabled$: function() {
		return this.validXml;
	},
	addXPathIsEnabled$: function() {
		return this.validXml && !this.invalidXPath;
	},
	addPlaceHolderIsVisible$: function() {
		return false;
	}
});

glu.defModel('RS.wiki.resolutionbuilder.XPathVarNamer', {
	name: '',
	xpathVarNameMsg: '',
	init: function() {
		this.set('xpathVarNameMsg', this.localize('xpathVarNameMsg'));
	},
	dump: function() {},
	cancel: function() {
		this.doClose();
	}
})