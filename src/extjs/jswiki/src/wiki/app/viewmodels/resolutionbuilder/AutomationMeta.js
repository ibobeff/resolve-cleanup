glu.defModel('RS.wiki.resolutionbuilder.AutomationMeta', {
	doTitle: function(namespace, name) {
		var title = namespace && name ? (this.general.namespace + '.' + this.general.name) : 'New Automation';
		this.set('title', this.localize('automationBuilder') + ' - ' + title);
	},
	title: '',
	reqCount: 0,

	id: '',
	wikiId: '',
	lockedByUsername: '',
	softLock: false,
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrganizationName: '',
	sourceStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [{
			name: 'CONSTANT',
			value: 'CONSTANT'
		}, {
			name: 'PARAM',
			value: 'PARAM'
		}, {
			name: 'FLOW',
			value: 'FLOW'
		}, {
			name: 'WSDATA',
			value: 'WSDATA'
		}, {
			name: 'PROPERTY',
			value: 'PROPERTY'
		}]
	},

	comparisonStore: null,

	fields: RS.common.grid.getSysFields(),

	activeItem: '',

	general: null,

	generalTab: function() {
		this.tabs.setActiveItem(this.general);
	},

	generalTabIsPressed$: function() {
		return this.activeItem == this.general;
	},

	tasks: {
		mtype: 'Tasks'
	},

	tasksTab: function() {
		this.tabs.setActiveItem(this.tasks);
	},

	tasksTabIsEnabled$: function() {
		return this.general && this.general.isReadOnly;
	},
	tasksTabIsPressed$: function() {
		return this.activeItem == this.tasks;
	},
	tabs: {
		mtype: 'activatorlist',
		focusProperty: 'activeItem',
		autoParent: true
	},

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},
	resetAutomationMeta: function() {
		this.general.resetGeneral();
		this.tasks.resetTasks();
	},
	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('windowTitle'));
		this.setSoftLocked(true);
		this.resetAutomationMeta();
		this.set('id', params && params.id ? params.id : '');
		this.set('wikiId', params && params.wikiId ? params.wikiId : '');
		if (this.wikiId)
			this.getWikiForParams();
		if (this.id)
			this.getGeneral();
		else if (params.name && params.namespace) {
			this.general.set('name', params && params.name || '');
			this.general.set('namespace', params && params.namespace || '');
		} else {
			this.open({
				mtype: 'RS.wiki.resolutionbuilder.ResolutionBuilderRunbookNamer'
			})
		}
		this.getTasks();
		this.tabs.setActiveItem(this.tasks);
	},
	setSoftLocked: function(value) {
		if (!this.id)
			return;
		this.ajax({
			url: '/resolve/service/wiki/setSoftLock',
			params: {
				docName: this.general.namespace + '.' + this.general.name,
				lock: value
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					if (value) {
						if (response.data)
							this.set('lockedByUsername', response.data);
						else
							this.set('lockedByUsername', user.name)
					}
					this.set('softLock', value);
				} else {
					this.set('softLock', !value)
					clientVM.displayError(response.message)
				}
			},
			failure: function(r) {
				this.set('softLock', !value)
				clientVM.displayFailure(r);
			}
		})
	},
	accessRights: null,
	getWikiForParams: function() {
		this.ajax({
			url: '/resolve/service/wiki/get',
			params: {
				id: this.wikiId,
				name: '',
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					//clientVM.displayError(this.localize('getWikiError', respData.message));
					clientVM.displayError(respData.message);
					return;
				}
				var params = [];
				this.set('accessRights', respData.data.accessRights);
				try {
					params = Ext.decode(respData.data.uwikiParameters) || [];
				} catch (e) {
					params = [];
				}
				this.tasks.paramStore.removeAll();
				this.tasks.paramStore.add(params);
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	getGeneral: function() {
		this.set('reqCount', this.reqCount + 1);
		this.ajax({
			url: '/resolve/service/resolutionbuilder/getGeneral',
			params: {
				id: this.id
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					//clientVM.displayError(this.localize('getGeneralError', respData.message));
					clientVM.displayError(respData.message);
				} else if (respData.data) {
					this.populateScreen(respData.data);
					this.setSoftLocked(true);
				} else {
					clientVM.displayError(this.localize('ServerError'));
				}
			},

			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
		});
	},

	getTasks: function() {
		if (!this.id)
			return;
		this.set('reqCount', this.reqCount + 1);
		this.ajax({
			url: '/resolve/service/resolutionbuilder/getRBTree',
			params: {
				id: this.id
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp)
				if (!respData.success) {
					//clientVM.displayError(this.localize('getTreeError', respData.message));
					clientVM.displayError(respData.message);
					return;
				}
				this.tasks.populateScreen(respData.data);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
		});
	},

	populateScreen: function(meta) {
		try {
			meta.params = Ext.decode(meta.wikiParameters) || [];
		} catch (e) {
			meta.params = [];
		}
		this.loadData(meta);
		this.general.populateScreen(meta);
		this.tasks.paramStore.removeAll();
		this.tasks.paramStore.add(meta.params);
	},
	init: function() {
		if (this.mock) this.backend = RS.wiki.resolutionbuilder.createMockBackend(true);
		var general = this.model({
			mtype: 'General'
		});
		general.on('nameChanged', function() {
			this.doTitle(general.namespace, general.name);
		}, this);
		general.on('namespaceChanged', function() {
			this.doTitle(general.namespace, general.name);
		}, this);
		this.set('general', general);
		//general tab is removed but I don't know whether it will be back again.. so.. code is still here
		//this.tabs.add(this.general);
		this.tabs.add(this.tasks);
		this.set('comparisonStore', Ext.create('Ext.data.Store', {
			fields: ['name', 'value'],
			data: [{
				name: this.localize('eq'),
				value: 'eq'
			}, {
				name: this.localize('neq'),
				value: 'neq'
			}, {
				name: this.localize('gt'),
				value: 'gt'
			}, {
				name: this.localize('gte'),
				value: 'gte'
			}, {
				name: this.localize('lt'),
				value: 'lt'
			}, {
				name: this.localize('lte'),
				value: 'lte'
			}, {
				name: this.localize('contains'),
				value: 'contains'
			}, {
				name: this.localize('notContains'),
				value: 'notContains'
			}, {
				name: this.localize('startsWith'),
				value: 'startsWith'
			}, {
				name: this.localize('endsWith'),
				value: 'endsWith'
			}]
		}));
	},
	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			params: {
				name: this.general.namespace + '.' + this.general.name,
				activeTab: 2
			}
		})
	},
	executionWin: null,
	execution: function(button) {
		var me = this;
		if (this.executionWin) {
			this.executionWin.doClose()
		} else {
			this.executionWin = this.open({
				mtype: 'RS.actiontask.Execute',
				target: button.getEl().id,
				fromWiki: true,
				inputsLoader: function(paramStore, mockStore) {
					//Load up the wiki params into the grid and the mock values for the wiki
					me.tasks.paramStore.each(function(param) {
						param.data.value = param.get('sourceName')
						paramStore.add(param)
					})

					var mockNames = Ext.state.Manager.get('wikiMockNames', []),
						mockNamesArray = []
					Ext.Array.forEach(mockNames, function(name) {
						mockNamesArray.push({
							uname: name
						})
					})
					mockStore.loadData(mockNamesArray)
				},
				executeDTO: {
					wiki: this.general.namespace + '.' + this.general.name
				}
			})
			this.executionWin.on('closed', function() {
				this.executionWin = null
			}, this)
		}
	},
	executionIsEnabled$: function() {
		return this.reqCount == 0 && this.generated;
	},
	refresh: function() {
		this.getGeneral();
		if (this.tasks.entrySelections.length > 0) {
			var selection = this.tasks.entrySelections[0]
			this.tasks.getEntry(selection.id, selection.entityType, function(data) {
				selection.setData(data);
				this.populateEditorScreen(selection.getData(), selection);
			}, this.tasks);
		}
	},
	saveGeneral: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveAutomationMetaGeneralInfo, this, [exitAfterSave])
		this.task.delay(500)
	},
	generalTabIsValid: false,
	saveGeneralIsEnabled$: function() {
		return !this.reqCount && this.generalTabIsValid && this.tasks.editorIsValid;
	},
	saveGeneralAndExit: function() {
		if (!this.saveGeneralIsEnabled)
			return;
		if (this.task) this.task.cancel()
		this.saveGeneral(true)
	},
	pollABGeneration: function(indicator) {
		this.ajax({
			url: '/resolve/service/resolutionbuilder/generate/poll',
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('pollABGenerationError', respData.message));
					this.set('masked', false);
					return;
				}
				// indicator.set('stage', respData.data.stage);
				if (respData.data) {
					if (!respData.data.success) {
						clientVM.displayError(this.localize('abGenerationError', respData.data.message));
						this.set('masked', false);
						return;
					}
					if (respData.data.stage != 'DONE') {
						var me = this;
						setTimeout(function() {
							me.pollABGeneration(indicator);
						}, 500);
						return;
					}
				}
				clientVM.displaySuccess(this.localize('abGenerationSuccess'));
				this.set('masked', false);
				indicator.fadeOut();
				this.set('generated', true);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	generated: false,
	masked: false,
	generate: function() {
		this.message({
			title: this.localize('generateRunbook'),
			msg: this.localize('confirmGenerateRunbook', {
				name: this.general.name,
				namespace: this.general.namespace
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('generateRunbook'),
				no: this.localize('cancel')
			},
			fn: function(btn) {
				if (btn != 'yes')
					return;

				function doGenerate() {
					this.set('reqCount', this.reqCount + 1);
					this.ajax({
						url: '/resolve/service/resolutionbuilder/generate',
						params: {
							id: this.general.id
						},
						scope: this,
						success: function(resp) {
							var respData = RS.common.parsePayload(resp);
							if (!respData.success) {
								clientVM.displayError(this.localize('generateReqError', respData.message));
								return;
							}
							this.set('masked', true);
							var indicator = clientVM.displayMessage(this.localize('generateAutomation'), this.localize('generateReqSuccess'), {
								duration: -1
							});
							// var indicator = this.open({
							// 	mtype: 'RS.wiki.resolutionbuilder.StageIndicator',
							// 	title: this.localize('generateAutomation')
							// });
							if (respData.data == 'INPROGRESS')
								this.localize('abGenerationInProgress');
							this.pollABGeneration(indicator);
						},
						failure: function(resp) {
							clientVM.displayFailure(resp);
						},
						callback: function() {
							this.set('reqCount', this.reqCount - 1);
						}
					})
				}
				var me = this;

				this.saveAutomationMetaGeneralInfo(false, function(respData) {
					me.defaultActionAfterSave(function() {
						doGenerate.call(me);
					})
				}, true);
			}
		});
	},
	generateIsEnabled$: function() {
		return this.saveGeneralIsEnabled && this.reqCount == 0;
	},
	defaultActionAfterSave: function(nextAction) {
		if (this.tasks.entrySelections.length > 0) {
			var me = this;
			this.tasks.entrySelections[0].setData(Ext.applyIf({
				order: this.tasks.entrySelections[0].order
			}, this.tasks.collectEditorScreen()));
			this.tasks.entrySelections[0].saveEntry(function(respData) {
				me.tasks.entrySelections[0].setData(respData.data);
				me.tasks.populateEditorScreen(me.tasks.entrySelections[0].getData(), me.tasks.entrySelections[0]);
				if (nextAction)
					nextAction();
				else
					clientVM.displaySuccess(me.localize('saveGeneralAndTaskSuccess'));
			});
		} else {
			if (nextAction)
				nextAction();
			else
				clientVM.displaySuccess(this.localize('saveGeneralAndTaskSuccess'));
		}
	},
	saveAutomationMetaGeneralInfo: function(exitAfterSave, nextAction, donDoPopUp) {
		//two sequential reqs here
		var general = this.general.collect();
		var params = [];
		this.tasks.paramStore.each(function(param) {
			params.push(param.raw);
		}, this);
		general.noOfColumn = this.tasks.noOfColumn;
		general.newWorksheet = this.tasks.newWorksheet;
		general.wikiParameters = Ext.encode(params) + ' ';
		general.wikiId = this.wikiId;
		this.set('reqCount', this.reqCount + 1);
		this.ajax({
			url: '/resolve/service/resolutionbuilder/saveGeneral',
			jsonData: general,
			scope: this,
			success: function(resp) {
				//don't reduce req counter coz need to do it in the 2nd req
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('saveGeneralError', respData.message));
					if (respData.data.id && respData.data.id != 'UNDEFINED' && respData.data.lockedByUser && respData.data.lockedByUser != user.name) {
						this.set('softLock', true)
						this.set('lockedByUsername', respData.data.lockedByUser);
						return;
					}
					if (!donDoPopUp)
						this.handleSaveGeneralError();
					return;
				}
				this.populateScreen(respData.data);
				if (!nextAction)
					this.defaultActionAfterSave();
				else
					nextAction(respData);
				if (exitAfterSave === true)
					this.back();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
		})
	},

	handleSaveGeneralError: function() {
		this.general.set('name', '');
		this.general.set('namespace', '');
		this.open({
			mtype: 'RS.wiki.resolutionbuilder.ResolutionBuilderRunbookNamer'
		});
	}
});

glu.defModel('RS.wiki.resolutionbuilder.StageIndicator', {
	title: '',
	stage: ''
});

glu.defModel('RS.wiki.resolutionbuilder.ResolutionBuilderRunbookNamer', {
	name: '',
	namespace: '',
	namespaceStore: {
		mtype: 'store',
		fields: ['unamespace'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/nsadmin/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	create: function() {
		this.parentVM.general.set('name', Ext.String.trim(this.name));
		this.parentVM.general.set('namespace', Ext.String.trim(this.namespace));
		this.parentVM.saveAutomationMetaGeneralInfo();
		this.doClose();
	},

	cancel: function() {
		if (this.wikiId)
			clientVM.handleNavigationBack();
		else
			clientVM.handleNavigation({
				modelName: 'RS.wiki.resolutionbuilder.Automations'
			})
		this.doClose();
	}
});