glu.defModel('RS.wiki.resolutionbuilder.ConditionContainer', {
	conditions: {
		mtype: 'list'
	},
	conditionsSelections: [],
	updateRuleSelections: function(selected, multiselect) {
		var newSelections = [];
		this.conditions.forEach(function(r) {
			if (multiselect)
				r.set('selected', !(r == selected && r.selected));
			else
				r.set('selected', r == selected);

			if (r.selected)
				newSelections.push(r);
		}, this);
		this.set('conditionsSelections', newSelections);
	},
	addCondition: function(modelName, config, init) {
		var vm = this.model(modelName, config || {})
		if (init) {
			init(vm);
		}
		this.conditions.add(vm);
		return vm;
	},
	removeConditions: function() {
		Ext.each(this.conditionsSelections, function(c) {
			this.conditions.remove(c);
		}, this);
		this.set('conditionsSelections', []);
	},
	removeCondition: function(conditionVM) {
		this.conditions.remove(conditionVM);
	},
	removeConditionsIsEnabled$: function() {
		return this.conditionsSelections.length > 0;
	},
	clearConditions: function() {
		this.conditions.removeAll();
	}
});

glu.defModel('RS.wiki.resolutionbuilder.BasicCondition', {
	selected: false,
	toggleSelection: function(multiselect) {
		this.parentVM.updateRuleSelections(this, multiselect);
	}
});