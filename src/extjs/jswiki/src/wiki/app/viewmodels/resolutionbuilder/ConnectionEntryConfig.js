glu.defModel('RS.wiki.resolutionbuilder.ConnectionEntryConfig', {
	ssh: true,
	telnet: false,
	database: false,
	http: false,
	local: false,
	type: '',
	activeConfig: null,
	showOption: false,
	configs: {
		mtype: 'activatorlist',
		focusProperty: 'activeConfig',
		autoParent: true
	},
	sshConfig: {
		mtype: 'SSHConnectionConfig'
	},
	telnetConfig: {
		mtype: 'TelnetConnectionConfig'
	},
	httpConfig: {
		mtype: 'HTTPConnectionConfig'
	},
	localConfig: {
		mtype: 'LocalConnectionConfig'
	},
	activeOption: null,
	options: {
		mtype: 'activatorlist',
		focusProperty: 'activeOption',
		autoParent: true
	},
	sshOption: {
		mtype: 'SSHConnectionConfigOption'
	},
	telnetOption: {
		mtype: 'TelnetConnectionConfigOption'
	},
	httpOption: {
		mtype: 'HTTPConnectionConfigOption'
	},
	localOption: {
		mtype: 'LocalConnectionConfigOption'
	},
	init: function() {
		this.sshConfig.set('optionVM', this.sshOption);
		this.telnetConfig.set('optionVM', this.telnetOption);
		this.localConfig.set('optionVM', this.localOption);
		this.httpConfig.set('optionVM', this.httpOption);
		this.configs.add(this.sshConfig);
		this.configs.add(this.telnetConfig);
		this.configs.add(this.httpConfig);
		this.configs.add(this.localConfig);
		this.options.add(this.sshOption);
		this.options.add(this.telnetOption);
		this.options.add(this.httpOption);
		this.options.add(this.localOption);
	},
	toggleTypes$: function() {
		if (this.ssh)
			this.set('type', 'ssh');
		else if (this.database)
			this.set('type', 'database');
		else if (this.http)
			this.set('type', 'http');
		else if (this.local)
			this.set('type', 'local');
		else
			this.set('type', 'telnet');
		this.configTab();
	},
	typeChanged$: function() {
		if (this.configs.length == 4 && this.options.length == 4) {
			this.configs.setActiveItem(this[this.type + 'Config']);
			this.options.setActiveItem(this[this.type + 'Option']);
			this[this.type + 'Config'].activate();
		}
	},
	dumper: null,
	update: function() {
		var typeSpecificParams = null;
		if (this[this.type + 'Config'].getAsParams)
			typeSpecificParams = this[this.type + 'Config'].getAsParams();
		else
			typeSpecificParams = [];
		if (this.dumper)
			this.dumper(typeSpecificParams);
		this.cancel();
	},
	cancel: function() {
		this.doClose();
	},
	optionTab: function() {
		this.set('showOption', true);
	},
	optionTabIsPressed$: function() {
		return this.showOption;
	},
	configTab: function() {
		this.set('showOption', false);
	},
	configTabIsPressed$: function() {
		return !this.showOption;
	}
});