glu.defModel('RS.wiki.resolutionbuilder.HTTPCommand', {
	mixins: ['ConditionContainer'],
	commandUrl: '',
	method: '',
	postMethod: true,
	headers: {
		mtype: 'list'
	},
	body : null,
	syntax : null,
	syntaxStore :null,
	init : function(){
		var syntaxStore = Ext.create('Ext.data.Store',{
			fields: ['name', 'value'],
			data: [{
				name: this.localize('text'),
				value: 'text'
			}, {
				name: this.localize('json'),
				value: 'json'
			}, {
				name: this.localize('xml'),
				value: 'xml'
			}]
		});
		this.set('syntaxStore', syntaxStore);
		this.set('syntax', 'text');
	},
	addParam: function() {
		var me = this;
		this.open({
			mtype: 'ArgumentPicker',
			excludeSelf: true,
			storeBuilder: function() {
				return Ext.create('Ext.data.Store', {
					fields: ['name', 'value'],
					data: [{
						name: 'FLOWS',
						value: 'FLOWS'
					}, {
						name: 'PARAM',
						value: 'PARAM'
					}, {
						name: 'WSDATA',
						value: 'WSDATA'
					}, {
						name: 'PROPERTIES',
						value: 'PROPERTIES'
					}]
				});
			},
			dumper: function() {
				//me.set('commandUrl', (me.commandUrl || '') + (me.commandUrl ? ' ' : '') + Ext.String.format('${0}\{{1}\}', this.variableSource, this.variable));
				me.set('commandUrl', (me.commandUrl || '') + Ext.String.format('${0}\{{1}\}', this.variableSource, this.variable))
			}
		});
	},
	addBodyParam: function() {
		var me = this;
		this.open({
			mtype: 'ArgumentPicker',
			excludeSelf: true,
			storeBuilder: function() {
				return Ext.create('Ext.data.Store', {
					fields: ['name', 'value'],
					data: [{
						name: 'FLOWS',
						value: 'FLOWS'
					}, {
						name: 'PARAMS',
						value: 'PARAMS'
					}, {
						name: 'WSDATA',
						value: 'WSDATA'
					}, {
						name: 'PROPERTIES',
						value: 'PROPERTIES'
					}]
				});
			},
			dumper: function() {
				me.fireEvent('insertAtCursor',  Ext.String.format('${0}\{{1}\}', this.variableSource, this.variable));
			}
		});
	},	
	addHeader: function() {
		this.headers.add(this.model({
			mtype : 'RS.wiki.resolutionbuilder.HTTPHeader',
			id : Ext.data.IdGenerator.get('uuid').generate()
		}))
	},	
	methodChange$ : function(){
		this.set('method', this.postMethod ? "POST" : "GET");
	},
	commandUrlIsValid$ : function(){
		if(Ext.isFunction(this.rootVM.tasks.set)){
			var regExp = new RegExp(/[^a-zA-Z0-9\-\.\_\~\:\/\?\#\[\]\@\!\$\&\'\(\)\*\+\,\;\=\{\}]+/);
			var match = regExp.exec(this.commandUrl);
			if( match == null){
				this.rootVM.tasks.set('editorIsValid', true);
				return true;
			}
			else{		
				this.rootVM.tasks.set('editorIsValid', false);
				return this.localize('invalidURL', [match.index]);
			}		
		}
	},
	getData: function() {
		var headers = [];
		this.headers.each(function(field){
			headers.push({
				name : field.name,
				value : field.value
			})
		})		
		return {
			method: this.method,
			commandUrl: this.commandUrl,
			headers: headers,
			body: this.body
		};
	},
	setData : function(data){
		this.set('postMethod', data.method == "GET" ? false : true);
		this.set('commandUrl', data.commandUrl);
		this.headers.removeAll();
		Ext.Array.each(data.headers, function(field){
			this.headers.add(this.model('HTTPHeader',field));
		},this);
		this.set('body',data.body);		
	},
	isUniqueHeader : function(name, id){
		var isUnique = true;
		this.headers.each(function(header){
			if(header.name == name && header.id != id){
				isUnique  = false;
			}
		})
		return isUnique;
	}
});

glu.defModel('RS.wiki.resolutionbuilder.HTTPHeader', {
	mixins: ['BasicCondition'],
	name: '',
	value: '',
	nameIsValid$ : function(){
		if(this.parentVM.isUniqueHeader(this.name, this.id)){
			this.rootVM.tasks.set('editorIsValid', true);
			return true;
		}
		else{
			this.rootVM.tasks.set('editorIsValid', false);
			return this.localize('invalidHeaderName');
		}
	},
	remove: function() {
		this.parentVM.headers.remove(this);
	}
});

glu.defModel('RS.wiki.resolutionbuilder.POSTParam', {
	mixins: ['BasicCondition'],
	name: '',
	value: '',
	remove: function() {
		this.parentVM.postParams.remove(this);
	}
});