glu.defModel('RS.wiki.resolutionbuilder.PlainTextParser', {
	mixins: ['BaseParser'],

	checkBoundary: function(from) {
		this.set('endParseIsEnabled', true);
		this.set('stopParseAtStartOfLineIsEnabled', true);
		this.set('beginParseIsEnabled', true);
		this.set('parseFromEndOfLineIsEnabled', true);
		for (var i = 0; i < this.markups.length; i++) {
			if ((this.markups[i].action == 'beginParse' || this.markups[i].action == 'parseFromEndOfLine') && from <= this.markups[i].from) {
				this.set('endParseIsEnabled', false);
				this.set('stopParseAtStartOfLineIsEnabled', false);
			}
			if ((this.markups[i].action == 'endParse' || this.markups[i].action == 'stopParseAtStartOfLine') && from >= this.markups[i].from) {
				this.set('beginParseIsEnabled', false);
				this.set('parseFromEndOfLineIsEnabled', false);
			}
		}
	},
	beginParseIsEnabled: true,
	parseFromEndOfLineIsEnabled: true,
	endParseIsEnabled: true,
	stopParseAtStartOfLineIsEnabled: true,
	repeat: false,
	markups: [],
	type: 'text',
	//mark**
	//this is not elegant coz originally we didnt consider markup with 0 length...
	markEndParse: function(markup) {
		for (var i = 0; i < this.markups.length; i++) {
			if (this.markups[i].action == 'endParse' || this.markups[i].action == 'stopParseAtStartOfLine') {
				Ext.Array.erase(this.markups, i, 1);
				break
			}
		}
		this.overrideSameTypeMarkup(markup);
	},
	markBeginParse: function(markup) {
		for (var i = 0; i < this.markups.length; i++) {
			if (this.markups[i].action == 'beginParse' || this.markups[i].action == 'parseFromEndOfLine') {
				Ext.Array.erase(this.markups, i, 1);
				break;
			}
		}
		this.overrideSameTypeMarkup(markup);
	},
	markParseFromEndOfLine: function(markup) {
		var beginParse = Ext.apply({
			fromTheEndOfLine: true
		}, markup);
		beginParse.action = 'beginParse';
		var subStrEndOffset = this.sample.substring(markup.from).indexOf(this.getCurrentSysNewLine());
		if (subStrEndOffset != -1)
			markup.from = markup.from + subStrEndOffset;
		else
			markup.from = this.sample.length;
		markup.length = 0;

		for (var i = 0; i < this.markups.length; i++) {
			if (this.markups[i].action == 'beginParse' || this.markups[i].action == 'parseFromEndOfLine') {
				Ext.Array.erase(this.markups, i, 1);
				break;
			}
		}
		this.overrideSameTypeMarkup(markup);
		this.overrideSameTypeMarkup(beginParse);
	},
	markStopParseAtStartOfLine: function(markup) {
		var endParse = Ext.applyIf({
			stopParseAtStartOfLine: true,
			action: 'endParse'
		}, markup);
		var subStrStartOffset = this.sample.substring(0, markup.from).lastIndexOf(this.getCurrentSysNewLine());
		if (subStrStartOffset != -1)
			markup.from = subStrStartOffset + (Ext.is.Windows ? 2 : 1);
		else
			markup.from = 0;
		markup.length = 0;
		for (var i = 0; i < this.markups.length; i++) {
			if (this.markups[i].action == 'endParse' || this.markups[i].action == 'stopParseAtStartOfLine') {
				Ext.Array.erase(this.markups, i, 1);
				break
			}
		}
		this.overrideSameTypeMarkup(markup);
		this.overrideSameTypeMarkup(endParse);
	},
	//===========
	addMarkup: function(markup) {
		if (Ext.isDefined(this['mark' + glu.string(markup.action).toPascalCase()])) {
			this['mark' + glu.string(markup.action).toPascalCase()](markup);
			return
		}
		if (Ext.isDefined(this['mark' + glu.string(markup.type).toPascalCase()])) {
			this['mark' + glu.string(markup.type).toPascalCase()](markup);
			return
		}
		this.overrideSameTypeMarkup(markup);
	},

	noSelectionError: function() {
		this.message({
			title: this.localize('noContentSelected'),
			msg: this.localize('noSelectionError'),
			buttons: Ext.MessageBox.OK
		});
	},
	createTip: function(parserView, initXY, actionIds) {
		this.open({
			mtype: 'RS.wiki.resolutionbuilder.ParserTip',
			parserView: parserView,
			initXY: initXY,
			actionIds: actionIds
		});
	},
	captureColumn: function(col) {
		this.addTblVarAction(col);
	},
	rowSeparator: '',
	columnSeparator: '',

	clearMarkup: function(rangeMarkup) {
		var newMarkups = [];
		if (rangeMarkup)
			Ext.each(this.markups, function(m) {
				if (m.from >= rangeMarkup.from && (m.from + m.length <= rangeMarkup.from + rangeMarkup.length))
					return;
				newMarkups.push(m);
			}, this)
		this.set('markups', newMarkups);
	},
	clearSample: function() {
		this.set('sample', '');
		this.clearMarkup();
	},
	tblVarStore: {
		mtype: 'store',
		fields: ['vid', 'columnNum', 'color', 'name', 'flow', 'wsdata', 'output','newlyAdded']
	},
	overrideSameTypeMarkup: function(markup) {
		var newMarkups = [];
		var markupTo = markup.from + markup.length - 1;
		Ext.each(this.markups, function(m) {
			if (m.action != markup.action) {
				newMarkups.push(m);
				return;
			}
			var to = m.from + m.length - 1;
			if (m.from < markup.from) {
				if (to < markup.from)
					newMarkups.push(m);
				else if (to <= markupTo) {
					m.length = markup.from - m.from;
					newMarkups.push(m);
				} else {
					var m0 = Ext.apply({}, m);
					m0.actionId = Ext.data.IdGenerator.get('uuid').generate();
					m0.from = markupTo + 1;
					m0.length = to - m0.from + 1;
					newMarkups.push(m0);
					m.length = markup.from - m.from;
					newMarkups.push(m);
				}
			} else {
				if (to <= markupTo)
					return;
				else if (m.from > markupTo)
					newMarkups.push(m);
				else {
					m.from = markupTo + 1;
					m.length = to - markupTo;
					newMarkups.push(m);
				}
			}
		}, this)
		newMarkups.push(markup);
		this.set('markups', newMarkups);
	},
	whenGetParserConfig: function() {
		return {}
	},
	getParserConfig: function() {
		var placeholders = [];
		this.placeholderStore.each(function(ph) {
			placeholders.push(ph.data);
		}, this);
		var config = {
			markups: this.markups,
			placeholders: placeholders,
			isInLoop: this.parentVM.parentVM.parentVM.entrySelections.length > 0 ? this.parentVM.parentVM.parentVM.entrySelections[0].isInLoop() : false
		};
		return Ext.applyIf(config, this.whenGetParserConfig());
	}
});