glu.defModel('RS.wiki.resolutionbuilder.XPathTokenEditor', {
	computeOffset: function() {},
	currentPath: '',
	currentSampleDom: null,
	attributeStore: {
		mtype: 'store',
		fields: ['name']
	},
	attrConditionStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	textConditionStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	predicatesSelections: [],
	augmentedXPathToken: null,
	loading: false,
	and: true,
	or: false,
	allValid: true,
	predicates: {
		mtype: 'list',
	},
	init: function() {
		this.set('loading', true);
		Ext.each(this.augmentedXPathToken.predicates, function(p) {
			if (p.type == 'attribute')
				this.doAddPredicate('RS.wiki.resolutionbuilder.XPathAttrPredicate', p);
			else if (p.type == 'text')
				this.doAddPredicate('RS.wiki.resolutionbuilder.XPathTextPredicate', p);
			else
				this.doAddPredicate('RS.wiki.resolutionbuilder.XPathCustomizedPredicate', p);
		}, this)
		this.set('loading', false);
		this.textConditionStore.add([{
			name: this.localize('startsWith'),
			value: 'starts-with'
		}, {
			name: this.localize('endsWith'),
			value: 'ends-with'
		}, {
			name: this.localize('contains'),
			value: 'contains'
		}]);

		this.attrConditionStore.add([{
			name: '=',
			value: '='
		}, {
			name: '!=',
			value: '!='
		}, {
			name: '>=',
			value: '>='
		}, {
			name: '<=',
			value: '<='
		}, {
			name: this.localize('startsWith'),
			value: 'starts-with'
		}, {
			name: this.localize('endsWith'),
			value: 'ends-with'
		}, {
			name: this.localize('contains'),
			value: 'contains'
		}])
		var nodes = Ext.fly(this.currentSampleDom).query(this.augmentedXPathToken.element)
		var attrs = [];
		this.attributeStore.removeAll();
		Ext.each(nodes, function(node) {
			for (var i = 0; i < node.attributes.length; i++) {
				var attr = node.attributes[i];
				if (this.attributeStore.findExact('name', attr.name) != -1)
					return;
				this.attributeStore.add({
					name: attr.name
				});
			}
		}, this);
	},
	addTextPredicate: function() {
		this.doAddPredicate('RS.wiki.resolutionbuilder.XPathTextPredicate', {
			pnid: this.augmentedXPathToken.pnid,
			ptid: Ext.data.IdGenerator.get('uuid').generate(),
			condition: 'contains',
			text: 'NewText'
		});
	},
	addAttrPredicate: function() {

		this.doAddPredicate('RS.wiki.resolutionbuilder.XPathAttrPredicate', {
			pnid: this.augmentedXPathToken.pnid,
			ptid: Ext.data.IdGenerator.get('uuid').generate(),
			name: 'NewAttribute',
			condition: '=',
			value: 'Value'
		});
	},
	addCustPredicate: function() {
		this.doAddPredicate('RS.wiki.resolutionbuilder.XPathCustomizedPredicate', {
			pnid: this.augmentedXPathToken.pnid,
			ptid: Ext.data.IdGenerator.get('uuid').generate(),
			predicate: 'NewPredicate'
		});
	},
	doAddPredicate: function(modelName, predicate) {
		this.predicates.add(this.model(modelName, predicate));
		// if (this.loading)
		// 	return;
		// this.doUpdate();
	},
	doRemovePredicate: function(predicateVM) {
		this.predicates.remove(predicateVM);
		// if (this.loading)
		// 	return;
		// this.doUpdate();
	},
	doUpdate: function() {
		var newPredicates = [];
		this.predicates.forEach(function(p) {
			newPredicates.push(p.getData());
		});
		// this.augmentedXPathToken.predicates = newPredicates;
		Ext.each(this.parentVM.augmentedXPath, function(token) {
			if (token.ptid == this.augmentedXPathToken.ptid)
				token.predicates = newPredicates;
		}, this);
		this.parentVM.set('augmentedXPath', Ext.decode(Ext.encode(this.parentVM.augmentedXPath)));
		this.parentVM.set('textXPath', this.parentVM.xpath);
		this.parentVM.xpathQuery();
	},
	update: function() {
		this.doUpdate();
		this.cancel();
	},
	updateIsEnabled$: function() {
		return this.allValid;
	},
	cancel: function() {
		return this.doClose();
	}
});

glu.defModel('RS.wiki.resolutionbuilder.XPathPredicate', {
	when_isValid_changed: {
		on: ['isValidChanged'],
		action: function() {
			this.parentVM.set('allValid', this.isValid);
		}
	},
	remove: function() {
		this.parentVM.doRemovePredicate(this);
	},
	getData: function() {
		return this.asObject();
	}
});

glu.defModel('RS.wiki.resolutionbuilder.XPathTextPredicate', {
	mixins: ['XPathPredicate'],
	type: 'text',
	condition: 'contains',
	fields: ['pnid', 'ptid', 'condition', 'value', 'type'],
	valueIsValid$: function() {
		return Ext.String.trim(this.value) ? true : this.localize('invalidValue');
	}
});

glu.defModel('RS.wiki.resolutionbuilder.XPathAttrPredicate', {
	mixins: ['XPathPredicate'],
	type: 'attribute',
	fields: ['pnid', 'ptid', 'name', 'condition', 'value', 'type'],
	nameIsValid$: function() {
		return Ext.String.trim(this.name) ? true : this.localize('invalidName');
	},
	valueIsValid$: function() {
		return Ext.String.trim(this.value) ? true : this.localize('invalidValue');
	}
});

glu.defModel('RS.wiki.resolutionbuilder.XPathCustomizedPredicate', {
	mixins: ['XPathPredicate'],
	fields: ['pnid', 'ptid', 'predicate'],
	predicateIsValid$: function() {
		return Ext.String.trim(this.predicate) ? true : this.localize('invalidPredicate');
	}
});