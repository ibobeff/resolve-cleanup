glu.defModel('RS.wiki.resolutionbuilder.EntryAccessor', {
	metaScreen: null,
	waitRootOp$: function() {
		if (this.metaScreen)
			return this.metaScreen.reqCount;
		if (this.rootVM && this.rootVM.reqCount)
			return this.rootVM.reqCount;
		return false;
	},
	toTitleCase: function(str) {
		return str.replace(/\w\S*/g, function(txt) {
			return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
		});
	},
	getEntry: function(id, typeToGet, callback, scope) {
		if (!id) {
			var m = 0;
			(scope || this).entries.forEach(function(item) {
				if (m < item.order)
					m = item.order;
			})
			m++;
			var initConfig = {
				order: m
			};
			if (typeToGet == RS.wiki.resolutionbuilder.viewmodels.TaskEntry.entityType) {
				var nextTaskIdx = this.getRoot().maxTaskIdx + 1;
				initConfig.name = 'New Task' + nextTaskIdx;
				this.getRoot().set('maxTaskIdx', nextTaskIdx);
			}
			if (typeToGet == RS.wiki.resolutionbuilder.viewmodels.RunbookEntry.entityType) {
				var nextRunbookIdx = this.getRoot().maxRunbookIdx + 1;
				initConfig.name = 'NewRunbook.New Runbook' + nextRunbookIdx;
				this.getRoot().set('maxRunbookIdx', nextRunbookIdx);
			}
			callback.call((scope || this), initConfig);
			return;
		}
		//just in case if we need to pull default from the server
		if (!this.metaScreen)
			this.set('metaScreen', this.getRoot().parentVM);
		this.metaScreen.set('reqCount', this.metaScreen.reqCount + 1);
		this.ajax({
			url: '/resolve/service/resolutionbuilder/get' + this.toTitleCase(typeToGet),
			params: {
				id: id
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError('getEntryTemplateError', respData.message);
					return;
				}
				callback.call((scope || this), (respData.data || {}));
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.metaScreen.set('reqCount', this.metaScreen.reqCount - 1);
			}
		})
	},
	saveEntryAction: function(callback, scope) {
		callback = callback || function() {};
		var entity = this.getData();
		entity.parentId = (this.parentVM && this.parentVM.entityType != 'builder' ? this.parentVM.id : this.metaScreen.id);
		entity.parentType = this.parentVM && this.parentVM.entityType ? this.parentVM.entityType : '';
		if (entity.entityType == RS.wiki.resolutionbuilder.viewmodels.TaskEntry.entityType || entity.entityType == RS.wiki.resolutionbuilder.viewmodels.RunbookEntry.entityType)
			entity.generalId = this.getRoot().parentVM.id;

		//Decode body 
		if(entity.body && entity.body != ""){
			entity.body =  encodeURIComponent(entity.body);
		}
		function doSave() {
			this.metaScreen.set('reqCount', this.metaScreen.reqCount + 1);
			this.ajax({
				url: '/resolve/service/resolutionbuilder/save' + this.toTitleCase(this.entityType),
				jsonData: entity,
				scope: this,
				success: function(resp) {
					var respData = RS.common.parsePayload(resp);
					if (!respData.success) {
						clientVM.displayError(this.localize('saveEntityError', respData.message));
					}
					callback.call(scope || this, respData || {});
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
					callback.call(scope || this, {
						success: false
					});
				},
				callback: function() {
					this.metaScreen.set('reqCount', this.metaScreen.reqCount - 1);
				}
			});
		}
		doSave.call(this);
		// var me = this;
		// this.beforeSave(function() {
		// 	doSave.call(me);
		// }, entity);
	},
	saveEntry: function(callback, scope) {
		var me = this;
		if (!this.metaScreen)
			this.set('metaScreen', this.getRoot().parentVM);
		if (this.metaScreen.id)
			me.saveEntryAction(callback, scope);
		else
			this.metaScreen.saveAutomationMetaGeneralInfo(false, function() {
				me.saveEntryAction(callback, scope);
			});
	},
	deleteEntry: function(callback, scope) {
		if (!this.metaScreen)
			this.set('metaScreen', this.getRoot().parentVM);
		callback = callback || function() {};
		this.metaScreen.set('reqCount', this.metaScreen.reqCount + 1);
		this.ajax({
			url: '/resolve/service/resolutionbuilder/delete' + this.toTitleCase(this.entityType),
			params: {
				id: this.id
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					//clientVM.displayError(this.localize('deleteEntityError', respData.message));
					clientVM.displayError(respData.message);
					return;
				}
				callback.call(scope || this, respData || {});
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
				callback.call(scope || this, {
					success: false
				});
			},
			callback: function() {
				this.metaScreen.set('reqCount', this.metaScreen.reqCount - 1);
			}
		})
	}
});