glu.defModel('RS.wiki.resolutionbuilder.ParserTip', {
	pinned: false,
	parserView: null,
	initXY: null,
	actionIds: [],
	markupsSelections: [],
	markupStore: {
		mtype: 'store',
		fields: ['actionId', 'action', 'content', 'variable']
	},
	init: function() {
		this.updateMarkups(this.actionIds);
	},
	togglePin: function() {
		this.set('pinned', !this.pinned);
	},
	pin: function() {
		this.set('pinned', true);
	},
	unpin: function() {
		this.set('pinned', false);
	},
	updateMarkups: function(actionIds) {
		this.markupStore.removeAll();
		this.set('actionIds', actionIds);
		Ext.each(this.parentVM.markups, function(markup) {
			for (var i = 0; i < this.actionIds.length; i++) {
				if (this.actionIds[i] == markup.actionId)
					this.markupStore.add(Ext.apply({
						content: this.parentVM.sample.substring(markup.from, markup.from + markup.length),
						variable: '--'
					}, markup))
			}
		}, this);
	},
	remove: function() {
		this.markupStore.remove(this.markupsSelections);
		var newMarkups = [];
		Ext.each(this.parentVM.markups, function(markup) {
			Ext.each(this.markupsSelections, function(selection) {
				if (selection.get('actionId') == markup.actionId)
					return;
				newMarkups.push(markup);
			}, this);
		}, this);
		this.parentVM.set('markups', newMarkups);
		if (newMarkups.length == 0)
			this.cancel();
	},
	removeIsEnabled$: function() {
		return this.markupsSelections.length > 0;
	},
	cancel: function() {
		return this.doClose();
	}
});