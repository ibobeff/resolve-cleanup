glu.defModel('RS.wiki.resolutionbuilder.TaskEditor', {
	nameIsValid: true,
	hasFocus$: function() {
		return this.parentVM.entrySelections.length == 1;
	},
	when_name_changed: {
		on: ['nameChanged'],
		action: function() {
			var valid = (/^[a-zA-Z0-9_\-]+$/.test(this.name) || /^[a-zA-Z0-9_\-][a-zA-Z0-9_\-\s]*[a-zA-Z0-9_\-]$/.test(this.name)) && !/.*\s{2,}.*/.test(this.name);
			valid = valid || this.parentVM.entrySelections.length == 0;
			this.parentVM.set('editorIsValid', valid);
			this.set('nameIsValid', valid);
			if (this.parentVM.entrySelections.length > 0)
				this.parentVM.entrySelections[0].set('name', this.name);
		}
	},
	fields: ['id', 'name', 'command', 'detail', 'promptSource', 'promptSourceName', 'description', 'summary', 'inputfile', {
		name: 'timeout',
		type: 'int'
	}, {
		name: 'expectTimeout',
		type: 'int'
	}],
	when_fields_changed: {
		on: ['nameChanged', 'commandChanged', 'promptSourceChanged', 'promptSourceNameChanged', 'descriptionChanged', 'summaryChanged', 'detailChanged'],
		action: function() {
			this.parentVM.set('taskEditorIsDirty', true);
		}
	},
	init: function() {
		this.parser.on('parserIsDirtyChanged', function() {
			this.parentVM.set('taskEditorIsDirty', this.taskEditorIsDirty || this.parser.parserIsDirty);
		}, this);
		this.assessor.on('assessorIsDirtyChanged', function() {
			this.parentVM.set('taskEditorIsDirty', this.taskEditorIsDirty || this.assessor.assessorIsDirty);
		}, this);
	},
	promptSource: 'CONSTANT',
	promptSourceIsConst$: function() {
		return this.promptSource == 'CONSTANT' || !this.promptSource;
	},
	promptSourceIsWSDATAOrFlow$: function() {
		return this.promptSource == 'WSDATA' || this.promptSource == 'FLOW';
	},
	promptSourceIsProperty$: function() {
		return this.promptSource == 'PROPERTY';
	},
	promptSourceIsParam$: function() {
		return this.promptSource == 'PARAM';
	},
	sourceStore$: function() {
		return this.parentVM.parentVM.sourceStore;
	},
	parser: {
		mtype: 'Parsers'
	},
	assessor: {
		mtype: 'Assessor'
	},
	resetEditorScreen: function() {
		this.loadData({
			name: '',
			command: '',
			summary: '',
			detail: ''
		});
		this.parser.resetParser();
		this.assessor.resetAssessor();
	},
	insertCommandArgument: function() {
		var me = this;
		this.open({
			mtype: 'ArgumentPicker',
			excludeSelf: true,
			dumper: function() {
				me.set('command', (me.command || '') + (me.command ? ' ' : '') + Ext.String.format('${0}\{{1}\}', this.variableSource, this.variable))
			}
		})
	},

	addSummaryArgs: function() {
		var me = this;
		this.open({
			mtype: 'ArgumentPicker',
			addRaw: true,
			dumper: function() {
				if (this.variableSource == 'RAW') {
					me.set('summary', '${RAW}');
					return;
				}
				if (this.variableSource == 'CONSTANT')
					me.set('summary', me.summary + (me.summary ? ' ' : '') + Ext.String.format('$\{\"{0}\"\}', this.variable))
				else
					me.set('summary', me.summary + (me.summary ? ' ' : '') + Ext.String.format('$\{{0}[\"{1}\"]\}', this.variableSource, this.variable))
			}
		})
	},
	addDetailArgs: function() {
		var me = this;
		this.open({
			mtype: 'ArgumentPicker',
			addRaw: true,
			dumper: function() {
				if (this.variableSource == 'RAW') {
					me.set('detail', '${RAW}');
					return;
				}
				me.set('detail', me.detail + (me.detail ? ' ' : '') + Ext.String.format('$\{{0}[\"{1}\"]\}', this.variableSource, this.variable))
			}
		})
	},
	collectEditorScreen: function() {
		var taskData = null;
		if (!this.isHTTPTask){
			taskData = Ext.apply(this.assessor.getData(), this.parser.getData(), this.asObject());
		}
		else{
			taskData = Ext.apply(this.assessor.getData(), this.parser.getData(), this.asObject());
			//For some reason apply methods fails with more than 3 parameters
			taskData = Ext.apply(taskData, this.httpCommand.getData());
		}
		return taskData;
	},
	isHTTPTask: false,
	httpCommand: {
		mtype: 'HTTPCommand'
	},
	populateEditorScreen: function(data, entry) {
		this.resetEditorScreen();
		this.loadData(data);
		this.assessor.setData(data);
		this.parser.setData(data);
		var current = entry;
		while (current.entityType && current.entityType != 'connection')
			current = current.parentVM
		this.set('isHTTPTask', current.type == 'http');
		if(current.type == 'http'){
			this.httpCommand.setData(data);
		}
		this.parentVM.set('taskEditorIsDirty', false);
	},
	expandInputFile: function() {
		this.open({
			mtype: 'RS.wiki.resolutionbuilder.INPUTFILEEditor',
			inputfile: this.inputfile
		})
	}
});

glu.defModel('RS.wiki.resolutionbuilder.INPUTFILEEditor', {
	inputfile: '',
	update: function() {
		this.parentVM.set('inputfile', this.inputfile);
		this.cancel();
	},
	cancel: function() {
		this.doClose();
	}
})