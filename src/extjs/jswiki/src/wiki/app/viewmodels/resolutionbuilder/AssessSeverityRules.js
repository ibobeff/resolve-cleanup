glu.defModel('RS.wiki.resolutionbuilder.AssessSeverityRules', {
	mixins: ['ConditionContainer'],
	goodDefaultSeverity: true,
	warningDefaultSeverity: false,
	severeDefaultSeverity: false,
	criticalDefaultSeverity: false,
	severitiesIsDirty: false,
	interactive: true,
	loadSeverities: function(severityJsonArray) {
		this.set('interactive', false);
		this.clearConditions();
		var groupMap = {};
		Ext.each(severityJsonArray, function(json) {
			if (!groupMap[json.severityId]) {
				var vm = this.addCondition('RS.wiki.resolutionbuilder.SeverityRuleGroup', {
					result: json.severity
				});
				groupMap[json.severityId] = vm;
			}
			groupMap[json.severityId].addCondition('RS.wiki.resolutionbuilder.SeverityRule', json, function(vm) {
				vm.fromServerData(json)
			});
		}, this)
		this.set('severitiesIsDirty', false);
		this.set('interactive', true);
	},
	getAssessSeverities: function() {
		var severities = [];
		this.conditions.forEach(function(severityGroup, idx) {
			var list = severityGroup.getGroupSeverities();
			Ext.each(list, function(l) {
				l.order = idx;
			});
			severities = severities.concat(list);
		});
		var s = '';
		if (this.goodDefaultSeverity)
			s = 'GOOD';
		else if (this.warningDefaultSeverity)
			s = 'WARNING';
		else if (this.severeDefaultSeverity)
			s = 'SEVERE';
		else
			s = 'CRITICAL';
		return {
			defaultSeverity: s,
			severities: severities
		};
	},
	add: function() {
		this.addCondition('RS.wiki.resolutionbuilder.SeverityRuleGroup', {
			result: this.goodDefaultSeverity ? 'SEVERE' : 'GOOD'
		});
	},
	remove: function() {
		this.removeConditions();
	},
	removeIsEnabled$: function() {
		return this.removeConditionsIsEnabled;
	},
	resetAssessSeverityRules: function() {
		this.clearConditions();
	}
});

glu.defModel('RS.wiki.resolutionbuilder.SeverityRuleGroup', {
	mixins: ['ConditionContainer', 'BasicCondition'],
	result: 'BAD',
	resultStore: null,
	severitiesIsDirty: false,
	qtip: '',
	severitiesIsDirtyChanged$: function() {
		this.parentVM.set('severitiesIsDirty', this.parentVM.severitiesIsDirty && this.severitiesIsDirty);
	},
	init: function() {
		this.set('qtip', this.localize('qtip'));
		this.set('resultStore', Ext.create('Ext.data.Store', {
			fields: ['value'],
			data: [{
				value: 'GOOD'
			}, {
				value: 'WARNING'
			}, {
				value: 'SEVERE'
			}, {
				vale: 'CRITICAL'
			}]
		}));
		this.conditions.on('lengthchanged', function() {
			this.parentVM.set('severitiesIsDirty', this.parentVM.severitiesIsDirty && this.severitiesIsDirty);
			if (this.conditions.length == 0)
				this.parentVM.removeCondition(this);
		}, this)
		if (this.parentVM.interactive)
			this.add();
	},
	add: function() {
		this.addCondition('RS.wiki.resolutionbuilder.SeverityRule');
	},
	toggleResult: function() {
		var s = ['GOOD', 'SEVERE', 'WARNING', 'CRITICAL'];
		Ext.each(s, function(item, idx) {
			if (this.result == item) {
				this.set('result', s[(idx + 1) % 4])
				return false;
			}
		}, this);
	},
	getGroupSeverities: function() {
		var severities = [];
		var severityId = null;
		this.conditions.forEach(function(severity) {
			if (!severityId)
				severityId = severity.id;
			severities.push(Ext.apply(severity.toServerData(), {
				severityId: severityId,
				id: '',
				severity: this.result
			}));
		}, this);
		return severities;
	}
});

glu.defModel('RS.wiki.resolutionbuilder.SeverityRule', {
	mixins: ['BasicCondition', 'VariableAware'],
	fields: ['severity', 'criteria', 'variableSource', 'variable', 'comparison', 'valueSource', 'value', {
		name: 'order',
		type: 'float'
	}],
	when_fields_changed: {
		on: ['criteriaChanged', 'variableSourceChanged', 'variableChanged', 'comparisonChanged', 'valueSourceChanged', 'valueChanged'],
		action: function() {
			this.parentVM.set('severitiesIsDirty', true);
		}
	},
	severity: 'critical',
	id: '',
	severityStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [{
			name: 'GOOD',
			value: 'good'
		}, {
			name: 'WARNING',
			value: 'warning'
		}, {
			name: 'SEVERE',
			value: 'severe'
		}, {
			name: 'CRITICAL',
			value: 'critical'
		}]
	},
	criteria: 'any',
	criteriaStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [{
			name: 'ANY',
			value: 'any'
		}, {
			name: 'ALL',
			value: 'all'
		}]
	},
	variables: ['variable', 'value'],
	defaults: {
		variableSource: 'FLOW',
		criteria: 'any',
		comparison: 'eq',
		valueSource: 'CONSTANT'
	},
	init: function() {
		this.loadData(this.defaults);
		this.updateVariables();
		this.updateSourceNames();
		this.set('id', Ext.data.IdGenerator.get('uuid').generate());
	},
	comparison: 'eq',
	valueChanged$: function() {
		this.propertyStore.keyword = this.value;
	},
	updateSourceNames: function() {
		this.updateGlobalVariableStore(this.valueSource);
	},
	updateVariables: function() {
		this.updateGlobalVariableStore(this.variableSource);
	},
	addSibling: function() {
		this.parentVM.add();
	},
	remove: function() {
		this.parentVM.removeCondition(this);
	},
	toServerData: function() {
		var obj = this.asObject();
		obj.source = this.valueSource;
		obj.sourceName = this.value;
		return obj;
	},
	fromServerData: function(data) {
		data.valueSource = data.source
		data.value = data.sourceName
		this.loadData(data);
	}
});