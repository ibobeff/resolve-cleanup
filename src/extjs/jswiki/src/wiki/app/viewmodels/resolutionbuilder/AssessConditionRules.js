glu.defModel('RS.wiki.resolutionbuilder.AssessConditionRules', {
	mixins: ['ConditionContainer'],
	goodDefaultCondition: true,
	badDefaultCondition: false,
	alwaysContinue: true,
	stopOnBad: false,
	continueOnGood: false,
	continueOnBad: false,
	conditionsIsDirty: false,
	interactive: true,
	loadConditions: function(conditionJsonArray) {
		this.set('interactive', false);
		this.clearConditions();
		var groupMap = {};
		Ext.each(conditionJsonArray, function(json) {
			if (!groupMap[json.conditionId]) {
				var vm = this.addCondition('RS.wiki.resolutionbuilder.ConditionRuleGroup', {
					result: json.result
				});
				groupMap[json.conditionId] = vm;
			}
			groupMap[json.conditionId].addCondition('RS.wiki.resolutionbuilder.ConditionRule', json, function(vm) {
				vm.fromServerData(json);
			});
		}, this)
		this.set('conditionsIsDirty', false);
		this.set('interactive', true);
	},
	getAssessConditions: function() {
		var conditions = [];
		this.conditions.forEach(function(conditionGroup, idx) {
			var list = conditionGroup.getGroupConditions();
			Ext.each(list, function(l) {
				l.order = idx;
			});
			conditions = conditions.concat(list);
		});
		return {
			conditions: conditions,
			defaultAssess: this.goodDefaultCondition ? 'GOOD' : 'BAD',
			continuation: this.alwaysContinue ? 'ALWAYS' : (this.stopOnBad ? 'STOPONBAD' : 'STOPONGOOD')
		};
	},
	add: function() {
		this.addCondition('RS.wiki.resolutionbuilder.ConditionRuleGroup', {
			result: this.goodDefaultCondition ? 'BAD' : 'GOOD'
		});
	},
	resetAssessConditionRules: function() {
		this.clearConditions();
	},
	remove: function() {
		this.removeConditions();
	},
	removeIsEnabled$: function() {
		return this.removeConditionsIsEnabled;
	}
});

glu.defModel('RS.wiki.resolutionbuilder.ConditionRuleGroup', {
	mixins: ['ConditionContainer', 'BasicCondition'],
	result: 'BAD',
	resultStore: null,
	conditionsIsDirty: false,
	qtip: '',
	conditionsIsDirtyChanged$: function() {
		this.parentVM.set('conditionsIsDirty', this.parentVM.conditionsIsDirty && this.conditionsIsDirty);
	},
	init: function() {
		this.set('qtip', this.localize('qtip'));
		this.set('resultStore', Ext.create('Ext.data.Store', {
			fields: ['value'],
			data: [{
				value: 'GOOD'
			}, {
				value: 'BAD'
			}]
		}));
		if (this.parentVM.interactive)
			this.add();
		this.conditions.on('lengthchanged', function() {
			this.parentVM.set('conditionsIsDirty', this.parentVM.conditionsIsDirty && this.conditionsIsDirty);
			if (this.conditions.length == 0)
				this.parentVM.removeCondition(this);
		}, this)
	},
	add: function() {
		this.addCondition('RS.wiki.resolutionbuilder.ConditionRule');
	},
	toggleResult: function() {
		this.set('result', this.result == 'GOOD' ? 'BAD' : 'GOOD');
	},
	getGroupConditions: function() {
		var conditions = [];
		var conditionId = null;
		this.conditions.forEach(function(con) {
			if (!conditionId)
				conditionId = con.id;
			conditions.push(Ext.apply(con.toServerData(), {
				conditionId: conditionId,
				id: '',
				result: this.result
			}));
		}, this);
		return conditions;
	}
});

glu.defModel('RS.wiki.resolutionbuilder.ConditionRule', {
	mixins: ['BasicCondition', 'VariableAware'],
	fields: ['result', 'criteria', 'variableSource', 'variable', 'comparison', 'valueSource', 'value', {
		name: 'order',
		type: 'float'
	}],
	when_fields_changed: {
		on: ['criteriaChanged', 'variableSourceChanged', 'variableChanged', 'comparisonChanged', 'valueChanged', 'valueSourceChanged'],
		action: function() {
			this.parentVM.set('conditionsIsDirty', true);
		}
	},
	result: 'bad',
	resultStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [{
			name: 'GOOD',
			value: 'good'
		}, {
			name: 'BAD',
			value: 'bad'
		}]
	},
	variables: ['variable', 'value'],
	variableChanged$: function() {
		this.propertyStore.keyword = this.variable;
	},
	criteria: 'any',
	criteriaStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [{
			name: 'ANY',
			value: 'any'
		}, {
			name: 'ALL',
			value: 'all'
		}]
	},
	defaults: {
		variableSource: 'FLOW',
		criteria: 'any',
		comparison: 'eq',
		valueSource: 'CONSTANT'
	},
	init: function() {
		this.loadData(this.defaults);
		this.updateVariables();
		this.updateSourceNames();
		this.set('id', Ext.data.IdGenerator.get('uuid').generate());
	},
	variableChanged$: function() {
		this.propertyStore.keyword = this.variable;
	},
	valueChanged$: function() {
		this.propertyStore.keyword = this.value;
	},
	updateSourceNames: function() {
		this.updateGlobalVariableStore(this.valueSource);
	},
	updateVariables: function() {
		this.updateGlobalVariableStore(this.variableSource);
	},
	addSibling: function() {
		this.parentVM.add();
	},
	remove: function() {
		this.parentVM.removeCondition(this);
	},
	toServerData: function() {
		var obj = this.asObject();
		obj.source = this.valueSource;
		obj.sourceName = this.value;
		return obj;
	},
	fromServerData: function(data) {
		data.valueSource = data.source
		data.value = data.sourceName
		this.loadData(data);
	}
});