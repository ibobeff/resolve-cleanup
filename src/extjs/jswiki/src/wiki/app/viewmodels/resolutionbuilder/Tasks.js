glu.defModel('RS.wiki.resolutionbuilder.Tasks', {
	entityType: 'builder',
	maxTaskIdx: 0,
	maxRunbookIdx: 0,
	getData: function() {
		return {
			entityType: 'builder'
		};
	},
	mixins: ['EntryAccessor', 'EntryContainer'],
	taskEditorIsDirty: false,
	editorIsValid: true,
	globalVariableStore: {
		mtype: 'store',
		fields: ['name', 'task', 'color']
	},
	taskEditor: {
		mtype: 'TaskEditor'
	},
	runbookEditor: {
		mtype: 'RunbookEditor'
	},

	init: function() {

		this.propertyStore.on('beforeload', function(store, op) {
			op.params = op.params || {};
			op.params.filter = Ext.encode([{
				"field": "uname",
				"type": "auto",
				"condition": "contains",
				"value": this.keyword
			}]);
		})
	},

	propertyStore: {
		mtype: 'store',
		fields: ['uname'],
		sorters: ['uname'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/atproperties/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		},
		pageSize: 20
	},
	noOfColumn: 1,
	newWorksheet: false,
	paramStore: {
		mtype: 'store',
		sorters: ['name'],
		fields: ['name', 'sourceName']
	},

	interactive: true,
	resetTasks: function() {
		this.entries.removeAll();
		Ext.each(this.entrySelections, function(entrySelection) {
			entrySelection.obselete = true;
		});
		this.set('entrySelections', []);
		this.paramStore.removeAll();
		this.taskEditor.resetEditorScreen();
		this.runbookEditor.resetEditorScreen();
	},

	addConnection: function() {
		this.insertAndSave('connection', 'RS.wiki.resolutionbuilder.ConnectionEntry');
	},
	addConnectionIsEnabled$: function() {
		return !this.waitRootOp;
	},
	addLoop: function() {
		this.insertAndSave('loop', 'RS.wiki.resolutionbuilder.LoopEntry');
	},
	addLoopIsEnabled$: function() {
		return !this.waitRootOp;
	},
	addParam: function() {
		var me = this;
		var params = [];
		this.paramStore.each(function(param) {
			params.push(param.raw);
		});
		this.open({
			mtype: 'RS.wiki.resolutionbuilder.ParamWindow',
			params: {
				noOfColumn: this.noOfColumn,
				newWorksheet: this.newWorksheet,
				params: params
			},
			dumper: function() {
				var params = this.parameter.collect();
				me.set('noOfColumn', params.noOfColumn);
				me.set('newWorksheet', params.newWorksheet);
				me.paramStore.removeAll();
				me.paramStore.add(params.params);
			}
		})
	},
	isTabValid: true,
	populateScreen: function(tasks) {
		this.set('interactive', false);
		this.entries.removeAll();
		this.set('newWorksheet', !!tasks && tasks.data.newWorksheet)
		this.deserializeCt(tasks, true);
		this.set('interactive', true);
	},
	collect: function() {
		return {
			tasks: this.serializeCt()
		}
	},
	collectEditorScreen: function() {
		if (this.taskEditorIsVisible)
			return this.taskEditor.collectEditorScreen();
		return this.runbookEditor.collectEditorScreen();
	},
	updateGlobalVariableStore: function(type, excludeSelf, actionNode) {
		this.globalVariableStore.removeAll();
		var variables = this.collectVariables(type, excludeSelf, actionNode);
		this.globalVariableStore.add(variables);
	},
	collectVariables: function(type, excludeSelf, actionNode) {
		var variables = [];
		var me = this;
		if (!actionNode)
			actionNode = this.entrySelections[0];

		function collect(node) {
			if (node == actionNode)
				return false;
			if (node.entries)
				for (var i = 0; i < node.entries.length; i++) {
					if (!collect(node.entries.getAt(i)))
						return;
				}

			if (node.entityType == 'task') {
				Ext.each(node.variableExtractedFromParserConfig, function(variable) {
					if (variable[type.toLowerCase()])
						variables.push(variable);
				});
			}
			return true;
		}

		collect(this);
		if (actionNode.isInLoop())
			variables.push({
				name: 'LOOP_ITEM'
			});
		if (actionNode.entityType != 'task')
			return variables;
		if (!excludeSelf && this.taskEditorIsVisible)
			this.taskEditor.parser.getVariableStore().each(function(variable) {
				if (variable.get(type && type.toLowerCase()))
					variables.push({
						name: variable.get('name'),
						task: this.name,
						color: variable.get('color')
					});
			}, this)
		return variables;
	},
	populateEditorScreen: function(data, entry) {
		if (data.entityType == 'task')
			this.taskEditor.populateEditorScreen(data, entry);
		else
			this.runbookEditor.populateEditorScreen(data, entry);
	},
	checkCascadeDelete: function(entry) {
		var current = entry;
		while (current) {
			if (current.deleted)
				return true;
			current = current.parentVM;
		}
		return false;
	},
	taskEditorIsVisible: true,
	when_root_entrySelections_changed: {
		on: ['entrySelectionsChanged'],
		action: function(newSelections, oldSelections) {
			var me = this;
			if (oldSelections.length > 0) {
				var oldEntry = oldSelections[0];
				if (!oldEntry.redoCozErrorDetected && !this.checkCascadeDelete(oldEntry) && !oldEntry.obselete) {
					oldEntry.setData(Ext.applyIf({
						order: oldEntry.order
					}, this.collectEditorScreen()));
					oldEntry.saveEntry(function(respData) {
						if (!respData.success) {
							if (newSelections.length > 0)
								newSelections[0].redoCozErrorDetected = true;
							me.set('entrySelections', [oldEntry])
							return;
						}
						if (newSelections.length > 0) {
							var newEntry = newSelections[0];
							this.getEntry(newEntry.id, newEntry.entityType, function(data) {
								me.populateEditorScreen(data, newEntry);
							});
						}
					});
					if (this.entrySelections.length > 0)
						this.set('taskEditorIsVisible', newSelections[0].entityType == 'task')
					return;
				}
				//this time the new selection comes from the rollback code(the last valid selection)...not manual action from user
				if (oldEntry.redoCozErrorDetected) {
					this.populateEditorScreen(newSelections[0].getData(), newSelections[0]);
					delete oldEntry.redoCozErrorDetected;
					this.set('taskEditorIsVisible', newSelections[0].entityType == 'task')
					return;
				}
			}
			if (newSelections.length > 0) {
				var newEntry = newSelections[0];
				this.getEntry(newEntry.id, newEntry.entityType, function(data) {
					newEntry.setData(data);
					this.populateEditorScreen(newEntry.getData(), newEntry);
				});
				this.set('taskEditorIsVisible', newSelections[0].entityType == 'task')
			}
		}
	}
});