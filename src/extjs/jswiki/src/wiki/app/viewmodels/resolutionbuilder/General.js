glu.defModel('RS.wiki.resolutionbuilder.General', {
	id: '',
	isReadOnly$: function() {
		return !!this.id;
	},
	name: '',
	namespace: '',
	summary: '',
	generated: false,
	fields: ['id', 'name', 'namespace', 'summary', {
		name: 'generated',
		type: 'bool',
	}],
	defaultData: {
		name: '',
		namespace: '',
		summary: '',
		generated: false
	},
	generatedChanged$: function() {
		this.parentVM.set('generated', this.generated);
	},
	resetGeneral: function() {
		this.loadData(this.defaultData);
	},
	validateName: function(name) {
		return (/^[a-zA-Z0-9_\-]+$/.test(name) || /^[a-zA-Z0-9_\-][a-zA-Z0-9_\-\s]*[a-zA-Z0-9_\-]$/.test(name)) && !/.*\s{2,}.*/.test(name);
	},
	validateNamespace: function(namespace) {
		return (/^[a-zA-Z0-9_\-]+$/.test(namespace) || /^[a-zA-Z0-9_\-][a-zA-Z0-9_\-\s\.]*[a-zA-Z0-9_\-]$/.test(namespace)) && !/.*\s{2,}.*/.test(namespace) && !/.*(\.\.|\s+\.|\.\s+).*/.test(namespace);
	},
	nameIsValid$: function() {
		return this.validateName(this.name) ? true : this.localize('invalidName');
	},
	namespaceIsValid$: function() {
		return this.validateNamespace(this.namespace) ? true : this.localize('invalidNamespace');
	},
	populateScreen: function(meta) {
		this.resetGeneral();
		this.loadData(meta);
	},
	collect: function() {
		return this.asObject();
	},
	validate$: function() {
		this.parentVM.set('generalTabIsValid', this.isValid);
	}
})