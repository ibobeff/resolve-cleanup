glu.defModel('RS.wiki.resolutionbuilder.PlainTextTableMarkupParser', {
	mixins: ['BaseParser', 'PlainTextParser'],
	init: function() {
		this.tblVarStore.on('datachanged', function() {
			this.set('parserIsDirty', true);
		}, this);
		this.variableStore.on('update', function(store, variable) {
			this.tblVarStore.each(function(columnVar) {
				if (variable.get('vid') == columnVar.get('vid')) {
					columnVar.set('name', variable.get('name'));
					columnVar.set('flow', variable.get('flow'));
					columnVar.set('wsdata', variable.get('wsdata'));
				}
			});
		}, this);
	},
	markTable: function(markup) {
		var actualFrom = markup.from;
		var actualEnd = markup.from + markup.length;
		Ext.each(this.markups, function(m) {
			if (m.action == 'beginParse' && m.from + m.length > actualFrom)
				actualFrom = m.from + m.length;
			if (m.action == 'parseFromEndOfLine' && m.from + m.length > actualFrom)
				actualFrom = m.from + m.length;
			if (m.action == 'endParse' && m.from < actualEnd)
				actualEnd = m.from;
			if (m.action == 'stopParseAtStartOfLine' && m.from < actualEnd)
				actualEnd = m.from;
		}, this);
		this.applySeparator(actualFrom, actualEnd - actualFrom);
	},
	columnSeparator: '',
	columnSeparatorModified: function(newValue) {
		this.set('columnSeparator', newValue);
	},
	rowSeparator: '',
	rowSeparatorModified: function(newValue) {
		this.set('rowSeparator', newValue);
	},
	offsetCaptureIsEnabled$: function() {
		return !!this.rowSeparator;
	},
	capturedColumnUpdated: function(record) {
		this.availableTblColumns.each(function(item) {
			if (item.get('columnNum') == record.get('columnNum'))
				record.set('color', item.get('color'));
		}, this)
	},
	addTblVar: function() {
		this.addTblVarAction();
	},
	addTblVarAction: function(col) {
		var firstAvailMarkup = null;
		var minIndex = Number.MAX_VALUE;
		var maxColumn = 0;
		var specificMarkup = null;
		Ext.each(this.markups, function(markup) {
			if (markup.action == 'column' && minIndex > markup.column && markup.column > 0 && this.tblVarStore.findExact('columnNum', markup.column) == -1) {
				minIndex = markup.column;
				firstAvailMarkup = markup;
				if (maxColumn < markup.column)
					maxColumn = markup.column;
			}
			if (markup.action == 'column' && markup.column == col && this.tblVarStore.findExact('columnNum', markup.column) == -1)
				specificMarkup = markup;
		}, this)
		if (col) {
			if (specificMarkup)
				this.tblVarStore.add({
					vid: Ext.data.IdGenerator.get('uuid').generate(),
					columnNum: specificMarkup.column,
					name: 'column' + specificMarkup.column,
					color: specificMarkup.inlineStyle.backgroundColor,
					flow: true,
					wsdata: false,
					newlyAdded: true
				});
			return;
		}
		if (firstAvailMarkup)
			this.tblVarStore.add({
				vid: Ext.data.IdGenerator.get('uuid').generate(),
				columnNum: firstAvailMarkup.column,
				name: 'column' + firstAvailMarkup.column,
				color: firstAvailMarkup.inlineStyle.backgroundColor,
				flow: true,
				wsdata: false,
				newlyAdded: true
			});
	},
	tblVarsSelections: [],
	removeTblVars: function() {
		this.tblVarStore.remove(this.tblVarsSelections);
	},
	applyTblVar: function() {
		this.variableStore.removeAll();
		if (this.markups.length == 0)
			this.tblVarStore.removeAll();
		this.tblVarStore.each(function(item) {
			item.set('newlyAdded', false);
			this.variableStore.add(item.data);
		}, this);
		this.placeholderStore.each(function(item) {
			this.variableStore.add(item.data);
		}, this);
	},
	availableTblColumns: {
		mtype: 'store',
		fields: ['columnNum', 'color']
	},
	cancelAdd: function() {
		var newlyAdded = [];
		this.tblVarStore.each(function(v) {
			if (v.get('newlyAdded'))
				newlyAdded.push(v);
		});
		this.tblVarStore.remove(newlyAdded);
	},
	trim: false,
	tabWidth: 8,
	type: 'table',
	// whenSampleUpdated: function() {
	// 	this.parentVM.manualMarkupParser.set('sample', this.sample);
	// 	this.parentVM.manualMarkupParser.clearParserConfig();
	// },
	when_markups_changed: {
		on: ['markupsChanged'],
		action: function() {
			this.applyTblVar();
			this.updateAvailableTblVarsBasedOnCurrentMarkups();
		}
	},
	applySeparator: function(from, length) {
		var extractedTable = this.extractTable(from, length);
		var rsep = null;
		if (Ext.isArray(this.rowSeparator))
			rsep = this.rowSeparator;
		else
			rsep = this.extractSeparatorAndBuildRegexStr(this.rowSeparator, this.replaceSpecChar);
		rowSpace = 0;
		Ext.each(rsep, function(sep) {
			rowSpace += sep.length;
		})
		var offset = from;
		var markups = [];
		var colors = [];
		for (var i = 0; i < extractedTable.length; i++) {
			var row = extractedTable[i];
			var col = 1;
			if (!row[0])
				col--;
			for (var j = 0; j < row.length; j++) {
				if (!colors[col])
					colors[col] = this.randomColor.color();
				markups.push({
					from: offset,
					length: row[j].length,
					action: (j % 2 == 0 ? 'column' : 'delimiter'),
					ignore: (j % 2 == 0 ? false : true),
					actionId: Ext.data.IdGenerator.get('uuid').generate(),
					column: col,
					expand: true,
					inlineStyle: (j % 2 == 0 ? {
						backgroundColor: colors[col]
					} : null)
				});
				col += (j % 2 == 0 ? 1 : 0);
				offset += row[j].length;
			}
			offset += rowSpace;
		}

		//remove all table related
		var filteredMarkups = [];
		Ext.each(this.markups, function(markup) {
			if (markup.column)
				return;
			filteredMarkups.push(markup);
		}, this)

		this.set('markups', filteredMarkups.concat(markups));
	},

	extractTable: function(from, length) {
		var table = this.sample.substring(from, from + length);
		var rows = table.split(this.extractSeparatorAndBuildRegex(this.rowSeparator));
		var splittedRows = [];
		var matchedColSep = [];
		var columnRegex = this.extractSeparatorAndBuildRegex(this.columnSeparator);
		Ext.each(rows, function(row) {
			splittedRows.push(row.split(columnRegex));
			matchedColSep.push(row.match(columnRegex));
		}, this)
		var spaceHead = false;
		var spaceTail = false;
		var processed = [];
		for (var i = 0; i < splittedRows.length; i++) {
			var r = [];
			for (var j = 0; j < splittedRows[i].length - 1; j++) {
				r.push(splittedRows[i][j]);
				r.push(matchedColSep[i][j]);
			}
			r.push(splittedRows[i][splittedRows[i].length - 1]);
			processed.push(r);
			if (r[0] == '')
				spaceHead = true;
			if (r[r.length - 1] == '')
				spaceTail = true;
		}
		Ext.each(processed, function(row) {
			if (row[0] != '' && spaceHead)
				Ext.Array.insert(row, 0, ['', '']);
		});
		return processed;
	},
	extractSeparatorAndBuildRegex: function(value) {
		var regexStrs = this.extractSeparatorAndBuildRegexStr(value, this.replaceSpecCharAndEscp);
		var reg = new RegExp(regexStrs.join('|'), 'g');
		return reg;
	},
	extractSeparatorAndBuildRegexStr: function(value, replaceSpecChar) {
		var sps = value.split(',');
		var regexStrs = [];
		Ext.each(sps, function(sp) {
			var regexStr = Ext.escapeRe(sp);
			regexStr = replaceSpecChar.call(this, regexStr);
			regexStrs.push(regexStr);
		}, this);
		return regexStrs;
	},
	replaceSpecCharAndEscp: function(sp) {
		return sp.replace(new RegExp(this.thatSpecChar + 'COMMA' + this.thatSpecChar, 'g'), '[,]+')
			.replace(this.thatSpecChar + 'SPACE' + this.thatSpecChar, '[ ]+')
			.replace(this.thatSpecChar + 'TAB' + this.thatSpecChar, '\\t+')
			.replace(this.thatSpecChar + 'LINEFEED' + this.thatSpecChar, this.getCurrentSysNewLine(true))
			.replace(this.thatSpecChar + 'Unix(LF)' + this.thatSpecChar, this.getCurrentSysNewLine(true))
			.replace(this.thatSpecChar + Ext.escapeRe('Unix(LF)') + this.thatSpecChar, this.getCurrentSysNewLine(true))
			.replace(this.thatSpecChar + 'Win(CR\\LF)' + this.thatSpecChar, this.getCurrentSysNewLine(true))
			.replace(this.thatSpecChar + Ext.escapeRe('Win(CR\\LF)') + this.thatSpecChar, this.getCurrentSysNewLine(true))
			.replace(this.thatSpecChar + 'CR' + this.thatSpecChar, this.getCurrentSysNewLine(true));
	},
	replaceSpecChar: function(sp) {
		return sp.replace(new RegExp(this.thatSpecChar + 'COMMA' + this.thatSpecChar, 'g'), '[,]+')
			.replace(this.thatSpecChar + 'SPACE' + this.thatSpecChar, '[ ]+')
			.replace(this.thatSpecChar + 'TAB' + this.thatSpecChar, '\t+')
			.replace(this.thatSpecChar + 'LINEFEED' + this.thatSpecChar, this.getCurrentSysNewLine(false))
			.replace(this.thatSpecChar + 'Unix(LF)' + this.thatSpecChar, this.getCurrentSysNewLine(false))
			.replace(this.thatSpecChar + Ext.escapeRe('Unix(LF)') + this.thatSpecChar, this.getCurrentSysNewLine(false))
			.replace(this.thatSpecChar + 'Win(CR\\LF)' + this.thatSpecChar, this.getCurrentSysNewLine(false))
			.replace(this.thatSpecChar + Ext.escapeRe('Win(CR\\LF)') + this.thatSpecChar, this.getCurrentSysNewLine(false))
			.replace(this.thatSpecChar + 'CR' + this.thatSpecChar, this.getCurrentSysNewLine(false));
	},
	updateAvailableTblVarsBasedOnCurrentMarkups: function() {
		var uniqueMarkup = {};
		Ext.each(this.markups, function(markup) {
			if (markup.action == 'column') {
				uniqueMarkup[markup.column] = markup;
			}
		}, this)
		this.availableTblColumns.removeAll();
		Ext.Object.each(uniqueMarkup, function(k, markup) {
			if (markup.column < 1)
				return;
			this.availableTblColumns.add({
				columnNum: markup.column,
				color: markup.inlineStyle && markup.inlineStyle.backgroundColor || ''
			});
		}, this);
	},
	removeTableColumnCapturedVariables: function(removedVariables) {
		var removedColumnVariables = [];
		Ext.each(removedVariables, function(variable) {
			this.tblVarStore.each(function(columnVariable) {
				if (columnVariable.get('vid') == variable.get('vid'))
					removedColumnVariables.push(columnVariable);
			}, this);
		}, this);
		this.tblVarStore.remove(removedColumnVariables);
	},
	whenVariableRemove: function() {
		this.removeTableColumnCapturedVariables(this.variablesSelections);
	},
	clearParserConfig: function() {
		this.set('markups', []);
		this.set('rowSeparator', '');
		this.set('columnSeparator', '');
		this.variableStore.removeAll();
		this.placeholderStore.removeAll();
		this.tblVarStore.removeAll();
	},
	setParserConfig: function(config, type) {
		this.clearParserConfig();
		if (!config)
			return;
		this.set('markups', config.markups || []);
		this.set('trim', config.option && config.option.trim ? true : false);
		this.set('columnSeparator', config.columnSeparator);
		this.set('rowSeparator', config.rowSeparator);
		this.tblVarStore.add(config.tblVariables || []);
		this.variableStore.add(config.tblVariables || []);
		this.placeholderStore.add(config.placeholders || []);
		Ext.each(config.placeholders, function(ph) {
			this.variableStore.add(Ext.applyIf({
				placeholder: true
			}, ph));
		}, this)
	},
	whenGetParserConfig: function(type) {
		var tblVariables = [];
		this.variableStore.each(function(variable) {
			if (variable.get('placeholder'))
				return;
			tblVariables.push(Ext.applyIf(variable.data, variable.raw));
		}, this)
		var me = this;
		var config = {
			columnSeparator: this.columnSeparator,
			rowSeparator: this.rowSeparator,
			tblVariables: tblVariables,
			options: {
				trim: this.trim
			}
		};
		return config;
	}
});