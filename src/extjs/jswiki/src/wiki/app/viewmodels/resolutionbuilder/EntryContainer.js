glu.defModel('RS.wiki.resolutionbuilder.EntryContainer', {
	entrySelections: [],
	entries: {
		mtype: 'list'
	},
	getData: function() {
		//override this if u have extra data
		return this.asObject ? this.asObject() : {};
	},
	setData: function(data) {
		//override this if u have extra data
		if (this.loadData) this.loadData(data);
	},
	removeEntry: function(entry) {
		this.entries.remove(entry);
	},
	select: function(entry, multiselect) {
		this.set('entrySelections', [entry]);
	},
	when_entrySelections_changed: {
		on: ['entrySelectionsChanged'],
		action: function(newSelection, oldSelection) {
			Ext.each(oldSelection, function(selection) {
				selection.set('selected', false);
			});
			Ext.each(newSelection, function(selection) {
				selection.set('selected', true);
			})
		}
	},
	getRoot: function() {
		var current = this;
		while (current.entityType != 'builder') current = current.parentVM;
		return current;
	},
	serializeCt: function() {
		var children = [];
		this.entries.forEach(function(entry) {
			children.push(entry.serializeCt ? entry.serializeCt() : entry.serialize());
		}, this)
		return {
			data: this.getData(),
			children: children
		}
	},
	deserializeCt: function(node, ignoreData) {
		if (!ignoreData)
			this.setData(node.data || node);
		Ext.each(node.children.sort(function(a, b) {
			var aData = a.data || a;
			var bData = b.data || b;
			if (aData.order == null || bData.order == null)
				throw Ext.String.format('Unknown order!{0},{1}', a.data.entityType, b.data.entityType);
			return aData.order > bData.order ? 1 : -1;
		}), function(child) {
			var model = null;
			switch (child.entityType || child.data.entityType) {
				case 'task':
					model = this.model('RS.wiki.resolutionbuilder.TaskEntry');
					break;
				case 'subrunbook':
					model = this.model('RS.wiki.resolutionbuilder.RunbookEntry');
					break;
				case 'condition':
					model = this.model('RS.wiki.resolutionbuilder.ConditionEntry');
					break;
				case 'if':
					model = this.model('RS.wiki.resolutionbuilder.IfEntry');
					break;
				case 'connection':
					model = this.model('RS.wiki.resolutionbuilder.ConnectionEntry');
					break;
				case 'loop':
					model = this.model('RS.wiki.resolutionbuilder.LoopEntry');
					break;
				case 'builder':
					break;
				default:
					throw 'Unknow entry!!';
			}
			model.set('uiId', Ext.data.IdGenerator.get('uuid').generate());
			model.deserializeCt ? model.deserializeCt(child) : model.deserialize(child);
			this.entries.add(model);
		}, this);
	},
	insert: function(entity) {
		if (this.getRoot().entrySelections.length > 0) {
			var task = this.getRoot().entrySelections[0];
			var hasCommonAncestor = false;
			var me = this;
			task.walkToTheRoot(function(currentTaskPathNode) {
				if (currentTaskPathNode.parentVM == me) {
					hasCommonAncestor = true;
					var idx1 = me.entries.indexOf(currentTaskPathNode);
					if (idx1 == me.entries.length - 1) {
						me.entries.add(entity);
						return false;
					}
					var nextEntity = me.entries.getAt(idx1 + 1);
					var newOrder = (currentTaskPathNode.order + nextEntity.order) / 2;
					entity.set('order', newOrder);
					me.entries.insert(idx1 + 1, entity);
				}
			});
			if (hasCommonAncestor)
				return;
		}
		this.entries.add(entity);
	},
	figurePositionAndOrder: function() {
		var order = 0;
		var pos = 0;
		if (this.entries.length > 0) {
			order = this.entries.getAt(this.entries.length - 1).order + 1;
			pos = this.entries.length
		}
		if (this.getRoot().entrySelections.length > 0) {
			var task = this.getRoot().entrySelections[0];
			var me = this;
			var idx1 = -1;
			task.walkToTheRoot(function(currentTaskPathNode) {
				if (currentTaskPathNode.parentVM == me) {
					idx1 = me.entries.indexOf(currentTaskPathNode);
					if (idx1 != -1)
						return false
				}
			});
			if (idx1 == -1 || idx1 + 1 == this.entries.length)
				return {
					order: order,
					pos: pos
				};
			pos = idx1 + 1;
			order = (this.entries.getAt(idx1).order + this.entries.getAt(idx1 + 1).order) / 2;
		}
		return {
			pos: pos,
			order: order
		};
	},
	insertAndSave: function(entityType, modelName, nextAction) {
		var posAndOrder = this.figurePositionAndOrder();
		this.getEntry('', entityType, function(data) {
			var entity = this.model(modelName);
			data.order = posAndOrder.order;
			entity.setData(data);
			entity.saveEntry(function(respData) {
				entity.setData(respData.data);
				this.entries.insert(posAndOrder.pos, entity);
				if (Ext.isFunction(nextAction)) {
					nextAction(entity);
				}
			}, this);
		});
	},
	removeFromParentAndUnselectChild: function(move) {
		if (move !== true)
			this.set('deleted', true);
		if (this.getRoot().entrySelections.length > 0) {
			var me = this;
			this.getRoot().entrySelections[0].walkToTheRoot(function(currentNode) {
				if (currentNode == me)
					me.getRoot().set('entrySelections', []);
			});
		}
		this.parentVM.removeEntry(this);
		this.unParent();
	}
});