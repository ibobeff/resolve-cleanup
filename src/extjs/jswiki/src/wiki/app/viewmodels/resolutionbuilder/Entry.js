glu.defModel('RS.wiki.resolutionbuilder.Entry', {
	uiId: '',
	order: '',
	selected: false,
	deleted: false,
	isSelectable: function() {
		return this.entityType && this.entityType == 'task'
	},
	entryClicked: function(evt) {
		if (!this.canSelect)
			return;
		if (!this.parentVM) {
			throw 'entrySelections must be defined!';
		}
		// if (evt.ctrlKey) {
		// 	var newSelections = [];
		// 	var found = false;
		// 	Ext.each(this.parentVM.entrySelections, function(selection) {
		// 		if (this.uiId == selection.uiId) {
		// 			found == true;
		// 			return;
		// 		}
		// 		newSelections.push(selection);
		// 	}, this);
		// 	if (found)
		// 		this.parentVM.set('entrySelections', newSelections);
		// 	else
		// 		this.parentVM.set('entrySelections', newSelections.concat([this]));
		// } else
		this.getRoot().select(this);
	},
	beforeSave: function(nextAction, data) {
		nextAction();
	},
	getData: function() {
		//override if u have extra data
		return this.asObject ? this.asObject() : {};
	},
	setData: function(data) {
		if (this.loadData) this.loadData(Ext.applyIf(data, this.defaultData));
	},
	getRoot: function() {
		var current = this;
		while (current.entityType != 'builder') current = current.parentVM;
		return current;
	},
	walkToTheRoot: function(visitorFunc) {
		var current = this;
		while (current.entityType != 'builder') {
			if (visitorFunc(current) === false)
				break;
			current = current.parentVM
		};
		return current;
	},
	getPath: function() {
		var current = this;
		var path = [];
		while (current.parentVM.entries) {
			path.push(current.uiId);
			current = current.parentVM;
		}
		return path.join('*');
	},
	remove: function() {
		this.removeFromParent()
	},
	removeFromParent: function(move) {
		if (move !== true)
			this.set('deleted', true);
		if (this.getRoot().entrySelections[0] == this)
			this.getRoot().set('entrySelections', []);
		this.parentVM.removeEntry(this);
		this.unParent();
	},
	serialize: function() {
		return {
			data: this.getData()
		};
	},
	deserialize: function(node) {
		var idx = null;
		if (/^New Task\d*$/.test(node.name)) {
			idx = parseInt(node.name.substring(8));
			if (idx > this.getRoot().maxTaskIdx)
				this.getRoot().set('maxTaskIdx', idx);
		}
		if (/^NewRunbook.New Runbook\d*$/.test(node.name)) {
			idx = parseInt(node.name.substring('NewRunbook.New Runbook'.length));
			if (idx > this.getRoot().maxRunbookIdx)
				this.getRoot().set('maxRunbookIdx', idx);
		}
		this.setData(node);
	},
	isInLoop: function() {
		var parent = this.parentVM;
		while (parent.entityType && parent.entityType != 'builder') {
			if (parent.entityType == 'loop')
				return true;
			parent = parent.parentVM;
		}
		return false;
	},
	canSelect: true,
	initMixin: function() {
		var meta = this.getRoot().parentVM;
		meta.on('reqCountChanged', function() {
			if (meta.reqCount > 0)
				this.set('canSelect', false);
			else
				this.set('canSelect', true);
		}, this)
	}
});