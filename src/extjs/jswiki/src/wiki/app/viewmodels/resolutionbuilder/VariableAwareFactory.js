glu.defModel('RS.wiki.resolutionbuilder.VariableAwareFactory', function(config) {
	Ext.each(config.variables, function(variable) {

		config[variable] = '';
		config[variable + 'FromConst'] = true;
		config[variable + 'FromParam'] = false;
		config[variable + 'FromProperty'] = false;
		config[variable + 'FromWSDATAOrFlow'] = false;
		config[variable + 'Source'] = 'CONSTANT';
		config['when_' + variable + 'Source_changed'] = {
			on: [variable + 'SourceChanged'],
			action: function() {
				switch (this[variable + 'Source']) {
					case 'CONSTANT':
						this.set(variable + 'FromConst', true);
						this.set(variable + 'FromParam', false);
						this.set(variable + 'FromProperty', false);
						this.set(variable + 'FromWSDATAOrFlow', false);
						break;
					case 'PARAM':
					case 'PARAMS':
						this.set(variable + 'FromConst', false);
						this.set(variable + 'FromParam', true);
						this.set(variable + 'FromProperty', false);
						this.set(variable + 'FromWSDATAOrFlow', false);
						break;
					case 'PROPERTIES':
						this.set(variable + 'FromConst', false);
						this.set(variable + 'FromParam', false);
						this.set(variable + 'FromProperty', true);
						this.set(variable + 'FromWSDATAOrFlow', false);
						break;
					case 'WSDATA':
					case 'FLOW':
					case 'FLOWS':
						this.set(variable + 'FromConst', false);
						this.set(variable + 'FromParam', false);
						this.set(variable + 'FromProperty', false);
						this.set(variable + 'FromWSDATAOrFlow', true);
						break;
					default:
						alert('unknown type.');
				}
			}
		};

	});

	function root(attributeName, foo) {
		var parentVM = this.parentVM;
		while (parentVM && !parentVM[attributeName])
			parentVM = parentVM.parentVM
		if (foo)
			foo(parentVM)
		return (parentVM || {})[attributeName];
	}

	config.paramStore$ = function() {
		return root.call(this, 'paramStore');
	};
	config.propertyStore$ = function() {
		return root.call(this, 'propertyStore');
	};
	config.globalVariableStore$ = function() {
		return root.call(this, 'globalVariableStore');
	};
	config.sourceStore$ = function() {
		return root.call(this, 'sourceStore');
	};
	config.comparison = 'eq';
	config.comparisonStore$ = function() {
		return root.call(this, 'comparisonStore');
	};
	config.updateGlobalVariableStore = function(sourceType, excludeSelf, actionNode) {
		root.call(this, 'updateGlobalVariableStore', function(vm) {
			vm.updateGlobalVariableStore(sourceType, excludeSelf, actionNode);
		})
	}
	return config;
});