glu.defModel('RS.wiki.resolutionbuilder.Parameter', {
	columnOptionIsVisible: true,
	worksheetOptionIsVisible: true,
	oneColumn: true,
	twoColumn: false,
	newWorksheet: false,
	currentWorksheet: true,
	firstColumnStore: {
		mtype: 'store',
		fields: ['name', 'displayName', 'type', 'fieldTip', 'sourceName', 'source', {
			name: 'hidden',
			type: 'boolean'
		}, {
			name: 'column',
			type: 'int'
		}, {
			name: 'order',
			type: 'int'
		}]
	},
	paramEditor: null,
	firstColumnSelections: [],
	secondColumnStore: {
		mtype: 'store',
		fields: ['name', 'displayName', 'type', 'fieldTip', 'sourceName', 'source', {
			name: 'hidden',
			type: 'boolean'
		}, {
			name: 'column',
			type: 'int'
		}, {
			name: 'order',
			type: 'init'
		}]
	},
	secondColumnSelections: [],
	editorIsVisible$: function() {
		return this.firstColumnSelections.length > 0 || this.secondColumnSelections.length > 0;
	},
	notifyDirty: function() {
		if (Ext.isFunction(this.rootVM.notifyDirtyFlag)) {
			this.rootVM.notifyDirtyFlag(this.viewmodelName);
		}
	},
	clearDirty: function() {
		if (Ext.isFunction(this.rootVM.clearComponentDirtyFlag)) {
			this.rootVM.clearComponentDirtyFlag(this.viewmodelName);
		}
	},
	init: function() {
		this.set('paramEditor', this.model({
			mtype: 'RS.wiki.resolutionbuilder.ParameterEditor'
		}));
	},
	resetParameters: function() {
		this.firstColumnStore.removeAll();
		this.secondColumnStore.removeAll();
		this.set('oneColumn', true);
		this.set('newWorksheet', true);
	},
	createNewRecord: function() {
		var idx = 0;
		var newParam = 'NewParam';
		while (this.firstColumnStore.find('name', newParam + (idx || '')) != -1 || this.secondColumnStore.find('name', newParam + (idx || '')) != -1)
			idx++;

		newParam += (idx || '');

		return {
			uiId: Ext.data.IdGenerator.get('uuid').generate(),
			name: newParam,
			displayName: newParam,
			type: 'String',
			source: 'CONSTANT',
			hidden: false
		}
	},

	columnNumberChanged$: function() {
		if (this.twoColumn) {
			this.secondColumnStore.removeAll();
			var shouldBeCleaned = [];
			this.firstColumnStore.each(function(r) {
				if (r.get('column') == 2) {
					this.secondColumnStore.add(r);
					shouldBeCleaned.push(r);
				}
			}, this);

			this.firstColumnStore.remove(shouldBeCleaned);
		} else {
			this.secondColumnStore.each(function(r) {
				this.firstColumnStore.add(r);
			}, this)

			this.secondColumnStore.removeAll();
		}

		this.firstColumnStore.sort('name', 'ASC');
		this.secondColumnStore.sort('name', 'ASC');
	},

	typeStore: {
		mtype: 'store',
		fields: ['value', 'name'],
		data: [{
			name: 'String',
			value: 'String'
		}, {
			name: 'Int',
			value: 'Int'
		}, {
			name: 'Decimal',
			value: 'Decimal'
		}, {
			name: 'Date',
			value: 'Date'
		}]
	},

	sourceStore$: function() {
		//only if param is not in RB app we create a new store. if it is in RB app we reuse the store in AutomationMeta to save  memory
		if (!this.parentVM.parentVM)
			return Ext.create('Ext.data.Store', {
				fields: ['name', 'value'],
				data: [{
					name: 'CONSTANT',
					value: 'CONSTANT'
				}, {
					name: 'FLOW',
					value: 'FLOW'
				}, {
					name: 'PARAM',
					value: 'PARAM'
				}, {
					name: 'WSDATA',
					value: 'WSDATA'
				}, {
					name: 'PROPERTY',
					value: 'PROPERTY'
				}]
			});
		return this.parentVM.parentVM.sourceStore;
	},

	addParam1stGrid: function() {
		var r = this.createNewRecord();
		r.column = 1;
		this.firstColumnStore.insert(this.firstColumnStore.getCount(), r);
		this.notifyDirty();

		// clear out all previously selected items
		this.firstColumnStore.grid.getSelectionModel().deselectAll();

		// determine row of newly added "NewParam"
		var row;
		for (row = this.firstColumnStore.data.items.length-1; row > 0; row--) {
			if (this.firstColumnStore.data.items[row].data.name == r.name) {
				break;
			}
		}

		// select newly added item
		this.firstColumnStore.grid.getSelectionModel().select(row);
	},

	removeParams1stGrid: function() {
		if (this.firstColumnSelections.length) {
			this.firstColumnStore.remove(this.firstColumnSelections);
			if (Ext.isFunction(this.rootVM.isStoreDirty)) {
				if (this.rootVM.isStoreDirty(this.firstColumnStore)) {
					this.notifyDirty();
				} else {
					this.clearDirty();
				}
			}
		}
	},

	removeParams1stGridIsEnabled$: function() {
		return this.firstColumnSelections.length > 0 && !this.automationViewTabPressed;
	},

	automationViewTabPressed$: function() {
		return this.rootVM.automationViewTabIsPressed;
	},

	addParam2ndGrid: function() {
		var r = this.createNewRecord();
		r.column = 2;
		this.secondColumnStore.add(r);
		this.notifyDirty();

		/*
		// clear out all previously selected items
		this.secondColumnStore.grid.getSelectionModel().deselectAll();

		// determine row of newly added "NewParam"
		var row;
		for (row = this.secondColumnStore.data.items.length-1; row > 0; row--) {
			if (this.secondColumnStore.data.items[row].data.name == r.name) {
				break;
			}
		}

		// select newly added item
		this.secondColumnStore.grid.getSelectionModel().select(row);
		*/
	},

	removeParams2ndGrid: function() {
		this.secondColumnStore.remove(this.secondColumnStore);
		if (Ext.isFunction(this.rootVM.isStoreDirty)) {
			if (this.rootVM.isStoreDirty(this.secondColumnStore)) {
				this.notifyDirty();
			} else {
				this.clearDirty();
			}
		}
	},

	removeParams2ndGridIsEnabled$: function() {
		return this.secondColumnSelections.length > 0;
	},
	when_first_column_selection_change: {
		on: ['firstColumnSelectionsChanged'],
		action: function(newValue, oldValue) {
			if (this.firstColumnSelections.length == 0)
				return;
			this.set('secondColumnSelections', []);
			if (this.firstColumnSelections.length == 1) {
				this.paramEditor.dumper.dump();
				this.editParam(this.firstColumnSelections[0]);
			}
		}
	},
	when_second_column_selection_change: {
		on: ['secondColumnSelectionsChanged'],
		action: function(newValue, oldValue) {
			if (this.secondColumnSelections.length == 0)
				return;
			this.set('firstColumnSelections', []);
			if (this.secondColumnSelections.length == 1) {
				this.paramEditor.dumper.dump();
				this.editParam(this.secondColumnSelections[0]);
			}
		}
	},
	editParam: function(param) {
		var oldName = param.get('name');
		this.paramEditor.set('nameIsValid', true);
		this.paramEditor.set('storeReferenceName', param.store.referenceName);
		this.paramEditor.set('paramIternalId', param.internalId);
		this.paramEditor.loadData(param.data);
		var me = this;
		this.paramEditor.set('dumper', {
			dump: function() {
				if (me.paramEditor.nameIsValid !== true)
					return
				var data = me.paramEditor.asObject();
				param.set('name', data.name);
				param.set('displayName', data.displayName);
				param.set('type', data.type);
				param.set('sourceName', data.sourceName);
				param.set('source', data.source);
				param.set('hidden', data.hidden);
				param.set('fieldTip', data.fieldTip);
			}
		})
	},
	populateScreen: function(meta) {
		this.resetParameters();
		if (meta.noOfColumn == 1) {
			this.set('oneColumn', true);
			this.set('twoColumn', false);
		} else {
			this.set('oneColumn', false);
			this.set('twoColumn', true);
		}
		Ext.each(meta.params, function(param) {
			this[param.column == 1 ? 'firstColumnStore' : 'secondColumnStore'].add(param);
		}, this);
		this.firstColumnStore.sort('order', 'ASC');
		this.secondColumnStore.sort('order', 'ASC');
		this.set('newWorksheet', meta.newWorksheet);
		this.set('currentWorksheet', !meta.newWorksheet);
	},
	collect: function() {
		if (this.firstColumnSelections.length == 1 && this.secondColumnSelections.length == 0)
			this.paramEditor.dumper.dump();
		if (this.firstColumnSelections.length == 0 && this.secondColumnSelections.length == 1)
			this.paramEditor.dumper.dump();
		var collected = {
			noOfColumn: this.oneColumn ? 1 : 2,
			params: [],
			newWorksheet: this.newWorksheet
		};
		this.firstColumnStore.each(function(r, idx) {
			r.set('order', idx);
			collected.params.push(r.data);
		}, this);

		this.secondColumnStore.each(function(r) {
			r.set('order', idx);
			collected.params.push(r.data);
		}, this);
		return collected;
	},
	checkDropOn1stColumn: function(data) {
		Ext.each(data.records, function(r) {
			r.set('column', 1);
		}, this);
		var count = 0;
		this.firstColumnStore.each(function(r) {
			r.set(count++)
		});
	},
	checkDropOn2ndColumn: function(data) {
		Ext.each(data.records, function(r) {
			r.set('column', 2);
		}, this);
		var count = 0;
		this.secondColumnStore.each(function(r) {
			r.set(count++);
		});
	}
});

glu.defModel('RS.wiki.resolutionbuilder.ParamWindow', {
	parameter: {
		mtype: 'Parameter'
	},

	params: null,
	init: function() {
		this.parameter.populateScreen(this.params);
	},

	dumper: null,

	save: function() {
		this.parentVM.parentVM.saveGeneral()
		if (this.dumper)
			this.dumper();
		this.doClose();
	},
	cancel: function() {
		this.doClose();
	}
})