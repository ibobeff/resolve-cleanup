glu.defModel('RS.wiki.resolutionbuilder.ConditionEntry', {
	entityType: 'condition',
	mixins: ['EntryAccessor', 'Entry', 'EntryContainer', 'DragWatcherContainer'],
	criteria: {
		mtype: 'list'
	},
	id: '',
	order: '',
	connectionType: '',
	fields: ['id', 'entityType', 'expression', {
		name: 'order',
		type: 'float'
	}],
	defaultData: {
		entityType: 'condition'
	},
	getData: function() {
		var criteria = [];
		this.criteria.forEach(function(r) {
			var c = Ext.apply({
				parentId: this.id,
				parentType: this.entityType
			}, r.toServerData());
			criteria.push(c);
		}, this);
		return Ext.apply({
			criteria: criteria
		}, this.asObject())
	},
	init: function() {
		this.entries.on('added', function(entry) {
			entry.set('margin', 10);
		}, this);
		if (this.getRoot().interactive)
			this.config();

		this.set('connectionType', this.parentVM.connectionType);
	},
	setData: function(data) {
		this.loadData(Ext.applyIf(data, this.defaultData));
		this.criteria.removeAll();
		Ext.each(data.criteria, function(condition) {
			var c = this.model('RS.wiki.resolutionbuilder.ConditionEntryRule', Ext.applyIf({
				uiId: Ext.data.IdGenerator.get('uuid').generate()
			}))
			c.fromServerData(condition);
			this.criteria.add(c);
		}, this)
	},
	addTask: function() {
		this.insertAndSave(RS.wiki.resolutionbuilder.viewmodels.TaskEntry.entityType, 'RS.wiki.resolutionbuilder.TaskEntry');
	},
	addRunbook: function() {
		this.insertAndSave(RS.wiki.resolutionbuilder.viewmodels.RunbookEntry.entityType, 'RS.wiki.resolutionbuilder.RunbookEntry');
	},
	addCondition: function() {
		this.add(this.model('RS.wiki.resolutionbuilder.ConditionEntryRule', {
			uiId: Ext.data.IdGenerator.get('uuid').generate()
		}));
	},
	config: function() {
		var me = this;
		this.open({
			mtype: 'RS.wiki.resolutionbuilder.ConditionEntryConfig',
			dumper: function() {
				me.criteria.removeAll();
				this.conditions.forEach(function(c) {
					var m = me.model('RS.wiki.resolutionbuilder.ConditionEntryRule');
					m.fromServerData(c.toServerData());
					me.criteria.add(m);
				}, this);

				me.saveEntry(function(respData) {
					this.setData(respData.data);
				}, me);
			}
		});
	},
	remove: function() {
		this.deleteEntry(function() {
			this.removeFromParentAndUnselectChild();
		}, this)
	}
});