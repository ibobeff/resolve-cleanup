glu.defModel('RS.wiki.resolutionbuilder.ParameterEditor', {
	storeReferenceName: '',
	paramIternalId: '',
	name: '',
	fieldTip: '',
	when_name_changed: {
		on: ['nameChanged'],
		action: function() {
			this.set('displayName', this.name);
			var count = 0;
			var value = this.name;
			this.parentVM.firstColumnStore.each(function(r) {
				if (r.get('name') == value && (this.storeReferenceName != 'firstColumnStore' || this.paramIternalId != r.internalId))
					count++;
			}, this);
			this.parentVM.secondColumnStore.each(function(r) {
				if (r.get('name') == value && (this.storeReferenceName != 'secondColumnStore' || this.paramIternalId != r.internalId))
					count++;
			}, this);
			this.set('nameIsValid', value && count == 0 && RS.common.validation.VariableName.test(this.name) ? true : this.parentVM.localize('invalidParam'));
			if(this.nameIsValid == true)
				this.parentVM.firstColumnSelections[0].set('name', this.name);
		}
	},
	when_displayName_changed: {
		on: ['displayNameChanged'],
		action: function() {
			this.parentVM.firstColumnSelections[0].set('displayName', this.displayName);
		}
	},
	nameIsValid: true,
	displayName: '',
	type: '',
	source: '',
	automationViewTabPressed$: function() {
		return this.rootVM.automationViewTabIsPressed;
	},
	checkDirty: function() {
		if (this.isDirty) {
			if (Ext.isFunction(this.rootVM.notifyDirtyFlag)) {
				this.rootVM.notifyDirtyFlag(this.viewmodelName);
			}
		} else if (Ext.isFunction(this.rootVM.clearComponentDirtyFlag)) {
			this.rootVM.clearComponentDirtyFlag(this.viewmodelName);
		}
	},
	typeStore: {
		mtype: 'store',
		fields: ['value', 'name'],
		data: [{
			name: 'String',
			value: 'String'
		}, {
			name: 'Int',
			value: 'Int'
		}, {
			name: 'Decimal',
			value: 'Decimal'
		}, {
			name: 'Date',
			value: 'Date'
		}, {
			name: 'Encrypted',
			value: 'Encrypted'
		}]
	},

	sourceStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [{
			name: 'CONSTANT',
			value: 'CONSTANT'
		}, {
			name: 'PARAM',
			value: 'PARAM'
		}, {
			name: 'FLOW',
			value: 'FLOW'
		}, {
			name: 'WSDATA',
			value: 'WSDATA'
		}, {
			name: 'PROPERTY',
			value: 'PROPERTY'
		}]
	},
	sourceName: '',
	hidden: false,
	fields: ['uiId', 'name', 'displayName', 'type', 'sourceName', 'source', 'fieldTip', {
		name: 'hidden',
		type: 'bool'
	}],
	dumper: {
		dump: function() {}
	}
});