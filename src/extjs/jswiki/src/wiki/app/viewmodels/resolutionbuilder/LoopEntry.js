glu.defModel('RS.wiki.resolutionbuilder.LoopEntry', {
	entityType: 'loop',
	description: '',
	order: 0,
	count: '',
	itemSource: '',
	itemSourceName: '',
	itemDelimiter: '',
	mixins: ['EntryAccessor', 'Entry', 'EntryContainer'],
	id: '',
	defaultData: {
		entityType: 'loop',
		itemSource: 'FLOW',
		itemSourceName: '',
		itemDelimiter: 'comma',
		count: 0
	},
	fields: ['id', 'entityType', {
		name: 'order',
		type: 'float'
	}, 'itemSourceName', 'itemSource', {
		name: 'count',
		type: 'int'
	}, 'itemDelimiter'],
	init: function() {
		this.entries.on('added', function(entry) {
			entry.set('margin', 10);
		}, this);
		this.loadData(this.defaultData);
		if (this.getRoot().interactive)
			this.config();
	},
	addConnection: function() {
		this.insertAndSave(RS.wiki.resolutionbuilder.viewmodels.ConnectionEntry.entityType, 'RS.wiki.resolutionbuilder.ConnectionEntry');
	},
	config: function() {
		var me = this;
		this.open({
			mtype: 'RS.wiki.resolutionbuilder.LoopEntryConfig',
			initConfig: function() {
				this.set('count', me.count);
				this.set('item', me.itemSourceName);
				this.set('itemDelimiter', me.itemDelimiter);
				this.set('itemSource', me.itemSource);
			},
			dumper: function() {
				if (this.fixed) {
					me.set('itemSource', 'CONSTANT');
					me.set('itemSourceName', '');
					me.set('count', this.count);
					me.set('itemDelimiter', 'comma');
				} else {
					me.set('count', 0);
					me.set('itemSource', this.itemSource);
					me.set('itemSourceName', this.item);
					me.set('itemDelimiter', this.itemDelimiter);
				}
				me.saveEntry(function(respData) {
					me.setData(respData.data)
				});
			}
		});
	},
	remove: function() {
		this.deleteEntry(function() {
			this.removeFromParentAndUnselectChild();
		}, this)
	}
});