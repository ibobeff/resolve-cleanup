glu.defModel('RS.wiki.resolutionbuilder.RunbookGenerator', {
	name: '',
	namespace: '',
	fields: ['name', 'namespace', 'summary'],
	generate: function() {
		this.cancel();
	},
	cancel: function() {
		this.doClose();
	}
});