glu.defModel('RS.wiki.resolutionbuilder.ConnectionEntry', {
	entityType: 'connection',
	connectionEntry$: function() {
		return this.localize('connectionEntry','<' + this.localize(this.type) + '>');
	},
	mixins: ['EntryAccessor', 'Entry', 'EntryContainer', 'DragWatcherContainer'],
	margin: 20,
	id: '',
	type: 'ssh',
	order: 0,
	params: [],
	fields: ['entityType', 'id', 'type', {
		name: 'order',
		type: 'float'
	}],
	init: function() {
		this.entries.on('added', function(entry) {
			if (entry.entityType == 'task')
				entry.set('margin', '10 30 10 30');
		}, this);
		if (this.getRoot().interactive)
			this.config();
	},
	getData: function() {
		var data = this.asObject();
		data.params = this.params;
		return data;
	},
	setData: function(data) {
		this.loadData(Ext.applyIf(data, this.defaultData));
		this.set('params', data.params);
	},
	defaultData: {
		type: 'ssh',
		entityType: 'connection'
	},
	addTask: function() {
		this.insertAndSave(RS.wiki.resolutionbuilder.viewmodels.TaskEntry.entityType, 'RS.wiki.resolutionbuilder.TaskEntry');
	},
	addIf: function() {
		this.insertAndSave(RS.wiki.resolutionbuilder.viewmodels.IfEntry.entityType, 'RS.wiki.resolutionbuilder.IfEntry', function(ifEntry) {
			ifEntry.addCondition();
		});
	},
	addRunbook: function() {
		this.insertAndSave(RS.wiki.resolutionbuilder.viewmodels.RunbookEntry.entityType, 'RS.wiki.resolutionbuilder.RunbookEntry');
	},
	config: function() {
		var me = this;
		var config = {
			ssh: this.type == 'ssh',
			database: this.type == 'database',
			http: this.type == 'http',
			telnet: this.type == 'telnet',
			local: this.type == 'local',
			mtype: 'RS.wiki.resolutionbuilder.ConnectionEntryConfig',
			dumper: function(params) {
				me.set('params', params);
				me.set('type', this.type);
				me.saveEntry();
			}
		};
		this.open(config);
	},
	remove: function() {
		this.deleteEntry(function() {
			this.removeFromParentAndUnselectChild();
		}, this)
	}
})