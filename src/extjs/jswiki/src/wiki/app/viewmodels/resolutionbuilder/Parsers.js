glu.defModel('RS.wiki.resolutionbuilder.Parsers', {
	textType: true,
	textTypeChanged$: function() {
		if (this.textType && this.parsers.indexOf(this.manualMarkupParser) != -1)
			this.set('activeItem', this.manualMarkupParser);
	},
	tableType: false,
	tableTypeChanged$: function() {
		if (this.tableType && this.parsers.indexOf(this.tableMarkupParser) != -1)
			this.set('activeItem', this.tableMarkupParser);
	},
	xmlType: false,
	xmlTypeChanged$: function() {
		if (this.xmlType && this.parsers.indexOf(this.xmlParser) != -1)
			this.set('activeItem', this.xmlParser);
	},
	parsers: {
		mtype: 'activatorlist',
		autoParent: true,
		focusProperty: 'activeItem'
	},
	manualMarkupParser: {
		mtype: 'PlainTextManualMarkupParser'
	},
	tableMarkupParser: {
		mtype: 'PlainTextTableMarkupParser'
	},
	xmlParser: {
		mtype: 'XmlParser'
	},
	activeItem: 0,
	parserIsDirty: false,
	variableStore: null,
	init: function() {
		this.parsers.add(this.manualMarkupParser);
		this.parsers.add(this.tableMarkupParser);
		this.parsers.add(this.xmlParser);
		this.parsers.forEach(function(p) {
			var me = this;
			p.on('parserIsDirtyChanged', function() {
				me.set('parserIsDirty', this.parserIsDirty);
			}, this);
			p.on('activate', function() {
				me.set('variableStore', p.variableStore);
			});
		}, this);
		this.set('activeItem', this.manualMarkupParser);
	},
	resetParser: function() {
		this.parsers.forEach(function(parser) {
			parser.resetParser();
		});
	},
	setData: function(data) {
		Ext.each(['textType', 'tableType', 'xmlType'], function(type) {
			if (data.parserType + 'Type' == type)
				this.set(type, true);
			else
				this.set(type, false);
		}, this);
		this.parsers.getActiveItem().setData(data);
		this.parsers.getActiveItem().afterSetData();
	},
	getVariableStore: function() {
		if (this.textType)
			return this.manualMarkupParser.variableStore;
		else if (this.tableType)
			return this.tableMarkupParser.variableStore;
		else
			return this.xmlParser.variableStore;
	},
	getData: function() {
		var parserData = this.parsers.getActiveItem().getData();
		Ext.each(['textType', 'tableType', 'xmlType'], function(type) {
			if (this[type]) {
				parserData.parserType = type.substring(0, type.length - 4);
				return false;
			}
		}, this);
		return parserData;
	}
});