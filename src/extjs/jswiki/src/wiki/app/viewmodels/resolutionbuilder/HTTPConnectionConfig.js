glu.defModel('RS.wiki.resolutionbuilder.HTTPConnectionConfig', {
	optionVM: null,
	protocolStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},
	protocol: 'http',
	host: '',
	port: '',
	init: function() {
		this.protocolStore.add([{
			name: this.localize('http'),
			value: 'http'
		}, {
			name: this.localize('https'),
			value: 'https'
		}]);
	},
	activate: function() {
		if (this.parentVM.parentVM.type != 'http')
			return;
		this.loadParams();
	},
	loadParams: function() {
		if (!this.parentVM.parentVM.params)
			return;
		Ext.Array.forEach(this.parentVM.parentVM.params, function(param) {
			if (param.name == 'protocol')
				this.set('protocol', param.sourceName)
			else if (param.name == 'host')
				this.set('host', param.sourceName)
			else if (param.name == 'port')
				this.set('port', param.sourceName)
		}, this)
	},
	getAsParams: function() {
		return [{
			name: 'protocol',
			sourceName: this.protocol,
			source: 'CONSTANT'
		}, {
			name: 'host',
			sourceName: this.host,
			source: 'CONSTANT'
		}, {
			name: 'port',
			sourceName: this.port,
			source: 'CONSTANT'
		}];
	}
});

glu.defModel('RS.wiki.resolutionbuilder.HTTPConnectionConfigOption', {});