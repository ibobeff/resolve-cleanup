glu.defModel('RS.wiki.resolutionbuilder.RunbookEditor', {
	mixins: ['VariableAware'],
	variables: [],
	runbook: null,
	runbookStore: {
		mtype: 'store',
		fields: ['ufullname'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wikiadmin/listRunbooks',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	wikiParameters: {
		mtype: 'store',
		fields: ['name', 'type', 'sourceName', 'source', 'fieldTip']
	},
	parameterStore: {
		mtype: 'store',
		fields: ['name', 'type', 'sourceName', 'source', 'fieldTip']
	},
	init: function() {
		this.runbookStore.on('beforeload', function(store, op) {
			op.params = op.params || {};
			Ext.apply(op.params, {
				filter: Ext.encode([{
					field: 'ufullname',
					condition: 'contains',
					type: 'auto',
					value: op.params.query || this.name
				}])
			});
		}, this);
		this.runbookStore.on('load', function() {
			this.validateRunbook();
		}, this);
		this.set('loadTask', new Ext.util.DelayedTask(function() {
			this.runbookStore.load();
		}, this));
	},
	id: '',
	name: '',
	nameIsValid: true,
	validateRunbook: function() {
		var r = this.runbookStore.findRecord('ufullname', this.name, 0, false, false, true);
		if (!r) {
			this.set('nameIsValid', this.localize('invalidRunbook'));
			// this.parentVM.set('editorIsValid', false);
			return;
		}
		this.set('nameIsValid', true);
		this.parentVM.set('editorIsValid', true);
		this.wikiParameters.removeAll();
		try {
			var params = Ext.decode(r.raw.uwikiParameters) || [];
			this.wikiParameters.add(params);
			Ext.each(modelInputs, function(p) {
				var r = this.parameterStore.findRecord('name', p.name);
				if (r) {
					r.set('source', p.source);
					r.set('sourceName', p.value);
					return;
				}
				this.parameterStore.add(p);
			}, this);
		} catch (e) {
			//for invalid json
		}
	},
	loadTask: null,
	when_name_changed: {
		on: ['nameChanged'],
		action: function() {
			if (this.parentVM.entrySelections.length == 0)
				return;
			this.parentVM.entrySelections[0].set('name', this.name);
		}
	},
	jumpToRunbook: function() {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			target: '_blank',
			params: {
				name: this.name,
				activeTab: 2
			}
		});
	},
	searchRunbook: function() {
		var me = this;
		this.open({
			mtype: 'RS.wiki.RunbookPicker',
			dumper: {
				dump: function(selections) {
					if (selections.length > 0) {
						var selection = selections[0]
						me.set('runbook', Ext.apply({}, selection.raw));
						me.set('name', me.runbook.ufullname);
						var params = Ext.decode(me.runbook.uwikiParameters || '[]');
						Ext.each(params, function(param) {
							me.parameterStore.add(param)
						});
					}

					me.runbookStore.load();
				}
			}
		})
	},
	openExecutionWindow: function(button) {
		var me = this;
		if (this.executionWin) {
			this.executionWin.doClose()
		} else {
			this.executionWin = this.open({
				mtype: 'RS.actiontaskbuilder.Execute',
				target: button.getEl().id,
				fromWiki: true,
				inputsLoader: function(paramStore, origParamStore, mockStore) {
					//Load up the wiki params into the grid and the mock values for the wiki
					me.parameterStore.each(function(param) {
						param.data.value = param.get('sourceName');
						param.data.utype = 'PARAM';
						paramStore.add(param);
						origParamStore.add(param);
					}, this);
				},
				executeDTO: {
					wiki: me.name
				}
			})
			this.executionWin.on('closed', function() {
				this.executionWin = null
			}, this)
		}
	},
	addParam: function() {
		var found = null;
		this.wikiParameters.each(function(param) {
			var idx = this.parameterStore.findExact('name', param.get('name'));
			if (idx == -1) {
				found = param
				return;
			}
			var modelParam = this.parameterStore.getAt(idx);
			modelParam.set('fieldTip', param.get('fieldTip'));
		}, this);
		if (found) {
			this.parameterStore.add(found);
			return;
		}
		var max = 0;
		var reg = /^NewParam(\d*)$/
		this.parameterStore.each(function(param) {
			if (!reg.test(param.get('name')))
				return;
			var m = reg.exec(param.get('name'));
			var idx = parseInt(m[1]);
			if (idx > max)
				max = idx;
		}, this);
		this.parameterStore.add({
			name: 'NewParam' + ((max + 1) || ''),
			source: 'CONSTANT',
			value: ''
		});
	},
	addParamIsEnabled$: function() {
		return this.nameIsValid === true;
	},
	paramsSelections: [],
	removeParams: function() {
		this.parameterStore.remove(this.paramsSelections);
	},
	removeParamsIsEnabled$: function() {
		return this.paramsSelections.length > 0;
	},
	resetEditorScreen: function(data) {
		this.set('runbook', null);
		this.set('name', 'New Runbook');
		this.wikiParameters.removeAll();
		this.parameterStore.removeAll();
	},
	extractor: /^\$(.+)\{(.+)\}$/,
	populateEditorScreen: function(data, entry) {
		this.set('id', data.id);
		this.set('name', data.name);
		this.parameterStore.removeAll();
		var cdata = Ext.decode(data.refRunbookParams || '{}');
		Ext.Object.each(cdata, function(name, value) {
			var m = null;
			if (this.extractor.test(value))
				m = this.extractor.exec(value);
			this.parameterStore.add({
				name: name,
				source: m ? m[1] : 'CONSTANT',
				sourceName: m ? m[2] : value
			});
		}, this);
		this.runbookStore.load();
	},
	collectEditorScreen: function() {
		var cdata = {};
		this.parameterStore.each(function(item) {
			var value = item.get('sourceName');
			if (item.get('source').toLowerCase() != 'constant') {
				if (item.get('sourceName'))
					value = Ext.String.format('${0}\{{1}\}', item.get('source'), item.get('sourceName'))
				else
					clientVM.displayError(this.localize('nonConstantEmptyValue', item.get('name')));
			}
			cdata[item.get('name')] = value;
		}, this);
		return {
			id: this.id,
			name: this.name,
			refRunbookParams: Ext.encode(cdata) + ' '
		};
	},
	updateGlobalVariableStore: function(source) {
		this.updateGlobalVariableStore(source);
	},
	runbookSelected: function(selections) {
		if (selections.length == 0)
			return;
		var r = selections[0];
		this.set('name', r.get('ufullname'));
		this.loadTask.delay(100);
	}
});