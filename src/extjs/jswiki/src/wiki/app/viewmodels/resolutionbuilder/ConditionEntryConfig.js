glu.defModel('RS.wiki.resolutionbuilder.ConditionEntryConfig', {
	mixins: ['ConditionContainer'],
	init: function() {
		this.parentVM.criteria.forEach(function(c) {
			this.addCondition('RS.wiki.resolutionbuilder.ConditionEntryRule', {}, function(vm) {
				vm.fromServerData(c.toServerData());
			});
		}, this);
		if (this.conditions.length == 0)
			this.add();
	},
	add: function() {
		this.addCondition('RS.wiki.resolutionbuilder.ConditionEntryRule');
	},
	remove: function() {
		this.removeConditions();
	},
	removeIsEnabled$: function() {
		return this.removeConditionsIsEnabled;
	},
	dumper: null,
	update: function() {
		if (this.dumper)
			this.dumper();
		this.cancel();
	},
	cancel: function() {
		this.doClose();
	}
});
glu.defModel('RS.wiki.resolutionbuilder.ConditionEntryRule', {
	mixins: ['BasicCondition', 'VariableAware'],
	fields: ['variable', 'variableSource', 'comparison', 'value', 'valueSource'],
	variables: ['variable', 'value'],
	defaultData: {
		variableSource: 'FLOW',
		valueSource: 'FLOW',
		comparison: 'eq'
	},
	init: function() {
		this.loadData(this.defaultData);
	},
	variableChanged$: function() {
		this.propertyStore.keyword = this.variable;
	},
	valueChanged$: function() {
		this.propertyStore.keyword = this.value;
	},
	comparison: 'eq',
	updateSourceNames: function() {
		this.updateGlobalVariableStore(this.valueSource, false, this.parentVM.parentVM);
	},
	updateVariables: function() {
		this.updateGlobalVariableStore(this.variableSource, false, this.parentVM.parentVM);
	},
	toServerData: function() {
		var obj = this.asObject();
		obj.source = this.valueSource;
		obj.sourceName = this.value;
		delete obj.value;
		delete obj.valueSource;
		return obj;
	},
	fromServerData: function(data) {
		data.valueSource = data.source
		data.value = data.sourceName
		this.loadData(data);
	}
});