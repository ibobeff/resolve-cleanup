glu.defModel('RS.wiki.resolutionbuilder.LocalConnectionConfig', {
	optionVM: null,
	loadParams: function() {
		this.optionVM.loadData(this.optionVM.defaultData);
		Ext.each(this.parentVM.parentVM.params, function(param) {
			if (this.optionVM[param.name + 'Source']) {
				this.optionVM.set(param.name + 'Source', param.source);
				this.optionVM.set(param.name, param.sourceName);
			}
		}, this)
	},
	activate: function() {
		if (this.parentVM.parentVM.type != 'local')
			return;
		this.loadParams();
	},
	getAsParams: function() {
		var name = '';
		var params = [];
		for (key in this) {
			if (key.indexOf('Source') == -1 || key.indexOf('Source') != key.length - 6)
				continue;
			name = key.substring(0, key.length - 6);
			params.push({
				name: name,
				sourceName: this[name],
				source: this[key]
			});
		}
		var options = this.optionVM.asObject();
		Ext.Object.each(options, function(k, v) {
			if (k == 'id')
				return;
			if (k == 'queueName')
				params.push({
					name: 'queueName',
					sourceName: v,
					source: options['queueNameSource']
				})
			else
				params.push({
					name: k,
					sourceName: v,
					source: 'CONSTANT'
				});
		}, this)
		return params;
	}
});
glu.defModel('RS.wiki.resolutionbuilder.LocalConnectionConfigOption', {
	mixins: ['VariableAware'],
	fields: ['queueName', 'queueNameSource'],
	variables: ['queueName'],
	defaultData: {
		queueName: '',
		queueNameSource: 'CONSTANT'
	},
	queueNameChanged$: function() {
		if (!this.queueName);
		this.propertyStore.keyword = this.queueName;
	},
	queueNameSourceChanged$: function() {
		this.set('queueName', '');
	},
	updateQueueNameVariables: function() {
		this.updateGlobalVariableStore(this.queueNameSource, true, this.parentVM.parentVM);
	}
});