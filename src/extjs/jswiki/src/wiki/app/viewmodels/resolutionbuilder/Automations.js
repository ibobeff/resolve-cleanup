glu.defModel('RS.wiki.resolutionbuilder.Automations', {
	stateId: 'rswikiautomationautomations',
	displayName: '',
	wait: false,
	store: {
		mtype: 'store',
		fields: ['id', 'uname', 'unamespace', 'usummary', 'ufullname', {
			name: 'uhasResolutionBuilder',
			type: 'bool'
		}, 'uresolutionBuilderId', {
			name: 'uhasActiveModel',
			type: 'bool'
		}, {
			name: 'uisDeleted',
			type: 'bool'
		}].concat(RS.common.grid.getSysFields()),
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		remoteSort: true,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wikiadmin/listRunbooks',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.store.load();
	},
	init: function() {
		if (this.mock) this.backend = RS.wiki.resolutionbuilder.createMockBackend(true)
		this.set('displayName', this.localize('displayName'))
		this.store.on('beforeload', function(store, op) {
			this.set('wait', true);
		}, this);
		this.store.on('load', function(store, records, success) {
			this.set('wait', false);
			//if (!success)
			//	clientVM.displayError(this.localize('listAutomationError', this.store.getProxy().getReader().rawData.message));
		}, this);

		// generate page token for actiontask/editorstub.jsp
		clientVM.getCSRFToken_ForURI('/resolve/wiki/editorstub.jsp');
	},
	automationMetasSelections: [],
	createAutomation: function() {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.resolutionbuilder.AutomationMeta'
		});
	},
	sendReq: function(url, errMsg, sucMsg, params, lockScreen) {
		lockScreen.set('wait', true);
		var ids = [];
		Ext.each(this.automationMetasSelections, function(r, idx) {
			ids.push(r.get('id'));
		});
		var reqParams = {};
		reqParams.ids = ids;
		reqParams.all = this.allSelected;
		if (params)
			Ext.apply(reqParams, params);
		var win = this.open({
			mtype: 'RS.wiki.Polling'
		});
		this.ajax({
			url: url,
			params: reqParams,
			scope: this,
			success: function(resp) {
				win.doClose();
				this.handleSucResp(resp, errMsg, sucMsg);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				lockScreen.set('wait', false);
			}
		});
	},
	handleSucResp: function(resp, errMsg, sucMsg) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize(errMsg) + '[' + respData.message + ']');
		} else
			clientVM.displaySuccess(this.localize(sucMsg));

		this.set('automationMetasSelections', []);

		this.store.load();
	},
	showMessage: function(title, OpsMsg, OpMsg, confirm, url, errMsg, sucMsg, params) {
		var strFullName = [];
		Ext.each(this.automationMetasSelections, function(r, idx) {
			strFullName.push(r.get('unamespace') + '.' + r.get('uname'));
		});
		var msg = this.localize(this.automationMetasSelections.length > 1 || this.allSelected ? OpsMsg : OpMsg, {
			num: this.allSelected ? this.store.getTotalCount() : this.automationMetasSelections.length,
			names: strFullName.join(',')
		});

		// msg += '<br/><input type="checkbox" id="index_attachment" /> Index Attachments' +
		// '<input type="checkbox" id="index_actiontask" /> Index ActionTasks<br/><br/>';
		this.message({
			title: this.localize(title),
			msg: msg,
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize(confirm),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				params = params || {};
				params.indexAttachment = true;
				params.indexActionTask = true;
				this.sendReq(url, errMsg, sucMsg, params, this);
			}
		});
	},
	doRename: function(params) {
		this.showMessage(
			'renameAutomations',
			'renamesMsg',
			'renameMsg',
			'renameAutomations',
			'/resolve/service/wikiadmin/moveRenameCopy',
			'renameErrMsg',
			'renameSucMsg',
			params
		);
	},

	renameAutomations: function() {
		var me = this;
		this.open({
			mtype: 'RS.wiki.pagebuilder.MoveOrRename',
			action: 'rename',
			namespace: this.automationMetasSelections[0].get('unamespace'),
			filename: this.automationMetasSelections[0].get('uname'),
			filenameIsVisible$: function() {
				return me.automationMetasSelections.length == 1;
			},
			moveRenameCopyTitle: 'RenameTitle',
			modelName: this.viewmodelName,
			selectionsLength: this.automationMetasSelections.length,
			dumper: function(params) {
				me.doRename(params);
			}
		});
	},
	renameAutomationsIsEnabled$: function() {
		return !this.wait && this.automationMetasSelections.length > 0;
	},
	doCopy: function(params) {
		this.showMessage(
			'copyAutomations',
			'copysMsg',
			'copyMsg',
			'copyAutomations',
			'/resolve/service/wikiadmin/moveRenameCopy',
			'copyErrMsg',
			'copySucMsg',
			params
		);
	},

	copyAutomations: function() {
		var me = this;
		this.open({
			mtype: 'RS.wiki.pagebuilder.MoveOrRename',
			action: 'copy',
			namespace: this.automationMetasSelections[0].get('unamespace'),
			filename: this.automationMetasSelections[0].get('uname'),
			filenameIsVisible$: function() {
				return me.automationMetasSelections.length == 1;
			},
			moveRenameCopyTitle: 'CopyTitle',
			modelName: this.viewmodelName,
			selectionsLength: this.automationMetasSelections.length,
			dumper: (function(self) {
				return function(params) {
					self.doCopy(params)
				}
			})(this)
		});
	},
	copyAutomationsIsEnabled$: function() {
		return !this.wait && this.automationMetasSelections.length > 0;
	},
	deleteAutomations: function() {
		this.showMessage(
			'deleteAutomations',
			'deletesMsg',
			'deleteMsg',
			'deleteAutomations',
			'/resolve/service/wikiadmin/setDeleted',
			'deleteErrMsg',
			'deleteSucMsg', {
				on: true
			}
		);
	},
	deleteAutomationsIsEnabled$: function() {
		return !this.wait && this.automationMetasSelections.length > 0;
	},
	purgeAutomations: function() {
		this.showMessage(
			'purgeAutomations',
			'purgesMsg',
			'purgeMsg',
			'purgeAutomations',
			'/resolve/service/wikiadmin/purge',
			'purgeErrMsg',
			'purgeSucMsg'
		);
	},
	purgeAutomationsIsEnabled$: function() {
		return !this.wait && this.automationMetasSelections.length > 0;
	},
	editAutomation: function(id) {
		var r = this.store.findRecord('id', id);
		if (r.get('uresolutionBuilderId') && r.raw.sysUpdatedOn <= r.raw.rbGeneralVO.sysUpdatedOn)
			clientVM.handleNavigation({
				modelName: 'RS.wiki.resolutionbuilder.AutomationMeta',
				params: {
					id: r.get('uresolutionBuilderId')
				}
			});
		else
			clientVM.handleNavigation({
				modelName: 'RS.wiki.Main',
				params: {
					name: r.get('ufullname'),
					activeTab: 2
				}
			})
	},
	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	}
});