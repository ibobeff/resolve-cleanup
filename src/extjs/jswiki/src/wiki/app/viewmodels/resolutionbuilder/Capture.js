glu.defModel('RS.wiki.resolutionbuilder.Capture', {
	name: '',
	text: '',
	alreadyMarkedUp: false,
	nameIsValid$: function() {
		return !/^\s*$/.test(this.name) ? true : this.localize('invalidName');
	},
	typeIsVisible: true,
	type: 'letters',
	autoTypeLogic: function(text) {
		if (this.alreadyMarkedUp)
			return 'useCurrentTypeMarkups';
		var guesses = [{
			type: 'whitespace',
			v: /^\s+$/
		}, {
			type: 'decimal',
			v: /^\d*\.{1}\d+$/
		}, {
			type: 'number',
			v: /^\d+$/
		}, {
			type: 'letters',
			v: /^[a-zA-z]+$/
		}, {
			type: 'lettersAndNumbers',
			v: /^[a-zA-z|\d]+$/
		}, {
			type: 'IPv4',
			v: {
				test: function(text) {
					var match = text.match(/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/);
					return match != null &&
						match[1] <= 255 && match[2] <= 255 &&
						match[3] <= 255 && match[4] <= 255;
				}
			}
		}, {
			type: 'IPv6',
			v: /^(([0-9a-fA-F]{1,4}:){7,7}[0-9a''-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))$/
		}, {
			type: 'MAC',
			v: /^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$/
		}, {
			type: 'lettersAndNumbers',
			v: /^[\w|\d]$/
		}];
		for (var i = 0; i < guesses.length; i++)
			if (guesses[i].v.test(text))
				return guesses[i].type;

		return 'ignoreNotGreedy';
	},
	dumper: null,
	typeStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: []
	},
	init: function() {
		var me = this;
		var store = Ext.create('Ext.data.Store', {
			fields: [{
				name: 'name',
				convert: function(v, record) {
					if (record.raw == '-' || record.raw == '-<>')
						return '';
					return me.localize(record.raw);
				}
			}, {
				name: 'value',
				convert: function(v, record) {
					return record.raw;
				}
			}],
			filters: [

				function(record, id) {
					if (!me.alreadyMarkedUp)
						return (record.data.value != 'useCurrentTypeMarkups');
					return true;
				}
			],
			data: [
				'useCurrentTypeMarkups',
				'-<>',
				'ignoreNotGreedy',
				'ignoreRemainder',
				'-',
				'spaceOrTab',
				'whitespace',
				'notWhitespace',
				'-',
				'letters',
				'lettersAndNumbers',
				'notLettersOrNumbers',
				'lowerCase',
				'upperCase',
				'-',
				'number',
				'notNumber',
				'-',
				'beginOfLine',
				'endOfLine',
				'-',
				'IPv4',
				//				'IPv6',
				'MAC',
				'decimal'
			]
		});
		this.set('typeStore', store);
		this.set('type', this.autoTypeLogic(this.text));
	},
	capture: function() {
		//this is for the keyup event
		if (!this.captureIsEnabled)
			return;
		this.dumper();
		this.cancel();
	},
	captureIsEnabled$: function() {
		return this.isValid;
	},
	cancel: function() {
		this.doClose();
	}
});