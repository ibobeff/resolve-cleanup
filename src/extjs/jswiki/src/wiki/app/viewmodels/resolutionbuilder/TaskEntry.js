glu.defModel('RS.wiki.resolutionbuilder.TaskEntry', {
	mixins: ['EntryAccessor', 'Entry', 'DragWatcher'],
	entityType: 'task',
	id: '',
	name: '',
	command: '',
	encodedCommand$: function() {
		return encodeURIComponent(encodeURIComponent(this.command));
	},
	order: '',
	repeat: false,
	summary: '${RAW}',
	description: '',
	detail: '${RAW}',
	defaultAssess: 'GOOD',
	defaultSeverity: 'GOOD',
	continuation: 'always',
	assessorScript: '',
	parserScript: '',
	assessorAutoGenEnabled: false,
	parserAutoGenEnabled: false,
	sampleOutput: '',

	conditions: [],
	severities: [],
	variables: [],
	parserConfig: '{markups:[]}',

	fields: ['id', 'name', 'command', 'queueName', 'queueNameSource', 'promptSource', 'promptSourceName',
		'entityType', {
			name: 'order',
			type: 'float'
		}, 'parserType', 'sampleOutput', {
			name: 'repeat',
			type: 'bool'
		}, 'description', 'summary', 'detail', 'defaultAssess', 'defaultSeverity', 'continuation', 'assessorScript', 'parserScript', {
			name: 'parserAutoGenEnabled',
			type: 'bool'
		}, {
			name: 'assessorAutoGenEnabled',
			type: 'bool'
		}, {
			name: 'expectTimeout',
			type: 'int'
		}, {
			name: 'timeout',
			type: 'int'
		}, 'inputfile',
	],

	//Theses fields are used for HTTP task.
	method: "",
	commandUrl: "",
	headers: [],
	body: "",
	connectionType: "",

	defaultData: {
		name: 'New Task',
		entityType: 'task',
		queueName: 'RSREMOTE',
		queueNameSource: 'CONSTANT',
		promptSourceName: '',
		promptSource: 'CONSTANT',
		defaultAssess: 'GOOD',
		defaultSeverity: 'GOOD',
		parserType: 'text',
		description: '',
		inputfile: '',
		summary: '${RAW}',
		detail: '${RAW}',
		continuation: 'always',
		timeout: 300,
		expectTimeout: 10,
		assessorAutoGenEnabled: true,
		parserAutoGenEnabled: true
	},
	module: '',
	general: null,
	getData: function() {
		var task = this.asObject();
		var connectionType = '';

		if (this.connectionType) {
			connectionType = this.connectionType.toUpperCase();
		}

		Ext.apply(task, {
			encodedCommand: this.encodedCommand,
			conditions: this.conditions,
			severities: this.severities,
			variables: this.variables,
			parserConfig: this.parserConfig,
			sampleOutput: this.sampleOutput,
			method: this.method,
			commandUrl: this.commandUrl,
			headers: this.headers,
			body: this.body,
			connectionType: connectionType
		});
		return task;
	},

	generateCode: function(nextAction, myData, codeType) {
		this.getRoot().parentVM.set('reqCount', this.getRoot().parentVM.reqCount + 1);
		this.ajax({
			url: '/resolve/service/resolutionbuilder/generateCode',
			jsonData: {
				codeType: codeType || 'both',
				task: myData
			},
			success: function(resp) {
				this.getRoot().parentVM.set('reqCount', this.getRoot().parentVM.reqCount - 1);
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('parseCodeError', respData.message));
					return;
				}
				myData.assessorScript = respData.data;
				nextAction();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		}, this);
	},
	beforeSave: function(nextAction, myData) {
		this.generateCode(nextAction, myData);
	},
	setData: function(data) {
		Ext.Object.each(data, function(k, v) {
			if (data[k] == 'UNDEFINED')
				delete data[k];
		});
		if (this.loadData)
			this.loadData(Ext.applyIf(data, this.defaultData));
		if (!data.promptSource)
			this.set('promptSource', 'CONSTANT');
		this.set('conditions', data.conditions || []);
		this.set('severities', data.severities || []);
		this.set('parserConfig', data.parserConfig);
		this.set('method', data.method || "");
		this.set('commandUrl', data.commandUrl || "");
		this.set('headers', data.headers || []);
		this.set('body', data.body || "");
		this.set('connectionType', this.parentVM.type || this.parentVM.connectionType);
	},
	variableExtractedFromParserConfig$: function() {
		var vars = [];
		var config = this.parserConfig ? Ext.decode(this.parserConfig): [];
		Ext.each(config.markups, function(markup) {
			if (markup.action != 'capture')
				return;
			vars.push({
				name: markup.variable,
				source: markup.source,
				task: this.name,
				flow: markup.flow,
				wsdata: markup.wsdata,
				color: markup.inlineStyle.backgroundColor
			});
		}, this);
		Ext.each(config.tblVariables, function(variable) {
			vars.push({
				name: variable.name,
				task: this.name,
				flow: variable.flow,
				wsdata: variable.wsdata,
				color: variable.color
			});
		}, this);
		return vars;
	},
	init: function() {
		this.set('general', this.getRoot().parentVM.general);
		this.general.on('nameChanged', function() {
			this.set('module', this.general.namespace + '.' + this.general.name);
		}, this);
		this.general.on('namespaceChanged', function() {
			this.set('module', this.general.namespace + '.' + this.general.name);
		}, this);
		this.set('module', this.general.namespace + '.' + this.general.name);
	},
	fullName$: function() {
		return this.name;
	},
	margin: '10 30 10 30',
	remove: function() {
		this.deleteEntry(function() {
			this.removeFromParent();
		}, this)
	}
});