glu.defModel('RS.wiki.resolutionbuilder.DragWatcherContainer', {
	//need a new Mixin to do this 
	accept: function(data) {
		var self = false;
		this.entries.forEach(function(watcher, idx) {
			if (watcher.position == 'N/A')
				return;
			if ((watcher.position == 'above' || watcher.position == 'below') && watcher == data.panel._vm)
				self = true;
		});
		if (self)
			return;
		var idxBelowTheDraggedTask = null;
		var idxAboveTheDraggedTask = null;
		data.panel._vm.removeFromParent(true);
		data.panel._vm.parentVM = this;
		this.entries.forEach(function(watcher, idx) {
			if (watcher.position == 'N/A')
				return;
			if (watcher.position == 'above')
				idxAboveTheDraggedTask = idx;
			if (watcher.position == 'below')
				idxBelowTheDraggedTask = idx;
		});
		var pos = null;
		if (!Ext.isEmpty(idxAboveTheDraggedTask)) {
			var taskAboveTheDraggedTask = this.entries.getAt(idxAboveTheDraggedTask);
			if (idxAboveTheDraggedTask == this.entries.length - 1)
				data.panel._vm.set('order', taskAboveTheDraggedTask.order + 1);
			else {
				var theNextTask = this.entries.getAt(idxAboveTheDraggedTask + 1);
				data.panel._vm.set('order', (taskAboveTheDraggedTask.order + theNextTask.order) / 2);
			}
			pos = idxAboveTheDraggedTask + 1;
		} else if (!Ext.isEmpty(idxBelowTheDraggedTask)) {
			var theTaskBelowTheDraggedTask = this.entries.getAt(idxBelowTheDraggedTask);
			if (idxBelowTheDraggedTask == 0)
				data.panel._vm.set('order', theTaskBelowTheDraggedTask.order - 1);
			else {
				var thePreviousTask = this.entries.getAt(idxBelowTheDraggedTask - 1);
				data.panel._vm.set('order', (thePreviousTask.order + theTaskBelowTheDraggedTask.order) / 2);
			}
			pos = idxBelowTheDraggedTask;
		} else {
			data.panel._vm.set('order', this.entries.length == 0 ? 0 : this.entries.getAt(this.entries.length - 1).order + 1);
			pos = this.entries.length;
		}
		this.entries.insert(pos, data.panel._vm);
		data.panel._vm.saveEntry();
	}
});
glu.defModel('RS.wiki.resolutionbuilder.DragWatcher', {
	position: '',
	taskEnterUpperHalf: function() {
		this.set('position', 'below');
	},
	taskEnterLowerHalf: function() {
		this.set('position', 'above');
	},
	taskLeaveWatcher: function() {
		this.set('position', 'N/A');
	}
})