glu.defModel('RS.wiki.Template', {
	id: '',
	state: '',
	title$: function() {
		return this.localize('title') + (this.uname ? ' - ' + this.uname : '')
	},
	uname: '',
	uwikiDocName: '',
	uwikiDocNameIsVisible$: function() {
		return this.uwikiDocName;
	},
	uformName: '',
	uformNameIsVisible$: function() {
		return this.uformName;
	},
	udescription: '',
	uisDefaultRole: false,

	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrganizationName: '',

	defaultsData: {
		id: '',
		uname: '',
		uwikiDocName: '',
		uformName: '',
		udescription: '',

		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrganizationName: ''
	},

	accessRights: {
		mtype: 'RS.common.AccessRights',
		rightsShown: ['read', 'write', 'execute']
	},

	fields: ['id', 'uname', 'uwikiDocName', 'uformName', 'udescription', 'uisDefaultRole'].concat(RS.common.grid.getSysFields()),

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.resetForm();
		this.set('id', params && params.id != null ? params.id : '');
		this.set('state', 'ready');
		if (this.id != '') {
			this.loadTemplate();
			return;
		}
	},

	init: function() {
		Ext.each(clientVM.user.roles, function(r) {
			if (r == 'admin')
				this.accessRights.rightsShown.push('admin');
		}, this);
	},

	loadTemplate: function() {
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/wikitemplate/get',
			params: {
				id: this.id
			},
			scope: this,
			success: function(resp) {
				this.handleLoadTemplateSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	jumpToWiki: function() {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			params: {
				name: this.uwikiDocName
			}
		})
	},

	jumpToForm: function() {
		clientVM.handleNavigation({
			modelName: 'RS.formbuilder.Main',
			params: {
				name: this.uformName
			}
		})
	},

	handleLoadTemplateSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('GetTemplateErrMsg') + '[' + respData.message + ']');
			return;
		}

		this.setData(respData.data);
	},

	save: function(exitAfterSave) {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveTemplate, this, [exitAfterSave])
		this.task.delay(500)
		this.set('state', 'wait')
	},
	saveIsEnabled$: function() {
		return this.state != 'wait' && this.isValid;
	},
	saveAndExit: function() {
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},

	saveTemplate: function(exitAfterSave) {
		var data = {
			id: this.id,
			UName: this.uname,
			UWikiDocName: this.uwikiDocName,
			UFormName: this.uformName,
			UDescription: this.udescription,
			UIsDefaultRole: this.accessRights.defaultRoles
		};

		data.accessRights = this.accessRights.toCVS();

		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/wikitemplate/save',
			jsonData: data,
			scope: this,
			success: function(resp) {
				if (this.handleSaveSucResp(resp) && exitAfterSave === true)
					clientVM.handleNavigation({
						modelName: 'RS.wiki.Templates'
					});
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Templates'
		});
	},

	addAccessRights: function(rights) {
		Ext.each(rights, function(accessRight) {
			this.accessRights.add(Ext.applyIf(accessRight.data, {
				view: true,
				edit: true,
				admin: true
			}));
		}, this);
	},

	removeAccessRight: function() {
		this.accessRights.remove(this.rightsSelections);
	},

	handleSaveSucResp: function(resp) {
		var respData = null;

		try {
			respData = RS.common.parsePayload(resp);
		} catch (e) {
			clientVM.displayError(this.localize('invalidJSON'));
			return false;
		}

		if (!respData.success) {
			clientVM.displayError(this.localize('SaveErr') + '[' + respData.message + ']');
			return false;
		}
		clientVM.displaySuccess(this.localize('SaveSuc'));

		this.setData(respData.data);
		return true;
	},

	resetForm: function() {
		this.accessRights.set('defaultRoles', false);
		this.loadData(this.defaultsData);
		this.accessRights.clearGrid();
		this.set('isPristine', true);
	},

	refresh: function() {
		if (this.id != null && this.id != '')
			this.loadTemplate();
	},

	setData: function(data) {
		this.loadData(data);
		this.accessRights.loadRights(data.accessRights);
		this.accessRights.set('defaultRoles', data.uisDefaultRole);
	},

	unameIsValid$: function() {
		var rexp1 = /^[A-Za-z0-9_ ]+[\\.]([A-Za-z0-9_ ]+[\\.])*[A-Za-z0-9_ ]+$/;
		var rexp2 = /^[A-Za-z0-9_ ]+$/;
		return this.uname != null &&
			(rexp1.test(this.uname) || rexp2.test(this.uname)) &&
			this.uname != '' ? true : this.localize('invalidName');
	}
});