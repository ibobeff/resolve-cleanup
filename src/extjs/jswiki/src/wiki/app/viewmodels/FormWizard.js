glu.defModel('RS.wiki.FormWizard', {
	name: '',
	formHeight: 200,
	formWidth: 600,
	properName$: function() {
		return this.name.toUpperCase()
	},
	nameIsValid$: function() {
		// if(this.name.length > 20) return this.localize('formNameTooLongInvalidText');
		if (/[^A-Z|_|0-9]/g.test(this.properName)) return this.localize('formNameInvalidText');
		return this.existingFormNames.findRecord('name', this.properName, 0, false, false, true) ? this.localize('formNameExist', this.properName) : true;
	},

	create: function() {
		this.parentVM.set('name', this.properName)
		clientVM.handleNavigation({
			modelName: 'RS.formbuilder.Main',
			params: {
				name: this.parentVM.name,
				create: true
			},
			target: '_blank'
		})
		this.doClose()
	},
	createIsEnabled$: function() {
		return this.isValid
	},
	cancel: function() {
		this.doClose()
	},

	existingFormNames: {
		mtype: 'store',
		fields: ['name'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getformnames',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	init: function() {
		this.existingFormNames.load();
		this.set('formHeight', Math.max(clientVM.getWindowHeight() * 4 / 5, 200));
		this.set('formWidth', Math.max(clientVM.getWindowWidth() * 4 / 5, 600));	
	}
})
