glu.defModel('RS.wiki.AccessRights', {
	rights: null,
	accessRights: {
		mtype: 'RS.common.AccessRights',
		rightsShown: ['read', 'write', 'execute'],
		local: true
	},
	defaultRolesIsVisible: true,

	dumper: null,

	init: function() {
		this.accessRights.set('defaultRolesText', this.localize('defaultRolesText'));
		var cv = this.clientVM || this.parentVM.clientVM;
		Ext.each(cv.user.roles, function(r) {
			if (r == 'admin')
				this.accessRights.rightsShown.push('admin');
		}, this);
		this.accessRights.init();
		this.accessRights.loadRights(this.rights);
		this.accessRights.set('defaultRolesIsVisible', this.defaultRolesIsVisible);
		this.accessRights.set('defaultRoles', false);
	},

	setRights: function() {
		var cvs = this.accessRights.toCVS();
		if (!cvs['uadminAccess'])
			cvs['uadminAccess'] = '';
		if (this.dumper)
			this.dumper(cvs);
		else
			this.parentVM.doConfigRoles(cvs);
		this.close();
	},

	close: function() {
		this.doClose();
	}
});