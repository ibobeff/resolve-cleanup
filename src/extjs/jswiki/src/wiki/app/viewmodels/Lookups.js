glu.defModel('RS.wiki.Lookups', {

	mock: false,

	activate: function() {
		clientVM.setWindowTitle(this.localize('windowTitle'))
	},

	displayName: '~~wikiLookups~~',
	stateId: 'wikilookups',
	state: '',
	store: {
		mtype: 'store',
		fields: ['id', 'uregex', {
			name: 'uorder',
			type: 'float'
		}, 'udescription', 'uwiki'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'uorder',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wikilookup/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	columns: null,

	lookupsSelections: [],

	init: function() {

		if (this.mock) this.backend = RS.wiki.createMockBackend(true)

		this.set('displayName', this.localize('WikiLookups'));

		this.set('columns', [{
			header: '~~uorder~~',
			dataIndex: 'uorder',
			filterable: true,
			sortable: true,
			width: 90,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~uregex~~',
			dataIndex: 'uregex',
			filterable: true,
			sortable: true,
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~uwiki~~',
			dataIndex: 'uwiki',
			filterable: true,
			sortable: true,
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~udescription~~',
			dataIndex: 'udescription',
			filterable: false,
			sortable: false,
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		}].concat(RS.common.grid.getSysColumns()));
		this.loadLookups();
	},

	loadLookups: function() {
		this.set('state', 'wait');
		this.store.load({
			scope: this,
			callback: function(rec, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('ListLookupsErrMsg'));
					return;
				}
				this.set('state', 'ready');
			}
		});
	},

	createLookup: function() {
		this.open({
			mtype: 'RS.wiki.Lookup'
		});
	},

	editLookup: function(id) {
		this.open({
			mtype: 'RS.wiki.Lookup',
			id: id
		});
	},

	deleteLookups: function() {
		this.message({
			title: this.localize('DeleteTitle'),
			msg: this.localize(this.lookupsSelections.length > 1 ? 'DeletesMsg' : 'DeleteMsg', {
				num: this.lookupsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirmDelete'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.sendDeleteReq
		});
	},

	sendDeleteReq: function(btn) {
		if (btn == 'no') {
			this.set('state', 'itemSelected');
			return;
		}

		this.set('state', 'wait');

		var ids = [];

		Ext.each(this.lookupsSelections, function(r) {
			ids.push(r.get('id'));
		});

		this.ajax({
			url: '/resolve/service/wikilookup/delete',
			jsonData: ids,
			scope: this,
			success: function(resp) {
				this.handleDeleteSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	handleDeleteSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('DeleteErrMsg') + '[' + resp.status + ']', respData.message);
		} else
			clientVM.displaySuccess(this.localize('DeleteSucMsg'));

		this.loadLookups();
	},

	createLookupIsEnabled$: function() {
		return this.state != 'wait';
	},

	deleteLookupsIsEnabled$: function() {
		return this.state == 'itemSelected';
	},

	when_lookupsSelections_changed: {
		on: ['lookupsSelectionsChanged'],
		action: function() {
			if (this.lookupsSelections.length > 0)
				this.set('state', 'itemSelected');
			else
				this.set('state', 'ready');
		}
	}
})