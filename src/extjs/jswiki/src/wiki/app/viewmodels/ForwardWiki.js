glu.defModel('RS.wiki.ForwardWiki', {

	isMove: false,

	windowTitle$: function() {
		return this.localize('forward')
	},
	id: '',
	name: '',
	namespace: '',

	forward: function() {
		this.ajax({
			url: '/resolve/service/wikiadmin/forward',
			params: {
				ids: this.id,
				namespace: this.namespace,
				filename: this.name
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.localize('wikiForwarded'))
					this.doClose()
				} else
					clientVM.displayError(reponse.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	cancel: function() {
		this.doClose()
	}
})