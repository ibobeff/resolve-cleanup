glu.defModel('RS.wiki.automation.DecisionTreeParameters', {
	dumper: function() {},
	getParamsFromStore: function() {
		var params = {};
		this.store.each(function(r) {
			if (r.get('source') == 'DEFAULT')
				return;
			params[r.get('name')] = r.get('source') == 'CONSTANT' ? r.get('value') : Ext.String.format('${0}\{{1}\}', r.get('source'), r.get('value'));
		}, this)
		return params;
	},
	store: {
		mtype: 'store',
		groupField: 'isDefault',
		fields: ['name', 'source', 'value', 'order', {
			name: 'encrypt',
			type: 'bool'
		}, 'description', {
			name: 'isDirty',
			type: 'bool'
		}, {
			name: 'isDefault',
			type: 'bool'
		}, 'defaultValue']
	},
	paramSource: {
		mtype: 'store',
		fields: [{
			name: 'source',
			convert: function(v, r) {
				return r.raw
			}
		}],
		data: ['DEFAULT', 'CONSTANT', 'OUTPUT', 'FLOW', 'WSDATA', 'PARAM', 'PROPERTY']
	},
	title: '',
	taskName: '',
	extractor: /^\$([^\{\}]+)\{([^\{\}]+)*\}$/,
	modelParams: {},
	defaultParams: {},
	invalidJson: '',
	init: function() {
		this.invalidJson = this.localize('invalidJson');
		if (this.pressInput)
			this.paramSource.clearFilter(true);
		else
			this.paramSource.filter({
				filterFn: function(item) {
					return ['CONSTANT', 'PROPERTY', 'OUTPUT'].indexOf(item.get('source')) == -1
				}
			});
	},
	loadParams: function() {
		this.store.removeAll();
		Ext.Object.each(this.modelParams, function(k, v) {
			var src = 'CONSTANT';
			var value = v;
			var isConst = !this.extractor.test(v);
			if (!isConst) {
				var m = this.extractor.exec(v);
				var indexParamSrc = this.paramSource.find('source', m[1], 0, false, false, true);
				if ( indexParamSrc == -1)
					src = 'CONSTANT';
				else{
					src = this.paramSource.getAt(indexParamSrc).raw;
					value = m[2];
				}
			}
			this.store.add({
				name: k,
				source: src,
				value: value,
				order: 0,
				isDirty: false,
				isDefault: false,
				defaultValue: null
			})
		}, this);
		Ext.each(this.defaultParams, function(v) {
			var recordIdx = this.store.find('name', v.uname, 0, false, false, true);
			if (recordIdx != -1) {
				var record = this.store.getAt(recordIdx);
				record.set('description', v.udescription);
				record.set('order', v.uorder);
				record.set('isDirty', true);
				record.set('isDefault', true);
				record.set('encrypt', v.uencrypt || false);
				record.set('protected', v.uprotected || false);
				record.set('defaultValue', v.udefaultValue || '');
			} else {
				this.store.add({
					name: v.uname,
					source: 'DEFAULT',
					encrypt: v.uencrypt || false,
					protected: v.uprotected || false,
					value: v.udefaultValue || '',
					description: v.udescription || '',
					order: v.uorder,
					isDirty: false,
					isDefault: true,
					defaultValue: v.udefaultValue || ''
				});
			}
		}, this);
		
		this.store.sort([{
			property: 'isDefault',
			direction: 'DESC'
		}, {
			property: 'order',
			direction: 'ASC'
		}]);

	},
	parametersSelections: [],
	addParam: function() {
		var max = 0;
		var reg = /^NewParam(\d*)$/;
		var newParam = 'NewParam';

		this.store.each(function(param) {
			if (!reg.test(param.get('name')))
				return;
			var m = reg.exec(param.get('name'));
			var idx = parseInt(m[1]);
			if (idx > max)
				max = idx;
		}, this);
		newParam += ((max + 1) || '');

		this.open({
			mtype: 'RS.wiki.automation.ParameterEdit',
			id: null,
			name: newParam,
			source: (this.pressInput ? 'CONSTANT' : 'FLOW'),
			value: '',
			description: '',
			isDefaultParam: false,
			defaultValue: '',
		});
	},
	addParamIsEnabled$: function() {
		return this.pressInput;
	},

	addNewParam: function(newParam, newSource, newValue) {
		this.store.add({
			name: newParam,
			source: newSource,
			value: newValue
		});
	},
	editParams: function() {
		this.open({
			mtype: 'RS.wiki.automation.ParameterEdit',
			id: this.parametersSelections[0].internalId,
			name: this.parametersSelections[0].get('name'),
			source: this.parametersSelections[0].get('source'),
			value: this.parametersSelections[0].get('value'),
			encrypt: this.parametersSelections[0].get('encrypt'),
			description: this.parametersSelections[0].get('description'),
			isDefaultParam: this.parametersSelections[0].get('isDefault'),
			defaultValue: this.parametersSelections[0].get('defaultValue'),
		});
	},
	editParamsIsEnabled$: function() {
		return this.parametersSelections.length == 1;
	},
	removeParams: function() {
		this.store.remove(this.parametersSelections);
	},
	removeParamsIsEnabled$: function() {
		var canRemove = true;
		for (var i=0, l=this.parametersSelections.length; i<l; i++) {
			var param = this.parametersSelections[i];
			if (param.get('isDefault')) {
				canRemove = false;
				break;
			}
		}

		return canRemove && this.parametersSelections.length > 0;
	},
	resetParams: function(paramName) {
		var recordIdx = this.store.find('name', paramName, 0, false, false, true);
		if (recordIdx != -1) {
			for (var i=0; i<this.defaultParams.length; i++) {
				var param = this.defaultParams[i];
				if (param.uname === paramName) {
					this.store.insert(recordIdx, {
						name: param.uname,
						source: 'DEFAULT',
						encrypt: param.uencrypt || false,
						protected: param.uprotected || false,
						value: param.udefaultValue || '',
						description: param.udescription || '',
						order: param.uorder,
						isDirty: false,
						isDefault: true,
						defaultValue: param.udefaultValue || ''
					});
					break;
				}
			}
			this.store.removeAt(recordIdx+1);
		}
	},
	doDump: function() {
		this.dumper();
		this.cancel();
	},
	showGrid: true,
	fromJson: function() {
		this.set('showGrid', false);
	},
	fromJsonIsVisible$: function() {
		return this.showGrid;
	},
	json: '',
	jsonIsValid$: function() {
		var valid = false;

		if (this.json && this.json.length > 0) {
			try {
				Ext.JSON.decode(this.json);
				valid = true;
			} catch (e) {
				valid = false;
			}
		}

		return valid || this.invalidJson;
	},
	parseJson: function() {
		var arr = Ext.decode(this.json);
		Ext.each(arr, function(item) {
			this.store.add({
				name: item.name,
				source: item.source,
				value: item.value
			})
		}, this)
	},
	parseJsonIsVisible$: function() {
		return !this.showGrid;
	},
	parseJsonIsEnabled$: function() {
		return this.jsonIsValid === true;
	},
	cancel: function() {
		if (!this.showGrid) {
			this.set('showGrid', true);
			return;
		}
		this.doClose();
	},
	pressInput: true,
	showFakeTab: false,
	fakeInput: function() {
		this.parentVM.fireEvent('toggleFakeTab', 'input');
	},
	fakeOutput: function() {
		this.parentVM.fireEvent('toggleFakeTab', 'output');
	},
	sourceTitle$: function() {
		return this.localize(this.pressInput ? 'source' : 'destination');
	},
	sourceValueTitle$: function() {
		return this.localize(this.pressInput ? 'value' : 'variable');
	}
})
