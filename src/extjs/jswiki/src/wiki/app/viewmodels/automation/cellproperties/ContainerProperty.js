glu.defModel('RS.wiki.automation.NoProperty', {});
glu.defModel('RS.wiki.automation.RootProperty', {});
glu.defModel('RS.wiki.automation.ContainerProperty', {
	mixins: ['LabelAware'],
	cell: null,
	loading: false,
	resetProperty: function() {
		this.set('loading', true);
		this.set('cell', null);
		this.set('label', '');
		this.set('loading', false);
	},
	populate: function(cell) {
		this.resetProperty();
		this.set('loading', true);
		this.set('cell', cell);
		this.set('label', cell.getAttribute('label'));
		this.set('loading', false);
	}
});