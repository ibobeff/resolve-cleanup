glu.defModel('RS.wiki.automation.DocumentMenu', {
	mixins: ['ElementMenu'],
	init: function() {
		function parseCDATA(cdata) {
			if (!cdata)
				return [];
			try {
				return Ext.decode(cdata);
			} catch (e) {
				return [];
			}
		}
		var me = this;

		function extract(node) {
			Ext.each(node.childNodes, function(child) {
				if (child.tagName) {
					var text = child.textContent || child.innerText || '';
					if (child.tagName.toLowerCase() == 'inputs')
						me.set('modelInputs', parseCDATA(text));
					if (child.tagName.toLowerCase() == 'outputs')
						me.set('modelOutputs', parseCDATA(text));
					if (child.tagName.toLowerCase() == 'name')
						me.set('taskName', text);
				}
				extract(child);
			})
		}
		extract(this.cellGetter().getValue());
	},
	searchDecisionTree: function() {
		var me = this;
		this.open({
			mtype: 'RS.decisiontree.DocumentPicker',
			dumper: {
				dump: function(selections) {
					if (selections.length > 0) {
						var selection = selections[0]
						me.parentVM.fireEvent('selectDocument', selection.get('ufullname'));
					}
				}
			}
		})
	},
	editProperties: function() {
		var me = this;
		this.open({
			elementType: 'decitionTree',
			mtype: 'RS.wiki.automation.ElementProperty',
			nodeId: this.cellGetter().id,
			elementName: this.taskName
		});
	}
});