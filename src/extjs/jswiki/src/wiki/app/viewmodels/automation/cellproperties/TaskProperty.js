glu.defModel('RS.wiki.automation.TaskProperty', {
	mixins: ['MergeAware', 'XmlAware', 'LabelAware', 'VersionAware', 'ATBUtil', 'ActionTaskValidation'],
	
	//Wizard
	parameterWizard : {
		mtype : 'ParameterWizard',
		taskIsPartOfCreation : false
	},
	showOptionIsTable : true,
	showOptionStateMap : {},
	taskHasNoWizard : true,
	when_show_option_changed : {
		on : ['showOptionIsTableChanged'],
		action : function(newVal){
			this.showOptionStateMap[this.cell.id] = newVal;
			if(!this.showOptionIsTable)			
				this.parameterWizard.loadWizardConfig(this.cell.id, this.getInputParams());		
			else
				this.parameterWizard.saveCurrentWizardConfig(this.cell.id);			
		}
	},	
	activeOption$ : function(){
		return this.showOptionIsTable ? 0 : 1;
	},
	getInputParams : function(){
		var defaultIncluded = true;
		return this.inputs.getParamsFromStore(defaultIncluded);
	},
	updateValueForProperty : function(propName, value){
		this.inputs.updateValueForProperty(propName, value);
	},	
	isTaskCreatedFromWizard : function(wizardType){
		return ['http'/*,'local', 'ssh', 'telnet'*/].indexOf(wizardType) != -1;
	},
	versionHistoryStore: {
		mtype: 'store',
		fields: ['documentName', {
			name: 'version',
			convert: function(v, r) {
				return r.raw.version || r.raw.revision;
			}
		}, 'isStable', 'referenced', 'comment', 'createdBy', {
			name: 'sysCreatedOn',
			type: 'date',
			dateFormat: 'time',
			format: 'time'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/actiontask/getHistory',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	taskStore: {
		mtype: 'store',
		fields: ['ufullName'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/actiontask/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			},
			extraParams: {
				delim: '/',
				id: 'root_menu_id'
			}
		}
	},
	activeTab: 0,
	isPartOfDT : false,	
	reqCount: 0,
	taskName: '',
	taskNameIsValid: true,
	newTask: true,
	when_taskName_change: {
		on: ['taskNameChanged'],
		action: function() {
			if (this.taskName) {
				this.validateTaskName(this.taskName, true);
			}
		}
	},

	configureVisible$: function() {
		return (this.activeTab == 0);
	},

	updateTaskProperty: function(task, taskNameChanged) {		
		var result = this.parse(this.cell.getValue());
		var modelInputs = result.modelInputs || {};
		var modelOutputs = result.modelOutputs || {};
		if (result.taskName != this.taskName) {
			modelInputs = {};
			modelOutputs = {};
		}
		var params = task.resolveActionInvoc.resolveActionParameters;
		var defaultInputs = this.filterParams('input', params)
		this.inputs.set('defaultParams',defaultInputs );
		this.inputs.set('modelParams', modelInputs);
		this.inputs.loadParams();

		this.outputs.set('defaultParams', this.filterParams('output', params));
		this.outputs.set('modelParams', modelOutputs);
		this.outputs.loadParams();	
		this.set('task', task);

		if (taskNameChanged) {
			this.getTask(task.ufullName, function() {
				this.rootVM.automation.updateActiveActionTaskEditPermission(this.actiontaskbuilder);
			});
		}
		else {
			this.rootVM.automation.updateActiveActionTaskEditPermission(this.actiontaskbuilder);
			if (this.actiontaskbuilder) {
				this.collapseAll();
			}
		}

		//Handle show option
		if(!this.isTaskCreatedFromWizard(task.wizardType)){	
			this.set('taskHasNoWizard', true);	
			this.set('showOptionIsTable', true);
		}	
		else {
			this.set('taskHasNoWizard', false);			
			this.parameterWizard.updateWizardType(task.wizardType);		
			if(!this.showOptionStateMap.hasOwnProperty(this.cell.id))			
				this.showOptionStateMap[this.cell.id] = false;			
			this.set('showOptionIsTable', false);
			this.parameterWizard.loadWizardConfig(this.cell.id, this.getInputParams());			
		}

		//Populate any output params from previous task
		this.getPreviousTasksOutputsOnly();		
	},
	viewactiontaskSelected: function() {
		this.rootVM.automation.updateActiveActionTaskEditPermission(this.actiontaskbuilder, true);
	},
	oldTaskName: '',
	oldTaskLabel: '',
	savedTaskRecord: null,
	revertTaskNameInProgress: false,
	taskNameChanged: false,
	validateTaskName: function(taskName, taskNameChanged, isNewTask) {
		var taskRecord = this.taskStore.findRecord('ufullName', taskName, 0, false, false, true)
		if (!taskRecord) {
			this.set('taskNameIsValid', this.localize('invalidTaskName'));
		} else if (this.viewmodelName != 'NewTask') {
			if (this.revertTaskNameInProgress) {
				// if there is a revert taskName change in progress, ignore checkActionTaskParamDependency() and just continue doValidateTaskName()
				this.doValidateTaskName(taskRecord, false, function() {
					this.revertTaskNameInProgress = false;
					this.taskNameChanged = false;
				}.bind(this), null, isNewTask);
			} else if (taskNameChanged) {
				this.parentVM.splitEdgeFlag = false;
				this.oldTaskLabel = this.label;
				this.oldTaskName = this.actiontaskbuilder.ufullName;
				this.savedTaskRecord = taskRecord;
				this.checkActionTaskParamDependency(0);
			}
		} else {
			this.doValidateTaskName(taskRecord, taskNameChanged, null, null, isNewTask);
		}
	},
	resumeValidateTaskName: function() {
		if (this.savedTaskRecord) {
			this.doValidateTaskName(this.savedTaskRecord, true, function() {
				this.savedTaskRecord = null;
				this.oldTaskLabel = '';
				this.oldTaskName = '';
				this.taskNameChanged = false;
			}.bind(this));
		}
	},
	cancelValidateTaskName: function() {
		this.savedTaskRecord = null;
		this.oldTaskLabel = '';
		this.oldTaskName = '';
		this.taskNameChanged = false;
	},
	doValidateTaskName: function(taskRecord, taskNameChanged, onComplete, labelAlreadySet, isNewTask) {
		this.inputs.store.removeAll();
		this.outputs.store.removeAll();
		this.set('taskNameIsValid', true);
		var desc = this.cell.getValue().getAttribute('description');
		var splits = desc.split('?');
		var task = taskRecord.raw;

		this.set('id', task.id);

		if (!isNewTask) {
			// when taskname changed, default to use "latest" version
			if (taskNameChanged) {
				this.resetVersion();
			}
			/* TODO - support Version Control
			this.versionHistoryStore.load({
				params: {
					id: task.id,
					docFullName: task.ufullName
				}
			});
			*/

			if (!labelAlreadySet) {
				this.set('label', task.uname);
				this.parentVM.fireEvent('celllabelchanged', this.cell, this.label || '');
			}
			this.set('newTask', false);
			this.parentVM.fireEvent('selectTask', task.uname, task.unamespace, function(valueXml) {
				//handle double encode issue
				var paramNode = null;
				var isCurrentTask = true;
	
				function findAndManipulate(node) {
					if (node.tagName) {
						var tagName = node.tagName.toLowerCase();
	
						if (tagName == 'params') {
							paramNode = node;
						} else if (tagName == 'name' && node.textContent && task.ufullName != node.textContent.trim()) {
							isCurrentTask = false;
						}
					}
	
					Ext.each(node.childNodes, function(child) {
						findAndManipulate(child);
					})
				}
				findAndManipulate(valueXml);
	
				if (isCurrentTask && paramNode) {//we respect current node
					return;
				}
	
				var xmlDoc = mxUtils.createXmlDocument();
				var oldDblEncodedDescription = valueXml.getAttribute('description');
				var splits = oldDblEncodedDescription.split('?');
				var isResolveTask = ['start#resolve', 'end#resolve', 'event#resolve'].indexOf(task.uname + '#' + task.unamespace) != -1;
	
				if (isCurrentTask && !(splits.length == 2 && splits[1]) && !isResolveTask) {
					return;
				}
	
				var oldInputs = {};
				var oldOutputs = {};
				var theBizarrePart = decodeURIComponent(splits[isResolveTask ? 0 : 1]).split('&');
				Ext.each(theBizarrePart, function(token) {
					var cleaned = decodeURIComponent(token);
					var splits = [];
					if (cleaned.indexOf(':=') != -1) {
						splits = cleaned.split(':=');
						oldOutputs[splits[0]] = splits[1];
						return;
					}
	
					var regexPattern = /([^=]*)=(.*)/;
					var matches = regexPattern.exec(cleaned);
					if (matches) {
						var paramName = matches[1];
						var paramValue = matches[2];
						oldInputs[paramName] = paramValue;
					}
				});
				//if we see a double encode stuff.. it definitely comes from the older version and cdata should not be there
				var paramNode = xmlDoc.createElement('params');
				var inputCDATA = xmlDoc.createCDATASection(Ext.encode(oldInputs));
				var outputCDATA = xmlDoc.createCDATASection(Ext.encode(oldOutputs));
				var inputNode = xmlDoc.createElement('inputs');
				var outputNode = xmlDoc.createElement('outputs');
				inputNode.appendChild(inputCDATA);
				outputNode.appendChild(outputCDATA);
				paramNode.appendChild(inputNode);
				paramNode.appendChild(outputNode);
				valueXml.appendChild(paramNode);
				this.rootVM.automation.setXmlDirty();
				return valueXml
			}.bind(this), this.cell);
		}
		this.updateTaskProperty(task, taskNameChanged);

		if (Ext.isFunction(onComplete)) {
			onComplete();
		}
	},
	inputs: {
		mtype: 'Parameters',
		showFakeTab: true
	},
	outputs: {
		mtype: 'Parameters',
		pressInput: false,
		showFakeTab: true
	},
	activeItem: 0,
	resetProperty: function() {
		this.set('loading', true);
		this.set('cell', null);
		this.set('reqCount', 0);
		this.set('task', null);
		this.set('label', '');
		this.set('taskNameIsValid', true);
		this.inputTab();
		this.inputs.store.removeAll();
		this.outputs.store.removeAll();
		this.set('loading', false);
	},
	inputTab: function() {
		this.set('activeItem', 0);
	},
	inputTabIsPressed$: function() {
		return this.activeItem == 0;
	},
	outputTab: function() {
		this.set('activeItem', 1);
	},
	outputTabIsPressed$: function() {
		return !this.inputTabIsPressed;
	},
	filterParams: function(type, params) {
		var filtered = [];
		Ext.each(params, function(param) {
			if ((param.utype || 'input').toLowerCase() == type)
				filtered.push(param);
		});
		return filtered;
	},
	taskStoreOnLoad: function() {
		this.validateTaskName(this.taskName, this.taskNameChanged);
		this.set('reqCount', this.reqCount - 1);
	},
	initMixin : function(){
		this.inputs.store.on('datachanged', function() {
			if (this.reqCount > 0)
				return;
			this.pushToCell();
		}, this);
		this.inputs.store.on('update', function(store, record, operation, modifiedFieldNames, eOpts) {
			if (this.reqCount > 0)
				return;
			if (record.get('source').toLowerCase() == 'default' && modifiedFieldNames.indexOf('source') == -1)
				record.set('source', 'CONSTANT');
			this.pushToCell();
		}, this);
		this.outputs.store.on('datachanged', function() {
			if (this.reqCount > 0)
				return;
			this.pushToCell();
		}, this);
		this.outputs.store.on('update', function(store, record, operation, modifiedFieldNames, eOpts) {
			if (this.reqCount > 0)
				return;
			if (record.get('source').toLowerCase() == 'default' && modifiedFieldNames.indexOf('source') == -1)
				record.set('source', 'CONSTANT');
			this.pushToCell();
		}, this);
		this.on('toggleFakeTab', function(tab) {
			this[tab + 　'Tab']();
		}, this);
		this.taskStore.on('beforeload', function(store, op) {
			op.params = op.params || {}
			Ext.apply(op.params, {
				filter: Ext.encode([{
					field: 'ufullName',
					condition: 'contains',
					type: 'auto',
					value: op.params.query || this.taskName
				}])
			})
			this.set('reqCount', this.reqCount + 1);
		}, this);
		this.taskStore.on('load', this.taskStoreOnLoad, this);
	},
	initVersionHistoryStore: function() {
		/* TODO - support Version Control 
		this.versionHistoryStore.on('beforeload', function(store, op) {
			this.set('reqCount', this.reqCount + 1);
		}, this);
		this.versionHistoryStore.on('load', function(store, recs) {
			if (recs && recs.length) {
				var version = this.cell.getAttribute('version');
				if (Ext.isNumeric(version)) {
					this.set('versionNumber', version);
				} else {
					this.set('versionNumber', recs[0].get('version'));
				}
				this.set('specificVersionIsDisabled', false); 
				// Determine if there is a Stable version then enable option
				var latestStableIsDisabled = true;
				for (var i = 0; i< recs.length; i++) {
					if (recs[i].get('isStable')) {
						latestStableIsDisabled = false;
						break;
					}
				}
				this.set('latestStableIsDisabled', latestStableIsDisabled);
				
				if (!this.latest && !this.latestStable && !this.specificVersion) {
					//// if user has admin role, default to use latestStable
					//if (!this.latestStableIsDisabled && Ext.Array.indexOf(clientVM.user.roles, 'admin') > -1) {
					//	this.set('latest', false);
					//	this.set('latestStable', true);
					//} else {
						this.set('latest', true);
						this.set('latestStable', false);
					//}
				}
			} else {
				this.set('versionNumber', '');
				this.set('latest', true);
				this.set('latestStable', false);
				this.set('specificVersion', false);
				this.set('latestStableIsDisabled', true);
				this.set('specificVersionIsDisabled', true);
			}
			this.set('reqCount', this.reqCount - 1);
		}, this);
		*/
	},
	init: function() {
		this.initMixin();
		this.outputStore.on('datachanged', function(store, records){
			var variableNames = store.collect('name');
			this.parameterWizard.saveVariableList('OUTPUT', variableNames)		
		}.bind(this))

		//Get Property variables
		var propertyStore = clientVM.getActionTaskPropertyStore();
		this.parameterWizard.saveVariableList('PROPERTY', propertyStore.collect('uname'));

		//Get Param variables
		var paramStore = this.rootVM.getParamStore();		
		paramStore.on('datachanged', function(){
			this.parameterWizard.saveVariableList('PARAM', paramStore.collect('name'));
		}.bind(this));	
		this.inputs.set('isPartOfDT', this.isPartOfDT);
		this.addRootVmEventListerners();
		this.initVersionHistoryStore();
	},

	beforeDestroyComponent: function() {
		this.removeRootVmEventListerners();
	},
	addRootVmEventListerners: function() {	
		this.rootVM.on('automationTabIsPressed', this.automationTabIsPressedHandler, this);
		this.rootVM.on('decisionTreeTabIsPressed', this.decisionTreeTabIsPressedHandler, this);
	},
	removeRootVmEventListerners: function() {	
		this.rootVM.removeListener('automationTabIsPressed', this.automationTabIsPressedHandler, this);
		this.rootVM.removeListener('decisionTreeTabIsPressed', this.decisionTreeTabIsPressedHandler, this);
	},	
	automationTabIsPressedHandler: function(flag) {
		 this.set('automationTabIsPressedFlag', flag);
	},
	decisionTreeTabIsPressedHandler: function(flag) {
		 this.set('decisionTreeTabIsPressedFlag', flag);
	},

	pushToCell: function() {
		if (!this.cell)
			return;
		var inputs = this.inputs.getParamsFromStore();
		this.toCDATA(inputs, 'inputs');
		var outputs = this.outputs.getParamsFromStore();
		this.toCDATA(outputs, 'outputs');
	},
	loading: false,
	cell: null,
	populate: function(newCell, isNewTask) {
		this.resetProperty();
		this.populateVersion(newCell, isNewTask);
		if(this.cell && this.cell.id){
			var currentShowState = this.showOptionIsTable;
			this.set('showOptionIsTable', true);
			this.showOptionStateMap[this.cell.id] = currentShowState;
		}

		this.set('loading', true);
		this.set('cell', newCell);
		this.set('label', newCell.getAttribute('label'));
		this.populateMerge(newCell);
		var result = this.parse(newCell.getValue());
		this.set('taskName', result.taskName || '');
		this.set('newTask', !this.taskName || isNewTask || newCell.isNewTask);
		this.set('reqCount', this.reqCount + 1);
		this.getTask(result.taskName, function(task) {
			//this.updateTaskProperty(task);
			var r = [];
			r.raw = task;
			this.doValidateTaskName(r, null, null, true);
			this.set('isTaskAuthorized', true);
		}, function() {
			this.set('isTaskAuthorized', false);

			/* TODO - support Version Control
			// getTask failed but we still want to getHistory list
			this.getTaskOnly(result.taskName, null, function(actiontask) {
				this.versionHistoryStore.load({
					params: {
						id: actiontask.id,
						docFullName: actiontask.ufullName
					}
				});
			})
			*/ 
		}, function() {
			this.set('reqCount', this.reqCount - 1);
		});
		this.set('taskNameIsValid', true);
		this.taskStore.removeAll();
		this.set('loading', false);
	},
	task: null,
	subscribeDirtyKey: null,

	// retrieve task, update task properties panel, and if not in actiontasklist, add to it
	wait: false,
	buildActionTask: function(content) {
		var actiontaskbuilder;
		if (this.actiontaskbuilder) {
			actiontaskbuilder = this.actiontaskbuilder;
		} else {
			actiontaskbuilder = this.model({
				mtype: 'RS.actiontask.ActionTask',
				embed: true
			});
		}
		actiontaskbuilder.successfulActionTaskLoad(content);

		var actiontaskData = actiontaskbuilder.gatherData();
		var originalData = actiontaskbuilder.gatherOriginalData();
		actiontaskData.originalData = originalData;
		return actiontaskData;
	},
	iSTaskSelected: function(taskName) {
		return (this.parentVM.cellSelected && this.parentVM.activeItem.taskName === taskName);
	},
	populateSelectedActionTask: function(content, onSuccess, onComplete) {
		// Populate iff the content matches with selected action task
		if (this.iSTaskSelected(content.data.ufullName)) {
			if (this.actiontaskbuilder) {
				if (!this.subscribeDirtyKey) {
					this.subscribeDirtyKey = this.actiontaskbuilder.subscribeDirty(function (dirty) {
						if (dirty) {
							if (Ext.isFunction(this.rootVM.notifyDirtyFlag)) {
								this.rootVM.notifyDirtyFlag(this.viewmodelName);
							}
						} else if (Ext.isFunction(this.rootVM.clearComponentDirtyFlag)) {
							this.rootVM.clearComponentDirtyFlag(this.viewmodelName);
						}
					}.bind(this));
				}
			}
			if (typeof onSuccess === 'function') {
				onSuccess.call(this, content.data);
			}
			if (typeof onComplete === 'function') {
				onComplete.call(this);
			}
		}
	},
	getTask: function(taskName, onSuccess, onFailure, onComplete) {
		if (!taskName) {
			if (typeof onComplete === 'function') {
				onComplete.call(this);
			}
			return;
		}
		var version = '';
		if (this.latestStable) {
			version = 'latestStable';
		} else if (this.specificVersion) {
			version = this.versionNumber;
		}
		var params = {
			id: '',
			name: taskName
		};
		/* TODO - support Version Control
		if (version) {
			Ext.apply(params, {
				version: version
			})
		}
		*/
		this.ajax({
			url: '/resolve/service/actiontask/get',
			params: params,
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (respData.success) {
					this.parentVM.parentVM.setActionTaskContent(taskName, version, respData);
					if (this.iSTaskSelected(taskName)) {
						var actiontask = this.buildActionTask(respData);
						if (this.actiontaskbuilder) {
							this.actiontaskbuilder.successfulActionTaskLoad(actiontask);
							this.actiontaskbuilder.loadOriginalData(actiontask.originalData, actiontask.isDirty);
						}
						this.populateSelectedActionTask(actiontask, onSuccess, onComplete);
					}
				} else {
					clientVM.displayError(this.localize('getTaskError', respData.message), null, null, false);

					if (typeof onFailure === 'function') {
						onFailure.call(this, respData);
					}
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
				if (typeof onFailure === 'function') {
					onFailure.call(this);
				}
			},
			callback: function() {
				if (typeof onComplete === 'function') {
					onComplete.call(this);
				}
			}
		});
	},

	searchTask: function() {
		var currentDocName = this.rootVM.fullName;
		var me = this;
		this.open({
			mtype: 'RS.actiontask.ActionTaskPicker',
			currentDocName : currentDocName,
			dumper: {
				dump: function(selections, isNew) {
					if (selections.length > 0) {
						var selection = selections[0]
						var isNewTask = me.newTask;
						if (!isNew) {
							//me.populate(me.cell, isNewTask);
							me.set('taskName', selection.get('ufullName'));
							me.taskNameChanged = true;
							me.taskStore.load();
							return;
						}
						me.parentVM.fireEvent('selectTask', selection.get('uname'), selection.get('unamespace'), null, me.cell);
						me.set('taskName', selection.get('ufullName'));
						Ext.fly(window).on('focus', function() {
							me.taskStore.load();
							if (me.taskNameIsValid)
								me.populate(me.cell);
						}, me, {
							single: true
						});
						this.doClose();
					}
				}
			}
		})
	},

	openExecutionWindow: function(button) {
		var task = this.task;
		var	mockBlockData = this.actiontaskbuilder.mockEdit.getData();
		var inputsParamStore = this.inputs.store;

		if (this.executionWin)
			this.executionWin.doClose();
		this.executionWin = this.open({
			mtype: 'RS.actiontaskbuilder.Execute',
			target: button.getEl().id,
			executeDTO: {
				actiontask: task.ufullName,
				actiontaskSysId: task.id,
				// problemId: this.newWorksheet ? 'NEW' : null,
				action: 'TASK'
			},
			inputsLoader: function(paramStore, origParamStore, mockStore) {
				for (var i = 0; i < inputsParamStore.getCount(); i++) {
					var inputsParam = inputsParamStore.getAt(i);
					var source = inputsParam.get('source');
					var value = inputsParam.get('value');
					if (source != 'DEFAULT' && source != 'CONSTANT') {
						value = '$'+source+'{'+value+'}';
					}
					var param = {
						name: inputsParam.get('name'),
						value: value,
						value: inputsParam.get('encrypt') ? '*****' : value,
						origValue: value,
						order: inputsParam.get('order'),
						protected: inputsParam.get('protected'),
						encrypt: inputsParam.get('encrypt'),
						modified: inputsParam.get('isDirty'),
						utype: 'INPUT'
					};
					paramStore.add(param);
					origParamStore.add(param);
				}
				origParamStore.sort('order');

				if (mockBlockData) {
					var mockData = mockBlockData[0];
					if (mockData) {
						mockStore.add(mockData);
					}
				}
			}
		});
	},
	jumpToTaskIsVisible$: function() {
		return this.task && this.taskNameIsValid === true;
	},
	jumpToTask: function() {
		if (this.task) {
			var params = {
				name: this.task.uname,
				namespace: this.task.unamespace
			};
			/* TODO - support Version Control
			var version = this.cell.getAttribute('version');
			if (version) {
				Ext.apply(params, {
					version: version
				})
			}
			*/ 
			clientVM.handleNavigation({
				modelName: 'RS.actiontask.ActionTask',
				target: '_blank',
				params: params
			});
		}
	},

	checkTargetParamDependency: function(outputParamName, onPerformAction, onCancel) {
		var cellOutputParamList = [];
		cellOutputParamList.push(outputParamName);

		var cell = this.cell;
		var targetInputParamList = [];
		var hasDependency = false;

		Ext.each(cell.edges, function(edge) {
			if (edge.source != cell)
				return;
			var target = edge.target;
			var targetValue = target ? target.getValue() : '';
			if (!(targetValue && (targetValue.tagName.toLowerCase() == 'task' || targetValue.tagName.toLowerCase() == 'start')))
				return;
			var targetParams = this.parse(targetValue);
			targetInputParamList = targetInputParamList.concat(this.getTargetInputParams(targetParams));
		}, this);
		
		// if any target has input params, check if they have dependencies on this element's source output parameters
		if (targetInputParamList.length) {
			for (var i=0, l=cellOutputParamList.length; i<l ; i++){
				if (targetInputParamList.indexOf(cellOutputParamList[i]) != -1) {
					hasDependency = true;
					break;
				}
			}
		}
		
		if (hasDependency) {
			this.message({
				title: this.localize('warning'),
				msg: this.localize('automationModelDependencyDeleteParam'),
				buttons: Ext.MessageBox.YESNO,
				buttonText: {
					yes: this.localize('confirm'),
					no: this.localize('cancel')
				},
				scope: this,
				fn: function(btn) {
					if (btn == 'yes') {
						if (typeof onPerformAction === 'function') {
							onPerformAction.call(this);
						}
					} else {
						if (typeof onCancel === 'function') {
							onCancel.call(this);
						}
					}
				},
			});
		} else {
			if (typeof onPerformAction === 'function') {
				onPerformAction.call(this);
			}
		}
	},

	showVersionNumberOption$: function() {
		return this.specificVersion;
	},

	details: function() {
		var me = this;
		this.open({
			mtype: 'RS.actiontaskbuilder.Version',
			name : this.taskName,
			uname: this.selectedTaskName,
			id : this.id,
			unamespace: this.selectedTaskNamespace,
			isDetails: true,
			dumper: {
				dump: function(selections) {
					if (selections.length > 0) {
						var selection = selections[0];
						this.set('versionNumber', selection.get('version') || selection.get('revision'));
						this.updateVersionNumber(this.versionNumber);
					}
				},
				scope: this
			}
		});
	}
});
