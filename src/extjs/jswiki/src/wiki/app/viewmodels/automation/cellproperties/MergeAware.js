glu.defModel('RS.wiki.automation.MergeAware', {
	merge: '',
	when_merge_changed: {
		on: ['mergeChanged'],
		action: function() {
			if (this.cell && (this.loading || this.reqCount > 0))
				return;
			this.parentVM.fireEvent('taskdependencychanged', this.cell, this.merge);
		}
	},
	mergeStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},
	initMixin: function() {
		this.mergeStore.add([{
			name: this.localize('all'),
			value: 'all'
		}, {
			name: this.localize('any'),
			value: 'any'
		}, {
			name: this.localize('targets'),
			value: 'targets'
		}]);
	},
	populateMerge: function(cell) {
		var merge = cell.getAttribute('merge');
		if (merge)
			merge = merge.split('=');
		this.set('merge', merge ? Ext.String.trim(merge[1]) : 'all');
	}
});