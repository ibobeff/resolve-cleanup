glu.defModel('RS.wiki.automation.DecisionTreeElementPicker', {
	mixins: ['ElementPicker'],

	initElements: function() {
		var list = [{
			elementType: 'root',
			elementImg: '/resolve/jsp/model/images/symbols/start.png',
			elementWidth: 30,
			elementHeight: 30
		}, {
			elementType: 'container',
			elementImg: '/resolve/jsp/model/images/swimlane.gif',
			elementWidth: 200,
			elementHeight: 200
		}, {
			elementType: 'question',
			elementImg: '/resolve/jsp/model/images/rhombus.gif',
			elementWidth: 30,
			elementHeight: 30
		}, {
			elementType: 'document',
			elementImg: '/resolve/jsp/model/images/rectangle.gif',
			elementWidth: 100,
			elementHeight: 40
		}, {
			elementType: 'runbook',
			elementImg: '/resolve/jsp/model/images/rectangle.gif',
			elementWidth: 100,
			elementHeight: 40
		}, {
			elementType: 'task',
			elementImg: '/resolve/jsp/model/images/rounded.gif',
			elementWidth: 100,
			elementHeight: 40
		}, {
			elementType: 'straight',
			elementImg: '/resolve/jsp/model/images/straight.gif',
			elementWidth: 150,
			elementHeight: 150
		}, {
			elementType: 'horizontal',
			elementImg: '/resolve/jsp/model/images/connect.gif',
			elementWidth: 150,
			elementHeight: 150
		}, {
			elementType: 'vertical',
			elementImg: '/resolve/jsp/model/images/vertical.gif',
			elementWidth: 150,
			elementHeight: 150
		}];

		for(var i = 0; i < list.length; i++) {
			this.elements.add(glu.model('RS.wiki.automation.ModelElement', list[i]));
		}
	}
});