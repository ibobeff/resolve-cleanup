glu.defModel('RS.wiki.automation.DecisionTree', {
	mixins: ['Model'],
	resolveModelType: 'decisionTree',
	decisionTreeElementPicker: {
		mtype: 'DecisionTreeElementPicker'
	},
	decisionTreeImagePicker: {
		mtype: 'DecisionTreeImagePicker'
	},
	on_before_remove_cell : {
		on : ['beforeRemoveCell'],
		action : function(){
			var activeProperty = this.cellProperties.getActiveItem();
			if(activeProperty.viewmodelName == 'AnswerProperty'){
				//Remove all dt WSDATA variables
				activeProperty.cleanUpWSDATA(); 
			}
		}
	},
	init: function() {
		if (this.rootVM.ns === 'RS.wiki') {
			// "NoProperty" must be last item in list
			var list = ['RootProperty', 'ContainerProperty', 'AnswerProperty', 'QuestionProperty', 'DocumentProperty', 'NewTask', 'TaskProperty', 'NewRunbook', 'RunbookProperty', 'NoProperty'];
			Ext.each(list, function(name) {
				var model = {
					mtype: name
				};
				if(name == 'TaskProperty' || name == 'RunbookProperty'){
					Ext.apply(model, {
						isPartOfDT : true,
					})
				}
				this.cellProperties.add(this.model(model));
			}, this);
			this.initEventWatchers();
		}
	},

	title$: function() {
		if (this.activeItem)
			return this.localize(this.activeItem.viewmodelName + 'Title');		
		else 
			return this.localize('properties');
	},

	selectAnswerProperty: function(cell) {
		var c = null;
		//Connector is used as answer. Only care the connector coming from question.
		if(cell && cell.source && cell.source.value && cell.source.value.tagName
			&& cell.source.value.tagName.toLowerCase() == 'question'){
			this.cellProperties.each(function(p) {
				if (p.viewmodelName.toLowerCase().indexOf('answer') != -1) {
					this.set('activeItem', p);
					p.populate(cell);
				}
			}, this);
		}
		else 
			//None answer connector should show nothing
			this.set('activeItem', this.cellProperties.length - 1);	
	},
	showToolTip: function(cell, callback) {
		var value = cell.getValue();
		var tagName = value.tagName.toLowerCase();
		if (tagName == 'task') {
			this.getTaskTip(cell, callback);
			return;
		}
		if (tagName == 'subprocess' || tagName == 'document') {
			this.getRunbookTip(cell, callback);
			return;
		}
		callback();
	}
});