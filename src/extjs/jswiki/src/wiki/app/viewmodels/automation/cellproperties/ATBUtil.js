glu.defModel('RS.wiki.automation.ATBUtil',{
	actiontaskbuilder: {
		mtype: 'RS.actiontask.ActionTask',
		embed: true
	},
	automationTabIsPressedFlag: false,
	decisionTreeTabIsPressedFlag: false,

	viewactiontasktitle$: function() {
		return this.localize('view'); // embedded ActionTaskBuilder in now read-only
	},
	isTaskAuthorized: true,
	taskAuthorized$: function() {
		return this.isTaskAuthorized;
	},
	when_isTaskAuthorized_changed: {
		on: ['isTaskAuthorizedChanged'],
		action: function () {
			if (!this.isTaskAuthorized) {
				this.set('activeTab', 0);
			}
		}
	},
	expandAllDisabled$: function() {
		if (this.configureVisible) {
			return true;
		} else if (this.actiontaskbuilder) {
			return (this.actiontaskbuilder.numSectionsOpen > 0 || this.actiontaskbuilder.isMaxScreenSize);
		}
	},

	expandAll: function() {
		if (this.actiontaskbuilder) {
			this.actiontaskbuilder.expandAll();
		}
	},

	collapseAllDisabled$: function() {
		if (this.configureVisible) {
			return true;
		} else if (this.actiontaskbuilder) {
			return (this.actiontaskbuilder.numSectionsOpen == 0 || this.actiontaskbuilder.isMaxScreenSize);
		}
	},

	collapseAll: function() {
		if (this.actiontaskbuilder) {
			this.actiontaskbuilder.collapseAll();
		}
	},

	actionTaskEditBtnsHidden$: function() {
		return true; // embedded ActionTaskBuilder in now read-only
	},

	editActionTaskDisabled$: function() {
		if (this.configureVisible) {
			return true;
		} else if (this.actiontaskbuilder) {
			return (!this.actiontaskbuilder.readOnlyMode || this.actionTaskEditBtnsHidden);
		}
	},

	editActionTask: function() {
		if (this.actiontaskbuilder) {
			this.actiontaskbuilder.edit();
		}
	},

	doneEditActionTaskDisabled$: function() {
		if (this.configureVisible) {
			return true;
		} else if (this.actiontaskbuilder) {
			return (this.actiontaskbuilder.readOnlyMode || this.actionTaskEditBtnsHidden);
		}
	},

	doneEditActionTask: function() {
		if (this.actiontaskbuilder) {
			this.actiontaskbuilder.exitToView();
		}
	}
})