glu.defModel('RS.wiki.automation.TelnetConnectionConfig', {
	mixins : ['ConnectionUtils'],	
	connectionType : 'telnet',
	taskWizard : {
		mtype : 'RS.wiki.automation.TaskWizard',
		connectionType : 'telnet'
	},

	//Main Config	
	password: '',
	hostSource: 'CONSTANT',
	portSource: 'CONSTANT',
	host: '',
	port: 23,
	username: '',
	paramStore : null,
	usernameSource: undefined,
	passwordSource: undefined,	
	usernameAndPassword: true,	
	sourceStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [{
			name: 'CONSTANT',
			value: 'CONSTANT'
		}, {
			name: 'PARAM',
			value: 'PARAM'
		}, {
			name: 'FLOW',
			value: 'FLOW'
		}, {
			name: 'WSDATA',
			value: 'WSDATA'
		}, {
			name: 'PROPERTY',
			value: 'PROPERTY'
		}]
	},
	when_prompt_changed : {
		on : ['promptChanged'],
		action : function(){
			this.taskWizard.updatePrompt(this.prompt);
		}
	},
	when_sessionName_changed : {
		on : ['sessionNameChanged'],
		action : function(){
			this.taskWizard.updateSessionName(this.sessionName);
		}
	},
	//Option
	prompt: '',
	loginPrompt: 'login:',
	passwordPrompt: 'Password:',
	timeout: 30,
	queueName: '',
	queueNameSource: 'CONSTANT',
	sessionName: '',

	//Task 	
	command : '',	
	propertyStore: {
		mtype: 'store',
		fields: ['uname'],
		sorters: ['uname'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/atproperties/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		},
		pageSize: -1
	},	
	when_password_source_changed: {
		on: ['passwordSourceChanged'],
		action: function(newValue, oldValue) {
			if (oldValue == 'CONSTANT' || newValue == 'CONSTANT')
				this.set('password', '');
		}
	},
	when_any_source_changed: {
		on: ['hostChanged', 'portChanged', 'usernameChanged', 'passwordChanged'],	
		action: function(newValue, oldValue, bindingInfo) {
			this.propertyStore.keyword = this[bindingInfo.modelPropName];
		}
	},

	init : function(){
		this.set('usernameSource', 'CONSTANT');
		this.set('passwordSource', 'CONSTANT');
		this.set('paramStore', this.rootVM.getParamStore());
		this.set('prompt','$FLOW{PROMPT}');
		this.propertyStore.load();
	},	

	//Main Config
	hostIsText$: function() {
		return this.hostSource == 'CONSTANT' || this.hostSource == 'FLOW' || this.hostSource == 'WSDATA';
	},	
	hostIsParam$: function() {
		return this.hostSource == 'PARAM';
	},
	hostIsProperty$: function() {
		return this.hostSource == 'PROPERTY';
	},	

	portIsText$: function() {
		return this.portSource == 'CONSTANT' || this.portSource == 'FLOW' || this.portSource == 'WSDATA';
	},	
	portIsParam$: function() {
		return this.portSource == 'PARAM';
	},
	portIsProperty$: function() {
		return this.portSource == 'PROPERTY';
	},

	usernameIsText$: function() {
		return this.usernameSource == 'CONSTANT' || this.usernameSource == 'FLOW' || this.usernameSource == 'WSDATA';
	},	
	usernameIsParam$: function() {
		return this.usernameSource == 'PARAM';
	},
	usernameIsProperty$: function() {
		return this.usernameSource == 'PROPERTY';
	},

	passwordIsConstant$: function() {
		return this.passwordSource == 'CONSTANT';
	},	
	passwordIsFlowOrWs$ : function(){
		return this.passwordSource == 'FLOW' || this.passwordSource == 'WSDATA';
	},
	passwordIsParam$: function() {
		return this.passwordSource == 'PARAM';
	},
	passwordIsProperty$: function() {
		return this.passwordSource == 'PROPERTY';
	},

	//Option Config
	queueNameSourceChanged$: function() {
		this.set('queueName', '');
	},
	queueNameIsText$: function() {
		return this.queueNameSource == 'CONSTANT' || this.queueNameSource == 'WSDATA' || this.queueNameSource == 'FLOW';;
	},	
	queueNameIsProperty$: function() {
		return this.queueNameSource == 'PROPERTY';
	},
	queueNameIsParam$: function() {
		return this.queueNameSource == 'PARAM';
	},

	getConnectionParams : function(){
		var params = {};
		var paramArray = ['host','port','username','password','queueName'];
		for(var i = 0; i < paramArray.length; i++){
			var paramName = paramArray[i];
			if(this[paramName]){
				params[paramName] = this[paramName + 'Source'] == "CONSTANT" ? this[paramName] : "$" + this[paramName + 'Source'] + "{" + this[paramName] + "}";
			}
		}		
		params['timeout'] = this.timeout;
		params['prompt'] = this.prompt;
		params['sessionName'] = this.sessionName;
		params['loginPrompt'] = this.loginPrompt;
		params['passwordPrompt'] = this.passwordPrompt;	
		return params;
	}
});
