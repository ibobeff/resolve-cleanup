glu.defModel('RS.wiki.automation.ParameterEdit', {
	id: '',
	title: '',
	name: '',
	source: '',
	value: '',
	encrypt: false,
	description: '',
	isDefaultParm: false,
	defaultValue: '',
	currentSourceType: '',
	updateParamBtnName: '',
	inputType: 'text',
	comboValue: '',
	comboValueMaxChar: 5000,

	extractor: /^\$([^\{\}]+)\{([^\{\}]+)\}$/,
	validParamRegex: /\$([^\{\}]+)\{/g,

	paramKeyRegex: /^[a-zA-Z_0-9]*$/,
	paramKeyRegexErrMsg: '',

	paramSource: null,

	addVarParamStore: {
		mtype: 'store',
		fields: [{
			name: 'source',
			convert: function(v, r) {
				return r.raw;
			}
		}],
		data: ['INPUT', 'OUTPUT', 'FLOW', 'WSDATA', 'PARAM', 'PROPERTY']
	},

	paramsStore: {
		mtype: 'store',
		fields: ['name']
	},

	propertyStore: {
		mtype: 'store',
		fields: ['name']
	},

	outputStore: {
		mtype: 'store',
		fields: ['name']
	},

	isInputPressed: null,

	isCustomValue: false,
	isCustomValueChecked$: function() {
		return this.isCustomValue;
	},

	defaultSourceValue: '',
	fromSourceType: '',
	fromSourceValue: '',
	fromSourceComboValue: '',

	customValue: '',
	origCustomValue: '',
	addVarSourceType: '',
	addVarSourceValue: '',
	sourceLabel: '',

	when_fromSourceType_changed: {
		on: ['fromSourceTypeChanged'],
		action: function() {
			if (this.fromSourceType != this.source) {
				this.set('fromSourceComboValue', '');
				this.set('fromSourceValue', '');
			}
		}
	},

	when_addVarSourceType_changed: {
		on: ['addVarSourceTypeChanged'],
		action: function() {
			this.set('addVarSourceValue', '');
		}
	},

	fromSourceComboValueIsValid: true,
	when_fromSourceComboValue_changed: {
		on: ['fromSourceComboValueChanged'],
		action: function() {
			if (this.paramKeyRegex.test(this.fromSourceComboValue)) {
				this.set('fromSourceComboValueIsValid', true);
			} else {
				this.set('fromSourceComboValueIsValid', this.paramKeyRegexErrMsg);
			}
		}
	},

	fromSourceValueIsValid: true,
	when_fromSourceValue_changed: {
		on: ['fromSourceValueChanged'],
		action: function() {
			if (this.paramKeyRegex.test(this.fromSourceValue)) {
				this.set('fromSourceValueIsValid', true);
			} else {
				this.set('fromSourceValueIsValid', this.paramKeyRegexErrMsg);
			}
		}
	},

	customValueIsValid: true,
	when_customValue_changed: {
		on: ['customValueChanged'],
		action: function() {
			if (this.validateCustomInput(this.customValue) == true) {
				this.set('customValueIsValid', true);
			}
		}
	},

	addVariableBtnDisabled: true,
	when_addVarSourceValue_changed: {
		on: ['addVarSourceValueChanged'],
		action: function() {
			if (this.addVarSourceValue) {
				this.set('addVariableBtnDisabled', false);
			} else {
				this.set('addVariableBtnDisabled', true);
			}
		}
	},
	
	validateCustomInput: function(value) {
		var m = null;
		var errMsg = '';

		if (value.indexOf('$') != -1 && value.indexOf('{') != -1 && value.indexOf('}') != -1) {
			this.validParamRegex.lastIndex = 0;
			while ((m = this.validParamRegex.exec(value)) !== null) {
				// This is necessary to avoid infinite loops with zero-width matches
				if (m.index === this.validParamRegex.lastIndex) {
					this.validParamRegex.lastIndex++;
				}
				
				// The result can be accessed through the `m`-variable.
				if (m[1]) {
					var indexParamSrc = this.paramSource.find('source', m[1], 0, false, false, true);
					if (indexParamSrc == -1 || m[1] == 'DEFAULT') {
						if (this.isInputPressed) {
							errMsg += this.localize('validFromSourceTypeCharacters', m[1]);
						} else {
							errMsg += this.localize('validFromDestinationTypeCharacters', m[1]);
						}
					}
				} else {
					if (this.isInputPressed) {
						errMsg += this.localize('missingSourceType', m.index +1);
					} else {
						errMsg += this.localize('missingDestinationType', m.index +1);
					}
				}
			}
		}

		if (errMsg) {
			this.set('customValueIsValid', errMsg);
			return false;
		} else {
			return true;
		}
	},

	init: function() {
		this.set('paramSource', Ext.create('Ext.data.Store',{
			fields: [{
				name: 'source',
				convert: function(v, r) {
					return r.raw
				}
			}],
			data:['DEFAULT', 'CONSTANT', 'WSDATA', 'PROPERTY'].concat(this.isPartOfDT ? [] : ['INPUT', 'OUTPUT', 'FLOW', 'PARAM'])
		}));
		if (this.source == 'CONSTANT') {
			var isConst = !this.extractor.test(this.value);
			if (!isConst) {
				var m = this.extractor.exec(this.value);
				var indexParamSrc = this.paramSource.find('source', m[1], 0, false, false, true);
				if (indexParamSrc != -1 && m[2] && this.paramKeyRegex.test(m[2])) {
					this.source = this.paramSource.getAt(indexParamSrc).raw;
					this.value = m[2];
				}
			}

			this.set('customValueIsValid', this.validateCustomInput(this.value));
		}

		this.currentSourceType = this.source;

		switch (this.currentSourceType) {
		case 'CONSTANT':
			this.set('customValue', this.value);
			break;
		case 'PROPERTY':
		case 'PARAM':
		case 'OUTPUT':
			this.set('comboValue', this.value.substr(0, this.comboValueMaxChar));
			this.set('fromSourceComboValue', this.comboValue);
			break;
		default:
			this.set('fromSourceValue', this.value);
			break;
		}

		if (this.encrypt) {
			this.set('inputType', 'password');
			if (this.customValue) {
				this.set('origCustomValue', this.customValue);
				this.set('customValue', '*****');
			}
		}

		var defaultSourceValue = this.encrypt ? '*****' : this.defaultValue || this.localize('noDefaultValueDefined');
		this.set('defaultSourceValue', defaultSourceValue);

		if (this.id) {
			this.set('title', this.localize('editParamTitle'));
			this.set('updateParamBtnName', this.localize('updateParamBtn'));
		} else {
			this.set('title', this.localize('addParamTitle'));
			this.set('updateParamBtnName', this.localize('addParamBtn'));
		}

		if (this.parentVM.pressInput) {
			this.set('sourceLabel', this.localize('fromSource'));
			this.set('isInputPressed', true);
		} else {
			this.set('sourceLabel', this.localize('toDestination'));
			this.set('isInputPressed', false);
			this.paramSource.filter({
				filterFn: function(item) {
					return ['INPUT', 'OUTPUT', 'PROPERTY', ].indexOf(item.get('source')) == -1;
				}
			});
			this.addVarParamStore.filter({
				filterFn: function(item) {
					return ['INPUT', 'OUTPUT', 'PROPERTY', ].indexOf(item.get('source')) == -1;
				}
			});
		}

		if (!this.isDefaultParam) {
			this.paramSource.filter({
				filterFn: function(item) {
					return ['DEFAULT', ].indexOf(item.get('source')) == -1;
				}
			});
		}

		if (this.source == 'CONSTANT') {
			this.set('isCustomValue', true);
		} else {
			this.set('fromSourceType', this.source);
		}

		// retrieve the Action Task properties list
		Ext.each(this.rootVM.allATProperties, function(v) {
			this.propertyStore.add({
				name: v.uname
			});
		}, this);

		// retrieve the params list
		for (var i=0; i<this.rootVM.params.firstColumnStore.data.items.length; i++) {
			var param = this.rootVM.params.firstColumnStore.data.items[i];
			this.paramsStore.add({
				name: param.data.name
			});
		}

		// retrieve the outputs parameter list from all previous actiontask
		var outputData = this.parentVM.parentVM.outputStore.getRange();
		this.outputStore.loadData(outputData);

		this.paramKeyRegexErrMsg = this.localize('validParamKeyCharacters');
	},

	clearEncryptValue: function() {
		if (this.encrypt && this.customValue == '*****') {
			this.set('customValue', '');
		}
	},

	updateEncryptValue: function() {
		if (this.encrypt && this.customValue) {
			this.set('origCustomValue', '');
		}
	},

	addVarTextboxValueReadOnly$: function() {
		return this.addVarSourceType == 'DEFAULT';
	},

	fromSourceTextboxValueEnabled$: function() {
		var isEnabled = false;

		switch(this.fromSourceType) {
		case 'INPUT':
		case 'WSDATA':
		case 'FLOW':
		case 'CONSTANT':
			isEnabled = true;
			break;
		default:
			isEnabled = false;
			break;
		}

		return isEnabled;
	},

	addVarTextboxValueEnabled$: function() {
		var isEnabled = false;

		switch(this.addVarSourceType) {
		case 'INPUT':
		case 'WSDATA':
		case 'FLOW':
			isEnabled = true;
			break;
		default:
			isEnabled = false;
			break;
		}

		return isEnabled;
	},

	defaultValueEnabled$: function() {
		if (this.encrypt) {
			this.set('defaultSourceValue', '*****');
		}
		return this.fromSourceType == 'DEFAULT';
	},

	fromSourcePropertyValueEnabled$: function() {
		return this.fromSourceType == 'PROPERTY';
	},

	addVarPropertyValueEnabled$: function() {
		return this.addVarSourceType == 'PROPERTY';
	},

	fromSourceParamsStoreEnabled$: function() {
		return this.fromSourceType == 'PARAM';
	},

	addVarParamsStoreEnabled$: function() {
		return this.addVarSourceType == 'PARAM';
	},

	fromSourceOutputStoreEnabled$: function() {
		return this.fromSourceType == 'OUTPUT';
	},

	addVarOutputStoreEnabled$: function() {
		return this.addVarSourceType == 'OUTPUT';
	},

	nodescription$: function() {
		return this.description == '';
	},

	isAddingVariable: false,
	addVariable: function() {
		this.set('isAddingVariable', true);
		this.set('addVariableDisabled', true);
		this.set('addVarSourceType', 'PARAM');
		this.set('addVarSourceValue', '');
	},

	addVariableIsPressed$: function() {
		return this.isAddingVariable;
	},

	addVariableDisabled: false,
	isAddVariableDisabled$: function() {
		return this.addVariableDisabled;
	},

	fromSourceClicked: function() {
		this.set('isCustomValue', false);
		if (this.fromSourceType == '') {
			this.set('fromSourceType', 'CONSTANT');
		}
	},

	customValueClicked: function() {
		this.set('isCustomValue', true);
	},

	addVariableBtn: function() {
		this.set('isAddingVariable', false);
		this.set('addVariableDisabled', false);
		var value = Ext.String.trim(this.customValue) + ' ' + '$'+this.addVarSourceType+'{'+this.addVarSourceValue+'}';
		this.set('customValue', Ext.String.trim(value));
	},

	cancelVariableBtn: function() {
		this.set('isAddingVariable', false);
		this.set('addVariableDisabled', false);
	},

	updateRecordInStore: function() {
		var record = this.parentVM.store.getByInternalId(this.id);
		if (record) {
			record.set('name', this.name);
			record.set('source', this.source);
			record.set('value', this.value);

			if (this.isDefaultParam && (this.source != this.currentSourceType || this.value != this.defaultValue)) {
				record.set('isDirty', true);
			}
		}
	},

	updateParam: function() {
		if (this.isCustomValue) {
			this.source = 'CONSTANT';
			if (this.encrypt && this.customValue == '*****' && this.origCustomValue) {
				this.value = this.origCustomValue;
			} else {
				this.value = this.customValue;
			}
		} else {
			switch (this.fromSourceType) {
			case 'DEFAULT':
				this.defaultParam();
				break;
			case 'PROPERTY':
			case 'PARAM':
			case 'OUTPUT':
				this.source = this.fromSourceType;
				this.value = this.fromSourceComboValue;
				break;
			default:
				this.source = this.fromSourceType;
				this.value = this.fromSourceValue;
				break;
			}
		}

		if (this.id) {
			this.updateRecordInStore();
		} else {
			this.parentVM.addNewParam(this.name, this.source, this.value);
		}

		this.doClose();
	},

	updateParamIsValid$: function() {
		if (this.isCustomValue) {
			return (this.name && this.validateParamName(this.name) && this.customValueIsValid == true);
		} else {
			var isValueValid = true;
			switch (this.fromSourceType) {
			case 'DEFAULT':
				break;
			case 'PROPERTY':
			case 'PARAM':
			case 'OUTPUT':
				// check fromSourceComboValue
				isValueValid = (this.fromSourceComboValueIsValid == true);
				break;
			default: 
				// check fromSourceValue
				isValueValid = (this.fromSourceValueIsValid == true);
				break;
			}
			return (this.name && this.validateParamName(this.name) && isValueValid);
		}
	},

	validateParamName: function(paramName) {
		return (/^[a-zA-Z_$][a-zA-Z_$0-9]*$/.test(paramName));
	},

	defaultParam: function() {
		this.parentVM.resetParams(this.name);
		this.doClose();
	},
	defaultParamIsHidden$: function() {
		return !this.isDefaultParam;
	},

	cancel: function() {
		this.doClose();
	}
})
