glu.defModel('RS.wiki.automation.XmlAware', {
	parseCDATA: function(cdata) {
		if (!cdata)
			return [];
		try {
			return Ext.decode(cdata);
		} catch (e) {
			return [];
		}
	},
	parse: function(cell) {
		var result = {};

		function extract(node) {
			Ext.each(node.childNodes, function(child) {
				if (child.tagName) {
					var text = child.textContent || child.innerText || '';
					if (child.tagName.toLowerCase() == 'inputs')
						result['modelInputs'] = this.parseCDATA(text);
					if (child.tagName.toLowerCase() == 'outputs')
						result['modelOutputs'] = this.parseCDATA(text);
					if (child.tagName.toLowerCase() == 'name')
						result['taskName'] = Ext.String.trim(text);
				}
				extract.call(this, child);
			}, this);
		}
		extract.call(this, cell);
		if (!result.taskName && cell.getAttribute('description')) {
			var splits = cell.getAttribute('description').split('?');
			if (splits[0].indexOf('#') != -1)
				result.taskName = splits[0]
		}
		if (['end', 'start', 'event'].indexOf(cell.tagName.toLowerCase()) != -1)
			result.taskName = cell.tagName.toLowerCase() + '#resolve';
		return result;
	},
	toCDATA: function(params, tagName) {
		var paramEl = null;
		var tagEl = false;
		var xmlDoc = mxUtils.createXmlDocument();

		function find(node) {
			Ext.each(node.childNodes, function(child) {
				if (child.tagName) {
					if (child.tagName.toLowerCase() == 'params')
						paramEl = child;
					if (child.tagName.toLowerCase() == tagName) {
						var cdataNode = xmlDoc.createCDATASection(Ext.encode(params));
						while (child.firstChild)
							child.removeChild(child.firstChild);
						child.appendChild(cdataNode);
						tagEl = child;
					}
				}
				find(child);
			})
		}
		find(this.cell.getValue());
		if (tagEl) {
			this.parentVM.fireEvent('cdataChanged', this.cell);
			return;
		}
		var cdataNode = xmlDoc.createCDATASection(Ext.encode(params));
		var targetNode = xmlDoc.createElement(tagName);
		targetNode.appendChild(cdataNode);
		if (!paramEl) {
			paramEl = xmlDoc.createElement('params');
			this.cell.getValue().appendChild(paramEl);
		}
		paramEl.appendChild(targetNode);
		this.parentVM.fireEvent('cdatachanged', this.cell);
	}
});