glu.defModel('RS.wiki.automation.ConnectorProperty', {
	mixins: ['XmlAware', 'LabelAware'],
	condition: '',
	conditionStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},
	when_condition_changed: {
		on: ['conditionChanged'],
		action: function() {
			if (this.loading)
				return;
			this.parentVM.fireEvent('edgeconditiondependencychanged', this.condition);
		}
	},
	cell: null,
	loading: false,
	required: true,
	notRequired: false,
	when_required_changed: {
		on: ['requiredChanged'],
		action: function() {
			if (this.loading)
				return;
			this.parentVM.fireEvent('edgeexecutiondependencychanged', this.required ? 'required' : 'notRequired');
		}
	},
	connector: '',
	connectorStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},
	when_connector_changed: {
		on: ['connectorChanged'],
		action: function() {
			if (this.loading)
				return;
			this.parentVM.fireEvent('connectorchanged', this.connector);
		}
	},
	resetProperty: function() {
		this.set('loading', true);
		this.set('label', '');
		this.set('cell', null);
		this.set('connector', 'straight');
		this.set('required', true);
		this.set('notRequired', false);
		this.set('loading', false);
	},
	init: function() {
		this.conditionStore.add([{
			name: this.localize('none'),
			value: 'none'
		}, {
			name: this.localize('good'),
			value: 'good'
		}, {
			name: this.localize('bad'),
			value: 'bad'
		}]);
		this.connectorStore.add([{
			name: this.localize('straight'),
			value: 'straight'
		}, {
			name: this.localize('horizontal'),
			value: 'horizontal'
		}, {
			name: this.localize('vertical'),
			value: 'vertical'
		}])
	},
	executionValue: function() {
		if (!this.cell.style)
			return 'required';
		return this.cell.style.indexOf('dashed=true') != -1 ? 'notRequired' : 'required';
	},
	conditionValue: function() {
		if (!this.cell.style)
			return 'none';
		if (this.cell.style.indexOf('strokeColor=green') != -1)
			return 'good';
		if (this.cell.style.indexOf('strokeColor=red') != -1)
			return 'bad';
	},
	connectorValue: function() {
		if (!this.cell.style)
			return 'straight';
		if (this.cell.style.indexOf('noEdgeStyle') != -1)
			return 'straight';
		if (this.cell.style.indexOf('horizontal') != -1)
			return 'horizontal';
		if (this.cell.style.indexOf('vertical') != -1)
			return 'vertical';
		return 'straight';
	},
	populate: function(cell) {
		this.resetProperty();
		this.set('loading', true);
		this.set('cell', cell);
		this.set('label', cell.getAttribute('label'));
		this.set('condition', this.conditionValue() || 'none');
		this.set('connector', this.connectorValue() || 'straight');
		this.set('required', this.executionValue() == 'required');
		this.set('notRequired', this.executionValue() == 'notRequired');
		this.set('loading', false);
	}
});