glu.defModel('RS.wiki.automation.VariableSubstitution',{
	variableName : '',
	variableType : '',
	typeList : ['INPUT','OUTPUT','PARAM','FLOW','WSDATA','PROPERTY'],
	allVariables : {
		INPUT : [],
		OUTPUT : [],
		FLOW : [],
		WSDATA : [],
		PROPERTY : [],
		PARAM : []
	},		
	variableTypeStore : {
		mtype : 'store',
		fields : [{
			name : 'type',
			convert : function(value,record){
				return record.raw;
			}
		}]		
	},
	variableDataStore : {
		mtype : 'store',
		fields : [{
			name : 'name',
			convert : function(value,record){
				return record.raw;
			}
		}]	
	},
	when_variable_type_changed : {
		on : ['variableTypeChanged'],
		action : function(newVal){
			this.set('variableName', '');	
			this.variableDataStore.loadData(this.allVariables[newVal]);		
		}
	},
	init : function(){
		this.variableTypeStore.loadData(this.typeList);
		this.set('variableType', this.typeList[0])
	},
	valueSyntax : '${0}\{{1}\}',
	variableValue$ : function(){
		return Ext.String.format(this.valueSyntax, this.variableType, this.variableName);
	},
	variableIsText$ : function(){
		return this.variableType == 'FLOW' || this.variableType == 'WSDATA';
	},	
	add: function() {      
        if (!this.dumper)
            return
        if (Ext.isFunction(this.dumper))
            this.dumper(this.variableValue);
        else {
            var scope = this.dumper.scope || this;
            this.dumper.dump.call(scope, this.variableValue);
        }       
    },  
	cancel : function(){
		this.parentVM.set('showVariableForm', false);	
	},	
	saveVariableList : function(variableType, data){
		this.allVariables[variableType] = data;		
	},
})