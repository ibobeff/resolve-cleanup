glu.defModel('RS.wiki.automation.MainModelImagePicker', {
	mixins: ['ImagePicker'],

	initElements: function() {
		var list = [{
			elementType: 'apache',
			elementImg: '/resolve/jsp/model/images/resolve/application/apache.png',
			elementWidth: 50,
			elementHeight: 50
		}, {
			elementType: 'email',
			elementImg: '/resolve/jsp/model/images/resolve/application/email.png',
			elementWidth: 50,
			elementHeight: 50
		}, {
			elementType: 'workplace',
			elementImg: '/resolve/jsp/model/images/resolve/application/workplace.png',
			elementWidth: 50,
			elementHeight: 50
		}, {
			elementType: 'bell',
			elementImg: '/resolve/jsp/model/images/resolve/default/bell.png',
			elementWidth: 50,
			elementHeight: 50
		}, {
			elementType: 'box',
			elementImg: '/resolve/jsp/model/images/resolve/default/box.png',
			elementWidth: 50,
			elementHeight: 50
		}, {
			elementType: 'cube',
			elementImg: '/resolve/jsp/model/images/resolve/default/cube.png',
			elementWidth: 50,
			elementHeight: 50
		}, {
			elementType: 'gear',
			elementImg: '/resolve/jsp/model/images/resolve/default/gear.png',
			elementWidth: 50,
			elementHeight: 50
		}, {
			elementType: 'package',
			elementImg: '/resolve/jsp/model/images/resolve/default/package.png',
			elementWidth: 50,
			elementHeight: 50
		}, {
			elementType: 'user',
			elementImg: '/resolve/jsp/model/images/resolve/default/user.png',
			elementWidth: 50,
			elementHeight: 50
		}, {
			elementType: 'wrench',
			elementImg: '/resolve/jsp/model/images/resolve/default/wrench.png',
			elementWidth: 50,
			elementHeight: 50
		}, {
			elementType: 'linux',
			elementImg: '/resolve/jsp/model/images/resolve/server/linux.png',
			elementWidth: 50,
			elementHeight: 50
		}, {
			elementType: 'server',
			elementImg: '/resolve/jsp/model/images/resolve/server/server.png',
			elementWidth: 50,
			elementHeight: 50
		}, {
			elementType: 'earth',
			elementImg: '/resolve/jsp/model/images/resolve/network/earth.png',
			elementWidth: 50,
			elementHeight: 50
		}];

		for(var i = 0; i < list.length; i++) {
			this.elements.add(glu.model('RS.wiki.automation.ModelImage', list[i]));
		}
	}
});
