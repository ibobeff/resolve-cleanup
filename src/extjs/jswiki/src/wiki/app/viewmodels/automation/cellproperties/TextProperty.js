glu.defModel('RS.wiki.automation.TextProperty', {
	mixins: ['XmlAware', 'LabelAware', 'ActionTaskValidation'],
	loading: false,
	cell: null,
	populate: function(cell) {
		this.set('loading', true);
		this.set('cell', cell);
		this.set('label', cell.getAttribute('label'));
		this.checkActionTaskParamDependency();
		this.set('loading', false);
	}
});