glu.defModel('RS.wiki.automation.HTTPTaskConfig',{
	fields : ['sessionName','rawBody','method','commandUrl','body',{
		name : 'useFormData',
		type : Ext.data.Types.BOOLEAN
	}],	
	allVariables : {
		INPUT : [],
		OUTPUT : [],
		FLOW : [],
		WSDATA : [],
		PROPERTY : [],
		PARAM : []
	},		
	syntax : null,
	syntaxStore :null,		
	taskIsPartOfCreation : true,
	urlVariableForm : {
		mtype : 'VariableSubstitution'	
	},
	showUrlVariableForm : false,
	variableForm : {
		mtype : 'VariableSubstitution'	
	},
	showVariableForm : false,
	methodStore : {
		mtype: 'store',
		fields: ['value'],
		data: [{
			value: 'POST'
		}, {
			value: 'GET'		
		}]
	},
	defaultData : {
		useFormData : true,
		commandUrl : '',
		body : '',
		method : 'GET'
	},
	labelWidth: 120,
	maxWidth: 240,
	editorMinHeight: 0,
	embedded: false,
	waitingForDataChange : false,

	//Used to update cell property on parameterWizard
	update_cell_properties : {
		on : ['sessionNameChanged','bodyChanged', 'methodChanged', 'useFormDataChanged','commandUrlChanged','HeaderChanged', 'FormDataChanged','rawBodyChanged'],
		action : function(newVal, oldVal, bindingInfo){
			if(this.waitingForDataChange){
				if(this.delayTask)
					clearTimeout(this.delayTask);
				this.delayTask = setTimeout(function(){
					var propName = bindingInfo['modelPropName'] || bindingInfo;
					if(propName == 'FormData')
						this.parentVM.updateValueForProperty('body', this.getAllFormData());
					else if(propName == 'rawBody')
						this.parentVM.updateValueForProperty('body', newVal);
					else if(propName == 'Header')
						this.parentVM.updateValueForProperty('headers', this.getAllHeaders());					
					else
						this.parentVM.updateValueForProperty(propName, newVal);

					//Form Data is a bit tricky 
					if(propName == 'useFormData' && newVal == true)
						this.parentVM.updateValueForProperty('body', this.getAllFormData());
					else if(propName == 'useFormData' && newVal == false)
						this.parentVM.updateValueForProperty('body', this.rawBody);
					
				}.bind(this), 500);
			}
		}
	},

	init : function(){	
		if (this.embedded) {
			this.set('labelWidth', 60);
			this.set('maxWidth', 166);
			this.set('editorMinHeight', 200);
		}
		this.variableForm.dumper = {
			dump : function(newVariableString){				
				this.fireEvent('insertAtCursor', Ext.String.trim(newVariableString));
			},
			scope : this
		}
		/*this.urlVariableForm.dumper = {
			dump : function(newVariableString){
				var value = Ext.String.trim(this.label) + ' ' + newVariableString;
				this.set('commandUrl', Ext.String.trim(value));
			},
			scope : this
		}
		this.urlVariableForm.cancel = function(){
			this.set('showUrlVariableForm', false);
		}.bind(this)*/
		var syntaxStore = Ext.create('Ext.data.Store',{
			fields: ['name', 'value'],
			data: [{
				name: this.localize('text'),
				value: 'text'
			}, {
				name: this.localize('json'),
				value: 'json'
			}, {
				name: this.localize('xml'),
				value: 'xml'
			}]
		});
		this.set('syntaxStore', syntaxStore);
		this.set('syntax', 'text');		
	},
	enableAddUrlVariable : function(){
		this.set('showUrlVariableForm', !this.showUrlVariableForm);	
	},
	activePayloadType$ : function(){
		return this.useFormData ? 0 : 1;
	},
	methodIsPost$ : function(){
		return this.method.toLowerCase() == 'post';
	},

	resetProperties : function(){
		this.loadData(this.defaultData);
		this.HeaderList.removeAll();
		this.ParamList.removeAll();
		this.FormDataList.removeAll();
		this.addNewHeader();
		this.addNewParam();	
		this.addNewFormData();
		this.set('isPristine', true);
	},
	saveVariableList : function(variableType, data){
		this.allVariables[variableType] = data;
		this.populateVariableList(variableType, data);
		this.variableForm.saveVariableList(variableType,data)
	},
	populateVariableList : function(variabelType, newData){
		this.HeaderList.foreach(function(entry){
			entry.computeStoreData(variabelType, newData);
		})
		this.ParamList.foreach(function(entry){
			entry.computeStoreData(variabelType, newData);
		})
		this.FormDataList.foreach(function(entry){
			entry.computeStoreData(variabelType, newData);
		})
	},
	suspendWaitingForDataChange : function(flag){
		this.set('waitingForDataChange', !flag);
	},

	//Get all current config to save (apply for creating new task in AD)
	getCurrentConfig : function(){
		var config = this.asObject();		
		config['headers'] = this.getAllHeaders();
		config['formData'] = this.getAllFormData();	
		return config;	
	},
	loadConfig : function(saveConfig){
		//Method and URL and body
		this.loadData(saveConfig);

		//Params
		this.removeAllParam();
		this.parseParam(saveConfig['commandUrl']);
		this.addNewParam();

		//Headers
		this.removeAllHeader();
		this.parseHeader(saveConfig['headers']);
		this.addNewHeader();

		//Form Data 
		this.removeAllFormData();
		this.parseFormData(this.useFormData == true ? saveConfig['body'] : saveConfig['formData']);		
		this.addNewFormData();

		if(this.useFormData != true)
			this.set('rawBody', saveConfig['body']);
	},	

	inputParamRegex : /\$INPUT{([a-zA-Z_$][a-zA-Z_$0-9]*)}/g,
	getTaskParams : function(){
		//Save body, method, useFormData and url
		var params = this.asObject();
		delete params['id'];
		
		params['headers'] = this.getAllHeaders();
		//Parse headers to check if any input is used within the headers, the auto generate those inputs.
		var m = null;
		while ((m = this.inputParamRegex.exec(params['headers'])) !== null) {
		    // This is necessary to avoid infinite loops with zero-width matches
		    if (m.index === this.inputParamRegex.lastIndex) {
		        this.inputParamRegex.lastIndex++;
		    }
		    
		    // The result can be accessed through the `m`-variable.
		    m.forEach(function(match, groupIndex){
		       if(groupIndex == 1)
		    		params[match] = ''; //Default to empty (might  allow user to define default in the future)
		    });
		}	
	
		//Parse command to check if any input is used within the command, the auto generate those inputs.
		var m = null;
		while ((m = this.inputParamRegex.exec(this.commandUrl)) !== null) {
		    // This is necessary to avoid infinite loops with zero-width matches
		    if (m.index === this.inputParamRegex.lastIndex) {
		        this.inputParamRegex.lastIndex++;
		    }
		    
		    // The result can be accessed through the `m`-variable.
		    m.forEach(function(match, groupIndex){
		       if(groupIndex == 1)
		    		params[match] = ''; //Default to empty (might  allow user to define default in the future)
		    });
		}	

		if(this.method == 'POST'){
			//If it is form data, then convert to raw before save.
			if(this.useFormData)
				params['body'] = this.getAllFormData();
			else
				params['body'] = this.rawBody;
		}
		
		//Parse body to check if any input is used within the command, the auto generate those inputs.
		var m = null;
		while ((m = this.inputParamRegex.exec(params['body'])) !== null) {
		    // This is necessary to avoid infinite loops with zero-width matches
		    if (m.index === this.inputParamRegex.lastIndex) {
		        this.inputParamRegex.lastIndex++;
		    }
		    
		    // The result can be accessed through the `m`-variable.
		    m.forEach(function(match, groupIndex){
		       if(groupIndex == 1)
		    		params[match] = ''; //Default to empty (might  allow user to define default in the future)
		    });
		}	

		return params;
	},
	
	enableAddVariable : function(){
		this.set('showVariableForm', !this.showVariableForm);	
	},
	addVariableIsHidden$ : function(){
		return !this.embedded && this.showVariableForm;	
	},

	HeaderList: {
		mtype: 'list'
	},
	parseHeader : function(headers){
		var headerList = headers.split(',');
		for(var i = 0; i < headerList.length; i++){
			var headerEntry = headerList[i].split(':') || [];
			if(headerEntry.length == 2){
				var key = headerEntry[0];
				var value = headerEntry[1];
				this.addNewHeader(key, value);
			}			
		}
	},
	addNewHeader : function(key, value){
		var newHeader = this.model({
			mtype : 'RS.wiki.automation.HTTPKeyValEntry',
			containerType : 'Header',
			key : key || '',
			value : value || '',
			isLast : (key || value) ? false : true
		})
		this.HeaderList.add(newHeader);
		for(var variabelType in this.allVariables){
			newHeader.computeStoreData(variabelType, this.allVariables[variabelType]);
		}
		this.fireEvent('HeaderChanged', undefined , undefined , 'Header');
	},
	getAllHeaders : function(){
		var headers = [];
		this.HeaderList.foreach(function(header){
			if(!header.isLast && header.isValid)
				headers.push(header.key + ":" + header.value);
		})
		return headers.join(',');
	},
	removeHeader : function(header){
		this.HeaderList.remove(header);
		this.fireEvent('HeaderChanged', undefined , undefined , 'Header');
	},
	removeAllHeader : function(){
		this.HeaderList.removeAll();
		this.fireEvent('HeaderChanged', undefined , undefined , 'Header');
	},

	ParamList : {
		mtype : 'list'
	},
	parseParam : function(url){
		var urlParts = url.split('?');
		if(urlParts.length == 2){
			var paramList = urlParts[1].split('&');
			for(var i = 0; i < paramList.length; i++){
				var paramEntry = paramList[i].split('=') || [];
				if(paramEntry.length == 2){
					var key = paramEntry[0];
					var value = paramEntry[1];
					this.addNewParam(key, value);
				}				
			}
		}
	},
	updateURLParam : function(){
		var baseURL = this.commandUrl.replace(/\?.*/g, '');		
		this.set('commandUrl', baseURL +  this.getAllParams());
	},
	addNewParam : function(key, value){
		var newParam = this.model({
			mtype : 'RS.wiki.automation.HTTPKeyValEntry',		
			containerType : 'Param',
			key : key || '',
			value : value || '',
			isLast : (key || value) ? false : true
		});
		this.ParamList.add(newParam);
		for(var variabelType in this.allVariables){
			newParam.computeStoreData(variabelType, this.allVariables[variabelType]);
		}
	},
	getAllParams : function(){
		var newParam = this.ParamList.length == 1 ? '' : '?';
		this.ParamList.foreach(function(param){
			if(!param.isLast && param.isValid){				
				if(param && param.key){
					if(newParam.charAt(newParam.length - 1) != '?')
						newParam += "&";
					newParam += param.key + "=" + param.value;
				}
			}
		});
		return newParam;
	},
	removeParam : function(param){
		this.ParamList.remove(param);
		this.updateURLParam();
	},
	removeAllParam : function(){
		this.ParamList.removeAll();
	},

	FormDataList : {
		mtype : 'list'
	},
	parseFormData : function(body){
		var formDataList = body.split('&');
		for(var i = 0; i < formDataList.length; i++){
			var formDataEntry = formDataList[i].split('=') || [];
			if(formDataEntry.length == 2){
				var key = formDataEntry[0];
				var value = formDataEntry[1];
				this.addNewFormData(key, value);
			}			
		}
	},
	addNewFormData : function(key, value){
		var newFormData = this.model({
			mtype : 'RS.wiki.automation.HTTPKeyValEntry',		
			containerType : "FormData",
			key : key || '',
			value : value || '',
			isLast : (key || value) ? false : true
		});
		this.FormDataList.add(newFormData);
		for(var variabelType in this.allVariables){
			newFormData.computeStoreData(variabelType, this.allVariables[variabelType]);
		}
		this.fireEvent('FormDataChanged', undefined, undefined, 'FormData');
	},
	getAllFormData : function(){
		var rawStr = "";
		this.FormDataList.foreach(function(formData){
			if(!formData.isLast && formData.isValid){					
				if(formData && formData.key){
					if(rawStr != "")
						rawStr += "&";
					rawStr += formData.key + "=" + formData.value;
				}
			}
		})
		return rawStr;
	},
	removeFormData : function(param){
		this.FormDataList.remove(param);
		this.fireEvent('FormDataChanged', undefined, undefined, 'FormData');	
	},
	removeAllFormData : function(){
		this.FormDataList.removeAll();
		this.fireEvent('FormDataChanged', undefined, undefined, 'FormData');
	}
})