glu.defModel('RS.wiki.automation.RunbookProperty', {
	mixins: ['LabelAware', 'VersionAware', 'XmlAware', 'ActionTaskValidation'],
	wikiParameters: {
		mtype: 'store',
		fields: ['name', 'type', 'sourceName', 'source', 'fieldTip']
	},
	runbook: '',
	id: '',
	mockList: null,
	paramSource : null,
	extractor: /^\$([^\{\}]+)\{([^\{\}]+)\}$/,
	runbookIsValid: true,
	isPartOfDT : false,	
	versionHistoryStore: {
		mtype: 'store',
		fields: ['documentName', {
			name: 'version',
			convert: function(v, r) {
				return r.raw.version || r.raw.revision;
			}
		}, 'isStable', 'referenced', 'comment', 'createdBy', {
			name: 'sysCreatedOn',
			type: 'date',
			dateFormat: 'time',
			format: 'time'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wiki/getHistory',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	showVersionNumberOption$: function() {
		return this.specificVersion;
	},
	details: function() {
		var me = this;
		this.open({
			mtype: 'RS.wiki.pagebuilder.Revision',
			name : this.runbook,
			uname: this.runbook.split('.')[0],
			id : this.id,
			unamespace: this.runbook.split('.')[1],
			isDetails: true,
			dumper: {
				dump: function(selections) {
					if (selections.length > 0) {
						var selection = selections[0];
						this.set('versionNumber', selection.get('version') || selection.get('revision'));
						this.updateVersionNumber(this.versionNumber);
					}
				},
				scope: this
			}
		});
	},
	initVersionHistoryStore: function() {
		/* TODO - support Version Control
		this.versionHistoryStore.on('beforeload', function(store, op) {
			this.set('reqCount', this.reqCount + 1);
		}, this);
		this.versionHistoryStore.on('load', function(store, recs) {
			if (recs && recs.length) {
				var version = this.cell.getAttribute('version');
				if (Ext.isNumeric(version)) {
					this.set('versionNumber', version);
				} else {
					this.set('versionNumber', recs[0].get('version'));
				}
				this.set('specificVersionIsDisabled', false);
				// Determine if there is a Stable version then enable option
				var latestStableIsDisabled = true;
				for (var i = 0; i< recs.length; i++) {
					if (recs[i].get('isStable')) {
						latestStableIsDisabled = false;
						break;
					}
				}
				this.set('latestStableIsDisabled', latestStableIsDisabled);

				if (!this.latest && !this.latestStable && !this.specificVersion) {
					//// if user has admin role, default to use latestStable
					//if (!this.latestStableIsDisabled && Ext.Array.indexOf(clientVM.user.roles, 'admin') > -1) {
					//	this.set('latest', false);
					//	this.set('latestStable', true);
					//} else {
						this.set('latest', true);
						this.set('latestStable', false);
					//}
				}
			} else {
				this.set('versionNumber', '');
				this.set('latest', true);
				this.set('latestStable', false);
				this.set('specificVersion', false);
				this.set('latestStableIsDisabled', true);
				this.set('specificVersionIsDisabled', true);
			}
			this.set('reqCount', this.reqCount - 1);
		}, this);
		*/
	},
	init: function() {
		this.set('paramSource', Ext.create('Ext.data.Store',{
			fields: [{
				name: 'source',
				convert: function(v, r) {
					return r.raw
				}
			}],
			data:['DEFAULT', 'CONSTANT', 'WSDATA', 'PROPERTY'].concat(this.isPartOfDT ? [] : ['OUTPUT', 'FLOW', 'PARAM'])
		}));		
		this.runbookStore.on('beforeload', function(store, op) {
			op.params = op.params || {};
			Ext.apply(op.params, {
				filter: Ext.encode([{
					field: 'ufullname',
					condition: 'contains',
					type: 'auto',
					value: op.params.query || this.runbook
				}])
			});
			this.set('reqCount', this.reqCount + 1);
			this.set('runbookIsValid', true);
		}, this);
		this.runbookStore.on('load', function() {
			this.validateRunbook(this.runbook);
			this.set('reqCount', this.reqCount - 1);

			var runbook = this.runbookStore.findRecord('ufullname', this.runbook, 0, false, false, true);
			if (runbook) {
				this.set('id', runbook.get('id'));
			}
		}, this)
		this.runbookPropertyStore.on('beforeload', function(store, op) {
			/* TODO - support Version Control
			var version = '';
			if (this.latestStable) {
				version = 'latestStable';
			} else if (this.specificVersion) {
				version = this.versionNumber;
			}
			if (version) {
				Ext.apply(op.params, {
					version: version
				});
			}
			*/
			this.set('reqCount', this.reqCount + 1);
		}, this);
		this.runbookPropertyStore.on('load', function(store, records, successful) {
			if (successful) {
				this.validateRunbook(this.runbook, null, this.runbookPropertyStore);
				this.set('reqCount', this.reqCount - 1);
	
				var runbookProperty = this.runbookPropertyStore.findRecord('ufullname', this.runbook, 0, false, false, true);
				if (runbookProperty) {
					this.set('id', runbookProperty.get('id'));
				}
			} else {
				/* TODO - support Version Control
				// if not successful, we still have to retrieve the version history list
				this.versionHistoryStore.load({
					params: {
						docFullName: this.runbook
					}
				});
				*/ 
			}
		}, this);
		this.parameterStore.on('update', function(store, record, operation, modifiedFieldNames, eOpts) {
			if (!this.cell || this.reqCount > 0)
				return;
			if (modifiedFieldNames.indexOf('source') != -1 && record.get('source').toLowerCase() == 'default')
				record.set('source', 'CONSTANT');
			var inputs = this.getParamsFromStore();
			this.toCDATA(inputs, 'inputs');
		}, this);
		this.parameterStore.on('datachanged', function() {
			if (!this.cell || this.reqCount > 0)
				return;
			var inputs = this.getParamsFromStore();
			this.toCDATA(inputs, 'inputs');
		}, this);
		this.initVersionHistoryStore();
	},
	handleRunbookLabelChange: function(newLabel) {
		this.parentVM.fireEvent('celllabelchanged', this.cell, this.newLabel || '');
	},
	handleRunbookChange : function(newRunbook, prevRunbook){
		this.validateRunbook(newRunbook);
	},
	handleRunbookSelect: function(record) {
		this.resetVersion();
		this.updateRunbookProperty(record[0].get('ufullname'));
	},
	validateRunbook: function(runbook, isNewRunbook, store) {
		var runbookStore = store || this.runbookStore;
		var r = runbookStore.findRecord('ufullname', runbook, 0, false, false, true);
		if (!r) {
			this.set('runbookIsValid', this.localize('invalidRunbook'));
			return;
		}
		this.parameterStore.removeAll();
		this.set('runbookIsValid', true);
		var desc = this.cell.getValue().getAttribute('description');
		if (!isNewRunbook) {
			this.parentVM.fireEvent('selectRunbook', r.raw.ufullname, this.cell);
		}
		/* TODO - support Version Control
		this.versionHistoryStore.load({
			params: {
				docFullName: r.raw.ufullname
			}
		});
		*/ 

		var result = this.parse(this.cell.getValue());
		var modelInputs = [];
		Ext.Object.each(result.modelInputs, function(name, value) {
			var m = null;
			if (this.extractor.test(value))
				m = this.extractor.exec(value);
			modelInputs.push({
				name: name,
				source: m ? m[1] : 'CONSTANT',
				value: m ? m[2] : value
			});
		}, this);
		try {
			var params = Ext.decode(r.raw.uwikiParameters) || [];
			this.wikiParameters.add(params);
			this.parameterStore.add(params);
		} catch (e) {
			//for invalid json
		}
		Ext.each(modelInputs, function(p) {
			var r = this.parameterStore.findRecord('name', p.name);
			if (r) {
				r.set('source', p.source);
				r.set('sourceName', p.value);
				return;
			}
			this.parameterStore.add({
				name: p.name,
				source: p.source.toUpperCase(),
				sourceName: p.value,
				type: 'String'
			});
		}, this);
	},
	parameterStore: {
		mtype: 'store',
		fields: ['name', 'type', 'sourceName', 'source', 'fieldTip']
	},
	runbookStore: {
		mtype: 'store',
		fields: ['ufullname'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wikiadmin/listRunbooks',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	runbookPropertyStore: {
		mtype: 'store',
		fields: ['id', 'ufullname', 'usummary', 'uwikiParameters'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wiki/get',
			reader: {
				type: 'json',
				root: 'data'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	reqCount: 0,
	loading: false,
	cell: null,
	resetProperty: function() {
		this.resetVersion();
		this.set('loading', true);
		this.set('cell', null);
		this.set('reqCount', 0);
		this.set('label', '');
		this.set('runbook', '');
		this.set('id', '');
		this.set('mockList', null);
		this.set('runbookIsValid', true);
		this.wikiParameters.removeAll();
		this.parameterStore.removeAll();
		this.set('loading', false);
	},
	populate: function(cell) {
		this.resetProperty();
		this.populateVersion(cell);
		this.set('loading', true);
		//this.set('reqCount', this.reqCount + 1);
		// var result = this.parse(cell.getValue());
		// Ext.Object.each(result.modelInputs, function(k, v) {
		// 	var src = 'CONSTANT';
		// 	value = v;
		// 	var isConst = !this.extractor.test(v);
		// 	if (!isConst) {
		// 		var m = this.extractor.exec(v);
		// 		var indexParamSrc = this.paramSource.find('source', m[1], 0, false, false, true);
		// 		if ( indexParamSrc == -1)
		// 			src = 'CONSTANT';
		// 		else{
		// 			src = this.paramSource.getAt(indexParamSrc).raw;
		// 			value = m[2];
		// 		}
		// 	}
		// 	this.parameterStore.add({
		// 		name: k,
		// 		source: src,
		// 		sourceName: value,
		// 		fieldTip: ''
		// 	});
		// }, this)
		this.set('cell', cell);
		this.set('label', cell.getAttribute('label'));
		this.set('runbook', cell.getAttribute('description') || cell.getAttribute('label'));
		//this.set('reqCount', this.reqCount - 1);

		//Load to do validation check.
		this.runbookPropertyStore.load({
			params: {
				id: this.id,
				name: this.runbook
			}
		});

		this.checkActionTaskParamDependency();
		this.set('loading', false);
	},	
	getParamsFromStore: function() {
		var params = {};
		this.parameterStore.each(function(r) {
			if (r.get('source') == 'DEFAULT')
				return;
			params[r.get('name')] = r.get('source') == 'CONSTANT' ? r.get('sourceName') : Ext.String.format('${0}\{{1}\}', r.get('source'), r.get('sourceName'));
		}, this)
		return params;
	},
	searchRunbook: function() {
		var me = this;
		this.open({
			mtype: 'RS.wiki.RunbookPicker',
			dumper: {
				dump: function(selections) {
					if (selections.length > 0) {
						var selection = selections[0]
						me.parentVM.fireEvent('selectRunbook', selection.get('ufullname'), me.cell);
						me.cell.setAttribute('description', selection.get('ufullname'));
						me.cell.setAttribute('label', selection.get('ufullname').split('.')[1]);
						me.parentVM.fireEvent('updateSelectedRunbook', [me.cell]);
					}
				}
			}
		})
	},
	jumpToRunbook: function() {
		var params = {
			name: this.runbook,
			activeTab: 2
		};
		/* TODO - support Version Control
		var version = this.cell.getAttribute('version');
		if (version) {
			Ext.apply(params, {
				version: version
			})
		}
		*/
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			target: '_blank',
			params: params
		});
	},
	jumpToRunbookIcon: function() {},
	jumpToRunbookIconIsVisible$: function() {
		return this.runbookIsValid === true;
	},
	executeRunbookIconIsVisible$: function() {
		return this.runbookIsValid === true;
	},
	openExecutionWindow: function(button) {
		//if (this.mockList != null) {
		//	this.doOpenExecutionWindow(button);
		//}
		//else {
			this.rootVM.getMockList(this.id, function(mockList){
				this.set('mockList', mockList);
				this.doOpenExecutionWindow(button);
			}.bind(this));
		//}
	},
	doOpenExecutionWindow: function(button) {
		var me = this;
		if (this.executionWin) {
			this.executionWin.doClose()
		} else {
			this.executionWin = this.open({
				mtype: 'RS.actiontaskbuilder.Execute',
				target: button.getEl().id,
				fromWiki: true,
				inputsLoader: function(paramStore, origParamStore, mockStore) {
					//Load up the wiki params into the grid and the mock values for the wiki
					me.parameterStore.each(function(param) {
						param.data.value = param.get('sourceName');
						param.data.utype = 'PARAM';
						paramStore.add(param);
						origParamStore.add(param);
					}, this);

					var mockNamesArray = [];
					if (me.mockList) {
						for (var i = 0; i < me.mockList.length; i++) {
							mockNamesArray.push({
								uname: me.mockList[i]
							})
						}
					}
					mockStore.loadData(mockNamesArray);
				},
				executeDTO: {
					wiki: me.runbook
				}
			})
			this.executionWin.on('closed', function() {
				this.executionWin = null
			}, this)
		}
	},
	addParam: function() {
		var found = null;
		this.wikiParameters.each(function(param) {
			var idx = this.parameterStore.findExact('name', param.get('name'));
			if (idx == -1) {
				found = param
				return;
			}
			var modelParam = this.parameterStore.getAt(idx);
			modelParam.set('fieldTip', param.get('fieldTip'));
		}, this);
		if (found) {
			this.parameterStore.add(found);
			return;
		}
		var max = 0;
		var reg = /^NewParam(\d*)$/;
		var newParam = 'NewParam';

		this.parameterStore.each(function(param) {
			if (!reg.test(param.get('name')))
				return;
			var m = reg.exec(param.get('name'));
			var idx = parseInt(m[1]);
			if (idx > max)
				max = idx;
		}, this);
		newParam += ((max + 1) || '');

		this.parameterStore.add({
			name: newParam,
			source: 'CONSTANT',
			value: ''
		});

		// clear out all previously selected items
		this.parameterStore.grid.getSelectionModel().deselectAll();

		// determine row of newly added "NewParam"
		var row;
		for (row = this.parameterStore.data.items.length-1; row > 0; row--) {
			if (this.parameterStore.data.items[row].data.name == newParam) {
				break;
			}
		}
		// start editing the newly selected item
		this.parameterStore.grid.getPlugin().startEditByPosition({
			row: row,
			column: 1
		});
	},
	paramsSelections: [],
	removeParams: function() {
		this.parameterStore.remove(this.paramsSelections);
		var inputs = this.getParamsFromStore();
		this.toCDATA(inputs, 'inputs');
	},
	removeParamsIsEnabled$: function() {
		return this.paramsSelections.length > 0;
	},

	updateRunbookProperty: function(runbook) {
		this.parentVM.fireEvent('selectRunbook', runbook, this.cell);
		this.parentVM.fireEvent('updateSelectedRunbook', [this.cell]);
		this.cell.setAttribute('label', runbook.split('.')[1]);
		this.cell.setAttribute('description', runbook);
	}
});