glu.defModel('RS.wiki.automation.ElementPicker', {
	elements: {
		mtype: 'list'
	},
	initElements: function() {},
	init: function() {
		this.initElements();
	}
});
glu.defModel('RS.wiki.automation.ModelElement', {
	elementType: '',
	elementImg: '',
	elementWidth: 0,
	elementHeight: 0,
	elementDisplayType$: function() {
		return this.localize(this.elementType);
	}
});

glu.defModel('RS.wiki.automation.ConnectionElement', {
	elementType: '',
	elementImg: '',
	elementWidth: 0,
	elementHeight: 0,
	elementDisplayType$: function() {
		return this.localize(this.elementType);
	}
});