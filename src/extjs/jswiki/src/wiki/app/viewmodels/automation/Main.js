glu.defModel('RS.wiki.automation.Main', {
	cards: {
		mtype: 'activatorlist',
		focusProperty: 'activeItem',
		autoParent: true
	},
	mainModel: {
		mtype: 'MainModel'
	},
	decisionTree: {
		mtype: 'DecisionTree'
	},
	abortModel: {
		mtype: 'AbortModel'
	},
	activeItem: 0,
	collapsed: false,
	dataIsReady : false,
	waitingForData : false,
	viewOnly: false,
	when_data_is_ready_reactive_dirtyFlag : {
		on : ['dataIsReadyChanged'],
		action : function(){
			if(this.dataIsReady && this.waitingForData)
				this.startDirtyFlagChecking();
		}
	},

	API : {
		validateTask : '/resolve/service/wiki/model/validate',
		saveTask : '/resolve/service/actiontask/save',
		getAllTags : '/resolve/service/tag/listTags',
		getAllATProperties : '/resolve/service/atproperties/list'
	},

	init: function() {
		if (this.rootVM.ns === 'RS.wiki') {
			this.cards.add(this.mainModel);
			this.cards.add(this.decisionTree);
			this.cards.add(this.abortModel);
			this.cards.setActiveItem(this.mainModel);
			if (this.parentVM) {
				this.parentVM.on('umodelProcessChanged', function() {
					this.mainModel.set('xml', this.parentVM.umodelProcess);
				}, this);
				this.parentVM.on('umodelExceptionChanged', function() {
					this.abortModel.set('xml', this.parentVM.umodelException);
				}, this);
				this.parentVM.on('udecisionTreeChanged', function() {
					this.decisionTree.set('xml', this.parentVM.udecisionTree);
				}, this);
	
				this.mainModel.on('xmlChanged', function() {
					this.parentVM.set('umodelProcess', this.mainModel.xml);
				}, this);
				this.abortModel.on('xmlChanged', function() {
					this.parentVM.set('umodelException', this.abortModel.xml);
				}, this);
				this.decisionTree.on('xmlChanged', function() {
					this.parentVM.set('udecisionTree', this.decisionTree.xml);
				}, this);
			}
	
			if (!this.rootVM.allTags.length && !this.rootVM.getAllTagsInProgress) {
				this.getAllTags(function(records){
					this.rootVM.set('allTags', records);
				}.bind(this))
			}
	
			if (!this.rootVM.allATProperties.length && !this.rootVM.getAllATPropertiesInProgress) {
				this.getAllATProperties(function(records){
					this.rootVM.set('allATProperties', records);
				}.bind(this))
			}

			if (window.mobile) {
				this.set('collapsed', true);
			}
		}
	},
	mainModelTab: function() {
		this.cards.setActiveItem(this.mainModel);
		this.activeItem.updatePropertiesModelChanged()
		this.startDirtyFlagChecking();
	},
	mainModelTabIsPressed$: function() {
		return this.activeItem == this.mainModel;
	},
	decisionTreeTab: function() {
		this.cards.setActiveItem(this.decisionTree);
		this.activeItem.updatePropertiesModelChanged()
		this.startDirtyFlagChecking();
	},
	decisionTreeTabIsPressed$: function() {
		return this.activeItem == this.decisionTree;
	},
	abortModelTab: function() {
		this.cards.setActiveItem(this.abortModel);
		this.activeItem.updatePropertiesModelChanged()
		this.startDirtyFlagChecking();
	},
	abortModelTabIsPressed$: function() {
		return this.activeItem == this.abortModel;
	},
	showXml: function() {},
	builder: function() {},
	builderIsVisible$: function() {
		return this.activeItem == this.mainModel;
	},
	validate: function(nextAction, scope) {
		this.ajax({
			url: this.API['validateTask'],
			params: {
				main: this.mainModel.xml,
				abort: this.abortModel.xml,
				decisionTree: this.decisionTree.xml
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				nextAction.call(scope || this, respData);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
				nextAction.call(scope || this, {
					success: false
				})
			}
		})
	},
	params: function() {},
	xmlTab: function() {},
	saveAutomation: function() {
		this.parentVM.save();
	},

	afterRender: function() {
		Ext.Ajax.request({
			scope: this,
			url: '/resolve/service/wiki/listAutomationImages',
			success: function(resp, opts) {
				var respData = RS.common.parsePayload(resp);
				if (respData.success){
					var recs= respData.records,
						imgPickers = [
						this.mainModel.mainModelImagePicker,
						this.abortModel.abortModelImagePicker,
						this.decisionTree.decisionTreeImagePicker
						],
						tokens = [];
					imgPickers.forEach(function(imgPicker) {
						if (recs) {
							for (var i=0; i<recs.length; i++) {
								var model = glu.model('RS.wiki.automation.ModelImage', {
									elementType: recs[i].substring(0, recs[i].lastIndexOf('.')),
									elementImg: '/resolve/jsp/model/images/resolve/custom/'+encodeURIComponent(recs[i]),
									elementWidth: 50,
									elementHeight: 50,
									customImg: true
								});
								imgPicker.elements.add(model);
								model.on('imageClicked', imgPicker.imageClicked, imgPicker);
							}
						}
					});
				} else {
					clientVM.displayError(RS.wiki.automation.locale.failedLoadingImage, null, RS.common.locale.failed);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	stopDirtyFlagChecking : function(){
		this.cards.foreach(function(model){
			model.set('isDirtyFlagCheckingOn', false);
		})
		this.set('dataIsReady', false);
		this.set('waitingForData', false);
	},
	startDirtyFlagChecking : function(){
		//Handle the case where data could come back after the tab is already activated.
		if(this.dataIsReady){
			//Deactivate dirty flag checking for all model. Then start the current selected model.
			this.cards.foreach(function(model){
				model.set('isDirtyFlagCheckingOn', false);
			})
			var activeModel = this.cards.getActiveItem();
			activeModel.set('isDirtyFlagCheckingOn', true);
			activeModel.set('originalXml', activeModel.xml);
		}
		else {
			this.set('waitingForData', true);
		}
	},

	activeActionTaskCleanUpCheck: function(nextAction) {
		if (this.activeItem.activeItem.viewmodelName == 'NewTask') {
			this.activeItem.activeItem.saveTaskWizardProperties();
		}
		if (Ext.isFunction(nextAction)) {
			nextAction();
		}
	},

	updateActiveActionTaskEditPermission: function(actiontaskbuilder, delay) {
		if (actiontaskbuilder && actiontaskbuilder.id && this.parentVM.automationViewTabIsPressed) {
			if (delay) {
				setTimeout(function() {
					actiontaskbuilder.disableEditLinks();
				}.bind(this), 100);
			}
			else {
				actiontaskbuilder.disableEditLinks();
			}
		}
	},

	actiontaskContents: {},
	getActionTaskContent: function(taskName, version) {
		var actiontaskContents = this.actiontaskContents || {};
		return actiontaskContents[taskName + ':' + (version || 'latest')];
	},
	setActionTaskContent: function(taskName, version, content) {
		this.actiontaskContents[taskName + ':' + (version || 'latest')] = content;
	},

	resetActionTasksContent: function() {
		delete this.actiontaskContents;
		this.actiontaskContents = {};
	},
	resetActionTaskList: function() {
		this.resetActionTasksContent();
		if (this.rootVM.ns === 'RS.wiki') {
			this.activeItem.fireEvent('clearHistory');
			this.clearXmlDirty();
		}
	},
	getAllTags: function (onSuccess, onFailure) {
		this.rootVM.set('getAllTagsInProgress', true);
		this.ajax({
			url: this.API['getAllTags'],
			method: 'get',
			scope: this,
			success: function (response) {
				var payload = RS.common.parsePayload(response);

				if (payload.success) {
					if (Ext.isFunction(onSuccess)) {
						onSuccess(payload.records);
					}
				} else {
					//clientVM.displayError(this.localize('serverError', payload.message));
					if (Ext.isFunction(onFailure)) {
						onFailure(response);
					}
				}
			},
			failure: function(response) {
				clientVM.displayFailure(response);
				if (Ext.isFunction(onFailure)) {
					onFailure(response);
				}
			},
			callback: function() {
				this.rootVM.set('getAllTagsInProgress', false);
			}
		});
	},

	getAllATProperties: function(onSuccess, onFailure) {
		this.rootVM.set('getAllATPropertiesInProgress', true);
		this.ajax({
			url: this.API['getAllATProperties'],
			scope: this,
			success : function (response){
				var payload = RS.common.parsePayload(response);

				if (payload.success) {
					if (Ext.isFunction(onSuccess)) {
						onSuccess(payload.records);
					}
				} else {
					//clientVM.displayError(this.localize('serverError', payload.message));
					if (Ext.isFunction(onFailure)) {
						onFailure(response);
					}
				}
			},
			failure: function(response) {
				clientVM.displayFailure(response);
				if (Ext.isFunction(onFailure)) {
					onFailure(response);
				}
			},
			callback: function() {
				this.rootVM.set('getAllATPropertiesInProgress', false);
			}
		});
	},
	xmlDirtyFlag: false,
	setXmlDirty: function() {
		this.set('xmlDirtyFlag', true);
	},
	clearXmlDirty: function() {
		this.set('xmlDirtyFlag', false);
	}

});
