glu.defModel('RS.wiki.automation.HTTPKeyValEntry', {	
	key: '',
	value: '',
	isLast : true, //should apply for the last row which is empty row,
	isValid : true,
	containerType : '',
	keyQueryMode : 'remote',
	regex : /\$([a-zA-z_0-9]*)\{?([a-zA-z_0-9]*)/g,
	variableTypes : ['INPUT','OUTPUT','PARAM','PROPERTY','WSDATA','FLOW'],
	inputData : [],
	outputData : [],
	wsdataData : [],
	flowData : [],
	paramData : [],
	propertyData : [],
	genericData : [],
	HTTPCommonHeader : [
	'Accept',
	'Accept-Charset',
	'Accept-Encoding',
	'Accept-Language',
	'Accept-Datetime',
	'Authorization',
	'Cache-Control',
	'Connection',
	'Cookie',
	'Content-Length',
	'Content-MD5',
	'Content-Type',
	'Date',
	'Expect',
	'Forwarded',
	'From',
	'Host',
	'If-Match',
	'If-Modified-Since',
	'If-None-Match',
	'If-Range',
	'If-Unmodified-Since',
	'Max-Forwards',
	'Origin',
	'Pragma',
	'Proxy-Authorization',
	'Range',
	'Referer',
	'TE',
	'User-Agent',
	'Upgrade',
	'Via',
	'Warning'],
	keySuggestionStore : {
		mtype : 'store',
		proxy : {
			type : 'memory'
		},
		fields : ['text','value','name']		
	},
	valueSuggestionStore : {
		mtype : 'store',
		proxy : {
			type : 'memory'
		},
		fields : ['text','value','name']		
	},
	
	update_url_param: {
		on :['keyChanged', 'valueChanged'],
		action : function(newVal,oldVal, bindingInfo){			
			if(this.delayTask)
				clearTimeout(this.delayTask);			
			this.delayTask = setTimeout(function(){			
				if(bindingInfo['modelPropName'] == 'key')			
					this.set('isValid', this.isKeyValid(newVal));			
					
				if(this.isValid){			
					if(this.containerType == 'Param' && this.key != '')
						this.parentVM.updateURLParam();
					else
						this.parentVM.fireEvent(this.containerType + 'Changed', undefined, undefined, this.containerType);
				}
				
			}.bind(this),500)
		}		
	},	
	init : function(){	
		for(var i = 0; i < this.variableTypes.length; i++){
			this.genericData.push({
				text : '$' + this.variableTypes[i].toUpperCase() + '{<b><i>variable_name</b></i>}',
				value : '$' + this.variableTypes[i].toUpperCase() + '{variable_name}',
				name : 'variable_name'
			})
		}
		this.keySuggestionStore.loadData(this.genericData);
		this.valueSuggestionStore.loadData(this.genericData);
		this.keySuggestionStore.on('load', function(){
			this.parseSuggestionsForStore('key');
		}.bind(this));
		this.valueSuggestionStore.on('load', function(){			
			this.parseSuggestionsForStore('value');
		}.bind(this));

		//For HTTP Header
		if(this.containerType == 'Header'){
			var httpHeaderData = [];
			for(var i =0; i < this.HTTPCommonHeader.length; i++){
				var name = this.HTTPCommonHeader[i];
				httpHeaderData.push({
					text : name,
					value : name,
					name : name
				})
			}
			this.keySuggestionStore.loadData(httpHeaderData);
			this.set('keyQueryMode', 'local');
		}		
	},	
	isKeyValid : function(key){
		var parentConntainer  = this.parentVM[this.containerType + 'List'];
		var found = false;
		parentConntainer.each(function(entry){
			if(entry != this && entry.isValid && entry.key == key)
				found = true;
		}, this)
		return !found;		
	},
	parseSuggestionsForStore : function(fieldName){
		this[fieldName + 'SuggestionStore'].removeAll();
		var storeData = [];
		var variableTypeKeyword = null;
		var nameKeyword = null;
		var m = null;
		while ((m = this.regex.exec(this[fieldName])) !== null) {
		    // This is necessary to avoid infinite loops with zero-width matches
		    if (m.index === this.regex.lastIndex) {
		        this.regex.lastIndex++;
		    }
		    
		    // The result can be accessed through the `m`-variable.
		    m.forEach(function(match, groupIndex){
	      		if(groupIndex == 1)
		      		variableTypeKeyword = match;
		      	else if(groupIndex == 2)
		      		nameKeyword = match;
		    });		   
		}
		if(variableTypeKeyword == "")
			storeData = this.genericData;
		else if(variableTypeKeyword != null) {
			for(var i = 0; i < this.variableTypes.length; i++){
				var variableType = this.variableTypes[i];
				if(variableType.indexOf(variableTypeKeyword.toUpperCase()) != -1){
					var data = this[variableType.toLowerCase() + 'Data'].length > 0 ? 
						this[variableType.toLowerCase() + 'Data'] : 
						[{
							text : '$' + variableType + '{<b><i>variable_name</b></i>}',
							value : '$' + variableType + '{variable_name}',
							name : 'variable_name'
						}];
					storeData = storeData.concat(data);
				}
			}
		}

		//Filter data by name
		var filteredStoreData = this.filterByName(storeData,nameKeyword);
		this[fieldName + 'SuggestionStore'].loadData(filteredStoreData);
	},
	filterByName : function(storeData,nameKeyword){
		var data = [];
		for(var i = 0; i < storeData.length; i++){
			if(storeData[i]['name'].toLowerCase().indexOf(nameKeyword.toLowerCase()) != -1)
				data.push(storeData[i]);
		}
		return data;
	},
	computeStoreData : function(variableType, data){
		if(this.variableTypes.indexOf(variableType) == -1)
			return;

		var computedData = [];
		for(var i = 0; i < data.length; i++){
			var name = data[i];
			computedData.push({
				text : '$' + variableType + '{<b><i>' + name + '</b></i>}',
				value : '$' + variableType + '{' + name + '}',
				name : name
			})
		}

		this.set(variableType.toLowerCase() + 'Data', computedData);
	},
	remove: function() {
		this.parentVM['remove' + this.containerType](this);
	},
	focusHandler : function(){	
		var parentConntainer  = this.parentVM[this.containerType + 'List'];
		//Add new row of header if last row is focused.
		if(parentConntainer.indexOf(this) == (parentConntainer.length - 1)){
			this.set('isLast', false);
			this.parentVM['addNew' + this.containerType]();
		}
	}	
});