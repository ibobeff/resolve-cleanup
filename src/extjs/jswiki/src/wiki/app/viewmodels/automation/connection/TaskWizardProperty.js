glu.defModel('RS.wiki.automation.TaskWizardProperty',{
	connectionType : null,
	fields : ['uname','unamespace',{
		name : 'taskId',
		mapping : 'id'
	},{
		name : 'resolveActionInvocOptions',
		mapping : 'resolveActionInvoc.resolveActionInvocOptions',
	},{
		name : 'myContentScript',
		convert : function(value,model){
			var resolveActionInvocOptions = model.get('resolveActionInvocOptions');
			if(resolveActionInvocOptions && resolveActionInvocOptions.length > 0){
				for(var i = 0; i < resolveActionInvocOptions.length; i++){
					var optionName = resolveActionInvocOptions[i]['uname'];
					if(optionName == 'INPUTFILE'){
						return resolveActionInvocOptions[i]['uvalue'];
					}
				}
			}
			else
				return '';
		}
	},{
		name : 'inputParams',
		mapping : 'resolveActionInvoc.resolveActionParameters',
		convert : function(value,model){
			var inputParams = [];
			for(var i = 0; i < value.length; i++){
				var param = value[i];
				if(param['utype'] == "INPUT")
					inputParams.push(param);
			}
			return inputParams;
		}
	}],
	defaultData : {
		uname : '',
		unamespace : '',
		myContentScript : ''
	},
	autoGenCode : '',	
	API : {
		getAT : '/resolve/service/actiontask/get'	
	},
	usingMyScript : true,
	activePropertyTab : 0,
	contentControl : 0,
	savedContentScript : '',
	scriptChoiceOptionIsHidden : true,
	revertMyCodeIsHidden : true,
	templateScriptIsHidden : true,
	taskPayload : null,
	prompt : '', //Only for SSH and Telnet task
	when_content_control_changed : {
		on : ['contentControlChanged'],
		action : function(){
			switch (this.contentControl){
				//Overwrite Code
				case 1 : 
					this.set('scriptChoiceOptionIsHidden', true);
					this.set('revertMyCodeIsHidden', false);
					this.set('templateScriptIsHidden', true);
					break;
				//User handles merge
				case 2 : 
					this.set('scriptChoiceOptionIsHidden', false);
					this.set('revertMyCodeIsHidden', true);
					this.set('templateScriptIsHidden', false);
					break;
				default : 
					this.set('scriptChoiceOptionIsHidden', true);
					this.set('revertMyCodeIsHidden', true);
					this.set('templateScriptIsHidden', true);
			}
		}
	},
	editParamFormVisibility$ : function(){
		return this.editParamFormIsHidden ? 'hidden' : 'visible';
	},
	variableFormVisibility$ : function(){
		return this.paramTypeIsCustom && !this.editParamFormIsHidden? 'visible' : 'hidden';
	},

	init : function(){
		this.variableForm.dumper = {
			dump : function(newVariableString){
				var value = Ext.String.trim(this.command) + ' ' + newVariableString;
				this.set('paramValue', Ext.String.trim(value));
			},
			scope : this
		}
		this.inputStore.on('datachanged', function(){
			this.saveVariableList('INPUT', this.inputStore.collect('name'));		
		}.bind(this));
	},
	saveVariableList : function(variableType, data){
		this.allVariables[variableType] = data;
		this.parameterWizard.saveVariableList(variableType,data);
		this.variableForm.saveVariableList(variableType,data);
	},
	updatePrompt : function(newPrompt){	
		this.set('prompt', newPrompt);
	},	
	getTaskInfo : function(){
		return {
			name : this.uname,
			namespace : this.unamespace,
			params : this.getTaskParams()
		}
	},
	getTaskParams : function(defaultIncluded){
		var params = {};
		this.inputStore.each(function(param){
			var paramName = param.get('name');
			var variableType = param.get('source');
			var variableName = param.get('value');
			if(defaultIncluded || variableType != 'DEFAULT'){
				params[paramName] = (variableType == "CONSTANT" || variableType == "DEFAULT") ? variableName : Ext.String.format('${0}\{{1}\}', variableType , variableName);
			}
		},this)
		if(this.connectionType != 'http')
			params['prompt'] = this.prompt;	
		return params;
	},
	getTaskPayload : function(){
		//Only Content code and action task type will need to be updated.
		var taskPayload = this.get('taskPayload');
		var resolveActionInvocOptions = taskPayload['resolveActionInvoc']['resolveActionInvocOptions'];
		if(resolveActionInvocOptions && resolveActionInvocOptions.length > 0){
			for(var i = 0; i < resolveActionInvocOptions.length; i++){
				var optionName = resolveActionInvocOptions[i]['uname'];
				if(optionName == 'INPUTFILE'){
					resolveActionInvocOptions[i]['uvalue'] = this.usingMyScript ? this.myContentScript : this.autoGenCode;
				}
			}
		}
		else {
			resolveActionInvocOptions = [{
				id : '',
				udescription : '',
				uname : 'INPUTFILE',
				uvalue : this.usingMyScript ? this.myContentScript : this.autoGenCode
			}]
		}		
		taskPayload['resolveActionInvoc']['utype'] = 'remote';
		return taskPayload;
	},
	openTaskPicker : function(){
		this.open({
			mtype : 'RS.actiontask.ActionTaskPicker',			
			dumper : {
				dump : function(selectedRecords){
					var selectedTask = selectedRecords[0];	
					var taskSysId = selectedTask.get('id');
					this.getTaskData(taskSysId);
				},
				scope : this
			}
		})
	},
	getTaskData : function(taskSysId){
		this.ajax({
			url: this.API['getAT'],
			method: 'post',
			params: {
				id: taskSysId,
				name : ''
			},
			scope: this,
			success: function (response) {
				var payload = RS.common.parsePayload(response);					
				if (payload.success) {
					this.populateTaskProperty(payload.data);
					this.set('taskPayload', payload.data);
				} else {
					clientVM.displayError(payload.message);
				}
			},
			failure: function(response) {
				clientVM.displayFailure(response);

				if (typeof onFailure === 'function') {
					onFailure(response);
				}
			}
		})
	},
	clearPreviousTaskData : function(){
		this.loadData(this.defaultData);	
		this.set('usingMyScript', true);
		this.set('contentControl', 0)
	},
	populateTaskProperty : function(selectedTask){
		if(this.taskId){
			var currentShowState = this.showOptionIsTable;
			this.set('showOptionIsTable', true);
			this.showOptionStateMap[this.taskId] = currentShowState;
		}
		this.clearPreviousTaskData();
		this.loadData(selectedTask);
		this.loadParams(this.inputParams);
		this.mergeCodeContent();

		//Handle show option
		if(!this.isTaskCreatedFromWizard(selectedTask.wizardType))		
			this.set('taskHasNoWizard', true);		
		else {
			this.set('taskHasNoWizard', false);			
			this.parameterWizard.updateWizardType(selectedTask.wizardType);		
			if(!this.showOptionStateMap.hasOwnProperty(this.taskId))			
				this.showOptionStateMap[this.taskId] = false;			
			this.set('showOptionIsTable', this.showOptionStateMap[this.taskId]);			
		}
	},
	taskFullName$ : function(){
		return this.uname + "#" + this.unamespace;
	},
	
	//Content Script Logic
	revertMyCode : function(){
		this.set('myContentScript', this.savedContentScript);
	},	
	updateAutoGenCode : function(newCode){
		this.set('autoGenCode', newCode);
	},
	mergeCodeContent : function(){
		//Empty script then just insert the template code.
		if(!this.myContentScript || this.myContentScript == "" || this.myContentScript == this.autoGenCode){
			this.set('myContentScript', this.autoGenCode);
			this.set('contentControl', 0)
			this.set('activePropertyTab', 0);
		}
		else {
			//Code Conflict
			this.set('activePropertyTab', 1);
			setTimeout(function(){
				this.message({
					closable : false,
					title : this.localize('contentScriptConflictTitle'),
					msg : this.localize('contentScriptConflictMsg'),			
					buttonText : {
						yes : this.localize('overwrite'),
						ok : this.localize('userHandle')
					},
					scope : this,
					fn : function(btn){
						if(btn == 'yes'){
							this.set('savedContentScript', this.myContentScript);
							this.set('myContentScript', this.autoGenCode);
							this.set('contentControl', 1);
						}
						else if (btn == 'ok'){
							this.set('contentControl', 2);
						}
					}
				})
			}.bind(this),0)			
		}
	},

	//Parameter
	parameterSelections: [],
	paramName : '',	
	paramValue : '',
	paramDescription : '',
	paramDefaultValue : '',
	variableType : 'CONSTANT',
	variableName : '',
	editParamFormIsHidden : true,
	paramType : 'source',
	paramHasDefault : true,
	allVariables : {
		INPUT : [],
		OUTPUT : [],
		FLOW : [],
		WSDATA : [],
		PROPERTY : [],
		PARAM : [],
		CONSTANT : []
	},		
	extractor: /^\$([^\{\}]+)\{([^\{\}]+)*\}$/,
	inputStore: {
		mtype: 'store',
		groupField: 'isDefault',
		fields: ['name', 'source', 'value', 'order', {
			name: 'encrypt',
			type: 'bool'
		}, 'description', {
			name: 'isDirty',
			type: 'bool'
		}, {
			name: 'isDefault',
			type: 'bool'
		}, 'defaultValue']		
	},
	variableTypeStore: {
		mtype: 'store',
		fields: [{
			name: 'type',
			convert: function(v, r) {
				return r.raw
			}
		}],
		data: ['CONSTANT','INPUT','OUTPUT','FLOW', 'WSDATA', 'PARAM', 'PROPERTY']
	},	
	variableDataStore : {
		mtype : 'store',
		fields: [{
			name: 'name',
			convert: function(v, r) {
				return r.raw
			}
		}]		
	},		
	when_variable_type_changed : {
		on : ['variableTypeChanged'],
		action : function(newVal){
			this.set('variableName', '');		
			this.variableDataStore.loadData(this.allVariables[newVal]);
		}
	},
	on_parameter_selection_changed : {
		on : ['parameterSelectionsChanged'],
		action : function(){
			this.set('editParamFormIsHidden', true);
		}
	},	
	variableForm : {
		mtype : 'VariableSubstitution',
		cancelIsVisible : false		
	},	
	loadParams : function(defaultParams){
		var inputs = [];
		Ext.each(defaultParams, function(v) {
			inputs.push({
				name: v.uname,
				source: 'DEFAULT',
				encrypt: v.uencrypt || false,
				protected: v.uprotected || false,
				value: v.udefaultValue || '',
				description: v.udescription || '',
				order: v.uorder,			
				isDefault: true,
				isDirty: false,
				defaultValue: v.udefaultValue || ''
			});
		}, this);
		this.inputStore.loadData(inputs);
		this.inputStore.sort([{
			property: 'isDefault',
			direction: 'DESC'
		}, {
			property: 'order',
			direction: 'ASC'
		}]);
	},
	addParam : function(){	
		var max = 0;
		var reg = /^NewParam(\d*)$/;
		var newParam = 'NewParam';
		this.inputStore.each(function(param) {
			if (!reg.test(param.get('name')))
				return;
			var m = reg.exec(param.get('name'));
			var idx = parseInt(m[1]);
			if (idx > max)
				max = idx;
		}, this);
		newParam += ((max + 1) || '');

		var newParam = this.inputStore.add({
			name: newParam,
			source: 'CONSTANT',
			value : ''
		});
		this.fireEvent('selectNewRecord', newParam);
		this.set('parameterSelections', newParam);
		this.editParam();
	},	
	editParam : function(){	
		this.set('paramName', this.parameterSelections[0].get('name'));
		this.set('paramDescription', this.parameterSelections[0].get('description'));
		this.set('paramHasDefault', this.parameterSelections[0].get('isDefault'));
		this.set('paramDefaultValue', this.parameterSelections[0].get('defaultValue'));		
		var paramType = this.parameterSelections[0].get('source') == 'DEFAULT' ? 'default' : 'source';
		if(paramType == 'default'){
			var paramSrc = 'CONSTANT';
			var value = this.parameterSelections[0].get('value');
			var m = this.extractor.exec(value);
			var indexParamSrc = m ? this.variableTypeStore.find('type', m[1], 0, false, false, true) : -1;
			if (indexParamSrc != -1){
				paramSrc = this.variableTypeStore.getAt(indexParamSrc).raw;
				value = m[2];
			}
			this.set('variableType', paramSrc);
			this.set('variableName', value);
		}
		else {
			this.set('variableType', this.parameterSelections[0].get('source'));
			this.set('variableName', this.parameterSelections[0].get('value'));
		}
		this.fireEvent('selectParamType', paramType);
		this.set('editParamIsEnabled',false);
		this.set('editParamFormIsHidden', false);
	},
	updateParam : function(){
		var currentRecord = this.parameterSelections[0];
		currentRecord.set('name', this.paramName);	
		if(this.paramType == 'default'){
			currentRecord.set('value', this.paramDefaultValue);
			currentRecord.set('isDirty', false);
			currentRecord.set('source', 'DEFAULT');
		}
		else if (this.paramType == 'source') {
			currentRecord.set('source', this.variableType);
			currentRecord.set('value', this.variableName);
			currentRecord.set('isDirty', true);
		}
		else {
			currentRecord.set('source', 'CONSTANT');
			currentRecord.set('value', this.paramValue);
			currentRecord.set('isDirty', true);
		}
		this.set('editParamIsEnabled', true);
		this.set('editParamFormIsHidden', true);
	},
	updateValueForProperty : function(propName, value){
		var record = this.inputStore.findRecord('name', propName);
		if(record){			
			var src = 'CONSTANT';
			var isConst = !this.extractor.test(value);
			if (!isConst) {
				var m = this.extractor.exec(value);
				var paramSrc = this.variableType.findRecord('source', m[1], 0, false, false, true);
				if (paramSrc){
					src = paramSrc.raw;
					value = m[2];
				}
			}
			record.set('value', value.toString());
			record.set('source', src);
			record.set('isDirty', true);	
		}
	},
	editParamIsEnabled$ : function(){
		return this.parameterSelections.length > 0;
	},
	removeParam : function(){
		this.inputStore.remove(this.parameterSelections);
	},
	removeParamIsEnabled$ : function(){
		return this.parameterSelections.length > 0 && !this.parameterSelections[0].get('isDefault');
	},
	
	paramTypeIsDefault$ : function(){
		return this.paramType == 'default';
	},
	paramTypeIsSource$ : function(){
		return this.paramType == 'source';
	},
	paramTypeIsCustom$ : function(){
		return this.paramType == 'custom';
	},
	
	variableIsText$ : function(){
		return this.variableType == "CONSTANT" || this.variableType == "WSDATA" || this.variableType == "FLOW";
	},

	//Wizard View
	showOptionIsTable : true,
	showOptionStateMap : {},
	taskHasNoWizard : true,	
	when_show_option_changed : {
		on : ['showOptionIsTableChanged'],
		action : function(newVal){
			this.showOptionStateMap[this.taskId] = newVal;
			if(!this.showOptionIsTable)			
				this.parameterWizard.loadWizardConfig(this.taskId, this.getTaskParams(true));		
			else
				this.parameterWizard.saveCurrentWizardConfig(this.taskId);			
		}
	},	
	parameterWizard : {
		mtype : 'ParameterWizard'
	},
	isTaskCreatedFromWizard : function(wizardType){
		return ['http'/*, 'local', 'ssh', 'telnet'*/].indexOf(wizardType) != -1;
	}
})