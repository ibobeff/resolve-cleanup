glu.defModel('RS.wiki.automation.ImagePicker', {
	elements: {
		mtype: 'list'
	},

	currentFilterVal: '',
	currentFilterType: '',
	collapsed: true,
	comboStore: null,
	addImgBtntooltip: 'Add Image',

	removeBtnDisabled: true,

	initElements: function() {},

	init: function() {
		this.currentFilterType = this.localize('all');
		this.set('comboStore', new Ext.data.Store({
			data:  [{
				type: this.localize('all')
			}, {
				type: this.localize('default')
			}, {
				type: this.localize('custom')
			}],
			fields: ['type']
		}));
		this.initElements();
	},

	launchImgPicker: function() {
		this.open({
			mtype: 'RS.wiki.automation.ImageUploader',
			images: this.elements
		});
	},

	filterChange: function(val) {
		if (!this.task) {
			this.task = new Ext.util.DelayedTask();;
		}

		this.task.delay(500, this.filterImages, this, arguments);
	},

	filterImages: function(val) {
		this.currentFilterVal = val;
		for(var i=0; i<this.elements.length; i++) {
			var elem = this.elements.getAt(i);
			var label = elem.customImg? elem.elementType: this.localize(elem.elementType);
			if (this.currentFilterType == this.localize('all')) {
				// Apply both predefined type and user type filter
				elem.set('hidden', !label.toLowerCase().startsWith(val.toLowerCase()));
			} else if (this.currentFilterType == this.localize('custom')) {
				// Apply both predefined type and user type filter
				elem.set('hidden', !label.toLowerCase().startsWith(val.toLowerCase()) || !elem.customImg);
			} else if (this.currentFilterType == this.localize('default')) {
				// Apply both predefined type and user type filter
				elem.set('hidden', !label.toLowerCase().startsWith(val.toLowerCase()) || elem.customImg);
			}
		}
	},
	filterByType: function(records) {
		this.currentFilterType = records[0].get('type');
		if (records[0].get('type') == this.localize('all')) {
			for(var i=0; i<this.elements.length; i++) {
				var elem = this.elements.getAt(i);
				var label = elem.customImg? elem.elementType: this.localize(elem.elementType);
				// Apply both predefined type and user typed filter
				elem.set('hidden',
					false || (this.currentFilterVal? !label.toLowerCase().startsWith(this.currentFilterVal.toLowerCase()): false));
			}
		} else if (records[0].get('type') == this.localize('custom')) {
			for(var i=0; i<this.elements.length; i++) {
				var elem = this.elements.getAt(i);
				var label = elem.customImg? elem.elementType: this.localize(elem.elementType);
				// Apply both predefined type and user type filter
				elem.set('hidden',
					!elem.customImg || (this.currentFilterVal? !label.toLowerCase().startsWith(this.currentFilterVal.toLowerCase()): false));
			}
		} else if (records[0].get('type') == this.localize('default')){
			for(var i=0; i<this.elements.length; i++) {
				var elem = this.elements.getAt(i);
				var label = elem.customImg? elem.elementType: this.localize(elem.elementType);
				// Apply both predefined type and user type filter
				elem.set('hidden',
					elem.customImg || (this.currentFilterVal? !label.toLowerCase().startsWith(this.currentFilterVal.toLowerCase()): false));
			}
		}
	},
	imagesRendered: function(p) {
		//p.down('#addImageBtn').setTooltip(this.localize('addImage'));
	},

	removeSelectedImages: function() {
		for(var i=0; i<this.elements.length; i++) {
			var image = this.elements.getAt(i);
			if (image.selectedCss == 'selected') {
				var imgExt = image.elementImg.substring(image.elementImg.lastIndexOf('.')+1);
				Ext.Ajax.request({
					url: '/resolve/service/wiki/deleteAutomationImage',
					method: 'POST',
					params: {
						imageName: image.elementType + (imgExt? '.' + imgExt: '')
					},
					scope: this,
					success: function(resp, opts) {
						var respData = RS.common.parsePayload(resp);
						if (respData.success){
							var imgPickers = [
									this.parentVM.parentVM.mainModel.mainModelImagePicker,
									this.parentVM.parentVM.abortModel.abortModelImagePicker,
									this.parentVM.parentVM.decisionTree.decisionTreeImagePicker
								],
								idx = this.elements.indexOf(image)
							imgPickers.forEach(function(imgPicker) {
								imgPicker.elements.removeAt(idx);
							});
							clientVM.displayMessage(RS.common.locale.success, RS.wiki.automation.locale.successfulDeleteImage);
							this.set('removeBtnDisabled', true);
						} else {
							clientVM.displayError(respData.message);
						}
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				}, this);
				break;
			}
		}
	},

	imageClicked: function(image, v) {
		var wasSelected = image.get('selectedCss') == 'selected';
		for( var i=0; i<this.elements.length; i++) {
			var curImg = this.elements.getAt(i);
			curImg.set('selectedCss', 'unselected');
			curImg.customImg? curImg.v.setBodyStyle('background-color: inherit;'): null;
		}
		if (!wasSelected) {
			image.set('selectedCss', 'selected');
			image.customImg? v.setBodyStyle('background-color: #E6E6E6;'): null;
			this.set('removeBtnDisabled', false);
		} else {
			image.customImg? v.setBodyStyle('background-color: inherit;'): null;
			this.set('removeBtnDisabled', true);
		}

	}
});
glu.defModel('RS.wiki.automation.ModelImage', {
	elementType: '',
	elementImg: '',
	elementWidth: 0,
	elementHeight: 0,
	customImg: false,
	hidden: false,
	selectedCss: 'unselected',

	elementDisplayType$: function() {
		return this.customImg? this.elementType: this.localize(this.elementType);
	},

	imageRendered: function(panel, editorPanel) {
		if (this.customImg) {
			this.v = panel;
			// Make the image draggable into the editor panel
			var graph = editorPanel.graph;
			var dragPreview = document.createElement('div');
			dragPreview.style.border = 'dashed black 1px';
			dragPreview.style.width = panel.elementWidth + 'px';
			dragPreview.style.height = panel.elementHeight + 'px';
			if(!this.editorPanel.viewOnly) {
				var dragSource = mxUtils.makeDraggable(panel.getEl().dom, graph, function(graph, evt, target) {
						editorPanel.fireEvent('elementDrop', panel, evt, target);
					}, dragPreview, -panel.elementWidth / 2, -panel.elementHeight / 2,
					graph.autoscroll, false);
			}
		}
	},

	imageClick: function(v) {
		if (this.customImg) {
			this.fireEvent('imageClicked', this, v);
		}
	}
});
