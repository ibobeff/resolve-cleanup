glu.defModel('RS.wiki.automation.ElementProperty', {
	nodeId: '',
	order: 9999,
	orderIsVisible: false,
	elementName: '',
	elementNameIsVisible$: function() {
		return this.elementName;
	},
	elementType: '',
	status: '',
	title$: function() {
		return this.localize('elementPropertyTitle', this.elementType);
	},
	cancel: function() {
		this.doClose();
	}
})