glu.defModel('RS.wiki.automation.SSHConnectionConfig', {
	mixins : ['ConnectionUtils'],	
	connectionType : 'ssh',
	taskWizard : {
		mtype : 'RS.wiki.automation.TaskWizard',
		connectionType : 'ssh'
	},
	sourceStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [{
			name: 'CONSTANT',
			value: 'CONSTANT'
		}, {
			name: 'PARAM',
			value: 'PARAM'
		}, {
			name: 'FLOW',
			value: 'FLOW'
		}, {
			name: 'WSDATA',
			value: 'WSDATA'
		}, {
			name: 'PROPERTY',
			value: 'PROPERTY'
		}]
	},
	propertyStore: {
		mtype: 'store',
		fields: ['uname'],
		sorters: ['uname'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/atproperties/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		},
		pageSize: -1
	},	
	when_password_source_changed: {
		on: ['passwordSourceChanged'],
		action: function(newValue, oldValue) {
			if (oldValue == 'CONSTANT' || newValue == 'CONSTANT')
				this.set('password', '');
		}
	},
	when_any_source_changed: {
		on: ['hostChanged', 'portChanged', 'usernameChanged', 'passwordChanged', 'keyfileChanged', 'passphraseChanged'],	
		action: function(newValue, oldValue, bindingInfo) {
			this.propertyStore.keyword = this[bindingInfo.modelPropName];
		}
	},
	when_prompt_changed : {
		on : ['promptChanged'],
		action : function(){
			this.taskWizard.updatePrompt(this.prompt);
		}
	},
	when_sessionName_changed : {
		on : ['sessionNameChanged'],
		action : function(){
			this.taskWizard.updateSessionName(this.sessionName);
		}
	},
	paramStore : null,

	//Main Config	
	password: '',
	hostSource: 'CONSTANT',
	portSource: 'CONSTANT',
	host: '',
	port: 22,
	username: '',	
	usernameSource: undefined,
	passwordSource: undefined,
	authType: 'usernameAndPassword',
	authTypeSource: 'CONSTANT',
	file: '',
	keyfile: '',
	keyfileSource: 'CONSTANT',
	passphrase: '',
	usernameAndPassword: true,
	keyFileNameAndPassphrase: false,
	passphraseSource: 'CONSTANT',	

	//Options
	queueNameSource: 'CONSTANT',
	queueName : '',
	sessionName : '',
	prompt : '',
	timeout : 300,
	
	init : function(){
		this.set('usernameSource', 'CONSTANT');
		this.set('passwordSource', 'CONSTANT');
		this.set('paramStore', this.rootVM.getParamStore());
		this.set('prompt', '$FLOW{PROMPT}');
		this.propertyStore.load();
	},	

	//Main Config
	hostIsText$ : function(){
		return this.hostSource == 'CONSTANT' || this.hostSource == 'FLOW' || this.hostSource == 'WSDATA';
	},
	hostIsParam$: function() {
		return this.hostSource == 'PARAM';
	},
	hostIsProperty$: function() {
		return this.hostSource == 'PROPERTY';
	},

	portIsText$: function() {
		return this.portSource == 'CONSTANT' || this.portSource == 'FLOW' || this.portSource == 'WSDATA';
	},	
	portIsParam$: function() {
		return this.portSource == 'PARAM';
	},
	portIsProperty$: function() {
		return this.portSource == 'PROPERTY';
	},

	usernameIsText$: function() {
		return this.usernameSource == 'CONSTANT' || this.usernameSource == 'FLOW' || this.usernameSource == 'WSDATA';
	},	
	usernameIsParam$: function() {
		return this.usernameSource == 'PARAM';
	},
	usernameIsProperty$: function() {
		return this.usernameSource == 'PROPERTY';
	},	

	passwordIsConstant$: function() {
		return this.passwordSource == 'CONSTANT';
	},
	passwordIsFlowOrWs$ : function(){
		return this.passwordSource == 'FLOW' || this.passwordSource == 'WSDATA';
	},
	passwordIsParam$: function() {
		return this.passwordSource == 'PARAM';
	},
	passwordIsProperty$: function() {
		return this.passwordSource == 'PROPERTY';
	},	

	keyfileIsText$: function() {
		return this.keyfileSource == 'CONSTANT' || this.keyfileSource == 'FLOW' || this.keyfileSource == 'WSDATA';
	},		
	keyfileIsParam$: function() {
		return this.keyfileSource == 'PARAM';
	},
	keyfileIsProperty$: function() {
		return this.keyfileSource == 'PROPERTY';
	},

	passphraseIsText$: function() {
		return this.passphraseSource == 'CONSTANT' || this.passphraseSource == 'FLOW' || this.passphraseSource == 'WSDATA';;
	},	
	passphraseIsParam$: function() {
		return this.passphraseSource == 'PARAM';
	},
	passphraseIsProperty$: function() {
		return this.passphraseSource == 'PROPERTY';
	},

	authTypeChanged$: function() {
		if (this.usernameAndPassword)
			this.set('authType', 'usernameAndPassword');
		else
			this.set('authType', 'keyFileNameAndPassphrase');
	},

	//Option Config
	queueNameSourceChanged$: function() {
		this.set('queueName', '');
	},
	queueNameIsText$: function() {
		return this.queueNameSource == 'CONSTANT' || this.queueNameSource == 'WSDATA' || this.queueNameSource == 'FLOW';
	},	
	queueNameIsProperty$: function() {
		return this.queueNameSource == 'PROPERTY';
	},
	queueNameIsParam$: function() {
		return this.queueNameSource == 'PARAM';
	},

	//Call from Mixin, will be different depend on different connection type
	getConnectionParams : function(){
		var params = {};
		params['authType'] = this.authType;
		var paramArray = ['host','port','username','queueName'];
		for(var i = 0; i < paramArray.length; i++){
			var paramName = paramArray[i];
			if(this[paramName]){
				params[paramName] = this[paramName + 'Source'] == "CONSTANT" ? this[paramName] : "$" + this[paramName + 'Source'] + "{" + this[paramName] + "}";
			}
		}		
		if(this.authType = "usernameAndPassword" && this.password){
			params['password'] = this.passwordSource == "CONSTANT" ? this.password : "$" + this.passwordSource + "{" + this.password + "}";
		}
		else {
			if(this.keyfile)
				params['keyfile'] = this.keyfileSource == "CONSTANT" ? this.keyfile : "$" + this.keyfileSource + "{" + this.keyfile + "}";
			if(this.passphrase)
			params['passphrase'] = this.passphraseSource == "CONSTANT" ? this.passphrase : "$" + this.passphraseSource + "{" + this.passphrase + "}";
		}

		//Options Params
		params['timeout'] = this.timeout;
		params['prompt'] = this.prompt;
		params['sessionName'] = this.sessionName;
		return params;		
	}	
});
