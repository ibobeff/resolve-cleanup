// glu.defModel('RS.wiki.automation.PreconditionMenu', {
// 	mixins: ['ElementMenu'],
// 	editProperties: function() {
// 		var me = this;
// 		this.open({
// 			elementType: this.localize('precondition'),
// 			mtype: 'RS.wiki.automation.PreconditionProperty',
// 			expression: this.cellGetter().getValue().getAttribute('description'),
// 			nodeId: this.cellGetter().id,
// 			dumper: function() {
// 				me.parentVM.fireEvent('elementpropertychanged', this.elementType, this.expression);
// 			}
// 		});
// 	}
// });