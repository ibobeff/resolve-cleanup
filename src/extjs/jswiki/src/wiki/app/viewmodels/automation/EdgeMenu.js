glu.defModel('RS.wiki.automation.EdgeMenu', {
	parentPanel: null,
	initX: 0,
	initY: 0,
	executionValue$: function() {
		if (!this.cellGetter().style)
			return 'required';
		return this.cellGetter().style.indexOf('dashed=true') != -1 ? 'notRequired' : 'required';
	},
	conditionValue$: function() {
		if (!this.cellGetter().style)
			return 'none';
		if (this.cellGetter().style.indexOf('strokeColor=green') != -1)
			return 'good';
		if (this.cellGetter().style.indexOf('strokeColor=red') != -1)
			return 'bad';
	},
	connectorValue$: function() {
		if (!this.cellGetter().style)
			return 'straight';
		if (this.cellGetter().style.indexOf('noEdgeStyle') != -1)
			return 'straight';
		if (this.cellGetter().style.indexOf('horizontal') != -1)
			return 'horizontal';
		if (this.cellGetter().style.indexOf('vertical') != -1)
			return 'vertical';
		return 'straight';
	}
});