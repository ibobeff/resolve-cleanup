glu.defModel('RS.wiki.automation.ConnectionUtils',{
	API : {	
		encryptParams : '/resolve/service/wiki/encryptParams'
	},
	saving: false,
	connectionIsValid : function(){
		return this.taskWizard.isTaskValid();
	},
	activeStep$ :function(){
		return this.connectionWizardStepIsPressed ? 0 : 1;
	},
	done : function(){
		if(!this.connectionIsValid())
			return;
		var connectionInfo = {
			connectionType : this.connectionType,
			connectionParams : this.getConnectionParams()
		}
		//If there is no task for this connection just generate model.
		if(!this.taskWizard.useActionTask){			
			this.generateConnection(connectionInfo);
		}
		//Gather task Info, save ,then generate model.
		else {
			var callback = function(taskInfo){			
				this.generateConnection(connectionInfo, taskInfo);
			}
			this.taskWizard.saveTaskInfo(callback,this);			
		}
	},
	doneIsEnabled$: function(){
		return !this.saving;
	},
	cancel : function(){
		this.doClose();
	},	
	next : function(){
		this.set('connectionWizardStepIsPressed', false);
		this.taskWizard.set('useActionTask', false);	
	},
	nextIsVisible$ : function(){
		return this.connectionWizardStepIsPressed;
	},
	doneIsVisible$ : function(){
		return !this.connectionWizardStepIsPressed;
	},	
	connectionWizardStep : function(){
		this.set('connectionWizardStepIsPressed', true);
	},
	connectionWizardStepIsPressed : true,
	taskWizardStep : function(){
		this.set('connectionWizardStepIsPressed', false);
	},
	taskWizardStepIsPressed$ : function(){
		return !this.connectionWizardStepIsPressed;
	},
	generateConnection : function(connectionInfo, taskInfo){
		this.set('saving', true);
		var me = this;
		var connectionParams = connectionInfo['connectionParams'];

		//Convert Number to string 
		for(var k in connectionParams){
			connectionParams[k] = connectionParams[k].toString();
		}
		//Encrypt params before generate connection.
		this.ajax({
			url: me.API['encryptParams'],
			jsonData : {
				connectionType : connectionInfo['connectionType'],
				params : JSON.stringify(connectionParams)
			},
			success : function(resp){
				var respData = RS.common.parsePayload(resp);
				if (respData.success) {
					params = {};
					params['connectionInfo'] = {
						param : respData.data
					};
					//Gathering data for template actiontask.
					if(taskInfo){
						params['taskInfo'] = taskInfo;					
					}
					me.parentVM.renderConnection(params);
					me.set('saving', false);
					me.doClose();
				} else {
					clientVM.displayError(respData.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
				me.set('saving', false);
			}
		})
	},
})
//Hardcoded for now until there is new API to save task with connection.
RS.wiki.automation.RemoteTaskTemplate = {
	DEFAULT : [{
		udefaultValue : '10',
		udescription : 'Custom timeout',
		uencrypt : false,
		ugroupName : 'UNCLASSIFIED',
		uname: 'EXPECT_TIMEOUT',
		uorder : 0,
		urequired : true,
		utype : 'INPUT'
	},{
		udefaultValue : '',
		udescription : 'Command prompt (e.g., \\$)',
		uencrypt : false,
		ugroupName : 'UNCLASSIFIED',
		uname: 'prompt',
		uorder : 0,
		urequired : true,
		utype : 'INPUT'
	},{
		udefaultValue : '',
		udescription : 'Command to be executed.',
		uencrypt : false,
		ugroupName : 'UNCLASSIFIED',
		uname: 'command',
		uorder : 0,
		urequired : true,
		uprotected : true,
		utype : 'INPUT'
	},{
		udefaultValue : '',
		udescription : "Connection's session name",
		uencrypt : false,
		ugroupName : 'UNCLASSIFIED',
		uname: 'sessionName',
		uorder : 0,
		urequired : false,
		utype : 'INPUT'
	}],
	LOCAL : [{
		udefaultValue : '',
		udescription : 'Command to be executed.',
		uencrypt : false,
		ugroupName : 'UNCLASSIFIED',
		uname: 'command',
		uorder : 0,
		urequired : true,
		uprotected : true,
		utype : 'INPUT'
	}],
	HTTP : [{
		udefaultValue : '10',
		udescription : 'Custom timeout',
		uencrypt : false,
		ugroupName : 'UNCLASSIFIED',
		uname: 'EXPECT_TIMEOUT',
		uorder : 0,
		urequired : true,
		utype : 'INPUT'
	},{
		udefaultValue : 'GET',
		udescription : 'GET or POST',
		uencrypt : false,
		ugroupName : 'UNCLASSIFIED',
		uname: 'method',
		uorder : 0,
		urequired : true,
		utype : 'INPUT'
	},{
		udefaultValue : '',
		udescription : 'URL to connect to',
		uencrypt : false,
		ugroupName : 'UNCLASSIFIED',
		uname: 'commandUrl',
		uorder : 0,
		urequired : true,
		utype : 'INPUT'
	},{
		udefaultValue : '',
		udescription : '',
		uencrypt : false,
		ugroupName : 'UNCLASSIFIED',
		uname: 'headers',
		uorder : 0,
		urequired : true,
		utype : 'INPUT'
	},{
		udefaultValue : '',
		udescription : '',
		uencrypt : false,
		ugroupName : 'UNCLASSIFIED',
		uname: 'body',
		uorder : 0,
		urequired : true,
		utype : 'INPUT'
	},{
		udefaultValue : 'true',
		udescription : "Body's type for POST request",
		uencrypt : false,
		ugroupName : 'UNCLASSIFIED',
		uname: 'useFormData',
		uorder : 0,
		urequired : false,
		utype : 'INPUT'
	},{
		udefaultValue : '',
		udescription : "Connection's session name",
		uencrypt : false,
		ugroupName : 'UNCLASSIFIED',
		uname: 'sessionName',
		uorder : 0,
		urequired : false,
		utype : 'INPUT'
	}]
};