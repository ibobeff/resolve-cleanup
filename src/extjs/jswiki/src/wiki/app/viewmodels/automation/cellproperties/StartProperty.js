glu.defModel('RS.wiki.automation.StartProperty', {
	mixins: ['MergeAware', 'TaskProperty', 'XmlAware', 'LabelAware', 'VersionAware', 'ActionTaskValidation'],
	inputs: {
		mtype: 'Parameters'
	},
	reqCount: 0,
	cell: null,
	populate: function(cell) {
		this.resetProperty();
		this.populateVersion(cell);
		this.set('loading', true);
		this.set('cell', cell);
		var result = this.parse(cell.getValue());
		this.set('label', cell.getAttribute('label'));
		this.populateMerge(cell);
		this.set('taskName', 'start#resolve');
		this.set('reqCount', this.reqCount + 1);
		this.getTask('start#resolve', function(task) {
			this.set('newTask', false);
			this.updateTaskProperty(task);
		}, function() {}, function() {
			this.set('reqCount', this.reqCount - 1);
		});
		this.checkActionTaskParamDependency();
		this.set('loading', false);
	},
	init : function(){}
});