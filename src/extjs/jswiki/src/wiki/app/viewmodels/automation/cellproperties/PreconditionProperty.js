glu.defModel('RS.wiki.automation.PreconditionProperty', {
	mixins: ['XmlAware', 'MergeAware', 'LabelAware', 'ActionTaskValidation'],
	cell: null,
	expression: '',
	when_expression_changed: {
		on: ['expressionChanged'],
		action: function() {
			if (this.loading)
				return;
			this.parentVM.fireEvent('elementpropertychanged', this.cell, 'description', this.expression);
		}
	},
	loading: false,
	resetProperty: function() {
		this.set('loading', true);
		this.set('cell', null);
		this.set('label', '');
		this.set('loading', false);
	},
	populate: function(cell) {
		this.resetProperty();
		this.set('loading', true);
		this.set('cell', cell);
		this.set('label', cell.getAttribute('label'));
		this.populateMerge(cell);
		this.set('expression', cell.getAttribute('description'));
		this.checkActionTaskParamDependency();
		this.set('loading', false);
	}
});