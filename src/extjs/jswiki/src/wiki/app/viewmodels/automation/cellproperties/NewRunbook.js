glu.defModel('RS.wiki.automation.NewRunbook', {
	mixins: ['MergeAware', 'RunbookProperty', 'XmlAware', 'LabelAware', 'VersionAware', 'ActionTaskValidation'],
	loading: false,
	cell: null,
	isCreateRunbook: false,
	isSelectRunbook: false,

	wait: false,
	runbook: '',
	defaultData: {
		name: '',
		namespace: '',
		runbook: '',
	},
	fields: ['name', 'namespace', 'runbook'],

	API : {
		save : '/resolve/service/wiki/save',
	},

	defaultModelProcess: '\
<mxGraphModel>\
  <root>\
    <mxCell id="0"/>\
    <mxCell id="1" parent="0"/>\
    <Start label="Start" description="" id="5">\
      <name>\
        start#resolve\
      </name>\
      <mxCell style="symbol;image=/resolve/jsp/model/images/symbols/start.png;fontFamily=Helvetica;fontSize=11" parent="1" vertex="1">\
        <mxGeometry x="325" y="55" width="30" height="30" as="geometry"/>\
      </mxCell>\
    </Start>\
    <End label="End" description="" id="6">\
      <name>\
        end#resolve\
      </name>\
      <mxCell style="symbol;image=/resolve/jsp/model/images/symbols/end.png;fontFamily=Helvetica;fontSize=11" parent="1" vertex="1">\
        <mxGeometry x="325" y="235" width="30" height="30" as="geometry"/>\
      </mxCell>\
    </End>\
    <Edge label="" description="" order="99999" id="7">\
      <mxCell style="straight;noEdgeStyle=1" parent="1" source="5" target="6" edge="1">\
        <mxGeometry relative="1" as="geometry"/>\
      </mxCell>\
    </Edge>\
  </root>\
</mxGraphModel>',

	init: function() {
		this.runbookStore.on('beforeload', function(store, op) {
			op.params = op.params || {}
			Ext.apply(op.params, {
				filter: Ext.encode([{
					field: 'ufullname',
					condition: 'contains',
					type: 'auto',
					value: op.params.query || this.runbook
				}])
			})
			this.set('reqCount', this.reqCount + 1);
		}, this);
		this.runbookStore.on('load', function() {
			this.validateRunbook(this.runbook, true);
			this.set('reqCount', this.reqCount - 1);
		}, this);
		this.initVersionHistoryStore();
	},
	populate: function(cell) {
		this.resetProperty();
		this.populateVersion(cell, true);
		this.set('loading', true);
		this.set('cell', cell);
		this.enableAllButton(true);
		this.checkActionTaskParamDependency();
		this.set('loading', false);
	},

	handleRunbookChange : function(newRunbook, prevRunbook){
		this.validateRunbook(newRunbook, true);
	},

	handleRunbookSelect: function(record) {
		this.updateRunbookProperty(record[0].get('ufullname'));
	},

	resetProperty: function() {
		this.resetVersion();
		this.set('loading', true);
		this.loadData(this.defaultData);
		this.set('isCreateRunbook', false);
		this.set('isSelectRunbook', false);
		this.set('isPristine', true);
		this.enableAllButton(true);
		this.set('runbook', '');
		this.set('runbookIsValid', true);
		this.set('loading', false);
	},

	selectRunbook: function() {
		this.set('isCreateRunbook', false);
		this.set('isSelectRunbook', true);
		if (!this.runbook) {
			this.set('runbook', '');
		}
	},

	selectRunbookHidden$: function() {
		return !this.isSelectRunbook || this.isCreateRunbook;
	},

	createRunbook: function() {
		this.set('isCreateRunbook', true);
		this.set('isSelectRunbook', false);
		this.set('isPristine', true);
		this.enableAllButton(true);
	},

	isCreateValid$: function() {
		return this.isNamespaceAndNameValid;
	},
	isDoneValid$: function() {
		return this.isSelectedRunbookNameValid == true && this.runbookIsValid == true; // must compare to true since false will be an error string
	},

	/* TODO - support Version Control
	showUseVersionOption$: function() {
		if (!this.isDoneValid) {
			this.resetVersion();
		}
		return this.isDoneValid;
	},

	showVersionNumberOption$: function() {
		return this.specificVersion && this.isDoneValid;
	},
	*/ 

	isSelectedRunbookNameValid$: function() {
		if (this.runbook) {
			var namespace = this.runbook.split('.')[0];
			var name = this.runbook.split('.')[1];
			if (namespace && name && this.validateNamespaceAll(namespace) && this.validateName(name)) {
				return true;
			}
			return this.localize('invalidRunbook');
		}
		return false;
	},

	isNamespaceAndNameValid$: function() {
		return (this.namespaceIsValid == true) && (this.nameIsValid == true);
	},

    nameIsValid$: function() {
        return this.name && this.validateName(this.name) ? true : this.localize('invalidName');
    },

	namespaceIsValid$: function() {
        return this.namespace && this.validateNamespace(this.namespace) ? true : this.localize('invalidNamespace');
    },

	createRunbookHidden$: function() {
		return !this.isCreateRunbook || this.isSelectRunbook;
	},

	create: function() {
		if(!this.isCreateValid){
			this.set('isPristine', false);
			this.enableAllButton(false);
			return;
		}
		this.doCreate();
	},
	on_form_is_valid : {
		on : ['isValidChanged'],
		action : function(){
			this.enableAllButton(this.isValid);
		}
	},
	enableAllButton : function(flag){
		this.set('createIsEnabled', flag);
	},
	createIsEnabled: true,
	doCreate : function(){
		if (this.wait)
			return

		var wikiParams = Ext.clone(this.rootVM.defaultData);
		wikiParams.accessRights = Ext.clone(this.rootVM.accessRights);
		wikiParams.accessRights.uresourceName = this.namespace + '.'+ this.name;
		wikiParams.accessRights.uresourceType = 'wikidoc';

		wikiParams.ufullname = this.namespace + '.'+ this.name;
		wikiParams.uname = this.name;
		wikiParams.unamespace = this.namespace;
		wikiParams.uhasActiveModel = true;
		wikiParams.umodelProcess = this.defaultModelProcess;

		var nullTheId = function(obj) {
			for (key in obj) {
				if (key == 'assignedTo' || !obj[key])
					continue;
				if (key == 'id' || key == 'sysId' || key == 'sys_id')
					obj[key] = '';
				else if (key == 'uresourceId' || key == 'sysCreatedBy' || key == 'sysPerm' || key == 'sysOrganizationName' || key == 'sysUpdatedBy' || key == 'sysUpdatedBy')
					obj[key] = "UNDEFINED";
				else if (key == 'sysCreatedOn' || key == 'sysUpdatedOn' || key == 'sysModCount' || key == 'sysOrg')
					obj[key] = 0;
				else if (Ext.isObject(obj[key]))
					nullTheId(obj[key]);
				else if (Ext.isArray(obj[key]))
					Ext.each(obj[key], function(o) {
						nullTheId(o)
					});
			}
		}
		nullTheId(wikiParams);

		this.set('wait', true);
		this.ajax({
			url: this.API.save,
			jsonData: wikiParams,
			params: {
				comment: '',
				postToSocial: false,
				reviewed: false
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.localize('wikiSaved'))

					this.cell.setAttribute('label', this.name);
					this.cell.setAttribute('description', this.namespace + '.'+ this.name);
					this.parentVM.fireEvent('selectRunbook', this.namespace + '.'+ this.name, this.cell);
					this.parentVM.fireEvent('updateSelectedRunbook', [this.cell]);
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false)
			}
		})
	},

	cancel: function() {
		this.resetProperty();
		this.populateVersion(this.cell, true);
	},
	searchRunbook: function() {
		this.set('isSelectRunbook', true);
		this.set('isCreateRunbook', false);
		var me = this;
		this.open({
			mtype: 'RS.wiki.RunbookPicker',
			dumper: {
				dump: function(selections) {
					if (selections.length > 0) {
						var selection = selections[0];
						me.set('runbook', selection.get('ufullname'));
						me.runbookStore.load();
					}
				}
			}
		})
	},

	updateRunbookProperty: function(runbook) {
		this.set('runbook', runbook);
	},
	done: function() {
		this.validateRunbook(this.runbook);
		this.parentVM.fireEvent('selectRunbook', this.runbook, this.cell);
		this.parentVM.fireEvent('updateSelectedRunbook', [this.cell]);
		this.cell.setAttribute('label', this.runbook.split('.')[1]); //Initial set label on new runbook
		this.cell.setAttribute('description', this.runbook);
		/* TODO - support Version Control
		if (this.latestStable) {
			this.cell.setAttribute('version', 'latestStable');
		} else if (this.specificVersion) {
			this.cell.setAttribute('version', this.versionNumber);
		}
		*/
		this.resetProperty();
	},
});
