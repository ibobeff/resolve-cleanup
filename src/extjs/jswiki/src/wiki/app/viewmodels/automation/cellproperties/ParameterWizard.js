glu.defModel('RS.wiki.automation.ParameterWizard',{
	httpTaskConfig : {
		mtype : 'HTTPTaskConfig'	
	},
	networkTaskConfig : {
		mtype : 'NetworkTaskConfig'		
	},
	httpTaskProperty : {
		method : '',
		headers : '',
		commandUrl : '',
		body : '',
		formData : '',
		useFormData : true
	},
	networkTaskProperty : {
		command : '',
		prompt : '',
		sessionName : ''
	},
	taskIsHTTP$ : function(){
		return this.wizardType.toUpperCase() == 'HTTP';
	},
	taskIsPartOfCreation : true,
	taskConfigDataMap : {},
	currentCellId : null,
	wizardType : 'LOCAL',
	init : function(){
		this.httpTaskConfig.set('taskIsPartOfCreation', this.taskIsPartOfCreation);
		this.networkTaskConfig.set('taskIsPartOfCreation', this.taskIsPartOfCreation);
	},
	saveVariableList : function(variabelType, data){
		this.httpTaskConfig.saveVariableList(variabelType, data);
		this.networkTaskConfig.saveVariableList(variabelType, data);
	},
	updateWizardType : function(wizardType){
		this.set('wizardType', wizardType);
		if(wizardType.toUpperCase() != 'HTTP')
			this.networkTaskConfig.updateWizardType(wizardType);
	},
	saveCurrentWizardConfig : function(cellId){
		this.taskConfigDataMap[cellId] = this.taskIsHTTP ? this.httpTaskConfig.getCurrentConfig() : this.networkTaskConfig.getCurrentConfig();
	},
	loadWizardConfig : function(cellId, newProperties){
		var inputNames = Object.keys(newProperties);
		this.saveVariableList('INPUT',inputNames);
		if(this.taskIsHTTP){
			var dataObject = this.taskConfigDataMap.hasOwnProperty(cellId) ? this.taskConfigDataMap[cellId] : this.httpTaskProperty;
			var updatedParams = Ext.apply(dataObject, newProperties);
			this.httpTaskConfig.suspendWaitingForDataChange(true);
			this.httpTaskConfig.loadConfig(updatedParams);	
			this.httpTaskConfig.suspendWaitingForDataChange(false);	
		}
		else {
			var dataObject = this.taskConfigDataMap.hasOwnProperty(cellId) ? this.taskConfigDataMap[cellId] : this.networkTaskProperty;	
			var updatedParams = Ext.apply(dataObject, newProperties);
			this.networkTaskConfig.suspendWaitingForDataChange(true);
			this.networkTaskConfig.loadConfig(updatedParams);	
			this.networkTaskConfig.suspendWaitingForDataChange(false);	
		}		
	},
	updateValueForProperty : function(propName, value){
		this.parentVM.updateValueForProperty(propName, value);
	}
})