// glu.defModel('RS.wiki.automation.TaskMenu', {
// 	mixins: ['ElementMenu'],
// 	taskName: '',
// 	modelInputs: [],
// 	modelOutputs: [],
// 	init: function() {
// 		function parseCDATA(cdata) {
// 			if (!cdata)
// 				return [];
// 			try {
// 				return Ext.decode(cdata);
// 			} catch (e) {
// 				return [];
// 			}
// 		}
// 		var me = this;

// 		function extract(node) {
// 			Ext.each(node.childNodes, function(child) {
// 				if (child.tagName) {
// 					var text = child.textContent || child.innerText || '';
// 					if (child.tagName.toLowerCase() == 'inputs')
// 						me.set('modelInputs', parseCDATA(text));
// 					if (child.tagName.toLowerCase() == 'outputs')
// 						me.set('modelOutputs', parseCDATA(text));
// 					if (child.tagName.toLowerCase() == 'name')
// 						me.set('taskName', text);
// 				}
// 				extract(child);
// 			})
// 		}
// 		extract(this.cellGetter().getValue());
// 	},
// 	searchTask: function() {
// 		var me = this;
// 		this.open({
// 			mtype: 'RS.actiontask.ActionTaskPicker',
// 			dumper: {
// 				dump: function(selections) {
// 					if (selections.length > 0) {
// 						var selection = selections[0]
// 						me.parentVM.fireEvent('selectTask', selection.get('uname'), selection.get('unamespace'), selection.get('resolveActionInvoc').resolveActionParameters, selection.get('udescription'));
// 					}
// 				}
// 			}
// 		})
// 	},
// 	executeTask: function() {
// 		var dto = {
// 			actiontask: this.taskName,
// 			action: 'TASK',
// 			params: this.modelInputs
// 		};
// 		this.ajax({
// 			url: '/resolve/service/execute/submit',
// 			jsonData: dto,
// 			success: function(r) {
// 				var response = RS.common.parsePayload(r)
// 				if (response.success) {
// 					clientVM.updateProblemInfo(response.data.problemId, response.data.problemNumber)
// 					clientVM.fireEvent('worksheetExecuted', clientVM)
// 					clientVM.displaySuccess(this.localize('executeSuccess', {
// 						type: !dto.wiki ? 'ActionTask' : 'Runbook',
// 						fullName: !dto.wiki ? dto.actiontask : dto.wiki
// 					}))
// 					this.doClose()
// 				} else clientVM.displayError(response.message)
// 			},
// 			failure: function(r) {
// 				clientVM.displayFailure(r)
// 			}
// 		})
// 	},
// 	executeTaskIsEnabled$: function() {
// 		return this.taskName;
// 	},
// 	editTask: function() {
// 		var splits = this.taskName.split('#');
// 		clientVM.handleNavigation({
// 			modelName: 'RS.actiontask.ActionTask',
// 			params: {
// 				name: splits[0],
// 				namespace: splits[1]
// 			},
// 			target: '_blank'
// 		});
// 	},
// 	editTaskIsEnabled$: function() {
// 		return this.taskName;
// 	},
// 	editInputs: function() {
// 		var me = this;
// 		this.ajax({
// 			url: '/resolve/service/actiontask/get',
// 			params: {
// 				id: '',
// 				name: this.taskName
// 			},
// 			success: function(resp) {
// 				var respData = RS.common.parsePayload(resp);
// 				if (!respData.success) {
// 					clientVM.displayError(this.localize('getTaskDefaultParamError', respData.message));
// 					return;
// 				}
// 				var params = respData.data.resolveActionInvoc.resolveActionParameters;
// 				var defaultParams = {}
// 				Ext.each(params, function(param) {
// 					if (param.utype.toLowerCase() == 'input')
// 						defaultParams[param.uname] = param.udefaultValue;
// 				}, this)
// 				this.open({
// 					mtype: 'RS.wiki.automation.Parameters',
// 					defaultParams: defaultParams,
// 					modelParams: this.modelInputs,
// 					taskName: this.taskName,
// 					isInput: true,
// 					dumper: function() {
// 						me.set('modelInputs', this.getParamsFromStore());
// 						me.toCDATA(me.modelInputs, 'inputs');
// 					}
// 				});
// 			},
// 			failure: function(resp) {
// 				clientVM.displayFailure(resp);
// 			}
// 		})
// 	},
// 	editInputsIsEnabled$: function() {
// 		return this.taskName;
// 	},
// 	editOutputs: function() {
// 		var me = this;
// 		this.ajax({
// 			url: '/resolve/service/actiontask/get',
// 			params: {
// 				id: '',
// 				name: this.taskName
// 			},
// 			success: function(resp) {
// 				var respData = RS.common.parsePayload(resp);
// 				if (!respData.success) {
// 					clientVM.displayError(this.localize('getTaskDefaultParamError', respData.message));
// 					return;
// 				}
// 				var params = respData.data.resolveActionInvoc.resolveActionParameters;
// 				var defaultParams = {}
// 				Ext.each(params, function(param) {
// 					if (param.utype.toLowerCase() == 'output')
// 						defaultParams[param.uname] = param.udefaultValue;
// 				}, this)
// 				this.open({
// 					mtype: 'RS.wiki.automation.Parameters',
// 					defaultParams: defaultParams,
// 					modelParams: this.modelOutputs,
// 					taskName: this.taskName,
// 					isInput: false,
// 					dumper: function() {
// 						me.set('modelOutputs', this.getParamsFromStore());
// 						me.toCDATA(me.modelOutputs, 'outputs');
// 					}
// 				});
// 			},
// 			failure: function(resp) {
// 				clientVM.displayFailure(resp);
// 			}
// 		});
// 	},
// 	editOutputsIsEnabled$: function() {
// 		return this.taskName;
// 	},
// 	editProperties: function() {
// 		var me = this;
// 		this.open({
// 			elementType: this.localize('task'),
// 			mtype: 'RS.wiki.automation.ElementProperty',
// 			nodeId: this.cellGetter().id,
// 			elementName: this.taskName
// 		});
// 	}
// });