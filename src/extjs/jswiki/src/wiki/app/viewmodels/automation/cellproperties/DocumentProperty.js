glu.defModel('RS.wiki.automation.DocumentProperty', {
	mixins: ['LabelAware', 'VersionAware'],
	wiki: '',
	documentStore: {
		mtype: 'store',
		fields: ['ufullname'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wiki/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	wikiIsValid: true,
	validateDocumentName: function() {
		if (this.loading)
			return;
		var r = this.documentStore.findRecord('ufullname', this.wiki, 0, false, false, true);
		if (!r) {
			this.set('wikiIsValid', this.localize('invalidWiki', this.wiki));
			this.parentVM.fireEvent('entityexistence', this.cell, false);
			return;
		}
		this.parentVM.fireEvent('entityexistence', this.cell, true);
		this.set('wikiIsValid', true);
	},
	when_wiki_changed: {
		on: ['wikiChanged'],
		action: function() {
			this.set('label', this.wiki);
			this.validateDocumentName();
		}
	},
	cell: null,
	loading: false,
	init: function() {
		this.documentStore.on('beforeload', function(store, op) {
			op.params = op.params || {};
			Ext.apply(op.params, {
				filter: Ext.encode([{
					field: 'ufullname',
					condition: 'contains',
					type: 'auto',
					value: op.params.query || this.wiki
				}])
			})
		}, this)
		this.documentStore.on('load', function() {
			this.validateDocumentName();
		}, this)
	},
	resetProperty: function() {
		this.resetVersion();
		this.set('loading', true);
		this.set('cell', null);
		this.set('label', '');
		this.set('loading', false);
	},
	populate: function(cell) {
		this.resetProperty();
		this.populateVersion(cell);
		this.set('loading', true);
		this.set('cell', cell);
		this.set('label', cell.getAttribute('label'));
		this.set('wiki', cell.getAttribute('wiki') || cell.getAttribute('label'));
		this.set('loading', false);
		this.documentStore.load();
	},
	searchDecisionTree: function() {
		var me = this;
		this.open({
			mtype: 'RS.decisiontree.DocumentPicker',
			dumper: {
				dump: function(selections) {
					if (selections.length > 0) {
						var selection = selections[0]
						me.parentVM.fireEvent('selectDocument', selection.get('ufullname'), me.cell);
						me.populate(me.cell);
					}
				}
			}
		})
	},
	jumpToWiki: function() {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			target: '_blank',
			params: {
				name: this.wiki,
				isEditMode: true
			}
		});
	},
	jumpToWikiIsVisible$: function() {
		return this.wikiIsValid === true;
	}
});
