glu.defModel("RS.wiki.automation.TaskWizard",{
	connectionType : null,
	useActionTask : false,
	taskInfo : '',
	//0 = existing task, 1 = new task
	activeTaskWizard : 0,
	autoGenCode : '',
	sessionName : '',
	taskWizardProperty : {
		mtype : 'TaskWizardProperty'
	},
	newTaskWizardProperty : {
		mtype : 'NewTaskWizardProperty'
	},
	API : {
		save : '/resolve/service/actiontask/save',
		getContentAutoGenCode : '/resolve/service/actiontask/getContentAutoGenCode'	
	},
	init : function(){
		this.ajax({
			url : this.API['getContentAutoGenCode'],
			jsonData : this.connectionType,
			success : function(resp){
				var respData = RS.common.parsePayload(resp);
				if (respData.success) {
					this.set('autoGenCode', respData.data);
					this.taskWizardProperty.updateAutoGenCode(this.autoGenCode);
				} else {
					clientVM.displayError(respData.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
		this.set('taskInfo', this.localize(this.connectionType + 'TaskInfo'));
		this.taskWizardProperty.set('connectionType', this.connectionType);
		this.newTaskWizardProperty.set('connectionType', this.connectionType);

		//Get Property variables
		var propertyStore = clientVM.getActionTaskPropertyStore();
		this.taskWizardProperty.saveVariableList('PROPERTY', propertyStore.collect('uname'));
		this.newTaskWizardProperty.saveVariableList('PROPERTY', propertyStore.collect('uname'));

		//Get Param variables
		var paramStore = this.rootVM.getParamStore();
		this.taskWizardProperty.saveVariableList('PARAM', paramStore.collect('name'));
		this.newTaskWizardProperty.saveVariableList('PARAM', paramStore.collect('name'));
		
	},
	isTaskValid : function(){
		return this.activeTaskWizard == 0 ? true : this.newTaskWizardProperty.isTaskValid();
	},	
	openTaskPicker : function(){
		this.open({
			mtype : 'RS.actiontask.ActionTaskPicker',			
			dumper : {
				dump : function(selectedRecords){
					var selectedTask = selectedRecords[0];
					var taskSysId = selectedTask.get('id');
					this.taskWizardProperty.getTaskData(taskSysId);
					this.set('activeTaskWizard',0);
					this.set('useActionTask', true);		
				},
				scope : this
			}
		})
	},
	createNewTask : function(){
		this.set('activeTaskWizard',1);
		this.set('useActionTask', true);
		this.newTaskWizardProperty.resetProperties();
	},
	updatePrompt : function(newPrompt){	
		this.newTaskWizardProperty.updatePrompt(newPrompt);
		this.taskWizardProperty.updatePrompt(newPrompt);
	},
	updateSessionName : function(newSessionName){	
		this.set('sessionName', newSessionName);
	},
	//Saving Task
	saveTaskInfo : function(callback, scope){
		//Save Existing Task
		if(this.activeTaskWizard == 0){
			var taskInfo = this.taskWizardProperty.getTaskInfo();
			//Add session name for this task.
			taskInfo['params']['sessionName'] = this.sessionName;
			var taskPayload = this.taskWizardProperty.getTaskPayload();
			this.saveExistingTask(taskInfo, taskPayload, callback, scope);
		}
		//New Task
		else if(this.activeTaskWizard == 1){
			var taskInfo = this.newTaskWizardProperty.getTaskInfo();
			var newTaskParam = [];
			var templateParams =  this.connectionType == "http" ? RS.wiki.automation.RemoteTaskTemplate['HTTP'] :  RS.wiki.automation.RemoteTaskTemplate['DEFAULT'];
			Ext.Array.each(templateParams, function(property){
				var newProp = {};
				Ext.apply(newProp, property);
				if(taskInfo['params'].hasOwnProperty(property.uname)){
					if(taskInfo['params'][property.uname] !== '')
						newProp.udefaultValue = taskInfo['params'][property.uname];
					//Set this value to undefined so we know that parameter is already used.
					taskInfo['params'][property.uname] = undefined;
				}
				newTaskParam.push(newProp);
			})
			//Any value that is not undefined doesnt have a template for it.
			for(var k in taskInfo['params']){
				var paramValue = taskInfo['params'][k];
				if(paramValue !== undefined){
					newTaskParam.push({
						uname : k,
						udefaultValue : taskInfo['params'][k],
						urequired : true,
						utype : 'INPUT'
					})
				}
			}
			var extraData = {
				"resolveActionInvoc.resolveActionInvocOptions" : [{
					id : '',
					udescription : '',
					uname : 'INPUTFILE',
					uvalue : this.autoGenCode
				}],
				"resolveActionInvoc.resolveActionParameters" :  newTaskParam
			}			
			this.saveNewTask(taskInfo, extraData, callback, scope);			
		}
	},
	saveNewTask : function(taskInfo, extraData, callback, scope){
		var me = this;
		var actiontaskbuilder = this.model({
			mtype: 'RS.actiontask.ActionTask',
			embed: true
		});
		actiontaskbuilder.initActionTaskBuilder();

		var actionTask = actiontaskbuilder.emptyActionTaskData.data;
		actionTask.uname = taskInfo['name'];
		actionTask.unamespace = taskInfo['namespace'];
		actionTask.umenuPath = '/' + taskInfo['namespace'].replace(/\./g, '/');
		actionTask.resolveActionInvoc.utype = 'remote';
		actionTask.resolveActionInvoc.utimeout = 300;
		actionTask.wizardType = this.connectionType;

		if(extraData){
			actionTask = RS.common.deepApply(actionTask, extraData);
		}		
	
		this.ajax({
			url: this.API['save'],
			jsonData: actionTask,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);

				if (respData.success) {
					//Add connection to canvas. All parameters of new task will be default. Remove those param from XML
					//except session, since session name is not default for this task.			
					taskInfo['params'] = {
						sessionName : this.sessionName
					}				
					callback.apply(scope,[taskInfo]);	
				} else {
					clientVM.displayError(this.localize('SaveErr') + '[' + respData.message + ']')
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},
	saveExistingTask : function(taskInfo, taskPayload, callback, scope){
		taskPayload.wizardType = this.connectionType;
		this.ajax({
			url: this.API['save'],
			jsonData: taskPayload,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);

				if (respData.success) {					
					callback.apply(scope,[taskInfo]);	
				} else {
					clientVM.displayError(this.localize('SaveErr') + '[' + respData.message + ']')
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	}
})