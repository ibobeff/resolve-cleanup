glu.defModel('RS.wiki.automation.LabelAware', {
	label: '',
	when_label_changed: {
		on: 'labelChanged',
		action: function() {
			if (!this.cell || this.loading || (Ext.isDefined(this.reqCount) && this.reqCount > 0))
				return;
			this.parentVM.fireEvent('celllabelchanged', this.cell, this.label || '');
		}
	}
})