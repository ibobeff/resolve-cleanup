glu.defModel('RS.wiki.automation.ModelXml', {
	resolveModelType: '',
	xml: '',
	originalXml: '',
	title$: function() {
		return this.localize('xmlTitle', this.localize(this.resolveModelType));
	},
	/*
	save: function() {
		this.parentVM.save();
		this.cancel();
	},
	*/
	update: function() {
		this.doClose();
	},
	cancel: function() {
		this.parentVM.automation.activeItem.set('xml', this.originalXml);
		this.doClose();
	},
	beforeClose: function() {
		this.parentVM.automation.activeItem.set('xml', this.originalXml);
	},
	afterClose: function() {}
})