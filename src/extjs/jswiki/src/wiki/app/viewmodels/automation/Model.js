glu.defModel('RS.wiki.automation.ModelFactory', function(cfg) {
	return Ext.applyIf(cfg, {
		xml: '',
		shouldUpdateModel: true,
		isDirtyFlagCheckingOn : false,
		originalXml : '',
		modelXml: {},
		when_xml_changed: {
			on: ['xmlChanged'],
			action: function() {
				if(this.isDirtyFlagCheckingOn) {
					if (this.originalXml != this.xml) {
						// By default, the model will contain the empty root elements:
						//  <mxGraphModel>
						//    <root>
						//	    <mxCell id="0"/>
						//	    <mxCell id="1" parent="0"/>
						//    </root>
						//  </mxGraphModel>
						if ((this.originalXml == '' && encodeURI(this.xml) == '%3CmxGraphModel%3E%0A%20%20%3Croot%3E%0A%20%20%20%20%3CmxCell%20id=%220%22/%3E%0A%20%20%20%20%3CmxCell%20id=%221%22%20parent=%220%22/%3E%0A%20%20%3C/root%3E%0A%3C/mxGraphModel%3E%0A') ||
							(this.xml == '' && encodeURI(this.originalXml) == '%3CmxGraphModel%3E%0A%20%20%3Croot%3E%0A%20%20%20%20%3CmxCell%20id=%220%22/%3E%0A%20%20%20%20%3CmxCell%20id=%221%22%20parent=%220%22/%3E%0A%20%20%3C/root%3E%0A%3C/mxGraphModel%3E%0A'))
						{
							// So empty xml document is equivalent to xml document containing only root elements
							this.clearDirty();
						} else {
							this.notifyDirty();
						}
					} else {
						this.clearDirty();
					}
				}
				this.set('modelXml', {
					xml: this.xml,
					shouldUpdateModel: this.shouldUpdateModel
				});
			}
		},
		currentCellId: '',
		cells: null,
		cellStyleFormat : {
			fontFamily : 'Helvetica',
			fontSize : 8,
			reset: function() {
				this.fontFamily = 'Helvetica';
				this.fontSize = 8;
			}
		},
		activeItem: '',
		cellProperties: {
			mtype: 'activatorlist',
			focusProperty: 'activeItem',
			autoParent: true
		},
		panIsPressed: false,
		initEventWatchers: function() {
			this.on('selectclicked', function() {
				this.set('panIsPressed', false);
			});
			this.on('panclicked', function() {
				this.set('panIsPressed', true);
			});
			this.on('removeclicked', function(graph) {
				this.removeClicked(graph);
			});
		},
		parseCDATA: function(cdata) {
			if (!cdata)
				return [];
			try {
				return Ext.decode(cdata);
			} catch (e) {
				return [];
			}
		},
		parse: function(cell) {
			var result = {};

			function extract(node) {
				Ext.each(node.childNodes, function(child) {
					if (child.tagName) {
						var text = child.textContent || child.innerText || '';
						if (child.tagName.toLowerCase() == 'inputs')
							result['modelInputs'] = this.parseCDATA(text);
						if (child.tagName.toLowerCase() == 'outputs')
							result['modelOutputs'] = this.parseCDATA(text);
						if (child.tagName.toLowerCase() == 'name')
							result['taskName'] = Ext.String.trim(text);
					}
					extract.call(this, child);
				}, this);
			}
			extract.call(this, cell);
			if (!result.taskName)
				result.taskName = (cell.getAttribute('description') || '').split('?')[0]
			return result;
		},
		parseRunbookCell: function(cell) {
			var result = {};
			var runbookName = cell.getAttribute('description');
			if (runbookName) {
				result.runbookName = runbookName;
			} else {
				runbookName = cell.getAttribute('label');
				result.runbookName = (runbookName == 'Runbook') ? '' : runbookName;
			}
			return result;
		},
		showMenu: function(panel, graph, x, y, cell) {},
		selectAnswerProperty: function(cell) {},
		updatePropertiesModelChanged: function() {
			if (this.cellSelected && this.cells) {
				this.cellSelectionChanged(this.cells);
			}
		}, 
		splitEdgeFlag: false,
		splitEdge: function() {
			this.splitEdgeFlag = true;
			// if adding a vertex and splitting an edge, it'll be handled in the populate function of that element
			if (!this.addVertexFlag && this.activeItem && Ext.isFunction(this.activeItem.checkActionTaskParamDependency)) {
				this.activeItem.checkActionTaskParamDependency(1);
			}
		},
		addVertexFlag: false,
		addVertex: function() {
			this.addVertexFlag = true;
		},
		revertTaskNameChange: function() {
			// revert the task name that was just temporary changed
			this.activeItem.revertTaskNameInProgress = true;
			this.activeItem.set('label', this.activeItem.oldTaskLabel);
			this.activeItem.set('taskName', this.activeItem.oldTaskName);
			this.activeItem.taskStore.load();
		},
		revertSplitEdge: function() {
			// hide detailed properties if zero or multiple cells selected
		},
		revertDropElementSplitElement: function() {
			this.set('activeItem', this.cellProperties.length - 1);
		},
		refreshSelectedCellVersion: function(cells) {
			this.cellSelectionChanged([cells], true);
		},
		cellSelectionChanged: function(cells, versionChangedOnly) {
			versionChangedOnly = (typeof versionChangedOnly == 'boolean' && versionChangedOnly) ? true : false;
			if (cells && cells.length) {
				this.set('cells', cells);
				var me = this;
				var task = new Ext.util.DelayedTask(function() {
					this.parentVM.activeActionTaskCleanUpCheck(function() {
						me.doCellSelectionChanged(cells, versionChangedOnly);
					});
				}, this);
				task.delay(100);
			} else {
				this.doCellSelectionChanged(cells, versionChangedOnly);
			}
		},
		versionChangedOnly: false,
		doCellSelectionChanged: function(cells, versionChangedOnly) {
			this.set('versionChangedOnly', versionChangedOnly);
			this.set('cellSelected', !!cells.length);
			if (cells.length != 1) {
				// hide detailed properties if zero or multiple cells selected
				this.set('activeItem', this.cellProperties.length - 1);
				return;
			}
			var cell = cells[0];
			var tagName = cell.value.tagName.toLowerCase();
			if (tagName == 'edge')
				tagName = 'connector';
			else if (tagName == 'subprocess') {
				var result = this.parseRunbookCell(cell);
				if (result.runbookName == '') {
					tagName = 'newrunbook';
				} else {
					tagName = 'runbook';
				}
			}
			else if (tagName == 'task') {
				var result = this.parse(cell);
				if (result.taskName == '') {
					tagName = 'newtask';
				}
				else
					tagName = 'taskproperty'
			}

			if (cell.getValue().getAttribute('noProperty') === 'true') {
				// hide detailed properties if image with no properties selected
				this.set('activeItem', this.cellProperties.length - 1);
			} else if ((tagName.indexOf('connector') != -1) && (this.viewmodelName == 'DecisionTree')) {
				this.selectAnswerProperty(cell);
			} else {
				this.cellProperties.each(function(p) {
					if (p.viewmodelName.toLowerCase().indexOf(tagName) != -1) {
						this.set('activeItem', p);
						p.populate(cell);
						return false;
					}
				}, this);
			}
	
			this.cellStyleFormat.reset();
	
			var cellstyles = cell.style.split(';');
			Ext.Array.forEach(cellstyles, function(cellstyle) {
				var style = cellstyle.split('=');
				if (this.cellStyleFormat.hasOwnProperty(style[0])) {
					this.cellStyleFormat[style[0]] = style[1];
				}
			}, this);
	
			this.fireEvent('cellSelectionChangedUpdateFormat');
			this.set('currentCellId', cell.id);
			this.set('versionChangedOnly', false);
		},
		graph: null,
		removeClicked: function(graph) {
			this.set('graph', graph);
			var cell = graph.getSelectionCell();
			if (!cell) {
				return;
			}

			var cellParams = this.parse(cell.getValue());
			var targetInputParamList = [];
			var cellOutputParamList = [];
			var dependencyTagName = '';
			var source = cell.source;

			// if this cell to be removed is a task, need to check for dependencies
			if (cell.getValue().tagName.toLowerCase() == 'task') {

				// get cell's output parameters list
				var actiontask = this.parentVM.getActionTaskContent(cellParams.taskName, cellParams.version);
				if (actiontask) {
					var paramList = actiontask.data.resolveActionInvoc.resolveActionParameters;
					for(var i=0, l=paramList.length; i<l ; i++){
						if(paramList[i].utype == 'OUTPUT'){
							cellOutputParamList.push(paramList[i].uname);
						}
					}
				}

				// if this cell has output parameters, check target
				if (cellOutputParamList.length) {

					// traverse all edges and if target is also task, retrieve all target's input params
					Ext.each(cell.edges, function(edge) {
						if (edge.source != cell)
							return;
						var target = edge.target;
						var targetValue = target ? target.getValue() : '';
						if (!(targetValue && (targetValue.tagName.toLowerCase() == 'task' || targetValue.tagName.toLowerCase() == 'start')))
							return;
						var targetParams = this.parse(targetValue);
						targetInputParamList = targetInputParamList.concat(this.getTargetInputParams(targetParams));
					}, this);
	
					// if any target has input params, check if they have dependencies on this cell's output parameters
					if (targetInputParamList.length) {
						for(var i=0, l=cellOutputParamList.length; i<l ; i++){
							if (targetInputParamList.indexOf(cellOutputParamList[i]) != -1) {
								dependencyTagName = 'task';
								break;
							}
						}
					}
				}
			} 
			// if cell clicked is an edge
			else if (cell.getValue().tagName.toLowerCase() == 'edge' && source) {
				var sourceValue = source ? source.getValue() : '';
				if (!(sourceValue && (sourceValue.tagName.toLowerCase() == 'task' || sourceValue.tagName.toLowerCase() == 'start'))) {
					this.doRemoveCell();
					return;
				}
				var sourceParams = this.parse(sourceValue);
				var target = cell.target;

				this.getTask(sourceParams.taskName, function(actiontask) {
					if (actiontask) {
						var paramList = actiontask.resolveActionInvoc.resolveActionParameters;
						for(var i=0, l=paramList.length; i<l ; i++){
							if(paramList[i].utype == 'OUTPUT'){
								cellOutputParamList.push(paramList[i].uname);
							}
						}
						// if this cell's source has output parameters, check target
						if (cellOutputParamList.length && target) {
							var targetValue = target ? target.getValue() : '';
							if (!(targetValue && (targetValue.tagName.toLowerCase() == 'task' || targetValue.tagName.toLowerCase() == 'start'))) {
								this.doRemoveCell();
								return;
							}
							var targetParams = this.parse(targetValue);
							targetInputParamList = targetInputParamList.concat(this.getTargetInputParams(targetParams));
		
							// if any target has input params, check if they have dependencies on this cell's source output parameters
							if (targetInputParamList.length) {
								for(var i=0, l=cellOutputParamList.length; i<l ; i++){
									if (targetInputParamList.indexOf(cellOutputParamList[i]) != -1) {
										dependencyTagName = 'edge';
										break;
									}
								}
							}
						}
					}
					if (dependencyTagName) {
						this.displayDependencyWarningMsg(dependencyTagName);
					} else {
						this.doRemoveCell();
					}
				}.bind(this))

				// just exit since dependency checks has already done inside the callback of this.getTask()
				return;
			}

			if (dependencyTagName) {
				this.displayDependencyWarningMsg(dependencyTagName);
			} else {
				this.doRemoveCell();
			}
		},
		displayDependencyWarningMsg: function(tagName) {
			var msg = '';
			if (tagName == 'task') {
				msg = this.localize('automationModelDependencyModifyTask', this.localize('deleting'));
			} else if (tagName == 'edge') {
				msg = this.localize('automationModelDependencyDeleteEdge');
			} else {
				msg = this.localize('automationModelDependency');
			}
			
			this.message({
				title: this.localize('warning'),
				msg: msg,
				buttons: Ext.MessageBox.YESNO,
				buttonText: {
					yes: this.localize('confirm'),
					no: this.localize('cancel')
				},
				scope: this,
				fn: function(btn) {
					if (btn == 'yes') {
						this.doRemoveCell();
					}
				},
			});
		},
		getTargetInputParams: function(targetParams) {
			// parse for target's input parameter name from encoded parameter, eg "$OUTPUT{sys_id}" will return "sys_id"
			var targetInputParamList = [];
			for (key in targetParams.modelInputs) {
				var modelInput = targetParams.modelInputs[key];
				if (modelInput.indexOf('$OUTPUT') != -1) {
					var paramName = modelInput.substring(8, modelInput.lastIndexOf('}'));
					targetInputParamList.push(paramName);
				}
			}
			return targetInputParamList;
		},
		doRemoveCell: function(graph) {
			this.fireEvent('beforeRemoveCell');
			this.removeSelectedCellDependencies();
			this.graph.removeCells();
			this.fireEvent('updateControls');
		},
		removeSelectedCellDependencies: function() {
			if (this.cells) {
				for (var i=0; i< this.cells.length; i++) {
					var cell = this.cells[i];
					var tagName = cell.value.tagName.toLowerCase();
					if (tagName == 'task') {
						var result = this.parse(cell);
						if (Ext.isFunction(this.activeItem.getWizardTaskFromList) && this.activeItem.getWizardTaskFromList(cell.id)) {
							this.activeItem.removeWizardTaskFromList(cell.id);
						}
					}
				}
			}
		},
		getTask: function(taskName, callback) {
			this.ajax({
				url: '/resolve/service/actiontask/get',
				params: {
					id: '',
					name: taskName
				},
				success: function(resp) {
					var respData = RS.common.parsePayload(resp);
					if (!respData.success) {
						callback();
					} else {
						var actiontaskbuilder = this.model({
							mtype: 'RS.actiontask.ActionTask',
							embed: true
						});
						actiontaskbuilder.successfulActionTaskLoad(respData);

						callback(respData.data);
					}
				},
				failure: function(resp) {
					 clientVM.displayFailure(resp);
				}
			});
		},
		modelChanged: function(xml) {
			this.set('shouldUpdateModel', false);
			this.set('xml', xml);
			this.set('shouldUpdateModel', true);
		},
		labelChangedInline: function(cell) {
			if (!this.activeItem)
				return;
			if (cell == this.activeItem.cell) {
				this.activeItem.set('label', cell.getAttribute('label'));
			}
		},
		showToolTip: function(cell, callback) {
			callback();
		},
		showImagePicker: false,
		cellSelected: false,
		elementTab: function() {
			this.set('showImagePicker', false);
		},
		elementTabIsPressed$: function() {
			return !this.showImagePicker;
		},
		imageTab: function() {
			this.set('showImagePicker', true);
		},
		imageTabIsPressed$: function() {
			return this.showImagePicker;
		},
		entityTitle$: function() {
			if (this.showImagePicker)
				return this.localize('images');
			return this.localize('elements');
		},
		connectionDrop : function(connectionType, event, graph){
			this.set('connectionType', connectionType);		
			var xPos = graph.getPointForEvent(event)['x'];
			var yPos = Math.max(graph.getPointForEvent(event)['y'], 300); 
			this.set('dropPos', {x : xPos, y : yPos });		
			this.open({
				mtype : 'RS.wiki.automation.' + connectionType + 'ConnectionConfig'			
			})
		},
		connectionType : null,
		dropPos : null,		
		renderConnection : function(params){
			var connectionType = this.connectionType;			
			var xPos = this.dropPos['x'];
			var yPos = this.dropPos['y'];
			var xmlTemplate = RS.wiki.automation.ConnectionXMLTemplate;
			var domParser = new DOMParser();
			var doc = domParser.parseFromString(this.xml || xmlTemplate['rootNode'] , 'text/xml');
			var root = doc.getElementsByTagName('root');

			var highestId = 0;
			for(var i = 0; i < root[0].childNodes.length;i++){
				var currentChild = root[0].childNodes[i];
				if(currentChild.nodeType == 1)
					highestId = Math.max(currentChild.getAttribute('id'), highestId);
			}
			highestId++;		

			//NODES
			//Start Node
			var startTasks = doc.getElementsByTagName('Start');
			if(startTasks.length > 0)
				var startNode = startTasks[0];
			else
			{
				var startNodeXMLStr = xmlTemplate['startNode'];
				startNodeXMLStr = startNodeXMLStr.replace("<==ID_FIELD==>", highestId);
				startNodeXMLStr = startNodeXMLStr.replace("<==X_POS==>", xPos - 15);
				startNodeXMLStr = startNodeXMLStr.replace("<==Y_POS==>", yPos - 250);			
				highestId++;
				var startNode = domParser.parseFromString(startNodeXMLStr , 'text/xml').childNodes[0];
				root[0].appendChild(startNode);
			}			

			//End Node
			var endTasks = doc.getElementsByTagName('End');
			if(endTasks.length > 0)
				var endNode = endTasks[0];
			else
			{
				var endNodeXMLStr = xmlTemplate['endNode'];
				endNodeXMLStr = endNodeXMLStr.replace("<==ID_FIELD==>", highestId);
				endNodeXMLStr = endNodeXMLStr.replace("<==X_POS==>", xPos - 15);
				endNodeXMLStr = endNodeXMLStr.replace("<==Y_POS==>", yPos + 250);
				highestId++;
				var endNode = domParser.parseFromString(endNodeXMLStr , 'text/xml').childNodes[0];
				root[0].appendChild(endNode);
			}			

			//Start Connection Node			
			var connectionParam = params['connectionInfo']['param'];		
			var connectionStartNode = this.buildTaskNode(connectionType.toLowerCase() + 'Connect', 'resolve', highestId,xPos - 50, yPos - 175, connectionParam);
			highestId++;
			root[0].appendChild(connectionStartNode);
			
			//Template Task			
			if( params.hasOwnProperty('taskInfo')){
				var templateTaskName = params['taskInfo']['name'];
				var templateTaskNamespace = params['taskInfo']['namespace'];
				var templateTaskParam = params['taskInfo']['params'];			
				var templateTaskNode = this.buildTaskNode(templateTaskName, templateTaskNamespace , highestId, xPos - 50, yPos , templateTaskParam);
				highestId++;
				root[0].appendChild(templateTaskNode);
			}
			
			//End Connection Node				
			var endConnectionParam = {sessionName : params['connectionInfo']['param']['sessionName']};
			var connectionEndNode = this.buildTaskNode(connectionType.toLowerCase() + 'Disconnect', 'resolve', highestId,xPos - 50, yPos + 175,endConnectionParam);
			highestId++;
			root[0].appendChild(connectionEndNode);

			//EDGES
			//Start node to connect node.
			var startToConnectionEdge = this.buildEdge(startNode, connectionStartNode, highestId);
			highestId++;
			root[0].appendChild(startToConnectionEdge);
			
			if( params.hasOwnProperty('taskInfo')){
				//Connect node to template task node.
				var connectionToTaskEdge = this.buildEdge(connectionStartNode, templateTaskNode, highestId, {straight: true, condition : 'good'});
				highestId++;
				root[0].appendChild(connectionToTaskEdge);

				//Template Task node to disconnect node.
				var taskToDisconnectionEdge = this.buildEdge(templateTaskNode, connectionEndNode, highestId, {straight: true});
				highestId++;
				root[0].appendChild(taskToDisconnectionEdge);
			}

			//Disconnect node to end node
			var connectionToEndEdge = this.buildEdge(connectionEndNode, endNode, highestId, {cornerPoint : 'LOWER'});
			highestId++;
			root[0].appendChild(connectionToEndEdge);

			//Fail connection edge
			var connectionFailEdge = this.buildEdge(connectionStartNode, endNode, highestId, {condition : 'bad', bufferWidth : 100});
			highestId++;
			root[0].appendChild(connectionFailEdge);

			var serializer = new XMLSerializer();
			var newXml = serializer.serializeToString(doc);
			this.set('xml', newXml);
		},
		buildTaskNode : function(name, namespace, id, x, y, param){
			var domParser = new DOMParser();
			var templateTaskNodeXMLStr = RS.wiki.automation.ConnectionXMLTemplate['taskTemplate'];
			var taskParams = param || {};
			templateTaskNodeXMLStr = templateTaskNodeXMLStr.replace("<==ID_FIELD==>", id);
			templateTaskNodeXMLStr = templateTaskNodeXMLStr.replace("<==X_POS==>", x);
			templateTaskNodeXMLStr = templateTaskNodeXMLStr.replace("<==Y_POS==>", y);
			templateTaskNodeXMLStr = templateTaskNodeXMLStr.replace("<==TASK_LABEL==>", name);
			templateTaskNodeXMLStr = templateTaskNodeXMLStr.replace(/<==TASK_FULLNAME==>/g, name + "#" + namespace);
			templateTaskNodeXMLStr = templateTaskNodeXMLStr.replace("<==TASK_PARAM==>", JSON.stringify(taskParams));		
			return domParser.parseFromString(templateTaskNodeXMLStr , 'text/xml').childNodes[0];
		},
		buildEdge : function(sourceNode, targetNode, id, opts){
			var domParser = new DOMParser();
			var opts = opts || {};
			var connectionTemplate = RS.wiki.automation.ConnectionXMLTemplate;
			var edgeTemplate = opts['straight'] ? connectionTemplate['straightEdgeTemplate'] : connectionTemplate['edgeTemplate'];
			
			var sourceNodeID = sourceNode.getAttribute('id');
			var targetNodeID = targetNode.getAttribute('id');
			edgeTemplate = edgeTemplate.replace('<==ID_FIELD==>', id);
			edgeTemplate = edgeTemplate.replace('<==SOURCE_NODE_ID==>', sourceNodeID);
			edgeTemplate = edgeTemplate.replace('<==TARGET_NODE_ID==>', targetNodeID);

			if(!opts['straight']){
				var sourceGeometry = sourceNode.getElementsByTagName('mxGeometry')[0];
				var targetGeometry = targetNode.getElementsByTagName('mxGeometry')[0];
				var sourceX = parseInt(sourceGeometry.getAttribute('x'));
				var sourceY = parseInt(sourceGeometry.getAttribute('y'));
				var sourceWidth = parseInt(sourceGeometry.getAttribute('width'));
				var sourceHeight = parseInt(sourceGeometry.getAttribute('height'));
				var targetX = parseInt(targetGeometry.getAttribute('x'));
				var targetY = parseInt(targetGeometry.getAttribute('y'));
				var targetWidth = parseInt(targetGeometry.getAttribute('width'));
				var targetHeight = parseInt(targetGeometry.getAttribute('height'));
				var edgeSourcePtsX = sourceX + sourceWidth / 2;
				var edgeSourcePtsY = sourceY + sourceHeight / 2;
				var edgeTargetPtsX = targetX + targetWidth / 2;
				var edgeTargetPtsY = targetY + targetHeight / 2;
				var cornerPointY = opts['cornerPoint'] == "LOWER" ? Math.max(edgeSourcePtsY,edgeTargetPtsY) : Math.min(edgeSourcePtsY,edgeTargetPtsY);
			
				//To make it not overlapping other edge.
				var bufferWidth =  opts['bufferWidth'] || 0;
				edgeTemplate = edgeTemplate.replace('<==SOURCE_POINT_X==>', edgeSourcePtsX);
				edgeTemplate = edgeTemplate.replace('<==SOURCE_POINT_Y==>', edgeSourcePtsY);
				edgeTemplate = edgeTemplate.replace('<==TARGET_POINT_X==>', edgeTargetPtsX);
				edgeTemplate = edgeTemplate.replace('<==TARGET_POINT_Y==>', edgeTargetPtsY);
				edgeTemplate = edgeTemplate.replace('<==CORNER_POINT_Y==>', cornerPointY);
				edgeTemplate = edgeTemplate.replace('<==CORNER_POINT_X==>', (cornerPointY == edgeSourcePtsY ? edgeTargetPtsX : edgeSourcePtsX) + bufferWidth);
			}
			var edgeStyle = opts['straight'] ? 'straight;noEdgeStyle=1;' : 'horizontal;';
			switch(opts['condition']){
				case 'good' : 
					edgeStyle += 'strokeColor=green;';
					break;
				case 'bad' : 
					edgeStyle += 'strokeColor=red;';
					break;
				default :
					break;
			}
			edgeTemplate = edgeTemplate.replace('<==EDGE_STYLE==>', edgeStyle);
			return domParser.parseFromString(edgeTemplate , 'text/xml').childNodes[0];
		},

		// default colors
		fontColor: '000000',
		fontLineColor: 'A9A9A9',
		fontBgColor: '969696',
	
		fontColorIcon$: function() {
			return 'rs-fontcolor-icon hex'+this.fontColor;
		},
		lineColorIcon$: function() {
			return 'rs-linecolor-icon hex'+this.fontLineColor;
		},
		fillColorIcon$: function() {
			return 'rs-fillcolor-icon hex'+this.fontBgColor;
		},
		updateElementFontColor: function(color) {
			this.set('fontColor', color);
		},
		updateElementLineColor: function(color) {
			this.set('fontLineColor', color);
		},
		updateElementFillColor: function(color) {
			this.set('fontBgColor', color);
		},
		resetElementStyle: function() {
			this.set('fontColor', '000000');
			this.set('fontLineColor', 'A9A9A9');
			this.set('fontBgColor', '969696');
		},
		notifyDirty: function() {
			if (Ext.isFunction(this.rootVM.notifyDirtyFlag)) {
				this.rootVM.notifyDirtyFlag(this.viewmodelName);
			}
		},
		clearDirty: function() {
			if (Ext.isFunction(this.rootVM.clearComponentDirtyFlag)) {
				this.rootVM.clearComponentDirtyFlag(this.viewmodelName);
			}
		},
		getTaskTip: function(cell, callback) {
			var result = this.parse(cell);
			var taskName = result.taskName;
			if (!taskName)
				return callback();
			var version = cell.getAttribute('version');

			var actiontask = this.parentVM.getActionTaskContent(taskName, version);
			if (actiontask) {
				callback(actiontask.data.usummary);
			} else {
				var params = {
					id: '',
					name: taskName
				};
				/* TODO - support Version Control
				if (version) {
					Ext.apply(params, {
						version: version
					})
				}
				*/
				this.ajax({
					url: '/resolve/service/actiontask/get',
					params: params,
					success: function(resp) {
						var respData = RS.common.parsePayload(resp);
						if (!respData.success) {
							callback();
						} else {
							this.parentVM.setActionTaskContent(taskName, version, respData);
	
							callback(respData.data.usummary);
						}
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				});
			}
		},
		getRunbookTip: function(cell, callback) {
			var params = {
				id: '',
				name: cell.getAttribute('runbook') || cell.getAttribute('description') || cell.getAttribute('label')
			};
			/* TODO - support Version Control
			var version = cell.getAttribute('version');
			if (version) {
				Ext.apply(params, {
					version: version
				})
			}
			*/
			this.ajax({
				url: '/resolve/service/wiki/get',
				params: params,
				success: function(resp) {
					var respData = RS.common.parsePayload(resp);
					if (!respData.success) {
						callback();
						return;
					}
					callback(respData.data.usummary);
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		},
	});
});
