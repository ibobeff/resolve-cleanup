glu.defModel('RS.wiki.automation.ImageUploader', {
	mixins: ['FileUploadManager'],
	api: {
		upload: '/resolve/service/wiki/uploadAutomationImage',
		list: ''
	},

	allowedExtensions: ['jpg', 'jpeg', 'gif', 'png'],
	fineUploaderTemplateHidden: false,
	checkForDuplicates: true,
	allowMultipleFiles: false,

	sizeLimit: 40000,	// 40KB
	autoUpload: false,

	uploaderAdditionalClass: 'automation',

	init: function() {
		this.set('dragHereText', this.localize('dragImageHere'));
	},

	images: null,

	upload: function() {
	},

	fileInfo: '',
	imageLabel: '',
	fileSize: '',

	fileOnCancelCallback: function(manager, id, filename) {
		if (manager.getFile(id)) {
			this.set('fileInfo', '');
		}
	},

	fileOnSubmittedCallback: function(manager, id, filename) {
		if (manager.getFile(id)) {
			var fileSz = Math.ceil(manager.getSize(id)/1024);
			this.set('fileSize', fileSz);
			this.set('fileInfo', filename + ' / ' + fileSz + ' KB');
		}
	},

	fileOnCompleteCallback: function(manager, filename, response, xhr) {
		this.fileUploaded.call(this, filename, this.view, response);
		this.close();
	},

	submit: function() {
		this.uploader.setParams({
			imageName: this.imageLabel
		});
		this.uploader.uploadStoredFiles();
	},

	fileUploaded: function(file, form, resp) {
		var imageLabel = form.getForm().getFieldValues().imageLabel.trim(),
			tokens = file.split('.'),
			fileExt = tokens[tokens.length-1],
			mainVM = this.parentVM.parentVM.parentVM,
			imgPickers = [
				mainVM.mainModel.mainModelImagePicker,
				mainVM.abortModel.abortModelImagePicker,
				mainVM.decisionTree.decisionTreeImagePicker
			];

		for (var i=0; i<imgPickers.length; i++) {
			var model = glu.model('RS.wiki.automation.ModelImage', {
				elementType: imageLabel,
				elementImg: '/resolve/jsp/model/images/resolve/custom/' + encodeURIComponent(imageLabel+'.'+fileExt),
				elementWidth: 50,
				elementHeight: 50,
				customImg: true
			});
			imgPickers[i].elements.add(model);
			model.on('imageClicked', imgPickers[i].imageClicked, imgPickers[i]);
		}
	},

	view: null,
	viewRenderCompleted: function(view) {
		this.set('view', view);
	}
})

glu.defModel('RS.wiki.automation.FileUploadManagerFactory', function(cfg) {
	Ext.applyIf(cfg, RS.common.viewmodels.FileUploadManager);
	return cfg;
});

/*
glu.defModel('RS.wiki.automation.ImageUploader', {

	images: null,

	fileUploaded: function(file, form, resp) {

		var imageLabel = form.getForm().getFieldValues().imageLabel.trim(),
			tokens = file.name.split('.'),
			fileExt = tokens[tokens.length-1],
			mainVM = this.parentVM.parentVM.parentVM,
			imgPickers = [
				mainVM.mainModel.mainModelImagePicker,
				mainVM.abortModel.abortModelImagePicker,
				mainVM.decisionTree.decisionTreeImagePicker
			];

		for (var i=0; i<imgPickers.length; i++) {
			var model = glu.model('RS.wiki.automation.ModelImage', {
				elementType: imageLabel,
				elementImg: '/resolve/jsp/model/images/resolve/custom/' + encodeURIComponent(imageLabel+'.'+fileExt),
				elementWidth: 50,
				elementHeight: 50,
				customImg: true
			});
			imgPickers[i].elements.add(model);
			model.on('imageClicked', imgPickers[i].imageClicked, imgPickers[i]);
		}
	},

	afterRender: function(form) {
		var me = form,
			vm = this,
			label = form.getForm().getFieldValues().imageLabel,
			uploadButton = me.down('#uploadBtn'),
			browseButton = me.down('#browseBtn'),
			cancelBnt = me.down('#cancelBnt'),
			progressBar  = me.down('#progressbar');

		var uploader = new plupload.Uploader({
			multi_selection: false,
			browse_button: browseButton.getId(),
			url: '/resolve/service/wiki/uploadAutomationImage',
			filters: {
				mime_types: [{
					title: 'Image Files',
					extensions: 'jpg,jpeg,gif,png'
				}]
			}
		});

		uploader.init();
		uploader.bind('FilesAdded', function(up, files) {
			var fileSz = Math.ceil(files[0].size/1024);

			me.down('#fileSize').setValue(fileSz);
			me.down('#fileInfo').setValue(files[0].name + ' / ' + fileSz+' KB', false);
			if (up.files.length > 1) {
				up.splice(0, up.files.length-1);   // Only allow single file upload
			}

			if (fileSz > 40) {
				me.down('#errMsg').setValue(vm.localize('fileExceedsLime'));
			} else {
				me.down('#errMsg').setValue('');
				if (me.isValid()) {
					up.setOption('multipart_params', {
							imageName: form.getForm().getFieldValues().imageLabel.trim()
						}
					);
					up.start();
					setTimeout(function() {
						if (!uploadButton.isDestroyed) {
							uploadButton.setDisabled(true);
							browseButton.setDisabled(true);
							cancelBnt.setDisabled(true);
							up.disableBrowse(true);
						}
					}, 500);
					progressBar.wait({
						text: RS.common.locale.updating
					});
				}
			}
		}, vm);

		uploader.bind('Error', function(up, err) {
			me.down('#errMsg').setValue(err.message, false);
			uploadButton.setDisabled(false);
			browseButton.setDisabled(false);
			cancelBnt.setDisabled(false);
			up.disableBrowse(false);
			progressBar.stopAnimation();
		}, vm);

		uploader.bind('FileUploaded', function(up, file, response) {
			if (this.fileUploaded) {
				var jsonResponse = Ext.decode(response.response);
				if (jsonResponse.success) {
					this.fileUploaded.call(this, file, form, jsonResponse);
					clientVM.displaySuccess(RS.wiki.automation.locale.succeededUploadingImage, null,RS.common.locale.succeeded);
				} else {
					clientVM.displayError(RS.wiki.automation.locale.failedUploadingImage, null, RS.common.locale.failed);
				}
				setTimeout(function() {
					form.up('window').close();
				}, 500);
			}
		}, vm);

		uploadButton.on('click', function () {
			uploader.setOption('multipart_params', {
					imageName: form.getForm().getFieldValues().imageLabel.trim()
				}
			);
			uploader.start();
			uploadButton.setDisabled(true);
			browseButton.setDisabled(true);
			cancelBnt.setDisabled(true);
			progressBar.wait({
				text: RS.common.locale.updating
			});
		});
	},

	hasDuplicate: function(label) {
		for (var i=0; i<this.images.length; i++) {
			if (this.images.getAt(i).elementType == label) {
				return true;
			}
		}
		return false;
	},

	discard: function() {
		this.doClose();
	}
})
*/
