glu.defModel('RS.wiki.automation.AnswerProperty', {
	mixins: ['LabelAware'],
	cell: null,
	loading: false,
	connector: '',
	recordInternalId : null,
	showVariableForm : false,	
	answerdataSelections: [],
	expressionSelections : [],
	fromQuestion: false,
	autoAnswer : false,
	textfield : null,
	order: 0,
	noExpression : true,	
	allVariables : {
		INPUT : [],
		OUTPUT : [],
		FLOW : [],
		WSDATA : [],
		PROPERTY : [],
		PARAM : []
	},		
	variableForm : {
		mtype : 'VariableSubstitution',
		valueSyntax : '{0}.{1}',
		typeList : ['WSDATA'],
	},
	connectorStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},
	cdataStore: {
		mtype: 'store',
		fields: ['name', 'value', {name: 'postfixCnt', type: 'int'}],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},	
	expressionStore : {
		mtype : 'store',
		fields : ['variableName','operator','operand','taskName', 'outputName']	
	},
	when_order_changed: {
		on: ['orderChanged'],
		action: function() {
			if (this.loading || !this.cell)
				return
			this.parentVM.fireEvent('dtanswerorderchanged', this.cell, this.order);
		}
	},
	when_expression_store_changed : {
		on : ['expressionStoreChanged'],
		action : function(){
			this.set('noExpression', this.expressionStore.getCount() == 0);
		}
	},
	when_connector_changed: {
		on: ['connectorChanged'],
		action: function() {
			if (this.loading)
				return;
			this.parentVM.fireEvent('connectorchanged', this.connector);
		}
	},
	when_autoAnswer_unchecked : {
		on : ['autoAnswerChanged'],
		action : function(){
			if(!this.autoAnswer)			
				this.persistExpression();			
		}
	},

	init: function() {		
		this.variableForm.dumper = {
			dump : function(newVariableString){		
				var value = (this.label ?  Ext.String.trim(this.label) : '' ) + ' ' + newVariableString;
				this.set('label', Ext.String.trim(value));
			},
			scope : this
		}
		this.expressionStore.on('datachanged', function(){
			this.fireEvent('expressionStoreChanged');
		}, this)

		this.addRootVmEventListerners();
	},

	beforeDestroyComponent: function() {
		this.removeRootVmEventListerners();
	},
	addRootVmEventListerners: function() {
		this.rootVM.on('decisionTreeViewTabIsPressed', this.decisionTreeViewTabIsPressedHandler, this);
	},
	removeRootVmEventListerners: function() {
		this.rootVM.removeListener('decisionTreeViewTabIsPressed', this.decisionTreeViewTabIsPressedHandler, this);
	},

	decisionTreeViewTabIsPressedFlag: false,
	decisionTreeViewTabIsPressedHandler: function(flag) {
		this.set('decisionTreeViewTabIsPressedFlag', flag);
	},

	wsdataList$ : function(){
		this.allVariables.WSDATA = [];
		for(var i = 0; i < this.rootVM.dtVariables.length; i++){
			this.allVariables.WSDATA.push(this.rootVM.dtVariables[i].variableName);
		}
	},
	populate: function(cell) {
		this.resetProperty();
		this.set('loading', true);
		this.set('cell', cell);
		this.set('connector', this.connectorValue());
		this.set('label', cell.getAttribute('label'));
		this.set('order', cell.getAttribute('order'));
		this.populateCDATA();
		this.populateExpression();
		this.set('loading', false);
	},
	resetProperty: function() {
		this.set('cell', null);
		this.set('order', 0);
		this.set('label', '');
		this.cdataStore.removeAll();
		this.counter.resetCounters();
		this.set('noExpression', true);			
		this.set('autoAnswer',false);
		this.expressionStore.removeAll();
	},
	enableAddVariable : function(){
		this.set('showVariableForm', true);
	},
	enableAddVariableIsVisible$ : function(){
		return !this.showVariableForm && !this.decisionTreeViewTabIsPressedFlag;
	},
	connectorValue: function() {
		if (!this.cell.style)
			return 'straight';
		if (this.cell.style.indexOf('noEdgeStyle') != -1)
			return 'straight';
		if (this.cell.style.indexOf('horizontal') != -1)
			return 'horizontal';
		if (this.cell.style.indexOf('vertical') != -1)
			return 'vertical';
		return 'straight';
	},

	//Expression Logic
	populateExpression : function(){
		var value = this.cell.getValue();
		var expressionNode = value.getElementsByTagName('expressions')[0];
		var expressionData = expressionNode ? expressionNode.textContent : null;
		if(expressionData){
			var data = Ext.decode(expressionData);
			if(data && data.length > 0){
				this.expressionStore.loadData(data);
				this.set('autoAnswer', true);
			}
		}
	},
	updateExpression : function(dataObject, recordInternalId){
		var expressionRecord = this.expressionStore.getByInternalId(recordInternalId);
		if(expressionRecord)
			this.expressionStore.remove(expressionRecord);		
		this.expressionStore.add(dataObject);		
		this.persistExpression();
	},
	persistExpression : function() {
		if (this.cell) {
			// 1. Remove existing params
			var value = this.cell.getValue();
			var	expressionNode = value.getElementsByTagName('expressions')[0];
			if (expressionNode) {
				value.removeChild(expressionNode);
			}

			// 2. Save to XML
			//Check for emtpy obj, dont need to add this node if there is no expression.			
			var expressions = this.expressionStore.getRange();
			var expressionSet = [];
			Ext.Array.each(expressions, function(expression){
				expressionSet.push(expression.data);
			})
			if(expressionSet.length > 0){
				var xmlDoc = mxUtils.createXmlDocument();
				var paramNode = xmlDoc.createElement('expressions');
				var cdataNode = xmlDoc.createCDATASection(Ext.encode(expressionSet));
				paramNode.appendChild(cdataNode);
				this.cell.getValue().appendChild(paramNode);
			}
			this.parentVM.fireEvent('cdatachanged', this.cell);
		}
	},
	cleanUpWSDATA : function(){
		this.expressionStore.each(function(expression){
			this.rootVM.removeDTVariables(expression.get('variableName'), this.cell.id);
		}, this)
	},
	updateVariable : function(map, oldMap, editMode){		
		var cellID = this.cell.id;
		return editMode ? this.rootVM.updateDTVariablesOnEdit(map, oldMap, cellID) : this.rootVM.updateDTVariables(map, cellID);
	},
	new : function(){
		this.open({
			mtype : 'ExpressionForm',
			editMode : false,
			keyList : this.expressionStore.collect('variableName'),
			allVariables : this.allVariables
		})
	},
	edit : function(){
		this.open({
			mtype : 'ExpressionForm',
			editMode : true,
			originalData : this.expressionSelections[0].data,
			recordInternalId : this.expressionSelections[0].internalId,
			keyList : this.expressionStore.collect('variableName'),
			allVariables : this.allVariables
		})	
	},
	editIsEnabled$ : function(){
		return this.expressionSelections.length > 0;
	},
	remove : function(){	
		var variableName = this.expressionSelections[0].get('variableName');	
		this.expressionStore.remove(this.expressionSelections[0]);
		this.rootVM.removeDTVariables(variableName, this.cell.id);
		this.persistExpression();
	},
	removeIsEnabled$ : function(){
		return this.expressionSelections.length > 0;
	},

	//Additional Data 
	counter: function() {
		var sortedCounters = [],
			prefix = 'Name';
		return {
			getPrefix: function() {
				return prefix;
			},
			resetCounters: function() {
				sortedCounters.splice(0, sortedCounters.length);
			},
			getCurrentCounter: function() {
				if (sortedCounters.length) {
					sortedCounters.push(sortedCounters[sortedCounters.length-1]+1);
				} else {
					sortedCounters.push(1);
				}
				return sortedCounters[sortedCounters.length-1];
			},
			updateCounters: function(store) {
				this.resetCounters();
				store.each(function(record) {
					if (record.get('postfixCnt') != -1) {
						this.push(record.get('postfixCnt'));
					}
				}, sortedCounters);
				sortedCounters.sort(function(a, b) {
					return (a - b);
				});
			},

			getPostfixCnt: function(name) {
				var postfixCnt = -1;
				if (name.match(/^Name[1-9][0-9]*$/)) {
					postfixCnt = parseInt(name.split(/Name/)[1]);
				}
				return postfixCnt;
			}
		}
	}(),
	addData: function() {
		var cnt = this.counter.getCurrentCounter();
		this.cdataStore.add({
			name: this.counter.getPrefix()+cnt,
			value: 'value',
			postfixCnt: cnt
		});
	},
	removeData: function() {
		this.cdataStore.remove(this.answerdataSelections);
		this.counter.updateCounters(this.cdataStore);
	},
	dataUpdated: function(store, record) {
		record.set('postfixCnt', this.counter.getPostfixCnt(record.get('name')));
		store.sync();
		this.updateCDATA();
		this.counter.updateCounters(this.cdataStore);
	},
	removeDataIsEnabled$: function() {
		return this.answerdataSelections.length > 0;
	},
	populateCDATA: function() {
		var value = this.cell.getValue(),
			paramsNode = value.getElementsByTagName('params')[0],
			cdata = paramsNode? paramsNode.textContent: null;
		this.cdataStore.un('datachanged', this.updateCDATA);
		this.cdataStore.un('update'	, this.dataUpdated);
		var data = [];
		if (cdata) {
			Ext.Object.each(Ext.decode(cdata), function(k, v) {
				this.data.push({
					name: k,
					value: v,
					postfixCnt: this.me.counter.getPostfixCnt(k)
				});
			}, {data: data, me: this});
			this.cdataStore.add(data);
			this.counter.updateCounters(this.cdataStore);
		}
		this.cdataStore.on('datachanged', this.updateCDATA, this);
		this.cdataStore.on('update', this.dataUpdated, this);
	},
	updateCDATA: function() {
		if (this.cell) {
			// 1. Remove existing params
			var value = this.cell.getValue(),
				params = value.getElementsByTagName('params')[0],
				o = {}
			if (params) {
				value.removeChild(params);
			}
			// Add new params from the store
			// 2a. Build CDATA from store
			var obj = {};
			this.cdataStore.each(function(record) {
				obj[record.get('name')] = record.get('value');
			}, this);
			// 2b. Add CDATA to the answer
			var xmlDoc = mxUtils.createXmlDocument();
			var paramNode = xmlDoc.createElement('params');
			var cdataNode = xmlDoc.createCDATASection(Ext.encode(obj));
			paramNode.appendChild(cdataNode);
			this.cell.getValue().appendChild(paramNode);
			this.parentVM.fireEvent('cdatachanged', this.cell);
		}
	},	
	
});
glu.defModel('RS.wiki.automation.ExpressionForm',{
	fields  : ['variableName','taskName','operand','operator','outputName'],
	originalData : {},
	editMode : false,
	duplicatedKeyIsHidden : true,		
	outputFromTask : false,	
	keyList : [],
	allVariables : {
		INPUT : [],
		OUTPUT : [],
		FLOW : [],
		WSDATA : [],
		PROPERTY : [],
		PARAM : []
	},	
	operandType : 'CONSTANT',
	outputStore : {
		mtype : 'store',
		fields : ['uname'],
		proxy : {
			type : 'memory'
		}
	},
	operatorStore: {
		mtype : 'store',
		fields : ['text', 'value'],
		proxy : {
			type : 'memory'
		},
		data : [{
			text : '=',
			value : '='
		},{
			text :'!=',
			value : '!='
		},{
			text : '>',
			value : '>'
		},{
			text : '>=',
			value : '>='
		},{
			text : '<',
			value : '<'
		},{
			text : '<=',
			value : '<='
		}]
	},
	wskeyStore : {
		mtype : 'store',
		fields : [{
			name : 'text',
			convert : function(val, record){
				return record.raw;
			}
		}]
	},
	taskStore: {
		mtype: 'store',
		fields: ['ufullName',{
			name : 'output',
			mapping : 'resolveActionInvoc',
			convert : function(v, record){
				var parameters = v.resolveActionParameters;
				var output = [];
				Ext.Array.each(parameters,function(parameter){
					if(parameter.utype.toLowerCase() == 'output')
						output.push(parameter);
				})
				return output;
			}
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/actiontask/list',
			reader: {
				type: 'json',
				root: 'records'
			},			
			extraParams: {
				delim: '/',
				id: 'root_menu_id'
			}
		}
	},	
	operandTypeStore : {
		mtype : 'store',
		fields : [{
			name : 'type',
			convert  : function(value, record){
				return record.raw;
			}
		}],
		data : ['CONSTANT','WSDATA']
	},
	operandStore : {
		mtype : 'store',
		fields : [{
			name : 'text',
			convert  : function(value, record){
				return record.raw;
			}
		}]
	},
	when_operandType_changed : {
		on : ['operandTypeChanged'],
		action : function(newVal){
			if(newVal == 'WSDATA'){
				this.set('operand', '');
				this.operandStore.loadData(this.allVariables.WSDATA);
			}
		}
	},
	init : function(){		
		this.taskStore.on('beforeload', function(store, op) {
			op.params = op.params || {}
			Ext.apply(op.params, {
				filter: Ext.encode([{
					field: 'ufullName',
					condition: 'contains',
					type: 'auto',
					value: op.params.query || this.taskName
				}])
			})
		}, this);
		if(this.editMode && this.originalData){
			var matcher = this.originalData.operand.match(/WSDATA\{(.*)\}/);
			if(matcher){
				this.originalData.operand = matcher[1];
				this.set('operandType', 'WSDATA');
			}
			if(this.originalData.taskName){
				this.set('outputFromTask', true);	
				this.taskName = this.originalData.taskName;
				this.taskStore.load({
					scope : this,
					callback : function(){
						this.loadData(this.originalData);
					}
				});	
			}
			else
				this.loadData(this.originalData);			
		}
		this.wskeyStore.loadData(this.allVariables.WSDATA);
	},
	operandIsText$ : function(){
		return this.operandType == 'CONSTANT';
	},
	variableNameIsValid$ : function(){
		if(this.originalData.variableName != this.variableName && this.keyList.indexOf(this.variableName) != -1)
			return this.localize('duplicatedExpressionKey');
		else
			return this.variableName && RS.common.validation.VariableName.test(this.variableName)  ? true : this.localize('invalidField');
	},
	operandIsValid$ : function(){
		return (this.operandType == 'WSDATA' ? (RS.common.validation.VariableName.test(this.operand) && this.operand) : this.operand) ? true : this.localize('invalidField');
	},
	outputNameIsValid$ : function(){
		return !this.outputFromTask || this.outputName ? true : this.localize('requiredField');
	},
	taskNameIsValid$ : function(){
		return !this.outputFromTask || (this.taskName && this.validateTaskName(this.taskName)) ? true : this.localize('invalidTaskName');
	},
	updateExpressionBtnText$ : function(){
		return this.editMode ? 'Update' : 'Add';
	},
	when_task_changed : { //This event should be trigger after taskName validation
		on : ['taskNameChanged'],
		action : function(){
			this.outputStore.removeAll();		
			this.set('outputName',null);
			if(this.taskNameIsValid == true){
				//Get all output for this task.
				this.getOutputForTask();
			}
		}
	},
	validateTaskName : function(taskName){
		var r = this.taskStore.findRecord('ufullName', taskName, 0, false, false, true)
		return r;
	},
	operatorIsValid$ : function(){
		return this.operator ? true : this.localize('invalidField');
	},
	expressionFieldSetTitle$ : function(){
		return (!this.editMode ? 'New ' : 'Edit ' ) + this.localize('expression');
	},

	getOutputForTask : function(){	
		var r = this.taskStore.findRecord('ufullName', this.taskName, 0, false, false, true);
		if(r){
			var output = r.get('output') || [];
			this.outputStore.loadData(output);
			if(this.outputStore.getCount() > 0){
				var outputName = this.outputStore.getAt(0).get('uname');
				this.set('outputName', outputName);
			}
		}
	},
	searchTask: function() {
		var currentDocName = this.rootVM.fullName;
		var me = this;
		this.open({
			mtype: 'RS.actiontask.ActionTaskPicker',
			creatable: false,
			currentDocName : currentDocName,
			dumper: {
				dump : function(selections) {
					if (selections.length > 0) {
						var selection = selections[0];
						this.set('taskName', selection.get('ufullName'));
						this.set('taskNameIsValid', true);
						this.taskStore.load({
							scope : this,
							callback : this.getOutputForTask
						});
					}
				},
				scope : this
			}
		})
	},
	updateExpression : function(){
		if(!this.isValid){
			this.set('isPristine',false);
			return;
		}
		
		//Update Variable
		this.set('taskName', this.outputFromTask ? this.taskName : '');
		this.set('outputName', this.outputFromTask ? this.outputName : '');
		var map = {
			variableName : this.variableName,
			taskName : this.taskName,
			outputName : this.outputName,
		}
		var updated = this.parentVM.updateVariable(map, this.originalData, this.editMode);
		if(!updated){
			this.set('duplicatedKeyIsHidden', false);
			return;
		}
		else {
			this.set('operand', this.operandType == 'CONSTANT' ? this.operand : 'WSDATA{' + this.operand + '}');
			this.parentVM.updateExpression(this.asObject(), this.recordInternalId);
			this.doClose();
		}
	},
	cancelAddExpression : function(){	
		this.doClose();
	},
})