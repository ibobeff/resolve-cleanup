// glu.defModel('RS.wiki.automation.StartMenu', {
// 	mixins: ['ElementMenu'],
// 	taskName: 'start#resolve',
// 	modelInputs: [],
// 	init: function() {
// 		function parseCDATA(cdata) {
// 			if (!cdata)
// 				return [];
// 			try {
// 				return Ext.decode(cdata);
// 			} catch (e) {
// 				return [];
// 			}
// 		}
// 		var me = this;

// 		function extract(node) {
// 			Ext.each(node.childNodes, function(child) {
// 				if (child.tagName) {
// 					var text = child.textContent || child.innerText || '';
// 					if (child.tagName.toLowerCase() == 'inputs')
// 						me.set('modelInputs', parseCDATA(text));
// 				}
// 				extract(child);
// 			})
// 		}
// 		extract(this.cellGetter().getValue());
// 	},
// 	editInputs: function() {
// 		var me = this;
// 		this.ajax({
// 			url: '/resolve/service/actiontask/get',
// 			params: {
// 				id: '',
// 				name: this.taskName
// 			},
// 			success: function(resp) {
// 				var respData = RS.common.parsePayload(resp);
// 				if (!respData.success) {
// 					clientVM.displayError(this.localize('getTaskDefaultParamError', respData.message));
// 					return;
// 				}
// 				var params = respData.data.resolveActionInvoc.resolveActionParameters;
// 				var defaultParams = {}
// 				Ext.each(params, function(param) {
// 					if (param.utype.toLowerCase() == 'input')
// 						defaultParams[param.uname] = param.udefaultValue;
// 				}, this)
// 				this.open({
// 					mtype: 'RS.wiki.automation.Parameters',
// 					defaultParams: defaultParams,
// 					modelParams: this.modelInputs,
// 					taskName: this.taskName,
// 					isInput: true,
// 					dumper: function() {
// 						me.set('modelInputs', this.getParamsFromStore());
// 						me.toCDATA(me.modelInputs, 'inputs');
// 					}
// 				});
// 			},
// 			failure: function(resp) {
// 				clientVM.displayFailure(resp);
// 			}
// 		})
// 	},
// 	editProperties: function() {
// 		var me = this;
// 		this.open({
// 			elementType: this.localize('start'),
// 			mtype: 'RS.wiki.automation.ElementProperty',
// 			nodeId: this.cellGetter().id
// 		});
// 	}
// });