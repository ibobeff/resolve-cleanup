RS.wiki.automation.ConnectionXMLTemplate = {
    rootNode : 
    '<mxGraphModel>' +
      '<root>' +
        '<mxCell id="0"/>' +
        '<mxCell id="1" parent="0"/>' +
      '</root>' +
    '</mxGraphModel>',
    startNode : 
    '<Start label="Start" description="start#resolve?" id="<==ID_FIELD==>">' +
      '<name>' +
       'start#resolve' +
      '</name>' +
      '<params>' +
        '<inputs>' +
          '<![CDATA[{"PROCESS_LOOP":"true"}]]>' +
        '</inputs>' +
        '<outputs>' +
          '<![CDATA[{}]]>' +
        '</outputs>' +
      '</params>' +
      '<mxCell style="symbol;image=/resolve/jsp/model/images/symbols/start.png" parent="1" vertex="1">' +
        '<mxGeometry x="<==X_POS==>" y="<==Y_POS==>" width="30" height="30" as="geometry"/>' + 
      '</mxCell>' +
    '</Start>',
    endNode :
    '<End label="End" description="end#resolve?" merge="merge = ANY" id="<==ID_FIELD==>">' +
      '<name>' +
        'end#resolve' +
      '</name>' +
      '<params>' +
        '<inputs>' +
          '<![CDATA[{}]]>' +
        '</inputs>' +
        '<outputs>' +
          '<![CDATA[{}]]>' +
        '</outputs>' +
      '</params>' +
      '<mxCell style="symbol;image=/resolve/jsp/model/images/symbols/end.png;dashed=1" parent="1" vertex="1">' +
        '<mxGeometry x="<==X_POS==>" y="<==Y_POS==>" width="30" height="30" as="geometry"/>' +
      '</mxCell>' +
    '</End>',   
    taskTemplate: 
    '<Task label="<==TASK_LABEL==>" description="<==TASK_FULLNAME==>?" tooltip="undefined" href="<==TASK_FULLNAME==>" detail="" merge="merge = ANY" id="<==ID_FIELD==>">' +
      '<name>' +
        '<==TASK_FULLNAME==>' +
      '</name>' +
      '<params>' +
        '<inputs>' +
          '<![CDATA[<==TASK_PARAM==>]]>' +
        '</inputs>' +
        '<outputs>' +
          '<![CDATA[{}]]>' +
        '</outputs>' +
      '</params>' +
      '<mxCell style="rounded;labelBackgroundColor=none;strokeColor=#A9A9A9;fillColor=#D3D3D3;gradientColor=white;verticalLabelPosition=middle;verticalAlign=middle" parent="1" vertex="1">' +
        '<mxGeometry x="<==X_POS==>" y="<==Y_POS==>" width="100" height="40" as="geometry"/>' + 
      '</mxCell>' + 
    '</Task>',  
    edgeTemplate : 
     '<Edge label="" description="" id="<==ID_FIELD==>">' +
      '<mxCell style="<==EDGE_STYLE==>" parent="1" source="<==SOURCE_NODE_ID==>" target="<==TARGET_NODE_ID==>" edge="1">' +
        '<mxGeometry relative="1" as="geometry">' +
          '<mxPoint x="<==SOURCE_POINT_X==>" y="<==SOURCE_POINT_Y==>" as="sourcePoint"/>' +
          '<mxPoint x="<==TARGET_POINT_X==>" y="<==TARGET_POINT_Y==>" as="targetPoint"/>' +
          '<Array as="points">' +
            '<mxPoint x="<==CORNER_POINT_X==>" y="<==CORNER_POINT_Y==>"/>' +
          '</Array>' +
        '</mxGeometry>' +
      '</mxCell>' +
    '</Edge>',
    straightEdgeTemplate : 
     '<Edge label="" description="" id="<==ID_FIELD==>">' +
      '<mxCell style="<==EDGE_STYLE==>" parent="1" source="<==SOURCE_NODE_ID==>" target="<==TARGET_NODE_ID==>" edge="1">' +
        '<mxGeometry relative="1" as="geometry">' +          
        '</mxGeometry>' +
      '</mxCell>' +
    '</Edge>',
  }
 
