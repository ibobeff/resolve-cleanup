glu.defModel('RS.wiki.automation.ElementMenu', {
	parentPanel: null,
	initX: 0,
	initY: 0,
	cellGetter: function() {},
	toCDATA: function(params, tagName) {
		var paramEl = null;
		var tagEl = false;

		function find(node) {
			Ext.each(node.childNodes, function(child) {
				if (child.tagName) {
					if (child.tagName.toLowerCase() == 'params')
						paramEl = child;
					if (child.tagName.toLowerCase() == tagName) {
						child.innerHTML = '<![CDATA[' + Ext.encode(params) + ']]>'
						tagEl = child;
					}
				}
				find(child);
			})
		}
		find(this.cellGetter().getValue());
		if (tagEl) {
			this.parentVM.fireEvent('cdataChanged');
			return;
		}
		if (paramEl) {
			paramEl.innerHTML = paramEl.innerHTML + '<' + tagName + '><![CDATA[' + Ext.encode(params) + ']]></' + tagName + '>';
			this.parentVM.fireEvent('cdataChanged');
			return;
		}
		this.cellGetter().getValue().innerHTML = '<params><' + tagName + '><![CDATA[' + Ext.encode(params) + ']]></' + tagName + '></params>';
		this.parentVM.fireEvent('cdataChanged');
	},
	mergeValue$: function() {
		if (!this.cellGetter().getAttribute('merge'))
			return 'all';
		return this.cellGetter().getAttribute('merge').split(' ')[2].toLowerCase();
	}
});