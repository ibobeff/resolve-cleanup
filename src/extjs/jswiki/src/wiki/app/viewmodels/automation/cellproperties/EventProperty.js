glu.defModel('RS.wiki.automation.EventProperty', {
	mixins: ['MergeAware', 'TaskProperty', 'LabelAware', 'VersionAware', 'XmlAware', 'ActionTaskValidation'],
	inputs: {
		mtype: 'Parameters'
	},
	cell: null,
	reqCount: 0,
	init: function(){},
	populate: function(cell) {
		this.resetProperty();
		this.populateVersion(cell);
		this.set('loading', true);
		var result = this.parse(cell.getValue());
		var description = unescape(cell.getValue().getAttribute('description'));
		var oldInputs = Ext.Object.fromQueryString(description);
		result.modelInputs = Ext.applyIf(result.modelInputs || {}, oldInputs);
		this.set('cell', cell);
		this.set('label', cell.getAttribute('label'));
		this.populateMerge(cell);
		this.set('taskName', 'event#resolve');
		this.set('reqCount', this.reqCount + 1);
		this.getTask('event#resolve', function(task) {
			this.set('newTask', false);
			this.updateTaskProperty(task);
		}, function() {}, function() {
			this.set('reqCount', this.reqCount - 1);
		});
		this.checkActionTaskParamDependency();
		this.set('loading', false);
	}
});