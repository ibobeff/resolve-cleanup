glu.defModel('RS.wiki.automation.NetworkTaskConfig',{
	fields : ['command', 'prompt','sessionName'],
	taskIsPartOfCreation : true,
	waitingForDataChange : false,
	showVariableForm : false,
	wizardType : 'local',
	variableForm : {
		mtype : 'VariableSubstitution'	
	},	
	update_cell_properties : {
		on : ['sessionNameChanged','commandChanged','promptChanged'],
		action : function(newVal, oldVal, bindingInfo){
			if(this.waitingForDataChange){
				if(this.delayTask)
					clearTimeout(this.delayTask);
				this.delayTask = setTimeout(function(){
					var propName = bindingInfo['modelPropName'];
					this.parentVM.updateValueForProperty(propName, newVal);				
				}.bind(this), 500);
			}
		}
	},
	
	init: function() {
		this.variableForm.dumper = {
			dump : function(newVariableString){
				var value = Ext.String.trim(this.command) + ' ' + newVariableString;
				this.set('command', Ext.String.trim(value));
			},
			scope : this
		}
	},
	updateWizardType : function(wizardType){
		this.set('wizardType', wizardType);
	},
	sessionPromptIsHidden$ : function(){
		return this.taskIsPartOfCreation || this.wizardType.toUpperCase() == 'LOCAL';
	},
	saveVariableList : function(variableType, data){
		this.variableForm.saveVariableList(variableType,data);
	},
	enableAddVariable : function(){
		this.set('showVariableForm', !this.showVariableForm);	
	},		
	getTaskParams : function(){
		var params = this.asObject();
		delete params['id'];
		//Parse command to check if any input is used within the command, the auto generate those inputs.
		Ext.apply(params, this.getInputOptionFromCommand(this.command));
		return params;
	},
	inputParamRegex : /\$INPUT{([a-zA-Z_$][a-zA-Z_$0-9]*)}/g,
	getInputOptionFromCommand : function(command){
		var inputOptions = {};
		var m = null;
		while ((m = this.inputParamRegex.exec(command)) !== null) {
		    // This is necessary to avoid infinite loops with zero-width matches
		    if (m.index === this.inputParamRegex.lastIndex) {
		        this.inputParamRegex.lastIndex++;
		    }
		    
		    // The result can be accessed through the `m`-variable.
		    m.forEach(function(match, groupIndex){
		       if(groupIndex == 1)
		    		inputOptions[match] = ''; //Default to empty (might allow user to define default in the future)
		    });
		}
		return inputOptions;
	},
	resetProperties : function(){
		this.set('command','');	
	},
	updatePrompt : function(newPrompt){
		this.set('prompt', newPrompt);
	},	
	getCurrentConfig : function(){
		return this.asObject();
	},
	loadConfig : function(saveConfig){
		this.loadData(saveConfig);		
	},
	suspendWaitingForDataChange : function(flag){
		this.set('waitingForDataChange', !flag);
	}
})