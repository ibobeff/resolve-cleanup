// glu.defModel('RS.wiki.automation.RunbookMenu', {
// 	mixins: ['ElementMenu'],
// 	searchRunbook: function() {
// 		var me = this;
// 		this.open({
// 			mtype: 'RS.wiki.RunbookPicker',
// 			dumper: {
// 				dump: function(selections) {
// 					if (selections.length > 0) {
// 						var selection = selections[0]
// 						me.parentVM.fireEvent('selectRunbook', selection.get('ufullname'));
// 					}
// 				}
// 			}
// 		})
// 	},
// 	editRunbook: function() {
// 		clientVM.handleNavigation({
// 			modelName: 'RS.wiki.Main',
// 			params: {
// 				name: this.cellGetter().getValue().getAttribute('label')
// 			},
// 			target: '_blank'
// 		});
// 	},
// 	editProperties: function() {

// 		var me = this;
// 		this.open({
// 			elementType: this.localize('runbook'),
// 			mtype: 'RS.wiki.automation.ElementProperty',
// 			nodeId: this.cellGetter().id,
// 			elementName: this.cellGetter().getValue().getAttribute('label')
// 		});
// 	}
// });