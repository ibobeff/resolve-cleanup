glu.defModel('RS.wiki.automation.HTTPConnectionConfig', {
	mixins : ['ConnectionUtils'],
	connectionType : 'http',
	taskWizard : {
		mtype : 'RS.wiki.automation.TaskWizard',
		connectionType : 'http'
	},
	protocolStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},
	sourceStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [{
			name: 'CONSTANT',
			value: 'CONSTANT'
		}, {
			name: 'PARAM',
			value: 'PARAM'
		}, {
			name: 'FLOW',
			value: 'FLOW'
		}, {
			name: 'WSDATA',
			value: 'WSDATA'
		}, {
			name: 'PROPERTY',
			value: 'PROPERTY'
		}]
	},
	propertyStore: {
		mtype: 'store',
		fields: ['uname'],
		sorters: ['uname'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/atproperties/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		},
		pageSize : -1
	},
	paramStore : null,	
	protocol: 'http',
	host: '',
	port: '',
	hostSource: 'CONSTANT',
	portSource: 'CONSTANT',
	sessionName: '',
	when_sessionName_changed : {
		on : ['sessionNameChanged'],
		action : function(){
			this.taskWizard.updateSessionName(this.sessionName);
		}
	},
	hostIsText$: function() {
		return this.hostSource == 'CONSTANT' || this.hostSource == 'FLOW' || this.hostSource == 'WSDATA';
	},
	hostIsParam$ : function(){
		return this.hostSource == 'PARAM';
	},
	hostIsProperty$ : function(){
		return this.hostSource == 'PROPERTY';
	},
	portIsText$: function() {
		return this.portSource == 'CONSTANT' || this.portSource == 'FLOW' || this.portSource == 'WSDATA';
	},
	portIsParam$ : function(){
		return this.portSource == 'PARAM';
	},
	portIsProperty$ : function(){
		return this.portSource == 'PROPERTY';
	},

	init: function() {
		this.set('paramStore', this.rootVM.getParamStore());
		this.protocolStore.add([{
			name: this.localize('http'),
			value: 'http'
		}, {
			name: this.localize('https'),
			value: 'https'
		}]);		
		this.propertyStore.load();
	},	

	//Call from Mixin, will be different depend on different connection type
	getConnectionParams : function(){
		var params = {};
		var paramArray = ['host','port'];
		for(var i = 0; i < paramArray.length; i++){
			var paramName = paramArray[i];
			if(this[paramName]){
				//Compute actual value for each field based on source type.
				params[paramName] = this[paramName + 'Source'] == "CONSTANT" ? this[paramName] : "$" + this[paramName + 'Source'] + "{" + this[paramName] + "}";
			}
		}		
		params['protocol'] = this.protocol;	
		params['sessionName'] = this.sessionName;
		return params;
	}	
});