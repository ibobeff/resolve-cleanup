glu.defModel('RS.wiki.automation.ActionTaskValidation', {
	validateName: function(name) {
		return RS.common.validation.TaskName.test(name);
	},
	validateNamespace: function(namespace) {
		return RS.common.validation.TaskNamespace.test(namespace);
	},
	validateNamespaceAll: function(namespace) {
		return RS.common.validation.TaskNamespaceAll.test(namespace);
	},

	outputStore: {
		mtype: 'store',
		fields: ['name']
	},

	numPendingGetTask: 0,
	numUndos: 0,
	isRenameActionTask: false,

	checkActionTaskParamDependency: function(numUndos) {
		// reset flag
		this.outputStore.removeAll();
		this.set('numPendingGetTask', 0);
		this.set('numUndos', typeof(numUndos) == 'undefined' ? 2 : numUndos); // default of 2 to undo a new element add + split edge

		this.isRenameActionTask = (this.numUndos == 0);

		// retrieve the previous outputs parameter list
		if (this.parentVM.splitEdgeFlag) {
			this.getPreviousTasksOutputs(true);
			this.parentVM.splitEdgeFlag = false;
		} else if (this.cell && this.cell.edges && this.cell.edges.length) {
			if (this.viewmodelName == 'NewTask') {
				this.getPreviousTasksOutputsOnly();
			} else {
				this.getPreviousTasksOutputs(false);
			}
		} else if (this.isRenameActionTask) {
			this.resumeValidateTaskName();
		}

		if (this.parentVM.addVertexFlag) {
			this.parentVM.addVertexFlag = false;
			if (this.cell) {
				if (Ext.isFunction(this.getWizardTaskFromList) && this.getWizardTaskFromList(this.cell.id)) {
					this.removeWizardTaskFromList(this.cell.id);
				}
			}
		}
	},
	
	getPreviousTasksOutputs: function(isInsert) {
		var cell = this.cell;
		this.outputData = [];
		var getTaskFlag = false;
		Ext.each(cell.edges, function(edge) {
			if (edge.target != cell && !this.isRenameActionTask)
				return;
			if (!isInsert && edge.source != cell)
				return;
			var source = edge.source;
			var sourceValue = source ? source.getValue() : '';
			if (!(sourceValue && (sourceValue.tagName.toLowerCase() == 'task' || sourceValue.tagName.toLowerCase() == 'start')))
				return;

			var params = this.parse(sourceValue);
			if (params.taskName) {
				var version = sourceValue.getAttribute('version');
				getTaskFlag = true;
				this.set('numPendingGetTask', ++this.numPendingGetTask);
				this.getTaskOnly(params.taskName, version, function(actiontask) {
					var paramList = actiontask.resolveActionInvoc.resolveActionParameters;				
					for(var i = 0; i < paramList.length; i++){
						if(paramList[i].utype == 'OUTPUT'){
							this.outputData.push({
								name: paramList[i].uname
							});
						}
					}			
					this.outputStore.loadData(this.outputData);	
				}, function() {}, function() {
					this.set('numPendingGetTask', --this.numPendingGetTask);
				})
			}
		}, this);		
		if (!getTaskFlag && Ext.isFunction(this.resumeValidateTaskName)) {
			// if we are not required to call API get any ActionTask, just resume validation of the TaskName
			this.resumeValidateTaskName();
		}
	},
	outputData : [],
	getPreviousTasksOutputsOnly: function() {
		var cell = this.cell;
		this.outputData = [];
		Ext.each(cell.edges, function(edge) {
			if (edge.target != cell)
				return;
			var source = edge.source;
			var sourceValue = source ? source.getValue() : '';
			if (!(sourceValue && (sourceValue.tagName.toLowerCase() == 'task' || sourceValue.tagName.toLowerCase() == 'start')))
				return;

			var params = this.parse(sourceValue);
			if (params.taskName) {
				var version = sourceValue.getAttribute('version');
				this.getTaskOnly(params.taskName, version, function(actiontask) {
					var paramList = actiontask.resolveActionInvoc.resolveActionParameters;				
					for(var i = 0; i < paramList.length; i++){
						if(paramList[i].utype == 'OUTPUT'){
							this.outputData.push({
								name: paramList[i].uname
							});
						}
					}
					this.outputStore.loadData(this.outputData);
				})
			}
		}, this);	
	},

	when_numPendingGetTask_change: {
		on: ['numPendingGetTaskChanged'],
		action: function() {
			if (this.numPendingGetTask == 0) {
				 if (this.outputStore.getCount()) {
					 this.checkParamDependency();
				 } else if (this.isRenameActionTask) {
					 this.resumeValidateTaskName();
				 }
			}
		}
	},

	checkParamDependency: function() {
		var cell = this.cell;
		var targetInputParamList = [];
		var hasDependency = false;

		Ext.each(cell.edges, function(edge) {
			if (edge.source != cell)
				return;
			var target = edge.target;
			var targetValue = target ? target.getValue() : '';
			if (!(targetValue && (targetValue.tagName.toLowerCase() == 'task' || targetValue.tagName.toLowerCase() == 'start')))
				return;
			var targetParams = this.parse(targetValue);
			targetInputParamList = targetInputParamList.concat(this.getTargetInputParams(targetParams));
		}, this);
		
		// if any target has input params, check if they have dependencies on this element's source output parameters
		if (targetInputParamList.length) {
			for (var i=0, l=this.outputStore.getCount(); i<l; i++) {
				if (targetInputParamList.indexOf(this.outputStore.getAt(i).get('name')) != -1) {
					hasDependency = true;
					break;
				}
			}
		}
		
		if (hasDependency) {
			var msg = '';
			if (this.isRenameActionTask) {
				msg = this.localize('automationModelDependencyModifyTask', this.localize('renaming'));
			} else {
				msg = this.localize('automationModelDependencyInsert');
			}
			this.message({
				title: this.localize('warning'),
				msg: msg,
				buttons: Ext.MessageBox.YESNO,
				buttonText: {
					yes: this.localize('confirm'),
					no: this.localize('cancel')
				},
				scope: this,
				fn: function(btn) {
					if (btn == 'no') {
						this.parentVM.fireEvent('paramDependencyUndo', this.numUndos, this.isRenameActionTask);
						if (this.isRenameActionTask) {
							this.cancelValidateTaskName();
						}
					} else if (this.isRenameActionTask) {
						this.resumeValidateTaskName();
					}
				},
			});
		} else if (this.isRenameActionTask) {
			this.resumeValidateTaskName();
		}
	},

	getTargetInputParams: function(targetParams) {
		// parse for target's input parameter name from encoded parameter, eg "$OUTPUT{sys_id}" will return "sys_id"
		var targetInputParamList = [];
		for (key in targetParams.modelInputs) {
			var modelInput = targetParams.modelInputs[key];
			if (modelInput.indexOf('$OUTPUT') != -1) {
				var paramName = modelInput.substring(8, modelInput.lastIndexOf('}'));
				targetInputParamList.push(paramName);
			}
		}
		return targetInputParamList;
	},

	// retrieve task, don't update task properties panel, and if not in actiontasklist, add to it
	getTaskOnly: function(taskName, version, onSuccess, onFailure, onComplete) {
		if (!taskName || this.parentVM.parentVM.viewOnly) {
			if (typeof onComplete === 'function') {
				onComplete.call(this);
			}
			return;
		}
		var actiontask = this.parentVM.parentVM.getActionTaskContent(taskName, version);
		if (actiontask) {
			if (typeof onSuccess === 'function') {
				onSuccess.call(this, actiontask.data);
			}
			if (typeof onComplete === 'function') {
				onComplete.call(this);
			}
		} else {
			var params = {
				id: '',
				name: taskName
			};
			/* TODO - support Version Control
			if (version) {
				Ext.apply(params, {
					version: version
				})
			}
			*/
			this.ajax({
				url: '/resolve/service/actiontask/get',
				params: params,
				scope: this,
				success: function(resp) {
					var respData = RS.common.parsePayload(resp);

					if (respData.success) {
						this.parentVM.parentVM.setActionTaskContent(taskName, version, respData);
	
						if (typeof onSuccess === 'function') {
							onSuccess.call(this, respData.data);
						}
					} else {
						clientVM.displayError(this.localize('getTaskError', respData.message), null, null, false);

						if (typeof onFailure === 'function') {
							onFailure.call(this, respData);
						}
					}
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
					if (typeof onFailure === 'function') {
						onFailure.call(this);
					}
				},
				callback: function() {
					if (typeof onComplete === 'function') {
						onComplete.call(this);
					}
				}
			});
		}
	}
});