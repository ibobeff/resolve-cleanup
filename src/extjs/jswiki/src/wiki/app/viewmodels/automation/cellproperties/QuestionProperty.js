glu.defModel('RS.wiki.automation.QuestionProperty', {
	mixins: ['LabelAware'],
	dropdown: true,
	radio: false,
	cell: null,
	loading: false,
	showVarFormForQuestion : false,
	showVarFormForRecommendation : false,
	recommendationText : '',
	varFormForQuestion : {
		mtype : 'VariableSubstitution',
		valueSyntax : '{0}.{1}',
		typeList : ['WSDATA'],
	},
	varFormForRecommendation : {
		mtype : 'VariableSubstitution',
		valueSyntax : '{0}.{1}',
		typeList : ['WSDATA'],
	},
	API : {
		getSystemProperty : '/resolve/service/sysproperties/getSystemPropertyByName'
	},
	init : function(){
		this.getDefaultQuestionType();
		this.varFormForQuestion.dumper = {
			dump : function(newVariableString){		
				var value = ( this.label ? Ext.String.trim(this.label) : '') + ' ' + newVariableString;
				this.set('label', Ext.String.trim(value));
			},
			scope : this
		}
		this.varFormForQuestion.cancel = function(){
			this.set('showVarFormForQuestion', false);	
		}.bind(this);	
		this.varFormForRecommendation.dumper = {
			dump : function(newVariableString){		
				var value = ( this.recommendationText ? Ext.String.trim(this.recommendationText) : '') + ' ' + newVariableString;
				this.set('recommendationText', Ext.String.trim(value));
			},
			scope : this
		}
		this.varFormForRecommendation.cancel = function(){
			this.set('showVarFormForRecommendation', false);	
		}.bind(this);
		this.addRootVmEventListerners();
	},
	beforeDestroyComponent: function() {
		this.removeRootVmEventListerners();
	},
	addRootVmEventListerners: function() {
		this.rootVM.on('decisionTreeViewTabIsPressed', this.decisionTreeViewTabIsPressedHandler, this);
	},
	removeRootVmEventListerners: function() {
		this.rootVM.removeListener('decisionTreeViewTabIsPressed', this.decisionTreeViewTabIsPressedHandler, this);
	},

	decisionTreeViewTabIsPressedFlag: false,
	decisionTreeViewTabIsPressedHandler: function(flag) {
		this.set('decisionTreeViewTabIsPressedFlag', flag);
	},

	addVarForQuestion : function(){
		this.set('showVarFormForQuestion', true);
	},
	addVarForQuestionIsVisible$ : function(){
		return !this.showVarFormForQuestion && !this.decisionTreeViewTabIsPressedFlag;
	},
	addVarForRecommendation : function(){
		this.set('showVarFormForRecommendation', true);
	},
	addVarForRecommendationIsVisible$ : function(){
		return !this.showVarFormForRecommendation && !this.decisionTreeViewTabIsPressedFlag;
	},
	getDefaultQuestionType : function(){
		this.ajax({
			url: this.API['getSystemProperty'],
			params: {
				id: '',
				name: "dt.option.default"
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (respData.success) {
					this.set("defaultType", respData.data.uvalue || "radio");
				} else {
					clientVM.displayError(respData.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	when_radio_changed: {
		on: ['radioChanged'],
		action: function() {
			if (!this.cell || this.loading)
				return;
			if (this.radio)
				this.parentVM.fireEvent('dtselecttypechanged', this.cell, 'radio')
		}
	},
	when_dropdown_changed: {
		on: ['dropdownChanged'],
		action: function() {
			if (!this.cell || this.loading)
				return;
			if (this.dropdown)
				this.parentVM.fireEvent('dtselecttypechanged', this.cell, 'dropdown')
		}
	},
	when_recommendation_changed : {
		on : ['recommendationTextChanged'],
		action : function(){
			if (!this.cell || this.loading)
				return;
			this.parentVM.fireEvent('dtrecommendationchanged', this.cell, this.recommendationText);
		}
	},
	resetProperty: function() {
		this.set('loading', true);
		this.set('label', '');
		this.set('cell', null);
		this.set('dropdown', true);
		this.set('radio', false);
		this.set('loading', false);
	},
	defaultType : "radio",
	populate: function(cell) {
		this.resetProperty();
		this.set('loading', true);
		this.set('cell', cell);
		this.set('label', cell.getAttribute('label'));
		var selectType = cell.getAttribute('select');
		if(!selectType){
			selectType = this.defaultType;
			this.parentVM.fireEvent('dtselecttypechanged', this.cell, selectType)
		}
		if (selectType.toLowerCase() == 'radio') {
			this.set('dropdown', false);
			this.set('radio', true);
		} else {
			this.set('dropdown', true)
			this.set('radio', false);
		}
		this.set('recommendationText', cell.getAttribute('recommendation') || '');	
		this.set('loading', false);
	}	
});