glu.defModel('RS.wiki.automation.AbortModel', {
	mixins: ['Model'],
	resolveModelType: 'abort',
	abortModelElementPicker: {
		mtype: 'AbortModelElementPicker'
	},
	abortModelImagePicker: {
		mtype: 'AbortModelImagePicker'
	},
	connectionPicker : {
		mtype : 'ConnectionPicker'
	},
	init: function() {
		if (this.rootVM.ns === 'RS.wiki') {
			// "NoProperty" must be last item in list
			var list = ['ContainerProperty', 'ConnectorProperty', 'TextProperty', 'StartProperty', 'NewTask', 'EndProperty', 'EventProperty', 'PreconditionProperty', 'ConnectorProperty', 'NewRunbook', 'RunbookProperty', 'TaskProperty', 'NoProperty'];
			Ext.each(list, function(name) {
				this.cellProperties.add(this.model({
					mtype: name
				}));
			}, this);
			this.initEventWatchers();
		}
	},
	showToolTip: function(cell, callback) {
		var value = cell.getValue();
		var tagName = value.tagName.toLowerCase();
		if (tagName == 'task') {
			this.getTaskTip(cell, callback);
			return;
		}
		if (tagName == 'subprocess') {
			this.getRunbookTip(cell, callback);
			return;
		}
		callback();
	}
})