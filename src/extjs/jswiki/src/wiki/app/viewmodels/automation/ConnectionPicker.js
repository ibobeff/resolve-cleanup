glu.defModel('RS.wiki.automation.ConnectionPicker', {
	mixins: ['ElementPicker'],
	initElements: function() {
		var list = [{
			elementType: 'sshConnection',
			elementImg: '/resolve/jsp/model/images/symbols/rule.png',
			elementWidth: 250,
			elementHeight: 525,
			connectionType : 'SSH',
		}, {
			elementType: 'telnetConnection',
			elementImg: '/resolve/jsp/model/images/symbols/rule.png',
			elementWidth: 250,
			elementHeight: 525,
			connectionType : 'Telnet'
		}, {
			elementType: 'httpConnection',
			elementImg: '/resolve/jsp/model/images/symbols/rule.png',
			elementWidth: 250,
			elementHeight: 525,
			connectionType : 'HTTP'
		}];

		for(var i = 0; i < list.length; i++) {		
			this.elements.add(glu.model('RS.wiki.automation.ConnectionElement', list[i]));
		}
	}
});