glu.defModel('RS.wiki.automation.NewTaskWizardProperty',{
	mixins : ['ActionTaskValidation'],
	fields : ['taskNamespace','taskName'],	
	connectionType : null,
	httpTaskConfig : {
		mtype : 'HTTPTaskConfig'
	},
	networkTaskConfig : {
		mtype : 'NetworkTaskConfig'
	},
	defaultData : {
		taskName : '',
		taskNamespace : ''
	},
	resetProperties : function(){
		this.loadData(this.defaultData);
		this.httpTaskConfig.resetProperties();
		this.networkTaskConfig.resetProperties();
		this.set('isPristine', true);
	},
	saveVariableList : function(variableType, data){		
		this.httpTaskConfig.saveVariableList(variableType,data);
		this.networkTaskConfig.saveVariableList(variableType,data);
	},
	isTaskValid : function(){
		this.set('isPristine',false);
		return this.taskNamespaceIsValid == true && this.taskNameIsValid == true;
	},
	taskIsHTTP$ : function(){
		return this.connectionType == 'http';
	},
	getTaskParams : function(){
		if(this.connectionType == 'http')
			return this.httpTaskConfig.getTaskParams();
		else 
			return this.networkTaskConfig.getTaskParams();
	},
	getTaskInfo : function(){
		return {
			name : this.taskName,
			namespace : this.taskNamespace,
			params : this.getTaskParams()
		}
	},
	updatePrompt : function(newPrompt){
		this.networkTaskConfig.updatePrompt(newPrompt);
	},	
	taskNamespaceIsValid$ : function(){
		return (this.taskNamespace != "" && this.validateNamespace(this.taskNamespace)) ? true : this.localize('invalidNamespace');
	},
	taskNameIsValid$ : function(){
		return (this.taskName != "" && this.validateName(this.taskName)) ? true : this.localize('invalidName');
	},

})