glu.defModel('RS.wiki.automation.NewTask', {
	mixins: ['MergeAware', 'TaskProperty', 'XmlAware', 'LabelAware', 'VersionAware', 'ActionTaskValidation'],
	loading: false,
	cell: null,
	selectedTaskName: '',
	selectedTaskNamespace: '',
	isCreateTask: false,
	isSelectTask: false,

	selectTemplateType: '',
	templateStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	NONE: 'None',
	optionsStore: {
		mtype : 'store',
		fields: [{
			name: 'name',
			convert: function(v, r) {
				return r.raw;
			}
		}],
		data: [
			'None',
			'QUEUE_NAME',
			'EXECPATH',
			'CMDLINE_EXCLUDE',
			'RESULTTYPE',
			'RESULTFILE',
			'RESULTFILE_REMOVE',
			'RESULTFILE_WAIT',
			'CLASSPATH',
			'JAVAOPTIONS',
			'TIMEOUT_DESTROY_THREAD',
			'TMPFILE_PREFIX',
			'TMPFILE_POSTFIX',
			'TMPFILE_REMOVE',
			'INPUTFILE_POSTFIX',
			'BLANKVALUES',
			'TAILLENGTH',
			'EXTERNAL_TYPE',
			'METRIC_GROUP',
			'METRIC_ID',
			'METRIC_UNIT'
		]
	},
	optionName: '',
	optionValue: '',

	paramSource: {
		mtype: 'store',
		fields: [{
			name: 'source',
			convert: function(v, r) {
				return r.raw;
			}
		}],
		data: ['DEFAULT', 'OUTPUT', 'FLOW', 'WSDATA', 'PARAM', 'PROPERTY']
	},
	addVarParamSource: {
		mtype: 'store',
		fields: [{
			name: 'source',
			convert: function(v, r) {
				return r.raw;
			}
		}],
		data: ['INPUT','OUTPUT', 'FLOW', 'WSDATA', 'PARAM', 'PROPERTY']
	},
	paramsStore: {
		mtype: 'store',
		fields: [{
			name: 'name',
			convert: function(v, r) {
				return r.raw;
			}
		}],
	},
	propertyStore: {
		mtype: 'store',
		fields: [{
			name: 'name',
			convert: function(v, r) {
				return r.raw;
			}
		}]
	},

	addVarSourceType: '',
	addVarSourceValue: '',
	comboValueMaxChar: 5000,
	taskTimeout: 300,
	expectedTimeout: 10,
	commandPrompt: '$FLOW{PROMPT}',

	HTTPNewTaskConfig: {
		mtype: 'RS.wiki.automation.HTTPTaskConfig',
		embedded: true
	},

	wait: false,
	type: 'assess',
	//connectionType : 'ssh',
	networkConnectionType : '',
	taskName: '',
	defaultData: {
		name: '',
		namespace: '',
		type: 'assess',
		taskName: '',
		selectTemplateType: '',
		sessionName : '',
		command: '',
		networkConnectionType: '',
		description: '',
		taskTimeout: 300,
		expectedTimeout: 10,
		commandPrompt: '$FLOW{PROMPT}',
		optionName: '',
		optionValue: ''
	},
	fields: ['name', 'namespace', 'type', 'taskName', 'selectTemplateType', 'command', 'sessionName', 'networkConnectionType', 'description', 'taskTimeout', 'commandPrompt', 'expectedTimeout', 'optionName', 'optionValue'],
	typeStore: null,
	//connectionTypeStore : null,
	networkConnectionTypeStore : null,
	API : {
		save : '/resolve/service/actiontask/save',
		getContentAutoGenCode : '/resolve/service/actiontask/getContentAutoGenCode'
	},
	commandIsValid: true,
	when_command_changed: {
		on: 'commandChanged',
		action: function() {
			if (this.command != '') {
				this.set('commandIsValid', true);
			} else {
				this.set('commandIsValid', this.localize('commandIsRequired'));
			}
		}
	},
	when_taskName_change: {
		on: ['taskNameChanged'],
		action: function() {
			if (this.taskName) {
				this.validateTaskName(this.taskName, true, true);
			}
		}
	},
	taskStoreOnLoad: function() {
		this.validateTaskName(this.taskName, true, true);
		this.set('reqCount', this.reqCount - 1);
	},
	optionValueDisabled$: function() {
		return this.optionName == this.NONE || this.optionName == '';
	},
	init: function() {
		this.resetProperty();
		var config = {
			mtype: 'store',
			fields: ['name', 'value'],
			data: this.createNameValueItems('remote', 'assess', 'os', 'bash', 'cmd', 'cscript', 'powershell')
		};

		this.templateStore.loadData([{
			name: this.localize('genericActionTask'),
			value: 'generic'
		}, {
			name: this.localize('restAPIActionTask'),
			value: 'restapi'
		}, {
			name: this.localize('executeCommandOverNetwork'),
			value: 'networkcommand'
		}, {
			name: this.localize('executeCommandLocally'),
			value: 'localcommand'
		}])

		this.set('typeStore', Ext.create('Ext.data.Store', config));

		/*
		var connectionConfig = {
			mtype: 'store',
			fields: ['name', 'value'],
			data: this.createNameValueItems('none','ssh','telnet','http')
		};
		this.set('connectionTypeStore', Ext.create('Ext.data.Store', connectionConfig));
		*/

		var networkConnectionTypeConfig = {
			mtype: 'store',
			fields: ['name', 'value'],
			data: this.createNameValueItems('ssh','telnet')
		};
		this.set('networkConnectionTypeStore', Ext.create('Ext.data.Store', networkConnectionTypeConfig));		

		this.taskStore.un('load', this.taskStoreOnLoad);
		this.taskStore.on('load', this.taskStoreOnLoad, this);
		this.initVersionHistoryStore();

		var propertyStore = clientVM.getActionTaskPropertyStore();
		var propertyData = propertyStore.collect('uname');
		this.propertyStore.loadData(propertyData);
		this.HTTPNewTaskConfig.saveVariableList('PROPERTY', propertyData);	

		//Get Param variables
		var paramStore = this.rootVM.getParamStore();		
		paramStore.on('datachanged', function(){
			var paramData = paramStore.collect('name');
			this.paramsStore.loadData(paramData);
			this.HTTPNewTaskConfig.saveVariableList('PARAM', paramData);
		}.bind(this));	

		this.outputStore.on('datachanged', function(store, records){		
			this.HTTPNewTaskConfig.saveVariableList('OUTPUT', store.collect('name'));		
		}.bind(this))
	},
	/*
	connectionTypeIsVisible$ : function(){
		return this.type == "remote";
	},
	*/
	taskWizardList: {
		mtype: 'list'
	},
	resetWizardTaskList: function() {
		this.taskWizardList.removeAll();
	},
	removeWizardTaskFromList: function(id) {
		for (var i=0; i < this.taskWizardList.length; i++) {
			var task = this.taskWizardList.getAt(i);
			if (task.id === id) {
				this.taskWizardList.remove(task);
				break;
			}
		}
	},
	getWizardTaskFromList: function(id) {
		for (var i=0; i < this.taskWizardList.length; i++) {
			var task = this.taskWizardList.getAt(i);
			if (task.id === id) {
				return task;
			}
		}
	},
	saveTaskWizardProperties: function() {
		this.removeWizardTaskFromList(this.cell.id);
		this.taskWizardList.add({
			id : this.cell.id,
			name :this.name,
			namespace : this.namespace,
			type : this.type,
			taskName : this.taskName,
			selectTemplateType : this.selectTemplateType,
			sessionName : this.sessionName,
			command : this.command,
			networkConnectionType : this.networkConnectionType,
			description : this.description,
			taskTimeout : this.taskTimeout,
			expectedTimeout : this.expectedTimeout,
			commandPrompt : this.commandPrompt,
			optionName : this.optionName,
			optionValue : this.optionValue,
			isCreateTask : this.isCreateTask,
			isSelectTask : this.isSelectTask,
			isAddingVariable : this.isAddingVariable,
			addVariableDisabled : this.addVariableDisabled,
			httpSavedConfig : this.HTTPNewTaskConfig.getCurrentConfig()
		});
	},
	populate: function(cell) {
		this.resetProperty();
		this.populateVersion(cell, true);
		this.set('loading', true);
		this.set('cell', cell);
		this.enableAllButton(true);
		this.checkActionTaskParamDependency();

		var task = this.getWizardTaskFromList(cell.id);
		if (task) {
			this.set('name', task.name);
			this.set('namespace', task.namespace);
			this.set('type', task.type);
			this.set('taskName', task.taskName);
			this.set('selectTemplateType', task.selectTemplateType);
			this.set('sessionName', task.sessionName);
			this.set('command', task.command);
			this.set('networkConnectionType', task.networkConnectionType);
			this.set('description', task.description);
			this.set('taskTimeout', task.taskTimeout);
			this.set('expectedTimeout', task.expectedTimeout);
			this.set('commandPrompt', task.commandPrompt);
			this.set('optionName', task.optionName);
			this.set('optionValue', task.optionValue);
			this.set('isCreateTask', task.isCreateTask);
			this.set('isSelectTask', task.isSelectTask);
			this.set('isAddingVariable', task.isAddingVariable);
			this.set('addVariableDisabled', task.addVariableDisabled);
			this.HTTPNewTaskConfig.loadConfig(task.httpSavedConfig);
		}
		this.set('loading', false);
	},

	resetProperty: function() {
		this.resetVersion();
		this.set('loading', true);
		this.loadData(this.defaultData);
		this.HTTPNewTaskConfig.resetProperties();
		this.set('isCreateTask', false);
		this.set('isSelectTask', false);
		this.set('isAddingVariable', false);
		this.set('addVariableDisabled', false);
		this.set('isPristine', true);
		this.enableAllButton(true);
		this.set('taskName', '');
		this.set('taskNameIsValid', true);
		this.set('optionName', '');
		this.set('selectedTaskName', '');
		this.set('selectedTaskNamespace', '');
		this.set('loading', false);
	},

	isNamespaceAndNameValid$: function() {
		return (this.namespaceIsValid == true) && (this.nameIsValid == true);
	},

	isCommandValid$: function() {
		return this.command != '';
	},

	isCreateValid$: function() {
		if (!this.isNamespaceAndNameValid) {
			return false;
		}
		else if (this.isTemplateSelected) {
			if (this.isAPIActionTask || this.isCustomActionTask) {
				return true;
			}
			else if (this.isCommandValid) {
				return true;
			}
		}

		return false;
	},
	isDoneValid$: function() {
		return this.isSelectedTaskValid == true && this.taskNameIsValid == true; // must compare to true since false will be an error string
	},

	/* TODO - support Version Control
	showUseVersionOption$: function() {
		if (!this.isDoneValid) {
			this.resetVersion();
		}
		return this.isDoneValid;
	},
	showVersionNumberOption$: function() {
		return this.specificVersion && this.isDoneValid;
	},
	*/ 

	isSelectedTaskValid$: function() {
		if (this.selectedTaskNamespace && this.selectedTaskName) {
			if (this.validateNamespaceAll(this.selectedTaskNamespace) && this.validateName(this.selectedTaskName)) {
				return true;
			}
			return this.localize('invalidTaskName');
		}
		return false;
	},

	isCustomActionTaskValid$: function() {
		return this.isCustomActionTask && this.isNamespaceAndNameValid;
	},
	isHTTPConnectionActionTaskValid$: function() {
		return this.isAPIActionTask && this.isNamespaceAndNameValid;
	},
	isNetworkCommandActionTaskValid$: function() {
		return this.isNetworkCommandActionTask && this.isNamespaceAndNameValid;
	},
	isLocalCommandActionTaskValid$: function() {
		return this.isLocalCommandActionTask && this.isNamespaceAndNameValid;
	},

	isTemplateSelected$: function() {
		return this.selectTemplateType != '';
	},
	networkConnectionTypeValid$ : function(){
		return this.networkConnectionType != '' && this.isNetworkCommandActionTask;
	},
	isCommandActionTaskValid$ : function(){
		return this.isLocalCommandActionTask || this.networkConnectionTypeValid;
	},

	isCustomActionTask$: function() {
		return this.selectTemplateType == 'generic';
	},
	isNetworkCommandActionTask$: function() {
		return this.selectTemplateType == 'networkcommand';
	},
	isLocalCommandActionTask$: function() {
		return this.selectTemplateType == 'localcommand';
	},
	isAPIActionTask$: function() {
		return this.selectTemplateType == 'restapi';
	},

	isAddingVariable: false,
	addVariable: function() {
		this.set('isAddingVariable', true);
		this.set('addVariableDisabled', true);
		this.set('addVarSourceType', 'PARAM');
		this.set('addVarSourceValue', '');
	},
	addVariableIsPressed$: function() {
		return this.isAddingVariable;
	},
	addVariableDisabled: false,
	isAddVariableDisabled$: function() {
		return this.addVariableDisabled;
	},

	selectActionTask: function() {
		this.set('isCreateTask', false);
		this.set('isSelectTask', true);
		if (!this.taskName) {
			this.set('taskName', '');
		}
	},

	selectTaskHidden$: function() {
		return !this.isSelectTask || this.isCreateTask;
	},

	createActionTask: function() {
		this.set('isCreateTask', true);
		this.set('isSelectTask', false);
		this.set('isPristine', true);
		this.enableAllButton(true);
	},

    nameIsValid$: function() {
        return this.name && this.validateName(this.name) ? true : this.localize('invalidName');
    },

	namespaceIsValid$: function() {
        return this.namespace && this.validateNamespace(this.namespace) ? true : this.localize('invalidNamespace');
    },

	createTaskHidden$: function() {
		return !this.isCreateTask || this.isSelectTask;
	},
	inputParamRegex : /\$INPUT{([a-zA-Z_$][a-zA-Z_$0-9]*)}/g,
	create: function() {
		if (this.wait)
			return

		if(!this.isCreateValid){
			this.set('isPristine', false);
			this.enableAllButton(false);
			return;
		}

		switch (this.selectTemplateType) {
		case 'restapi':
			var params = this.HTTPNewTaskConfig.getTaskParams();
			this.populateDataThenSave('http',params);
			break;
		case 'networkcommand':
			var params = {
				command : this.command,
				EXPECT_TIMEOUT : this.expectedTimeout,
				prompt : this.commandPrompt
			}
			//Parse command to check if any input is used within the command, the auto generate those inputs.
			var m = null;
			while ((m = this.inputParamRegex.exec(this.command)) !== null) {
			    // This is necessary to avoid infinite loops with zero-width matches
			    if (m.index === this.inputParamRegex.lastIndex) {
			        this.inputParamRegex.lastIndex++;
			    }

			    // The result can be accessed through the `m`-variable.
			    m.forEach(function(match, groupIndex){
			       if(groupIndex == 1)
			    		params[match] = ''; //Default to empty (might  allow user to define default in the future)
			    });
			}
			this.populateDataThenSave(this.networkConnectionType, params);
			break;
		case 'localcommand':
			var params = {
				command : this.command
			}
			//Parse command to check if any input is used within the command, the auto generate those inputs.
			var m = null;
			while ((m = this.inputParamRegex.exec(this.command)) !== null) {
			    // This is necessary to avoid infinite loops with zero-width matches
			    if (m.index === this.inputParamRegex.lastIndex) {
			        this.inputParamRegex.lastIndex++;
			    }

			    // The result can be accessed through the `m`-variable.
			    m.forEach(function(match, groupIndex){
			       if(groupIndex == 1)
			    		params[match] = ''; //Default to empty (might  allow user to define default in the future)
			    });
			}
			this.populateDataThenSave('local', params);
			break;
		case 'generic':
		default:
			this.doCreate();
			break;
		}
	},
	on_form_is_valid : {
		on : ['isValidChanged'],
		action : function(){
			this.enableAllButton(this.isValid);
		}
	},
	enableAllButton : function(flag){
		this.set('createIsEnabled', flag);
	},
	createIsEnabled: true,
	doCreate : function(extraData){
		var actiontaskbuilder = this.model({
			mtype: 'RS.actiontask.ActionTask',
			embed: true
		});
		actiontaskbuilder.initActionTaskBuilder();	// need to pre-init to get actiontaskbuilder default empty data

		var actionTask = actiontaskbuilder.emptyActionTaskData.data;
		actionTask.uname = this.name;
		actionTask.unamespace = this.namespace;
		actionTask.udescription = this.description;
		actionTask.umenuPath = '/' + this.namespace.replace(/\./g, '/');

		//Apply auto generated code (for command or restful template) and parameters
		if(extraData){
			actionTask = RS.common.deepApply(actionTask, extraData);
		}

		switch (this.selectTemplateType) {
		case 'restapi':
			actionTask.resolveActionInvoc.utype = 'remote';
			actionTask.resolveActionInvoc.utimeout = 300;
			actionTask.wizardType = 'http';
			break;
		case 'networkcommand':
			actionTask.resolveActionInvoc.utype = 'remote';
			actionTask.resolveActionInvoc.utimeout = this.taskTimeout;
			actionTask.wizardType = this.networkConnectionType;

			//Apply Option if there is any
			if (this.optionName && this.optionName != this.NONE) {
				actionTask.resolveActionInvoc.resolveActionInvocOptions.push({
					id : '',
					udescription : '',
					uname : this.optionName,
					uvalue : this.optionValue
				})
			}
			break;
		case 'localcommand':
			actionTask.resolveActionInvoc.utype = 'os';
			actionTask.resolveActionInvoc.ucommand = '$INPUT{command}';
			actionTask.resolveActionInvoc.utimeout = this.taskTimeout;
			actionTask.wizardType = 'local';

			//Apply Option if there is any
			if (this.optionName && this.optionName != this.NONE) {
				actionTask.resolveActionInvoc.resolveActionInvocOptions.push({
					id : '',
					udescription : '',
					uname : this.optionName,
					uvalue : this.optionValue
				})
			}
			break;
		case 'generic':
		default:
			actionTask.resolveActionInvoc.utype = this.type;
			actionTask.resolveActionInvoc.utimeout = this.taskTimeout;
			break;
		}

		this.set('wait', true);
		this.ajax({
			url: this.API['save'],
			jsonData: actionTask,
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);

				if (respData.success) {
					clientVM.displaySuccess(this.localize('SaveSuc'));

					actiontaskbuilder.successfulActionTaskLoad(respData);

					this.cell.setAttribute('label', this.name);

					//Append sessionName as runtime parameter if this task is command over network
					if(this.sessionName && (this.isAPIActionTask || this.isNetworkCommandActionTask)){
						var xmlDoc = mxUtils.createXmlDocument();
						var paramNode = xmlDoc.createElement('params');
						var inputNode = xmlDoc.createElement('inputs');
						var outputNode = xmlDoc.createElement('outputs');
						var cdataNode = xmlDoc.createCDATASection(Ext.encode({
							sessionName : this.sessionName
						}));
						inputNode.appendChild(cdataNode);
						paramNode.appendChild(inputNode);

						//Empty output param cuz we need it !!!!
						cdataNode = xmlDoc.createCDATASection(Ext.encode({}));
						outputNode.appendChild(cdataNode);
						paramNode.appendChild(outputNode);

						this.cell.getValue().appendChild(paramNode);
					}
					this.parentVM.fireEvent('selectTask', this.name, this.namespace, null, this.cell);
					this.parentVM.fireEvent('updateSelectedTask', [this.cell]);

					this.resetProperty();
				} else {
					clientVM.displayError(this.localize('SaveErr') + '[' + respData.message + ']')
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
				this.removeWizardTaskFromList(this.cell.id);
			}
		});
	},
	populateDataThenSave : function(connectionType, params){
		var newTaskParam = [];
		var templateParams;

		switch (connectionType) {
		case 'http':
			templateParams = RS.wiki.automation.RemoteTaskTemplate['HTTP'];
			break;
		case 'local':
			templateParams = RS.wiki.automation.RemoteTaskTemplate['LOCAL'];
			break;
		default:
			RS.wiki.automation.RemoteTaskTemplate['DEFAULT']
			break;
		}

		//Do a proper field copy and apply default value for parameter here
		Ext.Array.each(templateParams, function(property){
			var newProp = {};
			Ext.apply(newProp, property);
			if(params.hasOwnProperty(property.uname)){
				if(params[property.uname] !== '')
					newProp.udefaultValue = params[property.uname];
				//Set this value to undefined so we know that parameter is already used.
				params[property.uname] = undefined;
			}
			newTaskParam.push(newProp);
		})

		//Any value that is not undefined doesnt have a template for it.
		for(var k in params){
			var paramValue = params[k];
			if(paramValue !== undefined){
				newTaskParam.push({
					uname : k,
					udefaultValue : params[k],
					urequired : true,
					utype : 'INPUT'
				})
			}
		}

		if (connectionType == 'local') {
			var extraData = {
				"resolveActionInvoc.resolveActionParameters" : newTaskParam
			}
			this.doCreate(extraData);
		}
		else {
			this.ajax({
				url: this.API['getContentAutoGenCode'],
				jsonData: connectionType,
				scope: this,
				success: function(resp) {
					var respData = RS.common.parsePayload(resp);
					if (respData.success) {
						var extraData = {
							"resolveActionInvoc.resolveActionInvocOptions" : [{
								id : '',
								udescription : '',
								uname : 'INPUTFILE',
								uvalue : respData.data
							}],
							"resolveActionInvoc.resolveActionParameters" : newTaskParam
						}
						this.doCreate(extraData);
					} else {
						clientVM.displayError(this.localize('SaveErr') + '[' + respData.message + ']')
					}
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				},
				callback: function() {
					this.set('wait', false);
				}
			});
		}
	},
	deepApply : function(destination, extraData){
		for(var path in extraData){
			var newData = extraData[path];
			var pathParts = path.split('.');
			var targetDataLocation = destination[pathParts[0]];
			for(var i = 1; i < pathParts.length; i++){
				targetDataLocation = targetDataLocation[pathParts[i]];
			}
			if(Array.isArray(targetDataLocation)){
				//Clear out default value if there is any.
				targetDataLocation.shift();
				Array.prototype.push.apply(targetDataLocation, newData);
			}
			else
				Ext.apply(targetDataLocation, newData);
		}
		return destination;
	},

	cancel: function() {
		this.resetProperty();
		this.populateVersion(this.cell, true);
	},
	searchTask: function() {
		this.set('isSelectTask', true);
		this.set('isCreateTask', false);
		var currentDocName = this.rootVM.fullName;
		var me = this;
		this.open({
			mtype: 'RS.actiontask.ActionTaskPicker',
			creatable: false,
			currentDocName : currentDocName,
			dumper: {
 				dump: function(selections) {
 					if (selections.length > 0) {
 						var selection = selections[0];
						var name = selection.get('uname');
						/*
						me.set('label', '');   // RBA-14764 DO NOT REMOVE or picking the same task as prior won't populate the Task Name
						me.set('label', name); // RBA-14764
						me.parentVM.fireEvent('selectTask', name, selection.get('unamespace'), null, me.cell);
						me.parentVM.fireEvent('updateSelectedTask', [me.cell]);
						*/
						me.set('selectedTaskName', selection.get('uname'));
						me.set('selectedTaskNamespace', selection.get('unamespace'));
						me.set('taskName', selection.get('ufullName'));
						me.taskStore.load();
 					}
 				}
			}
		})
	},
	updateTaskProperty: function(selectedTask) {
		this.set('selectedTaskName', selectedTask.uname);
		this.set('selectedTaskNamespace', selectedTask.unamespace);
		/* TODO - support Version Control
		this.versionHistoryStore.load({
			params: {
				id: selectedTask.id,
				docFullName: selectedTask.ufullName
			}
		});
		*/ 
	},
	done: function() {
		this.parentVM.fireEvent('selectTask', this.selectedTaskName, this.selectedTaskNamespace, null, this.cell);
		this.parentVM.fireEvent('updateSelectedTask', [this.cell]);
		this.cell.setAttribute('label', this.selectedTaskName); //Initial set label on new task
		/* TODO - support Version Control
		if (this.latestStable) {
			this.cell.setAttribute('version', 'latestStable');
		} else if (this.specificVersion) {
			this.cell.setAttribute('version', this.versionNumber);
		}
		*/
		this.resetProperty();
	},
	createNameValueItems: function () {
		var values, items = [];

		if (arguments.length === 1) {
			values = arguments[0];
		} else {
			values = Array.apply(null, arguments);
		}

		for (var i = 0; i < values.length; i++) {
			items.push({
				name: this.localize(values[i]),
				value: values[i]
			});
		}
		return items;
	},

	addVarTextboxValueEnabled$: function() {
		var isEnabled = false;

		switch(this.addVarSourceType) {
		case 'INPUT' :
		case 'WSDATA':
		case 'FLOW':
			isEnabled = true;
			break;
		default:
			isEnabled = false;
			break;
		}

		return isEnabled;
	},

	addVarPropertyValueEnabled$: function() {
		return this.addVarSourceType == 'PROPERTY';
	},

	addVarParamsStoreEnabled$: function() {
		return this.addVarSourceType == 'PARAM';
	},

	addVarOutputStoreEnabled$: function() {
		return this.addVarSourceType == 'OUTPUT';
	},

	addVariableBtn: function() {
		this.set('isAddingVariable', false);
		this.set('addVariableDisabled', false);
		var value = Ext.String.trim(this.command) + ' ' + '$'+this.addVarSourceType+'{'+this.addVarSourceValue+'}';
		this.set('command', Ext.String.trim(value));
	},

	addVariableBtnIsEnabled$: function() {
		return this.addVarSourceValue !== '';
	},
	cancelVariableBtn: function() {
		this.set('isAddingVariable', false);
		this.set('addVariableDisabled', false);
	},
});