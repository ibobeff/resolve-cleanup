glu.defModel('RS.wiki.NamespaceAdmin', {
	displayName: '',

	columns: null,

	stateId: 'wikiNsAdmin',

	wait: false,

	store: {
		mtype: 'store',

		fields: ['id', 'unamespace', 'count'],
		remoteSort: true,
		sorters: [{
			property: 'unamespace',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/nsadmin/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	recordsSelections: [],

	activate: function() {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.loadRecords();
	},

	configRolesTip: '',
	configDefaultRolesTip: '',

	init: function() {
		if (this.mock) this.backend = RS.wiki.createMockBackend(true)
		this.set('displayName', this.localize('displayName'));

		this.set('columns', [{
			header: '~~unamespace~~',
			dataIndex: 'unamespace',
			filterable: true,
			flex: 1
		}, {
			header: '~~count~~',
			dataIndex: 'count',
			filterable: false,
			sortable: false
		}]);
		this.set('configRolesTip', this.localize('configRolesTip'));
		this.set('configDefaultRolesTip', this.localize('configDefaultRolesTip'));
		this.store.on('beforeload', function() {
			this.set('wait', true);
		}, this);
		this.store.on('load', function(store, records, successful) {
			this.set('wait', false);
			if (!successful)
				clientVM.displayError(this.localize('ListRecordsErr'));
		}, this);
	},

	loadRecords: function() {
		this.store.load();
	},

	editRecord: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.WikiAdmin',
			params: {
				unamespace: id
			}
		});
	},

	sendReq: function(url, errMsg, sucMsg, params, lockScreen) {
		lockScreen.set('wait', true);
		var ids = [];
		Ext.each(this.recordsSelections, function(r, idx) {
			ids.push(r.get('id'));
		});
		var reqParams = {};
		reqParams.namespaces = ids;
		reqParams.all = this.allSelected;
		if (params)
			Ext.apply(reqParams, params);
		var win = this.open({
			mtype: 'RS.wiki.Polling'
		});
		this.ajax({
			url: url,
			params: reqParams,
			scope: this,
			success: function(resp) {
				this.handleSucResp(resp, errMsg, sucMsg);
				win.doClose();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				lockScreen.set('wait', false);
			}
		});
	},

	handleSucResp: function(resp, errMsg, sucMsg) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize(errMsg) + '[' + respData.message + ']');
		} else
			clientVM.displaySuccess(this.localize(sucMsg));

		this.loadRecords();
	},

	showMessage: function(title, OpMsg, OpsMsg, confirm, url, errMsg, sucMsg, params) {
		var strFullName = [];
		Ext.each(this.recordsSelections, function(r, idx) {
			strFullName.push(r.get('unamespace'));
		});
		var msg = this.localize(this.recordsSelections.length > 1 ? OpMsg : OpsMsg, {
			num: this.recordsSelections.length,
			names: strFullName.join(',')
		});

		this.message({
			title: this.localize(title),
			msg: msg,
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize(confirm),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				params = params || {};
				this.sendReq(url, errMsg, sucMsg, params, this);
			}
		});
	},

	doRename: function(params) {
		this.showMessage(
			'renameWikis',
			'RenamesMsg',
			'RenameMsg',
			'renameWikis',
			'/resolve/service/nsadmin/moveRenameCopy',
			'RenameErrMsg',
			'RenameSucMsg',
			params
		);
	},

	renameWikis: function() {
		this.open({
			mtype: 'RS.wiki.pagebuilder.MoveOrRename',
			action: 'rename',
			namespace: this.recordsSelections[0].get('unamespace'),
			filename: null,
			moveRenameCopyTitle: 'RenameTitle',
			modelName: this.viewmodelName,
			selectionsLength: this.recordsSelections.length,
			dumper: (function(self) {
				return function(params) {
					self.doRename(params);
				}
			})(this)
		});
	},

	renameWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	doCopy: function(params) {
		this.showMessage(
			'copyWikis',
			'CopysMsg',
			'CopyMsg',
			'copyWikis',
			'/resolve/service/nsadmin/moveRenameCopy',
			'CopyErrMsg',
			'CopySucMsg',
			params
		);
	},

	copyWikis: function() {
		this.open({
			mtype: 'RS.wiki.pagebuilder.MoveOrRename',
			action: 'copy',
			namespace: this.recordsSelections[0].get('name'),
			filename: this.recordsSelections[0].get('uname'),
			moveRenameCopyTitle: 'CopyTitle',
			modelName: this.viewmodelName,
			selectionsLength: this.recordsSelections.length,
			dumper: (function(self) {
				return function(params) {
					self.doCopy(params)
				}
			})(this)
		});
	},

	copyWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	deleteWikis: function() {
		this.showMessage(
			'deleteWikis',
			'DeletesMsg',
			'DeleteMsg',
			'deleteWikis',
			'/resolve/service/nsadmin/setDeleted',
			'DeleteErrMsg',
			'DeleteSucMsg', {
				on: true
			}
		);
	},

	deleteWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	undeleteWikis: function() {
		this.showMessage(
			'undeleteWikis',
			'UndeletesMsg',
			'UndeleteMsg',
			'undeleteWikis',
			'/resolve/service/nsadmin/setDeleted',
			'UndeleteErrMsg',
			'UndeleteSucMsg', {
				on: false
			}
		);
	},

	undeleteWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	purgeWikis: function() {
		this.showMessage(
			'purgeWikis',
			'PurgesMsg',
			'PurgeMsg',
			'purgeWikis',
			'/resolve/service/nsadmin/purge',
			'PurgeErrMsg',
			'PurgeSucMsg'
		);
	},

	purgeWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	activateWikis: function() {
		this.showMessage(
			'activateWikis',
			'ActivatesMsg',
			'ActivateMsg',
			'activateWikis',
			'/resolve/service/nsadmin/setActivated',
			'ActivateErrMsg',
			'ActivateSucMsg', {
				on: true
			}
		);
	},

	activateWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	deactivateWikis: function() {
		this.showMessage(
			'deactivateWikis',
			'DeactivatesMsg',
			'DeactivateMsg',
			'deactivateWikis',
			'/resolve/service/nsadmin/setActivated',
			'DeactivateErrMsg',
			'DeactivateSucMsg', {
				on: false
			}
		);
	},

	deactivateWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	lockWikis: function() {
		this.showMessage(
			'lockWikis',
			'LocksMsg',
			'LockMsg',
			'lockWikis',
			'/resolve/service/nsadmin/setLocked',
			'LockErrMsg',
			'LockSucMsg', {
				on: true
			}
		);
	},

	lockWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	unlockWikis: function() {
		this.showMessage(
			'unlockWikis',
			'LocksMsg',
			'LockMsg',
			'unlockWikis',
			'/resolve/service/nsadmin/setLocked',
			'LockErrMsg',
			'LockSucMsg', {
				on: false
			}
		);
	},

	unlockWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	hideWikis: function() {
		this.showMessage(
			'hideWikis',
			'HidesMsg',
			'HideMsg',
			'hideWikis',
			'/resolve/service/nsadmin/setHidden',
			'HideErrMsg',
			'HideSucMsg', {
				on: true
			}
		);
	},

	hideWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	unhideWikis: function() {
		this.showMessage(
			'unhideWikis',
			'UnhidesMsg',
			'UnhideMsg',
			'unhideWikis',
			'/resolve/service/nsadmin/setHidden',
			'UnhideErrMsg',
			'UnhideSucMsg', {
				on: false
			}
		);
	},

	unhideWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	doRate: function(params) {
		this.showMessage(
			'rateWikis',
			'RatesMsg',
			'RateMsg',
			'rateWikis',
			'/resolve/service/nsadmin/rate',
			'RateErrMsg',
			'RateSucMsg',
			params
		);
	},

	rateWikis: function() {
		this.open({
			mtype: 'RS.wiki.Rater',
			dumper: (function(self) {
				return function(params) {
					self.doRate(params);
				}
			})(this)
		});
	},

	rateWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	// doAddSearchWeight: function(params) {
	// 	this.showMessage(
	// 		'AddSearchWeightTitle',
	// 		'AddSearchWeightsMsg',
	// 		'AddSearchWeightMsg',
	// 		'confirm',
	// 		'/resolve/service/nsadmin/addSearchWeight',
	// 		'AddSearchWeightErrMsg',
	// 		'AddSearchWeightSucMsg',
	// 		params
	// 	);
	// },

	// addSearchWeight: function() {
	// 	this.open({
	// 		mtype: 'RS.wiki.SearchWeightEditor',
	// 		dumper: (function(self) {
	// 			return function(params) {
	// 				self.doAddSearchWeight(params)
	// 			}
	// 		})(this)
	// 	})
	// },

	// addSearchWeightIsEnabled$: function() {
	// 	return !this.wait && this.recordsSelections.length > 0;
	// },

	setReviewed: function() {
		this.showMessage(
			'setReviewed',
			'ReviewsMsg',
			'ReviewMsg',
			'setReviewed',
			'/resolve/service/nsadmin/setReviewed',
			'ReviewErrMsg',
			'ReviewSucMsg'
		);
	},

	setReviewedIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	doSetExpiryDate: function(params) {
		this.showMessage(
			'setExpiryDate',
			'ExpiryDatesMsg',
			'ExpiryDateMsg',
			'setExpiryDate',
			'/resolve/service/nsadmin/setExpiryDate',
			'ExpiryDateErrMsg',
			'ExpiryDateSucMsg',
			params
		);
	},

	setExpiryDate: function() {
		this.open({
			mtype: 'RS.wiki.DatePicker',
			dumper: (function(self) {
				return function(params) {
					self.doSetExpiryDate(params);
				}
			})(this)
		});
	},

	setExpiryDateIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	doConfigRoles: function(cvs) {
		this.showMessage(
			'configRoles',
			'SetRolesMsg',
			'SetRoleMsg',
			'configRoles',
			'/resolve/service/nsadmin/setAccessRights',
			'SetRoleErrMsg',
			'SetRoleSucMsg',
			cvs
		);
	},

	configRoles: function() {
		var ids = [];

		Ext.each(this.recordsSelections, function(r) {
			ids.push(r.get('id'));
		});

		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/nsadmin/accessRights',
			params: {
				defaultRights: false,
				namespaces: ids
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('GetRolesErr', respData.message));
					return;
				}

				if (!respData.data)
					return;

				this.open({
					mtype: 'RS.wiki.AccessRights',
					rights: respData.data,
					dumper: (function(self) {
						return function(cvs) {
							cvs.defaultRights = this.accessRights.defaultRoles;
							self.doConfigRoles(cvs);
						}
					})(this)
				});
			},

			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	configRolesIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	doAddTags: function(params) {
		this.showMessage(
			'AddTagTitle',
			'AddTagsMsg',
			'AddTagMsg',
			'confirm',
			'/resolve/service/nsadmin/addTags',
			'AddTagErrMsg',
			'AddTagSucMsg',
			params
		);
	},

	doRemoveTags: function(params) {
		this.showMessage(
			'RemoveTagTitle',
			'RemoveTagsMsg',
			'RemoveTagMsg',
			'confirm',
			'/resolve/service/nsadmin/removeTags',
			'RemoveTagErrMsg',
			'RemoveTagSucMsg',
			params
		);
	},

	configTags: function() {
		this.open({
			mtype: 'RS.wiki.TagConfiguration'
		});
	},

	configTagsIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	indexWikis: function() {
		this.showMessage(
			'indexWikis',
			'IndexingsMsg',
			'IndexingMsg',
			'indexWikis',
			'/resolve/service/nsadmin/index',
			'IndexingErrMsg',
			'IndexingSucMsg'
		);
	},

	indexWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	indexAllWikis: function() {
		this.showMessage(
			'indexAllWikis',
			'IndexAllWikisMsg',
			'IndexAllWikisMsg',
			'indexWikis',
			'/resolve/service/nsadmin/index',
			'IndexingErrMsg',
			'IndexingSucMsg', {
				all: true,
				namespaces: null
			}
		);
	},

	indexAllWikisIsEnabled$: function() {
		return !this.wait;
	},

	purgeIndex: function() {
		this.showMessage(
			'purgeIndex',
			'PurgeIndexsMsg',
			'PurgeIndexMsg',
			'confirm',
			'/resolve/service/nsadmin/purgeIndex',
			'PurgeIndexErrMsg',
			'PurgeIndexSucMsg'
		);
	},

	purgeIndexIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	purgeAllIndexes: function() {
		this.showMessage(
			'purgeIndex',
			'PurgeAllIndexMsg',
			'PurgeAllIndexMsg',
			'purgeIndex',
			'/resolve/service/nsadmin/purgeIndex',
			'PurgeIndexErrMsg',
			'PurgeIndexSucMsg', {
				all: true,
				namespaces: null
			}
		);
	},

	purgeAllIndexesIsEnabled$: function() {
		return !this.wait;
	},

	doCommit: function(params) {
		this.showMessage(
			'commit',
			'CommitsMsg',
			'CommitMsg',
			'confirm',
			'/resolve/service/nsadmin/commit',
			'CommitErrMsg',
			'CommitSucMsg',
			params
		);
	},

	commit: function() {
		this.open({
			mtype: 'RS.wiki.Comment',
			dumper: (function(self) {
				return function(comment) {
					self.doCommit({
						comment: comment
					});
				}
			})(this)
		})
	},

	commitIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	resetStats: function() {
		this.showMessage(
			'resetStats',
			'ResetStatssMsg',
			'ResetStatsMsg',
			'resetStats',
			'/resolve/service/nsadmin/resetStats',
			'ResetStatsErrMsg',
			'ResetStatsSucMsg'
		);
	},

	resetStatsIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	doSetDefaultRoles: function(cvs) {
		this.showMessage(
			'configDefaultRoles',
			'SetDefaultRolesMsg',
			'SetDefaultRoleMsg',
			'configDefaultRoles',
			'/resolve/service/nsadmin/setDefaultAccessRights',
			'SetDefaultRoleErrMsg',
			'SetDefaultRoleSucMsg',
			cvs
		);
	},

	configDefaultRoles: function() {
		var ids = [];

		Ext.each(this.recordsSelections, function(r) {
			ids.push(r.get('id'));
		});

		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/nsadmin/accessRights',
			params: {
				defaultRights: true,
				namespaces: ids
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('GetRolesErr'), respData.message);
					return;
				}

				if (!respData.data)
					return;

				this.open({
					mtype: 'RS.wiki.AccessRights',
					rights: respData.data,
					defaultRolesIsVisible: false,
					dumper: (function(self) {
						return function(cvs) {
							self.doSetDefaultRoles(cvs);
						}
					})(this)
				});
			},

			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	configDefaultRolesIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	}
});