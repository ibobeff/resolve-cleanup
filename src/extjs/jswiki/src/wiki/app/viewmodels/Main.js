glu.defModel('RS.wiki.Main', {
	wait: false,
	softLock: false,
	flagged: false,
	id: '',
	name: '',
	fullName: '',
	version: '',
	fullNameIsEditable: false,
	sysOrganizationName: '',
	accessRights: {},
	originalData: {},
	control: true, //displays the toolbar/header or not
	havePersistedWikiSinceLastView: false,
	dirtyFlag : false,
	dirtyComponents : [],
	isEditMode: false,
	isSaveAs: false,
	tableOfContentsWasPressed: false,
	persisting: false,
	tableOfContentsIsPressed: false,
	lastViewedWikiAutomation: new Date(),
	activeTab: 0,
	activeTabMap : {
		VIEW : 0,
		PAGE : 1,
		AUTOMATIONVIEW: 2,
		DECISIONTREEVIEW : 3,
		AUTOMATION : 4,
		DECISIONTREE : 5,
	},
	automationMode: 0,
	automationModeMap : {
		MAINMODEL : 0,
		ABORTMODEL : 1,
		DECISIONTREEMODEL : 2
	},
	activeScreenMap : {
		VIEW : 0,
		PAGE : 1,
		AUTOMATION_DECISIONTREE : 2,
		PARAMS : 3,
		AUTOMATION_DECISIONTREE_VIEW : 4
	},
	xmlWin: null,
	xmlIsPressed: false,
	executionWin: null,
	executionModel: null,
	socialIsPressed: false,
	feedbackWin: null,
	modelDirty: false,
	params: {
		worksheetOptionIsVisible: false,
		columnOptionIsVisible: false,
		mtype: 'RS.wiki.resolutionbuilder.Parameter'
	},
	parametersIsPressed: false,
	defaultData: {
		id: '',
		name: '',
		fullName: '',
		utitle: '',
		uname: '',
		unameIsEditable: false,
		unamespace: '',
		unamespaceIsEditable: false,
		ucontent: '',
		usummary: '',
		umodelProcess: '',
		umodelException: '',
		udecisionTree: '',
		udisplayMode: '',
		ucatalog: '',
		uwikiParameters: '',
		uisDefaultRole: true,
		flagged: false,
		ulastReviewedOn: '',
		ulastReviewedBy: '',
		uisLocked: false,
		uisStable: false,
		ulockedBy: '',
		uisDeleted: false,
		uisHidden: false,
		uisCurrentUserFollowing: false,
		uisDocumentStreamLocked: false,
		udtoptions : '',
		ucontent: '',
		usummary: '',
		sysCreatedOn: '',
		sysUpdatedOn: '',
		sysCreatedBy: '',
		sysUpdatedBy: '',
		sysOrganizationName: ''
	},
	userDateFormat$: function() {
		return this.getUserDefaultDateFormat();
	},
	getUserDefaultDateFormat : function(){
		return clientVM.getUserDefaultDateFormat();
	},
	fields: ['id', 'utitle', 'ufullname', 'ucontent', 'umodelProcess', 'udisplayMode', 'ucatalogId', 'uwikiParameters', 'umodelException', 'udecisionTree', 'udtoptions',{
		name: 'uisRequestSubmission',
		type: 'bool'
	}, {
		name: 'uisDefaultRole',
		type: 'bool'
	}, {
		name: 'uisDocumentStreamLocked',
		type: 'bool'
	}, {
		name: 'uisCurrentUserFollowing',
		type: 'bool'
	}, {
		name:'uisDeleted',
		type: 'bool'
	}, {
		name: 'uisHidden',
		type: 'bool'
	}, {
		name: 'uisLocked',
		type: 'bool'
	}, {
		name: 'uisStable',
		type: 'bool'
	}, 'ulockedBy', 'uname', 'unamespace', 'usummary', {
		name: 'ulastReviewedOn',
		type: 'date',
		dateFormat: 'time'
	}, 'ulastReviewedBy', {
		name: 'softLock',
		type: 'bool'
	}, 'lockedByUsername', 'uresolutionBuilderId'].concat(RS.common.grid.getSysFields()),

	automation: null,
	automationView: null,
	socialDetail: null,
	sectionsLayout: {
		mtype: 'RS.wiki.pagebuilder.SectionsLayout'
	},
	ucatalogStore: {
		mtype: 'store',
		fields: ['id', 'name'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/catalog/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	catalogs: {
		mtype: 'store',
		sorters: ['name'],
		fields: ['id', 'name'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	catalogsColumns: [{
		dataIndex: 'name',
		text: '~~name~~',
		flex: 1
	}],
	catalogsSelections: [],
	roles: {
		mtype: 'store',
		sorters: ['uname'],
		fields: ['id', 'uname', 'ureadAccess', 'uwriteAccess', 'uexecuteAccess', 'uadminAccess'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	tags: {
		mtype: 'store',
		sorters: ['name'],
		fields: ['id', 'name'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	tagsColumns: [{
		dataIndex: 'name',
		text: '~~name~~',
		flex: 1
	}],

	allTags: [],
	allATProperties: [],
	getAllTagsInProgress: false,
	getAllATPropertiesInProgress: false,
	csrftoken: '',

	//API
	API : {
		requestSoftLock : '/resolve/service/wiki/requestSoftLock',
		getSoftLockBy : '/resolve/service/wiki/getSoftLockBy',
		getWiki : '/resolve/service/wiki/get',
		getCatalog : '/resolve/service/catalog/get',
		wikiSaveAndCommit : '/resolve/service/wiki/saveandcommit',
		wikiSave : '/resolve/service/wiki/save',
		wikiSaveAs : '/resolve/service/wiki/saveAs',
		wikiAddGlobalAttachment : '/resolve/service/wiki/addGlobalAttachment',
		wikiSetLock : '/resolve/service/wiki/setLock',
		wikiSetSoftLock : '/resolve/service/wiki/setSoftLock',
		wikiAdminSetDeleted : '/resolve/service/wikiadmin/setDeleted',
		wikiAdminSetPurge : '/resolve/service/wikiadmin/purge',
		socialSetFollowStream : '/resolve/service/social/setFollowStreams',
		socialSetLockedStream : '/resolve/service/wiki/setLockedStream',
		getMockList : '/resolve/service/wiki/getMockList',
	},

	//REACTORS
	when_fullname_changes_update_name_and_namespace: {
		on: ['fullNameChanged'],
		action: function() {
			var names = this.fullName.split('.')
			this.set('unamespace', names[0] || '')
			this.set('uname', names[1] || '')
		}
	},
	updateWikiParams: function() {
		//parse the params and load them into the params grid
		var params;
		try {
			params = Ext.decode(this.uwikiParameters || '[]')
		} catch (e) {
			params = []
		}
		this.params.firstColumnStore.removeAll();
		this.params.firstColumnStore.add(params);
		this.params.firstColumnStore.sync();
	},
	when_wikiParameters_changes_update_grid: {
		on: ['uwikiParametersChanged'],
		action: function() {
			this.updateWikiParams();
		}
	},
	when_activeTab_changes_hide_view_components: {
		on: ['activeTabChanged'],
		action: function() {
			if (this.activeTab != this.activeTabMap['VIEW']) {
				this.set('tableOfContentsWasPressed', this.tableOfContentsIsPressed);
				this.set('tableOfContentsIsPressed', false);
			} else {
				this.set('tableOfContentsIsPressed', this.tableOfContentsWasPressed)
			}
		}
	},
	when_socialIsPressed_changes_store_state: {
		on: ['socialIsPressedChanged'],
		action: function() {
			Ext.state.Manager.set('wikiSocialIsPressed', this.socialIsPressed)
		}
	},
	when_xmlIsPressed_changed: {
		on: ['xmlIsPressedChanged'],
		action: function() {
			if (this.xmlIsPressed) {
				var me = this;
				var win = this.open({
					mtype: 'RS.wiki.automation.ModelXml',
					xml: this.automation.activeItem.xml,
					originalXml: this.automation.activeItem.xml,
					resolveModelType: this.automation.activeItem.resolveModelType,
					afterClose: function() {
						me.set('xmlIsPressed', false);
					}
				});
				win.on('xmlChanged', function() {
					me.automation.activeItem.set('xml', this.xml);
				});
				this.set('xmlWin', win);
			} else
			if (this.xmlWin)
				this.xmlWin.doClose();
		}
	},
	when_activeTab_changed: {
		on: ['activeTabChanged'],
		action: function() {
			window.sessionStorage.setItem('wikiActiveTab', this.activeTab);
		}
	},
	gotoActiveTab: function(activeTab) {
		if (this.isNewWikiFlag) {
			this.set('isNewWikiFlag', false);
			switch (activeTab) {
			case this.activeTabMap['PAGE']:
				this.pageTab();
				break;
			case this.activeTabMap['AUTOMATION']:
				this.automationTab();
				break;
			case this.activeTabMap['AUTOMATIONVIEW']:
				this.automationViewTab();
				break;
			case this.activeTabMap['DECISIONTREE']:
				this.decisionTreeTab();
				break;
			case this.activeTabMap['DECISIONTREEVIEW']:
				this.decisionTreeViewTab();
				break;
			case this.activeTabMap['VIEW']:
			default:
				this.pageViewTab();
				break;
			}
		} else {
			switch (activeTab) {
			case this.activeTabMap['AUTOMATION']:
			case this.activeTabMap['AUTOMATIONVIEW']:
				this.automationViewTab();
				break;
			case this.activeTabMap['DECISIONTREE']:
			case this.activeTabMap['DECISIONTREEVIEW']:
				this.decisionTreeViewTab();
				break;
			case this.activeTabMap['PAGE']:
			case this.activeTabMap['VIEW']:
			default:
				this.pageViewTab();
				break;
			}
		}
	},
	when_id_changed: {
		on: ['idChanged'],
		action: function() {
			var wikiId = window.sessionStorage.getItem('wikiId');
			if (wikiId && wikiId != this.id) {
				window.sessionStorage.removeItem('wikiActiveScreen');
				window.sessionStorage.removeItem('wikiActiveTab');
				window.sessionStorage.removeItem('wikiAutomationMode');
				return;
			}

			if (this.wikiParams && this.wikiParams.activeTab) {
				// do nothing since it's already done below in activate
			} else {
				// navigate to last activeTab
				var wikiActiveTab = window.sessionStorage.getItem('wikiActiveTab');
				if (wikiActiveTab) {
					Ext.defer(function() {
						this.gotoActiveTab(parseInt(wikiActiveTab));
					}, 1000, this);
				}
			}

			if (this.wikiParams && this.wikiParams.activeScreen) {
				// do nothing since it's already done below in activate
			} else {
				var wikiActiveScreen = window.sessionStorage.getItem('wikiActiveScreen');
				if (wikiActiveScreen == this.activeScreenMap['PARAMS']) {
					this.set('parametersIsPressed', true);
				}
			}

			if (this.wikiParams && this.wikiParams.automationMode) {
				// do nothing since it's already done below in activate
			} else {
				var wikiAutomationMode = window.sessionStorage.getItem('wikiAutomationMode');
				if (wikiAutomationMode == this.automationModeMap['MAINMODEL'] || wikiAutomationMode == this.automationModeMap['ABORTMODEL']) {
					this.set('automationMode', wikiAutomationMode);
				}
			}
		}
	},

	init: function() {
		this.initToken();
		clientVM.on('problemIdChanged', this.problemIdChangedHandler, this)

		if (this.rootVM.ns === 'RS.wiki') {
			var automation = this.model({
				mtype: 'RS.wiki.automation.Main'
			});
			this.set('automation', automation);

			var automationView = this.model({
				mtype: 'RS.wiki.automation.Main',
				viewOnly: true
			});
			this.set('automationView', automationView);

			var socialDetail = this.model({
				mtype: 'RS.social.Main',
				mode: 'embed',
				initialStreamType: 'worksheet'
			});
			this.set('socialDetail', socialDetail);

			this.screenVM.on('sociallinkclicked', this.socialLinkClicked, this)
			this.set('socialIsPressed', Ext.state.Manager.get('wikiSocialIsPressed', false));

			//For DT
			this.getDTSystemProperties();
		}

		clientVM.getResultMacroLimit();

		// generate page token for actiontask/editorstub.jsp
		clientVM.getCSRFToken_ForURI('/resolve/wiki/editorstub.jsp');
	},
	activateSoftLockChecking : function(){
		if(this.softLockCheckingTask)
			clearInterval(this.softLockCheckingTask);
		this.softLockCheckingTask = setInterval(function(){
			this.ajax({
				url : this.API.getSoftLockBy,
				params : {
					docName : this.ufullname
				},
				scope: this,
				success : function(r){
					var response = RS.common.parsePayload(r);
					this.set('softLock', response.success);
					if(response.success){
						this.set('lockedByUsername', response.data);
					}
					else if(this.sir)//FOR SIR
						this.setSoftLocked(true);
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
					// if logout flag set, stop polling
					if (clientVM.getLogoutFlag()) {
						clearInterval(this.softLockCheckingTask)
					}
				}
			})
		}.bind(this), 30 * 1000)
	},
	getDTSystemProperties : function(){
		var dtDTSystemProperties = {
			verticalAnswerLayout : false,
			questionOnTop : true,
			autoConfirm : false
		};
		this.ajax({
			url : '/resolve/service/sysproperties/getAllProperties',
			params : {
				filter: Ext.encode([{"field":"uname","type":"auto","condition":"contains","value":"dt.option"}])
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					if (response.records) {
						Ext.Array.each(response.records, function(record){
							var propertyName = record.uname;
							if(propertyName == "dt.option.radio.vertical")
								dtDTSystemProperties['verticalAnswerLayout'] = (record.uvalue == 'true');
							if(propertyName == "dt.option.autoNavigate")
								dtDTSystemProperties['autoConfirm'] = (record.uvalue == 'true');
							if(propertyName == "dt.option.showQuestionOnBottom"){
								dtDTSystemProperties['questionOnTop'] = (record.uvalue != 'true');
							}
							if(propertyName == "dt.option.isAutoAnswerDisabled"){
								dtDTSystemProperties['isAutoAnswerDisabled'] = (record.uvalue == 'true');
							}
						}, this)
					}
					Ext.apply(this.dtDefaultTheme, dtDTSystemProperties);
				} else {
					clientVM.displayError(response.message)
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	beforeDestroyComponent : function(){
		clientVM.removeListener('problemIdChanged', this.problemIdChangedHandler);
		if(this.softLockCheckingTask){
			clearInterval(this.softLockCheckingTask);
			//Disable Lock on this
			this.setSoftLocked(false);
		}

		//Remove this handler from document's event.
		this.fireEvent('removeEventReference');
	},
	problemIdChangedHandler :  function() {
		Ext.defer(function() {
			//update social detail display
			if (this.socialDetail) {
				this.socialDetail.set('streamParent', clientVM.problemNumber)
				this.socialDetail.set('initialStreamId', clientVM.problemId)
			}
		}, 100, this)
	},
	wikiParams: null,
	activate: function(screen, params) {
		this.initToken();
		if (params && params['newWiki']) {
			this.open({
				mtype: 'RS.wiki.AddDocument'
			})
			return;
		}

		if (params) {
			this.set('wikiParams', params);
		}

		clientVM.setWindowTitle(this.localize('wikiDocument'))
		this.resetForm()
		this.set('id', params ? params.id || '' : '')
		this.set('name', params ? params.name || '' : '')
		this.set('version', '');
		/* TODO - support Version Control
		if (params && params.version) {
			var version = params.version.toLowerCase();
			if (version == 'lateststable' || version == 'latestcommit' || version == 'latestcommitted') {
				this.set('version', 'latestStable');
			} else if (Ext.isNumeric(version)) {
				this.set('version', version);
			}
		}
		*/ 
		this.set('wikiURL', this.getWikiURL());
		if (this.name == 'HOMEPAGE') {
			this.set('name', clientVM.user.homePage)
		}
		this.set('fullName', this.name || '')
		this.set('control', params && params.control == 'false' ? false : true)
			//we need to devide this big chunck of code into smaller modules and eliminate the if else and switch stuff for the activeTab/Mode...
		if (params && params.activeTab) {
			Ext.defer(function() {
				this.gotoActiveTab(parseInt(params.activeTab));
			}, 1000, this);
		}

		if (params && params.activeScreen) {
			this.set('activeScreen', Number(params.activeScreen));
			window.sessionStorage.setItem('wikiActiveScreen', this.activeScreen);
		}

		if (params && params.automationMode) {
			this.set('automationMode', Number(params.automationMode));
			window.sessionStorage.setItem('wikiAutomationMode', this.automationMode);
		}

		this.set('isEditMode', params ? params.isEditMode : null);

		/*
		this.ucatalogStore.load();
		this.loadWikiDocument();

		Ext.defer(function() {
			window.sessionStorage.setItem('wikiId', this.id);
		}, 2000, this);
		*/
		/*
		var delay = 0;
		var windowParams = Ext.Object.fromQueryString(window.location.search || '');
		if (windowParams && windowParams._username_ && windowParams._password_) {
			delay = 1000;
		}
		*/
		setTimeout(function(){
			this.loadWikiDocument(null, function(){
				this.ucatalogStore.load();
				window.sessionStorage.setItem('wikiId', this.id);
			}.bind(this));
		}.bind(this), 100/*delay*/)
	},
	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/wiki/view', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this));
	},
	ufullname$: function() {
		if(this.unamespace && this.uname){
			return this.unamespace + '.' + this.uname
		}
	},
	fullNameIsValid$: function() {
		if (!this.fullName) return this.localize('nameRequired')
		if (!/[\w]+[.]{1}[\w]+/.test(this.fullName)) return this.localize('invalidWikiNameFormat')
		return true
	},

	/* TODO - remove to support Version Control */
	lockedDisplayText$: function() {
		return this.softLock && this.lockedByUsername != user.name && !this.viewTabIsPressed ? this.localize('lockedBy', [this.lockedByUsername]) : ''
	},
	overrideLock: function() {
		this.message({
			title: this.localize('overrideSoftLock'),
			msg: this.localize('confirmOverride'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('overrideSoftLockAction'),
				no: this.localize('cancel')
			},
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.ajax({
					url: this.API.requestSoftLock,
					params: {
						docName: this.ufullname
					},
					success: function(resp) {
						var respData = RS.common.parsePayload(resp);
						if (!respData.success) {
							clientVM.displayError(respData.message);
							return;
						}
						if (respData.data && respData != user.name) {
							clientVM.displayError(respData.message);
							return;
						}
						this.setRaw('lockedByUsername', user.name, true);
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				});
			}
		});
	},

	ucatalogIdIsEnabled$: function() {
		return this.udisplayMode == 'catalog'
	},
	notifyDirtyFlag : function(component){
		this.set('dirtyFlag', true);
		if (this.dirtyComponents.indexOf(component) == -1) {
			this.dirtyComponents.push(component);
		}
	},
	clearComponentDirtyFlag : function(component){
		if (this.dirtyComponents.indexOf(component) != -1) {
			this.dirtyComponents.splice(component, 1)
		}
		if (!this.dirtyComponents.length) {
			this.clearDirtyFlag();
		}
	},
	clearDirtyFlag: function() {
		this.dirtyComponents = [];
		this.set('dirtyFlag', false);
	},
	wikiTitle$: function() {
		var fullName = this.ufullname;
		var	name = this.name;
		var	title = this.utitle;
		var realName = fullName || name;
		var dirtyFlag = this.dirtyFlag ? "*"  : "";
		var	winTitle = realName + (realName != title && title ? ' - ' + title : '') + dirtyFlag;
		this.setWindowTitle(winTitle);
		return winTitle;
	},
	setWindowTitle : function(newTitle){
		clientVM.setWindowTitle(newTitle);
	},
	deactivate: function() {
		this.cleanIframes();
	},
	cleanIframes: function() {
		//Clean up anything like leftover iframes and links
		var frame = document.getElementById('wiki_frame');
		if(frame && frame.contentWindow)
			frame.contentWindow.postMessage('beforedestroy', '/');
	},
	resetForm: function() {
		this.loadData(this.defaultData)
			//correct booleans
		this.set('uisDeleted', this.defaultData.uisDeleted)
		this.set('uisLocked', this.defaultData.uisLocked)
		this.set('uisStable', this.defaultData.uisStable)
	},
	activeScreen$: function() {
		switch (this.activeTab) {
			case this.activeTabMap['PAGE']:
				return this.activeScreenMap['PAGE'];
				break;
			case this.activeTabMap['AUTOMATION']:
				if (this.parametersIsPressed) {
					window.sessionStorage.setItem('wikiActiveScreen', this.activeScreenMap['PARAMS']);
					return this.activeScreenMap['PARAMS'];
				}
				window.sessionStorage.setItem('wikiActiveScreen', this.activeScreenMap['AUTOMATION_DECISIONTREE']);
				return this.activeScreenMap['AUTOMATION_DECISIONTREE'];
				break;
			case this.activeTabMap['DECISIONTREE']:
				return this.activeScreenMap['AUTOMATION_DECISIONTREE'];
				break;
			case this.activeTabMap['AUTOMATIONVIEW']:
				if (this.parametersIsPressed) {
					window.sessionStorage.setItem('wikiActiveScreen', this.activeScreenMap['PARAMS']);
					return this.activeScreenMap['PARAMS'];
				}
				window.sessionStorage.setItem('wikiActiveScreen', this.activeScreenMap['AUTOMATION_DECISIONTREE_VIEW']);
				return this.activeScreenMap['AUTOMATION_DECISIONTREE_VIEW'];
				break;
			case this.activeTabMap['DECISIONTREEVIEW']:
				return this.activeScreenMap['AUTOMATION_DECISIONTREE_VIEW'];
				break;
			case this.activeTabMap['VIEW']:
			default:
				return this.activeScreenMap['VIEW'];
				break;
		}
	},
	gotoViewTab: function() {
		if (this.pageTabIsPressed) {
			this.pageViewTab();
		} else if (this.automationTabIsPressed) {
			this.automationViewTab();
		} else if (this.decisionTreeTabIsPressed) {
			this.decisionTreeViewTab();
		}
	},
	exitToView: function() {
		this.viewTab();
	},
	viewTab: function() {
		if (this.dirtyFlag) {
			this.message({
				title: this.localize('exitWithoutSaveTitle'),
				msg: this.localize('exitWithoutSaveBody'),
				buttons: Ext.MessageBox.YESNOCANCEL,
				buttonText: {
					yes: this.localize('save'),
					no: this.localize('dontSave'),
					cancel: this.localize('cancel')
				},
				scope: this,
				fn: function(btn) {
					if (btn == "yes") {
						this.save(null, null, null, function() {
							this.reloadWikiDocument();
						})
					} else if (btn == "no") {
						this.reloadWikiDocument();
					}
				}
			})
		} else {
			this.setSoftLocked(false);
			this.gotoViewTab();
		}
	},

	reloadWikiDocument: function(editModeFlag) {
		//Turn off dirty flag checking on Automation
		this.automation.stopDirtyFlagChecking();
		//Always clean up before reload to make sure no memory is leaking.
		this.loadWikiDocument();
		this.set('lastViewedWiki', new Date());
		if (!editModeFlag) {
			this.setSoftLocked(false);
			this.gotoViewTab();
		} else {
			this.automation.startDirtyFlagChecking();
		}
	},

	loadLatestWikiDocument: function() {
		this.set('version', '');
		clientVM.removeURLHashVersion();
		this.reloadWikiDocument(true);
	},

	pageTabIsPressed$: function() {
		var pageTabIsPressed = (this.activeTab == this.activeTabMap['PAGE']);
		//this.fireEvent('pageTabIsPressed', pageTabIsPressed);
		return pageTabIsPressed;
	},
	pageViewTabIsPressed$: function() {
		var pageViewTabIsPressed = (this.activeTab == this.activeTabMap['VIEW']);
		//this.fireEvent('pageViewTabIsPressed', pageViewTabIsPressed);
		return pageViewTabIsPressed;
	},
	automationTabIsPressed$: function() {
		var automationTabIsPressed = (this.activeTab == this.activeTabMap['AUTOMATION']);
		this.fireEvent('automationTabIsPressed', automationTabIsPressed);
		return automationTabIsPressed;
	},
	automationViewTabIsPressed$: function() {
		var automationViewTabIsPressed = (this.activeTab == this.activeTabMap['AUTOMATIONVIEW']);
		//this.fireEvent('automationViewTabIsPressed', automationViewTabIsPressed);
		return automationViewTabIsPressed;
	},
	decisionTreeTabIsPressed$: function() {
		var decisionTreeTabIsPressed = (this.activeTab == this.activeTabMap['DECISIONTREE']);
		this.fireEvent('decisionTreeTabIsPressed', decisionTreeTabIsPressed);
		return decisionTreeTabIsPressed;
	},
	decisionTreeViewTabIsPressed$: function() {
		var decisionTreeViewTabIsPressed = (this.activeTab == this.activeTabMap['DECISIONTREEVIEW']);
		this.fireEvent('decisionTreeViewTabIsPressed', decisionTreeViewTabIsPressed);
		return decisionTreeViewTabIsPressed;
	},

	viewTabIsPressed$: function() {
		return this.pageViewTabIsPressed || this.automationViewTabIsPressed || this.decisionTreeViewTabIsPressed;
	},
	readOnlyTextIsVisible$: function() {
		return this.id && (this.automationViewTabIsPressed || this.decisionTreeViewTabIsPressed);
	},

	versionText$: function() {
		return this.originalData && this.originalData.uversion ? this.localize('versionTxt', this.originalData.uversion) : '';
	},

	automationTabEitherIsPressed$: function() {
		return (this.automationTabIsPressed || this.automationViewTabIsPressed);
	},
	pageAndAutomationTreeVisible$: function() {
		return this.pageTabIsPressed || this.automationTabIsPressed
	},
	exitToViewIsVisible$: function() {
		return (!this.viewTabIsPressed);
	},
	exitToViewIsEnabled$ : function(){
		return !this.wait;
	},
	editPage: function() {
		if (this.pageViewTabIsPressed)
			this.pageTab();
		else if (this.automationViewTabIsPressed)
			this.automationTab();
		else if (this.decisionTreeViewTabIsPressed)
			this.decisionTreeTab();
	},
	pageTab: function() {
		this.set('activeTab', this.activeTabMap['PAGE']);
		if (!this.softLock) {
			this.setSoftLocked(true);
		}
		// This is not quiet the most efficient way but it prevents the changes being lost
		// after switching back and forth between tabs on edit mode.
		if (!this.dirtyFlag) {
			this.sectionsLayout.parseSection(this.ucontent);
		}
	},
	pageTabIsEnabled$ : function(){
		return true; //(this.pageTabIsPressed) || !this.dirtyFlag;
	},
	pageTabIsVisible$: function() {
		return this.isEditable && !this.viewTabIsPressed;
	},
	automationTab: function() {
		if (this.automation.xmlDirtyFlag) {
			this.notifyDirtyFlag(this.viewmodelName);
			this.automation.clearXmlDirty();
		} else {
			this.clearComponentDirtyFlag(this.viewmodelName);
		}
		this.set('lastViewedWikiAutomation', new Date());
		this.set('activeTab', this.activeTabMap['AUTOMATION']);
		this.validateAutomationModel(null, function(errMsgs) {
			this.message({
				title: this.localize('invalidModel'),
				msg: errMsgs.join('<br/>'),
				buttons: Ext.MessageBox.OK
			})
		});
		if (window.sessionStorage.getItem('wikiAutomationMode') == this.automationModeMap['ABORTMODEL']) {
			this.abortModel();
		} else {
			this.mainModel();
		}
		this.setSoftLocked(true);
		if (!this.softLock) this.setSoftLocked(true);
	},
	automationTabIsVisible$: function() {
		return this.isEditable
	},
	automationTabIsEnabled$ :function(){
		return true; //(this.automationTabIsPressed) || !this.dirtyFlag;
	},
	validateAutomationModel: function(onSuccess, onError) {
		var result = this.automation.validate(function(respData) {
			if (!respData.success) {
				clientVM.displayError(this.localize('validationError', respData.message));
				return;
			}
			var validations = respData.data;
			var msgs = [];
			Ext.Object.each(validations, function(modelType, validation) {
				if (validation.task.length > 0)
					msgs.push(this.localize('invalidTasksDetected', [this.localize(modelType), validation.task.join()]));
				if (validation.runbook.length > 0)
					msgs.push(this.localize('invalidRunbooksDetected', [this.localize(modelType), validation.runbook.join()]));
				if (validation.document.length > 0)
					msgs.push(this.localize('invalidDocumentsDetected', [this.localize(modelType), validation.document.join()]));
				if (validation.danglingNodes.length > 0)
					msgs.push(this.localize('danglingNodesDetected', [this.localize(modelType), validation.danglingNodes.join()]));
				if (validation.invalidEdges.length > 0)
					msgs.push(this.localize('invalidEdgesDetected', [this.localize(modelType), validation.invalidEdges.join()]));
			}, this);
			if (msgs.length == 0) {
				if (Ext.isFunction(onSuccess)) {
					onSuccess.call(this);
				}
			} else {
				if (Ext.isFunction(onError)) {
					onError.call(this, msgs);
				}
			}
		}, this);
	},

	decisionTreeTab: function() {
		this.set('activeTab', this.activeTabMap['DECISIONTREE']);
		if (!this.softLock) this.setSoftLocked(true);
		this.set('automationMode', this.automationModeMap['DECISIONTREEMODEL']);
		window.sessionStorage.setItem('wikiAutomationMode', this.automationMode);
		this.automation.decisionTreeTab();
	},
	decisionTreeTabIsVisible$: function() {
		return this.isEditable;
	},
	decisionTreeTabIsEnabled$ :function(){
		return true; //(this.decisionTreeTabIsPressed) || !this.dirtyFlag;
	},

	pageViewTab: function() {
		this.set('activeTab', this.activeTabMap['VIEW']);

		if (this.havePersistedWikiSinceLastView) {
			this.loadWikiDocument();
			this.set('lastViewedWiki', new Date());
			this.set('havePersistedWikiSinceLastView', false);
		}
	},
	automationViewTab: function() {
		this.set('activeTab', this.activeTabMap['AUTOMATIONVIEW']);
		if (window.sessionStorage.getItem('wikiAutomationMode') == this.automationModeMap['ABORTMODEL']) {
			this.abortModel();
		} else {
			this.mainModel();
		}
	},
	decisionTreeViewTab: function() {
		this.set('activeTab', this.activeTabMap['DECISIONTREEVIEW']);
		this.automationView.decisionTreeTab();
	},

	autoViewOrDtreeViewIsPressed$: function() {
		return this.automationViewTabIsPressed || this.decisionTreeViewTabIsPressed;
	},

	designerPanelReadOnlyText$: function() {
		if (this.autoViewOrDtreeViewIsPressed) {
			return '<br><span class="readonlytxt">'+this.localize('designerPanelReadOnlyTxt')+'</span>';
		} else {
			return '';
		}
	},
	designerPanelTitle$: function() {
		return this.localize('designerPanel') + this.designerPanelReadOnlyText;
	},

	isEditable$: function() {
		return this.accessRights ? hasPermission(this.accessRights.uwriteAccess + ',' + this.accessRights.uadminAccess) : false //Can edit with edit or admin access
	},
	isNewWikiFlag: false,
	loadWikiDocument: function(xmlOnly, onSuccess, onFailure) {
		if (this.id || this.name) {
			this.roles.removeAll()
			this.tags.removeAll()
			this.catalogs.removeAll()
			this.set('wait', true);
			this.clearDirtyFlag();
			var params = {
				id: this.id,
				name: this.name
			};
			/* TODO - support Version Control
			if (this.version) {
				Ext.apply(params, {
					version: this.version
				})
			}
			*/
			this.ajax({
				url: this.API.getWiki,
				params: params,
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
					  if (response.data) {

						//Check display mode and edit mode, if we're not in edit mode and display mode is not wiki, then redirect immediately
						if (!this.isEditMode && !response.data.uisDeleted) {
							var hasWriteAccess = hasPermission(response.data.accessRights.uwriteAccess)
							if (response.data.udisplayMode == 'decisiontree') {
								clientVM.handleNavigation({
									//modelName: 'RS.wiki.DecisionTreeViewer',
									modelName: 'RS.decisiontree.Main',
									params: {
										name: response.data.ufullname
									}
								})
								/*if (hasWriteAccess) {
									clientVM.displayMessage(response.data.ufullname, this.localize('autoRedirectMsg', response.data.ufullname))
								}*/
								return
							}
							if (response.data.udisplayMode == 'playbook' && this.rootVM.ns === 'RS.wiki') {
								clientVM.handleNavigation({
									modelName: 'RS.incident.PlaybookTemplateRead',
									params: {
										name: response.data.ufullname
									}
								})
								return
							}
							if (response.data.udisplayMode == 'catalog' && response.data.ucatalogId) {
								var catalogId = response.data.ucatalogId
								this.ajax({
									url: this.API.getCatalog,
									params: {
										id: catalogId
									},
									success: function(r) {
										var resp = RS.common.parsePayload(r)
										if (resp.success && resp.data) {
											clientVM.handleNavigation({
												modelName: resp.data.catalogType == 'training' ? 'RS.catalog.TrainingViewer' : 'RS.catalog.CatalogViewer',
												params: {
													id: catalogId
												}
											})
											if (hasWriteAccess) {
												clientVM.displayMessage(response.data.ufullname, '', {
													msg: this.localize('autoRedirectMsg', response.data.ufullname)
												});
											}
										} else clientVM.displayError(resp.message)
									},
									failure: function(r) {
										clientVM.displayFailure(r);
									}
								})
								return
							}
						}

						//load roles regardless of the new/edit because we'll either get the default roles (new) or the specific roles (edit)
						//Also load these roles first so that we can react approprately to what the user is capable of doing
						if (response.data.accessRights) {
							this.set('accessRights', response.data.accessRights)
							this.processAccessRights(response.data.accessRights)
						}
						if (response.data.uwikiParameters) {
							this.set('uwikiParameters', response.data.uwikiParameters);
							this.updateWikiParams();
						}

						if (response.data.id && response.data.id != 'UNDEFINED') {
							clientVM.addToWikiHistory(response.data)
							if (xmlOnly) {
								this.set('umodelProcess', response.data.umodelProcess)
								this.set('umodelException', response.data.umodelException)
								this.set('udecisionTree', response.data.udecisionTree);
							} else {
								this.set('originalData', response.data);
								this.loadData(response.data);
								this.set('name', response.data.ufullname);
								this.set('flagged', response.data.flagged);
								this.set('uisDefaultRole', response.data.uisDefaultRole == true);
								this.set('uisDocumentStreamLocked', response.data.uisDocumentStreamLocked);
								this.set('uisCurrentUserFollowing', response.data.uisCurrentUserFollowing);
								this.setRaw('softLock', response.data.softLock, true);

								//setup social detail display
								if (this.socialDetail) {
									this.socialDetail.set('initialStreamId', clientVM.problemId)
									this.socialDetail.set('streamParent', clientVM.problemNumber)
								}

								//correct booleans
								this.set('uisDeleted', response.data.uisDeleted)
								this.set('uisLocked', response.data.uisLocked)
								this.set('uisStable', response.data.uisStable)

								//load tags, catalogs
								var tags = (response.data.utag || '').split(','),
									tagsStoreData = [];
								Ext.Array.forEach(tags, function(tag) {
									if (tag)
										tagsStoreData.push({
											name: tag,
											id: tag
										})
								})
								this.tags.loadData(tagsStoreData)

								var catalogs = (response.data.ucatalog || '').split(','),
									catalogsStoreData = [];
								Ext.Array.forEach(catalogs, function(catalog) {
									if (catalog)
										catalogsStoreData.push({
											name: catalog,
											id: catalog
										})
								})
								this.catalogs.loadData(catalogsStoreData);
							}
							//Tell Automation that XML is loaded and ready
							if (this.automation) {
								this.automation.set('dataIsReady', true);
							}
							this.sectionsLayout.parseSection(this.ucontent);

							//For Decision Tree
							this.parseDTVariables();
						} else {
							if (this.isEditable) {
								//display message to user that we created this document for them
								clientVM.displayMessage(this.localize('autoCreatedTitle'), '', {
									msg: this.localize('autoCreatedMessage', Ext.htmlEncode(this.name))
								});
								var splits = this.name.split('.');
								if (splits.length > 1) {
									this.set('unamespace', splits[0])
									this.set('uname', splits[1])
									this.set('fullNameIsEditable', true)
								}
								if (this.id) {
									this.pageViewTab();
									this.set('isNewWikiFlag', false);
								} else {
									this.set('isNewWikiFlag', true);
									this.pageTab();
								}
							}
						}
						if (Ext.isFunction(onSuccess)) {
							onSuccess();
						}
						this.fireEvent('pageLoaded');
						//Periodically check for softlock
						this.activateSoftLockChecking();
					  }
					}
					else {
						clientVM.displayError(response.message);

						if (Ext.isFunction(onFailure)) {
							onFailure();
						}
					}
				},
				failure: function(r) {
					clientVM.displayFailure(r);

					if (Ext.isFunction(onFailure)) {
						onFailure();
					}
				},
				callback: function() {
					this.set('wait', false);
				}
			})
		} else {
			this.pageTab()
		}
	},
	processAccessRights: function(accessRights) {
		// this.roles.removeAll()
		while (this.roles.getCount() > 0) this.roles.removeAt(0) //Not sure while removeAll doesn't work, but there is a bug with extjs so this is a hack to get it to work.  See RBA-8690 for more details
		var viewRoles = accessRights.ureadAccess ? accessRights.ureadAccess.split(',') : [],
			editRoles = accessRights.uwriteAccess ? accessRights.uwriteAccess.split(',') : [],
			executeRoles = accessRights.uexecuteAccess ? accessRights.uexecuteAccess.split(',') : [],
			adminRoles = accessRights.uadminAccess ? accessRights.uadminAccess.split(',') : [],
			roles = [],
			i;

		for (i = 0; i < viewRoles.length; i++)
			this.addToRoles(roles, viewRoles[i], 'ureadAccess', true)
		for (i = 0; i < editRoles.length; i++) {
			this.addToRoles(roles, editRoles[i], 'uwriteAccess', true)
			//automatically add to read access if already have write access
			this.addToRoles(roles, editRoles[i], 'ureadAccess', true)
		}
		for (i = 0; i < executeRoles.length; i++)
			this.addToRoles(roles, executeRoles[i], 'uexecuteAccess', true)
		for (i = 0; i < adminRoles.length; i++) {
			this.addToRoles(roles, adminRoles[i], 'uadminAccess', true)
			//automatically add to read, write, and execute access if already have admin access
			this.addToRoles(roles, adminRoles[i], 'ureadAccess', true)
			this.addToRoles(roles, adminRoles[i], 'uwriteAccess', true)
			this.addToRoles(roles, adminRoles[i], 'uexecuteAccess', true)
		}

		for (i = 0; i < roles.length; i++) {
			if (typeof(roles[i]['uadminAccess']) === 'undefined') {
				roles[i]['uadminAccess'] = false;
			}
			if (typeof(roles[i]['uwriteAccess']) === 'undefined') {
				roles[i]['uwriteAccess'] = false;
			}
			if (typeof(roles[i]['uexecuteAccess']) === 'undefined') {
				roles[i]['uexecuteAccess'] = false;
			}
			if (typeof(roles[i]['ureadAccess']) === 'undefined') {
				roles[i]['ureadAccess'] = false;
			}
		}

		this.roles.loadData(roles);
		this.roles.sync();
	},
	addToRoles: function(roles, name, right, value) {
		var i, len = roles.length;
		for (i = 0; i < len; i++) {
			if (roles[i].uname == name) {
				roles[i][right] = value
				return
			}
		}

		var newName = {
			uname: name
		};
		newName[right] = value
		roles.push(newName);
	},
	back: function() {
		clientVM.handleNavigationBack()
	},
	refresh: function() {
		this.loadWikiDocument();
		if (this.viewTabIsPressed) this.set('lastViewedWiki', new Date());
		if (this.automationTabIsPressed) this.set('lastViewedWikiAutomation', new Date());
	},
	refreshIsVisible$: function() {
		return this.activeTab != this.activeTabMap['VIEW'];
	},
	/* Legacy code
	refreshWithoutSave: function(btn) {
		if (btn === 'yes') {
			this.save(null, null, null, function() {
				this.loadWikiDocument()
				if (this.viewTabIsPressed) this.set('lastViewedWiki', new Date())
				if (this.automationTabIsPressed) this.set('lastViewedWikiAutomation', new Date())
			})
		} else if (btn === 'no') {
			this.loadWikiDocument();
			//this.parseSections();
			if (this.viewTabIsPressed) this.set('lastViewedWiki', new Date())
			if (this.automationTabIsPressed) this.set('lastViewedWikiAutomation', new Date())
		}
	},
	*/
	reload: function(){
		if (this.dirtyFlag) {
			this.message({
				title: this.localize('reloadWithoutSaveTitle'),
				msg: this.localize('reloadWithoutSaveBody'),
				buttons: Ext.MessageBox.YESNO,
				buttonText: {
					yes: this.localize('confirm'),
					no: this.localize('cancel')
				},
				scope: this,
				fn: this.reloadContent
			})
		}
		else {
			this.loadWikiDocument();
			if (this.automation) this.automation.resetActionTaskList();
			if (this.automationView)this.automationView.resetActionTaskList();
		}
	},
	reloadContent : function(btn){
		if(btn == 'yes'){
			/*
			if(this.pageTabIsPressed)
				this.sectionsLayout.parseSection(this.ucontent);
			else if(this.automationTabIsPressed && this.automationMode == this.automationModeMap['MAINMODEL'])
				this.set('umodelProcess', this.originalData.umodelProcess);
			else if(this.automationTabIsPressed && this.automationMode == this.automationModeMap['ABORTMODEL'])
				this.set('umodelException', this.originalData.umodelException);
			else if(this.decisionTreeTabIsPressed){
				this.set('udecisionTree', this.originalData.udecisionTree);
				this.parseDTVariables();
			}
			*/
			this.loadWikiDocument(null, function(){
				this.clearDirtyFlag();
			}.bind(this));

			if (this.automation) this.automation.resetActionTaskList();
			if (this.automationView) this.automationView.resetActionTaskList();
		}
	},
	parametersTab: function() {},
	parametersTabIsVisible$: function() {
		return (this.automationTabIsPressed || this.automationViewTabIsPressed) && this.parametersIsPressed;
	},
	backFromParameterIsVisible$: function() {
		return this.automationTabIsPressed && this.parametersIsPressed;
	},
	backToMainModel: function() {
		this.set('parametersIsPressed', false);
		this.mainModel();
	},
	backToMainModelIsVisible$: function() {
		return (this.automationTabIsPressed || this.automationViewTabIsPressed) && this.parametersIsPressed;
	},
	backToAbortModel: function() {
		this.set('parametersIsPressed', false);
		this.abortModel();
	},
	backToAbortModelIsVisible$: function() {
		return (this.automationTabIsPressed || this.automationViewTabIsPressed) && this.parametersIsPressed;
	},
	verifyCommitVersion: function(comment, postToSocial, reviewed) {
		/* TODO - support Version Control
		this.ajax({
			url: '/resolve/service/wiki/getHistory',
			params: {
				docSysId: this.id,
				docFullName: this.name
			},
			success: function(resp) {
				var response = RS.common.parsePayload(resp);
				if (response.success) {
					if (response.records && response.records.length && response.records[0].version > this.originalData.uversion) {
						this.displayCommitWarning(comment, postToSocial, reviewed);
					} else {
						this.doSaveAndCommit(comment, postToSocial, reviewed);
					}
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
		*/
		this.doSaveAndCommit(comment, postToSocial, reviewed); // TODO - remove to support Version Control
	},
	displayCommitWarning: function(comment, postToSocial, reviewed) {
		this.message({
			title: this.localize('attention'),
			msg: this.localize('newVersionsAvailabe'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('commitNewVersion'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn == 'yes') {
					this.doSaveAndCommit(comment, postToSocial, reviewed);
				}
			},
		});
		this.doClose()
	},
	doSaveAndCommit: function(comment, postToSocial, reviewed) {
		this.save(false, true, {
			comment: comment,
			postToSocial: postToSocial,
			reviewed: reviewed
		})
	},
	saveAndCommit: function() {
		this.open({
			mtype: 'RS.wiki.Commit'
		})
	},
	saveAndCommitIsVisible$: function() {
		return this.saveIsVisible;
	},
	saveAndCommitIsEnabled$: function() {
		/* TODO - support Version Control
		return !this.persisting && !this.wait && (!this.uisLocked || (this.uisLocked && this.ulockedBy == user.name)) && (!this.uisStable || this.dirtyFlag); 
		*/ 
		return !this.persisting && !this.wait && (!this.uisLocked || (this.uisLocked && this.ulockedBy == user.name)); // TODO - remove to support Version Control 
	},
	saveAsIsVisible$: function() {
		return this.saveIsVisible;
	},
	saveAs: function() {
		this.open({
			mtype: 'RS.wiki.SaveAsWiki',
			name: this.uname,
			namespace: this.unamespace,
			id: this.id
		})
	},
	newName: '',
	newNamespace: '',
	choice: '',
	doSaveAs: function(namespace, name, choice, onSuccess, onFailure) {
		this.set('newNamespace', namespace);
		this.set('newName', name);
		this.set('choice', choice);
		this.set('isSaveAs', true);

		this.save(null, null, null, function() {
			this.set('isSaveAs', false);
			if (Ext.isFunction(onSuccess)) {
				onSuccess();
			}
		}.bind(this),
		function() {
			this.set('isSaveAs', false);
			if (Ext.isFunction(onFailure)) {
				onFailure();
			}
		}.bind(this))
	},
	doSave: function(exitAfterSave, commit, commitParams, onSuccess, onFailure, scope) {
		if (this.task) this.task.cancel()
		if (this.automation) {
			this.task = new Ext.util.DelayedTask(this.persistWikiDocumentAfterValidateAutomation, this, [exitAfterSave, commit, commitParams, onSuccess, onFailure, scope])
		} else {
			this.task = new Ext.util.DelayedTask(this.persistWikiDocument, this, [exitAfterSave, commit, commitParams, onSuccess, onFailure, scope])
		}
		this.task.delay(500)
		this.set('persisting', true)
	},
	save: function(exitAfterSave, commit, commitParams, onSuccess, onFailure, scope) {
		if (this.sourceEditWin) {
			this.sourceEditWin.save();
		}
		this.doSave(exitAfterSave, commit, commitParams, onSuccess, onFailure, scope);
	},
	saveIsEnabled$: function() {
		/* TODO - support Version Control
		if (this.dirtyFlag) {
			return !this.persisting && !this.wait && (!this.uisLocked || (this.uisLocked && this.ulockedBy == user.name)); //&& this.isValid
		} else if (this.sourceEditWin) {
			return true;
		} else {
			return false;
		}
		*/
		return true; // TODO - remove to support Version Control
	},
	saveIsVisible$: function() {
		return !this.viewTabIsPressed
	},
	saveAndExit: function() {
		if (this.wait)
			return;
		if (this.task) this.task.cancel()
		this.save(false, true)
	},
	persistWikiDocumentAfterValidateAutomation: function(exitAfterSave, commit, commitParams, onSuccess, onFailure, scope) {
		this.validateAutomationModel(function(){
			this.persistWikiDocument(exitAfterSave, commit, commitParams, onSuccess, onFailure, scope);
		},
		function(errMsgs) {
			errMsgs.push('<br/>'+this.localize('saveAnyWay'));
			this.message({
				title: this.localize('invalidModel'),
				msg: errMsgs.join('<br/>'),
				buttons: Ext.MessageBox.YESNO,
				buttonText: {
					yes: this.localize('save'),
					no: this.localize('no')
				},
				fn: function(btn) {
					if (btn != 'yes') {
						this.set('persisting', false);
						return;
					}
					this.persistWikiDocument(exitAfterSave, commit, commitParams, onSuccess, onFailure, scope);
				}
			})
		});
	},
	persistWikiDocument: function(exitAfterSave, commit, commitParams, onSuccess, onFailure, scope) {
		if (this.wait)
			return
		if (this.dirtyFlag) {
			// Get the new content from the page builder' sections
			this.set('ucontent', this.getPageContent());
		}

		//Default theme for DT if none specified.
		if(!this.udtoptions)
			this.buildDTOption();
		var wikiParams = this.asObject();
		wikiParams.ucontent = wikiParams.ucontent.replace(/\u200d/g, '');
		wikiParams.uisStable = commit ? true : false;

		//add in the roles
		var ureadAccess = [],
			uwriteAccess = [],
			uexecuteAccess = [],
			uadminAccess = [],
			tags = [],
			catalogs = [];

		this.roles.each(function(role) {
			if (role.get('ureadAccess')) ureadAccess.push(role.get('uname'))
			if (role.get('uwriteAccess')) uwriteAccess.push(role.get('uname'))
			if (role.get('uexecuteAccess')) uexecuteAccess.push(role.get('uname'))
			if (role.get('uadminAccess')) uadminAccess.push(role.get('uname'))
		})

		wikiParams.accessRights = Ext.apply(this.accessRights, {
			ureadAccess: ureadAccess.join(','),
			uwriteAccess: uwriteAccess.join(','),
			uexecuteAccess: uexecuteAccess.join(','),
			uadminAccess: uadminAccess.join(',')
		})

		this.tags.each(function(tag) {
			tags.push(tag.get('name'))
		})
		wikiParams.utag = tags.join(',')

		this.catalogs.each(function(catalog) {
			catalogs.push(catalog.get('name'))
		})
		wikiParams.ucatalog = catalogs.join(',')

		//Pull in the params grid, add the order and then store that off on the server
		var parameters = []
		Ext.each(this.params.collect().params, function(record) {
			parameters.push(record);
		})
		wikiParams.uwikiParameters = ' ' + Ext.encode(parameters)

		var url = commit ? this.API.wikiSaveAndCommit : this.API.wikiSave;
		var params = commitParams || {
			comment: '',
			postToSocial: false,
			reviewed: false
		};

		if (this.isSaveAs) {
			url = this.API.wikiSaveAs;
			Ext.apply(params, {
				toDocument: this.newNamespace + '.' + this.newName,
				choice: this.choice
			})
		}

		this.set('wait', true);
		this.ajax({
			url: url,
			jsonData: wikiParams,
			params: params,
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.clearDirtyFlag();
					clientVM.displaySuccess(this.localize('WikiSaved'))
					this.set('havePersistedWikiSinceLastView', true)
					this.set('id', response.data.id)
					this.set('ucontent', response.data.ucontent)
					this.set('uisRequestSubmission', response.data.uisRequestSubmission);
					this.set('originalData', response.data);
					this.loadData(response.data)
					if (this.version) {
						this.set('version', '');
						clientVM.removeURLHashVersion();
						this.reloadWikiDocument(true);
						return;
					}
					RS.common.grid.updateSysFields(this, response.data)
					if (response.data && response.data.accessRights) {
						this.processAccessRights(response.data.accessRights)
						this.set('accessRights', response.data.accessRights)
					}
					if (this.automation) this.automation.set('dataIsReady', true);
					if (Ext.isFunction(onSuccess)) {
						if (scope) {
							onSuccess.apply(scope);
						} else {
							onSuccess.apply(this);
						}
					}
					if (exitAfterSave === true || commit === true) this.viewTab();
				} else {
					clientVM.displayError(response.message);
					if (Ext.isFunction(onFailure)) {
						if (scope) {
							onFailure.apply(scope);
						} else {
							onFailure.apply(this);
						}
					}
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
				if (Ext.isFunction(onFailure)) {
					if (scope) {
						onFailure.apply(scope);
					} else {
						onFailure.apply(this);
					}
				}
			},
			callback: function() {
				this.set('wait', false);
				this.set('persisting', false);
			}
		})
	},
	//view
	displayDTViewer$: function() {
		//TODO: Hide when no decision tree model on the wiki
		return this.viewTabIsPressed
	},
	dtViewer: function() {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.DecisionTreeViewer',
			params: {
				name: this.name
			}
		})
	},
	lastViewedWiki: new Date(),
	wikiURL : '',
	when_name_change : {
		on : ['nameChanged','lastViewedWikiChanged'],
		action : function() {
			var newURL = this.getWikiURL();
			//Clean the old iframe whenever new iframe is load.
			this.cleanIframes();
			setTimeout(function(){
				this.set('wikiURL', newURL);
			}.bind(this),500)
		}
	},
	
	getWikiURL: function(){
		var params = {
			r: Ext.Date.format(this.lastViewedWiki, 'time')
		},
		windowParams = Ext.Object.fromQueryString(window.location.search || '');
		var key = null;
		Ext.Object.each(windowParams, function(k, v) {
			if (k.toLowerCase() == 'problemid' && v.toLowerCase() == 'new')
				key = k;
		}, this);
		if (key)
			delete windowParams[key];
		Ext.apply(params, windowParams);
		if (!params.PROBLEMID && clientVM.screens) {
			var p;
			if (p = clientVM.screens.getAt(clientVM.screens.getActiveIndex()).params) {
				if (p.problemId) {
					params.PROBLEMID = p.problemId;
				}
				if (p.sir) {
					params.sir = p.sir;
				}
			}
		}
		if (this.version) {
			Ext.apply(params, {
				rev: this.version
			});
		}
		if (params.Referer) {
			delete params.Referer;
		}
		if (params[clientVM.CSRFTOKEN_NAME]) {
			params[clientVM.CSRFTOKEN_NAME] = this.csrftoken.split('=')[1];
			return this.name ? Ext.String.format('<div class="iframe-container"><iframe class="rs_iframe" id="wiki_frame" src="/resolve/service/wiki/view?wiki={0}&{1}" marginheight="0" marginwidth="0" frameborder="0"></iframe></div>', this.name, Ext.Object.toQueryString(params)) : '&nbsp;';
		} else {
			return this.name ? Ext.String.format('<div class="iframe-container"><iframe class="rs_iframe" id="wiki_frame" src="/resolve/service/wiki/view?wiki={0}&{1}&{2}" marginheight="0" marginwidth="0" frameborder="0"></iframe></div>', this.name, Ext.Object.toQueryString(params), this.csrftoken) : '&nbsp;';
		}
	},
	attachments: function() {
		this.id.trim()?
			this.open({
				mtype : 'RS.wiki.pagebuilder.Attachments',
				name : this.name,
				id : this.id,
				isEditMode: true,
			}):
			this.message({
				title: this.localize('docNotExist'),
				msg: this.localize('uploadImageOnNotExistDoc'),
				buttons: Ext.MessageBox.OK
			})
	},
	attachmentsIsVisible$: function() {
		return (this.pageViewTabIsPressed || this.pageTabIsPressed);
	},
	duplicateDetected: function(files) {
		var names = [];
		Ext.each(files, function(file) {
			names.push(file.name.toLowerCase() + '(' + file.name + ')');
		});
		this.message({
			title: this.localize('duplicateWarn'),
			msg: this.localize(names.length > 1 ? 'duplicatesDetected' : 'duplicateDetected', names),
			buttons: Ext.MessageBox.OK
		});
	},
	printIsVisible$: function() {
		return this.pageViewTabIsPressed;
	},
	tableOfContents: function() {
		this.set('tableOfContentsIsPressed', !this.tableOfContentsIsPressed);
	},
	tableOfContentsIsVisible$: function() {
		return this.pageViewTabIsPressed;
	},
	tableOfContentsIcon$: function() {
		return this.tableOfContentsIsPressed ? 'rs-social-menu-button icon-check' : 'rs-social-menu-button'
	},
	viewAttachments: function() {
		this.open({
			mtype : 'RS.wiki.pagebuilder.Attachments',
			name : this.name,
			id : this.id,
			isEditMode: false
		})
	},
	pageInfo: function() {
		this.open({
			mtype: 'RS.wiki.PageInfo',
			name: this.rootVM.ufullname
		})
	},
	goTo: function() {},
	goToIsVisible$: function() {
		return this.viewTabIsPressed
	},
	listDocuments: function() {
		this.open({
			mtype: 'RS.wiki.ListDocuments'
		})
	},
	history: function() {
		this.open({
			mtype: 'RS.wiki.History'
		})
	},
	properties: function() {
		var propertiesModel = this.model({
			mtype: 'RS.wiki.pagebuilder.Properties',
			title: 'Properties',
			fullNameIsEditable: this.fullNameIsEditable,
			ufullname: this.ufullname,
			utitle: this.utitle,
			usummary: this.usummary,
			udisplayMode: this.udisplayMode,
			uisDefaultRole: this.uisDefaultRole,
			accessRights: this.accessRights,
			fullName: this.fullName,
			ucatalogId: this.ucatalogId
		});
		propertiesModel.saveAndSetStore('roles', this.roles);
		propertiesModel.saveAndSetStore('tags', this.tags);
		propertiesModel.saveAndSetStore('catalogs', this.catalogs);
		propertiesModel.saveAndSetStore('ucatalogStore', this.ucatalogStore);
		this.open(propertiesModel);
	},
	propertiesIsVisible$: function() {
		return this.pageTabIsPressed;
	},
	backBuilder: function() {
		//Determine if there were changes made in the builder
		var newContent = this.getPageContent();
		if (newContent != this.ucontent) {
			this.message({
				title: this.localize('exitBuilderWithoutSaveTitle'),
				msg: this.localize('exitBuilderWithoutSaveBody'),
				buttons: Ext.MessageBox.YESNOCANCEL,
				buttonText: {
					yes: this.localize('save'),
					no: this.localize('dontSave'),
					cancel: this.localize('cancel')
				},
				scope: this,
				fn: this.backWithoutSave
			})
		}
		else {
			//restore content from data model if we didn't save (meaning just canceled)
			if (!this.havePersistedWikiSinceLastView)
				this.set('ucontent', this.originalData.ucontent)
			//this.source()
			this.viewTab();
		}
	},
	backWithoutSave: function(btn) {
		if (btn === 'yes') {
			this.save(null, null, null, function() {
				this.viewTab();
			})
		} else if (btn === 'no') {
			if (!this.havePersistedWikiSinceLastView)
				this.set('ucontent', this.originalData.ucontent);
			this.viewTab();
		}
	},
	sourceEditWin: null,
	source: function() {
		this.set('sourceEditWin', this.open({
			mtype: 'RS.wiki.pagebuilder.Source',
			content: this.getPageContent(),
			title: 'Source'
		}));
	},
	updateContentFromSource : function(content){
		this.sectionsLayout.parseSection(content);
	},
	sourceIsVisible$: function() {
		return this.pageTabIsPressed;
	},
	showEditTabSeparators$: function() {
		return this.pageTabIsPressed;
	},
	viewRevisionList: function() {
		this.open({
			mtype: 'RS.wiki.pagebuilder.Revision',
			name : this.name,
			uname: this.uname,
			id : this.id,
			unamespace: this.unamespace,
			dumper: {
				dump: function(selections) {
					if (selections.length > 0) {
						var selection = selections[0];
						this.loadSelectedWikiVersion(selection.get('version'));
					}
				},
				scope: this
			}
		});
	},
	loadSelectedWikiVersion: function(version) {
		if (this.dirtyFlag) {
			this.message({
				title: this.localize('loadVersionWithoutSaveTitle', version),
				msg: this.localize('loadVersionWithoutSaveBody'),
				buttons: Ext.MessageBox.YESNOCANCEL,
				buttonText: {
					yes: this.localize('save'),
					no: this.localize('dontSave'),
					cancel: this.localize('cancel')
				},
				scope: this,
				fn: function(btn) {
					if (btn == "yes") {
						this.save(null, null, null, function() {
							this.doLoadSelectedWikiVersion(version);
						})
					} else if (btn == "no") {
						this.doLoadSelectedWikiVersion(version);
					}
				}
			})
		} else {
			this.doLoadSelectedWikiVersion(version);
		}
	},
	doLoadSelectedWikiVersion: function(version) {
		this.set('version', version);
		clientVM.updateURLHashVersion(this.version);
		this.reloadWikiDocument(true);
	},
	revisionsIsVisible$: function() {
		return !this.automationViewTabIsPressed && !this.decisionTreeViewTabIsPressed;
	},

	viewFileOptions: function() {},
	viewFileOptionsIsVisible$: function() {
		return this.pageViewTabIsPressed || this.decisionTreeViewTabIsPressed || this.automationViewTabIsPressed;
	},
	editFileOptions: function() {},
	editFileOptionsIsVisible$: function() {
		return this.pageTabIsPressed || this.decisionTreeTabIsPressed || this.automationTabIsPressed;
	},
	collaborationOptions: function() {},

	collaborationOptionsIsVisible$: function() {
		return !this.automationViewTabIsPressed && !this.decisionTreeViewTabIsPressed && this.viewTabIsPressed;
	},

	resolutionBuilderDecisionTree$: function() {
		return (this.automationTabIsPressed) || (this.decisionTreeTabIsPressed);
	},
	lock: function() {
		this.message({
			title: this.localize('lockTitle'),
			msg: this.localize('lockMessage', this.ufullname),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('lockAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyLockWiki
		})
	},
	lockIsVisible$: function() {
		return !this.saveIsVisible && !this.uisLocked && !this.autoViewOrDtreeViewIsPressed;
	},
	lockIsDisabled$: function() {
		return !this.saveIsVisible && !this.uisLocked && (this.accessRights ? hasPermission(this.accessRights.uadminAccess) : true);
	},
	reallyLockWiki: function(btn) {
		if (btn == 'yes') this.setLocked(true)
	},
	unlock: function() {
		this.message({
			title: this.localize('unlockTitle'),
			msg: this.localize('unlockMessage', this.ufullname),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('unlockAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyUnlockWiki
		})
	},
	unlockIsVisible$: function() {
		return this.uisLocked && !this.autoViewOrDtreeViewIsPressed;
	},
	unlockIsDisabled$: function() {
		return this.uisLocked && (this.accessRights ? hasPermission(this.accessRights.uadminAccess) : true);
	},
	reallyUnlockWiki: function(btn) {
		if (btn == 'yes') this.setLocked(false)
	},
	setLocked: function(value) {
		this.set('uisLocked', value)
		this.set('uisDocumentStreamLocked', value)
		this.ajax({
			url: this.API.wikiSetLock,
			params: {
				id: this.id,
				lock: value
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.localize(value ? 'documentLocked' : 'documentUnlocked'))
					this.set('ulockedBy', user.name)
				} else {
					this.set('uisLocked', !value)
					this.set('uisDocumentStreamLocked', !value)
					clientVM.displayError(response.message)
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	setSoftLocked: function(value) {
		if (!this.id)
			return;
		this.setRaw('softLock', value, true);
		this.ajax({
			url: this.API.wikiSetSoftLock,
			params: {
				docName: this.ufullname,
				lock: value
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					if (value) {
						if (response.data)
							this.setRaw('lockedByUsername', response.data, true);
						else
							this.setRaw('lockedByUsername', user.name, true);
					}
				} else {
					this.setRaw('softLock', !this.softLock, true);
					clientVM.displayError(response.message)
				}
			},
			failure: function(r) {
				this.setRaw('softLock', !this.softLock, true);
				clientVM.displayFailure(r);
			}
		})
	},
	rename: function() {
		this.open({
			mtype: 'RS.wiki.MoveWiki',
			name: this.uname,
			namespace: this.unamespace,
			id: this.id
		})
	},
	renameIsVisible$: function() {
		return this.accessRights ? hasPermission(this.accessRights.uadminAccess) : true
	},
	move: function() {
		this.open({
			mtype: 'RS.wiki.MoveWiki',
			name: this.uname,
			namespace: this.unamespace,
			id: this.id,
			isMove: true
		})
	},
	moveIsVisible$: function() {
		return !this.saveIsVisible;
	},
	moveIsDisabled$: function() {
		return !this.saveIsVisible && (this.accessRights ? hasPermission(this.accessRights.uadminAccess) : true);
	},
	copy: function() {
		this.open({
			mtype: 'RS.wiki.CopyWiki',
			name: this.uname,
			namespace: this.unamespace,
			id: this.id
		})
	},
	copyIsVisible$: function() {
		return !this.saveIsVisible;
	},
	copyIsDisabled$: function() {
		return !this.saveIsVisible && (this.accessRights ? hasPermission(this.accessRights.uadminAccess) : true);
	},
	replaceContent: function() {
		this.open({
			mtype: 'RS.wiki.CopyWiki',
			isReplace: true,
			name: this.uname,
			namespace: this.unamespace,
			id: this.id,
			update: true,
			overwriteIsVisible: false,
			skipIsVisible: false,
			updateIsVisible: false
		})
	},
	replaceContentIsVisible$: function() {
		return this.accessRights ? hasPermission(this.accessRights.uadminAccess) : true
	},
	deleteWiki: function() {
		//Provide dialog for delete, delete & forward, cancel for the user to be able to set up a forward for the wiki document
		//delete and forward will need a separate dialog to provide the forwarding address for the document
		var msg = this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize('deleteMessage', this.ufullname),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				// no: this.localize('deleteAndForward'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteWiki
		})

		Ext.create('Ext.tip.ToolTip', {
			html: this.localize('deleteAndForwardTooltip'),
			target: msg.msgButtons[2].btnEl
		})
	},
	deleteWikiIsVisible$: function() {
		return !this.saveIsVisible && !this.uisDeleted && !this.undeleteWikiIsVisible && !this.autoViewOrDtreeViewIsPressed;
	},
	deleteWikiIsDisabled$: function() {
		return !this.saveIsVisible && !this.uisDeleted && (this.accessRights ? hasPermission(this.accessRights.uadminAccess) : true);
	},
	reallyDeleteWiki: function(btn) {
		if (btn == 'yes') this.setDeleted(true)
	},
	undeleteWiki: function() {
		this.message({
			title: this.localize('unDeleteTitle'),
			msg: this.localize('unDeleteMessage', this.ufullname),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('unDeleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyUndeleteWiki
		})
	},
	undeleteWikiIsVisible$: function() {
		return !this.saveIsVisible && this.uisDeleted && !this.deleteWikiIsVisible && !this.autoViewOrDtreeViewIsPressed;
	},
	undeleteWikiIsDisabled$: function() {
		return !this.saveIsVisible && this.uisDeleted && (this.accessRights ? hasPermission(this.accessRights.uadminAccess) : true);
	},
	reallyUndeleteWiki: function(btn) {
		if (btn == 'yes') this.setDeleted(false)
	},
	setDeleted: function(value) {
		this.set('uisDeleted', value)
		this.ajax({
			url: this.API.wikiAdminSetDeleted,
			params: {
				ids: [this.id],
				on: value
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(value ? this.localize('wikiDeleted') : this.localize('wikiUndeleted'))
				} else {
					this.set('uisDeleted', !value)
					clientVM.displayError(response.message)
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	purge: function() {
		this.message({
			title: this.localize('purgeTitle'),
			msg: this.localize('purgeMessage', this.ufullname),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('purgeAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyPurgeWiki
		})
	},
	purgeIsVisible$: function() {
		return !this.saveIsVisible;
	},
	purgeIsDisabled$: function() {
		return !this.saveIsVisible && (this.accessRights ? hasPermission(this.accessRights.uadminAccess) : true);
	},
	reallyPurgeWiki: function(btn) {
		if (btn == 'yes')
			this.ajax({
				url: this.API.wikiAdminSetPurge,
				params: {
					ids: [this.id],
					all: false
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						clientVM.displaySuccess(this.localize('wikiPurged'))
						this.resetForm()
						this.set('id', '')
						this.loadWikiDocument()
					} else {
						clientVM.displayError(response.message)
					}
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
	},
	follow: function() {
		this.setFollow(true)
	},
	followIsVisible$: function() {
		return !this.uisCurrentUserFollowing
	},
	unfollow: function() {
		this.setFollow(false)
	},
	unfollowIsVisible$: function() {
		return this.uisCurrentUserFollowing
	},
	setFollow: function(value) {
		this.set('uisCurrentUserFollowing', value)
		this.ajax({
			url: this.API.socialSetFollowStream,
			params: {
				ids: [this.id],
				follow: value
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.localize(value ? 'documentFollowed' : 'documentUnfollowed'))
				} else {
					this.set('uisCurrentUserFollowing', !value)
					clientVM.displayError(response.message)
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	lockStream: function() {
		this.message({
			title: this.localize('lockStreamTitle'),
			msg: this.localize('lockStreamMessage', this.ufullname),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('lockStreamAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyLockStream
		})
	},
	lockStreamIsVisible$: function() {
		return !this.uisDocumentStreamLocked
	},
	reallyLockStream: function(btn) {
		if (btn == 'yes') this.setLockedStreamServer(true)
	},
	unlockStream: function() {
		this.message({
			title: this.localize('unlockStreamTitle'),
			msg: this.localize('unlockStreamMessage', this.ufullname),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('unlockStreamAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyUnlockStream
		})
	},
	unlockStreamIsVisible$: function() {
		return this.uisDocumentStreamLocked
	},
	reallyUnlockStream: function(btn) {
		if (btn == 'yes') this.setLockedStreamServer(false)
	},
	setLockedStreamServer: function(value) {
		this.set('uisDocumentStreamLocked', value)
		this.ajax({
			url: this.API.socialSetLockedStream,
			params: {
				id: this.id,
				lock: value
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.localize(value ? 'documentStreamLocked' : 'documentStreamUnlocked'))
				} else {
					this.set('uisDocumentStreamLocked', !value)
					clientVM.displayError(response.message)
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	enableCollaboration: function() {
		this.set('socialIsPressed', true);
	},
	enableCollaborationIsVisible$: function() {
		return !this.socialIsPressed;
	},
	disableCollaboration: function() {
		this.set('socialIsPressed', false);
	},
	disableCollaborationIsVisible$: function() {
		return this.socialIsPressed;
	},
	showCollaboration$: function() {
		return this.socialIsPressed && this.pageViewTabIsPressed;
	},
	viewFeedbacks: function() {
		clientVM.handleNavigation({
			modelName: 'RS.social.Main',
			target: '_blank',
			params: {
				streamId: this.rootVM.id,
				streamParent: this.rootVM.ufullname
			}
		})
	},
	viewFollowers: function() {
		this.followersWin = this.open({
			mtype: 'RS.social.Followers',
			addFollowers: true
		})
	},
	xml: function() {
		this.set('parametersIsPressed', false);
		this.set('xmlIsPressed', !this.xmlIsPressed);
	},
	xmlIsVisible$: function() {
		return (this.automationTabIsPressed && !this.parametersIsPressed) || this.decisionTreeTabIsPressed;
	},
	viewsettingsIsVisible$: function() {
		return (this.decisionTreeTabIsPressed);
	},
	parametersIsVisible$: function() {
		return (this.automationTabIsPressed || this.automationViewTabIsPressed) && !this.parametersIsPressed;
	},
	versionsMenuseparator$: function() {
		return this.decisionTreeTabIsPressed || this.propertiesIsVisible;
	},
	resolutionBuilder: function() {
		if (this.dirtyFlag) {
			this.message({
				title: this.localize('confirmNavigationTitle'),
				msg: this.localize('confirmNavigationToBuilderMsg'),
				buttons: Ext.MessageBox.YESNO,
				buttonText: {
					yes: this.localize('confirm'),
					no: this.localize('cancel')
				},
				scope: this,
				fn: function(btn) {
					if (btn == 'yes') {
						this.doResolutionBuilder();
					}
				}
			})
		}
		else {
			this.doResolutionBuilder();
		}
	},
	doResolutionBuilder: function() {
		var splits = this.name.split('.');
		var namespace = splits[0];
		var name = splits[1];
		var params = {
			wikiId: this.id
		};
		if (this.uresolutionBuilderId)
			params.id = this.uresolutionBuilderId;
		else {
			params.name = name;
			params.namespace = namespace;
		}
		clientVM.handleNavigation({
			modelName: 'RS.wiki.resolutionbuilder.AutomationMeta',
			params: params
		});
	},
	resolutionBuilderIsVisible$: function() {
		return this.automationTabIsPressed && !this.parametersIsPressed && this.automationMode != this.automationModeMap['DECISIONTREEMODEL'];
	},
	resolutionBuilderIsEnabled$: function() {
		return !this.persisting && !this.wait && (!this.uisLocked || (this.uisLocked && this.ulockedBy == user.name));
	},
	automationModeSetMainModel: function() {
		this.set('automationMode', this.automationModeMap['MAINMODEL']);
		window.sessionStorage.setItem('wikiAutomationMode', this.automationMode);
	},
	automationModeSetAbortModel: function() {
		this.set('automationMode', this.automationModeMap['ABORTMODEL']);
		window.sessionStorage.setItem('wikiAutomationMode', this.automationMode);
	},
	mainModel: function() {
		if (this.modelDirty)
			this.confirmNavWithoutSave(this.mainModel);
		else {
			this.automationModeSetMainModel();
		}

		this.automationViewTabIsPressed? this.automationView.mainModelTab(): this.automation.mainModelTab();
	},
	mainModelIsPressed$: function() {
		return this.automationMode == this.automationModeMap['MAINMODEL'];
	},
	mainModelIsVisible$: function() {
		return (this.automationTabIsPressed || this.automationViewTabIsPressed) && !this.parametersIsPressed;
	},
	abortModel: function() {
		if (this.modelDirty)
			this.confirmNavWithoutSave(this.abortModel);
		else {
			this.automationModeSetAbortModel();
		}

		this.automationViewTabIsPressed? this.automationView.abortModelTab() : this.automation.abortModelTab();
	},
	abortModelIsPressed$: function() {
		return this.automationMode == this.automationModeMap['ABORTMODEL']
	},
	abortModelIsVisible$: function() {
		return (this.automationTabIsPressed || this.automationViewTabIsPressed) && !this.parametersIsPressed;
	},
	showExecution$: function() {
		return (this.pageViewTabIsPressed || this.automationTabIsPressed || this.automationViewTabIsPressed);
	},
	isExecutable$: function() {
		if (this.dirtyFlag) {
			return false;
		}
		return (this.pageViewTabIsPressed || this.automationTabIsPressed || this.automationViewTabIsPressed) && (this.accessRights ? hasPermission(this.accessRights.uexecuteAccess) : false);
	},
	executeWithDefault: function(button) {
		if (this.task) this.task.cancel()
		if (!this.executionModel) {
			var me = this;
			var model = this.model({
				mtype: 'RS.actiontaskbuilder.Execute',
				target: button.getEl().id,
				fromWiki: true,
				inputsLoader: function(paramStore, origParamStore, mockStore) {
					//Load up the wiki params into the grid and the mock values for the wiki
					me.params.firstColumnStore.each(function(param) {
						param.data.value = param.get('sourceName');
						param.data.utype = 'PARAM';
						paramStore.add(param);
						origParamStore.add(param);
					})
				},
				executeDTO: {
					wiki: this.name
				}
			});
			this.set('executionModel', model);
		}
		this.executionModel.execute();
	},
	execution: function(button) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.openExecutionWindow, this, [button])
		this.task.delay(500)
	},
	openExecutionWindow: function(button) {
		//if (this.mockList != null) {
		//	this.doOpenExecutionWindow(button);
		//}
		//else {
			this.getMockList(this.id, function(mockList){
				this.set('mockList', mockList);
				this.doOpenExecutionWindow(button);
			}.bind(this));
		//}
	},
	doOpenExecutionWindow: function(button) {
		var me = this;
		if (this.executionWin) {
			this.executionWin.doClose()
		} else {
			this.executionWin = this.open({
				mtype: 'RS.actiontaskbuilder.Execute',
				target: button.getEl().id,
				fromWiki: true,
				inputsLoader: function(paramStore, origParamStore, mockStore) {
					//Load up the wiki params into the grid and the mock values for the wiki
					me.params.firstColumnStore.each(function(param) {
						if(!param.data.hidden){
							param.data.value = param.get('sourceName');
							param.data.utype = 'PARAM';
							paramStore.add(param);
							origParamStore.add(param);
						}
					})

					var mockNamesArray = [];
					if (me.mockList) {
						for (var i = 0; i < me.mockList.length; i++) {
							mockNamesArray.push({
								uname: me.mockList[i]
							})
						}
					}
					mockStore.loadData(mockNamesArray);
				},
				executeDTO: {
					wiki: this.name
				}
			})
			this.executionWin.on('closed', function() {
				this.executionWin = null
			}, this)
		}
	},
	executionIsVisible$: function() {
		return true //this.viewTabIsPressed || this.editTabIsPressed
	},
	feedbackTooltip$: function() {
		var date = this.ulastReviewedOn ? Ext.Date.format(new Date(this.ulastReviewedOn), 'Y-m-d G:i:s') : this.localize('never'),
			reviewer = this.ulastReviewedBy || this.localize('noone');
		return this.localize('lastReviewedText', [date, reviewer])
	},
	feedback: function(button) {
		if (this.feedbackWin) {
			this.feedbackWin.doClose()
			this.feedbackWin = null
		} else {
			this.feedbackWin = this.open({
				mtype: 'RS.wiki.Feedback',
				target: button.getEl().id,
				markForReview: this.uisRequestSubmission,
				id: this.id
			})
			this.feedbackWin.on('closed', function() {
				this.feedbackWin = null
			}, this)
		}
	},
	/*
	feedbackCls$: function() {
		return this.uisRequestSubmission ? 'icon-flag rs-wiki-flagged' : 'icon-flag-alt'
	},
	*/
	preview: function() {
	},
	ratings: function() {
		var ratings = this.open({
			mtype: 'RS.wiki.macros.FeedbackResults',
			modal: true,
			docFullName: this.name
		});
	},
	socialPressedCls$: function() {
		return this.socialIsPressed ? 'rs-social-button-pressed' : '';
	},
	showSocial$: function() {
		return !isPublicUser() && this.viewTabIsPressed && !this.automationViewTabIsPressed && !this.decisionTreeViewTabIsPressed;
	},
	/*
	showFollow$: function() {
		return !isPublicUser() && this.viewTabIsPressed;
	},
	*/
	showSysInfo$: function() {
		return !isPublicUser() && this.viewTabIsPressed;
	},
	socialLinkClicked: function(vm) {
		this.set('socialIsPressed', true)
		this.socialDetail.switchToPostMessage()

		var newTo = [],
			currentTo = this.socialDetail.toHidden,
			split = vm.to.split('&&');

		Ext.Array.forEach(currentTo, function(current) {
			newTo.push(current)
		})

		Ext.Array.forEach(split, function(add) {
			var addParams = Ext.Object.fromQueryString(add)
			newTo.push(this.socialDetail.streamToStore.createModel({
				id: addParams.id,
				name: addParams.name,
				type: addParams.type
			}))
		}, this)

		this.socialDetail.set('toHidden', newTo)
	},
	modelSaved: function(resp) {
		var respData = RS.common.parsePayload(resp);
		this.set('modelDirty', false)
		this.set('havePersistedWikiSinceLastView', true)
		this.loadWikiDocument()
		if (!respData.success)
			clientVM.displayError(this.localize('automationSavedFailure', respData.message));
		else
			clientVM.displaySuccess(this.localize('automationSaved'))
	},
	modelChanged: function() {
		this.set('modelDirty', true)
	},
	confirmNavWithoutSave: function(continueFunction) {
		this.message({
			title: this.localize('confirmNavigationTitle'),
			msg: this.localize('confirmNavigationMsg'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('continueNavigation'),
				no: this.localize('cancel')
			},
			fn: function(btn) {
				if (btn == 'yes') {
					this.set('modelDirty', false)
					continueFunction.apply(this)
				}
			},
			scope: this
		})
	},
	addLayoutSection : function(){
		this.sectionsLayout.addNewSection();
	},
	addLayoutSectionIsVisible$: function() {
		return this.pageTabIsPressed;
	},
	reloadIsVisible$: function() {
		return this.activeTab != this.activeTabMap['VIEW'];
	},
	reloadIsEnabled$ : function(){
		return !this.wait;
	},
	getPageContent: function() {
		return this.sectionsLayout.getAllSectionContent();
	},
	separatorAfterParamsIsVisible$: function() {
		return this.parametersIsVisible;
	},
	rejectStoreChanges: function(stores) {
		var store = null;
		for(var i=0; i<stores.length; i++) {
			store = stores[i];
			store.removeAll();
			store.add(store['__saved']);
		}
	},

	//Helper functions for Connections
	getParamStore : function(){
		//REFRACTOR WHEN AB IS KILLED !!!!
		return this.params.firstColumnStore;
	},

	mockList: null,
	getMockList : function(id, onSuccess, onFailure){
		var params = {
			wikiSysId : id,
		}
		this.ajax({
			url : this.API.getMockList,
			params : params,
			success : function(resp){
				var respData = RS.common.parsePayload(resp);
				var mockList = [];
				if (respData.success) {
					if (respData.records.length) {
						var mocks = respData.records;
						for (var i=0; i< mocks.length; i++) {
							mockList.push(mocks[i]);
						}
						mockList.sort();
					}
					if (typeof onSuccess === 'function') {
						onSuccess(mockList);
					}
				}
				else {
					if (typeof onFailure === 'function') {
						onFailure();
					}
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
				if (typeof onFailure === 'function') {
					onFailure();
				}
			}
		});
	},

	//For DT
	viewsettings: function() {
		var ratings = this.open({
			mtype: 'RS.decisiontree.ViewSettings',
			defaultTheme : this.dtDefaultTheme
		});
	},
	dtLayout : null,
	dtVariables : [],
	dtVariableMap : {},
	parseDTVariables : function(){
		this.dtVariableMap = {};
		var variableList = [];
		var domParser = new DOMParser();
		var doc = domParser.parseFromString(this.udecisionTree, 'text/xml');
		var expressions = doc.getElementsByTagName('expressions');
		for(var i = 0; i < expressions.length; i++){
			var expressionNode = expressions[i];
			var expressionData = expressionNode ? expressionNode.textContent : null;
			var parentNode = expressionNode.parentNode;
			if(expressionData){
				var data = Ext.decode(expressionData);
				for(var j = 0; j < data.length; j++){
					var expression = data[j];
					var variableName = expression.variableName;
					var taskName = expression.taskName;
					if(taskName){
						var parentNodeId = parentNode.id;
						if(!this.dtVariableMap[variableName])
							this.dtVariableMap[variableName] = [];
						this.dtVariableMap[variableName].push(parentNodeId);
						variableList.push({
							variableName : expression.variableName,
							taskName : expression.taskName,
							outputName : expression.outputName,
						});
					}
				}
			}
		}
		this.set('dtVariables', variableList);
		this.buildDTOption();
	},
	removeDTVariables : function(variableName, cellID){
		var idList = this.dtVariableMap[variableName];
		var idIndex = idList.indexOf(cellID);
		if(idIndex != -1)
			idList.splice(idIndex, 1);
		//Remove from dtVariables when there is no reference to this key anymore.
		if(idList.length == 0){
			for(var i = 0; i < this.dtVariables.length; i++){
				var variable = this.dtVariables[i];
				if(variable.variableName == variableName)
					break;
			}
			var variableList = this.dtVariables.slice() || [];
			variableList.splice(i,1);
			this.set('dtVariables', variableList);
			this.buildDTOption();
		}
	},
	updateDTVariablesOnEdit : function(newMap, oldMap, cellID){
		//Treat update on edit as adding new entry, then remove the old entry.
		var updated = false;
		if(newMap.variableName == oldMap.variableName){
			var idList = this.dtVariableMap[oldMap.variableName] || [];
			//Variable that does not have output assigned to is not included in dtVariables but still count as used key.
			var keyUsedCounter = oldMap.taskName ? idList.length : idList.length + 1;
			if(newMap.taskName != oldMap.taskName || newMap.outputName != oldMap.outputName)
				updated = keyUsedCounter <= 1 ? this.updateDTVariables(newMap, cellID, true) : false;
			else
				updated = this.updateDTVariables(newMap, cellID);
		}
		else
			updated = this.updateDTVariables(newMap, cellID);
		if(updated && oldMap.taskName)
			this.removeDTVariables(oldMap.variableName, cellID);
		return updated;
	},
	updateDTVariables : function(map, cellID, relocated){
		if(!map.taskName)//Reused key or variable that has no output assigned to.
			return true;

		var contained = false;
		var duplicated = false;
		var variableList = this.dtVariables.slice() || [];

		for(var i = 0; i < variableList.length; i++){
			var dataObj = variableList[i] ;
			if(dataObj.variableName == map.variableName){
				contained = true;
				if(dataObj.taskName == map.taskName || dataObj.outputName == map.outputName){
					duplicated = true;
				}
				break;
			}
		}

		if(!contained || relocated){
			variableList.push(map);
			this.set('dtVariables', variableList);
			this.dtVariableMap[map.variableName] = [];
			this.dtVariableMap[map.variableName].push(cellID);
			this.buildDTOption();
			return true;
		} else if(contained && duplicated){
			this.dtVariableMap[map.variableName].push(cellID);
			return true;
		}
		else //Key is already assigned to different output.
			return false;
	},
	dtDefaultTheme : {
		questionFontColor: '000000',
		questionFont: 'Verdana',
		questionFontSize: '13',
		answerFontColor: '000000',
		answerFont: 'Verdana',
		answerFontSize: '10',
		recommendationFontColor: '41a510',
		recommendationFont: 'Verdana',
		recommendationFontSize: '10',
		confirmationFontColor: 'FFFFFF',
		confirmationBGColor: '41a510',
		confirmationFont: 'Verdana',
		confirmationFontSize: '10',
		navigationFontColor: 'FFFFFF',
		navigationBGColor: '41a510',
		navigationFont: 'Verdana',
		navigationFontSize: '13',
		autoConfirm : false,
		buttonText: 'Next',
		questionOnTop : true,
		navOnLeft: true,
		verticalAnswerLayout: false,
		isAutoAnswerDisabled: false,
	},
	updateDTTheme : function(data){
		this.set('dtLayout',data);
		this.buildDTOption();
	},
	when_udtoptions_changed : {
		on : ['udtoptionsChanged'],
		action : function(){
			this.parseDTOption();
		}
	},

	when_dirtyflag_changed: {
		on: ['dirtyFlagChanged'],
		action: function() {
			var me = this;
			// Make an async (non-block) call using setTimeout
			setTimeout(function() {
				// GDV - Currently only sir is interested in dirtyflag being changed to false.
				// originalContent is only meant for sir and has no meaning for wiki so just set it to itself (for wiki)
				me.originalContent = (me.rootVM.sir && !me.dirtyFlag)? me.getPageContent(): me.originalContent;
			}, 0);
		}
	},

	parseDTOption : function(){
		if(this.udtoptions){
			var options = Ext.decode(this.udtoptions);
			this.set('dtLayout', Ext.applyIf( options.dtLayout,this.dtDefaultTheme));
			this.set('dtVariables', options.dtVariables );
		}
	},
	buildDTOption : function(){
		var obj = {
			dtLayout : this.dtLayout || this.dtDefaultTheme,
			dtVariables : this.dtVariables || []
		}
		this.set('udtoptions', JSON.stringify(obj));
	},
	checkPageDirtyFlag: function() {
		var newContent = this.getPageContent();
		if (newContent != this.originalData.ucontent) {
			this.notifyDirtyFlag('Pagebuilder');
		} else {
			this.clearComponentDirtyFlag('Pagebuilder');
		}
	},
	isStoreDirty: function(theStore) {
		var isDirty = false;
		theStore.each(function(item){
			if (item.dirty) {
				var modifiedKeys = Object.keys(item.modified);
				for (var i in modifiedKeys) {
					var key = modifiedKeys[i];
					if (!!(item.data[key]) != !!(item.raw[key])) {
						isDirty = true;
						break;
					}
				}
			} else if(item.phantom){
				isDirty = true;
			}
		});
		if (!isDirty){
			isDirty = (theStore.removed.length > 0);
		}
		return isDirty;
	}
});
