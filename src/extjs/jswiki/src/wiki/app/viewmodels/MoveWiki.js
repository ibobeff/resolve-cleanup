glu.defModel('RS.wiki.MoveWiki', {

	isMove: false,
	wait: false,

	windowTitle$: function() {
		return this.isMove ? this.localize('move') : this.localize('rename')
	},
	id: '',
	overwrite: false,
	validationRegex : /^[0-9a-zA-Z_-]+$/,

	name: '',
	nameIsValid$: function() {
		return /^[\w|\-]+[\w|\-| ]*$/.test(this.name) ? true : this.localize('invalidName');
	},
	namespace: '',
	namespaceIsValid$: function(){
		return this.validationRegex.test(this.namespace) ? true : this.localize('invalidNamespace');
	},

	rename: function() {
		this.set('wait', true)
		this.ajax({
			url: '/resolve/service/wikiadmin/moveRenameCopy',
			params: {
				ids: [this.id],
				action: 'rename',
				namespace: this.namespace,
				filename: this.name,
				choice: this.overwrite ? 'overwrite' : 'skip'
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.isMove ? this.localize('wikiMoved') : this.localize('wikiRenamed'))
					clientVM.handleNavigation({
						modelName: 'RS.wiki.Main',
						params: {
							name: this.namespace + '.' + this.name
						}
					})
					this.doClose()
				} else
					clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},
	renameIsEnabled$: function(){
		return this.namespace && this.name && this.namespaceIsValid == true && this.nameIsValid == true;
	},
	/* 
	renameIsVisible$: function() {
		return !this.isMove
	},
	move: function() {
		this.rename()
	},
	moveIsVisible$: function() {
		return this.isMove
	},
	*/
	cancel: function() {
		this.doClose()
	}
})