glu.defModel('RS.wiki.Lookup', {

	mock: false,

	id: '',
	state: '',
	uregex: '',
	uwiki: '',
	udescription: '',
	uorder: '',

	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrganizationName: '',

	defaultData: {
		id: '',
		uregex: '',
		uwiki: '',
		udescription: '',
		uorder: '',

		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrganizationName: ''
	},

	title: '',
	fields: ['uregex', 'uwiki', 'udescription', 'uorder'].concat(RS.common.grid.getSysFields()),
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	activate: function() {
		clientVM.setWindowTitle(this.localize('windowTitle'))
	},

	init: function() {
		if (this.mock) this.backend = RS.wiki.createMockBackend(true);
		this.set('title', this.localize('WikiLookups'));
		if (this.id != '') {
			this.loadLookup();
			return;
		}
	},

	loadLookup: function() {
		this.ajax({
			url: '/resolve/service/wikilookup/get',
			params: {
				id: this.id
			},
			success: function(resp) {
				this.handleLoadSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	handleLoadSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('GetLookupErrMsg'), respData.message);
			return;
		}
		var data = respData.data;
		this.setData(data);
	},

	save: function() {
		this.saveLookup();
	},
	saveIsEnabled$: function() {
		return !this.state != 'wait' && this.isValid & this.isDirty;
	},

	saveLookup: function() {
		var data = {
			id: this.id,
			URegex: this.uregex,
			UDescription: this.udescription,
			UWiki: this.uwiki,
			UOrder: this.uorder
		};
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/wikilookup/save',
			jsonData: data,
			scope: this,
			success: function(resp) {
				this.handleSaveSucResp(resp);
				this.doClose();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	handleSaveSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('SaveErrMsg') + '[' + respData.message + ']');
			return false;
		}
		this.setData(respData.data);
		this.parentVM.loadLookups();
		clientVM.displaySuccess(this.localize('SaveSucMsg'));
		return true;
	},

	resetForm: function() {
		this.loadData(this.defaultData);
	},

	setData: function(data) {
		this.loadData(data);
	},

	cancel: function() {
		this.doClose();
	},

	openSearch: function() {
		this.open({
			mtype: 'RS.common.WikiDocSearch',
			dumper: {
				dump: function(record) {
					if (!record)
						return null;
					var ufullname = record.get('ufullname');
					this.set('uwiki', ufullname);
					return true;
				},
				scope: this
			},
			single: true
		}, 'Single');
	},

	uregexIsValid$: function() {
		return this.uregex != null && this.uregex != '' ? true : this.localize('invalidRegex');
	},

	uorderIsValid$: function() {
		var rexp = /^\d*$/;
		return this.uorder != null && this.uorder !== '' &&
			rexp.test(this.uorder) ? true : this.localize('invalidOrder');
	},

	uwikiIsValid$: function() {
		var rexp1 = /^[a-zA-Z0-9_-]+[a-zA-Z0-9 _-]*\.[a-zA-Z0-9 _-]*[a-zA-Z0-9_-]+$/;
		return this.uwiki != null && rexp1.test(this.uwiki) ? true : this.localize('invalidWikiNameFormat');
	},

	saveLookupIsEnabled$: function() {
		return this.state != 'wait' && this.isValid && this.isDirty;
	}
});