glu.defModel('RS.wiki.RevisionView', {
	title: '',
	id: '',
	namespace: '',
	name: '',
	revision: 0,
	activeItem: 0,
	content: '',
	mainModel: '',
	abortModel: '',
	src: '',
	fields: ['content', 'mainModel', 'abortModel'],
	init: function() {
		this.set('title', this.localize('title', {
			documentName: this.namespace + '.' + this.name,
			revision: this.revision
		}));
		var src = Ext.String.format('/resolve/service/wiki/view?wiki={0}.{1}&rev={2}&{3}={4}', this.namespace, this.name, this.revision, clientVM.CSRFTOKEN_NAME, clientVM.getPageToken('service/wiki/view'));
		this.set('src', src);
		this.ajax({
			url: '/resolve/service/wiki/revision/view',
			params: {
				id: this.id,
				docFullname: '',
				revision: this.revision
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(respData.message);
					return;
				}
				this.loadData(respData.data);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},
	displayTab: function() {
		this.set('activeItem', 0);
	},
	displayTabIsPressed$: function() {
		return this.activeItem == 0;
	},
	sourceTab: function() {
		this.set('activeItem', 1);
	},
	sourceTabIsPressed$: function() {
		return this.activeItem == 1;
	},
	mainTab: function() {
		this.set('activeItem', 2);
	},
	mainTabIsPressed$: function() {
		return this.activeItem == 2;
	},
	abortTab: function() {
		this.set('activeItem', 3);
	},
	abortTabIsPressed$: function() {
		return this.activeItem == 3;
	},
	close: function() {
		this.doClose();
	}
});