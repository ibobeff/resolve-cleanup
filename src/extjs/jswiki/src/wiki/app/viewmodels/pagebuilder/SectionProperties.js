glu.defModel('RS.wiki.pagebuilder.SectionProperties', {
	sectionTitle : '',
	sectionHeight : -1,
	hidden: false, 
	active: true,
	col1Width : '0',
	col2Width : '0',
	col3Width : '0',
	col4Width : '0',
	col5Width : '0',
	col6Width : '0',
	col7Width : '0',
	isWidthsValid: true,
	col1Alignment: 'top',
	col2Alignment: 'top',
	col3Alignment: 'top',
	col4Alignment: 'top',
	col5Alignment: 'top',
	col6Alignment: 'top',
	col7Alignment: 'top',
	alignments: {
		mtype: 'store',
		fields: ['value','name'],
		proxy: {
			type: 'memory'
		},
		data : [{
			name : 'Top',
			value : 'top'
		},{
			name : 'Middle',
			value : 'middle'
		},{
			name : 'Bottom',
			value : 'bottom'
		}]
	},
	properties : null,
	numberOfColumns : null,
	init: function() {
		this.populateProperties();
	},
	validWidthsWarningIsVisible$: function() {
		return this.isWidthsValid
	},
	validWidths$: function() {
		var col1 = parseInt(this.col1Width);
		var col2 = parseInt(this.col2Width);
		var col3 = parseInt(this.col3Width);
		var col4 = parseInt(this.col4Width);
		var col5 = parseInt(this.col5Width);
		var col6 = parseInt(this.col6Width);
		var col7 = parseInt(this.col7Width);

		if (100 >= (col1 + col2 + col3 + col4 + col5 + col6 + col7)) {
			this.set('isWidthsValid', true);
		} else {
			this.set('isWidthsValid', false);
		}
	},
	col2WidthIsVisible$: function() {
		return this.numberOfColumns > 1;
	},
	col3WidthIsVisible$: function() {
		return this.numberOfColumns > 2;
	},
	col4WidthIsVisible$: function() {
		return this.numberOfColumns > 3;
	},
	col5WidthIsVisible$: function() {
		return this.numberOfColumns > 4;
	},
	col6WidthIsVisible$: function() {
		return this.numberOfColumns > 5;
	},
	col7WidthIsVisible$: function() {
		return this.numberOfColumns > 6;
	},
	col2AlignmentIsVisible$: function() {
		return this.numberOfColumns > 1;
	},
	col3AlignmentIsVisible$: function() {
		return this.numberOfColumns > 2;
	},
	col4AlignmentIsVisible$: function() {
		return this.numberOfColumns > 3;
	},
	col5AlignmentIsVisible$: function() {
		return this.numberOfColumns > 4
	},
	col6AlignmentIsVisible$: function() {
		return this.numberOfColumns > 5;
	},
	col7AlignmentIsVisible$: function() {
		return this.numberOfColumns > 6;
	},
	populateProperties : function(){
		var me = this;
		Ext.Object.each(this.properties, function(propertyName,propertyValue){
			if(typeof propertyValue !== 'object'){
				me.set(propertyName, propertyValue);
			}
			else{
				Ext.Object.each(propertyValue, function(col, value){
					me.set(col + propertyName, value);
				})
			}
		})
	},
	gatherProperties : function(){
		for(var i = 0; i < this.numberOfColumns; i++){
			var colNum = i + 1;
			this.properties['Width']['col' + colNum] = this['col' + colNum + 'Width'];
			this.properties['Alignment']['col' + colNum] = this['col' + colNum + 'Alignment'];
		}
		this.properties['hidden'] = this.hidden;
		this.properties['active'] = this.active;
		this.properties['sectionHeight'] = this.sectionHeight;
		this.properties['sectionTitle'] = this.sectionTitle;
		this.parentVM.updateProperties(this.properties);
	},
	update: function() {
		this.gatherProperties();
		this.doClose();
	},

	updateIsEnabled$: function() {
		return this.isWidthsValid
	},

	calculateWidth$: function() {
		if (this.numberOfColumns > 3) {
			return (this.numberOfColumns * 130) + 250
		} else {
			return 600
		}
	},

	cancel: function() {
		this.doClose();
	}
});