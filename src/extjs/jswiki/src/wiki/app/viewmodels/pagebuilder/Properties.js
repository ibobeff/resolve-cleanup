glu.defModel('RS.wiki.pagebuilder.Properties', {
	title: '',
	fullNameIsEditable: false,
	udisplayMode: '',
	roles: null,
	tags: null,
	catalogs: null,
	catalogsSelections: [],
	accessRights: {},
	tagsSelections: [],
	rolesColumns: null,
	rolesSelections: [],
	ucatalogStore: null,
	dirtyComponents: [],
	displayModeWiki$: function() {
		return this.udisplayMode == '' || this.udisplayMode == 'wiki'
	},
	setDisplayModeWiki: function(value) {
		this.set('udisplayMode', 'wiki')
	},
	displayModeDecisionTree$: function() {
		return this.udisplayMode == 'decisiontree'
	},
	setDisplayModeDecisionTree: function(value) {
		this.set('udisplayMode', 'decisiontree')
	},
	displayModeCatalog$: function() {
		return this.udisplayMode == 'catalog'
	},
	setDisplayModeCatalog: function(value) {
		this.set('udisplayMode', 'catalog')
	},
	ucatalogIdIsEnabled$: function() {
		return this.udisplayMode == 'catalog'
	},
	updateDirtyComponents: function(isDirty, component) {
 		if (isDirty) {
			if (this.dirtyComponents.indexOf(component) == -1) {
				this.dirtyComponents.push(component);
			}
		} else if (this.dirtyComponents.indexOf(component) != -1) {
			this.dirtyComponents.splice(component, 1)
		}
	},
	init: function() {
		var cols = [{
			header: '~~name~~',
			dataIndex: 'uname',
			hideable: false,
			flex: 1
		}, {
			xtype: 'checkcolumn',
			stopSelection: false,
			header: '~~viewRight~~',
			dataIndex: 'ureadAccess',
			hideable: false,
			sortable: false,
			width: 70,
			renderer: function(value, meta, record) {
				if (record.get('uadminAccess') || record.get('uwriteAccess')) {
					meta.tdCls = 'x-item-disabled';
					if (!record.get('ureadAccess')) {
						record.data.ureadAccess = true;
						record.phantom = true; // workaround to flag dirty
					}
				}
				if (record.get('ureadAccess')) {
					return '<img class="x-grid-checkcolumn x-grid-checkcolumn-checked" src="/resolve/images/s.gif">';
				} else {
					return '<img class="x-grid-checkcolumn" src="/resolve/images/s.gif">';
				}
			}
		}, {
			xtype: 'checkcolumn',
			stopSelection: false,
			header: '~~editRight~~',
			dataIndex: 'uwriteAccess',
			hideable: false,
			sortable: false,
			width: 70,
			renderer: function(value, meta, record) {
				if (record.get('uadminAccess')) {
					meta.tdCls = 'x-item-disabled';
					if (!record.get('uwriteAccess')) {
						record.data.uwriteAccess = true;
						record.phantom = true; // workaround to flag dirty
					}
				}
				if (record.get('uwriteAccess')) {
					return '<img class="x-grid-checkcolumn x-grid-checkcolumn-checked" src="/resolve/images/s.gif">';
				} else {
					return '<img class="x-grid-checkcolumn" src="/resolve/images/s.gif">';
				}
			}
		}, {
			xtype: 'checkcolumn',
			stopSelection: false,
			header: '~~executeRight~~',
			dataIndex: 'uexecuteAccess',
			hideable: false,
			sortable: false,
			width: 70,
			renderer: function(value, meta, record, rowI, colI, store, view) {
				if (record.get('uadminAccess')) {
					meta.tdCls = 'x-item-disabled';
					if (!record.get('uexecuteAccess')) {
						record.data.uexecuteAccess = true;
						record.phantom = true; // workaround to flag dirty
					}
				}
				if (record.get('uexecuteAccess')) {
					return '<img class="x-grid-checkcolumn x-grid-checkcolumn-checked" src="/resolve/images/s.gif">';
				} else {
					return '<img class="x-grid-checkcolumn" src="/resolve/images/s.gif">';
				}
			}
		}, {
			xtype: 'checkcolumn',
			stopSelection: false,
			header: '~~adminRight~~',
			dataIndex: 'uadminAccess',
			hideable: false,
			sortable: false,
			width: 70,
			renderer: function(value, meta, record) {
				if (record.get('uname') == 'admin' || !clientVM.isAdmin) {
					meta.tdCls = 'x-item-disabled'; 
				}
				if (record.get('uadminAccess')) {
					return '<img class="x-grid-checkcolumn x-grid-checkcolumn-checked" src="/resolve/images/s.gif">';
				} else {
					return '<img class="x-grid-checkcolumn" src="/resolve/images/s.gif">';
				}
			}
		}];
		this.set('rolesColumns', cols);
	},
	addRole: function() {
		this.open({
			mtype: 'RS.sysadmin.RolePicker',
			callback: this.addRoles
		})
	},
	addRoleIsEnabled$: function() {
		return !this.uisDefaultRole
	},
	addRoles: function(newRoles) {
		Ext.Array.forEach(newRoles, function(role) {
			if (this.roles.findExact('uname', role.get('uname')) != -1)
				return;
			this.roles.add({
				uname: role.get('uname'),
				ureadAccess: true,
				uwriteAccess: true,
				uexecuteAccess: true,
				uadminAccess: false
			})
		}, this)
	},
	removeRole: function() {
		this.roles.remove(this.rolesSelections)
	},
	removeRoleIsEnabled$: function() {
		return !this.uisDefaultRole && this.rolesSelections.length > 0
	},
	addTag: function() {
		this.open({
			mtype: 'RS.catalog.TagPicker',
			callback: 'addTags'
		})
	},
	addTags: function(newTags) {
		var tagName = '',
			tagId = '';
		Ext.Array.forEach(newTags, function(tag) {
			tagName = tag.data['name']
			tagId = tag.data['id']
			if (this.tags.findExact('name', tagName) == -1) {
				this.tags.add({
					name: tagName,
					id: tagId
				})
			}
		}, this)
	},
	removeTag: function() {
		Ext.Array.forEach(this.tagsSelections, function(selection) {
			this.tags.remove(selection)
		}, this)
	},
	removeTagIsEnabled$: function() {
		return this.tagsSelections.length > 0
	},
	addCatalog: function() {
		this.open({
			mtype: 'RS.catalog.CatalogPicker',
			callback: this.addCatalogs
		})
	},
	addCatalogs: function(newCatalogs) {
		var catalogName = '',
			catalogId = '',
			realPath = '';
		Ext.Array.forEach(newCatalogs, function(catalog) {
			catalogName = catalog.name
			catalogId = catalog.name
			realPath = catalog.path
			if (this.catalogs.findExact('name', catalogName) == -1) {
				this.catalogs.add({
					name: realPath,
					id: catalogId
				})
			}
		}, this);
	},
	removeCatalog: function() {
		Ext.Array.forEach(this.catalogsSelections, function(selection) {
			this.catalogs.remove(selection);
		}, this)
	},
	removeCatalogIsEnabled$: function() {
		return this.catalogsSelections.length > 0;
	},
	saveAndSetStore: function(name, store){
		var records = store.getRange();
		store['__saved'] = [];
		for(var i=0; i<records.length; i++){
			store['__saved'].push(records[i].copy());
		}
		this.set(name, store);
	},
	saveProperty: function(property, ignoreDirtyChecking) {
		this.parentVM.set(property, this[property]);
		if (ignoreDirtyChecking) {
			return;
		}
		if (this.parentVM.originalData[property] != this[property]) {
			return true;
		} else {
			return false;
		}
	},
	savePropertiesNoDirtyChecking: function(properties) {
		for(var i=0; i<properties.length; i++) {
			this.saveProperty(properties[i], true);
		}
	},
	saveProperties: function(properties) {
		var isDirty = false;
		for(var i=0; i<properties.length; i++) {
			if (this.saveProperty(properties[i])) {
				isDirty = true;			
			}
		}
		this.updateDirtyComponents(isDirty, 'Properties');
	},
	checkStoresDirty: function(stores) {
		var isDirty = false;
		if (Ext.isFunction(this.rootVM.isStoreDirty)) {
			for(var i=0; i<stores.length; i++) {
				if (this.rootVM.isStoreDirty(stores[i])) {
					isDirty = true;
					break;
				}
			}
		}
		this.updateDirtyComponents(isDirty, 'Stores');
	},
	checkStoresDirtyData: function(storeNames) {
		var isDirty = false;
		for(var i=0; i<storeNames.length; i++) {
			var storeName = storeNames[i]+'s';	// since our local store name ends with 's'
			var storeData = [];

			// gather the local store data and compare against originalData
			this[storeName].each(function(storeItem) {
				storeData.push(storeItem.get('name'))
			})
			storeData = storeData.join(',');

			// retrieve the originalData to compare against local store data
			var originalData = this.parentVM.originalData['u'+storeNames[i]]; // originalData fieldname is string starting with "u" and not ending in "s"
			if (!originalData) { // convert null to empty string if applicable
				originalData = '';
			}

			if (storeData != originalData) {
				isDirty = true;
				break;
			}
		}
		this.updateDirtyComponents(isDirty, 'StoresData');
	},
	update: function(w) {
		this.savePropertiesNoDirtyChecking(['fullNameIsEditable', 'fullName']);
		this.saveProperties(['ufullname', 'utitle', 'usummary', 'udisplayMode', 'uisDefaultRole', 'ucatalogId']);
		this.checkStoresDirty([this.roles]);
		this.checkStoresDirtyData(['tag', 'catalog']);

		if (this.dirtyComponents.length) {
			if (Ext.isFunction(this.rootVM.notifyDirtyFlag)) {
				this.rootVM.notifyDirtyFlag(this.viewmodelName);
			}
		} else if (Ext.isFunction(this.rootVM.clearComponentDirtyFlag)) {
			this.rootVM.clearComponentDirtyFlag(this.viewmodelName);
		}
		this.doClose();
	},
	cancel: function() {
		setTimeout(this.parentVM.rejectStoreChanges, 0, [this.roles, this.tags, this.catalogs, this.ucatalogStore]);
		this.doClose();
	},
	beforeClose: function() {
		setTimeout(this.parentVM.rejectStoreChanges, 0, [this.roles, this.tags, this.catalogs, this.ucatalogStore]);
	}
})
