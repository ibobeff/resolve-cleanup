glu.defModel('RS.wiki.pagebuilder.Revision', {
	name : '',
	uname: '',
	id : '',
	unamespace: '',
	revisionsList: {
		mtype: 'store',
		fields: ['documentName', {
			name: 'version',
			convert: function(v, r) {
				return r.raw.version || r.raw.revision;
			}
		}, /* TODO - support Version Control: 'isStable', 'referenced',*/ 'comment', 'createdBy', {
			name: 'sysCreatedOn',
			type: 'time',
			format: 'c',
			dateFormat: 'c'
		}],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	dumper: null,
	compareRevisionIsVisible: true,
	rollbackRevisionIsVisible: true,
	resetRevisionIsVisible: true,
	revisionsListSelections: [],
	revisionsListColumns: [],
	init: function() {
		this.initPagination();
		this.loadRevision();
		var dateFormat = this.parentVM.userDateFormat || this.rootVM.userDateFormat;
		this.set('revisionsListColumns', [{
			header: '~~documentName~~',
			dataIndex: 'documentName',
			width: 130,
			sortable: false
		}, {
			header: '~~revisionNumber~~',
			dataIndex: 'version',
			width: 100
		}, 
		/* TODO - support Version Control
		{
			header: '~~stableVersion~~',
			dataIndex: 'isStable',
			width: 100,
			renderer: RS.common.grid.booleanRenderer(),
		}, {
			header: '~~referenced~~',
			dataIndex: 'referenced',
			width: 100,
			renderer: RS.common.grid.booleanRenderer(),
		}, 
		*/
		{
			header: '~~comment~~',
			flex: 1,
			dataIndex: 'comment',
			sortable: false,
			renderer: function(value, metaData) {
				metaData['tdAttr'] = Ext.String.format('data-qtip="{0}"', Ext.String.htmlEncode(value));
				return Ext.String.htmlEncode(value);
			},
		}, {
			header: '~~sysUpdatedBy~~',
			dataIndex: 'createdBy'
		}, {
			header: '~~sysUpdatedOn~~', // '~~createdOn~~', TODO - support Version Control
			dataIndex: 'sysCreatedOn',
			width: 150,
			renderer: function(value) {
				return Ext.Date.format(new Date(value), dateFormat)
			}
		}]);
		if (this.isDetails) {
			this.set('compareRevisionIsVisible', false);
			this.set('rollbackRevisionIsVisible', false);
			this.set('resetRevisionIsVisible', false);
		}
	},
	revisionsIsPressed$: function() {
		return this.activeTab == 1 && this.editMode == 3;
	},
	revisionsIsVisible$: function() {
		return !this.viewTabIsPressed && this.activeTab == 1;
	},
	loadRevision: function() {
		this.revisionsList.removeAll();
		this.ajax({
			url: '/resolve/service/wiki/getHistory',
			params: {
				docSysId: '',
				docFullName: this.name
			},

			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('listRevisionsError'), respData.message);
					return;
				}
				this.loadPageRecords(respData.records);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},
	commitRevision: function() {

	},

	// Client-side pagination
	list: [],
	pageList: [],
	currentPage: 1,
	numberPerPage: 20,
	numberOfPages: 1,
	paginationPadding: '0 0 0 0',

	nextDisabled: false,
	previousDisabled: false,
	firstDisabled: false,
	lastDisabled: false,

	initPagination: function() {
		var versionDialogWidth = Math.round(Ext.getBody().getWidth() * 0.8);
		this.set('paginationPadding', Ext.String.format('0 0 0 {0}', Math.round(versionDialogWidth / 2) - 100));
	},

	loadPageRecords: function(records) {
		this.set('list', records);
		this.set('numberOfPages', Math.ceil(this.list.length / this.numberPerPage));
		this.loadList();
	},

	firstPage: function() {
		this.set('currentPage', 1);
		this.loadList();
	},
	lastPage: function() {
		this.set('currentPage', this.numberOfPages);
		this.loadList();
	},
	nextPage: function() {
		this.set('currentPage', this.currentPage += 1);
		this.loadList();
	},
	previousPage: function() {
		this.set('currentPage', this.currentPage -= 1);
		this.loadList();
	},
	loadList: function() {
		var begin = ((this.currentPage - 1) * this.numberPerPage);
		var end = begin + this.numberPerPage;
		this.set('pageList', this.list); // this.set('pageList', this.list.slice(begin, end)); TODO - support Version Control
		this.revisionsList.loadData(this.pageList);
		this.check();
	},
	check: function() {
		this.set('nextDisabled', (!this.numberOfPages || this.currentPage == this.numberOfPages) ? true : false);
		this.set('previousDisabled', this.currentPage == 1 ? true : false);
		this.set('firstDisabled', this.currentPage == 1 ? true : false);
		this.set('lastDisabled', (!this.numberOfPages || this.currentPage == this.numberOfPages) ? true : false);
	},
	paginationText$: function() {
		var max = this.list.length;
		var begin = max ? ((this.currentPage - 1) * this.numberPerPage) + 1 : 0;
		var end = begin + this.numberPerPage - 1;
		if (end > max) {
			end = max;
		}
		return Ext.String.format('{0} - {1} of {2}', begin, end, max);
	},

	viewRevision: function() {
		var revisionNo = this.revisionsListSelections[0].get('version') || this.revisionsListSelections[0].get('revision');
		this.open({
			mtype: 'RS.wiki.RevisionView',
			id: this.id,
			namespace: this.unamespace,
			name: this.uname,
			revision: revisionNo
		});
	},
	viewRevisionIsEnabled$: function() {
		return this.revisionsListSelections.length == 1;
	},
	compareRevision: function() {
		this.open({
			mtype: 'RS.wiki.RevisionComparison',
			id: this.id,
			namespace: this.unamespace,
			name: this.uname,
			type: this.revisionsListSelections[0].get('type'),
			revision1: this.revisionsListSelections[0].get('version') || this.revisionsListSelections[0].get('revision'),
			revision2: this.revisionsListSelections[1].get('version') || this.revisionsListSelections[1].get('revision')
		});
	},
	compareRevisionIsEnabled$: function() {
		if (this.revisionsListSelections.length != 2)
			return false;
		var type1 = this.revisionsListSelections[0].get('type');
		var type2 = this.revisionsListSelections[1].get('type');
		return type1 == type2;
	},
	rollbackRevision: function() {
		this.message({
			title: this.localize('rollbackRevision'),
			msg: this.localize('rollbackRevisionMsg'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				'yes': this.localize('rollbackRevision'),
				'no': this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.ajax({
					url: '/resolve/service/wiki/revision/rollback',
					params: {
						id: this.id,
						docFullname: this.ufullname,
						revision: this.revisionsListSelections[0].get('version') || this.revisionsListSelections[0].get('revision')
					},
					success: function(resp) {
						var respData = RS.common.parsePayload(resp);
						if (!respData.success) {
							clientVM.displayError(this.localize('rollbackError', {
								msg: respData.message
							}));
							return;
						}
						clientVM.displaySuccess(this.localize('rollbackSuccess'));					
						this.parentVM.set('havePersistedWikiSinceLastView', true);					
						this.loadRevision();

						// after rollback, use the latest version just created via rollback
						this.parentVM.loadLatestWikiDocument();
						this.doClose();
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				});
			}
		})
	},
	rollbackRevisionIsEnabled$: function() {
		return this.revisionsListSelections.length == 1;
	},
	resetRevision: function() {
		this.message({
			title: this.localize('resetRevision'),
			msg: this.localize('resetRevisionMsg'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				'yes': this.localize('resetRevision'),
				'no': this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.ajax({
					url: '/resolve/service/wiki/revision/reset',
					params: {
						id: this.id,
						docFullname: this.ufullname
					},
					success: function(resp) {
						var respData = RS.common.parsePayload(resp);
						if (!respData.success) {
							clientVM.displayError(this.localize('resetError', respData.message));
							return;
						}
						clientVM.displaySuccess(this.localize('resetSuccess'));
						this.loadRevision();
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				});
			}
		});
	},
	selectRevision: function() {
        if (!this.dumper)
            return
        if (Ext.isFunction(this.dumper))
            this.dumper(this.revisionsListSelections[0]);
        else {
            var scope = this.dumper.scope;         
            this.dumper.dump.call(scope, [this.revisionsListSelections[0]]);
        }
        this.doClose()
	},
	selectRevisionIsEnabled$: function() {
		return this.revisionsListSelections.length == 1;
	},
	close: function() {
		this.doClose();
	}
});