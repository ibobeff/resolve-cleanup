glu.defModel('RS.wiki.pagebuilder.ImageManager',{
	mixins: ['FileUploadManager', 'Attachments'],
	filesOnAllCompleteCallback: function() {
		//this.loadFiles();
	},
	init: function() {
		this.set('title', this.localize('imageManagerTitle'));
		this.queuedStatus = this.localize('queuedStatus');
		this.errorStatus = this.localize('errorStatus');
		this.completedStatus = this.localize('completedStatus');
		this.processingStatus = this.localize('processingStatus');
		this.uploadingStatus = this.localize('uploadingStatus');
	},

	isEditMode: true,
	multiSelect: false,
	allowedExtensions: ['png', 'jpg', 'jpeg', 'gif'],

	select: function() {
		if (Ext.isDefined(this.parentVM.src)) this.parentVM.set('src', this.selections[0].get('fileName') || this.selections[0].get('ufilename') || this.selections[0].get('name'))
		if (Ext.isDefined(this.parentVM.text)) this.parentVM.set('text', this.selections[0].get('fileName') || this.selections[0].get('ufilename') || this.selections[0].get('name'))
		this.doClose()
	},

	selectIsEnabled$: function() {
		return this.selections.length > 0;
	}
})

/*
glu.defModel('RS.wiki.pagebuilder.ImageManager', {

	imagesSelections: [],

	selectionchange: function(selections) {
		this.set('imagesSelections', selections)
	},

	name: null,	// wiki name will be passed from parent to RS.wiki.pagebuilder.ImageManager

	init: function() {},

	select: function() {
		if (Ext.isDefined(this.parentVM.src)) this.parentVM.set('src', this.imagesSelections[0].get('fileName') || this.imagesSelections[0].get('ufilename') || this.imagesSelections[0].get('name'))
		if (Ext.isDefined(this.parentVM.text)) this.parentVM.set('text', this.imagesSelections[0].get('fileName') || this.imagesSelections[0].get('ufilename') || this.imagesSelections[0].get('name'))
		this.doClose()
	},
	selectIsEnabled$: function() {
		return this.imagesSelections.length == 1
	},
	cancel: function() {
		this.doClose()
	}
})
*/
