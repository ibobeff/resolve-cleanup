/*
glu.defModel('RS.wiki.pagebuilder.General', {
	id: '',
	isReadOnly$: function() {
		return !!this.id;
	},
	name: '',
	namespace: '',
	summary: '',
	generated: false,
	fields: ['id', 'name', 'namespace', 'summary', 'generated'],
	defaultData: {
		name: '',
		namespace: '',
		summary: ''
	},
	generatedChanged$: function() {
		this.parentVM.set('generated', this.generated);
	},
	resetGeneral: function() {
		this.loadData(this.defaultData);
	},
	nameIsValid$: function() {
		return this.validateName(this.name) ? true : this.localize('invalidName');
	},
	namespaceIsValid$: function() {
		return this.validateNamespace(this.namespace) ? true : this.localize('invalidNamespace');
	},
	populateScreen: function(meta) {
		this.resetGeneral();
		this.loadData(meta);
	},
	collect: function() {
		return this.asObject();
	},
	validate$: function() {
		this.parentVM.set('generalTabIsValid', this.isValid);
	}
})
*/