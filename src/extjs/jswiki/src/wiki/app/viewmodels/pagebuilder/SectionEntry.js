glu.defModel('RS.wiki.pagebuilder.SectionEntry', {
	// mixins: ['DragWatcherContainer'],
	content : "",
	regSection: /\{section(.*?)\}/gi,
	types: [],
	regWidth: /width:(.*?)%/i,
	regVerticalAlign: /vertical-align:(.*?)[;]*"/i,
	margin: 20,
	editingTitle: false,
	id: '',
	order: 0,
	moveUpIsEnabled: true,
	moveDownIsEnabled: true,
	params: [],
	properties : {
		Alignment : {
			col1 : "top",
			col2 : "top",
			col3 : "top",
			col4 : "top",
			col5 : "top",
			col6 : "top",
			col7 : "top"
		},
		Width : {
			col1 : 100,
			col2 : 0,
			col3 : 0,
			col4 : 0,
			col5 : 0,
			col6 : 0,
			col7 : 0
		},
		active : true,
		hidden : false,
		sectionHeight : -1,
		//sectionWidth : -1,
		sectionTitle : 'Untitled Section'
	},
	fields: ['id', 'type', {
		name: 'order',
		type: 'float'
	}],
	componentEntries: {
		mtype: 'list',
		autoParent: true
	},
	init: function() {
		if(this.new)
			this.addNewComponent(null, true);
		else
			this.parseSectionConfiguration();
		this.parentVM.updateUpDownBtn();
	},
	parseSectionConfiguration: function() {
		var sectionConfigSplit = this.content.match(this.regSection);

		if (sectionConfigSplit && sectionConfigSplit.length > 0) {
			var sectionParamSplit = this.regSection.exec(sectionConfigSplit[0]);

			if (sectionParamSplit.length > 0) {
				sectionParamSplit.splice(0, 1);
			}
			for (var i = 0; i < sectionParamSplit.length; i++) {
				var split = sectionParamSplit[i];
				if (split) {
					if (split[0] == ':') {
						split = split.substring(1);
					}
					var params = split.split('|');
					for(var j = 0; j < params.length; j++) {
						if (params[j].length > 0) {
							var nameVal = params[j].split('=');

							if (nameVal.length > 1 && typeof nameVal[0].toLowerCase === 'function') {
								this.setProperty(nameVal[0].toLowerCase(), nameVal[1]);
							}
						}
					}
				}
			}
		}
	},
	isValidComponentType: function(type) {
		var valid;
		switch (type.toLowerCase()) {
		case 'url':
		case 'source':
		case 'wysiwyg':
		case 'groovy':
		case 'html':
		case 'javascript':
		case 'form':
		case 'encrypt':
		case 'infobar':
		case 'image':
		case 'model':
		case 'code':
		case 'progress':
		case 'automation':
		case 'result':
		case 'actiontask':
		case 'detail':
		case 'wikidoc':
		case 'table':
		case 'xtable':
		case 'sociallink':
		case 'new':	// new component
			valid = true;
			break;
		case 'procedure': // new component for playbook & playbook template
		default:
			valid = false;
			break;
		}
		return valid;
	},
	setProperty: function (propertyName, propertyValue) {
		switch (propertyName) {
			case 'type':
				var types = propertyValue.toLowerCase().split(',');
				for (var i = 0; i < types.length; i++) {
					if (types[i] == '') {
						types[i] = 'new';
					}
					else if (!this.isValidComponentType(types[i])) {
						clientVM.displayError(this.localize('unknownLegacyType', types[i].toLowerCase()))
						// default all unknown types to 'source' (Resolve Markup type)
						types[i] = 'source';
					}
				}
				this.set('types', types);
				break;
			case 'title':
				this.properties['sectionTitle'] = propertyValue;
				this.set('sectionTitle', propertyValue);
				break;
			case 'hidden':
				this.properties['hidden'] = (propertyValue.toLowerCase() === 'true');
				break;
			case 'active':
					this.properties['active'] = (propertyValue.toLowerCase() === 'true');
				break;
			case 'height':
				var n = +propertyValue;
				if(!isNaN(n)) {
					this.properties['sectionHeight'] = n;
				}
				break;
		}
	},
	numberOfColumns$ : function(){
		return this.types.length;
	},
	sectionTitle$ : function(){
		return this.localize('untitledSection');
	},
	updateColumnWidth : function(columnNo, value){
		this.properties['Width']['col' + columnNo] = value;
	},
	updateColumnAlignment : function(columnNo, value){
		this.properties['Alignment']['col' + columnNo] = value;
	},
	configureDefaultColumnWidths$: function() {
		switch (this.numberOfColumns) {
			case 1:
				this.updateColumnWidth(1, 100);
				this.updateColumnWidth(2, 0);
				this.updateColumnWidth(3, 0);
				this.updateColumnWidth(4, 0);
				this.updateColumnWidth(5, 0);
				this.updateColumnWidth(6, 0);
				this.updateColumnWidth(7, 0);
				break;
			case 2:
				this.updateColumnWidth(1, 50);
				this.updateColumnWidth(2, 50);
				this.updateColumnWidth(3, 0);
				this.updateColumnWidth(4, 0);
				this.updateColumnWidth(5, 0);
				this.updateColumnWidth(6, 0);
				this.updateColumnWidth(7, 0);
				break;
			case 3:
				this.updateColumnWidth(1, 33);
				this.updateColumnWidth(2, 33);
				this.updateColumnWidth(3, 33);
				this.updateColumnWidth(4, 0);
				this.updateColumnWidth(5, 0);
				this.updateColumnWidth(6, 0);
				this.updateColumnWidth(7, 0);
				break;
			case 4:
				this.updateColumnWidth(1, 25);
				this.updateColumnWidth(2, 25);
				this.updateColumnWidth(3, 25);
				this.updateColumnWidth(4, 25);
				this.updateColumnWidth(5, 0);
				this.updateColumnWidth(6, 0);
				this.updateColumnWidth(7, 0);
				break;
			case 5:
				this.updateColumnWidth(1, 20);
				this.updateColumnWidth(2, 20);
				this.updateColumnWidth(3, 20);
				this.updateColumnWidth(4, 20);
				this.updateColumnWidth(5, 20);
				this.updateColumnWidth(6, 0);
				this.updateColumnWidth(7, 0);
				break;
			case 6:
				this.updateColumnWidth(1, 16);
				this.updateColumnWidth(2, 16);
				this.updateColumnWidth(3, 16);
				this.updateColumnWidth(4, 16);
				this.updateColumnWidth(5, 16);
				this.updateColumnWidth(6, 16);
				this.updateColumnWidth(7, 0);
				break;
			case 7:
				this.updateColumnWidth(1, 14);
				this.updateColumnWidth(2, 14);
				this.updateColumnWidth(3, 14);
				this.updateColumnWidth(4, 14);
				this.updateColumnWidth(5, 14);
				this.updateColumnWidth(6, 14);
				this.updateColumnWidth(7, 14);
				break;
		}
	},
	parseComponent: function(section) {
		//Based on the type of content we have, there might be various types of old macros or whatever, so we need to configure them properly now
		var sectionLines = section.split('\n');
		var	columns = [];
		var	lines = [];
		var	col1WidthSet = false;
		var	col2WidthSet = false;
		var	col3WidthSet = false;
		var	col4WidthSet = false;
		var	col5WidthSet = false;
		var	col6WidthSet = false;
		var	col7WidthSet = false;
		var	col1AlignmentSet = false;
		var	col2AlignmentSet = false;
		var	col3AlignmentSet = false;
		var	col4AlignmentSet = false;
		var	col5AlignmentSet = false;
		var	col6AlignmentSet = false;
		var	col7AlignmentSet = false;

		for (var i = 0; i < sectionLines.length; i++) {
			var line = sectionLines[i];

			if (line.indexOf('class="wiki-builder"') === -1) {
				lines.push(line);
			} else if (line.indexOf("<td") > -1) {
				var widthMatch = this.regWidth.exec(line);

				if (widthMatch && widthMatch.length > 1) {
					var percent = widthMatch[1].trim();

					if (!col1WidthSet) {
						this.updateColumnWidth(1, percent);
						col1WidthSet = true;
					} else if (!col2WidthSet) {
						this.updateColumnWidth(2, percent);
						col2WidthSet = true;
					} else if (!col3WidthSet) {
						this.updateColumnWidth(3, percent);
						col3WidthSet = true;
					} else if (!col4WidthSet) {
						this.updateColumnWidth(4, percent);
						col4WidthSet = true;
					} else if (!col5WidthSet) {
						this.updateColumnWidth(5, percent);
						col5WidthSet = true;
					} else if (!col6WidthSet) {
						this.updateColumnWidth(6, percent);
						col6WidthSet = true;
					} else if (!col7WidthSet) {
						this.updateColumnWidth(7, percent);
						col7WidthSet = true;
					}
				}

				var alignMatch = this.regVerticalAlign.exec(line);

				if (alignMatch && alignMatch.length > 1) {
					var alignment = alignMatch[1].trim();

					if (!col1AlignmentSet) {
						this.updateColumnAlignment(1, alignment);
						col1AlignmentSet = true;
					} else if (!col2AlignmentSet) {
						this.updateColumnAlignment(2, alignment);
						col2AlignmentSet = true;
					} else if (!col3AlignmentSet) {
						this.updateColumnAlignment(3, alignment);
						col3AlignmentSet = true;
					} else if (!col4AlignmentSet) {
						this.updateColumnAlignment(4, alignment);
						col4AlignmentSet = true;
					} else if (!col5AlignmentSet) {
						this.updateColumnAlignment(5, alignment);
						col5AlignmentSet = true;
					} else if (!col6AlignmentSet) {
						this.updateColumnAlignment(6, alignment);
						col6AlignmentSet = true;
					} else if (!col7AlignmentSet) {
						this.updateColumnAlignment(7, alignment);
						col7AlignmentSet = true;
					}
				}
			} else if (line.indexOf('</td class="wiki-builder">') > -1) {
				columns.push(lines.join('\n'));
				lines = [];
			}
		}
		//any leftover lines (like when we don't have any table information)
		if (lines.length > 0) {
			columns.push(lines.join('\n'));
		}

		if (this.types.length) {
			for(var i = 0; i < this.types.length; i++) {
				var type = this.types[i];

				var configurationLines = (columns[i] || '').split('\n'),
					properties = {};

				var props = this.extractComponentProperties(configurationLines);

				for (var j = 0; j < props.length; j++) {
					var split = props[j].split('=');

					if (split.length > 1) {
						properties[split[0]] = split.slice(1).join('=');
					} else  {
						properties['text'] = split[0];
					}
				}
				//this.parseTypeConfiguration(type, properties, columns[i], configurationLines)
				this.populateComponent(type, properties, columns[i]);
			}
		}
		else if (columns) {
			this.set('types', this.types.concat('source'));
			this.populateComponent('source', {}, columns[0]);
		}
		this.updateLeftRightBtn();
	},
	extractComponentProperties: function (configurationLines) {
		var props = [];
		if (configurationLines.length > 0) {
			var c = configurationLines.splice(0, 1)[0];
			var colonIndex = c.indexOf(':');
			if(colonIndex > -1) {
				var parenIndex = c.indexOf('}');
				var endIndex = c.length - 1;
				if(parenIndex > -1) {
					endIndex = parenIndex;
				}
				props = c.substring(colonIndex + 1, endIndex).split('|');
			}
		}
		return props;
	},
	populateComponent: function(type, properties, content) {
		var nametype = (type != '' && type != 'new') ? type : 'new';
		var columnPos = this.componentEntries.length;
		var componentEntry = this.model({
			mtype : 'RS.wiki.pagebuilder.ComponentEntry',
			name : this.localize(nametype),
			type: type,
			properties: properties,
			columnPos: columnPos,
			content: content,
			id : Ext.data.IdGenerator.get('uuid').generate()
		});
		this.componentEntries.add(componentEntry);
	},
	getSectionContent: function() {
		var content = [];
		var hasColumns = this.numberOfColumns > 1;
		var sectionProperties = this.properties;

		if (hasColumns) {
			content.push(RS.wiki.pagebuilder.Constant.TABLETAG);
		}
		this.componentEntries.foreach(function(component, position){
			var componentContent = component.getComponentContent();
			if(hasColumns){
				var colNum = position + 1;
				var columnWidth = sectionProperties['Width']['col' + colNum];
				var columnAlignment = sectionProperties['Alignment']['col' + colNum];
				var style = 'width:' + columnWidth + '%;vertical-align:' + columnAlignment + ';';
				content.push('<td style="' + style + '" class="wiki-builder">');
				content.push(componentContent);
				content.push('</td class="wiki-builder">');
			}
			else {
				content.push(componentContent);
			}
		})
		if (hasColumns) {
			content.push(RS.wiki.pagebuilder.Constant.TABLECLOSINGTAG);
		}

		//Get section properties
		var sectionConfig = ':type=' + this.types.join(',');

		if (this.title != this.localize('untitledSection')) {
			sectionConfig += '|title=' + sectionProperties['sectionTitle'];
		}
		if (this.properties['sectionHeight'] > -1) {
			sectionConfig+= '|height=' + sectionProperties['sectionHeight'];
		}
		if (this.properties['hidden']) {
			sectionConfig += '|hidden=' + sectionProperties['hidden'];
		}
		if (!this.properties['active']) {
			sectionConfig += '|active=' + sectionProperties['active'];
		}
		return '{section' + sectionConfig + '}\n' + content.join('\n') + '\n{section}'
	},
	addNewComponent: function(e, doNotLaunchPicker) {
		if (!doNotLaunchPicker) {
			if (Ext.isFunction(this.rootVM.notifyDirtyFlag)) {
				this.rootVM.notifyDirtyFlag('Pagebuilder');
			}
		}
		var columnPos = this.componentEntries.length;
		var componentEntry = this.model({
			mtype : 'RS.wiki.pagebuilder.ComponentEntry',
			name : 'New Component',
			type: this.parentVM.defaultComType || 'new',
			columnPos: columnPos,
			id : Ext.data.IdGenerator.get('uuid').generate(),
		});
		this.componentEntries.add(componentEntry);
		this.updateComponentType(this.parentVM.defaultComType || 'new', columnPos);
		this.updateLeftRightBtn();
		if (!doNotLaunchPicker) {
			componentEntry.launchComponentPicker();
		}
		this.parentVM.fireEvent('newComponentAdded', componentEntry);
	},
	updateComponentType : function(newType, columnPos){
		if (columnPos > this.types.length-1) {
			this.set('types', this.types.concat(newType));
		}
		else {
			this.types[columnPos] = newType;
		}
	},
	addNewComponentIsVisible$: function() {
		return this.componentEntries.length >= 7 ? false : true;
	},
	sectionProperties: function() {
		this.open({
			mtype: 'RS.wiki.pagebuilder.SectionProperties',
			numberOfColumns : this.types.length,
			properties : this.properties
		})
	},
	updateProperties : function(newProperties){
		this.set('properties', newProperties);
		this.set('sectionTitle', newProperties.sectionTitle);
		this.rootVM.checkPageDirtyFlag();
	},
	configureUpDownBtn: function() {
		this.set('moveUpIsEnabled', this.parentVM.sectionEntries.indexOf(this) > 0);
		this.set('moveDownIsEnabled', this.parentVM.sectionEntries.indexOf(this) < this.parentVM.sectionEntries.length - 1);
	},
	moveUp: function() {
		this.parentVM.sectionEntries.transferFrom(this.parentVM.sectionEntries, this, this.parentVM.sectionEntries.indexOf(this) - 1);
		this.parentVM.updateUpDownBtn();
		this.rootVM.checkPageDirtyFlag();
	},
	moveDown: function() {
		this.parentVM.sectionEntries.transferFrom(this.parentVM.sectionEntries, this, this.parentVM.sectionEntries.indexOf(this) + 1)
		this.parentVM.updateUpDownBtn();
		this.rootVM.checkPageDirtyFlag();
	},
	updateLeftRightBtn: function() {
		this.componentEntries.foreach(function(component) {
			component.configureLeftRightBtn();
		});
	},
	reallyRemoveSection: function() {
		this.parentVM.removeSection(this);
	},
	removeSection: function() {
		if (this.componentEntries.length) {
			var confirmDelete = false;
			var numComponents = 0;
			for (var i=0; i<this.componentEntries.length; i++) {
				var component = this.componentEntries.getAt(i);
				if (component.type && component.type != '' && component.type != 'new') {
					confirmDelete = true;
					numComponents++;
				}
			}
			if (confirmDelete) {
				var confirmMessage;
				if (this.componentEntries.length > 1) {
					confirmMessage = this.parentVM.localize('confirmRemoveSectionDescX', numComponents);
				}
				else {
					confirmMessage = this.parentVM.localize('confirmRemoveSectionDesc1');
				}
				this.message({
					title: this.parentVM.localize('confirmRemoveSectionTitle'),
					msg: confirmMessage,
					buttons: Ext.MessageBox.YESNO,
					buttonText: {
						'yes': this.parentVM.localize('remove'),
						'no':this.parentVM.localize('cancel')
					},
					fn: function(btn) {
						if (btn == 'yes') {
							this.reallyRemoveSection();
						}
					},
					scope: this
				})
			}
			else {
				this.reallyRemoveSection();
			}
		}
		else {
			this.reallyRemoveSection();
		}
	},
	removeComponent: function(component) {
		var componentIndex = this.componentEntries.indexOf(component);
		this.types.splice(componentIndex,1);
		this.set('numberOfColumns', this.types.length);
		this.componentEntries.remove(component);
		this.updateLeftRightBtn();
		if (Ext.isFunction(this.rootVM.checkPageDirtyFlag))
			this.rootVM.checkPageDirtyFlag();
	}
})
