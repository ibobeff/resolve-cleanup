glu.defModel('RS.wiki.pagebuilder.ComponentEntry', {
	// mixins: ['DragWatcher'],
	entityType: 'component',
	id: '',
	name: '',
	type: null,
	iconEl: null,
	content: '',
	imageSrc: '',
	imageSize: 0,
	imageCursor: '',
	showIcon: true,
	iconLabel: '',
	properties: {},
	columnPos: 0,
	moveLeftIsEnabled: true,
	moveRightIsEnabled: true,
	detailedComponentHtml: '',
	detailedComponentStyle: '',
	detailedComponentWidth: 0,
	detailedComponentHeight: 0,
	csrftoken: '',
	fields: ['id', 'name',
		'entityType', {
			name: 'order',
			type: 'float'
		},
	],
	defaultData: {
		name: 'New Component',
		entityType: 'component',
	},
	//REACTOR
	when_component_type_changed : {
		on: ['typeChanged'],
		action : function(){
			this.parentVM.updateComponentType(this.type, this.columnPos);
		}
	},
	init: function() {
		this.initToken();
		this.setItemMetaData(this.type);
		this.parentVM.updateLeftRightBtn();
	},
	activate: function() {
		this.initToken();
	},
	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/wiki/download', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this));
	},
	getData: function() {
		var task = this.asObject();
		return task;
	},
	setIconLabel$: function() {
		this.set('iconLabel', this.name);
	},
	getExpandFlag: function(type) {
		var expand;
		switch (type.toLowerCase()) {
			case 'wysiwyg':
			case 'code':
			case 'source':
			case 'groovy':
			case 'html':
			case 'javascript':
			case 'result':
			case 'detail':
			case 'table':
			case 'xtable':
				expand = true;
				break;
			default:
				expand = false;
				break;
		}
		return expand;
	},
	configureLeftRightBtn: function() {
		this.set('moveLeftIsEnabled', this.parentVM.componentEntries.indexOf(this) > 0);
		this.set('moveRightIsEnabled', this.parentVM.componentEntries.indexOf(this) < this.parentVM.componentEntries.length - 1);
	},
	moveLeft: function() {
		var currentComponentIdx = this.parentVM.componentEntries.indexOf(this);
		this.parentVM.types[currentComponentIdx] = this.parentVM.types[currentComponentIdx -1];
		this.parentVM.types[currentComponentIdx -1] = this.type;

		this.parentVM.componentEntries.transferFrom(this.parentVM.componentEntries, this, this.parentVM.componentEntries.indexOf(this) - 1);
		this.parentVM.updateLeftRightBtn();
		this.rootVM.checkPageDirtyFlag();
	},
	moveRight: function() {
		var currentComponentIdx = this.parentVM.componentEntries.indexOf(this);
		this.parentVM.types[currentComponentIdx] = this.parentVM.types[currentComponentIdx +1];
		this.parentVM.types[currentComponentIdx +1] = this.type;

		this.parentVM.componentEntries.transferFrom(this.parentVM.componentEntries, this, this.parentVM.componentEntries.indexOf(this) + 1)
		this.parentVM.updateLeftRightBtn();
		this.rootVM.checkPageDirtyFlag();
	},
	editComponent: function(event,isNew) {
		if (this.type && this.type != 'new') {
			this.open({
				mtype: 'RS.wiki.pagebuilder.ComponentEdit',
				componentName: this.name,
				type: this.type,
				properties: this.properties,
				content: this.content,
				expand: this.getExpandFlag(this.type),
				isNew : isNew
			});
		}
		else {
			this.launchComponentPicker();
		}
	},
	editComponentIsVisible$: function() {
		return (this.type && this.type != 'new') ? true : false;
	},
	launchComponentPicker: function() {
		this.open({
			mtype: 'RS.wiki.pagebuilder.ComponentPicker'
		});
	},
	addComponentIsVisible$: function() {
		return this.type ? false : true;
	},
	updateDetailedComponent: function(width, height, fromControl) {
		var type = this.type ? this.type.toLowerCase() : '',
			maxWidth = width - 20,
			maxHeight = height - 20;
		if (fromControl) {
			this.beQuiet = true;
		} else {
			this.beQuiet = false;
		}
		this.set('detailedComponentWidth', width);
		this.set('detailedComponentHeight', height);
		if (type === 'image') {
			this.set('detailedComponentHtml', '<img src="'+this.imageSrc+'" style="max-height:'+maxHeight+'px;max-width:'+maxWidth+'px" alt="Failed to load image!"/>');
		}
	},
	iconShown$: function() {
		return this.showIcon;
	},
	getImageURL: function(imageName, token_pair) {
		return '/resolve/service/wiki/download?wiki='+this.rootVM.unamespace+'.'+this.rootVM.uname+'&attachFileName='+encodeURIComponent(imageName) + '&' + this.csrftoken || (token_pair[0] + '=' + token_pair[1]);
	},
	updateComponentLayout: function(type) {
		var fileName = this.localize(type).replace(/ /g, '-'),
			imageSrc = Ext.String.format('/resolve/images/sections/components/{0}.png', fileName),
			iconLabel = this.localize(type.replace(/ /g, '').toLowerCase()),
			showIcon = true,
			imageSize = 80,
			detailedComponentHtml = '',
			detailedComponentStyle = '';

		switch (type.toLowerCase()) {
			case 'form':
				if (this.properties.name && this.properties.name.trim() != '') {
					iconLabel = this.properties.name;
				}
				break;
			case 'encrypt':
				if (this.properties.key && this.properties.key != 'undefined') {
					iconLabel = this.properties.key;
				}
				break;
			case 'model':
				if (this.properties.wiki && this.properties.wiki.trim() != '') {
					iconLabel = this.properties.wiki;
				}
				break;
			case 'progress':
				if (this.properties.text && this.properties.text.trim() != '') {
					// make sure we have the wiki download csrf page token before retrieving the ImageURL
					clientVM.getCSRFToken_ForURI('/resolve/service/wiki/download', function(token_pair) {
						imageSrc = this.getImageURL(this.properties.text, token_pair);
					}.bind(this));
				}
				if (this.properties.displayText) {
					iconLabel = this.properties.displayText;
				}
				break;
			case 'wysiwyg':
				if (this.content && this.content.trim() != '') {
					var hasNovelocityIdTag = ((this.content.indexOf('<novelocity:id=') > -1) && (this.content.indexOf('>\n') > -1));
					var hasNovelocityTag = (this.content.indexOf('<novelocity>\n') > -1);
					var hasNovelocityStartTag = (hasNovelocityTag || hasNovelocityIdTag);
					var hasNovelocityEndTag = (this.content.indexOf('\n</novelocity>') > -1);

					if (hasNovelocityStartTag && hasNovelocityEndTag){
						var contentStart;
						if (hasNovelocityIdTag) {
							contentStart = this.content.indexOf('>\n')+2;	// need to account for the 2 extra characters: the ">" and the "\n"
						} else {
							contentStart = '<novelocity>\n'.length;
						}
						detailedComponentHtml = this.content.substring(contentStart, this.content.length - ('\n</novelocity>'.length));
						detailedComponentStyle = 'overflow:auto';
						showIcon = false;
					}
				}
				break;
			case 'actiontask':
			case 'automation':
				if (this.properties.name && this.properties.name.trim() != '') {
					iconLabel = this.properties.name;
				}
				break;
			case 'result':
				if (this.properties.title && this.properties.title.trim() != '') {
					iconLabel = this.properties.title;
				}
				break;
			case 'wikidoc':
				if (this.content && this.content.trim() != '') {
					var wikiName = '', matches = /#includeForm\(['|"](.*?)['|"]\)/gi.exec(this.content);

					if (matches && matches.length > 1) {
						iconLabel = matches[1];
					}
				}
				break;
			case 'sociallink':
				if (this.properties.text && this.properties.text.trim() != '') {
					iconLabel = this.properties.text;
				}
				break;
			case 'image':
				if (this.properties.text && this.properties.text.trim() != '') {
					// make sure we have the wiki download csrf page token before retrieving the ImageURL
					clientVM.getCSRFToken_ForURI('/resolve/service/wiki/download', function(token_pair) {
						var imageUrl = this.getImageURL(this.properties.text, token_pair);
						if (imageUrl) {
							detailedComponentHtml = '<img src="'+imageUrl+'"/>';
							detailedComponentStyle = 'text-align:center';
							imageSrc = imageUrl;
							showIcon = false;
						}
					}.bind(this));
				}
				break;
			default:
				break;
		}

		this.set('imageSrc', imageSrc);
		this.set('iconLabel', iconLabel);
		this.set('showIcon', showIcon);
		this.set('imageSize', imageSize);
		this.set('detailedComponentHtml', detailedComponentHtml);
		this.set('detailedComponentStyle', detailedComponentStyle);

		// update after the view has render the image
		this.updateDetailedComponent(this.detailedComponentWidth, this.detailedComponentHeight);
	},

	setItemMetaData: function(type) {
		// var itemMetaData = {};
		if (type && type != 'new') {
			this.updateComponentLayout(type);
		}
		else {
			this.set('imageSrc', '/resolve/images/sections/components/fa-plus.png');
			this.set('iconLabel', this.localize('chooseComponent'));
			this.set('imageSize', 20);
		}
	},
	fullName$: function() {
		return this.name;
	},
	reallyRemoveComponent: function() {
		this.parentVM.removeComponent(this);
	},
	removeComponent: function() {
		if (this.type) {
			this.message({
				title: this.parentVM.localize('confirmRemoveComponentTitle'),
				msg: this.parentVM.localize('confirmRemoveComponentBody'),
				buttons: Ext.MessageBox.YESNO,
				buttonText: {
					yes: this.parentVM.localize('remove'),
					no: this.parentVM.localize('cancel')
				},
				fn: function(btn) {
					if (btn == 'yes') {
						this.reallyRemoveComponent();
					}
				},
				scope: this
			})
		}
		else {
			this.reallyRemoveComponent();
		}
	},
	setData: function(data) {
		if (this.loadData) this.loadData(Ext.applyIf(data, this.defaultData));
	},
	iconRendered: function(com) {
		if (!this.type || this.type == 'new') {
			var iconEl = com.getEl();
			// Add 'click' handler
			iconEl.on('click', this.launchComponentPicker, this);
			this['iconEl'] = iconEl;
			this.set('imageCursor', 'cursor: pointer');
		}
	},
	updateComponentType: function(type) {
		this.setItemMetaData(type);
		this.set('name', this.localize(type));
		this.set('type', type);
		// Remove 'click' handler
		this.iconEl.un('click', this.launchComponentPicker, this);
		this.set('imageCursor', 'cursor: default');

		//Open Editor after choose new component. Pass in the flag to indicate this component is edited for the 1st time.
		var isNew = true;
		this.editComponent(null,isNew);
	},
	updateContent: function(component, callback, callbackScope) {
		var type = this.type.toLowerCase();
		/*
		 if (type === 'automation') {
		 this.set('name', component.wikiName);
		 } else if (type === 'actiontask') {
		 this.set('name', component.name? component.name.substring(4): '');
		 }
		 */
		this.set('content', component.generateContent());
		this.set('properties', this.parseProperties(this.content));

		this.updateComponentLayout(type);

		//Check for dirty flag
		component.checkPageDirtyFlag();

		if (callback){
			callback.call(callbackScope);
		}
	},
	parseProperties: function(content) {
		var type = this.type;
		var configurationLines = (content || '').split('\n'),
			properties = {};
		var props = this.parentVM.extractComponentProperties(configurationLines);
		for (var j = 0; j < props.length; j++) {
			var split = props[j].split('=');

			if (split.length > 1) {
				properties[split[0]] = split.slice(1).join('=');
			} else  {
				properties['text'] = split[0];
			}
		}
		return properties;
	},
	getComponentContent : function(){
		return this.content;
	},
	componentEntryRender: function(v) {
		var me = this;
		v.body.el.on('dblclick', function(e) {
			me.editComponent(e, false);
		});
	}
});