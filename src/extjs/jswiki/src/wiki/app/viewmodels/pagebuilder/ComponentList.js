glu.defModel('RS.wiki.pagebuilder.Base', {
	originalContent : '',
	defaultData : {},
	removeComponent: function(btn) {
		if (btn === 'yes') {
			this.parentVM.types.splice(this.parentVM.typeConfigurations.indexOf(this) - 1, 1)
			this.parentVM.typeConfigurations.remove(this)
			this.parentVM.configureDefaultColumnWidths()
		}
	},
	checkPageDirtyFlag : function(){
		if (Ext.isFunction(this.rootVM.checkPageDirtyFlag))
			this.rootVM.checkPageDirtyFlag();
	},
	initMixin : function(){
		if(this.viewmodelName != "XTable" && this.viewmodelName != "Result" && this.viewmodelName != "Detail" && this.viewmodelName != "Table")
			this.set('originalContent', this.generateContent());
	},
	resetData : function(){
		for(var propName in this.defaultData){
			this.set(propName, this.defaultData[propName]);
		}
	}
})

glu.defModel('RS.wiki.pagebuilder.ResolveMarkup', {
	mixins: ['Base'],
	content: '',
	defaultData : {
		content : ''
	},
	generateContent: function() {
		return this.content
	}
})

glu.defModel('RS.wiki.pagebuilder.TextEditor', {
	mixins: ['Base'],
	content : '',
	novelocityId : '',
	defaultData : {
		content : ''
	},
	init: function() {
		var hasNovelocityIdTag = ((this.content.indexOf('<novelocity:id=') > -1) && (this.content.indexOf('>\n') > -1));
		var hasNovelocityTag = (this.content.indexOf('<novelocity>\n') > -1);
		var hasNovelocityStartTag = (hasNovelocityTag || hasNovelocityIdTag);
		var hasNovelocityEndTag = (this.content.indexOf('\n</novelocity>') > -1);

		if (hasNovelocityStartTag && hasNovelocityEndTag){
			var id;
			var contentStart;
			if (hasNovelocityIdTag) {
				var idStart = ('<novelocity:id='.length);
				var idEnd = this.content.indexOf('>\n');
				id = this.content.substring(idStart, idEnd);
				contentStart = this.content.indexOf('>\n')+2;	// need to account for the 2 extra characters: the ">" and the "\n"
			} else {
				id = Ext.data.IdGenerator.get('uuid').generate();
				contentStart = '<novelocity>\n'.length;
			}
			this.set('novelocityId', id);
			this.set('content', this.content.substring(contentStart, this.content.length - ('\n</novelocity>'.length)));
			this.set('originalContent', this.generateContent());
		}

		if (!this.novelocityId) {
			this.set('novelocityId', Ext.data.IdGenerator.get('uuid').generate());
		}
	},
	generateContent: function() {
		//if (this.content.trim() == "") {
		//	return "";
		//}
		if (this.novelocityId) {
			return '<novelocity:id='+this.novelocityId+'>\n' + this.content + '\n</novelocity>';
		} else {
			return '<novelocity>\n' + this.content + '\n</novelocity>';
		}
	}
})

glu.defModel('RS.wiki.pagebuilder.HTML', {
	mixins: ['Base'],
	content: '',
	defaultData : {
		content : ''
	},
	generateContent: function() {
		return this.content
	}
})

glu.defModel('RS.wiki.pagebuilder.Groovy', {
	mixins: ['Base'],
	content: '',
	defaultData : {
		content : ''
	},
	init: function() {
		this.set('content', this.content.substring('{pre}\n<%\n'.length, this.content.length - ('\n%>\n{pre}'.length)))
	},
	generateContent: function() {
		return '{pre}\n<%\n' + this.content + '\n%>\n{pre}'
	}
})

glu.defModel('RS.wiki.pagebuilder.Javascript', {
	mixins: ['Base'],
	content: '',
	defaultData : {
		content : ''
	},
	init: function() {
		this.set('content', this.content.substring('{pre}\n<script type="text/javascript">\n'.length, this.content.length - ('\n</script>\n{pre}'.length)))
	},
	generateContent: function() {
		return '{pre}\n<script type="text/javascript">\n' + this.content + '\n</script>\n{pre}'
	}
})

glu.defModel('RS.wiki.pagebuilder.CodeSample', {
	mixins: ['Base'],
	content: '',
	type: 'text',
	showLineNumbers: true,
	typeStore: {
		mtype: 'store',
		sorters: ['name'],
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	defaultData : {
		type: 'text',
		showLineNumbers: true,
		content: '',
	},
	init: function() {
		this.parseCodeContent()
		this.typeStore.loadData([{
			name: this.localize('text'),
			value: 'text'
		}, {
			name: this.localize('javascript'),
			value: 'javascript'
		}, {
			name: this.localize('html'),
			value: 'html'
		}, {
			name: this.localize('groovy'),
			value: 'groovy'
		}, {
			name: this.localize('sql'),
			value: 'sql'
		}, {
			name: this.localize('xml'),
			value: 'xml'
		}, {
			name: this.localize('java'),
			value: 'java'
		}, {
			name: this.localize('css'),
			value: 'css'
		}])
	},
	parseCodeContent: function() {
		var codeContent = [],
			codeContents = (this.content || '').split('\n');
		Ext.Array.forEach(codeContents, function(line) {
			if (line.indexOf('{code') == -1) codeContent.push(line)
		})
		this.set('content', codeContent.join('\n'))
	},
	generateContent: function() {
		return '{code:type=' + this.type + (this.showLineNumbers == false ? '|showLineNumbers=false' : '') + '}\n' + this.content + '\n{code}'
	}
})

glu.defModel('RS.wiki.pagebuilder.Image', {
	mixins: ['Base'],
	content : '',
	src: '',
	zoom: false,
	width: '',
	height: 'auto',
	defaultData : {
		content : '',
		src: '',
		zoom: false,
		width: '',
		height: 'auto',
	},
	displayMode: '',

	widthVal$: function() {
		if (this.width > -1) return this.width
		return null
	},
	heightVal$: function() {
		/*if (this.height > -1) return this.height
		return null*/
		this.set('height', '')
		return ''
	},

	attach: function() {
		this.open({
			mtype: 'RS.wiki.pagebuilder.ImageManager',
			name: this.rootVM.ufullname
		})
	},

	generateContent: function() {
		var content = '{image:' + this.src;
		if (this.width) content += '|width=' + this.width
		if (this.width && this.displayMode === 'percentage') content += '%'
		//if (this.height) content += '|height=' + this.height
		if (this.height) content += '|height=auto'
		if (this.zoom) content += '|zoom=true'
		content += '}'
		return content;
	},

	displayModeChange: function(newVal) {
		this.set('displayMode', newVal);
	},

	assignDisplayMode: function(radiogroup) {
		if (this.width && this.width.slice(-1) === '%') {
			this.set('width', this.width.slice(0, -1));
			radiogroup.setValue({displayMode: 'percentage'})
		} else {
			radiogroup.setValue({displayMode: 'pixels'})
		}
	},

	when_properties_change: {
		on: ['widthChanged'],
		action: function() {
			if (isNaN(this.width)) {
				this.parentVM.disableUpdate();
			} else {
				this.parentVM.enableUpdate();
			}
		}
	},

	init: function() {
	}
})

glu.defModel('RS.wiki.pagebuilder.Form', {
	mixins: ['Base'],
	name: '',
	wikiName: '',
	tooltip: '',
	popout: false,
	popoutHeight: -1,
	title: '',
	collapsed: false,
	popoutWidth: -1,
	defaultData : {
		name: '',
		wikiName: '',
		tooltip: '',
		popout: false,
		popoutHeight: -1,
		title: '',
		collapsed: false,
		popoutWidth: -1,
	},
	when_properties_change: {
		on: ['popoutHeightChanged', 'popoutWidthChanged'],
		action: function() {
			if (isNaN(this.popoutHeight) || isNaN(this.popoutWidth)) {
				this.parentVM.disableUpdate();
			} else {
				this.parentVM.enableUpdate();
			}
		}
	},
	when_popout_is_true_then_collapsed_must_be_false: {
		on: ['popoutChanged'],
		action: function() {
			if (this.popout) this.set('collapsed', false)
		}
	},
	popoutHeightIsEnabled$: function() {
		return this.popout
	},
	popoutWidthIsEnabled$: function() {
		return this.popout
	},
	collapsedIsEnabled$: function() {
		return !this.popout
	},
	chooseForm: function() {
		this.open({
			mtype: 'RS.formbuilder.FormPicker'
		})
	},
	createForm: function() {
		this.open({
			mtype: 'RS.wiki.FormWizard'
		})
	},
	editForm: function() {
		clientVM.handleNavigation({
			modelName: 'RS.formbuilder.Main',
			target: '_blank',
			params: {
				name: this.name
			}
		})
	},
	generateContent: function() {
		var content = '{form:name=' + this.name;
		if (this.wikiName) content += '|wikiName=' + this.wikiName;
		if (this.popout) content += '|popout=true'
		if (this.collapsed) content += '|collapsed=true'
		if (this.title && this.title != this.localize('untitledSection')) content += '|title=' + this.title
		if (this.tooltip) content += '|tooltip=' + this.tooltip
		if (this.popoutHeight > -1) content += '|popoutHeight=' + this.popoutHeight
		if (this.popoutWidth > -1) content += '|popoutWidth=' + this.popoutWidth
		content += '}'
		return content
	}
})

glu.defModel('RS.wiki.pagebuilder.Secure', {
	mixins: ['Base'],
	key: '',
	value: '',
	defaultData : {
		key: '',
		value: ''
	},
	chooseKey: function() {
		var me = this;
		this.open({
			mtype: 'RS.actiontask.PropertyDefinitionsPicker',
			dumper: function(selections) {
				me.set('key', selections[0].get('uname'))
			}
		})
	},
	chooseValue: function() {
		var me = this;
		this.open({
			mtype: 'RS.actiontask.PropertyDefinitionsPicker',
			dumper: function(selections) {
				me.set('value', selections[0].get('uname'))
			}
		})
	},
	generateContent: function() {
		return '{encrypt:key=' + this.key + '|value=' + this.value + '}'
	}
})

glu.defModel('RS.wiki.pagebuilder.Infobar', {
	mixins: ['Base'],
	height: 400,
	social: false,
	feedback: true,
	rating: true,
	attachments: true,
	history: true,
	tags: true,
	pageInfo: true,
	defaultData : {
		height: 400,
		social: false,
		feedback: true,
		rating: true,
		attachments: true,
		history: true,
		tags: true,
		pageInfo: true,
	},
	when_properties_change: {
		on: ['heightChanged'],
		action: function() {
			if (isNaN(this.height)) {
				this.parentVM.disableUpdate();
			} else {
				this.parentVM.enableUpdate();
			}
		}
	},
	generateContent: function() {
		return '{infobar:social=' + this.social + '|feedback=' + this.feedback + '|rating=' + this.rating + '|attachments=' + this.attachments + '|history=' + this.history + '|tags=' + this.tags + '|pageInfo=' + this.pageInfo + '|height=' + this.height + '}'
	}
})

glu.defModel('RS.wiki.pagebuilder.Model', {
	mixins: ['Base'],
	wiki: '',
	refreshInterval: 5,
	refreshCountMax: 60,
	status: '',
	zoom: '',
	height: -1,
	width: -1,
	statusStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	defaultData : {
		wiki: '',
		refreshInterval: 5,
		refreshCountMax: 60,
		status: 'CONDITION',
		zoom: '',
		height: -1,
		width: -1,
	},
	when_properties_change: {
		on: ['refreshIntervalChanged', 'refreshCountMaxChanged', 'zoomChanged', 'heightChanged', 'widthChanged'],
		action: function() {
			if (isNaN(this.refreshInterval) || isNaN(this.refreshCountMax) || isNaN(this.zoom) || isNaN(this.height) || isNaN(this.width)) {
				this.parentVM.disableUpdate();
			} else {
				this.parentVM.enableUpdate();
			}
		}
	},
	init: function() {
		this.statusStore.loadData([{
			name: this.localize('severity'),
			value: 'SEVERITY'
		}, {
			name: this.localize('condition'),
			value: 'CONDITION'
		}]);
		this.set('status', 'CONDITION');

		if (this.refreshInterval < 5) {
			this.set('refreshInterval', 5);
		}

		if (this.refreshCountMax < 1) {
			this.set('refreshCountMax', 1);
		}

	},
	chooseDocument: function() {
		var me = this;
		me.open({
			mtype: 'RS.wiki.RunbookPicker',
			dumper: function(selection) {
				if (selection.length) {
					me.set('wiki', selection[0].get('ufullname'))
				} else {
					me.set('wiki', selection.get('ufullname'))
				}
			}
		})
	},
	generateContent: function() {
		var content = []
		if (this.wiki) content.push('wiki=' + this.wiki)
		if (this.status) content.push('status=' + this.status)
		if (this.zoom) content.push('zoom=' + this.zoom)
		if (Ext.isString(this.width) || this.width > -1) content.push('width=' + this.width)
		if (Ext.isString(this.height) || this.height > -1) content.push('height=' + this.height)

		if (this.refreshInterval < 5) {
			this.refreshInterval = 5;
		}

		content.push('refreshInterval=' + this.refreshInterval);

		if (this.refreshCountMax < 1) {
			this.refreshCountMax = 1;
		}

		content.push('refreshCountMax=' + this.refreshCountMax);

		if (content.length == 0) content = '{model'
		else content = '{model:' + content.join('|')
		content += '}\n{model}'
		return content
	}
})

glu.defModel('RS.wiki.pagebuilder.Page', {
	mixins: ['Base'],
	wiki: '',
	defaultData : {
		wiki: ''
	},
	chooseWikiDocument: function() {
		var me = this;
		var currentDocument = this.rootVM.fullName;
		this.open({
			mtype: 'RS.wiki.RunbookPicker',
			runbookOnly: false,
			title: this.localize('selectWiki'),
			currentDocument : currentDocument,
			createActionText: this.localize('createWiki'),
			dumper: function(selection) {
				if (selection.length) {
					me.set('wiki', selection[0].get('ufullname'))
				} else {
					me.set('wiki', selection.get('ufullname'))
				}
			}
		})
	},
	generateContent: function() {
		return '#includeForm("' + this.wiki + '")';
	},
	jumpToWiki: function() {
		if (!this.wiki)
			return;
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			target: '_blank',
			params: {
				name: this.wiki
			}
		});
	}
});

glu.defModel('RS.wiki.pagebuilder.Result', {
	mixins: ['Base'],
	content: '',
	actionTasks: {
		mtype: 'treestore',
		fields: ['id', 'description', 'descriptionWikiLink', 'namespace', 'nodeId', 'resultWikiLink', 'task', 'wiki', 'actionTask', 'tags', {
			name: 'autoCollapse',
			type: 'boolean'
		}],
		proxy: {
			type: 'memory'
		}
	},
	actionTasksColumns: [{
		xtype: 'treecolumn',
		dataIndex: 'description',
		text: '~~description~~',
		flex: 1,
		renderer: function(value, meta, record) {
			var markup = '';
			var task = record.get('task');
			var tags = record.get('tags');

			if (value && task) {
				markup = value;
				markup += '&nbsp;&nbsp;&nbsp;';
				markup += '<span class="rs-accent">';
				markup += 'Task: ' + record.get('task');
				var namespace = record.get('namespace');

				if (namespace) {
					markup += '&nbsp;&nbsp;&nbsp;';
					markup += 'Namespace: ';
					markup += namespace;
				}

				var wiki = record.get('wiki');

				if (wiki) {
					markup += 'Wiki: ';
					markup += '&nbsp;&nbsp;&nbsp;';
					markup += wiki;
				}

				markup += '</span>';
			} else if (tags) {
				tags = tags.split(',');

				if (tags.length > 0) {
					markup = "#";
					markup += tags.join(' #');
				}
			} else {
				markup = value || record.get('task');
			}

			return markup;
		}
	}],
	actionTasksSelections: [],
	title: '',
	encodeSummary: false,
	refreshInterval: 5,
	refreshCountMax: 60,
	descriptionWidth: 0,
	order: 'DESC',
	autoCollapse: false,
	collapsed: false,
	autoHide: false,
	progress: true,
	selectAll: true,
	includeStartEnd: true,
	preserveTaskOrder: true,
	showWiki: 'any',
	filter: '',
	filterValue: [],
	filterStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	orderStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	defaultData : {
		title: '',
		encodeSummary: false,
		refreshInterval: 5,
		refreshCountMax: 60,
		descriptionWidth: 0,
		order: 'DESC',
		autoCollapse: false,
		collapsed: false,
		autoHide: false,
		progress: true,
		selectAll: true,
		includeStartEnd: true,
		preserveTaskOrder: true,
		showWiki: 'any',
		filterValue: [],
	},
	reqCount: 0,
	init: function() {
		this.filterStore.loadData([{
			name: this.localize('GOOD'),
			value: 'GOOD'
		}, {
			name: this.localize('WARNING'),
			value: 'WARNING'
		}, {
			name: this.localize('SEVERE'),
			value: 'SEVERE'
		}, {
			name: this.localize('CRITICAL'),
			value: 'CRITICAL'
		}])

		this.orderStore.loadData([{
			name: this.localize('ASC'),
			value: 'ASC'
		}, {
			name: this.localize('DESC'),
			value: 'DESC'
		}])

		this.set('filterValue', this.filter.split(','));
		for (var i=0, l=this.filterValue.length; i<l; i++) {
			this.filterValue[i] = this.filterValue[i].trim();
		}

		this.actionTasks.setRootNode({
			id: 'root'
		})

		if (this.refreshInterval < 5) {
			this.refreshInterval = 5;
		}

		if (this.refreshCountMax < 1) {
			this.refreshCountMax = 1;
		}

		//COMPATIBILITY : parse action tasks from content. Make sure task summary (which will be used as description) is in 1 line.
		this.content = this.content.replace(/([\s\S]*)({description=[^{]*})([\s\S]*)/g, function(v,g1,g2,g3){
			return g1 + g2.replace(/\n/g, ' ') + g3;
		});
		var content = [];
		var	contents = (this.content || '').split('\n');

		//Remove first and last containing {result2} tags
		contents.splice(contents.length - 1, 1)
		contents.splice(0, 1);

		//Parse this actiontask list.
		var processedActionTasks = processActionTasks(contents);

		Ext.Array.forEach(processedActionTasks, function(at) {
			this.actionTasks.getRootNode().appendChild(at)
		}, this)

		this.set('originalContent', this.generateContent());

		this.set('reqCount', 0);
		this.runbookStore.on('beforeload', function(store, op) {
			op.params = op.params || {}
			Ext.apply(op.params, {
				filter: Ext.encode([{
					field: 'ufullname',
					condition: 'contains',
					type: 'auto',
					value: op.params.query || this.showWikiName
				}])
			})
			this.set('reqCount', this.reqCount + 1);
		}, this);
		this.runbookStore.on('load', function() {
			this.set('reqCount', this.reqCount - 1);
		}, this);

		this.validCustomWikiName();
	},
	generateContent: function() {
		var actionTasks = [],
			tags = [];
		this.actionTasks.getRootNode().eachChild(function(child) {
			actionTasks = actionTasks.concat(this.serializeAT(child))
		}, this)
		var showWikiOption = '';
		if (this.showWiki == 'current') {
			showWikiOption += '|showWiki=' + this.rootVM.ufullname;
		} else if (this.showWikiName && this.showWiki == 'custom') {
			showWikiOption += '|showWiki=' + this.showWikiName;
		}
		return '{result2:title=' + this.title + '|encodeSummary=' + this.encodeSummary + '|refreshInterval=' + this.refreshInterval + '|refreshCountMax=' + this.refreshCountMax + '|descriptionWidth=' + this.descriptionWidth + '|order=' + this.order + '|autoCollapse=' + this.autoCollapse + '|collapsed=' + this.collapsed + '|autoHide=' + this.autoHide + '|filter=' + (this.filterValue || '') + '|progress=' + this.progress + '|selectAll=' + this.selectAll + '|includeStartEnd=' + this.includeStartEnd + '|preserveTaskOrder=' + this.preserveTaskOrder + showWikiOption + '}\n' + actionTasks.join('\n') + '\n{result2}';
	},
	resetData : function(){
		for(var propName in this.defaultData){
			this.set(propName, this.defaultData[propName]);
		}
		this.actionTasks.getRootNode().removeAll();
	},
	serializeAT: function(node) {
		var ats = [],
			data = Ext.clone(node.data),
			ser = [],
			split = data.actionTask.split('#');

		data.task = split[0]
		data.namespace = split[1]

		Ext.Object.each(data, function(key, value) {
			if (Ext.Array.indexOf(['description', 'descriptionWikiLink', 'namespace', 'nodeId', 'resultWikiLink', 'task', 'wiki', 'autoCollapse', 'tags'], key) > -1 && value)
				ser.push(Ext.String.format('{0}={1}', key, value))
		})

		if (!node.isLeaf()) { //node.childNodes && node.childNodes.length > 0) {
			ats.push('{group:title=' + node.get('description') + (!node.get('autoCollapse') ? '|autoCollapse=' + node.get('autoCollapse') : '') + '}')
			node.eachChild(function(child) {
				ats = ats.concat(this.serializeAT(child))
			}, this)
			ats.push('{group}')
		} else {
			ats.push('{' + ser.join('|') + '}')
		}

		return ats
	},

	itemdblclick: function(record, item, index, e, eOpts) {
		this.open(Ext.applyIf({
			mtype: 'RS.wiki.pagebuilder.TaskEdit'
		}, record.data))
	},

	selectionchange: function(records) {
		this.set('actionTasksSelections', records)
	},

	edit: function() {
		this.open(Ext.applyIf({
			mtype: 'RS.wiki.pagebuilder.TaskEdit'
		}, this.actionTasksSelections[0].data))
	},
	editIsEnabled$: function() {
		return this.actionTasksSelections.length == 1
	},

	addTag: function() {
		this.open({
			mtype: 'RS.catalog.TagPicker',
			callback: 'addTags'
		})
	},
	addTags: function(newTags) {
		var tags = [];
		Ext.Array.forEach(newTags, function(tag) {
			tags.push(tag.data['name'])
		})
		if (this.actionTasksSelections.length > 0 && !this.actionTasksSelections[0].isLeaf()) {
			this.actionTasksSelections[0].appendChild({
				leaf: true,
				tags: tags.join(',')
			})
		} else {
			this.actionTasks.getRootNode().appendChild({
				leaf: true,
				tags: tags.join(',')
			})
		}
	},

	addActionTask: function() {
		var currentDocName = this.rootVM.fullName;
		this.open({
			mtype: 'RS.actiontask.ActionTaskPicker',
			multiSelection : true,
			currentDocName : currentDocName,
			dumper: {
				scope: this,
				dump: this.reallyAddActionTask
			}
		})
	},
	reallyAddActionTask: function(selections) {
		//Parse summary txt to make it 1 line description.
		Ext.Array.each(selections,function(selection){
			var rawTaskSummary = selection.get('usummary');
			var parsedSummary = rawTaskSummary ? rawTaskSummary.replace(/\n/g, '. ') : '';
			if (this.actionTasksSelections.length > 0 && !this.actionTasksSelections[0].isLeaf()) {
				this.actionTasksSelections[0].appendChild({
					leaf: true,
					actionTask: selection.get('uname') + '#' + selection.get('unamespace'),
					description: parsedSummary,
					task: selection.get('uname'),
					namespace: selection.get('unamespace')
				})
			} else {
				this.actionTasks.getRootNode().appendChild({
					leaf: true,
					actionTask: selection.get('uname') + '#' + selection.get('unamespace'),
					description: parsedSummary,
					task: selection.get('uname'),
					namespace: selection.get('unamespace')
				})
			}
		},this)
	},
	addGroup: function() {
		if (this.actionTasksSelections.length > 0 && !this.actionTasksSelections[0].isLeaf()) {
			this.actionTasksSelections[0].appendChild({
				description: this.localize('newGroup'),
				expanded: true
			})
		} else {
			this.actionTasks.getRootNode().appendChild({
				description: this.localize('newGroup'),
				expanded: true
			})
		}
	},
	remove: function() {
		Ext.each(this.actionTasksSelections, function(t) {
			t.remove();
		}, this);
	},
	removeIsEnabled$: function() {
		return this.actionTasksSelections.length > 0
	},

	showAnyWikiClicked: function() {
		this.set('showWiki', 'any');
	},
	showCurrentWikiOnlyClicked: function() {
		this.set('showWiki', 'current');
	},
	showCustomWikiOnlyClicked: function() {
		this.set('showWiki', 'custom');
	},

	showAnyWiki$: function() {
		return this.showWiki == 'any';
	},
	showCurrentWikiOnly$: function() {
		return this.showWiki == 'current';
	},
	showCustomWikiOnly$: function() {
		return this.showWiki == 'custom';
	},

	validCustomWikiName: function() {
		if (this.showWiki == 'custom' && !this.showWikiName) {
			this.parentVM.disableUpdate();
		} else {
			this.parentVM.enableUpdate();
		}
	},

	when_showWiki_change: {
		on: ['showWikiChanged'],
		action: function() {
			this.validCustomWikiName();
		}
	},

	when_showWikiName_change: {
		on: ['showWikiNameChanged'],
		action: function() {
			this.validCustomWikiName();
		}
	},

	when_properties_change: {
		on: ['refreshIntervalChanged', 'refreshCountMaxChanged', 'descriptionWidthChanged'],
		action: function() {
			if (isNaN(this.refreshInterval) || isNaN(this.refreshCountMax) || isNaN(this.descriptionWidth)) {
				this.parentVM.disableUpdate();
			} else {
				this.parentVM.enableUpdate();
			}
		}
	},

	runbookStore: {
		mtype: 'store',
		fields: ['ufullname'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wikiadmin/listRunbooks',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	runbook: '',
	showWikiName: '',
	searchRunbook: function() {
		var me = this;
		this.open({
			mtype: 'RS.wiki.RunbookPicker',
			dumper: {
				dump: function(selections) {
					if (selections.length > 0) {
						var selection = selections[0]
						me.set('runbook', Ext.apply({}, selection.raw));
						me.set('showWikiName', me.runbook.ufullname);
					}

					me.runbookStore.load();
				}
			}
		})
	},
	runbookSelected: function(selections) {
		if (selections.length == 0)
			return;
		var r = selections[0];
		this.set('showWikiName', r.get('ufullname'));
	}
})

glu.defModel('RS.wiki.pagebuilder.Detail', {
	mixins: ['Base', 'Result'],
	progress: false,
	actionTasksColumns: [{
		xtype: 'treecolumn',
		dataIndex: 'description',
		text: '~~description~~',
		flex: 2,
		editor: {}
	}, {
		dataIndex: 'actionTask',
		text: '~~actionTask~~',
		flex: 1,
		editor: {}
	}, {
		dataIndex: 'wiki',
		text: '~~wiki~~',
		flex: 1,
		editor: {}
	}, {
		dataIndex: 'nodeId',
		text: '~~nodeId~~',
		flex: 1,
		editor: {}
	}],
	maximum: '',
	offset: '',

	when_properties_change: {
		on: ['refreshIntervalChanged', 'refreshCountMaxChanged', 'descriptionWidthChanged', 'maximumChanged', 'offsetChanged'],
		action: function() {
			if (isNaN(this.refreshInterval) || isNaN(this.refreshCountMax) || isNaN(this.descriptionWidth) || isNaN(this.maximum) || isNaN(this.offset)) {
				this.parentVM.disableUpdate();
			} else {
				this.parentVM.enableUpdate();
			}
		}
	},

	generateContent: function() {
		var actionTasks = [];
		this.actionTasks.getRootNode().eachChild(function(child) {
			actionTasks = actionTasks.concat(this.serializeAT(child))
		}, this)

		return '{detail:progress='+this.progress+'|max='+this.maximum+'|offset='+this.offset+'|refreshInterval=' + this.refreshInterval + '|refreshCountMax=' + this.refreshCountMax + '}\n'+actionTasks.join('\n')+'\n{detail}';
	}
})

glu.defModel('RS.wiki.pagebuilder.Automation', {
	mixins: ['Base', 'Form'],
	assignNameFirstTime: true,
	isNew: false,
	when_wiki_name_changed: {
		on: ['wikiNameChanged'],
		action: function() {
			if (!!this.wikiName && this.assignNameFirstTime) {
				if (!this.isNew)
					this.ajax({
						url: '/resolve/service/wikiadmin/listABTasks',
						params: {
							name: this.wikiName
						},
						success: function(resp) {
							var respData = RS.common.parsePayload(resp);
							if (!respData.success) {
								clientVM.displayError(this.localize('getABTaskError', respData.message));
								return;
							}
							var lastIdx = 0
							var me = this;
							this.rootVM.sections.forEach(function(sec, idx) {
								sec.typeConfigurations.forEach(function(t) {
									if (t == me)
										lastIdx = idx;
								})
							});
							/*
							var sec = this.rootVM.reallyAddSection({
								types: ['result']
							}, lastIdx + 1);
							*/
							var result = sec.typeConfigurations.getAt(1);
							Ext.each(respData.records, function(r) {
								result.actionTasks.getRootNode().appendChild({
									leaf: true,
									actionTask: r['uname'] + '#' + r['unamespace'],
									description: r['usummary'],
									task: r['uname'],
									namespace: r['unamespace']
								})
							});
						},
						failure: function(resp) {
							clientVM.displayError(this.localize('serverError', resp.staus));
						}
					});
				else {
					/*
					this.rootVM.reallyAddSection({
						types: ['result']
					});
					*/
				}
				this.set('assignNameFirstTime', false);
			}
		}
	},
	displayName$: function(selection) {
		return this.name ? this.name.substring(4) : '' //Pull off RBF_
	},
	chooseRunbook: function() {
		var me = this;
		this.open({
			mtype: 'RS.wiki.RunbookPicker',
			builderOnly: true,
			runbookOnly: false,
			dumper: function(selectedRunbook) {
				if (selectedRunbook.length) {
					me.set('name', 'ABF_' + selectedRunbook[0].get('ufullname').replace(/[\.| |-]/g, '_').toUpperCase());
					me.set('isNew', !selectedRunbook[0].get('uresolutionBuilderId'));
					me.set('wikiName', selectedRunbook[0].get('ufullname'));
				} else {
					me.set('name', 'ABF_' + selectedRunbook.get('ufullname').replace(/[\.| |-]/g, '_').toUpperCase());
					me.set('isNew', !selectedRunbook.get('uresolutionBuilderId'));
					me.set('wikiName', selectedRunbook.get('ufullname'));
				}
			}
		});
	},
	editAutomation: function() {
		if (!this.wikiName)
			return;
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			params: {
				name: this.wikiName,
				activeTab: 2
			},
			target: '_blank'
		});
	}
});

glu.defModel('RS.wiki.pagebuilder.ActionTask', {
	mixins: ['Base', 'Form'],

	displayName$: function() {
		return this.name ? this.name.substring(4) : '' //Pull off ATF_
	},
	chooseForm: function() {
		this.open({
			mtype: 'RS.formbuilder.FormPicker',
			formPickerTitle: this.localize('selectActionTaskForm'),
			showCreate: true,
			filter: [{
				field: 'uformName',
				condition: 'startsWith',
				value: 'atf_'
			}]
		})
	},
	createForm: function() {
		var me = this;
		var currentDocName = this.rootVM.fullName;
		this.open({
			mtype: 'RS.actiontask.ActionTaskPicker',
			dumper: function(selections) {
				if (selections.length) {
					me.set('name', 'ATF_' + selections[0].get('unamespace').replace(/\./gi, '_').replace(/ /gi, '_').toUpperCase() + '_' + selections[0].get('uname').replace(/\./gi, '_').replace(/ /gi, '_').toUpperCase())
					clientVM.handleNavigation({
						modelName: 'RS.formbuilder.Main',
						params: {
							name: me.name,
							create: true,
							atId: selections[0].get('id')
						},
						target: '_blank'
					})
				}
			}
		})
	}
})

glu.defModel('RS.wiki.pagebuilder.Table', {
	mixins: ['Base'],
	columns: [],
	rows: [],
	table: null,
	tableColumns: [],
	tableSelections: [],
	defaultData : {
		columns: [],
		rows: [],
		tableColumns: [],
		tableSelections: []
	},
	init: function() {
		var contents = (this.content || '').split('\n'),
			split, row, rows = [];

		//Remove first and last containing {table} tags
		contents.splice(contents.length - 1, 1)
		contents.splice(0, 1)

		Ext.Array.forEach(contents, function(record, index) {
			//split = record.split('|')


			// replace all "\|" with innocuous white space (\u000B) to not allow the array split to
			// occur in cell data with escaped | characters
			split = record.replace(/\\\|/g, '\u000B').split('|')

			// revert all white space back to \|
			for (var i = 0; i < split.length; i++) {
				split[i] = split[i].replace(/\u000B/g, '\|');

			};



			row = {}
			Ext.Array.forEach(split, function(c, idx) {
				if (index == 0) this.columns.push(Ext.String.trim(c))
				else {
					row[this.columns[idx]] = Ext.String.trim(c)
				}
			}, this)
			if (index > 0)
				rows.push(row);
		}, this)

		this.configureStore();
		this.configureColumns();
		this.populateRowsIntoStore(rows);
		this.set('originalContent', this.generateContent());
	},

	columnMove: function(movedColumn, fromIdx, toIdx){
		var movedColumnHeaderName = this.columns[fromIdx];
		var newColumns = this.columns.slice(0);
		newColumns.splice(fromIdx, 1);
		newColumns.splice(toIdx,0, movedColumnHeaderName);
		this.set('columns', newColumns);
	},

	configureColumns: function() {
		var cols = []
		Ext.Array.forEach(this.columns, function(col) {
			cols.push({
				dataIndex: col,
				text: col,
				flex: 1,
				editor: {}
			})
		})
		this.set('tableColumns', cols)
	},

	configureStore: function() {
		this.set('table', Ext.create('Ext.data.Store', {
			fields: this.columns,
			proxy: {
				type: 'memory'
			}
		}))
	},

	populateRowsIntoStore : function(newRows){
		this.table.loadData(newRows);
	},
	extractRowsFromStore : function(){
		var tableData = this.table.data;
		var rows = [];
		for(var i = 0; i < tableData.length; i++){
			rows.push(tableData.getAt(i).data);
		}
		return rows;
	},
	addColumn: function() {
		this.open({
			mtype: 'RS.wiki.pagebuilder.ColumnAdder'
		})
	},
	removeColumn: function() {
		this.open({
			mtype: 'RS.wiki.pagebuilder.ColumnPicker'
		})
	},

	addRow: function() {
		this.table.add({})
	},
	insertRow: function() {
		this.table.insert(this.table.indexOf(this.tableSelections[0]), {})
	},
	insertRowIsEnabled$: function() {
		return this.tableSelections.length > 0
	},
	removeRow: function() {
		this.table.remove(this.tableSelections)
	},
	removeRowIsEnabled$: function() {
		return this.tableSelections.length > 0
	},

	// Generates the table content when saving to the DB
	generateContent: function() {
		var output = ['{table}'];

		output.push(this.columns.join(' | '))

		this.table.each(function(row) {
			var r = [];
			Ext.Array.forEach(this.columns, function(col) {
				var rowSanitized = row.get(col);
				rowSanitized = rowSanitized.replace(/\|/g, "\\|");
				r.push(rowSanitized)
			})
			output.push(r.join(' | '))
		}, this)


		output.push('{table}')
		return output.join('\n')
	}
})

glu.defModel('RS.wiki.pagebuilder.XTable', {
	mixins: ['Base'],

	columns: [],
	rows: [],
	table: null,
	tableColumns: [],
	tableSelections: [],
	defaultData : {
		columns: [],
		rows: [],
		tableColumns: [],
		tableSelections: []
	},
	init: function() {
		var contents = (this.content || '').split('\n'),
			split, row;

		//Remove first and last containing {table} tags
		contents.splice(contents.length - 1, 1)
		contents.splice(0, 1)

		Ext.Array.forEach(contents, function(record, index) {
			split = record.split('|')
			row = {}
			Ext.Array.forEach(split, function(c, idx) {
				if (index == 0) {
					var colSplit = Ext.String.trim(c).split(':'),
						col = {
							text: colSplit[0],
							width: 0,
							type: '',
							dataFormat: '',
							format: '',
							dataIndex: '',
							sortable: false,
							draggable: false
						};
					var config = Ext.Object.fromQueryString(colSplit[1] || '');
					config.renderer = function(value, meta, record) {
						if (!meta.column.link)
							return value;
						var target = meta.column.linkTarget || '_blank'
						var link = new Ext.Template(meta.column.link).compile().apply(record.data);
						return '<a href="' + link + '" target="' + target + '">' + value + '</a>';
					}
					Ext.apply(col, config);
					if (col.width) col.width = Number(col.width)
					this.columns.push(col)
				} else {
					row[this.columns[idx].dataIndex || this.columns[idx].text] = Ext.String.trim(c)
				}
			}, this)
			if (index > 0) this.rows.push(row)
		}, this)

		this.configureStore();
		this.configureColumns();
		this.set('originalContent', this.generateContent());
	},

	configureColumns: function() {
		var cols = []
		Ext.Array.forEach(this.columns, function(col) {
			var c = {
				dataIndex: col.dataIndex || col.text,
				text: col.text,
				flex: 1,
				type: col.type,
				format: col.dataFormat,
				editor: {},
				renderer: col.renderer
			}
			if (c.type == 'date') {
				c.renderer = Ext.util.Format.dateRenderer(col.format || Ext.state.Manager.get('userDefaultDateFormat', 'Y-m-d G:i:s'))
				c.editor = {
					xtype: 'datefield'
				}
			}
			cols.push(c)
		})
		this.set('tableColumns', cols)
	},

	configureStore: function() {
		var fields = [];
		Ext.Array.forEach(this.columns, function(col) {
			fields.push({
				name: col.dataIndex || col.text,
				type: col.type,
				format: col.dataFormat,
				dateFormat: col.dataFormat
			})
		})
		this.set('table', Ext.create('Ext.data.Store', {
			fields: fields,
			proxy: {
				type: 'memory'
			}
		}))

		this.table.loadData(this.rows)
	},

	addColumn: function() {
		this.open({
			mtype: 'RS.wiki.pagebuilder.XColumnManager'
		})
	},
	editColumn: function() {
		this.open({
			mtype: 'RS.wiki.pagebuilder.XColumnManager',
			isEdit: true
		})
	},
	removeColumn: function() {
		this.open({
			mtype: 'RS.wiki.pagebuilder.XColumnPicker'
		})
	},
	columnMove: function(column, fromIdx, toIdx, eOpts) {
		this.columns.splice(toIdx, 0, this.columns.splice(fromIdx, 1)[0])
		this.configureColumns()
	},

	addRow: function() {
		this.table.add({})
	},
	insertRow: function() {
		this.table.insert(this.table.indexOf(this.tableSelections[0]), {})
	},
	insertRowIsEnabled$: function() {
		return this.tableSelections.length > 0
	},
	removeRow: function() {
		this.table.remove(this.tableSelections)
	},
	removeRowIsEnabled$: function() {
		return this.tableSelections.length > 0
	},

	generateContent: function() {
		var output = [];
		var xtable = ''
		if (this.drilldown) xtable = 'drilldown=' + this.drilldown
		if (this.sql) {
			if (xtable.length > 0) xtable += '|'
			xtable += 'sql=' + this.sql
		}

		output.push('{xtable' + (xtable.length > 0 ? ':' : '') + xtable + '}')

		var cols = []
		Ext.Array.forEach(this.columns, function(col) {
			var tCol = Ext.clone(col)
			delete tCol.text
			delete tCol.renderer
			if (tCol.width == 0) delete tCol.width
			cols.push(col.text + ':' + Ext.Object.toQueryString(tCol))
		})
		output.push(cols.join(' | '))

		this.table.each(function(row) {
			var r = [];
			Ext.Array.forEach(this.columns, function(col) {
				if (col.type == 'date')
					r.push(Ext.Date.format(row.get((col.dataIndex || col.text)), col.dataFormat))
				else if (col.type == 'boolean')
					r.push(row.get((col.dataIndex || col.text)) ? 1 : 0)
				else
					r.push(row.get((col.dataIndex || col.text)))
			})
			output.push(r.join(' | '))
		}, this)

		output.push('{xtable}')
		return output.join('\n')
	}
})

glu.defModel('RS.wiki.pagebuilder.Progress', {
	mixins: ['Base'],
	text: '',
	width: -1,
	height: -1,
	border: -1,
	borderColor: '',
	displayText: '',
	completedText: '',
	defaultData : {
		text: '',
		width: -1,
		height: -1,
		border: -1,
		borderColor: '',
		displayText: '',
		completedText: '',
	},
	attach: function() {
		this.open({
			mtype: 'RS.wiki.pagebuilder.ImageManager',
			name: this.rootVM.ufullname
		})
	},

	bgStyle$: function() {
		return '<div style="height:15px;width:15px;border:1px solid #000000;background-color:' + (this.borderColor || '#000000') + '"></div>';
	},
	selectBgColor: function(color) {
		this.set('borderColor', '#' + color)
	},

	when_properties_change: {
		on: ['heightChanged', 'widthChanged', 'borderChanged'],
		action: function() {
			if (isNaN(this.height) || isNaN(this.width) || isNaN(this.border)) {
				this.parentVM.disableUpdate();
			} else {
				this.parentVM.enableUpdate();
			}
		}
	},

	generateContent: function() {
		var props = []
		if (this.text) props.push(this.text)
		if (this.height > -1) props.push('height=' + this.height)
		if (this.width > -1) props.push('width=' + this.width)
		if (this.border > -1) props.push('border=' + this.border)
		if (this.borderColor) props.push('borderColor=' + this.borderColor)
		if (this.displayText) props.push('displayText=' + Ext.String.htmlEncode(this.displayText))
		if (this.completedText) props.push('completedText=' + Ext.String.htmlEncode(this.completedText))

		var propString = props.join('|')
		if (propString) propString = ':' + propString

		return '{progress' + propString + '}\n{progress}'
	}
})

glu.defModel('RS.wiki.pagebuilder.CollaborationLink', {
	mixins: ['Base'],

	displayText: '',
	preceedingText: '',
	openInNewTab: false,
	to: '',
	toIsEnabled$: function() {
		return !this.openInNewTab
	},
	toHidden: [],

	streamToStore: {
		mtype: 'store',
		fields: ['id', 'name', 'type'],
		proxy: {
			type: 'memory'
		}
	},
	defaultData : {
		toHidden: [],
		displayText: '',
		preceedingText: '',
		openInNewTab: false
	},
	init: function() {
		var split = this.to.split('&&'),
			toHidden = [];
		Ext.Array.forEach(split, function(s) {
			if (s) {
				toHidden.push(this.streamToStore.createModel(Ext.Object.fromQueryString(s)))
			}
		}, this)

		this.to = []
		this.set('toHidden', toHidden)
	},

	showAdvancedTo: function() {
		this.open({
			mtype: 'RS.social.StreamPicker'
		})
	},

	generateContent: function() {
		//serialize the to Array
		var serTo = [];
		Ext.Array.forEach(this.toHidden, function(t) {
			serTo.push(Ext.Object.toQueryString(t.data))
		})
		return '{sociallink:' + this.displayText + '|to=' + serTo.join('&&') + '|openInNewTab=' + this.openInNewTab + '|preceedingText=' + this.preceedingText + '}'
	}
})
