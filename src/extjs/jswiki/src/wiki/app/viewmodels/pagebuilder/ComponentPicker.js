glu.defModel('RS.wiki.pagebuilder.ComponentPicker', {
	filter: '',
	index: -1,
	selected: null,
	itemsByType: null,
	itemsByName:  null,
	toolbarHidden: false,
	items: {
		mtype: 'list'
	},
	//REACTOR
	when_filter_changes_update_component_store: {
		on: ['filterChanged'],
		action: function() {
			var me = this;
			for (var i=0; i<this.items.length; i++) {
				var dataViewer = this.items.getAt(i);
				var components = dataViewer.components;
				// components.filter([{
				// 	fn: function(record) {
				// 		return !me.filter || record.get('name').toLowerCase().indexOf(me.filter.toLowerCase()) == 0
				// 	}
				// }]);
				if (!me.addFilter(components)) {
					dataViewer.set('hidden', true);
				} else {
					dataViewer.set('hidden', false);
				}
			}
		}
	},
	init: function() {
		if (!this.itemsByType) {
			if (this.rootVM.viewmodelName == 'PlaybookTemplate') {
				this.set('toolbarHidden', true);
				this.itemsByType = [
					this.loadPlaybookComponents()
				];
			} else {
				this.set('toolbarHidden', false);
				this.itemsByType = [
					this.loadByTextBase(),
					this.loadByAutomationStatus(),
					this.loadByDataForm(),
					this.loadBySourcecode()
				];
			}
		}
		this.addItems(this.itemsByType);
	},

	loadPlaybookComponents: function() {
		var components = this.createComponentStore([
			{
				name: this.localize('wysiwyg'),
				type: 'wysiwyg'
			}, {
				name: this.localize('image'),
				type: 'image'
			}
		]);
		components['title'] = 'Available components for playbook template';
		return components;
	},

	updateSelection: function(selectionModel) {
		this.set('selected', selectionModel.getSelection()[0]);
		for (i=0; i<this.items.length; i++) {
			currentSelectionModel = this.items.getAt(i) .selectionModel;
			if (currentSelectionModel && currentSelectionModel !== selectionModel && currentSelectionModel.hasSelection()) {
				currentSelectionModel.deselectAll(true);
			}
		}
	},
	createComponentStore: function(data) {
		var components = Ext.create('Ext.data.Store', {
			sorters: ['name'],
			proxy: {
				type: 'memory'
			},
			fields: ['name', 'type']
		});
		components.loadData(data);
		return components;
	},
	loadByTextBase: function() {
		var components = this.createComponentStore([
	 		{
				name: this.localize('wysiwyg'),
				type: 'wysiwyg'
			}, {
				name: this.localize('source'),
				type: 'source'
			}, {
				name: this.localize('code'),
				type: 'code'
			}, {
				name: this.localize('wikidoc'),
				type: 'wikidoc'
			}, {
				name: this.localize('sociallink'),
				type: 'sociallink'
			}
		]);
		components['title'] = 'Text Based Components and Links';
		return components;
	},
	loadByAutomationStatus: function() {
		var components = this.createComponentStore([
			{
				name: this.localize('automation'),
				type: 'automation'
			}, {
				name: this.localize('actiontask'),
				type: 'actiontask'
			}, {
				name: this.localize('model'),
				type: 'model'
			}, {
				name: this.localize('progress'),
				type: 'progress'
			}, {
				name: this.localize('result'),
				type: 'result'
			}, {
				name: this.localize('detail'),
				type: 'detail'
			}
		]);
		components['title'] = 'Automation & Status Components';
		return components;
	},
	loadByDataForm: function() {
		var components = this.createComponentStore([
			{
				name: this.localize('form'),
				type: 'form'
			},  {
				name: this.localize('table'),
				type: 'table'
			}, {
				name: this.localize('xtable'),
				type: 'xtable'
			}, {
				name: this.localize('infobar'),
				type: 'infobar'
			}, {
				name: this.localize('image'),
				type: 'image'
			},  {
				name: this.localize('encrypt'),
				type: 'encrypt'
			}
		]);
		components['title'] = 'Data & Form';
		return components;
	},
	loadBySourcecode: function() {
		var components = this.createComponentStore([
			{
				name: this.localize('groovy'),
				type: 'groovy'
			}, {
				name: this.localize('javascript'),
				type: 'javascript'
			}, {
				name: this.localize('html'),
				type: 'html'
			}
		]);
		components['title'] = 'Source Code Components';
		return components;
	},
	loadByNames: function() {
		var components = this.createComponentStore([
			{
				name: this.localize('image'),
				type: 'image'
			}, {
				name: this.localize('result'),
				type: 'result'
			}, {
				name: this.localize('model'),
				type: 'model'
			}, {
				name: this.localize('form'),
				type: 'form'
			}, {
				name: this.localize('wysiwyg'),
				type: 'wysiwyg'
			}, {
				name: this.localize('groovy'),
				type: 'groovy'
			}, {
				name: this.localize('javascript'),
				type: 'javascript'
			}, {
				name: this.localize('html'),
				type: 'html'
			}, {
				name: this.localize('source'),
				type: 'source'
			}, {
				name: this.localize('actiontask'),
				type: 'actiontask'
			}, {
				name: this.localize('automation'),
				type: 'automation'
			}, {
				name: this.localize('infobar'),
				type: 'infobar'
			}, {
				name: this.localize('encrypt'),
				type: 'encrypt'
			}, {
				name: this.localize('table'),
				type: 'table'
			}, {
				name: this.localize('xtable'),
				type: 'xtable'
			}, {
				name: this.localize('code'),
				type: 'code'
			}, {
				name: this.localize('wikidoc'),
				type: 'wikidoc'
			}, {
				name: this.localize('detail'),
				type: 'detail'
			}, {
				name: this.localize('progress'),
				type: 'progress'
			}, {
				name: this.localize('sociallink'),
				type: 'sociallink'
			}
		]);
		return components;
	},
	addFilter: function(store) {
		var me = this,
		    filtered = false;
		store.filter([{
			fn: function(record) {
				var passed = (!me.filter || record.get('name').toLowerCase().indexOf(me.filter.toLowerCase()) == 0);
				if (passed) {
					filtered = passed
				}
				return passed;
			}
		}]);
		return filtered;
	},
	addItem: function(store, standAlone) {
		var dataViewer = this.model( 'ComponentsViewer');
		if (!this.addFilter(store)) {
			// Hide the view if there is no record
			dataViewer.set('hidden', true);
		} else {
			dataViewer.set('hidden', false);
		}
		dataViewer.set('components', store);
		dataViewer['componentPicker'] = this;
		if (store['title']) {
			dataViewer.set('title', store['title']);
		}
		var tpl = null;
		if (standAlone) {
			tpl = Ext.create('Ext.XTemplate',
			'<tpl for=".">',
			'<div class="componentPicker-section">', '<img width="70" height="70" src="/resolve/images/sections/components/{[values.name.replace(/ /g, "-")]}.png" />',
			'<strong>{name}</strong>',
			'</div>',
			'</tpl>');
			dataViewer.set('itemSelector', 'div.componentPicker-section');
		} else {
			tpl = Ext.create('Ext.XTemplate',
			'<tpl for=".">',
			'<div class="componentPicker-section-group">', '<img width="70" height="70" src="/resolve/images/sections/components/{[values.name.replace(/ /g, "-")]}.png" />',
			'<strong>{name}</strong>',
			'</div>',
			'</tpl>');
			dataViewer.set('itemSelector', 'div.componentPicker-section-group');
		}
		dataViewer.set('tpl', tpl);
		this.items.add(dataViewer);
	},
	addItems: function(stores) {
		for(var i=0; i<stores.length; i++) {
			this.addItem(stores[i], false);
		}
	},
	addComponent: function() {
		this.parentVM.updateComponentType(this.selected.get('type'), this.columnPos);
		this.doClose();
		if (Ext.isFunction(this.rootVM.notifyDirtyFlag)) {
			this.rootVM.notifyDirtyFlag('Pagebuilder');
		}
	},
	addComponentIsEnabled$: function() {
		return this.selected !== null;
	},
	cancel: function() {
		this.doClose();
	},
	sortChange: function(btn) {
		if (!btn.checked) {
			return;
		}
		this.items.removeAll();
		this.set('selected', null);
		if (btn.inputValue === 'type') {
			if (!this.itemsByType) {
					this.itemsByType = [
					this.loadByTextBase(),
					this.loadByAutomationStatus(),
					this.loadByDataForm(),
					this.loadBySourcecode()
				];
			}

			this.addItems(this.itemsByType);
		} else {
			if (!this.itemsByName) {
				this.itemsByName = this.loadByNames();
			}
			this.addItem(this.itemsByName, true);
		}
	},
	setWindowSize: function(v, eOpts) {
		var parentSize = Ext.getBody().getViewSize();
		if (this.rootVM.viewmodelName == 'PlaybookTemplate') {
			v.setWidth(500);
			v.setHeight(300);
		} else {
			v.setWidth(Math.round(parentSize.width *.6));
			v.setHeight(Math.round(parentSize.height *.9));
		}
	}
})
