glu.defModel('RS.wiki.pagebuilder.AttachFile', {
	mixins: ['FileUploadManager', 'Attachments'],
	filesOnAllCompleteCallback: function() {
		//this.loadFiles();
	},
	init: function() {
		this.set('title', RS.wiki.locale.AttachFile.attachFileWindowTitle);
		this.queuedStatus = this.localize('queuedStatus');
		this.errorStatus = this.localize('errorStatus');
		this.completedStatus = this.localize('completedStatus');
		this.processingStatus = this.localize('processingStatus');
		this.uploadingStatus = this.localize('uploadingStatus');
	},
	close: function() {
		this.parentVM.loadRecords();
		this.doClose()
	}
})
