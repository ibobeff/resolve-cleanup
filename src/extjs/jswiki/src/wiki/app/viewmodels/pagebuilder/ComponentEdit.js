glu.defModel('RS.wiki.pagebuilder.ComponentEdit', {	
	typeConfigurations: {
		mtype: 'list',
		autoParent: true
	},
	title : '',
	isNew: false,
	init: function() {
		this.set('title','Edit ' + this.componentName);
		this.parseComponent(this.type, this.properties, this.content);
		if (this.isNew) {
			this.preUpdate();
		}
	},
	parseComponent: function(type, properties, content) {
		properties = properties || {};
		switch (type.toLowerCase()) {
			case 'url':
			case 'source':
				this.typeConfigurations.add(this.model({
					mtype: 'RS.wiki.pagebuilder.ResolveMarkup',
					content: content
				}))
				break;

			case 'wysiwyg':
				this.typeConfigurations.add(this.model({
					mtype: 'RS.wiki.pagebuilder.TextEditor',
					content: content
				}))
				break;

			case 'groovy':
				this.typeConfigurations.add(this.model({
					mtype: 'RS.wiki.pagebuilder.Groovy',
					content: content
				}))
				break;

			case 'html':
				this.typeConfigurations.add(this.model({
					mtype: 'RS.wiki.pagebuilder.HTML',
					content: content
				}))
				break;

			case 'javascript':
				this.typeConfigurations.add(this.model({
					mtype: 'RS.wiki.pagebuilder.Javascript',
					content: content
				}))
				break;

			case 'form':
				var w = +properties.popoutWidth;
				var h = +properties.popoutHeight;

				if (isNaN(w)) {
					w = -1;
				}

				if (isNaN(h)) {
					h = -1;
				}

				this.typeConfigurations.add(this.model({
					mtype: 'RS.wiki.pagebuilder.Form',
					title: properties.title || '',
					name: properties.name || '',
					tooltip: properties.tooltip || '',
					popout: properties.popout === 'true',
					collapsed: properties.collapsed === 'true',
					popoutWidth: w,
					popoutHeight: h
				}))
				break;

			case 'encrypt':
				this.typeConfigurations.add(this.model({
					mtype: 'RS.wiki.pagebuilder.Secure',
					key: properties.key,
					value: properties.value
				}))
				break;

			case 'infobar':
				var h = +properties.height;

				if (isNaN(h)) {
					h = 400
				}

				var infobarParams = {};
				infobarParams.height = h;
				if (properties.social) infobarParams.social = properties.social === 'true';
				if (properties.feedback) infobarParams.feedback = properties.feedback === 'true';
				if (properties.rating) infobarParams.rating = properties.rating === 'true';
				if (properties.attachments) infobarParams.attachments = properties.attachments === 'true';
				if (properties.history) infobarParams.history = properties.history === 'true';
				if (properties.tags) infobarParams.tags = properties.tags === 'true';
				if (properties.pageInfo) infobarParams.pageInfo = properties.pageInfo === 'true';

				this.typeConfigurations.add(this.model(Ext.apply({
					mtype: 'RS.wiki.pagebuilder.Infobar',
				}, infobarParams)));
				break;

			case 'image':
				var w = properties.width;
				var h = properties.height;
				this.typeConfigurations.add(this.model({
					mtype: 'RS.wiki.pagebuilder.Image',
					height: h,
					width: w,
					zoom: properties.zoom === 'true',
					src: properties.text || ''
				}))
				break;

			case 'model':
				var w = +properties.width;
				var h = +properties.height;
				var interval = +properties.refreshInterval;
				var refreshCountMax = +properties.refreshCountMax;

				if (isNaN(w)) {
					w = -1;
				}

				if (isNaN(h)) {
					h = -1;
				}

				if (isNaN(interval)) {
					interval = 5;
				}

				if (isNaN(refreshCountMax)) {
					refreshCountMax = 60;
				}

				this.typeConfigurations.add(this.model({
					mtype: 'RS.wiki.pagebuilder.Model',
					wiki: properties.wiki || '',
					status: properties.status || '',
					zoom: properties.zoom || '',
					width: w,
					height: h,
					refreshInterval: interval,
					refreshCountMax: refreshCountMax
				}))
				break;

			case 'code':
				this.typeConfigurations.add(this.model({
					mtype: 'RS.wiki.pagebuilder.CodeSample',
					type: properties.type || 'text',
					showLineNumbers: properties.showLineNumbers === 'false' ? false : true,
					content: content
				}))
				break;

			case 'progress':
				var w = +properties.width;
				var h = +properties.height;
				var b = +properties.border;

				if (isNaN(w)) {
					w = -1;
				}

				if (isNaN(h)) {
					h = -1;
				}

				if (isNaN(b)) {
					b = -1;
				}				

				this.typeConfigurations.add(this.model({
					mtype: 'RS.wiki.pagebuilder.Progress',
					content: content,
					text: properties.text || '',
					completedText: properties.completedText || '',
					width: w,
					height: h,
					border: b,
					borderColor: properties.borderColor || '',
					displayText: properties.displayText || ''
				}))
				break;

			case 'automation':
				var w = +properties.popoutWidth;
				var h = +properties.popoutHeight;

				if (isNaN(w)) {
					w = -1;
				}

				if (isNaN(h)) {
					h = -1;
				}

				var automationVM = this.model({
					mtype: 'RS.wiki.pagebuilder.Automation',
					title: properties.title || '',
					name: properties.name || '',
					wikiName: properties.wikiName || '',
					tooltip: properties.tooltip || '',
					popout: properties.popout === 'true',
					collapsed: properties.collapsed === 'true',
					popoutWidth: w,
					popoutHeight: h
				});
				this.typeConfigurations.add(automationVM);
				break;

			case 'result':
				var interval = +properties.refreshInterval;
				var refreshCountMax = +properties.refreshCountMax;
				var w = +properties.descriptionWidth;
				var order = 'DESC';

				if (isNaN(interval)) {
					interval = 5;
				}
				
				if (isNaN(refreshCountMax)) {
					refreshCountMax = 60;
				}

				if (isNaN(w)) {
					w = 0;
				}

				if(properties.order && properties.order.toLowerCase() === 'asc') {
					order = 'ASC';
				}

				var resultParams = {};
				resultParams.refreshInterval = interval;
				resultParams.refreshCountMax = refreshCountMax;
				resultParams.descriptionWidth = w;
				resultParams.filter = properties.filter || '';

				if(Object.keys(properties).length !== 0) {
					if (properties.showWiki) {
						if (properties.showWiki == this.rootVM.ufullname) {
							resultParams.showWiki = 'current';
						} else {
							resultParams.showWiki = 'custom';
							resultParams.showWikiName = properties.showWiki;
						}
					} else {
						resultParams.showWiki = 'any';
					}
				} else {
					// new results component will default to only show current wiki
					resultParams.showWiki = 'current';
				}

				if (properties.text || properties.title) resultParams.title = properties.text || properties.title;
				if (properties.encodeSummary) resultParams.encodeSummary = properties.encodeSummary === 'true';
				if (properties.order) resultParams.order = properties.order.toLowerCase() == 'asc' ? 'ASC' : 'DESC';
				if (properties.autoCollapse) resultParams.autoCollapse = properties.autoCollapse === 'true';
				if (properties.collapsed) resultParams.collapsed = properties.collapsed === 'true';
				if (properties.autoHide) resultParams.autoHide = properties.autoHide === 'true';
				if (properties.progress) resultParams.progress = properties.progress === 'true';
				if (properties.selectAll) resultParams.selectAll = properties.selectAll === 'true';
				if (properties.preserveTaskOrder) resultParams.preserveTaskOrder = properties.preserveTaskOrder === 'true';
				if (properties.includeStartEnd) resultParams.includeStartEnd = properties.includeStartEnd === 'true';

				this.typeConfigurations.add(this.model(Ext.apply({
					mtype: 'RS.wiki.pagebuilder.Result',
					content: content
				}, resultParams)))
				
				break;
			case 'actiontask':
				var w = +properties.popoutWidth;
				var h = +properties.popoutHeight;

				if (isNaN(w)) {
					w = -1;
				}

				if (isNaN(h)) {
					h = -1;
				}

				this.typeConfigurations.add(this.model({
					mtype: 'RS.wiki.pagebuilder.ActionTask',
					title: properties.title || '',
					name: properties.name || '',
					tooltip: properties.tooltip || '',
					popout: properties.popout === 'true',
					collapsed: properties.collapsed === 'true',
					popoutWidth: w,
					popoutHeight: h
				}))
				break;

			case 'detail':
				var interval = +properties.refreshInterval;
				var refreshCountMax = +properties.refreshCountMax;

				if (isNaN(interval)) {
					interval = 5;
				}

				if (isNaN(refreshCountMax)) {
					refreshCountMax = 60;
				}

				var m = this.model({
					mtype: 'RS.wiki.pagebuilder.Detail',
					content: content,
					progress: properties.progress === 'true',
					maximum: properties.max || '',
					offset: properties.offset || '',
					refreshInterval: interval,
					refreshCountMax: refreshCountMax
				});

				this.typeConfigurations.add(m);
				break;

			case 'wikidoc':
				var wikiName = '', matches = /#includeForm\(['|"](.*?)['|"]\)/gi.exec(content);
				
				if (matches && matches.length > 1) {
					wikiName = matches[1];
				}

				this.typeConfigurations.add(this.model({
					mtype: 'RS.wiki.pagebuilder.Page',
					wiki: wikiName
				}))
				break;

			case 'table':
				this.typeConfigurations.add(this.model({
					mtype: 'RS.wiki.pagebuilder.Table',
					content: content
				}))
				break;

			case 'xtable':
				this.typeConfigurations.add(this.model({
					mtype: 'RS.wiki.pagebuilder.XTable',
					sql: properties.sql || '',
					drilldown: properties.drilldown || '',
					content: content
				}))
				break;

			case 'sociallink':
				this.typeConfigurations.add(this.model({
					mtype: 'RS.wiki.pagebuilder.CollaborationLink',
					to: properties.to || '',
					displayText: properties.text || '',
					preceedingText: properties.preceedingText || '',
					openInNewTab: properties.openInNewTab === 'true'
				}))
				break;

			default:
				this.typeConfigurations.add(this.model({
					mtype: 'RS.wiki.pagebuilder.ResolveMarkup',
					content: content
				}))
				clientVM.displayError(this.localize('unknownLegacyType', type.toLowerCase()))
				break;
		}
	},
	updateIsDisabled: false,
	update: function() {		
		if (this.task) {
			this.task.cancel();
		}
		this.task = new Ext.util.DelayedTask(this.parentVM.updateContent, this.parentVM, [this.typeConfigurations.getAt(0), this.doClose, this]);
		this.task.delay(100);
	},
	preUpdate: function() {		
		if (this.task) {
			this.task.cancel();
		}
		this.task = new Ext.util.DelayedTask(this.parentVM.updateContent, this.parentVM, [this.typeConfigurations.getAt(0), function(){}, this]);
		this.task.delay(100);
	},
	disableUpdate: function() {
		this.set('updateIsDisabled', true);
	},
	enableUpdate: function() {
		this.set('updateIsDisabled', false);
	},
	cancel: function() {		
		this.doClose();
	},
	beforeClose : function(){
		//Update anyway to generate the base content for this component.
		/*
		if(this.isNew){
			this.typeConfigurations.getAt(0).resetData();
			this.update();
		}
		*/
	},
	expandDialog: function(editWin, width, height) {
		if (this.expand) {
			editWin.setWidth(Math.round(width * 0.8));
			editWin.setHeight(Math.round(height * 0.8));
		}
	}

});