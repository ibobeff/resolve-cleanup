glu.defModel('RS.wiki.pagebuilder.TaskEdit', {
	description: '',
	descriptionIsVisible$: function() {
		return !this.isTag
	},
	descriptionWikiLink: '',
	descriptionWikiLinkIsVisible$: function() {
		return !this.isTag && !this.isGroup
	},
	namespace: '',
	namespaceIsVisible$: function() {
		return !this.isTag && !this.isGroup
	},
	nodeId: '',
	nodeIdIsVisible$: function() {
		return !this.isTag && !this.isGroup
	},
	resultWikiLink: '',
	resultWikiLinkIsVisible$: function() {
		return !this.isTag && !this.isGroup
	},
	task: '',
	taskIsVisible$: function() {
		return !this.isTag && !this.isGroup
	},
	wiki: '',
	wikiIsVisible$: function() {
		return !this.isTag && !this.isGroup
	},
	autoCollapse: false,
	autoCollapseIsVisible$: function() {
		return !this.isTag && this.isGroup
	},
	tags: '',

	height$: function() {
		return this.isGroup && !this.isTag ? 165 : 350
	},

	isTag$: function() {
		return this.tags
	},

	isGroup: false,

	tagStore: {
		mtype: 'store',
		fields: ['name'],
		proxy: {
			type: 'memory'
		}
	},
	tagColumns: [{
		dataIndex: 'name',
		text: '~~name~~',
		flex: 1
	}],

	init: function() {
		if (!this.task) this.set('isGroup', true)
		var tagSplit = this.tags.split(',')
		Ext.Array.forEach(tagSplit, function(tag) {
			this.tagStore.add({
				name: tag
			})
		}, this)
	},

	tagGridSelections: [],
	add: function() {
		this.open({
			mtype: 'RS.catalog.TagPicker',
			callback: 'addTags'
		})
	},
	addTags: function(newTags) {
		var tagName = '';
		Ext.Array.forEach(newTags, function(tag) {
			tagName = tag.get('name')
			if (this.tagStore.findExact('name', tagName) == -1) {
				this.tagStore.add({
					name: tagName
				})
			}
		}, this)
	},
	remove: function() {
		Ext.Array.forEach(this.tagGridSelections, function(selection) {
			this.tagStore.remove(selection)
		}, this)
	},
	removeIsEnabled$: function() {
		return this.tagGridSelections.length > 0
	},

	apply: function() {
		//persist tags to the tags property properly
		var tags = []
		this.tagStore.each(function(tag) {
			tags.push(tag.get('name'))
		})
		this.set('tags', tags.join(','))

		//Persist edit properties to the parent record
		var params = {}
		Ext.Object.each(this.parentVM.actionTasksSelections[0].data, function(key) {
			if (Ext.isDefined(this[key])) {
				params[key] = this[key]
			}
		}, this)
		this.parentVM.actionTasksSelections[0].set(params)
		this.parentVM.actionTasksSelections[0].commit()
		this.doClose()
	},
	cancel: function() {
		this.doClose()
	}
})

glu.defModel('RS.wiki.pagebuilder.ColumnAdder', {
	column: '',
	columnIsValid$: function() {
		return /^[_\w]*\s?[_\w]*$/.test(this.column) ? true : this.localize('invalidColumnName');
	},
	add: function() {
		this.parentVM.columns.push(this.column);
		var oldRows = this.parentVM.extractRowsFromStore();
		this.parentVM.configureStore();
		this.parentVM.configureColumns();
		this.parentVM.populateRowsIntoStore(oldRows);
		this.doClose();
	},
	addIsEnabled$: function() {
		return this.isValid;
	},
	cancel: function() {
		this.doClose()
	}
})

glu.defModel('RS.wiki.pagebuilder.ColumnPicker', {
	column: '',
	columns: {
		mtype: 'store',
		fields: ['name'],
		proxy: {
			type: 'memory'
		}
	},
	init: function() {
		Ext.Array.forEach(this.parentVM.columns, function(col) {
			this.columns.add({
				name: col
			})
		}, this)
	},
	remove: function() {
		var oldRows = this.parentVM.extractRowsFromStore();
		this.parentVM.columns.splice(Ext.Array.indexOf(this.parentVM.columns, this.column), 1)
		this.parentVM.configureStore();
		this.parentVM.configureColumns();
		this.parentVM.populateRowsIntoStore(oldRows);
		this.doClose();
	},
	cancel: function() {
		this.doClose()
	}
})

glu.defModel('RS.wiki.pagebuilder.XColumnPicker', {
	column: '',
	columns: {
		mtype: 'store',
		fields: ['name'],
		proxy: {
			type: 'memory'
		}
	},
	init: function() {
		Ext.Array.forEach(this.parentVM.columns, function(col) {
			this.columns.add({
				name: col.text
			})
		}, this)
	},
	remove: function() {
		var index = -1;
		Ext.Array.each(this.parentVM.columns, function(col, idx) {
			if (col.text == this.column) {
				index = idx
				return false
			}
		}, this)

		if (index > -1) {
			this.parentVM.columns.splice(index, 1)
			this.parentVM.configureStore()
			this.parentVM.configureColumns()
		}
		this.doClose()
	},
	cancel: function() {
		this.doClose()
	}
})

glu.defModel('RS.wiki.pagebuilder.XColumnManager', {
	isEdit: false,

	title$: function() {
		return this.isEdit ? this.localize('editColumn') : this.localize('addColumn')
	},
	link: '',
	linkTarget: '',
	column: '',
	columnIsVisible$: function() {
		return this.isEdit
	},
	columnStore: {
		mtype: 'store',
		fields: ['text', 'type', 'dataIndex', 'format', 'dataFormat', {
			name: 'width',
			type: 'int'
		}, {
			name: 'sortable',
			type: 'boolean'
		}, {
			name: 'draggable',
			type: 'boolean'
		}],
		proxy: {
			type: 'memory'
		}
	},
	when_column_changes_update_field_data: {
		on: ['columnChanged'],
		action: function() {
			Ext.Array.each(this.parentVM.columns, function(col) {
				if (col.text == this.column) {
					this.set('text', col.text)
					this.set('type', col.type)
					this.set('dataIndex', col.dataIndex)
					this.set('format', col.format)
					this.set('dataFormat', col.dataFormat)
					this.set('width', col.width)
					this.set('sortable', col.sortable)
					this.set('draggable', col.draggable)
					this.set('link', col.link)
					this.set('linkTarget', col.linkTarget)
					return false
				}
			}, this)
		}
	},

	text: '',
	textIsValid$: function() {
		return !!this.text ? true : this.localize('invalidText');
	},
	columnNames: {
		mtype: 'store',
		fields: ['name'],
		proxy: {
			type: 'memory'
		}
	},
	type: '',
	typeStore: {
		mtype: 'store',
		sorters: ['name'],
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	dataIndex: '',
	format: '',
	formatIsVisible$: function() {
		return this.type == 'date'
	},
	formatStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	dataFormat: '',
	dataFormatIsVisible$: function() {
		return this.type == 'date'
	},
	dataFormatStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	width: 0,
	sortable: false,
	draggable: false,

	init: function() {
		this.columnStore.loadData(this.parentVM.columns)

		this.formatStore.loadData([{
			name: 'Y-m-d H:i:s',
			value: 'Y-m-d H:i:s'
		}, {
			name: 'Y-m-d',
			value: 'Y-m-d'
		}])
		this.dataFormatStore.loadData([{
			name: 'Y-m-d H:i:s',
			value: 'Y-m-d H:i:s'
		}, {
			name: 'Y-m-d',
			value: 'Y-m-d'
		}])

		this.typeStore.loadData([{
			name: this.localize('stringType'),
			value: ''
		}, {
			name: this.localize('intType'),
			value: 'int'
		}, {
			name: this.localize('floatType'),
			value: 'float'
		}, {
			name: this.localize('dateType'),
			value: 'date'
		}])
	},

	add: function() {
		this.parentVM.columns.push({
			text: this.text,
			type: this.type,
			dataIndex: this.dataIndex,
			format: this.format,
			dataFormat: this.dataFormat,
			width: this.width,
			sortable: this.sortable,
			draggable: this.draggable,
			link: this.link,
			linkTarget: this.linkTarget
		})
		this.parentVM.configureStore()
		this.parentVM.configureColumns()
		this.doClose()
	},
	addIsVisible$: function() {
		return !this.isEdit
	},
	addIsEnabled$: function() {
		return this.isValid;
	},
	apply: function() {
		Ext.Array.each(this.parentVM.columns, function(col) {
			if (col.text == this.column) {
				Ext.apply(col, {
					text: Ext.String.htmlEncode(this.text),
					type: this.type,
					dataIndex: this.dataIndex,
					format: this.format,
					dataFormat: this.dataFormat,
					width: this.width,
					sortable: this.sortable,
					draggable: this.draggable,
					link: this.link,
					linkTarget: this.linkTarget
				})
				return false
			}
		}, this)
		this.parentVM.configureStore()
		this.parentVM.configureColumns()
		this.doClose()
	},
	applyIsEnabled$: function() {
		return this.column && this.isValid;
	},
	applyIsVisible$: function() {
		return this.isEdit
	},
	cancel: function() {
		this.doClose()
	}
})

