glu.defModel('RS.wiki.pagebuilder.SectionsLayout', {
	entityType: 'sectionlayout',
	maxSectionIdx: 0,
	maxComponentIdx: 0,
	taskEditorIsDirty: false,
	newWorksheet: false,
	editorIsValid: true,
	interactive: false,
	isTabValid: true,
	globalVariableStore: {
		mtype: 'store',
		fields: ['name', 'task', 'color']
	},
	sectionEntries: {
		mtype: 'list'
	},
	paramStore: {
		mtype: 'store',
		sorters: ['name'],
		fields: ['name', 'sourceName']
	},
	init: function() {
	},
	//Parse Page Content
	parseSection : function(content){
		this.sectionEntries.removeAll();

		//Step 1 - clean the data in case the user doesn't know to wrap everything in a section
		var badSections = content.split(/\{section[^\}]*\}[\s\S]*?\{section\}/gi);
		Ext.Array.forEach(badSections, function(badSection) {
			if (badSection && Ext.String.trim(badSection)) {
				content = content.replace(badSection, '\n{section}\n' + badSection + '\n{section}\n');
			}
		})
		//Step 2 - parse out each of the sections and setup their content into proper builder sections
		var sections = content.match(/\{section[^\}]*\}([\s\S]*?)\{section\}/gi) || []
		Ext.Array.forEach(sections, function(section) {
			var content = Ext.String.trim(section)
			if (content) {
				this.populateSection(content);
			}
		}, this)

		if (!sections.length) {
			this.addNewSection(!this.rootVM.isNewWikiFlag);
		}

		this.updateUpDownBtn();
	},
	addNewSection: function(ignoreDirty) {
		if (!ignoreDirty) {
			if (Ext.isFunction(this.rootVM.notifyDirtyFlag)) {
				this.rootVM.notifyDirtyFlag('Pagebuilder');
			}
		}
		var sectionEntry = this.model({
			mtype : 'RS.wiki.pagebuilder.SectionEntry',
			id : Ext.data.IdGenerator.get('uuid').generate(),
			new : true
		});
		this.sectionEntries.add(sectionEntry);
		this.updateUpDownBtn();
		this.fireEvent('new_section_added');
	},
	populateSection: function(section) {
		var sectionEntry = this.model({
			mtype : 'RS.wiki.pagebuilder.SectionEntry',
			name : 'New Section',
			content : section,
			id : Ext.data.IdGenerator.get('uuid').generate()
		});
		this.sectionEntries.add(sectionEntry);

		var components = [];
		var c = /\{section[^\}]*\}([\s\S]*?)\{section\}/gi.exec(section);
		if (Ext.isArray(c)) {
			c.splice(0, 1);
			components = c.join('');
		}
		sectionEntry.parseComponent(Ext.String.trim(components));
	},
	getAllSectionContent : function(){
		var allSectionContents = [];
		this.sectionEntries.forEach(function(sectionEntry) {
			allSectionContents .push(sectionEntry.getSectionContent());
		})
		return allSectionContents.join('\n').trim();
	},
	/*
	addLayoutSectionIsEnabled$: function() {
		return !this.waitRootOp;
	},
	*/
	populateScreen: function(sections) {
		this.sectionEntries.removeAll();
	},
	removeSection: function(entity) {
		this.sectionEntries.remove(entity);
		this.updateUpDownBtn();
		if (Ext.isFunction(this.rootVM.checkPageDirtyFlag))
			this.rootVM.checkPageDirtyFlag();
	},
	updateUpDownBtn: function() {
		this.sectionEntries.foreach(function(section) {
			section.configureUpDownBtn();
		});
	}
});

