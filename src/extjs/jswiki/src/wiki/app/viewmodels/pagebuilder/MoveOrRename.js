glu.defModel('RS.wiki.pagebuilder.MoveOrRename', {
	moveRenameCopyTitle: '',
	action: '',
	wait: false,
	namespace: '',
	modelName: '',
	selectionsLength: 0,


	namespaceIsValid$: function() {
		return this.namespace ? true : this.localize('invalidNamespace');
	},

	namespaceIsEnabled$: function() {
		return !this.wait;
	},

	namespaceIsVisible$: function() {
		return this.modelName != 'AttachmentsAdmin';
	},

	filename: '',

	filenameIsVisible$: function() {
		return this.selectionsLength === 1 && this.modelName != 'NamespaceAdmin';
	},

	filenameIsValid$: function() {
		return this.filename && !/^\s+.*$/.test(this.filename) || this.selectionsLength > 1 ? true : (this.type == "Attachment" ? this.localize("invalidAttachmentName") : this.localize('invalidFilename'));
	},

	store: {
		mtype: 'store',
		fields: ['unamespace'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wiki/namespace/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			},
			extraParams: {
				limit: -1
			}
		}
	},
	overwrite: false,
	skip: true,
	update: false,
	title: '',
	type: null,
	activate: function() {},

	dumper: null,

	init: function() {
		this.set('moveRenameCopyTitle', this.localize(this.moveRenameCopyTitle));
		this.store.on('beforeload', function() {
			this.set('wait', true);
		}, this);

		this.store.on('load', function(store, records, suc) {
			if (!suc)
				clientVM.displayError(this.localize('ListNamespaceErrMsg'));
			this.set('wait', false);
		}, this);
		this.loadNamespace();
	},

	loadNamespace: function() {
		this.store.load();
	},

	confirmAction: function() {
		this.dumper({
			action: this.action,
			namespace: this.namespace,
			filename: this.filename,
			choice: this.overwrite ? 'overwrite' : this.skip ? 'skip' : 'update'
			// overwrite:this.overwrite,
			// skip:this.skip,
			// update:this.update
		});
		this.doClose();
	},

	confirmActionIsEnabled$: function() {
		return !this.wait;
	},

	updateIsVisible$: function() {
		return this.action == 'copy';
	},
	skip_tooltip$: function() {
		return this.type === "Attachment" ? this.localize('skip_tooltip_attachment') : this.localize("skip_tooltip");
	},
	overwrite_tooltip$: function() {
		return this.type === "Attachment" ? this.localize("overwrite_tooltip_attachment") : this.localize("overwrite_tooltip");
	},	
	close: function() {
		this.doClose();
	}
});