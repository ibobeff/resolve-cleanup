glu.defModel('RS.wiki.pagebuilder.Source', {
	content: '',
	originalContent : '',
	init : function(){
		this.set('originalContent', this.content);
	},
	checkPageDirtyFlag : function(){
		if (Ext.isFunction(this.rootVM.checkPageDirtyFlag))
			this.rootVM.checkPageDirtyFlag();
	},
	getSourceHelp: function() {
		this.open({
			mtype: 'RS.wiki.SourceHelp'
		})
	},
	save: function() {
		this.parentVM.updateContentFromSource(this.content);
		this.checkPageDirtyFlag();
	},
	update: function() {
		this.parentVM.updateContentFromSource(this.content);
		this.checkPageDirtyFlag();
		this.parentVM.set('sourceEditWin', null);
		this.doClose();
	},
	cancel: function() {
		this.parentVM.set('sourceEditWin', null);
		this.doClose();
	}
});
