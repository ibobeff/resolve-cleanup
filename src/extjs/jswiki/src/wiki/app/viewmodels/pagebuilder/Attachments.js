glu.defModel('RS.wiki.pagebuilder.Attachments',{
	mixins: ['FileUploadManager'],
	api: {
		list: '/resolve/service/wiki/getAttachments',
		upload: '/resolve/service/wiki/attachment/upload',
		delete: '/resolve/service/wikiadmin/attachment/delete',
		attachmentRename : '/resolve/service/wikiadmin/attachment/rename',
	},
	isEditMode: false,

	queuedStatus: '',
	errorStatus: '',
	completedStatus: '',
	processingStatus: '',
	uploadingStatus: '',

	notifyDirty: function() {
		if (Ext.isFunction(this.rootVM.notifyDirtyFlag)) {
			this.rootVM.notifyDirtyFlag(this.viewmodelName);
		}
	},
	checkDirty: function() {
		var isDirty = false;
		var fileStoreSize = this.filesStore.getCount();
		var origAttachments = this.parentVM.originalData.attachments || [];
		if (origAttachments.length != fileStoreSize) {
			isDirty = true;
		} else {
			for (var i = 0; i < fileStoreSize; i++) {
				var file = this.filesStore.getAt(i);
				if (file.get('id') != origAttachments[i].id || file.get('fileName') != origAttachments[i].name) {
					isDirty = true;
					break;
				}
			}
		}
		if (isDirty) {
			this.notifyDirty();
		} else {
			this.clearDirty();
		}
	},
	clearDirty: function() {
		if (Ext.isFunction(this.rootVM.clearComponentDirtyFlag)) {
			this.rootVM.clearComponentDirtyFlag(this.viewmodelName);
		}
	},

	init: function() {
		if (!this.isEditMode) {
			this.set('allowUpload', false);
			this.set('allowRemove', false);
		}
		this.queuedStatus = this.localize('queuedStatus');
		this.errorStatus = this.localize('errorStatus');
		this.completedStatus = this.localize('completedStatus');
		this.processingStatus = this.localize('processingStatus');
		this.uploadingStatus = this.localize('uploadingStatus');

		this.filesStore.on('load', function(store, recs, suc) {
			if (!this.parentVM.originalData.attachments) {
				this.parentVM.originalData.attachments = [];
				for (var i = 0; i < recs.length; i++) {
					this.parentVM.originalData.attachments.push({
						id: recs[i].get('id'),
						name: recs[i].get('fileName')
					});
				}
			} else {
				this.checkDirty();
			}
		}, this);
	},
	renameAttachmentIsEnabled$: function() {
		return this.selections.length == 1;
	},
	renameAttachment: function() {
		var docSysId = this.id;
		var attachmentRenameAPI = this.api.attachmentRename;
		var attachment = this.selections[0];
		this.open({
			mtype: 'RS.wiki.pagebuilder.MoveOrRename',
			action: 'rename',
			filename: attachment.get('fileName') || attachment.get('name'),
			moveRenameCopyTitle: 'renameAttachement',
			namespaceIsVisible$: function() {
				return false;
			},

			filenameIsVisible$: function() {
				return true;
			},
			dumper: (function(self) {
				return function(params) {
					var attid = attachment.get('id');
					var newName = params.filename;
					this.ajax({
						url: attachmentRenameAPI,
						params: {
							docSysId: docSysId,
							docFullName: '',
							id: attid,
							choice: params.choice,
							filename: newName
						},
						success: function(resp) {
							var respData = RS.common.parsePayload(resp);
							this.parentVM.reload();
							if (!respData.success) {
								clientVM.displayError(this.localize('renameAttError', {
									msg: respData.message
								}));
								return;
							}
							// after successfully rename, modify the selection filename
							this.parentVM.selections[0].set('fileName', this.filename);
						},
						failure: function(resp) {
							clientVM.displayFailure(resp);
						}
					});
				}
			})(this)
		})
	},

	fileOnErrorCallback: function(manager, name, reason, resp) {
		var respData = RS.common.parsePayload(resp);
		if (respData.message) {
			clientVM.displayError(name + ': ' + respData.message);
		} else {
			clientVM.displayError(name + ': ' + RS.common.locale.FileUploadManager.uploadFailed+': '+reason);
		}
		var r = this.filesStore.findRecord('fileName', name, 0, false, false, true);
		if (r) {
			r.set('status', this.errorStatus);
		}
	},

	fileOnProgressCallback: function(manager, id, name, uploadedBytes, totalBytes) {
		var r = this.filesStore.findRecord('fileName', name, 0, false, false, true);
		if (r) {
			var percent = Math.round(uploadedBytes / totalBytes * 100);
			if (percent == 100) {
				r.set('status', this.processingStatus);
			} else {
				r.set('status', this.uploadingStatus + percent + '%');
			}
		}
	},

	fileOnCompleteCallback: function(manager, name, response, xhr) {
		if (response.success) {
			var r = this.filesStore.findRecord('fileName', name, 0, false, false, true);
			if (r) {
				r.set('status', this.completedStatus);
				r.set('sysUpdatedOn', new Date().getTime()+100);
				this.notifyDirty();
			}
		}
	},

	fileOnSubmittedCallback: function(manager, id, name) {
		var r = this.filesStore.findRecord('fileName', name, 0, false, false, true);
		if (r) {
			r.set('status', this.queuedStatus);
		} else {
			this.filesStore.add({
				fileName: name,
				status: this.queuedStatus,
				sysCreatedOn: new Date().getTime(),
				size: manager.getFile(id).size,
				uploadedBy: clientVM.user.name
			});
		}
	},

	filesOnAllCompleteCallback: function() {
		//this.loadFiles();
	},

	referGlobalAttachments: function(manager) {
		var me = this;
		this.open({
			mtype: 'RS.wiki.GlobalAttachmentPicker',
			dumper: function() {
				var ids = [];
				Ext.each(this.attachmentsSelections, function(att) {
					if (this.parentVM.filesStore.findBy(function(r) {
							if (att.get('fileName').toLowerCase() == (r.get('fileName') || r.get('name')).toLowerCase()) {
								//duplicates.push({
								//	name: att.get('fileName')
								//});
								return true;
							}
						}) != -1)
						return;
					ids.push(att.get('id'));
				}.bind(this));
				this.ajax({
					url: '/resolve/service/wiki/addGlobalAttachment',
					params: {
						docSysId: me.id,
						docFullName: me.name,
						attachIds: ids
					},
					success: function(resp) {
						var respData = RS.common.parsePayload(resp);
						this.parentVM.loadFiles();
						if (!respData.success) {
							clientVM.displayError(respData.message);
						}
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				});
				this.doClose();
			}
		});
	},

	selectionChanged: function(selected) {
		this.set('selections', selected);
	}
});

glu.defModel('RS.wiki.pagebuilder.FileUploadManagerFactory', function(cfg) {
	Ext.applyIf(cfg, RS.common.viewmodels.FileUploadManager);
	return cfg;
});

/*
glu.defModel('RS.wiki.pagebuilder.Attachments',{
	name : '',
	id : '',
	API : {
		attachmentRename : '/resolve/service/wikiadmin/attachment/rename',
	},
	renameAttachment: function(attachment, uploadmanager) {
		var docSysId = this.id;
		var attachmentRenameAPI = this.API.attachmentRename;
		this.open({
			mtype: 'RS.wiki.pagebuilder.MoveOrRename',
			action: 'rename',
			filename: attachment.get('fileName') || attachment.get('name'),
			moveRenameCopyTitle: 'renameAttachement',
			namespaceIsVisible$: function() {
				return false;
			},

			filenameIsVisible$: function() {
				return true;
			},
			dumper: (function(self) {
				return function(params) {
					var attid = attachment.get('id');
					var newName = params.filename;
					this.ajax({
						url: attachmentRenameAPI,
						params: {
							docSysId: docSysId,
							docFullName: '',
							id: attid,
							choice: params.choice,
							filename: newName
						},
						success: function(resp) {
							var respData = RS.common.parsePayload(resp);
							uploadmanager.loadFileUploads(true);
							if (!respData.success) {
								clientVM.displayError(this.localize('renameAttError', {
									msg: respData.message
								}));
								return;
							}
							// clientVM.displaySuccess(this.localize('renameAttSuccess'));
						},
						failure: function(resp) {
							clientVM.displayFailure(resp);
						}
					});
				}
			})(this)
		})
	},
	referGlobalAttachments: function(manager) {
		var me = this;
		this.open({
			mtype: 'RS.wiki.GlobalAttachmentPicker',
			dumper: function() {
				var ids = [];
				Ext.each(this.attachmentsSelections, function(att) {
					if (manager.getStore().findBy(function(r) {
							if (att.get('fileName').toLowerCase() == (r.get('fileName') || r.get('name')).toLowerCase()) {
								duplicates.push({
									name: att.get('fileName')
								});
								return true;
							}
						}) != -1)
						return;
					ids.push(att.get('id'));
				});
				this.ajax({
					url: '/resolve/service/wiki/addGlobalAttachment',
					params: {
						docSysId: me.id,
						docFullName: me.name,
						attachIds: ids
					},
					success: function(resp) {
						var respData = RS.common.parsePayload(resp);
						manager.loadFileUploads(true);
						if (!respData.success) {
							clientVM.displayError(respData.message);
							return;
						}
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				});
				this.doClose();
			}
		});
	},
	markAsGlobal : function(){

	},
	cancel: function() {
		this.doClose();
	},
});
*/