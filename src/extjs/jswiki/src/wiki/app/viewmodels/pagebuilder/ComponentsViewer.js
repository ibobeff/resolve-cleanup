glu.defModel('RS.wiki.pagebuilder.ComponentsViewer', {
        hidden: false,
        title: '',
        components: null,
        selectionModel: null,
        componentSelections: [],
        componentPicker: null,
        tpl: null,
        itemSelector: '',
        componentSelected: function(selected, selectionModel) {
                if (!this.selectionModel) {
                        this.selectionModel = selectionModel;
                }
                this.componentPicker.updateSelection(selectionModel);
        },
        addComponent: function(record) {
                var main = this.parentVM.parentVM.parentVM.parentVM.parentVM;
                !main.id.trim() && record.get('name') == 'Image'?
                    this.message({
                            title: main.localize('docNotExist'),
                            msg: main.localize('uploadImageOnNotExistDoc'),
                            buttons: Ext.MessageBox.OK
                    }) :
                    this.componentPicker.addComponent()
        }
});