glu.defModel('RS.wiki.TagConfiguration', {

	init: function() {
		this.tagStore.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {};
			var filter = [];
			if (this.searchTag) {
				filter = [{
					field: 'name',
					type: 'auto',
					condition: 'contains',
					value: this.searchTag
				}, {
					field: 'description',
					type: 'auto',
					condition: 'contains',
					value: this.searchTag
				}]
			}
			Ext.apply(operation.params, {
				operator: 'or',
				filter: Ext.encode(filter)
			})
		}, this)
		this.tagStore.load()
	},

	tagName: '',

	tagStore: {
		mtype: 'store',
		remoteSort: 'true',
		sorters: ['name'],
		fields: ['id','name'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/tag/listTags',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	searchTag: '',

	when_search_tag_changes_update_grid: {
		on: ['searchTagChanged'],
		action: function() {
			this.tagStore.loadPage(1)
		}
	},

	tagsSelections: [],

	tagColumns: [{
		header: '~~name~~',
		dataIndex: 'name',
		flex: 1
	}],

	add:function(){
		var tagIds = [];
		Ext.each(this.tagsSelections,function(tag){
			tagIds.push(tag.get('id'));
		});
		var params = {tagIds:tagIds};
		this.parentVM.doAddTags(params);
		this.cancel();
	},

	remove:function(){
		var tagIds = [];
		Ext.each(this.tagsSelections,function(tag){
			tagIds.push(tag.get('id'));
		});
		var params = {tagIds:tagIds};
		this.parentVM.doRemoveTags(params);
		this.cancel();
	},

	cancel: function() {
		this.doClose()
	}
})