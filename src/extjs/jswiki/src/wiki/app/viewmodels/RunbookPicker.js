glu.defModel('RS.wiki.RunbookPicker', {
    mock: false,
    title: '',
    createActionText: '',
    activeItem: 0,
    windowWidth$: function() {
        return this.activeItem == 0 ? (Ext.isIE8m ? 1200 : '80%') : 400
    },
    windowHeight$: function() {
        return this.activeItem == 0 ? (Ext.isIE8m ? 600 : '80%') : 200
    },
    runbookGrid: null,
    runbookTree: {
        mtype: 'treestore',
        fields: ['id', 'unamespace'],

        proxy: {
            type: 'ajax',
            url: '/resolve/service/nsadmin/list',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },
    moduleColumns: [],
    namespace: '',
    namespaceIsValid$: function() {
        if (/^\s*$/.test(this.namespace))
            return this.localize('invalidNamespaceConvention');
        return true;
    },
    name: '',
    nameIsValid$: function() {
        if (/^\s*$/.test(this.name))
            return this.localize('invalidNameConvention');
        return true;
    },
    runbookOnly: true,
    builderOnly: false,
    appendExtraParams: function() {
        this.runbookGrid.on('beforeload', function(store, operation, eOpts) {
            operation.params = operation.params || {};
            var filter = [];
            if (this.selectedNamespace && this.selectedNamespace.toLowerCase() != 'all')
                filter.push({
                    field: 'unamespace',
                    type: 'auto',
                    condition: 'equals',
                    value: this.selectedNamespace
                });
            if (this.builderOnly) {
                filter.push({
                    field: "uhasActiveModel",
                    type: "bool",
                    condition: "equals",
                    value: true
                });
                filter.push({
                    field: 'uhasResolutionBuilder',
                    type: 'bool',
                    condition: 'equals',
                    value: true
                });
                operation.params.type = 'AB_RUNBOOK';
            }
            Ext.apply(operation.params, {
                filter: Ext.encode(filter)
            })
        }, this);
    },

    init: function() {
        if (this.mock)
            this.backend = RS.actiontask.createMockBackend(true)
        var store = Ext.create('Ext.data.Store', {
            mtype: 'store',
            sorters: ['ufullname'],
            fields: ['id', 'ufullname', 'usummary', 'uresolutionBuilderId', 'uhasResolutionBuilder', {
                name: 'uhasActiveModel',
                type: 'bool'
            }, {
                name: 'uisRoot',
                type: 'bool'
            }].concat(RS.common.grid.getSysFields()),
            remoteSort: true,
            proxy: {
                type: 'ajax',
                url: (!this.runbookOnly && !this.builderOnly) ? '/resolve/service/wiki/list' : '/resolve/service/wikiadmin/listRunbooks',
                reader: {
                    type: 'json',
                    root: 'records'
                },
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
            }
        });
        this.set('runbookGrid', store);
        this.appendExtraParams();
        this.set('title', this.title || this.localize('title'));
        this.set('createActionText', this.createActionText || this.localize('createRunbook'));

        this.set('moduleColumns', [{
            xtype: 'treecolumn',
            dataIndex: 'unamespace',
            text: this.localize('namespace'),
            flex: 1
        }])

        this.runbookTree.on('load', function(store, node, records) {
            Ext.Array.forEach(records || [], function(record) {
                record.set({
                    leaf: true
                })
            })
        })

        this.runbookTree.setRootNode({
            unamespace: this.localize('all'),
            expanded: true
        })

        this.runbookGrid.load()
    },

    runbookSelected: null,

    selectedNamespace: '',
    searchDelimiter: '',
    when_selected_menu_node_id_changes_update_grid: {
        on: ['selectedNamespaceChanged'],
        action: function() {
            this.runbookGrid.load()
        }
    },

    menuPathTreeSelectionchange: function(selected, eOpts) {
        if (selected.length > 0) {
            var nodeId = selected[0].get('unamespace')
            this.set('selectedNamespace', nodeId)
        }
    },

    dumper: null,
    currentDocument : null,
    duplicatedDocumentErrorIsHidden : true,
    dump: function() {
        if (!this.dumper)
            return
        if(this.runbookSelected.get('ufullname') == this.currentDocument)
        {
            this.set('duplicatedDocumentErrorIsHidden', false);
            return;
        }
        if (Ext.isFunction(this.dumper))
            this.dumper(this.runbookSelected);
        else {
            var scope = this.dumper.scope;         
            this.dumper.dump.call(scope, [this.runbookSelected]);
        }
        this.doClose()
    },
    dumpIsVisible$: function() {
        return this.activeItem == 0
    },

    dumpIsEnabled$: function() {
        return !!this.runbookSelected
    },

    cancel: function() {
        if (this.activeItem == 1)
            this.selectExistingRunbook()
        else
            this.doClose()
    },
    activeItemChanged$: function() {
        if(!this.title)
            this.set('title', this.localize(this.activeItem == 0 ? 'title' : 'newAutomation'))
    },
    createRunbook: function() {
        this.set('activeItem', 1)
    },
    createRunbookIsVisible$: function() {
        return this.activeItem == 0 && !this.builderOnly;
    },
    createAutomationMeta: function() {
        this.set('activeItem', 2);
    },
    createAutomationMetaIsVisible$: function() {
        return this.activeItem == 0;
    },
    saveRunbookAndClose: function() {
        var selection = [{
            ufullname: this.namespace + '.' + this.name,
            get: function(name) {
                return this[name]
            }
        }];

        clientVM.handleNavigation({
            modelName: 'RS.wiki.Main',
            params: {
                name: this.namespace + '.' + this.name
            },
            target: '_blank'
        })

        if (!this.dumper)
            return
        if (Ext.isFunction(this.dumper))
            this.dumper(selection)
        else {
            var scope = this.dumper.scope;         
            this.dumper.dump.call(scope, selection)
        }
        this.doClose()
    },
    saveRunbookAndCloseIsVisible$: function() {
        return this.activeItem == 1;
    },

    //this is not elegant
    // saveAutomationAndClose: function() {
    //     this.ajax({
    //         url: '/resolve/service/resolutionbuilder/saveGeneral',
    //         jsonData: {
    //             generated: "",
    //             id: "",
    //             name: this.name,
    //             namespace: this.namespace,
    //             newWorksheet: true,
    //             noOfColumn: 1,
    //             summary: "",
    //             wikiId: "",
    //             wikiParameters: "[] "
    //         },
    //         success: function(resp) {
    //             var respData = RS.common.parsePayload(resp)
    //             if (!respData.success) {
    //                 clientVM.displayError(this.localize('saveAutomationMetaError', respData.message));
    //                 return;
    //             }
    //             clientVM.handleNavigation({
    //                 modelName: 'RS.wiki.resolutionbuilder.AutomationMeta',
    //                 target: '_blank',
    //                 params: {
    //                     id: respData.data.id
    //                 }
    //             });
    //             var selection = {
    //                 uresolutionBuilderId: respData.data.uresolutionBuilderId,
    //                 ufullname: respData.data.namespace + '.' + respData.data.name,
    //                 get: function(name) {
    //                     return this[name]
    //                 }
    //             };
    //             if (!this.dumper)
    //                 return
    //             if (Ext.isFunction(this.dumper))
    //                 this.dumper(selection)
    //             else {
    //                 var scope = this.dumper.scope;
    //                 this.dumper.dump.call(scope, selection)
    //             }
    //             this.doClose();
    //         },
    //         failure: function(resp) {
    //             clientVM.displayFailure(resp);
    //         }
    //     })
    // },
    saveAutomationAndClose: function() {
        var selection = [{
            ufullname: this.namespace + '.' + this.name,
            get: function(name) {
                return this[name]
            }
        }];

        clientVM.handleNavigation({
            modelName: 'RS.wiki.resolutionbuilder.AutomationMeta',
            params: {
                // activeTab: 2,
                // name: this.namespace + '.' + this.name,
                name: this.name,
                namespace: this.namespace
            },
            target: '_blank'
        })

        if (!this.dumper)
            return
        if (Ext.isFunction(this.dumper))
            this.dumper(selection)
        else {
            var scope = this.dumper.scope;
            this.dumper.dump.call(scope, selection)
        }
        this.doClose()
    },
    saveAutomationAndCloseIsEnabled$: function() {
        return this.isValid;
    },
    saveAutomationAndCloseIsVisible$: function() {
        return this.activeItem == 2;
    },
    createIsVisible$: function() {
        return this.activeItem == 1
    },
    createIsEnabled$: function() {
        return this.name && this.namespace
    },
    selectExistingRunbook: function() {
        this.set('activeItem', 0)
    },
    editRunbook: function(name) {
        if (this.runbookOnly)
            clientVM.handleNavigation({
                modelName: 'RS.wiki.Main',
                target: '_blank',
                params: {
                    activeTab: (this.runbookOnly ? 2 : 0),
                    name: name
                }
            })
        else if (this.builderOnly) {
            var idx = this.runbookGrid.findExact('ufullname', name);
            var r = this.runbookGrid.getAt(idx);
            clientVM.handleNavigation({
                modelName: 'RS.wiki.resolutionbuilder.AutomationMeta',
                target: '_blank',
                params: {
                    id: r.get('uresolutionBuilderId')
                }
            });
        }
    }
});