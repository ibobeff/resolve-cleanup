glu.defModel('RS.wiki.Rater', {
	wait: false,

	header: '',
	countTest: /^-{0,1}\d+$/,
	u1StarCount: 0,
	u1StarCountIsValid$: function() {
		return this.countTest.test(this.u1StarCount) ? true : this.localize('invalidCount');
	},
	u2StarCount: 0,
	u2StarCountIsValid$: function() {
		return this.countTest.test(this.u2StarCount) ? true : this.localize('invalidCount');
	},
	u3StarCount: 0,
	u3StarCountIsValid$: function() {
		return this.countTest.test(this.u3StarCount) ? true : this.localize('invalidCount');
	},
	u4StarCount: 0,
	u4StarCountIsValid$: function() {
		return this.countTest.test(this.u4StarCount) ? true : this.localize('invalidCount');
	},
	u5StarCount: 0,
	u5StarCountIsValid$: function() {
		return this.countTest.test(this.u5StarCount) ? true : this.localize('invalidCount');
	},
	ids: [],

	dumper: null,

	activate: function() {},
	init: function() {
		this.set('header', this.localize('Count'));
	},

	rate: function() {
		this.dumper({
			u1StarCount: this.u1StarCount,
			u2StarCount: this.u2StarCount,
			u3StarCount: this.u3StarCount,
			u4StarCount: this.u4StarCount,
			u5StarCount: this.u5StarCount
		});
		this.doClose();
	},

	rateIsEnabled$: function() {
		return !this.wait && this.isValid;
	},

	cancel: function() {
		this.doClose();
	}
});