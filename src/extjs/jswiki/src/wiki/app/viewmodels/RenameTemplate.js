glu.defModel('RS.wiki.RenameTemplate', {
	oldName: '',
	newName: '',
	action: '',
	wait: false,
	title: '',
	activate: function() {},
	init: function() {
		if (this.action === 'rename')
			this.set('title', this.localize('renameTitle'));
		else
			this.set('title', this.localize('copyTitle'));
	},
	sendReq: function() {
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/wikitemplate/' + this.action,
			params: {
				oldName: this.oldName,
				newName: this.newName
			},
			scope: this,
			success: function(resp) {
				var respData = null;
				try {
					respData = RS.common.parsePayload(resp);
				} catch (e) {
					clientVM.displayError(this.localize('invalidJSON'));
				}
				if (!respData.success) {
					clientVM.displayError(this.localize(this.action + 'ActionErrMsg') + '[' + respData.message + ']');
					return;
				}
				clientVM.displaySuccess(this.localize(this.action + 'ActionSucMsg'));
				this.parentVM.loadTemplates();
				this.parentVM.set('templatesSelections', []);
				this.doClose();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},
	sendReqIsEnabled$: function() {
		return !this.wait && this.isValid;
	},
	cancel: function() {
		this.doClose();
	},
	newNameIsValid$: function() {
		var rexp1 = /^[A-Za-z0-9_ ]+[\\.]([A-Za-z0-9_ ]+[\\.])*[A-Za-z0-9_ ]+$/;
		var rexp2 = /^[A-Za-z0-9_ ]+$/;
		return this.newName != null &&
			(rexp1.test(this.newName) || rexp2.test(this.newName)) &&
			this.newName != '' ? true : this.localize('invalidTemplateName');
	}
});