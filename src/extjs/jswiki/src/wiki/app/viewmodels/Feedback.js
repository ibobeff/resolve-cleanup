glu.defModel('RS.wiki.Feedback', {
	target: null,
	title$: function() {
		return this.markForReview ? this.localize('reviewThisPage') : this.localize('rateThisPage')
	},

	includeWorksheetLink: false,
	markForReview: false,
	useful: true,
	usefulIsVisible$: function() {
		return this.markForReview === false || this.markForReview === 'false'
	},
	rating: 0,
	ratingIsVisible$: function() {
		return this.markForReview === false || this.markForReview === 'false'
	},
	comment: '',
	submittingFeedback: false,
	submitFeedback: function() {
		this.set('submittingFeedback', true)
		this.ajax({
			url: '/resolve/service/wiki/submitSurvey',
			params: {
				docFullName: this.parentVM.name,
				useful: this.useful,
				rating: this.rating,
				comment:this.comment + (this.includeWorksheetLink ? Ext.String.format('<br/><br/>{2}: <a href="#RS.worksheet.Worksheet/id={0}" target="_blank">{1}</a>', clientVM.problemId, clientVM.problemNumber, this.localize('worksheet')) : '')
			},
			scope: this,
			success: function(r) {
				this.set('submittingFeedback', false)
				var response = RS.common.parsePayload(r)
				if (this.parentVM.infoBarDetail)
					this.parentVM.infoBarDetail.ratingSubmitted();
				if (response.success) {
					clientVM.displaySuccess(this.localize('feedbackSaved'))
					this.doClose()
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('submittingFeedback', false);
			}
		})
	},
	submitFeedbackIsEnabled$: function() {
		return !this.submittingFeedback && this.rating > 0
	},
	submitFeedbackIsVisible$: function() {
		return this.markForReview === false || this.markForReview === 'false'
	},

	requestingReview: false,
	requestReview: function() {
		this.set('requestingReview', true)
		this.ajax({
			url: '/resolve/service/wiki/flagDocument',
			params: {
				docFullName: this.parentVM.name,
				comment: this.comment + (this.includeWorksheetLink ? Ext.String.format('<br/><br/>{2}: <a href="#RS.worksheet.Worksheet/id={0}" target="_blank"> {1}</a>', clientVM.problemId, clientVM.problemNumber, this.localize('worksheet')) : '')
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.localize('reviewSaved'))
					this.parentVM.set('uisRequestSubmission', true);
					this.doClose()
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('requestingReview', false);
			}

		})
	},
	requestReviewIsVisible$: function() {
		return this.markForReview === true || this.markForReview === 'true'
	},
	requestReviewIsEnabled$: function() {
		return !this.requestingReview && this.comment
	}
})