glu.defModel('RS.wiki.LayoutPicker', {

	columnCount: 0,

	columns: {
		mtype: 'store',
		fields: ['name', {
			name: 'value',
			type: 'int'
		}],
		sorters: ['value'],
		proxy: {
			type: 'memory'
		},
		data: [{
			name: '1',
			value: 1
		}, {
			name: '2',
			value: 2
		}, {
			name: '3',
			value: 3
		}]
	},

	layoutSelected: function(selected) {
		this.set('columnCount', selected[0].get('value'))
	},

	init: function() {
		//determine which layout should be pre-selected and select it
		var shownColumnCount = 0;
		this.parentVM.columns.forEach(function(col) {
			if (col.showColumn) shownColumnCount++
		})
		this.set('columnCount', shownColumnCount)
	},

	apply: function() {
		//Apply column configuration
		for (var i = 0, cols = this.parentVM.columns, len = cols.length; i < len; i++) {
			cols.getAt(i).set('showColumn', i < this.columnCount)
		}
		this.doClose()
	},
	applyIsEnabled$: function() {
		return this.columnCount > 0
	},
	cancel: function() {
		this.doClose()
	}
})