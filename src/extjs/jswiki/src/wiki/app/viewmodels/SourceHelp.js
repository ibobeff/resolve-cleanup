glu.defModel('RS.wiki.SourceHelp', {

	mock: false,

	helpString: 'Doc.Wiki Help ',

	windowTitle$: function() {
		var title = this.localize('sourceHelpTitle') + (this.selectedItem ? ' - ' + this.selectedItem.substring(this.helpString.length).replace('_', ' ') : '')
		if (!this.parentVM) window.document.title = title
		return title
	},

	sourceHelp: {
		mtype: 'store',
		fields: ['ufullname'],
		sorters: ['ufullname'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wiki/listHelpDocs',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					if (typeof clientVM !== 'undefined') {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		}
	},

	sourceHelpColumns: [],
	sourceHelpSelections: [],

	selectHelp: '',
	search: '',
	activeItem: 0,
	selectedItem: '',

	when_search_changes_filter_store: {
		on: ['searchChanged'],
		action: function() {
			var me = this,
				searchText = me.search;
			me.sourceHelp.clearFilter()
			me.sourceHelp.addFilter(new Ext.util.Filter({
				filterFn: function(item) {
					return item.get('ufullname').substring(me.helpString.length).replace('_', ' ').toLowerCase().indexOf(searchText.toLowerCase()) > -1
				}
			}))
		}
	},

	itemClick: function() {
		if (this.sourceHelpSelections.length > 0) {
			this.set('selectedItem', this.sourceHelpSelections[0].get('ufullname'))
			this.set('activeItem', 1)
		}
	},

	enterPressed: function() {
		if (this.sourceHelpSelections.length > 0) this.selectRecord(this.sourceHelpSelections[0])
		else if (this.sourceHelp.getCount() > 0) this.selectRecord(this.sourceHelp.getAt(0))
	},

	selectRecord: function(record) {
		this.set('sourceHelpSelections', [record])
		this.itemClick()
	},


	detailDisplay$: function() {
		if (this.sourceHelpSelections.length > 0) {
			var selectedItem = this.sourceHelpSelections[0].get('ufullname');
			return selectedItem ? Ext.String.format('<iframe name="external" src="/resolve/service/wiki/view?wiki={0}&{1}" style="width:100%;height:100%;border:0px"></iframe>', selectedItem, this.wikiViewToken) : '';
		}
	},

	csrftoken: '',
	wikiViewToken: '',

	init: function() {
		var me = this;
		this.initToken();
		if (this.mock) this.backend = RS.wiki.createMockBackend(true)
		if (this.selectHelp) {
			this.sourceHelp.on('load', function(store, records) {
				if (records)
					Ext.Array.each(records, function(record) {
						if (record.get('ufullname') == this.selectHelp) {
							this.selectRecord(record)
							return false
						}
					}, this)
			}, this, {
				single: true
			})
		}
		this.set('sourceHelpColumns', [{
			dataIndex: 'ufullname',
			text: '~~name~~',
			flex: 1,
			renderer: function(value) {
				return value.substring(me.helpString.length).replace('_', ' ')
			}
		}])

		this.sourceHelp.load();
	},

	activate: function() {
		this.initToken();
	},

	initToken: function() {
		if (typeof clientVM !== 'undefined') {
			clientVM.getCSRFToken_ForURI('/resolve/wiki/wikiHelpExternal.jsp', function(token_pair) {
				this.csrftoken = token_pair[0] + '=' + token_pair[1];
			}.bind(this));
			clientVM.getCSRFToken_ForURI('/resolve/service/wiki/view', function(token_pair) {
				this.wikiViewToken = token_pair[0] + '=' + token_pair[1];
			}.bind(this));
		} else {
			var token_pair = this.getCSRFToken();
			token_pair = token_pair.split(":");
			this.updateCSRFToken(token_pair);

			this.getCSRFToken_ForURI('/resolve/service/wiki/view', function(token_pair) {
				this.wikiViewToken = token_pair[0] + '=' + token_pair[1];
			}.bind(this));
		}
	},

	close: function() {
		if (this.parentVM) this.doClose()
		else window.close()
	},

	back: function() {
		this.set('activeItem', 0)
		this.set('selectedItem', '')
	},
	backIsVisible$: function() {
		return this.activeItem == 1
	},

	externalClick: function() {
		window.open('/resolve/wiki/wikiHelpExternal.jsp?' + this.csrftoken + '&selectHelp=' + this.selectedItem, '_blank', 'height=500,width=500,menubar=no,status=no,toolbar=no')
		this.doClose()
	},

	getCSRFToken: function() {
		var xhr = window.XMLHttpRequest ? new window.XMLHttpRequest : new window.ActiveXObject("Microsoft.XMLHTTP");
		var csrfToken = {};
		xhr.open("POST", "/resolve/JavaScriptServlet", false);
		xhr.setRequestHeader("FETCH-CSRF-TOKEN", "1");
		xhr.send(null);

		var token_pair = xhr.responseText;

		return token_pair;
	},

	updateCSRFToken: function(csrftoken) {
		var token_name = csrftoken[0];
		var token_value = csrftoken[1];

		XMLHttpRequest.prototype.onsend = function(data) {
			this.setRequestHeader("X-Requested-With", "XMLHttpRequest")
			this.setRequestHeader(token_name, token_value);
		};
	},

	getCSRFToken_ForURI: function(uri, onSuccess, onFailure) {
		this.ajax({
			url : '/resolve/service/csrf/getCSRFTokenForPage',
			params : {
				uri: uri
			},
			success : function(resp){
				var respData = RS.common.parsePayload(resp);
				if(respData.success){
					if (Ext.isFunction(onSuccess)) {
						onSuccess(respData.data);
					}
				}
			},
			failure: function(resp) {
				if (Ext.isFunction(onFailure)) {
					onFailure();
				}
			}
		})
	},
})