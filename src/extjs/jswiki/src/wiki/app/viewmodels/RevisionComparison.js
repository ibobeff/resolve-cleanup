glu.defModel('RS.wiki.RevisionComparison', {
	revision1: 0,
	revision2: 0,
	id: '',
	namespace: '',
	name: '',
	version1: '',
	version2: '',
	version1Title: '',
	version2Title: '',
	type: '',
	init: function() {
		var v1 = this.revision1 > this.revision2 ? this.revision2 : this.revision1;
		var v2 = this.revision1 <= this.revision2 ? this.revision2 : this.revision1;
		this.set('version1Title', this.localize('versionTitle', {
			revision: v1
		}));
		this.set('version2Title', this.localize('versionTitle', {
			revision: v2
		}));
		this.ajax({
			url: '/resolve/service/wiki/revision/compare',
			params: {
				id: this.id,
				docFullname: '',
				type: this.type,
				rev1: this.revision1,
				rev2: this.revision2
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('getCompare'), respData.message);
					return;
				}
				var ver1 = respData.records[0];
				var ver2 = respData.records[1];
				this.set('version1', ver1.revision < ver2.revision ? ver1 : ver2);
				this.set('version2', ver1.revision >= ver2.revision ? ver1 : ver2);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},
	close: function() {
		this.doClose();
	},
	activeItem: 'content',
	contentTab: function() {
		this.set('activeItem', 'content');
	},
	content1$: function() {
		return this.version1.content;
	},
	content2$: function() {
		return this.version2.content;
	},
	contentTabIsPressed$: function() {
		return this.activeItem == 'content';
	},
	exceptionTab: function() {
		this.set('activeItem', 'abortModel');
	},
	abortModel1$: function() {
		return this.version1.abortModel;
	},
	abortModel2$: function() {
		return this.version2.abortModel;
	},
	exceptionTabIsPressed$: function() {
		return this.activeItem == 'abortModel';
	},
	decisionTreeTab: function() {
		this.set('activeItem', 'decisionTreeModel');
	},
	decisionTreeModel1$: function() {
		return this.version1.decisionTreeModel;
	},
	decisionTreeModel2$: function() {
		return this.version2.decisionTreeModel;
	},
	decisionTreeTabIsPressed$: function() {
		return this.activeItem == 'decisionTreeModel';
	},
	processTab: function() {
		this.set('activeItem', 'mainModel');
	},
	mainModel1$: function() {
		return this.version1.mainModel;
	},
	mainModel2$: function() {
		return this.version2.mainModel;
	},
	processTabIsPressed$: function() {
		return this.activeItem == 'mainModel';
	}
});