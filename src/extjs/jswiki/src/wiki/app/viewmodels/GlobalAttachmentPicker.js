glu.defModel('RS.wiki.GlobalAttachmentPicker', {
	store: {
		mtype: 'store',
		fields: ['id','fileName','size'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wikiadmin/attachment/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			},
			extraParams: {
				docFullName: 'System.Attachment',
				docSysId: ''
			}
		}
	},
	keyword: '',
	attachmentsSelections: [],
	dumper: null,
	init: function() {
		this.store.on('beforeload', function(store, op) {
			if (this.keyword)
				op.params = {
					filter: Ext.encode([{
						"field": "fileName",
						"type": "auto",
						"condition": "contains",
						"value": this.keyword
					}])
				};
		}, this);
		this.store.load();
	},
	dump: function() {
		this.dumper();
	},
	select: function() {
	},
	selectIsEnabled$: function() {
		return this.attachmentsSelections.length > 0;
	},
	recordSelections: [],
	close: function() {
		this.doClose();
	},
	search: function() {
		this.store.load();
	}
});