glu.defModel('RS.wiki.ResultViewer', {

	id: null,

	result: '',

	html$: function() {
		return this.result;
	},

	init: function() {
		this.ajax({
			url: '/resolve/service/result/task/detail',
			method: 'GET',
			params: {
				TASKRESULTID: this.id
			},
			success: function(resp) {
				this.set('result', resp.responseText);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	cancel: function() {
		this.close();
	}
})
