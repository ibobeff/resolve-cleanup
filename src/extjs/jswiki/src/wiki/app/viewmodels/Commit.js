glu.defModel('RS.wiki.Commit', {
	comment: '',
	postToSocial: false,
	reviewed: false,

	commentIsValid$: function() {
		if (this.postToSocial && !this.comment) return this.localize('commentRequired')
		return true
	},

	commitIsEnabled$: function() {
		return this.isValid; // return this.isValid && this.comment; TODO - support Version Control
	},
	commit: function() {
		this.parentVM.verifyCommitVersion(this.comment, this.postToSocial, this.reviewed);
		this.doClose()
		// this.ajax({
		// 	url: '/resolve/service/wiki/revision/commit',
		// 	params: {
		// 		id: this.parentVM.id,
		// 		comment: this.comment,
		// 		postToSocial: this.postToSocial
		// 	},
		// 	success: function(r) {
		// 		var response = RS.common.parsePayload(r)
		// 		if (response.success) {
		// 			clientVM.displaySuccess(this.localize('commitSuccess'))
		// 			this.parentVM.viewTab()
		// 			this.doClose()
		// 		} else clientVM.displayError(response.message)
		// 	},
		// 	failure: function(r) {
		// 		clientVM.displayFailure(r)
		// 	}
		// })
	},
	cancel: function() {
		this.doClose()
	}
})