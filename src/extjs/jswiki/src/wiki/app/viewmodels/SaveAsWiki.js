glu.defModel('RS.wiki.SaveAsWiki', {
	windowTitle$: function() {
		return this.localize('saveAs');
	},
	id: '',
	validationRegex : /^[0-9a-zA-Z_-]+$/,
	overwrite: false,
	name: '',
	nameIsValid$: function() {
		return /^[\w|\-]+[\w|\-| ]*$/.test(this.name) ? true : this.localize('invalidName');
	},
	namespace: '',
	namespaceIsValid$: function(){
		return this.validationRegex.test(this.namespace) ? true : this.localize('invalidNamespace');
	},
	saveAs: function() {
		var choice = this.overwrite ? 'overwrite' : 'skip';
		this.parentVM.doSaveAs(this.namespace, this.name, choice, function() {
			clientVM.displaySuccess(this.localize('wikiSavedAs'));
			this.doClose();
			clientVM.handleNavigation({
				modelName: this.rootVM.sir? 'RS.incident.PlaybookTemplate': 'RS.wiki.Main',
				params: {
					name: this.namespace + '.' + this.name
				}
			});
		}.bind(this));
	},
	saveAsIsEnabled$: function(){
		return this.namespace && this.name && this.namespaceIsValid == true && this.nameIsValid == true;
	},
	cancel: function() {
		this.doClose()
	}
})