glu.defModel('RS.wiki.AttachmentsAdmin', {
	displayName: '',

	columns: null,

	stateId: 'wikiattAdmin',

	wait: false,

	store: {
		mtype: 'store',

		fields: ['id', 'fileName', {
			type: 'int',
			name: 'size'
		}, 'uploadedBy', {
			name: 'uploadedOn',
			type: 'date',
			dateFormat: 'time',
			format: 'time'
		}, 'downloadUrl'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'fileName',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wikiadmin/attachment/list',
			extraParams: {
				docFullName: 'System.Attachment',
				docSysId: ''
			},
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	recordsSelections: [],

	activate: function() {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.loadRecords();
	},

	init: function() {
		if (this.mock) this.backend = RS.wiki.createMockBackend(true)
		this.set('displayName', this.localize('displayName'));
		var me = this;
		this.set('columns', [{
				header: '~~fileName~~',
				dataIndex: 'fileName',
				filterable: true,
				flex: 3,
				renderer: RS.common.grid.downloadLinkRenderer('downloadUrl')
			}, {
				header: '~~size~~',
				dataIndex: 'size',
				filterLabel: this.localize('sizeFilterLabel'),
				filterable: true,
				flex: 1,
				validator: function(value) {
					return /^\d*$/g.test(value) ? true : me.localize('invalidBytes')
				},
				renderer: function(value, metaData) {
					metaData.tdAttr = 'data-qtip="' + value + ' Bytes"';
					return Ext.util.Format.fileSize(value)
				}
			}
			// , {
			// 	header: '~~uploadedBy~~',
			// 	dataIndex: 'uploadedBy',
			// 	filterable: true,
			// 	flex: 1
			// }, {
			// 	header: '~~uploadedOn~~',
			// 	dataIndex: 'uploadedOn',
			// 	filterable: true,
			// 	flex: 1,
			// 	renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
			// }
		].concat(RS.common.grid.getSysColumns()));

		this.store.on('beforeload', function() {
			this.set('wait', true);
		}, this);

		this.store.on('load', function(store, recs, suc) {
			this.set('recordsSelections', []);
			/*
			if (!suc) {
				clientVM.displayError(this.localize('ListRecordsErr'))
			}
			*/
			this.set('wait', false);
		}, this);
	},

	loadRecords: function() {
		this.store.load();
	},

	sendReq: function(url, errMsg, sucMsg, params, lockScreen) {
		lockScreen.set('wait', true);
		var ids = [];
		Ext.each(this.recordsSelections, function(r, idx) {
			ids.push(r.get('id'));
		});
		var reqParams = {
			docSysId: '',
			docFullName: 'System.Attachment'
		};
		if (params)
			Ext.apply(reqParams, params);
		reqParams.ids = ids;
		this.ajax({
			url: url,
			params: reqParams,
			success: function(resp) {
				this.handleSucResp(resp, errMsg, sucMsg);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				lockScreen.set('wait', false);
			}
		});
	},

	handleSucResp: function(resp, errMsg, sucMsg) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize(errMsg) + '[' + respData.message + ']');
		}
		// else
		// 	clientVM.displaySuccess(this.localize(sucMsg));

		this.loadRecords();
	},

	showMessage: function(title, OpMsg, OpsMsg, confirm, url, errMsg, sucMsg, params) {
		var strFullName = [];
		Ext.each(this.recordsSelections, function(r, idx) {
			strFullName.push(r.get('fileName'));
		});
		this.message({
			title: this.localize(title),
			msg: this.localize(this.recordsSelections.length > 1 ? OpMsg : OpsMsg, {
				num: this.recordsSelections.length,
				names: strFullName.join(',')
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize(confirm),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				params = params || {};
				this.sendReq(url, errMsg, sucMsg, params, this);
			}
		});
	},

	upload: function() {
		this.open({
			mtype: 'RS.wiki.pagebuilder.AttachFile',
			name: 'System.Attachment',
			isEditMode: true
		});
	},

	uploadIsEnabled$: function() {
		return !this.wait;
	},

	index: function() {
		this.showMessage(
			'IndexAttachementsTitle',
			'IndexAttachementssMsg',
			'IndexAttachementsMsg',
			'confirm',
			'/resolve/service/wikiadmin/attachment/index',
			'IndexAttachementsErrMsg',
			'IndexAttachementsSucMsg'
		);
	},

	indexIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	doRename: function(params) {
		this.set('recordsSelections', this.recordsSelections[0]);
		this.showMessage(
			'rename',
			'RenamesMsg',
			'RenameMsg',
			'rename',
			'/resolve/service/wikiadmin/attachment/rename',
			'RenameErrMsg',
			'RenameSucMsg',
			params
		);
	},

	rename: function() {
		this.open({
			mtype: 'RS.wiki.pagebuilder.MoveOrRename',
			action: 'rename',
			namespace: this.recordsSelections[0].get('uname'),
			filename: this.recordsSelections[0].get('uname'),
			moveRenameCopyTitle: 'renameAttachement',
			type: 'Attachment',
			modelName: this.viewmodelName,
			selectionsLength: this.recordsSelections.length,
			dumper: (function(self) {
				return function(params) {
					params.id = self.recordsSelections[0].get('id');
					self.doRename(params)
				}
			})(this)
		});
	},

	renameIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length === 1;
	},

	deleteAttachments: function() {
		this.showMessage(
			'DeleteTitle',
			'DeletesMsg',
			'DeleteMsg',
			'confirm',
			'/resolve/service/wikiadmin/attachment/delete',
			'DeleteErrMsg',
			'DeleteSucMsg'
		);
	},

	deleteAttachmentsIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	}
});