glu.defModel('RS.wiki.ListDocuments', {	
    namespaceColumns: [],    
    documentGrid: null,
    documentSelected: null,
    selectedNamespace: '',
    searchDelimiter: '', 
    documentCols : '', 
    documentTree: {
        mtype: 'treestore',
        fields: ['id', 'unamespace'],
        proxy: {
            type: 'ajax',
            url: '/resolve/service/nsadmin/list',
            reader: {
                type: 'json',
                root: 'records'
            },
            listeners: {
                exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
            }
        }
    },  
    API : {
        listDocument : '/resolve/service/wiki/list'
    }, 
    when_selected_menu_node_id_changes_update_grid: {
        on: ['selectedNamespaceChanged'],
        action: function() {
            this.documentGrid.load()
        }
    },
    init: function() {   
        var store = Ext.create('Ext.data.Store', {
            mtype: 'store',
            sorters: ['ufullname'],
            fields: ['id', 'ufullname', 'usummary',{
                name: 'uhasActiveModel',
                type: 'bool'
            }, {
                name: 'uisRoot',
                type: 'bool'
            }].concat(RS.common.grid.getSysFields()),
            remoteSort: true,
            proxy: {
                type: 'ajax',
                url: this.API['listDocument'],
                reader: {
                    type: 'json',
                    root: 'records'
                },
                listeners: {
                    exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
                }
            }
        });
        this.set('documentGrid', store);
        this.set('documentCols', [{
			header: '~~name~~',
			dataIndex: 'ufullname',
			filterable: true,
			flex: 1
		}, {
			dataIndex: 'usummary',
			header: '~~summary~~',
			filterable: true,
			hidden: true,
			visible: false,
			flex: 1
		},{
			dataIndex: 'uhasActiveModel',
			header: '~~uhasActiveModel~~',
			tooltip: this.localize('uhasActiveModelTip'),
			width : 60,
			filterable : true,
			renderer: RS.common.grid.booleanRenderer()
		},{
			dataIndex: 'uisRoot',
			header: '~~uisRoot~~',
			tooltip: this.localize('uisRootTip'),
			width : 60,
			filterable : true,
			renderer: RS.common.grid.booleanRenderer()
		}].concat(RS.common.grid.getSysColumns()));

        this.appendExtraParams(); 
        this.set('namespaceColumns', [{
            xtype: 'treecolumn',
            dataIndex: 'unamespace',
            text: this.localize('namespace'),
            flex: 1
        }])
        this.documentTree.on('load', function(store, node, records) {
            Ext.Array.forEach(records || [], function(record) {
                record.set({
                    leaf: true
                })
            })
        })
        this.documentTree.setRootNode({
            unamespace: this.localize('all'),
            expanded: true
        })
        this.documentGrid.load();
    },
    windowWidth$: function() {
        return this.activeItem == 0 ? (Ext.isIE8m ? 1200 : '80%') : 400
    },
    windowHeight$: function() {
        return this.activeItem == 0 ? (Ext.isIE8m ? 600 : '80%') : 200
    },
    appendExtraParams: function() {
        this.documentGrid.on('beforeload', function(store, operation, eOpts) {
            operation.params = operation.params || {};
            var filter = [];
            if (this.selectedNamespace && this.selectedNamespace.toLowerCase() != 'all')
                filter.push({
                    field: 'unamespace',
                    type: 'auto',
                    condition: 'equals',
                    value: this.selectedNamespace
                });           
            Ext.apply(operation.params, {
                filter: Ext.encode(filter)
            })
        }, this);
    },   
    menuPathTreeSelectionchange: function(selected, eOpts) {
        if (selected.length > 0) {
            var nodeId = selected[0].get('unamespace'
            	)
            this.set('selectedNamespace', nodeId)
        }
    },  
	cancel: function() {
		this.doClose()
	},
	go: function() {
		this.cancel();
		this.parentVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			params: {
				name: this.documentSelected.get('ufullname')
			}
		});
	},
	goIsEnabled$: function() {
		return !!this.documentSelected;
	}
})