glu.defModel('RS.wiki.macros.SocialLink', {
	openInNewTab: false,
	preceedingText: '',
	to: '',
	text: '',
	displayText$: function() {
		return Ext.String.format('{1}<span class="rs-link">{0}</span>', this.text, this.preceedingText ? this.preceedingText + '<br/>' : '');
	},

	init: function() {},

	linkClicked: function() {
		if (this.openInNewTab) {
			if (clientVM) clientVM.handleNavigation({
				modelName: 'RS.social.Main',
				target: '_blank'
			})
		} else if (clientVM && clientVM.activeScreen && clientVM.activeScreen.screenVM) {
			clientVM.activeScreen.screenVM.fireEvent('sociallinkclicked', this)
		}
	}
})