/**
 * Model definition for Wiki History.  Keeps track of a store of history changes
 */
glu.defModel('RS.wiki.macros.History', {
	/**
	 * Store to keep track of history changes and display them in a grid
	 */
	historyStore: {
		mtype: 'store',
		fields: ['documentName', {
			name: 'version',
			convert: function(v, r) {
				return r.raw.version || r.raw.revision;
			}
		}, 'isStable', 'referenced', 'comment', 'createdBy', {
			name: 'sysCreatedOn',
			type: 'date',
			dateFormat: 'time',
			format: 'time'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wiki/getHistory',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	init: function() {
		this.historyStore.load({
			params: {
				docFullName: this.parentVM.documentName
			}
		});
	}
});