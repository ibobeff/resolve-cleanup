/**
 * Model definition for Feedback Results.  Keeps track of ratings and displays them in a chart and grid
 */
glu.defModel('RS.wiki.macros.FeedbackResults', {
	/**
	 * True to display the cancel button (if in a window) but defaults to false because we don't display a cancel button in a wiki document.
	 */
	isWindow: false,
	/**
	 * Height of the control, really only matters when displaying this in a window
	 */
	height: 400,
	/**
	 * Width of the control, really only matters when displaying this in a window
	 */
	width: 700,
	/**
	 * Rating value for the wiki document.  In the case of the results we want to leave it as 0, but we need it because the control is trying to bind the value
	 * to the field.  We really use the initialValue to properly display the avergae rating for this wiki document
	 */
	rating: 0,
	/**
	 * Average rating for this wiki document as calculated by averaging the star ratings of all the previous feedback
	 */
	initialValue: 0,

	/**
	 * modal flag to pass to the window to make sure that it displays properly in the wiki documents
	 */
	modal: false,

	/**
	 * The list of ratings from the server to display in the results panel.  This should be of the form
	 *
	 * 	ratings: [
	 * 		{
	 * 			rating: 5,
	 * 			date: '2012-06-15T16:29:35-07:00'	//formatting with 'c' (ISO Standard)
	 * 			comment: 'User Comment',
	 * 			questions: [1,2,3...]
	 * 		}
	 * 	]
	 */
	ratings: [],

	/**
	 * Store to keep track of the ratings to display in a grid
	 */
	ratingGridStore: {
		mtype: 'store',
		fields: ['id', {
			name: 'rating',
			type: 'number'
		}, {
			name: 'comment',
			type: 'string'
		}, {
			name: 'date',
			type: 'date',
			format: 'c',
			dateFormat: 'c'
		}],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'records'
			}
		}
	},

	ratingGridStoreSelections: [],

	/**
	 * Store to keep track of the results to display in the chart
	 */
	chartStore: {
		mtype: 'store',
		fields: ['name', 'data1', 'total']
	},
	docFullName: '',
	init: function() {
		//load the stores for the data from the server
		this.loadData();
	},

	/**
	 * Handler to load the Overview Rating (which is really the initial Value)
	 */
	loadData: function() {
		Ext.Ajax.request({
			url: '/resolve/service/wiki/getSurveyComments',
			params: {
				docFullName: this.parentVM.documentName || this.docFullName
			},
			scope: this,
			success: this.loadedSurveyComments,
			failure: function(r){
				clientVM.displayFailure(r);
			}
		})
	},
	loadedSurveyComments: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			if (response.data) {
				this.ratingGridStore.loadData(response.data.ratingDtos);
				this.chartStore.loadData(response.data.chartRatingDtos);
				this.set('initialValue', parseFloat(Ext.util.Format.number(response.data.rating, '0.0')));
				this.set('rating', parseFloat(Ext.util.Format.number(response.data.rating, '0.0')));
			}
		} else {
			clientVM.displayError(response.message);
		}
	},
	/**
	 * Server response handler to process the data for the chart store
	 * @param {Object} serverResponse
	 */
	loadedChart: function(serverResponse) {
		var response = RS.common.parsePayload(serverResponse);
		if (response.success) {
			this.chartStore.removeAll();
			this.chartStore.add(response.records);
		}
	},
	/**
	 * Handler to process when a chart bar is selected in order to change the filter on the grid so that only those star ratings are displayed
	 * @param {Object} item
	 */
	chartItemSelected: function(item) {
		var value = Number(item.storeItem.data.name.split(' ')[0]);
		this.ratingGridStore.fireEvent('ratingFilterChanged', this.ratingGridStore, value);
	},

	close: function() {
		this.doClose();
	},
	showCloseButtonOnPopup: function(btn) {
		// Only display the close button on popup
		if (this.modal) {
			var win = btn.up('window');
			btn.show();
		}
	},
	setPopupSize: function(win, height, width) {
		if (this.modal) {
			win.setWidth(Math.round(height * 0.8));
			win.setHeight(Math.round(width * 0.5));
		}
	}
});