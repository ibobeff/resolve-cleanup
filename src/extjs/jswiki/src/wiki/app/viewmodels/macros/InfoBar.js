/**
 * Model definition for an InfoBar.  Displays tabs of information in a wiki document
 */
glu.defModel('RS.wiki.macros.InfoBar', {
	/**
	 * The sysId of the wiki document to be used for passing the sysId to the social tab to properly load the social page.
	 */
	sysId: '',
	id: '',
	/**
	 * The name of the wiki document that we're looking at
	 */
	wikiName: '',
	/**
	 * True to display this tab, false	if the user doesn 't have rights to display this tab
	 */
	displaySocialTab: false,
	/**
	 * True to display this tab, false if the user doesn't have rights to display this tab
	 */
	displaySummaryTab: false,
	/**
	 * True to display this tab, false if the user doesn't have rights to display this tab
	 */
	displayFeedbackTab: true,
	/**
	 * True to display this tab, false if the user doesn't have rights to display this tab
	 */
	displayFeedbackResultsTab: true,
	/**
	 * True to display this tab, false if the user doesn't have rights to display this tab
	 */
	displayAttachmentsTab: true,
	/**
	 * True to display this tab, false if the user doesn't have rights to display this tab
	 */
	displayHistoryTab: true,
	/**
	 * True to display this tab, false if the user doesn't have rights to display this tab
	 */
	displayTagsTab: true,
	/**
	 * True to display this tab, false if the user doesn't have rights to display this tab
	 */
	displayPageInfoTab: true,

	autoDisplay: true,

	/**
	 * Height of the control.  If 0, then it will auto size the control to the maximum height available (which is the screen height);
	 */
	height: 0,
	configHeight: 0,

	/**
	 * Width of the control.  If 0, then it will auto size the control to the maximum width available (which is the screen width)
	 */
	width: 0,

	autoResize: true,

	/**
	 * List of tabs to keep track of the active item on the screen
	 */
	tabs: {
		mtype: 'activatorlist',
		focusProperty: 'selectedInfoTab',
		autoParent: true
	},
	/**
	 * The current active tab selected by the user
	 */
	selectedInfoTab: {
		mtype: 'viewmodel'
	},

	// InfoBar is now a Wiki component only (i.e. it's no longer part of the Wiki, like Table of Content)
	// so "var name = this.parentVM ? this.parentVM.name : this.wikiName" is no longer used
	documentName$: function() {
		return this.wikiName; 
	},

	usummary: '',

	// InfoBar is now a Wiki component only (i.e. it's no longer part of the Wiki, like Table of Content)
	// so "var content = this.parentVM ? this.parentVM.usummary : this.usummary" is no longer used
	summary$: function() {
		return Ext.String.htmlDecode(Ext.util.Format.nl2br(this.usummary))
	},

	init: function() {
		this.set('configHeight', this.height);

		var blank = this.model({
				mtype: 'BlankTab'
			}),
			social = this.model({
				mtype: 'Social'
			}),
			summary = this.model({
				mtype: 'Summary'
			});

		this.tabs.add(blank);

		if (this.displaySocialTab) {
			this.tabs.add(social)
		}

		if (this.displaySummaryTab) {
			this.tabs.add(summary)
		}

		if (this.displayFeedbackTab)
			this.tabs.add(this.model({
				mtype: 'Feedback'
			}))

		if (this.displayFeedbackResultsTab)
			this.tabs.add(this.model({
				mtype: 'FeedbackResults'
			}))

		if (this.displayAttachmentsTab)
			this.tabs.add(this.model({
				mtype: 'Attachments'
			}))

		if (this.displayHistoryTab)
			this.tabs.add(this.model({
				mtype: 'History'
			}))

		if (this.displayTagsTab)
			this.tabs.add(this.model({
				mtype: 'Tags'
			}))

		if (this.tabs.length > 0) this.set('selectedInfoTab', blank)
		if (this.autoDisplay && this.tabs.length > 1) Ext.defer(function() {
			this.set('selectedInfoTab', this.tabs.getAt(1))
		}, 500, this)
		if (this.displaySocialTab) Ext.defer(function() {
			this.set('selectedInfoTab', social)
		}, 500, this)
		if (this.displaySummaryTab) Ext.defer(function() {
			this.set('selectedInfoTab', summary)
		}, 500, this)
	},
	when_tab_changes_properly_set_height: {
		on: ['selectedInfoTabChanged'],
		action: function() {
			this.set('height', this.tabs.indexOf(this.selectedInfoTab) > 0 ? this.configHeight : 30);
		}
	},
	ratingSubmitted: function() {
		if (this.displayFeedbackResultsTab) {
			this.tabs.foreach(function(tab) {
				if (tab.viewmodelName == 'Feedback') {
					//tab.loadData();
					return false;
				}
			})
		}
	}
});