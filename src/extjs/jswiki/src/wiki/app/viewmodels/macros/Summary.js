glu.defModel('RS.wiki.macros.Summary', {

	summaryText$: function() {
		var content = this.getParentSummary();
		return Ext.String.htmlDecode(Ext.util.Format.nl2br(content));
	},
	getParentSummary : function(){
		return this.parentVM.summary;
	}

})