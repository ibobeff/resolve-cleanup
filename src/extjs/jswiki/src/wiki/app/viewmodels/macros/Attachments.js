/**
 * Model definition for Attachments.  Keeps track of the store that loads all the attachments
 */
glu.defModel('RS.wiki.macros.Attachments', {
	/**
	 * Store to track attachments and display in a grid
	 */
	attachmentsStore: {
		mtype: 'store',
		fields: ['sysId', 'downloadUrl', 'tableName', 'fileName', 'size', 'uploadedBy', {
			name: 'sysCreatedOn',
			type: 'date',
			dateFormat: 'time',
			format: 'time'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wiki/getAttachments',
			reader: {
				type: 'json',
				root: 'records'
			}
			/*
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
			*/
		}
	},
	isEditMode: true,     // to be launched from edit if true; false to be launched from view.
	attachmentColumns: [],

	init: function() {
		this.set('attachmentColumns', [{
			header: '~~fileName~~',
			flex: 1,
			dataIndex: 'fileName',
			renderer : function(value, metaData, record) {
				var name = value;		
				if (record.raw.global)
					name += ' <span style="font-weight:600;">(Global)</span>';
				return name;
			}
		}, {
			header: '~~fileSize~~',
			dataIndex: 'size',
			renderer: function(value) {
				return Ext.util.Format.fileSize(value);
			}
		}, {
			header: '~~uploadedBy~~',
			dataIndex: 'uploadedBy'
		}, {
			header: '~~uploadedOn~~',
			dataIndex: 'sysCreatedOn',
			width: 250,
			renderer: function(value) {
				return Ext.Date.format(new Date(value), 'Y-m-d g:i:s A');
			}
		}, {
			xtype: 'actioncolumn',
			hideable: false,
			width: 25,
			items: [{
				icon: '/resolve/images/arrow_down.png',
				tooltip: this.localize('download'),
				handler: function(grid, rowIndex, colIndex) {
					var rec = grid.getStore().getAt(rowIndex),
						url = rec.get('downloadUrl');
					if (url) grid.ownerCt.downloadURL(url);
				}
			}]
		}])
		this.attachmentsStore.load({
			params: {
				docFullName: this.parentVM.documentName,
				docSysId: ''
			}
		});
	}
});