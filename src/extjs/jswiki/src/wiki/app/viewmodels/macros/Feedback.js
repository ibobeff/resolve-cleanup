/**
 * Model definition for Feedback.  Keeps track of what the user has provided for feedback and reports that information to the server to be stored and displayed in the results tab
 */
glu.defModel('RS.wiki.macros.Feedback', {
	/**
	 * Formula for calculating the button alignment for the feedback view.  Defaults to left for info bar, but needs to be right in the dialog window
	 */
	buttonAlign$: function() {
		return 'left';
	},
	/**
	 * True to display the cancel button (if in a window) but defaults to false because we don't display a cancel button in a wiki document.
	 */
	isWindow: false,
	/**
	 * Height of the control, really only matters when displaying this in a window
	 */
	height$: function() {
		return this.isWindow ? (Ext.isGecko ? 270 : 240) : 300
	},
	/**
	 * Width of the control, really only matters when displaying this in a window
	 */
	width: 500,
	/**
	 * @cfg useful {Boolean} True if the article was voted as being useful false if not.  Leave unset if the user hasn't voted yet
	 */
	useful: '',
	usefulIsVisible$: function() {
		return !this.markForReview;
	},
	/**
	 * The rating that the user has voted on previously.  Leave unset (or 0) if the user hasn't voted yet
	 */
	rating: 0,
	ratingIsVisible$: function() {
		return !this.markForReview;
	},
	/**
	 * The average rating to currently display if no vote has been cast
	 */
	initialRating: 0,
	/**
	 * True to display the view survey results link
	 */
	viewSurvey: true,

	/**
	 * The user's feedback comment
	 */
	feedbackComment: '',

	lastReviewed: '',
	flagged: false,
	markForReview: false,

	flagCls$: function() {
		return (this.flagged || this.markForReview) ? 'rs-wiki-flagged' : 'icon-flag-alt'
	},

	/**
	 * The threshold for star rating in the survey before the detailed questions are automatically expanded.
	 * Defaults to 3, meaning if a user selects a rating of 3 or less, the detailed questions section will expand
	 */
	thresholdForDetailQuestions: 3,

	/**
	 * List of questions to display in the detailed questions fieldset if they want to answer them
	 */
	questions: {
		mtype: 'list'
	},
	requestReview: function() {
		this.ajax({
			url: '/resolve/service/wiki/flagDocument',
			params: {
				docFullName: this.parentVM.documentName,
				comment: this.feedbackComment //+ (this.includeWorksheetLink ? Ext.String.format('<br/><br/><a href="#RS.worksheet.Worksheet/id={0}">{2}: {1}</a>', clientVM.problemId, clientVM.problemNumber, this.localize('activeWorksheet')) : '')
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				this.set('feedbackComment', '');
				this.set('markForReview', false);
				this.set('useful', null);
				if (response.success) {
					clientVM.displaySuccess(this.localize('reviewSaved'))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}

		})
	},
	requestReviewIsVisible$: function() {
		return this.markForReview;
	},

	/**
	 * Button handler to submit feedback to the server with what the user has provided
	 */
	submitFeedback: function() {
		if (this.useful !== 'true' && this.useful !== 'false' && !this.markForReview) {
			this.message(this.localize('invalidUsefulTitle'), this.localize('invalidUsefulBody'));
			return;
		}
		if (this.rating < 1 && !this.markForReview) {
			this.message(this.localize('invalidRatingTitle'), this.localize('invalidRatingBody'));
			return;
		}
		var params = {
			useful: this.useful || 'false',
			rating: this.rating,
			comment: this.feedbackComment
		};
		params.docFullName = this.parentVM.documentName;
		this.ajax({
			url: '/resolve/service/wiki/submitSurvey',
			params: params,
			scope: this,
			success: this.submittedFeedback,
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		});
	},
	submitFeedbackIsVisible$: function() {
		return !this.markForReview;
	},
	/**
	 * Server response handler from the backend to deal with displaying success/failure information to the user
	 * @param {Object} serverResponse
	 */
	submittedFeedback: function(serverResponse) {
		var response = RS.common.parsePayload(serverResponse);
		if (!response.success) {
			clientVM.displayError(this.localize(this.markForReview) ? 'requestReviewErr' : 'submitFeedbackErr', response.message)
			return;
		}
		if (this.isWindow) this.doClose();
		else {
			this.set('useful', null);
			this.set('rating', 0);
			this.set('feedbackComment', '');
			if (this.parentVM.ratingSubmitted)
				this.parentVM.ratingSubmitted()
			clientVM.displaySuccess(this.localize(this.markForReview ? 'requestReviewSuccess' : 'submitFeedbackSuccess'));
		}
	},

	cancel: function() {
		this.doClose();
	},
	/**
	 * Formula for determining whether the detailed questions should be automatically displayed or not
	 */
	hideDetailedQuestions$: function() {
		return (this.rating > 0 ? this.rating > this.thresholdForDetailQuestions : true) && this.useful !== 'false';
	},
	init: function() {
		this.set('lastReviewed', this.localize('notReviewed'));
		Ext.Ajax.request({
			url: '/resolve/service/wiki/getUserSurveyValues',
			params: {
				documentName: this.parentVM.documentName
			},
			scope: this,
			success: this.loadedDocument,
			failure: function(r){
				clientVM.displayFailure(r);
			}
		});

		/*Pushed to 3.4.2
		Ext.Ajax.request({
			url : '/resolve/service/wiki/getSurveyQuestions',
			params : {
				documentName : this.parentVM.documentName
			},
			success : this.loadedQuestions,
			scope : this
		});*/
	},
	loadedDocument: function(serverResponse) {
		var response = RS.common.parsePayload(serverResponse);
		if (response) {
			this.set('flagged', response.data.flagged);
			this.set('rating', response.data.rating);
			// var lastReviewedDate = Ext.Date.parse(new Date(response.data.lastReviewedDate), 'c');
			if (!response.data.lastReviewedDate)
				return;
			var lastReviewedDate = new Date(response.data.lastReviewedDate);
			if (Ext.isDate(lastReviewedDate)) {
				if (Ext.Date.between(new Date(), lastReviewedDate, Ext.Date.add(lastReviewedDate, Ext.Date.YEAR, 1))) this.set('lastReviewed', Ext.Date.format(lastReviewedDate, 'M d, H:i'));
				else this.set('lastReviewed', Ext.Date.format(lastReviewedDate, 'M d, Y'));
			}
		}
	}
	/*  Pushed to 3.4.2,
	loadedQuestions: function(serverResponse) {
		var response = RS.common.parsePayload(serverResponse);
		//TODO: Populate the questions fieldset with questions
		this.questions.add({
			mtype: 'viewmodel',
			question: 'question1',
			labelWidth: 200,
			labelSeparator: '',
			answer: 0
		})
	}*/
});