var theWindow = getResolveRoot();
Ext.override(Ext.util.TaskRunner, {
	startTimer: function(timeout, now) {
		var me = this,
			expires = now + timeout,
			timerId = me.timerId;

		// Check to see if this request is enough in advance of the current timer. If so,
		// we reschedule the timer based on this new expiration.
		if (timerId && me.nextExpires - expires > me.interval) {
			theWindow.clearTimeout(timerId);
			timerId = null;
		}

		if (!timerId) {
			if (timeout < me.interval) {
				timeout = me.interval;
			}
			try {
				me.timerId = theWindow.setTimeout(me.timerFn, timeout);
			} catch (e) {
				Ext.Msg.alert('Warning', e.toString() + ' Some the system behaviors may not be correct.');
			}
			me.nextExpires = expires;
		}
	}
});
glu.defModel('RS.wiki.macros.ResultPoller', {
	resultStore: {
		mtype: 'store',
		fields: ['id', 'address',
			'completion',
			'condition',
			'esbaddr',
			'severity',
			'executeRequestId',
			'executeRequestNumber',
			'executeResultId',
			'problem',
			'process',
			'processNumber',
			'processId',
			'targetGUID',
			'actionTask',
			'actionTaskName',
			'actionTaskFullName',
			'actionResultId',
			'detail',
			'taskName',
			'taskSummary',
			'taskId',
			'taskFullName',
			'taskNamespace',
			'resultWikiLink',
			'descriptionWikiLink',
			'detail',
			'wiki',
			'nodeId',
			'activityId',
			'summary', 
			'sysId', {
				name: 'duration',
				type: 'int'
			},
			'taskTags', {
				name: 'hidden',
				type: 'boolean'
			}
		].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wiki/resultmacro',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		},
		listeners: {
			beforeload: function(store, operation, eOpts) {
				operation.params = operation.params || {}
					//Add in the filters from the actiontasks list along with the current wiki and problemId	
				Ext.apply(operation.params, {
					id: clientVM.problemId,
					//wiki: window['wikiDocumentName'],
					limit: clientVM.resultMacroLimit || 1000,
					hidden: false
				})
				if (!operation.params.id) return false
			}
		}
	},
	refreshCountMax: 60, 
	refreshCountNoStartMax: 5,
	refreshCountMin: 5,
	polling: false,
	task: null,
	refreshInterval: 5,
	initMixin: function() {
		if (!this.refreshCountMax) {
			this.refreshCountMax = 60;
		}
		if (!this.refreshInterval) {
			this.refreshInterval = 5;
		}
		this.createTask();
		if (clientVM.fromArchive)
			this.resultStore.getProxy().url = '';
		clientVM.on('problemIdChanged',this.problemIdChangedHandler, this);
	},		
	problemIdChangedHandler :  function() {
		if (clientVM.fromArchive)
			this.resultStore.getProxy().url = '';
		else
			this.resultStore.getProxy().url = '/resolve/service/wiki/resultmacro'
	},
	groupBy: function (array, f) {
		var groups = {};

		for (var i = 0; i < array.length; i++) {			
			var keyValues = f(array[i]).map(function (k) { 
				return k.toString(); 
			});

			var groupName = keyValues.join(',');
			groups[groupName] = groups[groupName] || [];
			groups[groupName].push(array[i]);
		}

		return Object.keys(groups).map(function (groupName) {
			return groups[groupName];
		});
	},

	getActiveRecords: function (records) {
		var data = [];
		var runbookName = '';

		for (var i = 0; i < records.length; i++) {
			data.push(records[i].data);
		}

		var recordGroups = this.groupBy(data, function (d) {
			if (d.executeRequestId && d.executeRequestId.indexOf('::') != -1) {
				runbookName = d.executeRequestId.split('::')[0];
			}
			return [runbookName];
		});				

		var groupTimes = [];

		for (var i = 0; i < recordGroups.length; i++) {
			groupTimes.push({
				index: i,
				createdOn: recordGroups[i][0].sysCreatedOn
			});
		}

		groupTimes.sort(function (a, b) {
			var result = 0;

			if (a.createdOn < b.CreatedOn) {
				result = 1;
			} else if (a.createdOn > b.CreatedOn) {
				result = -1;
			}

			return result;
		});

		var result = {};

		if (groupTimes.length > 0) {
			result = recordGroups[groupTimes[0].index];
		} 

		return result;
	},

	createTask: function() {
		var me = this;		
		var resultRunner = new Ext.util.TaskRunner();
		var task = resultRunner.newTask({
			fireOnStart: true,			
			currentRunbook: '',
			refreshCount: 0,
			refresh: function () {
				task.refreshCount = 0;
				task.currentRunbook = '';
			},

			run: function() {
				//Ensure progress indicators are shown for now during the load, we'll hide them once no more results are coming back
				//But if the task is ever restarted then we'll show them again in case they were hidden before
				if (task.refreshCount === 0) {
					var progressMacros = Ext.query('[class*=progressMacro]')
					Ext.Array.forEach(progressMacros, function(progress) {
						Ext.fly(progress).setVisibilityMode(Ext.dom.AbstractElement.DISPLAY).show()
					});

					var progressCompletedMacros = Ext.query('[class*=progressCompletedMacro]')
					Ext.Array.forEach(progressCompletedMacros, function(progress) {
						Ext.fly(progress).setVisibilityMode(Ext.dom.AbstractElement.DISPLAY).hide()
					});
				}

				me.set('polling', true);

				me.resultStore.load({
					callback: function(records, operation, success) {
						if (!success) {
							//console.log('success was false. task aborted.');
							me.task.stop();
							task.refresh();
							me.set('polling', false);
							return;
						}

						var response = RS.common.parsePayload(operation.response);

						if (response.success) {
							//Determine if start and end/abort tasks match, if they do then stop polling
							var startCount = 0, 
								endCount = 0, 
								isDone = false, 
								runbookName = '',
								activeRecords = me.getActiveRecords.call(me, records);

							for (var i = 0; i < activeRecords.length; i++) {
								var r = activeRecords[i];
								var name = r.taskName;

								if (r.executeRequestId && r.executeRequestId.indexOf('::') != -1) {
									runbookName = r.executeRequestId.split('::')[0];
								}

								if (name === 'start') { 
									startCount++;									

									if(runbookName && runbookName !== task.currentRunbook) {
										task.currentRunbook = runbookName;
										task.refreshCount = 0;
									}
								} else if (name === 'end' || name === 'abort') { 
									endCount++; 

									if(runbookName && runbookName !== task.currentRunbook) {
										task.currentRunbook = runbookName;
										task.refreshCount = 0;
									}
								}
							}

							task.refreshCount++;
							var hasSameNumberOfStartsAndEnds = startCount > 0 && task.refreshCount >= me.refreshCountMin && startCount === endCount;
							var taskHasTakenTooLong = response.data.open && task.refreshCount >= me.refreshCountMax;
							var taskAlreadyCompleted = response.data.open === 0 && task.refreshCount >= me.refreshCountMin && activeRecords.length && endCount >= startCount;
							var taskTookTooLongToStart = startCount === 0 && task.refreshCount >= me.refreshCountNoStartMax;
							isDone = hasSameNumberOfStartsAndEnds || taskHasTakenTooLong || taskTookTooLongToStart || taskAlreadyCompleted;

							if (isDone) {
								me.task.stop();
								task.refresh();
								me.set('polling', false);
								var progressMacros = Ext.query('[class*=progressMacro]')

								Ext.Array.forEach(progressMacros, function(progress) {
									Ext.fly(progress).setVisibilityMode(Ext.dom.AbstractElement.DISPLAY).hide();
								})

								var progressCompletedMacros = Ext.query('[class*=progressCompletedMacro]')
								Ext.Array.forEach(progressCompletedMacros, function(progress) {
									Ext.fly(progress).setVisibilityMode(Ext.dom.AbstractElement.DISPLAY).show();
								})

								//Update isDoneYet for legacy support - its possible that the document hasn't been reloaded yet, so this can throw exceptions
								try {
									var done = Ext.get('isDoneYet')

									if (done) {
										done.set({ value: 'STOP_REFRESH' });
									} else {
										Ext.DomHelper.append(document.body, {
											tag: 'input',
											type: 'hidden',
											name: 'isDoneYet',
											id: 'isDoneYet',
											value: 'STOP_REFRESH'
										});
									}
								} catch (e) {

								}

								clientVM.fireEvent('worksheetDone')
							}
						}
					}
				})
			},
			interval: this.refreshInterval >= 5 ? this.refreshInterval * 1000 : 5000
		});

		this.set('task', task);
	}
});
