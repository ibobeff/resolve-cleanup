function startTask(interval) {
	//refresh any models displayed on the page
	refreshModelLinks()

	try {
		//Change the isDoneYet div
		var isDoneYet = Ext.get('isDoneYet')
		if (!isDoneYet) {
			isDoneYet = Ext.DomHelper.append(document.body, {
				tag: 'input',
				type: 'hidden',
				name: 'isDoneYet',
				id: 'isDoneYet',
				value: ''
			}, true)
		}
		isDoneYet.set({
			value: ''
		})
	} catch (e) {}

	//Start the task to poll for more results
	var win = getResolveRoot();
	if (win) {
		clientVM.fireEvent('refreshResult');
		return
	}
	glu.log.info('discard this "startTask" coz the iframe has been detached..we should release the parent iframe')
	return 'discard'
}


glu.defModel('RS.wiki.macros.Results', {
	mixins: ['ResultPoller'],
	title: 'title',
	actionTasks: [],
	processedActionTasks: [],
	encodeSummary: true,
	order: 'DESC',
	collapsed: false,
	progress: false,
	activeWorksheet: false,
	filteredResults: {
		mtype: 'store',
		fields: ['id', 'address',
			'completion',
			'condition',
			'esbaddr',
			'severity',
			'executeRequestId',
			'executeRequestNumber',
			'executeResultId',
			'problem',
			'process',
			'processNumber',
			'processId',
			'targetGUID',
			'actionTask',
			'actionTaskName',
			'actionTaskFullName',
			'actionResultId',
			'detail',
			'taskName',
			'taskSummary',
			'taskId',
			'taskNamespace',
			'resultWikiLink',
			'descriptionWikiLink',
			'wiki',
			'nodeId',
			'summary', 'sysId', {
				name: 'duration',
				type: 'int'
			}
		].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'memory'
		}
	},

	worksheetResultColumns: [],
	titleDisplay$: function() {
		var img = ''
		if (this.polling && this.progress)
			img = '<img src="/resolve/images/loading.gif">&nbsp;'
		return img + this.title
	},
	init: function() {
		var me = this;
		this.set('worksheetResultColumns', [{
			text: '~~taskSummary~~',
			dataIndex: 'taskSummary',
			width: 250,
			renderer: function(value, metaData, record) {
				if (value) {
					var modelName = record.get('descriptionWikiLink')
					if (modelName) {
						if (modelName.indexOf('RS.') == 0 || modelName.indexOf('#RS.') == 0) {
							modelName = '{modelName: \'' + modelName + '\''
						} else if (modelName.indexOf('http') == 0) {
							modelName = '{modelName: \'RS.client.URLScreen\', params: {location: \'' + modelName + '\'}}'
						} else {
							modelName = '{modelName: \'RS.wiki.Main\', params: {name: \'' + modelName + '\'}}'
						}
						return Ext.String.format('<a class="rs-link wiki-macro-result-description-link-custom" href="#" onclick="clientVM.handleNavigation({0});return false;">{1}</a>', modelName, value)
					}
					return value
				}
				return ''
			},
			sortable: true,
			filterable: false
		}, {
			text: '~~summary~~',
			dataIndex: 'summary',
			flex: 1,
			sortable: false,
			filterable: false,
			renderer: function(value) {
				if (value.indexOf('<!-- DETAIL_TYPE_HTML -->') == 0 || value.indexOf('<!-- UI_TYPE_HTML -->') == 0 || !me.encodeSummary) {
					return Ext.util.Format.nl2br('<pre style="word-wrap:break-word;">' + clientVM.injectRsclientToken(value) + '</pre>')
				}
				return Ext.util.Format.nl2br('<pre style="word-wrap:break-word;">' + Ext.String.htmlEncode(value) + '</pre>')
			}
		}, {
			text: '~~createdOn~~',
			dataIndex: 'sysCreatedOn',
			renderer: function(value, meta, r, rowIdx) {
				return Ext.Date.format(value, clientVM.userDefaultDateFormat);
			},
			width: 200,
			sortable: true,
			filterable: false
		}, {
			text: '~~result~~',
			dataIndex: 'severity',
			renderer: function(value, metaData, record) {
				metaData.style = 'margin: 1px'
				metaData.tdCls = Ext.String.format('{0}-{1}', 'rs-worksheet-status', value.toLowerCase ? value.toLowerCase() : value)
				if (value) {
					var modelName = record.get('resultWikiLink')
					if (modelName) {
						if (modelName.indexOf('RS.') == 0 || modelName.indexOf('#RS.') == 0) {
							modelName = '{modelName: \'' + modelName + '\''
						} else if (modelName.indexOf('http') == 0) {
							modelName = '{modelName: \'RS.client.URLScreen\', params: {location: \'' + modelName + '\'}}'
						} else {
							modelName = '{modelName: \'RS.wiki.Main\', params: {name: \'' + modelName + '\'}}'
						}
						return Ext.String.format('<a class="rs-link wiki-macro-result-condition-link-custom" href="#" onclick="clientVM.handleNavigation({0});return false;">{1}</a>', modelName, value)
					}
					else {
						var detailId = record.raw.originalTaskId || record.get('id');
						return Ext.String.format('<a class="rs-link wiki-macro-result-description-link-default" href="#" onclick="clientVM.viewResult(\'{0}\');return false;">{1}</a>', detailId, value)
					}
				}
				return ''
			},
			width: 100,
			align: 'center',
			sortable: true,
			filterable: false
		}]);
		this.addGlobalEventListerners();
		this.resultStore.on('beforeload', function(store, op) {
			var fullNames = [];
			var names = []
			if (this.processedActionTasks.length > 0) {
				Ext.Array.forEach(this.processedActionTasks || [], function(actionTask) {
					if (actionTask.actionTask.indexOf('#') != -1)
						fullNames.push(actionTask.actionTask);
					else
						names.push(actionTask.actionTask);
				}, this)
				if (fullNames.length > 0 || names.length > 0)
					fullNames = fullNames.concat(['start#resolve', 'end#resolve']);
				op.params = op.params || {}
				Ext.apply(op.params, {
					operator: 'or',
					filter: Ext.encode([{
						field: 'taskFullName',
						type: 'terms',
						condition: 'equals',
						value: fullNames.join(',')
					}, {
						field: 'taskName',
						type: 'terms',
						condition: 'equals',
						value: names.join(',')
					}]),
					macrotype: 'results'
				});
			} else {
				op.params = op.params || {}
				Ext.apply(op.params, {
					macrotype: 'results'
				});
			}
			//console.info(fullNames);
		}, this);
		this.resultStore.on('load', function(store, records, successfull, eOpts) {
			//Correct the results for the store's records based on the provided action task configuration
			var filteredRecords = [];
			if (this.processedActionTasks.length > 0) {
				Ext.Array.forEach(this.processedActionTasks || [], function(actionTask) {
					Ext.Array.forEach(records || [], function(record) {
						if ((actionTask.task ? actionTask.task.toLowerCase() == record.get('taskName').toLowerCase() : true) && (actionTask.namespace ? actionTask.namespace.toLowerCase() == record.get('taskNamespace').toLowerCase() : true) && (actionTask.wiki ? actionTask.wiki.toLowerCase() == record.get('wiki').toLowerCase() : true) && (actionTask.nodeId ? actionTask.nodeId == record.get('nodeId').split(':')[record.get('nodeId').split(':').length - 1] : true)) {
							var recordParams = Ext.clone(record.data);
							Ext.apply(recordParams, {
								taskSummary: actionTask.description || recordParams.taskName,
								descriptionWikiLink: actionTask.descriptionWikiLink,
								resultWikiLink: actionTask.resultWikiLink,
								originalTaskId: record.get('id'),
								id: Ext.data.IdGenerator.get('uuid').generate()
							})
							filteredRecords.push(recordParams)
						}
					}, this)
				}, this)
			} else {
				Ext.Array.forEach(records || [], function(record) {
					filteredRecords.push(Ext.apply(Ext.clone(record.data), {
						taskSummary: record.get('taskName')
					}))
				})
			}
			this.filteredResults.loadData(filteredRecords)

			this.filteredResults.sort({
				property: 'sysUpdatedOn',
				direction: this.order
			});

		}, this)

		this.processActionTasks()

		this.resultStore.sort({
			property: 'sysUpdatedOn',
			direction: this.order.toLowerCase() == 'asc' || this.processedActionTasks.length == 0 ? 'ASC' : 'DESC'
		})

		this.refresh();
	},
	beforeDestroyComponent : function(){
		//Remove all global listeners
		this.removeGlobalEventListerners();
		//Destroy task
		this.task.destroy();
		//Remove this handler from document's event.
		this.fireEvent('removeEventReference');
	},
	addGlobalEventListerners : function(){	
		clientVM.on('refreshResult',this.refresh, this);
		clientVM.on('refreshActiveWorksheet', this.refreshActiveWorksheetHandler , this);
	},
	removeGlobalEventListerners : function(){
		clientVM.removeListener('refreshResult', this.refresh);	
		clientVM.removeListener('refreshActiveWorksheet', this.refreshActiveWorksheetHandler);
		//Add in Mixin 
		clientVM.removeListener('problemIdChanged',this.problemIdChangedHandler);	
	},
	refreshActiveWorksheetHandler : function() {
		this.refresh();
		this.activeWorksheet = true;
	},
	processActionTasks: function() {
		Ext.Array.forEach(this.actionTasks || [], function(actionTask) {
			if (actionTask) {
				this.processedActionTasks.push(processNewActionTask(actionTask))
			}
		}, this)
	},

	refresh: function() {
		this.activeWorksheet = false;
		this.task.stop();
		this.task.refresh();
		this.task.restart(this.refreshInterval * 1000);
	}
});

function processActionTask(actionTask, order) {
	//description>task#namespace{wiki::nodeId}=DescriptionLinkWiki|ResultLinkWiki
	var description = actionTask.substring(0, actionTask.indexOf('>'))
	var tail = actionTask.substring(actionTask.indexOf('>') + 1, actionTask.length)
	var task = tail.substring(tail.indexOf('>') + 1, tail.indexOf('#') == -1 ? (tail.indexOf('{') == -1 ? (tail.indexOf('=') > -1 ? tail.indexOf('=') : tail.length) : tail.indexOf('{')) : tail.indexOf('#'))
	tail = tail.substring(task.length)
	task = Ext.String.trim(task)
	tail = Ext.String.trim(tail.substring(tail.indexOf('#') == -1 ? (tail.indexOf('{') == -1 ? 0 : tail.indexOf('{')) : tail.indexOf('#') + 1));
	//tail = tail.substring(tail.indexOf('#') == -1 ? (tail.indexOf('{') == -1 ? 0 : tail.indexOf('{')) : tail.indexOf('#') + 1).trim()
	var namespace = '';
	if (tail.indexOf('{') > -1) namespace = Ext.String.trim(tail.substring(0, tail.indexOf('{')));
	if (!namespace && tail.indexOf('=') > -1) namespace = Ext.String.trim(tail.substring(0, tail.indexOf('=')));
	else if (!namespace && tail != actionTask) {
		if (tail.indexOf('{') > -1) namespace = Ext.String.trim(tail.substring(0, tail.indexOf('{')));
		else namespace = Ext.String.trim(tail);
	}
	if (namespace.indexOf('{') > -1) namespace = ''

	var wiki = Ext.String.trim(tail.substring(tail.indexOf('{') + 1, tail.indexOf('::')));
	var nodeId = Ext.String.trim(tail.substring(tail.indexOf('::') > -1 ? tail.indexOf('::') + 2 : 0, tail.indexOf('}')));
	var descriptionWikiLink = Ext.String.trim(tail.substring(tail.indexOf('=') > -1 ? tail.indexOf('=') + 1 : tail.length, tail.indexOf('|') > -1 ? tail.indexOf('|') : undefined));
	var resultWikiLink = tail.indexOf('|') > -1 ? Ext.String.trim(tail.substring(tail.indexOf('|') + 1)) : '';

	return {
		description: description,
		task: task,
		namespace: namespace,
		wiki: wiki,
		nodeId: nodeId,
		descriptionWikiLink: descriptionWikiLink,
		resultWikiLink: resultWikiLink,
		leaf: true,
		actionTask: task + (namespace ? '#' + namespace : ''),
		order: order || 0
	}
}