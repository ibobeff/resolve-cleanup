/**
 * Model definition for Tags
 */
glu.defModel('RS.wiki.macros.Tags', {
	/**
	 * The name of the wiki document that we're looking at
	 */
	documentName: '',
	/**
	 * Store to track attachments and display in a grid
	 */
	tagsStore: {
		mtype: 'store',
		fields: ['name'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wiki/getTags',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	init: function() {
		if (this.parentVM.parentVM)
			this.set('tagsStore', this.parentVM.parentVM.tags);
		else
			this.tagsStore.load({
				params: {
					docFullName: this.parentVM.documentName,
					docSysId: ''
				}
			});
	}

});