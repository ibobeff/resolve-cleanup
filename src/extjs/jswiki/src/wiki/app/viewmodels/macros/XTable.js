glu.defModel('RS.wiki.macros.XTable', {
	columns: [],
	columnsConfig: '',
	sql: '',
	drillDown: '',
	target: '',
	rows: '',

	store: {},

	showPager: false,

	init: function() {
		var decodedRow = Ext.decode(this.rows);
		this.set('rows', decodedRow ? decodedRow.split("#") : []);
		this.configureStore();
	},
	configureStore: function() {
		var split = this.columnsConfig.split('|'),
			cols = [],
			fields = [],
			col, field, fullSplit, store;

		Ext.Array.forEach(split, function(c, index) {
			col = {}
			field = {}
			if (c) {
				if (c.indexOf(':') == -1)
					col = {
						text: Ext.String.trim(c),
						dataIndex: 'col' + index
					};
				else {
					fullSplit = c.split(':')
					params = Ext.Object.fromQueryString(fullSplit[1] || '')
					if (params.width) 
						params.width = Number(params.width) || null;
					else 
						params.flex = 1;
					Ext.apply(col, params)
					col.text = col.text || Ext.String.trim(fullSplit[0] || '')
					col.dataIndex = col.dataIndex ? col.dataIndex.toLowerCase() : 'col' + index
					col.renderer = function(value, meta, record) {
						if (!meta.column.link)
							return value;
						var target = meta.column.linkTarget || '_blank'
						var link = new Ext.Template(meta.column.link).compile().apply(record.data);
						return Ext.String.format('<a href="{0}" target="{1}">{2}</a>', link, target, value);
					}
				}
			}

			//Fix up the column for the requirements that we've exposed through the xtable syntax
			switch (col.type) {
				case 'date':
					field.type = 'date'
					field.dateFormat = col.dataFormat || 'time'
					field.format = col.dataFormat || 'time'
					col.renderer = Ext.util.Format.dateRenderer(col.format || Ext.state.Manager.get('userDefaultDateFormat', 'Y-m-d G:i:s'))
					break;
				case 'boolean':
					col.renderer = RS.common.grid.booleanRenderer()
					col.align = 'center'
					break;
				default:
					break;
			}

			field.name = col.dataIndex

			cols.push(col)
			fields.push(field)
		})

		if (this.sql) {
			this.set('showPager', true)
			store = Ext.create('Ext.data.Store', {
				autoLoad: true,
				fields: fields,
				proxy: {
					type: 'ajax',
					url: '/resolve/service/dbapi/getRecordData',
					extraParams: {
						useSql: true,
						sqlQuery: this.sql,
						type: 'table'
					},
					reader: {
						type: 'json',
						root: 'records'
					},
					listeners: {
						exception: function(e, resp, op) {
							clientVM.displayExceptionError(e, resp, op);
						}
					}
				}
			})
		} else {
			//Go get the row data and parse it into the store
			var data = [];
			Ext.Array.forEach(this.rows, function(row) {
				var rowData = row.split('|'),
					rowObj = {};
				Ext.Array.forEach(rowData, function(c, index) {
					rowObj[(index < cols.length) ? cols[index].dataIndex || ('col' + index) : ('col' + index)] = Ext.String.trim(c)
				})
				data.push(rowObj)
			})
			store = Ext.create('Ext.data.Store', {
				fields: fields,
				proxy: {
					type: 'memory'
				},
				data: data
			})
		}

		this.set('columns', cols)
		this.set('store', store)
	},

	showEdit$: function() {
		return this.drillDown && this.drillDown.length > 0
	},

	editRecord: function(record) {
		if (this.drillDown) {
			var split = this.drillDown.split('?');
			var url = split[0];

			var rout = split.length > 1 ? split[1].match(/^#RS\..+\..+/) : '';
			var urlParams = !!rout && rout.length > 0 ? split[1].substring(0, split[1].indexOf(rout)) : split[1] || '';

			var params = {};
			if (url[url.length - 1] != '/')
				url += '/';
			Ext.Array.forEach(this.columns, function(column) {
				url = url.replace(new RegExp('\\^' + column.text + '\/'), record.get(column.dataIndex) + '/')
				params[column.dataIndex.toLowerCase()] = record.get(column.dataIndex.toLowerCase())
			})
			window.open(url + '?' + urlParams + rout + '&' + Ext.Object.toQueryString(params))
		}
	}
})