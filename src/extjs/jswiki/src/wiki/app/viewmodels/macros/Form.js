var formRegistry = [];

glu.defModel('RS.wiki.macros.Form', {
	name: '',
	collapsed: false,
	tooltip: '',
	title: '',
	popout: false,
	popoutHeight: 0,
	popoutWidth: 0,

	collapsible: false,

	titleDisplay$: function() {
		return (this.collapsed || this.popout ? '<i class="icon-play rs-icon" style="cursor:pointer;cursor:hand;"></i>&nbsp;&nbsp;' : '') + this.title
	},

	init: function() {
		this.set('collapsible', this.collapsed)

		if (this.popoutWidth === 0) this.set('popoutWidth', Ext.isIE8m ? 800 : '90%')
		if (this.popoutHeight === 0) this.set('popoutHeight', Ext.isIE8m ? 400 : '90%')

		formRegistry.push(this)
	},
	showpopout: function(popout) {}
})