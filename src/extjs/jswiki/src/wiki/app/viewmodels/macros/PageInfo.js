/**
 * Model definition for PageInfo.
 */
glu.defModel('RS.wiki.macros.PageInfo', {
	/**
	 * The name of the wiki document that we're looking at
	 */
	documentName: ''

});