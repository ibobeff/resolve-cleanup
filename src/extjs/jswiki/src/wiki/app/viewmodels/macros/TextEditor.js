
glu.defModel('RS.wiki.macros.TextEditor', {
	id: null,
	title: '',
	incidentId: null,
	activityId: null,
	activityName: null,
	notes: '',
	origNotes: '',
	lastCommentTimestamp: '',
	lastCommentUser: '',
	userDateFormat: '',
	isEditing: false,

	init: function() {
		if (this.id) {
			var texteditorDiv = document.getElementById(this.id);
			texteditorDiv.innerHTML = '<a class="hideShowIcon rs-social-button icon-file-text" onclick="JavaScript:expandCollapseNotes(\''+this.id+'\')"></a>' + texteditorDiv.innerHTML;

			this.userDateFormat = clientVM.getUserDefaultDateFormat();

			this.getNotes(function(){
				setTimeout(function() {
					this.hideAllNotes();
				}.bind(this), 100);
			}.bind(this));
		}

		this.addGlobalEventListerners();

	},

	addGlobalEventListerners : function(){	
		clientVM.on('showAllNotes', this.showAllNotesHandler, this);
		clientVM.on('hideAllNotes', this.hideAllNotesHandler, this);
	},

	showAllNotesHandler: function() {
		this.showAllNotes();
	},

	hideAllNotesHandler: function() {
		this.hideAllNotes();
	},

	removeGlobalEventListerners : function(){
		clientVM.removeListener('showAllNotes', this.showAllNotesHandler);
		clientVM.removeListener('hideAllNotes', this.hideAllNotesHandler);
	},

	beforeDestroyComponent : function(){
		//Remove all global listeners
		this.removeGlobalEventListerners();

		//Remove this handler from document's event.
		this.fireEvent('removeEventReference');
	},

	showLastComment$: function() {
		if (!this.lastCommentTimestamp) {
			return false;
		} else {
			return true;
		}
	},

	showNotes$: function() {
		if (this.isEditing || !this.notes) {
			return false;
		} else {
			return true;
		}
	},

	getNotes: function(onSuccess) {
		this.ajax({
			url: '/resolve/service/playbook/wikinote/getByParams',
			params: {
				componentId: this.id,
				incidentId: this.incidentId,
				activityId: this.activityId,
			},
			success: function(resp) {
				var response = RS.common.parsePayload(resp)
				if (response.success) {
					if (response.records.length) {
						this.set('notes', response.records[0].note);
						this.updateNotes();

						this.set('lastCommentUser', response.records[0].sysUpdatedBy);
						this.set('lastCommentTimestamp', response.records[0].sysUpdatedOn);
					}
					if (Ext.isFunction(onSuccess)) {
						onSuccess();
					}
				} else {
					clientVM.displayError(response.message)
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	saveNotes: function(onSuccess) {
		var params = {
			componentId: this.id,
			incidentId: this.incidentId,
			activityId: this.activityId,
			activityName: this.activityName,
			note: this.notes
		};
		this.ajax({
			url: '/resolve/service/playbook/wikinote/save',
			jsonData: params,
			success: function(resp) {
				var response = RS.common.parsePayload(resp)
				if (response.success) {

					this.set('lastCommentUser', response.data.sysUpdatedBy);
					this.set('lastCommentTimestamp', response.data.sysUpdatedOn);

					clientVM.displaySuccess(response.message);
					if (Ext.isFunction(onSuccess)) {
						onSuccess();
					}
				} else {
					clientVM.displayError(response.message)
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	showAllNotes: function() {
		var texteditorDiv = document.getElementById(this.id);
		var hideShowDiv = texteditorDiv.getElementsByClassName('hideShowIcon');
		var notesDiv = texteditorDiv.getElementsByClassName('texteditorNotes');
		hideShowDiv[0].classList.add('icon-file-text');
		hideShowDiv[0].classList.remove('icon-file-text-alt');
		notesDiv[0].style.display = 'block';
	},

	hideAllNotes: function() {
		var texteditorDiv = document.getElementById(this.id);
		var hideShowDiv = texteditorDiv.getElementsByClassName('hideShowIcon');
		var notesDiv = texteditorDiv.getElementsByClassName('texteditorNotes');
		hideShowDiv[0].classList.remove('icon-file-text');
		hideShowDiv[0].classList.add('icon-file-text-alt');
		notesDiv[0].style.display = 'none';
	},

	lastComment$: function() {
		if (this.lastCommentTimestamp) {
			return this.localize('lastComment', [this.lastCommentUser, Ext.Date.format(new Date(this.lastCommentTimestamp), this.userDateFormat)]);
		}
	},

	edit: function() {
		this.set('isEditing', true);
		this.origNotes = this.notes;
	},
	editIsVisible$: function() {
		return !this.isEditing;
	},

	updateNotes: function() {
		var texteditorDiv = document.getElementById(this.id);
		var notes = texteditorDiv.getElementsByClassName('x-component notes');
		notes[0].innerText = this.notes;
	},

	save: function() {
		this.saveNotes(function() {
			this.updateNotes();
			this.set('lastCommentTimestamp', new Date().getTime());
			this.set('isEditing', false);
	
			
		}.bind(this))
	},
	saveIsVisible$: function() {
		return this.isEditing;
	},

	cancel: function() {
		this.set('isEditing', false);
		this.set('notes', this.origNotes);
	},
	cancelIsVisible$: function() {
		return this.isEditing;
	}

})