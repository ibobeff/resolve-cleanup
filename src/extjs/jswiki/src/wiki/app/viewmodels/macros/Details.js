glu.defModel('RS.wiki.macros.Details', {
	mixins: ['ResultPoller'],
	actionTasks: [],
	processedActionTasks: [],
	encodeSummary: false,
	order: 'DESC',
	collapsed: false,
	progress: false,
	activeWorksheet: false,
	maximum: -1,
	offset: -1,
	onRender: '',
	filteredResults: {
		mtype: 'list'
	},
	titleDisplay$: function() {
		var img = ''
		if (this.polling && this.progress)
			img = '<img src="/resolve/images/loading.gif">&nbsp;'
		return img
	},
	init: function() {
		var me = this;

		this.filteredResults.on('lengthchanged', function() {
			me.set('detailMacroIsVisible', this.length >= 0);
		});
		this.addGlobalEventListerners();
		this.resultStore.on('beforeload', function(store, op) {
			var fullNames = [];
			var names = [];
			if (this.processedActionTasks.length > 0) {
				Ext.Array.forEach(this.processedActionTasks || [], function(actionTask) {
					if (actionTask.actionTask.indexOf('#') != -1)
						fullNames.push(actionTask.actionTask);
					else
						names.push(actionTask.actionTask);
				}, this)
				if (fullNames.length > 0 || names.length > 0)
					fullNames = fullNames.concat(['start#resolve', 'end#resolve']);
				op.params = op.params || {}
				Ext.apply(op.params, {
					filter: Ext.encode([{
						field: 'taskFullName',
						type: 'terms',
						condition: 'equals',
						value: fullNames.join(',')
					}, {
						field: 'taskName',
						type: 'terms',
						condition: 'equals',
						value: names.join(',')
					}]),
					macrotype: 'details'
				});
			} else {
				op.params = op.params || {}
				Ext.apply(op.params, {
					macrotype: 'details'
				});
			}
		}, this);	
		this.resultStore.on('load', function(store, records, successfull, eOpts) {
			// sort records in descending order before processing details
			if (records) {
				records.sort(function(a, b) {
					if (b.raw.sysCreatedOn > a.raw.sysCreatedOn)
						return 1;
					else if (b.raw.sysCreatedOn == a.raw.sysCreatedOn)
						return 0;
					else
						return -1;
				});
			}

			//Correct the results for the store's records based on the provided action task configuration
			var filteredRecords = [];
			if (this.processedActionTasks.length > 0) {
				Ext.Array.forEach(this.processedActionTasks || [], function(actionTask) {
					Ext.Array.forEach(records || [], function(record) {
						if ((actionTask.task ? actionTask.task.toLowerCase() == record.get('taskName').toLowerCase() : false) && (actionTask.namespace ? actionTask.namespace.toLowerCase() == record.get('taskNamespace').toLowerCase() : true) && (actionTask.wiki ? actionTask.wiki.toLowerCase() == record.get('wiki').toLowerCase() : true) && (actionTask.nodeId ? actionTask.nodeId == record.get('nodeId').split(':')[record.get('nodeId').split(':').length - 1] : true)) {
							var recordParams = Ext.clone(record.data);
							if (actionTask.description) {
								Ext.apply(recordParams, {
									taskSummary: actionTask.description
								})
							}
							Ext.apply(recordParams, {
								descriptionWikiLink: actionTask.descriptionWikiLink,
								resultWikiLink: actionTask.resultWikiLink
							})
							recordParams.nodeId = actionTask.nodeId || ''
							filteredRecords.push(recordParams)
						}
					}, this)
				}, this)
			} else {
				Ext.Array.forEach(records || [], function(record) {
					filteredRecords.push(Ext.apply(Ext.clone(record.data), {
						taskSummary: record.get('taskName')
					}))
				})
			}
			var aggregated = {},
				orderedKeys = [],
				origResultsLength = this.filteredResults.length;

			for (var i=0; i < filteredRecords.length; i++) {
				var filteredRecord = filteredRecords[i],
					key = filteredRecord.taskName + '#' + filteredRecord.taskNamespace + '<' + filteredRecord.nodeId + '>';

				if (orderedKeys.indexOf(key) == -1) {
					orderedKeys.push(key);
				}
				aggregated[key] = aggregated[key] || [];

				// store the "key" and set not duplicate
				filteredRecord.duplicate = false;
				filteredRecord.key = key;

				// determine if the new record is duplicate (already displayed in Details component in UI)
				for (var j=0; j < this.filteredResults.length; j++) {
					var filteredResult = this.filteredResults.getAt(j);
					if (filteredRecord.sysId == filteredResult.sysId) {
						filteredRecord.duplicate = true;
						break;
					}
				}

				aggregated[key].push(filteredRecord);
			}
			Ext.Array.forEach(orderedKeys, function(key) {
				var list = aggregated[key]
				for (var i = this.offset; i < list.length; i++) {
					if (this.maximum >= 0 && i >= this.maximum + this.offset) {
						return;
					}

					var newItem = list[i];
					newItem.timeStamp = new Date(newItem.sysUpdatedOn).getTime();

					// just add all if nothing currently displayed on the UI
					if (origResultsLength == 0) {
						if (i > this.offset)
							this.filteredResults.add(this.model({
								mtype: 'RS.wiki.macros.DetailSeparator'
							}))

						this.filteredResults.add(this.model(Ext.apply(newItem, {
							mtype: 'RS.wiki.macros.DetailsDisplay',
							onRender: this.onRender
						})))
					}
					else if (!newItem.duplicate) {
						var insertAtIdx = 0,
							includeSeparator = false,
							resultsItemOffset,
							resultsItemFound = 0;

						// determine where to insert the new record on the UI
						this.filteredResults.each(function(filteredResult, idx) {
							// if one level deep
							if (list.length == 1) {
								if (newItem.timeStamp < filteredResult.timeStamp) {
									insertAtIdx = idx +1;
									return false;
								}
							}
							// if multiple levels deep
							else {
								if (newItem.key == filteredResult.key) {
									insertAtIdx = idx;
									resultsItemFound++;
									includeSeparator = true;
									if (typeof(resultsItemOffset) == 'undefined') {
										// only set the same keys offset once
										resultsItemOffset = idx;
									}
									if (newItem.timeStamp < filteredResult.timeStamp) {
										insertAtIdx++;
									}
								}
							}
						});

						// determine what items to dropoff if we have reached our max items to display
						if ((this.maximum >= 1) && (resultsItemFound >= this.maximum)) {
							var removeAtIdx;
							if (this.maximum > 1) {
								removeAtIdx = (resultsItemFound-1)*2 + resultsItemOffset -1;
								// remove separator
								this.filteredResults.removeAt(removeAtIdx);
								// workaround to remove old record since removeAt(idx, 2) doesn't work
								this.filteredResults.removeAt(removeAtIdx);
							}
							else {
								includeSeparator = false;
								removeAtIdx = resultsItemFound + resultsItemOffset -1;
								this.filteredResults.removeAt(removeAtIdx);
							}

							// always insert at beginning of same key group
							insertAtIdx = resultsItemOffset;
						}
						else if (typeof(resultsItemOffset) != 'undefined') {
							// insert at beginning of same key group
							insertAtIdx = resultsItemOffset;
						}

						if (includeSeparator) {
							this.filteredResults.insert(insertAtIdx, this.model({
								mtype: 'RS.wiki.macros.DetailSeparator'
							}))
						}

						// finally insert the new record
						this.filteredResults.insert(insertAtIdx, this.model(Ext.apply(newItem, {
							mtype: 'RS.wiki.macros.DetailsDisplay',
							onRender: this.onRender
						})))
					}
				}
			}, this);
			// Ext.Array.forEach(filteredRecords || [], function(record) {
			// 	this.filteredResults.add(this.model(Ext.apply(record, {
			// 		mtype: 'RS.wiki.macros.DetailsDisplay',
			// 		onRender: this.onRender
			// 	})))
			// }, this)
		}, this)

		this.processActionTasks()

		if (!this.task.pending) {
			this.task.start(this.refreshInterval * 1000);
		}
	},
	beforeDestroyComponent : function(){
		//Remove all global listeners
		this.removeGlobalEventListerners();
		//Destroy task
		this.task.destroy();

		//Remove this handler from document's event.
		this.fireEvent('removeEventReference');
	},
	addGlobalEventListerners : function(){	
		clientVM.on('refreshResult', this.refreshResultHandler, this);
		clientVM.on('refreshActiveWorksheet', this.refreshActiveWorksheetHandler , this);
	},
	removeGlobalEventListerners : function(){
		clientVM.removeListener('refreshResult', this.refreshResultHandler);	
		clientVM.removeListener('refreshActiveWorksheet', this.refreshActiveWorksheetHandler);
		//Added in Mixin 
		clientVM.removeListener('problemIdChanged',this.problemIdChangedHandler);	
	},
	refreshResultHandler : function(){
		this.refresh();
		this.filteredResults.removeAll();
	},
	refreshActiveWorksheetHandler : function() {
		this.refresh();
		this.activeWorksheet = true;
	},
	detailMacroIsVisible: false,

	processActionTasks: function() {
		Ext.Array.forEach(this.actionTasks || [], function(actionTask) {
			if (actionTask && actionTask != '<br/>') {
				this.processedActionTasks.push(processNewActionTask(actionTask))
			}
		}, this)
	},
	refresh: function() {
		this.activeWorksheet = false;
		this.task.stop();
		this.task.refresh();
		this.task.restart(this.refreshInterval * 1000);
	}
})

glu.defModel('RS.wiki.macros.DetailsDisplay', {
	onRender: '',
	detailRendering$: function() {
		if (this.detail.indexOf('<!-- DETAIL_TYPE_HTML -->') == 0 || this.detail.indexOf('<!-- UI_TYPE_HTML -->') == 0) {
			return clientVM.injectRsclientToken(this.detail);
		}
		return Ext.util.Format.nl2br(Ext.String.htmlEncode(this.detail))
	}
})

glu.defModel('RS.wiki.macros.DetailSeparator', {});