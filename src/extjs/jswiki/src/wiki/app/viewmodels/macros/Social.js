/**
 * Model definition for Social.
 */
glu.defModel('RS.wiki.macros.Social', {
	socialDetail: {
		mtype: 'RS.social.Main',
		mode: 'embed',
		initialStreamType: 'DOCUMENT'
	},

	init: function() {
		this.socialDetail.set('clientVM', clientVM)
		this.socialDetail.set('id', this.parentVM.sysId)
		this.socialDetail.set('streamParent', this.parentVM.documentName)
	}
});