glu.defModel('RS.wiki.macros.Results2', {
	mixins: ['ResultPoller'],
	fields : ['title', 'order','refreshInterval','refreshCountMax','filter','showWiki','descriptionWidth',{   
    	name : 'autoHide',
    	type : 'boolean'
    },{   
    	name : 'autoCollapse',
    	type : 'boolean'
    },{   
    	name : 'encodeSummary',
    	type : 'boolean'
    },{   
    	name : 'progress',
    	type : 'boolean'
    },{   
    	name : 'selectAll',
    	type : 'boolean'
    },{   
    	name : 'includeStartEnd',
    	type : 'boolean'
    },{   
    	name : 'preserveTaskOrder',
    	type : 'boolean'
    },{
    	name : 'actionTasks',
    	type : 'raw'
    }],
	real: true,
	title: '',
	actionTasks: [],
	processedActionTasks: [],
	encodeSummary: true,
	descriptionWidth: 0,
	order: 'DESC',
	autoCollapse: false,
	progress: true,
	activeWorksheet: false,
	autoHide: false,
	selectAll: true,
	preserveTaskOrder: true,
	includeStartEnd: true,
	showWiki: '',
	useFilter: false,
	filter: '',
	filters: ['GOOD', 'WARNING', 'SEVERE', 'CRITICAL'],
	statusMap: {
		'UNKNOWN': 0,
		'GOOD': 1,
		'WARNING': 2,
		'SEVERE': 3,
		'CRITICAL': 4
	},
	filterTags: '',
	tags: [],
	sirProblemId : null,
	showHeader : true,
	titleDisplay$: function() {
		var img = ''
		if (this.polling && this.progress)
			img = '<img src="/resolve/images/loading.gif">&nbsp;'
		else
			img = (this.worstSeverity ? Ext.String.format('<i class="icon-circle rs-results-status-{0}"></i>&nbsp;', this.worstSeverity.toLowerCase()) : '')
		return img + this.title
	},

	worstSeverity: '',
	worstSeverityNumber: 1,
	filteredResults: {
		mtype: 'treestore',
		fields: ['id', 'address',
			'completion',
			'condition',
			'esbaddr',
			'severity',
			'executeRequestId',
			'executeRequestNumber',
			'executeResultId',
			'problem',
			'process',
			'processNumber',
			'processId',
			'targetGUID',
			'actionTask',
			'actionTaskName',
			'actionTaskFullName',
			'actionResultId',
			'detail',
			'taskName',
			'taskSummary',
			'taskFullName',
			'taskId',
			'resultWikiLink',
			'descriptionWikiLink',
			'wiki',
			'activityName',
			'activityId',
			'nodeId',
			'summary',
			'sysId', {
				name: 'duration',
				type: 'int'
			},
			'sysUpdatedOn',
			'order', {
				name: 'hidden',
				type: 'boolean'
			}
		].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'memory'
		}
	},

	filteredResultCount: 0,
	groupRecordsFound: 0,
	filteredRecordIds: {},
	filteredRecordProblemId: '',

	worksheetResultColumns: [],

	autoCollapseEvent: false,
	overrideAutoCollapse: false,

	init: function() {
		var me = this;
		if (this.filter) {
			this.set('filters', this.filter.split(','));
			for (var i=0, l=this.filters.length; i<l; i++) {
				this.filters[i] = this.filters[i].trim();
			}
			this.set('useFilter', true);
		}
		if (this.filterTags) this.set('tags', this.filterTags.split(','))

		this.filteredRecordProblemId = clientVM.problemId;

		var worksheetResultColumns = [{
			xtype: 'treecolumn',
			text: '~~taskSummary~~',
			dataIndex: 'taskSummary',
			width: this.descriptionWidth > 0 ? this.descriptionWidth : 450,
			tdCls: 'rs-wiki-result-description',
			renderer: function(value, metaData, record, rowIdx) {
				if (value) {
					var modelName = record.get('descriptionWikiLink')
					if (modelName) {
						if (modelName.indexOf('RS.') == 0 || modelName.indexOf('#RS.') == 0) {
							modelName = '{modelName: \'' + modelName + '\''
						} else if (modelName.indexOf('http') == 0) {
							modelName = '{modelName: \'RS.client.URLScreen\', params: {location: \'' + modelName + '\'}}'
						} else {
							modelName = '{modelName: \'RS.wiki.Main\', params: {name: \'' + modelName + '\'}}'
						}
						return Ext.String.format('<a class="rs-link wiki-macro-result-description-link-custom" href="#" onclick="clientVM.handleNavigation({0});return false;">{1}</a>', modelName, value)
					}
					return value;
				}
				return ''
			},
			sortable: true,
			filterable: false,
			hideable: true
		}, {
			text: '~~summary~~',
			dataIndex: 'summary',
			flex: 1,
			sortable: false,
			filterable: false,
			renderer: function(value, meta, r, rowIdx) {
				if (value.indexOf('<!-- DETAIL_TYPE_HTML -->') == 0 || value.indexOf('<!-- UI_TYPE_HTML -->') == 0 || !me.encodeSummary) {
					return Ext.util.Format.nl2br('<pre style="word-wrap:break-word;">' + clientVM.injectRsclientToken(value) + '</pre>')
				}
				return Ext.util.Format.nl2br('<pre style="word-wrap:break-word;">' + Ext.String.htmlEncode(value) + '</pre>')
			}
		}, {
			text: '~~createdOn~~',
			dataIndex: 'sysCreatedOn',
			renderer: function(value, meta, r, rowIdx) {
				return Ext.Date.format(value, clientVM.userDefaultDateFormat);
			},
			width: 200,
			sortable: true,
			filterable: false
		}, {
			text: '~~result~~',
			dataIndex: 'severity',
			renderer: function(value, metaData, record) {
				metaData.style = 'margin: 1px'
				metaData.tdCls = Ext.String.format('{0}-{1}', 'rs-worksheet-status', value.toLowerCase ? value.toLowerCase() : value)
				if (value) {
					var modelName = record.get('resultWikiLink')
					if (modelName) {
						if (modelName.indexOf('RS.') == 0 || modelName.indexOf('#RS.') == 0) {
							modelName = '{modelName: \'' + modelName + '\''
						} else if (modelName.indexOf('http') == 0) {
							modelName = '{modelName: \'RS.client.URLScreen\', params: {location: \'' + modelName + '\'}}'
						} else {
							modelName = '{modelName: \'RS.wiki.Main\', params: {name: \'' + modelName + '\'}}'
						}
						return Ext.String.format('<a class="rs-link wiki-macro-result-condition-link-custom" href="#" onclick="clientVM.handleNavigation({0});return false;">{1}</a>', modelName, value)
					}
					else{
						var detailId = record.get('sysId');
						return Ext.String.format('<a class="rs-link wiki-macro-result-description-link-default" href="#" onclick="clientVM.viewResult(\'{0}\');return false;">{1}</a>', detailId, value)
					}
				}
				return ''
			},
			width: 100,
			align: 'center',
			sortable: true,
			filterable: false
		}]

		if (this.sirProblemId) {
			worksheetResultColumns.unshift({
				text: this.localize('activity'),
				dataIndex: 'activityName'
			});
		}
		this.set('worksheetResultColumns', worksheetResultColumns);

		this.addGlobalEventListerners();
		if (this.real) {
			this.resultStore.on('beforeload', function(store, op) {
				var fullNames = [];

				function process(node) {
					if (!node.children)
						fullNames.push(node.actionTask);
						Ext.each(node.children, function(child) {
							process(child);
						});
					}
				Ext.each(this.processedActionTasks, function(node) {
					if (!node.children)
						fullNames.push(node.actionTask);
						Ext.each(node.children, function(child) {
							process(child);
						});
				});
				if (fullNames.length > 0)
					fullNames = fullNames.concat(['start#resolve', 'end#resolve']);
				op.params = op.params || {};
				Ext.apply(op.params, {
					macrotype: 'results2'
				});
				if (!this.selectAll && fullNames.length > 0) {
					Ext.apply(op.params, {
						filter: Ext.encode([{
							field: 'taskFullName',
							type: 'terms',
							condition: 'equals',
							value: fullNames.join(',')
						}])
					});
				}
				// Only true when in SIR "Automation Results" tab and an Activity filter is picked
				if (this.activityId) {
					Ext.apply(op.params, {
						activityId: this.activityId
					});
				} 
				// Only true when in SIR "Activities" tab and an Activity is selected
				else if (frameElement && frameElement.getAttribute('activityId')){
					Ext.apply(op.params, {
						activityId: frameElement.getAttribute('activityId')
					});
				} 
				// Only true in standalone Wiki pagebuilder view
				else if (this.showWiki) {
					Ext.apply(op.params, {
						wiki: this.showWiki
					});
				}
				//console.info(fullNames);
			}, this);
			this.resultStore.on('load', function(store, records, successfull, eOpts) {
				// flush the Results entries if the Worksheet number (ProblemNumber) has changed. This happens when user changes worksheet
				if (records && records.length && records[0].raw.problemId != this.filteredRecordProblemId) {
					this.filteredRecordProblemId = records[0].raw.problemId;
					this.filteredResults.setRootNode({
						children: []
					})
					this.filteredResultCount = 0;
				}

				var filteredRecords = [],
					filteredResultsRootNode = this.filteredResults.getRootNode();

				this.groupRecordsFound = 0;
				this.filteredRecordIds = {};

				filteredResultsRootNode.cascadeBy(function(child) {
					if (child.isLeaf()) {
						var sysId = child.get('sysId');
						this.filteredRecordIds[sysId] = true;
					}
				}, this);

				this.set('worstSeverity', 'GOOD');
				this.set('worstSeverityNumber', 1);

				var activityId = null;
				if (this.activityId) {
					activityId = this.activityId;
				} else if (frameElement && frameElement.getAttribute('activityId')){
					activityId = frameElement.getAttribute('activityId');
				}

				//Filter the results for the store's records based on the provided action task configuration
				var preFilteredRecords = [];
				Ext.Array.forEach(records || [], function(record) {
					if (this.useFilter && Ext.Array.indexOf(this.filters, record.get('severity')) == -1) {
						return;
					}
					if (!this.includeStartEnd) {
						//var isStartEndResolveTask = ['start#resolve', 'end#resolve'].indexOf(record.get('taskFullName')) != -1;
						//if (isStartEndResolveTask) {
						//	return;
						//}
						if (record.get('hidden')) {
							return;
						}
					}

					var recordActivityId = record.get('activityId');
					if (recordActivityId) {
						// Filter out records that don't belong to this SIR's Activity
						if (activityId && activityId != recordActivityId) {
							return;
						}
	
						if (this.sirProblemId) {
							if (this.isModal) {
								record.set('activityName', this.parentVM.activityFilter);
							} else if (this.parentVM.activityIdMap[recordActivityId]) {
								record.set('activityName', this.parentVM.activityIdMap[recordActivityId]);
							} else {
								record.set('activityName', 'N/A');
							}
						}
					} else if (this.sirProblemId) {
						// If "Filter on Activity" in SIR, don't include records with recordActivityId=null
						if (activityId) {
							return;
						}
						// If not filtering on SIR's Activity, then include all records (even those with recordActivityId=null)
						record.set('activityName', 'N/A');
					}

					preFilteredRecords.push(record);

				}, this)

				if (this.selectAll) {
					Ext.Array.forEach(preFilteredRecords || [], function(record) {
						if (this.worstSeverityNumber < this.statusMap[record.get('severity')]) {
							this.set('worstSeverityNumber', this.statusMap[record.get('severity')])
							this.set('worstSeverity', record.get('severity'))
						}

						var sysId = record.get('sysId');
						if (this.filteredResultCount == 0 || !this.filteredRecordIds[sysId]) {
							filteredRecords.push(Ext.apply(Ext.clone(record.data), {
								taskSummary: record.get('taskSummary') || record.get('taskName'),
								leaf: true
							}))
							this.filteredResultCount++;
							this.filteredRecordIds[sysId] = true;
						}
					}, this)
				}
				else if (this.processedActionTasks.length > 0) {
					Ext.Array.forEach(this.processedActionTasks || [], function(actionTask) {
						var d = this.loadActionTasksData(preFilteredRecords, actionTask, actionTask.order);
						if (d.length) {
							if (this.filteredResultCount == 0 || actionTask.leaf) {
								// no records in grid or at root level, just add
								filteredRecords = filteredRecords.concat(d);
							} else {
								this.loadGroupData(filteredResultsRootNode, d);
								if (this.groupRecordsFound == 0) {
									filteredRecords = filteredRecords.concat(d);
								}
							}
						}
					}, this)
				}

				if (filteredRecords.length + this.groupRecordsFound == 0) {
					if (this.filteredResultCount == 0) {
						this.set('worstSeverity', 'UNKNOWN');
						this.set('worstSeverityNumber', 0);
	
						this.filteredResults.setRootNode({
							children: filteredRecords
						})
					}
				}
				else {
					Ext.Array.forEach(filteredRecords || [], function(filteredRecord) {
						filteredResultsRootNode.appendChild(filteredRecord);
					}, this)

					if (this.preserveTaskOrder) {
						if (this.filteredResults.sorters.items.length) {
							this.filteredResults.sort(this.filteredResults.sorters.items[0].property, this.filteredResults.sorters.items[0].direction);
						}
					}
				}

				if (this.autoHide && this.filteredResultCount) {
					this.filteredResults.fireEvent('showWorksheet');
				}

				if (this.autoCollapse && !this.overrideAutoCollapse) {
					// if not ignoring autoCollapse setting, flag this as an autoCollapse Event
					this.autoCollapseEvent = true;
					if (this.statusMap[this.worstSeverity] > 1) {
						this.filteredResults.fireEvent('expandWorksheet');
					} else {
						this.filteredResults.fireEvent('collapseWorksheet');
					}
					// reset the autoCollapse Event flag after firing off the event
					setTimeout(function() {
						this.autoCollapseEvent = false;
					}.bind(this), 50);
				}

				if (this.autoHide && this.filteredResultCount == 0) {
					this.filteredResults.fireEvent('hideWorksheet');
				}

				if (this.preserveTaskOrder && !this.selectAll) {
					this.filteredResults.sort([{
						property: 'order',
					}, {
						property: 'sysUpdatedOn',
						direction: this.order
					}]);
				} else {
					this.filteredResults.sort({
						property: 'sysUpdatedOn',
						direction: this.order
					});
				}

			}, this)
		}

		var processedActionTasks = processActionTasks(this.actionTasks, 0);
		this.set('processedActionTasks', processedActionTasks);
		this.resultStore.sort({
			property: 'sysUpdatedOn',
			direction: this.order.toLowerCase() == 'asc' || this.processedActionTasks.length == 0 ? 'ASC' : 'DESC'
		})

		this.refresh();
		if(!clientVM.showCreatedOnFlagLoaded) {
			clientVM.showCreatedOnFlagLoaded = true;
			Ext.Ajax.request({
				scope: this,
				url: '/resolve/service/sysproperties/getSystemPropertyByName',
				params: {
					name: 'resultmacro.createdon.hide'
				},
				success: function(resp) {
					var respData = RS.common.parsePayload(resp);
					if (respData.success) {
						var show = respData.data && respData.data.uvalue?  (respData.data.uvalue == 'false'): true;
						clientVM['showCreatedOn'] = show;
						clientVM.fireEvent('showCreatedOnFlagLoaded', show);
					} else {
						clientVM.displayError(respData.message);
					}
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			});
		}
	},
	resultRender: function(cols) {
		this.cols = cols;
	},
	setCreatedOnVisible: function(show) {
		if (Array.isArray(this.cols)) {
			this.cols.forEach(function(col) {
				if(col.dataIndex == 'sysCreatedOn') {
					col.setVisible(show);
					return false;
				}
			});
		}
	},	
	beforeDestroyComponent : function(){
		//Remove all global listeners
		this.removeGlobalEventListerners();
		//Destroy task
		this.task.destroy();

		//Remove this handler from document's event.
		this.fireEvent('removeEventReference');
	},
	addGlobalEventListerners : function(){
		clientVM.on('refreshResult',this.refreshResultHandler, this);
		clientVM.on('refreshActiveWorksheet', this.refreshActiveWorksheetHandler , this);
		clientVM.on('showCreatedOnFlagLoaded', this.setCreatedOnVisible, this);
	},
	removeGlobalEventListerners : function(){
		clientVM.removeListener('refreshResult', this.refreshResultHandler);
		clientVM.removeListener('refreshActiveWorksheet', this.refreshActiveWorksheetHandler);
		//Add in Mixin
		clientVM.removeListener('problemIdChanged',this.problemIdChangedHandler);
		clientVM.removeListener('showCreatedOnFlagLoaded', this.setCreatedOnVisible);
	},
	refreshActiveWorksheetHandler : function() {
		this.refresh();
		this.activeWorksheet = true;
	},
	refreshResultHandler : function() {
		this.refresh(true);
	},
	flushResults: function() {
		this.filteredRecordProblemId = '';
		this.filteredResults.setRootNode({
			children: []
		})
		this.filteredResultCount = 0;
	},
	actionTaskTagMatchesRecord: function(tagString, record) {
		var tags = tagString.split(','),
			match = false;

		Ext.Array.each(tags, function(tag) {
			if (Ext.Array.indexOf(record.get('taskTags'), tag) > -1) {
				match = true
				return false
			}
		})

		return match
	},

	getChildObject: function(data) {
		if (Ext.isArray(data.children) && data.children.length > 0) {
			 return this.getChildObject(data.children[0]);
		}
		else {
			 return data;
		}
	},

	loadGroupData: function(node, data) {
		if (node.isLeaf()) {
			var record = this.getChildObject(data);
			node.insertChild(0, record);
			this.set('groupRecordsFound', ++this.groupRecordsFound);
		}
		else {
			for (var i=0; i < data.length; i++) {
				// if data has children, then traverse some more
				if (Ext.isArray(data[i].children) && data[i].children.length > 0) {
					for (var j=0; j < node.childNodes.length; j++) {
						if (node.childNodes[j].get('taskSummary') == data[i].taskSummary) {
							this.loadGroupData(node.childNodes[j], data[i].children);
						}
					}
				}
				else {
					// data has no children so we are at the leaf, insert into node
					node.insertChild(0, data[i]);
					this.set('groupRecordsFound', ++this.groupRecordsFound);
				}
			}
		}
	},

	loadActionTasksData: function(records, actionTask, order) {
		var data, actionTasksData = [];
		Ext.Array.forEach(records || [], function(record) {
			if ((actionTask.task ? actionTask.task.toLowerCase() == record.get('taskName').toLowerCase() : (actionTask.tags ? true : false)) && (actionTask.namespace ? actionTask.namespace.toLowerCase() == record.get('taskNamespace').toLowerCase() : true) && (actionTask.wiki ? actionTask.wiki.toLowerCase() == record.get('wiki').toLowerCase() : true) && (actionTask.nodeId ? actionTask.nodeId == record.get('nodeId').split(':')[record.get('nodeId').split(':').length - 1] : true) && (actionTask.tags ? this.actionTaskTagMatchesRecord(actionTask.tags, record) : true)) {
				var sysId = record.get('sysId');
				if (this.filteredResultCount == 0 || !this.filteredRecordIds[sysId]) {
					if (!this.useFilter || Ext.Array.indexOf(this.filters, record.get('severity')) > -1) {
						data = Ext.clone(record.data);
						Ext.apply(data, {
							taskSummary: actionTask.description || data.taskSummary || actionTask.task,
							descriptionWikiLink: actionTask.descriptionWikiLink,
							resultWikiLink: actionTask.resultWikiLink,
							leaf: true,
							iconCls: 'no-icon',
							originalTaskId: record.get('id'),
							id: Ext.data.IdGenerator.get('uuid').generate(),
							order: order
						})
						actionTasksData.push(data)
						this.filteredResultCount++;
						this.filteredRecordIds[sysId] = true;
					}
				}
				if (this.worstSeverityNumber < this.statusMap[record.get('severity')]) {
					this.set('worstSeverityNumber', this.statusMap[record.get('severity')])
					this.set('worstSeverity', record.get('severity'))
				}
			}
		}, this)

		if (Ext.isArray(actionTask.children) && actionTask.children.length > 0) {
			data = {
				children: [],
				expanded: !actionTask.autoCollapse,
				iconCls: 'no-icon',
				order: order
			}
			var worstChild = 1,
				worstSeverity = 'GOOD';
			Ext.Array.forEach(actionTask.children || [], function(child) {
				var d = this.loadActionTasksData(records, child, child.order)
				if (d)
					data.children = data.children.concat(d)

			}, this)

			Ext.each(data.children, function(child) {
				if (worstChild < this.statusMap[child.severity]) {
					worstChild = this.statusMap[child.severity]
					worstSeverity = child.severity
				}
			}, this);

			//If we have children then apply the actionTask properties because we want to show this group
			if (data.children.length > 0) {
				Ext.apply(data, {
					taskSummary: actionTask.description || recordParams.taskName,
					descriptionWikiLink: actionTask.descriptionWikiLink,
					resultWikiLink: actionTask.resultWikiLink,
					severity: worstSeverity
				})
				if (worstChild > 1)
					data.expanded = true
				actionTasksData.push(data)
			} else {
				data = null
			}
		}

		return actionTasksData
	},
	refresh: function(flushResults) {
		if (flushResults) {
			this.flushResults();
		}
		this.activeWorksheet = false;
		this.task.stop();
		this.task.refresh();
		this.task.restart(this.refreshInterval * 1000);
	},
	getCurrentData : function(){
		return this.filteredResults.getRootNode();
	}
})
function processActionTasks(actiontaskList, order) {
	var folders = [],
		folder,
		processedActionTasks = [];
	Ext.Array.forEach(actiontaskList || [], function(actionTask) {
		order++;
		if (actionTask) {
			//Determine group or plan action task
			if (/\{group/gi.test(actionTask)) {
				if (/\{group:(.*)\}/gi.exec(actionTask)) {
					//start of group
					var paramString = /\{group:(.*)\}/gi.exec(actionTask)[1],
						paramsSplit = paramString.split('|'),
						params = {};
					Ext.Array.forEach(paramsSplit, function(split) {
						var splitParams = split.split('=')
						if (splitParams.length > 1)
							params[splitParams[0]] = splitParams[1]
					})
					if (params.title) {
						var parentFolder = folder;
						folder = {
							description: params.title,
							children: [],
							autoCollapse: params.autoCollapse == 'false' ? false : true,
							expanded: true,
							order: order
						};
						if (parentFolder) parentFolder.children.push(folder)
						folders.push(folder)
						if (!parentFolder) processedActionTasks.push(folder)
					}
				} else {
					//end of group
					folders.pop()
					folder = folders[folders.length - 1]
				}
			} else {
				if (folder)
					folder.children.push(processNewActionTask(actionTask, order))
				else
					processedActionTasks.push(processNewActionTask(actionTask, order))
			}
		}
	})
	return processedActionTasks;
}
function processNewActionTask(actionTask, order) {
	if (actionTask && actionTask[0] == '{' && actionTask[actionTask.length - 1] == '}') {
		actionTask = actionTask.substring(1, actionTask.length - 1)
		var split = actionTask.split('|'),
			at = {
				leaf: true
			},
			paramSplit;
		Ext.Array.forEach(split, function(param) {
			paramSplit = param.split('=')
			if (paramSplit.length > 1) {
				at[paramSplit[0].trim()] = paramSplit[1].trim()
			}
		})
		at.actionTask = (at.task ? at.task : '') + (at.namespace ? '#' + at.namespace : '');
		at.order = order;
		return at;
	}

	return processActionTask(actionTask, order);
}