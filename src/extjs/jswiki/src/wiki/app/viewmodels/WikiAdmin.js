glu.defModel('RS.wiki.WikiAdmin', {
	displayName: '',
	columns: null,
	stateId: 'wikiwikiAdmin~~~',
	wait: false,
	store: {
		mtype: 'store',
		fields: ['id', 'ufullname', {
			name: 'uisActive',
			type: 'bool'
		}, {
			name: 'uisLocked',
			type: 'bool'
		}, {
			name: 'uisHidden',
			type: 'bool'
		}, {
			name: 'uhasActiveModel',
			type: 'bool'
		}, {
			name: 'uisDeleted',
			type: 'bool'
		}, {
			name: 'uisRoot',
			type: 'bool'
		}, 'usummary', 'unamespace', 'assignedToName'].concat(RS.common.grid.getSysFields()),	
		groupField: 'unamespace',
		sorters: [{
			property: 'ufullname',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wikiadmin/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	recordsSelections: [],
	init: function() {
		if (this.mock) this.backend = RS.wiki.createMockBackend(true)
		this.set('displayName', this.localize('displayName'));
		this.set('columns', [{
			header: '~~ufullname~~',
			dataIndex: 'ufullname',
			filterable: true,
			width: 300
		}, {
			header: '~~uisActive~~',
			dataIndex: 'uisActive',
			tooltip: this.localize('uisActiveTip'),
			filterable: true,
			renderer: RS.common.grid.booleanRenderer(),
			width: 60,
 			align:'center'
		}, {
			header: '~~uisLocked~~',
			dataIndex: 'uisLocked',
			tooltip: this.localize('uisLockedTip'),
			filterable: true,
			renderer: RS.common.grid.booleanRenderer(),
			width: 60,
		 	align:'center'
		}, {
			header: '~~uisHidden~~',
			dataIndex: 'uisHidden',
			tooltip: this.localize('uisHiddenTip'),
			filterable: true,
			renderer: RS.common.grid.booleanRenderer(),
			width: 60,
 			align:'center'
		}, {
			header: '~~uisDeleted~~',
			dataIndex: 'uisDeleted',
			tooltip: this.localize('uisDeletedTip'),
			filterable: true,
			renderer: RS.common.grid.booleanRenderer(),
			width: 60,
 			align:'center'
		}, {
			header: '~~uhasActiveModel~~',
			dataIndex: 'uhasActiveModel',
			tooltip: this.localize('uhasActiveModelTip'),
			filterable: true,
			renderer: RS.common.grid.booleanRenderer(),
			width: 60,
 			align:'center'
		}, {
			header: '~~uisRoot~~',
			dataIndex: 'uisRoot',
			tooltip: this.localize('uisRootTip'),
			filterable: true,
			renderer: RS.common.grid.booleanRenderer(),
			width: 60,
 			align:'center'
		}, {
			header: '~~usummary~~',
			dataIndex: 'usummary',
			filterable: true,
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~unamespace~~',
			dataIndex: 'unamespace',
			hideable: false,
			hidden: true,
			filterable: true
		}, {
			header: '~~assignedTo~~',
			dataIndex: 'assignedToName',
			filterable: true,
			hideable: false,
			hidden: true
		}].concat(RS.common.grid.getSysColumns()));

		Ext.Array.each(this.columns, function(column, idx) {
			if (column.dataIndex == 'sysUpdatedOn') {
				column.initialShow = true
				column.hidden = false
				return false
			}
		})

		this.store.on('load', function(store, rec, suc) {
			/*
			if (!suc) {
				clientVM.displayError(this.localize('ListRecordsErr'))
			}
			*/
			this.set('wait', false);
		}, this);
	},	
	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('wikiAdminWindowTitle'))
		this.store.load();
	},
	editRecord: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			params: {
				name: id,
				isEditMode: true
			}
		});
	},
	sendReq: function(url, errMsg, sucMsg, params, lockScreen) {
		lockScreen.set('wait', true);
		var ids = [];
		Ext.each(this.recordsSelections, function(r, idx) {
			ids.push(r.get('id'));
		});
		var reqParams = {};
		reqParams.ids = ids;
		reqParams.all = this.allSelected;
		if (params)
			Ext.apply(reqParams, params);
		var win = this.open({
			mtype: 'RS.wiki.Polling'
		});
		this.ajax({
			url: url,
			params: reqParams,
			scope: this,
			success: function(resp) {
				win.doClose();
				this.handleSucResp(resp, errMsg, sucMsg);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				lockScreen.set('wait', false);
			}
		});
	},
	handleSucResp: function(resp, errMsg, sucMsg) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize(errMsg) + '[' + respData.message + ']');
		} else
			clientVM.displaySuccess(this.localize(sucMsg));

		this.set('recordsSelections', []);

		this.store.load();
	},
	showMessage: function(title, OpsMsg, OpMsg, confirm, url, errMsg, sucMsg, params) {
		var strFullName = [];
		Ext.each(this.recordsSelections, function(r, idx) {
			strFullName.push(r.get('ufullname'));
		});
		var msg = this.localize(this.recordsSelections.length > 1 || this.allSelected ? OpsMsg : OpMsg, {
			num: this.allSelected ? this.store.getTotalCount() : this.recordsSelections.length,
			names: strFullName.join(',')
		});

		// msg += '<br/><input type="checkbox" id="index_attachment" /> Index Attachments' +
		// '<input type="checkbox" id="index_actiontask" /> Index ActionTasks<br/><br/>';
		this.message({
			title: this.localize(title),
			msg: msg,
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize(confirm),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				params = params || {};
				params.indexAttachment = true;
				params.indexActionTask = true;
				this.sendReq(url, errMsg, sucMsg, params, this);
			}
		});
	},
	doRename: function(params) {
		this.showMessage(
			'renameWikis',
			'RenamesMsg',
			'RenameMsg',
			'renameWikis',
			'/resolve/service/wikiadmin/moveRenameCopy',
			'RenameErrMsg',
			'RenameSucMsg',
			params
		);
	},
	renameWikis: function() {
		var names = this.recordsSelections[0].get('ufullname').split('.');
		this.open({
			mtype: 'RS.wiki.pagebuilder.MoveOrRename',
			action: 'rename',
			namespace: names[0],
			filename: names[1],
			moveRenameCopyTitle: 'RenameTitle',
			modelName: this.viewmodelName,
			selectionsLength: this.recordsSelections.length,
			dumper: (function(self) {
				return function(params) {
					self.doRename(params);
				}
			})(this)
		});
	},
	renameWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},
	doCopy: function(params) {
		this.showMessage(
			'copyWikis',
			'CopysMsg',
			'CopyMsg',
			'copyWikis',
			'/resolve/service/wikiadmin/moveRenameCopy',
			'CopyErrMsg',
			'CopySucMsg',
			params
		);
	},
	copyWikis: function() {
		this.open({
			mtype: 'RS.wiki.pagebuilder.MoveOrRename',
			action: 'copy',
			namespace: this.recordsSelections[0].get('unamespace'),
			filename: this.recordsSelections[0].get('uname'),
			moveRenameCopyTitle: 'CopyTitle',
			modelName: this.viewmodelName,
			selectionsLength: this.recordsSelections.length,
			dumper: (function(self) {
				return function(params) {
					self.doCopy(params)
				}
			})(this)
		});
	},
	copyWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},
	deleteWikis: function() {
		this.showMessage(
			'deleteWikis',
			'DeletesMsg',
			'DeleteMsg',
			'deleteWikis',
			'/resolve/service/wikiadmin/setDeleted',
			'DeleteErrMsg',
			'DeleteSucMsg', {
				on: true
			}
		);
	},
	deleteWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},
	undeleteWikis: function() {
		this.showMessage(
			'undeleteWikis',
			'UndeletesMsg',
			'UndeleteMsg',
			'undeleteWikis',
			'/resolve/service/wikiadmin/setDeleted',
			'UndeleteErrMsg',
			'UndeleteSucMsg', {
				on: false
			}
		);
	},
	undeleteWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},
	purgeWikis: function() {
		this.showMessage(
			'purgeWikis',
			'PurgesMsg',
			'PurgeMsg',
			'purgeWikis',
			'/resolve/service/wikiadmin/purge',
			'PurgeErrMsg',
			'PurgeSucMsg'
		);
	},
	purgeWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},
	activateWikis: function() {
		this.showMessage(
			'activateWikis',
			'ActivatesMsg',
			'ActivateMsg',
			'activateWikis',
			'/resolve/service/wikiadmin/setActivated',
			'ActivateErrMsg',
			'ActivateSucMsg', {
				on: true
			}
		);
	},
	activateWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},
	deactivateWikis: function() {
		this.showMessage(
			'deactivateWikis',
			'DeactivatesMsg',
			'DeactivateMsg',
			'deactivateWikis',
			'/resolve/service/wikiadmin/setActivated',
			'DeactivateErrMsg',
			'DeactivateSucMsg', {
				on: false
			}
		);
	},
	deactivateWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},
	lockWikis: function() {
		this.showMessage(
			'lockWikis',
			'LocksMsg',
			'LockMsg',
			'lockWikis',
			'/resolve/service/wikiadmin/setLocked',
			'LockErrMsg',
			'LockSucMsg', {
				on: true
			}
		);
	},
	lockWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},
	unlockWikis: function() {
		this.showMessage(
			'unlockWikis',
			'UnlocksMsg',
			'UnlockMsg',
			'unlockWikis',
			'/resolve/service/wikiadmin/setLocked',
			'UnlockErrMsg',
			'UnlockSucMsg', {
				on: false
			}
		);
	},
	unlockWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},
	hideWikis: function() {
		this.showMessage(
			'hideWikis',
			'HidesMsg',
			'HideMsg',
			'hideWikis',
			'/resolve/service/wikiadmin/setHidden',
			'HideErrMsg',
			'HideSucMsg', {
				on: true
			}
		);
	},
	hideWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},
	unhideWikis: function() {
		this.showMessage(
			'unhideWikis',
			'UnhidesMsg',
			'UnhideMsg',
			'unhideWikis',
			'/resolve/service/wikiadmin/setHidden',
			'UnhideErrMsg',
			'UnhideSucMsg', {
				on: false
			}
		);
	},
	unhideWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},
	doRate: function(params) {
		this.showMessage(
			'rateWikis',
			'RatesMsg',
			'RateMsg',
			'rateWikis',
			'/resolve/service/wikiadmin/rate',
			'RateErrMsg',
			'RateSucMsg',
			params
		);
	},
	rateWikis: function() {
		if (this.recordsSelections.length > 1) {
			this.open({
				mtype: 'RS.wiki.Rater',
				dumper: (function(self) {
					return function(params) {
						self.doRate(params);
					}
				})(this)
			});
			return;
		}
		var id = this.recordsSelections[0].get('id');
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/wikiadmin/getRating',
			params: {
				id: id
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('GetRatingErr') + '[' + respData.message + ']');
					return;
				}
				this.open({
					mtype: 'RS.wiki.Rater',
					u1StarCount: respData.data ? respData.data.u1StarCount : 0,
					u2StarCount: respData.data ? respData.data.u2StarCount : 0,
					u3StarCount: respData.data ? respData.data.u3StarCount : 0,
					u4StarCount: respData.data ? respData.data.u4StarCount : 0,
					u5StarCount: respData.data ? respData.data.u5StarCount : 0,
					dumper: (function(self) {
						return function(params) {
							self.doRate(params);
						}
					})(this)
				});

			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},
	rateWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	// doAddSearchWeight: function(params) {
	// 	this.showMessage(
	// 		'AddSearchWeightTitle',
	// 		'AddSearchWeightsMsg',
	// 		'AddSearchWeightMsg',
	// 		'confirm',
	// 		'/resolve/service/wikiadmin/addSearchWeight',
	// 		'AddSearchWeightErrMsg',
	// 		'AddSearchWeightSucMsg',
	// 		params
	// 	);
	// },

	// addSearchWeight: function() {
	// 	this.open({
	// 		mtype: 'RS.wiki.SearchWeightEditor',
	// 		dumper: (function(self) {
	// 			return function(params) {
	// 				self.doAddSearchWeight(params)
	// 			}
	// 		})(this)
	// 	})
	// },

	// addSearchWeightIsEnabled$: function() {
	// 	return !this.wait && this.recordsSelections.length > 0;
	// },

	setReviewed: function() {
		this.showMessage(
			'setReviewed',
			'ReviewsMsg',
			'ReviewMsg',
			'setReviewed',
			'/resolve/service/wikiadmin/setReviewed',
			'ReviewErrMsg',
			'ReviewSucMsg'
		);
	},

	setReviewedIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	doSetExpiryDate: function(params) {
		this.showMessage(
			'setExpiryDate',
			'ExpiryDatesMsg',
			'ExpiryDateMsg',
			'setExpiryDate',
			'/resolve/service/wikiadmin/setExpiryDate',
			'ExpiryDateErrMsg',
			'ExpiryDateSucMsg',
			params
		);
	},

	setExpiryDate: function() {
		this.open({
			mtype: 'RS.wiki.DatePicker',
			dumper: (function(self) {
				return function(params) {
					self.doSetExpiryDate(params);
				}
			})(this)
		});
	},

	setExpiryDateIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	doSetHomepage: function(params) {
		this.showMessage(
			'SetHomepageTitle',
			'SetHomepagesMsg',
			'SetHomepageMsg',
			'confirm',
			'/resolve/service/wikiadmin/homepage',
			'SetHomepageErrMsg',
			'SetHomepageSucMsg',
			params
		);
	},

	setHomepage: function() {
		this.open({
			mtype: 'RS.common.GridPicker',
			displayName: this.localize('roleDisplayName'),
			asWindowTitle: this.localize('roleDisplayName'),
			dumperText: this.localize('select'),
			columns: [{
				header: this.localize('uname'),
				dataIndex: 'uname',
				filterable: true,
				flex: 1
			}],
			storeConfig: {
				fields: ['id', 'uname', 'uhomepage'],
				baseParams : {
					includePublic : "true"
				},
				proxy: {
					type: 'ajax',
					url: '/resolve/service/common/roles/list',
					reader: {
						type: 'json',
						root: 'records'
					},
					listeners: {
						exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
					}
				}
			},
			dumper: (function(self) {
				return function(records) {
					var ids = [];
					Ext.each(records, function(r, idx) {
						ids.push(r.get('id'));
					});
					var id = self.recordsSelections[0].get('id');
					self.doSetHomepage({
						roleIds: ids,
						id: id
					});

					return true;
				}
			})(this)
		});
	},

	setHomepageIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length == 1;
	},

	doConfigRoles: function(cvs) {
		this.showMessage(
			'configRoles',
			'SetRolesMsg',
			'SetRoleMsg',
			'configRoles',
			'/resolve/service/wikiadmin/setAccessRights',
			'SetRoleErrMsg',
			'SetRoleSucMsg',
			cvs
		);
	},

	configRoles: function() {
		var ids = [];

		Ext.each(this.recordsSelections, function(r) {
			ids.push(r.get('id'));
		});

		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/wikiadmin/accessRights',
			params: {
				ids: ids
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('GetRolesErr', respData.message));
					return;
				}

				if (!respData.data)
					return;

				this.open({
					mtype: 'RS.wiki.AccessRights',
					rights: respData.data,
					dumper: (function(self) {
						return function(cvs) {
							cvs.defaultRights = this.accessRights.defaultRoles;
							self.doConfigRoles(cvs);
						}
					})(this)
				});
			},

			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	configRolesIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	doAddTags: function(params) {
		this.showMessage(
			'AddTagTitle',
			'AddTagsMsg',
			'AddTagMsg',
			'confirm',
			'/resolve/service/wikiadmin/addTags',
			'AddTagErrMsg',
			'AddTagSucMsg',
			params
		);
	},

	doRemoveTags: function(params) {
		this.showMessage(
			'RemoveTagTitle',
			'RemoveTagsMsg',
			'RemoveTagMsg',
			'confirm',
			'/resolve/service/wikiadmin/removeTags',
			'RemoveTagErrMsg',
			'RemoveTagSucMsg',
			params
		);
	},

	configTags: function() {
		this.open({
			mtype: 'RS.wiki.TagConfiguration'
		});
	},

	configTagsIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	indexWikis: function() {
		this.showMessage(
			'indexWikis',
			'IndexingsMsg',
			'IndexingMsg',
			'indexWikis',
			'/resolve/service/wikiadmin/index',
			'IndexingErrMsg',
			'IndexingSucMsg'
		);
	},

	indexWikisIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	indexAllWikis: function() {
		this.showMessage(
			'indexAllWikis',
			'IndexAllWikisMsg',
			'IndexAllWikisMsg',
			'indexWikis',
			'/resolve/service/wikiadmin/index',
			'IndexingErrMsg',
			'IndexingSucMsg', {
				all: true,
				indexAttachment: true,
				indexActionTask: true,
				ids: null
			}
		);
	},

	indexAllWikisIsEnabled$: function() {
		return !this.wait;
	},

	purgeIndex: function() {
		this.showMessage(
			'purgeIndex',
			'PurgeIndexsMsg',
			'PurgeIndexMsg',
			'purgeIndex',
			'/resolve/service/wikiadmin/purgeIndex',
			'PurgeIndexErrMsg',
			'PurgeIndexSucMsg'
		);
	},

	purgeIndexIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	purgeAllIndexes: function() {
		this.showMessage(
			'purgeIndex',
			'PurgeAllIndexMsg',
			'PurgeAllIndexMsg',
			'purgeIndex',
			'/resolve/service/wikiadmin/purgeIndex',
			'PurgeIndexErrMsg',
			'PurgeIndexSucMsg', {
				all: true,
				ids: null
			}
		);
	},

	purgeAllIndexesIsEnabled$: function() {
		return !this.wait;
	},

	doCommit: function(params) {
		this.showMessage(
			'commit',
			'CommitsMsg',
			'CommitMsg',
			'commit',
			'/resolve/service/wikiadmin/commit',
			'CommitErrMsg',
			'CommitSucMsg',
			params
		);
	},

	commit: function() {
		this.open({
			mtype: 'RS.wiki.Comment',
			dumper: (function(self) {
				return function(comment) {
					self.doCommit({
						comment: comment
					});
				}
			})(this)
		})
	},

	commitIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	resetStats: function() {
		this.showMessage(
			'resetStats',
			'ResetStatssMsg',
			'ResetStatsMsg',
			'resetStats',
			'/resolve/service/wikiadmin/resetStats',
			'ResetStatsErrMsg',
			'ResetStatsSucMsg'
		);
	},

	resetStatsIsEnabled$: function() {
		return !this.wait && this.recordsSelections.length > 0;
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.NamespaceAdmin'
		});
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	}
});

glu.defModel('RS.wiki.Polling', {
	busyText: '',
	init: function() {
		this.set('busyText', this.localize('busyText'));
	}
});