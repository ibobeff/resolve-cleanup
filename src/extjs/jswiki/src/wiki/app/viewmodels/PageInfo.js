glu.defModel('RS.wiki.PageInfo', {

	title$: function() {
		return this.localize('pageInfo') + ' - ' + this.name
	},

	name: '',
	fullName: '',
	emptySummary: '',
	summary: '',
	sysCreatedBy: '',
	sysCreatedOn: '',
	createdOn$: function() {
		return this.getParsedDate(this.sysCreatedOn);
	},
	sysUpdatedBy: '',
	sysUpdatedOn: '',
	updatedOn$: function() {
		return this.getParsedDate(this.sysUpdatedOn);
	},
	expireOn: '',
	expirationDate$: function() {
		if (this.expireOn)
			return this.getParsedDate(this.expireOn, 'Y-m-d');
		return '';
	},
	version: '',
	viewed: '',
	edited: '',
	executed: '',
	rating: '',
	tags: '',
	lastReviewedBy: '',
	lastReviewedOn: '',
	reviewedOn$: function() {
		if (this.lastReviewedOn)
			return this.getParsedDate(this.lastReviewedOn);
	},
	getParsedDate: function(date, f) {
		var d = date,
			formats = ['time', 'c', 'Y-m-d G:i:s'];

		if (!Ext.isDate(d)) {
			Ext.Array.each(formats, function(format) {
				d = Ext.Date.parse(d, format)
				return !Ext.isDate(d)
			})
		}

		if (Ext.isDate(d)) {
			if (f)
				return Ext.Date.format(d, f);
			return Ext.Date.format(d, 'Y-m-d G:i:s');
		}

		return RS.common.locale.unknownDate
	},
	embeddedUrl$: function() {
		return '';
	},
	outgoings: {
		mtype: 'list'
	},

	incomings: {
		mtype: 'list'
	},

	actionTaskStore: {
		mtype: 'store',
		fields: ['type', 'name'],
		proxy: {
			type: 'memory'
		}
	},

	runbookStore: {
		mtype: 'store',
		fields: ['type', 'name'],
		proxy: {
			type: 'memory'
		}
	},

	moreOrLess: 'More..',

	moreOrLessIsVisible$: function() {
		this.tags && this.tags.length > 0;
	},

	fields: ['summary', 'version', 'viewed', 'edited', 'executed', 'rating', 'embeddedUrl', 'fullName', 'expireOn', {
			name: 'tags',
			type: 'array'
		}, 'lastReviewedBy',
		'lastReviewedOn'
	].concat(RS.common.grid.getSysFields()),

	init: function() {
		this.ajax({
			url: '/resolve/service/wiki/getPageInfo',
			params: {
				name: this.name
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.loadData(response.data)
					Ext.each(response.data.incomingRefLinks, function(wikiName) {
						this.incomings.add(glu.model({
							mtype: 'RS.wiki.PageInfoRefLink',
							wikiName: wikiName
						}));
					}, this);

					Ext.each(response.data.outgoingRefLinks, function(wikiName) {
						this.outgoings.add(glu.model({
							mtype: 'RS.wiki.PageInfoRefLink',
							wikiName: wikiName
						}));
					}, this);

					Ext.each(['atRefInAbortModel', 'atRefInContent', 'atRefInMainModel'], function(atField) {
						Ext.each(response.data[atField], function(at) {
							this.actionTaskStore.add({
								type: this.localize(atField.substring(7)),
								name: at
							});
						}, this);
					}, this);

					Ext.each(['wikiAbortModel', 'wikiDecisionTreeModel', 'wikiMainModel'], function(runbookField) {
						Ext.each(response.data[runbookField], function(runbook) {
							this.runbookStore.add({
								type: this.localize(runbookField.substring(4)),
								name: runbook
							});
						}, this)
					}, this);
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	tagsDisplayed: '',

	tagsChange$: function() {
		if (!this.tags)
			return;
		this.set('tagsDisplayed', (this.tags.length > 5 ? this.tags.slice(0, 4) : this.tags).join(', '));
		this.set('moreOrLess', 'More..');
	},

	displayMoreOrLess: function() {
		this.set('moreOrLess', this.moreOrLess == 'More..' ? 'Less..' : 'More..');
		if (this.moreOrLess == 'More..')
			this.set('tagsDisplayed', (this.tags.length > 5 ? this.tags.slice(0, 4) : this.tags).join(', '));
		else
			this.set('tagsDisplayed', this.tags.join(', '));
	}

	// resetStats: function() {
	// 	this.ajax({
	// 		url: '/resolve/service/wiki/resetStats',
	// 		params: {
	// 			name: this.name
	// 		},
	// 		success: function(resp) {
	// 			var respData = RS.common.parsePayload(resp);
	// 			if (!respData.success)
	// 				//clientVM.displayError(this.localize('resetStatsError', respData.message));
	// 				clientVM.displayError(respData.message);
	// 		},
	// 		failure: function(resp) {
	// 			clientVM.displayFailure(resp);
	// 		}
	// 	})
	// }
});

glu.defModel('RS.wiki.PageInfoRefLink', {
	wikiName: '',
	link$: function() {
		return {
			tag: 'a',
			href: Ext.String.format('/resolve/jsp/rsclient.jsp?#RS.wiki.Main/name={0}', this.wikiName),
			target: '_blank',
			cls: "rs-link",
			html: this.wikiName
		};
	}
});