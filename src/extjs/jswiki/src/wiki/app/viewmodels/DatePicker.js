glu.defModel('RS.wiki.DatePicker',{
	
	date:new Date(),

	format:null,

	wait:false,

	dumper:null,

	dateIsValid$:function(){
		return this.date!=''?true:this.localize('invalidDate');
	},

	activate:function(){

	},

	init:function(){
		this.set('format','m/d/Y');
	},

	cancel:function(){
		this.doClose();
	},

	setExpiryDate:function(){
		var formatedDate = Ext.util.Format.date(this.date,'Y-m-d');
		this.dumper({date:formatedDate});
		this.cancel();
	},

	setExpiryDateIsEnabled$:function(){
		return !this.wait&&this.isValid;
	}
});