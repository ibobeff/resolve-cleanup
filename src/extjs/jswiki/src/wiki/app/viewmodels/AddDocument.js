glu.defModel('RS.wiki.AddDocument', {
	fields : ['name'],
	name: '',
	nameIsValid$: function() {
		if (!this.name) return this.localize('emptyNameInvalid')
		return clientVM.searchTextIsValid(this.name)
	},
	template: '',
	templates: {
		mtype: 'store',
		fields: ['uname', 'uwikiDocName', 'uformName'],
		data: [{
			name: 'test',
			value: 'test'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wikitemplate/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	init: function() {
		this.templates.load()
	},
	addIsEnabled$: function() {
		return this.isValid;
	},
	add: function() {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		if (this.template) {
			var nameSplit = this.name.split('.'),
				templateSplit = this.template.split('.'),
				templateRecord = this.templates.getAt(this.templates.findExact('uwikiDocName', this.template));
			clientVM.handleNavigation({
				modelName: 'RS.formbuilder.Viewer',
				params: {
					name: templateRecord.get('uformName'),
					params: Ext.Object.toQueryString({
						namespace: nameSplit[0],
						docname: nameSplit[1],
						template_namespace: templateSplit[0],
						template_docname: templateSplit[1]
					})
				}
			})
		} else
			clientVM.handleNavigation({
				modelName: 'RS.wiki.Main',
				params: {
					name: this.name,
					activeTab: this.parentVM.activeTab
				}
			})

		this.doClose()
	},	
	cancel: function() {
		this.doClose()
	}
})