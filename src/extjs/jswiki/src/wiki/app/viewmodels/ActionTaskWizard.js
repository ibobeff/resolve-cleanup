glu.defModel('RS.wiki.ActionTaskWizard', {
	searchText: '',
	when_searchText_changes_update_actionTasks_grid: {
		on: ['searchTextChanged'],
		action: function() {
			this.actionTasks.load()
		}
	},

	actionTasks: {
		mtype: 'store',
		fields: ['id', 'uname', 'unamespace'],
		sorters: [{
			property: 'uname',
			direction: 'ASC'
		}],
		remoteSort: true,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/actiontask/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	actionTasksSelections: [],
	actionTasksColumns: [{
		header: '~~uname~~',
		dataIndex: 'uname',
		filterable: true,
		flex: 1
	}, {
		header: '~~unamespace~~',
		dataIndex: 'unamespace',
		filterable: true,
		flex: 1
	}],

	init: function() {
		this.actionTasks.on('beforeload', function(store, operation) {
			operation.params = operation.params || {}
			operation.params.id = 'root_menu_id'
			operation.params.delim = '/'

			if (this.searchText) {
				operation.params.filter = Ext.encode([{
					field: 'uname',
					type: 'auto',
					condition: 'contains',
					value: this.searchText
				}])
			}
		}, this)

		this.actionTasks.load()
	},

	create: function() {
		this.parentVM.set('name', 'ATF_' + this.actionTasksSelections[0].get('unamespace').replace(/\./gi, '_').replace(/ /gi, '_').toUpperCase() + '_' + this.actionTasksSelections[0].get('uname').replace(/\./gi, '_').replace(/ /gi, '_').toUpperCase())
		clientVM.handleNavigation({
			modelName: 'RS.formbuilder.Main',
			params: {
				name: this.parentVM.name
			},
			target: '_blank'
		})
		this.doClose()
	},
	createIsEnabled$: function() {
		return this.actionTasksSelections.length == 1
	},
	cancel: function() {
		this.doClose()
	}
})