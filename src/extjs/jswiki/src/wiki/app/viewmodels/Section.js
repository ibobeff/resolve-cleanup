/*
glu.defModel('RS.wiki.Section', {
	types: ['source'],
	typeConfigurations: {
		mtype: 'list',
		autoParent: true
	},
	activeType: null,
	title: 'Untitled Section',
	hidden: false,
	active: true,
	maxHeight: -1,
	maxWidth: -1,
	collapsible: false,
	collapsed: false,
	content: '',
	originalContent: '',

	collapsedIsEnabled$: function() {
		if (!this.collapsible) {
			this.set('collapsed', false);
		}

		return this.collapsible;
	},

	showSection$: function() {
		return this.title.indexOf('dt_') === -1 && this.content.indexOf('##GENERATED CODE: DO NOT MODIFY') === -1;
	},

	init: function() {
		if(typeof 'test'.trim !== 'function') {
			this.trim = function (str) {
				return str.replace(/^\s+|\s+$/g,'');
			};
		}

		this.set('title', this.localize('untitledSection'))
		this.typeConfigurations.add(this.model({ mtype: 'RS.wiki.builder.SectionConfiguration' }));
		this.typeConfigurations.add(this.model({ mtype: 'RS.wiki.builder.AddTab' }));
		this.set('originalContent', this.content);
		this.stripSectionTags();
		this.parseSectionConfiguration();

		if (this.models) {
			Ext.Array.forEach(this.models, function(model) {
				model.unParent();
				this.typeConfigurations.insert(this.typeConfigurations.length - 1, model);
			}, this);
		} else {
			this.set('content', Ext.String.trim(this.content));
			this.parseContent();
		}
	},

	configureUpDown: function() {
		this.set('upIsEnabled', this.parentVM.sections.indexOf(this) > 0);
		this.set('downIsEnabled', this.parentVM.sections.indexOf(this) < this.parentVM.sections.length - 1);
	},

	removeClicked: function() {
		this.message({
			title: this.localize('confirmRemoveSectionTitle'),
			msg: this.localize('confirmRemoveSectionBody'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('remove'),
				no: this.localize('cancel')
			},
			fn: this.removeSection,
			scope: this
		});
	},

	removeSection: function(btn) {
		if (btn === 'yes') {
			this.parentList.remove(this)
		}
	},

	dropTab: function(startIndex, index) {
		var idx = Math.min(index, this.typeConfigurations.length - 1)
		var objs = this.typeConfigurations._private.objs;
		objs.splice(idx, 0, objs.splice(startIndex, 1)[0]);
		this.types.splice(idx - 1, 0, this.types.splice(startIndex - 1, 1)[0]);
	},

	addAbove: function() {
		this.open({
			mtype: 'RS.wiki.SectionPicker',
			index: Math.max(0, this.parentVM.sections.indexOf(this))
		});
	},

	addBelow: function() {
		this.open({
			mtype: 'RS.wiki.SectionPicker',
			index: Math.min(this.parentVM.sections.length, this.parentVM.sections.indexOf(this) + 1)
		});
	},

	reallyAddSection: function(sec, index) {
		this.parentVM.reallyAddSection(sec, index);
	},

	upIsEnabled: false,

	up: function() {
		this.parentVM.sections.transferFrom(this.parentVM.sections, this, this.parentVM.sections.indexOf(this) - 1)
		this.parentVM.sections.foreach(function(sec) {
			sec.configureUpDown();
		});
	},

	downIsEnabled: false,

	down: function() {
		this.parentVM.sections.transferFrom(this.parentVM.sections, this, this.parentVM.sections.indexOf(this) + 1)
		this.parentVM.sections.foreach(function(sec) {
			sec.configureUpDown();
		});
	},

	stripSectionTags: function() {
		var c = /\{section[^\}]*\}([\s\S]*?)\{section\}/gi.exec(this.content);

		if (Ext.isArray(c)) {
			c.splice(0, 1);
			this.set('content', c.join(''));
		}
	},

	regSection: /\{section(.*?)\}/gi,

	parseSectionConfiguration: function() {
		var sectionConfigSplit = this.originalContent.match(this.regSection);

		if (sectionConfigSplit && sectionConfigSplit.length > 0) {
			var sectionParamSplit = this.regSection.exec(sectionConfigSplit[0]);

			if (sectionParamSplit.length > 0) {
				sectionParamSplit.splice(0, 1);
			}

			for (var i = 0; i < sectionParamSplit.length; i++) {
				var split = sectionParamSplit[i];

				if (split) {
					if (split[0] == ':') {
						split = split.substring(1);
					}

					var params = split.split('|');

					for(var j = 0; j < params.length; j++) {
						if (params[j].length > 0) {
							var nameVal = params[j].split('=');

							if (nameVal.length > 1 && typeof nameVal[0].toLowerCase === 'function') {
								this.setProperty(nameVal[0].toLowerCase(), nameVal[1]);
							}
						}						
					}
				}				
			}
		}
	},

	setProperty: function (propertyName, propertyValue) {
		switch (propertyName) {
			case 'type':
				this.set('types', propertyValue.toLowerCase().split(','))
				break;
			case 'title':
				this.set('title', propertyValue)
				break;
			case 'hidden':
				this.set('hidden', propertyValue.toLowerCase() === 'true');
				break;
			case 'active':
				this.set('active', propertyValue.toLowerCase() === 'true');
				break;
			case 'height':
				var n = +propertyValue;

				if(!isNaN(n)) {
					this.set('maxHeight', n);
				}

				break;
		}
	},

	regWidth: /width:(.*?)%/gi,
	regVerticalAlign: /vertical-align:(.*?)[;]*"/gi,

	trim: function (str) {
		return str.trim();
	},

	parseContent: function() {
		//Based on the type of content we have, there might be various types of old macros or whatever, so we need to configure them properly now
		var contentSplit = this.content.split('\n'),
			columns = [],
			lines = [],
			col1WidthSet = false,
			col2WidthSet = false,
			col3WidthSet = false,
			col1AlignmentSet = false,
			col2AlignmentSet = false,
			col3AlignmentSet = false;

		for (var i = 0; i < contentSplit.length; i++) {
			var line = contentSplit[i];

			if (line.indexOf('class="wiki-builder"') === -1) {
				lines.push(line);
			} else if (line.indexOf('<td') > -1) {
				var widthMatch = this.regWidth.exec(line);

				if (widthMatch && widthMatch.length > 1) {
					var percent = this.trim(widthMatch[1]);

					if (!col1WidthSet) {
						this.typeConfigurations.getAt(0).set('column1Width', +percent);
						col1WidthSet = true;
					} else if (!col2WidthSet) {
						this.typeConfigurations.getAt(0).set('column2Width', +percent);
						col2WidthSet = true;
					} else if (!col3WidthSet) {
						this.typeConfigurations.getAt(0).set('column3Width', +percent);
						col3WidthSet = true;
					}
				}

				var alignMatch = this.regVerticalAlign.exec(line);

				if (alignMatch && alignMatch.length > 1) {
					var alignment = this.trim(alignMatch[1]);

					if (!col1AlignmentSet) {
						this.typeConfigurations.getAt(0).set('column1Alignment', alignment);
						col1AlignmentSet = true;
					} else if (!col2AlignmentSet) {
						this.typeConfigurations.getAt(0).set('column2Alignment', alignment);
						col2AlignmentSet = true;
					} else if (!col3AlignmentSet) {
						this.typeConfigurations.getAt(0).set('column3Alignment', alignment);
						col3AlignmentSet = true;
					}
				}
			} else if (line.indexOf('</td class="wiki-builder">') > -1) {
				columns.push(lines.join('\n'))
				lines = []
			}			
		}

		//any leftover lines (like when we don't have any table information)
		if (lines.length > 0) {
			columns.push(lines.join('\n'));
		}

		for(var i = 0; i < this.types.length; i++) {
			var type = this.types[i];

			var configurationLines = (columns[i] || '').split('\n'),				
				properties = {};

			var props = this.extractProperties(configurationLines);

			for (var j = 0; j < props.length; j++) {
				var split = props[j].split('=');

				if (split.length > 1) {
					properties[split[0]] = split.slice(1).join('=');
				} else  {
					properties['text'] = split[0];
				}				
			}

			this.parseTypeConfiguration(type, properties, columns[i], configurationLines)			
		}

		if (!col1WidthSet) {
			this.configureDefaultColumnWidths();
		}
	},

	extractProperties: function (configurationLines) {
		var props = [];

		if (configurationLines.length > 0) {
			var c = configurationLines.splice(0, 1)[0];
			var colonIndex = c.indexOf(':');

			if(colonIndex > -1) {
				var parenIndex = c.indexOf('}');
				var endIndex = c.length - 1;

				if(parenIndex > -1) {
					endIndex = parenIndex;
				}

				props = c.substring(colonIndex + 1, endIndex).split('|');
			}			
		}

		return props;
	},

	parseTypeConfiguration: function(type, properties, content, lines) {
		var tlen = this.typeConfigurations.length - 1;

		switch (type.toLowerCase()) {
			case 'url':
			case 'source':
				this.typeConfigurations.insert(tlen, this.model({
					mtype: 'RS.wiki.builder.Source',
					content: content
				}))
				break;

			case 'wysiwyg':
				this.typeConfigurations.insert(tlen, this.model({
					mtype: 'RS.wiki.builder.WYSIWYG',
					content: content
				}))
				break;

			case 'groovy':
				this.typeConfigurations.insert(tlen, this.model({
					mtype: 'RS.wiki.builder.Groovy',
					content: content
				}))
				break;

			case 'html':
				this.typeConfigurations.insert(tlen, this.model({
					mtype: 'RS.wiki.builder.HTML',
					content: content
				}))
				break;

			case 'javascript':
				this.typeConfigurations.insert(tlen, this.model({
					mtype: 'RS.wiki.builder.Javascript',
					content: content
				}))
				break;

			case 'form':
				var w = +properties.popoutWidth;
				var h = +properties.popoutHeight;

				if (isNaN(w)) {
					w = -1;
				}

				if (isNaN(h)) {
					h = -1;
				}

				this.typeConfigurations.insert(tlen, this.model({
					mtype: 'RS.wiki.builder.Form',
					title: properties.title || '',
					name: properties.name || '',
					tooltip: properties.tooltip || '',
					popout: properties.popout === 'true',
					collapsed: properties.collapsed === 'true',
					popoutWidth: w,
					popoutHeight: h
				}))
				break;

			case 'encrypt':
				this.typeConfigurations.insert(tlen, this.model({
					mtype: 'RS.wiki.builder.Encrypt',
					key: properties.key,
					value: properties.value
				}))
				break;

			case 'infobar':
				var h = +properties.height;

				if (isNaN(h)) {
					h = 400
				}

				var infobarParams = {};
				infobarParams.height = h;
				if (properties.social) infobarParams.social = properties.social === 'true';
				if (properties.feedback) infobarParams.feedback = properties.feedback === 'true';
				if (properties.rating) infobarParams.rating = properties.rating === 'true';
				if (properties.attachments) infobarParams.attachments = properties.attachments === 'true';
				if (properties.history) infobarParams.history = properties.history === 'true';
				if (properties.tags) infobarParams.tags = properties.tags === 'true';
				if (properties.pageInfo) infobarParams.pageInfo = properties.pageInfo === 'true';

				this.typeConfigurations.insert(tlen, this.model(Ext.apply({
					mtype: 'RS.wiki.builder.Infobar'
				}, infobarParams)));
				break;

			case 'image':
				var w = +properties.width;
				var h = +properties.height;

				if (isNaN(w)) {
					w = -1;
				}

				if (isNaN(h)) {
					h = -1;
				}

				this.typeConfigurations.insert(tlen, this.model({
					mtype: 'RS.wiki.builder.Image',
					height: h,
					width: w,
					zoom: properties.zoom === 'true',
					src: properties.text || ''
				}))
				break;

			case 'model':
				var w = +properties.width;
				var h = +properties.height;
				var interval = +properties.refreshInterval;

				if (isNaN(w)) {
					w = -1;
				}

				if (isNaN(h)) {
					h = -1;
				}

				if (isNaN(interval)) {
					interval = -1;
				}

				this.typeConfigurations.insert(tlen, this.model({
					mtype: 'RS.wiki.builder.Model',
					wiki: properties.wiki || '',
					status: properties.status || '',
					zoom: properties.zoom || '',
					width: w,
					height: h,
					refreshInterval: interval
				}))
				break;

			case 'code':
				this.typeConfigurations.insert(tlen, this.model({
					mtype: 'RS.wiki.builder.Code',
					type: properties.type || 'text',
					showLineNumbers: properties.showLineNumbers === 'false' ? false : true,
					content: content
				}))
				break;

			case 'progress':
				var w = +properties.width;
				var h = +properties.height;
				var b = +properties.border;

				if (isNaN(w)) {
					w = -1;
				}

				if (isNaN(h)) {
					h = -1;
				}

				if (isNaN(b)) {
					b = -1;
				}				

				this.typeConfigurations.insert(tlen, this.model({
					mtype: 'RS.wiki.builder.Progress',
					content: content,
					text: properties.text || '',
					completedText: properties.completedText || '',
					width: w,
					height: h,
					border: b,
					borderColor: properties.borderColor || '',
					displayText: properties.displayText || ''
				}))
				break;

			case 'automation':
				var w = +properties.popoutWidth;
				var h = +properties.popoutHeight;

				if (isNaN(w)) {
					w = -1;
				}

				if (isNaN(h)) {
					h = -1;
				}

				var automationVM = this.model({
					mtype: 'RS.wiki.builder.Automation',
					title: properties.title || '',
					name: properties.name || '',
					wikiName: properties.wikiName || '',
					tooltip: properties.tooltip || '',
					popout: properties.popout === 'true',
					collapsed: properties.collapsed === 'true',
					popoutWidth: w,
					popoutHeight: h
				});
				this.typeConfigurations.insert(tlen, automationVM);
				break;

			case 'result':
				var interval = +properties.refreshInterval;
				var w = +properties.descriptionWidth;
				var refreshCountMax = +properties.refreshCountMax;
				var order = 'DESC';

				if (isNaN(interval)) {
					interval = 5;
				}
				
				if (isNaN(w)) {
					w = 0;
				}

				if (isNaN(refreshCountMax)) {
					refreshCountMax = 60;
				}

				if(properties.order && properties.order.toLowerCase() === 'asc') {
					order = 'ASC';
				}

				var resultParams = {};
				resultParams.refreshInterval = interval;
				resultParams.refreshCountMax = refreshCountMax;
				resultParams.descriptionWidth = w;
				resultParams.filter = properties.filter || '';
				
				if (properties.text || properties.title) resultParams.title = properties.text || properties.title;
				if (properties.encodeSummary) resultParams.encodeSummary = properties.encodeSummary === 'true';
				if (properties.order) resultParams.order = properties.order.toLowerCase() == 'asc' ? 'ASC' : 'DESC';
				if (properties.autoCollapse) resultParams.autoCollapse = properties.autoCollapse === 'true';
				if (properties.collapsed) resultParams.collapsed = properties.collapsed === 'true';
				if (properties.autoHide) resultParams.autoHide = properties.autoHide === 'true';
				if (properties.progress) resultParams.progress = properties.progress === 'true';

				this.typeConfigurations.insert(tlen, this.model(Ext.apply({
					mtype: 'RS.wiki.builder.Result',
					content: content
				}, resultParams)))
				break;

			case 'actiontask':
				var w = +properties.popoutWidth;
				var h = +properties.popoutHeight;

				if (isNaN(w)) {
					w = -1;
				}

				if (isNaN(h)) {
					h = -1;
				}

				this.typeConfigurations.insert(tlen, this.model({
					mtype: 'RS.wiki.builder.ActionTask',
					title: properties.title || '',
					name: properties.name || '',
					tooltip: properties.tooltip || '',
					popout: properties.popout === 'true',
					collapsed: properties.collapsed === 'true',
					popoutWidth: w,
					popoutHeight: h
				}))
				break;

			case 'detail':
				this.typeConfigurations.insert(tlen, this.model({
					mtype: 'RS.wiki.builder.Detail',
					content: content,
					progress: properties.progress === 'true',
					maximum: properties.max || '',
					offset: properties.offset || ''
				}))
				break;

			case 'wikidoc':
				var wikiName = '', matches = /#includeForm\(['|"](.*?)['|"]\)/gi.exec(content);
				
				if (matches && matches.length > 1) {
					wikiName = matches[1];
				}

				this.typeConfigurations.insert(tlen, this.model({
					mtype: 'RS.wiki.builder.WikiDoc',
					wiki: wikiName
				}))
				break;

			case 'table':
				this.typeConfigurations.insert(tlen, this.model({
					mtype: 'RS.wiki.builder.Table',
					content: content
				}))
				break;

			case 'xtable':
				this.typeConfigurations.insert(tlen, this.model({
					mtype: 'RS.wiki.builder.XTable',
					sql: properties.sql || '',
					drilldown: properties.drilldown || '',
					content: content
				}))
				break;

			case 'sociallink':
				this.typeConfigurations.insert(tlen, this.model({
					mtype: 'RS.wiki.builder.SocialLink',
					to: properties.to || '',
					displayText: properties.text || '',
					preceedingText: properties.preceedingText || '',
					openInNewTab: properties.openInNewTab === 'true'
				}))
				break;

			default:
				this.typeConfigurations.insert(tlen, this.model({
					mtype: 'RS.wiki.builder.Source',
					content: content
				}))
				clientVM.displayError(this.localize('unknownLegacyType', type.toLowerCase()))
				break;
		}
	},

	getGeneratedContent: function() {
		var content = this.generateContent(),
			sectionConfig = ':type=' + this.types.join(',');

		if (this.title != this.localize('untitledSection')) {
			sectionConfig += '|title=' + this.title;
		}

		if (this.maxHeight > -1) {
			sectionConfig += '|height=' + this.maxHeight;	
		} 

		if (this.hidden) {
			sectionConfig += '|hidden=' + this.hidden;
		}

		if (!this.active) {
			sectionConfig += '|active=' + this.active;
		}

		return '{section' + sectionConfig + '}\n' + content + '\n{section}'
	},

	generateContent: function() {
		var content = [], hasColumns = this.typeConfigurations.length > 3;

		if (hasColumns) {
			content.push(RS.wiki.pagebuilder.Constant.TABLETAG);
		}

		for (var i = 0; i < this.typeConfigurations.length; i++) {
			var config = this.typeConfigurations._private.objs[i];

			if (i > 0 && config.generateContent) {
				if (hasColumns) {
					var t = this.typeConfigurations.getAt(0);
					var columnWidth = t['column' + i + 'Width'] + '%';
					var columnAlignment = t['column' + i + 'Alignment'];
					var style = 'width:' + columnWidth + ';vertical-align:' + columnAlignment + ';';
					content.push('<td style="' + style + '" class="wiki-builder">');
					content.push(config.generateContent());
					content.push('</td class="wiki-builder">');
				} else {
					content.push(config.generateContent());	
				}
			}			
		}

		if (hasColumns) {
			content.push(RS.wiki.pagebuilder.Constant.TABLECLOSINGTAG);
		} 

		return content.join('\n')
	},

	configureDefaultColumnWidths: function() {
		switch (this.typeConfigurations.length) {
			case 3:
				var t = this.typeConfigurations.getAt(0);
				t.set('column1Width', 100)
				t.set('column2Width', 0)
				t.set('column3Width', 0)
				break;
			case 4:
				var t = this.typeConfigurations.getAt(0);
				t.set('column1Width', 50)
				t.set('column2Width', 50)
				t.set('column3Width', 0)
				break;
			case 5:
				var t = this.typeConfigurations.getAt(0);
				t.set('column1Width', 33)
				t.set('column2Width', 33)
				t.set('column3Width', 33)
				break;
		}
	}
});

glu.defModel('RS.wiki.builder.AddTab', {
	showAddTab$: function() {
		return this.parentVM.typeConfigurations.length < 5
	},
	beforeactivate: function() {
		if (this.parentVM.typeConfigurations.length < 5)
			this.open({
				mtype: 'RS.wiki.SectionPicker'
			})
		return false
	},
	reallyAddSection: function(sec) {
		this.parentVM.set('types', this.parentVM.types.concat(sec.types))
		if (sec.models) {
			//use the models being provided instead
			Ext.Array.forEach(sec.models, function(model) {
				model.unParent()
				this.parentVM.typeConfigurations.insert(this.parentVM.typeConfigurations.length - 1, model)
			}, this)
		} else
			Ext.Array.forEach(sec.types, function(type) {
				this.parentVM.parseTypeConfiguration(type, {}, '')
			}, this)
		this.parentVM.configureDefaultColumnWidths()
	}
})

//Stub for the section configuration view to be added to the list of tabs for section configuration
glu.defModel('RS.wiki.builder.SectionConfiguration', {
	column1Width: 100,
	column1WidthIsVisible$: function() {
		return this.parentVM.typeConfigurations.length >= 4
	},
	column2Width: 0,
	column2WidthIsVisible$: function() {
		return this.parentVM.typeConfigurations.length >= 4
	},
	column3Width: 0,
	column3WidthIsVisible$: function() {
		return this.parentVM.typeConfigurations.length == 5
	},

	alignments: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	column1Alignment: 'top',
	column1AlignmentIsVisible$: function() {
		return this.parentVM.typeConfigurations.length >= 4
	},
	column2Alignment: 'top',
	column2AlignmentIsVisible$: function() {
		return this.parentVM.typeConfigurations.length >= 4
	},
	column3Alignment: 'top',
	column3AlignmentIsVisible$: function() {
		return this.parentVM.typeConfigurations.length == 5
	},

	init: function() {
		this.alignments.loadData([{
			name: this.localize('top'),
			value: 'top'
		}, {
			name: this.localize('middle'),
			value: 'middle'
		}, {
			name: this.localize('bottom'),
			value: 'bottom'
		}])
	}
})

glu.defModel('RS.wiki.builder.Base', {
	beforeClose: function() {
		this.message({
			title: this.parentVM.localize('confirmRemoveComponentTitle'),
			msg: this.parentVM.localize('confirmRemoveComponentBody'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.parentVM.localize('remove'),
				no: this.parentVM.localize('cancel')
			},
			fn: this.removeComponent,
			scope: this
		})
	},
	removeComponent: function(btn) {
		if (btn === 'yes') {
			this.parentVM.types.splice(this.parentVM.typeConfigurations.indexOf(this) - 1, 1)
			this.parentVM.typeConfigurations.remove(this)
			this.parentVM.configureDefaultColumnWidths()
		}
	}
})

glu.defModel('RS.wiki.builder.Source', {
	mixins: ['Base'],
	content: '',
	originalContent : '',
	updateDirtyFlag$ : function(){
		if(this.originalContent && this.content != this.originalContent){
			this.rootVM.set('isDirty', true);
		}
	},
	generateContent: function() {
		return this.content
	},
	init : function(){
		this.set('originalContent', this.content);
	}
})

glu.defModel('RS.wiki.builder.WYSIWYG', {
	mixins: ['Base'],
	content : '',
	originalContent : '',
	updateDirtyFlag$ : function(){
		if(this.originalContent && this.content != this.originalContent){
			this.rootVM.set('isDirty', true);
		}
	},
	init: function() {
		if (this.content.indexOf('<novelocity>\n') > -1 && this.content.indexOf('\n</novelocity>') > -1)
			this.set('content', this.content.substring('<novelocity>\n'.length, this.content.length - ('\n</novelocity>'.length)));
		this.set('originalContent', this.content);
	},
	generateContent: function() {
		if (this.content.trim() == "") {
			return "";
		}
		
		return '<novelocity>\n' + this.content + '\n</novelocity>';
	}
})

glu.defModel('RS.wiki.builder.HTML', {
	mixins: ['Base'],
	content: '',
	originalContent : '',
	updateDirtyFlag$ : function(){
		if(this.originalContent && this.content != this.originalContent){
			this.rootVM.set('isDirty', true);
		}
	},
	generateContent: function() {
		return this.content
	},
	init : function(){
		this.set('originalContent', this.content);
	}
})

glu.defModel('RS.wiki.builder.Groovy', {
	mixins: ['Base'],
	content: '',
	originalContent : '',
	updateDirtyFlag$ : function(){
		if(this.originalContent && this.content != this.originalContent){
			this.rootVM.set('isDirty', true);
		}
	},
	init: function() {
		this.set('content', this.content.substring('{pre}\n<%\n'.length, this.content.length - ('\n%>\n{pre}'.length)))
		this.set('originalContent', this.content);
	},
	generateContent: function() {
		return '{pre}\n<%\n' + this.content + '\n%>\n{pre}'
	}
})

glu.defModel('RS.wiki.builder.Javascript', {
	mixins: ['Base'],
	content: '',
	originalContent : '',
	updateDirtyFlag$ : function(){
		if(this.originalContent && this.content != this.originalContent){
			this.rootVM.set('isDirty', true);
		}
	},
	init: function() {
		this.set('content', this.content.substring('{pre}\n<script type="text/javascript">\n'.length, this.content.length - ('\n</script>\n{pre}'.length)))
		this.set('originalContent', this.content);
	},
	generateContent: function() {
		return '{pre}\n<script type="text/javascript">\n' + this.content + '\n</script>\n{pre}'
	}
})

glu.defModel('RS.wiki.builder.Code', {
	mixins: ['Base'],
	content: '',
	type: 'text',
	showLineNumbers: true,
	typeStore: {
		mtype: 'store',
		sorters: ['name'],
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	originalContent : '',
	updateDirtyFlag$ : function(){
		if(this.originalContent && this.content != this.originalContent){
			this.rootVM.set('isDirty', true);
		}
	},
	init: function() {
		this.parseCodeContent()
		this.typeStore.loadData([{
			name: this.localize('text'),
			value: 'text'
		}, {
			name: this.localize('javascript'),
			value: 'javascript'
		}, {
			name: this.localize('html'),
			value: 'html'
		}, {
			name: this.localize('groovy'),
			value: 'groovy'
		}, {
			name: this.localize('sql'),
			value: 'sql'
		}, {
			name: this.localize('xml'),
			value: 'xml'
		}, {
			name: this.localize('java'),
			value: 'java'
		}, {
			name: this.localize('css'),
			value: 'css'
		}])
		this.set('originalContent', this.content);
	},
	parseCodeContent: function() {
		var codeContent = [],
			codeContents = (this.content || '').split('\n');
		Ext.Array.forEach(codeContents, function(line) {
			if (line.indexOf('{code') == -1) codeContent.push(line)
		})
		this.set('content', codeContent.join('\n'))
	},
	generateContent: function() {
		return '{code:type=' + this.type + (this.showLineNumbers == false ? '|showLineNumbers=false' : '') + '}\n' + this.content + '\n{code}'
	}
})

glu.defModel('RS.wiki.builder.Image', {
	mixins: ['Base'],
	src: '',
	zoom: false,
	width: -1,
	height: -1,
	widthVal$: function() {
		if (this.width > -1) return this.width
		return null
	},
	heightVal$: function() {
		if (this.height > -1) return this.height
		return null
	},

	attach: function() {
		this.open({
			mtype: 'RS.wiki.ImageManager'
		})
	},

	generateContent: function() {
		var content = '{image:' + this.src;
		if (this.width > -1) content += '|width=' + this.width
		if (this.height > -1) content += '|height=' + this.height
		if (this.zoom) content += '|zoom=true'
		content += '}'
		return content
	}
})

glu.defModel('RS.wiki.builder.Form', {
	mixins: ['Base'],
	name: '',
	wikiName: '',
	tooltip: '',
	popout: false,
	popoutHeight: -1,
	popoutHeightIsEnabled$: function() {
		return this.popout
	},
	popoutWidth: -1,
	popoutWidthIsEnabled$: function() {
		return this.popout
	},
	title: '',
	collapsed: false,
	collapsedIsEnabled$: function() {
		return !this.popout
	},

	when_popout_is_true_then_collapsed_must_be_false: {
		on: ['popoutChanged'],
		action: function() {
			if (this.popout) this.set('collapsed', false)
		}
	},

	chooseForm: function() {
		this.open({
			mtype: 'RS.formbuilder.FormPicker'
		})
	},

	createForm: function() {
		this.open({
			mtype: 'RS.wiki.FormWizard'
		})
	},

	editForm: function() {
		clientVM.handleNavigation({
			modelName: 'RS.formbuilder.Main',
			target: '_blank',
			params: {
				name: this.name
			}
		})
	},

	generateContent: function() {
		var content = '{form:name=' + this.name;
		if (this.wikiName) content += '|wikiName=' + this.wikiName;
		if (this.popout) content += '|popout=true'
		if (this.collapsed) content += '|collapsed=true'
		if (this.title && this.title != this.localize('untitledSection')) content += '|title=' + this.title
		if (this.tooltip) content += '|tooltip=' + this.tooltip
		if (this.popoutHeight > -1) content += '|popoutHeight=' + this.popoutHeight
		if (this.popoutWidth > -1) content += '|popoutWidth=' + this.popoutWidth
		content += '}'
		return content
	}
})

glu.defModel('RS.wiki.builder.Encrypt', {
	mixins: ['Base'],
	key: '',
	value: '',

	chooseKey: function() {
		var me = this;
		this.open({
			mtype: 'RS.actiontask.PropertyDefinitionsPicker',
			dumper: function(selections) {
				me.set('key', selections[0].get('uname'))
			}
		})
	},

	chooseValue: function() {
		var me = this;
		this.open({
			mtype: 'RS.actiontask.PropertyDefinitionsPicker',
			dumper: function(selections) {
				me.set('value', selections[0].get('uname'))
			}
		})
	},

	generateContent: function() {
		return '{encrypt:key=' + this.key + '|value=' + this.value + '}'
	}
})

glu.defModel('RS.wiki.builder.Infobar', {
	mixins: ['Base'],
	height: 400,
	social: false,
	feedback: true,
	rating: true,
	attachments: true,
	history: true,
	tags: true,
	pageInfo: true,

	generateContent: function() {
		return '{infobar:social=' + this.social + '|feedback=' + this.feedback + '|rating=' + this.rating + '|attachments=' + this.attachments + '|history=' + this.history + '|tags=' + this.tags + '|pageInfo=' + this.pageInfo + '|height=' + this.height + '}'
	}
})

glu.defModel('RS.wiki.builder.Model', {
	mixins: ['Base'],
	wiki: '',
	refreshInterval: -1,
	status: 'CONDITION',
	zoom: '',
	height: -1,
	width: -1,

	statusStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	chooseDocument: function() {
		var me = this;
		me.open({
			mtype: 'RS.wiki.RunbookPicker',
			dumper: function(selection) {
				me.set('wiki', selection.get('ufullname'))
			}
		})
	},

	init: function() {
		this.statusStore.loadData([{
			name: this.localize('severity'),
			value: 'SEVERITY'
		}, {
			name: this.localize('condition'),
			value: 'CONDITION'
		}])
	},

	generateContent: function() {
		var content = []
		if (this.wiki) content.push('wiki=' + this.wiki)
		if (this.status) content.push('status=' + this.status)
		if (this.zoom) content.push('zoom=' + this.zoom)
		if (Ext.isString(this.width) || this.width > -1) content.push('width=' + this.width)
		if (Ext.isString(this.height) || this.height > -1) content.push('height=' + this.height)
		if (this.refreshInterval > -1) content.push('refreshInterval=' + this.refreshInterval)

		if (content.length == 0) content = '{model'
		else content = '{model:' + content.join('|')
		content += '}\n{model}'
		return content
	}
})

glu.defModel('RS.wiki.builder.WikiDoc', {
	mixins: ['Base'],
	wiki: '',

	chooseWikiDocument: function() {
		var me = this;
		this.open({
			mtype: 'RS.wiki.RunbookPicker',
			runbookOnly: false,
			title: this.localize('selectWiki'),
			createActionText: this.localize('createWiki'),
			dumper: function(selection) {
				me.set('wiki', selection.get('ufullname'))
			}
		})
	},
	generateContent: function() {
		return '#includeForm("' + this.wiki + '")';
	},
	jumpToWiki: function() {
		if (!this.wiki)
			return;
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			target: '_blank',
			params: {
				name: this.wiki
			}
		});
	}
});

glu.defModel('RS.wiki.builder.Result', {
	mixins: ['Base'],
	content: '',
	actionTasks: {
		mtype: 'treestore',
		fields: ['id', 'description', 'descriptionWikiLink', 'namespace', 'nodeId', 'resultWikiLink', 'task', 'wiki', 'actionTask', 'tags', {
			name: 'autoCollapse',
			type: 'boolean'
		}],
		proxy: {
			type: 'memory'
		}
	},
	actionTasksColumns: [{
		xtype: 'treecolumn',
		dataIndex: 'description',
		text: '~~description~~',
		flex: 1,
		renderer: function(value, meta, record) {
			if (value && record.get('task')) {
				//Display task template
				return value + '&nbsp;&nbsp;&nbsp;<span class="rs-accent">' + (record.get('task') ? 'Task: ' + record.get('task') : '') + '&nbsp;&nbsp;&nbsp;' + (record.get('namespace') ? 'Namespace: ' + record.get('namespace') : '') + '&nbsp;&nbsp;&nbsp;' + (record.get('wiki') ? 'Wiki: ' + record.get('wiki') : '') + '</span>';
			} else if (record.get('tags')) {
				var tags = record.get('tags').split(',')
				if (tags.length > 0) return '#' + tags.join(' #')
			}
			return value || record.get('task')
		}
	}],
	actionTasksSelections: [],

	title: '',
	encodeSummary: true,
	refreshInterval: 5,
	descriptionWidth: 0,
	order: 'DESC',
	autoCollapse: false,
	collapsed: false,
	autoHide: false,
	progress: false,
	filter: '',
	filterValue: [],

	filterStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	orderStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	init: function() {
		this.filterStore.loadData([{
			name: this.localize('GOOD'),
			value: 'GOOD'
		}, {
			name: this.localize('WARNING'),
			value: 'WARNING'
		}, {
			name: this.localize('SEVERE'),
			value: 'SEVERE'
		}, {
			name: this.localize('CRITICAL'),
			value: 'CRITICAL'
		}])

		this.orderStore.loadData([{
			name: this.localize('ASC'),
			value: 'ASC'
		}, {
			name: this.localize('DESC'),
			value: 'DESC'
		}])

		this.set('filterValue', this.filter.split(','));
		for (var i=0, l=this.filterValue.length; i<l; i++) {
			this.filterValue[i] = this.filterValue[i].trim();
		}

		this.actionTasks.setRootNode({
			id: 'root'
		})
		
		//COMPATIBILITY : parse action tasks from content. Make sure task summary (which will be used as description) is in 1 line.
		this.content = this.content.replace(/([\s\S]*)({description=[^{]*})([\s\S]*)/g, function(v,g1,g2,g3){
			return g1 + g2.replace(/\n/g, ' ') + g3;
		});
		var content = [];
		var	contents = (this.content || '').split('\n');

		//Remove first and last containing {result2} tags
		contents.splice(contents.length - 1, 1)
		contents.splice(0, 1)

		//Create a result2 macro and let it parse the action tasks for us (could be legacy formatted action tasks)
		var model = this.model({
			mtype: 'RS.wiki.macros.Results2',
			actionTasks: contents,
			real: false
		});

		//Now action tasks are in the model properly parsed, so we just need to add them to this model's action tasks
		Ext.Array.forEach(model.processedActionTasks, function(at) {
			this.actionTasks.getRootNode().appendChild(at)
		}, this)
	},
	generateContent: function() {
		var actionTasks = [],
			tags = [];
		this.actionTasks.getRootNode().eachChild(function(child) {
			actionTasks = actionTasks.concat(this.serializeAT(child))
		}, this)
		return '{result2:title=' + this.title + '|encodeSummary=' + this.encodeSummary + '|refreshInterval=' + this.refreshInterval + '|descriptionWidth=' + this.descriptionWidth + '|order=' + this.order + '|autoCollapse=' + this.autoCollapse + '|collapsed=' + this.collapsed + '|autoHide=' + this.autoHide + '|filter=' + (this.filterValue || '') + '|progress=' + this.progress + '}\n' + actionTasks.join('\n') + '\n{result2}';
	},

	serializeAT: function(node) {
		var ats = [],
			data = Ext.clone(node.data),
			ser = [],
			split = data.actionTask.split('#');

		data.task = split[0]
		data.namespace = split[1]

		Ext.Object.each(data, function(key, value) {
			if (Ext.Array.indexOf(['description', 'descriptionWikiLink', 'namespace', 'nodeId', 'resultWikiLink', 'task', 'wiki', 'autoCollapse', 'tags'], key) > -1 && value)
				ser.push(Ext.String.format('{0}={1}', key, value))
		})

		if (!node.isLeaf()) { //node.childNodes && node.childNodes.length > 0) {
			ats.push('{group:title=' + node.get('description') + (!node.get('autoCollapse') ? '|autoCollapse=' + node.get('autoCollapse') : '') + '}')
			node.eachChild(function(child) {
				ats = ats.concat(this.serializeAT(child))
			}, this)
			ats.push('{group}')
		} else {
			ats.push('{' + ser.join('|') + '}')
		}

		return ats
	},

	itemdblclick: function(record, item, index, e, eOpts) {
		this.open(Ext.applyIf({
			mtype: 'RS.wiki.builder.TaskEdit'
		}, record.data))
	},

	selectionchange: function(records) {
		this.set('actionTasksSelections', records)
	},

	edit: function() {
		this.open(Ext.applyIf({
			mtype: 'RS.wiki.builder.TaskEdit'
		}, this.actionTasksSelections[0].data))
	},
	editIsEnabled$: function() {
		return this.actionTasksSelections.length == 1
	},

	addTag: function() {
		this.open({
			mtype: 'RS.catalog.TagPicker',
			callback: 'addTags'
		})
	},
	addTags: function(newTags) {
		var tags = [];
		Ext.Array.forEach(newTags, function(tag) {
			tags.push(tag.data['name'])
		})
		if (this.actionTasksSelections.length > 0 && !this.actionTasksSelections[0].isLeaf()) {
			this.actionTasksSelections[0].appendChild({
				leaf: true,
				tags: tags.join(',')
			})
		} else {
			this.actionTasks.getRootNode().appendChild({
				leaf: true,
				tags: tags.join(',')
			})
		}
	},

	addActionTask: function() {
		this.open({
			mtype: 'RS.actiontask.ActionTaskPicker',
			dumper: {
				scope: this,
				dump: this.reallyAddActionTask
			}
		})
	},
	reallyAddActionTask: function(selections) {
		//Parse summary txt to make it 1 line description.
		var rawTaskSummary = selections[0].get('usummary');
		var parsedSummary = rawTaskSummary ? rawTaskSummary.replace(/\n/g, '. ') : '';
		if (this.actionTasksSelections.length > 0 && !this.actionTasksSelections[0].isLeaf()) {
			this.actionTasksSelections[0].appendChild({
				leaf: true,
				actionTask: selections[0].get('uname') + '#' + selections[0].get('unamespace'),
				description: parsedSummary,
				task: selections[0].get('uname'),
				namespace: selections[0].get('unamespace')
			})
		} else {
			this.actionTasks.getRootNode().appendChild({
				leaf: true,
				actionTask: selections[0].get('uname') + '#' + selections[0].get('unamespace'),
				description: parsedSummary,
				task: selections[0].get('uname'),
				namespace: selections[0].get('unamespace')
			})
		}
	},
	addGroup: function() {
		if (this.actionTasksSelections.length > 0 && !this.actionTasksSelections[0].isLeaf()) {
			this.actionTasksSelections[0].appendChild({
				description: this.localize('newGroup'),
				expanded: true
			})
		} else {
			this.actionTasks.getRootNode().appendChild({
				description: this.localize('newGroup'),
				expanded: true
			})
		}
	},
	remove: function() {
		Ext.each(this.actionTasksSelections, function(t) {
			t.remove();
		}, this);
	},
	removeIsEnabled$: function() {
		return this.actionTasksSelections.length > 0
	}
})

glu.defModel('RS.wiki.builder.TaskEdit', {
	description: '',
	descriptionIsVisible$: function() {
		return !this.isTag
	},
	descriptionWikiLink: '',
	descriptionWikiLinkIsVisible$: function() {
		return !this.isTag && !this.isGroup
	},
	namespace: '',
	namespaceIsVisible$: function() {
		return !this.isTag && !this.isGroup
	},
	nodeId: '',
	nodeIdIsVisible$: function() {
		return !this.isTag && !this.isGroup
	},
	resultWikiLink: '',
	resultWikiLinkIsVisible$: function() {
		return !this.isTag && !this.isGroup
	},
	task: '',
	taskIsVisible$: function() {
		return !this.isTag && !this.isGroup
	},
	wiki: '',
	wikiIsVisible$: function() {
		return !this.isTag && !this.isGroup
	},
	autoCollapse: false,
	autoCollapseIsVisible$: function() {
		return !this.isTag && this.isGroup
	},
	tags: '',

	height$: function() {
		return this.isGroup && !this.isTag ? 165 : 350
	},

	isTag$: function() {
		return this.tags
	},

	isGroup: false,

	tagStore: {
		mtype: 'store',
		fields: ['name'],
		proxy: {
			type: 'memory'
		}
	},
	tagColumns: [{
		dataIndex: 'name',
		text: '~~name~~',
		flex: 1
	}],

	init: function() {
		if (!this.task) this.set('isGroup', true)
		var tagSplit = this.tags.split(',')
		Ext.Array.forEach(tagSplit, function(tag) {
			this.tagStore.add({
				name: tag
			})
		}, this)
	},

	tagGridSelections: [],
	add: function() {
		this.open({
			mtype: 'RS.catalog.TagPicker',
			callback: 'addTags'
		})
	},
	addTags: function(newTags) {
		var tagName = '';
		Ext.Array.forEach(newTags, function(tag) {
			tagName = tag.get('name')
			if (this.tagStore.findExact('name', tagName) == -1) {
				this.tagStore.add({
					name: tagName
				})
			}
		}, this)
	},
	remove: function() {
		Ext.Array.forEach(this.tagGridSelections, function(selection) {
			this.tagStore.remove(selection)
		}, this)
	},
	removeIsEnabled$: function() {
		return this.tagGridSelections.length > 0
	},

	apply: function() {
		//persist tags to the tags property properly
		var tags = []
		this.tagStore.each(function(tag) {
			tags.push(tag.get('name'))
		})
		this.set('tags', tags.join(','))

		//Persist edit properties to the parent record
		var params = {}
		Ext.Object.each(this.parentVM.actionTasksSelections[0].data, function(key) {
			if (Ext.isDefined(this[key])) {
				params[key] = this[key]
			}
		}, this)
		this.parentVM.actionTasksSelections[0].set(params)
		this.parentVM.actionTasksSelections[0].commit()
		this.doClose()
	},
	cancel: function() {
		this.doClose()
	}
})

glu.defModel('RS.wiki.builder.Detail', {
	mixins: ['Base', 'Result'],
	progress: false,
	actionTasksColumns: [{
		xtype: 'treecolumn',
		dataIndex: 'description',
		text: '~~description~~',
		flex: 2,
		editor: {}
	}, {
		dataIndex: 'actionTask',
		text: '~~actionTask~~',
		flex: 1,
		editor: {}
	}, {
		dataIndex: 'wiki',
		text: '~~wiki~~',
		flex: 1,
		editor: {}
	}, {
		dataIndex: 'nodeId',
		text: '~~nodeId~~',
		flex: 1,
		editor: {}
	}],
	maximum: '',
	offset: '',
	generateContent: function() {
		var actionTasks = [];
		this.actionTasks.getRootNode().eachChild(function(child) {
			actionTasks = actionTasks.concat(this.serializeAT(child))
		}, this)
		
		return '{detail:progress='+this.progress+'|max='+this.maximum+'|offset='+this.offset+'}\n'+actionTasks.join('\n')+'\n{detail}';
	}
})

glu.defModel('RS.wiki.builder.Automation', {
	mixins: ['Base', 'Form'],
	assignNameFirstTime: true,
	isNew: false,
	when_wiki_name_changed: {
		on: ['wikiNameChanged'],
		action: function() {
			if (!!this.wikiName && this.assignNameFirstTime) {
				if (!this.isNew)
					this.ajax({
						url: '/resolve/service/wikiadmin/listABTasks',
						params: {
							name: this.wikiName
						},
						success: function(resp) {
							var respData = RS.common.parsePayload(resp);
							if (!respData.success) {
								clientVM.displayError(this.localize('getABTaskError', respData.message));
								return;
							}
							var lastIdx = 0
							var me = this;
							this.parentVM.parentVM.sections.forEach(function(sec, idx) {
								sec.typeConfigurations.forEach(function(t) {
									if (t == me)
										lastIdx = idx;
								})
							});
							var sec = this.parentVM.parentVM.reallyAddSection({
								types: ['result']
							}, lastIdx + 1);
							var result = sec.typeConfigurations.getAt(1);
							Ext.each(respData.records, function(r) {
								result.actionTasks.getRootNode().appendChild({
									leaf: true,
									actionTask: r['uname'] + '#' + r['unamespace'],
									description: r['usummary'],
									task: r['uname'],
									namespace: r['unamespace']
								})
							});
						},
						failure: function(resp) {
							clientVM.displayError(this.localize('serverError', resp.staus));
						}
					});
				else
					this.parentVM.parentVM.reallyAddSection({
						types: ['result']
					});
				this.set('assignNameFirstTime', false);
			}
		}
	},
	displayName$: function(selection) {
		return this.name ? this.name.substring(4) : '' //Pull off RBF_
	},
	chooseRunbook: function() {
		var me = this;
		this.open({
			mtype: 'RS.wiki.RunbookPicker',
			builderOnly: true,
			runbookOnly: false,
			dumper: function(selectedRunbook) {
				me.set('name', 'ABF_' + selectedRunbook.get('ufullname').replace(/[\.| |-]/g, '_').toUpperCase());
				me.set('isNew', !selectedRunbook.get('uresolutionBuilderId'));
				me.set('wikiName', selectedRunbook.get('ufullname'));
			}
		});
	},
	editAutomation: function() {
		if (!this.wikiName)
			return;
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			params: {
				name: this.wikiName,
				activeTab: 2
			},
			target: '_blank'
		});
	}
});

glu.defModel('RS.wiki.builder.ActionTask', {
	mixins: ['Base', 'Form'],

	displayName$: function() {
		return this.name ? this.name.substring(4) : '' //Pull off ATF_
	},
	chooseForm: function() {
		this.open({
			mtype: 'RS.formbuilder.FormPicker',
			formPickerTitle: this.localize('selectActionTaskForm'),
			showCreate: true,
			filter: [{
				field: 'uformName',
				condition: 'startsWith',
				value: 'atf_'
			}]
		})
	},
	createForm: function() {
		var me = this;
		this.open({
			mtype: 'RS.actiontask.ActionTaskPicker',
			dumper: function(selections) {
				me.set('name', 'ATF_' + selections[0].get('unamespace').replace(/\./gi, '_').replace(/ /gi, '_').toUpperCase() + '_' + selections[0].get('uname').replace(/\./gi, '_').replace(/ /gi, '_').toUpperCase())
				clientVM.handleNavigation({
					modelName: 'RS.formbuilder.Main',
					params: {
						name: me.name,
						create: true,
						atId: selections[0].get('id')
					},
					target: '_blank'
				})
			}
		})
	}
})

glu.defModel('RS.wiki.builder.Table', {
	mixins: ['Base'],

	columns: [],
	rows: [],

	table: null,
	tableColumns: [],
	tableSelections: [],

	init: function() {
		var contents = (this.content || '').split('\n'),
			split, row;

		//Remove first and last containing {table} tags
		contents.splice(contents.length - 1, 1)
		contents.splice(0, 1)

		Ext.Array.forEach(contents, function(record, index) {
			//split = record.split('|')


			// replace all "\|" with innocuous white space (\u000B) to not allow the array split to 
			// occur in cell data with escaped | characters
			split = record.replace('\\\|', '\u000B').split('|')

			// revert all white space back to \|
			for (var i = 0; i < split.length; i++) {
				split[i] = split[i].replace('\u000B', '\|');

			};



			row = {}
			Ext.Array.forEach(split, function(c, idx) {
				if (index == 0) this.columns.push(Ext.String.trim(c))
				else {
					row[this.columns[idx]] = Ext.String.trim(c)
				}
			}, this)
			if (index > 0) this.rows.push(row)
		}, this)

		this.configureStore()
		this.configureColumns()
	},

	configureColumns: function() {
		var cols = []
		Ext.Array.forEach(this.columns, function(col) {
			cols.push({
				dataIndex: col,
				text: col,
				flex: 1,
				editor: {}
			})
		})
		this.set('tableColumns', cols)
	},

	configureStore: function() {
		this.set('table', Ext.create('Ext.data.Store', {
			fields: this.columns,
			proxy: {
				type: 'memory'
			}
		}))

		this.table.loadData(this.rows)
	},

	addColumn: function() {
		this.open({
			mtype: 'RS.wiki.builder.ColumnAdder'
		})
	},
	removeColumn: function() {
		this.open({
			mtype: 'RS.wiki.builder.ColumnPicker'
		})
	},

	addRow: function() {
		this.table.add({})
	},
	insertRow: function() {
		this.table.insert(this.table.indexOf(this.tableSelections[0]), {})
	},
	insertRowIsEnabled$: function() {
		return this.tableSelections.length > 0
	},
	removeRow: function() {
		this.table.remove(this.tableSelections)
	},
	removeRowIsEnabled$: function() {
		return this.tableSelections.length > 0
	},

	// Generates the table content when saving to the DB
	generateContent: function() {
		var output = ['{table}'];

		output.push(this.columns.join(' | '))

		this.table.each(function(row) {
			var r = [];
			Ext.Array.forEach(this.columns, function(col) {
				var rowSanitized = row.get(col);
				rowSanitized = rowSanitized.replace(/\|/g, "\\|");
				r.push(rowSanitized)
			})
			output.push(r.join(' | '))
		}, this)


		output.push('{table}')
		return output.join('\n')
	}
})

glu.defModel('RS.wiki.builder.ColumnAdder', {
	column: '',
	columnIsValid$: function() {
		return /^[_\w][_\w\d]*$/.test(this.column) ? true : this.localize('invalidColumnName');
	},
	add: function() {
		this.parentVM.columns.push(this.column)
		this.parentVM.configureStore()
		this.parentVM.configureColumns()
		this.doClose()
	},
	addIsEnabled$: function() {
		return this.isValid;
	},
	cancel: function() {
		this.doClose()
	}
})

glu.defModel('RS.wiki.builder.ColumnPicker', {
	column: '',
	columns: {
		mtype: 'store',
		fields: ['name'],
		proxy: {
			type: 'memory'
		}
	},
	init: function() {
		Ext.Array.forEach(this.parentVM.columns, function(col) {
			this.columns.add({
				name: col
			})
		}, this)
	},
	remove: function() {
		this.parentVM.columns.splice(Ext.Array.indexOf(this.parentVM.columns, this.column), 1)
		this.parentVM.configureStore()
		this.parentVM.configureColumns()
		this.doClose()
	},
	cancel: function() {
		this.doClose()
	}
})

glu.defModel('RS.wiki.builder.XColumnPicker', {
	column: '',
	columns: {
		mtype: 'store',
		fields: ['name'],
		proxy: {
			type: 'memory'
		}
	},
	init: function() {
		Ext.Array.forEach(this.parentVM.columns, function(col) {
			this.columns.add({
				name: col.text
			})
		}, this)
	},
	remove: function() {
		var index = -1;
		Ext.Array.each(this.parentVM.columns, function(col, idx) {
			if (col.text == this.column) {
				index = idx
				return false
			}
		}, this)

		if (index > -1) {
			this.parentVM.columns.splice(index, 1)
			this.parentVM.configureStore()
			this.parentVM.configureColumns()
		}
		this.doClose()
	},
	cancel: function() {
		this.doClose()
	}
})

glu.defModel('RS.wiki.builder.XTable', {
	mixins: ['Base'],

	columns: [],
	rows: [],

	table: null,
	tableColumns: [],
	tableSelections: [],

	init: function() {
		var contents = (this.content || '').split('\n'),
			split, row;

		//Remove first and last containing {table} tags
		contents.splice(contents.length - 1, 1)
		contents.splice(0, 1)

		Ext.Array.forEach(contents, function(record, index) {
			split = record.split('|')
			row = {}
			Ext.Array.forEach(split, function(c, idx) {
				if (index == 0) {
					var colSplit = Ext.String.trim(c).split(':'),
						col = {
							text: colSplit[0],
							width: 0,
							type: '',
							dataFormat: '',
							format: '',
							dataIndex: '',
							sortable: false,
							draggable: false
						};
					var config = Ext.Object.fromQueryString(colSplit[1] || '');
					config.renderer = function(value, meta, record) {
						if (!meta.column.link)
							return value;
						var target = meta.column.linkTarget || '_blank'
						var link = new Ext.Template(meta.column.link).compile().apply(record.data);
						return '<a href="' + link + '" target="' + target + '">' + value + '</a>';
					}
					Ext.apply(col, config);
					if (col.width) col.width = Number(col.width)
					this.columns.push(col)
				} else {
					row[this.columns[idx].dataIndex || this.columns[idx].text] = Ext.String.trim(c)
				}
			}, this)
			if (index > 0) this.rows.push(row)
		}, this)

		this.configureStore()
		this.configureColumns()
	},

	configureColumns: function() {
		var cols = []
		Ext.Array.forEach(this.columns, function(col) {
			var c = {
				dataIndex: col.dataIndex || col.text,
				text: col.text,
				flex: 1,
				type: col.type,
				format: col.dataFormat,
				editor: {},
				renderer: col.renderer
			}
			if (c.type == 'date') {
				c.renderer = Ext.util.Format.dateRenderer(col.format || Ext.state.Manager.get('userDefaultDateFormat', 'Y-m-d G:i:s'))
				c.editor = {
					xtype: 'datefield'
				}
			}
			cols.push(c)
		})
		this.set('tableColumns', cols)
	},

	configureStore: function() {
		var fields = [];
		Ext.Array.forEach(this.columns, function(col) {
			fields.push({
				name: col.dataIndex || col.text,
				type: col.type,
				format: col.dataFormat,
				dateFormat: col.dataFormat
			})
		})
		this.set('table', Ext.create('Ext.data.Store', {
			fields: fields,
			proxy: {
				type: 'memory'
			}
		}))

		this.table.loadData(this.rows)
	},

	addColumn: function() {
		this.open({
			mtype: 'RS.wiki.builder.XColumnManager'
		})
	},
	editColumn: function() {
		this.open({
			mtype: 'RS.wiki.builder.XColumnManager',
			isEdit: true
		})
	},
	removeColumn: function() {
		this.open({
			mtype: 'RS.wiki.builder.XColumnPicker'
		})
	},
	columnMove: function(column, fromIdx, toIdx, eOpts) {
		this.columns.splice(toIdx, 0, this.columns.splice(fromIdx, 1)[0])
		this.configureColumns()
	},

	addRow: function() {
		this.table.add({})
	},
	insertRow: function() {
		this.table.insert(this.table.indexOf(this.tableSelections[0]), {})
	},
	insertRowIsEnabled$: function() {
		return this.tableSelections.length > 0
	},
	removeRow: function() {
		this.table.remove(this.tableSelections)
	},
	removeRowIsEnabled$: function() {
		return this.tableSelections.length > 0
	},

	generateContent: function() {
		var output = [];
		var xtable = ''
		if (this.drilldown) xtable = 'drilldown=' + this.drilldown
		if (this.sql) {
			if (xtable.length > 0) xtable += '|'
			xtable += 'sql=' + this.sql
		}

		output.push('{xtable' + (xtable.length > 0 ? ':' : '') + xtable + '}')

		var cols = []
		Ext.Array.forEach(this.columns, function(col) {
			var tCol = Ext.clone(col)
			delete tCol.text
			delete tCol.renderer
			if (tCol.width == 0) delete tCol.width
			cols.push(col.text + ':' + Ext.Object.toQueryString(tCol))
		})
		output.push(cols.join(' | '))

		this.table.each(function(row) {
			var r = [];
			Ext.Array.forEach(this.columns, function(col) {
				if (col.type == 'date')
					r.push(Ext.Date.format(row.get((col.dataIndex || col.text)), col.dataFormat))
				else if (col.type == 'boolean')
					r.push(row.get((col.dataIndex || col.text)) ? 1 : 0)
				else
					r.push(row.get((col.dataIndex || col.text)))
			})
			output.push(r.join(' | '))
		}, this)

		output.push('{xtable}')
		return output.join('\n')
	}
})

glu.defModel('RS.wiki.builder.XColumnManager', {
	isEdit: false,

	title$: function() {
		return this.isEdit ? this.localize('editColumn') : this.localize('addColumn')
	},
	link: '',
	linkTarget: '',
	column: '',
	columnIsVisible$: function() {
		return this.isEdit
	},
	columnStore: {
		mtype: 'store',
		fields: ['text', 'type', 'dataIndex', 'format', 'dataFormat', {
			name: 'width',
			type: 'int'
		}, {
			name: 'sortable',
			type: 'boolean'
		}, {
			name: 'draggable',
			type: 'boolean'
		}],
		proxy: {
			type: 'memory'
		}
	},
	when_column_changes_update_field_data: {
		on: ['columnChanged'],
		action: function() {
			Ext.Array.each(this.parentVM.columns, function(col) {
				if (col.text == this.column) {
					this.set('text', col.text)
					this.set('type', col.type)
					this.set('dataIndex', col.dataIndex)
					this.set('format', col.format)
					this.set('dataFormat', col.dataFormat)
					this.set('width', col.width)
					this.set('sortable', col.sortable)
					this.set('draggable', col.draggable)
					this.set('link', col.link)
					this.set('linkTarget', col.linkTarget)
					return false
				}
			}, this)
		}
	},

	text: '',
	textIsValid$: function() {
		return !!this.text ? true : this.localize('invalidText');
	},
	columnNames: {
		mtype: 'store',
		fields: ['name'],
		proxy: {
			type: 'memory'
		}
	},
	type: '',
	typeStore: {
		mtype: 'store',
		sorters: ['name'],
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	dataIndex: '',
	format: '',
	formatIsVisible$: function() {
		return this.type == 'date'
	},
	formatStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	dataFormat: '',
	dataFormatIsVisible$: function() {
		return this.type == 'date'
	},
	dataFormatStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	width: 0,
	sortable: false,
	draggable: false,

	init: function() {
		this.columnStore.loadData(this.parentVM.columns)

		this.formatStore.loadData([{
			name: 'Y-m-d H:i:s',
			value: 'Y-m-d H:i:s'
		}, {
			name: 'Y-m-d',
			value: 'Y-m-d'
		}])
		this.dataFormatStore.loadData([{
			name: 'Y-m-d H:i:s',
			value: 'Y-m-d H:i:s'
		}, {
			name: 'Y-m-d',
			value: 'Y-m-d'
		}])

		this.typeStore.loadData([{
			name: this.localize('stringType'),
			value: ''
		}, {
			name: this.localize('intType'),
			value: 'int'
		}, {
			name: this.localize('floatType'),
			value: 'float'
		}, {
			name: this.localize('dateType'),
			value: 'date'
		}])
	},

	add: function() {
		this.parentVM.columns.push({
			text: this.text,
			type: this.type,
			dataIndex: this.dataIndex,
			format: this.format,
			dataFormat: this.dataFormat,
			width: this.width,
			sortable: this.sortable,
			draggable: this.draggable,
			link: this.link,
			linkTarget: this.linkTarget
		})
		this.parentVM.configureStore()
		this.parentVM.configureColumns()
		this.doClose()
	},
	addIsVisible$: function() {
		return !this.isEdit
	},
	addIsEnabled$: function() {
		return this.isValid;
	},
	apply: function() {
		Ext.Array.each(this.parentVM.columns, function(col) {
			if (col.text == this.column) {
				Ext.apply(col, {
					text: this.text,
					type: this.type,
					dataIndex: this.dataIndex,
					format: this.format,
					dataFormat: this.dataFormat,
					width: this.width,
					sortable: this.sortable,
					draggable: this.draggable,
					link: this.link,
					linkTarget: this.linkTarget
				})
				return false
			}
		}, this)
		this.parentVM.configureStore()
		this.parentVM.configureColumns()
		this.doClose()
	},
	applyIsEnabled$: function() {
		return this.column && this.isValid;
	},
	applyIsVisible$: function() {
		return this.isEdit
	},
	cancel: function() {
		this.doClose()
	}
})


glu.defModel('RS.wiki.builder.Progress', {
	mixins: ['Base'],

	text: '',
	width: -1,
	height: -1,
	border: -1,
	borderColor: '',
	displayText: '',
	completedText: '',

	attach: function() {
		this.open({
			mtype: 'RS.wiki.ImageManager'
		})
	},

	bgStyle$: function() {
		return '<div style="height:15px;width:15px;border:1px solid #000000;background-color:' + (this.borderColor || '#000000') + '"></div>';
	},
	selectBgColor: function(color) {
		this.set('borderColor', '#' + color)
	},

	generateContent: function() {
		var props = []
		if (this.text) props.push(this.text)
		if (this.height > -1) props.push('height=' + this.height)
		if (this.width > -1) props.push('width=' + this.width)
		if (this.border > -1) props.push('border=' + this.border)
		if (this.borderColor) props.push('borderColor=' + this.borderColor)
		if (this.displayText) props.push('displayText=' + this.displayText)
		if (this.completedText) props.push('completedText=' + this.completedText)

		var propString = props.join('|')
		if (propString) propString = ':' + propString

		return '{progress' + propString + '}\n{progress}'
	}
})

glu.defModel('RS.wiki.builder.SocialLink', {
	mixins: ['Base'],

	displayText: '',
	preceedingText: '',
	openInNewTab: false,
	to: '',
	toIsEnabled$: function() {
		return !this.openInNewTab
	},
	toHidden: [],

	streamToStore: {
		mtype: 'store',
		fields: ['id', 'name', 'type'],
		proxy: {
			type: 'memory'
		}
	},

	init: function() {
		var split = this.to.split('&&'),
			toHidden = [];
		Ext.Array.forEach(split, function(s) {
			if (s) {
				toHidden.push(this.streamToStore.createModel(Ext.Object.fromQueryString(s)))
			}
		}, this)

		this.to = []
		this.set('toHidden', toHidden)
	},

	showAdvancedTo: function() {
		this.open({
			mtype: 'RS.social.StreamPicker'
		})
	},

	generateContent: function() {
		//serialize the to Array
		var serTo = [];
		Ext.Array.forEach(this.toHidden, function(t) {
			serTo.push(Ext.Object.toQueryString(t.data))
		})
		return '{sociallink:' + this.displayText + '|to=' + serTo.join('&&') + '|openInNewTab=' + this.openInNewTab + '|preceedingText=' + this.preceedingText + '}'
	}
})
*/
