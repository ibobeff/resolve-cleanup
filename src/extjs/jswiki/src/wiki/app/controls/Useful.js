/**
 * Displays a "useful" field with a Yes/No to allow the user to choose whether the wiki document was useful.
 */
Ext.define('RS.wiki.macros.infobar.Useful', {
	extend: 'Ext.form.Field',
	alias: 'widget.usefulfield',

	/**
	 * The css class to use for the thumbs down icon
	 */
	usefulNoCls: 'usefulNo',

	/**
	 * The css class to use for the thumbs down selected icon
	 */
	usefulNoSelectedCls: 'usefulNoSelected',

	/**
	 * The css class to use for the thumbs up icon
	 */
	usefulYesCls: 'usefulYes',

	/**
	 * The css class to use for the thumbs up selected icon
	 */
	usefulYesSelectedCls: 'usefulYesSelected',

	/**
	 * The css class to use for the thumbs selected icon (used to maintain sticky state)
	 */
	thumbSelectedCls: 'usefulSelected',

	/**
	 * True to display the control with useful text if there is no fieldLabel
	 */
	displayUsefulText: false,

	onRender: function(ct, position) {
		this.callParent(arguments);
		this.bodyEl.update('');

		if (this.displayUsefulText) {
			var textElement = document.createElement('div');
			var displayText = new Ext.Element(textElement);
			displayText.update('Useful?&nbsp;');
			displayText.addCls('usefulDisplayText');
			this.bodyEl.appendChild(displayText);
		}

		var yesElement = document.createElement('div');
		yesElement.setAttributeNode(this.createHtmlAttribute('key', 'yes'));
		this.yesThumb = new Ext.Element(yesElement);
		this.yesThumb.update('Yes');
		if (Ext.isDefined(this.value) && this.value) {
			this.yesThumb.addCls(this.usefulYesSelectedCls);
			this.yesThumb.addCls(this.thumbSelectedCls);
		} else this.yesThumb.addCls(this.usefulYesCls);
		this.bodyEl.appendChild(this.yesThumb);

		var separator = document.createElement('div');
		var separatorEl = new Ext.Element(separator);
		separatorEl.update('&nbsp;/&nbsp;');
		separatorEl.addCls('usefulDisplayText');
		this.bodyEl.appendChild(separatorEl);

		var noElement = document.createElement('div');
		noElement.setAttributeNode(this.createHtmlAttribute('key', 'no'));
		this.noThumb = new Ext.Element(noElement);
		this.noThumb.update('No');
		if (Ext.isBoolean(this.value) && !this.value) {
			this.noThumb.addCls(this.thumbSelectedCls);
			this.noThumb.addCls(this.usefulNoSelectedCls);
		} else this.noThumb.addCls(this.usefulNoCls);
		this.bodyEl.appendChild(this.noThumb);

	},
	initEvents: function() {
		this.callParent(arguments);
		if (!this.readOnly) {
			this.noThumb.on('mouseenter', this.mouseEnterThumb, this);
			this.noThumb.on('mouseleave', this.mouseLeaveThumb, this);
			this.noThumb.on('click', this.selectThumb, this);

			this.yesThumb.on('mouseenter', this.mouseEnterThumb, this);
			this.yesThumb.on('mouseleave', this.mouseLeaveThumb, this);
			this.yesThumb.on('click', this.selectThumb, this);
		}
	},

	mouseEnterThumb: function(e, t) {
		var key = t.getAttribute('key');
		if (key == 'no') {
			if (this.noThumb.hasCls(this.usefulNoSelectedCls) === false && this.noThumb.hasCls(this.thumbSelectedCls) === false) {
				this.noThumb.replaceCls(this.usefulNoCls, this.usefulNoSelectedCls);
			}
		} else {
			if (this.yesThumb.hasCls(this.usefulYesSelectedCls) === false && this.yesThumb.hasCls(this.thumbSelectedCls) === false) {
				this.yesThumb.replaceCls(this.usefulYesCls, this.usefulYesSelectedCls);
			}
		}
	},
	mouseLeaveThumb: function(e, t) {
		var key = t.getAttribute('key');
		if (key == 'no') {
			if (this.noThumb.hasCls(this.usefulNoCls) === false && this.noThumb.hasCls(this.thumbSelectedCls) === false) {
				this.noThumb.replaceCls(this.usefulNoSelectedCls, this.usefulNoCls);
			}
		} else {
			if (this.yesThumb.hasCls(this.usefulYesCls) === false && this.yesThumb.hasCls(this.thumbSelectedCls) === false) {
				this.yesThumb.replaceCls(this.usefulYesSelectedCls, this.usefulYesCls);
			}
		}
	},
	selectThumb: function(e, t) {
		var key = t.getAttribute('key');
		if (key == 'no') {
			this.setValue(false);
		} else {
			this.setValue(true);
		}
	},
	/**
	 * Private function, that ads a html attribute to a dom element
	 * @param {string} name The name of the attribute
	 * @param {string} value The value of the attribute
	 * @return {HTMLAttribute}
	 */
	createHtmlAttribute: function(name, value) {
		var attribute = document.createAttribute(name);
		attribute.nodeValue = value;
		return attribute;
	},

	setValue: function(value) {
		if (this.rendered) {
			if (value === true) {
				if (this.yesThumb.hasCls(this.usefulYesSelectedCls) === false) this.yesThumb.replaceCls(this.usefulYesCls, this.usefulYesSelectedCls);
				if (this.yesThumb.hasCls(this.thumbSelectedCls) === false) this.yesThumb.addCls(this.thumbSelectedCls);
				if (this.noThumb.hasCls(this.usefulNoSelectedCls) === true) this.noThumb.replaceCls(this.usefulNoSelectedCls, this.usefulNoCls);
			} else if (value === false) {
				if (this.noThumb.hasCls(this.usefulNoSelectedCls) === false) this.noThumb.replaceCls(this.usefulNoCls, this.usefulNoSelectedCls);
				if (this.noThumb.hasCls(this.thumbSelectedCls) === false) this.noThumb.addCls(this.thumbSelectedCls);
				if (this.yesThumb.hasCls(this.usefulYesSelectedCls) === true) this.yesThumb.replaceCls(this.usefulYesSelectedCls, this.usefulYesCls);
			} else {
				if (this.noThumb.hasCls(this.usefulNoSelectedCls) === true) this.noThumb.replaceCls(this.usefulNoSelectedCls, this.usefulNoCls);
				if (this.yesThumb.hasCls(this.usefulYesSelectedCls) === true) this.yesThumb.replaceCls(this.usefulYesSelectedCls, this.usefulYesCls);
			}
		}
		this.callParent(arguments);
	}
});