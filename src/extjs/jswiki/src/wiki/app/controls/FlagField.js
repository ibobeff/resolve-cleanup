Ext.define('RS.wiki.macros.infobar.FlagField', {
	extend: 'Ext.form.field.Display',
	alias: 'widget.flagfield',

	internalVal: false,
	documentName: '',

	onRender: function() {
		this.callParent(arguments);
		this.getEl().setStyle('cursor', 'hand');
		this.getEl().setStyle('cursor', 'pointer');
		this.getEl().on('click', function() {
			if (!this.internalVal) {
				Ext.MessageBox.show({
					title: RS.wiki.macros.locale.flagForReviewTitle,
					msg: RS.wiki.macros.locale.flagForReviewBody,
					buttons: Ext.MessageBox.YESNO,
					buttonText: {
						yes: RS.wiki.macros.locale.flagForReviewConfirm,
						no: RS.wiki.macros.locale.cancel
					},
					scope: this,
					fn: this.handleFlag
				});
			}
		}, this);
	},

	setValue: function(value) {
		if (Ext.isBoolean(value)) {
			this.internalVal = value;
			if (value) value = Ext.String.format('{0} <img src="{1}"/>', RS.wiki.macros.locale.flaggedForReview, '/resolve/images/flag-red.png');
			else value = Ext.String.format('{0} <img src="{1}"/>', RS.wiki.macros.locale.notFlaggedForReview, '/resolve/images/flag-white.png');
		}
		this.callParent(arguments);
	},

	handleFlag: function(button) {
		if (button == 'yes') {
			Ext.Ajax.request({
				url: '/resolve/service/wiki/flagDocument',
				params: {
					docFullName: this.documentName,
					comment: ''
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					var me = this;
					window.top['unflagDocument' + this.id.replace(/-/g, '')] = function(r) {
						me.setValue(false);
						window.top.Ext.get('msg-div').select('div').fadeOut({
							duration: 200,
							remove: true
						});
					};
					var link = Ext.String.format('<a style="cursor:hand;cursor:pointer" onClick="Ext.Ajax.request({url: \'/resolve/service/wiki/unFlagDocument\', params: {docFullName:\'{0}\'}, success: function(r){unflagDocument{1}(r);}});">', this.documentName, this.id.replace(/-/g, ''));
					if (response.success) {
						this.setValue(true);
						clientVM.displaySuccess(Ext.String.format(RS.wiki.macros.locale.flagSuccessMessage, link, '</a>'), null, RS.wiki.macros.locale.flagSuccess);
					} else {
						clientVM.displayError(response.message, null, RS.wiki.macros.locale.flagFailure);
					}
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			});
		}
	},

	destroy: function() {
		delete window.top['unflagDocument' + this.id.replace(/-/, '')];
		this.callParent();
	}
});