Ext.define('RS.wiki.controls.Image', {
	extend: 'Ext.Img',
	alias: 'rsimage',

	imageName: '',
	src: '',
	zoom: false,

	initComponent: function() {
		var me = this;

		me.callParent()

		if (me.zoom)
			me.on('render', function() {
				me.getEl().on('click', function() {
					displayLargeImage(me.src, me.imageName)
				})
			})
	}
})