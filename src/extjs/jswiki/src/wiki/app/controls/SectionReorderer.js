Ext.define('Ext.ux.SectionReorderer', {

	extend: 'Ext.ux.BoxReorderer',
	alias: 'plugin.sectionreorder',

	itemSelector: '.x-panel',

	init: function(section) {
		var me = this;
		me.callParent([section]);
	},

	onBoxReady: function() {
		this.callParent(arguments);
	},

	afterBoxReflow: function() {
		var me = this;

		// Cannot use callParent, this is not called in the scope of this plugin, but that of its Ext.dd.DD object
		Ext.ux.BoxReorderer.prototype.afterBoxReflow.apply(me, arguments);

		// Move the associated card to match the tab order
		if (me.dragCmp) {
			// me.container.tabPanel.setActiveTab(me.dragCmp.card);
			me.container.move(me.startIndex, me.curIndex);
			me.container.fireEvent('sectionDropped', me.container, me.startIndex, me.curIndex)
		}
	}
});