/**
 * Displays menu items defined by the user in a format similar to how the extjs examples are displayed.
 * See [Sencha product example webpage][1] for a visual example of what the control will render like.
 * [1]: http://www.sencha.com/products/extjs/examples/
 *
 * Renders the menus based on the catalog configuration passed to it.  See the {@link RS.MenuPanel#catalog} for more information.
 *
 * Additional functionality is provided in the table of contents as it can be rendered as a "jump to list"
 * where each category is simply listed, and clicking on it will jump the menu to the items in that category.
 * Another option is to render it as a tree, where the user can click on the item from the tree directly.
 * See the {@link RS.MenuPanel#toc} for more details
 */
Ext.define('RS.wiki.menu.MenuPanel', {
	extend: 'Ext.panel.Panel',
	layout: 'border',
	frame: false,
	border: false,
	alias: 'widget.rsmenupanel',

	autoSize: true,
	removeSize: false,

	/**
	 * @cfg catalog
	 * Defines the catalog of menu items to display.  Should be of the format:
	 * 	catalog : [{
	 * 			title : 'Category',
	 * 			items : [{
	 * 				title : 'Menu Item 1',
	 * 				url : 'path/to/your/site.html',
	 * 				image : 'pathToPictureDisplayingWebpage.gif',
	 * 				desc : 'Menu Item Description',
	 *				status: 'new'
	 * 			}]
	 * 	}]
	 *
	 */
	catalog: [],

	/**
	 * Initial Width of the control. If set then the control will remain at that width no matter what.  If not configured, then the control will fill the maximum width on the screen
	 */
	width: 850,

	/**
	 * Initial Height of the control. If set then the control will remain at that height no matter what.  If not configured, then the control will fill the maximum height on the screen
	 */
	height: 500,

	/**
	 * Css class to use for displaying menu items defaults to 'menupanel'
	 */
	cls: 'menupanel',

	/**
	 * Specifies the target attribute or the name of the window. The following values are supported:
	 * 		_blank - URL is loaded into a new window.
	 * 		_parent - URL is loaded into the parent frame
	 * 		_self - URL replaces the current page. This is default
	 * 		_top - URL replaces any framesets that may be loaded
	 * 		name - The name of the window
	 */
	target: '_self',

	/**
	 * @cfg {Object} tableOfContents
	 * Configures the Table of Contents
	 ** @cfg {Boolean} tableOfContents.display true to display a table of contents, false to simply render the menu
	 * @cfg {String} tableOfContents.position Determines the position to use for the table of contents.  Available options supported are 'west', 'east', 'north', 'south' (defaults to 'west')
	 * @cfg {String} tableOfContents.type Determines which way to display the table of contents.  Available options supported are 'tree' and 'list' (defaults to 'list')
	 * @cfg {Number} tableOfContents.width The width to use to display the table of contents when a position of 'west' or 'east is used
	 * @cfg {Number} tableOfContents.height The height to use to display the table of contents when a position of 'north' or 'south' is used
	 * @cfg {Boolean} tableOfContents.split True to allow the user to reconfigure the height/width of the table of contents, false to remain static
	 */
	toc: {
		display: true,
		position: 'west',
		type: 'list',
		width: 200,
		height: 200,
		split: true
	},

	initComponent: function() {
		var me = this;

		if (me.removeSize) {
			me.height = undefined
			me.width = undefined
		}

		Ext.apply(this.toc, this.tableOfContents);

		//configure catalog information
		for (var i = 0, c; c = this.catalog[i]; i++) {
			c.id = 'group-' + i;
		}

		this.store = this.store || Ext.create('Ext.data.Store', {
			fields: ['id', 'title', 'items'],
			data: this.catalog
		});

		var menuTarget = this.target;

		this.menuView = Ext.create('Ext.view.View', {
			store: this.store,
			region: 'center',
			autoHeight: true,
			autoScroll: true,
			frame: false,
			cls: 'demos',
			itemSelector: 'dl',
			overItemCls: 'over',
			trackOver: true,
			tpl: Ext.create('Ext.XTemplate',
				//@formatter:off
				'<div class="menu-ct">',
				'<tpl for=".">',
				'<div><a name="{id}"></a><h2><div>{title}</div><tpl if="xindex != 1"><span class="back-to-top">Back to top ▲</span></tpl></h2>',
				'<dl>',
				'<tpl for="items">',
				'<dd ext:url="{url}" ext:target="{[this.getTarget(values)]}" ext:node="{[this.getNode(values)]}" <tpl if="image">class="menupanel_img"<tpl else>class="menupanel_noimg"</tpl> title="{tooltip}"><tpl if="image"><img src="{image}"/></tpl>',
				'<div style="padding-top:5px"><h4><font style="font-size: 14px">{title}',
				'<tpl if="this.isNew(values.status)">',
				'<span class="new-sample"> (New)</span>',
				'<tpl elseif="this.isUpdated(values.status)">',
				'<span class="updated-sample"> (Updated)</span>',
				'<tpl elseif="this.isExperimental(values.status)">',
				'<span class="new-sample"> (Experimental)</span>',
				'<tpl else>',
				'<span class="status">{[this.getStatus(values.status)]}</span>',
				'</tpl>',
				'</font></h4><p>{desc}</p></div>',
				'</dd>',
				'</tpl>',
				'<div style="clear:left"></div></dl></div>',
				'</tpl>',
				'</div>', {
					//@formatter:on
					isExperimental: function(status) {
						return status == 'experimental';
					},
					isNew: function(status) {
						return status == 'new';
					},
					isUpdated: function(status) {
						return status == 'updated';
					},
					getStatus: function(status) {
						var result = "";
						if (status) {
							result = '(' + status + ')';
						}
						return result;
					},
					getTarget: function(values) {
						return values.target || menuTarget;
					},
					getNode: function(values) {
						return values.id
					}
				}),

			listeners: {
				itemclick: function(view, record, item, index, e) {
					var t = e.getTarget('dd', 5, true),
						url, target, node;

					if (t && !e.getTarget('a', 2)) {
						url = t.getAttributeNS('ext', 'url')
						target = t.getAttributeNS('ext', 'target')
						node = t.getAttributeNS('ext', 'node')
						if (me.fireEvent('nodeClick', me, node) !== false && url)
							window.open(url, target)
					}
				},
				containerclick: function(view, e) {
					var element = e.getTarget('', 3, true);
					if (element.hasCls('back-to-top')) {
						view.scrollBy({
							x: 0,
							y: view.getEl().dom.scrollHeight * -1
						}, true);
					} else {
						var group = e.getTarget('h2', 3, true);

						if (group) {
							group.up('div').toggleCls('collapsed');
						}
					}
				},
				containermouseover: function(view, e) {
					var group = e.getTarget('h2', 3, true);

					if (group) {
						group.toggleCls('over');
					}
				},
				containermouseout: function(view, e) {
					var group = e.getTarget('h2', 3, true);

					if (group) {
						group.toggleCls('over');
					}
				}
			}
		});

		this.items = [this.menuView];

		if (this.toc.display) {
			var tableOfContents;
			if (this.toc.type == 'tree') {
				var treeCatalog = Ext.clone(this.catalog);
				Ext.each(treeCatalog, function(item) {
					Ext.each(item.items, function(child) {
						delete child.icon;
						Ext.apply(child, {
							text: child.title,
							iconCls: 'icon-example',
							leaf: true
						});
					});
					Ext.applyIf(item, {
						text: item.title,
						iconCls: 'icon-category',
						expanded: true,
						children: item.items
					});
				});
				var store = Ext.create('Ext.data.TreeStore', {
					root: {
						expanded: true,
						children: treeCatalog
					}
				});

				tableOfContents = Ext.create('Ext.tree.Panel', {
					cls: 'treecontainer',
					split: this.toc.split,
					region: this.toc.position,
					width: this.toc.width,
					height: this.toc.height,
					store: store,
					useArrows: true,
					rootVisible: false,
					border: false,
					listeners: {
						itemclick: function(view, record, item, index, e) {
							if (record.isLeaf()) {
								window.open(record.raw.url);
							} else {
								if (record.isExpanded()) {
									record.collapse();
								} else {
									record.expand();
								}
							}
						}
					}
				});

			} else {
				if (this.toc.position == 'west' || this.toc.position == 'east') {
					tableOfContents = Ext.create('Ext.view.View', {
						region: this.toc.position,
						split: this.toc.split,
						width: this.toc.width,
						autoScroll: true,
						cls: 'side-box',
						tpl: '<ul><tpl for="."><li><a href="#{id}">{title:stripTags}</a></li></tpl></ul>',
						store: this.store
					});
				} else {
					tableOfContents = Ext.create('Ext.view.View', {
						split: this.toc.split,
						region: this.toc.position,
						height: this.toc.height,
						autoScroll: true,
						cls: 'side-box',
						tpl: '<dl><tpl for="."><dd><a href="#{id}">{title:stripTags}</a></dd></tpl></dl>',
						store: this.store
					});
				}
			}
			this.items.push(tableOfContents);
		}

		// Resize on demand
		Ext.EventManager.onWindowResize(this.doResize, this);

		this.callParent();
	},

	onRender: function(ct, pos) {
		this.callParent(arguments)
		if (this.autoSize) this.doResize()
	},
	doResize: function() {
		var size = Ext.getDoc().getViewSize();
		var windowHeight = size.height;
		var windowWidth = size.width;
		this.setSize(this.initialConfig.width || windowWidth, this.initialConfig.height || windowHeight);
	}
});