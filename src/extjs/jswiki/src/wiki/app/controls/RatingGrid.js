/**
 * The Rating Grid is a grid to display comments and ratings of feedback for a particular wiki document.  The grid is filterable so users can see which comments have which star values.
 */
Ext.define('RS.wiki.macros.infobar.RatingGrid', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.ratinggrid',

	ratingHeader: 'Rating',
	dateHeader: 'Date',
	commentsHeader: 'Comments',

	flex: 1,
	border: false,
	// features: [{
	// 	ftype: 'filters',
	// 	local: true,
	// 	updateBuffer: 0
	// }],
	autoScroll: true,

	initComponent: function() {
		this.columns = [{
			text: this.ratingHeader,
			dataIndex: 'rating',
			width: 130,
			filter: {
				type: 'list',
				active: true,
				store: Ext.create('Ext.data.Store', {
					fields: ['id', 'text'],
					data: [{
						id: 1,
						text: '<div class="starOnGrid"></div>'
					}, {
						id: 2,
						text: '<div class="starOnGrid"></div><div class="starOnGrid"></div>'
					}, {
						id: 3,
						text: '<div class="starOnGrid"></div><div class="starOnGrid"></div><div class="starOnGrid"></div>'
					}, {
						id: 4,
						text: '<div class="starOnGrid"></div><div class="starOnGrid"></div><div class="starOnGrid"></div><div class="starOnGrid"></div>'
					}, {
						id: 5,
						text: '<div class="starOnGrid"></div><div class="starOnGrid"></div><div class="starOnGrid"></div><div class="starOnGrid"></div><div class="starOnGrid"></div>'
					}],
					proxy: {
						type: 'memory',
						reader: {
							type: 'json'
						}
					}
				})
			},
			xtype: 'templatecolumn',
			tpl: '<tpl if="rating &gt;= 1"><div class="icon-large rating-star icon-star"></div></tpl><tpl if="rating &gt;= 2"><div class="icon-large rating-star icon-star"></div></tpl><tpl if="rating &gt;= 3"><div class="icon-large rating-star icon-star"></div></tpl><tpl if="rating &gt;= 4"><div class="icon-large rating-star icon-star"></div></tpl><tpl if="rating &gt;= 5"><div class="icon-large rating-star icon-star"></div></tpl>'
		}, {
			text: this.dateHeader,
			dataIndex: 'date',
			renderer: Ext.util.Format.dateRenderer('Y-m-d g:i:s A'),
			width: 180,
			filter: true
		}, {
			flex: 1,
			text: this.commentsHeader,
			dataIndex: 'comment',
			sortable: false,
			tpl: '<p>{comment}</p>'
		}];

		this.callParent();

		this.on('afterrender', function(grid) {
			// Don't wait for the menu to be shown to create the filters.
			// grid.filters.createFilters();
			// var ratingFilter = grid.filters.getFilter('rating');
			// if (ratingFilter) ratingFilter.setActive(false);
		});

		this.store.on('ratingFilterChanged', function(store, ratingValue) {
			//get the filter feature, and filter only by the selected star value
			var filters = this.filters;
			var ratingFilter = filters.getFilter('rating');
			if (ratingFilter) {
				ratingFilter.setValue(ratingValue);
				ratingFilter.setActive(true);
			}
		}, this);
	}
});
