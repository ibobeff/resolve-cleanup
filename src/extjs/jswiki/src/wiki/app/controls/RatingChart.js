/**
 * Chart to display the results of surveys for a particular wiki document.  Displays 5-star ratings along the bottom, and their values in the y-axis.
 * You shouldn't have to configure this control at all, just use its xtype
 */
Ext.define('RS.wiki.macros.infobar.RatingChart', {
	extend: 'Ext.chart.Chart',
	alias: 'widget.ratingchart',

	flex: 1,
	animate: true,
	shadow: true,
	border: false,
	theme: 'Blue',

	voteText: 'vote',
	votesText: 'votes',

	axes: [{
		hidden: true,
		type: 'Numeric',
		position: 'left',
		fields: ['data1'],
		minimum: 0
	}, {
		type: 'Category',
		position: 'bottom',
		fields: ['name'],
	}],
	background: {
		gradient: {
			id: 'backgroundGradient',
			angle: 45,
			stops: {
				0: {
					color: '#ffffff'
				},
				100: {
					color: '#ffffff'
				}
			}
		}
	},
	initComponent: function() {
		var voteText = this.voteText,
			votesText = this.votesText;
		this.series = [{
			type: 'column',
			axis: 'left',
			highlight: true,
			listeners: {
				itemmouseup: {
					scope: this,
					fn: function(item) {
						this.fireEvent('itemSelected', this, item);
					}
				}
			},
			tips: {
				trackMouse: true,
				width: 140,
				height: 28,
				renderer: function(storeItem, item) {
					this.setTitle(storeItem.get('name') + ': ' + storeItem.get('data1') + (storeItem.get('data1') === 1 ? ' ' + voteText : ' ' + votesText) + Ext.String.format(' ({0}%)', Ext.util.Format.number((storeItem.get('data1') / storeItem.get('total')) * 100, '0,0')));
				}
			},
			label: {
				display: 'insideEnd',
				field: 'data1',
				renderer: Ext.util.Format.numberRenderer('0'),
				orientation: 'horizontal',
				color: '#FFFFFF',
				contrast: true,
				'text-anchor': 'middle'
			},
			xField: 'name',
			yField: ['data1']
		}];
		this.callParent();
	}
});