Ext.define('RS.wiki.macros.TablePanel', {
	extend: 'Ext.panel.Panel',
	layout: 'fit',
	padding: 25,

	modelFields: [],
	columnsList: [],
	tableData: [],

	initComponent: function() {

		//store
		var store = Ext.create('Ext.data.Store', {
			fields: this.modelFields,
			data: this.tableData
		});

		//filters
		var filters = {
			ftype: 'filters',

			// encode and local configuration options defined previously for easier reuse
			encode: false,
			local: true,

			filters: [{
				type: 'boolean',
				dataIndex: 'visible'
			}]
		};

		//add flex if the width is not mentioned
		Ext.each(this.columnsList, function(column) {
			if (!column.width)
				column.flex = 1;
		});

		//add the grid
		this.tableGrid = Ext.create('Ext.grid.Panel', {
			border: false,
			store: store,
			// columns : columnsList,
			columns: {
				items: this.columnsList,
				defaults: {
					renderer: this.wrapText,
					filter: true
				}
			},
			loadMask: true,
			features: [filters],
			viewConfig: {
				stripeRows: true,
				enableTextSelection: true
			},
			emptyText: 'No Matching Records'

		});

		//add it to the panel
		this.items = [this.tableGrid];

		// Resize on demand
		Ext.EventManager.onWindowResize(this.doResize, this);

		//call the parent to render it
		this.callParent();

	},

	onRender: function(ct, pos) {
		this.callParent(arguments);
		this.doResize();
	},

	wrapText: function(value, metadata, record) {
		return "<p style=\"white-space: normal;word-wrap:break-word;\">" + value + "</p>";
	},
	doResize: function() {
		var size = Ext.getDoc().getViewSize();
		var windowHeight = size.height;
		var windowWidth = size.width;
		if (document.documentElement.scrollHeight >= document.documentElement.clientHeight) //to accomodate other grids/controls on the page and avoid the horizontal scroll bar
			this.setSize(this.initialConfig.width || windowWidth, this.initialConfig.height || windowHeight);
		else
			this.setSize(this.initialConfig.width || windowWidth - Ext.getScrollBarWidth(), this.initialConfig.height || windowHeight);
	}
});