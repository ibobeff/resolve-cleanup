/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.wiki').locale = {
	windowTitle: 'Wiki Project',
	wikiAdminWindowTitle: 'Wiki Administration',
	viewColumnTooltip: 'View',
	ServerError: 'Server Error',
	serverError: 'Server Error [{0}]',
	invalidNamespace: 'Namespace is required and should only contain alphabets, numbers, \'-\' and \'_\'',
	invalidName: 'Name is required and should only contain alphabets, numbers, \'-\' and \'_\'',
	//Fields
	//Look up
	uregex: 'Regex',
	uwikiDocName: 'Wiki',
	uwikiName: 'Wiki',
	umodule: 'Module',
	uorder: 'Order',
	DeleteSucMsg: 'The selected items have been deleted',
	DeleteErrMsg: 'Cannot delete the selected items',
	cancel: 'Cancel',
	close: 'Close',
	result: 'Result',
	//Tempalte
	uname: 'Name',
	summary: 'Summary',
	uwiki: 'Wiki',
	uformName: 'Form',
	udescription: 'Description',

	deleteText: 'Delete',

	description: 'Description',
	defaultValue: 'Default Value',

	//wiki admin
	unamespace: 'Namespace',
	ufullname: 'Name',
	uisDeleted: 'D',
	uisDeletedTip: 'Deleted',
	uhasActiveModel: 'RB',
	uhasActiveModelTip: 'Runbook',
	uisRoot: 'DT',
	uisRootTip: 'Decision Tree',
	uisActive: 'A',
	uisActiveTip: 'Active',
	uisLocked: 'L',
	uisLockedTip: 'Locked',
	uisHidden: 'H',
	uisHiddenTip: 'Hidden',
	usummary: 'Summary',
	assignedTo: 'Assigned To',

	busyTitle: 'Processing',
	busyText: 'Processing request, please wait..',

	//wiki admin rights
	pageBuilder: 'Layout',
	viewRight: 'View',
	editRight: 'Edit',
	executeRight: 'Execute',
	adminRight: 'Admin',
	editing: '(editing)',
	readOnlyTxt: '(read-only)',
	versionTxt: '(v.{0})',

	//attachment admin
	fileName: 'Name',
	fileSize: 'Size',
	size: 'Size',
	uploadedBy: 'Uploaded By',
	uploadedOn: 'Uploaded On',
	upload: 'Upload',
	deleteAttachment: 'Delete',

	//history admin
	documentName: 'Document Name',
	revisionNumber: 'Revision #', // 'Version #', TODO - support Version Control
	type: 'Type',
	comment: 'Comment',
	createdBy: 'Created By',
	createdOn: 'Created On',
	commitRevision: 'Commit',
	viewRevision: 'View',
	compareRevision: 'Compare',
	rollbackRevision: 'Rollback',
	resetRevision: 'Reset',

	save: 'Save',
	saveAs: 'Save As',
	saveAndCommit: 'Commit',
	back: 'Back',

	sysCreatedOn: 'Created On',
	sysCreatedBy: 'Created By',
	sysUpdatedOn: 'Updated On',
	sysUpdatedBy: 'Updated By',
	sysId: 'Sys ID',
	sysOrganizationName: 'Organization',

	name: 'Name',
	GetRolesErr: 'Cannot get roles from the server. [{0}]',
	Main: {
		dtViewer: 'Decision Tree',
		wikiDocument: 'Document',
		WikiSaved: 'Document saved successfully',
		automationSaved: 'Automation saved successfully',
		automationSavedFailure: 'Cannot save automation: {0}',

		lockedBy: '<b>{0}</b> is editing',
		lockedByTooltip: 'Click to override lock',

		autoRedirectTitle: '',
		autoRedirectMsg: '<a href="#RS.wiki.Main/name={0}&isEditMode=true">Click here to edit the document</a>',

		exitBuilderWithoutSaveTitle: 'Exit without saving?',
		exitBuilderWithoutSaveBody: 'Changes you have made to the document have not been saved, would you like to save before exiting?',
		reloadWithoutSaveTitle : 'Reload without saving?',
		reloadWithoutSaveBody : 'Reload will lose all changes you have made to the document.<br >Proceed to reload?',
		exitWithoutSaveTitle: 'Exit without saving?',
		exitWithoutSaveBody: 'Changes you have made to the document have not been saved, would you like to save before exiting?',
		dontSave: "Don't Save",

		loadVersionWithoutSaveTitle: 'Load version {0} without saving?',
		loadVersionWithoutSaveBody: 'Changes you have made to the current ActionTask have not been saved, would you like to save before opening the new version?',

		useDefaultRights: 'Use Default Rights',

		displayOptions: 'Display Options',
		wiki: 'Document',
		decisionTree: 'Decision Tree',
		catalog: 'Catalog',

		intText: 'Int',
		decimalText: 'Decimal',
		stringText: 'String',
		dateText: 'Date',

		utitle: 'Title',
		fullName: 'Full name',

		feedback: 'Feedback',

		liveTab: 'Live',
		viewTab: 'View',
		editTab: 'Edit',
		editPage: 'Edit',
		savePage: 'Save',
		preview: 'Preview',
		automationTab: 'Automation',
		decisionTreeTab: 'Decision Tree',
		automationViewTab: 'Automation',
		decisionTreeViewTab: 'Decision Tree',

		//Auto Creation Message
		autoCreatedTitle: 'New Document',
		autoCreatedMessage: 'This document does not currently exist, click save to create the document.  If you believe this document should exist, <a href="#RS.search.Main/search={0}"> try searching for it</a>',

		//validation for wiki name
		nameRequired: 'Please provide a document name',
		invalidWikiNameFormat: 'Invalid document name format.<br/>Use: Namespace.Document Name (e.g., Network.Health Check)',

		//properties
		uname: 'Name',
		unamespace: 'Namespace',

		//view
		pageInfo: 'Page Info',
		tableOfContents: 'Table of Contents',
		infoBar: 'Infobar',
		//attachments: 'Attachments',
		print: 'Print',
		goTo: 'Go to',
		listDocuments: 'Documents',
		history: 'History',

		submitFeedback: 'Submit Feedback',
		viewFeedbacks: 'View Feedback',
		feedbacks: 'Feedback',

		//edit
		properties: 'Properties',
		builder: 'Builder',
		source: 'Source',
		viewRevisionList: 'Revision', // 'View Version List', TODO - support Version Control
		revisions: 'Revision', // 'Version List', TODO - support Version Control
		attachments: 'Attachments',
		more: 'More',
		file: 'File',
		lock: 'Lock',
		lockTitle: 'Confirm Document Lock',
		lockMessage: 'Are you sure you want to lock "{0}"?',
		lockAction: 'Lock',
		unlock: 'Unlock',
		documentLocked: 'Document locked successfully',
		unlockTitle: 'Confirm Document Unlock',
		unlockMessage: 'Are you sure you want to unlock "{0}"',
		unlockAction: 'Unlock',
		documentUnlocked: 'Document unlocked successfully',
		rename: 'Rename',
		move: 'Move/Rename',
		copy: 'Copy',
		replaceContent: 'Replace Content',
		deleteWiki: 'Delete',
		deleteTitle: 'Confirm Document Deletion',
		deleteMessage: 'Are you sure you want to delete "{0}"?',
		deleteAction: 'Delete',
		deleteAndForward: 'Delete & Forward',
		deleteAndForwardTooltip: 'Delete the document and forward future requests to a replacement document',
		wikiDeleted: 'Document deleted successfully',
		cancel: 'Cancel',
		undeleteWiki: 'Undelete',
		unDeleteTitle: 'Confirm Document Undeletion',
		unDeleteMessage: 'Are you sure you want to undelete "{0}"?',
		unDeleteAction: 'Undelete',
		wikiUndeleted: 'Document undeleted successfully',
		purge: 'Purge',
		purgeTitle: 'Confirm Document Purge',
		purgeMessage: 'Are you sure you want to purge "{0}"?  This action cannot be undone!',
		purgeAction: 'Purge',
		wikiPurged: 'Document purged successfully',
		social: 'Collaboration',
		follow: 'Follow',
		documentFollowed: 'Document followed',
		unfollow: 'Unfollow',
		documentUnfollowed: 'Document unfollowed',
		lockStream: 'Disable Feedback',
		documentStreamLocked: 'Document feedback stream disabled successfully',
		lockStreamTitle: 'Disable Feedback?',
		lockStreamMessage: 'Do you want to disable the feedback stream for "{0}"?  This means no further posts or comments can be made to this document.',
		lockStreamAction: 'Disable',
		unlockStream: 'Enable Feedback',
		documentStreamUnlocked: 'Document feedback stream enabled successfully',
		unlockStreamTitle: 'Enable Feedback?',
		unlockStreamMessage: 'Do you want to enable the feedback stream for "{0}"?  This will enable users to post and comment on the document.',
		unlockStreamAction: 'Enable',
		viewFollowers: 'View Followers',
		ratings: 'View Ratings',

		roles: 'Roles',
		addRole: 'Add',
		removeRole: 'Remove',
		tags: 'Tags',
		addTag: 'Add',
		removeTag: 'Remove',
		catalogs: 'Catalogs',
		addCatalog: 'Add',
		removeCatalog: 'Remove',

		//automation
		xml: 'XML',
		parameters: 'Params',
		addParam: 'Add',
		removeParam: 'Remove',
		mainModel: 'Main Model',
		abortModel: 'Abort Model',
		decisionTreeModel: 'Decision Tree',
		backAutomation: 'Back',
		saveAutomation: 'Save',
		mainModelXmlTitle: 'Main Model XML',
		abortModelXmlTitle: 'Abort Model XML',
		decisionTreeXmlTitle: 'Decision Tree Model XML',
		resolutionBuilder: 'Automation Builder',

		confirmNavigationTitle: 'Confirm Navigation',
		confirmNavigationMsg: 'The model you are working on has not been saved.  Are you sure you want to navigate away?',
		continueNavigation: 'Continue Navigation',

		confirmNavigationToBuilderMsg: 'Changes you have made to the document have not been saved. Navigating away will lose all changes you have made.<br><br> Proceed to Automation Builder?',

		help: 'Help',
		executionTooltip: 'Execute',
		execution: 'Execute',
		feedbackTooltip: 'Feedback',
		lastReviewedText: 'Last reviewed on: {0}<br/>Last reviewed by: {1}',
		never: 'never',
		noone: 'no one',
		feedback: 'Feedback',
		//social: 'Collaboration',
		social: 'Worksheet Collaboration',
		socialTooltip: 'Collaboration',
		worksheetSocialTooltip: 'Worksheet Collaboration',
		refresh: 'Reload',
		showFeedback: 'Show Feedback',

		//builder
		backBuilder: 'Back',
		addSection: 'Add Section',
		addLayoutSection: 'Section',
		editLayout: 'Edit Layout',

		//revision
		rollbackRevisionMsg: 'Rollback resets to a previous revision and remove all later revisions. Do you want to roll back?', // 'Rollback loads and saves the version you have selected as latest.Do you want to rollback?', TODO - support Version Control
		rollbackSuccess: 'Successfully rollback to the selected version.',
		resetRevisionMsg: 'Resetting keeps the latest version and removes all previous versions. Are you sure you want to reset?',
		resetSuccess: 'Version reset.',
		resetError: 'Version error: {0}',

		//attachment
		renameAttachment: 'Rename',
		referGlobalAttachments: 'Reference Global',
		markAsGlobal: 'Mark As Global',
		markAsGlobalTooltip: 'Mark As Global Attachment',
		renameAttachmentTooltip: 'Rename the selected attachment.',
		referGlobalAttachmentsTooltip: 'Make a reference to global attachments.',
		duplicateWarn: 'Duplicate detected',
		duplicateDetected: '{0} is already in the attachment list.',
		duplicateDetected: '{0} are already in the attachment list.',

		overrideSoftLock: 'Override the soft lock',
		overrideSoftLockAction: 'Override',
		confirmOverride: 'Are you sure you want to override the lock?',
		invalidModel: 'Invalid Model',
		invalidTasksDetected: 'Invalid ActionTasks detected in {0} model: {1}',
		invalidRunbooksDetected: 'Invalid Runbooks detected in {0} model: {1}',
		invalidDocumentsDetected: 'Invalid Documents detected in {0} model: {1}',
		danglingNodesDetected : 'Dangling Nodes detected in {0} model: {1}',
		invalidEdgesDetected : 'Invalid Edges detected in {0} model: {1}',
		main: 'Main',
		abort: 'Abort',
		validationError: 'Error validation models. [{0}]',
		saveAnyWay: 'Do you still want to save the wiki?',
		no: 'No',

		//Pagebuilder specifics
		editFileOptions: 'File',
		viewFileOptions: 'File',
		collaborationOptions: 'Collaboration',
		exitToView: 'Exit to View',
		pagebuilder: 'Page Builder',
		pageTab: 'Page',
		pageViewTab: 'Page',
		refreshPage: 'Refresh Page',
		reloadPage: 'Reload',
		docNotExist: 'Document not saved',
		uploadImageOnNotExistDoc: 'Please save the document before uploading images.',

		viewsettings: 'View Settings',

	},

	Section: {
		untitledSection: 'Untitled Section',

		sectionConfiguration: 'Properties',
		title: 'Title',
		collapsible: 'Collapsible',
		collapsed: 'Collapsed',
		maxHeight: 'Max Height',
		maxHeightTooltip: 'Sets the maximum height for this section.  If the content exceeds this number then the section will scroll.  -1 sets scrolling to false and sizes the section to the size of the content.',
		maxWidth: 'Max Width',
		maxHeightTooltip: 'Sets the maximum width for this section.  If the content exceeds this number then the section will scroll.  -1 sets scrolling to false and sizes the section to the size of the content.',


		confirmRemoveSectionTitle: 'Confirm section removal',
		confirmRemoveSectionBody: 'Are you sure you want to remove this section?',
		remove: 'Remove',
		cancel: 'Cancel',

		addAbove: 'Add Above',
		addAboveTooltip: 'Add a new section above this section',
		addBelow: 'Add Below',
		addBelowTooltip: 'Add a new section below this section',
		upTooltip: 'Move Up',
		downTooltip: 'Move Down',

		unknownLegacyType: 'Unable to parse unknown type {0}',
		typeNotSupported: 'Type is not supported at this time',

		confirmRemoveComponentBody: 'Are you sure you want to remove this component?',
		confirmRemoveComponentTitle: 'Confirm Component Removal'
	},

	Feedback: {
		rateThisPage: 'Submit Feedback',
		reviewThisPage: 'Submit Content Request',
		markForReview: 'Flag for review',
		useful: 'Useful',
		rating: 'Rating',
		comment: 'Comment',
		submitFeedback: 'Submit feedback',
		requestReview: 'Request Review',
		close: 'Close',
		feedbackSaved: 'Feedback successfully saved',
		reviewSaved: 'Document marked for review',
		viewFeedback: 'View Feedback',
		activeWorksheet: 'Active Worksheet',
		worksheet: 'Worksheet',
		includeWorksheetLink: 'Include link to current worksheet'
	},

	History: {
		historyTitle: 'History',
		go: 'Go',
		keyword: 'Search',
		emptySearch: 'Enter the search keywords...',
		previousPage: 'Previous Page',
		nextPage: 'Next Page',
		refreshTooltip: 'Refresh'
	},

	ListDocuments: {
		listDocumentsTitle: 'Documents',
		go: 'Go',
		title: 'Documents',
		namespace: 'Namespace',
		all: 'All'
	},

	Execute: {
		executeTitle: 'Execute',
		execute: 'Execute',

		debug: 'Debug',
		mock: 'Mock',
		newWorksheet: 'New Worksheet',
		activeWorksheet: 'Active Worksheet',

		executeSuccess: 's executed successfully'
	},

	SourceHelp: {
		sourceHelpTitle: 'Help',
		close: 'Close',
		search: 'Search'
	},

	DecisionTreeViewer: {
		decisionTreeViewerTitle: 'Decision Tree Viewer',
		submit: 'Next',
		unknownAnswer: 'Unknown Answer',
		end: 'End',
		rateThisPage: 'Rate this page',
		feedbackRating: 'Feeback / Rating',
		useful: 'Useful',
		rating: 'Rating',
		comment: 'Comment',
		submitFeedback: 'Submit feedback',
		feedbackSaved: 'Feedback successfully saved',

		previousQuestion: 'Prev',
		nextQuestion: 'Next',

		refreshQuestions: 'Refresh questions',
		refreshWiki: 'Refresh document'
	},

	DecisionTreeQuestion: {
		answer: 'Answer:{0}'
	},

	MoveWiki: {
		move: 'Move',
		rename: 'Rename',
		moveRename: 'Move/Rename',
		cancel: 'Cancel',

		name: 'Name',
		namespace: 'Namespace',
		overwrite: 'Overwrite',
		overwrite_tooltip: 'Overwrite the existing wiki document if it exists',

		wikiMoved: 'Wiki moved successfully',
		wikiRenamed: 'Wiki renamed successfully'
	},

	attention: 'Attention',
	newVersionsAvailabe: 'There are newer versions available. Do you want to commit your version anyway?',
	commitNewVersion: 'Commit New Version',

	Commit: {
		commitTitle: 'Commit Document',
		commit: 'Commit',
		cancel: 'Cancel',

		comment: 'Comment',
		postToSocial: 'Post to social',
		reviewed: 'Reviewed',

		commentRequired: 'Comment is required when posting to social',

		commitSuccess: 'Document committed successfully'
	},

	CopyWiki: {
		replace: 'Replace',
		copy: 'Copy',
		cancel: 'Cancel',

		name: 'Name',
		namespace: 'Namespace',
		overwrite: 'Overwrite',
		overwrite_tooltip: 'Overwrite the existing wiki document if it exists',
		skip: 'Skip',
		skip_tooltip: 'Skip the copy if the existing wiki document already exists',
		update: 'Update',
		update_tooltip: 'Update the existing wiki document if it already exists, otherwise create it',

		replaceContent: 'Replace Content',

		wikiCopied: 'Wiki copied successfully.',
		askForNavigation: 'Do you want to go to the new wiki?',
		gotoWiki: 'Go to the new wiki',
		no: 'No',
		wikiCopied: 'Wiki copied successfully'
	},

	SaveAsWiki: {
		saveAs: 'Save As',
		cancel: 'Cancel',

		name: 'Name',
		namespace: 'Namespace',

		overwrite: 'Overwrite',
		overwrite_tooltip: 'Overwrite the existing wiki document if it exists',

		wikiSavedAs: 'Wiki saved successfully.',
	},

	ReplaceWiki: {
		replaceContent: 'Replace Content',
		cancel: 'Cancel',

		name: 'Name',
		namespace: 'Namespace',

		wikiReplaced: 'Wiki content replaced successfully'
	},

	ForwardWiki: {
		forward: 'Forward',
		cancel: 'Cancel',

		name: 'Name',
		namespace: 'Namespace',

		wikiForwarded: 'Wiki forwarded successfully'
	},

	AddDocument: {
		addDocumentTitle: 'Add Document',
		name: 'Document Name',
		template: 'Template',

		add: 'Add',
		cancel: 'Cancel',

		emptyNameInvalid: 'Please provide a document name'
	},

	Lookups: {
		ListLookupsErrTitle: 'Get Lookups Err',
		ListLookupsErrMsg: 'Cannot get lookups from the server',
		confirmErr: 'OK',
		createLookup: 'New',
		deleteLookups: 'Delete',
		WikiLookups: 'Wiki Lookups',
		DeleteTitle: 'Delete Lookups',
		DeleteMsg: 'Are you sure you want to delete the selected item?',
		DeletesMsg: 'Are you sure you want to delete the selected items?({num} items selected)',
		confirmDelete: 'Delete',
		cancel: 'Cancel',
		ServerErrTitle: 'Server Err',
		ServerErrMsg: 'The server currently cannot handle the request.'
	},
	Lookup: {
		WikiLookups: 'Wiki Lookup',
		invalidName: 'The Regex field should not be empty.',
		invalidModule: 'The Module field should not be empty.',
		invalidOrder: 'The Order should not be empty and should be integer.',
		invalidWikiNameFormat: 'Invalid wiki document name format.<br/>Use: Namespace.Document Name (e.g., Network.Health Check)',
		invalidRegex: 'The regex is required.',
		ServerErrTitle: 'Server Err',
		ServerErrMsg: 'The server currently cannot handle the request.',
		SaveSucTitle: 'Lookup Saved',
		SaveSucMsg: 'The lookup has been successfully saved.',
		confirmSuc: 'OK',
		SaveErrMsg: 'Cannot save the lookup.'
	},

	WikiDocSearch: {
		title: 'Search Wiki',
		emptyText: 'Search..',
		type: 'Choose a type',
		choose: 'Select'
	},

	Templates: {
		Templates: 'Templates',
		createTemplate: 'New',
		deleteTemplates: 'Delete',
		DeleteTitle: 'Delete Templates',
		DeletesMsg: 'Are you sure you want to delete the selected items?({num} items selected)',
		DeleteMsg: 'Are you sure you want to delete the selected item?',
		confirmDelete: 'Delete',
		DeleteErr: 'Cannot delete templates.',
		ListTemplateErr: 'Cannot get templates from the server.',

		rename: 'Rename',
		copy: 'Copy'
	},

	Template: {
		title: 'Template',
		AccessRights: 'AccessRights',
		showRolesList: 'Add',
		removeAccessRight: 'Delete',
		GetTemplateErrMsg: 'Cannot get template from server',
		SaveErr: 'Cannot save template.',
		SaveSuc: 'The template has been saved',
		invalidName: 'Template name can only contain alphanumeric,_ and a dot.'
	},

	RenameTemplate: {
		oldName: 'Old Name',
		newName: 'New Name',
		sendReq: 'OK',
		renameActionSucMsg: 'The selected template has been renamed',
		copyActionSucMsg: 'The selected template has been copied',
		renameActionErrMsg: 'Cannot rename the template.',
		copyActionErrMsg: 'Cannot copy the template.',
		renameTitle: 'Rename',
		copyTitle: 'Copy',
		invalidTemplateName: 'Template name can only contain alphanumeric,_ and a dot.'
	},

	AccessRightsPicker: {
		addAccessRights: 'Add Access Rights',
		addAccessRightsAction: 'Add',
		cancel: 'Cancel'
	},
	//for wiki and namespace
	renameWikis: 'Move / Rename',
	copyWikis: 'Copy',
	deleteWikis: 'Delete',
	undeleteWikis: 'Undelete',
	purgeWikis: 'Purge',
	activateWikis: 'Activate Runbook',
	deactivateWikis: 'Deactivate Runbook',
	// lockWikis: 'Edit Lock',
	// unlockWikis: 'Edit Unlock',
	lockWikis: 'Lock',
	unlockWikis: 'Unlock',
	hideWikis: 'Hide',
	unhideWikis: 'Unhide',
	rateWikis: 'Rating',
	addSearchWeight: 'Add Search Weight',
	setReviewed: 'Set Reviewed',
	setExpiryDate: 'Set Expiration',
	setHomepage: 'Set Homepage',
	configRoles: 'Set Roles',
	configRolesTip: 'Apply changes to Documents not using Namespace Default Roles.',
	configDefaultRoles: 'Set Default Roles',
	configDefaultRolesTip: 'Update Default Roles and apply changes to all Documents using Namespace Default Roles.',
	configTags: 'Set Tags',
	indexWikis: 'Index',
	indexAllWikis: 'Index All',
	purgeIndex: 'Purge',
	purgeAllIndexes: 'Purge All',
	homePageDisplayName: 'Roles',
	tagDisplayName: 'Tags',
	confirm: 'OK',
	count: 'Count',
	commit: 'Commit',
	resetStats: 'Reset Statistics',
	addDefaultRoles: 'Add Default Roles',
	removeDefaultRoles: 'Remove Default Roles',


	WikiAdmin: {
		assignedToName: 'Assigned To',
		status: 'Status',
		role: 'Role',
		index: 'Index',
		more: 'More',
		displayName: 'Wiki Admin',

		roleDisplayName: 'Roles',
		tagDisplayName: 'Tags',
		select: 'Select',

		ListRecordsErr: 'Cannot get wikis from the server.',
		RenameMsg: 'Are you sure you want to rename the selected wiki? Selected wikis ({num} selected)',
		RenamesMsg: 'Are you sure you want to rename the selected wikis? Selected wikis ({num} selected)',
		RenameSucMsg: 'The selected records have been renamed.',
		RenameErrMsg: 'Cannot rename the selected wikis.',

		CopyMsg: 'Are you sure you want to copy the selected wiki? Selected wikis ({num} selected)',
		CopysMsg: 'Are you sure you want to copy the selected wikis? Selected wikis ({num} selected)',
		CopySucMsg: 'The selected records have been copied.',
		CopyErrMsg: 'Cannot copy the selected wikis.',

		DeleteMsg: 'Are you sure you want to delete the selected wiki? Selected wikis ({num} selected)',
		DeletesMsg: 'Are you sure you want to delete the selected wikis? Selected wikis ({num} selected)',
		DeleteSucMsg: 'The selected records have been deleted.',
		DeleteErrMsg: 'Cannot delete the selected wikis.',

		UndeleteMsg: 'Are you sure you want to undelete the selected wiki? Selected wikis ({num} selected)',
		UndeletesMsg: 'Are you sure you want to undelete the selected wikis? Selected wikis ({num} selected)',
		UndeleteSucMsg: 'The selected records have been undeleted.',
		UndeleteErrMsg: 'Cannot udelete the selected wikis.',

		PurgeMsg: 'Are you sure you want to purge the selected wiki? Selected wikis ({num} selected)',
		PurgesMsg: 'Are you sure you want to purge the selected wikis? Selected wikis ({num} selected)',
		PurgeSucMsg: 'The selected records have been purged.',
		PurgeErrMsg: 'Cannot purge the selected wikis.',

		ActivateMsg: 'Are you sure you want to activate the selected wiki? Selected wikis ({num} selected)',
		ActivatesMsg: 'Are you sure you want to activate the selected wikis? Selected wikis ({num} selected)',
		ActivateSucMsg: 'The selected records have been activated.',
		ActivateErrMsg: 'Cannot activate the selected wikis.',

		DeactivateMsg: 'Are you sure you want to deactivate the selected wiki? Selected wikis ({num} selected)',
		DeactivatesMsg: 'Are you sure you want to deactivate the selected wikis? Selected wikis ({num} selected)',
		DeactivateSucMsg: 'The selected records have been deactivated.',
		DeactivateErrMsg: 'Cannot deactivate the selected wikis.',

		LockMsg: 'Are you sure you want to lock the selected wiki? Selected wikis ({num} selected)',
		LocksMsg: 'Are you sure you want to lock the selected wikis? Selected wikis ({num} selected)',
		LockSucMsg: 'The selected records have been locked.',
		LockErrMsg: 'Cannot lock the selected wikis.',

		UnlockMsg: 'Are you sure you want to unlock the selected wiki? Selected wikis ({num} selected)',
		UnlocksMsg: 'Are you sure you want to unlock the selected wikis? Selected wikis ({num} selected)',
		UnlockSucMsg: 'The selected records have been unlocked.',
		UnlockErrMsg: 'Cannot unlock the selected wikis.',

		HideMsg: 'Are you sure you want to hide the selected wiki? Selected wikis ({num} selected)',
		HidesMsg: 'Are you sure you want to hide the selected wikis? Selected wikis ({num} selected)',
		HideSucMsg: 'The selected records have been hidden.',
		HideErrMsg: 'Cannot hide the selected wikis.',

		UnhideMsg: 'Are you sure you want to unhide the selected wiki? Selected wikis ({num} selected)',
		UnhidesMsg: 'Are you sure you want to unhide the selected wikis? Selected wikis ({num} selected)',
		UnhideSucMsg: 'The selected records have been unhidden.',
		UnhideErrMsg: 'Cannot unhide the selected wikis.',

		RateMsg: 'Are you sure you want to rate the selected wiki? Selected wikis ({num} selected)',
		RatesMsg: 'Are you sure you want to rate the selected wikis? Selected wikis ({num} selected)',
		RateSucMsg: 'The selected records have been rated.',
		RateErrMsg: 'Cannot rate the selected wikis.',

		ReviewMsg: 'Are you sure you want to set the selected wiki as reviewed? Selected wikis ({num} selected)',
		ReviewsMsg: 'Are you sure you want to set the selected wikis as reviewed? Selected wikis ({num} selected)',
		ReviewSucMsg: 'The selected records have been set as reviewed.',
		ReviewErrMsg: 'Cannot set the set the selected wikis as reviewed.',

		ExpiryDateMsg: 'Are you sure you want to set the expiry date for the selected wiki? Selected wikis ({num} selected)',
		ExpiryDatesMsg: 'Are you sure you want to set the expiry dates for the selected wikis? Selected wikis ({num} selected)',
		ExpiryDateSucMsg: 'The expiry dates of selected records have been set.',
		ExpiryDateErrMsg: 'Cannot set the expiry dates of selected wikis.',

		AssginRoleMsg: 'Are you sure you want to assign the selected roles to the selected wiki? Selected wikis ({num} selected)',
		AssginRolesMsg: 'Are you sure you want to assign the selected roles to the selected wikis? Selected wikis ({num} selected)',
		AssginRoleSucMsg: 'The selected roles have been assigned to the selected wikis.',
		AssginRoleErrMsg: 'Cannot assign the selected roles to the selected wikis.',

		AddSearchWeightMsg: 'Are you sure you want to add search weight to the selected wiki? Selected wikis ({num} selected)',
		AddSearchWeightsMsg: 'Are you sure you want to add search weight to the selected wikis? Selected wikis ({num} selected)',
		AddSearchWeightSucMsg: 'The search weight of the selected records have been added.',
		AddSearchWeightErrMsg: 'Cannot add search weight to the selected wikis.',

		SetHomepageTitle: 'Confirm set homepage',
		SetHomepageMsg: 'Are you sure you want to set the homepages of selected roles to the selected wiki? Selected wikis ({num} selected)',
		SetHomepageSucMsg: 'The selected roles have been assigned to the selected wiki.',
		SetHomepageErrMsg: 'Cannot set the homepages of selected roles to the selected wiki.',

		SetRoleMsg: 'Are you sure you want to assign the selected roles to the selected wiki? Selected wikis ({num} selected)',
		SetRolesMsg: 'Are you sure you want to assign the selected roles to the selected wikis? Selected wikis ({num} selected)',
		SetRoleSucMsg: 'The selected roles have been assigned to the selected wikis.',
		SetRoleErrMsg: 'Cannot assign the selected roles to the selected wikis.',

		AddTagTitle: 'Add Tags',
		AddTagMsg: 'Are you sure you want to assign the selected tags to the selected wiki? Selected wikis ({num} selected)',
		AddTagsMsg: 'Are you sure you want to assign the selected tags to the selected wikis? Selected wikis ({num} selected)',
		AddTagSucMsg: 'The selected tags have been assigned to the selected wikis.',
		AddTagErrMsg: 'Cannot assign the selected tags to the selected wikis.',

		RemoveTagTitle: 'Remove Tags',
		RemoveTagMsg: 'Are you sure you want to remove the selected tags from the selected wiki? Selected wikis ({num} selected)',
		RemoveTagsMsg: 'Are you sure you want to remove the selected tags from the selected wikis? Selected wikis ({num} selected)',
		RemoveTagSucMsg: 'The selected tags have been removeed to the selected wikis.',
		RemoveTagErrMsg: 'Cannot remove the selected tags from the selected wikis.',

		IndexingMsg: 'Are you sure you want to index the selected wiki?',
		IndexingsMsg: 'Are you sure you want to index the selected wikis? Selected wikis ({num} selected)',
		IndexingSucMsg: 'The selected records have been indexed.',
		IndexingErrMsg: 'Cannot index the selected wikis.',

		IndexAllWikisMsg: 'Are you sure you want to index all the wikis?',

		PurgeIndexMsg: 'Are you sure you want to purge the index of the selected wiki? Selected wikis ({num} selected)',
		PurgeIndexsMsg: 'Are you sure you want to purge the indices of the selected wikis? Selected wikis ({num} selected)',
		PurgeIndexSucMsg: 'The indices of the selected wikis have been purged.',
		PurgeIndexErrMsg: 'Cannot purge the indices of the selected wikis.',

		PurgeAllIndexMsg: 'Are you sure you want to purge all wiki indices?',

		CommitMsg: 'Are you sure you want to commit the changes of the selected wiki? Selected wikis ({num} selected)',
		CommitsMsg: 'Are you sure you want to commit the changes of the selected wikis? Selected wikis ({num} selected)',
		CommitSucMsg: 'The change of the selected wikis have been committed.',
		CommitErrMsg: 'Cannot commit the changes of the selected wikis.',

		ResetStatsMsg: 'Are you sure you want to reset the statistics of the selected wiki? Selected wikis ({num} selected)',
		ResetStatssMsg: 'Are you sure you want to reset the statistics of the selected wikis? Selected wikis ({num} selected)',
		ResetStatsSucMsg: 'The statistics of the selected wikis have been reset.',
		ResetStatsErrMsg: 'Cannot reset the statistics of the selected wikis.'
	},

	MoveOrRename: {
		RenameTitle: 'Move / Rename',
		renameAttachement: 'Rename',
		CopyTitle: 'Copy',
		namespace: 'Namespace',
		invalidNamespace: 'Namespace is required.',
		filename: 'Filename',
		overwrite: 'Overwrite',
		overwrite_tooltip: 'Overwrite the existing wiki document if it exists',		
		skip: 'Skip',
		skip_tooltip: 'Skip the copy if the existing wiki document already exists',
		skip_tooltip_attachment: 'Skip the copy if the existing attachment already exists',
		update: 'Update',
		update_tooltip: 'Update the existing wiki document if it already exists, otherwise create it',
		confirmAction: 'OK',
		ListNamespaceErrMsg: 'Cannot get namespaces from the server.',
		invalidFilename: 'Wiki name should not start with space.',
		renameAttError: 'Cannot rename the attachment [{msg}].',
		renameAttSuccess: 'The selected attachment has been renamed.'
	},

	Rater: {
		ratingTitle: 'Rating',
		Count: 'Count',
		header: 'Star',
		u1StarCount: '1',
		u2StarCount: '2',
		u3StarCount: '3',
		u4StarCount: '4',
		u5StarCount: '5',
		rate: 'Rate',
		invalidCount: 'Invalid number. Please use an integer.'
	},

	SearchWeightEditor: {
		title: 'Add Search Weight',
		weight: 'Weight',
		addSearchWeight: 'OK'
	},

	DatePicker: {
		expiryDate: 'Set Expiry Date',
		date: 'Date',
		setExpiryDate: 'OK'
	},

	AccessRights: {
		accessRights: 'AccessRights',
		uname: 'Name'
	},

	Comment: {
		dump: 'Commit'
	},

	NamespaceAdmin: {
		displayName: 'Namespace',
		renameWikis: 'Rename',
		select: 'Select',
		file: 'File',
		status: 'Status',
		role: 'Role',
		index: 'Index',
		more: 'More',

		ListRecordsErr: 'Cannot get namespaces from the server.',
		RenameMsg: 'Are you sure you want to change namespaces of all wikis under the selected namespace? Selected namespaces ({num} selected)',
		RenamesMsg: 'Are you sure you want to change namespaces of all wikis under the selected namespaces? Selected namespaces ({num} selected)',
		RenameSucMsg: 'The wikis under the selected namespace have been move to the new namespace.',
		RenameErrMsg: 'Cannot change namespaces of wikis under the selected namespaces.',

		CopyMsg: 'Are you sure you want to copy all the wikis under the selected namespace? Selected namespaces ({num} selected)',
		CopysMsg: 'Are you sure you want to copy all the wikis under the selected namespaces? Selected namespaces ({num} selected)',
		CopySucMsg: 'All the wikis under the selected namespace have been copied.',
		CopyErrMsg: 'Cannot copy wikis under the selected namespaces.',

		DeleteMsg: 'Are you sure you want to delete all the wikis under the selected namespace? Selected namespaces ({num} selected)',
		DeletesMsg: 'Are you sure you want to delete all the wikis under the selected namespaces? Selected namespaces ({num} selected)',
		DeleteSucMsg: 'All the wikis under the selected namespaces have been deleted.',
		DeleteErrMsg: 'Cannot delete all the wikis under the selected namespaces.',

		UndeleteMsg: 'Are you sure you want to undelete all the wikis under the selected namespace? Selected namespaces ({num} selected)',
		UndeletesMsg: 'Are you sure you want to undelete all the wikis under the selected namespaces? Selected namespaces ({num} selected)',
		UndeleteSucMsg: 'All the wikis under the selected namespaces have been undeleted.',
		UndeleteErrMsg: 'Cannot undelete all the wikis under the selected namespaces.',

		PurgeMsg: 'Are you sure you want to purge all the wikis under the selected namespace? Selected namespaces ({num} selected)',
		PurgesMsg: 'Are you sure you want to purge all the wikis under the selected namespaces? Selected namespaces ({num} selected)',
		PurgeSucMsg: 'All the wikis under the selected namespaces have been purged.',
		PurgeErrMsg: 'Cannot purge all the wikis under the selected namespaces.',

		ActivateMsg: 'Are you sure you want to activate all the wikis under the selected namespace? Selected namespaces ({num} selected)',
		ActivatesMsg: 'Are you sure you want to activate all the wikis under the selected namespaces? Selected namespaces ({num} selected)',
		ActivateSucMsg: 'All the wikis under the selected namespaces have been activated.',
		ActivateErrMsg: 'Cannot activate all the wikis under the selected namespaces.',

		DeactivateMsg: 'Are you sure you want to deactivate all the wikis under the selected namespace? Selected namespaces ({num} selected)',
		DeactivatesMsg: 'Are you sure you want to deactivate all the wikis under the selected namespaces? Selected namespaces ({num} selected)',
		DeactivateSucMsg: 'All the wikis under the selected namespaces have been deactivated.',
		DeactivateErrMsg: 'Cannot deactivate all the wikis under the selected namespaces.',

		LockMsg: 'Are you sure you want to lock all the wikis under the selected namespace? Selected namespaces ({num} selected)',
		LocksMsg: 'Are you sure you want to lock all the wikis under the selected namespaces? Selected namespaces ({num} selected)',
		LockSucMsg: 'All the wikis under the selected namespaces have been locked.',
		LockErrMsg: 'Cannot lock all the wikis under the selected namespaces.',

		UnlockMsg: 'Are you sure you want to unlock all the wikis under the selected namespace? Selected namespaces ({num} selected)',
		UnlocksMsg: 'Are you sure you want to unlock all the wikis under the selected namespaces? Selected namespaces ({num} selected)',
		UnlockSucMsg: 'All the wikis under the selected namespaces have been unlocked.',
		UnlockErrMsg: 'Cannot unlock all the wikis under the selected namespaces.',

		HideMsg: 'Are you sure you want to hide all the wikis under the selected namespace? Selected namespaces ({num} selected)',
		HidesMsg: 'Are you sure you want to hide all the wikis under the selected namespaces? Selected namespaces ({num} selected)',
		HideSucMsg: 'All the wikis under the selected namespaces have been hidden.',
		HideErrMsg: 'Cannot hide all the wikis under the selected namespaces.',

		UnhideMsg: 'Are you sure you want to unhide all the wikis under the selected namespace? Selected namespaces ({num} selected)',
		UnhidesMsg: 'Are you sure you want to unhide all the wikis under the selected namespaces? Selected namespaces ({num} selected)',
		UnhideSucMsg: 'All the wikis under the selected namespaces have been unhidden.',
		UnhideErrMsg: 'Cannot unhide all the wikis under the selected namespaces.',

		RateMsg: 'Are you sure you want to rate all the wikis under the selected namespace? Selected namespaces ({num} selected)',
		RatesMsg: 'Are you sure you want to rate all the wikis under the selected namespaces? Selected namespaces ({num} selected)',
		RateSucMsg: 'All the wikis under the selected namespaces have been rated.',
		RateErrMsg: 'Cannot rate all the wikis under the selected namespaces.',

		ReviewMsg: 'Are you sure you want to set the wikis under the selected namespace as reviewed? Selected namespaces ({num} selected)',
		ReviewsMsg: 'Are you sure you want to set the wikis under the selected namespaces as reviewed? Selected namespaces ({num} selected)',
		ReviewSucMsg: 'All the wikis under selected namespaces have been set as reviewed.',
		ReviewErrMsg: 'Cannot set the set the wikis under the selected namespaces as reviewed.',

		ExpiryDateMsg: 'Are you sure you want to set the expiry date for the wikis under the selected namespace? Selected namespaces ({num} selected)',
		ExpiryDatesMsg: 'Are you sure you want to set the expiry dates for the wikis under the selected namespaces? Selected namespaces ({num} selected)',
		ExpiryDateSucMsg: 'The expiry dates of wikis under the selected namespace have been set.',
		ExpiryDateErrMsg: 'Cannot set the expiry dates of selected wikis.',

		AssginRoleMsg: 'Are you sure you want to assign the selected roles to the wikis under the selected namespace? Selected namespaces ({num} selected)',
		AssginRolesMsg: 'Are you sure you want to assign the selected roles to the wikis under the selected namespaces? Selected namespaces ({num} selected)',
		AssginRoleSucMsg: 'The selected roles have been assigned to the wikis under the selected namespaces.',
		AssginRoleErrMsg: 'Cannot assign the selected roles to the wikis under the selected namespaces.',

		AddSearchWeightMsg: 'Are you sure you want to add search weight to the wikis under the selected namespace? Selected namespaces ({num} selected)',
		AddSearchWeightsMsg: 'Are you sure you want to add search weight to the wikis under the selected namespaces? Selected namespaces ({num} selected)',
		AddSearchWeightSucMsg: 'The search weight of the selected records have been added.',
		AddSearchWeightErrMsg: 'Cannot add search weight to the wikis under the selected namespaces.',

		SetRoleMsg: 'Are you sure you want to assign the selected roles to the wikis under the selected namespace? Selected namespaces ({num} selected)',
		SetRolesMsg: 'Are you sure you want to assign the selected roles to the wikis under the selected namespaces? Selected namespaces ({num} selected)',
		SetRoleSucMsg: 'The selected roles have been assigned to the wikis under the selected namespaces.',
		SetRoleErrMsg: 'Cannot assign the selected roles to the wikis under the selected namespaces.',
		AddTagTitle: 'Add Tags',
		AddTagMsg: 'Are you sure you want to assign the selected tags to the wikis under the selected namespace? Selected namespaces ({num} selected)',
		AddTagsMsg: 'Are you sure you want to assign the selected tags to the wikis under the selected namespaces? Selected namespaces ({num} selected)',
		AddTagSucMsg: 'The selected tags have been assigned to the wikis under the selected namespaces.',
		AddTagErrMsg: 'Cannot assign the selected tags to the wikis under the selected namespaces.',

		RemoveTagTitle: 'Remove Tags',
		RemoveTagMsg: 'Are you sure you want to remove the selected tags from the wikis under the selected namespace? Selected namespace:{names}',
		RemoveTagsMsg: 'Are you sure you want to remove the selected tags from the wikis under the selected namespaces? Selected namespaces ({num} selected)',
		RemoveTagSucMsg: 'The selected tags have been removeed to the wikis under the selected namespaces.',
		RemoveTagErrMsg: 'Cannot remove the selected tags from the wikis under the selected namespaces.',

		IndexingMsg: 'Are you sure you want to index the wikis under the selected namespace?',
		IndexingsMsg: 'Are you sure you want to index the wikis under the selected namespaces?',
		IndexingSucMsg: 'The index request has been submitted for selected record(s).',
		IndexingErrMsg: 'Cannot index the wikis under the selected namespaces.',
		IndexAllWikisMsg: 'Are you sure you want to index all the wikis?',

		PurgeIndexMsg: 'Are you sure you want to purge the wikis under the selected namespace? Selected namespaces ({num} selected)',
		PurgeIndexsMsg: 'Are you sure you want to purge the wikis under the selected namespaces? Selected namespaces ({num} selected)',
		PurgeIndexSucMsg: 'The purge index request has been submitted for selected record(s).',
		PurgeIndexErrMsg: 'Cannot index the wikis under the selected namespaces.',
		PurgeAllIndexMsg: 'Are you sure you want to purge all wiki indices?',

		CommitMsg: 'Are you sure you want to commit the changes of the wikis under the selected namespace? Selected namespaces ({num} selected)',
		CommitsMsg: 'Are you sure you want to commit the changes of the  wikis under the selected namespaces? Selected namespaces ({num} selected)',
		CommitSucMsg: 'The change of the wikis under the selected namespaces have been committed.',
		CommitErrMsg: 'Cannot commit the changes of the wikis under the selected namespaces.',

		ResetStatsMsg: 'Are you sure you want to reset the statistics of the wikis under the selected namespace? Selected namespaces ({num} selected)',
		ResetStatssMsg: 'Are you sure you want to reset the statistics of the wikis under the selected namespaces? Selected namespaces ({num} selected)',
		ResetStatsSucMsg: 'The statistics of the wikis under the selected namespaces have been reset.',
		ResetStatsErrMsg: 'Cannot reset the statistics of the wikis under the selected namespaces.',

		SetDefaultRoleMsg: 'Are you sure you want to assign the default roles to the wikis under the selected namespace? Selected namespaces: <b>{names}</b><br><br><b>Note:</b> This will only apply for new document.',
		SetDefaultRolesMsg: 'Are you sure you want to assign the default roles to the wikis under the selected namespaces? Selected namespaces ({num} selected)',
		SetDefaultRoleSucMsg: 'The default roles have been assigned to the wikis under the selected namespaces.',
		SetDefaultRoleErrMsg: 'Cannot assign the default roles to the wikis under the selected namespaces.'
	},

	AttachmentsAdmin: {
		sizeFilterLabel: 'Size(in Bytes)',
		invalidBytes: 'Please enter the file size in Bytes and it should be integer.',
		ListRecordsErr: 'Cannot get attachments from the server',
		displayName: 'Attachments',
		upload: 'Upload',
		rename: 'Rename',
		deleteAttachments: 'Delete',
		index: 'Index',
		DeleteTitle: 'Delete Attachments',
		DeleteMsg: 'Are you sure you want to delete the selected attachment? Selected attachments:{names}',
		DeletesMsg: 'Are you sure you want to delete the selected attachments? Selected attachments({num} selected):{names}',
		DeleteSucMsg: 'The selected records have been deleted.',
		DeleteErrMsg: 'Cannot delete the selected attachments.',
		RenameMsg: 'Are you sure you want to rename the selected record?',
		RenameErrMsg: 'Cannot rename the attachments.'
	},

	AttachFile: {
		attachFileWindowTitle: 'Upload Attachments',
		attach: 'Upload',
		queuedStatus: 'Queued',
		errorStatus: 'Error',
		completedStatus: 'Completed',
		processingStatus: 'Processing...',
		uploadingStatus: 'Uploading...',
	},
	uhasResolutionBuilder: 'Automation Builder',
	RunbookPicker: {
		title: 'Select Automation',
		displayName: 'Runbooks',
		dump: 'Select',

		namespace: 'Namespace',
		all: 'All',

		saveRunbookAndClose: 'Create',
		saveAutomationAndClose: 'Create',
		createAutomationMeta: 'New Automation Builder',
		createRunbook: 'New Designer',
		newAutomation: 'New Automation',
		selectExistingRunbook: 'Select existing runbook',
		saveAutomationMetaError: 'Cannot save the automation. [{0}]',
		invalidNameConvention: 'Name is required.',
		invalidNamespaceConvention: 'Namespace is required.',
		cannotUseCurrentWiki: 'Automation runbook cannot be created in itself.',
		duplicatedDocumentError : '<i style="color:red;">* Cannot select same document</i>'
	},
	TagConfiguration: {
		searchTag: 'Search',
		title: 'Tags',
		add: 'Add',
		remove: 'Remove',
		cancel: 'Cancel'
	},

	PageInfo: {
		wikiDoc: 'Wiki Document',

		pageInfo: 'Page Info',
		runbookDependancies: 'Runbook Dependencies',
		close: 'Close',
		actionTask: 'Action Task',
		runbook: 'Runbook',
		outgoing: 'Outgoing',
		incoming: 'Incoming',

		AbortModel: 'Abort Model',
		DecisionTreeModel: 'Decision Tree Model',
		MainModel: 'Main Model',
		Content: 'Content',

		summary: 'Summary',
		emptySummary: 'Summary',
		fullName: 'Name',
		id: 'System ID',
		updatedOn: 'Updated On',
		updatedBy: 'Updated By',
		expirationDate: 'Expiration Date',
		version: 'Version',
		viewed: 'Viewed',
		edited: 'Edited',
		rating: 'Rating',
		executed: 'Executed',
		embeddedUrl: 'Embedded URL',
		lastReviewedBy: 'Last Reviewed By',
		reviewedOn: 'Last Reviewed On',
		tagsDisplayed: 'Tags',

		outRefLink: 'Outgoing Reference Links',
		inRefLink: 'Incoming Reference Links',
		resetStats: 'Reset Statistics'
	},

	AccessRights: {
		roles: 'Roles',
		setRights: 'Set Roles'
	},
	RevisionView: {
		title: "{documentName} -- Version No.{revision}",
		displayTab: 'Display',
		sourceTab: 'Source',
		mainTab: 'Main Model',
		abortTab: 'Abort Model'
	},
	RevisionComparison: {
		versionTitle: 'Version No.{revision}',
		revisionComparison: 'Compare Versions',
		contentTab: 'Content',
		exceptionTab: 'Abort Model',
		decisionTreeTab: 'Decision Tree',
		processTab: 'Main Model'
	},
	fileName: 'File',
	GlobalAttachmentPicker: {
		globalAttachment: 'Global Attachments',
		keyword: 'Search',
		dump: 'Select'
	},

	SectionPicker: {
		chooseComponent: 'Choose Component',
		filter: 'Filter',
		filterEmpty: 'Component name...',
		addComponent: 'Add',

		//section types
		Image: 'Image',
		Result: 'Result',
		Model: 'Model',
		Form: 'Form',
		WYSIWYG: 'WYSIWYG',
		Groovy: 'Groovy',
		Javascript: 'JavaScript',
		HTML: 'HTML',
		Source: 'Source',
		ActionTask: 'Action Task',
		Runbook: 'Runbook',
		InfoBar: 'InfoBar',
		Encrypt: 'Encrypt',
		Table: 'Table',
		XTable: 'XTable',
		Code: 'Code',
		WikiDoc: 'Wiki Document',
		Detail: 'Detail',
		Progress: 'Progress',
		SocialLink: 'Social Link',
		Automation: 'Automation'
	},

	LayoutPicker: {
		apply: 'Apply',
		layoutPicker: 'Layout Picker'
	},

	RunbookWizard: {
		runbookWizardTitle: 'Runbook Wizard',
		create: 'Create',
		name: 'Form Name',
		searchRunbooks: 'Search runbooks...'
	},

	FormWizard: {
		create: 'Create',
		formWizardTitle: 'Create a Form',
		formNameInvalidText: 'Form name must be all uppercase letters, numbers, or underscore.',
		formNameInvalidTitle: 'Form name is invalid',
		fieldNameInvalid: 'Field name can only contain letters without spaces',
		formNameExist: 'Form already exist, please enter a differnet name.'
	},

	ImageManager: {
		imageManagerTitle: 'Image Picker',
		select: 'Select'
	},

	designerPanel: 'Designer Panel',
	designerPanelReadOnlyTxt: 'Read Only. Full control shown in edit mode.'

}
Ext.namespace('RS.wiki.macros').locale = {

	//InfoBar
	social: 'Social',

	summary: 'Summary',

	feedback: 'Feedback',
	detailedQuestions: 'Detailed Questions',
	submitFeedback: 'Submit Feedback',
	cancel: 'Cancel',
	useful: 'Useful',
	overallRating: 'Overall',
	feedbackComment: 'Comment',
	lastReviewed: 'Last Reviewed',

	submitFeedbackErr: 'Cannot send the request. [{0}]',
	submitFeedbackSuccess: 'Feedback submitted.',
	requestReviewSuccess: 'Request submitted.',

	flagged: 'Flagged',
	notReviewed: 'Never',
	flaggedForReview: 'Flag submitted',
	notFlaggedForReview: 'Flag for review',
	flagForReviewTitle: 'Confirm Flag Document',
	flagForReviewBody: 'Are you sure you want to flag this document for review?',
	flagForReviewConfirm: 'Flag',
	flagSuccess: 'Flagged for Review',
	flagSuccessMessage: 'This document has been flagged for review. {0}Click to Undo{1}',
	flagFailure: 'Error flagging document for review',
	requestReview: 'Request Review',
	reviewSaved: 'Document marked for review',

	feedbackResults: 'Rating',
	deleteRating: 'Delete',
	total: 'Total',
	vote: 'Vote',
	votes: 'Votes',
	star: 'Star',
	stars: 'Stars',
	rating: 'Rating',
	date: 'Date',
	comments: 'Comments',

	attachments: 'Attachments',
	fileName: 'File Name',
	fileSize: 'Size',
	uploadedBy: 'Uploaded By',
	uploadedOn: 'Uploaded On',

	history: 'Revision', // 'Version', TODO - support Version Control
	documentName: 'Document Name',
	revisionNumber: 'Revision #', // 'Version #', TODO - support Version Control
	stableVersion: 'Committed',
	referenced: 'Referenced',
	type: 'Type',
	comment: 'Comment',
	createdBy: 'Created By',
	createdOn: 'Created On',
	sysUpdatedOn: 'Updated On',
	sysUpdatedBy: 'Updated By',

	tags: 'Tags',
	pageInfo: 'Page Info',
	tagName: 'Name',

	invalidUsefulTitle: 'Was this document useful?',
	invalidUsefulBody: 'Please indicate whether this document was useful before submitting your feedback.',
	invalidRatingTitle: 'No Rating',
	invalidRatingBody: 'Please provide a star rating for this document before submitting your feedback.',

	download: 'Download',
	//End Infobar

	//Results
	taskSummary: 'Description',
	result: 'Result',
		//End Results

	// Rating
	close: 'Close',

	activity: 'Activity',

	TextEditor: {
		lastComment: 'Last Updated: {0} at {1}',
		notes: 'Notes',
		editNotes: 'Edit Notes',
		edit: 'Edit',
		update: 'Update',
		cancel: 'Cancel',
		cancelNotes: 'Cancel Edit',
	}
}
Ext.namespace('RS.wiki.pagebuilder').locale = {

	overwrite_tooltip: 'Overwrite the existing wiki document if it exists',
	overwrite_tooltip_attachment: 'Overwrite the existing attachment if it exists',
	skip_tooltip: 'Skip the copy if the existing wiki document already exists',
	skip_tooltip_attachment: 'Skip the copy if the existing attachment already exists',
	update_tooltip: 'Update the existing wiki document if it already exists, otherwise create it',
	invalidAttachmentName: 'Attachment name should not start with space.',

	Image: {
		imageConfiguration: 'Image',
		src: 'Source',
		zoom: 'Expand image on click',
		scale: 'Scale',
		height: 'Height',
		width: 'Width',
		mode: 'Units',
		pixels: 'Pixels',
		percentage: 'Percentage'
	},

	ImageManager: {
		imageManagerTitle: 'Image Picker',
		select: 'Select'
	},

	Progress: {
		progressConfiguration: 'Progress',
		text: 'Image',
		height: 'Height',
		width: 'Width',
		displayText: 'Loading Text',
		completedText: 'Completed Text',
		border: 'Border',
		borderColor: 'Border Color',
		defaultLoadingImage: 'Default Loading Image'
	},

	Secure: {
		encryptConfiguration: 'Secure',
		key: 'Password',
		value: 'Display Value'
	},

	Infobar: {
		infobarConfiguration: 'Infobar',
		social: 'Social',
		rating: 'Rating',
		feedback: 'Feedback',
		attachments: 'Attachments',
		history: 'Version',
		tags: 'Tags',
		height: 'Height'
	},

	Model: {
		modelConfiguration: 'Model',
		wiki: 'Wiki',
		refreshInterval: 'Refresh Interval',
		refreshCountMax: 'Refresh Attempts',
		status: 'Status',
		condition: 'Condition',
		severity: 'Severity',
		zoom: 'Zoom',
		height: 'Height',
		width: 'Width'
	},

	Form: {
		formConfiguration: 'Form',
		name: 'Name',
		title: 'Title',
		tooltip: 'Tooltip',
		popout: 'Popout',
		popoutHeight: 'Popout Height',
		popoutWidth: 'Popout Width',
		collapsed: 'Collapsed'
	},

	Page: {
		wikiDocConfiguration: 'Page',
		wiki: 'Document Name',
		createWiki: 'New Document',
		selectWiki: 'Select a Document'
	},

	TextEditor: {
		wysiwygConfiguration: 'Text Editor'
	},

	ResolveMarkup: {
		sourceConfiguration: 'Resolve Markup'
	},

	CodeSample: {
		codeConfiguration: 'Code',
		type: 'Type',
		showLineNumbers: 'Show line numbers',
		text: 'Text',
		javascript: 'JavaScript',
		html: 'HTML',
		groovy: 'Groovy',
		sql: 'SQL',
		xml: 'XML',
		java: 'Java',
		css: 'CSS'
	},

	Result: {
		resultConfiguration: 'Result',
		addActionTask: 'Add Task',
		addGroup: 'Add Group',
		remove: 'Remove',
		filter: 'Filter',
		filterValue: 'Severity Filter',
		title: 'Title',
		preserveTaskOrder: 'Preserve Task Order',
		order: 'Order',
		refreshInterval: 'Refresh Interval',
		refreshCountMax: 'Refresh Attempts',
		descriptionWidth: 'Description Width',
		autoHide: 'Auto Hide',
		actionTasks: 'Action Tasks',
		actionTaskFilters: 'Action Task Filters',
		encodeSummary: 'Encode Summary',
		GOOD: 'GOOD',
		WARNING: 'WARNING',
		SEVERE: 'SEVERE',
		CRITICAL: 'CRITICAL',

		ASC: 'Ascending',
		DESC: 'Descending',

		newTask: 'New Task',
		newGroup: 'New Group',

		tags: 'Tags',
		addTag: 'Add Tag',
		removeTag: 'Remove',
		edit: 'Edit',

		progress: 'Progress',

		showAllActionTasks: 'Show All Action Tasks in Current Worksheet',
		includeStartEndTasks: 'Include Hidden Action Tasks',

		showResultsFrom: 'Show Results From',
		showAnyWiki: 'Any Automation',
		showCurrentWikiOnly: 'Current Automation Only',
		showCustomWikiOnly: 'Only this Automation',
	},

	TaskEdit: {
		edit: 'Edit',
		apply: 'Apply',
		cancel: 'Cancel',
		add: 'Add',
		descriptionWikiLink: 'Description Link',
		namespace: 'Task Namespace',
		task: 'Task Name',
		remove: 'Remove',
		nodeId: 'Source Node Id',
		wiki: 'Source Runbook'
	},

	Detail: {
		detailConfiguration: 'Detail',
		addActionTask: 'Add Task',
		refreshInterval: 'Refresh Interval',
		refreshCountMax: 'Refresh Attempts',
		remove: 'Remove',
		actionTasks: 'Action Tasks',
		maximum: 'Max',
		offset: 'Offset',
		newTask: 'New Task',
		progress: 'Progress'
	},

	Automation: {
		getABTaskError: 'Cannot get ActionTasks from the server [{0}].',
		automationConfiguration: 'Automation',
		name: 'Automation',
		displayName: 'Name',
		title: 'Title',
		tooltip: 'Tooltip',
		collapsed: 'Collapsed',
		popout: 'Popout',
		popoutHeight: 'Popout Height',
		popoutWidth: 'Popout Width'
	},

	ActionTask: {
		actionTaskConfiguration: 'Action Task',
		name: 'Name',
		displayName: 'Name',
		title: 'Title',
		tooltip: 'Tooltip',
		collapsed: 'Collapsed',
		popout: 'Popout',
		popoutHeight: 'Popout Height',
		popoutWidth: 'Popout Width',
		selectActionTaskForm: 'Select an Action Task Form'
	},

	Table: {
		tableConfiguration: 'Table',

		addColumn: 'Add Column',
		removeColumn: 'Remove Column',
		addRow: 'Add Row',
		insertRow: 'Insert Row',
		removeRow: 'Remove Row'
	},

	ColumnPicker: {
		removeColumnTitle: 'Remove Column',
		remove: 'Remove',
		cancel: 'Cancel',
		column: 'Column'
	},

	XColumnPicker: {
		removeColumnTitle: 'Remove Column',
		remove: 'Remove',
		cancel: 'Cancel',
		column: 'Column'
	},

	ColumnAdder: {
		invalidColumnName: 'Column name should not be empty and should only contain alphabets,_ and digits',
		addColumnTitle: 'Add Column',
		column: 'Column',
		add: 'Add',
		cancel: 'Cancel'
	},

	XTable: {
		xtableConfiguration: 'XTable',

		addColumn: 'Add Column',
		editColumn: 'Edit Column',
		removeColumn: 'Remove Column',
		addRow: 'Add Row',
		insertRow: 'Insert Row',
		removeRow: 'Remove Row',

		sql: 'SQL',
		drilldown: 'Drilldown'
	},

	XColumnManager: {
		addColumn: 'Add Column',
		editColumn: 'Edit Column',

		column: 'Select Column',

		text: 'Display Name',
		invalidText: 'Display name is required.',
		type: 'Type',
		dataIndex: 'Column Name',
		format: 'Format',
		dataFormat: 'Data Format',
		width: 'Width',
		sortable: 'Sortable',
		draggable: 'Draggable',
		link: 'Link',
		linkTarget: 'Target',

		add: 'Add',
		apply: 'Apply',
		cancel: 'Cancel',

		stringType: 'String',
		intType: 'Int',
		floatType: 'Float',
		dateType: 'Date'
	},

	CollaborationLink: {
		socialLinkConfiguration: 'Collaboration Link',
		displayText: 'Display Text',
		preceedingText: 'Proceeding Text',
		to: 'To',
		openInNewTab: 'Open in new tab'
	},

	Groovy: {
		groovyConfiguration: 'Groovy'
	},
	Javascript: {
		javascriptConfiguration: 'Javascript'
	},
	HTML: {
		htmlConfiguration: 'HTML'
	},

	Source : {
		editSourceWarning : 'Editing source directly could lead to unexpected results.'
	},
	actionTask: 'Action Task',
	task: 'Task',
	namespace: 'Namespace',
	description: 'Description',
	wiki: 'Wiki',
	nodeId: 'Node Id',
	descriptionWikiLink: 'Description Wiki Link',
	resultWikiLink: 'Results Wiki Link',
	autoCollapse: 'Auto Collapse',

	active : 'Active',
	hidden : 'Hidden',
	addSection : 'Add Section',
	editFileOptions: 'File',
	viewFileOptions: 'File',
	addNewSection: 'Add New Section',
	saveGeneral: 'Save',
	saveAs: 'Save As',
	saveAndCommit: 'Commit',
	untitledSection: 'Untitled Section',
	windowTitle: 'Builder',
	serverError: 'Server Error: {0}',
	remove: 'Remove',
	cancel: 'Cancel',
	update: 'Update',
	help: 'Help',
	sectionTitle: 'Title',
	sectionHeight: 'Height (px)',
	sectionWidth: 'Width',
	columns : 'Columns',
	sectionProperties : 'Properties',
	sectionPropertiesTitle : 'Section Properties',
	columnWidth : 'Column Width (%)',
	col1Width: 'col1',
	col2Width: 'col2',
	col3Width: 'col3',
	col4Width: 'col4',
	col5Width: 'col5',
	col6Width: 'col6',
	col7Width: 'col7',
	alignments : 'Alignments',
	col1Alignment: 'col1',
	col2Alignment: 'col2',
	col3Alignment: 'col3',
	col4Alignment: 'col4',
	col5Alignment: 'col5',
	col6Alignment: 'col6',
	col7Alignment: 'col7',
	invalidWidths: 'Total width must be less than 100%',

	// Component names
	new: 'New Component',
	image: 'Image',
	result: 'Result',
	model: 'Model',
	form: 'Form',
	wysiwyg: 'Text Editor',
	groovy: 'Groovy Script',
	javascript: 'JavaScript',
	html: 'HTML',
	source: 'Resolve Markup',
	actiontask: 'Action Task',
	runbook: 'Runbook',
	infobar: 'InfoBar',
	encrypt: 'Secure',
	table: 'Table',
	xtable: 'XTable',
	code: 'Code Sample',
	wikidoc: 'Page',
	detail: 'Detail',
	progress: 'Progress',
	sociallink: 'Collaboration Link',
	automation: 'Automation',

	//Position
	moveLeft : 'Move component left',
	moveRight : 'Move component right',
	moveSectionUp : 'Move section up',
	moveSectionDown : 'Move section down',

	addComponent: 'Add',
	addNewComponent: 'Add New Component',
	chooseComponent: 'Choose Component',
	filter: 'Filter By',
	filterEmpty: "Component's name...",

	//delete confirmation
	confirmRemoveSectionTitle: 'Confirm Section Removal',
	confirmRemoveSectionDesc1: 'There is 1 configured component in this section. Removing the section will also remove the component. Are you sure you want to remove this section?',
	confirmRemoveSectionDescX: 'There is {0} configured components in this section. Removing the section will remove all the components. Are you sure you want to remove this section?',

	confirmRemoveComponentTitle: 'Confirm Component Removal',
	confirmRemoveComponentBody: 'Are you sure you want to remove this component?',

	// SectionEntry
	SectionEntry: {
		properties: 'Properties',
		deleteSection: 'Delete section',
	},

	ComponentEntry: {
		edit: 'Edit',
		deleteComponent: 'Delete component',
	},

	// Properties popup
	displayOptions: 'Display Options',
	utitle: 'Title',
	ufullname: 'Name',
	fullName: 'Name',
	usummary: 'Summary',
	wiki: 'Wiki',
	decisionTree: 'Decision Tree',
	catalog: 'Catalog',
	roles: 'Roles',
	tags: 'Tags',
	catalogs: 'Catalogs',
	addRole: 'Add',
	removeRole: 'Remove',
	addTag: 'Add',
	removeTag: 'Remove',
	addCatalog: 'Add',
	removeCatalog: 'Remove',
	name: 'Name',
	view: 'View',
	edit: 'Edit',
	execute: 'Execute',
	admin: 'Admin',
	viewRight: 'View',
	editRight: 'Edit',
	executeRight: 'Execute',
	adminRight: 'Admin',
	useDefaultRights: 'Use Default Rights',

	//Attachments
	renameAttachment: 'Rename',
	attachments : 'Attachments',
	filename : 'Filename',
	overwrite : 'Overwrite',
	skip : 'Skip',
	confirmAction : 'OK',
	close : 'Close',
	upload: 'Upload',
	reload: 'Reload',
	delete: 'Delete',
	referGlobalAttachments: 'Reference Global',
	queuedStatus: 'Queued',
	errorStatus: 'Error',
	completedStatus: 'Completed',
	processingStatus: 'Processing...',
	uploadingStatus: 'Uploading...',
	renameAttachmentTooltip: 'Rename the selected attachment.',
	referGlobalAttachmentsTooltip: 'Make a reference to global attachments.',

	// Version
	viewRevisionList: 'Revision', // 'View Version List', TODO - support Version Control
	revisions: 'Revision', // 'Version List', TODO - support Version Control
	commitRevision: 'Commit',
	viewRevision: 'View',
	compareRevision: 'Compare',
	rollbackRevision: 'Rollback',
	selectRevision: 'Use Selected',
	resetRevision: 'Reset',
	documentName: 'Document Name',
	revisionNumber: 'Revision #', //'Version #', TODO - support Version Control
	stableVersion: 'Committed',
	referenced: 'Referenced',
	type: 'Type',
	comment: 'Comment',
	createdBy: 'Created By',
	createdOn: 'Created On',
	sysUpdatedOn: 'Updated On',
	sysUpdatedBy: 'Updated By',
	rollbackRevisionMsg: 'Rollback resets to a previous revision and remove all later revisions. Do you want to roll back?', // 'Rollback loads and saves the version you have selected as latest.Do you want to rollback?', TODO - support Version Control
	rollbackSuccess: 'Successfully rollback to the selected version.',
	resetRevisionMsg: 'Resetting keeps the latest version and removes all previous versions. Are you sure you want to reset?',
	resetSuccess: 'Version reset.',
	resetError: 'Version error: {0}',
	close: 'Close',
	refresh: 'Refresh',
	firstPage: 'First Page',
	lastPage: 'Last Page',
	previousPage: 'Previous Page',
	nextPage: 'Next Page',

	unknownLegacyType: 'Unable to parse unknown type {0}',
	typeNotSupported: 'Type is not supported at this time',

	MoveOrRename: {
		RenameTitle: 'Move / Rename',
		renameAttachement: 'Rename',
		CopyTitle: 'Copy',
		namespace: 'Namespace',
		invalidNamespace: 'Namespace is required.',
		filename: 'Filename',
		overwrite: 'Overwrite',
		overwrite_tooltip: 'Overwrite the existing wiki document if it exists',
		skip: 'Skip',
		skip_tooltip: 'Skip the copy if the existing wiki document already exists',
		update: 'Update',
		update_tooltip: 'Update the existing wiki document if it already exists, otherwise create it',
		confirmAction: 'OK',
		ListNamespaceErrMsg: 'Cannot get namespaces from the server.',
		invalidFilename: 'Wiki name should not start with space.',
		renameAttError: 'Cannot rename the attachment [{msg}].',
		renameAttSuccess: 'The selected attachment has been renamed.'
	}
}
Ext.namespace('RS.wiki.resolutionbuilder').locale = {
	group: 'Group',
	name: 'Name',
	uname: 'Name',
	module: 'Module',
	namespace: 'Namespace',
	unamespace: 'Namespace',
	description: 'Description',
	summary: 'Summary',
	type: 'Type',
	displayName: 'Display Name',
	defaultValue: 'Default Value',
	color: 'Color',
	flow: 'FLOW',
	wsdata: 'WSDATA',
	output : 'OUTPUT',
	valueSource: 'Source',
	hidden: 'Hidden',
	value: 'Value',
	source: 'Source',
	sourceName: 'Value',
	comparison: 'Comparison',
	variableSource: 'Variable Source',
	variable: 'Variable',
	columnNumber: 'Column',
	serverError: 'Server Error: {0}',
	usummary: 'Summary',
	cancel: 'Cancel',
	uisDeleted: 'Deleted',
	uhasResolutionBuilder: 'Automation Builder',
	uhasActiveModel: 'Automation',
	eq: '=',
	gt: '>',
	gte: '>=',
	lt: '<',
	lte: '<=',
	neq: '!=',
	contains: 'contains',
	notContains: 'not contains',
	startsWith: 'starts with',
	endsWith: 'ends with',
	Automations: {
		windowTitle: 'Automations',
		displayName: 'Automations',
		createAutomation: 'New',
		deleteAutomations: 'Delete',
		purgeAutomations: 'Purge',
		renameAutomations: 'Rename/Move',
		copyAutomations: 'Copy',
		listAutomationError: 'Cannot get the list of Runbooks from the server. [{0}]',

		renameMsg: 'Are you sure you want to rename the selected automation? Selected automations:{names}',
		renamesMsg: 'Are you sure you want to rename the selected automations? Selected automations({num} selected):{names}',
		renameSucMsg: 'The selected records have been renamed.',
		renameErrMsg: 'Cannot rename the selected automations.',

		copyMsg: 'Are you sure you want to copy the selected automation? Selected automations:{names}',
		copysMsg: 'Are you sure you want to copy the selected automations? Selected automations({num} selected):{names}',
		copySucMsg: 'The selected records have been copied.',
		copyErrMsg: 'Cannot copy the selected automations.',

		deleteMsg: 'Are you sure you want to delete the selected automation? Selected automations:{names}',
		deletesMsg: 'Are you sure you want to delete the selected automations? Selected automations({num} selected):{names}',
		deleteSucMsg: 'The selected records have been deleted.',
		deleteErrMsg: 'Cannot delete the selected automations.',

		undeleteMsg: 'Are you sure you want to undelete the selected automation? Selected automations:{names}',
		undeletesMsg: 'Are you sure you want to undelete the selected automations? Selected automations({num} selected):{names}',
		undeleteSucMsg: 'The selected records have been undeleted.',
		undeleteErrMsg: 'Cannot udelete the selected automations.',

		purgeMsg: 'Are you sure you want to purge the selected automation? Selected automations:{names}',
		purgesMsg: 'Are you sure you want to purge the selected automations? Selected automations({num} selected):{names}',
		purgeSucMsg: 'The selected records have been purged.',
		purgeErrMsg: 'Cannot purge the selected automations.'
	},

	AutomationMeta: {
		automationBuilder: 'Automation Builder',
		quitWithoutSavingChange: 'Quit without saving change',
		confirmQuitWithoutChange: 'Are you sure you want to discard the changes made to this AutomationMeta?',
		windowTitle: 'AutomationMeta',
		saveGeneral: 'Save',
		saveCurrentTask: 'Save Current Task',
		rename: 'Move/Rename',
		generate: 'Generate',
		back: 'Back',
		basicMode: 'Basic',
		advancedMode: 'Advanced',
		generalTab: 'General',
		parameterTab: 'Input',
		tasksTab: 'Tasks',
		saveGeneralAndTaskSuccess: 'Successfully saved the general information and the current task.',
		saveGeneralError: 'Cannot save the general information: {0}',
		generateRunbook: 'Generate Automation',
		confirmGenerateRunbook: 'Are you sure you want to create the runbook {namespace}.{name}?',
		cancel: 'Cancel',
		generateReqError: 'Server cannot process the automation generation request. {0}',
		generateReqSuccess: 'Generating automation...',
		executionTooltip: 'Execute the automation.',
		generateAutomation: 'Generate Automation',
		pollABGenerationError: 'Error tracking progress of the automation generation. {0}',
		abGenerationError: 'Error occured during the automation generation. {0}',
		abGenerationSuccess: 'Automation has been generated.',
		no: 'No'
	},

	StageIndicator: {
		close: 'Close'
	},

	ResolutionBuilderRunbookNamer: {
		newRunbook: 'New Runbook',
		create: 'OK',
		cancel: 'Cancel',
		invalidNamespace: 'Namespace is required and should only contain alphabets, numbers, \'-\' and \'_\'',
		invalidName: 'Name is required and should only contain alphabets, numbers, \'-\' and \'_\''
	},

	General: {
		tags: 'Tags',
		addTag: 'Add',
		removeTags: 'Remove',
		addCatalog: 'Add',
		removeCatalogs: 'Remove',
		catalogs: 'Catalogs',
		createTag: 'New'
	},

	RunbookGenerator: {
		generateRunbook: 'Generate Automation',
		generate: 'Generate Automation',
		cancel: 'Cancel'
	},

	Parameter: {
		layout: 'Column Layout',
		oneColumn: 'One',
		twoColumn: 'Two',
		addParam1stGrid: 'Add',
		addParam2ndGrid: 'Add',
		removeParams1stGrid: 'Remove',
		removeParams2ndGrid: 'Remove',
		parameter: 'Parameter',
		worksheet: 'Worksheet',
		newWorksheet: 'New',
		invalidParam: 'Name is required and should be unique, it can only contain letters without spaces.',
		parameterReadOnlyTxt: '(Read Only. Full control shown in edit mode.)',
		//parser
		currentWorksheet: 'Current'
	},

	ParamWindow: {
		parameter: 'Parameters',
		save: 'Save',
		cancel: 'Cancel'
	},

	ParameterEditor: {
		parameter: 'Parameter',
		update: 'Update',
		cancel: 'Cancel',
		dataType: 'Type',
		fieldTip: 'Tooltip'
	},

	Tasks: {
		//AT grid
		addConnection: 'Add CONNECTION',
		addLoop: 'Add LOOP',
		addTask: 'Add TASK',
		addCase: 'Add IF',
		addParam: 'Add PARAMS',
		parameter: 'Parameters'
	},
	TaskEditor: {
		//AT General
		general: 'Step 1 - Command',
		command: 'Command',
		insertCommandArgument: 'Add Variables',
		options: 'Options',


		//AT output
		output: 'Step 4 - Output (optional)',
		addSummaryArgs: 'Add Variables',

		detail: 'Detail',
		inputfile: 'INPUTFILE',
		addDetailArgs: 'Add Variables',
		queueName: 'Queue Name',
		promptSourceName: 'Prompt',
		timeout: 'Task Timeout (sec)',
		expectTimeout: 'Expect Timeout (sec)'
	},
	INPUTFILEEditor: {
		inputfile: 'INPUTFILE',
		update: 'Update',
		cancel: 'Cancel'
	},
	RunbookEditor: {
		runbook: 'Runbook',
		parameters: 'Parameters',
		addParam: 'Add',
		removeParams: 'Remove',
		nonConstantEmptyValue: 'The runbook param {0} is not a constant thus should not be empty. Automatically change its source to CONSTANT.',
		invalidRunbook: 'This runbook does not exist.'
	},
	ArgumentPicker: {
		argument: 'Argument',
		select: 'Select',
		cancel: 'Cancel'
	},
	saveEntityError: 'Cannot save entity [{0}]',
	TaskEntry: {
		taskEntry: 'TASK',
		overrideCode: 'Replace',
		parseCodeError: 'Cannot generate code: {0}',
		confirmOverrideCode: 'Do you want the system generated the code and override the current one?',
		donotOverrideCode: 'No'
	},
	RunbookEntry: {
		runbookEntry: 'RUNBOOK'
	},
	IfEntry: {
		ifEntry: 'IF',
		addCondition: 'Add CONDITION'
	},
	ConditionEntry: {
		conditionEntry: 'CONDITION',
		addTask: 'Add TASK',
		addRunbook: 'Add RUNBOOK'
	},
	ConditionEntryConfig: {
		conditionEntryConfig: 'CONDITION',
		add: 'Add',
		remove: 'Remove',
		update: 'Update',
		cancel: 'Cancel'
	},
	ConditionEntryRule: {
		condition: 'Condition'
	},
	ConnectionEntry: {
		connectionEntry: 'CONNECTION {0}',
		addTask: 'Add TASK',
		addRunbook: 'Add RUNBOOK',
		addIf: 'Add IF',
		ssh: 'SSH',
		telnet: 'TELNET',
		http: 'HTTP/S',
		local: 'OS',
		database: 'DATABASE',
		http: 'HTTP'
	},
	ConnectionEntryConfig: {
		connectionEntryConfig: 'CONNECTION',
		update: 'Update',
		cancel: 'Cancel',
		ssh: 'SSH',
		database: 'DATABASE',
		http: 'HTTP/S',
		telnet: 'TELNET',
		local: 'LOCAL',
		configTab: 'Configuration',
		optionTab: 'Options'
	},
	SSHConnectionConfig: {
		host: 'Host',
		port: 'Port',
		u_sername: 'Username',
		authType: 'Auth Type',
		p_assword: 'Password',
		usernameAndPassword: 'Username and Password',
		keyFileNameAndPassphrase: 'Key Filename and Passphrase',
		keyfile: 'Key',
		passphrase: 'Passphrase'
	},
	SSHConnectionConfigOption: {
		prompt: 'Prompt',
		timeout: 'Timeout (sec)',
		queueName: 'Queue Name'
	},
	TelnetConnectionConfig: {
		host: 'Host',
		port: 'Port',
		u_sername: 'Username',
		p_assword: 'Password'
	},
	TelnetConnectionConfigOption: {
		prompt: 'Prompt',
		timeout: 'Timeout (sec)',
		loginPrompt: 'Login Prompt',
		passwordPrompt: 'Password Prompt',
		queueName: 'Queue Name'
	},
	LocalConnectionConfigOption: {
		queueName: 'Queue Name'
	},
	LoopEntry: {
		loopEntry: 'LOOP',
		addConnection: 'Add CONNECTION'
	},
	LoopEntryConfig: {
		loopEntryConfig: 'Loop Configuration',
		update: 'Update',
		cancel: 'Cancel',
		fixed: 'Fixed',
		list: 'List',
		comma: 'COMMA',
		space: 'SPACE',
		count: 'Count'
	},
	ConditionRule: {
		result: 'Result',
		'if': 'if'
	},
	ConditionRuleGroup: {
		qtip: 'Click the vertical colored bar to change the condition result.'
	},
	SeverityRule: {
		severity: 'Severity',
		'if': 'if'
	},
	SeverityRuleGroup: {
		qtip: 'Click the vertical colored bar to change the severity result.'
	},
	AssessConditionRules: {
		conditionRules: 'Condition Rules',
		result: 'Result',
		add: 'Add',
		remove: 'Remove',
		goodDefaultCondition: 'GOOD',
		badDefaultCondition: 'BAD',
		continuation: 'Continue',
		alwaysContinue: 'Always',
		continueOnGood: 'On Good',
		continueOnBad: 'On Bad',
		stopOnBad: 'Stop / Return on Bad'
	},
	AssessSeverityRules: {
		add: 'Add',
		remove: 'Remove',
		severityRules: 'Severity Rules',
		goodDefaultSeverity: 'Good',
		warningDefaultSeverity: 'Warning',
		severeDefaultSeverity: 'Severe',
		criticalDefaultSeverity: 'Critical'
	},
	Assessor: {
		warning: 'Warning',
		overrideCustomizedCode: 'This will override your current assess code. Proceed?',
		override: 'Override',
		assessorAutoGenEnabled: 'UI Auto Generated',
		assess: 'Step 3 - Assess (optional)',
		generateAssessCode: 'Generate Code',
		assessDefinition: 'Definition',
		assessCode: 'Code',
		assessorCode: 'Assessor Code',
		readOnly: 'Turn off auto generation',
		turnOffAutoGenMsg: 'Please uncheck the UI Auto Generated checkbox to customize your code.'
	},
	columnNum: 'Column Number',
	//type
	ignoreRemainder: 'Any(greedy)',
	useCurrentTypeMarkups: 'Existing Markups',
	ignoreNotGreedy: 'Any',
	spaceOrTab: 'Space or Tab',
	whitespace: 'Whitespace',
	notWhitespace: 'Not Whitespace',
	letters: 'Letters',
	lettersAndNumbers: 'Letters and Numbers',
	notLettersOrNumbers: 'Not Letters or Numbers',
	lowerCase: 'Lower Case',
	upperCase: 'Upper Case',
	number: 'Number',
	notNumber: 'Not Number',
	beginOfLine: 'Begin of Line',
	endOfLine: 'End of Line',
	table: 'Table',
	specialType: 'Special Type',
	IPv4: 'IPv4',
	IPv6: 'IPv6',
	MAC: 'MAC',
	decimal: 'Decimal',

	parserAutoGenEnabled: 'UI Auto Generated',
	readOnly: 'Turn off auto generation',
	turnOffAutoGenMsg: 'Please uncheck the UI Auto Generated checkbox to customize your code.',

	testResult: 'Test Result',
	variables: 'Variables',
	removeVariables: 'Remove',
	addPlaceHolder: 'Add Variable',
	reset: 'Reset',
	clearMarkup: 'Clear Markup',
	clearSample: 'Clear Sample',
	templateTab: 'Definition',
	codeTab: 'Code',
	testTab: 'Test',
	testData: 'Test Data',
	executeTest: 'Execute Test',
	warning: 'Warning',
	overrideCustomizedCode: 'This will override your current parser code. Proceed?',
	override: 'Override',
	executeTestInProgress: 'Executing test code...',
	markBoundaryErrMsg: 'Please select the text that you want to mark as boundary.',
	noSelectionError: 'Please select content to capture',
	noContentSelected: 'No Content Selected',
	sampleTooLong: 'The maximum length of a sample is 25000',
	PlainTextManualMarkupParser: {
		literal: 'Literal',
		capture: 'Capture',
		content: 'Boundary',
		options: 'Options',
		beginParse: 'Begin Parse',
		endParse: 'End Parse',
		parseFromEndOfLine: 'Begin Parse from End of Line',
		stopParseAtStartOfLine: 'End Parse from Begin of Line',
		repeat: 'Block'
	},
	PlainTextTableMarkupParser: {
		delimiter: 'Delimiter',
		capture: 'Capture',
		content: 'Boundary',
		options: 'Options',
		tabWidth: 'Tab Width',
		trim: 'Trim',
		addTblVar: 'Add',
		removeTblVars: 'Remove',
		beginParse: 'Begin Parse',
		endParse: 'End Parse',
		parseFromEndOfLine: 'Begin Parse from End of Line',
		stopParseAtStartOfLine: 'End Parse from Begin of Line',
		applyTblVar: 'Apply'
	},
	XmlParser: {
		xpath: 'XPath',
		editXPath: 'Edit',
		addXPath: 'Add',
		updateXPath: 'Update',
		invalidXPath: 'Invalid XPath.',
		invalidXmlSyntaxError: 'The format of the sample XML is invalid.',
		multipleXmlSegError: 'Only one xml is allowed in the output.',
		noXmlSegFoundError: 'No XML is found in the sample.',
		saveXPathChange: 'XPath Changed',
		confirmSaveXPathChange: 'Do you want to save the change of the current xpath variable?',
		yes: 'Yes',
		no: 'No'
	},
	XPathVarNamer: {
		xpathVarName: 'Variable Name',
		xpathVarNameMsg: 'Please assign a variable name.<br/>',
		dump: 'Add'
	},
	XPathAttrPredicate: {
		invalidName: 'The attribute name shouldn\'t be empty.'
	},
	XPathCustomPredicate: {
		invalidPreidcate: 'The predicate shouldn\'t be empty.'
	},
	attribute: 'Attribute',
	customized: 'Customized',
	text: 'Text',
	XPathTokenEditor: {
		predicates: 'Predicates',
		and: 'And',
		or: 'Or',
		addTextPredicate: 'Text',
		addAttrPredicate: 'Attribute',
		addCustPredicate: 'Custom',
		update: 'Update',
	},
	Parsers: {
		parser: 'Step 2 - Parse (optional)',
		//AT Variables
		addVariable: 'Add',
		generateCode: 'Generate Code',
		parseCodeError: 'Cannot generate code: {0}',
		text: 'Text',
		table: 'Table',
		map: 'Map',
		xml: 'XML',
		json: 'JSON',
		parseBegin: 'Parse Begin',
		parseEnd: 'Parse End',
		repeat: 'Repeat',


		fixedLength: 'Fixed Length',
		columnSeparator: 'Column Separator',
		comma: 'COMMA',
		space: 'SPACE',
		tab: 'TAB',
		rowSeparator: 'Row Separator',
		lf: 'Unix(LF)',
		crlf: 'Win(CR\\LF)',
		cr: 'CR',
		applySeparator: 'Apply',
		addSpace: 'SPACE',
		addTab: 'TAB',
		addComma: ',',
		parserCode: 'Parser Code',
		//content
		beginParseFromEndOfLine: 'Begin From End of Line',
		provideVariableName: 'Please name a variable for this markup.',
		assign: 'Assign',
		cancel: 'Cancel',
		testCode: 'Test Parser',
		pollTestError: 'Cannot tracking testing progress. {0}'
	},
	action: 'Markup',
	content: 'Content',
	Capture: {
		assignVariable: 'Assign Variable',
		invalidName: 'Variable name is required.',
		capture: 'Capture'
	},
	SimpleCapture: {
		assignVariable: 'Assign Variable',
		invalidName: 'Variable name is required.',
		capture: 'Capture'
	},
	detail: 'Detail',
	ParserTip: {
		markups: 'Markups',
		remove: 'Remove'
	},
	HTTPCommand: {
		general: 'Command',
		method: 'Method',
		commandUrl: 'URL',
		addParam: 'Add Parameter',
		addBodyParam : 'Add Parameter',
		headers: 'Headers',
		addHeader: 'Add Header',
		body: 'Body',
		post: 'POST',
		get: 'GET',
		syntax : 'Syntax',
		text : 'Text (text/plain)',
		json : 'JSON (application/json)',
		xml : 'XML (application/xml)',
		invalidURL : 'This URL is invalid.<br> Please check the character at position {0}'
	},
	HTTPConnectionConfig: {
		protocol: 'Protocol',
		host: 'Host',
		port: 'Port',
		http: 'HTTP',
		https: 'HTTPS'
	},
	invalidHeaderName : 'This header already exists'
}
Ext.namespace('RS.wiki.automation').locale = {
	editSourceWarning : 'Editing source directly could lead to unexpected results.',
	add : 'Add',
	insert : 'Insert',
	start: 'Start',
	end: 'End',
	'event': 'Event',
	text: 'Text',
	container: 'Container',
	runbook: 'Automation',
	'document': 'Document',
	task: 'Task',
	precondition: 'Precondition',
	straight: 'Connector(straight)',
	horizontal: 'Connector(horizontal)',
	vertical: 'Connector(vertical)',
	root: 'Root',
	question: 'Question',
	elements: 'Elements',
	images: 'Images',
	elementTab: 'Elements',
	imageTab: 'Images',
	dependency: 'Dependency',
	cancel: 'Cancel',
	confirm: 'Confirm',
	editInputs: 'Edit Inputs',
	editProperties: 'Edit Properties',
	sshConnection : 'SSH',
	telnetConnection : 'TELNET',
	httpConnection : 'HTTP/HTTPS',
	merge: 'Merge',
	all: 'All',
	any: 'Any',
	targets: 'Targets',

	warning: 'Warning',
	automationModelDependency: 'Modifying this element may cause your automation to execute improperly.<br/></br>Do you want to continue?',
	automationModelDependencyModifyTask: 'OUTPUT parameters on this Action Task are being used as INPUT parameters in another Action Task. {0} this Action Task may cause your automation to execute improperly.<br/></br>Do you want to continue?',
	automationModelDependencyDeleteEdge: 'OUTPUT parameters from this edge\'s source Action Task are being used as INPUT parameters in another actionTask. Deleting this edge may cause your automation to execute improperly.<br/></br>Do you want to continue?',
	automationModelDependencyInsert: 'OUTPUT parameters from a source Action Task are being used as INPUT parameters in a target Action Task on this connector. Inserting an element into this conection may cause your automation to execute improperly.<br/></br>Do you want to continue?',
	automationModelDependencyDeleteParam: 'This OUTPUT parameter is being used as an INPUT parameter in another Action Task. Deleting this parameter may cause your automation to execute improperly.<br/></br>Do you want to continue?',
	deleting: 'Deleting',
	renaming: 'Renaming',

	SaveErr: 'Cannot save ActionTask.',
	ServerError: 'Server Error',

	apache: 'Apache',
	email: 'Email',
	workplace: 'Workplace',
	bell: 'Bell',
	box: 'Box',
	cube: 'Cube',
	gear: 'Gear',
	'package': 'Package',
	user: 'User',
	wrench: 'Wrench',
	linux: 'Linux',
	server: 'Server',
	earth: 'Earth',

	printPreview: 'Print Preview',
	saveAsPng: 'Save as PNG',
	saveAsJpg: 'Save as JPEG',
	saveAsGif: 'Save as GIF',
	select: 'Select',
	pan: 'Pan',
	cut: 'Cut',
	copy: 'Copy',
	paste: 'Paste',
	remove: 'Remove',
	undo: 'Undo',
	redo: 'Redo',
	resume: 'Restore',
	zoomIn: 'Zoom In',
	zoomOut: 'Zoom Out',
	order: 'Order',
	left: 'Left',
	right: 'Right',
	center: 'Center',
	middle: 'Middle',
	top: 'Top',
	bottom: 'Bottom',
	bringToFront: 'Bring to front',
	bringForward: 'Bring forward',
	sendBackward: 'Send backward',
	sendToBack: 'Send to back',
	getTaskDefaultParamError: 'Cannot get default parameters of {0}',
	getTaskError: 'Cannot retrieve Action Task. {0}',
	format: 'Format',
	'navigator': 'Navigator',
	properties: 'Properties',
	label: 'Display Name',
	inputs: 'Inputs',
	outputs: 'Outputs',
	targets: 'Targets',
	none: 'None',
	good: 'Good',
	bad: 'Bad',
	sourceName: 'Value',
	displayName: 'Display Name',
	destination: 'Destination',
	variable: 'Variable',

	actionTaskSaveFailureTitle: 'Failed to save Action Task',
	actionTasksSaveFailureTitle: 'Failed to save Action Tasks',
	actionTaskSaveFailureBody: 'The following Action Task is invalid and was not saved:',
	actionTasksSaveFailureBody: 'The following Action Tasks are invalid and were not saved:',

	ConnectorProperty: {
		connector: 'Connector',
		condition: 'Condition',
		completion: 'Completion',
		notRequired: 'Not Required',
		required: 'Required'
	},
	showOption : 'Show in',
	TaskProperty: {
		taskName: 'Task Name',
		searchTask: 'Search Task',
		executeTask: 'Execute Task',
		parameters: 'Parameters',
		invalidTaskName: 'ActionTask name is required.',
		configure: 'Configure',
		view: 'View',
		viewEdit: 'View/Edit',
		saveTask: 'Save Task',
		editInNewTab: 'Edit in new browser tab',
		expandAll: 'Expand All',
		collapseAll: 'Collapse All',
		editActionTask: 'Edit',
		doneEditActionTask: 'Done Editing',
		wizardMode : 'Wizard Mode',
		tableMode : 'Table Mode'
	},

	RunbookProperty: {
		searchRunbook: 'Search Runbook',
		editRunbook: 'Edit Runbook',
		editInNewTab: 'Edit in new browser tab',
		executeRunbook: 'Execute Runbook',
		params: 'Parameters',
		addParam: 'Add',
		removeParams: 'Remove',
		invalidRunbook: 'This runbook does not exist.'
	},

	DocumentProperty: {
		wiki: 'Document',
		invalidWiki: 'Document \'{0}\' does not exist.',
		editInNewTab: 'Edit in new browser tab',
	},

	QuestionProperty: {
		label: 'Question Text',
		type: 'Layout',
		radio: 'Radio',
		dropdown: 'Dropdown',
		recommendationText : 'Recommendation',
		recommendationInfo : '<b>Note</b>: Recommendation only applies for answers with auto matching enabled.',	
	},
	NoProperty : {
		noProperty : 'This selection has no property.'
	},
	variableName : 'WSDATA Key',
	operator : 'Operator',
	expressionDetail : 'Value of <b>WSDATA{{0}}</b> is pulled from Action Task <b>{1}</b>, output <b>{2}</b>.',
	AnswerProperty: {	
		label: 'Answer',
		answer: 'Answer',
		expression : 'Expression',
		additionalData : 'Additional Data',
		order: 'Order',
		name: 'Name',
		value: 'Value',
		addData: 'Add',
		removeData: 'Remove',
		autoAnswer : 'Enable Auto Matching',		
		noExpression : 'Note: You need to define an expression for this answer.',
		new : 'New',
		edit : 'Edit',
		remove : 'Remove',
	},
	ExpressionForm : {
		variable : '<b>Variable</b>',
		wsdataKey : 'WSDATA Key',
		outputFromTask : "Set an Action Task's output to this variable",
		variableNameEmptyText : 'Enter or select WSDATA Key.',
		taskName : 'Task Name',
		taskNameEmptyText : 'Select an actiontask',
		outputName : 'Output Name',
		outputNameEmptyText : 'Select an output',
		addExpression : 'Add',
		updateExpression : 'Update',
		cancelAddExpression : 'Cancel',
		operandType : 'Value',
		expression : 'Expression',
		operator : 'Operator',
		operandEmptyText : 'Enter a value',		
		selectOperator : 'Select an operator',
		invalidTaskName: 'ActionTask name is required.',
		requiredField : 'This field is required.',
		duplicatedExpressionKey : 'Expressions with same key are not allowed.',
		invalidField: 'This field is required and must be in alphanumerics, but may contain spaces or underscores \'_\'.',
		duplicatedKey : 'This key has been assigned to a different output.'
	},
	EdgeMenu: {
		connector: 'Connector',
		execution: 'Execution',
		condition: 'Condition',
		required: 'Required',
		notRequired: 'Not Required',
		good: 'Good',
		bad: 'Bad',
		none: 'None'
	},
	ElementProperty: {
		nodeId: 'Node ID',
		elementType: 'Type',
		elementName: 'Name',
		elementPropertyTitle: 'Properties'
	},
	PreconditionProperty: {
		nodeId: 'Node ID',
		elementType: 'Type',
		elementPropertyTitle: 'Properties',
		expression: 'Expression',
		dump: 'OK'
	},
	ModelXml: {
		xmlTitle: '{0} Model',
		save: 'Save',
		close: 'Close',
		update: 'Update',
		cancel: 'Cancel',
		main: 'Main',
		decisionTree: 'Decision Tree',
		abort: 'Abort'
	},
	QuestionMenu: {
		selectType: 'Select Type',
		radio: 'Radio',
		dropdown: 'Dropdown'
	},
	DocumentMenu: {
		searchDecisionTree: 'Search Decision Tree'
	},
	name: 'Name',
	source: 'Source',
	value: 'Value',

	sourceChanged: 'Type locally modified',
	valueChanged: 'Value locally modified',
	valueProtected: 'Value is protected (read-only)',

	Parameters: {
		doDump: 'OK',
		addParam: 'Add',
		editParams: 'Edit',
		removeParams: 'Remove',
		inputParams: 'Input Parameters: {0}',
		outputParams: 'Output Parameters: {0}',
		fakeInput: 'Input',
		fakeOutput: 'Output',
		userDefined: 'User Defined',
		defaultParams: 'Default',
		customParams: 'Custom',
	},

	ParameterEdit: {
		name: 'Name',
		addParamTitle: 'Add Parameter',
		editParamTitle: 'Edit Parameter',
		description: 'Description',
		paramType: 'Parameter Type',
		fromSource: 'From Source',
		toDestination: 'To Destination',
		customValue: 'Custom Value',

		addVariable: 'Add Variable',
		defaultSourceValue: 'Value',

		noDefaultValueDefined: 'No default value defined',

		variableArg: 'Variable Argument',
		addVariableBtn: 'Add',
		cancelVariableBtn: 'Cancel',

		addParamBtn: 'Add',
		updateParamBtn: 'Update',
		defaultParam: 'Reset Default',

		validParamNameCharacters: 'Parameter name must start with a letter. The remaining characters can be letters, numbers, and underscores.',
		validParamKeyCharacters: 'Parameter key can only contain letters, numbers, and underscores. Please use custom value.',

		validFromSourceTypeCharacters: '<i>{0}</i> is an invalid source type.<br>',
		validFromDestinationTypeCharacters: '<i>{0}</i> is an invalid destination type.<br>',
		missingSourceType: 'Missing source type at position {0}.<br>',
		missingDestinationType: 'Missing destination type at position {0}.<br>',
	},

	ContainerPropertyTitle: 'Container Properties',
	ConnectorPropertyTitle: 'Properties',
	TextPropertyTitle: 'Text Properties',
	StartPropertyTitle: 'Start Properties',
	EndPropertyTitle: 'End Properties',
	EventPropertyTitle: 'Event Properties',
	PreconditionPropertyTitle: 'Precondition Properties',
	RunbookPropertyTitle: 'Runbook Properties',
	TaskPropertyTitle: 'Task Properties',
	DocumentPropertyTitle: 'Document Properties',
	QuestionPropertyTitle: 'Question Properties',
	AnswerPropertyTitle: 'Answer Properties',
	RootPropertyTitle: 'Root Properties',
	NoPropertyTitle: 'Properties',
	NewTaskTitle: 'Task Wizard',
	NewRunbookTitle: 'Runbook Wizard',
	invalidNamespace: 'A namespace may contain letters, numbers, periods, underscores, and spaces. It must end in a number or letter. The namespace "Resolve" is reserved.',
	invalidName: 'Name is required and must be in alphanumerics, but may contain single spaces or underscores \'_\'.',

	NewTask: {
		selectActionTask: 'Select an existing Task',
		createActionTask: 'Create a new Task',
		or: 'OR',

		type: 'Type',
		timeout: 'Timeout (sec)',
		connectionType : 'Connection Type',

		remote: 'REMOTE',
		assess: 'ASSESS',
		os: 'OS',
		bash: 'BASH',
		cmd: 'CMD',
		cscript: 'CSCRIPT',
		powershell: 'POWERSHELL',
		process: 'PROCESS',
		external: 'EXTERNAL',
		ssh: 'SSH',
		telnet : 'TELNET',
		http : 'HTTP/HTTPS',
		none : 'NONE',
		local : 'LOCAL',

		done: 'Done',
		create: 'Create',
		cancel: 'Cancel',

		SaveErr: 'Cannot create ActionTask.',
		SaveSuc: 'The ActionTask has been created.',
		invalidTaskName: 'ActionTask name is required.',

		// Selecting an existin task
		newTaskName: '2. Select Task',

		// Creating a new task
		enterTaskName: '2. What is the new task name?',
		namespace: 'Namespace',
		name: 'Name',

		// Template questions
		selectTemplateType: '3. Select a task template',
		genericActionTask: 'Generic Action Task',
		restAPIActionTask: 'Make a RESTful API call over HTTP/S',
		executeCommandOverNetwork: 'Execute command over network',
		executeCommandLocally: 'Execute command locally',

		// Generic Action Task template
		enterPropertiesPrompt: '4. What is the task type',

		// RESTful API call over HTTP template
		enterRequestProperties: '4. What are the request properties?',

		// Execute command over network connection template
		selectConnectionType: '4. Select connection type',
		enterNetworkCommandPrompt: '5. What is the command?',

		// Execute command locally template
		enterLocalCommandPrompt: '4. What is the command?',

		sessionNameOptional: 'Session Name',
		command: 'Command',
		commandIsRequired: 'Command is required.',
		addVariable: 'Add Variable',
		variableArg: 'Variable Argument',
		addVariableBtn: 'Add',
		cancelVariableBtn: 'Cancel',

		description: 'Description',
		options: 'Options:',
		properties: 'Properties:',
		taskTimeout: 'Task Timeout (sec)',
		expectedTimeout: 'Expected Timeout (sec)',
		commandPrompt: 'Prompt',

		addOption: 'Add Option',
		enterOptionValue: 'Enter Option Value',

	},

	actionTaskNotSaved: 'Invalid ActionTask detected. Please fix the error and retry.',
	actionTasksNotSaved: 'The following ActionTasks were not saved: ',

	NewRunbook: {
		selectRunbook: 'Select an existing Runbook',
		createRunbook: 'Create a new Runbook',
		or: 'OR',

		// Selecting an existing runbook
		runbookName: '2. Select Runbook',

		// Creating a new Runbook
		enterRunbookName: '2. What is the new Runbook name?',
		namespace: 'Namespace',
		name: 'Name',

		done: 'Done',
		create: 'Create',
		cancel: 'Cancel',

		SaveErr: 'Cannot create Runbook.',
		SaveSuc: 'The Runbook has been created.',
		invalidRunbook: 'This runbook does not exist.',
		wikiSaved: 'Document created successfully.'
	},

	useVersion: 'Use Version',
	versionNumber: 'Revision #', // 'Version #', TODO - support Version Control
	latest: 'Latest',
	latestStable: 'Latest Commit',
	specificVersion: 'Specific Version',
	details: 'Details',

	//CONNECTION PANEL (AD)

	mainConfigTitle : 'Configuration',
	host: 'Host',
	port: 'Port',
	u_sername : 'Username',
	p_assword : 'Password',
	command : 'Command',
	prompt: 'Prompt',
	timeout: 'Timeout (secs)',
	queueName: 'Queue Name',
	sessionName : 'Session Name',
	optionTitle : 'Options',
	actionTaskConfigTitle : 'Generate a template actiontask for this connection.',
	taskName : 'Task Name',
	taskNamespace : 'Namespace',
	invalidKey : 'Duplicated Key is not allowed.',

	addVariable: 'Add Variable',
	variableArg: 'Variable Argument',
	addVariableBtn: 'Add',
	cancelVariableBtn: 'Cancel',

	SSHConnectionConfig: {
		sshConfigTitle : 'SSH Connection Configuration',
		authType: 'Auth Type',
		usernameAndPassword: 'Username and Password',
		keyFileNameAndPassphrase: 'Key Filename and Passphrase',
		keyfile: 'Key',
		passphrase: 'Passphrase',
		taskWizardStep : 'TASK (Optional)',
		connectionWizardStep : 'CONNECTION',
		next : 'Next',
		done : 'Done'
	},
	TelnetConnectionConfig: {
		telnetConfigTitle : 'TELNET Connection Configuration',
		loginPrompt: 'Login Prompt',
		passwordPrompt: 'Password Prompt',
		taskWizardStep : 'TASK (Optional)',
		connectionWizardStep : 'CONNECTION',
		next : 'Next',
		done : 'Done'
	},
	HTTPConnectionConfig: {
		httpConfigTitle : 'HTTP/S Connection Configuration',
		protocol : 'Protocol',
		http : 'HTTP',
		https : 'HTTPS',
		taskWizardStep : 'TASK (Optional)',
		connectionWizardStep : 'CONNECTION',
		next : 'Next',
		done : 'Done'
	},
	TaskWizard : {
		openTaskPicker : 'Select a Task',
		createNewTask : 'Create a New Task',
		httpTaskInfo : 'For <b>HTTP/S</b> connection, you can execute an actiontask to do a RESTful API over this connection.',
		sshTaskInfo : 'For <b>SSH</b> connection, you can execute an actiontask to run a command over this connection.',
		telnetTaskInfo : 'For <b>TELNET</b> connection, you can execute an actiontask to run a command over this connection.'
	},
	TaskWizardProperty : {
		taskFullName : 'Task Name',
		parameter : 'Input Parameter',
		content : 'Content Script',
		openTaskPicker : 'Pick a Different Task',
		scriptToExecuteTitle : 'On save, use',
		myScript : 'My script',
		templateScript : 'Auto-generated template script',
		contentScriptConflictTitle : 'Content Script Conflict',
		contentScriptConflictMsg : 'There is a code conflict when attempting to wire this ActionTask with the current connection. <br> How do you want to resolve this?',
		revertMyCode : 'Revert to my code',
		overwrite : 'Overwrite it',
		userHandle : 'Let me handle it',
		editParameter : 'Edit Parameter',
		paramName : 'Name',
		paramDescription : 'Description',
		addParam : 'Add',
		removeParam : 'Remove',
		default : 'Default',
		paramType : 'Parameter Type',
		paramValue : 'Value',
		source : 'From Source',
		custom : 'Custom',
		editParam : 'Edit',
		sourceChanged: 'Type locally modified',
		valueChanged: 'Value locally modified',
		userDefined: 'User Defined',
		defaultParams: 'Default',
		additionalParams: 'Additional',
		updateParam : 'Update'
	},
	HTTPTaskConfig : {
		method : 'Method',
		url : 'URL',
		headerTitle : 'Headers',
		bodyTitle : 'Body',
		paramTitle : 'Params',
		syntax : 'Syntax',
		json : 'JSON',
		xml : 'XML',
		formData : 'form-data',
		variable : 'Variable',
	},
	add: 'Add',
	enableAddVariable : 'Add Variable',
	delete: 'Delete',
	key : 'Key',
	value : 'Value',
	upload: 'Upload',
	uploadImage: 'Upload Image',
	dragImageHere: 'Drag image here',
	discard: 'Cancel',
	imageLabel: 'Image Label',
	imageSize: 'Image/Size',
	selectAFile: 'select a file...',
	browse: 'Browse...',
	progress: 'Progress',
	image: 'Image',
	addImage: 'Add Image',
	deleteSelectedImage: 'Delete selected image',
	all: 'All',
	default: 'Default',
	custom: 'Custom',
	filterBy: 'Filter by...',
	succeededUploadingImage: 'Image successfully uploaded.',
	successfulDeleteImage: 'Image successfully deleted.',
	failedUploadingImage: 'Failed uploading the image.',
	notAllowedCharacters: '\'<b>{1}</b>\' contains one or more special characters \'<b>{0}</b>\' which are not supported.<br>',
	failedLoadingImage: 'Failed loading automation design images.',
	fileExceedsLime: 'File size can be no more than 40 KB.',
	duplicateLabelError: 'Image label name exists, please choose a different name.',
	emptyLabelError: 'Image\'s label name is blank.'
}

//Decision Tree
Ext.namespace('RS.decisiontree').locale = {
	cancel : 'Cancel',
	name : 'Name',
	uisRoot : 'DT',
	Answer: {
		recommededAnswer: 'This is the recommended answer.'
	},
	DocumentPicker: {
		all: 'All',
		title: 'Document',
		namespace: 'Namespace',
		select: 'Select',
		decisionTree: 'Document',
		newDocument: 'New Document',
		createDocument: 'Create'
	},
	Main : {
		restartDecisionTree : 'Restart',
		editDecisionTree : 'Edit',
		newExecutionTitle : 'Restart Decision Tree Execution',
		confirmNewExecutionMsg : 'Restart Decision Tree will remove all history and data for this session. Proceed?',
		newWs : 'New WS',
		currentWs : 'Current WS',
		newWorksheetActiveSuccess: 'New worksheet created and is now selected, <a href="#RS.worksheet.Worksheet/id={0}">click to view</a>',
		isAutoAnswerDisabled : 'Auto Answer',
		confirm : 'Confirm',
		viewCurrentPage : 'View current page',
		refreshCurrentPage : 'Refresh page',
		addFeedback : 'Give feedback',
		failedToLoadMsg : 'Could not load Question Variables from WSDATA.',
		resetDTDataSucceeded: 'Successfully reset decision tree data.'
	},
	Navigation: {
		autoAnswerDesc : 'The recommended answer was automatically chosen.',
	},
	QuestionBox : {
		suggestionHint : '<i style="color:gray;"><i class="icon-small icon-cogs"></i> Suggested Answer.</i>',
		endOfExecution : 'End of Execution'
	},
	ViewSettings: {
		viewSettings: 'Decision Tree View Settings',
		close: 'Close',
		recommendationFont : 'Recommendation Font',
		recommendationFontSize: 'Font Size',
		questionAndAnswer: 'Question/Answer Text',
		questionFont: 'Question Font',
		questionFontSize: 'Font Size',
		questionColor: 'Font Color',
		answerFont: 'Answer Font',
		answerFontSize: 'Font Size',
		answerColor: 'Font Color',
		confirmationButton: 'Confirmation Button',
		confirmationFont: 'Font',
		confirmationFontSize: 'Font Size',
		confirmationColor: 'Font Color',
		navigation: 'Navigation',
		navigationFont: 'Font',
		navigationFontSize: 'Font Size',
		navigationColor: 'Font Color',
		autoConfirm: 'Auto Confirm',
		confirm : 'Confirm',
		restoreDefault : 'Restore Default',
		layout: 'Layout',
		questionBoxPos : 'Question Box',
		questionOnTop : 'On Top',
		questionOnBottom : 'On Bottom',
		navPos: 'Navigation',
		navOnLeft: 'On Left',
		navOnRight: 'On Right',
		vertical : 'Vertical',
		horizontal : 'Horizontal',
		verticalAnswerLayout : 'Answer Layout *',
		verticalAnswerLayoutHint : '<i style="color:gray;">*Only applies to radio style answer.</i>',
		isAutoAnswerDisabled : 'Disable Auto Answer By Default'
	},
	ExecutionProgress : {
		executingTask : 'Processing Execution',
		executionCompleted : 'Execution is completed.',
		abort : 'Cancel'
	}
}
