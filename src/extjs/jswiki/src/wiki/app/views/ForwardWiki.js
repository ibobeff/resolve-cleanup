glu.defView('RS.wiki.ForwardWiki', {
	asWindow: {
		title: '@{windowTitle}',
		height: 180,
		width: 400,
		modal: true
	},
	bodyPadding: '10px',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'textfield',
		name: 'namespace'
	}, {
		xtype: 'textfield',
		name: 'name'
	}],
	buttonAlign: 'left',
	buttons: ['forward', 'cancel']
})