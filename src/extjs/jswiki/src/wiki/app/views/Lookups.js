glu.defView('RS.wiki.Lookups', {
	xtype: 'grid',
	layout: 'fit',
	displayName: '@{displayName}',
	name: 'lookups',
	store: '@{store}',
	padding: 15,
	cls :'rs-grid-dark',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls :'rs-small-btn rs-btn-light'
		},
		items: ['createLookup', 'deleteLookups']
	}],
	columns: '@{columns}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	},{
		ptype:'pager'
	},{
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.editColumnTooltip,
		glyph : 0xF044,
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},

	listeners: {
		editAction: '@{editLookup}'
	}
});