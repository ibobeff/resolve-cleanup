glu.defView('RS.wiki.RenameTemplate',{
	padding : 15,
	modal:true,
	title:'@{title}',
	width:400,
	cls : 'rs-modal-popup',
	layout:{
		type:'vbox',
		align:'stretch'
	},
	buttonAlign:'left',	
	defaultType:'textfield',
	items:[{
		xtype:'displayfield',
		name:'oldName'
	},'newName'],
	buttons:[{
        text : '~~sendReq~~',
        cls : 'rs-med-btn rs-btn-dark',
        handler : '@{sendReq}',
    },{
        text : '~~cancel~~',
        cls : 'rs-med-btn rs-btn-light',
        handler : '@{cancel}',
    }]	
});