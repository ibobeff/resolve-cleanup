/*
glu.defView('RS.wiki.ImageManager', {
	asWindow: {
		height: 400,
		width: 700,
		title: '~~imageManagerTitle~~',
		modal: true,
		beforerender : function(){				
			var toolbar = this.down('button[name="uploadFile"]').ownerCt;
			toolbar.addCls('rs-dockedtoolbar');
			var toolbarItems = toolbar.items;			
			Ext.Array.each(toolbarItems.items, function(btn){					
				btn.addCls('rs-btn-light rs-small-btn');
			})
		}	
	},

	layout: 'fit',
	items: [{
		xtype: 'uploadmanager',
		multiSelect: true,
		documentName: '@{name}',
		allowUpload: true,
		allowRemove: true,
		allowDownload: true,
		recordId: '@{name}',
		nameColumn: 'fileName',
		uploader: {
			url: '/resolve/service/wiki/attachment/upload',
			autoStart: true,
			max_file_size: '2020mb',
			flash_swf_url: '/resolve/js/plupload/plupload.flash.swf',
			urlstream_upload: true
		},
		listeners: {
			selectionchange: '@{selectionchange}',
			render: function(panel) {
				panel.store.filter([{
					filterFn: function(item) {
						var name = item.get('fileName') || item.get('ufilename') || item.get('name'),
							split = name.split('.'),
							extension = split[split.length - 1].toLowerCase();
						return Ext.Array.indexOf(['png', 'jpg', 'jpeg', 'gif'], extension) > -1
					}
				}])
			},
		}
	}],
	buttons: [{
		name : 'select',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-dark'
	}]
})
*/