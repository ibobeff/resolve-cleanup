glu.defView('RS.wiki.WikiAdmin', {
	padding: 15,
	xtype: 'grid',
	cls : 'rs-grid-dark',
	layout: 'fit',
	stateId: '@{stateId}',
	stateful: true,
	displayName: '@{displayName}',
	name: 'records',
	store: '@{store}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		name: 'actionBar',
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',			
		}, {
			xtype: 'button',
			text: 'File',
			menu: [
				'renameWikis',
				'copyWikis',
				'deleteWikis',
				'undeleteWikis',
				'purgeWikis',
				'commit'
			]
		}, {
			xtype: 'button',
			text: '~~status~~',
			menu: [
				'activateWikis',
				'deactivateWikis',
				'lockWikis',
				'unlockWikis',
				'hideWikis',
				'unhideWikis'
			]
		}, {
			xtype: 'button',
			text: '~~role~~',
			menu: ['configRoles']
		}, {
			xtype: 'button',
			text: '~~index~~',
			menu: [
				'indexWikis',
				'purgeIndex',
				'indexAllWikis',
				'purgeAllIndexes'
			]
		}, {
			xtype: 'button',
			text: '~~more~~',
			menu: ['rateWikis',
				'setReviewed',
				'setExpiryDate',
				'setHomepage',
				'resetStats',
				'configTags'
			]
		}]
	}],
	columns: '@{columns}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	features: [{
		ftype: 'grouping',
		groupHeaderTpl: '<div>{name} ({rows.length} total)</div>',
		// groupHeaderTpl: '<div>{name}</div>',
		id: 'unamespace'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.viewColumnTooltip,		
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'ufullname'
	},

	listeners: {
		editAction: '@{editRecord}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
});

glu.defView('RS.wiki.Polling', {
	bodyPadding: 15,
	cls : 'rs-modal-popup',
	// baseCls: 'x-panel',
	// ui: 'sysinfo-dialog',
	title: '~~busyTitle~~',
	items: [{
		xtype: 'container',
		html: '<i class="icon-spinner icon-spin icon-large"></i>&nbsp&nbsp@{busyText}'
	}],
	closable: false,
	height: 100,
	modal: true
});