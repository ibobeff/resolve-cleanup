glu.defView('RS.wiki.Viewer', {
	autoScroll: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: '@{itemList}'
	// html: '@{content}'
})