glu.defView('RS.wiki.AddDocument', {
	title: '~~addDocumentTitle~~',
	width: 520,
	height: 230,
	modal: true,
	cls : 'rs-modal-popup',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	padding: 15,
	defaults: {
		labelWidth: 125
	},
	items: [{
		xtype: 'textfield',
		name: 'name'
	}, {
		xtype: 'clearablecombobox',
		name: 'template',
		store: '@{templates}',
		displayField: 'uname',
		valueField: 'uwikiDocName',
		queryMode: 'local',
		editable: false,
		forceSelection: true
	}],
	buttons: [{
		name :'add', 
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})