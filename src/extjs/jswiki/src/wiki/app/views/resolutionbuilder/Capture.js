glu.defView('RS.wiki.resolutionbuilder.Capture', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10',
	items: [{
		xtype: 'textfield',
		name: 'name'
	}, {
		xtype: 'combo',
		name: 'type',
		store: '@{typeStore}',
		queryMode: 'local',
		valueField: 'value',
		displayField: 'name',
		tpl: Ext.create('Ext.XTemplate',
			'<tpl for=".">',
			'<div class="x-boundlist-item">{name}<tpl if="value==\'-\'"><hr/></tpl></div>',
			'</tpl>'
		),
		editable: false
	}],
	asWindow: {
		modal: true,
		width: 400,
		title: '~~assignVariable~~',
		listeners: {
			render: function() {
				this.getEl().on('keyup', function(e) {
					if (e.getCharCode() == 13)
						this.fireEvent('capture');
				}, this);
			},
			capture: '@{capture}',
			show: function() {
				this.down('textfield').focus();
			}
		}
	},
	buttonAlign: 'left',
	buttons: ['capture', 'cancel']
});