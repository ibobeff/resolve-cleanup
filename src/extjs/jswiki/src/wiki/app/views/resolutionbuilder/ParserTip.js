Ext.define('RS.wiki.resolutionbuilder.ResizeAndDragEnhancedWindow', {
	extend: 'Ext.window.Window',
	alias: 'widget.resizeenhancedwindow',
	initResizable: function() {
		this.callParent(arguments);
		this.resizer.on('beforeresize', function() {
			this.fireEvent('beforeresize');
		}, this)
	},
	initDraggable: function() {
		this.callParent(arguments);
		this.dd.on('dragstart', function() {
			this.fireEvent('dragstart');
		}, this);
		this.dd.on('dragend', function() {
			this.fireEvent('dragend');
		}, this);
	}
});
glu.defView('RS.wiki.resolutionbuilder.ParserTip', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	autoScroll: true,
	items: [{
		xtype: 'grid',
		name: 'markups',
		store: '@{markupStore}',
		plugins: [{
			ptype: 'cellediting'
		}],
		columns: [{
			header: '~~action~~',
			dataIndex: 'action',
			width: 70
		}, {
			header: '~~detail~~',
			flex: 160,
			renderer: function(v, meta, r) {
				if (r.raw.action == 'type')
					return 'Type:' + r.raw.type;
				else if (r.raw.action == 'capture')
					return 'Variable:' + r.raw.variable;
				else
					return '--';
			}
		}],
		listeners: {
			itemmouseenter: function(view, record, item, index, e, options) {
				this.up().up().parserView.parser.emphasizeOverlap(record.get('actionId'));
			},
			itemmouseleave: function(view, record, item, index, e, options) {
				this.up().up().parserView.parser.understateOverlap(record.get('actionId'));
			}
		}
	}],
	buttonAlign: 'left',
	buttons: ['remove', 'cancel'],
	asWindow: {
		parserView: '@{parserView}',
		initXY: '@{initXY}',
		xtype: 'resizeenhancedwindow',
		title: '~~markups~~',
		width: 300,
		height: 250,
		ui: (!Ext.isIE8m ? 'sysinfo-dialog' : 'default'),
		baseCls: (!Ext.isIE8m ? 'x-panel' : 'x-window'),
		layout: 'fit',
		pinned: '@{pinned}',
		setPinned: function(pinned) {
			var cls = 'rs-social-button icon-pushpin';
			if (!pinned)
				cls += ' icon-rotate-90';
			this.down('button[name="togglePin"]').setIconCls(cls);
			if (!pinned)
				delete this.pinPos;
		},
		tools: [{
			xtype: 'button',
			name: 'togglePin',
			baseCls: 'x-tool',
			iconCls: 'rs-social-button icon-pushpin icon-rotate-90',
			handler: function() {
				this.up().up().fireEvent('togglePin');
			}
		}],
		listeners: {
			show: function(win) {
				this.parserView.parser.setTipWindow(this);
			},
			beforeresize: function() {
				this.parserView.disable();
			},
			resize: function() {
				this.parserView.enable();
			},
			dragstart: function() {
				this.parserView.disable();
			},
			dragend: function() {
				this.parserView.enable();
			},
			render: function() {
				this.getEl().on('mouseleave', function() {
					if (!this.pinned)
						this.hide();
				}, this);
				this.getEl().on('mouseup', function() {
					if (this.parserView.isDisabled())
						this.parserView.enable();
				}, this);
			},
			updateMarkups: '@{updateMarkups}',
			togglePin: '@{togglePin}',
			pin: '@{pin}',
			unpin: '@{unpin}'
		}
	}
});
// else
// 	glu.defView('RS.wiki.resolutionbuilder.ParserTip', {
// 		html: 'test',
// 		asWindow: {
// 			initXY: '@{initXY}',
// 			width: 300,
// 			height: 300,
// 			closable: false,
// 			tools: [{
// 				xtype: 'button',
// 				name: 'togglePin',
// 				baseCls: 'x-tool',
// 				iconCls: 'rs-social-button icon-pushpin icon-rotate-90',
// 				handler: function() {
// 					this.up().up().fireEvent('togglePin');
// 				}
// 			}]
// 		}
// 	});