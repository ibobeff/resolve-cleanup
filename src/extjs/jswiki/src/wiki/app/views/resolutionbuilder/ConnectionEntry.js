glu.defView('RS.wiki.resolutionbuilder.ConnectionEntry', {
	tbar: [{
		xtype: 'tbtext',
		text: '@{connectionEntry}',
		hidden: false,
		style: 'font-size:10px',
		listeners: {
			render: function() {
				this.getEl().on('click', function() {
					var panel = this.up().up().down('*[collapsible]')
					if (panel.items.length == 0)
						return;
					panel[panel.collapsed ? 'expand' : 'collapse']();
				}, this)
			}
		}
	}, '->', 'addTask', 'addRunbook', 'addIf', {
		xtype: 'tool',
		type: 'gear',
		handler: function() {
			this.ownerCt.ownerCt.fireEvent('config', this, this);
		}
	}, {
		xtype: 'tool',
		type: 'close',
		handler: function() {
			(this.ownerCt.ownerCt || this.ownerCt.ownerButton.ownerCt.ownerCt).fireEvent('remove', this, this);
		}
	}],
	margin: '@{margin}',
	items: [{
		collapsible: true,
		header: false,
		items: '@{entries}'
	}],
	plugins: [{
		ptype: 'rbentitydroptarget'
	}],
	selector: '.connectionEntry',
	selected: '@{selected}',
	setSelected: function(selected) {
		this.selected = selected;
		// if (!this.selected)
		// 	this.getEl().removeCls('entrySelection');
		// else
		// 	this.getEl().addCls('entrySelection');
	},
	collapsed: false,
	listeners: {
		render: function() {
			this.getEl().addCls('connectionEntry');
			this.getEl().dom.abCmp = this;
			// this.getEl().on('click', function(e) {
			// 	this.fireEvent('entryClicked', this, e)
			// }, this);
		},
		entryClicked: '@{entryClicked}',
		remove: '@{remove}',
		config: '@{config}',
		accept: '@{accept}'
	}
});