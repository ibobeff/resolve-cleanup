glu.defView('RS.wiki.resolutionbuilder.RunbookGenerator', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaultType: 'textfield',
	bodyPadding: 10,
	items: ['name', 'namespace'],
	asWindow: {
		modal: true,
		title: '~~generateRunbook~~',
		width: 500,
		height: 170
	},
	buttonAlign: 'left',
	buttons: ['generate', 'cancel']
});