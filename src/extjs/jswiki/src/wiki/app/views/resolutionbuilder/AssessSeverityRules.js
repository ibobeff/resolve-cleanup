glu.defView('RS.wiki.resolutionbuilder.AssessSeverityRules', {
	title: '~~severityRules~~',
	dockedItems: [{
		xtype: 'toolbar',
		cls: 'actionBar actionBar-form',
		items: ['add', 'remove', '-', {
			xtype: 'displayfield',
			fieldLabel: '~~defaultValue~~',
			value: ''
		}, {
			xtype: 'radiofield',
			name: 'severityField',
			boxLabel: '~~goodDefaultSeverity~~',
			value: '@{goodDefaultSeverity}'
		}, {
			xtype: 'radiofield',
			name: 'severityField',
			boxLabel: '~~warningDefaultSeverity~~',
			value: '@{warningDefaultSeverity}'
		}, {
			xtype: 'radiofield',
			name: 'severityField',
			boxLabel: '~~severeDefaultSeverity~~',
			value: '@{severeDefaultSeverity}'
		}, {
			xtype: 'radiofield',
			name: 'severityField',
			boxLabel: '~~criticalDefaultSeverity~~',
			value: '@{criticalDefaultSeverity}'
		}]
	}],

	autoScroll: true,
	items: '@{conditions}'
});

glu.defView('RS.wiki.resolutionbuilder.SeverityRuleGroup', {
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	margin: '5 0 0 0',
	items: [{
		result: '@{result}',
		setResult: function(result) {
			this.result = result;
			this.updateLeftBorder();
		},
		updateLeftBorder: function() {
			if (this.result.toLowerCase() == 'good')
				this.body.setStyle('background-color', 'palegreen');
			else if (this.result.toLowerCase() == 'warning')
				this.body.setStyle('background-color', 'khaki');
			else if (this.result.toLowerCase() == 'severe')
				this.body.setStyle('background-color', 'orange');
			else
				this.body.setStyle('background-color', 'tomato')
		},
		width: 10,
		overCls: 'rs-rb-rule-result-over',
		html: '',
		listeners: {
			render: function() {
				this.updateLeftBorder();
				this.body.on('click', function() {
					this.fireEvent('toggleResult');
				}, this);
			},
			toggleResult: '@{toggleResult}'
		}
	}, {
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		flex: 1,
		items: '@{conditions}'
	}],
	selected: '@{selected}',
	setSelected: function(selected) {
		this.selected = selected;
		if (this.selected) {
			this.removeCls('rs-rb-rule');
			this.addCls('rs-rb-rule-selected');
		} else {
			this.removeCls('rs-rb-rule-selected');
			this.addCls('rs-rb-rule');
		}
	},
	qtip: '@{qtip}',
	listeners: {
		render: function() {
			this.getEl().on('click', function() {
				this.fireEvent('toggleSelection');
			}, this)
			this.addCls('rs-rb-rule');
			Ext.create('Ext.tip.ToolTip', {
				target: this.getEl(),
				html: this.qtip
			});
		},
		toggleSelection: '@{toggleSelection}'
	}
});

glu.defView('RS.wiki.resolutionbuilder.SeverityRule', {
	bodyPadding: '5 0 0 5',
	layout: 'hbox',
	defaultType: 'combo',
	defaults: {
		displayField: 'name',
		valueField: 'value',
		editable: false,
		hideLabel: true,
		padding: '0 5 0 0'
	},
	items: [{
		name: 'criteria',
		width: 63,
		store: '@{criteriaStore}'
	}, {
		name: 'variableSource',
		width: 108,
		store: '@{sourceStore}'
	}, {
		xtype: 'fieldcontainer',
		flex: 1,
		minWidth: 100,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaultType: 'combo',
		defaults: {
			hideLabel: true,
			displayField: 'name',
			valueField: 'name'
		},
		items: [{
			xtype: 'textfield',
			name: 'variable',
			hidden: '@{!variableFromConst}'
		}, {
			name: 'variable',
			store: '@{globalVariableStore}',
			hidden: '@{!variableFromWSDATAOrFlow}',
			queryMode: 'local',
			tpl: Ext.create('Ext.XTemplate',
				'<tpl for=".">',
				'<div class="x-boundlist-item"><tpl if="color"><span style="background-color:{color}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;</tpl>{[this.formatFields(values)]}<tpl if="task">&nbsp; from:{task}</tpl></div>',
				'</tpl>', {
					formatFields: function(rec) {
						return Ext.htmlEncode(rec.name);
					}
				}
			),
			listeners: {
				focus: '@{updateVariables}'
			}
		}, {
			name: 'variable',
			store: '@{paramStore}',
			queryMode: 'local',
			hidden: '@{!variableFromParam}'
		}, {
			name: 'variable',
			store: '@{propertyStore}',
			pageSize: 10,
			minChars: 0,
			displayField: 'uname',
			valueField: 'uname',
			hidden: '@{!variableFromProperty}'
		}]
	}, {
		name: 'comparison',
		width: 120,
		store: '@{comparisonStore}'
	}, {
		name: 'valueSource',
		emptyText: '~~source~~',
		width: 108,
		store: '@{sourceStore}'
	}, {
		xtype: 'fieldcontainer',
		minWidth: 100,
		flex: 1,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaultType: 'combo',
		defaults: {
			hideLabel: true,
			displayField: 'name',
			valueField: 'name'
		},
		items: [{
			xtype: 'textfield',
			name: 'value',
			hidden: '@{!valueFromConst}'
		}, {
			name: 'value',
			store: '@{globalVariableStore}',
			hidden: '@{!valueFromWSDATAOrFlow}',
			queryMode: 'local',
			tpl: Ext.create('Ext.XTemplate',
				'<tpl for=".">',
				'<div class="x-boundlist-item"><tpl if="color"><span style="background-color:{color}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;</tpl>{name}&nbsp; <tpl if="task">from:{task}</tpl></div>',
				'</tpl>'
			),
			listeners: {
				focus: '@{updateSourceNames}'
			}
		}, {
			name: 'value',
			store: '@{paramStore}',
			queryMode: 'local',
			hidden: '@{!valueFromParam}'
		}, {
			name: 'value',
			store: '@{propertyStore}',
			pageSize: 10,
			minChars: 0,
			displayField: 'uname',
			valueField: 'uname',
			hidden: '@{!valueFromProperty}'
		}]
	}, {
		xtype: 'fieldcontainer',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		width: 40,
		items: [{
			xtype: 'fieldcontainer',
			layout: 'hbox',
			defaults: {
				style: {
					'padding-left': '3px',
					'padding-top': '3px',
					'color': '#999'
				}
			},
			defaultType: 'component',
			items: [{
				autoEl: {
					tag: 'a',
					cls: 'icon-plus icon-large'
				},
				listeners: {
					render: function() {
						this.getEl().on('mouseover', function() {
							this.setStyle('cursor', 'pointer');
						});
						this.getEl().on('click', function(e) {
							e.stopPropagation();
							this.fireEvent('addSibling');
						}, this)
					},
					addSibling: '@{addSibling}'
				}
			}, {
				autoEl: {
					tag: 'a',
					cls: 'icon-minus icon-large'
				},
				listeners: {
					render: function() {
						this.getEl().on('mouseover', function() {
							this.setStyle('cursor', 'pointer');
						});
						this.getEl().on('click', function(e) {
							e.stopPropagation();
							this.fireEvent('remove');
						}, this);
					},
					remove: '@{remove}'
				}
			}]
		}]
	}]
});