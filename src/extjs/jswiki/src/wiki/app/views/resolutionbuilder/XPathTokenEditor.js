glu.defView('RS.wiki.resolutionbuilder.XPathTokenEditor', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10',
	autoScroll: true,
	tbar: ['addTextPredicate', 'addAttrPredicate', 'addCustPredicate', {
		xtype: 'radiofield',
		name: 'operator',
		value: '@{and}',
		hideLabel: true,
		boxLabel: '~~and~~'
	}, {
		xtype: 'radiofield',
		name: 'operator',
		value: '@{or}',
		hideLabel: true,
		boxLabel: '~~or~~'
	}],
	items: '@{predicates}',
	asWindow: {
		title: '~~predicates~~',
		width: 600,
		height: 270,
		ui: (!Ext.isIE8m ? 'sysinfo-dialog' : 'default'),
		baseCls: (!Ext.isIE8m ? 'x-panel' : 'x-window'),
		listeners: {
			render: function() {
				var xy = this.items.getAt(0)._vm.computeOffset(this);
				this.showAt(xy);
			}
		},
		buttonAlign: 'left',
		buttons: ['update', 'cancel']
	}
});

glu.defView('RS.wiki.resolutionbuilder.XPathTextPredicate', {
	layout: 'hbox',
	defaults: {
		hideLabel: true
	},
	bodyPadding: '0 0 5 0',
	items: [{
		xtype: 'displayfield',
		hideLabel: false,
		width: 110,
		fieldLabel: '~~text~~',
		value: ''
	}, {
		xtype: 'combo',
		padding: '0 0 0 5',
		name: 'condition',
		displayField: 'name',
		valueField: 'value',
		store: '@{..textConditionStore}',
		width: 120,
		queryMode: 'local'
	}, {
		xtype: 'textfield',
		padding: '0 0 0 5',
		name: 'value',
		flex: 1
	}, {
		width: 20,
		style: {
			'padding-left': '3px',
			'padding-top': '3px',
			'color': '#999'
		},
		autoEl: {
			tag: 'a',
			cls: 'icon-minus icon-large'
		},
		listeners: {
			render: function() {
				this.getEl().on('mouseover', function() {
					this.setStyle('cursor', 'pointer');
				});
				this.getEl().on('click', function(e) {
					e.stopPropagation();
					this.fireEvent('remove');
				}, this);
			},
			remove: '@{remove}'
		}
	}]
});

glu.defView('RS.wiki.resolutionbuilder.XPathAttrPredicate', {
	layout: 'hbox',
	defaults: {
		hideLabel: true
	},
	bodyPadding: '0 0 5 0',
	items: [{
		xtype: 'displayfield',
		hideLabel: false,
		width: 110,
		fieldLabel: '~~attribute~~',
		value: ''
	}, {
		xtype: 'combo',
		padding: '0 0 0 5',
		name: 'name',
		store: '@{..attributeStore}',
		width: 130,
		displayField: 'name',
		valueField: 'name',
		queryMode: 'local'
	}, {
		xtype: 'combo',
		padding: '0 0 0 5',
		name: 'condition',
		store: '@{..attrConditionStore}',
		width: 120,
		displayField: 'name',
		valueField: 'value',
		queryMode: 'local'
	}, {
		xtype: 'textfield',
		padding: '0 0 0 5',
		name: 'value',
		flex: 1
	}, {
		width: 20,
		style: {
			'padding-left': '3px',
			'padding-top': '3px',
			'color': '#999'
		},
		autoEl: {
			tag: 'a',
			cls: 'icon-minus icon-large'
		},
		listeners: {
			render: function() {
				this.getEl().on('mouseover', function() {
					this.setStyle('cursor', 'pointer');
				});
				this.getEl().on('click', function(e) {
					e.stopPropagation();
					this.fireEvent('remove');
				}, this);
			},
			remove: '@{remove}'
		}
	}]
});

glu.defView('RS.wiki.resolutionbuilder.XPathCustomizedPredicate', {
	layout: 'hbox',
	bodyPadding: '0 0 5 0',
	items: [{
		xtype: 'displayfield',
		fieldLabel: '~~customized~~',
		width: 110,
		value: ''
	}, {
		xtype: 'textfield',
		padding: '0 0 0 5',
		name: 'predicate',
		hideLabel: true,
		flex: 1
	}, {
		width: 20,
		style: {
			'padding-left': '3px',
			'padding-top': '3px',
			'color': '#999'
		},
		autoEl: {
			tag: 'a',
			cls: 'icon-minus icon-large'
		},
		listeners: {
			render: function() {
				this.getEl().on('mouseover', function() {
					this.setStyle('cursor', 'pointer');
				});
				this.getEl().on('click', function(e) {
					e.stopPropagation();
					this.fireEvent('remove');
				}, this);
			},
			remove: '@{remove}'
		}
	}]
})