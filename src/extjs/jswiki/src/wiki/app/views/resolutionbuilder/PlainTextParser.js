glu.defView('RS.wiki.resolutionbuilder.dummy') //to make sure the ns is init before creating the factory
RS.wiki.resolutionbuilder.views.plainTextParserTemplate = function(actualView) {
	return {
		parentLayout: 'baseParser',
		templateActions: actualView.templateActions.concat([{
			text: '~~reset~~',
			hidden: '@{!templateTabIsPressed}',
			menu: {
				xtype: 'menu',
				items: [{
					name: 'clearMarkup',
					handler: function() {
						var parser = this.up().ownerButton.up().up().down('#parser');
						var selection = rangy.getIframeSelection(parser.parser.iframe);
						if (selection.anchorNode == selection.focusNode && selection.anchorOffset == selection.focusOffset) {
							this.fireEvent('clearMarkup');
							return;
						}
						var markup = parser.parser.computeMarkup(selection);
						this.fireEvent('clearMarkup', this, markup);
					},
					listeners: {
						clearMarkup: '@{clearMarkup}'
					}
				}, 'clearSample']
			}
		}]),
		renderer: {
			itemId: 'parser',
			minHeight: 450,
			inited: false,
			//we will see whether we need to make it generic
			flex: 1,
			parser: {
				iframe: null,
				win: null,
				tipWindow: null,
				calcuPos: function(e) {
					var iframeXY = Ext.fly(this.iframe).getXY();
					var doc = this.cdoc();
					var scrollTop = Ext.fly(doc.body).getScroll().top - 5;
					var scrollLeft = Ext.fly(doc.body).getScroll().left - 5;
					var x = e.getX() - scrollLeft;
					var y = e.getY() - scrollTop;
					if (e.browserEvent.view != window) {
						x += iframeXY[0]
						y += iframeXY[1]
					}
					return [iframeXY[0] + e.getX() - scrollLeft, iframeXY[1] + e.getY() - scrollTop];
				},
				recalcWithBoundary: function(xy) {
					var bottom = this.tipWindow.getHeight() + xy[1]
					var right = this.tipWindow.getWidth() + xy[0]
					if (bottom >= Ext.getBody().getHeight())
						xy[1] = xy[1] - this.tipWindow.getHeight() - 15;
					if (right >= Ext.getBody().getWidth())
						xy[0] = xy[0] - this.tipWindow.getWidth() - 15;
					return xy;
				},
				setTipWindow: function(tipWindow) {
					this.tipWindow = tipWindow;
					if (!this.tipWindow.pinned)
						this.tipWindow.showAt(this.recalcWithBoundary(tipWindow.initXY));
				},
				showTip: function(e, actionIds) {
					var xy = this.calcuPos(e);
					if (this.tipWindow == null || !this.tipWindow.getEl()) {
						this.eventEmitter.fireEvent('createTip', this.eventEmitter, this.eventEmitter, xy, actionIds);
						return;
					}
					this.tipWindow.fireEvent('updateMarkups', this.eventEmitter, actionIds);
					this.tipWindow.show();
					if (this.tipWindow && !this.tipWindow.pinned) {
						this.tipWindow.setPosition(this.recalcWithBoundary(xy));
					}
				},
				hideTip: function() {
					if (this.tipWindow == null)
						return;
					this.tipWindow.hide();
				},
				pinTip: function(xy) {
					this.tipWindow.pinPos = xy;
					this.tipWindow.fireEvent('pin');
				},
				cdoc: function() {
					return this.win.document;
				},
				getSelection: function(zeroLengthMark, selectAllIfNotSelected) {
					var selection = rangy.getIframeSelection(this.iframe);
					if (selection.anchorNode == selection.focusNode && selection.anchorOffset == selection.focusOffset) {
						if (!selectAllIfNotSelected && !zeroLengthMark) {
							this.eventEmitter.fireEvent('noSelectionError');
							return;
						}
						if (selectAllIfNotSelected) {
							var range = rangy.createRange();
							range.selectNodeContents(this.cdoc().body);
							selection = rangy.getIframeSelection(this.iframe);
							selection.setSingleRange(range);
						}
					}
					return selection;
				},
				getNodeText: function(node) {
					var textPropName = node.textContent === undefined ? 'nodeValue' : 'textContent';
					return node[textPropName];
				},
				eventEmitter: null,
				selectedMarkup: null,
				placeHolders: {
					space: '<span marker="space" class="format">.</span>',
					new_line: '<span marker="new_line"  class="return">&lt;</span><br>'
				},

				convert: function(text, pre, post) {
					return text.replace(/ /g, this.placeHolders.space).replace(Ext.is.Windows ? (/\r\n/g) : (/\n/g), this.placeHolders.new_line);
				},

				cook: function(sample, markups) {
					var pts = [];
					Ext.each(markups, function(markup) {
						pts.push(markup.from);
						pts.push(markup.from + markup.length);
					});
					pts = Ext.Array.unique(pts);
					if (pts.indexOf(0) == -1)
						pts.push(0);
					if (pts.indexOf(sample.length) == -1)
						pts.push(sample.length);
					pts.sort(function(a, b) {
						return a > b ? 1 : (a == b ? 0 : -1);
					});
					//chopping
					var chopped = [];
					for (var i = 0; i < pts.length - 1; i++)
						chopped.push({
							text: sample.substring(pts[i], pts[i + 1]),
							from: pts[i],
							to: pts[i + 1],
							actionIds: [],
							cols: []
						});
					//mix the ingredient
					var parseFromEndOfLineWedge = null;
					var stopParseAtStartOfLineWedge = Infinity;
					var beginOfLineWedge = null;
					var endOfLineWedge = null;
					Ext.each(chopped, function(slice, idx) {
						Ext.each(markups, function(markup) {
							if (slice.from >= markup.from && slice.to <= (markup.from + markup.length)) {
								slice.actionIds.push(markup.actionId);
								slice.col = markup.column;
								slice.ignore = markup.ignore;
							}
							if (markup.action == 'parseFromEndOfLine' && slice.to == markup.from)
								parseFromEndOfLineWedge = idx;
							if (markup.action == 'stopParseAtStartOfLine' && slice.from == markup.from && stopParseAtStartOfLineWedge > idx)
								stopParseAtStartOfLineWedge = idx;
							if (markup.type == 'beginOfLine' && slice.from == markup.from)
								beginOfLineWedge = idx;
							if (markup.type == 'endOfLine' && slice.to == markup.from + markup.length)
								endOfLineWedge = idx;
						});
					});
					//stir fry
					var done = [];
					Ext.each(chopped, function(slice, idx) {
						var pre = [];
						var post = [];
						if (idx == beginOfLineWedge)
							pre.push(Ext.String.format('<span class="beginOfLine"></span>'));
						if (idx == stopParseAtStartOfLineWedge)
							pre.push(Ext.String.format('<span><i class="icon-long-arrow-up stopParseAtStartOfLine"></i><i class="icon-long-arrow-up stopParseAtStartOfLine"></i></span>'));
						if (idx == endOfLineWedge)
							post.push(Ext.String.format('<span class="endOfLine"></span>'));
						if (idx == parseFromEndOfLineWedge)
							post.push(Ext.String.format('<span><i class="icon-long-arrow-down parseFromEndOfLine"></i><i class="icon-long-arrow-down parseFromEndOfLine"></i></span>'));
						var convertedContent = this.convert(Ext.htmlEncode(slice.text));
						done.push(pre.join(''));
						var endWithNewLine = false;
						if (Ext.String.endsWith(convertedContent, this.placeHolders.new_line)) {
							convertedContent = convertedContent.substring(0, convertedContent.length - 4);
							endWithNewLine = true;
						}
						if (slice.actionIds.length > 0)
							done.push(Ext.String.format('<span {1} {2}>{0}</span>', convertedContent, 'actionIds="' + slice.actionIds.toString() + '"', slice.col ? 'col="' + slice.col + '"' : ''))
						else
							done.push(convertedContent);
						done.push(post.join(''));
						if (endWithNewLine)
							done.push('<br>')
					}, this);
					var innerHTML = done.join('');
					return innerHTML;
				},

				checkIgnore: function(actionIdStr) {
					var ids = actionIdStr.split(',');
					var ignore = true;
					Ext.each(this.eventEmitter.markups, function(markup) {
						Ext.each(ids, function(id) {
							if (id == markup.actionId)
								ignore = ignore && markup.ignore === true;
						})
					});
					return ignore;
				},
				whenMarkupDblClick: actualView.whenMarkupDblClick || function() {},
				update: function(sample, markups) {
					var doc = this.cdoc();

					doc.body.innerHTML = '';
					if (!sample)
						return;
					if (!markups || markups.length == 0) {
						var innerHTML = this.convert(Ext.htmlEncode(sample));
						doc.body.innerHTML = innerHTML;
						return;
					}
					var cooked = this.cook(sample, markups);
					doc.body.innerHTML = cooked;
					Ext.each(Ext.fly(doc.body).query('span'), function(span) {
						if (span.getAttribute('actionIds')) {
							if (span.getAttribute('actionIds').split(',').length == 0)
								return;
							var actionIds = span.getAttribute('actionIds').split(',');
							Ext.each(actionIds, function(actionId) {
								Ext.each(markups, function(markup) {
									if (markup.actionId != actionId)
										return;
									Ext.fly(span).addCls(markup.action);
									if (markup.inlineStyle)
										Ext.Object.each(markup.inlineStyle, function(k, v) {
											span.style[k] = v;
										})
								});
							});

							Ext.fly(span).on('mouseover', function(e) {
								if (this.checkIgnore(span.getAttribute('actionIds')))
									return;
								actualView.emphasize.call(this, span, e);
							}, this);
							Ext.fly(span).on('mouseout', function(e) {
								if (this.checkIgnore(span.getAttribute('actionIds')))
									return;
								actualView.understate.call(this, span, e);
							}, this);
							Ext.fly(span).on('click', function(e) {
								this.toggleMarkup(span);
							}, this);
							Ext.fly(span).on('dblclick', function(e) {
								this.whenMarkupDblClick(span);
								if (this.tipWindow && !this.tipWindow.hidden)
									this.pinTip(e.getXY());
							}, this)
						}

						if (span.getAttribute('qtip')) {
							Ext.QuickTips.register({
								target: Ext.get(span),
								text: span.getAttribute('qtip')
							});
						}
					}, this);
				},
				getMarkup: function(attr, value) {
					var m = null;
					Ext.each(this.eventEmitter.markups, function(markup) {
						if (value == markup[attr])
							m = markup;
					}, this)
					return m;
				},
				getMarkupByActionId: function(actionId) {
					return this.getMarkup('actionId', actionId)
				},
				emphasizeOverlap: function(actionId) {
					var logicalConnected = Ext.fly(this.cdoc()).query('span[actionIds*="' + actionId + '"]');
					Ext.each(logicalConnected, function(span, idx) {
						Ext.fly(span).addCls('overlap-center');
						if (idx == 0)
							Ext.fly(span).addCls('overlap-left');
						if (idx == logicalConnected.length - 1)
							Ext.fly(span).addCls('overlap-right');
					});
				},
				understateOverlap: function(actionId) {
					var logicalConnected = Ext.fly(this.cdoc()).query('span[actionIds*="' + actionId + '"]');
					Ext.each(logicalConnected, function(span, idx) {
						Ext.fly(span).removeCls('overlap-center');
						if (idx == 0)
							Ext.fly(span).removeCls('overlap-left');
						if (idx == logicalConnected.length - 1)
							Ext.fly(span).removeCls('overlap-right');
					});
				},
				understateMarkup: function(actionId) {
					var spans = Ext.fly(this.cdoc()).query('span[actionIds*="' + actionId + '"]');
					if (spans.length > 0)
						this.understate(spans[0]);
				},

				toggleMarkup: function(span) {
					if (this.selectedMarkup == span) {
						this.selectedMarkup = null;
					} else {
						this.selectedMarkup = span;
					}
				},
				assembleComplex: function(segment) {
					var str = '';
					for (var i = 0; i < segment.length; i++) {
						if (segment[i].nodeType == 3)
							str += this.getNodeText(segment[i]);
						else {
							if (segment[i].getAttribute('marker') == 'new_line')
								str += (Ext.is.Windows ? '\r\n' : '\n');
							else if (segment[i].getAttribute('marker') == 'space')
								str += ' ';
						}
					}
					return str;
				},
				initParser: function(iframe) {
					var me = this;
					this.iframe = iframe;

					this.win = iframe.contentWindow;
					var doc = this.win.document;
					// IE 8 getSelection() missing object and properties simple hack
					if (this.win.getSelection == undefined) { // IE?
						this.eventEmitter.disable();
						var me = this;
						var wgS = setInterval(function() { // wait for Rangy 
							if (rangy.initialized) {
								clearTimeout(wgS); // exit 
								this.eventEmitter.enable();
							}
						}, 10);
					}
					var count = 0;
					// this.win.whenReady = function() {
					// 	count++;
					// 	if (me.inited = (count == 2))
					// 		me.update(me.eventEmitter.sample, me.eventEmitter.markups);
					// };
					//ie8 fires load twice..don't know why..
					var head = doc.head || doc.getElementsByTagName('head')[0];
					if (!head)
						return;
					doc.designMode = 'on';
					if (doc.body && ('spellcheck' in doc.body))
						doc.body.spellcheck = false;
					Ext.fly(doc).on('keydown', function(e) {
						if (!e.ctrlKey)
							e.preventDefault()
					});
					//need to pay attention to the last return,, need an extra return to make the last return selectable;
					//need to handle /r
					if (!doc.body)
						return;

					doc.body.onpaste = function(e) {
						doc.body.innerHTML = '';
						//again, for IE8
						var rawData = e && e.clipboardData ? e.clipboardData.getData('text/plain') : window.clipboardData.getData('Text');
						var lines = rawData.split(/\r*\n+/);
						var data = lines.join(Ext.is.Windows ? '\r\n' : '\n');
						if (data.length > 25000) {
							me.eventEmitter.fireEvent('sampleTooLong');
							return false;
						}
						me.eventEmitter.fireEvent('sampleChanged', me, data);
						var innerHTML = me.convert(Ext.htmlEncode(data));
						doc.body.innerHTML = '<font face="lucida console" size="2px">' + innerHTML + '</font>';
						return false;
					}
					Ext.fly(doc).on('click', function() {
						var parentPanel = this.eventEmitter.up().up();
						Ext.each(parentPanel.query('button[menu]'), function(btn) {
							if (btn.menu)
								btn.menu.hide();
						})
					}, this);
					// this.iframe.onresize = function() {
					// 	me.update(me.sample, me.markups);
					// }
					count++;
					// if (me.inited = (count == 2))

					me.update(me.eventEmitter.sample, me.eventEmitter.markups);
					me.inited = true;
				},
				parsePartialStartNode: function(node, startOffset) {
					// if (node.nodeType != 3)
					// 	return {
					// 		pre: '',
					// 		selected: node.getAttribute('marker') == 'new_line' ? (Ext.is.Windows ? '\r\n' : '\n') : ' '
					// 	};
					return {
						pre: this.getNodeText(node).substring(0, startOffset),
						selected: this.getNodeText(node).substring(startOffset, this.getNodeText(node).length)
					};
				},
				parsePartialEndNode: function(node, endOffset) {
					// if (node.nodeType != 3)
					// 	return {
					// 		post: '',
					// 		selected: node.getAttribute('marker') == 'new_line' ? (Ext.is.Windows ? '\r\n' : '\n') : ' '
					// 	};
					return {
						post: this.getNodeText(node).substring(endOffset, this.getNodeText(node).length),
						selected: this.getNodeText(node).substring(0, endOffset)
					};
				},
				computeMarkup: function(selection) {
					if (!selection || selection.getAllRanges().length == 0)
						return;
					var range = selection.getRangeAt(0);
					var start = range.startContainer;
					if (start.parentNode && start.parentNode.getAttribute('marker'))
						start = start.parentNode;
					var end = range.endContainer;
					if (end.parentNode && end.parentNode.getAttribute('marker'))
						end = end.parentNode;
					var segments = [];
					var currentSegment = [];
					var selecting = false;

					function extractText(node) {
						for (var i = 0; i < node.childNodes.length; i++) {
							if (node.childNodes[i] == start || node.childNodes[i] == end) {
								if (!selecting) {
									segments.push(currentSegment);
									if (start == end) {
										currentSegment = [node.childNodes[i]]; //which is also start,end
										segments.push(currentSegment);
										currentSegment = [];
										continue;
									}
									currentSegment = [];
									selecting = true;
								} else {
									currentSegment.push(node.childNodes[i]); //which is the logical 'end'(it can be start or end coz the selection direction may be reversed)
									segments.push(currentSegment);
									currentSegment = [];
									selecting = false;
									continue;
								}
							}
							if (node.childNodes[i].nodeType == 3)
								currentSegment.push(node.childNodes[i]);
							else if (node.childNodes[i].getAttribute('marker'))
								currentSegment.push(node.childNodes[i]);
							else if (!node.childNodes[i].getAttribute('exclude'))
								extractText(node.childNodes[i]);
						}
					}
					extractText(this.cdoc().body);
					segments.push(currentSegment);

					var selected = '';

					if (start.tagName && start.tagName.toLowerCase() == 'body') {
						selected = this.assembleComplex(segments[0]);
						return {
							from: 0,
							length: selected.length,
							actionId: Ext.data.IdGenerator.get('uuid').generate()
						};
					}
					var pre = this.assembleComplex(segments[0]);
					post = '';
					//in ff the body element may become the endNode if the range of the drag action is wider than the actual content
					if (end.tagName && end.tagName.toLowerCase() == 'body') {
						var list = [];
						for (var i = 0; i < range.endOffset; i++)
							list.push(end.childNodes[i])
						return {
							from: pre.length,
							length: this.assembleComplex(list).length - pre.length,
							actionId: Ext.data.IdGenerator.get('uuid').generate()
						}

					}
					post = this.assembleComplex(segments[2]);
					var prePlus = '';
					var postPlus = '';
					if (segments[1].length == 1) {
						var prePartialResult = this.parsePartialStartNode(segments[1][0], range.startOffset);
						var postPartialResult = this.parsePartialEndNode(segments[1][0], range.endOffset);
						pre = pre + prePartialResult.pre
						selected = (segments[1][0].nodeType != 3) ? prePartialResult.selected : this.getNodeText(segments[1][0]).substring(range.startOffset, range.endOffset);
						post = postPartialResult.post + post;
					} else {
						var partialStart = segments[1][0];
						var partialEnd = segments[1][segments[1].length - 1];
						var prePartialResult = this.parsePartialStartNode(partialStart, range.startOffset);
						var postPartialResult = this.parsePartialEndNode(partialEnd, range.endOffset);
						var midSelected = this.assembleComplex(segments[1].slice(1, segments[1].length - 1));
						pre = pre + prePartialResult.pre
						selected = prePartialResult.selected + midSelected + postPartialResult.selected;
						post = postPartialResult.post + post;
					}
					// if (!Ext.isIE8m) {
					// 	console.info('PRE----------------------------');
					// 	console.info(pre);
					// 	console.info('SEL----------------------------');
					// 	console.info(selected);
					// 	console.info('POST---------------------------');
					// 	console.info(post);
					// }

					return {
						from: pre.length,
						length: selected.length,
						actionId: Ext.data.IdGenerator.get('uuid').generate()
					};
				},
				markup: function(action, eOpt) {
					var selection = this.getSelection(action == 'parseFromEndOfLine' || action == 'stopParseAtStartOfLine' || eOpt && (eOpt.type == "beginOfLine" || eOpt.type == "endOfLine"), action == 'table');
					if (!selection)
						return;
					var markup = Ext.applyIf((eOpt || {}), this.computeMarkup(selection));
					markup.action = action;
					this.eventEmitter.fireEvent('addMarkup', this, markup);
					selection.removeAllRanges()
				}
			},
			html: '<iframe spellcheck="false" autocomplete="off" frameborder="0" style="width:100%;height:100%;border:1px solid #999;line-height: 0;" src=""></iframe>',
			markups: '@{markups}',
			sample: '@{sample}',
			parserType: '@{type}',
			setParserType: function(type) {
				this.type = type;
			},
			setSample: function(sample) {
				this.sample = sample;
				if (this.parser.inited)
					this.parser.update(sample, this.markups);
			},
			setMarkups: function(markups) {
				this.markups = markups;
				if (this.parser.inited)
					this.parser.update(this.sample, markups);
			},
			listeners: {
				afterrender: function() {
					var iframe = this.el.query('iframe')[0];
					iframe.src = '/resolve/wiki/editorstub.jsp?' + actualView.editorJspQueryString + '&' + clientVM.CSRFTOKEN_NAME + '=' + clientVM.getPageToken('wiki/editorstub.jsp');
					var me = this;
					me.parser.eventEmitter = this;
					if (iframe.attachEvent)
						iframe.attachEvent('onload', function() {
							me.parser.initParser(iframe);
						});
					else if (iframe.addEventListener)
						iframe.addEventListener('load', function() {
							me.parser.initParser(iframe);
						}, false);
				},
				addMarkup: '@{addMarkup}',
				sampleChanged: '@{sampleChanged}',
				sampleTooLong: '@{sampleTooLong}',
				noSelectionError: '@{noSelectionError}',
				createTip: '@{createTip}',
				captureColumn: '@{captureColumn}'
			}
		}
	}
};
Ext.define('RS.wiki.resolutionbuilder.SpecialCharSuggestionCombo', {
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.rsspecialchar',
	queryMode: 'local',
	specStart: String.fromCharCode(167),
	specEnd: String.fromCharCode(167),
	tpl: Ext.create('Ext.XTemplate',
		'<tpl for=".">',
		'<div class="x-boundlist-item">{name}</div>',
		'</tpl>'
	),
	valueField: 'value',
	displayField: 'value',
	valueNotFoundText: 'Not Found',
	initComponent: function() {
		this.callParent([arguments]);
		this.store = Ext.create('Ext.data.Store', {
			fields: ['name', 'value'],
			proxy: {
				type: 'memory'
			}
		});
		this.specChars = this.prepareSpecial();
		this.store.add(this.specChars);
		this.on('change', function() {
			this.doSpecCharFilter();
		}, this)
	},
	onListSelectionChange: function(list, selectedRecords) {
		var me = this,
			isMulti = me.multiSelect,
			hasRecords = selectedRecords.length > 0;


		if (!me.ignoreSelection && me.isExpanded) {
			if (!isMulti) {
				Ext.defer(me.collapse, 1, me);
			}

			if (isMulti || hasRecords) {
				var value = me.getValue() || '';
				if (!value)
					me.setValue(selectedRecords, false);
				else {
					Ext.each(selectedRecords, function(r) {
						value += r.get('value');
					});
					me.setValue(value, false);
				}
			}
			if (hasRecords) {
				me.fireEvent('select', me, selectedRecords);
			}
			me.inputEl.focus();
		}
	},
	doSpecCharFilter: function() {
		var value = this.getValue() || '';
		var available = [];
		Ext.each(this.specChars, function(c) {
			if (value.indexOf(c.value) == -1)
				available.push(c.value);
		});
		this.store.clearFilter(true);
		this.store.filterBy(function(rec) {
			if (available.indexOf(rec.get('value')) == -1)
				return false
			return true
		});
	}
});