glu.defView('RS.wiki.resolutionbuilder.Assessor', {
	title: '~~assess~~',
	collapsible: true,
	collapsed: true,
	animCollapse: false,
	hideCollapseTool: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		flex: 1
	},
	defaultType: 'panel',
	tools: [{
		type: 'down',
		handler: function() {
			this.up().up().fireEvent('togglePanel');
		}
	}, {
		hidden: true,
		type: 'up',
		handler: function() {
			this.up().up().fireEvent('togglePanel');
		}
	}],
	dockedItems: [{
		xtype: 'toolbar',
		items: ['->', 'assessDefinition', 'assessCode']
	}],
	items: [{
		defaults: {
			flex: 1,
			height: 250,
			layout: {
				type: 'vbox',
				align: 'stretch'
			}
		},
		hidden: '@{!assessDefinitionIsPressed}',
		items: [{
			xtype: '@{severityRules}'
		}, {
			xtype: '@{conditionRules}',
			padding: '0 0 10 0'
		}]
	}, {
		hidden: '@{!assessCodeIsPressed}',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'checkboxfield',
			hideLabel: true,
			boxLabel: '~~assessorAutoGenEnabled~~',
			name: 'assessorAutoGenEnabled',
			listeners: {
				render: function() {
					this.getEl().on('click', function() {
						this.fireEvent('turnOnAutoGen')
					}, this);
				},
				turnOnAutoGen: '@{turnOnAutoGen}'
			}
		}, {
			xtype: 'AceEditor',
			minHeight: 300,
			parser: 'groovy',
			flex: 1,
			readOnly: '@{assessorAutoGenEnabled}',
			name: 'assessorScript',
			listeners: {
				render: function() {
					this.getEl().on('keydown', function() {
						if (this.readOnly) {
							this.fireEvent('turnOffAutoGen');
							event.preventDefault()
						}
					}, this)
				},
				turnOffAutoGen: '@{turnOffAutoGen}'
			}
		}]
	}],
	togglePanel: function() {
		if (this.getCollapsed()) {
			this.expand();
			this.removeCls('rs-rb-section-collapsed');
			this.down('*[type="down"]').hide();
			this.down('*[type="up"]').show();
		} else {
			this.collapse();
			this.addCls('rs-rb-section-collapsed');
			this.down('*[type="down"]').show();
			this.down('*[type="up"]').hide();
		}
	},
	listeners: {
		togglePanel: function() {
			this.togglePanel();
		},
		render: function() {
			this.addCls('rs-rb-section-collapsed');
			this.getHeader().on('click', function() {
				this.togglePanel();
			}, this);
		}
	}
});