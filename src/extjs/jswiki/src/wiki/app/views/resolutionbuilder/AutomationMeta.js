glu.defView('RS.wiki.resolutionbuilder.AutomationMeta', {
	xtype: 'panel',
	disabled: '@{masked}',
	padding : '10 0',
	layout: 'border',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		style: 'padding: 10px 0px 10px 15px',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{title}'
		} /*, '->', 'generalTab', 'tasksTab'*/ ]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar actionBar-form',
		padding: '0 0 10 15',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',
		}, {
			name: 'saveGeneral',
			listeners: {
				doubleClickHandler: '@{saveGeneralAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, 'generate', '->', {
			iconCls: 'rs-social-button icon-play',
			tooltip: '~~executionTooltip~~',
			disabled: '@{!executionIsEnabled}',
			handler: function(button) {
				button.fireEvent('execution', button, button)
			},
			listeners: {
				execution: '@{execution}'
			}
		}, {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	items: [{
		region: 'center',
		xtype: 'panel',
		layout: 'card',
		activeItem: '@{activeItem}',
		items: '@{tabs}'
	}]
});

glu.defView('RS.wiki.resolutionbuilder.StageIndicator', {
	layout: 'hbox',
	bodyPadding: '10',
	items: [{
		html: '<img src="/resolve/images/loading.gif">&nbsp;&nbsp;&nbsp;@{stage}'
	}],
	asWindow: {
		baseCls: Ext.isIE8m ? 'x-window' : 'x-panel',
		ui: Ext.isIE8m ? 'default' : 'sysinfo-dialog',
		title: '@{title}',
		width: 500,
		height: 100,
		modal: true
	}
});

glu.defView('RS.wiki.resolutionbuilder.ResolutionBuilderRunbookNamer', {
	xtype: 'form',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10',
	defaultType: 'textfield',
	items: [{
		xtype: 'documentfield',
		trigger2Cls: '',
		fieldLabel: '~~namespace~~',
		value: '@{namespace}',
		store: '@{namespaceStore}',
		queryMode: 'remote',
		displayField: 'unamespace',
		valueField: 'unamespace',
		allowBlank: false,
		regex: /^[\w|\-]+$/,
		regexText: '~~invalidNamespace~~'
	}, {
		xtype: 'textfield',
		fieldLabel: '~~name~~',
		value: '@{name}',
		regex: /^[\w|\-]+[\w|\-| ]*$/,
		regexText: '~~invalidName~~',
		allowBlank: false
	}],

	buttonAlign: 'left',
	buttons: [{
		name: 'create',
		formBind: true
	}, 'cancel'
	],
	asWindow: {
		title: '~~newRunbook~~',
		modal: true,
		width: 400
	}
});
