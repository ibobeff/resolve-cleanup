glu.defView('RS.wiki.resolutionbuilder.ArgumentPicker', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10',
	defaults: {
		editable: false,
		displayField: 'name',
		valueField: 'value'
	},
	items: [{
		xtype: 'combo',
		name: 'variableSource',
		labelWidth: 130,
		store: '@{variableSourceStore}'
	}, {
		xtype: 'fieldcontainer',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaultType: 'combo',
		defaults: {
			labelWidth: 130,
			displayField: 'name',
			valueField: 'name'
		},
		items: [{
			name: 'variable',
			store: '@{globalVariableStore}',
			hidden: '@{!variableFromWSDATAOrFlow}',
			queryMode: 'local',
			tpl: Ext.create('Ext.XTemplate',
				'<tpl for=".">',
				'<div class="x-boundlist-item"><tpl if="color"><span style="background-color:{color}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;</tpl>{name}<tpl if="task">&nbsp; from:{task}</tpl></div>',
				'</tpl>'
			),
			listeners: {
				focus: '@{updateVariables}'
			}
		}, {
			name: 'variable',
			store: '@{paramStore}',
			queryMode: 'local',
			hidden: '@{!variableFromParam}'
		}, {
			name: 'variable',
			store: '@{propertyStore}',
			pageSize: 10,
			minChars: 0,
			displayField: 'uname',
			valueField: 'uname',
			hidden: '@{!variableFromProperty}'
		}, {
			xtype: 'textfield',
			name: 'variable',
			hidden: '@{!variableFromConst}'
		}]
	}],
	buttonAlign: 'left',
	buttons: ['select', 'cancel'],
	asWindow: {
		modal: true,
		title: '~~argument~~',
		width: 500
	}
});