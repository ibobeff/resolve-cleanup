glu.defView('RS.wiki.resolutionbuilder.XmlParser', {
	parentLayout: 'baseParser',
	templateActions: ['clearSample'],
	renderer: {
		flex: 1,
		minHeight: 450,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			flex: 1,
			html: '<iframe spellcheck="false" autocomplete="off" allowBackspace=true frameborder="0" style="width:100%;height:100%;border:1px solid #999;line-height: 0;" src=""></iframe>',
			parserType: 'xml',
			//this is purely for the ._vm attribute
			sample: '@{sample}',
			iframe: null,
			win: null,
			parser: {
				cdoc: function() {
					return this.win.document;
				},
				preTag: function(tagName, attrs, pnid, xpathSelections, currentColor) {
					var kv = [];
					for (var i = 0; i < attrs.length; i++) {
						if (attrs[i].name == 'pnid')
							continue;
						kv.push(Ext.String.format(
							'<span class="xmlSp attribute"{2}><span class="xmlSp attrName">{0}</span>=<span class="xmlSp attrValue">"{1}"</span></span>',
							attrs[i].name,
							Ext.String.escape(attrs[i].value).replace(/"/g, '\\"'),
							this.isNodeSelectedByXPath(attrs[i], xpathSelections) ? this.getSelectedStyle(currentColor) : ''
						));
					}

					return Ext.String.format(
						'<span class="xmlSp tag preTag">{0}</span><span pnid="{4}" class="xmlSp tagName">{1}</span>{3}<span class="xmlSp tag preTag">{2}</span>',
						Ext.htmlEncode('<'),
						tagName,
						Ext.htmlEncode('>'),
						kv.length > 0 ? ' ' + kv.join(' ') : '',
						pnid
					);
				},
				postTag: function(tagName, id) {
					return Ext.String.format(
						'<span class="xmlSp tag postTag">{0}</span><span pnid="{3}" class="xmlSp tagName">{1}</span><span class="xmlSp tag postTag">{2}</span>',
						Ext.htmlEncode('</'),
						tagName,
						Ext.htmlEncode('>'),
						id
					);
				},
				isNodeSelectedByXPath: function(node, xpathSelections) {
					return xpathSelections.indexOf(node) != -1
				},
				getSelectedStyle: function(color) {
					return ' highlighted style="background-color:' + color + '"';
				},
				renderNode: function(node, indent, xpathSelections, currentColor) {
					var indentSpan = [];
					indentSpan = indentSpan.join('');
					var id = Ext.data.IdGenerator.get('uuid').generate();
					node.pnid = id;
					if (Ext.isFunction(node.setAttribute))
						node.setAttribute('pnid', id);
					var str = '';
					switch (node.nodeType) {
						case 1:
							if (node.ignore) {
								str = Ext.String.format(
									'<span pnid="{0}" class="xmlSp rootText">{1}</span>',
									id,
									Ext.htmlEncode(Ext.String.format('<{0}>{1}</{0}>', node.tagName, node.innerHTML)));
								break;
							}
							var text = [];
							if (node.tagName != 'resolveparserroot' && node.tagName != 'error')
								text.push(indentSpan + this.preTag(node.tagName, node.attributes, id, xpathSelections, currentColor));
							if (node.tagName != 'error')
								Ext.each(node.childNodes, function(child) {
									text.push(this.renderNode(child, indent + 1, xpathSelections, currentColor));
								}, this)
							else
								text.push(Ext.htmlEncode(node.textContent))
							if (node.tagName != 'resolveparserroot' && node.tagName != 'error')
								text.push(indentSpan + this.postTag(node.tagName, id));
							var textStyle = '';
							if (node.tagName == 'error')
								textStyle = 'style="color:red"';
							else if (this.isNodeSelectedByXPath(node, xpathSelections))
								textStyle = this.getSelectedStyle(currentColor);
							str = Ext.String.format(
								'<span pnid="{0}" class="xmlSp element"{2}>{1}</span>',
								id,
								text.join(''),
								textStyle);
							break;
						case 3:
							str = Ext.htmlEncode(node.textContent)
								.replace(/ /g, '<span class="xmlSp space"></span>')
								.replace(/\n/g, '<span class="xmlSp newline"></span><br>');
							str = Ext.String.format(
								'<span pnid="{0}" class="xmlSp {2}"{3}>{1}</span>',
								id,
								str, (node.parentNode.tagName != 'resolveparserroot' && node.parentNode.tagName != 'error' ? 'text' : 'rootText'),
								this.isNodeSelectedByXPath(node, xpathSelections) ? this.getSelectedStyle(currentColor) : '');
							break;
						case 4:
							str = Ext.htmlEncode(node.textContent)
								.replace(/ /g, '<span class="xmlSp space"></span>')
								.replace(/\n/g, '<span class="xmlSp newline"></span><br>');
							str = Ext.String.format(
								'<span pnid="{0}" class="xmlSp cdata"{2}>{1}</span>',
								id,
								str,
								this.isNodeSelectedByXPath(node, xpathSelections) ? this.getSelectedStyle(currentColor) : '');
							break;
						case 9:
							var text = [];
							Ext.each(node.childNodes, function(child) {
								text.push(this.renderNode(child, 0, xpathSelections, currentColor));
							}, this)
							str = text.join('');
							break;
					}
					return str;
				},
				toggleElement: function(node, on) {
					Ext.fly(node.parentNode)[on ? 'addCls' : 'removeCls']('emphasize-xml');
				},
				toggleText: function(node, on) {
					Ext.fly(node)[on ? 'addCls' : 'removeCls']('emphasize-xml');
				},
				toggleAttr: function(node, on) {
					Ext.fly(node)[on ? 'addCls' : 'removeCls']('emphasize-xml');
				},
				toggleCDATA: function(node, on) {
					Ext.fly(node)[on ? 'addCls' : 'removeCls']('emphasize-xml');
				},
				liveXml: function() {
					var nodes = Ext.fly(this.cdoc().body).query('.xmlSp')
					Ext.each(nodes, function(node) {
						Ext.fly(node).on('mouseenter', function() {
							if (Ext.fly(node).hasCls('tagName'))
								this.toggleElement(node, true);
							else if (Ext.fly(node).hasCls('text'))
								this.toggleText(node, true);
							else if (Ext.fly(node).hasCls('attribute'))
								this.toggleAttr(node, true);
							else if (Ext.fly(node).hasCls('cdata'))
								this.toggleCDATA(node, true);
						}, this);
						Ext.fly(node).on('click', function() {
							if (Ext.fly(node).hasCls('tagName'))
								this.eventEmitter.fireEvent('nodeSelectionChanged', node, 'element');
							else if (Ext.fly(node).hasCls('text'))
								this.eventEmitter.fireEvent('nodeSelectionChanged', node, 'text');
							else if (Ext.fly(node).hasCls('attribute'))
								this.eventEmitter.fireEvent('nodeSelectionChanged', node, 'attribute');
							else if (Ext.fly(node).hasCls('cdata'))
								this.eventEmitter.fireEvent('nodeSelectionChanged', node, 'cdata');
						}, this);
						Ext.fly(node).on('mouseleave', function() {
							if (Ext.fly(node).hasCls('tagName'))
								this.toggleElement(node, false);
							else if (Ext.fly(node).hasCls('text'))
								this.toggleText(node, false);
							else if (Ext.fly(node).hasCls('attribute'))
								this.toggleAttr(node, false);
							else if (Ext.fly(node).hasCls('cdata'))
								this.toggleCDATA(node, false);
						}, this);
					}, this);
				},
				update: function() {
					var html = this.renderNode(this.eventEmitter._vm.currentSampleDom, 0, [], 'white');
					this.cdoc().body.innerHTML = '<pre>' + html + '</pre>';
					this.liveXml();
				},
				highLightSelections: function(xpathSelections, currentColor, scroll) {
					var html = this.renderNode(this.eventEmitter._vm.currentSampleDom, 0, xpathSelections, currentColor);
					var doc = this.cdoc();
					doc.body.innerHTML = '<pre>' + html + '</pre>';
					var bodyXY = Ext.fly(doc.body).getXY();
					if (scroll) {
						var first = Ext.fly(doc.body).query('*[highlighted]')[0];
						var offset = Ext.fly(first).getY() - bodyXY[1];
						Ext.fly(doc.body).scrollTo('top', offset);
					}
					this.liveXml();
				},
				initParser: function(iframe) {				
					this.iframe = iframe;
					this.win = iframe.contentWindow;

					var doc = iframe.contentWindow.document;
					// IE 8 getSelection() missing object and properties simple hack
					if (this.win.getSelection == undefined) { // IE?
						this.eventEmitter.disable();
						var me = this;
						var wgS = setInterval(function() { // wait for Rangy 
							if (rangy.initialized) {
								clearTimeout(wgS); // exit 
								this.eventEmitter.enable();
							}
						}, 10);
					}
					//ie8 fires load twice..don't know why..
					var head = doc.head || doc.getElementsByTagName('head')[0];
					if (!head)
						return;
					doc.designMode = 'on';
					if ('spellcheck' in doc.body || {})
						doc.body.spellcheck = false;
					Ext.fly(doc).on('keydown', function(e) {
						if (!e.ctrlKey)
							e.preventDefault()
					});
					//need to pay attention to the last return,, need an extra return to make the last return selectable;
					//need to handle /r
					var me = this;
					if (!doc.body)
						return;
					doc.body.allowBackspace = true;
					doc.body.onpaste = function(e) {
						doc.body.innerHTML = '';
						//again, for IE8
						var rawData = e && e.clipboardData ? e.clipboardData.getData('text/plain') : window.clipboardData.getData('Text');
						var lines = rawData.split(/\r*\n+/);
						var data = lines.join(Ext.is.Windows ? '\r\n' : '\n');
						me.eventEmitter.fireEvent('sampleChanged', me, data);
						me.eventEmitter.up().up().up().up().fireEvent('scrollParserProperly');
						return false;
					}

					Ext.fly(doc).on('click', function() {
						var parentPanel = this.eventEmitter.up().up();
						Ext.each(parentPanel.query('button[menu]'), function(btn) {
							if (btn.menu)
								btn.menu.hide();
						})
					}, this);
					this.inited = true;
				}
			},
			listeners: {
				afterrender: function() {
					var iframe = this.el.query('iframe')[0];
					iframe.src = '/resolve/wiki/editorstub.jsp?type=xml&' + clientVM.CSRFTOKEN_NAME + '=' + clientVM.getPageToken('wiki/editorstub.jsp');

					var me = this;
					var vm = this.up().up()._vm;
					me.parser.eventEmitter = this;
					if (iframe.attachEvent)
						iframe.attachEvent('onload', function() {
							me.parser.initParser(iframe);
							me.parser.update(vm.currentSampleDom);
						});
					else if (iframe.addEventListener)
						iframe.addEventListener('load', function() {
							me.parser.initParser(iframe);
							me.parser.update(vm.currentSampleDom);
						}, false);
					vm.on('renderSample', function() {
						this.parser.update(vm.currentSampleDom);
					}, this);
					this.up().up()._vm.on('highLightSelections', function(xpathSelections, currentColor, scroll) {
						this.parser.highLightSelections(xpathSelections, currentColor, scroll);
					}, this);
				},
				nodeSelectionChanged: function(node, type) {
					console.info(node.getAttribute('pnid'));
					var current = node;
					while (!Ext.fly(current).hasCls('element'))
						current = current.parentNode;
					this.fireEvent('selectXPath', this, type, current.getAttribute('pnid'), node);
				},
				selectXPath: '@{selectXPath}',
				sampleChanged: '@{sampleChanged}'
			}
		}, {
			xtype: 'displayfield',
			fieldLabel: '~~xpath~~',
			value: ''
		}, {
			xtype: 'fieldcontainer',
			layout: 'hbox',
			items: [{
				xtype: 'textarea',
				hideLabel: true,
				hidden: '@{!editting}',
				flex: 1,
				name: 'textXPath',
				listeners: {
					keyup: '@{selectionToManual}',
					contentPasted: '@{contentPasted}',
					render: function() {
						this.getEl().on('paste', function(evt) {
							this.on('change', function() {
								this.fireEvent('contentPasted', this, this.getValue())
							}, this, {
								single: true
							})
						}, this)
					}
				}
			}, {
				augmentedXPath: '@{augmentedXPath}',
				minHeight: 100,
				flex: 1,
				style: 'border:1px solid grey; padding:5px;',
				hidden: '@{editting}',
				cookPredicates: function(augmentedToken) {
					var predicates = augmentedToken.predicates;
					var pnid = augmentedToken.pnid;
					var tokens = [];
					Ext.each(predicates, function(p) {
						if (p.type == 'text')
							tokens.push(this.cookTextPredicate(p, pnid));
						else if (p.type == 'attribute')
							tokens.push(this.cookAttributePredicate(p, pnid));
					}, this);
					return Ext.String.format('<span pnid="{1}" ptid="predicates" class="xpath-predicate">&#91;{0}&#93;</span>', tokens.join('&nbsp;' + augmentedToken.op + '&nbsp;'), pnid);
				},
				cookTextPredicate: function(predicate) {
					return Ext.String.format(
						'<span pnid="{2}" ptid="{3}" class="xpath-predicate-text">{0}(., \'{1}\')</span>',
						predicate.condition,
						Ext.htmlEncode(predicate.value),
						predicate.pnid,
						predicate.ptid
					);
				},
				cookAttributePredicate: function(predicate) {
					return Ext.String.format(
						'<span pnid="{3}" ptid="{4}" class="xpath-predicate-attribute">@{0} {1} {2}</span>',
						predicate.name,
						predicate.condition, (predicate.isNumber ? predicate.value : Ext.htmlEncode('"' + predicate.value + '"')),
						predicate.pnid,
						predicate.ptid
					);
				},
				cookCustomPredicate: function(predicate) {
					return Ext.String.format('<span pnid="{1}" ptid="{2}" class="xpath-predicate-customized">{0}</span>', predicate.predicate, predicate.pnid, predicate.ptid);
				},
				cookToken: function(augmentedToken) {
					if (augmentedToken.type == 'selector')
						return this.cookSelector(augmentedToken);
					var predicate = augmentedToken.predicates.length == 0 ? '' : this.cookPredicates(augmentedToken);
					return Ext.String.format(
						'&#47;<span pnid={2} ptid={3} class="xpath-element">{0}</span>{1}',
						augmentedToken.element,
						predicate,
						augmentedToken.pnid,
						augmentedToken.ptid
					);
				},
				cookSelector: function(augmentedToken) {
					if (augmentedToken.selection == 'attribute')
						return Ext.String.format('<span ptid={0} class="xpath-selector-attribute">{1}</span>', augmentedToken.ptid, '/&#64;' + augmentedToken.name);
					else
						return Ext.String.format('/<span ptid={0} class="xpath-selector-text">text()<span>', augmentedToken.ptid, augmentedToken.name);
				},
				liveXPath: function() {
					var list = this.getEl().query('*[class*="xpath"]');
					var me = this;
					Ext.each(list, function(li) {
						Ext.get(li).on('mouseenter', this.showInLineTool, this);
						Ext.get(li).on('mouseleave', this.hideInlineTool, this);
						Ext.get(li).on('click', this.showDetail, this);
					}, this);
				},
				showInLineTool: function(evt, dom) {
					if (!this.quickTool) {
						var span = document.createElement('span');
						span.innerHTML = '<span class="icon-remove-sign" style="color:#999;position:absolute;">&nbsp;</span>'
						this.quickTool = span;
						Ext.fly(this.quickTool).on('click', this.removeToken, this);
					}
					var currentWidth = 0;
					var currentHeight = 0;
					var x = 0;
					var y = 0;
					with(Ext.fly(dom)) {
						currentWidth = getWidth();
						currentHeight = getHeight();
						x = getX() + currentWidth - 5;
						y = getY() + currentHeight - 22;
						setStyle('z-index', 900);
					}
					if (this.quickTool.parentNode)
						this.quickTool.parentNode.removeChild(this.quickTool);
					dom.appendChild(this.quickTool);
					with(Ext.fly(this.quickTool)) {
						setXY([x, y]);
						show();
						setStyle('z-index', 999);
					}
				},
				hideInlineTool: function(evt, dom) {
					var parent = Ext.fly(dom).parent();
					if (parent && parent.dom.getAttribute('class').indexOf('xpath-') != -1) {
						this.showInLineTool(this, Ext.fly(dom).parent().dom);
						return;
					}
					Ext.fly(this.quickTool).hide();
				},
				showDetail: function(evt, dom) {
					evt.stopEvent(); //so that the predicate container span won't receive the click event
					if (Ext.fly(evt.target).hasCls('icon-remove-sign')) {
						this.removeToken(evt, dom)
						return;
					}
					var parentTokenNode = dom;
					var ptid = '';
					while (parentTokenNode.parentNode && !(ptid = parentTokenNode.getAttribute('ptid')))
						parentTokenNode = parentTokenNode.parentNode
					if (!ptid)
						return;
					var pnid = parentTokenNode.getAttribute('pnid');
					var height = Ext.fly(dom).getHeight();
					var x = Ext.fly(dom).getX();
					var y = Ext.fly(dom).getY();
					var parser = this.up().up().down('*[parserType="xml"]');
					this.fireEvent('showDetail', this, pnid, ptid, function(wnd) {
						var bodyHeight = Ext.getBody().getHeight();
						var bodyWidth = Ext.getBody().getWidth();
						if (x + wnd.getWidth() > bodyWidth)
							x = bodyWidth - wnd.getWidth();
						if (y + wnd.getHeight() > bodyHeight)
							y -= wnd.getHeight();
						else
							y += height;
						return [x, y];
					});
				},
				removeToken: function(evt, dom) {
					evt.stopEvent();
					var parentTokenNode = dom;
					var ptid = '';
					while (parentTokenNode.parentNode && !(ptid = parentTokenNode.getAttribute('ptid')))
						parentTokenNode = parentTokenNode.parentNode
					if (!ptid)
						return;
					var pnid = parentTokenNode.getAttribute('pnid');
					this.fireEvent('removeToken', this, pnid, ptid);
				},
				setAugmentedXPath: function(augmentedXPath) {
					this.augmentedXPath = augmentedXPath;
					if (this.hidden)
						return;
					var tokens = [];
					Ext.each(augmentedXPath, function(augmentedToken) {
						tokens.push(this.cookToken(augmentedToken))
					}, this);
					var beautified = '<div class="xpath">&#47;' + tokens.join('') + '</div>';
					if (!this.getContentTarget())
						return;
					this.getContentTarget().dom.innerHTML = beautified;
					this.liveXPath();
				},
				listeners: {
					removeToken: '@{removeToken}',
					showDetail: '@{showDetail}',
					render: function() {
						this._vm.on('xpathVariableSelectionChanged', function(xpath) {
							this._vm.set('textXPath', xpath);
							this._vm.xpathQuery(true);
						}, this);
					}
				}
			}, {
				xtype: 'toolbar',
				layout: 'vbox',
				items: ['editXPath', 'addXPath', 'updateXPath']
			}]
		}]
	}
});

glu.defView('RS.wiki.resolutionbuilder.XPathVarNamer', {
	width: 300,
	bodyPadding: 10,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		html: '@{xpathVarNameMsg}'
	}, {
		xtype: 'textfield',
		name: 'name'
	}],
	asWindow: {
		modal: true,
		title: '~~xpathVarName~~',
		buttonAlign: 'left',
		buttons: ['dump', 'cancel']
	}
})