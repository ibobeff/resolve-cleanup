glu.defView('RS.wiki.resolutionbuilder.LoopEntryConfig', {
	bodyPadding: '10',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaultType: 'fieldcontainer',
	items: [{
		layout: 'hbox',
		items: [{
			xtype: 'radiofield',
			name: 'type',
			value: '@{fixed}',
			boxLabel: '~~fixed~~',
			width: 100
		}, {
			xtype: 'textfield',
			hideLabel: true,
			flex: 1,
			emptyText: '~~count~~',
			name: 'count'
		}]
	}, {
		layout: 'hbox',
		items: [{
			xtype: 'radiofield',
			name: 'type',
			value: '@{list}',
			boxLabel: '~~list~~',
			width: 100
		}, {
			xtype: 'combo',
			store: '@{sourceStore}',
			name: 'itemSource',
			editable: false,
			hideLabel: true,
			displayField: 'name',
			valueField: 'value',
			padding: '0 5 0 0',
			width: 110
		}, {
			xtype: 'fieldcontainer',
			flex: 1,
			minWidth: 100,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaultType: 'combo',
			defaults: {
				hideLabel: true,
				displayField: 'name',
				valueField: 'name'
			},
			items: [{
				xtype: 'textfield',
				name: 'item',
				hidden: '@{!itemFromConst}'
			}, {
				name: 'item',
				store: '@{globalVariableStore}',
				hidden: '@{!itemFromWSDATAOrFlow}',
				queryMode: 'local',
				tpl: Ext.create('Ext.XTemplate',
					'<tpl for=".">',
					'<div class="x-boundlist-item"><tpl if="color"><span style="background-color:{color}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;</tpl>{name}<tpl if="task">&nbsp; from:{task}</tpl></div>',
					'</tpl>'
				),
				listeners: {
					focus: '@{updateVariables}'
				}
			}, {
				name: 'item',
				store: '@{paramStore}',
				queryMode: 'local',
				hidden: '@{!itemFromParam}'
			}, {
				name: 'item',
				store: '@{propertyStore}',
				pageSize: 10,
				minChars: 0,
				displayField: 'uname',
				valueField: 'uname',
				hidden: '@{!itemFromProperty}'
			}]
		}, {
			padding: '0 0 0 5',
			xtype: 'combo',
			hideLabel: true,
			name: 'itemDelimiter',
			store: '@{delimiterStore}',
			queryMode: 'local',
			editable: false,
			width: 100,
			displayField: 'name',
			valueField: 'value'
		}]
	}],
	asWindow: {
		title: '~~loopEntryConfig~~',
		modal: true,
		width: 680,
		height: 170
	},
	buttonAlign: 'left',
	buttons: ['update', 'cancel']
});