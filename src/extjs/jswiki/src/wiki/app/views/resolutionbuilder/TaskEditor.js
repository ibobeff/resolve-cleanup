glu.defView('RS.wiki.resolutionbuilder.TaskEditor', {
	region: 'center',
	padding: '2',
	autoScroll: true,
	disabled: '@{!hasFocus}',
	defaultType: 'panel',
	defaults: {
		bodyPadding: '10',
		minWidth: 700,
		cls: 'rs-rb-section',
		overCls: 'rs-rb-section-hover',
		anchor: '-10'
	},
	layout: 'anchor',
	items: [{
		xtype: '@{httpCommand}',
		hidden: '@{!..isHTTPTask}'
	}, {
		title: '~~general~~',
		hidden: '@{isHTTPTask}',
		defaults: {
			labelWidth: 130
		},
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'textfield',
			name: 'name'
		}, {
			xtype: 'textarea',
			name: 'description'
		}, {
			xtype: 'textarea',
			name: 'command'
		}, {
			xtype: 'toolbar',
			padding: '0 0 0 135',
			layout: 'hbox',
			items: ['insertCommandArgument']
		}, {
			xtype: 'fieldset',
			collapsible: true,
			collapsed: true,
			title: '~~options~~',
			style: {
				'border-left': 'none',
				'border-right': 'none',
				'border-bottom': 'none',
				'padding-left': '0px',
				'padding-right': '0px'
			},
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				labelWidth: 160
			},
			items: [{
				xtype: 'numberfield',
				name: 'timeout'
			}, {
				xtype: 'numberfield',
				name: 'expectTimeout'
			}, {
				xtype: 'fieldcontainer',
				layout: 'hbox',
				defaultType: 'combo',
				defaults: {
					hideLabel: true,
					displayField: 'name',
					valueField: 'name'
				},
				items: [{
					xtype: 'displayfield',
					labelWidth: 160,
					hideLabel: false,
					fieldLabel: '~~promptSourceName~~',
					value: ''
				}, {
					xtype: 'combo',
					padding: '0 5 0 0',
					editable: false,
					store: '@{sourceStore}',
					name: 'promptSource'
				}, {
					xtype: 'textfield',
					name: 'promptSourceName',
					hidden: '@{!promptSourceIsConst}',
					flex: 1
				}, {
					name: 'promptSourceName',
					store: '@{..globalVariableStore}',
					hidden: '@{!promptSourceIsWSDATAOrFlow}',
					queryMode: 'local',
					tpl: Ext.create('Ext.XTemplate',
						'<tpl for=".">',
						'<div class="x-boundlist-item"><tpl if="color"><span style="background-color:{color}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;</tpl>{name}<tpl if="task">&nbsp; from:{task}</tpl></div>',
						'</tpl>'
					),
					listeners: {
						focus: '@{..updateGlobalVariableStore}'
					},
					flex: 1
				}, {
					name: 'promptSourceName',
					store: '@{..paramStore}',
					queryMode: 'local',
					hidden: '@{!promptSourceIsParam}',
					flex: 1
				}, {
					name: 'promptSourceName',
					store: '@{..propertyStore}',
					pageSize: 10,
					displayField: 'uname',
					valueField: 'uname',
					hidden: '@{!promptSourceIsProperty}',
					flex: 1
				}]
			}, {
				xtype: 'displayfield',
				fieldLabel: '~~inputfile~~'
			}, {
				xtype: 'AceEditor',
				name: 'inputfile',
				height: 100,
				readOnly: true,
				parser: 'groovy',
				listeners: {
					render: function() {
						this.getEl().on('click', function() {
							this.fireEvent('expandInputFile');
						}, this)
					},
					expandInputFile: '@{expandInputFile}'
				}
			}]
		}]
	}, {
		xtype: '@{parser}'
	}, {
		xtype: '@{assessor}'
	}, {
		title: '~~output~~',
		collapsible: true,
		collapsed: true,
		animCollapse: false,
		hideCollapseTool: true,
		tools: [{
			type: 'down',
			handler: function() {
				this.up().up().fireEvent('togglePanel');
			}
		}, {
			hidden: true,
			type: 'up',
			handler: function() {
				this.up().up().fireEvent('togglePanel');
			}
		}],
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaultType: 'panel',
		defaults: {
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				flex: 1
			}
		},
		items: [{
			items: [{
				xtype: 'textarea',
				name: 'summary'
			}, {
				xtype: 'toolbar',
				padding: '0 0 0 105',
				items: ['addSummaryArgs']
			}]
		}, {
			items: [{
				padding: '20 0 0 0',
				xtype: 'textarea',
				name: 'detail'
			}, {
				xtype: 'toolbar',
				padding: '0 0 0 105',
				items: ['addDetailArgs']
			}]
		}],
		togglePanel: function() {
			if (this.getCollapsed()) {
				this.expand();
				this.removeCls('rs-rb-section-collapsed');
				this.down('*[type="down"]').hide();
				this.down('*[type="up"]').show();
			} else {
				this.collapse();
				this.addCls('rs-rb-section-collapsed');
				this.down('*[type="down"]').show();
				this.down('*[type="up"]').hide();
			}
		},
		listeners: {
			togglePanel: function() {
				this.togglePanel();
			},
			render: function() {
				this.addCls('rs-rb-section-collapsed');
				this.getHeader().on('click', function() {
					this.togglePanel();
				}, this);
			}
		}
	}]
});

glu.defView('RS.wiki.resolutionbuilder.INPUTFILEEditor', {
	layout: 'fit',
	items: [{
		xtype: 'AceEditor',
		name: 'inputfile',
		parser: 'groovy'
	}],
	asWindow: {
		title: '~~inputfile~~',
		modal: true,
		buttonAlign: 'left',
		buttons: ['update', 'cancel'],
		listeners: {
			beforeshow: function() {
				this.setWidth(Math.round(Ext.getBody().getWidth() * 0.8));
				this.setHeight(Math.round(Ext.getBody().getHeight() * 0.8));
			}
		}
	}
})