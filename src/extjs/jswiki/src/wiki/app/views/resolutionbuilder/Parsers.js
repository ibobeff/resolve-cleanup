glu.defView('RS.wiki.resolutionbuilder.Parsers', {
	title: '~~parser~~',
	layout: 'card',
	activeItem: '@{activeItem}',
	collapsed: true,
	animCollapse: false,
	tools: [{
		type: 'down',
		handler: function() {
			this.up().up().fireEvent('togglePanel');
		}
	}, {
		hidden: true,
		type: 'up',
		handler: function() {
			this.up().up().fireEvent('togglePanel');
		}
	}],
	dockedItems: [{
		padding: '10 0 0 10',
		xtype: 'fieldcontainer',
		defaults: {
			padding: '0 10 0 0'
		},
		layout: 'hbox',
		defaultType: 'radio',
		items: [{
			boxLabel: '~~text~~',
			name: 'parserRadioType',
			value: '@{textType}'
		}, {
			boxLabel: '~~table~~',
			name: 'parserRadioType',
			value: '@{tableType}'
		}, {
			boxLabel: '~~xml~~',
			name: 'parserRadioType',
			value: '@{xmlType}'
		}]
	}],
	items: '@{parsers}',
	togglePanel: function() {
		if (this.getCollapsed()) {
			this.expand();
			this.removeCls('rs-rb-section-collapsed');
			this.down('*[type="down"]').hide();
			this.down('*[type="up"]').show();
		} else {
			this.collapse();
			this.addCls('rs-rb-section-collapsed');
			this.down('*[type="down"]').show();
			this.down('*[type="up"]').hide();
		}
	},
	listeners: {
		togglePanel: function() {
			this.togglePanel();
		},
		render: function() {
			this.addCls('rs-rb-section-collapsed');
			this.getHeader().on('click', function() {
				this.togglePanel();
			}, this);
		}
	}
});