glu.defView('RS.wiki.resolutionbuilder.PlainTextManualMarkupParser', function() {
	return RS.wiki.resolutionbuilder.views.plainTextParserTemplate({
		parentLayout: 'baseParser',
		editorJspQueryString: 'type=manual',
		whenMarkupDblClick: function() {},
		emphasize: function(span, e) {
			var actionIds = span.getAttribute('actionIds').split(',');
			var logicalConnected = Ext.fly(this.cdoc()).query('span[actionIds*="' + actionIds[actionIds.length - 1] + '"]');
			Ext.each(logicalConnected, function(span, idx) {
				Ext.fly(span).addCls('emphasize-center');
			});
			// if (actionIds.length > 1)
			this.showTip(e, actionIds);
		},
		understate: function(span) {
			var actionIds = span.getAttribute('actionIds').split(',');
			var logicalConnected = Ext.fly(this.cdoc()).query('span[actionIds*="' + actionIds[actionIds.length - 1] + '"]');
			Ext.each(logicalConnected, function(span) {
				Ext.fly(span).removeCls('emphasize-center');
			})
			if (this.tipWindow && !this.tipWindow.pinned)
				this.hideTip();
		},
		templateActions: [{
			name: 'literal',
			handler: function() {
				var parserPanel = this.up().up().up().down('#parser');
				parserPanel.parser.markup('literal', {
					ignore: true
				});
			}
		}, {
			text: '~~type~~',
			menu: {
				xtype: 'menu',
				defaults: {
					handler: function() {
						var parserPanel = this.up().ownerButton.up().up().up().down('#parser');
						parserPanel.parser.markup('type', {
							type: this.name
						});
					}
				},
				items: [
					'ignoreNotGreedy',
					'ignoreRemainder', {
						xtype: 'menuseparator'
					},
					'spaceOrTab',
					'whitespace',
					'notWhitespace', {
						xtype: 'menuseparator'
					},
					'letters',
					'lettersAndNumbers',
					'notLettersOrNumbers',
					'lowerCase',
					'upperCase', {
						xtype: 'menuseparator'
					},
					'number',
					'notNumber', {
						xtype: 'menuseparator'
					},
					'beginOfLine',
					'endOfLine', {
						xtype: 'menuseparator'
					}, {
						text: '~~specialType~~',
						handler: function() {},
						onClick: function() {},
						menu: {
							xtype: 'menu',
							defaults: {
								handler: function() {
									var parserPanel = this.up().up().up().up().up().up().down('*[parserType="text"]');
									parserPanel.parser.markup('type', {
										type: this.name
									});
								}
							},
							items: ['IPv4', /*'IPv6',*/ 'MAC', 'decimal']
						}
					}
				]
			}
		}, {
			name: 'capture',
			randomColor: '@{randomColor}',
			handler: function() {
				var parserPanel = this.up().up().down('#parser');
				parserPanel.parser.markup('capture', {
					inlineStyle: {
						backgroundColor: this.randomColor.color()
					}
				});
			}
		}, {
			text: '~~content~~',
			listeners: {
				menushow: function() {
					var parser = this.up().up().down('#parser');
					var selection = parser.parser.getSelection(true, false);
					if (!selection || selection.getAllRanges().length == 0) {
						this.fireEvent('markBoundaryError');
						this.menu.hide();
						return;
					}
					var markup = parser.parser.computeMarkup(selection);
					this.fireEvent('checkBoundary', this, markup.from);
				},
				checkBoundary: '@{checkBoundary}',
				markBoundaryError: '@{markBoundaryError}'
			},
			menu: {
				xtype: 'menu',
				plain: true,
				defaults: {
					handler: function() {
						var parserPanel = this.up().ownerButton.up().up().up().down('#parser');
						parserPanel.parser.markup(this.name, {
							ignore: true
						});
					}
				},
				items: [{
					name: 'beginParse',
					disabled: '@{!beginParseIsEnabled}'
				}, {
					name: 'parseFromEndOfLine',
					disabled: '@{!parseFromEndOfLineIsEnabled}'
				}, {
					name: 'endParse',
					disabled: '@{!endParseIsEnabled}'
				}, {
					name: 'stopParseAtStartOfLine',
					disabled: '@{!stopParseAtStartOfLineIsEnabled}'
				}]
			}
		}, {
			text: '~~options~~',
			menu: {
				xtype: 'menu',
				plain: true,
				items: [{
					xtype: 'checkboxfield',
					hideLabel: true,
					name: 'repeat',
					boxLabel: '~~repeat~~'
				}]
			}
		}]
	});
});