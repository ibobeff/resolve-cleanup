glu.defView('RS.wiki.resolutionbuilder.ConnectionEntryConfig', {
	buttonAlign: 'left',
	bodyPadding: 10,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		items: [{
			xtype: 'fieldcontainer',
			layout: 'hbox',
			defaultType: 'radiofield',
			defaults: {
				hideLabel: true,
				name: 'types'
			},
			items: [{
				hideLabel: false,
				width: 90,
				xtype: 'displayfield',
				fieldLabel: '~~type~~',
				value: ''
			}, {
				boxLabel: '~~ssh~~',
				padding: '8 10 0 0',
				value: '@{ssh}'
			}, {
				boxLabel: '~~telnet~~',
				padding: '8 10 0 0',
				value: '@{telnet}'
			}, {
				boxLabel: '~~http~~',
				padding: '8 10 0 0',
				value: '@{http}'
			}, {
				boxLabel: '~~local~~',
				padding: '8 10 0 0',
				value: '@{local}'
			}]
		}, '->', 'configTab', 'optionTab']
	}],
	items: [{
		layout: 'card',
		hidden: '@{showOption}',
		activeItem: '@{activeConfig}',
		items: '@{configs}'
	}, {
		layout: 'card',
		hidden: '@{!showOption}',
		activeItem: '@{activeOption}',
		items: '@{options}'
	}],
	buttons: ['update', 'cancel'],
	asWindow: {
		title: '~~connectionEntryConfig~~',
		modal: true,
		width: 600
	}
});