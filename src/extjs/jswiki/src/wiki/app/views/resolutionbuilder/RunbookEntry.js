glu.defView('RS.wiki.resolutionbuilder.RunbookEntry', {
	layout: {
		type: 'vbox',
		pack: 'center',
		align: 'center'
	},
	bodyPadding: '5px',
	style: 'border:2px solid #CDCDCD;border-radius:15px',
	tbar: [{
		xtype: 'tbtext',
		text: '~~runbookEntry~~',
		style: 'font-size:10px'
	}, '->', {
		xtype: 'tool',
		type: 'close',
		handler: function() {
			this.ownerCt.ownerCt.fireEvent('remove', this, this);
		}
	}],
	plugins: [{
		ptype: 'rbentitydragzone'
	}],
	margin: '@{margin}',
	items: [{
		xtype: 'displayfield',
		hideLabel: true,
		name: 'name'
	}],
	disabled: '@{!canSelect}',
	selected: '@{selected}',
	setSelected: function(selected) {
		this.selected = selected;
		if (!this.selected)
			this.getEl().removeCls('entrySelection');
		else
			this.getEl().addCls('entrySelection');
	},
	watch: true,
	listeners: {
		render: function() {
			this.getEl().addCls('runbookEntry');
			this.getEl().on('click', function(e) {
				this.fireEvent('entryClicked', this, e)
			}, this);
			this.getEl().on('mouseleave', function() {
				this.down('toolbar').removeCls('draggedTaskOnTheTop');
				this.removeCls('draggedTaskAtTheBottom');
			}, this);
			this.setSelected(this.selected);
			this.getEl().dom.abCmp = this;
		},
		taskLeave: function() {
			this.down('toolbar').removeCls('draggedTaskOnTheTop');
			this.removeCls('draggedTaskAtTheBottom');
			this.fireEvent('taskLeaveWatcher');
		},
		taskLeaveWatcher: '@{taskLeaveWatcher}',
		taskUpperHalf: function() {
			this.removeCls('draggedTaskAtTheBottom');
			this.down('toolbar').addCls('draggedTaskOnTheTop');
			this.fireEvent('taskEnterUpperHalf');
		},
		taskEnterUpperHalf: '@{taskEnterUpperHalf}',
		taskLowerHalf: function() {
			this.down('toolbar').removeCls('draggedTaskOnTheTop');
			this.addCls('draggedTaskAtTheBottom');
			this.fireEvent('taskEnterLowerHalf');
		},
		taskEnterLowerHalf: '@{taskEnterLowerHalf}',
		entryClicked: '@{entryClicked}',
		remove: '@{remove}'
	}
});