glu.defView('RS.wiki.resolutionbuilder.Tasks', {
	layout: 'border',
	items: [{
		region: 'west',
		xtype: 'panel',
		overflowY: 'auto',
		width: 500,
		dockedItems: [{
			xtype: 'toolbar',
			cls: 'actionBar actionBar-form',
			items: ['addConnection', 'addLoop', 'addParam']
		}],
		split: true,
		layout: {
			xtype: 'vbox',
			align: 'stretch'
		},
		items: [{
			items: '@{entries}'
		}],
		listeners: {
			resize: function() {
				this.doLayout();
			}
		}
	}, {
		region: 'center',
		hidden: '@{..taskEditorIsVisible}',
		xtype: '@{runbookEditor}'
	}, {
		region: 'center',
		hidden: '@{!..taskEditorIsVisible}',
		xtype: '@{taskEditor}'
	}]
});



// {
// 	region: 'center',
// 	padding: '2',
// 	autoScroll: true,
// 	disabled: '@{!hasFocus}',
// 	defaultType: 'panel',
// 	hidden: '@{!taskPanelIsVisible}',
// 	defaults: {
// 		bodyPadding: '10',
// 		minWidth: 700,
// 		cls: 'rs-rb-section',
// 		overCls: 'rs-rb-section-hover',
// 		anchor: '-10'
// 	},
// 	layout: 'anchor',
// 	items: [{
// 		title: '~~general~~',
// 		defaults: {
// 			labelWidth: 130
// 		},
// 		layout: {
// 			type: 'vbox',
// 			align: 'stretch'
// 		},
// 		items: [{
// 			xtype: 'textfield',
// 			name: 'name'
// 		}, {
// 			xtype: 'textarea',
// 			name: 'description'
// 		}, {
// 			xtype: 'textarea',
// 			name: 'command'
// 		}, {
// 			xtype: 'toolbar',
// 			padding: '0 0 0 135',
// 			layout: 'hbox',
// 			items: ['insertCommandArgument']
// 		}, {
// 			xtype: 'fieldset',
// 			collapsible: true,
// 			collapsed: true,
// 			title: '~~options~~',
// 			style: {
// 				'border-left': 'none',
// 				'border-right': 'none',
// 				'border-bottom': 'none',
// 				'padding-left': '0px',
// 				'padding-right': '0px'
// 			},
// 			layout: {
// 				type: 'vbox',
// 				align: 'stretch'
// 			},
// 			defaults: {
// 				labelWidth: 130
// 			},
// 			items: [{
// 				xtype: 'numberfield',
// 				name: 'timeout'
// 			}, {
// 				xtype: 'numberfield',
// 				name: 'timeoutExpected'
// 			}, {
// 				xtype: 'fieldcontainer',
// 				layout: 'hbox',
// 				items: [{
// 					xtype: 'displayfield',
// 					labelWidth: 130,
// 					fieldLabel: '~~promptSourceName~~',
// 					value: ''
// 				}, {
// 					xtype: 'combo',
// 					hideLabel: true,
// 					displayField: 'name',
// 					valueField: 'value',
// 					padding: '0 5 0 0',
// 					editable: false,
// 					store: '@{sourceStore}',
// 					name: 'promptSource'
// 				}, {
// 					xtype: 'fieldcontainer',
// 					flex: 1,
// 					layout: {
// 						type: 'vbox',
// 						align: 'stretch'
// 					},
// 					defaultType: 'combo',
// 					defaults: {
// 						hideLabel: true,
// 						displayField: 'name',
// 						valueField: 'name'
// 					},
// 					items: [{
// 						xtype: 'textfield',
// 						name: 'promptSourceName',
// 						hidden: '@{!promptSourceIsConst}'
// 					}, {
// 						name: 'promptSourceName',
// 						store: '@{globalVariableStore}',
// 						hidden: '@{!promptSourceIsWSDATAOrFlow}',
// 						queryMode: 'local',
// 						tpl: Ext.create('Ext.XTemplate',
// 							'<tpl for=".">',
// 							'<div class="x-boundlist-item"><tpl if="color"><span style="background-color:{color}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;</tpl>{name}<tpl if="task">&nbsp; from:{task}</tpl></div>',
// 							'</tpl>'
// 						),
// 						listeners: {
// 							focus: '@{updateGlobalVariableStore}'
// 						}
// 					}, {
// 						name: 'promptSourceName',
// 						store: '@{paramStore}',
// 						queryMode: 'local',
// 						hidden: '@{!promptSourceIsParam}'
// 					}, {
// 						name: 'promptSourceName',
// 						store: '@{propertyStore}',
// 						pageSize: 10,
// 						displayField: 'uname',
// 						valueField: 'uname',
// 						hidden: '@{!promptSourceIsProperty}'
// 					}]
// 				}]
// 			}]
// 		}]
// 	}, {
// 		xtype: '@{parser}',
// 	}, {
// 		xtype: '@{assessor}'
// 	}, {
// 		title: '~~output~~',
// 		collapsible: true,
// 		collapsed: true,
// 		hideCollapseTool: true,
// 		layout: {
// 			type: 'vbox',
// 			align: 'stretch'
// 		},
// 		defaultType: 'panel',
// 		defaults: {
// 			layout: {
// 				type: 'vbox',
// 				align: 'stretch'
// 			},
// 			defaults: {
// 				flex: 1
// 			}
// 		},
// 		items: [{
// 			items: [{
// 				xtype: 'textarea',
// 				name: 'summary'
// 			}, {
// 				xtype: 'toolbar',
// 				padding: '0 0 0 105',
// 				items: ['addSummaryArgs']
// 			}]
// 		}, {
// 			items: [{
// 				padding: '20 0 0 0',
// 				xtype: 'textarea',
// 				name: 'detail'
// 			}, {
// 				xtype: 'toolbar',
// 				padding: '0 0 0 105',
// 				items: ['addDetailArgs']
// 			}]
// 		}],
// 		listeners: {
// 			render: function() {
// 				this.addCls('rs-rb-section-collapsed');
// 				this.getHeader().on('click', function() {
// 					if (this.getCollapsed()) {
// 						this.expand();
// 						this.removeCls('rs-rb-section-collapsed');
// 					} else {
// 						this.collapse();
// 						this.addCls('rs-rb-section-collapsed');
// 					}
// 				}, this);
// 			}
// 		}
// 	}]
// }