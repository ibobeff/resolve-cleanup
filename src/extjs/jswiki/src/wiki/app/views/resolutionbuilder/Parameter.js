glu.defView('RS.wiki.resolutionbuilder.Parameter', {
	// title: '~~parameter~~',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	overflowY: 'auto',
	items: [{
			xtype: 'fieldcontainer',
			layout: 'hbox',
			defaultType: 'radiofield',
			hidden: '@{!worksheetOptionIsVisible}',
			defaults: {
				name: 'exe',
				padding: '0 10 0 0'
			},
			items: [{
				xtype: 'displayfield',
				labelWidth: 110,
				fieldLabel: '~~worksheet~~',
				value: ''
			}, {
				value: '@{newWorksheet}',
				padding: '8 10 0 0',
				boxLabel: '~~newWorksheet~~'
			}, {
				value: '@{currentWorksheet}',
				padding: '8 10 0 0',
				boxLabel: '~~currentWorksheet~~'
			}]
		},
		/*{
		xtype: 'fieldcontainer',
		hidden: '@{!columnOptionIsVisible}',
		layout: 'hbox',
		defaultType: 'radiofield',
		defaults: {
			name: 'radioNumOfColumns',
			padding: '0 10 0 0'
		},
		items: [{
			xtype: 'displayfield',
			labelWidth: 110,
			fieldLabel: '~~layout~~',
			value: ''
		}, {
			boxLabel: '~~oneColumn~~',
			value: '@{oneColumn}'
		}, {
			boxLabel: '~~twoColumn~~',
			value: '@{twoColumn}'
		}]
	}, */
		{
			xtype: 'container',
			minHeight: 300,
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			flex: 1,
			defaults: {
				flex: 1,
				columns: [{
					header: '~~name~~',
					dataIndex: 'name',
					flex: 3
				}, {
					header: '~~displayName~~',
					dataIndex: 'displayName',
					flex: 1
				}]
			},
			items: [{
				xtype: 'grid',
				cls : 'rs-grid-dark',
				name: 'firstColumn',
				dockedItems : [{
					xtype : 'toolbar',
					cls : 'rs-dockedtoolbar',
					defaults : {
						cls : 'rs-small-btn rs-btn-light'
					},
					items : [{
						name: 'addParam1stGrid',					
						disabled: '@{automationViewTabPressed}'
					}, {
						name: 'removeParams1stGrid'
					}, {
						xtype: 'text',
						text: '~~parameterReadOnlyTxt~~',
						style: {
							fontSize: '10px'
						},
						hidden: '@{!automationViewTabPressed}'
					}]
				}],		
				store: '@{firstColumnStore}',
				viewConfig: {
					markDirty: false,
					plugins: {
						ptype: 'gridviewdragdrop',
						ddGroup: 'param'
					},
					listeners: {
						beforedrop: function(node, data, overModel, dropPosition, dropHandlers) {
							this.ownerCt.fireEvent('beforeDropRec', node, data);
						}
					}
				},
				listeners: {
					beforeedit: function(editor, context) {
						if (context.record.get('id'))
							return false;
						this.oldParamValue = context.value;
					},
					render: function(grid) {
						grid.store.grid = grid;
					},
					editAction: '@{editParam}',
					beforeDropRec: '@{checkDropOn1stColumn}'
				},
				padding: '0 5 0 0'
			}, {
				xtype: 'grid',
				name: 'secondColumn',
				tbar: ['addParam2ndGrid', 'removeParams2ndGrid'],
				store: '@{secondColumnStore}',
				hidden: '@{oneColumn}',
				viewConfig: {
					markDirty: false,
					plugins: {
						ptype: 'gridviewdragdrop',
						ddGroup: 'param'
					},
					listeners: {
						beforedrop: function(node, data, overModel, dropPosition, dropHandlers) {
							this.ownerCt.fireEvent('beforeDropRec', node, data);
						}
					}
				},
				padding: '0 0 0 5',
				listeners: {
					beforeedit: function(editor, context) {
						this.oldParamValue = context.value;
					},
					/*
					render: function(grid) {
						grid.store.grid = grid;
					},
					*/
					editAction: '@{editParam}',
					beforeDropRec: '@{checkDropOn2ndColumn}',
					hide: function() {
						var first = this.up().up().down('grid[name="firstColumn"]');
						first.getView().getPlugin().dragZone.lock();
					},
					show: function() {
						var first = this.up().up().down('grid[name="firstColumn"]');
						first.getView().getPlugin().dragZone.unlock();
					}
				}
			}]
		}, {
			xtype: 'container',
			hidden: '@{!editorIsVisible}',
			items: [{
				xtype: '@{paramEditor}'
			}]
		}
	]
});

glu.defView('RS.wiki.resolutionbuilder.ParamWindow', {
	layout: 'fit',
	bodyPadding: '10',
	items: [{
		xtype: '@{parameter}'
	}],
	asWindow: {
		title: '~~parameter~~',
		width: 800,
		height: 600,
		buttonAlign: 'left',
		buttons: ['save', 'cancel']
	}
});