glu.defView('RS.wiki.resolutionbuilder.IfEntry', {
	tbar: [{
		xtype: 'tbtext',
		text: '~~ifEntry~~',
		style: 'font-size:10px',
		listeners: {
			render: function() {
				this.getEl().on('click', function() {
					var panel = this.up().up().down('*[collapsible]')
					if (panel.items.length == 0)
						return;
					panel[panel.collapsed ? 'expand' : 'collapse']();
				}, this)
			}
		}
	}, '->', {
		name: 'addCondition'
	}, {
		xtype: 'tool',
		type: 'close',
		handler: function() {
			this.ownerCt.ownerCt.fireEvent('remove', this, this);
		}
	}],
	margin: 10,
	items: [{
		collapsible: true,
		header: false,
		items: '@{entries}'
	}],
	selected: '@{selected}',
	setSelected: function(selected) {
		this.selected = selected;
		// if (!this.selected)
		// 	this.getEl().removeCls('entrySelection');
		// else
		// 	this.getEl().addCls('entrySelection');
	},
	watch: true,
	listeners: {
		render: function() {
			this.getEl().addCls('ifEntry');
			this.getEl().dom.abCmp = this;
			// this.getEl().on('click', function(e) {
			// 	this.fireEvent('entryClicked', this, e)
			// }, this);
		},
		taskLeave: function() {
			this.removeCls('draggedTaskOnTheTopOfIf');
			this.down('toolbar').removeCls('draggedTaskOnTheTop');
			this.removeCls('draggedTaskAtTheBottom');
			this.fireEvent('taskLeaveWatcher');
		},
		taskLeaveWatcher: '@{taskLeaveWatcher}',
		taskUpperHalf: function() {
			this.removeCls('draggedTaskAtTheBottom');
			this.addCls('draggedTaskOnTheTopOfIf');
			this.down('toolbar').addCls('draggedTaskOnTheTop');
			this.fireEvent('taskEnterUpperHalf');
		},
		taskEnterUpperHalf: '@{taskEnterUpperHalf}',
		taskLowerHalf: function() {
			this.removeCls('draggedTaskOnTheTopOfIf');
			this.down('toolbar').removeCls('draggedTaskOnTheTop');
			this.addCls('draggedTaskAtTheBottom');
			this.fireEvent('taskEnterLowerHalf');
		},
		taskEnterLowerHalf: '@{taskEnterLowerHalf}',
		entryClicked: '@{entryClicked}',
		remove: '@{remove}'
	}
});