glu.defView('RS.wiki.resolutionbuilder.PlainTextTableMarkupParser', function() {
	return RS.wiki.resolutionbuilder.views.plainTextParserTemplate({
		parentLayout: 'baseParser',
		editorJspQueryString: 'type=table',
		whenMarkupDblClick: function(span) {
			if (span.getAttribute('col') && !this.checkIgnore(span.getAttribute('actionIds'))) {
				Ext.each(this.eventEmitter.up().up().query('button[name="capture"]'), function(btn) {
					if (!btn.up().hidden) {
						btn.showMenu();
						btn.menu.fireEvent('captureColumn', this.eventEmitter, span.getAttribute('col'));
						return false;
					}
				}, this);
			}
		},
		emphasize: function(span, e) {
			var cols = span.getAttribute('col');
			var markup = this.getMarkup('column', cols);
			if (!markup)
				return;
			var spans = Ext.fly(this.cdoc()).query(Ext.String.format('span[col="{0}"]', cols));
			Ext.each(spans, function(span) {
				if (this.checkIgnore(span.getAttribute('actionIds')))
					return;
				Ext.fly(span).addCls('emphasize-center');
			}, this);
		},
		understate: function(span, e) {
			var cols = span.getAttribute('col');
			var markup = this.getMarkup('column', cols);
			if (!markup)
				return;
			var spans = Ext.fly(this.cdoc()).query(Ext.String.format('span[col="{0}"]', cols));
			Ext.each(spans, function(span) {
				if (this.checkIgnore(span.getAttribute('actionIds')))
					return;
				// Ext.fly(span).removeCls('emphasize-left');
				Ext.fly(span).removeCls('emphasize-center');
				// Ext.fly(span).removeCls('emphasize-right');
			}, this);
		},
		templateActions: [{
			text: '~~delimiter~~',
			//wip
			enableToggle: true,
			columnSeparator: '@{columnSeparator}',
			setColumnSeparator: function(columnSeparator) {
				this.columnSeparator = columnSeparator;
				this.specCharWindow.down('rsspecialchar[name="columnSeparator"]').setValue(columnSeparator);
			},
			rowSeparator: '@{rowSeparator}',
			setRowSeparator: function(rowSeparator) {
				this.rowSeparator = rowSeparator;
				this.specCharWindow.down('rsspecialchar[name="rowSeparator"]').setValue(rowSeparator);
			},
			menu: {
				style: 'border:none !important',
				height: 0,
				border: 0
			},
			listeners: {
				columnSeparatorModified: '@{columnSeparatorModified}',
				rowSeparatorModified: '@{rowSeparatorModified}',
				render: function() {
					var me = this;
					//we need to rethink this design and impl..at least we should  use another viewmodel for this
					this.specCharWindow = Ext.create('Ext.window.Window', {
						baseCls: (!Ext.isIE8m ? 'x-panel' : 'x-window'),
						header: false,
						border: false,
						closable: false,
						draggable: false,
						resizable: false,
						width: 400,
						bodyPadding: '10',
						layout: {
							type: 'vbox',
							align: 'stretch'
						},
						defaults: {
							listeners: {
								change: function() {
									me.fireEvent(this.name + 'Modified', this, this.getValue());
								}
							}
						},
						items: [{
							labelWidth: 130,
							xtype: 'rsspecialchar',
							fieldLabel: 'Column Separator',
							name: 'columnSeparator',
							prepareSpecial: function() {
								return [{
									name: 'COMMA',
									value: Ext.String.format('{0}{1}{2}', this.specStart, 'COMMA', this.specEnd)
								}, {
									name: 'SPACE',
									value: Ext.String.format('{0}{1}{2}', this.specStart, 'SPACE', this.specEnd)
								}, {
									name: 'TAB',
									value: Ext.String.format('{0}{1}{2}', this.specStart, 'TAB', this.specEnd)
								}]
							}
						}, {
							labelWidth: 130,
							xtype: 'rsspecialchar',
							fieldLabel: 'Row Separator',
							name: 'rowSeparator',
							prepareSpecial: function() {
								return [{
									name: 'Unix(LF)',
									value: Ext.String.format('{0}{1}{2}', this.specStart, 'Unix(LF)', this.specEnd)
								}, {
									name: 'Win(CR\\LF)',
									value: Ext.String.format('{0}{1}{2}', this.specStart, 'Win(CR\\LF)', this.specEnd)
								}]
							}
						}],
						listeners: {
							hide: function() {
								if (me.pressed)
									me.toggle();
							}
						},
						buttonAlign: 'left',
						buttons: [{
							text: 'Apply',
							handler: function() {
								me.specCharWindow.hide();
								var parserPanel = me.up().up().up().down('*[parserType="table"]');
								parserPanel.parser.markup('table');
							}
						}, {
							text: 'Cancel',
							handler: function() {
								me.specCharWindow.hide();
								if (me.pressed)
									me.toggle();
							}
						}]
					});
					var applyBtn = this.specCharWindow.query('button[text="Apply"]')[0];
					this._vm.on('rowSeparatorChanged', function() {
						var combos = me.specCharWindow.query('combo');
						if (combos[0].getValue() == combos[1].getValue())
							applyBtn.disable();
						else
							applyBtn.enable();
					});
					this._vm.on('columnSeparatorChanged', function() {
						var combos = me.specCharWindow.query('combo');
						if (combos[0].getValue() == combos[1].getValue())
							applyBtn.disable();
						else
							applyBtn.enable();
					})
				},
				toggle: function() {
					this.specCharWindow[this.pressed ? 'show' : 'hide']();
					this.specCharWindow.alignTo(this.getEl(), 'tl-bl');
				}
			}
		}, {
			text: '~~capture~~',
			name: 'capture',
			handler: function() {},
			menu: {
				xtype: 'menu',
				plain: true,
				resizable: true,
				height: 300,
				width: 500,
				onMouseOver: function() {},
				items: [{
					xtype: 'grid',
					canActivate: false,
					store: '@{tblVarStore}',
					name: 'tblVars',
					tbar: ['addTblVar', 'removeTblVars'],
					viewConfig: {
						markDirty: false
					},
					columns: [{
						header: '~~columnNum~~',
						dataIndex: 'columnNum',
						renderer: function(value, meta, record, row, col, store) {
							return Ext.String.format('<span style="background-color:{0}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;{1}',
								record.get('color'), value);
						},
						editor: {
							xtype: 'combo',
							queryMode: 'local',
							editable: false,
							displayField: 'columnNum',
							valueField: 'columnNum',
							store: '@{availableTblColumns}',
							tpl: Ext.create('Ext.XTemplate',
								'<tpl for=".">',
								'<div class="x-boundlist-item" style="background-color:{color} !important">{columnNum}</div>',
								'</tpl>'
							)
						}
					}, {
						header: '~~name~~',
						dataIndex: 'name',
						flex: 1,
						editor: {
							xtype: 'textfield'
						}
					}],
					plugins: [{
						ptype: 'cellediting',
						listeners: {
							edit: function(editor, e) {
								if (e.column.dataIndex != 'name')
									return;
								e.record.set('name', e.record.get('name').toLowerCase());
							}
						}
					}],
					listeners: {
						edit: function(editor, e) {
							this.fireEvent('capturedColumnUpdated', this, e.record)
						},
						capturedColumnUpdated: '@{capturedColumnUpdated}'
					}
				}],
				buttonAlign: 'left',
				buttons: [{
					name: 'applyTblVar',
					handler: function() {
						this.fireEvent('applyTblVar');
						this.up().up().hide();
					},
					listeners: {
						applyTblVar: '@{applyTblVar}'
					}
				}, {
					name: 'cancel',
					handler: function() {
						this.fireEvent('cancelAdd');
						this.up().up().hide();
					},
					listeners: {
						cancelAdd: '@{cancelAdd}'
					}
				}],
				listeners: {
					captureColumn: '@{captureColumn}'
				}
			}
		}, {
			text: '~~content~~',
			listeners: {
				menushow: function() {
					var parser = this.up().up().down('#parser');
					var selection = parser.parser.getSelection(true, false);
					if (!selection || selection.getAllRanges().length == 0) {
						this.fireEvent('markBoundaryError');
						this.menu.hide();
						return false;
					}
					var markup = parser.parser.computeMarkup(selection);
					this.fireEvent('checkBoundary', this, markup.from);
				},
				checkBoundary: '@{checkBoundary}',
				markBoundaryError: '@{markBoundaryError}'
			},
			menu: {
				xtype: 'menu',
				plain: true,
				defaults: {
					handler: function() {
						var parserPanel = this.up().ownerButton.up().up().down('#parser');
						parserPanel.parser.markup(this.name, {
							ignore: true
						});
					}
				},
				items: [{
					name: 'beginParse',
					disabled: '@{!beginParseIsEnabled}'
				}, {
					name: 'parseFromEndOfLine',
					disabled: '@{!parseFromEndOfLineIsEnabled}'
				}, {
					name: 'endParse',
					disabled: '@{!endParseIsEnabled}'
				}, {
					name: 'stopParseAtStartOfLine',
					disabled: '@{!stopParseAtStartOfLineIsEnabled}'
				}]
			}
		}, {
			text: '~~options~~',
			menu: {
				xtype: 'menu',
				plain: true,
				items: [{
					xtype: 'checkboxfield',
					hideLabel: true,
					boxLabel: '~~trim~~',
					name: 'trim'
				}, {
					xtype: 'numberfield',
					width: 180,
					name: 'tabWidth'
				}]
			}
		}],
		renderer: {
			html: 'manual'
		}
	});
});