glu.defView('RS.wiki.resolutionbuilder.LoopEntry', {
	tbar: [{
		xtype: 'tbtext',
		text: '~~loopEntry~~',
		hidden: false,
		style: 'font-size:10px',
		listeners: {
			render: function() {
				this.getEl().on('click', function() {
					var panel = this.up().up().down('*[collapsible]')
					if (panel.items.length == 0)
						return;
					panel[panel.collapsed ? 'expand' : 'collapse']();
				}, this)
			}
		}
	}, '->', {
		name: 'addConnection'
	}, {
		xtype: 'tool',
		type: 'gear',
		handler: function() {
			this.ownerCt.ownerCt.fireEvent('config', this, this);
		}
	}, {
		xtype: 'tool',
		type: 'close',
		handler: function() {
			this.ownerCt.ownerCt.fireEvent('remove', this, this);
		}
	}],
	margin: 10,
	items: [{
		collapsible: true,
		header: false,
		items: '@{entries}'
	}],
	selected: '@{selected}',
	setSelected: function(selected) {
		this.selected = selected;
		// if (!this.selected)
		// 	this.getEl().removeCls('entrySelection');
		// else
		// 	this.getEl().addCls('entrySelection');
	},
	listeners: {
		render: function() {
			this.getEl().addCls('loopEntry');
			this.getEl().dom.abCmp = this;
			// this.getEl().on('click', function(e) {
			// 	this.fireEvent('entryClicked', this, e)
			// }, this);
		},
		entryClicked: '@{entryClicked}',
		remove: '@{remove}',
		config: '@{config}'
	}
});