glu.defView('RS.wiki.resolutionbuilder.General', {
	autoScroll: true,
	bodyPadding: '10',
	type: '',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaultType: 'panel',
	items: [{
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'textfield',
			hidden: '@{isReadOnly}',
			name: 'name'
		}, {
			xtype: 'textfield',
			hidden: '@{isReadOnly}',
			name: 'namespace'
		}, {
			xtype: 'displayfield',
			hidden: '@{!isReadOnly}',
			name: 'name'
		}, {
			xtype: 'displayfield',
			hidden: '@{!isReadOnly}',
			name: 'namespace'
		}, {
			xtype: 'textarea',
			name: 'summary',
			minHeight: 100
		}]
	}, {
		minHeight: 200,
		xtype: '@{parameters}'
	}]
})