glu.defView('RS.wiki.resolutionbuilder.dummy') //to make sure the ns is init before creating the factory
RS.wiki.resolutionbuilder.views.baseParserFactory = function(actualView) {
	var dockedTbBtns = [];
	Ext.each(actualView.templateActions, function(action) {
		var btn = Ext.applyIf(action, {
			hidden: '@{!templateTabIsPressed}'
		});
		dockedTbBtns.push(btn);
	})
	dockedTbBtns = dockedTbBtns.concat([{
		name: 'executeTest',
		hidden: '@{!testTabIsPressed}'
	}, '->', 'templateTab', 'codeTab', 'testTab'])
	var dockedItems = [{
		xtype: 'toolbar',
		items: dockedTbBtns
	}];

	if (actualView.extraTbs)
		dockedItems = dockedItems.concat(actualView.extraTbs)
	return {
		dockedItems: dockedItems,
		items: [{
			layout: 'card',
			activeItem: '@{activeItem}',
			items: [actualView.renderer, {
				xtype: 'panel',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [{
					xtype: 'checkboxfield',
					hideLabel: true,
					boxLabel: '~~parserAutoGenEnabled~~',
					name: 'parserAutoGenEnabled',
					listeners: {
						render: function() {
							this.getEl().on('click', function() {
								this.fireEvent('turnOnAutoGen')
							}, this);
						},
						turnOnAutoGen: '@{turnOnAutoGen}'
					}
				}, {
					xtype: 'AceEditor',
					minHeight: 300,
					parser: 'groovy',
					flex: 1,
					name: 'code',
					readOnly: '@{parserAutoGenEnabled}',
					listeners: {
						render: function() {
							this.getEl().on('keydown', function() {
								if (this.readOnly) {
									this.fireEvent('turnOffAutoGen')
									event.preventDefault()
								}
							}, this);
						},
						turnOffAutoGen: '@{turnOffAutoGen}'
					}
				}]
			}, {
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [{
					xtype: 'displayfield',
					fieldLabel: '~~testData~~',
					value: ''
				}, {
					xtype: 'textarea',
					name: 'testData',
					hideLabel: true,
					flex: 1,
					minHeight: 300,
					listeners: {
						render: function() {
							Ext.fly(this.getEl().query('textarea')[0]).setStyle('font-family', 'monospace');
						}
					}
				}, {
					xtype: 'grid',
					tbar: [{
						xtype: 'displayfield',
						fieldLabel: '~~testResult~~',
						value: ''
					}],
					name: 'testResult',
					maxHeight: 300,
					minHeight: 100,
					plugins: [{
						ptype: 'rowexpander',
						rowBodyTpl: new Ext.XTemplate(
							'<div style="word-wrap: break-word;"><pre>{value}</pre></div>'
						)
					}],
					store: '@{testResultStore}',
					columns: [{
						header: '~~name~~',
						dataIndex: 'name'
					}, {
						header: '~~value~~',
						dataIndex: 'value',
						flex: 1,
						renderer: function(value) {
							return Ext.util.Format.nl2br('<pre>' + Ext.String.htmlEncode(value) + '</pre>')
						}
					}]
				}]
			}]
		}, {
			title: '~~variables~~',
			padding: '10 0 0 0',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				flex: 1,
				xtype: 'grid',
				tbar: ['addPlaceHolder', 'removeVariables'],
				store: '@{variableStore}',
				name: 'variables',
				plugins: [{
					ptype: 'cellediting'
				}],
				viewConfig: {
					markDirty: false
				},
				columns: [{
					header: '~~name~~',
					dataIndex: 'name',
					editor: {
						xtype: 'textfield'
					},
					flex: 1
				}, {
					header: '~~color~~',
					dataIndex: 'color',
					renderer: function(value, meta) {
						meta.style = 'background-color:' + value;
					}
				}, {
					xtype: 'checkcolumn',
					header: '~~flow~~',
					dataIndex: 'flow'
				}, {
					xtype: 'checkcolumn',
					header: '~~wsdata~~',
					dataIndex: 'wsdata'
				}, {
					xtype: 'checkcolumn',
					header: '~~output~~',
					dataIndex: 'output'
				}]
			}]
		}]
	}
}