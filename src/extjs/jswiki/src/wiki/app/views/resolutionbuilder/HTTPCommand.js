glu.defView('RS.wiki.resolutionbuilder.HTTPCommand', {
	title: '~~general~~',
	defaults: {
		labelWidth: 130
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'textfield',
		fieldLabel: '~~name~~',
		value: '@{..name}'
	}, {
		xtype: 'textarea',
		fieldLabel: '~~description~~',
		value: '@{..description}'
	}, {
		xtype: 'fieldcontainer',
		layout: 'hbox',
		defaultType: 'radiofield',
		defaults: {
			hideLabel: true,
			name: 'method'
		},
		items: [{
			xtype: 'displayfield',
			hideLabel: false,
			fieldLabel: '~~method~~',
			value: ''
		}, {
			padding: '0 0 0 50',
			value: '@{postMethod}',
			boxLabel: '~~post~~'
		}, {
			padding: '0 0 0 20',
			value: '@{!postMethod}',
			boxLabel: '~~get~~'
		}]
	}, {
		xtype: 'textfield',
		name: 'commandUrl'
	}, {
		xtype: 'toolbar',
		padding: '0 0 0 150',
		items: ['addParam']
	}, {
		xtype: 'panel',
		title: '~~headers~~',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		tbar: ['addHeader'],
		items: '@{headers}'
	}, {
		xtype: 'panel',
		title: '~~body~~',
		hidden: '@{!postMethod}',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaults : {
			padding : '5px',
			labelWidth: 130
		},
		items: [{
			xtype : 'combobox',
			displayField: 'name',
			valueField: 'value',
			queryMode: 'local',
			store : '@{syntaxStore}',
			name : 'syntax',
			editable : false,
			maxWidth : 400
		},{
			xtype : 'AceEditor',
			minHeight : 300,
			parser : '@{syntax}',
			value : '@{body}',
			listeners : {
				render : function(){
					var me = this;
					this._vm.on('insertAtCursor', function(newVal){
						me.insertAtCursor(newVal);
					})
				}
			}
		}],
		bbar: ['addBodyParam']
	}]
});

glu.defView('RS.wiki.resolutionbuilder.HTTPHeader', {
	layout: 'hbox',
	padding: '5px',
	defaultType: 'textfield',
	items: [{
		name: 'name',
		flex: 1,
		padding: '0 5 0 0'
	}, {
		name: 'value',
		flex: 1,
		padding: '0 0 0 5'
	}, {
		xtype: 'fieldcontainer',
		layout: 'hbox',
		hideMode: 'visibility',
		defaults: {
			style: {
				'padding-left': '3px',
				'padding-top': '3px',
				'color': '#999'
			}
		},
		defaultType: 'component',
		items: [{
			autoEl: {
				tag: 'a',
				cls: 'icon-minus icon-large'
			},
			listeners: {
				render: function() {
					this.getEl().on('mouseover', function() {
						this.setStyle('cursor', 'pointer');
					});
					this.getEl().on('click', function(e) {
						e.stopPropagation();
						this.fireEvent('remove');
					}, this);
				},
				remove: '@{remove}'
			}
		}]
	}]
});

glu.defView('RS.wiki.resolutionbuilder.POSTParam', {
	layout: 'hbox',
	padding: '5px',
	defaultType: 'textfield',
	items: [{
		name: 'name',
		flex: 1,
		padding: '0 5 0 0'
	}, {
		name: 'value',
		flex: 1,
		padding: '0 0 0 5'
	}, {
		xtype: 'fieldcontainer',
		layout: 'hbox',
		hideMode: 'visibility',
		defaults: {
			style: {
				'padding-left': '3px',
				'padding-top': '3px',
				'color': '#999'
			}
		},
		defaultType: 'component',
		items: [{
			autoEl: {
				tag: 'a',
				cls: 'icon-minus icon-large'
			},
			listeners: {
				render: function() {
					this.getEl().on('mouseover', function() {
						this.setStyle('cursor', 'pointer');
					});
					this.getEl().on('click', function(e) {
						e.stopPropagation();
						this.fireEvent('remove');
					}, this);
				},
				remove: '@{remove}'
			}
		}]
	}]
});