glu.defView('RS.wiki.resolutionbuilder.TelnetConnectionConfig', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'fieldcontainer',
		layout: 'hbox',
		defaultType: 'fieldcontainer',
		items: [{
			defaultType: 'displayfield',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				width: 90,
				value: ''
			},
			items: [{
				fieldLabel: '~~host~~'
			}, {
				fieldLabel: '~~port~~'
			}]
		}, {
			width: 110,
			defaultType: 'combo',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				hideLabel: true,
				displayField: 'name',
				valueField: 'value'
			},
			items: [{
				name: 'hostSource',
				store: '@{sourceStore}',
				editable: false
			}, {
				name: 'portSource',
				store: '@{sourceStore}',
				editable: false
			}]
		}, {
			padding: '0 0 0 5',
			flex: 1,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaultType: 'combo',
			defaults: {
				displayField: 'name',
				valueField: 'name',
				hideLabel: true
			},
			items: [{
				xtype: 'textfield',
				name: 'host',
				hidden: '@{!hostSourceIsConst}'
			}, {
				name: 'host',
				store: '@{globalVariableStore}',
				hidden: '@{!hostSourceIsWSDATAOrFlow}',
				queryMode: 'local',
				tpl: Ext.create('Ext.XTemplate',
					'<tpl for=".">',
					'<div class="x-boundlist-item"><tpl if="color"><span style="background-color:{color}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;</tpl>{name}<tpl if="task">&nbsp; from:{task}</tpl></div>',
					'</tpl>'
				),
				listeners: {
					focus: '@{updateHostVariables}'
				}
			}, {
				name: 'host',
				store: '@{paramStore}',
				queryMode: 'local',
				hidden: '@{!hostSourceIsParam}'
			}, {
				name: 'host',
				store: '@{propertyStore}',
				pageSize: 10,
				minChars: 0,
				displayField: 'uname',
				valueField: 'uname',
				hidden: '@{!hostSourceIsProperty}'
			}, {
				xtype: 'textfield',
				name: 'port',
				hidden: '@{!portSourceIsConst}'
			}, {
				name: 'port',
				store: '@{globalVariableStore}',
				hidden: '@{!portSourceIsWSDATAOrFlow}',
				queryMode: 'local',
				tpl: Ext.create('Ext.XTemplate',
					'<tpl for=".">',
					'<div class="x-boundlist-item"><tpl if="color"><span style="background-color:{color}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;</tpl>{name}<tpl if="task">&nbsp; from:{task}</tpl></div>',
					'</tpl>'
				),
				listeners: {
					focus: '@{updatePortVariables}'
				}
			}, {
				name: 'port',
				store: '@{paramStore}',
				queryMode: 'local',
				hidden: '@{!portSourceIsParam}'
			}, {
				name: 'port',
				store: '@{propertyStore}',
				pageSize: 10,
				minChars: 0,
				displayField: 'uname',
				valueField: 'uname',
				hidden: '@{!portSourceIsProperty}'
			}]
		}]
	}, {
		xtype: 'fieldcontainer',
		defaultType: 'fieldcontainer',
		layout: 'hbox',
		defaults: {
			layout: {
				type: 'vbox',
				align: 'stretch'
			}
		},
		items: [{
			defaultType: 'displayfield',
			defaults: {
				width: 90
			},
			items: [{
				fieldLabel: '~~u_sername~~'
			}, {
				fieldLabel: '~~p_assword~~'
			}]
		}, {
			defaultType: 'combo',
			width: 110,
			defaults: {
				editable: false,
				displayField: 'name',
				hideLabel: true,
				valueField: 'value'
			},
			items: [{
				name: 'usernameSource',
				store: '@{sourceStore}'
			}, {
				name: 'passwordSource',
				store: '@{sourceStore}'
			}]
		}, {
			padding: '0 0 0 5',
			defaultType: 'textfield',
			flex: 1,
			defaults: {
				hideLabel: true
			},
			items: [{
				xtype: 'container',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaultType: 'combo',
				defaults: {
					labelWidth: 130,
					displayField: 'name',
					valueField: 'name',
					hideLabel: true
				},
				items: [{
					xtype: 'textfield',
					name: 'username',
					hidden: '@{!usernameSourceIsConst}'
				}, {
					name: 'username',
					store: '@{globalVariableStore}',
					hidden: '@{!usernameSourceIsWSDATAOrFlow}',
					queryMode: 'local',
					tpl: Ext.create('Ext.XTemplate',
						'<tpl for=".">',
						'<div class="x-boundlist-item"><tpl if="color"><span style="background-color:{color}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;</tpl>{name}<tpl if="task">&nbsp; from:{task}</tpl></div>',
						'</tpl>'
					),
					listeners: {
						focus: '@{updateUsernameVariables}'
					}
				}, {
					name: 'username',
					store: '@{paramStore}',
					queryMode: 'local',
					hidden: '@{!usernameSourceIsParam}'
				}, {
					name: 'username',
					store: '@{propertyStore}',
					pageSize: 10,
					minChars: 0,
					displayField: 'uname',
					valueField: 'uname',
					hidden: '@{!usernameSourceIsProperty}'
				}]
			}, {
				xtype: 'container',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaultType: 'combo',
				defaults: {
					labelWidth: 130,
					displayField: 'name',
					valueField: 'name',
					hideLabel: true
				},
				items: [{
					xtype: 'textfield',
					inputType: 'password',
					name: 'password',
					hidden: '@{!passwordSourceIsConst}'
				}, {
					name: 'password',
					store: '@{globalVariableStore}',
					hidden: '@{!passwordSourceIsWSDATAOrFlow}',
					queryMode: 'local',
					tpl: Ext.create('Ext.XTemplate',
						'<tpl for=".">',
						'<div class="x-boundlist-item"><tpl if="color"><span style="background-color:{color}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;</tpl>{name}<tpl if="task">&nbsp; from:{task}</tpl></div>',
						'</tpl>'
					),
					listeners: {
						focus: '@{updatePasswordVariables}'
					}
				}, {
					name: 'password',
					store: '@{paramStore}',
					queryMode: 'local',
					hidden: '@{!passwordSourceIsParam}'
				}, {
					name: 'password',
					store: '@{propertyStore}',
					pageSize: 10,
					minChars: 0,
					displayField: 'uname',
					valueField: 'uname',
					hidden: '@{!passwordSourceIsProperty}'
				}]
			}]
		}]
	}]
});

glu.defView('RS.wiki.resolutionbuilder.TelnetConnectionConfigOption', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		labelWidth: 150
	},
	defaultType: 'textfield',
	items: ['prompt', 'timeout', 'loginPrompt', 'passwordPrompt', {
		xtype: 'fieldcontainer',
		layout: 'hbox',
		items: [{
			xtype: 'displayfield',
			fieldLabel: '~~queueName~~',
			labelWidth: 147,
			value: ''
		}, {
			xtype: 'combo',
			hideLabel: true,
			displayField: 'name',
			valueField: 'value',
			padding: '0 5 0 0',
			editable: false,
			width: 110,
			store: '@{sourceStore}',
			name: 'queueNameSource'
		}, {
			xtype: 'fieldcontainer',
			flex: 1,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaultType: 'combo',
			defaults: {
				hideLabel: true,
				displayField: 'name',
				valueField: 'name'
			},
			items: [{
				xtype: 'textfield',
				name: 'queueName',
				hidden: '@{!queueNameSourceIsConst}'
			}, {
				name: 'queueName',
				store: '@{globalVariableStore}',
				hidden: '@{!queueNameSourceIsWSDATAOrFlow}',
				queryMode: 'local',
				tpl: Ext.create('Ext.XTemplate',
					'<tpl for=".">',
					'<div class="x-boundlist-item"><tpl if="color"><span style="background-color:{color}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;</tpl>{name}<tpl if="task">&nbsp; from:{task}</tpl></div>',
					'</tpl>'
				),
				listeners: {
					focus: '@{updateQueueNameVariables}'
				}
			}, {
				name: 'queueName',
				store: '@{paramStore}',
				queryMode: 'local',
				hidden: '@{!queueNameSourceIsParam}'
			}, {
				name: 'queueName',
				store: '@{propertyStore}',
				pageSize: 10,
				displayField: 'uname',
				valueField: 'uname',
				hidden: '@{!queueNameSourceIsProperty}'
			}]
		}]
	}]
})