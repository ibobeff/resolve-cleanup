glu.defView('RS.wiki.resolutionbuilder.LocalConnectionConfig', {

});

glu.defView('RS.wiki.resolutionbuilder.LocalConnectionConfigOption', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'fieldcontainer',
		layout: 'hbox',
		items: [{
			xtype: 'displayfield',
			fieldLabel: '~~queueName~~',
			value: ''
		}, {
			xtype: 'combo',
			hideLabel: true,
			displayField: 'name',
			valueField: 'value',
			padding: '0 5 0 0',
			editable: false,
			width: 110,
			store: '@{sourceStore}',
			name: 'queueNameSource'
		}, {
			xtype: 'fieldcontainer',
			flex: 1,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaultType: 'combo',
			defaults: {
				hideLabel: true,
				displayField: 'name',
				valueField: 'name'
			},
			items: [{
				xtype: 'textfield',
				name: 'queueName',
				hidden: '@{!queueNameFromConst}'
			}, {
				name: 'queueName',
				store: '@{globalVariableStore}',
				hidden: '@{!queueNameFromWSDATAOrFlow}',
				queryMode: 'local',
				tpl: Ext.create('Ext.XTemplate',
					'<tpl for=".">',
					'<div class="x-boundlist-item"><tpl if="color"><span style="background-color:{color}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;</tpl>{name}<tpl if="task">&nbsp; from:{task}</tpl></div>',
					'</tpl>'
				),
				listeners: {
					focus: '@{updateQueueNameVariables}'
				}
			}, {
				name: 'queueName',
				store: '@{paramStore}',
				queryMode: 'local',
				hidden: '@{!queueNameFromParam}'
			}, {
				name: 'queueName',
				store: '@{propertyStore}',
				pageSize: 10,
				displayField: 'uname',
				valueField: 'uname',
				hidden: '@{!queueNameFromProperty}'
			}]
		}]
	}]
});