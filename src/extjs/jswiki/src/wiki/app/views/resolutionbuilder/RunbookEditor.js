glu.defView('RS.wiki.resolutionbuilder.RunbookEditor', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: '10',
	items: [{
		title: '~~runbook~~',
		bodyPadding: '10 0 10 0',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'fieldcontainer',
			layout: 'hbox',
			items: [{
				xtype: 'combo',
				flex: 1,
				name: 'name',
				store: '@{runbookStore}',
				trigger2Cls: 'x-form-search-trigger',
				enableKeyEvents: true,
				displayField: 'ufullname',
				valueField: 'ufullname',
				queryMode: 'remote',
				typeAhead: false,
				autoSelect: false,
				minChars: 1,
				pageSize: 20,
				onTrigger2Click: function() {
					this.fireEvent('searchRunbook');
				},
				listeners: {
					searchRunbook: '@{searchRunbook}',
					select: '@{runbookSelected}'
				}
			}, {
				xtype: 'image',
				cls: 'rs-btn-edit',
				style: 'top:2px !important',
				listeners: {
					handler: '@{jumpToRunbook}',
					render: function(image) {
						image.getEl().on('click', function() {
							image.fireEvent('handler')
						})
					}
				}
			}, {
				xtype: 'button',
				baseCls: 'x-tool',
				iconCls: 'rs-social-button icon-play',
				handler: function() {
					this.fireEvent('openExecutionWindow', this, this);
				},
				listeners: {
					openExecutionWindow: '@{openExecutionWindow}'
				}
			}]
		}]
	}, {
		title: '~~parameters~~',
		xtype: 'grid',
		flex: 1,
		store: '@{parameterStore}',
		name: 'params',
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		tbar: ['addParam', 'removeParams'],
		columns: [{
			header: '~~name~~',
			dataIndex: 'name',
			flex: 1,
			editor: {
				xtype: 'textarea',
				listeners: {
					focus: function() {
						this.getFocusEl().dom.select()
					}
				}
			},
			renderer: function(value) {
				return '<pre style="word-wrap: break-word;">' + value + '</pre>'
			}
		}, {
			header: '~~source~~',
			dataIndex: 'source',
			align: 'center',
			width: 90,
			editor: {
				xtype: 'combo',
				displayField: 'source',
				valueField: 'source',
				editable: false,
				store: {
					xtype: 'store',
					fields: [{
						name: 'source',
						convert: function(v, r) {
							return r.raw
						}
					}],
					data: ['DEFAULT', 'CONSTANT', 'OUTPUT', 'FLOW', 'WSDATA', 'PARAM', 'PROPERTY']
				}
			}
		}, {
			header: '~~sourceName~~',
			dataIndex: 'sourceName',
			flex: 2,
			editor: {
				xtype: 'textarea',
				listeners: {
					focus: function() {
						this.getFocusEl().dom.select()
					}
				}
			},
			renderer: function(value) {
				return '<pre style="word-wrap: break-word;">' + value + '</pre>'
			}
		}],
		listeners: {
			beforeedit: function(plugin, eOpt) {
				var r = eOpt.record;
				this.fireEvent('updateGlobalVariableStore', this, eOpt.field);
			},
			updateGlobalVariableStore: '@{updateGlobalVariableStore}'
		}
	}]
});