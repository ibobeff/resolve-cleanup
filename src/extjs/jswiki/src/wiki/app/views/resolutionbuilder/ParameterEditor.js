glu.defView('RS.wiki.resolutionbuilder.ParameterEditor', {
	bodyPadding: 10,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaultType: 'textfield',
	items: [{
		name: 'name',
		readOnly: '@{automationViewTabPressed}',
		listeners: {
			blur: '@{checkDirty}'
		}
	}, {
		name: 'displayName', 
		readOnly: '@{automationViewTabPressed}',
		listeners: {
			blur: '@{checkDirty}'
		}
	}, {
		xtype: 'combo',
		name: 'type',
		readOnly: '@{automationViewTabPressed}',
		displayField: 'name',
		valueField: 'value',
		editable: false,
		store: '@{typeStore}',
		listeners: {
			blur: '@{checkDirty}'
		}
	}, {
		xtype: 'combo',
		name: 'source',
		readOnly: '@{automationViewTabPressed}',
		editable: false,
		displayField: 'name',
		valueField: 'value',
		store: '@{sourceStore}',
		listeners: {
			blur: '@{checkDirty}'
		}
	}, {
		xtype: 'textfield',
		name: 'sourceName',
		readOnly: '@{automationViewTabPressed}',
		listeners: {
			blur: '@{checkDirty}'
		}
	}, {
		xtype: 'textarea',
		name: 'fieldTip',
		readOnly: '@{automationViewTabPressed}',
		rows: 2,
		listeners: {
			blur: '@{checkDirty}'
		}
	}, {
		xtype: 'checkboxfield',
		name: 'hidden',
		readOnly: '@{automationViewTabPressed}',
		listeners: {
			blur: '@{checkDirty}'
		}
	}]
});