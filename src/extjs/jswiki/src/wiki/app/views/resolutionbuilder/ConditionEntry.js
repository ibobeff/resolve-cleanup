glu.defView('RS.wiki.resolutionbuilder.ConditionEntry', {
	tbar: [{
		xtype: 'tbtext',
		text: '~~conditionEntry~~',
		style: 'font-size:10px',
		listeners: {
			render: function() {
				this.getEl().on('click', function() {
					var panel = this.up().up().down('*[collapsible]')
					if (panel.items.length == 0)
						return;
					panel[panel.collapsed ? 'expand' : 'collapse']();
				}, this)
			}
		}
	}, '->', 'addTask', 'addRunbook', {
		xtype: 'tool',
		type: 'gear',
		handler: function() {
			this.ownerCt.ownerCt.fireEvent('config', this, this);
		}
	}, {
		xtype: 'tool',
		type: 'close',
		handler: function() {
			this.ownerCt.ownerCt.fireEvent('remove', this, this);
		}
	}],
	margin: 10,
	plugins: [{
		ptype: 'rbentitydroptarget'
	}],
	selector: '.conditionEntry',
	items: [{
		collapsible: true,
		header: false,
		items: '@{entries}'
	}],
	selected: '@{selected}',
	setSelected: function(selected) {
		this.selected = selected;
		// if (!this.selected)
		// 	this.getEl().removeCls('entrySelection');
		// else
		// 	this.getEl().addCls('entrySelection');
	},
	listeners: {
		render: function() {
			this.getEl().addCls('abdd');
			this.getEl().addCls('conditionEntry');
			this.getEl().dom.abCmp = this;
			// this.getEl().on('click', function(e) {
			// 	this.fireEvent('entryClicked', this, e)
			// }, this);
		},
		entryClicked: '@{entryClicked}',
		remove: '@{remove}',
		config: '@{config}',
		accept: '@{accept}'
	}
});