glu.defView('RS.wiki.resolutionbuilder.ConditionEntryConfig', {
	autoScroll: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	bodyPadding: 5,
	buttonAlign: 'left',
	dockedItems: [{
		xtype: 'toolbar',
		items: ['add', 'remove']
	}],
	items: '@{conditions}',
	asWindow: {
		title: '~~conditionEntryConfig~~',
		modal: true,
		width: 800,
		height: 300
	},
	buttons: ['update', 'cancel']
});

glu.defView('RS.wiki.resolutionbuilder.ConditionEntryRule', {
	xtype: 'panel',
	bodyPadding: '5 0 0 0',
	layout: 'hbox',
	defaultType: 'combo',
	overCls: 'rs-rb-rule-over',
	defaults: {
		hideLabel: true,
		editable: false,
		padding: '0 5 0 0',
		width: 80,
		displayField: 'name',
		valueField: 'value',
		listeners: {
			render: function(p) {
				this.getEl().set({
					'data-qtip': p.tooltip
				});
			}
		}
	},
	items: [{
		name: 'variableSource',
		tooltip: '~~variableSource~~',
		emptyText: '~~variableSource~~',
		minWidth: 110,
		store: '@{sourceStore}'
	}, {
		xtype: 'fieldcontainer',
		minWidth: 100,
		flex: 1,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaults: {
			hideLabel: true,
			flex: 1,
			tooltip: '~~variable~~',
			emptyText: '~~variable~~'
		},
		items: [{
			xtype: 'textfield',
			name: 'variable',
			hidden: '@{!variableFromConst}'
		}, {
			xtype: 'combo',
			displayField: 'name',
			valueField: 'name',
			name: 'variable',
			hidden: '@{!variableFromWSDATAOrFlow}',
			store: '@{globalVariableStore}',
			queryMode: 'local',
			tpl: Ext.create('Ext.XTemplate',
				'<tpl for=".">',
				'<div class="x-boundlist-item"><tpl if="color"><span style="background-color:{color}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;</tpl>{name}<tpl if="task">&nbsp; from:{task}</tpl></div>',
				'</tpl>'
			),
			listeners: {
				focus: '@{updateVariables}'
			}
		}, {
			xtype: 'combo',
			displayField: 'uname',
			valueField: 'uname',
			name: 'variable',
			store: '@{propertyStore}',
			pageSize: 10,
			minChars: 0,
			hidden: '@{!variableFromProperty}'
		}, {
			xtype: 'combo',
			displayField: 'name',
			valueField: 'name',
			name: 'variable',
			store: '@{paramStore}',
			queryMode: 'local',
			hidden: '@{!variableFromParam}'
		}]
	}, {
		name: 'comparison',
		tooltip: '~~comparison~~',
		emptyText: '~~comparison~~',
		minWidth: 120,
		store: '@{comparisonStore}'
	}, {
		name: 'valueSource',
		tooltip: '~~source~~',
		emptyText: '~~source~~',
		width: 110,
		store: '@{sourceStore}'
	}, {
		xtype: 'fieldcontainer',
		minWidth: 100,
		flex: 1,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaults: {
			flex: 1,
			hideLabel: true
		},
		items: [{
			xtype: 'textfield',
			tooltip: '~~sourceName~~',
			emptyText: '~~sourceName~~',
			name: 'value',
			hidden: '@{!valueFromConst}'
		}, {
			xtype: 'combo',
			name: 'value',
			hidden: '@{!valueFromWSDATAOrFlow}',
			displayField: 'name',
			valueField: 'value',
			hideLabel: true,
			tooltip: '~~sourceName~~',
			emptyText: '~~sourceName~~',
			store: '@{globalVariableStore}',
			queryMode: 'local',
			tpl: Ext.create('Ext.XTemplate',
				'<tpl for=".">',
				'<div class="x-boundlist-item"><tpl if="color"><span style="background-color:{color}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;</tpl>{name}<tpl if="task">&nbsp; from:{task}</tpl></div>',
				'</tpl>'
			),
			listeners: {
				focus: '@{updateSourceNames}'
			}
		}, {
			xtype: 'combo',
			displayField: 'uname',
			valueField: 'uname',
			tooltip: '~~sourceName~~',
			emptyText: '~~sourceName~~',
			name: 'value',
			store: '@{propertyStore}',
			pageSize: 10,
			minChars: 0,
			hidden: '@{!valueFromProperty}'
		}, {
			xtype: 'combo',
			displayField: 'name',
			valueField: 'name',
			tooltip: '~~sourceName~~',
			emptyText: '~~sourceName~~',
			name: 'value',
			store: '@{paramStore}',
			queryMode: 'local',
			hidden: '@{!valueFromParam}'
		}]
	}],
	selected: '@{selected}',
	setSelected: function(selected) {
		this.selected = selected;
		if (this.selected)
			this.getEl().addCls('rs-rb-rule-selected')
		else
			this.getEl().removeCls('rs-rb-rule-selected')
	},
	listeners: {
		render: function() {
			this.getEl().on('click', function() {
				this.fireEvent('toggleSelection');
			}, this)
			this.getEl().addCls('rs-rb-rule');
		},
		toggleSelection: '@{toggleSelection}'
	}
});