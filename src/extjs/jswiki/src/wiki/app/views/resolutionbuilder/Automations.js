glu.defView('RS.wiki.resolutionbuilder.Automations', {
	xtype: 'grid',
	cls : 'rs-grid-dark',
	stateId: '@{stateId}',
	stateful: true,
	padding: 15,
	layout: 'fit',
	displayName: '@{displayName}',
	name: 'automationMetas',
	store: '@{store}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light',
		},
		items: ['createAutomation', 'deleteAutomations', 'purgeAutomations', 'renameAutomations', 'copyAutomations']
	}],
	columns: [{
		header: '~~uname~~',
		dataIndex: 'uname',
		filterable: true,
		flex: 1
	}, {
		header: '~~unamespace~~',
		dataIndex: 'unamespace',
		filterable: true,
		flex: 1
	}, {
		header: '~~usummary~~',
		dataIndex: 'usummary',
		filterable: true,
		flex: 2,
		renderer: RS.common.grid.plainRenderer()
	}, {
		header: '~~uisDeleted~~',
		dataIndex: 'uisDeleted',
		hidden: true,
		filterable: true,
		renderer: RS.common.grid.booleanRenderer()
	}, {
		header: '~~uhasResolutionBuilder~~',
		dataIndex: 'uhasResolutionBuilder',
		filterable: true,
		renderer: RS.common.grid.booleanRenderer()
	}, {
		header: '~~uhasActiveModel~~',
		dataIndex: 'uhasActiveModel',
		filterable: true,
		renderer: RS.common.grid.booleanRenderer()
	}].concat((function(columns) {
		Ext.each(columns, function(col) {
			if (col.dataIndex == 'sysUpdatedOn') {
				col.hidden = false;
				col.initialShow = true;
			}
			if (col.dataIndex == 'sysUpdatedBy') {
				col.hidden = false;
				col.initialShow = true;
			}
		});
		return columns;
	})(RS.common.grid.getSysColumns())),
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		//columnTooltip: RS.common.locale.editColumnTooltip,
		columnTooltip: RS.common.locale.gotoDetailsColumnTooltip,
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},

	listeners: {
		editAction: '@{editAutomation}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
});