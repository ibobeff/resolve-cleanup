glu.defView('RS.wiki.resolutionbuilder.AssessConditionRules', {
	title: '~~conditionRules~~',
	padding: '0 5 0 0',
	dockedItems: [{
		xtype: 'toolbar',
		cls: 'actionBar actionBar-form',
		items: ['add', 'remove', '-', {
			xtype: 'displayfield',
			fieldLabel: '~~defaultValue~~',
			width: 100,
			value: ''
		}, {
			xtype: 'radiofield',
			boxLabel: '~~goodDefaultCondition~~',
			name: 'defaultRadio',
			value: '@{goodDefaultCondition}'
		}, {
			xtype: 'radiofield',
			boxLabel: '~~badDefaultCondition~~',
			name: 'defaultRadio',
			padding: '0 20 0 0',
			value: '@{badDefaultCondition}'
		}, '-', {
			xtype: 'displayfield',
			fieldLabel: '~~continuation~~',
			width: 70,
			value: ''
		}, {
			xtype: 'radiofield',
			name: 'continuationRadio',
			boxLabel: '~~alwaysContinue~~',
			value: '@{alwaysContinue}'
		}, {
			xtype: 'radiofield',
			name: 'continuationRadio',
			boxLabel: '~~stopOnBad~~',
			value: '@{stopOnBad}'
		}]
	}],
	autoScroll: true,
	items: '@{conditions}'
});

glu.defView('RS.wiki.resolutionbuilder.ConditionRuleGroup', {
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	margin: '5 0 0 0',
	items: [{
		result: '@{result}',
		setResult: function(result) {
			this.result = result;
			this.updateLeftBorder();
		},
		updateLeftBorder: function() {
			if (this.result.toLowerCase() == 'good')
				this.body.setStyle('background-color', 'palegreen');
			else
				this.body.setStyle('background-color', 'red');
		},
		width: 10,
		overCls: 'rs-rb-rule-result-over',
		html: '',
		listeners: {
			render: function() {
				this.updateLeftBorder();
				this.body.on('click', function() {
					this.fireEvent('toggleResult');
				}, this);
			},
			toggleResult: '@{toggleResult}'
		}
	}, {
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		flex: 1,
		items: '@{conditions}'
	}],
	selected: '@{selected}',
	setSelected: function(selected) {
		this.selected = selected;
		if (this.selected) {
			this.removeCls('rs-rb-rule');
			this.addCls('rs-rb-rule-selected');
		} else {
			this.removeCls('rs-rb-rule-selected');
			this.addCls('rs-rb-rule');
		}
	},
	qtip: '@{qtip}',
	listeners: {
		render: function() {
			this.getEl().on('click', function() {
				this.fireEvent('toggleSelection');
			}, this)
			this.addCls('rs-rb-rule');
			Ext.create('Ext.tip.ToolTip', {
				target: this.getEl(),
				html: this.qtip
			});
		},
		toggleSelection: '@{toggleSelection}'
	}
});

glu.defView('RS.wiki.resolutionbuilder.ConditionRule', {
	bodyPadding: '5 0 0 5',
	layout: 'hbox',
	defaultType: 'combo',
	defaults: {
		displayField: 'name',
		valueField: 'value',
		hideLabel: true,
		padding: '0 5 0 0',
		width: 80,
		editable: false
	},
	items: [{
		name: 'criteria',
		emptyText: '~~criteria~~',
		width: 63,
		store: '@{criteriaStore}'
	}, {
		name: 'variableSource',
		emptyText: '~~variableSource~~',
		width: 108,
		store: '@{sourceStore}'
	}, {
		xtype: 'fieldcontainer',
		flex: 1,
		minWidth: 100,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaultType: 'combo',
		defaults: {
			hideLabel: true,
			displayField: 'name',
			valueField: 'name'
		},
		items: [{
			xtype: 'textfield',
			name: 'variable',
			hidden: '@{!variableFromConst}'
		}, {
			name: 'variable',
			store: '@{globalVariableStore}',
			hidden: '@{!variableFromWSDATAOrFlow}',
			queryMode: 'local',
			tpl: Ext.create('Ext.XTemplate',
				'<tpl for=".">',
				'<div class="x-boundlist-item"><tpl if="color"><span style="background-color:{color}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;</tpl>{[this.formatFields(values)]}<tpl if="task">&nbsp; from:{task}</tpl></div>',
				'</tpl>', {
					formatFields: function(rec) {
						return Ext.htmlEncode(rec.name);
					}
				}
			),
			listeners: {
				focus: '@{updateVariables}'
			}
		}, {
			name: 'variable',
			store: '@{paramStore}',
			queryMode: 'local',
			hidden: '@{!variableFromParam}'
		}, {
			name: 'variable',
			store: '@{propertyStore}',
			pageSize: 10,
			minChars: 0,
			displayField: 'uname',
			valueField: 'uname',
			hidden: '@{!variableFromProperty}'
		}]
	}, {
		name: 'comparison',
		emptyText: '~~comparison~~',
		width: 120,
		store: '@{comparisonStore}'
	}, {
		name: 'valueSource',
		emptyText: '~~source~~',
		store: '@{sourceStore}',
		width: 110
	}, {
		xtype: 'fieldcontainer',
		minWidth: 100,
		flex: 1,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaultType: 'combo',
		defaults: {
			hideLabel: true,
			displayField: 'name',
			valueField: 'name'
		},
		items: [{
			xtype: 'textfield',
			name: 'value',
			hidden: '@{!valueFromConst}'
		}, {
			name: 'value',
			store: '@{globalVariableStore}',
			hidden: '@{!valueFromWSDATAOrFlow}',
			queryMode: 'local',
			tpl: Ext.create('Ext.XTemplate',
				'<tpl for=".">',
				'<div class="x-boundlist-item"><tpl if="color"><span style="background-color:{color}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;</tpl>{[this.formatFields(values)]}<tpl if="task">&nbsp; from:{task}</tpl></div>',
				'</tpl>', {
					formatFields: function(rec) {
						return Ext.htmlEncode(rec.name);
					}
				}
			),
			listeners: {
				focus: '@{updateSourceNames}'
			}
		}, {
			name: 'value',
			store: '@{paramStore}',
			queryMode: 'local',
			hidden: '@{!valueFromParam}'
		}, {
			name: 'value',
			store: '@{propertyStore}',
			pageSize: 10,
			minChars: 0,
			displayField: 'uname',
			valueField: 'uname',
			hidden: '@{!valueFromProperty}'
		}]
	}, {
		xtype: 'fieldcontainer',
		width: 40,
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'fieldcontainer',
			layout: 'hbox',
			hideMode: 'visibility',
			defaults: {
				style: {
					'padding-left': '3px',
					'padding-top': '3px',
					'color': '#999'
				}
			},
			defaultType: 'component',
			items: [{
				autoEl: {
					tag: 'a',
					cls: 'icon-plus icon-large'
				},
				listeners: {
					render: function() {
						this.getEl().on('mouseover', function() {
							this.setStyle('cursor', 'pointer');
						});
						this.getEl().on('click', function(e) {
							e.stopPropagation();
							this.fireEvent('addSibling');
						}, this)
					},
					addSibling: '@{addSibling}'
				}
			}, {
				autoEl: {
					tag: 'a',
					cls: 'icon-minus icon-large'
				},
				listeners: {
					render: function() {
						this.getEl().on('mouseover', function() {
							this.setStyle('cursor', 'pointer');
						});
						this.getEl().on('click', function(e) {
							e.stopPropagation();
							this.fireEvent('remove');
						}, this);
					},
					remove: '@{remove}'
				}
			}]
		}]
	}]
});