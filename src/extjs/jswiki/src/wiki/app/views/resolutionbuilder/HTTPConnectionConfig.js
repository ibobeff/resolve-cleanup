glu.defView('RS.wiki.resolutionbuilder.HTTPConnectionConfig', {
	bodyPadding: '10px',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'combo',
		store: '@{protocolStore}',
		queryMode: 'local',
		displayField: 'name',
		valueField: 'value',
		name: 'protocol'
	}, {
		xtype: 'textfield',
		name: 'host'
	}, {
		xtype: 'textfield',
		name: 'port'
	}]
});

glu.defView('RS.wiki.resolutionbuilder.HTTPConnectionConfigOption', {

});