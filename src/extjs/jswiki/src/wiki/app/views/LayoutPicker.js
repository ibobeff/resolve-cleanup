glu.defView('RS.wiki.LayoutPicker', {
	asWindow: {
		title: '~~layoutPicker~~',
		height: 250,
		width: 500
	},
	bodyPadding: '10px',
	buttonAlign: 'left',
	buttons: ['apply', 'cancel'],
	layout: 'fit',
	items: [{
		xtype: 'dataview',
		deferInitialRefresh: false,
		store: '@{columns}',
		tpl: Ext.create('Ext.XTemplate',
			'<tpl for=".">',
			'<div class="sectionPicker-section">', '<img width="85" height="85" src="/resolve/images/wikisections/{[values.name.replace(/ /g, "-")]}.png" />',
			'<strong>{name}</strong>',
			'</div>',
			'</tpl>'
		),
		itemSelector: 'div.sectionPicker-section',
		overItemCls: 'sectionPicker-section-hover',
		selectedItemCls: 'sectionPicker-section-selected',
		autoScroll: true,
		listeners: {
			selectionChange: function(sm, selected, eOpts) {
				sm.view.fireEvent('layoutSelected', sm.view, selected)
			},
			layoutSelected: '@{layoutSelected}'
		}
	}]
})