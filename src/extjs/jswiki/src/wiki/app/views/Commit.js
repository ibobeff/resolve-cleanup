glu.defView('RS.wiki.Commit', {
	title: '~~commitTitle~~',
	height: 320,
	width: 600,
	modal: true,	
	padding : 15,
	cls : 'rs-modal-popup',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		/* TODO - remove to support Version Control */
		xtype: 'checkbox',
		name: 'reviewed'
	}, 
	{
		/* TODO - remove to support Version Control */
		xtype: 'checkbox',
		name: 'postToSocial'
	}, 
	{
		xtype: 'textarea',
		labelAlign: 'top',
		name: 'comment',
		maxLength: 2000,
		enforceMaxLength: true,
		flex: 1
	}],
	buttons: [{
        text : '~~commit~~',
        cls : 'rs-med-btn rs-btn-dark',
        disabled : '@{!commitIsEnabled}',
        handler : '@{commit}',
    },{
        text : '~~cancel~~',
        cls : 'rs-med-btn rs-btn-light',
        handler : '@{cancel}',
    }]
})