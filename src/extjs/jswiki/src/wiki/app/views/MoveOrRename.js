/*
glu.defView('RS.wiki.MoveOrRename', {
	modal: true,
	cls : 'rs-modal-popup',
	title: '@{moveRenameCopyTitle}',
	width: 600,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	padding: 15,
	defaultType: 'textfield',
	defaults: {
		labelWidth: 130
	},
	items: [{
		xtype: 'combo',
		fieldLabel: '~~namespace~~',
		store: '@{store}',
		displayField: 'unamespace',
		valueField: 'unamespace',
		name: 'namespace'
	}, 'filename', {
		padding: '0px 0px 0px 125px',
		xtype: 'fieldcontainer',
		layout: 'hbox',
		defaultType: 'radiofield',
		defaults: {
			hideLabel: true,
			padding: '0px 10px 0px 0px'
		},
		items: [{
			name: 'replace',
			value: '@{overwrite}',
			boxLabel: '~~overwrite~~',
			tooltip: '~~overwrite_tooltip~~',
			listeners: {
				render: function(radio) {
					if (radio.tooltip)
						Ext.create('Ext.tip.ToolTip', {
							target: radio.getEl(),
							html: radio.tooltip
						})
				}
			}
		}, {
			name: 'replace',
			value: '@{skip}',
			boxLabel: '~~skip~~',
			tooltip: '~~skip_tooltip~~',
			listeners: {
				render: function(radio) {
					if (radio.tooltip)
						Ext.create('Ext.tip.ToolTip', {
							target: radio.getEl(),
							html: radio.tooltip
						})
				}
			}
		}, {
			name: 'replace',
			value: '@{update}',
			hidden: '@{!updateIsVisible}',
			boxLabel: '~~update~~',
			tooltip: '~~update_tooltip~~',
			listeners: {
				render: function(radio) {
					if (radio.tooltip)
						Ext.create('Ext.tip.ToolTip', {
							target: radio.getEl(),
							html: radio.tooltip
						})
				}
			}
		}]
	}],
	buttons: [{
        text : '~~confirmAction~~',
        cls : 'rs-med-btn rs-btn-dark',
        handler : '@{confirmAction}',
    },{
        text : '~~close~~',
        cls : 'rs-med-btn rs-btn-light',
        handler : '@{close}',
    }]	
});
*/