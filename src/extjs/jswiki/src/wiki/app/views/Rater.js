glu.defView('RS.wiki.Rater', {
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    bodyPadding: '10px',
    defaults: {
        labelWidth: 50
    },
    defaultType: 'textfield',

    items: [{
            xtype: 'displayfield',
            name: 'header'
        }, 'u1StarCount',
        'u2StarCount',
        'u3StarCount',
        'u4StarCount',
        'u5StarCount'
    ],
    buttons: ['rate', 'cancel'],
    asWindow: {
        modal: true,
        title: '~~ratingTitle~~',
        width: 400
    }
});