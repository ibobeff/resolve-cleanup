glu.defView('RS.wiki.RunbookPicker', {
    title: '@{title}',
    width: '@{windowWidth}',
    height: '@{windowHeight}',
    modal: true,
    //layout: 'card',
    layout : 'fit',
    cls : 'rs-modal-popup',
    padding : 15,
    //activeItem: '@{activeItem}',
    items: [{
        flex : 1,
        layout: 'border',
        margin : '0 0 10 0',
        plugins: [{
            ptype: 'searchfilter',
            allowPersistFilter: false,
            hideMenu: true
        }, {
            ptype: 'pager'
        }],
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            cls: 'actionBar rs-dockedtoolbar',
            name: 'actionBar',
            items: []
        }],
        items: [{
            region: 'west',
            width: 200,
            split: true,
            maxWidth: 300,
            xtype: 'treepanel',
            cls : 'rs-grid-dark',
            columns: '@{moduleColumns}',
            name: 'runbookTree',
            listeners: {
                selectionchange: '@{menuPathTreeSelectionchange}'
            }
        },{
            region: 'center',
            xtype: 'grid',
            cls : 'rs-grid-dark',
            displayName: '@{title}',
            name: 'runbookGrid',
            columns: [{
                header: '~~name~~',
                dataIndex: 'ufullname',
                filterable: true,
                flex: 1
            }, {
                dataIndex: 'usummary',
                header: '~~summary~~',
                filterable: true,
                hidden: true,
                visible: false,
                flex: 2
            }, {
                dataIndex: 'uhasResolutionBuilder',
                header: '~~uhasResolutionBuilder~~',
                filterable: true,
                width : 150,
                align : 'center',
                renderer: RS.common.grid.booleanRenderer()
            }, {
                dataIndex: 'uhasActiveModel',
                header: '~~uhasActiveModel~~',
                width : 100,
                align : 'center',
                renderer: RS.common.grid.booleanRenderer()
            }].concat((function() {
                var cols = RS.common.grid.getSysColumns();
                Ext.each(cols, function(c) {
                    if (c.dataIndex == 'sysUpdatedOn' || c.dataIndex == 'sysUpdatedBy')
                        c.hidden = false;
                })
                return cols;
            })()),
            multiSelect: false,
            selected: '@{runbookSelected}',
            autoScroll: true,
            viewConfig: {
                enableTextSelection: true
            },
            plugins: [{
                ptype: 'resolveexpander',
                pluginId: 'rowExpander',
                rowBodyTpl: new Ext.XTemplate('<div resolveId="resolveRowBody" style="color:#333"><span style="font-weight:bold">Summary:</span><br/><br/>&nbsp&nbsp&nbsp&nbsp{usummary:htmlEncode}</div>')
            }],
            selModel: {
                selType: 'resolvecheckboxmodel',
                columnTooltip: RS.common.locale.editColumnTooltip,
                columnTarget: '_self',
                columnEventName: 'editAction',
                columnIdField: 'ufullname'
            },
            listeners: {
                editAction: '@{editRunbook}'
            }
        },{
            xtype : 'displayfield',
            cls : 'rs-displayfield-value',
            region : 'south',
            value : '~~duplicatedDocumentError~~',         
            hidden : '@{duplicatedDocumentErrorIsHidden}'
        }]
    }
    /*, {
        bodyPadding: '10px',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [{
            xtype: 'textfield',
            name: 'namespace'
        }, {
            xtype: 'textfield',
            name: 'name'
        }]
    }, {
        bodyPadding: '10px',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [{
            xtype: 'textfield',
            name: 'namespace'
        }, {
            xtype: 'textfield',
            name: 'name'
        }]
    }
    */],

    buttons: [
    //Disable for now.
    /*{
        name : 'saveRunbookAndClose' ,
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'saveAutomationAndClose',
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name: 'createRunbook',
        cls : 'rs-med-btn rs-btn-dark',
        text: '@{createActionText}'
    },{
        name : 'createAutomationMeta',
        cls : 'rs-med-btn rs-btn-dark'
    },*/
    {
        name : 'dump',
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'cancel',
        cls : 'rs-med-btn rs-btn-light'
    }], 
    listeners: {
        beforeshow: function(win) {
            win.setWidth(Math.round(Ext.getBody().getWidth() * 0.8));
            win.setHeight(Math.round(Ext.getBody().getHeight() * 0.8));
        },
    }
});