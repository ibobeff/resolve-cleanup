glu.defView('RS.wiki.RevisionView', {
	layout: 'card',
	title: '@{title}',
	cls : 'rs-modal-popup',
	padding : 15,
	modal: true,
	dockedItems: [{
		xtype: 'toolbar',
		items: ['->', 'displayTab', 'sourceTab', 'mainTab', 'abortTab']
	}],
	defaultType: 'AceEditor',
	defaults: {
		readOnly: true,
		hideLabel: true
	},
	activeItem: '@{activeItem}',
	items: [{
		xtype: 'panel',
		html: '<iframe class="rs_iframe" src="@{src}" frameborder="0" style="width:100%;height:100%"></iframe>'
	}, {
		name: 'content',
		parser: 'groovy'
	}, {
		name: 'mainModel',
		parser: 'xml'
	}, {
		name: 'abortModel',
		parser: 'xml'
	}],
	buttons: [{
        text : '~~close~~',
        cls : 'rs-med-btn rs-btn-dark',
        handler : '@{close}',
    }],
	listeners: {
		beforeshow: function() {
			this.setWidth(Math.round(Ext.getBody().getWidth() * 0.8));
			this.setHeight(Math.round(Ext.getBody().getHeight() * 0.8));
		}
	}
});