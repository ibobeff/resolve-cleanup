glu.defView('RS.wiki.GlobalAttachmentPicker', {
	title: '~~globalAttachment~~',
	cls : 'rs-modal-popup',
	width: 650,
	height: 500,
	modal: true,
	padding : 15,
	items: [{
		dockedItems: [{
			xtype: 'toolbar',
			name: 'actionBar',
			cls : 'rs-dockedtoolbar',
			items: [{
				xtype: 'triggerfield',
				triggerCls: 'x-form-search-trigger',
				name: 'keyword',
				labelWidth: 90,
				onTriggerClick: function() {
					this.fireEvent('search')
				},
				listeners: {
					search: '@{search}'
				}
			}]
		}],
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'attachments',
		plugins: [{
			ptype: 'pager',
			showSysInfo: false
		}],
		store: '@{store}',
		columns: [{
			header: '~~fileName~~',
			dataIndex: 'fileName',
			flex: 1
		}]
	}],	
	buttons: [{
		name: 'select',
        text : '~~dump~~',
        cls : 'rs-med-btn rs-btn-dark',
        handler : '@{dump}',
    },{
        text : '~~close~~',
        cls : 'rs-med-btn rs-btn-light',
        handler : '@{close}',
    }]	
});