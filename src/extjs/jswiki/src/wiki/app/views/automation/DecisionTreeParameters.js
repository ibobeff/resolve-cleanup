glu.defView('RS.wiki.automation.DecisionTreeParameters', {
	layout: 'fit',	
	items: [{
			dockedItems: [{
				xtype : 'toolbar',
				cls : 'rs-dockedtoolbar',
				items : [{
					name: 'addParam',
					cls : 'rs-btn-light rs-small-btn',
					hidden: '@{..viewOnly}'
				}, {
					name: 'editParams',
					cls : 'rs-btn-light rs-small-btn',
					hidden: '@{..viewOnly}'
				}, {
					name: 'removeParams',
					cls : 'rs-btn-light rs-small-btn',
					hidden: '@{..viewOnly}'
				}, '->', {
					name: 'fakeInput',
					pressed: '@{pressInput}',				
					hidden: '@{!showFakeTab}'
				}, {
					name: 'fakeOutput',				
					pressed: '@{!pressInput}',
					hidden: '@{!showFakeTab}'
				}]
			}],
			xtype: 'grid',			
			minHeight: 200,
			hidden: '@{!showGrid}',
			store: '@{store}',
			name: 'parameters',
			cls: 'parametersGrid',
			sourceChangedInfoTxt: '~~sourceChanged~~',
			valueChangedInfoTxt: '~~valueChanged~~',
			valueProtectedInfoTxt: '~~valueProtected~~',
			userDefinedTxt: '~~userDefined~~',
			defaultParamsTxt: '~~defaultParams~~',
			customParamsTxt: '~~customParams~~',
			viewConfig: {
				markDirty: false,
				listeners : {
					expandbody : function(row,r,expandR, e){
						// on row expansion (clicking the + icon), switch class to display full text with word wrap as necessary
						row.innerHTML = row.innerHTML.replace(/resolve_overflow_ellipsis/g, 'resolve_overflow_break-word');
						if (expandR.innerText != '') {
							// if there is description, add a separator (by replacing the default "resolve_param_desc" class with "resolve_param_desc_border" which has a top border)
							row.innerHTML = row.innerHTML.replace(/resolve_param_desc/g, 'resolve_param_desc_border');
						}
					},
					collapsebody : function(row,r,expandR, e){
						// on row collapse, revert to default classes
						row.innerHTML = row.innerHTML.replace(/resolve_overflow_break-word/g, 'resolve_overflow_ellipsis');
						row.innerHTML = row.innerHTML.replace(/resolve_param_desc_border/g, 'resolve_param_desc');
					}
				}
			},
			features: [{
				ftype: 'grouping',
				groupHeaderTpl: [
					'{name:this.IsDefault}', {
						IsDefault: function(isDefaultParam) {
							if (isDefaultParam) {
								return this.owner.view.up().defaultParamsTxt;
							} else {
								return this.owner.view.up().customParamsTxt;
							}
						}
					}
				],
				enableNoGroups: false,
				enableGroupingMenu : false,
			}],
			columns: [{
				header: '~~name~~',
				dataIndex: 'name',
				flex: 1,
				renderer: function(value) {
					if (value) {
						return '<pre><span class="resolve_overflow_ellipsis">' + Ext.htmlEncode(value) + '</span></pre>';
					} else {
						return '';
					}
				}
			}, {
				header: '~~source~~',
				dataIndex: 'source',
				renderer: function(value, meta, record) {
					if (value) {
						if (value == 'CONSTANT') {
							value = this.userDefinedTxt;
						}
						if (record.get('isDirty')) {
							meta.tdCls = 'modified-default-param';
							meta.tdAttr = 'data-qtip="'+this.sourceChangedInfoTxt+'"';
						}
						return '<pre class="resolve_overflow_ellipsis">' + Ext.htmlEncode(value) + '</pre>';
					} else {
						return '';
					}
				},
				width: 115,
				listeners: {
					render: function() {
						var vm = this.up().up()._vm;
						this.setText(vm.sourceTitle);
					}
				}
			}, {
				header: '~~value~~',
				dataIndex: 'value',
				flex: 1,
				renderer: function(value, meta, record) {
					if (record.get('encrypt'))
						return '*****'
					if (value) {
						if (record.get('protected')) {
							meta.tdCls = 'modified-default-param';
							meta.tdAttr = 'data-qtip="' + this.valueProtectedInfoTxt + '"';
						}
						else if (record.get('isDirty')) {
							meta.tdCls = 'modified-default-param';
							meta.tdAttr = 'data-qtip="'+this.valueChangedInfoTxt+'"';
						}
						return '<pre class="resolve_overflow_ellipsis">' + Ext.htmlEncode(value) + '</pre>';
					} else {
						return '';
					}
				},
				listeners: {
					render: function() {
						var vm = this.up().up()._vm;
						this.setText(vm.sourceValueTitle);
					}
				}
			}],
			viewOnly: '@{..viewOnly}',
			plugins: [
			{
				ptype: 'rowexpander',
				expandOnDblClick: false,
				expandOnEnter: false,
				rowBodyTpl: new Ext.XTemplate('<div resolveId="resolveRowBody" style="color:#333;" class="resolve_param_desc">{description}</div>')
			}],
			listeners: {
				render: function(grid) {
					grid.store.grid = grid;
					grid.on('celldblclick', function() {
						if (!this.viewOnly && this._vm.editParamsIsEnabled) {
							this.fireEvent('editParams');
						}
					})
				},
				editParams: '@{editParams}',
			}
		}
	],
	asWindow: {
		buttonAlign: 'left',
		buttons: ['doDump' /*,'parseJson', 'fromJson'*/ , 'cancel'],
		title: '@{title}',
		width: 500
	}
})
