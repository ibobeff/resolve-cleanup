glu.defView('RS.wiki.automation.NewTaskWizardProperty',{
	xtype : 'container',
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {			
		labelWidth : 120		
	},		
	items : [{
		xtype : 'textfield',
		name : 'taskNamespace',
		margin : '4 0'		
	},{
		xtype : 'textfield',
		name : 'taskName',
		margin : '4 0'		
	},{
		xtype : '@{httpTaskConfig}',
		hidden : '@{!..taskIsHTTP}',
		flex : 1
	},{
		xtype : '@{networkTaskConfig}',
		hidden : '@{..taskIsHTTP}',
		flex : 1
	}]	
})