glu.defView('RS.wiki.automation.DecisionTree', {
	parentLayout: 'graphEditor',
	getNextCell: function(cell) {
		if (cell.getAttribute('isImage') === 'true')
			return cell.clone();
		if (cell.getValue().tagName.toLowerCase() == 'question')
			return 'document'
		return 'question'
	},
	elementPicker: '@{decisionTreeElementPicker}',
	imagePicker: '@{decisionTreeImagePicker}',
	propertyPanel: {
		layout: 'card',
		modelType: 'main',
		title: '@{title}',
		activeItem: '@{activeItem}',
		defaults: {
			bodyPadding: '10 10 0 10'
		},
		items: '@{cellProperties}'
	}
});