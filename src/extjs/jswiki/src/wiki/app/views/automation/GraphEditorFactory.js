glu.defView('RS.wiki.automation.dummy') //to make sure the ns is init before creating the factory
RS.wiki.automation.views.graphEditorFactory = function(actualView) {
	actualView.propertyPanel = actualView.propertyPanel || {
			html: 'Properties'
		};
	var elementPanelItems = [{
		xtype: actualView.elementPicker,
		animCollapse : false,
		cls : 'automation-subpanel-header',
		title : 'Element',
		collapsible : true,
		viewOnly: '@?{..viewOnly}',
		listeners: {
			afterrender: function(picker) {
				picker.viewOnly? picker.hide(): true;
			}
		}
	}, {
		xtype: actualView.imagePicker,
		animCollapse : false,
		cls : 'automation-subpanel-header',
		title : '~~image~~',
		collapsible : true,
		collapsed : '@{..collapsed}',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			padding: '6 0 0 0',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'panel',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [{
					xtype: 'combobox',
					flex: 1.25,
					padding: '0 4 0 0',
					queryMode: 'local',
					valueField: 'type',
					displayField: 'type',
					value: '~~all~~',
					editable: false,
					store: '@{..comboStore}',
					listeners: {
						select: '@{..filterByType}'
					}
				}, {
					xtype: 'textfield',
					emptyText: '~~filterBy~~',
					flex: 2,
					listeners: {
						change: '@{..filterChange}'
					}
				}]
			}, {
				xtype: 'panel',
				height: 30,
				padding: '6 0 6 0',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [{
					xtype: 'button',
					cls: 'rs-ad-image-button',
					text: '~~add~~',
					tooltip: '~~addImage~~',
					flex: 0.5,
					itemId: 'addImageBtn',
					handler: function() {
						this.up().up().up().fireEvent('launchimgpicker', this);
					}
				}, {
					xtype: 'button',
					cls: 'rs-ad-image-button',
					disabled: '@{removeBtnDisabled}',
					text: '~~delete~~',
					tooltip: '~~deleteSelectedImage~~',
					flex: 0.6,
					itemId: 'removeImageBtn',
					handler: function() {
						this.up().up().up().fireEvent('removeimages', this);
					}
				}, {
					xtype: 'displayfield',
					flex: 1.5
				}]
			}]
		}],
		viewOnly: '@?{..viewOnly}',
		listeners: {
			afterrender: function (picker) {
				picker.viewOnly ? picker.hide() : true;
				this.fireEvent('imagesrendered', this, this);
			},
			launchimgpicker: '@{launchImgPicker}',
			removeimages: '@{removeSelectedImages}',
			imagesrendered: '@{imagesRendered}'
		}
	}];
	if(actualView.connectionPicker){
		elementPanelItems.push({
			xtype: actualView.connectionPicker,
			cls : 'automation-subpanel-header',
			title : 'Connection',
			animCollapse : false,
			collapsible : true,
			collapsed : true,
			viewOnly: '@?{..viewOnly}',
			listeners: {
				afterrender: function(picker) {
					picker.viewOnly? picker.hide(): true;
				}
			}
		})
	}
	elementPanelItems.push({
		title: '~~navigator~~',
		cls : 'automation-subpanel-header',
		outline: true,
		minHeight: 230,
		flex: 1,
		layout: 'fit',
		listeners: {
			grapheditorready: function(panel, graph) {
				this.graph = graph;
			},
			resize: function() {
				if (!this.mxOutline)
					this.mxOutline = new mxOutline(this.graph, this.body.dom);
				this.mxOutline.update(true);
			}
		}
	});
	var output = {
		layout: 'border',
		cls: 'automation-designer',
		items: [{
			region: 'center',
			itemId: 'centerRegionId',
			layout: 'fit',
			viewOnly: '@?{..viewOnly}',
			dockedItems: [{
				xtype: 'toolbar',
				hidden: '@{..viewOnly}',
				items: [{
					xtype: 'combo',
					displayField: 'font',
					valueField: 'font',
					width: 180,
					editable: false,
					value: 'Helvetica',
					store: Ext.create('Ext.data.Store', {
						fields: [{
							name: 'font',
							convert: function(v, r) {
								return r.raw
							}
						}],
						data: [
							'Helvetica',
							'Verdana',
							'Times New Roman',
							'Garamond',
							'Courier New'
						]
					}),
					listeners: {
						select: function(combo, records) {
							var r = records[0],
								font = r.get('font'),
								panel = this.up('#centerRegionId');

							panel.graph.setCellStyles(mxConstants.STYLE_FONTFAMILY, font);
							panel.fireEvent('cellStyleFormatChanged', {
								fontFamily: font
							});
						},
						specialkey: function(evt) {
							if (evt.keyCode == 10 || evt.keyCode == 13) {
								var family = field.getValue();

								if (family != null &&
									family.length > 0) {
									this.up('#centerRegionId').graph.setCellStyles(mxConstants.STYLE_FONTFAMILY, family);
								}
							}
						},
						afterrender: function() {
							var me = this,
								panel = this.up('#centerRegionId');

							panel.up()._vm.on('cellSelectionChangedUpdateFormat', function() {
								me.setValue(this.cellStyleFormat.fontFamily);
								panel.fireEvent('cellStyleFormatChanged', {
									fontFamily: this.cellStyleFormat.fontFamily
								});
							});
						}
					}
				},{
					xtype: 'combo',
					displayField: 'fontsize',
					valueField: 'fontsize',
					width: 65,
					value: 8,
					editable: false,
					displayTpl: Ext.create('Ext.XTemplate',
						'<tpl for=".">',
						'{fontsize}pt',
						'</tpl>'
					),
					// Approximate fontsize conversion from px to pt
					fontSizePxMapping : {
						8 : 6,
						11 : 8,
						12 : 9,
						13 : 10,
						16 : 12,
						19 : 14,
						24 : 18,
						32 : 24,
						40 : 30,
						48 : 36,
						64 : 48,
						80 : 60
					},
					// Approximate fontsize conversion from pt to px
					fontSizePtMapping : {
						6 : 8,
						8 : 11,
						9 : 12,
						10 : 13,
						12 : 16,
						14 : 19,
						18 : 24,
						24 : 32,
						30 : 40,
						36 : 48,
						48 : 64,
						60 : 80
					},
					store: Ext.create('Ext.data.ArrayStore', {
						fields: [{
							name: 'fontsize',
							convert: function(v, r) {
								return r.raw
							}
						}],
						data: [6, 8, 9, 10, 12, 14, 18, 24, 30, 36, 48, 60]
					}),
					listeners: {
						select: function(combo, records) {
							var r = records[0],
								fontSizePt = parseInt(r.get('fontsize')),
								fontSize = this.fontSizePtMapping[fontSizePt],
								panel = this.up('#centerRegionId');

							panel.graph.setCellStyles(mxConstants.STYLE_FONTSIZE, fontSize);
							panel.fireEvent('cellStyleFormatChanged', {
								fontSize: fontSize
							});
						},
						specialkey: function(evt) {
							if (evt.keyCode == 10 || evt.keyCode == 13) {
								var size = parseInt(field.getValue());
								if (!isNaN(size) && size > 0) {
									panel.graph.setCellStyles(mxConstants.STYLE_FONTSIZE, size);
								}
							}
						},
						afterrender: function() {
							var me = this,
								panel = this.up('#centerRegionId');

							panel.up()._vm.on('cellSelectionChangedUpdateFormat', function() {
								var selectionCell = panel.graph.getSelectionCell();
								var style = selectionCell ? selectionCell.style : '';
								var fontSize = parseInt(this.cellStyleFormat.fontSize);

								// cell style's fontSize is in px (pixels) so need to convert to pt
								if (style.indexOf('fontSize') != -1) {
									var fontSizePt = me.fontSizePxMapping[fontSize];
									me.setValue(fontSizePt);
									panel.fireEvent('cellStyleFormatChanged', {
										fontSize: fontSize,
									});
								}
								else {
									var fontSizePx = me.fontSizePtMapping[fontSize];
									me.setValue(fontSize);
									panel.fireEvent('cellStyleFormatChanged', {
										fontSize: fontSizePx
									});
								}

								// update the cell font color, cell line color, and cell backgroud color
								var styles = panel.graph.getSelectionCell().getStyle().split(';');
								panel.fireEvent('resetelementstyle');
								styles = Ext.Array.filter(styles, function(item, idx) {
									if (item.indexOf('fontColor') != -1) {
										var fontColor = item.split('=#');
										panel.fireEvent('updateelementfontcolor', panel, fontColor[1]);
									}
									else if (item.indexOf('fillColor') != -1) {
										var fillColor = item.split('=#');
										panel.fireEvent('updateelementfillcolor', panel, fillColor[1]);
									}
									else if (item.indexOf('strokeColor') != -1) {
										var strokeColor = item.split('=#');
										panel.fireEvent('updateelementlinecolor', panel, strokeColor[1]);
									}
								});
							});
						}
					}
				},{
					iconCls: '@{fontColorIcon}',
					menu: {
						xtype: 'resolvecolormenu',
						handler: function(cm, color) {
							if (typeof(color) == "string") {
								this.up('#centerRegionId').graph.setCellStyles(mxConstants.STYLE_FONTCOLOR, '#' + color);
								this.up('#centerRegionId').fireEvent('updateelementfontcolor', this, color);
								this.clear();
							}
						}
					}
				},{
					iconCls: '@{lineColorIcon}',
					menu: {
						xtype: 'resolvecolormenu',
						handler: function(cm, color) {
							if (typeof(color) == "string") {
								this.up('#centerRegionId').graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, '#' + color);
								this.up('#centerRegionId').fireEvent('updateelementlinecolor', this, color);
								this.clear();
							}
						}
					}
				},{
					iconCls: '@{fillColorIcon}',
					menu: {
						xtype: 'resolvecolormenu',
						handler: function(cm, color) {
							if (typeof(color) == "string") {
								this.up('#centerRegionId').graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, '#' + color);
								this.up('#centerRegionId').fireEvent('updateelementfillcolor', this, color);
								this.clear();
							}
						}
					}
				}, {
					iconCls: 'left-icon',
					menu: {
						xtype: 'menu',
						items: [{
							iconCls: 'left-icon',
							text: 'Left',
							handler: function() {
								this.up('#centerRegionId').graph.setCellStyles(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_LEFT);
							}
						}, {
							iconCls: 'center-icon',
							text: 'Center',
							handler: function() {
								this.up('#centerRegionId').graph.setCellStyles(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_CENTER);
							}
						}, {
							iconCls: 'right-icon',
							text: 'Right',
							handler: function() {
								this.up('#centerRegionId').graph.setCellStyles(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_RIGHT);
							}
						}, {
							xtype: 'menuseparator'
						}, {
							iconCls: 'top-icon',
							text: 'Top',
							handler: function() {
								this.up('#centerRegionId').graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_TOP);
							}
						}, {
							iconCls: 'middle-icon',
							text: 'Middle',
							handler: function() {
								this.up('#centerRegionId').graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_MIDDLE);
							}
						}, {
							iconCls: 'bottom-icon',
							text: 'Bottom',
							handler: function() {
								this.up('#centerRegionId').graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_BOTTOM);
							}
						}]
					}
				}, {
					text: 'Bg',
					menu: {
						items: [{
							text: 'Opacity',
							handler: function() {
								var me = this;

								function doPrompt() {
									Ext.MessageBox.prompt('Opacity', 'Please enter the opacity (0-100)', function(btn, value) {
										if (btn != 'ok')
											return;
										if (!/^\d+$/.test(value)) {
											clientVM.displayError('Invalid Opacity', 'Please enter an integer (0-100).');
											doPrompt();
										}
										value = parseInt(value);
										if (value < 0 || value > 100) {
											clientVM.displayError('Invalid Opacity', 'Please enter an integer (0-100).');
											doPrompt();
										}
										me.up().ownerButton.up('#centerRegionId').graph.setCellStyles(mxConstants.STYLE_OPACITY, value);
									}, 1)
								}
								doPrompt()
							}
						}, {
							text: 'Gradient',
							menu: {
								xtype: 'resolvecolormenu',
								handler: function(menu, color) {
									this.up('#centerRegionId').graph.setCellStyles(mxConstants.STYLE_GRADIENTCOLOR, '#' + color);
									this.clear();
								}
							}
						}, {
							text: 'Shadow',
							handler: function() {
								this.up('#centerRegionId').graph.toggleCellStyles(mxConstants.STYLE_SHADOW);
							}
						}, {
							text: 'Image',
							handler: function() {
								var me = this;
								Ext.MessageBox.prompt('Image', 'Please enter the image URL.', function(btn, value) {
									if (btn != 'ok')
										return;
									me.up('#centerRegionId').graph.setCellStyles(mxConstants.STYLE_IMAGE, value)
								}, 1)
							}
						}]
					}
				}]
			},{
				xtype: 'toolbar',
				hidden: '@{..viewOnly}',
				defaults: {
					text: ''
				},
				items: [/*{
					iconCls: 'rs-social-button icon-print',
					tooltip: '~~printPreview~~',
					handler: function() {
						var preview = new mxPrintPreview(this.up('#centerRegionId').graph, 1);
						preview.open();
					}
				}, */{
					tooltip: '~~printPreview~~',
					iconCls: 'rs-social-button icon-download-alt',
					menu: {
						xtype: 'menu',
						items: [{
							iconCls: 'rs-social-button icon-print',
							text: '~~printPreview~~',
							handler: function() {
								var preview = new mxPrintPreview(this.up('#centerRegionId').graph, 1);
								preview.open();
							}
						}, {
							iconCls: 'rs-social-button icon-picture',
							text: '~~saveAsPng~~',
							handler: function() {
								saveSvgAsPng(this.up('#centerRegionId').graph.container.getElementsByTagName('svg')[0], "automation_graph.png", {backgroundColor: '#FFFFFF'});
							}
						}]
					}
				}, '-', {
					iconCls: 'rs-social-button icon-hand-up',
					tooltip: '~~select~~',
					pressed: '@{!panIsPressed}',
					handler: function() {
						var graph = this.up('#centerRegionId').graph;
						graph.panningHandler.useLeftButtonForPanning = false;
						graph.container.style.cursor = 'pointer';
						this.up('#centerRegionId')._vm.fireEvent('selectclicked');
					}
				}, {
					iconCls: 'rs-social-button icon-move',
					tooltip: '~~pan~~',
					pressed: '@{panIsPressed}',
					handler: function() {
						var graph = this.up('#centerRegionId').graph;
						graph.panningHandler.useLeftButtonForPanning = true;
						graph.container.style.cursor = 'move';
						this.up('#centerRegionId')._vm.fireEvent('panclicked');
					}
				}, '-', {
					iconCls: 'rs-social-button icon-cut',
					tooltip: '~~cut~~',
					disabled: '@{!..cellSelected}',
					handler: function() {
						mxClipboard.cut(this.up('#centerRegionId').graph);
						this.up('#centerRegionId').updateControls();
					}
				}, {
					iconCls: 'rs-social-button icon-copy',
					tooltip: '~~copy~~',
					disabled: '@{!..cellSelected}',
					handler: function() {
						mxClipboard.copy(this.up('#centerRegionId').graph);
						this.up('#centerRegionId').updateControls();
					}
				}, {
					itemId: 'pasteId',
					iconCls: 'rs-social-button icon-paste',
					tooltip: '~~paste~~',
					disabled: true,
					handler: function() {
						if (mxClipboard.isEmpty() == false) {
							mxClipboard.paste(this.up('#centerRegionId').graph);
							this.up('#centerRegionId').updateControls();
						}
					}
				}, {
					iconCls: 'rs-social-button icon-remove',
					tooltip: '~~remove~~',
					disabled: '@{!..cellSelected}',
					handler: function() {
						this.up('#centerRegionId')._vm.fireEvent('removeclicked', this.up('#centerRegionId').graph);
						/*
						this.up('#centerRegionId').graph.removeCells();
						this.up('#centerRegionId').updateControls();
						*/
					}
				}, '-', {
					itemId: 'undoId',
					iconCls: 'rs-social-button icon-rotate-left',
					tooltip: '~~undo~~',
					disabled: true,
					handler: function() {
						var history = this.up('#centerRegionId').history;
						if (history.canUndo()) {
							history.undo();
							this.up('#centerRegionId').updateControls();
						}
					}
				}, {
					itemId: 'redoId',
					iconCls: 'rs-social-button icon-rotate-right',
					tooltip: '~~redo~~',
					disabled: true,
					handler: function() {
						var history = this.up('#centerRegionId').history;
						if (history.canRedo()) {
							history.redo();
							this.up('#centerRegionId').updateControls();
						}
					}
				}, '-', {
					iconCls: 'rs-social-button icon-retweet',
					tooltip: '~~resume~~',
					handler: function() {
						this.up('#centerRegionId').graph.zoomActual();
						this.up('#centerRegionId').updateControls();
					}
				}, {
					iconCls: 'rs-social-button icon-zoom-in',
					tooltip: '~~zoomIn~~',
					handler: function() {
						this.up('#centerRegionId').graph.zoomIn();
					}
				}, {
					iconCls: 'rs-social-button icon-zoom-out',
					tooltip: '~~zoomOut~~',
					handler: function() {
						this.up('#centerRegionId').graph.zoomOut();
					}
				}, '-', {
					icon: '/resolve/jsp/model/images/alignleft.gif',
					disabled: '@{!..cellSelected}',
					menu: {
						xtype: 'menu',
						items: [{
							icon: '/resolve/jsp/model/images/alignleft.gif',
							text: '~~left~~',
							handler: function() {
								this.up('#centerRegionId').graph.alignCells(mxConstants.ALIGN_LEFT)
							}
						}, {
							icon: '/resolve/jsp/model/images/alignright.gif',
							text: '~~right~~',
							handler: function() {
								this.up('#centerRegionId').graph.alignCells(mxConstants.ALIGN_RIGHT)
							}
						}, {
							icon: '/resolve/jsp/model/images/aligncenter.gif',
							text: '~~center~~',
							handler: function() {
								this.up('#centerRegionId').graph.alignCells(mxConstants.ALIGN_CENTER)
							}
						}, {
							icon: '/resolve/jsp/model/images/alignmiddle.gif',
							text: '~~middle~~',
							handler: function() {
								this.up('#centerRegionId').graph.alignCells(mxConstants.ALIGN_MIDDLE)
							}
						}, {
							icon: '/resolve/jsp/model/images/aligntop.gif',
							text: '~~top~~',
							handler: function() {
								this.up('#centerRegionId').graph.alignCells(mxConstants.ALIGN_TOP)
							}
						}, {
							icon: '/resolve/jsp/model/images/alignbottom.gif',
							text: '~~bottom~~',
							handler: function() {
								this.up('#centerRegionId').graph.alignCells(mxConstants.ALIGN_BOTTOM)
							}
						}]
					}
				}, {
					tooltip: '~~order~~',
					icon: '/resolve/jsp/model/images/tofront.gif',
					disabled: '@{!..cellSelected}',
					menu: {
						xtype: 'menu',
						items: [{
							icon: '/resolve/jsp/model/images/tofront.gif',
							text: '~~bringToFront~~',
							disabled: '@{!..cellSelected}',
							handler: function() {
								this.up('#centerRegionId').graph.orderCells(false);
							}
						}, {
							icon: '/resolve/jsp/model/images/toback.gif',
							text: '~~sendToBack~~',
							disabled: '@{!..cellSelected}',
							handler: function() {
								this.up('#centerRegionId').graph.orderCells(true);
							}
						}]
					}
				}]
			},{
				xtype: 'toolbar',
				hidden: '@{!..viewOnly}',
				defaults: {
					text: ''
				},
				items: [{
					iconCls: 'rs-social-button icon-print',
					tooltip: '~~printPreview~~',
					handler: function() {
						var preview = new mxPrintPreview(this.up('#centerRegionId').graph, 1);
						preview.open();
					}
				}, '-', {
					iconCls: 'rs-social-button icon-hand-up',
					tooltip: '~~select~~',
					pressed: '@{!panIsPressed}',
					handler: function() {
						var graph = this.up('#centerRegionId').graph;
						graph.panningHandler.useLeftButtonForPanning = false;
						graph.container.style.cursor = 'pointer';
						this.up('#centerRegionId')._vm.fireEvent('selectclicked');
					}
				}, {
					iconCls: 'rs-social-button icon-move',
					tooltip: '~~pan~~',
					pressed: '@{panIsPressed}',
					handler: function() {
						var graph = this.up('#centerRegionId').graph;
						graph.panningHandler.useLeftButtonForPanning = true;
						graph.container.style.cursor = 'move';
						this.up('#centerRegionId')._vm.fireEvent('panclicked');
					}
				}, '-', {
					iconCls: 'rs-social-button icon-retweet',
					tooltip: '~~resume~~',
					handler: function() {
						this.up('#centerRegionId').graph.zoomActual();
					}
				}, {
					iconCls: 'rs-social-button icon-zoom-in',
					tooltip: '~~zoomIn~~',
					handler: function() {
						this.up('#centerRegionId').graph.zoomIn();
					}
				}, {
					iconCls: 'rs-social-button icon-zoom-out',
					tooltip: '~~zoomOut~~',
					handler: function() {
						this.up('#centerRegionId').graph.zoomOut();
					}
				}]
			}],
			plugins: [{
				ptype: 'resolvegrapheditor',
				getNextCell: actualView.getNextCell || function(cell) {
					if (cell.getAttribute('isImage') === 'true')
						return cell.clone();
					return 'task';
				}
			}],
			modelXml: '@{modelXml}',
			setModelXml: function(modelXml) {
				this.modelXml = modelXml;
				if (modelXml.shouldUpdateModel)
					this.fireEvent('modelXmlChanged');
			},
			afterGraphReady: function(graph) {
				this._vm.on('selectTask', function(name, namespace, massager, cell) {
					var valueXml = cell.getValue();
					var xmlDoc = mxUtils.createXmlDocument();
					if (Ext.isFunction(massager))
						massager(valueXml);
					var nameNode = null;

					function findAndManipulate(node) {
						if (node.tagName && node.tagName.toLowerCase() == 'name')
							nameNode = node;

						Ext.each(node.childNodes, function(child) {
							findAndManipulate(child);
						})
					}
					findAndManipulate(valueXml);

					if (nameNode) {
						valueXml.removeChild(nameNode);
					}

					nameNode = xmlDoc.createElement('name');
					nameNode.appendChild(xmlDoc.createTextNode(name + '#' + namespace));
					valueXml.insertBefore(nameNode, valueXml.childNodes[0]);
					valueXml.setAttribute('description', name + "#" + namespace + '?'); // we need this questionmark to trigger the logic of reading param from CDATA...should be fixed later..
					cell.setValue(valueXml);

					if (!(cell.getAttribute('customIcon') || namespace.toLowerCase() == 'resolve' && (name.toLowerCase() == 'start' || name.toLowerCase() == 'event' || name.toLowerCase() == 'end'))) {
						graph.setCellStyles(mxConstants.STYLE_VERTICAL_LABEL_POSITION, mxConstants.ALIGN_MIDDLE);
						graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_MIDDLE);
					}

					graph.getModel().fireEvent(new mxEventObject(mxEvent.CHANGE, "edit", {
						changes: ['resolvemodeledit']
					}));
					graph.getView().refresh();
				});
				this._vm.on('selectRunbook', function(ufullname, cell) {
					var xmlValue = cell.getValue();
					xmlValue.setAttribute('description', ufullname);
					cell.setValue(xmlValue);
					graph.setAutoSizeCells = true;
					graph.getModel().fireEvent(new mxEventObject(mxEvent.CHANGE, "edit", {
						changes: ['resolvemodeledit']
					}));
					graph.getView().refresh();
				});
				this._vm.on('selectDocument', function(ufullname, cell) {
					var xmlValue = cell.getValue();
					xmlValue.setAttribute('label', ufullname);
					cell.setValue(xmlValue);
					graph.setAutoSizeCells = true;
					graph.getModel().fireEvent(new mxEventObject(mxEvent.CHANGE, "edit", {
						changes: ['resolvemodeledit']
					}));
					graph.getView().refresh();
				});
				this._vm.on('cdatachanged', function(cell) {
					this.fireEvent('cdatachanged', cell);
				}, this);
				this._vm.on('elementpropertychanged', function(cell, property, dataStr) {
					this.fireEvent('elementpropertychanged', cell, property, dataStr);
				}, this);
				this._vm.on('taskdependencychanged', function(cell, merge) {
					this.fireEvent('taskdependencychanged', cell, merge);
				}, this);
				this._vm.on('celllabelchanged', function(cell, label) {
					this.fireEvent('celllabelchanged', cell, label);
				}, this);
				this._vm.on('cellversionchanged', function(cell, version) {
					this.fireEvent('cellversionchanged', cell, version);
				}, this);
				this._vm.on('dtrecommendationchanged', function(cell, text) {
					this.fireEvent('dtrecommendationchanged', cell, text);
				}, this);
				this._vm.on('edgeexecutiondependencychanged', function(executeDependency) {
					this.fireEvent('edgeexecutiondependencychanged', executeDependency)
				}, this);
				this._vm.on('edgeconditiondependencychanged', function(conditionDependency) {
					this.fireEvent('edgeconditiondependencychanged', conditionDependency);
				}, this);
				this._vm.on('connectorchanged', function(connector) {
					this.fireEvent('connectorchanged', connector);
				}, this);
				this._vm.on('dtselecttypechanged', function(cell, type) {
					this.fireEvent('dtselecttypechanged', cell, type);
				}, this);
				this._vm.on('dtanswerorderchanged', function(cell, order) {
					this.fireEvent('dtanswerorderchanged', cell, order);
				}, this);
				this._vm.on('entityexistence', function(cell, existence) {
					this.fireEvent('entityexistence', cell, existence);
				}, this);
				this._vm.on('updateSelectedTask', function(cell) {
					this.fireEvent('updateSelectedTask', cell);
				}, this);
				this._vm.on('updateSelectedRunbook', function(cell) {
					this.fireEvent('updateSelectedRunbook', cell);
				}, this);
				this._vm.on('clearHistory', function() {
					this.history.clear();
					this.updateControls();
				}, this);
				this._vm.on('updateControls', function() {
					this.updateControls();
				}, this);
				this._vm.on('paramDependencyUndo', function(numUndos, isRenameActionTask) {
					for (var i=0; i<numUndos; i++) {
						if (this.history.canUndo()) {
							this.history.undo();
							this.history.trim();
						}
					}

					if (isRenameActionTask) {
						this.fireEvent('revertTaskNameChange')
					} else if (numUndos == 1) {
						this.fireEvent('revertSplitEdge');
					} else if (numUndos == 2) {
						this.fireEvent('revertDropElementSplitElement');
					}
					this.updateControls();
				}, this);

				graph.container.style.cursor = 'pointer';
			},
			updateControls: function() {
				var history = this.history,
					redo = this.down('#redoId'),
					undo = this.down('#undoId'),
					paste = this.down('#pasteId');

				if (history.canRedo()) {
					redo.enable();
				} else {
					redo.disable();
				}

				if (history.canUndo()) {
					undo.enable();
				} else {
					undo.disable();
				}

				if (mxClipboard.isEmpty()) {
					paste.disable();
				} else {
					paste.enable();
				}
			},
			listeners: {
				render: function() {
					this.body.setStyle('overflow', 'auto');

				},
				grapheditorready: function(panel, graph) {
					Ext.Array.each(this.up().query('*[elementType]'), function(cmp) {
						cmp.fireEvent('grapheditorready', panel, graph, panel);
					});
					this.up().down('*[outline]').fireEvent('grapheditorready', panel, graph);
					this.fireEvent('modelXmlChanged');
					this.afterGraphReady(this.graph);

					this.history.clear();
					this.updateControls();
				},
				showelementmenu: function(graph, x, y, cell, evt) {
					if (!cell)
						return;
					this.fireEvent('showMenu', this, this, graph, x, y, cell);
				},
				labelChangedInline: '@{labelChangedInline}',
				splitEdge: '@{splitEdge}',
				addVertex: '@{addVertex}',
				revertSplitEdge: '@{revertSplitEdge}',
				revertTaskNameChange: '@{revertTaskNameChange}',
				revertDropElementSplitElement: '@{revertDropElementSplitElement}',
				cellselectionchange: function(panel, cells) {
					/*
					 var east = this.up().down('panel[region="east"]');

					 if (cells && cells.length == 1 && east.collapsed && cells[0].getValue().getAttribute('noProperty') !== 'true') {
					 east.expand();
					 }
					 */

					this.fireEvent('cellselectionchanged', this, cells);
				},
				beforeshowcelltooltip: '@{showToolTip}',
				cellselectionchanged: '@{cellSelectionChanged}',
				refreshselectedcellversion: '@{refreshSelectedCellVersion}',
				modelchanged: '@{modelChanged}',
				showMenu: '@{showMenu}',
				cellversionchanged: function(cells, version) {
					this.fireEvent('refreshselectedcellversion', this, cells);
				},
				updateSelectedTask: function(cells) {
					this.fireEvent('cellselectionchanged', this, cells);
				},
				updateSelectedRunbook: function(cells) {
					this.fireEvent('cellselectionchanged', this, cells);
				},
				connectionDrop : '@{connectionDrop}',
				updateControls: function() {
					this.updateControls();
				},
				removeclicked: '@{removeClicked}',
				beforedestroy : function(p){
					//Clean up all events registered with mxEvents
					var domEl = p.getEl() ? p.getEl().dom : null;
					if(domEl){						
						mxEvent.removeAllListeners(domEl);
						mxEvent.objects = [];
					}
				},
				resetelementstyle: '@{resetElementStyle}',
				updateelementfontcolor: '@{updateElementFontColor}',
				updateelementlinecolor: '@{updateElementLineColor}',
				updateelementfillcolor: '@{updateElementFillColor}'
			}
		}, {
			region: 'west',
			itemId: 'westRegionId',
			bodyPadding : '0 10px',
			title : '@{..designerPanelTitle}',
			collapsible: true,
			autoScroll: true,
			width: 280,
			style: {
				borderRightWidth:'1px',
				borderRightStyle:'solid',
				borderRightColor:'#ccc'
			},
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: elementPanelItems,
			listeners : {
				afterrender : function(){
					this.header.addCls('automation-panel');
				}
			}
		},
			Ext.apply({
				region: 'east',
				itemId: 'eastRegionId',
				collapsible: true,
				collapsed: '@{..collapsed}',
				animCollapse: false,
				width: 550,
				minWidth : 550,
				split: true,
				style: {
					borderLeftWidth:'1px',
					borderLeftStyle:'solid',
					borderLeftColor:'#ccc'
				},
				listeners: {
					collapse: function(p, eOpts) {
						this.up().up()._vm.collapsed = true;
					},
					expand: function(p, eOpts) {
						this.up().up()._vm.collapsed = false;
					},
					afterrender : function(){
						this.header.addCls('automation-panel');
					}
				}
			}, actualView.propertyPanel)
		]
	};
	return output;
}
