glu.defView('RS.wiki.automation.ParameterEdit', {
	width: 700,
	height: 380,
	title: '@{title}',
	cls : 'rs-modal-popup',
	modal: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	padding : 15,
	defaults: {
		labelWidth: 140,
		margin : '4 0'
	},
	items: [{
		xtype: 'displayfield',
		cls : 'rs-displayfield-value',
		name: 'name',
		hidden: '@{!isDefaultParam}',
	},{
		xtype: 'textfield',
		name: 'name',
		regex: RS.common.validation.VariableName,
		regexText: '~~validParamNameCharacters~~',
		hidden: '@{isDefaultParam}',
	},{

		hidden: '@{nodescription}',
		xtype: 'displayfield',
		cls : 'rs-displayfield-value',
		name: 'description',		
	},{
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [
		/*
		{
			xtype: 'displayfield',
			cls : 'rs-displayfield-value',
			value: '@{sourceLabel}',
			hidden: '@{isInputPressed}',
			width: 144,
		},
		*/
		{		
			xtype: 'radio',
			//hidden: '@{!isInputPressed}',
			inputValue: 'fromSource',
			boxLabel: '@{sourceLabel}',
			checked: '@{!isCustomValueChecked}',
			width: 144,
			listeners: {
				change: function(comp, newValue, oldValue){
					if (newValue) {
						this.fireEvent('fromSourceClicked');
					}
				},
				fromSourceClicked: '@{fromSourceClicked}',
			}
		}, {
			flex: 1,
			disabled: '@{isCustomValue}',
			layout: {
				type: 'hbox',
				//align: 'stretch'
			},
			currentValue: '@{value}',
			currentComboValue: '@{comboValue}',
			items: [{
				width : 200,
				margin : '0 5 0 0',
				xtype: 'combo',
				name: 'fromSourceType',
				displayField: 'source',
				valueField: 'source',
				hideLabel: true,
				editable: false,
				store: '@{paramSource}',
			}, {
				// textbox for DEFAULT
				flex: 1,			
				xtype: 'displayfield',
				cls : 'rs-displayfield-value',
				inputType: '@{inputType}',
				hideLabel: true,
				name: 'defaultSourceValue',
				hidden: '@{!defaultValueEnabled}',
				renderer: RS.common.grid.plainRenderer(),
			}, {
				// textarea for FLOWS, WSDATA, INPUT
				flex: 1,
				xtype: 'textarea',
				cls: 'parametersTextArea',
				rows: 1,
				inputType: '@{inputType}',
				hideLabel: true,
				name: 'fromSourceValue',
				hidden: '@{!fromSourceTextboxValueEnabled}',
				regex: '@{paramKeyRegex}',
				regexText: '~~validParamKeyCharacters~~',
			}, {
				// dropdown list for PROPERTY
				flex: 1,			
				xtype: 'combo',
				name: 'fromSourceComboValue',
				maxLength: '@{comboValueMaxChar}',
				enforceMaxLength: true,
				displayField: 'name',
				valueField: 'name',
				hideLabel: true,
				store: '@{propertyStore}',
				queryMode: 'local',
				hidden: '@{!fromSourcePropertyValueEnabled}',
				regex: '@{paramKeyRegex}',
				regexText: '~~validParamKeyCharacters~~',
				listeners: {
					afterrender : function(combobox){
						//This is needed since empty store will always set value field to empty.
						combobox.setRawValue(this.up().currentComboValue);
					}
				}
			}, {
				// dropdown list for PARAM
				flex: 1,				
				xtype: 'combo',
				name: 'fromSourceComboValue',
				maxLength: '@{comboValueMaxChar}',
				enforceMaxLength: true,
				displayField: 'name',
				valueField: 'name',
				hideLabel: true,
				store: '@{paramsStore}',
				queryMode: 'local',
				hidden: '@{!fromSourceParamsStoreEnabled}',
				regex: '@{paramKeyRegex}',
				regexText: '~~validParamKeyCharacters~~',
				listeners: {
					afterrender : function(combobox){
						//This is needed since empty store will always set value field to empty.
						combobox.setRawValue(this.up().currentComboValue);
					}
				}
			}, {
				// dropdown list for OUTPUT
				flex: 1,				
				xtype: 'combo',
				name: 'fromSourceComboValue',
				maxLength: '@{comboValueMaxChar}',
				enforceMaxLength: true,
				displayField: 'name',
				valueField: 'name',
				hideLabel: true,
				store: '@{outputStore}',
				queryMode: 'local',
				hidden: '@{!fromSourceOutputStoreEnabled}',
				regex: '@{paramKeyRegex}',
				regexText: '~~validParamKeyCharacters~~',
				listeners: {
					afterrender : function(combobox){
						//This is needed since empty store will always set value field to empty.
						combobox.setRawValue(this.up().currentComboValue);
					}
				}
			}]
		}]
	}, {	
		hidden: '@{isPartOfDT}',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'radio',
			inputValue: 'customValue',
			boxLabel: '~~customValue~~',
			checked: '@{isCustomValueChecked}',
			width: 144,
			listeners: {
				change: function(comp, newValue, oldValue){
					if (newValue) {
						this.fireEvent('customValueClicked');
					}
				},
				customValueClicked: '@{customValueClicked}',
			}
		}, {
			flex: 1,
			disabled: '@{!isCustomValue}',
			layout: {
				type: 'hbox',
				//align: 'stretch'
			},
			items: [{
				flex: 2,
				xtype: 'textarea',
				grow: true,
				growMin: 24,
				growPad: -8,
				growAppend: '',
				growMax: 60,
				hideLabel: true,
				name: 'customValue',
				inputType: '@{inputType}',
				margin : '0 5 0 0',
				listeners: {
					focus: '@{clearEncryptValue}',
					blur: '@{updateEncryptValue}'
				}
			}, {			
				xtype: 'button',
				name: 'addVariable',
				cls : 'rs-small-btn rs-btn-dark',
				disabled: '@{isAddVariableDisabled}',
				listeners: {
					click: '@{clearEncryptValue}'
				}
			}]
		}]
	}, {
		hidden: '@{!addVariableIsPressed}',
		disabled: '@{!isCustomValue}',	
		items: [{
			xtype : 'fieldset',
			padding : 10,
			title : '~~variableArg~~',
			defaultType : 'container',
			layout: {
				type: 'hbox',
				//align: 'stretch',
			},
			defaults: {
				hideLabel: true,
				margin : '0 5 0 0'			
			},		
			items: [{				
				width : 200,
				xtype: 'combo',
				editable: false,
				name: 'addVarSourceType',
				maxLength: '@{comboValueMaxChar}',
				enforceMaxLength: true,
				displayField: 'source',
				valueField: 'source',
				store: '@{addVarParamStore}',
			}, {
				// textarea for FLOWS, WSDATA, INPUT
				flex: 1,
				xtype: 'textarea',
				cls: 'parametersTextArea',
				rows: 1,
				hideLabel: true,
				allowBlank: false,
				name: 'addVarSourceValue',
				readOnly: '@{addVarTextboxValueReadOnly}',
				hidden: '@{!addVarTextboxValueEnabled}'
			}, {
				// dropdown list for PROPERTY
				flex: 1,
				xtype: 'combo',
				name: 'addVarSourceValue',
				maxLength: '@{comboValueMaxChar}',
				enforceMaxLength: true,
				displayField: 'name',
				valueField: 'name',
				hideLabel: true,
				allowBlank: false,
				store: '@{propertyStore}',
				queryMode: 'local',
				hidden: '@{!addVarPropertyValueEnabled}'
			}, {
				// dropdown list for PARAM
				flex: 1,
				xtype: 'combo',
				name: 'addVarSourceValue',
				maxLength: '@{comboValueMaxChar}',
				enforceMaxLength: true,
				displayField: 'name',
				valueField: 'name',
				hideLabel: true,
				allowBlank: false,
				store: '@{paramsStore}',
				queryMode: 'local',
				hidden: '@{!addVarParamsStoreEnabled}'
			}, {
				// dropdown list for OUTPUT
				flex: 1,
				xtype: 'combo',
				name: 'addVarSourceValue',
				maxLength: '@{comboValueMaxChar}',
				enforceMaxLength: true,
				displayField: 'name',
				valueField: 'name',
				hideLabel: true,
				allowBlank: false,
				store: '@{outputStore}',
				queryMode: 'local',
				hidden: '@{!addVarOutputStoreEnabled}'
			}, {
				xtype: 'button',
				name: 'addVariableBtn',
				cls : 'rs-small-btn rs-btn-dark',
				disabled: '@{addVariableBtnDisabled}',
			}, {
				xtype: 'button',
				cls : 'rs-small-btn rs-btn-light',
				name: 'cancelVariableBtn',
			}],
		}],
	
	}],	
	buttons: [{
		name: 'updateParam',
		cls : 'rs-med-btn rs-btn-dark',
		text: '@{updateParamBtnName}',
		disabled: '@{!updateParamIsValid}',
	}, {
		name: 'defaultParam',
		cls : 'rs-med-btn rs-btn-dark',
		hidden: '@{defaultParamIsHidden}',
	}, {
		cls : 'rs-med-btn rs-btn-light',
		name : 'cancel'
	}]
})
