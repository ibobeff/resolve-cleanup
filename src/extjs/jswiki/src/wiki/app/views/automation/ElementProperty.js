glu.defView('RS.wiki.automation.ElementProperty', {//DEPRECATED
	bodyPadding: '10',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'displayfield',
		name: 'nodeId'
	}, {
		xtype: 'displayfield',
		name: 'elementType'
	}, {
		xtype: 'displayfield',
		name: 'elementName'
	}],
	buttonAlign: 'left',
	buttons: ['cancel'],
	asWindow: {
		title: '@{title}',
		width: 300,
		modal: true
	}
});