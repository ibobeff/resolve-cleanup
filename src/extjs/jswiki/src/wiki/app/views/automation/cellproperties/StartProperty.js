glu.defView('RS.wiki.automation.StartProperty', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	cls : 'rs-grid-dark',
	//title: '~~start~~',
	items: [{
		xtype: 'textfield',
		name: 'label',
		allowBlank: false,
		readOnly: '@{..viewOnly}'
	},/*{
		xtype: 'radiogroup',
		layout: 'hbox',
		defaultType: 'radio',
		fieldLabel: '~~useVersion~~',
		cls: 'use-version',
		defaults: {
			readOnly: '@{..viewOnly}',
		},
		items: [{
			name: 'useVersion',
			boxLabel: '~~latest~~',
			value: '@{latest}'
		}, {
			padding: '0 0 0 20',
			name: 'useVersion',
			boxLabel: '~~latestStable~~',
			value: '@{latestStable}'
		}, {
			padding: '0 0 0 20',
			name: 'useVersion',
			boxLabel: '~~specificVersion~~',
			value: '@{specificVersion}'
		}]
	},*/{
		xtype: 'combo',
		readOnly: '@{..viewOnly}',
		displayField: 'name',
		valueField: 'value',
		name: 'merge',
		editable: false,
		store: '@{mergeStore}',
		queryMode: 'local'
	}, {
		flex: 1,
		title: '~~inputs~~',
		xtype: '@{inputs}'
	}]
});