glu.defView('RS.wiki.automation.EventProperty', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	cls : 'rs-grid-dark',
	//title: '~~event~~',
	items: [{
		xtype: 'textfield',
		readOnly: '@{..viewOnly}',
		name: 'label',
		allowBlank: false,
	},/*{
		xtype: 'radiogroup',
		layout: 'hbox',
		defaultType: 'radio',
		fieldLabel: '~~useVersion~~',
		cls: 'use-version',
		defaults: {
			readOnly: '@{..viewOnly}',
		},
		items: [{
			name: 'useVersion',
			boxLabel: '~~latest~~',
			value: '@{latest}'
		}, {
			padding: '0 0 0 20',
			name: 'useVersion',
			boxLabel: '~~latestStable~~',
			value: '@{latestStable}'
		}, {
			padding: '0 0 0 20',
			name: 'useVersion',
			boxLabel: '~~specificVersion~~',
			value: '@{specificVersion}'
		}]
	},*/{
		xtype: 'combo',
		displayField: 'name',
		valueField: 'value',
		name: 'merge',
		readOnly: '@{..viewOnly}',
		editable: false,
		store: '@{mergeStore}',
		queryMode: 'local'
	}, {
		title: '~~inputs~~',
		xtype: '@{inputs}'
	}]
});