glu.defView('RS.wiki.automation.RunbookMenu', {
	xtype: 'menu',
	initX: '@{initX}',
	initY: '@{initY}',
	items: ['searchRunbook', 'editRunbook', {
			xtype: 'menuseparator'
		},
		'editProperties'
	],
	listeners: {
		show: function() {
			this.setX(this.initX);
			this.setY(this.initY);
		},
		hide: function() {
			this.doClose();
		}
	}
});