glu.defView('RS.wiki.automation.TextProperty', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	//title: '~~text~~',
	items: [{
		xtype: 'textfield',
		name: 'label',
		readOnly: '@{..viewOnly}'
	}]
});