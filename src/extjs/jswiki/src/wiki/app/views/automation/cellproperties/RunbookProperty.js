glu.defView('RS.wiki.automation.RunbookProperty', {
	//title: '~~runbook~~',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	cls : 'rs-grid-dark',
	items: [{
		xtype: 'textfield',
		name: 'label',
		allowBlank: false,
		readOnly: '@{..viewOnly}',
		listeners: {
			change: '@{handleRunbookLabelChange}'
		}
	}, {
		xtype: 'fieldcontainer',
		layout: 'hbox',
		items: [{
			xtype: 'combo',
			flex: 1,
			name: 'runbook',
			readOnly: '@{..viewOnly}',
			store: '@{runbookStore}',
			trigger2Cls: 'x-form-search-trigger',
			enableKeyEvents: true,
			displayField: 'ufullname',
			valueField: 'ufullname',
			queryMode: 'remote',
			typeAhead: false,
			minChars: 1,
			queryDelay: 500,
			pageSize: 20,
			listConfig: {
				loadMask: false
			},
			onTrigger2Click: function() {
				this.fireEvent('searchRunbook');
			},
			listeners: {
				searchRunbook: '@{searchRunbook}',
				change : '@{handleRunbookChange}',
				select : '@{handleRunbookSelect}'
			}
		}, {
			name: 'jumpToRunbookIcon',
			text: '',
			xtype: 'button',
			padding: '5 0 0 5',
			baseCls: 'x-tool',
			iconCls: 'rs-social-button icon-external-link',
			tooltip: '~~editInNewTab~~',
			listeners: {
				handler: '@{jumpToRunbook}',
				render: function(image) {
					image.getEl().on('click', function() {
						image.fireEvent('handler')
					})
				}
			}
		}, {
			name: 'executeRunbookIcon',
			text: '',
			xtype: 'button',
			padding: '4 0 0 0',
			baseCls: 'x-tool',
			iconCls: 'rs-social-button icon-play',
			tooltip: '~~executeRunbook~~',
			handler: function() {
				this.up().up().fireEvent('executeRunbook', this);
			}
		}]
	}, 
	/* TODO - support Version Control
	{
		xtype: 'radiogroup',
		layout: 'hbox',
		defaultType: 'radio',
		fieldLabel: '~~useVersion~~',
		cls: 'use-version',
		defaults: {
			readOnly: '@{..viewOnly}',
			disabledCls: 'use-version-disabled'
		},
		items: [{
			name: 'useVersion',
			boxLabel: '~~latest~~',
			value: '@{latest}',
			disabled: '@{latestIsDisabled}',
			listeners: {
				change: '@{changeLatestVersion}'
			}
		}, {
			padding: '0 0 0 20',
			name: 'useVersion',
			boxLabel: '~~latestStable~~',
			value: '@{latestStable}',
			disabled: '@{latestStableIsDisabled}',
			listeners: {
				change: '@{changeLatestStableVersion}'
			}
		}, {
			padding: '0 0 0 20',
			name: 'useVersion',
			boxLabel: '~~specificVersion~~',
			value: '@{specificVersion}',
			disabled: '@{specificVersionIsDisabled}',
			listeners: {
				change: '@{changeSpecificVersion}'
			}
		}]
	}, {
		layout: 'hbox',
		hidden: '@{!showVersionNumberOption}',
		cls: 'version-number',
		defaults: {
			readOnly: '@{..viewOnly}',
			disabledCls: 'use-version-disabled',
		},
		items: [{
			xtype: 'combo',
			name: 'versionNumber',
			flex: 2,
			width: 20,
			labelWidth : 100,
			store: '@{versionHistoryStore}',
			displayField: 'version',
			valueField: 'version',
			editable: false,
			queryMode: 'local',
			listeners: {
				change: '@{updateVersionNumber}'
			}
		}, {
			xtype: 'toolbar',
			cls: 'version-toolbar',
			flex: 3,
			defaults: {
				disabled: '@{..viewOnly}',
			},
			items: [{
				xtype: 'button',
				name: 'details',
				text: '',
				iconCls: 'icon-large icon-list-ul rs-icon',
				cls: 'rs-med-btn rs-btn-light version-details-btn',
			}, {
				xtype: 'label',
				text: '~~details~~'
			}]
		}]
	}, 
	*/ 
	{
		title: '~~params~~',
		xtype: 'grid',
		cls : 'rs-grid-dark automation-subpanel-header',
		store: '@{parameterStore}',
		name: 'params',
		flex : 1,	
		dockedItems: [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items : [{
				name: 'addParam',				
				disabled: '@{..viewOnly}'
			}, {
				name: 'removeParams',				
				disabled: '@{..viewOnly}'
			}]
		}],
		viewConfig: {
			markDirty: false,
			listeners : {
				expandbody : function(row,r,expandR, e){
					// on row expansion (clicking the + icon), switch class to display full text with word wrap as necessary
					row.innerHTML = row.innerHTML.replace(/resolve_overflow_ellipsis/g, 'resolve_overflow_break-word');
					if (expandR.innerText != '') {
						// if there is description, add a separator (by replacing the default "resolve_param_desc" class with "resolve_param_desc_border" which has a top border)
						row.innerHTML = row.innerHTML.replace(/resolve_param_desc/g, 'resolve_param_desc_border');
					}
				},
				collapsebody : function(row,r,expandR, e){
					// on row collapse, revert to default classes
					row.innerHTML = row.innerHTML.replace(/resolve_overflow_break-word/g, 'resolve_overflow_ellipsis');
					row.innerHTML = row.innerHTML.replace(/resolve_param_desc_border/g, 'resolve_param_desc');
				}
			}
		},
		viewOnly: '@{..viewOnly}',
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 2,
			isOpen: false,
			listeners: {
				validateedit: function(editor, context, eOpts) {
					// determine if the current row is already opened (expanded)
					if (context.row.innerHTML.indexOf('resolve_overflow_break-word') != -1) {
						this.isOpen = true;
					}
					else {
						this.isOpen = false;
					}
				},
				edit: function(editor, context, eOpts) {
					// if current row is already opened, any edits will expand that text
					if (this.isOpen) {
						context.row.innerHTML = context.row.innerHTML.replace(/resolve_overflow_ellipsis/g, 'resolve_overflow_break-word');
					}
				},
				beforeedit: function( editor, context, eOpts ) {
					return !editor.view.up('grid').viewOnly;
				}
			}
		}, {
			ptype: 'rowexpander',
			expandOnDblClick: false,
			expandOnEnter: false,
			rowBodyTpl: new Ext.XTemplate('<div resolveId="resolveRowBody" style="color:#333" class="resolve_param_desc">{fieldTip}</div>')
		}],
		columns: [{
			header: '~~name~~',
			dataIndex: 'name',
			editor: {
				xtype: 'textfield'
			},
			renderer: function(value) {
				if (value) {
					return '<pre class="resolve_overflow_ellipsis">' + Ext.htmlEncode(value) + '</pre>';
				}
				else {
					return '';
				}
			}
		}, {
			header: '~~source~~',
			dataIndex: 'source',
			align: 'center',
			width: 115,
			editor: {
				xtype: 'combo',
				displayField: 'source',
				valueField: 'source',
				editable: false,
				store: '@{paramSource}'
			}
		}, {
			header: '~~sourceName~~',
			dataIndex: 'sourceName',
			flex: 1,
			editor: {
				xtype: 'textarea',
				listeners: {
					focus: function() {
						this.getFocusEl().dom.select()
					}
				}
			},
			renderer: function(value) {
				if (value) {
					return '<pre class="resolve_overflow_ellipsis">' + Ext.htmlEncode(value) + '</pre>'
				}
				else {
					return '';
				}
			}
		}],
		listeners: {
			render: function(grid) {
				grid.store.grid = grid;
			}
			/*
			beforerender: function() {
				var p = this.plugins[1];
				var me = this;
				this.getView().on('cellclick', function(view, td, cellIndex, record, tr, rowIndex, e) {
					if (cellIndex > 0) {
						p.onDblClick(view, record, null, rowIndex, e)
					}
				});
			}
			*/
		}
	}],
	listeners: {
		executeRunbook: function(btn) {
			this.fireEvent('openExecutionWindow', this, btn);
		},
		openExecutionWindow: '@{openExecutionWindow}'
	}
});