glu.defView('RS.wiki.automation.EdgeMenu', {
	xtype: 'menu',
	initX: '@{initX}',
	initY: '@{initY}',
	items: [{
		text: '~~dependency~~',
		menu: {
			xtype: 'menu',
			items: [{
				text: '~~execution~~',
				menu: {
					xtype: 'menu',
					parentPanel: '@{parentPanel}',
					executionValue: '@{executionValue}',
					defaultType: 'menucheckitem',
					defaults: {
						group: 'execution',
						handler: function() {
							this.up().parentPanel.fireEvent('edgeexecutiondependencychanged', this.name);
						}
					},
					listeners: {
						beforerender: function() {
							//hack for extjs 4.2.1 bug..up() returns null when clicking this menuitem
							this.items.each(function(item) {
								item.parentPanel = this.parentPanel;
								if (item.name == this.executionValue)
									item.checked = true;
								else
									item.checked = false;
							}, this)
						}
					},
					items: ['required', 'notRequired']
				}
			}, {
				text: '~~condition~~',
				menu: {
					parentPanel: '@{parentPanel}',
					xtype: 'menu',
					defaultType: 'menucheckitem',
					conditionValue: '@{conditionValue}',
					defaults: {
						group: 'condition',
						handler: function() {
							this.up().parentPanel.fireEvent('edgeconditiondependencychanged', this.name);
						}
					},
					listeners: {
						beforerender: function() {
							//hack for extjs 4.2.1 bug..up() returns null when clicking this menuitem
							this.items.each(function(item) {
								item.parentPanel = this.parentPanel;
								if (item.name == this.conditionValue)
									item.checked = true;
								else
									item.checked = false;
							}, this)
						}
					},
					items: ['good', 'bad', 'none']
				}
			}]
		}
	}, {
		text: '~~connector~~',
		menu: {
			xtype: 'menu',
			parentPanel: '@{parentPanel}',
			connectorValue: '@{connectorValue}',
			defaultType: 'menucheckitem',
			defaults: {
				group: 'connector',
				handler: function() {
					this.up().parentPanel.fireEvent('connectorchanged', this.name)
				}
			},
			listeners: {
				beforerender: function() {
					//hack for extjs 4.2.1 bug..up() returns null when clicking this menuitem
					this.items.each(function(item) {
						item.parentPanel = this.parentPanel;
						if (item.name == this.connectorValue)
							item.checked = true;
						else
							item.checked = false;
					}, this)
				}
			},
			items: ['straight', 'horizontal', 'vertical']
		}
	}],
	listeners: {
		show: function() {
			this.setX(this.initX);
			this.setY(this.initY);
		}
	}
})