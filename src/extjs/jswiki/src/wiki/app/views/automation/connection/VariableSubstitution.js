glu.defView('RS.wiki.automation.VariableSubstitution',{
	xtype : 'container',
	layout : {
		type : 'hbox',
		align : 'stretch'
	},
	defaults : {
		margin : '0 0 0 5'
	},
	items : [{		
		xtype : 'combobox',
		queryMode : 'local',
		store : '@{variableTypeStore}',
		displayField : 'type',
		valueField : 'type',
		value : '@{variableType}',
		editable : false,		
		width: 110,
		margin : 0 
	},{
		xtype : 'combobox',
		queryMode : 'local',
		value : '@{variableName}',
		displayField : 'name',
		valueField : 'name',
		store : '@{variableDataStore}',
		hidden : '@{variableIsText}',
		flex : 1
	},{
		xtype : 'textfield',
		value : '@{variableName}',
		hidden : '@{!variableIsText}',
		flex : 1
	},{
		xtype : 'button',
		name : 'add',
		cls : 'rs-small-btn rs-btn-dark'
	},{
		xtype : 'button',
		name : 'cancel',
		cls : 'rs-small-btn rs-btn-light'		
	}]
})