glu.defView('RS.wiki.automation.AbortModel', {
	parentLayout: 'graphEditor',
	elementPicker: '@{abortModelElementPicker}',
	imagePicker: '@{abortModelImagePicker}',
	connectionPicker : '@{connectionPicker}',
	propertyPanel: {
		layout: 'card',
		modelType: 'main',
		title: '~~properties~~',
		activeItem: '@{activeItem}',
		defaults: {
			bodyPadding: '10px'
		},
		items: '@{cellProperties}'
	}
})