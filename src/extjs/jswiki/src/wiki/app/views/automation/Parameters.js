glu.defView('RS.wiki.automation.Parameters', {
	xtype: 'grid',
	dockedItems: [{
		xtype : 'toolbar',
		cls : 'rs-dockedtoolbar',
		items : [{
			name: 'addParam',
			cls : 'rs-btn-light rs-small-btn',
			hidden: '@{..viewOnly}'
		}, {
			name: 'editParams',
			cls : 'rs-btn-light rs-small-btn',
			hidden: '@{..viewOnly}'
		}, {
			name: 'removeParams',
			cls : 'rs-btn-light rs-small-btn',
			hidden: '@{..viewOnly}'
		}, '->', {
			name: 'fakeInput',
			pressed: '@{pressInput}',
			hidden: '@{!showFakeTab}'
		}, {
			name: 'fakeOutput',
			pressed: '@{!pressInput}',
			hidden: '@{!showFakeTab}'
		}]
	}],
	hidden: '@{!showGrid}',
	store: '@{store}',
	name: 'parameters',
	cls : 'rs-grid-dark parametersGrid',
	sourceChangedInfoTxt: '~~sourceChanged~~',
	valueChangedInfoTxt: '~~valueChanged~~',
	valueProtectedInfoTxt: '~~valueProtected~~',
	userDefinedTxt: '~~userDefined~~',
	defaultParamsTxt: '~~defaultParams~~',
	customParamsTxt: '~~customParams~~',
	extractor: /^\$([^\{\}]+)\{([^\{\}]+)\}$/,
	viewConfig: {
		markDirty: false,
		listeners : {
			expandbody : function(row,r,expandR, e){
				// on row expansion (clicking the + icon), switch class to display full text with word wrap as necessary
				row.innerHTML = row.innerHTML.replace(/resolve_overflow_ellipsis/g, 'resolve_overflow_break-word');
				if (expandR.innerText != '') {
					// if there is description, add a separator (by replacing the default "resolve_param_desc" class with "resolve_param_desc_border" which has a top border)
					row.innerHTML = row.innerHTML.replace(/resolve_param_desc/g, 'resolve_param_desc_border');
				}
			},
			collapsebody : function(row,r,expandR, e){
				// on row collapse, revert to default classes
				row.innerHTML = row.innerHTML.replace(/resolve_overflow_break-word/g, 'resolve_overflow_ellipsis');
				row.innerHTML = row.innerHTML.replace(/resolve_param_desc_border/g, 'resolve_param_desc');
			}
		}
	},
	features: [{
		ftype: 'grouping',
		groupHeaderTpl: [
			'{name:this.IsDefault}', {
				IsDefault: function(isDefaultParam) {
					if (isDefaultParam) {
						return this.owner.view.up().defaultParamsTxt;
					} else {
						return this.owner.view.up().customParamsTxt;
					}
				}
			}
		],
		enableNoGroups: false,
		enableGroupingMenu : false,
	}],
	columns: [{
		header: '~~name~~',
		dataIndex: 'name',
		flex: 1,
		/*
		editor: {
			xtype: 'textfield',
			listeners: {
				focus: function(view, e, eOpts) {
					function selectText() {
						view.selectText();
					}
					setTimeout(selectText, 50);
				}
			}
		},
		*/
		renderer: function(value) {
			if (value) {
				return '<pre><span class="resolve_overflow_ellipsis">' + Ext.htmlEncode(value) + '</span></pre>';
			} else {
				return '';
			}
		}
	}, {
		header: '~~source~~',
		dataIndex: 'source',
		/*
		editor: {
			xtype: 'combo',
			displayField: 'source',
			valueField: 'source',
			editable: false,
			store: '@{paramSource}'
		},
		*/
		renderer: function(value, meta, record) {
			if (value) {
				if (value == 'CONSTANT') {
					value = this.userDefinedTxt;
					var v = record.get('value');
					var isConst = !this.extractor.test(v);
					if (!isConst) {
						var m = this.extractor.exec(v);
						var indexParamSrc = this._vm.paramSource.find('source', m[1], 0, false, false, true);
						if (indexParamSrc != -1) {
							value = m[1];
						}
					}
				}
				if (record.get('isDirty')) {
					meta.tdCls = 'modified-default-param';
					meta.tdAttr = 'data-qtip="'+this.sourceChangedInfoTxt+'"';
				}
				return '<pre class="resolve_overflow_ellipsis">' + Ext.htmlEncode(value) + '</pre>';
			} else {
				return '';
			}
		},
		width: 115,
		listeners: {
			render: function() {
				var vm = this.up().up()._vm;
				this.setText(vm.sourceTitle);
			}
		}
	}, {
		header: '~~value~~',
		dataIndex: 'value',
		flex: 1,
		/*
		getEditor: function(record, defaultField) {
			var m = this.up().up()._vm.parentVM;
			if (!this.comboForOutput)
				this.comboForOutput = Ext.create('Ext.grid.CellEditor', {
					field: Ext.create('Ext.form.ComboBox', {
						store: m.outputStore,
						displayField: 'name',
						valueField: 'name',
						queryMode: 'local'
					})
				});
			if (!this.textAreaForOthers)
				this.textAreaForOthers = Ext.create('Ext.grid.CellEditor', {
					field: Ext.create('Ext.form.TextArea')
				});
			if (record.get('source').toLowerCase() == 'output')
				return this.comboForOutput;
			return this.textAreaForOthers;
		},

		editor: {
			xtype: 'textarea',
			grow: true,
			anchor: '100%',
			listeners: {
				focus: function() {
					this.getFocusEl().dom.select()
				}
			}
		},
		*/
		renderer: function(value, meta, record) {
			if (record.get('encrypt'))
				return '*****'
			if (value) {
				if (record.get('protected')) {
					meta.tdCls = 'modified-default-param';
					meta.tdAttr = 'data-qtip="' + this.valueProtectedInfoTxt + '"';
				}
				else if (record.get('isDirty')) {
					meta.tdCls = 'modified-default-param';
					meta.tdAttr = 'data-qtip="'+this.valueChangedInfoTxt+'"';
				}
				var source = record.get('source');
				if (source == 'CONSTANT') {
					var isConst = !this.extractor.test(value);
					if (!isConst) {
						var m = this.extractor.exec(value);
						var indexParamSrc = this._vm.paramSource.find('source', m[1], 0, false, false, true);
						if (indexParamSrc != -1) {
							value = m[2];
						}
					}
				}
				return '<pre class="resolve_overflow_ellipsis">' + Ext.htmlEncode(value) + '</pre>';
			} else {
				return '';
			}
		},
		listeners: {
			render: function() {
				var vm = this.up().up()._vm;
				this.setText(vm.sourceValueTitle);
			}
		}
	}],
	viewOnly: '@{..viewOnly}',
	plugins: [
	/*
	{
		ptype: 'cellediting',
		clicksToEdit: 2,
		isOpen: false,
		listeners: {
			validateedit: function(editor, context, eOpts) {
				// determine if the current row is already opened (expanded)
				if (context.row.innerHTML.indexOf('resolve_overflow_break-word') != -1) {
					this.isOpen = true;
				} else {
					this.isOpen = false;
				}
			},
			edit: function(editor, context, eOpts) {
				var store = context.store;
				var row = store.getAt(context.rowIdx);
				if (row.get('isDefault')) {
					row.set('isDirty', row.dirty);
				}

				// if current row is already opened, any edits will expand that text
				if (this.isOpen) {
					context.row.innerHTML = context.row.innerHTML.replace(/resolve_overflow_ellipsis/g, 'resolve_overflow_break-word');
				}
			},
			beforeedit: function( editor, context, eOpts ) {
				return !editor.view.up('grid').viewOnly;
			}
		}
	},
	*/
	{
		ptype: 'rowexpander',
		expandOnDblClick: false,
		expandOnEnter: false,
		rowBodyTpl: new Ext.XTemplate('<div resolveId="resolveRowBody" style="color:#333;" class="resolve_param_desc">{[this.formatFields(values)]}</div>', {
			formatFields: function(values) {
				return Ext.htmlEncode(values.description);
			}
		})
	}],
	listeners: {
		render: function(grid) {
			grid.store.grid = grid;
			grid.on('celldblclick', function() {
				if (!this.viewOnly && this._vm.editParamsIsEnabled) {
					this.fireEvent('editParams');
				}
			})
		},
		editParams: '@{editParams}',
	}
})
