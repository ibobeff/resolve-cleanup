glu.defView('RS.wiki.automation.MainModel', {
	parentLayout: 'graphEditor',
	elementPicker: '@{mainModelElementPicker}',
	imagePicker: '@{mainModelImagePicker}',
	connectionPicker : '@{connectionPicker}',
	propertyPanel: {
		layout: 'card',
		modelType: 'main',
		title: '@{title}',
		activeItem: '@{activeItem}',
		defaults: {
			autoScroll: true,
			bodyPadding: '10 10 0 10'
		},
		items: '@{cellProperties}'
	}
});