glu.defView('RS.wiki.automation.NoProperty', {
	html : '~~noProperty~~'
});
glu.defView('RS.wiki.automation.RootProperty', {});
glu.defView('RS.wiki.automation.ContainerProperty', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	//title: '~~container~~',
	items: [{
		xtype: 'textfield',
		name: 'label',
		readOnly: '@{..viewOnly}'
	}]
});