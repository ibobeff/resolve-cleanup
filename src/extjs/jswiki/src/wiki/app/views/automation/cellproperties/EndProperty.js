glu.defView('RS.wiki.automation.EndProperty', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	cls : 'rs-grid-dark',
	//title: '~~end~~',
	items: [{
		xtype: 'textfield',
		name: 'label',
		allowBlank: false,
		readOnly: '@{..viewOnly}',
	},/*{
		xtype: 'radiogroup',
		layout: 'hbox',
		defaultType: 'radio',
		fieldLabel: '~~useVersion~~',
		cls: 'use-version',
		defaults: {
			readOnly: '@{..viewOnly}',
		},
		items: [{
			name: 'useVersion',
			boxLabel: '~~latest~~',
			value: '@{latest}'
		}, {
			padding: '0 0 0 20',
			name: 'useVersion',
			boxLabel: '~~latestStable~~',
			value: '@{latestStable}'
		}, {
			padding: '0 0 0 20',
			name: 'useVersion',
			boxLabel: '~~specificVersion~~',
			value: '@{specificVersion}'
		}]
	},*/{
		xtype: 'combo',
		displayField: 'name',
		valueField: 'value',
		name: 'merge',
		readOnly: '@{..viewOnly}',
		editable: false,
		store: '@{mergeStore}',
		queryMode: 'local'
	}, {
		flex : 1,
		title: '~~inputs~~',
		xtype: '@{inputs}'
	}]
});