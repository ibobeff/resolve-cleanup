glu.defView('RS.wiki.automation.HTTPKeyValEntry', {	
	readOnly : '@{..viewOnly}',
	layout: {
		type : 'hbox',
		align : 'middle stretch'
	},
	defaultType: 'combobox',	
	defaults : {
		hideLabel : true,
		hideTrigger: true,		
		valueField : 'value',
		displayField : 'value',
		allowBlank : true,
		readOnly : '@{..viewOnly}',		
		minChars : 1,
		tpl: Ext.create('Ext.XTemplate',
			'<tpl for=".">',
			'<div class="x-boundlist-item">{text}</div>',
			'</tpl>'
		),	
		cls : 'no-border',		
		listeners : {
			focus : function(field){
				if(this.readOnly)
					return;
				this._vm.focusHandler();
				setTimeout(this.expand.bind(this),0);
			}
		}
	},
	items: [{
		xtype: 'container',	
		hidden : '@{..viewOnly}',
		width : 20,		
		items : [{
			xtype : 'component',
			hidden : '@{isValid}',					
			style: {				
				'color': 'crimson'				
			},			
			autoEl: {				
				cls: 'icon-exclamation-sign rs-social-button',
				'data-qtip': 'Duplicated Key is not allowed.'
			}			
		}]
	},{
		name: 'key',
		emptyText : '~~key~~',
		margin : '0 10 0 0',
		store : '@{keySuggestionStore}',
		queryMode : '@{keyQueryMode}',
		queryDelay : 500,
		flex : 1	
	}, {
		name: 'value',
		emptyText : '~~value~~',
		margin : '0 10 0 0',
		store : '@{valueSuggestionStore}',
		queryMode : 'remote',
		queryDelay : 500,
		flex : 1
	},{
		xtype: 'container',	
		hidden : '@{..viewOnly}',		
		width : 20,	
		items : [{
			xtype : 'component',
			hidden : '@{isLast}',					
			style: {				
				'color': '#999',
				'cursor' : 'pointer'
			},			
			autoEl: {				
				cls: 'icon-minus-sign-alt rs-social-button'
			},
			listeners: {
				render: function() {					
					this.getEl().on('click', function(e) {
						e.stopPropagation();
						this.fireEvent('remove');
					}, this);
				},
				remove: '@{remove}'
			}	
		}]
	}],
	listeners : {
		render : function(){
			if(this.readOnly && this._vm.isLast)
				this.hide();
		}
	}
});