glu.defView('RS.wiki.automation.AnswerProperty', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults : {
		labelWidth : 120,
		margin : '4 0'
	},
	items: [{
		xtype : 'container',
		layout : {
			type : 'hbox',
			align : 'stretch'
		},
		items : [{
			labelWidth : 120,
			xtype: 'textfield',
			flex : 1,
			name: 'label',
			allowBlank: false,
			readOnly: '@{..viewOnly}'
		},{
			xtype : 'button',
			margin : '0 0 0 5',			
			cls : 'rs-small-btn rs-btn-dark',		
			name : 'enableAddVariable',
			text : '~~enableAddVariable~~',
		}]
	},{
		xtype : '@{variableForm}',
		margin : '4 0 4 125',
		hidden : '@{!..showVariableForm}',
	},{
		xtype: 'numberfield',
		name: 'order',
		readOnly: '@{..viewOnly}'
	}, {
		xtype : 'checkbox',
		disabled: '@{..viewOnly}',
		margin : '0 0 0 125',
		hideLabel : true,
		name : 'autoAnswer',		
		boxLabel : '~~autoAnswer~~'
	},{
		xtype : 'panel',
		title : '~~expression~~',
		cls : 'automation-subpanel-header',
		hidden : '@{!autoAnswer}',
		dockedItems : [{
			hidden: '@{..viewOnly}',
			xtype : 'toolbar',
			margin : '10 0 0 0',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items : ['new','edit','remove']
		}],
		layout: {
			type : 'vbox',
			align : 'stretch'
		},
		defaults : {
			flex : 1,
			margin : '10 0'
		},
		items : [{
			xtype : 'component',
			html: '~~noExpression~~',			
			hidden : '@{!noExpression}'
		},{
			xtype : 'grid',
			hidden : '@{noExpression}',
			cls : 'rs-grid-dark ',
			store : '@{expressionStore}',
			name : 'expression',
			columns : [{
				dataIndex : 'variableName',
				header : '~~variableName~~'
			},{
				dataIndex : 'operator',
				header : '~~operator~~',
			},{
				dataIndex : 'operand',
				header : '~~value~~',
				flex : 1
			}],
			viewConfig: {
				// This renderer to apply conditional css on each grid row based specific column value (dtcc status)
				getRowClass: function (record, rowIndex, rowParams, store) {
					var taskName = record.get('taskName');
					if(!taskName)
						return 'hide-row-expander';
				}							
			},
			plugins : [{
				ptype: 'rowexpander',
				expandOnDblClick: false,
				expandOnEnter: false,
				rowBodyTpl: new Ext.XTemplate("Value of <b>WSDATA{{variableName}}</b> is pulled from Action Task <b>{taskName}</b>, output <b>{outputName}</b>.")
			}]
		}]
	},{
		xtype: 'grid',
		margin : '4 0 0 0',
		flex : 1,
		autoScroll : true,
		cls : 'rs-grid-dark automation-subpanel-header',
		viewOnly: '@{..viewOnly}',
		title: '~~additionalData~~',
		store: '@{cdataStore}',
		name: 'answerdata',
		dockedItems: [{
			dock: 'top',
			xtype: 'toolbar',
			cls : 'rs-dockedtoolbar',
			disabled: '@{..viewOnly}',
			layout: {
				type: 'hbox'
			},
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: ['addData', 'removeData']
		}],
		plugins: [{
			ptype: 'cellediting',
			listeners: {
				beforeedit: function( editor, context ) {
					context.grid.edittingRecord = context.record;
					return !editor.view.up('grid').viewOnly;
				}
			},
			clicksToEdit: 1
		}],
		columns: [{
			header: '~~name~~',
			dataIndex: 'name',
			flex: 1,
			editor: {
				xtype: 'textfield',
				allowBlank: false,
				validator: function(val) {
					var grid = this.up('grid'),
						editingRecord = this.up('grid').edittingRecord,
						store = grid.store,
						scope = {val: val, editingRecord: editingRecord, valid: true};
					store.each(function(record) {
						if (record != this.editingRecord && record.get('name') == this.val) {
							this.valid = false;
							return false;
						}
					}, scope);
					return scope.valid? true: clientVM.localize('duplicateName');
				}
			}
		}, {
			header: '~~value~~',
			dataIndex: 'value',
			flex: 1,
			editor: {
				xtype: 'textfield'
			}
		}]
	}],
	listeners: {
		beforedestroy : '@{beforeDestroyComponent}'
	}
});
glu.defView('RS.wiki.automation.ExpressionForm',{
	modal : true,
	padding : 15,
	width : 700,
	height : 400,
	cls : 'rs-modal-popup',
	title : '@{expressionFieldSetTitle}',
	defaults : {
		labelWidth : 140,
		margin : '4 0',
	},
	layout: {
		type : 'vbox',
		align : 'stretch'
	},
	items : [{
		xtype : 'component',
		html : '~~variable~~'
	},{
		xtype : 'combo',	
		name : 'variableName',
		store : '@{wskeyStore}',
		queryMode : 'local',
		displayField : 'text',
		valueField : 'text',
		margin : '4 0 4 20',
		labelWidth : 120,
		emptyText : '~~variableNameEmptyText~~'
	},{
		xtype : 'checkbox',
		name : 'outputFromTask',
		hideLabel : true,
		boxLabel : '~~outputFromTask~~',
		margin : '4 0 4 40'
	},{
		xtype: 'combo',		
		name: 'taskName',
		labelWidth : 100,
		margin : '4 0 4 40',
		disabled : '@{!outputFromTask}',
		emptyText : '~~taskNameEmptyText~~',		
		store: '@{taskStore}',
		trigger2Cls: 'x-form-search-trigger',		
		displayField: 'ufullName',
		valueField: 'ufullName',
		queryMode: 'remote',
		typeAhead: false,
		minChars: 1,
		queryDelay: 500,		
		keyDelay : 1,
		//This will fix the issue where value in control in glu is out of sync with what send to server.
		ignoreSetValueOnLoad : true,			
		onTrigger2Click: function() {
			this.fireEvent('searchTask');
		},
		listeners: {				
			searchTask: '@{searchTask}'
		}
	},{
		xtype : 'combo',
		margin : '4 0 4 40',
		labelWidth : 100,
		disabled : '@{!outputFromTask}',
		emptyText : '~~outputNameEmptyText~~',
		store : '@{outputStore}',
		editable: false,
		name : 'outputName',
		valueField : 'uname',
		displayField : 'uname',
		queryMode: 'local',
	},{
		xtype: 'combobox',
		name: 'operator',
		labelStyle:'font-weight:bold',
		valueField : 'value',
		emptyText: '~~selectOperator~~',
		store: '@{operatorStore}',
		editable: false
	},{
		xtype : 'container',
		layout : {
			type : 'hbox',
			align : 'stretch'
		},
		items : [{
			xtype : 'combo',
			labelWidth : 140,
			width : 260,		
			store : '@{operandTypeStore}',
			name : 'operandType',
			labelStyle:'font-weight:bold',
			valueField : 'type',
			displayField : 'type',
			queryMode : 'local',
			editable : false			
		},{
			xtype : 'textfield',
			hidden : '@{!operandIsText}',
			margin : '0 0 0 5',
			flex : 1,
			name : 'operand',
			hideLabel : true,
			emptyText : '~~operandEmptyText~~'
		},{
			xtype : 'combo',
			hidden : '@{operandIsText}',
			store : '@{operandStore}',
			queryMode : 'local',
			displayField : 'text',
			valueField : 'text',
			margin : '0 0 0 5',
			flex : 1,
			name : 'operand',
			hideLabel : true,
			emptyText : '~~operandEmptyText~~'
		}]
	},{
		xtype : 'displayfield',
		value : '~~duplicatedKey~~',				
		fieldStyle : 'color:red;',
		margin : '0 0 0 145',
		hidden : '@{duplicatedKeyIsHidden}'
	}],
	buttons : [{
		name : 'updateExpression',
		text : '@{updateExpressionBtnText}',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name :'cancelAddExpression',
		cls : 'rs-med-btn rs-btn-light'
	}]
})
