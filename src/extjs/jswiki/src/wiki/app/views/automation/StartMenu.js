glu.defView('RS.wiki.automation.StartMenu', {
	xtype: 'menu',
	initX: '@{initX}',
	initY: '@{initY}',
	items: [{
			text: '~~dependency~~',
			menu: {
				xtype: 'menu',
				items: [{
					text: '~~merge~~',
					menu: {
						parentPanel: '@{parentPanel}',
						mergeValue: '@{mergeValue}',
						defaultType: 'menucheckitem',
						xtype: 'menu',
						defaults: {
							group: 'merge',
							handler: function() {
								this.parentPanel.fireEvent('taskdependencychanged', this.name);
							}
						},
						listeners: {
							beforerender: function() {
								//hack for extjs 4.2.1 bug..up() returns null when clicking this menuitem
								this.items.each(function(item) {
									item.parentPanel = this.parentPanel;
									if (item.name == this.mergeValue)
										item.checked = true;
									else
										item.checked = false;
								}, this)
							}
						},
						items: ['all', 'any', 'targets']
					}
				}]
			}
		},
		'editInputs', {
			xtype: 'menuseparator'
		},
		'editProperties'
	],
	listeners: {
		show: function() {
			this.setX(this.initX);
			this.setY(this.initY);
		},
		hide: function() {
			this.doClose();
		}
	}
});