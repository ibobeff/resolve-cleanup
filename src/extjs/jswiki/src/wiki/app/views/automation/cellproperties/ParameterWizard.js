glu.defView('RS.wiki.automation.ParameterWizard',{
	xtype : 'container',	
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		flex : 1,
	},
	items : [{
		xtype : '@{httpTaskConfig}',
		hidden : '@{!..taskIsHTTP}'
	},{
		xtype : '@{networkTaskConfig}',
		hidden : '@{..taskIsHTTP}'
	}]
})