glu.defView('RS.wiki.automation.DocumentProperty', {
	//title: '~~document~~',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'fieldcontainer',
		layout: 'hbox',
		items: [{
			xtype: 'combo',
			flex: 1,
			name: 'wiki',
			readOnly: '@{..viewOnly}',
			store: '@{documentStore}',
			trigger2Cls: 'x-form-search-trigger',
			enableKeyEvents: true,
			displayField: 'ufullname',
			valueField: 'ufullname',
			queryMode: 'remote',
			typeAhead: false,
			minChars: 1,
			queryDelay: 500,
			pageSize: 20,
			onTrigger2Click: function() {
				this.fireEvent('searchDecisionTree');
			},
			listeners: {
				searchDecisionTree: '@{searchDecisionTree}'
			}
		}, {
			text: '',
			xtype: 'button',
			padding: '5 0 0 5',
			baseCls: 'x-tool',		
			iconCls: 'rs-social-button icon-external-link',
			tooltip: '~~editInNewTab~~',
			hidden: '@{!jumpToWikiIsVisible}',
			listeners: {
				handler: '@{jumpToWiki}',
				render: function(image) {
					image.getEl().on('click', function() {
						image.fireEvent('handler')
					})
				}
			}
		}]
	},/*{
		xtype: 'radiogroup',
		layout: 'hbox',
		defaultType: 'radio',
		fieldLabel: '~~useVersion~~',
		cls: 'use-version',
		hidden: '@{!jumpToWikiIsVisible}',
		defaults: {
			readOnly: '@{..viewOnly}',
		},
		items: [{
			name: 'useVersion',
			boxLabel: '~~latest~~',
			value: '@{latest}'
		}, {
			padding: '0 0 0 20',
			name: 'useVersion',
			boxLabel: '~~latestStable~~',
			value: '@{latestStable}'
		}, {
			padding: '0 0 0 20',
			name: 'useVersion',
			boxLabel: '~~specificVersion~~',
			value: '@{specificVersion}'
		}]
	}*/]
});
