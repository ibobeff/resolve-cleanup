glu.defView('RS.wiki.automation.NewTask', {
	//title: '~~task~~',
	disabled: '@{..viewOnly}',
	cls: 'automation-panel',
	items: [{
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			// wizard step 1: select a task or create new task
			layout: 'hbox',
			items: [{
				padding: '10 5 10 10',
				xtype: 'text',
				text: '1.',
			}, {
				xtype: 'button',
				name: 'selectActionTask',
				scale: 'medium',
				pressed: '@{isSelectTask}'
			}, {
				padding: '10',
				xtype: 'text',
				text: '~~or~~',
			}, {
				xtype: 'button',
				name: 'createActionTask',
				scale: 'medium',
				pressed: '@{isCreateTask}'
			}]
		}, {
			// select task - wizard step 2: search for task
			xtype: 'fieldcontainer',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			hidden: '@{selectTaskHidden}',
			padding: '10 10 0 0',
			items: [{
				xtype: 'combo',
				padding: '0 0 0 10',
				labelWidth : 110,
				flex: 1,
				name: 'taskName',
				fieldLabel: '~~newTaskName~~',
				store: '@{taskStore}',
				trigger2Cls: 'x-form-search-trigger',
				enableKeyEvents: true,
				displayField: 'ufullName',
				valueField: 'ufullName',
				queryMode: 'remote',
				typeAhead: false,
				minChars: 1,
				queryDelay: 500,
				pageSize: 20,
				//This will fix the issue where value in control in glu is out of sync with what send to server.
				ignoreSetValueOnLoad : true,		
				onTrigger2Click: function() {
					this.fireEvent('searchTask');
				},
				listeners: {
					searchTask: '@{searchTask}'
				}
			}, 
			/* TODO - support Version Control
			{
				xtype: 'radiogroup',
				layout: 'hbox',
				defaultType: 'radio',
				defaults: {
					disabledCls: 'use-version-disabled',
				},	
				fieldLabel: '~~useVersion~~',
				hidden: '@{!showUseVersionOption}',
				padding: '0 0 0 28',
				cls: 'selectedtask-version',
				items: [{
					name: 'useVersion',
					boxLabel: '~~latest~~',
					value: '@{latest}',
					disabled: '@{latestIsDisabled}'
				}, {
					padding: '0 0 0 20',
					name: 'useVersion',
					boxLabel: '~~latestStable~~',
					value: '@{latestStable}',
					disabled: '@{latestStableIsDisabled}'
				}, {
					padding: '0 0 0 20',
					name: 'useVersion',
					boxLabel: '~~specificVersion~~',
					value: '@{specificVersion}',
					disabled: '@{specificVersionIsDisabled}'
				}]
			}, {
				layout: 'hbox',
				hidden: '@{!showVersionNumberOption}',
				cls: 'version-number',
				items: [{
					xtype: 'combo',
					name: 'versionNumber',
					hidden: '@{!showVersionNumberOption}',
					padding: '0 0 0 28',
					flex: 2,
					width: 20,
					labelWidth : 92,
					store: '@{versionHistoryStore}',
					displayField: 'version',
					valueField: 'version',
					editable: false,
					queryMode: 'local'
				}, {
					xtype: 'toolbar',
					cls: 'version-toolbar',
					flex: 3,
					items: [{
						xtype: 'button',
						name: 'details',
						text: '',
						iconCls: 'icon-large icon-list-ul rs-icon',
						cls: 'rs-med-btn rs-btn-light version-details-btn',
					}, {
						xtype: 'label',
						text: '~~details~~'
					}]
				}]
			}, 
			*/
			{
				xtype: 'panel',
				margin: '8 0 0 125',
				items: [{
					xtype: 'button',
					name: 'done',
					//cls: 'rs-med-btn rs-btn-dark',
					margin: '0 15 0 0',
					disabled: '@{!isDoneValid}',
					scale: 'medium'
				}, {
					xtype: 'button',
					name: 'cancel',
					//cls: 'rs-med-btn rs-btn-light',
					scale: 'medium'
				}]
			}]
		}, { 
			// create task
			xtype: 'form',
			hidden: '@{createTaskHidden}',
			padding: '10 10 0 10',
			defaults : {
				labelWidth : 183,
				margin : '4 0',
			},
			layout: {
				type: 'vbox',
				align: 'stretch'
			}, 
			items: [{
				// create task - wizard step 2: prompt to enter namespace & name
				xtype: 'text',
				text: '~~enterTaskName~~'
			}, {
				// create task - wizard step 2: enter new task namespace and name
				margin: '4 0 0 18',
				defaults : {
					labelWidth : 164,
				},
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [{
					xtype: 'textfield',
					name: 'namespace',
					allowBlank: false
				}, {
					xtype: 'textfield',
					name: 'name',
					allowBlank: false
				}, {
					xtype: 'textarea',
					name: 'description',
					rows: 2
				}, {
					xtype: 'numberfield',
					cls: 'no-input-border',
					name: 'taskTimeout',
					minValue: 1
				}]
			}, {
				// create task - wizard step 3: select task template
				xtype: 'combobox',
				name: 'selectTemplateType',
				store: '@{templateStore}',
				displayField: 'name',
				valueField: 'value',
				queryMode: 'local',
				hidden: '@{!isNamespaceAndNameValid}',
				editable: false
			}, {
				// create task / generic action task template - wizard step 4: prompt to enter task type
				xtype: 'combobox',
				name: 'type',
				fieldLabel: '~~enterPropertiesPrompt~~',
				store: '@{typeStore}',
				displayField: 'name',
				valueField: 'value',
				queryMode: 'local',
				hidden: '@{!isCustomActionTaskValid}',
				editable: false
			}, {
			 	// create task / restAPI over HTTP - wizard step 4
				xtype: 'container',
				hidden: '@{!isHTTPConnectionActionTaskValid}',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [{
					xtype: 'component',
					html: '~~enterRequestProperties~~',
				},{
					xtype : 'textfield',
					name : 'sessionName',
					labelWidth : 164,
					margin: '10 0 8 18',
					fieldLabel: '~~sessionNameOptional~~'
				},{					
					xtype: '@{HTTPNewTaskConfig}',
					margin: '0 0 0 18',
				}]
			}, {
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaults : {
					labelWidth : 183,
					//margin : '4 0',
				},
				items:[{
					// create task / enter command over network template
					hidden: '@{!isNetworkCommandActionTask}',
					defaults : {
						labelWidth : 183
					},
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items:[{
						// create task / enter command over network template - wizard step 4: select connection type
						xtype: 'combobox',
						name: 'networkConnectionType',
						fieldLabel: '~~selectConnectionType~~',
						cls: 'no-input-border',
						editable: false,
						displayField: 'name',
						valueField: 'value',
						queryMode: 'local',
						value : '@{networkConnectionType}',
						store: '@{networkConnectionTypeStore}'
					}, {
						// create task / enter command over network template - wizard step 5: enter command
						xtype: 'text',
						hidden: '@{!networkConnectionTypeValid}',
						text: '~~enterNetworkCommandPrompt~~',
						margin : '4 0'
					}]
				},{
					// create task / enter command locally template - wizard step 4: enter command
					hidden: '@{!isLocalCommandActionTask}',
					xtype: 'text',
					text: '~~enterLocalCommandPrompt~~',
					margin : '4 0'
				}, 
				{
					hidden: '@{!isCommandActionTaskValid}',
					margin: '4 0 0 18',
					defaults : {
						labelWidth : 164,
					},
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: [{
						xtype : 'textfield',
						name : 'sessionName',
						fieldLabel: '~~sessionNameOptional~~',
						hidden: '@{!isNetworkCommandActionTask}',
					},{
						margin: '0 0 4 0',
						layout: {
							type: 'hbox',
							//align: 'stretch'
						},
						defaults : {
							labelWidth : 164,
						},
						items: [{
							flex: 2,
							xtype: 'textarea',
							grow: true,
							growMin: 24,
							growPad: -8,
							growAppend: '',
							growMax: 90,
							name: 'command',
							//allowBlank: false,
							margin : '0 5 0 0'
						}, {			
							xtype: 'button',
							name: 'addVariable',
							cls : 'rs-small-btn rs-btn-dark'
						}]
					}, {
						hidden: '@{!addVariableIsPressed}',
						items: [{
							xtype : 'fieldset',
							padding : 10,
							title : '~~variableArg~~',
							defaultType : 'container',
							layout: {
								type: 'hbox',
								//align: 'stretch',
							},
							defaults: {
								hideLabel: true,
								margin : '0 5 0 0'
							},		
							items: [{
								flex: 1,			
								width : 200,
								xtype: 'combo',
								editable: false,
								name: 'addVarSourceType',
								maxLength: '@{comboValueMaxChar}',
								enforceMaxLength: true,
								displayField: 'source',
								valueField: 'source',
								store: '@{addVarParamSource}',
							}, {
								// textarea for FLOWS, WSDATA, INPUT
								flex: 2,
								xtype: 'textarea',
								cls: 'parametersTextArea',
								rows: 1,
								hideLabel: true,
								allowBlank: false,
								name: 'addVarSourceValue',
								hidden: '@{!addVarTextboxValueEnabled}'
							}, {
								// dropdown list for PROPERTY
								flex: 2,
								xtype: 'combo',
								name: 'addVarSourceValue',
								maxLength: '@{comboValueMaxChar}',
								enforceMaxLength: true,
								displayField: 'name',
								valueField: 'name',
								hideLabel: true,
								allowBlank: false,
								store: '@{propertyStore}',
								queryMode: 'local',
								hidden: '@{!addVarPropertyValueEnabled}'
							}, {
								// dropdown list for PARAM
								flex: 2,
								xtype: 'combo',
								name: 'addVarSourceValue',
								maxLength: '@{comboValueMaxChar}',
								enforceMaxLength: true,
								displayField: 'name',
								valueField: 'name',
								hideLabel: true,
								allowBlank: false,
								store: '@{paramsStore}',
								queryMode: 'local',
								hidden: '@{!addVarParamsStoreEnabled}'
							}, {
								// dropdown list for OUTPUT
								flex: 2,
								xtype: 'combo',
								name: 'addVarSourceValue',
								maxLength: '@{comboValueMaxChar}',
								enforceMaxLength: true,
								displayField: 'name',
								valueField: 'name',
								hideLabel: true,
								allowBlank: false,
								store: '@{outputStore}',
								queryMode: 'local',
								hidden: '@{!addVarOutputStoreEnabled}'
							}, {
								xtype: 'button',
								name: 'addVariableBtn',
								cls : 'rs-small-btn rs-btn-dark',
								disabled: '@{!addVariableBtnIsEnabled}',
							}, {
								xtype: 'button',
								cls : 'rs-small-btn rs-btn-light',
								name: 'cancelVariableBtn',
								margin : '0'
							}]
						}]
					}, {
						xtype: 'numberfield',
						cls: 'no-input-border',
						name: 'expectedTimeout',
						minValue: 0,
						hidden: '@{!isNetworkCommandActionTask}'
					}, {
						xtype: 'textfield',
						name: 'commandPrompt',
						hidden: '@{!isNetworkCommandActionTask}'
					}, {
						xtype: 'panel',
						layout: {
							type: 'hbox',
							align: 'stretch',
						},
						items: [{
							xtype: 'combobox',
							name: 'optionName',
							emptyText: '~~addOption~~',
							store: '@{optionsStore}',
							value: '@{optionName}',
							editable: false,
							allowBlank: true,
							queryMode: 'local',
							hideLabel: true,
							displayField: 'name',
							valueField: 'name',			
							cls: 'no-input-border combo',
							width: 164,
							listeners: {
								select: function (combobox) {
									if (combobox.value == 'None') {
										combobox.setRawValue('');
									}
								},
							}
						}, {
							flex: 1,
							xtype: 'textfield',
							hideLabel: true,
							name: 'optionValue',
							emptyText: '~~enterOptionValue~~',
							padding: '0 0 0 5',
							disabled: '@{optionValueDisabled}',
						}]
					}]
				}]
			}, {
				xtype: 'panel',
				margin: '8 0 0 187',
				items: [{
					xtype: 'button',
					name: 'create',
					//cls: 'rs-med-btn rs-btn-dark',
					margin: '0 15 0 0',
					disabled: '@{!isCreateValid}',
					scale: 'medium'
				}, {
					xtype: 'button',
					name: 'cancel',
					//cls: 'rs-med-btn rs-btn-light'
					scale: 'medium'
				}]
			}]
		}] 
	}]
});