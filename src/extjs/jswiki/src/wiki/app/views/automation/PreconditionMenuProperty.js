glu.defView('RS.wiki.automation.PreconditionMenuProperty', {
	bodyPadding: '10',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'displayfield',
		name: 'nodeId'
	}, {
		xtype: 'displayfield',
		name: 'elementType'
	}, {
		xtype: 'displayfield',
		name: 'elementName'
	}, {
		xtype: 'textarea',
		name: 'expression'
	}],
	buttonAlign: 'left',
	buttons: ['dump', 'cancel'],
	asWindow: {
		title: '@{title}',
		width: 500,
		modal: true
	}
})