glu.defView('RS.wiki.automation.ConnectorProperty', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'textfield',
		name: 'label',
		allowBlank: false,
		readOnly: '@{..viewOnly}'
	}, {
		xtype: 'combo',
		displayField: 'name',
		valueField: 'value',
		name: 'condition',
		readOnly: '@{..viewOnly}',
		editable: false,
		store: '@{conditionStore}',
		queryMode: 'local'
	}, {
		xtype: 'radiogroup',
		disabled: '@{..viewOnly}',
		layout: 'hbox',
		defaultType: 'radiofield',
		fieldLabel: '~~completion~~',
		items: [{
			name: 'completionRadio',
			boxLabel: '~~required~~',
			value: '@{required}'
		}, {
			padding: '0 0 0 10',
			name: 'completionRadio',
			boxLabel: '~~notRequired~~',
			value: '@{notRequired}'
		}]
	}]
});