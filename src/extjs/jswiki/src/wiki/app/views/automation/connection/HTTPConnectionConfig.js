glu.defView('RS.wiki.automation.HTTPConnectionConfig',{
	title: '~~httpConfigTitle~~',
	modal: true,
	width: 1000,
	height : 650,
	id : 'rs-connection-config',
	cls : 'rs-modal-popup',	
	defaults : {	
		bodyPadding : '10 15 5 15'
	},
	layout : 'card',
	activeItem : '@{activeStep}',
	dockedItems : [{
		xtype : 'container',
		width : 200,	
		style : 'background : #8c8a8a;',
		dock : 'left',	
		layout : {
			type :'vbox',
			align : 'stretch'
		},
		items : [{
			xtype : 'container',
			/*html : '<div class="connectionWizardHeader">Settings</div>'*/
		},{
			xtype : 'button',
			name : 'connectionWizardStep',
			cls : 'connectionWizardButton'
		},{
			xtype : 'button',
			name : 'taskWizardStep',		
			cls : 'connectionWizardButton'
		}]
	},{
		xtype : 'toolbar',
		dock : 'bottom',
		style : 'border-top: 1px solid #b5b8c8 !important;',
		padding : '8 15',
		items : ['->',{
			name : 'next',			
			//cls : 'rs-med-btn rs-btn-dark wizardNavButton',
			cls : 'rs-med-btn rs-btn-dark',		
		},{
			name : 'done',	
			//cls : 'rs-med-btn rs-btn-dark wizardNavButton',		
			cls : 'rs-med-btn rs-btn-dark', 
		},{
			name : 'cancel',
			//width : 80,
			cls : 'rs-med-btn rs-btn-light',
			//cls : 'wizardCancelButton', 
		}]
	}],	
	items : [{
		itemId : 'mainConfig',			
		items : [
		{		
			xtype : 'container',		
			defaultType : 'container',
			layout: {
				type : 'vbox',
				align : 'stretch'
			},
			defaults : {
				labelWidth : 120,
				margin : '4 0',	
				defaultType : 'combobox',
				layout: {
					type : 'hbox',
					align : 'stretch'
				},			
				defaults : {				
					flex : 1,
					hideLabel : true,
					labelWidth : 120,
					margin : '0 0 0 5'	
				}
			},
			items: [{
				xtype : 'combobox',		
				store: '@{protocolStore}',
				queryMode: 'local',
				displayField: 'name',
				valueField: 'value',
				editable : false,
				name: 'protocol'
			},{				
				items : [
				{
					fieldLabel : '~~host~~',
					displayField : 'name',
					valueField : 'value',
					name : 'hostSource',
					store : '@{sourceStore}',
					editable : false,
					hideLabel : false,			
					margin : 0,					
					maxWidth : 270
				},{
					xtype: 'textfield',
					name: 'host',
					hidden: '@{!hostIsText}'
				},{				
					name: 'host',
					store: '@{paramStore}',
					queryMode: 'local',
					displayField : 'name',
					valueField : 'name',
					hidden: '@{!hostIsParam}'
				},{
					name: 'host',
					store: '@{propertyStore}',
					queryMode : 'local',		
					minChars: 0,
					displayField: 'uname',
					valueField: 'uname',
					hidden: '@{!hostIsProperty}'
				}]
			},{		
				items : [
				{
					fieldLabel : '~~port~~',
					displayField : 'name',
					valueField : 'value',			
					name: 'portSource',
					store: '@{sourceStore}',
					editable: false,
					hideLabel : false,			
					margin : 0,
					maxWidth : 270
				},{
					xtype: 'textfield',
					name: 'port',
					hidden: '@{!portIsText}'
				},{
					name: 'port',
					store: '@{paramStore}',
					queryMode: 'local',
					displayField : 'name',
					valueField : 'name',
					hidden: '@{!portIsParam}'
				},{
					name: 'port',
					store: '@{propertyStore}',				
					minChars: 0,
					displayField: 'uname',
					valueField: 'uname',
					queryMode : 'local',					
					hidden: '@{!portIsProperty}'
				}]
			},{
				xtype: 'textfield',
				name: 'sessionName'			
			}]
		}]
	},{
		xtype : '@{taskWizard}'		
	}]	
});