glu.defView("RS.wiki.automation.TaskWizard",{
	layout : 'fit',
	cls : 'rs-body',
	items : [{
		hidden : '@{useActionTask}',	
		padding : 50,		
		items : [{
			xtype : 'component',
			cls : 'rs-displayfield-value',		
			html : 'An actiontask can be optionally added to this connection.'
		},{
			xtype : 'component',
			cls : 'rs-displayfield-value',		
			html : '@{taskInfo}',
			margin : '15 0'
		},{
			layout : 'hbox',
			items : [{
				xtype : 'component',
				html : 'You can&nbsp;'
			},{		
				xtype : 'button',
				name : 'openTaskPicker',
				cls : 'rs-small-btn rs-btn-dark'
			},{
				xtype : 'component',			
				html : '&nbsp;from existing actiontask list, or&nbsp;'
			},{		
				xtype : 'button',
				name : 'createNewTask',
				cls : 'rs-small-btn rs-btn-dark'
			}]
		}]
	},{
		hidden : '@{!useActionTask}',
		layout : 'card',
		activeItem : '@{activeTaskWizard}',
		items : [{
			xtype : '@{taskWizardProperty}'
		},{
			xtype : '@{newTaskWizardProperty}',	
		}]
	}]	
})