glu.defView('RS.wiki.automation.TaskProperty', {
	//title: '~~task~~',	
	itemId: 'TaskProperty',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	overflowY: 'auto',
	items: [{
		xtype: 'textfield',
		name: 'label',
		allowBlank: false,
		readOnly: '@{..viewOnly}'
	},{
		xtype: 'fieldcontainer',
		layout: 'hbox',
		items: [{
			xtype: 'combo',
			flex: 1,
			name: 'taskName',
			readOnly: '@{..viewOnly}',
			store: '@{taskStore}',
			trigger2Cls: 'x-form-search-trigger',		
			displayField: 'ufullName',
			valueField: 'ufullName',
			queryMode: 'remote',
			typeAhead: false,
			minChars: 1,
			queryDelay: 500,		
			keyDelay : 1,
			//This will fix the issue where value in control in glu is out of sync with what send to server.
			ignoreSetValueOnLoad : true,			
			onTrigger2Click: function() {
				this.fireEvent('searchTask');
			},
			listeners: {				
				searchTask: '@{searchTask}'
			}
		},{
			xtype: 'button',
			padding: '5 0 0 5',
			baseCls: 'x-tool',
			iconCls: 'rs-social-button icon-external-link',
			tooltip: '~~editInNewTab~~',
			hidden: '@{!jumpToTaskIsVisible}',
			listeners: {
				handler: '@{jumpToTask}',
				render: function(image) {
					image.getEl().on('click', function() {
						image.fireEvent('handler')
					})
				}
			}
		},{
			xtype: 'button',
			padding: '4 0 0 0',
			baseCls: 'x-tool',
			iconCls: 'rs-social-button icon-play',
			tooltip: '~~executeTask~~',
			hidden: '@{!taskAuthorized}',
			handler: function() {
				this.up('#TaskProperty').fireEvent('executeTask', this);
			}
		}]
	}, 
	/* TODO - support Version Control
	{
		xtype: 'radiogroup',
		layout: 'hbox',
		defaultType: 'radio',
		fieldLabel: '~~useVersion~~',
		cls: 'use-version',
		defaults: {
			readOnly: '@{..viewOnly}',
			disabledCls: 'use-version-disabled'
		},
		items: [{
			name: 'useVersion',
			boxLabel: '~~latest~~',
			value: '@{latest}',
			disabled: '@{latestIsDisabled}',
			listeners: {
				change: '@{changeLatestVersion}'
			}
		}, {
			padding: '0 0 0 20',
			name: 'useVersion',
			boxLabel: '~~latestStable~~',
			value: '@{latestStable}',
			disabled: '@{latestStableIsDisabled}',
			listeners: {
				change: '@{changeLatestStableVersion}'
			}
		}, {
			padding: '0 0 0 20',
			name: 'useVersion',
			boxLabel: '~~specificVersion~~',
			value: '@{specificVersion}',
			disabled: '@{specificVersionIsDisabled}',
			listeners: {
				change: '@{changeSpecificVersion}'
			}
		}]
	}, {
		layout: 'hbox',
		hidden: '@{!showVersionNumberOption}',
		cls: 'version-number',
		defaults: {
			readOnly: '@{..viewOnly}',
			disabledCls: 'use-version-disabled',
		},
		padding: '0 0 5 0',
		items: [{
			xtype: 'combo',
			name: 'versionNumber',
			flex: 2,
			width: 20,
			labelWidth : 100,
			store: '@{versionHistoryStore}',
			displayField: 'version',
			valueField: 'version',
			editable: false,
			queryMode: 'local',
			listeners: {
				change: '@{updateVersionNumber}'
			}
		}, {
			xtype: 'toolbar',
			cls: 'version-toolbar',
			flex: 3,
			defaults: {
				disabled: '@{..viewOnly}',
			},
			items: [{
				xtype: 'button',
				name: 'details',
				text: '',
				iconCls: 'icon-large icon-list-ul rs-icon',
				cls: 'rs-med-btn rs-btn-light version-details-btn',
			}, {
				xtype: 'label',
				text: '~~details~~'
			}]
		}]
	}, 
	*/
	{
		xtype: 'combo',
		readOnly: '@{..viewOnly}',
		displayField: 'name',
		valueField: 'value',
		name: 'merge',
		editable: false,
		store: '@{mergeStore}',
		queryMode: 'local',		
	},{
		cls: 'view-actiontask-controls',
		margin: '10 0 0 0',
		items: [{
			xtype: 'toolbar',
			items: ['->', {
				// workaround to make toolbar take up space when not viewing/editing actiontaskbuilder
				disabled: true
			}, {
				name: 'expandAll',
				hidden: '@{expandAllDisabled}',
				iconCls: 'rs-social-button icon-expand-alt',
			}, {
				name: 'collapseAll',
				hidden: '@{collapseAllDisabled}',
				iconCls: 'rs-social-button icon-collapse-alt',
			}, {
				name: 'editActionTask',
				hidden: '@{editActionTaskDisabled}',
				iconCls: 'rs-social-button icon-edit',
			}, {
				name: 'doneEditActionTask',
				hidden: '@{doneEditActionTaskDisabled}',
				iconCls: 'rs-social-button icon-check-sign',
			}]
		}]
	},{
		xtype: 'tabpanel',
		flex : 1,	
		activeTab: '@{activeTab}',
		plain: true,	
		margin: '-25 0 0 0',
		items: [{
			xtype: 'panel',	
			title: '~~configure~~',			
			layout : 'card',		
			activeItem : '@{activeOption}',				
			dockedItems : [{
				xtype : 'toolbar',
				margin : '0 0 5 0',	
				height : 30,							
				items : [{
					xtype : 'component',
					html : '~~parameters~~',
					style : {
						fontWeight : 700,
						fontSize : '14px',
						color : '#666'
					}
				},'->',{
					xtype : 'fieldcontainer',
					hidden : '@{taskHasNoWizard}',
					defaultType: 'radio',
					fieldLabel : '~~showOption~~',
					width : 160,
					labelWidth : 70,
					labelStyle : 'font-weight:bold',					
					layout : 'hbox',							
					items : [{							
						boxLabel : '<span class="rs-social-button icon-table" style="color:#999999; width:20px"></span>',
						value : '@{showOptionIsTable}',												
						margin : '0 15 0 0',
						listeners : {
							afterrender: function(){
						   		Ext.QuickTips.register({
									target: this.getEl(),
									text: 'Table Mode',
									width: 100,
									dismissDelay: 2000
								})							 
							}
						}
					},{							
						boxLabel : '<i class="rs-social-button icon-list-ol" style="color:#999999"></i>',
						value : '@{!showOptionIsTable}',						
						listeners : {
							afterrender: function(){								 
							   	Ext.QuickTips.register({
									target: this.getEl(),
									text: 'Wizard Mode',
									width: 100,
									dismissDelay: 2000
								});
							}
						}
					}],
					
				}]
			}],
			defaults : {
				style : 'border-top: 1px solid #ccc',
				padding : '8 0 0 0',
				flex : 1
			},
			items : [{				
				xtype : 'container',
				layout : 'fit',
				defaults : {
					autoScroll : true,				
					flex : 1
				},
				items: [{
					xtype: '@{inputs}',						
					hidden: '@{!..inputTabIsPressed}'
				}, {
					xtype: '@{outputs}',						
					hidden: '@{..inputTabIsPressed}'
				}],
			},{						
				xtype : '@{parameterWizard}'
			}]	
		}, {
			cls: 'viewactiontask',
			xtype: 'panel',
			title: '@{viewactiontasktitle}',
			hidden: '@{!taskAuthorized}',
			layout : 'fit',
			items: [{
				xtype: '@{actiontaskbuilder}',
				padding : 0
			}],
			listeners: {
				activate: function() {
					this.fireEvent('viewactiontaskSelected');
				},
				viewactiontaskSelected: '@{viewactiontaskSelected}'
			}
		}]
	}],
	listeners: {
		executeTask: function(btn) {
			this.fireEvent('openExecutionWindow', this, btn);
		},
		openExecutionWindow: '@{openExecutionWindow}',
		beforedestroy : '@{beforeDestroyComponent}'
	}
});