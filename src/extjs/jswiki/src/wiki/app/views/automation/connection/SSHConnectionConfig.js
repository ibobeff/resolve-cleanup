glu.defView('RS.wiki.automation.SSHConnectionConfig',{		
	title: '~~sshConfigTitle~~',
	modal: true,
	width: 1000,
	height : 650,
	cls : 'rs-modal-popup',
	id : 'rs-connection-config',
	defaults : {	
		bodyPadding : 15
	},
	layout : 'card',
	activeItem : '@{activeStep}',
	dockedItems : [{
		xtype : 'container',
		width : 200,	
		style : 'background : #8c8a8a;',
		dock : 'left',	
		layout : {
			type :'vbox',
			align : 'stretch'
		},
		items : [/*{
			xtype : 'container',
			html : '<div class="connectionWizardHeader">Settings</div>'
		},*/{
			xtype : 'button',
			name : 'connectionWizardStep',
			cls : 'connectionWizardButton'
		},{
			xtype : 'button',
			name : 'taskWizardStep',		
			cls : 'connectionWizardButton'
		}]
	},{
		xtype : 'toolbar',
		dock : 'bottom',
		style : 'border-top: 1px solid #b5b8c8 !important;',
		padding : '8 15',
		items : ['->',{
			name : 'next',			
			//cls : 'rs-med-btn rs-btn-dark wizardNavButton',
			cls : 'rs-med-btn rs-btn-dark',		
		},{
			name : 'done',	
			//cls : 'rs-med-btn rs-btn-dark wizardNavButton',		
			cls : 'rs-med-btn rs-btn-dark', 
		},{
			name : 'cancel',
			//width : 80,
			cls : 'rs-med-btn rs-btn-light',
			//cls : 'wizardCancelButton', 
		}]
	}],
	items : [
	{
		items : [{
			itemId : 'mainConfig',
			xtype : 'fieldset',
			padding : '0 10 5 10',
			title : '~~mainConfigTitle~~',	
			defaultType : 'panel',
			defaults : {
				layout: {
					type : 'hbox',
					align : 'stretch'
				},
				labelWidth : 130,
				defaults : {
					flex : 1,
					hideLabel : true,
					labelWidth : 130,
					margin : '0 0 0 5'	
				},			
				margin : '8 0',	
				defaultType : 'combobox'
			},
			items : [
			{	
				items : [
				{
					fieldLabel : '~~host~~',
					displayField : 'name',
					valueField : 'value',
					name : 'hostSource',
					store : '@{sourceStore}',
					editable : false,
					hideLabel : false,			
					margin : 0,
					maxWidth : 250
				},{
					xtype: 'textfield',
					name: 'host',
					hidden: '@{!hostIsText}'
				},{				
					name: 'host',
					store: '@{paramStore}',
					queryMode: 'local',
					displayField : 'name',
					valueField : 'name',
					hidden: '@{!hostIsParam}'
				},{
					name: 'host',
					store: '@{propertyStore}',
					queryMode: 'local',					
					displayField: 'uname',
					valueField: 'uname',
					hidden: '@{!hostIsProperty}'
				}]
			},{		
				items : [
				{
					fieldLabel : '~~port~~',
					displayField : 'name',
					valueField : 'value',			
					name: 'portSource',
					store: '@{sourceStore}',
					editable: false,
					hideLabel : false,			
					margin : 0,
					maxWidth : 250
				},{
					xtype: 'textfield',
					name: 'port',
					hidden: '@{!portIsText}'
				},{
					name: 'port',
					store: '@{paramStore}',
					queryMode: 'local',
					displayField : 'name',
					valueField : 'name',
					hidden: '@{!portIsParam}'
				},{
					name: 'port',
					store: '@{propertyStore}',
					queryMode: 'local',				
					displayField: 'uname',
					valueField: 'uname',
					hidden: '@{!portIsProperty}'
				}]
			},{
				xtype : 'radio',
				fieldLabel : '~~authType~~',
				name : 'passType',
				boxLabel: '~~usernameAndPassword~~',
				value: '@{usernameAndPassword}'
			},{
				xtype : 'radio',
				name : 'passType',
				margin : '0 0 6 135',
				boxLabel: '~~keyFileNameAndPassphrase~~',
				value: '@{keyFileNameAndPassphrase}'
			},{			
				items : [
				{
					name: 'usernameSource',
					fieldLabel: '~~u_sername~~',
					store: '@{sourceStore}',
					displayField : 'name',
					valueField : 'value',
					editable : false,	
					margin : 0,
					maxWidth : 250,
					hideLabel : false
				},{
					xtype: 'textfield',
					name: 'username',
					hidden: '@{!usernameIsText}'
				},{
					name: 'username',
					store: '@{paramStore}',
					queryMode: 'local',
					displayField : 'name',
					valueField : 'name',
					hidden: '@{!usernameIsParam}'
				},{
					name: 'username',
					store: '@{propertyStore}',
					queryMode: 'local',
					displayField: 'uname',
					valueField: 'uname',
					hidden: '@{!usernameIsProperty}'
				}]
			},{
				hidden: '@{usernameAndPassword}',
				items : [
				{
					fieldLabel: '~~keyfile~~',
					name: 'keyfileSource',
					store: '@{sourceStore}',
					displayField : 'name',
					valueField : 'value',
					editable : false,	
					maxWidth : 250,
					margin : 0,
					hideLabel : false
				},{
					xtype: 'textfield',
					name: 'keyfile',
					hidden: '@{!keyfileIsText}'
				},{
					name: 'keyfile',
					store: '@{paramStore}',
					queryMode: 'local',
					displayField : 'name',
					valueField : 'name',
					hidden: '@{!keyfileIsParam}'
				}, {
					name: 'keyfile',
					store: '@{propertyStore}',
					queryMode: 'local',
					displayField: 'uname',
					valueField: 'uname',
					hidden: '@{!keyfileIsProperty}'
				}]
			},{			
				hidden: '@{usernameAndPassword}',
				items : [
				{		
					fieldLabel: '~~passphrase~~',		
					name: 'passphraseSource',
					store: '@{sourceStore}',
					displayField : 'name',
					valueField : 'value',
					editable : false,
					margin : 0,
					maxWidth : 250,
					hideLabel : false
				},{
					xtype: 'textfield',
					name: 'passphrase',
					hidden: '@{!passphraseIsText}'
				},{
					name: 'passphrase',
					store: '@{paramStore}',
					queryMode: 'local',
					displayField : 'name',
					valueField : 'name',
					hidden: '@{!passphraseIsParam}'
				}, {
					name: 'passphrase',
					store: '@{propertyStore}',
					queryMode: 'local',
					displayField: 'uname',
					valueField: 'uname',
					hidden: '@{!passphraseIsProperty}'
				}]
			},{
				hidden: '@{!usernameAndPassword}',
				items : [
				{
					name: 'passwordSource',
					fieldLabel: '~~p_assword~~',
					store: '@{sourceStore}',
					displayField : 'name',
					valueField : 'value',
					editable : false,			
					margin : 0,
					maxWidth : 250,
					hideLabel : false
				},{
					xtype: 'textfield',
					inputType: 'password',
					name: 'password',
					hidden: '@{!passwordIsConstant}',
					listeners:{
				        afterrender:function(cmp){
				            cmp.inputEl.set({
				            	//Prevent autofill from Chrome.
				                autocomplete:'new-password'
				            });
				        }
				    }
				},{
					xtype: 'textfield',				
					name: 'password',
					hidden: '@{!passwordIsFlowOrWs}'
				},{
					name: 'password',
					store: '@{paramStore}',
					queryMode: 'local',
					displayField : 'name',
					valueField : 'name',
					hidden: '@{!passwordIsParam}'
				},{
					name: 'password',
					store: '@{propertyStore}',
					queryMode: 'local',
					displayField: 'uname',
					valueField: 'uname',
					hidden: '@{!passwordIsProperty}'
				}]
			}]
		},{
			xtype : 'fieldset',
			padding : '0 10 5 10',
			itemId: 'optionConfig',
			title : '~~optionTitle~~',
			defaultType : 'textfield',		
			collapsible : true,
			layout : {
				type : 'vbox',
				align : 'stretch'
			},
			defaults : {
				layout: {
					type : 'hbox',
					align : 'stretch'
				},
				labelWidth : 130,
				defaults : {				
					flex : 2,
					hideLabel : true,
					labelWidth : 130,
					margin : '0 0 0 5'	
				},			
				margin : '4 0',	
				defaultType : 'combobox'
			},
			items : ['prompt', {
				name : 'timeout',
				xtype : 'numberfield'
			},{
				xtype : 'container',			
				items: [
				{	
					fieldLabel: '~~queueName~~',
					displayField: 'name',
					valueField: 'value',				
					editable: false,			
					store: '@{sourceStore}',
					name: 'queueNameSource',
					hideLabel : false,			
					margin : 0,
					maxWidth : 250
				},{
					xtype: 'textfield',
					name: 'queueName',
					hidden: '@{!queueNameIsText}'
				},{
					name: 'queueName',
					store: '@{paramStore}',
					queryMode: 'local',
					displayField : 'name',
					valueField : 'name',
					hidden: '@{!queueNameIsParam}'
				},{
					name: 'queueName',
					store: '@{propertyStore}',				
					displayField: 'uname',
					valueField: 'uname',
					queryMode: 'local',
					hidden: '@{!queueNameIsProperty}'
				}]
			},{
				xtype: 'textfield',
				name: 'sessionName'			
			}]
		}]
	},{
		xtype : '@{taskWizard}'		
	}]
})