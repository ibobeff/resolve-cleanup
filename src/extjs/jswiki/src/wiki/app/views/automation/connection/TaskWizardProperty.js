glu.defView('RS.wiki.automation.TaskWizardProperty',{		
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	dockedItems : [{
		xtype : 'toolbar',
		items : [{
			xtype : 'displayfield',
			labelStyle : 'font-weight:bold',
			name : 'taskFullName'	
		},'->',{
			name : 'openTaskPicker',
			cls : 'rs-small-btn rs-btn-dark'
		}]
	}],
	items : [{
		xtype : 'tabpanel',
		cls : 'rs-custom-tab',	
		flex : 1,	
		activeTab : '@{activePropertyTab}',
		items : [{			
			title : '~~parameter~~',
			tabConfig : {
				width : 170
			},
			layout : 'card',		
			bodyPadding : '5 0',			
			flex : 1,
			layout : {
				type : 'vbox',
				align : 'stretch'
			},
			dockedItems : [{
				xtype : 'toolbar',
				hidden : '@{taskHasNoWizard}',
				items : ['->',{
					xtype : 'fieldcontainer',				
					defaultType: 'radio',
					fieldLabel : '~~showOption~~',				
					labelWidth : 70,
					labelStyle : 'font-weight:bold',					
					layout : 'hbox',							
					items : [{							
						boxLabel : '<span class="rs-social-button icon-table" style="color:#999999; width:20px"></span>',
						value : '@{showOptionIsTable}',												
						margin : '0 15 0 0',
						listeners : {
							afterrender: function(){
						   		Ext.QuickTips.register({
									target: this.getEl(),
									text: 'Table Mode',
									width: 100,
									dismissDelay: 2000
								})							 
							}
						}
					},{							
						boxLabel : '<i class="rs-social-button icon-list-ol" style="color:#999999"></i>',
						value : '@{!showOptionIsTable}',						
						listeners : {
							afterrender: function(){								 
							   	Ext.QuickTips.register({
									target: this.getEl(),
									text: 'Wizard Mode',
									width: 100,
									dismissDelay: 2000
								});
							}
						}
					}]
				}]
			}],
			defaults : {
				flex : 1
			},
			items : [{
				hidden : '@{!showOptionIsTable}',
				xtype : 'container',
				layout : {
					type : 'vbox',
					align : 'stretch'
				},
				items : [{
					xtype : 'grid',				
					cls : 'rs-grid-dark parametersGrid',
					autoScroll : true,
					flex : 1,
					name : 'parameter',	
					sourceChangedInfoTxt: '~~sourceChanged~~',
					valueChangedInfoTxt: '~~valueChanged~~',
					valueProtectedInfoTxt: '~~valueProtected~~',
					userDefinedTxt: '~~userDefined~~',
					defaultParamsTxt: '~~defaultParams~~',
					additionalParamsTxt: '~~additionalParams~~',
					dockedItems: [{
						xtype : 'toolbar',				
						cls : 'rs-dockedtoolbar',
						items : [{
							name: 'addParam',
							cls : 'rs-btn-light rs-small-btn'					
						},{
							name: 'editParam',
							cls : 'rs-btn-light rs-small-btn'					
						},{				
							name: 'removeParam',
							cls : 'rs-btn-light rs-small-btn'					
						}]
					}],
					features: [{
						ftype: 'grouping',
						groupHeaderTpl: [
							'{name:this.IsDefault}', {
								IsDefault: function(isDefaultParam) {
									if (isDefaultParam) {
										return this.owner.view.up().defaultParamsTxt;
									} else {
										return this.owner.view.up().additionalParamsTxt;
									}
								}
							}
						],
						enableNoGroups: false,
						enableGroupingMenu : false,
					}],
					viewConfig: {
						markDirty: false,
						listeners : {
							expandbody : function(row,r,expandR, e){
								// on row expansion (clicking the + icon), switch class to display full text with word wrap as necessary
								row.innerHTML = row.innerHTML.replace(/resolve_overflow_ellipsis/g, 'resolve_overflow_break-word');
								if (expandR.innerText != '') {
									// if there is description, add a separator (by replacing the default "resolve_param_desc" class with "resolve_param_desc_border" which has a top border)
									row.innerHTML = row.innerHTML.replace(/resolve_param_desc/g, 'resolve_param_desc_border');
								}
							},
							collapsebody : function(row,r,expandR, e){
								// on row collapse, revert to default classes
								row.innerHTML = row.innerHTML.replace(/resolve_overflow_break-word/g, 'resolve_overflow_ellipsis');
								row.innerHTML = row.innerHTML.replace(/resolve_param_desc_border/g, 'resolve_param_desc');
							}
						}
					},
					plugins : [{
						ptype: 'rowexpander',
						expandOnDblClick: false,
						expandOnEnter: false,
						rowBodyTpl: new Ext.XTemplate('<div resolveId="resolveRowBody" style="color:#333;" class="resolve_param_desc">{description}</div>')
					}],
					store: '@{inputStore}',
					columns: [{
						header: 'Name',
						dataIndex: 'name',
						flex: 1,				
						renderer: function(value) {
							if (value) {
								return '<span class="resolve_overflow_ellipsis">' + Ext.htmlEncode(value) + '</span>';
							} else {
								return '';
							}
						}
					}, {
						header: 'Source',
						dataIndex: 'source',				
						renderer: function(value, meta, record) {
							if (value) {
								if (value == 'CONSTANT') {
									value = this.userDefinedTxt;
								}
								if (record.get('isDirty')) {
									meta.tdCls = 'modified-default-param';
									meta.tdAttr = 'data-qtip="' + this.sourceChangedInfoTxt + '"';
								}
								return '<span class="resolve_overflow_ellipsis">' + Ext.htmlEncode(value) + '</span>';
							} else {
								return '';
							}
						},
						width: 115					
					}, {
						header: 'Value',
						dataIndex: 'value',
						flex: 1,				
						renderer: function(value, meta, record) {
							if (record.get('encrypt'))
								return '*****'
							if (value) {
								if (record.get('protected')) {
									meta.tdCls = 'modified-default-param';
									meta.tdAttr = 'data-qtip="' + this.valueProtectedInfoTxt + '"';
								}
								else if (record.get('isDirty')) {
									meta.tdCls = 'modified-default-param';
									meta.tdAttr = 'data-qtip="' + this.valueChangedInfoTxt + '"';
								}
								return '<pre class="resolve_overflow_ellipsis">' + Ext.htmlEncode(value) + '</pre>';
							} else {
								return '';
							}
						}					
					}],
					listeners : {
						render : function(panel){
							panel._vm.on('selectNewRecord', function(newRecord){
								panel.getSelectionModel().select(newRecord);							
							})
						}
					}
				},{
					xtype : 'fieldset',
					cls : 'fieldset-bold-title',
					padding : '5 10 10',
					margin : '10 0 0 0',							
					title : '~~editParameter~~',
					style : 'visibility: @{editParamFormVisibility}',
					defaultType : 'textfield',
					defaults : {
						labelWidth : 130,
						margin : '4 0',
						cls : 'rs-displayfield-value'
					},
					layout : {
						type : 'vbox',
						align : 'stretch'
					},
					items : [{
						xtype : 'button',
						name : 'updateParam',
						cls : 'rs-small-btn rs-btn-dark',
						maxWidth : 80
					},{
						xtype : 'textfield',
						name : 'paramName',
						hidden : '@{paramHasDefault}'
					},{
						xtype : 'displayfield',
						name : 'paramName',
						height : 26,
						hidden : '@{!paramHasDefault}'
					},{
						xtype : 'displayfield',
						name : 'paramDescription',
						hidden : '@{!paramHasDefault}',
						height : 26
					},{
						xtype : 'radiogroup',								
						fieldLabel : '~~paramType~~',
						defaultType : 'radio',
						defaults : {
							margin : '0 15 0 0'
						},
						value : '@{paramType}',
						layout : 'hbox',
						items : [{
							boxLabel : '~~default~~',
							name : 'paramType',
							inputValue : 'default',
							hidden : '@{!paramHasDefault}',
						},{
							boxLabel : '~~source~~',
							name : 'paramType',
							inputValue : 'source',
							checked : true
						},{
							boxLabel : '~~custom~~',
							name : 'paramType',
							inputValue : 'custom'
						}],
						listeners : {
							afterrender : function(panel){
								this._vm.on('selectParamType', function(paramType){
									panel.setValue({
										paramType : paramType
									})
								})
							}
						}
					},{
						xtype : 'displayfield',
						name : 'paramDefaultValue',	
						fieldLabel : 'Value',	
						hidden : '@{!paramTypeIsDefault}',
						cls : 'rs-displayfield-value',
						height : 26
					},{
						xtype : 'fieldcontainer',
						fieldLabel : 'Value',
						hidden : '@{!paramTypeIsSource}',				
						layout : {
							type :'hbox',
							align : 'stretch'
						},
						defaults : {
							hideLabel : true,
							flex : 1
						},
						items : [{
							xtype : 'combobox',
							store : '@{variableTypeStore}',
							displayField: 'type',
							valueField: 'type',
							value : '@{variableType}',
							editable : false,
							margin : '0 5 0 0',
							maxWidth : 150
						},{
							xtype : 'combobox',
							displayField: 'name',
							valueField: 'name',
							store : '@{variableDataStore}',
							queryMode : 'local',
							value : '@{variableName}',
							hidden : '@{variableIsText}'
						},{
							xtype : 'textfield',
							name : 'variableName',
							hidden : '@{!variableIsText}'
						}]
					},{
						xtype : 'textfield',
						hidden : '@{!paramTypeIsCustom}',
						name : 'paramValue',
						height : 26		
					},{
						xtype : '@{variableForm}',
						style : 'visibility : @{..variableFormVisibility}',				
						margin : '4 0 0 135'
					}]
				}]
			},{
				hidden : '@{..showOptionIsTable}',
				xtype : '@{parameterWizard}'
			}]			
		},{
			title : 'Content Script',
			tabConfig : {
				width : 170
			},
			bodyPadding : '5 0',
			flex : 1,
			layout : {
				type : 'vbox',
				align : 'stretch'
			},
			dockedItems : [{
				xtype : 'fieldcontainer',
				margin : '0 0 10 0',
				fieldLabel : '~~scriptToExecuteTitle~~',
				labelWidth : 100,
				layout : 'hbox',
				defaultType : 'radio',
				hidden : '@{scriptChoiceOptionIsHidden}',
				items : [{
					boxLabel : '~~myScript~~',
					value : '@{usingMyScript}',
					margin : '0 15 0 0'
				},{
					boxLabel : '~~templateScript~~',
					value : '@{!usingMyScript}'
				}]
			}],
			items : [{
				xtype : 'toolbar',
				cls : 'rs-dockedtoolbar',
				items : [{
					xtype : 'displayfield',
					margin : 0,
					fieldLabel : '~~myScript~~'
				},'->',{
					xtype : 'button',
					name : 'revertMyCode',
					hidden : '@{revertMyCodeIsHidden}',
					cls : 'rs-small-btn rs-btn-dark'
				}]
			},{
				xtype : 'AceEditor',				
				parser : 'groovy',	
				flex : 1,			
				value : '@{myContentScript}'					
			},{
				xtype : 'displayfield',
				hidden : '@{templateScriptIsHidden}',
				margin : '20 0 0 0',
				labelWidth : 220,
				fieldLabel : '~~templateScript~~'
			},{
				xtype : 'AceEditor',
				parser : 'groovy',	
				hidden : '@{templateScriptIsHidden}',		
				flex : 1,			
				value : '@{autoGenCode}',
				editable : false			
			}]
		}]
	}],
	listeners : {
		boxready: function() {
			var win = Ext.getCmp('rs-connection-config');
			win.setWidth(Math.round(Ext.getBody().getWidth() * 0.65));
			win.setHeight(Math.round(Ext.getBody().getHeight() * 0.85));
			win.center();				
		},
	} 	
})
