glu.defView('RS.wiki.automation.ModelXml', {
	layout: {
		type : 'vbox',
		align : 'stretch'
	},
	cls :'rs-modal-popup',
	title: '@{title}',
	modal: true,
	padding : 15,
	items: [{
		flex : 1,
		xtype: 'AceEditor',
		name: 'xml',
		parser: 'xml'
	},{
		xtype : 'infoicon',
		margin : '8 0',
		iconType : 'warn',
		displayText : '~~editSourceWarning~~'
	}],
	listeners: {
		beforeshow: function() {
			this.setWidth(Math.round(Ext.getBody().getWidth() * 0.8));
			this.setHeight(Math.round(Ext.getBody().getHeight() * 0.8));
		},
		beforeclose: '@{beforeClose}',
		close: '@{afterClose}'
	},
	buttons: [
	/*
	{
		name :'save', 
		cls : 'rs-med-btn rs-btn-dark'
	},
	*/
	{
		name :'update',
		cls : 'rs-med-btn rs-btn-dark'
	}, {
		name :'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]	
})