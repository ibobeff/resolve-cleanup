glu.defView('RS.wiki.automation.HTTPTaskConfig',{
	xtype : 'container',
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {		
		margin : '4 0'
	},		
	items : [{
		labelWidth : '@{labelWidth}',
		xtype : 'textfield',
		name : 'sessionName',
		hidden : '@{taskIsPartOfCreation}'
	},{
		xtype : 'container',	
		layout: {
			type : 'hbox',
			align : 'stretch'
		},
		defaults : {				
			labelWidth : '@{labelWidth}',
			margin : '0 0 0 5',
			readOnly : '@{..viewOnly}'
		},
		items : [
		{
			xtype : 'combobox',
			maxWidth : '@{maxWidth}',
			fieldLabel : '~~url~~',
			displayField : 'value',
			valueField : 'value',
			name : 'method',
			store : '@{methodStore}',
			editable : false,			
			margin : 0,			
		},{
			xtype: 'textfield',
			name: 'commandUrl',
			hideLabel : true,
			flex : 1		
		},{
			xtype : 'button',		
			cls : 'rs-small-btn rs-btn-dark',		
			name : 'enableAddUrlVariable',
			text : '~~enableAddVariable~~',
			hidden : true
		}]
	},{
		xtype : '@{urlVariableForm}',
		margin : '4 0 4 120',
		hidden : '@{!..showUrlVariableForm}'
	},{
		xtype : 'tabpanel',
		cls : 'rs-custom-tab',		
		margin : '10 0 0 0',		
		flex : 1,		
		defaults: {			
			padding : '5 0 0 0'						
		},
		items : [
		{
			title : '~~headerTitle~~',
			defaults : {
				margin : '4 0'
			},
			overflowY : 'auto',
			items : '@{HeaderList}'
		},{
			title : '~~paramTitle~~',
			defaults : {
				margin : '4 0'
			},
			overflowY : 'auto',
			items : '@{ParamList}'
		},{
			title : '~~bodyTitle~~',	
			layout : 'card',
			activeItem : '@{activePayloadType}',
			disabled : '@{!methodIsPost}',				
			dockedItems: [{
				xtype : 'container',
				margin : '0 0 8 0',			
				layout: 'hbox',
				defaultType : 'radio',
				defaults : {
					readOnly : '@{..viewOnly}'
				},			
				items : [{						
					boxLabel : '~~formData~~',						
					value : '@{useFormData}',
					margin : '0 15 0 0'
				},{						
					boxLabel : 'raw',
					value : '@{!useFormData}',
				}]
			}],
			items : [{
				defaults : {
					margin : '4 0'
				},
				overflowY : 'auto',
				items : '@{FormDataList}'
			},{
				xtype: 'container',	
				layout : {
					type : 'vbox',
					align : 'stretch'
				},	
				items : [{
					hidden : '@{..viewOnly}',
					layout : {
						type : 'hbox',
						align : 'stretch'
					},
					items: [{
						xtype : 'combobox',
						margin : '0 5 0 0',
						displayField: 'name',
						valueField: 'value',
						queryMode: 'local',
						store : '@{syntaxStore}',
						name : 'syntax',
						editable : false,
						labelWidth : '@{labelWidth}',
						maxWidth : '@{maxWidth}'
					},{
						hidden: '@{!embedded}',
						items: [{
							xtype : 'button',
							name : 'enableAddVariable',
							cls : 'rs-small-btn rs-btn-dark',
							text : '~~enableAddVariable~~',
							maxWidth : 110
						}]
					}]
				},{
					hidden: '@{!embedded}',
					items: [{
						hidden : '@{!..showVariableForm}',
						margin: '4 0 0 0',
						items: [{
							xtype : '@{variableForm}',
						}]
					}]
				},{
					xtype : 'AceEditor',
					readOnly : '@{..viewOnly}',				
					parser : '@{syntax}',				
					flex : 1,					
					margin : '8 0',
					minHeight: '@{editorMinHeight}',
					value : '@{rawBody}',				
					listeners : {
						render : function(){
							var me = this;
							this._vm.on('insertAtCursor', function(newVal){
								me.insertAtCursor(newVal);
							})
						}
					}				
				},{
					hidden : '@{embedded}',
					isViewOnly : '@{..viewOnly}',
					items:[{
						xtype : 'button',
						name : 'enableAddVariable',
						hidden : '@{showVariableForm}',
						cls : 'rs-small-btn rs-btn-dark',
						text : '~~enableAddVariable~~',
						maxWidth : 110
					},{
						xtype : '@{variableForm}',				
						hidden : '@{!..showVariableForm}'
					}],
					listeners : {
						render : function(panel){
							if(this.isViewOnly)
								panel.hide();
						}
					}
				}]
			}],
			listeners :{
				disable : function(){
					var tabPanel = this.up();
					if(tabPanel.getActiveTab() == this)
						tabPanel.setActiveTab(0);
				}
			}
		}]
	}]
})