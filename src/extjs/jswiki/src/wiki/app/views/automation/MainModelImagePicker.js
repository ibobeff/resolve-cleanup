glu.defView('RS.wiki.automation.MainModelImagePicker', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: '@{elements}',
	height: 500,
	overflowY: 'auto'
});