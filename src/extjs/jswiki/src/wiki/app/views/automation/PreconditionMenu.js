glu.defView('RS.wiki.automation.PreconditionMenu', {
	xtype: 'menu',
	initX: '@{initX}',
	initY: '@{initY}',
	items: [
		'editProperties'
	],
	listeners: {
		show: function() {
			this.setX(this.initX);
			this.setY(this.initY);
		},
		hide: function() {
			this.doClose();
		}
	}
})