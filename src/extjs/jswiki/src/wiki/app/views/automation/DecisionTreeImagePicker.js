glu.defView('RS.wiki.automation.DecisionTreeImagePicker', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: '@{elements}',
	height: 500,
	overflowY: 'auto'
})