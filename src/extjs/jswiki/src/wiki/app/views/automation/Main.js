glu.defView('RS.wiki.automation.Main', {
	layout: 'card',
	activeItem: '@{activeItem}',
	items: '@{cards}',
	listeners: {
		render: function() {
			this.getEl().dom.setAttribute('tabindex', 0);

			this.getEl().on('keyup', function(evt) {
				evt.stopEvent();
				if (evt.ctrlKey && evt.getCharCode() == evt.S)
					this.fireEvent('saveAutomation');

			}, this);
		},
		saveAutomation: '@{saveAutomation}',
		afterrender: '@{afterRender}'
	}
});