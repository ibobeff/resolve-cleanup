glu.defView('RS.wiki.automation.EndMenu', {
	xtype: 'menu',
	initX: '@{initX}',
	initY: '@{initY}',
	items: [{
			text: '~~dependency~~',
			menu: {
				xtype: 'menu',
				items: [{
					text: '~~merge~~',
					menu: {
						parentPanel: '@{parentPanel}',
						xtype: 'menu',
						mergeValue: '@{mergeValue}',
						defaultType: 'menucheckitem',
						defaults: {
							group: 'merge',
							handler: function() {
								this.parentPanel.fireEvent('taskdependencychanged', this.name);
							}
						},
						listeners: {
							beforerender: function() {
								for(var i = 0; i < this.items.length; i++) {
									var item = this.items[i];
									item.parentPanel = this.parentPanel;
									item.checked = item.name === this.mergeValue;
								}
							}
						},
						items: ['all', 'any', 'targets']
					}
				}]
			}
		},
		'editInputs', {
			xtype: 'menuseparator'
		},
		'editProperties'
	],
	listeners: {
		show: function() {
			this.setX(this.initX);
			this.setY(this.initY);
		},
		hide: function() {
			this.doClose();
		}
	}
});