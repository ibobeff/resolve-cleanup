glu.defView('RS.wiki.automation.PreconditionProperty', {
	//title: '~~precondition~~',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'textfield',
		name: 'label',
		allowBlank: false,
		readOnly: '@{..viewOnly}'
	}, {
		xtype: 'combo',
		displayField: 'name',
		valueField: 'value',
		name: 'merge',
		readOnly: '@{..viewOnly}',
		editable: false,
		store: '@{mergeStore}',
		queryMode: 'local'
	}, {
		xtype: 'textarea',
		name: 'expression',
		readOnly: '@{..viewOnly}'
	}]
});