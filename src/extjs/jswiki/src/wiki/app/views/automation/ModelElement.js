glu.defView('RS.wiki.automation.ModelElement', {
	layout: {
		type: 'hbox'
	},
	bodyPadding: '5px',
	style: 'cursor:pointer',
	plugins: [{
		ptype: 'automationmodeldragzone'
	}],
	elementType: '@{elementType}',
	elementDisplayType: '@{elementDisplayType}',
	elementImg: '@{elementImg}',
	elementWidth: '@{elementWidth}',
	elementHeight: '@{elementHeight}',
	items: [{
		html: '<img src="@{elementImg}" width=16px height=16px/>'
	}, {
		flex: 1,
		padding: '0 0 0 5',
		html: '<span style="color:#666 !important">@{elementDisplayType}</span>'
	}]
});

glu.defView('RS.wiki.automation.ConnectionElement', {
	layout: {
		type: 'hbox'
	},
	bodyPadding: '5px',
	style: 'cursor:pointer',
	plugins: [{
		ptype: 'automationconnectiondragzone'
	}],
	elementType: '@{elementType}',
	elementDisplayType: '@{elementDisplayType}',
	elementImg: '@{elementImg}',
	elementWidth: '@{elementWidth}',
	elementHeight: '@{elementHeight}',
	connectionType : '@{connectionType}',
	items: [{
		html: '<img src="@{elementImg}" width=16px height=16px/>'
	}, {
		flex: 1,
		padding: '0 0 0 5',
		html: '<span style="color:#666 !important">@{elementDisplayType}</span>'
	}]
});