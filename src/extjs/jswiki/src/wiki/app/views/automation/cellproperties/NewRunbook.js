glu.defView('RS.wiki.automation.NewRunbook', {
	disabled: '@{..viewOnly}',
	cls: 'runbook-panel',
	items: [{
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			// wizard step 1: select an existing runbook or create new
			layout: 'hbox',
			items: [{
				padding: '10 5 10 10',
				xtype: 'text',
				text: '1.',
			}, {
				xtype: 'button',
				name: 'selectRunbook',
				scale: 'medium',
				pressed: '@{isSelectRunbook}'
			}, {
				padding: '10',
				xtype: 'text',
				text: '~~or~~',
			}, {
				xtype: 'button',
				name: 'createRunbook',
				scale: 'medium',
				pressed: '@{isCreateRunbook}'
			}]
		}, {
			// select existing runbook - wizard step 2
			xtype: 'fieldcontainer',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			hidden: '@{selectRunbookHidden}',
			padding: '10 10 0 10',
			items: [{
				xtype: 'combo',
				flex: 1,
				name: 'runbook',
				fieldLabel: '~~runbookName~~',
				labelWidth : 130,
				store: '@{runbookStore}',
				trigger2Cls: 'x-form-search-trigger',
				enableKeyEvents: true,
				displayField: 'ufullname',
				valueField: 'ufullname',
				queryMode: 'remote',
				typeAhead: false,
				minChars: 1,
				queryDelay: 500,
				pageSize: 20,
				//This will fix the issue where value in control in glu is out of sync with what send to server.
				ignoreSetValueOnLoad : true,
				onTrigger2Click: function() {
					this.fireEvent('searchRunbook');
				},
				listeners: {
					searchRunbook: '@{searchRunbook}',
					change : '@{handleRunbookChange}',
					select: '@{handleRunbookSelect}',
				}
			}, 
			/* TODO - support Version Control
			{
				xtype: 'radiogroup',
				layout: 'hbox',
				defaultType: 'radio',
				defaults: {
					disabledCls: 'use-version-disabled',
				},	
				fieldLabel: '~~useVersion~~',
				hidden: '@{!showUseVersionOption}',
				padding: '0 0 0 18',
				items: [{
					padding: '0 0 0 8',
					name: 'useVersion',
					boxLabel: '~~latest~~',
					value: '@{latest}',
					disabled: '@{latestIsDisabled}'
				}, {
					padding: '0 0 0 20',
					name: 'useVersion',
					boxLabel: '~~latestStable~~',
					value: '@{latestStable}',
					disabled: '@{latestStableIsDisabled}',
				}, {
					padding: '0 0 0 20',
					name: 'useVersion',
					boxLabel: '~~specificVersion~~',
					value: '@{specificVersion}',
					disabled: '@{specificVersionIsDisabled}',
				}]
			}, {
				layout: 'hbox',
				hidden: '@{!showVersionNumberOption}',
				cls: 'version-number',
				items: [{
					xtype: 'combo',
					name: 'versionNumber',
					padding: '0 0 0 18',
					flex: 2,
					width: 20,
					labelWidth : 112,
					store: '@{versionHistoryStore}',
					displayField: 'version',
					valueField: 'version',
					editable: false,
					queryMode: 'local'
				}, {
					xtype: 'toolbar',
					cls: 'version-toolbar',
					flex: 3,
					items: [{
						xtype: 'button',
						name: 'details',
						text: '',
						iconCls: 'icon-large icon-list-ul rs-icon',
						cls: 'rs-med-btn rs-btn-light version-details-btn',
					}, {
						xtype: 'label',
						text: '~~details~~'
					}]
				}]
			}, 
			*/
			{
				xtype: 'panel',
				margin: '8 0 0 135',
				items: [{
					xtype: 'button',
					name: 'done',
					//cls: 'rs-med-btn rs-btn-dark',
					margin: '0 15 0 0',
					disabled: '@{!isDoneValid}',
					scale: 'medium'
				}, {
					xtype: 'button',
					name: 'cancel',
					//cls: 'rs-med-btn rs-btn-light',
					scale: 'medium'
				}]
			}]
		}, {
			// create runbook - wizard step 2
			xtype: 'form',
			hidden: '@{createRunbookHidden}',
			padding: '10 10 0 10',
			defaults : {
				labelWidth : 120,
				margin : '4 0',
			},
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				// create runbook - wizard step 2: prompt to select / enter namespace & name
				xtype: 'text',
				text: '~~enterRunbookName~~'
			}, {
				// create runbook - wizard step 2: enter new ruknbook namespace and name
				margin: '4 0 0 18',
				defaults : {
					labelWidth : 164,
				},
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [{
					xtype: 'textfield',
					name: 'namespace',
					allowBlank: false
				}, {
					xtype: 'textfield',
					name: 'name',
					allowBlank: false
				}]
			}, {
				xtype: 'panel',
				margin: '8 0 0 187',
				items: [{
					xtype: 'button',
					name: 'create',
					//cls: 'rs-med-btn rs-btn-dark',
					margin: '0 15 0 0',
					disabled: '@{!isCreateValid}',
					scale: 'medium'
				},{
					xtype: 'button',
					name: 'cancel',
					//cls: 'rs-med-btn rs-btn-light',
					scale: 'medium'
				}]
			}]
		}]
	}]
});