glu.defView('RS.wiki.automation.DocumentMenu', {
	xtype: 'menu',
	initX: '@{initX}',
	initY: '@{initY}',
	items: ['searchDecisionTree', 'editProperties'],
	listeners: {
		show: function() {
			this.setX(this.initX);
			this.setY(this.initY);
		},
		hide: function() {
			this.doClose();
		}
	}
})