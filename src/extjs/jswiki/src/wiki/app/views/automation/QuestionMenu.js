glu.defView('RS.wiki.automation.QuestionMenu', {
	xtype: 'menu',
	initX: '@{initX}',
	initY: '@{initY}',
	items: [{
		text: '~~selectType~~',
		menu: {
			parentPanel: '@{parentPanel}',
			xtype: 'menu',
			selectType: '@{selectType}',
			defaultType: 'menucheckitem',
			defaults: {
				group: 'selectType',
				handler: function() {
					this.parentPanel.fireEvent('dtselecttypechanged', this.name);
				}
			},
			listeners: {
				beforerender: function() {
					//hack for extjs 4.2.1 bug..up() returns null when clicking this menuitem
					this.items.each(function(item) {
						item.parentPanel = this.parentPanel;
						if (item.name == this.selectType)
							item.checked = true;
						else
							item.checked = false;
					}, this)
				}
			},
			items: ['radio', 'dropdown']
		}
	}, 'editProperties'],
	listeners: {
		show: function() {
			this.setX(this.initX);
			this.setY(this.initY);
		},
		hide: function() {
			this.doClose();
		}
	}
});