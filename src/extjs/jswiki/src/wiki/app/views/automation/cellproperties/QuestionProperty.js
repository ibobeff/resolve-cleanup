glu.defView('RS.wiki.automation.QuestionProperty', {
	// '~~question~~',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults : {
		labelWidth : 120,
		margin : '4 0'
	},
	items: [{
		xtype : 'container',
		layout : {
			type : 'hbox',
			align : 'stretch'
		},
		items : [{
			flex : 1,
			labelWidth : 120,
			xtype: 'textfield',
			name: 'label',
			readOnly: '@{..viewOnly}'
		},{
			xtype : 'button',
			margin : '0 0 0 5',			
			cls : 'rs-small-btn rs-btn-dark',		
			name : 'addVarForQuestion',
			text : '~~enableAddVariable~~',
		}]
	},{
		xtype : '@{varFormForQuestion}',
		margin : '4 0 4 125',
		hidden : '@{!..showVarFormForQuestion}',
	},{
		xtype : 'container',
		layout : {
			type : 'hbox',
			align : 'stretch'
		},
		items : [{
			flex : 1,
			labelWidth : 120,
			xtype : 'textfield',
			name : 'recommendationText',
			readOnly: '@{..viewOnly}'
		},{
			xtype : 'button',
			margin : '0 0 0 5',			
			cls : 'rs-small-btn rs-btn-dark',		
			name : 'addVarForRecommendation',
			text : '~~enableAddVariable~~',
		}]
	},{
		xtype : '@{varFormForRecommendation}',
		margin : '4 0 4 125',
		hidden : '@{!..showVarFormForRecommendation}',
	},{
		xtype : 'component',
		margin : '0 0 0 125',
		html : '~~recommendationInfo~~'
	}],
	listeners: {
		beforedestroy : '@{beforeDestroyComponent}'
	}
});