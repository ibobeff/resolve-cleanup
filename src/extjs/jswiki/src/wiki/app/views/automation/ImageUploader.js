glu.defView('RS.wiki.automation.ImageUploader', {
	asWindow: {
		title: '~~uploadImage~~',
		cls : 'rs-modal-popup',
		height: 400,
		width: 800,
		modal: true,
		padding : 15,
		closable: true
	},

	images: '@{images}',

	xtype: 'form',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults : {
		labelWidth : 120,
		margin : '4 0'
	},
	items: [{
		xtype: 'textfield',
		fieldLabel: '~~imageLabel~~',
		name: 'imageLabel',
		//value: '',
		allowBlank: false,
		maxLength: 12,
		validator: function(val) {
			var form = this.up('form'),
				images = form.images,
				imageLabel = '',
				image = null,
				vm = form._vm;
			if (!val.trim()) {
				return vm.localize('emptyLabelError');
			}
			for (var i=0; i<images.length; i++) {
				image = images.getAt(i);
				imageLabel = image.customImg? image.elementType: vm.localize(image.elementType);
				if (imageLabel.toLocaleLowerCase() == val.toLocaleLowerCase()) {
					return vm.localize('duplicateLabelError');
				}
			}
			return true;
		}
	},{
		xtype: 'fieldcontainer',
		fieldLabel: '~~imageSize~~',
		layout: {
			type: 'hbox',
			align : 'stretch'
		},
		items: [{
			name: 'fileInfo',
			xtype: 'textfield',
			fieldLabel: '',
			flex: 6,
			//itemId: 'fileInfo',
			emptyText: '~~selectAFile~~',
			readOnly: true,
			padding: '0 3 0 0'
		}, {
			xtype: 'button',
			width: 75,
			text: '~~browse~~',
			//itemId: 'browseBtn',
			cls : 'rs-btn-dark',
			id: 'upload_button',
			preventDefault: false
		}]
	}, {
		hidden: true,
		xtype: 'numberfield',
		name: 'fileSize',
		anchor: '100%',
		//itemId: 'fileSize',
		maxValue: 40,
		minValue: 0,
		allowBlank: false
	}, {
		itemId: 'fineUploaderTemplate',
		html: '@{fineUploaderTemplate}',
		height: '@{fineUploaderTemplateHeight}',
		hidden: '@{fineUploaderTemplateHidden}',
	}, {
		id: 'attachments_grid',
		hidden: true
	}],
	buttons: [{
		name: 'submit',
		text: '~~upload~~',
		//itemId: 'uploadBtn',
		formBind: true,
		cls : 'rs-med-btn rs-btn-dark'
	}, {
		text: '~~discard~~',
		//itemId:  'cancelBnt',
		handler: '@{close}',
		cls : 'rs-med-btn rs-btn-light'
	}],

	listeners: {
		afterrender: function() {
			this.fireEvent('afterRendered', this, this);
		},
		afterRendered: '@{viewRenderCompleted}'
	}
})

/*
glu.defView('RS.wiki.automation.ImageUploader', {
	asWindow: {
		title: '~~uploadImage~~',
		cls : 'rs-modal-popup',
		height: 250,
		width: 600,
		modal: true,
		padding : 15,
		closable: false
	},

	images: '@{images}',

	xtype: 'form',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults : {
		labelWidth : 120,
		margin : '4 0'
	},
	items: [{
		xtype: 'textfield',
		fieldLabel: '~~imageLabel~~',
		name: 'imageLabel',
		value: '',
		allowBlank: false,
		maxLength: 12,
		validator: function(val) {
			var form = this.up('form'),
				images = form.images,
				imageLabel = '',
				image = null,
				vm = form._vm;
			if (!val.trim()) {
				return vm.localize('emptyLabelError');
			}
			for (var i=0; i<images.length; i++) {
				image = images.getAt(i);
				imageLabel = image.customImg? image.elementType: vm.localize(image.elementType);
				if (imageLabel.toLocaleLowerCase() == val.toLocaleLowerCase()) {
					return vm.localize('duplicateLabelError');
				}
			}
			return true;
		}
	}, {
		xtype: 'fieldcontainer',
		fieldLabel: '~~imageSize~~',
		layout: {
			type: 'hbox',
			align : 'stretch'
		},
		items: [{
			xtype: 'textfield',
			flex: 6,
			itemId: 'fileInfo',
			emptyText: '~~selectAFile~~',
			readOnly: true,
			padding: '0 3 0 0'
		}, {
			xtype: 'button',
			width: 75,
			text: '~~browse~~',
			itemId: 'browseBtn',
			cls : 'rs-btn-dark'
		}]
	}, {
		xtype: 'fieldcontainer',
		fieldLabel: '~~progress~~',
		layout: {
			type: 'hbox'
		},
		items: {
			xtype: 'progressbar',
			flex: 1,
			itemId: 'progressbar'
		},
		flex: 2
	}, {
		hidden: true,
		xtype: 'numberfield',
		anchor: '100%',
		itemId: 'fileSize',
		maxValue: 40,
		minValue: 0,
		allowBlank: false
	},{
		xtype: 'displayfield',
		fieldLabel: '&nbsp;',
		labelSeparator: '',
		itemId: 'errMsg',
		fieldStyle: 'color:red;'
	}],
	buttons: [{
		text: '~~upload~~',
		itemId: 'uploadBtn',
		formBind: true,
		cls : 'rs-med-btn rs-btn-dark'
	}, {
		text: '~~discard~~',
		itemId:  'cancelBnt',
		handler: '@{discard}',
		cls : 'rs-med-btn rs-btn-light'
	}],

	listeners: {
		afterrender: function(form) {
			form.fireEvent('formRendered', this, form);
		},
		formRendered: '@{afterRender}'
	}
})
*/