glu.defView('RS.wiki.automation.ModelImage', {
	layout: {
		type: 'hbox'
	},
	bodyPadding: '5px',
	style: 'cursor:pointer;',
	plugins: [{
		ptype: 'automationmodeldragzone'
	}],
	elementType: '@{elementType}',
	elementDisplayType: '@{elementDisplayType}',
	elementImg: '@{elementImg}',
	elementWidth: '@{elementWidth}',
	elementHeight: '@{elementHeight}',
	customImg: '@{customImg}',
	hidden: '@{hidden}',
	items: [{
		html: '<img src="@{elementImg}" width=16px height=16px/>'
	}, {
		xtype: 'displayfield',
		itemId: 'imageLabel',
		flex: 1,
		padding: '0 0 0 5',
		height: 20,
		fieldCls: 'rs-ad-image-displayfield',
		value: '@{elementDisplayType}',
	}],
	listeners: {
		afterrender: function(p) {
			var centerRegion = p.up('#westRegionId').up().down('#centerRegionId');
			p.fireEvent('imagerendered', this, p, centerRegion);
			p.getEl().on('click', function(p) {
				this.fireEvent('imageclick', this, this);
			}, p);
		},
		imagerendered: '@{imageRendered}',
		imageclick: '@{imageClick}'
	}
});