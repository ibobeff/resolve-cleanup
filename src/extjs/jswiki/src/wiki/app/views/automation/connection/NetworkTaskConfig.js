glu.defView('RS.wiki.automation.NetworkTaskConfig',{
	xtype : 'container',
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		labelWidth : 120,
		margin : '4 0',
		readOnly : '@{..viewOnly}'
	},
	items : [{
		xtype : 'textfield',
		name : 'sessionName',
		hidden : '@{sessionPromptIsHidden}',

	},{
		xtype : 'textfield',
		name : 'prompt',
		hidden : '@{sessionPromptIsHidden}'
	},{
		xtype : 'container',
		hidden : '@{!taskIsPartOfCreation}',
		layout : {
			type : 'hbox',
			align : 'stretch'
		},
		items : [{		
			xtype : 'textfield',
			flex : 1,
			labelWidth : 120,			
			name : 'command',			
			readOnly : '@{..viewOnly}'
		},{
			xtype : 'button',
			margin : '0 0 0 5',
			cls : 'rs-small-btn rs-btn-dark',		
			name : 'enableAddVariable',
			hidden : '@{showVariableForm}'
		}]
	},{
		xtype : '@{variableForm}',
		hidden : '@{!..showVariableForm}',	
		margin : '4 0 0 125'
	},{
		xtype : 'displayfield',
		name : 'command',
		hidden : '@{taskIsPartOfCreation}'
	}]
})