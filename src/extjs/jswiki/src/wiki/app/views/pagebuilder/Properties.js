glu.defView('RS.wiki.pagebuilder.Properties', {
	autoScroll: true,
	defaults : {
		margin : '4 0'
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'displayfield',
		name: 'ufullname',
		hidden: '@{fullNameIsEditable}'
	}, {
		xtype: 'textfield',
		name: 'fullName',
		hidden: '@{!fullNameIsEditable}'
	}, {
		xtype: 'textfield',
		name: 'utitle'
	}, {
		xtype: 'textarea',
		name: 'usummary'
	}, {
		title: '~~displayOptions~~',
		layout: {
			type: 'hbox',
			align: 'bottom'			
		},
		bodyPadding: '10px',
		items: [{
			xtype: 'radio',
			hideLabel: true,
			boxLabel: '~~wiki~~',
			name: 'displayMode',
			value: '@{displayModeWiki}',
			margin: '0 20 0 0'
		}, {
			xtype: 'radio',
			hideLabel: true,
			boxLabel: '~~decisionTree~~',
			name: 'displayMode',
			value: '@{displayModeDecisionTree}',
			margin: '0 20 0 0'
		}, {
			xtype: 'radio',
			hideLabel: true,
			boxLabel: '~~catalog~~',
			name: 'displayMode',
			margin: '0 10 0 0',
			value: '@{displayModeCatalog}'
		}, {
			xtype: 'combobox',
			editable: true,
			forceSelection: true,
			name: 'ucatalogId',
			store: '@{ucatalogStore}',
			pageSize: 20,
			displayField: 'name',
			valueField: 'id',
			hideLabel: true,
			queryMode: 'local',	
			width : 500		
		}]
	}, {	
		layout: {
			type: 'hbox',
			align: 'stretch'
		},		
		items: [{
			title: '~~roles~~',
			xtype : 'panel',
			cls : 'rs-grid-dark',
			flex: 1,
			items : [
			{
				xtype : 'toolbar',
				cls : 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-btn-light rs-small-btn'
				},
				items: ['addRole', 'removeRole', {
					xtype: 'checkbox',
					hideLabel: true,
					boxLabel: '~~useDefaultRights~~',
					itemId: 'defaultRoleCheckbox',
					name: 'uisDefaultRole',
					listeners: {
						change: function(field, newValue) {
							field.up().up().down('grid').getView()[newValue ? 'disable' : 'enable']()
						}
					}
				}],
			},{
				xtype: 'grid',
				name: 'roles',			
				columns: '@{rolesColumns}',
				plugins: [{
					ptype: 'cellediting',
					clicksToEdit: 1
				}],
				flex: 1,
				accessRights: '@{accessRights}',
				setAccessRights: function(value) {
					this.accessRights = value
				},
				viewConfig: {
					markDirty: false
				},
				listeners: {
					afterrender: function(grid) {
						grid.store.on('datachanged', function() {
							if (!hasPermission(grid.accessRights.uadminAccess)) {
								grid.columns[grid.columns.length - 1].hide()
							}
						})
					},
					afterlayout: function(grid) {
						grid.getView()[grid.up().down('#defaultRoleCheckbox').getValue() ? 'disable' : 'enable']();
					}
				}
			}]
		}, {
			title: '~~tags~~',
			flex: 1,		
			xtype: 'grid',
			cls : 'rs-grid-dark',
			name: 'tags',
			margin : '0 10',
			columns: [{
				dataIndex: 'name',
				text: '~~name~~',
				flex: 1
			}],
			dockedItems: [
			{
				xtype : 'toolbar',
				dock : 'top',
				cls : 'rs-dockedtoolbar',
				items : [{
					text :'~~addTag~~' ,
					handler : '@{addTag}',
					cls : 'rs-btn-light rs-small-btn'
				},{
					text : '~~removeTag~~',
					handler : '@{removeTag}',
					cls : 'rs-btn-light rs-small-btn'
				}]
			}]			
		}, {
			title: '~~catalogs~~',
			xtype: 'grid',
			cls : 'rs-grid-dark',
			overflowY : 'auto',
			flex: 1,		
			name: 'catalogs',
			columns:  [{
				dataIndex: 'name',
				text: '~~name~~',
				flex: 1
			}],
			dockedItems: [
			{
				xtype : 'toolbar',
				dock : 'top',
				cls : 'rs-dockedtoolbar',
				items : [{
					text :'~~addCatalog~~' ,
					handler : '@{addCatalog}',
					cls : 'rs-btn-light rs-small-btn'
				},{
					text : '~~removeCatalog~~',
					handler : '@{removeCatalog}',
					cls : 'rs-btn-light rs-small-btn'
				}]
			}]			
		}]
	}],
	buttons: [{
		text : '~~update~~',
		handler : '@{update}',
		cls : 'rs-btn-dark rs-med-btn'
	},{
	 	text : '~~cancel~~',
		handler : '@{cancel}',
		cls : 'rs-btn-light rs-med-btn'
	}],
	asWindow: {
		title: '@{title}',
		cls : 'rs-modal-popup',
		modal: true,
		padding : 15,	
		listeners: {
			scope: this,
			beforeshow: function(win) {
				win.setWidth(Math.round(Ext.getBody().getWidth() * 0.8));
				win.setHeight(Math.round(Ext.getBody().getHeight() * 0.8));
			},
			beforeclose: '@{beforeClose}'
		}
	}
});
