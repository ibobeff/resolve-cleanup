
glu.defView('RS.wiki.pagebuilder.ResolveMarkup', {
	header: false,
	layout: 'fit',
	items: [{
		xtype: 'AceEditor',
		name: 'content',
		parser: 'text'
	}]
})

glu.defView('RS.wiki.pagebuilder.TextEditor', {
	header: false,
	layout: 'fit',
	cls: 'wysiwyg',
	items: [{
		xtype: 'wikibuildereditor',
		name: 'content',
		hideLabel: true,
		plugins: [
			Ext.create('Ext.ux.form.HtmlEditor.Word'),
			Ext.create('Ext.ux.form.HtmlEditor.IndentOutdent')
		]
	}]
})

glu.defView('RS.wiki.pagebuilder.Groovy', {
	header: false,
	layout: 'fit',
	items: [{
		xtype: 'AceEditor',
		name: 'content',
		parser: 'groovy'
	}]
})

glu.defView('RS.wiki.pagebuilder.HTML', {
	header: false,
	layout: 'fit',
	items: [{
		xtype: 'AceEditor',
		name: 'content',
		parser: 'html'
	}]
})


glu.defView('RS.wiki.pagebuilder.Javascript', {
	header: false,
	layout: 'fit',
	items: [{
		xtype: 'AceEditor',
		name: 'content',
		parser: 'javascript'
	}]
})


glu.defView('RS.wiki.pagebuilder.CodeSample', {
	header: false,
	layout: {
		type: 'vbox'
	},
	defaults: {
		width: 550,
		labelWidth: 135
	},
	items: [{
		xtype: 'combobox',
		name: 'type',
		editable: false,
		store: '@{typeStore}',
		displayField: 'name',
		valueField: 'value',
		queryMode: 'local',
		forceSelection: true
	}, {
		xtype: 'checkbox',
		name: 'showLineNumbers',
		listeners: {
			change: function() {
				var editor = this.up().down('AceEditor').editor;
				if (this.value) {
					editor.renderer.setShowGutter(true);
				} else {
					editor.renderer.setShowGutter(false);
				}
			}
		}
	}, {
		xtype: 'AceEditor',
		name: 'content',
		flex: 1,
		width: '100%',
		parser: '@{type}',
		showGutter: true,
		listeners: {
			render: function() {
				var editor = this.down('#' + this.editorId);
				this.getEl().on('click', function() {
					editor.focus();
				});
			},
			boxready: function() {
				if (this._vm.showLineNumbers) {
					setTimeout(function () {
						this.setShowGutter(true);
					}.bind(this), 10);
				} else {
					setTimeout(function () {
						this.setShowGutter(false);
					}.bind(this), 10);
				}
			}
		}
	}]
})

glu.defView('RS.wiki.pagebuilder.Image', {
	header: false,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		labelWidth : 120,
		margin : '4 0'
	},
	items: [{
		xtype: 'imagepickerfield',
		editable: false,
		name: 'src',
		listeners: {
			attach: '@{attach}'
		}
	},{
		xtype: 'numberfield',
		name: 'width',
		fieldLabel: '~~scale~~',
	},/*{
		xtype: 'textfield',
		name: 'height',
		emptyText : '100%'
	},*/ {
		xtype: 'radiogroup',
		fieldLabel: '~~mode~~',
		columns: 2,
		items: [{
			xtype: 'radiofield',
			boxLabel: '~~pixels~~',
			name: 'displayMode',
			inputValue: 'pixels',
			margin: '0 20 0 0'
		}, {
			xtype: 'radiofield',
			boxLabel: '~~percentage~~',
			name: 'displayMode',
			inputValue: 'percentage',
			margin: '0 20 0 0'
		}],
		listeners: {
			afterrender: function(radiogroup) {
				this.findParentByType('panel')._vm.assignDisplayMode(radiogroup)
			},
			change: function(radiogroup, newVal) {
				this.findParentByType('panel')._vm.displayModeChange(newVal.displayMode)
			}
		}
	}, {
		xtype: 'checkbox',
		name: 'zoom',
		hideLabel : true,
		margin : '0 0 0 125',
		boxLabel : '~~zoom~~',
		hidden: '@?{..hideImgComZoom}'
	}]
})

glu.defView('RS.wiki.pagebuilder.Secure', {
	header: false,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults : {
		margin : '4 0',
		labelWidth : 130
	},
	items: [{
		xtype: 'triggerfield',
		editable: false,
		name: 'key',
		triggerCls: 'x-form-search-trigger',
		onTriggerClick: function() {
			this.fireEvent('choose', this)
		},
		listeners: {
			choose: '@{chooseKey}'
		}
	}, {
		xtype: 'triggerfield',
		editable: false,
		name: 'value',
		triggerCls: 'x-form-search-trigger',
		onTriggerClick: function() {
			this.fireEvent('choose', this)
		},
		listeners: {
			choose: '@{chooseValue}'
		}
	}]
})

glu.defView('RS.wiki.pagebuilder.Infobar', {
	header: false,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		margin : '4 0 0 0',
		labelWidth : 130
	},
	items: [{
		xtype: 'numberfield',
		name: 'height',
		minValue: 0
	}, {
		xtype: 'checkbox',
		margin : '4 0 4 135',
		hideLabel : true,
		boxLabel : '~~social~~',
		name: 'social'
	}, {
		xtype: 'checkbox',
		margin : '4 0 4 135',
		hideLabel : true,
		boxLabel : '~~feedback~~',
		name: 'feedback'
	}, {
		xtype: 'checkbox',
		margin : '4 0 4 135',
		hideLabel : true,
		boxLabel : '~~rating~~',
		name: 'rating'
	}, {
		xtype: 'checkbox',
		margin : '4 0 4 135',
		hideLabel : true,
		boxLabel : '~~attachments~~',
		name: 'attachments'
	}, {
		xtype: 'checkbox',
		margin : '4 0 4 135',
		hideLabel : true,
		boxLabel : '~~history~~',
		name: 'history'
	}, {
		xtype: 'checkbox',
		margin : '4 0 4 135',
		hideLabel : true,
		boxLabel : '~~tags~~',
		name: 'tags'
	}]
})

glu.defView('RS.wiki.pagebuilder.Model', {
	header: false,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		margin : '4 0',
		labelWidth : 130
	},
	items: [{
		name: 'wiki',
		xtype: 'triggerfield',
		triggerCls: 'x-form-search-trigger',
		onTriggerClick: function() {
			this.fireEvent('choose', this)
		},
		listeners: {
			choose: '@{chooseDocument}'
		}
	}, {
		xtype: 'combobox',
		name: 'status',
		store: '@{statusStore}',
		displayField: 'name',
		valueField: 'value',
		queryMode: 'local',
		editable: false
	}, {
		name: 'refreshInterval',
		xtype: 'numberfield',
		minValue: 5
	}, {
		xtype: 'numberfield',
		name: 'refreshCountMax',
		minValue: 1,
	}, {
		name: 'zoom',
		xtype: 'numberfield',
		minValue: 0
	}, {
		name: 'height',
		xtype: 'numberfield',
		minValue: 0
	}, {
		name: 'width',
		xtype: 'numberfield',
		minValue: 0
	}]
})

glu.defView('RS.wiki.pagebuilder.Form', {
	header: false,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		margin : '4 0',
		labelWidth : 130
	},
	items: [{
		xtype: 'formpickerfield',
		name: 'name',
		allowCreate: true,
		listeners: {
			choose: '@{chooseForm}',
			edit: '@{editForm}',
			create: '@{createForm}'
		}
	}, {
		xtype: 'textfield',
		name: 'title'
	}, {
		xtype: 'textfield',
		name: 'tooltip'
	}, {
		xtype: 'checkbox',
		margin : '4 0 4 135',
		boxLabel : '~~collapsed~~',
		hideLabel : true,
		name: 'collapsed'
	}, {
		xtype: 'checkbox',
		margin : '4 0 4 135',
		boxLabel : '~~popout~~',
		hideLabel : true,
		name: 'popout'
	}, {
		xtype: 'numberfield',
		name: 'popoutHeight',
		minValue: -1
	}, {
		xtype: 'numberfield',
		name: 'popoutWidth',
		minValue: -1
	}]
})

glu.defView('RS.wiki.pagebuilder.Page', {
	header: false,
	layout: {
		type :'hbox'
	},
	items: [{
		xtype: 'triggerfield',
		flex : 1,
		labelWidth: 150,
		name: 'wiki',
		editable: false,
		triggerCls: 'x-form-search-trigger',
		onTriggerClick: function() {
			this.fireEvent('choose', this)
		},
		listeners: {
			choose: '@{chooseWikiDocument}'
		}
	}, {
		xtype: 'image',
		cls: 'rs-btn-edit',
		listeners: {
			handler: '@{jumpToWiki}',
			render: function(image) {
				image.getEl().on('click', function() {
					image.fireEvent('handler')
				})
			}
		}
	}]
})

glu.defView('RS.wiki.pagebuilder.Result', {
	header: false,
	minHeight: 500,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaults: {
			labelWidth: 130
		},
		items: [{
			xtype: 'textfield',
			name: 'title'
		}, {
			xtype: 'combobox',
			name: 'filterValue',
			store: '@{filterStore}',
			displayField: 'name',
			valueField: 'value',
			queryMode: 'local',
			multiSelect: true
		}, {
			xtype: 'combobox',
			name: 'order',
			store: '@{orderStore}',
			displayField: 'name',
			valueField: 'value',
			queryMode: 'local',
			editable : false
		}, {
			xtype: 'numberfield',
			name: 'refreshInterval',
			minValue: 5,
		}, {
			xtype: 'numberfield',
			name: 'refreshCountMax',
			minValue: 1,
		}, {
			xtype: 'numberfield',
			name: 'descriptionWidth'
		}, {
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'checkboxgroup',
				columns: 1,
				margin: '4 0 4 130',
				defaults : {
					hideLabel : true
				},
				items: [{
					xtype: 'checkbox',
					name: 'autoCollapse',
					boxLabel: '~~autoCollapse~~'
				}, {
					xtype: 'checkbox',
					name: 'autoHide',
					boxLabel: '~~autoHide~~'
				}, {
					xtype: 'checkbox',
					name: 'encodeSummary',
					boxLabel: '~~encodeSummary~~'
				}, {
					xtype: 'checkbox',
					name: 'progress',
					boxLabel: '~~progress~~'
				}, {
					xtype: 'checkbox',
					name: 'selectAll',
					boxLabel: '~~showAllActionTasks~~'
				}, {
					xtype: 'checkbox',
					disabled: '@{!selectAll}',
					margin: '0 0 0 20',
					name: 'includeStartEnd',
					boxLabel: '~~includeStartEndTasks~~'
				}]
			}, {
				xtype: 'radiogroup',
				fieldLabel: '~~showResultsFrom~~',
				labelWidth: 135,
				columns: 1,
				defaults : {
					hideLabel : true
				},
				items: [{
					xtype: 'radio',
					name: 'showWiki',
					inputValue: 'any',
					boxLabel: '~~showAnyWiki~~',
					checked: '@{showAnyWiki}',
					listeners: {
						change: function(comp, newValue, oldValue){
							if (newValue) {
								this.fireEvent('showAnyWikiClicked');
							}
						},
						showAnyWikiClicked: '@{showAnyWikiClicked}',
					}
				}, {
					xtype: 'radio',
					name: 'showWiki',
					inputValue: 'current',
					boxLabel: '~~showCurrentWikiOnly~~',
					checked: '@{showCurrentWikiOnly}',
					listeners: {
						change: function(comp, newValue, oldValue){
							if (newValue) {
								this.fireEvent('showCurrentWikiOnlyClicked');
							}
						},
						showCurrentWikiOnlyClicked: '@{showCurrentWikiOnlyClicked}',
					}
				}, {
					xtype: 'fieldcontainer',
					layout: {
						type: 'hbox',
						//align: 'stretch'
					},
					items: [{
						xtype: 'radio',
						name: 'showWiki',
						inputValue: 'custom',
						boxLabel: '~~showCustomWikiOnly~~',
						checked: '@{showCustomWikiOnly}',
						listeners: {
							change: function(comp, newValue, oldValue){
								if (newValue) {
									this.fireEvent('showCustomWikiOnlyClicked');
								}
							},
							showCustomWikiOnlyClicked: '@{showCustomWikiOnlyClicked}',
						}
					}, {
						flex: 1,
						xtype: 'combo',
						name: 'showWikiName',
						disabled: '@{!showCustomWikiOnly}',
						currentComboValue: '@{showWikiName}',
						margin: '0 0 0 5',
						hideLabel: true,
						store: '@{runbookStore}',
						trigger2Cls: 'x-form-search-trigger',
						enableKeyEvents: true,
						displayField: 'ufullname',
						valueField: 'ufullname',
						queryMode: 'remote',
						typeAhead: false,
						minChars: 1,
						queryDelay: 500,
						pageSize: 20,
						//This will fix the issue where value in control in glu is out of sync with what send to server.
						ignoreSetValueOnLoad : true,
						onTrigger2Click: function() {
							this.fireEvent('searchRunbook');
						},
						listeners: {
							afterrender : function(combobox){
								//This is needed since empty store will always set value field to empty.
								combobox.setRawValue(this.currentComboValue);
							},
							searchRunbook: '@{searchRunbook}',
							select: '@{runbookSelected}',
						}
					}]
				}]
			}]
		}]
	}, {
		disabled: '@{selectAll}',
		xtype: 'treepanel',
		cls : 'actionTaskFilters rs-grid-dark',
		title: '~~actionTaskFilters~~',
		name: 'actionTasks',
		columns: '@{actionTasksColumns}',
		rootVisible: false,
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items :['addGroup', 'addActionTask','edit', 'remove',
			{
				xtype: 'checkbox',
				name: 'preserveTaskOrder',
				boxLabel: '~~preserveTaskOrder~~',
				hideLabel: true
			}]
		}],
		flex: 1,
		viewConfig: {
			plugins: {
				ptype: 'treeviewdragdrop',
				containerScroll: true
			}
		},
		plugins: [{
			ptype: 'cellediting',
			pluginId: 'celledit',
			clicksToEdit: 1,
			listeners: {
				beforeEdit: function(editor, e) {
					if (!e.record.isLeaf() && (e.column.dataIndex != 'description' && e.column.dataIndex != 'autoCollapse')) {
						return false
					}
				},
				edit: function(editor, e) {
					e.record.commit()
				}
			}
		}],
		listeners: {
			selectionchange: '@{selectionchange}',
			itemdblclick: '@{itemdblclick}'
		},
		margin : {
			bottom : 8
		}
	}]
})

glu.defView('RS.wiki.pagebuilder.Progress', {
	header: false,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		margin : '4 0',
		labelWidth : 130
	},
	items: [{
		xtype: 'imagepickerfield',
		emptyText: '~~defaultLoadingImage~~',
		editable: false,
		name: 'text',
		listeners: {
			attach: '@{attach}'
		}
	}, {
		xtype: 'textfield',
		name: 'displayText'
	}, {
		xtype: 'textfield',
		name: 'completedText'
	}, {
		xtype: 'numberfield',
		name: 'height'
	}, {
		xtype: 'numberfield',
		name: 'width'
	}, {
		xtype: 'numberfield',
		name: 'border'
	}, {
		xtype: 'fieldcontainer',
		fieldLabel: '~~borderColor~~',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'toolbar',
			items: [{
				xtype: 'button',
				text: '@{bgStyle}',
				htmlEncode: false,
				menu: {
					xtype: 'colormenu',
					handler: '@{selectBgColor}'
				}
			}]
		}]
	}]
})

glu.defView('RS.wiki.pagebuilder.Detail', {
	header: false,
	minHeight: 400,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'fieldcontainer',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaults: {
			labelWidth: 130,
			margin : '4 0'
		},
		items: [{
			xtype: 'numberfield',
			name: 'refreshInterval',
			minValue: 5
		}, {
			xtype: 'numberfield',
			name: 'refreshCountMax',
			minValue: 1
		}, {
			xtype: 'textfield',
			name: 'maximum'
		}, {
			xtype: 'textfield',
			name: 'offset'
		}, {
			xtype: 'checkboxgroup',
			margin: '4 0 4 130',
			items: [{
				xtype: 'checkbox',
				hideLabel: true,
				boxLabel: '~~progress~~',
				name: 'progress'
			}]
		}]
	},{
		xtype: 'treepanel',
		title: '~~actionTasks~~',
		cls : 'rs-grid-dark',
		store: '@{actionTasks}',
		columns: '@{actionTasksColumns}',
		rootVisible: false,
		flex : 1,
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items :['addActionTask', 'remove']
		}],
		useArrows: true,
		viewConfig: {
			plugins: {
				ptype: 'treeviewdragdrop',
				containerScroll: true
			}
		},
		plugins: [{
			ptype: 'cellediting',
			pluginId: 'celledit',
			clicksToEdit: 1,
			listeners: {
				beforeEdit: function(editor, e) {
					if (!e.record.isLeaf() && (e.column.dataIndex != 'description' && e.column.dataIndex != 'autoCollapse')) {
						return false
					}
				},
				edit: function(editor, e) {
					e.record.commit()
				}
			}
		}],
		listeners: {
			selectionchange: '@{selectionchange}'
		}
	}]
})

glu.defView('RS.wiki.pagebuilder.Automation', {
	header: false,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		margin : '4 0',
		labelWidth : 130
	},
	items: [{
		xtype: 'trigger',
		trigger1Cls: 'x-form-search-trigger',
		trigger2Cls: 'rs-btn-edit',
		editable: false,
		name: 'name',
		onTrigger1Click: function() {
			this.fireEvent('chooseRunbook');
		},
		onTrigger2Click: function() {
			this.fireEvent('editAutomation');
		},
		listeners: {
			chooseRunbook: '@{chooseRunbook}',
			editAutomation: '@{editAutomation}'
		}
	}, {
		xtype: 'textfield',
		name: 'title'
	}, {
		xtype: 'textfield',
		name: 'tooltip'
	}, {
		xtype: 'checkbox',
		margin : '4 0 4 135',
		hideLabel : true,
		boxLabel : '~~collapsed~~',
		name: 'collapsed'
	}, {
		xtype: 'checkbox',
		margin : '4 0 4 135',
		hideLabel : true,
		boxLabel : '~~popout~~',
		name: 'popout'
	}, {
		xtype: 'numberfield',
		name: 'popoutHeight',
		minValue: -1
	}, {
		xtype: 'numberfield',
		name: 'popoutWidth',
		minValue: -1
	}]
})

glu.defView('RS.wiki.pagebuilder.ActionTask', {
	header: false,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		margin : '4 0',
		labelWidth : 130
	},
	items: [{
		xtype: 'formpickerfield',
		name: 'displayName',
		allowCreate: true,
		listeners: {
			choose: '@{chooseForm}',
			edit: '@{editForm}',
			create: '@{createForm}'
		}
	}, {
		xtype: 'textfield',
		name: 'title'
	}, {
		xtype: 'textfield',
		name: 'tooltip'
	}, {
		xtype: 'checkbox',
		margin : '4 0 4 135',
		name: 'collapsed',
		hideLabel : true,
		boxLabel : '~~collapsed~~'
	}, {
		xtype: 'checkbox',
		margin : '4 0 4 135',
		name: 'popout',
		hideLabel : true,
		boxLabel : '~~popout~~'
	}, {
		xtype: 'numberfield',
		name: 'popoutHeight',
		minValue: -1
	}, {
		xtype: 'numberfield',
		name: 'popoutWidth',
		minValue: -1
	}]
})

glu.defView('RS.wiki.pagebuilder.Table', {
	header: false,
	xtype: 'grid',
	cls : 'rs-grid-dark',
	name: 'table',
	store: '@{table}',
	setStore: function(value) {
		this.reconfigure(value, null)
	},
	columns: '@{tableColumns}',
	setColumns: function(value) {
		this.reconfigure(this.store, value)
	},
	plugins: [{
		ptype: 'cellediting',
		pluginId: 'celledit',
		clicksToEdit: 1,
		listeners: {
			edit: function(editor, e) {
				e.record.commit()
			}
		}
	}],
	listeners : {
		columnmove : '@{columnMove}'
	},
	dockedItems : [{
		xtype : 'toolbar',
		cls : 'rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items : ['addColumn', 'removeColumn', 'addRow', 'insertRow', 'removeRow']
	}]
})


glu.defView('RS.wiki.pagebuilder.XTable', {
	header: false,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults : {
		margin : '4 0',
		labelWidth : 130
	},
	items: [{
		xtype: 'textfield',
		name: 'sql'
	}, {
		xtype: 'textfield',
		name: 'drilldown'
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		flex: 1,
		name: 'table',
		store: '@{table}',
		setStore: function(value) {
			this.reconfigure(value, null)
		},
		columns: '@{tableColumns}',
		setColumns: function(value) {
			this.reconfigure(this.store, value)
		},
		plugins: [{
			ptype: 'cellediting',
			pluginId: 'celledit',
			clicksToEdit: 1,
			listeners: {
				edit: function(editor, e) {
					e.record.commit()
				}
			}
		}],
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items : ['addColumn', 'editColumn', 'removeColumn'], // 'addRow', 'insertRow', 'removeRow'],
		}],
		listeners: {
			columnmove: '@{columnMove}'
		}
	}]
})

glu.defView('RS.wiki.pagebuilder.CollaborationLink', {
	header: false,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		margin : '4 0',
		labelWidth : 130
	},
	items: [{
		xtype: 'textfield',
		name: 'displayText'
	},{
		xtype: 'textfield',
		name: 'preceedingText'
	},{
		xtype: 'checkbox',
		name: 'openInNewTab',
		hideLabel : true,
		boxLabel : '~~openInNewTab~~',
		margin : '4 0 4 135'
	},{
		xtype: 'hidden',
		name: 'toHidden',
		setValue: function(value) {
			if (this.rendered) {
				var form = this.up('panel');
				if (form) form.down('#toField').setValue(value)
			} else {
				this.initialVal = value
			}
		},
		listeners: {
			render: function(field) {
				if (field.initialVal) {
					var form = field.up('panel')
					if (form) form.down('#toField').setValue(this.initialVal)
				}
			}
		}
	},{
		xtype: 'comboboxselect',
		displayField: 'name',
		valueField: 'id',
		itemId: 'toField',
		name: 'to',
		queryMode: 'local',
		editable: false,
		multiSelect: true,
		tpl: '<tpl for="."><div class="x-boundlist-item">{name} {[values["type"] ? "(" + values["type"] + ")" : ""]}</div></tpl>',
		triggerOnClick: false,
		triggerCls: '',
		onTriggerClick: function() {
			this.fireEvent('showAdvancedTo', this)
		},
		listeners: {
			showAdvancedTo: '@{showAdvancedTo}'
		}
	}, ]
})
