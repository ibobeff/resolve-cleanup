glu.defView('RS.wiki.pagebuilder.ComponentPicker', {
	title: '~~chooseComponent~~',
	modal: true,
	padding: '0 15 15 15',
	cls : 'rs-modal-popup',
	autoScroll: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		margin: '15 0 0 0'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		hidden: '@{toolbarHidden}',
		style: 'padding-top: 15px;',
		items: [
			{
				xtype: 'fieldcontainer',
				fieldLabel: 'Sort By',
				flex : 1,
				labelStyle : "font-weight : 600;",
				defaultType: 'radiofield',
				labelWidth: 60,
				layout: 'hbox',
				defaults: {
					listeners: {
						change: function(btn, newValue, oldValue, eOpts) {
							btn.fireEvent('sortChange', btn, btn);
						},
						sortChange: '@{sortChange}'
					}
				},
				items: [{
					boxLabel: 'Category',
					name: 'sort',
					inputValue: 'type',
					padding: '0 20',
					checked: true
				}, {
					boxLabel: 'Alphabet Order',
					name: 'sort',
					inputValue: 'name'
				}]
			},{
				xtype: 'textfield',
				name: 'filter',
				flex : 1,
				labelStyle : "font-weight : 600;",
				emptyText: '~~filterEmpty~~',
				listeners: {
					afterrender: function(field) {
						Ext.defer(function() {
							field.focus()
						}, 100)
					}
				}
			}],
	}],
	items: '@{items}',
	buttons: [{
        name : 'addComponent',
        cls : 'rs-med-btn rs-btn-dark',
    },{
        name : 'cancel',
        cls : 'rs-med-btn rs-btn-light'
    }],
	listeners: {
		beforerender: function(v, eOpts) {
			v.fireEvent('setwindowsize', this, v, eOpts);
		},
		setwindowsize: '@{setWindowSize}'
	}
})
