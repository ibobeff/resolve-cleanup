glu.defView('RS.wiki.pagebuilder.ComponentsViewer', {
        title: '@{title}',
        layout: 'fit',
        hidden: '@{hidden}',
        items: [{
                xtype: 'dataview',
                deferInitialRefresh: false,
                store: '@{components}',
                tpl: '@{..tpl}',
                itemSelector: '@{..itemSelector}',
                overItemCls: 'componentPicker-section-hover',
                selectedItemCls: 'componentPicker-section-selected',
                autoScroll: true,
                listeners: {
                        scope: this,
                        selectionChange: function(sm, selected, eOpts) {
                                sm.view.fireEvent('componentSelected', sm.view, selected, sm);
                        },
                        componentSelected: '@{componentSelected}',
                        itemdblclick: function( dataView, record, item, index, e, eOpts ) {
                                dataView.fireEvent('addComponent', this, record);
                        },
                        addComponent: '@{addComponent}'
                }
        }]
});


