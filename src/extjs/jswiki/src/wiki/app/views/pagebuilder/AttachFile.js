glu.defView('RS.wiki.pagebuilder.AttachFile', {
	isEditMode: '@{isEditMode}',
	asWindow: {
		title: '@{title}',
		cls : 'rs-modal-popup',
		height: 500,
		width: 900,
		modal: true,
		padding : 15,
		closable: true
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	dockedItems: [{
		xtype: 'toolbar',
		margin: '4 0',
		cls: 'rs-dockedtoolbar',
		defaults : {
			cls: 'rs-small-btn rs-btn-light'
		},
		items: [{
			xtype: 'button',
			text: '~~upload~~',
			preventDefault: false,
			hidden: '@{!allowUpload}',
			id: 'upload_button'
		}, {
			name: 'delete',
			hidden: '@{!allowRemove}',
		}, '->', {
			name: 'reload',
			tooltip: '~~reload~~',
			text: '',
			iconCls: 'x-tbar-loading',
		}]
	}],

	items: [{
		itemId: 'fineUploaderTemplate',
		html: '@{fineUploaderTemplate}',
		height: '@{fineUploaderTemplateHeight}',
		hidden: '@{fineUploaderTemplateHidden}',
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		id: 'attachments_grid',
		store: '@{filesStore}',
		columns: '@{filesColumns}',
		multiSelect: '@{multiSelect}',
		margin: '6 0 0 0',
		flex: 1,
		viewConfig: {
			markDirty: false
		},
		listeners: {
			selectionChange: '@{selectionChanged}'
		}
	}],
	buttons: [{
		text: '~~close~~',
		itemId:  'cancelBnt',
		handler: '@{close}',
		cls : 'rs-med-btn rs-btn-light'
	}]		
});