glu.defView('RS.wiki.pagebuilder.ComponentEntry', {
	cls: 'ComponentEntryId',
	minWidth: 300,
	margin: 10,
	padding :8,
	type: '@{type}',
	iconShown: '@{iconShown}',
	// plugins: [{
	// 	ptype: 'rbentitydragzone'
	// }],
	style: {
		borderWidth: '1px',
		borderStyle: 'dashed',
		borderColor: 'silver'
	},
	layout: {
		type: 'vbox',
		pack: 'center',
		align: 'center'
	},
	tbar: [{
		xtype: 'tbtext',
		text: '@{..name}',
		style: 'font-size:13px;font-weight:bold'
	},
		'->',
		/* // TBD
		 {
		 iconCls: 'rs-social-button icon-gear',
		 name: 'componentProperties',
		 text: 'Properties',
		 tooltip: 'Component Properties'
		 },
		 */
		{
			iconCls: 'rs-social-button icon-pencil',
			name: 'editComponent',
			text: '',
			tooltip: '~~edit~~'
		},
		{
			iconCls: 'rs-social-button icon-chevron-left',
			name: 'moveLeft',
			text: '',
			tooltip: '~~moveLeft~~'
		},
		{
			iconCls: 'rs-social-button icon-chevron-right',
			name: 'moveRight',
			text: '',
			tooltip: '~~moveRight~~'
		},
		{
			iconCls: 'rs-social-button icon-remove-sign',
			name: 'removeComponent',
			text: '',
			tooltip: '~~deleteComponent~~'
		}],
	items: [{
		//itemId: 'ComponentEntryIcon',
		xtype: 'image',
		src: '@{..imageSrc}',
		width: '@{..imageSize}',
		height: '@{..imageSize}',
		style: '@{..imageCursor}',
		hidden: '@{!..iconShown}',
		listeners: {
			render: function() {
				this.up().fireEvent('iconRendered', this, this);
			}
		}
	}, {
		//itemId: 'ComponentEntryLabel',
		xtype: 'label',
		margin: '5 0 0 0',
		hidden: '@{!..iconShown}',
		text: '@{..iconLabel}'
	}, {
		//itemId: 'ComponentEntryImage',
		cls: 'ComponentEntryContent',
		xtype: 'panel',
		width: '@{..detailedComponentWidth}',
		height: '@{..detailedComponentHeight}',
		html: '@{..detailedComponentHtml}',
		style: '@{..detailedComponentStyle}',
		hidden: '@{..iconShown}',
		autoScroll: true,
		listeners: {
			afterlayout: function() {
				if (!this._vm.beQuiet) {
					this.up().fireEvent('updateDetailedComponent', this, this.up().getWidth(), this.up().body.getHeight(), true);
				}
			}
		}
	}],

	setSelected: function(selected) {
		this.selected = selected;
		if (!this.selected)
			this.getEl().removeCls('entrySelection');
		else
			this.getEl().addCls('entrySelection');
	},
	watch: true,
	listeners: {
		launchComponentPicker: '@{launchComponentPicker}',
		iconRendered: '@{iconRendered}',

		resize: function() {
			if (this.type) {
				var type = this.type.toLowerCase();
				if (type == 'image' || type == 'wysiwyg') {
					this.fireEvent('updateDetailedComponent', this, this.getWidth(), this.body.getHeight(), true);
				}
			}
		},
		boxready: function() {
			if (this.type) {
				var type = this.type.toLowerCase();
				if (type == 'image' || type == 'wysiwyg') {
					this.fireEvent('updateDetailedComponent', this, this.getWidth(), this.body.getHeight(), true);
				}
			}
		},
		afterrender: function(v) {
			v.fireEvent('componententryrender', this, v);
		},
		updateDetailedComponent: '@{updateDetailedComponent}',
		componententryrender: '@{componentEntryRender}'
	},
	moveLeftIsEnabled: '@{moveLeftIsEnabled}',
	moveRightIsEnabled: '@{moveRightIsEnabled}'
});