glu.defView('RS.wiki.pagebuilder.SectionEntry', {	
	minHeight: 300,
	margin : '15 0',
	padding : 8,
	overflowX: 'auto',
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	style: {
		borderWidth: '1px',
		borderColor: 'silver',
		borderStyle: 'solid'
	},
	tbar: [
	{
		xtype: 'tbtext',
		text: '@{sectionTitle}',
		style: 'font-size:14px;font-weight:bold'		
	},{
		iconCls: 'rs-social-button icon-gear',
		name: 'sectionProperties',
		text: '',
		tooltip: '~~properties~~'
	},'->',{
		iconCls: 'rs-icon-button icon-plus-sign-alt',
		name: 'addNewComponent',
		text: 'Component',
		cls : 'rs-small-btn rs-btn-light'
		//tooltip: 'Add New Component'
	},{
		xtype: 'tbseparator',
		hidden: '@{!addNewComponentIsVisible}',
	},{
		iconCls: 'rs-social-button icon-chevron-up',
		name: 'moveUp',
		text: '',
		tooltip: '~~moveSectionUp~~'
	},{
		iconCls: 'rs-social-button icon-chevron-down',
		name: 'moveDown',
		text: '',
		tooltip: '~~moveSectionDown~~'
	},{
		iconCls: 'rs-social-button icon-remove-sign',
		name: 'removeSection',
		text: '',
		tooltip: '~~deleteSection~~'
	}],	
	defaults: {
		flex: 1
	},
	items: '@{componentEntries}',
	// plugins: [{
	// 	ptype: 'rbentitydroptarget'
	// }],
	selector: '.sectionEntry',
	//selected: '@{selected}',
	setSelected: function(selected) {
		this.selected = selected;
		// if (!this.selected)
		// 	this.getEl().removeCls('entrySelection');
		// else
		// 	this.getEl().addCls('entrySelection');
	},
	collapsed: false,
	listeners: {
		render: function() {
			this.getEl().addCls('sectionEntry');
			this.getEl().dom.abCmp = this;
			// this.getEl().on('click', function(e) {
			// 	this.fireEvent('entryClicked', this, e)
			// }, this);
		},
		// accept: '@{accept}'
	},
	moveUpIsEnabled: '@{moveUpIsEnabled}',
	moveDownIsEnabled: '@{moveDownIsEnabled}'
});