glu.defView('RS.wiki.pagebuilder.MoveOrRename', {	
	title: '@{moveRenameCopyTitle}',
	width: 600,
	height : 250,
	modal: true,
	cls : 'rs-modal-popup',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	padding: 15,
	defaultType: 'textfield',
	defaults: {
		labelWidth: 130
	},
	items: [{
		xtype: 'combo',
		fieldLabel: '~~namespace~~',
		store: '@{store}',
		displayField: 'unamespace',
		valueField: 'unamespace',
		name: 'namespace'

	}, 'filename', {		
		xtype: 'fieldcontainer',		
		defaultType: 'radiofield',
		defaults: {
			hideLabel: true,
			margin: '0 0 0 135'
		},
		items: [{
			name: 'replace',
			value: '@{overwrite}',
			boxLabel: '~~overwrite~~',
			tooltip: '~~overwrite_tooltip~~',
			listeners: {
				render: function(radio) {
					var tooltip = this._vm.overwrite_tooltip;
					if (radio.tooltip)
						Ext.create('Ext.tip.ToolTip', {
							target: radio.getEl(),
							html: tooltip
						})
				}
			}
		}, {
			name: 'replace',
			value: '@{skip}',
			boxLabel: '~~skip~~',
			tooltip: '~~skip_tooltip~~',
			listeners: {
				render: function(radio) {
					var tooltip = this._vm.skip_tooltip;
					if (radio.tooltip)
						Ext.create('Ext.tip.ToolTip', {
							target: radio.getEl(),
							html: tooltip
						})
				}
			}
		}, {
			name: 'replace',
			value: '@{update}',
			hidden: '@{!updateIsVisible}',
			boxLabel: '~~update~~',
			tooltip: '~~update_tooltip~~',
			listeners: {
				render: function(radio) {
					if (radio.tooltip)
						Ext.create('Ext.tip.ToolTip', {
							target: radio.getEl(),
							html: radio.tooltip
						})
				}
			}
		}]
	}],
	buttons: [{
        text : '~~confirmAction~~',
        cls : 'rs-med-btn rs-btn-dark',
        handler : '@{confirmAction}',
    },{
        text : '~~close~~',
        cls : 'rs-med-btn rs-btn-light',
        handler : '@{close}',
    }]
});