glu.defView('RS.wiki.pagebuilder.ComponentEdit', {
	padding : 15,
	cls : 'rs-modal-popup',
	title: '@{title}',
	modal: true,
	minWidth: 700,
	minHeight: 350,
	resizable: true,
	layout: 'fit',
	items: '@{typeConfigurations}',
	buttons : [{
		xtype : 'button',
		text : '~~update~~',
		cls : 'rs-med-btn rs-btn-dark',
		disabled : '@{updateIsDisabled}',
		handler : '@{update}'
	},{
		xtype : 'button',
		text : '~~cancel~~',
		cls : 'rs-med-btn rs-btn-light',
		handler : '@{cancel}'
	}],
	listeners: {
		beforeshow: function() {
			this.fireEvent('expandDialog', this, this, Ext.getBody().getWidth(), Ext.getBody().getHeight());
		},
		expandDialog: '@{expandDialog}',
		close : '@{beforeClose}'
	}
});
