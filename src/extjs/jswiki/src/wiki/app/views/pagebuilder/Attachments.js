glu.defView('RS.wiki.pagebuilder.Attachments', {
	isEditMode: '@{isEditMode}',
	asWindow: {
		title: '~~attachments~~',
		cls : 'rs-modal-popup',
		height: 500,
		width: 900,
		modal: true,
		padding : 15,
		closable: true
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	dockedItems: [{
		xtype: 'toolbar',
		margin: '4 0',
		cls: 'rs-dockedtoolbar',
		defaults : {
			cls: 'rs-small-btn rs-btn-light'
		},
		items: [{
			xtype: 'button',
			text: '~~upload~~',
			preventDefault: false,
			hidden: '@{!allowUpload}',
			id: 'upload_button'
		}, {
			name: 'delete',
			hidden: '@{!allowRemove}',
		}, {
			name: 'renameAttachment',
			cls: 'rs-btn-light rs-small-btn',
			hidden: '@{!isEditMode}',
			tooltip: '~~renameAttachmentTooltip~~',
		}, {
			name: 'referGlobalAttachments',
			cls: 'rs-btn-light rs-small-btn',
			hidden: '@{!isEditMode}',
			tooltip: '~~referGlobalAttachmentsTooltip~~',
		}, '->', {
			name: 'reload',
			tooltip: '~~reload~~',
			text: '',
			iconCls: 'x-tbar-loading',
		}]
	}],

	items: [{
		itemId: 'fineUploaderTemplate',
		html: '@{fineUploaderTemplate}',
		height: '@{fineUploaderTemplateHeight}',
		hidden: '@{fineUploaderTemplateHidden}',
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		id: 'attachments_grid',
		store: '@{filesStore}',
		columns: '@{filesColumns}',
		multiSelect: '@{multiSelect}',
		margin: '6 0 0 0',
		flex: 1,
		viewConfig: {
			markDirty: false
		},
		listeners: {
			selectionChange: '@{selectionChanged}'
		}
	}],
	buttons: [{
		text: '~~close~~',
		itemId:  'cancelBnt',
		handler: '@{close}',
		cls : 'rs-med-btn rs-btn-light'
	}]
});

/*
glu.defView('RS.wiki.pagebuilder.Attachments',{
	xtype: 'twomorebuttonuploadmanager',
	statusBarHidden: true,
	padding: 15,	
	cls : 'rs-grid-dark',
	resizable: false,
	resizeHandles: '',
	multiSelect: true,
	documentName: '@{name}',
	allowUpload: true,
	allowRemove: true,
	allowDownload: true,
	recordId: '@{name}',
	nameColumn: 'fileName',
	isEditMode: '@{isEditMode}',
	allowUpload: '@{isEditMode}',
	allowRemove: '@{isEditMode}',
	uploader: {
		url: '/resolve/service/wiki/attachment/upload',
		autoStart: true,
		max_file_size: '2020mb',
		flash_swf_url: '/resolve/js/plupload/Moxie.swf',
		urlstream_upload: true
	},
	listeners: {
		renameAttachment: '@{renameAttachment}',
		referGlobalAttachments: '@{referGlobalAttachments}',
		markAsGlobal: '@{markAsGlobal}'
			// duplicatedetected: '@{duplicateDetected}'
	},
	asWindow: {
		title: '~~attachments~~',
		modal: true,
		cls : 'rs-modal-popup',
		width : 850,
		height :450,
		listeners: {
			beforerender : function(){				
				var toolbar = this.down('button[name="uploadFile"]').ownerCt;
				toolbar.addCls('rs-dockedtoolbar');
				var toolbarItems = toolbar.items;			
				Ext.Array.each(toolbarItems.items, function(btn){					
					btn.addCls('rs-btn-light rs-small-btn');
				})
			}			
		}
	},	
	buttons: [{
        name : 'close',
        cls : 'rs-med-btn rs-btn-dark'     
    }]
});
*/
