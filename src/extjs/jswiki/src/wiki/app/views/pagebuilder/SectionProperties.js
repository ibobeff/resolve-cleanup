glu.defView('RS.wiki.pagebuilder.SectionProperties', {	
	title: '~~sectionPropertiesTitle~~',
	modal: true,
	padding : 15,
	width: '@{calculateWidth}',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},	
	cls : 'rs-modal-popup',
	defaults : {
		margin : '4 0',
		labelWidth : 150,
	},
	items: [{
		xtype: 'textfield',
		name: 'sectionTitle'
	},{
		xtype : 'checkboxgroup',
		fieldLabel : '~~active~~',
		items : [
		{
			xtype : 'checkboxfield',
			checked : '@{active}'
		}]
	},{
		xtype : 'checkboxgroup',
		fieldLabel : '~~hidden~~',
		items : [
		{
			xtype : 'checkboxfield',
			checked : '@{hidden}'
		}]
	},{
		xtype: 'numberfield',
		name: 'sectionHeight'
	},{
		xtype : 'textfield',
		readOnly : true,
		fieldLabel : '~~columns~~',
		value : '@{numberOfColumns}',
		cls : 'hideReadOnlyBorder'
	},{
		xtype: 'fieldcontainer',
		width: '100%',
		fieldLabel: '~~alignments~~',	
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		defaults: {
			padding: '0px 15px 0px 0px',
			valueField: 'value',
			displayField : 'name',
			store: '@{alignments}',
			queryMode: 'local',
			width: 125,
			labelWidth: 35,
		},
		items: [{
			xtype: 'combo',
			name: 'col1Alignment',
		},{
			xtype: 'combo',
			name: 'col2Alignment',
		}, {
			xtype: 'combo',
			name: 'col3Alignment',
		}, {
			xtype: 'combo',
			name: 'col4Alignment',
		}, {
			xtype: 'combo',
			name: 'col5Alignment',
		}, {
			xtype: 'combo',
			name: 'col6Alignment',
		}, {
			xtype: 'combo',
			name: 'col7Alignment',
		}]
	},{
		xtype: 'fieldcontainer',
		width: '100%',
		msgTarget: 'side',
		fieldLabel: '~~columnWidth~~',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		defaults: {
			width: 125,
			minValue: 1,
			labelWidth: 35,
			padding : '0 15px 0 0',
			validateOnChange: true,
			allowDecimals: false,
		},
		items: [{
			xtype: 'numberfield',
			name: 'col1Width',
		},{
			xtype: 'numberfield',
			name: 'col2Width',
		},{
			xtype: 'numberfield',
			name: 'col3Width',
		},{
			xtype: 'numberfield',
			name: 'col4Width',
		},{
			xtype: 'numberfield',
			name: 'col5Width',
		},{
			xtype: 'numberfield',
			name: 'col6Width',
		},{
			xtype: 'numberfield',
			name: 'col7Width',
		}]
	}, {
		xtype: 'displayfield',
		fieldLabel: '',
		fieldStyle: 'margin-left: 155px;color: red;',
		hidden: '@{validWidthsWarningIsVisible}',
		value: '~~invalidWidths~~'
	}],
	buttons : [{
		xtype : 'button',
		name: 'update',
		cls : 'rs-med-btn rs-btn-dark',
		handler : '@{update}'
	},{
		xtype : 'button',
		text : '~~cancel~~',
		cls : 'rs-med-btn rs-btn-light',
		handler : '@{cancel}'
	}]	
});