glu.defView('RS.wiki.pagebuilder.SectionsLayout', {
	itemId: 'SectionsLayoutId',
	xtype: 'panel',
	overflowY: 'auto',
	defaults: {
		flex: 1
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: '@{sectionEntries}',
	listeners : {
		afterrender : function(panel){
			this._vm.on('new_section_added', function(){
				//Lazy way to do scroll.
				setTimeout(function(){
					var d = panel.body.dom;
					var distance = d.scrollHeight - d.offsetHeight;
					panel.body.scroll('b',distance,true);
				},0);
			})
		}
	}
});