
glu.defView('RS.wiki.pagebuilder.ImageManager', {
	isEditMode: '@{isEditMode}',
	asWindow: {
		title: '@{title}',
		cls : 'rs-modal-popup',
		height: 500,
		width: 900,
		modal: true,
		padding : 15,
		closable: true
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	dockedItems: [{
		xtype: 'toolbar',
		margin: '4 0',
		cls: 'rs-dockedtoolbar',
		defaults : {
			cls: 'rs-small-btn rs-btn-light'
		},
		items: [{
			xtype: 'button',
			text: '~~upload~~',
			preventDefault: false,
			hidden: '@{!allowUpload}',
			id: 'upload_button'
		}, {
			name: 'delete',
			hidden: '@{!allowRemove}',
		}, '->', {
			name: 'reload',
			tooltip: '~~reload~~',
			text: '',
			iconCls: 'x-tbar-loading',
		}]
	}],

	items: [{
		itemId: 'fineUploaderTemplate',
		html: '@{fineUploaderTemplate}',
		height: '@{fineUploaderTemplateHeight}',
		hidden: '@{fineUploaderTemplateHidden}',
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		id: 'attachments_grid',
		store: '@{filesStore}',
		columns: '@{filesColumns}',
		multiSelect: '@{multiSelect}',
		margin: '6 0 0 0',
		flex: 1,
		viewConfig: {
			markDirty: false
		},
		listeners: {
			selectionChange: '@{selectionChanged}'
		}
	}],
	listeners: {
		render: function(panel) {
			var grid = this.down('#attachments_grid');
			grid.store.filter([{
				filterFn: function(item) {
					var name = item.get('fileName') || item.get('ufilename') || item.get('name'),
						split = name.split('.'),
						extension = split[split.length - 1].toLowerCase();
					return Ext.Array.indexOf(['png', 'jpg', 'jpeg', 'gif'], extension) > -1
				}
			}])
		}
	},
	buttons: [{
		name: 'select',
		cls: 'rs-med-btn rs-btn-dark',
	}, {
		text: '~~close~~',
		handler: '@{close}',
		cls : 'rs-med-btn rs-btn-light'
	}]		
});

/*
glu.defView('RS.wiki.pagebuilder.ImageManager', {
	parentLayout: 'FileUploadManager',
	propertyPanel: {
		listeners: {
			beforerender : function(){				
				var me = this;
				var buttons = this.down('#cancelBnt').ownerCt;
				buttons.insert(0, {
					name: 'select',
					text: RS.wiki.locale.ImageManager.select,
					cls: 'rs-med-btn rs-btn-dark',
					handler: function() {
						me.fireEvent('select', this)
					}
				});
				this.on({
					render: function() {
						this.down('button[name="select"]').disable();
					},
					updateSelectBtn: function(selections) {
						if (selections.length == 1) {
							this.down('button[name="select"]').enable();
						} else {
							this.down('button[name="select"]').disable();
						}
					},
					afterrender: function() {
						 this.fireEvent('afterRendered', this, this);
					}
				});
			},
			render: function(panel) {
				var grid = this.down('#attachments_grid');
				grid.store.filter([{
					filterFn: function(item) {
						var name = item.get('fileName') || item.get('ufilename') || item.get('name'),
							split = name.split('.'),
							extension = split[split.length - 1].toLowerCase();
						return Ext.Array.indexOf(['png', 'jpg', 'jpeg', 'gif'], extension) > -1
					}
				}])
			},
			reload: '@{reload}',
			select: '@{select}',
			afterRendered: '@{viewRenderCompleted}'
		}
	}
})

glu.defView('RS.wiki.pagebuilder.dummy');
RS.wiki.pagebuilder.views.FileUploadManagerFactory = function(subView) {
	var config = RS.common.views.FileUploadManagerFactory();
	if (subView && subView.propertyPanel) {
		Ext.applyIf(config, subView.propertyPanel);
	}
	return config;
};
*/

/*
glu.defView('RS.wiki.pagebuilder.ImageManager', {
	asWindow: {
		height: 400,
		width: 700,
		title: '~~imageManagerTitle~~',
		modal: true,
		cls :'rs-modal-popup'
	},
	padding : 15,
	layout: 'fit',
	items: [{
		xtype: 'uploadmanager',
		cls : 'rs-grid-dark',
		multiSelect: true,
		documentName: '@{name}',
		allowUpload: true,
		allowRemove: true,
		allowDownload: true,
		recordId: '@{name}',
		nameColumn: 'fileName',
		uploader: {
			url: '/resolve/service/wiki/attachment/upload',
			autoStart: true,
			max_file_size: '2020mb',
			flash_swf_url: '/resolve/js/plupload/plupload.flash.swf',
			urlstream_upload: true
		},
		listeners: {
			selectionchange: '@{selectionchange}',
			render: function(panel) {
				panel.store.filter([{
					filterFn: function(item) {
						var name = item.get('fileName') || item.get('ufilename') || item.get('name'),
							split = name.split('.'),
							extension = split[split.length - 1].toLowerCase();
						return Ext.Array.indexOf(['png', 'jpg', 'jpeg', 'gif'], extension) > -1
					}
				}])
			}
		}
	}],
	buttons: [{
        text : '~~select~~',
        cls : 'rs-med-btn rs-btn-dark',
        handler : '@{select}',
    },{
        text : '~~cancel~~',
        cls : 'rs-med-btn rs-btn-light',
        handler : '@{cancel}',
    }]
})
*/
