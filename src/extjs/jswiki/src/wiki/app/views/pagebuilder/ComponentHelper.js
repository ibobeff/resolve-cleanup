glu.defView('RS.wiki.pagebuilder.TaskEdit', {
	id: "wikiTaskEdit",
	asWindow: {
		height: '@{height}',
		width: 500,
		title: '~~edit~~',
		modal: true
	},
	autoScroll: true,
	bodyPadding: '10px',
	buttonAlign: 'left',
	buttons: ['apply', 'cancel'],
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaultType: 'textfield',
	defaults: {
		labelWidth: 150
	},
	items: [{
			name: 'task',
			readOnly: true
		}, {
			name: 'namespace',
			readOnly: true
		}, 'description',
		'descriptionWikiLink',
		'wiki',
		'nodeId',
		'resultWikiLink', {
			xtype: 'checkbox',
			name: 'autoCollapse'
		}, {
			flex: 1,
			xtype: 'grid',
			name: 'tagGrid',
			hidden: '@{!isTag}',
			store: '@{tagStore}',
			columns: '@{tagColumns}',
			tbar: ['add', 'remove']
		}
	]
})

glu.defView('RS.wiki.pagebuilder.ColumnAdder', {
	asWindow: {
		title: '~~addColumnTitle~~',
		height: 150,
		width: 400
	},
	bodyPadding: '10px',
	buttonAlign: 'left',
	buttons: ['add', 'cancel'],
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'textfield',
		name: 'column'
	}]
})

glu.defView('RS.wiki.pagebuilder.ColumnPicker', {
	asWindow: {
		title: '~~removeColumnTitle~~',
		height: 150,
		width: 400
	},
	bodyPadding: '10px',
	buttonAlign: 'left',
	buttons: ['remove', 'cancel'],
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'combobox',
		name: 'column',
		store: '@{columns}',
		queryMode: 'local',
		displayField: 'name',
		valueField: 'name'
	}]
})

glu.defView('RS.wiki.pagebuilder.XColumnManager', {
	title: '@{title}',	
	width: 600,
	padding : 15,
	cls : 'rs-modal-popup',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults : {
		labelWidth : 130,
		margin : '4 0'
	},
	items: [{
		xtype: 'combobox',
		name: 'column',
		displayField: 'text',
		valueField: 'text',
		queryMode: 'local',
		editable: false
	}, {	
		hidden: '@{!columnIsVisible}',
		html: '<div style="border-bottom:1px solid #bbb"></div>'
	}, {
		xtype: 'textfield',
		name: 'text'
	}, {
		xtype: 'combobox',
		name: 'type',
		displayField: 'name',
		valueField: 'value',
		queryMode: 'local'
	}, {
		xtype: 'textfield',
		name: 'dataIndex'
	}, {
		xtype: 'textfield',
		name: 'link'
	}, {
		xtype: 'textfield',
		name: 'linkTarget'
	}, {
		xtype: 'combobox',
		name: 'format',
		store: '@{formatStore}',
		displayField: 'name',
		displayValue: 'value',
		queryMode: 'local'
	}, {
		xtype: 'combobox',
		name: 'dataFormat',
		store: '@{dataFormatStore}',
		displayField: 'name',
		displayValue: 'value',
		queryMode: 'local'
	}, {
		xtype: 'numberfield',
		name: 'width',
		minValue: 0
	}, {
		xtype: 'checkbox',
		margin : '0 0 0 135',
		hideLabel : true,
		boxLabel : '~~sortable~~',
		name: 'sortable'
	}, {
		xtype: 'checkbox',
		margin : '0 0 0 135',
		hideLabel : true,
		boxLabel : '~~draggable~~',
		name: 'draggable'
	}],
	buttons: [{
		name : 'add',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'apply',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}],
})

glu.defView('RS.wiki.pagebuilder.XColumnPicker', {
	asWindow: {
		title: '~~removeColumnTitle~~',
		height: 150,
		width: 400
	},
	bodyPadding: '10px',
	buttonAlign: 'left',
	buttons: ['remove', 'cancel'],
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'combobox',
		name: 'column',
		store: '@{columns}',
		queryMode: 'local',
		displayField: 'name',
		valueField: 'name'
	}]
})
