glu.defView('RS.wiki.pagebuilder.Source', {
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	title: '@{title}',
	cls : 'rs-modal-popup',
	padding : 15,
	modal: true,
	tools:[{
		margin: '0 5 0 0',
		type:'help',
		tooltip: '~~help~~',
		callback: function(panel, tool, event) {
			this.up().up().fireEvent('getSourceHelp');
		}
	}],		
	items: [{
		flex : 1,
		xtype: 'AceEditor',
		name: 'content'	
	},{
		xtype : 'infoicon',
		margin : '8 0',
		iconType : 'warn',
		displayText : '~~editSourceWarning~~'
	}],
	buttons: [{
        text : '~~update~~',
        cls : 'rs-med-btn rs-btn-dark',
        handler : '@{update}',
    },{
        text : '~~cancel~~',
        cls : 'rs-med-btn rs-btn-light',
        handler : '@{cancel}',
    }],
    listeners: {
		beforeshow: function() {
			this.setWidth(Math.round(Ext.getBody().getWidth() * 0.8));
			this.setHeight(Math.round(Ext.getBody().getHeight() * 0.8));
		},
		getSourceHelp: '@{getSourceHelp}'
	},	
})
