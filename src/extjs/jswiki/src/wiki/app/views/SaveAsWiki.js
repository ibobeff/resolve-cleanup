glu.defView('RS.wiki.SaveAsWiki', {
	title: '@{windowTitle}',	
	width: 600,
	height : 250,
	modal: true,
	padding: 15,
	cls : 'rs-modal-popup',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults : {
		labelWidth : 130
	},
	items: [{
		xtype: 'textfield',
		name: 'namespace'
	}, {
		xtype: 'textfield',
		name: 'name'
	}, {
		xtype: 'checkbox',
		name: 'overwrite',
		hideLabel : true,
		margin : '0 0 0 135',
		boxLabel : '~~overwrite~~'
	}],	
	buttons: [{
		name: 'saveAs',
        cls : 'rs-med-btn rs-btn-dark'
    },{
		name: 'cancel',
        cls : 'rs-med-btn rs-btn-light'
    }]
})