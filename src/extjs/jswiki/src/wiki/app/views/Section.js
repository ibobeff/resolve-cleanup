glu.defView('RS.wiki.Section', {
	title: {
		value: '@{title}',
		trigger: 'dblclick',
		alignment: 'tl',
		listeners: {
			afterlayout: function() {
				var mininumRequired = Math.min(this.boundEl.getWidth(), this.boundEl.parent().getWidth())
				if (this.field.getWidth() != mininumRequired)
					this.field.setWidth(mininumRequired);
			}
		},
		field: {
			xtype: 'textfield'
		}
	},
	hidden: '@{!showSection}',
	resizable: true,
	resizeHandles: 's',
	xtype: 'tabpanel',
	minHeight: 265,
	plain: true,
	defaults: {
		bodyPadding: '10px'
	},
	tabBar: {
		padding: '5px 0px 0px 0px'
	},
	plugins: [{
		ptype: 'tabreorder',
		listeners: {
			drop: function(box, container, dragCmp, startIdx, idx, eOpts) {
				container.up('tabpanel').fireEvent('dropTab', container, startIdx, idx)
			}
		}
	}],
	items: '@{typeConfigurations}',
	listeners: {
		afterrender: function(tabpanel) {
			//Hack to make the section's first tab show "selected" when first opened
			if (!tabpanel.activeTab) tabpanel.setActiveTab(1)
		},
		removeClicked: '@{removeClicked}',
		addAbove: '@{addAbove}',
		addBelow: '@{addBelow}',
		up: '@{up}',
		down: '@{down}',
		dropTab: '@{dropTab}'
	},
	addAbove: '~~addAbove~~',
	addAboveTooltip: '~~addAboveTooltip~~',
	addBelow: '~~addBelow~~',
	addBelowTooltip: '~~addBelowTooltip~~',
	upTooltip: '~~upTooltip~~',
	downTooltip: '~~downTooltip~~',
	upIsEnabled: '@{upIsEnabled}',
	setUpIsEnabled: function(value) {
		if (this.down('#upButton'))
			this.down('#upButton').setDisabled(!value)
		else
			this.tools[0].items[2].disabled = !value
	},
	downIsEnabled: '@{downIsEnabled}',
	setDownIsEnabled: function(value) {
		if (this.down('#downButton'))
			this.down('#downButton').setDisabled(!value)
		else
			this.tools[0].items[3].disabled = !value
	},
	tools: [{
		xtype: 'toolbar',
		items: [{
			xtype: 'button',
			text: '',
			handler: function(btn) {
				btn.up('panel').fireEvent('addAbove')
			},
			listeners: {
				render: function(btn) {
					btn.setText(btn.up('panel').addAbove)
					Ext.create('Ext.tip.ToolTip', {
						target: btn.getEl(),
						html: btn.up('panel').addAboveTooltip
					})
				}
			}
		}, {
			xtype: 'button',
			text: '',
			handler: function(btn) {
				btn.up('panel').fireEvent('addBelow')
			},
			listeners: {
				render: function(btn) {
					btn.setText(btn.up('panel').addBelow)
					Ext.create('Ext.tip.ToolTip', {
						target: btn.getEl(),
						html: btn.up('panel').addBelowTooltip
					})
				}
			}
		}, {
			xtype: 'button',
			iconCls: 'icon-large icon-chevron-up rs-icon',
			itemId: 'upButton',
			handler: function(btn) {
				btn.up('panel').fireEvent('up', btn)
			},
			listeners: {
				render: function(btn) {
					btn.setDisabled(!btn.up('panel').upIsEnabled)
					Ext.create('Ext.tip.ToolTip', {
						target: btn.getEl(),
						html: btn.up('panel').upTooltip
					})
				}
			}
		}, {
			xtype: 'button',
			iconCls: 'icon-large icon-chevron-down rs-icon',
			itemId: 'downButton',
			handler: function(btn) {
				btn.up('panel').fireEvent('down', btn)
			},
			listeners: {
				render: function(btn) {
					btn.setDisabled(!btn.up('panel').downIsEnabled)
					Ext.create('Ext.tip.ToolTip', {
						target: btn.getEl(),
						html: btn.up('panel').downTooltip
					})
				}
			}
		}]
	}, {
		type: 'close',
		handler: function() {
			this.up('panel').fireEvent('removeClicked', this)
		}
	}]
});

glu.defView('RS.wiki.builder.AddTab', {
	reorderable: false,
	iconCls: 'icon-large icon-plus rs-icon',
	hidden: '@{!showAddTab}',
	listeners: {
		beforeactivate: function(tp) {
			if (!this.hidden)
				tp.fireEvent('addTab', tp)
			return false
		},
		addTab: '@{beforeactivate}'
	}
})


glu.defView('RS.wiki.builder.SectionConfiguration', {
	reorderable: false,
	title: '~~sectionConfiguration~~',
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	items: [{
		flex: 1,
		defaults: {
			width: 550,
			labelWidth: 150
		},
		items: [{
				xtype: 'fieldcontainer',
				width: '100%',
				fieldLabel: '~~widths~~',
				hidden: '@{!column1WidthIsVisible}',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				defaults: {
					padding: '0px 10px 0px 0px'
				},
				items: [{
					xtype: 'numberfield',
					maxValue: 100,
					minValue: 0,
					width: 125,
					labelWidth: 35,
					name: 'column1Width'
				}, {
					xtype: 'numberfield',
					maxValue: 100,
					minValue: 0,
					width: 125,
					labelWidth: 35,
					name: 'column2Width'
				}, {
					xtype: 'numberfield',
					maxValue: 100,
					minValue: 0,
					width: 125,
					labelWidth: 35,
					name: 'column3Width'
				}]
			}, {
				xtype: 'fieldcontainer',
				width: '100%',
				fieldLabel: '~~alignments~~',
				hidden: '@{!column1WidthIsVisible}',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				defaults: {
					padding: '0px 10px 0px 0px'
				},
				items: [{
					xtype: 'combo',
					displayField: 'name',
					valueField: 'value',
					store: '@{alignments}',
					queryMode: 'local',
					width: 125,
					labelWidth: 35,
					name: 'column1Alignment'
				}, {
					xtype: 'combo',
					displayField: 'name',
					valueField: 'value',
					store: '@{alignments}',
					queryMode: 'local',
					width: 125,
					labelWidth: 35,
					name: 'column2Alignment'
				}, {
					xtype: 'combo',
					displayField: 'name',
					valueField: 'value',
					store: '@{alignments}',
					queryMode: 'local',
					width: 125,
					labelWidth: 35,
					name: 'column3Alignment'
				}]
			}, {
				xtype: 'textfield',
				fieldLabel: '~~title~~',
				value: '@{..title}'
			}, {
				xtype: 'numberfield',
				fieldLabel: '~~maxHeight~~',
				value: '@{..maxHeight}',
				minValue: -1
			}, {
				xtype: 'numberfield',
				fieldLabel: '~~maxWidth~~',
				value: '@{..maxWidth}',
				minValue: -1
			}, {
				xtype: 'checkbox',
				hideLabel: true,
				name: 'hidden',
				value: '@{..hidden}',
				boxLabel: '~~hidden~~',
				padding: '0 0 0 150'
			}, {
				xtype: 'checkbox',
				hideLabel: true,
				name: 'active',
				value: '@{..active}',
				boxLabel: '~~active~~',
				padding: '0 0 0 150'
			}
			/*, {
			xtype: 'checkbox',
			value: '@{..collapsible}',
			hideLabel: true,
			boxLabel: '~~collapsible~~',
			padding: '0px 0px 0px 155px'
		}, {
			xtype: 'checkbox',
			value: '@{..collapsed}',
			disabled: '@{!..collapsedIsEnabled}',
			hideLabel: true,
			boxLabel: '~~collapsed~~',
			padding: '0px 0px 0px 155px'
		}*/
		]
	}]
})

glu.defView('RS.wiki.builder.Source', {
	title: '~~sourceConfiguration~~',
	layout: 'fit',
	items: [{
		xtype: 'AceEditor',
		name: 'content',
		parser: 'text'
	}],
	closable: true,
	listeners: {
		b4Close: '@{beforeClose}',
		beforeClose: function() {
			this.fireEvent('b4Close', this)
			return false
		}
	}
})

Ext.define('RS.wiki.WikiBuilderEditor', {
	extend: 'RS.common.HtmlLintEditor',
	alias: 'widget.wikibuildereditor',
	initEditor: function() {
		this.callParent(arguments);
		var me = this;
		var doc = me.getDoc();
		Ext.EventManager.on(doc, 'drop', me.fileDrop, me);
		Ext.EventManager.on(doc, 'dragenter', function(e) {
			e.preventDefault();
		}, me);
		Ext.EventManager.on(doc, 'dragover', function(e) {
			e.preventDefault();
		}, me);
		this.reader = new FileReader();
		this.queue = [];
		var me = this;
		this.reader.onload = function(e) {
			me.insertAtCursor(Ext.String.format('<image src="{0}"/>', e.target.result));
			me.queue.shift();
			if (me.queue.length > 0)
				me.reader.readAsDataURL(me.queue[0]);
		}
	},
	fileDrop: function(e) {
		e.preventDefault();
		if (e.browserEvent.type.indexOf('drop') === 0) {
			//Find if any item in the clipboard is an image
			var dataTransfer = e.browserEvent.dataTransfer;
			Ext.each(dataTransfer.files, function(file) {
				if (file.type.indexOf('image') != -1)
					this.queue.push(file);
			}, this);
			this.reader.readAsDataURL(this.queue[0]);
		}
	}
});

glu.defView('RS.wiki.builder.WYSIWYG', {
	title: '~~wysiwygConfiguration~~',
	layout: 'fit',
	cls: 'wysiwyg',
	items: [{
		xtype: 'wikibuildereditor',
		name: 'content',
		hideLabel: true,
		plugins: [
			Ext.create('Ext.ux.form.HtmlEditor.Word'),
			Ext.create('Ext.ux.form.HtmlEditor.IndentOutdent')
		]
	}],
	closable: true,
	listeners: {
		b4Close: '@{beforeClose}',
		beforeClose: function() {
			this.fireEvent('b4Close', this)
			return false
		}
	}
})

glu.defView('RS.wiki.builder.Groovy', {
	title: '~~groovyConfiguration~~',
	layout: 'fit',
	items: [{
		xtype: 'AceEditor',
		name: 'content',
		parser: 'groovy'
	}],
	closable: true,
	listeners: {
		b4Close: '@{beforeClose}',
		beforeClose: function() {
			this.fireEvent('b4Close', this)
			return false
		}
	}
})

glu.defView('RS.wiki.builder.HTML', {
	title: '~~htmlConfiguration~~',
	layout: 'fit',
	items: [{
		xtype: 'AceEditor',
		name: 'content',
		parser: 'html'
	}],
	closable: true,
	listeners: {
		b4Close: '@{beforeClose}',
		beforeClose: function() {
			this.fireEvent('b4Close', this)
			return false
		}
	}
})


glu.defView('RS.wiki.builder.Javascript', {
	title: '~~javascriptConfiguration~~',
	layout: 'fit',
	items: [{
		xtype: 'AceEditor',
		name: 'content',
		parser: 'javascript'
	}],
	closable: true,
	listeners: {
		b4Close: '@{beforeClose}',
		beforeClose: function() {
			this.fireEvent('b4Close', this)
			return false
		}
	}
})


glu.defView('RS.wiki.builder.Code', {
	title: '~~codeConfiguration~~',
	layout: {
		type: 'vbox'
	},
	defaults: {
		width: 550,
		labelWidth: 135
	},
	items: [{
		xtype: 'combobox',
		name: 'type',
		editable: false,
		store: '@{typeStore}',
		displayField: 'name',
		valueField: 'value',
		queryMode: 'local',
		forceSelection: true
	}, {
		xtype: 'checkbox',
		name: 'showLineNumbers'
	}, {
		xtype: 'AceEditor',
		name: 'content',
		flex: 1,
		width: '100%',
		parser: '@{type}',
		showGutter: '@{showLineNumbers}',
		listeners: {
			render: function() {
				var editor = this.down('#' + this.editorId);
				this.getEl().on('click', function() {
					editor.focus();
				});
			}
		}
	}],
	closable: true,
	listeners: {
		b4Close: '@{beforeClose}',
		beforeClose: function() {
			this.fireEvent('b4Close', this)
			return false
		}
	}
})


glu.defView('RS.wiki.builder.Image', {
	title: '~~imageConfiguration~~',
	defaults: {
		width: 550
	},
	items: [{
		xtype: 'imagepickerfield',
		editable: false,
		name: 'src',
		listeners: {
			attach: '@{attach}'
		}
	}, {
		xtype: 'numberfield',
		name: 'height'
	}, {
		xtype: 'numberfield',
		name: 'width'
	}, {
		xtype: 'checkbox',
		name: 'zoom'
	}],
	closable: true,
	listeners: {
		b4Close: '@{beforeClose}',
		beforeClose: function() {
			this.fireEvent('b4Close', this)
			return false
		}
	}
})


glu.defView('RS.wiki.builder.Encrypt', {
	title: '~~encryptConfiguration~~',
	defaults: {
		width: 550
	},
	items: [{
		xtype: 'triggerfield',
		editable: false,
		name: 'key',
		triggerCls: 'x-form-search-trigger',
		onTriggerClick: function() {
			this.fireEvent('choose', this)
		},
		listeners: {
			choose: '@{chooseKey}'
		}
	}, {
		xtype: 'triggerfield',
		editable: false,
		name: 'value',
		triggerCls: 'x-form-search-trigger',
		onTriggerClick: function() {
			this.fireEvent('choose', this)
		},
		listeners: {
			choose: '@{chooseValue}'
		}
	}],
	closable: true,
	listeners: {
		b4Close: '@{beforeClose}',
		beforeClose: function() {
			this.fireEvent('b4Close', this)
			return false
		}
	}
})


glu.defView('RS.wiki.builder.Infobar', {
	title: '~~infobarConfiguration~~',
	items: [{
		xtype: 'numberfield',
		name: 'height',
		minValue: 0
	}, {
		xtype: 'checkbox',
		name: 'social'
	}, {
		xtype: 'checkbox',
		name: 'rating'
	}, {
		xtype: 'checkbox',
		name: 'attachments'
	}, {
		xtype: 'checkbox',
		name: 'history'
	}, {
		xtype: 'checkbox',
		name: 'tags'
	}],
	closable: true,
	listeners: {
		b4Close: '@{beforeClose}',
		beforeClose: function() {
			this.fireEvent('b4Close', this)
			return false
		}
	}
})


glu.defView('RS.wiki.builder.Model', {
	title: '~~modelConfiguration~~',
	defaults: {
		width: 550,
		labelWidth: 120
	},
	items: [{
		name: 'wiki',
		xtype: 'triggerfield',
		triggerCls: 'x-form-search-trigger',
		onTriggerClick: function() {
			this.fireEvent('choose', this)
		},
		listeners: {
			choose: '@{chooseDocument}'
		}
	}, {
		xtype: 'combobox',
		name: 'status',
		store: '@{statusStore}',
		displayField: 'name',
		valueField: 'value',
		queryMode: 'local',
		editable: false
	}, {
		name: 'refreshInterval',
		xtype: 'numberfield',
		minValue: 0
	}, {
		name: 'zoom',
		xtype: 'numberfield',
		minValue: 0
	}, {
		name: 'height',
		xtype: 'numberfield',
		minValue: 0
	}, {
		name: 'width',
		xtype: 'numberfield',
		minValue: 0
	}],
	closable: true,
	listeners: {
		b4Close: '@{beforeClose}',
		beforeClose: function() {
			this.fireEvent('b4Close', this)
			return false
		}
	}
})


glu.defView('RS.wiki.builder.Form', {
	title: '~~formConfiguration~~',
	defaults: {
		width: 550
	},
	items: [{
		xtype: 'formpickerfield',
		name: 'name',
		allowCreate: true,
		listeners: {
			choose: '@{chooseForm}',
			edit: '@{editForm}',
			create: '@{createForm}'
		}
	}, {
		xtype: 'textfield',
		name: 'title'
	}, {
		xtype: 'textfield',
		name: 'tooltip'
	}, {
		xtype: 'checkbox',
		name: 'collapsed'
	}, {
		xtype: 'checkbox',
		name: 'popout'
	}, {
		xtype: 'numberfield',
		name: 'popoutHeight',
		minValue: -1
	}, {
		xtype: 'numberfield',
		name: 'popoutWidth',
		minValue: -1
	}],
	closable: true,
	listeners: {
		b4Close: '@{beforeClose}',
		beforeClose: function() {
			this.fireEvent('b4Close', this)
			return false
		}
	}
})


glu.defView('RS.wiki.builder.WikiDoc', {
	title: '~~wikiDocConfiguration~~',
	layout: 'hbox',
	items: [{
		xtype: 'triggerfield',
		width: 550,
		labelWidth: 150,
		name: 'wiki',
		editable: false,
		triggerCls: 'x-form-search-trigger',
		onTriggerClick: function() {
			this.fireEvent('choose', this)
		},
		listeners: {
			choose: '@{chooseWikiDocument}'
		}
	}, {
		xtype: 'image',
		cls: 'rs-btn-edit',
		listeners: {
			handler: '@{jumpToWiki}',
			render: function(image) {
				image.getEl().on('click', function() {
					image.fireEvent('handler')
				})
			}
		}
	}],
	closable: true,
	listeners: {
		b4Close: '@{beforeClose}',
		beforeClose: function() {
			this.fireEvent('b4Close', this)
			return false
		}
	}
})


glu.defView('RS.wiki.builder.Result', {
	title: '~~resultConfiguration~~',
	minHeight: 400,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaults: {
			labelWidth: 125
		},
		items: [{
			xtype: 'textfield',
			name: 'title'
		}, {
			xtype: 'combobox',
			name: 'filterValue',
			store: '@{filterStore}',
			displayField: 'name',
			valueField: 'value',
			queryMode: 'local',
			multiSelect: true
		}, {
			xtype: 'combobox',
			name: 'order',
			store: '@{orderStore}',
			displayField: 'name',
			valueField: 'value',
			queryMode: 'local'
		}, {
			xtype: 'numberfield',
			name: 'refreshInterval'
		}, {
			xtype: 'numberfield',
			name: 'descriptionWidth'
		}, {
			xtype: 'checkbox',
			name: 'autoCollapse'
		}, {
			xtype: 'checkbox',
			name: 'autoHide'
		}, {
			xtype: 'checkbox',
			name: 'encodeSummary'
		}, {
			xtype: 'checkbox',
			name: 'progress'
		}]
	}, {
		xtype: 'treepanel',
		title: '~~actionTasks~~',
		name: 'actionTasks',
		columns: '@{actionTasksColumns}',
		rootVisible: false,
		tbar: ['addGroup', 'addActionTask', /* 'addTag',*/ 'edit', 'remove'],
		width: '100%',
		flex: 1,
		viewConfig: {
			plugins: {
				ptype: 'treeviewdragdrop'
			}
		},
		plugins: [{
			ptype: 'cellediting',
			pluginId: 'celledit',
			clicksToEdit: 1,
			listeners: {
				beforeEdit: function(editor, e) {
					if (!e.record.isLeaf() && (e.column.dataIndex != 'description' && e.column.dataIndex != 'autoCollapse')) {
						return false
					}
				},
				edit: function(editor, e) {
					e.record.commit()
				}
			}
		}],
		listeners: {
			selectionchange: '@{selectionchange}',
			itemdblclick: '@{itemdblclick}'
		}
	}],
	closable: true,
	listeners: {
		b4Close: '@{beforeClose}',
		beforeClose: function() {
			this.fireEvent('b4Close', this)
			return false
		}
	}
})

glu.defView('RS.wiki.builder.TaskEdit', {
	id: "wikiTaskEdit",
	asWindow: {
		height: '@{height}',
		width: 500,
		title: '~~edit~~',
		modal: true
	},
	autoScroll: true,
	bodyPadding: '10px',
	buttonAlign: 'left',
	buttons: ['apply', 'cancel'],
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaultType: 'textfield',
	defaults: {
		labelWidth: 150
	},
	items: [{
			name: 'task',
			readOnly: true
		}, {
			name: 'namespace',
			readOnly: true
		}, 'description',
		'descriptionWikiLink',
		'wiki',
		'nodeId',
		'resultWikiLink', {
			xtype: 'checkbox',
			name: 'autoCollapse'
		}, {
			flex: 1,
			xtype: 'grid',
			name: 'tagGrid',
			hidden: '@{!isTag}',
			store: '@{tagStore}',
			columns: '@{tagColumns}',
			tbar: ['add', 'remove']
		}
	]
})

glu.defView('RS.wiki.builder.Detail', {
	title: '~~detailConfiguration~~',
	layout: {
		type: 'vbox'
	},
	items: [{
		xtype: 'fieldcontainer',
		layout: 'hbox',
		defaults: {
			labelWidth: 50,
			width: 120,
			padding: '0 20 0 0'
		},
		items: [{
			xtype: 'textfield',
			name: 'maximum'
		}, {
			xtype: 'textfield',
			name: 'offset'
		}, {
			xtype: 'checkbox',
			hideLabel: true,
			boxLabel: '~~progress~~',
			name: 'progress'
		}]
	}, {
		xtype: 'treepanel',
		title: '~~actionTasks~~',
		store: '@{actionTasks}',
		columns: '@{actionTasksColumns}',
		rootVisible: false,
		tbar: ['addActionTask', 'remove'],
		width: '100%',
		flex: 1,
		useArrows: true,
		viewConfig: {
			plugins: {
				ptype: 'treeviewdragdrop'
			}
		},
		plugins: [{
			ptype: 'cellediting',
			pluginId: 'celledit',
			clicksToEdit: 1,
			listeners: {
				beforeEdit: function(editor, e) {
					if (!e.record.isLeaf() && (e.column.dataIndex != 'description' && e.column.dataIndex != 'autoCollapse')) {
						return false
					}
				},
				edit: function(editor, e) {
					e.record.commit()
				}
			}
		}],
		listeners: {
			selectionchange: '@{selectionchange}'
		}
	}],
	closable: true,
	listeners: {
		b4Close: '@{beforeClose}',
		beforeClose: function() {
			this.fireEvent('b4Close', this)
			return false
		}
	}
})


glu.defView('RS.wiki.builder.Automation', {
	title: '~~automationConfiguration~~',
	defaults: {
		width: 550
	},
	items: [{
		xtype: 'trigger',
		trigger1Cls: 'x-form-search-trigger',
		trigger2Cls: 'rs-btn-edit',
		editable: false,
		name: 'name',
		onTrigger1Click: function() {
			this.fireEvent('chooseRunbook');
		},
		onTrigger2Click: function() {
			this.fireEvent('editAutomation');
		},
		listeners: {
			chooseRunbook: '@{chooseRunbook}',
			editAutomation: '@{editAutomation}'
		}
	}, {
		xtype: 'textfield',
		name: 'title'
	}, {
		xtype: 'textfield',
		name: 'tooltip'
	}, {
		xtype: 'checkbox',
		name: 'collapsed'
	}, {
		xtype: 'checkbox',
		name: 'popout'
	}, {
		xtype: 'numberfield',
		name: 'popoutHeight',
		minValue: -1
	}, {
		xtype: 'numberfield',
		name: 'popoutWidth',
		minValue: -1
	}],
	closable: true,
	listeners: {
		b4Close: '@{beforeClose}',
		beforeClose: function() {
			this.fireEvent('b4Close', this)
			return false
		}
	}
})

glu.defView('RS.wiki.builder.ActionTask', {
	title: '~~actionTaskConfiguration~~',
	defaults: {
		width: 550
	},
	items: [{
		xtype: 'formpickerfield',
		name: 'displayName',
		allowCreate: true,
		listeners: {
			choose: '@{chooseForm}',
			edit: '@{editForm}',
			create: '@{createForm}'
		}
	}, {
		xtype: 'textfield',
		name: 'title'
	}, {
		xtype: 'textfield',
		name: 'tooltip'
	}, {
		xtype: 'checkbox',
		name: 'collapsed'
	}, {
		xtype: 'checkbox',
		name: 'popout'
	}, {
		xtype: 'numberfield',
		name: 'popoutHeight',
		minValue: -1
	}, {
		xtype: 'numberfield',
		name: 'popoutWidth',
		minValue: -1
	}],
	closable: true,
	listeners: {
		b4Close: '@{beforeClose}',
		beforeClose: function() {
			this.fireEvent('b4Close', this)
			return false
		}
	}
})

glu.defView('RS.wiki.builder.Table', {
	title: '~~tableConfiguration~~',
	bodyPadding: '0px',
	xtype: 'grid',
	name: 'table',
	store: '@{table}',
	setStore: function(value) {
		this.reconfigure(value, null)
	},
	columns: '@{tableColumns}',
	setColumns: function(value) {
		this.reconfigure(this.store, value)
	},
	plugins: [{
		ptype: 'cellediting',
		pluginId: 'celledit',
		clicksToEdit: 1,
		listeners: {
			edit: function(editor, e) {
				e.record.commit()
			}
		}
	}],
	tbar: ['addColumn', 'removeColumn', 'addRow', 'insertRow', 'removeRow'],
	closable: true,
	listeners: {
		b4Close: '@{beforeClose}',
		beforeClose: function() {
			this.fireEvent('b4Close', this)
			return false
		}
	}
})

glu.defView('RS.wiki.builder.ColumnAdder', {
	asWindow: {
		title: '~~addColumnTitle~~',
		height: 150,
		width: 400
	},
	bodyPadding: '10px',
	buttonAlign: 'left',
	buttons: ['add', 'cancel'],
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'textfield',
		name: 'column'
	}]
})

glu.defView('RS.wiki.builder.ColumnPicker', {
	asWindow: {
		title: '~~removeColumnTitle~~',
		height: 150,
		width: 400
	},
	bodyPadding: '10px',
	buttonAlign: 'left',
	buttons: ['remove', 'cancel'],
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'combobox',
		name: 'column',
		store: '@{columns}',
		queryMode: 'local',
		displayField: 'name',
		valueField: 'name'
	}]
})

glu.defView('RS.wiki.builder.XColumnPicker', {
	asWindow: {
		title: '~~removeColumnTitle~~',
		height: 150,
		width: 400
	},
	bodyPadding: '10px',
	buttonAlign: 'left',
	buttons: ['remove', 'cancel'],
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'combobox',
		name: 'column',
		store: '@{columns}',
		queryMode: 'local',
		displayField: 'name',
		valueField: 'name'
	}]
})

glu.defView('RS.wiki.builder.XTable', {
	title: '~~xtableConfiguration~~',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'textfield',
		name: 'sql'
	}, {
		xtype: 'textfield',
		name: 'drilldown'
	}, {
		xtype: 'grid',
		flex: 1,
		name: 'table',
		store: '@{table}',
		setStore: function(value) {
			this.reconfigure(value, null)
		},
		columns: '@{tableColumns}',
		setColumns: function(value) {
			this.reconfigure(this.store, value)
		},
		plugins: [{
			ptype: 'cellediting',
			pluginId: 'celledit',
			clicksToEdit: 1,
			listeners: {
				edit: function(editor, e) {
					e.record.commit()
				}
			}
		}],
		tbar: ['addColumn', 'editColumn', 'removeColumn'], // 'addRow', 'insertRow', 'removeRow'],
		listeners: {
			columnmove: '@{columnMove}'
		}
	}],
	closable: true,
	listeners: {
		b4Close: '@{beforeClose}',
		beforeClose: function() {
			this.fireEvent('b4Close', this)
			return false
		}
	}
})

glu.defView('RS.wiki.builder.XColumnManager', {
	asWindow: {
		title: '@{title}',
		height: 365,
		width: 450
	},
	bodyPadding: '10px',
	buttonAlign: 'left',
	buttons: ['add', 'apply', 'cancel'],
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'combobox',
		name: 'column',
		displayField: 'text',
		valueField: 'text',
		queryMode: 'local',
		editable: false
	}, {
		padding: '5 0 10 0',
		hidden: '@{!columnIsVisible}',
		html: '<div style="border-bottom:1px solid #bbb"></div>'
	}, {
		xtype: 'textfield',
		name: 'text'
	}, {
		xtype: 'combobox',
		name: 'type',
		displayField: 'name',
		valueField: 'value',
		queryMode: 'local'
	}, {
		xtype: 'textfield',
		name: 'dataIndex'
	}, {
		xtype: 'textfield',
		name: 'link'
	}, {
		xtype: 'textfield',
		name: 'linkTarget'
	}, {
		xtype: 'combobox',
		name: 'format',
		store: '@{formatStore}',
		displayField: 'name',
		displayValue: 'value',
		queryMode: 'local'
	}, {
		xtype: 'combobox',
		name: 'dataFormat',
		store: '@{dataFormatStore}',
		displayField: 'name',
		displayValue: 'value',
		queryMode: 'local'
	}, {
		xtype: 'numberfield',
		name: 'width',
		minValue: 0
	}, {
		xtype: 'checkbox',
		name: 'sortable'
	}, {
		xtype: 'checkbox',
		name: 'draggable'
	}]
})

glu.defView('RS.wiki.builder.Progress', {
	title: '~~progressConfiguration~~',
	defaults: {
		width: 550
	},
	items: [{
		xtype: 'imagepickerfield',
		emptyText: '~~defaultLoadingImage~~',
		editable: false,
		name: 'text',
		listeners: {
			attach: '@{attach}'
		}
	}, {
		xtype: 'textfield',
		name: 'displayText'
	}, {
		xtype: 'textfield',
		name: 'completedText'
	}, {
		xtype: 'numberfield',
		name: 'height'
	}, {
		xtype: 'numberfield',
		name: 'width'
	}, {
		xtype: 'numberfield',
		name: 'border'
	}, {
		xtype: 'fieldcontainer',
		fieldLabel: '~~borderColor~~',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'toolbar',
			items: [{
				xtype: 'button',
				text: '@{bgStyle}',
				padding: '5px 0px 0px 5px',
				menu: {
					xtype: 'colormenu',
					handler: '@{selectBgColor}'
				}
			}]
		}]
	}],
	closable: true,
	listeners: {
		b4Close: '@{beforeClose}',
		beforeClose: function() {
			this.fireEvent('b4Close', this)
			return false
		}
	}
})

glu.defView('RS.wiki.builder.SocialLink', {
	title: '~~socialLinkConfiguration~~',
	defaults: {
		width: 550,
		labelWidth: 120
	},
	items: [{
		xtype: 'textfield',
		name: 'displayText'
	}, {
		xtype: 'textfield',
		name: 'preceedingText'
	}, {
		xtype: 'checkbox',
		name: 'openInNewTab'
	}, {
		xtype: 'comboboxselect',
		displayField: 'name',
		valueField: 'id',
		itemId: 'toField',
		name: 'to',
		queryMode: 'local',
		editable: false,
		multiSelect: true,
		tpl: '<tpl for="."><div class="x-boundlist-item">{name} {[values["type"] ? "(" + values["type"] + ")" : ""]}</div></tpl>',
		triggerOnClick: false,
		triggerCls: '',
		onTriggerClick: function() {
			this.fireEvent('showAdvancedTo', this)
		},
		listeners: {
			showAdvancedTo: '@{showAdvancedTo}'
		}
	}, {
		xtype: 'hidden',
		name: 'toHidden',
		setValue: function(value) {
			if (this.rendered) {
				var form = this.up('panel');
				if (form) form.down('#toField').setValue(value)
			} else {
				this.initialVal = value
			}
		},
		listeners: {
			render: function(field) {
				if (field.initialVal) {
					var form = field.up('panel')
					if (form) form.down('#toField').setValue(this.initialVal)
				}
			}
		}
	}],
	closable: true,
	listeners: {
		b4Close: '@{beforeClose}',
		beforeClose: function() {
			this.fireEvent('b4Close', this)
			return false
		}
	}
})