glu.defView('RS.wiki.RunbookWizard', {
	asWindow: {
		height: 400,
		width: 700,
		title: '~~runbookWizardTitle~~',
		modal: true
	},

	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'runbooks',
		columns: '@{runbooksColumns}',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: [{
				xtype: 'textfield',
				name: 'searchText',
				hideLabel: true,
				width: 250,
				emptyText: '~~searchRunbooks~~'
			}]
		}],
		plugins: [{
			ptype: 'pager'
		}]
	}],

	buttonAlign: 'left',
	buttons: ['create', 'cancel']
})