glu.defView('RS.decisiontree.ContentPage', {
	padding : '@{regionPadding}',
	region : 'center',	
	flex : 1,
	layout: 'fit',
	cls: 'wiki-content',
	items : [{
		itemId: 'decisiontreeContent',
		overflowY : 'auto',	
		padding: '0 0 0 0',
		bodyPadding : 0,
		html : '@{content}'
	}],
	listeners : {
		afterrender : function(panel) {
			var p = panel.up().down('#navigationContainer');
			var hideExpand = p.collapsed ? 'Expand' : 'Hide';
			var btn = Ext.DomHelper.insertFirst(panel.el, '<div class="dt-collapse-btn"><img class="dt-collapse-btn-img" id="dt-collapse-btn-img-id" src="/resolve/images/collapse-button.png"/><span id="dt-collapse-btn-label-id" class="dt-collapse-btn-label">Hide</span></div>');
			btn.onclick = function() {
				var p = panel.up().down('#navigationContainer');
				var label = document.getElementById('dt-collapse-btn-label-id');
				var image = document.getElementById('dt-collapse-btn-img-id');
				if (p.collapsed) {
					p.expand();
					p.setVisible(true);
					p.expand();
					image.classList.remove('expand');
					label.innerText = 'Hide';
				} else {
					p.collapse();
					p.setVisible(false);
					image.classList.add('expand');
					label.innerText = 'Expand';
				}
			}  	
			this._vm.on('updatePadding', function(questionOnTop){
				if(!questionOnTop){
					panel.getEl().setStyle({ padding : '0 0 10px 0'});
					panel.doLayout();
				}
			});
    	},
		beforedestroy : '@{beforeDestroyComponent}',
	}
})
