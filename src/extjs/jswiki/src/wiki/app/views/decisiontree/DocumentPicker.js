glu.defView('RS.decisiontree.DocumentPicker', {
	layout : 'fit',
	title: '~~decisionTree~~',
	cls : 'rs-modal-popup',
	padding : 15,	
	modal: true,	
	//layout: 'card',
	//activeItem: '@{activeItem}',
	items: [{
		layout: 'border',
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true,
			useWindowParams: false
		}, {
			ptype: 'pager'
		}],
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: []
		}],
		items: [{
			region: 'west',
			width: 200,
			maxWidth: 300,		
			xtype: 'treepanel',
			cls : 'rs-grid-dark',
			split : true,
			columns: '@{moduleColumns}',
			name: 'runbookTree',
			listeners: {
				selectionchange: '@{menuPathTreeSelectionchange}'
			}

		}, {
			region: 'center',
			xtype: 'grid',
			displayName: '~~title~~',
			cls : 'rs-grid-dark',
			name: 'runbookGrid',
			columns: [{
				header: '~~name~~',
				dataIndex: 'ufullname',
				filterable: true,
				flex: 1
			}, {
				dataIndex: 'usummary',
				header: '~~summary~~',
				filterable: true,
				hidden: true,
				visible: false
			}, {
				dataIndex: 'uisRoot',
				header: '~~uisRoot~~',
				filterable: true,
				sortable: true,
				width: 100,
				align : 'center',
				renderer: RS.common.grid.booleanRenderer()
			}].concat((function() {
				var cols = RS.common.grid.getSysColumns();
				Ext.each(cols, function(c) {
					if (c.dataIndex == 'sysUpdatedOn' || c.dataIndex == 'sysUpdatedBy')
						c.hidden = false;
				})
				return cols;
			})()),
			multiSelect: false,
			selected: '@{runbookSelected}',
			autoScroll: true,
			viewConfig: {
				enableTextSelection: true
			},
			plugins: [{
				ptype: 'resolveexpander',
				pluginId: 'rowExpander',
				rowBodyTpl: new Ext.XTemplate('<div resolveId="resolveRowBody" style="color:#888"><span style="font-weight:bold">Summary:</span><br/><br/>&nbsp&nbsp&nbsp&nbsp{usummary:htmlEncode}</div>')
			}],
			selModel: {
				selType: 'resolvecheckboxmodel',
				columnTooltip: RS.common.locale.editColumnTooltip,
				columnTarget: '_self',
				columnEventName: 'editAction',
				columnIdField: 'ufullname'
			},
			listeners: {
				render: function(grid) {
					var plugin = grid.getPlugin('rowExpander')
					if (plugin)
						grid.on('cellclick', function(view, td, cellIndex, record, tr, rowIndex, e) {
							if (cellIndex > 0) {
								plugin.onDblClick(view, record, null, rowIndex, e)
							}
						})
				},
				editAction: '@{editRunbook}'
			}
		}],
		margin : {
			bottom : 10
		}
	}
	/*,{
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaultType: 'textfield',
		bodyPadding: '10px',
		items: ['name', 'namespace'],
		listeners: {
			beforeshow: function() {
				this.up().up().setWidth(400)
				this.up().up().setHeight(200)
			},
			afterLayout: function() {
				this.up().up().center();
			}
		}
	}*/],
	listeners: {
		beforeshow: function() {
			this.setWidth(Math.round(Ext.getBody().getWidth() * 0.8))
			this.setHeight(Math.round(Ext.getBody().getHeight() * 0.8))
		}
	},
	buttons: [{
		name : 'select',
		cls : 'rs-med-btn rs-btn-dark'
	},{/* 'newDocument', 'createDocument', */
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}],
});