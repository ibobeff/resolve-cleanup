glu.defView('RS.decisiontree.Navigation',{	
	region : '@{region}',
	padding : '@{regionPadding}',
	width : '18%',
	layout : {
		type: 'vbox',
		align: 'stretch'
	},
	style: 'background-color: #ebf1f5;',
	cls: 'rs-dt-navigation-panel',
	items : [{
		flex: 1,
		// title : 'Navigation',
		itemId: 'navigationContainer',
		// collapsible : true,
		collapseDirection : 'left',
		animCollapse : false,	
		collapsed : '@{collapsed}', 
		overflowY : 'auto',
		width : '18%',
		items : '@{navigationEntryList}', 
		// style : 'border:1px solid silver;',
		bodyStyle: 'background: #ebf1f5 !important',
		listeners : {
			beforeexpand : function(panel){
				panel.up().setWidth('18%');
				panel.up().doLayout();
			},
			beforecollapse :  function(panel){
				panel.up().setWidth('initial');
				panel.up().doLayout();
			}
		}
	}],
	
    listeners : {
    	afterrender : function(panel){
			this._vm.on('updateRegion', function(newRegion){
				panel.setBorderRegion(newRegion);
				if(newRegion == "east"){
					panel.getEl().setStyle({padding : '0 0 0 10px'});
					panel.doLayout();
				}
			});
    	}    	
    }
});

glu.defView('RS.decisiontree.NavigationEntry', {
	cls : '@{containerCls}',	
	items: [{
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		cls: 'decision-tree-qa qa-@{selectedCss}',	
		items : [{
			xtype: 'container',
			cls: 'dt-question-container',
			layout: {
				type: 'hbox',
				align: 'middle'
			},
			minHeight: 21,
			items: [{
				xtype: 'image',
				src: '/resolve/images/question.png',
				width: 21,
				hidden: '@{isEnd}',
				cls: 'dt-navigation-entry-question-icon'
			}, {
				xtype: 'label',
				flex: 10,
				cls: 'dt-navigation-entry-question',
				text: '@{displayQuestion}',
			}]
		}, {
			cls: 'dt-navigation-entry-answer',
			html: '@{displayAnswer}',
			hidden : '@{answerIsHidden}'
		}, {
			xtype: 'container',
			cls: 'answers-container',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: '@{answers}',
			hidden: '@{hideAnswers}',
		}],
	}],
	
	listeners: {
		render: function(panel) {
			panel.getEl().on('click', function() {
				panel.fireEvent('select', panel)
			})
		},
		select: '@{select}'		
	}
});

Ext.define('RS.decisiontree.AnswerDecorator', {
	extend: 'Ext.container.Container',
	alias: 'widget.answerdecorator',
	config: {
		recommendedAnswerText:  ''
	},
	setRecommendedAnswerText: function(recommendedAnswerText) {
		var answer = this.down('#answerId');
		answer.update(answer._vm.answer + answer._vm.getRecommendationTextWithStyle(recommendedAnswerText));
	},
	setAnswerText: function(answerText) {
		var answer = this.down('#answerId');
		answer.update(answerText + answer._vm.getRecommendationTextWithStyle());
	}
});

glu.defView('RS.decisiontree.Answer', {
	xtype: 'answerdecorator',
	recommendedAnswerText: '@{recommendedAnswerText}',
	answerText: '@{answer}',
	cls: 'answer-container icon-ok-sign-@{answeredIconVisibility}',
	layout: {
		type: 'hbox',
		align: 'middle'
	},
	minHeight: 38,
	items:[{
		xtype: 'image',
		src: '/resolve/images/recommended-answer.png',
		width: 19,
		cls: 'dt-answer-recommended @{recommendedAnswerIcon}',
		title: '~~recommededAnswer~~',
		hidden: true,
	}, {
		xtype: 'button',
		width: 30,
		height: 27,
		cls: 'dt-answer-selected',
		iconCls: 'dt-answer-selected icon-ok-sign @{answeredIconVisibility}',
	}, {
		xtype: 'container',
		itemId: 'answerId',
		flex: 10,
		html: '@{answer}',
		cls: 'dt-answer-button icon-ok-sign-@{answeredIconVisibility}',
		listeners: {
			afterrender: function(v) {
				v.getEl().on('click', function() {
					v.fireEvent('answerSelected');
				});
			},
			answerSelected: '@{answerSelected}'
		}
	}, {
		xtype: 'container',
		width: 28,
		height: 27,
	}]

});
