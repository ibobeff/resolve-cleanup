glu.defView('RS.wiki.DecisionTreeViewer', {
	layout: {
		type: 'border',
		regionWeights: {
			west: 20,
			north: 10,
			south: 10,
			east: 20
		}
	},
	dummy : '@{dummy}',
	listeners: {
		afterrender: function(panel) {
			var splitters = this.query('bordersplitter')
			Ext.Array.forEach(splitters, function(splitter) {
				splitter.setSize(1, 1)
				splitter.getEl().setStyle({
					backgroundColor: '#fbfbfb'
				})
			})
			//Hack to remove hidden region which will cause side effect on answer selection.
			this._vm.on('removeRegion', function(regionId){				
				var region = panel.down('#' + regionId);
				panel.remove(region, true);
			})
		},		
	},

	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		style: 'padding: 12px 0px 2px 18px',
		items: [{
				xtype: 'tbtext',
				cls: 'rs-display-name',
				text: '@{name}'
			}
		]
	}],

	items: [{
			region: 'west',
			width: 250,
			split: true,
			hidden: '@{!decisionTreeLoaded}',
			collapsed: '@{!displayQuestions}',
			collapseMode: 'mini',
			bodyCls: 'decision-tree-questions-container',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: '@{questions}',
			tbar: [{
				iconCls: 'icon-chevron-left rs-icon-button',
				name: 'previousQuestion'
			}, {
				iconCls: 'icon-chevron-right rs-icon-button',
				name: 'nextQuestion',
				iconAlign: 'right'
			}, '->', {
				xtype: 'button',
				name: 'refreshQuestions',
				text: '',
				iconCls: 'x-btn-icon x-tbar-loading',
				listeners: {
					render: function(button) {
						clientVM.updateRefreshButtons(button);
					}
				}
			}, {
				iconCls: 'icon-pushpin rs-icon-button',
				pinned: true,
				handler: function(button) {
					button.pinned = !button.pinned
					button.up('panel').fireEvent('pinChanged', button, button.pinned)
					if (!button.pinned)
						button.setIconCls('icon-pushpin rs-icon-button icon-rotate-90')
					else
						button.setIconCls('icon-pushpin rs-icon-button')
				}
			}],
			listeners: {
				previousQuestion: '@{previousQuestion}',
				nextQuestion: '@{nextQuestion}',
				//refreshQuestions: '@{refreshQuestions}',
				afterrender: function(panel) {
					Ext.defer(function() {
						panel.getEl().on('mouseover', function(e, target) {
							panel.fireEvent('expandIfNeeded')
						})
						panel.splitter.getEl().on('mouseover', function(e, target) {
							panel.fireEvent('expandIfNeeded')
						})
					}, 1)
				},
				expandIfNeeded: '@{expandIfNeeded}',
				pinChanged: '@{pinToggled}'
			}
		},{
			region: 'center',			
			html: '@{content}',
			overflowY : 'auto',	
			listeners: {			
				afterrender: function(panel) {
					panel.getEl().on('mouseover', function(e, target) {
						panel.fireEvent('mouseover')
					}, this);					
				},
				mouseover: '@{collapseIfNeeded}'
			}
		},{
			region: 'east',
			split: true,
			width: 480,
			stateId: 'wikiSocialBar',
			stateful: true,
			hidden: '@{!socialIsPressed}',
			layout: 'fit',
			items: [{
				xtype: '@{socialDetail}'
			}]
		},{
			region: 'north',
			itemId : 'northRegion',
			title: '@{currentQuestion}',		
			hidden: '@{northQuestionIsHidden}',
			split: true,
			bodyPadding: '10px',
			//height: '@{height}',
			showSocial: '@{showSocial}',
			setShowSocial: function(value) {
				this.down('#socialButton')[value ? 'show' : 'hide']()
			},
			showEditWiki: '@{editWikiIsVisible}',
			setShowEditWiki: function(value) {
				this.down('#editWikiButton')[value ? 'show' : 'hide']()
			},
			socialPressed: '@{socialIsPressed}',
			setSocialPressed: function(value) {
				if (!(this.down('#socialButton').pressed && value))
					this.down('#socialButton').toggle();
			},
			feedbackCls: '@{feedbackCls}',
			setFeedbackCls: function(cls) {
				var btn = this.down('#feedBackButton');
				btn.setIconCls('rs-social-button ' + cls);
			},
			tools: [{
				xtype: 'button',
				itemId: 'feedBackButton',
				ui: 'clear-button-small-toolbar',
				iconCls: 'rs-social-button icon-flag-alt',
				handler: function(button) {
					button.up('panel').fireEvent('showFeedback', button, button)
				}
			}, {
				xtype: 'button',
				itemId: 'socialButton',
				ui: 'clear-button-small-toolbar',
				iconCls: 'rs-social-button icon-comment-alt',
				enableToggle: true,
				pressed: false,
				listeners: {
					toggle: function(button, pressed) {
						if (pressed) button.setIconCls('rs-social-button rs-social-button-pressed icon-comment')
						else button.setIconCls('rs-social-button icon-comment-alt')
						button.up('panel').fireEvent('socialToggled', button, pressed)
					}
				}
			}, {
				xtype: 'button',
				itemId: 'editWikiButton',
				ui: 'clear-button-small-toolbar',
				iconCls: 'rs-social-button icon-external-link',
				handler: function(button) {
					button.up('panel').fireEvent('editWiki', button)
				}
			}, {
				xtype: 'button',
				ui: 'clear-button-small-toolbar',
				iconCls: 'x-btn-icon x-tbar-loading',
				handler: function(button) {
					button.up('panel').fireEvent('refreshWiki', button)
				}
			}],
			layout: {
				type: 'vbox',
				align: 'left'
			},
			items: [{
				xtype: 'combobox',
				width: '70%',
				name: 'comboAnswer',
				store: '@{comboAnswers}',
				queryMode: 'local',
				displayField: 'displayAnswer',
				valueField: 'value',
				hideLabel: true,
				editable: false,
				listeners : {
					change : '@{autoNavigateHandler}'
				}
			}, {
				xtype: 'radiogroup',
				hidden : '@{horizontalRGIsHidden}',
				items: '@{horizontalRadioAnswers}',			
				columns: 6, //Horizontal Layout
				width: '100%',				
				itemTemplate: {
					xtype: 'radio',
					name: 'question',
					inputValue: '@{value}',
					boxLabel: '@{displayAnswer}',
					checked: '@{checked}'					
				},
				listeners : {
					change : '@{autoNavigateHandler}',
					
				}
			}, {
				xtype: 'radiogroup',
				hidden :'@{verticalRGIsHidden}',			
				items: '@{verticalRadioAnswers}',			
				columns: 1, //Vertical Layout
				width: '100%',				
				itemTemplate: {
					xtype: 'radio',
					name: 'question',
					inputValue: '@{value}',
					boxLabel: '@{displayAnswer}',
					checked: '@{checked}',					
				},
				listeners : {
					change : '@{autoNavigateHandler}'					
				}			
			}],
			buttonAlign: 'left',
			buttons: {
				xtype: 'toolbar',
				hidden: '@{!showQuestionSubmit}',
				items: [{ name:'submit', cls: 'rs-proceed-button' }]
			},
			listeners: {
				afterrender: function(panel) {
					panel.down('#socialButton')[panel.showSocial ? 'show' : 'hide']()
					panel.down('#editWikiButton')[panel.editWikiButton ? 'show' : 'hide']()
					panel.getEl().on('mouseover', function(e, target) {
						panel.fireEvent('mouseover')
					}, this);
				},
				showFeedback: '@{feedback}',
				mouseover: '@{collapseIfNeeded}',
				socialToggled: '@{socialToggled}',
				refreshWiki: '@{refreshWiki}',
				editWiki: '@{editWiki}'
			}
		},{
			region: 'south',
			title: '@{currentQuestion}',			
			hidden: '@{southQuestionIsHidden}',
			itemId : 'southRegion',
			split: true,
			bodyPadding: '10px',
			//height: '@{height}',
			showSocial: '@{showSocial}',
			setShowSocial: function(value) {
				this.down('#socialButton')[value ? 'show' : 'hide']()
			},
			showEditWiki: '@{editWikiIsVisible}',
			setShowEditWiki: function(value) {
				this.down('#editWikiButton')[value ? 'show' : 'hide']()
			},
			socialPressed: '@{socialIsPressed}',
			setSocialPressed: function(value) {
				if (!(this.down('#socialButton').pressed && value))
					this.down('#socialButton').toggle();
			},
			feedbackCls: '@{feedbackCls}',
			setFeedbackCls: function(cls) {
				var btn = this.down('#feedBackButton');
				btn.setIconCls('rs-social-button ' + cls);
			},
			tools: [{
				xtype: 'button',
				itemId: 'feedBackButton',
				ui: 'clear-button-small-toolbar',
				iconCls: 'rs-social-button icon-flag-alt',
				handler: function(button) {
					button.up('panel').fireEvent('showFeedback', button, button)
				}
			}, {
				xtype: 'button',
				itemId: 'socialButton',
				ui: 'clear-button-small-toolbar',
				iconCls: 'rs-social-button icon-comment-alt',
				enableToggle: true,
				pressed: false,
				listeners: {
					toggle: function(button, pressed) {
						if (pressed) button.setIconCls('rs-social-button rs-social-button-pressed icon-comment')
						else button.setIconCls('rs-social-button icon-comment-alt')
						button.up('panel').fireEvent('socialToggled', button, pressed)
					}
				}
			}, {
				xtype: 'button',
				itemId: 'editWikiButton',
				ui: 'clear-button-small-toolbar',
				iconCls: 'rs-social-button icon-external-link',
				handler: function(button) {
					button.up('panel').fireEvent('editWiki', button)
				}
			}, {
				xtype: 'button',
				ui: 'clear-button-small-toolbar',
				iconCls: 'x-btn-icon x-tbar-loading',
				handler: function(button) {
					button.up('panel').fireEvent('refreshWiki', button)
				}
			}],
			layout: {
				type: 'vbox',
				align: 'left'
			},
			items: [{
				xtype: 'combobox',
				width: '70%',
				name: 'comboAnswer',
				store: '@{comboAnswers}',
				queryMode: 'local',
				displayField: 'displayAnswer',
				valueField: 'value',
				hideLabel: true,
				editable: false,
				listeners : {
					change : '@{autoNavigateHandler}'
				}
			}, {
				xtype: 'radiogroup',
				hidden : '@{horizontalRGIsHidden}',
				items: '@{horizontalRadioAnswers}',			
				columns: 6, //Horizontal Layout
				width: '100%',				
				itemTemplate: {
					xtype: 'radio',
					name: 'question',
					inputValue: '@{value}',
					boxLabel: '@{displayAnswer}',
					checked: '@{checked}'					
				},
				listeners : {
					change : '@{autoNavigateHandler}',
					
				}
			}, {
				xtype: 'radiogroup',
				hidden :'@{verticalRGIsHidden}',			
				items: '@{verticalRadioAnswers}',			
				columns: 1, //Vertical Layout
				width: '100%',				
				itemTemplate: {
					xtype: 'radio',
					name: 'question',
					inputValue: '@{value}',
					boxLabel: '@{displayAnswer}',
					checked: '@{checked}',					
				},
				listeners : {
					change : '@{autoNavigateHandler}'					
				}			
			}],
			buttonAlign: 'left',
			buttons: {
				xtype: 'toolbar',
				hidden: '@{!showQuestionSubmit}',
				items: [{ name:'submit', cls: 'rs-proceed-button' }]
			},
			listeners: {
				afterrender: function(panel) {
					panel.down('#socialButton')[panel.showSocial ? 'show' : 'hide']()
					panel.down('#editWikiButton')[panel.editWikiButton ? 'show' : 'hide']()
					panel.getEl().on('mouseover', function(e, target) {
						panel.fireEvent('mouseover')
					}, this);
				},
				showFeedback: '@{feedback}',
				mouseover: '@{collapseIfNeeded}',
				socialToggled: '@{socialToggled}',
				refreshWiki: '@{refreshWiki}',
				editWiki: '@{editWiki}'
			}
		}
	]
})

glu.defView('RS.wiki.DecisionTreeQuestion', {
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	cls : '@{containerCls}',	
	items: [{
		cls: 'decision-tree-qa qa-@{selectedCss}',
		items : [
		{
			html: '@{displayQuestion}'
		},{
			html: '@{displayAnswer}',
			hidden : '@{answerIsHidden}',
			margin : '8 0 0 0'
		}]
	},{
		xtype: 'displayfield',
		hidden: true,
		name: 'answer',
		listeners: {
			render: function() {
				var field = this.up().down('displayfield')
				var text = field.getValue();
				if (text)
					this.up().getEl().set({
						'data-qtip': text
					});
			},
			change: function() {
				var field = this.up().down('displayfield')
				var text = field.getValue();
				if (text)
					this.up().getEl().set({
						'data-qtip': text
					});
			}
		}
	}],
	listeners: {
		render: function(panel) {
			panel.getEl().on('click', function() {
				panel.fireEvent('select', panel)
			})
		},
		select: '@{select}'
	}
})
