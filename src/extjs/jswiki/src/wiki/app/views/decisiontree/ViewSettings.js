glu.defView('RS.decisiontree.ViewSettings', {
	title: '~~viewSettings~~',
	modal: true,
	padding: 15,
	width : 800,
	cls : 'rs-modal-popup',
	tbar: ['->',{
		xtype : 'button',
		name : 'restoreDefault',
		cls : 'rs-small-btn rs-btn-light'
	}],	
	defaults: {
		bodyPadding : '5 0',
		margin : '0 0 10 0',
		cls : 'decision-tree-view-setting'
	},
	items: [{
		//Question Text
		title : '~~questionAndAnswer~~',
		layout: {
			type: 'vbox', 
			align: 'stretch'
		},
		defaults : {
			margin : '4 0',
			defaults: {
				margin: '0 15 0 0',				
				editable: false
			},
		},
		items : [{
			layout: {
				type: 'hbox',
				align: 'stretch'
			},	
			items :[{
				xtype: 'combobox',
				labelWidth : 120,
				flex : 1,
				displayField: 'display',
				valueField: 'value',
				name: 'questionFont',
				store: '@{fonts}',
				queryMode: 'local'
			},{
				xtype: 'combobox',
				width : 175,	
				displayField: 'display',
				valueField: 'value',
				name: 'questionFontSize',
				store: '@{fontSizes}',
				queryMode: 'local'
			},{
				xtype: 'displayfield',
				fieldLabel: 'Font Color',
				cls : 'rs-displayfield-value'
			},{
				xtype: 'button',
				margin : 0,
				htmlEncode: false,
				text: '@{returnQuestionSwatch}',
				style: {
					backgroundColor: '#FFFFFF',
					display: 'flex',
					alignItems: 'center',
				},
				cls: 'dt-colorpicker',
				menu: {
					xtype: 'colormenu',
					handler: '@{updateQuestionFontColor}'
				}
			}]
		},{
			layout: {
				type: 'hbox',
				align: 'stretch'
			},	
			items: [{
				xtype: 'combobox',
				flex : 1,
				labelWidth : 120,
				displayField: 'display',
				valueField: 'value',
				name: 'answerFont',
				store: '@{fonts}',
				queryMode: 'local'
			}, {
				xtype: 'combobox',		
				displayField: 'display',
				valueField: 'value',
				name: 'answerFontSize',
				store: '@{fontSizes}',
				width : 175,
				queryMode: 'local'
			},{
				xtype: 'displayfield',
				fieldLabel: 'Font Color',
				cls : 'rs-displayfield-value'
			},{
				xtype: 'button',
				margin : 0,
				text: '@{returnAnswerSwatch}',
				htmlEncode: false,
				style: {
					backgroundColor: '#FFFFFF',
					display: 'flex',
					alignItems: 'center',
				},
				cls: 'dt-colorpicker',
				menu: {
					xtype: 'colormenu',
					handler: '@{updateAnswerFontColor}'
				}
			}]
		},{
			layout: {
				type: 'hbox',
				align: 'stretch'
			},		
			items :[{
				xtype: 'combobox',
				labelWidth : 120,
				flex : 1,
				displayField: 'display',
				valueField: 'value',
				name: 'recommendationFont',			
				store: '@{fonts}',
				queryMode: 'local'
			},{
				xtype: 'combobox',
				width : 175,
				displayField: 'display',
				valueField: 'value',
				name: 'recommendationFontSize',
				store: '@{fontSizes}',
				queryMode: 'local'
			},{
				xtype: 'displayfield',
				fieldLabel: 'Font Color',
				cls : 'rs-displayfield-value'
			},{
				xtype: 'button',
				margin : 0,
				text: '@{returnRecommendationSwatch}',
				cls: 'dt-colorpicker',	
				htmlEncode: false,
				style: {
					backgroundColor: '#FFFFFF',
					display: 'flex',
					alignItems: 'center',
				},			
				menu: {
					xtype: 'colormenu',
					handler: '@{updateRecommendationFontColor}'
				}
			}]
		}]
	},{
		//Confirmation
		title : '~~confirmationButton~~',	
		layout:{
			type: 'vbox',
	 		align: 'stretch'
	 	},
	 	defaults : {
			margin : '4 0',
			defaults: {
				margin: '0 15 0 0',				
				editable: false
			},
		},
	 	items : [{		 	
	 		layout:{
				type: 'hbox',
		 		align: 'stretch'
		 	},		 	
	 		items: [{
				xtype: 'combobox',
				flex : 1,
				labelWidth : 120,
				displayField: 'display',
				valueField: 'value',
				name: 'confirmationFont',
				store: '@{fonts}',
				queryMode: 'local'
			}, {
				xtype: 'combobox',		
				displayField: 'display',
				valueField: 'value',
				name: 'confirmationFontSize',
				store: '@{fontSizes}',
				width : 175,
				queryMode: 'local'
			},{
				xtype: 'displayfield',
				fieldLabel: 'Font Color',
				cls : 'rs-displayfield-value'
			}, {
				xtype: 'button',
				margin : 0,
				htmlEncode: false,
				text: '@{returnConfirmationSwatch}',
				style: {
					backgroundColor: '#FFFFFF',
					display: 'flex',
					alignItems: 'center',
				},
				cls: 'dt-colorpicker',
				menu: {
					xtype: 'colormenu',
					handler: '@{updateConfirmationFontColor}'
				}
			}]
	 	},{
			xtype: 'checkboxfield',		
			name: 'autoConfirm',
			hideLabel : true,
			boxLabel : '~~autoConfirm~~',
			margin : '0 0 0 125',			
		},{			
			hidden : '@{autoConfirm}',			
			layout: {
				type: 'hbox',
				align: 'stretch'
			},		
			items: [{
				xtype: 'textfield',
				flex: 1,
				labelWidth : 120,
				name: 'buttonText',
				fieldLabel: 'Button Text',
				allowBlank: 'false'
			}, {
				xtype: 'displayfield',
				fieldLabel: 'Background',
				cls : 'rs-displayfield-value'
			}, {
				xtype: 'button',
				margin : 0,
				htmlEncode: false,
				text: '@{returnConfirmationBGSwatch}',
				style: {
					backgroundColor: '#FFF',
					display: 'flex',
					alignItems: 'center',
				},
				cls: 'dt-colorpicker',
				menu: {
					xtype: 'colormenu',
					handler: '@{updateConfirmationBGColor}'
				}
			}]
		}]		
	},{
		//Navigation
		title : '~~navigation~~',	
		layout:{
			type: 'vbox',
	 		align: 'stretch'
	 	},
	 	defaults : {
			margin : '4 0',
			defaults: {
				margin: '0 15 0 0',
				labelWidth : 110,
				editable: false
			},
		},
	 	items : [{	 		 		
	 		flex : 1,
	 		layout:{
				type: 'hbox',
		 		align: 'stretch'
		 	},		 	
	 		items: [{
				xtype: 'displayfield',
				fieldLabel: 'Background',
				labelWidth : 105,			
			}, {
				xtype: 'button',			
				text: '@{returnNavigationBGSwatch}',
				htmlEncode: false,
				style: {
					backgroundColor: '#FFFFFF',
					display: 'flex',
					alignItems: 'center',
				},
				cls: 'dt-colorpicker',
				menu: {
					xtype: 'colormenu',
					handler: '@{updateNavigationBGColor}'
				}
			}]
	 	},{
	 		xtype : 'checkboxfield',
	 		name: 'isAutoAnswerDisabled',
	 		hideLabel: true,
	 		boxLabel : '~~isAutoAnswerDisabled~~',
	 		margin : '0 0 0 125'		
	 	}]		
	},{
		//Layout
		title : '~~layout~~',	
		layout:{
			type: 'vbox',
	 		align: 'stretch',
	 	},
	 	defaults : {
			margin : '4 0',
			labelWidth : 120,
		},
		items: [{
			flex: 1,
			xtype: 'radiogroup',
			fieldLabel: '~~navPos~~',
			columns: 2,
			items: [
				{
					xtype: 'radio',
					boxLabel: '~~navOnLeft~~',
					checked : '@{navOnLeft}'
				}, {
					xtype: 'radio',
					boxLabel: '~~navOnRight~~',
					checked : '@{!navOnLeft}'
				}
			]
		},{
			flex: 1,
			xtype : 'radiogroup',		
			fieldLabel : '~~questionBoxPos~~',
			columns : 2,
			items : [{
				xtype : 'radio',
				boxLabel : '~~questionOnTop~~',				
				checked : '@{questionOnTop}'
			},{
				xtype : 'radio',
				boxLabel : '~~questionOnBottom~~',
				checked : '@{!questionOnTop}'			
			}]
		},{
			flex: 1,
			xtype : 'radiogroup',		
			fieldLabel : '~~verticalAnswerLayout~~',
			columns : 2,
			items : [{
				xtype : 'radio',
				boxLabel : '~~vertical~~',				
				checked : '@{verticalAnswerLayout}'
			},{
				xtype : 'radio',
				boxLabel : '~~horizontal~~',
				checked : '@{!verticalAnswerLayout}'			
			}]
		},{
			xtype : 'component',
			margin : 0,
			html : '~~verticalAnswerLayoutHint~~'
		}]
	}],
	buttons: [{
		name: 'confirm',
		cls : 'rs-med-btn rs-btn-dark'		
	},{
		name: 'close',
		cls : 'rs-med-btn rs-btn-light'		
	}]
});


