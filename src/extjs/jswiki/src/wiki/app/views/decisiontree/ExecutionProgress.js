glu.defView('RS.decisiontree.ExecutionProgress',{
	title : '~~executingTask~~',
	width : 500,
	height : 200,
	padding : 15,
	modal : true,
	closable: false,
	cls : 'rs-modal-popup',
	items : [{
		html : '@{progressText}',
		margin : {
			bottom : 5
		}
	},{
		xtype : 'progressbar',
		itemId : 'progressBarId'
	},{
		xtype : 'displayfield',
		value : '@{progress}',
		hidden : true,
		listeners: {
            change: function(field, newData) {
                var progressBar = this.ownerCt.down('#progressBarId');
                progressBar.updateProgress(newData, parseInt(newData * 100) + '%');
            }
        }
	}],
	//buttonAlign : 'center',
	buttons : [{
		name : 'abort',
		cls : 'rs-med-btn rs-btn-dark'
	}] 
})