Ext.define('RS.decisiontree.DecisiontreeMain', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.decisiontreemain',
	config: {
		masked:  false
	},
	setMasked: function(value) {
		if (!this.loadingMask) {
			this.loadingMask = new Ext.LoadMask({
				msg: 'Loading content please wait...',
				target: this
			});
		}
		value ? this.loadingMask.show(): this.loadingMask.hide();
	}
});
glu.defView('RS.decisiontree.Main',{
	xtype: 'decisiontreemain',
	masked: '@{masked}',
	itemId: 'dtView',
	cls: 'rs-wiki-main',
	padding : '15 5 15 1',
	style : 'visibility:@{componentVisibility}',
	dockedItems : [{
		xtype: 'toolbar',
		cls : 'rs-dockedtoolbar dt-dockedtoolbar',
		margin : '0 10 15 0',
		items: [{
				xtype: 'tbtext',			
				cls: 'rs-display-name dt-fullname-title',
				hidden: '@?{..sirContext}',
				text: '@{dtName}'
			},{
				xtype : 'checkbox',
				cls: 'decision-tree-checkbox',
				hideLabel : true,
				name : 'isAutoAnswerDisabled',
				boxLabel : '~~isAutoAnswerDisabled~~',
				boxLabelCls : 'decision-tree-checkbox-label x-form-cb-label'
			},'->',{
				name : 'restartDecisionTree',
				hidden: '@?{..sirContext}',
				cls: 'dt-restart-btn',
				width: 67,
				height: 18
			},{
				name: 'editDecisionTree',
				hidden: '@?{..sirContext}',
				cls: 'dt-edit-btn',
				width: 48,
				height: 18,
				margin: '0 25 0 0'
			},{
				name : 'addFeedback',
				hidden: '@?{..sirContext}',
				text : '',
				iconCls: 'rs-social-button icon-flag-alt',
				tooltip : '~~addFeedback~~'
			},{
				name : 'viewCurrentPage',
				hidden: '@?{..sirContext}',
				text : '',
				iconCls: 'rs-social-button icon-external-link',
				tooltip : '~~viewCurrentPage~~'
			},{
				name : 'refreshCurrentPage',
				hidden: '@?{..sirContext}',
				text : '',
				iconCls: 'rs-social-button icon-refresh',
				tooltip : '~~refreshCurrentPage~~'
		}]
	}],
	layout : {
		type: 'border',
		regionWeights: {
			west: 40,
			north: 20,
			south: 10,
			east: 0
		}
	},
	items : [{
		xtype : '@{navigationPanel}',
	},{
		xtype : '@{contentPage}',
	}]
});
