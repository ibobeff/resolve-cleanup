glu.defView('RS.wiki.PageInfo', {	
	height: 500,
	width: 600,
	modal: true,
	title: '@{title}',
	padding : 15,
	autoScroll: true,
	cls : 'rs-modal-popup',	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {		
		margin : '4 0'
	},
	items: [{
		xtype: 'panel',
		//title: '~~wikiDoc~~',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaultType: 'displayfield',
		defaults: {
			labelWidth: 130
		},
		items: ['fullName', 'id',
			'sysCreatedBy', 'createdOn',
			'sysUpdatedBy', 'updatedOn',
			'expirationDate', 'version', 'viewed',
			'edited', 'executed', 'lastReviewedBy', 'reviewedOn', 'rating', {
				name: 'tagsDisplayed',
				fieldStyle: 'overflow-wrap: break-word;'
			}, {
				hideLabel: true,
				name: 'moreOrLess',
				style: 'color:#999;cursor:pointer;width: 100!important',
				fieldStyle: 'color:#999;cursor:pointer;',
				padding: '0 0 0 125',
				listeners: {
					render: function() {
						var me = this;
						this.getEl().on('click', function() {
							me.fireEvent('moreOrLess');
						});
					},
					moreOrLess: '@{displayMoreOrLess}'
				}
			}, 'emptySummary', {				
				value: '@{summary}',
				htmlEncode : true,
				cls : 'rs-displayfield-value'
			}
		]
	}, {
		xtype: 'panel',
		title: '~~outRefLink~~',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'fieldcontainer',
			layout: 'vbox',
			items: '@{outgoings}'
		}]
	}, {
		xtype: 'panel',
		title: '~~inRefLink~~',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'fieldcontainer',
			layout: 'vbox',
			items: '@{incomings}'
		}]
	}, {
		xtype: 'panel',
		title: '~~runbookDependancies~~',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'grid',
			title: '~~actionTask~~',
			cls : 'rs-grid-dark',
			name: 'actiontask',
			columns: [{
				header: '~~type~~',
				dataIndex: 'type'
			}, {
				header: '~~name~~',
				dataIndex: 'name',
				flex: 1
			}],
			store: '@{actionTaskStore}'
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			title: '~~runbook~~',
			name: 'runbook',
			columns: [{
				header: '~~type~~',
				dataIndex: 'type'
			}, {
				header: '~~name~~',
				dataIndex: 'name',
				flex: 1
			}],
			store: '@{runbookStore}'
		}]
	}],	
	buttons: [{
        text : '~~close~~',
        cls : 'rs-med-btn rs-btn-dark',
        handler : '@{close}',
    }]
});

glu.defView('RS.wiki.PageInfoRefLink', {
	xtype: 'component',
	autoEl: '@{link}',
	margins: {
		left: 5
	}
});