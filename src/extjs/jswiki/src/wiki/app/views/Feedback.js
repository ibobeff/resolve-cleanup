glu.defView('RS.wiki.Feedback', {	
	target: '@{target}',
	width: 600,
	minHeight: 250,
	cls : 'rs-modal-popup',
	modal : true,
	padding : 15,
	title: '@{title}',
/*	baseCls: Ext.isIE8m ? 'x-window' : 'x-panel',
	ui: Ext.isIE8m ? 'default' : 'sysinfo-dialog',	*/
	layout: {
		type: 'vbox',
		align: 'stretch'
	},	
	defaults: {
		labelWidth: 120,
		margin : '4 0'
	},
	items: [{
		xtype: 'usefulfield',
		name: 'markForReview'
	}, {
		xtype: 'usefulfield',
		name: 'useful'
	}, {
		xtype: 'ratingfield',
		name: 'rating'
	}, {
		xtype: 'checkbox',
		boxLabel: '~~includeWorksheetLink~~',
		name: 'includeWorksheetLink',
		hideLabel: true
	}, {
		xtype: 'textarea',
		name: 'comment',
		hidden: '@{!ratingIsVisible}',
		flex: 1
	}, {
		xtype: 'textarea',
		hidden: '@{ratingIsVisible}',
		value: '@{comment}',
		fieldLabel: '~~description~~',
		flex: 1
	}],	
	buttons: [{
		name :'submitFeedback',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'requestReview',
		cls : 'rs-med-btn rs-btn-dark',		
	},{
		name : 'close',
		cls : 'rs-med-btn rs-btn-light',		
	}]
})
