glu.defView('RS.wiki.ResultViewer', {
	bodyPadding: '10px',

	layout: 'fit',

	items: [{
		autoScroll: true,
		html: '@{html}',
	}],

	asWindow: {
		title: '~~result~~',
		padding : 15,
		cls : 'rs-modal-popup',		
		modal: true,
		buttons: [{
			name : 'cancel',
			text : '~~close~~',
			cls : 'rs-med-btn rs-btn-dark'
		}],
		listeners: {
			beforeshow: function(win) {
				win.setWidth(Math.round(Ext.getBody().getWidth() * 0.8));
				win.setHeight(Math.round(Ext.getBody().getHeight() * 0.8));
			}        
		}
	}
})
