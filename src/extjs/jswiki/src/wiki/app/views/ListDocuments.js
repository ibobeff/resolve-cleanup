glu.defView('RS.wiki.ListDocuments', {
	layout: 'border',
	modal: true,
	title: '~~listDocumentsTitle~~',
	cls : 'rs-modal-popup',	
	padding: 15,
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true,
		useWindowParams: false
	}, {
		ptype: 'pager'
	}],
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar',
		name: 'actionBar',
		items: []
	}],
	defaults : {
		margin : '0 0 10 0'
	},
	items: [{
		region: 'west',
		width: 200,
		split: true,
		maxWidth: 300,
		tbar: [],
		xtype: 'treepanel',
		cls : 'rs-grid-dark',
		columns: '@{namespaceColumns}',
		name: 'documentTree',
		listeners: {
			selectionchange: '@{menuPathTreeSelectionchange}'
		}

	}, {
		region: 'center',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		displayName: '~~title~~',
		name: 'documentGrid',
		columns: '@{documentCols}',
		multiSelect: false,
		selected: '@{documentSelected}',
		autoScroll: true,
		viewConfig: {
			enableTextSelection: true
		},	
		plugins: [{
			ptype: 'resolveexpander',
			pluginId: 'rowExpander',
			expanderIndex: 0,
			rowBodyTpl: new Ext.XTemplate('<div resolveId="resolveRowBody" style="color:#888"><span style="font-weight:bold">Summary:</span><br/><br/>&nbsp&nbsp&nbsp&nbsp{usummary:htmlEncode}</div>')
		}],
		listeners: {
			render: function(grid) {
				var plugin = grid.getPlugin('rowExpander')
				if (plugin)
					grid.on('cellclick', function(view, td, cellIndex, record, tr, rowIndex, e, eOpts) {
						if (cellIndex > 0) {
							plugin.onDblClick(view, record, null, rowIndex, e)
						}
					})
			}
		}
	}],
	listeners: {
		beforeshow: function() {
			this.setWidth(Math.round(Ext.getBody().getWidth() * 0.8));
			this.setHeight(Math.round(Ext.getBody().getHeight() * 0.8));
		}
	},
	buttons: [{
		name : 'go', 
		cls :'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})