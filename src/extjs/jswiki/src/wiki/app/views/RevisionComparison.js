glu.defView('RS.wiki.RevisionComparison', {	
	modal: true,
	padding : 15,
	cls :'rs-modal-popup',
	title: '~~revisionComparison~~',
	xtype: 'panel',
	dockedItems: [{
		xtype: 'toolbar',
		items: ['->', 'contentTab', 'processTab', 'exceptionTab', 'decisionTreeTab']
	}],
	layout: 'card',
	activeItem: '@{activeItem}',	
	items: [{
			itemId: 'content',
			autoScroll: true,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			bodyPadding: '10px',
			flex: 1,
			title1: '@{version1Title}',
			title2: '@{version2Title}',
			content1: '@{content1}',
			content2: '@{content2}',
			setContent1: function(c) {
				this.content1 = c;
				this.compare();
			},
			setContent2: function(c) {
				this.content2 = c;
				this.compare();
			},
			compare: function() {
				var base = difflib.stringAsLines(this.content1 || '');
				var newtxt = difflib.stringAsLines(this.content2 || '');
				var sm = new difflib.SequenceMatcher(base, newtxt);
				var opcodes = sm.get_opcodes();
				var diffoutputdiv = this.body.dom;
				while (diffoutputdiv.firstChild) diffoutputdiv.removeChild(diffoutputdiv.firstChild);
				var view = diffview.buildView({
					baseTextLines: base,
					newTextLines: newtxt,
					opcodes: opcodes,
					baseTextName: this.title1,
					newTextName: this.title2,
					viewType: 0
				});
				diffoutputdiv.appendChild(view);
				// if (this.up().getLayout().getActiveItem().itemId == this.itemId)
				// 	this.up().up().setHeight(Ext.fly(view).getHeight() > 400 ? 500 : Ext.fly(view).getHeight() + 130);
			}
		}, {
			itemId: 'abortModel',
			autoScroll: true,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			bodyPadding: '10px',
			flex: 1,
			title1: '@{version1Title}',
			title2: '@{version2Title}',
			content1: '@{abortModel1}',
			content2: '@{abortModel2}',
			setContent1: function(c) {
				this.content1 = c;
				this.compare();
			},
			setContent2: function(c) {
				this.content2 = c;
				this.compare();
			},
			compare: function() {
				var base = difflib.stringAsLines(this.content1 || '');
				var newtxt = difflib.stringAsLines(this.content2 || '');
				var sm = new difflib.SequenceMatcher(base, newtxt);
				var opcodes = sm.get_opcodes();
				var diffoutputdiv = this.body.dom;
				while (diffoutputdiv.firstChild) diffoutputdiv.removeChild(diffoutputdiv.firstChild);
				var view = diffview.buildView({
					baseTextLines: base,
					newTextLines: newtxt,
					opcodes: opcodes,
					baseTextName: this.title1,
					newTextName: this.title2,
					viewType: 0
				});
				diffoutputdiv.appendChild(view);
				// if (this.up().getLayout().getActiveItem().itemId == this.itemId)
				// 	this.up().up().setHeight(Ext.fly(view).getHeight() > 400 ? 500 : Ext.fly(view).getHeight() + 130);
			}
		}, {
			itemId: 'mainModel',
			autoScroll: true,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			bodyPadding: '10px',
			flex: 1,
			title1: '@{version1Title}',
			title2: '@{version2Title}',
			content1: '@{mainModel1}',
			content2: '@{mainModel2}',
			setContent1: function(c) {
				this.content1 = c;
				this.compare();
			},
			setContent2: function(c) {
				this.content2 = c;
				this.compare();
			},
			compare: function() {
				var base = difflib.stringAsLines(this.content1 || '');
				var newtxt = difflib.stringAsLines(this.content2 || '');
				var sm = new difflib.SequenceMatcher(base, newtxt);
				var opcodes = sm.get_opcodes();
				var diffoutputdiv = this.body.dom;
				while (diffoutputdiv.firstChild) diffoutputdiv.removeChild(diffoutputdiv.firstChild);
				var view = diffview.buildView({
					baseTextLines: base,
					newTextLines: newtxt,
					opcodes: opcodes,
					baseTextName: this.title1,
					newTextName: this.title2,
					viewType: 0
				});
				diffoutputdiv.appendChild(view);
				// if (this.up().getLayout().getActiveItem().itemId == this.itemId)
				// 	this.up().up().setHeight(Ext.fly(view).getHeight() > 400 ? 500 : Ext.fly(view).getHeight() + 130);
			}
		}, {
			itemId: 'decisionTreeModel',
			autoScroll: true,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			bodyPadding: '10px',
			flex: 1,
			title1: '@{version1Title}',
			title2: '@{version2Title}',
			content1: '@{decisionTreeModel1}',
			content2: '@{decisionTreeModel2}',
			setContent1: function(c) {
				this.content1 = c;
				this.compare();
			},
			setContent2: function(c) {
				this.content2 = c;
				this.compare();
			},
			compare: function() {
				var base = difflib.stringAsLines(this.content1 || '');
				var newtxt = difflib.stringAsLines(this.content2 || '');
				var sm = new difflib.SequenceMatcher(base, newtxt);
				var opcodes = sm.get_opcodes();
				var diffoutputdiv = this.body.dom;
				while (diffoutputdiv.firstChild) diffoutputdiv.removeChild(diffoutputdiv.firstChild);
				var view = diffview.buildView({
					baseTextLines: base,
					newTextLines: newtxt,
					opcodes: opcodes,
					baseTextName: this.title1,
					newTextName: this.title2,
					viewType: 0
				});
				diffoutputdiv.appendChild(view);
				// if (this.up().getLayout().getActiveItem().itemId == this.itemId)
				// 	this.up().up().setHeight(Ext.fly(view).getHeight() > 400 ? 500 : Ext.fly(view).getHeight() + 130);
			}
		}
	],	
	listeners: {
		beforeshow: function() {
			this.setWidth(Math.round(Ext.getBody().getWidth() * 0.8));
			this.setHeight(Math.round(Ext.getBody().getHeight() * 0.8));
		}
	},
	buttons: [{
        text : '~~close~~',
        cls : 'rs-med-btn rs-btn-dark',
        handler : '@{close}',
    }]
});