glu.defView('RS.wiki.AccessRights',{
	title:'~~roles~~',
	cls : 'rs-modal-popup',
	padding : 15,
	width: 625,
	height: 400,
	modal:true,
	layout:{
		type:'vbox',
		align:'stretch'
	},	
	items:[{
		flex:1,
		xtype:'@{accessRights}'
	}],	
	buttons:[{
		name : 'setRights',
		cls : 'rs-btn-dark rs-med-btn'
	},{
		name : 'close',
		cls : 'rs-btn-light rs-med-btn'
	}]
});