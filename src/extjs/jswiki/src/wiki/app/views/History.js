glu.defView('RS.wiki.History', {	
	padding: 15,
	width: 600,
	height: 500,	
	modal: true,
	overflowY : 'scroll',
	cls : 'rs-modal-popup',
	title: '~~historyTitle~~',
	dockedItems: [{
		xtype: 'toolbar',	
		items: [{
			xtype: 'triggerfield',
			triggerCls: 'x-form-search-trigger',
			name: 'keyword',
			flex : 1,
			hideLabel: false,
			emptyText: '~~emptySearch~~',		
			padding: '0px 0px 10px 0px',
			listeners: {
				specialKey: function(field, e) {
					if (e.getKey() == e.ENTER) field.fireEvent('searchNow', field)
				},
				searchNow: '@{searchNow}'
			},
			onTriggerClick: function() {
				this.fireEvent('searchNow', this, this);
			}
		}]
	}],	
	items: '@{list}',
	listeners: {
		historyrendererselectionchange: function(render, id) {
			this.fireEvent('selectionChange', render, id);
		},
		selectionChange: '@{selectionChange}'
	},
	buttons: [{
		name : 'go',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});

glu.defView('RS.wiki.HistoryRenderer', {
	xtype: 'panel',
	bodyPadding : '5 0',
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	overCls: 'render-hover',
	cls: 'render-body',
	items: [{
		xtype: 'fieldcontainer',
		layout: 'vbox',
		flex: 1,
		items: [{
			xtype: 'tbtext',
			style: 'color: #0088cc;font-weight: bold;font-size: 14px',
			text: '@{title}',
			margin : '0 0 5 5'		
		}, {
			xtype: 'tbtext',
			style: 'color: #999 !important',
			text: '@{fullName}'
		}]
	}, {
		xtype: 'displayfield',
		hideLabel: true,	
		name: 'lastViewedDate'
	}],
	recordId: '@{fullName}',
	listeners: {
		render: function(c) {
			var me = this;
			c.body.on('click', function() {
				me.selected = !me.selected;
				me[me.selected ? 'addCls' : 'removeCls']('render-selected');
				me.ownerCt.fireEvent('historyrendererselectionchange', me, me.recordId);
			});
			c.body.on('dblclick', function() {
				me.fireEvent('dblclick', this, this);
			});
		},
		dblclick: '@{gotoWiki}'
	}
});