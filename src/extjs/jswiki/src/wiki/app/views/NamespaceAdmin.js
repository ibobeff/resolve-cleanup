glu.defView('RS.wiki.NamespaceAdmin', {
	padding: 15,
	xtype: 'grid',
	cls : 'rs-grid-dark',
	layout: 'fit',
	stateId: '@{stateId}',
	stateful: true,
	displayName: '@{displayName}',
	name: 'records',
	store: '@{store}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			xtype: 'button',
			text: '~~file~~',
			iconCls: 'rs-icon-button icon-file-text',
			menu: [
				'renameWikis',
				'copyWikis',
				'deleteWikis',
				'undeleteWikis',
				'purgeWikis',
				'commit'
			]
		}, {
			xtype: 'button',
			text: '~~status~~',
			menu: [
				'activateWikis',
				'deactivateWikis',
				'lockWikis',
				'unlockWikis',
				'hideWikis',
				'unhideWikis'
			]
		}, {
			xtype: 'button',
			text: '~~role~~',
			menu: [{
				xtype: 'menuitem',
				name: 'configRoles',
				tooltip: '@{configRolesTip}'
			}, {
				xtype: 'menuitem',
				name: 'configDefaultRoles',
				tooltip: '@{configDefaultRolesTip}'
			}]
		}, {
			xtype: 'button',
			text: '~~index~~',
			menu: [
				'indexWikis',
				'purgeIndex',
				'indexAllWikis',
				'purgeAllIndexes'
			]
		}, {
			xtype: 'button',
			text: '~~more~~',
			menu: [
				'configTags',
				'rateWikis',
				'setReviewed',
				'setExpiryDate',
				'resetStats'
			]
		}]
	}],
	columns: '@{columns}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],

	selModel: {
		selType: 'resolvecheckboxmodel',		
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},

	listeners: {
		editAction: '@{editRecord}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}

});