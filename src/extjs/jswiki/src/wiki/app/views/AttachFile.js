/*
glu.defView('RS.wiki.AttachFile', {
	asWindow: {
		width: 600,
		height: 400,
		title: '~~attachFileWindowTitle~~',
		cls : 'rs-modal-popup',
		modal: true,
		padding : 15,
		listeners: {
			beforerender : function(){				
				var toolbar = this.down('button[name="uploadFile"]').ownerCt;
				toolbar.addCls('rs-dockedtoolbar');
				var toolbarItems = toolbar.items;			
				Ext.Array.each(toolbarItems.items, function(btn){					
					btn.addCls('rs-btn-light rs-small-btn');
				})
			}			
		}
	},
	layout: 'fit',
	cls : 'rs-grid-dark',
	items: [{
			xtype: 'uploadmanager',
			resizable: false,
			resizeHandles: '',
			documentName: 'System.Attachment',
			allowUpload: true,
			allowRemove: true,
			allowDownload: true,
			recordId: 'System.Attachment',
			nameColumn: 'fileName',
			uploader: {
				url: '/resolve/service/wiki/attachment/upload',
				autoStart: true,
				max_file_size: '2020mb',
				flash_swf_url: '/resolve/js/plupload/Moxie.swf',
				urlstream_upload: true
			}
		}
		// {
		// 	xtype: 'imagepicker',
		// 	allowUpload: true,
		// 	resizable: false,
		// 	fileServiceUploadUrl: '/resolve/service/wiki/attachment/upload',
		// 	uploader: {
		// 		uploadpath: 'dev',
		// 		autoStart: true,
		// 		max_file_size: '2020mb',
		// 		flash_swf_url: '/resolve/js/plupload/plupload.flash.swf',
		// 		urlstream_upload: true,
		// 		multipart_params: {
		// 			docFullName: 'System.Attachment'
		// 		}
		// 	},
		// 	listeners: {
		// 		selectionchange: '@{selectionChanged}',
		// 		itemdblclick: '@{selectRecord}',
		// 		uploadcomplete: function(uploader, files) {
		// 			if (files.length > 0) this.getSelectionModel().select(this.getStore().getAt(this.getStore().find('id', files[files.length - 1].id)))
		// 		}
		// 	}
		// }
	],
	buttons: [{
		name : 'close',
		cls : 'rs-med-btn rs-btn-dark'
	}]
})
*/