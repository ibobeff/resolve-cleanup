glu.defView('RS.wiki.SourceHelp', {
	asWindow: {
		height: 500,
		width: 680,
		title: '@{windowTitle}',
		tools: [{
			xtype: 'button',
			iconCls: 'icon-external-link',
			ui: 'message-box-toolbar-button-small',
			cls: 'wiki-external-link',
			handler: function(button) {
				button.up('window').fireEvent('externalClick', button)
			}
		}],
		listeners: {
			externalClick: '@{externalClick}'
		}
	},
	layout: 'card',
	activeItem: '@{activeItem}',
	buttonAlign: 'left',
	buttons: ['back', 'close'],
	items: [{
		xtype: 'grid',
		name: 'sourceHelp',
		columns: '@{sourceHelpColumns}',
		tbar: [{
			xtype: 'triggerfield',
			name: 'search',
			triggerCls: 'x-form-search-trigger',
			hideLabel: true,
			flex: 1,
			emptyText: '~~search~~',
			listeners: {
				specialKey: function(field, e) {
					if (e.getKey() == e.ENTER) {
						field.fireEvent('enterPressed', field)
						return
					}
					if (e.getKey() == e.UP) {
						var grid = field.up('grid'),
							selectionModel = grid.getSelectionModel(),
							selection = selectionModel.getSelection();
						if (selection.length > 0) selectionModel.selectPrevious()
						else {
							if (grid.store.getCount() > 0)
								selectionModel.select(grid.store.getAt(0))
						}
						field.focus()
					}
					if (e.getKey() == e.DOWN) {
						var grid = field.up('grid'),
							selectionModel = grid.getSelectionModel(),
							selection = selectionModel.getSelection();
						if (selection.length > 0) selectionModel.selectNext()
						else {
							if (grid.store.getCount() > 0)
								selectionModel.select(grid.store.getAt(0))
						}
					}
				},
				enterPressed: '@{enterPressed}'
			}
		}],
		listeners: {
			itemclick: '@{itemClick}'
		},
		viewConfig: {
			listeners: {
				itemkeydown: function(view, record, item, index, e, eOpts) {
					if (e.getKey() == e.ENTER) view.ownerCt.fireEvent('itemclick', view.ownerCt)
				}
			}
		}
	}, {
		html: '@{detailDisplay}'
	}]
})