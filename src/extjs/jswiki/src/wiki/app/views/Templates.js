glu.defView('RS.wiki.Templates', {
	xtype: 'grid',
	layout: 'fit',
	padding: 15,
	cls : 'rs-grid-dark',
	displayName: '@{displayName}',
	stateId: '@{stateId}',
	name: 'templates',
	store: '@{store}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		name: 'actionBar',
		items: ['createTemplate', 'deleteTemplates', 'rename', 'copy']
	}],
	columns: '@{columns}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},

	listeners: {
		editAction: '@{editTemplate}'
	}
});