glu.defView('RS.wiki.ActionTaskWizard', {
	asWindow: {
		height: 400,
		width: 700,
		title: '~~actionTaskWizardTitle~~',
		modal: true
	},

	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'actionTasks',
		columns: '@{actionTasksColumns}',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: [{
				xtype: 'textfield',
				name: 'searchText',
				hideLabel: true,
				width: 250,
				emptyText: '~~searchActionTasks~~'
			}]
		}],
		plugins: [{
			ptype: 'pager'
		}]
	}],

	buttonAlign: 'left',
	buttons: ['create', 'cancel']
})