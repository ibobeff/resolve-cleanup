glu.defView('RS.wiki.TagConfiguration', {
	title: '~~title~~',
	width: 625,
	height: 400,

	layout: {
		type:'vbox',
		align:'stretch'
	},

	items: [{
		flex:1,
		xtype: 'grid',
		plugins: [{
			ptype: 'pager',
			allowAutoRefresh: false
		}],
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: [{
				xtype: 'textfield',
				name: 'searchTag'
			}]
		}],
		name: 'tags',
		store: '@{tagStore}',
		columns: '@{tagColumns}'
	}],
	buttonAlign: 'left',
	buttons: ['add', 'remove', 'cancel'],
	listeners: {
		resize: function() {
			this.center()
		}
	}
})