glu.defView('RS.wiki.Main', {
	layout: 'border',
	cls: 'rs-wiki-main',
	padding : '15 0',
	dockedItems: [{
		xtype: 'toolbar',
		hidden: '@{!control}',		
		ui: 'display-toolbar',
		cls : 'rs-dockedtoolbar',
		margin : '0 0 10 0',
		padding : '0 15',
		defaultButtonUI: 'display-toolbar-button',		
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{wikiTitle}'
		}, 
		/* TODO - support Version Control
		{
			xtype: 'tbtext',
			text: '@{versionText}',
		},
		*/
		{
			xtype: 'tbtext',
			text: '~~readOnlyTxt~~',
			hidden: '@{!readOnlyTextIsVisible}'
		},
		'->', 
		{
			//Workaround for height issue when switching between mode.
			name: 'editPage',
			style: 'visibility: hidden'
		},
		{
			name: 'pageTab'
		},
		{
			name: 'automationTab',
			hidden: '@{viewTabIsPressed}'
		},
		{
			name: 'decisionTreeTab',
			hidden: '@{viewTabIsPressed}'
		},
		{
			name: 'pageViewTab',
			hidden: '@{!viewTabIsPressed}'
		},
		{
			name: 'automationViewTab',
			hidden: '@{!viewTabIsPressed}'
		},
		{
			name: 'decisionTreeViewTab',
			hidden: '@{!viewTabIsPressed}'
		}]
	}, {
		xtype: 'toolbar',
		hidden: '@{!control}',	
		cls: 'actionBar rs-dockedtoolbar',
		padding : '0 15',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [
		{	
			iconCls: 'rs-icon-button icon-file-text',
			name: 'viewFileOptions',
			menu: [
			{
				name: 'editPage',
				disabled: '@{!isEditable}',
				width : 200
			},
			{
				xtype: 'menuseparator',
				hidden: '@{autoViewOrDtreeViewIsPressed}'
			},
			{
				name: 'copy',
				disabled: '@{!copyIsDisabled}',
				hidden: '@{autoViewOrDtreeViewIsPressed}'
			},
			{
				name: 'move',
				disabled: '@{!moveIsDisabled}',
				hidden: '@{autoViewOrDtreeViewIsPressed}'
			},
			{
				xtype: 'menuseparator',
				hidden: '@{autoViewOrDtreeViewIsPressed}'
			},

			{
				name: 'deleteWiki',
				disabled: '@{!deleteWikiIsDisabled}',
			},
			{
				name: 'undeleteWiki',
				disabled: '@{!undeleteWikiIsDisabled}',
			},
			{
				name: 'purge',
				disabled: '@{!purgeIsDisabled}',
				hidden: '@{autoViewOrDtreeViewIsPressed}'
			},
			{
				xtype: 'menuseparator',
				hidden: '@{autoViewOrDtreeViewIsPressed}'
			},
			{
				name: 'lock',
				disabled: '@{!lockIsDisabled}',
			},
			{
				iconCls: 'rs-social-menu-button icon-check',
				name: 'unlock',
				text: '~~lock~~',
				disabled: '@{!unlockIsDisabled}',
			},
			{
				xtype: 'menuseparator',
				hidden: '@{autoViewOrDtreeViewIsPressed}'
			},
			{
				iconCls: '@{tableOfContentsIcon}',
				name: 'tableOfContents',
				hidden: '@{autoViewOrDtreeViewIsPressed}'
			},
			{
				name: 'viewAttachments',
				text: '~~attachments~~',
				hidden: '@{autoViewOrDtreeViewIsPressed}'
			},
			{
				xtype: 'menuseparator',
				hidden: '@{autoViewOrDtreeViewIsPressed}'
			},
			{
				name: 'print',
				hidden: '@{autoViewOrDtreeViewIsPressed}',
				handler: function(button) {
					var frames = button.up('menu').up('panel').down('#wikiView').getEl().query('iframe#wiki_frame');
					if (frames.length > 0) {
						frames[0].contentWindow.print();
					}
				}
			},
			{
				name: 'pageInfo',
				hidden: '@{autoViewOrDtreeViewIsPressed}'
			}]
		},{	
			//iconCls: 'rs-icon-button icon-comments',
			name: 'collaborationOptions',
			menu: ['follow', 
			{
				iconCls: 'rs-social-menu-button icon-check',
				name: 'unfollow',
				text: '~~follow~~',
				width : 250
			}, 'viewFollowers', 
			{
				xtype: 'menuseparator',
			},
			{
				name: 'enableCollaboration',
				text: '~~social~~',
			},
			{
				iconCls: 'rs-social-menu-button icon-check',
				name: 'disableCollaboration',
				text: '~~social~~',
			},
			{
				xtype: 'menuseparator',
			},
			{
				name: 'unlockStream',
				disabled: '@{!isEditable}'
			},
			{
				iconCls: 'rs-social-menu-button icon-check',
				name: 'lockStream',
				disabled: '@{!isEditable}',
				text: '~~unlockStream~~'
			},
			{
				name: 'viewFeedbacks',
			},
			{
				text: '~~submitFeedback~~',
				disabled: '@{unlockStreamIsVisible}',
				handler: function(button) {
					button.fireEvent('showFeedback', button, button)
				},
				listeners: {
					showFeedback: '@{feedback}'
				}
			},
			{
				xtype: 'menuseparator',
			},
			{
				name: 'ratings',
			}]
		},{
			iconCls: 'rs-icon-button icon-file-text',
			name: 'editFileOptions',
			menu: [
			{
				name: 'save',
				width : 200,
				listeners: {
					doubleClickHandler: '@{saveAndExit}',
					render: function(button) {
						button.getEl().on('dblclick', function() {
							button.fireEvent('doubleClickHandler')
						});
						clientVM.updateSaveButtons(button);
					}
				}
			},
			'saveAs', 
			{
				xtype: 'menuseparator',
			},
			'saveAndCommit', 
			'viewRevisionList',
			{
				xtype: 'menuseparator',
				hidden: '@{!versionsMenuseparator}',
			}, 
			'properties',
			'source',
			'attachments', 
			{
				xtype: 'menuseparator',
				hidden: '@{!resolutionBuilderIsVisible}',
			}, 
			'resolutionBuilder', 
			'xml',
			{
				xtype: 'menuseparator',
				hidden: '@{!exitToViewIsVisible}',
			}, 
			'exitToView']
		},		
		{
			xtype: 'tbseparator',
			hidden: '@{!mainModelIsVisible}'
		}, {
			xtype: 'button',
			text: '~~parameters~~',
			cls: 'rs-toggle-btn rs-small-btn',
			enableToggle: true,
			hidden: '@{!parametersIsVisible}',
			pressed: '@{parametersIsPressed}'
		},{
			xtype: 'tbseparator',
			hidden: '@{!backToMainModelIsVisible}'
		},{
			name: 'parametersTab',
			cls: 'rs-toggle-btn rs-small-btn',
			text: '~~parameters~~',
			//cls: 'rs-page-tab',
			pressed: true
		},{
			name: 'mainModel',
			cls: 'rs-toggle-btn rs-small-btn' 
		}, {
			name: 'abortModel',
			cls: 'rs-toggle-btn rs-small-btn' 
		}, {
			name: 'backToMainModel',
			text: '~~mainModel~~',
			cls: 'rs-toggle-btn rs-small-btn' 
		}, {
			name: 'backToAbortModel',
			text: '~~abortModel~~',
			cls: 'rs-toggle-btn rs-small-btn' 
		},
		'->', 
		/* TODO - remove to support Version Control */
		{
			xtype: 'tbtext',
			text: '@{lockedDisplayText}',
			htmlEncode: false,
			cls: 'rs-link',
			margin : '0 10 0 0',
			tooltip: '~~lockedByTooltip~~',
			listeners: {
				render: function(tbtext) {
					if (hasPermission(tbtext._vm.accessRights.uadminAccess)) {
						Ext.create('Ext.tip.ToolTip', {
							target: tbtext.getEl(),
							html: tbtext.tooltip
						})

						tbtext.getEl().on('click', function() {
							tbtext.fireEvent('overrideLock', tbtext)
						})
					}
				},
				overrideLock: '@{overrideLock}'
			}
		},
		{
			text: '',
			cls: 'rs-execute-button',
			iconCls: 'rs-social-button icon-play',
			tooltip: '~~executionTooltip~~',
			hidden: '@{!showExecution}',
			disabled: '@{!isExecutable}',
			handler: function(button) {
				button.fireEvent('execution', button, button)
			},
			listeners: {
				doubleClickHandler: '@{executeWithDefault}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler', button, button)
					});
				},
				execution: '@{execution}'
			}
		},
		{
			iconCls: 'rs-icon-button @{socialPressedCls} icon-comments',
			hidden: '@{!showSocial}',
			style: 'left: -1px',
			enableToggle: true,
			pressed: '@{socialIsPressed}',
			tooltip: '~~worksheetSocialTooltip~~'
		},
		{
			iconCls: 'rs-social-button icon-pencil',
			name: 'editPage',
			text: '',
			tooltip: '~~editPage~~',
			hidden: '@{!viewTabIsPressed}',
			disabled: '@{!isEditable}'
		},
		{
			text: '',
			iconCls: 'rs-social-button icon-refresh',
			tooltip: '~~refreshPage~~',
			hidden: '@{reloadIsVisible}',
			handler:'@{refresh}',	
			listeners: {		
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		},
		{
			iconCls: 'rs-icon-button icon-plus-sign-alt',
			name: 'addLayoutSection',
			hidden: '@{!addLayoutSectionIsVisible}',
			cls : 'rs-small-btn rs-btn-light'
		},
		{
			xtype: 'tbseparator',
			hidden: '@{!addLayoutSectionIsVisible}'
		},
		{
			name: 'save',
			iconCls: 'rs-social-button icon-save',
			text: '',
			tooltip: '~~savePage~~',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		},
		{
			name: 'exitToView',
			iconCls: 'rs-social-button icon-reply-all',
			text: '',
			tooltip: '~~exitToView~~'
		},
		{
			iconCls: 'rs-social-button icon-repeat',
			name: 'reload',
			text: '',
			tooltip: '~~reloadPage~~',
		}]
	}],
	items: [{
		style : 'border-top:1px solid silver',	
		region: 'center',
		layout: {
			type: 'card',
			deferredRender: true
		},
		activeItem: '@{activeScreen}',
		defaults: {
			hideMode: 'offsets' //Offsets because of bug in firefox https://bugzilla.mozilla.org/show_bug.cgi?id=548397
		},
		items: [{
			itemId: 'wikiView',		
			html: '@{wikiURL}'
		},{
			xtype: '@{sectionsLayout}',
			itemId : 'section-layout',
			bodyPadding : '10 20 0 20',
		},{
			xtype: '@{automation}'
		},{
			xtype: '@{params}',
			bodyPadding: '10'
		}, {
			xtype: '@{automationView}'
		}],
		listeners: {
			afterrender: function(panel) {
				function beforedestroyHandler(){
					panel.fireEvent('beforedestroy');
				};
				document.addEventListener("beforedestroy", beforedestroyHandler);
				panel._vm.on("removeEventReference", function(){
					document.removeEventListener("beforedestroy", beforedestroyHandler);
				})
			},
			beforedestroy : '@{beforeDestroyComponent}'
		}
	}, {
		region: 'east',
		split: true,
		width: 480,
		stateId: 'wikiSocialBar',
		stateful: true,
		hidden: '@{!showCollaboration}',
		layout: 'fit',
		items: [{
			xtype: '@{socialDetail}'
		}]
	},{
		region: 'west',
		split: true,
		width: 350,
		autoScroll: true,
		html: '&nbsp;',
		itemId: 'contentTable',
		hidden: '@{!tableOfContentsIsPressed}',
		listeners: {
			show: function(panel) {
				var count = 0;

				function updateContent() {
					var wikiView = panel.up('panel').down('#wikiView'),
						iframe = wikiView.getEl().query('iframe#wiki_frame')[0],
						doc = iframe.contentDocument,
						win = iframe.contentWindow,
						headings = Ext.fly(doc).query('[id^=_H_]'),
						html = [],
						tempHtml;
					Ext.Array.forEach(headings, function(heading, idx) {
						var level = heading.className.split('-').length - 1;
						if (level > 3)
							return;
						tempHtml = Ext.fly(heading).getHTML()
						if (tempHtml[tempHtml.length - 1] == ':') tempHtml = tempHtml.substring(0, tempHtml.length - 1);
						html.push(Ext.String.format('<div style="padding-left:10px;padding-top:{2}px;{3}"><a href="#" class="rs-link" data="{0}">{1}</a></div>',
							Ext.fly(heading).getAttribute('id'),
							tempHtml, (level == 1 ? 20 : (level == 2 ? 10 : 0)), (level == 1 ? 'font-size:18px;' : (level == 2 ? 'font-weight:bold' : ''))
						))
					})
					panel.update(html.join(''))
					links = panel.getEl().query('a')
					Ext.Array.forEach(links, function(link) {
						Ext.create('Ext.tip.ToolTip', {
							target: link,
							html: Ext.fly(link).getHTML()
						})
						Ext.fly(link).on('click', function(e) {
							e.preventDefault()
							win.Ext.fly(Ext.fly(link).getAttribute('data')).dom.scrollIntoView(true)
						})
					})
					if (panel.up().up().down('[name="tableOfContents"]').pressed && count++ < 5)
						Ext.defer(function() {
							updateContent();
						}, 2000);
				}
				updateContent();
			}
		}
	}],
	listeners : {
		//beforedestroy : '@{beforeDestroyComponent}'
	}
});
