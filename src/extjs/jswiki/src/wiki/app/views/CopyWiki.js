glu.defView('RS.wiki.CopyWiki', {
	title: '@{windowTitle}',	
	modal: true,
	padding : 15,
	width : 600,
	minHeight : 250,	
	cls : 'rs-modal-popup',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults : {
		labelWidth : 130,
		margin : '4 0'
	},
	items: [{
		xtype: 'textfield',
		name: 'namespace'
	}, {
		xtype: 'textfield',
		name: 'name'
	}, {
		xtype: 'fieldcontainer',		
		defaultType: 'radiofield',		
		defaults: {
			hideLabel: true,
			margin: '0px 0px 0px 135px',
		},
		items: [{
			name: 'replace',
			value: '@{overwrite}',
			hidden: '@{!overwriteIsVisible}',
			boxLabel: '~~overwrite~~',
			tooltip: '~~overwrite_tooltip~~',
			listeners: {
				render: function(radio) {
					if (radio.tooltip)
						Ext.create('Ext.tip.ToolTip', {
							target: radio.getEl(),
							html: radio.tooltip
						})
				}
			}
		}, {
			name: 'replace',
			hidden: '@{!skipIsVisible}',
			value: '@{skip}',
			boxLabel: '~~skip~~',
			tooltip: '~~skip_tooltip~~',
			listeners: {
				render: function(radio) {
					if (radio.tooltip)
						Ext.create('Ext.tip.ToolTip', {
							target: radio.getEl(),
							html: radio.tooltip
						})
				}
			}
		}, {
			name: 'replace',
			hidden: '@{!updateIsVisible}',
			value: '@{update}',
			boxLabel: '~~update~~',
			tooltip: '~~update_tooltip~~',
			listeners: {
				render: function(radio) {
					if (radio.tooltip)
						Ext.create('Ext.tip.ToolTip', {
							target: radio.getEl(),
							html: radio.tooltip
						})
				}
			}
		}]
	}],
	buttons: [{
        text : '@{copySaveTxt}',
        cls : 'rs-med-btn rs-btn-dark',
        name: 'copy',
    },{       
        cls : 'rs-med-btn rs-btn-dark',
        name : 'replace',
    },{       
        cls : 'rs-med-btn rs-btn-light',
        name : 'cancel',
    }]
})