glu.defView('RS.wiki.FormWizard', {
	asWindow: {
		height: '@{formHeight}',
		width: '@{formWidth}',		
		title: '~~formWizardTitle~~',
		modal: true
	},

	bodyPadding: '10px',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'textfield',
		fieldStyle: 'text-transform:uppercase',
		name: 'name'
	}],

	buttonAlign: 'left',
	buttons: ['create', 'cancel']
})