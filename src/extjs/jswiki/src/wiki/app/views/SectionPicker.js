glu.defView('RS.wiki.SectionPicker', {
	asWindow: {
		title: '~~chooseComponent~~',
		height: 690,
		width: 800,
		modal: true
	},
	bodyPadding: '10px',
	buttonAlign: 'left',
	buttons: ['addComponent', 'cancel'],
	tbar: [{
		xtype: 'textfield',
		name: 'filter',
		emptyText: '~~filterEmpty~~',
		listeners: {
			afterrender: function(field) {
				Ext.defer(function() {
					field.focus()
				}, 100)
			}
		}
	}],
	layout: 'fit',
	items: [{
		xtype: 'dataview',
		deferInitialRefresh: false,
		store: '@{sections}',
		tpl: Ext.create('Ext.XTemplate',
			'<tpl for=".">',
			'<div class="sectionPicker-section">', '<img width="85" height="85" src="/resolve/images/sections/components/{[values.name.replace(/ /g, "-")]}.png" />',
			'<strong>{name}</strong>',
			'</div>',
			'</tpl>'
		),
		itemSelector: 'div.sectionPicker-section',
		overItemCls: 'sectionPicker-section-hover',
		selectedItemCls: 'sectionPicker-section-selected',
		autoScroll: true,
		listeners: {
			selectionChange: function(sm, selected, eOpts) {
				sm.view.fireEvent('sectionSelected', sm.view, selected)
			},
			sectionSelected: '@{sectionSelected}'
		}
	}]
})