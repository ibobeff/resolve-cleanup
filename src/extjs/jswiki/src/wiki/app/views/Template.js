glu.defView('RS.wiki.Template', {
	xtype: 'form',
	autoScroll: true,
	padding: 15,	
	dockedItems: [{
		xtype: 'toolbar',		
		ui: 'display-toolbar',
		cls : 'rs-dockedtoolbar',
		defaultButtonUI: 'display-toolbar-button',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{title}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',			
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],	
	items: [{
		style : 'border-top:1px solid silver',
		padding : '8 0 0 0',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items : [{
			xtype: 'textfield',
			fieldLabel: '~~uname~~',
			name: 'uname',
			maxLength: 300
		}, {
			xtype: 'fieldcontainer',
			layout: 'hbox',
			hidden: '@{!uwikiDocNameIsVisible}',
			items: [{
				xtype: 'displayfield',
				fieldLabel: '~~uwikiDocName~~',
				name: 'uwikiDocName',
				margin: '0px 10px 0px 0px',
				maxLength: 300
			}, {
				xtype: 'image',
				cls: 'rs-btn-edit',
				listeners: {
					handler: '@{jumpToWiki}',
					render: function(image) {
						image.getEl().on('click', function() {
							image.fireEvent('handler')
						})
					}
				}
			}]
		}, {
			xtype: 'fieldcontainer',
			layout: 'hbox',
			hidden: '@{!uformNameIsVisible}',
			items: [{
				xtype: 'displayfield',
				fieldLabel: '~~uformName~~',
				name: 'uformName',
				margin: '0px 10px 0px 0px',
				maxLength: 300
			}, {
				xtype: 'image',
				cls: 'rs-btn-edit',
				listeners: {
					handler: '@{jumpToForm}',
					render: function(image) {
						image.getEl().on('click', function() {
							image.fireEvent('handler')
						})
					}
				}
			}]
		}, {
			xtype: 'textarea',
			fieldLabel: '~~udescription~~',
			name: 'udescription'
		}]
	},{		
		title : '~~Roles~~',
		bodyPadding : '10 0 0 0',
		xtype: '@{accessRights}'
	}]
});
