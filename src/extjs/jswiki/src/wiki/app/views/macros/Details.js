glu.defView('RS.wiki.macros.Details', {
	dockedItems: [{
		html: '@{titleDisplay}'
	}],
	items: '@{filteredResults}',
	hidden: '@{!detailMacroIsVisible}',
	listeners: {
		afterrender: function(panel) {
			Ext.defer(function() {
				var width = Ext.getBody().getWidth(),
					masks = Ext.query('div[class*=x-mask]');
				Ext.Array.forEach(masks, function(mask) {
					if (Ext.fly(mask).getWidth() > width) {
						Ext.fly(mask).setWidth(width)
					}
				})
				panel.doLayout()
			}, 100)
			Ext.EventManager.onWindowResize(function() {
				var width = Ext.getBody().getWidth(),
					masks = Ext.query('div[class*=x-mask]');
				Ext.Array.forEach(masks, function(mask) {
					if (Ext.fly(mask).getWidth() > width) {
						Ext.fly(mask).setWidth(width)
					}
				})
				panel.doLayout()
			});
			function beforedestroyHandler(){
				panel.fireEvent('beforedestroy');
			};
			document.addEventListener("beforedestroy", beforedestroyHandler);
			panel._vm.on("removeEventReference", function(){
				document.removeEventListener("beforedestroy", beforedestroyHandler);
			})
		},
		expand: function(panel) {
			panel.doLayout()
		},
		collapse: function(panel) {
			panel.doLayout()
		},
		beforedestroy : '@{beforeDestroyComponent}'
	}
})

glu.defView('RS.wiki.macros.DetailsDisplay', {
	html: '@{detailRendering}',
	// this is going againt our general pattern... so another hack...need to think more about it
	onDetailRender: '@{onRender}',
	listeners: {
		render: function() {
			try {
				new Function('view', this.onDetailRender)(this)
			} catch (e) {}
		}
	}
})

glu.defView('RS.wiki.macros.DetailSeparator', {
	html: '<hr/>'
})