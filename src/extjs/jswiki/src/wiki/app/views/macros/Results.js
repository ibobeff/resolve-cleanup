glu.defView('RS.wiki.macros.Results', {
	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'worksheetResults',
		store: '@{filteredResults}',
		columns: '@{worksheetResultColumns}',
		title: '@{titleDisplay}',
		collapsible: true,
		disableSelection: true,
		collapsed: '@{collapsed}',
		viewConfig: {
			loadMask: false,
			enableTextSelection: true
		},
		tools: [{
			type: 'refresh',
			handler: function() {
				this.up('grid').fireEvent('refresh', this)
			}
		}],
		listeners: {
			refresh: '@{refresh}',
			expand: function(panel) {
				panel.doLayout()
			},
			collapse: function(panel) {
				panel.doLayout()
			},
			afterrender: function(panel) {
				panel.store.on('datachanged', function() {
					panel.doLayout()
				})
				Ext.defer(function() {
					panel.doLayout()
				}, 100)
				Ext.EventManager.onWindowResize(function() {
					panel.doLayout()
				})
				function beforedestroyHandler(){
					panel.fireEvent('beforedestroy');
				};
				document.addEventListener("beforedestroy", beforedestroyHandler);
				panel._vm.on("removeEventReference", function(){
					document.removeEventListener("beforedestroy", beforedestroyHandler);
				})
			},
			beforedestroy : '@{beforeDestroyComponent}'
		}
	}]
})