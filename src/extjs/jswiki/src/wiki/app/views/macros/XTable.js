glu.defView('RS.wiki.macros.XTable', {
	xtype: 'grid',
	columns: '@{columns}',
	store: '@{store}',
	hideSelModel: '@{!showEdit}',
	hidePager: '@{!showPager}',
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.editColumnTooltip,
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'sysId',
		forceFire: true
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar',
		name: 'actionBar',
		items: []
	}],
	plugins: [{
		ptype: 'pager',
		showSysInfo: false
	}],
	listeners: {
		editAction: '@{editRecord}',
		afterrender: function(panel) {
			Ext.defer(function() {
				var width = Ext.getBody().getWidth(),
					masks = Ext.query('div[class*=x-mask]');
				Ext.Array.forEach(masks, function(mask) {
					if (Ext.fly(mask).getWidth() > width) {
						Ext.fly(mask).setWidth(width)
					}
				})
				panel.doLayout()
			}, 100)
			Ext.EventManager.onWindowResize(function() {
				var width = Ext.getBody().getWidth(),
					masks = Ext.query('div[class*=x-mask]');
				Ext.Array.forEach(masks, function(mask) {
					if (Ext.fly(mask).getWidth() > width) {
						Ext.fly(mask).setWidth(width)
					}
				})
				panel.doLayout()
			})
		}
	}
})