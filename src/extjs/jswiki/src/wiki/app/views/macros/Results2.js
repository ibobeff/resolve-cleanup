glu.defView('RS.wiki.macros.Results2', {
	xtype: 'treepanel',
	name: 'worksheetResults',
	store: '@{filteredResults}',
	columns: '@{worksheetResultColumns}',
	title: '@{titleDisplay}',
	cls: 'results rs-grid-dark',
	header : '@{showHeader}',
	collapsible: true,
	rootVisible: false,
	useArrows: true,
	viewConfig: {
		loadMask: false,
		enableTextSelection: true,
		stripeRows: true
	},
	tools: [{
		type: 'refresh',
		handler: function() {
			this.up('treepanel').fireEvent('refresh', this)
		}
	}],
	comfortableSize: function() {
		this.doLayout();
	},	
	listeners: {
		refresh: '@{refresh}',
		expand: function(panel) {
			panel.comfortableSize();
			// if user manually expand autoCollapse panel, then ignore autoCollapse setting
			if (!panel._vm.autoCollapseEvent) {
				panel._vm.overrideAutoCollapse = true;
			}
		},
		collapse: function(panel) {
			panel.comfortableSize();
			// if user manually collapse autoCollapse panel, then ignore autoCollapse setting
			if (!panel._vm.autoCollapseEvent) {
				panel._vm.overrideAutoCollapse = true;
			}
		},
		afterrender: function(panel) {
			var cols = this.getColumns();
			if (clientVM.showCreatedOn == undefined || clientVM.showCreatedOn == null) {
				this.fireEvent('resultrender', this, cols);
			} else {
				cols.forEach(function(col){
					if(col.dataIndex == 'sysCreatedOn') {
						col.setVisible(clientVM.showCreatedOn);
					}
				});
			}
			panel.store.on('datachanged', function() {
				panel.comfortableSize();
				if (panel.getEl().query(panel.getView().getBodySelector()).length == 0)
					return;
			});
			panel.store.on('showWorksheet', function() {
				panel.show();
			});
			panel.store.on('hideWorksheet', function() {
				panel.hide();
			});
			panel.store.on('expandWorksheet', function() {
				panel.expand();
			});
			panel.store.on('collapseWorksheet', function() {
				panel.collapse();
			});
			Ext.EventManager.onWindowResize(function() {
				panel.comfortableSize();
			});
			function beforedestroyHandler(){
				panel.fireEvent('beforedestroy');
			};
			document.addEventListener("beforedestroy", beforedestroyHandler);
			panel._vm.on("removeEventReference", function(){
				document.removeEventListener("beforedestroy", beforedestroyHandler);
			})
		},
		beforedestroy : '@{beforeDestroyComponent}',
		resultrender: '@{resultRender}'
	}
})