/**
 * View definition for the InfoBar.  Displays tabs to give the user additional information
 * about a wiki document.
 */
glu.defView('RS.wiki.macros.InfoBar', {

	xtype: 'tabpanel',

	border: false,

	plain: true,

	height: '@{height}',
	width: '@{width}',

	items: '@{tabs}',
	activeTab: '@{selectedInfoTab}',

	autoResize: '@{autoResize}',

	listeners: {
		render: function(tabpanel) {
			if (tabpanel.autoResize) {
				Ext.EventManager.onWindowResize(tabpanel.doResize, tabpanel)
				tabpanel.doResize()
			}
		},
		afterrender : function(panel){
			document.addEventListener("beforedestroy", function(){			
				panel.removeAll();
			})
		}		
	},
	/**
	 * function to autoSize the control based on initial configuration and whenever the window
	 * resizes to allow for a "max width/height" control
	 */
	doResize: function() {
		var parent = this.getEl().parent(),
			windowHeight = parent.getHeight(),
			windowWidth = parent.getWidth();

		if (document.documentElement.scrollHeight >= document.documentElement.clientHeight) this.setWidth(this.initialConfig.width || windowWidth - Ext.getScrollBarWidth())
		// this.setSize(this.initialConfig.width || windowWidth - Ext.getScrollBarWidth(), this.initialConfig.height || windowHeight);
		else this.setWidth(this.initialConfig.width || windowWidth)
		// this.setSize(this.initialConfig.width || windowWidth, this.initialConfig.height || windowHeight);
	}
});