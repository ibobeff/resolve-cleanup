/**
 * View definition for Social tab to display in the InfoBar.
 */
glu.defView('RS.wiki.macros.Social', {
	title: '~~social~~',

	layout: 'fit',
	items: [{
		xtype: '@{socialDetail}'
	}],
	listeners : {	
		beforedestroy : function(panel){
			panel.removeAll();	
		}	
	}
});