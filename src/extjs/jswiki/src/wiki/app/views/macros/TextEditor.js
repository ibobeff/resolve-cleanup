
glu.defView('RS.wiki.macros.TextEditor', {
	margin: '10 0 0 0',
	title: '@{title}',
	layout: {
		type: 'vbox',
		align: 'stretch'		
	}, 
	listeners: {
		afterrender: function(panel) {
			function beforedestroyHandler(){
				panel.fireEvent('beforedestroy');
			};
			document.addEventListener("beforedestroy", beforedestroyHandler);
			panel._vm.on("removeEventReference", function(){
				document.removeEventListener("beforedestroy", beforedestroyHandler);
			})
		},
		beforedestroy : '@{beforeDestroyComponent}'
	},
	items: [{
		layout: 'hbox',
		xtype: 'toolbar',
		items: [{
			xtype: 'tbtext', 
			cls: 'rs-display-name',
			text: '~~notes~~'
		}, {
			xtype: 'button',
			name: 'edit',
			text: '',
			tooltip: '~~editNotes~~',
			baseCls: 'x-tool',
			iconCls: 'rs-social-button notes icon-pencil',
		},
		/*
		{
			xtype: 'button',
			name: 'cancel',
			text: '',
			tooltip: '~~cancelNotes~~',
			baseCls: 'x-tool',
			iconCls: 'rs-social-button notes icon-remove-sign',
		}, 
		*/
		'->', {
			xtype: 'text',
			text: '@{lastComment}',
			hidden: '@{!showLastComment}',
			style: {
				'font-style': 'italic'
			}
		}]
	}, {
		items: [{
			layout: 'fit', 
			hidden: '@{!isEditing}',
			resizable: {
				minHeight: 120,
				maxHeight: 300,
				handles: 's',
				pinned: true
			},
			items: [{
				flex: 1,
				xtype: 'textarea',
				name: 'notes',
				style: {
					width: '100%',
				},
				grow: true,
				growMin: 120,
				growMax: 300,
				height: 120,
				hideLabel: true,
			}]
		}, {
			xtype: 'component',
			html: '@{notes}',
			maxHeight: 300,
			overflowY: 'auto',
			padding: '4 5 5 6',
			cls: 'notes',
			border: 1,
			style: {
				borderColor: 'lightgrey',
				borderStyle: 'solid'
			},
			hidden: '@{!showNotes}',
		}],
	}],
	dockedItems: [{
		xtype: 'toolbar',
		margin : '5 0 0 0',
		cls: 'rs-body',
		dock: 'bottom',		
		items: ['->', {
			name: 'save', 
			text: '~~update~~',
			cls: 'rs-small-btn rs-btn-dark',
		}, {
			name: 'cancel',
			cls: 'rs-small-btn rs-btn-light',
		}]
	}]
});
