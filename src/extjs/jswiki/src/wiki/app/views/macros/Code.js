glu.defView('RS.wiki.macros.Code', {
	xtype: 'AceEditor',
	autofocus: false,
	parser: '@{type}',
	sourceEl: '@{contentId}',
	showGutter: '@{showLineNumbers}',
	readOnly: '@{readOnly}',
	listeners: {
		editorcreated: function(panel) {
			var newHeight = panel.editor.getSession().getScreenLength() * panel.editor.renderer.lineHeight + panel.editor.renderer.scrollBar.getWidth();
			panel.setHeight(newHeight)
		},
		afterrender: function(panel) {
			Ext.defer(function() {
				var containerHeight = Ext.fly(panel.getEl().parent().parent()).getHeight();
				if (panel.getHeight() > containerHeight) panel.setHeight(containerHeight)
				panel.doLayout()
			}, 100)
			Ext.EventManager.onWindowResize(function() {
				panel.doLayout()
			})
		}
	}
})