/**
 * View definition for the Feeback Results tab in the InfoBar
 */
glu.defView('RS.wiki.macros.FeedbackResults', {
	title: '~~feedbackResults~~',
	modal: '@{modal}',
	height: '@{height}',
	width: '@{width}',
	cls : 'rs-modal-popup',
	padding : 15,
	border: false,
	layout: {
		type: 'hbox',
		align: 'stretch'
	},	
	buttons: [{
		name: 'close',
		cls : 'rs-med-btn rs-btn-dark',
		hidden: true,
		listeners: {
			beforerender: function(btn) {
				btn.fireEvent('closeButtonRender', btn, btn);
			},
			closeButtonRender: '@{showCloseButtonOnPopup}'
		}
	}],
	listeners: {
		beforeshow: function(win) {
			win.fireEvent('beforeWinShow', this, win, Ext.getBody().getWidth(), Ext.getBody().getHeight());
		},
		beforeWinShow: '@{setPopupSize}'
	},
	items: [{
		width: 250,
		border: false,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			fieldLabel: '~~total~~',
			padding: '0 0 0 30',
			labelWidth: 50,
			name: 'rating',
			anchor: '100%',
			xtype: 'ratingfield',
			readOnly: true,
			displayTextRating: true,
			initialValue: '@{initialValue}'
		}, {
			xtype: 'ratingchart',
			voteText: '~~vote~~',
			votesText: '~~votes~~',
			starText: '~~star~~',
			starsText: '~~stars~~',
			store: '@{chartStore}'			
		}]
	}, {
		xtype: 'ratinggrid',
		cls : 'rs-grid-dark',
		ratingHeader: '~~rating~~',
		dateHeader: '~~date~~',
		commentsHeader: '~~comments~~',
		name: 'ratingGridStore'
	}]
});