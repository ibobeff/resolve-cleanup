glu.defView('RS.wiki.macros.SocialLink', {
	html: '@{displayText}',
	listeners: {
		render: function(panel) {
			var links = panel.getEl().query('span[class*=rs-link]');
			Ext.Array.forEach(links, function(link) {
				Ext.fly(link).on('click', function() {
					panel.fireEvent('linkClicked', panel)
				})

			})
		},
		linkClicked: '@{linkClicked}'
	}
})