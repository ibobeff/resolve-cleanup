/**
 * View definition for History.  Displays as a grid in the InfoBar
 */
glu.defView('RS.wiki.macros.History', {
	xtype: 'grid',

	border: false,

	title: '~~history~~',

	store: '@{historyStore}',

	columns: [{
		header: '~~documentName~~',
		dataIndex: 'documentName'
	}, {
		header: '~~revisionNumber~~',
		dataIndex: 'version',
		width: 75
	},
	/* TODO - support Version Control 
	{
		header: '~~stableVersion~~',
		dataIndex: 'isStable',
		width: 75,
		renderer: RS.common.grid.booleanRenderer(),
	}, {
		header: '~~referenced~~',
		dataIndex: 'referenced',
		width: 75,
		renderer: RS.common.grid.booleanRenderer(),
	}, 
	*/
	{
		header: '~~comment~~',
		flex: 1,
		dataIndex: 'comment',
		renderer: RS.common.grid.plainRenderer()
	}, {
		header: '~~sysUpdatedBy~~',
		dataIndex: 'createdBy'
	}, {
		header: '~~sysUpdatedOn~~', // '~~createdOn~~', TODO - support Version Control
		dataIndex: 'sysCreatedOn',
		width: 150,
		renderer: function(value) {
			return Ext.Date.format(new Date(value), 'Y-m-d g:i:s A');
		}
	}]
});