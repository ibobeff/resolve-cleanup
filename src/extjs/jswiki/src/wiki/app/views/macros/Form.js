glu.defView('RS.wiki.macros.Form', {
	title: '@{titleDisplay}',
	titleCollapse: true,
	collapsible: '@{collapsible}',
	collapsed: '@{collapsed}',
	tooltip: '@{tooltip}',
	formName: '@{name}',
	recordId: '@{recordId}',
	popout: '@{popout}',
	popoutHeight: '@{popoutHeight}',
	popoutWidth: '@{popoutWidth}',
	hideCollapseTool: true,
	items: [],
	comfortableSize: function() {
		this.doLayout();
	},
	listeners: {
		render: function(form) {
			if (form.popout) {
				//setup on click listener
				form.getHeader().on('click', function() {
					form.fireEvent('beforepopout', form)
					//open up the window with the rsform in it
					var pop = Ext.create('Ext.window.Window', {
						layout: 'fit',
						modal: true,
						height: this.popoutHeight,
						width: this.popoutWidth,
						items: [{
							xtype: 'rsform',
							showLoading: false,
							formName: this.formName,
							recordId: this.recordId,
							items: []
						}]
					});
					pop.show()
					form.fireEvent('showpopout', pop)
				}, form)

				//Remove underline on header style
				form.getHeader().on('afterrender', function(h) {
					h.getEl().setStyle('border-bottom', '0px')
				})
			} else {
				var problemId;
				if (problemId = this._vm.queryString) {
					problemId = problemId.match(/PROBLEMID=[0-9a-fA-F]*/);
					if (problemId && problemId.length) {
						problemId = problemId[0].split('PROBLEMID=')[1];
					}
				}
				form.add({
					xtype: 'rsform',
					showLoading: false,
					formName: form.formName,
					recordId: this.recordId,
					problemId: problemId,
					items: []
				})
			}
		},
		afterrender: function(form) {
			//Configure proper tooltip on the title
			if (form.tooltip && form.getHeader()) {
				Ext.create('Ext.tip.ToolTip', {
					html: form.tooltip,
					target: form.getHeader().getEl()
				})
			}

			Ext.EventManager.onWindowResize(function() {
				form.comfortableSize();
			});
		},
		showpopout: '@{showpopout}'
	}
})