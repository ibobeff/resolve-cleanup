function executeActionTaskPopUp(id, namespace, name, isDebug, urlParams) {
	var params = {};
	try {
		params = Ext.Object.fromQueryString(urlParams);
	} catch (e) {}
	var childVM = {
		mtype: 'RS.actiontask.Execute',
		embedded: true,
		executeDTO: {
			inputParams: params,
			id: id
		}
	};
	childVM = glu.model(childVM);
	childVM.init();
	return false;
}