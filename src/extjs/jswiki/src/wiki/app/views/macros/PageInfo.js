/**
 * View definition for PageInfo tab to display in the InfoBar.
 */
glu.defView('RS.wiki.macros.PageInfo', {
	title: '~~pageInfo~~',

	html: 'Page Info goes here'
});