/**
 * View definition for the Feeback tab in the InfoBar.
 */
glu.defView('RS.wiki.macros.Feedback', {
	title: '~~feedback~~',

	border: false,

	width: '@{width}',
	height: '@{height}',

	items: [{
		bodyPadding: '0 10 0 10',
		border: false,

		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		autoScroll: true,

		buttonAlign: '@{buttonAlign}',

		buttons: ['requestReview', {
			name: 'submitFeedback'
		}, {
			name: 'cancel',
			hidden: '@{!isWindow}'
		}],
		defaults: {
			labelWidth: 120
		},
		items: [{
			xtype: 'fieldcontainer',
			layout: {
				type: 'hbox',
				align: 'left'
			},
			items: [{
				xtype: 'displayfield',
				labelWidth: 120,
				name: 'lastReviewed'
			}, {
				html: '<i class="rs-social-button icon-flag @{flagCls}" style="padding-left:10px;height:17px;width:17px"></i>'
			}]
		}, {
			xtype: 'usefulfield',
			name: 'markForReview',
			fieldLabel: '~~notFlaggedForReview~~'
		}, {
			xtype: 'usefulfield',
			name: 'useful'
		}, {
			name: 'rating',
			xtype: 'ratingfield'
			// initialValue: '@{initialRating}'
		}, {
			xtype: 'textarea',
			name: 'feedbackComment'
		}]
	}]

});