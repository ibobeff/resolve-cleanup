/**
 * View definition for the Tags tab to display in the InfoBar
 */
glu.defView('RS.wiki.macros.Tags', {

	xtype: 'grid',

	border: false,

	title: '~~tags~~',

	store: '@{tagsStore}',

	columns: [{
		header: '~~tagName~~',
		dataIndex: 'name',
		flex: 1
	}]
});