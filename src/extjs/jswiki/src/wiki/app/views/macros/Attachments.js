/**
 * View definition for Attachments.  Displays as a grid in the InfoBar.
 */
glu.defView('RS.wiki.macros.Attachments', {
	xtype: 'grid',

	border: false,

	title: '~~attachments~~',

	store: '@{attachmentsStore}',

	columns: '@{attachmentColumns}',

	downloadURL: function(url) {
		var iframe;
		iframe = document.getElementById("hiddenDownloader");
		if (iframe === null) {
			iframe = document.createElement('iframe');
			iframe.id = "hiddenDownloader";
			iframe.style.visibility = 'hidden';
			iframe.style.display = 'none';
			document.body.appendChild(iframe);
		}
		iframe.src = url;
	}
});