Ext.define('RS.wiki.SearchField', {
	extend: 'Ext.form.field.Trigger',
	alias: 'widget.lookupwikisearchfield',

	triggerBaseCls: 'icon-search',
	triggerCls: 'icon-search',
	initComponent: function() {
		var me = this;
		me.callParent();
		me.onTriggerClick = function() {
			me.fireEvent('openSearch', me);
		}
	}

})
glu.defView('RS.wiki.Lookup', {
	xtype: 'form',
	autoScroll: true,	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'textfield',
		fieldLabel: '~~uorder~~',
		name: 'uorder',
		maxLength: 300
	}, {
		xtype: 'textfield',
		fieldLabel: '~~uregex~~',
		name: 'uregex',
		maxLength: 300
	}, {
		xtype: 'lookupwikisearchfield',
		fieldLabel: '~~uwiki~~',
		name: 'uwiki',
		listeners: {
			openSearch: '@{openSearch}'
		}
	}, {
		xtype: 'textarea',
		fieldLabel: '~~udescription~~',
		flex: 1,
		name: 'udescription'
	}],
	buttons: [{
		name : 'save',
		cls :'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls :'rs-med-btn rs-btn-light'
	}],
	asWindow: {
		width: 600,
		title: '@{title}',
		cls : 'rs-modal-popup',
		padding : 15,
		modal: true
	}
});