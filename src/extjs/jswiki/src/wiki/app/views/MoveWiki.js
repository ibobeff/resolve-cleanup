glu.defView('RS.wiki.MoveWiki', {
	title: '@{windowTitle}',	
	width: 600,
	minHeight : 250,
	modal: true,
	padding : 15,
	cls :'rs-modal-popup',
	defaults : {
		labelWidth : 130,
		margin : '4 0'
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'textfield',
		name: 'namespace'
	}, {
		xtype: 'textfield',
		name: 'name'
	}, {
		xtype: 'checkbox',
		name: 'overwrite',
		hideLabel : true,
		margin : '0 0 0 135',
		boxLabel : '~~overwrite~~'
	}],
	buttons: [{
		name: 'rename',
        text : '~~moveRename~~',
        cls : 'rs-med-btn rs-btn-dark'
    },
	/*
	{ 
		name: 'move', 
        cls : 'rs-med-btn rs-btn-dark'
    },
	*/
	{
		name: 'cancel',
        cls : 'rs-med-btn rs-btn-light'
    }]
})