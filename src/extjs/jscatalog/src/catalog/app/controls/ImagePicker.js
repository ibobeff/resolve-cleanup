/*
Ext.define('RS.common.ImagePicker', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.imagepicker',

	// allowRemove: true,

	emptyText: '',
	allowDeselect: true,

	anchor: '-20',
	height: 300,

	// resizable: true,
	// resizeHandles: 's',

	fileServiceListUrl: '',
	fileServiceUploadUrl: '',

	uploader: {
		url: '/resolve/service',
		uploadpath: 'dev',
		autoStart: true,
		max_file_size: '2020mb',
		flash_swf_url: '/resolve/js/plupload/plupload.flash.swf',
		urlstream_upload: true,
		resize: {
			width: 120,
			height: 90,
			quality: 100
		},
		filters: [{
			title: 'Image files',
			extensions: 'jpg,gif,png'
		}]

	},

	initComponent: function() {
		var me = this

		me.title = me.displayName || me.title

		me.columns = [{
			text: RS.formbuilder.locale.uploadColumnName,
			flex: 1,
			sortable: false,
			dataIndex: 'name'
		}, {
			text: RS.formbuilder.locale.uploadColumnSize,
			width: 90,
			sortable: true,
			align: 'right',
			renderer: Ext.util.Format.fileSize,
			dataIndex: 'size'
		}, {
			text: RS.formbuilder.locale.uploadColumnChange,
			width: 75,
			sortable: true,
			hidden: true,
			hideable: false,
			dataIndex: 'percent'
		}, {
			text: RS.formbuilder.locale.uploadColumnState,
			width: 75,
			hidden: true,
			sortable: true,
			hideable: false,
			dataIndex: 'status'
		}, {
			text: RS.formbuilder.locale.uploadColumnStatus,
			width: 175,
			sortable: true,
			dataIndex: 'msg'
		}, {
			xtype: 'actioncolumn',
			hideable: false,
			hidden: !this.allowDownload,
			width: 25,
			items: [{
				icon: '/resolve/images/arrow_down.png',
				// Use a URL in the icon config
				tooltip: RS.formbuilder.locale.download,
				handler: function(grid, rowIndex, colIndex) {
					var rec = grid.getStore().getAt(rowIndex)
					if (rec) grid.ownerCt.downloadURL(Ext.String.format('/resolve/service/catalog/download?id={0}', rec.get('id'), grid.ownerCt.fileUploadTableName));
					else Ext.Msg.alert(RS.formbuilder.locale.noDownloadTitle, RS.formbuilder.locale.noDownloadBody)
				}
			}]
		}]

		me.uploader = me.createUploader()
		me.store = me.uploader.store

		var tbarItems = this.tbar && this.tbar.items ? this.tbar.items : []
		this.tbar = [{
			text: RS.formbuilder.locale.upload,
			name: 'uploadFile',
			itemId: 'uploadButton',
			tooltip: RS.formbuilder.locale.uploadTooltip,
			hidden: !this.allowUpload
		}, {
			text: RS.formbuilder.locale.removeFile,
			disabled: true,
			name: 'removeFile',
			itemId: 'removeButton',
			hidden: !this.allowRemove,
			scope: this,
			handler: function(button) {
				var selections = this.getSelectionModel().getSelection();
				Ext.MessageBox.show({
					title: RS.formbuilder.locale.confirmRemove,
					msg: selections.length === 1 ? RS.formbuilder.locale.confirmRemoveFile : Ext.String.format(RS.formbuilder.locale.confirmRemoveFiles, selections.length),
					buttons: Ext.MessageBox.YESNO,
					buttonText: {
						yes: RS.formbuilder.locale.removeFile,
						no: RS.formbuilder.locale.cancel
					},
					scope: me,
					fn: me.removeSelectedFiles
				})
			}
		}].concat(tbarItems)

		me.statusBar = new Ext.ux.StatusBar({
			dock: 'bottom',
			defaultText: RS.formbuilder.locale.ready
		})
		me.dockedItems = [me.statusBar]

		me.callParent()

		Ext.apply(me.uploader, {
			url: this.fileServiceUploadUrl,
			browse_button: this.down('#uploadButton').id,
			container: this.down('#uploadButton').ownerCt.id,
			drop_element: this.id,
			multipart_params: me.uploader.multipart_params || {
				recordId: me.recordId,
				tableName: me.fileUploadTableName
			},
			statusQueuedText: RS.formbuilder.locale.statusQueuedText,
			statusUploadingText: RS.formbuilder.locale.statusUploadingText,
			statusFailedText: Ext.String.format('<span style="color: red">{0}</span>', RS.formbuilder.locale.statusFailedText),
			statusDoneText: Ext.String.format('<span style="color: green">{0}</span>', RS.formbuilder.locale.statusDoneText),

			statusInvalidSizeText: RS.formbuilder.locale.statusInvalidSizeText,
			statusInvalidExtensionText: RS.formbuilder.locale.statusInvalidExtensionText
		});

		if (me.uploader.drop_element && (e = Ext.getCmp(me.uploader.drop_element))) {
			e.addListener('afterRender', function() {
				me.uploader.initialize();
			}, {
				single: true,
				scope: me
			})
		} else {
			me.listeners = {
				afterRender: {
					fn: function() {
						me.uploader.initialize();
						me.setEnabled(true);
					},
					single: true,
					scope: me
				}
			}
		}

		me.relayEvents(me.uploader, ['beforestart', 'uploadready', 'uploadstarted', 'uploadcomplete', 'uploaderror', 'filesadded', 'beforeupload', 'fileuploaded', 'updateprogress', 'uploadprogress', 'storeempty']);

		me.on({
			scope: this,
			afterlayout: function(grid, layout, eOpts) {
				var upEl = Ext.get(grid.uploader.uploader.id + '_' + grid.uploader.uploader.runtime + '_container');
				if (upEl) upEl.setTop(0).setWidth(100);
			},
			afterrender: function(grid) {
				grid.view.refresh();
			},
			selectionchange: function(selectionModel, selections, eOpts) {
				if (selections.length > 0) this.down('#removeButton').enable();
				else this.down('#removeButton').disable();
			},
			updateprogress: function(uploader, total, percent, sent, success, failed, queued, speed) {
				var uploading = false;
				Ext.each(uploader.uploader.files, function(file) {
					if (file.status != plupload.DONE) uploading = true;
					return false;
				});

				if (uploading) {
					var t = Ext.String.format(RS.formbuilder.locale.updateProgressText, percent, sent, total);
					this.statusBar.showBusy({
						text: t,
						clear: false
					});
					this.statusBar.isBusy = true;
				} else {
					this.statusBar.isBusy = false;
					this.statusBar.clearStatus();
					this.statusBar.setStatus({
						text: RS.formbuilder.locale.ready
					});
				}
			},
			uploadprogress: function(uploader, file, name, size, percent) {
				// me.statusBar.setText(name + ' ' + percent + '%');
			},
			uploadcomplete: function(uploader, success, failed) {
				this.statusBar.isBusy = false;
				this.statusBar.clearStatus();
				this.statusBar.setStatus({
					text: RS.formbuilder.locale.ready
				});
			}
		})
		if (me.fileServiceListUrl)
			me.loadFileUploads()
	},

	removeSelectedFiles: function(button) {
		if (button === 'yes') {
			var selections = this.getSelectionModel().getSelection(),
				ids = [],
				fileIds = [];
			Ext.each(selections, function(selection) {
				ids.push(selection.get('id'));
				fileIds.push(selection.get('id'));
			});
			Ext.each(fileIds, function(fileId) {
				this.uploader.removeFile(fileId)
			}, this);
			Ext.Ajax.request({
				url: '/resolve/service/catalogbuilder/deleteFile',
				params: {
					tableName: this.fileUploadTableName,
					ids: ids
				},
				scope: this,
				success: this.removeDeletedFiles
			})
		}
	},

	removeDeletedFiles: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			this.loadFileUploads();
		}
	},

	loadFileUploads: function() {
		Ext.Ajax.request({
			url: this.fileServiceListUrl,
			params: {
				sqlQuery: Ext.String.format("select * from {0} where {1}='{2}'", this.fileUploadTableName, this.referenceColumnName, this.recordId),
				type: 'table',
				useSql: true
			},
			success: this.loadFiles,
			scope: this
		})
	},

	loadFiles: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			if (response.records) {
				Ext.each(response.records, function(record) {
					Ext.apply(record, {
						id: record.id,
						name: record.filename,
						size: record.u_size
					})
				}, this);
				this.store.loadData(response.records, false);
			}
		}
	},

	/**
	 * @private
	 * /
	createUploader: function() {
		return Ext.create('RS.formbuilder.viewer.desktop.fields.PlUpload', this, Ext.applyIf({
			listeners: {}
		}, this));
	},

	setEnabled: function(enable) {
		if (enable) this.uploader.multipart_params.recordId = enable;
		enable = enable && !this.internalDisabled;
		var uploadButton = this.down('#uploadButton');
		uploadButton[enable ? 'enable' : 'disable']();
		uploadButton.ownerCt[enable ? 'enable' : 'disable']();
		if (this.uploader.uploader) this.uploader.uploader.disabled = !enable;
		this.view.emptyText = enable ? (this.uploader.uploader.features.dragdrop ? RS.formbuilder.locale.dragDropEnabledEmptyText : RS.formbuilder.locale.dragDropDisabledEmptyText) : RS.formbuilder.locale.noRecordIdForUpload;
		this.view.refresh();
		if (enable) this.uploader.uploader.refresh();
	},

	disable: function() {
		this.internalDisabled = true;
		this.setEnabled(false);
	},
	enable: function() {
		if (this.rendered) {
			this.internalDisabled = false;
			this.setEnabled(true);
		}
	},

	downloadURL: function(url) {
		var iframe;
		iframe = document.getElementById("hiddenDownloader");
		if (iframe === null) {
			iframe = document.createElement('iframe');
			iframe.id = "hiddenDownloader";
			iframe.style.visibility = 'hidden';
			document.body.appendChild(iframe);
		}
		iframe.src = url;
	},

	isUploading: function() {
		return this.statusBar.isBusy || false;
	}
});
*/