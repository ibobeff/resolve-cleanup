Ext.define('RS.catalog.ImagePickerField', {
	extend: 'Ext.form.field.Trigger',
	alias: 'widget.imagepickerfield',

	triggerBaseCls: 'x-icon-over icon-paper-clip icon-large',
	triggerCls: 'x-icon-over icon-paper-clip icon-large',

	initComponent: function() {
		var me = this;
		me.callParent()
		me.on('render', function() {
			me.getEl().on('click', function() {
				me.fireEvent('attach')
			})
		})
	},

	onTriggerClick: function() {
		this.fireEvent('attach', this)
	}
})