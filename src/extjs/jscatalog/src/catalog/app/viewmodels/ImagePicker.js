glu.defModel('RS.catalog.ImagePicker', {
	mixins: ['FileUploadManager'],
	api: {
		list: '/resolve/service/catalog/imageList',
		upload: '/resolve/service/catalogbuilder/upload'
	},

	allowedExtensions: ['png', 'jpg', 'jpeg', 'gif'],
	allowMultipleFiles: false,
	allowDownload: false,
	showUploadedBy: false,

	callback: '',

	initStore: function() {
		var store = Ext.create('Ext.data.Store', {
			fields: ['sys_id', 'id', {
				name: 'fileName',
				type: 'String',
				convert: function(v, r) {
					return r.raw.filename;
				}
			}, {
				name: 'size', 
				convert: function(v, r) {
					return r.raw.u_size;
				}
			}, {
				name: 'uploadedBy', 
				convert: function(v, r) {
					return r.raw.sysCreatedBy;
				}
			}, {
				name: 'sysUpdatedOn',
				type: 'date',
				dateFormat: 'time',
				format: 'time'
			}],
			sorters: [{
				property: 'fileName',
				direction: 'ASC'
			}],
			proxy: {
				type: 'ajax',
				url: this.api.list,
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});
		this.set('filesStore', store);
	},

	init: function() {
	},

	choose: function() { 
		if (this.parentVM[this.callback]) this.parentVM[this.callback](this.selections)
		this.doClose()
	},
	chooseIsEnabled$: function() {
		return this.selections.length == 1;
	},
	cancel: function() {
		this.doClose()
	},

	selectionChanged: function(selected, eOpts) {
		this.set('selections', selected)
	},
	selectRecord: function(record, item, index, e, eOpts) {
		this.set('selections', [record])
		this.choose()
	}

});

glu.defModel('RS.catalog.FileUploadManagerFactory', function(cfg) {
	Ext.applyIf(cfg, RS.common.viewmodels.FileUploadManager);
	return cfg;
});
