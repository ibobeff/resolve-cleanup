glu.defModel('RS.catalog.RenameCopyDialog', {
	rename: false,

	id: '',
	name: '',

	newName: '',

	names: {
		mtype: 'store',
		fields: ['name', 'id'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/catalog/names',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	title: '',

	init: function() {
		this.set('title', (this.rename ? this.localize('renameCatalog') : this.localize('copyCatalog')) + ' - ' + this.name)
		this.names.load()
	},

	newNameIsValid$: function() {
		if (this.newName.length == 0) return this.localize('newNameRequired')

		if (this.names.findRecord('name', this.newName, 0, false, false, true)) return this.localize('duplicateName')
		return true
	},

	newNameTbTextIsVisible$: function() {
		return this.newNameIsValid !== true
	},
	newNameTbText$: function() {
		return Ext.String.format('<font style="color:red">{0}</font>', this.newNameIsValid)
	},

	cancel: function() {
		this.doClose()
	},

	renameCatalog: function() {
		this.parentVM.reallyRenameCatalog(this.newName)
		this.doClose()
	},
	renameCatalogIsEnabled$: function() {
		return this.newNameIsValid === true
	},
	renameCatalogIsVisible$: function() {
		return this.rename
	},

	copyCatalog: function() {
		this.parentVM.reallyCopyCatalog(this.newName)
		this.doClose()
	},
	copyCatalogIsEnabled$: function() {
		return this.newNameIsValid === true
	},
	copyCatalogIsVisible$: function() {
		return !this.rename
	}
})