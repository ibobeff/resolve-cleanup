glu.defModel('RS.catalog.TagPicker', {

	callback: '',

	init: function() {
		this.tagStore.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {}

			var filter = [];
			if (this.searchTag) {
				filter = [{
					field: 'name',
					type: 'auto',
					condition: 'contains',
					value: this.searchTag
				}, {
					field: 'description',
					type: 'auto',
					condition: 'contains',
					value: this.searchTag
				}]
			}
			Ext.apply(operation.params, {
				operator: 'or',
				filter: Ext.encode(filter)
			})
		}, this)
		this.tagStore.load()
	},

	title$: function() {
		return this.activeCard == 0 ? this.localize('selectTag') : this.localize('createTag')
	},

	width$: function() {
		return this.activeCard == 0 ? 625 : 400
	},

	height$: function() {
		return this.activeCard == 0 ? 500 : 200
	},

	activeCard: 0,

	tagName: '',
	// tagNameIsValid$: function() {
	// 	var valid = (/^[a-zA-Z0-9_\-\/]+$/.test(this.tagName) ||
	// 		/^[a-zA-Z0-9_\-\/][a-zA-Z0-9_\-\/\s\.]*[a-zA-Z0-9_\-\/]$/.test(this.tagName)) && !/.*\s{2,}.*/.test(this.tagName) && !/.*(\.\.|\s+\.|\.\s+).*/.test(this.tagName);
	// 	return this.tagName && valid ? true : this.localize('invalidName');
	// },

	tagStore: {
		mtype: 'store',
		remoteSort: 'true',
		sorters: ['name'],
		fields: ['name'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/tag/listTags',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	searchTag: '',

	when_search_tag_changes_update_grid: {
		on: ['searchTagChanged'],
		action: function() {
			this.tagStore.loadPage(1)
		}
	},

	tagsSelections: [],

	tagColumns: [{
		header: '~~name~~',
		dataIndex: 'name',
		flex: 1,
		renderer: RS.common.grid.plainRenderer()
	}],

	select: function() {
		//Add the selected tags to the list of selected tags
		if (this.parentVM[this.callback]) this.parentVM[this.callback](this.tagsSelections)
		this.doClose()
	},
	selectIsVisible$: function() {
		return this.activeCard == 0
	},
	selectIsEnabled$: function() {
		return this.tagsSelections.length > 0
	},
	cancel: function() {
		if (this.activeCard == 1)
			this.set('activeCard', 0)
		else
			this.doClose()
	},
	create: function() {
		this.ajax({
			url: '/resolve/service/tag/saveTag',
			jsonData: {
				id: '',
				name: this.tagName,
				description: ''
			},
			success: function(r) {
				var respData = RS.common.parsePayload(r);
				if (!respData.success) {
					clientVM.displayError(this.localize('SaveTagErrMsg', {
						msg: respData.message
					}));
					return;
				}

				if (this.parentVM[this.callback]) this.parentVM[this.callback]([{
					data: {
						name: this.tagName,
						id: respData.data.id
					}
				}])
				this.doClose()
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	tagNameIsValid$: function() {
		this.isValid = /^[a-zA-Z\d\_]+$/.test(this.tagName) && !this.tagName.replace(/_/g, "").length == 0;
		return this.isValid || this.localize('invalidName')
	},
	createIsVisible$: function() {
		return this.activeCard == 1
	},
	createIsEnabled$: function() {
		return this.isValid
	},
	newTag: function() {
		this.set('activeCard', 1)
	},
	newTagIsVisible$: function() {
		return this.activeCard == 0
	},
	selectExistingTag: function() {
		this.set('activeCard', 0)
	},
	selectExistingTagIsVisible$: function() {
		return this.activeCard == 1
	},

	selectRecord: function(record, item, index, e, eOpts) {
		this.set('tagsSelections', [record])
		this.select()
	}
})