glu.defModel('RS.catalog.CatalogItem', {
	id: '',
	type: 'CatalogItem',
	initialized: false,
	csrftoken: '',

	fields: ['id', 'name', 'title', 'description', 'image', 'imageName', 'tooltip', 'wiki', 'link', 'displayType', 'type', 'leaf', 'internalName', 'icon', 'maxImageWidth', 'path', 'openInNewTab'],
	name: '',
	when_name_changes_update_title_automatically: {
		on: ['nameChanged'],
		action: function() {
			this.set('title', this.name);
		}
	},
	internalName$: function() {
		return this.name
	},
	title: '',
	maxImageWidth: 0,
	maxImageWidthIsVisible$: function() {
		return this.displayType == 'listOfDocuments'
	},
	description: '',
	descriptionIsVisible$: function() {
		return !this.rootVM.isTrainingCatalog
	},
	image: '',
	imageIsVisible$: function() {
		return !this.rootVM.isTrainingCatalog
	},
	icon$: function() {
		return this.image ? this.image + '&' + this.csrftoken : '';
	},
	imageName: '',
	tooltip: '',
	wiki: '',
	wikiIsVisible$: function() {
		return this.displayType == 'wiki'
	},
	link: 'http://',
	linkIsVisible$: function() {
		return this.displayType == 'url'
	},
	path: '',
	leaf: true,

	displayType: 'wiki',

	openInNewTab: true,
	openInNewTabIsVisible$: function() {
		var types = ['wiki', 'url', 'listOfDocuments']
		if (this.rootVM.isTrainingCatalog) types = ['listOfDocuments']
		return Ext.Array.indexOf(types, this.displayType) > -1
	},

	displayTypeStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	docs: [],
	tags: [],

	selectedTags: {
		mtype: 'list'
	},

	selectedWikis: {
		mtype: 'list'
	},

	tagsIsVisible$: function() {
		return !this.rootVM.isTrainingCatalog
	},

	selectedTagStore: {
		mtype: 'store',
		fields: ['name'],
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'selectedTags'
		}]
	},

	selectedTagColumns: [{
		header: '~~name~~',
		dataIndex: 'name',
		flex: 1
	}],

	selectedWikiStore: {
		mtype: 'store',
		fields: ['fullname'],
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'selectedWikis'
		}]
	},

	selectedWikiColumns: [{
		header: '~~fullname~~',
		dataIndex: 'fullname',
		flex: 1
	}],

	init: function() {
		this.initToken();
		if (!this.id) this.set('id', Ext.id())
		if (!this.name) this.set('name', this.localize('newCatalogItem'))
		Ext.defer(function() {
			this.set('path', this.rootVM.getPath(this))
		}, 100, this)

		this.displayTypeStore.add([{
			name: this.localize('wiki'),
			value: 'wiki'
		}, {
			name: this.localize('url'),
			value: 'url'
		}, {
			name: this.localize('listOfDocuments'),
			value: 'listOfDocuments'
		}])

		this.selectedTags.on('lengthchanged', function() {
			var t = [];
			this.selectedTags.foreach(function(tag) {
				t.push(tag.id)
			})
			this.set('tags', t)
		}, this)

		this.selectedWikis.on('lengthchanged', function() {
			var w = [];
			this.selectedWikis.foreach(function(wiki) {
				w.push(wiki.id)
			})
			this.set('docs', w)
		}, this)

		//parse any initialized tags
		var t = this.tags
		Ext.Array.forEach(t, function(tag) {
			this.selectedTags.add({
				id: tag.id,
				name: tag.name
			})
		}, this)

		var w = this.docs
		Ext.Array.forEach(w, function(doc) {
			this.selectedWikis.add({
				id: doc.id,
				fullname: doc.ufullname
			})
		}, this)

		this.displayTypeStore.sort('name', 'ASC')
		this.set('initialized', true)
	},

	activate: function() {
		this.initToken();
	},

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/catalog/download', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this))
	},

	when_name_changes_update_tree_and_path: {
		on: ['nameChanged', 'descriptionChanged', 'imageChanged', 'titleChanged'],
		action: function() {
			if (this.initialized) this.rootVM.updateName({
					name: this.name,
					title: this.title,
					description: this.description,
					icon: this.image
				})
				// this.set('path', this.rootVM.getPath(this))
		}
	},

	attach: function() {
		this.open({
			mtype: 'RS.catalog.ImagePicker',
			callback: 'chooseImage'
		})
	},

	chooseImage: function(data) {
		if (data.length > 0) {
			this.set('image', Ext.String.format('/resolve/service/catalog/download?id={0}&{1}', data[0].get('sys_id'), this.csrftoken))
			this.set('imageName', data[0].get('name') || data[0].get('fileName'))
		}
	},

	tagsIsVisible$: function() {
		return this.displayType == 'listOfDocuments'
	},

	wikisIsVisible$: function() {
		return this.displayType == 'listOfDocuments'
	},

	tagsSelections: [],

	addTag: function() {
		this.open({
			mtype: 'RS.catalog.TagPicker',
			callback: 'addTags'
		})
	},
	addTags: function(newTags) {
		var tagName = '',
			tagId = '',
			len = this.selectedTags.length,
			i, contains = false;

		Ext.Array.forEach(newTags, function(tag) {
			tagName = tag.data['name']
			tagId = tag.data['id']
			for (i = 0; i < len && !contains; i++) {
				if (this.selectedTags.getAt(i).name == tagName) contains = true
			}
			if (!contains) {
				this.selectedTags.add({
					name: tagName,
					id: tagId
				})
			}
		}, this)
	},
	removeTag: function() {
		//remove the selected tags
		var len = this.selectedTags.length,
			i, name

		Ext.Array.forEach(this.tagsSelections, function(tag) {
			name = tag.data['name']
			for (i = 0; i < len; i++) {
				if (this.selectedTags.getAt(i).name == name) {
					this.selectedTags.removeAt(i)
					i--;
					len = this.selectedTags.length
				}
			}
		}, this)
	},
	removeTagIsEnabled$: function() {
		return this.tagsSelections.length > 0
	},
	wikisSelections: [],
	addWiki: function() {
		this.open({
			mtype: 'RS.common.WikiDocSearch',
			dumper: {
				dump: function(records) {
					var fullname = '',
						wikiId = '',
						len = this.selectedWikis.length,
						i, contains = false;

					Ext.Array.forEach(records || [], function(wiki) {
						fullname = wiki.data['ufullname']
						wikiId = wiki.data['id']
						for (i = 0; i < len && !contains; i++) {
							if (this.selectedWikis.getAt(i).fullname == fullname) contains = true
						}
						if (!contains) {
							this.selectedWikis.add({
								fullname: fullname,
								id: wikiId
							})
						}
					}, this)
					return true;
				},
				scope: this
			}
		});
	},
	removeWiki: function() {
		//remove the selected tags
		var len = this.selectedWikis.length,
			i, fullname

		Ext.Array.forEach(this.wikisSelections, function(wiki) {
			fullname = wiki.data['fullname']
			for (i = 0; i < len; i++) {
				if (this.selectedWikis.getAt(i).fullname == fullname) {
					this.selectedWikis.removeAt(i)
					i--;
					len = this.selectedWikis.length
				}
			}
		}, this)
	},
	removeWikiIsEnabled$: function() {
		return this.wikisSelections.length > 0;
	},
	searchWiki: function() {
		this.open({
			mtype: 'RS.common.WikiDocSearch',
			dumper: {
				dump: function(records) {
					Ext.each(records, function(r) {
						this.set('wiki', r.get('ufullname'))
					}, this)
					return true;
				},
				scope: this
			},
			single: true
		}, 'Single');
	}
})