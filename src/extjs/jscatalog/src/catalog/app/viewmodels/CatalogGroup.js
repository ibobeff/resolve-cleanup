glu.defModel('RS.catalog.CatalogGroup', {
	id: '',
	type: 'CatalogGroup',
	initialized: false,

	fields: ['name', 'id', 'type', 'expanded', 'internalName'],
	internalName$: function() {
		return this.name
	},
	name: '',
	expanded: true,


	init: function() {
		if (!this.id) this.set('id', Ext.id())
		if (!this.name) this.set('name', this.localize('newCatalogGroup'))
		this.set('initialized', true)
	},

	when_name_changes_update_tree: {
		on: ['nameChanged'],
		action: function() {
			if (this.initialized) this.parentVM.updateName({
				name: this.name
			})
		}
	}
})