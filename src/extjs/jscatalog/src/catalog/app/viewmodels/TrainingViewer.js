glu.defModel('RS.catalog.TrainingViewer', {
	mock: false,

	id: '',

	mode: '',
	wikiUrl$: function() {
		return this.mode == 'edit' ? '/resolve/jsp/rsclient.jsp?' + this.rsclientToken + '&displayClient=false#RS.wiki.Main/name=' : '/resolve/service/wiki/view'
	},

	activeCard: 0,

	sideWidth: 300,

	catalogReferenceStore: {
		mtype: 'store',
		fields: ['name', 'id'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/catalog/names',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	catalogTreeStore: {
		mtype: 'treestore',
		fields: ['id', 'name', 'type', 'description', 'icon', 'catalogId', 'path', 'tags', {
			name: 'maxImageWidth',
			type: 'int'
		}, {
			name: 'openInNewTab',
			type: 'bool'
		}, {
			name: 'unique',
			convert: function() {
				this.uniqueCount = this.uniqueCount || 0;
				return this.uniqueCount++;
			}
		}],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json',
				idProperty: 'unique'
			}
		}
	},

	catalogItems: {
		mtype: 'activatorlist',
		focusProperty: 'activeItem'
	},

	activeItem: -1,
	activeNode: null,

	catalogTreeSelected: null,
	catalogNodeSelected: null,

	fields: ['name', 'wiki'],
	wiki: '',
	name: '',
	nameClip$: function() {
		return Ext.util.Format.ellipsis(this.name, 20)
	},

	loaded: false,

	activate: function(screen, params) {		
		this.initToken();
		if (params && params.mode) {
			this.set('mode', params.mode)
		}

		if (params && params.id) {
			this.set('id', params.id)
		}

		if (params && params.activeNode) {
			this.set('activeNode', params.activeNode)
			this.catalogTreeStore.getRootNode().getOwnerTree().selectPath(params.activeNode, 'name')
			var node = this.catalogTreeStore.getRootNode().findChildBy(function(child) {
				return child.getPath('name') == this.activeNode
			}, this)
			if (node) {
				this.selectNode(node)
			}
		}

		if (!this.loaded)
			this.loadCatalog()
	},

	when_id_changes_load_catalog: {
		on: ['idChanged'],
		action: function() {
			this.loadCatalog()
		}
	},

	when_name_changes_update_window_title: {
		on: ['nameChanged'],
		action: function() {
			clientVM.setWindowTitle(this.name + ' - ' + this.localize('catalog'))
		}
	},

	init: function() {
		this.initToken();
		if (this.mock) this.backend = RS.catalog.createMockBackend(true)

		var root = this.model({
			mtype: 'CatalogReference',
			isRoot: true,
			name: this.localize('newCatalog')
		})
		this.catalogItems.add(root)
		this.catalogTreeStore.setRootNode({
			name: this.localize('newCatalog'),
			id: root.id,
			type: root.type,
			expanded: true
		})
		this.catalogTreeStore.on('load', this.treeStoreLoaded, this)
		this.set('activeItem', this.catalogItems.getAt(0))
		this.set('catalogTreeSelected', this.catalogTreeStore.getRootNode())

		this.listOfDocuments.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				path: this.path,
				tags: Ext.isArray(this.tags) ? this.tags.join(',') : this.tags
			})
		}, this)

		// this.loadCatalog()
	},

	wikiViewToken: '',
	rsclientToken: '',

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/wiki/view', function(token_pair) {
			this.set('wikiViewToken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this));
		this.set('rsclientToken', clientVM.rsclientToken);
	},

	path: '',
	tags: [],

	treeStoreLoaded: function() {
		if (!this.activeNode) {
			var node = this.catalogTreeStore.getRootNode()
			if (node && node.childNodes.length > 0) {
				Ext.defer(function() {
					node.getOwnerTree().getSelectionModel().select(node.childNodes[0])
				}, 100)
			}
		}
	},

	loadCatalog: function() {
		if (this.id) {
			this.ajax({
				url: '/resolve/service/catalog/get',
				params: {
					id: this.id
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.set('loaded', true)
						if (response.data.catalogType != 'training') this.message({
								title: this.localize('wrongCatalogTypeTitle'),
								msg: this.localize('wrongCatalogTypeMessage'),
								buttons: Ext.MessageBox.OK
							})
							//Load the fields for the catalog
							//this is a temporary hack...id and parentSysId shouldn't be used in such a way,from a grid the value of id
						var data = response.data;
						if (data.parentSysId)
							data.id = data.parentSysId;
						this.loadData(data);
						//load sideWidth manually to get the real number
						this.set('sideWidth', response.data.sideWidth || this.sideWidth)

						//set the root nodes path properly
						this.catalogTreeStore.getRootNode().set({
							name: response.data.name
						})

						//load in the children
						this.set('catalogTreeSelected', this.catalogTreeStore.getRootNode())
						this.catalogTreeStore.getRootNode().removeAll()
						this.loadChildren(response.data.children, this.catalogTreeStore.getRootNode())
						//Check to verify this activeNode is actually contains path from this catalog (This activeNode could be carried over from previous catalog).
						if (this.activeNode && this.activeNode.indexOf(data.path) != -1) {
							this.catalogTreeStore.getRootNode().getOwnerTree().selectPath(this.activeNode, 'name', null, function(success, node) {
								if (!success && node) this.catalogTreeStore.getRootNode().getOwnerTree().getSelectionModel().select(node)
								else this.catalogTreeStore.getRootNode().getOwnerTree().getSelectionModel().selectNext()
							}, this)

							var node = this.catalogTreeStore.getRootNode().findChildBy(function(child) {
								return child.getPath('name') == this.activeNode
							}, this)
							if (node) {
								this.selectNode(node)
							}
						} else {
							//Select first node
							this.set('activeNode', null);
							if (this.catalogTreeStore.getRootNode().childNodes.length > 0) {
								this.catalogTreeStore.getRootNode().getOwnerTree().getSelectionModel().select(this.catalogTreeStore.getRootNode().childNodes[0])
							}
						}
					} else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
	},

	loadReference: function(node, referenceId) {
		this.ajax({
			url: '/resolve/service/catalog/get',
			params: {
				id: referenceId
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.loadChildren(response.data.children, node)
				} else clientVM.displayError(response.message)
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	loadChildren: function(children, node) {
		Ext.Array.forEach(children || [], function(child) {
			var n = this.addToCatalog(this.model(Ext.apply(child, {
				mtype: child.type,
				expanded: false
			})), node, false)
			if (child.type == 'CatalogReference' && !child.root)
				return
			this.loadChildren(child.children, n)
		}, this)
	},

	catalogSelectionChanged: function(selected, eOpts) {
		if (selected.length > 0) {
			var id = selected[0].internalId
			this.set('catalogTreeSelected', selected[0])
			this.catalogItems.foreach(function(item, index) {
					if (item.id == id) {
						this.set('activeItem', index)
						return false
					}
				}, this)
				//Switch to selected wiki document
				// this.set('selectedWiki', selected[0].raw.wiki || '')
			if (!clientVM.activeScreen.sticky) {
				Ext.History.back()
				Ext.defer(function() {
					clientVM.handleNavigation({
						modelName: 'RS.catalog.TrainingViewer',
						params: {
							id: this.id,
							activeNode: selected[0].getPath('name'),
							mode: this.mode
						},
						sticky: true
					})
				}, 10, this)
			} else {
				clientVM.handleNavigation({
					modelName: 'RS.catalog.TrainingViewer',
					params: {
						id: this.id,
						activeNode: selected[0].getPath('name'),
						mode: this.mode
					},
					sticky: true
				})
			}
		}
	},

	addToCatalog: function(model, parent, noSelect) {
		this.catalogItems.add(model)
		var temp = Ext.apply({
			qtip: model.tooltip || model.name
		}, model.asObject());
		temp.expanded = temp.expanded == 'true'
		if (temp.type == 'CatalogReference') {
			temp.leaf = false
			if (!temp.isRoot) return
		}
		var node = (parent || this.catalogTreeSelected).appendChild(temp)
		if (noSelect !== false) this.selectNode(node)
		return node;
	},

	selectNode: function(node) {
		this.set('catalogTreeSelected', node)
		if (this.catalogTreeSelected.getOwnerTree()) Ext.defer(function() {
			this.catalogTreeSelected.getOwnerTree().getSelectionModel().select(this.catalogTreeSelected)
		}, 100, this)
	},

	getPath: function(catalogItem) {
		var id = catalogItem.id,
			node = this.catalogTreeStore.getNodeById(id)
		if (node) return node.getPath('name')
	},

	when_catalogTreeSelected_changes_update_display: {
		on: ['catalogTreeSelectedChanged'],
		action: function() {
			this.set('catalogNodeSelected', this.catalogItems.find(function(item) {
				return item.id == this.catalogTreeSelected.data.id
			}, this))
			if (this.catalogTreeSelected.raw.displayType == 'listOfDocuments' && this.catalogNodeSelected) {
				this.set('activeCard', 1)
				this.set('path', this.catalogNodeSelected.path)
				this.set('tags', this.catalogNodeSelected.tags)
				this.set('listOfDocumentsMaxImageWidth', this.catalogNodeSelected.maxImageWidth || 0)
				this.set('listOfDocumentsOpenTarget', this.catalogNodeSelected.openInNewTab)
				this.listOfDocuments.load()
			} else this.set('activeCard', 0)
		}
	},

	selectedWiki: '',
	selectedWikiUrl$: function() {
		var newNode = this.catalogTreeSelected;
		if (newNode && Ext.Array.indexOf(['CatalogFolder', 'CatalogItem'], newNode.get('type')) > -1) {
			switch (newNode.raw.displayType) {
				case 'url':
					return Ext.String.format('<iframe class="rs_iframe" src="{0}" style="border:0px;width:100%;height:100%"></iframe>', newNode.raw.link)

				case 'wiki':
					var wikiSrc = '';
					if (this.wikiUrl.indexOf('/resolve/jsp/rsclient.jsp') != -1) {
						wikiSrc = this.wikiUrl + newNode.raw.wiki;
					} else {
						wikiSrc = this.wikiUrl + '?wiki=' + newNode.raw.wiki + '&' + this.wikiViewToken;
					}
					return Ext.String.format('<iframe class="rs_iframe" src="{0}" style="border:0px;width:100%;height:100%"></iframe>', wikiSrc);

				case 'listOfDocuments':
					return ''
			}
		}
		return ''
	},

	itemClick: function(record, item, index, eOpts) {
		// if (!record.isLeaf() && record.isExpanded()) {
		// 	Ext.defer(function() {
		// 		this.catalogTreeStore.getRootNode().getOwnerTree().getSelectionModel().deselect(record)
		// 	}, 1, this)
		// }
		// record[record.isExpanded() ? 'collapse' : 'expand']();
		if (!record.isExpanded()) {
			if (record.get('type') == 'CatalogReference') {
				record.removeAll()
				this.loadReference(record, record.get('catalogId'))
			}
			record.expand()
		} else if (Ext.EventObject.getTarget('img')) {
			Ext.defer(function() {
				this.catalogTreeStore.getRootNode().getOwnerTree().getSelectionModel().deselect(record)
			}, 1, this)
			record.collapse()
		}
	},

	listOfDocumentsMaxImageWidth: 0,

	listOfDocuments: {
		mtype: 'store',
		fields: ['id', 'utitle', 'usummary', 'attachments', 'ufullname'],
		// buffered: true,
		// leadingBufferZone: 300,
		pageSize: 100,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/catalog/listOfDocuments',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	listOfDocumentsOpenTarget: false,

	documentClicked: function(record, item, index, e, eOpts) {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			params: {
				name: record.get('ufullname')
					// location: '/resolve/service/wiki/view/' + record.get('ufullname').replace(/[.]/g, '/') + '?IS_CATALOG=true'
			},
			target: this.listOfDocumentsOpenTarget ? '_blank' : '_self'
		})
	}
})