glu.defModel('RS.catalog.CatalogViewer', {
	mock: false,

	id: '',

	active: false,

	csrftoken: '',

	activate: function(screen, params) {
		this.initToken();
		if (params.id && params.id != this.id) {
			this.catalogs.removeAll()
			this.toolbar.removeAll()
			this.set('id', params.id)
			this.loadCatalog(params.id)
			return
		}

		var nodeId = params.activeNode || this.id

		if (nodeId == this.activeNode) return

		//Find the toolbar index and if its back then remove to that point for the root node
		var btn = null
		this.toolbar.each(function(button) {
			if (button.nodeId == nodeId) {
				btn = button
				return false
			}
		}, this)

		if (btn) this.toolbarButtonClick(btn)
		if (params.activeNode) {
			var node = null
			this.set('activeNode', params.activeNode)
			this.catalogs.foreach(function(catalog) {
				temp = this.findNode(nodeId, catalog)
				if (temp) node = temp
			}, this)
			if (node) {
				this.nodeClickHandler(node.id, true)
			}
		}

		this.set('active', true)
	},

	name: '',
	when_name_changes_update_window_title: {
		on: ['nameChanged'],
		action: function() {
			clientVM.setWindowTitle(this.name + ' - ' + this.localize('catalog'))
		}
	},

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/catalog/download', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this))
	},

	decactivate: function() {
		this.set('active', false)
	},

	init: function() {
		this.initToken();
		if (this.mock) this.backend = RS.catalog.createMockBackend(true)
		if (this.id) this.loadCatalog(this.id)
		this.listOfDocuments.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				path: this.path,
				tags: Ext.isArray(this.tags) ? this.tags.join(',') : this.tags
			})
		}, this)
	},

	activeCard: 0,
	activeNode: null,

	path: '',
	tags: '',

	catalogStore: {
		mtype: 'store',
		fields: ['id', 'title', 'items']
	},

	toolbar: {
		mtype: 'list'
	},

	catalogs: {
		mtype: 'list'
	},

	catalog: null,
	when_catalog_changes_ensure_data: {
		on: ['catalogChanged'],
		action: function() {
			this.massageChildren(this.catalog.children)
		}
	},
	massageChildren: function(children) {
		Ext.each(children, function(child) {
			child.title = child.title || child.name
			child.desc = child.description
			if (child.imageName) {
				var urlParts = child.image.split('?');
				if(urlParts.length == 2){
					var url = urlParts[0] + '?' + this.csrftoken + '&' + urlParts[1];
					child.image = url;
					child.icon = url;
				}
			}
			this.massageChildren(child.children)
			child.items = child.children
		}, this)
	},

	loadCatalog: function(id) {
		//See if we already have the catalog we're looking for
		var alreadyHave = false
		this.catalogs.foreach(function(catalog) {
			if (catalog.id == id) {
				this.set('catalog', catalog)
				alreadyHave = true
				return false
			}
		}, this)
		if (alreadyHave) this.processLoadCatalog()
		else this.ajax({
			url: '/resolve/service/catalog/get',
			params: {
				id: id
			},
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					if (response.data.catalogType == 'training') this.message({
							title: this.localize('wrongCatalogTypeTitle'),
							msg: this.localize('wrongCatalogTypeMessage'),
							buttons: Ext.MessageBox.OK
						})
						//load up the data into the store to be drawn on the page
					this.set('name', response.data.name)
					this.catalogs.add(response.data)
					this.set('catalog', response.data)
					this.processLoadCatalog()
				} else clientVM.displayError(response.message)
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	processLoadCatalog: function() {
		if ((this.catalog.title || this.catalog.name) && this.active) window.document.title = (this.catalog.title || this.catalog.name) + ' - ' + this.localize('catalog')
		this.addToolbarItem(this.catalog.title || this.catalog.name, this.catalog.id)
		this.catalogStore.removeAll();
		this.catalogStore.add(this.catalog.children)
			// if (this.activeNode) this.nodeClickHandler(this.activeNode)
		if (this.activeNode) {
			this.selectNodeByPath(this.activeNode)
		}
	},

	nodeClickHandler: function(nodeId, internal) {
		var node, temp
		this.catalogs.foreach(function(catalog) {
			temp = this.findNode(nodeId, catalog)
			if (temp) node = temp
		}, this)

		if (node) {
			switch (node.type) {
				case 'CatalogFolder':
					if (internal !== true) clientVM.handleNavigation({
						modelName: 'RS.catalog.CatalogViewer',
						params: {
							id: this.id,
							activeNode: node.id
						},
						sticky: true
					})
					else {
						this.catalogStore.loadData(node.children)
						this.addToolbarItem(node.title || node.name, node.id)
					}
					break;

				case 'CatalogReference':
					this.set('activeCard', 0)
						//go get the new catalog root
					if (internal !== true) clientVM.handleNavigation({
						modelName: 'RS.catalog.CatalogViewer',
						params: {
							id: this.id,
							activeNode: node.root ? node.id : node.children[0].id
						},
						sticky: true
					})
					else {
						if (node.root) this.loadCatalog(node.id)
						else this.loadCatalog(node.children[0].id)
					}
					return;
					break;

				case 'CatalogItem':
					break;
			}

			this.set('activeCard', 0)

			switch (node.displayType) {
				case 'wiki':
					clientVM.handleNavigation({
						modelName: 'RS.wiki.Main',
						params: {
							name: node.wiki
								// location: '/resolve/service/wiki/view/' + node.wiki.split('.').join('/') + '?IS_CATALOG=true'
						},
						target: node.openInNewTab ? '_blank' : '_self'
					})
					break;

				case 'url':
					clientVM.handleNavigation({
						modelName: 'RS.client.URLScreen',
						params: {
							location: node.link
						},
						target: node.openInNewTab ? '_blank' : '_self'
					})
					break;

				case 'form':
					// debugger;
					break;

				case 'listOfDocuments':
					if (internal !== true) clientVM.handleNavigation({
						modelName: 'RS.catalog.CatalogViewer',
						params: {
							id: this.id,
							activeNode: node.id
						},
						target: node.openInNewTab ? '_blank' : '_self'
					})
					else {
						this.addToolbarItem(node.title || node.name, node.id)
						this.set('activeCard', 1)
						this.set('path', node.path)
						this.set('tags', node.tags)
						this.set('listOfDocumentsMaxImageWidth', node.maxImageWidth || 0)
						this.set('listOfDocumentsOpenInNewTab', node.openInNewTab)
						this.listOfDocuments.load()
					}
					break;
			}
		}
		return false
	},

	findNode: function(nodeId, node) {
		if (nodeId == node.id) return node
		var foundNode = null
		Ext.each(node.children, function(child) {
			if (child.id == nodeId) {
				foundNode = child
				return false
			}
			foundNode = this.findNode(nodeId, child)
			if (foundNode) return false
		}, this)
		return foundNode
	},

	addToolbarItem: function(text, id) {
		if (this.toolbar.length > 0) {
			this.toolbar.add({
				mtype: 'viewmodel',
				xtype: 'tbtext',
				text: '>',
				nodeId: '',
				toolbarButtonClick: Ext.emptyFn
			})
		}
		this.toolbar.add({
			mtype: 'viewmodel',
			text: text,
			nodeId: id,
			toolbarButtonClick: function() {
				this.parentVM.toolbarButtonClick(this)
			}
		})
	},
	toolbarButtonClick: function(button) {
		if (button.nodeId != this.activeNode) {
			var index = this.toolbar.indexOf(button)
			if (index > 0) index--; //to remove the separator
			while (index < this.toolbar.length) {
				this.toolbar.removeAt(index)
			}
			this.nodeClickHandler(button.nodeId)
		}
	},

	listOfDocumentsMaxImageWidth: 0,

	listOfDocuments: {
		mtype: 'store',
		fields: ['id', 'utitle', 'usummary', 'attachments', 'ufullname'],
		// buffered: true,
		// leadingBufferZone: 300,
		pageSize: 100,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/catalog/listOfDocuments',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	listOfDocumentsOpenInNewTab: false,

	documentClicked: function(record, item, index, e, eOpts) {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			params: {
				name: record.get('ufullname')
					// location: '/resolve/service/wiki/view/' + record.get('ufullname').replace(/[.]/g, '/') + '?IS_CATALOG=true'
			},
			target: this.listOfDocumentsOpenTarget ? '_blank' : '_self'
		})
	},

	findChildByName: function(key, node) {
		if (key == node.name) return node
		var foundNode = null
		Ext.each(node.children, function(child) {
			if (child.name == key) {
				foundNode = child
				return false
			}
		}, this)
		return foundNode
	},

	selectNodeByPath: function(path, field, separator, callback, scope) {
		var me = this,
			root,
			keys,
			last;

		field = field || 'id';
		separator = separator || '/';

		keys = path.split(separator);
		last = keys.pop();
		if (keys.length > 1) {
			this.catalogs.forEach(function(catalog) {
				if (catalog.name == keys[1]) {
					keys.splice(1, 1)
					me.expandPath(catalog, keys.join(separator), field, separator, function(success, node) {
						var lastNode = node;
						if (success && node) {
							node = this.findChildByName(last, lastNode);
							if (node) {
								me.nodeClickHandler(node.id, true)
								Ext.callback(callback, scope || me, [true, node]);
								return;
							}
						}
						Ext.callback(callback, scope || me, [false, lastNode]);
					}, me);
					return false
				}
			})
		}
	},

	expandPath: function(node, path, field, separator, callback, scope) {
		var me = this,
			index = 1,
			keys,
			expander;

		field = field || 'id';
		separator = separator || '/';

		keys = path.split(separator);
		if (keys.length > 1) {
			Ext.Array.forEach(node.children, function(child) {
				if (child.name == keys[1]) {
					if (child.type != 'CatalogGroup') {
						me.nodeClickHandler(child.id, true)
					}
					keys.splice(1, 1)
					me.expandPath(child, keys.join(separator), field, separator, callback, scope)
				}
			})
		} else {
			Ext.callback(callback, scope || me, [true, node])
		}
	}
})

glu.regAdapter('catalogToolbarButton', {
	beforeCreate: function(config) {
		if (config.text == '>') config.xtype = 'tbtext'
	}
})