glu.defModel('RS.catalog.Catalog', {

	mock: false,

	active: false,

	wasChanged: false,
	activate: function(sceen, params) {
		this.set('wasChanged', false)
		this.set('active', true)
		this.set('name', '')
		this.set('catalogType', params ? params.catalogType : '')
		this.set('id', params ? params.id : '')
		this.setupCatalog();
		this.set('wasChanged', false)
	},

	deactivate: function() {
		this.set('active', false)
	},

	id: '',

	catalogTitle$: function() {
		clientVM.setWindowTitle(this.localize('catalogBuilder') + (this.name ? ' - ' + this.name : ''))
		return this.localize('catalogBuilder') + (this.name ? ' - ' + this.name : '')
	},

	catalogReferenceStore: {
		mtype: 'store',
		fields: ['name', 'id', 'catalogType'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/catalog/refnames',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	formReferenceStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getformnames',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	when_name_changes_update_window_and_root: {
		on: ['nameChanged'],
		action: function() {
			if (this.active) this.updateWindowTitle()
			var root = this.catalogTreeStore.getRootNode()
			if (root) {
				root.set({
					name: Ext.String.htmlEncode(this.name),
					internalName: Ext.String.htmlEncode(this.name)
				})
				root.commit()
				this.updatePaths()
			}
		}
	},

	updateWindowTitle: function() {
		clientVM.setWindowTitle(this.name + ' - ' + this.localize('catalogBuilder'));
	},
	//refers to the node that's currently selected, represented by an Ext component called a nodeInterface: http://docs.sencha.com/extjs/4.2.2/#!/api/Ext.data.NodeInterface
	catalogTreeSelected: null,
	//points to the sibling nodes of the catalogTreeSelected. Watched by a formula which updates the state of the move buttons
	catalogTreeSelectedPrevSibling: null,
	catalogTreeSelectedNextSibling: null,
	//this is a glu convention known as a formula. a property containing the return value of this function is available with the same name sans the dollar sign. 
	//The property "catalogTreeSelectedType" will automatically be updated with a new value internally by glu by running this function.
	//This will be triggered whenever the properties used in the formula are modified (in this case: catalogTreeSelected)
	//This particular formula is useful for determining the type string of the currently selected node in the catalog tree.
	catalogTreeSelectedType$: function() {
		return (this.catalogTreeSelected) ? this.catalogTreeSelected['data'].type : null;
	},
	//these "is" functions return a boolean telling you if a type of node is selected or not. Useful for logic that depends on the currently selected node.
	//ParentCatalog is the current parent catalog, or container catalog. ChildCatalog is a catalog reference that would have been added to a group or folder, which points to another Catalog.
	isParentCatalogSelected$: function() {
		return this.catalogTreeSelectedType === 'CatalogReference' && this.catalogTreeSelected.isRoot();
	},
	isChildCatalogSelected$: function() {
		return this.catalogTreeSelectedType === 'CatalogReference' && !this.catalogTreeSelected.isRoot();
	},
	isGroupSelected$: function() {
		return this.catalogTreeSelectedType === 'CatalogGroup';
	},
	isFolderSelected$: function() {
		return this.catalogTreeSelectedType === 'CatalogFolder';
	},
	isItemSelected$: function() {
		return this.catalogTreeSelectedType === 'CatalogItem';
	},
	isAnythingSelected$: function() {
		return !!this.catalogTreeSelected;
	},
	catalogTreeStore: {
		mtype: 'treestore',
		fields: ['id', 'name', 'type', 'description', 'icon', 'internalName'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	//fields
	fields: ['name', 'sideWidth', 'namespace', 'wiki', 'catalogType', 'sys_created_on', 'sys_created_by', 'sys_updated_on', 'sys_updated_by', 'sysOrganizationName'],
	wiki: '',
	name: '',
	namespace: '',
	catalogType: '',
	sideWidth: 300,

	sys_created_on: '',
	sys_updated_on: '',
	sys_created_by: '',
	sys_updated_by: '',
	sysOrganizationName: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	isTrainingCatalog$: function() { //if catalogtype is 'training', it's a side Catalog, otherwise it's a tile catalog
		return this.catalogType == 'training'
	},

	catalogItems: {
		mtype: 'activatorlist',
		focusProperty: 'activeItem',
		autoParent: true
	},

	activeItem: -1,

	init: function() {
		if (this.mock) this.backend = RS.catalog.createMockBackend(true)
		this.set('name', this.localize('newCatalog'))
		var me = this
		this.catalogReferenceStore.filter({
			filterFn: function(item) {
				//really really need to use a decent field name for this catalogType
				return !(item.get('id') == me.id || (item.get('catalogType') || '') != me.catalogType)
			}
		})
	},

	resetCatalog: function() {
		var root = this.model({
			mtype: 'CatalogReference',
			isRoot: true,
			nameIsReadOnly: false,
			name: this.localize('newCatalog')
		});
		this.catalogItems.removeAll();
		this.catalogItems.add(root);
		this.catalogTreeStore.setRootNode({
			name: this.localize('newCatalog'),
			internalName: this.localize('newCatalog'),
			id: root.id,
			type: root.type,
			expanded: true
		});
		this.set('name', this.localize('newCatalog'));
		this.set('namespace', '');
		this.set('activeItem', this.catalogItems.getAt(0));
		this.updateSelectedNode(this.catalogTreeStore.getRootNode());

		//reset sys properties
		this.set('sys_updated_by', '');
		this.set('sys_updated_on', '');
		this.set('sys_created_by', '');
		this.set('sys_created_on', '');

		this.formReferenceStore.load();
	},

	setupCatalog: function() {
		this.resetCatalog();
		if (this.id) this.loadCatalog()
		else {
			this.catalogReferenceStore.load({
					params: {
						catalogId: '',
						catalogName: ''
					}
				})
				//Configure some default information for the catalog
			if (!this.isTrainingCatalog) this.addGroup()
			this.addItem()
		}
	},

	maybePopulateCat: null,

	reloadCatRefStoreAndThenPopulateCat: function(catalog) {
		this.catalogReferenceStore.load({
			params: {
				catalogId: this.id,
				catalogName: ''
			}
		});
		//Load the fields for the catalog
		this.loadData(catalog)
			//this is a temporary hack...id and parentSysId shouldn't be used in such a way,from a grid the value of id
		this.set('id', catalog.parentSysId)
		this.updateWindowTitle()

		//restore the sideWidth on the catalog
		this.catalogItems.getAt(0).set('sideWidth', catalog.sideWidth || 300)
		this.catalogItems.getAt(0).set('wiki', catalog.wiki || '')
		this.catalogItems.getAt(0).set('link', catalog.link || 'http://')
		this.catalogItems.getAt(0).set('displayType', catalog.displayType || 'wiki')
		this.catalogItems.getAt(0).set('nameIsReadOnly', true)
			//restore the tags on the root
		Ext.Array.forEach(catalog.tags, function(tag) {
			this.catalogItems.getAt(0).selectedTags.add(tag)
		}, this)

		//load in the children
		this.loadChildren(catalog.children, this.catalogTreeStore.getRootNode())
	},
	//this function is called when the catalog builder is loaded. It makes a request to get the data for the catalog that was selected on the previous page, then proceeds to load that data into the view.
	loadCatalog: function() {
		this.ajax({
			url: '/resolve/service/catalog/get',
			params: {
				id: this.id
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (!response.success) {
					clientVM.displayError(response.message)
					return
				}
				this.reloadCatRefStoreAndThenPopulateCat(response.data);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	//Most catalogs contain child nodes. This relationship is defined using an Ext nodeInterface component. http://docs.sencha.com/extjs/4.2.2/#!/api/Ext.data.NodeInterface
	// This function loops through the children of the catalog and and loads them, recursively loads and child nodes of the current child noe that's being loaded as well.
	loadChildren: function(children, node) {
		if (children) {
			Ext.Array.forEach(children, function(child) {
				this.updateSelectedNode(node);
				//Unencode name, title, description
				child.name = Ext.String.htmlDecode(child.name)
				child.title = Ext.String.htmlDecode(child.title)
				child.description = Ext.String.htmlDecode(child.description)
				child.tooltip = Ext.String.htmlDecode(child.tooltip)
				this.addToCatalog(this.model(Ext.apply(child, {
					mtype: child.type
				})))
				if (!child.rootRef)
					this.loadChildren(child.children, this.catalogTreeSelected)
			}, this)
		}
	},
	//trigger saving the catalog after 500 milliseconds, cancel a previous save if it's in progress.
	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.persistCatalog, this, [exitAfterSave])
		this.task.delay(500)
		this.set('persisting', true)
	},

	saveIsEnabled$: function() {
		return !this.persisting
	},

	persisting: false,
	//save the current catalog configuration to the database
	persistCatalog: function(exitAfterSave) {
		var serializedCatalog = this.getTreeNodeInformation(this.catalogTreeStore.getRootNode(), {
			id: this.id,
			name: this.name,
			namespace: this.namespace,
			catalogType: this.catalogType
		})

		if (this.validate(serializedCatalog)) {
			this.ajax({
				url: '/resolve/service/catalogbuilder/save',
				jsonData: serializedCatalog,
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (!response.success) {
						var msgs = response.message.split(', '),
							messages = [];
						Ext.Array.forEach(msgs, function(msg) {
							messages.push(msg)
						}, this)
						clientVM.displayError(messages.join(', '))
						return
					}
					if (exitAfterSave === true) clientVM.handleNavigation({
						modelName: 'RS.catalog.Main'
					})
					else {
						clientVM.displaySuccess(this.localize('CatalogSaved'))
						this.resetCatalog();
						this.reloadCatRefStoreAndThenPopulateCat(response.data);
					}
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				},
				callback: function() {
					this.set('persisting', false)
				}
			})
		} else {
			this.set('persisting', false)
		}
	},

	validate: function(catalog) {
		var hasDuplicate = false,
			catalogPaths = {};

		Ext.Array.forEach(catalog.children, function(child) {
			if (!this.validateChildren(child, catalogPaths))
				hasDuplicate = true
		}, this)

		return !hasDuplicate
	},

	validateChildren: function(child, paths) {
		if (child.path && paths[child.path]) {
			clientVM.displayError(this.localize('duplicatePath', child.path), 10000)
			return false
		}

		paths[child.path] = true

		var hasDuplicate = false;
		Ext.Array.forEach(child.children || [], function(child) {
			if (!this.validateChildren(child, paths))
				hasDuplicate = true
		}, this)

		return !hasDuplicate
	},

	getTreeNodeInformation: function(node, config) {
		var serialized = this.getNodeInformation(node) || {},
			childSerialized = [],
			count = 0;
		Ext.apply(serialized, config)
		if (serialized.isRootRef === 'true') childSerialized = serialized.children
		node.eachChild(function(child) {
			var ser = this.getTreeNodeInformation(child)
			ser.order = count
			count++;
			if (ser) childSerialized.push(ser)
		}, this)
		serialized.children = childSerialized
		return serialized
	},

	getNodeInformation: function(node) {
		var id = node.internalId,
			it
		this.catalogItems.foreach(function(item) {
			if (item.id == id) {
				it = item.asObject()
				if (it.id.indexOf('ext') > -1) delete it.id
				if (it.catalogId) {
					it.children = [{
						id: it.catalogId
					}]
				}
				delete it.catalogId
				delete it.leaf
				delete it.expanded

				it.tags = (item.tags || []).join(',')
				var temp = it.tags.split(',')
				it.tags = []
				Ext.Array.forEach(temp, function(tagId) {
					if (tagId) it.tags.push({
						id: tagId
					})
				})

				it.docs = []
				Ext.Array.forEach((item.docs || []).join(',').split(','), function(wikiId) {
					if (wikiId) it.docs.push({
						id: wikiId
					})
				})

				if (it.sideWidth == 300) delete it.sideWidth

				//Encode name, title, description
				it.name = Ext.String.htmlEncode(it.name)
				it.title = Ext.String.htmlEncode(it.title)
				it.description = Ext.String.htmlEncode(it.description)
				it.tooltip = Ext.String.htmlEncode(it.tooltip)

				return false
			}
		})
		return it
	},
	saveAndExit: function() {
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return clientVM.screens.length > 1 && this.saveIsEnabled
	},
	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.catalog.Main'
		})
	},
	generateCatalogIsEnabled$: function() {
		return this.id
	},
	generateCatalog: function() {
		this.message({
			title: this.localize('generateTitle'),
			msg: this.localize('generateMessage'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('generateAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyGenerateCatalog
		})
	},
	reallyGenerateCatalog: function(button) {
		if (button == 'yes') {
			Ext.Ajax.request({
				url: '/resolve/service/catalogbuilder/generate',
				params: {
					ids: [this.id]
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) this.catalogs.load()
					else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},

	addGroup: function() {
		var model = this.model({
			mtype: 'CatalogGroup'
		});
		this.addToCatalog(model);
	},
	addGroupIsEnabled$: function() {
		//if it's a tile Catalog and the root Catalog or any Folder is selected
		return !this.isTrainingCatalog && (this.isParentCatalogSelected || this.isFolderSelected);

	},
	addGroupIsVisible$: function() {
		//don't display on Side Catalogs
		return !this.isTrainingCatalog;
	},
	addFolder: function() {
		var model = this.model({
			mtype: 'CatalogFolder'
		});
		this.addToCatalog(model);
	},
	addFolderIsEnabled$: function() {
		if (this.isTrainingCatalog) { //is Side Catalog
			//it can be added to the root catalog or a sub folder
			return this.isParentCatalogSelected || this.isFolderSelected;
		} else { //is Tile Catalog
			//it can only be added to a group header
			return this.isGroupSelected;
		}
	},
	addCatalog: function() {
		var model = this.model({
			mtype: 'CatalogReference'
		});
		this.addToCatalog(model);
	},
	addCatalogIsEnabled$: function() {
		if (this.isTrainingCatalog) { //is Side Catalog
			//it can be added to the root catalog or a sub folder
			return this.isParentCatalogSelected || this.isFolderSelected;
		} else { //is Tile Catalog
			//it can only be added to a group header
			return this.isGroupSelected;
		}
	},
	addCatalogIsVisible$: function() {
		//return true //!this.isTrainingCatalog
		return false; // TODO - catalog to catalog broken so hide for now
	},
	addItem: function() {
		var model = this.model({
			mtype: 'CatalogItem'
		});
		this.addToCatalog(model);
	},
	addItemIsEnabled$: function() {
		if (this.isTrainingCatalog) { //is Side Catalog
			//it can be added to the root catalog or a sub folder
			return this.isParentCatalogSelected || this.isFolderSelected;
		} else { //is Tile Catalog
			//it can only be added to a group header
			return this.isGroupSelected;
		}
	},
	removeItem: function() {
		var newNode = this.catalogTreeSelected.parentNode
		this.catalogTreeSelected.remove()
		this.catalogItems.removeAt(this.activeItem)
		this.selectNode(newNode)
	},
	removeItemIsEnabled$: function() {
		return this.isAnythingSelected && !this.isParentCatalogSelected;
	},

	addToCatalog: function(model, noSelect) {
		var config = model.asObject(),
			node;

		this.catalogItems.add(model)
			//encode name, title, description
		config.name = Ext.String.htmlEncode(config.name)
		config.title = Ext.String.htmlEncode(config.title)
		config.description = Ext.String.htmlEncode(config.description)
		config.tooltip = Ext.String.htmlEncode(config.tooltip)
		if (config.icon) {
			config.iconCls = 'x-tree-icon-catalog'
		}
		//TODO: this is a hacky approach that only works when the Group is first created. This custom html gets lost when the Group is edited and resaved.
		//This shouldn't be in the model, it should go in the view as a configuration for the catalog tree elements
		if (config.type == 'CatalogGroup') {
			config.iconCls = 'icon-cog' //clears the folder from displaying
			config.name = '<i class="icon-cog icon-large" style="color:#999"></i>  ' + config.name
		}
		node = this.catalogTreeSelected.appendChild(config);
		if (noSelect !== false) this.selectNode(node);
	},

	moveUp: function() {
		var nodeToMove = this.catalogTreeSelected,
			parentNode = this.catalogTreeSelected.parentNode,
			sibilingNode = this.catalogTreeSelected.previousSibling;
		parentNode.insertBefore(nodeToMove, sibilingNode);
		this.updateSiblings(nodeToMove);
	},

	moveUpIsEnabled$: function() {
		return this.isAnythingSelected && !!this.catalogTreeSelectedPrevSibling;
	},

	moveDown: function() {
		var nodeToMove = this.catalogTreeSelected,
			parentNode = this.catalogTreeSelected.parentNode,
			sibilingNode = this.catalogTreeSelected.nextSibling;
		parentNode.insertBefore(sibilingNode, nodeToMove);
		this.updateSiblings(nodeToMove);
	},

	moveDownIsEnabled$: function() {
		return this.isAnythingSelected && !!this.catalogTreeSelectedNextSibling;
	},

	selectNode: function(node) {
		this.updateSelectedNode(node);
		if (this.catalogTreeSelected.getOwnerTree()) {
			this.catalogTreeSelected.getOwnerTree().getSelectionModel().select(this.catalogTreeSelected);
		}
	},

	catalogSelectionChanged: function(selected, eOpts) {
		if (selected.length > 0) {
			var node = selected[0],
				id = node.internalId;
			this.updateSelectedNode(node);
			this.catalogItems.foreach(function(item, index) {
				if (item.id == id) {
					this.set('activeItem', index)
					return false
				}
			}, this)
		}
	},

	updateSelectedNode: function(node) {
		this.set('catalogTreeSelected', node);
		this.updateSiblings(node);
	},
	updateSiblings: function(node) {
		this.set('catalogTreeSelectedPrevSibling', node.previousSibling);
		this.set('catalogTreeSelectedNextSibling', node.nextSibling);
	},

	updateName: function(config) {
		if (this.catalogTreeSelected && this.catalogTreeSelected.getOwnerTree()) {
			if (config.icon) config.iconCls = 'x-tree-icon-catalog'
			config.internalName = config.name
			config.name = config.title || config.name
			Ext.Object.each(config, function(key) {
				config[key] = Ext.String.htmlEncode(config[key])
			})
			this.catalogTreeSelected.set(config)
			this.catalogTreeSelected.commit()
			this.updatePaths()
		}
	},

	updatePaths: function() {
		//recalculate paths for all nodes because we could have changed a parent which changes the children
		this.catalogItems.foreach(function(item) {
			if (item.path) item.set('path', this.getPath(item))
		}, this)
	},

	getPath: function(catalogItem) {
		var id = catalogItem.id,
			node = this.catalogTreeStore.getNodeById(id)
		if (node) return node.getPath('internalName')
	},

	refresh: function() {
		this.message({
			title: this.localize('refreshTitle'),
			msg: this.localize('refreshMessage'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('refreshAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyRefreshCatalog
		})
	},
	refreshIsVisible$: function() {
		return this.id
	},
	reallyRefreshCatalog: function(btn) {
		if (btn == 'yes') {
			this.catalogTreeStore.getRootNode().removeAll()
			while (this.catalogItems.length > 1) this.catalogItems.removeAt(1)
			this.setupCatalog()
		}
	},

	viewCatalog: function() {
		var modelName = 'RS.catalog.CatalogViewer';
		if (this.catalogType === 'training') modelName = 'RS.catalog.TrainingViewer'

		clientVM.handleNavigation({
			modelName: modelName,
			params: {
				id: this.id,
				mode: 'edit'
			}
		})
	},
	viewCatalogIsEnabled$: function() {
		return this.id
	},
	nodeDrop: function() {
		this.updatePaths()
	}
})