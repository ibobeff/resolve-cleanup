glu.defModel('RS.catalog.CatalogPicker', {

	catalogColumns: [{
		xtype: 'treecolumn', //this is so we know which column will show the tree
		text: '~~name~~',
		flex: 2,
		sortable: true,
		dataIndex: 'name'
	}],

	catalogs: {
		mtype: 'treestore',
		fields: ['id', 'name', 'type', 'path', {
			name: 'unique',
			convert: function(newValue, model) {
				this.uniqueCount = this.uniqueCount || 0;
				return this.uniqueCount++;
			}
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/catalog/list',
			reader: {
				type: 'json',
				root: 'records',
				idProperty: 'unique'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	catalogsSelections: [],

	init: function() {
		this.catalogs.on('load', function(store, records) {
			Ext.Array.forEach(records.childNodes || [], function(record) {
				this.ajax({
					url: '/resolve/service/catalog/get',
					params: {
						id: record.get('id')
					},
					success: function(r, opts) {
						var response = RS.common.parsePayload(r);
						if (response.success) {
							var node = this.catalogs.getRootNode().findChildBy(function(node) {
								return node.get('id') == opts.params.id
							})
							Ext.Array.forEach(response.data.children, function(child) {
								this.cleanNodeAndAppend(child, node)
							}, this)
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}, this)
		}, this)
		this.catalogs.load()
	},

	cleanNodeAndAppend: function(node, parentNode) {
		//console.log(node.id);
		if (node.children.length == 0) node.leaf = true
		delete node.icon

		var n = parentNode.appendChild(node)

		Ext.Array.forEach(node.children, function(child) {
			this.cleanNodeAndAppend(child, n)
		}, this)
	},

	select: function() {
		var selectedCatalogs = [{
			name: this.catalogsSelections[0].getPath('name').substring(1),
			path: this.catalogsSelections[0].get('path')
		}];
		this.callback.apply(this.parentVM, [selectedCatalogs])
		this.doClose()
	},
	selectIsEnabled$: function() {
		return this.catalogsSelections.length > 0
	},

	cancel: function() {
		this.doClose()
	}
})