/**
 * Model definition for the Main entry point to the catalog builder
 */
glu.defModel('RS.catalog.Main', {

	mock: false,

	activate: function() {
		clientVM.setWindowTitle(this.localize('catalogs'))
		this.catalogs.load()
	},

	displayName: '~~catalogs~~',
	stateId: 'catalogBuilderList',

	catalogs: {
		mtype: 'store',
		fields: ['id', 'name', 'catalogType', {
			name: 'sys_created_on',
			type: 'date',
			format: 'time',
			dateFormat: 'time'
		}, 'sys_created_by', {
			name: 'sys_updated_on',
			type: 'date',
			format: 'time',
			dateFormat: 'time'
		}, 'sys_updated_by'],
		remoteSort: true,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/catalog/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	columns: [],

	allSelected: false,
	gridSelections: [],

	init: function() {
		//Called when mocking the backend to test the UI
		if (this.mock) this.backend = RS.catalog.createMockBackend(true)

		this.set('displayName', this.localize('catalogs'))

		var sysColumns = RS.common.grid.getSysColumns()
		Ext.Array.forEach(sysColumns, function(column) {
			switch (column.dataIndex) {
				case 'sysCreatedOn':
					column.dataIndex = 'sys_created_on'
					break;
				case 'sysCreatedBy':
					column.dataIndex = 'sys_created_by'
					break;
				case 'sysUpdatedOn':
					column.dataIndex = 'sys_updated_on'
					break;
				case 'sysUpdatedBy':
					column.dataIndex = 'sys_updated_by'
					break;
				case 'sys_id':
					column.dataIndex = 'id'
					break;

			}
		})

		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'name',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~type~~',
			dataIndex: 'catalogType',
			filterable: true,
			sortable: true,
			renderer: function(value) {
				return value === 'training' ? 'Side Catalog' : 'Tile Catalog'
			}
		}].concat(sysColumns))

		//Load the catalogs from the server
		this.catalogs.load()
				
		// generate page tokens for service/catalog/download and service/wiki/download
		clientVM.getCSRFToken_ForURI('/resolve/service/catalog/download');
		clientVM.getCSRFToken_ForURI('/resolve/service/wiki/download');
		clientVM.getCSRFToken_ForURI('/resolve/service/wiki/download/rs/rs');
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createCatalog: function() {
		clientVM.handleNavigation({
			modelName: 'RS.catalog.Catalog'
		})
	},
	createTrainingCatalog: function() {
		clientVM.handleNavigation({
			modelName: 'RS.catalog.Catalog',
			params: {
				catalogType: 'training'
			}
		})
	},
	viewCatalog: function() {
		var selection = this.gridSelections[0],
			modelName = 'RS.catalog.CatalogViewer';
		if (this.gridSelections[0].get('catalogType') === 'training') modelName = 'RS.catalog.TrainingViewer'

		clientVM.handleNavigation({
			target: '_blank',
			modelName: modelName,
			params: {
				id: this.gridSelections[0].get('id')
			}
		})
	},
	viewCatalogIsEnabled$: function() {
		return this.singleRecordSelected
	},
	singleRecordSelected$: function() {
		return this.gridSelections.length == 1
	},
	editCatalogIsEnabled$: function() {
		return this.singleRecordSelected
	},
	deleteCatalogIsEnabled$: function() {
		return this.gridSelections.length > 0
	},
	deleteCatalog: function() {
		//Delete the selected record(s), but first confirm the action
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.gridSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.gridSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/catalogbuilder/delete',
					params: {
						ids: '',
						whereClause: this.catalogs.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r);
						if (response.success) this.catalogs.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.gridSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/catalogbuilder/delete',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r);
						if (response.success) this.catalogs.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	},

	renameCatalogIsEnabled$: function() {
		return this.singleRecordSelected
	},
	renameCatalog: function() {
		this.open({
			mtype: 'RS.catalog.RenameCopyDialog',
			rename: true,
			name: Ext.htmlEncode(this.gridSelections[0].get('name'))
		});
	},
	reallyRenameCatalog: function(newName) {
		var selection = this.gridSelections[0],
			id = selection.get('id');
		this.ajax({
			url: '/resolve/service/catalogbuilder/rename',
			params: {
				id: id,
				newName: newName
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) this.catalogs.load()
				else clientVM.displayError(response.message)
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	copyCatalogIsEnabled$: function() {
		return this.singleRecordSelected
	},
	copyCatalog: function() {
		this.open({
			mtype: 'RS.catalog.RenameCopyDialog',
			rename: false,
			name: Ext.htmlEncode(this.gridSelections[0].get('name'))
		})
	},
	reallyCopyCatalog: function(newName) {
		var selection = this.gridSelections[0],
			id = selection.get('id');
		this.ajax({
			url: '/resolve/service/catalogbuilder/copy',
			params: {
				id: id,
				newName: newName
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) this.catalogs.load()
				else clientVM.displayError(response.message)
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	generateCatalog: function() {
		this.message({
			title: this.localize('generateTitle'),
			msg: this.localize(this.gridSelections.length == 1 ? 'generateMessage' : 'generatesMessage', this.gridSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('generateAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyGenerateCatalog
		})
	},
	generateCatalogIsEnabled$: function() {
		return this.gridSelections.length > 0
	},
	reallyGenerateCatalog: function(button) {
		if (button == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/catalogbuilder/generate',
					params: {
						ids: '',
						whereClause: this.catalogs.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.catalogs.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.gridSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/catalogbuilder/generate',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.catalogs.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	}
})