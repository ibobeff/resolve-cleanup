glu.defModel('RS.catalog.CatalogReference', {
	id: '',
	type: 'CatalogReference',
	initialized: false,

	catalogTitle$: function() {
		return this.isRoot ? this.localize('catalogTitle') : this.localize('catalogReferenceTitle')
	},

	fields: ['name', 'catalogId', 'id', 'type', 'leaf', 'isRoot', 'isRootRef', 'tags', 'wiki', 'link', 'displayType', 'sideWidth', 'namespace'],

	name: '',
	//set by the Catalog vm when loaded
	nameIsReadOnly: false,
	isRoot: false,
	isRootRef$: function() {
		return !this.isRoot
	},

	namespace: '',
	leaf: true,
	catalogId: '',

	sideWidth: 300,
	sideWidthIsVisible$: function() {
		return this.isRoot && this.parentVM.catalogType == 'training'
	},

	wiki: '',
	wikiIsVisible$: function() {
		return this.displayType == 'wiki' && this.isRoot && this.parentVM.catalogType == 'training'
	},
	link: 'http://',
	linkIsVisible$: function() {
		return this.displayType == 'url' && this.isRoot && this.parentVM.catalogType == 'training'
	},

	displayType: 'wiki',
	displayTypeIsVisible$: function() {
		return this.isRoot && this.parentVM.catalogType == 'training'
	},
	displayTypeStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	tags: [],
	tagsIsVisible$: function() {
		return false; //this.isRoot && !this.rootVM.isTrainingCatalog
	},

	selectedTags: {
		mtype: 'list'
	},

	selectedTagStore: {
		mtype: 'store',
		fields: ['name'],
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'selectedTags'
		}]
	},

	selectedTagColumns: [{
		header: '~~name~~',
		dataIndex: 'name',
		flex: 1
	}],

	init: function() {	
		if (!this.id) this.set('id', 'dummy' + Ext.id())
		if (!this.name) this.set('name', this.localize('newCatalogReference'))
		if (this.children && this.children.length > 0) this.set('catalogId', this.children[0].parentSysId)

		this.parentVM.catalogReferenceStore.on('load', this.updateName, this);

		if (this.catalogId && this.parentVM.catalogReferenceStore.getById(this.catalogId)) this.set('name', this.parentVM.catalogReferenceStore.getById(this.catalogId).get('name'))

		this.set('initialized', true)

		this.selectedTags.on('lengthchanged', function() {
			var t = [];
			this.selectedTags.foreach(function(tag) {
				t.push(tag.id)
			})
			this.set('tags', t)
		}, this)

		this.displayTypeStore.add([{
			name: this.localize('wiki'),
			value: 'wiki'
		}, {
			name: this.localize('url'),
			value: 'url'
		}])

		//parse any initialized tags
		var t = this.tags
		Ext.Array.forEach(t, function(tag) {
			this.selectedTags.add({
				id: tag.id,
				name: tag.name
			})
		}, this)
	},
	beforeDestroyComponent : function(){
		this.parentVM.catalogReferenceStore.un('load', this.updateName, this);
	},
	updateName: function() {
		if (this.catalogId && this.parentVM.catalogReferenceStore.getById(this.catalogId)) 
			this.set('name', this.parentVM.catalogReferenceStore.getById(this.catalogId).get('name'));
	},

	when_catalogId_changes_update_name: {
		on: ['catalogIdChanged'],
		action: function() {
			this.updateName()
		}
	},

	when_name_changes_update_tree: {
		on: ['nameChanged'],
		action: function() {
			if (this.initialized) this.parentVM.updateName({
				name: this.name
			})
		}
	},

	tagsSelections: [],

	addTag: function() {
		this.open({
			mtype: 'RS.catalog.TagPicker',
			callback: 'addTags'
		})
	},
	addTags: function(newTags) {
		var tagName = '',
			tagId = '',
			len = this.selectedTags.length,
			i, contains = false;

		Ext.Array.forEach(newTags, function(tag) {
			tagName = tag.data['name']
			tagId = tag.data['id']
			for (i = 0; i < len && !contains; i++) {
				if (this.selectedTags.getAt(i).name == tagName) contains = true
			}
			if (!contains) {
				this.selectedTags.add({
					name: tagName,
					id: tagId
				})
			}
		}, this)
	},
	removeTag: function() {
		//remove the selected tags
		var len = this.selectedTags.length,
			i, name

		Ext.Array.forEach(this.tagsSelections, function(tag) {
			name = tag.data['name']
			for (i = 0; i < len; i++) {
				if (this.selectedTags.getAt(i).name == name) {
					this.selectedTags.removeAt(i)
					i--;
					len = this.selectedTags.length
				}
			}
		}, this)
	},
	removeTagIsEnabled$: function() {
		return this.tagsSelections.length > 0
	},
	searchWiki: function() {
		this.open({
			mtype: 'RS.common.WikiDocSearch',
			dumper: {
				dump: function(records) {
					Ext.each(records, function(r) {
						this.set('wiki', r.get('ufullname'))
					}, this)
					return true;
				},
				scope: this
			},
			single: true
		}, 'Single');
	}
})