glu.defView('RS.catalog.RenameCopyDialog', {
	height: 125,
	width: 400,
	layout: 'anchor',
	title: '@{title}',
	bodyPadding: '10px',
	items: [{
		xtype: 'textfield',
		labelWidth: 145,
		anchor: '-20',
		name: 'newName'
	}],
	buttonAlign: 'left',
	buttons: [{
		name: 'renameCatalog'
	}, {
		name: 'copyCatalog'
	}, {
		name: 'cancel'
	}, {
		xtype: 'tbtext',
		text: '@{newNameTbText}',
		htmlEncode: false,
		hidden: '@{!newNameTbTextIsVisible}'
	}]
})