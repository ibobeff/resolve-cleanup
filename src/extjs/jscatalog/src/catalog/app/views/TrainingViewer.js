glu.defView('RS.catalog.TrainingViewer', {
	layout: 'border',
	border: false,
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		style: 'padding: 12px 0px 2px 18px',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{name}'
		}]
	}],
	items: [{
		region: 'west',
		xtype: 'treepanel',
		cls: 'training',
		animate: false,
		useArrows: true,
		width: '@{sideWidth}',
		minWidth: 225,
		maxWidth: 500,
		store: '@{catalogTreeStore}',
		hideHeaders: true,
		split: true,
		autoScroll: true,
		border: false,
		rootVisible: false,
		collapseMode: 'mini',
		tbar: [{
			xtype: 'button',
			iconCls: 'icon-chevron-left rs-icon-button',
			text: '~~previous~~',
			// tooltip: '~~previousSlideTooltip~~',
			handler: function(button) {
				var tree = button.up('treepanel'),
					sm = tree.getSelectionModel(),
					current = sm.getSelection()[0],
					previous = current.previousSibling;
				if (previous && !previous.isExpanded()) {
					previous.expand();
				}
				sm.selectPrevious();
			}
		}, {
			xtype: 'button',
			iconCls: 'icon-chevron-right rs-icon-button',
			iconAlign: 'right',
			text: '~~next~~',
			// tooltip: '~~nextSlideTooltip~~',
			handler: function(button) {
				var tree = button.up('treepanel'),
					r = tree.getSelection(),
					nextRecord = tree.getNext(r);
				if (nextRecord) {
					nextRecord.parentNode.expand();
					tree.getSelectionModel().select(nextRecord);
				} else {
					// Assert: nextRecord is falsey
					tree.getSelectionModel().select(tree.getRootNode().childNodes[0]);
				}
			}
		}, '->', {
			xtype: 'button',
			itemId: 'pinTool',
			iconCls: 'icon-pushpin rs-icon-button',
			// tooltip: '~~pinTooltip~~',
			handler: function(button) {
				button.up('treepanel').toggleAutoCollapse()
			}
		}],
		getSelection: function() {
			return this.getSelectionModel().getSelection()[0];
		},
		getNextUncle: function(node) {
			// Gets the next parent node's next sibling. If that
			// doesn't have a sibling, go up again. Hitting root
			// returns null.
			if (node.isRoot()) {
				return null;
			} else {
				if (node.parentNode.nextSibling) {
					return node.parentNode.nextSibling;
				} else {
					return this.getNextUncle(node.parentNode);
				}
			}
		},

		getNext: function(node) {
			// returns the next node from the specified node
			// This should probably be a method on the tree.
			var result = null;
			if (node) {
				if (node.hasChildNodes()) {
					result = node.firstChild;
				} else if (node.nextSibling) {
					result = node.nextSibling;
				} else if (node.parentNode) {
					// No next siblings or kids -- must be the last child
					return this.getNextUncle(node);
				}
			}
			return result;
		},
		columns: [{
			xtype: 'treecolumn',
			header: '~~catalogItems~~',
			flex: 1,
			dataIndex: 'name',
			width: Ext.isIE6 ? null : 10000
		}],
		listeners: {
			selectionchange: '@{catalogSelectionChanged}',
			afterrender: function(tree) {
				tree.addCls(tree.autoWidthCls)
				Ext.defer(function() {
					tree.getEl().on('mouseover', function(e, target) {
						tree.expandIfNeeded()
					})

					tree.splitter.getEl().on('mouseover', function(e, target) {
						tree.expandIfNeeded()
					})

					// Ext.create('Ext.tip.ToolTip', {
					// 	target: tree.getHeader().getEl(),
					// 	html: tree.title
					// })
				}, 1)
			},
			itemClick: '@{itemClick}'
		},
		getPinTool: function() {
			return this.down('#pinTool');
		},
		autoCollapse: false,
		getAutoCollapse: function() {
			return this.autoCollapse
		},
		setAutoCollapse: function(val) {
			this.updateAutoCollapse(val, this.autoCollapse)
			this.autoCollapse = val
		},
		expandIfNeeded: function() {
			if (this.getAutoCollapse() && this.collapsed) {
				this.expand();
			}
		},
		collapseIfNeeded: function() {
			if (this.getAutoCollapse() && !this.collapsed) {
				this.collapse();
			}
		},
		toggleAutoCollapse: function() {
			this.setAutoCollapse(!this.getAutoCollapse());
		},
		updateAutoCollapse: function(newValue, oldValue) {
			// Setting to true means it's un-pinned.
			var tool = this.getPinTool();
			if (tool) {
				tool.setIconCls(newValue ? 'icon-pushpin rs-icon-button icon-rotate-90' : 'icon-pushpin rs-icon-button')
				this.saveState()
			}
		}
	}, {
		region: 'center',
		border: false,
		layout: 'card',
		activeItem: '@{activeCard}',
		items: [{
			html: '@{selectedWikiUrl}',
			listeners: {
				afterrender: function(panel) {
					panel.getEl().on('mouseover', function(e, target) {
						panel.up('panel').up('panel').down('treepanel').collapseIfNeeded();
					}, this);
				}
			}
		}, {
			xtype: 'grid',
			store: '@{listOfDocuments}',
			hideHeaders: true,
			maxImageWidth: '@{listOfDocumentsMaxImageWidth}',
			setMaxImageWidth: function(value) {
				this.maxImageWidth = value
			},
			columns: [{
				dataIndex: 'utitle',
				flex: 1,
				renderer: function(value, metaData, record) {
					var summary = record.get('usummary'),
						doc = document.createElement('div');
					doc.innerHTML = summary;
					if (doc.innerHTML !== summary) summary = Ext.String.htmlEncode(summary)

					var attachmentStr = '';
					if (record.get('attachments').length > 0) {
						var style = this.maxImageWidth > 0 ? 'max-width:' + this.maxImageWidth + 'px' : '';
						Ext.String.format('<img src="/resolve/service/wiki/download/rs/rs?attach={0}&{1}={2}" style="{3}" />',
							record.get('attachments')[0],
							clientVM.CSRFTOKEN_NAME,
							clientVM.getPageToken('service/wiki/download/rs/rs'),
							style)
					}

					return Ext.String.format('<table><tr><td colspan=2 style="font-size: 16px; font-weight: bold;padding: 4px">{0}</td></tr><tr><td>{1}</td><td style="font-size: 14px;padding: 8px">{2}</td></tr></table>',
						record.get('utitle') || record.get('ufullname'),
						attachmentStr,
						Ext.util.Format.nl2br(summary));
				}
			}],
			listeners: {
				itemclick: '@{documentClicked}',
				afterrender: function(panel) {
					panel.getEl().on('mouseover', function(e, target) {
						panel.up('panel').up('panel').down('treepanel').collapseIfNeeded();
					}, this);
				}
			}
		}]
	}],
	listeners: {
		afterrender: function() {
			var splitters = this.query('bordersplitter')
			Ext.Array.forEach(splitters, function(splitter) {
				splitter.setSize(1, 1)
				splitter.getEl().setStyle({
					backgroundColor: '#fbfbfb'
				})
			})
		}
	}
})