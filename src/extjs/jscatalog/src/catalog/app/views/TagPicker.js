glu.defView('RS.catalog.TagPicker', {
	title: '@{title}',
	width: '@{width}',
	height: '@{height}',

	layout: 'card',
	activeItem: '@{activeCard}',
	items: [{
		xtype: 'grid',
		plugins: [{
			ptype: 'pager',
			allowAutoRefresh: false
		}],
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: [{
				xtype: 'textfield',
				name: 'searchTag'
			}]
		}],
		name: 'tags',
		store: '@{tagStore}',
		columns: '@{tagColumns}',
		listeners: {
			itemdblclick: '@{selectRecord}'
		}
	}, {
		xtype: 'form',
		bodyPadding: '10px',
		items: [{
			xtype: 'textfield',
			name: 'tagName',
			anchor: '-20'
		}]
	}],
	buttonAlign: 'left',
	buttons: ['select', 'newTag', 'create', 'cancel'],
	listeners: {
		resize: function() {
			this.center()
		}
	}
})