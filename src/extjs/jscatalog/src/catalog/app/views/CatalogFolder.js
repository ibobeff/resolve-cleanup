glu.defView('RS.catalog.CatalogFolder', {
	xtype: 'form',
	bodyPadding: '10px',
	autoScroll: true,
	defaults: {
		anchor: '-20'
	},
	title: '~~catalogFolderTitle~~',
	items: [{
		xtype: 'textfield',
		name: 'name'
	}, {
		xtype: 'displayfield',
		htmlEncode: true,
		name: 'path'
	}, {
		xtype: 'textfield',
		name: 'title'
	}, {
		xtype: 'textarea',
		name: 'description'
	}, {
		xtype: 'textfield',
		name: 'tooltip'
	}, {
		xtype: 'imagepickerfield',
		name: 'imageName',
		hidden: '@{!imageIsVisible}',
		listeners: {
			attach: '@{attach}'
		}
	}, {
		xtype: 'combobox',
		name: 'displayType',
		store: '@{displayTypeStore}',
		queryMode: 'local',
		displayField: 'name',
		valueField: 'value'
	}, {
		name: 'wiki',
		xtype: 'triggerfield',
		triggerBaseCls: 'icon-search',
		triggerCls: 'icon-search',
		onTriggerClick: function() {
			this.fireEvent('openSearch', this);
		},
		listeners: {
			openSearch: '@{searchWiki}'
		}
	}, {
		xtype: 'textfield',
		name: 'link'
	}, {
		xtype: 'checkbox',
		name: 'openInNewTab'
	}, {
		xtype: 'combobox',
		name: 'form',
		store: '@{..formReferenceStore}',
		queryMode: 'local',
		displayField: 'name',
		valueField: 'value'
	}, {
		xtype: 'grid',
		title: '~~tags~~',
		name: 'tags',
		tbar: ['addTag', 'removeTag'],
		store: '@{selectedTagStore}',
		columns: '@{selectedTagColumns}'
	}]
})