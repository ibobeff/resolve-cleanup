glu.defView('RS.catalog.CatalogReference', {
	xtype: 'form',
	bodyPadding: '10px',
	title: '@{catalogTitle}',
	defaults: {
		anchor: '-20'
	},
	items: [{
		xtype: 'textfield',
		value: '@{rootVM.name}',
		fieldLabel: '~~name~~',
		readOnly: '@{nameIsReadOnly}',
		hidden: '@{!isRoot}'
	}, {
		xtype: 'textfield',
		value: '@{rootVM.namespace}',
		fieldLabel: '~~namespace~~',
		hidden: '@{!isRoot}'
	}, {
		xtype: 'numberfield',
		name: 'sideWidth',
		minValue: 225,
		maxValue: 500
	}, {
		xtype: 'combobox',
		name: 'catalogId',
		hidden: '@{isRoot}',
		store: '@{..catalogReferenceStore}',
		queryMode: 'local',
		displayField: 'name',
		valueField: 'id'
	}, {
		xtype: 'combobox',
		name: 'displayType',
		readOnly: '@{isRoot}',
		store: '@{displayTypeStore}',
		queryMode: 'local',
		displayField: 'name',
		valueField: 'value'
	}, {
		name: 'wiki',
		xtype: 'triggerfield',
		triggerBaseCls: 'icon-search',
		triggerCls: 'icon-search',
		onTriggerClick: function() {
			this.fireEvent('openSearch', this);
		},
		listeners: {
			openSearch: '@{searchWiki}'
		}
	}, {
		xtype: 'textfield',
		name: 'link'
	}, {
		xtype: 'grid',
		title: '~~tags~~',
		name: 'tags',
		tbar: ['addTag', 'removeTag'],
		store: '@{selectedTagStore}',
		columns: '@{selectedTagColumns}'
	}],
	listeners: {
		beforedestroy : '@{beforeDestroyComponent}'
	}
})