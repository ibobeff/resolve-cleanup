/**
 * View definition for the main view of the catalog builder.
 */
glu.defView('RS.catalog.Main', {
	//padding: '10px',
	xtype: 'grid',
	name: 'grid',
	cls : 'rs-grid-dark',
	padding: 15,
	stateId: '@{stateId}',
	displayName: '@{displayName}',
	stateful: true,
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	store: '@{catalogs}',
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.editColumnTooltip,
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		childLink: 'RS.catalog.Catalog',
		childLinkIdProperty: 'id'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls : 'rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			text: '~~newText~~',
			menu: ['createCatalog', 'createTrainingCatalog']
		}, 'deleteCatalog', 'viewCatalog', 'renameCatalog', 'copyCatalog']
	}],
	columns: '@{columns}',
	viewConfig: {
		enableTextSelection: true
	},
	listeners: {
		// showDetail: '@{showDetails}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
			// viewSelected: '@{viewSelected}',
			// columnhide: function(ct, column, eOpts) {
			// 	this.view.refresh();
			// },
			// columnshow: function(ct, column, eOpts) {
			// 	this.view.refresh();
			// }
	}
});