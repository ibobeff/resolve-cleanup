glu.defView('RS.catalog.ImagePicker', {
	asWindow: {
		title: '~~ImagePicker~~',
		cls : 'rs-modal-popup',
		height: 500,
		width: 800,
		modal: true,
		padding : 15,
		closable: true
	},
	dockedItems: [{
		xtype: 'toolbar',
		margin: '4 0',
		cls: 'rs-dockedtoolbar',
		defaults : {
			cls: 'rs-small-btn rs-btn-light'
		},
		items: [{
			xtype: 'button',
			name: 'uploadFile',
			text: 'Upload',
			preventDefault: false,
			hidden: '@{!allowUpload}',
			id: 'upload_button'
		}, '->', {
			name: 'reload',
			tooltip: 'Reload',
			text: '',
			iconCls: 'x-tbar-loading',
		}]
	}],

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
		
	items: [{
		itemId: 'fineUploaderTemplate',
		html: '@{fineUploaderTemplate}',
		height: '@{fineUploaderTemplateHeight}',
		hidden: '@{fineUploaderTemplateHidden}',
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		id: 'attachments_grid',
		store: '@{filesStore}',
		columns: '@{filesColumns}',
		multiSelect: '@{multiSelect}',
		margin: '6 0 0 0',
		flex: 1,
		listeners: {
			selectionChange: '@{selectionChanged}'
		}
	}],

	buttons: [{
		name: 'choose',
		cls : 'rs-med-btn rs-btn-dark'
	}, {
		name: 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})