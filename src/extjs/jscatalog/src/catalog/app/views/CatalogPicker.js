glu.defView('RS.catalog.CatalogPicker', {

	asWindow: {
		title: '~~catalogPickerTitle~~',
		width: 600,
		height: 500
	},

	buttonAlign: 'left',
	buttons: ['select', 'cancel'],

	layout: 'fit',

	items: [{
		xtype: 'treepanel',
		columns: '@{catalogColumns}',
		name: 'catalogs',
		rootVisible: false
	}]
})