glu.defView('RS.catalog.Catalog', {
	padding : 15,
	layout: 'border',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls: 'actionBar rs-dockedtoolbar',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{catalogTitle}'
		}],
		margin : '0 0 15 0'
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, 'viewCatalog', '|', 'addGroup', 'addFolder', 'addItem', 'addCatalog', 'removeItem', '|',  'moveUp', 'moveDown', '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sys_created_on}',
			sysCreatedBy: '@{sys_created_by}',
			sysUpdated: '@{sys_updated_on}',
			sysUpdatedBy: '@{sys_updated_by}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	items: [{
		region: 'center',
		xtype: 'treepanel',
		useArrows: true,
		border: false,
		bodyStyle: 'border-top: 0px !important',
		flex: 1,
		store: '@{catalogTreeStore}',
		enableDragAndDrop: '@{isTrainingCatalog}',
		setEnableDragAndDrop: function(value) {
			this.enableDragAndDrop = value
			if (this.getView().getPlugin().dragZone)
				this.getView().getPlugin().dragZone[value ? 'unlock' : 'lock']()
			if (this.getView().getPlugin().dropZone)
				this.getView().getPlugin().dropZone[value ? 'unlock' : 'lock']()
		},
		hideHeaders: true,
		viewConfig: {
			plugins: {
				ptype: 'treeviewdragdrop',
				containerScroll: true
			},
			listeners: {
				drop: function() {
					this.ownerCt.fireEvent('nodeDrop', this.ownerCt)
				}
			}
		},
		columns: [{
			xtype: 'treecolumn',
			header: '~~catalogItems~~',
			width: Ext.isIE6 ? '100%' : 10000, // IE6 needs width:100%
			dataIndex: 'name',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				if (record.data.type == 'CatalogFolder' || record.data.type == 'CatalogItem') {
					var padding = metaData.value.match(/img/g) ? (metaData.value.match(/img/g).length * 16) + 3 : 0
					return value + (record.data.description ? Ext.String.format('<br/><div style="margin-left:{0}px;width:160px;overflow:hidden"><p style="margin:0px;white-space:normal;">{1}</p></div>', padding, Ext.util.Format.nl2br(record.data.description)) : '')
				}
				return value
			}
		}],
		listeners: {
			selectionchange: '@{catalogSelectionChanged}',
			render: function(tree) {
				tree.addCls(tree.autoWidthCls);				
			},			
			afterrender: function(tree) {
				tree.getView().getPlugin().dragZone[tree.enableDragAndDrop ? 'unlock' : 'lock']()
				tree.getView().getPlugin().dropZone[tree.enableDragAndDrop ? 'unlock' : 'lock']()
			},
			nodeDrop: '@{nodeDrop}'
		}
	}, {
		region: 'east',
		width: 500,
		split: true,
		layout: 'card',
		activeItem: '@{activeItem}',
		items: '@{catalogItems}'
	}]
})
