glu.defView('RS.catalog.CatalogViewer', {
	layout: 'card',
	activeItem: '@{activeCard}',
	cls: 'rs-catalog-catalog-viewer-page',
	dockedItems: [{
		dock: 'top',
		xtype: 'toolbar',
		items: '@{toolbar}',
		itemTemplate: {
			transforms: ['catalogToolbarButton'],
			xtype: 'button',
			text: '@{text}',
			nodeId: '@{nodeId}',
			handler: '@{toolbarButtonClick}'
		}
	}],
	items: [{
		xtype: 'rsmenupanel',
		id : "catalogviewer",
		flex: 1,
		store: '@{catalogStore}',
		autoSize: false,
		removeSize: true,
		toc: {
			display: false
		},
		listeners: {
			nodeClick: '@{nodeClickHandler}'
		}
	}, {
		xtype: 'grid',		
		store: '@{listOfDocuments}',
		hideHeaders: true,
		maxImageWidth: '@{listOfDocumentsMaxImageWidth}',
		setMaxImageWidth: function(value) {
			this.maxImageWidth = value
		},
		columns: [{
			dataIndex: 'utitle',
			flex: 1,
			renderer: function(value, metaData, record) {
				var summary = record.get('usummary'),
					doc = document.createElement('div');
				doc.innerHTML = summary;
				if (doc.innerHTML !== summary) summary = Ext.String.htmlEncode(summary)

				var attachmentStr = '';
				if (record.get('attachments').length > 0) {
					var style = this.maxImageWidth > 0 ? 'max-width:' + this.maxImageWidth + 'px' : '';
					attachmentStr = Ext.String.format('<img src="/resolve/service/wiki/download/rs/rs?attach={0}&{1}={2}" style="{3}" />',
						record.get('attachments')[0],
						clientVM.CSRFTOKEN_NAME,
						clientVM.getPageToken('service/wiki/download/rs/rs'),
						style);
				}
				return Ext.String.format('<table><tr><td colspan=2 style="font-size: 16px; font-weight: bold;padding: 4px">{0}</td></tr><tr><td>{1}</td><td style="font-size: 14px;padding: 8px">{2}</td></tr></table>',
					record.get('utitle') || record.get('ufullname'),
					attachmentStr,
					Ext.util.Format.nl2br(summary));
			}
		}],
		listeners: {
			itemclick: '@{documentClicked}'
		}
	}]
})