<!-- ******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
********************************************************************************* -->

<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="org.owasp.esapi.ESAPI"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%
    boolean debug = false;
    String d = request.getParameter("debug");
    if( d != null && d.indexOf("t") > -1){
        debug = true;
    }

    boolean mock = false;
    String m = request.getParameter("mock");
    if( m != null && m.indexOf("t") > -1){
        mock = true;
    }
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title id="page-title">Resolve Category Builder</title>

    <jsp:include page="../client/common.jsp" flush="true" />
    <jsp:include page="loader.jsp" flush="true" />
    
    <script type="text/javascript">
        Ext.onReady(function() {
            Ext.tip.QuickTipManager.init();
            var catalogId = '<%=request.getParameter("catalogId") == null ? "" : ESAPI.encoder().decodeForHTML(ESAPI.encoder().encodeForHTML(request.getParameter("catalogId")))%>'; 
            glu.viewport({
                mtype : catalogId ? 'RS.catalog.Catalog' : 'RS.catalog.Main',
                catalogId: catalogId,
                mock: <%=mock%>
            });
        });
    </script>
    
</head>
    <body>
    </body>
</html>
