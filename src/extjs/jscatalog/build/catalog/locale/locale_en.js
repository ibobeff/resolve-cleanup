/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.catalog').locale = {
	catalogBuilder: 'Catalog Builder',
	catalog: 'Catalog',
	catalogs: 'Catalogs',

	duplicatePath: 'You cannot have duplicate node paths in the catalog.  Duplicate path detected \'{0}\'.',

	error: 'Error',
	cancel: 'Cancel',
	back: 'Back',

	save: 'Save',
	CatalogSaved: 'Catalog successfully saved',

	previous: 'Prev',
	next: 'Next',

	//Actions
	newText: 'New',
	createCatalog: 'Tile Catalog',
	createTrainingCatalog: 'Side Catalog',
	deleteCatalog: 'Delete',
	renameCatalog: 'Rename',
	copyCatalog: 'Copy',
	generateCatalog: 'Generate',
	viewCatalog: 'View',

	//Refresh Action
	refreshTitle: 'Confirm Refresh',
	refreshAction: 'Refresh',
	refreshMessage: 'Are you sure you want to refresh the current catalog?  Any changes you have made before saving will be lost!',

	//Delete Action
	deleteTitle: 'Confirm Deletion',
	deleteAction: 'Delete',
	deleteMessage: 'Are you sure you want to delete the selected catalog?',
	deletesMessage: 'Are you sure you want to delete the {0} selected catalogs?',

	//Generate Action
	generateTitle: 'Confirm Generation',
	generateAction: 'Generate',
	generateMessage: 'Are you sure you want to generate the selected catalog?',
	generatesMessage: 'Are you sure you want to generate the {0} selected catalogs?',

	//Copy/Rename Action
	duplicateName: 'A catalog with that name already exists',
	newNameRequired: 'Please provide a new catalog name',
	newName: 'New catalog name',

	//Columns
	name: 'Name',
	wiki: 'Wiki',
	type: 'Type',
	newCatalog: 'New Catalog',

	//Tags
	addTag: 'Add Tag',
	removeTag: 'Remove Tag',
	tags: 'Tags',

	//wikis
	fullname: 'Fullname',
	wikis: 'Documents',
	addWiki: 'Add Document',
	removeWiki: 'Remove Document',

	addItem: 'Add Item',
	addFolder: 'Add Folder',
	addGroup: 'Add Group Header',
	addCatalog: 'Add Catalog',
	removeItem: 'Remove',
	moveUp: 'Move Up',
	moveDown: 'Move Down',

	catalogItems: 'Catalog Items',

	namespace: 'Namespace',

	catalogItemTitle: 'Catalog Item',
	catalogFolderTitle: 'Catalog Folder',
	title: 'Title',
	description: 'Description',
	tooltip: 'Tooltip',
	wiki: 'Wiki',
	url: 'URL',
	link: 'URL',
	path: 'Path',
	image: 'Image',
	listOfDocuments: 'List of Documents',
	newCatalogItem: 'New Item',
	newCatalogReference: 'New Catalog Reference',
	newCatalogFolder: 'New Folder',

	form: 'Form',
	searchGrid: 'Search Grid',

	displayType: 'Display Type',

	ImagePicker: 'Image Picker',
	choose: 'Select',
	imageName: 'Image',
	idealImageSizeMessage: 'Images are scaled to 120px X 90px',

	openInNewTab: 'Open in new tab',

	maxImageWidth: 'Max Image Width',

	Catalog: {
		viewCatalog: 'View'
	},

	CatalogReference: {
		sideWidth: 'Width',
		catalogDetailTitle: 'Catalog Reference',
		catalog: 'Catalog',
		name: 'Name',
		catalogReferenceTitle: 'Catalog Reference',
		catalogTitle: 'Catalog',
		catalogId: 'Name'
	},

	CatalogGroup: {
		catalogGroupTitle: 'Catalog Group Header',
		name: 'Name',
		newCatalogGroup: 'New Group Header',
		catalogGroupTitle: 'Catalog Group Header'
	},

	TagPicker: {
		select: 'Select',
		create: 'Create',
		selectExistingTag: 'Select Existing Tag',
		newTag: 'New Tag',
		name: 'Name',
		selectTag: 'Select Tag',
		createTag: 'Create Tag',
		tagName: 'Name',
		searchTag: 'Search',
		SaveTagErrMsg: 'The server currently cannot handle this request.[{msg}]',
		invalidName: 'Name is required and should only contain alphanumerics and \'_\', continuous underscores are not allowed.'
	},

	CatalogViewer: {
		wrongCatalogTypeTitle: 'Wrong Catalog Type',
		wrongCatalogTypeMessage: 'You are trying to load a catalog of type "Training" in the wrong viewer.  Please use the Training Viewer instead'
	},

	TrainingViewer: {
		previousSlideTooltip: 'Go to the previous slide',
		nextSlideTooltip: 'Go to the next slide',
		pinTooltip: 'Pinned toolbar does not auto-collapse (click to toggle)',
		wrongCatalogTypeTitle: 'Wrong Catalog Type',
		wrongCatalogTypeMessage: 'You are trying to load a catalog of type "Catalog" in the wrong viewer.  Please use the Catalog Viewer instead'
	},

	CatalogPicker: {
		catalogPickerTitle: 'Select a Catalog Path',

		select: 'Select',
		cancel: 'Cancel'
	}
}