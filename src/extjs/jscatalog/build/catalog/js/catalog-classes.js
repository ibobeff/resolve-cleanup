/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/
glu.defModel('RS.catalog.Catalog', {

	mock: false,

	active: false,

	wasChanged: false,
	activate: function(sceen, params) {
		this.set('wasChanged', false)
		this.set('active', true)
		this.set('name', '')
		this.set('catalogType', params ? params.catalogType : '')
		this.set('id', params ? params.id : '')
		this.setupCatalog();
		this.set('wasChanged', false)
	},

	deactivate: function() {
		this.set('active', false)
	},

	id: '',

	catalogTitle$: function() {
		clientVM.setWindowTitle(this.localize('catalogBuilder') + (this.name ? ' - ' + this.name : ''))
		return this.localize('catalogBuilder') + (this.name ? ' - ' + this.name : '')
	},

	catalogReferenceStore: {
		mtype: 'store',
		fields: ['name', 'id', 'catalogType'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/catalog/refnames',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	formReferenceStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getformnames',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	when_name_changes_update_window_and_root: {
		on: ['nameChanged'],
		action: function() {
			if (this.active) this.updateWindowTitle()
			var root = this.catalogTreeStore.getRootNode()
			if (root) {
				root.set({
					name: Ext.String.htmlEncode(this.name),
					internalName: Ext.String.htmlEncode(this.name)
				})
				root.commit()
				this.updatePaths()
			}
		}
	},

	updateWindowTitle: function() {
		clientVM.setWindowTitle(this.name + ' - ' + this.localize('catalogBuilder'));
	},
	//refers to the node that's currently selected, represented by an Ext component called a nodeInterface: http://docs.sencha.com/extjs/4.2.2/#!/api/Ext.data.NodeInterface
	catalogTreeSelected: null,
	//points to the sibling nodes of the catalogTreeSelected. Watched by a formula which updates the state of the move buttons
	catalogTreeSelectedPrevSibling: null,
	catalogTreeSelectedNextSibling: null,
	//this is a glu convention known as a formula. a property containing the return value of this function is available with the same name sans the dollar sign. 
	//The property "catalogTreeSelectedType" will automatically be updated with a new value internally by glu by running this function.
	//This will be triggered whenever the properties used in the formula are modified (in this case: catalogTreeSelected)
	//This particular formula is useful for determining the type string of the currently selected node in the catalog tree.
	catalogTreeSelectedType$: function() {
		return (this.catalogTreeSelected) ? this.catalogTreeSelected['data'].type : null;
	},
	//these "is" functions return a boolean telling you if a type of node is selected or not. Useful for logic that depends on the currently selected node.
	//ParentCatalog is the current parent catalog, or container catalog. ChildCatalog is a catalog reference that would have been added to a group or folder, which points to another Catalog.
	isParentCatalogSelected$: function() {
		return this.catalogTreeSelectedType === 'CatalogReference' && this.catalogTreeSelected.isRoot();
	},
	isChildCatalogSelected$: function() {
		return this.catalogTreeSelectedType === 'CatalogReference' && !this.catalogTreeSelected.isRoot();
	},
	isGroupSelected$: function() {
		return this.catalogTreeSelectedType === 'CatalogGroup';
	},
	isFolderSelected$: function() {
		return this.catalogTreeSelectedType === 'CatalogFolder';
	},
	isItemSelected$: function() {
		return this.catalogTreeSelectedType === 'CatalogItem';
	},
	isAnythingSelected$: function() {
		return !!this.catalogTreeSelected;
	},
	catalogTreeStore: {
		mtype: 'treestore',
		fields: ['id', 'name', 'type', 'description', 'icon', 'internalName'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	//fields
	fields: ['name', 'sideWidth', 'namespace', 'wiki', 'catalogType', 'sys_created_on', 'sys_created_by', 'sys_updated_on', 'sys_updated_by', 'sysOrganizationName'],
	wiki: '',
	name: '',
	namespace: '',
	catalogType: '',
	sideWidth: 300,

	sys_created_on: '',
	sys_updated_on: '',
	sys_created_by: '',
	sys_updated_by: '',
	sysOrganizationName: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	isTrainingCatalog$: function() { //if catalogtype is 'training', it's a side Catalog, otherwise it's a tile catalog
		return this.catalogType == 'training'
	},

	catalogItems: {
		mtype: 'activatorlist',
		focusProperty: 'activeItem',
		autoParent: true
	},

	activeItem: -1,

	init: function() {
		if (this.mock) this.backend = RS.catalog.createMockBackend(true)
		this.set('name', this.localize('newCatalog'))
		var me = this
		this.catalogReferenceStore.filter({
			filterFn: function(item) {
				//really really need to use a decent field name for this catalogType
				return !(item.get('id') == me.id || (item.get('catalogType') || '') != me.catalogType)
			}
		})
	},

	resetCatalog: function() {
		var root = this.model({
			mtype: 'CatalogReference',
			isRoot: true,
			nameIsReadOnly: false,
			name: this.localize('newCatalog')
		});
		this.catalogItems.removeAll();
		this.catalogItems.add(root);
		this.catalogTreeStore.setRootNode({
			name: this.localize('newCatalog'),
			internalName: this.localize('newCatalog'),
			id: root.id,
			type: root.type,
			expanded: true
		});
		this.set('name', this.localize('newCatalog'));
		this.set('namespace', '');
		this.set('activeItem', this.catalogItems.getAt(0));
		this.updateSelectedNode(this.catalogTreeStore.getRootNode());

		//reset sys properties
		this.set('sys_updated_by', '');
		this.set('sys_updated_on', '');
		this.set('sys_created_by', '');
		this.set('sys_created_on', '');

		this.formReferenceStore.load();
	},

	setupCatalog: function() {
		this.resetCatalog();
		if (this.id) this.loadCatalog()
		else {
			this.catalogReferenceStore.load({
					params: {
						catalogId: '',
						catalogName: ''
					}
				})
				//Configure some default information for the catalog
			if (!this.isTrainingCatalog) this.addGroup()
			this.addItem()
		}
	},

	maybePopulateCat: null,

	reloadCatRefStoreAndThenPopulateCat: function(catalog) {
		this.catalogReferenceStore.load({
			params: {
				catalogId: this.id,
				catalogName: ''
			}
		});
		//Load the fields for the catalog
		this.loadData(catalog)
			//this is a temporary hack...id and parentSysId shouldn't be used in such a way,from a grid the value of id
		this.set('id', catalog.parentSysId)
		this.updateWindowTitle()

		//restore the sideWidth on the catalog
		this.catalogItems.getAt(0).set('sideWidth', catalog.sideWidth || 300)
		this.catalogItems.getAt(0).set('wiki', catalog.wiki || '')
		this.catalogItems.getAt(0).set('link', catalog.link || 'http://')
		this.catalogItems.getAt(0).set('displayType', catalog.displayType || 'wiki')
		this.catalogItems.getAt(0).set('nameIsReadOnly', true)
			//restore the tags on the root
		Ext.Array.forEach(catalog.tags, function(tag) {
			this.catalogItems.getAt(0).selectedTags.add(tag)
		}, this)

		//load in the children
		this.loadChildren(catalog.children, this.catalogTreeStore.getRootNode())
	},
	//this function is called when the catalog builder is loaded. It makes a request to get the data for the catalog that was selected on the previous page, then proceeds to load that data into the view.
	loadCatalog: function() {
		this.ajax({
			url: '/resolve/service/catalog/get',
			params: {
				id: this.id
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (!response.success) {
					clientVM.displayError(response.message)
					return
				}
				this.reloadCatRefStoreAndThenPopulateCat(response.data);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	//Most catalogs contain child nodes. This relationship is defined using an Ext nodeInterface component. http://docs.sencha.com/extjs/4.2.2/#!/api/Ext.data.NodeInterface
	// This function loops through the children of the catalog and and loads them, recursively loads and child nodes of the current child noe that's being loaded as well.
	loadChildren: function(children, node) {
		if (children) {
			Ext.Array.forEach(children, function(child) {
				this.updateSelectedNode(node);
				//Unencode name, title, description
				child.name = Ext.String.htmlDecode(child.name)
				child.title = Ext.String.htmlDecode(child.title)
				child.description = Ext.String.htmlDecode(child.description)
				child.tooltip = Ext.String.htmlDecode(child.tooltip)
				this.addToCatalog(this.model(Ext.apply(child, {
					mtype: child.type
				})))
				if (!child.rootRef)
					this.loadChildren(child.children, this.catalogTreeSelected)
			}, this)
		}
	},
	//trigger saving the catalog after 500 milliseconds, cancel a previous save if it's in progress.
	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.persistCatalog, this, [exitAfterSave])
		this.task.delay(500)
		this.set('persisting', true)
	},

	saveIsEnabled$: function() {
		return !this.persisting
	},

	persisting: false,
	//save the current catalog configuration to the database
	persistCatalog: function(exitAfterSave) {
		var serializedCatalog = this.getTreeNodeInformation(this.catalogTreeStore.getRootNode(), {
			id: this.id,
			name: this.name,
			namespace: this.namespace,
			catalogType: this.catalogType
		})

		if (this.validate(serializedCatalog)) {
			this.ajax({
				url: '/resolve/service/catalogbuilder/save',
				jsonData: serializedCatalog,
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (!response.success) {
						var msgs = response.message.split(', '),
							messages = [];
						Ext.Array.forEach(msgs, function(msg) {
							messages.push(msg)
						}, this)
						clientVM.displayError(messages.join(', '))
						return
					}
					if (exitAfterSave === true) clientVM.handleNavigation({
						modelName: 'RS.catalog.Main'
					})
					else {
						clientVM.displaySuccess(this.localize('CatalogSaved'))
						this.resetCatalog();
						this.reloadCatRefStoreAndThenPopulateCat(response.data);
					}
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				},
				callback: function() {
					this.set('persisting', false)
				}
			})
		} else {
			this.set('persisting', false)
		}
	},

	validate: function(catalog) {
		var hasDuplicate = false,
			catalogPaths = {};

		Ext.Array.forEach(catalog.children, function(child) {
			if (!this.validateChildren(child, catalogPaths))
				hasDuplicate = true
		}, this)

		return !hasDuplicate
	},

	validateChildren: function(child, paths) {
		if (child.path && paths[child.path]) {
			clientVM.displayError(this.localize('duplicatePath', child.path), 10000)
			return false
		}

		paths[child.path] = true

		var hasDuplicate = false;
		Ext.Array.forEach(child.children || [], function(child) {
			if (!this.validateChildren(child, paths))
				hasDuplicate = true
		}, this)

		return !hasDuplicate
	},

	getTreeNodeInformation: function(node, config) {
		var serialized = this.getNodeInformation(node) || {},
			childSerialized = [],
			count = 0;
		Ext.apply(serialized, config)
		if (serialized.isRootRef === 'true') childSerialized = serialized.children
		node.eachChild(function(child) {
			var ser = this.getTreeNodeInformation(child)
			ser.order = count
			count++;
			if (ser) childSerialized.push(ser)
		}, this)
		serialized.children = childSerialized
		return serialized
	},

	getNodeInformation: function(node) {
		var id = node.internalId,
			it
		this.catalogItems.foreach(function(item) {
			if (item.id == id) {
				it = item.asObject()
				if (it.id.indexOf('ext') > -1) delete it.id
				if (it.catalogId) {
					it.children = [{
						id: it.catalogId
					}]
				}
				delete it.catalogId
				delete it.leaf
				delete it.expanded

				it.tags = (item.tags || []).join(',')
				var temp = it.tags.split(',')
				it.tags = []
				Ext.Array.forEach(temp, function(tagId) {
					if (tagId) it.tags.push({
						id: tagId
					})
				})

				it.docs = []
				Ext.Array.forEach((item.docs || []).join(',').split(','), function(wikiId) {
					if (wikiId) it.docs.push({
						id: wikiId
					})
				})

				if (it.sideWidth == 300) delete it.sideWidth

				//Encode name, title, description
				it.name = Ext.String.htmlEncode(it.name)
				it.title = Ext.String.htmlEncode(it.title)
				it.description = Ext.String.htmlEncode(it.description)
				it.tooltip = Ext.String.htmlEncode(it.tooltip)

				return false
			}
		})
		return it
	},
	saveAndExit: function() {
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return clientVM.screens.length > 1 && this.saveIsEnabled
	},
	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.catalog.Main'
		})
	},
	generateCatalogIsEnabled$: function() {
		return this.id
	},
	generateCatalog: function() {
		this.message({
			title: this.localize('generateTitle'),
			msg: this.localize('generateMessage'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('generateAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyGenerateCatalog
		})
	},
	reallyGenerateCatalog: function(button) {
		if (button == 'yes') {
			Ext.Ajax.request({
				url: '/resolve/service/catalogbuilder/generate',
				params: {
					ids: [this.id]
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) this.catalogs.load()
					else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},

	addGroup: function() {
		var model = this.model({
			mtype: 'CatalogGroup'
		});
		this.addToCatalog(model);
	},
	addGroupIsEnabled$: function() {
		//if it's a tile Catalog and the root Catalog or any Folder is selected
		return !this.isTrainingCatalog && (this.isParentCatalogSelected || this.isFolderSelected);

	},
	addGroupIsVisible$: function() {
		//don't display on Side Catalogs
		return !this.isTrainingCatalog;
	},
	addFolder: function() {
		var model = this.model({
			mtype: 'CatalogFolder'
		});
		this.addToCatalog(model);
	},
	addFolderIsEnabled$: function() {
		if (this.isTrainingCatalog) { //is Side Catalog
			//it can be added to the root catalog or a sub folder
			return this.isParentCatalogSelected || this.isFolderSelected;
		} else { //is Tile Catalog
			//it can only be added to a group header
			return this.isGroupSelected;
		}
	},
	addCatalog: function() {
		var model = this.model({
			mtype: 'CatalogReference'
		});
		this.addToCatalog(model);
	},
	addCatalogIsEnabled$: function() {
		if (this.isTrainingCatalog) { //is Side Catalog
			//it can be added to the root catalog or a sub folder
			return this.isParentCatalogSelected || this.isFolderSelected;
		} else { //is Tile Catalog
			//it can only be added to a group header
			return this.isGroupSelected;
		}
	},
	addCatalogIsVisible$: function() {
		//return true //!this.isTrainingCatalog
		return false; // TODO - catalog to catalog broken so hide for now
	},
	addItem: function() {
		var model = this.model({
			mtype: 'CatalogItem'
		});
		this.addToCatalog(model);
	},
	addItemIsEnabled$: function() {
		if (this.isTrainingCatalog) { //is Side Catalog
			//it can be added to the root catalog or a sub folder
			return this.isParentCatalogSelected || this.isFolderSelected;
		} else { //is Tile Catalog
			//it can only be added to a group header
			return this.isGroupSelected;
		}
	},
	removeItem: function() {
		var newNode = this.catalogTreeSelected.parentNode
		this.catalogTreeSelected.remove()
		this.catalogItems.removeAt(this.activeItem)
		this.selectNode(newNode)
	},
	removeItemIsEnabled$: function() {
		return this.isAnythingSelected && !this.isParentCatalogSelected;
	},

	addToCatalog: function(model, noSelect) {
		var config = model.asObject(),
			node;

		this.catalogItems.add(model)
			//encode name, title, description
		config.name = Ext.String.htmlEncode(config.name)
		config.title = Ext.String.htmlEncode(config.title)
		config.description = Ext.String.htmlEncode(config.description)
		config.tooltip = Ext.String.htmlEncode(config.tooltip)
		if (config.icon) {
			config.iconCls = 'x-tree-icon-catalog'
		}
		//TODO: this is a hacky approach that only works when the Group is first created. This custom html gets lost when the Group is edited and resaved.
		//This shouldn't be in the model, it should go in the view as a configuration for the catalog tree elements
		if (config.type == 'CatalogGroup') {
			config.iconCls = 'icon-cog' //clears the folder from displaying
			config.name = '<i class="icon-cog icon-large" style="color:#999"></i>  ' + config.name
		}
		node = this.catalogTreeSelected.appendChild(config);
		if (noSelect !== false) this.selectNode(node);
	},

	moveUp: function() {
		var nodeToMove = this.catalogTreeSelected,
			parentNode = this.catalogTreeSelected.parentNode,
			sibilingNode = this.catalogTreeSelected.previousSibling;
		parentNode.insertBefore(nodeToMove, sibilingNode);
		this.updateSiblings(nodeToMove);
	},

	moveUpIsEnabled$: function() {
		return this.isAnythingSelected && !!this.catalogTreeSelectedPrevSibling;
	},

	moveDown: function() {
		var nodeToMove = this.catalogTreeSelected,
			parentNode = this.catalogTreeSelected.parentNode,
			sibilingNode = this.catalogTreeSelected.nextSibling;
		parentNode.insertBefore(sibilingNode, nodeToMove);
		this.updateSiblings(nodeToMove);
	},

	moveDownIsEnabled$: function() {
		return this.isAnythingSelected && !!this.catalogTreeSelectedNextSibling;
	},

	selectNode: function(node) {
		this.updateSelectedNode(node);
		if (this.catalogTreeSelected.getOwnerTree()) {
			this.catalogTreeSelected.getOwnerTree().getSelectionModel().select(this.catalogTreeSelected);
		}
	},

	catalogSelectionChanged: function(selected, eOpts) {
		if (selected.length > 0) {
			var node = selected[0],
				id = node.internalId;
			this.updateSelectedNode(node);
			this.catalogItems.foreach(function(item, index) {
				if (item.id == id) {
					this.set('activeItem', index)
					return false
				}
			}, this)
		}
	},

	updateSelectedNode: function(node) {
		this.set('catalogTreeSelected', node);
		this.updateSiblings(node);
	},
	updateSiblings: function(node) {
		this.set('catalogTreeSelectedPrevSibling', node.previousSibling);
		this.set('catalogTreeSelectedNextSibling', node.nextSibling);
	},

	updateName: function(config) {
		if (this.catalogTreeSelected && this.catalogTreeSelected.getOwnerTree()) {
			if (config.icon) config.iconCls = 'x-tree-icon-catalog'
			config.internalName = config.name
			config.name = config.title || config.name
			Ext.Object.each(config, function(key) {
				config[key] = Ext.String.htmlEncode(config[key])
			})
			this.catalogTreeSelected.set(config)
			this.catalogTreeSelected.commit()
			this.updatePaths()
		}
	},

	updatePaths: function() {
		//recalculate paths for all nodes because we could have changed a parent which changes the children
		this.catalogItems.foreach(function(item) {
			if (item.path) item.set('path', this.getPath(item))
		}, this)
	},

	getPath: function(catalogItem) {
		var id = catalogItem.id,
			node = this.catalogTreeStore.getNodeById(id)
		if (node) return node.getPath('internalName')
	},

	refresh: function() {
		this.message({
			title: this.localize('refreshTitle'),
			msg: this.localize('refreshMessage'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('refreshAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyRefreshCatalog
		})
	},
	refreshIsVisible$: function() {
		return this.id
	},
	reallyRefreshCatalog: function(btn) {
		if (btn == 'yes') {
			this.catalogTreeStore.getRootNode().removeAll()
			while (this.catalogItems.length > 1) this.catalogItems.removeAt(1)
			this.setupCatalog()
		}
	},

	viewCatalog: function() {
		var modelName = 'RS.catalog.CatalogViewer';
		if (this.catalogType === 'training') modelName = 'RS.catalog.TrainingViewer'

		clientVM.handleNavigation({
			modelName: modelName,
			params: {
				id: this.id,
				mode: 'edit'
			}
		})
	},
	viewCatalogIsEnabled$: function() {
		return this.id
	},
	nodeDrop: function() {
		this.updatePaths()
	}
})
glu.defModel('RS.catalog.CatalogFolder', {
	id: '',
	type: 'CatalogFolder',
	initialized: false,
	csrftoken: '',

	fields: ['id', 'name', 'title', 'description', 'image', 'imageName', 'tooltip', 'type', 'displayType', 'wiki', 'link', 'form', 'expanded', 'tags', 'internalName', 'icon', 'openInNewTab'],
	expanded: true,
	name: '',
	when_name_changes_update_title_automatically: {
		on: ['nameChanged'],
		action: function() {
			this.set('title', this.name);
		}
	},
	internalName$: function() {
		return this.name
	},
	title: '',
	description: '',
	descriptionIsVisible$: function() {
		return !this.rootVM.isTrainingCatalog
	},
	image: '',
	imageIsVisible$: function() {
		return !this.rootVM.isTrainingCatalog
	},
	icon$: function() {
		return this.image ? this.image + '&' + this.csrftoken : '';
	},
	imageName: '',
	tooltip: '',
	path: '',
	wiki: '',
	wikiIsVisible$: function() {
		return this.displayType == 'wiki' && this.rootVM.isTrainingCatalog
	},
	link: '',
	linkIsVisible$: function() {
		return this.displayType == 'url' && this.rootVM.isTrainingCatalog
	},
	form: '',
	formIsVisible$: function() {
		return this.displayType == 'form' && this.rootVM.isTrainingCatalog
	},
	displayType: '', // 'wiki',
	displayTypeIsVisible$: function() {
		return this.rootVM.isTrainingCatalog
	},

	openInNewTab: false,
	openInNewTabIsVisible$: function() {
		return Ext.Array.indexOf(['wiki', 'url'], this.displayType) > -1
	},

	displayTypeStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	tags: [],

	selectedTags: {
		mtype: 'list'
	},

	tagsIsVisible$: function() {
		return !this.rootVM.isTrainingCatalog
	},

	selectedTagStore: {
		mtype: 'store',
		fields: ['name'],
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'selectedTags'
		}]
	},

	selectedTagColumns: [{
		header: '~~name~~',
		dataIndex: 'name',
		flex: 1,
		renderer: RS.common.grid.plainRenderer()
	}],


	init: function() {
		this.initToken();
		if (!this.id) this.set('id', Ext.id())
		if (!this.name) this.set('name', this.localize('newCatalogFolder'))
		Ext.defer(function() {
			this.set('path', this.rootVM.getPath(this))
		}, 100, this)

		if (this.rootVM.isTrainingCatalog) {
			this.displayTypeStore.add([{
				name: this.localize('wiki'),
				value: 'wiki'
			}, {
				name: this.localize('url'),
				value: 'url'
			}])
		} else {
			this.displayTypeStore.add([{
				name: this.localize('form'),
				value: 'form'
			}, {
				name: this.localize('listOfDocuments'),
				value: 'listOfDocuments'
			}])
		}

		this.displayTypeStore.sort('name', 'DESC')
		this.displayTypeStore.sort('name', 'ASC')
		this.set('initialized', true)

		this.selectedTags.on('lengthchanged', function() {
			var t = [];
			this.selectedTags.foreach(function(tag) {
				t.push(tag.id)
			})
			this.set('tags', t)
		}, this)

		//parse any initialized tags
		var t = this.tags
		Ext.Array.forEach(t, function(tag) {
			this.selectedTags.add({
				id: tag.id,
				name: tag.name
			})
		}, this)
	},

	activate: function() {
		this.initToken();
	},

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/catalog/download', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this))
	},

	when_name_changes_update_tree_and_path: {
		on: ['nameChanged', 'descriptionChanged', 'imageChanged', 'titleChanged'],
		action: function() {
			if (this.initialized) this.rootVM.updateName({
					name: this.name,
					title: this.title,
					description: this.description,
					icon: this.image
				})
				// this.set('path', this.rootVM.getPath(this))
		}
	},

	attach: function() {
		this.open({
			mtype: 'RS.catalog.ImagePicker',
			callback: 'chooseImage'
		})
	},

	chooseImage: function(data) {
		if (data.length > 0) {
			this.set('image', Ext.String.format('/resolve/service/catalog/download?id={0}&{1}', data[0].get('sys_id'), this.csrftoken))
			this.set('imageName', data[0].get('name') || data[0].get('fileName'))
		}
	},

	tagsSelections: [],

	addTag: function() {
		this.open({
			mtype: 'RS.catalog.TagPicker',
			callback: 'addTags'
		})
	},
	addTags: function(newTags) {
		var tagName = '',
			tagId = '',
			len = this.selectedTags.length,
			i, contains = false;

		Ext.Array.forEach(newTags, function(tag) {
			tagName = tag.data['name']
			tagId = tag.data['id']
			for (i = 0; i < len && !contains; i++) {
				if (this.selectedTags.getAt(i).name == tagName) contains = true
			}
			if (!contains) {
				this.selectedTags.add({
					name: tagName,
					id: tagId
				})
			}
		}, this)
	},
	removeTag: function() {
		//remove the selected tags
		var len = this.selectedTags.length,
			i, name

		Ext.Array.forEach(this.tagsSelections, function(tag) {
			name = tag.data['name']
			for (i = 0; i < len; i++) {
				if (this.selectedTags.getAt(i).name == name) {
					this.selectedTags.removeAt(i)
					i--;
					len = this.selectedTags.length
				}
			}
		}, this)
	},
	removeTagIsEnabled$: function() {
		return this.tagsSelections.length > 0
	},
	searchWiki: function() {
		this.open({
			mtype: 'RS.common.WikiDocSearch',
			dumper: {
				dump: function(records) {
					Ext.each(records, function(r) {
						this.set('wiki', r.get('ufullname'))
					}, this)
					return true;
				},
				scope: this
			},
			single: true
		}, 'Single');
	}
})
glu.defModel('RS.catalog.CatalogGroup', {
	id: '',
	type: 'CatalogGroup',
	initialized: false,

	fields: ['name', 'id', 'type', 'expanded', 'internalName'],
	internalName$: function() {
		return this.name
	},
	name: '',
	expanded: true,


	init: function() {
		if (!this.id) this.set('id', Ext.id())
		if (!this.name) this.set('name', this.localize('newCatalogGroup'))
		this.set('initialized', true)
	},

	when_name_changes_update_tree: {
		on: ['nameChanged'],
		action: function() {
			if (this.initialized) this.parentVM.updateName({
				name: this.name
			})
		}
	}
})
glu.defModel('RS.catalog.CatalogItem', {
	id: '',
	type: 'CatalogItem',
	initialized: false,
	csrftoken: '',

	fields: ['id', 'name', 'title', 'description', 'image', 'imageName', 'tooltip', 'wiki', 'link', 'displayType', 'type', 'leaf', 'internalName', 'icon', 'maxImageWidth', 'path', 'openInNewTab'],
	name: '',
	when_name_changes_update_title_automatically: {
		on: ['nameChanged'],
		action: function() {
			this.set('title', this.name);
		}
	},
	internalName$: function() {
		return this.name
	},
	title: '',
	maxImageWidth: 0,
	maxImageWidthIsVisible$: function() {
		return this.displayType == 'listOfDocuments'
	},
	description: '',
	descriptionIsVisible$: function() {
		return !this.rootVM.isTrainingCatalog
	},
	image: '',
	imageIsVisible$: function() {
		return !this.rootVM.isTrainingCatalog
	},
	icon$: function() {
		return this.image ? this.image + '&' + this.csrftoken : '';
	},
	imageName: '',
	tooltip: '',
	wiki: '',
	wikiIsVisible$: function() {
		return this.displayType == 'wiki'
	},
	link: 'http://',
	linkIsVisible$: function() {
		return this.displayType == 'url'
	},
	path: '',
	leaf: true,

	displayType: 'wiki',

	openInNewTab: true,
	openInNewTabIsVisible$: function() {
		var types = ['wiki', 'url', 'listOfDocuments']
		if (this.rootVM.isTrainingCatalog) types = ['listOfDocuments']
		return Ext.Array.indexOf(types, this.displayType) > -1
	},

	displayTypeStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	docs: [],
	tags: [],

	selectedTags: {
		mtype: 'list'
	},

	selectedWikis: {
		mtype: 'list'
	},

	tagsIsVisible$: function() {
		return !this.rootVM.isTrainingCatalog
	},

	selectedTagStore: {
		mtype: 'store',
		fields: ['name'],
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'selectedTags'
		}]
	},

	selectedTagColumns: [{
		header: '~~name~~',
		dataIndex: 'name',
		flex: 1
	}],

	selectedWikiStore: {
		mtype: 'store',
		fields: ['fullname'],
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'selectedWikis'
		}]
	},

	selectedWikiColumns: [{
		header: '~~fullname~~',
		dataIndex: 'fullname',
		flex: 1
	}],

	init: function() {
		this.initToken();
		if (!this.id) this.set('id', Ext.id())
		if (!this.name) this.set('name', this.localize('newCatalogItem'))
		Ext.defer(function() {
			this.set('path', this.rootVM.getPath(this))
		}, 100, this)

		this.displayTypeStore.add([{
			name: this.localize('wiki'),
			value: 'wiki'
		}, {
			name: this.localize('url'),
			value: 'url'
		}, {
			name: this.localize('listOfDocuments'),
			value: 'listOfDocuments'
		}])

		this.selectedTags.on('lengthchanged', function() {
			var t = [];
			this.selectedTags.foreach(function(tag) {
				t.push(tag.id)
			})
			this.set('tags', t)
		}, this)

		this.selectedWikis.on('lengthchanged', function() {
			var w = [];
			this.selectedWikis.foreach(function(wiki) {
				w.push(wiki.id)
			})
			this.set('docs', w)
		}, this)

		//parse any initialized tags
		var t = this.tags
		Ext.Array.forEach(t, function(tag) {
			this.selectedTags.add({
				id: tag.id,
				name: tag.name
			})
		}, this)

		var w = this.docs
		Ext.Array.forEach(w, function(doc) {
			this.selectedWikis.add({
				id: doc.id,
				fullname: doc.ufullname
			})
		}, this)

		this.displayTypeStore.sort('name', 'ASC')
		this.set('initialized', true)
	},

	activate: function() {
		this.initToken();
	},

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/catalog/download', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this))
	},

	when_name_changes_update_tree_and_path: {
		on: ['nameChanged', 'descriptionChanged', 'imageChanged', 'titleChanged'],
		action: function() {
			if (this.initialized) this.rootVM.updateName({
					name: this.name,
					title: this.title,
					description: this.description,
					icon: this.image
				})
				// this.set('path', this.rootVM.getPath(this))
		}
	},

	attach: function() {
		this.open({
			mtype: 'RS.catalog.ImagePicker',
			callback: 'chooseImage'
		})
	},

	chooseImage: function(data) {
		if (data.length > 0) {
			this.set('image', Ext.String.format('/resolve/service/catalog/download?id={0}&{1}', data[0].get('sys_id'), this.csrftoken))
			this.set('imageName', data[0].get('name') || data[0].get('fileName'))
		}
	},

	tagsIsVisible$: function() {
		return this.displayType == 'listOfDocuments'
	},

	wikisIsVisible$: function() {
		return this.displayType == 'listOfDocuments'
	},

	tagsSelections: [],

	addTag: function() {
		this.open({
			mtype: 'RS.catalog.TagPicker',
			callback: 'addTags'
		})
	},
	addTags: function(newTags) {
		var tagName = '',
			tagId = '',
			len = this.selectedTags.length,
			i, contains = false;

		Ext.Array.forEach(newTags, function(tag) {
			tagName = tag.data['name']
			tagId = tag.data['id']
			for (i = 0; i < len && !contains; i++) {
				if (this.selectedTags.getAt(i).name == tagName) contains = true
			}
			if (!contains) {
				this.selectedTags.add({
					name: tagName,
					id: tagId
				})
			}
		}, this)
	},
	removeTag: function() {
		//remove the selected tags
		var len = this.selectedTags.length,
			i, name

		Ext.Array.forEach(this.tagsSelections, function(tag) {
			name = tag.data['name']
			for (i = 0; i < len; i++) {
				if (this.selectedTags.getAt(i).name == name) {
					this.selectedTags.removeAt(i)
					i--;
					len = this.selectedTags.length
				}
			}
		}, this)
	},
	removeTagIsEnabled$: function() {
		return this.tagsSelections.length > 0
	},
	wikisSelections: [],
	addWiki: function() {
		this.open({
			mtype: 'RS.common.WikiDocSearch',
			dumper: {
				dump: function(records) {
					var fullname = '',
						wikiId = '',
						len = this.selectedWikis.length,
						i, contains = false;

					Ext.Array.forEach(records || [], function(wiki) {
						fullname = wiki.data['ufullname']
						wikiId = wiki.data['id']
						for (i = 0; i < len && !contains; i++) {
							if (this.selectedWikis.getAt(i).fullname == fullname) contains = true
						}
						if (!contains) {
							this.selectedWikis.add({
								fullname: fullname,
								id: wikiId
							})
						}
					}, this)
					return true;
				},
				scope: this
			}
		});
	},
	removeWiki: function() {
		//remove the selected tags
		var len = this.selectedWikis.length,
			i, fullname

		Ext.Array.forEach(this.wikisSelections, function(wiki) {
			fullname = wiki.data['fullname']
			for (i = 0; i < len; i++) {
				if (this.selectedWikis.getAt(i).fullname == fullname) {
					this.selectedWikis.removeAt(i)
					i--;
					len = this.selectedWikis.length
				}
			}
		}, this)
	},
	removeWikiIsEnabled$: function() {
		return this.wikisSelections.length > 0;
	},
	searchWiki: function() {
		this.open({
			mtype: 'RS.common.WikiDocSearch',
			dumper: {
				dump: function(records) {
					Ext.each(records, function(r) {
						this.set('wiki', r.get('ufullname'))
					}, this)
					return true;
				},
				scope: this
			},
			single: true
		}, 'Single');
	}
})
glu.defModel('RS.catalog.CatalogPicker', {

	catalogColumns: [{
		xtype: 'treecolumn', //this is so we know which column will show the tree
		text: '~~name~~',
		flex: 2,
		sortable: true,
		dataIndex: 'name'
	}],

	catalogs: {
		mtype: 'treestore',
		fields: ['id', 'name', 'type', 'path', {
			name: 'unique',
			convert: function(newValue, model) {
				this.uniqueCount = this.uniqueCount || 0;
				return this.uniqueCount++;
			}
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/catalog/list',
			reader: {
				type: 'json',
				root: 'records',
				idProperty: 'unique'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	catalogsSelections: [],

	init: function() {
		this.catalogs.on('load', function(store, records) {
			Ext.Array.forEach(records.childNodes || [], function(record) {
				this.ajax({
					url: '/resolve/service/catalog/get',
					params: {
						id: record.get('id')
					},
					success: function(r, opts) {
						var response = RS.common.parsePayload(r);
						if (response.success) {
							var node = this.catalogs.getRootNode().findChildBy(function(node) {
								return node.get('id') == opts.params.id
							})
							Ext.Array.forEach(response.data.children, function(child) {
								this.cleanNodeAndAppend(child, node)
							}, this)
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}, this)
		}, this)
		this.catalogs.load()
	},

	cleanNodeAndAppend: function(node, parentNode) {
		//console.log(node.id);
		if (node.children.length == 0) node.leaf = true
		delete node.icon

		var n = parentNode.appendChild(node)

		Ext.Array.forEach(node.children, function(child) {
			this.cleanNodeAndAppend(child, n)
		}, this)
	},

	select: function() {
		var selectedCatalogs = [{
			name: this.catalogsSelections[0].getPath('name').substring(1),
			path: this.catalogsSelections[0].get('path')
		}];
		this.callback.apply(this.parentVM, [selectedCatalogs])
		this.doClose()
	},
	selectIsEnabled$: function() {
		return this.catalogsSelections.length > 0
	},

	cancel: function() {
		this.doClose()
	}
})
glu.defModel('RS.catalog.CatalogReference', {
	id: '',
	type: 'CatalogReference',
	initialized: false,

	catalogTitle$: function() {
		return this.isRoot ? this.localize('catalogTitle') : this.localize('catalogReferenceTitle')
	},

	fields: ['name', 'catalogId', 'id', 'type', 'leaf', 'isRoot', 'isRootRef', 'tags', 'wiki', 'link', 'displayType', 'sideWidth', 'namespace'],

	name: '',
	//set by the Catalog vm when loaded
	nameIsReadOnly: false,
	isRoot: false,
	isRootRef$: function() {
		return !this.isRoot
	},

	namespace: '',
	leaf: true,
	catalogId: '',

	sideWidth: 300,
	sideWidthIsVisible$: function() {
		return this.isRoot && this.parentVM.catalogType == 'training'
	},

	wiki: '',
	wikiIsVisible$: function() {
		return this.displayType == 'wiki' && this.isRoot && this.parentVM.catalogType == 'training'
	},
	link: 'http://',
	linkIsVisible$: function() {
		return this.displayType == 'url' && this.isRoot && this.parentVM.catalogType == 'training'
	},

	displayType: 'wiki',
	displayTypeIsVisible$: function() {
		return this.isRoot && this.parentVM.catalogType == 'training'
	},
	displayTypeStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	tags: [],
	tagsIsVisible$: function() {
		return false; //this.isRoot && !this.rootVM.isTrainingCatalog
	},

	selectedTags: {
		mtype: 'list'
	},

	selectedTagStore: {
		mtype: 'store',
		fields: ['name'],
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'selectedTags'
		}]
	},

	selectedTagColumns: [{
		header: '~~name~~',
		dataIndex: 'name',
		flex: 1
	}],

	init: function() {	
		if (!this.id) this.set('id', 'dummy' + Ext.id())
		if (!this.name) this.set('name', this.localize('newCatalogReference'))
		if (this.children && this.children.length > 0) this.set('catalogId', this.children[0].parentSysId)

		this.parentVM.catalogReferenceStore.on('load', this.updateName, this);

		if (this.catalogId && this.parentVM.catalogReferenceStore.getById(this.catalogId)) this.set('name', this.parentVM.catalogReferenceStore.getById(this.catalogId).get('name'))

		this.set('initialized', true)

		this.selectedTags.on('lengthchanged', function() {
			var t = [];
			this.selectedTags.foreach(function(tag) {
				t.push(tag.id)
			})
			this.set('tags', t)
		}, this)

		this.displayTypeStore.add([{
			name: this.localize('wiki'),
			value: 'wiki'
		}, {
			name: this.localize('url'),
			value: 'url'
		}])

		//parse any initialized tags
		var t = this.tags
		Ext.Array.forEach(t, function(tag) {
			this.selectedTags.add({
				id: tag.id,
				name: tag.name
			})
		}, this)
	},
	beforeDestroyComponent : function(){
		this.parentVM.catalogReferenceStore.un('load', this.updateName, this);
	},
	updateName: function() {
		if (this.catalogId && this.parentVM.catalogReferenceStore.getById(this.catalogId)) 
			this.set('name', this.parentVM.catalogReferenceStore.getById(this.catalogId).get('name'));
	},

	when_catalogId_changes_update_name: {
		on: ['catalogIdChanged'],
		action: function() {
			this.updateName()
		}
	},

	when_name_changes_update_tree: {
		on: ['nameChanged'],
		action: function() {
			if (this.initialized) this.parentVM.updateName({
				name: this.name
			})
		}
	},

	tagsSelections: [],

	addTag: function() {
		this.open({
			mtype: 'RS.catalog.TagPicker',
			callback: 'addTags'
		})
	},
	addTags: function(newTags) {
		var tagName = '',
			tagId = '',
			len = this.selectedTags.length,
			i, contains = false;

		Ext.Array.forEach(newTags, function(tag) {
			tagName = tag.data['name']
			tagId = tag.data['id']
			for (i = 0; i < len && !contains; i++) {
				if (this.selectedTags.getAt(i).name == tagName) contains = true
			}
			if (!contains) {
				this.selectedTags.add({
					name: tagName,
					id: tagId
				})
			}
		}, this)
	},
	removeTag: function() {
		//remove the selected tags
		var len = this.selectedTags.length,
			i, name

		Ext.Array.forEach(this.tagsSelections, function(tag) {
			name = tag.data['name']
			for (i = 0; i < len; i++) {
				if (this.selectedTags.getAt(i).name == name) {
					this.selectedTags.removeAt(i)
					i--;
					len = this.selectedTags.length
				}
			}
		}, this)
	},
	removeTagIsEnabled$: function() {
		return this.tagsSelections.length > 0
	},
	searchWiki: function() {
		this.open({
			mtype: 'RS.common.WikiDocSearch',
			dumper: {
				dump: function(records) {
					Ext.each(records, function(r) {
						this.set('wiki', r.get('ufullname'))
					}, this)
					return true;
				},
				scope: this
			},
			single: true
		}, 'Single');
	}
})
glu.defModel('RS.catalog.CatalogViewer', {
	mock: false,

	id: '',

	active: false,

	csrftoken: '',

	activate: function(screen, params) {
		this.initToken();
		if (params.id && params.id != this.id) {
			this.catalogs.removeAll()
			this.toolbar.removeAll()
			this.set('id', params.id)
			this.loadCatalog(params.id)
			return
		}

		var nodeId = params.activeNode || this.id

		if (nodeId == this.activeNode) return

		//Find the toolbar index and if its back then remove to that point for the root node
		var btn = null
		this.toolbar.each(function(button) {
			if (button.nodeId == nodeId) {
				btn = button
				return false
			}
		}, this)

		if (btn) this.toolbarButtonClick(btn)
		if (params.activeNode) {
			var node = null
			this.set('activeNode', params.activeNode)
			this.catalogs.foreach(function(catalog) {
				temp = this.findNode(nodeId, catalog)
				if (temp) node = temp
			}, this)
			if (node) {
				this.nodeClickHandler(node.id, true)
			}
		}

		this.set('active', true)
	},

	name: '',
	when_name_changes_update_window_title: {
		on: ['nameChanged'],
		action: function() {
			clientVM.setWindowTitle(this.name + ' - ' + this.localize('catalog'))
		}
	},

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/catalog/download', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this))
	},

	decactivate: function() {
		this.set('active', false)
	},

	init: function() {
		this.initToken();
		if (this.mock) this.backend = RS.catalog.createMockBackend(true)
		if (this.id) this.loadCatalog(this.id)
		this.listOfDocuments.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				path: this.path,
				tags: Ext.isArray(this.tags) ? this.tags.join(',') : this.tags
			})
		}, this)
	},

	activeCard: 0,
	activeNode: null,

	path: '',
	tags: '',

	catalogStore: {
		mtype: 'store',
		fields: ['id', 'title', 'items']
	},

	toolbar: {
		mtype: 'list'
	},

	catalogs: {
		mtype: 'list'
	},

	catalog: null,
	when_catalog_changes_ensure_data: {
		on: ['catalogChanged'],
		action: function() {
			this.massageChildren(this.catalog.children)
		}
	},
	massageChildren: function(children) {
		Ext.each(children, function(child) {
			child.title = child.title || child.name
			child.desc = child.description
			if (child.imageName) {
				var urlParts = child.image.split('?');
				if(urlParts.length == 2){
					var url = urlParts[0] + '?' + this.csrftoken + '&' + urlParts[1];
					child.image = url;
					child.icon = url;
				}
			}
			this.massageChildren(child.children)
			child.items = child.children
		}, this)
	},

	loadCatalog: function(id) {
		//See if we already have the catalog we're looking for
		var alreadyHave = false
		this.catalogs.foreach(function(catalog) {
			if (catalog.id == id) {
				this.set('catalog', catalog)
				alreadyHave = true
				return false
			}
		}, this)
		if (alreadyHave) this.processLoadCatalog()
		else this.ajax({
			url: '/resolve/service/catalog/get',
			params: {
				id: id
			},
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					if (response.data.catalogType == 'training') this.message({
							title: this.localize('wrongCatalogTypeTitle'),
							msg: this.localize('wrongCatalogTypeMessage'),
							buttons: Ext.MessageBox.OK
						})
						//load up the data into the store to be drawn on the page
					this.set('name', response.data.name)
					this.catalogs.add(response.data)
					this.set('catalog', response.data)
					this.processLoadCatalog()
				} else clientVM.displayError(response.message)
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	processLoadCatalog: function() {
		if ((this.catalog.title || this.catalog.name) && this.active) window.document.title = (this.catalog.title || this.catalog.name) + ' - ' + this.localize('catalog')
		this.addToolbarItem(this.catalog.title || this.catalog.name, this.catalog.id)
		this.catalogStore.removeAll();
		this.catalogStore.add(this.catalog.children)
			// if (this.activeNode) this.nodeClickHandler(this.activeNode)
		if (this.activeNode) {
			this.selectNodeByPath(this.activeNode)
		}
	},

	nodeClickHandler: function(nodeId, internal) {
		var node, temp
		this.catalogs.foreach(function(catalog) {
			temp = this.findNode(nodeId, catalog)
			if (temp) node = temp
		}, this)

		if (node) {
			switch (node.type) {
				case 'CatalogFolder':
					if (internal !== true) clientVM.handleNavigation({
						modelName: 'RS.catalog.CatalogViewer',
						params: {
							id: this.id,
							activeNode: node.id
						},
						sticky: true
					})
					else {
						this.catalogStore.loadData(node.children)
						this.addToolbarItem(node.title || node.name, node.id)
					}
					break;

				case 'CatalogReference':
					this.set('activeCard', 0)
						//go get the new catalog root
					if (internal !== true) clientVM.handleNavigation({
						modelName: 'RS.catalog.CatalogViewer',
						params: {
							id: this.id,
							activeNode: node.root ? node.id : node.children[0].id
						},
						sticky: true
					})
					else {
						if (node.root) this.loadCatalog(node.id)
						else this.loadCatalog(node.children[0].id)
					}
					return;
					break;

				case 'CatalogItem':
					break;
			}

			this.set('activeCard', 0)

			switch (node.displayType) {
				case 'wiki':
					clientVM.handleNavigation({
						modelName: 'RS.wiki.Main',
						params: {
							name: node.wiki
								// location: '/resolve/service/wiki/view/' + node.wiki.split('.').join('/') + '?IS_CATALOG=true'
						},
						target: node.openInNewTab ? '_blank' : '_self'
					})
					break;

				case 'url':
					clientVM.handleNavigation({
						modelName: 'RS.client.URLScreen',
						params: {
							location: node.link
						},
						target: node.openInNewTab ? '_blank' : '_self'
					})
					break;

				case 'form':
					// debugger;
					break;

				case 'listOfDocuments':
					if (internal !== true) clientVM.handleNavigation({
						modelName: 'RS.catalog.CatalogViewer',
						params: {
							id: this.id,
							activeNode: node.id
						},
						target: node.openInNewTab ? '_blank' : '_self'
					})
					else {
						this.addToolbarItem(node.title || node.name, node.id)
						this.set('activeCard', 1)
						this.set('path', node.path)
						this.set('tags', node.tags)
						this.set('listOfDocumentsMaxImageWidth', node.maxImageWidth || 0)
						this.set('listOfDocumentsOpenInNewTab', node.openInNewTab)
						this.listOfDocuments.load()
					}
					break;
			}
		}
		return false
	},

	findNode: function(nodeId, node) {
		if (nodeId == node.id) return node
		var foundNode = null
		Ext.each(node.children, function(child) {
			if (child.id == nodeId) {
				foundNode = child
				return false
			}
			foundNode = this.findNode(nodeId, child)
			if (foundNode) return false
		}, this)
		return foundNode
	},

	addToolbarItem: function(text, id) {
		if (this.toolbar.length > 0) {
			this.toolbar.add({
				mtype: 'viewmodel',
				xtype: 'tbtext',
				text: '>',
				nodeId: '',
				toolbarButtonClick: Ext.emptyFn
			})
		}
		this.toolbar.add({
			mtype: 'viewmodel',
			text: text,
			nodeId: id,
			toolbarButtonClick: function() {
				this.parentVM.toolbarButtonClick(this)
			}
		})
	},
	toolbarButtonClick: function(button) {
		if (button.nodeId != this.activeNode) {
			var index = this.toolbar.indexOf(button)
			if (index > 0) index--; //to remove the separator
			while (index < this.toolbar.length) {
				this.toolbar.removeAt(index)
			}
			this.nodeClickHandler(button.nodeId)
		}
	},

	listOfDocumentsMaxImageWidth: 0,

	listOfDocuments: {
		mtype: 'store',
		fields: ['id', 'utitle', 'usummary', 'attachments', 'ufullname'],
		// buffered: true,
		// leadingBufferZone: 300,
		pageSize: 100,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/catalog/listOfDocuments',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	listOfDocumentsOpenInNewTab: false,

	documentClicked: function(record, item, index, e, eOpts) {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			params: {
				name: record.get('ufullname')
					// location: '/resolve/service/wiki/view/' + record.get('ufullname').replace(/[.]/g, '/') + '?IS_CATALOG=true'
			},
			target: this.listOfDocumentsOpenTarget ? '_blank' : '_self'
		})
	},

	findChildByName: function(key, node) {
		if (key == node.name) return node
		var foundNode = null
		Ext.each(node.children, function(child) {
			if (child.name == key) {
				foundNode = child
				return false
			}
		}, this)
		return foundNode
	},

	selectNodeByPath: function(path, field, separator, callback, scope) {
		var me = this,
			root,
			keys,
			last;

		field = field || 'id';
		separator = separator || '/';

		keys = path.split(separator);
		last = keys.pop();
		if (keys.length > 1) {
			this.catalogs.forEach(function(catalog) {
				if (catalog.name == keys[1]) {
					keys.splice(1, 1)
					me.expandPath(catalog, keys.join(separator), field, separator, function(success, node) {
						var lastNode = node;
						if (success && node) {
							node = this.findChildByName(last, lastNode);
							if (node) {
								me.nodeClickHandler(node.id, true)
								Ext.callback(callback, scope || me, [true, node]);
								return;
							}
						}
						Ext.callback(callback, scope || me, [false, lastNode]);
					}, me);
					return false
				}
			})
		}
	},

	expandPath: function(node, path, field, separator, callback, scope) {
		var me = this,
			index = 1,
			keys,
			expander;

		field = field || 'id';
		separator = separator || '/';

		keys = path.split(separator);
		if (keys.length > 1) {
			Ext.Array.forEach(node.children, function(child) {
				if (child.name == keys[1]) {
					if (child.type != 'CatalogGroup') {
						me.nodeClickHandler(child.id, true)
					}
					keys.splice(1, 1)
					me.expandPath(child, keys.join(separator), field, separator, callback, scope)
				}
			})
		} else {
			Ext.callback(callback, scope || me, [true, node])
		}
	}
})

glu.regAdapter('catalogToolbarButton', {
	beforeCreate: function(config) {
		if (config.text == '>') config.xtype = 'tbtext'
	}
})
glu.defModel('RS.catalog.ImagePicker', {
	mixins: ['FileUploadManager'],
	api: {
		list: '/resolve/service/catalog/imageList',
		upload: '/resolve/service/catalogbuilder/upload'
	},

	allowedExtensions: ['png', 'jpg', 'jpeg', 'gif'],
	allowMultipleFiles: false,
	allowDownload: false,
	showUploadedBy: false,

	callback: '',

	initStore: function() {
		var store = Ext.create('Ext.data.Store', {
			fields: ['sys_id', 'id', {
				name: 'fileName',
				type: 'String',
				convert: function(v, r) {
					return r.raw.filename;
				}
			}, {
				name: 'size', 
				convert: function(v, r) {
					return r.raw.u_size;
				}
			}, {
				name: 'uploadedBy', 
				convert: function(v, r) {
					return r.raw.sysCreatedBy;
				}
			}, {
				name: 'sysUpdatedOn',
				type: 'date',
				dateFormat: 'time',
				format: 'time'
			}],
			sorters: [{
				property: 'fileName',
				direction: 'ASC'
			}],
			proxy: {
				type: 'ajax',
				url: this.api.list,
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});
		this.set('filesStore', store);
	},

	init: function() {
	},

	choose: function() { 
		if (this.parentVM[this.callback]) this.parentVM[this.callback](this.selections)
		this.doClose()
	},
	chooseIsEnabled$: function() {
		return this.selections.length == 1;
	},
	cancel: function() {
		this.doClose()
	},

	selectionChanged: function(selected, eOpts) {
		this.set('selections', selected)
	},
	selectRecord: function(record, item, index, e, eOpts) {
		this.set('selections', [record])
		this.choose()
	}

});

glu.defModel('RS.catalog.FileUploadManagerFactory', function(cfg) {
	Ext.applyIf(cfg, RS.common.viewmodels.FileUploadManager);
	return cfg;
});

/**
 * Model definition for the Main entry point to the catalog builder
 */
glu.defModel('RS.catalog.Main', {

	mock: false,

	activate: function() {
		clientVM.setWindowTitle(this.localize('catalogs'))
		this.catalogs.load()
	},

	displayName: '~~catalogs~~',
	stateId: 'catalogBuilderList',

	catalogs: {
		mtype: 'store',
		fields: ['id', 'name', 'catalogType', {
			name: 'sys_created_on',
			type: 'date',
			format: 'time',
			dateFormat: 'time'
		}, 'sys_created_by', {
			name: 'sys_updated_on',
			type: 'date',
			format: 'time',
			dateFormat: 'time'
		}, 'sys_updated_by'],
		remoteSort: true,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/catalog/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	columns: [],

	allSelected: false,
	gridSelections: [],

	init: function() {
		//Called when mocking the backend to test the UI
		if (this.mock) this.backend = RS.catalog.createMockBackend(true)

		this.set('displayName', this.localize('catalogs'))

		var sysColumns = RS.common.grid.getSysColumns()
		Ext.Array.forEach(sysColumns, function(column) {
			switch (column.dataIndex) {
				case 'sysCreatedOn':
					column.dataIndex = 'sys_created_on'
					break;
				case 'sysCreatedBy':
					column.dataIndex = 'sys_created_by'
					break;
				case 'sysUpdatedOn':
					column.dataIndex = 'sys_updated_on'
					break;
				case 'sysUpdatedBy':
					column.dataIndex = 'sys_updated_by'
					break;
				case 'sys_id':
					column.dataIndex = 'id'
					break;

			}
		})

		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'name',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~type~~',
			dataIndex: 'catalogType',
			filterable: true,
			sortable: true,
			renderer: function(value) {
				return value === 'training' ? 'Side Catalog' : 'Tile Catalog'
			}
		}].concat(sysColumns))

		//Load the catalogs from the server
		this.catalogs.load()
				
		// generate page tokens for service/catalog/download and service/wiki/download
		clientVM.getCSRFToken_ForURI('/resolve/service/catalog/download');
		clientVM.getCSRFToken_ForURI('/resolve/service/wiki/download');
		clientVM.getCSRFToken_ForURI('/resolve/service/wiki/download/rs/rs');
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createCatalog: function() {
		clientVM.handleNavigation({
			modelName: 'RS.catalog.Catalog'
		})
	},
	createTrainingCatalog: function() {
		clientVM.handleNavigation({
			modelName: 'RS.catalog.Catalog',
			params: {
				catalogType: 'training'
			}
		})
	},
	viewCatalog: function() {
		var selection = this.gridSelections[0],
			modelName = 'RS.catalog.CatalogViewer';
		if (this.gridSelections[0].get('catalogType') === 'training') modelName = 'RS.catalog.TrainingViewer'

		clientVM.handleNavigation({
			target: '_blank',
			modelName: modelName,
			params: {
				id: this.gridSelections[0].get('id')
			}
		})
	},
	viewCatalogIsEnabled$: function() {
		return this.singleRecordSelected
	},
	singleRecordSelected$: function() {
		return this.gridSelections.length == 1
	},
	editCatalogIsEnabled$: function() {
		return this.singleRecordSelected
	},
	deleteCatalogIsEnabled$: function() {
		return this.gridSelections.length > 0
	},
	deleteCatalog: function() {
		//Delete the selected record(s), but first confirm the action
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.gridSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.gridSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/catalogbuilder/delete',
					params: {
						ids: '',
						whereClause: this.catalogs.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r);
						if (response.success) this.catalogs.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.gridSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/catalogbuilder/delete',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r);
						if (response.success) this.catalogs.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	},

	renameCatalogIsEnabled$: function() {
		return this.singleRecordSelected
	},
	renameCatalog: function() {
		this.open({
			mtype: 'RS.catalog.RenameCopyDialog',
			rename: true,
			name: Ext.htmlEncode(this.gridSelections[0].get('name'))
		});
	},
	reallyRenameCatalog: function(newName) {
		var selection = this.gridSelections[0],
			id = selection.get('id');
		this.ajax({
			url: '/resolve/service/catalogbuilder/rename',
			params: {
				id: id,
				newName: newName
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) this.catalogs.load()
				else clientVM.displayError(response.message)
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	copyCatalogIsEnabled$: function() {
		return this.singleRecordSelected
	},
	copyCatalog: function() {
		this.open({
			mtype: 'RS.catalog.RenameCopyDialog',
			rename: false,
			name: Ext.htmlEncode(this.gridSelections[0].get('name'))
		})
	},
	reallyCopyCatalog: function(newName) {
		var selection = this.gridSelections[0],
			id = selection.get('id');
		this.ajax({
			url: '/resolve/service/catalogbuilder/copy',
			params: {
				id: id,
				newName: newName
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) this.catalogs.load()
				else clientVM.displayError(response.message)
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	generateCatalog: function() {
		this.message({
			title: this.localize('generateTitle'),
			msg: this.localize(this.gridSelections.length == 1 ? 'generateMessage' : 'generatesMessage', this.gridSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('generateAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyGenerateCatalog
		})
	},
	generateCatalogIsEnabled$: function() {
		return this.gridSelections.length > 0
	},
	reallyGenerateCatalog: function(button) {
		if (button == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/catalogbuilder/generate',
					params: {
						ids: '',
						whereClause: this.catalogs.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.catalogs.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.gridSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/catalogbuilder/generate',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.catalogs.load()
						else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	}
})
glu.defModel('RS.catalog.RenameCopyDialog', {
	rename: false,

	id: '',
	name: '',

	newName: '',

	names: {
		mtype: 'store',
		fields: ['name', 'id'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/catalog/names',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	title: '',

	init: function() {
		this.set('title', (this.rename ? this.localize('renameCatalog') : this.localize('copyCatalog')) + ' - ' + this.name)
		this.names.load()
	},

	newNameIsValid$: function() {
		if (this.newName.length == 0) return this.localize('newNameRequired')

		if (this.names.findRecord('name', this.newName, 0, false, false, true)) return this.localize('duplicateName')
		return true
	},

	newNameTbTextIsVisible$: function() {
		return this.newNameIsValid !== true
	},
	newNameTbText$: function() {
		return Ext.String.format('<font style="color:red">{0}</font>', this.newNameIsValid)
	},

	cancel: function() {
		this.doClose()
	},

	renameCatalog: function() {
		this.parentVM.reallyRenameCatalog(this.newName)
		this.doClose()
	},
	renameCatalogIsEnabled$: function() {
		return this.newNameIsValid === true
	},
	renameCatalogIsVisible$: function() {
		return this.rename
	},

	copyCatalog: function() {
		this.parentVM.reallyCopyCatalog(this.newName)
		this.doClose()
	},
	copyCatalogIsEnabled$: function() {
		return this.newNameIsValid === true
	},
	copyCatalogIsVisible$: function() {
		return !this.rename
	}
})
glu.defModel('RS.catalog.TagPicker', {

	callback: '',

	init: function() {
		this.tagStore.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {}

			var filter = [];
			if (this.searchTag) {
				filter = [{
					field: 'name',
					type: 'auto',
					condition: 'contains',
					value: this.searchTag
				}, {
					field: 'description',
					type: 'auto',
					condition: 'contains',
					value: this.searchTag
				}]
			}
			Ext.apply(operation.params, {
				operator: 'or',
				filter: Ext.encode(filter)
			})
		}, this)
		this.tagStore.load()
	},

	title$: function() {
		return this.activeCard == 0 ? this.localize('selectTag') : this.localize('createTag')
	},

	width$: function() {
		return this.activeCard == 0 ? 625 : 400
	},

	height$: function() {
		return this.activeCard == 0 ? 500 : 200
	},

	activeCard: 0,

	tagName: '',
	// tagNameIsValid$: function() {
	// 	var valid = (/^[a-zA-Z0-9_\-\/]+$/.test(this.tagName) ||
	// 		/^[a-zA-Z0-9_\-\/][a-zA-Z0-9_\-\/\s\.]*[a-zA-Z0-9_\-\/]$/.test(this.tagName)) && !/.*\s{2,}.*/.test(this.tagName) && !/.*(\.\.|\s+\.|\.\s+).*/.test(this.tagName);
	// 	return this.tagName && valid ? true : this.localize('invalidName');
	// },

	tagStore: {
		mtype: 'store',
		remoteSort: 'true',
		sorters: ['name'],
		fields: ['name'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/tag/listTags',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	searchTag: '',

	when_search_tag_changes_update_grid: {
		on: ['searchTagChanged'],
		action: function() {
			this.tagStore.loadPage(1)
		}
	},

	tagsSelections: [],

	tagColumns: [{
		header: '~~name~~',
		dataIndex: 'name',
		flex: 1,
		renderer: RS.common.grid.plainRenderer()
	}],

	select: function() {
		//Add the selected tags to the list of selected tags
		if (this.parentVM[this.callback]) this.parentVM[this.callback](this.tagsSelections)
		this.doClose()
	},
	selectIsVisible$: function() {
		return this.activeCard == 0
	},
	selectIsEnabled$: function() {
		return this.tagsSelections.length > 0
	},
	cancel: function() {
		if (this.activeCard == 1)
			this.set('activeCard', 0)
		else
			this.doClose()
	},
	create: function() {
		this.ajax({
			url: '/resolve/service/tag/saveTag',
			jsonData: {
				id: '',
				name: this.tagName,
				description: ''
			},
			success: function(r) {
				var respData = RS.common.parsePayload(r);
				if (!respData.success) {
					clientVM.displayError(this.localize('SaveTagErrMsg', {
						msg: respData.message
					}));
					return;
				}

				if (this.parentVM[this.callback]) this.parentVM[this.callback]([{
					data: {
						name: this.tagName,
						id: respData.data.id
					}
				}])
				this.doClose()
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	tagNameIsValid$: function() {
		this.isValid = /^[a-zA-Z\d\_]+$/.test(this.tagName) && !this.tagName.replace(/_/g, "").length == 0;
		return this.isValid || this.localize('invalidName')
	},
	createIsVisible$: function() {
		return this.activeCard == 1
	},
	createIsEnabled$: function() {
		return this.isValid
	},
	newTag: function() {
		this.set('activeCard', 1)
	},
	newTagIsVisible$: function() {
		return this.activeCard == 0
	},
	selectExistingTag: function() {
		this.set('activeCard', 0)
	},
	selectExistingTagIsVisible$: function() {
		return this.activeCard == 1
	},

	selectRecord: function(record, item, index, e, eOpts) {
		this.set('tagsSelections', [record])
		this.select()
	}
})
glu.defModel('RS.catalog.TrainingViewer', {
	mock: false,

	id: '',

	mode: '',
	wikiUrl$: function() {
		return this.mode == 'edit' ? '/resolve/jsp/rsclient.jsp?' + this.rsclientToken + '&displayClient=false#RS.wiki.Main/name=' : '/resolve/service/wiki/view'
	},

	activeCard: 0,

	sideWidth: 300,

	catalogReferenceStore: {
		mtype: 'store',
		fields: ['name', 'id'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/catalog/names',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	catalogTreeStore: {
		mtype: 'treestore',
		fields: ['id', 'name', 'type', 'description', 'icon', 'catalogId', 'path', 'tags', {
			name: 'maxImageWidth',
			type: 'int'
		}, {
			name: 'openInNewTab',
			type: 'bool'
		}, {
			name: 'unique',
			convert: function() {
				this.uniqueCount = this.uniqueCount || 0;
				return this.uniqueCount++;
			}
		}],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json',
				idProperty: 'unique'
			}
		}
	},

	catalogItems: {
		mtype: 'activatorlist',
		focusProperty: 'activeItem'
	},

	activeItem: -1,
	activeNode: null,

	catalogTreeSelected: null,
	catalogNodeSelected: null,

	fields: ['name', 'wiki'],
	wiki: '',
	name: '',
	nameClip$: function() {
		return Ext.util.Format.ellipsis(this.name, 20)
	},

	loaded: false,

	activate: function(screen, params) {		
		this.initToken();
		if (params && params.mode) {
			this.set('mode', params.mode)
		}

		if (params && params.id) {
			this.set('id', params.id)
		}

		if (params && params.activeNode) {
			this.set('activeNode', params.activeNode)
			this.catalogTreeStore.getRootNode().getOwnerTree().selectPath(params.activeNode, 'name')
			var node = this.catalogTreeStore.getRootNode().findChildBy(function(child) {
				return child.getPath('name') == this.activeNode
			}, this)
			if (node) {
				this.selectNode(node)
			}
		}

		if (!this.loaded)
			this.loadCatalog()
	},

	when_id_changes_load_catalog: {
		on: ['idChanged'],
		action: function() {
			this.loadCatalog()
		}
	},

	when_name_changes_update_window_title: {
		on: ['nameChanged'],
		action: function() {
			clientVM.setWindowTitle(this.name + ' - ' + this.localize('catalog'))
		}
	},

	init: function() {
		this.initToken();
		if (this.mock) this.backend = RS.catalog.createMockBackend(true)

		var root = this.model({
			mtype: 'CatalogReference',
			isRoot: true,
			name: this.localize('newCatalog')
		})
		this.catalogItems.add(root)
		this.catalogTreeStore.setRootNode({
			name: this.localize('newCatalog'),
			id: root.id,
			type: root.type,
			expanded: true
		})
		this.catalogTreeStore.on('load', this.treeStoreLoaded, this)
		this.set('activeItem', this.catalogItems.getAt(0))
		this.set('catalogTreeSelected', this.catalogTreeStore.getRootNode())

		this.listOfDocuments.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				path: this.path,
				tags: Ext.isArray(this.tags) ? this.tags.join(',') : this.tags
			})
		}, this)

		// this.loadCatalog()
	},

	wikiViewToken: '',
	rsclientToken: '',

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/wiki/view', function(token_pair) {
			this.set('wikiViewToken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this));
		this.set('rsclientToken', clientVM.rsclientToken);
	},

	path: '',
	tags: [],

	treeStoreLoaded: function() {
		if (!this.activeNode) {
			var node = this.catalogTreeStore.getRootNode()
			if (node && node.childNodes.length > 0) {
				Ext.defer(function() {
					node.getOwnerTree().getSelectionModel().select(node.childNodes[0])
				}, 100)
			}
		}
	},

	loadCatalog: function() {
		if (this.id) {
			this.ajax({
				url: '/resolve/service/catalog/get',
				params: {
					id: this.id
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.set('loaded', true)
						if (response.data.catalogType != 'training') this.message({
								title: this.localize('wrongCatalogTypeTitle'),
								msg: this.localize('wrongCatalogTypeMessage'),
								buttons: Ext.MessageBox.OK
							})
							//Load the fields for the catalog
							//this is a temporary hack...id and parentSysId shouldn't be used in such a way,from a grid the value of id
						var data = response.data;
						if (data.parentSysId)
							data.id = data.parentSysId;
						this.loadData(data);
						//load sideWidth manually to get the real number
						this.set('sideWidth', response.data.sideWidth || this.sideWidth)

						//set the root nodes path properly
						this.catalogTreeStore.getRootNode().set({
							name: response.data.name
						})

						//load in the children
						this.set('catalogTreeSelected', this.catalogTreeStore.getRootNode())
						this.catalogTreeStore.getRootNode().removeAll()
						this.loadChildren(response.data.children, this.catalogTreeStore.getRootNode())
						//Check to verify this activeNode is actually contains path from this catalog (This activeNode could be carried over from previous catalog).
						if (this.activeNode && this.activeNode.indexOf(data.path) != -1) {
							this.catalogTreeStore.getRootNode().getOwnerTree().selectPath(this.activeNode, 'name', null, function(success, node) {
								if (!success && node) this.catalogTreeStore.getRootNode().getOwnerTree().getSelectionModel().select(node)
								else this.catalogTreeStore.getRootNode().getOwnerTree().getSelectionModel().selectNext()
							}, this)

							var node = this.catalogTreeStore.getRootNode().findChildBy(function(child) {
								return child.getPath('name') == this.activeNode
							}, this)
							if (node) {
								this.selectNode(node)
							}
						} else {
							//Select first node
							this.set('activeNode', null);
							if (this.catalogTreeStore.getRootNode().childNodes.length > 0) {
								this.catalogTreeStore.getRootNode().getOwnerTree().getSelectionModel().select(this.catalogTreeStore.getRootNode().childNodes[0])
							}
						}
					} else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
	},

	loadReference: function(node, referenceId) {
		this.ajax({
			url: '/resolve/service/catalog/get',
			params: {
				id: referenceId
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.loadChildren(response.data.children, node)
				} else clientVM.displayError(response.message)
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	loadChildren: function(children, node) {
		Ext.Array.forEach(children || [], function(child) {
			var n = this.addToCatalog(this.model(Ext.apply(child, {
				mtype: child.type,
				expanded: false
			})), node, false)
			if (child.type == 'CatalogReference' && !child.root)
				return
			this.loadChildren(child.children, n)
		}, this)
	},

	catalogSelectionChanged: function(selected, eOpts) {
		if (selected.length > 0) {
			var id = selected[0].internalId
			this.set('catalogTreeSelected', selected[0])
			this.catalogItems.foreach(function(item, index) {
					if (item.id == id) {
						this.set('activeItem', index)
						return false
					}
				}, this)
				//Switch to selected wiki document
				// this.set('selectedWiki', selected[0].raw.wiki || '')
			if (!clientVM.activeScreen.sticky) {
				Ext.History.back()
				Ext.defer(function() {
					clientVM.handleNavigation({
						modelName: 'RS.catalog.TrainingViewer',
						params: {
							id: this.id,
							activeNode: selected[0].getPath('name'),
							mode: this.mode
						},
						sticky: true
					})
				}, 10, this)
			} else {
				clientVM.handleNavigation({
					modelName: 'RS.catalog.TrainingViewer',
					params: {
						id: this.id,
						activeNode: selected[0].getPath('name'),
						mode: this.mode
					},
					sticky: true
				})
			}
		}
	},

	addToCatalog: function(model, parent, noSelect) {
		this.catalogItems.add(model)
		var temp = Ext.apply({
			qtip: model.tooltip || model.name
		}, model.asObject());
		temp.expanded = temp.expanded == 'true'
		if (temp.type == 'CatalogReference') {
			temp.leaf = false
			if (!temp.isRoot) return
		}
		var node = (parent || this.catalogTreeSelected).appendChild(temp)
		if (noSelect !== false) this.selectNode(node)
		return node;
	},

	selectNode: function(node) {
		this.set('catalogTreeSelected', node)
		if (this.catalogTreeSelected.getOwnerTree()) Ext.defer(function() {
			this.catalogTreeSelected.getOwnerTree().getSelectionModel().select(this.catalogTreeSelected)
		}, 100, this)
	},

	getPath: function(catalogItem) {
		var id = catalogItem.id,
			node = this.catalogTreeStore.getNodeById(id)
		if (node) return node.getPath('name')
	},

	when_catalogTreeSelected_changes_update_display: {
		on: ['catalogTreeSelectedChanged'],
		action: function() {
			this.set('catalogNodeSelected', this.catalogItems.find(function(item) {
				return item.id == this.catalogTreeSelected.data.id
			}, this))
			if (this.catalogTreeSelected.raw.displayType == 'listOfDocuments' && this.catalogNodeSelected) {
				this.set('activeCard', 1)
				this.set('path', this.catalogNodeSelected.path)
				this.set('tags', this.catalogNodeSelected.tags)
				this.set('listOfDocumentsMaxImageWidth', this.catalogNodeSelected.maxImageWidth || 0)
				this.set('listOfDocumentsOpenTarget', this.catalogNodeSelected.openInNewTab)
				this.listOfDocuments.load()
			} else this.set('activeCard', 0)
		}
	},

	selectedWiki: '',
	selectedWikiUrl$: function() {
		var newNode = this.catalogTreeSelected;
		if (newNode && Ext.Array.indexOf(['CatalogFolder', 'CatalogItem'], newNode.get('type')) > -1) {
			switch (newNode.raw.displayType) {
				case 'url':
					return Ext.String.format('<iframe class="rs_iframe" src="{0}" style="border:0px;width:100%;height:100%"></iframe>', newNode.raw.link)

				case 'wiki':
					var wikiSrc = '';
					if (this.wikiUrl.indexOf('/resolve/jsp/rsclient.jsp') != -1) {
						wikiSrc = this.wikiUrl + newNode.raw.wiki;
					} else {
						wikiSrc = this.wikiUrl + '?wiki=' + newNode.raw.wiki + '&' + this.wikiViewToken;
					}
					return Ext.String.format('<iframe class="rs_iframe" src="{0}" style="border:0px;width:100%;height:100%"></iframe>', wikiSrc);

				case 'listOfDocuments':
					return ''
			}
		}
		return ''
	},

	itemClick: function(record, item, index, eOpts) {
		// if (!record.isLeaf() && record.isExpanded()) {
		// 	Ext.defer(function() {
		// 		this.catalogTreeStore.getRootNode().getOwnerTree().getSelectionModel().deselect(record)
		// 	}, 1, this)
		// }
		// record[record.isExpanded() ? 'collapse' : 'expand']();
		if (!record.isExpanded()) {
			if (record.get('type') == 'CatalogReference') {
				record.removeAll()
				this.loadReference(record, record.get('catalogId'))
			}
			record.expand()
		} else if (Ext.EventObject.getTarget('img')) {
			Ext.defer(function() {
				this.catalogTreeStore.getRootNode().getOwnerTree().getSelectionModel().deselect(record)
			}, 1, this)
			record.collapse()
		}
	},

	listOfDocumentsMaxImageWidth: 0,

	listOfDocuments: {
		mtype: 'store',
		fields: ['id', 'utitle', 'usummary', 'attachments', 'ufullname'],
		// buffered: true,
		// leadingBufferZone: 300,
		pageSize: 100,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/catalog/listOfDocuments',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	listOfDocumentsOpenTarget: false,

	documentClicked: function(record, item, index, e, eOpts) {
		clientVM.handleNavigation({
			modelName: 'RS.wiki.Main',
			params: {
				name: record.get('ufullname')
					// location: '/resolve/service/wiki/view/' + record.get('ufullname').replace(/[.]/g, '/') + '?IS_CATALOG=true'
			},
			target: this.listOfDocumentsOpenTarget ? '_blank' : '_self'
		})
	}
})
glu.defView('RS.catalog.Catalog', {
	padding : 15,
	layout: 'border',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls: 'actionBar rs-dockedtoolbar',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{catalogTitle}'
		}],
		margin : '0 0 15 0'
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, 'viewCatalog', '|', 'addGroup', 'addFolder', 'addItem', 'addCatalog', 'removeItem', '|',  'moveUp', 'moveDown', '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sys_created_on}',
			sysCreatedBy: '@{sys_created_by}',
			sysUpdated: '@{sys_updated_on}',
			sysUpdatedBy: '@{sys_updated_by}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	items: [{
		region: 'center',
		xtype: 'treepanel',
		useArrows: true,
		border: false,
		bodyStyle: 'border-top: 0px !important',
		flex: 1,
		store: '@{catalogTreeStore}',
		enableDragAndDrop: '@{isTrainingCatalog}',
		setEnableDragAndDrop: function(value) {
			this.enableDragAndDrop = value
			if (this.getView().getPlugin().dragZone)
				this.getView().getPlugin().dragZone[value ? 'unlock' : 'lock']()
			if (this.getView().getPlugin().dropZone)
				this.getView().getPlugin().dropZone[value ? 'unlock' : 'lock']()
		},
		hideHeaders: true,
		viewConfig: {
			plugins: {
				ptype: 'treeviewdragdrop',
				containerScroll: true
			},
			listeners: {
				drop: function() {
					this.ownerCt.fireEvent('nodeDrop', this.ownerCt)
				}
			}
		},
		columns: [{
			xtype: 'treecolumn',
			header: '~~catalogItems~~',
			width: Ext.isIE6 ? '100%' : 10000, // IE6 needs width:100%
			dataIndex: 'name',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				if (record.data.type == 'CatalogFolder' || record.data.type == 'CatalogItem') {
					var padding = metaData.value.match(/img/g) ? (metaData.value.match(/img/g).length * 16) + 3 : 0
					return value + (record.data.description ? Ext.String.format('<br/><div style="margin-left:{0}px;width:160px;overflow:hidden"><p style="margin:0px;white-space:normal;">{1}</p></div>', padding, Ext.util.Format.nl2br(record.data.description)) : '')
				}
				return value
			}
		}],
		listeners: {
			selectionchange: '@{catalogSelectionChanged}',
			render: function(tree) {
				tree.addCls(tree.autoWidthCls);				
			},			
			afterrender: function(tree) {
				tree.getView().getPlugin().dragZone[tree.enableDragAndDrop ? 'unlock' : 'lock']()
				tree.getView().getPlugin().dropZone[tree.enableDragAndDrop ? 'unlock' : 'lock']()
			},
			nodeDrop: '@{nodeDrop}'
		}
	}, {
		region: 'east',
		width: 500,
		split: true,
		layout: 'card',
		activeItem: '@{activeItem}',
		items: '@{catalogItems}'
	}]
})

glu.defView('RS.catalog.CatalogFolder', {
	xtype: 'form',
	bodyPadding: '10px',
	autoScroll: true,
	defaults: {
		anchor: '-20'
	},
	title: '~~catalogFolderTitle~~',
	items: [{
		xtype: 'textfield',
		name: 'name'
	}, {
		xtype: 'displayfield',
		htmlEncode: true,
		name: 'path'
	}, {
		xtype: 'textfield',
		name: 'title'
	}, {
		xtype: 'textarea',
		name: 'description'
	}, {
		xtype: 'textfield',
		name: 'tooltip'
	}, {
		xtype: 'imagepickerfield',
		name: 'imageName',
		hidden: '@{!imageIsVisible}',
		listeners: {
			attach: '@{attach}'
		}
	}, {
		xtype: 'combobox',
		name: 'displayType',
		store: '@{displayTypeStore}',
		queryMode: 'local',
		displayField: 'name',
		valueField: 'value'
	}, {
		name: 'wiki',
		xtype: 'triggerfield',
		triggerBaseCls: 'icon-search',
		triggerCls: 'icon-search',
		onTriggerClick: function() {
			this.fireEvent('openSearch', this);
		},
		listeners: {
			openSearch: '@{searchWiki}'
		}
	}, {
		xtype: 'textfield',
		name: 'link'
	}, {
		xtype: 'checkbox',
		name: 'openInNewTab'
	}, {
		xtype: 'combobox',
		name: 'form',
		store: '@{..formReferenceStore}',
		queryMode: 'local',
		displayField: 'name',
		valueField: 'value'
	}, {
		xtype: 'grid',
		title: '~~tags~~',
		name: 'tags',
		tbar: ['addTag', 'removeTag'],
		store: '@{selectedTagStore}',
		columns: '@{selectedTagColumns}'
	}]
})
glu.defView('RS.catalog.CatalogGroup', {
	xtype: 'form',
	bodyPadding: '10px',
	title: '~~catalogGroupTitle~~',
	defaults: {
		anchor: '-20'
	},
	items: [{
			xtype: 'textfield',
			name: 'name'
		}
	]
})
glu.defView('RS.catalog.CatalogItem', {
	xtype: 'form',
	bodyPadding: '10px',
	autoScroll: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		labelWidth: 125
	},
	title: '~~catalogItemTitle~~',
	items: [{
		xtype: 'textfield',
		name: 'name'
	}, {
		xtype: 'displayfield',
		htmlEncode: true,
		name: 'path'
	}, {
		xtype: 'textfield',
		name: 'title'
	}, {
		xtype: 'textarea',
		name: 'description'
	}, {
		xtype: 'textfield',
		name: 'tooltip'
	}, {
		xtype: 'imagepickerfield',
		name: 'imageName',
		hidden: '@{!imageIsVisible}',
		listeners: {
			attach: '@{attach}'
		}
	}, {
		xtype: 'combobox',
		name: 'displayType',
		store: '@{displayTypeStore}',
		queryMode: 'local',
		displayField: 'name',
		valueField: 'value'
	}, {
		name: 'wiki',
		xtype: 'triggerfield',
		triggerBaseCls: 'icon-search',
		triggerCls: 'icon-search',
		onTriggerClick: function() {
			this.fireEvent('openSearch', this);
		},
		listeners: {
			openSearch: '@{searchWiki}'
		}
	}, {
		xtype: 'textfield',
		name: 'link'
	}, {
		xtype: 'checkbox',
		name: 'openInNewTab'
	}, {
		xtype: 'numberfield',
		name: 'maxImageWidth',
		allowDecimals: false,
		minValue: 0
	}, {
		xtype: 'grid',
		title: '~~tags~~',
		name: 'tags',
		minHeight: 150,
		tbar: ['addTag', 'removeTag'],
		store: '@{selectedTagStore}',
		columns: '@{selectedTagColumns}'
	}, {
		xtype: 'grid',
		title: '~~wikis~~',
		name: 'wikis',
		minHeight: 150,
		tbar: ['addWiki', 'removeWiki'],
		store: '@{selectedWikiStore}',
		columns: '@{selectedWikiColumns}'
	}]
})
glu.defView('RS.catalog.CatalogPicker', {

	asWindow: {
		title: '~~catalogPickerTitle~~',
		width: 600,
		height: 500
	},

	buttonAlign: 'left',
	buttons: ['select', 'cancel'],

	layout: 'fit',

	items: [{
		xtype: 'treepanel',
		columns: '@{catalogColumns}',
		name: 'catalogs',
		rootVisible: false
	}]
})
glu.defView('RS.catalog.CatalogReference', {
	xtype: 'form',
	bodyPadding: '10px',
	title: '@{catalogTitle}',
	defaults: {
		anchor: '-20'
	},
	items: [{
		xtype: 'textfield',
		value: '@{rootVM.name}',
		fieldLabel: '~~name~~',
		readOnly: '@{nameIsReadOnly}',
		hidden: '@{!isRoot}'
	}, {
		xtype: 'textfield',
		value: '@{rootVM.namespace}',
		fieldLabel: '~~namespace~~',
		hidden: '@{!isRoot}'
	}, {
		xtype: 'numberfield',
		name: 'sideWidth',
		minValue: 225,
		maxValue: 500
	}, {
		xtype: 'combobox',
		name: 'catalogId',
		hidden: '@{isRoot}',
		store: '@{..catalogReferenceStore}',
		queryMode: 'local',
		displayField: 'name',
		valueField: 'id'
	}, {
		xtype: 'combobox',
		name: 'displayType',
		readOnly: '@{isRoot}',
		store: '@{displayTypeStore}',
		queryMode: 'local',
		displayField: 'name',
		valueField: 'value'
	}, {
		name: 'wiki',
		xtype: 'triggerfield',
		triggerBaseCls: 'icon-search',
		triggerCls: 'icon-search',
		onTriggerClick: function() {
			this.fireEvent('openSearch', this);
		},
		listeners: {
			openSearch: '@{searchWiki}'
		}
	}, {
		xtype: 'textfield',
		name: 'link'
	}, {
		xtype: 'grid',
		title: '~~tags~~',
		name: 'tags',
		tbar: ['addTag', 'removeTag'],
		store: '@{selectedTagStore}',
		columns: '@{selectedTagColumns}'
	}],
	listeners: {
		beforedestroy : '@{beforeDestroyComponent}'
	}
})
glu.defView('RS.catalog.CatalogViewer', {
	layout: 'card',
	activeItem: '@{activeCard}',
	cls: 'rs-catalog-catalog-viewer-page',
	dockedItems: [{
		dock: 'top',
		xtype: 'toolbar',
		items: '@{toolbar}',
		itemTemplate: {
			transforms: ['catalogToolbarButton'],
			xtype: 'button',
			text: '@{text}',
			nodeId: '@{nodeId}',
			handler: '@{toolbarButtonClick}'
		}
	}],
	items: [{
		xtype: 'rsmenupanel',
		id : "catalogviewer",
		flex: 1,
		store: '@{catalogStore}',
		autoSize: false,
		removeSize: true,
		toc: {
			display: false
		},
		listeners: {
			nodeClick: '@{nodeClickHandler}'
		}
	}, {
		xtype: 'grid',		
		store: '@{listOfDocuments}',
		hideHeaders: true,
		maxImageWidth: '@{listOfDocumentsMaxImageWidth}',
		setMaxImageWidth: function(value) {
			this.maxImageWidth = value
		},
		columns: [{
			dataIndex: 'utitle',
			flex: 1,
			renderer: function(value, metaData, record) {
				var summary = record.get('usummary'),
					doc = document.createElement('div');
				doc.innerHTML = summary;
				if (doc.innerHTML !== summary) summary = Ext.String.htmlEncode(summary)

				var attachmentStr = '';
				if (record.get('attachments').length > 0) {
					var style = this.maxImageWidth > 0 ? 'max-width:' + this.maxImageWidth + 'px' : '';
					attachmentStr = Ext.String.format('<img src="/resolve/service/wiki/download/rs/rs?attach={0}&OWASP_CSRFTOKEN={1}" style="{2}" />',
						record.get('attachments')[0],
						clientVM.getPageToken('service/wiki/download/rs/rs'),
						style);
				}
				return Ext.String.format('<table><tr><td colspan=2 style="font-size: 16px; font-weight: bold;padding: 4px">{0}</td></tr><tr><td>{1}</td><td style="font-size: 14px;padding: 8px">{2}</td></tr></table>',
					record.get('utitle') || record.get('ufullname'),
					attachmentStr,
					Ext.util.Format.nl2br(summary));
			}
		}],
		listeners: {
			itemclick: '@{documentClicked}'
		}
	}]
})
glu.defView('RS.catalog.ImagePicker', {
	asWindow: {
		title: '~~ImagePicker~~',
		cls : 'rs-modal-popup',
		height: 500,
		width: 800,
		modal: true,
		padding : 15,
		closable: true
	},
	dockedItems: [{
		xtype: 'toolbar',
		margin: '4 0',
		cls: 'rs-dockedtoolbar',
		defaults : {
			cls: 'rs-small-btn rs-btn-light'
		},
		items: [{
			xtype: 'button',
			name: 'uploadFile',
			text: 'Upload',
			preventDefault: false,
			hidden: '@{!allowUpload}',
			id: 'upload_button'
		}, '->', {
			name: 'reload',
			tooltip: 'Reload',
			text: '',
			iconCls: 'x-tbar-loading',
		}]
	}],

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
		
	items: [{
		itemId: 'fineUploaderTemplate',
		html: '@{fineUploaderTemplate}',
		height: '@{fineUploaderTemplateHeight}',
		hidden: '@{fineUploaderTemplateHidden}',
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		id: 'attachments_grid',
		store: '@{filesStore}',
		columns: '@{filesColumns}',
		multiSelect: '@{multiSelect}',
		margin: '6 0 0 0',
		flex: 1,
		listeners: {
			selectionChange: '@{selectionChanged}'
		}
	}],

	buttons: [{
		name: 'choose',
		cls : 'rs-med-btn rs-btn-dark'
	}, {
		name: 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})
/**
 * View definition for the main view of the catalog builder.
 */
glu.defView('RS.catalog.Main', {
	//padding: '10px',
	xtype: 'grid',
	name: 'grid',
	cls : 'rs-grid-dark',
	padding: 15,
	stateId: '@{stateId}',
	displayName: '@{displayName}',
	stateful: true,
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	store: '@{catalogs}',
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.editColumnTooltip,
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		childLink: 'RS.catalog.Catalog',
		childLinkIdProperty: 'id'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls : 'rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			text: '~~newText~~',
			menu: ['createCatalog', 'createTrainingCatalog']
		}, 'deleteCatalog', 'viewCatalog', 'renameCatalog', 'copyCatalog']
	}],
	columns: '@{columns}',
	viewConfig: {
		enableTextSelection: true
	},
	listeners: {
		// showDetail: '@{showDetails}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
			// viewSelected: '@{viewSelected}',
			// columnhide: function(ct, column, eOpts) {
			// 	this.view.refresh();
			// },
			// columnshow: function(ct, column, eOpts) {
			// 	this.view.refresh();
			// }
	}
});
glu.defView('RS.catalog.RenameCopyDialog', {
	height: 125,
	width: 400,
	layout: 'anchor',
	title: '@{title}',
	bodyPadding: '10px',
	items: [{
		xtype: 'textfield',
		labelWidth: 145,
		anchor: '-20',
		name: 'newName'
	}],
	buttonAlign: 'left',
	buttons: [{
		name: 'renameCatalog'
	}, {
		name: 'copyCatalog'
	}, {
		name: 'cancel'
	}, {
		xtype: 'tbtext',
		text: '@{newNameTbText}',
		htmlEncode: false,
		hidden: '@{!newNameTbTextIsVisible}'
	}]
})
glu.defView('RS.catalog.TagPicker', {
	title: '@{title}',
	width: '@{width}',
	height: '@{height}',

	layout: 'card',
	activeItem: '@{activeCard}',
	items: [{
		xtype: 'grid',
		plugins: [{
			ptype: 'pager',
			allowAutoRefresh: false
		}],
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: [{
				xtype: 'textfield',
				name: 'searchTag'
			}]
		}],
		name: 'tags',
		store: '@{tagStore}',
		columns: '@{tagColumns}',
		listeners: {
			itemdblclick: '@{selectRecord}'
		}
	}, {
		xtype: 'form',
		bodyPadding: '10px',
		items: [{
			xtype: 'textfield',
			name: 'tagName',
			anchor: '-20'
		}]
	}],
	buttonAlign: 'left',
	buttons: ['select', 'newTag', 'create', 'cancel'],
	listeners: {
		resize: function() {
			this.center()
		}
	}
})
glu.defView('RS.catalog.TrainingViewer', {
	layout: 'border',
	border: false,
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		style: 'padding: 12px 0px 2px 18px',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{name}'
		}]
	}],
	items: [{
		region: 'west',
		xtype: 'treepanel',
		cls: 'training',
		animate: false,
		useArrows: true,
		width: '@{sideWidth}',
		minWidth: 225,
		maxWidth: 500,
		store: '@{catalogTreeStore}',
		hideHeaders: true,
		split: true,
		autoScroll: true,
		border: false,
		rootVisible: false,
		collapseMode: 'mini',
		tbar: [{
			xtype: 'button',
			iconCls: 'icon-chevron-left rs-icon-button',
			text: '~~previous~~',
			// tooltip: '~~previousSlideTooltip~~',
			handler: function(button) {
				var tree = button.up('treepanel'),
					sm = tree.getSelectionModel(),
					current = sm.getSelection()[0],
					previous = current.previousSibling;
				if (previous && !previous.isExpanded()) {
					previous.expand();
				}
				sm.selectPrevious();
			}
		}, {
			xtype: 'button',
			iconCls: 'icon-chevron-right rs-icon-button',
			iconAlign: 'right',
			text: '~~next~~',
			// tooltip: '~~nextSlideTooltip~~',
			handler: function(button) {
				var tree = button.up('treepanel'),
					r = tree.getSelection(),
					nextRecord = tree.getNext(r);
				if (nextRecord) {
					nextRecord.parentNode.expand();
					tree.getSelectionModel().select(nextRecord);
				} else {
					// Assert: nextRecord is falsey
					tree.getSelectionModel().select(tree.getRootNode().childNodes[0]);
				}
			}
		}, '->', {
			xtype: 'button',
			itemId: 'pinTool',
			iconCls: 'icon-pushpin rs-icon-button',
			// tooltip: '~~pinTooltip~~',
			handler: function(button) {
				button.up('treepanel').toggleAutoCollapse()
			}
		}],
		getSelection: function() {
			return this.getSelectionModel().getSelection()[0];
		},
		getNextUncle: function(node) {
			// Gets the next parent node's next sibling. If that
			// doesn't have a sibling, go up again. Hitting root
			// returns null.
			if (node.isRoot()) {
				return null;
			} else {
				if (node.parentNode.nextSibling) {
					return node.parentNode.nextSibling;
				} else {
					return this.getNextUncle(node.parentNode);
				}
			}
		},

		getNext: function(node) {
			// returns the next node from the specified node
			// This should probably be a method on the tree.
			var result = null;
			if (node) {
				if (node.hasChildNodes()) {
					result = node.firstChild;
				} else if (node.nextSibling) {
					result = node.nextSibling;
				} else if (node.parentNode) {
					// No next siblings or kids -- must be the last child
					return this.getNextUncle(node);
				}
			}
			return result;
		},
		columns: [{
			xtype: 'treecolumn',
			header: '~~catalogItems~~',
			flex: 1,
			dataIndex: 'name',
			width: Ext.isIE6 ? null : 10000
		}],
		listeners: {
			selectionchange: '@{catalogSelectionChanged}',
			afterrender: function(tree) {
				tree.addCls(tree.autoWidthCls)
				Ext.defer(function() {
					tree.getEl().on('mouseover', function(e, target) {
						tree.expandIfNeeded()
					})

					tree.splitter.getEl().on('mouseover', function(e, target) {
						tree.expandIfNeeded()
					})

					// Ext.create('Ext.tip.ToolTip', {
					// 	target: tree.getHeader().getEl(),
					// 	html: tree.title
					// })
				}, 1)
			},
			itemClick: '@{itemClick}'
		},
		getPinTool: function() {
			return this.down('#pinTool');
		},
		autoCollapse: false,
		getAutoCollapse: function() {
			return this.autoCollapse
		},
		setAutoCollapse: function(val) {
			this.updateAutoCollapse(val, this.autoCollapse)
			this.autoCollapse = val
		},
		expandIfNeeded: function() {
			if (this.getAutoCollapse() && this.collapsed) {
				this.expand();
			}
		},
		collapseIfNeeded: function() {
			if (this.getAutoCollapse() && !this.collapsed) {
				this.collapse();
			}
		},
		toggleAutoCollapse: function() {
			this.setAutoCollapse(!this.getAutoCollapse());
		},
		updateAutoCollapse: function(newValue, oldValue) {
			// Setting to true means it's un-pinned.
			var tool = this.getPinTool();
			if (tool) {
				tool.setIconCls(newValue ? 'icon-pushpin rs-icon-button icon-rotate-90' : 'icon-pushpin rs-icon-button')
				this.saveState()
			}
		}
	}, {
		region: 'center',
		border: false,
		layout: 'card',
		activeItem: '@{activeCard}',
		items: [{
			html: '@{selectedWikiUrl}',
			listeners: {
				afterrender: function(panel) {
					panel.getEl().on('mouseover', function(e, target) {
						panel.up('panel').up('panel').down('treepanel').collapseIfNeeded();
					}, this);
				}
			}
		}, {
			xtype: 'grid',
			store: '@{listOfDocuments}',
			hideHeaders: true,
			maxImageWidth: '@{listOfDocumentsMaxImageWidth}',
			setMaxImageWidth: function(value) {
				this.maxImageWidth = value
			},
			columns: [{
				dataIndex: 'utitle',
				flex: 1,
				renderer: function(value, metaData, record) {
					var summary = record.get('usummary'),
						doc = document.createElement('div');
					doc.innerHTML = summary;
					if (doc.innerHTML !== summary) summary = Ext.String.htmlEncode(summary)

					var attachmentStr = '';
					if (record.get('attachments').length > 0) {
						var style = this.maxImageWidth > 0 ? 'max-width:' + this.maxImageWidth + 'px' : '';
						Ext.String.format('<img src="/resolve/service/wiki/download/rs/rs?attach={0}&OWASP_CSRFTOKEN={1}" style="{2}" />',
							record.get('attachments')[0],
							clientVM.getPageToken('service/wiki/download/rs/rs'),
							style)
					}

					return Ext.String.format('<table><tr><td colspan=2 style="font-size: 16px; font-weight: bold;padding: 4px">{0}</td></tr><tr><td>{1}</td><td style="font-size: 14px;padding: 8px">{2}</td></tr></table>',
						record.get('utitle') || record.get('ufullname'),
						attachmentStr,
						Ext.util.Format.nl2br(summary));
				}
			}],
			listeners: {
				itemclick: '@{documentClicked}',
				afterrender: function(panel) {
					panel.getEl().on('mouseover', function(e, target) {
						panel.up('panel').up('panel').down('treepanel').collapseIfNeeded();
					}, this);
				}
			}
		}]
	}],
	listeners: {
		afterrender: function() {
			var splitters = this.query('bordersplitter')
			Ext.Array.forEach(splitters, function(splitter) {
				splitter.setSize(1, 1)
				splitter.getEl().setStyle({
					backgroundColor: '#fbfbfb'
				})
			})
		}
	}
})
/*
Ext.define('RS.common.ImagePicker', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.imagepicker',

	// allowRemove: true,

	emptyText: '',
	allowDeselect: true,

	anchor: '-20',
	height: 300,

	// resizable: true,
	// resizeHandles: 's',

	fileServiceListUrl: '',
	fileServiceUploadUrl: '',

	uploader: {
		url: '/resolve/service',
		uploadpath: 'dev',
		autoStart: true,
		max_file_size: '2020mb',
		flash_swf_url: '/resolve/js/plupload/plupload.flash.swf',
		urlstream_upload: true,
		resize: {
			width: 120,
			height: 90,
			quality: 100
		},
		filters: [{
			title: 'Image files',
			extensions: 'jpg,gif,png'
		}]

	},

	initComponent: function() {
		var me = this

		me.title = me.displayName || me.title

		me.columns = [{
			text: RS.formbuilder.locale.uploadColumnName,
			flex: 1,
			sortable: false,
			dataIndex: 'name'
		}, {
			text: RS.formbuilder.locale.uploadColumnSize,
			width: 90,
			sortable: true,
			align: 'right',
			renderer: Ext.util.Format.fileSize,
			dataIndex: 'size'
		}, {
			text: RS.formbuilder.locale.uploadColumnChange,
			width: 75,
			sortable: true,
			hidden: true,
			hideable: false,
			dataIndex: 'percent'
		}, {
			text: RS.formbuilder.locale.uploadColumnState,
			width: 75,
			hidden: true,
			sortable: true,
			hideable: false,
			dataIndex: 'status'
		}, {
			text: RS.formbuilder.locale.uploadColumnStatus,
			width: 175,
			sortable: true,
			dataIndex: 'msg'
		}, {
			xtype: 'actioncolumn',
			hideable: false,
			hidden: !this.allowDownload,
			width: 25,
			items: [{
				icon: '/resolve/images/arrow_down.png',
				// Use a URL in the icon config
				tooltip: RS.formbuilder.locale.download,
				handler: function(grid, rowIndex, colIndex) {
					var rec = grid.getStore().getAt(rowIndex)
					if (rec) grid.ownerCt.downloadURL(Ext.String.format('/resolve/service/catalog/download?id={0}', rec.get('id'), grid.ownerCt.fileUploadTableName));
					else Ext.Msg.alert(RS.formbuilder.locale.noDownloadTitle, RS.formbuilder.locale.noDownloadBody)
				}
			}]
		}]

		me.uploader = me.createUploader()
		me.store = me.uploader.store

		var tbarItems = this.tbar && this.tbar.items ? this.tbar.items : []
		this.tbar = [{
			text: RS.formbuilder.locale.upload,
			name: 'uploadFile',
			itemId: 'uploadButton',
			tooltip: RS.formbuilder.locale.uploadTooltip,
			hidden: !this.allowUpload
		}, {
			text: RS.formbuilder.locale.removeFile,
			disabled: true,
			name: 'removeFile',
			itemId: 'removeButton',
			hidden: !this.allowRemove,
			scope: this,
			handler: function(button) {
				var selections = this.getSelectionModel().getSelection();
				Ext.MessageBox.show({
					title: RS.formbuilder.locale.confirmRemove,
					msg: selections.length === 1 ? RS.formbuilder.locale.confirmRemoveFile : Ext.String.format(RS.formbuilder.locale.confirmRemoveFiles, selections.length),
					buttons: Ext.MessageBox.YESNO,
					buttonText: {
						yes: RS.formbuilder.locale.removeFile,
						no: RS.formbuilder.locale.cancel
					},
					scope: me,
					fn: me.removeSelectedFiles
				})
			}
		}].concat(tbarItems)

		me.statusBar = new Ext.ux.StatusBar({
			dock: 'bottom',
			defaultText: RS.formbuilder.locale.ready
		})
		me.dockedItems = [me.statusBar]

		me.callParent()

		Ext.apply(me.uploader, {
			url: this.fileServiceUploadUrl,
			browse_button: this.down('#uploadButton').id,
			container: this.down('#uploadButton').ownerCt.id,
			drop_element: this.id,
			multipart_params: me.uploader.multipart_params || {
				recordId: me.recordId,
				tableName: me.fileUploadTableName
			},
			statusQueuedText: RS.formbuilder.locale.statusQueuedText,
			statusUploadingText: RS.formbuilder.locale.statusUploadingText,
			statusFailedText: Ext.String.format('<span style="color: red">{0}</span>', RS.formbuilder.locale.statusFailedText),
			statusDoneText: Ext.String.format('<span style="color: green">{0}</span>', RS.formbuilder.locale.statusDoneText),

			statusInvalidSizeText: RS.formbuilder.locale.statusInvalidSizeText,
			statusInvalidExtensionText: RS.formbuilder.locale.statusInvalidExtensionText
		});

		if (me.uploader.drop_element && (e = Ext.getCmp(me.uploader.drop_element))) {
			e.addListener('afterRender', function() {
				me.uploader.initialize();
			}, {
				single: true,
				scope: me
			})
		} else {
			me.listeners = {
				afterRender: {
					fn: function() {
						me.uploader.initialize();
						me.setEnabled(true);
					},
					single: true,
					scope: me
				}
			}
		}

		me.relayEvents(me.uploader, ['beforestart', 'uploadready', 'uploadstarted', 'uploadcomplete', 'uploaderror', 'filesadded', 'beforeupload', 'fileuploaded', 'updateprogress', 'uploadprogress', 'storeempty']);

		me.on({
			scope: this,
			afterlayout: function(grid, layout, eOpts) {
				var upEl = Ext.get(grid.uploader.uploader.id + '_' + grid.uploader.uploader.runtime + '_container');
				if (upEl) upEl.setTop(0).setWidth(100);
			},
			afterrender: function(grid) {
				grid.view.refresh();
			},
			selectionchange: function(selectionModel, selections, eOpts) {
				if (selections.length > 0) this.down('#removeButton').enable();
				else this.down('#removeButton').disable();
			},
			updateprogress: function(uploader, total, percent, sent, success, failed, queued, speed) {
				var uploading = false;
				Ext.each(uploader.uploader.files, function(file) {
					if (file.status != plupload.DONE) uploading = true;
					return false;
				});

				if (uploading) {
					var t = Ext.String.format(RS.formbuilder.locale.updateProgressText, percent, sent, total);
					this.statusBar.showBusy({
						text: t,
						clear: false
					});
					this.statusBar.isBusy = true;
				} else {
					this.statusBar.isBusy = false;
					this.statusBar.clearStatus();
					this.statusBar.setStatus({
						text: RS.formbuilder.locale.ready
					});
				}
			},
			uploadprogress: function(uploader, file, name, size, percent) {
				// me.statusBar.setText(name + ' ' + percent + '%');
			},
			uploadcomplete: function(uploader, success, failed) {
				this.statusBar.isBusy = false;
				this.statusBar.clearStatus();
				this.statusBar.setStatus({
					text: RS.formbuilder.locale.ready
				});
			}
		})
		if (me.fileServiceListUrl)
			me.loadFileUploads()
	},

	removeSelectedFiles: function(button) {
		if (button === 'yes') {
			var selections = this.getSelectionModel().getSelection(),
				ids = [],
				fileIds = [];
			Ext.each(selections, function(selection) {
				ids.push(selection.get('id'));
				fileIds.push(selection.get('id'));
			});
			Ext.each(fileIds, function(fileId) {
				this.uploader.removeFile(fileId)
			}, this);
			Ext.Ajax.request({
				url: '/resolve/service/catalogbuilder/deleteFile',
				params: {
					tableName: this.fileUploadTableName,
					ids: ids
				},
				scope: this,
				success: this.removeDeletedFiles
			})
		}
	},

	removeDeletedFiles: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			this.loadFileUploads();
		}
	},

	loadFileUploads: function() {
		Ext.Ajax.request({
			url: this.fileServiceListUrl,
			params: {
				sqlQuery: Ext.String.format("select * from {0} where {1}='{2}'", this.fileUploadTableName, this.referenceColumnName, this.recordId),
				type: 'table',
				useSql: true
			},
			success: this.loadFiles,
			scope: this
		})
	},

	loadFiles: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			if (response.records) {
				Ext.each(response.records, function(record) {
					Ext.apply(record, {
						id: record.id,
						name: record.filename,
						size: record.u_size
					})
				}, this);
				this.store.loadData(response.records, false);
			}
		}
	},

	/**
	 * @private
	 * /
	createUploader: function() {
		return Ext.create('RS.formbuilder.viewer.desktop.fields.PlUpload', this, Ext.applyIf({
			listeners: {}
		}, this));
	},

	setEnabled: function(enable) {
		if (enable) this.uploader.multipart_params.recordId = enable;
		enable = enable && !this.internalDisabled;
		var uploadButton = this.down('#uploadButton');
		uploadButton[enable ? 'enable' : 'disable']();
		uploadButton.ownerCt[enable ? 'enable' : 'disable']();
		if (this.uploader.uploader) this.uploader.uploader.disabled = !enable;
		this.view.emptyText = enable ? (this.uploader.uploader.features.dragdrop ? RS.formbuilder.locale.dragDropEnabledEmptyText : RS.formbuilder.locale.dragDropDisabledEmptyText) : RS.formbuilder.locale.noRecordIdForUpload;
		this.view.refresh();
		if (enable) this.uploader.uploader.refresh();
	},

	disable: function() {
		this.internalDisabled = true;
		this.setEnabled(false);
	},
	enable: function() {
		if (this.rendered) {
			this.internalDisabled = false;
			this.setEnabled(true);
		}
	},

	downloadURL: function(url) {
		var iframe;
		iframe = document.getElementById("hiddenDownloader");
		if (iframe === null) {
			iframe = document.createElement('iframe');
			iframe.id = "hiddenDownloader";
			iframe.style.visibility = 'hidden';
			document.body.appendChild(iframe);
		}
		iframe.src = url;
	},

	isUploading: function() {
		return this.statusBar.isBusy || false;
	}
});
*/
Ext.define('RS.catalog.ImagePickerField', {
	extend: 'Ext.form.field.Trigger',
	alias: 'widget.imagepickerfield',

	triggerBaseCls: 'x-icon-over icon-paper-clip icon-large',
	triggerCls: 'x-icon-over icon-paper-clip icon-large',

	initComponent: function() {
		var me = this;
		me.callParent()
		me.on('render', function() {
			me.getEl().on('click', function() {
				me.fireEvent('attach')
			})
		})
	},

	onTriggerClick: function() {
		this.fireEvent('attach', this)
	}
})
/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.catalog').locale = {
	catalogBuilder: 'Catalog Builder',
	catalog: 'Catalog',
	catalogs: 'Catalogs',

	duplicatePath: 'You cannot have duplicate node paths in the catalog.  Duplicate path detected \'{0}\'.',

	error: 'Error',
	cancel: 'Cancel',
	back: 'Back',

	save: 'Save',
	CatalogSaved: 'Catalog successfully saved',

	previous: 'Prev',
	next: 'Next',

	//Actions
	newText: 'New',
	createCatalog: 'Tile Catalog',
	createTrainingCatalog: 'Side Catalog',
	deleteCatalog: 'Delete',
	renameCatalog: 'Rename',
	copyCatalog: 'Copy',
	generateCatalog: 'Generate',
	viewCatalog: 'View',

	//Refresh Action
	refreshTitle: 'Confirm Refresh',
	refreshAction: 'Refresh',
	refreshMessage: 'Are you sure you want to refresh the current catalog?  Any changes you have made before saving will be lost!',

	//Delete Action
	deleteTitle: 'Confirm Deletion',
	deleteAction: 'Delete',
	deleteMessage: 'Are you sure you want to delete the selected catalog?',
	deletesMessage: 'Are you sure you want to delete the {0} selected catalogs?',

	//Generate Action
	generateTitle: 'Confirm Generation',
	generateAction: 'Generate',
	generateMessage: 'Are you sure you want to generate the selected catalog?',
	generatesMessage: 'Are you sure you want to generate the {0} selected catalogs?',

	//Copy/Rename Action
	duplicateName: 'A catalog with that name already exists',
	newNameRequired: 'Please provide a new catalog name',
	newName: 'New catalog name',

	//Columns
	name: 'Name',
	wiki: 'Wiki',
	type: 'Type',
	newCatalog: 'New Catalog',

	//Tags
	addTag: 'Add Tag',
	removeTag: 'Remove Tag',
	tags: 'Tags',

	//wikis
	fullname: 'Fullname',
	wikis: 'Documents',
	addWiki: 'Add Document',
	removeWiki: 'Remove Document',

	addItem: 'Add Item',
	addFolder: 'Add Folder',
	addGroup: 'Add Group Header',
	addCatalog: 'Add Catalog',
	removeItem: 'Remove',
	moveUp: 'Move Up',
	moveDown: 'Move Down',

	catalogItems: 'Catalog Items',

	namespace: 'Namespace',

	catalogItemTitle: 'Catalog Item',
	catalogFolderTitle: 'Catalog Folder',
	title: 'Title',
	description: 'Description',
	tooltip: 'Tooltip',
	wiki: 'Wiki',
	url: 'URL',
	link: 'URL',
	path: 'Path',
	image: 'Image',
	listOfDocuments: 'List of Documents',
	newCatalogItem: 'New Item',
	newCatalogReference: 'New Catalog Reference',
	newCatalogFolder: 'New Folder',

	form: 'Form',
	searchGrid: 'Search Grid',

	displayType: 'Display Type',

	ImagePicker: 'Image Picker',
	choose: 'Select',
	imageName: 'Image',
	idealImageSizeMessage: 'Images are scaled to 120px X 90px',

	openInNewTab: 'Open in new tab',

	maxImageWidth: 'Max Image Width',

	Catalog: {
		viewCatalog: 'View'
	},

	CatalogReference: {
		sideWidth: 'Width',
		catalogDetailTitle: 'Catalog Reference',
		catalog: 'Catalog',
		name: 'Name',
		catalogReferenceTitle: 'Catalog Reference',
		catalogTitle: 'Catalog',
		catalogId: 'Name'
	},

	CatalogGroup: {
		catalogGroupTitle: 'Catalog Group Header',
		name: 'Name',
		newCatalogGroup: 'New Group Header',
		catalogGroupTitle: 'Catalog Group Header'
	},

	TagPicker: {
		select: 'Select',
		create: 'Create',
		selectExistingTag: 'Select Existing Tag',
		newTag: 'New Tag',
		name: 'Name',
		selectTag: 'Select Tag',
		createTag: 'Create Tag',
		tagName: 'Name',
		searchTag: 'Search',
		SaveTagErrMsg: 'The server currently cannot handle this request.[{msg}]',
		invalidName: 'Name is required and should only contain alphanumerics and \'_\', continuous underscores are not allowed.'
	},

	CatalogViewer: {
		wrongCatalogTypeTitle: 'Wrong Catalog Type',
		wrongCatalogTypeMessage: 'You are trying to load a catalog of type "Training" in the wrong viewer.  Please use the Training Viewer instead'
	},

	TrainingViewer: {
		previousSlideTooltip: 'Go to the previous slide',
		nextSlideTooltip: 'Go to the next slide',
		pinTooltip: 'Pinned toolbar does not auto-collapse (click to toggle)',
		wrongCatalogTypeTitle: 'Wrong Catalog Type',
		wrongCatalogTypeMessage: 'You are trying to load a catalog of type "Catalog" in the wrong viewer.  Please use the Catalog Viewer instead'
	},

	CatalogPicker: {
		catalogPickerTitle: 'Select a Catalog Path',

		select: 'Select',
		cancel: 'Cancel'
	}
}
