<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.resolve.util.JspUtils" %>
<%@page import="org.owasp.esapi.ESAPI"%>
<%
    String ver = ESAPI.encoder().encodeForURL(JspUtils.getResolveVersion());

    boolean debug = false;
    String d = request.getParameter("debug");
    if( d != null && d.indexOf("t") > -1){
        debug = true;
    }

    boolean debugTag = false;
    String dT = request.getParameter("debugTag");
    if( dT != null && dT.indexOf("t") > -1){
        debugTag = true;
    }

    boolean mock = false;
    String m = request.getParameter("mock");
    if( m != null && m.indexOf("t") > -1){
        mock = true;
    }
%>

<%
    if( debug || debugTag ){
%>
<script type="text/javascript" src="/resolve/tag/js/tag-classes.js?_v=<%=ver%>"></script>
<%
    }else{
%>
<script type="text/javascript" src="/resolve/tag/js/tag-classes.js?_v=<%=ver%>"></script>
<%
    }
%>

