glu.defView('RS.tag.Tags', {
	layout: 'fit',
	padding: 15,
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['createTag', 'deleteTags']
	}],
	xtype: 'grid',
	name: 'grid',
	cls : 'rs-grid-dark',
	displayName: '@{displayName}',
	store: '@{gridStore}',
	columns: '@{gridColumns}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.editColumnTooltip,
		glyph : 0xF044,
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},
	listeners: {
		editAction: '@{editTag}'
	}
});