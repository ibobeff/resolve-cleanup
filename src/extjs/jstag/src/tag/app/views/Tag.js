glu.defView('RS.tag.Tag', {
	title: '@{title}',
	xtype: 'form',
	modal: true,
	autoScroll: true,
	cls : 'rs-modal-popup',
	padding : 15,
	width: 600,
	height: 300,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},	
	items: [{
		xtype: 'textfield',
		name: 'name'
	}, {
		xtype: 'textarea',
		name: 'description',
		flex: 1
	}],
	buttons: [{
		name : 'save',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light',
	}]	
});