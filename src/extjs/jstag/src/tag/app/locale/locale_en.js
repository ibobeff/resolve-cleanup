/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.tag').locale = {
	windowTitle: 'Tag Project',
	Name: 'Name',
	Description: 'Description',
	Module: 'Module',
	LastUpdated: 'Last Updated On',
	name: 'Name',
	module: 'Module',
	description: 'Description',
	sysCreatedOn: 'Created On',
	sysCreatedBy: 'Created By',
	sysUpdatedOn: 'Updated On',
	sysUpdatedBy: 'Updated By',

	sys_created_on: 'Created On',
	sys_created_by: 'Created By',
	sys_updated_on: 'Updated On',
	sys_updated_by: 'Updated By',

	sysOrganizationName: 'Organization',
	sysId: 'Sys ID',
	Tags: {
		createTag: 'New',
		deleteTags: 'Delete',
		DeleteTagsTitle: 'Delete Tags',
		DeleteTagMsg: 'Are you sure you want to delete the selected item?',
		DeleteTagsMsg: 'Are you sure you want to delete the selected items?({num} items selected)',
		confirmDeleteTags: 'Delete',
		cancel: 'Cancel',
		displayName: 'Tags',
		GetChildrenErrTitle: 'Cannot Get Namespace',
		GetChildrenErrMsg: 'Can not get namespaces from the server.',
		confirmErr: 'OK',
		ServerErrTitle: 'Server Error',
		ServerErrMsg: 'The server currently cannot handle the request',
		confirmLoadTagErr: 'OK',
		ListTagsErrMsg: 'Cannot get tags from server',
		GetLayerErrMsg: 'Cannot get items under this folder',
		DeleteTagsErrMsg: 'Cannot delete the selected tags.',
		DeleteTagSucMsg: 'The selected tags have been deleted.'
	},
	Tag: {
		ServerErrTitle: 'Server Error',
		ServerErrMsg: 'The server currently cannot handle the request.',
		cancel: 'Cancel',
		save: 'Save',
		invalidName: 'Only alphanumeric and underscore are allowed.',
		SaveTagErrTitle: 'Cannot Save Tag',
		SaveTagErrMsg: 'The server currently cannot handle this request.',
		confirmErr: "OK",
		SaveTagSucTitle: 'Tag Saved',
		SaveTagSucMsg: 'The tag has been successfully saved.',
		confirmSaveTagSuc: 'OK'
	}
}