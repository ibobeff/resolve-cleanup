glu.defModel('RS.tag.Tags', {

	mock: false,
	wait: false,
	displayName: '',

	gridStore: {
		mtype: 'store',
		fields: ['id', 'name', 'description'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/tag/listTags',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	gridColumns: null,
	gridSelections: [],

	activate: function() {
		window.document.title = this.localize('windowTitle');
	},

	init: function() {
		if (this.mock) this.backend = RS.tag.createMockBackend(true);

		this.gridStore.on('beforeload', function(store, operation, opts) {
			this.set('wait', true);
		}, this);
		this.gridStore.on('load', function(store, records, suc, opts) {
			/*
			if (!suc) {
				clientVM.displayError(this.localize('ListTagsErrMsg'));
			}
			*/
			this.set('wait', false);
		}, this);

		this.set('displayName', this.localize('displayName'));
		this.set('gridColumns', [{
			header: '~~Name~~',
			dataIndex: 'name',
			filterable: true,
			sortable: true,
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~Description~~',
			dataIndex: 'description',
			filterable: false,
			sortable: false,
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		}].concat(RS.common.grid.getSysColumns()));

		this.loadTags();
	},

	loadTags: function(page) {
		this.gridStore.load();
	},

	editTag: function(id) {
		this.open({
			mtype: 'RS.tag.Tag',
			id: id
		});
	},
	createTag: function() {
		this.open({
			mtype: 'RS.tag.Tag',
			height: 500
		});
	},

	deleteTags: function() {
		this.message({
			title: this.localize('DeleteTagsTitle'),
			msg: this.localize(this.gridSelections.length > 1 ? 'DeleteTagsMsg' : 'DeleteTagMsg', {
				num: this.gridSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirmDeleteTags'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.sendDeleteTagsReq
		});
	},

	sendDeleteTagsReq: function(btn) {
		if (btn == 'no')
			return;

		var ids = [];
		Ext.each(this.gridSelections, function(r) {
			ids.push(r.get('id'));
		});

		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/tag/deleteTags',
			params: {
				ids: ids
			},
			scope: this,
			success: this.handleDeleteTagsReqSuc,
			failure: function(resp) {
				clientVM.displayFailure(resp);
				this.loadTags();
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},

	handleDeleteTagsReqSuc: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(this.localize('DeleteTagsErrMsg') + '[' + respData.message + ']');
		else
			clientVM.displaySuccess(this.localize('DeleteTagSucMsg'));

		this.loadTags();
	},

	createTagIsEnabled$: function() {
		return !this.wait;
	},

	deleteTagsIsEnabled$: function() {
		return this.gridSelections.length > 0 && !this.wait;
	}
});