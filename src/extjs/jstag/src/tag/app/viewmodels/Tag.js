glu.defModel('RS.tag.Tag', {
	title: 'Tag',
	state: '',
	id: '',
	name: '',
	description: '',

	fields: ['id', 'name', 'description'],

	init: function() {
		if (this.id != '') {
			this.loadTag();
		}
	},

	loadTag: function() {
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/tag/getTag',
			params: {
				id: this.id
			},
			scope: this,
			success: this.handleLoadTagSuc,
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	setData: function(data) {
		this.loadData(data);
		this.commit();
	},

	handleLoadTagSuc: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('LoadTagErrMsg') + '[' + respData.message + ']');
		} else {
			this.setData(respData.data);
		}
	},

	save: function() {
		this.saveTag(true);
		this.set('state', 'wait');
	},
	saveIsEnabled$: function() {
		return this.state != 'wait' && this.isValid & this.isDirty
	},

	saveTag: function(exitAfterSave) {
		var data = {
			id: this.id,
			name: this.name,
			description: this.description
		};
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/tag/saveTag',
			jsonData: data,
			scope: this,
			success: function(resp) {
				this.handleSaveTagSuc(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		})
	},

	handleSaveTagSuc: function(resp) {
		var respData = RS.common.parsePayload(resp);
		this.commit();
		if (!respData.success) {
			clientVM.displayError(this.localize('SaveTagErrMsg') + '[' + respData.message + ']');
			return;
		} else {
			clientVM.displaySuccess(this.localize('SaveTagSucMsg'));
			this.parentVM.loadTags();
		}
		this.cancel();
	},

	cancel: function() {
		this.doClose();
	},
	checkName: function(name) {
		var valid = (/^[a-zA-Z0-9_\-\/]+$/.test(name) ||
			/^[a-zA-Z0-9_\-\/][a-zA-Z0-9_\-\/\s\.]*[a-zA-Z0-9_\-\/]$/.test(name)) && !/.*\s{2,}.*/.test(name) && !/.*(\.\.|\s+\.|\.\s+).*/.test(name);
		return name && valid ? true : this.localize('invalidName');
	},
	nameIsValid$: function() {
		this.isValid = /^[a-zA-Z\d\_]+$/.test(this.name) && !this.name.replace(/_/g, "").length == 0;
		return this.isValid || this.localize('invalidName')
	}
});