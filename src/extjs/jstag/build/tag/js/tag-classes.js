/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/
glu.defModel('RS.tag.Tag', {
	title: 'Tag',
	state: '',
	id: '',
	name: '',
	description: '',

	fields: ['id', 'name', 'description'],

	init: function() {
		if (this.id != '') {
			this.loadTag();
		}
	},

	loadTag: function() {
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/tag/getTag',
			params: {
				id: this.id
			},
			scope: this,
			success: this.handleLoadTagSuc,
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	setData: function(data) {
		this.loadData(data);
		this.commit();
	},

	handleLoadTagSuc: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('LoadTagErrMsg') + '[' + respData.message + ']');
		} else {
			this.setData(respData.data);
		}
	},

	save: function() {
		this.saveTag(true);
		this.set('state', 'wait');
	},
	saveIsEnabled$: function() {
		return this.state != 'wait' && this.isValid & this.isDirty
	},

	saveTag: function(exitAfterSave) {
		var data = {
			id: this.id,
			name: this.name,
			description: this.description
		};
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/tag/saveTag',
			jsonData: data,
			scope: this,
			success: function(resp) {
				this.handleSaveTagSuc(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		})
	},

	handleSaveTagSuc: function(resp) {
		var respData = RS.common.parsePayload(resp);
		this.commit();
		if (!respData.success) {
			clientVM.displayError(this.localize('SaveTagErrMsg') + '[' + respData.message + ']');
			return;
		} else {
			clientVM.displaySuccess(this.localize('SaveTagSucMsg'));
			this.parentVM.loadTags();
		}
		this.cancel();
	},

	cancel: function() {
		this.doClose();
	},
	checkName: function(name) {
		var valid = (/^[a-zA-Z0-9_\-\/]+$/.test(name) ||
			/^[a-zA-Z0-9_\-\/][a-zA-Z0-9_\-\/\s\.]*[a-zA-Z0-9_\-\/]$/.test(name)) && !/.*\s{2,}.*/.test(name) && !/.*(\.\.|\s+\.|\.\s+).*/.test(name);
		return name && valid ? true : this.localize('invalidName');
	},
	nameIsValid$: function() {
		this.isValid = /^[a-zA-Z\d\_]+$/.test(this.name) && !this.name.replace(/_/g, "").length == 0;
		return this.isValid || this.localize('invalidName')
	}
});
glu.defModel('RS.tag.Tags', {

	mock: false,
	wait: false,
	displayName: '',

	gridStore: {
		mtype: 'store',
		fields: ['id', 'name', 'description'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/tag/listTags',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	gridColumns: null,
	gridSelections: [],

	activate: function() {
		window.document.title = this.localize('windowTitle');
	},

	init: function() {
		if (this.mock) this.backend = RS.tag.createMockBackend(true);

		this.gridStore.on('beforeload', function(store, operation, opts) {
			this.set('wait', true);
		}, this);
		this.gridStore.on('load', function(store, records, suc, opts) {
			/*
			if (!suc) {
				clientVM.displayError(this.localize('ListTagsErrMsg'));
			}
			*/
			this.set('wait', false);
		}, this);

		this.set('displayName', this.localize('displayName'));
		this.set('gridColumns', [{
			header: '~~Name~~',
			dataIndex: 'name',
			filterable: true,
			sortable: true,
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~Description~~',
			dataIndex: 'description',
			filterable: false,
			sortable: false,
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		}].concat(RS.common.grid.getSysColumns()));

		this.loadTags();
	},

	loadTags: function(page) {
		this.gridStore.load();
	},

	editTag: function(id) {
		this.open({
			mtype: 'RS.tag.Tag',
			id: id
		});
	},
	createTag: function() {
		this.open({
			mtype: 'RS.tag.Tag',
			height: 500
		});
	},

	deleteTags: function() {
		this.message({
			title: this.localize('DeleteTagsTitle'),
			msg: this.localize(this.gridSelections.length > 1 ? 'DeleteTagsMsg' : 'DeleteTagMsg', {
				num: this.gridSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirmDeleteTags'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.sendDeleteTagsReq
		});
	},

	sendDeleteTagsReq: function(btn) {
		if (btn == 'no')
			return;

		var ids = [];
		Ext.each(this.gridSelections, function(r) {
			ids.push(r.get('id'));
		});

		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/tag/deleteTags',
			params: {
				ids: ids
			},
			scope: this,
			success: this.handleDeleteTagsReqSuc,
			failure: function(resp) {
				clientVM.displayFailure(resp);
				this.loadTags();
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},

	handleDeleteTagsReqSuc: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success)
			clientVM.displayError(this.localize('DeleteTagsErrMsg') + '[' + respData.message + ']');
		else
			clientVM.displaySuccess(this.localize('DeleteTagSucMsg'));

		this.loadTags();
	},

	createTagIsEnabled$: function() {
		return !this.wait;
	},

	deleteTagsIsEnabled$: function() {
		return this.gridSelections.length > 0 && !this.wait;
	}
});
glu.defView('RS.tag.Tag', {
	title: '@{title}',
	xtype: 'form',
	modal: true,
	autoScroll: true,
	cls : 'rs-modal-popup',
	padding : 15,
	width: 600,
	height: 300,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},	
	items: [{
		xtype: 'textfield',
		name: 'name'
	}, {
		xtype: 'textarea',
		name: 'description',
		flex: 1
	}],
	buttons: [{
		name : 'save',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light',
	}]	
});
glu.defView('RS.tag.Tags', {
	layout: 'fit',
	padding: 15,
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['createTag', 'deleteTags']
	}],
	xtype: 'grid',
	name: 'grid',
	cls : 'rs-grid-dark',
	displayName: '@{displayName}',
	store: '@{gridStore}',
	columns: '@{gridColumns}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.editColumnTooltip,
		glyph : 0xF044,
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},
	listeners: {
		editAction: '@{editTag}'
	}
});
/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.tag').locale = {
	windowTitle: 'Tag Project',
	Name: 'Name',
	Description: 'Description',
	Module: 'Module',
	LastUpdated: 'Last Updated On',
	name: 'Name',
	module: 'Module',
	description: 'Description',
	sysCreatedOn: 'Created On',
	sysCreatedBy: 'Created By',
	sysUpdatedOn: 'Updated On',
	sysUpdatedBy: 'Updated By',

	sys_created_on: 'Created On',
	sys_created_by: 'Created By',
	sys_updated_on: 'Updated On',
	sys_updated_by: 'Updated By',

	sysOrganizationName: 'Organization',
	sysId: 'Sys ID',
	Tags: {
		createTag: 'New',
		deleteTags: 'Delete',
		DeleteTagsTitle: 'Delete Tags',
		DeleteTagMsg: 'Are you sure you want to delete the selected item?',
		DeleteTagsMsg: 'Are you sure you want to delete the selected items?({num} items selected)',
		confirmDeleteTags: 'Delete',
		cancel: 'Cancel',
		displayName: 'Tags',
		GetChildrenErrTitle: 'Cannot Get Namespace',
		GetChildrenErrMsg: 'Can not get namespaces from the server.',
		confirmErr: 'OK',
		ServerErrTitle: 'Server Error',
		ServerErrMsg: 'The server currently cannot handle the request',
		confirmLoadTagErr: 'OK',
		ListTagsErrMsg: 'Cannot get tags from server',
		GetLayerErrMsg: 'Cannot get items under this folder',
		DeleteTagsErrMsg: 'Cannot delete the selected tags.',
		DeleteTagSucMsg: 'The selected tags have been deleted.'
	},
	Tag: {
		ServerErrTitle: 'Server Error',
		ServerErrMsg: 'The server currently cannot handle the request.',
		cancel: 'Cancel',
		save: 'Save',
		invalidName: 'Only alphanumeric and underscore are allowed.',
		SaveTagErrTitle: 'Cannot Save Tag',
		SaveTagErrMsg: 'The server currently cannot handle this request.',
		confirmErr: "OK",
		SaveTagSucTitle: 'Tag Saved',
		SaveTagSucMsg: 'The tag has been successfully saved.',
		confirmSaveTagSuc: 'OK'
	}
}
