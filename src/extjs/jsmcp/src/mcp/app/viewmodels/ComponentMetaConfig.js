glu.ns("RS.mcp");
RS.mcp.MetaConfigMap = {
	severityStore : {
		mtype : 'store',
		proxy : {
			type :'memory'
		},
		fields : ['display','UAlertStatus'],
		data : [
			{	display : 'Fatal',
				UAlertStatus : 'fatal'
			},{
				display : 'Critical',
				UAlertStatus : 'critical'
			},{
				display : 'Warning',
				UAlertStatus : 'warning'
			},{
				display : 'Severe',
				UAlertStatus : 'severe'
			},{
				display : 'Info',
				UAlertStatus : 'info'
			}
		]
	},
	metricGroupStore : {
		mtype : 'store',
		proxy : {
			type : 'memory'
		},
		fields : ['display', 'UGroup'],
		data : [
			{
				display : 'JVM',
				UGroup : 'jvm'
			},{
				display : 'DATABASE',
				UGroup : 'database'
			},{
				display: 'JMS QUEUE',
				UGroup : 'jms_queue'
			},{
				display : 'LATENCY',
				UGroup : 'latency'
			},{
				display : 'RUNBOOK',
				UGroup : 'runbook'
			},{
				display : 'SERVER',
				UGroup : 'server'
			},{
				display : 'TRANSACTION',
				UGroup : 'transaction'
			},{
				display : 'USERS',
				UGroup :'users'
			}
		],
		sorters : [
		{
			property : 'display',
			direction : 'ASC'
		}]
	},
	metricNameStore : {
		mtype : 'store',
		proxy : {
			type : 'memory'
		},
		fields : ['display', 'UName'],
		data : [],
	},
	metricMetaData : {
		jvm : [
		{
			display : 'Memory Free',
			UName : 'mem_free'
		},
		{
			display : 'Thread Count',
			UName : 'thread_count'
		}],
		database : [
		{
			display : 'Free Space (MB)',
			UName : 'free_space'
		},
		{
			display : 'Size (MB)',
			UName : 'size'
		},{
			display : 'Percentage Used (%)',
			UName : 'percentage_used'
		},{
			display : 'Response Time (msec)',
			UName : 'response_time'
		},{
			display : 'Query Count',
			UName : 'query_count'
		},{
			display : 'Percentage Wait (%)',
			UName : 'percentage_wait'
		}],
		jms_queue : [
		{
			display : 'Message Count',
			UName : 'msgs_count'
		}],
		latency : [
		{
			display : 'Opened Wiki Count',
			UName : 'wiki'
		},{
			display : 'Wiki Response Time',
			UName : 'wikiresponsetime'
		},{
			display : 'Social Page Visited',
			UName : 'social',
		},{
			display : 'Social Response Time',
			UName : 'socialresponsetime'
		}],
		runbook : [
		{
			display : 'Aborted Count',
			UName : 'aborted'
		},{
			display : 'Completed Count',
			UName : 'completed'
		},{
			display : 'Duration (msec)',
			UName : 'duration'
		},{
			display : 'Good Severity Count',
			UName : 'gseverity'
		},{
			display : 'Critical Severity Count',
			UName : 'cseverity'
		},{
			display : 'Warning Serverity Count',
			UName : 'wseverity'
		},{
			display : 'Severe Severity Count',
			UName : 'sseverity'
		},{
			display : 'Unknown Severity Count',
			UName : 'useverity'
		},{
			display : 'Good Condition Count',
			UName : 'gcondition'
		},{
			display : 'Bad Condition Count',
			UName : 'bcondition'
		},{
			display : 'Unknown Condition Count',
			UName : 'ucondition'
		}],
		server : [
		{
			display : 'Load Per Minute',
			UName : 'load1'
		},{
			display : 'Load Per 5 Minutes',
			UName : 'load5'
		},{
			display : 'Load Per 15 Minutes',
			UName : 'load15'
		},{
			display : 'Server Disk Space',
			UName : 'disk_space'
		}],
		transaction : [
		{
			display : 'Count',
			UName : 'transaction'
		},{
			display : 'Latency',
			UName : 'latency'
		}],
		users : [
		{
			display : 'Active',
			UName : 'active'
		}]
	},
	operatorStore : {
		mtype : 'store',
		proxy : {
			type : 'memory'
		},
		fields : ['operator'],
		data : [
			{
				operator : '>',
				UName : 'gt'
			},
			{
				operator : '<',
				UName : 'lt'
			},
			{
				operator : '=',
				UName : 'eq'
			},
			{
				operator : '>=',
				UName : 'gte'
			},
			{
				operator : '<=',
				UName : 'lte'
			},
			// {
			// 	operator : 'contains'
			// }
		]
	}
}