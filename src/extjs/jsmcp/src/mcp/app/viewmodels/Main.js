glu.defModel("RS.mcp.Main", {
	displayName : '',
	thresholdStore: '',
	thresholdColumns: '',
	thresholdTabs : {
		mtype : 'RS.mcp.tabs.ThresholdTab'
	},
	wait : false,
	metricNameStore : RS.mcp.MetaConfigMap.metricNameStore,
	metricGroupStore : RS.mcp.MetaConfigMap.metricGroupStore,
	metricMetaData : RS.mcp.MetaConfigMap.metricMetaData,	
	selectedThresholdId : null,
	selectedThresholdModule : null,
	selectedThresholdName : null,
	selectedRecord : null,
	thresholdUpdated : false,
	API : {
		thresholdRuleList : '/resolve/service/metric/list',
		thresholdRuleDelete : '/resolve/service/metric/delete',
		getClusterList: '/resolve/service/mcplogin/getclusterlist'
	},
	isAdmin: false,
	groupName: '',
	clusterList: null,
	clusterListValid: false,
	init : function(){
		this.set('displayName', this.localize('thresholdRuleDisplayName'));
		this.initThresholdStore();
		this.initThresholdColumns();
		this.thresholdStore.on('load', function(){
			if(this.thresholdUpdated){
				this.reSelectThreshold();
				this.set('thresholdUpdated', false);
			}
		}, this)

		Ext.each(clientVM.user.roles, function(r) {
			if (r == 'admin') {
				this.set('isAdmin', true);
				return;
			}
		}, this);
	},
	activate : function(){
		this.thresholdStore.load();
		this.getClusterList();
	},
	getClusterList : function(){
		this.clusterList = [];
		this.set('clusterListValid', false);
		var params = {
			group : this.groupName,
		}
		this.ajax({
			url : this.API['getClusterList'],
			method : 'POST',
			params : params,
			success : function(resp){
				var respData = RS.common.parsePayload(resp);
				if (respData.success) {
					if (respData.data) {
						var clusterList = respData.data;
						for (var i=0; i< clusterList.length; i++) {
							this.clusterList.push(clusterList[i]);
							this.set('clusterListValid', true);
						}
					}
				} else {
					clientVM.displayError(respData.message);
				}
			},
			failure : function(resp){
				clientVM.displayFailure(resp);
			}
		});
	},
	initThresholdStore : function(){
		var me = this;
		var fields = ['uruleModule','uruleName','usource','ualertStatus',
			{
				name : 'ugroup',
				type : 'string',
				convert : function(value, record){
					var recordData = me.metricGroupStore.findRecord('UGroup', value);
					return recordData ? recordData.get('display') : value;
				}
			},{
				name : 'uname',
				type : 'string',
				convert : function(value, record){
					var grpName = record.raw.ugroup;
					var grpArr = me.metricMetaData[grpName] || [];
					for(var i = 0; i < grpArr.length; i++){
						var metricInfo = grpArr[i];
						if( metricInfo.UName == value){
							return metricInfo.display;
						}
					}
					return value;
				}
			},{
				name : 'uactive',
				type : 'bool'
			},{
				name : 'utype',
				type : 'string',
				convert : function(value, record){
					return (value && value.toLowerCase()) == 'default' ? true : false;
				}
			},{
				name : 'isOutdated'
			},{
				name : 'uversion',
				type : 'long',
				convert : function(value, record){
					if(!value || value == 0)
						return "None (not deployable)";
					else if(record.get('isOutdated'))
						return value + '*';
					else
						return value;
				}
			}
		].concat(RS.common.grid.getSysFields());

		var store = Ext.create('Ext.data.Store', {
			fields : fields,
			proxy : {
				type : 'ajax',
				url : this.API['thresholdRuleList'],
				reader : {
					type : 'json',
					root : 'records'
				}
			},
			sorters : [{
				property : 'uruleName',
				direction : 'ASC'
			}]
		})
		this.set('thresholdStore', store);
	},
	initThresholdColumns : function(){
		var columns = [
		{
			header : "Module",
			dataIndex : 'uruleModule',
			sortable  : true,
			filterable : true,
			flex : 3
		},{
			header : "Name",
			dataIndex : "uruleName",
			sortable : true,
			filterable : true,
			flex : 3
		},{
			header : "Version",
			dataIndex : "uversion",
			sortable : false,
			filterable : false,
			flex : 2
		},{
			header : "Active",
			dataIndex : "uactive",
			flex : 1,
			align : 'center',
			renderer : RS.common.grid.booleanRenderer()
		},{
			header : "Auto Deploy",
			dataIndex : "utype",
			flex : 1,
			align : 'center',
			renderer : RS.common.grid.booleanRenderer()
		},{
			header : "Severity",
			dataIndex : "ualertStatus",
			sortable : true,
			filterable : true,
			flex : 1
		},{
			header : "Metric Group",
			dataIndex : "ugroup",
			sortable : true,
			filterable : true,
			flex : 3
		},{
			header : "Metric Name",
			dataIndex : "uname",
			sortable : true,
			filterable : true,
			flex : 3
		},{
			header : "Source",
			dataIndex : "usource",
			sortable : true,
			filterable : true,
			flex : 3
		}].concat(RS.common.grid.getSysColumns());

		this.set('thresholdColumns', columns);
	},
	createThreshold : function(){
		this.open({
			mtype : 'ThresholdForm',
			mode : 1
		})
	},
	createThresholdIsEnabled$ : function(){
		return !this.wait;
	},

	copyThresholdIsEnabled$ : function(){
		return !this.wait && this.selectedThresholdId != null;
	},

	copyThreshold : function(){
		this.open({
			mtype : 'ThresholdForm',
			thresholdId : this.selectedThresholdId,
			mode : 2
		})
	},

	deleteThreshold : function(){
		this.message({			
			width : 450,
			title : this.localize('deleteTitle'),
			msg : this.localize('deleteMessage'),
			button : Ext.MessageBox.YESNO,
			buttonText : {
				yes : this.localize('ok'),
				no : this.localize('cancel')
			},
			fn : function(btn){
				if(btn == 'yes'){
					this.doDelete();
				}
			}
		})
	},
	doDelete : function(){
		var ids = [this.selectedThresholdId];
		this.set('wait', true);
		this.ajax({
			url : this.API['thresholdRuleDelete'],
			method : 'POST',
			params : {
				ids : ids
			},
			scope: this,
			success : function(resp){
				var respData = RS.common.parsePayload(resp);
				if (respData.success){
					this.deSelectThreshold();
					this.reloadThresholdTable();
					clientVM.displaySuccess(this.localize('deleteSucMsg'));
				}
			},
			failure : function(resp){
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},
	deleteThresholdIsEnabled$ : function(){
		return !this.wait && this.selectedThresholdId != null;
	},
	selectThreshold : function(record){
		this.set('selectedThresholdId', record.raw.sys_id);
		this.set('selectedThresholdModule', record.raw.uruleModule);
		this.set('selectedThresholdName',record.raw.uruleName);
		this.set('selectedRecord',record);
		this.updateThresholdInfo();
	},
	deSelectThreshold : function(){
		this.set('selectedThresholdId', null);
		this.set('selectedThresholdModule', null);
		this.set('selectedThresholdName',null);
		this.set('selectedRecord',null);
		this.thresholdTabs.tabs.forEach(function(tab){
			tab.set('thresholdSelected', false);
		})	
	},
	reSelectThreshold : function(){
		//Get the new record from the store using name and module of previous selected record.
		var records = this.thresholdStore.queryBy(function(record, id){
			return (record.get('uruleModule') == this.selectedThresholdModule && record.get('uruleName') == this.selectedThresholdName );
		}, this);
		this.set('selectedRecord',records.first());
		this.fireEvent('reSelectRecord');
	},
	reloadThresholdTable : function(updateThresholdFlag){
		this.set('thresholdUpdated', updateThresholdFlag == true ? true : false);
		this.thresholdStore.load();
	},
	updateThresholdInfo : function(){
		var id = this.selectedRecord.get('id');
		var ruleModule = this.selectedRecord.get('uruleModule');
		var ruleName = this.selectedRecord.get('uruleName');
		this.thresholdTabs.tabs.forEach(function(tab){
			tab.updateTabInfo(id, ruleModule, ruleName);
		})
	},
	mcpadmin : function() {
		this.open({
			mtype : 'RS.mcp.Admin',
			groupName: this.groupName,
			clusterList: this.clusterList
		})
	},
	mcpadminIsVisible$ : function() {
		if (this.isAdmin && this.clusterListValid) {
			return true;
		} else {
			return false;
		}
	}
})

