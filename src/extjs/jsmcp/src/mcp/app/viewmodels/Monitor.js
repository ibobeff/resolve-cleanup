glu.defModel('RS.mcp.Monitor',{
	hostInfoMap : {},
	hostTreeStore : {
		mtype: 'treestore',
        // fields: ['id','ustatus'],
        proxy: {
            type: 'ajax',
            url : '/resolve/service/mcp/getRegisteredInstances'
        },
        root: {
            name: 'All',
            expanded: false
        },
        autoLoad : false
	},
    view : function(){
        var root = this.hostTreeStore.getRootNode();
        var group = root.getChildAt(0);
        var child1 = group.getChildAt(0);
        var child2 = group.getChildAt(1);
        group.insertChild(0,child2);
    },
    statusPollingTask : null,
    pollingInterval : 10000,
	init : function(){
		var me = this;
        this.hostTreeStore.on('beforeappend', me.updateTree);
        //this.hostTreeStore.getRootNode().on('beforeexpand', me.updateTree);
	},
    activate : function(){
        this.hostTreeStore.getRootNode().expand();
        //this.startCheckingStatusTask();
    },
    updateTree : function(tree, node){
        var nodeData = node.raw;
        if(nodeData.hasOwnProperty('ugroupName')){
            node.set('iconCls', "group-level" );
            var grpName = nodeData.ugroupName;
            node.set('name', grpName);
        }
        else if(nodeData.hasOwnProperty('uclusterName')){
            node.set('iconCls', "cluster-level" );
            var clusterName = nodeData.uclusterName;
            node.set('name', clusterName);
        }
        else if(nodeData.hasOwnProperty('uhostName')){
            var hostId = node.get('id');
           // var hostPath = node.getPath();
            var hostName = nodeData.uhostName;
            var status = node.get('ustatus') || 'good';
            node.set('iconCls', "status-" + status );
            node.set('name', hostName);
            node.set('leaf', true);
            this.hostInfoMap[hostId] = {
                name : hostName,
                // path : hostPath,
                status : status
            }
        }
    },
    startCheckingStatusTask : function(){
        if(this.statusPollingTask){
            this.statusPollingTask.stop();
        }
        this.statusPollingTask = Ext.TaskManager.start({
            run : this.checkStatus,
            scope : this,
            interval : this.pollingInterval
        })
    },
	checkStatus : function(){
        this.ajax({
            url : 'getStatus',
            method :  'GET',
            success : function(resp){
                var respData = RS.common.parsePayload(resp);
                this.updateHostStatus(respData);
            },
			failure : function(resp){
				clientVM.displayFailure(resp);
			}
        })
	},
    updateHostStatus : function(hostStatus){
        for(var i = 0; i < hostStatus.length; i++){
            var host = hostStatus[i];
            var hostId = host.id;
            this.hostInfoMap[hostId].status = host.status;
        }
        var root = this.hostTreeStore.getRootNode();
        root.cascadeBy(function(node){
            // console.log(node.get('name'));
            if(node.isLeaf()){
                var hostStatus =  this.hostInfoMap[node.get('id')].status;
                node.set('iconCls', "status-" + hostStatus );
                // console.log(hostStatus);
            }
        },this)
    }
});