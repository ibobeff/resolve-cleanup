glu.defModel("RS.mcp.tabs.NotDeployedTab", {
	title : '',
	width : 180,
	disabled : false,
	thresholdSelected : false,
	init : function(){
		this.set('title', this.localize('notDeployedTabTitle'));
	},
	updateTabInfo : function(thresholdId){
	},
	deactivateTab : function(){
		this.set("disabled", true);
	},
	reactivateTab : function(){
		this.set("disabled", false);
	}
});