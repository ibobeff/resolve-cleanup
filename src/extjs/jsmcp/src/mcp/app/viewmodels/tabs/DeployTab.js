glu.defModel('RS.mcp.tabs.DeployTab',{
	title : '',
    width : 180,
	disabled : false,
	thresholdSelected : false,
	hostTreeStore : {
		mtype: 'treestore',
        fields: ['name','status', 'level'],
        proxy: {
            type: 'ajax',
            url : 'treelist'
        },
        root: {
        	expanded: false,
            name: 'All'
        },
        autoLoad : false
	},
	init : function(){
		this.set('title', this.localize('deployTabTitle'));
		this.hostTreeStore.on('beforeappend', this.updateTree, this);
	},
	 updateTree : function(tree, node){
        if(node.get('level') == "group"){
            node.set('iconCls', "group-level" );
        }
        else if(node.get('level') == "cluster"){
            node.set('iconCls', "cluster-level" );
            node.set('checked', false);
        }
        else if(node.isLeaf()){
            var hostId = node.get('id');
           // var hostPath = node.getPath();
            var hostName = node.get('name');
            var status = node.get('status') || 'good';
            node.set('iconCls', "no-icon" );
            node.set('checked', false);
        }
    },
	updateTabInfo : function(thresholdId){
	},
	deactivateTab : function(){
		this.set("disabled", true);
	},
	reactivateTab : function(){
		this.set("disabled", false);
	}
});
