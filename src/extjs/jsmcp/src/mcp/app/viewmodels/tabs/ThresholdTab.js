glu.defModel("RS.mcp.tabs.ThresholdTab", {
	tabs : {
		mtype :'activatorlist',
		focusProperty : 'selectedTab',
		autoParent: true
	},
	selectedTab :'',
	init : function(){
		var detailTab = this.model({
			mtype : 'DetailTab',
			rootVM : this.parentVM
		})
		this.tabs.add(detailTab);

		// var deployTab = this.model({
		// 	mtype : 'DeployTab'
		// })
		// this.tabs.add(deployTab);

		// var deployedTab = this.model({
		// 	mtype : 'DeployedTab'
		// })
		// this.tabs.add(deployedTab);

		// var notDeployedTab = this.model({
		// 	mtype : 'NotDeployedTab'
		// })
		// this.tabs.add(notDeployedTab);		

		// var historyTab = this.model({
		// 	mtype : 'HistoryTab'
		// })
		// this.tabs.add(historyTab);		

		Ext.defer(function() {
			this.set('selectedTab', detailTab );
		}, 250, this)
	},
	
	// deactivateTab : function(){
	// 	this.tabs.foreach(function (tab){
	// 		if(tab.viewmodelName != "DetailTab")
	// 			tab.deactivateTab();
	// 	})
	// },

	// reactivateTab : function(){
	// 	this.tabs.foreach(function (tab){
	// 		if(tab.viewmodelName != "DetailTab")
	// 			tab.reactivateTab();
	// 	})
	// }
})