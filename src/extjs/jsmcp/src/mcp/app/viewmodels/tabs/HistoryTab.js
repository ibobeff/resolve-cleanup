glu.defModel('RS.mcp.tabs.HistoryTab',{
	title : '',
	width : 180,
	disabled : false,
	thresholdSelected : false,
	init : function(){
		this.set('title', this.localize('historyTabTitle'));
	},
	updateTabInfo : function(thresholdId){
	},
	deactivateTab : function(){
		this.set("disabled", true);
	},
	reactivateTab : function(){
		this.set("disabled", false);
	}
});
