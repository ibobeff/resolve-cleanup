glu.defModel("RS.mcp.tabs.DeployedTab", {
	width : 180,
	title : '',
	disabled : false,
	thresholdSelected : false,
	init : function(){
		this.set('title', this.localize('deployedTabTitle'));
	},
	updateTabInfo : function(thresholdId){
	},
	deactivateTab : function(){
		this.set("disabled", true);
	},
	reactivateTab : function(){
		this.set("disabled", false);
	}
});