glu.defModel('RS.mcp.tabs.DetailTab',{
	title : '',	
	width : 180,
	addIsDisabled : true,
	wait : false,
	thresholdSelected : false,
	fields : ['id',
	{
		name : 'UVersion',
		mapping : 'uversion'
	},{
		name : 'UActive',
		mapping : 'uactive'
	},{
		name : 'URuleModule', 
		mapping : 'uruleModule'
	},{
		name : 'URuleName', 
		mapping : 'uruleName'
	},{
		name : 'UAlertStatus',
		mapping : 'ualertStatus'
	},{ 
		name : 'UGroup',
		mapping : 'ugroup'
	},{
		name : 'UName',
		mapping : 'uname'
	},{
		name :'USource',
		mapping : 'usource'
	},{
		name : 'UType',
		mapping : 'utype',
		convert : function(value, record){
			return value == "default" ? true : false;
		}
	},{
		name : 'UActive',
		mapping : 'uactive',
		type : 'bool'
	}],
	//Action field
	email : false,
	http : false,
	snmp : false,
	database : false,

	severityStore : RS.mcp.MetaConfigMap.severityStore,
	metricNameStore : RS.mcp.MetaConfigMap.metricNameStore,
	metricGroupStore : RS.mcp.MetaConfigMap.metricGroupStore,
	metricMetaData : RS.mcp.MetaConfigMap.metricMetaData,
	conditionColumns : null,
	conditionStore : {
		mtype : 'store',
		proxy : {
			type : 'memory'
		},
		fields : ['id','operator', 'value']
	},
	conditionValue : '',
	selectedOperator : '=',
	operatorStore : RS.mcp.MetaConfigMap.operatorStore,
	
	versionStore : {
		mtype : 'store',
		proxy : {
			type : 'memory'
		},
		fields : ['display','version']
	},
	versionLimit : 5,
	selectedVersion : null,
	//API
	API : {
		thresholdRuleGetAPI : '/resolve/service/metric/get',
		thresholdRuleGetRevisionAPI : '/resolve/service/metric/getRevision',
		thresholdRuleSaveVersionAPI : '/resolve/service/metric/saveversion',
		thresholdRuleForVersionAPI : '/resolve/service/metric/getbynameversion',
	},	
	suppressVersionChangedEvt : false,
	when_selected_version_changes : {
		on: ['selectedVersionChanged'],
		action : function(){
			if(this.suppressVersionChangedEvt){
				this.set('suppressVersionChangedEvt', false);
				return;
			}
			if(this.selectedVersion != null){
				this.getRuleForVersion(this.selectedVersion);
			}
		}
	},
	when_metric_group_change : {
		on : ['UGroupChanged'],
		action : function(){
			this.metricNameStore.removeAll();
			this.metricNameStore.add(this.metricMetaData[this.UGroup]);
			//Select first name in the group by default
			this.set('UName', this.metricNameStore.getAt(0).data.UName);
		}
	},
	init : function(){
		this.buildConditionGrid();
		this.set('title', this.localize('detailTabTitle'));	
	},
	editIsEnabled$: function(){
		return !this.wait && ( this.selectedVersion == 0);
	},
	saveVersionIsEnabled$ : function(){
		return !this.wait;
	},	
	buildConditionGrid : function(){
		this.set('conditionColumns',[{
			dataIndex : 'operator',
			header : 'Operator',
			flex : 1
		},{
			dataIndex : 'value',
			header : 'Value',
			flex : 1,
			sortable : true
		}]);
	},
	resetData :function(){		
		this.set('selectedVersion', null);
		this.versionStore.removeAll();
		this.conditionStore.removeAll();
	},
	updateTabInfo : function(thresholdId, thresholdModule, thresholdName){
		var me = this;
		me.resetData();
		me.ajax({
			url : this.API['thresholdRuleGetRevisionAPI'],
			method : 'POST',
			params : {
				module : thresholdModule,
				name : thresholdName				
			},
			success : function(resp){
				var respData = RS.common.parsePayload(resp);
				if (respData.success){
					me.populateForm(respData.data);
					var limit = me.versionLimit;
					var currentVersion = respData.total;
					//Add version 0 (which is the revision)
					me.versionStore.add({
						display : "Latest Revision",
						version : me.UVersion
					})
					while(currentVersion > 0 && limit > 0){
						me.versionStore.add({
							display : currentVersion,
							version : currentVersion,
						})
						currentVersion--;
						limit--;
					}

					//Select latest version as default
					me.set('suppressVersionChangedEvt', true);
					me.set('selectedVersion', me.UVersion);
					if(!me.thresholdSelected){
						me.set('thresholdSelected', true);
					}
				}
			},
			failure : function(resp){
				clientVM.displayFailure(resp);
			},
			callback: function() {
				me.set('wait', false);
			}
		})
	},
	getRuleForVersion : function(version){
		var me = this;
		var params = {
			module : this.URuleModule,
			name : this.URuleName,
			version : version
		}
		this.ajax({
			url : this.API['thresholdRuleForVersionAPI'],
			method : 'POST',
			params : params,
			success : function(resp){
				var respData = RS.common.parsePayload(resp);
				if (respData.success){
					me.populateForm(respData.data);
				}
			},
			failure : function(resp){
				clientVM.displayFailure(resp);
			}
		})
	},
	populateForm : function(data){
		this.loadData(data);
		var actions = data.uaction ? data.uaction.split(',') : [];
		this.set('http', Ext.Array.contains(actions, 'http'));	
		this.set('snmp', Ext.Array.contains(actions, 'snmp'));
		this.set('email', Ext.Array.contains(actions, 'email'));
		this.set('database', Ext.Array.contains(actions, 'database'));		
		var operators = data.uconditionOperator.split(',');
		var conditionValues = data.uconditionValue.split(',');
		this.conditionStore.removeAll();
		for(var i = 0; i < operators.length; i++){
			this.conditionStore.add({
				operator : operators[i],
				value : conditionValues[i]
			})
		}
		this.set('isDirty', false);
	},

	edit : function(){
		this.open({
			mtype : 'RS.mcp.ThresholdForm',
			mode : 3,
			thresholdId : this.id
		})
	},
	saveVersion : function(){
		this.message({
			title : this.localize('saveVersionTitle'),
			msg : this.localize('saveVersionMsg'),
			button : Ext.MessageBox.YESNO,
			buttonText : {
				yes : this.localize('confirm'),
				no : this.localize('cancel')
			},
			fn : function(btn){
				if(btn == 'yes'){
					this.doSaveVersion();
				}
			}
		})
	},
	doSaveVersion : function(){
		this.set('wait', true);
		var me = this;
		var payload = this.asObject();

		//Check for default type
		payload.UType = this.UType ? "default" : "";

		//Add actions
		var actions = [];
		if(this.http)
			actions.push('http');
		if(this.snmp)
			actions.push('snmp');
		if(this.email)
			actions.push('email');
		if(this.database)
			actions.push('database');
		payload.UAction = actions.join(",");

		//Add condtions
		var conditionValues = [];
		var conditionOperators = [];
		this.conditionStore.each(function(item){
			conditionOperators.push(item.data.operator);
			conditionValues.push(item.data.value);
		})
		payload.UConditionOperator = conditionOperators.join(',');
		payload.UConditionValue = conditionValues.join(',');

		this.ajax({
			url : this.API['thresholdRuleSaveVersionAPI'],
			jsonData : payload,
			scope: this,
			success : function(r){
				var resp = RS.common.parsePayload(r);
				if(resp.success){
					clientVM.displaySuccess(me.localize('saveSucMsg'));
					var updateThreshold = true;
					me.rootVM.reloadThresholdTable(updateThreshold );
				}
				else{
					clientVM.displayError(me.localize('saveErrMsg') + '[' + resp.message + ']');
				}
			},
			failure : function(r){
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	}
});