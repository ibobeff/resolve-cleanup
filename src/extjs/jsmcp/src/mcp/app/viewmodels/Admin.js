
glu.defModel("RS.mcp.Admin",{
	groupName: '',
	clusterName: '',
	mcp_Pas_sword: '',
	mcpPasswordValid: false,
	mcpPasswordIsValid$: function() {
		this.set('mcpPasswordValid', false);
		if (this.mcp_Pas_sword.length > 0 && this.mcp_Pas_sword != '**********') {
			if (this.mcp_Pas_sword.indexOf('_') == 0) {
				this.set('mcpPasswordValid', true);
				return true //start with _ to force basic passwords
			}
			if (this.mcp_Pas_sword.length < 8) return this.localize('passwordMinLengthInvalid');
			if (!this.mcp_Pas_sword.match(/[0-9]+/g)) return this.localize('passwordNumberRequiredInvalid');
			if (!this.mcp_Pas_sword.match(/[A-Z]+/g)) return this.localize('passwordCapitalLetterRequiredInvalid');
			if (!this.mcp_Pas_sword.match(/[a-z]+/g)) return this.localize('passwordLowercaseLetterRequiredInvalid');

			this.set('mcpPasswordValid', true);
			return true;
		}
		return true;
	},
	mcpPasswordConfirm: '',
	mcpPasswordConfirmValid: false,
	mcpPasswordConfirmIsValid$: function() {
		this.set('mcpPasswordConfirmValid', false);
		if (this.mcp_Pas_sword && this.mcp_Pas_sword != '**********') {
			if (this.mcp_Pas_sword == this.mcpPasswordConfirm) {
				this.set('mcpPasswordConfirmValid', true);
				return true;
			}
			return this.localize('passwordConfirmMismatchInvalid');
		}
		return true;
	},
	wait : false,
	clusterStore: null,
	clusterList: null,
	newClusterList: null,
	clusterCount: 0,
	init: function() {
		this.newClusterList = [];
		this.set('clusterCount', 0);
		var config = {
			mtype: 'store',
			fields: ['name', 'value'],
			data: this.createNameValueItems(this.clusterList)
		};
		this.set('clusterStore', Ext.create('Ext.data.Store', config));

		if (this.clusterList.length) {
			this.set('clusterName', this.clusterList[0]);
	
			for (var i = 0; i < this.clusterList.length; i++) {
				this.getMCPLoginInfo(this.groupName, this.clusterList[i], function(clusterName, data){
					if (!data) {
						this.newClusterList.push(clusterName);
					}
					this.set('clusterCount', ++this.clusterCount)
				}.bind(this), 
				function(clusterName) {
					this.newClusterList.push(clusterName);
					this.set('clusterCount', ++this.clusterCount);
				}.bind(this))
			}
		}
	},
	createNameValueItems: function () {
		var values, items = [];
	
		if (arguments.length === 1) {
			values = arguments[0];
		} else {
			values = Array.apply(null, arguments);
		}
	
		for (var i = 0; i < values.length; i++) {
			items.push({ 
				name: values[i],
				value: values[i] 
			});
		}
		return items;
	},
	getMCPLoginInfo : function(groupName, clusterName, onSuccess, onFailure){
		this.set('wait', true);
		var params = {
			group : groupName,
			cluster : clusterName
		}
		this.ajax({
			url : '/resolve/service/mcplogin/findbyname',
			method : 'POST',
			params : params,
			scope: this,
			success : function(resp){
				var respData = RS.common.parsePayload(resp);
				if (respData.success){
					if (Ext.isFunction(onSuccess)) {
						onSuccess(clusterName, respData.data);
					}
				} else {
					if (Ext.isFunction(onFailure)) {
						onFailure(clusterName, resp);
					}
				}
			},
			failure : function(resp){
				clientVM.displayFailure(resp);
				if (Ext.isFunction(onFailure)) {
					onFailure(clusterName, resp);
				}
			},
			callback: function() {
				 this.set('wait', false);
			}
		})
	},
	saveMCPLoginInfo : function(onSuccess, onFailure, onComplete){
		this.set('wait', true);
		this.ajax({
			url : '/resolve/service/mcplogin/save',
			method : 'POST',
			jsonData: {
				UGroupName: this.groupName,
				UClusterName: this.clusterName,
				UPWDValue: this.mcp_Pas_sword
			},
			scope: this,
			success : function(resp){
				var respData = RS.common.parsePayload(resp);
				if (respData.success){
					clientVM.displaySuccess(this.localize('SaveSuccess'));
					if (Ext.isFunction(onSuccess)) {
						onSuccess(resp);
					}
				} else {
					clientVM.displayError(this.localize('SaveErr') + '[' + respData.message + ']')
					if (Ext.isFunction(onFailure)) {
						onFailure(resp);
					}
				}
			},
			failure : function(resp){
				clientVM.displayFailure(resp);
				if (Ext.isFunction(onFailure)) {
					onFailure(resp);
				}
			},
			callback: function() {
				this.set('wait', false);
				if (Ext.isFunction(onComplete)) {
					onComplete();
				}
			}
		})
	},
	isNewCluster$: function() {
		if (this.clusterCount && this.newClusterList && this.newClusterList.indexOf(this.clusterName) != -1) {
			this.set('titleTxt', this.localize('createMCPAdminPassword'));
			this.set('submitBtnTxt', this.localize('create'));
			return true;
		} else {
			this.set('titleTxt', this.localize('modify_MCP_Ad_min_Pas_sword'));
			this.set('submitBtnTxt', this.localize('update'));
			return false;
		}
	},
	titleTxt: '',
	title$: function() {
		return this.titleTxt;
	},
	submitBtnTxt: '',
	submitBtn$: function() {
		return this.submitBtnTxt;
	},
	submitIsEnabled$: function() {
		return !this.wait && this.mcpPasswordValid && this.mcpPasswordConfirmValid;
	},
	submit: function() {
		this.saveMCPLoginInfo(function() {
			// only close dialog if successfully saved password
			this.doClose();
		}.bind(this));
	},
	cancel : function(){
		this.doClose();
	}
})
