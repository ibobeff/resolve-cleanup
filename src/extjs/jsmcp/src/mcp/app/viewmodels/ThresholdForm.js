glu.defModel("RS.mcp.ThresholdForm",{
	wait : false,
	title : '',	
	addIsDisabled : true,
	selectedConditionIndex : null,
	conditionIsEmpty : false,
	submitBtnTxt : "",
	//Action field
	email : false,
	http : false,
	snmp : false,
	database : false,
	fields : ['id',
	{
		name : 'UVersion',
		mapping : 'uversion'
	},{
		name : 'UActive',
		mapping : 'uactive'
	},{
		name : 'URuleModule', 
		mapping : 'uruleModule'
	},{
		name : 'URuleName', 
		mapping : 'uruleName'
	},{
		name : 'UAlertStatus',
		mapping : 'ualertStatus'
	},{ 
		name : 'UGroup',
		mapping : 'ugroup'
	},{
		name : 'UName',
		mapping : 'uname'
	},{
		name :'USource',
		mapping : 'usource'
	},{
		name : 'UType',
		mapping : 'utype',
		convert : function(value, record){
			return value == "default" ? true : false;
		}
	},{
		name : 'UActive',
		mapping : 'uactive',
		type : 'bool'
	}],
	severityStore : RS.mcp.MetaConfigMap.severityStore,
	metricNameStore : RS.mcp.MetaConfigMap.metricNameStore,
	metricGroupStore : RS.mcp.MetaConfigMap.metricGroupStore,
	metricMetaData : RS.mcp.MetaConfigMap.metricMetaData,	
	conditionStore : {
		mtype : 'store',
		proxy : {
			type : 'memory'
		},
		fields : ['id','operator', 'value']
	},
	selectedOperator : '=',
	conditionValue : '',
	operatorStore : RS.mcp.MetaConfigMap.operatorStore,
	defaultData : {
		uruleModule : '',
		uruleName : '',
		utype : "default",
		uactive : true,
		ualertStatus : 'warning',
		ugroup : 'jvm',
		uname : 'thread_count',
		usource : '',
		uversion : 0
	},
	modeMap : {
		'CREATE' : 1,
		'COPY' : 2,
		'EDIT' : 3
	},
	mode : 1,
	//Reactors
	when_metric_group_change : {
		on : ['UGroupChanged'],
		action : function(){
			this.metricNameStore.removeAll();
			this.metricNameStore.add(this.metricMetaData[this.UGroup]);
			//Select first name in the group by default
			this.set('UName', this.metricNameStore.getAt(0).data.UName );
		}
	},
	//API
	API : {
		thresholdRuleGetAPI : '/resolve/service/metric/get',
		thresholdRuleSaveAPI : '/resolve/service/metric/save',
		thresholdRuleCopyAPI : '/resolve/service/metric/copy',
		thresholdRuleSaveVersionAPI : '/resolve/service/metric/saveversion'
	},	
	init : function(){
		this.set('submitBtnTxt', this.localize('create'));
		if(this.mode == this.modeMap['COPY']){
			this.getThresholdRule(this.thresholdId);
			this.set('title', this.localize('copyThresholdTitle'));
		}
		else if(this.mode == this.modeMap['EDIT']){
			this.getThresholdRule(this.thresholdId);
			this.set('title', this.localize('editThresholdTitle'));
			this.set('submitBtnTxt', this.localize('save'));
		}
		else{
			this.set('title', this.localize('createThresholdTitle'));
			this.loadData(this.defaultData);
			this.set('http', true);
			this.set('conditionIsEmpty', true);
			this.set('isPristine', true);
		}
	},
	//Validation
	URuleModuleIsValid$ : function(){
		return (this.URuleModule && this.validateInput(this.URuleModule)) ? true : this.localize('invalidModule');
	},

	URuleNameIsValid$ : function(){
		return (this.URuleName && this.validateInput(this.URuleName)) ? true : this.localize('invalidName');
	},	
	conditionIsValid$ : function(){
		return !this.conditionIsEmpty;
	},
	validateInput: function(input) {
		return (/^[a-zA-Z0-9_\-]+$/.test(input) || /^[a-zA-Z0-9_\-][a-zA-Z0-9_\-\s]*[a-zA-Z0-9_\-]$/.test(input)) && !/.*\s{2,}.*/.test(input) && /^[a-zA-Z0-9_\-]{1,40}$/.test(input.replace(/\s/g, '')) ;
	},
	actionGroup : null,
	actionGrouptIsValid$ : function(){
		return (this.http || this.snmp || this.email || this.database) ? true : ""
	},
	editMode$ : function(){
		return this.mode == this.modeMap['EDIT'];
	},
	getThresholdRule : function(thresholdId){
		this.set('wait', true);
		var me = this;
		this.ajax({
			url : this.API['thresholdRuleGetAPI'],
			method : 'POST',
			params : { 
				id : thresholdId 
			},
			scope: this,
			success : function(resp){
				var respData = RS.common.parsePayload(resp);
				if (respData.success){
					this.populateForm(respData.data);
				}
			},
			failure : function(resp){
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},
	populateForm : function(data){	
		this.loadData(data);
		var actions = data.uaction ? data.uaction.split(',') : [];
		for(var i = 0; i < actions.length; i++){
			this.set(actions[i], true);
		}
		var operators = data.uconditionOperator.split(',');
		var conditionValues = data.uconditionValue.split(',');
		//this.conditionStore.removeAll();
		for(var i = 0; i < operators.length; i++){
			this.conditionStore.add({
				operator : operators[i],
				value : conditionValues[i]
			})
		}
		
		this.set('isDirty', false);
		this.set('isPristine', false);
		if(this.mode == this.modeMap['COPY']){
			this.set("URuleName", this.URuleName + " - copy");
		}
	},
	addCondition : function(){
		this.conditionStore.add({operator : this.selectedOperator, value : this.conditionValue});
		this.clearConditionInput();
		this.set('conditionIsEmpty', false );
		this.set('conditionErrorIsHidden', true);		
	},
	clearConditionInput : function (){
		this.set('conditionValue', null);
		this.set('addIsDisabled', true);
	},	
	deleteCondition : function(record){
		this.conditionStore.remove(record);
		this.set('selectedConditionIndex', null);
		if(this.conditionStore.data.length == 0){
			this.set('conditionIsEmpty', true );
			this.set('conditionErrorIsHidden', false);
		}
	},
	deleteIsDisabled$ : function(){
		if(this.selectedConditionIndex != null)
			return false;
		else
			return true;
	},
	conditionSelect : function(record, item, index){
		this.set('selectedConditionIndex', index);
	},

	submitForm : function(btn,saveNewVersion){
		if(!this.isValid){
			this.set('isPristine', false);
			this.set('conditionErrorIsHidden', this.conditionIsValid ? true : false);
			this.enableAllButton(false);
			return;
		}
		var me = this;
		var payload = this.asObject();
		var api = null;
		var msg = null;
		if(me.mode == me.modeMap['CREATE']){
			api = me.API['thresholdRuleSaveAPI'];
			sucMsg = me.localize('createSucMsg');
			errorMsg = me.localize('createErrMsg');
		}
		else if (me.mode == me.modeMap['COPY']){
			api = me.API['thresholdRuleCopyAPI'];
			sucMsg = me.localize('copySucMsg');
			errorMsg = me.localize('copyErrMsg');		
		}
		else {
			api = me.API['thresholdRuleSaveAPI'];
			sucMsg = me.localize('saveSucMsg');
			errorMsg = me.localize('saveErrMsg');
		}
		//Check for default type
		payload.UType = this.UType ? "default" : "";

		//Add actions
		var actions = [];
		if(this.http)
			actions.push('http');
		if(this.snmp)
			actions.push('snmp');
		if(this.email)
			actions.push('email');
		if(this.database)
			actions.push('database');
		payload.UAction = actions.join(",");

		//Add condtions
		var conditionValues = [];
		var conditionOperators = [];
		me.conditionStore.each(function(item){
			conditionOperators.push(item.data.operator);
			conditionValues.push(item.data.value);
		})
		payload.UConditionOperator = conditionOperators.join(',');
		payload.UConditionValue = conditionValues.join(',');
		me.ajax({
			url : api,
			jsonData : payload,
			success : function(r){
				var resp = RS.common.parsePayload(r);
				me.rootVM.set('wait', false);
				if(saveNewVersion){
					me.saveVersion(resp.data);
				}
				else {
					//Reload threshold detail if this is edit mode.
					var updateThreshold = false;
					if(me.mode == me.modeMap['EDIT']){
						updateThreshold = true;
					}
					me.rootVM.reloadThresholdTable(updateThreshold);
					
					if(resp.success)
						clientVM.displaySuccess(sucMsg);
					else
						clientVM.displayError(errorMsg + '[' + resp.message + ']');
					this.doClose();
				}
			
			}
		})		
	},
	createWithVersion : function(){
		if(!this.isValid){
			this.set('isPristine', false);
			this.set('conditionErrorIsHidden', this.conditionIsValid ? true : false);
			this.enableAllButton(false);
			return;
		}
		var saveNewVersion = true;
		this.submitForm(null,saveNewVersion);
	},		
	saveVersion : function(thresholdRecord){		
		//Extra step cuz backend refuse to use the same fields for response and request parameters !!!
		var payload = {
			UAction: thresholdRecord['uaction'],
			UActive: thresholdRecord['uactive'],
			UAlertStatus: thresholdRecord['ualertStatus'],
			UConditionOperator: thresholdRecord['uconditionOperator'],
			UConditionValue: thresholdRecord['uconditionValue'],
			UGroup: thresholdRecord['ugroup'],
			UName: thresholdRecord['uname'],
			URuleModule: thresholdRecord['uruleModule'],
			URuleName: thresholdRecord['uruleName'],
			USource: thresholdRecord['usource'],
			UType: thresholdRecord['utype'],
			UVersion: 0,
			id: thresholdRecord['id'],
		}
		var me = this;		
		this.ajax({
			url : this.API['thresholdRuleSaveVersionAPI'],
			jsonData : payload,
			success : function(r){			
				var resp = RS.common.parsePayload(r);
				if(resp.success){
					clientVM.displaySuccess(me.localize('saveSucMsg'));			
					me.rootVM.reloadThresholdTable();
				}
				else{
					clientVM.displayError(me.localize('saveErrMsg') + '[' + resp.message + ']');
				}
				this.doClose();
			},
			failure : function(r){
				clientVM.displayFailure(r);
			}
		})
	},
	createWithVersionIsEnabled : true,
	submitFormIsEnabled : true,
	on_form_is_valid : {
		on : ['isValidChanged'],
		action : function(){
			if(!this.isPristine){
				this.enableAllButton(this.isValid);
				this.set('conditionErrorIsHidden', this.conditionIsValid ? true : false);
			}
		}
	},
	conditionErrorIsHidden : true,
	enableAllButton : function(flag){
		this.set('createWithVersionIsEnabled',flag);
		this.set('submitFormIsEnabled',flag);		
	},
	createWithVersionIsVisible$ : function(){
		return this.mode != this.modeMap['EDIT'];
	},
	infoIsVisible$ : function(){
		return this.mode != this.modeMap['EDIT'];
	},
	cancel : function(){
		this.doClose();
	}
})