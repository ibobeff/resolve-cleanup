glu.defView("RS.mcp.ThresholdForm",{	
	title : '@{title}',
	cls : 'hideReadOnlyBorder rs-modal-popup',
	layout : {
		align : 'stretch',
		type : 'vbox'
	},
	width : 600,
	padding : 15,
	defaults : {
		margin : '4 0',
		labelWidth : 150		
	},
	items : [
	{
		xtype :'textfield',
		name : 'URuleModule',
		fieldLabel : 'Module',
		emptyText : '~~moduleEmptyText~~',
		readOnly : '@{editMode}'
	},{
		xtype : 'textfield',
		fieldLabel : 'Name',
		name : 'URuleName',
		emptyText : '~~nameEmptyText~~',
		readOnly : '@{editMode}'
	},{
		xtype : 'checkboxgroup',
		layout : 'hbox',
		defaults : {
			margin : '0 10 0 0'
		},
		items : [
		{
			xtype : 'checkboxfield',
			boxLabel : 'Active',
			checked : '@{UActive}',
		},{
			xtype : 'checkboxfield',
			boxLabel : 'Auto Deploy',
			checked : '@{UType}',
			hidden : true
		}]			
	},{
		xtype : 'combobox',
		value : '@{UAlertStatus}',
		fieldLabel : 'Severity',
		valueField : 'UAlertStatus',
		displayField : 'display',
		store : '@{severityStore}',
		queryMode : 'local',
		editable : false
	},{
		xtype :'checkboxgroup',
		fieldLabel : 'Actions',
		msgTarget: 'side',
		allowBlank : false,
		name: 'actionGroup',
		layout : 'hbox',
		defaults : {
			margin : '0 10 0 0'
		},
		items : [
		{
			xtype : 'checkboxfield',
			boxLabel : 'HTTP',
			checked : '@{http}'
		},{
			xtype : 'checkboxfield',
			boxLabel : 'SNMP',
			checked : '@{snmp}'
		},{
			xtype : 'checkboxfield',
			boxLabel : 'Email',
			checked : '@{email}'
		},{
			xtype : 'checkboxfield',
			boxLabel : 'Write to DB',
			checked : '@{database}'
		}]
	},{
		xtype : 'combobox',
		value : '@{UGroup}',
		valueField : 'UGroup',
		fieldLabel : 'Metric Group',
		displayField : 'display',
		store : '@{metricGroupStore}',
		queryMode : 'local',
		editable : false,
		readOnly : '@{editMode}'
	},{
		xtype : 'combobox',
		value : '@{UName}',
		valueField : 'UName',
		fieldLabel : 'Metric Name',
		displayField : 'display',
		store : '@{metricNameStore}',
		queryMode : 'local',
		editable : false,
		readOnly : '@{editMode}'
	},{
		xtype : 'textfield',
		fieldLabel : 'Source (optional)',
		name : 'USource',
		emptyText : 'Source'
	},{
		xtype : 'fieldcontainer',
		layout :'hbox',
		fieldLabel : 'Conditions',
		items : [
		{
			xtype : 'combobox',
			displayField : 'operator',
			store : '@{operatorStore}',
			queryMode : 'local',
			margin : '0 5 0 0',
			flex : 2,
			editable : false,
			value : '@{selectedOperator}'
		},{
			xtype :'textfield',
			emptyText : 'value',
			margin : '0 5 0 0',
			flex : 3,
			value : '@{conditionValue}',
			validator : function(){
				var selectedOperator = this._vm.selectedOperator;
				if(this.value == ''){
					this._vm.set('addIsDisabled', true);
					return 'Value cannot be empty.'
				}
				else if(selectedOperator == 'contains' || (!isNaN(parseFloat(this.value)) && isFinite(this.value))){
					this._vm.set('addIsDisabled', false);
					return true;
				}
				else{
					this._vm.set('addIsDisabled', true);
					return 'Value most be numeric type.'
				}
			}
		},{
			xtype : 'button',
			cls : 'rs-small-btn rs-btn-dark',
			text : 'Add',
			margin : '0 5 0 0',
			disabled : '@{addIsDisabled}',			
			handler : '@{addCondition}'
		},{
			xtype : 'button',
			cls : 'rs-small-btn rs-btn-light',
			text : 'Clear',		
			handler : '@{clearConditionInput}'
		}]
	},{
		xtype : 'grid',
		cls : 'rs-grid-dark',
		store : '@{conditionStore}',
		columns : [
		{
			dataIndex : 'operator',
			header : 'Operator',
			flex : 1
		},{
			dataIndex : 'value',
			header : 'Value',
			flex : 1,
			sorttable : true
		},{
			xtype: 'actioncolumn',
			hideable: false,
			sortable: false,
			align: 'center',		
			width: 45,		
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record) {
					view.ownerCt._vm.deleteCondition(record);				
				}		
			}]
		}],
		hidden : '@{conditionIsEmpty}',
		maxHeight : 250,		
		listeners : {
			select : '@{conditionSelect}',
			deleteAction : '@{deleteCondition}'
		}
	},{
		html : "~~infoText~~",
		hidden : '@{!infoIsVisible}'
	},{
		html : "~~emptyConditionText~~",
		hidden : '@{conditionErrorIsHidden}'
	}],
	buttons : [
	{
		cls : 'rs-med-btn rs-btn-dark',
		name: 'createWithVersion'
	},{
        text: '@{submitBtnTxt}',
        cls : 'rs-med-btn rs-btn-dark',       
        name : 'submitForm',	           
    },{      
        cls : 'rs-med-btn rs-btn-light',
        name : 'cancel'	       
    }],	
  	modal: true
})