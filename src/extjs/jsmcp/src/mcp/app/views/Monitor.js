Ext.define('Ext.tree.plugin.ResolveTreeViewDropDrag', {
    extend: 'Ext.AbstractPlugin',
    alias: 'plugin.resolvetreeviewdragdrop',

    uses: [
        'Ext.tree.ViewDragZone',
        'Ext.tree.ViewDropZone'
    ],
    b4StartDrag: function(x, y) {
        // show the drag frame
        if(this.tooltip)
        	this.showFrame(x, y);
    },
    tooltip : true,
    indicatorCls: 'x-tree-ddindicator',
    dragText : '{0} selected node{1}',
    allowParentInserts: false,
    allowContainerDrops: false,
    appendOnly: false,
    ddGroup : "TreeDD",
    containerScroll: false,
    expandDelay : 5000,
    enableDrop: true,
    enableDrag: true,
    nodeHighlightColor: 'c3daf9',
    nodeHighlightOnDrop: Ext.enableFx,
    nodeHighlightOnRepair: Ext.enableFx,
    displayField: 'text',
    init : function(view) {
        view.on('render', this.onViewRender, this, {single: true});
    },

    destroy: function() {
        Ext.destroy(this.dragZone, this.dropZone);
    },
	onViewRender : function(view) {
        var me = this,
            scrollEl;

        if (me.enableDrag) {
            if (me.containerScroll) {
                scrollEl = view.getEl();
            }
            me.dragZone = new Ext.tree.ViewDragZone({
                view: view,
                ddGroup: me.dragGroup || me.ddGroup,
                dragText: me.dragText,
                displayField: me.displayField,
                repairHighlightColor: me.nodeHighlightColor,
                repairHighlight: me.nodeHighlightOnRepair,
                scrollEl: scrollEl,
                animRepair : true,
                b4StartDrag : me. b4StartDrag
            });
        }

        if (me.enableDrop) {
            me.dropZone = new Ext.tree.ViewDropZone({
                view: view,
                ddGroup: me.dropGroup || me.ddGroup,
                allowContainerDrops: me.allowContainerDrops,
                appendOnly: me.appendOnly,
                allowParentInserts: me.allowParentInserts,
                expandDelay: me.expandDelay,
                dropHighlightColor: me.nodeHighlightColor,
                dropHighlight: me.nodeHighlightOnDrop,
                sortOnDrop: me.sortOnDrop,
                containerScroll: me.containerScroll,
                indicatorCls :  me.indicatorCls
            });
        }
    }
}, function(){
    var proto = this.prototype;
    proto.nodeHighlightOnDrop = proto.nodeHighlightOnRepair = Ext.enableFx;
});



glu.defView('RS.mcp.Monitor', {
	title : '~~monitorTitle~~',
	layout : 'border',
	items : [
	{
		xtype: 'treepanel',
		region : 'west',
		split : true,
		width : 400,
		name : 'hostTreeStore',
		id : 'NAVIGATIONTREE',
		useArrows : true,
		rootVisible : false,
		// dockedItems : [{
		// 	xtype : 'toolbar',
		// 	dock : 'top',
		// 	items : ['view']
		// }],
		viewConfig: {
		    plugins:{	
		    	ptype: 'resolvetreeviewdragdrop',
		    	dragText : 'Insert here.',
		    	indicatorCls: 'dd-custom-indicator',
		    	tooltip : false
		    },
		    listeners : {
	    		beforedrop : function(item, data, node, dropPosition, dropHandlers, eOpts){
	    			//Allow drop and drag on same level only
	    			var prevParent = data.records[0].parentNode.get('name');
	    			var nextParent = node.parentNode.get('name');
	    			if(prevParent !== nextParent || dropPosition == "append")
	    				return false;
	    		}
	    	}
		},
		columns: {
			xtype : 'treecolumn',
			dataIndex : 'name',
			text : 'Instances',
			flex : 1
		}
	},{
		xtype : 'panel',
		region : 'center',
		title : 'Testing'
	}]
})