
glu.defView("RS.mcp.Admin",{
	xtype : 'panel',
	title : '@{title}',
	layout : {
		align : 'stretch',
		type : 'vbox'
	},
	width : 550,
	padding : 20,
	defaults : {
		margin : '0 0 10 0',
		labelWidth : 150,
		msgTarget: 'side',
		labelStyle : "font-weight : 400;"
	},
	items: [
	/* TBD
	{
		xtype :'textfield',
		name : 'groupName',
	},
	*/
	{
		xtype: 'combobox',
		name: 'clusterName',
		editable: false,
		displayField: 'name',
		valueField: 'name',
		queryMode: 'local',
		store: '@{clusterStore}',
		forceSelection: true
	}, {
		id: 'mcp_Pas_sword',
		xtype : 'textfield',
		name : 'mcp_Pas_sword',
		inputType: 'password',
		selectOnFocus: true,
		allowBlank: false
	},{
		xtype : 'textfield',
		name : 'mcpPasswordConfirm',
		inputType: 'password',
		selectOnFocus: true,
		allowBlank: false
	}],
	dockedItems : [{
		xtype : 'toolbar',
		dock : 'bottom',
		ui: 'footer',
		margin : '10 0 0 0',
		items: [{
			name: 'submit',
			text: '@{submitBtn}',
	        width : 80,
	    },{
	        name: 'cancel',
	        width : 80
	    }]
	}],
	buttonAlign: 'left',
  	modal: true
})
