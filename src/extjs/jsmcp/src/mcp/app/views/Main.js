glu.defView("RS.mcp.Main", {
	bodyPadding : 15,
	autoScroll : true,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	items : [
	{
		xtype : 'grid',
		displayName : '@{displayName}',
		cls :'rs-grid-dark',
		overflowY : 'auto',
		flex : 2,		
		plugins : [
		{
			ptype : 'searchfilter',
			allowPersistFilter : false,
			hideMenu : true
		},{
			ptype: 'pager'
		},{
			ptype: 'columnautowidth'
		}],
		dockedItems : [
		{
			xtype : 'toolbar',		
			dock : 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
		 	items : ['createThreshold','copyThreshold','deleteThreshold','mcpadmin']
		}],
		store : '@{thresholdStore}',
		columns : '@{thresholdColumns}',
		listeners : {			
			select : '@{selectThreshold}',
			render : function(){
				var grid = this;
				this._vm.on('reSelectRecord', function(){
					//Manually reselect to hightlight the row.
					var selectedRecord = this.selectedRecord;
					var selectionModel = grid.getSelectionModel();
					if(selectionModel.isSelected(selectedRecord)){
						//reselect to update the detail tab.
						this.selectThreshold(selectedRecord);
					}
					else
						selectionModel.select(selectedRecord);
				})
			}
		}
	},{
		xtype : '@{thresholdTabs}',
		style : 'border-top :1px solid #cccccc;',
		margin : '10 0 0 0',
		padding : '10 0 0 0',
		flex : 3
	}]
})