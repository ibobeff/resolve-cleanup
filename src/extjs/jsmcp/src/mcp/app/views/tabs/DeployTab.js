glu.defView('RS.mcp.tabs.DeployTab',{
	xtype : 'panel',
	title : '@{title}',
	disabled : '@{disabled}',
	autoScroll : true,
	layout : 'vbox',
	id : 'NAVIGATIONTREE',
	items : [
	{
		xtype : 'treepanel',
		title : 'Cluster',
		width : 400,
		useArrows : true,
		rootVisible : false,
		store : '@{hostTreeStore}',
		columns: {
			xtype : 'treecolumn',
			dataIndex : 'name',
			text : 'Instances',
			flex : 1
		}
	}]
});