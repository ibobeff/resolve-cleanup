glu.defView("RS.mcp.tabs.ThresholdTab",{
	xtype: 'tabpanel',
	cls : 'rs-tabpanel-dark',
	items : '@{tabs}',
	activeTab : '@{selectedTab}',
	id : "thresholdTab"
})