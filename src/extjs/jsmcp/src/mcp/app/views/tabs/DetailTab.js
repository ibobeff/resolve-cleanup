glu.defView('RS.mcp.tabs.DetailTab',{
	title : '@{title}',
	autoScroll : true,
	padding : '10 0 0 0',
	items : [
	{
		html : '<h2 class="thresholdText" >Please select a threshold rule above.<h2>',
		hidden : '@{thresholdSelected}',
		layout : 'fit'
	},{
		xtype : 'panel',
		width : 600,
		flex : 1,
		hidden : '@{!thresholdSelected}',
		dockedItems : [
		{
			xtype : 'toolbar',
			dock : 'top',
			cls :' rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				minWidth : 60,
			},
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
		 	items : ['edit','saveVersion' ]		 	
		}],
		layout : {
			type : 'vbox',
			align : 'stretch'
		},
		defaults :{		
			labelWidth : 150,
			margin : '4 0',
			msgTarget: 'side',
			readOnly : true
		},
		defaultType: 'displayfield',		
		items : [
		{
			name : 'URuleModule',
			fieldLabel : 'Module'
		},{
			fieldLabel : 'Name',
			name : 'URuleName'
		},{
			xtype : 'combobox',
			fieldLabel : 'Version',
			store : '@{versionStore}',
			value : '@{selectedVersion}',
			queryMode : 'local',
			editable : false,
			readOnly : false,
			displayField : 'display',
			valueField : 'version'
		},{
			xtype : 'checkboxgroup',
			layout :{
				type : "hbox"
			},
			defaults : {
				readOnly : true
			},
			items : [
				{
					xtype : 'checkboxfield',
					boxLabel : 'Active',
					checked : '@{UActive}',
					margin : '0 20 0 0',
				},
				{
					xtype : 'checkboxfield',
					boxLabel : 'Auto Deploy',
					checked : '@{UType}',
					hidden : true
				}
			]
		},{
			xtype : 'combobox',
			value : '@{UAlertStatus}',
			fieldLabel : 'Severity',
			valueField : 'UAlertStatus',
			displayField : 'display',
			store : '@{severityStore}',
			queryMode : 'local',
			editable : false
		},{
			xtype :'checkboxgroup',
			fieldLabel : '~~notification~~',
			msgTarget: 'side',
			allowBlank : false,
			layout : 'hbox',
			defaults : {
				readOnly : true,
				margin : '0 15 0 5'
			},
			items : [
			{
				xtype : 'checkboxfield',
				boxLabel : 'HTTP',
				checked : '@{http}'
			},{
				xtype : 'checkboxfield',
				boxLabel : 'SNMP',
				checked : '@{snmp}'
			},{
				xtype : 'checkboxfield',
				boxLabel : 'Email',
				checked : '@{email}'
			},{
				xtype : 'checkboxfield',
				boxLabel : 'Write to DB',
				checked : '@{database}'
			}]
		},{
			xtype : 'combobox',
			value : '@{UGroup}',
			valueField : 'UGroup',
			fieldLabel : 'Metric Group',
			displayField : 'display',
			store : '@{metricGroupStore}',
			queryMode : 'local',
			readOnly : true
		},{
			xtype : 'combobox',
			value : '@{UName}',
			valueField : 'UName',
			fieldLabel : 'Metric Name',
			displayField : 'display',
			store : '@{metricNameStore}',
			queryMode : 'local',
			readOnly : true
		},{		
			fieldLabel : 'Source (optional)',
			name : 'USource'
		},{		
			fieldLabel : 'Condition',
		},{
			xtype : 'grid',
			cls : 'rs-grid-dark',
			store : '@{conditionStore}',
			allowDeselect : true,			
			columns : '@{conditionColumns}'
		}]	
	}],
	listeners : {
		render : function(){
			var me = this;
			me._vm.on('waitChanged', function(){
				me.setLoading(this.get('wait'), true);
			})
		}
	}
});