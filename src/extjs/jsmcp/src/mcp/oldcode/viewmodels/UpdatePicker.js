glu.defModel('RS.mcp.UpdatePicker', {

	mock: false,

	pickerTitle$: function() {
		return this.localize('displayNameSelectUpdate')
	},

	updates: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['name'],
		fields: ['id', 'name', 'version', 'date'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/mcp/update/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	updatesSelections: [],
	columns: [],

	init: function() {
		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'name',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~version~~',
			dataIndex: 'version',
			filterable: true,
			sortable: true,
			flex: 1,
		}, {
			header: '~~buildDate~~',
			dataIndex: 'date',
			filterable: true,
			sortable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))
		this.updates.load()
	},

	cancel: function() {
		this.doClose()
	},

	deploy: function() {
	   this.message({
			title: this.localize('deployTitle'),
			msg: this.localize('deployMessage'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deploy'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeployUpdate
		}); 
	},
	reallyDeployUpdate: function() {
		this.callback.apply(this.parentVM, [this.updatesSelections])
		this.doClose()
	},
	deployIsEnabled$: function() {
		return this.updatesSelections.length == 1
	},
});
