glu.defModel('RS.mcp.Updates', {

	mock: false,
	stateId: 'updatesList',

	updatesStore:{
		mtype:'store',
		fields:['id', 'name', 'type', 'version', 'date', 'size', 'downloadUrl'].concat(RS.common.grid.getSysFields()),
		proxy:{
			type:'ajax',
			url:'/resolve/service/mcp/update/list',
			reader:{
				type:'json',
				root:'records'
			}
		}
	},

	//in this case, it's being intialized in init() function because we have a locale renderer.
	columns:[],

	//attribute that keeps track your selections. It will store items you selected
	//need to follow the naming convention: [name of the grid + 'Selections']
	updatesSelections:[],
	allSelected: false,

	activate: function() {
		window.document.title = this.localize('windowTitleUpdates');
	},	

	init: function() {
		if (this.mock) this.backend = RS.mcp.createMockBackend(true);
		var me = this;
		this.updatesStore.load();
		this.set('columns', [
     	    {
    	  		header : '~~name~~',
    	  		dataIndex : 'name',
    	  		filterable : true,
				renderer: RS.common.grid.downloadLinkRenderer('downloadUrl'),
    	  		flex : 1
      		},
      		{
          		header : '~~type~~',
          		dataIndex : 'type',
          		filterable : true,
          		flex : 1
          	},
          	{
          		header : '~~version~~',
          		dataIndex : 'version',
          		filterable : true,
          		flex : 1
          	},
          	{
          		header : '~~buildDate~~',
          		dataIndex : 'date',
          		filterable : true,
          		flex : 1
          	},
          	{
          		header : '~~size~~',
          		dataIndex : 'size',
          		filterable : true,
          		renderer: function(value) {
					return Ext.util.Format.fileSize(value)
				},
          		flex : 1
          	}]
		);
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll);
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false);
		console.log(this.updatesSelections);
	},
	deleteAction: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.updatesSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.updatesSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		});
	},
	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			var ids = [];
			Ext.each(this.updatesSelections, function(selection) {
				ids.push(selection.get('id'));
			});
			this.ajax({
				url: '/resolve/service/mcp/file/delete',
				params: {
					ids: ids.join(',')
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success)
						this.updatesStore.load();
					else
						clientVM.displayError(response.message);
				}
			});
		}
	},	
	addAction: function() {
		this.open({
			mtype: 'RS.mcp.UploadUpdate'
		});
	},
	addActionIsEnabled$: function() {
		return !this.wait;
	},
	loadRecords: function() {
		this.updatesStore.load();
	}
});