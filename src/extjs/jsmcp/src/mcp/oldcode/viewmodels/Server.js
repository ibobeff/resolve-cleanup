glu.defModel('RS.mcp.Server', {

	mock: false,
	stateId: 'serverList',

	serverStore:{
		mtype:'store',
		fields:['id', 'name', 'serverIp', 'status', 'version', 'build', 'worksheet', 'worksheetId' ].concat(RS.common.grid.getSysFields()),
		proxy:{
			type:'ajax',
			url:'/resolve/service/mcp/blueprint/list',
			reader:{
				type:'json',
				root:'records'
			}
		}
	},

	//in this case, it's being intialized in init() function because we have a locale renderer.
	columns:[],

	//attribute that keeps track your selections. It will store items you selected
	//need to follow the naming convention: [name of the grid + 'Selections']
	serverSelections:[],
	allSelected: false,

	activate: function() {
		window.document.title = this.localize('windowTitleServer');
	},	

	init: function() {
		if (this.mock) this.backend = RS.mcp.createMockBackend(true);
		var me = this;
		this.serverStore.load();
//		this.serverStore.pageSize = 1;
		this.set('columns', [
     	    {
    	  		header : '~~name~~',
    	  		dataIndex : 'name',
    	  		filterable : true,
    	  		flex : 1
      		},
      		{
          		header : '~~serverIp~~',
          		dataIndex : 'serverIp',
          		filterable : true,
          		flex : 1
          	},
          	{
          		header : '~~status~~',
          		dataIndex : 'status',
          		filterable : true,
          		flex : 1,
          		renderer : function(value) {
         			return me.localize(value);
   	      		}
          	},
          	{
          		header : '~~version~~',
          		dataIndex : 'version',
          		filterable : true,
          		flex : 1
          	},
          	{
          		header : '~~buildDate~~',
          		dataIndex : 'build',
          		filterable : true,
          		flex : 1
          	},
          	{
          		header : '~~worksheet~~',
          		dataIndex : 'worksheet',
          		filterable : true,
          		renderer: RS.common.grid.internalLinkRenderer('RS.worksheet.Worksheet', 'id', 'worksheetId'),
          		flex : 1
          	}]
		);
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll);
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false);
	},

	deployUpdateAction: function() {
		this.open({
			mtype: 'RS.mcp.UpdatePicker',
			callback: this.reallyDeployUpdate
		})
	},

	deployUpdateActionIsEnabled$: function() {
		return this.serverSelections.length > 0
	},

	// function that fires when you click the edit icon in the first column of the grid
	// won't be triggered if field 'id' is not specified
	edit:function(id){
		//alert(id);
		clientVM.handleNavigation({
			modelName: 'RS.mcp.BlueprintItem',
			params: {
				id: id
			}
		});
	},

	reallyDeployUpdate: function(updateFile) {
		var fileName = updateFile[0];
		var ids = [];
		Ext.each(this.serverSelections, function(selection) {
			ids.push(selection.get('id'));
		});
		this.ajax({
			url: '/resolve/service/mcp/server/update',
			params: {
				ids: ids.join(','),
				updateFilename: fileName.get('name'),
				force: "false"
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success)
					this.serverStore.load();
				else
					clientVM.displayError(response.message);
			}
		});
	}
});