glu.defModel('RS.mcp.Blueprint', {

	mock: false,
	stateId: 'blueprintList',

	blueprintStore:{
		mtype:'store',
		//fields:['id', 'serverIp', 'summary', 'status', 'tag', 'worksheet' ],
		fields:['id', 'name', 'serverIp', 'sshPort', 'remoteUser', 'resolveHome', 'status', 'worksheet', 'worksheetId' ],
		proxy:{
			type:'ajax',
			url:'/resolve/service/mcp/blueprint/list',
			reader:{
				type:'json',
				root:'records'
			}
		}
	},

	//in this case, it's being intialized in init() function because we have a locale renderer.
	columns:[],

	//attribute that keeps track your selections. It will store items you selected
	//need to follow the naming convention: [name of the grid + 'Selections']
	blueprintSelections:[],
	allSelected: false,

	activate: function() {
		window.document.title = this.localize('windowTitleBlueprint');
	},

	init: function() {
		if (this.mock) this.backend = RS.mcp.createMockBackend(true);
		var me = this;
		this.blueprintStore.load();
//		this.blueprintStore.pageSize = 1;
		this.set('columns', [
     	    {
    	  		header : '~~name~~',
    	  		dataIndex : 'name',
    	  		filterable : true,
    	  		flex : 1
      		},
      		{
          		header : '~~serverIp~~',
          		dataIndex : 'serverIp',
          		filterable : true,
          		flex : 1
          	},
          	{
          		header : '~~sshPort~~',
          		dataIndex : 'sshPort',
          		filterable : true,
          		flex : 1
          	},
          	{
          		header : '~~remoteUser~~',
          		dataIndex : 'remoteUser',
          		filterable : true,
          		flex : 1
          	},
          	{
          		header : '~~resolveHome~~',
          		dataIndex : 'resolveHome',
          		filterable : true,
          		flex : 1
          	},
          	{
          		header : '~~status~~',
          		dataIndex : 'status',
          		filterable : true,
          		flex : 1,
          		renderer : function(value) {
         			return me.localize(value);
   	      		}
          	},
          	{
          		header : '~~worksheet~~',
          		dataIndex : 'worksheet',
          		filterable : true,
          		renderer: RS.common.grid.internalLinkRenderer('RS.worksheet.Worksheet', 'id', 'worksheetId'),
          		flex : 1
          	}]
		);
	},
	// items in the toolbar
	createAction:function(){
		//alert('new clicked');
		clientVM.handleNavigation({
			modelName: 'RS.mcp.BlueprintItem'
		});
	},
	// function that fires when you click the edit icon in the first column of the grid
	// won't be triggered if field 'id' is not specified
	edit:function(id){
		//alert(id);
		clientVM.handleNavigation({
			modelName: 'RS.mcp.BlueprintItem',
			params: {
				id: id
			}
		});
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll);
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false);
	},

	deployAction:  function() {
		this.message({
			title: this.localize('deployTitle'),
			msg: this.localize(this.blueprintSelections.length == 1 ? 'deployMessage' : 'deployesMessage', this.blueprintSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deployAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeployRecords
		});
	},
	reallyDeployRecords: function(button) {
		if (button === 'yes') {
			var ids = [];
			Ext.each(this.blueprintSelections, function(selection) {
				ids.push(selection.get('id'));
			});
			this.ajax({
				url: '/resolve/service/mcp/blueprint/deployBlueprints',
				params: {
					ids: ids.join(',')
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success)
						this.blueprintStore.load();
					else
						clientVM.displayError(response.message);
				}
			});
		}
	},
	verifyAction: function() {
		this.message({
			title: this.localize('verifyTitle'),
			msg: this.localize(this.blueprintSelections.length == 1 ? 'verifyMessage' : 'verifyMessage', this.blueprintSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('verifyAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyVerifyRecords
		});
	},
	reallyVerifyRecords: function(button) {
		if (button === 'yes') {
			var ids = [];
			Ext.each(this.blueprintSelections, function(selection) {
				ids.push(selection.get('id'));
			});
			this.ajax({
				url: '/resolve/service/mcp/blueprint/verify',
				params: {
					ids: ids.join(',')
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success)
						this.blueprintStore.load();
					else
						clientVM.displayError(response.message);
				}
			});
		}
	},
	deleteAction: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.blueprintSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.blueprintSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		});
	},
	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			var ids = [];
			Ext.each(this.blueprintSelections, function(selection) {
				ids.push(selection.get('id'));
			});
			this.ajax({
				url: '/resolve/service/mcp/blueprint/delete',
				params: {
					ids: ids.join(',')
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success)
						this.blueprintStore.load();
					else
						clientVM.displayError(response.message);
				}
			});
		}
	},
});