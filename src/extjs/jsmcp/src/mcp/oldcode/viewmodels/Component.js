glu.defModel('RS.mcp.Component', {

	mock: false,
	stateId: 'componentList',

	componentStore:{
		mtype:'store',
		fields:['id', 'guid', 'serverIp', 'serverName', 'componentName', 'instanceName', 'status', {name:'lastStatusUpdatedOn',type:'date',format:'time',dateFormat:'time'}, 'worksheetId', 'worksheet' ],
		groupField: 'serverIp',
		remoteSort: true,
		sorters:['componentName'],
		proxy:{
			type:'ajax',
			url:'/resolve/service/mcp/component/list',
			reader:{
				type:'json',
				root:'records'
			}
		}
	},

	//in this case, it's being intialized in init() function because we have a date renderer.
	columns:[],

	//attribute that keeps track your selections. It will store items you selected
	//need to follow the naming convention: [name of the grid + 'Selections']
	componentSelections:[],
	allSelected: false,

	activate: function() {
		window.document.title = this.localize('windowTitleComponent');
	},

	init: function() {
		if (this.mock) this.backend = RS.mcp.createMockBackend(true);
		this.componentStore.load();
		me = this;
		this.set('columns', [
	  		{
	      		header : '~~serverIp~~',
	      		dataIndex : 'serverIp',
	      		filterable : true,
	      		flex : 1
	      	},
	      	{
	      		header : '~~componentName~~',
	      		dataIndex : 'componentName',
	      		filterable : true,
	      		flex : 1
	      	},
	      	{
	      		header : '~~instanceName~~',
	      		dataIndex : 'instanceName',
	      		filterable : true,
	      		flex : 1
	      	},
	      	{
	      		header : '~~status~~',
	      		dataIndex : 'status',
	      		filterable : true,
	      		flex : 1
	      	},
	      	{
	      		header : '~~lastStatusUpdatedOn~~',
	      		dataIndex : 'lastStatusUpdatedOn',
	      		filterable : true,
	      		flex : 1,
	      		renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
	      	},
	      	/*
	      	{
	      		header : '~~worksheetId~~',
	      		dataIndex : 'worksheetId',
	      		hidden: true
	      	},*/
			{
		  		header : '~~guid~~',
		  		dataIndex : 'guid',
		  		filterable : true,
		  		flex : 1
	  		},
	      	{
	      		header : '~~worksheet~~',
	      		dataIndex : 'worksheet',
	      		filterable : true,
	      		flex : 1,
	      		/*
	      		renderer: function(value, metaData, record, rowIndex, colIndex, store) {
	      	      //The <a> tag is used to create an anchor to link from
	      	      //the href attribute is used to address the document to link to
	      	      //the words between the open and close of the anchor tag will
	      	      //be displayed as a hyperlink (value).
	      	      //the target attribute defines where the linked document will
	      	      //be opened (_blank = open the document in a new browser window)
	      			//alert('Hello' + record.worksheetId());
	      			if(record.get('worksheetId'))
	      			{
	      				return '<a href="rsclient.jsp?#RS.client.URLScreen/location=http://localhost:8888/resolve/jsp/rsworksheet.jsp?main=Worksheet&title=' + value + '&sys_id=' + record.get('worksheetId') + '" target="_blank">'
	      					+ value +'</a>';
      				}
	      	   }//end renderer
	      	   */
	      	}]
		);
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll);
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false);
	},

	startAction:  function() {
		this.message({
			title: this.localize('startTitle'),
			msg: this.localize(this.componentSelections.length == 1 ? 'startMessage' : 'startsMessage', this.componentSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('startAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyStartAction
		});
	},
	reallyStartAction: function(button) {
		if (button === 'yes') {
			var ids = [];
			Ext.each(this.componentSelections, function(selection) {
				ids.push(selection.get('id'));
			});
			this.ajax({
				url: '/resolve/service/mcp/component/start',
				params: {
					ids: ids.join(',')
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success)
						this.componentStore.load();
					else
						clientVM.displayError(response.message);
				}
			});
		}
	},
	stopAction:  function() {
		this.message({
			title: this.localize('stopTitle'),
			msg: this.localize(this.componentSelections.length == 1 ? 'stopMessage' : 'stopsMessage', this.componentSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('stopAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyStopAction
		});
	},
	reallyStopAction: function(button) {
		if (button === 'yes') {
			var ids = [];
			Ext.each(this.componentSelections, function(selection) {
				ids.push(selection.get('id'));
			});
			this.ajax({
				url: '/resolve/service/mcp/component/stop',
				params: {
					ids: ids.join(',')
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success)
						this.componentStore.load();
					else
						clientVM.displayError(response.message);
				}
			});
		}
	},
	restartAction:  function() {
		this.message({
			title: this.localize('restartTitle'),
			msg: this.localize(this.componentSelections.length == 1 ? 'restartMessage' : 'restartsMessage', this.componentSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('restartAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyRestartAction
		});
	},
	reallyRestartAction: function(button) {
		if (button === 'yes') {
			var ids = [];
			Ext.each(this.componentSelections, function(selection) {
				ids.push(selection.get('id'));
			});
			this.ajax({
				url: '/resolve/service/mcp/component/restart',
				params: {
					ids: ids.join(',')
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success)
						this.componentStore.load();
					else
						clientVM.displayError(response.message);
				}
			});
		}
	},
	statusAction:  function() {
		this.message({
			title: this.localize('statusTitle'),
			msg: this.localize(this.componentSelections.length == 1 ? 'statusMessage' : 'statussMessage', this.componentSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('statusAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyStatusAction
		});
	},
	reallyStatusAction: function(button) {
		if (button === 'yes') {
			var ids = [];
			Ext.each(this.componentSelections, function(selection) {
				ids.push(selection.get('id'));
			});
			this.ajax({
				url: '/resolve/service/mcp/component/status',
				params: {
					ids: ids.join(',')
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success)
						this.componentStore.load();
					else
						clientVM.displayError(response.message);
				}
			});
		}
	},
});