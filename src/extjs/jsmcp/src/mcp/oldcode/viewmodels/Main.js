glu.defModel('RS.mcp.Main', {

	mock: false,

	displayName:'displayName',

	mcpStore:{
		mtype:'store',
		fields:['ucomponent'],
		proxy:{
			type:'ajax',
			url:'/resolve/service/mcp/get',
			reader:{
				type:'json',
				root:'records'
			}
		}
	},
	//attribute that keeps track your selections. It will store items you selected
	//need to follow the naming convention: [name of the grid + 'Selections']
	mcpSelections:[],

	columns:[{
		header:'~~component~~',
		dataIndex:'ucomponent',
		filterable:true,
		flex:1
	}],

	activate: function() {
		window.document.title = this.localize('windowTitle')
	},

	init: function() {
		if (this.mock) this.backend = RS.mcp.createMockBackend(true);
		// this.mcpStore.load(function(records,op,suc){
		// 	alert(records.length);
		// });
	},
	// items in the toolbar
	test:function(){
		alert('test');
	},
	// function that fires when you click the edit icon in the first column of the grid
	// won't be triggered if field 'id' is not specified
	action:function(id){
		alert(id);
	},
	// a formula, fired when the content of the mcpSelections is changed
	testIsEnabled$:function(){
		return this.mcpSelections.length>0;
	}
})