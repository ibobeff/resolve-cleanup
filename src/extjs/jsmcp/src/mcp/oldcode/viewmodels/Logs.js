glu.defModel('RS.mcp.Logs', {

    mock: false,

    logTreeMenuStore: {
        mtype: 'treestore',
        fields: ['id', 'name'],

        proxy: {
            type: 'ajax',
            url: '/resolve/service/mcp/loglist',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },

    logTreeMenuPathColumns: [],

    activate: function() {
        clientVM.setWindowTitle(this.localize('windowTitleLogs'));
    },

    init: function() {
        if (this.mock)
            this.backend = RS.actiontask.createMockBackend(true)

        this.set('logTreeMenuPathColumns', [{
            xtype: 'treecolumn',
            dataIndex: 'name',
            text: 'Menu Path',
            flex: 1
        }])

        this.logTreeMenuStore.on('beforeload', function(store, operation, eOpts) {
            operation.params = operation.params || {};
            Ext.apply(operation.params, {
                field: this.searchField,
                serverId: operation.node.getId()
            });

            this.set('searchField', 'name');
        }, this)

        this.logTreeMenuStore.on('load', function(store, rec, suc) {
            if (!suc)
                clientVM.displayError(this.localize('LoadTreeErr'));
        }, this);
    },

    deleteAction: function() {
        if (this.selectedTreeNodeId) 
        {
            this.message({
                title: this.localize('deleteTitle'),
                msg: this.localize('deleteMessage'),
                buttons: Ext.MessageBox.YESNO,
                buttonText: {
                    yes: this.localize('deleteAction'),
                    no: this.localize('cancel')
                },
                scope: this,
                fn: this.reallyDeleteRecords
            });
        }
    },

    reallyDeleteRecords: function(button) {
        if (button === 'yes') {
            var ids = [];
            ids.push(this.selectedTreeNodeId);
            this.ajax({
                url: '/resolve/service/mcp/file/delete',
                params: {
                    ids: ids.join(',')
                },
                scope: this,
                success: function(r) {
                    var response = RS.common.parsePayload(r);
                    if (response.success)
                    {
                        this.set('logDetail', '');
                        this.set('selectedTreeNodeId', '');

                        var logTreePanel = Ext.getCmp('logTreePanel');
                        var sm = logTreePanel.getSelectionModel();
                        if (sm.hasSelection()) {
                            sm.getSelection()[0].remove(true);
                        }
                    }
                    else
                        clientVM.displayError(response.message);
                },

                failure: function(resp) {
                    clientVM.displayFailure(resp);
                }
            });
        }
    },  

    deleteActionIsEnabled$: function() {
        return this.logDetail && this.logDetail != '';
    },

    selectedTreeNodeId: '',

    searchField: 'server',

    loadLog: function() {
        this.fireEvent('selectedTreeNodeIdChanged');
        // this.set('selectedTreeNodeId', this.selectedTreeNodeId);
    },

    // logFiles: function() {
    //     var logTreePanel = Ext.getCmp('logTreePanel');

    //     sm = logTreePanel.getSelectionModel()
    //     if (sm.hasSelection()) {
    //         this.set('selectedTreeNodeId', sm.getSelection()[0].internalId)
    //     } else {
    //         this.set('selectedTreeNodeId', 'root_menu_id')
    //     }
    // },

    when_selected_menu_node_id_changes_update_page: {
        on: ['selectedTreeNodeIdChanged'],
        action: function() {
            console.log("load file store: " + this.selectedTreeNodeId)
            if (this.selectedTreeNodeId) {
                this.ajax({
                    url: '/resolve/service/mcp/log/get',
                    params: {
                        id: this.selectedTreeNodeId
                    },
                    success: function(r) {
                        var response = RS.common.parsePayload(r)
                        if (response.success) {
                            this.set('logDetail', response.data);
                        }
                        else {
                            clientVM.displayError(response.message)
                        }
                    }
                })
            }
        }
    },

    logTreeSelectionchange: function(selected, eOpts) {
        if (selected.length > 0) {
            // console.log(selected[0]);
            var nodeId = selected[0].getId();
            this.set('selectedTreeNodeId', nodeId)
        }
    },

    logDetail: '',

    logDisplay$: function() {
        // console.log('load log display');
        return Ext.util.Format.nl2br(this.logDetail)
    },
})
