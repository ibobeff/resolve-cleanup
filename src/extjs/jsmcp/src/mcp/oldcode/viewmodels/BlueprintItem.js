glu.defModel('RS.mcp.BlueprintItem', {
	type : 'BlueprintItem',
	initialized : false,
	fields : [ 'id', 'name', 'serverIp', 'status', 'sshPort', 'remoteUser', 'remotePassword', 'resolveHome', 'terminalPrompt', 'privateKeyLocation', 'tag', 'description', 'blueprint' ],
	id : '',
	name : '',
	serverIp : 'Extracted from Blueprint\'s LOCALHOST property',
	status : 'NOT DEPLOYED',
	sshPort : '22',
	remoteUser : 'resolve',
	remotePassword : '',
	resolveHome : '/opt/resolve',
	terminalPrompt : '$',
	privateKeyLocation : '',
	tag : '',
	description : '',
	blueprint : '',

	init : function()
	{
		//alert(this.id);
		if (this.id)
		{
			this.loadBlueprint();
		}
	},

	activate: function(screen, params) {
		var id = this.id || "";
		this.resetForm();
		this.set("id", params.id);
		this.loadBlueprint();
		window.document.title = this.localize('windowTitleBlueprint');
	},

	resetForm : function()
	{
		this.loadData(
		{
			id : '',
			name : '',
			serverIp : 'Extracted from Blueprint\'s LOCALHOST property',
			status : 'NOT DEPLOYED',
			sshPort : '22',
			remoteUser : 'resolve',
			remotePassword : '',
			resolveHome : '/opt/resolve',
			terminalPrompt : '$',
			privateKeyLocation : '',
			tag : '',
			description : '',
			blueprint : '',
		});
	},

	loadBlueprint: function() {
		if(this.id)
		{
			this.ajax({
				url: '/resolve/service/mcp/blueprint/get',
				params: {
					id: this.id
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) {
						//Load the fields for the blueprint
						this.loadData(response.data);
						//this.updateWindowTitle()
					} else {
						clientVM.displayError(response.message);
					}
				}
			});
		}
	},
	back: function() {
		clientVM.handleNavigationBack();
	},
	save: function() {
		var serializedBlueprint = {
			id: this.id,
			name: this.name,
			serverIp: this.serverIp,
			sshPort : this.sshPort,
			remoteUser : this.remoteUser,
			remotePassword : this.remotePassword,
			resolveHome : this.resolveHome,
			terminalPrompt : this.terminalPrompt,
			privateKeyLocation : this.privateKeyLocation,
			tag: this.tag,
			description: this.description,
			blueprint: this.blueprint
		};
		this.ajax({
			url: '/resolve/service/mcp/blueprint/save',
			jsonData: serializedBlueprint,
			scope: this,
			success: function(r) {
				//this.set('persisting', false);
				var response = RS.common.parsePayload(r);
				if (response.success) {
					clientVM.displaySuccess(this.localize('BlueprintSaved'));
					//once save completed, go back
					clientVM.handleNavigationBack();
					//this.set('id', response.data);
				} else {
					var msgs = response.message.split(', '),
						messages = [];
					Ext.Array.forEach(msgs, function(msg) {
						messages.push(this.localize(msg));
					}, this);
					clientVM.displayError(messages.join(', '));
				}
			},
			failure: function() {
				//this.set('persisting', false)
			}
		});
	},
	deployAction: function() {
		this.message({
			title: this.localize('deployTitle'),
			msg: this.localize('deployMessage'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deployAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeployRecords
		});
	},
	reallyDeployRecords: function(button) {
		if (button === 'yes') {
			var serializedBlueprint = {
				id: this.id,
				name: this.name,
				serverIp: this.serverIp,
				status: "DEPLOYING",
				sshPort : this.sshPort,
				remoteUser : this.remoteUser,
				remotePassword : this.remotePassword,
				resolveHome : this.resolveHome,
				terminalPrompt : this.terminalPrompt,
				privateKeyLocation : this.privateKeyLocation,
				tag: this.tag,
				description: this.description,
				blueprint: this.blueprint
			};
			this.ajax({
				url: '/resolve/service/mcp/blueprint/deploy',
				jsonData: serializedBlueprint,
				scope: this,
				success: function(r) {
					//this.set('persisting', false);
					var response = RS.common.parsePayload(r);
					if (response.success) {
						clientVM.displaySuccess(this.localize('BlueprintDeployed'));
						//once save completed, go back
						clientVM.handleNavigationBack();
						//this.set('id', response.data);
					} else {
						var msgs = response.message.split(', '),
							messages = [];
						Ext.Array.forEach(msgs, function(msg) {
							messages.push(this.localize(msg));
						}, this);
						clientVM.displayError(messages.join(', '));
					}
				},
				failure: function() {
					//this.set('persisting', false)
				}
			});
		}
	},
	/*
	verifyAction: function() {
		this.message({
			title: this.localize('verifyTitle'),
			msg: this.localize('verifyMessage'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('verifyAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyVerifyRecords
		});
	},
	reallyVerifyRecords: function(button) {
		if (button === 'yes') {
			var ids = [];
			ids.push(this.id);
			this.ajax({
				url: '/resolve/service/mcp/blueprint/verify',
				params: {
					ids: ids.join(',')
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success)
					{
						this.blueprintStore.load();
						clientVM.handleNavigationBack();
					}
					else
					{
						clientVM.displayError(response.message);
					}
				}
			});
		}
	},
	*/
});