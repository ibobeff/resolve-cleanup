glu.defView('RS.mcp.BlueprintItem', {
	xtype: 'form',
	bodyPadding: '10px',
	autoScroll: true,
	defaults: {
		anchor: '-20'
	},
	title: '~~blueprintItemTitle~~',
	layout: {type: 'vbox', align: 'stretch'},
	items: [
	{
		xtype: 'textfield',
		name: 'name',
		allowBlank: false
	}, {
		xtype: 'textfield',
		name: 'serverIp',
		disabled: true
	}, {
		xtype: 'textfield',
		name: 'status',
		disabled: true
	}, {
		xtype: 'textfield',
		name: 'sshPort'
	}, {
		xtype: 'textfield',
		name: 'remoteUser'
	}, {
		xtype: 'textfield',
		name: 'remotePassword',
		fieldLabel: '~~remoteP_assword~~',
		inputType: "password"
	}, {
		xtype: 'textfield',
		name: 'resolveHome'
	}, {
		xtype: 'textfield',
		name: 'terminalPrompt'
	}, {
		xtype: 'textfield',
		name: 'privateKeyLocation'
	}, {
		xtype: 'textfield',
		name: 'tag'
	}, {
		xtype: 'textarea',
		name: 'description'
	}, {
		xtype: 'textarea',
		name: 'blueprint',
		flex: 1
	}],
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar',
		items: [{
			name: 'back'
		}, {
			name: 'save',
		}, {
			name: 'deployAction',
		}]
	}],
});