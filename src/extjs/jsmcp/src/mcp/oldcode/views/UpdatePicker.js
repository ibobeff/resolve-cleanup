glu.defView('RS.mcp.UpdatePicker', {
	asWindow: {
		title: '~~displayNameSelectUpdate~~',	
		width: 600,
		height: 400
	},

	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'updates',
		columns: '@{columns}',
		plugins: [{
			ptype: 'pager'
		}],
		selModel: {
			mode: 'MULTI'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: []
		}]
	}],

	buttonAlign: 'left',
	buttons: ['deploy', 'cancel']
});
