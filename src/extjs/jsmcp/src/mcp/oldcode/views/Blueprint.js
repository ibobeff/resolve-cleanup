glu.defView('RS.mcp.Blueprint', {
	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'blueprint',
		border: false,
		stateId: '@{stateId}',
		displayName: '~~displayNameBlueprint~~',
		stateful: true,
		store : '@{blueprintStore}',
		//since we added columns[] in the ViewModel this declaration is important or it'll not show up
		columns: '@{columns}',
		//This is the tool bar where we have buttons like "New" etc.
		dockedItems : [ {
			xtype : 'toolbar',
			dock : 'top',
			cls : 'actionBar',
			name : 'actionBar',
			// 'create' is defined in the viewmodel as a function
			// it also renders the button Label from the locale file
			// with the same name, check local_en.js for a key "create"
			// you can add more buttons with exact same convension
			items: ['createAction','deleteAction','deployAction','verifyAction']
		} ],

		//This section injects plugins, "searchfilter" plugin
		//id developed by someone else and available through some common
		//code that gets included at runtime.
		plugins : [ {
			ptype : 'searchfilter',
			allowPersistFilter : false,
			hideMenu : true
		}, {
			ptype : 'pager'
		}, {
			ptype : 'columnautowidth'
		} ],

		selModel : {
			selType : 'resolvecheckboxmodel',
			columnTooltip : RS.common.locale.editColumnTooltip,
			columnTarget : '_self',
			columnEventName : 'editAction',
			columnIdField : 'id'
		},
		listeners : {
			editAction : '@{edit}',
			selectAllAcrossPages: '@{selectAllAcrossPages}',
			selectionChange: '@{selectionChanged}',
		}
	}]
});