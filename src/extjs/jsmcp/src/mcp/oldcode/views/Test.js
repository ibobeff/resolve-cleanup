/*glu.defModel('RS.mcp.Test', {
	mock: false,
	stateId: 'testList',

	serverStore:{
		mtype:'store',
		fields:['id', 'serverIp', 'sshPort', 'remoteUser', 'resolveHome', 'summary', 'status', 'worksheet' ],
		proxy:{
			type:'ajax',
			url:'/resolve/service/mcp/blueprint/list',
			reader:{
				type:'json',
				root:'records'
			}
		}
	},

	columns : [{
      		header : '~~serverIp~~',
      		dataIndex : 'serverIp',
      		filterable : true,
      		flex : 1      	},
      	{
      		header : '~~sshPort~~',
      		dataIndex : 'sshPort',
      		filterable : true,
      		flex : 1
      	},
      	{
      		header : '~~remoteUser~~',
      		dataIndex : 'remoteUser',
      		filterable : true,
      		flex : 1
      	},
      	{
      		header : '~~resolveHome~~',
      		dataIndex : 'resolveHome',
      		filterable : true,
      		flex : 1
      	},
      	{
      		header : '~~summary~~',
      		dataIndex : 'summary',
      		filterable : true,
      		flex : 1
      	},
      	{
      		header : '~~status~~',
      		dataIndex : 'status',
      		filterable : true,
      		flex : 1,
      		renderer : function(value, metadata, record) {
      			//this checks if the value contains the string
      			if (value.match("FAILED") || value.match("UNSYNCH")) {
      			   metadata.tdCls = metadata.tdCls +" failed";
      			} else {
       			   metadata.tdCls = metadata.tdCls +" passed";
  				}
      			return value;
      	    }
      	},
      	{
      		header : '~~worksheet~~',
      		dataIndex : 'worksheet',
      		filterable : true,
      		flex : 1
      	}
  	],

	//attribute that keeps track your selections. It will store items you selected
	//need to follow the naming convention: [name of the grid + 'Selections']
	serverSelections:[],
	allSelected: false,

	activate: function() {
		window.document.title = this.localize('windowTitleServer');
	},

	init: function() {
		if (this.mock) this.backend = RS.mcp.createMockBackend(true);
		this.serverStore.load();
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll);
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false);
	},

	// function that fires when you click the edit icon in the first column of the grid
	// won't be triggered if field 'id' is not specified
	view:function(id){
		//alert(id);
		clientVM.handleNavigation({
			modelName: 'RS.mcp.BlueprintItem',
			params: {
				id: id
			}
		});
	},

	startAction:  function() {
		this.message({
			title: this.localize('startTitle'),
			msg: this.localize(this.serverSelections.length == 1 ? 'startMessage' : 'startsMessage', this.serverSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('startAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyStartAction
		});
	},
	reallyStartAction: function(button) {
		if (button === 'yes') {
			var ids = [];
			Ext.each(this.serverSelections, function(selection) {
				ids.push(selection.get('id'));
			});
			this.ajax({
				url: '/resolve/service/mcp/server/start',
				params: {
					ids: ids.join(',')
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success)
						this.serverStore.load();
					else
						clientVM.displayError(response.message);
				}
			});
		}
	},
	stopAction:  function() {
		this.message({
			title: this.localize('stopTitle'),
			msg: this.localize(this.serverSelections.length == 1 ? 'stopMessage' : 'stopsMessage', this.serverSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('startAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyStopAction
		});
	},
	reallyStopAction: function(button) {
		if (button === 'yes') {
			var ids = [];
			Ext.each(this.serverSelections, function(selection) {
				ids.push(selection.get('id'));
			});
			this.ajax({
				url: '/resolve/service/mcp/server/stop',
				params: {
					ids: ids.join(',')
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success)
						this.serverStore.load();
					else
						clientVM.displayError(response.message);
				}
			});
		}
	},
	statusAction:  function() {
		this.message({
			title: this.localize('statusTitle'),
			msg: this.localize(this.serverSelections.length == 1 ? 'statusMessage' : 'statussMessage', this.serverSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('statusAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyStatusAction
		});
	},
	reallyStatusAction: function(button) {
		if (button === 'yes') {
			var ids = [];
			Ext.each(this.serverSelections, function(selection) {
				ids.push(selection.get('id'));
			});
			this.ajax({
				url: '/resolve/service/mcp/server/status',
				params: {
					ids: ids.join(',')
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success)
						this.serverStore.load();
					else
						clientVM.displayError(response.message);
				}
			});
		}
	},
});
*/