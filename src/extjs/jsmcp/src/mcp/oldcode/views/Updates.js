glu.defView('RS.mcp.Updates', {
	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'updates',
		border: false,
		stateId: '@{stateId}',
		displayName: '~~displayNameUpdates~~',
		stateful: true,
		store : '@{updatesStore}',
		//since we added columns[] in the ViewModel this declaration is important or it'll not show up
		columns: '@{columns}',
		//This is the tool bar where we have buttons like "New" etc.
		dockedItems : [ {
			xtype : 'toolbar',
			dock : 'top',
			cls : 'actionBar',
			name : 'actionBar',
			// 'create' is defined in the viewmodel as a function
			// it also renders the button Label from the locale file
			// with the same name, check local_en.js for a key "create"
			// you can add more buttons with exact same convension
			items: ['addAction', 'deleteAction']
		} ],

		//This section injects plugins, "searchfilter" plugin
		//id developed by someone else and available through some common
		//code that gets included at runtime.
		plugins : [ {
			ptype : 'searchfilter',
			allowPersistFilter : false,
			hideMenu : true
		}, {
			ptype : 'pager'
		}, {
			ptype : 'columnautowidth'
		} ],

		// selModel : {
		// 	selType : 'resolvecheckboxmodel',
		// 	columnTooltip : RS.common.locale.editColumnTooltip,
		// 	columnTarget : '_self',
		// 	columnEventName : 'editAction',
		// 	columnIdField : 'id'
		// },
		listeners : {
			selectionChange: '@{selectionChanged}',
		}
	}]
});