glu.defView('RS.mcp.Component', {
	layout: 'fit',
	items: [{
		xtype: 'grid',
	    requires: [
	        'Ext.grid.feature.Grouping'
	    ],
	    collapsible: true,
	    features: [{
	        ftype: 'grouping',
	        //groupHeaderTpl: '{columnName}: {serverName} ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})',
	        //groupHeaderTpl: '{columnName}',
	        hideGroupedHeader: true,
	        startCollapsed: false,
	        id: 'serverIpGrouping'
	    }],
		name: 'component',
		border: false,
		stateId: '@{stateId}',
		displayName: '~~displayNameComponent~~',
		stateful: true,
		store : '@{componentStore}',
		//since we added columns[] in the ViewModel this declaration is important or it'll not show up
		columns: '@{columns}',
		//This is the tool bar where we have buttons like "New" etc.
		dockedItems : [ {
			xtype : 'toolbar',
			dock : 'top',
			cls : 'actionBar',
			name : 'actionBar',
			// 'create' is defined in the viewmodel as a function
			// it also renders the button Label from the locale file
			// with the same name, check local_en.js for a key "create"
			// you can add more buttons with exact same convension
			items: ['startAction','stopAction','restartAction','statusAction']
		} ],

		//This section injects plugins, "searchfilter" plugin
		//id developed by someone else and available through some common
		//code that gets included at runtime.
		plugins : [ {
			ptype : 'searchfilter',
			allowPersistFilter : false,
			hideMenu : true
		}, {
			ptype : 'pager'
		}, {
			ptype : 'columnautowidth'
		} ],
	}]
});