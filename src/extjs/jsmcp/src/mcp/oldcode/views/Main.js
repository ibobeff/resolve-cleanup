/*
What is required for a grid:
columns----If not defined:Uncaught TypeError: Cannot read property 'length' of undefined
store----If not defined:Uncaught TypeError: Cannot read property 'recType' of undefined  

There is a default layout for a grid( may be 'fit')

To build the tool bar above the grid
specify the dockedItems in the grid panel
remember to assign 'actionBar' to the cls(css class) attribute in order to make the pager plugin funtion well

the attribute 'plugin' and the things below it are standard, it defines pager and search box plugin for our view

the record must have a id attribute, if not specified, nothing fired when you click the edit icon in the first column


*/
glu.defView('RS.mcp.Main', {
	xtype:'grid',
	// layout:'fit',
	store:'@{mcpStore}',
	name:'mcp',
	columns:[{
		header:'~~component~~',
		dataIndex:'ucomponent',
		filterable:true,
		flex:1
	}],

	displayName:'@{displayName}',

	dockedItems:[{
		xtype:'toolbar',
		dock: 'top',
		cls: 'actionBar',
		name: 'actionBar',
		items:['test']//'test' is correspond to the test function in the view model
	}],

	plugins: [
	{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, 
	{
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.editColumnTooltip,
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},
	listeners: {
		editAction: '@{action}'
	}
});