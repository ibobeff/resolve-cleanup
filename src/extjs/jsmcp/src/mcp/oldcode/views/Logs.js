glu.defView('RS.mcp.Logs', {
    padding: 10,
    layout: 'border',

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',
        cls: 'actionBar',
        name: 'actionBar',
        items: [{
            xtype: 'tbtext',
            text: '~~logFiles~~',
            name: 'logFiles',
            cls: 'rs-display-name',
            // enableToggle: false,
            // toggleGroup: 'mygroup',
            // allowDepress: false,
            // pressed: true,
            margin: '0 105 0 0'
        }, {
            xtype: 'button',
            name: 'deleteAction',
            margin: '0 10 0 90',
        },'->', {
            iconCls: 'x-btn-icon x-tbar-loading',
            handler: '@{loadLog}'
        }]
    }],
    items: [{
        xtype: 'treepanel',
        // layout: 'card',
        region: 'west',
        width: 350,
        split: true,
        maxWidth: 500,
        id: 'logTreePanel',
        root: {
            name: 'Server',
            id: 'root_menu_id',
            expanded: true
        },
        tbar: [],
        columns: '@{logTreeMenuPathColumns}',
        name: 'logTreeMenuStore',
        listeners: {
            selectionchange: '@{logTreeSelectionchange}'
        }
    }, {
        region: 'center',
        layout: 'fit',
        items: [{
            autoScroll: true,
            style: 'border: 1px solid #999 !important',
            bodyPadding: '10px',
            html: '@{logDisplay}'
        }]
    }]
});
