glu.defView('RS.mcp.UploadUpdate', {

	asWindow: {
		width: 600,
		height: 400,
		title: '~~windowTitleUploadUpdate~~',

		modal: true
	},

	layout: 'fit',
	items: [{
		xtype: 'imagepicker',
		allowUpload: true,
		resizable: false,
		fileServiceUploadUrl: '/resolve/service/mcp/update/upload',
		filters:  {
			title: "zip",
			extensions: "zip"
		},
		multipart: true,
		uploader: {
			uploadpath: 'dev',
			autoStart: true,
			max_file_size: '2020mb',
			flash_swf_url: '/resolve/js/plupload/plupload.flash.swf',
			urlstream_upload: true,
		},
		listeners: {
			selectionchange: '@{selectionChanged}',
			itemdblclick: '@{selectRecord}',
			uploadcomplete: function(uploader, files) {
				if (files.length > 0) this.getSelectionModel().select(this.getStore().getAt(this.getStore().find('id', files[files.length - 1].id)));
			}
		}
	}],

	buttonAlign: 'left',
	buttons: ['close']
})
