/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/

glu.defModel("RS.mcp.Admin",{
	groupName: '',
	clusterName: '',
	mcp_Pas_sword: '',
	mcpPasswordValid: false,
	mcpPasswordIsValid$: function() {
		this.set('mcpPasswordValid', false);
		if (this.mcp_Pas_sword.length > 0 && this.mcp_Pas_sword != '**********') {
			if (this.mcp_Pas_sword.indexOf('_') == 0) {
				this.set('mcpPasswordValid', true);
				return true //start with _ to force basic passwords
			}
			if (this.mcp_Pas_sword.length < 8) return this.localize('passwordMinLengthInvalid');
			if (!this.mcp_Pas_sword.match(/[0-9]+/g)) return this.localize('passwordNumberRequiredInvalid');
			if (!this.mcp_Pas_sword.match(/[A-Z]+/g)) return this.localize('passwordCapitalLetterRequiredInvalid');
			if (!this.mcp_Pas_sword.match(/[a-z]+/g)) return this.localize('passwordLowercaseLetterRequiredInvalid');

			this.set('mcpPasswordValid', true);
			return true;
		}
		return true;
	},
	mcpPasswordConfirm: '',
	mcpPasswordConfirmValid: false,
	mcpPasswordConfirmIsValid$: function() {
		this.set('mcpPasswordConfirmValid', false);
		if (this.mcp_Pas_sword && this.mcp_Pas_sword != '**********') {
			if (this.mcp_Pas_sword == this.mcpPasswordConfirm) {
				this.set('mcpPasswordConfirmValid', true);
				return true;
			}
			return this.localize('passwordConfirmMismatchInvalid');
		}
		return true;
	},
	wait : false,
	clusterStore: null,
	clusterList: null,
	newClusterList: null,
	clusterCount: 0,
	init: function() {
		this.newClusterList = [];
		this.set('clusterCount', 0);
		var config = {
			mtype: 'store',
			fields: ['name', 'value'],
			data: this.createNameValueItems(this.clusterList)
		};
		this.set('clusterStore', Ext.create('Ext.data.Store', config));

		if (this.clusterList.length) {
			this.set('clusterName', this.clusterList[0]);
	
			for (var i = 0; i < this.clusterList.length; i++) {
				this.getMCPLoginInfo(this.groupName, this.clusterList[i], function(clusterName, data){
					if (!data) {
						this.newClusterList.push(clusterName);
					}
					this.set('clusterCount', ++this.clusterCount)
				}.bind(this), 
				function(clusterName) {
					this.newClusterList.push(clusterName);
					this.set('clusterCount', ++this.clusterCount);
				}.bind(this))
			}
		}
	},
	createNameValueItems: function () {
		var values, items = [];
	
		if (arguments.length === 1) {
			values = arguments[0];
		} else {
			values = Array.apply(null, arguments);
		}
	
		for (var i = 0; i < values.length; i++) {
			items.push({ 
				name: values[i],
				value: values[i] 
			});
		}
		return items;
	},
	getMCPLoginInfo : function(groupName, clusterName, onSuccess, onFailure){
		this.set('wait', true);
		var params = {
			group : groupName,
			cluster : clusterName
		}
		this.ajax({
			url : '/resolve/service/mcplogin/findbyname',
			method : 'POST',
			params : params,
			scope: this,
			success : function(resp){
				var respData = RS.common.parsePayload(resp);
				if (respData.success){
					if (Ext.isFunction(onSuccess)) {
						onSuccess(clusterName, respData.data);
					}
				} else {
					if (Ext.isFunction(onFailure)) {
						onFailure(clusterName, resp);
					}
				}
			},
			failure : function(resp){
				clientVM.displayFailure(resp);
				if (Ext.isFunction(onFailure)) {
					onFailure(clusterName, resp);
				}
			},
			callback: function() {
				 this.set('wait', false);
			}
		})
	},
	saveMCPLoginInfo : function(onSuccess, onFailure, onComplete){
		this.set('wait', true);
		this.ajax({
			url : '/resolve/service/mcplogin/save',
			method : 'POST',
			jsonData: {
				UGroupName: this.groupName,
				UClusterName: this.clusterName,
				UPWDValue: this.mcp_Pas_sword
			},
			scope: this,
			success : function(resp){
				var respData = RS.common.parsePayload(resp);
				if (respData.success){
					clientVM.displaySuccess(this.localize('SaveSuccess'));
					if (Ext.isFunction(onSuccess)) {
						onSuccess(resp);
					}
				} else {
					clientVM.displayError(this.localize('SaveErr') + '[' + respData.message + ']')
					if (Ext.isFunction(onFailure)) {
						onFailure(resp);
					}
				}
			},
			failure : function(resp){
				clientVM.displayFailure(resp);
				if (Ext.isFunction(onFailure)) {
					onFailure(resp);
				}
			},
			callback: function() {
				this.set('wait', false);
				if (Ext.isFunction(onComplete)) {
					onComplete();
				}
			}
		})
	},
	isNewCluster$: function() {
		if (this.clusterCount && this.newClusterList && this.newClusterList.indexOf(this.clusterName) != -1) {
			this.set('titleTxt', this.localize('createMCPAdminPassword'));
			this.set('submitBtnTxt', this.localize('create'));
			return true;
		} else {
			this.set('titleTxt', this.localize('modify_MCP_Ad_min_Pas_sword'));
			this.set('submitBtnTxt', this.localize('update'));
			return false;
		}
	},
	titleTxt: '',
	title$: function() {
		return this.titleTxt;
	},
	submitBtnTxt: '',
	submitBtn$: function() {
		return this.submitBtnTxt;
	},
	submitIsEnabled$: function() {
		return !this.wait && this.mcpPasswordValid && this.mcpPasswordConfirmValid;
	},
	submit: function() {
		this.saveMCPLoginInfo(function() {
			// only close dialog if successfully saved password
			this.doClose();
		}.bind(this));
	},
	cancel : function(){
		this.doClose();
	}
})

glu.ns("RS.mcp");
RS.mcp.MetaConfigMap = {
	severityStore : {
		mtype : 'store',
		proxy : {
			type :'memory'
		},
		fields : ['display','UAlertStatus'],
		data : [
			{	display : 'Fatal',
				UAlertStatus : 'fatal'
			},{
				display : 'Critical',
				UAlertStatus : 'critical'
			},{
				display : 'Warning',
				UAlertStatus : 'warning'
			},{
				display : 'Severe',
				UAlertStatus : 'severe'
			},{
				display : 'Info',
				UAlertStatus : 'info'
			}
		]
	},
	metricGroupStore : {
		mtype : 'store',
		proxy : {
			type : 'memory'
		},
		fields : ['display', 'UGroup'],
		data : [
			{
				display : 'JVM',
				UGroup : 'jvm'
			},{
				display : 'DATABASE',
				UGroup : 'database'
			},{
				display: 'JMS QUEUE',
				UGroup : 'jms_queue'
			},{
				display : 'LATENCY',
				UGroup : 'latency'
			},{
				display : 'RUNBOOK',
				UGroup : 'runbook'
			},{
				display : 'SERVER',
				UGroup : 'server'
			},{
				display : 'TRANSACTION',
				UGroup : 'transaction'
			},{
				display : 'USERS',
				UGroup :'users'
			}
		],
		sorters : [
		{
			property : 'display',
			direction : 'ASC'
		}]
	},
	metricNameStore : {
		mtype : 'store',
		proxy : {
			type : 'memory'
		},
		fields : ['display', 'UName'],
		data : [],
	},
	metricMetaData : {
		jvm : [
		{
			display : 'Memory Free',
			UName : 'mem_free'
		},
		{
			display : 'Thread Count',
			UName : 'thread_count'
		}],
		database : [
		{
			display : 'Free Space (MB)',
			UName : 'free_space'
		},
		{
			display : 'Size (MB)',
			UName : 'size'
		},{
			display : 'Percentage Used (%)',
			UName : 'percentage_used'
		},{
			display : 'Response Time (msec)',
			UName : 'response_time'
		},{
			display : 'Query Count',
			UName : 'query_count'
		},{
			display : 'Percentage Wait (%)',
			UName : 'percentage_wait'
		}],
		jms_queue : [
		{
			display : 'Message Count',
			UName : 'msgs_count'
		}],
		latency : [
		{
			display : 'Opened Wiki Count',
			UName : 'wiki'
		},{
			display : 'Wiki Response Time',
			UName : 'wikiresponsetime'
		},{
			display : 'Social Page Visited',
			UName : 'social',
		},{
			display : 'Social Response Time',
			UName : 'socialresponsetime'
		}],
		runbook : [
		{
			display : 'Aborted Count',
			UName : 'aborted'
		},{
			display : 'Completed Count',
			UName : 'completed'
		},{
			display : 'Duration (msec)',
			UName : 'duration'
		},{
			display : 'Good Severity Count',
			UName : 'gseverity'
		},{
			display : 'Critical Severity Count',
			UName : 'cseverity'
		},{
			display : 'Warning Serverity Count',
			UName : 'wseverity'
		},{
			display : 'Severe Severity Count',
			UName : 'sseverity'
		},{
			display : 'Unknown Severity Count',
			UName : 'useverity'
		},{
			display : 'Good Condition Count',
			UName : 'gcondition'
		},{
			display : 'Bad Condition Count',
			UName : 'bcondition'
		},{
			display : 'Unknown Condition Count',
			UName : 'ucondition'
		}],
		server : [
		{
			display : 'Load Per Minute',
			UName : 'load1'
		},{
			display : 'Load Per 5 Minutes',
			UName : 'load5'
		},{
			display : 'Load Per 15 Minutes',
			UName : 'load15'
		},{
			display : 'Server Disk Space',
			UName : 'disk_space'
		}],
		transaction : [
		{
			display : 'Count',
			UName : 'transaction'
		},{
			display : 'Latency',
			UName : 'latency'
		}],
		users : [
		{
			display : 'Active',
			UName : 'active'
		}]
	},
	operatorStore : {
		mtype : 'store',
		proxy : {
			type : 'memory'
		},
		fields : ['operator'],
		data : [
			{
				operator : '>',
				UName : 'gt'
			},
			{
				operator : '<',
				UName : 'lt'
			},
			{
				operator : '=',
				UName : 'eq'
			},
			{
				operator : '>=',
				UName : 'gte'
			},
			{
				operator : '<=',
				UName : 'lte'
			},
			// {
			// 	operator : 'contains'
			// }
		]
	}
}
glu.defModel("RS.mcp.Main", {
	displayName : '',
	thresholdStore: '',
	thresholdColumns: '',
	thresholdTabs : {
		mtype : 'RS.mcp.tabs.ThresholdTab'
	},
	wait : false,
	metricNameStore : RS.mcp.MetaConfigMap.metricNameStore,
	metricGroupStore : RS.mcp.MetaConfigMap.metricGroupStore,
	metricMetaData : RS.mcp.MetaConfigMap.metricMetaData,	
	selectedThresholdId : null,
	selectedThresholdModule : null,
	selectedThresholdName : null,
	selectedRecord : null,
	thresholdUpdated : false,
	API : {
		thresholdRuleList : '/resolve/service/metric/list',
		thresholdRuleDelete : '/resolve/service/metric/delete',
		getClusterList: '/resolve/service/mcplogin/getclusterlist'
	},
	isAdmin: false,
	groupName: '',
	clusterList: null,
	clusterListValid: false,
	init : function(){
		this.set('displayName', this.localize('thresholdRuleDisplayName'));
		this.initThresholdStore();
		this.initThresholdColumns();
		this.thresholdStore.on('load', function(){
			if(this.thresholdUpdated){
				this.reSelectThreshold();
				this.set('thresholdUpdated', false);
			}
		}, this)

		Ext.each(clientVM.user.roles, function(r) {
			if (r == 'admin') {
				this.set('isAdmin', true);
				return;
			}
		}, this);
	},
	activate : function(){
		this.thresholdStore.load();
		this.getClusterList();
	},
	getClusterList : function(){
		this.clusterList = [];
		this.set('clusterListValid', false);
		var params = {
			group : this.groupName,
		}
		this.ajax({
			url : this.API['getClusterList'],
			method : 'POST',
			params : params,
			success : function(resp){
				var respData = RS.common.parsePayload(resp);
				if (respData.success) {
					if (respData.data) {
						var clusterList = respData.data;
						for (var i=0; i< clusterList.length; i++) {
							this.clusterList.push(clusterList[i]);
							this.set('clusterListValid', true);
						}
					}
				} else {
					clientVM.displayError(respData.message);
				}
			},
			failure : function(resp){
				clientVM.displayFailure(resp);
			}
		});
	},
	initThresholdStore : function(){
		var me = this;
		var fields = ['uruleModule','uruleName','usource','ualertStatus',
			{
				name : 'ugroup',
				type : 'string',
				convert : function(value, record){
					var recordData = me.metricGroupStore.findRecord('UGroup', value);
					return recordData ? recordData.get('display') : value;
				}
			},{
				name : 'uname',
				type : 'string',
				convert : function(value, record){
					var grpName = record.raw.ugroup;
					var grpArr = me.metricMetaData[grpName] || [];
					for(var i = 0; i < grpArr.length; i++){
						var metricInfo = grpArr[i];
						if( metricInfo.UName == value){
							return metricInfo.display;
						}
					}
					return value;
				}
			},{
				name : 'uactive',
				type : 'bool'
			},{
				name : 'utype',
				type : 'string',
				convert : function(value, record){
					return (value && value.toLowerCase()) == 'default' ? true : false;
				}
			},{
				name : 'isOutdated'
			},{
				name : 'uversion',
				type : 'long',
				convert : function(value, record){
					if(!value || value == 0)
						return "None (not deployable)";
					else if(record.get('isOutdated'))
						return value + '*';
					else
						return value;
				}
			}
		].concat(RS.common.grid.getSysFields());

		var store = Ext.create('Ext.data.Store', {
			fields : fields,
			proxy : {
				type : 'ajax',
				url : this.API['thresholdRuleList'],
				reader : {
					type : 'json',
					root : 'records'
				}
			},
			sorters : [{
				property : 'uruleName',
				direction : 'ASC'
			}]
		})
		this.set('thresholdStore', store);
	},
	initThresholdColumns : function(){
		var columns = [
		{
			header : "Module",
			dataIndex : 'uruleModule',
			sortable  : true,
			filterable : true,
			flex : 3
		},{
			header : "Name",
			dataIndex : "uruleName",
			sortable : true,
			filterable : true,
			flex : 3
		},{
			header : "Version",
			dataIndex : "uversion",
			sortable : false,
			filterable : false,
			flex : 2
		},{
			header : "Active",
			dataIndex : "uactive",
			flex : 1,
			align : 'center',
			renderer : RS.common.grid.booleanRenderer()
		},{
			header : "Auto Deploy",
			dataIndex : "utype",
			flex : 1,
			align : 'center',
			renderer : RS.common.grid.booleanRenderer()
		},{
			header : "Severity",
			dataIndex : "ualertStatus",
			sortable : true,
			filterable : true,
			flex : 1
		},{
			header : "Metric Group",
			dataIndex : "ugroup",
			sortable : true,
			filterable : true,
			flex : 3
		},{
			header : "Metric Name",
			dataIndex : "uname",
			sortable : true,
			filterable : true,
			flex : 3
		},{
			header : "Source",
			dataIndex : "usource",
			sortable : true,
			filterable : true,
			flex : 3
		}].concat(RS.common.grid.getSysColumns());

		this.set('thresholdColumns', columns);
	},
	createThreshold : function(){
		this.open({
			mtype : 'ThresholdForm',
			mode : 1
		})
	},
	createThresholdIsEnabled$ : function(){
		return !this.wait;
	},

	copyThresholdIsEnabled$ : function(){
		return !this.wait && this.selectedThresholdId != null;
	},

	copyThreshold : function(){
		this.open({
			mtype : 'ThresholdForm',
			thresholdId : this.selectedThresholdId,
			mode : 2
		})
	},

	deleteThreshold : function(){
		this.message({			
			width : 450,
			title : this.localize('deleteTitle'),
			msg : this.localize('deleteMessage'),
			button : Ext.MessageBox.YESNO,
			buttonText : {
				yes : this.localize('ok'),
				no : this.localize('cancel')
			},
			fn : function(btn){
				if(btn == 'yes'){
					this.doDelete();
				}
			}
		})
	},
	doDelete : function(){
		var ids = [this.selectedThresholdId];
		this.set('wait', true);
		this.ajax({
			url : this.API['thresholdRuleDelete'],
			method : 'POST',
			params : {
				ids : ids
			},
			scope: this,
			success : function(resp){
				var respData = RS.common.parsePayload(resp);
				if (respData.success){
					this.deSelectThreshold();
					this.reloadThresholdTable();
					clientVM.displaySuccess(this.localize('deleteSucMsg'));
				}
			},
			failure : function(resp){
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},
	deleteThresholdIsEnabled$ : function(){
		return !this.wait && this.selectedThresholdId != null;
	},
	selectThreshold : function(record){
		this.set('selectedThresholdId', record.raw.sys_id);
		this.set('selectedThresholdModule', record.raw.uruleModule);
		this.set('selectedThresholdName',record.raw.uruleName);
		this.set('selectedRecord',record);
		this.updateThresholdInfo();
	},
	deSelectThreshold : function(){
		this.set('selectedThresholdId', null);
		this.set('selectedThresholdModule', null);
		this.set('selectedThresholdName',null);
		this.set('selectedRecord',null);
		this.thresholdTabs.tabs.forEach(function(tab){
			tab.set('thresholdSelected', false);
		})	
	},
	reSelectThreshold : function(){
		//Get the new record from the store using name and module of previous selected record.
		var records = this.thresholdStore.queryBy(function(record, id){
			return (record.get('uruleModule') == this.selectedThresholdModule && record.get('uruleName') == this.selectedThresholdName );
		}, this);
		this.set('selectedRecord',records.first());
		this.fireEvent('reSelectRecord');
	},
	reloadThresholdTable : function(updateThresholdFlag){
		this.set('thresholdUpdated', updateThresholdFlag == true ? true : false);
		this.thresholdStore.load();
	},
	updateThresholdInfo : function(){
		var id = this.selectedRecord.get('id');
		var ruleModule = this.selectedRecord.get('uruleModule');
		var ruleName = this.selectedRecord.get('uruleName');
		this.thresholdTabs.tabs.forEach(function(tab){
			tab.updateTabInfo(id, ruleModule, ruleName);
		})
	},
	mcpadmin : function() {
		this.open({
			mtype : 'RS.mcp.Admin',
			groupName: this.groupName,
			clusterList: this.clusterList
		})
	},
	mcpadminIsVisible$ : function() {
		if (this.isAdmin && this.clusterListValid) {
			return true;
		} else {
			return false;
		}
	}
})


glu.defModel('RS.mcp.Monitor',{
	hostInfoMap : {},
	hostTreeStore : {
		mtype: 'treestore',
        // fields: ['id','ustatus'],
        proxy: {
            type: 'ajax',
            url : '/resolve/service/mcp/getRegisteredInstances'
        },
        root: {
            name: 'All',
            expanded: false
        },
        autoLoad : false
	},
    view : function(){
        var root = this.hostTreeStore.getRootNode();
        var group = root.getChildAt(0);
        var child1 = group.getChildAt(0);
        var child2 = group.getChildAt(1);
        group.insertChild(0,child2);
    },
    statusPollingTask : null,
    pollingInterval : 10000,
	init : function(){
		var me = this;
        this.hostTreeStore.on('beforeappend', me.updateTree);
        //this.hostTreeStore.getRootNode().on('beforeexpand', me.updateTree);
	},
    activate : function(){
        this.hostTreeStore.getRootNode().expand();
        //this.startCheckingStatusTask();
    },
    updateTree : function(tree, node){
        var nodeData = node.raw;
        if(nodeData.hasOwnProperty('ugroupName')){
            node.set('iconCls', "group-level" );
            var grpName = nodeData.ugroupName;
            node.set('name', grpName);
        }
        else if(nodeData.hasOwnProperty('uclusterName')){
            node.set('iconCls', "cluster-level" );
            var clusterName = nodeData.uclusterName;
            node.set('name', clusterName);
        }
        else if(nodeData.hasOwnProperty('uhostName')){
            var hostId = node.get('id');
           // var hostPath = node.getPath();
            var hostName = nodeData.uhostName;
            var status = node.get('ustatus') || 'good';
            node.set('iconCls', "status-" + status );
            node.set('name', hostName);
            node.set('leaf', true);
            this.hostInfoMap[hostId] = {
                name : hostName,
                // path : hostPath,
                status : status
            }
        }
    },
    startCheckingStatusTask : function(){
        if(this.statusPollingTask){
            this.statusPollingTask.stop();
        }
        this.statusPollingTask = Ext.TaskManager.start({
            run : this.checkStatus,
            scope : this,
            interval : this.pollingInterval
        })
    },
	checkStatus : function(){
        this.ajax({
            url : 'getStatus',
            method :  'GET',
            success : function(resp){
                var respData = RS.common.parsePayload(resp);
                this.updateHostStatus(respData);
            },
			failure : function(resp){
				clientVM.displayFailure(resp);
			}
        })
	},
    updateHostStatus : function(hostStatus){
        for(var i = 0; i < hostStatus.length; i++){
            var host = hostStatus[i];
            var hostId = host.id;
            this.hostInfoMap[hostId].status = host.status;
        }
        var root = this.hostTreeStore.getRootNode();
        root.cascadeBy(function(node){
            // console.log(node.get('name'));
            if(node.isLeaf()){
                var hostStatus =  this.hostInfoMap[node.get('id')].status;
                node.set('iconCls', "status-" + hostStatus );
                // console.log(hostStatus);
            }
        },this)
    }
});
glu.defModel("RS.mcp.tabs.DeployedTab", {
	width : 180,
	title : '',
	disabled : false,
	thresholdSelected : false,
	init : function(){
		this.set('title', this.localize('deployedTabTitle'));
	},
	updateTabInfo : function(thresholdId){
	},
	deactivateTab : function(){
		this.set("disabled", true);
	},
	reactivateTab : function(){
		this.set("disabled", false);
	}
});
glu.defModel('RS.mcp.tabs.DeployTab',{
	title : '',
    width : 180,
	disabled : false,
	thresholdSelected : false,
	hostTreeStore : {
		mtype: 'treestore',
        fields: ['name','status', 'level'],
        proxy: {
            type: 'ajax',
            url : 'treelist'
        },
        root: {
        	expanded: false,
            name: 'All'
        },
        autoLoad : false
	},
	init : function(){
		this.set('title', this.localize('deployTabTitle'));
		this.hostTreeStore.on('beforeappend', this.updateTree, this);
	},
	 updateTree : function(tree, node){
        if(node.get('level') == "group"){
            node.set('iconCls', "group-level" );
        }
        else if(node.get('level') == "cluster"){
            node.set('iconCls', "cluster-level" );
            node.set('checked', false);
        }
        else if(node.isLeaf()){
            var hostId = node.get('id');
           // var hostPath = node.getPath();
            var hostName = node.get('name');
            var status = node.get('status') || 'good';
            node.set('iconCls', "no-icon" );
            node.set('checked', false);
        }
    },
	updateTabInfo : function(thresholdId){
	},
	deactivateTab : function(){
		this.set("disabled", true);
	},
	reactivateTab : function(){
		this.set("disabled", false);
	}
});

glu.defModel('RS.mcp.tabs.DetailTab',{
	title : '',	
	width : 180,
	addIsDisabled : true,
	wait : false,
	thresholdSelected : false,
	fields : ['id',
	{
		name : 'UVersion',
		mapping : 'uversion'
	},{
		name : 'UActive',
		mapping : 'uactive'
	},{
		name : 'URuleModule', 
		mapping : 'uruleModule'
	},{
		name : 'URuleName', 
		mapping : 'uruleName'
	},{
		name : 'UAlertStatus',
		mapping : 'ualertStatus'
	},{ 
		name : 'UGroup',
		mapping : 'ugroup'
	},{
		name : 'UName',
		mapping : 'uname'
	},{
		name :'USource',
		mapping : 'usource'
	},{
		name : 'UType',
		mapping : 'utype',
		convert : function(value, record){
			return value == "default" ? true : false;
		}
	},{
		name : 'UActive',
		mapping : 'uactive',
		type : 'bool'
	}],
	//Action field
	email : false,
	http : false,
	snmp : false,
	database : false,

	severityStore : RS.mcp.MetaConfigMap.severityStore,
	metricNameStore : RS.mcp.MetaConfigMap.metricNameStore,
	metricGroupStore : RS.mcp.MetaConfigMap.metricGroupStore,
	metricMetaData : RS.mcp.MetaConfigMap.metricMetaData,
	conditionColumns : null,
	conditionStore : {
		mtype : 'store',
		proxy : {
			type : 'memory'
		},
		fields : ['id','operator', 'value']
	},
	conditionValue : '',
	selectedOperator : '=',
	operatorStore : RS.mcp.MetaConfigMap.operatorStore,
	
	versionStore : {
		mtype : 'store',
		proxy : {
			type : 'memory'
		},
		fields : ['display','version']
	},
	versionLimit : 5,
	selectedVersion : null,
	//API
	API : {
		thresholdRuleGetAPI : '/resolve/service/metric/get',
		thresholdRuleGetRevisionAPI : '/resolve/service/metric/getRevision',
		thresholdRuleSaveVersionAPI : '/resolve/service/metric/saveversion',
		thresholdRuleForVersionAPI : '/resolve/service/metric/getbynameversion',
	},	
	suppressVersionChangedEvt : false,
	when_selected_version_changes : {
		on: ['selectedVersionChanged'],
		action : function(){
			if(this.suppressVersionChangedEvt){
				this.set('suppressVersionChangedEvt', false);
				return;
			}
			if(this.selectedVersion != null){
				this.getRuleForVersion(this.selectedVersion);
			}
		}
	},
	when_metric_group_change : {
		on : ['UGroupChanged'],
		action : function(){
			this.metricNameStore.removeAll();
			this.metricNameStore.add(this.metricMetaData[this.UGroup]);
			//Select first name in the group by default
			this.set('UName', this.metricNameStore.getAt(0).data.UName);
		}
	},
	init : function(){
		this.buildConditionGrid();
		this.set('title', this.localize('detailTabTitle'));	
	},
	editIsEnabled$: function(){
		return !this.wait && ( this.selectedVersion == 0);
	},
	saveVersionIsEnabled$ : function(){
		return !this.wait;
	},	
	buildConditionGrid : function(){
		this.set('conditionColumns',[{
			dataIndex : 'operator',
			header : 'Operator',
			flex : 1
		},{
			dataIndex : 'value',
			header : 'Value',
			flex : 1,
			sortable : true
		}]);
	},
	resetData :function(){		
		this.set('selectedVersion', null);
		this.versionStore.removeAll();
		this.conditionStore.removeAll();
	},
	updateTabInfo : function(thresholdId, thresholdModule, thresholdName){
		var me = this;
		me.resetData();
		me.ajax({
			url : this.API['thresholdRuleGetRevisionAPI'],
			method : 'POST',
			params : {
				module : thresholdModule,
				name : thresholdName				
			},
			success : function(resp){
				var respData = RS.common.parsePayload(resp);
				if (respData.success){
					me.populateForm(respData.data);
					var limit = me.versionLimit;
					var currentVersion = respData.total;
					//Add version 0 (which is the revision)
					me.versionStore.add({
						display : "Latest Revision",
						version : me.UVersion
					})
					while(currentVersion > 0 && limit > 0){
						me.versionStore.add({
							display : currentVersion,
							version : currentVersion,
						})
						currentVersion--;
						limit--;
					}

					//Select latest version as default
					me.set('suppressVersionChangedEvt', true);
					me.set('selectedVersion', me.UVersion);
					if(!me.thresholdSelected){
						me.set('thresholdSelected', true);
					}
				}
			},
			failure : function(resp){
				clientVM.displayFailure(resp);
			},
			callback: function() {
				me.set('wait', false);
			}
		})
	},
	getRuleForVersion : function(version){
		var me = this;
		var params = {
			module : this.URuleModule,
			name : this.URuleName,
			version : version
		}
		this.ajax({
			url : this.API['thresholdRuleForVersionAPI'],
			method : 'POST',
			params : params,
			success : function(resp){
				var respData = RS.common.parsePayload(resp);
				if (respData.success){
					me.populateForm(respData.data);
				}
			},
			failure : function(resp){
				clientVM.displayFailure(resp);
			}
		})
	},
	populateForm : function(data){
		this.loadData(data);
		var actions = data.uaction ? data.uaction.split(',') : [];
		this.set('http', Ext.Array.contains(actions, 'http'));	
		this.set('snmp', Ext.Array.contains(actions, 'snmp'));
		this.set('email', Ext.Array.contains(actions, 'email'));
		this.set('database', Ext.Array.contains(actions, 'database'));		
		var operators = data.uconditionOperator.split(',');
		var conditionValues = data.uconditionValue.split(',');
		this.conditionStore.removeAll();
		for(var i = 0; i < operators.length; i++){
			this.conditionStore.add({
				operator : operators[i],
				value : conditionValues[i]
			})
		}
		this.set('isDirty', false);
	},

	edit : function(){
		this.open({
			mtype : 'RS.mcp.ThresholdForm',
			mode : 3,
			thresholdId : this.id
		})
	},
	saveVersion : function(){
		this.message({
			title : this.localize('saveVersionTitle'),
			msg : this.localize('saveVersionMsg'),
			button : Ext.MessageBox.YESNO,
			buttonText : {
				yes : this.localize('confirm'),
				no : this.localize('cancel')
			},
			fn : function(btn){
				if(btn == 'yes'){
					this.doSaveVersion();
				}
			}
		})
	},
	doSaveVersion : function(){
		this.set('wait', true);
		var me = this;
		var payload = this.asObject();

		//Check for default type
		payload.UType = this.UType ? "default" : "";

		//Add actions
		var actions = [];
		if(this.http)
			actions.push('http');
		if(this.snmp)
			actions.push('snmp');
		if(this.email)
			actions.push('email');
		if(this.database)
			actions.push('database');
		payload.UAction = actions.join(",");

		//Add condtions
		var conditionValues = [];
		var conditionOperators = [];
		this.conditionStore.each(function(item){
			conditionOperators.push(item.data.operator);
			conditionValues.push(item.data.value);
		})
		payload.UConditionOperator = conditionOperators.join(',');
		payload.UConditionValue = conditionValues.join(',');

		this.ajax({
			url : this.API['thresholdRuleSaveVersionAPI'],
			jsonData : payload,
			scope: this,
			success : function(r){
				var resp = RS.common.parsePayload(r);
				if(resp.success){
					clientVM.displaySuccess(me.localize('saveSucMsg'));
					var updateThreshold = true;
					me.rootVM.reloadThresholdTable(updateThreshold );
				}
				else{
					clientVM.displayError(me.localize('saveErrMsg') + '[' + resp.message + ']');
				}
			},
			failure : function(r){
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	}
});
glu.defModel('RS.mcp.tabs.HistoryTab',{
	title : '',
	width : 180,
	disabled : false,
	thresholdSelected : false,
	init : function(){
		this.set('title', this.localize('historyTabTitle'));
	},
	updateTabInfo : function(thresholdId){
	},
	deactivateTab : function(){
		this.set("disabled", true);
	},
	reactivateTab : function(){
		this.set("disabled", false);
	}
});

glu.defModel("RS.mcp.tabs.NotDeployedTab", {
	title : '',
	width : 180,
	disabled : false,
	thresholdSelected : false,
	init : function(){
		this.set('title', this.localize('notDeployedTabTitle'));
	},
	updateTabInfo : function(thresholdId){
	},
	deactivateTab : function(){
		this.set("disabled", true);
	},
	reactivateTab : function(){
		this.set("disabled", false);
	}
});
glu.defModel("RS.mcp.tabs.ThresholdTab", {
	tabs : {
		mtype :'activatorlist',
		focusProperty : 'selectedTab',
		autoParent: true
	},
	selectedTab :'',
	init : function(){
		var detailTab = this.model({
			mtype : 'DetailTab',
			rootVM : this.parentVM
		})
		this.tabs.add(detailTab);

		// var deployTab = this.model({
		// 	mtype : 'DeployTab'
		// })
		// this.tabs.add(deployTab);

		// var deployedTab = this.model({
		// 	mtype : 'DeployedTab'
		// })
		// this.tabs.add(deployedTab);

		// var notDeployedTab = this.model({
		// 	mtype : 'NotDeployedTab'
		// })
		// this.tabs.add(notDeployedTab);		

		// var historyTab = this.model({
		// 	mtype : 'HistoryTab'
		// })
		// this.tabs.add(historyTab);		

		Ext.defer(function() {
			this.set('selectedTab', detailTab );
		}, 250, this)
	},
	
	// deactivateTab : function(){
	// 	this.tabs.foreach(function (tab){
	// 		if(tab.viewmodelName != "DetailTab")
	// 			tab.deactivateTab();
	// 	})
	// },

	// reactivateTab : function(){
	// 	this.tabs.foreach(function (tab){
	// 		if(tab.viewmodelName != "DetailTab")
	// 			tab.reactivateTab();
	// 	})
	// }
})
glu.defModel("RS.mcp.ThresholdForm",{
	wait : false,
	title : '',	
	addIsDisabled : true,
	selectedConditionIndex : null,
	conditionIsEmpty : false,
	submitBtnTxt : "",
	//Action field
	email : false,
	http : false,
	snmp : false,
	database : false,
	fields : ['id',
	{
		name : 'UVersion',
		mapping : 'uversion'
	},{
		name : 'UActive',
		mapping : 'uactive'
	},{
		name : 'URuleModule', 
		mapping : 'uruleModule'
	},{
		name : 'URuleName', 
		mapping : 'uruleName'
	},{
		name : 'UAlertStatus',
		mapping : 'ualertStatus'
	},{ 
		name : 'UGroup',
		mapping : 'ugroup'
	},{
		name : 'UName',
		mapping : 'uname'
	},{
		name :'USource',
		mapping : 'usource'
	},{
		name : 'UType',
		mapping : 'utype',
		convert : function(value, record){
			return value == "default" ? true : false;
		}
	},{
		name : 'UActive',
		mapping : 'uactive',
		type : 'bool'
	}],
	severityStore : RS.mcp.MetaConfigMap.severityStore,
	metricNameStore : RS.mcp.MetaConfigMap.metricNameStore,
	metricGroupStore : RS.mcp.MetaConfigMap.metricGroupStore,
	metricMetaData : RS.mcp.MetaConfigMap.metricMetaData,	
	conditionStore : {
		mtype : 'store',
		proxy : {
			type : 'memory'
		},
		fields : ['id','operator', 'value']
	},
	selectedOperator : '=',
	conditionValue : '',
	operatorStore : RS.mcp.MetaConfigMap.operatorStore,
	defaultData : {
		uruleModule : '',
		uruleName : '',
		utype : "default",
		uactive : true,
		ualertStatus : 'warning',
		ugroup : 'jvm',
		uname : 'thread_count',
		usource : '',
		uversion : 0
	},
	modeMap : {
		'CREATE' : 1,
		'COPY' : 2,
		'EDIT' : 3
	},
	mode : 1,
	//Reactors
	when_metric_group_change : {
		on : ['UGroupChanged'],
		action : function(){
			this.metricNameStore.removeAll();
			this.metricNameStore.add(this.metricMetaData[this.UGroup]);
			//Select first name in the group by default
			this.set('UName', this.metricNameStore.getAt(0).data.UName );
		}
	},
	//API
	API : {
		thresholdRuleGetAPI : '/resolve/service/metric/get',
		thresholdRuleSaveAPI : '/resolve/service/metric/save',
		thresholdRuleCopyAPI : '/resolve/service/metric/copy',
		thresholdRuleSaveVersionAPI : '/resolve/service/metric/saveversion'
	},	
	init : function(){
		this.set('submitBtnTxt', this.localize('create'));
		if(this.mode == this.modeMap['COPY']){
			this.getThresholdRule(this.thresholdId);
			this.set('title', this.localize('copyThresholdTitle'));
		}
		else if(this.mode == this.modeMap['EDIT']){
			this.getThresholdRule(this.thresholdId);
			this.set('title', this.localize('editThresholdTitle'));
			this.set('submitBtnTxt', this.localize('save'));
		}
		else{
			this.set('title', this.localize('createThresholdTitle'));
			this.loadData(this.defaultData);
			this.set('http', true);
			this.set('conditionIsEmpty', true);
			this.set('isPristine', true);
		}
	},
	//Validation
	URuleModuleIsValid$ : function(){
		return (this.URuleModule && this.validateInput(this.URuleModule)) ? true : this.localize('invalidModule');
	},

	URuleNameIsValid$ : function(){
		return (this.URuleName && this.validateInput(this.URuleName)) ? true : this.localize('invalidName');
	},	
	conditionIsValid$ : function(){
		return !this.conditionIsEmpty;
	},
	validateInput: function(input) {
		return (/^[a-zA-Z0-9_\-]+$/.test(input) || /^[a-zA-Z0-9_\-][a-zA-Z0-9_\-\s]*[a-zA-Z0-9_\-]$/.test(input)) && !/.*\s{2,}.*/.test(input) && /^[a-zA-Z0-9_\-]{1,40}$/.test(input.replace(/\s/g, '')) ;
	},
	actionGroup : null,
	actionGrouptIsValid$ : function(){
		return (this.http || this.snmp || this.email || this.database) ? true : ""
	},
	editMode$ : function(){
		return this.mode == this.modeMap['EDIT'];
	},
	getThresholdRule : function(thresholdId){
		this.set('wait', true);
		var me = this;
		this.ajax({
			url : this.API['thresholdRuleGetAPI'],
			method : 'POST',
			params : { 
				id : thresholdId 
			},
			scope: this,
			success : function(resp){
				var respData = RS.common.parsePayload(resp);
				if (respData.success){
					this.populateForm(respData.data);
				}
			},
			failure : function(resp){
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},
	populateForm : function(data){	
		this.loadData(data);
		var actions = data.uaction ? data.uaction.split(',') : [];
		for(var i = 0; i < actions.length; i++){
			this.set(actions[i], true);
		}
		var operators = data.uconditionOperator.split(',');
		var conditionValues = data.uconditionValue.split(',');
		//this.conditionStore.removeAll();
		for(var i = 0; i < operators.length; i++){
			this.conditionStore.add({
				operator : operators[i],
				value : conditionValues[i]
			})
		}
		
		this.set('isDirty', false);
		this.set('isPristine', false);
		if(this.mode == this.modeMap['COPY']){
			this.set("URuleName", this.URuleName + " - copy");
		}
	},
	addCondition : function(){
		this.conditionStore.add({operator : this.selectedOperator, value : this.conditionValue});
		this.clearConditionInput();
		this.set('conditionIsEmpty', false );
		this.set('conditionErrorIsHidden', true);		
	},
	clearConditionInput : function (){
		this.set('conditionValue', null);
		this.set('addIsDisabled', true);
	},	
	deleteCondition : function(record){
		this.conditionStore.remove(record);
		this.set('selectedConditionIndex', null);
		if(this.conditionStore.data.length == 0){
			this.set('conditionIsEmpty', true );
			this.set('conditionErrorIsHidden', false);
		}
	},
	deleteIsDisabled$ : function(){
		if(this.selectedConditionIndex != null)
			return false;
		else
			return true;
	},
	conditionSelect : function(record, item, index){
		this.set('selectedConditionIndex', index);
	},

	submitForm : function(btn,saveNewVersion){
		if(!this.isValid){
			this.set('isPristine', false);
			this.set('conditionErrorIsHidden', this.conditionIsValid ? true : false);
			this.enableAllButton(false);
			return;
		}
		var me = this;
		var payload = this.asObject();
		var api = null;
		var msg = null;
		if(me.mode == me.modeMap['CREATE']){
			api = me.API['thresholdRuleSaveAPI'];
			sucMsg = me.localize('createSucMsg');
			errorMsg = me.localize('createErrMsg');
		}
		else if (me.mode == me.modeMap['COPY']){
			api = me.API['thresholdRuleCopyAPI'];
			sucMsg = me.localize('copySucMsg');
			errorMsg = me.localize('copyErrMsg');		
		}
		else {
			api = me.API['thresholdRuleSaveAPI'];
			sucMsg = me.localize('saveSucMsg');
			errorMsg = me.localize('saveErrMsg');
		}
		//Check for default type
		payload.UType = this.UType ? "default" : "";

		//Add actions
		var actions = [];
		if(this.http)
			actions.push('http');
		if(this.snmp)
			actions.push('snmp');
		if(this.email)
			actions.push('email');
		if(this.database)
			actions.push('database');
		payload.UAction = actions.join(",");

		//Add condtions
		var conditionValues = [];
		var conditionOperators = [];
		me.conditionStore.each(function(item){
			conditionOperators.push(item.data.operator);
			conditionValues.push(item.data.value);
		})
		payload.UConditionOperator = conditionOperators.join(',');
		payload.UConditionValue = conditionValues.join(',');
		me.ajax({
			url : api,
			jsonData : payload,
			success : function(r){
				var resp = RS.common.parsePayload(r);
				me.rootVM.set('wait', false);
				if(saveNewVersion){
					me.saveVersion(resp.data);
				}
				else {
					//Reload threshold detail if this is edit mode.
					var updateThreshold = false;
					if(me.mode == me.modeMap['EDIT']){
						updateThreshold = true;
					}
					me.rootVM.reloadThresholdTable(updateThreshold);
					
					if(resp.success)
						clientVM.displaySuccess(sucMsg);
					else
						clientVM.displayError(errorMsg + '[' + resp.message + ']');
					this.doClose();
				}
			
			}
		})		
	},
	createWithVersion : function(){
		if(!this.isValid){
			this.set('isPristine', false);
			this.set('conditionErrorIsHidden', this.conditionIsValid ? true : false);
			this.enableAllButton(false);
			return;
		}
		var saveNewVersion = true;
		this.submitForm(null,saveNewVersion);
	},		
	saveVersion : function(thresholdRecord){		
		//Extra step cuz backend refuse to use the same fields for response and request parameters !!!
		var payload = {
			UAction: thresholdRecord['uaction'],
			UActive: thresholdRecord['uactive'],
			UAlertStatus: thresholdRecord['ualertStatus'],
			UConditionOperator: thresholdRecord['uconditionOperator'],
			UConditionValue: thresholdRecord['uconditionValue'],
			UGroup: thresholdRecord['ugroup'],
			UName: thresholdRecord['uname'],
			URuleModule: thresholdRecord['uruleModule'],
			URuleName: thresholdRecord['uruleName'],
			USource: thresholdRecord['usource'],
			UType: thresholdRecord['utype'],
			UVersion: 0,
			id: thresholdRecord['id'],
		}
		var me = this;		
		this.ajax({
			url : this.API['thresholdRuleSaveVersionAPI'],
			jsonData : payload,
			success : function(r){			
				var resp = RS.common.parsePayload(r);
				if(resp.success){
					clientVM.displaySuccess(me.localize('saveSucMsg'));			
					me.rootVM.reloadThresholdTable();
				}
				else{
					clientVM.displayError(me.localize('saveErrMsg') + '[' + resp.message + ']');
				}
				this.doClose();
			},
			failure : function(r){
				clientVM.displayFailure(r);
			}
		})
	},
	createWithVersionIsEnabled : true,
	submitFormIsEnabled : true,
	on_form_is_valid : {
		on : ['isValidChanged'],
		action : function(){
			if(!this.isPristine){
				this.enableAllButton(this.isValid);
				this.set('conditionErrorIsHidden', this.conditionIsValid ? true : false);
			}
		}
	},
	conditionErrorIsHidden : true,
	enableAllButton : function(flag){
		this.set('createWithVersionIsEnabled',flag);
		this.set('submitFormIsEnabled',flag);		
	},
	createWithVersionIsVisible$ : function(){
		return this.mode != this.modeMap['EDIT'];
	},
	infoIsVisible$ : function(){
		return this.mode != this.modeMap['EDIT'];
	},
	cancel : function(){
		this.doClose();
	}
})

glu.defView("RS.mcp.Admin",{
	xtype : 'panel',
	title : '@{title}',
	layout : {
		align : 'stretch',
		type : 'vbox'
	},
	width : 550,
	padding : 20,
	defaults : {
		margin : '0 0 10 0',
		labelWidth : 150,
		msgTarget: 'side',
		labelStyle : "font-weight : 400;"
	},
	items: [
	/* TBD
	{
		xtype :'textfield',
		name : 'groupName',
	},
	*/
	{
		xtype: 'combobox',
		name: 'clusterName',
		editable: false,
		displayField: 'name',
		valueField: 'name',
		queryMode: 'local',
		store: '@{clusterStore}',
		forceSelection: true
	}, {
		id: 'mcp_Pas_sword',
		xtype : 'textfield',
		name : 'mcp_Pas_sword',
		inputType: 'password',
		selectOnFocus: true,
		allowBlank: false
	},{
		xtype : 'textfield',
		name : 'mcpPasswordConfirm',
		inputType: 'password',
		selectOnFocus: true,
		allowBlank: false
	}],
	dockedItems : [{
		xtype : 'toolbar',
		dock : 'bottom',
		ui: 'footer',
		margin : '10 0 0 0',
		items: [{
			name: 'submit',
			text: '@{submitBtn}',
	        width : 80,
	    },{
	        name: 'cancel',
	        width : 80
	    }]
	}],
	buttonAlign: 'left',
  	modal: true
})

glu.defView("RS.mcp.Main", {
	bodyPadding : 15,
	autoScroll : true,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	items : [
	{
		xtype : 'grid',
		displayName : '@{displayName}',
		cls :'rs-grid-dark',
		overflowY : 'auto',
		flex : 2,		
		plugins : [
		{
			ptype : 'searchfilter',
			allowPersistFilter : false,
			hideMenu : true
		},{
			ptype: 'pager'
		},{
			ptype: 'columnautowidth'
		}],
		dockedItems : [
		{
			xtype : 'toolbar',		
			dock : 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
		 	items : ['createThreshold','copyThreshold','deleteThreshold','mcpadmin']
		}],
		store : '@{thresholdStore}',
		columns : '@{thresholdColumns}',
		listeners : {			
			select : '@{selectThreshold}',
			render : function(){
				var grid = this;
				this._vm.on('reSelectRecord', function(){
					//Manually reselect to hightlight the row.
					var selectedRecord = this.selectedRecord;
					var selectionModel = grid.getSelectionModel();
					if(selectionModel.isSelected(selectedRecord)){
						//reselect to update the detail tab.
						this.selectThreshold(selectedRecord);
					}
					else
						selectionModel.select(selectedRecord);
				})
			}
		}
	},{
		xtype : '@{thresholdTabs}',
		style : 'border-top :1px solid #cccccc;',
		margin : '10 0 0 0',
		padding : '10 0 0 0',
		flex : 3
	}]
})
Ext.define('Ext.tree.plugin.ResolveTreeViewDropDrag', {
    extend: 'Ext.AbstractPlugin',
    alias: 'plugin.resolvetreeviewdragdrop',

    uses: [
        'Ext.tree.ViewDragZone',
        'Ext.tree.ViewDropZone'
    ],
    b4StartDrag: function(x, y) {
        // show the drag frame
        if(this.tooltip)
        	this.showFrame(x, y);
    },
    tooltip : true,
    indicatorCls: 'x-tree-ddindicator',
    dragText : '{0} selected node{1}',
    allowParentInserts: false,
    allowContainerDrops: false,
    appendOnly: false,
    ddGroup : "TreeDD",
    containerScroll: false,
    expandDelay : 5000,
    enableDrop: true,
    enableDrag: true,
    nodeHighlightColor: 'c3daf9',
    nodeHighlightOnDrop: Ext.enableFx,
    nodeHighlightOnRepair: Ext.enableFx,
    displayField: 'text',
    init : function(view) {
        view.on('render', this.onViewRender, this, {single: true});
    },

    destroy: function() {
        Ext.destroy(this.dragZone, this.dropZone);
    },
	onViewRender : function(view) {
        var me = this,
            scrollEl;

        if (me.enableDrag) {
            if (me.containerScroll) {
                scrollEl = view.getEl();
            }
            me.dragZone = new Ext.tree.ViewDragZone({
                view: view,
                ddGroup: me.dragGroup || me.ddGroup,
                dragText: me.dragText,
                displayField: me.displayField,
                repairHighlightColor: me.nodeHighlightColor,
                repairHighlight: me.nodeHighlightOnRepair,
                scrollEl: scrollEl,
                animRepair : true,
                b4StartDrag : me. b4StartDrag
            });
        }

        if (me.enableDrop) {
            me.dropZone = new Ext.tree.ViewDropZone({
                view: view,
                ddGroup: me.dropGroup || me.ddGroup,
                allowContainerDrops: me.allowContainerDrops,
                appendOnly: me.appendOnly,
                allowParentInserts: me.allowParentInserts,
                expandDelay: me.expandDelay,
                dropHighlightColor: me.nodeHighlightColor,
                dropHighlight: me.nodeHighlightOnDrop,
                sortOnDrop: me.sortOnDrop,
                containerScroll: me.containerScroll,
                indicatorCls :  me.indicatorCls
            });
        }
    }
}, function(){
    var proto = this.prototype;
    proto.nodeHighlightOnDrop = proto.nodeHighlightOnRepair = Ext.enableFx;
});



glu.defView('RS.mcp.Monitor', {
	title : '~~monitorTitle~~',
	layout : 'border',
	items : [
	{
		xtype: 'treepanel',
		region : 'west',
		split : true,
		width : 400,
		name : 'hostTreeStore',
		id : 'NAVIGATIONTREE',
		useArrows : true,
		rootVisible : false,
		// dockedItems : [{
		// 	xtype : 'toolbar',
		// 	dock : 'top',
		// 	items : ['view']
		// }],
		viewConfig: {
		    plugins:{	
		    	ptype: 'resolvetreeviewdragdrop',
		    	dragText : 'Insert here.',
		    	indicatorCls: 'dd-custom-indicator',
		    	tooltip : false
		    },
		    listeners : {
	    		beforedrop : function(item, data, node, dropPosition, dropHandlers, eOpts){
	    			//Allow drop and drag on same level only
	    			var prevParent = data.records[0].parentNode.get('name');
	    			var nextParent = node.parentNode.get('name');
	    			if(prevParent !== nextParent || dropPosition == "append")
	    				return false;
	    		}
	    	}
		},
		columns: {
			xtype : 'treecolumn',
			dataIndex : 'name',
			text : 'Instances',
			flex : 1
		}
	},{
		xtype : 'panel',
		region : 'center',
		title : 'Testing'
	}]
})
glu.defView("RS.mcp.tabs.DeployedTab", {
	title : '@{title}',
	disabled : '@{disabled}'
});
glu.defView('RS.mcp.tabs.DeployTab',{
	xtype : 'panel',
	title : '@{title}',
	disabled : '@{disabled}',
	autoScroll : true,
	layout : 'vbox',
	id : 'NAVIGATIONTREE',
	items : [
	{
		xtype : 'treepanel',
		title : 'Cluster',
		width : 400,
		useArrows : true,
		rootVisible : false,
		store : '@{hostTreeStore}',
		columns: {
			xtype : 'treecolumn',
			dataIndex : 'name',
			text : 'Instances',
			flex : 1
		}
	}]
});
glu.defView('RS.mcp.tabs.DetailTab',{
	title : '@{title}',
	autoScroll : true,
	padding : '10 0 0 0',
	items : [
	{
		html : '<h2 class="thresholdText" >Please select a threshold rule above.<h2>',
		hidden : '@{thresholdSelected}',
		layout : 'fit'
	},{
		xtype : 'panel',
		width : 600,
		flex : 1,
		hidden : '@{!thresholdSelected}',
		dockedItems : [
		{
			xtype : 'toolbar',
			dock : 'top',
			cls :' rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				minWidth : 60,
			},
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
		 	items : ['edit','saveVersion' ]		 	
		}],
		layout : {
			type : 'vbox',
			align : 'stretch'
		},
		defaults :{		
			labelWidth : 150,
			margin : '4 0',
			msgTarget: 'side',
			readOnly : true
		},
		defaultType: 'displayfield',		
		items : [
		{
			name : 'URuleModule',
			fieldLabel : 'Module'
		},{
			fieldLabel : 'Name',
			name : 'URuleName'
		},{
			xtype : 'combobox',
			fieldLabel : 'Version',
			store : '@{versionStore}',
			value : '@{selectedVersion}',
			queryMode : 'local',
			editable : false,
			readOnly : false,
			displayField : 'display',
			valueField : 'version'
		},{
			xtype : 'checkboxgroup',
			layout :{
				type : "hbox"
			},
			defaults : {
				readOnly : true
			},
			items : [
				{
					xtype : 'checkboxfield',
					boxLabel : 'Active',
					checked : '@{UActive}',
					margin : '0 20 0 0',
				},
				{
					xtype : 'checkboxfield',
					boxLabel : 'Auto Deploy',
					checked : '@{UType}',
					hidden : true
				}
			]
		},{
			xtype : 'combobox',
			value : '@{UAlertStatus}',
			fieldLabel : 'Severity',
			valueField : 'UAlertStatus',
			displayField : 'display',
			store : '@{severityStore}',
			queryMode : 'local',
			editable : false
		},{
			xtype :'checkboxgroup',
			fieldLabel : '~~notification~~',
			msgTarget: 'side',
			allowBlank : false,
			layout : 'hbox',
			defaults : {
				readOnly : true,
				margin : '0 15 0 5'
			},
			items : [
			{
				xtype : 'checkboxfield',
				boxLabel : 'HTTP',
				checked : '@{http}'
			},{
				xtype : 'checkboxfield',
				boxLabel : 'SNMP',
				checked : '@{snmp}'
			},{
				xtype : 'checkboxfield',
				boxLabel : 'Email',
				checked : '@{email}'
			},{
				xtype : 'checkboxfield',
				boxLabel : 'Write to DB',
				checked : '@{database}'
			}]
		},{
			xtype : 'combobox',
			value : '@{UGroup}',
			valueField : 'UGroup',
			fieldLabel : 'Metric Group',
			displayField : 'display',
			store : '@{metricGroupStore}',
			queryMode : 'local',
			readOnly : true
		},{
			xtype : 'combobox',
			value : '@{UName}',
			valueField : 'UName',
			fieldLabel : 'Metric Name',
			displayField : 'display',
			store : '@{metricNameStore}',
			queryMode : 'local',
			readOnly : true
		},{		
			fieldLabel : 'Source (optional)',
			name : 'USource'
		},{		
			fieldLabel : 'Condition',
		},{
			xtype : 'grid',
			cls : 'rs-grid-dark',
			store : '@{conditionStore}',
			allowDeselect : true,			
			columns : '@{conditionColumns}'
		}]	
	}],
	listeners : {
		render : function(){
			var me = this;
			me._vm.on('waitChanged', function(){
				me.setLoading(this.get('wait'), true);
			})
		}
	}
});
glu.defView("RS.mcp.tabs.HistoryTab", {
	title : '@{title}',
	disabled : '@{disabled}'
});
glu.defView('RS.mcp.tabs.NotDeployedTab',{
	title : '@{title}',
	disabled : '@{disabled}'
});
glu.defView("RS.mcp.tabs.ThresholdTab",{
	xtype: 'tabpanel',
	cls : 'rs-tabpanel-dark',
	items : '@{tabs}',
	activeTab : '@{selectedTab}',
	id : "thresholdTab"
})
glu.defView("RS.mcp.ThresholdForm",{	
	title : '@{title}',
	cls : 'hideReadOnlyBorder rs-modal-popup',
	layout : {
		align : 'stretch',
		type : 'vbox'
	},
	width : 600,
	padding : 15,
	defaults : {
		margin : '4 0',
		labelWidth : 150		
	},
	items : [
	{
		xtype :'textfield',
		name : 'URuleModule',
		fieldLabel : 'Module',
		emptyText : '~~moduleEmptyText~~',
		readOnly : '@{editMode}'
	},{
		xtype : 'textfield',
		fieldLabel : 'Name',
		name : 'URuleName',
		emptyText : '~~nameEmptyText~~',
		readOnly : '@{editMode}'
	},{
		xtype : 'checkboxgroup',
		layout : 'hbox',
		defaults : {
			margin : '0 10 0 0'
		},
		items : [
		{
			xtype : 'checkboxfield',
			boxLabel : 'Active',
			checked : '@{UActive}',
		},{
			xtype : 'checkboxfield',
			boxLabel : 'Auto Deploy',
			checked : '@{UType}',
			hidden : true
		}]			
	},{
		xtype : 'combobox',
		value : '@{UAlertStatus}',
		fieldLabel : 'Severity',
		valueField : 'UAlertStatus',
		displayField : 'display',
		store : '@{severityStore}',
		queryMode : 'local',
		editable : false
	},{
		xtype :'checkboxgroup',
		fieldLabel : 'Actions',
		msgTarget: 'side',
		allowBlank : false,
		name: 'actionGroup',
		layout : 'hbox',
		defaults : {
			margin : '0 10 0 0'
		},
		items : [
		{
			xtype : 'checkboxfield',
			boxLabel : 'HTTP',
			checked : '@{http}'
		},{
			xtype : 'checkboxfield',
			boxLabel : 'SNMP',
			checked : '@{snmp}'
		},{
			xtype : 'checkboxfield',
			boxLabel : 'Email',
			checked : '@{email}'
		},{
			xtype : 'checkboxfield',
			boxLabel : 'Write to DB',
			checked : '@{database}'
		}]
	},{
		xtype : 'combobox',
		value : '@{UGroup}',
		valueField : 'UGroup',
		fieldLabel : 'Metric Group',
		displayField : 'display',
		store : '@{metricGroupStore}',
		queryMode : 'local',
		editable : false,
		readOnly : '@{editMode}'
	},{
		xtype : 'combobox',
		value : '@{UName}',
		valueField : 'UName',
		fieldLabel : 'Metric Name',
		displayField : 'display',
		store : '@{metricNameStore}',
		queryMode : 'local',
		editable : false,
		readOnly : '@{editMode}'
	},{
		xtype : 'textfield',
		fieldLabel : 'Source (optional)',
		name : 'USource',
		emptyText : 'Source'
	},{
		xtype : 'fieldcontainer',
		layout :'hbox',
		fieldLabel : 'Conditions',
		items : [
		{
			xtype : 'combobox',
			displayField : 'operator',
			store : '@{operatorStore}',
			queryMode : 'local',
			margin : '0 5 0 0',
			flex : 2,
			editable : false,
			value : '@{selectedOperator}'
		},{
			xtype :'textfield',
			emptyText : 'value',
			margin : '0 5 0 0',
			flex : 3,
			value : '@{conditionValue}',
			validator : function(){
				var selectedOperator = this._vm.selectedOperator;
				if(this.value == ''){
					this._vm.set('addIsDisabled', true);
					return 'Value cannot be empty.'
				}
				else if(selectedOperator == 'contains' || (!isNaN(parseFloat(this.value)) && isFinite(this.value))){
					this._vm.set('addIsDisabled', false);
					return true;
				}
				else{
					this._vm.set('addIsDisabled', true);
					return 'Value most be numeric type.'
				}
			}
		},{
			xtype : 'button',
			cls : 'rs-small-btn rs-btn-dark',
			text : 'Add',
			margin : '0 5 0 0',
			disabled : '@{addIsDisabled}',			
			handler : '@{addCondition}'
		},{
			xtype : 'button',
			cls : 'rs-small-btn rs-btn-light',
			text : 'Clear',		
			handler : '@{clearConditionInput}'
		}]
	},{
		xtype : 'grid',
		cls : 'rs-grid-dark',
		store : '@{conditionStore}',
		columns : [
		{
			dataIndex : 'operator',
			header : 'Operator',
			flex : 1
		},{
			dataIndex : 'value',
			header : 'Value',
			flex : 1,
			sorttable : true
		},{
			xtype: 'actioncolumn',
			hideable: false,
			sortable: false,
			align: 'center',		
			width: 45,		
			items: [{
				glyph: 0xF146,
				tooltip : 'Remove',
				handler: function(view, rowIndex, colIndex, item, e, record) {
					view.ownerCt._vm.deleteCondition(record);				
				}		
			}]
		}],
		hidden : '@{conditionIsEmpty}',
		maxHeight : 250,		
		listeners : {
			select : '@{conditionSelect}',
			deleteAction : '@{deleteCondition}'
		}
	},{
		html : "~~infoText~~",
		hidden : '@{!infoIsVisible}'
	},{
		html : "~~emptyConditionText~~",
		hidden : '@{conditionErrorIsHidden}'
	}],
	buttons : [
	{
		cls : 'rs-med-btn rs-btn-dark',
		name: 'createWithVersion'
	},{
        text: '@{submitBtnTxt}',
        cls : 'rs-med-btn rs-btn-dark',       
        name : 'submitForm',	           
    },{      
        cls : 'rs-med-btn rs-btn-light',
        name : 'cancel'	       
    }],	
  	modal: true
})
/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.mcp').locale = {
	thresholdRuleDisplayName : 'Threshold Rules',
	createThreshold: 'New',
	createThresholdTitle : 'New Threshold Rule',
	copyThreshold: 'Copy',
	copyThresholdTitle : 'Copy Threshold Rule',
	editThresholdTitle : 'Edit Threshold Rule',
	deleteThreshold: 'Delete',
	deleteSucMsg: 'The threshold rule has been deleted successfully.',
	createSucMsg: 'The threshold has been created successfully.',
	saveSucMsg: 'The threshold has been saved successfully.',
	saveErrMsg: 'Cannot save the threshold.',
	copySucMsg: 'The threshold has been copied successfully.',
	copyErrMsg: 'Cannot copy the threshold.',
	createErrMsg: 'Cannot create the threshold.',
	version : 'Version',
	noVersion : "None (not deployable)",
	monitorTitle : 'Monitoring Center',
	
	windowTitleBlueprint: 'MCP - Blueprints',
	displayNameBlueprint: 'Blueprints',

	windowTitleComponent: 'MCP - Components',
	displayNameComponent: 'Components',

	windowTitleServer: 'MCP - Server Status',
	displayNameServer: 'Server Status',

	windowTitleSelectUpdate: 'MCP - Select Update',
	displayNameSelectUpdate: 'Select Update',

	windowTitleUpdates: 'MCP - Updates/Upgrades',
	displayNameUpdates: 'Update/Upgrades',

	windowTitleLogs: 'MCP - Log Files',
	displayNameLogs: 'View Log Files',

	windowTitleUploadUpdate: 'MCP - Upload',

	back: 'Back',
	save: 'Save',
	saveVersion : 'Save a Version',
	reset : 'Reset',
	cancel: 'Cancel',
	close: 'Close',
	deploy: 'Deploy',	
	confirm : 'Confirm',
	ok : 'OK',
	submit : 'Submit',
	create : 'Create',
	update : 'Update',

	id: 'ID',
	name: 'Name',
	serverIp: 'Server IP',
	guid: 'GUID',
	sshPort : 'SSH Port',
	remoteUser : 'Remote Username',
	remoteP_assword : 'Remote Password',
	resolveHome : 'Resolve Home',
	terminalPrompt : 'Terminal Prompt',
	privateKeyLocation : 'Private Key Location',
	tag: 'Tag',
	status: 'Status',
	description: 'Description',
	blueprint: 'Blueprint',
	worksheet: 'Worksheet',
	version: 'Version',
	size: 'Size',
	buildDate: 'Build Date',

	serverName: 'Server Name',
	componentName: 'Component',
	instanceName: 'Instance',
	status: 'Status',
	lastStatusUpdatedOn: 'Status Updated On',

	blueprintItemTitle: 'Blueprint',
	BlueprintSaved: 'Blueprint Saved',
	BlueprintDeployed: 'Blueprint Deployed',

	
	deployAction: 'Deploy',
	verifyAction: 'Verify',
	startAction: 'Start',
	stopAction: 'Stop',
	restartAction: 'Restart',
	statusAction: 'Status',

	addAction: 'Add',
	type: 'Type',
	date: 'Date',
	server: 'Server',

	deployUpdateAction: 'Deploy Update',

	deleteTitle: 'Confirm Delete',
	deleteMessage: 'Are you sure you want to delete the selected record?  This action cannot be undone.',
	deletesMessage: 'Are you sure you want to delete the {0} selected records?  This action cannot be undone.',

	deployTitle: 'Confirm Deploy',
	deployMessage: 'Are you sure you want to deploy the selected record?  This action cannot be undone.',
	deploysMessage: 'Are you sure you want to deploy the {0} selected records?  This action cannot be undone.',

	verifyTitle: 'Confirm Verify',
	verifyMessage: 'Are you sure you want to verify the selected record?  This will take a while.',
	verifysMessage: 'Are you sure you want to verify the {0} selected records?  This will take a while.',

	startTitle: 'Confirm Start',
	startMessage: 'Are you sure you want to start the selected servers?  This will take a while.',
	startsMessage: 'Are you sure you want to start the {0} selected servers?  This will take a while.',

	stopTitle: 'Confirm Stop',
	stopMessage: 'Are you sure you want to stop the selected servers?  This will take a while.',
	stopsMessage: 'Are you sure you want to stopt the {0} selected servers?  This will take a while.',

	restartTitle: 'Confirm Restart',
	restartMessage: 'Are you sure you want to restart the selected servers?  This will take a while.',
	restartsMessage: 'Are you sure you want to restart the {0} selected servers?  This will take a while.',

	statusTitle: 'Confirm Status',
	statusMessage: 'Are you sure you want to get status of the selected servers?  This will take a while.',
	statussMessage: 'Are you sure you want to get status of the {0} selected servers?  This will take a while.',

	logFiles: 'Log Files by Server',

	/* status message locale, keys are defined in MCPConstants.java and they have to be compliant */
	MCP_PING_PASSED: 'Server Ping Successful',
	MCP_PING_FAILED: 'Server Ping Failed',
	MCP_SERVER_CONFIG_FAILED: 'Server Config Failed',

    MCP_BLUEPRINT_SYNCHONIZED: 'Blueprint Synchronized',
    MCP_BLUEPRINT_OUT_OF_SYNC: 'Blueprint Out of Sync',
    MCP_BLUEPRINT_DEPLOY_FAILED: 'Blueprint Deploy Failed',
    MCP_BLUEPRINT_DEPLOYED: 'Blueprint Deployed',
    MCP_BLUEPRINT_NOT_DEPLOYED: 'Blueprint Not Deployed',
    MCP_BLUEPRINT_DEPLOYING: 'Deploying Blueprint',
    MCP_BLUEPRINT_VERIFYING: 'Vefirying Blueprint',
    MCP_BLUEPRINT_VERIFY_FAILED: 'Failed to Verify Blueprint',

	MCP_UPDATE_DEPLOYING: 'Deploying Update',
    MCP_UPDATE_DEPLOYED: 'Update Deployed',
    MCP_UPDATE_STOPPING: 'Stopping Components',
    MCP_UPDATE_CHECKSUM: 'Verifying Update Checksum',
    MCP_UPDATE_APPLYING: 'Applying Update',
    MCP_UPDATE_STARTING: 'Starting Components',
    MCP_UPDATE_IMPORTING: 'Importing Updated Libraries',
    MCP_UPDATE_FAILED: 'Update Failed',
    MCP_UPDATE_SUCCESS: 'Update Successful',

    //Validation
    invalidName: 'Name is required and must be in alphanumerics, but may contain single spaces or underscores \'_\'.',
    invalidModule: 'Module name is required and must be in alphanumerics, but may contain periods \'.\', single spaces, or underscores \'_\'.',

    //ThresholdForm
    moduleEmptyText : 'Package to which the rules shall belong.',
    nameEmptyText : 'Name of the threshold rule.',
    createWithVersion : 'Create with Version',
    infoText : '<i>* A rule cannot be deployed until a version has been created.</i>',
    emptyConditionText : '<i style="color:red;">** There must be at least 1 condition.</i>',

	//Administration
	mcpadmin: 'Settings',
	createMCPAdminPassword: 'Create Cluster password',
	modify_MCP_Ad_min_Pas_sword: 'Change Cluster password',
	groupName: 'Group Name',
	clusterName: 'Cluster Name',
	mcp_Pas_sword: 'Password',
	mcpPasswordConfirm: 'Confirm Password',

	passwordMinLengthInvalid: 'Password must be a minimum of 8 characters long',
	passwordNumberRequiredInvalid: 'Password must contain a number',
	passwordCapitalLetterRequiredInvalid: 'Password must contain a capital letter',
	passwordLowercaseLetterRequiredInvalid: 'Password must contain a lower case letter',
	passwordConfirmMismatchInvalid: 'Passwords do not match',

	ServerError: 'Server Error',
	GetClusterErr: 'Cluster not found.',
	SaveErr: 'Cannot update Cluster password.',
	SaveSuccess: 'Cluster password updated successfully.'
};

Ext.namespace('RS.mcp.tabs').locale = { 
    edit : 'Edit',
    back: 'Back',
	save: 'Save',
	saveVersion : 'Save a Version',
	reset : 'Reset',
	cancel: 'Cancel',
	confirm : 'Confirm',
	close: 'Close',
	deploy: 'Deploy',
	done : 'Done',
	addCondition  : 'Add',
	deleteCondition : 'Delete',
	version : 'Version',
	notification : 'Notification',
	detailTabTitle : 'Threshold Details',
	deployTabTitle : 'Deploy',
	deployedTabTitle : 'Already Deployed',
	notDeployedTabTitle : 'Not Deployed',
	historyTabTitle : 'Change History',
	unsavedTitle : "Unsaved Changes",
	resetThresholdTitle : "Reset Threshold Changes",

	saveSucMsg: 'The threshold has been saved successfully.',
	saveErrMsg: 'Cannot save the threshold.',
	saveVersionTitle : 'Save a version',
	saveVersionMsg : 'Do you to want save this revision as a new version?',
	invalidName: 'Name is required and must be in alphanumerics, but may contain single spaces or underscores \'_\'.',
    invalidModule: 'Module name is required and must be in alphanumerics, but may contain periods \'.\', single spaces, or underscores \'_\'.',
	invalidForm : 'Please fix your changes and try to save again. ',

	unsavedMsg : "There are unsaved changes. Save your changes now?",
	resetMsg : "Are you sure you want to reset your changes?"


}
