/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.mcp').locale = {
	thresholdRuleDisplayName : 'Threshold Rules',
	createThreshold: 'New',
	createThresholdTitle : 'New Threshold Rule',
	copyThreshold: 'Copy',
	copyThresholdTitle : 'Copy Threshold Rule',
	editThresholdTitle : 'Edit Threshold Rule',
	deleteThreshold: 'Delete',
	deleteSucMsg: 'The threshold rule has been deleted successfully.',
	createSucMsg: 'The threshold has been created successfully.',
	saveSucMsg: 'The threshold has been saved successfully.',
	saveErrMsg: 'Cannot save the threshold.',
	copySucMsg: 'The threshold has been copied successfully.',
	copyErrMsg: 'Cannot copy the threshold.',
	createErrMsg: 'Cannot create the threshold.',
	version : 'Version',
	noVersion : "None (not deployable)",
	monitorTitle : 'Monitoring Center',
	
	windowTitleBlueprint: 'MCP - Blueprints',
	displayNameBlueprint: 'Blueprints',

	windowTitleComponent: 'MCP - Components',
	displayNameComponent: 'Components',

	windowTitleServer: 'MCP - Server Status',
	displayNameServer: 'Server Status',

	windowTitleSelectUpdate: 'MCP - Select Update',
	displayNameSelectUpdate: 'Select Update',

	windowTitleUpdates: 'MCP - Updates/Upgrades',
	displayNameUpdates: 'Update/Upgrades',

	windowTitleLogs: 'MCP - Log Files',
	displayNameLogs: 'View Log Files',

	windowTitleUploadUpdate: 'MCP - Upload',

	back: 'Back',
	save: 'Save',
	saveVersion : 'Save a Version',
	reset : 'Reset',
	cancel: 'Cancel',
	close: 'Close',
	deploy: 'Deploy',	
	confirm : 'Confirm',
	ok : 'OK',
	submit : 'Submit',
	create : 'Create',
	update : 'Update',

	id: 'ID',
	name: 'Name',
	serverIp: 'Server IP',
	guid: 'GUID',
	sshPort : 'SSH Port',
	remoteUser : 'Remote Username',
	remoteP_assword : 'Remote Password',
	resolveHome : 'Resolve Home',
	terminalPrompt : 'Terminal Prompt',
	privateKeyLocation : 'Private Key Location',
	tag: 'Tag',
	status: 'Status',
	description: 'Description',
	blueprint: 'Blueprint',
	worksheet: 'Worksheet',
	version: 'Version',
	size: 'Size',
	buildDate: 'Build Date',

	serverName: 'Server Name',
	componentName: 'Component',
	instanceName: 'Instance',
	status: 'Status',
	lastStatusUpdatedOn: 'Status Updated On',

	blueprintItemTitle: 'Blueprint',
	BlueprintSaved: 'Blueprint Saved',
	BlueprintDeployed: 'Blueprint Deployed',

	
	deployAction: 'Deploy',
	verifyAction: 'Verify',
	startAction: 'Start',
	stopAction: 'Stop',
	restartAction: 'Restart',
	statusAction: 'Status',

	addAction: 'Add',
	type: 'Type',
	date: 'Date',
	server: 'Server',

	deployUpdateAction: 'Deploy Update',

	deleteTitle: 'Confirm Delete',
	deleteMessage: 'Are you sure you want to delete the selected record?  This action cannot be undone.',
	deletesMessage: 'Are you sure you want to delete the {0} selected records?  This action cannot be undone.',

	deployTitle: 'Confirm Deploy',
	deployMessage: 'Are you sure you want to deploy the selected record?  This action cannot be undone.',
	deploysMessage: 'Are you sure you want to deploy the {0} selected records?  This action cannot be undone.',

	verifyTitle: 'Confirm Verify',
	verifyMessage: 'Are you sure you want to verify the selected record?  This will take a while.',
	verifysMessage: 'Are you sure you want to verify the {0} selected records?  This will take a while.',

	startTitle: 'Confirm Start',
	startMessage: 'Are you sure you want to start the selected servers?  This will take a while.',
	startsMessage: 'Are you sure you want to start the {0} selected servers?  This will take a while.',

	stopTitle: 'Confirm Stop',
	stopMessage: 'Are you sure you want to stop the selected servers?  This will take a while.',
	stopsMessage: 'Are you sure you want to stopt the {0} selected servers?  This will take a while.',

	restartTitle: 'Confirm Restart',
	restartMessage: 'Are you sure you want to restart the selected servers?  This will take a while.',
	restartsMessage: 'Are you sure you want to restart the {0} selected servers?  This will take a while.',

	statusTitle: 'Confirm Status',
	statusMessage: 'Are you sure you want to get status of the selected servers?  This will take a while.',
	statussMessage: 'Are you sure you want to get status of the {0} selected servers?  This will take a while.',

	logFiles: 'Log Files by Server',

	/* status message locale, keys are defined in MCPConstants.java and they have to be compliant */
	MCP_PING_PASSED: 'Server Ping Successful',
	MCP_PING_FAILED: 'Server Ping Failed',
	MCP_SERVER_CONFIG_FAILED: 'Server Config Failed',

    MCP_BLUEPRINT_SYNCHONIZED: 'Blueprint Synchronized',
    MCP_BLUEPRINT_OUT_OF_SYNC: 'Blueprint Out of Sync',
    MCP_BLUEPRINT_DEPLOY_FAILED: 'Blueprint Deploy Failed',
    MCP_BLUEPRINT_DEPLOYED: 'Blueprint Deployed',
    MCP_BLUEPRINT_NOT_DEPLOYED: 'Blueprint Not Deployed',
    MCP_BLUEPRINT_DEPLOYING: 'Deploying Blueprint',
    MCP_BLUEPRINT_VERIFYING: 'Vefirying Blueprint',
    MCP_BLUEPRINT_VERIFY_FAILED: 'Failed to Verify Blueprint',

	MCP_UPDATE_DEPLOYING: 'Deploying Update',
    MCP_UPDATE_DEPLOYED: 'Update Deployed',
    MCP_UPDATE_STOPPING: 'Stopping Components',
    MCP_UPDATE_CHECKSUM: 'Verifying Update Checksum',
    MCP_UPDATE_APPLYING: 'Applying Update',
    MCP_UPDATE_STARTING: 'Starting Components',
    MCP_UPDATE_IMPORTING: 'Importing Updated Libraries',
    MCP_UPDATE_FAILED: 'Update Failed',
    MCP_UPDATE_SUCCESS: 'Update Successful',

    //Validation
    invalidName: 'Name is required and must be in alphanumerics, but may contain single spaces or underscores \'_\'.',
    invalidModule: 'Module name is required and must be in alphanumerics, but may contain periods \'.\', single spaces, or underscores \'_\'.',

    //ThresholdForm
    moduleEmptyText : 'Package to which the rules shall belong.',
    nameEmptyText : 'Name of the threshold rule.',
    createWithVersion : 'Create with Version',
    infoText : '<i>* A rule cannot be deployed until a version has been created.</i>',
    emptyConditionText : '<i style="color:red;">** There must be at least 1 condition.</i>',

	//Administration
	mcpadmin: 'Settings',
	createMCPAdminPassword: 'Create Cluster password',
	modify_MCP_Ad_min_Pas_sword: 'Change Cluster password',
	groupName: 'Group Name',
	clusterName: 'Cluster Name',
	mcp_Pas_sword: 'Password',
	mcpPasswordConfirm: 'Confirm Password',

	passwordMinLengthInvalid: 'Password must be a minimum of 8 characters long',
	passwordNumberRequiredInvalid: 'Password must contain a number',
	passwordCapitalLetterRequiredInvalid: 'Password must contain a capital letter',
	passwordLowercaseLetterRequiredInvalid: 'Password must contain a lower case letter',
	passwordConfirmMismatchInvalid: 'Passwords do not match',

	ServerError: 'Server Error',
	GetClusterErr: 'Cluster not found.',
	SaveErr: 'Cannot update Cluster password.',
	SaveSuccess: 'Cluster password updated successfully.'
};

Ext.namespace('RS.mcp.tabs').locale = { 
    edit : 'Edit',
    back: 'Back',
	save: 'Save',
	saveVersion : 'Save a Version',
	reset : 'Reset',
	cancel: 'Cancel',
	confirm : 'Confirm',
	close: 'Close',
	deploy: 'Deploy',
	done : 'Done',
	addCondition  : 'Add',
	deleteCondition : 'Delete',
	version : 'Version',
	notification : 'Notification',
	detailTabTitle : 'Threshold Details',
	deployTabTitle : 'Deploy',
	deployedTabTitle : 'Already Deployed',
	notDeployedTabTitle : 'Not Deployed',
	historyTabTitle : 'Change History',
	unsavedTitle : "Unsaved Changes",
	resetThresholdTitle : "Reset Threshold Changes",

	saveSucMsg: 'The threshold has been saved successfully.',
	saveErrMsg: 'Cannot save the threshold.',
	saveVersionTitle : 'Save a version',
	saveVersionMsg : 'Do you to want save this revision as a new version?',
	invalidName: 'Name is required and must be in alphanumerics, but may contain single spaces or underscores \'_\'.',
    invalidModule: 'Module name is required and must be in alphanumerics, but may contain periods \'.\', single spaces, or underscores \'_\'.',
	invalidForm : 'Please fix your changes and try to save again. ',

	unsavedMsg : "There are unsaved changes. Save your changes now?",
	resetMsg : "Are you sure you want to reset your changes?"


}