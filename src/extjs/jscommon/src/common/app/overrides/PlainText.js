Ext.define('RS.text.TBText',{
    override : 'Ext.toolbar.TextItem',
    htmlEncode : true,  
    beforeRender : function() {
        var me = this;
        me.callParent();
        Ext.apply(me.renderData, {
            text: me.htmlEncode ? Ext.String.htmlEncode(me.text) : me.text
        });
    },
    setText : function(text){
        var me = this;
        me.text = text;
        if (me.rendered) {
            var processedText = me.htmlEncode ? Ext.String.htmlEncode(text) : text;
            me.el.update(processedText);
            me.updateLayout();
        }
    }
});
Ext.define('RS.text.DisplayField',{
    override : 'Ext.form.field.Display',
    htmlEncode : true   
});
Ext.define('RS.grid.column.DefaultRenderer',{
    override : 'Ext.grid.column.Column',
    renderer : function(v){
        return Ext.String.htmlEncode(v);
    }
});
Ext.define('RS.grid.column.ColumnTemplateRenderer',{
   override : 'Ext.grid.column.Template',
   usingDefaultRenderer: true,
   renderer : function(value, meta, record) {
       var data = Ext.apply({}, record.data, record.getAssociatedData());
       return this.tpl.apply(data);
   }
});
Ext.define('RS.validation.InvalidNumber',{
    override : 'Ext.form.field.Number',
    nanText: 'Not a valid number',
});
Ext.define('RS.validation.InvalidDate',{
    override : 'Ext.form.field.Date',
    invalidText: "Not a valid date - it must be in the format {1}",
});
Ext.define('RS.validation.InvalidTime',{
    override : 'Ext.form.field.Time',
     invalidText: 'Not a valid time',
});
Ext.define('RS.text.FieldLabel',{
    override : 'Ext.form.field.Base',
    htmlLabel : false,
    trimLabelSeparator: function() {
        var me = this,
            separator = me.labelSeparator,
            label = (me.htmlLabel ? me.fieldLabel : Ext.String.htmlEncode(me.fieldLabel)) || '',
            lastChar = label.substr(label.length - 1);
        // if the last char is the same as the label separator then slice it off otherwise just return label value
        return lastChar === separator ? label.slice(0, -1) : label;
    }
});
Ext.define('RS.text.FieldContainerLabel',{
    override : 'Ext.form.FieldContainer',
    htmlLabel : false,
    getFieldLabel: function() {
        var label = (this.htmlLabel ? this.fieldLabel : Ext.String.htmlEncode(this.fieldLabel)) || '';
        if (!label && this.combineLabels) {
            label = Ext.Array.map(this.query('[isFieldLabelable]'), function(field) {
                return field.getFieldLabel();
            }).join(this.labelConnector);
        }
        return label;
    },
    trimLabelSeparator: function() {
        var me = this,
            separator = me.labelSeparator,
            label = (me.htmlLabel ? me.fieldLabel : Ext.String.htmlEncode(me.fieldLabel)) || '',
            lastChar = label.substr(label.length - 1);
        // if the last char is the same as the label separator then slice it off otherwise just return label value
        return lastChar === separator ? label.slice(0, -1) : label;
    }
});
Ext.define('RS.text.ButtonText',{
    override : 'Ext.button.Button',
    htmlEncode : true,
    setText: function(text) {     
        var me = this,
            oldText = me.text || '';

        if (text != oldText) {
            me.text = me.htmlEncode ? Ext.String.htmlEncode(text) : text;
            if (me.rendered) {
                me.btnInnerEl.update(me.text || '&#160;');
                me.setComponentCls();
                if (Ext.isStrict && Ext.isIE8) {
                    // weird repaint issue causes it to not resize
                    me.el.repaint();
                }
                me.updateLayout();
            }
            me.fireEvent('textchange', me, oldText, me.text);
        }
        return me;
    },
    initComponent : function(){
        var me = this;
        me.text = me.htmlEncode ? Ext.String.htmlEncode(me.text) : me.text;
        me.callParent();
    }
});
Ext.define('RS.text.HeaderTitle',{    
    override : 'Ext.panel.Header',
    htmlEncodeTitle : false,
    initComponent: function() {
        var me = this;
        var parentCont = me.up();
        var htmlEncodeTitle = parentCont ? parentCont.htmlEncodeTitle : false;
        me.title = htmlEncodeTitle ? Ext.String.htmlEncode(me.title) : me.title;
        me.callParent();       
    },
    setTitle: function(title) {
        var me = this,
            titleCmp = me.titleCmp;

        var parentCont= me.up();
        var htmlEncodeTitle = parentCont ? parentCont.htmlEncodeTitle : false;
        me.title = (htmlEncodeTitle ? Ext.String.htmlEncode(title) : title);
        if (titleCmp.rendered) {
            titleCmp.textEl.update(me.title || '&#160;');
            titleCmp.updateLayout();
        } else {
            me.titleCmp.on({
                render: function() {
                    me.setTitle(title);
                },
                single: true
            });
        }
    },
});
Ext.define('RS.text.ComboBox',{
    override : 'Ext.form.field.ComboBox',
    listConfig: {
		getInnerTpl: function(displayField) {
			return '{[Ext.String.htmlEncode(values.' + displayField + ')]}';
		}
	}
});
