glu.defView('RS.common.ContentPicker',{
	layout: 'border',
	padding : 15,		
	title: '@{title}',
	cls : 'rs-modal-popup',
	modal: true,
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}],
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockectoolbar',
		name: 'actionBar',
		items: []
	}],
	items: [{
		region: 'west',
		width: 200,
		maxWidth: 300,
		split : true,
		xtype: 'treepanel',
		cls : 'rs-grid-dark',
		columns: [{
			xtype: 'treecolumn',
			dataIndex: 'unamespace',
			text: 'Namespace',
			flex: 1
		}],
		plugins: [{
	        ptype: 'bufferedrenderer',
	        trailingBufferZone: 20,  // Keep 20 rows rendered in the table behind scroll
	        leadingBufferZone: 50   // Keep 50 rows rendered in the table ahead of scroll
	    }],
		store: '@{moduleStore}',
		name : 'module',
		listeners: {
			selectionchange: '@{menuPathTreeSelectionchange}'
		},
		margin : {
			bottom : 10
		}
	}, {
		region: 'center',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		displayName: '@{title}',
		name: 'content',
		store : '@{contentStore}',
		columns: '@{columns}',
		multiSelect: false,
		selected: '@{selected}',
		autoScroll: true,
		viewConfig: {
			enableTextSelection: true
		},
		plugins: [{
			ptype: 'resolveexpander',
			pluginId: 'rowExpander',
			rowBodyTpl: new Ext.XTemplate('<div resolveId="resolveRowBody" style="color:#333"><span style="font-weight:bold">Summary:</span><br/><br/>&nbsp&nbsp&nbsp&nbsp{usummary:htmlEncode}</div>')
		}],		
		selModel: {
			selType: 'resolvecheckboxmodel',		
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'ufullname'
		},
		listeners: {
			editAction: '@{editContent}'
		},
		margin : {
			bottom : 10
		}
	}],
	buttons: [{
		name : 'dump',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}],	
	listeners: {
		beforerender: function() {
			clientVM.setPopupSizeToEightyPercent(this);
		}
	}
});