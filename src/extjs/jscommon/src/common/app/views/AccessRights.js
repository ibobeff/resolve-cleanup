glu.defView('RS.common.AccessRights', {
	xtype: 'panel',
	layout: 'fit',
	items: [{		
		dockedItems: [{
			xtype: 'toolbar',			
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: ['showRoleList', 'removeAccessRight', {
				xtype: 'checkbox',
				hideLabel: true,
				name: 'defaultRoles',
				hidden: '@{!defaultRolesIsVisible}',
				boxLabel: '@{defaultRolesText}'
			}]
		}],
		items: [{
			xtype: 'grid',		
			autoScroll : true,
			minHeight : 300,
			cls : 'rs-grid-dark',
			displayName: '~~Roles~~',
			name: 'rights',
			store: '@{accessRights}',
			viewConfig: {
				markDirty: false
			},			
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}],
			columns: '@{columns}'
		}]
	}],

	asWindow: {
		title: '~~Roles~~',
		width: 800,
		height: 500,
		padding : 15,
		cls : 'rs-modal-popup',		
		buttons: [{
			name : 'dump',
			cls : 'rs-med-btn rs-btn-dark'
		},{
			name : 'cancel',
			cls : 'rs-med-btn rs-btn-light'
		}]
	}
});

glu.defView('RS.common.AccessRights', 'Search', {
	layout: 'fit',
	bodyPadding: '@{bodyPadding}',
	items: [{
		xtype: 'grid',
		displayName: '~~Roles~~',
		name: 'rights',
		store: '@{accessRights}',
		viewConfig: {
			markDirty: false
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: []
		}],
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			useWindowParams: false,
			hideMenu: true
		}, {
			ptype: 'cellediting',
			clicksToEdit: 1
		}, {
			ptype: 'pager'
		}],
		columns: '@{columns}'
	}],

	asWindow: {
		title: '~~Roles~~',
		width: 800,
		height: 500,
		buttonAlign: 'left',
		buttons: ['dump', 'cancel']
	}
});