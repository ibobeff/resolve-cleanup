glu.defView('RS.common.GridPicker', function(arg) {
	var vm = arg.vm;
	return {
		layout: 'fit',	
		items: [{
			xtype: 'grid',
			stateId: '@{stateId}',
			cls :'rs-grid-dark',
			stateful: true,
			layout: 'fit',
			displayName: '@{displayName}',
			name: 'records',
			store: '@{store}',
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar',
				name: 'actionBar',
				items: []
			}],
			columns: '@{columns}',
			plugins: [{
				ptype: 'searchfilter',
				allowPersistFilter: false,
				hideMenu: true
			}, {
				ptype: 'pager',
				pageable: vm.pageable,
				showSysInfo: vm.showSysInfo
			}],
			listeners: {
				selectAllAcrossPages: '@{selectAllAcrossPages}',
				selectionChange: '@{selectionChanged}'
			},
			margin : {
				bottom : 8
			}
		}],

		asWindow: {
			title: '@{asWindowTitle}',
			modal: true,
			padding : 15,
			cls : 'rs-modal-popup',
			width: '@{width}',
			height: '@{height}',			
			buttons: [{
				text: '@{dumperText}',
				name: 'dump',
				cls : 'rs-med-btn rs-btn-dark'
			}, {
				name : 'cancel',
				cls : 'rs-med-btn rs-btn-light'
			}]
		}
	}
});

glu.defView('RS.common.AutoCompleteGridPicker', function(arg) {
	var vm = arg.vm;
	return {
		layout: 'fit',
		bodyPadding: '10px',
		items: [{
			xtype: 'grid',
			stateId: '@{stateId}',
			stateful: true,
			layout: 'fit',
			displayName: '@{displayName}',
			name: 'records',
			store: '@{store}',
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar',
				name: 'actionBar',
				items: [{
					xtype: 'textfield',
					name: 'query'
				}]
			}],
			columns: '@{columns}',
			plugins: [{
				ptype: 'pager',
				pageable: vm.pageable,
				showSysInfo: vm.showSysInfo
			}],
			listeners: {
				selectAllAcrossPages: '@{selectAllAcrossPages}',
				selectionChange: '@{selectionChanged}'
			}
		}],

		asWindow: {
			title: '@{asWindowTitle}',
			modal: true,
			width: '@{width}',
			height: '@{height}',
			buttonAlign: 'left',
			buttons: [{
				text: '@{dumperText}',
				name: 'dump'
			}, 'cancel']
		}
	}
});