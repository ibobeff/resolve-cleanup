glu.defView('RS.common.SysInfo', {
	asWindow: {
		height: 290,
		width: 500,
		modal: true,
		title: '~~sysInfoButtonTitle~~'
	},
	autoScroll: true,
	buttonAlign: 'left',
	buttons: ['close'],

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		bodyPadding: '0 10 10 10',
		padding: '10px 0 0 0'
	},
	items: [{
		xtype: 'panel',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaultType: 'displayfield',
		defaults: {
			labelWidth: 130
		},
		items: [
			'sysId', 
			'createdOn', 
			'sysCreatedBy', 
			'updatedOn', 
			'sysUpdatedBy'
		]
	}]
});
