glu.defView('RS.common.UploadWordDoc', {
	asWindow: {
		title: '~~uploadWordDocument~~',
		cls : 'rs-modal-popup',
		height: 400,
		width: 800,
		modal: true,
		padding : 15,
		closable: true
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	
	items: [{
			html: '@{intro}',
			margin: '0 0 10 0'
	}, {
		itemId: 'fineUploaderTemplate',
		html: '@{fineUploaderTemplate}',
		height: '@{fineUploaderTemplateHeight}',
		hidden: '@{fineUploaderTemplateHidden}',
	}, {
		id: 'attachments_grid',
		hidden: true
	}],
	buttons: [{
		text: '~~browse~~',
		id: 'upload_button',
        cls : 'rs-med-btn rs-btn-dark',
		preventDefault: false,
    }, {
        name: 'close',
        cls : 'rs-med-btn rs-btn-light'
	}]
})
