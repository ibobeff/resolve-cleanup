glu.defView('RS.common.GridButton', {
	xtype: 'button',
	text: '@{text}',
	handler: '@{dump}',
	cls : '@{cls}'
});
glu.defView('RS.common.KeywordGridPicker', function(args) {
	var vm = args.vm;
	return {
		title: '@{title}',
		width: '@{width}',
		height: '@{height}',
		modal: true,
		cls : 'rs-modal-popup',
		padding : 15,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},

		items: [{
			flex: 1,
			xtype: 'grid',
			margin : '0 0 8 0',
			cls : 'rs-grid-dark',
			plugins: [{
				ptype: 'pager',
				allowAutoRefresh: false,
				showSysInfo: vm.showSysInfo
			}],
			dockedItems: [{
				xtype: 'toolbar',				
				cls: 'actionBar rs-dockedtoolbar',
				name: 'actionBar',
				items: [{
					xtype: 'textfield',
					name: 'keyword'
				}]
			}],
			name: 'records',
			store: '@{store}',
			columns: '@{columns}'
		}],			
		buttons: '@{buttons}',
		listeners: {
			resize: function() {
				this.center()
			}
		}
	}
});