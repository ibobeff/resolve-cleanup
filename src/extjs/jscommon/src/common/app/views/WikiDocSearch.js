glu.defView('RS.common.WikiDocSearch', 'Single', {	
	layout: 'fit',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		padding: '10px 0px 0px 10px',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'panel',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'combobox',
				editable: false,
				store: '@{typesStore}',
				displayField: 'name',
				valueField: 'value',
				queryMode: 'local',
				name: 'type',
				width: 220,
				labelWidth: 100
			}, {
				xtype: 'combobox',
				itemId: 'filter',
				emptyText: '~~filter~~',
				inputCls: 'post-message',
				filterStore: '@{store}',
				displayField: 'name',
				valueField: 'value',
				width: 510,
				margins: {
					left: 5
				},
				getFilterBar: function() {
					return this.ownerCt.ownerCt.ownerCt.down('#filterBar');
				},
				columns: '@{columns}',
				plugins: [{
					ptype: 'searchfieldfilter',
					filterBarItemId: 'filterBar'
				}],
				listeners: {
					beforerender: function(combo) {
						Ext.Array.forEach(combo.columns, function(column) {
							switch (column.text) {
								case '~~ufullname~~':
									column.text = RS.common.locale.ufullname;
									break;
								case '~~sysCreatedOn~~':
									column.text = RS.common.locale.sysCreatedOn;
									break;
								case '~~sysCreatedBy~~':
									column.text = RS.common.locale.sysCreatedBy;
									break;
								case '~~sysUpdatedOn~~':
									column.text = RS.common.locale.sysUpdatedOn;
									break;
								case '~~sysUpdatedBy~~':
									column.text = RS.common.locale.sysUpdatedBy;
									break;
								case '~~sys_id~~':
									column.text = RS.common.locale.sys_id;
									break;
								case '~~sysOrganizationName~~':
									column.text = RS.common.locale.sysOrganizationName;
									break;
							}
						})
					}
				}
			}]
		}]
	}, {
		xtype: 'filterbar',
		itemId: 'filterBar',
		allowPersistFilter: false,
		listeners: {
			add: {
				fn: function() {
					this.ownerCt.down('#filter').getPlugin().filterChanged()
				},
				buffer: 10
			},
			remove: {
				fn: function() {
					this.ownerCt.down('#filter').getPlugin().filterChanged()
				},
				buffer: 10
			},
			clearFilter: function() {
				this.ownerCt.down('#filter').clearValue()
			}
		}
	}],
	items: [{
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: []
		}],
		// name:'wiki',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		margin : '0 0 8 0',
		layout: 'fit',
		store: '@{store}',
		columns: '@{columns}',
		selected: '@{wikiSelection}',
		plugins: [{
			ptype: 'pager'
		}, {
			ptype: 'columnautowidth'
		}]
	}],
	buttons: [{
		name : 'choose',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}],
	asWindow: {
		title: '~~title~~',
		cls : 'rs-modal-popup',
		padding : 15,
		modal: true,
		listeners: {
			beforeshow: function() {
				clientVM.setPopupSizeToEightyPercent(this);
			}
		}
	}
});


glu.defView('RS.common.WikiDocSearch', {
	bodyPadding: '10px',
	layout: 'fit',

	buttonAlign: 'left',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		padding: '10px 0px 0px 10px',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'panel',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'combobox',
				editable: false,
				store: '@{typesStore}',
				displayField: 'name',
				valueField: 'value',
				queryMode: 'local',
				name: 'type',
				width: 220,
				labelWidth: 100
			}, {
				xtype: 'combobox',
				itemId: 'filter',
				emptyText: '~~filter~~',
				inputCls: 'post-message',
				filterStore: '@{store}',
				displayField: 'name',
				valueField: 'value',
				width: 510,
				margins: {
					left: 5
				},
				getFilterBar: function() {
					return this.ownerCt.ownerCt.ownerCt.down('#filterBar');
				},
				columns: '@{columns}',
				plugins: [{
					ptype: 'searchfieldfilter',
					filterBarItemId: 'filterBar'
				}],
				listeners: {
					beforerender: function(combo) {
						Ext.Array.forEach(combo.columns, function(column) {
							switch (column.text) {
								case '~~ufullname~~':
									column.text = RS.common.locale.ufullname;
									break;
								case '~~sysCreatedOn~~':
									column.text = RS.common.locale.sysCreatedOn;
									break;
								case '~~sysCreatedBy~~':
									column.text = RS.common.locale.sysCreatedBy;
									break;
								case '~~sysUpdatedOn~~':
									column.text = RS.common.locale.sysUpdatedOn;
									break;
								case '~~sysUpdatedBy~~':
									column.text = RS.common.locale.sysUpdatedBy;
									break;
								case '~~sys_id~~':
									column.text = RS.common.locale.sys_id;
									break;
								case '~~sysOrganizationName~~':
									column.text = RS.common.locale.sysOrganizationName;
									break;
							}
						})
					}
				}
			}]
		}]
	}, {
		xtype: 'filterbar',
		itemId: 'filterBar',
		allowPersistFilter: false,
		listeners: {
			add: {
				fn: function() {
					this.ownerCt.down('#filter').getPlugin().filterChanged()
				},
				buffer: 10
			},
			remove: {
				fn: function() {
					this.ownerCt.down('#filter').getPlugin().filterChanged()
				},
				buffer: 10
			},
			clearFilter: function() {
				this.ownerCt.down('#filter').clearValue()
			}
		}
	}],
	items: [{
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: []
		}],
		name: 'wiki',
		selType: 'checkboxmodel',
		xtype: 'grid',
		layout: 'fit',
		store: '@{store}',
		columns: '@{columns}',
		plugins: [{
			ptype: 'pager'
		}, {
			ptype: 'columnautowidth'
		}]
	}],

	buttons: ['choose', 'cancel'],
	asWindow: {
		title: '~~title~~',
		modal: true,
		listeners: {
			beforeshow: function() {
				clientVM.setPopupSizeToEightyPercent(this);
			}
		}
	}
});