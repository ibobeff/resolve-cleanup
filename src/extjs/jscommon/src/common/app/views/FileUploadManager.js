glu.defView('RS.common.UploadFile', {
	parentLayout: 'FileUploadManager',
})

glu.defView('RS.common.dummy') //to make sure the ns is init before creating the factory
RS.common.views.FileUploadManagerFactory = function(actualView) {
	var output = {
		asWindow: {
			title: '@{title}',
			cls : 'rs-modal-popup',		
			width: 800,
			modal: true,
			padding : 15,
			closable: true
		},
	
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		
		dockedItems: [{
			xtype: 'toolbar',			
			cls: 'rs-dockedtoolbar',
			defaults : {
				cls: 'rs-small-btn rs-btn-light'
			},
			items: [{
				xtype: 'button',
				name: 'uploadFile',
				text: 'Upload',
				preventDefault: false,
				hidden: '@{!allowUpload}',
				id: 'upload_button'
			}, {
				name: 'delete',
				text: 'Delete',
				hidden: '@{!allowRemove}',
	
			}, '->', {
				name: 'reload',
				tooltip: 'Reload',
				text: '',
				iconCls: 'x-tbar-loading',
			}]
		}],
		items: [{
			itemId: 'fineUploaderTemplate',
			html: '@{fineUploaderTemplate}',
			height: '@{fineUploaderTemplateHeight}',
			hidden: '@{fineUploaderTemplateHidden}',
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			id: 'attachments_grid',
			store: '@{filesStore}',
			viewConfig: {
				markDirty: false
			},
			columns: '@{filesColumns}',
			multiSelect: '@{multiSelect}',
			margin: '6 0 0 0',
			flex: 1,
			listeners: {
				selectionChange: '@{selectionChanged}'
			}
		}],
		buttons: [{
			text: '~~close~~',
			itemId:  'cancelBnt',
			handler: '@{close}',
			cls : 'rs-med-btn rs-btn-light'
		}]
	}

	if (actualView && actualView.propertyPanel) {
		Ext.apply(output, actualView.propertyPanel);
	}
	return output;
}

