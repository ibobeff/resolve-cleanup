Ext.define('RS.common.DocumentField', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'documentfield',
    enableKeyEvents: true,
    trigger2Cls: 'x-form-search-trigger',
    queryMode: 'remote',
    typeAhead: false,
    minChars: 0,
    queryDelay: 500,
    pageSize: 20,
    ignoreSetValueOnLoad: true, // To prevent chars typed being deleted
    listConfig: {
        loadMask: false
    },

    onTrigger2Click: function() {
        this.fireEvent('searchdocument', this, this, this.inline);
    },

    initComponent: function() {
        this.callParent(arguments);
        this.store.on('beforeload', function(store, op) {
            op.params = op.params || {};
            var curFilter = op.params.filter || '[]';
            if (typeof(curFilter) == 'string') {
                curFilter = Ext.decode(curFilter);
            }
            Ext.apply(op.params, {
                filter: Ext.encode([{
                    field: this.displayField,
                    condition: 'contains',
                    type: 'auto',
                    value: op.params.query
                }].concat(curFilter))
            });
        }, this);
    }
});