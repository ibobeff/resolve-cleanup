/*
Ext.define('RS.common.SinglePlupUpload', {
	extend: 'Ext.util.Observable',
	configs: {
		uploader: {
			runtimes: "html5,flash,silverlight,html4",
			url: '',
			browse_button: null,
			flash_swf_url: '/resolve/js/plupload/Moxie.swf',
			unique_names: true,
			multipart: true,
			multipart_params: {},
			multi_selection: false,
			browse: true,
			required_features: true
		},

		uploaderListener: {
			/**
			 * Plupload EVENTS
			 * /
			_Init: function(uploader, data) {
				this.runtime = data.runtime;
				this.fireEvent('uploadready', this);
			},

			// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			_BeforeUpload: function(uploader, file) {
				this.fireEvent('beforeupload', this, uploader, file);
			},

			_ChunkUploaded: function() {},

			_FilesAdded: function(uploader, files) {
				var me = this;

				if (uploader.settings.multi_selection != true) {
					files = [files[0]];
					uploader.files = [files[0]];
				}

				if (me.fireEvent('filesadded', me, files) !== false) {
					if (uploader.settings.autoStart && uploader.state != 2) Ext.defer(function() {
						me.start();
					}, 300);
				}
			},

			_FilesRemoved: function(uploader, files) {

			},

			_FileUploaded: function(uploader, file, status) {
				var me = this,
					response = Ext.JSON.decode(status.response);

				if (response.success == true) {
					Ext.apply(file, response.data);
					file.server_error = 0;
					me.success.push(file);
					me.fireEvent('fileuploaded', me, file, response);
				} else {
					if (response.message) {
						file.msg = '<span style="color: red">' + response.message + '</span>';
					}
					file.server_error = 1;
					me.failed.push(file);
					me.fireEvent('uploaderror', me, Ext.apply(status, {
						file: file
					}));
				}
			},

			_PostInit: function(uploader) {},

			_QueueChanged: function(uploader) {},

			_Refresh: function(uploader) {},

			_StateChanged: function(uploader) {
				if (uploader.state == 2) {
					this.uploader.disableBrowse(true);
					this.fireEvent('uploadstarted', this);
				} else {
					this.uploader.disableBrowse(false);
					this.fireEvent('uploadcomplete', this, this.success, this.failed);
					if (this.autoRemoveUploaded) this.removeUploaded();
				}
			},

			_UploadFile: function(uploader, file) {},

			_UploadProgress: function(uploader, file) {
				var me = this,
					name = file.name,
					size = file.size,
					percent = file.percent;
				me.fireEvent('uploadprogress', me, file, name, size, percent);
				if (file.server_error) file.status = 4;
			},

			_Error: function(uploader, data) {
				if (data.file) {
					data.file.status = 4;
					if (data.code == -600) {
						data.file.msg = Ext.String.format('<span style="color: red">{0}</span>', this.statusInvalidSizeText);
					} else if (data.code == -700) {
						data.file.msg = Ext.String.format('<span style="color: red">{0}</span>', this.statusInvalidExtensionText);
					} else {
						data.file.msg = Ext.String.format('<span style="color: red">{2} ({0}: {1})</span>', data.code, data.details, data.message);
					}
					this.failed.push(data.file);
				}
				this.fireEvent('uploaderror', this, data);
			}
		}
	},

	constructor: function(owner) {
		var me = this;
		me.owner = owner;
		me.success = [];
		me.failed = [];
		me.addEvents('beforestart', 'uploadready', 'uploadstarted', 'uploadcomplete', 'uploaderror', 'filesadded', 'beforeupload', 'fileuploaded', 'updateprogress', 'uploadprogress', 'storeempty', 'uploadstopped');
		me.listeners = Ext.apply({}, owner.uploaderListeners);
		me.callParent();
	},

	/**
	 * @private
	 * /
	initialize: function(config) {
		var me = this;
		if (!me.initialized) {
			me.initialized = true;
			me.initializeUploader(config);
		}
	},

	initializeUploader: function(config) {
		var me = this;
		me.uploaderConfig = Ext.applyIf(config.uploader, me.configs.uploader);
		me.uploader = Ext.create('plupload.Uploader', me.uploaderConfig);

		Ext.each([
			'Init',
			'ChunkUploaded',
			'FilesAdded',
			'FilesRemoved',
			'FileUploaded',
			'PostInit',
			'QueueChanged',
			'Refresh',
			'StateChanged',
			'BeforeUpload',
			'UploadFile',
			'UploadProgress',
			'Error'
		], function(v) {
			me.uploader.bind(v, me.configs.uploaderListener['_' + v], me);
		}, me);

		me.uploader.init();
	},

	start: function() {
		var me = this;
		me.fireEvent('beforestart', me);
		if (me.multipart_params) {
			me.uploader.settings.multipart_params = me.multipart_params;
		}
		me.uploader.start();
	},

	stop: function() {
		this.uploader.stop();
		this.fireEvent('uploadstopped', this);
	},

	clearQueue: function() {
		this.uploader.splice();
	}
});
*/