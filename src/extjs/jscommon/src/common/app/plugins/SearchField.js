Ext.define('RS.common.plugin.SearchFieldFilter', {
	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.searchfieldfilter',

	dateFormat: 'Y-m-d',
	timeFormat: 'g:i a',
	submitFormat: 'Y-m-d H:i:s',

	showComparisons: true,

	columns: [],


	init: function(field) {
		var me = this;
		me.field = field
		me.columns = field.columns || me.columns

		Ext.apply(field, {
			displayField: 'name',
			valueField: 'value',
			trigger2Cls: 'x-form-search-trigger',
			enableKeyEvents: true,
			queryMode: 'remote',
			typeAhead: false,
			minChars: 1,
			queryDelay: 10,
			listConfig: {
				emptyText: RS.common.locale.noSuggestions
			},
			onTriggerClick: function() {
				if (!this.advanced) {
					var pos = this.getPosition(),
						items = [];
					Ext.each(me.fieldColumns, function(column) {
						var columnXtype = '',
							comparisons = me.getShortOptionsForDataType(column.dataType);

						switch (column.dataType) {
							case 'date':
								switch (column.uiType) {
									case 'Date':
										columnXtype = 'xdatefield';
										break;
									case 'Timestamp':
										columnXtype = 'xtimefield';
										break;
									default:
										columnXtype = 'xdatetimefield';
										break;
								}
								break;
							case 'float':
							case 'int':
								columnXtype = 'numberfield';
								break;
							case 'bool':
								columnXtype = 'booleancombobox';
								break;
							default:
								columnXtype = 'textfield';
								break;
						}

						if (column.filterable) {
							items.push({
								fieldLabel: column.text,
								labelAlign: 'top',
								name: column.dataIndex + '_type',
								xtype: 'fieldcontainer',
								layout: 'hbox',
								items: [{
									xtype: 'combobox',
									name: column.dataIndex + '_comparison',
									width: 115,
									displayField: 'name',
									valueField: 'value',
									padding: '0 5 0 0',
									hidden: !me.showComparisons,
									store: Ext.create('Ext.data.Store', {
										fields: ['name', 'value'],
										data: comparisons
									}),
									value: column.dataType == 'date' ? 'on' : 'equals'
								}, {
									name: column.dataIndex,
									xtype: columnXtype,
									format: columnXtype == 'xtimefield' ? me.timeFormat : me.dateFormat,
									submitFormat: 'Y-m-d H:i:sO',
									flex: 1
								}]

							});
						}
					}, this);

					this.advanced = Ext.create('Ext.Window', {
						closeAction: 'hide',
						modal: true,
						cls : 'rs-modal-popup',
						title: 'Advanced Search',					
						width: 800,
						height : 600,
						layout: 'fit',
						padding : 15,					
						items: {
							xtype: 'form',
							autoScroll: true,												
							items: items,							
							buttons: [{
								text: RS.common.locale.search,
								name: 'searchBtn',
								cls : 'rs-med-btn rs-btn-dark',
								handler: function(button) {
									var form = button.up('form').getForm(),
										values = form.getValues();
									Ext.each(me.fieldColumns, function(column) {
										var value = values[column.dataIndex],
											comp = values[column.dataIndex + '_comparison'];
										if (value) me.addFilter(Ext.String.format('{0} {1} {2}', column.text, comp, value));
									});
									form.reset();
									button.up('window').close();
								}
							}, {
								text: RS.common.locale.cancel,
								cls : 'rs-med-btn rs-btn-light',
								handler: function(button) {
									button.up('form').getForm().reset()
									button.up('window').close()
								}
							}]
						}
					});
				}			
				this.advanced.show();
			},
			onTrigger2Click: function() {
				if (!this.getValue()) return;
				if (me.addFilter(this.getValue())) this.reset();
			}
		})

		me.fieldColumns = []

		me.conditions = {
			on: RS.common.locale.on,
			after: RS.common.locale.after,
			before: RS.common.locale.before,
			equals: RS.common.locale.equals,
			notEquals: RS.common.locale.notEquals,
			contains: RS.common.locale.contains,
			notContains: RS.common.locale.notContains,
			startsWith: RS.common.locale.startsWith,
			notStartsWith: RS.common.locale.notStartsWith,
			endsWith: RS.common.locale.endsWith,
			notEndsWith: RS.common.locale.notEndsWith,
			greaterThan: RS.common.locale.greaterThan,
			greaterThanOrEqualTo: RS.common.locale.greaterThanOrEqualTo,
			lessThan: RS.common.locale.lessThan,
			lessThanOrEqualTo: RS.common.locale.lessThanOrEqualTo,
			shortEquals: RS.common.locale.shortEquals,
			shortNotEquals: RS.common.locale.shortNotEquals,
			shortGreaterThan: RS.common.locale.shortGreaterThan,
			shortGreaterThanOrEqualTo: RS.common.locale.shortGreaterThanOrEqualTo,
			shortLessThan: RS.common.locale.shortLessThan,
			shortLessThanOrEqualTo: RS.common.locale.shortLessThanOrEqualTo
		};

		me.shortConditions = {};
		me.shortConditions[RS.common.locale.shortEquals] = 'equals';
		me.shortConditions[RS.common.locale.shortNotEquals] = 'notEquals';
		me.shortConditions[RS.common.locale.shortGreaterThan] = 'greaterThan';
		me.shortConditions[RS.common.locale.shortGreaterThanOrEqualTo] = 'greaterThanOrEqualTo';
		me.shortConditions[RS.common.locale.shortLessThan] = 'lessThan';
		me.shortConditions[RS.common.locale.shortLessThanOrEqualTo] = 'lessThanOrEqualTo';

		me.dataValues = {
			today: RS.common.locale.today,
			yesterday: RS.common.locale.yesterday,
			lastWeek: RS.common.locale.lastWeek,
			lastMonth: RS.common.locale.lastMonth,
			lastYear: RS.common.locale.lastYear
		}

		if (me.field.filterStore)
			me.field.filterStore.on('beforeLoad', function(store, operation, eOpts) {
				var whereClause = this.getWhereClause(),
					whereFilter = this.getWhereFilter();
				operation.params = operation.params || {}

				//merge any existing filters
				if (operation.params.filter) {
					var existingFilter = Ext.decode(operation.params.filter)
					whereFilter = whereFilter.concat(existingFilter)
				}

				Ext.apply(operation.params, {
					filter: Ext.encode(whereFilter)
				})
				store.lastWhereClause = whereClause
			}, me)

		var fields = me.field.filterStore.proxy.reader.model.getFields();
		Ext.each(me.columns, function(column) {
			var col = column;
			Ext.each(fields, function(field) {
				if (field.name == col.dataIndex) {
					col.dataType = field.type.type;
					return false;
				}
			})
			me.fieldColumns.push(col)
		})

		me.field.store = Ext.create('Ext.data.Store', {
			fields: ['name', 'value', 'type', 'displayName'],
			proxy: {
				type: 'memory',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			},
			listeners: {
				load: function(store, records, successful, eOpts) {
					var combo = me.field,
						value = combo.nextVal || combo.getValue();
					delete combo.nextVal
					me.parseSuggestionsForStore(store, value)
				}
			}
		})

		field.on({
			specialkey: function(field, e) {
				// e.HOME, e.END, e.PAGE_UP, e.PAGE_DOWN,
				// e.TAB, e.ESC, arrow keys: e.LEFT, e.RIGHT, e.UP, e.DOWN
				if (e.getKey() == e.ENTER) {
					Ext.defer(function() {
						field.onTrigger2Click()
					}, 10)
				}

				if (e.getKey() == e.TAB) {
					e.preventDefault()
				}
			},
			beforeSelect: function(combo, record, index, eOpts) {
				record.data.name = record.data.displayName;
				record.data.value = record.data.displayName;
				combo.nextVal = record.data.name;
				if (record.data.name.lastIndexOf(' ') == record.data.name.length - 1) {
					combo.store.load({
						params: {
							query: record.data.displayName
						}
					});
					Ext.defer(function() {
						combo.expand();
					}, 10);
				}
			},
			afterRender: function(field) {
				me.filterBar = field.getFilterBar ? field.getFilterBar(field) : field.ownerCt.down('#' + me.filterBarItemId)
			}
		})
	},

	addFilter: function(value) {
		var internalValue = this.getInternalValue(value),
			split = internalValue.split(' ');

		//if the user has typed: field condition value, then add it
		if (split.length > 2 && split[2]) {
			this.filterBar.addFilter(value, internalValue);
			return true;
		}

		//If the user types: value then pick the first column and add a contains
		// split = (value + ' ').split(' ')
		// if (split.length > 1 && (split[0] || split[1])) {
		// 	//find the first non-date filterable column to filter on
		// 	var col = null;
		// 	Ext.Array.each(this.grid.columns, function(c) {
		// 		if (c.dataType != 'date' && c.uiType != 'DateTime' && c.filterable) {
		// 			col = c
		// 			return false
		// 		}
		// 	})
		// 	if (col) {
		// 		this.filterBar.addFilter([col.text, RS.common.locale.contains, value].join(' '), [col.dataIndex, RS.common.locale.contains, value].join(' '))
		// 		return true;
		// 	}
		// }
		return false;
	},
	parseSuggestionsForStore: function(store, value) {
		//Split the value on spaces for the search query
		var searches = (value || '').split(' '),
			columnText = '',
			column = null,
			i;

		//Parse column text
		for (i = 0; i < searches.length && this.couldBeColumn(Ext.String.trim(columnText + ' ' + searches[i])); i++) {
			columnText += ' ' + searches[i];
			columnText = Ext.String.trim(columnText);
		}

		Ext.each(this.fieldColumns, function(fieldColumn) {
			if (fieldColumn.text && fieldColumn.text.toLowerCase() == columnText.toLowerCase()) {
				column = fieldColumn;
				return false;
			}
		});

		store.removeAll();

		//Add in the first non-date column as a search suggestion if the current selection isn't good
		if (!column || !column.filterable) {
			var col = null;
			Ext.each(this.fieldColumns, function(c) {
				if (c.dataType != 'date' && c.uiType != 'DateTime' && c.filterable) {
					col = c
					return false
				}
			})
			if (col) {
				store.add({
					name: Ext.String.format('{0} {1} <b>{2}</b>', col.text, RS.common.locale.contains, Ext.String.htmlEncode(value)),
					value: RS.common.locale.contains,
					displayName: Ext.String.format('{0} {1} {2}', col.text, RS.common.locale.contains, value)
				})
			}
		}

		//If we have a column, then we're searching on a column and need the operators
		if (column && column.filterable) {
			var conditionText = '',
				operator = null;
			for (; i < searches.length && this.couldBeCondition(Ext.String.trim(conditionText + ' ' + searches[i])); i++) {
				conditionText += ' ' + searches[i];
				conditionText = Ext.String.trim(conditionText);
				}

			//Get operator equivalent
			Ext.Object.each(this.conditions, function(con) {
				if (this.conditions[con] == conditionText) {
					if (con.indexOf('short') == 0) {
						operator = this.shortConditions[conditionText]
					} else
						operator = this.conditions[con];
					return false;
				}
			}, this);

			if (operator) {
				//Then we have an operator and need to display the data suggestions
				var value = searches.splice(i).join(' '),
					options = this.getValueOptionsForDataType(column, columnText, conditionText, value),
					dataValues = [];

				//Add in the user's typed in search criteria as the first option
				store.add({
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, conditionText, Ext.String.htmlEncode(value)),
					value: conditionText,
					displayName: Ext.String.format('{0} {1} {2}', columnText, conditionText, value)
				});

				store.add(options);
				//Get the data options for the column
				this.field.filterStore.each(function(record) {
					var data = record.get(column.dataIndex);
					if (data) {
						if (Ext.isDate(data)) data = Ext.Date.format(data, this.dateFormat);
						if (Ext.isBoolean(data)) data = data.toString();
						if (Ext.isNumber(data)) data = data.toString();
						if (dataValues.indexOf(data.toLowerCase()) === -1 && data.toLowerCase().indexOf(value.toLowerCase()) > -1) {
							dataValues.push(data.toLowerCase());
							store.add({
								name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, conditionText, Ext.String.htmlEncode(data)),
								value: conditionText,
								displayName: Ext.String.format('{0} {1} {2}', columnText, conditionText, data)
							});
						}
					}
					if (store.getCount() >= 6) return false;
				}, this);

				//If we have the operator, but don't have any data suggestions for what they are typing, then we need to just return the value that they are typing to help them out
				if (store.getCount() == 0) {
					store.add({
						name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, conditionText, Ext.String.htmlEncode(value)),
						value: conditionText,
						displayName: Ext.String.format('{0} {1} {2}', columnText, conditionText, value)
					})
				}
			} else {
				//Then we have a part of an operator and need to display the operators that match the search
				var options = this.getOptionsForDataType(column);

				//First add in the contains of the user provided value as an option for the user if it doesn't match a condition option
				var val = searches.slice(1).join(' ');
				if (val) {
					store.add({
						name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, RS.common.locale.contains, Ext.String.htmlEncode(val)),
						value: RS.common.locale.contains,
						displayName: Ext.String.format('{0} {1} {2}', columnText, RS.common.locale.contains, val)
					})
				}

				Ext.each(options, function(option) {
					if (option.value.indexOf(conditionText) > -1) store.add(option);
				});
			}

		} else if (columnText) {
			//We don't have a column, but have a part of a column so show those that match the current search
			Ext.each(this.fieldColumns, function(column) {
				if (column.text.toLowerCase().indexOf(columnText.toLowerCase()) == 0 && column.filterable) {
					store.add({
						name: Ext.String.format('<b>{0}</b>{1}', column.text.substring(0, columnText.length), column.text.substring(columnText.length)),
						value: column.text,
						type: column.dataType,
						displayName: column.text + ' '
					});
				}
			});
		} else {
			//No column matches, so we need to search the data
			var query = value.toLowerCase(),
				columnData = [],
				recordData = [],
				records = [];
			this.field.filterStore.each(function(record) {
				Ext.Object.each(record.data, function(data) {
					var tempValue = record.data[data];
					if (Ext.isBoolean(tempValue)) tempValue = tempValue.toString()
					if (Ext.isDate(tempValue)) tempValue = Ext.Date.format(tempValue, this.dateFormat)
					if (Ext.isNumber(tempValue)) tempValue = tempValue.toString()
					if (tempValue && Ext.isString(tempValue) && tempValue.toLowerCase().indexOf(query.toLowerCase()) > -1 && recordData.indexOf(tempValue.toLowerCase()) == -1) {
						if (columnData.indexOf(data) == -1) columnData.push(data);
						recordData.push(tempValue.toLowerCase());
						records.push({
							column: data,
							data: tempValue
						});
					}
					if (columnData.length >= 6 || recordData.length >= 6) return false;
				}, this);
			}, this);

			//Now we have a list of columns.  Display the columns with the appropriate comparisons for the data
			Ext.each(columnData, function(column) {
				if (store.getCount() >= 6) return false;
				Ext.each(this.fieldColumns, function(fieldColumn) {
					if (store.getCount() >= 6) return false;
					if (fieldColumn.dataIndex == column && column.filterable) {
						var conditionText = RS.common.locale.contains;
						if (fieldColumn.dataType == 'date') conditionText = RS.common.locale.on;
						store.add({
							name: Ext.String.format('{0} {1} <b>{2}</b>', fieldColumn.text, conditionText, query),
							value: conditionText,
							displayName: Ext.String.format('{0} {1} {2}', fieldColumn.text, conditionText, query)
						});
					}
				});
			}, this);

			//Now we have a list of the records and their column dataIndexes so we need to turn those into proper queries based on their types
			Ext.each(records, function(record) {
				if (store.getCount() >= 6) return false;
				Ext.each(this.fieldColumns, function(column) {
					if (column.dataIndex == record.column && column.filterable) {
						record.columnText = column.text;
						record.columnDataType = column.dataType;
						if (column.dataType == 'date') record.conditionText = RS.common.locale.on;
						else record.conditionText = RS.common.locale.equals;
						return false;
					}
				});

				if (record.columnText) {
					//Now that we have a record with all the information we could need, we need to add it to the store
					store.add({
						name: Ext.String.format('{0} {1} <b>{2}</b>', record.columnText, record.conditionText, Ext.String.htmlEncode(record.data)),
						value: record.data,
						displayName: Ext.String.format('{0} {1} {2}', record.columnText, record.conditionText, record.data)
					});
				}
			}, this);
		}

		if (store.getCount() == 0) {
			//No records match the search criteria as of yet (could be another page or something), so load up the columns with their types based on the type of query
			var queryType = 'string',
				conditionText = RS.common.locale.contains;
			if (Ext.isNumber(Number(value))) {
				queryType = 'float';
				conditionText = RS.common.locale.equals
			}
			if (Ext.isDate(Ext.Date.parse(value, this.dateFormat))) {
				queryType = 'date';
				conditionText = RS.common.locale.on;
			}

			Ext.each(this.fieldColumns, function(column) {
				if (column.filterable && column.dataType == queryType) {
					store.add({
						name: Ext.String.format('{0} {1} <b>{2}</b>', column.text, conditionText, Ext.String.htmlEncode(value)),
						value: value,
						displayName: Ext.String.format('{0} {1} {2}', column.text, conditionText, value)
					})
				}
				if (store.getCount() >= 6) return false
			})
		}
	},
	getInternalValue: function(value) {
		var returnText = '',
			split = value.split(' ');

		var columnText = '',
			i;
		//Get column dataIndex
		for (i = 0; i < split.length && this.couldBeColumn(Ext.String.trim(columnText + ' ' + split[i])); i++) {
			columnText += ' ' + split[i];
			columnText = Ext.String.trim(columnText);
		}
		Ext.each(this.fieldColumns, function(column) {
			if (column.text && column.text.toLowerCase() == columnText.toLowerCase()) returnText += column.dataIndex + ' ';
		});

		var conditionText = '';
		for (; i < split.length && this.couldBeCondition(Ext.String.trim(conditionText + ' ' + split[i])); i++) {
			conditionText += ' ' + split[i];
			conditionText = Ext.String.trim(conditionText);
		}

		//Get operator equivalent
		Ext.Object.each(this.conditions, function(con) {
			var c = con
			if (this.conditions[con] == conditionText) {
				if (con.indexOf('short') == 0) {
					c = this.shortConditions[conditionText]
				}
				returnText += c;
				return false;
			}
		}, this);
		returnText += ' ';

		var val = split.slice(i).join(' '),
			matchDataValue = false;
		//Get value equivalent
		Ext.Object.each(this.dataValues, function(value) {
			if (this.dataValues[value] == val) {
				matchDataValue = true;
				returnText += value;
				return false;
			}
		}, this);
		if (!matchDataValue) returnText += val;

		return returnText;
	},
	getExternalValue: function(filter) {
		var split = filter.split(' '),
			columnText = split[0],
			condition = split[1];
		split.splice(0, 2);
		var value = split.join(' ');
		//Convert column
		Ext.each(this.fieldColumns, function(column) {
			if (column.dataIndex == columnText) {
				columnText = column.text;
				return false;
			}
		});
		//Convert condition
		condition = RS.common.locale[condition];

		return Ext.String.format('{0} {1} {2}', columnText, condition, value);
	},
	couldBeColumn: function(columnText) {
		var exists = false;
		Ext.each(this.fieldColumns, function(column) {
			if (column.filterable && column.text && column.text.toLowerCase().indexOf(columnText.toLowerCase()) == 0) {
				exists = true;
				return false;
			}
		});
		return exists;
	},
	couldBeCondition: function(condition) {
		condition = condition.toLowerCase();
		var returnVal = false;
		Ext.Object.each(this.conditions, function(con) {
			if (this.conditions[con].indexOf(condition) == 0) {
				returnVal = true;
				return false;
			}
		}, this);
		return returnVal;
	},
	filterChanged: function() {
		//Reset to page 1 whenever the filter changes because the current page might not be around anymore and the server is naive to know
		this.field.filterStore.loadPage(1);
	},
	getWhereFilter: function() {
		if (!this.filterBar) return []
		var filters = this.filterBar.getFilters(),
			whereFilter = [];

		Ext.each(filters, function(filter, index) {
			var split = filter.split(' '),
				column = split[0],
				condition = split[1],
				value = split.slice(2).join(' '),
				col = {},
				field, val;

			//get the column based on the column dataIndex
			Ext.each(this.fieldColumns, function(fieldColumn) {
				if (fieldColumn.dataIndex == column) {
					col = fieldColumn;
					return false;
				}
			});

			if (col) {
				if (col.dataType == 'date') {
					val = Ext.Date.parse(value, this.dateFormat);
					if (!Ext.isDate(val)) val = Ext.Date.parse(value, 'c')
					if (Ext.isDate(val)) {
						value = Ext.Date.format(val, 'c');
					}
				}

				if (col.dataType == 'bool') {
					value = value === 'true' || value === true || value === 1 || value === '1'
				}

				field = col.dataIndex;

				whereFilter.push({
					field: field,
					type: col.dataType,
					condition: condition,
					value: value
				});
			}
		}, this);

		return whereFilter;
	},
	getWhereClause: function() {
		if (!this.filterBar) return []
		var filters = this.filterBar.getFilters(),
			whereClause = '';
		//Parse through filters to get the where clause
		Ext.each(filters, function(filter, index) {
			var split = filter.split(' '),
				column = split[0],
				condition = split[1],
				value = split.splice(2).join(' '),
				where = '(',
				addValue = true;

			where += column + ' ';
			var values = value.split('|');
			//get the column based on the column dataIndex
			var col = {};
			Ext.each(this.fieldColumns, function(fieldColumn) {
				if (fieldColumn.dataIndex == column) {
					col = fieldColumn;
					return false;
				}
			});
			Ext.each(values, function(value, index) {
				if (value) {
					if (index > 0) where += 'OR ' + column + ' ';
					if (col.dataType == 'bool') {
						value = value === 'true' ? '1' : '0';
					}
					//Get condition equivalent
					switch (condition.toLowerCase()) {
						case 'on':
							where += "BETWEEN (date '{0}') AND (date '{1}')";
							break;
						case 'after':
							where += "> (date '{0}')";
							break;
						case 'before':
							where += "< (date '{0}')";
							break;
						case 'equals':
							var eqCond = Ext.isNumber(Number(value)) ? '=' : "= '{0}'";
							if (value === 'NULL') {
								eqCond = 'IS NULL';
								addValue = false;
							}
							if (value === 'NIL') {
								eqCond = Ext.String.format("IS NULL OR {0} = ''", column);
								addValue = false;
							}
							where += eqCond;
							break;
						case 'notequals':
							var eqCond = Ext.isNumber(Number(value)) ? '<>' : "<> '{0}'";
							if (value === 'NULL') {
								eqCond = 'IS NOT NULL';
								addValue = false;
							}
							if (value === 'NIL') {
								eqCond = Ext.String.format("IS NOT NULL OR {0} <> ''", column);
								addValue = false;
							}
							where += eqCond;
							break;
						case 'contains':
							where += "LIKE '%{0}%'";
							break;
						case 'notcontains':
							where += "NOT LIKE '%{0}%'";
							break;
						case 'startswith':
							where += "LIKE '{0}%'";
							break;
						case 'notstartswith':
							where += "NOT LIKE '{0}%'";
							break;
						case 'endswith':
							where += "LIKE '%{0}'";
							break;
						case 'notendswith':
							where += "NOT LIKE '%{0}'";
							break;
						case 'greaterthan':
							where += '> {0}';
							break;
						case 'greaterthanorequalto':
							where += '>= {0}';
							break;
						case 'lessthan':
							where += '< {0}';
							break;
						case 'lessthanorequalto':
							where += '<= {0}';
							break;
						default:
							where += '=';
					}

					where += ' ';

					switch (value) {
						case 'today':
							where = Ext.String.format(where, Ext.Date.format(new Date(), this.dateFormat), Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, 1), this.dateFormat));
							break;
						case 'yesterday':
							where = Ext.String.format(where, Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, -1), this.dateFormat), Ext.Date.format(Ext.Date.format(new Date(), this.dateFormat)));
							break;
						case 'lastweek':
							where = Ext.String.format(where, Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.WEEK, -1), this.dateFormat), Ext.Date.format(Ext.Date.format(new Date(), this.dateFormat)));
							break;
						case 'lastmonth':
							where = Ext.String.format(where, Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.MONTH, -1), this.dateFormat), Ext.Date.format(Ext.Date.format(new Date(), this.dateFormat)));
							break;
						case 'lastyear':
							where = Ext.String.format(where, Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, -1), this.dateFormat), Ext.Date.format(Ext.Date.format(new Date(), this.dateFormat)));
							break;
						default:
							if (addValue) {
								var val = value,
									secondVal = '';
								if (col.dataType == 'date') {
									val = Ext.Date.parse(value, this.dateFormat)
									if (!Ext.isDate(val)) val = Ext.Date.parse(value, 'c');
									if (Ext.isDate(val)) {
										value = Ext.Date.format(val, this.submitFormat);
										secondVal = Ext.Date.format(Ext.Date.add(val, Ext.Date.DAY, 1), this.submitFormat);
									}
								}

								if (where.indexOf('{0}') > -1) where = Ext.String.format(where, value.replace(/'/g, "''"), secondVal.replace(/'/g, "''"));
								else where += value.replace(/'/g, "''");
							}
					}
				}
			}, this);

			whereClause += (index > 0 ? ' AND ' : ' ') + where + ')';
		}, this);
		return whereClause;
	},
	getOptionsForDataType: function(column) {
		var me = this,
			options = [];
		switch (column.dataType) {
			case 'date':
				options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.on),
					value: RS.common.locale.on,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.on)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.before),
					value: RS.common.locale.before,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.before)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.after),
					value: RS.common.locale.after,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.after)
				}];
				break;
			case 'float':
			case 'int':
				options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.equals),
					value: RS.common.locale.equals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.equals)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notEquals),
					value: RS.common.locale.notEquals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notEquals)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.greaterThan),
					value: RS.common.locale.greaterThan,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.greaterThan)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.greaterThanOrEqualTo),
					value: RS.common.locale.greaterThanOrEqualTo,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.greaterThanOrEqualTo)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.lessThan),
					value: RS.common.locale.lessThan,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.lessThan)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.lessThanOrEqualTo),
					value: RS.common.locale.lessThanOrEqualTo,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.lessThanOrEqualTo)
				}];
				break;
			case 'bool':
				options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.equals),
					value: RS.common.locale.equals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.equals)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notEquals),
					value: RS.common.locale.notEquals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notEquals)
				}];
				break;
			default:
				options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.equals),
					value: RS.common.locale.equals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.equals)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notEquals),
					value: RS.common.locale.notEquals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notEquals)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.contains),
					value: RS.common.locale.contains,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.contains)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notContains),
					value: RS.common.locale.notContains,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notContains)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.startsWith),
					value: RS.common.locale.startsWith,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.startsWith)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notStartsWith),
					value: RS.common.locale.notStartsWith,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notStartsWith)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.endsWith),
					value: RS.common.locale.endsWith,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.endsWith)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notEndsWith),
					value: RS.common.locale.notEndsWith,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notEndsWith)
				}];
				if (!me.showComparisons) options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.equals),
					value: RS.common.locale.equals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.equals)
				}]
				break;
		}
		return options;
	},
	getShortOptionsForDataType: function(type) {
		var options = [];
		switch (type) {
			case 'date':
				options = [{
					name: Ext.String.format('{0}', RS.common.locale.on),
					value: Ext.String.format('{0}', RS.common.locale.on)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.before),
					value: Ext.String.format('{0}', RS.common.locale.before)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.after),
					value: Ext.String.format('{0}', RS.common.locale.after)
				}];
				break;
			case 'float':
			case 'int':
				options = [{
					name: Ext.String.format('{0}', RS.common.locale.shortEquals),
					value: Ext.String.format('{0}', RS.common.locale.shortEquals)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortNotEquals),
					value: Ext.String.format('{0}', RS.common.locale.shortNotEquals)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortGreaterThan),
					value: Ext.String.format('{0}', RS.common.locale.shortGreaterThan)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortGreaterThanOrEqualTo),
					value: Ext.String.format('{0}', RS.common.locale.shortGreaterThanOrEqualTo)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortLessThan),
					value: Ext.String.format('{0}', RS.common.locale.shortLessThan)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortLessThanOrEqualTo),
					value: Ext.String.format('{0}', RS.common.locale.shortLessThanOrEqualTo)
				}];
				break;
			case 'bool':
				options = [{
					name: Ext.String.format('{0}', RS.common.locale.equals),
					value: Ext.String.format('{0}', RS.common.locale.equals)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notEquals),
					value: Ext.String.format('{0}', RS.common.locale.notEquals)
				}];
				break;
			default:
				options = [{
					name: Ext.String.format('{0}', RS.common.locale.equals),
					value: Ext.String.format('{0}', RS.common.locale.equals)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notEquals),
					value: Ext.String.format('{0}', RS.common.locale.notEquals)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.contains),
					value: Ext.String.format('{0}', RS.common.locale.contains)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notContains),
					value: Ext.String.format('{0}', RS.common.locale.notContains)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.startsWith),
					value: Ext.String.format('{0}', RS.common.locale.startsWith)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notStartsWith),
					value: Ext.String.format('{0}', RS.common.locale.notStartsWith)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.endsWith),
					value: Ext.String.format('{0}', RS.common.locale.endsWith)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notEndsWith),
					value: Ext.String.format('{0}', RS.common.locale.notEndsWith)
				}];
		}
		return options;
	},
	getValueOptionsForDataType: function(column, columnText, operatorText, dataValue) {
		var options = [],
			returnOptions = [];
		switch (column.dataType) {
			case 'date':
				options = [{
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.today),
					value: RS.common.locale.today,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.today)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.yesterday),
					value: RS.common.locale.yesterday,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.yesterday)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.lastWeek),
					value: RS.common.locale.lastWeek,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.lastWeek)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.lastMonth),
					value: RS.common.locale.lastMonth,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.lastMonth)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.lastYear),
					value: RS.common.locale.lastYear,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.lastYear)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, Ext.Date.format(new Date(), this.dateFormat)),
					value: Ext.Date.format(new Date(), this.dateFormat),
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, Ext.Date.format(new Date(), this.dateFormat))
				}];
				break;
		}
		Ext.each(options, function(option) {
			if (option.value.indexOf(dataValue) > -1) returnOptions.push(option);
		});
		return returnOptions;
	},
	getFiltersString: function() {
		var filters = this.filterBar.getFilters(),
			filterString = '';
		Ext.each(filters, function(filter, index) {
			if (index > 0) filterString += '|&&|';
			filterString += filter.replace(/ /g, '^')
		});
		return filterString;
	}
});