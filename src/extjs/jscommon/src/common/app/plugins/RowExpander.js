Ext.define('RS.common.plugin.RowExpander', {
	extend: 'Ext.grid.plugin.RowExpander',
	alias: 'plugin.resolveexpander',
	expandImg: '<img src="/resolve/images/group-expand.png"/>',
	collapseImg: '<img src="/resolve/images/group-collapse.png"/>',
	batchToggling: false,
	currentWidth: 500,
	expanderIndex: 1,
	init: function(grid) {
		var me = this;
		this.callParent([grid]);
		grid.addListener('render', function() {
			grid.on('cellclick', function(view, td, cellIndex, record, tr, rowIndex, e, eOpts) {
				if (cellIndex > me.expanderIndex) {
					me.onDblClick(view, record, null, rowIndex, e)
				}
			})
		});
		grid.addListener('afterlayout', function() {
			if (!grid.rendered)
				return;
			var divs = Ext.query('div[resolveId]')
			Ext.each(divs, function(div) {
				Ext.get(div).setWidth(grid.getWidth() - 50);
			});
		});
		var view = grid.getView();
		view.addListener('expandbody', function() {
			me.replaceHeaderNoFurtherAction();
		});
		view.addListener('collapsebody', function() {
			me.replaceHeaderNoFurtherAction();
		});
		grid.getStore().on('load', function() {
			this.recordsExpanded = {};
			this.replaceHeaderNoFurtherAction();
		}, this);
	},
	toggleAll: function() {
		var anyExpanded = this.replaceHeaderNoFurtherAction();

		var me = this,
			view = me.view,
			addOrRemoveCls = !anyExpanded ? 'removeCls' : 'addCls',
			ownerLock, rowHeight, fireView, rowNode, row, nextBd;
		// Suspend layouts because of possible TWO views having their height change
		Ext.suspendLayouts();

		var store = this.grid.getStore();
		store.each(function(r) {
			var idx = store.indexOf(r);
			rowNode = view.getNode(idx);
			row = Ext.fly(rowNode, '_rowExpander');
			nextBd = row.down(me.rowBodyTrSelector, true);
			row[addOrRemoveCls](me.rowCollapsedCls);
			Ext.fly(nextBd)[addOrRemoveCls](me.rowBodyHiddenCls);
			me.recordsExpanded[r.internalId] = !anyExpanded;
			view.refreshSize();
			// Sync the height and class of the row on the locked side
			if (me.grid.ownerLockable) {
				ownerLock = me.grid.ownerLockable;
				fireView = ownerLock.getView();
				view = ownerLock.lockedGrid.view;
				rowHeight = row.getHeight();
				// EXTJSIV-9848: in Firefox the offsetHeight of a row may not match
				// it's actual rendered height due to sub-pixel rounding errors. To ensure
				// the rows heights on both sides of the grid are the same, we have to set
				// them both.
				row.setHeight(anyExpanded ? rowHeight : '');
				row = Ext.fly(view.getNode(rowIdx), '_rowExpander');
				row.setHeight(anyExpanded ? rowHeight : '');
				row[addOrRemoveCls](me.rowCollapsedCls);
				view.refreshSize();
			} else {
				fireView = view;
			}
		}, this);

		if (fireView) {
			fireView.fireEvent(anyExpanded ? 'expandbody' : 'collapsebody', null, null, null);
		}
		// Coalesce laying out due to view size changes
		Ext.resumeLayouts(true);
		this.replaceHeaderNoFurtherAction();
	},
	addExpander: function() {
		var me = this,
			expanderGrid = me.grid,
			expanderHeader = me.getHeaderConfig();
		expanderHeader.text = this.expandImg;
		expanderHeader.width = 35;
		// If this is the normal side of a lockable grid, find the other side.
		if (expanderGrid.ownerLockable) {
			expanderGrid = expanderGrid.ownerLockable.lockedGrid;
			expanderGrid.width += expanderHeader.width;
		}
		expanderGrid.headerCt.insert(0, expanderHeader);
		var header = expanderGrid.headerCt.getHeaderAtIndex(0);
		header.on('headerclick', this.toggleAll, this);
	},
	replaceHeaderNoFurtherAction: function() {
		var anyExpanded = false;
		for (id in this.recordsExpanded)
			anyExpanded = anyExpanded || this.recordsExpanded[id]
		header = this.grid.headerCt.getHeaderAtIndex(this.expanderIndex);
		header.width = 35;
		header.setText(this[anyExpanded ? 'collapseImg' : 'expandImg']);
		return anyExpanded;
	}
});