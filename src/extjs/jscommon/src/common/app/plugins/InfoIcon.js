Ext.define('RS.common.InfoIcon',{
	extend : 'Ext.container.Container',
	alias : 'widget.infoicon', 
    iconType : 'info',
    displayText : '',
	initComponent : function(){
        var iconCls = 'icon-info-sign';
        var iconStyle = {
            color: '#7be8fa',
            backgroundColor: '#e5f7fa',
            border: '1px solid #7be8fa',
            borderRadius : '3px',
            fontSize: '17px',
            textAlign : 'center'
        };
        if(this.iconType == 'warn'){
            iconCls  = 'icon-warning-sign';
            iconStyle = Ext.apply(iconStyle, {
                color: '#fab67b',
                backgroundColor: '#faf8ee',
                border: '1px solid #fab67b'             
           })
        }   
        Ext.apply(this, {
        	layout : {
        		type : 'hbox',
        		align : 'middle'
        	},
            items: [
            {
            	xtype : 'component',  
            	cls : iconCls,       	 
        	 	height: 30,
    			width: 30,
        		padding : 5,      	 
        	 	style: iconStyle
            },{
            	xtype : 'component',
            	margin : '0 0 0 10',
            	style : {
            		color : "#505050"
            	},
            	html : this.displayText
            }]
        });     
        this.callParent(arguments);
	}
})