Ext.define('RS.common.plugin.Pager', {
	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.pager',
	mixins: {
		bindable: 'Ext.util.Bindable'
	},

	pageable: true,
	allowAutoRefresh: true,
	showSysInfo: true,

	displayJumpMenu: true,

	autoRefreshInterval: 30,
	autoRefreshEnabled: false,

	showSocial: false,
	socialStreamType: '',

	init: function(grid) {
		var me = this,
			myGrid = grid;

		me.grid = myGrid

		if (me.grid.hidePager === true) {
			return
		}

		this.autoRefreshInterval = Ext.state.Manager.get('autoRefreshInterval', 30)
		this.autoRefreshEnabled = Ext.state.Manager.get('autoRefreshEnabled', false)


		me.displayMsg = RS.common.locale.pageDisplayText
		me.firstText = RS.common.locale.firstText
		me.prevText = RS.common.locale.prevText
		me.nextText = RS.common.locale.nextText
		me.lastText = RS.common.locale.lastText
		me.refreshText = RS.common.locale.refreshText
		me.configureText = RS.common.locale.configureText
		me.emptyMsg = RS.common.locale.emptyMsg
		me.systemInformation = RS.common.locale.sysInfoButtonTooltip
		me.systemInformationTooltip = RS.common.locale.sysInfoButtonTitle
		me.goToText = RS.common.locale.goToText

		//Auto refresh
		me.autoRefreshText = RS.common.locale.autoRefreshText;

		myGrid.dockedItems.each(function(dockedItem) {
			if (dockedItem.name == 'actionBar') {
				me.toolbar = dockedItem
				return false
			}
		})

		if (!myGrid.isXType('grid')) myGrid = myGrid.down('grid')

		if (me.pageable) myGrid.store.pageSize = myGrid.store.pageSize || 50
		else myGrid.store.pageSize = -1

		if (me.toolbar) {
			// me.toolbar.insert(0, [{
			// 	itemId: 'setup',
			// 	tooltip: me.configureText,
			// 	overflowText: me.configureText,
			// 	iconCls: Ext.baseCSSPrefix + 'tbar-configure',
			// 	scope: me,
			// 	menu: [{
			// 		text: 'Views'
			// 	}, {
			// 		text: 'Manage'
			// 	}, {
			// 		text: 'Admin Views'
			// 	},{text: 'Save Current As'}]
			// }, '-', ' ', ' ']);

			me.toolbar.add(['->'])

			if (me.pageable) me.toolbar.add([{
				xtype: 'tbtext',
				itemId: 'displayItem',
				listeners: {
					render: function(tbtext) {
						if (me.displayJumpMenu) {
							tbtext.getEl().on('click', function() {
								if (!tbtext.menu) {
									tbtext.menu = Ext.create('Ext.menu.Menu', {
										items: [{
											xtype: 'numberfield',
											itemId: 'pageNumberField',
											fieldLabel: me.goToText,
											labelWidth: 45,
											width: 120,
											minValue: 1,
											listeners: {
												change: function(field, newValue) {
													if (Ext.isNumber(newValue) && newValue > 0 && me.store.currentPage != newValue && newValue <= me.getPageData().pageCount)
														me.store.loadPage(newValue)
												}
											}
										}]
									})
								}
								tbtext.menu.down('#pageNumberField').setValue(me.store.currentPage)
								tbtext.menu.down('#pageNumberField').setMaxValue(me.getPageData().pageCount)
								tbtext.menu.showBy(tbtext)
							})
						}
					}
				}
			}, {
				itemId: 'first',
				tooltip: me.firstText,
				overflowText: me.firstText,
				iconCls: Ext.baseCSSPrefix + 'tbar-page-first',
				disabled: true,
				handler: me.moveFirst,
				scope: me
			}, {
				itemId: 'prev',
				tooltip: me.prevText,
				overflowText: me.prevText,
				iconCls: Ext.baseCSSPrefix + 'tbar-page-prev',
				disabled: true,
				handler: me.movePrevious,
				scope: me
			}, {
				itemId: 'next',
				tooltip: me.nextText,
				overflowText: me.nextText,
				iconCls: Ext.baseCSSPrefix + 'tbar-page-next',
				disabled: true,
				handler: me.moveNext,
				scope: me
			}, {
				itemId: 'last',
				tooltip: me.lastText,
				overflowText: me.lastText,
				iconCls: Ext.baseCSSPrefix + 'tbar-page-last',
				disabled: true,
				handler: me.moveLast,
				scope: me
			}, ' ', ' '])

			if (me.showSocial) {
				var socialButton = -1,
					i = 0,
					len = me.toolbar.items.length;
				for (; i < len; i++) {
					if (me.toolbar.items.getAt(i).iconCls && me.toolbar.items.getAt(i).iconCls.indexOf('rs-social-button') > -1) {
						socialButton = i
					}
				}

				if (socialButton > -1)
					me.toolbar.move(socialButton, me.toolbar.items.length)

				me.toolbar.add({
					xtype: 'followbutton',
					itemId: 'followButton',
					streamType: me.socialStreamType
				})
			}

			if (me.showSysInfo) {
				var showSysInfoButton = {
					xtype: 'button',
					ui: 'system-info-button-small',
					iconCls: 'rs-icon-button icon-info-sign' + (Ext.isGecko ? ' rs-icon-firefox' : ''),
					tooltip: me.systemInformation,
					overflowText: me.systemInformationTooltip,
					enableToggle: true,
					pressed: Ext.state.Manager.get('userShowSysInfoColumns', false),
					handler: me.toggleSysColumns,
					scope: myGrid
				}
				me.toolbar.add(showSysInfoButton)
				me.toggleSysColumns.apply(myGrid, [showSysInfoButton])
			}

			me.toolbar.add({
				xtype: me.allowAutoRefresh ? 'splitbutton' : 'button',
				itemId: 'refresh',
				tooltip: me.refreshText,
				overflowText: me.refreshText,
				iconCls: Ext.baseCSSPrefix + 'tbar-loading',
				handler: me.doRefresh,
				scope: me,
				menu: me.allowAutoRefresh ? {
					width: 175,
					items: [{
						text: me.autoRefreshText,
						checked: this.autoRefreshEnabled,
						checkHandler: function(item, checked) {
							var interval = item.ownerCt.down('numberfield').getValue();
							if (checked) {
								me.startAutoRefresh(interval || this.autoRefreshInterval)
								Ext.state.Manager.set('autoRefreshEnabled', true)
							} else {
								me.stopAutoRefresh()
								Ext.state.Manager.set('autoRefreshEnabled', false)
							}
						}
					}, {
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [{
							xtype: 'numberfield',
							padding: '5 5 7 60',
							hideLabel: true,
							value: this.autoRefreshInterval,
							width: 60,
							minValue: 10,
							listeners: {
								change: function(field, value) {
									me.changeInterval(value);
								},
								blur: function(field, value) {
									if (this.getValue() < 10)
										this.setValue(10);
								}
							}
						}]
					}]
				} : null,
				listeners: {
					render: function(button) {
						if (!me.notActivateCtrl_R) {
							clientVM.updateRefreshButtons(button);
						}
					}
				}
			});

			if (this.autoRefreshEnabled) me.startAutoRefresh(this.autoRefreshInterval)

			me.toolbar.addEvents(
				/**
				 * @event change
				 * Fires after the active page has been changed.
				 * @param {Ext.toolbar.Paging} this
				 * @param {Object} pageData An object that has these properties:
				 *
				 * - `total` : Number
				 *
				 *   The total number of records in the dataset as returned by the server
				 *
				 * - `currentPage` : Number
				 *
				 *   The current page number
				 *
				 * - `pageCount` : Number
				 *
				 *   The total number of pages (calculated from the total number of records in the dataset as returned by the
				 *   server and the current {@link Ext.data.Store#pageSize pageSize})
				 *
				 * - `toRecord` : Number
				 *
				 *   The starting record index for the current page
				 *
				 * - `fromRecord` : Number
				 *
				 *   The ending record index for the current page
				 */
				'change',

				/**
				 * @event beforechange
				 * Fires just before the active page is changed. Return false to prevent the active page from being changed.
				 * @param {Ext.toolbar.Paging} this
				 * @param {Number} page The page number that will be loaded on change
				 */
				'beforechange');

			me.toolbar.on('beforerender', me.onLoad, me, {
				single: true
			});

			me.bindStore(myGrid.store || 'ext-empty-store', true);
		}
	},

	// @private
	updateInfo: function() {
		var me = this,
			displayItem = me.toolbar.child('#displayItem'),
			store = me.store,
			pageData = me.getPageData(),
			count, msg;

		if (displayItem) {
			count = store.getCount();
			if (count === 0) {
				msg = me.emptyMsg;
			} else {
				msg = Ext.String.format(
					me.displayMsg, pageData.fromRecord, pageData.toRecord, pageData.total);
			}
			displayItem.setText(msg);
		}
	},

	// @private
	beforeLoad: function() {
		if (this.rendered && this.refresh) {
			this.refresh.disable();
		}
	},

	// @private
	onLoadError: function() {
		if (!this.rendered) {
			return;
		}
		this.child('#refresh').enable();
	},

	// @private
	onLoad: function() {
		var me = this,
			pageData, currPage, pageCount, afterText, count, isEmpty;

		count = me.store.getCount();
		isEmpty = count === 0;
		if (!isEmpty) {
			pageData = me.getPageData();
			currPage = pageData.currentPage;
			pageCount = pageData.pageCount;
		} else {
			currPage = 0;
			pageCount = 0;
		}

		if (me.pageable) {
			Ext.suspendLayouts();
			me.toolbar.child('#first').setDisabled(currPage === 1 || isEmpty);
			me.toolbar.child('#prev').setDisabled(currPage === 1 || isEmpty);
			me.toolbar.child('#next').setDisabled(currPage === pageCount || isEmpty);
			me.toolbar.child('#last').setDisabled(currPage === pageCount || isEmpty);
			me.toolbar.child('#refresh').enable();
			me.updateInfo();
			Ext.resumeLayouts(true);
		} else {
			me.toolbar.child('#refresh').enable();
			me.updateInfo();
		}

		if (me.toolbar.rendered) {
			me.toolbar.fireEvent('change', me, pageData);
		}
	},

	// @private
	getPageData: function() {
		var store = this.store,
			totalCount = store.getTotalCount();

		return {
			total: totalCount,
			currentPage: store.currentPage,
			pageCount: Math.ceil(totalCount / store.pageSize),
			fromRecord: ((store.currentPage - 1) * store.pageSize) + 1,
			toRecord: Math.min(store.currentPage * store.pageSize, totalCount)

		};
	},

	/**
	 * Move to the first page, has the same effect as clicking the 'first' button.
	 */
	moveFirst: function() {
		var me = this;
		if (me.toolbar.fireEvent('beforechange', me, 1) !== false) {
			me.store.loadPage(1);
		}
	},

	/**
	 * Move to the previous page, has the same effect as clicking the 'previous' button.
	 */
	movePrevious: function() {
		var me = this,
			prev = me.store.currentPage - 1;

		if (prev > 0) {
			if (me.toolbar.fireEvent('beforechange', me, prev) !== false) {
				me.store.previousPage();
			}
		}
	},

	/**
	 * Move to the next page, has the same effect as clicking the 'next' button.
	 */
	moveNext: function() {
		var me = this,
			total = me.getPageData().pageCount,
			next = me.store.currentPage + 1;

		if (next <= total) {
			if (me.toolbar.fireEvent('beforechange', me, next) !== false) {
				me.store.nextPage();
			}
		}
	},

	/**
	 * Move to the last page, has the same effect as clicking the 'last' button.
	 */
	moveLast: function() {
		var me = this,
			last = me.getPageData().pageCount;

		if (me.toolbar.fireEvent('beforechange', me, last) !== false) {
			me.store.loadPage(last);
		}
	},

	/**
	 * Refresh the current page, has the same effect as clicking the 'refresh' button.
	 */
	doRefresh: function() {
		var me = this,
			current = me.store.currentPage;

		if (me.toolbar.fireEvent('beforechange', me, current) !== false) {
			me.store.loadPage(current);
		}
	},

	getStoreListeners: function() {
		return {
			beforeload: this.beforeLoad,
			load: this.onLoad,
			exception: this.onLoadError
		};
	},

	/**
	 * Unbinds the paging toolbar from the specified {@link Ext.data.Store} **(deprecated)**
	 * @param {Ext.data.Store} store The data store to unbind
	 */
	unbind: function(store) {
		this.bindStore(null);
	},

	/**
	 * Binds the paging toolbar to the specified {@link Ext.data.Store} **(deprecated)**
	 * @param {Ext.data.Store} store The data store to bind
	 */
	bind: function(store) {
		this.bindStore(store);
	},

	// @private
	destroy: function() {
		this.stopAutoRefresh()
		this.unbind()
		this.callParent()
	},

	changeInterval: function(interval) {
		if (this.autoRefreshTask) {
			this.stopAutoRefresh()
			this.startAutoRefresh(interval)
		}
		Ext.state.Manager.set('autoRefreshInterval', interval < 10 ? 10 : interval);
	},

	startAutoRefresh: function(interval) {
		if (this.autoRefreshTask) this.autoRefreshTask.destroy();

		var runner = new Ext.util.TaskRunner();
		this.autoRefreshTask = runner.newTask({
			run: this.doRefresh,
			scope: this,
			interval: interval * 1000
		});
		this.autoRefreshTask.start();
	},
	stopAutoRefresh: function() {
		if (this.autoRefreshTask) {
			this.autoRefreshTask.destroy();
			delete this.autoRefreshTask;
		}
	},

	toggleSysColumns: function(button) {
		Ext.state.Manager.set('userShowSysInfoColumns', button.pressed)
		Ext.Array.forEach(this.columns, function(column) {
			if ((column.dataIndex && column.dataIndex.indexOf('sys') == 0 && !column.initialShow) || column.dataIndex == 'id') {
				if (column.rendered)
					column[button.pressed ? 'show' : 'hide']()
				else column.hidden = !button.pressed
			} else if (column.dataIndex && Ext.Array.indexOf(['processNumber', 'duration', 'address', 'targetGUID', 'esbaddr'], column.dataIndex) > -1) {
				//This is for worksheet columns.  Total hack, but Duke wants these columns hidden/shown based on the sysinfo button too
				if (column.rendered)
					column[button.pressed ? 'show' : 'hide']()
				else column.hidden = !button.pressed
			}
		})
	},

	setSocialId: function(value) {
		this.toolbar.down('#followButton').streamId = value
	},
	setSocialStreamType: function(value) {
		this.toolbar.down('#followButton').streamType = value
	}
});