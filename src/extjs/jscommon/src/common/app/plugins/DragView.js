Ext.define("RS.common.plugin.DragView", (function () { 
	var dragCls = 'x-view-sortable-drag',
		viewDragCls = 'x-view-sortable-dragging',
		dragData = null,
		view = null;

	function onMouseDown(e) {
		var result = false;
		dragData = null;

		try {
			var t = e.getTarget(view.itemSelector);
			var i = view.indexOf(t);
			var record = view.store.getAt(i);

			if (t && record) {
				dragData = {
					originalIndex: i,
					lastIndex: i,
					record: record
				};

				result = true;
			}
		} catch (ex) { 
			dragData = null;
		}

		return result;
	}

	function startDrag (x, y) {
		if (dragData) {
			var node = view.getNode(dragData.originalIndex);
			Ext.fly(node).addClass(dragCls);
			view.el.addClass(viewDragCls);
		}
	}

	function onDrag (e) {
		var result = false;

		if (dragData) { 
			try {
				var t = e.getTarget(view.itemSelector);

				if (t) {				
					var i = view.indexOf(t);

					if (i !== data.lastIndex) { 
						var store = view.store;
						var record = store.getAt(i);

						if (record) {
							dragData.lastIndex = i;
							store.remove(dragData.record);
							store.insert(i, [dragData.record]);
							var node = view.getNode(i);
							Ext.fly(node).addClass(dragCls);
							result = true;
						}
					}
				}
			} catch (ex) { 
				result = false; 
			}
		}

		return false;
	}

	function endDrag (e) {
		if (dragData) { 
			var node = view.getNode(dragData.lastIndex);
			Ext.fly(node).removeClass(dragCls);
			view.el.removeClass(viewDragCls);
			this.fireEvent('drop', dragData.origanlIndex, dragData.lastIndex, dragData.record);
		}

		return true;
	}

	return {
		extend: 'Ext.util.Observable',
		alias: 'plugin.dragview',
		events: {
			drop: true
		},

		dragDrop: null,

		init: function (v) {
			view = v;			
			view.on('render', this.onRender, this);
		},

		onRender : function() {
			this.callParent(arguments);
			this.dragDrop = new Ext.dd.DragDrop(view.el);
			this.dragDrop.onMouseDown = onMouseDown;
			this.dragDrop.startDrag = startDrag;
			this.dragDrop.onDrag = onDrag;
			this.dragDrop.endDrag = endDrag.bind(this);
		}		
	};
})());