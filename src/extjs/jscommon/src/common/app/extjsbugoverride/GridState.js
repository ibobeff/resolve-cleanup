//https: //www.sencha.com/forum/showthread.php?282218-ExtJS-4.2.2-Grid-state-issue-with-multiple-grids-of-the-same-type 
Ext.override(Ext.grid.column.Column, {
	getStateId: function() {
		return this.stateId || this.dataIndex || this.headerId;
	}
});