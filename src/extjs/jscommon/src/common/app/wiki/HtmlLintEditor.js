Ext.define('RS.common.HtmlLintEditor', {
    extend: 'Ext.form.HtmlEditor',
    xtype: 'wysiwyg',
    defaultValue: '',
	enableSourceEdit: false,
    defaultFont: 'Tahoma',
    enableUpload: true,
    regListComponents: new RegExp(/<p class="MsoList(?:Bullet|ParagraphCxSpFirst|ParagraphCxSpMiddle|ParagraphCxSpLast)"(?: style="([\s\S]+?)")?>(?:<!--\[if !supportLists\]-->([\s\S]*?)<!--\[endif\]-->|([\s\S]*?)<\/span>[\s\S]*?)(?:<\/span>)?([\s\S]*?)<\/p>[\n\s]{0,3}/gim),
    regListRelativeIndentation: new RegExp(/(?:text-indent:([-]?\d{0,2}?\.?\d{0,2})in;?)?(?:margin-left:([-]?\d{0,2}?\.?\d{0,2})in;?)?(?:tab-stops:([-]?\d{0,2}?\.?\d{0,2})in;?)?/g),
    regListExplicitIndentation: new RegExp(/mso-list:l\d{1} level(\d{1,2}) lfo(\d{1,2}?)/g),
    regListBullet: new RegExp(/([·o§Øv])/),
    regPoint: new RegExp(/((\d{1,3})\.(\d{1,2})pt)/g),
    regBorder: new RegExp(/(mso-element:para-border-div;(?!margin-bottom:10px))/gi),
    regReturn: new RegExp(/[\r\n]/g),
    changeFired: false,
    dirtyHtmlTags: [
        {regex: /<\\?\?xml[^>]*?>/gi, replaceVal: ''},
        {regex: /\s*PAGE[-:][^;"']*?/gi, replaceVal: ''},
        {regex: /\s*TAB[-:][^;"']*?/gi, replaceVal: '&nbsp;&nbsp;&nbsp;&nbsp;'},
        {regex: /\n{1,2}/g, replaceVal: ' ' },
        {regex: /<(\/?title|\/?meta|\/?head|\/?html|\/?body)[^>]*?>/gi, replaceVal: ''},
        {regex: /<o:p><\/o:p>/g, replaceVal: '' },
        {regex: /—/g, replaceVal: ' - '},
        {regex: /&zwj;/g, replaceVal:'' },
        {regex: /\u200d/g, replaceVal:'' },
        {regex: new RegExp(String.fromCharCode(161), 'g'), replaceVal: '&iexcl;'},
        {regex: new RegExp(String.fromCharCode(162), 'g'), replaceVal: '&cent;'},
        {regex: new RegExp(String.fromCharCode(163), 'g'), replaceVal: '&pound;'},
        {regex: new RegExp(String.fromCharCode(164), 'g'), replaceVal: '&curren;'},
        {regex: new RegExp(String.fromCharCode(165), 'g'), replaceVal: '&yen;'},
        {regex: new RegExp(String.fromCharCode(166), 'g'), replaceVal: '&brvbar;'},
        {regex: new RegExp(String.fromCharCode(167), 'g'), replaceVal: '&sect;'},
        {regex: new RegExp(String.fromCharCode(168), 'g'), replaceVal: '&uml;'},
        {regex: new RegExp(String.fromCharCode(169), 'g'), replaceVal: '&copy;'},
        {regex: new RegExp(String.fromCharCode(170), 'g'), replaceVal: '&ordf;'},
        {regex: new RegExp(String.fromCharCode(171), 'g'), replaceVal: '&laquo;'},
        {regex: new RegExp(String.fromCharCode(172), 'g'), replaceVal: '&not;'},
        {regex: new RegExp(String.fromCharCode(173), 'g'), replaceVal: '&shy;'},
        {regex: new RegExp(String.fromCharCode(174), 'g'), replaceVal: '&reg;'},
        {regex: new RegExp(String.fromCharCode(175), 'g'), replaceVal: '&macr;'},
        {regex: new RegExp(String.fromCharCode(176), 'g'), replaceVal: '&deg;'},
        {regex: new RegExp(String.fromCharCode(177), 'g'), replaceVal: '&plusmn;'},
        {regex: new RegExp(String.fromCharCode(178), 'g'), replaceVal: '&sup2;'},
        {regex: new RegExp(String.fromCharCode(179), 'g'), replaceVal: '&sup3;'},
        {regex: new RegExp(String.fromCharCode(180), 'g'), replaceVal: '&acute;'},
        {regex: new RegExp(String.fromCharCode(181), 'g'), replaceVal: '&micro;'},
        {regex: new RegExp(String.fromCharCode(182), 'g'), replaceVal: '&para;'},
        {regex: new RegExp(String.fromCharCode(183), 'g'), replaceVal: '&middot;'},
        {regex: new RegExp(String.fromCharCode(184), 'g'), replaceVal: '&cedil;'},
        {regex: new RegExp(String.fromCharCode(185), 'g'), replaceVal: '&sup1;'},
        {regex: new RegExp(String.fromCharCode(186), 'g'), replaceVal: '&ordm;'},
        {regex: new RegExp(String.fromCharCode(187), 'g'), replaceVal: '&raquo;'},
        {regex: new RegExp(String.fromCharCode(188), 'g'), replaceVal: '&frac14;'},
        {regex: new RegExp(String.fromCharCode(189), 'g'), replaceVal: '&frac12;'},
        {regex: new RegExp(String.fromCharCode(190), 'g'), replaceVal: '&frac34;'},
        {regex: new RegExp(String.fromCharCode(191), 'g'), replaceVal: '&iquest;'},
        {regex: new RegExp(String.fromCharCode(192), 'g'), replaceVal: '&Agrave;'},
        {regex: new RegExp(String.fromCharCode(193), 'g'), replaceVal: '&Aacute;'},
        {regex: new RegExp(String.fromCharCode(194), 'g'), replaceVal: '&Acirc;'},
        {regex: new RegExp(String.fromCharCode(195), 'g'), replaceVal: '&Atilde;'},
        {regex: new RegExp(String.fromCharCode(196), 'g'), replaceVal: '&Auml;'},
        {regex: new RegExp(String.fromCharCode(197), 'g'), replaceVal: '&Aring;'},
        {regex: new RegExp(String.fromCharCode(198), 'g'), replaceVal: '&AElig;'},
        {regex: new RegExp(String.fromCharCode(199), 'g'), replaceVal: '&Ccedil;'},
        {regex: new RegExp(String.fromCharCode(200), 'g'), replaceVal: '&Egrave;'},
        {regex: new RegExp(String.fromCharCode(201), 'g'), replaceVal: '&Eacute;'},
        {regex: new RegExp(String.fromCharCode(202), 'g'), replaceVal: '&Ecirc;'},
        {regex: new RegExp(String.fromCharCode(203), 'g'), replaceVal: '&Euml;'},
        {regex: new RegExp(String.fromCharCode(204), 'g'), replaceVal: '&Igrave;'},
        {regex: new RegExp(String.fromCharCode(205), 'g'), replaceVal: '&Iacute;'},
        {regex: new RegExp(String.fromCharCode(206), 'g'), replaceVal: '&Icirc;'},
        {regex: new RegExp(String.fromCharCode(207), 'g'), replaceVal: '&Iuml;'},
        {regex: new RegExp(String.fromCharCode(208), 'g'), replaceVal: '&ETH;'},
        {regex: new RegExp(String.fromCharCode(209), 'g'), replaceVal: '&Ntilde;'},
        {regex: new RegExp(String.fromCharCode(210), 'g'), replaceVal: '&Ograve;'},
        {regex: new RegExp(String.fromCharCode(211), 'g'), replaceVal: '&Oacute;'},
        {regex: new RegExp(String.fromCharCode(212), 'g'), replaceVal: '&Ocirc;'},
        {regex: new RegExp(String.fromCharCode(213), 'g'), replaceVal: '&Otilde;'},
        {regex: new RegExp(String.fromCharCode(214), 'g'), replaceVal: '&Ouml;'},
        {regex: new RegExp(String.fromCharCode(215), 'g'), replaceVal: '&times;'},
        {regex: new RegExp(String.fromCharCode(216), 'g'), replaceVal: '&Oslash;'},
        {regex: new RegExp(String.fromCharCode(217), 'g'), replaceVal: '&Ugrave;'},
        {regex: new RegExp(String.fromCharCode(218), 'g'), replaceVal: '&Uacute;'},
        {regex: new RegExp(String.fromCharCode(219), 'g'), replaceVal: '&Ucirc;'},
        {regex: new RegExp(String.fromCharCode(220), 'g'), replaceVal: '&Uuml;'},
        {regex: new RegExp(String.fromCharCode(221), 'g'), replaceVal: '&Yacute;'},
        {regex: new RegExp(String.fromCharCode(222), 'g'), replaceVal: '&THORN;'},
        {regex: new RegExp(String.fromCharCode(223), 'g'), replaceVal: '&szlig;'},
        {regex: new RegExp(String.fromCharCode(8205), 'g'), replaceVal: ''},
        {regex: new RegExp(String.fromCharCode(8220), 'g'), replaceVal: '"'},
        {regex: new RegExp(String.fromCharCode(8221), 'g'), replaceVal: '"'},
        {regex: new RegExp(String.fromCharCode(8216), 'g'), replaceVal: "'"},
        {regex: new RegExp(String.fromCharCode(8217), 'g'), replaceVal: "'"},
        {regex: new RegExp(String.fromCharCode(8211), 'g'), replaceVal: '-'},
        {regex: new RegExp(String.fromCharCode(8212), 'g'), replaceVal: '--'},
        {regex: new RegExp(String.fromCharCode(8230), 'g'), replaceVal: '&hellip;'}
    ],

    initEditor: function () {
        this.mostRecentEventType = '';
        RS.common.HtmlLintEditor.superclass.initEditor.call(this);
        this.textareaEl.dom.setAttribute("contenteditable", "true");
        var doc = this.getDoc();
        var fn = Ext.Function.bind(this.onEditorEvent, this);
        Ext.EventManager.on(doc, {
            blur: fn,
            focusout: fn
        });
        var me = this;

        if (this.enableUpload) {
            this.getToolbar().add({
                iconCls: 'x-import-worddoc',
                tooltip: RS.common.locale.uploadWordDocument,
                hidden: this._vm.rootVM.sir,
                handler: function() {
					/*
                    Ext.create('Ext.window.Window', {
                        title: RS.common.locale.uploadAWordDocument,
                        modal: true,
                        layout: 'fit',
                        items: {
                            xtype: 'rsfileupload',
                            callback: me.appendHtmlValueAfterUpload,
                            url: '/resolve/service/wiki/docx/uploadDocx',
                            autoUpload: true,
                            multipart_params: {
                            wikiID: me._vm.rootVM.id,
                            wikiName: me._vm.rootVM.name,
                            userName: clientVM.username
                            },
                            filters: {
                                mime_types: [{
                                    title: RS.common.locale.uploadWordDocument,
                                    extensions: 'docx'
                                }],
                                special_chars_not_allowed: "&|\\*|\\^|~|'"   // set of chars separated by '|' if more than one. For example '&|#|@'
                            },
                            view: me
                        }
                    }).show();
					*/
					this._vm.open({
						mtype: 'RS.common.UploadWordDoc',
						editor: this
					});
                }.bind(this)
            });
        }
    },

    appendHtmlValueAfterUpload: function(response) {
        var htmlContents = response.data.docxContent.htmlContent;

        for (var i=0; i<htmlContents.length; i++) {
            this.setValue(this.getValue() + htmlContents[i]);
        }
    },

    fontFamilies: ['Arial','Calibri','Cambria','Consolas','Courier New','Georgia','Open Sans','Tahoma','Times New Roman','Ubuntu','Verdana'],

    initDefaultFont: function() {
        var font;

        if (!this.defaultFont) {
            font = this.textareaEl.getStyle('font-family');
            font = Ext.String.capitalize(font.split(',')[0]);
            this.defaultFont = font;
        } else {
            font = Ext.String.capitalize(this.defaultFont);
        }

        var fonts = Ext.Array.clone(this.fontFamilies);
        Ext.Array.include(fonts, font);
        fonts.sort();
        var select = this.down('#fontSelect').selectEl.dom;

        for (var i = 0; i < fonts.length; i++) {
            var f = fonts[i];
            var lower = f.toLowerCase();
            var option = new Option(f, lower);

            if (f === font) {
                selIdx = i;
            }

            option.style.fontFamily = lower;

            if (Ext.isIE) {
                select.add(option);
            } else {
                select.options.add(option);
            }
        }

        // Old IE versions have a problem if we set the selected property
        // in the loop, so set it after.
        select.options[selIdx].selected = true;
        this.textareaEl.setStyle('font-family', font);
    },

    getDocMarkup: function () {
        var h = this.iframeEl.getHeight() - this.iframePad * 2, oldIE = Ext.isIE8m;
        var markup = '';

        if (oldIE) {
            markup += '<!DOCTYPE html>';
        }

        markup += '<html><head>';
        markup += '<link rel="stylesheet" type="text/css" href="/resolve/css/resources/ResolveTheme-all.css">';
        markup += '<style type="text/css">';

        if (Ext.isOpera || Ext.isIE) {
            markup += 'p {margin:0;}';
        }

        markup += 'body {border:0;margin:0;padding:';
        markup += this.iframePad;
        markup += 'px;direction:';

        if(this.rtl) {
            markup += 'rtl;';
        } else {
            markup += 'ltr;';
        }

        if (!oldIE) {
            markup += 'min-';
        }

        markup += 'height:';
        markup += h;
        markup += 'px;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:text;background-color:white;}</style></head><body class="wiki-builder"></body></html>';
        return markup;
    },

    onEditorEvent: function (e) {
        this.mostRecentEventType = e.type;

        if(e.type !== 'keyup') {
            // override the default htmleditor behavior.
            // We don't want to sync on keypress because we're running regular expressions to clean the html.
            // if we did this every keypress then the editor would be unresponsive.
            // this.readImages(e.browserEvent);
            RS.common.HtmlLintEditor.superclass.onEditorEvent.call(this, e);
        }
    },

    readImages: function (e) {
        // sadly this code does not seem to work. Sigh.
        var images = [];

        if(e.type === 'paste') {
            e = e.originalEvent || e;
            var clipboardData = e.clipboardData || window.clipboardData;

            if(clipboardData) {
                var items = clipboardData.items;

                if(items) {
                    //console.log(JSON.stringify(items));

                    for (var i = 0; i < items.length; i++) {
                        if (items[i].type.indexOf("image") !== -1) {
                            var blob = items[i].getAsFile();
                            var urlFactory = window.URL || window.webkitURL;
                            var source = urlFactory.createObjectURL(blob);
                            var img = new Image();
                            img.onload = function() {};
                            img.src = source;
                        }
                    }
                }
            }
        }

        return images;
    },

    syncValue: function () {
        if (this.changeFired) {
            this.changeFired = false;
            return;
        }

        if (this.initialized) {
            var body = this.getEditorBody();
            var cloneBody = body.cloneNode(true);
            var doc = this.getDoc();

            if (typeof doc.createRange === 'function' && this.mostRecentEventType === 'paste') {
                var nodes = this.categorizeProblematicNodes(cloneBody);
                this.replaceElementsWithSpan(doc, nodes.spans);
                this.removeShapes(doc, nodes.shapes);
                this.replaceImagesWithPlaceholders(doc, nodes.images);
            }

            var changed = this.mostRecentEventType === 'paste' || this.mostRecentEventType === 'keyup';
            var agent = navigator.userAgent.toLowerCase();
            var isFirefox = agent.indexOf('firefox') > -1;
            var isIE = agent.indexOf('msie') > -1 || agent.indexOf('trident') > -1;
            var html = '';

            if (isFirefox) {
                html = this.nodeToHtml(doc, cloneBody);
            } else if (isIE) {
                html = this.ieInnerHTML(cloneBody, false);
            } else {
                html = cloneBody.innerHTML;
            }

            if(this.purgeTagsIfEmpty(isFirefox, cloneBody)) {
                html = "";
                changed = true;
            } else if (this.mostRecentEventType === 'paste' || changed) {
                html = this.cleanHtml(html);
                html = this.wrapMarkupWithWordStyling(cloneBody, html);
                this.mostRecentEventType = 'handled';
                body.innerHTML = html;
                setTimeout(this.setFocus.bind(this), 100);
            }

            var textElDom = this.textareaEl.dom;
            var old = textElDom.value;

            if(!changed) {
                changed = textElDom.value != html;
            }

            this.fireEvent('beforesync', this, html);

            if (changed) {
                textElDom.value = html;
            }

            if (changed) {
                this.fireEvent('sync', this, html);
                this.changeFired = true;
                this.lastValue = old;
                this.fireEvent('change', this, html, old);
                this.fireEvent('dirtychange', this, true);
                this.onDirtyChange(true);
                this.wasDirty = true;
            }
        }
    },

    wrapMarkupWithWordStyling: function (cloneBody, html) {
        var first = cloneBody.childNodes[0];

        if (first && first.nodeType === Node.ELEMENT_NODE) {
            if(!first.nodeName.toLowerCase() === 'div' || first.getAttribute('class') !== 'word-styling') {
                html = '<div class="word-styling">' + html + '</div>';
            }
        }

        return html;
    },

    nodeToHtml: function (doc, root) {
        var html = [];

        if (root.nodeType === Node.ELEMENT_NODE) {
            var children = Array.prototype.slice.call(root.childNodes);

            if (children.length === 1) {
                var candidate = children[0];

                if (candidate.nodeName.toLowerCase() === 'div') {
                    var css = candidate.getAttribute('class');

                    if(css === 'word-styling') {
                        children = Array.prototype.slice.call(candidate.childNodes);
                    }
                }
            }

            for(var i = 0; i < children.length; i++) {
                var div = doc.createElement('div');
                div.appendChild(children[i]);
                html.push(div.innerHTML);
            }
        }

        return html.join('');
    },

    ieInnerHTML: function (obj, convertToLowerCase) {
        var zz = obj.innerHTML ? String(obj.innerHTML) : ''
           ,z  = zz.match(/(<.+[^>])/g);

        if (z) {
         for ( var i=0;i<z.length;(i=i+1) ){
          var y
             ,zSaved = z[i]
             ,attrRE = /\=[a-zA-Z\.\:\[\]_\(\)\&\$\%#\@\!0-9\/]+[?\s+|?>]/g
          ;

          //z[i] = z[i]
		  //        .replace(/([<].+?\w+[>])/,
          //           function(a){return a.toLowerCase();
          //          });
          y = z[i].match(attrRE);

          if (y){
            var j   = 0
               ,len = y.length
            while(j<len){
              var replaceRE =
                   /(\=)([a-zA-Z\.\:\[\]_\(\)\&\$\%#\@\!0-9\/]+)?([\s+|?>])/g
                 ,replacer  = function(){
                      var args = Array.prototype.slice.call(arguments);
                      return '="'+(convertToLowerCase
                              ? args[2].toLowerCase()
                              : args[2])+'"'+args[3];
                    };
              z[i] = z[i].replace(y[j],y[j].replace(replaceRE,replacer));
              j+=1;
            }
           }
           zz = zz.replace(zSaved,z[i]);
         }
        }
        return zz.replace(/\\"/g, '"');
    },

    purgeTagsIfEmpty: function (isFirefox, root) {
        if (!isFirefox && typeof root.textContent !== 'undefined') {
            var text = root.textContent.trim();

            if (text.length === 0 && !this.containsImageTags(root)) {
                html = "";
                changed = true;
            }
        }
    },

    containsImageTags: function (root) {
        var containsImageTags = false;
        var workingQueue = [];

        if (root.nodeType === Node.ELEMENT_NODE) {
            workingQueue.push(root);

            while(workingQueue.length) {
                var node = workingQueue.pop();
                var children = Array.prototype.slice.call(node.childNodes);

                for(var i = 0; i < children.length; i++) {
                    if(children[i].nodeType === Node.ELEMENT_NODE) {
                        workingQueue.push(children[i]);
                    }
                }

                var nodeName = node.nodeName.toLowerCase();
                containsImageTags = nodeName === 'img';

                if(containsImageTags) {
                    workingQueue = [];
                }
            }
        }

        return containsImageTags;
    },

    categorizeProblematicNodes: function (root) {
        var workingQueue = [];
        var results = {
            spans: [],
            shapes: [],
            images: []
        };

        if (root.nodeType === Node.ELEMENT_NODE || root.nodeType === Node.COMMENT_NODE) {
            workingQueue.push(root);

            while(workingQueue.length) {
                var node = workingQueue.pop();
                var children = Array.prototype.slice.call(node.childNodes);

                for(var i = 0; i < children.length; i++) {
                    if(children[i].nodeType === Node.ELEMENT_NODE || children[i].nodeType === Node.COMMENT_NODE) {
                        workingQueue.push(children[i]);
                    }
                }

                var nodeName = node.nodeName.toLowerCase();

                if (nodeName === 'w:sdt'
                    || nodeName === 'w:sdtpr'
                    || nodeName === 'w:wrap'
                    || nodeName === 'v:group') {
                    results.spans.push(node);
                } else if (nodeName === 'img') {
                    var src = node.getAttribute('src');

                    if (src && src.startsWith('file:///')) {
                        results.images.push(node);
                    }
                } else if (nodeName === 'v:rect'
                    || nodeName === 'v:stroke'
                    || nodeName === 'v:oval'
                    || nodeName === 'v:imagedata'
                    || nodeName === 'v:shape'
                    || nodeName === 'v:shapetype'
                    || nodeName === 'v:path'
                    || nodeName === 'v:f'
                    || nodeName === 'v:fill'
                    || nodeName === '#comment') {
                    results.shapes.push(node);
                }
            }
        }

        return results;
    },

    replaceElementsWithSpan: function (doc, nodesToReplace) {
        for (var i = 0; i < nodesToReplace.length; i++) {
            var node = nodesToReplace[i];
            var span = doc.createElement('span');
            var style = node.getAttribute('style');

            if(style) {
                span.setAttribute('style', style);
            }

            while(node.firstChild) {
                span.appendChild(node.firstChild);
            }

            node.parentNode.replaceChild(span, node);
        }
    },

    removeShapes: function (doc, nodesToRemove) {
        for(var i = 0; i < nodesToRemove.length; i++) {
            var node = nodesToRemove[i];

            while (node.firstChild) {
                node.parentNode.insertBefore(node.firstChild, node);
            }

            node.parentNode.removeChild(node);
        }
    },

    replaceImagesWithPlaceholders: function (doc, images) {
        for(var i = 0; i < images.length; i++) {
            var image = images[i];
            var src = image.getAttribute('src');
            image.setAttribute('data-src', src);
            var style = image.getAttribute('style');
            var width = image.getAttribute('width');
            var height = image.getAttribute('height');

            if(style === null) {
                style = '';
            } else {
                style += ';';
            }

            style += 'border:1px solid #ccc';

            if (width) {
                style += ';width:';
                style += width;
                style += 'px';
            }

            if (height) {
                style += ';height:';
                style += height;
                style += 'px';
            }

            var alt = image.getAttribute('alt');

            if(alt) {
                alt = alt.replace(/\[/g, '(');
                alt = alt.replace(/\]/g, ')');
                image.setAttribute('alt', alt);
            }

            image.setAttribute('style', style);
            image.setAttribute('src', '');
            image.removeAttribute('width');
            image.removeAttribute('height');
            image.removeAttribute('border');
            image.removeAttribute('v:shapes');
        }
    },

    cleanHtml: function(html) {
        html = html.replace(/\\/g, '&#92;');
        var toolbar = this.getToolbar();
        var wordComponent = toolbar.getComponent('wordpaste');

        if(wordComponent && wordComponent.pressed) {
            html = this.convertLists(html);

            for (var i = 0; i < this.dirtyHtmlTags.length; i++) {
                html = html.replace(this.dirtyHtmlTags[i].regex, this.dirtyHtmlTags[i].replaceVal);
            }

            html = html.replace(this.regPoint, function (outerMatch, match, p1, p2) {
                var pointSize = +p1 + (+p2 / 10.0);
                var pxSize = Math.ceil(pointSize * 4 / 3);
                return pxSize +'px';
            });

            html = html.replace(this.regBorder, function (match, p1) {
                return p1 + 'margin-bottom:10px;'
            });
        }

        return html;
    },

    setFocus: function () {
        this.iframeEl.dom.contentDocument.body.focus();
    },

    convertLists: function (html) {
        var priorLastIndex = 0;
        var currentNesting = 0;
        var priorNesting = 0;
        var markup = [];
        var nestingStack = [];
        var closingTagStack = [];
        var m;

        while (m = this.regListComponents.exec(html)) {
            if (m.index - 3 > priorLastIndex) {
                this.closeLists(markup, nestingStack, closingTagStack);
                priorNesting = -1;
                nestingStack = [];
                var nonListContent = html.substring(priorLastIndex, m.index);
                markup.push(nonListContent);
            }

            currentNesting = this.currentNesting(nestingStack, currentNesting, priorNesting, m[1]);

            if (currentNesting > priorNesting || nestingStack.length === 0) {
                priorNesting = currentNesting;
                nestingStack.push(currentNesting);
                var div = this.getDoc().createElement('div');
                div.innerHTML = m[2] || m[3];
                var bulletText = div.textContent;
                this.openList(this.regListBullet.test(bulletText), markup, closingTagStack);
            } else {
                while (currentNesting < priorNesting) {
                    priorNesting = this.closeList(markup, nestingStack, closingTagStack);
                }
            }

            this.addListItem(markup, m[4]);
            priorLastIndex = m.index + m[0].length;
        }

        if (closingTagStack.length > 0) {
            this.closeLists(markup, nestingStack, closingTagStack);

            if(markup.length === 0) {
                priorLastIndex++;
            }

            markup.push(html.substring(priorLastIndex));
            html = markup.join('');
        }

        return html;
    },

    currentNesting: function (nestingStack, currentNesting, priorNesting, str) {
        var n = currentNesting;

        if (str) {
            var m = this.regListExplicitIndentation.exec(str);
            this.regListExplicitIndentation.lastIndex = 0;

            if (m !== null && m.length === 3) {
                n = 0;

                if(m[1]) {
                    n += +m[1];
                }

                if(m[2]) {
                    n += +m[2];
                }
            } else {
                m = this.regListRelativeIndentation.exec(str);
                this.regListRelativeIndentation.lastIndex = 0;

                if(m !== null & m.length === 4) {
                    if(m[1]) {
                        n += +m[1];
                    }

                    if(m[2]) {
                        n += +m[2];
                    }

                    if(m[3]) {
                        n += +m[3];
                    }
                }
            }
        }

        return n;
    },

    openList: function (isBullet, markup, closingTagStack) {
        var openingTag, closingTag;

        if (isBullet) {
            openingTag = '<ul>';
            closingTag = '</ul>';
        } else {
            openingTag = '<ol>';
            closingTag = '</ol>';
        }

        markup.push(openingTag);
        closingTagStack.push(closingTag);
    },

    addListItem: function (markup, content) {
        markup.push('<li>');
        markup.push(content);
        markup.push('</li>');
    },

    closeList: function (markup, nestingStack, closingTagStack) {
        markup.push(closingTagStack.pop());
        nestingStack.pop();
        return nestingStack[nestingStack.length - 1];
    },

    closeLists: function (markup, nestingStack, closingTagStack) {
        nestingStack = [];

        while(closingTagStack.length) {
            markup.push(closingTagStack.pop());
        }
    }
});
