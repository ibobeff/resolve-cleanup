(function() {
	Ext.ns('RS.common.grid');

	var RSFormat = RS.common.grid = RS.common.grid || {};

	Ext.apply(RSFormat, {
		/**
		 * Formats the passed string using the specified number of lines.
		 * @param {String} value The value to format.
		 * @param {Number} [numberOfLines] The number of lines to display on the client before elipsing the text
		 * @return {String} The formatted log string.
		 */
		log: function(v, numberOfLines, htmlEncode) {
			if (!v) return v
			var content = v.split(/\n|<br\/>|<\/br>|<br>/g);
		if(htmlEncode)
			return Ext.util.Format.htmlEncode(content.slice(0, numberOfLines).join('<br/>') + (content.length > numberOfLines ? '<br/>&hellip;' : ''));
		else
			return content.slice(0, numberOfLines).join('<br/>') + (content.length > numberOfLines ? '<br/>&hellip;' : '');
		},

		/**
		 * Returns a log format rendering function that can be reused to apply a log string multiple times efficiently
		 * @param {Number} numberOfLines The number of lines to display on the grid before elipsing the text
		 * @return {Function} The log formatting function
		 */
		logRenderer: function(numberOfLines,htmlEncode) {
			return function(v) {
				return RSFormat.log(v, numberOfLines,htmlEncode)
			}
		},

		booleanRenderer: function() {
			return function(value) {
				if (!value || value === 'false') return '';
				return '<span class="icon-large icon-ok-sign rs-boolean-renderer"></span>'
			}
		},

		htmlRenderer: function() {
			return function(v) {
				var doc = document.createElement('div');
				doc.innerHTML = v;
				if (doc.innerHTML !== v) return Ext.String.htmlEncode(v)
				return v
			}
		},

		plainRenderer: function() {
			return function(v) {
				return Ext.util.Format.htmlEncode(v)
			}
		},

		statusRenderer: function(prefix) {
			return function(value, metaData) {
				metaData.style = 'margin: 1px'
				metaData.tdCls = Ext.String.format('{0}-{1}', prefix, value && value.toLowerCase ? value.toLowerCase() : value)
				return value
			}
		},

		linkRenderer: function(modelName, idParam, target) {
			return function(value) {
				if (value)
					return Ext.String.format('<a target="{0}" href="{1}#{2}/{3}={4}">{4}</a>', target, target != '_self' ? window.location.href.split('#')[0] : '', modelName, idParam || 'id', value)
				return ''
			}
		},

		internalLinkRenderer: function(modelName, idParam, valueParam) {
			return function(value, metaData, record) {
				if (value)
					return Ext.String.format('<a class="rs-link" href="#{0}/{1}={2}">{3}</a>', modelName, idParam || 'id', record.get(valueParam), value)
				return ''
			}
		},

		downloadLinkRenderer: function(linkParam) {
			return function(value, metaData, record) {
				if (!value)
					return '';
				return Ext.String.format('<a class="rs-link" href="#" onclick="downloadFile(\'{1}\');return false">{0}</a>', value, record.get(linkParam))
			}
		},

		downloadRenderer: function() {
			return function(value) {
				return Ext.String.format('<a href="#" class="icon-large icon-download rs-icon" onclick="downloadFile(\'{0}\');return false"></div>', value)
			}
		},

		updatedDateRenderer: function() {
			return function(value) {
				if (Ext.isDate(value)) {
					var now = new Date(),
						date = value,
						diff = (now - date) / 1000;

					if (diff < 60) //1 minute
						return Ext.String.format('{0} {1} {2}', Math.round(diff), RS.common.locale.seconds, RS.common.locale.ago)
					if (60 < diff && diff < 60 * 2) // > 1 minute but < 2 minutes
						return Ext.String.format('{0} {1} {2}', 1, RS.common.locale.minute, RS.common.locale.ago)
					if (60 * 2 < diff && diff < 60 * 60) // > 2 minutes but < 60 minutes
						return Ext.String.format('{0} {1} {2}', Math.round(diff / 60), RS.common.locale.minutes, RS.common.locale.ago)
					if (60 * 60 < diff && diff < 2 * 60 * 60) // > 1 hour but < 2 hours
						return Ext.String.format('{0} {1} {2}', 1, RS.common.locale.hour, RS.common.locale.ago)
					if (2 * 60 * 60 < diff && diff < 24 * 60 * 60) // > 2 hours but < 1 day
						return Ext.String.format('{0} {1} {2}', Math.round(diff / (60 * 60)), RS.common.locale.hours, RS.common.locale.ago)
					if (24 * 60 * 60 < diff && diff < 2 * 24 * 60 * 60) // > 24 hours but < 48 hours
						return Ext.String.format('{0}', RS.common.locale.yesterday)
					if (2 * 24 * 60 * 60 < diff && diff < 7 * 24 * 60 * 60)
						return Ext.String.format('{0} {1} {2}', Math.round(diff / (24 * 60 * 60)), RS.common.locale.days, RS.common.locale.ago)

					return Ext.Date.format(value, Ext.state.Manager.get('userDefaultDateFormat', 'c'))
				}
				return value
			}
		}
	})
}());