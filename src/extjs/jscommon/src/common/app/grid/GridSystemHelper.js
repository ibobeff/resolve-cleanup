(function() {
	Ext.ns('RS.common.grid');

	var RSFormat = RS.common.grid = RS.common.grid || {};

	Ext.apply(RSFormat, {

		getSysColumns: function() {
			var defaultDateFormat = Ext.state.Manager.get('userDefaultDateFormat', 'Y-m-d G:i:s');
			return [{
					text: RS.common.locale.sysCreated,
					dataIndex: 'sysCreatedOn',
					hidden: true,
					filterable: true,
					sortable: true,
					groupable: false,
					renderer: Ext.util.Format.dateRenderer(defaultDateFormat)
				}, {
					text: RS.common.locale.sysCreatedBy,
					dataIndex: 'sysCreatedBy',
					hidden: true,
					filterable: true,
					sortable: true,
					groupable: false,
					width: 120
				}, {
					text: RS.common.locale.sysUpdated,
					dataIndex: 'sysUpdatedOn',
					hidden: true,
					filterable: true,
					sortable: true,
					groupable: false,
					width: 200,
					renderer: Ext.util.Format.dateRenderer(defaultDateFormat)
				}, {
					text: RS.common.locale.sysUpdatedBy,
					dataIndex: 'sysUpdatedBy',
					hidden: true,
					filterable: true,
					sortable: true,
					groupable: false,
					width: 120
				},
				/*{
				text: RS.common.locale.sysOrg,
				dataIndex: 'sysOrganizationName',
				hidden: true,
				filterable: false,
				sortable: false,
				width: 130
			}, */
				{
					text: RS.common.locale.sysId,
					dataIndex: 'id',
					hidden: true,
					autoWidth: true,
					sortable: false,
					filterable: false,
					groupable: false,
					width: 300
				}
			]
		},

		getSysFields: function() {
			return ['sysCreatedBy', 'sysUpdatedBy', 'sysOrg', 'id', {
				name: 'sysCreatedOn',
				type: 'date',
				dateFormat: 'time',
				format: 'time'
			}, {
				name: 'sysUpdatedOn',
				type: 'date',
				dateFormat: 'time',
				format: 'time'
			}]
		},

		updateSysFields: function(obj, data) {
			if (data) {
				if (Ext.isDefined(obj.sysCreatedBy)) obj.set('sysCreatedBy', data.sysCreatedBy)
				if (Ext.isDefined(obj.sysUpdatedBy)) obj.set('sysUpdatedBy', data.sysUpdatedBy)
				if (Ext.isDefined(obj.sysOrganizationName)) obj.set('sysOrganizationName', data.sysOrganizationName)
				if (Ext.isDefined(obj.id)) obj.set('id', data.id)
				if (Ext.isDefined(obj.sysCreatedOn)) obj.set('sysCreatedOn', data.sysCreatedOn)
				if (Ext.isDefined(obj.sysUpdatedOn)) obj.set('sysUpdatedOn', data.sysUpdatedOn)

				//legacy support which shouldn't exist but has to because the backend can't be made consistent
				if (Ext.isDefined(obj.sys_created_by)) obj.set('sysCreatedBy', data.sys_created_by)
				if (Ext.isDefined(obj.sys_updated_by)) obj.set('sysUpdatedBy', data.sys_updated_by)
				if (Ext.isDefined(obj.sys_id)) obj.set('id', data.sys_id)
				if (Ext.isDefined(obj.sys_created_on)) obj.set('sysCreatedOn', data.sys_created_on)
				if (Ext.isDefined(obj.sys_updated_on)) obj.set('sysUpdatedOn', data.sys_updated_on)
			}
		},

		getCustomSysColumns: function() {
			var defaultDateFormat = Ext.state.Manager.get('userDefaultDateFormat', 'Y-m-d G:i:s');
			return [{
				text: RS.common.locale.sysCreated,
				dataIndex: 'sys_created_on',
				hidden: false,
				filterable: true,
				sortable: true,
				flex: 1,
				renderer: Ext.util.Format.dateRenderer(defaultDateFormat)
			}, {
				text: RS.common.locale.sysCreatedBy,
				dataIndex: 'sys_created_by',
				hidden: true,
				filterable: true,
				sortable: true,
				width: 120
			}, {
				text: RS.common.locale.sysUpdated,
				dataIndex: 'sys_updated_on',
				hidden: true,
				filterable: true,
				sortable: true,
				width: 200,
				renderer: Ext.util.Format.dateRenderer(defaultDateFormat)
			}, {
				text: RS.common.locale.sysUpdatedBy,
				dataIndex: 'sys_updated_by',
				hidden: true,
				filterable: true,
				sortable: true,
				width: 120
			}, {
				text: RS.common.locale.sysId,
				dataIndex: 'sys_id',
				hidden: true,
				autoWidth: true,
				sortable: false,
				filterable: false,
				width: 300
			}]
		},

		getCustomSysFields: function() {
			return ['sys_created_by', 'sys_updated_by', 'sys_id', {
				name: 'sys_created_on',
				type: 'date',
				dateFormat: 'time',
				format: 'time'
			}, {
				name: 'sys_updated_on',
				type: 'date',
				dateFormat: 'time',
				format: 'time'
			}]
		}
	})
}());