function configureColumns(fields, modelFields, columns, autoSize, initialConfig, form) {
	Ext.Array.forEach(fields, function(record) {
		if (!record) {
			return;
		}
		switch (record.dbtype) {
			case 'timestamp':
				type = 'date';
				break;
			case 'boolean':
				type = 'boolean';
				break;
			case 'long':
			case 'double':
				type = 'number';
				break;
			default:
				type = 'string';
				break;
		}

		var storeCol = {
			name: record.columnModelName,
			type: type
		},
			col = {
				text: Ext.String.htmlEncode(record.displayName),
				dataIndex: record.columnModelName,
				fieldConfig: record,
				hidden: record.hidden,
				uiType: record.uiType,
				filterable: !record.encrypted,
				dataType: type == 'number' ? 'float' : type
			};

		if (autoSize) {
			col.width = 150;
			col.autoWidth = record.dbtype != 'clob';
		} else col.flex = 1;

		if (type == 'date') {
			storeCol.format = 'time';
			storeCol.dateFormat = 'time';
			col.renderer = Ext.util.Format.dateRenderer(Ext.state.Manager.get('userDefaultDateFormat', 'Y-m-d G:i:s'));
			col.width = 175;
			delete col.flex;
		}

		var styleRenderer = Ext.emptyFn,
			parseDate = function(value) {
				switch (value.toLowerCase()) {
					case 'today':
						value = Ext.Date.format(new Date(), 'Y-m-d');
						break;
					case 'yesterday':
						value = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, -1), 'Y-m-d');
						break;
					case 'tomorrow':
						value = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, 1), 'Y-m-d');
						break;
					case 'last week':
						value = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, -7), 'Y-m-d');
						break;
					case 'next week':
						value = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, 7), 'Y-m-d');
						break;
					case 'last month':
						value = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.MONTH, -1), 'Y-m-d');
						break;
					case 'next month':
						value = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.MONTH, 1), 'Y-m-d');
						break;
					case 'last year':
						value = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, -1), 'Y-m-d');
						break;
					case 'next year':
						value = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, 1), 'Y-m-d');
						break;
					default:
						var formats = ['Y-m-d', 'g:i a', 'g:i A', 'G:i', 'Y-m-d g:i a', 'Y-m-d g:i A', 'Y-m-d G:i'];
						Ext.Array.each(formats, function(format) {
							var date = Ext.Date.parse(value, format);
							if (Ext.isDate(date)) {
								value = date;
								return false;
							}
						});
						break;
				}
				return value
			};

		if (form && form.items) {
			Ext.Array.each(form.items, function(item) {
				//tested item dependencies with dates and strings
				// item.dependencies = [{
				// 	action: "setBgColor",
				// 	actionOptions: "#0000FF",
				// 	condition: "contains",
				// 	id: "",
				// 	target: "u_status",
				// 	value: "N"
				// }, {
				// 	action: "setBgColor",
				// 	actionOptions: "#FF0000",
				// 	condition: "after",
				// 	id: "",
				// 	target: "sys_updated_on",
				// 	value: "2013-11-17"
				// }];
				if (item.name == col.dataIndex && item.dependencies && Ext.isArray(item.dependencies)) {
					styleRenderer = function(value, metaData, record) {
						Ext.Array.forEach(item.dependencies, function(dependency) {
							if (dependency.action == 'setBgColor') {
								var val = record.get(dependency.target),
									match = false,
									checkVal = dependency.value;

								if (Ext.isBoolean(val)) checkVal = Boolean(checkVal)
								if (Ext.isNumber(val)) checkVal = Number(checkVal)
								if (Ext.isDate(val)) checkVal = parseDate(checkVal)

								switch (dependency.condition) {
									case 'equals':
									case 'on':
										match = val == checkVal
										break;
									case 'notequals':
										match = val != checkVal
										break;
									case 'greaterThan':
									case 'after':
										match = val > checkVal
										break;
									case 'greaterThanOrEqual':
										match = val >= checkVal
										break;
									case 'lessThan':
									case 'before':
										match = val < checkVal
										break;
									case 'lessThanOrEqual':
										match = val <= checkVal
										break;
									case 'contains':
										match = val && val.indexOf(checkVal) > -1
										break;
									case 'notcontains':
										match = val && val.indexOf(checkVal) == -1
										break;
								}

								if (match) {
									metaData.style = 'background-color:' + dependency.actionOptions
								}
							}
						}, this)
					}
				}
			})
		}

		switch (col.uiType) {
			case 'NameField':
				col.renderer = function(value, metaData, record, rowIndex, colIndex, store, view) {
					styleRenderer(value, metaData, record)
					// if (view.ownerCt.columns[colIndex] && view.ownerCt.columns[colIndex].hidden) return '';
					var val = value.split('|&|');
					return (val[3] ? val[3] + ', ' : '') + (val[0] ? val[0] + ' ' : '') + (val[1] ? val[1] : '') + (val[2] ? val[2] + '.' : '');
				}
				break;
			case 'AddressField':
				col.renderer = function(value, metaData, record, rowIndex, colIndex, store, view) {
					styleRenderer(value, metaData, record)
					// if (view.ownerCt.columns[colIndex] && view.ownerCt.columns[colIndex].hidden) return '';
					var val = value.split('|&|');
					return (val[0] ? val[0] + '<br/>' : '') + (val[1] ? val[1] + '<br/>' : '') + (val[2] ? val[2] + ' ' : '') + (val[3] ? val[3] + ' ' : '') + (val[4] ? val[4] : '') + (val[5] ? '<br/>' + val[5] : '');
				}
				break;
			case 'DateField':
			case 'Date':
				col.renderer = function(value, metaData, record, rowIndex, colIndex, store, view) {
					styleRenderer(value, metaData, record)
					return Ext.util.Format.date(value, 'Y-m-d')
				}
				break;
			case 'TimeField':
			case 'Timestamp':
				col.renderer = function(value, metaData, record, rowIndex, colIndex, store, view) {
					styleRenderer(value, metaData, record)
					return Ext.util.Format.date(value, 'g:i:s A')
				}
				break;
			case 'DateTimeField':
			case 'DateTime':
				col.renderer = function(value, metaData, record, rowIndex, colIndex, store, view) {
					styleRenderer(value, metaData, record)
					return Ext.util.Format.date(value, 'Y-m-d g:i:s A')
				}
				break;

			case 'CheckBox':
			case 'Checkbox':
			case 'CheckboxField':
				col.renderer = function(value, metaData, record, rowIndex, colIndex, store, view) {
					styleRenderer(value, metaData, record)
					// if (view.ownerCt.columns[colIndex] && view.ownerCt.columns[colIndex].hidden) return '';
					return value === true ? RS.common.locale.trueText : RS.common.locale.falseText;
				}
				break;
			case 'Journal':
				col.renderer = function(value, metaData, record, rowIndex, colIndex, store, view) {
					styleRenderer(value, metaData, record)
					try {
						var entries = Ext.decode(value),
							notes = [];
						Ext.Array.forEach(entries, function(entry) {
							notes.push(entry.note)
						})
						return notes.join('<br/>')
					} catch (e) {}
					return value
				}
				break;
			default:
				col.renderer = function(value, metaData, record, rowIndex, colIndex, store, view) {
					styleRenderer(value, metaData, record)
					// if (view.ownerCt.columns[colIndex] && view.ownerCt.columns[colIndex].hidden) return '';
					return Ext.String.htmlEncode(value);
				}
				break;
		}

		// if(initialConfig && record.columnModelName.indexOf('sys_') == 0) col.hidden = true;  //Took out auto-hide columns because it was causing problems for duke
		modelFields.push(storeCol);
		columns.push(col);
	});
}
