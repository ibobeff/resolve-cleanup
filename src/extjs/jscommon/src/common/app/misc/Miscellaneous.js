/*Apply extra data into destination object
	Example use 
	var extraData = {
		"resolveActionInvoc.resolveActionInvocOptions" : [{
			id : '',
			udescription : '',
			uname : 'INPUTFILE',
			uvalue : this.autoGenCode
		}],
		"resolveActionInvoc.resolveActionParameters" :  newTaskParam
	}
*/
RS.common.deepApply = function(destination, extraData){
	for(var path in extraData){
		var newData = extraData[path];
		var pathParts = path.split('.');
		var targetDataLocation = destination[pathParts[0]];
		for(var i = 1; i < pathParts.length; i++){
			targetDataLocation = targetDataLocation[pathParts[i]];
		}
		if(Array.isArray(targetDataLocation)){
			//Clear out default value if there is any.
			targetDataLocation.shift();
			Array.prototype.push.apply(targetDataLocation, newData);
		}
		else
			Ext.apply(targetDataLocation, newData);
	}
	return destination;
};

RS.common.decodeHTML = function (html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
};
RS.common.hexToRgb = function(hex) {
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
};
RS.common.saturateColor = function(color, amount){
	var color = color.toUpperCase().split('');
	var letters = '0123456789ABCDEF'.split('');
	for(var i = 0; i < color.length; i++){
		var newSaturation = 0;
		if(letters.indexOf(color[i]) + amount > 15) 
			newSaturation = 15;
		else if(letters.indexOf(color[i]) + amount < 0) 
			newSaturation = 0;
		else 
			newSaturation = letters.indexOf(color[i]) + amount;
		color[i] = letters[newSaturation];
	}
	return color.join('');
};
RS.common.parsePayload = function (response) {
	var payload = {};
	try {
		payload = Ext.JSON.decode(response.responseText);
	} catch (ex) {
		var msg = '';
		var errCode = '';
		if (response && Ext.isString(response.responseText) && response.responseText.indexOf('signinDiv') != -1 ) {
			msg = 'Session Timeout';
			errCode = 'SESSION_TIMEOUT';
		} else if (response.status == 403) {
			msg = 'Error 403 Access Denied/Forbidden';
			errCode = 'ACCESS_FORBIDDEN';
		} else {
			msg = ex.message;
			errCode = 'INVALID_SERVER_RESPONSE';
		}
		payload = {
			success: false,
			message: msg,
			errCode: errCode
		};
	}
	if (payload) {
		return payload;
	} else {
		var msg = response.statusText || 'Unknown error';
		return {
			success: false,
			message: msg,
			errCode: 'COMMUNICATION_TIMEOUT'
		};
	}
};
//Convert img src into data URI
RS.common.getDataUri = function(url, callback, scope) {
    var image = new Image();

    image.onload = function () {
        var canvas = document.createElement('canvas');
        canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
        canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size

        canvas.getContext('2d').drawImage(this, 0, 0);

        // Get raw image data
        //callback(canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''));

        // ... or get as Data URI
        callback.apply(scope ? scope : this, [canvas.toDataURL('image/png')]);
    };

    image.src = url;
};

RS.common.DEBUG = function(scope, msg) {
	if (window.console && window.console.log) {

		var listenerCount = 0;
		if (typeof(clientVM) != 'undefined') listenerCount = glu.getListenerCount(clientVM);
		else listenerCount = glu.getListenerCount(this);

		var timestamp = new Date().getTime();

		var debugMsg = timestamp + ' DEBUG: ListenerCount=' + listenerCount;

		if (scope.ns && scope.viewmodelName) debugMsg += ' ' + scope.ns + '.' + scope.viewmodelName;
		else if (scope.viewmodelName) debugMsg += ' ' + scope.viewmodelName;
		else if (scope.$className) debugMsg += ' ' + scope.$className;
		//else debugger;

		if (msg) debugMsg += ' : ' + msg;

		window.console.log(debugMsg);
	}
};

RS.common.mergeObject = function(objA, objB){
	var r = Ext.apply({}, objA);
	for(var property in objA){
		if(objB.hasOwnProperty(property))
			r[property] = objB[property];
	}
	return r;
};

RS.common.encodeForURL = function(url) {
	try {
		return $ESAPI.encoder().encodeForURL(url);
	} catch (err) {
		return encodeURIComponent(url);
	}
};

RS.common.decodeFromURL = function(url) {
	try {
		return $ESAPI.encoder().decodeFromURL(url);
	} catch (err) {
		return encodeURIComponent(url);
	}
};

RS.common.encodeForHTML = function(param) {
	try {
		return $ESAPI.encoder().encodeForHTML(param);
	} catch (err) {
		return Ext.util.Format.htmlEncode(param);
	}
};

