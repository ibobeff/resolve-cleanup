RS.common.blocks.BlockContainer = function () {
	this.blocks = {};
	this.blockNames = [];
	this.dataDependencies = {};
	this.dataInternalDependencies = {};
	this.callbackDependencies = {};
	this.dirtySubscribers = [];
	this.validSubcribers = [];	
	this.payload = {};
	this.initialLoad = true;

	this.getBlocks = function () { 
		return this.blocks; 
	};

	this.getBlockNames = function () { 
		return this.blockNames; 
	};

	this.getDataDependencies = function (sourceBlockName, sourceBlockModelName) {
		var dependency,
			sourceBlockHasDependency = typeof this.dataDependencies[sourceBlockName] !== 'undefined';

		if (sourceBlockHasDependency) {
			var sourceModelHasDependency = typeof this.dataDependencies[sourceBlockName][sourceBlockModelName] !== 'undefined';

			if (sourceModelHasDependency) {
				dependency = this.dataDependencies[sourceBlockName][sourceBlockModelName];
			}
		}

		return dependency;
	};

	this.addDataDependency = function (sourceBlockName, sourceBlockModelName, dependentBlockName, dependentBlockModelName) { 
		if (typeof this.dataDependencies[sourceBlockName] === 'undefined') {
			this.dataDependencies[sourceBlockName] = {};
		}

		if (typeof this.dataDependencies[sourceBlockName][sourceBlockModelName] === 'undefined') {
			this.dataDependencies[sourceBlockName][sourceBlockModelName] = [];
		}

		var found = false,
			i = 0,
			d = this.dataDependencies[sourceBlockName][sourceBlockModelName];

		while (!found && i < d.length) {
			found = d[i].blockName === dependentBlockName && d[i].blockModelName === dependentBlockModelName;
			i++;
		}

		if (!found) {
			d.push({
				blockName: dependentBlockName,
				blockModelName: dependentBlockModelName
			});
		}
	};

	this.addInternalDataDependency = function (sourceBlockName, sourceBlockModelName, dependentBlockName, dependentBlockModelName) { 
		if (typeof this.dataInternalDependencies[sourceBlockName] === 'undefined') {
			this.dataInternalDependencies[sourceBlockName] = {};
		}

		if (typeof this.dataInternalDependencies[sourceBlockName][sourceBlockModelName] === 'undefined') {
			this.dataInternalDependencies[sourceBlockName][sourceBlockModelName] = [];
		}

		var found = false,
			i = 0,
			d = this.dataInternalDependencies[sourceBlockName][sourceBlockModelName];

		while (!found && i < d.length) {
			found = d[i].blockName === dependentBlockName && d[i].blockModelName === dependentBlockModelName;
			i++;
		}

		if (!found) {
			d.push({
				blockName: dependentBlockName,
				blockModelName: dependentBlockModelName
			});
		}
	};

	this.addCallback = function (triggerMethod, sourceBlockName, sourceBlockModelName, callback) {
		if (typeof this.callbackDependencies[sourceBlockName] === 'undefined') {
			this.callbackDependencies[sourceBlockName] = {};
		}	

		if (typeof this.callbackDependencies[sourceBlockName][sourceBlockModelName] === 'undefined') {
			this.callbackDependencies[sourceBlockName][sourceBlockModelName] = {
				onChange: [],
				onLoadOrClose: [],
				onClose: []
			};
		}

		var found = false, 
			i = 0, 
			serializedCallback = callback.toString(),
			d = this.callbackDependencies[sourceBlockName][sourceBlockModelName][triggerMethod];

		while (!found && i < d.length) {
			found = d[i].toString() === serializedCallback;
			i++;
		}

		var id = null;

		if (!found) {
			id = new Date().getTime();
			callback.uniqueCallbackID = id;
			d.push(callback);
		}

		return id;
	};

	this.spliceCallback = function (triggerMethod, sourceBlockName, sourceBlockModelName, uniqueID) {
		if (this.callbackDependencies[sourceBlockName] && 
			this.callbackDependencies[sourceBlockName][sourceBlockModelName] &&
			this.callbackDependencies[sourceBlockName][sourceBlockModelName][triggerMethod]) {

			var i = 0, 
				d = this.callbackDependencies[sourceBlockName][sourceBlockModelName][triggerMethod];

			while (i < d.length) {
				if (d[i].uniqueCallbackID === uniqueID)	{
					d.splice(i, 1);
					break;
				}

				i++
			}
		}			
	};

	this.getCallbacks = function (triggerMethod, sourceBlockName, sourceBlockModelName) {
		var callbacks = [];

		if (typeof this.callbackDependencies[sourceBlockName] !== 'undefined' &&
			typeof this.callbackDependencies[sourceBlockName][sourceBlockModelName] !== 'undefined' &&
			typeof this.callbackDependencies[sourceBlockName][sourceBlockModelName][triggerMethod] !== 'undefined') {
			callbacks = this.callbackDependencies[sourceBlockName][sourceBlockModelName][triggerMethod];
		}

		return callbacks;
	};

	this.getDirtySubscribers = function () {
		return this.dirtySubscribers;
	};

	this.getValidSubcribers = function(){
		return this.validSubcribers;
	}

	this.getInitialLoad = function () {
		return this.initialLoad;
	};

	this.setInitialLoad = function (value) {
		this.initialLoad = value;
	};	
};

RS.common.blocks.BlockContainer.prototype = {
	subscribeValid: function(fn){
		var key = null;

		if(typeof fn === 'function'){
			key = new Date().getTime();
			var validSubcribers = this.getValidSubcribers();
			validSubcribers.push({
				key : key,
				callback : fn
			})
		}
		return key;
	},

	unsubscribeValid: function (key) {
		var validSubcribers = this.getValidSubcribers();

		for (var i = 0; i < validSubcribers.length; i++) {
			if (validSubcribers[i].key === key) {
				validSubcribers.splice(i, 1);
			}
		}
	},

	notifyValid: function () {
		var valid = this.isValid();
		var validSubcribers = this.getValidSubcribers();

		for (var i = 0; i < validSubcribers.length; i++) {
			validSubcribers[i].callback(valid);
		}
	},

	isValid: function () {
		var valid = true;
		var blocks = this.getBlocks();

		for (var k in blocks) {			
			if (!blocks[k].getIsValid()) {
				valid = false;
				break;
			}
		}

		return valid;
	},

	subscribeDirty: function (fn) {
		var key = null;

		if (typeof fn === 'function') {
			key = new Date().getTime();
			var dirtySubscribers = this.getDirtySubscribers();
			dirtySubscribers.push({
				key: key,
				callback: fn
			});
		}

		return key;
	},

	unsubscribeDirty: function (key) {
		var dirtySubscribers = this.getDirtySubscribers();

		for (var i = 0; i < dirtySubscribers.length; i++) {
			if (dirtySubscribers[i].key === key) {
				dirtySubscribers.splice(i, 1);
			}
		}
	},

	notifyDirty: function () {
		var dirty = this.isDirty();		
		var dirtySubscribers = this.getDirtySubscribers();

		for (var i = 0; i < dirtySubscribers.length; i++) {
			dirtySubscribers[i].callback(dirty);
		}
	},

	notifyDataChange : function(blockName, blockModelName, data){
		if(this.dataInternalDependencies[blockName]){
			var sourceBlockName = this.dataInternalDependencies[blockName] || {};
			var dependentBlockNames = sourceBlockName[blockModelName] || [];
			for(var i = 0; i < dependentBlockNames.length; i++){
				var dependentBlockInfo = dependentBlockNames[i];
				var dependentBlockName = dependentBlockInfo['blockName'];
				var dependentBlockModelName = dependentBlockInfo['blockModelName'];
				var dependentBlockModel = this.blocks[dependentBlockName]['blockModels'][dependentBlockModelName];
				var processingMethod = dependentBlockModel['processDataChange'];
				if(Ext.isFunction(processingMethod)){
					processingMethod.apply(dependentBlockModel,[data]);
				}
			}
		}
	},

	isDirty: function () {
		var dirty = false;
		var blocks = this.getBlocks();

		for (var k in blocks) {			
			if (blocks[k].getIsDirty()) {
				dirty = true;
				break;
			}
		}

		return dirty;
	},

	addBlock: function (blockName) {
		var block = new RS.common.blocks.Block(this, blockName);
		var blocks = this.getBlocks();
		var blockNames = this.getBlockNames();
		blocks[blockName] = block;
		blockNames.push(blockName);
		return block;
	},

	hideBlocks: function () {
		var blockNames = [];

		if (arguments.length === 0) {
			blockNames = this.getBlockNames();
		} else if (arguments.length === 1) {
			if (arguments[0].constructor === Array) {
				blockNames = arguments[0];
			} else {
				blockNames.push(arguments[0]);
			}			
		} else {
			blockNames = Array.apply(null, arguments);
		}

		var blocks = this.getBlocks();

		for(var i = 0; i < blockNames.length; i++) {
			blocks[blockNames[i]].hide();
		}
	},

	showBlocks: function () {
		var blockNames = [];

		if (arguments.length === 0) {
			blockNames = this.getBlockNames();
		} else if (arguments.length === 1) {
			if (arguments[0].constructor === Array) {
				blockNames = arguments[0];
			} else {
				blockNames.push(arguments[0]);
			}			
		} else {
			blockNames = Array.apply(null, arguments);
		}

		var blocks = this.getBlocks();

		for(var i = 0; i < blockNames.length; i++) {
			blocks[blockNames[i]].show();
		}
	},

	defineDataDependency: function (source, reactor) {
		var sourceBlockName = source.getBlock().getName(), 
			sourceBlockModelName = source.getName(), 
			dependentBlockName = reactor.getBlock().getName(), 
			dependentBlockModelName = reactor.getName(),
			blockNames = this.getBlockNames();

		if (blockNames.indexOf(sourceBlockName) === -1) {
			throw new Error('The sourceBlockName is not defined.');
		}

		if (blockNames.indexOf(dependentBlockName) === -1) {
			throw new Error('The dependentBlockName is not defined.');
		}

		this.addDataDependency(sourceBlockName, sourceBlockModelName, dependentBlockName, dependentBlockModelName);
	},

	//Handle internal data that not neccessary need to send to backend. Source will trigger notifyDataChange, reactor will process change via processDataChange
	defineInternalDataDependency : function(source, reactor){
		var sourceBlockName = source.getBlock().getName(), 
			sourceBlockModelName = source.getName(), 
			dependentBlockName = reactor.getBlock().getName(), 
			dependentBlockModelName = reactor.getName(),
			blockNames = this.getBlockNames();

		if (blockNames.indexOf(sourceBlockName) === -1) {
			throw new Error('The sourceBlockName is not defined.');
		}

		if (blockNames.indexOf(dependentBlockName) === -1) {
			throw new Error('The dependentBlockName is not defined.');
		}

		this.addInternalDataDependency(sourceBlockName, sourceBlockModelName, dependentBlockName, dependentBlockModelName);
	},

	defineCallback: function (source, callback, triggerMethod) {
		if (!triggerMethod) {
			triggerMethod = 'onLoadOrClose';
			// other callbacks are onChange and onClose
		}

		var sourceBlockName = source.getBlock().getName(), 
			sourceBlockModelName = source.getName(), 
			blockNames = this.getBlockNames();

		if (blockNames.indexOf(sourceBlockName) === -1) {
			throw new Error('The sourceBlockName is not defined.');
		}

		if (typeof callback !== 'function') {
			throw new Error('The callback must be a function.');
		}

		return this.addCallback(triggerMethod, sourceBlockName, sourceBlockModelName, callback);
	},

	removeCallback: function (source, triggerMethod, uniqueID) {
		var sourceBlockName = source.getBlock().getName(), 
			sourceBlockModelName = source.getName(), 
			blockNames = this.getBlockNames();

		if (blockNames.indexOf(sourceBlockName) === -1) {
			throw new Error('The sourceBlockName is not defined.');
		}

		this.spliceCallback(triggerMethod, sourceBlockName, sourceBlockModelName, uniqueID);
	},

	checkDataDependencies: function (triggerMethod, sourceBlockName, sourceBlockModelName, sourceData) {
		var d = this.getDataDependencies(sourceBlockName, sourceBlockModelName),
			callbacks = this.getCallbacks(triggerMethod, sourceBlockName, sourceBlockModelName);

		if (d && d.constructor === Array) {
			var blocks = this.getBlocks();

			for (var i = 0; i < d.length; i++) {
				var block = blocks[d[i].blockName];

				if (block) {					
					block.modifyByNotification(d[i].blockModelName, sourceData.slice(), sourceBlockModelName);
				}
			}
		}

		for (var i = 0; i < callbacks.length; i++) {
			callbacks[i].apply(this, sourceData);
		}
	},

	checkCallbacksOnLoadOrClose: function (sourceBlockName, sourceBlockModelName, sourceData) {
		var callbacks = this.getCallbacks('onLoadOrClose', sourceBlockName, sourceBlockModelName);

		for (var i = 0; i < callbacks.length; i++) {
			callbacks[i].apply(this, sourceData);
		}
	},

	checkCallbacksOnClose: function (sourceBlockName, sourceBlockModelName, sourceData) {
		var callbacks = this.getCallbacks('onClose', sourceBlockName, sourceBlockModelName);

		for (var i = 0; i < callbacks.length; i++) {
			callbacks[i].apply(this, sourceData);
		}
	},

	getData: function () {
		var blockNames = this.getBlockNames(),
			blocks = this.getBlocks(),
			data = {},
			result = {
				paths: [],
				leaves: []
			};

		for (var i = 0; i < blockNames.length; i++) {
			var blockResult = blocks[blockNames[i]].getDataForServer();
			result.paths.push.apply(result.paths, blockResult.paths);
			result.leaves.push.apply(result.leaves, blockResult.leaves);
		}

		if (result.paths.length !== result.leaves.length) {
			throw new Error('paths.length does not match leaves.length');
		}

		var firstBlock = blocks[blockNames[0]];

		for (var i = 0; i < result.paths.length; i++) {
			var tuple = firstBlock.getPathAndFilter(result.paths[i]);
			firstBlock.deepUpdate(data, result.leaves[i], tuple.path.split('.'), tuple.filter, true);
		}

		return data;
	},

	loadOriginalData: function (originalData, originalDirtyFlag) {
		var blockNames = this.getBlockNames(),
			blocks = this.getBlocks();

		for(var i = 0; i < blockNames.length; i++) {
			blocks[blockNames[i]].loadOriginalData(originalData[i], originalDirtyFlag);
		}
	},

	getOriginalData: function () {
		var blockNames = this.getBlockNames(),
			blocks = this.getBlocks(),
			originalData = [];

		for(var i = 0; i < blockNames.length; i++) {
			originalData.push(blocks[blockNames[i]].getOriginalData());
		}

		return originalData;
	},

	load: function (payload) {
		var blockNames = this.getBlockNames(),
			blocks = this.getBlocks();

		for(var i = 0; i < blockNames.length; i++) {
			blocks[blockNames[i]].load(payload);
		}

		if (!this.getInitialLoad()) {
			for(var i = 0; i < blockNames.length; i++) {
				blocks[blockNames[i]].reportDataModificationForAllBlockModels(this, true);
			}

			this.notifyDirty(false);
		} else {
			this.setInitialLoad(false);
		}
	},

	reload: function (payload) {
		var blockNames = this.getBlockNames(),
			blocks = this.getBlocks();

		for(var i = 0; i < blockNames.length; i++) {
			blocks[blockNames[i]].load(payload);
		}

		for(var i = 0; i < blockNames.length; i++) {
			blocks[blockNames[i]].reportDataModificationForAllBlockModels(this, false);
		}
	},

	saveComplete: function () {
		var blockNames = this.getBlockNames(),
			blocks = this.getBlocks();

		for(var i = 0; i < blockNames.length; i++) {
			blocks[blockNames[i]].saveComplete();
		}
	}

};