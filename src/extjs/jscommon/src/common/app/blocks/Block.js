Ext.namespace('RS.common', 'RS.common.blocks');
RS.common.blocks.Block = function (blockContainer, blockName) {
	this.blockModels = {};
	this.blockModelNames = [];
	this.dataIndexes = [];
	this.currentModelName = '';
	this.nextModelName = '';
	this.initialLoad = true;
	this.isDirty = false;
	this.isValid = true;

	this.getBlockContainer = function () { 
		return blockContainer; 
	};

	this.getBlockModels = function () { 
		return this.blockModels; 
	};

	this.getBlockModelNames = function () { 
		return this.blockModelNames; 
	};

	this.getName = function () { 
		return blockName; 
	};
		
	this.getCurrentModelName = function () {
		if (this.currentModelName === '') {
			this.currentModelName = this.blockModelNames[0];
		}

		return this.currentModelName; 
	};
	
	this.setCurrentModelName = function (current) { 
		this.currentModelName = current; 
	};
	
	this.addDataIndex = function (i) {
		if (this.dataIndexes.indexOf(i) === -1) {
			this.dataIndexes.push(i);
		}
	};

	this.getDataIndexes = function () {
		return this.dataIndexes;
	};

	this.getInitialLoad = function () {
		return this.initialLoad;
	};

	this.setInitialLoad = function (value) {
		this.initialLoad = value;
	};

	this.setIsDirty = function(dirty){
		this.isDirty = dirty;
	};

	this.getIsDirty = function(){
		return this.isDirty;
	}

	this.setIsValid = function(valid){
		this.isValid = valid;
	};

	this.getIsValid = function(){
		return this.isValid;
	}
};

RS.common.blocks.Block.prototype = (function () {
	function getLeaf(node, paths) {
		var leaf = null;

		for (var i = 0; i < paths.length; i++) {
			leaf = node = node[paths[i]];
		}

		return leaf;
	}

	function mergeArrays(original, modified, filter, buildArray) {
		var newEntries = [];

		for (var i = modified.length - 1; i >= 0; i--) {
			if (buildArray || !modified[i].id) {
				newEntries.unshift(modified[i]);
				modified.splice(i, 1);
			}
		}

		if (typeof filter === 'function') {
			for (var i = original.length - 1; i >= 0; i--) {
				var o = original[i];

				if (filter(o)) {
					var modifiedIndex = indexByID(modified, o.id);

					if (modifiedIndex === -1) {
						original.splice(i, 1);
					} else {
						copyProperties(o, modified[modifiedIndex]);
					}
				} 
			}
		} else {
			for (var i = original.length - 1; i >= 0; i--) {
				var modifiedIndex = indexByID(modified, original[i].id);
				
				if (modifiedIndex === -1) {
					original.splice(i, 1);
				} else {
					copyProperties(original[i], modified[modifiedIndex]);
				}
			}
		}

		original.push.apply(original, newEntries);
	}

	function indexByID(original, id) {
		var found = -1;
		var i = 0;

	    while (found < 0 && i < original.length) {
	    	if (original[i].id === id) {
	    		found = i;
	    	}

    		i++;
		}

		return found;
	}

	function copyProperties(node, leaf) {
		for (key in leaf) {
			if (leaf.hasOwnProperty(key)) {
				node[key] = leaf[key];
			}
		}
	}

	return {
		addModel: function (context, title, gluViewmodelNamespace, modelFn) {
			var blockModelNames = this.getBlockModelNames(),
				blockModels = this.getBlockModels(),
				blockContainer = this.getBlockContainer(),
				parts = gluViewmodelNamespace.split('.'),			
				modelName = parts[parts.length - 1];

			blockModelNames.push(modelName);
			var blockModel = new modelFn(this, modelName, blockModelNames.length - 1);
			glu.defModel(gluViewmodelNamespace, blockModel.modelConfig);
			blockModels[modelName] = blockModel;
			blockModel.suspendDirtyNotification = true;
			blockModel.gluModel = context.model(gluViewmodelNamespace);

			if (typeof blockModel.gluModel.postInit === 'function') {
				blockModel.gluModel.postInit(blockModel, context);
			}

			blockModel.suspendDirtyNotification = false;				
			blockModel.setViewTitle(context.localize(title));
			return blockModel;
		},

		addDataSource: function (model) {
			this.addDataIndex(model.getModelIndex());
		},

		showView: function (modelName) {
			var blockModels = this.getBlockModels();			
			var d = blockModels[modelName];
			var currentModelName = this.getCurrentModelName();

			if (currentModelName.length > 0 && currentModelName !== modelName) {
				var current = blockModels[currentModelName];
				current.hide();
			}

			this.setCurrentModelName(modelName);
			this.show();
			return this;
		},

		close: function (sourceBlockModelName, nextBlockModelName) {
            this.setCurrentModelName(nextBlockModelName);
			var blockContainer = this.getBlockContainer(),
				blockModels = this.getBlockModels(),
				blockModelName = this.getName(),
				result = this.getDataForServer(),
				data = {};

			for (var i = 0; i < result.paths.length; i++) {
				var tuple = this.getPathAndFilter(result.paths[i]);
				this.deepUpdate(data, result.leaves[i], tuple.path.split('.'), tuple.filter, true);
			}				

			if (nextBlockModelName !== sourceBlockModelName) {
				var d = blockModels[nextBlockModelName],
					dataPaths = d.getDataPaths(),
					leaves = [];

				if (dataPaths.length > 0) {
					for (var i = 0; i < dataPaths.length; i++) {
						leaves.push(this.getFilteredLeaf(data, dataPaths[i]));
					}

					d.suspendDirtyNotification = true;
		            d.load.apply(d, leaves);
		            d.suspendDirtyNotification = false;
				}
			}
			
			var blockModelData = blockModels[sourceBlockModelName].getData();
			blockContainer.checkCallbacksOnLoadOrClose(blockModelName, sourceBlockModelName, blockModelData);
			blockContainer.checkCallbacksOnClose(blockModelName, sourceBlockModelName, blockModelData);

            if (nextBlockModelName) {                
                this.showView(nextBlockModelName);
            }

            return this;
		},

		loadOriginalData: function (originalData, originalDirtyFlag) {
			var blockModels = this.getBlockModels(),				
				blockModelNames = this.getBlockModelNames(),
				dataIndexes = this.getDataIndexes();

			for (var i = 0; i < dataIndexes.length; i++) {
				blockModels[blockModelNames[dataIndexes[i]]].setOriginalDataString(originalData[i]);
			}

			if (originalDirtyFlag) {
				for (var i = 0; i < dataIndexes.length; i++) {			
					blockModels[blockModelNames[dataIndexes[i]]].notifyDirty();
				}
			}

			return this;
		},

		getOriginalData: function () {
			var blockModels = this.getBlockModels(),				
				blockModelNames = this.getBlockModelNames(),
				dataIndexes = this.getDataIndexes(),
				originalData = [];

			for (var i = 0; i < dataIndexes.length; i++) {			
				originalData.push(blockModels[blockModelNames[dataIndexes[i]]].getOriginalData());
			}

			return originalData;
		},

		load: function (payload) {
			var blockContainer = this.getBlockContainer(),
				blockModelName = this.getName(),
				blockModels = this.getBlockModels(),				
				blockModelNames = this.getBlockModelNames(),
				dataIndexes = this.getDataIndexes();

			for (var i = 0; i < blockModelNames.length; i++) {
				var d = blockModels[blockModelNames[i]],
					leaves = [],
					dataPaths = d.getDataPaths(),
					strategy;

				if(dataIndexes.indexOf(i) >= 0) {
					strategy = this.loadEditSection;
				} else {
					strategy = this.loadReadSection;
				}

				if (dataPaths.length > 0) {
					for (var j = 0; j < dataPaths.length; j++) {
						leaves.push(this.getFilteredLeaf(payload, dataPaths[j]));
					}

					strategy(d, leaves);

					if (!this.getInitialLoad()) {
						blockContainer.checkCallbacksOnLoadOrClose(blockModelName, blockModelNames[i], leaves);
					} else {
						this.setInitialLoad(false);
					}
				}
			}

			return this;		
		},

		loadEditSection: function (d, leaves) {
			d.beforeLoad();
			d.load.apply(d, leaves);
			d.afterLoad();
		},

		loadReadSection: function (d, leaves) {
			d.load.apply(d, leaves);
		},

		getFilteredLeaf: function (root, dataPath) {
			var tuple = this.getPathAndFilter(dataPath),
				leaf = getLeaf(root, tuple.path.split('.'));

			if (typeof tuple.filter === 'function' && leaf && leaf.constructor === Array) {
				leaf = leaf.filter(tuple.filter);
			}

			return leaf;
		},

		saveComplete: function () {
			var blockModels = this.getBlockModels(),
				blockModelNames = this.getBlockModelNames(),
				dataIndexes = this.getDataIndexes();			

			for (var i = 0; i < dataIndexes.length; i++) {
				var d = blockModels[blockModelNames[dataIndexes[i]]];
				var dataIsForServer = true;
				d.setOriginalData(d.getData(dataIsForServer));
				d.notifyDirty();
			}

			return this;
		},

		reportDataModificationForAllBlockModels: function (blockContainer, clear) {
			// to be called after a load or reload from the blockContainer
			var dataIndexes = this.getDataIndexes(),
				blockModelNames = this.getBlockModelNames(),
				blockModels = this.getBlockModels(),
				blockName = this.getName();

			for (var i = 0; i < dataIndexes.length; i++) {
				var dataIndex = dataIndexes[i];

				if (dataIndex < blockModelNames.length) {
					var blockModel = blockModels[blockModelNames[dataIndex]],
						sourceBlockModelName = blockModel.getName(),
						data = blockModel.getData();
					blockContainer.checkDataDependencies('onLoadOrClose', blockName, sourceBlockModelName, data);
				}
			}
			
			if (clear) {
				for (var i = 0; i < blockModelNames.length; i++) {
					var d = blockModels[blockModelNames[i]];
					d.isDirty = false;
					d.isValid = true;
					d.notifyDirty();
					d.notifyValid(true);
				}
			}

			return this;
		},

		reportDataModification: function (blockModel) {
			this.getBlockContainer()
				.checkDataDependencies(
					'onChange',
					this.getName(), 
					blockModel.getName(), 
					blockModel.getData());
		},

		checkDataDependencies: function (triggerMethod, data, sourceBlockModelName) {
			if (!data) {
				var blockModels = this.getBlockModels();
				data = blockModels[currentModelName].getData(false);
			}

			var blockName = this.getName();
			this.getBlockContainer().checkDataDependencies(triggerMethod, blockName, sourceBlockModelName, data);
		},

		modifyByNotification: function (blockModelName, sourceData, sourceBlockModelName) {
			var blockModels = this.getBlockModels(),
				blockModel = blockModels[blockModelName];
			sourceData.push(sourceBlockModelName);
			blockModel.modifyByNotification.apply(blockModel, sourceData);
			return this;
		},

		notifyDirty: function () {
			var dataIndexes = this.getDataIndexes(),
				dirty = false;
			
			if (dataIndexes.length > 0) {	
				var blockModels = this.getBlockModels(),	
					blockModelNames = this.getBlockModelNames();

				for (var i = 0; i < dataIndexes.length; i++) {
					var dataIndex = dataIndexes[i];

					if (dataIndex < blockModelNames.length) {
						var d = blockModels[blockModelNames[dataIndex]];
						
						if (d.isDirty) {
							dirty = true;
							//Reflect current section dirty
							blockModels[blockModelNames[i]].updateTitle(this.isValid, dirty);
							break;
						}
					}
				}				
				//Reflect dirty of whole section on the 1st BlockModel (which is the view).
				blockModels[blockModelNames[0]].updateTitle(this.isValid, dirty);
				//Set this block to dirty.
				this.setIsDirty(dirty);
			}
		
			this.getBlockContainer().notifyDirty();
			return this;
		},

		notifyValid : function(){
			var dataIndexes = this.getDataIndexes(),
				valid = true;
			
			if (dataIndexes.length > 0) {	
				var blockModels = this.getBlockModels(),	
					blockModelNames = this.getBlockModelNames();

				for (var i = 0; i < dataIndexes.length; i++) {
					var dataIndex = dataIndexes[i];

					if (dataIndex < blockModelNames.length) {
						var d = blockModels[blockModelNames[dataIndex]];
						
						if (!d.isValid) {
							valid = false;
							break;
						}
					}
				}		
				
				//Set this block to valid.
				this.setIsValid(valid);
			}
		
			this.getBlockContainer().notifyValid();
			return this;
		},

		notifyDataChange : function(blockModelName, data){
			var blockName = this.getName();			
			this.getBlockContainer().notifyDataChange(blockName, blockModelName, data);
		},
		hide: function () {
			var blockModels = this.getBlockModels(),
				blockModelNames = this.getBlockModelNames();				

			for (var i = 0; i < blockModelNames.length; i++) {
				blockModels[blockModelNames[i]].hide();
			}

			return this;
		},

		show: function () {
			var blockModels = this.getBlockModels(),
				currentModelName = this.getCurrentModelName(),
				d = blockModels[currentModelName];

			d.show();

			if (d.isCollapsible()) {
				var blockModelNames = this.getBlockModelNames(),
					index = blockModelNames.indexOf(currentModelName),
					dataIndexes = this.getDataIndexes();

				if (dataIndexes.indexOf(index) === -1) {
					d.collapse();
				}
			}

			return this;
		},

		hideBlocks: function () {
			var blockNames = arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments),
				blockContainer = this.getBlockContainer();
			blockContainer.hideBlocks(blockNames);
			return this;
		},

		showBlocks: function () {
			var blockNames = arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments),
				blockContainer = this.getBlockContainer();
			blockContainer.showBlocks(blockNames);
			return this;
		},

		hideEditLink: function () {
			var blockModelNames = this.getBlockModelNames(),
				blockModels = this.getBlockModels();
			blockModels[blockModelNames[0]].hideEditLink();
			return this;
		},

		showEditLink: function () {
			var blockModelNames = this.getBlockModelNames(),
				blockModels = this.getBlockModels();
			blockModels[blockModelNames[0]].showEditLink();
			return this;
		},

		getData: function () {
			var dataIndexes = this.getDataIndexes(),
				data = {};
			
			if (dataIndexes.length > 0) {	
				var blockModels = this.getBlockModels(),	
					blockModelNames = this.getBlockModelNames(),
					leaves = [];

				for (var i = 0; i < dataIndexes.length; i++) {
					var dataIndex = dataIndexes[i];

					if (dataIndex < blockModelNames.length) {
						var d = blockModels[blockModelNames[dataIndex]];
						var dataPaths = d.getDataPaths();
						var blockModelData = d.getData(false);
						this.mergeData(data, dataPaths, blockModelData);	
					}
				}
			}

			return data;
		},

		getDataForServer: function () {
			var dataIndexes = this.getDataIndexes(),
				result = {
					paths: [],
					leaves: []
				};
			
			if (dataIndexes.length > 0) {	
				var blockModels = this.getBlockModels(),	
					blockModelNames = this.getBlockModelNames(),
					leaves = [];

				for (var i = 0; i < dataIndexes.length; i++) {
					var dataIndex = dataIndexes[i];

					if (dataIndex < blockModelNames.length) {
						var d = blockModels[blockModelNames[dataIndex]];
						result.paths.push.apply(result.paths, d.getDataPaths());
						result.leaves.push.apply(result.leaves, d.getData(true));
					}
				}
			}

			return result;
		},			

		mergeData: function (node, dataPaths, leaves) {
			if (dataPaths.length !== leaves.length) {
				throw new Error('dataPaths.length does not match leaves.length');
			}

			for (var i = 0; i < dataPaths.length; i++) {
				var modified = leaves[i] || {};
				var tuple = this.getPathAndFilter(dataPaths[i]);	
				this.deepUpdate(node, modified, tuple.path.split('.'), tuple.filter);
			}
		},

		getPathAndFilter: function (dataPath) {
			var result = {
				path: dataPath,
				filter: null
			};

			if (typeof dataPath == 'object' && Object.keys(dataPath).length === 2 && dataPath.hasOwnProperty('path') && dataPath.hasOwnProperty('filter')) {
				result.path = dataPath.path;

				if (typeof dataPath.filter === 'function') {
					result.filter = dataPath.filter;
				}
			}

			return result;
		},

		deepUpdate: function (node, leaf, paths, filter, buildArray) {
			var name = paths[0];

			if (paths.length > 1) {
		 		paths.splice(0, 1);

		 		if (typeof node[name] === 'undefined') {
		 			// create missing branch
		 			node[name] = {};
		 		}

				this.deepUpdate(node[name], leaf, paths, filter, buildArray);
			} else {
		 		if (leaf && leaf.constructor === Array) {
		 			if (typeof node[name] === 'undefined') {
		 				node[name] = [];
		 			}

					mergeArrays(node[name], leaf, filter, buildArray);
		 		} else {
			 		if (leaf !== null || typeof leaf !== 'undefined') {
			 			if (typeof node[name] === 'undefined') {
			 				node[name] = {};
			 			}

			 			copyProperties(node[name], leaf);
			 		} else {
			 			node[name] = null;
			 		}
		 		}	 		
	 		}
		}
	};
})();
