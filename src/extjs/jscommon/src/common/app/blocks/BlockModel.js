Ext.define('RS.common.blocks.BlockModel', {
    autoInit: false,
    isDirty: false,
    isValid : true,
    suspendDirtyNotification: false,

    constructor: function (block, blockModelName, modelIndex) {
        this.gluModel = null;
        var originalData = '';

        this.getBlock = function () {
            return block;
        };

        this.getName = function () {
            return blockModelName;
        };

        this.getModelIndex = function () {
            return modelIndex;
        };

        this.setOriginalData = function (d) {
            originalData = JSON.stringify(d);
        };

        this.setOriginalDataString = function (d) {
            originalData = d;
        };

        this.getOriginalData = function () {
            return originalData;
        };

    },

    beforeLoad: function () {
        this.suspendDirtyNotification = true;
    },

    load: function () {
    },

    afterLoad: function (data) {
        this.setOriginalData(this.getData(true));
        this.suspendDirtyNotification = false;
    },

    close: function (nextBlockModelName) {            
        var block = this.getBlock(),
            blockName = this.getName();
            //collapse = false;

        if (!nextBlockModelName) {
            nextBlockModelName = blockName;
            //collapse = this.isCollapsible();
        }

        block.close(blockName, nextBlockModelName);
        
        //if (collapse) {
        //    this.collapse();
        //} else {
        //   this.hide();
        //}
		this.hide();
    },

    addEditLink: function (nextBlockViewName, tooltip, isHidden, margin, callback) {
        var block = this.getBlock();

        if (typeof margin === 'undefined' || margin === null) {
            margin = '0 10 0 0';
        } else {
            margin = 0;
        }
        
        var config = {
            xtype: 'button',
            iconCls: 'rs-block-button icon-pencil with-gap',
            url: nextBlockViewName,
            tooltip: tooltip,
            text: '',
            preventDefault: true,
            isEdit: true,
            margin: margin,
			callback: callback,
            style: {
                top: '2px important'
            },
            handler: function (c, e) {
                e.preventDefault();
                e.stopPropagation();
                block.showView(config.url);
                if (typeof this.callback === 'function') {
					callback();
				}
            }

        };

        this.gluModel.header.items.push(config);
    },

    addWizardLink: function (nextBlockViewName, tooltip, isHidden, margin, callback) {
        var block = this.getBlock();

        if (typeof margin === 'undefined' || margin === null) {
            margin = '0 10 0 0';
        } else {
            margin = 0;
        }

        var config = {
            xtype: 'button',
            iconCls: 'rs-block-button icon-magic with-gap',
            url: nextBlockViewName,
            tooltip: tooltip,
            text: '',
            preventDefault: true,
            isEdit: true,
            margin: margin,
			callback: callback,
            style: {
                top: '2px important'
            },
            handler: function (c, e) {
                e.preventDefault();
                e.stopPropagation();
                block.showView(config.url);
				if (typeof this.callback === 'function') {
					callback();
				}
            }
        };

        this.gluModel.header.items.push(config);
    },

	addResizeMaxBtn: function (tooltip, hidden, callback, margin) {
        var block = this.getBlock();

        if (typeof margin === 'undefined' || margin === null) {
            margin = '0 10 0 0';
        } else {
            margin = 0;
        }
        
        var config = {
            xtype: 'button',
            iconCls: 'rs-block-button icon-resize-full with-gap',
            tooltip: tooltip,
            text: '',
            preventDefault: true,
			isResizeMaxBtn: true,
			hidden: hidden,
            margin: margin,
			callback: callback,
            style: {
                top: '2px important'
            },
			handler: function (c, e) {
                e.preventDefault();
                e.stopPropagation();
                if (typeof this.callback === 'function') {
					callback();
				}
            }
        };

        this.gluModel.header.items.push(config);
    },

	addResizeNormalBtn: function (tooltip, hidden, callback, margin) {
        var block = this.getBlock();

        if (typeof margin === 'undefined' || margin === null) {
            margin = '0 10 0 0';
        } else {
            margin = 0;
        }
        
        var config = {
            xtype: 'button',
            iconCls: 'rs-block-button icon-resize-small with-gap',
            tooltip: tooltip,
            text: '',
            preventDefault: true,
			isResizeNormalBtn: true,
			hidden: hidden,
            margin: margin,
			callback: callback,
            style: {
                top: '2px important'
            },
            handler: function (c, e) {
                e.preventDefault();
                e.stopPropagation();
                if (typeof this.callback === 'function') {
					callback();
				}
            }
        };

        this.gluModel.header.items.push(config);
    },

    modifyByNotification: function () {
    },

    notifyDirty: function () {
        if (!this.suspendDirtyNotification) {
            this.setIsDirty();
            var block = this.getBlock();
            block.reportDataModification(this);
            block.notifyDirty();
        }
    },

    notifyValid : function(valid){
        this.isValid = valid;  
        this.updateTitle(valid, this.isDirty);     
        this.getBlock().notifyValid();
    },

    notifyDataChange : function(data){
        var block = this.getBlock();
        var blockModelName = this.getName();
        block.notifyDataChange(blockModelName, data);
    },

    setIsDirty: function () {
        var data = this.getData(true),
            prior = this.getOriginalData();
        this.isDirty = JSON.stringify(data) !== prior;
        this.updateTitle(this.isValid, this.isDirty);
        return this.isDirty;
    },

    getData: function (dataIsForServer) {
        return [];
    },

    getDataPaths: function () {
        return [];
    },    

    updateTitle: function (valid,dirty) {
        var title = this.gluModel.title.replace(/ <span style=\"color:crimson;\">!<\/span>/, '');
        title = title.replace(/\*/,'');

        if (!valid){
             title = title + ' <span style="color:crimson;">!</span>';
        }
        else if (dirty) {
            title = title + '*';
        }

        this.setViewTitle(title);
        return title;
    },

    setViewTitle: function (title) {
        this.gluModel.set('title', title);
    },

    hide: function () {
        this.gluModel.set('hidden', true);

        //if (this.isCollapsible()) {
        //   this.collapse();
        //}
    },

    show: function () {
        this.gluModel.set('hidden', false);
    },

    showCurrent: function () {
        this.getBlock().show();
    },

    collapse: function () {
		//if (!this.gluModel.isSectionMaxSize) {
			this.gluModel.set('collapsed', true);
		//}
        return this;
    },

    expand: function () {
        this.gluModel.set('collapsed', false);
    },

    set: function (name, value) {
        this.gluModel.set(name, value);
        return this;
    },

    get: function (name) {
        return this.gluModel.get(name);
    },

    createNameValueItems: function () {
        var items = [], values = [];

        if (arguments.length === 1) {
            values = arguments[0];
        } else {
            values = Array.apply(null, arguments);
        }

        for (var i = 0; i < values.length; i++) {
            items.push({ 
                name: this.gluModel.localize(values[i]), 
                value: values[i] 
            });
        }
        
        return items;
    },

    isCollapsible: function () {
        return typeof this.gluModel.collapsed !== 'undefined';
    }
});