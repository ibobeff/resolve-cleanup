Ext.define('RS.common.selection.CheckboxModel', {
	extend: 'Ext.selection.CheckboxModel',
	alias: 'selection.resolvecheckboxmodel',

	columnTooltip: '',
	glyph : '',
	columnTarget: '_self',
	columnEventName: 'editAction',
	headerWidth: 40,
	forceFire: false,
	cellNavigation: false,
	currentCell: {
		row: 0,
		column: 0
	},

	/**
	 * Retrieve a configuration to be used in a HeaderContainer.
	 * This should be used when injectCheckbox is set to false.
	 */
	getHeaderConfig: function() {
		var me = this,
			showCheck = me.showHeaderCheckbox !== false,
			canSelectEntireTable = me.canSelectEntireTable === true;
		return {
			xtype: 'viewcolumn',
			align : 'center',
			isCheckerHd: showCheck,
			text: canSelectEntireTable ? '&#160;<div class="x-column-header-trigger"></div>' : '',
			width: me.headerWidth,
			sortable: false,
			draggable: false,
			resizable: false,
			hideable: false,
			hidden: (me.view.up('grid') && me.view.up('grid').hideSelModel === true),
			menuDisabled: true,
			dataIndex: '',
			tooltip: RS.common.locale.selectAllTooltip,
			viewTooltip: me.columnTooltip,
			glyph : me.glyph,
			target: me.columnTarget,
			eventName: me.columnEventName,
			forceFire: me.forceFire,
			idField: me.columnIdField || 'sys_id',
			cls: showCheck ? Ext.baseCSSPrefix + 'column-header-checkbox ' : '',
			editRenderer: me.editRenderer || me.renderEmpty,
			locked: me.hasLockedHeader(),
			childLink: me.childLink,
			childLinkIdProperty: me.childLinkIdProperty,
			listeners: {
				render: function(column, eOpts) {
					var header = column.getEl(),
						hoverCls = Ext.baseCSSPrefix + 'column-header-over';
					header.on('mouseover', function() {
						column.titleEl.addCls(hoverCls);
					});
					header.on('mouseout', function() {
						column.titleEl.removeCls(hoverCls);
					});
				}
			}
		};
	},

	navigateCell: function(evt) {
		if (!this.cellNavigation)
			return;
		var columns = this.view.getGridColumns();
		var c = this.currentCell.column;
		switch (evt.getCharCode()) {
			case evt.UP:
				if (this.currentCell.row > 0)
					this.currentCell.row--;
				break;
			case evt.DOWN:
				if (this.currentCell.row < this.store.getCount() - 1)
					this.currentCell.row++;
				break;
			case evt.LEFT:
				while (c-- > 1) {
					if (!this.canNavigateToCol(columns[c], c)) continue;
					else {
						this.currentCell.column = c;
						break;
					}
				}
				break;
			case evt.RIGHT:
				while (c++ < columns.length - 1)
					if (!this.canNavigateToCol(columns[c], c)) continue;
					else {
						this.currentCell.column = c;
						break;
					}
				break;
		}
		this.view.panel.fireEvent('navigatecell', this);
	},

	onKeyUp: function(evt) {
		this.callParent(arguments);
		this.navigateCell(evt);
	},

	onKeyDown: function(evt) {
		this.callParent(arguments);
		this.navigateCell(evt);
	},

	onKeyLeft: function(evt) {
		this.callParent(arguments);
		this.navigateCell(evt);
	},

	onKeyRight: function(evt) {
		this.callParent(arguments);
		this.navigateCell(evt);
	},

	/**
	 * Toggle between selecting all and deselecting all when clicking on
	 * a checkbox header.
	 */
	onHeaderClick: function(headerCt, header, e) {
		var me = this,
			t = e.getTarget('.x-column-header-trigger');

		if (t) {
			//Trying to select the menu instead of the checkbox
			e.stopEvent();
			var grid = header.up('grid'),
				selectionsCount = grid.getStore().getTotalCount();
			Ext.create('Ext.menu.Menu', {
				items: [{
					text: Ext.String.format(RS.common.locale.selectAll, selectionsCount),
					scope: grid,
					handler: function(btn) {
						me.preventFocus = true;
						if (isChecked) {
							me.deselectAll();
						} else {
							me.selectAll();
						}
						delete me.preventFocus;
						this.fireEvent('selectAllAcrossPages', this, true);
					}
				}]
			}).showBy(t, 'tl-bl');
		} else {
			if (header.isCheckerHd) {
				e.stopEvent();
				var isChecked = header.el.hasCls(Ext.baseCSSPrefix + 'grid-hd-checker-on');

				// Prevent focus changes on the view, since we're selecting/deselecting all records
				me.preventFocus = true;
				if (isChecked) {
					me.deselectAll();
				} else {
					me.selectAll();
				}
				delete me.preventFocus;
			}
		}
	}
});


Ext.define("RS.common.selection.ActionModel",{
	extend : "Ext.selection.CheckboxModel",
	alias : "selection.resolveactionmodel",
	
	//Default config, can be overrided by define with the same properties name when using it in selModel: {}
	editAction : false,
	editTooltip : '',
	editEventName : 'editAction',
	deleteAction : true,	
	deleteTooltip : '',
	deleteEventName : 'deleteAction',
	injectCheckbox : 'last',
	showHeaderCheckbox : false,
	header : true,
	actionHidden : false,
	idField : 'sys_id',

	getHeaderConfig : function(){
		var me = this;
		return {
			xtype : 'actioncolumn',
			deleteTooltip : me.deleteTooltip,
			deleteEventName : me.deleteEventName,
			editAction : me.editAction,
			editTooltip : me.editTooltip,
			editEventName : me.editEventName,
			sortable: false,
			draggable: false,
			resizable: false,
			align : 'center',
			width : 100,
			text : me.header ? "Actions" : null,
			idField : me.idField,
			hidden : (me.actionHidden === true) ? true : false
		}
	}
});