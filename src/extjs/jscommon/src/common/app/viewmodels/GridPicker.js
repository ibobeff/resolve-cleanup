RS.common.GridPickerVMConfig = {
	asWindowTitle: '',
	stateId: 'youneedtooverridethisnamespaceforyouowngrid',
	recordsSelections: [],
	storeConfig: null,
	displayName: '',
	store: null,
	columns: null,
	dumper: null,
	loader: null,
	width: 900,
	height: 500,
	showSysInfo: false,
	pageable: true,
	activate: function() {},
	init: function() {
		// console.log('common picker created')
		this.createStore();
		if (!this.loader)
			this.store.load();
		else
			this.loader();
	},

	createStore: function() {
		var store = Ext.create('Ext.data.Store', {
			fields: this.storeConfig.fields,
			proxy: this.storeConfig.proxy
		});
		this.set('store', store);
	},

	dump: function() {
		var flag = true;
		if (Ext.isFunction(this.dumper))
			flag = this.dumper(this.recordsSelections);
		else
			flag = this.dumper.dump.call(this.dumper.scope, this.recordsSelections);
		if (flag)
			this.doClose();
	},

	dumperText: 'dumperNeedToBeLocalized',

	cancel: function() {
		this.doClose();
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},

	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	}
};
glu.defModel('RS.common.GridPicker', Ext.applyIf({
	stateId: 'commongridpicker'
}, RS.common.GridPickerVMConfig));
glu.defModel('RS.common.AutoCompleteGridPicker', Ext.applyIf({
	stateId: 'commonautocompletegridpicker'
}, RS.common.GridPickerVMConfig));