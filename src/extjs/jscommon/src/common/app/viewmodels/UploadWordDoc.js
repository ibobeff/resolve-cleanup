glu.defModel('RS.common.UploadWordDoc', {
	mixins: ['FileUploadManager'],
	api: {
		upload: '/resolve/service/wiki/docx/uploadDocx',
		list: '',
	},

	intro: '',
	wait: false,

	allowedExtensions: ['doc', 'docx'],
	fineUploaderTemplateHidden: false,
	checkForDuplicates: true,
	allowMultipleFiles: false,

	uploaderAdditionalClass: 'uploadword',

	fileOnUploadCallback: function(manager, filename) {
		this.set('wait', true);
		this.uploader.setParams({
			wikiID: this.rootVM.id,
			wikiName: this.rootVM.name,
			userName: clientVM.username
		});
	},

	fileOnCompleteCallback: function(manager, filename, response, xhr) {
        this.set('wait', false);
		this.appendHtmlValueAfterUpload(response);
		this.doClose();
	},

	appendHtmlValueAfterUpload: function(response) {
        var htmlContents = response.data.docxContent.htmlContent;

        for (var i=0; i<htmlContents.length; i++) {
            this.editor.setValue(this.editor.getValue() + htmlContents[i]);
        }
    },
	
	editor: null,
	init: function() {
		this.set('wait', false);
		this.set('dragHereText', this.localize('dragHere'));
		this.set('intro', this.localize('uploadIntro'));
	},

	close: function() {
		this.doClose();
	}
})

