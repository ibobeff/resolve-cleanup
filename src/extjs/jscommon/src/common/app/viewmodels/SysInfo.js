glu.defModel('RS.common.SysInfo', {

	sysId: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',

	createdOn$: function() {
		return this.getParsedDate(this.sysCreatedOn);
	},
	updatedOn$: function() {
		return this.getParsedDate(this.sysUpdatedOn);
	},

	userDateFormat: function() {
		return clientVM.getUserDefaultDateFormat();
	},

	getParsedDate: function(date, f) {
		var d = date,
			formats = ['time', 'c', this.userDateFormat()];

		if (!Ext.isDate(d)) {
			Ext.Array.each(formats, function(format) {
				d = Ext.Date.parse(d, format)
				return !Ext.isDate(d)
			})
		}

		if (Ext.isDate(d)) {
			if (f)
				return Ext.Date.format(d, f);
			return Ext.Date.format(d, this.userDateFormat());
		}

		return RS.common.locale.unknownDate
	}
});
