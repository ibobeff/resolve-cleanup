glu.defModel('RS.common.UploadFile', {
	mixins: ['FileUploadManager'],
})

glu.defModel('RS.common.FileUploadManager', {
	title: '',
	name: '',
	id: '',

	allowUpload: true,
	allowRemove: true,

	uploader: null,
	api: {
		list: '/resolve/service/wiki/getAttachments',
		upload: '/resolve/service/wiki/attachment/upload',
		delete: '/resolve/service/wikiadmin/attachment/delete',
	},

	stopOnFirstInvalidFile: false,
	allowedExtensions: [],
	sizeLimit: null,
	autoUpload: true,
	checkForDuplicates: true,
	uploadBtnName: 'upload_button',
	allowMultipleFiles: true,
	allowDownload: true,
	showUploadedBy: true,
	inputName: 'name',

	fineUploaderTemplateHidden: true,
	fineUploaderTemplateHeight: 388,
	showProgress: false,

	filesOnAllCompleteCallback: function() {
		this.loadFiles();
	},

	filesStore: null,

	filesColumns: [],

	loadFiles: function() {
		if (this.api.list) {
			this.filesStore.load({
				params: {
					docFullName: this.name,
					docSysId: ''
				}
			});
		}
	},

	initStore: function() {
		var store = Ext.create('Ext.data.Store', {
			fields: ['sys_id', 'sysId', 'downloadUrl', 'tableName', {
				name: 'fileName',
				type: 'String'
			}, 'size', 'uploadedBy', {
				name: 'sysUpdatedOn',
				type: 'date',
				dateFormat: 'time',
				format: 'time'
			}, 'status'],
			sorters: [{
				property: 'fileName',
				direction: 'ASC'
			}],
			proxy: {
				type: 'ajax',
				url: this.api ? this.api.list : '/resolve/service/wiki/getAttachments',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});
		this.set('filesStore', store);
	},

	init: function() {
		this.initMixin();
		this.set('dragHereText', this.localize('dragText'));
	},

	isEditMode: false,

	initMixin: function() {
		this.initStore();
		this.set('filesColumns', [{
			header: RS.formbuilder.locale.uploadColumnName,
			flex: 2,
			dataIndex: 'fileName',
			renderer : function(value, metaData, record) {
				var name = value;
				if (record.raw.global)
					name += ' <span style="font-weight:600;">(Global)</span>';
				return name;
			}
		}, {
			header: RS.formbuilder.locale.uploadColumnSize,
			dataIndex: 'size',
			renderer: function(value) {
				return Ext.util.Format.fileSize(value);
			}
		}, {
			header: RS.formbuilder.locale.uploadColumnStatus,
			flex: 1,
			dataIndex: 'status',
			hidden: !this.isEditMode,
		}, {
			header: RS.formbuilder.locale.sysUploadedOn,
			dataIndex: 'sysUpdatedOn',
			width: 175,
			renderer: function(value) {
				//return Ext.Date.format(new Date(value), 'Y-m-d g:i:s A');
				return Ext.Date.format(value, clientVM.userDefaultDateFormat)
			}
		}, {
			header: RS.formbuilder.locale.uploadColumnUploadedBy,
			hidden: !this.showUploadedBy,
			dataIndex: 'uploadedBy'
		}, {
			xtype: 'actioncolumn',
			hideable: false,
			hidden: !this.allowDownload,
			width: 38,
			items: [{
				icon: '/resolve/images/arrow_down.png',
				tooltip: RS.formbuilder.locale.download,
				handler: function(grid, rowIndex, colIndex) {
					var rec = grid.getStore().getAt(rowIndex),
						url = rec.get('downloadUrl');
					if (url) grid.ownerCt._vm.downloadURL(url);
				}
			}]
		}])

		this.loadFiles();

		setTimeout(function(){
			this.initUploader();
		}.bind(this), 100)
	},



	initUploader: function() {
		var me = this;
		var extraDropzones = [];
		var attachments_grid = document.getElementById('attachments_grid');
		if (attachments_grid) {
			extraDropzones.push(attachments_grid.parentElement.parentElement);
		}

		var fileuploadMaxSize = (clientVM.fileuploadMaxSize || 500) * 1000000; // in Bytes

		this.uploader = new qq.FineUploader({
			//debug: true,
			autoUpload: this.autoUpload,
			template: 'qq-template',
			element: document.getElementById('fine-uploader'),
			button: document.getElementById(this.uploadBtnName),
			multiple: this.allowMultipleFiles,
			dragAndDrop: {
				extraDropzones: extraDropzones
			},
			request: {
				endpoint: this.api ? this.api.upload : '/resolve/service/wiki/attachment/upload',
				filenameParam: 'overriddenFilename',  // Allowing filename being editable; used by the back end
				inputName: 'name',
				params: {
					recordId: this.name,
					docFullName: this.name
				}
			},
			validation: {
				allowedExtensions: this.allowedExtensions,
				sizeLimit: this.sizeLimit || fileuploadMaxSize,
				stopOnFirstInvalidFile: this.stopOnFirstInvalidFile
			},
			callbacks: {
				onValidateBatch: function(fileOrBlobDataArray) {
					if (me.checkForDuplicates) {
						var duplicateFiles = [];
						for (var i=0; i<fileOrBlobDataArray.length; i++) {
							var file = fileOrBlobDataArray[i];
							var dup = me.filesStore.findRecord('fileName', file.name, 0, false, false, true);
							if (dup) {
								duplicateFiles.push(file.name);
							}
						}

						if (duplicateFiles.length) {
							var promise = new qq.Promise();

							var msg = duplicateFiles.join();
							if (duplicateFiles.length > 1) {
								msg += ' ' + RS.common.locale.FileUploadManager.duplicatesDetected;
							} else {
								msg += ' ' + RS.common.locale.FileUploadManager.duplicateDetected;
							}

							me.message({
								title: RS.common.locale.FileUploadManager.duplicateWarn,
								msg: msg,
								buttons: Ext.MessageBox.YESNO,
								buttonText: {
									yes: RS.common.locale.FileUploadManager.override,
									no: RS.common.locale.FileUploadManager.no
								},
								scope: this,
								fn: function(btn) {
									if (btn === 'yes') {
										promise.success();
									} else {
										promise.failure();
									}
								}
							});

							return promise;
						}
					}
				},
				onSubmitted: function(id, name) {
					if (Ext.isFunction(me.fileOnSubmittedCallback)) {
						me.fileOnSubmittedCallback(this, id, name);
					}
				},
				onCancel: function(id, name) {
					if (Ext.isFunction(me.fileOnCancelCallback)) {
						me.fileOnCancelCallback(this, id, name)
					}
				},
				onUpload: function(id, name) {
					if (Ext.isFunction(me.fileOnUploadCallback)) {
						me.fileOnUploadCallback(this, name)
					}
				},
				onProgress: function(id, name, uploadedBytes, totalBytes) {
					if (Ext.isFunction(me.fileOnProgressCallback)) {
						me.fileOnProgressCallback(this, id, name, uploadedBytes, totalBytes)
					}
				},
				onError: function(id, name, errorReason, xhr) {
					if (Ext.isFunction(me.fileOnErrorCallback)) {
						me.fileOnErrorCallback(this, name, errorReason, xhr)
					}
				},
				onComplete: function(id, name, responseJSON, xhr) {
					if (Ext.isFunction(me.fileOnCompleteCallback)) {
						me.fileOnCompleteCallback(this, name, responseJSON, xhr)
					}
				},
				onAllComplete: function(succeeded, failed) {
					if (!failed.length) {
						clientVM.displaySuccess(RS.common.locale.FileUploadManager.uploadSuccess);
					}

					if (Ext.isFunction(me.filesOnAllCompleteCallback)) {
						me.filesOnAllCompleteCallback(this, succeeded, failed)
					}
				}
			},
			showMessage: function(message) {
				clientVM.displayError(message);
			}
		});
	},

	dragHereText: '',

	progressHtml$: function() {
		if (this.showProgress) {
			return '<div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container"><div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div></div>\n';
		} else {
			return '';
		}
	},

	uploaderAdditionalClass: '',

	fineUploaderTemplate$: function() {
		return '\n'+
		'<div id="fine-uploader"></div>\n'+
		'<script type="text/template" id="qq-template">\n'+
			'<div class="'+this.uploaderAdditionalClass+' qq-uploader-selector qq-uploader" qq-drop-area-text="'+this.dragHereText+'">\n'+
				this.progressHtml+
				'<div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>\n'+
					'<span class="qq-upload-drop-area-text-selector"></span>\n'+
				'</div>\n'+
				'<span class="qq-drop-processing-selector qq-drop-processing">\n'+
					'<span>'+RS.common.locale.FileUploadManager.processingDroppedFiles+'</span>\n'+
					'<span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>\n'+
				'</span>\n'+
				'<ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">\n'+
					'<li>\n'+
						'<div class="qq-progress-bar-container-selector">\n'+
							'<div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>\n'+
						'</div>\n'+
						'<span class="qq-upload-spinner-selector qq-upload-spinner"></span>\n'+
						'<img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>\n'+
						'<span class="qq-upload-file-selector qq-upload-file"></span>\n'+
						'<input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">\n'+
						'<span class="qq-btn-container">\n'+
							'<span class="qq-upload-size-selector qq-upload-size"></span>\n'+
							'<span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="'+RS.common.locale.FileUploadManager.editFilename+'"></span>\n'+
							'<button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">'+RS.common.locale.FileUploadManager.cancel+'</button>\n'+
							'<button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">'+RS.common.locale.FileUploadManager.retry+'</button>\n'+
							'<button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">'+RS.common.locale.FileUploadManager.delete+'</button>\n'+
						'</span>\n'+
						'<span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>\n'+
					'</li>\n'+
				'</ul>\n'+
				'<dialog class="qq-alert-dialog-selector">\n'+
					'<div class="qq-dialog-message-selector"></div>\n'+
					'<div class="qq-dialog-buttons">\n'+
						'<button type="button" class="qq-cancel-button-selector">'+RS.common.locale.FileUploadManager.close+'</button>\n'+
					'</div>\n'+
				'</dialog>\n'+
				'<dialog class="qq-confirm-dialog-selector">\n'+
					'<div class="qq-dialog-message-selector"></div>\n'+
					'<div class="qq-dialog-buttons">\n'+
						'<button type="button" class="qq-cancel-button-selector">'+RS.common.locale.FileUploadManager.no+'</button>\n'+
						'<button type="button" class="qq-ok-button-selector">'+RS.common.locale.FileUploadManager.yes+'</button>\n'+
					'</div>\n'+
				'</dialog>\n'+
				'<dialog class="qq-prompt-dialog-selector">\n'+
					'<div class="qq-dialog-message-selector"></div>\n'+
					'<input type="text">\n'+
					'<div class="qq-dialog-buttons">\n'+
						'<button type="button" class="qq-cancel-button-selector">'+RS.common.locale.FileUploadManager.cancel+'</button>\n'+
						'<button type="button" class="qq-ok-button-selector">'+RS.common.locale.FileUploadManager.ok+'</button>\n'+
					'</div>\n'+
				'</dialog>\n'+
			'</div>\n'+
		'</script>\n'
	},

	multiSelect: true,

	selections: [],

	selectionChanged: function(selected) {
		this.set('selections', selected)
	},

	uploadFile: function() {
	},

	deleteIsEnabled$: function() {
		return this.selections.length > 0;
	},
	delete: function() {
		this.message({
			title: RS.formbuilder.locale.confirmRemove,
			msg: this.selections.length === 1 ? RS.formbuilder.locale.confirmRemoveFile : Ext.String.format(RS.formbuilder.locale.confirmRemoveFiles, this.selections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: RS.formbuilder.locale.removeFile,
				no: RS.formbuilder.locale.cancel
			},
			scope: this,
			fn: this.removeSelectedFiles
		});
	},

	removeSelectedFiles: function(button) {
		if (button === 'yes') {
			var ids = [],
				fileIds = [];
			Ext.each(this.selections, function(selection) {
				ids.push(selection.get('sys_id'));
				fileIds.push(selection.get('id'));
			});
			if (this.name && this.name.length > 1) {
				Ext.Ajax.request({
					url: this.api ? this.api.delete : '/resolve/service/wikiadmin/attachment/delete', // Configurable
					params: {
						docSysId: '',
						docFullName: this.name,
						ids: ids
					},
					scope: this,
					success: function(resp) {
						var response = RS.common.parsePayload(resp);
						if (response.success) {
							this.loadFiles();
						} else {
							clientVM.displayError(RS.common.locale.FileUploadManager.deleteFailed);
						}
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}
	},

	downloadURL: function(url) {
		var urlparts = url.split('?');
		clientVM.getCSRFToken_ForURI(urlparts[0], function(data, uri) {
			var csrftoken = '?' + data[0] + '=' + data[1];
			var newUrl = uri + csrftoken + '&' + urlparts[1];
			var iframe = document.getElementById("hiddenDownloader");
			if (iframe === null) {
				iframe = document.createElement('iframe');
				iframe.id = "hiddenDownloader";
				iframe.style.display = 'none';
				document.body.appendChild(iframe);
			}
			iframe['src'] = newUrl.replace(/javascript\:/g, '');;
		});
	},

	reload: function() {
		this.loadFiles();
	},

	close: function() {
		this.doClose()
	}
})
