glu.defModel('RS.common.WikiDocSearch', {
	store: {
		mtype: 'store',
		fields: ['ufullname', 'umodule', 'id',
			'sysOrganizationName', {
				name: 'sysCreatedOn',
				type: 'date',
				format: 'time',
				dateFormat: 'time'
			},
			'sysCreatedBy', {
				name: 'sysUpdatedOn',
				type: 'date',
				format: 'time',
				dateFormat: 'time'
			}, 'sysUpdatedBy'
		],
		remoteSort: true,
		sorters: [{
			property: 'ufullname',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/common/wiki/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	typesStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	type: 'All',
	when_type_changes_update_grid: {
		on: 'typeChanged',
		action: function() {
			this.store.load()
		}
	},

	typesSelection: null,

	columns: null,

	searchQuery: '',

	dumper: null,

	wikiSelections: [],

	wikiSelection: null,

	single: false,

	init: function() {
		this.typesStore.add([{
			name: 'All',
			value: 'All'
		}, {
			name: 'Documents',
			value: 'Document'
		}, {
			name: 'Runbook',
			value: 'Runbook'
		}, {
			name: 'Decision Tree',
			value: 'DecisionTree'
		}]);
		this.set('columns', [{
			header: '~~ufullname~~',
			text: '~~ufullname~~',
			dataIndex: 'ufullname',
			filterable: true,
			flex: 2
		}, {
			header: '~~sysCreatedOn~~',
			text: '~~sysCreatedOn~~',
			dataIndex: 'sysCreatedOn',
			filterable: true,
			width: 200,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}, {
			header: '~~sysCreatedBy~~',
			text: '~~sysCreatedBy~~',
			dataIndex: 'sysCreatedBy',
			filterable: true,
			width: 120
		}, {
			header: '~~sysUpdatedOn~~',
			text: '~~sysUpdatedOn~~',
			dataIndex: 'sysUpdatedOn',
			filterable: true,
			width: 200,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}, {
			header: '~~sysUpdatedBy~~',
			text: '~~sysUpdatedBy~~',
			dataIndex: 'sysUpdatedBy',
			filterable: true,
			width: 120
		}, {
			header: '~~sysOrganizationName~~',
			text: '~~sysOrganizationName~~',
			dataIndex: 'sysOrganizationName',
			hidden: true,
			filterable: false,
			width: 130
		}, {
			header: '~~sys_id~~',
			text: '~~sys_id~~',
			dataIndex: 'id',
			hidden: true,
			filterable: true,
			width: 300
		}]);
		this.store.on('beforeload', function(store, op) {
			op.params = op.params || {};
			op.params.type = this.type;
		}, this);
		this.store.load();
	},

	choose: function() {
		var param = null;
		var close = false;
		if (this.single)
			param = this.wikiSelection;
		else
			param = this.wikiSelections;
		if (!this.dumper)
			return;
		if (Ext.isFunction(this.dumper))
			close = this.dumper(param);
		else
			close = this.dumper.dump.call(this.dumper.scope, param);

		if (close)
			this.doClose();

	},

	cancel: function() {
		this.doClose();
	}
});