glu.defModel('RS.common.ContentPicker',{
	title : '',
	selected: null,	
	content : '',
	contentType : 'page',
	contentStore : null,
	selectedNamespace: '',
	columns : '',
	API : {
		getModule : '/resolve/service/nsadmin/list',
		getpage : '/resolve/service/wiki/list',
		getautomation : '/resolve/service/wikiadmin/listRunbooks',
		getplaybook : '/resolve/service/wiki/listPlaybookTemplates'
	},
	contentModelMap : {
		page : 'RS.wiki.Main',
		automation : 'RS.wiki.Main'
	},
	moduleStore: {
		mtype: 'treestore',
		fields: ['id', 'unamespace'],
		root: {
			unamespace: 'All'
		}		
	},
	expandingToNode : false,
	init: function() {
		var contentType = this.contentType.toLowerCase();
		this.set('title', this.localize(contentType));
		var storeUrl = this.API['get' + contentType];
		this.set('columns',[{
			header: '~~name~~',
			dataIndex: 'ufullname',
			filterable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()));
		var store = Ext.create('Ext.data.Store', {		
			sorters: ['ufullname'],
			fields: ['id', 'ufullname', 'usummary', 'uresolutionBuilderId','uwikiParameters','uisRoot'].concat(RS.common.grid.getSysFields()),
			remoteSort: true,
			proxy: {
				type: 'ajax',
				url: storeUrl,
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});
		store.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {};
			var filter = [];
			if (this.selectedNamespace && this.selectedNamespace.toLowerCase() != 'all')
				filter.push({
					field: 'unamespace',
					type: 'auto',
					condition: 'equals',
					value: this.selectedNamespace
				});			
			Ext.apply(operation.params, {
				filter: Ext.encode(filter)
			})
		}, this);
		this.set('contentStore', store);
		this.contentStore.load();

		this.moduleStore.on('expand', function(node) {
			if (!this.expandingToNode)
				this.expandModuleNode(node);
		}, this);
		this.moduleStore.getRootNode().expand();
	},
	expandModuleNode: function(node) {		
		this.ajax({
			url: this.API['getModule'],
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(respData.message);
					return;
				}
				node.removeAll();
				var childNodes = [];
				Ext.Object.each(respData.records, function(i,r) {
					childNodes.push({
						id: Ext.data.IdGenerator.get('uuid').generate(),
						unamespace: r.unamespace,
						leaf: true
					});
				}, this)
				node.appendChild(childNodes);
				this.set('expandingToNode', true);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}			
		});
	},
	menuPathTreeSelectionchange: function(selected, eOpts) {
		if (selected.length > 0) {
			var nodeId = selected[0].get('unamespace')
			this.set('selectedNamespace', nodeId);
			this.contentStore.load();
		}
	},
	editContent: function(name) {		
		clientVM.handleNavigation({
			modelName: this.contentModelMap[this.contentType],
			target: '_blank',
			params: {
		/*		activeTab: (this.runbookOnly ? 2 : 0),*/
				name: name
			}
		})		
	},
	dumper: null,
	dump: function() {
		if (Ext.isFunction(this.dumper))
            this.dumper(this.selected);
        else {
            var scope = this.dumper.scope || this;
            this.dumper.dump.call(scope, this.selected);
        }
		this.doClose()
	},
	dumpIsEnabled$: function() {
		return !!this.selected
	},
	cancel: function() {		
		this.doClose()
	}	
});