glu.defModel('RS.common.AccessRights', {
	accessRights: null,

	sys_id: '',

	rightsLoader: function() {},

	title: '',

	defaultRoles: false,

	defaultRolesIsVisible: true,

	bodyPadding: '0px',

	defaultRolesText: '~~defaultRolesText~~',

	isWindow: false,

	local: false,

	rightsShown: ['admin', 'write', 'read'],

	fields: ['uname'],

	columns: null,

	rightsIsEnabled$: function() {
		if (this.defaultRoles)
			this.set('rightsSelections', []);
		return !this.defaultRoles
	},

	dumper: function() {
		this.parentVM.set('accessRights', this.toCVS());
	},

	rightsSelections: [],

	activate: function(screen, params) {
		this.set('isWindow', params.isWindow);
	},

	init: function() {
		Ext.each(this.rightsShown, function(r) {
			this.fields.push({
				name: r,
				type: 'bool'
			});
		}, this);
		this.createStore(this.local);
		if (!this.title)
			this.set('title', this.localize('AccessRights'));
		this.rightsLoader();
		if (this.isWindow)
			this.set('bodyPadding', '10px');
		this.set('defaultRolesText', this.localize('defaultRolesText'));
	},

	createStore: function(local) {
		var cols = [];
		Ext.each(this.fields, function(f) {
			var nonBoolCol = {
				filterable: true,
				sortable: true,
				flex: 1
			};
			var boolCol = {
				xtype: 'checkcolumn',
				stopSelection: false,
				width: 70
			};
			var c = {
				header: '~~' + (f.name || f) + (f.type == 'bool' ? 'Right' : '') + '~~',
				dataIndex: f.name || f,
				hideable: false
			};
			if (f.type == 'bool')
				Ext.apply(c, boolCol);
			else
				Ext.apply(c, nonBoolCol);
			cols.push(c)
		});

		this.set('columns', cols);
		if (local) {
			this.set('accessRights', new Ext.create('Ext.data.Store', {
				mtype: 'store',
				fields: this.fields,
				proxy: {
					type: 'memory',
					reader: {
						type: 'json',
						root: 'records'
					}
				}
			}));
			return;
		}
		this.set('accessRights', new Ext.create('Ext.data.Store', {
			mtype: 'store',
			fields: this.fields,
			remoteSort: true,
			baseParams : {
				includePublic : "true"
			},
			sorters: [{
				property: 'uname',
				direction: 'ASC'
			}],
			proxy: {
				type: 'ajax',
				url: '/resolve/service/common/roles/list',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		}));
		/*
		this.on('load', function(store, rec, suc) {
			if (!suc)
				clientVM.displayError('listRolesErrMsg');

		});
		*/
	},

	clearGrid: function() {
		this.set('sys_id', '')
		this.accessRights.loadData([], false);
	},

	loadRights: function(data) {
		this.set('sys_id', data.sys_id);
		var recs = this.cvsToRows(data);
		this.accessRights.add(recs);
	},

	cvsToRows: function(data) {
		var revert = {};
		this.accessRights.loadData([], false);

		Ext.each(this.rightsShown, function(r) {
			var cvs = data['u' + r + 'Access']
			if (!cvs)
				return;
			var roles = cvs.split(',');
			Ext.each(roles, function(role) {
				if (!role)
					return;
				if (!revert[role])
					revert[role] = {};
				revert[role][r] = true;
			});
		});
		var recs = [];
		for (role in revert) {
			revert[role].uname = role;
			recs.push(revert[role]);
		}

		return recs;
	},

	addAccessRights: function(rights) {
		Ext.each(rights, function(accessRight) {
			if (this.accessRights.find('uname', accessRight.get('uname')) != -1)
				return;
			this.accessRights.add(Ext.applyIf(accessRight.data, {
				'admin': true,
				'write': true,
				'read': true,
				'execute': true
			}));
		}, this);
	},

	showRoleList: function() {
		this.open({
			mtype: 'RS.common.KeywordGridPicker',
			width: 710,
			title: this.localize('rolesDisplayName'),
			storeFields: ['uname'],
			searchFields: ['uname'],
			columns: [{
				header: this.localize('uname'),
				dataIndex: 'uname',
				filterable: true,
				flex: 1
			}],
			storeUrl: '/resolve/service/common/roles/list',
			baseParams : {
				includePublic : "true"
			},
			dumperText: this.localize('add'),
			dumper: (function(self) {
				return function(records) {
					Ext.each(records, function(r, idx) {
						self.addAccessRights(r);
					});
					return true;
				}
			})(this)
		});
	},

	showRoleListIsEnabled$: function() {
		return this.rightsIsEnabled;
	},

	toCVS: function(reverse) {
		var accessRights = {
			sys_id: this.sys_id
		};

		Ext.each(this.rightsShown, function(shown) {
			accessRights['u' + shown + 'Access'] = [];
		});

		this.accessRights.each(function(r) {
			Ext.each(this.rightsShown, function(shown) {
				if (reverse ? !r.get(shown) : r.get(shown))
					accessRights['u' + shown + 'Access'].push(r.get('uname'));
			}, this);
		}, this);

		Ext.each(this.rightsShown, function(shown) {
			accessRights['u' + shown + 'Access'] = accessRights['u' + shown + 'Access'].join(',');
		});

		return accessRights;
	},

	removeAccessRight: function() {
		this.accessRights.remove(this.rightsSelections);
	},

	removeAccessRightIsEnabled$: function() {
		return this.rightsSelections.length > 0 && this.rightsIsEnabled;
	},

	// removeAccessRightIsVisible$: function() {
	// 	return !this.local;
	// },

	dump: function() {
		if (Ext.isFunction(this.dumper))
			this.dumper(this.rightsSelections);
		else
			this.dumper.dump.call(this.dumper.scope, this.rightsSelections);
		this.doClose();
	},

	cancel: function() {
		this.doClose();
	}
});