glu.defModel('RS.common.KeywordGridPicker', {
	keyword: '',
	storeFields: [],
	searchFields: [],
	storeUrl: '',
	columns: null,
	width: 600,
	height: 500,
	title: '',
	dumperText: '',
	store: null,
	sorters: null,
	actions: [],
	buttons: {
		mtype: 'list'
	},
	showSysInfo: false,
	init: function() {
		var me = this;
		if (this.actions.length > 0)
			Ext.each(this.actions, function(action) {
				this.buttons.add({
					mtype: 'RS.common.GridButton',
					text: action.text,
					dumper: function() {
						action.action.apply(me);
					},
					cls : 'rs-med-btn rs-btn-dark'
				});
			}, this);
		else {
			this.buttons.add({
				mtype: 'RS.common.GridButton',
				text: this.dumperText,
				dumper: function() {
					me.dump();
					me.close();
				},
				cls : 'rs-med-btn rs-btn-dark'
			})
		}

		this.buttons.add({
			mtype: 'RS.common.GridButton',
			text: this.localize('close'),
			dumper: function() {
				me.doClose();
			},
			cls : 'rs-med-btn rs-btn-light'
		});
		this.set('store', Ext.create('Ext.data.Store', {
			fields: this.storeFields,
			remoteSort: true,
			sorters: this.sorters ? this.sorters : [{
				property: this.storeFields[0],
				direction: 'ASC'
			}],
			proxy: {
				type: 'ajax',
				url: this.storeUrl,
				baseParams : this.baseParams || {},
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		}));
		this.store.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {};
			var filter = [];
			if (this.keyword)
				Ext.each(this.searchFields, function(field) {
					filter.push({
						field: field,
						type: 'auto',
						condition: 'contains',
						value: this.keyword
					});
				}, this)
			Ext.apply(operation.params, {
				operator: 'or',
				filter: Ext.encode(filter)
			})
		}, this);
		this.store.load()
	},

	when_keyword_changes_update_grid: {
		on: ['keywordChanged'],
		action: function() {
			this.store.loadPage(1)
		}
	},

	recordsSelections: [],

	dumper: null,

	dump: function() {
		this.dumper(this.recordsSelections);
		this.close();
	},

	dumpIsVisible$: function() {
		return this.buttons.length == 0;
	},

	close: function() {
		this.doClose()
	}
});
glu.defModel('RS.common.GridButton', {
	text: 'button',
	dumper: function() {
		alert('test');
	},
	dump: function() {
		this.dumper();
	}
});


