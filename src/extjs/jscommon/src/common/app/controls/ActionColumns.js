Ext.define('RS.common.grid.column.ActionColumn', {
	extend: 'Ext.grid.column.Action',
	alias: 'widget.actioncolumn',

	idField: 'sys_id',

	constructor: function(config) {
		var me = this;
		var items = [{
				icon: '/resolve/images/common/delete.png',
				tooltip: config.deleteTooltip ||"Delete",
				handler: function(gridView, rowIndex, colIndex) {
					var rec = gridView.getStore().getAt(rowIndex),
						idField = rec ? rec.get(me.idField) : 'sys_id';
					gridView.ownerCt.fireEvent(me.deleteEventName, gridView , idField, rowIndex);
				}
			}]

		if(config.editAction){
			items.push(
			{
				icon: '/resolve/images/common/edit.png',
				tooltip: config.editTooltip || "Edit",
				handler: function(gridView, rowIndex, colIndex) {
				
				}
			});
		}
		Ext.applyIf(config, { 
			items : items
		 });
		me.callParent([config]);
	}
})