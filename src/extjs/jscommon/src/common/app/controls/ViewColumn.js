Ext.define('RS.common.grid.column.ViewColumn', {
	extend: 'Ext.grid.column.Action',
	alias: 'widget.viewcolumn',

	hideable: false,

	url: '',
	target: '_blank',
	eventName: 'showDetail',
	draggable: false,
	idField: 'sys_id',
	childLink: '',
	childLinkIdProperty: 'id',
	forceFire: false,

	constructor: function(config) {
		var me = this;
		Ext.applyIf(config, {
			items: [{				
				glyph : config.glyph || null,
				icon: '/resolve/images/common/detail.png',
				tooltip: config.viewTooltip || RS.common.locale.gotoDetailsColumnTooltip,
				handler: function(gridView, rowIndex, colIndex, item, e, rec) {
					var sys_id = rec ? rec.get(me.idField) : '';
					if (sys_id || this.forceFire) {
						if (me.target == '_self') gridView.ownerCt.fireEvent(me.eventName, gridView, sys_id || rec);
						else {
							//open new window to the url configured for this reference table
							var split = me.url.split('?'),
								params = {};
							if (split.length > 1) {
								params = Ext.Object.fromQueryString(split[1]);
							}
							window.open(split[0] + '?' + Ext.urlEncode(Ext.applyIf({
								sys_id: sys_id
							}, params)));
						}
					}
				}
			}]
		});

		me.callParent([config]);
	},
	/*defaultRenderer: function(v, meta, record, rowIdx, colIdx, store, view) {
		var me = this,
			prefix = Ext.baseCSSPrefix,
			scope = me.origScope || me,
			items = me.items,
			len = items.length,
			i = 0,
			item, ret, disabled, tooltip, location;

		// Allow a configured renderer to create initial value (And set the other values in the "metadata" argument!)
		// Assign a new variable here, since if we modify "v" it will also modify the arguments collection, meaning
		// we will pass an incorrect value to getClass/getTip
		ret = Ext.isFunction(me.origRenderer) ? me.origRenderer.apply(scope, arguments) || '' : '';

		location = window.location.href.split('#')[0];

		meta.tdCls += ' ' + Ext.baseCSSPrefix + 'action-col-cell';
		for (; i < len; i++) {
			item = items[i];

			disabled = item.disabled || (item.isDisabled ? item.isDisabled.call(item.scope || scope, view, rowIdx, colIdx, item, record) : false);
			tooltip = disabled ? null : (item.tooltip || (item.getTip ? item.getTip.apply(item.scope || scope, arguments) : null));

			// Only process the item action setup once.
			if (!item.hasActionConfiguration) {

				// Apply our documented default to all items
				item.stopSelection = me.stopSelection;
				item.disable = Ext.Function.bind(me.disableAction, me, [i], 0);
				item.enable = Ext.Function.bind(me.enableAction, me, [i], 0);
				item.hasActionConfiguration = true;
			}

			ret += (me.childLink ? '<a href="' + location + '#' + me.childLink + '/' + me.childLinkIdProperty + '=' + record.get(me.childLinkIdProperty) + '">' : '') +
				'<img alt="' + (item.altText || me.altText) + '" src="' + (item.icon || Ext.BLANK_IMAGE_URL) +
				'" class="' + prefix + 'action-col-icon ' + prefix + 'action-col-' + String(i) + ' ' + (disabled ? prefix + 'item-disabled' : ' ') +
				' ' + (Ext.isFunction(item.getClass) ? item.getClass.apply(item.scope || scope, arguments) : (item.iconCls || me.iconCls || '')) + '"' + (tooltip ? ' data-qtip="' + tooltip + '"' : '') + ' />' + (me.childLink ? '</a>' : '');
		}
		return ret;
	}*/
});