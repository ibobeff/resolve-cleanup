//the UX router won't consider params before # so add the temporay hack to let wiki accept url params(which is before#,param after # are only for router use)
function openLink(link, config) {
	var result = false;

	if(link.href) {
		//if the link has ?, then need to reload the page to get the url param updated, temporary solution, need to solve it in client
		if (link.href.indexOf('?') != -1) {
			result = true;
		} else {
			//if the link does not have ? do redirect using client

			if (getResolveRoot().location.href.indexOf('rsclient.jsp') > -1) {
				var params = {};
				
				if (link.href.match(/#.+$/g)) {
					var rout = link.href.match(/#.+$/g)[0];
					var split = rout.split('\/');
					var app = split[0];
					var query = split[1];
					var params = Ext.urlDecode(query || '');
				}

				if (config.name) {
					Ext.defer(function() {
						clientVM.handleNavigation({
							modelName: 'RS.wiki.Main',
							params: Ext.applyIf(params, config),
							target: link.target
						});
					}, 1);
					
					result = false;
				} else {
					result = true;
				}
			}
		}
	}

	return result;
}

function refreshModelLinks() {
	var modelFrames = Ext.query('iframe[src^=/resolve/service/wiki/viewmodel]');
	Ext.Array.forEach(modelFrames, function(frame) {
		var splits = Ext.fly(frame).getAttribute('src').split('?'),
			params = Ext.Object.fromQueryString(splits[1]);
		if (params.PROBLEMID != clientVM.problemId) {
			params.PROBLEMID = clientVM.problemId
			frame.src = splits[0] + '?' + Ext.Object.toQueryString(params)
		}
	})
}

//Processes legacy wiki linkes that used to go directly to /resolve/service/wiki/view/namespace/name and keep them in the client.
//The link macro already does this processing, but apparently users were using their own links to the wiki documents directly
//so this will handle the case where the link macro didn't catch any pre-existing links to the old pages and make everything
//work properly with the new rsclient.jsp
function processLegacyWikiLinks() {
	var links = Ext.getBody().query('[href*=]'),
		link;
	Ext.Array.forEach(links, function(l) {
		link = Ext.get(l)
		if (link.dom.href) {
			var index = link.dom.href.indexOf('/resolve/service/wiki/view/');
			
			if(index > -1 && !link.getAttribute('onclick') && (!link.getAttribute('target') || link.getAttribute('target') == '_top')) {
				var start = index + '/resolve/service/wiki/view/'.length;
				link.set({
					onclick: Ext.String.format('return openLink(this, {name: \'{0}\'})', decodeURI(link.dom.href.substring(start).replace('/', '.')))
				});
			}
		}
	})
}

function gotoURL(windowName, url) {
	var params = windowName.substring(windowName.indexOf('?') + 1);
	var target = '';
	var arrOfStrings = params.split('&');
	for (var i = 0; i < arrOfStrings.length; i++) {
		var value = arrOfStrings[i];
		if (value && value.indexOf('=') > -1) {
			var kvarr = value.split('=');
			if (kvarr[0] == 'target' && Ext.String.trim(kvarr[1]) != '') {
				target = kvarr[1];
				break;
			}
		}
	}
	target = Ext.String.trim(target);
	if (!target) {
		doAddTab(windowName, url);
	} else {
		doOpenWindow(windowName, target, url)
	}
}

function doOpenWindow(windowName, target, url) {
		window.open(url, target);
	}
	//always add a new tab or open a new window

function doAddTab(windowName, url) {
	if (window.navigator.userAgent.indexOf('MSIE') != -1 || !windowName || windowName.indexOf('.') > 0) {
		window.open(url, '_blank');
	} else {
		if (windowName) {
			windowName = windowName.replace('#', '');
		}
		window.open(url, windowName);
	}
}