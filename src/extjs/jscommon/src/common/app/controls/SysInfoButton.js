Ext.define('RS.common.controls.SysInfoButton', {
	extend: 'Ext.button.Button',
	alias: 'widget.sysinfobutton',

	ui: 'system-info-button-small',
	iconCls: 'rs-icon-button icon-info-sign',

	config: {
		dateFormat: 'c',
		sysId: '',
		sysCreated: '',
		sysCreatedBy: '',
		sysUpdated: '',
		sysUpdatedBy: '',
		sysOrg: '',
		wikiName: ''
	},

	initComponent: function() {
		if (Ext.isGecko) this.setIconCls(this.iconCls + ' rs-icon-firefox')
		this.tooltip = this.tooltip || RS.common.locale.sysInfoButtonTooltip
		this.handler = this.handleButtonClick
		this.overflowText = RS.common.locale.sysInfoButtonTitle
		this.callParent()
	},

	handleButtonClick: function(button) {
		var me = button;
		if (!button.popup) {
			button.popup = Ext.create('Ext.window.Window', {
				baseCls: Ext.isIE8m ? 'x-window' : 'x-panel',
				ui: Ext.isIE8m ? 'default' : 'sysinfo-dialog',
				draggable: false,
				height: 280,
				width: 400,
				title: RS.common.locale.sysInfoButtonTitle,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				bodyPadding: '10px',
				defaults: {
					labelWidth: 100
				},
				items: [{
						xtype: 'displayfield',
						fieldLabel: RS.common.locale.sysId,
						value: button.getSysId()
					}, {
						xtype: 'displayfield',
						fieldLabel: RS.common.locale.sysCreated,
						value: button.getParsedDate(button.getSysCreated())
					}, {
						xtype: 'displayfield',
						fieldLabel: RS.common.locale.sysCreatedBy,
						value: button.getSysCreatedBy()
					}, {
						xtype: 'displayfield',
						fieldLabel: RS.common.locale.sysUpdated,
						value: button.getParsedDate(button.getSysUpdated())
					}, {
						xtype: 'displayfield',
						fieldLabel: RS.common.locale.sysUpdatedBy,
						value: button.getSysUpdatedBy()
					}
					/*, {
					xtype: 'displayfield',
					fieldLabel: RS.common.locale.sysOrg,
					value: button.getSysOrg()
				}*/
				],				
				buttons: {
					xtype: 'toolbar',
					cls: 'sysinfo-toolbar-footer rs-dockedtoolbar',
					items: [{
						text: RS.common.locale.pageInfo,
						hidden: !button.getWikiName(),
						handler: function(button) {
							var model = glu.model({
								mtype: 'RS.wiki.PageInfo',
								name: me.getWikiName()
							});
							model.init()
							glu.openWindow(model)
							button.up('window').close()
						}
					}, {
						text: RS.common.locale.close,
						cls : 'rs-med-btn rs-btn-dark',
						handler: function(button) {
							button.up('window').close()
						}
					}]
				},
				listeners: {
					close: function() {
						button.popup = null;
					}
				}
			})
			button.popup.show()
			button.popup.alignTo(button.getEl().id, 'tr-br')
		} else {
			button.popup.close()
		}
	},

	getParsedDate: function(date) {
		var d = date,
			formats = ['time', 'c', this.getDateFormat()];

		if (!Ext.isDate(d)) {
			Ext.Array.each(formats, function(format) {
				d = Ext.Date.parse(d, format)
				return !Ext.isDate(d)
			})
		}

		if (Ext.isDate(d))
			return Ext.Date.format(d, this.getDateFormat())

		return RS.common.locale.unknownDate
	},

	onDestroy: function() {
		this.callParent()
		if (this.popup) this.popup.destroy()
	}
})