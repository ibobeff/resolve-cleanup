/**
 * @class Ext.ux.aceeditor.Editor
 * @extends Ext.AbstractComponent
 *
 * @author Harald Hanek (c) 2011-2012
 * @license http://harrydeluxe.mit-license.org
 */
Ext.define('Ext.ux.aceeditor.Editor', {
    extend: 'Ext.util.Observable',
    path: '',
    sourceCode: '',
    autofocus: true,
    fontSize: '12px',   
    theme: 'eclipse',  
    printMargin: false,
    printMarginColumn: 80,
    highlightActiveLine: true,
    highlightGutterLine: true,
    highlightSelectedWord: true,
    showGutter: true,
    fullLineSelection: true,
    tabSize: 4,
    useSoftTabs: false,
    showInvisible: false,
    useWrapMode: false,
    codeFolding: true,
    behavioursEnabled : false,
    lineHeight: 'auto',
    showCursor: true,

    constructor: function(owner, config) {
        var me = this;
        me.owner = owner;
        me.addEvents({
            editorcreated: true,
            change: true,
            editorDestroyed: true
        });
        me.callParent();
    },

    initEditor: function() {
        var me = this;
        me.editor = ace.edit(me.editorId);
		me.setMode(me.parser || Ext.state.Manager.get('aceLanguage', 'groovy'));
        me.setTheme(me.theme);
        me.setKeyboardHandler(me.keyboardHandler || Ext.state.Manager.get('aceKeybinding', 'windows'));
        me.editor.getSession().setUseWrapMode(me.useWrapMode);
        me.editor.setShowFoldWidgets(me.codeFolding);
        me.editor.setShowInvisibles(me.showInvisible);
        me.editor.setHighlightGutterLine(me.highlightGutterLine);
        me.editor.setHighlightSelectedWord(me.highlightSelectedWord);
        me.editor.renderer.setShowGutter(me.showGutter);
        me.setFontSize(me.fontSize);
        me.editor.container.style.lineHeight = me.lineHeight;
        me.editor.setShowPrintMargin(me.printMargin);
        me.editor.setPrintMarginColumn(me.printMarginColumn);
        me.editor.setHighlightActiveLine(me.highlightActiveLine);
        me.getSession().setTabSize(me.tabSize);
        me.getSession().setUseSoftTabs(me.useSoftTabs);
        me.setValue(me.sourceCode || me.value);
        me.editor.setReadOnly(me.readOnly);
        me.editor.setBehavioursEnabled(me.behavioursEnabled);
        me.editor.getSession().on('change', function() {
            me.fireEvent('change', me);
        }, me);

        if (!me.showCursor) {
          me.editor.renderer.$cursorLayer.element.style.opacity = 0;
        }

        if (me.autofocus) {
            me.editor.focus();
        } else {
            me.editor.renderer.hideCursor();
            me.editor.blur();
        }

        me.editor.initialized = true;
        me.fireEvent('editorcreated', me);
    },

    getEditor: function() {
        return this.editor;
    },

    getSession: function() {
        var session = null;

        if (this.editor) {
            session = this.editor.getSession();
        }

        return session;
    },

    getTheme: function() {
        var theme = null;

        if (this.editor) {
            theme = this.editor.getTheme();
        }

        return theme;
    },

    setTheme: function(name) {
        if (this.editor) {
            this.editor.setTheme("ace/theme/" + name);
        }
    },

    setMode: function(mode) {
        var session = this.getSession();
        
        if (session) {
            session.setMode("ace/mode/" + mode);    
        }
    },

    setKeyboardHandler: function(keyboardHandler) {
        if (Ext.Array.indexOf(['vim', 'emacs'], keyboardHandler) > -1) {
            this.editor.setKeyboardHandler("ace/keyboard/" + keyboardHandler);
        }
    },

    setReadOnly: function(value) {
        if (this.editor) {
            this.editor.setReadOnly(value)
        } else {
            this.readOnly = value
        }
    },

    setShowGutter: function(value) {
        if (this.editor && this.editor.renderer) {
            this.editor.renderer.setShowGutter(value);
        } else {
            this.showGutter = value;
        }
    },

    getValue: function() {
        return this.editor ? this.editor.getSession().getValue() : this.sourceCode;
    },

    setValue: function(value) {
        if (this.editor) {
            this.editor.getSession().setValue(value);
        } else {
            this.sourceCode = value;
        }
    },

    setFontSize: function(value) {
        this.editor.setFontSize(value);
    },

    undo: function() {
        this.editor.undo();
    },

    redo: function() {
        this.editor.redo();
    },
    insertAtCursor : function(data){
        this.editor.insert(data);
    }
});