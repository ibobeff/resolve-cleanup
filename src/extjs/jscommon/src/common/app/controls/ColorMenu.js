Ext.define('RS.common.ResolveColorPicker',{
	extend : 'Ext.picker.Color',
	alias : 'widget.resolvecolorpicker',
	colors : [
        '333333', '000000', '993300', '333300', '003300', '003366', '000080', '333399', 
        '808080', '800000', 'FF6600', '808000', '008000', '008080', '0000FF', '666699', 
        '969696', 'FF0000', 'FF9900', '99CC00', '339966', '33CCCC', '3366FF', '800080', 
        'C0C0C0', 'FF00FF', 'FFCC00', 'FFFF00', '00FF00', '00FFFF', '357EC7', '993366', 
        'F1EFEF', 'FF99CC', 'FFCC99', 'FFFF99', 'A6F3A6', 'ADD8E6', '00CCFF', 'CC99FF', 
        'FFFFFF', 'FDD6E9', 'F1E0CF', 'F5F5DC', 'E0FDE0', 'CCFFFF', '99CCFF', 'DBD3E4'
    ],
    initComponent : function(config){
    	this.callParent([config]);
    }
})
Ext.define('RS.common.ResolveColorMenu',{
	extend: 'Ext.menu.Menu',
	alias : 'widget.resolvecolormenu',
	requires : ['RS.common.ResolveColorPicker'],
	hideOnClick : true,
    pickerId : null,
    height : 146,
    initComponent : function(){
        var me = this,
            cfg = Ext.apply({}, me.initialConfig);

        // Ensure we don't get duplicate listeners
        delete cfg.listeners;
        Ext.apply(me, {
            plain: true,
            showSeparator: false,
            bodyPadding: 0,
            items: Ext.applyIf({
                cls: Ext.baseCSSPrefix + 'menu-color-item',
                margin: 0,
                id: me.pickerId,
                xtype: 'resolvecolorpicker'
            }, cfg)
        });

        me.callParent(arguments);

        me.picker = me.down('resolvecolorpicker');
        me.relayEvents(me.picker, ['select']);

        if (me.hideOnClick) {
            me.on('select', me.hidePickerOnSelect, me);
        }
    },
    hidePickerOnSelect: function() {
        Ext.menu.Manager.hideAll();
    }
})