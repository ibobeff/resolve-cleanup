/**
 * @class Ext.ux.aceeditor.Panel
 * @extends Ext.panel.Panel
 *
 * @author Harald Hanek (c) 2011-2012
 * @license http://harrydeluxe.mit-license.org
 */
Ext.define('Ext.ux.aceeditor.Panel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.AceEditor',
    mixins: {
        editor: 'Ext.ux.aceeditor.Editor'
    },
    layout: 'fit',
    autofocus: true,
    border: false,
    cls: '',
    listeners : {},

    defaultListeners: {
        resize: function() {
            if (this.editor) {
                this.editor.resize();
            }
        },
        activate: function() {
            if (this.editor) {
                this.editor.setStyle(this.cls);

                if (this.autofocus) {
                    this.editor.focus();
                }
            }
        },
        beforedestroy : function(){
            this.un('afterlayout', this.afterlayoutHandler, this);
            var editorSession = this.editor ? this.editor.getSession() : null;
            if(editorSession)
                editorSession.removeAllListeners('change');
        },      
    },

    initComponent: function() {
        var me = this,
            items = {
                xtype: 'component',
                style : 'border:1px solid grey',
                autoEl: 'pre'
            };

        Ext.apply(me, {
            items: items
        });

        Ext.applyIf(me.listeners, me.defaultListeners);
        me.callParent(arguments);
    },

    onRender: function() {
        var me = this;

        if (me.sourceEl != null) {
            me.sourceCode = Ext.get(me.sourceEl).dom.textContent || Ext.get(me.sourceEl).dom.outerText;
        }

        me.editorId = me.items.keys[0];
        me.oldSourceCode = me.sourceCode;
        me.callParent(arguments);

        // init editor on afterlayout
        me.on('afterlayout', me.afterlayoutHandler, me);
    },
    afterlayoutHandler : function() {
        if (this.url) {
            Ext.Ajax.request({
                url: this.url,
                success: function(response) {
                    this.sourceCode = response.responseText;

                    if (!me.editor) {
                        this.initEditor();
                    }
                }
            });
        }
        else if (!this.editor) {
            this.initEditor();
        }
        if (this.editor) {
            this.editor.resize();
            //HACK: on using ace editor in a pop up window, the layout of this editor will cause the document windown to scroll (due to extra height added).
            //This will cause the background ,which the window pop up render on top, be off by this scroll.
            //Just need to reset window scroll when using aceditor inside a window pop up.
            var parentContainer = this.up();
            while(parentContainer && parentContainer.up()){
                parentContainer = parentContainer.up();
            }
            if( parentContainer && parentContainer.xtype == 'window')
                document.body.scrollTop = 0;             
        }
    }   
});

glu.regAdapter('AceEditor', {
    extend: 'field',

    afterCreate: function(control, viewmodel) {
        glu.provider.adapters.Field.prototype.afterCreate.apply(this, arguments);

        if (!control.delayedEvent) {
            control.delayedEvent = new Ext.util.DelayedTask(function() {
                control.fireEvent('textchanged', control);
            });
        }

        control.on('change', function(ctrl) {
            control.delayedEvent.delay(control.keyDelay || 100);
        }, control)
    },

    initAdapter: function() {
        this.valueBindings = glu.deepApplyIf({
            eventName: 'textchanged'
        }, this.valueBindings);
    },

    valueBindings: {
        eventName: 'change',
        eventConverter: function(field, newVal) {
            return field.getValue();
        },
        setComponentProperty: function(value, oldvalue, options, control) {
            control.suspendCheckChange++;

            if (control.getValue() != value) {
                control.setValue(value);
            }
            control.lastValue = value;
            control.suspendCheckChange--;
        }
    },

    parserBindings: {
        setComponentProperty: function(value, oldValue, options, control) {
            control.suspendCheckChange++;
            control.setMode(value);
            Ext.state.Manager.set('aceLanguage', value);
            control.lastParser = value;
            control.suspendCheckChange--;
        }
    },

    keyboardBindings: {
        setComponentProperty: function(value, oldValue, options, control) {
            control.suspendCheckChange++;
            control.setKeyboardHandler(value);
            Ext.state.Manager.set('aceKeybinding', value);
            control.lastKeyboard = value;
            control.suspendCheckChange--;
        }
    },

    readOnlyBindings: {
        setComponentProperty: function(value, oldValue, options, control) {
            control.suspendCheckChange++;
            control.setReadOnly(value);
            control.lastReadOnly = value;
            control.suspendCheckChange--;
        }
    },

    showGutterBindings: {
        setComponentProperty: function(value, oldValue, options, control) {
            control.suspendCheckChange++;
            control.setShowGutter(value);
            control.lastShowGutter = value;
            control.suspendCheckChange--;
        }
    }
})