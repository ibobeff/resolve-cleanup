Ext.define('RS.common.FilterBar', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.filterbar',

	allowPersistFilter: true,

	border: false,
	dock: 'top',
	layout: {
		type: 'hbox',
		align: 'middle'
	},
	items: [],
	autoScroll: true,
	bodyCls: 'x-toolbar-default',
	padding: '0px 0px 0px 10px',
	addFilter: function(value, internalValue) {
		if (this.items.length == 0) {
			//Add in the filter specific items (save, edit, clear)
			this.add({
				xtype: 'toolbar',
				items: [{
					text: RS.common.locale.saveFilter,
					hidden: !this.allowPersistFilter,
					scope: this,
					handler: this.saveFilter
				}, {
					text: RS.common.locale.clearFilter,
					scope: this,
					handler: this.clearFilter
				}]
			});
		}
		//Check uniqueness of the filter
		var found = false;
		this.items.each(function(item) {
			if (item.value == value && item.internalValue == internalValue) {
				found = true
				return false
			}
		})
		if (!found)
			this.insert(this.items.length - 1, {
				xtype: 'filterbaritem',
				value: value,
				internalValue: internalValue
			});
	},
	removeFilter: function(item) {
		this.remove(item);
		if (this.items.length == 1) {
			this.clearFilter();
		}
		this.fireEvent('filterChanged');
	},
	saveFilter: function(button) {
		this.fireEvent('saveFilter', this);
	},
	clearingFilters: false,
	clearFilter: function(button,continueFiring,suppressChangeEvt) {
		this.clearingFilters = true;
		this.items.each(function(item) {
			this.remove(item)
		}, this);
		this.clearingFilters = false;
		if(continueFiring!==false)
			this.fireEvent('clearFilter');
		if(suppressChangeEvt !== true){
			this.fireEvent('filterChanged');
		}
	},
	getFilters: function() {
		var filters = [];
		this.items.each(function(filter) {
			if (filter.internalValue) filters.push(filter.internalValue);
		});
		return filters;
	},
	setFilters: function(filters) {
		this.suspendEvents(false);
		this.items.each(function(item) {
			this.remove(item);
		}, this);
		this.resumeEvents();
		Ext.each(filters, function(filter) {
			this.addFilter(filter.value, filter.internalValue);
		}, this);
		this.fireEvent('filterChanged');
	}
});

Ext.define('RS.common.FilterBarItem', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.filterbaritem',
	border: false,
	layout: {
		type: 'hbox',
		align: 'middle'
	},
	bodyCls: 'x-toolbar-default',
	height: 30,
	padding: '0 10 0 0',

	initComponent: function() {
		var me = this;
		me.items = [{
			xtype: 'label',
			flex: 1,
			padding: '0 5 0 0',
			text: Ext.util.Format.ellipsis(this.value, 60)
		}, {
			xtype: 'image',
			src: '/resolve/images/remove.gif',
			listeners: {
				render: function() {
					this.getEl().on('mouseover', function() {
						this.setSrc('/resolve/images/remove2.gif');
					}, this);
					this.getEl().on('mouseout', function() {
						this.setSrc('/resolve/images/remove.gif')
					}, this);
					this.getEl().on('click', me.removeFilter, me);
					this.getEl().on('load', function() {
						me.doLayout()
					})
				}
			}

		}];
		me.callParent();
	},

	getValue: function() {
		return this.value;
	},

	removeFilter: function(filter) {
		this.ownerCt.removeFilter(this);
	}

});