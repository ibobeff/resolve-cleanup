Ext.define('RS.common.DateTimeField', {

	extend: 'Ext.form.FieldContainer',
	alias: 'widget.xdatetimefield',

	submitFormat: 'c',
	width: '100%',

	layout: {
		type: 'hbox',
		align: 'stretch'
	},

	initComponent: function() {
		this.items = [{
			xtype: 'datefield',
			itemId: 'datefield',
			value: this.value,
			readOnly: this.readOnly,
			fieldCls: this.fieldCls,
			flex: 1,
			listeners: {
				scope: this,
				change: this.dateTimeChanged
			}
		}, {
			xtype: 'timefield',
			itemId: 'timefield',
			padding: '0 0 0 5',
			value: this.value,
			fieldCls: this.fieldCls,
			readOnly: this.readOnly,
			flex: 1,
			listeners: {
				scope: this,
				change: this.dateTimeChanged
			}
		}, {
			xtype: 'hidden',
			name: this.name,
			submitFormat: this.submitFormat,
			value: this.formatHiddenDate(this.parseDate(this.value)),
			listeners: {
				change: function(panel, newValue, oldValue) {
					if (newValue) {
						var datetime = new Date(newValue);
						if (isNaN(datetime)) { // newValue is in milliseconds so convert to humnan readable
							datetime = new Date(parseInt(newValue));
							this.setValue(Ext.Date.format(datetime, this.submitFormat));
						}
						this.up().down('#datefield').setValue(datetime);
						this.up().down('#timefield').setValue(datetime);
					}
				}
			}
		}];

		if (this.readOnly) {
			delete this.items[0].flex;
			delete this.items[1].flex;
		}

		this.callParent();
	},
	dateTimeChanged: function() {
		if (!this.rendered) return

		var myDate = this.down('#datefield').getValue(),
			myTime = this.down('#timefield').getValue(),
			datetime = '';

		if (Ext.isDate(myDate) && Ext.isDate(myTime)) datetime = new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), myTime.getHours(), myTime.getMinutes(), myTime.getSeconds());
		else if (Ext.isDate(myDate)) {
			datetime = new Date();
			datetime.setFullYear(myDate.getFullYear());
			datetime.setMonth(myDate.getMonth());
			datetime.setDate(myDate.getDate());
		} else if (Ext.isDate(myTime)) {
			datetime = new Date();
			datetime.setHours(myTime.getHours());
			datetime.setMinutes(myTime.getMinutes());
			datetime.setSeconds(myTime.getSeconds());
		}

		var oldValue = this.down('hiddenfield').value;
		this.down('hiddenfield').setValue(Ext.Date.format(datetime, this.submitFormat));
		this.fireEvent('change', this, this.down('hiddenfield').getValue(), oldValue)
	},
	formatHiddenDate: function(date) {
		if (!Ext.isDate(date)) {
			return date;
		}
		if ('timestamp' === this.submitFormat) {
			return date.getTime() / 1000;
		} else {
			return Ext.util.Format.date(date, this.submitFormat);
		}
	},
	parseDate: function(value) {
		if (Ext.isDate(value)) return value;
		var date = Ext.Date.parse(value, this.submitFormat);
		if (Ext.isDate(date)) return date;
		date = Ext.Date.parse(value, this.format);
		if (Ext.isDate(date)) return date;
		if (/^\d+$/g.test(value))
			value = parseInt(value);
		if (value) {
			value = new Date(value);
			if (!Ext.isNumber(value.getMilliseconds())) {
				this.invalidText = 'Invalid date value';
				return null;
			}
		}
		return value;
	},
	setValue: function(value) {
		if (Ext.isDate(value)) {
			this.down('#datefield').setValue(value)
			this.down('#timefield').setValue(value)
		}

		if (value == '') {
			this.down('#datefield').setValue(null)
			this.down('#timefield').setValue(null)
		}
	},

	getValue: function() {
		return this.down('hiddenfield').getValue()
	}
});

glu.regAdapter('xdatetimefield', {
	extend: 'field'
});