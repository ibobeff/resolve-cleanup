/**
 * Control to display a large image when the user clicks on a thumbnail image within a post.
 */
Ext.define('RS.common.controls.ImageViewer', {
	extend: 'Ext.window.Window',
	alias: 'rsimageviewer',

	layout: 'fit',
	frame: false,
	closeAction: 'hide',
	modal: true,
	constrain: true,
	onEsc: function() {
		this.close();
	},
	baseCls: Ext.isIE8m ? 'x-window' : 'x-panel',
	ui: Ext.isIE8m ? 'default' : 'sysinfo-dialog',

	/**
	 * The source of the image tag to display in the large window
	 */
	src: '',

	initComponent: function() {
		this.items = {
			xtype: 'component',
			autoEl: {
				tag: 'img',
				style: 'height:auto;width:auto',
				src: this.src
			},
			listeners: {
				render: {
					scope: this,
					fn: function(cmp) {
						this.getImage().on('load', this.onImageLoad, this, {
							single: true
						});
					}
				}
			}
		};
		this.callParent(arguments);
	},

	onImageLoad: function() {
		this.removeMask();
		var size = Ext.fly(Ext.getBody()).getSize(),
			bodyHeight = size.height,
			bodyWidth = size.width,
			image = this.getImage();
		image.setStyle({
			height: 'auto',
			width: 'auto'
		});
		var h = image.dom.offsetHeight,
			w = image.dom.offsetWidth,
			toConfig = {};

		//preserve scale
		var scaledSize = this.scaleSize(bodyWidth - 26, bodyHeight - 58, w, h);
		//10-25 to bring the image away from the edges
		var width = scaledSize[0];
		var height = scaledSize[1];
		var x = Math.round((bodyWidth - width) / 2);
		var y = Math.round((bodyHeight - height) / 2);
		toConfig = {
			width: width,
			height: height,
			x: x,
			y: y
		};

		this.animate({
			to: toConfig,
			listeners: {
				afteranimate: {
					fn: function() {
						this.doComponentLayout();
					},
					scope: this
				}
			}
		});
	},
	removeMask: function() {
		this.getEl().unmask();
	},

	/**
	 * Scales the image to the max height and width from the current height and width
	 */
	scaleSize: function(maxW, maxH, currW, currH) {

		var ratio = currH / currW;

		if (currW >= maxW && ratio <= 1) {
			currW = maxW;
			currH = currW * ratio;
		}
		if (currH >= maxH) {
			currH = maxH;
			currW = currH / ratio;
		}

		return [currW, currH];
	},

	/**
	 * Sets the image source and resizes the window to the largest size available (the size of the image or the size of the browser window) maintaining scale of the image.
	 */
	setSrc: function(src) {
		this.getEl().mask('Loading...');
		var image = this.getImage();
		image.on('load', this.onImageLoad, this, {
			single: true
		});
		image.dom.src = src;
		image.dom.style.width = image.dom.style.height = 'auto';
		//this is primarily to fix an issue with Chrome dealing with cached images
		if(this.is_img_cached(src)) {
			this.removeMask();
		}
	},
	is_img_cached: function(src) {
	    //It's necessary to create an image object to test complete property.
	    //It always returns true when you test it directly on an image element from the DOM.
	    //IE9 and IE10 always return false. No known fix at this time.
	    var image = new Image();
	    image.src = src;
	    return image.complete;
	},
	getImage: function() {
		return this.items.items[0].getEl();
	},
	initEvents: function() {
		this.callParent(arguments);
		if (this.resizer) {
			this.resizer.preserveRatio = true;
		}
	}
});


function displayLargeImage(url, displayName) {
	if (!this.imageWindow) {
		this.imageWindow = Ext.create('RS.common.controls.ImageViewer', {
			src: url,
			title: displayName
		});
	} else {
		this.imageWindow.setTitle(displayName);
		this.imageWindow.setSrc(url);
	}
	this.imageWindow.show();
}