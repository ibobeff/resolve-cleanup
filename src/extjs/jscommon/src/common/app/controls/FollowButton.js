Ext.define('RS.common.controls.FollowButton', {
	extend: 'Ext.button.Button',
	alias: 'widget.followbutton',

	ui: 'system-info-button-small',
	iconCls: 'rs-icon-button icon-mail-forward',

	config: {
		streamId: ''
	},

	initComponent: function() {
		if (Ext.isGecko) this.setIconCls(this.iconCls + ' rs-icon-firefox')
		this.tooltip = this.tooltip || RS.common.locale.followButtonTooltip
		this.handler = this.handleButtonClick
		this.overflowText = RS.common.locale.followButtonTooltip
		this.callParent()
	},

	handleButtonClick: function(button) {
		if (!button.popup) {
			var model = glu.model({
				mtype: 'RS.social.Followers',
				addFollowers: true,
				buttonWindow: true,
				streamId: button.streamId,
				streamType: button.streamType,
				removeFollowers: button.removeFollowers || true
			})
			model.init()
			button.popup = glu.openWindow(model)
			button.popup.on('close', function() {
				button.popup = null
			})
			button.popup.show()
			button.popup.alignTo(button.getEl().id, 'tr-br')
		} else {
			button.popup.close()
		}
	},

	onDestroy: function() {
		this.callParent()
		if (this.popup) this.popup.destroy()
	}
})