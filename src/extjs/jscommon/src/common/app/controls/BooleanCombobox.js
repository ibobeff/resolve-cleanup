Ext.define('RS.common.BooleanComboBox', {
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.booleancombobox',

	displayField: 'name',
	valueField: 'value',

	initComponent: function() {
		this.store = Ext.create('Ext.data.Store', {
			fields: ['name', 'value'],
			data: [{
				name: RS.common.locale.trueText,
				value: 'true'
			}, {
				name: RS.common.locale.falseText,
				value: 'false'
			}]
		});
		this.callParent();
	}
});