/**
 * Message class that displays a box at the top of the screen to indicate a sucess or failure from the server for a dramatic way for the users to see what happened.
 */
Ext.namespace('RS');
RS.UserMessage = function() {
	var msgCt;

	function createBox(t, s, tbId) {
		return ['<div class="msg" style="z-index: 20000">',
			'<div class="msg-header">',
			'<div class="icon-large icon-remove-sign message-close" style="float:right;"></div>',
			'<h3>' + t + '</h3>',
			'</div>',
			'<p>' + s + '</p>',
			'<div id="' + tbId + '"></div',
			'</div>'
		].join('');
	}

	return {
		msg: function(config) {
			if (getResolveRoot().RS && getResolveRoot().RS.UserMessage && getResolveRoot().RS.UserMessage != RS.UserMessage) {
				getResolveRoot().RS.UserMessage.msg(config);
				return;
			}
			Ext.applyIf(config, {
				success: true,
				title: '',
				msg: '',
				duration: 4000,
				closeable: true,
				position: 'tr-tr',
				offset: [-10, 3],
				animationDirection: 'r',
				width: 300,
				items: [],
				postItems: []
			});
			if (!document.getElementById('msg-div')) {
				msgCt = Ext.DomHelper.append(document.body, {
					id: 'msg-div'
				}, true);
			}

			//set the appropriate width for the message
			msgCt.setWidth(config.width);
			var tbId = Ext.id(),
				m = Ext.DomHelper.append(msgCt, createBox(config.title, config.msg, tbId), true);
			m.configuration = config;

			//setup on click handlers for links to auto close the dialog
			var links = m.query('a')
			Ext.Array.forEach(links, function(link) {
				Ext.fly(link).on('click', function() {
					m.fadeOut({
						duration: 200,
						remove: true
					})
				})
			})

			if (config.closeable)
				config.items.push({
					xtype: 'button',
					text: config.closeText || RS.common.locale.close,
					handler: function(button) {
						m.fadeOut({
							duration: 200,
							remove: true
						})
					}
				})
			else
				m.select('img').setStyle('visibility', 'hidden');

			if (config.items.length > 0 || config.postItems.length > 0) Ext.create('Ext.toolbar.Toolbar', {
				renderTo: tbId,
				cls: 'message-box-toolbar-footer',
				defaultButtonUI: 'message-box-toolbar-button-small',
				items: config.items.concat(config.postItems)
			})

			if (config.success === 2) {
				m.down('h3').setStyle({
					color: '#333333'
				});
			} else if (config.success) {
				m.down('h3').addCls('success');
			} else {
				m.down('h3').addCls('fail');
			}

			m.on('mouseover', function() {
				//stop animations so that we don't hide the message when the user has hovered it
				m.hovered = true;
				if (m.getActiveAnimation())
					return;
				m.stopAnimation();
				m.setOpacity(1, false);
				m.show();
			}, m);
			m.on('mouseout', function(e) {
				//resume ghosting
				m.hovered = false;
				if (config.duration) m.fadeOut({
					duration: 200,
					delay: config.duration,
					scope: m,
					callback: this.removeElement
				});
			}, this);

			//m.select('div').on('click', function() {
			//	m.fadeOut({
			//		duration: 200,
			//		remove: true
			//	});
			//});

			m.select('div > a.x-btn-noicon').on('click', function() {
				m.remove();
			});
			m.select('div.icon-remove-sign').on('click', function() {
				m.remove();
			});

			m.hide();
			// m.alignTo(document.body, config.position, config.offset) //removed with msg-id moved to the right now
			// m.slideIn(config.animationDirection);
			m.fadeIn({
				duration: 400,
				easing: 'easeOut',
				opacity: 1,
				from: {
					opacity: 0
				}
			})
			if (config.duration > 0) {
				m.fadeOut({
					duration: 200,
					delay: config.duration,
					scope: m,
					callback: this.removeElement
				});
			}
			return m;
		},
		removeElement: function() {
			if (this.hovered) return;
			if (this.configuration && this.configuration.callback) this.configuration.callback(this.configuration);
			this.remove();
		}
	};
}();