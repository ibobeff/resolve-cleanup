Ext.define('RS.formbuilder.viewer.desktop.fields.DateField', {

	extend: 'Ext.form.field.Date',
	alias: 'widget.xdatefield',

	submitFormat: 'c',
	onRender: function() {

		// call parent
		this.callParent(arguments);

		var name = this.name || this.el.dom.name;
		this.hiddenField = this.el.insertSibling({
			tag: 'input',
			type: 'hidden',
			name: name,
			value: this.formatHiddenDate(this.parseDate(this.value))
		});
		this.hiddenName = name;
		// otherwise field is not found by BasicForm::findField
		this.el.dom.removeAttribute('name');
		this.el.on({
			keyup: {
				scope: this,
				fn: this.updateHidden
			},
			blur: {
				scope: this,
				fn: this.updateHidden
			}
		}, Ext.isIE ? 'after' : 'before');
	},
	onDisable: function() {
		this.callParent(arguments);
		if (this.hiddenField) {
			this.hiddenField.dom.setAttribute('disabled', 'disabled');
		}
	},
	onEnable: function() {
		this.callParent(arguments);
		if (this.hiddenField) {
			this.hiddenField.dom.removeAttribute('disabled');
		}
	},
	formatHiddenDate: function(date) {
		if (!Ext.isDate(date)) {
			return date;
		}
		if ('timestamp' === this.submitFormat) {
			return date.getTime() / 1000;
		} else {
			return Ext.util.Format.date(date, this.submitFormat);
		}
	},
	updateHidden: function() {
		this.hiddenField.dom.value = this.formatHiddenDate(this.parseDate(this.getValue()));
	},
	parseDate: function(value) {
		if (Ext.isDate(value)) return value;
		var date = Ext.Date.parse(value, this.submitFormat);
		if (Ext.isDate(date)) return date;
		date = Ext.Date.parse(value, this.format);
		if (Ext.isDate(date)) return date;
		if (/^\d+$/g.test(value))
			value = parseInt(value);
		if (value) {
			value = new Date(value);
			if (!Ext.isNumber(value.getMilliseconds())) {
				this.invalidText = 'Invalid date value';
				return null;
			}
		}
		return value;
	}
});