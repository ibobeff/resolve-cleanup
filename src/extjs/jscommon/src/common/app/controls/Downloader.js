function downloadFile(url) {
	var downloadFrame = Ext.get('downloaderFrame');
	if (downloadFrame) {
		downloadFrame.dom.src = url;
	} else {
		var m = Ext.core.DomHelper.append(Ext.getBody(), {
			html: ['<iframe id="downloaderFrame" src="', url, '" style="display:none"'].join('')
		}, true);
	}
}