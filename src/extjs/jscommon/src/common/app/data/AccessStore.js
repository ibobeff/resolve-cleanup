Ext.define('RS.common.data.AccessStore', (function () {
	function shapeData(accessRights) {
		var data = [];

		if (accessRights) {
			var	readUsers = accessRights.ureadAccess.length > 0 ? accessRights.ureadAccess.split(',') : [],
				writeUsers = accessRights.uwriteAccess.length > 0 ? accessRights.uwriteAccess.split(',') : [],
				executeUsers = accessRights.uexecuteAccess.length > 0 ? accessRights.uexecuteAccess.split(',') : [],
				adminUsers = accessRights.uadminAccess.length > 0 ? accessRights.uadminAccess.split(',') : [],
				users = readUsers.slice();
			consolidateArrays(users, writeUsers);
			consolidateArrays(users, executeUsers);
			consolidateArrays(users, adminUsers);

			for (var i = 0; i < users.length; i++) {
				var u = users[i];
				data.push({
					uname: u,
					read: readUsers.indexOf(u) !== -1,
					write: writeUsers.indexOf(u) !== -1,
					execute: executeUsers.indexOf(u) !== -1,
					admin: adminUsers.indexOf(u) !== -1
				});
			}
		}

		return data;
	}

	function consolidateArrays(destination, source) {
		for (var i = 0; i < source.length; i++) {
			if (destination.indexOf(source[i]) === -1) {
				destination.push(source[i]);
			}
		}
	}

	return {
		extend: 'Ext.data.Store',
		config: {
			fields: ['uname', 'read', 'write', 'execute', 'admin'],
			listeners: {},
			data: []
		},

		constructor: function(listeners) {
			var config = this.config;

			if (typeof listeners === 'object') {
				for (var key in listeners) {
					config.listeners[key] = listeners[key];
				}
			}

			this.callParent([config]);
		},

		loadData: function (accessRights, append) {
			var data;

			if (accessRights.hasOwnProperty('ureadAccess')) {
				data = shapeData(accessRights);
			} else {
				data = accessRights;
			}
			
			this.callParent([data, append]);
			return this;
		},

		getAccessRights: function (dataIsForServer) {
			var readRoles = [], writeRoles = [], executeRoles = [], adminRoles = [],
				range = this.getRange();

			for (var i = 0; i < range.length; i++) {
				var d = range[i].data;
				var user = d.uname;

				if (d.read) {
					readRoles.push(user);
				}

				if (d.write) {

					writeRoles.push(user);
				}

				if (d.execute) {
					executeRoles.push(user);
				}

				if (d.admin) {
					adminRoles.push(user);
				}
			}

			var accessRights = {
				ureadAccess: readRoles.join(','),
				uwriteAccess: writeRoles.join(','),
				uexecuteAccess: executeRoles.join(','),
				uadminAccess: adminRoles.join(',')
			};

			if (!dataIsForServer) {
				var isAdmin = false, canEdit = false, canExecute = false, currentUserRoles = clientVM.user.roles;

				if (typeof currentUserRoles === 'undefined' || currentUserRoles === null) {
					isAdmin = true;
					canEdit = true;
					canExecute = true;
				} else {
					for (var i = 0; i < currentUserRoles.length; i++) {
						isAdmin = adminRoles.indexOf(currentUserRoles[i]) >= 0;

						if (!canEdit) {
							canEdit = isAdmin || writeRoles.indexOf(currentUserRoles[i]) >= 0;
						}

						if (!canExecute) {
							canExecute = isAdmin || executeRoles.indexOf(currentUserRoles[i]) >= 0;
						}

						if (canEdit && canExecute) {
							break;
						}
					}

					for (var i = 0; i < adminRoles.length; i++) {
						isAdmin = currentUserRoles.indexOf(adminRoles[i]) >= 0;

						if (isAdmin) {
							canEdit = true;
							canExecute = false;					
							break;
						}
					}
				}

				if (!isAdmin) {
					for (var i = 0; i < writeRoles.length; i++) {
						canEdit = currentUserRoles.indexOf(writeRoles[i]) >= 0;

						if (canEdit) {
							break;
						}
					}

					for (var i = 0; i < executeRoles.length; i++) {
						canExecute = currentUserRoles.indexOf(executeRoles[i]) >= 0;

						if (canExecute) {
							break;
						}
					}
				}

				accessRights.canEdit = canEdit;
				accessRights.canExecute = canExecute;
			}

			return accessRights;

		},

		getRoles: function () {
			var range = this.getRange();
			var roles = [];

			for (var i = 0; i < range.length; i++) {
				roles.push(range[i].data.uname);
			}

			return roles;
		}
	};
})());