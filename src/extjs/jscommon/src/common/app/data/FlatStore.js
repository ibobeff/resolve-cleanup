Ext.define('RS.common.data.FlatStore', {
	extend: 'Ext.data.Store',

	defaultConfigProperties: ['mtype', 'fields', 'data', 'filterMethod', 'accessorMethods'],
	defaultFlatConfig: {
		mtype: 'store',
		fields: ['text', 'value'],
		filterMethod: function () { return true; },
		accessorMethods: [
			function (d) { return d; },
			function (d) { return d; }
		]
	},

	dataFieldNames: [],
	serverFieldNames: [],
	filterMethod: function () { return true; },
	accessorMethods: [],

	constructor: function(config) {
		config = config || {};

		if (config.constructor === Array) {
			var data = config;
			config = this.defaultFlatConfig;
			this.dataFieldNames = config.fields;
			this.serverFieldNames = config.fields;
			this.filterBy(config.filterMethod)
				.accessors(config.accessorMethods);
			config.data = this.shapeData(data);
		} else {
			for (var i = 0; i < this.defaultConfigProperties.length; i++) {
				var p = this.defaultConfigProperties[i];

				if (typeof config[p] === 'undefined') {
					if (p === 'data') {
						config[p] = [];
					} else {
						config[p] = this.defaultFlatConfig[p];	
					}					
				}
			}

			this.dataFieldNames = config.fields;

			if (config.serverFields && config.serverFields.constructor === Array) {
				this.serverFieldNames = config.serverFields;
			} else {
				this.serverFieldNames = config.fields;
			}

			this.filterBy(config.filterMethod)
				.accessors(config.accessorMethods);
		}

		this.callParent([config]);
	},

	shapeData: function () {
		var data = [],
			fields = this.dataFieldNames,
			accessors = this.accessors();		

		if (fields.length !== accessors.length) {
			throw new Error('fields.length != to accessors.length in config');
		}

		if (arguments.length === 1) {
			if (arguments[0].constructor === Array) {
				data = arguments[0];
			} else {
				data.push(arguments[0]);
			}
		} else {
			data = Array.apply(null, arguments);
		}

		var filteredData = data.filter(this.filterBy());
		var shapedData = [];

		for (var i = 0; i < filteredData.length; i++) {
			var item = {};

			for (var j = 0; j < fields.length && j < accessors.length; j++) {
				item[fields[j]] = accessors[j](filteredData[i]);
			}

			shapedData.push(item);
		}

		return shapedData;
	},

	getData: function (getDataForServer) {
		var records = this.getRange(),
			data = [];

		if (getDataForServer) {
			for (var i = 0; i < records.length; i++) {
				var d = {},
					r = records[i];

				for (var j = 0; j < this.serverFieldNames.length; j++) {
					d[this.serverFieldNames[j]] = r.data[this.serverFieldNames[j]];
				}

				data.push(d);
			}
		} else {
			for (var i = 0; i < records.length; i++) {
				data.push(records[i].data);
			}
		}

		return data;
	},

	filterBy: function (fn) {
		if (typeof fn === 'function') {
			this.filterMethod = fn;
			return this;
		}

		return this.filterMethod;
	},	

	accessors: function () {
		if (arguments.length > 0) {
			var accessors = [];

			if (arguments.length === 1) {
				if (arguments[0].constructor === Array) {
					accessors = arguments[0];
				} else {
					accessors.push(arguments[0]);
				}
			} else {
				accessors = Array.apply(null, arguments);
			}

			for (var i = 0; i < accessors.length; i++) {
				if (typeof accessors[i] !== 'function') {
					throw new Error('Accessor at index='+ i + ' is not a function.');
				}
			}

			this.accessorMethods = accessors;
			return this;
		}

		return this.accessorMethods;
	},

	removeBy: function (fieldName, value) {
		var result = {
			index: -1,
			data: null
		};

		result.index = this.findExact(fieldName, value);

		if (result.index !== -1) {
			result.data = this.getAt(result.index).data;
			this.removeAt(result.index);
		}

		return result;
	}	
});