glu.mreg('ResolveScreen', {

	activeCard: 0,
	gridSelections: [],
	allSelected: false,
	mock: false,

	showDetails: function(sys_id) {
		this.set('activeCard', 1);
	},

	showList: function() {
		this.set('activeCard', 0);
	}

});