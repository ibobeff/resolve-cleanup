(function() {
	Ext.ns('RS.common.validation');
	var RSValidation = RS.common.validation || {};
	Ext.apply(RSValidation, {
		PlainText : /^[a-zA-Z0-9_\-](\s{0,1}[a-zA-Z0-9_\-,?!])*$/,		
		VariableName : /^[a-zA-Z_][a-zA-Z0-9_\-]*$/,
		TaskNamespaceAll : /^[a-zA-Z0-9_\-\.](\s{0,1}[a-zA-Z0-9_\-\.])*$/i,
		TaskNamespace : /^(?!resolve?$)^[a-zA-Z0-9_\-\.](\s{0,1}[a-zA-Z0-9_\-\.])*$/i,
		TaskName : /^[a-zA-Z0-9_\-](\s{0,1}[a-zA-Z0-9_\-])*$/
	})
}())