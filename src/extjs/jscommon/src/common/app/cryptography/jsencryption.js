//Patch CryptoJS to support aes 128-bit
CryptoJS.algo.AES128 = CryptoJS.algo.AES.extend({
	keySize: 256 / 32
});
CryptoJS.AES128 = CryptoJS.lib.Cipher._createHelper(CryptoJS.algo.AES128);

function decryptText(elementId, encodedValue, iv) {
	Ext.Msg.prompt(RS.common.locale.decriptionTitle, RS.common.locale.decriptionMessage, CryptoDecrypt, {
		elementId: elementId,
		encodedValue: encodedValue,
		iv: iv
	});
}

function CryptoDecrypt(btn, prompt) {
	if (btn == 'ok') {
		try {
            var salt = this.encodedValue.split("$")[0],
			key = CryptoJS.PBKDF2(prompt, CryptoJS.enc.Utf8.parse(salt), {
				keySize: 256 / 32,
				iterations: 1024
			}),
				iv = CryptoJS.enc.Base64.parse(this.iv),
				enc = this.encodedValue.split("$")[1],
				decoded = CryptoJS.AES128.decrypt(enc, key, {
					iv: iv
				}),
				decodedString = CryptoJS.enc.Utf8.stringify(decoded);
			if (decodedString) {
				Ext.fly(this.elementId).update(Ext.String.htmlEncode(decodedString));
			} else {
				Ext.Msg.alert(RS.common.locale.invalidKeyTitle, RS.common.locale.invalidKeyMessage);
			}
		} catch (e) {
			Ext.Msg.alert(RS.common.locale.decryptErrorTitle, RS.common.locale.decryptErrorMessage);
		}
	}
}
