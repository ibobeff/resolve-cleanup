Ext.define('RS.common.ViewFilterManagementWindow', {
	extend: 'Ext.window.Window',
	alias: 'widget.viewfiltermanagementwindow',

	height: 400,
	width: 600,
	title: RS.common.locale.manage,
	border: false,
	modal: true,
	buttonAlign: 'left',
	layout: 'fit',

	initComponent: function() {
		var viewGrid = this.getGridConfig('View'),
			filterGrid = this.getGridConfig('Filter');

		viewGrid.style = 'padding-bottom:5px';
		this.items = {
			xtype: 'tabpanel',
			items: [filterGrid, viewGrid]
		};


		this.buttons = [{
			text: RS.common.locale.close,
			handler: function(button) {
				button.up('window').close();
			}
		}];

		this.callParent(arguments);
	},

	getGridConfig: function(obj) {
		return {
			xtype: 'grid',
			border: false,
			title: RS.common.locale[obj.toLowerCase() + 'Title'],
			selModel: {
				xtype: 'rowmodel',
				mode: 'MULTI'
			},
			flex: 1,
			tbar: [{
				text: RS.common.locale.deleteText,
				itemId: 'deleteButton',
				disabled: true,
				type: obj,
				scope: this,
				handler: function(button) {
					var grid = button.up('grid'),
						selections = grid.getSelectionModel().getSelection();

					Ext.MessageBox.show({
						title: RS.common.locale.deleteTitle,
						msg: Ext.String.format(RS.common.locale[Ext.String.format(selections.length === 1 ? 'delete{0}Message' : 'delete{0}sMessage', obj)], selections.length),
						buttons: Ext.MessageBox.YESNO,
						buttonText: {
							yes: RS.common.locale.deleteText,
							no: RS.common.locale.cancel
						},
						scope: button,
						fn: this.handleDelete
					});
				}
			}],
			columns: [{
				text: 'Name',
				dataIndex: 'name',
				flex: 1
			}],
			store: Ext.create('Ext.data.Store', {
				autoLoad: true,
				model: Ext.String.format('RS.common.model.{0}', obj),
				proxy: {
					type: 'ajax',
					url: Ext.String.format('/resolve/service/{0}/list', obj.toLowerCase()),
					extraParams: {
						table: this.tableName
					},
					reader: {
						type: 'json',
						root: 'records'
					}
				}
			}),
			listeners: {
				selectionchange: function(selectionModel, selected, eOpts) {
					if (selected.length > 0) this.down('#deleteButton').enable();
					else this.down('#deleteButton').disable();
				}
			}
		}
	},

	handleDelete: function(btn) {
		if (btn == 'yes') {
			var grid = this.up('grid'),
				selections = grid.getSelectionModel().getSelection(),
				ids = '';

			Ext.each(selections, function(selection) {
				if (ids.length > 0) ids += ',';
				ids += selection.internalId;
			});

			Ext.Ajax.request({
				url: Ext.String.format('/resolve/service/{0}/delete', this.type.toLowerCase()),
				params: {
					sysIds: ids
				},
				scope: grid,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) this.store.load();
					else {
						clientVM.displayError(response.message,0,RS.common.locale.error);
					} 
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			});
		}
	}
});