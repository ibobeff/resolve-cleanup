/**
    This module defines a single file upload form.
*/
/*
Ext.define('RS.formbuilder.viewer.desktop.fields.FileUpload', {
    extend: 'Ext.form.Panel',
    alias: 'widget.rsfileupload',

    width: 500,
    bodyPadding: 10,

    config: {
        // See 'RS.common.HtmlLintEditor' for usage.
        ulr: '',                // Back end upload api
        callback: undefined,    // callback on successful upload
        view: undefined,        // The parent container
        autoUpload: false,      // Automatically upload when a valid file is added
        multipart_params: {},   // extra payload sent to the backend along with the uploading file
        filters: {}             // File extension filter
    },
    buttonAlign: 'left',
    items: [{
        xtype: 'progressbar',
        itemId: 'progressBar'
    }, {
        xtype: 'label',
        itemId: 'fileName',
        html: RS.common.locale.noFileSelected
    }],

    buttons: [{
        text: RS.common.locale.selectAWordDocFile,
        itemId: 'browseButton'
    }],

    listeners: {
        afterrender: function(form, eOpts) {
            var me = form;
            var uploadButton = me.down('#uploadButton')
                browseButton = me.down('#browseButton'),
                progressBar  = me.down('#progressBar');
            plupload.addFileFilter('special_chars_not_allowed', function(charsNotAllowed, file, cb) {
                if (file.name.match(RegExp(charsNotAllowed,'g'))) {
                    this.trigger('Error', {
                      code : plupload.FAILED,
                      message : Ext.String.format(RS.common.locale.notAllowedCharacters, charsNotAllowed.replace(/\|\\|\|/g, ','), file.name),
                      file : file
                    });
                    cb(false);
                } else {
                    cb(true);
                }
            });
            var uploader = new plupload.Uploader({
              browse_button: me.down('#browseButton').getId(),
              url: me.url,
              multipart_params: me.multipart_params,
              filters: me.filters,
            });
            uploader.init();
            uploader.bind('FilesAdded', function(up, files) {
                me.down('#fileName').setText(files[0].name, false);
                if (!me.autoUpload) {
                    uploadButton.setDisabled(false)
                } else {
                    uploader.start();
                    progressBar.wait({
                        text: RS.common.locale.updating
                    });
                    browseButton.setDisabled(true);
                } 
            });

            uploader.bind('Error', function(up, err) {
                me.down('#fileName').setText(err.message + RS.common.locale.pleaseSelectAWordDoc, false);
                !me.autoUpload? setDisabled(true): null;
            });

            uploader.bind('FileUploaded', function(up, file, response) {
                if (!!me.callback) {
                    var jsonResponse = Ext.decode(response.response);
                    if (jsonResponse.success) {
                        me.callback.call(me.view, jsonResponse);
                        clientVM.displaySuccess(RS.common.locale.succeededUploadingWordDoc, null,RS.common.locale.succeeded);
                    } else {
                        clientVM.displayError(RS.common.locale.failedUploadingWordDoc, null, RS.common.locale.failed);
                    }
                    form.up('window').close();
                }
            })

            !me.autoUpload? uploadButton.on('click', function () {
                uploader.start();
                uploadButton.setDisabled(true);
                browseButton.setDisabled(true);
                progressBar.wait({
                    text: RS.common.locale.updating
                });
            }): null;
        }
    },

    initComponent: function() {
        if (!this.autoUpload) {
            this.buttons.push({
                    text: RS.common.locale.upload,
                    itemId: 'uploadButton',
                    disabled: true
            });
        }
        this.callParent();
    }
});
*/