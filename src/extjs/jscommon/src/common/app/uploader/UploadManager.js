Ext.define('RS.formbuilder.viewer.desktop.fields.UploadManager', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.uploadmanager',

	emptyText: '',
	allowDeselect: true,

	anchor: '-20',
	height: 300,

	resizable: true,
	resizeHandles: 's',

	statusBarHidden: false,

	documentName: '',

	stopOnFirstInvalidFile: false,
	allowedExtensions: [],
	sizeLimit: null,
	autoUpload: true,
	checkForDuplicates: true,
	uploadBtnName: 'upload_button',
	allowMultipleFiles: true,
	allowDownload: true,
	showUploadedBy: true,
	inputName: 'name',
	csrftoken: '',

	config: {
		api: {
			upload: '/resolve/service/form/upload',
			downloadViaDocName: '/resolve/service/wiki/download/{0}?attach={1}',
			downloadViaId: '/resolve/service/form/downloadFile?sys_id={0}&tableName={1}&{2}',
			delete: '/resolve/service/wikiadmin/attachment/delete',
			list: '/resolve/service/wiki/getAttachments'
		},
		recordId: ''
	},

	store: null,
	initStore: function() {
		this.store = Ext.create('Ext.data.Store', {
			fields: ['sys_id', 'downloadUrl', {
				name: 'name',
				type: 'String'
			}, 'size', 'sysCreatedBy', {
				name: 'sysCreatedOn',
				type: 'date',
				dateFormat: 'time',
				format: 'time'
			}, 'status'],
			sorters: [{
				property: 'name',
				direction: 'ASC'
			}]
		});
	},

	/*
	uploader: {
		url: '/resolve/service/form/upload',
		uploadpath: 'ryan',
		autoStart: true,
		max_file_size: '2020mb',
		flash_swf_url: '/resolve/js/plupload/Moxie.swf',
		urlstream_upload: true
	},
	*/

	uploader: null,
	initUploader: function() {
		var me = this;

		var fileuploadMaxSize = (clientVM.fileuploadMaxSize || 500) * 1000000; // in Bytes

		if (this.allowedFileTypes) {
			var allowedFileTypesList = this.allowedFileTypes.split(',');
			for (var i=0; i<allowedFileTypesList.length; i++) {
				allowedFileTypesList[i] = allowedFileTypesList[i].trim();
			}
			this.allowedExtensions = allowedFileTypesList;
		}

		this.uploader = new qq.FineUploaderBasic({
			//debug: true,
			autoUpload: this.autoUpload,
			button: document.getElementById(this.uploadBtnName),
			multiple: this.allowMultipleFiles,
			request: {
				endpoint: this.api ? this.api.upload : '/resolve/service/wiki/attachment/upload',
				filenameParam: 'name',
				inputName: 'name',
				params: {
					recordId: me.recordId,
					tableName: me.fileUploadTableName,
					columnName: me.referenceColumnName
				}
			},
			validation: {
				allowedExtensions: this.allowedExtensions,
				sizeLimit: this.sizeLimit || fileuploadMaxSize,
				stopOnFirstInvalidFile: this.stopOnFirstInvalidFile
			},
			callbacks: {
				onValidateBatch: function(fileOrBlobDataArray) {
					if (me.checkForDuplicates) {
						var duplicateFiles = [];
						for (var i=0; i<fileOrBlobDataArray.length; i++) {
							var file = fileOrBlobDataArray[i];
							var dup = me.store.findRecord('name', file.name, 0, false, false, true);
							if (dup) {
								duplicateFiles.push(file.name);
							}
						}

						if (duplicateFiles.length) {
							var promise = new qq.Promise();

							var msg = duplicateFiles.join();
							if (duplicateFiles.length > 1) {
								msg += ' ' + RS.common.locale.FileUploadManager.duplicatesDetected;
							} else {
								msg += ' ' + RS.common.locale.FileUploadManager.duplicateDetected;
							}
						
							Ext.MessageBox.show({
								title: RS.common.locale.FileUploadManager.duplicateWarn,
								msg: msg,
								buttons: Ext.MessageBox.YESNO,
								buttonText: {
									yes: RS.common.locale.FileUploadManager.override,
									no: RS.common.locale.FileUploadManager.no
								},
								scope: this,
								fn: function(btn) {
									if (btn === 'yes') {
										promise.success();
									} else {
										promise.failure();
									}
								}
							});
						
							return promise;
						}
					}
				},
				onSubmitted: function(id, name) {
					if (Ext.isFunction(me.fileOnSubmittedCallback)) {
						me.fileOnSubmittedCallback(this, id, name);
					}
				},
				onCancel: function(id, name) {
					if (Ext.isFunction(me.fileOnCancelCallback)) {
						me.fileOnCancelCallback(this, id, name)
					}
				},
				onUpload: function(id, name) {
					if (Ext.isFunction(me.fileOnUploadCallback)) {
						me.fileOnUploadCallback(this, name)
					}
				},
				onProgress: function(id, name, uploadedBytes, totalBytes) {
					if (Ext.isFunction(me.fileOnProgressCallback)) {
						me.fileOnProgressCallback(this, id, name, uploadedBytes, totalBytes)
					}
				},
				onError: function(id, name, errorReason, xhr) {
					if (Ext.isFunction(me.fileOnErrorCallback)) {
						me.fileOnErrorCallback(this, name, errorReason, xhr)
					}
				},
				onComplete: function(id, name, responseJSON, xhr) {
					if (Ext.isFunction(me.fileOnCompleteCallback)) {
						me.fileOnCompleteCallback(this, name, responseJSON, xhr)
					}
				},
				onAllComplete: function(succeeded, failed) {
					if (!failed.length) {
						clientVM.displaySuccess(RS.common.locale.FileUploadManager.uploadSuccess);
					}

					if (Ext.isFunction(me.filesOnAllCompleteCallback)) {
						me.filesOnAllCompleteCallback(this, succeeded, failed)
					}
				}
			},
			showMessage: function(message) {
				clientVM.displayError(message);
			}
		});
	},

	fineUploaderTemplateHeight: 388,
	fineUploaderTemplateHidden: true,

	initComponent: function() {
		var me = this;

		clientVM.getCSRFToken_ForURI('/resolve/service/user/downloadFile', function(token_pair) {
			this.csrftoken = token_pair[0] + '=' + token_pair[1];
		}.bind(this))

		this.initStore();

		this.title = this.displayName || this.title;

		this.columns = Ext.isFunction(this.getUploadColumns) ? this.getUploadColumns() : [];

		if (!me.columns.length) {
			me.columns = [{
				text: RS.formbuilder.locale.uploadColumnName,
				flex: 3,
				sortable: false,
				dataIndex: 'name',
				renderer: function(value, metaData, record) {
					return record.get(me.nameColumn) || record.get('name')
				}
			}, {
				text: RS.formbuilder.locale.uploadColumnSize,
				width: 90,
				sortable: true,
				renderer: Ext.util.Format.fileSize,
				dataIndex: 'size'
			}, {
				text: RS.formbuilder.locale.uploadColumnChange,
				width: 75,
				sortable: true,
				hidden: true,
				hideable: false,
				dataIndex: 'percent'
			}, {
				text: RS.formbuilder.locale.uploadColumnState,
				width: 75,
				hidden: true,
				sortable: true,
				hideable: false,
				dataIndex: 'status'
			}, {
				text: RS.formbuilder.locale.uploadColumnStatus,
				width: 175,
				sortable: true,
				dataIndex: 'msg'
			}, {
				text: RS.formbuilder.locale.sysUploadedOn,
				width: 200,
				sortable: true,
				dataIndex: 'sysCreatedOn',
				renderer: function(value) {
					var r = Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat());
					if (Ext.isString(value))
						return value;
					return r(value);
				}
			}, {
				text: RS.formbuilder.locale.uploadColumnUploadedBy,
				sortable: true,
				flex: 1,
				dataIndex: 'sysCreatedBy',
			}, {
				xtype: 'actioncolumn',
				hideable: false,
				hidden: !this.allowDownload,
				width: 38,
				items: [{
					icon: '/resolve/images/arrow_down.png',
					// Use a URL in the icon config
					tooltip: RS.formbuilder.locale.download,
					handler: function(gridView, rowIndex, colIndex) {
						var rec = gridView.getStore().getAt(rowIndex),
							sys_id = rec.get('sys_id'),
							api = gridView.ownerCt.api;
						if (gridView.ownerCt.documentName && gridView.ownerCt.documentName.length > 1) { // Configurable
							gridView.ownerCt.downloadURL(rec.raw.downloadUrl || Ext.String.format(api.downloadViaDocName, gridView.ownerCt.documentName.replace(/[.]/g, '/'), rec.get('id')))
							return
						}
	
						if (sys_id) gridView.ownerCt.downloadURL(Ext.String.format(api.downloadViaId, sys_id, gridView.ownerCt.fileUploadTableName, gridView.ownerCt.csrftoken));
						else clientVM.displayError(RS.formbuilder.locale.noDownloadBody)
					}
				}]
			}]
		}

		this.tbar = [{
			text: RS.formbuilder.locale.upload,
			name: 'uploadFile',
			itemId: 'uploadButton',
			id: 'upload_button',
			tooltip: RS.formbuilder.locale.uploadTooltip,
			hidden: !this.allowUpload,
			preventDefault: false
		}, {
			text: RS.formbuilder.locale.removeFile,
			disabled: true,
			name: 'removeFile',
			itemId: 'removeButton',
			hidden: !this.allowRemove,
			scope: this,
			handler: function(button) {
				var selections = this.getSelectionModel().getSelection();
				Ext.MessageBox.show({
					title: RS.formbuilder.locale.confirmRemove,
					msg: selections.length === 1 ? RS.formbuilder.locale.confirmRemoveFile : Ext.String.format(RS.formbuilder.locale.confirmRemoveFiles, selections.length),
					buttons: Ext.MessageBox.YESNO,
					buttonText: {
						yes: RS.formbuilder.locale.removeFile,
						no: RS.formbuilder.locale.cancel
					},
					scope: me,
					fn: me.removeSelectedFiles
				});
			}
		}];

		this.statusBar = new Ext.ux.StatusBar({
			dock: 'bottom',
			defaultText: RS.formbuilder.locale.ready,
			hidden: this.statusBarHidden
		});
		this.dockedItems = [this.statusBar];

		this.callParent();

		this.listeners = {
			afterRender: {
				fn: function() {
					me.initUploader();
					me.setEnabled(me.recordId);
				},
				single: true,
				scope: me
			}
		};

		this.on({
			scope: this,
			afterlayout: function(grid, layout, eOpts) {
				//var upEl = Ext.get(grid.uploader.uploader.id + '_' + grid.uploader.uploader.runtime + '_container');
				//if (upEl) upEl.setTop(0).setWidth(100);
			},
			afterrender: function(grid) {
				grid.view.refresh();
			},
			selectionchange: function(selectionModel, selections, eOpts) {
				if (selections.length > 0) this.down('#removeButton').enable();
				else this.down('#removeButton').disable();
			},
			/*
			updateprogress: function(uploader, total, percent, sent, success, failed, queued, speed) {
				var uploading = false;
				Ext.each(uploader.uploader.files, function(file) {
					if (file.status != plupload.DONE) uploading = true;
					return false;
				});

				if (uploading) {
					var t = Ext.String.format(RS.formbuilder.locale.updateProgressText, percent, sent, total);
					this.statusBar.showBusy({
						text: t,
						clear: false
					});
					this.statusBar.isBusy = true;
				} else {
					this.statusBar.isBusy = false;
					this.statusBar.clearStatus();
					this.statusBar.setStatus({
						text: RS.formbuilder.locale.ready
					});
				}
			},
			uploadprogress: function(uploader, file, name, size, percent) {
				// me.statusBar.setText(name + ' ' + percent + '%');
			},
			*/
			uploadcomplete: function(uploader, success, failed) {
				this.statusBar.isBusy = false;
				this.statusBar.clearStatus();
				this.statusBar.setStatus({
					text: RS.formbuilder.locale.ready
				});
			}
		});

		this.loadFileUploads();
	},

	fileOnSubmittedCallback: function(manager, filename) {
		this.uploader.setParams({
			recordId: this.recordId,
			tableName: this.fileUploadTableName,
			columnName: this.referenceColumnName
		});
	},

	fileOnErrorCallback: function(manager, name, reason, resp) {
		var respData = RS.common.parsePayload(resp);
		if (respData.message && respData.errCode && respData.errCode != 'INVALID_SERVER_RESPONSE') {
			clientVM.displayError(name + ': ' + respData.message);
		} else {
			clientVM.displayError(reason);
		}
	},

	filesOnAllCompleteCallback: function() {
		this.loadFileUploads();
	},

	removeSelectedFiles: function(button) {
		if (button === 'yes') {
			var selections = this.getSelectionModel().getSelection(),
				ids = [],
				fileIds = [];
			Ext.each(selections, function(selection) {
				ids.push(selection.get('sys_id'));
				fileIds.push(selection.get('id'));
			});
			//Ext.each(fileIds, function(fileId) {
			//	this.uploader.removeFile(fileId)
			//}, this);
			if (this.documentName && this.documentName.length > 1) {
				var api = this.api;
				this.lastDocumentName = this.documentName;
				Ext.Ajax.request({
					url: api.delete, // Configurable
					params: {
						docSysId: '',
						docFullName: this.documentName,
						ids: ids
					},
					success: this.loadFiles,
					scope: this
				})
			} else {
				Ext.Ajax.request({
					url: '/resolve/service/form/deleteFile',
					params: {
						tableName: this.fileUploadTableName,
						ids: ids
					},
					scope: this,
					success: this.removeDeletedFiles
				})
			}
		}
	},

	removeDeletedFiles: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			this.loadFileUploads();
		}
	},

	loadFileUploads: function() {
		if (this.preventLoading) return

		if (this.documentName && this.documentName.length > 1) {
			if (this.documentName.toLowerCase() != this.lastDocumentName) {
				var api = this.api;
				this.lastDocumentName = this.documentName.toLowerCase();
				Ext.Ajax.request({
					url: api.list,
					method: 'GET',
					params: {
						docSysId: '',
						docFullName: this.documentName
					},
					success: this.loadFiles,
					scope: this
				})
			}
		} else {
			var field = this.referenceColumnName;
			if (this.referenceColumnName == 'u_content_request' && this.fileUploadTableName != 'content_request') {
				field += '.sys_id';
			}

			Ext.Ajax.request({
				url: '/resolve/service/dbapi/getRecordData',
				params: {
					tableName: this.fileUploadTableName,
					type: 'table',
					start: 0,
					filter: Ext.encode([{
						field: field,
						type: 'auto',
						condition: 'equals',
						value: this.recordId || '__blank'
					}])
				},
				success: this.loadFiles,
				scope: this
			})
		}
	},

	loadFiles: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			if (response.records) {
				Ext.each(response.records, function(record) {
					Ext.apply(record, {
						id: record.sys_id || record.id,
						name: record.u_filename || record.name,
						size: record.u_size || record.size,
						sys_id: record.sys_id,
						sysCreatedOn: record.sysCreatedOn || record.sys_created_on,
						sysCreatedBy: record.sysCreatedBy || record.sys_created_by
					});
					//if (this.uploader && this.uploader.uploader)
					//	Ext.each(this.uploader.uploader.files || [], function(file) {
					//		if (file.sysId == record.sysId) {
					//			record.id = file.id;
					//		}
					//	});
				}, this);
				this.store.loadData(response.records, false);
			}
		}
	},

	/**
	 * @private
	 */
	/*
	createUploader: function() {
		return Ext.create('RS.formbuilder.viewer.desktop.fields.PlUpload', this, Ext.applyIf({
			listeners: {}
		}, this));
	}, 
	*/ 

	setEnabled: function(enable) {
		//if (enable) this.uploader.multipart_params.recordId = enable;
		enable = enable && !this.internalDisabled;
		var uploadButton = this.down('#uploadButton');
		uploadButton[enable ? 'enable' : 'disable']();
		uploadButton.ownerCt[enable ? 'enable' : 'disable']();
		//if (this.uploader.uploader) this.uploader.uploader.disabled = !enable;
		if (this.isEditMode === false) {
			this.view.emptyText = '';
		} else if (enable) {
			this.view.emptyText = RS.formbuilder.locale.dragDropDisabledEmptyText;
		} else {
			this.view.emptyText = RS.formbuilder.locale.noRecordIdForUpload;
		}
		this.view.refresh()
		//if (enable) this.uploader.uploader.refresh()
	},

	disable: function() {
		this.internalDisabled = true;
		this.setEnabled(false);
	},
	enable: function() {
		if (this.rendered) {
			this.internalDisabled = false;
			this.setEnabled(true);
		}
	},

	downloadURL: function(url) {
		var iframe;
		iframe = document.getElementById("hiddenDownloader");
		if (iframe === null) {
			iframe = document.createElement('iframe');
			iframe.id = "hiddenDownloader";
			iframe.style.display = 'none';
			document.body.appendChild(iframe);
		}
		iframe['src'] = url.replace(/javascript\:/g, '');
	},

	isUploading: function() {
		return this.statusBar.isBusy || false;
	},

	setDocumentName: function(documentName) {
		this.documentName = documentName;
		if (this.uploader) this.uploader.documentName = documentName;
		this.loadFileUploads();
	}
});
