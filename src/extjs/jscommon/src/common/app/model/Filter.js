Ext.define('RS.common.model.Filter', {
	extend: 'Ext.data.Model',
	idProperty: 'sysId',
	fields: ['sysId', 'name', 'value']
});