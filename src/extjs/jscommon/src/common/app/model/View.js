Ext.define('RS.common.model.View', {
	extend: 'Ext.data.Model',
	idProperty: 'id',
	fields: ['id', 'name', 'value']
});