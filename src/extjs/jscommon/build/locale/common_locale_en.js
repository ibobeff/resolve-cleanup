/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.common').locale = {
	error: 'Error',
	success: 'Success',
	close: 'Close',
	deleteText: 'Delete',
	deleteTitle: 'Confirm Delete',

	pageInfo: 'Page Info',

	deleteFilterMessage: 'Are you sure you want to delete the selected filter?  This action cannot be undone.',
	deleteFiltersMessage: 'Are you sure you want to delete the {0} selected filters?  This action cannot be undone.',
	deleteViewMessage: 'Are you sure you want to delete the selected view?  This action cannot be undone.',
	deleteViewsMessage: 'Are you sure you want to delete the {0} selected views?  This action cannot be undone.',

	invalidJSON: 'The server replied with an invalid JSON string',

	//Paging
	pageDisplayText: '{0} - {1} of {2}',
	firstText: 'First Page',
	prevText: 'Previous Page',
	nextText: 'Next Page',
	lastText: 'Last Page',
	refreshText: 'Refresh',
	configureText: 'Configure',
	emptyMsg: 'No Records',
	goToText: 'Go to',

	//Auto Refresh
	autoRefreshText: 'Auto Refresh (secs)',

	//Search
	egText: '  e.g. Name equals John Doe',
	noSuggestions: 'No suggestions for your search, try searching by a column name',

	search: 'Search',

	//Conditions
	on: 'on',
	after: 'after',
	before: 'before',
	equals: 'equals',
	notEquals: 'not equals',
	contains: 'contains',
	notContains: 'not contains',
	startsWith: 'starts with',
	notStartsWith: 'not starts with',
	endsWith: 'ends with',
	notEndsWith: 'not ends with',
	greaterThan: 'greater than',
	greaterThanOrEqualTo: 'greater than or equal to',
	lessThan: 'less than',
	lessThanOrEqualTo: 'less than or equal to',
	shortEquals: '=',
	shortNotEquals: '!=',
	shortGreaterThan: '>',
	shortGreaterThanOrEqualTo: '>=',
	shortLessThan: '<',
	shortLessThanOrEqualTo: '<=',

	//Values
	today: 'today',
	yesterday: 'yesterday',
	lastWeek: 'last week',
	lastMonth: 'last month',
	lastYear: 'last year',

	//Labels
	Query: 'Query',
	Filter: 'Filter',
	manageFilters: 'Manage Filters',
	SelectFilter: 'Please select a filter',
	filterDeleted: 'Filter deleted successfully',
	filtersDeleted: 'Filters deleted successfully',

	//Filter Toolbar
	saveFilter: 'Save',
	editFilter: 'Edit',
	clearFilter: 'Clear',
	saveFilterTitle: 'Save Filter',
	saveFilterMessage: 'Please enter the name of the filter:',
	cancel: 'Cancel',
	saveFilterTitleConfirm: 'Filter already exists',
	saveFilterMessageConfirm: 'A filter with the name <b>{0}</b> already exist, are you sure you want to overwrite that existing filter?',
	saveFilterConfirm: 'Overwrite',
	filterSaved: 'Filter was saved successfully',
	filterTitle: 'Filters',

	// View Toolbar
	saveViewTitle: 'Save View',
	saveViewMessage: 'Please enter the name of the view:',
	saveViewTitleConfirm: 'View already exists',
	saveViewMessageConfirm: 'A view with the name <b>{0}</b> already exist, are you sure you want to overwrite that existing view?',
	saveView: 'Save View',
	saveViewConfirm: 'Overwrite',
	viewSaved: 'View was saved successfully',
	saveSystemViewTitle: 'System View already exists',
	saveSystemViewBody: 'You cannot save a view with that name because a system view with that name already exists.  Please choose a different name.',
	views: 'Views',
	viewTitle: 'Views',
	settings: 'Settings',
	manage: 'Manage',
	saveViewAs: 'Save View As',

	//View Column
	viewColumnTooltip: 'Go To',
	selectAll: 'Select all pages ({0})',
	selectAllTooltip: 'Select All',

	//Boolean Column
	trueText: 'True',
	falseText: 'False',

	//Edit Column
	editColumnTooltip: 'Edit',
	gotoDetailsColumnTooltip: 'View Details',

	//Updated Date Renderer
	seconds: 'seconds',
	minute: 'minute',
	minutes: 'minutes',
	hour: 'hour',
	hours: 'hours',
	yesterday: 'Yesterday',
	days: 'days',
	ago: 'ago',

	//SysInfoButton
	sysInfoButtonTooltip: 'System Information',
	sysInfoButtonTitle: 'System Information',
	sysId: 'System ID',
	sysCreated: 'Created On',
	sysCreatedBy: 'Created By',
	sysUpdated: 'Updated On',
	sysUpdatedBy: 'Updated By',
	sysOrg: 'Organization',
	unknownDate: 'Unknown Date',
	sysCreatedOn: 'Created On',
	sysUpdatedOn: 'Updated On',
	sysOrganizationName: 'Organization',
	sys_id: 'Sys ID',
	createdOn: 'Created On',
	updatedOn: 'Updated On',

	//Follow Button
	followButtonTooltip: 'Configure Social Followers',


	//Grid Picker
	dump: 'OK',
	keyword: 'Search',

	//Access Right Picker
	accessRights: 'Access Rights',
	add: 'Add',
	Roles: 'Roles',
	showRoleList: 'Add Roles',
	removeAccessRight: 'Remove Roles',
	name: 'Name',
	adminRight: 'Admin',
	writeRight: 'Edit',
	readRight: 'View',
	executeRight: 'Execute',
	rolesPicker: 'Roles',
	uname: 'Name',
	udescription: 'Description',
	rolesDisplayName: 'Roles',
	defaultRolesText: 'Apply Default Roles',
	//wiki search widget
	ufullname: 'Full Name',
	WikiDocSearch: {
		title: 'Search Wiki',
		filter: 'Filter the wiki doc...',
		type: 'Search type',
		choose: 'Select'
	},

	//Encryption/Decryption
	decriptionTitle: 'Decryption Key Required',
	decriptionMessage: 'Enter the decryption key:',
	invalidKeyTitle: 'Invalid decryption key',
	invalidKeyMessage: 'The key you entered was invalid.',
	decryptErrorTitle: 'An error occurred while decrypting',
	decryptErrorMessage: 'There was an error while decrypting with the provided key.',

	//Uploader
	overwriteTitle: 'File already exists',
	overwriteMsg: 'Do you want to overwrite {0}?',
	overwriteAttachment: 'Overwrite',
	download: 'Download',

	// Single File Uploader 
	selectAWordDocFile: 'Browse..', 
	fileName: 'File name',
	upload: 'Upload',
	succeeded: 'Succeeded',
	failed: 'Failed',
	succeededUploadingWordDoc: 'Succeeded uploading a word docx.',
	failedUploadingWordDoc: 'Failed uploading a word docx.',
	uploadAWordDocument: 'Upload a Word Document (docx)',
	uploadWordDocument: 'Upload Word Document (docx)',
	noFileSelected: 'no file selected.',
	pleaseSelectAWordDoc: ' Please select a word docx.',
	updating: 'File being uploaded...',
	notAllowedCharacters: '\'<b>{1}</b>\' contains one or more special characters \'<b>{0}</b>\' which are not supported.<br>',

	ContentPicker : {
		page : 'Page',
		automation : 'Automation',
		playbook : 'Playbook Template',
		namespace : 'Namespace',
		all : 'All'
	},

	FileUploadManager: {
		dragText: 'DROP FILES HERE',
		deleteFailed: 'Delete failed',
		uploadFailed: 'Upload failed',
		uploadSuccess: 'Upload successful',
		duplicateWarn: 'Duplicate detected',
		duplicateDetected: 'is already in the attachment list.',
		duplicatesDetected: 'are already in the attachment list.',
		override: 'Override',

		processingDroppedFiles: 'Processing dropped files...',
		editFilename: 'Edit filename',
		cancel: 'Cancel',
		retry: 'Retry',
		delete: 'Delete',
		close: 'Close',
		no: 'No',
		yes: 'Yes',
		ok: 'Ok'
	},

	UploadWordDoc: {
		browse: 'Browse',
		dragHere: 'Drop your Word Document (docx) here',
		uploadIntro: 'Please click "Browse" button to select a Word Document for uploading or drag the file into the area below.',
	}
}
