/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/
Ext.define('RS.common.model.Filter', {
	extend: 'Ext.data.Model',
	idProperty: 'sysId',
	fields: ['sysId', 'name', 'value']
});
Ext.define('RS.common.model.View', {
	extend: 'Ext.data.Model',
	idProperty: 'id',
	fields: ['id', 'name', 'value']
});
glu.defModel('RS.common.AccessRights', {
	accessRights: null,

	sys_id: '',

	rightsLoader: function() {},

	title: '',

	defaultRoles: false,

	defaultRolesIsVisible: true,

	bodyPadding: '0px',

	defaultRolesText: '~~defaultRolesText~~',

	isWindow: false,

	local: false,

	rightsShown: ['admin', 'write', 'read'],

	fields: ['uname'],

	columns: null,

	rightsIsEnabled$: function() {
		if (this.defaultRoles)
			this.set('rightsSelections', []);
		return !this.defaultRoles
	},

	dumper: function() {
		this.parentVM.set('accessRights', this.toCVS());
	},

	rightsSelections: [],

	activate: function(screen, params) {
		this.set('isWindow', params.isWindow);
	},

	init: function() {
		Ext.each(this.rightsShown, function(r) {
			this.fields.push({
				name: r,
				type: 'bool'
			});
		}, this);
		this.createStore(this.local);
		if (!this.title)
			this.set('title', this.localize('AccessRights'));
		this.rightsLoader();
		if (this.isWindow)
			this.set('bodyPadding', '10px');
		this.set('defaultRolesText', this.localize('defaultRolesText'));
	},

	createStore: function(local) {
		var cols = [];
		Ext.each(this.fields, function(f) {
			var nonBoolCol = {
				filterable: true,
				sortable: true,
				flex: 1
			};
			var boolCol = {
				xtype: 'checkcolumn',
				stopSelection: false,
				width: 70
			};
			var c = {
				header: '~~' + (f.name || f) + (f.type == 'bool' ? 'Right' : '') + '~~',
				dataIndex: f.name || f,
				hideable: false
			};
			if (f.type == 'bool')
				Ext.apply(c, boolCol);
			else
				Ext.apply(c, nonBoolCol);
			cols.push(c)
		});

		this.set('columns', cols);
		if (local) {
			this.set('accessRights', new Ext.create('Ext.data.Store', {
				mtype: 'store',
				fields: this.fields,
				proxy: {
					type: 'memory',
					reader: {
						type: 'json',
						root: 'records'
					}
				}
			}));
			return;
		}
		this.set('accessRights', new Ext.create('Ext.data.Store', {
			mtype: 'store',
			fields: this.fields,
			remoteSort: true,
			baseParams : {
				includePublic : "true"
			},
			sorters: [{
				property: 'uname',
				direction: 'ASC'
			}],
			proxy: {
				type: 'ajax',
				url: '/resolve/service/common/roles/list',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		}));
		/*
		this.on('load', function(store, rec, suc) {
			if (!suc)
				clientVM.displayError('listRolesErrMsg');

		});
		*/
	},

	clearGrid: function() {
		this.set('sys_id', '')
		this.accessRights.loadData([], false);
	},

	loadRights: function(data) {
		this.set('sys_id', data.sys_id);
		var recs = this.cvsToRows(data);
		this.accessRights.add(recs);
	},

	cvsToRows: function(data) {
		var revert = {};
		this.accessRights.loadData([], false);

		Ext.each(this.rightsShown, function(r) {
			var cvs = data['u' + r + 'Access']
			if (!cvs)
				return;
			var roles = cvs.split(',');
			Ext.each(roles, function(role) {
				if (!role)
					return;
				if (!revert[role])
					revert[role] = {};
				revert[role][r] = true;
			});
		});
		var recs = [];
		for (role in revert) {
			revert[role].uname = role;
			recs.push(revert[role]);
		}

		return recs;
	},

	addAccessRights: function(rights) {
		Ext.each(rights, function(accessRight) {
			if (this.accessRights.find('uname', accessRight.get('uname')) != -1)
				return;
			this.accessRights.add(Ext.applyIf(accessRight.data, {
				'admin': true,
				'write': true,
				'read': true,
				'execute': true
			}));
		}, this);
	},

	showRoleList: function() {
		this.open({
			mtype: 'RS.common.KeywordGridPicker',
			width: 710,
			title: this.localize('rolesDisplayName'),
			storeFields: ['uname'],
			searchFields: ['uname'],
			columns: [{
				header: this.localize('uname'),
				dataIndex: 'uname',
				filterable: true,
				flex: 1
			}],
			storeUrl: '/resolve/service/common/roles/list',
			baseParams : {
				includePublic : "true"
			},
			dumperText: this.localize('add'),
			dumper: (function(self) {
				return function(records) {
					Ext.each(records, function(r, idx) {
						self.addAccessRights(r);
					});
					return true;
				}
			})(this)
		});
	},

	showRoleListIsEnabled$: function() {
		return this.rightsIsEnabled;
	},

	toCVS: function(reverse) {
		var accessRights = {
			sys_id: this.sys_id
		};

		Ext.each(this.rightsShown, function(shown) {
			accessRights['u' + shown + 'Access'] = [];
		});

		this.accessRights.each(function(r) {
			Ext.each(this.rightsShown, function(shown) {
				if (reverse ? !r.get(shown) : r.get(shown))
					accessRights['u' + shown + 'Access'].push(r.get('uname'));
			}, this);
		}, this);

		Ext.each(this.rightsShown, function(shown) {
			accessRights['u' + shown + 'Access'] = accessRights['u' + shown + 'Access'].join(',');
		});

		return accessRights;
	},

	removeAccessRight: function() {
		this.accessRights.remove(this.rightsSelections);
	},

	removeAccessRightIsEnabled$: function() {
		return this.rightsSelections.length > 0 && this.rightsIsEnabled;
	},

	// removeAccessRightIsVisible$: function() {
	// 	return !this.local;
	// },

	dump: function() {
		if (Ext.isFunction(this.dumper))
			this.dumper(this.rightsSelections);
		else
			this.dumper.dump.call(this.dumper.scope, this.rightsSelections);
		this.doClose();
	},

	cancel: function() {
		this.doClose();
	}
});
glu.defModel('RS.common.ContentPicker',{
	title : '',
	selected: null,	
	content : '',
	contentType : 'page',
	contentStore : null,
	selectedNamespace: '',
	columns : '',
	API : {
		getModule : '/resolve/service/nsadmin/list',
		getpage : '/resolve/service/wiki/list',
		getautomation : '/resolve/service/wikiadmin/listRunbooks',
		getplaybook : '/resolve/service/wiki/listPlaybookTemplates'
	},
	contentModelMap : {
		page : 'RS.wiki.Main',
		automation : 'RS.wiki.Main'
	},
	moduleStore: {
		mtype: 'treestore',
		fields: ['id', 'unamespace'],
		root: {
			unamespace: 'All'
		}		
	},
	expandingToNode : false,
	init: function() {
		var contentType = this.contentType.toLowerCase();
		this.set('title', this.localize(contentType));
		var storeUrl = this.API['get' + contentType];
		this.set('columns',[{
			header: '~~name~~',
			dataIndex: 'ufullname',
			filterable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()));
		var store = Ext.create('Ext.data.Store', {		
			sorters: ['ufullname'],
			fields: ['id', 'ufullname', 'usummary', 'uresolutionBuilderId','uwikiParameters','uisRoot'].concat(RS.common.grid.getSysFields()),
			remoteSort: true,
			proxy: {
				type: 'ajax',
				url: storeUrl,
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});
		store.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {};
			var filter = [];
			if (this.selectedNamespace && this.selectedNamespace.toLowerCase() != 'all')
				filter.push({
					field: 'unamespace',
					type: 'auto',
					condition: 'equals',
					value: this.selectedNamespace
				});			
			Ext.apply(operation.params, {
				filter: Ext.encode(filter)
			})
		}, this);
		this.set('contentStore', store);
		this.contentStore.load();

		this.moduleStore.on('expand', function(node) {
			if (!this.expandingToNode)
				this.expandModuleNode(node);
		}, this);
		this.moduleStore.getRootNode().expand();
	},
	expandModuleNode: function(node) {		
		this.ajax({
			url: this.API['getModule'],
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(respData.message);
					return;
				}
				node.removeAll();
				var childNodes = [];
				Ext.Object.each(respData.records, function(i,r) {
					childNodes.push({
						id: Ext.data.IdGenerator.get('uuid').generate(),
						unamespace: r.unamespace,
						leaf: true
					});
				}, this)
				node.appendChild(childNodes);
				this.set('expandingToNode', true);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}			
		});
	},
	menuPathTreeSelectionchange: function(selected, eOpts) {
		if (selected.length > 0) {
			var nodeId = selected[0].get('unamespace')
			this.set('selectedNamespace', nodeId);
			this.contentStore.load();
		}
	},
	editContent: function(name) {		
		clientVM.handleNavigation({
			modelName: this.contentModelMap[this.contentType],
			target: '_blank',
			params: {
		/*		activeTab: (this.runbookOnly ? 2 : 0),*/
				name: name
			}
		})		
	},
	dumper: null,
	dump: function() {
		if (Ext.isFunction(this.dumper))
            this.dumper(this.selected);
        else {
            var scope = this.dumper.scope || this;
            this.dumper.dump.call(scope, this.selected);
        }
		this.doClose()
	},
	dumpIsEnabled$: function() {
		return !!this.selected
	},
	cancel: function() {		
		this.doClose()
	}	
});
glu.defModel('RS.common.UploadFile', {
	mixins: ['FileUploadManager'],
})

glu.defModel('RS.common.FileUploadManager', {
	title: '',
	name: '',
	id: '',

	allowUpload: true,
	allowRemove: true,

	uploader: null,
	api: {
		list: '/resolve/service/wiki/getAttachments',
		upload: '/resolve/service/wiki/attachment/upload',
		delete: '/resolve/service/wikiadmin/attachment/delete',
	},

	stopOnFirstInvalidFile: false,
	allowedExtensions: [],
	sizeLimit: null,
	autoUpload: true,
	checkForDuplicates: true,
	uploadBtnName: 'upload_button',
	allowMultipleFiles: true,
	allowDownload: true,
	showUploadedBy: true,
	inputName: 'name',

	fineUploaderTemplateHidden: true,
	fineUploaderTemplateHeight: 388,
	showProgress: false,

	filesOnAllCompleteCallback: function() {
		this.loadFiles();
	},

	filesStore: null,

	filesColumns: [],

	loadFiles: function() {
		if (this.api.list) {
			this.filesStore.load({
				params: {
					docFullName: this.name,
					docSysId: ''
				}
			});
		}
	},

	initStore: function() {
		var store = Ext.create('Ext.data.Store', {
			fields: ['sys_id', 'sysId', 'downloadUrl', 'tableName', {
				name: 'fileName',
				type: 'String'
			}, 'size', 'uploadedBy', {
				name: 'sysUpdatedOn',
				type: 'date',
				dateFormat: 'time',
				format: 'time'
			}, 'status'],
			sorters: [{
				property: 'fileName',
				direction: 'ASC'
			}],
			proxy: {
				type: 'ajax',
				url: this.api ? this.api.list : '/resolve/service/wiki/getAttachments',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});
		this.set('filesStore', store);
	},

	init: function() {
		this.initMixin();
		this.set('dragHereText', this.localize('dragText'));
	},

	isEditMode: false,

	initMixin: function() {
		this.initStore();
		this.set('filesColumns', [{
			header: RS.formbuilder.locale.uploadColumnName,
			flex: 2,
			dataIndex: 'fileName',
			renderer : function(value, metaData, record) {
				var name = value;
				if (record.raw.global)
					name += ' <span style="font-weight:600;">(Global)</span>';
				return name;
			}
		}, {
			header: RS.formbuilder.locale.uploadColumnSize,
			dataIndex: 'size',
			renderer: function(value) {
				return Ext.util.Format.fileSize(value);
			}
		}, {
			header: RS.formbuilder.locale.uploadColumnStatus,
			flex: 1,
			dataIndex: 'status',
			hidden: !this.isEditMode,
		}, {
			header: RS.formbuilder.locale.sysUploadedOn,
			dataIndex: 'sysUpdatedOn',
			width: 175,
			renderer: function(value) {
				//return Ext.Date.format(new Date(value), 'Y-m-d g:i:s A');
				return Ext.Date.format(value, clientVM.userDefaultDateFormat)
			}
		}, {
			header: RS.formbuilder.locale.uploadColumnUploadedBy,
			hidden: !this.showUploadedBy,
			dataIndex: 'uploadedBy'
		}, {
			xtype: 'actioncolumn',
			hideable: false,
			hidden: !this.allowDownload,
			width: 38,
			items: [{
				icon: '/resolve/images/arrow_down.png',
				tooltip: RS.formbuilder.locale.download,
				handler: function(grid, rowIndex, colIndex) {
					var rec = grid.getStore().getAt(rowIndex),
						url = rec.get('downloadUrl');
					if (url) grid.ownerCt._vm.downloadURL(url);
				}
			}]
		}])

		this.loadFiles();

		setTimeout(function(){
			this.initUploader();
		}.bind(this), 100)
	},



	initUploader: function() {
		var me = this;
		var extraDropzones = [];
		var attachments_grid = document.getElementById('attachments_grid');
		if (attachments_grid) {
			extraDropzones.push(attachments_grid.parentElement.parentElement);
		}

		var fileuploadMaxSize = (clientVM.fileuploadMaxSize || 500) * 1000000; // in Bytes

		this.uploader = new qq.FineUploader({
			//debug: true,
			autoUpload: this.autoUpload,
			template: 'qq-template',
			element: document.getElementById('fine-uploader'),
			button: document.getElementById(this.uploadBtnName),
			multiple: this.allowMultipleFiles,
			dragAndDrop: {
				extraDropzones: extraDropzones
			},
			request: {
				endpoint: this.api ? this.api.upload : '/resolve/service/wiki/attachment/upload',
				filenameParam: 'overriddenFilename',  // Allowing filename being editable; used by the back end
				inputName: 'name',
				params: {
					recordId: this.name,
					docFullName: this.name
				}
			},
			validation: {
				allowedExtensions: this.allowedExtensions,
				sizeLimit: this.sizeLimit || fileuploadMaxSize,
				stopOnFirstInvalidFile: this.stopOnFirstInvalidFile
			},
			callbacks: {
				onValidateBatch: function(fileOrBlobDataArray) {
					if (me.checkForDuplicates) {
						var duplicateFiles = [];
						for (var i=0; i<fileOrBlobDataArray.length; i++) {
							var file = fileOrBlobDataArray[i];
							var dup = me.filesStore.findRecord('fileName', file.name, 0, false, false, true);
							if (dup) {
								duplicateFiles.push(file.name);
							}
						}

						if (duplicateFiles.length) {
							var promise = new qq.Promise();

							var msg = duplicateFiles.join();
							if (duplicateFiles.length > 1) {
								msg += ' ' + RS.common.locale.FileUploadManager.duplicatesDetected;
							} else {
								msg += ' ' + RS.common.locale.FileUploadManager.duplicateDetected;
							}

							me.message({
								title: RS.common.locale.FileUploadManager.duplicateWarn,
								msg: msg,
								buttons: Ext.MessageBox.YESNO,
								buttonText: {
									yes: RS.common.locale.FileUploadManager.override,
									no: RS.common.locale.FileUploadManager.no
								},
								scope: this,
								fn: function(btn) {
									if (btn === 'yes') {
										promise.success();
									} else {
										promise.failure();
									}
								}
							});

							return promise;
						}
					}
				},
				onSubmitted: function(id, name) {
					if (Ext.isFunction(me.fileOnSubmittedCallback)) {
						me.fileOnSubmittedCallback(this, id, name);
					}
				},
				onCancel: function(id, name) {
					if (Ext.isFunction(me.fileOnCancelCallback)) {
						me.fileOnCancelCallback(this, id, name)
					}
				},
				onUpload: function(id, name) {
					if (Ext.isFunction(me.fileOnUploadCallback)) {
						me.fileOnUploadCallback(this, name)
					}
				},
				onProgress: function(id, name, uploadedBytes, totalBytes) {
					if (Ext.isFunction(me.fileOnProgressCallback)) {
						me.fileOnProgressCallback(this, id, name, uploadedBytes, totalBytes)
					}
				},
				onError: function(id, name, errorReason, xhr) {
					if (Ext.isFunction(me.fileOnErrorCallback)) {
						me.fileOnErrorCallback(this, name, errorReason, xhr)
					}
				},
				onComplete: function(id, name, responseJSON, xhr) {
					if (Ext.isFunction(me.fileOnCompleteCallback)) {
						me.fileOnCompleteCallback(this, name, responseJSON, xhr)
					}
				},
				onAllComplete: function(succeeded, failed) {
					if (!failed.length) {
						clientVM.displaySuccess(RS.common.locale.FileUploadManager.uploadSuccess);
					}

					if (Ext.isFunction(me.filesOnAllCompleteCallback)) {
						me.filesOnAllCompleteCallback(this, succeeded, failed)
					}
				}
			},
			showMessage: function(message) {
				clientVM.displayError(message);
			}
		});
	},

	dragHereText: '',

	progressHtml$: function() {
		if (this.showProgress) {
			return '<div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container"><div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div></div>\n';
		} else {
			return '';
		}
	},

	uploaderAdditionalClass: '',

	fineUploaderTemplate$: function() {
		return '\n'+
		'<div id="fine-uploader"></div>\n'+
		'<script type="text/template" id="qq-template">\n'+
			'<div class="'+this.uploaderAdditionalClass+' qq-uploader-selector qq-uploader" qq-drop-area-text="'+this.dragHereText+'">\n'+
				this.progressHtml+
				'<div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>\n'+
					'<span class="qq-upload-drop-area-text-selector"></span>\n'+
				'</div>\n'+
				'<span class="qq-drop-processing-selector qq-drop-processing">\n'+
					'<span>'+RS.common.locale.FileUploadManager.processingDroppedFiles+'</span>\n'+
					'<span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>\n'+
				'</span>\n'+
				'<ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">\n'+
					'<li>\n'+
						'<div class="qq-progress-bar-container-selector">\n'+
							'<div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>\n'+
						'</div>\n'+
						'<span class="qq-upload-spinner-selector qq-upload-spinner"></span>\n'+
						'<img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>\n'+
						'<span class="qq-upload-file-selector qq-upload-file"></span>\n'+
						'<input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">\n'+
						'<span class="qq-btn-container">\n'+
							'<span class="qq-upload-size-selector qq-upload-size"></span>\n'+
							'<span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="'+RS.common.locale.FileUploadManager.editFilename+'"></span>\n'+
							'<button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">'+RS.common.locale.FileUploadManager.cancel+'</button>\n'+
							'<button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">'+RS.common.locale.FileUploadManager.retry+'</button>\n'+
							'<button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">'+RS.common.locale.FileUploadManager.delete+'</button>\n'+
						'</span>\n'+
						'<span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>\n'+
					'</li>\n'+
				'</ul>\n'+
				'<dialog class="qq-alert-dialog-selector">\n'+
					'<div class="qq-dialog-message-selector"></div>\n'+
					'<div class="qq-dialog-buttons">\n'+
						'<button type="button" class="qq-cancel-button-selector">'+RS.common.locale.FileUploadManager.close+'</button>\n'+
					'</div>\n'+
				'</dialog>\n'+
				'<dialog class="qq-confirm-dialog-selector">\n'+
					'<div class="qq-dialog-message-selector"></div>\n'+
					'<div class="qq-dialog-buttons">\n'+
						'<button type="button" class="qq-cancel-button-selector">'+RS.common.locale.FileUploadManager.no+'</button>\n'+
						'<button type="button" class="qq-ok-button-selector">'+RS.common.locale.FileUploadManager.yes+'</button>\n'+
					'</div>\n'+
				'</dialog>\n'+
				'<dialog class="qq-prompt-dialog-selector">\n'+
					'<div class="qq-dialog-message-selector"></div>\n'+
					'<input type="text">\n'+
					'<div class="qq-dialog-buttons">\n'+
						'<button type="button" class="qq-cancel-button-selector">'+RS.common.locale.FileUploadManager.cancel+'</button>\n'+
						'<button type="button" class="qq-ok-button-selector">'+RS.common.locale.FileUploadManager.ok+'</button>\n'+
					'</div>\n'+
				'</dialog>\n'+
			'</div>\n'+
		'</script>\n'
	},

	multiSelect: true,

	selections: [],

	selectionChanged: function(selected) {
		this.set('selections', selected)
	},

	uploadFile: function() {
	},

	deleteIsEnabled$: function() {
		return this.selections.length > 0;
	},
	delete: function() {
		this.message({
			title: RS.formbuilder.locale.confirmRemove,
			msg: this.selections.length === 1 ? RS.formbuilder.locale.confirmRemoveFile : Ext.String.format(RS.formbuilder.locale.confirmRemoveFiles, this.selections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: RS.formbuilder.locale.removeFile,
				no: RS.formbuilder.locale.cancel
			},
			scope: this,
			fn: this.removeSelectedFiles
		});
	},

	removeSelectedFiles: function(button) {
		if (button === 'yes') {
			var ids = [],
				fileIds = [];
			Ext.each(this.selections, function(selection) {
				ids.push(selection.get('sys_id'));
				fileIds.push(selection.get('id'));
			});
			if (this.name && this.name.length > 1) {
				Ext.Ajax.request({
					url: this.api ? this.api.delete : '/resolve/service/wikiadmin/attachment/delete', // Configurable
					params: {
						docSysId: '',
						docFullName: this.name,
						ids: ids
					},
					scope: this,
					success: function(resp) {
						var response = RS.common.parsePayload(resp);
						if (response.success) {
							this.loadFiles();
						} else {
							clientVM.displayError(RS.common.locale.FileUploadManager.deleteFailed);
						}
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}
	},

	downloadURL: function(url) {
		var urlparts = url.split('?');
		clientVM.getCSRFToken_ForURI(urlparts[0], function(data, uri) {
			var csrftoken = '?' + data[0] + '=' + data[1];
			var newUrl = uri + csrftoken + '&' + urlparts[1];
			var iframe = document.getElementById("hiddenDownloader");
			if (iframe === null) {
				iframe = document.createElement('iframe');
				iframe.id = "hiddenDownloader";
				iframe.style.display = 'none';
				document.body.appendChild(iframe);
			}
			iframe['src'] = newUrl.replace(/javascript\:/g, '');;
		});
	},

	reload: function() {
		this.loadFiles();
	},

	close: function() {
		this.doClose()
	}
})

RS.common.GridPickerVMConfig = {
	asWindowTitle: '',
	stateId: 'youneedtooverridethisnamespaceforyouowngrid',
	recordsSelections: [],
	storeConfig: null,
	displayName: '',
	store: null,
	columns: null,
	dumper: null,
	loader: null,
	width: 900,
	height: 500,
	showSysInfo: false,
	pageable: true,
	activate: function() {},
	init: function() {
		// console.log('common picker created')
		this.createStore();
		if (!this.loader)
			this.store.load();
		else
			this.loader();
	},

	createStore: function() {
		var store = Ext.create('Ext.data.Store', {
			fields: this.storeConfig.fields,
			proxy: this.storeConfig.proxy
		});
		this.set('store', store);
	},

	dump: function() {
		var flag = true;
		if (Ext.isFunction(this.dumper))
			flag = this.dumper(this.recordsSelections);
		else
			flag = this.dumper.dump.call(this.dumper.scope, this.recordsSelections);
		if (flag)
			this.doClose();
	},

	dumperText: 'dumperNeedToBeLocalized',

	cancel: function() {
		this.doClose();
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},

	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	}
};
glu.defModel('RS.common.GridPicker', Ext.applyIf({
	stateId: 'commongridpicker'
}, RS.common.GridPickerVMConfig));
glu.defModel('RS.common.AutoCompleteGridPicker', Ext.applyIf({
	stateId: 'commonautocompletegridpicker'
}, RS.common.GridPickerVMConfig));
glu.defModel('RS.common.KeywordGridPicker', {
	keyword: '',
	storeFields: [],
	searchFields: [],
	storeUrl: '',
	columns: null,
	width: 600,
	height: 500,
	title: '',
	dumperText: '',
	store: null,
	sorters: null,
	actions: [],
	buttons: {
		mtype: 'list'
	},
	showSysInfo: false,
	init: function() {
		var me = this;
		if (this.actions.length > 0)
			Ext.each(this.actions, function(action) {
				this.buttons.add({
					mtype: 'RS.common.GridButton',
					text: action.text,
					dumper: function() {
						action.action.apply(me);
					},
					cls : 'rs-med-btn rs-btn-dark'
				});
			}, this);
		else {
			this.buttons.add({
				mtype: 'RS.common.GridButton',
				text: this.dumperText,
				dumper: function() {
					me.dump();
					me.close();
				},
				cls : 'rs-med-btn rs-btn-dark'
			})
		}

		this.buttons.add({
			mtype: 'RS.common.GridButton',
			text: this.localize('close'),
			dumper: function() {
				me.doClose();
			},
			cls : 'rs-med-btn rs-btn-light'
		});
		this.set('store', Ext.create('Ext.data.Store', {
			fields: this.storeFields,
			remoteSort: true,
			sorters: this.sorters ? this.sorters : [{
				property: this.storeFields[0],
				direction: 'ASC'
			}],
			proxy: {
				type: 'ajax',
				url: this.storeUrl,
				baseParams : this.baseParams || {},
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		}));
		this.store.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {};
			var filter = [];
			if (this.keyword)
				Ext.each(this.searchFields, function(field) {
					filter.push({
						field: field,
						type: 'auto',
						condition: 'contains',
						value: this.keyword
					});
				}, this)
			Ext.apply(operation.params, {
				operator: 'or',
				filter: Ext.encode(filter)
			})
		}, this);
		this.store.load()
	},

	when_keyword_changes_update_grid: {
		on: ['keywordChanged'],
		action: function() {
			this.store.loadPage(1)
		}
	},

	recordsSelections: [],

	dumper: null,

	dump: function() {
		this.dumper(this.recordsSelections);
		this.close();
	},

	dumpIsVisible$: function() {
		return this.buttons.length == 0;
	},

	close: function() {
		this.doClose()
	}
});
glu.defModel('RS.common.GridButton', {
	text: 'button',
	dumper: function() {
		alert('test');
	},
	dump: function() {
		this.dumper();
	}
});



glu.defModel('RS.common.SysInfo', {

	sysId: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',

	createdOn$: function() {
		return this.getParsedDate(this.sysCreatedOn);
	},
	updatedOn$: function() {
		return this.getParsedDate(this.sysUpdatedOn);
	},

	userDateFormat: function() {
		return clientVM.getUserDefaultDateFormat();
	},

	getParsedDate: function(date, f) {
		var d = date,
			formats = ['time', 'c', this.userDateFormat()];

		if (!Ext.isDate(d)) {
			Ext.Array.each(formats, function(format) {
				d = Ext.Date.parse(d, format)
				return !Ext.isDate(d)
			})
		}

		if (Ext.isDate(d)) {
			if (f)
				return Ext.Date.format(d, f);
			return Ext.Date.format(d, this.userDateFormat());
		}

		return RS.common.locale.unknownDate
	}
});

glu.defModel('RS.common.UploadWordDoc', {
	mixins: ['FileUploadManager'],
	api: {
		upload: '/resolve/service/wiki/docx/uploadDocx',
		list: '',
	},

	intro: '',
	wait: false,

	allowedExtensions: ['doc', 'docx'],
	fineUploaderTemplateHidden: false,
	checkForDuplicates: true,
	allowMultipleFiles: false,

	uploaderAdditionalClass: 'uploadword',

	fileOnUploadCallback: function(manager, filename) {
		this.set('wait', true);
		this.uploader.setParams({
			wikiID: this.rootVM.id,
			wikiName: this.rootVM.name,
			userName: clientVM.username
		});
	},

	fileOnCompleteCallback: function(manager, filename, response, xhr) {
        this.set('wait', false);
		this.appendHtmlValueAfterUpload(response);
		this.doClose();
	},

	appendHtmlValueAfterUpload: function(response) {
        var htmlContents = response.data.docxContent.htmlContent;

        for (var i=0; i<htmlContents.length; i++) {
            this.editor.setValue(this.editor.getValue() + htmlContents[i]);
        }
    },
	
	editor: null,
	init: function() {
		this.set('wait', false);
		this.set('dragHereText', this.localize('dragHere'));
		this.set('intro', this.localize('uploadIntro'));
	},

	close: function() {
		this.doClose();
	}
})


glu.defModel('RS.common.WikiDocSearch', {
	store: {
		mtype: 'store',
		fields: ['ufullname', 'umodule', 'id',
			'sysOrganizationName', {
				name: 'sysCreatedOn',
				type: 'date',
				format: 'time',
				dateFormat: 'time'
			},
			'sysCreatedBy', {
				name: 'sysUpdatedOn',
				type: 'date',
				format: 'time',
				dateFormat: 'time'
			}, 'sysUpdatedBy'
		],
		remoteSort: true,
		sorters: [{
			property: 'ufullname',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/common/wiki/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	typesStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	type: 'All',
	when_type_changes_update_grid: {
		on: 'typeChanged',
		action: function() {
			this.store.load()
		}
	},

	typesSelection: null,

	columns: null,

	searchQuery: '',

	dumper: null,

	wikiSelections: [],

	wikiSelection: null,

	single: false,

	init: function() {
		this.typesStore.add([{
			name: 'All',
			value: 'All'
		}, {
			name: 'Documents',
			value: 'Document'
		}, {
			name: 'Runbook',
			value: 'Runbook'
		}, {
			name: 'Decision Tree',
			value: 'DecisionTree'
		}]);
		this.set('columns', [{
			header: '~~ufullname~~',
			text: '~~ufullname~~',
			dataIndex: 'ufullname',
			filterable: true,
			flex: 2
		}, {
			header: '~~sysCreatedOn~~',
			text: '~~sysCreatedOn~~',
			dataIndex: 'sysCreatedOn',
			filterable: true,
			width: 200,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}, {
			header: '~~sysCreatedBy~~',
			text: '~~sysCreatedBy~~',
			dataIndex: 'sysCreatedBy',
			filterable: true,
			width: 120
		}, {
			header: '~~sysUpdatedOn~~',
			text: '~~sysUpdatedOn~~',
			dataIndex: 'sysUpdatedOn',
			filterable: true,
			width: 200,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}, {
			header: '~~sysUpdatedBy~~',
			text: '~~sysUpdatedBy~~',
			dataIndex: 'sysUpdatedBy',
			filterable: true,
			width: 120
		}, {
			header: '~~sysOrganizationName~~',
			text: '~~sysOrganizationName~~',
			dataIndex: 'sysOrganizationName',
			hidden: true,
			filterable: false,
			width: 130
		}, {
			header: '~~sys_id~~',
			text: '~~sys_id~~',
			dataIndex: 'id',
			hidden: true,
			filterable: true,
			width: 300
		}]);
		this.store.on('beforeload', function(store, op) {
			op.params = op.params || {};
			op.params.type = this.type;
		}, this);
		this.store.load();
	},

	choose: function() {
		var param = null;
		var close = false;
		if (this.single)
			param = this.wikiSelection;
		else
			param = this.wikiSelections;
		if (!this.dumper)
			return;
		if (Ext.isFunction(this.dumper))
			close = this.dumper(param);
		else
			close = this.dumper.dump.call(this.dumper.scope, param);

		if (close)
			this.doClose();

	},

	cancel: function() {
		this.doClose();
	}
});
glu.defView('RS.common.AccessRights', {
	xtype: 'panel',
	layout: 'fit',
	items: [{		
		dockedItems: [{
			xtype: 'toolbar',			
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: ['showRoleList', 'removeAccessRight', {
				xtype: 'checkbox',
				hideLabel: true,
				name: 'defaultRoles',
				hidden: '@{!defaultRolesIsVisible}',
				boxLabel: '@{defaultRolesText}'
			}]
		}],
		items: [{
			xtype: 'grid',		
			autoScroll : true,
			minHeight : 300,
			cls : 'rs-grid-dark',
			displayName: '~~Roles~~',
			name: 'rights',
			store: '@{accessRights}',
			viewConfig: {
				markDirty: false
			},			
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}],
			columns: '@{columns}'
		}]
	}],

	asWindow: {
		title: '~~Roles~~',
		width: 800,
		height: 500,
		padding : 15,
		cls : 'rs-modal-popup',		
		buttons: [{
			name : 'dump',
			cls : 'rs-med-btn rs-btn-dark'
		},{
			name : 'cancel',
			cls : 'rs-med-btn rs-btn-light'
		}]
	}
});

glu.defView('RS.common.AccessRights', 'Search', {
	layout: 'fit',
	bodyPadding: '@{bodyPadding}',
	items: [{
		xtype: 'grid',
		displayName: '~~Roles~~',
		name: 'rights',
		store: '@{accessRights}',
		viewConfig: {
			markDirty: false
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: []
		}],
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			useWindowParams: false,
			hideMenu: true
		}, {
			ptype: 'cellediting',
			clicksToEdit: 1
		}, {
			ptype: 'pager'
		}],
		columns: '@{columns}'
	}],

	asWindow: {
		title: '~~Roles~~',
		width: 800,
		height: 500,
		buttonAlign: 'left',
		buttons: ['dump', 'cancel']
	}
});
glu.defView('RS.common.ContentPicker',{
	layout: 'border',
	padding : 15,		
	title: '@{title}',
	cls : 'rs-modal-popup',
	modal: true,
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}],
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockectoolbar',
		name: 'actionBar',
		items: []
	}],
	items: [{
		region: 'west',
		width: 200,
		maxWidth: 300,
		split : true,
		xtype: 'treepanel',
		cls : 'rs-grid-dark',
		columns: [{
			xtype: 'treecolumn',
			dataIndex: 'unamespace',
			text: 'Namespace',
			flex: 1
		}],
		plugins: [{
	        ptype: 'bufferedrenderer',
	        trailingBufferZone: 20,  // Keep 20 rows rendered in the table behind scroll
	        leadingBufferZone: 50   // Keep 50 rows rendered in the table ahead of scroll
	    }],
		store: '@{moduleStore}',
		name : 'module',
		listeners: {
			selectionchange: '@{menuPathTreeSelectionchange}'
		},
		margin : {
			bottom : 10
		}
	}, {
		region: 'center',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		displayName: '@{title}',
		name: 'content',
		store : '@{contentStore}',
		columns: '@{columns}',
		multiSelect: false,
		selected: '@{selected}',
		autoScroll: true,
		viewConfig: {
			enableTextSelection: true
		},
		plugins: [{
			ptype: 'resolveexpander',
			pluginId: 'rowExpander',
			rowBodyTpl: new Ext.XTemplate('<div resolveId="resolveRowBody" style="color:#333"><span style="font-weight:bold">Summary:</span><br/><br/>&nbsp&nbsp&nbsp&nbsp{usummary:htmlEncode}</div>')
		}],		
		selModel: {
			selType: 'resolvecheckboxmodel',		
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'ufullname'
		},
		listeners: {
			editAction: '@{editContent}'
		},
		margin : {
			bottom : 10
		}
	}],
	buttons: [{
		name : 'dump',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}],	
	listeners: {
		beforerender: function() {
			clientVM.setPopupSizeToEightyPercent(this);
		}
	}
});
Ext.define('RS.common.DocumentField', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'documentfield',
    enableKeyEvents: true,
    trigger2Cls: 'x-form-search-trigger',
    queryMode: 'remote',
    typeAhead: false,
    minChars: 0,
    queryDelay: 500,
    pageSize: 20,
    ignoreSetValueOnLoad: true, // To prevent chars typed being deleted
    listConfig: {
        loadMask: false
    },

    onTrigger2Click: function() {
        this.fireEvent('searchdocument', this, this, this.inline);
    },

    initComponent: function() {
        this.callParent(arguments);
        this.store.on('beforeload', function(store, op) {
            op.params = op.params || {};
            var curFilter = op.params.filter || '[]';
            if (typeof(curFilter) == 'string') {
                curFilter = Ext.decode(curFilter);
            }
            Ext.apply(op.params, {
                filter: Ext.encode([{
                    field: this.displayField,
                    condition: 'contains',
                    type: 'auto',
                    value: op.params.query
                }].concat(curFilter))
            });
        }, this);
    }
});
glu.defView('RS.common.UploadFile', {
	parentLayout: 'FileUploadManager',
})

glu.defView('RS.common.dummy') //to make sure the ns is init before creating the factory
RS.common.views.FileUploadManagerFactory = function(actualView) {
	var output = {
		asWindow: {
			title: '@{title}',
			cls : 'rs-modal-popup',		
			width: 800,
			modal: true,
			padding : 15,
			closable: true
		},
	
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		
		dockedItems: [{
			xtype: 'toolbar',			
			cls: 'rs-dockedtoolbar',
			defaults : {
				cls: 'rs-small-btn rs-btn-light'
			},
			items: [{
				xtype: 'button',
				name: 'uploadFile',
				text: 'Upload',
				preventDefault: false,
				hidden: '@{!allowUpload}',
				id: 'upload_button'
			}, {
				name: 'delete',
				text: 'Delete',
				hidden: '@{!allowRemove}',
	
			}, '->', {
				name: 'reload',
				tooltip: 'Reload',
				text: '',
				iconCls: 'x-tbar-loading',
			}]
		}],
		items: [{
			itemId: 'fineUploaderTemplate',
			html: '@{fineUploaderTemplate}',
			height: '@{fineUploaderTemplateHeight}',
			hidden: '@{fineUploaderTemplateHidden}',
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			id: 'attachments_grid',
			store: '@{filesStore}',
			viewConfig: {
				markDirty: false
			},
			columns: '@{filesColumns}',
			multiSelect: '@{multiSelect}',
			margin: '6 0 0 0',
			flex: 1,
			listeners: {
				selectionChange: '@{selectionChanged}'
			}
		}],
		buttons: [{
			text: '~~close~~',
			itemId:  'cancelBnt',
			handler: '@{close}',
			cls : 'rs-med-btn rs-btn-light'
		}]
	}

	if (actualView && actualView.propertyPanel) {
		Ext.apply(output, actualView.propertyPanel);
	}
	return output;
}


glu.defView('RS.common.GridPicker', function(arg) {
	var vm = arg.vm;
	return {
		layout: 'fit',	
		items: [{
			xtype: 'grid',
			stateId: '@{stateId}',
			cls :'rs-grid-dark',
			stateful: true,
			layout: 'fit',
			displayName: '@{displayName}',
			name: 'records',
			store: '@{store}',
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar',
				name: 'actionBar',
				items: []
			}],
			columns: '@{columns}',
			plugins: [{
				ptype: 'searchfilter',
				allowPersistFilter: false,
				hideMenu: true
			}, {
				ptype: 'pager',
				pageable: vm.pageable,
				showSysInfo: vm.showSysInfo
			}],
			listeners: {
				selectAllAcrossPages: '@{selectAllAcrossPages}',
				selectionChange: '@{selectionChanged}'
			},
			margin : {
				bottom : 8
			}
		}],

		asWindow: {
			title: '@{asWindowTitle}',
			modal: true,
			padding : 15,
			cls : 'rs-modal-popup',
			width: '@{width}',
			height: '@{height}',			
			buttons: [{
				text: '@{dumperText}',
				name: 'dump',
				cls : 'rs-med-btn rs-btn-dark'
			}, {
				name : 'cancel',
				cls : 'rs-med-btn rs-btn-light'
			}]
		}
	}
});

glu.defView('RS.common.AutoCompleteGridPicker', function(arg) {
	var vm = arg.vm;
	return {
		layout: 'fit',
		bodyPadding: '10px',
		items: [{
			xtype: 'grid',
			stateId: '@{stateId}',
			stateful: true,
			layout: 'fit',
			displayName: '@{displayName}',
			name: 'records',
			store: '@{store}',
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar',
				name: 'actionBar',
				items: [{
					xtype: 'textfield',
					name: 'query'
				}]
			}],
			columns: '@{columns}',
			plugins: [{
				ptype: 'pager',
				pageable: vm.pageable,
				showSysInfo: vm.showSysInfo
			}],
			listeners: {
				selectAllAcrossPages: '@{selectAllAcrossPages}',
				selectionChange: '@{selectionChanged}'
			}
		}],

		asWindow: {
			title: '@{asWindowTitle}',
			modal: true,
			width: '@{width}',
			height: '@{height}',
			buttonAlign: 'left',
			buttons: [{
				text: '@{dumperText}',
				name: 'dump'
			}, 'cancel']
		}
	}
});
glu.defView('RS.common.GridButton', {
	xtype: 'button',
	text: '@{text}',
	handler: '@{dump}',
	cls : '@{cls}'
});
glu.defView('RS.common.KeywordGridPicker', function(args) {
	var vm = args.vm;
	return {
		title: '@{title}',
		width: '@{width}',
		height: '@{height}',
		modal: true,
		cls : 'rs-modal-popup',
		padding : 15,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},

		items: [{
			flex: 1,
			xtype: 'grid',
			margin : '0 0 8 0',
			cls : 'rs-grid-dark',
			plugins: [{
				ptype: 'pager',
				allowAutoRefresh: false,
				showSysInfo: vm.showSysInfo
			}],
			dockedItems: [{
				xtype: 'toolbar',				
				cls: 'actionBar rs-dockedtoolbar',
				name: 'actionBar',
				items: [{
					xtype: 'textfield',
					name: 'keyword'
				}]
			}],
			name: 'records',
			store: '@{store}',
			columns: '@{columns}'
		}],			
		buttons: '@{buttons}',
		listeners: {
			resize: function() {
				this.center()
			}
		}
	}
});
glu.defView('RS.common.SysInfo', {
	asWindow: {
		height: 290,
		width: 500,
		modal: true,
		title: '~~sysInfoButtonTitle~~'
	},
	autoScroll: true,
	buttonAlign: 'left',
	buttons: ['close'],

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		bodyPadding: '0 10 10 10',
		padding: '10px 0 0 0'
	},
	items: [{
		xtype: 'panel',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaultType: 'displayfield',
		defaults: {
			labelWidth: 130
		},
		items: [
			'sysId', 
			'createdOn', 
			'sysCreatedBy', 
			'updatedOn', 
			'sysUpdatedBy'
		]
	}]
});

glu.defView('RS.common.UploadWordDoc', {
	asWindow: {
		title: '~~uploadWordDocument~~',
		cls : 'rs-modal-popup',
		height: 400,
		width: 800,
		modal: true,
		padding : 15,
		closable: true
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	
	items: [{
			html: '@{intro}',
			margin: '0 0 10 0'
	}, {
		itemId: 'fineUploaderTemplate',
		html: '@{fineUploaderTemplate}',
		height: '@{fineUploaderTemplateHeight}',
		hidden: '@{fineUploaderTemplateHidden}',
	}, {
		id: 'attachments_grid',
		hidden: true
	}],
	buttons: [{
		text: '~~browse~~',
		id: 'upload_button',
        cls : 'rs-med-btn rs-btn-dark',
		preventDefault: false,
    }, {
        name: 'close',
        cls : 'rs-med-btn rs-btn-light'
	}]
})

glu.defView('RS.common.WikiDocSearch', 'Single', {	
	layout: 'fit',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		padding: '10px 0px 0px 10px',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'panel',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'combobox',
				editable: false,
				store: '@{typesStore}',
				displayField: 'name',
				valueField: 'value',
				queryMode: 'local',
				name: 'type',
				width: 220,
				labelWidth: 100
			}, {
				xtype: 'combobox',
				itemId: 'filter',
				emptyText: '~~filter~~',
				inputCls: 'post-message',
				filterStore: '@{store}',
				displayField: 'name',
				valueField: 'value',
				width: 510,
				margins: {
					left: 5
				},
				getFilterBar: function() {
					return this.ownerCt.ownerCt.ownerCt.down('#filterBar');
				},
				columns: '@{columns}',
				plugins: [{
					ptype: 'searchfieldfilter',
					filterBarItemId: 'filterBar'
				}],
				listeners: {
					beforerender: function(combo) {
						Ext.Array.forEach(combo.columns, function(column) {
							switch (column.text) {
								case '~~ufullname~~':
									column.text = RS.common.locale.ufullname;
									break;
								case '~~sysCreatedOn~~':
									column.text = RS.common.locale.sysCreatedOn;
									break;
								case '~~sysCreatedBy~~':
									column.text = RS.common.locale.sysCreatedBy;
									break;
								case '~~sysUpdatedOn~~':
									column.text = RS.common.locale.sysUpdatedOn;
									break;
								case '~~sysUpdatedBy~~':
									column.text = RS.common.locale.sysUpdatedBy;
									break;
								case '~~sys_id~~':
									column.text = RS.common.locale.sys_id;
									break;
								case '~~sysOrganizationName~~':
									column.text = RS.common.locale.sysOrganizationName;
									break;
							}
						})
					}
				}
			}]
		}]
	}, {
		xtype: 'filterbar',
		itemId: 'filterBar',
		allowPersistFilter: false,
		listeners: {
			add: {
				fn: function() {
					this.ownerCt.down('#filter').getPlugin().filterChanged()
				},
				buffer: 10
			},
			remove: {
				fn: function() {
					this.ownerCt.down('#filter').getPlugin().filterChanged()
				},
				buffer: 10
			},
			clearFilter: function() {
				this.ownerCt.down('#filter').clearValue()
			}
		}
	}],
	items: [{
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: []
		}],
		// name:'wiki',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		margin : '0 0 8 0',
		layout: 'fit',
		store: '@{store}',
		columns: '@{columns}',
		selected: '@{wikiSelection}',
		plugins: [{
			ptype: 'pager'
		}, {
			ptype: 'columnautowidth'
		}]
	}],
	buttons: [{
		name : 'choose',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}],
	asWindow: {
		title: '~~title~~',
		cls : 'rs-modal-popup',
		padding : 15,
		modal: true,
		listeners: {
			beforeshow: function() {
				clientVM.setPopupSizeToEightyPercent(this);
			}
		}
	}
});


glu.defView('RS.common.WikiDocSearch', {
	bodyPadding: '10px',
	layout: 'fit',

	buttonAlign: 'left',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		padding: '10px 0px 0px 10px',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'panel',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'combobox',
				editable: false,
				store: '@{typesStore}',
				displayField: 'name',
				valueField: 'value',
				queryMode: 'local',
				name: 'type',
				width: 220,
				labelWidth: 100
			}, {
				xtype: 'combobox',
				itemId: 'filter',
				emptyText: '~~filter~~',
				inputCls: 'post-message',
				filterStore: '@{store}',
				displayField: 'name',
				valueField: 'value',
				width: 510,
				margins: {
					left: 5
				},
				getFilterBar: function() {
					return this.ownerCt.ownerCt.ownerCt.down('#filterBar');
				},
				columns: '@{columns}',
				plugins: [{
					ptype: 'searchfieldfilter',
					filterBarItemId: 'filterBar'
				}],
				listeners: {
					beforerender: function(combo) {
						Ext.Array.forEach(combo.columns, function(column) {
							switch (column.text) {
								case '~~ufullname~~':
									column.text = RS.common.locale.ufullname;
									break;
								case '~~sysCreatedOn~~':
									column.text = RS.common.locale.sysCreatedOn;
									break;
								case '~~sysCreatedBy~~':
									column.text = RS.common.locale.sysCreatedBy;
									break;
								case '~~sysUpdatedOn~~':
									column.text = RS.common.locale.sysUpdatedOn;
									break;
								case '~~sysUpdatedBy~~':
									column.text = RS.common.locale.sysUpdatedBy;
									break;
								case '~~sys_id~~':
									column.text = RS.common.locale.sys_id;
									break;
								case '~~sysOrganizationName~~':
									column.text = RS.common.locale.sysOrganizationName;
									break;
							}
						})
					}
				}
			}]
		}]
	}, {
		xtype: 'filterbar',
		itemId: 'filterBar',
		allowPersistFilter: false,
		listeners: {
			add: {
				fn: function() {
					this.ownerCt.down('#filter').getPlugin().filterChanged()
				},
				buffer: 10
			},
			remove: {
				fn: function() {
					this.ownerCt.down('#filter').getPlugin().filterChanged()
				},
				buffer: 10
			},
			clearFilter: function() {
				this.ownerCt.down('#filter').clearValue()
			}
		}
	}],
	items: [{
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: []
		}],
		name: 'wiki',
		selType: 'checkboxmodel',
		xtype: 'grid',
		layout: 'fit',
		store: '@{store}',
		columns: '@{columns}',
		plugins: [{
			ptype: 'pager'
		}, {
			ptype: 'columnautowidth'
		}]
	}],

	buttons: ['choose', 'cancel'],
	asWindow: {
		title: '~~title~~',
		modal: true,
		listeners: {
			beforeshow: function() {
				clientVM.setPopupSizeToEightyPercent(this);
			}
		}
	}
});
Ext.namespace('RS.common', 'RS.common.blocks');
RS.common.blocks.Block = function (blockContainer, blockName) {
	this.blockModels = {};
	this.blockModelNames = [];
	this.dataIndexes = [];
	this.currentModelName = '';
	this.nextModelName = '';
	this.initialLoad = true;
	this.isDirty = false;
	this.isValid = true;

	this.getBlockContainer = function () { 
		return blockContainer; 
	};

	this.getBlockModels = function () { 
		return this.blockModels; 
	};

	this.getBlockModelNames = function () { 
		return this.blockModelNames; 
	};

	this.getName = function () { 
		return blockName; 
	};
		
	this.getCurrentModelName = function () {
		if (this.currentModelName === '') {
			this.currentModelName = this.blockModelNames[0];
		}

		return this.currentModelName; 
	};
	
	this.setCurrentModelName = function (current) { 
		this.currentModelName = current; 
	};
	
	this.addDataIndex = function (i) {
		if (this.dataIndexes.indexOf(i) === -1) {
			this.dataIndexes.push(i);
		}
	};

	this.getDataIndexes = function () {
		return this.dataIndexes;
	};

	this.getInitialLoad = function () {
		return this.initialLoad;
	};

	this.setInitialLoad = function (value) {
		this.initialLoad = value;
	};

	this.setIsDirty = function(dirty){
		this.isDirty = dirty;
	};

	this.getIsDirty = function(){
		return this.isDirty;
	}

	this.setIsValid = function(valid){
		this.isValid = valid;
	};

	this.getIsValid = function(){
		return this.isValid;
	}
};

RS.common.blocks.Block.prototype = (function () {
	function getLeaf(node, paths) {
		var leaf = null;

		for (var i = 0; i < paths.length; i++) {
			leaf = node = node[paths[i]];
		}

		return leaf;
	}

	function mergeArrays(original, modified, filter, buildArray) {
		var newEntries = [];

		for (var i = modified.length - 1; i >= 0; i--) {
			if (buildArray || !modified[i].id) {
				newEntries.unshift(modified[i]);
				modified.splice(i, 1);
			}
		}

		if (typeof filter === 'function') {
			for (var i = original.length - 1; i >= 0; i--) {
				var o = original[i];

				if (filter(o)) {
					var modifiedIndex = indexByID(modified, o.id);

					if (modifiedIndex === -1) {
						original.splice(i, 1);
					} else {
						copyProperties(o, modified[modifiedIndex]);
					}
				} 
			}
		} else {
			for (var i = original.length - 1; i >= 0; i--) {
				var modifiedIndex = indexByID(modified, original[i].id);
				
				if (modifiedIndex === -1) {
					original.splice(i, 1);
				} else {
					copyProperties(original[i], modified[modifiedIndex]);
				}
			}
		}

		original.push.apply(original, newEntries);
	}

	function indexByID(original, id) {
		var found = -1;
		var i = 0;

	    while (found < 0 && i < original.length) {
	    	if (original[i].id === id) {
	    		found = i;
	    	}

    		i++;
		}

		return found;
	}

	function copyProperties(node, leaf) {
		for (key in leaf) {
			if (leaf.hasOwnProperty(key)) {
				node[key] = leaf[key];
			}
		}
	}

	return {
		addModel: function (context, title, gluViewmodelNamespace, modelFn) {
			var blockModelNames = this.getBlockModelNames(),
				blockModels = this.getBlockModels(),
				blockContainer = this.getBlockContainer(),
				parts = gluViewmodelNamespace.split('.'),			
				modelName = parts[parts.length - 1];

			blockModelNames.push(modelName);
			var blockModel = new modelFn(this, modelName, blockModelNames.length - 1);
			glu.defModel(gluViewmodelNamespace, blockModel.modelConfig);
			blockModels[modelName] = blockModel;
			blockModel.suspendDirtyNotification = true;
			blockModel.gluModel = context.model(gluViewmodelNamespace);

			if (typeof blockModel.gluModel.postInit === 'function') {
				blockModel.gluModel.postInit(blockModel, context);
			}

			blockModel.suspendDirtyNotification = false;				
			blockModel.setViewTitle(context.localize(title));
			return blockModel;
		},

		addDataSource: function (model) {
			this.addDataIndex(model.getModelIndex());
		},

		showView: function (modelName) {
			var blockModels = this.getBlockModels();			
			var d = blockModels[modelName];
			var currentModelName = this.getCurrentModelName();

			if (currentModelName.length > 0 && currentModelName !== modelName) {
				var current = blockModels[currentModelName];
				current.hide();
			}

			this.setCurrentModelName(modelName);
			this.show();
			return this;
		},

		close: function (sourceBlockModelName, nextBlockModelName) {
            this.setCurrentModelName(nextBlockModelName);
			var blockContainer = this.getBlockContainer(),
				blockModels = this.getBlockModels(),
				blockModelName = this.getName(),
				result = this.getDataForServer(),
				data = {};

			for (var i = 0; i < result.paths.length; i++) {
				var tuple = this.getPathAndFilter(result.paths[i]);
				this.deepUpdate(data, result.leaves[i], tuple.path.split('.'), tuple.filter, true);
			}				

			if (nextBlockModelName !== sourceBlockModelName) {
				var d = blockModels[nextBlockModelName],
					dataPaths = d.getDataPaths(),
					leaves = [];

				if (dataPaths.length > 0) {
					for (var i = 0; i < dataPaths.length; i++) {
						leaves.push(this.getFilteredLeaf(data, dataPaths[i]));
					}

					d.suspendDirtyNotification = true;
		            d.load.apply(d, leaves);
		            d.suspendDirtyNotification = false;
				}
			}
			
			var blockModelData = blockModels[sourceBlockModelName].getData();
			blockContainer.checkCallbacksOnLoadOrClose(blockModelName, sourceBlockModelName, blockModelData);
			blockContainer.checkCallbacksOnClose(blockModelName, sourceBlockModelName, blockModelData);

            if (nextBlockModelName) {                
                this.showView(nextBlockModelName);
            }

            return this;
		},

		loadOriginalData: function (originalData, originalDirtyFlag) {
			var blockModels = this.getBlockModels(),				
				blockModelNames = this.getBlockModelNames(),
				dataIndexes = this.getDataIndexes();

			for (var i = 0; i < dataIndexes.length; i++) {
				blockModels[blockModelNames[dataIndexes[i]]].setOriginalDataString(originalData[i]);
			}

			if (originalDirtyFlag) {
				for (var i = 0; i < dataIndexes.length; i++) {			
					blockModels[blockModelNames[dataIndexes[i]]].notifyDirty();
				}
			}

			return this;
		},

		getOriginalData: function () {
			var blockModels = this.getBlockModels(),				
				blockModelNames = this.getBlockModelNames(),
				dataIndexes = this.getDataIndexes(),
				originalData = [];

			for (var i = 0; i < dataIndexes.length; i++) {			
				originalData.push(blockModels[blockModelNames[dataIndexes[i]]].getOriginalData());
			}

			return originalData;
		},

		load: function (payload) {
			var blockContainer = this.getBlockContainer(),
				blockModelName = this.getName(),
				blockModels = this.getBlockModels(),				
				blockModelNames = this.getBlockModelNames(),
				dataIndexes = this.getDataIndexes();

			for (var i = 0; i < blockModelNames.length; i++) {
				var d = blockModels[blockModelNames[i]],
					leaves = [],
					dataPaths = d.getDataPaths(),
					strategy;

				if(dataIndexes.indexOf(i) >= 0) {
					strategy = this.loadEditSection;
				} else {
					strategy = this.loadReadSection;
				}

				if (dataPaths.length > 0) {
					for (var j = 0; j < dataPaths.length; j++) {
						leaves.push(this.getFilteredLeaf(payload, dataPaths[j]));
					}

					strategy(d, leaves);

					if (!this.getInitialLoad()) {
						blockContainer.checkCallbacksOnLoadOrClose(blockModelName, blockModelNames[i], leaves);
					} else {
						this.setInitialLoad(false);
					}
				}
			}

			return this;		
		},

		loadEditSection: function (d, leaves) {
			d.beforeLoad();
			d.load.apply(d, leaves);
			d.afterLoad();
		},

		loadReadSection: function (d, leaves) {
			d.load.apply(d, leaves);
		},

		getFilteredLeaf: function (root, dataPath) {
			var tuple = this.getPathAndFilter(dataPath),
				leaf = getLeaf(root, tuple.path.split('.'));

			if (typeof tuple.filter === 'function' && leaf && leaf.constructor === Array) {
				leaf = leaf.filter(tuple.filter);
			}

			return leaf;
		},

		saveComplete: function () {
			var blockModels = this.getBlockModels(),
				blockModelNames = this.getBlockModelNames(),
				dataIndexes = this.getDataIndexes();			

			for (var i = 0; i < dataIndexes.length; i++) {
				var d = blockModels[blockModelNames[dataIndexes[i]]];
				var dataIsForServer = true;
				d.setOriginalData(d.getData(dataIsForServer));
				d.notifyDirty();
			}

			return this;
		},

		reportDataModificationForAllBlockModels: function (blockContainer, clear) {
			// to be called after a load or reload from the blockContainer
			var dataIndexes = this.getDataIndexes(),
				blockModelNames = this.getBlockModelNames(),
				blockModels = this.getBlockModels(),
				blockName = this.getName();

			for (var i = 0; i < dataIndexes.length; i++) {
				var dataIndex = dataIndexes[i];

				if (dataIndex < blockModelNames.length) {
					var blockModel = blockModels[blockModelNames[dataIndex]],
						sourceBlockModelName = blockModel.getName(),
						data = blockModel.getData();
					blockContainer.checkDataDependencies('onLoadOrClose', blockName, sourceBlockModelName, data);
				}
			}
			
			if (clear) {
				for (var i = 0; i < blockModelNames.length; i++) {
					var d = blockModels[blockModelNames[i]];
					d.isDirty = false;
					d.isValid = true;
					d.notifyDirty();
					d.notifyValid(true);
				}
			}

			return this;
		},

		reportDataModification: function (blockModel) {
			this.getBlockContainer()
				.checkDataDependencies(
					'onChange',
					this.getName(), 
					blockModel.getName(), 
					blockModel.getData());
		},

		checkDataDependencies: function (triggerMethod, data, sourceBlockModelName) {
			if (!data) {
				var blockModels = this.getBlockModels();
				data = blockModels[currentModelName].getData(false);
			}

			var blockName = this.getName();
			this.getBlockContainer().checkDataDependencies(triggerMethod, blockName, sourceBlockModelName, data);
		},

		modifyByNotification: function (blockModelName, sourceData, sourceBlockModelName) {
			var blockModels = this.getBlockModels(),
				blockModel = blockModels[blockModelName];
			sourceData.push(sourceBlockModelName);
			blockModel.modifyByNotification.apply(blockModel, sourceData);
			return this;
		},

		notifyDirty: function () {
			var dataIndexes = this.getDataIndexes(),
				dirty = false;
			
			if (dataIndexes.length > 0) {	
				var blockModels = this.getBlockModels(),	
					blockModelNames = this.getBlockModelNames();

				for (var i = 0; i < dataIndexes.length; i++) {
					var dataIndex = dataIndexes[i];

					if (dataIndex < blockModelNames.length) {
						var d = blockModels[blockModelNames[dataIndex]];
						
						if (d.isDirty) {
							dirty = true;
							//Reflect current section dirty
							blockModels[blockModelNames[i]].updateTitle(this.isValid, dirty);
							break;
						}
					}
				}				
				//Reflect dirty of whole section on the 1st BlockModel (which is the view).
				blockModels[blockModelNames[0]].updateTitle(this.isValid, dirty);
				//Set this block to dirty.
				this.setIsDirty(dirty);
			}
		
			this.getBlockContainer().notifyDirty();
			return this;
		},

		notifyValid : function(){
			var dataIndexes = this.getDataIndexes(),
				valid = true;
			
			if (dataIndexes.length > 0) {	
				var blockModels = this.getBlockModels(),	
					blockModelNames = this.getBlockModelNames();

				for (var i = 0; i < dataIndexes.length; i++) {
					var dataIndex = dataIndexes[i];

					if (dataIndex < blockModelNames.length) {
						var d = blockModels[blockModelNames[dataIndex]];
						
						if (!d.isValid) {
							valid = false;
							break;
						}
					}
				}		
				
				//Set this block to valid.
				this.setIsValid(valid);
			}
		
			this.getBlockContainer().notifyValid();
			return this;
		},

		notifyDataChange : function(blockModelName, data){
			var blockName = this.getName();			
			this.getBlockContainer().notifyDataChange(blockName, blockModelName, data);
		},
		hide: function () {
			var blockModels = this.getBlockModels(),
				blockModelNames = this.getBlockModelNames();				

			for (var i = 0; i < blockModelNames.length; i++) {
				blockModels[blockModelNames[i]].hide();
			}

			return this;
		},

		show: function () {
			var blockModels = this.getBlockModels(),
				currentModelName = this.getCurrentModelName(),
				d = blockModels[currentModelName];

			d.show();

			if (d.isCollapsible()) {
				var blockModelNames = this.getBlockModelNames(),
					index = blockModelNames.indexOf(currentModelName),
					dataIndexes = this.getDataIndexes();

				if (dataIndexes.indexOf(index) === -1) {
					d.collapse();
				}
			}

			return this;
		},

		hideBlocks: function () {
			var blockNames = arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments),
				blockContainer = this.getBlockContainer();
			blockContainer.hideBlocks(blockNames);
			return this;
		},

		showBlocks: function () {
			var blockNames = arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments),
				blockContainer = this.getBlockContainer();
			blockContainer.showBlocks(blockNames);
			return this;
		},

		hideEditLink: function () {
			var blockModelNames = this.getBlockModelNames(),
				blockModels = this.getBlockModels();
			blockModels[blockModelNames[0]].hideEditLink();
			return this;
		},

		showEditLink: function () {
			var blockModelNames = this.getBlockModelNames(),
				blockModels = this.getBlockModels();
			blockModels[blockModelNames[0]].showEditLink();
			return this;
		},

		getData: function () {
			var dataIndexes = this.getDataIndexes(),
				data = {};
			
			if (dataIndexes.length > 0) {	
				var blockModels = this.getBlockModels(),	
					blockModelNames = this.getBlockModelNames(),
					leaves = [];

				for (var i = 0; i < dataIndexes.length; i++) {
					var dataIndex = dataIndexes[i];

					if (dataIndex < blockModelNames.length) {
						var d = blockModels[blockModelNames[dataIndex]];
						var dataPaths = d.getDataPaths();
						var blockModelData = d.getData(false);
						this.mergeData(data, dataPaths, blockModelData);	
					}
				}
			}

			return data;
		},

		getDataForServer: function () {
			var dataIndexes = this.getDataIndexes(),
				result = {
					paths: [],
					leaves: []
				};
			
			if (dataIndexes.length > 0) {	
				var blockModels = this.getBlockModels(),	
					blockModelNames = this.getBlockModelNames(),
					leaves = [];

				for (var i = 0; i < dataIndexes.length; i++) {
					var dataIndex = dataIndexes[i];

					if (dataIndex < blockModelNames.length) {
						var d = blockModels[blockModelNames[dataIndex]];
						result.paths.push.apply(result.paths, d.getDataPaths());
						result.leaves.push.apply(result.leaves, d.getData(true));
					}
				}
			}

			return result;
		},			

		mergeData: function (node, dataPaths, leaves) {
			if (dataPaths.length !== leaves.length) {
				throw new Error('dataPaths.length does not match leaves.length');
			}

			for (var i = 0; i < dataPaths.length; i++) {
				var modified = leaves[i] || {};
				var tuple = this.getPathAndFilter(dataPaths[i]);	
				this.deepUpdate(node, modified, tuple.path.split('.'), tuple.filter);
			}
		},

		getPathAndFilter: function (dataPath) {
			var result = {
				path: dataPath,
				filter: null
			};

			if (typeof dataPath == 'object' && Object.keys(dataPath).length === 2 && dataPath.hasOwnProperty('path') && dataPath.hasOwnProperty('filter')) {
				result.path = dataPath.path;

				if (typeof dataPath.filter === 'function') {
					result.filter = dataPath.filter;
				}
			}

			return result;
		},

		deepUpdate: function (node, leaf, paths, filter, buildArray) {
			var name = paths[0];

			if (paths.length > 1) {
		 		paths.splice(0, 1);

		 		if (typeof node[name] === 'undefined') {
		 			// create missing branch
		 			node[name] = {};
		 		}

				this.deepUpdate(node[name], leaf, paths, filter, buildArray);
			} else {
		 		if (leaf && leaf.constructor === Array) {
		 			if (typeof node[name] === 'undefined') {
		 				node[name] = [];
		 			}

					mergeArrays(node[name], leaf, filter, buildArray);
		 		} else {
			 		if (leaf !== null || typeof leaf !== 'undefined') {
			 			if (typeof node[name] === 'undefined') {
			 				node[name] = {};
			 			}

			 			copyProperties(node[name], leaf);
			 		} else {
			 			node[name] = null;
			 		}
		 		}	 		
	 		}
		}
	};
})();

RS.common.blocks.BlockContainer = function () {
	this.blocks = {};
	this.blockNames = [];
	this.dataDependencies = {};
	this.dataInternalDependencies = {};
	this.callbackDependencies = {};
	this.dirtySubscribers = [];
	this.validSubcribers = [];	
	this.payload = {};
	this.initialLoad = true;

	this.getBlocks = function () { 
		return this.blocks; 
	};

	this.getBlockNames = function () { 
		return this.blockNames; 
	};

	this.getDataDependencies = function (sourceBlockName, sourceBlockModelName) {
		var dependency,
			sourceBlockHasDependency = typeof this.dataDependencies[sourceBlockName] !== 'undefined';

		if (sourceBlockHasDependency) {
			var sourceModelHasDependency = typeof this.dataDependencies[sourceBlockName][sourceBlockModelName] !== 'undefined';

			if (sourceModelHasDependency) {
				dependency = this.dataDependencies[sourceBlockName][sourceBlockModelName];
			}
		}

		return dependency;
	};

	this.addDataDependency = function (sourceBlockName, sourceBlockModelName, dependentBlockName, dependentBlockModelName) { 
		if (typeof this.dataDependencies[sourceBlockName] === 'undefined') {
			this.dataDependencies[sourceBlockName] = {};
		}

		if (typeof this.dataDependencies[sourceBlockName][sourceBlockModelName] === 'undefined') {
			this.dataDependencies[sourceBlockName][sourceBlockModelName] = [];
		}

		var found = false,
			i = 0,
			d = this.dataDependencies[sourceBlockName][sourceBlockModelName];

		while (!found && i < d.length) {
			found = d[i].blockName === dependentBlockName && d[i].blockModelName === dependentBlockModelName;
			i++;
		}

		if (!found) {
			d.push({
				blockName: dependentBlockName,
				blockModelName: dependentBlockModelName
			});
		}
	};

	this.addInternalDataDependency = function (sourceBlockName, sourceBlockModelName, dependentBlockName, dependentBlockModelName) { 
		if (typeof this.dataInternalDependencies[sourceBlockName] === 'undefined') {
			this.dataInternalDependencies[sourceBlockName] = {};
		}

		if (typeof this.dataInternalDependencies[sourceBlockName][sourceBlockModelName] === 'undefined') {
			this.dataInternalDependencies[sourceBlockName][sourceBlockModelName] = [];
		}

		var found = false,
			i = 0,
			d = this.dataInternalDependencies[sourceBlockName][sourceBlockModelName];

		while (!found && i < d.length) {
			found = d[i].blockName === dependentBlockName && d[i].blockModelName === dependentBlockModelName;
			i++;
		}

		if (!found) {
			d.push({
				blockName: dependentBlockName,
				blockModelName: dependentBlockModelName
			});
		}
	};

	this.addCallback = function (triggerMethod, sourceBlockName, sourceBlockModelName, callback) {
		if (typeof this.callbackDependencies[sourceBlockName] === 'undefined') {
			this.callbackDependencies[sourceBlockName] = {};
		}	

		if (typeof this.callbackDependencies[sourceBlockName][sourceBlockModelName] === 'undefined') {
			this.callbackDependencies[sourceBlockName][sourceBlockModelName] = {
				onChange: [],
				onLoadOrClose: [],
				onClose: []
			};
		}

		var found = false, 
			i = 0, 
			serializedCallback = callback.toString(),
			d = this.callbackDependencies[sourceBlockName][sourceBlockModelName][triggerMethod];

		while (!found && i < d.length) {
			found = d[i].toString() === serializedCallback;
			i++;
		}

		var id = null;

		if (!found) {
			id = new Date().getTime();
			callback.uniqueCallbackID = id;
			d.push(callback);
		}

		return id;
	};

	this.spliceCallback = function (triggerMethod, sourceBlockName, sourceBlockModelName, uniqueID) {
		if (this.callbackDependencies[sourceBlockName] && 
			this.callbackDependencies[sourceBlockName][sourceBlockModelName] &&
			this.callbackDependencies[sourceBlockName][sourceBlockModelName][triggerMethod]) {

			var i = 0, 
				d = this.callbackDependencies[sourceBlockName][sourceBlockModelName][triggerMethod];

			while (i < d.length) {
				if (d[i].uniqueCallbackID === uniqueID)	{
					d.splice(i, 1);
					break;
				}

				i++
			}
		}			
	};

	this.getCallbacks = function (triggerMethod, sourceBlockName, sourceBlockModelName) {
		var callbacks = [];

		if (typeof this.callbackDependencies[sourceBlockName] !== 'undefined' &&
			typeof this.callbackDependencies[sourceBlockName][sourceBlockModelName] !== 'undefined' &&
			typeof this.callbackDependencies[sourceBlockName][sourceBlockModelName][triggerMethod] !== 'undefined') {
			callbacks = this.callbackDependencies[sourceBlockName][sourceBlockModelName][triggerMethod];
		}

		return callbacks;
	};

	this.getDirtySubscribers = function () {
		return this.dirtySubscribers;
	};

	this.getValidSubcribers = function(){
		return this.validSubcribers;
	}

	this.getInitialLoad = function () {
		return this.initialLoad;
	};

	this.setInitialLoad = function (value) {
		this.initialLoad = value;
	};	
};

RS.common.blocks.BlockContainer.prototype = {
	subscribeValid: function(fn){
		var key = null;

		if(typeof fn === 'function'){
			key = new Date().getTime();
			var validSubcribers = this.getValidSubcribers();
			validSubcribers.push({
				key : key,
				callback : fn
			})
		}
		return key;
	},

	unsubscribeValid: function (key) {
		var validSubcribers = this.getValidSubcribers();

		for (var i = 0; i < validSubcribers.length; i++) {
			if (validSubcribers[i].key === key) {
				validSubcribers.splice(i, 1);
			}
		}
	},

	notifyValid: function () {
		var valid = this.isValid();
		var validSubcribers = this.getValidSubcribers();

		for (var i = 0; i < validSubcribers.length; i++) {
			validSubcribers[i].callback(valid);
		}
	},

	isValid: function () {
		var valid = true;
		var blocks = this.getBlocks();

		for (var k in blocks) {			
			if (!blocks[k].getIsValid()) {
				valid = false;
				break;
			}
		}

		return valid;
	},

	subscribeDirty: function (fn) {
		var key = null;

		if (typeof fn === 'function') {
			key = new Date().getTime();
			var dirtySubscribers = this.getDirtySubscribers();
			dirtySubscribers.push({
				key: key,
				callback: fn
			});
		}

		return key;
	},

	unsubscribeDirty: function (key) {
		var dirtySubscribers = this.getDirtySubscribers();

		for (var i = 0; i < dirtySubscribers.length; i++) {
			if (dirtySubscribers[i].key === key) {
				dirtySubscribers.splice(i, 1);
			}
		}
	},

	notifyDirty: function () {
		var dirty = this.isDirty();		
		var dirtySubscribers = this.getDirtySubscribers();

		for (var i = 0; i < dirtySubscribers.length; i++) {
			dirtySubscribers[i].callback(dirty);
		}
	},

	notifyDataChange : function(blockName, blockModelName, data){
		if(this.dataInternalDependencies[blockName]){
			var sourceBlockName = this.dataInternalDependencies[blockName] || {};
			var dependentBlockNames = sourceBlockName[blockModelName] || [];
			for(var i = 0; i < dependentBlockNames.length; i++){
				var dependentBlockInfo = dependentBlockNames[i];
				var dependentBlockName = dependentBlockInfo['blockName'];
				var dependentBlockModelName = dependentBlockInfo['blockModelName'];
				var dependentBlockModel = this.blocks[dependentBlockName]['blockModels'][dependentBlockModelName];
				var processingMethod = dependentBlockModel['processDataChange'];
				if(Ext.isFunction(processingMethod)){
					processingMethod.apply(dependentBlockModel,[data]);
				}
			}
		}
	},

	isDirty: function () {
		var dirty = false;
		var blocks = this.getBlocks();

		for (var k in blocks) {			
			if (blocks[k].getIsDirty()) {
				dirty = true;
				break;
			}
		}

		return dirty;
	},

	addBlock: function (blockName) {
		var block = new RS.common.blocks.Block(this, blockName);
		var blocks = this.getBlocks();
		var blockNames = this.getBlockNames();
		blocks[blockName] = block;
		blockNames.push(blockName);
		return block;
	},

	hideBlocks: function () {
		var blockNames = [];

		if (arguments.length === 0) {
			blockNames = this.getBlockNames();
		} else if (arguments.length === 1) {
			if (arguments[0].constructor === Array) {
				blockNames = arguments[0];
			} else {
				blockNames.push(arguments[0]);
			}			
		} else {
			blockNames = Array.apply(null, arguments);
		}

		var blocks = this.getBlocks();

		for(var i = 0; i < blockNames.length; i++) {
			blocks[blockNames[i]].hide();
		}
	},

	showBlocks: function () {
		var blockNames = [];

		if (arguments.length === 0) {
			blockNames = this.getBlockNames();
		} else if (arguments.length === 1) {
			if (arguments[0].constructor === Array) {
				blockNames = arguments[0];
			} else {
				blockNames.push(arguments[0]);
			}			
		} else {
			blockNames = Array.apply(null, arguments);
		}

		var blocks = this.getBlocks();

		for(var i = 0; i < blockNames.length; i++) {
			blocks[blockNames[i]].show();
		}
	},

	defineDataDependency: function (source, reactor) {
		var sourceBlockName = source.getBlock().getName(), 
			sourceBlockModelName = source.getName(), 
			dependentBlockName = reactor.getBlock().getName(), 
			dependentBlockModelName = reactor.getName(),
			blockNames = this.getBlockNames();

		if (blockNames.indexOf(sourceBlockName) === -1) {
			throw new Error('The sourceBlockName is not defined.');
		}

		if (blockNames.indexOf(dependentBlockName) === -1) {
			throw new Error('The dependentBlockName is not defined.');
		}

		this.addDataDependency(sourceBlockName, sourceBlockModelName, dependentBlockName, dependentBlockModelName);
	},

	//Handle internal data that not neccessary need to send to backend. Source will trigger notifyDataChange, reactor will process change via processDataChange
	defineInternalDataDependency : function(source, reactor){
		var sourceBlockName = source.getBlock().getName(), 
			sourceBlockModelName = source.getName(), 
			dependentBlockName = reactor.getBlock().getName(), 
			dependentBlockModelName = reactor.getName(),
			blockNames = this.getBlockNames();

		if (blockNames.indexOf(sourceBlockName) === -1) {
			throw new Error('The sourceBlockName is not defined.');
		}

		if (blockNames.indexOf(dependentBlockName) === -1) {
			throw new Error('The dependentBlockName is not defined.');
		}

		this.addInternalDataDependency(sourceBlockName, sourceBlockModelName, dependentBlockName, dependentBlockModelName);
	},

	defineCallback: function (source, callback, triggerMethod) {
		if (!triggerMethod) {
			triggerMethod = 'onLoadOrClose';
			// other callbacks are onChange and onClose
		}

		var sourceBlockName = source.getBlock().getName(), 
			sourceBlockModelName = source.getName(), 
			blockNames = this.getBlockNames();

		if (blockNames.indexOf(sourceBlockName) === -1) {
			throw new Error('The sourceBlockName is not defined.');
		}

		if (typeof callback !== 'function') {
			throw new Error('The callback must be a function.');
		}

		return this.addCallback(triggerMethod, sourceBlockName, sourceBlockModelName, callback);
	},

	removeCallback: function (source, triggerMethod, uniqueID) {
		var sourceBlockName = source.getBlock().getName(), 
			sourceBlockModelName = source.getName(), 
			blockNames = this.getBlockNames();

		if (blockNames.indexOf(sourceBlockName) === -1) {
			throw new Error('The sourceBlockName is not defined.');
		}

		this.spliceCallback(triggerMethod, sourceBlockName, sourceBlockModelName, uniqueID);
	},

	checkDataDependencies: function (triggerMethod, sourceBlockName, sourceBlockModelName, sourceData) {
		var d = this.getDataDependencies(sourceBlockName, sourceBlockModelName),
			callbacks = this.getCallbacks(triggerMethod, sourceBlockName, sourceBlockModelName);

		if (d && d.constructor === Array) {
			var blocks = this.getBlocks();

			for (var i = 0; i < d.length; i++) {
				var block = blocks[d[i].blockName];

				if (block) {					
					block.modifyByNotification(d[i].blockModelName, sourceData.slice(), sourceBlockModelName);
				}
			}
		}

		for (var i = 0; i < callbacks.length; i++) {
			callbacks[i].apply(this, sourceData);
		}
	},

	checkCallbacksOnLoadOrClose: function (sourceBlockName, sourceBlockModelName, sourceData) {
		var callbacks = this.getCallbacks('onLoadOrClose', sourceBlockName, sourceBlockModelName);

		for (var i = 0; i < callbacks.length; i++) {
			callbacks[i].apply(this, sourceData);
		}
	},

	checkCallbacksOnClose: function (sourceBlockName, sourceBlockModelName, sourceData) {
		var callbacks = this.getCallbacks('onClose', sourceBlockName, sourceBlockModelName);

		for (var i = 0; i < callbacks.length; i++) {
			callbacks[i].apply(this, sourceData);
		}
	},

	getData: function () {
		var blockNames = this.getBlockNames(),
			blocks = this.getBlocks(),
			data = {},
			result = {
				paths: [],
				leaves: []
			};

		for (var i = 0; i < blockNames.length; i++) {
			var blockResult = blocks[blockNames[i]].getDataForServer();
			result.paths.push.apply(result.paths, blockResult.paths);
			result.leaves.push.apply(result.leaves, blockResult.leaves);
		}

		if (result.paths.length !== result.leaves.length) {
			throw new Error('paths.length does not match leaves.length');
		}

		var firstBlock = blocks[blockNames[0]];

		for (var i = 0; i < result.paths.length; i++) {
			var tuple = firstBlock.getPathAndFilter(result.paths[i]);
			firstBlock.deepUpdate(data, result.leaves[i], tuple.path.split('.'), tuple.filter, true);
		}

		return data;
	},

	loadOriginalData: function (originalData, originalDirtyFlag) {
		var blockNames = this.getBlockNames(),
			blocks = this.getBlocks();

		for(var i = 0; i < blockNames.length; i++) {
			blocks[blockNames[i]].loadOriginalData(originalData[i], originalDirtyFlag);
		}
	},

	getOriginalData: function () {
		var blockNames = this.getBlockNames(),
			blocks = this.getBlocks(),
			originalData = [];

		for(var i = 0; i < blockNames.length; i++) {
			originalData.push(blocks[blockNames[i]].getOriginalData());
		}

		return originalData;
	},

	load: function (payload) {
		var blockNames = this.getBlockNames(),
			blocks = this.getBlocks();

		for(var i = 0; i < blockNames.length; i++) {
			blocks[blockNames[i]].load(payload);
		}

		if (!this.getInitialLoad()) {
			for(var i = 0; i < blockNames.length; i++) {
				blocks[blockNames[i]].reportDataModificationForAllBlockModels(this, true);
			}

			this.notifyDirty(false);
		} else {
			this.setInitialLoad(false);
		}
	},

	reload: function (payload) {
		var blockNames = this.getBlockNames(),
			blocks = this.getBlocks();

		for(var i = 0; i < blockNames.length; i++) {
			blocks[blockNames[i]].load(payload);
		}

		for(var i = 0; i < blockNames.length; i++) {
			blocks[blockNames[i]].reportDataModificationForAllBlockModels(this, false);
		}
	},

	saveComplete: function () {
		var blockNames = this.getBlockNames(),
			blocks = this.getBlocks();

		for(var i = 0; i < blockNames.length; i++) {
			blocks[blockNames[i]].saveComplete();
		}
	}

};
Ext.define('RS.common.blocks.BlockModel', {
    autoInit: false,
    isDirty: false,
    isValid : true,
    suspendDirtyNotification: false,

    constructor: function (block, blockModelName, modelIndex) {
        this.gluModel = null;
        var originalData = '';

        this.getBlock = function () {
            return block;
        };

        this.getName = function () {
            return blockModelName;
        };

        this.getModelIndex = function () {
            return modelIndex;
        };

        this.setOriginalData = function (d) {
            originalData = JSON.stringify(d);
        };

        this.setOriginalDataString = function (d) {
            originalData = d;
        };

        this.getOriginalData = function () {
            return originalData;
        };

    },

    beforeLoad: function () {
        this.suspendDirtyNotification = true;
    },

    load: function () {
    },

    afterLoad: function (data) {
        this.setOriginalData(this.getData(true));
        this.suspendDirtyNotification = false;
    },

    close: function (nextBlockModelName) {            
        var block = this.getBlock(),
            blockName = this.getName();
            //collapse = false;

        if (!nextBlockModelName) {
            nextBlockModelName = blockName;
            //collapse = this.isCollapsible();
        }

        block.close(blockName, nextBlockModelName);
        
        //if (collapse) {
        //    this.collapse();
        //} else {
        //   this.hide();
        //}
		this.hide();
    },

    addEditLink: function (nextBlockViewName, tooltip, isHidden, margin, callback) {
        var block = this.getBlock();

        if (typeof margin === 'undefined' || margin === null) {
            margin = '0 10 0 0';
        } else {
            margin = 0;
        }
        
        var config = {
            xtype: 'button',
            iconCls: 'rs-block-button icon-pencil with-gap',
            url: nextBlockViewName,
            tooltip: tooltip,
            text: '',
            preventDefault: true,
            isEdit: true,
            margin: margin,
			callback: callback,
            style: {
                top: '2px important'
            },
            handler: function (c, e) {
                e.preventDefault();
                e.stopPropagation();
                block.showView(config.url);
                if (typeof this.callback === 'function') {
					callback();
				}
            }

        };

        this.gluModel.header.items.push(config);
    },

    addWizardLink: function (nextBlockViewName, tooltip, isHidden, margin, callback) {
        var block = this.getBlock();

        if (typeof margin === 'undefined' || margin === null) {
            margin = '0 10 0 0';
        } else {
            margin = 0;
        }

        var config = {
            xtype: 'button',
            iconCls: 'rs-block-button icon-magic with-gap',
            url: nextBlockViewName,
            tooltip: tooltip,
            text: '',
            preventDefault: true,
            isEdit: true,
            margin: margin,
			callback: callback,
            style: {
                top: '2px important'
            },
            handler: function (c, e) {
                e.preventDefault();
                e.stopPropagation();
                block.showView(config.url);
				if (typeof this.callback === 'function') {
					callback();
				}
            }
        };

        this.gluModel.header.items.push(config);
    },

	addResizeMaxBtn: function (tooltip, hidden, callback, margin) {
        var block = this.getBlock();

        if (typeof margin === 'undefined' || margin === null) {
            margin = '0 10 0 0';
        } else {
            margin = 0;
        }
        
        var config = {
            xtype: 'button',
            iconCls: 'rs-block-button icon-resize-full with-gap',
            tooltip: tooltip,
            text: '',
            preventDefault: true,
			isResizeMaxBtn: true,
			hidden: hidden,
            margin: margin,
			callback: callback,
            style: {
                top: '2px important'
            },
			handler: function (c, e) {
                e.preventDefault();
                e.stopPropagation();
                if (typeof this.callback === 'function') {
					callback();
				}
            }
        };

        this.gluModel.header.items.push(config);
    },

	addResizeNormalBtn: function (tooltip, hidden, callback, margin) {
        var block = this.getBlock();

        if (typeof margin === 'undefined' || margin === null) {
            margin = '0 10 0 0';
        } else {
            margin = 0;
        }
        
        var config = {
            xtype: 'button',
            iconCls: 'rs-block-button icon-resize-small with-gap',
            tooltip: tooltip,
            text: '',
            preventDefault: true,
			isResizeNormalBtn: true,
			hidden: hidden,
            margin: margin,
			callback: callback,
            style: {
                top: '2px important'
            },
            handler: function (c, e) {
                e.preventDefault();
                e.stopPropagation();
                if (typeof this.callback === 'function') {
					callback();
				}
            }
        };

        this.gluModel.header.items.push(config);
    },

    modifyByNotification: function () {
    },

    notifyDirty: function () {
        if (!this.suspendDirtyNotification) {
            this.setIsDirty();
            var block = this.getBlock();
            block.reportDataModification(this);
            block.notifyDirty();
        }
    },

    notifyValid : function(valid){
        this.isValid = valid;  
        this.updateTitle(valid, this.isDirty);     
        this.getBlock().notifyValid();
    },

    notifyDataChange : function(data){
        var block = this.getBlock();
        var blockModelName = this.getName();
        block.notifyDataChange(blockModelName, data);
    },

    setIsDirty: function () {
        var data = this.getData(true),
            prior = this.getOriginalData();
        this.isDirty = JSON.stringify(data) !== prior;
        this.updateTitle(this.isValid, this.isDirty);
        return this.isDirty;
    },

    getData: function (dataIsForServer) {
        return [];
    },

    getDataPaths: function () {
        return [];
    },    

    updateTitle: function (valid,dirty) {
        var title = this.gluModel.title.replace(/ <span style=\"color:crimson;\">!<\/span>/, '');
        title = title.replace(/\*/,'');

        if (!valid){
             title = title + ' <span style="color:crimson;">!</span>';
        }
        else if (dirty) {
            title = title + '*';
        }

        this.setViewTitle(title);
        return title;
    },

    setViewTitle: function (title) {
        this.gluModel.set('title', title);
    },

    hide: function () {
        this.gluModel.set('hidden', true);

        //if (this.isCollapsible()) {
        //   this.collapse();
        //}
    },

    show: function () {
        this.gluModel.set('hidden', false);
    },

    showCurrent: function () {
        this.getBlock().show();
    },

    collapse: function () {
		//if (!this.gluModel.isSectionMaxSize) {
			this.gluModel.set('collapsed', true);
		//}
        return this;
    },

    expand: function () {
        this.gluModel.set('collapsed', false);
    },

    set: function (name, value) {
        this.gluModel.set(name, value);
        return this;
    },

    get: function (name) {
        return this.gluModel.get(name);
    },

    createNameValueItems: function () {
        var items = [], values = [];

        if (arguments.length === 1) {
            values = arguments[0];
        } else {
            values = Array.apply(null, arguments);
        }

        for (var i = 0; i < values.length; i++) {
            items.push({ 
                name: this.gluModel.localize(values[i]), 
                value: values[i] 
            });
        }
        
        return items;
    },

    isCollapsible: function () {
        return typeof this.gluModel.collapsed !== 'undefined';
    }
});
/**
 * @class Ext.ux.aceeditor.Editor
 * @extends Ext.AbstractComponent
 *
 * @author Harald Hanek (c) 2011-2012
 * @license http://harrydeluxe.mit-license.org
 */
Ext.define('Ext.ux.aceeditor.Editor', {
    extend: 'Ext.util.Observable',
    path: '',
    sourceCode: '',
    autofocus: true,
    fontSize: '12px',   
    theme: 'eclipse',  
    printMargin: false,
    printMarginColumn: 80,
    highlightActiveLine: true,
    highlightGutterLine: true,
    highlightSelectedWord: true,
    showGutter: true,
    fullLineSelection: true,
    tabSize: 4,
    useSoftTabs: false,
    showInvisible: false,
    useWrapMode: false,
    codeFolding: true,
    behavioursEnabled : false,
    lineHeight: 'auto',
    showCursor: true,

    constructor: function(owner, config) {
        var me = this;
        me.owner = owner;
        me.addEvents({
            editorcreated: true,
            change: true,
            editorDestroyed: true
        });
        me.callParent();
    },

    initEditor: function() {
        var me = this;
        me.editor = ace.edit(me.editorId);
		me.setMode(me.parser || Ext.state.Manager.get('aceLanguage', 'groovy'));
        me.setTheme(me.theme);
        me.setKeyboardHandler(me.keyboardHandler || Ext.state.Manager.get('aceKeybinding', 'windows'));
        me.editor.getSession().setUseWrapMode(me.useWrapMode);
        me.editor.setShowFoldWidgets(me.codeFolding);
        me.editor.setShowInvisibles(me.showInvisible);
        me.editor.setHighlightGutterLine(me.highlightGutterLine);
        me.editor.setHighlightSelectedWord(me.highlightSelectedWord);
        me.editor.renderer.setShowGutter(me.showGutter);
        me.setFontSize(me.fontSize);
        me.editor.container.style.lineHeight = me.lineHeight;
        me.editor.setShowPrintMargin(me.printMargin);
        me.editor.setPrintMarginColumn(me.printMarginColumn);
        me.editor.setHighlightActiveLine(me.highlightActiveLine);
        me.getSession().setTabSize(me.tabSize);
        me.getSession().setUseSoftTabs(me.useSoftTabs);
        me.setValue(me.sourceCode || me.value);
        me.editor.setReadOnly(me.readOnly);
        me.editor.setBehavioursEnabled(me.behavioursEnabled);
        me.editor.getSession().on('change', function() {
            me.fireEvent('change', me);
        }, me);

        if (!me.showCursor) {
          me.editor.renderer.$cursorLayer.element.style.opacity = 0;
        }

        if (me.autofocus) {
            me.editor.focus();
        } else {
            me.editor.renderer.hideCursor();
            me.editor.blur();
        }

        me.editor.initialized = true;
        me.fireEvent('editorcreated', me);
    },

    getEditor: function() {
        return this.editor;
    },

    getSession: function() {
        var session = null;

        if (this.editor) {
            session = this.editor.getSession();
        }

        return session;
    },

    getTheme: function() {
        var theme = null;

        if (this.editor) {
            theme = this.editor.getTheme();
        }

        return theme;
    },

    setTheme: function(name) {
        if (this.editor) {
            this.editor.setTheme("ace/theme/" + name);
        }
    },

    setMode: function(mode) {
        var session = this.getSession();
        
        if (session) {
            session.setMode("ace/mode/" + mode);    
        }
    },

    setKeyboardHandler: function(keyboardHandler) {
        if (Ext.Array.indexOf(['vim', 'emacs'], keyboardHandler) > -1) {
            this.editor.setKeyboardHandler("ace/keyboard/" + keyboardHandler);
        }
    },

    setReadOnly: function(value) {
        if (this.editor) {
            this.editor.setReadOnly(value)
        } else {
            this.readOnly = value
        }
    },

    setShowGutter: function(value) {
        if (this.editor && this.editor.renderer) {
            this.editor.renderer.setShowGutter(value);
        } else {
            this.showGutter = value;
        }
    },

    getValue: function() {
        return this.editor ? this.editor.getSession().getValue() : this.sourceCode;
    },

    setValue: function(value) {
        if (this.editor) {
            this.editor.getSession().setValue(value);
        } else {
            this.sourceCode = value;
        }
    },

    setFontSize: function(value) {
        this.editor.setFontSize(value);
    },

    undo: function() {
        this.editor.undo();
    },

    redo: function() {
        this.editor.redo();
    },
    insertAtCursor : function(data){
        this.editor.insert(data);
    }
});
/**
 * @class Ext.ux.aceeditor.Panel
 * @extends Ext.panel.Panel
 *
 * @author Harald Hanek (c) 2011-2012
 * @license http://harrydeluxe.mit-license.org
 */
Ext.define('Ext.ux.aceeditor.Panel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.AceEditor',
    mixins: {
        editor: 'Ext.ux.aceeditor.Editor'
    },
    layout: 'fit',
    autofocus: true,
    border: false,
    cls: '',
    listeners : {},

    defaultListeners: {
        resize: function() {
            if (this.editor) {
                this.editor.resize();
            }
        },
        activate: function() {
            if (this.editor) {
                this.editor.setStyle(this.cls);

                if (this.autofocus) {
                    this.editor.focus();
                }
            }
        },
        beforedestroy : function(){
            this.un('afterlayout', this.afterlayoutHandler, this);
            var editorSession = this.editor ? this.editor.getSession() : null;
            if(editorSession)
                editorSession.removeAllListeners('change');
        },      
    },

    initComponent: function() {
        var me = this,
            items = {
                xtype: 'component',
                style : 'border:1px solid grey',
                autoEl: 'pre'
            };

        Ext.apply(me, {
            items: items
        });

        Ext.applyIf(me.listeners, me.defaultListeners);
        me.callParent(arguments);
    },

    onRender: function() {
        var me = this;

        if (me.sourceEl != null) {
            me.sourceCode = Ext.get(me.sourceEl).dom.textContent || Ext.get(me.sourceEl).dom.outerText;
        }

        me.editorId = me.items.keys[0];
        me.oldSourceCode = me.sourceCode;
        me.callParent(arguments);

        // init editor on afterlayout
        me.on('afterlayout', me.afterlayoutHandler, me);
    },
    afterlayoutHandler : function() {
        if (this.url) {
            Ext.Ajax.request({
                url: this.url,
                success: function(response) {
                    this.sourceCode = response.responseText;

                    if (!me.editor) {
                        this.initEditor();
                    }
                }
            });
        }
        else if (!this.editor) {
            this.initEditor();
        }
        if (this.editor) {
            this.editor.resize();
            //HACK: on using ace editor in a pop up window, the layout of this editor will cause the document windown to scroll (due to extra height added).
            //This will cause the background ,which the window pop up render on top, be off by this scroll.
            //Just need to reset window scroll when using aceditor inside a window pop up.
            var parentContainer = this.up();
            while(parentContainer && parentContainer.up()){
                parentContainer = parentContainer.up();
            }
            if( parentContainer && parentContainer.xtype == 'window')
                document.body.scrollTop = 0;             
        }
    }   
});

glu.regAdapter('AceEditor', {
    extend: 'field',

    afterCreate: function(control, viewmodel) {
        glu.provider.adapters.Field.prototype.afterCreate.apply(this, arguments);

        if (!control.delayedEvent) {
            control.delayedEvent = new Ext.util.DelayedTask(function() {
                control.fireEvent('textchanged', control);
            });
        }

        control.on('change', function(ctrl) {
            control.delayedEvent.delay(control.keyDelay || 100);
        }, control)
    },

    initAdapter: function() {
        this.valueBindings = glu.deepApplyIf({
            eventName: 'textchanged'
        }, this.valueBindings);
    },

    valueBindings: {
        eventName: 'change',
        eventConverter: function(field, newVal) {
            return field.getValue();
        },
        setComponentProperty: function(value, oldvalue, options, control) {
            control.suspendCheckChange++;

            if (control.getValue() != value) {
                control.setValue(value);
            }
            control.lastValue = value;
            control.suspendCheckChange--;
        }
    },

    parserBindings: {
        setComponentProperty: function(value, oldValue, options, control) {
            control.suspendCheckChange++;
            control.setMode(value);
            Ext.state.Manager.set('aceLanguage', value);
            control.lastParser = value;
            control.suspendCheckChange--;
        }
    },

    keyboardBindings: {
        setComponentProperty: function(value, oldValue, options, control) {
            control.suspendCheckChange++;
            control.setKeyboardHandler(value);
            Ext.state.Manager.set('aceKeybinding', value);
            control.lastKeyboard = value;
            control.suspendCheckChange--;
        }
    },

    readOnlyBindings: {
        setComponentProperty: function(value, oldValue, options, control) {
            control.suspendCheckChange++;
            control.setReadOnly(value);
            control.lastReadOnly = value;
            control.suspendCheckChange--;
        }
    },

    showGutterBindings: {
        setComponentProperty: function(value, oldValue, options, control) {
            control.suspendCheckChange++;
            control.setShowGutter(value);
            control.lastShowGutter = value;
            control.suspendCheckChange--;
        }
    }
})
Ext.define('RS.common.grid.column.ActionColumn', {
	extend: 'Ext.grid.column.Action',
	alias: 'widget.actioncolumn',

	idField: 'sys_id',

	constructor: function(config) {
		var me = this;
		var items = [{
				icon: '/resolve/images/common/delete.png',
				tooltip: config.deleteTooltip ||"Delete",
				handler: function(gridView, rowIndex, colIndex) {
					var rec = gridView.getStore().getAt(rowIndex),
						idField = rec ? rec.get(me.idField) : 'sys_id';
					gridView.ownerCt.fireEvent(me.deleteEventName, gridView , idField, rowIndex);
				}
			}]

		if(config.editAction){
			items.push(
			{
				icon: '/resolve/images/common/edit.png',
				tooltip: config.editTooltip || "Edit",
				handler: function(gridView, rowIndex, colIndex) {
				
				}
			});
		}
		Ext.applyIf(config, { 
			items : items
		 });
		me.callParent([config]);
	}
})
Ext.define('RS.common.BooleanComboBox', {
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.booleancombobox',

	displayField: 'name',
	valueField: 'value',

	initComponent: function() {
		this.store = Ext.create('Ext.data.Store', {
			fields: ['name', 'value'],
			data: [{
				name: RS.common.locale.trueText,
				value: 'true'
			}, {
				name: RS.common.locale.falseText,
				value: 'false'
			}]
		});
		this.callParent();
	}
});
Ext.define('RS.common.ResolveColorPicker',{
	extend : 'Ext.picker.Color',
	alias : 'widget.resolvecolorpicker',
	colors : [
        '333333', '000000', '993300', '333300', '003300', '003366', '000080', '333399', 
        '808080', '800000', 'FF6600', '808000', '008000', '008080', '0000FF', '666699', 
        '969696', 'FF0000', 'FF9900', '99CC00', '339966', '33CCCC', '3366FF', '800080', 
        'C0C0C0', 'FF00FF', 'FFCC00', 'FFFF00', '00FF00', '00FFFF', '357EC7', '993366', 
        'F1EFEF', 'FF99CC', 'FFCC99', 'FFFF99', 'A6F3A6', 'ADD8E6', '00CCFF', 'CC99FF', 
        'FFFFFF', 'FDD6E9', 'F1E0CF', 'F5F5DC', 'E0FDE0', 'CCFFFF', '99CCFF', 'DBD3E4'
    ],
    initComponent : function(config){
    	this.callParent([config]);
    }
})
Ext.define('RS.common.ResolveColorMenu',{
	extend: 'Ext.menu.Menu',
	alias : 'widget.resolvecolormenu',
	requires : ['RS.common.ResolveColorPicker'],
	hideOnClick : true,
    pickerId : null,
    height : 146,
    initComponent : function(){
        var me = this,
            cfg = Ext.apply({}, me.initialConfig);

        // Ensure we don't get duplicate listeners
        delete cfg.listeners;
        Ext.apply(me, {
            plain: true,
            showSeparator: false,
            bodyPadding: 0,
            items: Ext.applyIf({
                cls: Ext.baseCSSPrefix + 'menu-color-item',
                margin: 0,
                id: me.pickerId,
                xtype: 'resolvecolorpicker'
            }, cfg)
        });

        me.callParent(arguments);

        me.picker = me.down('resolvecolorpicker');
        me.relayEvents(me.picker, ['select']);

        if (me.hideOnClick) {
            me.on('select', me.hidePickerOnSelect, me);
        }
    },
    hidePickerOnSelect: function() {
        Ext.menu.Manager.hideAll();
    }
})
function downloadFile(url) {
	var downloadFrame = Ext.get('downloaderFrame');
	if (downloadFrame) {
		downloadFrame.dom.src = url;
	} else {
		var m = Ext.core.DomHelper.append(Ext.getBody(), {
			html: ['<iframe id="downloaderFrame" src="', url, '" style="display:none"'].join('')
		}, true);
	}
}
Ext.define('RS.common.FilterBar', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.filterbar',

	allowPersistFilter: true,

	border: false,
	dock: 'top',
	layout: {
		type: 'hbox',
		align: 'middle'
	},
	items: [],
	autoScroll: true,
	bodyCls: 'x-toolbar-default',
	padding: '0px 0px 0px 10px',
	addFilter: function(value, internalValue) {
		if (this.items.length == 0) {
			//Add in the filter specific items (save, edit, clear)
			this.add({
				xtype: 'toolbar',
				items: [{
					text: RS.common.locale.saveFilter,
					hidden: !this.allowPersistFilter,
					scope: this,
					handler: this.saveFilter
				}, {
					text: RS.common.locale.clearFilter,
					scope: this,
					handler: this.clearFilter
				}]
			});
		}
		//Check uniqueness of the filter
		var found = false;
		this.items.each(function(item) {
			if (item.value == value && item.internalValue == internalValue) {
				found = true
				return false
			}
		})
		if (!found)
			this.insert(this.items.length - 1, {
				xtype: 'filterbaritem',
				value: value,
				internalValue: internalValue
			});
	},
	removeFilter: function(item) {
		this.remove(item);
		if (this.items.length == 1) {
			this.clearFilter();
		}
		this.fireEvent('filterChanged');
	},
	saveFilter: function(button) {
		this.fireEvent('saveFilter', this);
	},
	clearingFilters: false,
	clearFilter: function(button,continueFiring,suppressChangeEvt) {
		this.clearingFilters = true;
		this.items.each(function(item) {
			this.remove(item)
		}, this);
		this.clearingFilters = false;
		if(continueFiring!==false)
			this.fireEvent('clearFilter');
		if(suppressChangeEvt !== true){
			this.fireEvent('filterChanged');
		}
	},
	getFilters: function() {
		var filters = [];
		this.items.each(function(filter) {
			if (filter.internalValue) filters.push(filter.internalValue);
		});
		return filters;
	},
	setFilters: function(filters) {
		this.suspendEvents(false);
		this.items.each(function(item) {
			this.remove(item);
		}, this);
		this.resumeEvents();
		Ext.each(filters, function(filter) {
			this.addFilter(filter.value, filter.internalValue);
		}, this);
		this.fireEvent('filterChanged');
	}
});

Ext.define('RS.common.FilterBarItem', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.filterbaritem',
	border: false,
	layout: {
		type: 'hbox',
		align: 'middle'
	},
	bodyCls: 'x-toolbar-default',
	height: 30,
	padding: '0 10 0 0',

	initComponent: function() {
		var me = this;
		me.items = [{
			xtype: 'label',
			flex: 1,
			padding: '0 5 0 0',
			text: Ext.util.Format.ellipsis(this.value, 60)
		}, {
			xtype: 'image',
			src: '/resolve/images/remove.gif',
			listeners: {
				render: function() {
					this.getEl().on('mouseover', function() {
						this.setSrc('/resolve/images/remove2.gif');
					}, this);
					this.getEl().on('mouseout', function() {
						this.setSrc('/resolve/images/remove.gif')
					}, this);
					this.getEl().on('click', me.removeFilter, me);
					this.getEl().on('load', function() {
						me.doLayout()
					})
				}
			}

		}];
		me.callParent();
	},

	getValue: function() {
		return this.value;
	},

	removeFilter: function(filter) {
		this.ownerCt.removeFilter(this);
	}

});
Ext.define('RS.common.controls.FollowButton', {
	extend: 'Ext.button.Button',
	alias: 'widget.followbutton',

	ui: 'system-info-button-small',
	iconCls: 'rs-icon-button icon-mail-forward',

	config: {
		streamId: ''
	},

	initComponent: function() {
		if (Ext.isGecko) this.setIconCls(this.iconCls + ' rs-icon-firefox')
		this.tooltip = this.tooltip || RS.common.locale.followButtonTooltip
		this.handler = this.handleButtonClick
		this.overflowText = RS.common.locale.followButtonTooltip
		this.callParent()
	},

	handleButtonClick: function(button) {
		if (!button.popup) {
			var model = glu.model({
				mtype: 'RS.social.Followers',
				addFollowers: true,
				buttonWindow: true,
				streamId: button.streamId,
				streamType: button.streamType,
				removeFollowers: button.removeFollowers || true
			})
			model.init()
			button.popup = glu.openWindow(model)
			button.popup.on('close', function() {
				button.popup = null
			})
			button.popup.show()
			button.popup.alignTo(button.getEl().id, 'tr-br')
		} else {
			button.popup.close()
		}
	},

	onDestroy: function() {
		this.callParent()
		if (this.popup) this.popup.destroy()
	}
})
/**
 * Control to display a large image when the user clicks on a thumbnail image within a post.
 */
Ext.define('RS.common.controls.ImageViewer', {
	extend: 'Ext.window.Window',
	alias: 'rsimageviewer',

	layout: 'fit',
	frame: false,
	closeAction: 'hide',
	modal: true,
	constrain: true,
	onEsc: function() {
		this.close();
	},
	baseCls: Ext.isIE8m ? 'x-window' : 'x-panel',
	ui: Ext.isIE8m ? 'default' : 'sysinfo-dialog',

	/**
	 * The source of the image tag to display in the large window
	 */
	src: '',

	initComponent: function() {
		this.items = {
			xtype: 'component',
			autoEl: {
				tag: 'img',
				style: 'height:auto;width:auto',
				src: this.src
			},
			listeners: {
				render: {
					scope: this,
					fn: function(cmp) {
						this.getImage().on('load', this.onImageLoad, this, {
							single: true
						});
					}
				}
			}
		};
		this.callParent(arguments);
	},

	onImageLoad: function() {
		this.removeMask();
		var size = Ext.fly(Ext.getBody()).getSize(),
			bodyHeight = size.height,
			bodyWidth = size.width,
			image = this.getImage();
		image.setStyle({
			height: 'auto',
			width: 'auto'
		});
		var h = image.dom.offsetHeight,
			w = image.dom.offsetWidth,
			toConfig = {};

		//preserve scale
		var scaledSize = this.scaleSize(bodyWidth - 26, bodyHeight - 58, w, h);
		//10-25 to bring the image away from the edges
		var width = scaledSize[0];
		var height = scaledSize[1];
		var x = Math.round((bodyWidth - width) / 2);
		var y = Math.round((bodyHeight - height) / 2);
		toConfig = {
			width: width,
			height: height,
			x: x,
			y: y
		};

		this.animate({
			to: toConfig,
			listeners: {
				afteranimate: {
					fn: function() {
						this.doComponentLayout();
					},
					scope: this
				}
			}
		});
	},
	removeMask: function() {
		this.getEl().unmask();
	},

	/**
	 * Scales the image to the max height and width from the current height and width
	 */
	scaleSize: function(maxW, maxH, currW, currH) {

		var ratio = currH / currW;

		if (currW >= maxW && ratio <= 1) {
			currW = maxW;
			currH = currW * ratio;
		}
		if (currH >= maxH) {
			currH = maxH;
			currW = currH / ratio;
		}

		return [currW, currH];
	},

	/**
	 * Sets the image source and resizes the window to the largest size available (the size of the image or the size of the browser window) maintaining scale of the image.
	 */
	setSrc: function(src) {
		this.getEl().mask('Loading...');
		var image = this.getImage();
		image.on('load', this.onImageLoad, this, {
			single: true
		});
		image.dom.src = src;
		image.dom.style.width = image.dom.style.height = 'auto';
		//this is primarily to fix an issue with Chrome dealing with cached images
		if(this.is_img_cached(src)) {
			this.removeMask();
		}
	},
	is_img_cached: function(src) {
	    //It's necessary to create an image object to test complete property.
	    //It always returns true when you test it directly on an image element from the DOM.
	    //IE9 and IE10 always return false. No known fix at this time.
	    var image = new Image();
	    image.src = src;
	    return image.complete;
	},
	getImage: function() {
		return this.items.items[0].getEl();
	},
	initEvents: function() {
		this.callParent(arguments);
		if (this.resizer) {
			this.resizer.preserveRatio = true;
		}
	}
});


function displayLargeImage(url, displayName) {
	if (!this.imageWindow) {
		this.imageWindow = Ext.create('RS.common.controls.ImageViewer', {
			src: url,
			title: displayName
		});
	} else {
		this.imageWindow.setTitle(displayName);
		this.imageWindow.setSrc(url);
	}
	this.imageWindow.show();
}
//the UX router won't consider params before # so add the temporay hack to let wiki accept url params(which is before#,param after # are only for router use)
function openLink(link, config) {
	var result = false;

	if(link.href) {
		//if the link has ?, then need to reload the page to get the url param updated, temporary solution, need to solve it in client
		if (link.href.indexOf('?') != -1) {
			result = true;
		} else {
			//if the link does not have ? do redirect using client

			if (getResolveRoot().location.href.indexOf('rsclient.jsp') > -1) {
				var params = {};
				
				if (link.href.match(/#.+$/g)) {
					var rout = link.href.match(/#.+$/g)[0];
					var split = rout.split('\/');
					var app = split[0];
					var query = split[1];
					var params = Ext.urlDecode(query || '');
				}

				if (config.name) {
					Ext.defer(function() {
						clientVM.handleNavigation({
							modelName: 'RS.wiki.Main',
							params: Ext.applyIf(params, config),
							target: link.target
						});
					}, 1);
					
					result = false;
				} else {
					result = true;
				}
			}
		}
	}

	return result;
}

function refreshModelLinks() {
	var modelFrames = Ext.query('iframe[src^=/resolve/service/wiki/viewmodel]');
	Ext.Array.forEach(modelFrames, function(frame) {
		var splits = Ext.fly(frame).getAttribute('src').split('?'),
			params = Ext.Object.fromQueryString(splits[1]);
		if (params.PROBLEMID != clientVM.problemId) {
			params.PROBLEMID = clientVM.problemId
			frame.src = splits[0] + '?' + Ext.Object.toQueryString(params)
		}
	})
}

//Processes legacy wiki linkes that used to go directly to /resolve/service/wiki/view/namespace/name and keep them in the client.
//The link macro already does this processing, but apparently users were using their own links to the wiki documents directly
//so this will handle the case where the link macro didn't catch any pre-existing links to the old pages and make everything
//work properly with the new rsclient.jsp
function processLegacyWikiLinks() {
	var links = Ext.getBody().query('[href*=]'),
		link;
	Ext.Array.forEach(links, function(l) {
		link = Ext.get(l)
		if (link.dom.href) {
			var index = link.dom.href.indexOf('/resolve/service/wiki/view/');
			
			if(index > -1 && !link.getAttribute('onclick') && (!link.getAttribute('target') || link.getAttribute('target') == '_top')) {
				var start = index + '/resolve/service/wiki/view/'.length;
				link.set({
					onclick: Ext.String.format('return openLink(this, {name: \'{0}\'})', decodeURI(link.dom.href.substring(start).replace('/', '.')))
				});
			}
		}
	})
}

function gotoURL(windowName, url) {
	var params = windowName.substring(windowName.indexOf('?') + 1);
	var target = '';
	var arrOfStrings = params.split('&');
	for (var i = 0; i < arrOfStrings.length; i++) {
		var value = arrOfStrings[i];
		if (value && value.indexOf('=') > -1) {
			var kvarr = value.split('=');
			if (kvarr[0] == 'target' && Ext.String.trim(kvarr[1]) != '') {
				target = kvarr[1];
				break;
			}
		}
	}
	target = Ext.String.trim(target);
	if (!target) {
		doAddTab(windowName, url);
	} else {
		doOpenWindow(windowName, target, url)
	}
}

function doOpenWindow(windowName, target, url) {
		window.open(url, target);
	}
	//always add a new tab or open a new window

function doAddTab(windowName, url) {
	if (window.navigator.userAgent.indexOf('MSIE') != -1 || !windowName || windowName.indexOf('.') > 0) {
		window.open(url, '_blank');
	} else {
		if (windowName) {
			windowName = windowName.replace('#', '');
		}
		window.open(url, windowName);
	}
}
/**
 * Message class that displays a box at the top of the screen to indicate a sucess or failure from the server for a dramatic way for the users to see what happened.
 */
Ext.namespace('RS');
RS.UserMessage = function() {
	var msgCt;

	function createBox(t, s, tbId) {
		return ['<div class="msg" style="z-index: 20000">',
			'<div class="msg-header">',
			'<div class="icon-large icon-remove-sign message-close" style="float:right;"></div>',
			'<h3>' + t + '</h3>',
			'</div>',
			'<p>' + s + '</p>',
			'<div id="' + tbId + '"></div',
			'</div>'
		].join('');
	}

	return {
		msg: function(config) {
			if (getResolveRoot().RS && getResolveRoot().RS.UserMessage && getResolveRoot().RS.UserMessage != RS.UserMessage) {
				getResolveRoot().RS.UserMessage.msg(config);
				return;
			}
			Ext.applyIf(config, {
				success: true,
				title: '',
				msg: '',
				duration: 4000,
				closeable: true,
				position: 'tr-tr',
				offset: [-10, 3],
				animationDirection: 'r',
				width: 300,
				items: [],
				postItems: []
			});
			if (!document.getElementById('msg-div')) {
				msgCt = Ext.DomHelper.append(document.body, {
					id: 'msg-div'
				}, true);
			}

			//set the appropriate width for the message
			msgCt.setWidth(config.width);
			var tbId = Ext.id(),
				m = Ext.DomHelper.append(msgCt, createBox(config.title, config.msg, tbId), true);
			m.configuration = config;

			//setup on click handlers for links to auto close the dialog
			var links = m.query('a')
			Ext.Array.forEach(links, function(link) {
				Ext.fly(link).on('click', function() {
					m.fadeOut({
						duration: 200,
						remove: true
					})
				})
			})

			if (config.closeable)
				config.items.push({
					xtype: 'button',
					text: config.closeText || RS.common.locale.close,
					handler: function(button) {
						m.fadeOut({
							duration: 200,
							remove: true
						})
					}
				})
			else
				m.select('img').setStyle('visibility', 'hidden');

			if (config.items.length > 0 || config.postItems.length > 0) Ext.create('Ext.toolbar.Toolbar', {
				renderTo: tbId,
				cls: 'message-box-toolbar-footer',
				defaultButtonUI: 'message-box-toolbar-button-small',
				items: config.items.concat(config.postItems)
			})

			if (config.success === 2) {
				m.down('h3').setStyle({
					color: '#333333'
				});
			} else if (config.success) {
				m.down('h3').addCls('success');
			} else {
				m.down('h3').addCls('fail');
			}

			m.on('mouseover', function() {
				//stop animations so that we don't hide the message when the user has hovered it
				m.hovered = true;
				if (m.getActiveAnimation())
					return;
				m.stopAnimation();
				m.setOpacity(1, false);
				m.show();
			}, m);
			m.on('mouseout', function(e) {
				//resume ghosting
				m.hovered = false;
				if (config.duration) m.fadeOut({
					duration: 200,
					delay: config.duration,
					scope: m,
					callback: this.removeElement
				});
			}, this);

			//m.select('div').on('click', function() {
			//	m.fadeOut({
			//		duration: 200,
			//		remove: true
			//	});
			//});

			m.select('div > a.x-btn-noicon').on('click', function() {
				m.remove();
			});
			m.select('div.icon-remove-sign').on('click', function() {
				m.remove();
			});

			m.hide();
			// m.alignTo(document.body, config.position, config.offset) //removed with msg-id moved to the right now
			// m.slideIn(config.animationDirection);
			m.fadeIn({
				duration: 400,
				easing: 'easeOut',
				opacity: 1,
				from: {
					opacity: 0
				}
			})
			if (config.duration > 0) {
				m.fadeOut({
					duration: 200,
					delay: config.duration,
					scope: m,
					callback: this.removeElement
				});
			}
			return m;
		},
		removeElement: function() {
			if (this.hovered) return;
			if (this.configuration && this.configuration.callback) this.configuration.callback(this.configuration);
			this.remove();
		}
	};
}();
Ext.define('RS.common.controls.SysInfoButton', {
	extend: 'Ext.button.Button',
	alias: 'widget.sysinfobutton',

	ui: 'system-info-button-small',
	iconCls: 'rs-icon-button icon-info-sign',

	config: {
		dateFormat: 'c',
		sysId: '',
		sysCreated: '',
		sysCreatedBy: '',
		sysUpdated: '',
		sysUpdatedBy: '',
		sysOrg: '',
		wikiName: ''
	},

	initComponent: function() {
		if (Ext.isGecko) this.setIconCls(this.iconCls + ' rs-icon-firefox')
		this.tooltip = this.tooltip || RS.common.locale.sysInfoButtonTooltip
		this.handler = this.handleButtonClick
		this.overflowText = RS.common.locale.sysInfoButtonTitle
		this.callParent()
	},

	handleButtonClick: function(button) {
		var me = button;
		if (!button.popup) {
			button.popup = Ext.create('Ext.window.Window', {
				baseCls: Ext.isIE8m ? 'x-window' : 'x-panel',
				ui: Ext.isIE8m ? 'default' : 'sysinfo-dialog',
				draggable: false,
				height: 280,
				width: 400,
				title: RS.common.locale.sysInfoButtonTitle,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				bodyPadding: '10px',
				defaults: {
					labelWidth: 100
				},
				items: [{
						xtype: 'displayfield',
						fieldLabel: RS.common.locale.sysId,
						value: button.getSysId()
					}, {
						xtype: 'displayfield',
						fieldLabel: RS.common.locale.sysCreated,
						value: button.getParsedDate(button.getSysCreated())
					}, {
						xtype: 'displayfield',
						fieldLabel: RS.common.locale.sysCreatedBy,
						value: button.getSysCreatedBy()
					}, {
						xtype: 'displayfield',
						fieldLabel: RS.common.locale.sysUpdated,
						value: button.getParsedDate(button.getSysUpdated())
					}, {
						xtype: 'displayfield',
						fieldLabel: RS.common.locale.sysUpdatedBy,
						value: button.getSysUpdatedBy()
					}
					/*, {
					xtype: 'displayfield',
					fieldLabel: RS.common.locale.sysOrg,
					value: button.getSysOrg()
				}*/
				],				
				buttons: {
					xtype: 'toolbar',
					cls: 'sysinfo-toolbar-footer rs-dockedtoolbar',
					items: [{
						text: RS.common.locale.pageInfo,
						hidden: !button.getWikiName(),
						handler: function(button) {
							var model = glu.model({
								mtype: 'RS.wiki.PageInfo',
								name: me.getWikiName()
							});
							model.init()
							glu.openWindow(model)
							button.up('window').close()
						}
					}, {
						text: RS.common.locale.close,
						cls : 'rs-med-btn rs-btn-dark',
						handler: function(button) {
							button.up('window').close()
						}
					}]
				},
				listeners: {
					close: function() {
						button.popup = null;
					}
				}
			})
			button.popup.show()
			button.popup.alignTo(button.getEl().id, 'tr-br')
		} else {
			button.popup.close()
		}
	},

	getParsedDate: function(date) {
		var d = date,
			formats = ['time', 'c', this.getDateFormat()];

		if (!Ext.isDate(d)) {
			Ext.Array.each(formats, function(format) {
				d = Ext.Date.parse(d, format)
				return !Ext.isDate(d)
			})
		}

		if (Ext.isDate(d))
			return Ext.Date.format(d, this.getDateFormat())

		return RS.common.locale.unknownDate
	},

	onDestroy: function() {
		this.callParent()
		if (this.popup) this.popup.destroy()
	}
})
Ext.define('RS.common.grid.column.ViewColumn', {
	extend: 'Ext.grid.column.Action',
	alias: 'widget.viewcolumn',

	hideable: false,

	url: '',
	target: '_blank',
	eventName: 'showDetail',
	draggable: false,
	idField: 'sys_id',
	childLink: '',
	childLinkIdProperty: 'id',
	forceFire: false,

	constructor: function(config) {
		var me = this;
		Ext.applyIf(config, {
			items: [{				
				glyph : config.glyph || null,
				icon: '/resolve/images/common/detail.png',
				tooltip: config.viewTooltip || RS.common.locale.gotoDetailsColumnTooltip,
				handler: function(gridView, rowIndex, colIndex, item, e, rec) {
					var sys_id = rec ? rec.get(me.idField) : '';
					if (sys_id || this.forceFire) {
						if (me.target == '_self') gridView.ownerCt.fireEvent(me.eventName, gridView, sys_id || rec);
						else {
							//open new window to the url configured for this reference table
							var split = me.url.split('?'),
								params = {};
							if (split.length > 1) {
								params = Ext.Object.fromQueryString(split[1]);
							}
							window.open(split[0] + '?' + Ext.urlEncode(Ext.applyIf({
								sys_id: sys_id
							}, params)));
						}
					}
				}
			}]
		});

		me.callParent([config]);
	},
	/*defaultRenderer: function(v, meta, record, rowIdx, colIdx, store, view) {
		var me = this,
			prefix = Ext.baseCSSPrefix,
			scope = me.origScope || me,
			items = me.items,
			len = items.length,
			i = 0,
			item, ret, disabled, tooltip, location;

		// Allow a configured renderer to create initial value (And set the other values in the "metadata" argument!)
		// Assign a new variable here, since if we modify "v" it will also modify the arguments collection, meaning
		// we will pass an incorrect value to getClass/getTip
		ret = Ext.isFunction(me.origRenderer) ? me.origRenderer.apply(scope, arguments) || '' : '';

		location = window.location.href.split('#')[0];

		meta.tdCls += ' ' + Ext.baseCSSPrefix + 'action-col-cell';
		for (; i < len; i++) {
			item = items[i];

			disabled = item.disabled || (item.isDisabled ? item.isDisabled.call(item.scope || scope, view, rowIdx, colIdx, item, record) : false);
			tooltip = disabled ? null : (item.tooltip || (item.getTip ? item.getTip.apply(item.scope || scope, arguments) : null));

			// Only process the item action setup once.
			if (!item.hasActionConfiguration) {

				// Apply our documented default to all items
				item.stopSelection = me.stopSelection;
				item.disable = Ext.Function.bind(me.disableAction, me, [i], 0);
				item.enable = Ext.Function.bind(me.enableAction, me, [i], 0);
				item.hasActionConfiguration = true;
			}

			ret += (me.childLink ? '<a href="' + location + '#' + me.childLink + '/' + me.childLinkIdProperty + '=' + record.get(me.childLinkIdProperty) + '">' : '') +
				'<img alt="' + (item.altText || me.altText) + '" src="' + (item.icon || Ext.BLANK_IMAGE_URL) +
				'" class="' + prefix + 'action-col-icon ' + prefix + 'action-col-' + String(i) + ' ' + (disabled ? prefix + 'item-disabled' : ' ') +
				' ' + (Ext.isFunction(item.getClass) ? item.getClass.apply(item.scope || scope, arguments) : (item.iconCls || me.iconCls || '')) + '"' + (tooltip ? ' data-qtip="' + tooltip + '"' : '') + ' />' + (me.childLink ? '</a>' : '');
		}
		return ret;
	}*/
});
Ext.define('RS.formbuilder.viewer.desktop.fields.DateField', {

	extend: 'Ext.form.field.Date',
	alias: 'widget.xdatefield',

	submitFormat: 'c',
	onRender: function() {

		// call parent
		this.callParent(arguments);

		var name = this.name || this.el.dom.name;
		this.hiddenField = this.el.insertSibling({
			tag: 'input',
			type: 'hidden',
			name: name,
			value: this.formatHiddenDate(this.parseDate(this.value))
		});
		this.hiddenName = name;
		// otherwise field is not found by BasicForm::findField
		this.el.dom.removeAttribute('name');
		this.el.on({
			keyup: {
				scope: this,
				fn: this.updateHidden
			},
			blur: {
				scope: this,
				fn: this.updateHidden
			}
		}, Ext.isIE ? 'after' : 'before');
	},
	onDisable: function() {
		this.callParent(arguments);
		if (this.hiddenField) {
			this.hiddenField.dom.setAttribute('disabled', 'disabled');
		}
	},
	onEnable: function() {
		this.callParent(arguments);
		if (this.hiddenField) {
			this.hiddenField.dom.removeAttribute('disabled');
		}
	},
	formatHiddenDate: function(date) {
		if (!Ext.isDate(date)) {
			return date;
		}
		if ('timestamp' === this.submitFormat) {
			return date.getTime() / 1000;
		} else {
			return Ext.util.Format.date(date, this.submitFormat);
		}
	},
	updateHidden: function() {
		this.hiddenField.dom.value = this.formatHiddenDate(this.parseDate(this.getValue()));
	},
	parseDate: function(value) {
		if (Ext.isDate(value)) return value;
		var date = Ext.Date.parse(value, this.submitFormat);
		if (Ext.isDate(date)) return date;
		date = Ext.Date.parse(value, this.format);
		if (Ext.isDate(date)) return date;
		if (/^\d+$/g.test(value))
			value = parseInt(value);
		if (value) {
			value = new Date(value);
			if (!Ext.isNumber(value.getMilliseconds())) {
				this.invalidText = 'Invalid date value';
				return null;
			}
		}
		return value;
	}
});
Ext.define('RS.common.DateTimeField', {

	extend: 'Ext.form.FieldContainer',
	alias: 'widget.xdatetimefield',

	submitFormat: 'c',
	width: '100%',

	layout: {
		type: 'hbox',
		align: 'stretch'
	},

	initComponent: function() {
		this.items = [{
			xtype: 'datefield',
			itemId: 'datefield',
			value: this.value,
			readOnly: this.readOnly,
			fieldCls: this.fieldCls,
			flex: 1,
			listeners: {
				scope: this,
				change: this.dateTimeChanged
			}
		}, {
			xtype: 'timefield',
			itemId: 'timefield',
			padding: '0 0 0 5',
			value: this.value,
			fieldCls: this.fieldCls,
			readOnly: this.readOnly,
			flex: 1,
			listeners: {
				scope: this,
				change: this.dateTimeChanged
			}
		}, {
			xtype: 'hidden',
			name: this.name,
			submitFormat: this.submitFormat,
			value: this.formatHiddenDate(this.parseDate(this.value)),
			listeners: {
				change: function(panel, newValue, oldValue) {
					if (newValue) {
						var datetime = new Date(newValue);
						if (isNaN(datetime)) { // newValue is in milliseconds so convert to humnan readable
							datetime = new Date(parseInt(newValue));
							this.setValue(Ext.Date.format(datetime, this.submitFormat));
						}
						this.up().down('#datefield').setValue(datetime);
						this.up().down('#timefield').setValue(datetime);
					}
				}
			}
		}];

		if (this.readOnly) {
			delete this.items[0].flex;
			delete this.items[1].flex;
		}

		this.callParent();
	},
	dateTimeChanged: function() {
		if (!this.rendered) return

		var myDate = this.down('#datefield').getValue(),
			myTime = this.down('#timefield').getValue(),
			datetime = '';

		if (Ext.isDate(myDate) && Ext.isDate(myTime)) datetime = new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), myTime.getHours(), myTime.getMinutes(), myTime.getSeconds());
		else if (Ext.isDate(myDate)) {
			datetime = new Date();
			datetime.setFullYear(myDate.getFullYear());
			datetime.setMonth(myDate.getMonth());
			datetime.setDate(myDate.getDate());
		} else if (Ext.isDate(myTime)) {
			datetime = new Date();
			datetime.setHours(myTime.getHours());
			datetime.setMinutes(myTime.getMinutes());
			datetime.setSeconds(myTime.getSeconds());
		}

		var oldValue = this.down('hiddenfield').value;
		this.down('hiddenfield').setValue(Ext.Date.format(datetime, this.submitFormat));
		this.fireEvent('change', this, this.down('hiddenfield').getValue(), oldValue)
	},
	formatHiddenDate: function(date) {
		if (!Ext.isDate(date)) {
			return date;
		}
		if ('timestamp' === this.submitFormat) {
			return date.getTime() / 1000;
		} else {
			return Ext.util.Format.date(date, this.submitFormat);
		}
	},
	parseDate: function(value) {
		if (Ext.isDate(value)) return value;
		var date = Ext.Date.parse(value, this.submitFormat);
		if (Ext.isDate(date)) return date;
		date = Ext.Date.parse(value, this.format);
		if (Ext.isDate(date)) return date;
		if (/^\d+$/g.test(value))
			value = parseInt(value);
		if (value) {
			value = new Date(value);
			if (!Ext.isNumber(value.getMilliseconds())) {
				this.invalidText = 'Invalid date value';
				return null;
			}
		}
		return value;
	},
	setValue: function(value) {
		if (Ext.isDate(value)) {
			this.down('#datefield').setValue(value)
			this.down('#timefield').setValue(value)
		}

		if (value == '') {
			this.down('#datefield').setValue(null)
			this.down('#timefield').setValue(null)
		}
	},

	getValue: function() {
		return this.down('hiddenfield').getValue()
	}
});

glu.regAdapter('xdatetimefield', {
	extend: 'field'
});
Ext.define('RS.formbuilder.viewer.desktop.fields.TimeField', {

	extend: 'Ext.form.field.Time',
	alias: 'widget.xtimefield',

	submitFormat: 'c',
	onRender: function() {

		// call parent
		this.callParent(arguments);

		var name = this.name || this.el.dom.name;
		this.hiddenField = this.el.insertSibling({
			tag: 'input',
			type: 'hidden',
			name: name,
			value: this.formatHiddenDate(this.parseDate(this.value))
		});
		this.hiddenName = name;
		// otherwise field is not found by BasicForm::findField
		this.el.dom.removeAttribute('name');
		this.el.on({
			keyup: {
				scope: this,
				fn: this.updateHidden
			},
			blur: {
				scope: this,
				fn: this.updateHidden
			}
		}, Ext.isIE ? 'after' : 'before');
	},
	onDisable: function() {
		this.callParent(arguments);
		if (this.hiddenField) {
			this.hiddenField.dom.setAttribute('disabled', 'disabled');
		}
	},
	onEnable: function() {
		this.callParent(arguments);
		if (this.hiddenField) {
			this.hiddenField.dom.removeAttribute('disabled');
		}
	},
	formatHiddenDate: function(date) {
		if (!Ext.isDate(date)) {
			return date;
		}
		if ('timestamp' === this.submitFormat) {
			return date.getTime() / 1000;
		} else {
			return Ext.util.Format.date(date, this.submitFormat);
		}
	},
	updateHidden: function() {
		this.hiddenField.dom.value = this.formatHiddenDate(this.parseDate(this.getValue()));
	},
	parseDate: function(value) {
		if (Ext.isDate(value)) {
			var newDate = new Date();
			newDate.setHours(value.getHours());
			newDate.setMinutes(value.getMinutes());
			newDate.setSeconds(value.getSeconds());
			return newDate;
		}
		var date = Ext.Date.parse(value, this.submitFormat);
		if (Ext.isDate(date)) return date;
		date = Ext.Date.parse(value, this.format);
		if (Ext.isDate(date)) return date;
		if (/^\d+$/g.test(value))
			value = parseInt(value);
		if (value) {
			value = new Date(value);
			if (!Ext.isNumber(value.getMilliseconds())) {
				this.invalidText = 'Invalid date value';
				return null;
			}
		}
		return value;
	}
});
/*
CryptoJS v3.1
code.google.com/p/crypto-js
(c) 2009-2013 by Jeff Mott. All rights reserved.
code.google.com/p/crypto-js/wiki/License
*/
var CryptoJS=CryptoJS||function(u,p){var d={},l=d.lib={},s=function(){},t=l.Base={extend:function(a){s.prototype=this;var c=new s;a&&c.mixIn(a);c.hasOwnProperty("init")||(c.init=function(){c.$super.init.apply(this,arguments)});c.init.prototype=c;c.$super=this;return c},create:function(){var a=this.extend();a.init.apply(a,arguments);return a},init:function(){},mixIn:function(a){for(var c in a)a.hasOwnProperty(c)&&(this[c]=a[c]);a.hasOwnProperty("toString")&&(this.toString=a.toString)},clone:function(){return this.init.prototype.extend(this)}},
r=l.WordArray=t.extend({init:function(a,c){a=this.words=a||[];this.sigBytes=c!=p?c:4*a.length},toString:function(a){return(a||v).stringify(this)},concat:function(a){var c=this.words,e=a.words,j=this.sigBytes;a=a.sigBytes;this.clamp();if(j%4)for(var k=0;k<a;k++)c[j+k>>>2]|=(e[k>>>2]>>>24-8*(k%4)&255)<<24-8*((j+k)%4);else if(65535<e.length)for(k=0;k<a;k+=4)c[j+k>>>2]=e[k>>>2];else c.push.apply(c,e);this.sigBytes+=a;return this},clamp:function(){var a=this.words,c=this.sigBytes;a[c>>>2]&=4294967295<<
32-8*(c%4);a.length=u.ceil(c/4)},clone:function(){var a=t.clone.call(this);a.words=this.words.slice(0);return a},random:function(a){for(var c=[],e=0;e<a;e+=4)c.push(4294967296*u.random()|0);return new r.init(c,a)}}),w=d.enc={},v=w.Hex={stringify:function(a){var c=a.words;a=a.sigBytes;for(var e=[],j=0;j<a;j++){var k=c[j>>>2]>>>24-8*(j%4)&255;e.push((k>>>4).toString(16));e.push((k&15).toString(16))}return e.join("")},parse:function(a){for(var c=a.length,e=[],j=0;j<c;j+=2)e[j>>>3]|=parseInt(a.substr(j,
2),16)<<24-4*(j%8);return new r.init(e,c/2)}},b=w.Latin1={stringify:function(a){var c=a.words;a=a.sigBytes;for(var e=[],j=0;j<a;j++)e.push(String.fromCharCode(c[j>>>2]>>>24-8*(j%4)&255));return e.join("")},parse:function(a){for(var c=a.length,e=[],j=0;j<c;j++)e[j>>>2]|=(a.charCodeAt(j)&255)<<24-8*(j%4);return new r.init(e,c)}},x=w.Utf8={stringify:function(a){try{return decodeURIComponent(escape(b.stringify(a)))}catch(c){throw Error("Malformed UTF-8 data");}},parse:function(a){return b.parse(unescape(encodeURIComponent(a)))}},
q=l.BufferedBlockAlgorithm=t.extend({reset:function(){this._data=new r.init;this._nDataBytes=0},_append:function(a){"string"==typeof a&&(a=x.parse(a));this._data.concat(a);this._nDataBytes+=a.sigBytes},_process:function(a){var c=this._data,e=c.words,j=c.sigBytes,k=this.blockSize,b=j/(4*k),b=a?u.ceil(b):u.max((b|0)-this._minBufferSize,0);a=b*k;j=u.min(4*a,j);if(a){for(var q=0;q<a;q+=k)this._doProcessBlock(e,q);q=e.splice(0,a);c.sigBytes-=j}return new r.init(q,j)},clone:function(){var a=t.clone.call(this);
a._data=this._data.clone();return a},_minBufferSize:0});l.Hasher=q.extend({cfg:t.extend(),init:function(a){this.cfg=this.cfg.extend(a);this.reset()},reset:function(){q.reset.call(this);this._doReset()},update:function(a){this._append(a);this._process();return this},finalize:function(a){a&&this._append(a);return this._doFinalize()},blockSize:16,_createHelper:function(a){return function(b,e){return(new a.init(e)).finalize(b)}},_createHmacHelper:function(a){return function(b,e){return(new n.HMAC.init(a,
e)).finalize(b)}}});var n=d.algo={};return d}(Math);
(function(){var u=CryptoJS,p=u.lib.WordArray;u.enc.Base64={stringify:function(d){var l=d.words,p=d.sigBytes,t=this._map;d.clamp();d=[];for(var r=0;r<p;r+=3)for(var w=(l[r>>>2]>>>24-8*(r%4)&255)<<16|(l[r+1>>>2]>>>24-8*((r+1)%4)&255)<<8|l[r+2>>>2]>>>24-8*((r+2)%4)&255,v=0;4>v&&r+0.75*v<p;v++)d.push(t.charAt(w>>>6*(3-v)&63));if(l=t.charAt(64))for(;d.length%4;)d.push(l);return d.join("")},parse:function(d){var l=d.length,s=this._map,t=s.charAt(64);t&&(t=d.indexOf(t),-1!=t&&(l=t));for(var t=[],r=0,w=0;w<
l;w++)if(w%4){var v=s.indexOf(d.charAt(w-1))<<2*(w%4),b=s.indexOf(d.charAt(w))>>>6-2*(w%4);t[r>>>2]|=(v|b)<<24-8*(r%4);r++}return p.create(t,r)},_map:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="}})();
(function(u){function p(b,n,a,c,e,j,k){b=b+(n&a|~n&c)+e+k;return(b<<j|b>>>32-j)+n}function d(b,n,a,c,e,j,k){b=b+(n&c|a&~c)+e+k;return(b<<j|b>>>32-j)+n}function l(b,n,a,c,e,j,k){b=b+(n^a^c)+e+k;return(b<<j|b>>>32-j)+n}function s(b,n,a,c,e,j,k){b=b+(a^(n|~c))+e+k;return(b<<j|b>>>32-j)+n}for(var t=CryptoJS,r=t.lib,w=r.WordArray,v=r.Hasher,r=t.algo,b=[],x=0;64>x;x++)b[x]=4294967296*u.abs(u.sin(x+1))|0;r=r.MD5=v.extend({_doReset:function(){this._hash=new w.init([1732584193,4023233417,2562383102,271733878])},
_doProcessBlock:function(q,n){for(var a=0;16>a;a++){var c=n+a,e=q[c];q[c]=(e<<8|e>>>24)&16711935|(e<<24|e>>>8)&4278255360}var a=this._hash.words,c=q[n+0],e=q[n+1],j=q[n+2],k=q[n+3],z=q[n+4],r=q[n+5],t=q[n+6],w=q[n+7],v=q[n+8],A=q[n+9],B=q[n+10],C=q[n+11],u=q[n+12],D=q[n+13],E=q[n+14],x=q[n+15],f=a[0],m=a[1],g=a[2],h=a[3],f=p(f,m,g,h,c,7,b[0]),h=p(h,f,m,g,e,12,b[1]),g=p(g,h,f,m,j,17,b[2]),m=p(m,g,h,f,k,22,b[3]),f=p(f,m,g,h,z,7,b[4]),h=p(h,f,m,g,r,12,b[5]),g=p(g,h,f,m,t,17,b[6]),m=p(m,g,h,f,w,22,b[7]),
f=p(f,m,g,h,v,7,b[8]),h=p(h,f,m,g,A,12,b[9]),g=p(g,h,f,m,B,17,b[10]),m=p(m,g,h,f,C,22,b[11]),f=p(f,m,g,h,u,7,b[12]),h=p(h,f,m,g,D,12,b[13]),g=p(g,h,f,m,E,17,b[14]),m=p(m,g,h,f,x,22,b[15]),f=d(f,m,g,h,e,5,b[16]),h=d(h,f,m,g,t,9,b[17]),g=d(g,h,f,m,C,14,b[18]),m=d(m,g,h,f,c,20,b[19]),f=d(f,m,g,h,r,5,b[20]),h=d(h,f,m,g,B,9,b[21]),g=d(g,h,f,m,x,14,b[22]),m=d(m,g,h,f,z,20,b[23]),f=d(f,m,g,h,A,5,b[24]),h=d(h,f,m,g,E,9,b[25]),g=d(g,h,f,m,k,14,b[26]),m=d(m,g,h,f,v,20,b[27]),f=d(f,m,g,h,D,5,b[28]),h=d(h,f,
m,g,j,9,b[29]),g=d(g,h,f,m,w,14,b[30]),m=d(m,g,h,f,u,20,b[31]),f=l(f,m,g,h,r,4,b[32]),h=l(h,f,m,g,v,11,b[33]),g=l(g,h,f,m,C,16,b[34]),m=l(m,g,h,f,E,23,b[35]),f=l(f,m,g,h,e,4,b[36]),h=l(h,f,m,g,z,11,b[37]),g=l(g,h,f,m,w,16,b[38]),m=l(m,g,h,f,B,23,b[39]),f=l(f,m,g,h,D,4,b[40]),h=l(h,f,m,g,c,11,b[41]),g=l(g,h,f,m,k,16,b[42]),m=l(m,g,h,f,t,23,b[43]),f=l(f,m,g,h,A,4,b[44]),h=l(h,f,m,g,u,11,b[45]),g=l(g,h,f,m,x,16,b[46]),m=l(m,g,h,f,j,23,b[47]),f=s(f,m,g,h,c,6,b[48]),h=s(h,f,m,g,w,10,b[49]),g=s(g,h,f,m,
E,15,b[50]),m=s(m,g,h,f,r,21,b[51]),f=s(f,m,g,h,u,6,b[52]),h=s(h,f,m,g,k,10,b[53]),g=s(g,h,f,m,B,15,b[54]),m=s(m,g,h,f,e,21,b[55]),f=s(f,m,g,h,v,6,b[56]),h=s(h,f,m,g,x,10,b[57]),g=s(g,h,f,m,t,15,b[58]),m=s(m,g,h,f,D,21,b[59]),f=s(f,m,g,h,z,6,b[60]),h=s(h,f,m,g,C,10,b[61]),g=s(g,h,f,m,j,15,b[62]),m=s(m,g,h,f,A,21,b[63]);a[0]=a[0]+f|0;a[1]=a[1]+m|0;a[2]=a[2]+g|0;a[3]=a[3]+h|0},_doFinalize:function(){var b=this._data,n=b.words,a=8*this._nDataBytes,c=8*b.sigBytes;n[c>>>5]|=128<<24-c%32;var e=u.floor(a/
4294967296);n[(c+64>>>9<<4)+15]=(e<<8|e>>>24)&16711935|(e<<24|e>>>8)&4278255360;n[(c+64>>>9<<4)+14]=(a<<8|a>>>24)&16711935|(a<<24|a>>>8)&4278255360;b.sigBytes=4*(n.length+1);this._process();b=this._hash;n=b.words;for(a=0;4>a;a++)c=n[a],n[a]=(c<<8|c>>>24)&16711935|(c<<24|c>>>8)&4278255360;return b},clone:function(){var b=v.clone.call(this);b._hash=this._hash.clone();return b}});t.MD5=v._createHelper(r);t.HmacMD5=v._createHmacHelper(r)})(Math);
(function(){var u=CryptoJS,p=u.lib,d=p.Base,l=p.WordArray,p=u.algo,s=p.EvpKDF=d.extend({cfg:d.extend({keySize:4,hasher:p.MD5,iterations:1}),init:function(d){this.cfg=this.cfg.extend(d)},compute:function(d,r){for(var p=this.cfg,s=p.hasher.create(),b=l.create(),u=b.words,q=p.keySize,p=p.iterations;u.length<q;){n&&s.update(n);var n=s.update(d).finalize(r);s.reset();for(var a=1;a<p;a++)n=s.finalize(n),s.reset();b.concat(n)}b.sigBytes=4*q;return b}});u.EvpKDF=function(d,l,p){return s.create(p).compute(d,
l)}})();
CryptoJS.lib.Cipher||function(u){var p=CryptoJS,d=p.lib,l=d.Base,s=d.WordArray,t=d.BufferedBlockAlgorithm,r=p.enc.Base64,w=p.algo.EvpKDF,v=d.Cipher=t.extend({cfg:l.extend(),createEncryptor:function(e,a){return this.create(this._ENC_XFORM_MODE,e,a)},createDecryptor:function(e,a){return this.create(this._DEC_XFORM_MODE,e,a)},init:function(e,a,b){this.cfg=this.cfg.extend(b);this._xformMode=e;this._key=a;this.reset()},reset:function(){t.reset.call(this);this._doReset()},process:function(e){this._append(e);return this._process()},
finalize:function(e){e&&this._append(e);return this._doFinalize()},keySize:4,ivSize:4,_ENC_XFORM_MODE:1,_DEC_XFORM_MODE:2,_createHelper:function(e){return{encrypt:function(b,k,d){return("string"==typeof k?c:a).encrypt(e,b,k,d)},decrypt:function(b,k,d){return("string"==typeof k?c:a).decrypt(e,b,k,d)}}}});d.StreamCipher=v.extend({_doFinalize:function(){return this._process(!0)},blockSize:1});var b=p.mode={},x=function(e,a,b){var c=this._iv;c?this._iv=u:c=this._prevBlock;for(var d=0;d<b;d++)e[a+d]^=
c[d]},q=(d.BlockCipherMode=l.extend({createEncryptor:function(e,a){return this.Encryptor.create(e,a)},createDecryptor:function(e,a){return this.Decryptor.create(e,a)},init:function(e,a){this._cipher=e;this._iv=a}})).extend();q.Encryptor=q.extend({processBlock:function(e,a){var b=this._cipher,c=b.blockSize;x.call(this,e,a,c);b.encryptBlock(e,a);this._prevBlock=e.slice(a,a+c)}});q.Decryptor=q.extend({processBlock:function(e,a){var b=this._cipher,c=b.blockSize,d=e.slice(a,a+c);b.decryptBlock(e,a);x.call(this,
e,a,c);this._prevBlock=d}});b=b.CBC=q;q=(p.pad={}).Pkcs7={pad:function(a,b){for(var c=4*b,c=c-a.sigBytes%c,d=c<<24|c<<16|c<<8|c,l=[],n=0;n<c;n+=4)l.push(d);c=s.create(l,c);a.concat(c)},unpad:function(a){a.sigBytes-=a.words[a.sigBytes-1>>>2]&255}};d.BlockCipher=v.extend({cfg:v.cfg.extend({mode:b,padding:q}),reset:function(){v.reset.call(this);var a=this.cfg,b=a.iv,a=a.mode;if(this._xformMode==this._ENC_XFORM_MODE)var c=a.createEncryptor;else c=a.createDecryptor,this._minBufferSize=1;this._mode=c.call(a,
this,b&&b.words)},_doProcessBlock:function(a,b){this._mode.processBlock(a,b)},_doFinalize:function(){var a=this.cfg.padding;if(this._xformMode==this._ENC_XFORM_MODE){a.pad(this._data,this.blockSize);var b=this._process(!0)}else b=this._process(!0),a.unpad(b);return b},blockSize:4});var n=d.CipherParams=l.extend({init:function(a){this.mixIn(a)},toString:function(a){return(a||this.formatter).stringify(this)}}),b=(p.format={}).OpenSSL={stringify:function(a){var b=a.ciphertext;a=a.salt;return(a?s.create([1398893684,
1701076831]).concat(a).concat(b):b).toString(r)},parse:function(a){a=r.parse(a);var b=a.words;if(1398893684==b[0]&&1701076831==b[1]){var c=s.create(b.slice(2,4));b.splice(0,4);a.sigBytes-=16}return n.create({ciphertext:a,salt:c})}},a=d.SerializableCipher=l.extend({cfg:l.extend({format:b}),encrypt:function(a,b,c,d){d=this.cfg.extend(d);var l=a.createEncryptor(c,d);b=l.finalize(b);l=l.cfg;return n.create({ciphertext:b,key:c,iv:l.iv,algorithm:a,mode:l.mode,padding:l.padding,blockSize:a.blockSize,formatter:d.format})},
decrypt:function(a,b,c,d){d=this.cfg.extend(d);b=this._parse(b,d.format);return a.createDecryptor(c,d).finalize(b.ciphertext)},_parse:function(a,b){return"string"==typeof a?b.parse(a,this):a}}),p=(p.kdf={}).OpenSSL={execute:function(a,b,c,d){d||(d=s.random(8));a=w.create({keySize:b+c}).compute(a,d);c=s.create(a.words.slice(b),4*c);a.sigBytes=4*b;return n.create({key:a,iv:c,salt:d})}},c=d.PasswordBasedCipher=a.extend({cfg:a.cfg.extend({kdf:p}),encrypt:function(b,c,d,l){l=this.cfg.extend(l);d=l.kdf.execute(d,
b.keySize,b.ivSize);l.iv=d.iv;b=a.encrypt.call(this,b,c,d.key,l);b.mixIn(d);return b},decrypt:function(b,c,d,l){l=this.cfg.extend(l);c=this._parse(c,l.format);d=l.kdf.execute(d,b.keySize,b.ivSize,c.salt);l.iv=d.iv;return a.decrypt.call(this,b,c,d.key,l)}})}();
(function(){for(var u=CryptoJS,p=u.lib.BlockCipher,d=u.algo,l=[],s=[],t=[],r=[],w=[],v=[],b=[],x=[],q=[],n=[],a=[],c=0;256>c;c++)a[c]=128>c?c<<1:c<<1^283;for(var e=0,j=0,c=0;256>c;c++){var k=j^j<<1^j<<2^j<<3^j<<4,k=k>>>8^k&255^99;l[e]=k;s[k]=e;var z=a[e],F=a[z],G=a[F],y=257*a[k]^16843008*k;t[e]=y<<24|y>>>8;r[e]=y<<16|y>>>16;w[e]=y<<8|y>>>24;v[e]=y;y=16843009*G^65537*F^257*z^16843008*e;b[k]=y<<24|y>>>8;x[k]=y<<16|y>>>16;q[k]=y<<8|y>>>24;n[k]=y;e?(e=z^a[a[a[G^z]]],j^=a[a[j]]):e=j=1}var H=[0,1,2,4,8,
16,32,64,128,27,54],d=d.AES=p.extend({_doReset:function(){for(var a=this._key,c=a.words,d=a.sigBytes/4,a=4*((this._nRounds=d+6)+1),e=this._keySchedule=[],j=0;j<a;j++)if(j<d)e[j]=c[j];else{var k=e[j-1];j%d?6<d&&4==j%d&&(k=l[k>>>24]<<24|l[k>>>16&255]<<16|l[k>>>8&255]<<8|l[k&255]):(k=k<<8|k>>>24,k=l[k>>>24]<<24|l[k>>>16&255]<<16|l[k>>>8&255]<<8|l[k&255],k^=H[j/d|0]<<24);e[j]=e[j-d]^k}c=this._invKeySchedule=[];for(d=0;d<a;d++)j=a-d,k=d%4?e[j]:e[j-4],c[d]=4>d||4>=j?k:b[l[k>>>24]]^x[l[k>>>16&255]]^q[l[k>>>
8&255]]^n[l[k&255]]},encryptBlock:function(a,b){this._doCryptBlock(a,b,this._keySchedule,t,r,w,v,l)},decryptBlock:function(a,c){var d=a[c+1];a[c+1]=a[c+3];a[c+3]=d;this._doCryptBlock(a,c,this._invKeySchedule,b,x,q,n,s);d=a[c+1];a[c+1]=a[c+3];a[c+3]=d},_doCryptBlock:function(a,b,c,d,e,j,l,f){for(var m=this._nRounds,g=a[b]^c[0],h=a[b+1]^c[1],k=a[b+2]^c[2],n=a[b+3]^c[3],p=4,r=1;r<m;r++)var q=d[g>>>24]^e[h>>>16&255]^j[k>>>8&255]^l[n&255]^c[p++],s=d[h>>>24]^e[k>>>16&255]^j[n>>>8&255]^l[g&255]^c[p++],t=
d[k>>>24]^e[n>>>16&255]^j[g>>>8&255]^l[h&255]^c[p++],n=d[n>>>24]^e[g>>>16&255]^j[h>>>8&255]^l[k&255]^c[p++],g=q,h=s,k=t;q=(f[g>>>24]<<24|f[h>>>16&255]<<16|f[k>>>8&255]<<8|f[n&255])^c[p++];s=(f[h>>>24]<<24|f[k>>>16&255]<<16|f[n>>>8&255]<<8|f[g&255])^c[p++];t=(f[k>>>24]<<24|f[n>>>16&255]<<16|f[g>>>8&255]<<8|f[h&255])^c[p++];n=(f[n>>>24]<<24|f[g>>>16&255]<<16|f[h>>>8&255]<<8|f[k&255])^c[p++];a[b]=q;a[b+1]=s;a[b+2]=t;a[b+3]=n},keySize:8});u.AES=p._createHelper(d)})();

//Patch CryptoJS to support aes 128-bit
CryptoJS.algo.AES128 = CryptoJS.algo.AES.extend({
	keySize: 256 / 32
});
CryptoJS.AES128 = CryptoJS.lib.Cipher._createHelper(CryptoJS.algo.AES128);

function decryptText(elementId, encodedValue, iv) {
	Ext.Msg.prompt(RS.common.locale.decriptionTitle, RS.common.locale.decriptionMessage, CryptoDecrypt, {
		elementId: elementId,
		encodedValue: encodedValue,
		iv: iv
	});
}

function CryptoDecrypt(btn, prompt) {
	if (btn == 'ok') {
		try {
            var salt = this.encodedValue.split("$")[0],
			key = CryptoJS.PBKDF2(prompt, CryptoJS.enc.Utf8.parse(salt), {
				keySize: 256 / 32,
				iterations: 1024
			}),
				iv = CryptoJS.enc.Base64.parse(this.iv),
				enc = this.encodedValue.split("$")[1],
				decoded = CryptoJS.AES128.decrypt(enc, key, {
					iv: iv
				}),
				decodedString = CryptoJS.enc.Utf8.stringify(decoded);
			if (decodedString) {
				Ext.fly(this.elementId).update(Ext.String.htmlEncode(decodedString));
			} else {
				Ext.Msg.alert(RS.common.locale.invalidKeyTitle, RS.common.locale.invalidKeyMessage);
			}
		} catch (e) {
			Ext.Msg.alert(RS.common.locale.decryptErrorTitle, RS.common.locale.decryptErrorMessage);
		}
	}
}

/*
CryptoJS v3.1
code.google.com/p/crypto-js
(c) 2009-2013 by Jeff Mott. All rights reserved.
code.google.com/p/crypto-js/wiki/License
*/
var CryptoJS=CryptoJS||function(g,j){var e={},d=e.lib={},m=function(){},n=d.Base={extend:function(a){m.prototype=this;var c=new m;a&&c.mixIn(a);c.hasOwnProperty("init")||(c.init=function(){c.$super.init.apply(this,arguments)});c.init.prototype=c;c.$super=this;return c},create:function(){var a=this.extend();a.init.apply(a,arguments);return a},init:function(){},mixIn:function(a){for(var c in a)a.hasOwnProperty(c)&&(this[c]=a[c]);a.hasOwnProperty("toString")&&(this.toString=a.toString)},clone:function(){return this.init.prototype.extend(this)}},
q=d.WordArray=n.extend({init:function(a,c){a=this.words=a||[];this.sigBytes=c!=j?c:4*a.length},toString:function(a){return(a||l).stringify(this)},concat:function(a){var c=this.words,p=a.words,f=this.sigBytes;a=a.sigBytes;this.clamp();if(f%4)for(var b=0;b<a;b++)c[f+b>>>2]|=(p[b>>>2]>>>24-8*(b%4)&255)<<24-8*((f+b)%4);else if(65535<p.length)for(b=0;b<a;b+=4)c[f+b>>>2]=p[b>>>2];else c.push.apply(c,p);this.sigBytes+=a;return this},clamp:function(){var a=this.words,c=this.sigBytes;a[c>>>2]&=4294967295<<
32-8*(c%4);a.length=g.ceil(c/4)},clone:function(){var a=n.clone.call(this);a.words=this.words.slice(0);return a},random:function(a){for(var c=[],b=0;b<a;b+=4)c.push(4294967296*g.random()|0);return new q.init(c,a)}}),b=e.enc={},l=b.Hex={stringify:function(a){var c=a.words;a=a.sigBytes;for(var b=[],f=0;f<a;f++){var d=c[f>>>2]>>>24-8*(f%4)&255;b.push((d>>>4).toString(16));b.push((d&15).toString(16))}return b.join("")},parse:function(a){for(var c=a.length,b=[],f=0;f<c;f+=2)b[f>>>3]|=parseInt(a.substr(f,
2),16)<<24-4*(f%8);return new q.init(b,c/2)}},k=b.Latin1={stringify:function(a){var c=a.words;a=a.sigBytes;for(var b=[],f=0;f<a;f++)b.push(String.fromCharCode(c[f>>>2]>>>24-8*(f%4)&255));return b.join("")},parse:function(a){for(var c=a.length,b=[],f=0;f<c;f++)b[f>>>2]|=(a.charCodeAt(f)&255)<<24-8*(f%4);return new q.init(b,c)}},h=b.Utf8={stringify:function(a){try{return decodeURIComponent(escape(k.stringify(a)))}catch(b){throw Error("Malformed UTF-8 data");}},parse:function(a){return k.parse(unescape(encodeURIComponent(a)))}},
u=d.BufferedBlockAlgorithm=n.extend({reset:function(){this._data=new q.init;this._nDataBytes=0},_append:function(a){"string"==typeof a&&(a=h.parse(a));this._data.concat(a);this._nDataBytes+=a.sigBytes},_process:function(a){var b=this._data,d=b.words,f=b.sigBytes,l=this.blockSize,e=f/(4*l),e=a?g.ceil(e):g.max((e|0)-this._minBufferSize,0);a=e*l;f=g.min(4*a,f);if(a){for(var h=0;h<a;h+=l)this._doProcessBlock(d,h);h=d.splice(0,a);b.sigBytes-=f}return new q.init(h,f)},clone:function(){var a=n.clone.call(this);
a._data=this._data.clone();return a},_minBufferSize:0});d.Hasher=u.extend({cfg:n.extend(),init:function(a){this.cfg=this.cfg.extend(a);this.reset()},reset:function(){u.reset.call(this);this._doReset()},update:function(a){this._append(a);this._process();return this},finalize:function(a){a&&this._append(a);return this._doFinalize()},blockSize:16,_createHelper:function(a){return function(b,d){return(new a.init(d)).finalize(b)}},_createHmacHelper:function(a){return function(b,d){return(new w.HMAC.init(a,
d)).finalize(b)}}});var w=e.algo={};return e}(Math);
(function(){var g=CryptoJS,j=g.lib,e=j.WordArray,d=j.Hasher,m=[],j=g.algo.SHA1=d.extend({_doReset:function(){this._hash=new e.init([1732584193,4023233417,2562383102,271733878,3285377520])},_doProcessBlock:function(d,e){for(var b=this._hash.words,l=b[0],k=b[1],h=b[2],g=b[3],j=b[4],a=0;80>a;a++){if(16>a)m[a]=d[e+a]|0;else{var c=m[a-3]^m[a-8]^m[a-14]^m[a-16];m[a]=c<<1|c>>>31}c=(l<<5|l>>>27)+j+m[a];c=20>a?c+((k&h|~k&g)+1518500249):40>a?c+((k^h^g)+1859775393):60>a?c+((k&h|k&g|h&g)-1894007588):c+((k^h^
g)-899497514);j=g;g=h;h=k<<30|k>>>2;k=l;l=c}b[0]=b[0]+l|0;b[1]=b[1]+k|0;b[2]=b[2]+h|0;b[3]=b[3]+g|0;b[4]=b[4]+j|0},_doFinalize:function(){var d=this._data,e=d.words,b=8*this._nDataBytes,l=8*d.sigBytes;e[l>>>5]|=128<<24-l%32;e[(l+64>>>9<<4)+14]=Math.floor(b/4294967296);e[(l+64>>>9<<4)+15]=b;d.sigBytes=4*e.length;this._process();return this._hash},clone:function(){var e=d.clone.call(this);e._hash=this._hash.clone();return e}});g.SHA1=d._createHelper(j);g.HmacSHA1=d._createHmacHelper(j)})();
(function(){var g=CryptoJS,j=g.enc.Utf8;g.algo.HMAC=g.lib.Base.extend({init:function(e,d){e=this._hasher=new e.init;"string"==typeof d&&(d=j.parse(d));var g=e.blockSize,n=4*g;d.sigBytes>n&&(d=e.finalize(d));d.clamp();for(var q=this._oKey=d.clone(),b=this._iKey=d.clone(),l=q.words,k=b.words,h=0;h<g;h++)l[h]^=1549556828,k[h]^=909522486;q.sigBytes=b.sigBytes=n;this.reset()},reset:function(){var e=this._hasher;e.reset();e.update(this._iKey)},update:function(e){this._hasher.update(e);return this},finalize:function(e){var d=
this._hasher;e=d.finalize(e);d.reset();return d.finalize(this._oKey.clone().concat(e))}})})();
(function(){var g=CryptoJS,j=g.lib,e=j.Base,d=j.WordArray,j=g.algo,m=j.HMAC,n=j.PBKDF2=e.extend({cfg:e.extend({keySize:4,hasher:j.SHA1,iterations:1}),init:function(d){this.cfg=this.cfg.extend(d)},compute:function(e,b){for(var g=this.cfg,k=m.create(g.hasher,e),h=d.create(),j=d.create([1]),n=h.words,a=j.words,c=g.keySize,g=g.iterations;n.length<c;){var p=k.update(b).finalize(j);k.reset();for(var f=p.words,v=f.length,s=p,t=1;t<g;t++){s=k.finalize(s);k.reset();for(var x=s.words,r=0;r<v;r++)f[r]^=x[r]}h.concat(p);
a[0]++}h.sigBytes=4*c;return h}});g.PBKDF2=function(d,b,e){return n.create(e).compute(d,b)}})();

/*
CryptoJS v3.1.2
code.google.com/p/crypto-js
(c) 2009-2013 by Jeff Mott. All rights reserved.
code.google.com/p/crypto-js/wiki/License
*/
var CryptoJS=CryptoJS||function(a,m){var r={},f=r.lib={},g=function(){},l=f.Base={extend:function(a){g.prototype=this;var b=new g;a&&b.mixIn(a);b.hasOwnProperty("init")||(b.init=function(){b.$super.init.apply(this,arguments)});b.init.prototype=b;b.$super=this;return b},create:function(){var a=this.extend();a.init.apply(a,arguments);return a},init:function(){},mixIn:function(a){for(var b in a)a.hasOwnProperty(b)&&(this[b]=a[b]);a.hasOwnProperty("toString")&&(this.toString=a.toString)},clone:function(){return this.init.prototype.extend(this)}},
p=f.WordArray=l.extend({init:function(a,b){a=this.words=a||[];this.sigBytes=b!=m?b:4*a.length},toString:function(a){return(a||q).stringify(this)},concat:function(a){var b=this.words,d=a.words,c=this.sigBytes;a=a.sigBytes;this.clamp();if(c%4)for(var j=0;j<a;j++)b[c+j>>>2]|=(d[j>>>2]>>>24-8*(j%4)&255)<<24-8*((c+j)%4);else if(65535<d.length)for(j=0;j<a;j+=4)b[c+j>>>2]=d[j>>>2];else b.push.apply(b,d);this.sigBytes+=a;return this},clamp:function(){var n=this.words,b=this.sigBytes;n[b>>>2]&=4294967295<<
32-8*(b%4);n.length=a.ceil(b/4)},clone:function(){var a=l.clone.call(this);a.words=this.words.slice(0);return a},random:function(n){for(var b=[],d=0;d<n;d+=4)b.push(4294967296*a.random()|0);return new p.init(b,n)}}),y=r.enc={},q=y.Hex={stringify:function(a){var b=a.words;a=a.sigBytes;for(var d=[],c=0;c<a;c++){var j=b[c>>>2]>>>24-8*(c%4)&255;d.push((j>>>4).toString(16));d.push((j&15).toString(16))}return d.join("")},parse:function(a){for(var b=a.length,d=[],c=0;c<b;c+=2)d[c>>>3]|=parseInt(a.substr(c,
2),16)<<24-4*(c%8);return new p.init(d,b/2)}},G=y.Latin1={stringify:function(a){var b=a.words;a=a.sigBytes;for(var d=[],c=0;c<a;c++)d.push(String.fromCharCode(b[c>>>2]>>>24-8*(c%4)&255));return d.join("")},parse:function(a){for(var b=a.length,d=[],c=0;c<b;c++)d[c>>>2]|=(a.charCodeAt(c)&255)<<24-8*(c%4);return new p.init(d,b)}},fa=y.Utf8={stringify:function(a){try{return decodeURIComponent(escape(G.stringify(a)))}catch(b){throw Error("Malformed UTF-8 data");}},parse:function(a){return G.parse(unescape(encodeURIComponent(a)))}},
h=f.BufferedBlockAlgorithm=l.extend({reset:function(){this._data=new p.init;this._nDataBytes=0},_append:function(a){"string"==typeof a&&(a=fa.parse(a));this._data.concat(a);this._nDataBytes+=a.sigBytes},_process:function(n){var b=this._data,d=b.words,c=b.sigBytes,j=this.blockSize,l=c/(4*j),l=n?a.ceil(l):a.max((l|0)-this._minBufferSize,0);n=l*j;c=a.min(4*n,c);if(n){for(var h=0;h<n;h+=j)this._doProcessBlock(d,h);h=d.splice(0,n);b.sigBytes-=c}return new p.init(h,c)},clone:function(){var a=l.clone.call(this);
a._data=this._data.clone();return a},_minBufferSize:0});f.Hasher=h.extend({cfg:l.extend(),init:function(a){this.cfg=this.cfg.extend(a);this.reset()},reset:function(){h.reset.call(this);this._doReset()},update:function(a){this._append(a);this._process();return this},finalize:function(a){a&&this._append(a);return this._doFinalize()},blockSize:16,_createHelper:function(a){return function(b,d){return(new a.init(d)).finalize(b)}},_createHmacHelper:function(a){return function(b,d){return(new ga.HMAC.init(a,
d)).finalize(b)}}});var ga=r.algo={};return r}(Math);
(function(a){var m=CryptoJS,r=m.lib,f=r.Base,g=r.WordArray,m=m.x64={};m.Word=f.extend({init:function(a,p){this.high=a;this.low=p}});m.WordArray=f.extend({init:function(l,p){l=this.words=l||[];this.sigBytes=p!=a?p:8*l.length},toX32:function(){for(var a=this.words,p=a.length,f=[],q=0;q<p;q++){var G=a[q];f.push(G.high);f.push(G.low)}return g.create(f,this.sigBytes)},clone:function(){for(var a=f.clone.call(this),p=a.words=this.words.slice(0),g=p.length,q=0;q<g;q++)p[q]=p[q].clone();return a}})})();
(function(){function a(){return g.create.apply(g,arguments)}for(var m=CryptoJS,r=m.lib.Hasher,f=m.x64,g=f.Word,l=f.WordArray,f=m.algo,p=[a(1116352408,3609767458),a(1899447441,602891725),a(3049323471,3964484399),a(3921009573,2173295548),a(961987163,4081628472),a(1508970993,3053834265),a(2453635748,2937671579),a(2870763221,3664609560),a(3624381080,2734883394),a(310598401,1164996542),a(607225278,1323610764),a(1426881987,3590304994),a(1925078388,4068182383),a(2162078206,991336113),a(2614888103,633803317),
a(3248222580,3479774868),a(3835390401,2666613458),a(4022224774,944711139),a(264347078,2341262773),a(604807628,2007800933),a(770255983,1495990901),a(1249150122,1856431235),a(1555081692,3175218132),a(1996064986,2198950837),a(2554220882,3999719339),a(2821834349,766784016),a(2952996808,2566594879),a(3210313671,3203337956),a(3336571891,1034457026),a(3584528711,2466948901),a(113926993,3758326383),a(338241895,168717936),a(666307205,1188179964),a(773529912,1546045734),a(1294757372,1522805485),a(1396182291,
2643833823),a(1695183700,2343527390),a(1986661051,1014477480),a(2177026350,1206759142),a(2456956037,344077627),a(2730485921,1290863460),a(2820302411,3158454273),a(3259730800,3505952657),a(3345764771,106217008),a(3516065817,3606008344),a(3600352804,1432725776),a(4094571909,1467031594),a(275423344,851169720),a(430227734,3100823752),a(506948616,1363258195),a(659060556,3750685593),a(883997877,3785050280),a(958139571,3318307427),a(1322822218,3812723403),a(1537002063,2003034995),a(1747873779,3602036899),
a(1955562222,1575990012),a(2024104815,1125592928),a(2227730452,2716904306),a(2361852424,442776044),a(2428436474,593698344),a(2756734187,3733110249),a(3204031479,2999351573),a(3329325298,3815920427),a(3391569614,3928383900),a(3515267271,566280711),a(3940187606,3454069534),a(4118630271,4000239992),a(116418474,1914138554),a(174292421,2731055270),a(289380356,3203993006),a(460393269,320620315),a(685471733,587496836),a(852142971,1086792851),a(1017036298,365543100),a(1126000580,2618297676),a(1288033470,
3409855158),a(1501505948,4234509866),a(1607167915,987167468),a(1816402316,1246189591)],y=[],q=0;80>q;q++)y[q]=a();f=f.SHA512=r.extend({_doReset:function(){this._hash=new l.init([new g.init(1779033703,4089235720),new g.init(3144134277,2227873595),new g.init(1013904242,4271175723),new g.init(2773480762,1595750129),new g.init(1359893119,2917565137),new g.init(2600822924,725511199),new g.init(528734635,4215389547),new g.init(1541459225,327033209)])},_doProcessBlock:function(a,f){for(var h=this._hash.words,
g=h[0],n=h[1],b=h[2],d=h[3],c=h[4],j=h[5],l=h[6],h=h[7],q=g.high,m=g.low,r=n.high,N=n.low,Z=b.high,O=b.low,$=d.high,P=d.low,aa=c.high,Q=c.low,ba=j.high,R=j.low,ca=l.high,S=l.low,da=h.high,T=h.low,v=q,s=m,H=r,E=N,I=Z,F=O,W=$,J=P,w=aa,t=Q,U=ba,K=R,V=ca,L=S,X=da,M=T,x=0;80>x;x++){var B=y[x];if(16>x)var u=B.high=a[f+2*x]|0,e=B.low=a[f+2*x+1]|0;else{var u=y[x-15],e=u.high,z=u.low,u=(e>>>1|z<<31)^(e>>>8|z<<24)^e>>>7,z=(z>>>1|e<<31)^(z>>>8|e<<24)^(z>>>7|e<<25),D=y[x-2],e=D.high,k=D.low,D=(e>>>19|k<<13)^
(e<<3|k>>>29)^e>>>6,k=(k>>>19|e<<13)^(k<<3|e>>>29)^(k>>>6|e<<26),e=y[x-7],Y=e.high,C=y[x-16],A=C.high,C=C.low,e=z+e.low,u=u+Y+(e>>>0<z>>>0?1:0),e=e+k,u=u+D+(e>>>0<k>>>0?1:0),e=e+C,u=u+A+(e>>>0<C>>>0?1:0);B.high=u;B.low=e}var Y=w&U^~w&V,C=t&K^~t&L,B=v&H^v&I^H&I,ha=s&E^s&F^E&F,z=(v>>>28|s<<4)^(v<<30|s>>>2)^(v<<25|s>>>7),D=(s>>>28|v<<4)^(s<<30|v>>>2)^(s<<25|v>>>7),k=p[x],ia=k.high,ea=k.low,k=M+((t>>>14|w<<18)^(t>>>18|w<<14)^(t<<23|w>>>9)),A=X+((w>>>14|t<<18)^(w>>>18|t<<14)^(w<<23|t>>>9))+(k>>>0<M>>>
0?1:0),k=k+C,A=A+Y+(k>>>0<C>>>0?1:0),k=k+ea,A=A+ia+(k>>>0<ea>>>0?1:0),k=k+e,A=A+u+(k>>>0<e>>>0?1:0),e=D+ha,B=z+B+(e>>>0<D>>>0?1:0),X=V,M=L,V=U,L=K,U=w,K=t,t=J+k|0,w=W+A+(t>>>0<J>>>0?1:0)|0,W=I,J=F,I=H,F=E,H=v,E=s,s=k+e|0,v=A+B+(s>>>0<k>>>0?1:0)|0}m=g.low=m+s;g.high=q+v+(m>>>0<s>>>0?1:0);N=n.low=N+E;n.high=r+H+(N>>>0<E>>>0?1:0);O=b.low=O+F;b.high=Z+I+(O>>>0<F>>>0?1:0);P=d.low=P+J;d.high=$+W+(P>>>0<J>>>0?1:0);Q=c.low=Q+t;c.high=aa+w+(Q>>>0<t>>>0?1:0);R=j.low=R+K;j.high=ba+U+(R>>>0<K>>>0?1:0);S=l.low=
S+L;l.high=ca+V+(S>>>0<L>>>0?1:0);T=h.low=T+M;h.high=da+X+(T>>>0<M>>>0?1:0)},_doFinalize:function(){var a=this._data,f=a.words,h=8*this._nDataBytes,g=8*a.sigBytes;f[g>>>5]|=128<<24-g%32;f[(g+128>>>10<<5)+30]=Math.floor(h/4294967296);f[(g+128>>>10<<5)+31]=h;a.sigBytes=4*f.length;this._process();return this._hash.toX32()},clone:function(){var a=r.clone.call(this);a._hash=this._hash.clone();return a},blockSize:32});m.SHA512=r._createHelper(f);m.HmacSHA512=r._createHmacHelper(f)})();

Ext.define('RS.common.data.AccessStore', (function () {
	function shapeData(accessRights) {
		var data = [];

		if (accessRights) {
			var	readUsers = accessRights.ureadAccess.length > 0 ? accessRights.ureadAccess.split(',') : [],
				writeUsers = accessRights.uwriteAccess.length > 0 ? accessRights.uwriteAccess.split(',') : [],
				executeUsers = accessRights.uexecuteAccess.length > 0 ? accessRights.uexecuteAccess.split(',') : [],
				adminUsers = accessRights.uadminAccess.length > 0 ? accessRights.uadminAccess.split(',') : [],
				users = readUsers.slice();
			consolidateArrays(users, writeUsers);
			consolidateArrays(users, executeUsers);
			consolidateArrays(users, adminUsers);

			for (var i = 0; i < users.length; i++) {
				var u = users[i];
				data.push({
					uname: u,
					read: readUsers.indexOf(u) !== -1,
					write: writeUsers.indexOf(u) !== -1,
					execute: executeUsers.indexOf(u) !== -1,
					admin: adminUsers.indexOf(u) !== -1
				});
			}
		}

		return data;
	}

	function consolidateArrays(destination, source) {
		for (var i = 0; i < source.length; i++) {
			if (destination.indexOf(source[i]) === -1) {
				destination.push(source[i]);
			}
		}
	}

	return {
		extend: 'Ext.data.Store',
		config: {
			fields: ['uname', 'read', 'write', 'execute', 'admin'],
			listeners: {},
			data: []
		},

		constructor: function(listeners) {
			var config = this.config;

			if (typeof listeners === 'object') {
				for (var key in listeners) {
					config.listeners[key] = listeners[key];
				}
			}

			this.callParent([config]);
		},

		loadData: function (accessRights, append) {
			var data;

			if (accessRights.hasOwnProperty('ureadAccess')) {
				data = shapeData(accessRights);
			} else {
				data = accessRights;
			}
			
			this.callParent([data, append]);
			return this;
		},

		getAccessRights: function (dataIsForServer) {
			var readRoles = [], writeRoles = [], executeRoles = [], adminRoles = [],
				range = this.getRange();

			for (var i = 0; i < range.length; i++) {
				var d = range[i].data;
				var user = d.uname;

				if (d.read) {
					readRoles.push(user);
				}

				if (d.write) {

					writeRoles.push(user);
				}

				if (d.execute) {
					executeRoles.push(user);
				}

				if (d.admin) {
					adminRoles.push(user);
				}
			}

			var accessRights = {
				ureadAccess: readRoles.join(','),
				uwriteAccess: writeRoles.join(','),
				uexecuteAccess: executeRoles.join(','),
				uadminAccess: adminRoles.join(',')
			};

			if (!dataIsForServer) {
				var isAdmin = false, canEdit = false, canExecute = false, currentUserRoles = clientVM.user.roles;

				if (typeof currentUserRoles === 'undefined' || currentUserRoles === null) {
					isAdmin = true;
					canEdit = true;
					canExecute = true;
				} else {
					for (var i = 0; i < currentUserRoles.length; i++) {
						isAdmin = adminRoles.indexOf(currentUserRoles[i]) >= 0;

						if (!canEdit) {
							canEdit = isAdmin || writeRoles.indexOf(currentUserRoles[i]) >= 0;
						}

						if (!canExecute) {
							canExecute = isAdmin || executeRoles.indexOf(currentUserRoles[i]) >= 0;
						}

						if (canEdit && canExecute) {
							break;
						}
					}

					for (var i = 0; i < adminRoles.length; i++) {
						isAdmin = currentUserRoles.indexOf(adminRoles[i]) >= 0;

						if (isAdmin) {
							canEdit = true;
							canExecute = false;					
							break;
						}
					}
				}

				if (!isAdmin) {
					for (var i = 0; i < writeRoles.length; i++) {
						canEdit = currentUserRoles.indexOf(writeRoles[i]) >= 0;

						if (canEdit) {
							break;
						}
					}

					for (var i = 0; i < executeRoles.length; i++) {
						canExecute = currentUserRoles.indexOf(executeRoles[i]) >= 0;

						if (canExecute) {
							break;
						}
					}
				}

				accessRights.canEdit = canEdit;
				accessRights.canExecute = canExecute;
			}

			return accessRights;

		},

		getRoles: function () {
			var range = this.getRange();
			var roles = [];

			for (var i = 0; i < range.length; i++) {
				roles.push(range[i].data.uname);
			}

			return roles;
		}
	};
})());
Ext.namespace('RS.common.data').countries = [{
	name: 'Afghanistan',
	value: 'AF'
}, {
	name: 'Åland Islands',
	value: 'AX'
}, {
	name: 'Albania',
	value: 'AL'
}, {
	name: 'Algeria',
	value: 'DZ'
}, {
	name: 'American Samoa',
	value: 'AS'
}, {
	name: 'AndorrA',
	value: 'AD'
}, {
	name: 'Angola',
	value: 'AO'
}, {
	name: 'Anguilla',
	value: 'AI'
}, {
	name: 'Antarctica',
	value: 'AQ'
}, {
	name: 'Antigua and Barbuda',
	value: 'AG'
}, {
	name: 'Argentina',
	value: 'AR'
}, {
	name: 'Armenia',
	value: 'AM'
}, {
	name: 'Aruba',
	value: 'AW'
}, {
	name: 'Australia',
	value: 'AU'
}, {
	name: 'Austria',
	value: 'AT'
}, {
	name: 'Azerbaijan',
	value: 'AZ'
}, {
	name: 'Bahamas',
	value: 'BS'
}, {
	name: 'Bahrain',
	value: 'BH'
}, {
	name: 'Bangladesh',
	value: 'BD'
}, {
	name: 'Barbados',
	value: 'BB'
}, {
	name: 'Belarus',
	value: 'BY'
}, {
	name: 'Belgium',
	value: 'BE'
}, {
	name: 'Belize',
	value: 'BZ'
}, {
	name: 'Benin',
	value: 'BJ'
}, {
	name: 'Bermuda',
	value: 'BM'
}, {
	name: 'Bhutan',
	value: 'BT'
}, {
	name: 'Bolivia',
	value: 'BO'
}, {
	name: 'Bosnia and Herzegovina',
	value: 'BA'
}, {
	name: 'Botswana',
	value: 'BW'
}, {
	name: 'Bouvet Island',
	value: 'BV'
}, {
	name: 'Brazil',
	value: 'BR'
}, {
	name: 'British Indian Ocean Territory',
	value: 'IO'
}, {
	name: 'Brunei Darussalam',
	value: 'BN'
}, {
	name: 'Bulgaria',
	value: 'BG'
}, {
	name: 'Burkina Faso',
	value: 'BF'
}, {
	name: 'Burundi',
	value: 'BI'
}, {
	name: 'Cambodia',
	value: 'KH'
}, {
	name: 'Cameroon',
	value: 'CM'
}, {
	name: 'Canada',
	value: 'CA'
}, {
	name: 'Cape Verde',
	value: 'CV'
}, {
	name: 'Cayman Islands',
	value: 'KY'
}, {
	name: 'Central African Republic',
	value: 'CF'
}, {
	name: 'Chad',
	value: 'TD'
}, {
	name: 'Chile',
	value: 'CL'
}, {
	name: 'China',
	value: 'CN'
}, {
	name: 'Christmas Island',
	value: 'CX'
}, {
	name: 'Cocos (Keeling) Islands',
	value: 'CC'
}, {
	name: 'Colombia',
	value: 'CO'
}, {
	name: 'Comoros',
	value: 'KM'
}, {
	name: 'Congo',
	value: 'CG'
}, {
	name: 'Congo, The Democratic Republic of the',
	value: 'CD'
}, {
	name: 'Cook Islands',
	value: 'CK'
}, {
	name: 'Costa Rica',
	value: 'CR'
}, {
	name: 'Cote D\'Ivoire',
	value: 'CI'
}, {
	name: 'Croatia',
	value: 'HR'
}, {
	name: 'Cuba',
	value: 'CU'
}, {
	name: 'Cyprus',
	value: 'CY'
}, {
	name: 'Czech Republic',
	value: 'CZ'
}, {
	name: 'Denmark',
	value: 'DK'
}, {
	name: 'Djibouti',
	value: 'DJ'
}, {
	name: 'Dominica',
	value: 'DM'
}, {
	name: 'Dominican Republic',
	value: 'DO'
}, {
	name: 'Ecuador',
	value: 'EC'
}, {
	name: 'Egypt',
	value: 'EG'
}, {
	name: 'El Salvador',
	value: 'SV'
}, {
	name: 'Equatorial Guinea',
	value: 'GQ'
}, {
	name: 'Eritrea',
	value: 'ER'
}, {
	name: 'Estonia',
	value: 'EE'
}, {
	name: 'Ethiopia',
	value: 'ET'
}, {
	name: 'Falkland Islands (Malvinas)',
	value: 'FK'
}, {
	name: 'Faroe Islands',
	value: 'FO'
}, {
	name: 'Fiji',
	value: 'FJ'
}, {
	name: 'Finland',
	value: 'FI'
}, {
	name: 'France',
	value: 'FR'
}, {
	name: 'French Guiana',
	value: 'GF'
}, {
	name: 'French Polynesia',
	value: 'PF'
}, {
	name: 'French Southern Territories',
	value: 'TF'
}, {
	name: 'Gabon',
	value: 'GA'
}, {
	name: 'Gambia',
	value: 'GM'
}, {
	name: 'Georgia',
	value: 'GE'
}, {
	name: 'Germany',
	value: 'DE'
}, {
	name: 'Ghana',
	value: 'GH'
}, {
	name: 'Gibraltar',
	value: 'GI'
}, {
	name: 'Greece',
	value: 'GR'
}, {
	name: 'Greenland',
	value: 'GL'
}, {
	name: 'Grenada',
	value: 'GD'
}, {
	name: 'Guadeloupe',
	value: 'GP'
}, {
	name: 'Guam',
	value: 'GU'
}, {
	name: 'Guatemala',
	value: 'GT'
}, {
	name: 'Guernsey',
	value: 'GG'
}, {
	name: 'Guinea',
	value: 'GN'
}, {
	name: 'Guinea-Bissau',
	value: 'GW'
}, {
	name: 'Guyana',
	value: 'GY'
}, {
	name: 'Haiti',
	value: 'HT'
}, {
	name: 'Heard Island and Mcdonald Islands',
	value: 'HM'
}, {
	name: 'Holy See (Vatican City State)',
	value: 'VA'
}, {
	name: 'Honduras',
	value: 'HN'
}, {
	name: 'Hong Kong',
	value: 'HK'
}, {
	name: 'Hungary',
	value: 'HU'
}, {
	name: 'Iceland',
	value: 'IS'
}, {
	name: 'India',
	value: 'IN'
}, {
	name: 'Indonesia',
	value: 'ID'
}, {
	name: 'Iran, Islamic Republic Of',
	value: 'IR'
}, {
	name: 'Iraq',
	value: 'IQ'
}, {
	name: 'Ireland',
	value: 'IE'
}, {
	name: 'Isle of Man',
	value: 'IM'
}, {
	name: 'Israel',
	value: 'IL'
}, {
	name: 'Italy',
	value: 'IT'
}, {
	name: 'Jamaica',
	value: 'JM'
}, {
	name: 'Japan',
	value: 'JP'
}, {
	name: 'Jersey',
	value: 'JE'
}, {
	name: 'Jordan',
	value: 'JO'
}, {
	name: 'Kazakhstan',
	value: 'KZ'
}, {
	name: 'Kenya',
	value: 'KE'
}, {
	name: 'Kiribati',
	value: 'KI'
}, {
	name: 'Korea, Democratic People\'S Republic of',
	value: 'KP'
}, {
	name: 'Korea, Republic of',
	value: 'KR'
}, {
	name: 'Kuwait',
	value: 'KW'
}, {
	name: 'Kyrgyzstan',
	value: 'KG'
}, {
	name: 'Lao People\'S Democratic Republic',
	value: 'LA'
}, {
	name: 'Latvia',
	value: 'LV'
}, {
	name: 'Lebanon',
	value: 'LB'
}, {
	name: 'Lesotho',
	value: 'LS'
}, {
	name: 'Liberia',
	value: 'LR'
}, {
	name: 'Libyan Arab Jamahiriya',
	value: 'LY'
}, {
	name: 'Liechtenstein',
	value: 'LI'
}, {
	name: 'Lithuania',
	value: 'LT'
}, {
	name: 'Luxembourg',
	value: 'LU'
}, {
	name: 'Macao',
	value: 'MO'
}, {
	name: 'Macedonia, The Former Yugoslav Republic of',
	value: 'MK'
}, {
	name: 'Madagascar',
	value: 'MG'
}, {
	name: 'Malawi',
	value: 'MW'
}, {
	name: 'Malaysia',
	value: 'MY'
}, {
	name: 'Maldives',
	value: 'MV'
}, {
	name: 'Mali',
	value: 'ML'
}, {
	name: 'Malta',
	value: 'MT'
}, {
	name: 'Marshall Islands',
	value: 'MH'
}, {
	name: 'Martinique',
	value: 'MQ'
}, {
	name: 'Mauritania',
	value: 'MR'
}, {
	name: 'Mauritius',
	value: 'MU'
}, {
	name: 'Mayotte',
	value: 'YT'
}, {
	name: 'Mexico',
	value: 'MX'
}, {
	name: 'Micronesia, Federated States of',
	value: 'FM'
}, {
	name: 'Moldova, Republic of',
	value: 'MD'
}, {
	name: 'Monaco',
	value: 'MC'
}, {
	name: 'Mongolia',
	value: 'MN'
}, {
	name: 'Montserrat',
	value: 'MS'
}, {
	name: 'Morocco',
	value: 'MA'
}, {
	name: 'Mozambique',
	value: 'MZ'
}, {
	name: 'Myanmar',
	value: 'MM'
}, {
	name: 'Namibia',
	value: 'NA'
}, {
	name: 'Nauru',
	value: 'NR'
}, {
	name: 'Nepal',
	value: 'NP'
}, {
	name: 'Netherlands',
	value: 'NL'
}, {
	name: 'Netherlands Antilles',
	value: 'AN'
}, {
	name: 'New Caledonia',
	value: 'NC'
}, {
	name: 'New Zealand',
	value: 'NZ'
}, {
	name: 'Nicaragua',
	value: 'NI'
}, {
	name: 'Niger',
	value: 'NE'
}, {
	name: 'Nigeria',
	value: 'NG'
}, {
	name: 'Niue',
	value: 'NU'
}, {
	name: 'Norfolk Island',
	value: 'NF'
}, {
	name: 'Northern Mariana Islands',
	value: 'MP'
}, {
	name: 'Norway',
	value: 'NO'
}, {
	name: 'Oman',
	value: 'OM'
}, {
	name: 'Pakistan',
	value: 'PK'
}, {
	name: 'Palau',
	value: 'PW'
}, {
	name: 'Palestinian Territory, Occupied',
	value: 'PS'
}, {
	name: 'Panama',
	value: 'PA'
}, {
	name: 'Papua New Guinea',
	value: 'PG'
}, {
	name: 'Paraguay',
	value: 'PY'
}, {
	name: 'Peru',
	value: 'PE'
}, {
	name: 'Philippines',
	value: 'PH'
}, {
	name: 'Pitcairn',
	value: 'PN'
}, {
	name: 'Poland',
	value: 'PL'
}, {
	name: 'Portugal',
	value: 'PT'
}, {
	name: 'Puerto Rico',
	value: 'PR'
}, {
	name: 'Qatar',
	value: 'QA'
}, {
	name: 'Reunion',
	value: 'RE'
}, {
	name: 'Romania',
	value: 'RO'
}, {
	name: 'Russian Federation',
	value: 'RU'
}, {
	name: 'RWANDA',
	value: 'RW'
}, {
	name: 'Saint Helena',
	value: 'SH'
}, {
	name: 'Saint Kitts and Nevis',
	value: 'KN'
}, {
	name: 'Saint Lucia',
	value: 'LC'
}, {
	name: 'Saint Pierre and Miquelon',
	value: 'PM'
}, {
	name: 'Saint Vincent and the Grenadines',
	value: 'VC'
}, {
	name: 'Samoa',
	value: 'WS'
}, {
	name: 'San Marino',
	value: 'SM'
}, {
	name: 'Sao Tome and Principe',
	value: 'ST'
}, {
	name: 'Saudi Arabia',
	value: 'SA'
}, {
	name: 'Senegal',
	value: 'SN'
}, {
	name: 'Serbia and Montenegro',
	value: 'CS'
}, {
	name: 'Seychelles',
	value: 'SC'
}, {
	name: 'Sierra Leone',
	value: 'SL'
}, {
	name: 'Singapore',
	value: 'SG'
}, {
	name: 'Slovakia',
	value: 'SK'
}, {
	name: 'Slovenia',
	value: 'SI'
}, {
	name: 'Solomon Islands',
	value: 'SB'
}, {
	name: 'Somalia',
	value: 'SO'
}, {
	name: 'South Africa',
	value: 'ZA'
}, {
	name: 'South Georgia and the South Sandwich Islands',
	value: 'GS'
}, {
	name: 'Spain',
	value: 'ES'
}, {
	name: 'Sri Lanka',
	value: 'LK'
}, {
	name: 'Sudan',
	value: 'SD'
}, {
	name: 'Suriname',
	value: 'SR'
}, {
	name: 'Svalbard and Jan Mayen',
	value: 'SJ'
}, {
	name: 'Swaziland',
	value: 'SZ'
}, {
	name: 'Sweden',
	value: 'SE'
}, {
	name: 'Switzerland',
	value: 'CH'
}, {
	name: 'Syrian Arab Republic',
	value: 'SY'
}, {
	name: 'Taiwan, Province of China',
	value: 'TW'
}, {
	name: 'Tajikistan',
	value: 'TJ'
}, {
	name: 'Tanzania, United Republic of',
	value: 'TZ'
}, {
	name: 'Thailand',
	value: 'TH'
}, {
	name: 'Timor-Leste',
	value: 'TL'
}, {
	name: 'Togo',
	value: 'TG'
}, {
	name: 'Tokelau',
	value: 'TK'
}, {
	name: 'Tonga',
	value: 'TO'
}, {
	name: 'Trinidad and Tobago',
	value: 'TT'
}, {
	name: 'Tunisia',
	value: 'TN'
}, {
	name: 'Turkey',
	value: 'TR'
}, {
	name: 'Turkmenistan',
	value: 'TM'
}, {
	name: 'Turks and Caicos Islands',
	value: 'TC'
}, {
	name: 'Tuvalu',
	value: 'TV'
}, {
	name: 'Uganda',
	value: 'UG'
}, {
	name: 'Ukraine',
	value: 'UA'
}, {
	name: 'United Arab Emirates',
	value: 'AE'
}, {
	name: 'United Kingdom',
	value: 'GB'
}, {
	name: 'United States',
	value: 'US'
}, {
	name: 'United States Minor Outlying Islands',
	value: 'UM'
}, {
	name: 'Uruguay',
	value: 'UY'
}, {
	name: 'Uzbekistan',
	value: 'UZ'
}, {
	name: 'Vanuatu',
	value: 'VU'
}, {
	name: 'Venezuela',
	value: 'VE'
}, {
	name: 'Viet Nam',
	value: 'VN'
}, {
	name: 'Virgin Islands, British',
	value: 'VG'
}, {
	name: 'Virgin Islands, U.S.',
	value: 'VI'
}, {
	name: 'Wallis and Futuna',
	value: 'WF'
}, {
	name: 'Western Sahara',
	value: 'EH'
}, {
	name: 'Yemen',
	value: 'YE'
}, {
	name: 'Zambia',
	value: 'ZM'
}, {
	name: 'Zimbabwe',
	value: 'ZW'
}];
Ext.define('RS.common.data.FlatStore', {
	extend: 'Ext.data.Store',

	defaultConfigProperties: ['mtype', 'fields', 'data', 'filterMethod', 'accessorMethods'],
	defaultFlatConfig: {
		mtype: 'store',
		fields: ['text', 'value'],
		filterMethod: function () { return true; },
		accessorMethods: [
			function (d) { return d; },
			function (d) { return d; }
		]
	},

	dataFieldNames: [],
	serverFieldNames: [],
	filterMethod: function () { return true; },
	accessorMethods: [],

	constructor: function(config) {
		config = config || {};

		if (config.constructor === Array) {
			var data = config;
			config = this.defaultFlatConfig;
			this.dataFieldNames = config.fields;
			this.serverFieldNames = config.fields;
			this.filterBy(config.filterMethod)
				.accessors(config.accessorMethods);
			config.data = this.shapeData(data);
		} else {
			for (var i = 0; i < this.defaultConfigProperties.length; i++) {
				var p = this.defaultConfigProperties[i];

				if (typeof config[p] === 'undefined') {
					if (p === 'data') {
						config[p] = [];
					} else {
						config[p] = this.defaultFlatConfig[p];	
					}					
				}
			}

			this.dataFieldNames = config.fields;

			if (config.serverFields && config.serverFields.constructor === Array) {
				this.serverFieldNames = config.serverFields;
			} else {
				this.serverFieldNames = config.fields;
			}

			this.filterBy(config.filterMethod)
				.accessors(config.accessorMethods);
		}

		this.callParent([config]);
	},

	shapeData: function () {
		var data = [],
			fields = this.dataFieldNames,
			accessors = this.accessors();		

		if (fields.length !== accessors.length) {
			throw new Error('fields.length != to accessors.length in config');
		}

		if (arguments.length === 1) {
			if (arguments[0].constructor === Array) {
				data = arguments[0];
			} else {
				data.push(arguments[0]);
			}
		} else {
			data = Array.apply(null, arguments);
		}

		var filteredData = data.filter(this.filterBy());
		var shapedData = [];

		for (var i = 0; i < filteredData.length; i++) {
			var item = {};

			for (var j = 0; j < fields.length && j < accessors.length; j++) {
				item[fields[j]] = accessors[j](filteredData[i]);
			}

			shapedData.push(item);
		}

		return shapedData;
	},

	getData: function (getDataForServer) {
		var records = this.getRange(),
			data = [];

		if (getDataForServer) {
			for (var i = 0; i < records.length; i++) {
				var d = {},
					r = records[i];

				for (var j = 0; j < this.serverFieldNames.length; j++) {
					d[this.serverFieldNames[j]] = r.data[this.serverFieldNames[j]];
				}

				data.push(d);
			}
		} else {
			for (var i = 0; i < records.length; i++) {
				data.push(records[i].data);
			}
		}

		return data;
	},

	filterBy: function (fn) {
		if (typeof fn === 'function') {
			this.filterMethod = fn;
			return this;
		}

		return this.filterMethod;
	},	

	accessors: function () {
		if (arguments.length > 0) {
			var accessors = [];

			if (arguments.length === 1) {
				if (arguments[0].constructor === Array) {
					accessors = arguments[0];
				} else {
					accessors.push(arguments[0]);
				}
			} else {
				accessors = Array.apply(null, arguments);
			}

			for (var i = 0; i < accessors.length; i++) {
				if (typeof accessors[i] !== 'function') {
					throw new Error('Accessor at index='+ i + ' is not a function.');
				}
			}

			this.accessorMethods = accessors;
			return this;
		}

		return this.accessorMethods;
	},

	removeBy: function (fieldName, value) {
		var result = {
			index: -1,
			data: null
		};

		result.index = this.findExact(fieldName, value);

		if (result.index !== -1) {
			result.data = this.getAt(result.index).data;
			this.removeAt(result.index);
		}

		return result;
	}	
});
/**
 * A control that allows selection of multiple items in a list.
 */
Ext.define('Ext.ux.form.MultiSelect', {
    
    extend: 'Ext.form.FieldContainer',
    
    mixins: {
        bindable: 'Ext.util.Bindable',
        field: 'Ext.form.field.Field'    
    },
    
    alternateClassName: 'Ext.ux.Multiselect',
    alias: ['widget.multiselectfield', 'widget.multiselect'],
    
    requires: ['Ext.panel.Panel', 'Ext.view.BoundList', 'Ext.layout.container.Fit'],
    
    uses: ['Ext.view.DragZone', 'Ext.view.DropZone'],
    
    layout: 'anchor',
    
    multiSelect: true,
    
    /**
     * @cfg {String} [dragGroup=""] The ddgroup name for the MultiSelect DragZone.
     */

    /**
     * @cfg {String} [dropGroup=""] The ddgroup name for the MultiSelect DropZone.
     */
    
    /**
     * @cfg {String} [title=""] A title for the underlying panel.
     */
    
    /**
     * @cfg {Boolean} [ddReorder=false] Whether the items in the MultiSelect list are drag/drop reorderable.
     */
    ddReorder: false,

    /**
     * @cfg {Object/Array} tbar An optional toolbar to be inserted at the top of the control's selection list.
     * This can be a {@link Ext.toolbar.Toolbar} object, a toolbar config, or an array of buttons/button configs
     * to be added to the toolbar. See {@link Ext.panel.Panel#tbar}.
     */

    /**
     * @cfg {String} [appendOnly=false] `true` if the list should only allow append drops when drag/drop is enabled.
     * This is useful for lists which are sorted.
     */
    appendOnly: false,

    /**
     * @cfg {String} [displayField="text"] Name of the desired display field in the dataset.
     */
    displayField: 'text',

    /**
     * @cfg {String} [valueField="text"] Name of the desired value field in the dataset.
     */

    /**
     * @cfg {Boolean} [allowBlank=true] `false` to require at least one item in the list to be selected, `true` to allow no
     * selection.
     */
    allowBlank: true,

    /**
     * @cfg {Number} [minSelections=0] Minimum number of selections allowed.
     */
    minSelections: 0,

    /**
     * @cfg {Number} [maxSelections=Number.MAX_VALUE] Maximum number of selections allowed.
     */
    maxSelections: Number.MAX_VALUE,

    /**
     * @cfg {String} [blankText="This field is required"] Default text displayed when the control contains no items.
     */
    blankText: 'This field is required',

    /**
     * @cfg {String} [minSelectionsText="Minimum {0}item(s) required"] 
     * Validation message displayed when {@link #minSelections} is not met. 
     * The {0} token will be replaced by the value of {@link #minSelections}.
     */
    minSelectionsText: 'Minimum {0} item(s) required',
    
    /**
     * @cfg {String} [maxSelectionsText="Maximum {0}item(s) allowed"] 
     * Validation message displayed when {@link #maxSelections} is not met
     * The {0} token will be replaced by the value of {@link #maxSelections}.
     */
    maxSelectionsText: 'Maximum {0} item(s) required',

    /**
     * @cfg {String} [delimiter=","] The string used to delimit the selected values when {@link #getSubmitValue submitting}
     * the field as part of a form. If you wish to have the selected values submitted as separate
     * parameters rather than a single delimited parameter, set this to `null`.
     */
    delimiter: ',',
    
    /**
     * @cfg String [dragText="{0} Item{1}"] The text to show while dragging items.
     * {0} will be replaced by the number of items. {1} will be replaced by the plural
     * form if there is more than 1 item.
     */
    dragText: '{0} Item{1}',

    /**
     * @cfg {Ext.data.Store/Array} store The data source to which this MultiSelect is bound (defaults to `undefined`).
     * Acceptable values for this property are:
     * <div class="mdetail-params"><ul>
     * <li><b>any {@link Ext.data.Store Store} subclass</b></li>
     * <li><b>an Array</b> : Arrays will be converted to a {@link Ext.data.ArrayStore} internally.
     * <div class="mdetail-params"><ul>
     * <li><b>1-dimensional array</b> : (e.g., <tt>['Foo','Bar']</tt>)<div class="sub-desc">
     * A 1-dimensional array will automatically be expanded (each array item will be the combo
     * {@link #valueField value} and {@link #displayField text})</div></li>
     * <li><b>2-dimensional array</b> : (e.g., <tt>[['f','Foo'],['b','Bar']]</tt>)<div class="sub-desc">
     * For a multi-dimensional array, the value in index 0 of each item will be assumed to be the combo
     * {@link #valueField value}, while the value at index 1 is assumed to be the combo {@link #displayField text}.
     * </div></li></ul></div></li></ul></div>
     */
    
    ignoreSelectChange: 0,

    /**
     * @cfg {Object} listConfig
     * An optional set of configuration properties that will be passed to the {@link Ext.view.BoundList}'s constructor.
     * Any configuration that is valid for BoundList can be included.
     */

    initComponent: function(){
        var me = this;

        me.bindStore(me.store, true);
        if (me.store.autoCreated) {
            me.valueField = me.displayField = 'field1';
            if (!me.store.expanded) {
                me.displayField = 'field2';
            }
        }

        if (!Ext.isDefined(me.valueField)) {
            me.valueField = me.displayField;
        }
        me.items = me.setupItems();
        
        
        me.callParent();
        me.initField();
        me.addEvents('drop');    
    },
    
    setupItems: function() {
        var me = this;

        me.boundList = Ext.create('Ext.view.BoundList', Ext.apply({
            anchor: 'none 100%',
            deferInitialRefresh: false,
            border: 1,
            multiSelect: this.multiSelect,
            store: me.store,
            displayField: me.displayField,
            disabled: me.disabled
        }, me.listConfig));
        me.boundList.getSelectionModel().on('selectionchange', me.onSelectChange, me);
        
        // Only need to wrap the BoundList in a Panel if we have a title.
        if (!me.title) {
            return me.boundList;
        }

        // Wrap to add a title
        me.boundList.border = false;
        return {
            border: true,
            anchor: 'none 100%',
            layout: 'anchor',
            title: me.title,
            tbar: me.tbar,
            items: me.boundList
        };
    },

    onSelectChange: function(selModel, selections){
        if (!this.ignoreSelectChange) {
            this.setValue(selections);
        }    
    },
    
    getSelected: function(){
        return this.boundList.getSelectionModel().getSelection();
    },
    
    // compare array values
    isEqual: function(v1, v2) {
        var fromArray = Ext.Array.from,
            i = 0, 
            len;

        v1 = fromArray(v1);
        v2 = fromArray(v2);
        len = v1.length;

        if (len !== v2.length) {
            return false;
        }

        for(; i < len; i++) {
            if (v2[i] !== v1[i]) {
                return false;
            }
        }

        return true;
    },
    
    afterRender: function(){
        var me = this,
            records;
        
        me.callParent();
        if (me.selectOnRender) {
            records = me.getRecordsForValue(me.value);
            if (records.length) {
                ++me.ignoreSelectChange;
                me.boundList.getSelectionModel().select(records);
                --me.ignoreSelectChange;
            }
            delete me.toSelect;
        }    
        
        if (me.ddReorder && !me.dragGroup && !me.dropGroup){
            me.dragGroup = me.dropGroup = 'MultiselectDD-' + Ext.id();
        }

        if (me.draggable || me.dragGroup){
            me.dragZone = Ext.create('Ext.view.DragZone', {
                view: me.boundList,
                ddGroup: me.dragGroup,
                dragText: me.dragText
            });
        }
        if (me.droppable || me.dropGroup){
            me.dropZone = Ext.create('Ext.view.DropZone', {
                view: me.boundList,
                ddGroup: me.dropGroup,
                handleNodeDrop: function(data, dropRecord, position) {
                    var view = this.view,
                        store = view.getStore(),
                        records = data.records,
                        index;

                    // remove the Models from the source Store
                    data.view.store.remove(records);

                    index = store.indexOf(dropRecord);
                    if (position === 'after') {
                        index++;
                    }
                    store.insert(index, records);
                    view.getSelectionModel().select(records);
                    me.fireEvent('drop', me, records);
                }
            });
        }
    },
    
    isValid : function() {
        var me = this,
            disabled = me.disabled,
            validate = me.forceValidation || !disabled;
            
        
        return validate ? me.validateValue(me.value) : disabled;
    },
    
    validateValue: function(value) {
        var me = this,
            errors = me.getErrors(value),
            isValid = Ext.isEmpty(errors);
            
        if (!me.preventMark) {
            if (isValid) {
                me.clearInvalid();
            } else {
                me.markInvalid(errors);
            }
        }

        return isValid;
    },
    
    markInvalid : function(errors) {
        // Save the message and fire the 'invalid' event
        var me = this,
            oldMsg = me.getActiveError();
        me.setActiveErrors(Ext.Array.from(errors));
        if (oldMsg !== me.getActiveError()) {
            me.updateLayout();
        }
    },

    /**
     * Clear any invalid styles/messages for this field.
     *
     * __Note:__ this method does not cause the Field's {@link #validate} or {@link #isValid} methods to return `true`
     * if the value does not _pass_ validation. So simply clearing a field's errors will not necessarily allow
     * submission of forms submitted with the {@link Ext.form.action.Submit#clientValidation} option set.
     */
    clearInvalid : function() {
        // Clear the message and fire the 'valid' event
        var me = this,
            hadError = me.hasActiveError();
        me.unsetActiveError();
        if (hadError) {
            me.updateLayout();
        }
    },
    
    getSubmitData: function() {
        var me = this,
            data = null,
            val;
        if (!me.disabled && me.submitValue && !me.isFileUpload()) {
            val = me.getSubmitValue();
            if (val !== null) {
                data = {};
                data[me.getName()] = val;
            }
        }
        return data;
    },

    /**
     * Returns the value that would be included in a standard form submit for this field.
     *
     * @return {String} The value to be submitted, or `null`.
     */
    getSubmitValue: function() {
        var me = this,
            delimiter = me.delimiter,
            val = me.getValue();
        
        return Ext.isString(delimiter) ? val.join(delimiter) : val;
    },
    
    getValue: function(){
        return this.value || [];
    },
    
    getRecordsForValue: function(value){
        var me = this,
            records = [],
            all = me.store.getRange(),
            valueField = me.valueField,
            i = 0,
            allLen = all.length,
            rec,
            j,
            valueLen;
            
        for (valueLen = value.length; i < valueLen; ++i) {
            for (j = 0; j < allLen; ++j) {
                rec = all[j];   
                if (rec.get(valueField) == value[i]) {
                    records.push(rec);
                }
            }    
        }
            
        return records;
    },
    
    setupValue: function(value){
        var delimiter = this.delimiter,
            valueField = this.valueField,
            i = 0,
            out,
            len,
            item;
            
        if (Ext.isDefined(value)) {
            if (delimiter && Ext.isString(value)) {
                value = value.split(delimiter);
            } else if (!Ext.isArray(value)) {
                value = [value];
            }
        
            for (len = value.length; i < len; ++i) {
                item = value[i];
                if (item && item.isModel) {
                    value[i] = item.get(valueField);
                }
            }
            out = Ext.Array.unique(value);
        } else {
            out = [];
        }
        return out;
    },
    
    setValue: function(value){
        var me = this,
            selModel = me.boundList.getSelectionModel(),
            store = me.store;

        // Store not loaded yet - we cannot set the value
        if (!store.getCount()) {
            store.on({
                load: Ext.Function.bind(me.setValue, me, [value]),
                single: true
            });
            return;
        }

        value = me.setupValue(value);
        me.mixins.field.setValue.call(me, value);
        
        if (me.rendered) {
            ++me.ignoreSelectChange;
            selModel.deselectAll();
            selModel.select(me.getRecordsForValue(value));
            --me.ignoreSelectChange;
        } else {
            me.selectOnRender = true;
        }
    },
    
    clearValue: function(){
        this.setValue([]);    
    },
    
    onEnable: function(){
        var list = this.boundList;
        this.callParent();
        if (list) {
            list.enable();
        }
    },
    
    onDisable: function(){
        var list = this.boundList;
        this.callParent();
        if (list) {
            list.disable();
        }
    },
    
    getErrors : function(value) {
        var me = this,
            format = Ext.String.format,
            errors = [],
            numSelected;

        value = Ext.Array.from(value || me.getValue());
        numSelected = value.length;

        if (!me.allowBlank && numSelected < 1) {
            errors.push(me.blankText);
        }
        if (numSelected < me.minSelections) {
            errors.push(format(me.minSelectionsText, me.minSelections));
        }
        if (numSelected > me.maxSelections) {
            errors.push(format(me.maxSelectionsText, me.maxSelections));
        }
        return errors;
    },
    
    onDestroy: function(){
        var me = this;
        
        me.bindStore(null);
        Ext.destroy(me.dragZone, me.dropZone);
        me.callParent();
    },
    
    onBindStore: function(store){
        var boundList = this.boundList;
        
        if (boundList) {
            boundList.bindStore(store);
        }
    }
    
});

/**
 * Basic status bar component that can be used as the bottom toolbar of any {@link Ext.Panel}.  In addition to
 * supporting the standard {@link Ext.toolbar.Toolbar} interface for adding buttons, menus and other items, the StatusBar
 * provides a greedy status element that can be aligned to either side and has convenient methods for setting the
 * status text and icon.  You can also indicate that something is processing using the {@link #showBusy} method.
 *
 *     Ext.create('Ext.Panel', {
 *         title: 'StatusBar',
 *         // etc.
 *         bbar: Ext.create('Ext.ux.StatusBar', {
 *             id: 'my-status',
 *      
 *             // defaults to use when the status is cleared:
 *             defaultText: 'Default status text',
 *             defaultIconCls: 'default-icon',
 *      
 *             // values to set initially:
 *             text: 'Ready',
 *             iconCls: 'ready-icon',
 *      
 *             // any standard Toolbar items:
 *             items: [{
 *                 text: 'A Button'
 *             }, '-', 'Plain Text']
 *         })
 *     });
 *
 *     // Update the status bar later in code:
 *     var sb = Ext.getCmp('my-status');
 *     sb.setStatus({
 *         text: 'OK',
 *         iconCls: 'ok-icon',
 *         clear: true // auto-clear after a set interval
 *     });
 *
 *     // Set the status bar to show that something is processing:
 *     sb.showBusy();
 *
 *     // processing....
 *
 *     sb.clearStatus(); // once completeed
 *
 */
Ext.define('Ext.ux.statusbar.StatusBar', {
    extend: 'Ext.toolbar.Toolbar',
    alternateClassName: 'Ext.ux.StatusBar',
    alias: 'widget.statusbar',
    requires: ['Ext.toolbar.TextItem'],
    /**
     * @cfg {String} statusAlign
     * The alignment of the status element within the overall StatusBar layout.  When the StatusBar is rendered,
     * it creates an internal div containing the status text and icon.  Any additional Toolbar items added in the
     * StatusBar's {@link #cfg-items} config, or added via {@link #method-add} or any of the supported add* methods, will be
     * rendered, in added order, to the opposite side.  The status element is greedy, so it will automatically
     * expand to take up all sapce left over by any other items.  Example usage:
     *
     *     // Create a left-aligned status bar containing a button,
     *     // separator and text item that will be right-aligned (default):
     *     Ext.create('Ext.Panel', {
     *         title: 'StatusBar',
     *         // etc.
     *         bbar: Ext.create('Ext.ux.statusbar.StatusBar', {
     *             defaultText: 'Default status text',
     *             id: 'status-id',
     *             items: [{
     *                 text: 'A Button'
     *             }, '-', 'Plain Text']
     *         })
     *     });
     *
     *     // By adding the statusAlign config, this will create the
     *     // exact same toolbar, except the status and toolbar item
     *     // layout will be reversed from the previous example:
     *     Ext.create('Ext.Panel', {
     *         title: 'StatusBar',
     *         // etc.
     *         bbar: Ext.create('Ext.ux.statusbar.StatusBar', {
     *             defaultText: 'Default status text',
     *             id: 'status-id',
     *             statusAlign: 'right',
     *             items: [{
     *                 text: 'A Button'
     *             }, '-', 'Plain Text']
     *         })
     *     });
     */
    /**
     * @cfg {String} [defaultText='']
     * The default {@link #text} value.  This will be used anytime the status bar is cleared with the
     * `useDefaults:true` option.
     */
    /**
     * @cfg {String} [defaultIconCls='']
     * The default {@link #iconCls} value (see the iconCls docs for additional details about customizing the icon).
     * This will be used anytime the status bar is cleared with the `useDefaults:true` option.
     */
    /**
     * @cfg {String} text
     * A string that will be <b>initially</b> set as the status message.  This string
     * will be set as innerHTML (html tags are accepted) for the toolbar item.
     * If not specified, the value set for {@link #defaultText} will be used.
     */
    /**
     * @cfg {String} [iconCls='']
     * A CSS class that will be **initially** set as the status bar icon and is
     * expected to provide a background image.
     *
     * Example usage:
     *
     *     // Example CSS rule:
     *     .x-statusbar .x-status-custom {
     *         padding-left: 25px;
     *         background: transparent url(images/custom-icon.gif) no-repeat 3px 2px;
     *     }
     *
     *     // Setting a default icon:
     *     var sb = Ext.create('Ext.ux.statusbar.StatusBar', {
     *         defaultIconCls: 'x-status-custom'
     *     });
     *
     *     // Changing the icon:
     *     sb.setStatus({
     *         text: 'New status',
     *         iconCls: 'x-status-custom'
     *     });
     */

    /**
     * @cfg {String} cls
     * The base class applied to the containing element for this component on render.
     */
    cls : 'x-statusbar',
    /**
     * @cfg {String} busyIconCls
     * The default {@link #iconCls} applied when calling {@link #showBusy}.
     * It can be overridden at any time by passing the `iconCls` argument into {@link #showBusy}.
     */
    busyIconCls : 'x-status-busy',
    /**
     * @cfg {String} busyText
     * The default {@link #text} applied when calling {@link #showBusy}.
     * It can be overridden at any time by passing the `text` argument into {@link #showBusy}.
     */
    busyText : 'Loading...',
    /**
     * @cfg {Number} autoClear
     * The number of milliseconds to wait after setting the status via
     * {@link #setStatus} before automatically clearing the status text and icon.
     * Note that this only applies when passing the `clear` argument to {@link #setStatus}
     * since that is the only way to defer clearing the status.  This can
     * be overridden by specifying a different `wait` value in {@link #setStatus}.
     * Calls to {@link #clearStatus} always clear the status bar immediately and ignore this value.
     */
    autoClear : 5000,

    /**
     * @cfg {String} emptyText
     * The text string to use if no text has been set. If there are no other items in
     * the toolbar using an empty string (`''`) for this value would end up in the toolbar
     * height collapsing since the empty string will not maintain the toolbar height.
     * Use `''` if the toolbar should collapse in height vertically when no text is
     * specified and there are no other items in the toolbar.
     */
    emptyText : '&#160;',

    // private
    activeThreadId : 0,

    // private
    initComponent : function(){
        var right = this.statusAlign === 'right';

        this.callParent(arguments);
        this.currIconCls = this.iconCls || this.defaultIconCls;
        this.statusEl = Ext.create('Ext.toolbar.TextItem', {
            cls: 'x-status-text ' + (this.currIconCls || ''),
            text: this.text || this.defaultText || ''
        });

        if (right) {
            this.cls += ' x-status-right';
            this.add('->');
            this.add(this.statusEl);
        } else {
            this.insert(0, this.statusEl);
            this.insert(1, '->');
        }
    },

    /**
     * Sets the status {@link #text} and/or {@link #iconCls}. Also supports automatically clearing the
     * status that was set after a specified interval.
     *
     * Example usage:
     *
     *     // Simple call to update the text
     *     statusBar.setStatus('New status');
     *
     *     // Set the status and icon, auto-clearing with default options:
     *     statusBar.setStatus({
     *         text: 'New status',
     *         iconCls: 'x-status-custom',
     *         clear: true
     *     });
     *
     *     // Auto-clear with custom options:
     *     statusBar.setStatus({
     *         text: 'New status',
     *         iconCls: 'x-status-custom',
     *         clear: {
     *             wait: 8000,
     *             anim: false,
     *             useDefaults: false
     *         }
     *     });
     *
     * @param {Object/String} config A config object specifying what status to set, or a string assumed
     * to be the status text (and all other options are defaulted as explained below). A config
     * object containing any or all of the following properties can be passed:
     *
     * @param {String} config.text The status text to display.  If not specified, any current
     * status text will remain unchanged.
     *
     * @param {String} config.iconCls The CSS class used to customize the status icon (see
     * {@link #iconCls} for details). If not specified, any current iconCls will remain unchanged.
     *
     * @param {Boolean/Number/Object} config.clear Allows you to set an internal callback that will
     * automatically clear the status text and iconCls after a specified amount of time has passed. If clear is not
     * specified, the new status will not be auto-cleared and will stay until updated again or cleared using
     * {@link #clearStatus}. If `true` is passed, the status will be cleared using {@link #autoClear},
     * {@link #defaultText} and {@link #defaultIconCls} via a fade out animation. If a numeric value is passed,
     * it will be used as the callback interval (in milliseconds), overriding the {@link #autoClear} value.
     * All other options will be defaulted as with the boolean option.  To customize any other options,
     * you can pass an object in the format:
     * 
     * @param {Number} config.clear.wait The number of milliseconds to wait before clearing
     * (defaults to {@link #autoClear}).
     * @param {Boolean} config.clear.anim False to clear the status immediately once the callback
     * executes (defaults to true which fades the status out).
     * @param {Boolean} config.clear.useDefaults False to completely clear the status text and iconCls
     * (defaults to true which uses {@link #defaultText} and {@link #defaultIconCls}).
     *
     * @return {Ext.ux.statusbar.StatusBar} this
     */
    setStatus : function(o) {
        var me = this;

        o = o || {};
        Ext.suspendLayouts();
        if (Ext.isString(o)) {
            o = {text:o};
        }
        if (o.text !== undefined) {
            me.setText(o.text);
        }
        if (o.iconCls !== undefined) {
            me.setIcon(o.iconCls);
        }

        if (o.clear) {
            var c = o.clear,
                wait = me.autoClear,
                defaults = {useDefaults: true, anim: true};

            if (Ext.isObject(c)) {
                c = Ext.applyIf(c, defaults);
                if (c.wait) {
                    wait = c.wait;
                }
            } else if (Ext.isNumber(c)) {
                wait = c;
                c = defaults;
            } else if (Ext.isBoolean(c)) {
                c = defaults;
            }

            c.threadId = this.activeThreadId;
            Ext.defer(me.clearStatus, wait, me, [c]);
        }
        Ext.resumeLayouts(true);
        return me;
    },

    /**
     * Clears the status {@link #text} and {@link #iconCls}. Also supports clearing via an optional fade out animation.
     *
     * @param {Object} [config] A config object containing any or all of the following properties.  If this
     * object is not specified the status will be cleared using the defaults below:
     * @param {Boolean} config.anim True to clear the status by fading out the status element (defaults
     * to false which clears immediately).
     * @param {Boolean} config.useDefaults True to reset the text and icon using {@link #defaultText} and
     * {@link #defaultIconCls} (defaults to false which sets the text to '' and removes any existing icon class).
     *
     * @return {Ext.ux.statusbar.StatusBar} this
     */
    clearStatus : function(o) {
        o = o || {};

        var me = this,
            statusEl = me.statusEl;

        if (o.threadId && o.threadId !== me.activeThreadId) {
            // this means the current call was made internally, but a newer
            // thread has set a message since this call was deferred.  Since
            // we don't want to overwrite a newer message just ignore.
            return me;
        }

        var text = o.useDefaults ? me.defaultText : me.emptyText,
            iconCls = o.useDefaults ? (me.defaultIconCls ? me.defaultIconCls : '') : '';

        if (o.anim) {
            // animate the statusEl Ext.Element
            statusEl.el.puff({
                remove: false,
                useDisplay: true,
                callback: function() {
                    statusEl.el.show();
                    me.setStatus({
                        text: text,
                        iconCls: iconCls
                    });
                }
            });
        } else {
             me.setStatus({
                 text: text,
                 iconCls: iconCls
             });
        }
        return me;
    },

    /**
     * Convenience method for setting the status text directly.  For more flexible options see {@link #setStatus}.
     * @param {String} text (optional) The text to set (defaults to '')
     * @return {Ext.ux.statusbar.StatusBar} this
     */
    setText : function(text) {
        var me = this;
        me.activeThreadId++;
        me.text = text || '';
        if (me.rendered) {
            me.statusEl.setText(me.text);
        }
        return me;
    },

    /**
     * Returns the current status text.
     * @return {String} The status text
     */
    getText : function(){
        return this.text;
    },

    /**
     * Convenience method for setting the status icon directly.  For more flexible options see {@link #setStatus}.
     * See {@link #iconCls} for complete details about customizing the icon.
     * @param {String} iconCls (optional) The icon class to set (defaults to '', and any current icon class is removed)
     * @return {Ext.ux.statusbar.StatusBar} this
     */
    setIcon : function(cls) {
        var me = this;

        me.activeThreadId++;
        cls = cls || '';

        if (me.rendered) {
            if (me.currIconCls) {
                me.statusEl.removeCls(me.currIconCls);
                me.currIconCls = null;
            }
            if (cls.length > 0) {
                me.statusEl.addCls(cls);
                me.currIconCls = cls;
            }
        } else {
            me.currIconCls = cls;
        }
        return me;
    },

    /**
     * Convenience method for setting the status text and icon to special values that are pre-configured to indicate
     * a "busy" state, usually for loading or processing activities.
     *
     * @param {Object/String} config (optional) A config object in the same format supported by {@link #setStatus}, or a
     * string to use as the status text (in which case all other options for setStatus will be defaulted).  Use the
     * `text` and/or `iconCls` properties on the config to override the default {@link #busyText}
     * and {@link #busyIconCls} settings. If the config argument is not specified, {@link #busyText} and
     * {@link #busyIconCls} will be used in conjunction with all of the default options for {@link #setStatus}.
     * @return {Ext.ux.statusbar.StatusBar} this
     */
    showBusy : function(o){
        if (Ext.isString(o)) {
            o = { text: o };
        }
        o = Ext.applyIf(o || {}, {
            text: this.busyText,
            iconCls: this.busyIconCls
        });
        return this.setStatus(o);
    }
});

/**
 * A {@link Ext.ux.statusbar.StatusBar} plugin that provides automatic error
 * notification when the associated form contains validation errors.
 */
Ext.define('Ext.ux.statusbar.ValidationStatus', {
    extend: 'Ext.Component', 
    requires: ['Ext.util.MixedCollection'],
    /**
     * @cfg {String} errorIconCls
     * The {@link Ext.ux.statusbar.StatusBar#iconCls iconCls} value to be applied
     * to the status message when there is a validation error.
     */
    errorIconCls : 'x-status-error',
    /**
     * @cfg {String} errorListCls
     * The css class to be used for the error list when there are validation errors.
     */
    errorListCls : 'x-status-error-list',
    /**
     * @cfg {String} validIconCls
     * The {@link Ext.ux.statusbar.StatusBar#iconCls iconCls} value to be applied
     * to the status message when the form validates.
     */
    validIconCls : 'x-status-valid',
    
    /**
     * @cfg {String} showText
     * The {@link Ext.ux.statusbar.StatusBar#text text} value to be applied when
     * there is a form validation error.
     */
    showText : 'The form has errors (click for details...)',
    /**
     * @cfg {String} hideText
     * The {@link Ext.ux.statusbar.StatusBar#text text} value to display when
     * the error list is displayed.
     */
    hideText : 'Click again to hide the error list',
    /**
     * @cfg {String} submitText
     * The {@link Ext.ux.statusbar.StatusBar#text text} value to be applied when
     * the form is being submitted.
     */
    submitText : 'Saving...',
    
    // private
    init : function(sb) {
        var me = this;

        me.statusBar = sb;
        sb.on({
            single: true,
            scope: me,
            render: me.onStatusbarRender,
            beforedestroy: me.destroy
        });
        sb.on({
            click: {
                element: 'el',
                fn: me.onStatusClick,
                scope: me,
                buffer: 200
            }
        });
    },

    onStatusbarRender: function(sb) {
        var me = this,
            startMonitor = function() {
                me.monitor = true;
            };

        me.monitor = true;
        me.errors = Ext.create('Ext.util.MixedCollection');
        me.listAlign = (sb.statusAlign === 'right' ? 'br-tr?' : 'bl-tl?');

        if (me.form) {
            me.formPanel = Ext.getCmp(me.form);
            me.basicForm = me.formPanel.getForm();
            me.startMonitoring();
            me.basicForm.on('beforeaction', function(f, action) {
                if (action.type === 'submit') {
                    // Ignore monitoring while submitting otherwise the field validation
                    // events cause the status message to reset too early
                    me.monitor = false;
                }
            });
            me.basicForm.on('actioncomplete', startMonitor);
            me.basicForm.on('actionfailed', startMonitor);
        }
   },
    
    // private
    startMonitoring : function() {
        this.basicForm.getFields().each(function(f) {
            f.on('validitychange', this.onFieldValidation, this);
        }, this);
    },
    
    // private
    stopMonitoring : function() {
        this.basicForm.getFields().each(function(f) {
            f.un('validitychange', this.onFieldValidation, this);
        }, this);
    },
    
    // private
    onDestroy : function() {
        this.stopMonitoring();
        this.statusBar.statusEl.un('click', this.onStatusClick, this);
        this.callParent(arguments);
    },
    
    // private
    onFieldValidation : function(f, isValid) {
        var me = this,
            msg;

        if (!me.monitor) {
            return false;
        }
        msg = f.getErrors()[0];
        if (msg) {
            me.errors.add(f.id, {field:f, msg:msg});
        } else {
            me.errors.removeAtKey(f.id);
        }
        this.updateErrorList();
        if (me.errors.getCount() > 0) {
            if (me.statusBar.getText() !== me.showText) {
                me.statusBar.setStatus({
                    text: me.showText,
                    iconCls: me.errorIconCls
                });
            }
        } else {
            me.statusBar.clearStatus().setIcon(me.validIconCls);
        }
    },

    // private
    updateErrorList : function() {
        var me = this,
            msg,
            msgEl = me.getMsgEl();

        if (me.errors.getCount() > 0) {
            msg = ['<ul>'];
            this.errors.each(function(err) {
                msg.push('<li id="x-err-', err.field.id, '"><a href="#">', err.msg, '</a></li>');
            });
            msg.push('</ul>');
            msgEl.update(msg.join(''));
        } else {
            msgEl.update('');
        }
        // reset msgEl size
        msgEl.setSize('auto', 'auto');
    },
    
    // private
    getMsgEl : function() {
        var me = this,
            msgEl = me.msgEl,
            t;

        if (!msgEl) {
            msgEl = me.msgEl = Ext.DomHelper.append(Ext.getBody(), {
                cls: me.errorListCls
            }, true);
            msgEl.hide();
            msgEl.on('click', function(e) {
                t = e.getTarget('li', 10, true);
                if (t) {
                    Ext.getCmp(t.id.split('x-err-')[1]).focus();
                    me.hideErrors();
                }
            }, null, {stopEvent: true}); // prevent anchor click navigation
        }
        return msgEl;
    },
    
    // private
    showErrors : function() {
        var me = this;

        me.updateErrorList();
        me.getMsgEl().alignTo(me.statusBar.getEl(), me.listAlign).slideIn('b', {duration: 300, easing: 'easeOut'});
        me.statusBar.setText(me.hideText);
        me.formPanel.body.on('click', me.hideErrors, me, {single:true}); // hide if the user clicks directly into the form
    },

    // private
    hideErrors : function() {
        var el = this.getMsgEl();
        if (el.isVisible()) {
            el.slideOut('b', {duration: 300, easing: 'easeIn'});
            this.statusBar.setText(this.showText);
        }
        this.formPanel.body.un('click', this.hideErrors, this);
    },
    
    // private
    onStatusClick : function() {
        if (this.getMsgEl().isVisible()) {
            this.hideErrors();
        } else if (this.errors.getCount() > 0) {
            this.showErrors();
        }
    }
});
//https: //www.sencha.com/forum/showthread.php?282218-ExtJS-4.2.2-Grid-state-issue-with-multiple-grids-of-the-same-type 
Ext.override(Ext.grid.column.Column, {
	getStateId: function() {
		return this.stateId || this.dataIndex || this.headerId;
	}
});
function configureColumns(fields, modelFields, columns, autoSize, initialConfig, form) {
	Ext.Array.forEach(fields, function(record) {
		if (!record) {
			return;
		}
		switch (record.dbtype) {
			case 'timestamp':
				type = 'date';
				break;
			case 'boolean':
				type = 'boolean';
				break;
			case 'long':
			case 'double':
				type = 'number';
				break;
			default:
				type = 'string';
				break;
		}

		var storeCol = {
			name: record.columnModelName,
			type: type
		},
			col = {
				text: Ext.String.htmlEncode(record.displayName),
				dataIndex: record.columnModelName,
				fieldConfig: record,
				hidden: record.hidden,
				uiType: record.uiType,
				filterable: !record.encrypted,
				dataType: type == 'number' ? 'float' : type
			};

		if (autoSize) {
			col.width = 150;
			col.autoWidth = record.dbtype != 'clob';
		} else col.flex = 1;

		if (type == 'date') {
			storeCol.format = 'time';
			storeCol.dateFormat = 'time';
			col.renderer = Ext.util.Format.dateRenderer(Ext.state.Manager.get('userDefaultDateFormat', 'Y-m-d G:i:s'));
			col.width = 175;
			delete col.flex;
		}

		var styleRenderer = Ext.emptyFn,
			parseDate = function(value) {
				switch (value.toLowerCase()) {
					case 'today':
						value = Ext.Date.format(new Date(), 'Y-m-d');
						break;
					case 'yesterday':
						value = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, -1), 'Y-m-d');
						break;
					case 'tomorrow':
						value = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, 1), 'Y-m-d');
						break;
					case 'last week':
						value = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, -7), 'Y-m-d');
						break;
					case 'next week':
						value = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, 7), 'Y-m-d');
						break;
					case 'last month':
						value = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.MONTH, -1), 'Y-m-d');
						break;
					case 'next month':
						value = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.MONTH, 1), 'Y-m-d');
						break;
					case 'last year':
						value = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, -1), 'Y-m-d');
						break;
					case 'next year':
						value = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, 1), 'Y-m-d');
						break;
					default:
						var formats = ['Y-m-d', 'g:i a', 'g:i A', 'G:i', 'Y-m-d g:i a', 'Y-m-d g:i A', 'Y-m-d G:i'];
						Ext.Array.each(formats, function(format) {
							var date = Ext.Date.parse(value, format);
							if (Ext.isDate(date)) {
								value = date;
								return false;
							}
						});
						break;
				}
				return value
			};

		if (form && form.items) {
			Ext.Array.each(form.items, function(item) {
				//tested item dependencies with dates and strings
				// item.dependencies = [{
				// 	action: "setBgColor",
				// 	actionOptions: "#0000FF",
				// 	condition: "contains",
				// 	id: "",
				// 	target: "u_status",
				// 	value: "N"
				// }, {
				// 	action: "setBgColor",
				// 	actionOptions: "#FF0000",
				// 	condition: "after",
				// 	id: "",
				// 	target: "sys_updated_on",
				// 	value: "2013-11-17"
				// }];
				if (item.name == col.dataIndex && item.dependencies && Ext.isArray(item.dependencies)) {
					styleRenderer = function(value, metaData, record) {
						Ext.Array.forEach(item.dependencies, function(dependency) {
							if (dependency.action == 'setBgColor') {
								var val = record.get(dependency.target),
									match = false,
									checkVal = dependency.value;

								if (Ext.isBoolean(val)) checkVal = Boolean(checkVal)
								if (Ext.isNumber(val)) checkVal = Number(checkVal)
								if (Ext.isDate(val)) checkVal = parseDate(checkVal)

								switch (dependency.condition) {
									case 'equals':
									case 'on':
										match = val == checkVal
										break;
									case 'notequals':
										match = val != checkVal
										break;
									case 'greaterThan':
									case 'after':
										match = val > checkVal
										break;
									case 'greaterThanOrEqual':
										match = val >= checkVal
										break;
									case 'lessThan':
									case 'before':
										match = val < checkVal
										break;
									case 'lessThanOrEqual':
										match = val <= checkVal
										break;
									case 'contains':
										match = val && val.indexOf(checkVal) > -1
										break;
									case 'notcontains':
										match = val && val.indexOf(checkVal) == -1
										break;
								}

								if (match) {
									metaData.style = 'background-color:' + dependency.actionOptions
								}
							}
						}, this)
					}
				}
			})
		}

		switch (col.uiType) {
			case 'NameField':
				col.renderer = function(value, metaData, record, rowIndex, colIndex, store, view) {
					styleRenderer(value, metaData, record)
					// if (view.ownerCt.columns[colIndex] && view.ownerCt.columns[colIndex].hidden) return '';
					var val = value.split('|&|');
					return (val[3] ? val[3] + ', ' : '') + (val[0] ? val[0] + ' ' : '') + (val[1] ? val[1] : '') + (val[2] ? val[2] + '.' : '');
				}
				break;
			case 'AddressField':
				col.renderer = function(value, metaData, record, rowIndex, colIndex, store, view) {
					styleRenderer(value, metaData, record)
					// if (view.ownerCt.columns[colIndex] && view.ownerCt.columns[colIndex].hidden) return '';
					var val = value.split('|&|');
					return (val[0] ? val[0] + '<br/>' : '') + (val[1] ? val[1] + '<br/>' : '') + (val[2] ? val[2] + ' ' : '') + (val[3] ? val[3] + ' ' : '') + (val[4] ? val[4] : '') + (val[5] ? '<br/>' + val[5] : '');
				}
				break;
			case 'DateField':
			case 'Date':
				col.renderer = function(value, metaData, record, rowIndex, colIndex, store, view) {
					styleRenderer(value, metaData, record)
					return Ext.util.Format.date(value, 'Y-m-d')
				}
				break;
			case 'TimeField':
			case 'Timestamp':
				col.renderer = function(value, metaData, record, rowIndex, colIndex, store, view) {
					styleRenderer(value, metaData, record)
					return Ext.util.Format.date(value, 'g:i:s A')
				}
				break;
			case 'DateTimeField':
			case 'DateTime':
				col.renderer = function(value, metaData, record, rowIndex, colIndex, store, view) {
					styleRenderer(value, metaData, record)
					return Ext.util.Format.date(value, 'Y-m-d g:i:s A')
				}
				break;

			case 'CheckBox':
			case 'Checkbox':
			case 'CheckboxField':
				col.renderer = function(value, metaData, record, rowIndex, colIndex, store, view) {
					styleRenderer(value, metaData, record)
					// if (view.ownerCt.columns[colIndex] && view.ownerCt.columns[colIndex].hidden) return '';
					return value === true ? RS.common.locale.trueText : RS.common.locale.falseText;
				}
				break;
			case 'Journal':
				col.renderer = function(value, metaData, record, rowIndex, colIndex, store, view) {
					styleRenderer(value, metaData, record)
					try {
						var entries = Ext.decode(value),
							notes = [];
						Ext.Array.forEach(entries, function(entry) {
							notes.push(entry.note)
						})
						return notes.join('<br/>')
					} catch (e) {}
					return value
				}
				break;
			default:
				col.renderer = function(value, metaData, record, rowIndex, colIndex, store, view) {
					styleRenderer(value, metaData, record)
					// if (view.ownerCt.columns[colIndex] && view.ownerCt.columns[colIndex].hidden) return '';
					return Ext.String.htmlEncode(value);
				}
				break;
		}

		// if(initialConfig && record.columnModelName.indexOf('sys_') == 0) col.hidden = true;  //Took out auto-hide columns because it was causing problems for duke
		modelFields.push(storeCol);
		columns.push(col);
	});
}

(function() {
	Ext.ns('RS.common.grid');

	var RSFormat = RS.common.grid = RS.common.grid || {};

	Ext.apply(RSFormat, {

		getSysColumns: function() {
			var defaultDateFormat = Ext.state.Manager.get('userDefaultDateFormat', 'Y-m-d G:i:s');
			return [{
					text: RS.common.locale.sysCreated,
					dataIndex: 'sysCreatedOn',
					hidden: true,
					filterable: true,
					sortable: true,
					groupable: false,
					renderer: Ext.util.Format.dateRenderer(defaultDateFormat)
				}, {
					text: RS.common.locale.sysCreatedBy,
					dataIndex: 'sysCreatedBy',
					hidden: true,
					filterable: true,
					sortable: true,
					groupable: false,
					width: 120
				}, {
					text: RS.common.locale.sysUpdated,
					dataIndex: 'sysUpdatedOn',
					hidden: true,
					filterable: true,
					sortable: true,
					groupable: false,
					width: 200,
					renderer: Ext.util.Format.dateRenderer(defaultDateFormat)
				}, {
					text: RS.common.locale.sysUpdatedBy,
					dataIndex: 'sysUpdatedBy',
					hidden: true,
					filterable: true,
					sortable: true,
					groupable: false,
					width: 120
				},
				/*{
				text: RS.common.locale.sysOrg,
				dataIndex: 'sysOrganizationName',
				hidden: true,
				filterable: false,
				sortable: false,
				width: 130
			}, */
				{
					text: RS.common.locale.sysId,
					dataIndex: 'id',
					hidden: true,
					autoWidth: true,
					sortable: false,
					filterable: false,
					groupable: false,
					width: 300
				}
			]
		},

		getSysFields: function() {
			return ['sysCreatedBy', 'sysUpdatedBy', 'sysOrg', 'id', {
				name: 'sysCreatedOn',
				type: 'date',
				dateFormat: 'time',
				format: 'time'
			}, {
				name: 'sysUpdatedOn',
				type: 'date',
				dateFormat: 'time',
				format: 'time'
			}]
		},

		updateSysFields: function(obj, data) {
			if (data) {
				if (Ext.isDefined(obj.sysCreatedBy)) obj.set('sysCreatedBy', data.sysCreatedBy)
				if (Ext.isDefined(obj.sysUpdatedBy)) obj.set('sysUpdatedBy', data.sysUpdatedBy)
				if (Ext.isDefined(obj.sysOrganizationName)) obj.set('sysOrganizationName', data.sysOrganizationName)
				if (Ext.isDefined(obj.id)) obj.set('id', data.id)
				if (Ext.isDefined(obj.sysCreatedOn)) obj.set('sysCreatedOn', data.sysCreatedOn)
				if (Ext.isDefined(obj.sysUpdatedOn)) obj.set('sysUpdatedOn', data.sysUpdatedOn)

				//legacy support which shouldn't exist but has to because the backend can't be made consistent
				if (Ext.isDefined(obj.sys_created_by)) obj.set('sysCreatedBy', data.sys_created_by)
				if (Ext.isDefined(obj.sys_updated_by)) obj.set('sysUpdatedBy', data.sys_updated_by)
				if (Ext.isDefined(obj.sys_id)) obj.set('id', data.sys_id)
				if (Ext.isDefined(obj.sys_created_on)) obj.set('sysCreatedOn', data.sys_created_on)
				if (Ext.isDefined(obj.sys_updated_on)) obj.set('sysUpdatedOn', data.sys_updated_on)
			}
		},

		getCustomSysColumns: function() {
			var defaultDateFormat = Ext.state.Manager.get('userDefaultDateFormat', 'Y-m-d G:i:s');
			return [{
				text: RS.common.locale.sysCreated,
				dataIndex: 'sys_created_on',
				hidden: false,
				filterable: true,
				sortable: true,
				flex: 1,
				renderer: Ext.util.Format.dateRenderer(defaultDateFormat)
			}, {
				text: RS.common.locale.sysCreatedBy,
				dataIndex: 'sys_created_by',
				hidden: true,
				filterable: true,
				sortable: true,
				width: 120
			}, {
				text: RS.common.locale.sysUpdated,
				dataIndex: 'sys_updated_on',
				hidden: true,
				filterable: true,
				sortable: true,
				width: 200,
				renderer: Ext.util.Format.dateRenderer(defaultDateFormat)
			}, {
				text: RS.common.locale.sysUpdatedBy,
				dataIndex: 'sys_updated_by',
				hidden: true,
				filterable: true,
				sortable: true,
				width: 120
			}, {
				text: RS.common.locale.sysId,
				dataIndex: 'sys_id',
				hidden: true,
				autoWidth: true,
				sortable: false,
				filterable: false,
				width: 300
			}]
		},

		getCustomSysFields: function() {
			return ['sys_created_by', 'sys_updated_by', 'sys_id', {
				name: 'sys_created_on',
				type: 'date',
				dateFormat: 'time',
				format: 'time'
			}, {
				name: 'sys_updated_on',
				type: 'date',
				dateFormat: 'time',
				format: 'time'
			}]
		}
	})
}());
(function() {
	Ext.ns('RS.common.grid');

	var RSFormat = RS.common.grid = RS.common.grid || {};

	Ext.apply(RSFormat, {
		/**
		 * Formats the passed string using the specified number of lines.
		 * @param {String} value The value to format.
		 * @param {Number} [numberOfLines] The number of lines to display on the client before elipsing the text
		 * @return {String} The formatted log string.
		 */
		log: function(v, numberOfLines, htmlEncode) {
			if (!v) return v
			var content = v.split(/\n|<br\/>|<\/br>|<br>/g);
		if(htmlEncode)
			return Ext.util.Format.htmlEncode(content.slice(0, numberOfLines).join('<br/>') + (content.length > numberOfLines ? '<br/>&hellip;' : ''));
		else
			return content.slice(0, numberOfLines).join('<br/>') + (content.length > numberOfLines ? '<br/>&hellip;' : '');
		},

		/**
		 * Returns a log format rendering function that can be reused to apply a log string multiple times efficiently
		 * @param {Number} numberOfLines The number of lines to display on the grid before elipsing the text
		 * @return {Function} The log formatting function
		 */
		logRenderer: function(numberOfLines,htmlEncode) {
			return function(v) {
				return RSFormat.log(v, numberOfLines,htmlEncode)
			}
		},

		booleanRenderer: function() {
			return function(value) {
				if (!value || value === 'false') return '';
				return '<span class="icon-large icon-ok-sign rs-boolean-renderer"></span>'
			}
		},

		htmlRenderer: function() {
			return function(v) {
				var doc = document.createElement('div');
				doc.innerHTML = v;
				if (doc.innerHTML !== v) return Ext.String.htmlEncode(v)
				return v
			}
		},

		plainRenderer: function() {
			return function(v) {
				return Ext.util.Format.htmlEncode(v)
			}
		},

		statusRenderer: function(prefix) {
			return function(value, metaData) {
				metaData.style = 'margin: 1px'
				metaData.tdCls = Ext.String.format('{0}-{1}', prefix, value && value.toLowerCase ? value.toLowerCase() : value)
				return value
			}
		},

		linkRenderer: function(modelName, idParam, target) {
			return function(value) {
				if (value)
					return Ext.String.format('<a target="{0}" href="{1}#{2}/{3}={4}">{4}</a>', target, target != '_self' ? window.location.href.split('#')[0] : '', modelName, idParam || 'id', value)
				return ''
			}
		},

		internalLinkRenderer: function(modelName, idParam, valueParam) {
			return function(value, metaData, record) {
				if (value)
					return Ext.String.format('<a class="rs-link" href="#{0}/{1}={2}">{3}</a>', modelName, idParam || 'id', record.get(valueParam), value)
				return ''
			}
		},

		downloadLinkRenderer: function(linkParam) {
			return function(value, metaData, record) {
				if (!value)
					return '';
				return Ext.String.format('<a class="rs-link" href="#" onclick="downloadFile(\'{1}\');return false">{0}</a>', value, record.get(linkParam))
			}
		},

		downloadRenderer: function() {
			return function(value) {
				return Ext.String.format('<a href="#" class="icon-large icon-download rs-icon" onclick="downloadFile(\'{0}\');return false"></div>', value)
			}
		},

		updatedDateRenderer: function() {
			return function(value) {
				if (Ext.isDate(value)) {
					var now = new Date(),
						date = value,
						diff = (now - date) / 1000;

					if (diff < 60) //1 minute
						return Ext.String.format('{0} {1} {2}', Math.round(diff), RS.common.locale.seconds, RS.common.locale.ago)
					if (60 < diff && diff < 60 * 2) // > 1 minute but < 2 minutes
						return Ext.String.format('{0} {1} {2}', 1, RS.common.locale.minute, RS.common.locale.ago)
					if (60 * 2 < diff && diff < 60 * 60) // > 2 minutes but < 60 minutes
						return Ext.String.format('{0} {1} {2}', Math.round(diff / 60), RS.common.locale.minutes, RS.common.locale.ago)
					if (60 * 60 < diff && diff < 2 * 60 * 60) // > 1 hour but < 2 hours
						return Ext.String.format('{0} {1} {2}', 1, RS.common.locale.hour, RS.common.locale.ago)
					if (2 * 60 * 60 < diff && diff < 24 * 60 * 60) // > 2 hours but < 1 day
						return Ext.String.format('{0} {1} {2}', Math.round(diff / (60 * 60)), RS.common.locale.hours, RS.common.locale.ago)
					if (24 * 60 * 60 < diff && diff < 2 * 24 * 60 * 60) // > 24 hours but < 48 hours
						return Ext.String.format('{0}', RS.common.locale.yesterday)
					if (2 * 24 * 60 * 60 < diff && diff < 7 * 24 * 60 * 60)
						return Ext.String.format('{0} {1} {2}', Math.round(diff / (24 * 60 * 60)), RS.common.locale.days, RS.common.locale.ago)

					return Ext.Date.format(value, Ext.state.Manager.get('userDefaultDateFormat', 'c'))
				}
				return value
			}
		}
	})
}());
/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.common').locale = {
	error: 'Error',
	success: 'Success',
	close: 'Close',
	deleteText: 'Delete',
	deleteTitle: 'Confirm Delete',

	pageInfo: 'Page Info',

	deleteFilterMessage: 'Are you sure you want to delete the selected filter?  This action cannot be undone.',
	deleteFiltersMessage: 'Are you sure you want to delete the {0} selected filters?  This action cannot be undone.',
	deleteViewMessage: 'Are you sure you want to delete the selected view?  This action cannot be undone.',
	deleteViewsMessage: 'Are you sure you want to delete the {0} selected views?  This action cannot be undone.',

	invalidJSON: 'The server replied with an invalid JSON string',

	//Paging
	pageDisplayText: '{0} - {1} of {2}',
	firstText: 'First Page',
	prevText: 'Previous Page',
	nextText: 'Next Page',
	lastText: 'Last Page',
	refreshText: 'Refresh',
	configureText: 'Configure',
	emptyMsg: 'No Records',
	goToText: 'Go to',

	//Auto Refresh
	autoRefreshText: 'Auto Refresh (secs)',

	//Search
	egText: '  e.g. Name equals John Doe',
	noSuggestions: 'No suggestions for your search, try searching by a column name',

	search: 'Search',

	//Conditions
	on: 'on',
	after: 'after',
	before: 'before',
	equals: 'equals',
	notEquals: 'not equals',
	contains: 'contains',
	notContains: 'not contains',
	startsWith: 'starts with',
	notStartsWith: 'not starts with',
	endsWith: 'ends with',
	notEndsWith: 'not ends with',
	greaterThan: 'greater than',
	greaterThanOrEqualTo: 'greater than or equal to',
	lessThan: 'less than',
	lessThanOrEqualTo: 'less than or equal to',
	shortEquals: '=',
	shortNotEquals: '!=',
	shortGreaterThan: '>',
	shortGreaterThanOrEqualTo: '>=',
	shortLessThan: '<',
	shortLessThanOrEqualTo: '<=',

	//Values
	today: 'today',
	yesterday: 'yesterday',
	lastWeek: 'last week',
	lastMonth: 'last month',
	lastYear: 'last year',

	//Labels
	Query: 'Query',
	Filter: 'Filter',
	manageFilters: 'Manage Filters',
	SelectFilter: 'Please select a filter',
	filterDeleted: 'Filter deleted successfully',
	filtersDeleted: 'Filters deleted successfully',

	//Filter Toolbar
	saveFilter: 'Save',
	editFilter: 'Edit',
	clearFilter: 'Clear',
	saveFilterTitle: 'Save Filter',
	saveFilterMessage: 'Please enter the name of the filter:',
	cancel: 'Cancel',
	saveFilterTitleConfirm: 'Filter already exists',
	saveFilterMessageConfirm: 'A filter with the name <b>{0}</b> already exist, are you sure you want to overwrite that existing filter?',
	saveFilterConfirm: 'Overwrite',
	filterSaved: 'Filter was saved successfully',
	filterTitle: 'Filters',

	// View Toolbar
	saveViewTitle: 'Save View',
	saveViewMessage: 'Please enter the name of the view:',
	saveViewTitleConfirm: 'View already exists',
	saveViewMessageConfirm: 'A view with the name <b>{0}</b> already exist, are you sure you want to overwrite that existing view?',
	saveView: 'Save View',
	saveViewConfirm: 'Overwrite',
	viewSaved: 'View was saved successfully',
	saveSystemViewTitle: 'System View already exists',
	saveSystemViewBody: 'You cannot save a view with that name because a system view with that name already exists.  Please choose a different name.',
	views: 'Views',
	viewTitle: 'Views',
	settings: 'Settings',
	manage: 'Manage',
	saveViewAs: 'Save View As',

	//View Column
	viewColumnTooltip: 'Go To',
	selectAll: 'Select all pages ({0})',
	selectAllTooltip: 'Select All',

	//Boolean Column
	trueText: 'True',
	falseText: 'False',

	//Edit Column
	editColumnTooltip: 'Edit',
	gotoDetailsColumnTooltip: 'View Details',

	//Updated Date Renderer
	seconds: 'seconds',
	minute: 'minute',
	minutes: 'minutes',
	hour: 'hour',
	hours: 'hours',
	yesterday: 'Yesterday',
	days: 'days',
	ago: 'ago',

	//SysInfoButton
	sysInfoButtonTooltip: 'System Information',
	sysInfoButtonTitle: 'System Information',
	sysId: 'System ID',
	sysCreated: 'Created On',
	sysCreatedBy: 'Created By',
	sysUpdated: 'Updated On',
	sysUpdatedBy: 'Updated By',
	sysOrg: 'Organization',
	unknownDate: 'Unknown Date',
	sysCreatedOn: 'Created On',
	sysUpdatedOn: 'Updated On',
	sysOrganizationName: 'Organization',
	sys_id: 'Sys ID',
	createdOn: 'Created On',
	updatedOn: 'Updated On',

	//Follow Button
	followButtonTooltip: 'Configure Social Followers',


	//Grid Picker
	dump: 'OK',
	keyword: 'Search',

	//Access Right Picker
	accessRights: 'Access Rights',
	add: 'Add',
	Roles: 'Roles',
	showRoleList: 'Add Roles',
	removeAccessRight: 'Remove Roles',
	name: 'Name',
	adminRight: 'Admin',
	writeRight: 'Edit',
	readRight: 'View',
	executeRight: 'Execute',
	rolesPicker: 'Roles',
	uname: 'Name',
	udescription: 'Description',
	rolesDisplayName: 'Roles',
	defaultRolesText: 'Apply Default Roles',
	//wiki search widget
	ufullname: 'Full Name',
	WikiDocSearch: {
		title: 'Search Wiki',
		filter: 'Filter the wiki doc...',
		type: 'Search type',
		choose: 'Select'
	},

	//Encryption/Decryption
	decriptionTitle: 'Decryption Key Required',
	decriptionMessage: 'Enter the decryption key:',
	invalidKeyTitle: 'Invalid decryption key',
	invalidKeyMessage: 'The key you entered was invalid.',
	decryptErrorTitle: 'An error occurred while decrypting',
	decryptErrorMessage: 'There was an error while decrypting with the provided key.',

	//Uploader
	overwriteTitle: 'File already exists',
	overwriteMsg: 'Do you want to overwrite {0}?',
	overwriteAttachment: 'Overwrite',
	download: 'Download',

	// Single File Uploader 
	selectAWordDocFile: 'Browse..', 
	fileName: 'File name',
	upload: 'Upload',
	succeeded: 'Succeeded',
	failed: 'Failed',
	succeededUploadingWordDoc: 'Succeeded uploading a word docx.',
	failedUploadingWordDoc: 'Failed uploading a word docx.',
	uploadAWordDocument: 'Upload a Word Document (docx)',
	uploadWordDocument: 'Upload Word Document (docx)',
	noFileSelected: 'no file selected.',
	pleaseSelectAWordDoc: ' Please select a word docx.',
	updating: 'File being uploaded...',
	notAllowedCharacters: '\'<b>{1}</b>\' contains one or more special characters \'<b>{0}</b>\' which are not supported.<br>',

	ContentPicker : {
		page : 'Page',
		automation : 'Automation',
		playbook : 'Playbook Template',
		namespace : 'Namespace',
		all : 'All'
	},

	FileUploadManager: {
		dragText: 'DROP FILES HERE',
		deleteFailed: 'Delete failed',
		uploadFailed: 'Upload failed',
		uploadSuccess: 'Upload successful',
		duplicateWarn: 'Duplicate detected',
		duplicateDetected: 'is already in the attachment list.',
		duplicatesDetected: 'are already in the attachment list.',
		override: 'Override',

		processingDroppedFiles: 'Processing dropped files...',
		editFilename: 'Edit filename',
		cancel: 'Cancel',
		retry: 'Retry',
		delete: 'Delete',
		close: 'Close',
		no: 'No',
		yes: 'Yes',
		ok: 'Ok'
	},

	UploadWordDoc: {
		browse: 'Browse',
		dragHere: 'Drop your Word Document (docx) here',
		uploadIntro: 'Please click "Browse" button to select a Word Document for uploading or drag the file into the area below.',
	}
}

/*The MIT License (MIT)

Copyright (c) 2014 Guilherme Lopes Portela

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.*/

/*
 * File: DateTimePicker.js
 *
 * This file requires use of the Ext JS library, under independent license.
 * This is part of the UX for DateTimeField developed by Guilherme Portela
 */
 
Ext.define('Ext.ux.DateTimePicker', {
    extend: 'Ext.picker.Date',
    alias: 'widget.datetimepicker',
    requires: [
        'Ext.picker.Date',
        'Ext.slider.Single',
        'Ext.form.field.Time',
        'Ext.form.Label'
    ],
    
    // <locale>
    /**
     * @cfg {String} todayText
     * The default text that will be displayed in the calendar to pick the curent date.
     */
    todayText: 'Current Date',
    // </locale>
    // <locale>
    /**
     * @cfg {String} hourText
     * The default text displayed above the hour slider
     */
    hourText: 'Hour',
    // </locale>
    // <locale>
    /**
     * @cfg {String} minuteText
     * The default text displayed above the minute slider
     */
    minuteText : 'Minutes',
    // </locale>

    initEvents: function() {
        var me = this,
            eDate = Ext.Date,
            day = eDate.DAY;

        Ext.apply(me.keyNavConfig,{
            up: function(e) {
                if (e.ctrlKey) {
                    if (e.shiftKey) {
                        me.minuteSlider.setValue(me.minuteSlider.getValue() + 1);
                    } else {
                        me.showNextYear();
                    }
                } else {
                    if (e.shiftKey) {
                        me.hourSlider.setValue(me.hourSlider.getValue() + 1);
                    } else {
                        me.update(eDate.add(me.activeDate, day, - 7));
                    }
                }
            },

            down: function(e) {
                if (e.ctrlKey) {
                    if (e.shiftKey) {
                        me.minuteSlider.setValue(me.minuteSlider.getValue() - 1);
                    } else {
                        me.showPrevYear();
                    }
                } else {
                    if (e.shiftKey) {
                        me.hourSlider.setValue(me.hourSlider.getValue() - 1);
                    } else {
                        me.update(eDate.add(me.activeDate, day, 7));
                    }
                }
            }
        });
        me.callParent();
    },

    initComponent: function() {
        var me = this,
            dtAux = me.value ? new Date(me.value) : new Date();

        dtAux.setSeconds(0);
        
        me.timeFormat = me.format.indexOf("h") !== -1 ? 'h' : 'H';
        me.hourSlider = new Ext.slider.Single({
            fieldLabel: me.hourText,
            labelAlign: 'top',
            labelSeparator: ' ',
            padding: '0 0 10 17',
            focusable : false,
            value: 0,
            minValue: 0,
            maxValue: 23,
            vertical: true,
            listeners: {
                change: me.changeTimeValue,
                scope: me
            }
        });

        me.minuteSlider = new Ext.slider.Single({
            fieldLabel: me.minuteText,
            labelAlign: 'top',
            labelSeparator: ' ',
            padding: '0 10 10 0',
            focusable : false,
            value: 0,
            increment: 1,
            minValue: 0,
            maxValue: 59,
            vertical: true,
            listeners: {
                change: me.changeTimeValue,
                scope: me
            }
        });
        
        me.callParent();
        me.setValue(new Date(dtAux));
    },

    afterRender: function() {
        var me = this;

        me.timePicker = Ext.create('Ext.panel.Panel', {
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            border: false,
            defaults: {
                flex: 1
            },
            width: 130,
            floating: true,
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                ui: 'footer',
                items: [
                    '->', {
                        xtype: 'label',
                        text: me.timeFormat == 'h' ? '12:00 AM' : '00:00'
                    },
                    '->'
                ]
            }],
            items: [me.hourSlider, me.minuteSlider],
            onMouseDown: function(e) {
                e.preventDefault();
            },
        });

        me.callParent();
    },

    handleTabClick: function (e) {
        this.handleDateClick(e, this.activeCell.firstChild, true);
    },

    getSelectedDate: function (date) {
        var me = this,
            t = Ext.Date.clearTime(date,true).getTime(),
            cells = me.cells,
            cls = me.selectedCls,
            cellItems = cells.elements,
            cLen = cellItems.length,
            cell, c;

        cells.removeCls(cls);

        for (c = 0; c < cLen; c++) {
            cell = cellItems[c].firstChild;
            if (cell.dateValue === t) {
                return cell;
            }
        }
        return null;
    },

    changeTimeValue: function(slider, e, eOpts) {
        var me = this,
            label = me.timePicker.down('label'),
            minutePrefix = me.minuteSlider.getValue() < 10 ? '0' : '',
            hourDisplay = me.hourSlider.getValue(),
            pickerValue, hourValue, minuteValue, hourPrefix, timeSufix, auxValue;

        if (me.timeFormat == 'h') {
            timeSufix = me.hourSlider.getValue() < 12 ? ' AM' : ' PM';
            hourDisplay = me.hourSlider.getValue() < 13 ? hourDisplay : hourDisplay - 12;
            hourDisplay = hourDisplay || '12';
        }

        hourPrefix = hourDisplay < 10 ? '0' : '';

        label.setText(hourPrefix + hourDisplay + ':' + minutePrefix + me.minuteSlider.getValue() + (timeSufix || ''));

        if (me.pickerField && (pickerValue = me.pickerField.getValue())) {
            hourValue = me.hourSlider.getValue();
            minuteValue = me.minuteSlider.getValue();
            auxValue = new Date(pickerValue.setHours(hourValue, minuteValue));

            me.pickerField.setValue(auxValue);
        }
    },

    onShow: function() {
        var me = this;
        me.showTimePicker();
        me.callParent();
    },

    showTimePicker: function() {
        var me = this,
            el = me.el;

        Ext.defer(function() {
            var body = Ext.getBody(),
                bodyWidth = body.getViewSize().width,
                alignTo = (bodyWidth < (el.getX() + el.getWidth() + 140)) ? 'tl' : 'tr',
                xPos = alignTo == 'tl' ? -135 : 5,
                backgroundColor, toolbar;

            me.timePicker.setHeight(el.getHeight());
            me.timePicker.showBy(me, alignTo, [xPos, 0]);

            toolbar = me.timePicker.down('toolbar').getEl();
            backgroundColor = toolbar.getStyle('background-color');
            if (backgroundColor == 'transparent') {
                toolbar.setStyle('background-color', toolbar.getStyle('border-color'));
            }
        }, 1);
    },

    onHide: function() {
        var me = this;
        me.timePicker.hide();
        me.callParent();
    },

    beforeDestroy: function() {
        var me = this;

        if (me.rendered) {
            Ext.destroy(
                me.timePicker,
                me.minuteSlider,
                me.hourSlider
            );
        }
        me.callParent();
    },

    setValue: function(value) {
        value = value || new Date();
        value.setSeconds(0);
        this.value = new Date(value);
        return this.update(this.value);
    },

    selectToday: function() {
        var me = this,
            btn = me.todayBtn,
            handler = me.handler,
            auxDate = new Date();

        if (btn && !btn.disabled) {
            me.setValue(new Date(auxDate.setSeconds(0)));
            me.fireEvent('select', me, me.value);
            if (handler) {
                handler.call(me.scope || me, me, me.value);
            }
            me.onSelect();
        }
        return me;
    },

    handleDateClick: function(e, t, /*private*/ blockStopEvent) {
        var me = this,
            handler = me.handler,
            hourSet = me.timePicker.items.items[0].getValue(),
            minuteSet = me.timePicker.items.items[1].getValue(),
            auxDate = new Date(t.dateValue);

        if(blockStopEvent !== true) {
            e.stopEvent();
        }

        if (!me.disabled && t.dateValue && !Ext.fly(t.parentNode).hasCls(me.disabledCellCls)) {
            me.doCancelFocus = me.focusOnSelect === false;
            auxDate.setHours(hourSet, minuteSet, 0);
            me.setValue(new Date(auxDate));
            delete me.doCancelFocus;
            me.fireEvent('select', me, me.value);
            if (handler) {
                handler.call(me.scope || me, me, me.value);
            }
            me.onSelect();
        }
    },

    selectedUpdate: function(date) {
        var me = this,
            dateOnly = Ext.Date.clearTime(date, true),
            currentDate = (me.pickerField && me.pickerField.getValue()) || new Date();

        this.callParent([dateOnly]);

        if (currentDate) {
            Ext.defer(function() {
                me.hourSlider.setValue(currentDate.getHours());
                me.minuteSlider.setValue(currentDate.getMinutes());
            }, 10);

        }
    },

    fullUpdate: function(date) {
        var me = this,
            dateOnly = Ext.Date.clearTime(date, true),
            currentDate = (me.pickerField && me.pickerField.getValue()) || new Date();

        this.callParent([dateOnly]);

        if (currentDate) {
            Ext.defer(function() {
                me.hourSlider.setValue(currentDate.getHours());
                me.minuteSlider.setValue(currentDate.getMinutes());
            }, 10);
        }
    }
});



/*
 * File: DateTimeField.js
 *
 * This file requires use of the Ext JS library, under independent license.
 * This is part of the UX for DateTimeField developed by Guilherme Portela
 */
Ext.define('Ext.ux.DateTimeField', {
    extend: 'Ext.form.field.Date',
    alias: 'widget.datetimefield',
    requires: ['Ext.ux.DateTimePicker'],

    //<locale>
    /**
     * @cfg {String} format
     * The default date format string which can be overriden for localization support. The format must be valid
     * according to {@link Ext.Date#parse}.
     */
    format: "m/d/Y H:i",
    //</locale>
    //<locale>
    /**
     * @cfg {String} altFormats
     * Multiple date formats separated by "|" to try when parsing a user input value and it does not match the defined
     * format.
     */
    altFormats: "m/d/Y H:i:s|c",
    width: 270,

    mimicBlur: function(e) {
        var me = this,
            picker = me.picker;

        // ignore mousedown events within the picker element
        if (!picker || !e.within(picker.el, false, true) && !e.within(picker.timePicker.el, false, true)) {
            me.callParent(arguments);
        }
    },

    triggerBlur: function() {
        return false;
    },

    collapseIf: function(e) {
        var me = this,
            picker = me.picker;

        if (picker.timePicker && !e.within(picker.timePicker.el, false, true)) {
            me.callParent([e]);
        }
    },

    createPicker: function() {
        var me = this,
            parentPicker = this.callParent(),
            config = Ext.clone(Ext.merge(me.initialConfig, parentPicker.initialConfig)),
            excludes = ['renderTo', 'width', 'height', 'id', 'itemId'];

        // Avoiding duplicate ids error
        parentPicker.destroy();
        
        for (var i=0; i < excludes.length; i++) {
            if (config.hasOwnProperty([excludes[i]])) {
                delete config[excludes[i]];
            }
        }
        
        return Ext.create('Ext.ux.DateTimePicker', config);
    },

    getErrors: function(value) {
        value = arguments.length > 0 ? value : this.formatDate(this.processRawValue(this.getRawValue()));

        var me = this,
            format = Ext.String.format,
            errors = me.superclass.superclass.getErrors.apply(this, arguments),
            disabledDays = me.disabledDays,
            disabledDatesRE = me.disabledDatesRE,
            minValue = me.minValue,
            maxValue = me.maxValue,
            len = disabledDays ? disabledDays.length : 0,
            i = 0,
            svalue,
            fvalue,
            day,
            time;

        if (value === null || value.length < 1) { // if it's blank and textfield didn't flag it then it's valid
             return errors;
        }

        svalue = value;
        value = me.parseDate(value);
        if (!value) {
            errors.push(format(me.invalidText, svalue, Ext.Date.unescapeFormat(me.format)));
            return errors;
        }

        time = value.getTime();
        if (minValue && time < minValue.getTime()) {
            errors.push(format(me.minText, me.formatDate(minValue)));
        }

        if (maxValue && time > maxValue.getTime()) {
            errors.push(format(me.maxText, me.formatDate(maxValue)));
        }

        if (disabledDays) {
            day = value.getDay();

            for(; i < len; i++) {
                if (day === disabledDays[i]) {
                    errors.push(me.disabledDaysText);
                    break;
                }
            }
        }

        fvalue = me.formatDate(value);
        if (disabledDatesRE && disabledDatesRE.test(fvalue)) {
            errors.push(format(me.disabledDatesText, fvalue));
        }

        return errors;
    }
});
/*Apply extra data into destination object
	Example use 
	var extraData = {
		"resolveActionInvoc.resolveActionInvocOptions" : [{
			id : '',
			udescription : '',
			uname : 'INPUTFILE',
			uvalue : this.autoGenCode
		}],
		"resolveActionInvoc.resolveActionParameters" :  newTaskParam
	}
*/
RS.common.deepApply = function(destination, extraData){
	for(var path in extraData){
		var newData = extraData[path];
		var pathParts = path.split('.');
		var targetDataLocation = destination[pathParts[0]];
		for(var i = 1; i < pathParts.length; i++){
			targetDataLocation = targetDataLocation[pathParts[i]];
		}
		if(Array.isArray(targetDataLocation)){
			//Clear out default value if there is any.
			targetDataLocation.shift();
			Array.prototype.push.apply(targetDataLocation, newData);
		}
		else
			Ext.apply(targetDataLocation, newData);
	}
	return destination;
};

RS.common.decodeHTML = function (html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
};
RS.common.hexToRgb = function(hex) {
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
};
RS.common.saturateColor = function(color, amount){
	var color = color.toUpperCase().split('');
	var letters = '0123456789ABCDEF'.split('');
	for(var i = 0; i < color.length; i++){
		var newSaturation = 0;
		if(letters.indexOf(color[i]) + amount > 15) 
			newSaturation = 15;
		else if(letters.indexOf(color[i]) + amount < 0) 
			newSaturation = 0;
		else 
			newSaturation = letters.indexOf(color[i]) + amount;
		color[i] = letters[newSaturation];
	}
	return color.join('');
};
RS.common.parsePayload = function (response) {
	var payload = {};
	try {
		payload = Ext.JSON.decode(response.responseText);
	} catch (ex) {
		var msg = '';
		var errCode = '';
		if (response && Ext.isString(response.responseText) && response.responseText.indexOf('signinDiv') != -1 ) {
			msg = 'Session Timeout';
			errCode = 'SESSION_TIMEOUT';
		} else if (response.status == 403) {
			msg = 'Error 403 Access Denied/Forbidden';
			errCode = 'ACCESS_FORBIDDEN';
		} else {
			msg = ex.message;
			errCode = 'INVALID_SERVER_RESPONSE';
		}
		payload = {
			success: false,
			message: msg,
			errCode: errCode
		};
	}
	if (payload) {
		return payload;
	} else {
		var msg = response.statusText || 'Unknown error';
		return {
			success: false,
			message: msg,
			errCode: 'COMMUNICATION_TIMEOUT'
		};
	}
};
//Convert img src into data URI
RS.common.getDataUri = function(url, callback, scope) {
    var image = new Image();

    image.onload = function () {
        var canvas = document.createElement('canvas');
        canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
        canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size

        canvas.getContext('2d').drawImage(this, 0, 0);

        // Get raw image data
        //callback(canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''));

        // ... or get as Data URI
        callback.apply(scope ? scope : this, [canvas.toDataURL('image/png')]);
    };

    image.src = url;
};

RS.common.DEBUG = function(scope, msg) {
	if (window.console && window.console.log) {

		var listenerCount = 0;
		if (typeof(clientVM) != 'undefined') listenerCount = glu.getListenerCount(clientVM);
		else listenerCount = glu.getListenerCount(this);

		var timestamp = new Date().getTime();

		var debugMsg = timestamp + ' DEBUG: ListenerCount=' + listenerCount;

		if (scope.ns && scope.viewmodelName) debugMsg += ' ' + scope.ns + '.' + scope.viewmodelName;
		else if (scope.viewmodelName) debugMsg += ' ' + scope.viewmodelName;
		else if (scope.$className) debugMsg += ' ' + scope.$className;
		//else debugger;

		if (msg) debugMsg += ' : ' + msg;

		window.console.log(debugMsg);
	}
};

RS.common.mergeObject = function(objA, objB){
	var r = Ext.apply({}, objA);
	for(var property in objA){
		if(objB.hasOwnProperty(property))
			r[property] = objB[property];
	}
	return r;
};
/**
* vkBeautify - javascript plugin to pretty-print or minify text in XML, JSON, CSS and SQL formats.
*  
* Version - 0.99.00.beta 
* Copyright (c) 2012 Vadim Kiryukhin
* vkiryukhin @ gmail.com
* http://www.eslinstructor.net/vkbeautify/
* 
* Dual licensed under the MIT and GPL licenses:
*   http://www.opensource.org/licenses/mit-license.php
*   http://www.gnu.org/licenses/gpl.html
*
*   Pretty print
*
*        vkbeautify.xml(text [,indent_pattern]);
*        vkbeautify.json(text [,indent_pattern]);
*        vkbeautify.css(text [,indent_pattern]);
*        vkbeautify.sql(text [,indent_pattern]);
*
*        @text - String; text to beatufy;
*        @indent_pattern - Integer | String;
*                Integer:  number of white spaces;
*                String:   character string to visualize indentation ( can also be a set of white spaces )
*   Minify
*
*        vkbeautify.xmlmin(text [,preserve_comments]);
*        vkbeautify.jsonmin(text);
*        vkbeautify.cssmin(text [,preserve_comments]);
*        vkbeautify.sqlmin(text);
*
*        @text - String; text to minify;
*        @preserve_comments - Bool; [optional];
*                Set this flag to true to prevent removing comments from @text ( minxml and mincss functions only. )
*
*   Examples:
*        vkbeautify.xml(text); // pretty print XML
*        vkbeautify.json(text, 4 ); // pretty print JSON
*        vkbeautify.css(text, '. . . .'); // pretty print CSS
*        vkbeautify.sql(text, '----'); // pretty print SQL
*
*        vkbeautify.xmlmin(text, true);// minify XML, preserve comments
*        vkbeautify.jsonmin(text);// minify JSON
*        vkbeautify.cssmin(text);// minify CSS, remove comments ( default )
*        vkbeautify.sqlmin(text);// minify SQL
*
*/

(function() {


function createShiftArr(step) {

	var space = '    ';
	
	if ( isNaN(parseInt(step)) ) {  // argument is string
		space = step;
	} else { // argument is integer
		switch(step) {
			case 1: space = ' '; break;
			case 2: space = '  '; break;
			case 3: space = '   '; break;
			case 4: space = '    '; break;
			case 5: space = '     '; break;
			case 6: space = '      '; break;
			case 7: space = '       '; break;
			case 8: space = '        '; break;
			case 9: space = '         '; break;
			case 10: space = '          '; break;
			case 11: space = '           '; break;
			case 12: space = '            '; break;
		}
	}

	var shift = ['\n']; // array of shifts
	for(ix=0;ix<100;ix++){
		shift.push(shift[ix]+space); 
	}
	return shift;
}

function vkbeautify(){
	this.step = '    '; // 4 spaces
	this.shift = createShiftArr(this.step);
};

vkbeautify.prototype.xml = function(text,step) {

	var ar = text.replace(/>\s{0,}</g,"><")
				 .replace(/</g,"~::~<")
				 .replace(/\s*xmlns\:/g,"~::~xmlns:")
				 .replace(/\s*xmlns\=/g,"~::~xmlns=")
				 .split('~::~'),
		len = ar.length,
		inComment = false,
		deep = 0,
		str = '',
		ix = 0,
		shift = step ? createShiftArr(step) : this.shift;

		for(ix=0;ix<len;ix++) {
			// start comment or <![CDATA[...]]> or <!DOCTYPE //
			if(ar[ix].search(/<!/) > -1) { 
				str += shift[deep]+ar[ix];
				inComment = true; 
				// end comment  or <![CDATA[...]]> //
				if(ar[ix].search(/-->/) > -1 || ar[ix].search(/\]>/) > -1 || ar[ix].search(/!DOCTYPE/) > -1 ) { 
					inComment = false; 
				}
			} else 
			// end comment  or <![CDATA[...]]> //
			if(ar[ix].search(/-->/) > -1 || ar[ix].search(/\]>/) > -1) { 
				str += ar[ix];
				inComment = false; 
			} else 
			// <elm></elm> //
			if( /^<\w/.exec(ar[ix-1]) && /^<\/\w/.exec(ar[ix]) &&
				/^<[\w:\-\.\,]+/.exec(ar[ix-1]) == /^<\/[\w:\-\.\,]+/.exec(ar[ix])[0].replace('/','')) { 
				str += ar[ix];
				if(!inComment) deep--;
			} else
			 // <elm> //
			if(ar[ix].search(/<\w/) > -1 && ar[ix].search(/<\//) == -1 && ar[ix].search(/\/>/) == -1 ) {
				str = !inComment ? str += shift[deep++]+ar[ix] : str += ar[ix];
			} else 
			 // <elm>...</elm> //
			if(ar[ix].search(/<\w/) > -1 && ar[ix].search(/<\//) > -1) {
				str = !inComment ? str += shift[deep]+ar[ix] : str += ar[ix];
			} else 
			// </elm> //
			if(ar[ix].search(/<\//) > -1) { 
				str = !inComment ? str += shift[--deep]+ar[ix] : str += ar[ix];
			} else 
			// <elm/> //
			if(ar[ix].search(/\/>/) > -1 ) { 
				str = !inComment ? str += shift[deep]+ar[ix] : str += ar[ix];
			} else 
			// xml //
			if(ar[ix].search(/<\?/) > -1) { 
				str += shift[deep]+ar[ix];
			} else 
			// xmlns //
			if( ar[ix].search(/xmlns\:/) > -1  || ar[ix].search(/xmlns\=/) > -1) { 
				str += shift[deep]+ar[ix];
			} 
			
			else {
				str += ar[ix];
			}
		}
		
	return  (str[0] == '\n') ? str.slice(1) : str;
}

vkbeautify.prototype.json = function(text,step) {

	var step = step ? step : this.step;
	
	if (typeof JSON === 'undefined' ) return text; 
	
	if ( typeof text === "string" ) return JSON.stringify(JSON.parse(text), null, step);
	if ( typeof text === "object" ) return JSON.stringify(text, null, step);
		
	return text; // text is not string nor object
}

vkbeautify.prototype.css = function(text, step) {

	var ar = text.replace(/\s{1,}/g,' ')
				.replace(/\{/g,"{~::~")
				.replace(/\}/g,"~::~}~::~")
				.replace(/\;/g,";~::~")
				.replace(/\/\*/g,"~::~/*")
				.replace(/\*\//g,"*/~::~")
				.replace(/~::~\s{0,}~::~/g,"~::~")
				.split('~::~'),
		len = ar.length,
		deep = 0,
		str = '',
		ix = 0,
		shift = step ? createShiftArr(step) : this.shift;
		
		for(ix=0;ix<len;ix++) {

			if( /\{/.exec(ar[ix]))  { 
				str += shift[deep++]+ar[ix];
			} else 
			if( /\}/.exec(ar[ix]))  { 
				str += shift[--deep]+ar[ix];
			} else
			if( /\*\\/.exec(ar[ix]))  { 
				str += shift[deep]+ar[ix];
			}
			else {
				str += shift[deep]+ar[ix];
			}
		}
		return str.replace(/^\n{1,}/,'');
}

//----------------------------------------------------------------------------

function isSubquery(str, parenthesisLevel) {
	return  parenthesisLevel - (str.replace(/\(/g,'').length - str.replace(/\)/g,'').length )
}

function split_sql(str, tab) {

	return str.replace(/\s{1,}/g," ")

				.replace(/ AND /ig,"~::~"+tab+tab+"AND ")
				.replace(/ BETWEEN /ig,"~::~"+tab+"BETWEEN ")
				.replace(/ CASE /ig,"~::~"+tab+"CASE ")
				.replace(/ ELSE /ig,"~::~"+tab+"ELSE ")
				.replace(/ END /ig,"~::~"+tab+"END ")
				.replace(/ FROM /ig,"~::~FROM ")
				.replace(/ GROUP\s{1,}BY/ig,"~::~GROUP BY ")
				.replace(/ HAVING /ig,"~::~HAVING ")
				//.replace(/ SET /ig," SET~::~")
				.replace(/ IN /ig," IN ")
				
				.replace(/ JOIN /ig,"~::~JOIN ")
				.replace(/ CROSS~::~{1,}JOIN /ig,"~::~CROSS JOIN ")
				.replace(/ INNER~::~{1,}JOIN /ig,"~::~INNER JOIN ")
				.replace(/ LEFT~::~{1,}JOIN /ig,"~::~LEFT JOIN ")
				.replace(/ RIGHT~::~{1,}JOIN /ig,"~::~RIGHT JOIN ")
				
				.replace(/ ON /ig,"~::~"+tab+"ON ")
				.replace(/ OR /ig,"~::~"+tab+tab+"OR ")
				.replace(/ ORDER\s{1,}BY/ig,"~::~ORDER BY ")
				.replace(/ OVER /ig,"~::~"+tab+"OVER ")

				.replace(/\(\s{0,}SELECT /ig,"~::~(SELECT ")
				.replace(/\)\s{0,}SELECT /ig,")~::~SELECT ")
				
				.replace(/ THEN /ig," THEN~::~"+tab+"")
				.replace(/ UNION /ig,"~::~UNION~::~")
				.replace(/ USING /ig,"~::~USING ")
				.replace(/ WHEN /ig,"~::~"+tab+"WHEN ")
				.replace(/ WHERE /ig,"~::~WHERE ")
				.replace(/ WITH /ig,"~::~WITH ")
				
				//.replace(/\,\s{0,}\(/ig,",~::~( ")
				//.replace(/\,/ig,",~::~"+tab+tab+"")

				.replace(/ ALL /ig," ALL ")
				.replace(/ AS /ig," AS ")
				.replace(/ ASC /ig," ASC ")	
				.replace(/ DESC /ig," DESC ")	
				.replace(/ DISTINCT /ig," DISTINCT ")
				.replace(/ EXISTS /ig," EXISTS ")
				.replace(/ NOT /ig," NOT ")
				.replace(/ NULL /ig," NULL ")
				.replace(/ LIKE /ig," LIKE ")
				.replace(/\s{0,}SELECT /ig,"SELECT ")
				.replace(/\s{0,}UPDATE /ig,"UPDATE ")
				.replace(/ SET /ig," SET ")
							
				.replace(/~::~{1,}/g,"~::~")
				.split('~::~');
}

vkbeautify.prototype.sql = function(text,step) {

	var ar_by_quote = text.replace(/\s{1,}/g," ")
							.replace(/\'/ig,"~::~\'")
							.split('~::~'),
		len = ar_by_quote.length,
		ar = [],
		deep = 0,
		tab = this.step,//+this.step,
		inComment = true,
		inQuote = false,
		parenthesisLevel = 0,
		str = '',
		ix = 0,
		shift = step ? createShiftArr(step) : this.shift;;

		for(ix=0;ix<len;ix++) {
			if(ix%2) {
				ar = ar.concat(ar_by_quote[ix]);
			} else {
				ar = ar.concat(split_sql(ar_by_quote[ix], tab) );
			}
		}
		
		len = ar.length;
		for(ix=0;ix<len;ix++) {
			
			parenthesisLevel = isSubquery(ar[ix], parenthesisLevel);
			
			if( /\s{0,}\s{0,}SELECT\s{0,}/.exec(ar[ix]))  { 
				ar[ix] = ar[ix].replace(/\,/g,",\n"+tab+tab+"")
			} 
			
			if( /\s{0,}\s{0,}SET\s{0,}/.exec(ar[ix]))  { 
				ar[ix] = ar[ix].replace(/\,/g,",\n"+tab+tab+"")
			} 
			
			if( /\s{0,}\(\s{0,}SELECT\s{0,}/.exec(ar[ix]))  { 
				deep++;
				str += shift[deep]+ar[ix];
			} else 
			if( /\'/.exec(ar[ix]) )  { 
				if(parenthesisLevel<1 && deep) {
					deep--;
				}
				str += ar[ix];
			}
			else  { 
				str += shift[deep]+ar[ix];
				if(parenthesisLevel<1 && deep) {
					deep--;
				}
			} 
			var junk = 0;
		}

		str = str.replace(/^\n{1,}/,'').replace(/\n{1,}/g,"\n");
		return str;
}


vkbeautify.prototype.xmlmin = function(text, preserveComments) {

	var str = preserveComments ? text
							   : text.replace(/\<![ \r\n\t]*(--([^\-]|[\r\n]|-[^\-])*--[ \r\n\t]*)\>/g,"")
									 .replace(/[ \r\n\t]{1,}xmlns/g, ' xmlns');
	return  str.replace(/>\s{0,}</g,"><"); 
}

vkbeautify.prototype.jsonmin = function(text) {

	if (typeof JSON === 'undefined' ) return text; 
	
	return JSON.stringify(JSON.parse(text), null, 0); 
				
}

vkbeautify.prototype.cssmin = function(text, preserveComments) {
	
	var str = preserveComments ? text
							   : text.replace(/\/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+\//g,"") ;

	return str.replace(/\s{1,}/g,' ')
			  .replace(/\{\s{1,}/g,"{")
			  .replace(/\}\s{1,}/g,"}")
			  .replace(/\;\s{1,}/g,";")
			  .replace(/\/\*\s{1,}/g,"/*")
			  .replace(/\*\/\s{1,}/g,"*/");
}

vkbeautify.prototype.sqlmin = function(text) {
	return text.replace(/\s{1,}/g," ").replace(/\s{1,}\(/,"(").replace(/\s{1,}\)/,")");
}

window.vkbeautify = new vkbeautify();
}());


glu.mreg('ResolveScreen', {

	activeCard: 0,
	gridSelections: [],
	allSelected: false,
	mock: false,

	showDetails: function(sys_id) {
		this.set('activeCard', 1);
	},

	showList: function() {
		this.set('activeCard', 0);
	}

});
Ext.define('RS.overrides.grid.column.Action', {
    override: 'Ext.grid.column.Action',

    // overridden to implement
    defaultRenderer: function(v, meta, record, rowIdx, colIdx, store, view){
        var me = this,
            prefix = Ext.baseCSSPrefix,
            scope = me.origScope || me,
            items = me.items,
            len = items.length,
            i = 0,
            item, ret, disabled, tooltip, glyph, glyphParts, glyphFontFamily;

        // Allow a configured renderer to create initial value (And set the other values in the "metadata" argument!)
        // Assign a new variable here, since if we modify "v" it will also modify the arguments collection, meaning
        // we will pass an incorrect value to getClass/getTip
        ret = Ext.isFunction(me.origRenderer) ? me.origRenderer.apply(scope, arguments) || '' : '';
        meta.tdCls += ' ' + Ext.baseCSSPrefix + 'action-col-cell';
        for (; i < len; i++) {
            item = items[i];

            disabled = item.disabled || (item.isDisabled ? item.isDisabled.call(item.scope || scope, view, rowIdx, colIdx, item, record) : false);
            tooltip = disabled ? null : (item.tooltip || (item.getTip ? item.getTip.apply(item.scope || scope, arguments) : null));
            glyph = item.glyph;

            // Only process the item action setup once.
            if (!item.hasActionConfiguration) {

                // Apply our documented default to all items
                item.stopSelection = me.stopSelection;
                item.disable = Ext.Function.bind(me.disableAction, me, [i], 0);
                item.enable = Ext.Function.bind(me.enableAction, me, [i], 0);
                item.hasActionConfiguration = true;
            }

            if (glyph) {
                if (typeof glyph === 'string') {
                    glyphParts = glyph.split('@');
                    glyph = glyphParts[0];
                    glyphFontFamily = glyphParts[1];
                } else {
                    glyphFontFamily = Ext._glyphFontFamily;
                }

                ret += '<span role="button" title="' + (item.altText || me.altText) + '" class="' + prefix + 'action-col-icon ' + prefix + 'action-col-glyph ' + prefix + 'action-col-' + String(i) + ' ' + (disabled ? prefix + 'item-disabled' : ' ') +
                    ' ' + (Ext.isFunction(item.getClass) ? item.getClass.apply(item.scope || scope, arguments) : (item.iconCls || me.iconCls || '')) + '"' +
                    ' style="font-family:' + glyphFontFamily + '"' +
                    (tooltip ? ' data-qtip="' + tooltip + '"' : '') + '>&#' + glyph + ';</span>';
            } else {
               ret += (me.childLink ? '<a href="#' + me.childLink + '/' + me.childLinkIdProperty + '=' + record.get(me.childLinkIdProperty) + '">' : '') +
                    '<img alt="' + (item.altText || me.altText) + '" src="' + (item.icon || Ext.BLANK_IMAGE_URL) +
                    '" class="' + prefix + 'action-col-icon ' + prefix + 'action-col-' + String(i) + ' ' + (disabled ? prefix + 'item-disabled' : ' ') +
                    ' ' + (Ext.isFunction(item.getClass) ? item.getClass.apply(item.scope || scope, arguments) : (item.iconCls || me.iconCls || '')) + '"' + (tooltip ? ' data-qtip="' + tooltip + '"' : '') + ' />' + (me.childLink ? '</a>' : '');
            }
        }
        return ret;    
    }
});

Ext.define("RS.common.DefaultLoadingText", {
    override: "Ext.grid.View",
    loadingText: "LOADING"
});
Ext.define('RS.text.TBText',{
    override : 'Ext.toolbar.TextItem',
    htmlEncode : true,  
    beforeRender : function() {
        var me = this;
        me.callParent();
        Ext.apply(me.renderData, {
            text: me.htmlEncode ? Ext.String.htmlEncode(me.text) : me.text
        });
    },
    setText : function(text){
        var me = this;
        me.text = text;
        if (me.rendered) {
            var processedText = me.htmlEncode ? Ext.String.htmlEncode(text) : text;
            me.el.update(processedText);
            me.updateLayout();
        }
    }
});
Ext.define('RS.text.DisplayField',{
    override : 'Ext.form.field.Display',
    htmlEncode : true   
});
Ext.define('RS.grid.column.DefaultRenderer',{
    override : 'Ext.grid.column.Column',
    renderer : function(v){
        return Ext.String.htmlEncode(v);
    }
});
Ext.define('RS.grid.column.ColumnTemplateRenderer',{
   override : 'Ext.grid.column.Template',
   usingDefaultRenderer: true,
   renderer : function(value, meta, record) {
       var data = Ext.apply({}, record.data, record.getAssociatedData());
       return this.tpl.apply(data);
   }
});
Ext.define('RS.validation.InvalidNumber',{
    override : 'Ext.form.field.Number',
    nanText: 'Not a valid number',
});
Ext.define('RS.validation.InvalidDate',{
    override : 'Ext.form.field.Date',
    invalidText: "Not a valid date - it must be in the format {1}",
});
Ext.define('RS.validation.InvalidTime',{
    override : 'Ext.form.field.Time',
     invalidText: 'Not a valid time',
});
Ext.define('RS.text.FieldLabel',{
    override : 'Ext.form.field.Base',
    htmlLabel : false,
    trimLabelSeparator: function() {
        var me = this,
            separator = me.labelSeparator,
            label = (me.htmlLabel ? me.fieldLabel : Ext.String.htmlEncode(me.fieldLabel)) || '',
            lastChar = label.substr(label.length - 1);
        // if the last char is the same as the label separator then slice it off otherwise just return label value
        return lastChar === separator ? label.slice(0, -1) : label;
    }
});
Ext.define('RS.text.FieldContainerLabel',{
    override : 'Ext.form.FieldContainer',
    htmlLabel : false,
    getFieldLabel: function() {
        var label = (this.htmlLabel ? this.fieldLabel : Ext.String.htmlEncode(this.fieldLabel)) || '';
        if (!label && this.combineLabels) {
            label = Ext.Array.map(this.query('[isFieldLabelable]'), function(field) {
                return field.getFieldLabel();
            }).join(this.labelConnector);
        }
        return label;
    },
    trimLabelSeparator: function() {
        var me = this,
            separator = me.labelSeparator,
            label = (me.htmlLabel ? me.fieldLabel : Ext.String.htmlEncode(me.fieldLabel)) || '',
            lastChar = label.substr(label.length - 1);
        // if the last char is the same as the label separator then slice it off otherwise just return label value
        return lastChar === separator ? label.slice(0, -1) : label;
    }
});
Ext.define('RS.text.ButtonText',{
    override : 'Ext.button.Button',
    htmlEncode : true,
    setText: function(text) {     
        var me = this,
            oldText = me.text || '';

        if (text != oldText) {
            me.text = me.htmlEncode ? Ext.String.htmlEncode(text) : text;
            if (me.rendered) {
                me.btnInnerEl.update(me.text || '&#160;');
                me.setComponentCls();
                if (Ext.isStrict && Ext.isIE8) {
                    // weird repaint issue causes it to not resize
                    me.el.repaint();
                }
                me.updateLayout();
            }
            me.fireEvent('textchange', me, oldText, me.text);
        }
        return me;
    },
    initComponent : function(){
        var me = this;
        me.text = me.htmlEncode ? Ext.String.htmlEncode(me.text) : me.text;
        me.callParent();
    }
});
Ext.define('RS.text.HeaderTitle',{    
    override : 'Ext.panel.Header',
    htmlEncodeTitle : false,
    initComponent: function() {
        var me = this;
        var parentCont = me.up();
        var htmlEncodeTitle = parentCont ? parentCont.htmlEncodeTitle : false;
        me.title = htmlEncodeTitle ? Ext.String.htmlEncode(me.title) : me.title;
        me.callParent();       
    },
    setTitle: function(title) {
        var me = this,
            titleCmp = me.titleCmp;

        var parentCont= me.up();
        var htmlEncodeTitle = parentCont ? parentCont.htmlEncodeTitle : false;
        me.title = (htmlEncodeTitle ? Ext.String.htmlEncode(title) : title);
        if (titleCmp.rendered) {
            titleCmp.textEl.update(me.title || '&#160;');
            titleCmp.updateLayout();
        } else {
            me.titleCmp.on({
                render: function() {
                    me.setTitle(title);
                },
                single: true
            });
        }
    },
});
Ext.define('RS.text.ComboBox',{
    override : 'Ext.form.field.ComboBox',
    listConfig: {
		getInnerTpl: function(displayField) {
			return '{[Ext.String.htmlEncode(values.' + displayField + ')]}';
		}
	}
});

(function(){
    
var squishCls = Ext.id(null,'colAutoWidth');
    
Ext.util.CSS.createStyleSheet([
    'table.' + squishCls + '{ table-layout: auto !important; width: auto !important; }', 
    'table.' + squishCls + ' .x-grid-header-row th { width: auto !important; }',
    'table.' + squishCls + ' .x-grid-cell { width: auto !important; }',
    '.x-grid-header-ct.' + squishCls + ' .x-column-header { width: auto !important; }'
].join(''), squishCls );

Ext.define('Ext.ux.ColumnAutoWidthPlugin', {
    alias: 'plugin.columnautowidth',
    extend: 'Ext.AbstractPlugin',
    mixins: {observable: 'Ext.util.Observable'},
    autoUpdate: true,
    allColumns: false,
    constructor: function(config) {
        var me = this;
        Ext.apply(me, config);
        
        me.viewChangeDT = new Ext.util.DelayedTask(function(){ 
            me.refresh();
        }, 50);
        me.addEvents('beforecolumnresize', 'columnresize');
        me.mixins.observable.constructor.call(me);
        
    },
    init: function(grid) {
        var me = this;
        me.disable();
        me.grid = grid;
        grid.columnAutoWidthPlugin = me;
        me.enable();
    },
    destroy: function() {
        this.clearListeners();
    },
    enable: function() {
        var me = this,  grid = me.grid;
        
        if (me.autoUpdate && me.disabled && grid){
            var view = grid.getView();
            
            me.mon(view, 'refresh',         me.onViewChange, me );
            me.mon(view, 'itemadd',         me.onViewChange, me );
            me.mon(view, 'itemremove',      me.onViewChange, me );
            me.mon(view, 'itemupdate',      me.onViewChange, me );
            me.mon(view, 'afteritemexpand', me.onViewChange, me );
            
            me.mon(grid, 'columnshow', me.onColumnChange, me);//, { buffer: 100 });
        }
        
        me.callParent();
    },
    disable: function() {
       this.clearManagedListeners();
       this.callParent();
    },
    suspend: function(){
	this.suspendAutoSize = true;
    },
    resume: function(refresh){
        this.suspendAutoSize = false;
        if ( refresh ) this.viewChangeDT.delay(300);
    },
    onColumnChange: function(ct, column) {
        if( this.suspendAutoSize ) return;
        // console.log('ColumnAutoWidthPlugin','onColumnChange');
        if ( column.autoWidth ) this.doAutoSize([column]); 
    },
    onViewChange: function() {
        this.viewChangeDT.delay(300);
    },
    refresh: function() {
        var me = this, grid = me.grid;
        if ( me.suspendAutoSize || !grid.rendered || !grid.getView().rendered ) return;
        
        if( grid.view.isExpandingOrCollapsing ){ 
            me.viewChangeDT.delay(300);
            return;
        }
        
        var cols = me.getAutoCols();
        if ( cols.length ) me.doAutoSize( cols );
    },
    getAutoCols: function(){
        var me = this, cols = me.grid.columns, out = [], i = cols.length, col;
        
        while(i--){
            col = cols[i];
            if ( me.allColumns || col.autoWidth ) out.push(col);
        }
        
        return out;
    },
    getTableResizers: function() {
        var els = this.grid.getView().getEl().query( '.' + Ext.baseCSSPrefix + 'grid-table-resizer');
    
        // Grouping feature - first table wraps everything and can be ignored
        if (els.length > 1 && Ext.fly(els[0]).contains(els[1])) {
            els.shift();
        }
    
        return els;
    },
    getColumnResizers: function(column, config) {
        // Grab the <th> rows (one per table) that are used to size the columns
        var els = this.grid.getEl().query( '.' + Ext.baseCSSPrefix + 'grid-col-resizer-' + column.id);
 
        // Grouping feature - first table wraps everything and needs to be ignored
        if (els.length > 1 && Ext.fly(els[0]).parent('table').contains(els[1])) {
            els.shift();
        }
 
        return els;
    },
    getHeaderWidth: function(column){
        //var el = this.grid.el.down( '#' + column.id + ' .' + Ext.baseCSSPrefix + 'column-header-inner' );
        if(column.el){
            return column.el.getTextWidth() + column.el.getFrameWidth('lr');
        }
        
        return 0;
    },
    doAutoSize: function( resizeCols ){
        var me = this,
            view = me.grid.getView();
            
        if( me.suspendAutoSize ) return;
        
        var start = new Date().getTime();
        var restoreScroll = me.grid.getEl().cacheScrollValues();
        
        Ext.batchLayouts(function(){
            
            me.grid.headerCt.el.addCls( squishCls );
            var tableResizers = me.getTableResizers()
            // set the table resizers to auto
            Ext.each( tableResizers , function(el) {
                el = Ext.fly(el).addCls( squishCls );
            });
            // console.log('ColumnAutoWidthPlugin','autofy table resizers took', 0-start + (start = new Date().getTime()), 'ms');
            
            // no further dom changes beyond this point - to avoid reflows
            
            // console.log('ColumnAutoWidthPlugin','autofy column resizers took', 0-start + (start = new Date().getTime()), 'ms');
            
            Ext.each(resizeCols, function(col){
                var els = me.getColumnResizers(col), newWidth = 0;
                
                if( col.el ) newWidth = Ext.num(col.el.dom.scrollWidth,0);
                
                Ext.each(els, function(el) {
                    newWidth = Math.max(el.scrollWidth, newWidth); // scrollwidth should be cheaper
                });
                
                newWidth = Math.max( newWidth , col.minAutoWidth || 0 );
                if( col.maxAutoWidth ) newWidth = Math.min(col.maxAutoWidth, newWidth );
                
                if( newWidth == col.width ){
                    //
                } else if( col.el ){
                    col.setWidth( newWidth );
                } else {
                    col.width = newWidth;
                }
            });
            
            // console.log('ColumnAutoWidthPlugin','measure and set took', 0-start + (start = new Date().getTime()), 'ms');
            
            // put the table resizers back how you found them
            
            me.grid.headerCt.el.removeCls( squishCls );
            Ext.each( tableResizers , function(el) {
                el = Ext.fly(el).removeCls( squishCls );
            });
            //console.log('ColumnAutoWidthPlugin','restore table layout took', 0-start + (start = new Date().getTime()), 'ms');
        });
        
        restoreScroll();
        // console.log('ColumnAutoWidthPlugin','doAutoSize took', 0-start + (start = new Date().getTime()), 'ms');
        
    }
    
});

})();
Ext.define("RS.common.plugin.DragView", (function () { 
	var dragCls = 'x-view-sortable-drag',
		viewDragCls = 'x-view-sortable-dragging',
		dragData = null,
		view = null;

	function onMouseDown(e) {
		var result = false;
		dragData = null;

		try {
			var t = e.getTarget(view.itemSelector);
			var i = view.indexOf(t);
			var record = view.store.getAt(i);

			if (t && record) {
				dragData = {
					originalIndex: i,
					lastIndex: i,
					record: record
				};

				result = true;
			}
		} catch (ex) { 
			dragData = null;
		}

		return result;
	}

	function startDrag (x, y) {
		if (dragData) {
			var node = view.getNode(dragData.originalIndex);
			Ext.fly(node).addClass(dragCls);
			view.el.addClass(viewDragCls);
		}
	}

	function onDrag (e) {
		var result = false;

		if (dragData) { 
			try {
				var t = e.getTarget(view.itemSelector);

				if (t) {				
					var i = view.indexOf(t);

					if (i !== data.lastIndex) { 
						var store = view.store;
						var record = store.getAt(i);

						if (record) {
							dragData.lastIndex = i;
							store.remove(dragData.record);
							store.insert(i, [dragData.record]);
							var node = view.getNode(i);
							Ext.fly(node).addClass(dragCls);
							result = true;
						}
					}
				}
			} catch (ex) { 
				result = false; 
			}
		}

		return false;
	}

	function endDrag (e) {
		if (dragData) { 
			var node = view.getNode(dragData.lastIndex);
			Ext.fly(node).removeClass(dragCls);
			view.el.removeClass(viewDragCls);
			this.fireEvent('drop', dragData.origanlIndex, dragData.lastIndex, dragData.record);
		}

		return true;
	}

	return {
		extend: 'Ext.util.Observable',
		alias: 'plugin.dragview',
		events: {
			drop: true
		},

		dragDrop: null,

		init: function (v) {
			view = v;			
			view.on('render', this.onRender, this);
		},

		onRender : function() {
			this.callParent(arguments);
			this.dragDrop = new Ext.dd.DragDrop(view.el);
			this.dragDrop.onMouseDown = onMouseDown;
			this.dragDrop.startDrag = startDrag;
			this.dragDrop.onDrag = onDrag;
			this.dragDrop.endDrag = endDrag.bind(this);
		}		
	};
})());
Ext.define('Ext.ux.panel.header.ExtraIcons', {
	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.headericons',
	alternateClassName: 'Ext.ux.PanelHeaderExtraIcons',

	iconCls: '',
	index: undefined,

	headerButtons: [],

	init: function(panel) {
		this.panel = panel;
		this.callParent();
		panel.on('render', this.onAddIcons, this, {single: true});
	},

	onAddIcons :function () {
		if (this.panel.getHeader) {
			this.header = this.panel.getHeader();
		} else if (this.panel.getOwnerHeaderCt) {
			this.header = this.panel.getOwnerHeaderCt();
		}
		this.header.insert(this.index || this.header.items.length, this.headerButtons);
	}
});
Ext.define('RS.common.InfoIcon',{
	extend : 'Ext.container.Container',
	alias : 'widget.infoicon', 
    iconType : 'info',
    displayText : '',
	initComponent : function(){
        var iconCls = 'icon-info-sign';
        var iconStyle = {
            color: '#7be8fa',
            backgroundColor: '#e5f7fa',
            border: '1px solid #7be8fa',
            borderRadius : '3px',
            fontSize: '17px',
            textAlign : 'center'
        };
        if(this.iconType == 'warn'){
            iconCls  = 'icon-warning-sign';
            iconStyle = Ext.apply(iconStyle, {
                color: '#fab67b',
                backgroundColor: '#faf8ee',
                border: '1px solid #fab67b'             
           })
        }   
        Ext.apply(this, {
        	layout : {
        		type : 'hbox',
        		align : 'middle'
        	},
            items: [
            {
            	xtype : 'component',  
            	cls : iconCls,       	 
        	 	height: 30,
    			width: 30,
        		padding : 5,      	 
        	 	style: iconStyle
            },{
            	xtype : 'component',
            	margin : '0 0 0 10',
            	style : {
            		color : "#505050"
            	},
            	html : this.displayText
            }]
        });     
        this.callParent(arguments);
	}
})
Ext.define('RS.common.plugin.Pager', {
	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.pager',
	mixins: {
		bindable: 'Ext.util.Bindable'
	},

	pageable: true,
	allowAutoRefresh: true,
	showSysInfo: true,

	displayJumpMenu: true,

	autoRefreshInterval: 30,
	autoRefreshEnabled: false,

	showSocial: false,
	socialStreamType: '',

	init: function(grid) {
		var me = this,
			myGrid = grid;

		me.grid = myGrid

		if (me.grid.hidePager === true) {
			return
		}

		this.autoRefreshInterval = Ext.state.Manager.get('autoRefreshInterval', 30)
		this.autoRefreshEnabled = Ext.state.Manager.get('autoRefreshEnabled', false)


		me.displayMsg = RS.common.locale.pageDisplayText
		me.firstText = RS.common.locale.firstText
		me.prevText = RS.common.locale.prevText
		me.nextText = RS.common.locale.nextText
		me.lastText = RS.common.locale.lastText
		me.refreshText = RS.common.locale.refreshText
		me.configureText = RS.common.locale.configureText
		me.emptyMsg = RS.common.locale.emptyMsg
		me.systemInformation = RS.common.locale.sysInfoButtonTooltip
		me.systemInformationTooltip = RS.common.locale.sysInfoButtonTitle
		me.goToText = RS.common.locale.goToText

		//Auto refresh
		me.autoRefreshText = RS.common.locale.autoRefreshText;

		myGrid.dockedItems.each(function(dockedItem) {
			if (dockedItem.name == 'actionBar') {
				me.toolbar = dockedItem
				return false
			}
		})

		if (!myGrid.isXType('grid')) myGrid = myGrid.down('grid')

		if (me.pageable) myGrid.store.pageSize = myGrid.store.pageSize || 50
		else myGrid.store.pageSize = -1

		if (me.toolbar) {
			// me.toolbar.insert(0, [{
			// 	itemId: 'setup',
			// 	tooltip: me.configureText,
			// 	overflowText: me.configureText,
			// 	iconCls: Ext.baseCSSPrefix + 'tbar-configure',
			// 	scope: me,
			// 	menu: [{
			// 		text: 'Views'
			// 	}, {
			// 		text: 'Manage'
			// 	}, {
			// 		text: 'Admin Views'
			// 	},{text: 'Save Current As'}]
			// }, '-', ' ', ' ']);

			me.toolbar.add(['->'])

			if (me.pageable) me.toolbar.add([{
				xtype: 'tbtext',
				itemId: 'displayItem',
				listeners: {
					render: function(tbtext) {
						if (me.displayJumpMenu) {
							tbtext.getEl().on('click', function() {
								if (!tbtext.menu) {
									tbtext.menu = Ext.create('Ext.menu.Menu', {
										items: [{
											xtype: 'numberfield',
											itemId: 'pageNumberField',
											fieldLabel: me.goToText,
											labelWidth: 45,
											width: 120,
											minValue: 1,
											listeners: {
												change: function(field, newValue) {
													if (Ext.isNumber(newValue) && newValue > 0 && me.store.currentPage != newValue && newValue <= me.getPageData().pageCount)
														me.store.loadPage(newValue)
												}
											}
										}]
									})
								}
								tbtext.menu.down('#pageNumberField').setValue(me.store.currentPage)
								tbtext.menu.down('#pageNumberField').setMaxValue(me.getPageData().pageCount)
								tbtext.menu.showBy(tbtext)
							})
						}
					}
				}
			}, {
				itemId: 'first',
				tooltip: me.firstText,
				overflowText: me.firstText,
				iconCls: Ext.baseCSSPrefix + 'tbar-page-first',
				disabled: true,
				handler: me.moveFirst,
				scope: me
			}, {
				itemId: 'prev',
				tooltip: me.prevText,
				overflowText: me.prevText,
				iconCls: Ext.baseCSSPrefix + 'tbar-page-prev',
				disabled: true,
				handler: me.movePrevious,
				scope: me
			}, {
				itemId: 'next',
				tooltip: me.nextText,
				overflowText: me.nextText,
				iconCls: Ext.baseCSSPrefix + 'tbar-page-next',
				disabled: true,
				handler: me.moveNext,
				scope: me
			}, {
				itemId: 'last',
				tooltip: me.lastText,
				overflowText: me.lastText,
				iconCls: Ext.baseCSSPrefix + 'tbar-page-last',
				disabled: true,
				handler: me.moveLast,
				scope: me
			}, ' ', ' '])

			if (me.showSocial) {
				var socialButton = -1,
					i = 0,
					len = me.toolbar.items.length;
				for (; i < len; i++) {
					if (me.toolbar.items.getAt(i).iconCls && me.toolbar.items.getAt(i).iconCls.indexOf('rs-social-button') > -1) {
						socialButton = i
					}
				}

				if (socialButton > -1)
					me.toolbar.move(socialButton, me.toolbar.items.length)

				me.toolbar.add({
					xtype: 'followbutton',
					itemId: 'followButton',
					streamType: me.socialStreamType
				})
			}

			if (me.showSysInfo) {
				var showSysInfoButton = {
					xtype: 'button',
					ui: 'system-info-button-small',
					iconCls: 'rs-icon-button icon-info-sign' + (Ext.isGecko ? ' rs-icon-firefox' : ''),
					tooltip: me.systemInformation,
					overflowText: me.systemInformationTooltip,
					enableToggle: true,
					pressed: Ext.state.Manager.get('userShowSysInfoColumns', false),
					handler: me.toggleSysColumns,
					scope: myGrid
				}
				me.toolbar.add(showSysInfoButton)
				me.toggleSysColumns.apply(myGrid, [showSysInfoButton])
			}

			me.toolbar.add({
				xtype: me.allowAutoRefresh ? 'splitbutton' : 'button',
				itemId: 'refresh',
				tooltip: me.refreshText,
				overflowText: me.refreshText,
				iconCls: Ext.baseCSSPrefix + 'tbar-loading',
				handler: me.doRefresh,
				scope: me,
				menu: me.allowAutoRefresh ? {
					width: 175,
					items: [{
						text: me.autoRefreshText,
						checked: this.autoRefreshEnabled,
						checkHandler: function(item, checked) {
							var interval = item.ownerCt.down('numberfield').getValue();
							if (checked) {
								me.startAutoRefresh(interval || this.autoRefreshInterval)
								Ext.state.Manager.set('autoRefreshEnabled', true)
							} else {
								me.stopAutoRefresh()
								Ext.state.Manager.set('autoRefreshEnabled', false)
							}
						}
					}, {
						xtype: 'fieldcontainer',
						layout: 'hbox',
						items: [{
							xtype: 'numberfield',
							padding: '5 5 7 60',
							hideLabel: true,
							value: this.autoRefreshInterval,
							width: 60,
							minValue: 10,
							listeners: {
								change: function(field, value) {
									me.changeInterval(value);
								},
								blur: function(field, value) {
									if (this.getValue() < 10)
										this.setValue(10);
								}
							}
						}]
					}]
				} : null,
				listeners: {
					render: function(button) {
						if (!me.notActivateCtrl_R) {
							clientVM.updateRefreshButtons(button);
						}
					}
				}
			});

			if (this.autoRefreshEnabled) me.startAutoRefresh(this.autoRefreshInterval)

			me.toolbar.addEvents(
				/**
				 * @event change
				 * Fires after the active page has been changed.
				 * @param {Ext.toolbar.Paging} this
				 * @param {Object} pageData An object that has these properties:
				 *
				 * - `total` : Number
				 *
				 *   The total number of records in the dataset as returned by the server
				 *
				 * - `currentPage` : Number
				 *
				 *   The current page number
				 *
				 * - `pageCount` : Number
				 *
				 *   The total number of pages (calculated from the total number of records in the dataset as returned by the
				 *   server and the current {@link Ext.data.Store#pageSize pageSize})
				 *
				 * - `toRecord` : Number
				 *
				 *   The starting record index for the current page
				 *
				 * - `fromRecord` : Number
				 *
				 *   The ending record index for the current page
				 */
				'change',

				/**
				 * @event beforechange
				 * Fires just before the active page is changed. Return false to prevent the active page from being changed.
				 * @param {Ext.toolbar.Paging} this
				 * @param {Number} page The page number that will be loaded on change
				 */
				'beforechange');

			me.toolbar.on('beforerender', me.onLoad, me, {
				single: true
			});

			me.bindStore(myGrid.store || 'ext-empty-store', true);
		}
	},

	// @private
	updateInfo: function() {
		var me = this,
			displayItem = me.toolbar.child('#displayItem'),
			store = me.store,
			pageData = me.getPageData(),
			count, msg;

		if (displayItem) {
			count = store.getCount();
			if (count === 0) {
				msg = me.emptyMsg;
			} else {
				msg = Ext.String.format(
					me.displayMsg, pageData.fromRecord, pageData.toRecord, pageData.total);
			}
			displayItem.setText(msg);
		}
	},

	// @private
	beforeLoad: function() {
		if (this.rendered && this.refresh) {
			this.refresh.disable();
		}
	},

	// @private
	onLoadError: function() {
		if (!this.rendered) {
			return;
		}
		this.child('#refresh').enable();
	},

	// @private
	onLoad: function() {
		var me = this,
			pageData, currPage, pageCount, afterText, count, isEmpty;

		count = me.store.getCount();
		isEmpty = count === 0;
		if (!isEmpty) {
			pageData = me.getPageData();
			currPage = pageData.currentPage;
			pageCount = pageData.pageCount;
		} else {
			currPage = 0;
			pageCount = 0;
		}

		if (me.pageable) {
			Ext.suspendLayouts();
			me.toolbar.child('#first').setDisabled(currPage === 1 || isEmpty);
			me.toolbar.child('#prev').setDisabled(currPage === 1 || isEmpty);
			me.toolbar.child('#next').setDisabled(currPage === pageCount || isEmpty);
			me.toolbar.child('#last').setDisabled(currPage === pageCount || isEmpty);
			me.toolbar.child('#refresh').enable();
			me.updateInfo();
			Ext.resumeLayouts(true);
		} else {
			me.toolbar.child('#refresh').enable();
			me.updateInfo();
		}

		if (me.toolbar.rendered) {
			me.toolbar.fireEvent('change', me, pageData);
		}
	},

	// @private
	getPageData: function() {
		var store = this.store,
			totalCount = store.getTotalCount();

		return {
			total: totalCount,
			currentPage: store.currentPage,
			pageCount: Math.ceil(totalCount / store.pageSize),
			fromRecord: ((store.currentPage - 1) * store.pageSize) + 1,
			toRecord: Math.min(store.currentPage * store.pageSize, totalCount)

		};
	},

	/**
	 * Move to the first page, has the same effect as clicking the 'first' button.
	 */
	moveFirst: function() {
		var me = this;
		if (me.toolbar.fireEvent('beforechange', me, 1) !== false) {
			me.store.loadPage(1);
		}
	},

	/**
	 * Move to the previous page, has the same effect as clicking the 'previous' button.
	 */
	movePrevious: function() {
		var me = this,
			prev = me.store.currentPage - 1;

		if (prev > 0) {
			if (me.toolbar.fireEvent('beforechange', me, prev) !== false) {
				me.store.previousPage();
			}
		}
	},

	/**
	 * Move to the next page, has the same effect as clicking the 'next' button.
	 */
	moveNext: function() {
		var me = this,
			total = me.getPageData().pageCount,
			next = me.store.currentPage + 1;

		if (next <= total) {
			if (me.toolbar.fireEvent('beforechange', me, next) !== false) {
				me.store.nextPage();
			}
		}
	},

	/**
	 * Move to the last page, has the same effect as clicking the 'last' button.
	 */
	moveLast: function() {
		var me = this,
			last = me.getPageData().pageCount;

		if (me.toolbar.fireEvent('beforechange', me, last) !== false) {
			me.store.loadPage(last);
		}
	},

	/**
	 * Refresh the current page, has the same effect as clicking the 'refresh' button.
	 */
	doRefresh: function() {
		var me = this,
			current = me.store.currentPage;

		if (me.toolbar.fireEvent('beforechange', me, current) !== false) {
			me.store.loadPage(current);
		}
	},

	getStoreListeners: function() {
		return {
			beforeload: this.beforeLoad,
			load: this.onLoad,
			exception: this.onLoadError
		};
	},

	/**
	 * Unbinds the paging toolbar from the specified {@link Ext.data.Store} **(deprecated)**
	 * @param {Ext.data.Store} store The data store to unbind
	 */
	unbind: function(store) {
		this.bindStore(null);
	},

	/**
	 * Binds the paging toolbar to the specified {@link Ext.data.Store} **(deprecated)**
	 * @param {Ext.data.Store} store The data store to bind
	 */
	bind: function(store) {
		this.bindStore(store);
	},

	// @private
	destroy: function() {
		this.stopAutoRefresh()
		this.unbind()
		this.callParent()
	},

	changeInterval: function(interval) {
		if (this.autoRefreshTask) {
			this.stopAutoRefresh()
			this.startAutoRefresh(interval)
		}
		Ext.state.Manager.set('autoRefreshInterval', interval < 10 ? 10 : interval);
	},

	startAutoRefresh: function(interval) {
		if (this.autoRefreshTask) this.autoRefreshTask.destroy();

		var runner = new Ext.util.TaskRunner();
		this.autoRefreshTask = runner.newTask({
			run: this.doRefresh,
			scope: this,
			interval: interval * 1000
		});
		this.autoRefreshTask.start();
	},
	stopAutoRefresh: function() {
		if (this.autoRefreshTask) {
			this.autoRefreshTask.destroy();
			delete this.autoRefreshTask;
		}
	},

	toggleSysColumns: function(button) {
		Ext.state.Manager.set('userShowSysInfoColumns', button.pressed)
		Ext.Array.forEach(this.columns, function(column) {
			if ((column.dataIndex && column.dataIndex.indexOf('sys') == 0 && !column.initialShow) || column.dataIndex == 'id') {
				if (column.rendered)
					column[button.pressed ? 'show' : 'hide']()
				else column.hidden = !button.pressed
			} else if (column.dataIndex && Ext.Array.indexOf(['processNumber', 'duration', 'address', 'targetGUID', 'esbaddr'], column.dataIndex) > -1) {
				//This is for worksheet columns.  Total hack, but Duke wants these columns hidden/shown based on the sysinfo button too
				if (column.rendered)
					column[button.pressed ? 'show' : 'hide']()
				else column.hidden = !button.pressed
			}
		})
	},

	setSocialId: function(value) {
		this.toolbar.down('#followButton').streamId = value
	},
	setSocialStreamType: function(value) {
		this.toolbar.down('#followButton').streamType = value
	}
});
Ext.define('RS.common.plugin.RowExpander', {
	extend: 'Ext.grid.plugin.RowExpander',
	alias: 'plugin.resolveexpander',
	expandImg: '<img src="/resolve/images/group-expand.png"/>',
	collapseImg: '<img src="/resolve/images/group-collapse.png"/>',
	batchToggling: false,
	currentWidth: 500,
	expanderIndex: 1,
	init: function(grid) {
		var me = this;
		this.callParent([grid]);
		grid.addListener('render', function() {
			grid.on('cellclick', function(view, td, cellIndex, record, tr, rowIndex, e, eOpts) {
				if (cellIndex > me.expanderIndex) {
					me.onDblClick(view, record, null, rowIndex, e)
				}
			})
		});
		grid.addListener('afterlayout', function() {
			if (!grid.rendered)
				return;
			var divs = Ext.query('div[resolveId]')
			Ext.each(divs, function(div) {
				Ext.get(div).setWidth(grid.getWidth() - 50);
			});
		});
		var view = grid.getView();
		view.addListener('expandbody', function() {
			me.replaceHeaderNoFurtherAction();
		});
		view.addListener('collapsebody', function() {
			me.replaceHeaderNoFurtherAction();
		});
		grid.getStore().on('load', function() {
			this.recordsExpanded = {};
			this.replaceHeaderNoFurtherAction();
		}, this);
	},
	toggleAll: function() {
		var anyExpanded = this.replaceHeaderNoFurtherAction();

		var me = this,
			view = me.view,
			addOrRemoveCls = !anyExpanded ? 'removeCls' : 'addCls',
			ownerLock, rowHeight, fireView, rowNode, row, nextBd;
		// Suspend layouts because of possible TWO views having their height change
		Ext.suspendLayouts();

		var store = this.grid.getStore();
		store.each(function(r) {
			var idx = store.indexOf(r);
			rowNode = view.getNode(idx);
			row = Ext.fly(rowNode, '_rowExpander');
			nextBd = row.down(me.rowBodyTrSelector, true);
			row[addOrRemoveCls](me.rowCollapsedCls);
			Ext.fly(nextBd)[addOrRemoveCls](me.rowBodyHiddenCls);
			me.recordsExpanded[r.internalId] = !anyExpanded;
			view.refreshSize();
			// Sync the height and class of the row on the locked side
			if (me.grid.ownerLockable) {
				ownerLock = me.grid.ownerLockable;
				fireView = ownerLock.getView();
				view = ownerLock.lockedGrid.view;
				rowHeight = row.getHeight();
				// EXTJSIV-9848: in Firefox the offsetHeight of a row may not match
				// it's actual rendered height due to sub-pixel rounding errors. To ensure
				// the rows heights on both sides of the grid are the same, we have to set
				// them both.
				row.setHeight(anyExpanded ? rowHeight : '');
				row = Ext.fly(view.getNode(rowIdx), '_rowExpander');
				row.setHeight(anyExpanded ? rowHeight : '');
				row[addOrRemoveCls](me.rowCollapsedCls);
				view.refreshSize();
			} else {
				fireView = view;
			}
		}, this);

		if (fireView) {
			fireView.fireEvent(anyExpanded ? 'expandbody' : 'collapsebody', null, null, null);
		}
		// Coalesce laying out due to view size changes
		Ext.resumeLayouts(true);
		this.replaceHeaderNoFurtherAction();
	},
	addExpander: function() {
		var me = this,
			expanderGrid = me.grid,
			expanderHeader = me.getHeaderConfig();
		expanderHeader.text = this.expandImg;
		expanderHeader.width = 35;
		// If this is the normal side of a lockable grid, find the other side.
		if (expanderGrid.ownerLockable) {
			expanderGrid = expanderGrid.ownerLockable.lockedGrid;
			expanderGrid.width += expanderHeader.width;
		}
		expanderGrid.headerCt.insert(0, expanderHeader);
		var header = expanderGrid.headerCt.getHeaderAtIndex(0);
		header.on('headerclick', this.toggleAll, this);
	},
	replaceHeaderNoFurtherAction: function() {
		var anyExpanded = false;
		for (id in this.recordsExpanded)
			anyExpanded = anyExpanded || this.recordsExpanded[id]
		header = this.grid.headerCt.getHeaderAtIndex(this.expanderIndex);
		header.width = 35;
		header.setText(this[anyExpanded ? 'collapseImg' : 'expandImg']);
		return anyExpanded;
	}
});
Ext.define('RS.common.plugin.SearchFilter', {
	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.searchfilter',

	dateFormat: 'Y-m-d',
	timeFormat: 'g:i a',
	submitFormat: 'Y-m-d H:i:s',

	allowPersistFilter: true,

	showComparisons: true,

	useWindowParams: true,

	gridName: '',

	displayFilter: true,

	clobCheckURL : '',
	clobTypeFields : [],
	convertColumnToField : function(dataArr){
        if(dataArr.length == 0)
            return dataArr;
        var resArr = [];
        for(var i = 0; i < dataArr.length; i++){
            var column = dataArr[i];
            var field = column.toLowerCase().replace(/_/g, "");
            resArr.push(field);
        }
        return resArr;
    },
	init: function(grid) {
		var me = this;
		if(this.clobCheckURL != ""){
			Ext.Ajax.request({
				url : this.clobCheckURL,
				success : function(resp){
					var response = RS.common.parsePayload(resp);
					me.clobTypeFields = me.convertColumnToField(response.data);
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
		me.grid = grid;
		if (!me.grid.isXType('grid')) {
			me.originalGrid = me.grid
			me.grid = me.grid.down(this.gridName ? '#' + this.gridName : 'grid')
		}
		me.gridColumns = [];

		me.conditions = {
			on: RS.common.locale.on,
			after: RS.common.locale.after,
			before: RS.common.locale.before,
			equals: RS.common.locale.equals,
			notEquals: RS.common.locale.notEquals,
			contains: RS.common.locale.contains,
			notContains: RS.common.locale.notContains,
			startsWith: RS.common.locale.startsWith,
			notStartsWith: RS.common.locale.notStartsWith,
			endsWith: RS.common.locale.endsWith,
			notEndsWith: RS.common.locale.notEndsWith,
			greaterThan: RS.common.locale.greaterThan,
			greaterThanOrEqualTo: RS.common.locale.greaterThanOrEqualTo,
			lessThan: RS.common.locale.lessThan,
			lessThanOrEqualTo: RS.common.locale.lessThanOrEqualTo,
			shortEquals: RS.common.locale.shortEquals,
			shortNotEquals: RS.common.locale.shortNotEquals,
			shortGreaterThan: RS.common.locale.shortGreaterThan,
			shortGreaterThanOrEqualTo: RS.common.locale.shortGreaterThanOrEqualTo,
			shortLessThan: RS.common.locale.shortLessThan,
			shortLessThanOrEqualTo: RS.common.locale.shortLessThanOrEqualTo
		};

		me.shortConditions = {};
		me.shortConditions[RS.common.locale.shortEquals] = 'equals';
		me.shortConditions[RS.common.locale.shortNotEquals] = 'notEquals';
		me.shortConditions[RS.common.locale.shortGreaterThan] = 'greaterThan';
		me.shortConditions[RS.common.locale.shortGreaterThanOrEqualTo] = 'greaterThanOrEqualTo';
		me.shortConditions[RS.common.locale.shortLessThan] = 'lessThan';
		me.shortConditions[RS.common.locale.shortLessThanOrEqualTo] = 'lessThanOrEqualTo';

		me.dataValues = {
			today: RS.common.locale.today,
			yesterday: RS.common.locale.yesterday,
			lastWeek: RS.common.locale.lastWeek,
			lastMonth: RS.common.locale.lastMonth,
			lastYear: RS.common.locale.lastYear
		};
		// me.grid.store.pageSize = 50;
		me.grid.store.on('beforeLoad', function(store, operation, eOpts) {
			var whereClause = this.getWhereClause();
			var	whereFilter = this.getWhereFilter();
			operation.params = operation.params || {};

			//merge any existing filters
			if (operation.params.filter) {
				var existingFilter = Ext.decode(operation.params.filter)
				whereFilter = whereFilter.concat(existingFilter)
			}

			Ext.apply(operation.params, {
				filter: Ext.encode(whereFilter)
			});
			store.lastWhereClause = whereClause;
		}, me);


		if (me.grid.store.parentVM && !me.grid.store.parentVM.activate)
			me.grid.store.parentVM.activate = Ext.emptyFn

		if (me.grid.store.parentVM)
			me.grid.store.parentVM.activate = Ext.Function.createInterceptor(me.grid.store.parentVM.activate, function() {
				me.parseWindowParameters();
			})


		me.grid.on('celldblclick', function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
			var column = grid.getHeaderCt().getHeaderAtIndex(cellIndex);
			var	dataIndex = column.dataIndex;
			var	cellValue = record.get(dataIndex);
			var operator = RS.common.locale.equals;
			Ext.each(me.clobTypeFields, function(clobField){
				if(column.dataIndex == clobField){
					operator = RS.common.locale.contains;
					return;
				}
			})
				
			if (!column.filterable) return;
			if (column.computeFilter) {
				me.addFilter(column.computeFilter(cellValue));
				return;
			}
			if (Ext.isDate(cellValue)) {
				//cellValue = Ext.Date.format(cellValue, me.dateFormat);
				cellValue = Ext.Date.format(cellValue, me.submitFormat);
				operator = RS.common.locale.on;
			}

			if (Ext.isBoolean(cellValue)) cellValue = cellValue.toString();

			if (Ext.isNumber(cellValue)) cellValue = cellValue.toString();

			if (cellValue.indexOf('|&|') > -1) {
				cellValue = cellValue.replace(/\|&\|/g, '|');
				operator = RS.common.locale.contains;
			}
			if (dataIndex) me.addFilter(column.text + ' ' + operator + ' ' + cellValue); //Check data index for action columns or columns that aren't a part of the data
		}, me);

		me.grid.on('reconfigure', function() {
			Ext.defer(function() {
				me.parseWindowParameters()
				me.grid.store.load()
			}, 1, me)
		})

		var fields = me.grid.store.proxy.reader.model.getFields();
		Ext.each(me.grid.columns, function(column) {
			var col = column;
			Ext.each(fields, function(field) {
				if (field.name == col.dataIndex) {
					col.dataType = field.type.type;
					return false;
				}
			});
			me.gridColumns.push(col);
		});

		me.searchStore = Ext.create('Ext.data.Store', {
			fields: ['name', 'value', 'type', 'displayName'],
			proxy: {
				type: 'memory',
				reader: {
					type: 'json',
					root: 'records'
				}
			},
			listeners: {
				load: function(store, records, successful, eOpts) {
					var combo = me.toolbar.down('#searchText'),
						value = combo.nextVal || combo.getValue();
					combo.nextVal = null;
					me.parseSuggestionsForStore(store, value);
				}
			}
		});

		me.filterStore = Ext.create('Ext.data.Store', {
			model: 'RS.common.model.Filter',
			proxy: {
				type: 'ajax',
				url: '/resolve/service/filter/list',
				reader: {
					type: 'json',
					root: 'records'
				}
			}
		});

		me.filterStore.on('load', function() {
			me.filterStore.add({
				name: RS.common.locale.manageFilters,
				value: 'managefilters',
				sysId: 'managefilters'
			})
		})

		me.filterStore.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {}

			Ext.apply(operation.params, {
				table: me.grid.tableName,
				view: me.grid.viewName
			})
		})

		if (me.allowPersistFilter) me.filterStore.load();

		me.viewStore = Ext.create('Ext.data.Store', {
			model: 'RS.common.model.View',
			proxy: {
				type: 'ajax',
				url: '/resolve/service/view/list',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			},
			listeners: {
				scope: me,
				load: function(store, records, successful, eOpts) {
					this.configureViewMenuItems()
				}
			}
		});

		me.viewStore.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {}

			Ext.apply(operation.params, {
				table: me.grid.tableName
			})
		})

		me.grid.on('serverDataChanged', function() {
			this.configureViewMenuItems()
		}, me)

		if (me.grid.tableName && !me.hideMenu) me.viewStore.load();

		var displayTableMenuItem = {
			cls: 'rs-display-name',
			text: me.displayName || me.grid.displayName,
			itemId: 'tableMenu'
		};

		if (me.hideMenu) {
			displayTableMenuItem.xtype = 'tbtext';
		} else {
			displayTableMenuItem.listeners = {
				render: function(button) {
					Ext.fly(button.getEl().query('span.x-btn-inner')[0]).setStyle({
						'font-size': '16px',
						'font-weight': 'bold'
					})
				}
			}
			displayTableMenuItem.menu = [{
					text: RS.common.locale.views,
					itemId: 'viewMenuItem',
					menu: {
						items: []
					}
				},
				{
					text: RS.common.locale.settings,
					handler: function(button) {
						clientVM.handleNavigation({
							modelName: 'RS.customtable.TableDefinition',
							params: {
								id: me.grid.tableSysId
							}
						})
					}
				}, {
					text: RS.common.locale.saveViewAs,
					scope: me,
					handler: me.saveView
				}
			]
		}

		var toolbarItems = [displayTableMenuItem, ' ', ' ', ' ', ' ', ' ', ' ', ' ', {
			xtype: 'combobox',
			itemId: 'filter',
			hidden: !me.allowPersistFilter,
			width: 200,
			emptyText: RS.common.locale.SelectFilter,
			store: me.filterStore,
			queryMode: 'local',
			displayField: 'name',
			valueField: 'sysId',
			listeners: {
				scope: me,
				change: function(combo, newValue, oldValue, eOpts) {
					if (newValue) {
						if (newValue == 'managefilters') {
							combo.setValue(oldValue)
							var w = Ext.create('Ext.window.Window', {
								width: 400,
								height: 300,
								layout: 'fit',
								title: RS.common.locale.manageFilters,
								items: [{
									xtype: 'grid',
									tbar: [{
										text: RS.common.locale.deleteText,
										itemId: 'deleteFilter',
										disabled: true,
										scope: this,
										handler: function(btn) {
											var selections = btn.up('window').down('grid').getSelectionModel().getSelection(),
												ids = [];
											Ext.Array.forEach(selections, function(selection) {
												ids.push(selection.get('sysId'))
											})
											Ext.Ajax.request({
												url: '/resolve/service/filter/delete',
												params: {
													sysIds: ids.join(',')
												},
												success: function(r) {
													var response = RS.common.parsePayload(r)
													if (response.success) {
														clientVM.displaySuccess(ids.length === 1 ? RS.common.locale.filterDeleted : RS.common.locale.filtersDeleted)
														this.filterStore.load()
														btn.up('window').close()
													} else clientVM.displayError(response.message)
												},
												failure: function(f) {
													clientVM.displayFailure(f);
												}
											})
										}
									}],
									selMode: 'MULTI',
									columns: [{
										text: RS.common.locale.name,
										dataIndex: 'name',
										flex: 1
									}],
									store: Ext.create('Ext.data.Store', {
										autoLoad: true,
										model: 'RS.common.model.Filter',
										proxy: {
											type: 'ajax',
											url: '/resolve/service/filter/list',
											extraParams: {
												table: me.grid.tableName,
												view: me.grid.viewName
											},
											reader: {
												type: 'json',
												root: 'records'
											},
											listeners: {
												exception: function(e, resp, op) {
													clientVM.displayExceptionError(e, resp, op);
												}
											}
										}
									}),
									listeners: {
										selectionchange: function(grid, selections) {
											w.down('#deleteFilter')[selections.length > 0 ? 'enable' : 'disable']()
										}
									}
								}],
								buttonAlign: 'left',
								buttons: [{
									xtype: 'button',
									text: RS.common.locale.close,
									handler: function(btn) {
										btn.up('window').close()
									}
								}]
							}).show()
						} else
							this.setFilter(combo.store.getById(newValue).data.value);
					}
				}
			}
		}, {
			xtype: 'combobox',
			itemId: 'searchText',
			width: 520,
			emptyText: RS.common.locale.Filter + RS.common.locale.egText,
			trigger2Cls: 'x-form-search-trigger',
			margin: '0 0 0 20',
			enableKeyEvents: true,
			// autoSelect: false,
			queryMode: 'remote',
			typeAhead: false,
			minChars: 1,
			store: me.searchStore,
			displayField: 'name',
			valueField: 'value',
			queryDelay: 10,
			listConfig: {
				emptyText: RS.common.locale.noSuggestions
			},
			onTriggerClick: function() {
				if (!this.advanced) {
					var pos = this.getPosition(),
						items = [];
					Ext.each(me.grid.columns, function(column) {
						var isClobField = false;
						Ext.each(me.clobTypeFields, function(clobField){
							if(column.dataIndex == clobField){
								isClobField = true;
								return;
							}
						})
						var columnXtype = '',
							comparisons = me.getShortOptionsForDataType(column.dataType, isClobField);

						switch (column.dataType) {
							case 'date':
								switch (column.uiType) {
									case 'Date':
										columnXtype = 'xdatefield';
										break;
									case 'Timestamp':
										columnXtype = 'xtimefield';
										break;
									default:
										columnXtype = 'xdatetimefield';
										break;
								}
								break;
							case 'float':
							case 'int':
								columnXtype = 'numberfield';
								break;
							case 'bool':
								columnXtype = 'booleancombobox';
								break;
							default:
								columnXtype = 'textfield';
								break;
						}

						if (column.filterable) {
							items.push({
								fieldLabel: column.filterLabel || column.tooltip || column.text,
								labelAlign: 'top',
								name: column.dataIndex + '_type',
								xtype: 'fieldcontainer',
								layout: 'hbox',
								items: [{
									xtype: 'combobox',
									name: column.dataIndex + '_comparison',
									width: 130,
									displayField: 'name',
									valueField: 'value',
									padding: '0 5 0 0',
									hidden: !me.showComparisons,
									store: Ext.create('Ext.data.Store', {
										fields: ['name', 'value'],
										data: comparisons
									}),
									value: column.dataType == 'date' ? 'on' : comparisons[0].name
								}, {
									name: column.dataIndex,
									xtype: columnXtype,
									validator: column.validator,
									format: columnXtype == 'xtimefield' ? me.timeFormat : me.dateFormat,
									submitFormat: 'Y-m-d H:i:sO',
									flex: 1,
									listeners: {
										validitychange: function(btn, isValid) {
											this.up().up().up().down('button[name="searchBtn"]')[isValid ? 'enable' : 'disable']();
										}
									}
								}]

							});
						}
					}, this);

					this.advanced = Ext.create('Ext.Window', {
						closeAction: 'hide',
						modal: true,
						cls : 'rs-modal-popup',
						title: 'Advanced Search',					
						width: 800,
						height : 600,
						layout: 'fit',
						padding : 15,					
						items: {
							xtype: 'form',
							autoScroll: true,												
							items: items,							
							buttons: [{
								text: RS.common.locale.search,
								name: 'searchBtn',
								cls : 'rs-med-btn rs-btn-dark',
								handler: function(button) {
									var form = button.up('form').getForm(),
										values = form.getValues();
									Ext.each(me.grid.columns, function(column) {
										var value = values[column.dataIndex],
											comp = values[column.dataIndex + '_comparison'];
										if (value) me.addFilter(Ext.String.format('{0} {1} {2}', column.text, comp, value));
									});
									form.reset();
									button.up('window').close();
								}
							}, {
								text: RS.common.locale.cancel,
								cls : 'rs-med-btn rs-btn-light',
								handler: function(button) {
									button.up('form').getForm().reset()
									button.up('window').close()
								}
							}]
						}
					});
				}			
				this.advanced.show();
			},
			onTrigger2Click: function() {
				if (!this.getValue()) return;
				if (me.addFilter(this.getValue())) this.reset();
			},
			listeners: {
				specialkey: function(field, e) {
					if (e.getKey() == e.ENTER) {
						Ext.defer(function() {
							field.onTrigger2Click();
						}, 10);
					}

					if (e.getKey() == e.TAB) {
						e.preventDefault();
					}
				},
				beforeSelect: function(combo, record, index, eOpts) {
					record.data.name = record.data.displayName;
					record.data.value = record.data.displayName;
					combo.nextVal = record.data.name;
					if (record.data.name.lastIndexOf(' ') == record.data.name.length - 1) {
						combo.store.load({
							params: {
								query: record.data.displayName
							}
						});
						Ext.defer(function() {
							combo.expand();
						}, 10);
					}
				},
				render: function(combo) {
					combo.setVisible(me.displayFilter)
				}
			}
		}];

		var tb = me.originalGrid ? me.originalGrid.down('#actionBar') : null
		if (tb) {
			me.toolbar = tb
			Ext.Array.forEach(toolbarItems, function(item, index) {
				tb.insert(index, item)
			})
		} else {
			me.toolbar = Ext.create('Ext.toolbar.Toolbar', {
				enableOverflow: false,
				dock: 'top',
				// height: 30,
				cls: 'title-toolbar rs-section-title',
				items: toolbarItems
			});
			grid.insertDocked(0, me.toolbar);
		}

		me.filterBar = Ext.create('RS.common.FilterBar', {
			allowPersistFilter: me.allowPersistFilter,
			listeners: {
				scope: me,
				filterChanged : {
					fn : me.filterChanged,
					buffer : 10
				},
				saveFilter: me.saveFilter,
				clearFilter: me.clearFilter
			}
		});
		grid.insertDocked(1, me.filterBar);
		me.grid.store.fireEvent('commonSearchFilterInited', me, me);
	},

	configureViewMenuItems: function() {
		//Load the views into the view menu
		var viewMenuItem = this.toolbar.down('#viewMenuItem');
		if (viewMenuItem) {
			var viewMenu = viewMenuItem.menu;

			viewMenu.removeAll();
			this.viewStore.each(function(record) {
				viewMenu.add({
					text: record.data.name,
					group: 'view',
					checked: this.grid.serverData.id == record.data.id,
					tableName: this.grid.tableName,
					grid: this.grid,
					record: record.raw,
					checkHandler: this.selectView
				});
			}, this);
		}
	},
	saveView: function() {
		//Save the current view as a new view, so prompt the user for a new name (pre-populated with the current view if one is selected)
		var viewsMenuItem = this.toolbar.down('#viewMenuItem'),
			value = '';
		//figure out the selected view based on the menu
		viewsMenuItem.menu.items.each(function(view) {
			if (view.checked) {
				value = view.text;
				return false;
			}
		});

		Ext.MessageBox.show({
			title: RS.common.locale.saveViewTitle,
			msg: RS.common.locale.saveViewMessage,
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: RS.common.locale.saveView,
				no: RS.common.locale.cancel
			},
			scope: this,
			fn: this.handleSaveView,
			prompt: true,
			value: value || ''
		});
	},
	handleSaveView: function(button, input) {
		if (button == 'yes') {
			//Check if the name matches an existing view.  If it does, warm them that it will replace the existing view and are they sure they want to do that
			var viewsMenuItem = this.toolbar.down('#viewMenuItem'),
				match = false,
				isSystem = false;
			//figure out the selected view based on the menu
			viewsMenuItem.menu.items.each(function(view) {
				if (view.text == input) {
					match = true;
					isSystem = view.type == 'Shared';
					return false;
				}
			});

			if (isSystem) {
				Ext.MessageBox.show({
					title: RS.common.locale.saveSystemViewTitle,
					msg: RS.common.locale.saveSystemViewBody,
					buttons: Ext.MessageBox.YESNO,
					buttonText: {
						yes: RS.common.locale.saveView,
						no: RS.common.locale.cancel
					},
					scope: this,
					fn: this.handleSaveView,
					prompt: true
				});
				return;
			}

			if (match) {
				var inputConfig = {
					input: input,
					searchControl: this
				};
				//Double check about the duplicate name
				Ext.MessageBox.show({
					title: RS.common.locale.saveViewTitleConfirm,
					msg: Ext.String.format(RS.common.locale.saveViewMessageConfirm, input),
					buttons: Ext.MessageBox.YESNO,
					buttonText: {
						yes: RS.common.locale.saveViewConfirm,
						no: RS.common.locale.cancel
					},
					scope: inputConfig,
					fn: this.reallySaveView
				});
			} else this.persistView(input);
		}
	},
	reallySaveView: function(button) {
		if (button == 'yes') this.searchControl.persistView(this.input);
	},
	persistView: function(viewName) {
		var viewsMenuItem = this.toolbar.down('#viewMenuItem'),
			match = null,
			params = {},
			fields = [];

		this.grid.headerCt.items.each(function(column) {
			if (column.fieldConfig) {
				column.fieldConfig.hidden = column.hidden;
				fields.push(column.fieldConfig);
			}
		});

		//figure out the selected view based on the menu
		viewsMenuItem.menu.items.each(function(view) {
			if (view.checked) {
				match = view;
				return false;
			}
		});

		if (match) {
			var matchRecord = match.record;

			matchRecord.fields = fields;
			if (match.text != viewName || match.type == 'Shared') {

				var viewsMenuItem = this.toolbar.down('#viewMenuItem'),
					matchId = '';
				//figure out the selected view based on the menu
				viewsMenuItem.menu.items.each(function(view) {
					if (view.text == viewName) {
						matchId = view.id
						return false
					}
				});

				matchRecord.id = matchId || '';
				matchRecord.type = 'Self';
				matchRecord.user = '';
				matchRecord.name = viewName;

				//Need to make sure to get these from the parent when creating a new view
				delete matchRecord.metaFormLink;
				delete matchRecord.metaNewLink;
			}

			Ext.Ajax.request({
				url: '/resolve/service/view/save',
				jsonData: matchRecord,
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) {
						this.grid.serverData.id = response.data.id;
						this.viewStore.load();
					}
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			});
		}
	},

	clearFilter: function() {
		this.toolbar.down('#filter').clearValue();
	},
	saveFilter: function() {
		var value = this.toolbar.down('#filter').getDisplayValue();
		//Prompt the user for a name to save the filter as (must be unique in the list of filter names that they have though)
		Ext.MessageBox.show({
			title: RS.common.locale.saveFilterTitle,
			msg: RS.common.locale.saveFilterMessage,
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: RS.common.locale.saveFilter,
				no: RS.common.locale.cancel
			},
			scope: this,
			fn: this.handleSaveFilter,
			prompt: true,
			value: value || ''
		});
	},
	handleSaveFilter: function(button, input) {
		if (button == 'yes') {
			//Check if the name matches an existing filter.  If it does, warn them that it will replace the existing filter and are they sure they want to do that
			var rec = this.filterStore.findRecord('name', input, 0, false, false, true),
				inputConfig = {
					input: input,
					searchControl: this
				};
			if (rec) {
				//Double check about the duplicate name
				Ext.MessageBox.show({
					title: RS.common.locale.saveFilterTitleConfirm,
					msg: Ext.String.format(RS.common.locale.saveFilterMessageConfirm, input),
					buttons: Ext.MessageBox.YESNO,
					buttonText: {
						yes: RS.common.locale.saveFilterConfirm,
						no: RS.common.locale.cancel
					},
					scope: inputConfig,
					fn: this.reallySaveFilter
				});
			} else {
				this.persistFilter(input);
			}
		}
	},
	reallySaveFilter: function(button) {
		if (button == 'yes') this.searchControl.persistFilter(this.input);
	},
	persistFilter: function(filterName) {
		var filterString = this.getFiltersString(),
			rec = this.filterStore.findRecord('name', filterName, 0, false, false, true),
			params = {
				value: filterString
			};

		if (rec) {
			Ext.applyIf(params, rec.raw);
		}

		Ext.applyIf(params, {
			name: filterName,
			metaTableName: this.grid.tableName,
			metaTableSysId: this.grid.tableSysId,
			isSelf: true
		});
		Ext.Ajax.request({
			url: '/resolve/service/filter/save',
			params: params,
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					this.filterStore.load({
						scope: this,
						callback: function(records) {
							Ext.each(records, function(record) {
								if (record.data.name == params.name) this.toolbar.down('#filter').setValue(record.internalId);
							}, this);
							clientVM.displaySuccess(RS.common.locale.filterSaved,4000,RS.common.locale.success);							
						}
					});
				} else{
					clientVM.displayError(response.message, 0,RS.common.locale.error);
				} 
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		});
	},
	addFilter: function(value, suppressChangeEvt) {
		var internalValue = this.getInternalValue(value),
			split = internalValue.split(' ');

		//if the user has typed: field condition value, then add it
		if (split.length > 2 && split[2]) {
			this.filterBar.addFilter(value, internalValue);
			if(!suppressChangeEvt){
				this.filterBar.fireEvent('filterChanged');
			}
			return true;
		}

		return false;
	},
	parseSuggestionsForStore: function(store, value) {
		//Split the value on spaces for the search query
		var searches = value.split(' '),
			columnText = '',
			column = null,
			i;
		var me = this;
		//Parse column text
		for (i = 0; i < searches.length && this.couldBeColumn(Ext.String.trim(columnText + ' ' + searches[i])); i++) {
			columnText += ' ' + searches[i];
			columnText = Ext.String.trim(columnText);
		}

		Ext.each(this.grid.columns, function(gridColumn) {
			if (gridColumn.text.toLowerCase() === columnText.toLowerCase()) {
				column = gridColumn;
				return false;
			}
		});

		store.removeAll();

		//Add in the first non-date column as a search suggestion if the current selection isn't good
		if (!column || !column.filterable) {
			var col = null;
			Ext.each(this.grid.columns, function(c) {
				if (c.dataType != 'date' && c.uiType != 'DateTime' && c.filterable) {
					col = c
					return false
				}
			})
			if (col) {
				store.add({
					name: Ext.String.format('{0} {1} <b>{2}</b>', col.text, RS.common.locale.contains, Ext.String.htmlEncode(value)),
					value: RS.common.locale.contains,
					displayName: Ext.String.format('{0} {1} {2}', col.text, RS.common.locale.contains, value)
				})
			}
		}

		//If we have a column, then we're searching on a column and need the operators
		if (column && column.filterable) {
			var conditionText = '';
			var	operator = null;
			var isClobField = false;
			Ext.each(me.clobTypeFields, function(clobField){
				if(column.dataIndex == clobField){
					isClobField = true;
					return;
				}
			})
			for (; i < searches.length && this.couldBeCondition(Ext.String.trim(conditionText + ' ' + searches[i])); i++) {
				conditionText += ' ' + searches[i];
				conditionText = Ext.String.trim(conditionText);
				if(conditionText == RS.common.locale.equals && isClobField){
					conditionText = RS.common.locale.contains;
				}
			}

			//Get operator equivalent
			Ext.Object.each(this.conditions, function(con) {
				if (this.conditions[con] == conditionText) {
					if (con.indexOf('short') == 0) {
						operator = this.shortConditions[conditionText]
					} else
						operator = this.conditions[con];
					return false;
				}
			}, this);

			if (operator) {
				//Then we have an operator and need to display the data suggestions
				var value = searches.splice(i).join(' '),
					options = this.getValueOptionsForDataType(column, columnText, conditionText, value),
					dataValues = [];

				//Add in the user's typed in search criteria as the first option
				store.add({
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, conditionText, Ext.String.htmlEncode(value)),
					value: conditionText,
					displayName: Ext.String.format('{0} {1} {2}', columnText, conditionText, value)
				});

				store.add(options);

				//Get the data options for the column
				this.grid.store.each(function(record) {
					var data = record.get(column.dataIndex);
					if (data) {
						if (Ext.isDate(data)) data = Ext.Date.format(data, this.dateFormat);
						if (Ext.isBoolean(data)) data = data.toString();
						if (Ext.isNumber(data)) data = data.toString();
						if (dataValues.indexOf(data.toLowerCase()) === -1 && data.toLowerCase().indexOf(value.toLowerCase()) > -1) {
							dataValues.push(data.toLowerCase());
							store.add({
								name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, conditionText, Ext.String.htmlEncode(data)),
								value: conditionText,
								displayName: Ext.String.format('{0} {1} {2}', columnText, conditionText, data)
							});
						}
					}
					if (store.getCount() >= 6) return false;
				}, this);

				//If we have the operator, but don't have any data suggestions for what they are typing, then we need to just return the value that they are typing to help them out
				if (store.getCount() === 0) {
					store.add({
						name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, conditionText, Ext.String.htmlEncode(value)),
						value: conditionText,
						displayName: Ext.String.format('{0} {1} {2}', columnText, conditionText, value)
					})
				}
			} else {
				//Then we have a part of an operator and need to display the operators that match the search
				var options = this.getOptionsForDataType(column,isClobField);

				//First add in the contains of the user provided value as an option for the user if it doesn't match a condition option
				var val = searches.slice(1).join(' ');
				if (val) {
					store.add({
						name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, RS.common.locale.contains, Ext.String.htmlEncode(val)),
						value: RS.common.locale.contains,
						displayName: Ext.String.format('{0} {1} {2}', columnText, RS.common.locale.contains, val)
					})
				}

				Ext.each(options, function(option) {
					if (option.value.indexOf(conditionText) > -1) store.add(option);
				});
			}
		}

		if (columnText && store.getCount() < 7) {
			//We don't have a column, but have a part of a column so show those that match the current search
			Ext.each(this.grid.columns, function(column) {
				if (column.filterable && column.text.toLowerCase().indexOf(columnText.toLowerCase()) === 0) {
					store.add({
						name: Ext.String.format('<b>{0}</b>{1}', column.text.substring(0, columnText.length), column.text.substring(columnText.length)),
						value: column.text,
						type: column.dataType,
						displayName: column.text + ' '
					});
					if (store.getCount() >= 6) return false;
				}
			});
		}

		if (store.getCount() < 7) {
			//No column matches, so we need to search the data
			var query = value.toLowerCase(),
				columnData = [],
				recordData = [],
				records = [];
			this.grid.store.each(function(record) {
				Ext.Object.each(record.data, function(data) {
					var tempValue = record.data[data];
					if (Ext.isBoolean(tempValue)) tempValue = tempValue.toString()
					if (Ext.isDate(tempValue)) tempValue = Ext.Date.format(tempValue, this.dateFormat)
					if (Ext.isNumber(tempValue)) tempValue = tempValue.toString()
					if (tempValue && Ext.isString(tempValue) && tempValue.toLowerCase().indexOf(query.toLowerCase()) > -1 && recordData.indexOf(tempValue.toLowerCase()) == -1) {
						if (columnData.indexOf(data) == -1) columnData.push(data);
						recordData.push(tempValue.toLowerCase());
						records.push({
							column: data,
							data: tempValue
						});
					}
					if (columnData.length >= 6 || recordData.length >= 6) return false;
				}, this);
			}, this);

			//Now we have a list of columns.  Display the columns with the appropriate comparisons for the data
			Ext.each(columnData, function(column) {
				if (store.getCount() >= 6) return false;
				Ext.each(this.grid.columns, function(gridColumn) {
					if (store.getCount() >= 6) return false;
					if (gridColumn.dataIndex == column && column.filterable) {
						var conditionText = RS.common.locale.contains;
						if (gridColumn.dataType == 'date') conditionText = RS.common.locale.on;
						store.add({
							name: Ext.String.format('{0} {1} <b>{2}</b>', gridColumn.text, conditionText, Ext.String.htmlEncode(query)),
							value: conditionText,
							displayName: Ext.String.format('{0} {1} {2}', gridColumn.text, conditionText, query)
						});
					}
				});
			}, this);

			//Now we have a list of the records and their column dataIndexes so we need to turn those into proper queries based on their types
			Ext.each(records, function(record) {
				if (store.getCount() >= 6) return false;
				Ext.each(this.grid.columns, function(column) {
					if (column.dataIndex == record.column && column.filterable) {
						record.columnText = column.text;
						record.columnDataType = column.dataType;
						if (column.dataType == 'date') record.conditionText = RS.common.locale.on;
						else {
							var isClobField = false;
							Ext.each(me.clobTypeFields, function(clobField){
								if(column.dataIndex == clobField){
									isClobField = true;
									return;
								}
							})
							record.conditionText = isClobField ? RS.common.locale.contains : RS.common.locale.equals;
						}
						return false;
					}
				});
				if (record.columnText) {
					//Now that we have a record with all the information we could need, we need to add it to the store
					store.add({
						name: Ext.String.format('{0} {1} <b>{2}</b>', record.columnText, record.conditionText, Ext.String.htmlEncode(record.data)),
						value: record.data,
						displayName: Ext.String.format('{0} {1} {2}', record.columnText, record.conditionText, record.data)
					});
				}
			}, this);
		}

		if (store.getCount() === 0) {
			//No records match the search criteria as of yet (could be another page or something), so load up the columns with their types based on the type of query
			var queryType = 'string',
				conditionText = RS.common.locale.contains;
			if (Ext.isNumber(Number(value))) {
				queryType = 'float';
				conditionText = RS.common.locale.equals
			}
			if (Ext.isDate(Ext.Date.parse(value, this.dateFormat))) {
				queryType = 'date';
				conditionText = RS.common.locale.on;
			}

			Ext.each(this.grid.columns, function(column) {
				if (column.filterable && column.dataType == queryType) {
					store.add({
						name: Ext.String.format('{0} {1} <b>{2}</b>', column.text, conditionText, Ext.String.htmlEncode(value)),
						value: value,
						displayName: Ext.String.format('{0} {1} {2}', column.text, conditionText, value)
					});
				}
				if (store.getCount() >= 6) return false;
			});
		}
	},
	getInternalValue: function(value) {
		var returnText = '',
			split = value.split(' ');

		var columnText = '',
			i;
		//Get column dataIndex
		for (i = 0; i < split.length && this.couldBeColumn(Ext.String.trim(columnText + ' ' + split[i])); i++) {
			columnText += ' ' + split[i];
			columnText = Ext.String.trim(columnText);
		}
		Ext.each(this.grid.columns, function(column) {
			if (column.text.toLowerCase() == columnText.toLowerCase()) returnText += column.dataIndex + ' ';
		});

		var conditionText = '';
		for (; i < split.length && this.couldBeCondition(Ext.String.trim(conditionText + ' ' + split[i])); i++) {
			conditionText += ' ' + split[i];
			conditionText = Ext.String.trim(conditionText);
		}

		//Get operator equivalent
		Ext.Object.each(this.conditions, function(con) {
			var c = con;
			if (this.conditions[con] == conditionText) {
				if (con.indexOf('short') == 0) {
					c = this.shortConditions[conditionText]
				}
				returnText += c;
				return false;
			}
		}, this);
		returnText += ' ';

		var val = split.slice(i).join(' '),
			matchDataValue = false;
		//Get value equivalent
		Ext.Object.each(this.dataValues, function(value) {
			if (this.dataValues[value] == val) {
				matchDataValue = true;
				returnText += value;
				return false;
			}
		}, this);
		if (!matchDataValue) returnText += val;

		return returnText;
	},
	getExternalValue: function(filter) {
		var split = filter.split(' '),
			columnText = split[0],
			condition = split[1];
		split.splice(0, 2);
		var value = split.join(' ');
		//Convert column
		Ext.each(this.grid.columns, function(column) {
			if (column.dataIndex == columnText) {
				columnText = column.text;
				return false;
			}
		});
		//Convert condition
		condition = RS.common.locale[condition];

		return Ext.String.format('{0} {1} {2}', columnText, condition, value);
	},
	couldBeColumn: function(columnText) {
		var exists = false;
		Ext.each(this.grid.columns, function(column) {
			if (column.filterable && column.text.toLowerCase().indexOf(columnText.toLowerCase()) == 0) {
				exists = true;
				return false;
			}
		});
		return exists;
	},
	couldBeCondition: function(condition) {
		condition = condition.toLowerCase();
		var returnVal = false;
		Ext.Object.each(this.conditions, function(con) {
			if (this.conditions[con].indexOf(condition) == 0) {
				returnVal = true;
				return false;
			}
		}, this);
		return returnVal;
	},
	filterChanged: function(a,b,c,d) {
		//Reset to page 1 whenever the filter changes because the current page might not be around anymore and the server is naive to know
		this.grid.store.loadPage(1);
	},
	getWhereFilter: function() {
		var filters = this.filterBar.getFilters(),
			whereFilter = [];

		Ext.each(filters, function(filter, index) {
			var split = filter.split(' '),
				column = split[0],
				condition = split[1],
				value = split.slice(2).join(' '),
				col = {},
				field, val;

			//get the column based on the column dataIndex
			Ext.each(this.grid.columns, function(gridColumn) {
				if (gridColumn.dataIndex == column) {
					col = gridColumn;
					return false;
				}
			});

			if (col) {
				if (col.dataType == 'date') {
					val = Ext.Date.parse(value, this.dateFormat);
					if (!Ext.isDate(val)) val = Ext.Date.parse(value, 'c')
					if (Ext.isDate(val)) {
						value = Ext.Date.format(val, 'c');
					}
				}

				if (col.dataType == 'bool') {
					value = value === 'true' || value === true || value === 1 || value === '1'
				}

				field = col.dataIndex;

				whereFilter.push({
					field: field,
					type: col.dataType,
					condition: condition,
					value: value
				});
			}
		}, this);

		return whereFilter;
	},
	getWhereClause: function() {
		var filters = this.filterBar.getFilters(),
			whereClause = '';
		//Parse through filters to get the where clause
		Ext.each(filters, function(filter, index) {
			var split = filter.split(' '),
				column = split[0],
				condition = split[1],
				value = split.splice(2).join(' '),
				where = '(',
				addValue = true;

			where += column + ' ';
			var values = value.split('|');
			//get the column based on the column dataIndex
			var col = {};
			Ext.each(this.grid.columns, function(gridColumn) {
				if (gridColumn.dataIndex == column) {
					col = gridColumn;
					return false;
				}
			});
			Ext.each(values, function(value, index) {
				if (value) {
					if (index > 0) where += 'OR ' + column + ' ';
					if (col.dataType == 'bool') {
						value = value === 'true' ? '1' : '0';
					}
					//Get condition equivalent
					switch (condition.toLowerCase()) {
						case 'on':
							where += "BETWEEN (date '{0}') AND (date '{1}')";
							break;
						case 'after':
							where += "> (date '{0}')";
							break;
						case 'before':
							where += "< (date '{0}')";
							break;
						case 'equals':
							var eqCond = Ext.isNumber(Number(value)) ? '=' : "= '{0}'";
							if (value === 'NULL') {
								eqCond = 'IS NULL';
								addValue = false;
							}
							if (value === 'NIL') {
								eqCond = Ext.String.format("IS NULL OR {0} = ''", column);
								addValue = false;
							}
							where += eqCond;
							break;
						case 'notequals':
							var eqCond = Ext.isNumber(Number(value)) ? '<>' : "<> '{0}'";
							if (value === 'NULL') {
								eqCond = 'IS NOT NULL';
								addValue = false;
							}
							if (value === 'NIL') {
								eqCond = Ext.String.format("IS NOT NULL OR {0} <> ''", column);
								addValue = false;
							}
							where += eqCond;
							break;
						case 'contains':
							where += "LIKE '%{0}%'";
							break;
						case 'notcontains':
							where += "NOT LIKE '%{0}%'";
							break;
						case 'startswith':
							where += "LIKE '{0}%'";
							break;
						case 'notstartswith':
							where += "NOT LIKE '{0}%'";
							break;
						case 'endswith':
							where += "LIKE '%{0}'";
							break;
						case 'notendswith':
							where += "NOT LIKE '%{0}'";
							break;
						case 'greaterthan':
							where += '> {0}';
							break;
						case 'greaterthanorequalto':
							where += '>= {0}';
							break;
						case 'lessthan':
							where += '< {0}';
							break;
						case 'lessthanorequalto':
							where += '<= {0}';
							break;
						default:
							where += '=';
					}

					where += ' ';

					switch (value) {
						case 'today':
							where = Ext.String.format(where, Ext.Date.format(new Date(), this.dateFormat), Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, 1), this.dateFormat));
							break;
						case 'yesterday':
							where = Ext.String.format(where, Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, -1), this.dateFormat), Ext.Date.format(Ext.Date.format(new Date(), this.dateFormat)));
							break;
						case 'lastweek':
							where = Ext.String.format(where, Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.WEEK, -1), this.dateFormat), Ext.Date.format(Ext.Date.format(new Date(), this.dateFormat)));
							break;
						case 'lastmonth':
							where = Ext.String.format(where, Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.MONTH, -1), this.dateFormat), Ext.Date.format(Ext.Date.format(new Date(), this.dateFormat)));
							break;
						case 'lastyear':
							where = Ext.String.format(where, Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, -1), this.dateFormat), Ext.Date.format(Ext.Date.format(new Date(), this.dateFormat)));
							break;
						default:
							if (addValue) {
								var val = value,
									secondVal = '';
								if (col.dataType == 'date') {
									val = Ext.Date.parse(value, this.dateFormat)
									if (!Ext.isDate(val)) val = Ext.Date.parse(value, 'c');
									if (Ext.isDate(val)) {
										value = Ext.Date.format(val, this.submitFormat);
										secondVal = Ext.Date.format(Ext.Date.add(val, Ext.Date.DAY, 1), this.submitFormat);
									}
								}

								if (where.indexOf('{0}') > -1) where = Ext.String.format(where, value.replace(/'/g, "''"), secondVal.replace(/'/g, "''"));
								else where += value.replace(/'/g, "''");
							}
					}
				}
			}, this);

			whereClause += (index > 0 ? ' AND ' : ' ') + where + ')';
		}, this);
		return whereClause;
	},
	getOptionsForDataType: function(column, isClobField) {
		var me = this,
			options = [];
		switch (column.dataType) {
			case 'date':
				options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.on),
					value: RS.common.locale.on,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.on)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.before),
					value: RS.common.locale.before,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.before)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.after),
					value: RS.common.locale.after,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.after)
				}];
				break;
			case 'float':
			case 'int':
				options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.equals),
					value: RS.common.locale.equals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.equals)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notEquals),
					value: RS.common.locale.notEquals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notEquals)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.greaterThan),
					value: RS.common.locale.greaterThan,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.greaterThan)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.greaterThanOrEqualTo),
					value: RS.common.locale.greaterThanOrEqualTo,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.greaterThanOrEqualTo)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.lessThan),
					value: RS.common.locale.lessThan,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.lessThan)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.lessThanOrEqualTo),
					value: RS.common.locale.lessThanOrEqualTo,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.lessThanOrEqualTo)
				}];
				break;
			case 'bool':
				options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.equals),
					value: RS.common.locale.equals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.equals)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notEquals),
					value: RS.common.locale.notEquals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notEquals)
				}];
				break;
			default:
				options = [];
				if(!isClobField){
					options = [{
						name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.equals),
						value: RS.common.locale.equals,
						displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.equals)
					}, {
						name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notEquals),
						value: RS.common.locale.notEquals,
						displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notEquals)
					}]
				}
				options.push({
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.contains),
					value: RS.common.locale.contains,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.contains)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notContains),
					value: RS.common.locale.notContains,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notContains)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.startsWith),
					value: RS.common.locale.startsWith,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.startsWith)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notStartsWith),
					value: RS.common.locale.notStartsWith,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notStartsWith)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.endsWith),
					value: RS.common.locale.endsWith,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.endsWith)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notEndsWith),
					value: RS.common.locale.notEndsWith,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notEndsWith)
				});
				if (!me.showComparisons && !isClobField) options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.equals),
					value: RS.common.locale.equals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.equals)
				}]
				break;
		}
		return options;
	},
	getShortOptionsForDataType: function(type, isClobField) {
		var options = [];
		switch (type) {
			case 'date':
				options = [{
					name: Ext.String.format('{0}', RS.common.locale.on),
					value: Ext.String.format('{0}', RS.common.locale.on)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.before),
					value: Ext.String.format('{0}', RS.common.locale.before)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.after),
					value: Ext.String.format('{0}', RS.common.locale.after)
				}];
				break;
			case 'float':
			case 'int':
				options = [{
					name: Ext.String.format('{0}', RS.common.locale.shortEquals),
					value: Ext.String.format('{0}', RS.common.locale.shortEquals)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortNotEquals),
					value: Ext.String.format('{0}', RS.common.locale.shortNotEquals)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortGreaterThan),
					value: Ext.String.format('{0}', RS.common.locale.shortGreaterThan)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortGreaterThanOrEqualTo),
					value: Ext.String.format('{0}', RS.common.locale.shortGreaterThanOrEqualTo)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortLessThan),
					value: Ext.String.format('{0}', RS.common.locale.shortLessThan)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortLessThanOrEqualTo),
					value: Ext.String.format('{0}', RS.common.locale.shortLessThanOrEqualTo)
				}];
				break;
			case 'bool':
				options = [{
					name: Ext.String.format('{0}', RS.common.locale.equals),
					value: Ext.String.format('{0}', RS.common.locale.equals)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notEquals),
					value: Ext.String.format('{0}', RS.common.locale.notEquals)
				}];
				break;
			default:
				options = [];
				if(!isClobField){
					options.push({
						name: Ext.String.format('{0}', RS.common.locale.equals),
						value: Ext.String.format('{0}', RS.common.locale.equals)
					},{
						name: Ext.String.format('{0}', RS.common.locale.notEquals),
						value: Ext.String.format('{0}', RS.common.locale.notEquals)
					});
				}
				options.push({
					name: Ext.String.format('{0}', RS.common.locale.contains),
					value: Ext.String.format('{0}', RS.common.locale.contains)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notContains),
					value: Ext.String.format('{0}', RS.common.locale.notContains)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.startsWith),
					value: Ext.String.format('{0}', RS.common.locale.startsWith)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notStartsWith),
					value: Ext.String.format('{0}', RS.common.locale.notStartsWith)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.endsWith),
					value: Ext.String.format('{0}', RS.common.locale.endsWith)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notEndsWith),
					value: Ext.String.format('{0}', RS.common.locale.notEndsWith)
				});
		}
		return options;
	},
	getValueOptionsForDataType: function(column, columnText, operatorText, dataValue) {
		var options = [],
			returnOptions = [];
		switch (column.dataType) {
			case 'date':
				options = [{
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.today),
					value: RS.common.locale.today,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.today)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.yesterday),
					value: RS.common.locale.yesterday,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.yesterday)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.lastWeek),
					value: RS.common.locale.lastWeek,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.lastWeek)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.lastMonth),
					value: RS.common.locale.lastMonth,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.lastMonth)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.lastYear),
					value: RS.common.locale.lastYear,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.lastYear)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, Ext.Date.format(new Date(), this.dateFormat)),
					value: Ext.Date.format(new Date(), this.dateFormat),
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, Ext.Date.format(new Date(), this.dateFormat))
				}];
				break;
		}
		Ext.each(options, function(option) {
			if (option.value.indexOf(dataValue) > -1) returnOptions.push(option);
		});
		return returnOptions;
	},
	getFiltersString: function() {
		var filters = this.filterBar.getFilters(),
			filterString = '';
		Ext.each(filters, function(filter, index) {
			if (index > 0) filterString += '|&&|';
			filterString += filter.replace(/ /g, '^')
		});
		return filterString;
	},
	setFilter: function(filter) {
		if (!filter) return;
		var filterStrings = filter.split('|&&|'),
			filters = [];
		Ext.each(filterStrings, function(filterString) {
			var parsed = filterString.replace(/\^/g, ' ');
			filters.push({
				value: this.getExternalValue(parsed),
				internalValue: parsed
			});
		}, this);
		this.filterBar.setFilters(filters);
	},

	selectView: function(item, checked) {
		if (checked) {
			Ext.Ajax.request({
				url: '/resolve/service/view/read',
				params: {
					table: item.tableName,
					view: item.text
				},
				scope: item,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) {
						if (response.data && response.data.fields) {
							var modelFields = [],
								columns = [];
							configureColumns(response.data.fields, modelFields, columns, this.grid.serverData.autoSizeColumns);
	
							Ext.define('RS.customtable.Table', {
								extend: 'Ext.data.Model',
								idProperty: 'sys_id',
								fields: modelFields
							});
							//reconfigure the grid to use the new columns
							this.grid.reconfigure(this.grid.store, columns); //reconfigure will fire the load automatically
							this.grid.fireEvent('viewSelected', this.grid, response.data);
						}
					} else {
						clientVM.displayError(response.message);
					}
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			});
		}
	},
	parseWindowParameters: function() {
		if (!this.useWindowParams) return false

		//Parse through url parameters matching the dataIndex of the columns
		var me = this,
			windowLocation = window.location.href,
			winParams = {},
			addedFilter = false,
			windowParams = windowLocation.split('?')[1],
			hashParams = windowLocation.split('#')[1],
			windowParamsSplit = [],
			suppressChangeEvt = true;

		if (windowParams) {
			windowParamsSplit = windowParams.split('#'); //remove hastag navigation
			windowParams = windowParamsSplit[0]
			winParams = Ext.Object.fromQueryString(windowParams)
		}

		if (hashParams && clientVM && clientVM.routes) {
			Ext.Object.each(clientVM.routes, function(route) {
				var params = RS.util.routeMatcher(route).parse(hashParams)
				if (params && params.params) {
					var p = Ext.Object.fromQueryString(params.params)
					Ext.apply(winParams, p)
					return false
				}
			})
		}

		var clearBeforeAdd = true;
		//handle 'All XXX' menu item
		var noFilter = winParams['noFilter'];		
		if (noFilter == true || noFilter== "true") {
			me.clearFilter();
			me.filterBar.clearFilter(me,false,suppressChangeEvt);
		}
		else{
			Ext.Object.each(winParams, function(param) {
				Ext.each(me.grid.columns, function(column) {
					if (column.dataIndex == param && column.filterable) {
						var tmp = winParams[param];

						if (Ext.isString(tmp)) tmp = [tmp];

						Ext.each(tmp, function(tmpString) {
							var split = tmpString.split('~'),
								value = split[split.length - 1],
								comp = RS.common.locale.equals;
							if (split.length > 1) {
								switch (split[0]) {
									case 'on':
										comp = RS.common.locale.on;
										break;
									case 'after':
										comp = RS.common.locale.after;
										break;
									case 'before':
										comp = RS.common.locale.before;
										break;
									case 'equals':
									case 'eq':
										comp = RS.common.locale.equals;
										break;
									case 'notEquals':
									case 'neq':
										comp = RS.common.locale.notEquals;
										break;
									case 'contains':
										comp = RS.common.locale.contains;
										break;
									case 'notContains':
										comp = RS.common.locale.notContains;
										break;
									case 'startsWith':
										comp = RS.common.locale.startsWith;
										break;
									case 'notStartsWith':
										comp = RS.common.locale.notStartsWith;
										break;
									case 'endsWith':
										comp = RS.common.locale.endsWith;
										break;
									case 'notEndsWith':
										comp = RS.common.locale.notEndsWith;
										break;
									case 'greaterThan':
									case 'gt':
										comp = RS.common.locale.greaterThan;
										break;
									case 'greaterThanOrEqualTo':
									case 'gteq':
										comp = RS.common.locale.greaterThanOrEqualTo;
										break;
									case 'lessThan':
									case 'lt':
										comp = RS.common.locale.lessThan;
										break;
									case 'lessThanOrEqualTo':
									case 'lteq':
										comp = RS.common.locale.lessThanOrEqualTo;
										break;
									default:
										comp = RS.common.locale.equals;
										break;
								}
							}

							if (clearBeforeAdd) {
								me.filterBar.clearFilter(me,false,suppressChangeEvt);
								clearBeforeAdd = false
							}

							me.addFilter(column.text + ' ' + comp + ' ' + value.replace(/\${USERID}/g, user.name || me.grid.serverData.currentUsername), suppressChangeEvt);
							addedFilter = true;
						});
						return false;
					}
				});

				if (param == 'filter') {
					var filterParam = winParams[param];
					//Set the filter by name
					me.filterStore.on('load', function() {
						var rec = this.filterStore.findRecord('name', winParams[param], 0, false, false, true);
						if (rec) {
							this.toolbar.down('#filter').setValue(rec.data.sysId);
						}
					}, me, {
						single: true
					});
				}
			});
		}
		return addedFilter
	},

	setDisplayName: function(name) {
		this.toolbar.down('#tableMenu').setText(name)
		if (this.allowPersistFilter) this.filterStore.load()
		if (this.grid.tableName && !this.hideMenu) this.viewStore.load()
	},

	setDisplayFilter: function(value) {
		this.displayFilter = value
		this.toolbar.down('#searchText').setVisible(value)
		this.filterBar.setVisible(value)
	}
});
Ext.define('RS.common.plugin.SearchFieldFilter', {
	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.searchfieldfilter',

	dateFormat: 'Y-m-d',
	timeFormat: 'g:i a',
	submitFormat: 'Y-m-d H:i:s',

	showComparisons: true,

	columns: [],


	init: function(field) {
		var me = this;
		me.field = field
		me.columns = field.columns || me.columns

		Ext.apply(field, {
			displayField: 'name',
			valueField: 'value',
			trigger2Cls: 'x-form-search-trigger',
			enableKeyEvents: true,
			queryMode: 'remote',
			typeAhead: false,
			minChars: 1,
			queryDelay: 10,
			listConfig: {
				emptyText: RS.common.locale.noSuggestions
			},
			onTriggerClick: function() {
				if (!this.advanced) {
					var pos = this.getPosition(),
						items = [];
					Ext.each(me.fieldColumns, function(column) {
						var columnXtype = '',
							comparisons = me.getShortOptionsForDataType(column.dataType);

						switch (column.dataType) {
							case 'date':
								switch (column.uiType) {
									case 'Date':
										columnXtype = 'xdatefield';
										break;
									case 'Timestamp':
										columnXtype = 'xtimefield';
										break;
									default:
										columnXtype = 'xdatetimefield';
										break;
								}
								break;
							case 'float':
							case 'int':
								columnXtype = 'numberfield';
								break;
							case 'bool':
								columnXtype = 'booleancombobox';
								break;
							default:
								columnXtype = 'textfield';
								break;
						}

						if (column.filterable) {
							items.push({
								fieldLabel: column.text,
								labelAlign: 'top',
								name: column.dataIndex + '_type',
								xtype: 'fieldcontainer',
								layout: 'hbox',
								items: [{
									xtype: 'combobox',
									name: column.dataIndex + '_comparison',
									width: 115,
									displayField: 'name',
									valueField: 'value',
									padding: '0 5 0 0',
									hidden: !me.showComparisons,
									store: Ext.create('Ext.data.Store', {
										fields: ['name', 'value'],
										data: comparisons
									}),
									value: column.dataType == 'date' ? 'on' : 'equals'
								}, {
									name: column.dataIndex,
									xtype: columnXtype,
									format: columnXtype == 'xtimefield' ? me.timeFormat : me.dateFormat,
									submitFormat: 'Y-m-d H:i:sO',
									flex: 1
								}]

							});
						}
					}, this);

					this.advanced = Ext.create('Ext.Window', {
						closeAction: 'hide',
						modal: true,
						cls : 'rs-modal-popup',
						title: 'Advanced Search',					
						width: 800,
						height : 600,
						layout: 'fit',
						padding : 15,					
						items: {
							xtype: 'form',
							autoScroll: true,												
							items: items,							
							buttons: [{
								text: RS.common.locale.search,
								name: 'searchBtn',
								cls : 'rs-med-btn rs-btn-dark',
								handler: function(button) {
									var form = button.up('form').getForm(),
										values = form.getValues();
									Ext.each(me.fieldColumns, function(column) {
										var value = values[column.dataIndex],
											comp = values[column.dataIndex + '_comparison'];
										if (value) me.addFilter(Ext.String.format('{0} {1} {2}', column.text, comp, value));
									});
									form.reset();
									button.up('window').close();
								}
							}, {
								text: RS.common.locale.cancel,
								cls : 'rs-med-btn rs-btn-light',
								handler: function(button) {
									button.up('form').getForm().reset()
									button.up('window').close()
								}
							}]
						}
					});
				}			
				this.advanced.show();
			},
			onTrigger2Click: function() {
				if (!this.getValue()) return;
				if (me.addFilter(this.getValue())) this.reset();
			}
		})

		me.fieldColumns = []

		me.conditions = {
			on: RS.common.locale.on,
			after: RS.common.locale.after,
			before: RS.common.locale.before,
			equals: RS.common.locale.equals,
			notEquals: RS.common.locale.notEquals,
			contains: RS.common.locale.contains,
			notContains: RS.common.locale.notContains,
			startsWith: RS.common.locale.startsWith,
			notStartsWith: RS.common.locale.notStartsWith,
			endsWith: RS.common.locale.endsWith,
			notEndsWith: RS.common.locale.notEndsWith,
			greaterThan: RS.common.locale.greaterThan,
			greaterThanOrEqualTo: RS.common.locale.greaterThanOrEqualTo,
			lessThan: RS.common.locale.lessThan,
			lessThanOrEqualTo: RS.common.locale.lessThanOrEqualTo,
			shortEquals: RS.common.locale.shortEquals,
			shortNotEquals: RS.common.locale.shortNotEquals,
			shortGreaterThan: RS.common.locale.shortGreaterThan,
			shortGreaterThanOrEqualTo: RS.common.locale.shortGreaterThanOrEqualTo,
			shortLessThan: RS.common.locale.shortLessThan,
			shortLessThanOrEqualTo: RS.common.locale.shortLessThanOrEqualTo
		};

		me.shortConditions = {};
		me.shortConditions[RS.common.locale.shortEquals] = 'equals';
		me.shortConditions[RS.common.locale.shortNotEquals] = 'notEquals';
		me.shortConditions[RS.common.locale.shortGreaterThan] = 'greaterThan';
		me.shortConditions[RS.common.locale.shortGreaterThanOrEqualTo] = 'greaterThanOrEqualTo';
		me.shortConditions[RS.common.locale.shortLessThan] = 'lessThan';
		me.shortConditions[RS.common.locale.shortLessThanOrEqualTo] = 'lessThanOrEqualTo';

		me.dataValues = {
			today: RS.common.locale.today,
			yesterday: RS.common.locale.yesterday,
			lastWeek: RS.common.locale.lastWeek,
			lastMonth: RS.common.locale.lastMonth,
			lastYear: RS.common.locale.lastYear
		}

		if (me.field.filterStore)
			me.field.filterStore.on('beforeLoad', function(store, operation, eOpts) {
				var whereClause = this.getWhereClause(),
					whereFilter = this.getWhereFilter();
				operation.params = operation.params || {}

				//merge any existing filters
				if (operation.params.filter) {
					var existingFilter = Ext.decode(operation.params.filter)
					whereFilter = whereFilter.concat(existingFilter)
				}

				Ext.apply(operation.params, {
					filter: Ext.encode(whereFilter)
				})
				store.lastWhereClause = whereClause
			}, me)

		var fields = me.field.filterStore.proxy.reader.model.getFields();
		Ext.each(me.columns, function(column) {
			var col = column;
			Ext.each(fields, function(field) {
				if (field.name == col.dataIndex) {
					col.dataType = field.type.type;
					return false;
				}
			})
			me.fieldColumns.push(col)
		})

		me.field.store = Ext.create('Ext.data.Store', {
			fields: ['name', 'value', 'type', 'displayName'],
			proxy: {
				type: 'memory',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			},
			listeners: {
				load: function(store, records, successful, eOpts) {
					var combo = me.field,
						value = combo.nextVal || combo.getValue();
					delete combo.nextVal
					me.parseSuggestionsForStore(store, value)
				}
			}
		})

		field.on({
			specialkey: function(field, e) {
				// e.HOME, e.END, e.PAGE_UP, e.PAGE_DOWN,
				// e.TAB, e.ESC, arrow keys: e.LEFT, e.RIGHT, e.UP, e.DOWN
				if (e.getKey() == e.ENTER) {
					Ext.defer(function() {
						field.onTrigger2Click()
					}, 10)
				}

				if (e.getKey() == e.TAB) {
					e.preventDefault()
				}
			},
			beforeSelect: function(combo, record, index, eOpts) {
				record.data.name = record.data.displayName;
				record.data.value = record.data.displayName;
				combo.nextVal = record.data.name;
				if (record.data.name.lastIndexOf(' ') == record.data.name.length - 1) {
					combo.store.load({
						params: {
							query: record.data.displayName
						}
					});
					Ext.defer(function() {
						combo.expand();
					}, 10);
				}
			},
			afterRender: function(field) {
				me.filterBar = field.getFilterBar ? field.getFilterBar(field) : field.ownerCt.down('#' + me.filterBarItemId)
			}
		})
	},

	addFilter: function(value) {
		var internalValue = this.getInternalValue(value),
			split = internalValue.split(' ');

		//if the user has typed: field condition value, then add it
		if (split.length > 2 && split[2]) {
			this.filterBar.addFilter(value, internalValue);
			return true;
		}

		//If the user types: value then pick the first column and add a contains
		// split = (value + ' ').split(' ')
		// if (split.length > 1 && (split[0] || split[1])) {
		// 	//find the first non-date filterable column to filter on
		// 	var col = null;
		// 	Ext.Array.each(this.grid.columns, function(c) {
		// 		if (c.dataType != 'date' && c.uiType != 'DateTime' && c.filterable) {
		// 			col = c
		// 			return false
		// 		}
		// 	})
		// 	if (col) {
		// 		this.filterBar.addFilter([col.text, RS.common.locale.contains, value].join(' '), [col.dataIndex, RS.common.locale.contains, value].join(' '))
		// 		return true;
		// 	}
		// }
		return false;
	},
	parseSuggestionsForStore: function(store, value) {
		//Split the value on spaces for the search query
		var searches = (value || '').split(' '),
			columnText = '',
			column = null,
			i;

		//Parse column text
		for (i = 0; i < searches.length && this.couldBeColumn(Ext.String.trim(columnText + ' ' + searches[i])); i++) {
			columnText += ' ' + searches[i];
			columnText = Ext.String.trim(columnText);
		}

		Ext.each(this.fieldColumns, function(fieldColumn) {
			if (fieldColumn.text && fieldColumn.text.toLowerCase() == columnText.toLowerCase()) {
				column = fieldColumn;
				return false;
			}
		});

		store.removeAll();

		//Add in the first non-date column as a search suggestion if the current selection isn't good
		if (!column || !column.filterable) {
			var col = null;
			Ext.each(this.fieldColumns, function(c) {
				if (c.dataType != 'date' && c.uiType != 'DateTime' && c.filterable) {
					col = c
					return false
				}
			})
			if (col) {
				store.add({
					name: Ext.String.format('{0} {1} <b>{2}</b>', col.text, RS.common.locale.contains, Ext.String.htmlEncode(value)),
					value: RS.common.locale.contains,
					displayName: Ext.String.format('{0} {1} {2}', col.text, RS.common.locale.contains, value)
				})
			}
		}

		//If we have a column, then we're searching on a column and need the operators
		if (column && column.filterable) {
			var conditionText = '',
				operator = null;
			for (; i < searches.length && this.couldBeCondition(Ext.String.trim(conditionText + ' ' + searches[i])); i++) {
				conditionText += ' ' + searches[i];
				conditionText = Ext.String.trim(conditionText);
				}

			//Get operator equivalent
			Ext.Object.each(this.conditions, function(con) {
				if (this.conditions[con] == conditionText) {
					if (con.indexOf('short') == 0) {
						operator = this.shortConditions[conditionText]
					} else
						operator = this.conditions[con];
					return false;
				}
			}, this);

			if (operator) {
				//Then we have an operator and need to display the data suggestions
				var value = searches.splice(i).join(' '),
					options = this.getValueOptionsForDataType(column, columnText, conditionText, value),
					dataValues = [];

				//Add in the user's typed in search criteria as the first option
				store.add({
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, conditionText, Ext.String.htmlEncode(value)),
					value: conditionText,
					displayName: Ext.String.format('{0} {1} {2}', columnText, conditionText, value)
				});

				store.add(options);
				//Get the data options for the column
				this.field.filterStore.each(function(record) {
					var data = record.get(column.dataIndex);
					if (data) {
						if (Ext.isDate(data)) data = Ext.Date.format(data, this.dateFormat);
						if (Ext.isBoolean(data)) data = data.toString();
						if (Ext.isNumber(data)) data = data.toString();
						if (dataValues.indexOf(data.toLowerCase()) === -1 && data.toLowerCase().indexOf(value.toLowerCase()) > -1) {
							dataValues.push(data.toLowerCase());
							store.add({
								name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, conditionText, Ext.String.htmlEncode(data)),
								value: conditionText,
								displayName: Ext.String.format('{0} {1} {2}', columnText, conditionText, data)
							});
						}
					}
					if (store.getCount() >= 6) return false;
				}, this);

				//If we have the operator, but don't have any data suggestions for what they are typing, then we need to just return the value that they are typing to help them out
				if (store.getCount() == 0) {
					store.add({
						name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, conditionText, Ext.String.htmlEncode(value)),
						value: conditionText,
						displayName: Ext.String.format('{0} {1} {2}', columnText, conditionText, value)
					})
				}
			} else {
				//Then we have a part of an operator and need to display the operators that match the search
				var options = this.getOptionsForDataType(column);

				//First add in the contains of the user provided value as an option for the user if it doesn't match a condition option
				var val = searches.slice(1).join(' ');
				if (val) {
					store.add({
						name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, RS.common.locale.contains, Ext.String.htmlEncode(val)),
						value: RS.common.locale.contains,
						displayName: Ext.String.format('{0} {1} {2}', columnText, RS.common.locale.contains, val)
					})
				}

				Ext.each(options, function(option) {
					if (option.value.indexOf(conditionText) > -1) store.add(option);
				});
			}

		} else if (columnText) {
			//We don't have a column, but have a part of a column so show those that match the current search
			Ext.each(this.fieldColumns, function(column) {
				if (column.text.toLowerCase().indexOf(columnText.toLowerCase()) == 0 && column.filterable) {
					store.add({
						name: Ext.String.format('<b>{0}</b>{1}', column.text.substring(0, columnText.length), column.text.substring(columnText.length)),
						value: column.text,
						type: column.dataType,
						displayName: column.text + ' '
					});
				}
			});
		} else {
			//No column matches, so we need to search the data
			var query = value.toLowerCase(),
				columnData = [],
				recordData = [],
				records = [];
			this.field.filterStore.each(function(record) {
				Ext.Object.each(record.data, function(data) {
					var tempValue = record.data[data];
					if (Ext.isBoolean(tempValue)) tempValue = tempValue.toString()
					if (Ext.isDate(tempValue)) tempValue = Ext.Date.format(tempValue, this.dateFormat)
					if (Ext.isNumber(tempValue)) tempValue = tempValue.toString()
					if (tempValue && Ext.isString(tempValue) && tempValue.toLowerCase().indexOf(query.toLowerCase()) > -1 && recordData.indexOf(tempValue.toLowerCase()) == -1) {
						if (columnData.indexOf(data) == -1) columnData.push(data);
						recordData.push(tempValue.toLowerCase());
						records.push({
							column: data,
							data: tempValue
						});
					}
					if (columnData.length >= 6 || recordData.length >= 6) return false;
				}, this);
			}, this);

			//Now we have a list of columns.  Display the columns with the appropriate comparisons for the data
			Ext.each(columnData, function(column) {
				if (store.getCount() >= 6) return false;
				Ext.each(this.fieldColumns, function(fieldColumn) {
					if (store.getCount() >= 6) return false;
					if (fieldColumn.dataIndex == column && column.filterable) {
						var conditionText = RS.common.locale.contains;
						if (fieldColumn.dataType == 'date') conditionText = RS.common.locale.on;
						store.add({
							name: Ext.String.format('{0} {1} <b>{2}</b>', fieldColumn.text, conditionText, query),
							value: conditionText,
							displayName: Ext.String.format('{0} {1} {2}', fieldColumn.text, conditionText, query)
						});
					}
				});
			}, this);

			//Now we have a list of the records and their column dataIndexes so we need to turn those into proper queries based on their types
			Ext.each(records, function(record) {
				if (store.getCount() >= 6) return false;
				Ext.each(this.fieldColumns, function(column) {
					if (column.dataIndex == record.column && column.filterable) {
						record.columnText = column.text;
						record.columnDataType = column.dataType;
						if (column.dataType == 'date') record.conditionText = RS.common.locale.on;
						else record.conditionText = RS.common.locale.equals;
						return false;
					}
				});

				if (record.columnText) {
					//Now that we have a record with all the information we could need, we need to add it to the store
					store.add({
						name: Ext.String.format('{0} {1} <b>{2}</b>', record.columnText, record.conditionText, Ext.String.htmlEncode(record.data)),
						value: record.data,
						displayName: Ext.String.format('{0} {1} {2}', record.columnText, record.conditionText, record.data)
					});
				}
			}, this);
		}

		if (store.getCount() == 0) {
			//No records match the search criteria as of yet (could be another page or something), so load up the columns with their types based on the type of query
			var queryType = 'string',
				conditionText = RS.common.locale.contains;
			if (Ext.isNumber(Number(value))) {
				queryType = 'float';
				conditionText = RS.common.locale.equals
			}
			if (Ext.isDate(Ext.Date.parse(value, this.dateFormat))) {
				queryType = 'date';
				conditionText = RS.common.locale.on;
			}

			Ext.each(this.fieldColumns, function(column) {
				if (column.filterable && column.dataType == queryType) {
					store.add({
						name: Ext.String.format('{0} {1} <b>{2}</b>', column.text, conditionText, Ext.String.htmlEncode(value)),
						value: value,
						displayName: Ext.String.format('{0} {1} {2}', column.text, conditionText, value)
					})
				}
				if (store.getCount() >= 6) return false
			})
		}
	},
	getInternalValue: function(value) {
		var returnText = '',
			split = value.split(' ');

		var columnText = '',
			i;
		//Get column dataIndex
		for (i = 0; i < split.length && this.couldBeColumn(Ext.String.trim(columnText + ' ' + split[i])); i++) {
			columnText += ' ' + split[i];
			columnText = Ext.String.trim(columnText);
		}
		Ext.each(this.fieldColumns, function(column) {
			if (column.text && column.text.toLowerCase() == columnText.toLowerCase()) returnText += column.dataIndex + ' ';
		});

		var conditionText = '';
		for (; i < split.length && this.couldBeCondition(Ext.String.trim(conditionText + ' ' + split[i])); i++) {
			conditionText += ' ' + split[i];
			conditionText = Ext.String.trim(conditionText);
		}

		//Get operator equivalent
		Ext.Object.each(this.conditions, function(con) {
			var c = con
			if (this.conditions[con] == conditionText) {
				if (con.indexOf('short') == 0) {
					c = this.shortConditions[conditionText]
				}
				returnText += c;
				return false;
			}
		}, this);
		returnText += ' ';

		var val = split.slice(i).join(' '),
			matchDataValue = false;
		//Get value equivalent
		Ext.Object.each(this.dataValues, function(value) {
			if (this.dataValues[value] == val) {
				matchDataValue = true;
				returnText += value;
				return false;
			}
		}, this);
		if (!matchDataValue) returnText += val;

		return returnText;
	},
	getExternalValue: function(filter) {
		var split = filter.split(' '),
			columnText = split[0],
			condition = split[1];
		split.splice(0, 2);
		var value = split.join(' ');
		//Convert column
		Ext.each(this.fieldColumns, function(column) {
			if (column.dataIndex == columnText) {
				columnText = column.text;
				return false;
			}
		});
		//Convert condition
		condition = RS.common.locale[condition];

		return Ext.String.format('{0} {1} {2}', columnText, condition, value);
	},
	couldBeColumn: function(columnText) {
		var exists = false;
		Ext.each(this.fieldColumns, function(column) {
			if (column.filterable && column.text && column.text.toLowerCase().indexOf(columnText.toLowerCase()) == 0) {
				exists = true;
				return false;
			}
		});
		return exists;
	},
	couldBeCondition: function(condition) {
		condition = condition.toLowerCase();
		var returnVal = false;
		Ext.Object.each(this.conditions, function(con) {
			if (this.conditions[con].indexOf(condition) == 0) {
				returnVal = true;
				return false;
			}
		}, this);
		return returnVal;
	},
	filterChanged: function() {
		//Reset to page 1 whenever the filter changes because the current page might not be around anymore and the server is naive to know
		this.field.filterStore.loadPage(1);
	},
	getWhereFilter: function() {
		if (!this.filterBar) return []
		var filters = this.filterBar.getFilters(),
			whereFilter = [];

		Ext.each(filters, function(filter, index) {
			var split = filter.split(' '),
				column = split[0],
				condition = split[1],
				value = split.slice(2).join(' '),
				col = {},
				field, val;

			//get the column based on the column dataIndex
			Ext.each(this.fieldColumns, function(fieldColumn) {
				if (fieldColumn.dataIndex == column) {
					col = fieldColumn;
					return false;
				}
			});

			if (col) {
				if (col.dataType == 'date') {
					val = Ext.Date.parse(value, this.dateFormat);
					if (!Ext.isDate(val)) val = Ext.Date.parse(value, 'c')
					if (Ext.isDate(val)) {
						value = Ext.Date.format(val, 'c');
					}
				}

				if (col.dataType == 'bool') {
					value = value === 'true' || value === true || value === 1 || value === '1'
				}

				field = col.dataIndex;

				whereFilter.push({
					field: field,
					type: col.dataType,
					condition: condition,
					value: value
				});
			}
		}, this);

		return whereFilter;
	},
	getWhereClause: function() {
		if (!this.filterBar) return []
		var filters = this.filterBar.getFilters(),
			whereClause = '';
		//Parse through filters to get the where clause
		Ext.each(filters, function(filter, index) {
			var split = filter.split(' '),
				column = split[0],
				condition = split[1],
				value = split.splice(2).join(' '),
				where = '(',
				addValue = true;

			where += column + ' ';
			var values = value.split('|');
			//get the column based on the column dataIndex
			var col = {};
			Ext.each(this.fieldColumns, function(fieldColumn) {
				if (fieldColumn.dataIndex == column) {
					col = fieldColumn;
					return false;
				}
			});
			Ext.each(values, function(value, index) {
				if (value) {
					if (index > 0) where += 'OR ' + column + ' ';
					if (col.dataType == 'bool') {
						value = value === 'true' ? '1' : '0';
					}
					//Get condition equivalent
					switch (condition.toLowerCase()) {
						case 'on':
							where += "BETWEEN (date '{0}') AND (date '{1}')";
							break;
						case 'after':
							where += "> (date '{0}')";
							break;
						case 'before':
							where += "< (date '{0}')";
							break;
						case 'equals':
							var eqCond = Ext.isNumber(Number(value)) ? '=' : "= '{0}'";
							if (value === 'NULL') {
								eqCond = 'IS NULL';
								addValue = false;
							}
							if (value === 'NIL') {
								eqCond = Ext.String.format("IS NULL OR {0} = ''", column);
								addValue = false;
							}
							where += eqCond;
							break;
						case 'notequals':
							var eqCond = Ext.isNumber(Number(value)) ? '<>' : "<> '{0}'";
							if (value === 'NULL') {
								eqCond = 'IS NOT NULL';
								addValue = false;
							}
							if (value === 'NIL') {
								eqCond = Ext.String.format("IS NOT NULL OR {0} <> ''", column);
								addValue = false;
							}
							where += eqCond;
							break;
						case 'contains':
							where += "LIKE '%{0}%'";
							break;
						case 'notcontains':
							where += "NOT LIKE '%{0}%'";
							break;
						case 'startswith':
							where += "LIKE '{0}%'";
							break;
						case 'notstartswith':
							where += "NOT LIKE '{0}%'";
							break;
						case 'endswith':
							where += "LIKE '%{0}'";
							break;
						case 'notendswith':
							where += "NOT LIKE '%{0}'";
							break;
						case 'greaterthan':
							where += '> {0}';
							break;
						case 'greaterthanorequalto':
							where += '>= {0}';
							break;
						case 'lessthan':
							where += '< {0}';
							break;
						case 'lessthanorequalto':
							where += '<= {0}';
							break;
						default:
							where += '=';
					}

					where += ' ';

					switch (value) {
						case 'today':
							where = Ext.String.format(where, Ext.Date.format(new Date(), this.dateFormat), Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, 1), this.dateFormat));
							break;
						case 'yesterday':
							where = Ext.String.format(where, Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, -1), this.dateFormat), Ext.Date.format(Ext.Date.format(new Date(), this.dateFormat)));
							break;
						case 'lastweek':
							where = Ext.String.format(where, Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.WEEK, -1), this.dateFormat), Ext.Date.format(Ext.Date.format(new Date(), this.dateFormat)));
							break;
						case 'lastmonth':
							where = Ext.String.format(where, Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.MONTH, -1), this.dateFormat), Ext.Date.format(Ext.Date.format(new Date(), this.dateFormat)));
							break;
						case 'lastyear':
							where = Ext.String.format(where, Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, -1), this.dateFormat), Ext.Date.format(Ext.Date.format(new Date(), this.dateFormat)));
							break;
						default:
							if (addValue) {
								var val = value,
									secondVal = '';
								if (col.dataType == 'date') {
									val = Ext.Date.parse(value, this.dateFormat)
									if (!Ext.isDate(val)) val = Ext.Date.parse(value, 'c');
									if (Ext.isDate(val)) {
										value = Ext.Date.format(val, this.submitFormat);
										secondVal = Ext.Date.format(Ext.Date.add(val, Ext.Date.DAY, 1), this.submitFormat);
									}
								}

								if (where.indexOf('{0}') > -1) where = Ext.String.format(where, value.replace(/'/g, "''"), secondVal.replace(/'/g, "''"));
								else where += value.replace(/'/g, "''");
							}
					}
				}
			}, this);

			whereClause += (index > 0 ? ' AND ' : ' ') + where + ')';
		}, this);
		return whereClause;
	},
	getOptionsForDataType: function(column) {
		var me = this,
			options = [];
		switch (column.dataType) {
			case 'date':
				options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.on),
					value: RS.common.locale.on,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.on)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.before),
					value: RS.common.locale.before,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.before)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.after),
					value: RS.common.locale.after,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.after)
				}];
				break;
			case 'float':
			case 'int':
				options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.equals),
					value: RS.common.locale.equals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.equals)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notEquals),
					value: RS.common.locale.notEquals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notEquals)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.greaterThan),
					value: RS.common.locale.greaterThan,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.greaterThan)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.greaterThanOrEqualTo),
					value: RS.common.locale.greaterThanOrEqualTo,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.greaterThanOrEqualTo)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.lessThan),
					value: RS.common.locale.lessThan,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.lessThan)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.lessThanOrEqualTo),
					value: RS.common.locale.lessThanOrEqualTo,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.lessThanOrEqualTo)
				}];
				break;
			case 'bool':
				options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.equals),
					value: RS.common.locale.equals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.equals)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notEquals),
					value: RS.common.locale.notEquals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notEquals)
				}];
				break;
			default:
				options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.equals),
					value: RS.common.locale.equals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.equals)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notEquals),
					value: RS.common.locale.notEquals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notEquals)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.contains),
					value: RS.common.locale.contains,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.contains)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notContains),
					value: RS.common.locale.notContains,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notContains)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.startsWith),
					value: RS.common.locale.startsWith,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.startsWith)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notStartsWith),
					value: RS.common.locale.notStartsWith,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notStartsWith)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.endsWith),
					value: RS.common.locale.endsWith,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.endsWith)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notEndsWith),
					value: RS.common.locale.notEndsWith,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notEndsWith)
				}];
				if (!me.showComparisons) options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.equals),
					value: RS.common.locale.equals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.equals)
				}]
				break;
		}
		return options;
	},
	getShortOptionsForDataType: function(type) {
		var options = [];
		switch (type) {
			case 'date':
				options = [{
					name: Ext.String.format('{0}', RS.common.locale.on),
					value: Ext.String.format('{0}', RS.common.locale.on)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.before),
					value: Ext.String.format('{0}', RS.common.locale.before)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.after),
					value: Ext.String.format('{0}', RS.common.locale.after)
				}];
				break;
			case 'float':
			case 'int':
				options = [{
					name: Ext.String.format('{0}', RS.common.locale.shortEquals),
					value: Ext.String.format('{0}', RS.common.locale.shortEquals)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortNotEquals),
					value: Ext.String.format('{0}', RS.common.locale.shortNotEquals)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortGreaterThan),
					value: Ext.String.format('{0}', RS.common.locale.shortGreaterThan)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortGreaterThanOrEqualTo),
					value: Ext.String.format('{0}', RS.common.locale.shortGreaterThanOrEqualTo)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortLessThan),
					value: Ext.String.format('{0}', RS.common.locale.shortLessThan)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortLessThanOrEqualTo),
					value: Ext.String.format('{0}', RS.common.locale.shortLessThanOrEqualTo)
				}];
				break;
			case 'bool':
				options = [{
					name: Ext.String.format('{0}', RS.common.locale.equals),
					value: Ext.String.format('{0}', RS.common.locale.equals)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notEquals),
					value: Ext.String.format('{0}', RS.common.locale.notEquals)
				}];
				break;
			default:
				options = [{
					name: Ext.String.format('{0}', RS.common.locale.equals),
					value: Ext.String.format('{0}', RS.common.locale.equals)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notEquals),
					value: Ext.String.format('{0}', RS.common.locale.notEquals)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.contains),
					value: Ext.String.format('{0}', RS.common.locale.contains)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notContains),
					value: Ext.String.format('{0}', RS.common.locale.notContains)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.startsWith),
					value: Ext.String.format('{0}', RS.common.locale.startsWith)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notStartsWith),
					value: Ext.String.format('{0}', RS.common.locale.notStartsWith)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.endsWith),
					value: Ext.String.format('{0}', RS.common.locale.endsWith)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notEndsWith),
					value: Ext.String.format('{0}', RS.common.locale.notEndsWith)
				}];
		}
		return options;
	},
	getValueOptionsForDataType: function(column, columnText, operatorText, dataValue) {
		var options = [],
			returnOptions = [];
		switch (column.dataType) {
			case 'date':
				options = [{
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.today),
					value: RS.common.locale.today,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.today)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.yesterday),
					value: RS.common.locale.yesterday,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.yesterday)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.lastWeek),
					value: RS.common.locale.lastWeek,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.lastWeek)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.lastMonth),
					value: RS.common.locale.lastMonth,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.lastMonth)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.lastYear),
					value: RS.common.locale.lastYear,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.lastYear)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, Ext.Date.format(new Date(), this.dateFormat)),
					value: Ext.Date.format(new Date(), this.dateFormat),
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, Ext.Date.format(new Date(), this.dateFormat))
				}];
				break;
		}
		Ext.each(options, function(option) {
			if (option.value.indexOf(dataValue) > -1) returnOptions.push(option);
		});
		return returnOptions;
	},
	getFiltersString: function() {
		var filters = this.filterBar.getFilters(),
			filterString = '';
		Ext.each(filters, function(filter, index) {
			if (index > 0) filterString += '|&&|';
			filterString += filter.replace(/ /g, '^')
		});
		return filterString;
	}
});
/*
Ext.define('RS.common.SinglePlupUpload', {
	extend: 'Ext.util.Observable',
	configs: {
		uploader: {
			runtimes: "html5,flash,silverlight,html4",
			url: '',
			browse_button: null,
			flash_swf_url: '/resolve/js/plupload/Moxie.swf',
			unique_names: true,
			multipart: true,
			multipart_params: {},
			multi_selection: false,
			browse: true,
			required_features: true
		},

		uploaderListener: {
			/**
			 * Plupload EVENTS
			 * /
			_Init: function(uploader, data) {
				this.runtime = data.runtime;
				this.fireEvent('uploadready', this);
			},

			// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			_BeforeUpload: function(uploader, file) {
				this.fireEvent('beforeupload', this, uploader, file);
			},

			_ChunkUploaded: function() {},

			_FilesAdded: function(uploader, files) {
				var me = this;

				if (uploader.settings.multi_selection != true) {
					files = [files[0]];
					uploader.files = [files[0]];
				}

				if (me.fireEvent('filesadded', me, files) !== false) {
					if (uploader.settings.autoStart && uploader.state != 2) Ext.defer(function() {
						me.start();
					}, 300);
				}
			},

			_FilesRemoved: function(uploader, files) {

			},

			_FileUploaded: function(uploader, file, status) {
				var me = this,
					response = Ext.JSON.decode(status.response);

				if (response.success == true) {
					Ext.apply(file, response.data);
					file.server_error = 0;
					me.success.push(file);
					me.fireEvent('fileuploaded', me, file, response);
				} else {
					if (response.message) {
						file.msg = '<span style="color: red">' + response.message + '</span>';
					}
					file.server_error = 1;
					me.failed.push(file);
					me.fireEvent('uploaderror', me, Ext.apply(status, {
						file: file
					}));
				}
			},

			_PostInit: function(uploader) {},

			_QueueChanged: function(uploader) {},

			_Refresh: function(uploader) {},

			_StateChanged: function(uploader) {
				if (uploader.state == 2) {
					this.uploader.disableBrowse(true);
					this.fireEvent('uploadstarted', this);
				} else {
					this.uploader.disableBrowse(false);
					this.fireEvent('uploadcomplete', this, this.success, this.failed);
					if (this.autoRemoveUploaded) this.removeUploaded();
				}
			},

			_UploadFile: function(uploader, file) {},

			_UploadProgress: function(uploader, file) {
				var me = this,
					name = file.name,
					size = file.size,
					percent = file.percent;
				me.fireEvent('uploadprogress', me, file, name, size, percent);
				if (file.server_error) file.status = 4;
			},

			_Error: function(uploader, data) {
				if (data.file) {
					data.file.status = 4;
					if (data.code == -600) {
						data.file.msg = Ext.String.format('<span style="color: red">{0}</span>', this.statusInvalidSizeText);
					} else if (data.code == -700) {
						data.file.msg = Ext.String.format('<span style="color: red">{0}</span>', this.statusInvalidExtensionText);
					} else {
						data.file.msg = Ext.String.format('<span style="color: red">{2} ({0}: {1})</span>', data.code, data.details, data.message);
					}
					this.failed.push(data.file);
				}
				this.fireEvent('uploaderror', this, data);
			}
		}
	},

	constructor: function(owner) {
		var me = this;
		me.owner = owner;
		me.success = [];
		me.failed = [];
		me.addEvents('beforestart', 'uploadready', 'uploadstarted', 'uploadcomplete', 'uploaderror', 'filesadded', 'beforeupload', 'fileuploaded', 'updateprogress', 'uploadprogress', 'storeempty', 'uploadstopped');
		me.listeners = Ext.apply({}, owner.uploaderListeners);
		me.callParent();
	},

	/**
	 * @private
	 * /
	initialize: function(config) {
		var me = this;
		if (!me.initialized) {
			me.initialized = true;
			me.initializeUploader(config);
		}
	},

	initializeUploader: function(config) {
		var me = this;
		me.uploaderConfig = Ext.applyIf(config.uploader, me.configs.uploader);
		me.uploader = Ext.create('plupload.Uploader', me.uploaderConfig);

		Ext.each([
			'Init',
			'ChunkUploaded',
			'FilesAdded',
			'FilesRemoved',
			'FileUploaded',
			'PostInit',
			'QueueChanged',
			'Refresh',
			'StateChanged',
			'BeforeUpload',
			'UploadFile',
			'UploadProgress',
			'Error'
		], function(v) {
			me.uploader.bind(v, me.configs.uploaderListener['_' + v], me);
		}, me);

		me.uploader.init();
	},

	start: function() {
		var me = this;
		me.fireEvent('beforestart', me);
		if (me.multipart_params) {
			me.uploader.settings.multipart_params = me.multipart_params;
		}
		me.uploader.start();
	},

	stop: function() {
		this.uploader.stop();
		this.fireEvent('uploadstopped', this);
	},

	clearQueue: function() {
		this.uploader.splice();
	}
});
*/
//Custom Event for IE https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent
(function () {

  if ( typeof window.CustomEvent === "function" ) return false;

  function CustomEvent ( event, params ) {
    params = params || { bubbles: false, cancelable: false, detail: undefined };
    var evt = document.createEvent( 'CustomEvent' );
    evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
    return evt;
   }

  CustomEvent.prototype = window.Event.prototype;

  window.CustomEvent = CustomEvent;

  // https://tc39.github.io/ecma262/#sec-array.prototype.includes
if (!Array.prototype.includes) {
  Object.defineProperty(Array.prototype, 'includes', {
    value: function(searchElement, fromIndex) {

      // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If len is 0, return false.
      if (len === 0) {
        return false;
      }

      // 4. Let n be ? ToInteger(fromIndex).
      //    (If fromIndex is undefined, this step produces the value 0.)
      var n = fromIndex | 0;

      // 5. If n ≥ 0, then
      //  a. Let k be n.
      // 6. Else n < 0,
      //  a. Let k be len + n.
      //  b. If k < 0, let k be 0.
      var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

      function sameValueZero(x, y) {
        return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
      }

      // 7. Repeat, while k < len
      while (k < len) {
        // a. Let elementK be the result of ? Get(O, ! ToString(k)).
        // b. If SameValueZero(searchElement, elementK) is true, return true.
        // c. Increase k by 1. 
        if (sameValueZero(o[k], searchElement)) {
          return true;
        }
        k++;
      }

      // 8. Return false
      return false;
    }
  });
}

if (typeof Object.assign != 'function') {
  Object.assign = function(target, varArgs) { // .length of function is 2
    'use strict';
    if (target == null) { // TypeError if undefined or null
      throw new TypeError('Cannot convert undefined or null to object');
    }

    var to = Object(target);

    for (var index = 1; index < arguments.length; index++) {
      var nextSource = arguments[index];

      if (nextSource != null) { // Skip over if undefined or null
        for (var nextKey in nextSource) {
          // Avoid bugs when hasOwnProperty is shadowed
          if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
            to[nextKey] = nextSource[nextKey];
          }
        }
      }
    }
    return to;
  };
}
})();

Ext.define('RS.common.selection.CheckboxModel', {
	extend: 'Ext.selection.CheckboxModel',
	alias: 'selection.resolvecheckboxmodel',

	columnTooltip: '',
	glyph : '',
	columnTarget: '_self',
	columnEventName: 'editAction',
	headerWidth: 40,
	forceFire: false,
	cellNavigation: false,
	currentCell: {
		row: 0,
		column: 0
	},

	/**
	 * Retrieve a configuration to be used in a HeaderContainer.
	 * This should be used when injectCheckbox is set to false.
	 */
	getHeaderConfig: function() {
		var me = this,
			showCheck = me.showHeaderCheckbox !== false,
			canSelectEntireTable = me.canSelectEntireTable === true;
		return {
			xtype: 'viewcolumn',
			align : 'center',
			isCheckerHd: showCheck,
			text: canSelectEntireTable ? '&#160;<div class="x-column-header-trigger"></div>' : '',
			width: me.headerWidth,
			sortable: false,
			draggable: false,
			resizable: false,
			hideable: false,
			hidden: (me.view.up('grid') && me.view.up('grid').hideSelModel === true),
			menuDisabled: true,
			dataIndex: '',
			tooltip: RS.common.locale.selectAllTooltip,
			viewTooltip: me.columnTooltip,
			glyph : me.glyph,
			target: me.columnTarget,
			eventName: me.columnEventName,
			forceFire: me.forceFire,
			idField: me.columnIdField || 'sys_id',
			cls: showCheck ? Ext.baseCSSPrefix + 'column-header-checkbox ' : '',
			editRenderer: me.editRenderer || me.renderEmpty,
			locked: me.hasLockedHeader(),
			childLink: me.childLink,
			childLinkIdProperty: me.childLinkIdProperty,
			listeners: {
				render: function(column, eOpts) {
					var header = column.getEl(),
						hoverCls = Ext.baseCSSPrefix + 'column-header-over';
					header.on('mouseover', function() {
						column.titleEl.addCls(hoverCls);
					});
					header.on('mouseout', function() {
						column.titleEl.removeCls(hoverCls);
					});
				}
			}
		};
	},

	navigateCell: function(evt) {
		if (!this.cellNavigation)
			return;
		var columns = this.view.getGridColumns();
		var c = this.currentCell.column;
		switch (evt.getCharCode()) {
			case evt.UP:
				if (this.currentCell.row > 0)
					this.currentCell.row--;
				break;
			case evt.DOWN:
				if (this.currentCell.row < this.store.getCount() - 1)
					this.currentCell.row++;
				break;
			case evt.LEFT:
				while (c-- > 1) {
					if (!this.canNavigateToCol(columns[c], c)) continue;
					else {
						this.currentCell.column = c;
						break;
					}
				}
				break;
			case evt.RIGHT:
				while (c++ < columns.length - 1)
					if (!this.canNavigateToCol(columns[c], c)) continue;
					else {
						this.currentCell.column = c;
						break;
					}
				break;
		}
		this.view.panel.fireEvent('navigatecell', this);
	},

	onKeyUp: function(evt) {
		this.callParent(arguments);
		this.navigateCell(evt);
	},

	onKeyDown: function(evt) {
		this.callParent(arguments);
		this.navigateCell(evt);
	},

	onKeyLeft: function(evt) {
		this.callParent(arguments);
		this.navigateCell(evt);
	},

	onKeyRight: function(evt) {
		this.callParent(arguments);
		this.navigateCell(evt);
	},

	/**
	 * Toggle between selecting all and deselecting all when clicking on
	 * a checkbox header.
	 */
	onHeaderClick: function(headerCt, header, e) {
		var me = this,
			t = e.getTarget('.x-column-header-trigger');

		if (t) {
			//Trying to select the menu instead of the checkbox
			e.stopEvent();
			var grid = header.up('grid'),
				selectionsCount = grid.getStore().getTotalCount();
			Ext.create('Ext.menu.Menu', {
				items: [{
					text: Ext.String.format(RS.common.locale.selectAll, selectionsCount),
					scope: grid,
					handler: function(btn) {
						me.preventFocus = true;
						if (isChecked) {
							me.deselectAll();
						} else {
							me.selectAll();
						}
						delete me.preventFocus;
						this.fireEvent('selectAllAcrossPages', this, true);
					}
				}]
			}).showBy(t, 'tl-bl');
		} else {
			if (header.isCheckerHd) {
				e.stopEvent();
				var isChecked = header.el.hasCls(Ext.baseCSSPrefix + 'grid-hd-checker-on');

				// Prevent focus changes on the view, since we're selecting/deselecting all records
				me.preventFocus = true;
				if (isChecked) {
					me.deselectAll();
				} else {
					me.selectAll();
				}
				delete me.preventFocus;
			}
		}
	}
});


Ext.define("RS.common.selection.ActionModel",{
	extend : "Ext.selection.CheckboxModel",
	alias : "selection.resolveactionmodel",
	
	//Default config, can be overrided by define with the same properties name when using it in selModel: {}
	editAction : false,
	editTooltip : '',
	editEventName : 'editAction',
	deleteAction : true,	
	deleteTooltip : '',
	deleteEventName : 'deleteAction',
	injectCheckbox : 'last',
	showHeaderCheckbox : false,
	header : true,
	actionHidden : false,
	idField : 'sys_id',

	getHeaderConfig : function(){
		var me = this;
		return {
			xtype : 'actioncolumn',
			deleteTooltip : me.deleteTooltip,
			deleteEventName : me.deleteEventName,
			editAction : me.editAction,
			editTooltip : me.editTooltip,
			editEventName : me.editEventName,
			sortable: false,
			draggable: false,
			resizable: false,
			align : 'center',
			width : 100,
			text : me.header ? "Actions" : null,
			idField : me.idField,
			hidden : (me.actionHidden === true) ? true : false
		}
	}
});
/**
    This module defines a single file upload form.
*/
/*
Ext.define('RS.formbuilder.viewer.desktop.fields.FileUpload', {
    extend: 'Ext.form.Panel',
    alias: 'widget.rsfileupload',

    width: 500,
    bodyPadding: 10,

    config: {
        // See 'RS.common.HtmlLintEditor' for usage.
        ulr: '',                // Back end upload api
        callback: undefined,    // callback on successful upload
        view: undefined,        // The parent container
        autoUpload: false,      // Automatically upload when a valid file is added
        multipart_params: {},   // extra payload sent to the backend along with the uploading file
        filters: {}             // File extension filter
    },
    buttonAlign: 'left',
    items: [{
        xtype: 'progressbar',
        itemId: 'progressBar'
    }, {
        xtype: 'label',
        itemId: 'fileName',
        html: RS.common.locale.noFileSelected
    }],

    buttons: [{
        text: RS.common.locale.selectAWordDocFile,
        itemId: 'browseButton'
    }],

    listeners: {
        afterrender: function(form, eOpts) {
            var me = form;
            var uploadButton = me.down('#uploadButton')
                browseButton = me.down('#browseButton'),
                progressBar  = me.down('#progressBar');
            plupload.addFileFilter('special_chars_not_allowed', function(charsNotAllowed, file, cb) {
                if (file.name.match(RegExp(charsNotAllowed,'g'))) {
                    this.trigger('Error', {
                      code : plupload.FAILED,
                      message : Ext.String.format(RS.common.locale.notAllowedCharacters, charsNotAllowed.replace(/\|\\|\|/g, ','), file.name),
                      file : file
                    });
                    cb(false);
                } else {
                    cb(true);
                }
            });
            var uploader = new plupload.Uploader({
              browse_button: me.down('#browseButton').getId(),
              url: me.url,
              multipart_params: me.multipart_params,
              filters: me.filters,
            });
            uploader.init();
            uploader.bind('FilesAdded', function(up, files) {
                me.down('#fileName').setText(files[0].name, false);
                if (!me.autoUpload) {
                    uploadButton.setDisabled(false)
                } else {
                    uploader.start();
                    progressBar.wait({
                        text: RS.common.locale.updating
                    });
                    browseButton.setDisabled(true);
                } 
            });

            uploader.bind('Error', function(up, err) {
                me.down('#fileName').setText(err.message + RS.common.locale.pleaseSelectAWordDoc, false);
                !me.autoUpload? setDisabled(true): null;
            });

            uploader.bind('FileUploaded', function(up, file, response) {
                if (!!me.callback) {
                    var jsonResponse = Ext.decode(response.response);
                    if (jsonResponse.success) {
                        me.callback.call(me.view, jsonResponse);
                        clientVM.displaySuccess(RS.common.locale.succeededUploadingWordDoc, null,RS.common.locale.succeeded);
                    } else {
                        clientVM.displayError(RS.common.locale.failedUploadingWordDoc, null, RS.common.locale.failed);
                    }
                    form.up('window').close();
                }
            })

            !me.autoUpload? uploadButton.on('click', function () {
                uploader.start();
                uploadButton.setDisabled(true);
                browseButton.setDisabled(true);
                progressBar.wait({
                    text: RS.common.locale.updating
                });
            }): null;
        }
    },

    initComponent: function() {
        if (!this.autoUpload) {
            this.buttons.push({
                    text: RS.common.locale.upload,
                    itemId: 'uploadButton',
                    disabled: true
            });
        }
        this.callParent();
    }
});
*/
Ext.define('RS.formbuilder.viewer.desktop.fields.UploadManager', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.uploadmanager',

	emptyText: '',
	allowDeselect: true,

	anchor: '-20',
	height: 300,

	resizable: true,
	resizeHandles: 's',

	statusBarHidden: false,

	documentName: '',

	stopOnFirstInvalidFile: false,
	allowedExtensions: [],
	sizeLimit: null,
	autoUpload: true,
	checkForDuplicates: true,
	uploadBtnName: 'upload_button',
	allowMultipleFiles: true,
	allowDownload: true,
	showUploadedBy: true,
	inputName: 'name',
	csrftoken: '',

	config: {
		api: {
			upload: '/resolve/service/form/upload',
			downloadViaDocName: '/resolve/service/wiki/download/{0}?attach={1}',
			downloadViaId: '/resolve/service/form/downloadFile?sys_id={0}&tableName={1}&{2}',
			delete: '/resolve/service/wikiadmin/attachment/delete',
			list: '/resolve/service/wiki/getAttachments'
		},
		recordId: ''
	},

	store: null,
	initStore: function() {
		this.store = Ext.create('Ext.data.Store', {
			fields: ['sys_id', 'downloadUrl', {
				name: 'name',
				type: 'String'
			}, 'size', 'sysCreatedBy', {
				name: 'sysCreatedOn',
				type: 'date',
				dateFormat: 'time',
				format: 'time'
			}, 'status'],
			sorters: [{
				property: 'name',
				direction: 'ASC'
			}]
		});
	},

	/*
	uploader: {
		url: '/resolve/service/form/upload',
		uploadpath: 'ryan',
		autoStart: true,
		max_file_size: '2020mb',
		flash_swf_url: '/resolve/js/plupload/Moxie.swf',
		urlstream_upload: true
	},
	*/

	uploader: null,
	initUploader: function() {
		var me = this;

		var fileuploadMaxSize = (clientVM.fileuploadMaxSize || 500) * 1000000; // in Bytes

		if (this.allowedFileTypes) {
			var allowedFileTypesList = this.allowedFileTypes.split(',');
			for (var i=0; i<allowedFileTypesList.length; i++) {
				allowedFileTypesList[i] = allowedFileTypesList[i].trim();
			}
			this.allowedExtensions = allowedFileTypesList;
		}

		this.uploader = new qq.FineUploaderBasic({
			//debug: true,
			autoUpload: this.autoUpload,
			button: document.getElementById(this.uploadBtnName),
			multiple: this.allowMultipleFiles,
			request: {
				endpoint: this.api ? this.api.upload : '/resolve/service/wiki/attachment/upload',
				filenameParam: 'name',
				inputName: 'name',
				params: {
					recordId: me.recordId,
					tableName: me.fileUploadTableName,
					columnName: me.referenceColumnName
				}
			},
			validation: {
				allowedExtensions: this.allowedExtensions,
				sizeLimit: this.sizeLimit || fileuploadMaxSize,
				stopOnFirstInvalidFile: this.stopOnFirstInvalidFile
			},
			callbacks: {
				onValidateBatch: function(fileOrBlobDataArray) {
					if (me.checkForDuplicates) {
						var duplicateFiles = [];
						for (var i=0; i<fileOrBlobDataArray.length; i++) {
							var file = fileOrBlobDataArray[i];
							var dup = me.store.findRecord('name', file.name, 0, false, false, true);
							if (dup) {
								duplicateFiles.push(file.name);
							}
						}

						if (duplicateFiles.length) {
							var promise = new qq.Promise();

							var msg = duplicateFiles.join();
							if (duplicateFiles.length > 1) {
								msg += ' ' + RS.common.locale.FileUploadManager.duplicatesDetected;
							} else {
								msg += ' ' + RS.common.locale.FileUploadManager.duplicateDetected;
							}
						
							Ext.MessageBox.show({
								title: RS.common.locale.FileUploadManager.duplicateWarn,
								msg: msg,
								buttons: Ext.MessageBox.YESNO,
								buttonText: {
									yes: RS.common.locale.FileUploadManager.override,
									no: RS.common.locale.FileUploadManager.no
								},
								scope: this,
								fn: function(btn) {
									if (btn === 'yes') {
										promise.success();
									} else {
										promise.failure();
									}
								}
							});
						
							return promise;
						}
					}
				},
				onSubmitted: function(id, name) {
					if (Ext.isFunction(me.fileOnSubmittedCallback)) {
						me.fileOnSubmittedCallback(this, id, name);
					}
				},
				onCancel: function(id, name) {
					if (Ext.isFunction(me.fileOnCancelCallback)) {
						me.fileOnCancelCallback(this, id, name)
					}
				},
				onUpload: function(id, name) {
					if (Ext.isFunction(me.fileOnUploadCallback)) {
						me.fileOnUploadCallback(this, name)
					}
				},
				onProgress: function(id, name, uploadedBytes, totalBytes) {
					if (Ext.isFunction(me.fileOnProgressCallback)) {
						me.fileOnProgressCallback(this, id, name, uploadedBytes, totalBytes)
					}
				},
				onError: function(id, name, errorReason, xhr) {
					if (Ext.isFunction(me.fileOnErrorCallback)) {
						me.fileOnErrorCallback(this, name, errorReason, xhr)
					}
				},
				onComplete: function(id, name, responseJSON, xhr) {
					if (Ext.isFunction(me.fileOnCompleteCallback)) {
						me.fileOnCompleteCallback(this, name, responseJSON, xhr)
					}
				},
				onAllComplete: function(succeeded, failed) {
					if (!failed.length) {
						clientVM.displaySuccess(RS.common.locale.FileUploadManager.uploadSuccess);
					}

					if (Ext.isFunction(me.filesOnAllCompleteCallback)) {
						me.filesOnAllCompleteCallback(this, succeeded, failed)
					}
				}
			},
			showMessage: function(message) {
				clientVM.displayError(message);
			}
		});
	},

	fineUploaderTemplateHeight: 388,
	fineUploaderTemplateHidden: true,

	initComponent: function() {
		var me = this;

		clientVM.getCSRFToken_ForURI('/resolve/service/user/downloadFile', function(token_pair) {
			this.csrftoken = token_pair[0] + '=' + token_pair[1];
		}.bind(this))

		this.initStore();

		this.title = this.displayName || this.title;

		this.columns = Ext.isFunction(this.getUploadColumns) ? this.getUploadColumns() : [];

		if (!me.columns.length) {
			me.columns = [{
				text: RS.formbuilder.locale.uploadColumnName,
				flex: 3,
				sortable: false,
				dataIndex: 'name',
				renderer: function(value, metaData, record) {
					return record.get(me.nameColumn) || record.get('name')
				}
			}, {
				text: RS.formbuilder.locale.uploadColumnSize,
				width: 90,
				sortable: true,
				renderer: Ext.util.Format.fileSize,
				dataIndex: 'size'
			}, {
				text: RS.formbuilder.locale.uploadColumnChange,
				width: 75,
				sortable: true,
				hidden: true,
				hideable: false,
				dataIndex: 'percent'
			}, {
				text: RS.formbuilder.locale.uploadColumnState,
				width: 75,
				hidden: true,
				sortable: true,
				hideable: false,
				dataIndex: 'status'
			}, {
				text: RS.formbuilder.locale.uploadColumnStatus,
				width: 175,
				sortable: true,
				dataIndex: 'msg'
			}, {
				text: RS.formbuilder.locale.sysUploadedOn,
				width: 200,
				sortable: true,
				dataIndex: 'sysCreatedOn',
				renderer: function(value) {
					var r = Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat());
					if (Ext.isString(value))
						return value;
					return r(value);
				}
			}, {
				text: RS.formbuilder.locale.uploadColumnUploadedBy,
				sortable: true,
				flex: 1,
				dataIndex: 'sysCreatedBy',
			}, {
				xtype: 'actioncolumn',
				hideable: false,
				hidden: !this.allowDownload,
				width: 38,
				items: [{
					icon: '/resolve/images/arrow_down.png',
					// Use a URL in the icon config
					tooltip: RS.formbuilder.locale.download,
					handler: function(gridView, rowIndex, colIndex) {
						var rec = gridView.getStore().getAt(rowIndex),
							sys_id = rec.get('sys_id'),
							api = gridView.ownerCt.api;
						if (gridView.ownerCt.documentName && gridView.ownerCt.documentName.length > 1) { // Configurable
							gridView.ownerCt.downloadURL(rec.raw.downloadUrl || Ext.String.format(api.downloadViaDocName, gridView.ownerCt.documentName.replace(/[.]/g, '/'), rec.get('id')))
							return
						}
	
						if (sys_id) gridView.ownerCt.downloadURL(Ext.String.format(api.downloadViaId, sys_id, gridView.ownerCt.fileUploadTableName, gridView.ownerCt.csrftoken));
						else clientVM.displayError(RS.formbuilder.locale.noDownloadBody)
					}
				}]
			}]
		}

		this.tbar = [{
			text: RS.formbuilder.locale.upload,
			name: 'uploadFile',
			itemId: 'uploadButton',
			id: 'upload_button',
			tooltip: RS.formbuilder.locale.uploadTooltip,
			hidden: !this.allowUpload,
			preventDefault: false
		}, {
			text: RS.formbuilder.locale.removeFile,
			disabled: true,
			name: 'removeFile',
			itemId: 'removeButton',
			hidden: !this.allowRemove,
			scope: this,
			handler: function(button) {
				var selections = this.getSelectionModel().getSelection();
				Ext.MessageBox.show({
					title: RS.formbuilder.locale.confirmRemove,
					msg: selections.length === 1 ? RS.formbuilder.locale.confirmRemoveFile : Ext.String.format(RS.formbuilder.locale.confirmRemoveFiles, selections.length),
					buttons: Ext.MessageBox.YESNO,
					buttonText: {
						yes: RS.formbuilder.locale.removeFile,
						no: RS.formbuilder.locale.cancel
					},
					scope: me,
					fn: me.removeSelectedFiles
				});
			}
		}];

		this.statusBar = new Ext.ux.StatusBar({
			dock: 'bottom',
			defaultText: RS.formbuilder.locale.ready,
			hidden: this.statusBarHidden
		});
		this.dockedItems = [this.statusBar];

		this.callParent();

		this.listeners = {
			afterRender: {
				fn: function() {
					me.initUploader();
					me.setEnabled(me.recordId);
				},
				single: true,
				scope: me
			}
		};

		this.on({
			scope: this,
			afterlayout: function(grid, layout, eOpts) {
				//var upEl = Ext.get(grid.uploader.uploader.id + '_' + grid.uploader.uploader.runtime + '_container');
				//if (upEl) upEl.setTop(0).setWidth(100);
			},
			afterrender: function(grid) {
				grid.view.refresh();
			},
			selectionchange: function(selectionModel, selections, eOpts) {
				if (selections.length > 0) this.down('#removeButton').enable();
				else this.down('#removeButton').disable();
			},
			/*
			updateprogress: function(uploader, total, percent, sent, success, failed, queued, speed) {
				var uploading = false;
				Ext.each(uploader.uploader.files, function(file) {
					if (file.status != plupload.DONE) uploading = true;
					return false;
				});

				if (uploading) {
					var t = Ext.String.format(RS.formbuilder.locale.updateProgressText, percent, sent, total);
					this.statusBar.showBusy({
						text: t,
						clear: false
					});
					this.statusBar.isBusy = true;
				} else {
					this.statusBar.isBusy = false;
					this.statusBar.clearStatus();
					this.statusBar.setStatus({
						text: RS.formbuilder.locale.ready
					});
				}
			},
			uploadprogress: function(uploader, file, name, size, percent) {
				// me.statusBar.setText(name + ' ' + percent + '%');
			},
			*/
			uploadcomplete: function(uploader, success, failed) {
				this.statusBar.isBusy = false;
				this.statusBar.clearStatus();
				this.statusBar.setStatus({
					text: RS.formbuilder.locale.ready
				});
			}
		});

		this.loadFileUploads();
	},

	fileOnSubmittedCallback: function(manager, filename) {
		this.uploader.setParams({
			recordId: this.recordId,
			tableName: this.fileUploadTableName,
			columnName: this.referenceColumnName
		});
	},

	fileOnErrorCallback: function(manager, name, reason, resp) {
		var respData = RS.common.parsePayload(resp);
		if (respData.message && respData.errCode && respData.errCode != 'INVALID_SERVER_RESPONSE') {
			clientVM.displayError(name + ': ' + respData.message);
		} else {
			clientVM.displayError(reason);
		}
	},

	filesOnAllCompleteCallback: function() {
		this.loadFileUploads();
	},

	removeSelectedFiles: function(button) {
		if (button === 'yes') {
			var selections = this.getSelectionModel().getSelection(),
				ids = [],
				fileIds = [];
			Ext.each(selections, function(selection) {
				ids.push(selection.get('sys_id'));
				fileIds.push(selection.get('id'));
			});
			//Ext.each(fileIds, function(fileId) {
			//	this.uploader.removeFile(fileId)
			//}, this);
			if (this.documentName && this.documentName.length > 1) {
				var api = this.api;
				this.lastDocumentName = this.documentName;
				Ext.Ajax.request({
					url: api.delete, // Configurable
					params: {
						docSysId: '',
						docFullName: this.documentName,
						ids: ids
					},
					success: this.loadFiles,
					scope: this
				})
			} else {
				Ext.Ajax.request({
					url: '/resolve/service/form/deleteFile',
					params: {
						tableName: this.fileUploadTableName,
						ids: ids
					},
					scope: this,
					success: this.removeDeletedFiles
				})
			}
		}
	},

	removeDeletedFiles: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			this.loadFileUploads();
		}
	},

	loadFileUploads: function() {
		if (this.preventLoading) return

		if (this.documentName && this.documentName.length > 1) {
			if (this.documentName.toLowerCase() != this.lastDocumentName) {
				var api = this.api;
				this.lastDocumentName = this.documentName.toLowerCase();
				Ext.Ajax.request({
					url: api.list,
					method: 'GET',
					params: {
						docSysId: '',
						docFullName: this.documentName
					},
					success: this.loadFiles,
					scope: this
				})
			}
		} else {
			var field = this.referenceColumnName;
			if (this.referenceColumnName == 'u_content_request' && this.fileUploadTableName != 'content_request') {
				field += '.sys_id';
			}

			Ext.Ajax.request({
				url: '/resolve/service/dbapi/getRecordData',
				params: {
					tableName: this.fileUploadTableName,
					type: 'table',
					start: 0,
					filter: Ext.encode([{
						field: field,
						type: 'auto',
						condition: 'equals',
						value: this.recordId || '__blank'
					}])
				},
				success: this.loadFiles,
				scope: this
			})
		}
	},

	loadFiles: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			if (response.records) {
				Ext.each(response.records, function(record) {
					Ext.apply(record, {
						id: record.sys_id || record.id,
						name: record.u_filename || record.name,
						size: record.u_size || record.size,
						sys_id: record.sys_id,
						sysCreatedOn: record.sysCreatedOn || record.sys_created_on,
						sysCreatedBy: record.sysCreatedBy || record.sys_created_by
					});
					//if (this.uploader && this.uploader.uploader)
					//	Ext.each(this.uploader.uploader.files || [], function(file) {
					//		if (file.sysId == record.sysId) {
					//			record.id = file.id;
					//		}
					//	});
				}, this);
				this.store.loadData(response.records, false);
			}
		}
	},

	/**
	 * @private
	 */
	/*
	createUploader: function() {
		return Ext.create('RS.formbuilder.viewer.desktop.fields.PlUpload', this, Ext.applyIf({
			listeners: {}
		}, this));
	}, 
	*/ 

	setEnabled: function(enable) {
		//if (enable) this.uploader.multipart_params.recordId = enable;
		enable = enable && !this.internalDisabled;
		var uploadButton = this.down('#uploadButton');
		uploadButton[enable ? 'enable' : 'disable']();
		uploadButton.ownerCt[enable ? 'enable' : 'disable']();
		//if (this.uploader.uploader) this.uploader.uploader.disabled = !enable;
		if (this.isEditMode === false) {
			this.view.emptyText = '';
		} else if (enable) {
			this.view.emptyText = RS.formbuilder.locale.dragDropDisabledEmptyText;
		} else {
			this.view.emptyText = RS.formbuilder.locale.noRecordIdForUpload;
		}
		this.view.refresh()
		//if (enable) this.uploader.uploader.refresh()
	},

	disable: function() {
		this.internalDisabled = true;
		this.setEnabled(false);
	},
	enable: function() {
		if (this.rendered) {
			this.internalDisabled = false;
			this.setEnabled(true);
		}
	},

	downloadURL: function(url) {
		var iframe;
		iframe = document.getElementById("hiddenDownloader");
		if (iframe === null) {
			iframe = document.createElement('iframe');
			iframe.id = "hiddenDownloader";
			iframe.style.display = 'none';
			document.body.appendChild(iframe);
		}
		iframe['src'] = url.replace(/javascript\:/g, '');
	},

	isUploading: function() {
		return this.statusBar.isBusy || false;
	},

	setDocumentName: function(documentName) {
		this.documentName = documentName;
		if (this.uploader) this.uploader.documentName = documentName;
		this.loadFileUploads();
	}
});

(function() {
	Ext.ns('RS.common.validation');
	var RSValidation = RS.common.validation || {};
	Ext.apply(RSValidation, {
		PlainText : /^[a-zA-Z0-9_\-](\s{0,1}[a-zA-Z0-9_\-,?!])*$/,		
		VariableName : /^[a-zA-Z_][a-zA-Z0-9_\-]*$/,
		TaskNamespaceAll : /^[a-zA-Z0-9_\-\.](\s{0,1}[a-zA-Z0-9_\-\.])*$/i,
		TaskNamespace : /^(?!resolve?$)^[a-zA-Z0-9_\-\.](\s{0,1}[a-zA-Z0-9_\-\.])*$/i,
		TaskName : /^[a-zA-Z0-9_\-](\s{0,1}[a-zA-Z0-9_\-])*$/
	})
}())
Ext.define('RS.common.HtmlLintEditor', {
    extend: 'Ext.form.HtmlEditor',
    xtype: 'wysiwyg',
    defaultValue: '',
	enableSourceEdit: false,
    defaultFont: 'Tahoma',
    enableUpload: true,
    regListComponents: new RegExp(/<p class="MsoList(?:Bullet|ParagraphCxSpFirst|ParagraphCxSpMiddle|ParagraphCxSpLast)"(?: style="([\s\S]+?)")?>(?:<!--\[if !supportLists\]-->([\s\S]*?)<!--\[endif\]-->|([\s\S]*?)<\/span>[\s\S]*?)(?:<\/span>)?([\s\S]*?)<\/p>[\n\s]{0,3}/gim),
    regListRelativeIndentation: new RegExp(/(?:text-indent:([-]?\d{0,2}?\.?\d{0,2})in;?)?(?:margin-left:([-]?\d{0,2}?\.?\d{0,2})in;?)?(?:tab-stops:([-]?\d{0,2}?\.?\d{0,2})in;?)?/g),
    regListExplicitIndentation: new RegExp(/mso-list:l\d{1} level(\d{1,2}) lfo(\d{1,2}?)/g),
    regListBullet: new RegExp(/([·o§Øv])/),
    regPoint: new RegExp(/((\d{1,3})\.(\d{1,2})pt)/g),
    regBorder: new RegExp(/(mso-element:para-border-div;(?!margin-bottom:10px))/gi),
    regReturn: new RegExp(/[\r\n]/g),
    changeFired: false,
    dirtyHtmlTags: [
        {regex: /<\\?\?xml[^>]*?>/gi, replaceVal: ''},
        {regex: /\s*PAGE[-:][^;"']*?/gi, replaceVal: ''},
        {regex: /\s*TAB[-:][^;"']*?/gi, replaceVal: '&nbsp;&nbsp;&nbsp;&nbsp;'},
        {regex: /\n{1,2}/g, replaceVal: ' ' },
        {regex: /<(\/?title|\/?meta|\/?head|\/?html|\/?body)[^>]*?>/gi, replaceVal: ''},
        {regex: /<o:p><\/o:p>/g, replaceVal: '' },
        {regex: /—/g, replaceVal: ' - '},
        {regex: /&zwj;/g, replaceVal:'' },
        {regex: /\u200d/g, replaceVal:'' },
        {regex: new RegExp(String.fromCharCode(161), 'g'), replaceVal: '&iexcl;'},
        {regex: new RegExp(String.fromCharCode(162), 'g'), replaceVal: '&cent;'},
        {regex: new RegExp(String.fromCharCode(163), 'g'), replaceVal: '&pound;'},
        {regex: new RegExp(String.fromCharCode(164), 'g'), replaceVal: '&curren;'},
        {regex: new RegExp(String.fromCharCode(165), 'g'), replaceVal: '&yen;'},
        {regex: new RegExp(String.fromCharCode(166), 'g'), replaceVal: '&brvbar;'},
        {regex: new RegExp(String.fromCharCode(167), 'g'), replaceVal: '&sect;'},
        {regex: new RegExp(String.fromCharCode(168), 'g'), replaceVal: '&uml;'},
        {regex: new RegExp(String.fromCharCode(169), 'g'), replaceVal: '&copy;'},
        {regex: new RegExp(String.fromCharCode(170), 'g'), replaceVal: '&ordf;'},
        {regex: new RegExp(String.fromCharCode(171), 'g'), replaceVal: '&laquo;'},
        {regex: new RegExp(String.fromCharCode(172), 'g'), replaceVal: '&not;'},
        {regex: new RegExp(String.fromCharCode(173), 'g'), replaceVal: '&shy;'},
        {regex: new RegExp(String.fromCharCode(174), 'g'), replaceVal: '&reg;'},
        {regex: new RegExp(String.fromCharCode(175), 'g'), replaceVal: '&macr;'},
        {regex: new RegExp(String.fromCharCode(176), 'g'), replaceVal: '&deg;'},
        {regex: new RegExp(String.fromCharCode(177), 'g'), replaceVal: '&plusmn;'},
        {regex: new RegExp(String.fromCharCode(178), 'g'), replaceVal: '&sup2;'},
        {regex: new RegExp(String.fromCharCode(179), 'g'), replaceVal: '&sup3;'},
        {regex: new RegExp(String.fromCharCode(180), 'g'), replaceVal: '&acute;'},
        {regex: new RegExp(String.fromCharCode(181), 'g'), replaceVal: '&micro;'},
        {regex: new RegExp(String.fromCharCode(182), 'g'), replaceVal: '&para;'},
        {regex: new RegExp(String.fromCharCode(183), 'g'), replaceVal: '&middot;'},
        {regex: new RegExp(String.fromCharCode(184), 'g'), replaceVal: '&cedil;'},
        {regex: new RegExp(String.fromCharCode(185), 'g'), replaceVal: '&sup1;'},
        {regex: new RegExp(String.fromCharCode(186), 'g'), replaceVal: '&ordm;'},
        {regex: new RegExp(String.fromCharCode(187), 'g'), replaceVal: '&raquo;'},
        {regex: new RegExp(String.fromCharCode(188), 'g'), replaceVal: '&frac14;'},
        {regex: new RegExp(String.fromCharCode(189), 'g'), replaceVal: '&frac12;'},
        {regex: new RegExp(String.fromCharCode(190), 'g'), replaceVal: '&frac34;'},
        {regex: new RegExp(String.fromCharCode(191), 'g'), replaceVal: '&iquest;'},
        {regex: new RegExp(String.fromCharCode(192), 'g'), replaceVal: '&Agrave;'},
        {regex: new RegExp(String.fromCharCode(193), 'g'), replaceVal: '&Aacute;'},
        {regex: new RegExp(String.fromCharCode(194), 'g'), replaceVal: '&Acirc;'},
        {regex: new RegExp(String.fromCharCode(195), 'g'), replaceVal: '&Atilde;'},
        {regex: new RegExp(String.fromCharCode(196), 'g'), replaceVal: '&Auml;'},
        {regex: new RegExp(String.fromCharCode(197), 'g'), replaceVal: '&Aring;'},
        {regex: new RegExp(String.fromCharCode(198), 'g'), replaceVal: '&AElig;'},
        {regex: new RegExp(String.fromCharCode(199), 'g'), replaceVal: '&Ccedil;'},
        {regex: new RegExp(String.fromCharCode(200), 'g'), replaceVal: '&Egrave;'},
        {regex: new RegExp(String.fromCharCode(201), 'g'), replaceVal: '&Eacute;'},
        {regex: new RegExp(String.fromCharCode(202), 'g'), replaceVal: '&Ecirc;'},
        {regex: new RegExp(String.fromCharCode(203), 'g'), replaceVal: '&Euml;'},
        {regex: new RegExp(String.fromCharCode(204), 'g'), replaceVal: '&Igrave;'},
        {regex: new RegExp(String.fromCharCode(205), 'g'), replaceVal: '&Iacute;'},
        {regex: new RegExp(String.fromCharCode(206), 'g'), replaceVal: '&Icirc;'},
        {regex: new RegExp(String.fromCharCode(207), 'g'), replaceVal: '&Iuml;'},
        {regex: new RegExp(String.fromCharCode(208), 'g'), replaceVal: '&ETH;'},
        {regex: new RegExp(String.fromCharCode(209), 'g'), replaceVal: '&Ntilde;'},
        {regex: new RegExp(String.fromCharCode(210), 'g'), replaceVal: '&Ograve;'},
        {regex: new RegExp(String.fromCharCode(211), 'g'), replaceVal: '&Oacute;'},
        {regex: new RegExp(String.fromCharCode(212), 'g'), replaceVal: '&Ocirc;'},
        {regex: new RegExp(String.fromCharCode(213), 'g'), replaceVal: '&Otilde;'},
        {regex: new RegExp(String.fromCharCode(214), 'g'), replaceVal: '&Ouml;'},
        {regex: new RegExp(String.fromCharCode(215), 'g'), replaceVal: '&times;'},
        {regex: new RegExp(String.fromCharCode(216), 'g'), replaceVal: '&Oslash;'},
        {regex: new RegExp(String.fromCharCode(217), 'g'), replaceVal: '&Ugrave;'},
        {regex: new RegExp(String.fromCharCode(218), 'g'), replaceVal: '&Uacute;'},
        {regex: new RegExp(String.fromCharCode(219), 'g'), replaceVal: '&Ucirc;'},
        {regex: new RegExp(String.fromCharCode(220), 'g'), replaceVal: '&Uuml;'},
        {regex: new RegExp(String.fromCharCode(221), 'g'), replaceVal: '&Yacute;'},
        {regex: new RegExp(String.fromCharCode(222), 'g'), replaceVal: '&THORN;'},
        {regex: new RegExp(String.fromCharCode(223), 'g'), replaceVal: '&szlig;'},
        {regex: new RegExp(String.fromCharCode(8205), 'g'), replaceVal: ''},
        {regex: new RegExp(String.fromCharCode(8220), 'g'), replaceVal: '"'},
        {regex: new RegExp(String.fromCharCode(8221), 'g'), replaceVal: '"'},
        {regex: new RegExp(String.fromCharCode(8216), 'g'), replaceVal: "'"},
        {regex: new RegExp(String.fromCharCode(8217), 'g'), replaceVal: "'"},
        {regex: new RegExp(String.fromCharCode(8211), 'g'), replaceVal: '-'},
        {regex: new RegExp(String.fromCharCode(8212), 'g'), replaceVal: '--'},
        {regex: new RegExp(String.fromCharCode(8230), 'g'), replaceVal: '&hellip;'}
    ],

    initEditor: function () {
        this.mostRecentEventType = '';
        RS.common.HtmlLintEditor.superclass.initEditor.call(this);
        this.textareaEl.dom.setAttribute("contenteditable", "true");
        var doc = this.getDoc();
        var fn = Ext.Function.bind(this.onEditorEvent, this);
        Ext.EventManager.on(doc, {
            blur: fn,
            focusout: fn
        });
        var me = this;

        if (this.enableUpload) {
            this.getToolbar().add({
                iconCls: 'x-import-worddoc',
                tooltip: RS.common.locale.uploadWordDocument,
                hidden: this._vm.rootVM.sir,
                handler: function() {
					/*
                    Ext.create('Ext.window.Window', {
                        title: RS.common.locale.uploadAWordDocument,
                        modal: true,
                        layout: 'fit',
                        items: {
                            xtype: 'rsfileupload',
                            callback: me.appendHtmlValueAfterUpload,
                            url: '/resolve/service/wiki/docx/uploadDocx',
                            autoUpload: true,
                            multipart_params: {
                            wikiID: me._vm.rootVM.id,
                            wikiName: me._vm.rootVM.name,
                            userName: clientVM.username
                            },
                            filters: {
                                mime_types: [{
                                    title: RS.common.locale.uploadWordDocument,
                                    extensions: 'docx'
                                }],
                                special_chars_not_allowed: "&|\\*|\\^|~|'"   // set of chars separated by '|' if more than one. For example '&|#|@'
                            },
                            view: me
                        }
                    }).show();
					*/
					this._vm.open({
						mtype: 'RS.common.UploadWordDoc',
						editor: this
					});
                }.bind(this)
            });
        }
    },

    appendHtmlValueAfterUpload: function(response) {
        var htmlContents = response.data.docxContent.htmlContent;

        for (var i=0; i<htmlContents.length; i++) {
            this.setValue(this.getValue() + htmlContents[i]);
        }
    },

    fontFamilies: ['Arial','Calibri','Cambria','Consolas','Courier New','Georgia','Open Sans','Tahoma','Times New Roman','Ubuntu','Verdana'],

    initDefaultFont: function() {
        var font;

        if (!this.defaultFont) {
            font = this.textareaEl.getStyle('font-family');
            font = Ext.String.capitalize(font.split(',')[0]);
            this.defaultFont = font;
        } else {
            font = Ext.String.capitalize(this.defaultFont);
        }

        var fonts = Ext.Array.clone(this.fontFamilies);
        Ext.Array.include(fonts, font);
        fonts.sort();
        var select = this.down('#fontSelect').selectEl.dom;

        for (var i = 0; i < fonts.length; i++) {
            var f = fonts[i];
            var lower = f.toLowerCase();
            var option = new Option(f, lower);

            if (f === font) {
                selIdx = i;
            }

            option.style.fontFamily = lower;

            if (Ext.isIE) {
                select.add(option);
            } else {
                select.options.add(option);
            }
        }

        // Old IE versions have a problem if we set the selected property
        // in the loop, so set it after.
        select.options[selIdx].selected = true;
        this.textareaEl.setStyle('font-family', font);
    },

    getDocMarkup: function () {
        var h = this.iframeEl.getHeight() - this.iframePad * 2, oldIE = Ext.isIE8m;
        var markup = '';

        if (oldIE) {
            markup += '<!DOCTYPE html>';
        }

        markup += '<html><head>';
        markup += '<link rel="stylesheet" type="text/css" href="/resolve/css/resources/ResolveTheme-all.css">';
        markup += '<style type="text/css">';

        if (Ext.isOpera || Ext.isIE) {
            markup += 'p {margin:0;}';
        }

        markup += 'body {border:0;margin:0;padding:';
        markup += this.iframePad;
        markup += 'px;direction:';

        if(this.rtl) {
            markup += 'rtl;';
        } else {
            markup += 'ltr;';
        }

        if (!oldIE) {
            markup += 'min-';
        }

        markup += 'height:';
        markup += h;
        markup += 'px;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:text;background-color:white;}</style></head><body class="wiki-builder"></body></html>';
        return markup;
    },

    onEditorEvent: function (e) {
        this.mostRecentEventType = e.type;

        if(e.type !== 'keyup') {
            // override the default htmleditor behavior.
            // We don't want to sync on keypress because we're running regular expressions to clean the html.
            // if we did this every keypress then the editor would be unresponsive.
            // this.readImages(e.browserEvent);
            RS.common.HtmlLintEditor.superclass.onEditorEvent.call(this, e);
        }
    },

    readImages: function (e) {
        // sadly this code does not seem to work. Sigh.
        var images = [];

        if(e.type === 'paste') {
            e = e.originalEvent || e;
            var clipboardData = e.clipboardData || window.clipboardData;

            if(clipboardData) {
                var items = clipboardData.items;

                if(items) {
                    //console.log(JSON.stringify(items));

                    for (var i = 0; i < items.length; i++) {
                        if (items[i].type.indexOf("image") !== -1) {
                            var blob = items[i].getAsFile();
                            var urlFactory = window.URL || window.webkitURL;
                            var source = urlFactory.createObjectURL(blob);
                            var img = new Image();
                            img.onload = function() {};
                            img.src = source;
                        }
                    }
                }
            }
        }

        return images;
    },

    syncValue: function () {
        if (this.changeFired) {
            this.changeFired = false;
            return;
        }

        if (this.initialized) {
            var body = this.getEditorBody();
            var cloneBody = body.cloneNode(true);
            var doc = this.getDoc();

            if (typeof doc.createRange === 'function' && this.mostRecentEventType === 'paste') {
                var nodes = this.categorizeProblematicNodes(cloneBody);
                this.replaceElementsWithSpan(doc, nodes.spans);
                this.removeShapes(doc, nodes.shapes);
                this.replaceImagesWithPlaceholders(doc, nodes.images);
            }

            var changed = this.mostRecentEventType === 'paste' || this.mostRecentEventType === 'keyup';
            var agent = navigator.userAgent.toLowerCase();
            var isFirefox = agent.indexOf('firefox') > -1;
            var isIE = agent.indexOf('msie') > -1 || agent.indexOf('trident') > -1;
            var html = '';

            if (isFirefox) {
                html = this.nodeToHtml(doc, cloneBody);
            } else if (isIE) {
                html = this.ieInnerHTML(cloneBody, false);
            } else {
                html = cloneBody.innerHTML;
            }

            if(this.purgeTagsIfEmpty(isFirefox, cloneBody)) {
                html = "";
                changed = true;
            } else if (this.mostRecentEventType === 'paste' || changed) {
                html = this.cleanHtml(html);
                html = this.wrapMarkupWithWordStyling(cloneBody, html);
                this.mostRecentEventType = 'handled';
                body.innerHTML = html;
                setTimeout(this.setFocus.bind(this), 100);
            }

            var textElDom = this.textareaEl.dom;
            var old = textElDom.value;

            if(!changed) {
                changed = textElDom.value != html;
            }

            this.fireEvent('beforesync', this, html);

            if (changed) {
                textElDom.value = html;
            }

            if (changed) {
                this.fireEvent('sync', this, html);
                this.changeFired = true;
                this.lastValue = old;
                this.fireEvent('change', this, html, old);
                this.fireEvent('dirtychange', this, true);
                this.onDirtyChange(true);
                this.wasDirty = true;
            }
        }
    },

    wrapMarkupWithWordStyling: function (cloneBody, html) {
        var first = cloneBody.childNodes[0];

        if (first && first.nodeType === Node.ELEMENT_NODE) {
            if(!first.nodeName.toLowerCase() === 'div' || first.getAttribute('class') !== 'word-styling') {
                html = '<div class="word-styling">' + html + '</div>';
            }
        }

        return html;
    },

    nodeToHtml: function (doc, root) {
        var html = [];

        if (root.nodeType === Node.ELEMENT_NODE) {
            var children = Array.prototype.slice.call(root.childNodes);

            if (children.length === 1) {
                var candidate = children[0];

                if (candidate.nodeName.toLowerCase() === 'div') {
                    var css = candidate.getAttribute('class');

                    if(css === 'word-styling') {
                        children = Array.prototype.slice.call(candidate.childNodes);
                    }
                }
            }

            for(var i = 0; i < children.length; i++) {
                var div = doc.createElement('div');
                div.appendChild(children[i]);
                html.push(div.innerHTML);
            }
        }

        return html.join('');
    },

    ieInnerHTML: function (obj, convertToLowerCase) {
        var zz = obj.innerHTML ? String(obj.innerHTML) : ''
           ,z  = zz.match(/(<.+[^>])/g);

        if (z) {
         for ( var i=0;i<z.length;(i=i+1) ){
          var y
             ,zSaved = z[i]
             ,attrRE = /\=[a-zA-Z\.\:\[\]_\(\)\&\$\%#\@\!0-9\/]+[?\s+|?>]/g
          ;

          //z[i] = z[i]
		  //        .replace(/([<].+?\w+[>])/,
          //           function(a){return a.toLowerCase();
          //          });
          y = z[i].match(attrRE);

          if (y){
            var j   = 0
               ,len = y.length
            while(j<len){
              var replaceRE =
                   /(\=)([a-zA-Z\.\:\[\]_\(\)\&\$\%#\@\!0-9\/]+)?([\s+|?>])/g
                 ,replacer  = function(){
                      var args = Array.prototype.slice.call(arguments);
                      return '="'+(convertToLowerCase
                              ? args[2].toLowerCase()
                              : args[2])+'"'+args[3];
                    };
              z[i] = z[i].replace(y[j],y[j].replace(replaceRE,replacer));
              j+=1;
            }
           }
           zz = zz.replace(zSaved,z[i]);
         }
        }
        return zz.replace(/\\"/g, '"');
    },

    purgeTagsIfEmpty: function (isFirefox, root) {
        if (!isFirefox && typeof root.textContent !== 'undefined') {
            var text = root.textContent.trim();

            if (text.length === 0 && !this.containsImageTags(root)) {
                html = "";
                changed = true;
            }
        }
    },

    containsImageTags: function (root) {
        var containsImageTags = false;
        var workingQueue = [];

        if (root.nodeType === Node.ELEMENT_NODE) {
            workingQueue.push(root);

            while(workingQueue.length) {
                var node = workingQueue.pop();
                var children = Array.prototype.slice.call(node.childNodes);

                for(var i = 0; i < children.length; i++) {
                    if(children[i].nodeType === Node.ELEMENT_NODE) {
                        workingQueue.push(children[i]);
                    }
                }

                var nodeName = node.nodeName.toLowerCase();
                containsImageTags = nodeName === 'img';

                if(containsImageTags) {
                    workingQueue = [];
                }
            }
        }

        return containsImageTags;
    },

    categorizeProblematicNodes: function (root) {
        var workingQueue = [];
        var results = {
            spans: [],
            shapes: [],
            images: []
        };

        if (root.nodeType === Node.ELEMENT_NODE || root.nodeType === Node.COMMENT_NODE) {
            workingQueue.push(root);

            while(workingQueue.length) {
                var node = workingQueue.pop();
                var children = Array.prototype.slice.call(node.childNodes);

                for(var i = 0; i < children.length; i++) {
                    if(children[i].nodeType === Node.ELEMENT_NODE || children[i].nodeType === Node.COMMENT_NODE) {
                        workingQueue.push(children[i]);
                    }
                }

                var nodeName = node.nodeName.toLowerCase();

                if (nodeName === 'w:sdt'
                    || nodeName === 'w:sdtpr'
                    || nodeName === 'w:wrap'
                    || nodeName === 'v:group') {
                    results.spans.push(node);
                } else if (nodeName === 'img') {
                    var src = node.getAttribute('src');

                    if (src && src.startsWith('file:///')) {
                        results.images.push(node);
                    }
                } else if (nodeName === 'v:rect'
                    || nodeName === 'v:stroke'
                    || nodeName === 'v:oval'
                    || nodeName === 'v:imagedata'
                    || nodeName === 'v:shape'
                    || nodeName === 'v:shapetype'
                    || nodeName === 'v:path'
                    || nodeName === 'v:f'
                    || nodeName === 'v:fill'
                    || nodeName === '#comment') {
                    results.shapes.push(node);
                }
            }
        }

        return results;
    },

    replaceElementsWithSpan: function (doc, nodesToReplace) {
        for (var i = 0; i < nodesToReplace.length; i++) {
            var node = nodesToReplace[i];
            var span = doc.createElement('span');
            var style = node.getAttribute('style');

            if(style) {
                span.setAttribute('style', style);
            }

            while(node.firstChild) {
                span.appendChild(node.firstChild);
            }

            node.parentNode.replaceChild(span, node);
        }
    },

    removeShapes: function (doc, nodesToRemove) {
        for(var i = 0; i < nodesToRemove.length; i++) {
            var node = nodesToRemove[i];

            while (node.firstChild) {
                node.parentNode.insertBefore(node.firstChild, node);
            }

            node.parentNode.removeChild(node);
        }
    },

    replaceImagesWithPlaceholders: function (doc, images) {
        for(var i = 0; i < images.length; i++) {
            var image = images[i];
            var src = image.getAttribute('src');
            image.setAttribute('data-src', src);
            var style = image.getAttribute('style');
            var width = image.getAttribute('width');
            var height = image.getAttribute('height');

            if(style === null) {
                style = '';
            } else {
                style += ';';
            }

            style += 'border:1px solid #ccc';

            if (width) {
                style += ';width:';
                style += width;
                style += 'px';
            }

            if (height) {
                style += ';height:';
                style += height;
                style += 'px';
            }

            var alt = image.getAttribute('alt');

            if(alt) {
                alt = alt.replace(/\[/g, '(');
                alt = alt.replace(/\]/g, ')');
                image.setAttribute('alt', alt);
            }

            image.setAttribute('style', style);
            image.setAttribute('src', '');
            image.removeAttribute('width');
            image.removeAttribute('height');
            image.removeAttribute('border');
            image.removeAttribute('v:shapes');
        }
    },

    cleanHtml: function(html) {
        html = html.replace(/\\/g, '&#92;');
        var toolbar = this.getToolbar();
        var wordComponent = toolbar.getComponent('wordpaste');

        if(wordComponent && wordComponent.pressed) {
            html = this.convertLists(html);

            for (var i = 0; i < this.dirtyHtmlTags.length; i++) {
                html = html.replace(this.dirtyHtmlTags[i].regex, this.dirtyHtmlTags[i].replaceVal);
            }

            html = html.replace(this.regPoint, function (outerMatch, match, p1, p2) {
                var pointSize = +p1 + (+p2 / 10.0);
                var pxSize = Math.ceil(pointSize * 4 / 3);
                return pxSize +'px';
            });

            html = html.replace(this.regBorder, function (match, p1) {
                return p1 + 'margin-bottom:10px;'
            });
        }

        return html;
    },

    setFocus: function () {
        this.iframeEl.dom.contentDocument.body.focus();
    },

    convertLists: function (html) {
        var priorLastIndex = 0;
        var currentNesting = 0;
        var priorNesting = 0;
        var markup = [];
        var nestingStack = [];
        var closingTagStack = [];
        var m;

        while (m = this.regListComponents.exec(html)) {
            if (m.index - 3 > priorLastIndex) {
                this.closeLists(markup, nestingStack, closingTagStack);
                priorNesting = -1;
                nestingStack = [];
                var nonListContent = html.substring(priorLastIndex, m.index);
                markup.push(nonListContent);
            }

            currentNesting = this.currentNesting(nestingStack, currentNesting, priorNesting, m[1]);

            if (currentNesting > priorNesting || nestingStack.length === 0) {
                priorNesting = currentNesting;
                nestingStack.push(currentNesting);
                var div = this.getDoc().createElement('div');
                div.innerHTML = m[2] || m[3];
                var bulletText = div.textContent;
                this.openList(this.regListBullet.test(bulletText), markup, closingTagStack);
            } else {
                while (currentNesting < priorNesting) {
                    priorNesting = this.closeList(markup, nestingStack, closingTagStack);
                }
            }

            this.addListItem(markup, m[4]);
            priorLastIndex = m.index + m[0].length;
        }

        if (closingTagStack.length > 0) {
            this.closeLists(markup, nestingStack, closingTagStack);

            if(markup.length === 0) {
                priorLastIndex++;
            }

            markup.push(html.substring(priorLastIndex));
            html = markup.join('');
        }

        return html;
    },

    currentNesting: function (nestingStack, currentNesting, priorNesting, str) {
        var n = currentNesting;

        if (str) {
            var m = this.regListExplicitIndentation.exec(str);
            this.regListExplicitIndentation.lastIndex = 0;

            if (m !== null && m.length === 3) {
                n = 0;

                if(m[1]) {
                    n += +m[1];
                }

                if(m[2]) {
                    n += +m[2];
                }
            } else {
                m = this.regListRelativeIndentation.exec(str);
                this.regListRelativeIndentation.lastIndex = 0;

                if(m !== null & m.length === 4) {
                    if(m[1]) {
                        n += +m[1];
                    }

                    if(m[2]) {
                        n += +m[2];
                    }

                    if(m[3]) {
                        n += +m[3];
                    }
                }
            }
        }

        return n;
    },

    openList: function (isBullet, markup, closingTagStack) {
        var openingTag, closingTag;

        if (isBullet) {
            openingTag = '<ul>';
            closingTag = '</ul>';
        } else {
            openingTag = '<ol>';
            closingTag = '</ol>';
        }

        markup.push(openingTag);
        closingTagStack.push(closingTag);
    },

    addListItem: function (markup, content) {
        markup.push('<li>');
        markup.push(content);
        markup.push('</li>');
    },

    closeList: function (markup, nestingStack, closingTagStack) {
        markup.push(closingTagStack.pop());
        nestingStack.pop();
        return nestingStack[nestingStack.length - 1];
    },

    closeLists: function (markup, nestingStack, closingTagStack) {
        nestingStack = [];

        while(closingTagStack.length) {
            markup.push(closingTagStack.pop());
        }
    }
});

var addLoadEvents = [];

function addLoadEvent(func) {
	addLoadEvents.push(func)
}
Ext.define('RS.common.ViewFilterManagementWindow', {
	extend: 'Ext.window.Window',
	alias: 'widget.viewfiltermanagementwindow',

	height: 400,
	width: 600,
	title: RS.common.locale.manage,
	border: false,
	modal: true,
	buttonAlign: 'left',
	layout: 'fit',

	initComponent: function() {
		var viewGrid = this.getGridConfig('View'),
			filterGrid = this.getGridConfig('Filter');

		viewGrid.style = 'padding-bottom:5px';
		this.items = {
			xtype: 'tabpanel',
			items: [filterGrid, viewGrid]
		};


		this.buttons = [{
			text: RS.common.locale.close,
			handler: function(button) {
				button.up('window').close();
			}
		}];

		this.callParent(arguments);
	},

	getGridConfig: function(obj) {
		return {
			xtype: 'grid',
			border: false,
			title: RS.common.locale[obj.toLowerCase() + 'Title'],
			selModel: {
				xtype: 'rowmodel',
				mode: 'MULTI'
			},
			flex: 1,
			tbar: [{
				text: RS.common.locale.deleteText,
				itemId: 'deleteButton',
				disabled: true,
				type: obj,
				scope: this,
				handler: function(button) {
					var grid = button.up('grid'),
						selections = grid.getSelectionModel().getSelection();

					Ext.MessageBox.show({
						title: RS.common.locale.deleteTitle,
						msg: Ext.String.format(RS.common.locale[Ext.String.format(selections.length === 1 ? 'delete{0}Message' : 'delete{0}sMessage', obj)], selections.length),
						buttons: Ext.MessageBox.YESNO,
						buttonText: {
							yes: RS.common.locale.deleteText,
							no: RS.common.locale.cancel
						},
						scope: button,
						fn: this.handleDelete
					});
				}
			}],
			columns: [{
				text: 'Name',
				dataIndex: 'name',
				flex: 1
			}],
			store: Ext.create('Ext.data.Store', {
				autoLoad: true,
				model: Ext.String.format('RS.common.model.{0}', obj),
				proxy: {
					type: 'ajax',
					url: Ext.String.format('/resolve/service/{0}/list', obj.toLowerCase()),
					extraParams: {
						table: this.tableName
					},
					reader: {
						type: 'json',
						root: 'records'
					}
				}
			}),
			listeners: {
				selectionchange: function(selectionModel, selected, eOpts) {
					if (selected.length > 0) this.down('#deleteButton').enable();
					else this.down('#deleteButton').disable();
				}
			}
		}
	},

	handleDelete: function(btn) {
		if (btn == 'yes') {
			var grid = this.up('grid'),
				selections = grid.getSelectionModel().getSelection(),
				ids = '';

			Ext.each(selections, function(selection) {
				if (ids.length > 0) ids += ',';
				ids += selection.internalId;
			});

			Ext.Ajax.request({
				url: Ext.String.format('/resolve/service/{0}/delete', this.type.toLowerCase()),
				params: {
					sysIds: ids
				},
				scope: grid,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) this.store.load();
					else {
						clientVM.displayError(response.message,0,RS.common.locale.error);
					} 
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			});
		}
	}
});
