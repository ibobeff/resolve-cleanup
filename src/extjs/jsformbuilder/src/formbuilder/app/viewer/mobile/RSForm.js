/**
 * @author ryan.smith
 */
Ext.define('RS.formbuilder.viewer.mobile.RSForm', {
	choiceSeparator: '|&|',
	choiceValueSeparator: '|=|',

	extend: 'Ext.Panel',
	alias: 'widget.rsform_mobile',

	fullscreen: true,
	border: false,

	fieldConfiguration: [],
	submitConfiguration: {
		viewName: '',
		formSysId: '',
		tableName: '',
		userId: '',
		timezone: '',
		crudAction: '',
		query: '',
		controlItemSysId: '',
		dbRowData: {},
		sourceRowData: {}
	},

	/**
	 * Id of the form to use when looking up the controls from the server
	 */
	formId: '',

	/**
	 * Name of the form to use when looking up the controls from the server
	 */
	formName: '',

	/**
	 * The record to load into the grid once the form has actually been rendered (or empty for a new record)
	 */
	recordId: '',

	initialize: function() {
		//convert all the items configurations to proper configurations
		var configuration = this.formConfiguration;
		if (configuration) this.convertConfig(configuration);
		else Ext.Ajax.request({
			url: '/resolve/service/form/getform',
			params: {
				formname: this.initialConfig.formName,
				formsysid: this.initialConfig.formId,
				recordid: this.initialConfig.recordId
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					if (response.data) this.convertConfig(response.data);
					else clientVM.displayError('Form not found');
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		});
		this.callParent();
	},

	convertConfig: function(config) {
		var form = {},
			configuration;
		var buttonPanel = config.buttonPanel.controls;

		Ext.apply(this.submitConfiguration, {
			viewName: config.viewName,
			formSysId: config.id,
			tableName: config.tableName,
			userId: ''
		});

		//look in temp for the real controls to display
		if (config.panels[0].tabs.length > 1) {
			var tabItems = [],
				tabs = config.panels[0].tabs;
			Ext.each(tabs, function(tab) {
				var controls = tab.columns[0].fields;
				Ext.each(controls, this.convertControl, this);
				tabItems.push({
					layout: 'anchor',
					title: tab.name,
					items: controls
				});
			}, this);
			configuration = {
				xtype: 'tabpanel',
				items: tabItems
			};
		} else {
			configuration = config.panels[0].tabs[0].columns[0].fields;
			this.fieldConfiguration = config.panels[0].tabs;

			//convert all the controls to appropriate extjs controls
			//TODO: Pass the userId to the user picker field to have an "assign to me" feature
			Ext.each(configuration, this.convertControl, this);
		}

		//convert the buttons
		Ext.each(buttonPanel, this.convertButton, this);
		var buttonToolbar = {
			xtype: 'toolbar',
			ui: 'light',
			docked: 'bottom',
			layout: {
				pack: 'left'
			},
			items: buttonPanel
		};
		configuration.push(buttonToolbar);

		Ext.apply(form, {
			// bodyPadding: 10,
			autoScroll: true,
			xtype: 'formpanel',
			title: config.displayName,
			items: configuration
		});

		//Showing a tab panel instead of just a form, so we need to correct some normal form styling
		if (config.panels[0].tabs.length > 1) {
			form.layout = 'fit';
			delete form.bodyPadding;
		}

		if (this.rendered) {
			this.add(form);
			this.processUrlVariables();
		} else this.items = form;
	},

	/**
	 * Converts an individual control provided by the server to the correct representation for extjs to display
	 * @param {Object} config
	 */
	convertControl: function(config) {
		config.label = config.displayName;
		config.value = config.value || config.defaultValue;

		config.required = config.mandatory;

		if (!config.width) delete config.width;
		if (!config.height) delete config.height;

		if (!config.width) config.anchor = '-20';

		switch (config.uiType) {
			case 'TextField':
				config.xtype = 'textfield';
				config.minLength = config.stringMinLength;
				config.maxLength = config.stringMaxLength;
				break;

			case 'TextArea':
				config.xtype = 'textareafield';
				config.minLength = config.stringMinLength;
				config.maxLength = config.stringMaxLength;
				break;

			case 'NumberTextField':
				config.xtype = 'numberfield';
				config.allowDecimals = false;
				config.minValue = config.integerMinValue;
				config.maxValue = config.integerMaxValue;
				break;

			case 'DecimalTextField':
				config.xtype = 'numberfield';
				config.minValue = config.decimalMinValue;
				config.maxValue = config.decimalMaxValue;
				break;

			case 'ComboBox':
				config.xtype = 'combobox';
				config.displayField = 'name';
				config.valueField = 'value';
				config.value = this.parseValue(config.value).join('');
				config.store = Ext.create('Ext.data.Store', {
					data: this.parseOptions(config.selectValues),
					fields: [{
						name: 'name'
					}, {
						name: 'value'
					}]
				});
				break;
			case 'MultipleCheckBox':
				config.xtype = 'checkboxgroup';
				config.columns = 1;
				config.items = this.parseOptions(config.checkboxValues, config.value);
				Ext.each(config.items, function(item) {
					Ext.apply(item, {
						name: config.name
					});
				});
				delete config.value;
				break;

			case 'RadioButton':
				config.xtype = 'radiogroup';
				config.columns = 1;
				config.items = this.parseOptions(config.choiceValues, config.value);
				Ext.each(config.items, function(item) {
					Ext.apply(item, {
						name: config.name
					});
				});
				delete config.value;
				break;

			case 'Date':
				config.xtype = 'xdatefield';
				break;

			case 'Timestamp':
				config.xtype = 'xtimefield';
				break;

			case 'DateTime':
				config.xtype = 'xdatetimefield';
				break;

			case 'Link':
				config.xtype = 'linkfield';
				break;

			case 'CheckBox':
				config.xtype = 'checkbox';
				config.checked = config.value;
				config.inputValue = 'true';
				break;

			case 'Password':
				config.xtype = 'textfield';
				config.inputType = 'password';
				break;

			case 'TagField':
				config.xtype = 'tagfield';
				break;

			case 'Sequence':
				config.xtype = 'displayfield';
				break;

			case 'List':
				config.xtype = 'listfield';
				config.parseOptions = this.parseOptions;
				break;

			case 'ButtonField':
				//deal with actions
				config.xtype = 'button';
				delete config.anchor;
				config.text = config.displayName;
				config.handler = this.handleButtonClick;
				config.scope = this;
				break;

			case 'Reference':
				config.xtype = 'textfield';
				//TODO: might need to be a custom control
				break;

			case 'Journal':
				config.xtype = 'journalfield';
				break;

			case 'ReadOnlyTextField':
				config.xtype = 'displayfield';
				break;

			case 'UserPickerField':
				config.xtype = 'userpickerfield';
				break;

			case 'TeamPickerField':
				config.xtype = 'teampickerfield';
				break;

			case 'EmailField':
				config.xtype = 'textfield';
				config.regex = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
				config.invalidText = 'The value in this field is invalid.  It must be an email address like: someone@example.com';
				break;
			
			case 'MACAddressField':
				config.xtype = 'textfield';
				config.regex = /^[0-9a-f]{1,2}([\.:-])(?:[0-9a-f]{1,2}\1){4}[0-9a-f]{1,2}$/;
				config.invalidText = 'The value in this field is invalid.  It must be an MAC address like: 00-11-22-33-44-55';
				break;

			case 'IPAddressField':
				config.xtype = 'textfield';
				config.regex = /^(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))$/;
				config.invalidText = 'The value in this field is invalid.  It must be an IP Address like: 192.168.0.1';
				break;

			case 'CIDRAddressField':
				config.xtype = 'textfield';
				config.regex = /^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2]))?$/;
				config.invalidText = 'The value in this field is invalid.  It must be an CIDR Address like: 192.168.0.1/14';
				break;

			case 'PhoneField':
				config.xtype = 'textfield';
				//config.plugins = [new RS.formbuilder.viewer.desktop.InputTextMask('(999) 999 - 9999')];
				break;

			case 'NameField':
				config.xtype = 'namefield';
				break;

			case 'AddressField':
				// config.xtype = 'addressfield';
				config.xtype = 'textfield';
				break;

			case 'HiddenField':
				config.xtype = 'hiddenfield';
				config.value = config.hiddenValue;
				break;

			default:
				// debugger;
				break;
		}
	},

	/**
	 * Converts a server configuration of a button into a button that can be rendered by extjs
	 * @param {Object} button
	 */
	convertButton: function(button) {
		button.text = button.displayName || "Fix Me!";
		button.handler = this.handleButtonClick;
		button.sysId = button.id;
		button.scope = this;
		button.xtype = 'button';
		//determine crud action for db type actions
		button.crudAction = this.getCrudAction(button);
	},
	getCrudAction: function(button) {
		var i, actions = button.actions,
			len = actions.length,
			action;
		for (i = 0; i < len; i++) {
			action = actions[i];
			if (action.operation == 'DB') return action.crudAction;
		}
	},
	/**
	 * Parses values out of a select or choice control
	 * @param {Object} configValue
	 */
	parseValue: function(configValue) {
		var returnValue = [],
			configValue = configValue || '';
		var values = configValue.split(this.choiceSeparator);
		Ext.each(values, function(value) {
			var name = '';
			var val = value.split(this.choiceValueSeparator);
			returnValue.push(val[1] == 'true' ? val[0] : val[1]);
		}, this);
		return returnValue;
	},
	/**
	 * Parses options out of a select or choice control
	 * @param {Object} optionString
	 * @param {Object} valueString
	 */
	parseOptions: function(optionString, valueString) {
		var options = [],
			optionString = optionString || '';
		var split = optionString.split(this.choiceSeparator);
		Ext.each(split, function(option) {
			var nameValue = option.split(this.choiceValueSeparator);
			if (nameValue[0]) options.push({
				boxLabel: nameValue[0],
				name: nameValue[0],
				inputValue: nameValue[0],
				value: nameValue[1] && (nameValue[1] != 'true' || nameValue[1] != 'false') ? nameValue[1] : nameValue[0]
			})
		}, this);

		if (valueString) {
			var values = valueString.split(this.choiceSeparator),
				i, j;
			for (i = 0; i < values.length; i++) {
				var val = values[i].split(this.choiceValueSeparator);
				if (val[1] == 'true') {
					for (j = 0; j < options.length; j++) {
						if (options[j].boxLabel == val[0]) options[j].checked = true;
					}
				}
			}
		}
		return options;
	},
	/**
	 * Handles the button clicks to actions relationship and will perform the appropriate posting of the values to be processed by the server.
	 * Deals with reset and close actions client side if everything goes well on the server side.
	 * @param {Object} button
	 */
	handleButtonClick: function(button) {
		//Change button to show "working" state
		button.disable();
		button.setIcon('/resolve/images/loading.gif');

		//Handle actions
		var shouldReset = false,
			shouldClose = false,
			submitToServer = false;
		Ext.each(button.actions, function(action) {
			switch (action.operation.toLowerCase()) {
				case 'reset':
					shouldReset = true;
					break;
				case 'close':
					shouldClose = true;
					break;
				case 'db':
				case 'event':
				case 'runbook':
				case 'script':
					submitToServer = true;
					break;
				default:
					Ext.Msg.alert('Unknown action', 'The form you have loaded has an action that is unknown to this viewer.')
					break;
			}
		});
		if (submitToServer) {
			var params = button.up('form').getForm().getValues(),
				serverParams = this.submitConfiguration,
				key;

			//The server needs to know the id of the button that was clicked to know which actions to perform
			serverParams.controlItemSysId = button.sysId;
			serverParams.crudAction = button.crudAction;

			//Prepare the params for form submission
			var dbData = {},
				inputData = {};
			for (key in params) {
				if (this.getParamType(key) === 'DB') dbData[key] = Ext.isArray(params[key]) ? this.serializeChoice(params[key]) : params[key]
				else inputData[key] = Ext.isArray(params[key]) ? this.serializeChoice(params[key]) : params[key];
			}
			serverParams.dbRowData.data = dbData;
			serverParams.sourceRowData.data = inputData;

			Ext.Ajax.request({
				url: '/resolve/service/form/submit',
				jsonData: serverParams,
				scope: this,
				success: function(r) {
					button.enable();
					button.setIcon('');
					var response = RS.common.parsePayload(r);
					if (response.success) {
						if (shouldReset) {
							this.getForm().reset();
						}
						//display success message
						clientVM.displaySuccess('Form saved successfully', null, 'Success');						

						if (shouldClose) {
							var win = this.up('window') || window;
							Ext.defer(function() {
								win.close();
							}, 3000);
						}
						this.fireEvent('actioncompleted', this, button);
					} else {
						//display error message
						RS.UserMessage.msg({
							success: false,
							title: 'Failure',
							msg: response.message
						});
					}
				},
				failure: function(r) {
					button.enable();
					button.setIcon('');
					//Ext.Msg.alert('Error', 'An unknown error has occured');
					clientVM.displayFailure(r);
				}
			});
		} else {
			button.enable();
			button.setIcon('');
			if (shouldReset) this.getForm().reset();
			if (shouldClose) {
				var win = button.up('window') || window;
				win.close();
			}
		}
	},
	serializeChoice: function(paramsArray) {
		var params = [];
		Ext.each(paramsArray, function(param) {
			params.push(param + this.choiceValueSeparator + 'true');
		}, this);

		return params.join(this.choiceSeparator);
	},
	getParamType: function(param) {
		var i, j, tab, column, field;
		for (i = 0; i < this.fieldConfiguration.length; i++) {
			tab = this.fieldConfiguration[i];
			for (j = 0; j < tab.columns.length; j++) {
				column = tab.columns[i];
				for (f = 0; f < column.fields.length; f++) {
					field = column.fields[f];
					if (field.name == param) return field.sourceType;
				}
			}
		}
		return 'INPUT';
	},
	setRecordId: function(recordId) {
		this.recordId = recordId;
		Ext.Ajax.request({
			url: '/resolve/service/form/getRecordData',
			params: {
				formId: this.id,
				recordId: this.recordId
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					// debugger;
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(r){
				clientVM.displayFailure(r);
			}
		});
	},
	processUrlVariables: function() {
		var search = window.location.search;
		if (search) {
			var params = Ext.Object.fromQueryString(search.substring(1));
			this.down('form').getForm().setValues(params);
		}
	}
});