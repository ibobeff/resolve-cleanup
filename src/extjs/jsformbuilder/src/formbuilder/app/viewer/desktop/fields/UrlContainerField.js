/**
 *
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.UrlContainerField', {
	extend: 'Ext.Panel',
	alias: 'widget.urlcontainerfield',

	initComponent: function() {
		var me = this;
		me.html = '&nbsp;';
		me.callParent();
		me.on('afterrender', function() {
			me.update(Ext.String.format('<iframe class="rs_iframe" src="{0}" height="100%" style="width:100%;border:0" frameborder="0"/>', me.referenceTableUrl));
		})
	}
});