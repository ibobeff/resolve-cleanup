/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.ListField', {
	extend: 'Ext.container.Container',
	alias: 'widget.listfield',

	layout: 'fit',
	border: false,
	choiceSeparator: '|&|',
	choiceValueSeparator: '|=|',

	initComponent: function() {
		var me = this;

		if (!Ext.isArray(this.items)) this.items = [{
			xtype: 'hidden',
			name: this.name
		}];

		this.items = [{
			xtype: 'grid',
			forceFit: true,
			title: this.fieldLabel,
			disableSelection: this.readOnly,
			displayField: 'name',
			valueField: 'value',
			store: Ext.create('Ext.data.Store', {
				data: this.parseOptions(this.choiceValues),
				fields: [{
					name: 'name'
				}, {
					name: 'value'
				}]
			}),
			plugins: [Ext.create('Ext.grid.plugin.CellEditing', {
				pluginId: 'cellEditor',
				listeners: {
					scope: this,
					edit: this.listChanged
				}
			})],
			hideHeaders: true,
			columns: [{
				dataIndex: 'name',
				editor: {}
			}],
			selModel: {
				mode: 'MULTI'
			},
			listeners: {
				selectionchange: {
					scope: this,
					fn: this.listChanged
				}
			},
			tbar: [{
				text: RS.formbuilder.locale.add,
				disabled: this.readOnly,
				scope: this,
				handler: function(button) {
					var grid = this.down('grid'),
						editor = grid.getPlugin('cellEditor');
					grid.store.add({
						name: 'NEW'
					});
					//Automatically start editing the newly added record
					if (editor) {
						editor.startEditByPosition({
							row: grid.store.getCount() - 1,
							column: 0
						});
					}
				}
			}, {
				text: RS.formbuilder.locale.remove,
				itemId: 'removeButton',
				disabled: true,
				scope: this,
				handler: function(button) {
					var grid = this.down('grid'),
						values = grid.getSelectionModel().getSelection(),
						i = 0;
					Ext.each(values, function(value) {
						grid.store.remove(value);
					});
				}
			}]
		}].concat(this.items)

		this.callParent()

		this.on('afterrender', function() {
			me.initializeValues()
		}, this, {
			single: true
		})

		var hidden = this.down('hiddenfield')
		if (hidden) {
			hidden.listSetValue = hidden.setValue
			hidden.setValue = function(value, internal) {
				me.value = value
				if (!internal) me.initializeValues()
				hidden.listSetValue(value)
			}
		}
	},
	initializeValues: function() {
		var values = this.parseValue(this.value || this.defaultValue || '');
		var grid = this.down('grid');
		if (grid) {
			var store = grid.store,
				sm = grid.getSelectionModel();
			sm.deselectAll()
			Ext.each(values, function(value) {
				var index = store.indexOf(store.findRecord('name', value, 0, false, false, true))
				if (index == -1) {
					store.add({
						name: value,
						value: value
					})
					sm.select(store.getCount() - 1, true)
				} else
					sm.select(index, true)
			}, this)
		}
	},
	listChanged: function(model, selected, eOpts) {
		if (!Ext.isArray(selected)) selected = this.getGrid().getSelectionModel().getSelection()
		if (selected.length > 0) {
			this.down('#removeButton').enable()
		} else {
			this.down('#removeButton').disable()
		}
		var selectedNames = [];
		Ext.Array.forEach(selected, function(select) {
			selectedNames.push(select.data.name);
		});

		this.down('hiddenfield').setValue(selectedNames.join(this.choiceSeparator), true)
	},
	getGrid: function() {
		return this.down('grid')
	},
	setFieldStyle: function(style) {
		var fields = this.query('field');
		Ext.each(fields, function(field) {
			if (field.setFieldStyle) field.setFieldStyle(style);
		});
	}
});