Ext.define('RS.formbuilder.viewer.desktop.fields.FormPickerField', {
	extend: 'Ext.form.field.Trigger',
	alias: 'widget.formpickerfield',

	trigger1Cls: 'x-form-search-trigger',
	trigger2Cls: 'rs-btn-edit',
	trigger3Cls: 'icon-plus',

	allowCreate: false,
	editable: false,

	initComponent: function() {
		var me = this;
		me.callParent()
		me.on('render', function() {
			if (!me.allowCreate)
				me.triggerEl.item(2).parent().setDisplayed('none')
			me.triggerEl.item(2).addCls('x-icon-over icon-large rs-icon')
			me.triggerEl.item(2).parent().setStyle({
				'padding-top': '3px'
			})
			if (me.getValue()) me.triggerEl.item(1).parent().setDisplayed('table-cell')
			else me.triggerEl.item(1).parent().setDisplayed('none')
			me.triggerEl.item(1).addCls('x-icon-over')
			me.inputEl.on('click', function() {
				me.fireEvent('choose')
			})
		})
	},

	setValue: function(value) {
		this.callParent(arguments)
		if (this.rendered) {
			if (value) this.triggerEl.item(1).parent().setDisplayed('table-cell')
			else this.triggerEl.item(1).parent().setDisplayed('none')
		}
	},

	onTrigger1Click: function() {
		this.fireEvent('choose', this)
	},

	onTrigger2Click: function() {
		this.fireEvent('edit', this)
	},

	onTrigger3Click: function() {
		this.fireEvent('create', this)
	}
})