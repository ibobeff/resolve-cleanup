/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.NameField', {
	extend: 'Ext.form.FieldContainer',
	alias: 'widget.namefield',

	layout: 'hbox',
	nameSeparator: '|&|',

	initComponent: function() {
		var fieldName = this.name,
			values = this.value ? this.value.split(this.nameSeparator) : [],
			showings = this.selectValues ? this.selectValues.split(this.nameSeparator) : [true, true, false, true],
			i = 0;
		for(; i < showings.length; i++) {
			if(Ext.isString(showings[i])) showings[i] = showings[i] === 'true'
		}

		if(!Ext.isArray(this.items)) this.items = [{
			xtype: 'hiddenfield',
			name: this.name
		}];
		this.fieldLabel =  Ext.String.htmlEncode(this.fieldLabel || '');
		this.items = [{
			xtype: 'textfield',
			flex: 1,
			hideLabel: true,
			allowBlank: !this.mandatory || this.readOnly,
			readOnly: this.readOnly,
			fieldCls: this.fieldCls,
			value: values[0] || '',
			hidden: showings[0],
			disabled: showings[0],
			listeners: {
				change: {
					scope: this,
					fn: this.nameChanged
				}
			}
		}, {
			xtype: 'textfield',
			flex: 1,
			hideLabel: true,
			readOnly: this.readOnly,
			fieldCls: this.fieldCls,
			allowBlank: !this.mandatory || this.readOnly,
			margins: '0 0 0 5',
			value: values[1] || '',
			hidden: showings[1],
			disabled: showings[1],
			listeners: {
				change: {
					scope: this,
					fn: this.nameChanged
				}
			}
		}, {
			xtype: 'textfield',
			width: 30,
			hideLabel: true,
			readOnly: this.readOnly,
			fieldCls: this.fieldCls,
			allowBlank: !this.mandatory || this.readOnly,
			margins: '0 0 0 5',
			value: values[2] || '',
			hidden: showings[2],
			disabled: showings[2],
			listeners: {
				change: {
					scope: this,
					fn: this.nameChanged
				}
			}
		}, {
			xtype: 'textfield',
			flex: 2,
			hideLabel: true,
			readOnly: this.readOnly,
			fieldCls: this.fieldCls,
			allowBlank: !this.mandatory || this.readOnly,
			margins: '0 0 0 5',
			value: values[3] || '',
			hidden: showings[3],
			disabled: showings[3],
			listeners: {
				change: {
					scope: this,
					fn: this.nameChanged
				}
			}
		}].concat(this.items);

		this.callParent();
	},
	nameChanged: function() {
		var name = [];
		this.items.each(function(part) {
			if(part.xtype !== 'hiddenfield') name.push(part.getValue());
		});
		this.items.getAt(4).setValue(name.join(this.nameSeparator));
	},
	setFieldStyle: function(style){
		var fields = this.query('field');
		Ext.each(fields, function(field){
			if( field.setFieldStyle)field.setFieldStyle(style);
		});
	}
});