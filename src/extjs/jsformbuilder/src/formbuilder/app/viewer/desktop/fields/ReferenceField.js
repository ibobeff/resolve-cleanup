/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.ReferenceField', {
	extend: 'Ext.form.field.Trigger',
	alias: 'widget.referencefield',

	initComponent: function() {
		this.editable = false;
		this.callParent();
	},
	onRender: function() {
		//call into the server to get information about the columns that are going to come back from a particular table
		Ext.Ajax.request({
			url: '/resolve/service/form/getcustomtablecolumns',
			params: {
				tableName: this.referenceTable
			},
			scope: this,
			success: this.processColumns,
			failure: function(r){
				clientVM.displayFailure(r);
			}
		});
		this.callParent(arguments);
		this.setShowClearTrigger(false);

		var name = this.name || this.el.dom.name;
		// this.el.dom.removeAttribute('name');
		this.hiddenField = this.el.insertSibling({
			tag: 'input',
			type: 'hidden',
			name: name
		});
		this.hiddenName = name;
		// otherwise field is not found by BasicForm::findField
		if (this.value) {
			this.setShowClearTrigger(true);
			Ext.Ajax.request({
				url: '/resolve/service/dbapi/getRecordData',
				params: {
					tableName: this.referenceTable,
					type: 'form',
					start: 0,
					filter: Ext.encode([{
						field: 'sys_id',
						type: 'auto',
						condition: 'equals',
						value: this.value
					}])
				},
				scope: this,
				success: this.processValue,
				failure: function(r){
					clientVM.displayFailure(r);
				}
			})
		}

		//setup tooltips
		Ext.create('Ext.tip.ToolTip', {
			target: this.triggerEl.item(0),
			html: RS.formbuilder.locale.refrenceClear
		});
		Ext.create('Ext.tip.ToolTip', {
			target: this.triggerEl.item(1),
			html: RS.formbuilder.locale.referenceJump
		});
		Ext.create('Ext.tip.ToolTip', {
			target: this.triggerEl.item(2),
			html: RS.formbuilder.locale.referenceSearch
		});

	},

	processColumns: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			if (Ext.isArray(response.records)) {
				this.gridColumns = [];
				this.storeFields = [];
				var storeType = '';
				Ext.each(response.records, function(record) {
					this.gridColumns.push({
						header: record.displayName,
						dataIndex: record.columnModelName,
						filter: true
					});

					switch (record.type) {
						case 'boolean':
							storeType = 'boolean';
							break;

						case 'timestamp':
							storeType = 'date';
							break;

						case 'integer':
						case 'long':
						case 'float':
						case 'double':
							storeType = 'numeric';
							break;

						default:
							storeType = 'string';
							break;
					}
					this.storeFields.push({
						name: record.columnModelName,
						type: storeType,
						dateFormat: 'c'
					});
				}, this);

				this.gridStore = Ext.create('Ext.data.Store', {
					fields: this.storeFields,
					remoteSort: true,
					remoteFilter: true,
					proxy: {
						type: 'ajax',
						url: '/resolve/service/dbapi/getRecordData',
						extraParams: {
							tableName: this.referenceTable,
							type: 'table'
						},
						reader: {
							type: 'json',
							root: 'records'
						},
						listeners: {
							exception: function(e, resp, op) {
								clientVM.displayExceptionError(e, resp, op);
							}
						}
					}
				});
				if (this.referenceTable) this.gridStore.load();
			}
			//go through the records and populate the grid columns and types for the grid store
		} else clientVM.displayError(response.message);
	},

	processValue: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			if (response.data) {
				this.selectReferenceRecord(response);
			}
		} else clientVM.displayError(response.message);
	},

	setValue: function(value, process) {
		this.callParent(arguments);
		if (value && process !== false) {
			this.setShowClearTrigger(true);
			Ext.Ajax.request({
				url: '/resolve/service/dbapi/getRecordData',
				params: {
					tableName: this.referenceTable,
					type: 'form',
					start: 0,
					filter: Ext.encode([{
						field: 'sys_id',
						type: 'auto',
						condition: 'equals',
						value: value
					}])
				},
				scope: this,
				success: this.processValue,
				failure: function(r){
					clientVM.displayFailure(r);
				}
			})
		}

	},

	//Search Trigger
	trigger3Cls: Ext.baseCSSPrefix + 'form-search-trigger',
	onTrigger3Click: function() {
		var me = this;
		//open search dialog window
		var selectionWindow = Ext.create('Ext.window.Window', {
			modal: true,
			width: 600,
			height: 400,
			layout: 'fit',
			title: RS.formbuilder.locale.SelectRecordTitle,
			items: [{
				xtype: 'grid',
				forceFit: false,
				columns: this.gridColumns,
				store: this.gridStore,
				bbar: Ext.create('Ext.PagingToolbar', {
					store: this.gridStore,
					displayInfo: true,
					displayMsg: 'Displaying records {0} - {1} of {2}',
					emptyMsg: "No records to display"
				}),
				listeners: {
					selectionchange: function(grid, selected, eOpts) {
						var selectButton = selectionWindow.down('#selectButton');
						if (selected.length == 1) selectButton.enable()
						else selectButton.disable()
					},
					itemdblclick: function(selectionModel, record, item, index, e, eOpts) {
						me.selectReferenceRecord(record)
						selectionWindow.close()
					}
				}
			}],
			buttonAlign: 'left',
			buttons: [{
				text: RS.formbuilder.locale.select,
				itemId: 'selectButton',
				disabled: true,
				handler: function(button) {
					var win = button.up('window'),
						grid = win.down('grid');
					me.selectReferenceRecord(grid.getSelectionModel().getSelection()[0]);
					win.close();
				}
			}, {
				text: RS.formbuilder.locale.cancel,
				handler: function(button) {
					button.up('window').close();
				}
			}]
		});

		selectionWindow.show();
	},

	selectReferenceRecord: function(record) {
		this.setValue(record.data[this.referenceDisplayColumn], false);
		this.hiddenField.dom.value = record.data.sys_id;
		this.setShowClearTrigger(true);
	},

	//Clear Trigger
	triggerCls: Ext.baseCSSPrefix + 'form-clear-trigger',
	onTriggerClick: function() {
		//clear the current value and hide the clear button
		this.setValue('');
		this.hiddenField.dom.value = '';
		this.setShowClearTrigger(false);
	},

	//Jump Trigger
	trigger2Cls: Ext.baseCSSPrefix + 'form-jump-trigger',
	onTrigger2Click: function() {
		window.open(Ext.String.format(this.referenceTarget, this.hiddenField.dom.value), '_blank');
	},

	setShowClearTrigger: function(display) {
		this.triggerEl.item(0).parent().setDisplayed(display ? 'table-cell' : 'none');
		if (this.referenceTarget) this.triggerEl.item(1).parent().setDisplayed(display ? 'table-cell' : 'none');
		else this.triggerEl.item(1).parent().setDisplayed('none');
		this.doComponentLayout();
	},

	getRawValue: function() {
		return this.rendered ? this.hiddenField.dom.value : '';
	}
});