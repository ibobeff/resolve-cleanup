/**
 *
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.TabContainerField', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.tabcontainerfield',

	layout: 'fit',

	anchor: '-20',
	height: 300,
	border: false,

	resizable: true,
	resizeHandles: 's',
	csrftoken: '',

	initComponent: function() {
		var me = this;

		if (this.referenceTableUrl != '') {
			var url = this.referenceTableUrl.split('?');
			clientVM.getCSRFToken_ForURI(url[0], function(token_pair) {
				this.csrftoken = token_pair[0] + '=' + token_pair[1];
			}.bind(this))
		}

		var displayNames = me.displayName ? me.displayName.split('|') : [],
			referenceTables = me.refGridReferenceByTable ? me.refGridReferenceByTable.split('|') : [],
			refGridReferenceColumnName = me.refGridReferenceColumnName ? me.refGridReferenceColumnName.split('|') : [],
			referenceColumns = me.refGridSelectedReferenceColumns ? me.refGridSelectedReferenceColumns.split('|') : [],
			allowReferenceTableAdd = me.allowReferenceTableAdd ? me.allowReferenceTableAdd.split('|') : [],
			allowReferenceTableRemove = me.allowReferenceTableRemove ? me.allowReferenceTableRemove.split('|') : [],
			allowReferenceTableView = me.allowReferenceTableView ? me.allowReferenceTableView.split('|') : [],
			referenceTableUrl = me.referenceTableUrl ? me.referenceTableUrl.split('|') : [],
			referenceTableViewPopup = me.refGridViewPopup ? me.refGridViewPopup.split('|') : [],
			referenceTableViewPopupWidth = me.refGridViewPopupWidth ? me.refGridViewPopupWidth.split('|') : [],
			referenceTableViewPopupHeight = me.refGridViewPopupHeight ? me.refGridViewPopupHeight.split('|') : [],
			referenceTableViewPopupTitle = me.refGridViewTitle ? me.refGridViewTitle.split('|') : [],
			len = displayNames.length,
			i, tabs = [];

		for (i = 0; i < len; i++) {
			var cols = referenceColumns[i] ? Ext.decode(referenceColumns[i]) : [];
			tabs.push({
				title: displayNames[i],
				referenceTable: referenceTables[i],
				referenceColumn: refGridReferenceColumnName[i],
				referenceColumns: cols,
				allowReferenceTableAdd: allowReferenceTableAdd[i] == 'true',
				allowReferenceTableRemove: allowReferenceTableRemove[i] == 'true',
				allowReferenceTableView: allowReferenceTableView[i] == 'true',
				referenceTableUrl: referenceTableUrl[i],
				referenceTableViewPopup: referenceTableViewPopup[i] == 'true',
				referenceTableViewPopupWidth: Number(referenceTableViewPopupWidth[i]),
				referenceTableViewPopupHeight: Number(referenceTableViewPopupHeight[i]),
				referenceTableViewPopupTitle: referenceTableViewPopupTitle[i]
			});
		}

		//Now render either the tabs or the one grid
		var config = {};
		if (len > 1) {
			config.xtype = 'tabpanel';
			config.plain = true;
			config.items = [];
			Ext.each(tabs, function(tab) {
				config.items.push(this.convertToGrid(tab));
			}, this);
		} else {
			config = this.convertToGrid(tabs[0]);
		}
		me.items = config;

		me.callParent();

		if (this.recordId) me.loadReferences();
	},
	convertToGrid: function(serverConf) {
		var me = this;

		if (serverConf.referenceTableViewPopupTitle == 'FileManager') {
			return {
				border: false,
				title: serverConf.title,
				xtype: 'uploadmanager',
				formName: me.formName,
				recordId: me.recordId,
				fileUploadTableName: me.fileUploadTableName,
				referenceColumnName: me.referenceColumnName,
				allowUpload: serverConf.allowReferenceTableAdd,
				allowRemove: serverConf.allowReferenceTableRemove
			};
		}
		if (serverConf.referenceTableViewPopupTitle == 'URL') {
			if (serverConf.referenceTableUrl && serverConf.referenceTableUrl.indexOf('/resolve/') === 0) {
    			const urlData = serverConf.referenceTableUrl.split('#');
    			const uriData = urlData[0].split('?');
    			clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
    				var csrftoken = '?' + data[0] + '=' + data[1];
    				var tokenizedData = uri + csrftoken;
    				if (uriData.length > 1) {
    					tokenizedData += '&' + uriData[1];
    				}
    				if (urlData.length > 1) {
    					tokenizedData += '#' + urlData[1];
    				}
    				serverConf.referenceTableUrl = tokenizedData;
    			});
    		}

			return {
				border: false,
				title: serverConf.title,
				html: '&nbsp;',
				listeners: {
					afterrender: function(panel) {
						panel.update(Ext.String.format('<iframe class="rs_iframe" src="{0}" height="100%" style="width:100%;border:0" frameborder="0"/>', serverConf.referenceTableUrl))
					}
				}
			}
		}


		var config = Ext.apply({}, serverConf, {
			xtype: 'grid'
		});
		config.columns = [{
			xtype: 'actioncolumn',
			hideable: false,
			hidden: !serverConf.allowReferenceTableView,
			width: 25,
			items: [{
				icon: '/resolve/images/link.gif',
				// Use a URL in the icon config
				tooltip: RS.formbuilder.locale.viewReference,
				scope: this,
				handler: function(grid, rowIndex, colIndex) {
					var rec = grid.getStore().getAt(rowIndex),
						sys_id = rec.get('sys_id'),
						url = serverConf.referenceTableUrl,
						me = this;
					if (sys_id) {
						//open new window to the url configured for this reference table
						var containsSysIdAlready = url.indexOf('${sys_id}') > -1;

						url = url.replace(/${sys_id}/g, sys_id).replace(/${parent_sys_id}/g, me.recordId);

						if (!containsSysIdAlready) {
							var split = url.split('?'),
								params = {};
							if (split.length > 1) {
								params = Ext.Object.fromQueryString(split[1]);
							}
							url = split[0] + '?' + Ext.urlEncode(Ext.applyIf({
								sys_id: sys_id
							}, params));
						}

						if (url.indexOf('/resolve/') === 0 && url.indexOf(clientVM.CSRFTOKEN_NAME) === -1) {
							const urlData = url.split('#');
							const uriData = urlData[0].split('?');
							clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
								var csrftoken = '?' + data[0] + '=' + data[1];
								var tokenizedData = uri + csrftoken;
								if (uriData.length > 1) {
									tokenizedData += '&' + uriData[1];
								}
								if (urlData.length > 1) {
									tokenizedData += '#' + urlData[1];
								}
								url = tokenizedData;
							});
						}

						if (!serverConf.referenceTableViewPopup) window.open(url);
						else {
							Ext.create('Ext.window.Window', {
								modal: true,
								border: false,
								autoScroll: false,
								title: serverConf.referenceTableViewPopupTitle || RS.formbuilder.locale.EditReference + serverConf.title,
								width: Number(serverConf.referenceTableViewPopupWidth) || 600,
								height: Number(serverConf.referenceTableViewPopupHeight) || 400,
								layout: 'fit',
								html: Ext.String.format('<iframe class="rs_iframe" src="{0}" height="100%" style="width:100%;border:0" frameborder="0"/>', url),
								listeners: {
									render: function(w) {
										w.getEl().select('iframe').on('load', function(e, t, eOpts) {
											var me = this;
											t.contentWindow.registerForm = function(form) {
												form.on('beforeclose', function() {
													Ext.defer(function() {
														this.close();
													}, 100, this);
													return false;
												}, me);
												form.on('beforeexit', function() {
													Ext.defer(function() {
														this.close();
													}, 100, this);
													return false;
												}, me);
												form.on('titleChanged', function(title) {
													this.setTitle(title);
													return false;
												}, me);
											}
										}, w);
									},
									close: function() {
										me.loadReferences();
									}
								}
							}).show();
						}
					}
				}
			}]
		}];

		//calculate columns to display
		var storeFields = ['sys_id'];
		Ext.each(serverConf.referenceColumns, function(displayColumn) {
			var type = 'string';
			switch (displayColumn.dbtype) {
				case 'timestamp':
					type = 'date';
					break;
				default:
					type = 'string';
					break;
			}
			var storeCol = {
				name: displayColumn.value,
				type: type
			};
			var col = {
				flex: 1,
				text: displayColumn.name,
				dataIndex: displayColumn.value
			};
			if (type == 'date') {
				storeCol.format = 'c';
				col.renderer = Ext.util.Format.dateRenderer('Y-m-d g:i:s A');
			}

			storeFields.push(storeCol);
			config.columns.push(col);
		});

		var field = serverConf.referenceColumn;
		if (serverConf.referenceColumn == 'u_content_request' && serverConf.referenceTable != 'content_request') {
			field += '.sys_id';
		}

		config.store = Ext.create('Ext.data.Store', {
			fields: storeFields,
			proxy: {
				type: 'ajax',
				url: '/resolve/service/dbapi/getRecordData',
				extraParams: {
					tableName: serverConf.referenceTable,
					type: 'table',
					start: 0,
					filter: Ext.encode([{
						field: field,
						type: 'auto',
						condition: 'equals',
						value: this.recordId || '__blank'
					}])
				},
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});

		// config.selModel = Ext.create('Ext.selection.CheckboxModel');
		var generatedId = Ext.id();
		config.dockedItems = [{
			xtype: 'toolbar',
			dock: 'top',
			name: 'actionBar',
			items: [{
				text: RS.formbuilder.locale.referenceTableNew,
				name: 'newReference',
				itemId: 'newButton',
				hidden: !serverConf.allowReferenceTableAdd,
				scope: this,
				handler: function(button) {
					var url = serverConf.referenceTableUrl ? serverConf.referenceTableUrl : '';

					if (url != '' && this.csrftoken != '') {
						var urlData = url.split('?');
						url = urlData[0] + '?' + this.csrftoken;
						if (urlData.length > 1) {
							url += '&' + urlData[1];
						}
					}

					url = url.replace(/\${parent_sys_id}/g, me.recordId);

					if (url.indexOf('/resolve/') === 0 && url.indexOf(clientVM.CSRFTOKEN_NAME) === -1) {
						const urlData = url.split('#');
						const uriData = urlData[0].split('?');
						clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
							var csrftoken = '?' + data[0] + '=' + data[1];
							var tokenizedData = uri + csrftoken;
							if (uriData.length > 1) {
								tokenizedData += '&' + uriData[1];
							}
							if (urlData.length > 1) {
								tokenizedData += '#' + urlData[1];
							}
							url = tokenizedData;
						});
					}

					if (!serverConf.referenceTableViewPopup && url) window.open(url);
					else {
						Ext.create('Ext.window.Window', {
							modal: true,
							border: false,
							title: serverConf.referenceTableViewPopupTitle || RS.formbuilder.locale.NewReference + serverConf.title,
							width: Number(serverConf.referenceTableViewPopupWidth) || 600,
							height: Number(serverConf.referenceTableViewPopupHeight) || 400,
							layout: 'fit',
							html: Ext.String.format('<iframe class="rs_iframe" src="{0}" height="100%" style="width:100%;border:0" frameborder="0"/>', url),
							listeners: {
								render: function(w) {
									w.getEl().select('iframe').on('load', function(e, t, eOpts) {
										var me = this;
										t.contentWindow.registerForm = function(form) {
											form.on('beforeclose', function() {
												Ext.defer(function() {
													this.close();
												}, 100, this);
												return false;
											}, me);
											form.on('beforeexit', function() {
												Ext.defer(function() {
													this.close();
												}, 100, this);
												return false;
											}, me);
										}
									}, w);
								},
								close: {
									scope: this,
									fn: function() {
										this.loadReferences();
									}
								}
							}
						}).show();
					}
				}
			}, {
				text: RS.formbuilder.locale.referenceTableDelete,
				disabled: true,
				name: 'removeReference',
				itemId: 'removeButton' + generatedId,
				hidden: !serverConf.allowReferenceTableRemove,
				scope: this,
				handler: function(button) {
					var grid = button.up('grid'),
						selections = grid.getSelectionModel().getSelection();
					Ext.MessageBox.show({
						title: RS.formbuilder.locale.confirmRemove,
						msg: selections.length === 1 ? RS.formbuilder.locale.confirmRemoveReference : Ext.String.format(RS.formbuilder.locale.confirmRemoveReferences, selections.length),
						buttons: Ext.MessageBox.YESNO,
						buttonText: {
							yes: RS.formbuilder.locale.deleteReference,
							no: RS.formbuilder.locale.cancel
						},
						scope: grid,
						fn: this.removeSelectedReferences
					});
				}
			}]
		}];

		config.plugins = [{
			ptype: 'pager'
		}];

		config.listeners = {
			scope: this,
			selectionchange: function(selectionModel, selections, eOpts) {
				if (selections.length > 0) this.down('#removeButton' + generatedId).enable();
				else this.down('#removeButton' + generatedId).disable();
			}
		};

		return config;
	},
	removeSelectedReferences: function(button) {
		if (button === 'yes') {
			var selections = this.getSelectionModel().getSelection(),
				ids = [];
			Ext.each(selections, function(selection) {
				ids.push(selection.get('sys_id'));
			});
			var panel = this.up('referencetable');
			Ext.Ajax.request({
				url: '/resolve/service/dbapi/delete',
				params: {
					tableName: this.referenceTable,
					whereClause: 'sys_id in (\'' + ids.join('\',\'') + '\')',
					type: 'form',
					useSql: true
				},
				scope: panel,
				success: panel.removeDeletedReferences,
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			});
		}
	},
	removeDeletedReferences: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			this.loadReferences();
		} else {
			clientVM.displayError(response.message);
		}
	},
	loadReferences: function() {
		var grids = this.query('grid');
		Ext.each(grids, function(grid) {
			grid.getStore().load();
		}, this);
	}
});