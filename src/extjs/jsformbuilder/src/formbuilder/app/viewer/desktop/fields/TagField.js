/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.TagField', {
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.tagfield',

	displayField: 'name',
	valueField: 'value',
	queryMode: 'local',
	multiSelect: true,

	initComponent: function() {
		this.store = Ext.create('Ext.data.Store', {
			fields: ['name', 'value'],
			proxy: {
				type: 'ajax',
				url: '/resolve/service/form/getTags',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});
		this.store.load({});

		this.callParent();
	}
});