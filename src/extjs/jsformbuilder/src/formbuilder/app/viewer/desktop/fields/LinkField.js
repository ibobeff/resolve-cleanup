/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.Link', {
	extend: 'Ext.form.Label',
	alias: 'widget.linkfield',

	padding: '10px 0px 10px 0px',
	style: {
		display: 'block'
	},

	initComponent: function() {
		this.callParent();
		this.setValue(this.value || this.defaultValue);

		var me = this;
		this.on('afterrender', function() {
			me.setDisabled(me.disabled)
		})
	},

	setValue: function(value) {
		var link = this.generateLink(value);
		this.setText(link, false)
	},

	generateLink: function(value) {
		//parse the link and return it
		if (!value) return '';

		//We have a document link and we need to look that information up
		Ext.Ajax.request({
			url: '/resolve/service/form/getLink',
			params: {
				wikiLink: value,
				target: this.linkTarget
			},
			scope: this,
			success: this.processServerResponse,
			failure: function(r){
				clientVM.displayFailure(r);
			}
		});
		return value;
	},

	processServerResponse: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			var link = response.data;
			if (this.linkTarget && link.indexOf('target') <= 0) {
				var hrefIndex = link.indexOf('href'),
					linkArray = link.split('');
				linkArray.splice(hrefIndex, 0, Ext.String.format(' target="{0}" ', this.linkTarget));
				link = linkArray.join('');
			}
			if (this.rendered) this.setText(link, false)
			else this.html = link
			// check HTML anchor href links
			var anchors = window.document.querySelectorAll('a');
			for (var i=0; i < anchors.length; i++) {
				var element = anchors[i];
				var link = element.getAttribute('href');
				if (link) {
					clientVM.injectCSRFPageTokenToElement(link, element, 'href');
				}
			}
			this.setDisabled(this.disabled)
		} else {
			clientVM.displayError(response.message);
		}
	},

	setDisabled: function(disabled) {
		this.disabled = disabled;
		if (!this.rendered) return;
		var links = this.getEl().query('a');
		Ext.each(links, function(link) {
			if (disabled) {
				this.oldLink = Ext.fly(link).dom.href;
				Ext.fly(link).dom.href = 'javascript:void(0)';
				Ext.fly(link).setStyle({
					color: 'grey'
				});
			} else {
				if (this.oldLink) Ext.fly(link).dom.href = this.oldLink;
				Ext.fly(link).setStyle({
					color: 'blue'
				});
			}
		}, this)
	}
});