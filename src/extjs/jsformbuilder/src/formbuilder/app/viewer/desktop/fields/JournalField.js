/**
 *
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.JournalField', {
	extend: 'Ext.form.FieldContainer',
	alias: 'widget.journalfield',

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	journalSeparator: '|&|',

	userId: '',
	journalEntries: [],

	allowJournalEdit: false,

	disabled: false,

	resizable: true,
	resizeHandles: 's',

	initComponent: function() {
		var me = this;
		me.value = me.value || [];
		if (Ext.isString(me.value)) me.value = Ext.decode(me.value);

		me.height = me.height || 300; //to ensure the grid is displayed
		me.journalEntries = me.value;

		me.journalStore = Ext.create('Ext.data.Store', {
			fields: ['userId', {
				name: 'createDate',
				type: 'date',
				format: 'c',
				dateFormat: 'c'
			}, 'note'],
			data: me.value,
			proxy: {
				type: 'memory',
				reader: {
					type: 'json'
				}
			}
		});

		var editor = Ext.create('Ext.grid.plugin.CellEditing', {});

		if (!Ext.isArray(me.items)) me.items = [{
			xtype: 'hiddenfield',
			name: me.name,
			getValue: function() {
				var temp = me.ownerCt.journalEntries.slice(0),  // me.ownerCt.journalEntries is immutable
					newVal = me.ownerCt.down('textarea').getValue();
				if (newVal) {
					temp.splice(0, 0, {
						userId: me.ownerCt.userId,
						createDate: Ext.Date.format(new Date(), 'c'),
						note: Ext.htmlEncode(newVal).replace(/\n/g, '<br/>')
					});
				}
				return Ext.encode(temp);
			}
		}];

		var journalPlugins = [];
		if (this.allowJournalEdit) journalPlugins.push(editor)

		me.items = me.items.concat([{
			xtype: 'textarea',
			readOnly: me.readOnly,
			hidden: me.readOnly
		}, {
			xtype: 'grid',
			flex: 1,
			plugins: journalPlugins,
			columns: [{
				header: 'Note',
				dataIndex: 'note',
				flex: 1,
				editor: {}
			}, {
				header: 'User',
				width: 100,
				dataIndex: 'userId'
			}, {
				header: 'Date',
				dataIndex: 'createDate',
				width: 200,
				renderer: Ext.util.Format.dateRenderer('D, M d, Y g:i:s A')
			}, {
				xtype: 'actioncolumn',
				hidden: !this.allowJournalEdit,
				hideable: false,
				width: 40,
				items: [{
					icon: '/resolve/images/edittsk_tsk.gif',
					// Use a URL in the icon config
					tooltip: 'Edit',
					handler: function(grid, rowIndex, colIndex) {
						if (journalPlugins.length > 0)
							editor.startEditByPosition({
								row: rowIndex,
								column: 0
							});
					}
				}, {
					icon: '/resolve/images/closex.gif',
					tooltip: 'Delete',
					scope: me,
					handler: function(grid, rowIndex, colIndex) {
						grid.lastRowIndex = rowIndex;
						Ext.MessageBox.show({
							title: 'Confirm Delete',
							msg: 'Are you sure you want to delete the selected journal entry?',
							buttons: Ext.MessageBox.YESNO,
							buttonText: {
								yes: 'Delete',
								no: 'Cancel'
							},
							scope: grid,
							fn: me.removeJournalEntry
						});
					}
				}]
			}],
			listeners: {
				edit: function() {
					me.storeJournalEntries(this);
				}
			},
			store: me.journalStore
		}]);

		me.callParent();
	},
	journalEntryChanged: function(field, newValue, oldValue, eOpts) {
		//serialize the current value for the field into the hidden field
		var temp = [{
			userId: this.userId,
			createDate: Ext.Date.format(new Date(), 'c'),
			note: Ext.htmlEncode(newValue).replace(/\n/g, '<br/>')
		}];
		temp = temp.concat(this.journalEntries);
		this.down('hiddenfield').setValue(Ext.encode(temp));
	},
	removeJournalEntry: function(btn) {
		if (btn === 'yes') {
			this.getStore().removeAt(this.lastRowIndex);
			this.up('fieldcontainer').storeJournalEntries(this);
		}
	},
	storeJournalEntries: function(grid) {
		var records = [];
		grid.getStore().each(function(record) {
			records.push({
				userId: record.get('userId'),
				createDate: record.get('createDate'),
				note: record.get('note')
			});
		});
		this.journalEntries = records;
		var textarea = this.down('textarea'),
			val = textarea.getValue();
		if (val) {
			this.journalEntryChanged(textarea, val);
		}
	},
	setValue: function(value) {
		if (value && Ext.isString(value)) {
			var journals = Ext.decode(value);
			if (Ext.isArray(journals)) {
				this.down('textareafield').setValue('');
				this.journalEntries = journals;
				this.journalStore.removeAll();
				this.journalStore.add(journals);
			}
		}
	},
	setFieldStyle: function(style) {
		var fields = this.query('field');
		Ext.each(fields, function(field) {
			if (field.setFieldStyle) field.setFieldStyle(style);
		});
	}
});