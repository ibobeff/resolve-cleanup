/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.TeamPicker', {
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.teampickerfield',

	displayField: 'name',
	valueField: 'value',
	queryMode: 'local',

	forceSelection: true,

	/**
	 * Pipe separated list of team id's to filter the users with.  For example 'id123|id456'
	 */
	teamsOfTeams: '',
	recurseThroughTeamsList: false,

	//Clear Trigger
	clearCls: Ext.baseCSSPrefix + 'form-clear-trigger',
	clearClick: function() {
		//clear the current value and hide the clear button
		this.setValue('');
		this.triggerEl.item(0).parent().setDisplayed('none');
	},

	initComponent: function() {
		this.trigger2Cls = this.triggerCls;
		this.onTrigger2Click = this.onTriggerClick;
		this.trigger1Cls = this.clearCls;
		this.onTrigger1Click = this.clearClick;

		this.store = Ext.create('Ext.data.Store', {
			fields: ['name', 'value', 'type'],
			proxy: {
				type: 'ajax',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});

		Ext.Ajax.request({
			url: '/resolve/service/form/getTeams',
			params: {
				teamPickerTeamsOfTeams: this.teamPickerTeamsOfTeams,
				recurseTeamsOfTeam: this.recurseTeamsOfTeam
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					this.store.loadData(response.records);
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(r){
				clientVM.displayFailure(r);
			}
		});

		this.callParent();
	},

	onRender: function() {
		this.callParent(arguments);
		this.triggerEl.item(0).parent().setDisplayed('none');
	},

	setValue: function(value) {
		this.callParent(arguments);
		if (this.rendered && this.value && !this.mandatory && !this.readOnly) {
			this.triggerEl.item(0).parent().setDisplayed('table-cell');
			this.doComponentLayout();
		}
	}
});