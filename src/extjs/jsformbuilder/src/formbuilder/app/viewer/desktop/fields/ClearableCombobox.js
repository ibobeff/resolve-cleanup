Ext.define('RS.formbuilder.viewer.desktop.fields.ClearableCombobox', {
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.clearablecombobox',

	//Clear Trigger
	clearCls: Ext.baseCSSPrefix + 'form-clear-trigger',
	clearClick: function() {
		//clear the current value and hide the clear button
		this.setValue('');
		this.triggerEl.item(0).parent().setDisplayed('none');
	},

	initComponent: function() {
		this.trigger2Cls = this.triggerCls;
		this.onTrigger2Click = this.onTriggerClick;
		this.trigger1Cls = this.clearCls;
		this.onTrigger1Click = this.clearClick;

		this.callParent();
	},

	onRender: function() {
		this.callParent(arguments);
		if (this.value && !this.readOnly) this.triggerEl.item(0).parent().setDisplayed('table-cell')
		else this.triggerEl.item(0).parent().setDisplayed('none');
	},

	setValue: function(value) {
		this.callParent(arguments);
		if (this.rendered && this.value && !this.mandatory && !this.readOnly) {
			this.triggerEl.item(0).parent().setDisplayed('table-cell');
			this.doComponentLayout();
		}
	}
});