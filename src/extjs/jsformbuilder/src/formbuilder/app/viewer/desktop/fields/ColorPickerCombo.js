Ext.define('RS.formbuilder.viewer.desktop.fields.ColorPickerCombo', {
	extend: 'Ext.form.field.Trigger',
	alias: 'widget.colorpickercombo',
	onTriggerClick: function() {
		var me = this;
		if (!me.picker)
			me.picker = Ext.create('Ext.picker.Color', {
				pickerField: this,
				ownerCt: this,
				renderTo: document.body,
				floating: true,
				// hidden: true,
				focusOnShow: true,
				style: {
					backgroundColor: "#fff"
				},
				listeners: {
					scope: this,
					select: function(field, value, opts) {
						me.setValue('#' + value);
						me.inputEl.setStyle({
							backgroundColor: value
						});
						me.picker.hide();
						me.picker.destroy();
						me.picker = null;
					},
					show: function(field, opts) {
						field.getEl().monitorMouseLeave(500, field.hide, field);
					}
				}
			})
		me.picker.alignTo(me.inputEl, 'tl-bl?');
		me.picker.show(me.inputEl);
	}
});

glu.regAdapter('colorpickercombo', {
	extend: 'combo'
});