/**
 * Rendering engine for the FormBuilder.  Takes a formConfiguration, formName, or formId to display the user configured controls.
 */
Ext.define('RS.formbuilder.viewer.desktop.RSForm', {
	API : {
		submit : '/resolve/service/form/submit',
		getForm : '/resolve/service/form/getform',
		getRecordData : '/resolve/service/dbapi/getRecordData',
		getDefaultData : '/resolve/service/form/getdefaultdata',
		getATProperty : '/resolve/service/atproperties/list',
		getWSData : '/resolve/service/wsdata/getMap'
	},
	showLoading: true,
	params: '',
	fromViewer: false,
	formdefinition: false,

	/**
	 * Separator used to separate different fields in the choice and select fields
	 */
	choiceSeparator: '|&|',

	/**
	 * Separator used to separate different values in the choice and select fields
	 */
	choiceValueSeparator: '|=|',

	extend: 'Ext.panel.Panel',
	alias: 'widget.rsform',

	border: false,
	layout: 'fit',
	fieldConfiguration: [],
	submitConfiguration: {
		viewName: '',
		formSysId: '',
		tableName: '',
		userId: '',
		timezone: '',
		crudAction: '',
		query: '',
		controlItemSysId: '',
		dbRowData: {},
		sourceRowData: {}
	},

	embed: false,
	hideToolbar: false,

	/**
	 * Id of the form to use when looking up the controls from the server
	 */
	formId: '',
	setFormId: function(value) {
		if (value != this.formId) {
			this.formId = value
			this.fireEvent('reloadForm', this)
		}
	},

	/**
	 * Name of the form to use when looking up the controls from the server
	 */
	formName: '',
	setFormName: function(value) {
		if (value != this.formName) {
			this.formName = value
			this.fireEvent('reloadForm', this)
		}
	},

	/**
	 * The record to load into the grid once the form has actually been rendered (or empty for a new record)
	 */
	recordId: '',
	setRecordId: function(value) {
		if (value != this.recordId) {
			this.recordId = value
			this.fireEvent('reloadForm', this)
		}
	},
	queryString: '',
	setQueryString: function(value) {
		if (value != this.queryString) {
			this.queryString = value
			this.fireEvent('reloadForm', this)
		}
	},

	params: '',
	setParams: function(value) {
		if (value != this.params) {
			this.params = value
			this.fireEvent('reloadForm', this)
		}
	},

	initComponent: function() {
		this.addEvents(
			/**
			 * @event actioncompleted
			 * Fires when the button is clicked and all actions have completed running on the server
			 * @param {RS.formbuilder.RSForm} form The form generating this event
			 * @param {Ext.button.Button} button The button that was clicked
			 */
			'actioncompleted', 'beforeclose', 'beforeexit', 'beforeredirect', 'reloadForm');
		//convert all the items configurations to proper configurations
		var configuration = this.formConfiguration;
		if (configuration) this.convertConfig(configuration);
		else if (!this.fromViewer) this.loadForm()
		if (!this.height) delete this.height;
		this.callParent();

		this.on('render', function() {
			var me = this;
			if (me.autoSize) {
				if (!me.height) {
					var doc = Ext.getDoc(),
						size = doc.getViewSize(),
						windowHeight = size.height,
						windowWidth = size.width;
					Ext.EventManager.onWindowResize(function() {
						var doc = Ext.getDoc(),
							size = doc.getViewSize();
						me.maxHeight = size.height - 1;
						me.setWidth(size.width);
						me.doLayout();
						Ext.each(Ext.dom.Query.select('div[class=x-mask]'), function(el) {
							Ext.fly(el).setStyle('display', 'none');
						});
					});
					doc.select('body').setStyle('overflow', 'hidden');
					me.maxHeight = windowHeight - 1;
					me.setWidth(windowWidth);
					me.doLayout();
				} else {
					//Still correct the width if it changes regardless of height setting
					Ext.EventManager.onWindowResize(function() {
						var doc = Ext.getDoc(),
							size = doc.getViewSize();
						me.setWidth(size.width);
						me.doLayout();
						Ext.each(Ext.dom.Query.select('div[class=x-mask]'), function(el) {
							Ext.fly(el).setStyle('display', 'none');
						});
					});
					var size = Ext.getDoc().getViewSize();
					me.setWidth(size.width);
					me.doLayout();
				}
			}
		}, this, {
			single: true
		});

		this.on('reloadForm', function() {
			if (this.rendered) this.loadForm()
		}, this, {
			buffer: 100
		})

		Ext.defer(function() {
			if (typeof(registerForm) == 'function') {
				registerForm(this);
			}
		}, 100, this);
	},

	loadForm: function() {
		if (this.formName || this.formId || this.recordId) {
			Ext.Ajax.request({
				url: this.API['getForm'],
				params: {
					view: this.formName,
					formsysid: this.formId,
					recordid: this.recordId,
					formdefinition: this.formdefinition
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) {
						if (response.data) {
							this.convertConfig(response.data)
							this.getWSData()
							this.getATProperty()
							// store the form's tablename for use in SIR custom data form
							clientVM.forms = {};
							clientVM.forms[response.data.viewName] = response.data.tableName;
						} else {
							clientVM.displayError(RS.formbuilder.locale.formNotFound)
							this.removeAll()
						}
						return;
					}
					clientVM.displayError(response.message);
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			});
		} else this.processUrlVariables()
	},

	/**
	 * Converts the configuration from the server to real controls to be rendered on the page
	 * @param {Object} config
	 */
	convertConfig: function(config) {
		config.displayName = Ext.String.htmlEncode(config.displayName) || '';
		this.viewmodelSpec = {
			mtype: 'viewmodel',
			ns: 'RS.formbuilder'
		};
		var viewSpec = {},
			configuration;
		var buttonPanel = config.buttonPanel ? config.buttonPanel.controls : [] || [];

		this.submitConfiguration = Ext.apply({}, {
			viewName: config.viewName,
			formSysId: config.id,
			tableName: config.tableName,
			userId: ''
		}, this.submitConfiguration);

		this.userName = config.username;

		if (config.windowTitle) {
			var variables = config.windowTitle.match(/\${[\w]+}/g),
				functionString = Ext.String.format('\nvar title="{0}";\n', Ext.String.htmlEncode(config.windowTitle));
			Ext.each(variables, function(variable, index) {
				functionString += Ext.String.format('var s{0} = this.{1};\ntitle = title.replace(/\\{2}/g, s{0});\n', index, variable.substring(2, variable.length - 1), variable);
			});

			//actually set the window title based on the new title
			functionString += 'if( this[\'formScope\'] && this[\'formScope\'].fireEvent(\'titleChanged\', title) !== false){clientVM.setWindowTitle(title);}return title;';
			this.viewmodelSpec.windowTitle$ = new Function(functionString);
		}

		//look in temp for the real controls to display
		config.panels = config.panels || [];
		if (config.panels.length == 0) config.panels.push({
			tabs: [{
				columns: [{
					fields: []
				}]
			}]
		});
		if (config.panels[0].tabs.length > 1) {
			this.viewmodelSpec.activeTabIndex = 0;
			this.viewmodelSpec.tabCount = config.panels[0].tabs.length;
			var tabItems = [],
				tabs = config.panels[0].tabs;
			Ext.each(tabs, function(tab) {
				var controls = tab.columns[0].fields;
				Ext.each(controls, this.convertControl, this);
				tabItems.push({
					layout: 'anchor',
					bodyPadding: '10px',
					title: tab.name,
					name: tab.name.replace(/[^\w]/g, ''),
					defaults: {
						labelWidth: 125
					},
					items: controls,
					autoScroll: true
				});
				tab.name = tab.name.replace(/[^\w]/g, '');
				this.parseDependencies(tab);
			}, this);

			var temp = {
				xtype: 'tabpanel',
				plain: true,
				activeTab: '@{activeTabIndex}',
				items: tabItems
			};

			if (config.wizard) {
				Ext.apply(temp, {
					tabBar: {
						cls: 'wizard-steps'
					}
				});
			}

			configuration = [temp];
		} else {
			if (config.panels[0].tabs[0].columns.length > 0)
				configuration = config.panels[0].tabs[0].columns[0].fields;
			
			if (configuration) {
				for (var i=0; i<configuration.length; i++) {
					if (configuration[i].uiType === 'UrlContainerField' && configuration[i].referenceTableUrl.indexOf('/resolve/') === 0) {
						const urlData = configuration[i].referenceTableUrl.split('#');
						const uriData = urlData[0].split('?');
						clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
							var csrftoken = '?' + data[0] + '=' + data[1];
							var tokenizedData = uri + csrftoken;
							if (uriData.length > 1) {
								tokenizedData += '&' + uriData[1];
							}
							if (urlData.length > 1) {
								tokenizedData += '#' + urlData[1];
							}
							configuration[i].referenceTableUrl = tokenizedData;
						});
					}
				}
			}

			//convert all the controls to appropriate extjs controls
			Ext.Array.forEach(configuration || [], this.convertControl, this);
		}

		this.fieldConfiguration = config.panels[0].tabs;

		//convert the buttons
		if (!buttonPanel) buttonPanel = [];
		Ext.Array.forEach(buttonPanel, this.convertButton, this);

		var parentWindow = this.up('window')
		if (parentWindow && config.displayName) {
			parentWindow.setTitle(config.displayName)
			config.displayName = ''
		}

		Ext.apply(viewSpec, {
			xtype: 'form',
			border: false,
			autoScroll: true,
			bodyPadding: '10px',
			htmlEncodeTitle : true,
			title: this.embed ? '' : config.displayName,
			buttonAlign: 'left',
			cls: 'rs-custom-button',
			items: Ext.Array.from(configuration),
			defaults: {
				labelWidth: 125
			}
		});

		if (this.embed) Ext.apply(viewSpec, {
			tbar: {
				xtype: 'toolbar',
				cls: 'actionBar actionBar-form',
				items: buttonPanel
			}
		})
		else Ext.apply(viewSpec, {
			buttons: buttonPanel
		})

		//Showing a tab panel instead of just a form, so we need to correct some normal form styling
		if (config.panels[0].tabs.length > 1) {
			viewSpec.layout = 'fit';
			viewSpec.autoScroll = false;
			// delete viewSpec.bodyPadding;
		}

		//load up the model and the view
		this.viewmodelSpec.formScope = '';
		this.vm = glu.model(this.viewmodelSpec);
		this.vm.formScope = this;
		this.viewSpec = viewSpec;
		if (this.vm.windowTitle$) this.vm.windowTitle$();
		var view = glu.viewFromSpec(this.vm, viewSpec);
		if (this.hideToolbar) {
			view.dockedItems.each(function(item) {
				if (item.isXType('toolbar')) item.hide()
			})
		}
		if (this.items) {
			this.removeAll()
			this.add(view);
		} else this.items = [view]

		if (this.recordId || this.queryString) this.setRecordId(this.recordId)
		else this.processUrlVariables();

		this.fireEvent('formLoaded', this, this)
	},

	processDataRequest: function(r, button) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			if (response.data) {
				var data = null,
					tempDate = null;
				if (Ext.isArray(response.data)) {
					var vals = {};
					Ext.each(response.data, function(val) {
						vals[val.fieldName] = val.defaultValue;
					})
					response.data = vals;
				}
				for (var key in response.data) {
					data = response.data[key];
					if (this.vm[key + 'IsDate']) {
						tempDate = Ext.Date.parse(data, 'c');
						if (Ext.isDate(tempDate)) data = tempDate;
						//Parse out the option values
						if (data && Ext.isString(data) && data.indexOf(this.choiceSeparator) > -1) {
							data = this.parseValue(data);
						}
					}
					if (data && Ext.isString(data) && data.indexOf(this.choiceSeparator) > -1) {
						data = this.parseValue(data);
					}
					if (Ext.isDefined(this.vm[key])) {
						if (Ext.isDate(this.vm[key]) && !data) data = new Date();
						this.vm.set(key, data);
					}
				}
				//Special processing for radios because the bindings don't work with extjs's messed up structure, we have to manually set the values
				//checkbox group is fix with a manual event wiring..need to find a better way to deal with field group
				var components = this.query('radiogroup');
				Ext.each(components, function(component) {
					var name = component.name;
					if (!name) name = component.items.getAt(0).name;
					if (Ext.isDefined(response.data[name])) {
						var obj = {};
						obj[name] = response.data[name];
						component.setValue(obj);
					}
				});
				this.recordId = response.data.sys_id;
				//Also make sure to reload the reference grids if there are any
				var referenceGrids = this.query('referencetable');
				Ext.each(referenceGrids, function(referenceGrid) {
					referenceGrid.loadReferences();
				});
				//check if there is a reload target, condition, and value on the button.  If there is then we need to reload if that condition isn't met
				if (button && button.reloadCondition && button.reloadTarget && button.reloadValue) {
					var needsReload = false;
					if (Ext.isDefined(this.vm[button.reloadTarget])) {
						var val = this.vm[button.reloadTarget],
							value = button.reloadValue;
						needsReload = true;

						switch (button.reloadCondition) {
							case 'equals':
								if (val == value) needsReload = false;
								break;
							case 'notequals':
								if (val != value) needsReload = false;
								break;
							case 'greaterThan':
								if (val > value) needsReload = false;
								break;
							case 'greaterThanOrEqual':
								if (val >= value) needsReload = false;
								break;
							case 'lessThan':
								if (val < value) needsReload = false;
								break;
							case 'lessThanOrEqual':
								if (val <= value) needsReload = false;
								break;
							case 'contains':
								if (val.indexOf(value) > -1) needsReload = false;
								break;
							case 'notcontains':
								if (val.indexOf(value) == -1) needsReload = false;
								break;
						}
					}

					if (needsReload && this.recordId) {
						if (response.data && !response.data.u_worksheet) {
							//button.enable();
							button.setIcon('');
							clientVM.displayError(RS.formbuilder.locale.worksheet_not_found);
						} else if (response.data.u_worksheet) {
							this.verifyWS(response.data.u_worksheet, function(){
								Ext.defer(this.setRecordId, button.reloadDelay * 1000, this, [this.recordId, button]);
							}.bind(this), function() {
								//button.enable();
								button.setIcon('');
							});
						} else {
							Ext.defer(this.setRecordId, button.reloadDelay * 1000, this, [this.recordId, button]);
						}
					} else {
						button.enable();
						button.setIcon('');
					}
				}
			}
		} else clientVM.displayError(response.message)
		this.processUrlVariables()
			//Ensure the window isn't scrolled by focusing on fields that aren't visible
		window.scrollTo(0, 0)
	},

	/**
	 * Converts an individual control provided by the server to the correct representation for extjs to display
	 * @param {Object} config
	 */
	convertControl: function(config) {
		config.sectionTitle =  Ext.String.htmlEncode(config.sectionTitle|| '');
		config.tooltip =  Ext.String.htmlEncode(config.tooltip|| '');
		config.fieldLabel = config.displayName;
		// config.value = config.value || config.defaultValue;
		config.allowBlank = !config.mandatory;
		config.labelSeparator = config.mandatory ? ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>' : ':';

		//configure viewmodel and dependencies
		this.viewmodelSpec[config.name] = config.value || config.defaultValue || '';

		this.parseDependencies(config);
		// if (config.mandatory) this.viewmodelSpec[config.name + 'IsValid$'] = new Function('if( !this.' + config.name + ')return false;');
		if (!config.width) delete config.width;
		if (!config.height) delete config.height;

		//if its the default height, the just let the controls be what they want to be
		if (config.height == 22 || config.height == 25) delete config.height;

		if (!config.width) {
			config.anchor = '99.5%';
		} else {
			config.width += 125;
		}
		delete config.id; //this could screw things up since the id's start with numbers
		delete config.value;
		if (!config.hidden) delete config.hidden;

		if (config.readOnly) config.fieldCls = 'x-form-field-readOnly';

		if (config.tooltip) {
			config.listeners = {
				render: function(field) {
					var target = field.getEl().dom.id;
					Ext.tip.QuickTipManager.register({
						target: field.getEl().dom.id,
						text: config.tooltip
					});
				}
			};
		}

		switch (config.uiType) {
			case 'TextField':
				config.xtype = 'textfield';
				config.minLength = config.stringMinLength;
				if (config.stringMaxLength > 0 && config.stringMaxLength != 4001) config.maxLength = config.stringMaxLength;
				break;

			case 'TextArea':
				config.xtype = 'textareafield';
				config.resizable = true;
				config.resizeHandles = 's';
				config.minLength = config.stringMinLength;
				if (config.stringMaxLength > 0 && config.stringMaxLength != 4001) config.maxLength = config.stringMaxLength;
				break;

			case 'NumberTextField':
				config.xtype = 'numberfield';
				config.allowDecimals = false;
				config.minValue = config.integerMinValue;
				if (config.integerMaxValue > 0) config.maxValue = config.integerMaxValue;
				break;

			case 'DecimalTextField':
				config.xtype = 'numberfield';
				config.minValue = config.decimalMinValue;
				if (config.decimalMaxVaue > 0) config.maxValue = config.decimalMaxValue;
				break;

			case 'ComboBox':
				config.xtype = 'clearablecombobox';
				config.displayField = 'name';
				config.valueField = 'value';
				config.editable = config.selectUserInput;
				config.forceSelection = !config.selectUserInput;
				config.store = '@{' + config.name + 'Store' + '}';
				this.viewmodelSpec[config.name] = this.parseValue(config.defaultValue).join('');
				//If the store is configured to be from advanced or custom, then we need to get the data once we have values in the fields
				if (config.choiceOptionSelection > 0) {
					this.viewmodelSpec[config.name] = '';
					config.disabled = '@{!' + config.name + 'IsLoadedAndEnabled}';
					config.pageSize = 50;
					var variableString = this.getVariableString(config);
					this.viewmodelSpec[config.name + 'IsLoadedAndEnabled$'] = function() {
						return true;
					}
					if (variableString) {
						this.viewmodelSpec[config.name + 'loadStoreOn$'] = new Function(Ext.String.format('this.set("{1}".substring(0,"{1}".length-5), "");if( {0} ){var ps = this.{1}.proxy.extraParamsOrig || this.{1}.proxy.extraParams,vars=ps.sqlQuery.match(/\\$\{(.*?)\\}/g);this.{1}.proxy.extraParamsOrig=Ext.clone(ps);Ext.each(vars, function(variable, index) {ps.sqlQuery = ps.sqlQuery.replace(variable, this[variable.substring(2,variable.length-1)])},this);this.{1}.load({params: ps});}', variableString, config.name + 'Store'));
						this.viewmodelSpec[config.name + 'IsLoadedAndEnabled$'] = new Function(Ext.String.format('return {0}', variableString));
					}
					config.queryMode = 'local';
					this.viewmodelSpec[config.name + 'Store'] = {
						mtype: 'store',
						autoLoad: variableString ? false : true,
						fields: ['name', 'value'],
						proxy: {
							type: 'ajax',
							url: this.API['getRecordData'],
							extraParams: {
								useSql: true,
								type: 'table',
								sqlQuery: config.choiceOptionSelection == 2 ? config.choiceSql : Ext.String.format('select {0} as name, {1} as value from {2} {3}', config.choiceDisplayField, config.choiceValueField, config.choiceTable, variableString ? Ext.String.format('where {0} = \'{1}\'', config.choiceWhere, '${' + config.choiceField + '}') : '')
							},
							reader: {
								type: 'json',
								root: 'records'
							},
							listeners: {
								exception: function(e, resp, op) {
									clientVM.displayExceptionError(e, resp, op);
								}
							}
						}
					};
				} else {
					this.viewmodelSpec[config.name + 'Store'] = {
						mtype: 'store',
						data: this.parseOptions(config.choiceValues),
						fields: ['name', 'value']
					}
				}
				break;

			case 'MultiSelectComboBoxField':
				config.xtype = 'combobox';
				config.displayField = 'name';
				config.valueField = 'value';
				config.editable = config.selectUserInput;
				config.multiSelect = true;
				config.delimiter = '; ';
				config.forceSelection = !config.selectUserInput;
				this.viewmodelSpec[config.name] = this.parseValue(config.defaultValue);
				config.store = Ext.create('Ext.data.Store', {
					data: this.parseOptions(config.choiceValues),
					fields: [{
						name: 'name'
					}, {
						name: 'value'
					}]
				});
				break;

			case 'MultipleCheckBox':
				config.xtype = 'checkboxgroup';
				config.columns = config.widgetColumns || 0;
				config.items = this.parseOptions(config.choiceValues, config.defaultValue, 'checkbox');
				config.listeners = Ext.applyIf(config.listeners || {}, {
					scope: this,
					change: function(checkboxgroup, newValue, oldValue, eOpts) {
						this.vm.set(checkboxgroup.configName, newValue[checkboxgroup.configName] || oldValue);
					}
				});
				config.hidden = '@{!' + config.name + 'IsVisible}';
				config.currentValue = '@{' + config.name + 'Compute}'
				config.setCurrentValue = function(value) {
					this.items.each(function(item) {
						Ext.each(value, function(val) {
							if (val == item.boxLabel)
								item.setValue(true);
						});
					});
				}
				if (!this.viewmodelSpec[config.name + 'IsVisible$']) this.viewmodelSpec[config.name + 'IsVisible$'] = function() {
					return true
				}
				this.viewmodelSpec[config.name] = this.parseValue(config.defaultValue);
				this.viewmodelSpec[config.name + 'Compute$'] = new Function(Ext.String.format('return this.{0}', config.name));
				Ext.Array.forEach(config.items, function(item) {
					if (Ext.isString(item.value)) item.value = false;
					Ext.apply(item, {
						name: config.name,
						readOnly: config.readOnly
					});
				});
				delete config.value;
				config.configName = config.name;
				delete config.name;
				break;

			case 'RadioButton':
				config.xtype = 'radiogroup';
				config.columns = config.widgetColumns || 0;
				config.items = this.parseOptions(config.choiceValues, config.defaultValue, 'radio');
				this.viewmodelSpec[config.name] = this.parseValue(config.defaultValue).join('');
				Ext.Array.forEach(config.items, function(item) {
					Ext.apply(item, {
						name: config.name,
						readOnly: config.readOnly
					});
				});
				delete config.value;
				config.configName = config.name;
				//delete config.name; /* Preserve the radio item name so IsVisible$ and IsEnabled$ functions will work */ 
				break;

			case 'Date':
				config.xtype = 'xdatefield';
				this.viewmodelSpec[config.name] = config.value || config.defaultValue || new Date()
				this.viewmodelSpec[config.name + 'IsDate'] = true
				if (!Ext.isDate(this.viewmodelSpec[config.name])) this.viewmodelSpec[config.name] = Ext.Date.parse(this.viewmodelSpec[config.name], 'c')
				break;

			case 'Timestamp':
				config.xtype = 'xtimefield';
				this.viewmodelSpec[config.name] = config.value || config.defaultValue || new Date()
				this.viewmodelSpec[config.name + 'IsDate'] = true
				if (!Ext.isDate(this.viewmodelSpec[config.name])) this.viewmodelSpec[config.name] = Ext.Date.parse(this.viewmodelSpec[config.name], 'c')
				config.store = '';
				break;

			case 'DateTime':
				config.xtype = 'xdatetimefield';
				config.hidden = '@{!' + config.name + 'IsVisible}';
				config.disabled = '@{!' + config.name + 'IsEnabled}';
				if (!this.viewmodelSpec[config.name + 'IsVisible$']) this.viewmodelSpec[config.name + 'IsVisible$'] = function() {
					return true
				}
				if (!this.viewmodelSpec[config.name + 'IsEnabled$']) this.viewmodelSpec[config.name + 'IsEnabled$'] = function() {
					return true
				}
				this.viewmodelSpec[config.name] = config.value || config.defaultValue || new Date()
				this.viewmodelSpec[config.name + 'IsDate'] = true
				if (!Ext.isDate(this.viewmodelSpec[config.name])) this.viewmodelSpec[config.name] = Ext.Date.parse(this.viewmodelSpec[config.name], 'c')
				config.items = [{
					xtype: 'hidden',
					name: config.name,
					listeners: {
						render: function(field) {
							if (Ext.isBoolean(field.disabled)) field.setDisabled(field.disabled);
							if (Ext.isBoolean(field.visible)) field.setVisible(field.visible);
						}
					},
					setVisible: function(visible) {
						this.ownerCt.setVisible(visible);
					},
					setDisabled: function(disabled) {
						this.ownerCt.setDisabled(disabled);
					}
				}];
				break;

			case 'Link':
				config.xtype = 'linkfield';
				config.listeners = Ext.applyIf(config.listeners || {}, {
					afterrender: function(field) {
						if (Ext.isBoolean(field.disabled)) field.setDisabled(field.disabled);
					}
				});
				break;

			case 'CheckBox':
				config.xtype = 'checkbox';
				this.viewmodelSpec[config.name] = config.value || (config.defaultValue === true || config.defaultValue === 'true' ? true : false);
				config.inputValue = 'true';
				delete config.fieldCls;
				break;

			case 'Password':
				config.xtype = 'textfield';
				config.inputType = 'password';
				break;

			case 'TagField':
				config.xtype = 'tagfield';
				break;

			case 'Sequence':
				config.xtype = 'displayfield';
				break;

			case 'List':
				config.xtype = 'listfield';
				config.parseOptions = this.parseOptions;
				config.parseValue = this.parseValue;
				config.items = [{
					xtype: 'hidden',
					name: config.name,
					listeners: {
						render: function(field) {
							if (Ext.isBoolean(field.disabled)) field.setDisabled(field.disabled);
							if (Ext.isBoolean(field.visible)) field.setVisible(field.visible);
						}
					},
					setVisible: function(visible) {
						this.ownerCt.setVisible(visible);
					},
					setDisabled: function(disabled) {
						this.ownerCt.setDisabled(disabled);
					}
				}];
				this.viewmodelSpec[config.name] = this.parseValue(config.defaultValue);
				break;

			case 'ButtonField':
				//deal with actions
				config.xtype = 'button';
				delete config.anchor;
				config.text = config.displayName;
				config.handler = this.handleButtonClick;
				config.scope = this;
				break;

			case 'Reference':
				config.xtype = 'referencefield';
				break;

			case 'Journal':
				config.xtype = 'journalfield';
				config.userId = this.userName;
				config.items = [{
					xtype: 'hiddenfield',
					name: config.name,
					listeners: {
						render: function(field) {
							if (Ext.isBoolean(field.disabled)) field.setDisabled(field.disabled);
							if (Ext.isBoolean(field.visible)) field.setVisible(field.visible);
						}
					},
					getValue: function() {
						var temp = this.ownerCt.journalEntries.slice(0),  // me.ownerCt.journalEntries is immutable
							newVal = this.ownerCt.down('textarea').getValue();
						if (newVal) {
							temp.splice(0, 0, {
								userId: this.ownerCt.userId,
								createDate: Ext.Date.format(new Date(), 'c'),
								note: Ext.htmlEncode(newVal).replace(/\n/g, '<br/>')
							});
						}
						return Ext.encode(temp);
					},
					setVisible: function(visible) {
						this.ownerCt.setVisible(visible);
					},
					setDisabled: function(disabled) {
						this.ownerCt.setDisabled(disabled);
					}
				}];
				break;

			case 'ReadOnlyTextField':
				config.xtype = 'displayfield';
				break;

			case 'UserPickerField':
				config.xtype = 'userpickerfield';
				config.meValue = this.userName;
				config.store = '';
				break;

			case 'TeamPickerField':
				config.xtype = 'teampickerfield';
				config.store = '';
				break;

			case 'EmailField':
				config.xtype = 'textfield';
				config.regex = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
				config.invalidText = 'The value in this field is invalid. It must be an email address like: someone@example.com';
				break;

			case 'MACAddressField':
				config.xtype = 'textfield';
				config.regex = /(?=^[0-9a-f]{2}([\.:-])(?:[0-9a-f]{2}\1){4}[0-9a-f]{2}$|^[0-9a-f]{2}([\.:-])(?:[0-9a-f]{2}\2){6}[0-9a-f]{2}$|^[0-9a-f]{3}([\.:-])(?:[0-9a-f]{3}\3){2}[0-9a-f]{3}$|^[0-9a-f]{4}([\.:-])(?:[0-9a-f]{4}\4){2}[0-9a-f]{4}$)/i;
				config.invalidText = 'The value in this field is invalid. It must follow standard 48 bits or 64 bits MAC Address Format';
				break;

			case 'IPAddressField':
				config.xtype = 'textfield';
				config.regex = /^(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))$/;
				config.invalidText = 'The value in this field is invalid. It must be an IP Address like: 192.168.0.1';
				break;

			case 'CIDRAddressField':
				config.xtype = 'textfield';
				config.regex = /^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2])){1}$/;
				config.invalidText = 'The value in this field is invalid. It must be an CIDR Address like: 192.168.0.1/14';
				break;

			case 'PhoneField':
				config.xtype = 'textfield';
				config.plugins = [new RS.formbuilder.viewer.desktop.InputTextMask('(999) 999 - 9999', undefined, 1)];
				break;

			case 'NameField':
				config.xtype = 'namefield';
				config.choiceSeparator = this.choiceSeparator;
				config.items = [{
					xtype: 'hiddenfield',
					name: config.name,
					listeners: {
						render: function(field) {
							if (Ext.isBoolean(field.disabled)) field.setDisabled(field.disabled);
							if (Ext.isBoolean(field.visible)) field.setVisible(field.visible);
						},
						change: function(panel, newValue, oldValue) {
							var choiceSeparator = this.up().choiceSeparator;
							var values = newValue.split(choiceSeparator);
							var idx = 0;
							var items = panel.ownerCt.items.items;
							for (var i = 0; i < items.length; i++) {
								if ((items[i].xtype === 'textfield' || items[i].xtype === 'combobox') && Ext.isFunction(items[i].setValue)) {
									items[i].setRawValue(values[idx++]);
								}
							}
						}
					},
					setVisible: function(visible) {
						this.ownerCt.setVisible(visible);
					},
					setDisabled: function(disabled) {
						this.ownerCt.setDisabled(disabled);
					}
				}];
				break;

			case 'AddressField':
				config.xtype = 'addressfield';
				config.choiceSeparator = this.choiceSeparator;
				config.items = [{
					xtype: 'hiddenfield',
					name: config.name,
					listeners: {
						render: function(field) {
							if (Ext.isBoolean(field.disabled)) field.setDisabled(field.disabled);
							if (Ext.isBoolean(field.visible)) field.setVisible(field.visible);
						},
						change: function(panel, newValue, oldValue) {
							var choiceSeparator = this.up().choiceSeparator;
							var values = newValue.split(choiceSeparator);
							var idx = 0;
							var items = panel.ownerCt.items.items;
							for (var i = 0; i < items.length; i++) {
								if (items[i].xtype === 'textfield' && Ext.isFunction(items[i].setValue)) {
									items[i].setRawValue(values[idx++]);
								} else if (items[i].xtype === 'fieldcontainer') {
									var subItems = items[i].items.items;
									for (var j = 0; j < subItems.length; j++) {
										if ((subItems[j].xtype === 'textfield' || subItems[j].xtype === 'combobox') && Ext.isFunction(subItems[j].setValue)) {
											subItems[j].setRawValue(values[idx++]);
										}
									}
								}
							}
						}
					},
					setVisible: function(visible) {
						this.ownerCt.setVisible(visible);
					},
					setDisabled: function(disabled) {
						this.ownerCt.setDisabled(disabled);
					}
				}];
				break;

			case 'FileUploadField':
				this.viewmodelSpec[config.name] = [];
				config.xtype = 'uploadmanager';
				config.formName = this.formName;
				config.recordId = this.recordId;
				break;

			case 'ReferenceTableField':
				this.viewmodelSpec[config.name] = [];
				config.xtype = 'referencetable';
				config.recordId = this.recordId;
				break;

			case 'HiddenField':
				config.xtype = 'hiddenfield';
				config.value = config.hiddenValue;
				break;

			case 'SpacerField':
				delete this.viewmodelSpec[config.name];
				config.xtype = 'component';
				if (config.showHorizontalRule) {
					config.html = '<hr/>'
				}
				delete config.name;
				break;

			case 'LabelField':
				config.xtype = 'label'
				config.text = config.displayName
				break;

			case 'WysiwygField':
				config.xtype = 'htmleditor';
				config.defaultValue = config.defaultValue || (Ext.isOpera || Ext.isIE6) ? '&#160;' : '&#8203;'
				break;

			case 'TabContainerField':
				this.viewmodelSpec[config.name] = [];
				config.xtype = 'tabcontainerfield';
				config.formName = this.formName;
				config.recordId = this.recordId;
				break;

			case 'UrlContainerField':
				this.viewmodelSpec[config.name] = [];
				config.xtype = 'urlcontainerfield';
				break;

			case 'SectionContainerField':
				if (config.height == 25) delete config.height;
				config.xtype = config.sectionType;
				config.border = false;
				config.bodyPadding = '10px 0px 10px 0px';
				config.layout = {
					type: 'hbox',
					align: 'stretch'
				};
				config.defaults = {
					flex: 1,
					border: false
				};
				config.collapsible = true;
				config.title = config.sectionTitle;
				var cols = [],
					decoded = Ext.decode(config.sectionColumns);
				Ext.Array.forEach(decoded, function(column) {
					var ctrls = [];
					Ext.Array.forEach(column.controls, function(control) {
						if (control) {
							this.convertControl(control)
							ctrls.push(control);
						}
					}, this);
					cols.push({
						layout: 'anchor',
						items: ctrls
					})
				}, this)
				config.items = cols;
				break;

			default:
				config.xtype = 'textfield'

				break;
		}
	},

	getVariableString: function(config) {
		var variables = '';
		if (config.choiceOptionSelection == 1 && config.choiceField && config.choiceWhere) {
			variables = Ext.String.format('this.{0}', config.choiceField);
		} else {
			config.choiceSql = config.choiceSql || '';
			var vars = config.choiceSql.match(/\$\{(.*?)\}/g)
			Ext.Array.forEach(vars || [], function(variable, index) {
				if (index > 0) variables += ' && ';
				variables += 'this.' + variable.substring(2, variable.length - 1);
			});
		}

		return variables;
	},

	defaultFont: 'Verdana',
	defaultFontSize: '10',
	defaultFontColor: '333333',
	defaultBackgroundColor: 'DDDDDD',

	// Approximate fontsize conversion from pt to px
	fontSizePtMapping : {
		6 : 8,
		8 : 11,
		9 : 12,
		10 : 13,
		12 : 16,
		14 : 19,
		18 : 24,
		24 : 32,
		30 : 40,
		36 : 48,
		48 : 64,
		60 : 80
	},

	/**
	 * Converts a server configuration of a button into a button that can be rendered by extjs
	 * @param {Object} button
	 */
	convertButton: function(button) {
		button.text = button.displayName || "Fix Me!";
		button.handler = this.handleButtonClick;
		button.sysId = button.id;
		button.scope = this;

		if (Ext.firefoxVersion !== 0) {
			// RBA-12536: this is Ext JS 4 bug. We are putting a bandage to get around the issue
			button.cls = 'fixFirefoxIssue';
		}
		switch (button.type) {
			case 'separator':
				button.xtype = 'tbseparator';
				break;
			case 'spacer':
				button.xtype = 'tbspacer';
				break;
			case 'rightJustify':
				button.xtype = 'tbfill';
				break;
			case 'button':
				var fontFamily = button.font+', Tahoma' || this.defaultFont+', Tahoma',
					fontSize,
					color = '#' + (button.fontColor ? button.fontColor : this.defaultFontColor),
					backgroundColor = '#' + (button.backgroundColor ? button.backgroundColor : this.defaultBackgroundColor);

				if (button.fontSize && this.fontSizePtMapping[parseInt(button.fontSize)]) {
					fontSize = parseInt(button.fontSize);
				} else {
					fontSize = parseInt(this.defaultFontSize);
				}

				var style = {
					fontFamily: fontFamily,
					fontSize: this.fontSizePtMapping[fontSize] + 'px',
					color: color,
					backgroundColor: backgroundColor
				}
				Ext.apply(button, {
					style: style,
					height: this.fontSizePtMapping[fontSize] + 10
				});
			default:
				button.xtype = 'button';
				break;
		}

		//determine crud action for db type actions
		button.crudAction = this.getCrudAction(button);

		//make sure we have name for the glu dependencies
		button.name = 'button_' + button.id;
		//setup standard glu dependencies for next/back buttons
		//conflicts with glu bindings
		delete button.hidden;

		//delete id because it screws things up to have an id on extjs controls
		delete button.id;
		this.parseButtonDependencies(button);
	},
	getCrudAction: function(button) {
		var i, actions = button.actions || [],
			len = actions.length,
			action;
		for (i = 0; i < len; i++) {
			action = actions[i];
			if (action.operation == 'DB' || action.operation == 'SIR') return action.crudAction;
		}
	},
	/**
	 * Parses values out of a select or choice control
	 * @param {Object} configValue
	 */
	parseValue: function(configValue) {
		var returnValue = [],
			configValue = configValue || '';
		var values = Ext.isString(configValue) ? configValue.split(this.choiceSeparator) : configValue;
		Ext.each(values, function(value) {
			if (value) {
				var val = value.split(this.choiceValueSeparator);
				var nameVal = val[0].split('=');
				if (nameVal.length == 1) nameVal.push(nameVal[0]);
				returnValue.push(val[1] == 'true' ? nameVal[1] : val[0]);
			}
		}, this);
		return returnValue;
	},
	/**
	 * Parses options out of a select or choice control
	 * @param {Object} optionString
	 * @param {Object} valueString
	 */
	parseOptions: function(optionString, valueString, type) {
		var options = [],
			optionString = optionString || '',
			defaultOptions = {};
		if (type) defaultOptions.xtype = type;
		var split = optionString.split(this.choiceSeparator);
		Ext.each(split, function(option) {
			var nameValue = option.split(this.choiceValueSeparator);
			if (type != 'radio') defaultOptions.value = nameValue[1] && (nameValue[1] != 'true' || nameValue[1] != 'false') ? nameValue[1] : nameValue[0];
			if (nameValue[0]) {
				var nameValCombo = nameValue[0].split('=');
				if (nameValCombo.length == 1) nameValCombo.push(nameValCombo[0]);
				if (!type) {
					defaultOptions.name = nameValCombo[0];
					defaultOptions.value = nameValCombo[1];
				}
				options.push(Ext.apply({
					boxLabel: nameValCombo[0],
					inputValue: nameValCombo[1],
					xtype: type,
					hideLabel: true
				}, defaultOptions))
			}
			delete defaultOptions.value;
			delete defaultOptions.name;
		}, this);
		if (valueString) {
			var values = valueString.split(this.choiceSeparator),
				i, j;
			for (i = 0; i < values.length; i++) {
				var val = values[i].split(this.choiceValueSeparator);
				// if(val[1] == 'true') {
				for (j = 0; j < options.length; j++) {
					var nameVal = val[0].split('=');
					if (nameVal.length == 1) nameVal.push(nameVal[0]);
					if (options[j].boxLabel == nameVal[0]) options[j][type == 'checkbox' ? 'value' : 'checked'] = true;
					else options[j][type == 'checkbox' ? 'value' : 'checked'] = options[j][type == 'checkbox' ? 'value' : 'checked'] == true || false;
				}
				// }
			}
		}
		return options;
	},
	/**
	 * Handles the button clicks to actions relationship and will perform the appropriate posting of the values to be processed by the server.
	 * Deals with reset and close actions client side if everything goes well on the server side.
	 * @param {Object} button
	 */
	handleButtonClick: function(button) {
		//If we're in the middle of uploading, then message the user that they can't perform actions until the upload is done
		var uploadForm = button.up('form'),
			uploader = uploadForm.down('uploadmanager');
		if (uploader && uploader.isUploading()) {
			Ext.Msg.alert(RS.formbuilder.locale.uploadInProgressTitle, RS.formbuilder.locale.uploadInProgressBody);
			return;
		}
		//Handle actions
		var shouldReset = false,
			shouldReload = false,
			reloadDelay = 0,
			reloadTarget = '',
			reloadCondition = '',
			reloadValue = '',
			shouldClose = false,
			shouldExit = false,
			shouldReturn = false,
			submitToServer = false,
			automationExecuted = false,
			actionParams = {},
			displayMessage = false,
			messageBoxTitle = '',
			messageBoxMessage = '',
			messageBoxYesText = '',
			messageBoxNoText = '';
		Ext.each(button.actions, function(action) {
			switch (action.operation) {
				case 'RESET':
					shouldReset = true;
					break;
				case 'CLOSE':
					shouldClose = true;
					break;
				case 'EXIT':
					shouldExit = true;
					break;
				case 'RETURN':
					shouldReturn = true
					break;
				case 'MESSAGEBOX':
					displayMessage = true;
					if (action.additionalParam && Ext.isString(action.additionalParam)) {
						var decode = this.deserializeParams(action.additionalParam);
						messageBoxTitle = Ext.String.htmlEncode(decode.messageBoxTitle);
						messageBoxMessage = decode.messageBoxMessage;
						messageBoxYesText = decode.messageBoxYesText;
						messageBoxNoText = decode.messageBoxNoText;
					}
					break;
				case 'RELOAD':
					shouldReload = true;
					if (action.additionalParam && Ext.isString(action.additionalParam)) {
						var decode = this.deserializeParams(action.additionalParam);
						reloadDelay = decode.reloadDelay;
						reloadTarget = decode.reloadTarget;
						reloadCondition = decode.reloadCondition;
						reloadValue = decode.reloadValue;
					}
					break;
				case 'DB':
					if (action.additionalParam && Ext.isString(action.additionalParam)) {
						var decode = this.deserializeParams(action.additionalParam);
						if (decode.sirAction && decode.sirAction == 'saveCaseData') {
							action.operation = 'SIR';
							button.crudAction = 'saveCaseData';
							action.additionalParam = '';
						}
					}
				case 'SIR':
				case 'EVENT':
				case 'RUNBOOK':
				case 'ACTIONTASK':
				case 'SCRIPT':
				case 'REDIRECT':
				case 'MAPPING':
					automationExecuted = true;
					submitToServer = true;
					if (action.additionalParam && Ext.isString(action.additionalParam)) {
						var decode = this.deserializeParams(action.additionalParam);
						for (var key in decode) {
							if (!decode[key]) delete decode[key];
						}
						Ext.applyIf(actionParams, decode);
						if (actionParams.showConfirmation) {
							displayMessage = true;
							messageBoxTitle = RS.formbuilder.locale.confirmDeleteTitle;
							messageBoxMessage = RS.formbuilder.locale.confirmDeleteMessage;
							messageBoxYesText = RS.formbuilder.locale.confirmDeleteYesText;
							messageBoxNoText = RS.formbuilder.locale.confirmDeleteNoText;
						}
					}
					break;
				case 'NEXT':
					var tabPanel = this.down('tabpanel');
					if (tabPanel) {
						var nextIndex = this.vm.activeTabIndex + 1,
							nextTab = tabPanel.items.getAt(nextIndex);
						while (tabPanel.items.getAt(nextIndex) && (tabPanel.items.getAt(nextIndex).tab.hidden || tabPanel.items.getAt(nextIndex).tab.disabled)) {
							nextIndex += 1;
						}
						if (tabPanel.items.getAt(nextIndex)) this.vm.set('activeTabIndex', nextIndex);
					}
					break;
				case 'BACK':
					var tabPanel = this.down('tabpanel');
					if (tabPanel) {
						var previousIndex = this.vm.activeTabIndex - 1,
							previousTab = tabPanel.items.getAt(previousIndex);
						while (tabPanel.items.getAt(previousIndex) && (tabPanel.items.getAt(previousIndex).tab.hidden || tabPanel.items.getAt(previousIndex).tab.disabled)) {
							previousIndex -= 1;
						}
						if (tabPanel.items.getAt(previousIndex)) this.vm.set('activeTabIndex', previousIndex);
					}
					break;
				case 'GOTO':
					var tabPanel = this.down('tabpanel');
					if (tabPanel) {
						var decode = this.deserializeParams(action.additionalParam),
							index = -1;
						var gotoTabName = decode['gotoTabName']
						gotoTabName = gotoTabName.replace(/[^\w]/g, '')
						tabPanel.items.each(function(tab, idx) {
							if (tab.name === gotoTabName) index = idx
						}, this)
						if (index > -1)
							this.vm.set('activeTabIndex', index)
					}
					break;
				default:
					Ext.Msg.alert(RS.formbuilder.locale.unknownAction, Ext.String.format(RS.formbuilder.locale.unknownActionBody, action.operation));
					break;
			}
		}, this);

		if (shouldReturn) {
			clientVM.handleNavigationBack()
		}

		button.shouldSubmitToServer = submitToServer;
		button.shouldReset = shouldReset;
		button.shouldClose = shouldClose;
		button.shouldExit = shouldExit;
		button.actionParams = actionParams;
		button.shouldReload = shouldReload;
		button.reloadDelay = reloadDelay;
		button.reloadTarget = reloadTarget;
		button.reloadCondition = reloadCondition;
		button.reloadValue = reloadValue;
		button.automationExecuted = automationExecuted;

		if (displayMessage) {
			Ext.MessageBox.show({
				title: messageBoxTitle,
				msg: messageBoxMessage,
				buttons: Ext.MessageBox.YESNO,
				buttonText: {
					yes: messageBoxYesText,
					no: messageBoxNoText
				},
				scope: button,
				fn: this.handleMessage
			});
			return;
		}

		if (submitToServer) {
			this.submitToServer(button);
		} else {
			if (shouldReset) {
				button.up('form').getForm().reset();
				this.getDefaultData();
			}
			if (shouldExit && this.fireEvent('beforeexit') !== false) {
				var win = button.up('window');
				if (win)
					win.close()
				else
					clientVM.handleNavigationBackOrExit()
			}
			if (shouldClose && this.fireEvent('beforeclose') !== false) {
				var win = button.up('window') || getResolveRoot();
				win.close();
			}
			if (shouldReload) {
				if (this.recordId) Ext.defer(this.setRecordId, button.reloadDelay * 1000, this, [this.recordId, button]);
				else Ext.defer(this.getDefaultData, button.reloadDelay * 1000, this, []);
			}
			this.fireEvent('actioncompleted', this, button);
		}
	},
	handleMessage: function(button) {
		if (button === 'yes') {
			var form = this.up('form'),
				rsForm = this.up('rsform');
			if (this.shouldSubmitToServer) rsForm.submitToServer(this);
			else {
				if (this.shouldReset) {
					this.getDefaultData();
				}
				if (this.shouldExit && this.fireEvent('beforeexit') !== false) {
					var win = button.up('window');
					if (win)
						win.close()
					else
						clientVM.handleNavigationBackOrExit()
				}
				if (this.shouldClose && this.fireEvent('beforeclose') !== false) {
					var win = this.up('window') || getResolveRoot();
					win.close();
				}
				this.fireEvent('actioncompleted', this, button);
			}
		}
	},
	getDefaultData: function() {
		Ext.Ajax.request({
			url: this.API['getDefaultData'],
			params: {
				view: this.formName,
				formsysid: this.formId,
				recordid: this.recordId
			},
			scope: this,
			success: function(r) {
				this.processDataRequest(r)
				this.processUrlVariables()
				this.getWSData()
				this.getATProperty()
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		});
	},

	getATProperty: function() {
		var properties = {}
		var filters = []
		Ext.Object.each(this.vm, function(key, value) {
			if (Ext.isString(value) && /\$PROPERTY\{(.*)\}/gi.test(value)) {
				var k = /\$PROPERTY\{(.*)\}/gi.exec(value)[1]
				if (k) {
					properties[k] = key
					filters.push({
						field: 'uname',
						type: 'auto',
						condition: 'equals',
						value: k
					});
				}
			}
		}, this)
		if (filters.length == 0)
			return;
		Ext.Ajax.request({
			url: this.API['getATProperty'],
			scope: this,
			params: {
				limit: -1,
				operator: 'or',
				filter: Ext.encode(filters)
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					//clientVM.displayError(this.localize('getPropertyError', respData.message))
					clientVM.displayError(respData.message);
					return
				}
				Ext.each(respData.records, function(r) {
					if (properties[r.uname])
						this.vm.set(properties[r.uname], r.uvalue);
				}, this)
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})

	},

	getWSData: function() {
		//Now that we have all the default data from the form and the url params processed, we need to apply any WSDATA data
		Ext.Ajax.request({
			url: this.API['getWSData'],
			params: {
				problemId: clientVM.problemId
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					//Process through fields, looking for their default values to be set to something that is in the WSDATA
					//Something like ${test} means test: 'seomthing' must exist in the WSDATA
					Ext.Object.each(this.vm, function(key, value) {
						if (Ext.isString(value) && /\$WSDATA\{(.*)\}/gi.test(value)) {
							var k = /\$WSDATA\{(.*)\}/gi.exec(value)[1]
							if (k && Ext.isDefined(response.data[k])) {
								this.vm.set(key, response.data[k])
							}
							else
								//Default to blank value if couldnt find a match.
								this.vm.set(key, '');
						}
					}, this)
				} else
					clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	resetForm: function() {
		this.down('form').getForm().reset()
	},

	saveForm: function() {
		var saveButton = null;
		this.down('form').dockedItems.each(function(item) {
			if (item.isXType('toolbar')) {
				item.items.each(function(button) {
					if (button.actions && Ext.isArray(button.actions)) {
						Ext.Array.forEach(button.actions, function(action) {
							if (action.crudAction == 'SUBMIT') {
								saveButton = button
							}
						})
					}
					if (button) return false
				})
			}
		})

		if (!saveButton) {
			clientVM.displayError(RS.formbuilder.locale.noSaveButtonBody, 10000, RS.formbuilder.locale.noSaveButtonTitle);
		} else this.handleButtonClick(saveButton)
	},
	submitToServer: function(button, form) {
		if (!form) form = button.up('form')

		if (!form.getForm().isValid()) {
			clientVM.displayError(RS.formbuilder.locale.invalidFormBody, 10000, RS.formbuilder.locale.invalidFormTitle);			
			return;
		}

		if (!button) {
			button = {}
		} else {
			//Change button to show "working" state
			button.disable();
			button.setIcon('/resolve/images/loading.gif');
		}

		var params = form.getForm().getValues(),
			serverParams = this.submitConfiguration,
			key;

		//The server needs to know the id of the button that was clicked to know which actions to perform
		serverParams.controlItemSysId = button.sysId;
		serverParams.crudAction = button.crudAction;

		//Go get the displayField information too
		var displayFields = form.query('displayfield');
		Ext.each(displayFields, function(displayField) {
			params[displayField.getName()] = displayField.getValue();
		});

		//Because they are so different, do the same with journal fields
		var journalFields = form.query('journalfield');
		Ext.each(journalFields, function(journalField) {
			var hidden = journalField.down('hiddenfield');
			if (hidden) {
				var name = hidden.getName(),
					value = hidden.getValue();
				params[name] = value;
				if (Ext.isDefined(this.vm[name])) this.vm.set(name, value);
			}
		}, this);

		//Prepare the params for form submission
		var dbData = {},
			inputData = {},
			paramType, actionParams = button.actionParams;

		//Take the action params, and if the PROBLEMID or EVENT_REFERENCE is a field, then populate them with the field value instead
		if (actionParams && actionParams.PROBLEMID && actionParams.PROBLEMID == 'MAPFROM' && params[actionParams.mappingFrom]) {
			params.PROBLEMID = params[actionParams.mappingFrom];
			params.EVENT_REFERENCE = params[actionParams.mappingFrom];
		}

		for (key in params) {
			paramType = this.getParamType(key);
			if (paramType === 'DB') dbData[key] = Ext.isArray(params[key]) ? this.serializeChoice(params[key]) : params[key]
			else if (paramType === 'INPUT') inputData[key] = Ext.isArray(params[key]) ? this.serializeChoice(params[key]) : params[key];
		}

		// if button action is "saveCaseData" & customDataForm, use formRecord from customDataForm (if exist)
		if (button.crudAction === 'saveCaseData' && clientVM.customDataForm) {
			if (clientVM.customDataForm && clientVM.customDataForm.formNames && clientVM.customDataForm.formNames[serverParams.viewName]) {
				Ext.apply(dbData, {
					sys_id: clientVM.customDataForm.formNames[serverParams.viewName]
				});
			}
		} 
		//Make sure the sys_id is in there if it came from the server
		else if (this.recordId) Ext.apply(dbData, {
			sys_id: this.recordId
		});

		serverParams.dbRowData.data = dbData;
		serverParams.sourceRowData.data = inputData;

		if (clientVM.activeScreen.parentVM && clientVM.activeScreen.parentVM.sirContext) {
			var sirContextVM = clientVM.activeScreen.parentVM;
			if (sirContextVM.ticketInfo && sirContextVM.ticketInfo.problemId) {
				serverParams.sourceRowData.data.PROBLEMID = sirContextVM.ticketInfo.problemId;	// SIR problemId has highest precedence
				serverParams.sourceRowData.data.incidentId = sirContextVM.ticketInfo.id;
				serverParams.sourceRowData.data.phase = sirContextVM.activitiesTab.activitiesView.phase;
				serverParams.sourceRowData.data.activityName = sirContextVM.activitiesTab.activitiesView.activityName;
			}
			if (sirContextVM.activityId) {
				serverParams.sourceRowData.data.activityId = sirContextVM.activityId;
			}
		} else if (clientVM.SIR_SELECTED_ACTIVITY) {
				serverParams.sourceRowData.data.PROBLEMID = clientVM.SIR_PROBLEMID;	// SIR problemId has highest precedence
				serverParams.sourceRowData.data.incidentId = clientVM.SIR_SELECTED_ACTIVITY.incidentId;
				serverParams.sourceRowData.data.phase = clientVM.SIR_SELECTED_ACTIVITY.phase;
				serverParams.sourceRowData.data.activityName = clientVM.SIR_SELECTED_ACTIVITY.activityName;		
				serverParams.sourceRowData.data.activityId = clientVM.SIR_SELECTED_ACTIVITY.id;			
		} else {
			actionParams.PROBLEMID = clientVM.SIR_PROBLEMID || this.problemId || actionParams.PROBLEMID;  // URL problemId has higher precedence
		}

		//Apply any additional parameters from the action that is being performed
		Ext.applyIf(serverParams.sourceRowData.data, actionParams);
		Ext.apply(serverParams.sourceRowData.data, {
			'RESOLVE.ORG_NAME' : clientVM.orgName,
			'RESOLVE.ORG_ID' : clientVM.orgId
		})
		//Apply the current runbook that the form is on
		serverParams.currentDocumentName = window['wikiDocumentName'] || ''

		Ext.Ajax.request({
			url: this.API['submit'],
			jsonData: serverParams,
			scope: this,
			success: function(r) {
				if (!button.shouldReload && button.enable) {
					button.enable();
					button.setIcon('');
				}
				var response = RS.common.parsePayload(r);
				if (response.success) {

					//update problemId and problemNumber on the client if possible
					if (clientVM.SIR_PROBLEMID) {
						(clientVM || getResolveRoot().clientVM).fireEvent('refreshResult');
						// if button action is "saveCaseData" and we have customDataForm, then also submit the data to Custom Form Data
						if (button.crudAction === 'saveCaseData' && clientVM.SIR_INCIDENTID && clientVM.customDataForm) {
							this.submitCustomDataForm(response.data);
						}
					}
					else if (button.actionParams.PROBLEMID == 'ACTIVE') {
						Ext.defer(function() {
							(clientVM || getResolveRoot().clientVM).fireEvent('problemIdChanged');
						}, 500)
					} else if (button.actionParams.PROBLEMID == 'NEW' && response.data.PROBLEMID && getResolveRoot().clientVM) {
						clientVM.updateProblemInfo(response.data.PROBLEMID);
					} else if (button.automationExecuted && (clientVM || getResolveRoot().clientVM)) {
						Ext.defer(function() {
							(clientVM || getResolveRoot().clientVM).fireEvent('worksheetExecuted');

							// trigger refresh result since worksheetExecuted is currently not doing anything
							(clientVM || getResolveRoot().clientVM).fireEvent('refreshResult');
						}, 500)
					}

					if (button.crudAction === 'saveCaseData' && clientVM.customDataForm) {
						// do nothing for custom data form
					} else {
						//Populate sys_id if it comes back from the server so that any further actions are updates until reloaded
						if (response.data.DB_SYS_ID) this.recordId = response.data.DB_SYS_ID;
					}


					//TODO: errorException

					//Redirect the user if they specified a redirect action
					if (response.data.REDIRECT) {
						var redirect = response.data.REDIRECT,
							executeRedirect = true;
						if (response.data.REDIRECT_TARGET == '_self' && this.fireEvent('beforeredirect', response.data.REDIRECT) === false) {
							executeRedirect = false
						}

						if (executeRedirect) {
							if (redirect.indexOf('#') == 0 || redirect.indexOf('RS.') == 0) {
								var splits = redirect.indexOf('#') == 0 ? redirect.substring(1).split('/') : redirect.split('/')
								clientVM.handleNavigation({
									modelName: splits[0],
									target: response.data.REDIRECT_TARGET,
									params: Ext.Object.fromQueryString(splits[1])
								})
							} else if (redirect.indexOf('/') == 0) {
								clientVM.handleNavigation({
									modelName: 'RS.client.URLScreen',
									target: response.data.REDIRECT_TARGET,
									params: {
										location: redirect
									}
								})
							} else {
								window.open(response.data.REDIRECT.replace(/${sys_id}/g, this.recordId), response.data.REDIRECT_TARGET)
							}
						}
					}

					if (button.shouldReset) {
						this.down('form').getForm().reset();
						this.recordId = '';
						this.getDefaultData();
					}
					//display success message
					clientVM.displaySuccess(RS.formbuilder.locale.formSavedSuccessfully, null, RS.formbuilder.locale.success);
					if (button.shouldExit && this.fireEvent('beforeexit') !== false) {
						var win = button.up('window');
						if (win)
							Ext.defer(function() {
								win.close();
							}, 3000);
						else
							Ext.defer(function() {
								clientVM.handleNavigationBackOrExit()
							}, 3000);
					}

					if (button.shouldClose && this.fireEvent('beforeclose') !== false) {
						var win = this.up('window') || getResolveRoot();
						Ext.defer(function() {
							win.close();
						}, 3000);
					}

					var managers = this.query('uploadmanager');
					Ext.each(managers, function(manager) {
						manager.recordId = this.recordId;
						manager.setEnabled(this.recordId);
					}, this);

					if (button.shouldReload && this.recordId) {
						Ext.defer(this.setRecordId, button.reloadDelay * 1000, this, [this.recordId, button]);
					}
					this.fireEvent('actioncompleted', this, button);
				} else {
					//display error message
					clientVM.displayError(response.message);
				}
			},
			failure: function(r) {
				if (button.enable) {
					button.enable();
					button.setIcon('');
				}
				clientVM.displayFailure(r);
			}
		});
	},
	submitCustomDataForm: function(data, onSuccess, onFailure) {
		if (clientVM.SIR_INCIDENTID && clientVM.customDataForm) {
			if (!clientVM.customDataForm.formNames) {
				clientVM.customDataForm.formNames = {};
			}
			if (!clientVM.customDataForm.formNames[this.formName]) {
				clientVM.customDataForm.formNames[this.formName] = data.DB_SYS_ID;
			}
			var formRecord = {};
			formRecord[this.formName] = data.DB_SYS_ID;
			Ext.Ajax.request({
				url: '/resolve/service/playbook/customdata',
				jsonData: {
					incidentId: clientVM.SIR_INCIDENTID,
					wikiId: clientVM.customDataForm.baseWikiId,
					formRecord: formRecord,
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) {
						if (Ext.isFunction(onSuccess)) {
							onSuccess();
						}
					} else {
						if (Ext.isFunction(onFailure)) {
							onFailure();
						}
						clientVM.displayError(response.message);
					}
				},
				failure: function(resp) {
					if (Ext.isFunction(onFailure)) {
						onFailure();
					}
					clientVM.displayFailure(resp);
				}
			});
		}
	},
	verifyWS: function(worksheetId, onSuccess, onFailure) {
		Ext.Ajax.request({
			url: '/resolve/service/worksheet/getWS',
			params: {
				id: worksheetId
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					if (Ext.isFunction(onSuccess)) {
						onSuccess();
					}
				} else {
					if (Ext.isFunction(onFailure)) {
						onFailure();
					}
					clientVM.displayError(response.message);
				}
			},
			failure: function(resp) {
				if (Ext.isFunction(onFailure)) {
					onFailure();
				}
				clientVM.displayFailure(resp);
			}
		});
	},
	deserializeParams: function(paramString) {
		var params = {},
			paramsSplit = paramString.split(this.choiceSeparator);

		Ext.each(paramsSplit, function(split) {
			var val = split.split(this.choiceValueSeparator);
			if (val.length > 1) {
				params[val[0]] = val[1];
			}
		}, this);

		return params;
	},
	serializeChoice: function(paramsArray) {
		var params = [];
		Ext.each(paramsArray, function(param) {
			if (param) params.push(param);
		}, this);

		return params.join(this.choiceSeparator);
	},
	getParamType: function(param) {
		var i, j, tab, column, f, field, returnType;
		for (i = 0; i < this.fieldConfiguration.length; i++) {
			tab = this.fieldConfiguration[i]
			for (j = 0; j < tab.columns.length; j++) {
				column = tab.columns[j]
				if (column) {
					for (f = 0; f < column.fields.length; f++) {
						field = column.fields[f]
						returnType = this.getParamTypeOfField(field, param)
						if (returnType) return returnType
					}
				}
			}
		}

		if (param == 'EVENT_REFERENCE' || param == 'PROBLEMID') return 'INPUT';

		//unknown field so we need to return blank here
		return '';
	},

	getParamTypeOfField: function(field, param) {
		if (field.name == param || field.configName == param) return field.sourceType
		if (field.uiType == 'SectionContainerField') {
			var sc, sectionColumn, sf, sectionField, returnType
			sectionColumns = Ext.decode(field.sectionColumns) || [];
			for (sc = 0; sc < sectionColumns.length; sc++) {
				sectionColumn = sectionColumns[sc]
				for (sf = 0; sf < sectionColumn.controls.length; sf++) {
					sectionField = sectionColumn.controls[sf]
					if (sectionField) {
						returnType = this.getParamTypeOfField(sectionField, param)
						if (returnType) return returnType
					}
				}
			}
		}
		return null
	},

	setRecordId: function(recordId, button) {
		this.recordId = recordId;

		var queryParams = this.queryString ? Ext.Object.fromQueryString(this.queryString) : {};
		if (this.recordId) Ext.apply(queryParams, {
			sys_id: this.recordId
		});

		var filter = [];
		for (var key in queryParams) {
			var field = key;
			if (key == 'u_content_request' && this.submitConfiguration.tableName != 'content_request') {
				field += '.sys_id';
			}

			filter.push({
				field: field,
				type: 'auto',
				condition: 'equals',
				value: queryParams[key]
			});
		}

		if (this.showLoading)
			Ext.MessageBox.show({
				msg: RS.formbuilder.locale.loadingMessage,
				progressText: RS.formbuilder.locale.loadingProgress,
				width: 300,
				wait: true,
				waitConfig: {
					interval: 200
				}
			});

		if (filter.length && this.submitConfiguration.tableName) {
			Ext.Ajax.request({
				url: this.API['getRecordData'],
				params: {
					tableName: this.submitConfiguration.tableName,
					type: 'form',
					start: 0,
					filter: Ext.encode(filter)
				},
				scope: this,
				success: function(r) {
					if (this.showLoading) Ext.MessageBox.hide()
					Ext.each(Ext.dom.Query.select('div[class=x-mask]'), function(el) {
						Ext.fly(el).setStyle('display', 'none');
					});
					this.doLayout();

					Ext.defer(function() {
						Ext.each(Ext.dom.Query.select('div[class=x-mask]'), function(el) {
							Ext.fly(el).setStyle('display', 'none');
						});
						this.doLayout();
					}, 500, this);

					this.processDataRequest(r, button);
				},
				failure: function(r) {
					if (this.showLoading) Ext.MessageBox.hide()
					clientVM.displayFailure(r);
				}
			})
		}
	},

	processUrlVariables: function() {
		if (!this.rendered || !this.vm) return;
		var theRootWnd = window;
		var params = {};
		var hasParams = false;
		if (this.params) {
			hasParams = true;
			params = Ext.Object.fromQueryString(this.params)
		} else {
			try {
				while (theRootWnd.parent != theRootWnd) {
					if (theRootWnd.location.search) {
						hasParams = true;
						Ext.apply(params, Ext.Object.fromQueryString(theRootWnd.location.search));
					}
					theRootWnd = theRootWnd.parent;
				}
				if (theRootWnd.location.search) {
					hasParams = true;
					Ext.apply(params, Ext.Object.fromQueryString(theRootWnd.location.search));
				}
			} catch (e) {
				//cross-site security
			}
		}

		if (hasParams) {
			for (var key in params) {
				if (Ext.isDefined(this.vm[key])) this.vm.set(key, params[key]);
			}
			//Special processing for radios/checkboxes because the bindings don't work with extjs's messed up structure, we have to manually set the values
			var components = this.query('radiogroup, checkboxgroup');
			Ext.each(components, function(component) {
				var name = component.name;
				if (!name) name = component.items.getAt(0).name;
				if (Ext.isDefined(params[name])) {
					var obj = {};
					obj[name] = params[name];
					component.setValue(obj);
				}
			});
			//Special processing for hidden variables
			var hiddenFields = this.query('hiddenfield');
			Ext.each(hiddenFields, function(hiddenField) {
				if (Ext.isDefined(params[hiddenField.name])) {
					hiddenField.setValue(params[hiddenField.name]);
				}
			});
		}
	},
	parseDependencies: function(controlConfig) {
		this.applyDependencies(controlConfig, controlConfig.dependencies);
	},
	parseButtonDependencies: function(buttonConfig) {
		var dependencies = buttonConfig.dependencies || [];
		Ext.each(buttonConfig.actions, function(action) {
			if (action.operation == 'NEXT') dependencies.push({
				target: 'activeTabIndex',
				action: 'isVisible',
				condition: 'lessThan',
				value: 'this.tabCount-1',
				template: '{0} {1} {2}'
			});
			if (action.operation == 'BACK') dependencies.push({
				target: 'activeTabIndex',
				action: 'isVisible',
				condition: 'greaterThan',
				value: '0',
				template: '{0} {1} {2}'
			});
		});
		this.applyDependencies(buttonConfig, dependencies);
	},
	applyDependencies: function(source, dependencies) {
		var actionFunctions = {},
			actionOptions = {};
		Ext.each(dependencies, function(dependency) {
			var values = (dependency.value || '').split(/[\||,]/),
				target = 'this.' + dependency.target,
				targetFormField = this.getFormControl(dependency.target),
				targetFieldType = this.getControlType(targetFormField),
				functionString = '',
				actionKey = '',
				condition = '==',
				template = dependency.template;

			switch (dependency.condition) {
				case 'isEmpty':
				case 'equals':
				case 'on':
					condition = '==';
					break;
				case 'notequals':
				case 'notEmpty':
					condition = '!=';
					break;
				case 'greaterThan':
				case 'after':
					condition = '>';
					break;
				case 'greaterThanOrEqual':
					condition = '>=';
					break;
				case 'lessThan':
				case 'before':
					condition = '<';
					break;
				case 'lessThanOrEqual':
					condition = '<=';
					break;
				case 'contains':
					template = '{0} == null ? false : {0}.toString().indexOf("{2}") > -1';
					condition = dependency.condition;
					break;
				case 'notcontains':
					template = '{0} == null ? false : {0}.toString().indexOf("{2}") == -1';
					condition = dependency.condition;
					break;
			}

			Ext.each(values, function(value, index) {
				if (index > 0) functionString += ' || ';
				var val = Ext.String.trim(value);

				if (dependency.condition === 'isEmpty' || dependency.condition === 'notEmpty') {
					val = "";
					template = '{0} {1} "{2}"';
				} else if (targetFieldType === "date") {
					var dateString = val;
					switch (dateString.toLowerCase()) {
						case 'today':
							dateString = Ext.Date.format(new Date(), 'Y-m-d');
							break;
						case 'yesterday':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, -1), 'Y-m-d');
							break;
						case 'tomorrow':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, 1), 'Y-m-d');
							break;
						case 'last week':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, -7), 'Y-m-d');
							break;
						case 'next week':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, 7), 'Y-m-d');
							break;
						case 'last month':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.MONTH, -1), 'Y-m-d');
							break;
						case 'next month':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.MONTH, 1), 'Y-m-d');
							break;
						case 'last year':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, -1), 'Y-m-d');
							break;
						case 'next year':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, 1), 'Y-m-d');
							break;
						default:
							break;
					}
					var tryDate = this.parseDates(dateString);
					if (Ext.isDate(tryDate.date)) {
						if (Ext.Date.formatContainsHourInfo(tryDate.format) && Ext.Date.formatContainsDateInfo(tryDate.format)) {
							val = Ext.String.format('Ext.Date.parse("{0}", "{1}")', dateString, tryDate.format);
							target = Ext.String.format('Ext.Date.parse({0}, "c")', target);
						} else if (Ext.Date.formatContainsHourInfo(tryDate.format)) {
							//For time we need to assume we're deal with a time field, so we need to clear the date and use the default date that sencha uses
							//initDate: [2008,0,1],
							val = Ext.String.format('Ext.Date.parse("{0}", "{1}").setFullYear(2008,0,1)', dateString, tryDate.format);
						} else if (Ext.Date.formatContainsDateInfo(tryDate.format)) {
							val = Ext.String.format('Ext.Date.clearTime(Ext.Date.parse("{0}", "{1}"))', dateString, tryDate.format);
						}
						template = Ext.Date.formatContainsHourInfo(tryDate.format) ? '{0} {1} {2}' : (condition == '==' ? '(!Ext.isDate({0}) ? false : Ext.Date.clearTime({0}) >= {2} && Ext.Date.clearTime({0}) <= {2})' : '(!Ext.isDate({0}) ? false : Ext.Date.clearTime({0}) {1} {2})');
					}
				} else if (targetFieldType === "bool" && (val.toLowerCase() === 'true' || val.toLowerCase() === 'false')) {
					template = '{0} {1} {2} ' + (dependency.condition == 'notequals' ? '&&' : '||') + ' {0} {1} \"{2}\"';
					val = val.toLowerCase();
				} else if (targetFieldType === "number" && Ext.isNumeric(val) && (Ext.Array.indexOf(['==', '!=', '<', '<=', '>', '>='], condition) > -1)) {
					val = Number(val);
					template = '{0} {1} {2}';
				}

				if (!template) template = '{0} {1} "{2}"';

				functionString += Ext.String.format(template, target, condition, val);
			}, this);

			switch (dependency.action) {
				case 'isEnabled':
					actionKey = 'IsEnabled$';
					break;
				case 'isVisible':
					actionKey = 'IsVisible$';
					break;
				case 'setBgColor':
					actionKey = 'SetBackgroundColor$';
					break;
				default:
					break;
			}

			if (actionKey) {
				if (actionFunctions[actionKey]) {
					if (actionKey == 'SetBackgroundColor$') {
						var count = 1;
						while (actionFunctions[actionKey]) {
							actionKey = Ext.String.format('SetBackgroundColor{0}$', count);
							count++;
						}
						actionFunctions[actionKey] += ' ( ' + functionString + ' ) ';
						actionOptions[actionKey] = dependency.actionOptions;
					} else {
						if (actionFunctions[actionKey].length > 0) actionFunctions[actionKey] += ' && ';
						actionFunctions[actionKey] += ' ( ' + functionString + ' ) ';
						actionOptions[actionKey] = dependency.actionOptions;
					}
				} else {
					actionFunctions[actionKey] = ' ( ' + functionString + ' ) ';
					actionOptions[actionKey] = dependency.actionOptions;
				}
			}
		}, this);

		for (var key in actionFunctions) {
			if (key.indexOf('SetBackgroundColor') > -1) {
				this.viewmodelSpec[source.name + key] = new Function('return (' + actionFunctions[key] + ') ? "' + actionOptions[key] + '" : "transparent"');
				source.fieldStyle = 'background-color:@{' + source.name + key.substring(0, key.length - 1) + '}';
			} else this.viewmodelSpec[source.name + key] = new Function('return ' + actionFunctions[key]);
		}
	},

	getAllFormControls: function(fieldName) {
		var self = this,
			i = 0,
			config = self.formConfiguration,
			controls = [];
		if (config) {
			if (config.panels[0].tabs.length > 1) { //User checked the "use tabs" checkbox
				var tabItems = [],
					tabs = config.panels[0].tabs;
				for (i = tabs.length - 1; i > 0; i--) {
					controls.concat(tabs[i].columns[0].fields); //concatenate each fields array into a single controls array.
				}
			} else if (config.panels[0].tabs[0].columns.length > 0) { //more common use case - no tabs. //check that there's at least one column before proceeidng
				controls = config.panels[0].tabs[0].columns[0].fields; //set the controls = to these fields, as they are all there is in this form.
				/*Ext.each(controls, function(control){
					//testFormControl
				});*/
			}
		}
		return controls;
	},

	getFormControl: function(fieldName) {
		var matchingControl = null,
			i = 0,
			k = 0,
			controls = this.getAllFormControls(),
			control = null;
		loopOverControls: for (i = controls.length; i--;) {
			control = controls[i];
			if (control.uiType === 'SpacerField') { //ignore space fields
				continue loopOverControls;
			} else if (false /*control.columns*/ ) { //if there are formfields nested in this control (i.e. it's a section) iterate through those and check them.
				/*control.columns.foreach(function(col) {
					col.controls.foreach(function(subControl) {}
				}*/
			} else if (control.name === fieldName) { //otherwise check the current field in the iteration to see if the names match. if so return it.
				matchingControl = control;
				break loopOverControls;
			}
		}
		return matchingControl;
	},

	getControlType: function(control) {
		var type = '';
		if (control) {
			switch (control.uiType) {
				case 'CheckBox':
				case 'MultipleCheckBox':
					type = 'bool';
					break;
				case 'NumberTextField':
				case 'DecimalTextField':
					type = 'number';
					break;
				case 'Date':
				case 'DateTime':
					type = 'date';
					break;
				default:
					type = 'string';
					break;
			}
		}
		return type;
	},

	parseDates: function(dateString) {
		var formats = ['Y-m-d', 'g:i a', 'g:i A', 'G:i', 'Y-m-d g:i a', 'Y-m-d g:i A', 'Y-m-d G:i'],
			returnVal = {
				date: '',
				format: ''
			};

		Ext.each(formats, function(format) {
			var date = Ext.Date.parse(dateString, format);
			if (Ext.isDate(date)) {
				returnVal = {
					date: date,
					format: format
				};
				return false;
			}
		});
		return returnVal;
	}
});
