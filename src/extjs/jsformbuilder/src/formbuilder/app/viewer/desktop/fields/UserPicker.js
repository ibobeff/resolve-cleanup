/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.UserPicker', {
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.userpickerfield',

	displayField: 'name',
	valueField: 'value',
	queryMode: 'local',

	meValue: 'me',

	allowAssignToMe: false,
	autoAssignToMe: false,
	// forceSelection: true,

	//user trigger
	userCls: 'icon-user', //Ext.baseCSSPrefix + 'form-user-trigger',
	userClick: function() {
		this.setValue(this.meValue);
	},

	//Clear Trigger
	clearCls: Ext.baseCSSPrefix + 'form-clear-trigger',
	clearClick: function() {
		//clear the current value and hide the clear button
		this.clearing = true;
		this.setValue('', true);
		this.triggerEl.item(0).parent().setDisplayed('none');
	},

	/**
	 * Pipe separated list of group id's to filter the users with.  For example 'id123|id456'
	 */
	groups: '',
	usersOfTeams: '',
	recurseUsersOfTeam: false,
	teamsOfTeams: '',
	recurseTeamsOfTeam: false,

	stillLoading: false,

	initComponent: function() {
		this.trigger2Cls = this.triggerCls;
		this.onTrigger2Click = this.onTriggerClick;
		this.trigger1Cls = this.clearCls;
		this.onTrigger1Click = this.clearClick;
		this.trigger3Cls = this.userCls;
		this.trigger3OverCls = this.userCls;
		this.onTrigger3Click = this.userClick;

		this.store = Ext.create('Ext.data.Store', {
			fields: ['name', 'value', 'type'],
			proxy: {
				type: 'memory',
				reader: {
					type: 'json',
					root: 'records'
				}
			}
		});

		this.stillLoading = true;
		Ext.Ajax.request({
			url: '/resolve/service/form/getUsers',
			jsonData: {
				groups: this.groups || '',
				usersOfTeams: this.usersOfTeams || '',
				recurseUsersOfTeam: this.recurseUsersOfTeam,
				teamsOfTeams: this.teamsOfTeams,
				recurseTeamsOfTeam: this.recurseTeamsOfTeam
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (!response.success) {
					clientVM.displayError(response.message);
					return;
				}
				this.store.add(response.records);
				this.stillLoading = false;
				this.setValueActual(this.tempValue);
				if (this.autoAssignToMe && !this.getValue()) this.setValue(this.meValue);
				//if there is only one team, then auto assign to that team, overriding the auto-assign-to-me option
				var assignToGroup = '';
				this.store.each(function(record) {
					if (record.data.type == 'team') {
						if (!assignToGroup) assignToGroup = record.data.value;
						else {
							assignToGroup = false;
							return false;
						}
					}
				})
				if (assignToGroup && !this.getValue()) {
					this.setValue(assignToGroup);
				}
			},
			failure: function(r){
				clientVM.displayFailure(r);
			}
		});


		this.callParent();
	},
	onRender: function() {
		this.callParent(arguments);
		if (this.readOnly) {
			this.getEl().select('input').setStyle({
				border: '0px solid transparent',
				backgroundImage: 'none',
				cursor: 'auto'
			});
		}
		var tip = Ext.create('Ext.tip.ToolTip', {
			target: this.triggerEl.item(2),
			html: RS.formbuilder.locale.assignToMe
		});
		this.triggerEl.item(0).parent().setDisplayed('none');
		if (!this.allowAssignToMe) this.triggerEl.item(2).parent().setDisplayed('none');
		else {
			this.triggerEl.item(2).addCls('icon-large rs-icon rs-no-background')
		}
	},
	//snapshot of current value when the control is still loading the field
	tempValue: '',
	setValue: function(value, clear) {
		if (this.stillLoading) {
			this.tempValue = value;
			return;
		}
		this.callParent(arguments);
		if (this.rendered && this.value && !this.mandatory && !this.readOnly) {
			this.triggerEl.item(0).parent().setDisplayed('table-cell');
			this.doComponentLayout();
		}

		if (this.rendered && !this.value && !clear) {
			if (this.clearing) {
				this.clearing = false;
				return;
			}
			//Being passed null because we're being reset.  Run through the default logic again
			if (this.autoAssignToMe === null) {
				Ext.defer(function() {
					this.setValue(this.meValue);
				}, 100, this)
				return;
			}
			//if there is only one team, then auto assign to that team, overriding the auto-assign-to-me option
			var assignToGroup = '';
			this.store.each(function(record) {
				if (record.data.type == 'team') {
					if (!assignToGroup) assignToGroup = record.data.value;
					else {
						assignToGroup = false;
						return false;
					}
				}
			});
			if (assignToGroup && !this.getValue()) {
				this.setValue(assignToGroup);
			}
		}
	}
});