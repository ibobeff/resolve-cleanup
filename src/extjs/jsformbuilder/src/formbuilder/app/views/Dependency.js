glu.defView('RS.formbuilder.Dependency', {
	title: '@{title}',
	modal: true,
	width: 600,
	height: 250,
	padding : 15,
	cls : 'rs-modal-popup',	
	border: false,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	items: [{
		xtype: 'combobox',
		name: 'action',
		store: '@{dependencyActionStore}',
		displayField: 'name',
		valueField: 'value',
		forceSelection: true,
		editable: false,
		selectOnTab: true,
		triggerAction: 'all',
		queryMode: 'local'
	}, {
		xtype: 'colorpickercombo',
		name: 'color'
	}, {
		xtype: 'combobox',
		name: 'target',
		store: '@{dependencyTargetStore}',
		displayField: 'name',
		valueField: 'value',
		forceSelection: true,
		editable: false,
		selectOnTab: true,
		triggerAction: 'all',
		queryMode: 'local'
	}, {
		xtype: 'combobox',
		name: 'condition',
		store: '@{dependencyConditionStore}',
		displayField: 'name',
		valueField: 'value',
		forceSelection: true,
		editable: false,
		selectOnTab: true,
		triggerAction: 'all',
		queryMode: 'local',
		listConfig: {
			minWidth: 150
		},
		listeners: {
			expand: function(field) {
				field.store.load()
			}
		}
	}, {
		xtype: 'textfield',
		name: 'stringValue'
	}/*, {
		xtype: 'numberfield',
		name: 'numberValue'
	}, {
		xtype: 'datefield',
		name: 'dateValue'
	}, {
		xtype: 'textfield',
		name: 'booleanValue'
	}*/],
	buttons: [{
		cls : 'rs-med-btn rs-btn-dark',
		name: 'applyDependency'
	}, {
		cls : 'rs-med-btn rs-btn-light',
		name: 'cancel'
	}]	
});