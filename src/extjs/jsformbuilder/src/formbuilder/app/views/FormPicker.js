glu.defView('RS.formbuilder.FormPicker', {
	asWindow: {
		title: '@{title}',
		cls : 'rs-modal-popup',
		padding : 15,
		modal: true,
		listeners: {
			beforeshow: function() {
				clientVM.setPopupSizeToEightyPercent(this);
			}
		}
	},
	layout: 'fit',
	buttons: [{
		name :'select',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'createForm',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}],
	items: [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'forms',
		columns: '@{formsColumns}',
		plugins: [{
			ptype: 'pager'
		}],
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',			
			items: [{
				xtype: 'textfield',
				name: 'searchQuery',			
				width : 400				
			}]
		}]
	}]
})