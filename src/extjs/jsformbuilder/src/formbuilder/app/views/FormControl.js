Ext.define('RS.formbuilder.Control.Panel', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.formfieldcontrolpanel',
	config: {
		unsupportedField: false
	},
	setUnsupportedField: function(unsupported) {
		if (unsupported) {
			this.addCls('unsupported_formfieldcontrolpanel');
		} else {
			this.removeCls('unsupported_formfieldcontrolpanel');
		}
	}
});

/**
 * Factory definition for Form Controls to render as a preview in the form
 * Adds a remove button and overall wrapper template to each control so that we can drag-n-drop the controls as well as remove them
 */
RS.formbuilder.views.FormControlLayoutFactory = function(subView) {
	var config = {}

	if (Ext.Array.indexOf(subView.vm.mixins, 'FormControl') >= 0) {
		glu.applyIf(config, {
			flex: 1,
			labelWidth: 125,
			fieldLabel: {
				value: '@{fieldLabel}',
				trigger: 'dblclick',
				field: {
					xtype: 'textfield'								
				}				
			},
			readOnly: '@{readOnly}',
			disabled: '@{hidden}',
			labelSeparator: '@{mandatoryLabelSeparator}',
			value: '@{defaultValue}',
			labelAlign: '@{labelAlign}'			
		});
	}

	if (Ext.Array.indexOf(subView.vm.mixins, 'SizeControl') >= 0) {
		glu.applyIf(config, {
			height: '@{actualHeight}',
			width: '@{width}'
		});
	}	

	glu.applyIf(subView, config);
	var items = [subView, {
		xtype: 'button',
		icon: '/resolve/images/remove.png',
		hidden: true,
		name: 'removeControl'
	}];

	if (Ext.Array.indexOf(subView.vm.mixins, 'ButtonControl') >= 0) {
		items.splice(1, 0, {
			xtype: 'displayfield',
			flex: 1
		});
	}

	return {
		xtype: 'formfieldcontrolpanel',
		cls: subView.vm.isInSection ? 'formControlSectionContainer' : 'formControlContainer',
		unsupportedField: '@{unsupportedField}',
		bodyCls: 'formControlContainerBody',
		padding: '5px',
		layout: {
			type: 'hbox',
			align: 'middle'
		},
		items: items,
		border: false
	}
};

/**
 * Template definition for the FormControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.formControlTemplate = [{
	xtype: 'textfield',
	name: 'fieldLabel',
	listeners: {
		focus: '@{focusFieldLabel}',
		blur: '@{blurFieldLabel}'
	}	
}, {
	xtype: 'combobox',
	name: 'labelAlign',
	editable: false,
	forceSelection: true,
	queryMode: 'local',
	displayField: 'text',
	valueField: 'value',
	store: '@{labelAlignOptionsList}'
}, {
	xtype: 'checkbox',
	name: 'readOnly',
	hideLabel : true,
	boxLabel : '~~readOnly~~',
	margin : '0 0 0 130'
}, {
	xtype: 'checkbox',
	name: 'hidden',
	hideLabel : true,
	boxLabel : '~~hidden~~',
	margin : '0 0 0 130'
}];

/**
 * Template definition for the SizeControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.formControlSizeTemplate = [{
	xtype: 'fieldset',
	padding : '0 10 5 10',
	defaults: {
		labelWidth: 115
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	checkboxToggle: true,
	title: '~~Size~~',
	collapsed: '@{collapsed}',
	collapsible: true,
	items: [{
		xtype: 'numberfield',
		minValue: 125,
		maxValue: 9999999,
		minValue: 0,
		name: 'width'
	}, {
		xtype: 'numberfield',
		minValue: 22,
		maxValue: 9999999,
		minValue: 0,
		name: 'height'
	}]
}];

/**
 * Template definition for the RequireableControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.requireableControlTemplate = [{
	xtype: 'checkbox',
	name: 'mandatory',
	hideLabel : true,
	boxLabel : '~~mandatory~~',
	margin : '0 0 0 130'
}];

RS.formbuilder.tooltipControlTemplate = [{
	xtype: 'textfield',
	name: 'tooltip',
	maxLength: 500
}];

/**
 * Template definition for the RequireableControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.encryptableControlTemplate = [{
	xtype: 'checkbox',
	name: 'encrypted',
	hideLabel : true,
	boxLabel : '~~encrypted~~',
	margin : '0 0 0 130'
}];

/**
 * Template definition for the DefaultValueControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.defaultValueTemplate = [{
	xtype: 'textfield',
	name: 'defaultValue',
	maxLength: 1000
}];

RS.formbuilder.defaultValueBooleanTemplate = [{
	xtype: 'checkbox',
	name: 'defaultValue',
	hideLabel : true,
	boxLabel : '~~defaultValue~~',
	margin : '0 0 0 130'
}];

/**
 * Template definition for the StringControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.stringRangeTemplate = [{
	xtype: 'fieldset',
	defaults: {
		labelWidth: 115
	},
	title: '~~dbTypeParams~~',
	hidden: '@{!columnSizeIsVisible}',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'combobox',
		name: 'columnSize',
		store: '@{columnSizeStore}',
		readOnly: '@{columnSizeIsReadOnly}',
		displayField: 'name',
		valueField: 'value',
		editable: false,
		forceSelection: true
	}]
}, {
	xtype: 'fieldset',
	padding : '0 10 5 10',
	defaults: {
		labelWidth: 115
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	title: '~~Range~~',
	items: [{
		xtype: 'numberfield',
		name: 'minLength'
	}, {
		xtype: 'numberfield',
		name: 'maxLength'
	}]
}];
/**
 * Template definition for the NumberControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.numberRangeTemplate = [{
	xtype: 'numberfield',
	allowDecimals: false,
	name: 'defaultValue'
}, {
	xtype: 'fieldset',
	padding : '0 10 5 10',
	defaults: {
		labelWidth: 115
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	title: '~~Range~~',
	items: [{
		xtype: 'numberfield',
		allowDecimals: false,
		name: 'minValue'
	}, {
		xtype: 'numberfield',
		allowDecimals: false,
		name: 'maxValue',
		maxValue: '@{allowedMaxValue}'
	}]
}];

/**
 * Template definition for the DecimalControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.decimalRangeTemplate = [{
	xtype: 'numberfield',
	allowDecimals: true,
	decimalPrecision: 5,
	name: 'defaultValue'
}, {
	xtype: 'fieldset',
	defaults: {
		labelWidth: 117
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	title: '~~Range~~',
	items: [{
		xtype: 'numberfield',
		allowDecimals: true,
		name: 'minValue'
	}, {
		xtype: 'numberfield',
		allowDecimals: true,
		name: 'maxValue',
		maxValue: '@{allowedMaxValue}'
	}]
}];
/**
 * Template definition for the ChoiceControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.choiceRadioTemplate = [{
	xtype: 'numberfield',
	name: 'columns',
	maxValue: 10,
	minValue: 0
}, {
	xtype: 'radiogroup',
	fieldLabel: '~~optionType~~',
	labelWidth : 125,
	hidden: '@{!displayOptionsSelections}',
	items: [{
		xtype: 'radio',
		name: 'optionType',
		value: '@{simple}',
		boxLabel: '~~simple~~'
	}, {
		xtype: 'radio',
		name: 'optionType',
		value: '@{advanced}',
		boxLabel: '~~advanced~~'
	}, {
		xtype: 'radio',
		name: 'optionType',
		value: '@{custom}',
		boxLabel: '~~custom~~'
	}]
}, {
	layout: 'card',
	border: false,
	activeItem: '@{activeOption}',
	items: [{
		xtype: 'fieldset',		
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		title: '~~Options~~',
		items: '@{options}',
		itemTemplate: {
			border: false,
			layout: 'hbox',
			bodyPadding: '3px',
			defaults : {
				margin : '0 5 0 0'
			},
			items: [{
				xtype: 'radio',
				name: '@{..tempId}',
				inputValue: '@{id}',
				checked: '@{isSelected}',			
			}, {
				flex: 1,
				xtype: 'textfield',
				value: '@{text}',
				maxLength: 60
			}, {
				xtype: 'button',
				name: 'addOption',
				cls : 'rs-small-btn rs-btn-dark',
			}, {
				xtype: 'button',
				margin :0,
				hidden: '@{!..optionsGreaterThanOne}',
				name: 'removeOption',
				cls : 'rs-small-btn rs-btn-light',
			}]
		}
	}, {
		xtype: 'panel',
		border: false,
		layout: 'anchor',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaults : {
			labelWidth : 125
		},
		items: [{
			xtype: 'combobox',
			name: 'dropdownChoiceTable',
			queryMode: 'local',
			store: '@{..customTablesList}',
			displayField: 'uname',
			valueField: 'umodelName'
		}, {
			xtype: 'combobox',
			name: 'displayField',
			store: '@{tableColumnStore}',
			queryMode: 'local',
			displayField: 'displayName',
			valueField: 'name'
		}, {
			xtype: 'combobox',
			name: 'valueField',
			store: '@{tableColumnStore}',
			queryMode: 'local',
			displayField: 'displayName',
			valueField: 'name'
		}, {
			xtype: 'clearablecombobox',
			name: 'where',
			store: '@{tableColumnStore}',
			queryMode: 'local',
			displayField: 'displayName',
			valueField: 'name'
		}, {
			xtype: 'clearablecombobox',
			name: 'parentField',
			store: '@{fieldStore}',
			queryMode: 'local',
			displayField: 'name',
			valueField: 'value',
			listeners: {
				expand: function(combo) {
					combo.getStore().load();
				}
			}
		}]
	}, {
		xtype: 'textarea',
		labelWidth : 125,
		name: 'sqlQuery'
	}]
}, {
	layout: {
		type: 'hbox',
		pack: 'center',
		align: 'middle'
	},
	padding: '0 0 10 0',
	border: false,
	items: [
		{
			xtype: 'button',
			name: 'importOptions',
			cls : 'rs-small-btn rs-btn-dark'
		}
	]
}];

/**
 * Template definition for the MultiChoiceControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.choiceCheckTemplate = [{
	xtype: 'numberfield',
	name: 'columns',
	maxValue: 10
}, {
	xtype: 'fieldset',	
	padding : '0 10 5 10',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	title: '~~Options~~',
	items: '@{options}',
	itemTemplate: {
		border: false,
		layout: 'hbox',
		bodyPadding: '3px',
		defaults: {		
			margin: '0 5 0 0'
		},
		items: [{
			xtype: 'checkbox',
			value: '@{isChecked}'
		}, {
			flex: 1,
			xtype: 'textfield',
			value: '@{text}',
			maxLength: 60			
		}, {
			xtype: 'button',
			name: 'addOption',
			cls : 'rs-small-btn rs-btn-dark'		
		}, {
			xtype: 'button',
			margin : 0,
			hidden: '@{!..optionsGreaterThanOne}',
			cls : 'rs-small-btn rs-btn-light',
			name: 'removeOption'
		}]
	}
}, {
	layout: {
		type: 'hbox',
		pack: 'center',
		align: 'middle'
	},
	border: false,
	items: [{
		xtype: 'button',
		name: 'importOptions',
		cls : 'rs-small-btn rs-btn-dark',
	}]
}];
/**
 * Template definition for the DateControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.dateTemplate = [{
	xtype: 'datefield',
	name: 'defaultValue'
}];
/**
 * Template definition for the TimeControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.timeTemplate = [{
	xtype: 'timefield',
	name: 'defaultValue',
	store: ''
}];
/**
 * Template definition for the DateTimeControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.dateTimeTemplate = [{
	xtype: 'datefield',
	name: 'date'
}, {
	xtype: 'timefield',
	name: 'time',
	store: ''
}];
/**
 * Template definition for the ButtonControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.buttonTemplate = [{
	xtype: 'textfield',
	fieldLabel: '~~text~~',
	name: 'operation'
}, {
	xtype: 'grid',
	forceFit: true,
	store: '@{actionsStore}',
	name: 'actions',
	tbar: [{
		name: 'addAction'
	}, {
		name: 'editAction'
	}, {
		name: 'removeAction'
	}],
	columns: [{
		dataIndex: 'operationName',
		header: '~~operation~~'
	}, {
		dataIndex: 'value',
		header: '~~value~~'
	}]
}]
/**
 * Template definition for the SequenceControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.sequenceTemplate = [{
	xtype: 'textfield',
	name: 'prefix'
}]
/**
 * Template definition for the DatabaseControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.databaseTemplate = [{
		xtype: 'radiogroup',
		fieldLabel: '~~type~~',
		items: [{
			xtype: 'radio',
			name: 'inputType',
			inputValue: 'INPUT',
			boxLabel: '~~input~~',
			value: '@{inputTypeSelected}',
			disabled: '@{!inputTypeIsEnabled}'
		}, {
			xtype: 'radio',
			name: 'inputType',
			inputValue: 'DB',
			boxLabel: '~~db~~',
			value: '@{dbTypeSelected}',
			disabled: '@{!dbTypeIsEnabled}'
		}]
	}, {
		xtype: 'combo',
		name: 'column',
		editable: '@{columnIsEditable}',
		queryMode: 'local',
		displayField: 'name',
		valueField: 'name',
		keyDelay: 1,
		store: '@{..columnStore}',
		hidden: '@{hideColumnCombobox}',
		displayTpl: '<tpl for=".">{[typeof values === "string" ? values : values["name"] === "newColumn" ? "u_" : ((values["name"] ? values["name"] : values["displayName"]))]}</tpl>',
		tpl: new Ext.XTemplate('<ul><tpl for=".">', '<li role="option" class="x-boundlist-item" <tpl if="this.showDisabled(values)">style="color:#888888" </tpl> >', '{[values["name"] === "newColumn" ? "**" + values["displayName"] + "**" : (values["name"] + " [" + values["displayName"] + "]")]}</li>', '</tpl></ul>', {
			showDisabled: function(values) {
				if (values.name === 'newColumn') return false;
				// console.log('ui type is ' + values.uiType + ' and model type is ' + values.modelType );
				return values.modelType().indexOf(values.uiType) == -1;
			}
		}),
		listeners: {
			blur: '@{blurDatabaseFieldName}'
		}
	}, {
		xtype: 'textfield',
		name: 'inputName',
		keyDelay: 1,
		hidden: '@{!hideColumnCombobox}'
	}
];
/**
 * Template definition for the UserPickerControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.groupPickerTemplate = [{
	xtype: 'checkbox',
	name: 'allowAssignToMe',
	boxLabel : '~~allowAssignToMe~~',
	margin : '0 0 0 130',
	hideLabel : true
}, {
	xtype: 'checkbox',
	name: 'autoAssignToMe',
	boxLabel : '~~autoAssignToMe~~',
	margin : '0 0 0 130',
	hideLabel : true
}, {
	xtype: 'fieldset',
	defaults: {
		labelWidth: 115
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	title: '~~userFilter~~',
	items: [{
		xtype: 'combo',
		editable: false,
		forceSelection: true,
		queryMode: 'local',
		displayField: 'name',
		valueField: 'value',
		multiSelect: true,
		name: 'groups',
		store: '@{groupStore}'
	}, {
		xtype: 'combo',
		editable: false,
		forceSelection: true,
		queryMode: 'local',
		displayField: 'name',
		valueField: 'value',
		multiSelect: true,
		name: 'teams',
		store: '@{teamStore}'
	}, {
		xtype: 'checkbox',
		name: 'recurseThroughTeams',
		hideLabel: true,
		margin: '0 0 0 120',
		boxLabel: '~~recurseThroughTeams~~'
	}]
}, {
	xtype: 'fieldset',
	padding : '0 10 5 10',
	defaults: {
		labelWidth: 115
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	title: '~~teamFilter~~',
	items: [{
			xtype: 'combo',
			editable: false,
			forceSelection: true,
			queryMode: 'local',
			displayField: 'name',
			valueField: 'value',
			multiSelect: true,
			name: 'teamList',
			store: '@{teamStore}'
		}
	]
}];

RS.formbuilder.teamPickerTemplate = [{
	xtype: 'fieldset',
	defaults: {
		labelWidth: 115
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	title: '~~teamFilter~~',
	items: [{
		xtype: 'combo',
		editable: false,
		forceSelection: true,
		queryMode: 'local',
		displayField: 'name',
		valueField: 'value',
		multiSelect: true,
		name: 'teamPickerTeamsOfTeams',
		store: '@{teamStore}'
	}, {
		xtype: 'checkbox',
		name: 'recurseThroughTeamsList',
		hideLabel: true,
		padding: '0 0 0 120',
		boxLabel: '~~recurseThroughTeams~~'
	}]
}];

RS.formbuilder.nameControlTemplate = [{
	xtype: 'fieldset',
	defaults: {
		labelWidth: 125
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	title: '~~nameOptions~~',
	defaults : {
		hideLabel : true,
	},
	items: [{
		xtype: 'checkbox',
		name: 'showFirstName',
		boxLabel : '~~showFirstName~~'
	}, {
		xtype: 'checkbox',
		name: 'showMiddleInitial',
		boxLabel : '~~showMiddleInitial~~'
	}, {
		xtype: 'checkbox',
		name: 'showMiddleName',
		boxLabel : '~~showMiddleName~~'
	}, {
		xtype: 'checkbox',
		name: 'showLastName',
		boxLabel : '~~showLastName~~'
	}]
}];

RS.formbuilder.dependencyControlTemplate = [{
	xtype: 'grid',
	cls : 'rs-grid-dark',
	title: '~~dependencies~~',
	name: 'dependencies',
	flex: 1,
	minHeight: 250,
	autoScroll : true,
	store: '@{dependenciesStore}',
	forceFit: true,
	selModel: {
		selType: 'rowmodel'
	},
	dockedItems : [{
		xtype : 'toolbar',
		cls : 'rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items :  ['addDependency','editDependency','removeDependency']	
	}],
	columns: [{
		dataIndex: 'actionDisplay',
		header: '~~dependencyAction~~'
	}, {
		dataIndex: 'target',
		header: '~~target~~'
	}, {
		dataIndex: 'conditionDisplay',
		header: '~~condition~~'
	}, {
		dataIndex: 'value',
		header: '~~value~~',
		renderer: function(value) {
			return Ext.util.Format.htmlEncode(value);
		}
	}],
	listeners: {
		itemdblclick: '@{depDoubleClick}'
	}
}];

RS.formbuilder.addressControlTemplate = [{
	name: 'country',
	xtype: 'combobox',
	store: '@{countryStore}',
	queryMode: 'local',
	displayField: 'name',
	valueField: 'code',
	forceSelection: true,
	editable: false
}, {
	name: 'state',
	xtype: 'textfield'
}];

RS.formbuilder.allowInputControlTemplate = [{
	xtype: 'checkbox',
	name: 'allowUserInput',
	hideLabel : true,
	boxLabel : '~~allowUserInput~~',
	margin : '0 0 0 130'
}];

RS.formbuilder.referenceControlTemplate = [{
	xtype: 'combobox',
	name: 'referenceTable',
	store: '@{customTablesList}',
	queryMode: 'local',
	displayField: 'name',
	valueField: 'value',
	forceSelection: true,
	editable: false,
	listeners: {
		expand: function(combo) {
			combo.store.clearFilter();
		}
	}
}, {
	xtype: 'combobox',
	name: 'referenceDisplayColumn',
	store: '@{referenceColumnStore}',
	queryMode: 'local',
	displayField: 'displayName',
	valueField: 'name',
	forceSelection: true,
	editable: false
}, {
	xtype: 'textfield',
	name: 'referenceTarget',
	listeners: {
		render: function(field) {
			Ext.create('Ext.tip.ToolTip', {
				target: field.getEl(),
				html: RS.formbuilder.locale.referenceTargetTooltip
			});
		}
	}
}];

RS.formbuilder.linkTemplate = [{
	xtype: 'combobox',
	name: 'linkTarget',
	store: '@{linkTargetStore}',
	queryMode: 'local',
	displayField: 'name',
	valueField: 'name',
	editable: true
}];

RS.formbuilder.fileUploadControlTemplate = [{
	xtype: 'textfield',
	name: 'fieldLabel',
	labelWidth: 125
}, {
	xtype: 'checkbox',
	name: 'allowUpload',
	hideLabel : 'true',
	margin : '0 0 0 130',
	boxLabel : '~~allowUpload~~'
}, {
	xtype: 'checkbox',
	name: 'allowRemove',
	hideLabel : 'true',
	margin : '0 0 0 130',
	boxLabel : '~~allowRemove~~'
}, {
	xtype: 'checkbox',
	name: 'allowDownload',
	hideLabel : 'true',
	margin : '0 0 0 130',
	boxLabel : '~~allowDownload~~'
}, {
	xtype: 'textfield',
	name: 'allowedFileTypes'	
}];

RS.formbuilder.referenceTableControlTemplate = [{
	xtype: 'grid',
	cls : 'rs-grid-dark',
	title: '~~referenceTabs~~',
	store: '@{referenceTablesStore}',
	forceFit: true,
	name: 'referenceTables',
	padding: '0 0 10 0',
	dockedItems :[{
		xtype : 'toolbar',
		cls : 'rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items :  ['addReference','removeReference']
	}],
	listeners: {
		render: function(grid) {
			grid.store.on('cellSelectionVMChanged', function(vm, selections) {
				var editor = this.getPlugin('cellediting');
				if (editor && selections.length == 1) {
					var r = this.store.indexOf(selections[0]);
					editor.cancelEdit();
					editor.startEditByPosition({
						row: r,
						column: 0
					});
				}
			}, grid);
		}
	},
	selModel: {
		selType: 'cellmodel'
	},
	plugins: [{
		ptype: 'cellediting',
		pluginId: 'cellediting',
		clicksToEdit: 1
	}],
	columns: [{
		header: '~~referenceTabTitle~~',
		dataIndex: 'fieldLabel',
		hideable: false,
		editor: {
			allowBlank: false
		}
	}]
}, {
	xtype : 'fieldset',
	title : '~~buttonSet~~',
	padding : '0 10 5 10',
	layout : {
		type : 'hbox'
	},
	defaults : {
		hideLabel : true,
		margin : '0 15 0 0'
	},
	items : [{
		xtype: 'checkbox',
		name: 'allowReferenceTableAdd',
		boxLabel : '~~allowReferenceTableAdd~~'		
	}, {
		xtype: 'checkbox',
		name: 'allowReferenceTableRemove',
		boxLabel : '~~allowReferenceTableRemove~~'
	}, {
		xtype: 'checkbox',
		name: 'allowReferenceTableView',
		boxLabel : '~~allowReferenceTableView~~'
	}]
}, {
	xtype: 'combobox',
	name: 'referenceTable',
	store: '@{referenceStore}',
	queryMode: 'local',
	displayField: 'value',
	valueField: 'value',
	forceSelection: true,
	editable: false
}, {
	xtype: 'combobox',
	name: 'referenceColumns',
	multiSelect: true,
	store: '@{referenceColumnStore}',
	queryMode: 'local',
	displayField: 'displayName',
	valueField: 'name',
	forceSelection: true,
	editable: false
}, {
	xtype: 'textfield',
	name: 'referenceTableUrl'
},{
	xtype : 'fieldset',
	padding : '0 10 5 10',
	checkboxToggle : true,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	collapsed : '@{!referenceTableViewPopup}',
	collapsible : true,
	title : '~~referenceTableViewPopup~~',
	defaults : {
		labelWidth : 115
	},
	items : [{
		xtype: 'textfield',
		name: 'referenceTableViewPopupTitle'
	}, {
		xtype: 'numberfield',
		minValue: 0,
		maxValue: 4000,
		name: 'referenceTableViewPopupWidth'
	}, {
		xtype: 'numberfield',
		minValue: 0,
		maxValue: 4000,
		name: 'referenceTableViewPopupHeight'
	}]
}];

RS.formbuilder.tabContainerControlTemplate = [{
	xtype: 'grid',
	cls : 'rs-grid-dark',
	title: '~~referenceTabs~~',
	store: '@{referenceTablesStore}',
	forceFit: true,
	name: 'referenceTables',
	padding: '0 0 5 0',
	dockedItems: [{
		xtype : 'toolbar',
		cls : 'rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items : [{
			text: '~~addReference~~',
			menu: {
				items: [{
					name: 'addReferenceTable'
				}, {
					name: 'addFileManager'
				}, {
					name: 'addURL'
				}]
			}
		}, {
			name: 'removeReference'
		}]
	}],
	listeners: {
		render: function(grid) {
			grid.store.on('cellSelectionVMChanged', function(vm, selections) {
				var editor = this.getPlugin('cellediting');
				if (editor && selections.length == 1) {
					var r = this.store.indexOf(selections[0]);
					editor.cancelEdit();
					editor.startEditByPosition({
						row: r,
						column: 0
					});
				}
			}, grid);
		}
	},
	selModel: {
		selType: 'cellmodel'
	},
	plugins: [{
		ptype: 'cellediting',
		pluginId: 'cellediting',
		clicksToEdit: 1
	}],
	columns: [{
		header: '~~referenceTabTitle~~',
		dataIndex: 'fieldLabel',
		hideable: false,
		editor: {
			allowBlank: false
		}
	}]
}, {
	xtype : 'fieldset',
	hidden : '@{buttonSetIsHidden}',
	title : '~~buttonSet~~',
	padding : '0 10 5 10',
	layout : {
		type : 'hbox'
	},
	defaults : {
		hideLabel : true,
		margin : '0 15 0 0'
	},
	items : [{
		xtype: 'checkbox',
		name: 'allowReferenceTableAdd',
		boxLabel : '~~allowReferenceTableAdd~~'		
	}, {
		xtype: 'checkbox',
		name: 'allowReferenceTableRemove',
		boxLabel : '~~allowReferenceTableRemove~~'
	}, {
		xtype: 'checkbox',
		name: 'allowReferenceTableView',
		boxLabel : '~~allowReferenceTableView~~'
	}]
},{
	xtype: 'combobox',
	labelWidth: 125,
	name: 'referenceTable',
	store: '@{referenceStore}',
	queryMode: 'local',
	displayField: 'value',
	valueField: 'value',
	forceSelection: true,
	editable: false
}, {
	xtype: 'combobox',
	labelWidth: 125,
	name: 'referenceColumns',
	multiSelect: true,
	store: '@{referenceColumnStore}',
	queryMode: 'local',
	displayField: 'displayName',
	valueField: 'name',
	forceSelection: true,
	editable: false
}, {
	xtype: 'textfield',
	labelWidth: 125,
	name: 'referenceTableUrl',
	fieldLabel: '@{referenceTableUrlText}'
},{
	xtype : 'fieldset',
	hidden : '@{popupFieldSetIsHidden}',
	checkboxToggle : true,
	collapsible : true,
	title : '~~referenceTableViewPopup~~',
	collapsed : '@{!referenceTableViewPopup}',
	padding : '0 10 5 10',
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		labelWidth : 115
	},
	items : [{
		xtype: 'textfield',		
		name: 'referenceTableViewPopupTitle'
	}, {
		xtype: 'numberfield',	
		minValue: 0,
		maxValue: 4000,
		name: 'referenceTableViewPopupWidth'
	}, {
		xtype: 'numberfield',	
		minValue: 0,
		maxValue: 4000,
		name: 'referenceTableViewPopupHeight'
	}]
}];

RS.formbuilder.urlControlTemplate = [{
	xtype: 'textfield',
	name: 'referenceTableUrl'
}];

RS.formbuilder.catalogControlTemplate = [{
	xtype: 'grid',
	store: '@{comboboxesStore}',
	selected: '@{catalogSelections}',
	columns: [{
		header: '~~fieldLabel~~',
		flex: 1,
		dataIndex: 'fieldLabel',
		sortable: false
	}],
	tbar: [{
		name: 'addCombobox'
	}, {
		name: 'editCombobox'
	}, {
		name: 'removeCombobox'
	}]
}];

RS.formbuilder.sectionContainerControlTemplate = [{
	xtype: 'combobox',
	name: 'sectionType',
	store: '@{sectionTypeStore}',
	displayField: 'name',
	valueField: 'value',
	queryMode: 'local',
	padding: '10 0 0 0'
}, {
	xtype: 'textfield',
	name: 'title'
}, {
	xtype: 'combobox',
	name: 'layout',
	store: '@{layoutStore}',
	displayField: 'name',
	valueField: 'value',
	queryMode: 'local'
}, {
	xtype: 'numberfield',
	name: 'columnCount',
	minValue: 1,
	maxValue: 4
}, {
	flex: 1,
	layout: 'card',
	activeItem: '@{selectedColumnIndex}',
	items: '@{columns}',
	defaults: {
		viewMode: 'detail'
	}
}];

RS.formbuilder.spacerControlTemplate = [{
	xtype: 'checkbox',	
	name: 'showHorizontalRule',
	hideLabel : true,
	boxLabel : '~~showHorizontalRule~~'
}];

RS.formbuilder.labelControlTemplate = [{
	xtype: 'textfield',
	name: 'fieldLabel',
	fieldLabel: '~~labelDisplayText~~'
}];

RS.formbuilder.journalControlTemplate = [{
	xtype: 'checkbox',	
	name: 'allowJournalEdit',
	hideLabel : true,
	boxLabel : '~~allowJournalEdit~~',
	margin : '0 0 0 130'
}];

/**
 * Adds the template obj to the provided config before it gets rendered in the detail form view
 * @param {Object} config
 * @param {Object} obj
 */

function addTemplateToFactory(config, obj) {
	if (Ext.isArray(obj)) {
		config = config.concat(obj);
	} else {
		config.push(obj);
	}

	return config;
}

/**
 * Factory definition for Form Controls DETAIL to render controls to edit the properties of the form's controls
 * Concatinates all the templates to create one FieldSettings card for the control based on the mixins of the control
 */
RS.formbuilder.views.FormDetailControlLayoutFactory = function(subView) {
	var config = [];

	if (Ext.Array.indexOf(subView.vm.mixins, 'DatabaseControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.databaseTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'FormControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.formControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'RequireableControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.requireableControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'EncryptableControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.encryptableControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'AllowInputControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.allowInputControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'CatalogControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.catalogControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'TooltipControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.tooltipControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'UrlControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.urlControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'DefaultValueControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.defaultValueTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'LinkControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.linkTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'DefaultBooleanControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.defaultValueBooleanTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'NumberControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.numberRangeTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'DecimalControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.decimalRangeTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'DateControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.dateTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'TimeControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.timeTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'SequenceControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.sequenceTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'NameControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.nameControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'StringControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.stringRangeTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'DateTimeControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.dateTimeTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'ButtonControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.buttonTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'ChoiceControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.choiceRadioTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'GroupPickerControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.groupPickerTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'TeamPickerControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.teamPickerTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'MultiChoiceControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.choiceCheckTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'AddressControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.addressControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'ReferenceControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.referenceControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'FileUploadControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.fileUploadControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'ReferenceTableControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.referenceTableControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'TabContainerControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.tabContainerControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'SpacerControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.spacerControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'LabelControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.labelControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'JournalControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.journalControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'SizeControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.formControlSizeTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'DependencyControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.dependencyControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'SectionContainerControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.sectionContainerControlTemplate));

	return {
		xtype: 'form',
		border: false,
		autoScroll: true,
		title: '@{viewmodelTranslatedName}',
		bodyPadding: '10px',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaults: {
			labelWidth: 125
		},
		items: subView.items ? config.concat(subView.items) : config,
		/*listeners: {
			activate: function(panel) {
				if (panel.body) panel.body.highlight('#d6e8ff', {
					duration: 1000
				});
			}
		}*/
	}
};

/**
 * View definition for a TextField viewmodel
 * serverEnum: UI_TEXTFIELD("TextField")
 */
glu.defView('RS.formbuilder.TextField', {
	xtype: 'textfield',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a TextField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.TextField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a TextAreaField viewmodel
 * serverEnum: UI_TEXTAREA("TextArea")
 */
glu.defView('RS.formbuilder.TextAreaField', {
	xtype: 'textarea',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a TextAreaField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.TextAreaField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a WysiwygField viewmodel
 * serverEnum: UI_TEXTAREA("TextArea")
 */
glu.defView('RS.formbuilder.WysiwygField', {
	xtype: 'htmleditor',
	parentLayout: 'FormControlLayout',
	enableSourceEdit: false
});
/**
 * View DETAIL definition for a WysiwygField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.WysiwygField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a ReadOnlyTextField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.ReadOnlyTextField', {
	xtype: 'box',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a ReadOnlyTextField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.ReadOnlyTextField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a SpacerField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.SpacerField', {
	xtype: 'component',
	html: '~~SpacerField~~',
	style: '@{ruleDisplay}',
	flex: 1,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a SpacerField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.SpacerField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a LabelField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.LabelField', {
	xtype: 'label',
	text: '@{fieldLabel}',
	flex: 1,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a LabelField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.LabelField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a RadioButtonField viewmodel
 * serverEnum: UI_CHOICE("RadioButton")
 */
glu.defView('RS.formbuilder.RadioButtonField', {
	xtype: 'radiogroup',
	parentLayout: 'FormControlLayout',
	items: '@{options}',
	columns: '@{columns}',
	itemTemplate: {
		boxLabel: {
			value: '@{displayText}',
			trigger: 'dblclick',
			field: {
				xtype: 'textfield'
			}
		},
		inputValue: '@{id}',
		checked: '@{isSelected}',
		name: '@{..id}',
		xtype: 'radio'
	}
});
/**
 * View DETAIL definition for a RadioButtonField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.RadioButtonField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a NumberField viewmodel
 * serverEnum: UI_NUMBERTEXTFIELD("NumberTextField")
 */
glu.defView('RS.formbuilder.NumberField', {
	xtype: 'numberfield',
	allowDecimals: false,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a NumberField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.NumberField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a DecimalField viewmodel
 * serverEnum: UI_DECIMALTEXTFIELD("DecimalTextField")
 */
glu.defView('RS.formbuilder.DecimalField', {
	xtype: 'numberfield',
	allowDecimals: true,
	decimalPrecision: 5,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a DecimalField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.DecimalField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a MultipleCheckboxField viewmodel
 * serverEnum: UI_MULTIPLECHECKBOX("MultipleCheckBox")
 */
glu.defView('RS.formbuilder.MultipleCheckboxField', {
	xtype: 'checkboxgroup',
	parentLayout: 'FormControlLayout',
	items: '@{options}',
	columns: '@{columns}',
	itemTemplate: {
		boxLabel: {
			value: '@{displayText}',
			trigger: 'dblclick',
			field: {
				xtype: 'textfield'
			}
		},
		xtype: 'checkbox',
		inputId: '@{id}',
		value: '@{isChecked}'
	}
});
/**
 * View DETAIL definition for a MultipleCheckboxField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.MultipleCheckboxField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a ComboBoxField viewmodel
 * serverEnum: UI_COMBOBOX("ComboBox")
 */
glu.defView('RS.formbuilder.ComboBoxField', {
	xtype: 'combobox',
	value: '@{calculateDefaultValue}',
	store: '@{optionsStore}',
	displayField: 'displayText',
	valueField: 'id',
	queryMode: 'local',
	editable: false,
	forceSelection: true,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a ComboBoxField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.ComboBoxField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a MultiSelectComboBoxField viewmodel
 * serverEnum: UI_COMBOBOX("ComboBox")
 */
glu.defView('RS.formbuilder.MultiSelectComboBoxField', {
	xtype: 'combobox',
	value: '@{calculateDefaultValue}',
	store: '@{optionsStore}',
	displayField: 'displayText',
	valueField: 'id',
	queryMode: 'local',
	delimiter: '; ',
	editable: false,
	forceSelection: true,
	multiSelect: true,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a MultiSelectComboBoxField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.MultiSelectComboBoxField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a NameField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.NameField', {
	xtype: 'fieldcontainer',
	parentLayout: 'FormControlLayout',
	layout: 'hbox',
	defaultType: 'textfield',
	items: [{
		flex: 1,
		name: 'firstName',
		hidden: '@{!showFirstName}',
		hideLabel: true
	}, {
		width: 30,
		name: 'middleInitial',
		hidden: '@{!showMiddleInitial}',
		hideLabel: true,
		margins: '0 0 0 5'
	}, {
		flex: 1,
		name: 'middleName',
		hidden: '@{!showMiddleName}',
		hideLabel: true,
		margins: '0 0 0 5'
	}, {
		flex: 2,
		name: 'lastName',
		hidden: '@{!showLastName}',
		hideLabel: true,
		allowBlank: false,
		margins: '0 0 0 5'
	}]
});
/**
 * View DETAIL definition for a NameField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.NameField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a AddressField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.AddressField', {
	xtype: 'fieldcontainer',
	parentLayout: 'FormControlLayout',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'textfield',
		labelAlign: 'top',
		fieldLabel: '~~Street1~~'
	}, {
		xtype: 'textfield',
		labelAlign: 'top',
		fieldLabel: '~~Street2~~'
	}, {
		xtype: 'fieldcontainer',
		hideLabel: true,
		layout: 'hbox',
		items: [{
			xtype: 'textfield',
			labelAlign: 'top',
			flex: 1,
			fieldLabel: '~~City~~'
		}, {
			xtype: 'textfield',
			name: 'state',
			labelAlign: 'top',
			margin: '0 0 0 5',
			flex: 1
		}]
	}, {
		xtype: 'fieldcontainer',
		hideLabel: true,
		layout: 'hbox',
		items: [{
			xtype: 'textfield',
			labelAlign: 'top',
			flex: 1,
			fieldLabel: '~~Zip~~'
		}, {
			xtype: 'combobox',
			name: 'country',
			labelAlign: 'top',
			margin: '0 0 0 5',
			flex: 1,
			store: '@{countryStore}',
			queryMode: 'local',
			displayField: 'name',
			valueField: 'code',
			forceSelection: true,
			editable: false
		}]
	}]
});
/**
 * View DETAIL definition for a AddressField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.AddressField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a EmailField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.EmailField', {
	xtype: 'textfield',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a EmailField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.EmailField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});


glu.defView('RS.formbuilder.MACAddressField', {
	xtype: 'textfield',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a MACAddressField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.MACAddressField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

glu.defView('RS.formbuilder.IPAddressField', {
	xtype: 'textfield',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a MACAddressField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.IPAddressField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

glu.defView('RS.formbuilder.CIDRAddressField', {
	xtype: 'textfield',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a EmailField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.CIDRAddressField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a PhoneField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.PhoneField', {
	xtype: 'textfield',
	parentLayout: 'FormControlLayout',
	readOnly: true,
	plugins: [new RS.formbuilder.viewer.desktop.InputTextMask('(999) 999 - 9999')]
});
/**
 * View DETAIL definition for a PhoneField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.PhoneField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a DateField viewmodel
 * serverEnum: UI_DATE("Date")
 */
glu.defView('RS.formbuilder.DateField', {
	xtype: 'datefield',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a DateField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.DateField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a TimeField viewmodel
 * serverEnum: UI_TIMESTAMP("Timestamp")
 */
glu.defView('RS.formbuilder.TimeField', {
	xtype: 'timefield',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a TimeField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.TimeField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a LinkField viewmodel
 * serverEnum: UI_LINK("Link")
 */
glu.defView('RS.formbuilder.LinkField', {
	xtype: 'textfield',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a LinkField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.LinkField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a LikeRTField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.LikeRTField', {
	xtype: 'textfield',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a LikeRTField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.LikeRTField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a CheckboxField viewmodel
 * serverEnum: UI_CHECKBOX("CheckBox")
 */
glu.defView('RS.formbuilder.CheckboxField', {
	xtype: 'checkbox',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a CheckboxField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.CheckboxField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a PasswordField viewmodel
 * serverEnum: UI_PASSWORD("Password")
 */
glu.defView('RS.formbuilder.PasswordField', {
	xtype: 'textfield',
	inputType: 'password',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a PasswordField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.PasswordField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a JournalField viewmodel
 * serverEnum: UI_JOURNALWIDGET("Journal")
 */
glu.defView('RS.formbuilder.JournalField', {
	xtype: 'journalfield',
	readOnly: true,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a JournalField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.JournalField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a DateTimeField viewmodel
 * serverEnum: UI_DATETIME("DateTime")
 */
glu.defView('RS.formbuilder.DateTimeField', {
	xtype: 'fieldcontainer',
	layout: 'hbox',
	items: [{
		xtype: 'datefield',
		flex: 1,
		value: '@{date}'
	}, {
		xtype: 'timefield',
		flex: 1,
		margin: '0 0 0 5',
		value: '@{time}'
	}],
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a DateTimeField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.DateTimeField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a ListField viewmodel
 * serverEnum: UI_LIST("List")
 */
glu.defView('RS.formbuilder.ListField', {
	xtype: 'grid',
	disableSelection: true,
	viewConfig: {
		trackOver: false
	},
	title: {
		value: '@{fieldLabel}',
		trigger: 'dblclick',
		field: {
			xtype: 'textfield'
		}
	},
	value: '@{calculateDefaultValue}',
	store: '@{optionsStore}',
	tbar: [{
		text: '~~add~~'
	}, {
		text: '~~remove~~'
	}],
	columns: [{
		dataIndex: 'text'
	}],
	hideHeaders: true,
	forceFit: true,
	selModel: {
		mode: 'MULTI'
	},
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a ListField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.ListField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a SequenceField viewmodel
 * serverEnum: UI_SEQUENCE("Sequence")
 */
glu.defView('RS.formbuilder.SequenceField', {
	xtype: 'textfield',
	value: '@{prefix}',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a SequenceField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.SequenceField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a ReferenceField viewmodel
 * serverEnum: UI_REFERENCE("Reference")
 */
glu.defView('RS.formbuilder.ReferenceField', {
	xtype: 'referencefield',
	readOnly: true,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a ReferenceField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.ReferenceField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a HiddenField viewmodel
 * serverEnum: UI_HIDDEN("HiddenField")
 */
glu.defView('RS.formbuilder.HiddenField', {
	xtype: 'textfield',
	parentLayout: 'FormControlLayout'
});

/**
 * View definition for a TagField viewmodel
 * serverEnum: UI_TAG("TagField")
 */
glu.defView('RS.formbuilder.TagField', {
	xtype: 'tagfield',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a TagField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.TagField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a ButtonField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.ButtonField', {
	xtype: 'button',
	text: '@{operationName}',
	minWidth: 50,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a ButtonField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.ButtonField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a UserPickerField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.UserPickerField', {
	xtype: 'combobox',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a UserPickerField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.UserPickerField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a TeamPickerField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.TeamPickerField', {
	xtype: 'combobox',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a TeamPickerField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.TeamPickerField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

glu.defView('RS.formbuilder.FileUploadField', {
	xtype: 'grid',
	flex: 1,
	disableSelection: true,
	title: {
		value: '@{fieldLabel}',
		trigger: 'dblclick',
		field: {
			xtype: 'textfield'
		}
	},
	store: '@{fileStore}',
	tbar: [{
		text: '~~upload~~',
		disabled: true
	}, {
		text: '~~removeFile~~',
		disabled: true
	}],
	columns: [{
		header: '~~uploadColumnName~~',
		dataIndex: 'name'
	}],
	forceFit: true,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a FileUploadField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.FileUploadField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

glu.defView('RS.formbuilder.ReferenceTableField', {
	xtype: 'tabpanel',
	plain: true,
	items: '@{referenceTables}',
	activeTab: '@{currentReference}',
	flex: 1,
	itemTemplate: {
		title: '@{fieldLabel}',
		xtype: 'grid',
		disableSelection: true,
		store: '@{..referenceDisplayStore}',
		tbar: [{
			text: '~~referenceTableNew~~',
			hidden: '@{!allowReferenceTableAdd}',
			disabled: true
		}, {
			text: '~~referenceTableDelete~~',
			hidden: '@{!allowReferenceTableRemove}',
			disabled: true
		}],
		columns: [{
			header: '~~referenceTableColumnName~~',
			dataIndex: 'name'
		}],
		forceFit: true
	},
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a ReferenceTableField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.ReferenceTableField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

glu.defView('RS.formbuilder.ReferenceTableField', {
	xtype: 'tabpanel',
	plain: true,
	items: '@{referenceTables}',
	activeTab: '@{currentReference}',
	flex: 1,
	itemTemplate: {
		title: '@{fieldLabel}',
		xtype: 'grid',
		disableSelection: true,
		store: '@{..referenceDisplayStore}',
		tbar: [{
			text: '~~referenceTableNew~~',
			hidden: '@{!allowReferenceTableAdd}',
			disabled: true
		}, {
			text: '~~referenceTableDelete~~',
			hidden: '@{!allowReferenceTableRemove}',
			disabled: true
		}],
		columns: [{
			header: '~~referenceTableColumnName~~',
			dataIndex: 'name'
		}],
		forceFit: true
	},
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a TabContainerField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.TabContainerField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

glu.defView('RS.formbuilder.TabContainerField', {
	xtype: 'tabpanel',
	plain: true,
	items: '@{referenceTables}',
	activeTab: '@{currentReference}',
	flex: 1,
	itemTemplate: {
		title: '@{fieldLabel}',
		xtype: 'grid',
		disableSelection: true,
		store: '@{..referenceDisplayStore}',
		tbar: [{
			text: '@{..currentReferenceNew}',
			hidden: '@{!allowReferenceTableAdd}',
			disabled: true
		}, {
			text: '~~referenceTableDelete~~',
			hidden: '@{!allowReferenceTableRemove}',
			disabled: true
		}],
		columns: [{
			header: '~~referenceTableColumnName~~',
			dataIndex: 'name'
		}],
		forceFit: true
	},
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a TabContainerField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.TabContainerField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View DETAIL definition for a UrlField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.UrlField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

glu.defView('RS.formbuilder.UrlField', {
	title: '~~UrlField~~',
	html: '~~urlFieldHtmlText~~',
	flex: 1,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a UrlField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.UrlField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

glu.defView('RS.formbuilder.SectionContainerField', {
	title: '@{title}',
	flex: 1,
	layout: 'hbox',
	items: '@{columns}',
	height: null,
	parentLayout: 'FormControlLayout'
});

glu.defView('RS.formbuilder.Column', {
	flex: 1,
	items: '@{controls}',
	activeItem: '@{detailControl}',
	layout: 'anchor',
	minHeight: 300,
	defaults: {
		anchor: '100%'
	},
	cls: 'formControlSectionContainer',
	bodyCls: 'formControlSectionContainerBody',
	plugins: [{
		ptype: 'activeitemfieldsectionselector'
	}, {
		ptype: 'panelfielddragzone',
		ddGroup: 'columnControls',
		targetCls: '.formControlSectionContainer'
	}, {
		ptype: 'panelfielddroptarget',
		ddGroup: 'columnControls',
		targetCls: 'formControlSectionContainer'
	}],
	listeners: {
		itemDropped: '@{dropControl}',
		clicked: '@{columnClicked}',
		render: function(col) {
			col.getEl().on('click', function() {
				col.fireEvent('clicked');
			});
		}
	}
});

glu.defView('RS.formbuilder.Column', 'detail', {
	items: '@{controls}',
	layout: 'card',
	activeItem: '@{detailControl}',
	defaults: {
		viewMode: 'detail'
	}
});

glu.defView('RS.formbuilder.Panel', {
	hidden: true,
	border: false
})
glu.defView('RS.formbuilder.Panel', 'detail', {
	hidden: true,
	border: false
})

/**
 * View DETAIL definition for a TabContainerField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.SectionContainerField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});
