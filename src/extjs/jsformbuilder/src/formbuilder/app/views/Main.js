/**
 * View definition for the main view of the form builder.  This component can be placed anywhere in the system and will render the form builder inside of a border layout
 * It provides an overal container for the available controls, the preview form, and detailed information about the form
 */
glu.defView('RS.formbuilder.Main', {	
	padding: 15,
	layout: 'border',
	defaults: {
		border: false
	},
	dockedItems: [{
		xtype: 'toolbar',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls: 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '~~formBuilderTitle~~'
		}]
	},{
		xtype : 'toolbar',
		cls : 'rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items :  [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',		
		}, {
			name: 'saveForm',
			listeners: {
				render: function(button) {
					// Insert the save button into clientVM
					clientVM.updateSaveButtons(button);
				}
			}

		}, 'saveFormAs', 'preview', '->', {
			iconCls: 'x-tbar-loading',
			handler: '@{refreshForm}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	items: [{
		region: 'center',
		defaults: {
			border: false
		},
		layout: 'border',
		items: [{
			title: '~~availableFields~~',
			bodyCls: 'availableFields',
			region: 'east',
			width: 200,
			maxWidth: 200,
			split: true,
			overflowY: 'scroll',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaultType: 'button',
			items: '@{availableControls}',
			itemTemplate: {
				ui: 'form-builder-button-small-toolbar',
				xtype: 'button',
				text: '@{name}',
				name: 'addControl',
				value: '@{controlType}',
				cls: 'formControlButton',
				padding: '3px',
				margin: '2px 5px 2px 5px',
				disabled: '@{isDisabled}'
			},
			plugins: [{
				ptype: 'panelavailablecontroldragzone'
			}]
		}, {
			region: 'center',
			xtype: '@{mainForm}'
		}],
		listeners: {
			afterrender: function() {
				var splitters = this.query('bordersplitter')
				Ext.Array.forEach(splitters, function(splitter) {
					splitter.setSize(1, 1)
					splitter.getEl().setStyle({
						backgroundColor: '#fbfbfb'
					})
				})
			}
		}
	}, {
		region: 'east',
		width: 525,
		layout: 'card',
		plain: true,
		deferredRender: false,
		activeItem: '@{mainForm.showFieldSettings}',
		tbar: {
			xtype: 'toolbar',
			cls: 'actionBar actionBar-form',
			style: 'padding-bottom: 6px',
			ui: 'display-toolbar',
			defaultButtonUI: 'display-toolbar-button',
			items: [{
				name: 'showFormSettings',
				pressed: '@{showFormSettingsIsPressed}'
			}, {
				name: 'showFieldSettings',
				pressed: '@{showFieldSettingsIsPressed}'
			}]
		},
		bodyStyle: 'padding-top:4px',
		items: [{
			xtype: '@{mainForm}',
			viewMode: 'detail'
		}, {			
			layout: 'card',
			border: false,
			activeItem: '@{mainForm.showSelectFieldCard}',
			items: [{
				border: false,
				html: '<div class="notice-small"><h3>Please select a field.</h3></div>'
			}, {
				layout: 'card',
				border: false,
				activeItem: '@{mainForm.detailControl}',
				items: '@{mainForm.controls}',
				defaults: {
					viewMode: 'detail'
				}
			}]
		}]
	}],	
	listeners: {
		afterrender: function() {
			var splitters = this.query('bordersplitter')
			Ext.Array.forEach(splitters, function(splitter) {
				splitter.setSize(1, 1)
				splitter.getEl().setStyle({
					backgroundColor: '#fbfbfb'
				})
			})
		}
	}
});
