/**
 * @author ryan.smith
 */
glu.defView('RS.formbuilder.AccessRightsPicker', {
	height: 300,
	width: 600,
	cls : 'rs-modal-popup',
	modal : true,
	padding : 15,
	title: '~~addAccessRights~~',
	layout: 'fit',
	items: [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		forceFit: true,
		name: 'roles',
		columns: [{
			header: '~~Name~~',
			dataIndex: 'name'
		}],
		margin : '0 0 10 0'
	}],	
	buttons: [{
		cls : 'rs-med-btn rs-btn-dark',
		name: 'addAccessRightsAction'
	},{
		cls : 'rs-med-btn rs-btn-light',
		name: 'cancel'
	}]
});