glu.defView('RS.formbuilder.Viewer', {
	layout: 'fit',
	items: [{
		xtype: 'rsform',
		showLoading: false,
		formName: '@{formName}',
		formId: '@{formId}',
		recordId: '@{recordId}',
		queryString: '@{queryString}',
		params: '@{params}',
		embed: '@{embed}',
		formdefinition: '@{formdefinition}',
		fromViewer: true,
		items: [],
		listeners: {
			afterrender: function(form) {
				form.fireEvent('registerWithViewmodel', form, form)
			},
			registerWithViewmodel: '@{registerForm}',
			beforeclose: function() {
				this.up('panel').fireEvent('beforeclose')
			},
			//circulating back to update the record id of the viewer
			actioncompleted: function(rsform, actionBtn) {
				this.up('panel').fireEvent('actioncompleted', this, rsform.recordId, actionBtn.actions);
			},
			beforeredirect: function() {
				this.up('panel').fireEvent('beforeredirect')
			},
			reloadForm: function() {
				this.up('panel').fireEvent('reloadForm')
			},
			formLoaded: '@{formLoaded}'
		}
	}],
	listeners: {
		//circulating back to update the record id of the viewer
		actioncompleted: '@{actionCompleted}'
	}
})