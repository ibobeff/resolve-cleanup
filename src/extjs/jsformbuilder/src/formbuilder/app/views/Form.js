/**
 * View definition for a Form viewmodel.  Displays the preview of a Form with each of the controls
 * If no controls are added, then a card flips to show the user a helpful text indicating that they should add controls to the form
 */
glu.defView('RS.formbuilder.Form', {
	border: false,
	title: {
		value: '@{formTitle}',
		trigger: 'dblclick',
		field: {
			xtype: 'textfield'
		}
	},
	layout: 'card',
	activeItem: '@{showNoFieldsCard}',
	plugins: [{
		ptype: 'panelfielddragzone'
	}, {
		ptype: 'panelfielddroptarget'
	}],
	buttonAlign: 'left',
	buttons: '@{formButtons}',
	items: [{
		border: false,
		bodyPadding: '10px',
		html: '<div class="notice"><h2>Please add a field.</h2><p>This is a <strong>live preview</strong> of your form. Currently, <strong>you don\'t have any fields</strong>. Use the buttons on the right to create inputs for your form. Click on the fields to change their properties.</p></div>'
	}, {
		xtype: 'form',
		border: false,
		autoScroll: true,
		activeItem: '@{detailControl}',
		items: '@{controls}',
		plugins: [{
			ptype: 'activeitemfieldselector'
		}]
	}],
	cls: 'formControlsContainer',
	listeners: {
		itemDropped: '@{dropControl}',
		fieldlabeledited: '@{editFieldLabel}',
		titleedited: '@{editTitle}'
	}
});

/**
 * View (Detail) definition for a Form viewmodel.  This displays the options for the form like buttons, hidden fields, and global settings for the form
 */
glu.defView('RS.formbuilder.Form', 'detail', {
	autoScroll: true,
	bodyPadding: '10px',
	border: false,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		labelWidth: 125
	},
	items: [{
			xtype: 'textfield',
			allowBlank: false,
			name: 'name',
			fieldStyle: 'text-transform:uppercase',
			enableKeyEvents: true
		}, {
			xtype: 'textfield',
			name: 'title',
			listeners: {
				focus: '@{focusTitle}',
				blur: '@{blurTitle}'
			}
		}
		, {
			xtype: 'combobox',
			name: 'labelAlign',
			editable: false,
			forceSelection: true,
			queryMode: 'local',
			displayField: 'text',
			valueField: 'value',
			store: '@{labelAlignOptionsList}'
		}, {
			xtype: 'combo',
			name: 'dbTable',
			editable: '@{isAdmin}',
			forceSelection: '@{!isAdmin}',
			queryMode: 'local',
			displayField: 'uname',
			typeAhead: false,
			valueField: 'umodelName',
			store: '@{customTablesList}',
			listeners: {
				valuechanged: function() {
					var actual = this.getActionEl().dom.value;
					if (this.getValue() != this.getActionEl().dom.value)
						this.setValue(actual);
				}
			}
		}, {
			xtype: 'textfield',
			name: 'formWindowTitle'
		}, {
			xtype: 'fieldset',
			padding : '0 10 10 10',
			title: '~~useTabs~~',
			checkboxToggle: true,
			collapsed: '@{!useTabs}',
			items: [{
				xtype: 'checkbox',
				name: 'isWizard',
				boxLabel : '~~isWizard~~',
				hideLabel : true
			}, {
				xtype: 'grid',
				cls : 'rs-grid-dark',
				store: '@{tabPanel.tabStore}',
				title: '~~tabs~~',
				forceFit: true,
				name: 'tabs',
				dockedItems : [{
					xtype : 'toolbar',
					cls : 'rs-dockedtoolbar',
					defaults : {
						cls : 'rs-small-btn rs-btn-light'
					},
					items :  ['addTab','editTab','removeTab']
				}],
				selModel: {
					selType: 'cellmodel'
				},
				plugins: [{
					ptype: 'cellediting',
					clicksToEdit: 2
				}],
				columns: [{
					header: '~~Name~~',
					dataIndex: 'title',
					hideable: false,
					sortable: false,
					editor: {
						allowBlank: false,
						maxLength: 100
					}
				}]
			}]
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			title: '~~HiddenFields~~',
			name: 'hiddenFields',
			forceFit: true,
			padding: '0 0 10 0',
			dockedtoolbar : [{
				xtype : 'toolbar',
				cls : 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items : ['addHiddenField','removeHiddenField']
			}],
			selModel: {
				selType: 'cellmodel'
			},
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 2
			}],
			columns: [{
				header: '~~Name~~',
				dataIndex: 'name',
				hideable: false,
				editor: {
					allowBlank: false
				}
			}, {
				header: '~~Value~~',
				dataIndex: 'value',
				hideable: false,
				editor: {
					allowBlank: false
				}
			}]
		}, {
			title: '~~Buttons~~',
			dockedItems : [{
				xtype : 'toolbar',
				cls : 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items : [{
					name: 'addButton',
					xtype: 'splitbutton',
					menu: {
						xtype: 'menu',
						items: ['addNewButton','addSeparator','addSpacer','addRightJustify']
					}
				},'editButton','removeButton',{
					xtype : 'button',
					text : 'Action',
					menu : {
						xtype : 'menu',
						items : ['addAction','editAction','removeAction']
					}
				}]
			}],
			xtype: 'treepanel',
			cls : 'rs-grid-dark',
			padding: '0 0 10 0',
			useArrows: true,
			rootVisible: false,
			selected: '@{formButtonsSelections}',
			store: '@{treeStore}',
			scroll: false,
			viewConfig: {
				plugins: {
					ptype: 'treeviewdragdrop'
				},
				listeners: {
					beforeDrop: function(node, data, overModel, dropPosition, dropHandler, eOpts) {
						//Can only drop a button to another button position
						var returnVal = false,
							eventName;
						if (overModel.data.type != 'action') {
							if (dropPosition == 'append' && data.records[0].data.type == 'action') {
								eventName = 'dropAction';
								returnVal = true;
							} //appending an action to a button is ok
							if (data.records[0].data.type != 'action' && (dropPosition == 'before' || dropPosition == 'after')) {
								eventName = 'dropButton';
								returnVal = true;
							} //moving a button before or after another button is ok
						}
						if (data.records[0].data.type == 'action' && overModel.data.type == 'action' && (dropPosition == 'before' || dropPosition == 'after')) {
							eventName = 'dropAction';
							returnVal = true;
						} //action before or after another action is ok
						if (eventName == 'dropAction') this.ownerCt.fireEvent(eventName, this.ownerCt, node, data, overModel, dropPosition, eOpts);
						return returnVal;
					},
					drop: function(node, data, overModel, dropPosition, eOpts) {
						var eventName = 'dropButton';

						if (overModel.data.type != 'action') {
							if (dropPosition == 'append' && data.records[0].data.type == 'action') eventName = 'dropAction'; //appending an action to a button is ok
							if (data.records[0].data.type != 'action' && (dropPosition == 'before' || dropPosition == 'after')) eventName = 'dropButton'; //moving a button before or after another button is ok
						}
						if (data.records[0].data.type == 'action' && overModel.data.type == 'action' && (dropPosition == 'before' || dropPosition == 'after')) eventName = 'dropAction'; //action before or after another action is ok
						if (eventName == 'dropButton') this.ownerCt.fireEvent(eventName, this.ownerCt, node, data, overModel, dropPosition, eOpts);
					}
				}
			},
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}],
			columns: [{
				xtype: 'treecolumn',
				//this is so we know which column will show the tree
				header: '~~operation~~',
				flex: 1,
				dataIndex: 'operationName',
				sortable: false,
				editor: {}
			}, {
				header: '~~value~~',
				flex: 1,
				dataIndex: 'value',
				sortable: false,
				editor: {}
			}],
			listeners: {
				beforeEdit: function(editor, e, eOpts) {
					if (e.record.data.type === 'button' && e.colIdx === 1) return false;
					if (e.record.data.type === 'action') return false;
					if (e.record.data.type === 'separator') return false;
					if (e.record.data.type === 'spacer') return false;
					if (e.record.data.type === 'rightJustify') return false;
				},
				dropButton: '@{dropButton}',
				dropAction: '@{dropAction}',
				itemappend: function() {
					this.expandAll();
				},
				itemdblclick: '@{actionItemDoubleClick}'
			}
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			padding: '0 0 10 0',
			title: '~~AccessRights~~',
			name: 'accessRights',
			dockedItems : [{
				xtype : 'toolbar',
				cls : 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items :  ['addAccessRight','removeAccessRight']
			}],
			selModel: {
				selType: 'cellmodel'
			},
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 2
			}],
			columns: [{
					header: '~~Name~~',
					dataIndex: 'name',
					hideable: false,
					flex: 1,
					editor: {
						allowBlank: false
					}
				}, {
					xtype: 'checkcolumn',
					stopSelection: false,
					header: '~~adminRight~~',
					dataIndex: 'admin',
					hideable: false,
					width: 80
				},
				{
					xtype: 'checkcolumn',
					stopSelection: false,
					header: '~~viewRight~~',
					dataIndex: 'view',
					hideable: false,
					width: 80
				}
			]
		}
	]
});