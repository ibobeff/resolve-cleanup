/**
 * View definition for the ImportOption viewmodel.  Displays a window with a listbox on the left to choose from a list of
 * categories and then displays the actual options on the right for the group chosen.
 */
glu.defView('RS.formbuilder.ImportOption', {
	width: 600,
	height: 300,
	modal: true,
	cls : 'rs-modal-popup',
	title: '~~importOptions~~',
	border: false,
	layout: 'fit',
	items: [{
		layout: {
			type: 'hbox',
			align: 'stretch',
			padding: 10
		},
		items: [{
			flex: 1,
			hideLabel: true,
			xtype: 'multiselect',
			multiSelect: false,
			name: 'selectedOption',
			valueField: 'name',
			displayField: 'name',
			store: '@{availableOptions}'
		}, {
			flex: 1,
			xtype: 'textarea',
			hideLabel: true,
			name: 'optionText',
			padding: '0px 5px 0px 10px'
		}]
	}],
	buttons: [{
		name: 'importOption',
		cls : 'rs-med-btn rs-btn-dark'
	}, {
		name: 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});