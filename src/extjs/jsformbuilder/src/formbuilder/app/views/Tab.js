glu.defView('RS.formbuilder.TabPanel', {
	xtype: 'tabpanel',
	plain: true,
	items: '@{tabs}',
	activeTab: '@{selectedTab}',
	height: 23
});

glu.defView('RS.formbuilder.TabPanel', 'detail', {
	xtype: 'panel',
	bodyPadding: '10px',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'grid',
		title: '~~tabs~~',
		store: '@{tabStore}',
		forceFit: true,
		name: 'tabs',
		tbar: [{
			name: 'addTab'
		}, {
			name: 'editTab'
		}, {
			name: 'removeTab'
		}],
		selModel: {
			selType: 'cellmodel'
		},
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		columns: [{
			header: '~~Name~~',
			dataIndex: 'title',
			hideable: false,
			sortable: false,
			editor: {
				allowBlank: false
			}
		}]
	}]
});

glu.defView('RS.formbuilder.Tab', {
	title: '@{title}'
	// closable : '@{closable}'
});

glu.defView('RS.formbuilder.Tab', 'detail', {
	title: '~~editTabTitle~~',
	height: 300,
	width: 600,
	layout: 'fit',
	border: false,
	modal: true,
	items: [{
		xtype: 'form',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		border: false,
		bodyPadding: '10px',
		items: [{
			xtype: 'textfield',
			name: 'title'
		}, {
			xtype: 'grid',
			title: '~~dependencies~~',
			name: 'dependencies',
			flex: 1,
			store: '@{dependenciesStore}',
			forceFit: true,
			selModel: {
				selType: 'rowmodel'
			},
			/*plugins: [{
		ptype: 'cellediting',
		clicksToEdit: 1
	}],*/
			tbar: [{
				name: 'addDependency'
			}, {
				name: 'editDependency'
			}, {
				name: 'removeDependency'
			}],
			columns: [{
				dataIndex: 'actionDisplay',
				header: '~~dependencyAction~~'
				/*editor: {
			xtype: 'combobox',
			store: '@{dependencyActionStore}',
			displayField: 'name',
			valueField: 'name',
			forceSelection: true,
			editable: false,
			selectOnTab: true,
			triggerAction: 'all',
			queryMode: 'local'
		}*/
			}, {
				dataIndex: 'target',
				header: '~~target~~'
				/*editor: {
			xtype: 'combobox',
			store: '@{dependencyTargetStore}',
			displayField: 'name',
			valueField: 'value',
			forceSelection: true,
			editable: false,
			selectOnTab: true,
			triggerAction: 'all',
			queryMode: 'local',
			listeners: {
				expand: function(field) {
					field.store.load();
				}
			}
		}*/
			}, {
				dataIndex: 'conditionDisplay',
				header: '~~condition~~'
				/*editor: {
			xtype: 'combobox',
			store: '@{dependencyConditionStore}',
			displayField: 'name',
			valueField: 'name',
			forceSelection: true,
			editable: false,
			selectOnTab: true,
			triggerAction: 'all',
			queryMode: 'local',
			listConfig: {
				minWidth: 150
			},
			listeners: {
				expand: function(field) {
					field.store.load()
				}
			}
		}*/
			}, {
				dataIndex: 'value',
				header: '~~value~~',
				// editor: {},
				renderer: function(value) {
					return Ext.util.Format.htmlEncode(value);
				}
			}],
			listeners: {
				itemdblclick: '@{depDoubleClick}'
			}
		}]
	}],
	buttonAlign: 'left',
	buttons: [{
		name: 'applyTab'
	}, {
		name: 'cancel'
	}]
});