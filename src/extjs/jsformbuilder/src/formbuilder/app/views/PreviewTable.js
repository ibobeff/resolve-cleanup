/**
 * View definition for a PreviewTable which is a window with a grid in it that displays the changes to be made to the database so users
 * can confirm that the changes are what they are expecting to be made
 */
glu.defView('RS.formbuilder.PreviewTable', {
	height: 500,
	width: 700,
	padding : 15,
	cls : 'rs-modal-popup',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	title: '~~previewOfChanges~~',
	border: false,
	modal: true,

	items: [{
			border: false,
			bodyPadding: '10px',
			html: '@{previewWarning}',
			hidden: '@{!previewWarningIsVisible}'
		}, {
			border: false,
			bodyPadding: '10px',
			html: '@{sizeWarning}',
			hidden: '@{!sizeWarningIsVisible}'
		},
		/*{
		xtype: 'form',
		items: [{
			xtype: 'fieldset',
			title: 'Wiki Options',
			items: [{
				xtype: 'fieldcontainer',
				layout: 'hbox',
				hidden: '@{showCreateWiki}',
				items: [{
					xtype: 'radio',
					name: 'wikiOption',
					inputValue: 'add',
					checked: '@{addToWiki}',
					boxLabel: '~~addToWiki~~'
				}, {
					xtype: 'displayfield',
					name: 'wikiName',
					flex: 1,
					hideLabel: true,
					padding: '0 0 0 5'
				}]
			}, {
				xtype: 'fieldcontainer',
				layout: 'hbox',
				hidden: '@{!showCreateWiki}',
				items: [{
					xtype: 'radio',
					name: 'wikiOption',
					inputValue: 'create',
					checked: '@{createWiki}',
					boxLabel: '~~createWiki~~'
				}, {
					xtype: 'textfield',
					flex: 1,
					hideLabel: true,
					name: 'wikiNameCreate',
					padding: '0 0 0 10'
				}]
			}, {
				xtype: 'radio',
				name: 'wikiOption',
				inputValue: 'none',
				checked: '@{noWiki}',
				boxLabel: '~~noWiki~~'
			}]
		}]
	},*/
		{
			xtype: 'grid',
			flex: 1,
			cls : 'rs-grid-dark',
			store: '@{tableStore}',
			title: '@{tableName}',
			border: false,
			forceFit: true,
			emptyText: '~~noColumnChanges~~',
			viewConfig: {
				deferEmptyText: false
			},
			columns: [{
					header: '~~columnName~~',
					dataIndex: 'columnName'
				}, {
					header: '~~action~~',
					dataIndex: 'action',
					renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
						if (record.data.actionState === '1') metaData.style = 'background-color: #dc4b4b; color: white';
						else metaData.style = '';
						return value;
					}
				}
				/*, {
		 header : '~~dataType~~',
		 dataIndex : 'type'
		 }*/
			]
		}
	],	
	buttons: [{
		name :'save',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});