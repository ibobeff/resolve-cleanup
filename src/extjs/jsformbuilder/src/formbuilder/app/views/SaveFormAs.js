glu.defView('RS.formbuilder.SaveFormAs', {
	height: 230,
	width: 500,
	padding : 15,
	cls : 'rs-modal-popup',
	layout: 'fit',
	title: '~~saveFormAsTitle~~',
	modal: true,
	items: [{
		xtype: 'form',	
		items: [{
			name: 'name',
			xtype: 'textfield',
			anchor: '-20'
		}, {
			name: 'title',
			xtype: 'textfield',
			anchor: '-20'
		}]
	}],	
	buttons: [{
		name: 'saveFormAs',
		cls : 'rs-med-btn rs-btn-dark'
	}, {
		name: 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});