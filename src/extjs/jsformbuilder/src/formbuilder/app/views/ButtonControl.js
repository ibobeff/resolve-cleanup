/**
 * View definition for a Button Control.  Renders a button in the buttons bar of the form
 */
glu.defView('RS.formbuilder.ButtonControl', {
	xtype: 'autobutton',
	text: '@{operation}'
});

glu.defView('RS.formbuilder.ButtonControl', 'detail', {
	title: '~~editButtonTitle~~',
	height: 400,
	width: 600,
	padding : 15,
	layout: 'fit',
	border: false,
	cls : 'rs-modal-popup',
	modal: true,
	items: [{
		xtype: 'form',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		border: false,	
		items: [{
			xtype: 'textfield',
			name: 'operation',
			fieldLabel: '~~name~~'
		}, {
			xtype: 'panel',
			layout: 'hbox',
			padding: '5 0 5 0',
			hidden: '@{!..editButton}',
			items: [{
				xtype: 'combo',
				displayField: 'font',
				valueField: 'font',
				fieldLabel: '~~font~~',
				editable: false,
				width: 270,
				value: '@{font}',
				store: '@{fontStore}',
				listeners: {
					select: function(combo, records) {
						var r = records[0],
							font = r.get('font');

						this.fireEvent('updatebuttonfont', this, font);
					},
					updatebuttonfont: '@{updateButtonFont}'
				}
			}, {
				xtype: 'combo',
				displayField: 'fontsize',
				valueField: 'fontsize',
				width: 65,
				value: '@{fontSize}',
				padding: '0 0 0 10',
				editable: false,
				displayTpl: Ext.create('Ext.XTemplate',
					'<tpl for=".">',
					'{fontsize}pt',
					'</tpl>'
				),
				store: '@{fontSizeStore}',
				listeners: {
					select: function(combo, records) {
						var r = records[0],
							fontSize = parseInt(r.get('fontsize'));

						this.fireEvent('updatebuttonfontsize', this, fontSize);
					},
					updatebuttonfontsize: '@{updateButtonFontSize}'
				}
			}, {
				xtype: 'toolbar',
				itemId: 'buttonColorId',
				items: [{
					iconCls: '@{fontColorIcon}',
					width: 45,
					tooltip: '~~fgColorTooltip~~',
					menu: {
						xtype: 'resolvecolormenu',
						handler: function(cm, color) {
							if (typeof(color) == "string") {
								this.up('#buttonColorId').fireEvent('updatebuttonfontcolor', this, color);
								this.clear();
							}
						}
					}
				}, {
					iconCls: '@{fillColorIcon}',
					width: 45,
					tooltip: '~~bgColorTooltip~~',
					menu: {
						xtype: 'resolvecolormenu',
						handler: function(cm, color) {
							if (typeof(color) == "string") {
								this.up('#buttonColorId').fireEvent('updatebuttonbackgroundcolor', this, color);
								this.clear();
							}
						}
					}
				}],
				listeners: {
					updatebuttonfontcolor: '@{updateButtonFontColor}',
					updatebuttonbackgroundcolor: '@{updateButtonBackgroundColor}',
				}
			}]
		}, {
			xtype: 'checkbox',
			name: 'customTableDisplay',
			hideLabel: true,
			boxLabel: '~~customTableDisplay~~',
			padding: '-2 0 0 104',
			tooltip: '~~customTableDisplayTooltip~~',
			listeners: {
				render: function(field) {
					Ext.create('Ext.tip.ToolTip', {
						target: field.getEl(),
						html: field.tooltip
					})
				}
			}
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',		
			title: '~~dependencies~~',
			name: 'dependencies',
			flex: 1,
			store: '@{dependenciesStore}',
			forceFit: true,
			selModel: {
				selType: 'rowmodel'
			},
			/*plugins: [{
		ptype: 'cellediting',
		clicksToEdit: 1
	}],*/
			dockedItems :[{
				xtype : 'toolbar',
				cls : 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items : ['addDependency','editDependency','removeDependency']
			}] ,
			columns: [{
				dataIndex: 'actionDisplay',
				header: '~~dependencyAction~~'
				/*editor: {
			xtype: 'combobox',
			store: '@{dependencyActionStore}',
			displayField: 'name',
			valueField: 'name',
			forceSelection: true,
			editable: false,
			selectOnTab: true,
			triggerAction: 'all',
			queryMode: 'local'
		}*/
			}, {
				dataIndex: 'target',
				header: '~~target~~'
				/*editor: {
			xtype: 'combobox',
			store: '@{dependencyTargetStore}',
			displayField: 'name',
			valueField: 'value',
			forceSelection: true,
			editable: false,
			selectOnTab: true,
			triggerAction: 'all',
			queryMode: 'local',
			listeners: {
				expand: function(field) {
					field.store.load();
				}
			}
		}*/
			}, {
				dataIndex: 'conditionDisplay',
				header: '~~condition~~'
				/*editor: {
			xtype: 'combobox',
			store: '@{dependencyConditionStore}',
			displayField: 'name',
			valueField: 'name',
			forceSelection: true,
			editable: false,
			selectOnTab: true,
			triggerAction: 'all',
			queryMode: 'local',
			listConfig: {
				minWidth: 150
			},
			listeners: {
				expand: function(field) {
					field.store.load()
				}
			}
		}*/
			}, {
				dataIndex: 'value',
				header: '~~value~~',
				// editor: {},
				renderer: function(value) {
					return Ext.util.Format.htmlEncode(value);
				}
			}],
			listeners: {
				itemdblclick: '@{depDoubleClick}'
			}
		}]
	}],
	buttons: [{
		name: 'applyButton',
		cls : 'rs-med-btn rs-btn-dark'
	}, {
		name: 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});

glu.regAdapter('autobutton', {
	beforeCollect: (function() {
		return function(config, dataModel) {
			var xtype = 'button';
			switch (dataModel.type) {
				case 'separator':
					xtype = 'tbseparator';
					break;
				case 'spacer':
					xtype = 'tbspacer';
					break;
				case 'rightJustify':
					xtype = 'tbfill';
					break;
			}
			config.xtype = xtype;
		}
	})()

});