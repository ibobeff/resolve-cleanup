/**
 * View definition for an Action viewmodel.  Shows an operation (required) and if additional
 * information (like runbook or script) is required based on the operation.  Hides the comboboxes
 * if they aren't needed for the selected operation.
 */
glu.defView('RS.formbuilder.Action', {
	minHeight: 250,
	width: 600,
	padding : 15,
	cls : 'rs-modal-popup',	
	title: '@{actionTitle}',	
	modal: true,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	items: [{
		xtype: 'combobox',
		store: '@{operationStore}',
		displayField: 'name',
		valueField: 'value',
		editable: false,
		forceSelection: true,
		queryMode: 'local',
		name: 'operation'
	}, {
		xtype: 'textfield',
		name: 'EVENT_EVENTID'
	}, {
		xtype: 'combobox',
		store: '@{dbActionStore}',
		displayField: 'name',
		valueField: 'value',
		editable: false,
		forceSelection: true,
		queryMode: 'local',
		name: 'dbAction'
	}, {
		xtype: 'combobox',
		store: '@{sirActionStore}',
		displayField: 'name',
		valueField: 'value',
		editable: false,
		forceSelection: true,
		queryMode: 'local',
		name: 'sirAction'
	}, {
		xtype: 'combobox',
		store: '@{rootVM.mainForm.runbookStore}',
		displayField: 'name',
		valueField: 'value',
		forceSelection: false,
		typeAhead: true,
		queryMode: 'local',
		// name: 'RUNBOOK',
		fieldLabel: '~~RUNBOOK~~',
		hidden: '@{!RUNBOOKIsVisible}',
		listeners: {
			render: function(combo) {
				combo.setValue(combo._vm.RUNBOOK)
			},
			change: '@{runbookChange}'
		}
	}, {
		xtype: 'combobox',
		store: '@{rootVM.mainForm.scriptStore}',
		displayField: 'name',
		valueField: 'value',
		editable: false,
		forceSelection: true,
		queryMode: 'local',
		name: 'SCRIPT'
	}, {
		xtype: 'combobox',
		store: '@{rootVM.mainForm.actionTaskStore}',
		displayField: 'name',
		valueField: 'value',
		editable: true,
		forceSelection: true,
		queryMode: 'local',
		name: 'ACTIONTASK'
	}, {
		xtype: 'combobox',
		store: '@{problemIdStore}',
		displayField: 'name',
		valueField: 'value',
		editable: true,
		queryMode: 'local',
		// hideTrigger: true,
		name: 'PROBLEMID'
	},
	/* {
	xtype: 'textfield',
	name: 'EVENT_REFERENCE'
}, */
	{
		xtype: 'radiogroup',
		fieldLabel: '~~execution~~',
		name: 'scriptTimeout',
		columns: 1,
		items: [{
			layout: 'hbox',
			border: false,
			items: [{
				xtype: 'radio',
				name: 'timeout',
				boxLabel: '~~synchronous~~',
				inputValue: 0,
				value: '@{synchronous}'
			}, {
				xtype: 'numberfield',
				flex: 1,
				padding: '0 0 0 10',
				hideLabel: true,
				minValue: 0,
				name: 'waitSeconds'
			},
			{
				xtype : 'checkboxfield',
				padding: '0 0 0 10',
				boxLabel: '~~useDefault~~',
				disabled: '@{!useDefaultIsEnabled}',
				checked : '@{usingDefault}',
				tooltip: '~~useDefaultTooltip~~',
				submitValue: false,
				listeners: {
					render: function(checkbox) {
						if (checkbox.tooltip) {
							Ext.create('Ext.tip.ToolTip', {
								target: checkbox.getEl(),
								html: checkbox.tooltip
							})
						}
					},
					change: '@{updateUseDefault}'
				}
			}]
		}, {
			xtype: 'radio',
			name: 'timeout',
			boxLabel: '~~asynchronous~~',
			inputValue: -1,
			value: '@{asynchronous}'
		}]
	}, {
		name: 'redirectTarget',
		xtype: 'combobox',
		store: '@{redirectStore}',
		editable: true,
		displayField: 'name',
		valueField: 'name',
		queryMode: 'local'
	}, {
		name: 'redirectUrl',
		xtype: 'textfield'
	}, {
		name: 'mappingFrom',
		xtype: 'combobox',
		store: '@{mappingStore}',
		editable: true,
		displayField: 'name',
		valueField: 'name',
		queryMode: 'local'
	}, {
		name: 'mappingTo',
		xtype: 'combobox',
		store: '@{mappingStore}',
		editable: true,
		displayField: 'name',
		valueField: 'name',
		queryMode: 'local'
	}, {
		name: 'showConfirmation',
		xtype: 'checkbox'
	}, {
		name: 'messageBoxTitle',
		xtype: 'textfield'
	}, {
		name: 'messageBoxMessage',
		xtype: 'textfield'
	}, {
		name: 'messageBoxYesText',
		xtype: 'textfield'
	}, {
		name: 'messageBoxNoText',
		xtype: 'textfield'
	}, {
		name: 'reloadDelay',
		xtype: 'numberfield',
		minValue: 0
	}, {
		name: 'gotoTabName',
		xtype: 'textfield'
	}, {
		xtype: 'fieldcontainer',
		fieldLabel: '~~StopWhen~~',
		hidden: '@{!reloadDependencyIsVisible}',
		layout: 'hbox',
		items: [{
			xtype: 'combobox',
			name: 'reloadTarget',
			store: '@{dependencyTargetStore}',
			displayField: 'name',
			valueField: 'value',
			forceSelection: true,
			editable: false,
			selectOnTab: true,
			triggerAction: 'all',
			queryMode: 'local',
			listConfig: {
				minWidth: 150
			},
			listeners: {
				expand: function(field) {
					field.store.load()
				}
			},
			hideLabel: true,
			flex: 1
		}, {
			xtype: 'combobox',
			name: 'reloadCondition',
			store: '@{dependencyConditionStore}',
			displayField: 'name',
			valueField: 'value',
			forceSelection: true,
			editable: false,
			selectOnTab: true,
			triggerAction: 'all',
			queryMode: 'local',
			listConfig: {
				minWidth: 150
			},
			listeners: {
				expand: function(field) {
					field.store.load()
				}
			},
			hideLabel: true,
			padding: '0 0 0 5',
			flex: 1
		}, {
			xtype: 'textfield',
			name: 'reloadValue',
			hideLabel: true,
			padding: '0 0 0 5',
			flex: 1
		}]
	}],
	buttons: [{
		name: 'addAction',
		hidden: '@{!isNew}',
		cls : 'rs-med-btn rs-btn-dark'
	}, {
		name: 'applyAction',
		hidden: '@{isNew}',
		cls : 'rs-med-btn rs-btn-dark'
	}, {
		name: 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}],
});