
Ext.define('RS.formbuilder.model.WikiName', {
	extend: 'Ext.data.Model',
	idProperty: 'name',
	fields: ['name', 'value'],
	proxy: {
			url: '/resolve/service/form/getwikinames',
			type: 'ajax',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
});