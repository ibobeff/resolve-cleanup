/**
 * DTO for the Custom Table information
 */
Ext.define('RS.formbuilder.model.CustomTableDTO', {
	extend: 'Ext.data.Model',
	idProperty: 'sys_id',
	fields: ['sys_id', 'uname', 'umodelName', 'udisplayName', 'utype', 'usource', 'udestination']
});