/**
 * Plugin that makes Fields within a Panel dragable
 * Any component that has the css class formControlButton (like all the fields have) will be reorderable in the form
 */
Ext.define('RS.formbuilder.plugins.PanelFieldDragZone', {

	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.panelfielddragzone',

	ddGroup: 'formControls',
	targetCls: '.formControlContainer',

	init: function(panel) {
		panel.on('render', this.onPanelRender, this, {
			single: true
		});
	},

	onPanelRender: function(panel) {
		var me = this;
		var proxyPanel;
		new Ext.dd.DragZone(panel.getEl(), {
			ddGroup: me.ddGroup,

			//Gets the drag data information by finding the drag targets based on their css class
			getDragData: function(e) {
				var sourceEl = e.getTarget(me.targetCls, 9);
				if (sourceEl && !Ext.fly(sourceEl).child('.formControlSectionContainerBody')) {
					var d = sourceEl.cloneNode(true);
					d.id = Ext.id();
					proxyPanel = new Ext.panel.Proxy(Ext.getCmp(sourceEl.id), {});
					return {
						ddel: d,
						sourceEl: sourceEl,
						proxyPanel: proxyPanel,
						repairXY: Ext.fly(sourceEl).getXY()
					}
				}
			},

			//Provide coordinates for the proxy to slide back to on failed drag.
			//This is the original XY coordinates of the draggable element captured
			//in the getDragData method.
			getRepairXY: function() {
				return this.dragData.repairXY;
			},
			afterInvalidDrop: function() {
				proxyPanel.hide();
				return false;
			}
		});
	}
});