/**
 * Plugin that highlights the selected field (whether it was done by the user on cilck or programatically)
 */
Ext.define('RS.formbuilder.plugins.ActiveItemFieldSelector', {
	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.activeitemfieldselector',

	init: function(panel) {
		panel.on('activeitemchanged', this.selectField, this, {
			delay: 1,
			buffer: 50
		});
	},

	selectField: function(form, control) {
		//focus on the input field of the control so the user can edit the control
		if (control.items.first() && control.items.first().focus) control.items.first().focus();

		//if the control isn't already selected, then we need to select it and highlight it
		var highlightColor = 'efefef';
		// if (control.getEl().getColor('background-color', '#ffffff') !== '#' + highlightColor.toLowerCase()) {
		//display a background highlight on the item when its selected
		var form = control.up('form');
		if (form) {
			form.items.each(function(item) {
				var buttons = item.query('button');
				Ext.each(buttons, function(button) {
					if (button.ownerCt == item && button.icon) button.hide();
				});
				var el = item.getEl();
				if (el.getColor('background-color', '#ffffff') !== '#ffffff') {
					item.getEl().animate({
						duration: 100,
						easing: 'ease-in',
						from: {
							backgroundColor: highlightColor
						},
						to: {
							backgroundColor: 'ffffff'
						}
					});
				}
			});

			//display the duplicate and remove buttons
			var buttons = control.query('button');
			Ext.each(buttons, function(button) {
				if (button.ownerCt == control) button.show();
			});

			control.getEl().stopAnimation();
			control.getEl().animate({
				duration: 250,
				easing: 'ease-in',
				from: {
					backgroundColor: 'ffffff'
				},
				to: {
					backgroundColor: highlightColor
				}
			});
		}

		// }
	}
});