/**
 * Plugin that makes Buttons from the Avaialable Controls dragable onto the form
 */
Ext.define('RS.formbuilder.plugins.PanelAvailableControlDragZone', {

	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.panelavailablecontroldragzone',

	init: function(panel) {
		panel.on('render', this.onPanelRender, this, {
			single: true
		});
	},

	onPanelRender: function(panel) {
		var temp = new Ext.dd.DragZone(panel.getEl(), {
			ddGroup: 'columnControls',

			//configures the different drag targets based on a css selector and returns the item data for dragging
			getDragData: function(e) {
				var sourceEl = e.getTarget('.formControlButton');
				if (sourceEl) {
					var d = sourceEl.cloneNode(true);
					d.id = Ext.id();
					return {
						ddel: d,
						sourceEl: sourceEl,
						repairXY: Ext.fly(sourceEl).getXY(),
						proxyPanel: new Ext.panel.Proxy(Ext.getCmp(sourceEl.id), {})
					}
				}
			},

			//Provide coordinates for the proxy to slide back to on failed drag.
			//This is the original XY coordinates of the draggable element captured
			//in the getDragData method.
			getRepairXY: function() {
				return this.dragData.repairXY;
			}
		});
		temp.addToGroup('formControls');
	}
});