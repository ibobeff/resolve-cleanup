glu.defModel('RS.formbuilder.BaseControl', {
	unsupportedField: false,
	viewmodelTranslatedName$: function() {
		return this.localize(this.viewmodelName);
	},

	/**
	 * Button Handler that forwards the event to the parentVM and removes this control from the form
	 */
	removeControl: function() {
		this.parentVM.removeControl(this);
	},
	/**
	 * Button Handler that forwards the event to the parentVM and duplicates this control on the form
	 */
	duplicateControl: function() {
		this.parentVM.duplicateControl(this);
	},
	/**
	 * Event Handler that forwards the event to the parentVM and selects this control on the form
	 */
	selectControl: function() {
		this.parentVM.selectControl(this);
	}
});
/**
 * Mixin definition for a FormControl
 * A form control is the base of all the controls.  It is a mixin, and as such its attributes will get added to all controls
 * It provides basic functionality for each field like fieldLabel, required, defaultValues, etc.
 */
glu.defModel('RS.formbuilder.FormControl', {
	/**
	 * Internal id of the control, this is not persisted to the backend
	 */
	id: '',
	/**
	 * The default value to show to the user when the control initially loads
	 */
	defaultValueInput: '',
	defaultValue: '',
	when_default_value_change : {
		on : ['defaultValueInputChanged'],
		action : function(val){
			this.set('defaultValue',/^[a-zA-Z0-9_\-](\s{0,1}[a-zA-Z0-9])*$/.test(val) ? val : '');			
		}
	},
	allowedMaxValue: 999999999,
	/**
	 * Field label of the control to display to the user, otherwise known as displayName
	 */
	fieldLabel: 'Untitled',
	/**
	 * Formula for determining the label separator of a control.  If its a required field, then we give it a red star to indicate that it is a required field
	 */
	mandatoryLabelSeparator$: function() {
		return this.mandatory ? ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>' : ':'
	},
	/**
	 * Label alignment (top, left, right) of the field label for the control
	 */
	labelAlign: 'left',
	/**
	 * List of label alignment options, configures a store with left, right, and top options for the user to choose from
	 */
	labelAlignOptionsList: {
		mtype: 'store',
		fields: ['text', 'value'],
		data: [{
			text: 'Left',
			value: 'left'
		}, {
			text: 'Right',
			value: 'right'
		}, {
			text: 'Top',
			value: 'top'
		}]
	},

	/**
	 * Indicates whether the user should be able to change this field's value
	 */
	readOnly: false,
	/**
	 * Indicates whether the control should be shown to the user or not
	 */
	hidden: false,

	fieldLabelOnFocus: 'Untitled',
    fieldLabelIsChanged: false,

    focusFieldLabel: function (field) {
        this.fieldLabelOnFocus = field.target.value;
    },
    
    blurFieldLabel: function (field) {
		this.fieldLabelIsChanged = this.fieldLabelIsChanged || this.fieldLabelOnFocus !== field.target.value;
    },

	when_hidden_changes_update_required: {
		on: ['hiddenChanged'],
		action: function() {
			if (Ext.isDefined(this.requiredShouldBeEnabled)) this.set('requiredShouldBeEnabled', !this.hidden);
			if (this.hidden && Ext.isDefined(this.mandatory)) this.set('mandatory', false);
		}
	},

	initMixin: function() {
		this.labelAlignOptionsList.each(function(option) {
			option.set('text', this.localize(option.get('text')));
		}, this);

		if (this.fieldLabel === 'Untitled') this.set('fieldLabel', this.localize(this.fieldLabel));

		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getFormControlConversionProperties) : this.serializeFunctions = [this.getFormControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseFormControlConversionProperties) : this.deserializeFunctions = [this.parseFormControlConversionProperties];
	},

	/**
	 * Reactor listening for events that cannot be automatically updated by the framework because extjs doesn't support it, so we need to rebuild the control
	 */
	whenControlNeedsRebuilding: {
		on: ['labelAlignChanged', 'mandatoryChanged'],
		action: function() {
			//Rebuild just the live view if at all possible
			this.parentVM.rebuildControl(this, true);
		}
	},
	/**
	 * Conversion method that turns a form control into server fields to be persisted in the db
	 */
	getFormControlConversionProperties: function() {
		return {
			id: this.id,
			name: (this.inputType === 'INPUT' ? this.inputName : this.column) || this.fieldLabel,
			displayName: this.fieldLabel,
			hidden: this.hidden,
			readOnly: this.readOnly,
			labelAlign: this.labelAlign
		};
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseFormControlConversionProperties: function(config) {
		this.set('id', config.id);
		this.set('fieldLabel', config.displayName);
		this.set('inputName', config.name);
		this.set('readOnly', config.readOnly);
		this.set('hidden', config.hidden);
		this.set('labelAlign', config.labelAlign || this.parentVM.labelAlign);
		this.fieldLabelIsChanged = this.fieldLabelIsChanged || this.fieldLabelOnFocus !== config.displayName;
	}
});

/**
 * Mixin definition for a SizeControl
 * A size control is a mixin for storing whether the field can be resized by the user.
 * It keeps track of things like width and height for the control
 */
glu.defModel('RS.formbuilder.SizeControl', {
	/**
	 * Width of the control in pixels
	 */
	width: 300,
	/**
	 * Height of the control in pixels
	 */
	height: 25,
	/**
	 * Default height of controls
	 */
	defaultHeight: 25,
	/**
	 * Whether the section should be collapsed (hidden)
	 */
	collapsed: true,
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getSizeControlConversionProperties) : this.serializeFunctions = [this.getSizeControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseSizeControlConversionProperties) : this.deserializeFunctions = [this.parseSizeControlConversionProperties];
	},
	/**
	 * Conversion method that turns a size control into server fields to be persisted in the db
	 */
	getSizeControlConversionProperties: function() {
		return {
			height: this.collapsed ? this.defaultHeight : this.height,
			width: this.actualWidth
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseSizeControlConversionProperties: function(config) {
		this.set('height', config.height || this.height);
		this.set('width', config.width || this.width);

		//old default height was 22, in that case up it to new default height
		if (this.height == 22) this.set('height', this.defaultHeight)

		if (config.width || (config.height > 0 && config.height != this.defaultHeight)) {
			this.set('collapsed', false);
		}
	},
	actualWidth$: function() {
		return this.collapsed ? 0 : this.width;
	},
	/**
	 * The actual height to be used for the control based on whether the size fieldset is collapsed or not
	 */
	actualHeight$: function() {
		var dh = this.defaultHeight,
			h = this.height;
		if (this.labelAlign == 'top') {
			dh += 20;
			h += 20;
		}
		return this.collapsed ? dh : h;
	}
});
/**
 * Mixin definition for a StringControl
 * A string control has a min and max length associated with it
 */
glu.defModel('RS.formbuilder.StringControl', {
	/**
	 * Minimum length of the string to make the control valid
	 */
	minLength: 0,
	minLengthIsValid$: function() {
		if (this.minLength < 0) return this.localize('minValueInvalid', [0]);
		if (this.minLength > this.allowedMaxValue) return this.localize('maxValueInvalid', [this.allowedMaxValue]);
		return true;
	},
	/**
	 * Maximum length of the string to make the control valid
	 */
	maxLength: 100,
	maxLengthIsValid$: function() {
		if (this.maxLength < 0) return this.localize('minValueInvalid', [0]);
		if (this.maxLength > this.allowedMaxValue) return this.localize('maxValueInvalid', [this.allowedMaxValue]);
		return true;
	},
	columnSize: 333,
	columnSizeStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [{
			name: '40',
			value: 40
		}, {
			name: '333',
			value: 333
		}, {
			name: '1000',
			value: 1000
		}, {
			name: '4000',
			value: 4000
		}, {
			name: '4000+',
			value: 4001
		}]
	},

	columnSizeIsVisible$: function() {
		return this.inputType == 'DB';
	},
	columnSizeIsReadOnly$: function() {
		return this.id || (this.column && this.parentVM.columnStore.findRecord('name', this.column, 0, false, false, true));
	},
	when_column_size_changes_update_allowedMaxValue: {
		on: ['columnSizeChanged'],
		action: function() {
			if (this.columnSize <= 4000) this.set('allowedMaxValue', this.columnSize);
			else this.set('allowedMaxValue', 999999999);
		}
	},

	initMixin: function() {
		this.set('maxLength', this.allowedMaxValue);
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getStringControlConversionProperties) : this.serializeFunctions = [this.getStringControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseStringControlConversionProperties) : this.deserializeFunctions = [this.parseStringControlConversionProperties];
	},
	/**
	 * Conversion method that turns a string control into server fields to be persisted in the db
	 */
	getStringControlConversionProperties: function() {
		return {
			stringMinLength: this.minLength,
			uiStringMaxLength: this.inputType == 'DB' ? (Math.min(this.maxLength <= 0 ? 999999999 : this.maxLength, this.columnSize)) : this.maxLength
		};
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseStringControlConversionProperties: function(config) {
		this.set('maxLength', config.uiStringMaxLength <= 4000 ? config.uiStringMaxLength : 999999999);
		this.set('minLength', config.stringMinLength);
	}
});
/**
 * Mixin definition for a DefaultValueControl
 * Keeps track of the default value for the control
 */
glu.defModel('RS.formbuilder.DefaultValueControl', {
	/**
	 * The default value of the control to pre-populate for the user
	 */
	defaultValue: '',
	defaultValueIsEnabled: true,
	when_field_label_starts_with_sys_disable_default_value: {
		on: ['dbFieldNameChanged'],
		action: function() {
			if (this.dbFieldName && this.dbFieldName.indexOf('sys_') == 0) {
				this.set('defaultValue', '');
				this.set('defaultValueIsEnabled', false);
			} else this.set('defaultValueIsEnabled', true);
		}
	},
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getDefaultValueControlConversionProperties) : this.serializeFunctions = [this.getDefaultValueControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseDefaultValueControlConversionProperties) : this.deserializeFunctions = [this.parseDefaultValueControlConversionProperties];
	},
	/**
	 * Conversion method that turns a default value control into server fields to be persisted in the db
	 */
	getDefaultValueControlConversionProperties: function() {
		return {
			defaultValue: this.defaultValue
		};
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseDefaultValueControlConversionProperties: function(config) {
		this.set('defaultValue', config.defaultValue || '');
	}
});
glu.defModel('RS.formbuilder.DefaultBooleanControl', {
	defaultValue: false,

	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getDefaultBooleanValueControlConversionProperties) : this.serializeFunctions = [this.getDefaultBooleanValueControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseDefaultBooleanValueControlConversionProperties) : this.deserializeFunctions = [this.parseDefaultBooleanValueControlConversionProperties];
	},
	/**
	 * Conversion method that turns a default value control into server fields to be persisted in the db
	 */
	getDefaultBooleanValueControlConversionProperties: function() {
		return {
			defaultValue: this.defaultValue
		};
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseDefaultBooleanValueControlConversionProperties: function(config) {
		this.set('defaultValue', config.defaultValue === 'true' || false);
	}
});

/**
 * Mixin definition for a DateControl
 * Keeps track of a date default value instead of a string
 */
glu.defModel('RS.formbuilder.DateControl', {
	/**
	 * The default value of the control to pre-populate for the user
	 */
	defaultValue: '',
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getDateControlConversionProperties) : this.serializeFunctions = [this.getDateControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseDateControlConversionProperties) : this.deserializeFunctions = [this.parseDateControlConversionProperties];
	},
	/**
	 * Conversion method that turns a date control into server fields to be persisted in the db
	 */
	getDateControlConversionProperties: function() {
		return {
			defaultValue: Ext.Date.format(this.defaultValue, 'c')
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseDateControlConversionProperties: function(config) {
		this.set('defaultValue', Ext.Date.parse(config.defaultValue, 'c'));
	}
});
/**
 * Mixin definition for a TimeControl
 * Keeps track of a time default value instead of a string
 */
glu.defModel('RS.formbuilder.TimeControl', {
	/**
	 * The default value of the control to pre-populate for the user
	 */
	defaultValue: '',
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getTimeControlConversionProperties) : this.serializeFunctions = [this.getTimeControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseTimeControlConversionProperties) : this.deserializeFunctions = [this.parseTimeControlConversionProperties];
	},
	/**
	 * Conversion method that turns a time control into server fields to be persisted in the db
	 */
	getTimeControlConversionProperties: function() {
		return {
			defaultValue: Ext.Date.format(this.defaultValue, 'c')
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseTimeControlConversionProperties: function(config) {
		this.set('defaultValue', Ext.Date.parse(config.defaultValue, 'c'));
	}
});
/**
 * Mixin definition for a DateTimeControl
 * Keeps track of a date and a time and merges the two together for the initial value of the control
 */
glu.defModel('RS.formbuilder.DateTimeControl', {
	/**
	 * Internal date to store from the date control to be merged with the time
	 */
	date: '',
	/**
	 * Internal time to store from the time control to be merged with the date
	 */
	time: '',
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getDateTimeControlConversionProperties) : this.serializeFunctions = [this.getDateTimeControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseDateTimeControlConversionProperties) : this.deserializeFunctions = [this.parseDateTimeControlConversionProperties];
	},
	/**
	 * Conversion method that turns a date time control into server fields to be persisted in the db
	 */
	getDateTimeControlConversionProperties: function() {
		var myDate = this.date,
			myTime = this.time,
			datetime = new Date();

		if (Ext.isDate(myDate) && Ext.isDate(myTime)) datetime = new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), myTime.getHours(), myTime.getMinutes(), myTime.getSeconds());
		else if (Ext.isDate(myDate)) datetime = myDate;
		else if (Ext.isDate(myTime)) datetime = myTime;
		else return {
			defaultValue: ''
		};

		return {
			defaultValue: Ext.Date.format(datetime, 'c')
		};

	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseDateTimeControlConversionProperties: function(config) {
		this.set('date', Ext.Date.parse(config.defaultValue, 'c'));
		this.set('time', Ext.Date.parse(config.defaultValue, 'c'));
	}
});
/**
 * Mixin definition for a RequireableControl
 * Determines whether this field is required before submitting to the server
 */
glu.defModel('RS.formbuilder.RequireableControl', {
	/**
	 * Whether the control is required to be filled out or not
	 */
	mandatory: false,
	requiredShouldBeEnabled: true,
	mandatoryIsEnabled$: function() {
		return this.requiredShouldBeEnabled;
	},
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getRequireableControlConversionProperties) : this.serializeFunctions = [this.getRequireableControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseRequireableControlConversionProperties) : this.deserializeFunctions = [this.parseRequireableControlConversionProperties];
	},
	/**
	 * Conversion method that turns a requireable control into server fields to be persisted in the db
	 */
	getRequireableControlConversionProperties: function() {
		return {
			mandatory: this.mandatory
		};
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseRequireableControlConversionProperties: function(config) {
		this.set('mandatory', config.mandatory);
	}
});
/**
 * Mixin definition for an Encryptable Control
 * Determines whether this field is encrypted before submitting to the database
 */
glu.defModel('RS.formbuilder.EncryptableControl', {
	/**
	 * Whether the control is required to be filled out or not
	 */
	encrypted: false,
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getEncryptableControlConversionProperties) : this.serializeFunctions = [this.getEncryptableControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseEncryptableControlConversionProperties) : this.deserializeFunctions = [this.parseEncryptableControlConversionProperties];
	},
	/**
	 * Conversion method that turns a requireable control into server fields to be persisted in the db
	 */
	getEncryptableControlConversionProperties: function() {
		return {
			encrypted: this.encrypted
		};
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseEncryptableControlConversionProperties: function(config) {
		this.set('encrypted', config.encrypted);
	}
});
/**
 * Mixin definition for a SequenceControl
 * Keeps track of a sequence prefix for the user
 */
glu.defModel('RS.formbuilder.SequenceControl', {
	/**
	 * User defined sequence prefix
	 */
	prefix: '',
	prefixIsValid$: function() {
		if (!this.prefix) return this.localize('prefixRequired');
		return true;
	},
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getPrefixControlConversionProperties) : this.serializeFunctions = [this.getPrefixControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parsePrefixControlConversionProperties) : this.deserializeFunctions = [this.parsePrefixControlConversionProperties];
	},
	/**
	 * Conversion method that turns a sequence control into server fields to be persisted in the db
	 */
	getPrefixControlConversionProperties: function() {
		return {
			sequencePrefix: this.prefix
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parsePrefixControlConversionProperties: function(config) {
		this.set('prefix', config.sequencePrefix);
	}
});
/**
 * Mixin definition for a NumberControl
 * Keeps track of min and max VALUE instead of length like in a string control
 */
glu.defModel('RS.formbuilder.NumberControl', {
	/**
	 * The default value to show to the user when the control initially loads
	 */
	defaultValue: '',
	/**
	 * Min value for the number
	 */
	minValue: 0,
	/**
	 * Max value for the number
	 */
	maxValue: 999999999,
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getNumberControlConversionProperties) : this.serializeFunctions = [this.getNumberControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseNumberControlConversionProperties) : this.deserializeFunctions = [this.parseNumberControlConversionProperties];
	},
	/**
	 * Conversion method that turns a number control into server fields to be persisted in the db
	 */
	getNumberControlConversionProperties: function() {
		return {
			integerMinValue: this.minValue,
			integerMaxValue: this.maxValue <= 0 ? 999999999 : this.maxValue,
			defaultValue: this.defaultValue
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseNumberControlConversionProperties: function(config) {
		this.set('minValue', config.integerMinValue);
		this.set('maxValue', config.integerMaxValue);
		this.set('defaultValue', config.defaultValue);
	}
});
/**
 * Mixin definition for a DecimalControl
 * Keeps track of min and max VALUE instead of length like in a string control
 */
glu.defModel('RS.formbuilder.DecimalControl', {
	/**
	 * The default value to show to the user when the control initially loads
	 */
	defaultValue: '',
	/**
	 * Min value for the decimal
	 */
	minValue: 0,
	/**
	 * Max value for the decimal
	 */
	maxValue: 999999999,
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getDecimalControlConversionProperties) : this.serializeFunctions = [this.getDecimalControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseDecimalControlConversionProperties) : this.deserializeFunctions = [this.parseDecimalControlConversionProperties];
	},
	/**
	 * Conversion method that turns a decimal control into server fields to be persisted in the db
	 */
	getDecimalControlConversionProperties: function() {
		return {
			decimalMinValue: this.minValue,
			decimalMaxValue: this.maxValue <= 0 ? 999999999 : this.maxValue,
			defaultValue: this.defaultValue
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseDecimalControlConversionProperties: function(config) {
		this.set('minValue', config.decimalMinValue);
		this.set('maxValue', config.decimalMaxValue);
		this.set('defaultValue', config.defaultValue);
	}
});

glu.defModel('RS.formbuilder.AllowInputControl', {

	allowUserInput: false,
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getAllowInputControlConversionProperties) : this.serializeFunctions = [this.getAllowInputControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseAllowInputControlConversionProperties) : this.deserializeFunctions = [this.parseAllowInputControlConversionProperties];
	},

	getAllowInputControlConversionProperties: function() {
		return {
			selectUserInput: this.allowUserInput
		}
	},
	parseAllowInputControlConversionProperties: function(config) {
		this.set('allowUserInput', config.selectUserInput);
	}
});
/**
 * Mixin defintion for a ChoiceControl
 * Choice control is used for dropdown and radiobuttons, and defines a list of available options for the user to choose from
 */
glu.defModel('RS.formbuilder.ChoiceControl', {
	/**
	 * The separator for serializing the multiple options
	 */
	choiceSeparator: '|&|',
	/**
	 * The separator for serializing the option to its value (checked)
	 */
	choiceValueSeparator: '|=|',
	/**
	 * List of options provided by the user
	 */
	options: {
		mtype: 'list',
		autoParent: true
	},

	columns: 0,
	columnsIsVisible$: function() {
		return !this.isCombobox;
	},
	when_columns_count_changes_rebuild_control: {
		on: ['columnsChanged'],
		action: function() {
			this.parentVM.rebuildControl(this, true);
		}
	},

	/**
	 * The actual height to be used for the control based on whether the size fieldset is collapsed or not
	 */
	actualHeight$: function() {
		var dh = this.defaultHeight,
			h = this.height;

		if (this.columns > 0) {
			dh += (this.optionsStore.getCount() / this.columns) * 20
		}
		if (this.labelAlign == 'top') {
			dh += 20;
			h += 20;
		}
		return this.collapsed ? dh : h;
	},

	/**
	 * Store linked to the options to display the options in a store if we need to
	 */
	optionsStore: {
		mtype: 'store',
		fields: ['text', 'id', 'displayText'],
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'options'
		}]
	},
	syncStore: function() {
		this.optionsStore.removeAll();
		this.options.foreach(function(option) {
			this.optionsStore.add(option)
		}, this);
	},
	/**
	 * The default value for the control to present to the user once the control loads
	 */
	defaultValue: '',

	/**
	 * Determines whether the options list is greater than one in order to not be allowed to remove the last option (otherwise you would loose the ability to add another option)
	 */
	optionsGreaterThanOne$: function() {
		return this.options.length > 1;
	},
	/**
	 * Determines which option is selected by default based on the defaultValue
	 */
	calculateDefaultValue$: function() {
		var optionValue = '',
			val = this.defaultValue;
		this.options.foreach(function(option) {
			if (option.id == val) optionValue = option.id;
		});
		return optionValue;
	},
	setCalculateDefaultValue: function(value) {
		this.options.foreach(function(option) {
			if (option.id == value) option.set('isSelected', true);
		}, this);
	},
	tempId: '',
	initMixin: function() {
		this.set('tempId', Ext.id());

		var i = 0;
		for (; i < 3; i++) {
			this.addOption(null, this.localize('Option') + ' ' + (this.options.length + 1));
		}

		this.tableColumnStore.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				tableName: this.dropdownChoiceTable
			});
		}, this);

		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getChoiceControlConversionProperties) : this.serializeFunctions = [this.getChoiceControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseChoiceControlConversionProperties) : this.deserializeFunctions = [this.parseChoiceControlConversionProperties];
	},
	/**
	 * Adds an option to the control with the provided text
	 * @param {Object} ctrl
	 * @param {Object} text
	 */
	addOption: function(ctrl, text) {
		var index = this.options.indexOf(ctrl)
		if (index == -1) index = this.options.length;

		var model = this.model({
			mtype: 'Option',
			text: text || ' '
		});
		model.init();

		this.options.insert(index + 1, model);
		if (this.options.length == 1) {
			this.set('defaultValue', this.options.getAt(0).id);
		}
		if (model.text == ' ') model.set('text', '');
	},
	/**
	 * Removes the provided option from the list of options
	 * @param {Object} ctrl
	 */
	removeOption: function(ctrl) {
		if (ctrl.id == this.defaultValue) this.set('defaultValue', '');
		this.options.remove(ctrl);
	},
	/**
	 * Conversion method that turns a choice control into server fields to be persisted in the db
	 */
	getChoiceControlConversionProperties: function() {
		var choices = [],
			choiceValue = '',
			values = [];
		this.options.foreach(function(option) {
			choices.push(option.text);
			if (option.isSelected) values.push(option.text);
		}, this);

		var choicesString = choices.join(this.choiceSeparator);
		var valuesString = values.join(this.choiceSeparator);

		return {
			choiceValues: choicesString,
			defaultValue: valuesString,
			widgetColumns: this.columns,

			choiceOptionSelection: this.activeOption,
			choiceTable: this.dropdownChoiceTable,
			choiceDisplayField: this.displayField,
			choiceValueField: this.valueField,
			choiceWhere: this.where,
			choiceField: this.parentField,
			choiceSql: this.activeOption == 2 ? this.sqlQuery : ''
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseChoiceControlConversionProperties: function(config) {
		this.set('columns', config.widgetColumns || 0);


		this.set('simple', false);
		this.set('advanced', false);
		this.set('custom', false);
		switch (config.choiceOptionSelection) {
			case 1:
				this.set('advanced', true);
				this.set('dropdownChoiceTable', config.choiceTable);
				this.set('displayField', config.choiceDisplayField);
				this.set('valueField', config.choiceValueField);
				this.set('where', config.choiceWhere);
				this.fieldStore.add({
					name: config.choiceField,
					value: config.choiceField
				})
				this.set('parentField', config.choiceField);
				break;
			case 2:
				this.set('custom', true);
				if (config.choiceSql) this.set('sqlQuery', config.choiceSql);
				break;
			case 0:
			default:
				this.set('simple', true);
				var choices = config.choiceValues ? config.choiceValues.split(this.choiceSeparator) : '';
				var values = config.defaultValue ? config.defaultValue.split(this.choiceSeparator) : '';
				var i = 0;
				for (; i < values.length; i++) {
					values[i] = values[i].split(this.choiceValueSeparator)[0];
				}
				this.options.removeAll();
				Ext.each(choices, function(choice) {
					this.options.add(this.model({
						mtype: 'Option',
						text: choice || ' ',
						id: Ext.id()
					}));
					this.options.getAt(this.options.length - 1).set('isSelected', Ext.Array.indexOf(values, choice) > -1);
				}, this);
				break;
		}

		if (config.choiceOptionSelection == 0) {
			//Do nothing
		} else {
			while (this.options.length > 1) this.options.removeAt(1);
			this.options.getAt(0).set('text', this.localize(config.choiceOptionSelection == 1 ? 'advanced' : 'custom'))
		}
	},
	/**
	 * Button Handler to open the ImportOption dialog box
	 */
	importOptions: function() {
		this.open({
			mtype: 'ImportOption'
		});
	},

	importOptionsIsVisible$: function() {
		return this.simple;
	},

	activeOption$: function() {
		if (this.custom) return 2;
		if (this.advanced) return 1;
		return 0;
	},

	simple: true,
	advanced: false,
	custom: false,

	displayOptionsSelections$: function() {
		return this.viewmodelName == 'ComboBoxField'
	},

	sqlQuery: 'select f1 as name, f2 as value from table where f3=\'${fieldName}\'',
	sqlQueryIsVisible$: function() {
		return this.custom
	},

	tableColumnStore: {
		mtype: 'store',
		fields: ['name', 'dbcolumnId', 'displayName', 'dbtype', 'uiType', 'integerMinValue', 'integerMaxValue', 'decimalMinValue', 'decimalMaxValue', 'stringMinLength', 'stringMaxLength', 'uiStringMaxLength', 'selectValues', 'choiceValues', 'referenceTable', 'referenceDisplayColumn'],
		data: [],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getcustomtablecolumns',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	dropdownChoiceTable: '',
	when_dropdown_choiceTable_changes_update_columns: {
		on: 'dropdownChoiceTableChanged',
		action: function() {
			this.tableColumnStore.load();
		}
	},
	displayField: '',
	displayFieldIsEnabled$: function() {
		return this.dropdownChoiceTable
	},
	valueField: '',
	valueFieldIsEnabled$: function() {
		return this.dropdownChoiceTable
	},
	where: '',
	whereIsEnabled$: function() {
		return this.dropdownChoiceTable
	},
	parentField: '',
	parentFieldIsEnabled$: function() {
		return this.where
	},

	fieldStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		listeners: {
			load: function() {
				var fieldNames = this.rootVM.getFormFieldNames();
				Ext.each(fieldNames, function(name) {
					this.add({
						name: name,
						value: name
					})
				}, this);
				this.sort('name', 'ASC');
			}
		}
	}


});
/**
 * Mixin definition for the MultiChoiceControl
 * Very similar to the choice control except that options can be multi-selected
 */
glu.defModel('RS.formbuilder.MultiChoiceControl', {
	/**
	 * The actual height to be used for the control based on whether the size fieldset is collapsed or not
	 */
	actualHeight$: function() {
		var dh = this.defaultHeight,
			h = this.height;

		if (this.columns > 0) {
			dh += (this.optionsStore.getCount() / this.columns) * 20
		}
		if (this.labelAlign == 'top') {
			dh += 20;
			h += 20;
		}
		return this.collapsed ? null : h;
	},
	/**
	 * The separator for serializing the multiple options
	 */
	choiceSeparator: '|&|',
	/**
	 * The separator for serializing the option to its value (checked)
	 */
	choiceValueSeparator: '|=|',
	/**
	 * List of options provided by the user
	 */
	options: {
		mtype: 'list',
		autoParent: true
	},
	/**
	 * Store linked to the options to display the options in a store if we need to
	 */
	optionsStore: {
		mtype: 'store',
		fields: ['text', 'id', 'displayText'],
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'options'
		}]
	},
	syncStore: function() {
		this.optionsStore.removeAll();
		this.options.foreach(function(option) {
			this.optionsStore.add(option)
		}, this);
	},

	columns: 0,
	columnsIsVisible$: function() {
		return !this.isCombobox;
	},
	when_columns_count_changes_rebuild_control: {
		on: ['columnsChanged'],
		action: function() {
			this.parentVM.rebuildControl(this, true);
		}
	},

	/**
	 * Determines whether there is more than one option in the list to make sure you cannot remove the last option (becaue the add button is on the option)
	 */
	optionsGreaterThanOne$: function() {
		return this.options.length > 1;
	},
	tempId: '',
	initMixin: function() {
		this.set('tempId', Ext.id());
		var i = 0;
		for (; i < 3; i++)
			this.addOption(null, this.localize('Option') + ' ' + (this.options.length + 1));
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getMultiChoiceControlConversionProperties) : this.serializeFunctions = [this.getMultiChoiceControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseMultiChoiceControlConversionProperties) : this.deserializeFunctions = [this.parseMultiChoiceControlConversionProperties];
	},
	/**
	 * Determines the default value based on which options are selected
	 */
	defaultValue$: function() {
		var values = [];
		for (var i = 0; i < this.options.length; i++) {
			if (this.options.getAt(i).isChecked) values.push(this.options.getAt(i).id);
		}
		return values;
	},
	/**
	 * Determines which option is selected by default based on the defaultValue
	 */
	calculateDefaultValue$: function() {
		return this.defaultValue;
	},
	setCalculateDefaultValue: function(val) {
		var i = 0,
			j, found;
		for (; i < this.options.length; i++) {
			found = false;

			for (j = 0; j < val.length; j++) {
				if (this.options.getAt(i).id == val[j]) {
					found = true;
				}
			}

			this.options.getAt(i).set('isChecked', found);
		}
	},
	/**
	 * Fires an event to indicate the options have changed when the checked state of an option is changed
	 * @param {Object} ctrl
	 */
	checkControl: function(ctrl) {
		this.fireEvent('optionschanged');
	},
	/**
	 * Adds an option to the list of options with the provided text
	 * @param {Object} ctrl
	 * @param {Object} text
	 */
	addOption: function(ctrl, text) {
		var index = this.options.indexOf(ctrl)
		if (index == -1) index = this.options.length - 1;

		var model = this.model({
			mtype: 'CheckOption',
			text: text || ' '
		});
		model.init();
		this.options.insert(index + 1, model);
		if (model.text == ' ') model.set('text', '');
	},
	/**
	 * Removes the provided option from the list of options
	 * @param {Object} ctrl
	 */
	removeOption: function(ctrl) {
		this.options.remove(ctrl);
	},
	/**
	 * Conversion method that turns a multi-choice control into server fields to be persisted in the db
	 */
	getMultiChoiceControlConversionProperties: function() {
		var choices = [],
			choiceValue = '',
			values = [];
		this.options.foreach(function(option) {
			choices.push(option.text);
			if (option.isChecked) values.push(option.text + this.choiceValueSeparator + 'true');
		}, this);

		var choicesString = choices.join(this.choiceSeparator);
		var valuesString = values.join(this.choiceSeparator);

		return {
			choiceValues: choicesString,
			defaultValue: valuesString,
			widgetColumns: this.columns
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseMultiChoiceControlConversionProperties: function(config) {
		this.set('columns', config.widgetColumns || 0);
		var choices = config.choiceValues ? config.choiceValues.split(this.choiceSeparator) : '';
		var values = config.defaultValue ? config.defaultValue.split(this.choiceSeparator) : '';
		var i = 0;
		for (; i < values.length; i++) {
			values[i] = values[i].split(this.choiceValueSeparator)[0];
		}
		this.options.removeAll();
		Ext.each(choices, function(choice) {
			this.options.add(this.model({
				mtype: 'Option',
				text: choice || ' ',
				isChecked: Ext.Array.indexOf(values, choice) > -1
			}));
		}, this);
	},
	/**
	 * Button Handler to open the ImportOption dialog
	 */
	importOptions: function() {
		this.open({
			mtype: 'ImportOption'
		});
	}
});

/**
 * Mixin definition for a ButtonControl
 * A button is not like a form control at all, it doesn't have a field label, but has text to display on the button and a list of actions to perform when the button is clicked
 */
glu.defModel('RS.formbuilder.ButtonControl', {
	mixins: ['DependencyControl'],
	fields: ['operation', 'operationName', 'customTableDisplay', 'font', 'fontSize', 'fontColor', 'backgroundColor'],
	asObject: function() {
		var buttonControl = {
			operation: this.operation,
			operationName: this.operationName,
			customTableDisplay: this.customTableDisplay,
			dependenciesJson: this.getDependencyControlConversionProperties().dependencies,
			id: this.id
		}

		if (this.type == 'button') {
			Ext.apply(buttonControl, {
				font: this.font,
				fontSize: this.fontSize,
				fontColor: this.fontColor,
				backgroundColor: this.backgroundColor
			});
		}

		return buttonControl;
	},
	id: '',
	/**
	 * Operation to perform when the button is clicked by the user
	 */
	operation: 'New Button',
	/**
	 * Translated operation name to display to the user
	 */
	operationName$: function() {
		return this.operation;
	},
	/**
	 * Sets the operation to the provided value
	 * @param {Object} val
	 */
	setOperationName: function(val) {
		this.set('operation', val);
		var idx = this.parentList.indexOf(this);
		this.parentVM.set('formButtonsSelections', [this.parentVM.treeStore.getRootNode().getChildAt(idx)]);
	},

	customTableDisplay: false,
	/**
	 * For view purposes we need to define this object as a button to separate it from Actions
	 */
	type: 'button',
	buttonXType: 'button',
	leaf: false,
	/**
	 * A list of actions to be performed when the button is clicked
	 */
	actions: {
		mtype: 'list',
		autoParent: true
	},
	/**
	 * Store to display the list of actions to the user in a grid
	 */
	actionsStore: {
		mtype: 'store',
		fields: ['operationName', 'value'],
		data: [],
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'actions'
		}]
	},

	defaultFont: 'Verdana',
	defaultFontSize: '10',
	defaultFontColor: '333333',
	defaultBackgroundColor: 'DDDDDD',

	editButton: false,

	init: function() {
		this.set('editButton', this.type == 'button');
		if (!this.id) this.set('id', Ext.id());
		if (this.operation == 'New Button') this.set('operation', this.localize('newButton'));
		if (this.type == 'button') {
			if (!this.font) {
				this.resetButtonStyle();
			}
		} else {
			this.set('operation', this.localize(this.type));
			this.set('leaf', true);
		}
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getButtonControlConversionProperties) : this.serializeFunctions = [this.getButtonControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseButtonControlConversionProperties) : this.deserializeFunctions = [this.parseButtonControlConversionProperties];
	},
	/**
	 * Button Handler that forwards this event to the parentVM to remove this control from the form
	 */
	removeControl: function() {
		this.parentVM.removeControl(this);
	},
	/**
	 * Conversion method that turns a button control into a server buttonPanel to be persisted in the db
	 */
	getButtonControlConversionProperties: function(isSave) {
		var convertedActions = [];
		this.actions.foreach(function(action, index) {
			var params = {};
			switch (action.operation) {
				case 'MAPPING':
					if (action.mappingFrom && action.mappingTo) {
						params[action.mappingTo] = action.mappingFrom;
					}
					break;
	
				case 'SCRIPT':
					Ext.apply(params, {
						TIMEOUT: action.TIMEOUT
					});
					break;
	
				case 'EVENT':
					Ext.apply(params, {
						EVENT_EVENTID: action.EVENT_EVENTID,
						PROBLEMID: action.PROBLEMID,
						EVENT_REFERENCE: action.PROBLEMID,
						mappingFrom: action.PROBLEMID == 'MAPFROM' ? action.mappingFrom : ''
					});
					break;
	
				case 'RUNBOOK':
					Ext.apply(params, {
						PROBLEMID: action.PROBLEMID
					});
					break;
	
				case 'ACTIONTASK':
					Ext.apply(params, {
						PROBLEMID: action.PROBLEMID
					});
					break;
	
				case 'DB':
					if (action.dbAction == 'DELETE') {
						Ext.apply(params, {
							showConfirmation: action.showConfirmation
						});
					}
					break;

				case 'SIR':
					if (isSave && action.sirAction === 'saveCaseData') {
						action.dbAction = 'SUBMIT';
						action.operation = 'DB';
						Ext.apply(params, {
							sirAction: action.sirAction
						});
					} else {
						action.dbAction = action.sirAction; // since sirAction is not supported in BE, just pass sirAction to the dbAction
					}
					break;
	
				case 'MESSAGEBOX':
					Ext.apply(params, {
						messageBoxTitle: action.messageBoxTitle,
						messageBoxMessage: action.messageBoxMessage,
						messageBoxYesText: action.messageBoxYesText,
						messageBoxNoText: action.messageBoxNoText
					});
					break;
	
				case 'RELOAD':
					Ext.apply(params, {
						reloadDelay: action.reloadDelay,
						reloadTarget: action.reloadTarget,
						reloadCondition: action.reloadCondition,
						reloadValue: action.reloadValue
					});
					break;
	
				case 'GOTO':
					Ext.apply(params, {
						gotoTabName: action.gotoTabName
					});
					break;
	
				default:
					break;
			}

			convertedActions.push({
				id: action.id,
				crudAction: action.dbAction,
				operation: action.operation,
				runbook: action.RUNBOOK,
				actionTask: action.ACTIONTASK,
				script: action.SCRIPT,
				orderNumber: index,
				redirectTarget: action.redirectTarget,
				redirectUrl: action.redirectUrl,
				additionalParam: this.serializeParams(params)
			});
		}, this);

		var deps = this.getDependencyControlConversionProperties().dependencies;
		var buttonControl = {
			id: this.id.indexOf('ext') > -1 ? '' : this.id,
			displayName: this.operation,
			customTableDisplay: this.customTableDisplay,
			name: 'CUSTOM',
			type: this.type,
			orderNumber: this.orderNumber,
			actions: convertedActions,
			dependencies: deps,
		};

		if (this.type == 'button') {
			Ext.apply(buttonControl, {
				font: this.font,
				fontSize: this.fontSize,
				fontColor: this.fontColor,
				backgroundColor: this.backgroundColor
			});
		}

		return buttonControl;
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseButtonControlConversionProperties: function(config, actionsOnly) {
		this.set('operation', config.displayName);
		this.set('id', config.id);
		this.set('type', config.type);
		this.set('customTableDisplay', config.customTableDisplay);
		if (config.type == 'button') {
			this.set('font', config.font || this.defaultFont);
			this.set('fontSize', config.fontSize || this.defaultFontSize);
			this.set('fontColor', config.fontColor || this.defaultFontColor);
			this.set('backgroundColor', config.backgroundColor || this.defaultBackgroundColor);
		} else {
			// this.set('operation', this.localize(config.type));
			this.set('leaf', true);
		}
		this.parseDependencyControlConversionProperties(config);
		if (actionsOnly) {
			this.actions.removeAll();
			Ext.each(config.actions, function(actionConfig, index) {
				var action = this.model({
					mtype: 'Action'
				});
				action.set('id', actionConfig.id || '')
				action.set('dbAction', actionConfig.crudAction);
				action.set('operation', actionConfig.operation || '');
				action.set('RUNBOOK', actionConfig.runbook || '');
				action.set('ACTIONTASK', actionConfig.actionTask || '');
				action.set('SCRIPT', actionConfig.script || '');
				action.set('redirectTarget', actionConfig.redirectTarget || '');
				action.set('redirectUrl', actionConfig.redirectUrl || '');
				if (actionConfig.additionalParam && Ext.isString(actionConfig.additionalParam)) {
					var additionalParam = this.deserializeParams(actionConfig.additionalParam);
					switch (action.operation) {
						case 'EVENT':
							action.set('PROBLEMID', additionalParam.PROBLEMID || '');
							action.set('EVENT_EVENTID', additionalParam.EVENT_EVENTID || '');
							action.set('EVENT_REFERENCE', additionalParam.EVENT_REFERENCE || '');
							action.set('mappingFrom', additionalParam.mappingFrom || '');
							break;
	
						case 'RUNBOOK':
							action.set('PROBLEMID', additionalParam.PROBLEMID || '');
							break;
	
						case 'ACTIONTASK':
							action.set('PROBLEMID', additionalParam.PROBLEMID || '');
							break;
	
						case 'SCRIPT':
							action.set('TIMEOUT', additionalParam.TIMEOUT);
							if (additionalParam.TIMEOUT == '') {
								action.set('waitSeconds', 0);
							}
							else if (additionalParam.TIMEOUT > 0) {
								action.set('waitSeconds', additionalParam.TIMEOUT / 1000);
							}
							break;
	
						case 'MAPPING':
							for (var key in additionalParam) {
								action.set('mappingFrom', additionalParam[key]);
								action.set('mappingTo', key);
							}
							break;
	
						case 'DB':
							if (action.dbAction == 'DELETE') {
								action.set('showConfirmation', additionalParam.showConfirmation);
							} else if (action.dbAction == 'SUBMIT' && additionalParam.sirAction == 'saveCaseData') {
								action.set('sirAction', additionalParam.sirAction);
								action.set('operation', 'SIR');
							}
							break;
	
						case 'MESSAGEBOX':
							action.set('messageBoxTitle', additionalParam.messageBoxTitle);
							action.set('messageBoxMessage', additionalParam.messageBoxMessage);
							action.set('messageBoxYesText', additionalParam.messageBoxYesText);
							action.set('messageBoxNoText', additionalParam.messageBoxNoText);
							break;
	
						case 'RELOAD':
							action.set('reloadDelay', additionalParam.reloadDelay);
							action.set('reloadTarget', additionalParam.reloadTarget);
							action.set('reloadCondition', additionalParam.reloadCondition);
							action.set('reloadValue', additionalParam.reloadValue);
							break;
	
						case 'GOTO':
							action.set('gotoTabName', additionalParam.gotoTabName)
							break;
						default:
							break;
					}
				}
				this.addRealAction(action);
				this.parentList.fireEvent('appendchild', action, this.parentList.indexOf(this));
			}, this);
		}
	},
	/**
	 * The separator for serializing the multiple options
	 */
	choiceSeparator: '|&|',
	/**
	 * The separator for serializing the option to its value (checked)
	 */
	choiceValueSeparator: '|=|',
	serializeParams: function(params) {
		var serialized = '';
		Ext.Object.each(params, function(param) {
			if (serialized) serialized += this.choiceSeparator;
			serialized += param + this.choiceValueSeparator + params[param];
		}, this);
		return serialized;
	},
	deserializeParams: function(paramString) {
		var params = {},
			paramsSplit = paramString.split(this.choiceSeparator);

		Ext.each(paramsSplit, function(split) {
			var val = split.split(this.choiceValueSeparator);
			if (val.length > 1) {
				params[val[0]] = val[1];
			}
		}, this);

		return params;
	},
	/**
	 * Array of selections in the grid to determine which action is selected by the user
	 */
	actionsSelections: [],
	/**
	 * Button Handler to add an action that opens a dialog box with the action properties
	 */
	addAction: function() {
		this.open({
			mtype: 'Action'
		});
	},
	/**
	 * Event Handler to actually add the action to the button with the provided action object (unlike addAction that raises the dialog)
	 * @param {Object} action
	 */
	addRealAction: function(action) {
		action.set('isNew', false);
		this.actions.add(action);
	},
	/**
	 * Button Handler to edit an action that opens a dialog box with the action properties
	 */
	editAction: function() {
		var action = this.actions.getAt(this.actionsStore.indexOf(this.actionsSelections[0]));
		this.open(Ext.applyIf({
			mtype: 'Action',
			isNew: false
		}, action.asObject()));
	},
	/**
	 * Event Handler to actually edit the action in the button with the provided action properties (unlike editAction that raises the dialog)
	 * @param {Object} actionProperties
	 */
	editRealAction: function(actionProperties) {
		var index = this.actionsStore.indexOf(this.actionsSelections[0]),
			action = this.actions.getAt(index);
		action.loadData(actionProperties);
		this.actions.fireEvent('edited', actionProperties, index);
	},
	/**
	 * Determines whether the edit action should be enabled based on the selection of the action
	 */
	editActionIsEnabled: {
		on: ['actionsSelectionsChanged'],
		formula: function() {
			return this.actionsSelections.length == 1;
		}
	},
	/**
	 * Deletes the selected action from the button
	 */
	removeAction: function() {
		for (var i = 0; i < this.actionsSelections.length; i++) {
			this.actions.removeAt(this.actionsStore.indexOf(this.actionsSelections[i]));
		}
	},
	/**
	 * Determines if an action is selected and enables the delete button if an action is selected
	 */
	removeActionIsEnabled: {
		on: ['actionsSelectionsChanged'],
		formula: function() {
			return this.actionsSelections.length == 1;
		}
	},

	fontStore: {
		mtype: 'store',
		fields: [{
			name: 'font',
			convert: function(v, r) {
				return r.raw
			}
		}],
		data: ['Helvetica', 'Verdana', 'Times New Roman', 'Garamond', 'Courier New']
	},

	fontSizeStore: {
		mtype: 'store',
		fields: [{
			name: 'fontsize',
			convert: function(v, r) {
				return r.raw
			}
		}],
		data: [6, 8, 9, 10, 12, 14, 18, 24, 30, 36, 48, 60]
	},

	font: '',
	fontSize: '',
	fontColor: '',
	backgroundColor: '',

	fontFamily$: function() {
		return this.font;
	},

	fontColorIcon$: function() {
		return 'rs-fontcolor-icon hex'+this.fontColor;
	},

	fillColorIcon$: function() {
		return 'rs-fillcolor-icon hex'+this.backgroundColor;
	},

	updateButtonFont: function(font) {
		this.set('font', font);
	},

	updateButtonFontSize: function(fontSize) {
		this.set('fontSize', fontSize);
	},

	updateButtonFontColor: function(color) {
		this.set('fontColor', color);
	},

	updateButtonBackgroundColor: function(color) {
		this.set('backgroundColor', color);
	},

	resetButtonStyle: function() {
		this.set('font', this.defaultFont);
		this.set('fontSize', this.defaultFontSize);
		this.set('fontColor', this.defaultFontColor);
		this.set('backgroundColor', this.defaultBackgroundColor);
	},

	applyButton: function() {
		this.doClose();
		this.parentVM.editRealButton(this.asObject());
	},
	cancel: function() {
		this.doClose();
	}
});
/**
 * Mixin definition for a DatabaseControl
 * A database control is persisted in the database or can be an input field on the form, but this keeps track of that information
 */
glu.defModel('RS.formbuilder.DatabaseControl', {
	/**
	 * Table to store the control in
	 */
	table: '',
	/**
	 * Column to store the control in
	 */
	column: '',
	columnIsValid$: function() {
		if (!this.column || this.inputType == 'INPUT') return true;
		//determine the column type from the column name (if this isn't a new column) and check its type with the type of this control to make sure they match
		var record = this.parentVM.columnStore.findRecord('name', this.column, 0, false, false, true);
		if (record && record.data.uiType && this.dbColumnType.indexOf(record.data.uiType) === -1) {
			return this.localize('columnInvalidText', [this.dbColumnType, record.data.uiType]);
		}
		//if no record, then its a new column
		if (this.column.length > 30) return this.localize('columnTooLongInvalidText');
		if (/[^A-Z|a-z|_|0-9]/g.test(this.column)) return this.localize('columnSpecialCharactersInvalidText');
		return true;
	},
	when_column_changes_update_field_label: {
		on: ['columnChanged'],
		action: function() {
			var record = this.parentVM.columnStore.findRecord('name', this.column, 0, false, false, true)
			if (record) {
				this.set('fieldLabel', record.data.displayName);
				//also update the min/max length/value as well as any options
				for (var key in record.data) {
					var realKey = '',
						isMax = false;
					switch (key) {
						case 'stringMaxLength':
							realKey = 'allowedMaxValue';
							break;
						case 'uiStringMaxLength':
							realKey = 'maxLength';
							break;
						case 'stringMinLength':
							realKey = 'minLength';
							break;
						case 'integerMaxValue':
						case 'decimalMaxValue':
							realKey = 'maxValue';
							if (!Ext.isDefined(this[realKey])) {
								realKey = 'maxLength';
							}
							isMax = true;
							break;
						case 'integerMinValue':
						case 'decimalMinValue':
							realKey = 'minValue';
							break;
						case 'referenceTable':
							if (Ext.isDefined(this.referenceTable)) this.set(key, record.data[key]);
							break;
						case 'referenceDisplayColumn':
							if (Ext.isDefined(this.referenceDisplayColumn)) this.set(key, record.data[key]);
							break;
					}
					if (realKey && Ext.isDefined(this[realKey]) && record.data[key]) {
						this.set(realKey, record.data[key]);
						if (realKey === 'allowedMaxValue' && Ext.isDefined(this.columnSize)) {
							this.set('columnSize', record.data[key]);
						}
						if (isMax) {
							if (Ext.isDefined(this.columnSize)) {
								this.set('columnSize', record.data[key]);
							}
							this.set('allowedMaxValue', record.data[key] == 4001 ? 999999999 : record.data[key]);
						}
					}

					//options
					if (Ext.isDefined(this.options) && (key == 'selectValues' || key == 'choiceValues')) {
						//parse the options and fill them up
						var values = record.data[key];
						if (values) {
							if (this.parseChoiceControlConversionProperties) this.parseChoiceControlConversionProperties({
								choiceValues: values
							});
							else if (this.parseMultiChoiceControlConversionProperties) this.parseMultiChoiceControlConversionProperties({
								choiceValues: values
							})
						}
					}
				}
			}
		}
	},
	columnIsEditable$: function() {
		var isAdmin = this.rootVM.isAdmin,
			isNew = false;
		var record = this.parentVM.columnStore.findRecord('name', this.column, 0, false, false, true);
		if (record) {
			if (record.data.name == 'newColumn') isNew = true;
		} else isNew = true;

		return isAdmin && isNew;
	},
	/**
	 * Name to pass the value of this control to when submitting a form
	 */
	inputName: '',
	
	regWord: /[^\w]+/, 

	regNumber: /^[0-9]+/,

	inputNameIsValid$: function() {
		var result = true;

		if (this.regWord.test(this.inputName)) {
			result = this.localize('fieldNameInvalid');
		}

		if (result && this.regNumber.test(this.inputName)) {
			result = this.localize('fieldNameStartInvalid');
		}

		return result;
	},

	when_fieldName_changes_on_input_field_update_fieldLabel_automatically: {
		on: ['inputNameChanged'],
		action: function() {			
			if (this.inputType === 'INPUT' && this.inputName !== null) {
				var fieldLabelIsTracked = typeof this.fieldLabelIsChanged !== 'undefined';

				if(fieldLabelIsTracked) {
					if(!this.fieldLabelIsChanged) {
						this.set('fieldLabel', this.inputName);
					}
				} else {
					this.set('fieldLabel', this.inputName);
				}				
			}
		}
	},

	blurDatabaseFieldName: function() {
		if (this.inputType === 'DB') {
			if (this.column.indexOf('sys_') === 0) {
				this.set('column', this.column.toLowerCase());	
			} else if (this.column.indexOf('u_') !== 0) {
				this.parentVM.columnStore.clearFilter()
				this.set('column', 'u_' + this.column.toLowerCase());
			}

			//replace spaces automatically with _'s
			this.set('column', this.column.replace(/ /g, '_'));
		}		
	},
	/**
	 * Input type of the control.  Either 'DB' or 'INPUT'
	 */
	inputType: 'INPUT',

	/**
	 * Tracker to determine if the input radiobutton is selected
	 */
	inputTypeSelected$: function() {
		return this.inputType == 'INPUT'
	},
	inputTypeIsEnabled$: function() {
		return Ext.Array.indexOf(['ReferenceField', 'SequenceField', 'JournalField'], this.viewmodelName) == -1;
	},
	dbTypeIsEnabled$: function() {
		return this.parentVM.dbTable;
	},
	/**
	 * Tracker to determine if the db radiobutton is selected
	 */
	dbTypeSelected$: function() {
		return this.inputType == 'DB'
	},

	dbFieldName$: function() {
		var fieldName = this.inputType == 'INPUT' ? this.inputName : this.column;
		if (!fieldName) fieldName = this.localize('untitledField');
		return fieldName;
	},
	/**
	 * Formula for setting the inputType properly based on the change of
	 */
	whenTypeSelectionChanges_UpdateInputType$: {
		on: ['inputTypeSelectedChanged', 'dbTypeSelectedChanged'],
		action: function() {
			if (this.inputTypeSelected) this.set('inputType', 'INPUT');
			else this.set('inputType', 'DB');
		}
	},
	/**
	 * Determines whether the user has indicated if the input type is a db or input type
	 */
	hideColumnCombobox$: function() {
		return this.inputType == 'INPUT';
	},

	initMixin: function() {
		this.set('inputName', this.fieldLabel);
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getDatabaseControlConversionProperties) : this.serializeFunctions = [this.getDatabaseControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseDatabaseControlConversionProperties) : this.deserializeFunctions = [this.parseDatabaseControlConversionProperties];
	},

	/**
	 * Conversion method that turns a database control into server fields to be persisted in the db
	 */
	getDatabaseControlConversionProperties: function() {
		if (this.inputType == 'DB' && this.column.indexOf('sys_') !== 0 && this.column.indexOf('u_') !== 0) this.set('column', 'u_' + this.column);
		this.parentVM.columnStore.clearFilter();
		//when the exsiting field gets renamed..we need to use dbcolumnId to find it in the store
		var record = this.parentVM.columnStore.findRecord('dbcolumnId', this.dbcolumnId, 0, false, false, true);
		//when a new field gets created...and we want to use the existing dbtable.. then we need the db column name to map the field...
		if (!record)
			record = this.parentVM.columnStore.findRecord('name', this.column, 0, false, false, true);
		return {
			dbtable: this.table || this.parentVM.dbTable,
			dbcolumn: this.inputType == 'INPUT' ? '' : this.column,
			columnModelName: this.inputType == 'INPUT' ? '' : this.column,
			dbcolumnId: this.inputType == 'INPUT' ? '' : record ? record.data.dbcolumnId : '',
			sourceType: this.inputType,
			stringMaxLength: this.inputType == 'INPUT' ? this.maxLength : this.columnSize
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseDatabaseControlConversionProperties: function(config) {
		this.set('table', config.dbtable);
		this.set('inputType', config.sourceType);
		this.set('inputName', config.name);
		this.set('fieldLabel', config.fieldLabel);
		// this.set('inputTypeSelected', config.sourceType == 'INPUT');
		// this.set('dbTypeSelected', config.sourceType == 'DB');
		this.set('column', config.dbcolumn);
		if (this.inputType == 'DB' && config.stringMaxLength) {
			if (Ext.isDefined(this.columnSize)) this.set('columnSize', config.stringMaxLength);
			this.set('allowedMaxValue', config.stringMaxLength == 4001 ? 999999999 : config.stringMaxLength);
		}
		if (this.inputType == 'INPUT') {

		}
	}
});

glu.defModel('RS.formbuilder.ReferenceControl', {
	referenceTable: '',
	referenceDisplayColumn: '',
	referenceTarget: '',

	customTablesList: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getcustomtablesforref',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	referenceTableIsValid$: function() {
		if (!this.referenceTable) return this.localize('referenceTableInvalid');
		return true;
	},
	referenceDisplayColumnIsValid$: function() {
		if (!this.referenceDisplayColumn) return this.localize('referenceColumnInvalid');
		return true;
	},

	referenceColumnStore: {
		mtype: 'store',
		fields: ['name', 'dbcolumnId', 'displayName', 'dbtype', 'uiType', 'integerMinValue', 'integerMaxValue', 'decimalMinValue', 'decimalMaxValue', 'stringMinLength', 'stringMaxLength', 'uiStringMaxLength', 'selectValues', 'choiceValues', 'referenceTable', 'referenceDisplayColumn'],
		data: [],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getcustomtablecolumns',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	when_table_changes_update_columns: {
		on: ['referenceTableChanged'],
		action: function() {
			this.referenceColumnStore.load();
		}
	},

	initMixin: function() {
		this.customTablesList.load({
			scope: this,
			callback: this.customTablesLoaded
		});

		this.referenceColumnStore.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				tableName: this.referenceTable
			});
		}, this);

		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getReferenceControlConversionProperties) : this.serializeFunctions = [this.getReferenceControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseReferenceControlConversionProperties) : this.deserializeFunctions = [this.parseReferenceControlConversionProperties];
	},

	customTablesLoaded: function(records, operation, success) {
		Ext.each(records, function(record, index) {
			if (record.data.name.indexOf('_fu') != -1 && record.data.name.indexOf('_fu') == record.data.name.length - 3) {
				this.customTablesList.remove(record)
			}
		}, this)
	},

	getReferenceControlConversionProperties: function() {
		return {
			referenceTable: this.referenceTable,
			referenceDisplayColumn: this.referenceDisplayColumn,
			referenceTarget: this.referenceTarget
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseReferenceControlConversionProperties: function(config) {
		this.set('referenceTarget', config.referenceTarget);
		this.set('referenceTable', config.referenceTable);
		this.set('referenceDisplayColumn', config.referenceDisplayColumn);
		this.referenceColumnStore.load();
	}
});

glu.defModel('RS.formbuilder.ReferenceTableControl', {
	id: '',
	fieldLabel: '',
	fieldLabelIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceTable: '',
	referenceTableIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceColumn: '',
	referenceColumnIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceColumns: '',
	referenceColumnsIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceColumnsIsValid$: function() {
		return this.referenceColumns.length > 0 ? true : this.localize('invalidReferenceColumns');
	},
	allowReferenceTableAdd: true,
	allowReferenceTableAddIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	allowReferenceTableRemove: true,
	allowReferenceTableRemoveIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	allowReferenceTableView: true,
	allowReferenceTableViewIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceTableUrl: '',
	referenceTableUrlIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceTableViewPopup: false,
	referenceTableViewPopupIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceTableViewPopupWidth: 600,
	referenceTableViewPopupWidthIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1 && this.referenceTableViewPopup;
	},
	referenceTableViewPopupHeight: 600,
	referenceTableViewPopupHeightIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1 && this.referenceTableViewPopup;
	},
	referenceTableViewPopupTitle: '',
	referenceTableViewPopupTitleIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1 && this.referenceTableViewPopup;
	},

	referenceTableUrlNewTab: true,

	fields: ['fieldLabel', 'referenceTable', 'referenceColumn', 'referenceColumns', 'allowReferenceTableAdd', 'allowReferenceTableRemove', 'allowReferenceTableView', 'referenceTableUrl', 'referenceTableViewPopup', 'referenceTableViewPopupWidth', 'referenceTableViewPopupHeight', 'referenceTableViewPopupTitle'],

	when_field_changes_update_current_reference: {
		on: ['fieldLabelChanged', 'referenceTableChanged', 'referenceColumnChanged', 'referenceColumnsChanged', 'allowReferenceTableAddChanged', 'allowReferenceTableRemoveChanged', 'allowReferenceTableViewChanged', 'referenceTableUrlChanged', 'referenceTableViewPopupChanged', 'referenceTableViewPopupWidthChanged', 'referenceTableViewPopupHeightChanged', 'referenceTableViewPopupTitleChanged'],
		action: function() {
			//persist to current reference
			if (Ext.isObject(this.currentReference)) {
				this.currentReference.referenceColumnStore.removeAll();
				this.referenceColumnStore.each(function(record) {
					this.currentReference.referenceColumnStore.add(record);
				}, this);
				Ext.Object.each(this.currentReference.asObject(), function(attribute) {
					this.currentReference.set(attribute, this[attribute]);
				}, this);
			}
		}
	},
	when_reference_table_changes_update_reference_column_too: {
		on: ['referenceTableChanged'],
		action: function() {
			var record = this.referenceStore.findRecord('value', this.referenceTable, 0, false, false, true)
			if (record) {
				this.set('referenceColumn', record.data.name);
			}
		}
	},

	referenceTables: {
		mtype: 'list',
		autoParent: true
	},

	referenceTablesSelections: [],
	currentReference: 0,
	when_current_reference_changes_update_selection_in_grid: {
		on: 'currentReferenceChanged',
		action: function() {
			if (Ext.isObject(this.currentReference)) {
				this.changeReferenceTablesSelections([this.referenceTablesStore.getAt(this.referenceTables.indexOf(this.currentReference))]);
			}
		}
	},
	when_selection_changes_update_fields: {
		on: ['referenceTablesSelectionsChanged'],
		action: function() {
			var sel = this.referenceTablesSelections[0];
			if (sel) {
				var selection = this.referenceTables.getAt(this.referenceTablesStore.indexOf(sel));
				if (selection) {
					//Persist dependencies
					// if(Ext.isObject(this.currentReference)) {
					// 	var deps = this.getDependencyControlConversionProperties();
					// 	this.currentReference.parseDependencyControlConversionProperties(deps);
					// }
					var temp = this.currentReference;
					this.currentReference = null;
					Ext.Object.each(selection.asObject(), function(attribute) {
						if (Ext.isDefined(this[attribute]) && attribute != 'id') this.set(attribute, selection[attribute]);
					}, this);
					// this.parseDependencyControlConversionProperties(selection.getDependencyControlConversionProperties());
					this.currentReference = temp;
					this.set('currentReference', selection);
				}
			}
		}
	},

	referenceTablesStore: {
		mtype: 'store',
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'referenceTables'
		}],
		fields: ['fieldLabel']
	},

	referenceDisplayStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},

	referenceStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getReferenceTables',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	referenceColumnStore: {
		mtype: 'store',
		fields: ['name', 'dbcolumnId', 'displayName', 'dbtype', 'uiType', 'integerMinValue', 'integerMaxValue', 'decimalMinValue', 'decimalMaxValue', 'stringMinLength', 'stringMaxLength', 'uiStringMaxLength', 'selectValues', 'choiceValues', 'referenceTable', 'referenceDisplayColumn'],
		data: [],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getcustomtablecolumns',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	when_referenceTable_changes_update_reference_columns: {
		on: ['referenceTableChanged'],
		action: function() {
			this.set('referenceColumns', '');
			this.referenceColumnStore.load();
		}
	},

	referenceColumnsIsEnabled$: function() {
		return this.referenceTable;
	},

	referenceTableIsValid$: function() {
		if (!this.referenceTable) return this.localize('referenceTableInvalid')
		return true;
	},

	referenceTableUrlIsValid$: function() {
		if ((this.allowReferenceTableAdd || this.allowReferenceTableView) && !this.referenceTableUrl) return this.localize('referenceTableUrlInvalid');
		return true;
	},

	when_referenceTableUrl_changed_check_match_to_wiki: {
		on: ['referenceTableUrlChanged'],
		action: function() {
			if ((this.parentVM.wikiNames && this.parentVM.wikiNames.getById(this.referenceTableUrl)) || (this.parentVM.parentVM.wikiNames && this.parentVM.parentVM.wikiNames.getById(this.referenceTableUrl))) {
				Ext.defer(function() {
					this.set('referenceTableUrl', '/resolve/service/wiki/view/' + this.referenceTableUrl.split('.').join('/'));
				}, 100, this);
			}
		}
	},

	initMixin: function() {
		this.referenceStore.load({
			params: {
				tableName: this.parentVM.dbTable
			}
		});
		this.referenceColumnStore.on('beforeload', function(store, operation, eOpts) {
			if (!this.referenceTable) return false
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				tableName: this.referenceTable
			});
		}, this);

		//Initialize first reference
		this.referenceTables.add(this.model({
			mtype: 'ReferenceTable',
			fieldLabel: this.localize('references')
		}));
		//Initially select the only record so that all the fields are initialized properly
		this.set('currentReference', this.referenceTables.getAt(0));
		this.changeReferenceTablesSelections([this.referenceTablesStore.getAt(0)]);

		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getReferenceTableControlConversionProperties) : this.serializeFunctions = [this.getReferenceTableControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseReferenceTableControlConversionProperties) : this.deserializeFunctions = [this.parseReferenceTableControlConversionProperties];
	},

	changeReferenceTablesSelections: function(selections) {
		this.set('referenceTablesSelections', selections);
		this.referenceTablesStore.fireEvent('cellSelectionVMChanged', this, selections);
	},

	addReference: function() {
		var selectedIndex = -1;
		var sel = this.referenceTablesSelections[0];
		if (sel) selectedIndex = this.referenceTablesStore.indexOf(sel);
		if (selectedIndex == -1) selectedIndex = this.referenceTables.length;
		else selectedIndex++;
		this.referenceTables.insert(selectedIndex, this.model({
			mtype: 'ReferenceTable',
			fieldLabel: this.localize('unknownReference')
		}));

		this.set('referenceTablesSelections', [this.referenceTablesStore.getAt(selectedIndex)]);
	},
	removeReference: function() {
		var sel = this.referenceTablesSelections[0];
		if (sel) {
			var selection = this.referenceTables.getAt(this.referenceTablesStore.indexOf(sel));
			if (selection) {
				this.referenceTables.remove(selection);
				this.changeReferenceTablesSelections([this.referenceTablesStore.getAt(0)]);
			}
		}
	},
	removeReferenceIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},

	getReferenceTableControlConversionProperties: function() {

		//Go through each of the ReferenceTables and combine them to persist on the backend
		var displayName = '',
			refGridReferenceByTable = '',
			refGridReferenceColumnName = '',
			refGridSelectedReferenceColumns = '',
			allowReferenceTableAdd = '',
			allowReferenceTableRemove = '',
			allowReferenceTableView = '',
			referenceTableUrl = '',
			referenceTableViewPopup = '',
			referenceTableViewPopupWidth = '',
			referenceTableViewPopupHeight = '',
			referenceTableViewPopupTitle = '';

		this.referenceTables.foreach(function(referenceTable, index) {
			displayName += (index > 0 ? '|' : '') + referenceTable.fieldLabel;
			refGridReferenceByTable += (index > 0 ? '|' : '') + referenceTable.referenceTable;
			refGridReferenceColumnName += (index > 0 ? '|' : '') + referenceTable.referenceColumn;
			allowReferenceTableAdd += (index > 0 ? '|' : '') + referenceTable.allowReferenceTableAdd;
			allowReferenceTableRemove += (index > 0 ? '|' : '') + referenceTable.allowReferenceTableRemove;
			allowReferenceTableView += (index > 0 ? '|' : '') + referenceTable.allowReferenceTableView;
			referenceTableUrl += (index > 0 ? '|' : '') + referenceTable.referenceTableUrl;
			referenceTableViewPopup += (index > 0 ? '|' : '') + referenceTable.referenceTableViewPopup;
			referenceTableViewPopupWidth += (index > 0 ? '|' : '') + referenceTable.referenceTableViewPopupWidth;
			referenceTableViewPopupHeight += (index > 0 ? '|' : '') + referenceTable.referenceTableViewPopupHeight;
			referenceTableViewPopupTitle += (index > 0 ? '|' : '') + referenceTable.referenceTableViewPopupTitle;

			var displayColumns = [];

			Ext.each(referenceTable.referenceColumns, function(refColumn) {
				var rec = referenceTable.referenceColumnStore.findRecord('name', refColumn, 0, false, false, true);
				if (rec) {
					displayColumns.push({
						name: rec.data.displayName,
						value: refColumn,
						dbtype: rec.data.dbtype
					});
				}
			}, this);
			refGridSelectedReferenceColumns += (index > 0 ? '|' : '') + (displayColumns.length > 0 ? Ext.encode(displayColumns) : '');
		}, this);

		return {
			id: this.id,
			sourceType: 'INPUT',
			name: 'u_referenceTable_',
			displayName: displayName,
			refGridReferenceByTable: refGridReferenceByTable,
			refGridReferenceColumnName: refGridReferenceColumnName,
			refGridSelectedReferenceColumns: refGridSelectedReferenceColumns,
			allowReferenceTableAdd: allowReferenceTableAdd,
			allowReferenceTableRemove: allowReferenceTableRemove,
			allowReferenceTableView: allowReferenceTableView,
			referenceTableUrl: referenceTableUrl,
			refGridViewPopup: referenceTableViewPopup,
			refGridViewPopupWidth: referenceTableViewPopupWidth,
			refGridViewPopupHeight: referenceTableViewPopupHeight,
			refGridViewTitle: referenceTableViewPopupTitle
		}
	},

	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseReferenceTableControlConversionProperties: function(config) {
		this.set('id', config.id);

		//Parse out each Reference Table from each of the fields
		var displayNames = config.displayName ? config.displayName.split('|') : [],
			referenceTables = config.refGridReferenceByTable ? config.refGridReferenceByTable.split('|') : [],
			refGridReferenceColumnName = config.refGridReferenceColumnName ? config.refGridReferenceColumnName.split('|') : [],
			referenceColumns = config.refGridSelectedReferenceColumns ? config.refGridSelectedReferenceColumns.split('|') : [],
			allowReferenceTableAdd = config.allowReferenceTableAdd ? config.allowReferenceTableAdd.split('|') : [],
			allowReferenceTableRemove = config.allowReferenceTableRemove ? config.allowReferenceTableRemove.split('|') : [],
			allowReferenceTableView = config.allowReferenceTableView ? config.allowReferenceTableView.split('|') : [],
			referenceTableUrl = config.referenceTableUrl ? config.referenceTableUrl.split('|') : [],
			referenceTableViewPopup = config.refGridViewPopup ? config.refGridViewPopup.split('|') : [],
			referenceTableViewPopupWidth = config.refGridViewPopupWidth ? config.refGridViewPopupWidth.split('|') : [],
			referenceTableViewPopupHeight = config.refGridViewPopupHeight ? config.refGridViewPopupHeight.split('|') : [],
			referenceTableViewPopupTitle = config.refGridViewTitle ? config.refGridViewTitle.split('|') : [],
			len = displayNames.length,
			i;

		for (i = 0; i < len; i++) {
			var cols = referenceColumns[i] ? Ext.decode(referenceColumns[i]) : [],
				displayColumns = [];

			Ext.each(cols, function(col) {
				displayColumns.push(col.value);
			})

			var refTable = this.model({
				mtype: 'ReferenceTable',
				fieldLabel: displayNames[i],
				referenceTable: referenceTables[i],
				referenceColumn: refGridReferenceColumnName[i],
				referenceColumns: displayColumns,
				allowReferenceTableAdd: allowReferenceTableAdd[i] == 'true',
				allowReferenceTableRemove: allowReferenceTableRemove[i] == 'true',
				allowReferenceTableView: allowReferenceTableView[i] == 'true',
				referenceTableUrl: referenceTableUrl[i],
				referenceTableViewPopup: referenceTableViewPopup[i] == 'true',
				referenceTableViewPopupWidth: Number(referenceTableViewPopupWidth[i]),
				referenceTableViewPopupHeight: Number(referenceTableViewPopupHeight[i]),
				referenceTableViewPopupTitle: referenceTableViewPopupTitle[i]
			});

			refTable.referenceColumnStore.load({
				params: {
					tableName: refTable.referenceTable
				}
			})
			this.referenceTables.add(refTable)
		}
		if (len > 0) {
			this.referenceTables.removeAt(0)
			this.set('currentReference', this.referenceTables.getAt(0))
		}
	}
});

/**
 * Mixin defiinition for a GroupPickerControl
 * Used in the user picker detail view, and keeps track of the groups to filter when displaying the form to the user
 */
glu.defModel('RS.formbuilder.GroupPickerControl', {
	/**
	 * Pipe ( | ) delimited list of groups to get users from to display to the user when displaying the user picker control
	 */
	groups: '',
	/**
	 * Store for the dropdown that allows the user to choose which groups the user should be allowed to pick from
	 */
	groupStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getUserGroups',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	teams: '',
	recurseThroughTeams: false,
	teamStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getAllTeams',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	teamList: '',
	recurseThroughTeamsList: false,

	allowAssignToMe: false,
	autoAssignToMe: false,

	initMixin: function() {
		this.groupStore.load();
		this.teamStore.load();
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getGroupsControlConversionProperties) : this.serializeFunctions = [this.getGroupsControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseGroupsControlConversionProperties) : this.deserializeFunctions = [this.parseGroupsControlConversionProperties];
	},
	/**
	 * Conversion method that turns a group picker control into server fields to be persisted in the db
	 */
	getGroupsControlConversionProperties: function() {
		return {
			groups: Ext.isArray(this.groups) ? this.groups.join('|') : this.groups,
			usersOfTeams: Ext.isArray(this.teams) ? this.teams.join('|') : this.teams,
			teamsOfTeams: Ext.isArray(this.teamList) ? this.teamList.join('|') : this.teamList,
			recurseUsersOfTeam: this.recurseThroughTeams,
			recurseTeamsOfTeam: this.recurseThroughTeamsList,
			allowAssignToMe: this.allowAssignToMe,
			autoAssignToMe: this.autoAssignToMe
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseGroupsControlConversionProperties: function(config) {
		this.set('groups', config.groups ? config.groups.split('|') : '');
		this.set('teams', config.usersOfTeams ? config.usersOfTeams.split('|') : '');
		this.set('teamList', config.teamsOfTeams ? config.teamsOfTeams.split('|') : '');
		this.set('recurseThroughTeams', config.recurseUsersOfTeam);
		this.set('recurseThroughTeamsList', config.recurseTeamsOfTeam);
		this.set('allowAssignToMe', config.allowAssignToMe);
		this.set('autoAssignToMe', config.autoAssignToMe)
	}
});

/**
 * Mixin defiinition for a TeamPickerControl
 * Used in the team picker detail view, and keeps track of the teams to filter when displaying the form to the user
 */
glu.defModel('RS.formbuilder.TeamPickerControl', {

	teamPickerTeamsOfTeams: '',
	recurseThroughTeamsList: false,
	teamStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getAllTeams',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	initMixin: function() {
		this.teamStore.load();
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getTeamControlConversionProperties) : this.serializeFunctions = [this.getTeamControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseTeamControlConversionProperties) : this.deserializeFunctions = [this.parseTeamControlConversionProperties];
	},
	/**
	 * Conversion method that turns a group picker control into server fields to be persisted in the db
	 */
	getTeamControlConversionProperties: function() {
		return {
			teamPickerTeamsOfTeams: Ext.isArray(this.teamPickerTeamsOfTeams) ? this.teamPickerTeamsOfTeams.join('|') : this.teamPickerTeamsOfTeams,
			recurseTeamsOfTeam: this.recurseThroughTeamsList
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseTeamControlConversionProperties: function(config) {
		if (!config.teamPickerTeamsOfTeams) config.teamPickerTeamsOfTeams = '';
		this.set('teamPickerTeamsOfTeams', config.teamPickerTeamsOfTeams ? config.teamPickerTeamsOfTeams.split('|') : '');
		this.set('recurseThroughTeamsList', config.recurseTeamsOfTeam);
	}
});

glu.defModel('RS.formbuilder.NameControl', {
	nameSeparator: '|&|',

	showFirstName: true,
	showMiddleInitial: true,
	showMiddleName: false,
	showLastName: true,

	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getNameControlConversionProperties) : this.serializeFunctions = [this.getNameControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseNameControlConversionProperties) : this.deserializeFunctions = [this.parseNameControlConversionProperties];
	},

	/**
	 * Conversion method that turns a name control into server fields to be persisted in the db
	 */
	getNameControlConversionProperties: function() {
		return {
			selectValues: [!this.showFirstName, !this.showMiddleName, !this.showMiddleInitial, !this.showLastName].join(this.nameSeparator)
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseNameControlConversionProperties: function(config) {
		var vals = config.selectValues.split(this.nameSeparator);
		this.set('showFirstName', vals[0]);
		this.set('showMiddleName', vals[1]);
		this.set('showMiddleInitial', vals[2]);
		this.set('showLastName', vals[3]);
	}
});

glu.defModel('RS.formbuilder.LinkControl', {
	linkTarget: '_blank',
	linkTargetStore: {
		mtype: 'store',
		fields: ['name'],
		data: [{
			name: '_blank'
		}, {
			name: '_self'
		}, {
			name: '_parent'
		}, {
			name: '_top'
		}]
	},

	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getLinkControlConversionProperties) : this.serializeFunctions = [this.getLinkControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseLinkControlConversionProperties) : this.deserializeFunctions = [this.parseLinkControlConversionProperties];
	},

	getLinkControlConversionProperties: function() {
		return {
			linkTarget: this.linkTarget
		}
	},
	parseLinkControlConversionProperties: function(config) {
		this.set('linkTarget', config.linkTarget);
	}
});

glu.defModel('RS.formbuilder.FileUploadControl', {
	id: '',
	fieldLabel: '',
	allowUpload: true,
	allowRemove: true,
	allowDownload: true,
	allowedFileTypes: '*',
	referenceColumnName: '',

	// fileTypes: {
	// 	mtype: 'store',
	// 	fields: ['name', 'value']
	// },
	tableName: '',

	fileStore: {
		mtype: 'store',
		fields: ['name']
	},

	inputType: 'INPUT',
	inputName: 'u_fileupload',

	initMixin: function() {
		this.set('fieldLabel', this.localize('attachments'));
		//Add in suggestions for file types
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getFileUploadControlConversionProperties) : this.serializeFunctions = [this.getFileUploadControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseFileUploadControlConversionProperties) : this.deserializeFunctions = [this.parseFileUploadControlConversionProperties];
	},

	getFileUploadControlConversionProperties: function() {
		return {
			id: this.tableName == Ext.String.format('{0}_fu', this.parentVM.dbTable) ? this.id : '',
			fileUploadTableName: Ext.String.format('{0}_fu', this.parentVM.dbTable),
			allowUpload: this.allowUpload,
			allowRemove: this.allowRemove,
			allowDownload: this.allowDownload,
			displayName: this.fieldLabel,
			allowedFileTypes: this.allowedFileTypes,
			sourceType: this.inputType,
			name: this.inputName,
			referenceColumnName: this.referenceColumnName
		}
	},
	parseFileUploadControlConversionProperties: function(config) {
		this.set('id', config.id);
		this.set('fieldLabel', config.displayName);
		this.set('tableName', config.fileUploadTableName || '');
		this.set('allowUpload', config.allowUpload);
		this.set('allowRemove', config.allowRemove);
		this.set('allowDownload', config.allowDownload);
		this.set('allowedFileTypes', config.allowedFileTypes);
		this.set('referenceColumnName', config.referenceColumnName);
	}
});

glu.defModel('RS.formbuilder.DependencyControl', {
	dependenciesStore: {
		mtype: 'store',
		fields: ['id', 'action', 'actionDisplay', 'target', 'condition', 'conditionDisplay', 'value', 'actionOptions', 'actionOptionsDisplay']
	},

	dependenciesJson: '',

	depDoubleClick: function(record, item, index, e, eOpts) {
		this.set('dependenciesSelections', [record]);
		this.editDependency();
	},

	initMixin: function() {
		this.parseDependencyControlConversionProperties(this.dependenciesJson);
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getDependencyControlConversionProperties) : this.serializeFunctions = [this.getDependencyControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseDependencyControlConversionProperties) : this.deserializeFunctions = [this.parseDependencyControlConversionProperties];
	},
	translateDependency: function(dep) {
		return Ext.applyIf(dep, {
			actionDisplay: this.localize(dep.action),
			conditionDisplay: this.localize(dep.condition)
		});
	},
	addDependency: function() {
		//open a window to display the dependency options
		//on close of the window, then you can really add the dependency
		this.open({
			mtype: 'Dependency'
		});
	},
	reallyAddDependency: function(dep) {
		this.dependenciesStore.add(this.translateDependency(dep));
	},
	editDependency: function() {
		var current = this.dependenciesSelections[0];
		this.open(Ext.apply({
			mtype: 'Dependency',
			isEdit: true,
			initialValue: current.data.value,
			initialActionOptions: current.data.actionOptions
		}, current.data));
	},
	editDependencyIsEnabled$: function() {
		return this.dependenciesSelections.length == 1;
	},
	reallyEditDependency: function(dep) {
		var current = this.dependenciesSelections[0],
			translated = this.translateDependency(dep);
		for (var k in translated) {
			current.set(k, dep[k]);
		}
	},
	dependenciesSelections: [],
	removeDependency: function() {
		this.dependenciesStore.remove(this.dependenciesSelections[0]);
	},
	removeDependencyIsEnabled$: function() {
		return this.dependenciesSelections.length == 1;
	},
	getDependencyControlConversionProperties: function() {
		var deps = [];
		this.dependenciesStore.each(function(record) {
			// var actionName = record.data.action,
			// 	actionRecord = this.dependencyActionStore.findRecord('name', actionName, 0, false, false, true);
			// if(actionRecord) actionName = actionRecord.data.value;
			// var conditionName = record.data.condition,
			// 	conditionRecord = this.dependencyConditionStore.findRecord('name', conditionName, 0, false, false, true);
			// if(conditionRecord) conditionName = conditionRecord.data.value;
			if (record.data.target) {
				deps.push({
					id: record.data.id,
					action: record.data.action,
					condition: record.data.condition,
					target: record.data.target,
					actionOptions: record.data.actionOptions,
					value: record.data.value
				});
				// deps.push(Ext.apply(record.data, {
				// 	action: actionName,
				// 	condition: conditionName
				// }));
			}
		}, this);
		return {
			dependencies: deps
		};
	},


	parseDependencyControlConversionProperties: function(config) {
		var deps = config;
		if (!Ext.isArray(config) && config.dependencies) {
			deps = config.dependencies;
		}

		if (Ext.isArray(deps)) {
			this.dependenciesStore.removeAll();
			Ext.each(deps, function(item) {
				this.dependenciesStore.add(this.translateDependency(item));
			}, this);
		}
	}
});

glu.defModel('RS.formbuilder.AddressControl', {
	country: 'US',
	state: '',

	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getAddressControlConversionProperties) : this.serializeFunctions = [this.getAddressControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseAddressControlConversionProperties) : this.deserializeFunctions = [this.parseAddressControlConversionProperties];
	},
	getAddressControlConversionProperties: function() {
		return {
			defaultValue: Ext.String.format('|&||&||&|{0}|&||&|{1}', this.state, this.country)
		}
	},
	parseAddressControlConversionProperties: function(config) {
		var vals = config.defaultValue ? config.defaultValue.split('|&|') : '';
		if (vals.length == 6) {
			this.set('state', vals[3]);
			this.set('country', vals[5]);
		}
	},
	countryStore: {
		mtype: 'store',
		fields: ['name', 'code'],
		data: [{
			name: 'Afghanistan',
			code: 'AF'
		}, {
			name: 'Åland Islands',
			code: 'AX'
		}, {
			name: 'Albania',
			code: 'AL'
		}, {
			name: 'Algeria',
			code: 'DZ'
		}, {
			name: 'American Samoa',
			code: 'AS'
		}, {
			name: 'AndorrA',
			code: 'AD'
		}, {
			name: 'Angola',
			code: 'AO'
		}, {
			name: 'Anguilla',
			code: 'AI'
		}, {
			name: 'Antarctica',
			code: 'AQ'
		}, {
			name: 'Antigua and Barbuda',
			code: 'AG'
		}, {
			name: 'Argentina',
			code: 'AR'
		}, {
			name: 'Armenia',
			code: 'AM'
		}, {
			name: 'Aruba',
			code: 'AW'
		}, {
			name: 'Australia',
			code: 'AU'
		}, {
			name: 'Austria',
			code: 'AT'
		}, {
			name: 'Azerbaijan',
			code: 'AZ'
		}, {
			name: 'Bahamas',
			code: 'BS'
		}, {
			name: 'Bahrain',
			code: 'BH'
		}, {
			name: 'Bangladesh',
			code: 'BD'
		}, {
			name: 'Barbados',
			code: 'BB'
		}, {
			name: 'Belarus',
			code: 'BY'
		}, {
			name: 'Belgium',
			code: 'BE'
		}, {
			name: 'Belize',
			code: 'BZ'
		}, {
			name: 'Benin',
			code: 'BJ'
		}, {
			name: 'Bermuda',
			code: 'BM'
		}, {
			name: 'Bhutan',
			code: 'BT'
		}, {
			name: 'Bolivia',
			code: 'BO'
		}, {
			name: 'Bosnia and Herzegovina',
			code: 'BA'
		}, {
			name: 'Botswana',
			code: 'BW'
		}, {
			name: 'Bouvet Island',
			code: 'BV'
		}, {
			name: 'Brazil',
			code: 'BR'
		}, {
			name: 'British Indian Ocean Territory',
			code: 'IO'
		}, {
			name: 'Brunei Darussalam',
			code: 'BN'
		}, {
			name: 'Bulgaria',
			code: 'BG'
		}, {
			name: 'Burkina Faso',
			code: 'BF'
		}, {
			name: 'Burundi',
			code: 'BI'
		}, {
			name: 'Cambodia',
			code: 'KH'
		}, {
			name: 'Cameroon',
			code: 'CM'
		}, {
			name: 'Canada',
			code: 'CA'
		}, {
			name: 'Cape Verde',
			code: 'CV'
		}, {
			name: 'Cayman Islands',
			code: 'KY'
		}, {
			name: 'Central African Republic',
			code: 'CF'
		}, {
			name: 'Chad',
			code: 'TD'
		}, {
			name: 'Chile',
			code: 'CL'
		}, {
			name: 'China',
			code: 'CN'
		}, {
			name: 'Christmas Island',
			code: 'CX'
		}, {
			name: 'Cocos (Keeling) Islands',
			code: 'CC'
		}, {
			name: 'Colombia',
			code: 'CO'
		}, {
			name: 'Comoros',
			code: 'KM'
		}, {
			name: 'Congo',
			code: 'CG'
		}, {
			name: 'Congo, The Democratic Republic of the',
			code: 'CD'
		}, {
			name: 'Cook Islands',
			code: 'CK'
		}, {
			name: 'Costa Rica',
			code: 'CR'
		}, {
			name: 'Cote D\'Ivoire',
			code: 'CI'
		}, {
			name: 'Croatia',
			code: 'HR'
		}, {
			name: 'Cuba',
			code: 'CU'
		}, {
			name: 'Cyprus',
			code: 'CY'
		}, {
			name: 'Czech Republic',
			code: 'CZ'
		}, {
			name: 'Denmark',
			code: 'DK'
		}, {
			name: 'Djibouti',
			code: 'DJ'
		}, {
			name: 'Dominica',
			code: 'DM'
		}, {
			name: 'Dominican Republic',
			code: 'DO'
		}, {
			name: 'Ecuador',
			code: 'EC'
		}, {
			name: 'Egypt',
			code: 'EG'
		}, {
			name: 'El Salvador',
			code: 'SV'
		}, {
			name: 'Equatorial Guinea',
			code: 'GQ'
		}, {
			name: 'Eritrea',
			code: 'ER'
		}, {
			name: 'Estonia',
			code: 'EE'
		}, {
			name: 'Ethiopia',
			code: 'ET'
		}, {
			name: 'Falkland Islands (Malvinas)',
			code: 'FK'
		}, {
			name: 'Faroe Islands',
			code: 'FO'
		}, {
			name: 'Fiji',
			code: 'FJ'
		}, {
			name: 'Finland',
			code: 'FI'
		}, {
			name: 'France',
			code: 'FR'
		}, {
			name: 'French Guiana',
			code: 'GF'
		}, {
			name: 'French Polynesia',
			code: 'PF'
		}, {
			name: 'French Southern Territories',
			code: 'TF'
		}, {
			name: 'Gabon',
			code: 'GA'
		}, {
			name: 'Gambia',
			code: 'GM'
		}, {
			name: 'Georgia',
			code: 'GE'
		}, {
			name: 'Germany',
			code: 'DE'
		}, {
			name: 'Ghana',
			code: 'GH'
		}, {
			name: 'Gibraltar',
			code: 'GI'
		}, {
			name: 'Greece',
			code: 'GR'
		}, {
			name: 'Greenland',
			code: 'GL'
		}, {
			name: 'Grenada',
			code: 'GD'
		}, {
			name: 'Guadeloupe',
			code: 'GP'
		}, {
			name: 'Guam',
			code: 'GU'
		}, {
			name: 'Guatemala',
			code: 'GT'
		}, {
			name: 'Guernsey',
			code: 'GG'
		}, {
			name: 'Guinea',
			code: 'GN'
		}, {
			name: 'Guinea-Bissau',
			code: 'GW'
		}, {
			name: 'Guyana',
			code: 'GY'
		}, {
			name: 'Haiti',
			code: 'HT'
		}, {
			name: 'Heard Island and Mcdonald Islands',
			code: 'HM'
		}, {
			name: 'Holy See (Vatican City State)',
			code: 'VA'
		}, {
			name: 'Honduras',
			code: 'HN'
		}, {
			name: 'Hong Kong',
			code: 'HK'
		}, {
			name: 'Hungary',
			code: 'HU'
		}, {
			name: 'Iceland',
			code: 'IS'
		}, {
			name: 'India',
			code: 'IN'
		}, {
			name: 'Indonesia',
			code: 'ID'
		}, {
			name: 'Iran, Islamic Republic Of',
			code: 'IR'
		}, {
			name: 'Iraq',
			code: 'IQ'
		}, {
			name: 'Ireland',
			code: 'IE'
		}, {
			name: 'Isle of Man',
			code: 'IM'
		}, {
			name: 'Israel',
			code: 'IL'
		}, {
			name: 'Italy',
			code: 'IT'
		}, {
			name: 'Jamaica',
			code: 'JM'
		}, {
			name: 'Japan',
			code: 'JP'
		}, {
			name: 'Jersey',
			code: 'JE'
		}, {
			name: 'Jordan',
			code: 'JO'
		}, {
			name: 'Kazakhstan',
			code: 'KZ'
		}, {
			name: 'Kenya',
			code: 'KE'
		}, {
			name: 'Kiribati',
			code: 'KI'
		}, {
			name: 'Korea, Democratic People\'S Republic of',
			code: 'KP'
		}, {
			name: 'Korea, Republic of',
			code: 'KR'
		}, {
			name: 'Kuwait',
			code: 'KW'
		}, {
			name: 'Kyrgyzstan',
			code: 'KG'
		}, {
			name: 'Lao People\'S Democratic Republic',
			code: 'LA'
		}, {
			name: 'Latvia',
			code: 'LV'
		}, {
			name: 'Lebanon',
			code: 'LB'
		}, {
			name: 'Lesotho',
			code: 'LS'
		}, {
			name: 'Liberia',
			code: 'LR'
		}, {
			name: 'Libyan Arab Jamahiriya',
			code: 'LY'
		}, {
			name: 'Liechtenstein',
			code: 'LI'
		}, {
			name: 'Lithuania',
			code: 'LT'
		}, {
			name: 'Luxembourg',
			code: 'LU'
		}, {
			name: 'Macao',
			code: 'MO'
		}, {
			name: 'Macedonia, The Former Yugoslav Republic of',
			code: 'MK'
		}, {
			name: 'Madagascar',
			code: 'MG'
		}, {
			name: 'Malawi',
			code: 'MW'
		}, {
			name: 'Malaysia',
			code: 'MY'
		}, {
			name: 'Maldives',
			code: 'MV'
		}, {
			name: 'Mali',
			code: 'ML'
		}, {
			name: 'Malta',
			code: 'MT'
		}, {
			name: 'Marshall Islands',
			code: 'MH'
		}, {
			name: 'Martinique',
			code: 'MQ'
		}, {
			name: 'Mauritania',
			code: 'MR'
		}, {
			name: 'Mauritius',
			code: 'MU'
		}, {
			name: 'Mayotte',
			code: 'YT'
		}, {
			name: 'Mexico',
			code: 'MX'
		}, {
			name: 'Micronesia, Federated States of',
			code: 'FM'
		}, {
			name: 'Moldova, Republic of',
			code: 'MD'
		}, {
			name: 'Monaco',
			code: 'MC'
		}, {
			name: 'Mongolia',
			code: 'MN'
		}, {
			name: 'Montserrat',
			code: 'MS'
		}, {
			name: 'Morocco',
			code: 'MA'
		}, {
			name: 'Mozambique',
			code: 'MZ'
		}, {
			name: 'Myanmar',
			code: 'MM'
		}, {
			name: 'Namibia',
			code: 'NA'
		}, {
			name: 'Nauru',
			code: 'NR'
		}, {
			name: 'Nepal',
			code: 'NP'
		}, {
			name: 'Netherlands',
			code: 'NL'
		}, {
			name: 'Netherlands Antilles',
			code: 'AN'
		}, {
			name: 'New Caledonia',
			code: 'NC'
		}, {
			name: 'New Zealand',
			code: 'NZ'
		}, {
			name: 'Nicaragua',
			code: 'NI'
		}, {
			name: 'Niger',
			code: 'NE'
		}, {
			name: 'Nigeria',
			code: 'NG'
		}, {
			name: 'Niue',
			code: 'NU'
		}, {
			name: 'Norfolk Island',
			code: 'NF'
		}, {
			name: 'Northern Mariana Islands',
			code: 'MP'
		}, {
			name: 'Norway',
			code: 'NO'
		}, {
			name: 'Oman',
			code: 'OM'
		}, {
			name: 'Pakistan',
			code: 'PK'
		}, {
			name: 'Palau',
			code: 'PW'
		}, {
			name: 'Palestinian Territory, Occupied',
			code: 'PS'
		}, {
			name: 'Panama',
			code: 'PA'
		}, {
			name: 'Papua New Guinea',
			code: 'PG'
		}, {
			name: 'Paraguay',
			code: 'PY'
		}, {
			name: 'Peru',
			code: 'PE'
		}, {
			name: 'Philippines',
			code: 'PH'
		}, {
			name: 'Pitcairn',
			code: 'PN'
		}, {
			name: 'Poland',
			code: 'PL'
		}, {
			name: 'Portugal',
			code: 'PT'
		}, {
			name: 'Puerto Rico',
			code: 'PR'
		}, {
			name: 'Qatar',
			code: 'QA'
		}, {
			name: 'Reunion',
			code: 'RE'
		}, {
			name: 'Romania',
			code: 'RO'
		}, {
			name: 'Russian Federation',
			code: 'RU'
		}, {
			name: 'RWANDA',
			code: 'RW'
		}, {
			name: 'Saint Helena',
			code: 'SH'
		}, {
			name: 'Saint Kitts and Nevis',
			code: 'KN'
		}, {
			name: 'Saint Lucia',
			code: 'LC'
		}, {
			name: 'Saint Pierre and Miquelon',
			code: 'PM'
		}, {
			name: 'Saint Vincent and the Grenadines',
			code: 'VC'
		}, {
			name: 'Samoa',
			code: 'WS'
		}, {
			name: 'San Marino',
			code: 'SM'
		}, {
			name: 'Sao Tome and Principe',
			code: 'ST'
		}, {
			name: 'Saudi Arabia',
			code: 'SA'
		}, {
			name: 'Senegal',
			code: 'SN'
		}, {
			name: 'Serbia and Montenegro',
			code: 'CS'
		}, {
			name: 'Seychelles',
			code: 'SC'
		}, {
			name: 'Sierra Leone',
			code: 'SL'
		}, {
			name: 'Singapore',
			code: 'SG'
		}, {
			name: 'Slovakia',
			code: 'SK'
		}, {
			name: 'Slovenia',
			code: 'SI'
		}, {
			name: 'Solomon Islands',
			code: 'SB'
		}, {
			name: 'Somalia',
			code: 'SO'
		}, {
			name: 'South Africa',
			code: 'ZA'
		}, {
			name: 'South Georgia and the South Sandwich Islands',
			code: 'GS'
		}, {
			name: 'Spain',
			code: 'ES'
		}, {
			name: 'Sri Lanka',
			code: 'LK'
		}, {
			name: 'Sudan',
			code: 'SD'
		}, {
			name: 'Suriname',
			code: 'SR'
		}, {
			name: 'Svalbard and Jan Mayen',
			code: 'SJ'
		}, {
			name: 'Swaziland',
			code: 'SZ'
		}, {
			name: 'Sweden',
			code: 'SE'
		}, {
			name: 'Switzerland',
			code: 'CH'
		}, {
			name: 'Syrian Arab Republic',
			code: 'SY'
		}, {
			name: 'Taiwan, Province of China',
			code: 'TW'
		}, {
			name: 'Tajikistan',
			code: 'TJ'
		}, {
			name: 'Tanzania, United Republic of',
			code: 'TZ'
		}, {
			name: 'Thailand',
			code: 'TH'
		}, {
			name: 'Timor-Leste',
			code: 'TL'
		}, {
			name: 'Togo',
			code: 'TG'
		}, {
			name: 'Tokelau',
			code: 'TK'
		}, {
			name: 'Tonga',
			code: 'TO'
		}, {
			name: 'Trinidad and Tobago',
			code: 'TT'
		}, {
			name: 'Tunisia',
			code: 'TN'
		}, {
			name: 'Turkey',
			code: 'TR'
		}, {
			name: 'Turkmenistan',
			code: 'TM'
		}, {
			name: 'Turks and Caicos Islands',
			code: 'TC'
		}, {
			name: 'Tuvalu',
			code: 'TV'
		}, {
			name: 'Uganda',
			code: 'UG'
		}, {
			name: 'Ukraine',
			code: 'UA'
		}, {
			name: 'United Arab Emirates',
			code: 'AE'
		}, {
			name: 'United Kingdom',
			code: 'GB'
		}, {
			name: 'United States',
			code: 'US'
		}, {
			name: 'United States Minor Outlying Islands',
			code: 'UM'
		}, {
			name: 'Uruguay',
			code: 'UY'
		}, {
			name: 'Uzbekistan',
			code: 'UZ'
		}, {
			name: 'Vanuatu',
			code: 'VU'
		}, {
			name: 'Venezuela',
			code: 'VE'
		}, {
			name: 'Viet Nam',
			code: 'VN'
		}, {
			name: 'Virgin Islands, British',
			code: 'VG'
		}, {
			name: 'Virgin Islands, U.S.',
			code: 'VI'
		}, {
			name: 'Wallis and Futuna',
			code: 'WF'
		}, {
			name: 'Western Sahara',
			code: 'EH'
		}, {
			name: 'Yemen',
			code: 'YE'
		}, {
			name: 'Zambia',
			code: 'ZM'
		}, {
			name: 'Zimbabwe',
			code: 'ZW'
		}]
	}
});

glu.defModel('RS.formbuilder.TooltipControl', {
	tooltip: '',
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getTooltipControlConversionProperties) : this.serializeFunctions = [this.getTooltipControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseTooltipControlConversionProperties) : this.deserializeFunctions = [this.parseTooltipControlConversionProperties];
	},
	/**
	 * Conversion method that turns a time control into server fields to be persisted in the db
	 */
	getTooltipControlConversionProperties: function() {
		return {
			tooltip: this.tooltip
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseTooltipControlConversionProperties: function(config) {
		this.set('tooltip', config.tooltip);
	}
});

glu.defModel('RS.formbuilder.UrlControl', {
	inputName: 'u_urlcontrol',
	inputType: 'INPUT',

	referenceTableUrl: '',

	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getUrlControlConversionProperties) : this.serializeFunctions = [this.getUrlControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseUrlControlConversionProperties) : this.deserializeFunctions = [this.parseUrlControlConversionProperties];
		this.set('inputName', this.inputName + Ext.id())
	},
	/**
	 * Conversion method that turns a time control into server fields to be persisted in the db
	 */
	getUrlControlConversionProperties: function() {
		return {
			sourceType: this.inputType,
			id: this.id,
			name: this.inputName,
			referenceTableUrl: this.referenceTableUrl
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseUrlControlConversionProperties: function(config) {
		this.set('referenceTableUrl', config.referenceTableUrl);
	}
});

glu.defModel('RS.formbuilder.SectionContainerControl', {
	sectionType: 'panel',
	sectionTypeStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},

	title: '',

	dbTable$: function() {
		return this.parentVM.dbTable;
	},
	columnStore$: function() {
		return this.parentVM.columnStore
	},

	layout: 'column',
	layoutStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},

	columnCount: 1,
	columnCountIsVisible$: function() {
		return this.layout == 'column'
	},
	when_columns_count_changes_rebuild_control: {
		on: ['columnCountChanged'],
		action: function() {
			if (this.columnCount != -1) {
				while (this.columns.length > this.columnCount) this.columns.removeAt(this.columns.length - 1)
				while (this.columns.length < this.columnCount) this.columns.add(this.model({
					mtype: 'Column'
				}))
			}
		}
	},

	columns: {
		mtype: 'activatorlist',
		focusProperty: 'selectedColumn',
		autoParent: true
	},
	selectedColumn: -1,
	when_selected_column_changes_update_selectedColumnIndex: {
		on: ['selectedColumnChanged'],
		action: function() {
			if (this.selectedColumn) this.set('selectedColumnIndex', this.columns.indexOf(this.selectedColumn))
		}
	},
	selectedColumnIndex: -1,

	init: function() {
		this.sectionTypeStore.add([{
			name: this.localize('panel'),
			value: 'panel'
		}, {
			name: this.localize('fieldset'),
			value: 'fieldset'
		}]);

		this.layoutStore.add([{
			name: this.localize('auto'),
			value: 'auto'
		}, {
			name: this.localize('columnLayout'),
			value: 'column'
		}]);

		this.columns.add({
			mtype: 'Column'
		})

		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getSectionContainerControlConversionProperties) : this.serializeFunctions = [this.getSectionContainerControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseSectionContainerControlConversionProperties) : this.deserializeFunctions = [this.parseSectionContainerControlConversionProperties];
	},

	getSectionContainerControlConversionProperties: function() {
		//Go through each column and serialize the controls
		//Then encode the resulting array and store as the sectionColumns
		var encodeColumns = []
		this.columns.foreach(function(column) {
			encodeColumns.push({
				controls: this.rootVM.serializeControls(column.controls)
			})
		}, this)
		return {
			id: this.id,
			sourceType: 'INPUT',
			name: 'u_sectionContainer',
			sectionLayout: this.layout,
			sectionTitle: this.title,
			sectionType: this.sectionType,
			sectionNumberOfColumns: this.columnCount,
			sectionColumns: Ext.encode(encodeColumns)
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseSectionContainerControlConversionProperties: function(config) {
		this.set('id', config.id)
		this.set('layout', config.sectionLayout)
		this.set('title', config.sectionTitle)
		this.set('sectionType', config.sectionType)
		this.set('columnCount', -1)
		this.columns.removeAll()
		var decodeColumns = Ext.decode(config.sectionColumns)
		if (decodeColumns && Ext.isArray(decodeColumns)) {
			Ext.each(decodeColumns, function(column) {
				var c = this.model({
					mtype: 'Column'
				})
				this.columns.add(c)
				Ext.each(column.controls || [], function(control) {
					if (control && control.uiType) {
						var ctrl = this.rootVM.deserializeControl(control, false, null, c)
						ctrl.isInSection = true
						c.controls.add(ctrl)
					}
				}, this)
			}, this)
		}
		this.set('columnCount', config.sectionNumberOfColumns)
		if (this.columns.length == 0) {
			this.columns.add({
				mtype: 'Column'
			})
			this.set('columnCount', 1)
		}
		this.set('selectedColumn', 0)
	}
});
/**
 * TODO: look into adding mixin RS.formbuilder.Form. Uses many similar methods. Would reduce duplication - HR
 */
glu.defModel('RS.formbuilder.Column', {
	mixins: [],
	controls: {
		mtype: 'activatorlist',
		focusProperty: 'detailControl',
		autoParent: true
	},
	detailControl: -1,
	/* The currently selected control on the form or -1 if no selection */
	reselectControlIndex: -1,
	/* Flag internally used to determine if the control should be re-selected because an operation is rebuilding it (bulk)  */
	reselectControlCount: 0,
	when_detail_control_changes_make_sure_column_has_focus_on_section: {
		on: ['detailControlChanged', 'activeitemchangerequest'],
		action: function() {
			this.parentVM.set('selectedColumn', this);
		}
	},

	dbTable$: function() {
		return this.parentVM.dbTable
	},
	columnStore$: function() {
		return this.parentVM.columnStore
	},

	columnClicked: function() {
		this.parentVM.set('selectedColumn', this);
	},

	init: function() {
		this.controls.add({
			mtype: 'Panel'
		})
	},
	removeControl: function(control) {
		this.controls.remove(control);
		if (control.viewmodelName == 'FileUploadField') {
			this.rootVM.set('fileUploadIsEnabled', true);
		}
	},
	dropControl: function(dropIndex, sourceIndex, controlConfig, columnIndex) {
		if (dropIndex >= 0 && sourceIndex >= 0) {
			this.controls.insert(sourceIndex < dropIndex ? dropIndex - 1 : dropIndex, this.controls.removeAt(sourceIndex));
		} else if (dropIndex >= 0 && controlConfig) {
			this.insertControl(dropIndex, controlConfig);
		}
	},
	insertControl: function(index, controlConfig) {
		var control;

		if (controlConfig == 'SectionContainerField') {
			this.message({
				title: this.localize('sectionInSectionTitle'),
				msg: this.localize('sectionInSectionMessage'),
				buttons: Ext.MessageBox.OK
			})
			return null
		}

		if (Ext.isString(controlConfig)) control = this.model({
			mtype: controlConfig,
			isInSection: true,
			labelAlign: this.labelAlign
		});
		else {
			control = this.model(Ext.apply(controlConfig, {
				isInSection: true
			}));
		}

		//Disable the file upload button if this is a file upload field
		if (control.viewmodelName == 'FileUploadField') {
			this.rootVM.set('fileUploadIsEnabled', false);
		}

		this.controls.insert(index, control);
		this.selectControl(control);
		return control;
	},
	selectControl: function(control) {
		this.set('detailControl', control)
	},
	rebuildControl: function(ctrl, reselectControl) {
		var currentIndex = this.controls.indexOf(ctrl);
		var control = this.controls.removeAt(currentIndex);
		if (control) this.controls.insert(currentIndex, control);
		if (currentIndex != -1 && (this.reselectControlIndex == currentIndex || this.reselectControlCount <= 0) && reselectControl) {
			this.selectControl(this.controls.getAt(currentIndex));
			this.reselectControlCount--;
		}
	}
});

glu.defModel('RS.formbuilder.Panel', {})

glu.defModel('RS.formbuilder.TabContainerControl', {

	id: '',
	fieldLabel: '',
	fieldLabelIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceTable: '',
	referenceTableIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceColumn: '',
	referenceColumnIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceColumns: '',
	referenceColumnsIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	buttonSetIsHidden$ : function(){
		return Ext.Array.indexOf(['ReferenceTable', 'FileManager'], this.currentReference.rsType) == -1;
 	},
	allowReferenceTableAdd: true,
	allowReferenceTableAddIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	allowReferenceTableAddText$: function() {
		if (this.currentReference.rsType == 'FileManager') return this.localize('allowUpload');
		return this.localize('allowReferenceTableAdd');
	},
	allowReferenceTableRemove: true,
	allowReferenceTableRemoveIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	allowReferenceTableRemoveText$: function() {
		if (this.currentReference.rsType == 'FileManager') return this.localize('allowRemove');
		return this.localize('allowReferenceTableRemove');
	},
	allowReferenceTableView: true,
	allowReferenceTableViewIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	allowReferenceTableViewText$: function() {
		if (this.currentReference.rsType == 'FileManager') return this.localize('allowDownload');
		return this.localize('allowReferenceTableView');
	},
	referenceTableUrl: '',
	referenceTableUrlIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceTableViewPopup: false,
	referenceTableViewPopupIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	popupFieldSetIsHidden$ : function(){
		return Ext.Array.indexOf(['ReferenceTable'], this.currentReference.rsType) == -1;
	},
	referenceTableViewPopupWidth: 600,
	referenceTableViewPopupHeight: 600,
	referenceTableViewPopupTitle: '',
	currentReferenceNew$: function() {
		if (this.currentReference.rsType == 'FileManager') return this.localize('upload');
		return this.localize('referenceTableNew');
	},

	referenceTableUrlNewTab: true,
	rsType: 'ReferenceTable',

	fields: ['fieldLabel', 'referenceTable', 'referenceColumn', 'referenceColumns', 'allowReferenceTableAdd', 'allowReferenceTableRemove', 'allowReferenceTableView', 'referenceTableUrl', 'referenceTableViewPopup', 'referenceTableViewPopupWidth', 'referenceTableViewPopupHeight', 'referenceTableViewPopupTitle'],

	when_field_changes_update_current_reference: {
		on: ['fieldLabelChanged', 'referenceTableChanged', 'referenceColumnChanged', 'referenceColumnsChanged', 'allowReferenceTableAddChanged', 'allowReferenceTableRemoveChanged', 'allowReferenceTableViewChanged', 'referenceTableUrlChanged', 'referenceTableViewPopupChanged', 'referenceTableViewPopupWidthChanged', 'referenceTableViewPopupHeightChanged', 'referenceTableViewPopupTitleChanged'],
		action: function() {
			//persist to current reference
			if (Ext.isObject(this.currentReference)) {
				Ext.Object.each(this.currentReference.asObject(), function(attribute) {
					this.currentReference.set(attribute, this[attribute]);
				}, this);
				this.currentReference.referenceColumnStore.removeAll();
				this.referenceColumnStore.each(function(record) {
					this.currentReference.referenceColumnStore.add(record);
				}, this);
			}
		}
	},
	when_reference_table_changes_update_reference_column_too: {
		on: ['referenceTableChanged'],
		action: function() {
			var record = this.referenceStore.findRecord('value', this.referenceTable, 0, false, false, true)
			if (record) {
				this.set('referenceColumn', record.data.name);
			}
		}
	},

	referenceTables: {
		mtype: 'list',
		autoParent: true
	},

	referenceTablesSelections: [],
	currentReference: 0,
	when_current_reference_changes_update_selection_in_grid: {
		on: 'currentReferenceChanged',
		action: function() {
			if (Ext.isObject(this.currentReference)) {
				this.changeReferenceTablesSelections([this.referenceTablesStore.getAt(this.referenceTables.indexOf(this.currentReference))]);
			}
		}
	},
	when_selection_changes_update_fields: {
		on: ['referenceTablesSelectionsChanged'],
		action: function() {
			var sel = this.referenceTablesSelections[0];
			if (sel) {
				var selection = this.referenceTables.getAt(this.referenceTablesStore.indexOf(sel));
				if (selection) {
					//Persist dependencies
					// if(Ext.isObject(this.currentReference)) {
					// 	var deps = this.getDependencyControlConversionProperties();
					// 	this.currentReference.parseDependencyControlConversionProperties(deps);
					// }
					var temp = this.currentReference;
					this.currentReference = null;
					Ext.Object.each(selection.asObject(), function(attribute) {
						if (Ext.isDefined(this[attribute])) this.set(attribute, selection[attribute]);
					}, this);
					// this.parseDependencyControlConversionProperties(selection.getDependencyControlConversionProperties());
					this.currentReference = temp;
					this.set('currentReference', selection);
				}
			}
		}
	},

	referenceTablesStore: {
		mtype: 'store',
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'referenceTables'
		}],
		fields: ['fieldLabel']
	},

	referenceDisplayStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},

	referenceStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getReferenceTables',
			// url: '/resolve/service/form/getcustomtablesforref',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	referenceColumnStore: {
		mtype: 'store',
		fields: ['name', 'dbcolumnId', 'displayName', 'dbtype', 'uiType', 'integerMinValue', 'integerMaxValue', 'decimalMinValue', 'decimalMaxValue', 'stringMinLength', 'stringMaxLength', 'uiStringMaxLength', 'selectValues', 'choiceValues', 'referenceTable', 'referenceDisplayColumn'],
		data: [],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getcustomtablecolumns',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	when_referenceTable_changes_update_reference_columns: {
		on: ['referenceTableChanged'],
		action: function() {
			this.set('referenceColumns', '');
			this.referenceColumnStore.load();
		}
	},

	referenceColumnsIsEnabled$: function() {
		return this.referenceTable;
	},
	referenceColumnsIsVisible$: function() {
		return Ext.Array.indexOf(['ReferenceTable'], this.currentReference.rsType) > -1;
	},

	referenceTableIsValid$: function() {
		if (this.currentReference) {
			if (this.currentReference.rsType == 'ReferenceTable' && !this.referenceTable) return this.localize('referenceTableInvalid')
		}
		return true;
	},
	referenceTableIsVisible$: function() {
		return Ext.Array.indexOf(['ReferenceTable'], this.currentReference.rsType) > -1;
	},

	referenceTableUrlText$: function() {
		if (this.currentReference.rsType == 'FileManager') return this.localize('allowedFileTypes');
		return this.localize('referenceTableUrl');
	},

	referenceTableUrlIsValid$: function() {
		if (this.currentReference) {
			switch (this.currentReference.rsType) {
				case 'ReferenceTable':
					if ((this.allowReferenceTableAdd || this.allowReferenceTableView) && !this.referenceTableUrl) return this.localize('referenceTableUrlInvalid');
					break;
				case 'FileManager':
					return true;
					break;
				case 'URL':
					if (!this.referenceTableUrl) return this.localize('urlTabInvalid');
					break;
			}
		}
		return true;
	},

	when_referenceTableUrl_changed_check_match_to_wiki: {
		on: ['referenceTableUrlChanged'],
		action: function() {
			if ((this.parentVM.wikiNames && this.parentVM.wikiNames.getById(this.referenceTableUrl)) || (this.parentVM.parentVM.wikiNames && this.parentVM.parentVM.wikiNames.getById(this.referenceTableUrl))) {
				Ext.defer(function() {
					this.set('referenceTableUrl', '/resolve/service/wiki/view/' + this.referenceTableUrl.split('.').join('/'));
				}, 100, this);
			}
		}
	},

	changeReferenceTablesSelections: function(selections) {
		this.set('referenceTablesSelections', selections);
		this.referenceTablesStore.fireEvent('cellSelectionVMChanged', this, selections);
	},

	addReferenceTable: function() {
		var selectedIndex = -1;
		var sel = this.referenceTablesSelections[0];
		if (sel) selectedIndex = this.referenceTablesStore.indexOf(sel);
		if (selectedIndex == -1) selectedIndex = this.referenceTables.length;
		else selectedIndex++;
		this.referenceTables.insert(selectedIndex, this.model({
			mtype: 'ReferenceTable',
			rsType: 'ReferenceTable',
			fieldLabel: this.localize('unknownReference')
		}));

		this.set('referenceTablesSelections', [this.referenceTablesStore.getAt(selectedIndex)]);
	},

	addFileManager: function() {
		var selectedIndex = -1;
		var sel = this.referenceTablesSelections[0];
		if (sel) selectedIndex = this.referenceTablesStore.indexOf(sel);
		if (selectedIndex == -1) selectedIndex = this.referenceTables.length;
		else selectedIndex++;
		this.referenceTables.insert(selectedIndex, this.model({
			mtype: 'ReferenceTable',
			rsType: 'FileManager',
			referenceTableUrl: '*',
			fieldLabel: this.localize('attachments')
		}));

		//disable the adding of more file managers on the page
		this.rootVM.set('fileUploadIsEnabled', false);

		this.set('referenceTablesSelections', [this.referenceTablesStore.getAt(selectedIndex)]);
	},

	addFileManagerIsEnabled$: function() {
		return this.rootVM.fileUploadIsEnabled;
	},

	addURL: function() {
		var selectedIndex = -1;
		var sel = this.referenceTablesSelections[0];
		if (sel) selectedIndex = this.referenceTablesStore.indexOf(sel);
		if (selectedIndex == -1) selectedIndex = this.referenceTables.length;
		else selectedIndex++;
		this.referenceTables.insert(selectedIndex, this.model({
			mtype: 'ReferenceTable',
			rsType: 'URL',
			allowReferenceTableAdd: false,
			allowReferenceTableRemove: false,
			fieldLabel: this.localize('urlTab')
		}));

		this.set('referenceTablesSelections', [this.referenceTablesStore.getAt(selectedIndex)]);
	},
	removeReference: function() {
		var sel = this.referenceTablesSelections[0];
		if (sel) {
			var selection = this.referenceTables.getAt(this.referenceTablesStore.indexOf(sel));
			if (selection) {
				if (this.currentReference.rsType == 'FileManager') this.rootVM.set('fileUploadIsEnabled', true);
				this.referenceTables.remove(selection);
				this.changeReferenceTablesSelections([this.referenceTablesStore.getAt(0)]);
			}
		}
	},
	removeReferenceIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},

	initMixin: function() {
		this.referenceStore.load({
			params: {
				tableName: this.parentVM.dbTable
			}
		});
		this.referenceColumnStore.on('beforeload', function(store, operation, eOpts) {
			if (!this.referenceTable) return false
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				tableName: this.referenceTable
			});
		}, this);

		//Initialize first reference
		this.referenceTables.add(this.model({
			mtype: 'ReferenceTable',
			fieldLabel: this.localize('references')
		}));
		//Initially select the only record so that all the fields are initialized properly
		this.set('currentReference', this.referenceTables.getAt(0));
		this.changeReferenceTablesSelections([this.referenceTablesStore.getAt(0)]);

		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getTabContainerControlConversionProperties) : this.serializeFunctions = [this.getTabContainerControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseTabContainerControlConversionProperties) : this.deserializeFunctions = [this.parseTabContainerControlConversionProperties];
	},

	getTabContainerControlConversionProperties: function() {

		//Go through each of the ReferenceTables and combine them to persist on the backend
		var displayName = '',
			refGridReferenceByTable = '',
			refGridReferenceColumnName = '',
			refGridSelectedReferenceColumns = '',
			allowReferenceTableAdd = '',
			allowReferenceTableRemove = '',
			allowReferenceTableView = '',
			referenceTableUrl = '',
			referenceTableViewPopup = '',
			referenceTableViewPopupWidth = '',
			referenceTableViewPopupHeight = '',
			referenceTableViewPopupTitle = '';

		this.referenceTables.foreach(function(referenceTable, index) {
			displayName += (index > 0 ? '|' : '') + referenceTable.fieldLabel;
			refGridReferenceByTable += (index > 0 ? '|' : '') + referenceTable.referenceTable;
			refGridReferenceColumnName += (index > 0 ? '|' : '') + referenceTable.referenceColumn;
			allowReferenceTableAdd += (index > 0 ? '|' : '') + referenceTable.allowReferenceTableAdd;
			allowReferenceTableRemove += (index > 0 ? '|' : '') + referenceTable.allowReferenceTableRemove;
			allowReferenceTableView += (index > 0 ? '|' : '') + referenceTable.allowReferenceTableView;
			referenceTableUrl += (index > 0 ? '|' : '') + referenceTable.referenceTableUrl;
			referenceTableViewPopup += (index > 0 ? '|' : '') + referenceTable.referenceTableViewPopup;
			referenceTableViewPopupWidth += (index > 0 ? '|' : '') + referenceTable.referenceTableViewPopupWidth;
			referenceTableViewPopupHeight += (index > 0 ? '|' : '') + referenceTable.referenceTableViewPopupHeight;
			referenceTableViewPopupTitle += (index > 0 ? '|' : '') + (referenceTable.referenceTableViewPopupTitle || referenceTable.rsType);

			var displayColumns = [];

			Ext.each(referenceTable.referenceColumns, function(refColumn) {
				var rec = referenceTable.referenceColumnStore.findRecord('name', refColumn, 0, false, false, true);
				if (rec) {
					displayColumns.push({
						name: rec.data.displayName,
						value: refColumn,
						dbtype: rec.data.dbtype
					});
				}
			}, this);
			refGridSelectedReferenceColumns += (index > 0 ? '|' : '') + (displayColumns.length > 0 ? Ext.encode(displayColumns) : '');
		}, this);

		return {
			id: this.id,
			sourceType: 'INPUT',
			name: 'u_referenceTable_',
			fileUploadTableName: Ext.String.format('{0}_fu', this.parentVM.dbTable),
			displayName: displayName,
			refGridReferenceByTable: refGridReferenceByTable,
			refGridReferenceColumnName: refGridReferenceColumnName,
			refGridSelectedReferenceColumns: refGridSelectedReferenceColumns,
			allowReferenceTableAdd: allowReferenceTableAdd,
			allowReferenceTableRemove: allowReferenceTableRemove,
			allowReferenceTableView: allowReferenceTableView,
			referenceTableUrl: referenceTableUrl,
			refGridViewPopup: referenceTableViewPopup,
			refGridViewPopupWidth: referenceTableViewPopupWidth,
			refGridViewPopupHeight: referenceTableViewPopupHeight,
			refGridViewTitle: referenceTableViewPopupTitle
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseTabContainerControlConversionProperties: function(config) {
		this.set('id', config.id);

		//Parse out each Reference Table from each of the fields
		var displayNames = config.displayName ? config.displayName.split('|') : [],
			referenceTables = config.refGridReferenceByTable ? config.refGridReferenceByTable.split('|') : [],
			refGridReferenceColumnName = config.refGridReferenceColumnName ? config.refGridReferenceColumnName.split('|') : [],
			referenceColumns = config.refGridSelectedReferenceColumns ? config.refGridSelectedReferenceColumns.split('|') : [],
			allowReferenceTableAdd = config.allowReferenceTableAdd ? config.allowReferenceTableAdd.split('|') : [],
			allowReferenceTableRemove = config.allowReferenceTableRemove ? config.allowReferenceTableRemove.split('|') : [],
			allowReferenceTableView = config.allowReferenceTableView ? config.allowReferenceTableView.split('|') : [],
			referenceTableUrl = config.referenceTableUrl ? config.referenceTableUrl.split('|') : [],
			referenceTableViewPopup = config.refGridViewPopup ? config.refGridViewPopup.split('|') : [],
			referenceTableViewPopupWidth = config.refGridViewPopupWidth ? config.refGridViewPopupWidth.split('|') : [],
			referenceTableViewPopupHeight = config.refGridViewPopupHeight ? config.refGridViewPopupHeight.split('|') : [],
			referenceTableViewPopupTitle = config.refGridViewTitle ? config.refGridViewTitle.split('|') : [],
			len = displayNames.length,
			i;

		for (i = 0; i < len; i++) {
			var cols = referenceColumns[i] ? Ext.decode(referenceColumns[i]) : [],
				displayColumns = [];
			Ext.each(cols, function(col) {
				displayColumns.push(col.value);
			});
			var refTable = this.model({
				mtype: 'ReferenceTable',
				fieldLabel: displayNames[i],
				referenceTable: referenceTables[i],
				referenceColumn: refGridReferenceColumnName[i],
				referenceColumns: displayColumns,
				allowReferenceTableAdd: allowReferenceTableAdd[i] == 'true',
				allowReferenceTableRemove: allowReferenceTableRemove[i] == 'true',
				allowReferenceTableView: allowReferenceTableView[i] == 'true',
				referenceTableUrl: referenceTableUrl[i],
				referenceTableViewPopup: referenceTableViewPopup[i] == 'true',
				referenceTableViewPopupWidth: Number(referenceTableViewPopupWidth[i]),
				referenceTableViewPopupHeight: Number(referenceTableViewPopupHeight[i]),
				referenceTableViewPopupTitle: referenceTableViewPopupTitle[i]
			});
			if (Ext.Array.indexOf(['ReferenceTable', 'FileManager', 'URL'], refTable.referenceTableViewPopupTitle) > -1) {
				refTable.set('rsType', refTable.referenceTableViewPopupTitle);
				if (refTable.rsType == 'FileManager') this.rootVM.set('fileUploadIsEnabled', false);
			}
			refTable.referenceColumnStore.load({
				params: {
					tableName: refTable.referenceTable
				}
			});
			this.referenceTables.add(refTable);
		}

		if (len > 0) {
			this.referenceTables.removeAt(0);
			this.set('currentReference', this.referenceTables.getAt(0));
		}
	}
});

// STANDARD CONTROLS
/**
 * Model Definition for a TextField
 * uiType: TextField
 * xtype: textfield
 * serverEnum: UI_TEXTFIELD("TextField")
 */
glu.defModel('RS.formbuilder.TextField', {
	dbColumnType: ['TextField'],
	allowedMaxValue: 333,
	mixins: ['BaseControl', 'FormControl', 'DefaultValueControl', 'StringControl', 'RequireableControl', 'TooltipControl', 'EncryptableControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a TextAreaField
 * uiType: TextArea
 * xtype: textareafield
 * serverEnum: UI_TEXTAREA("TextArea")
 */
glu.defModel('RS.formbuilder.TextAreaField', {
	defaultHeight: 60,
	dbColumnType: ['TextArea'],
	mixins: ['BaseControl', 'FormControl', 'DefaultValueControl', 'StringControl', 'RequireableControl', 'TooltipControl', 'EncryptableControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});

glu.defModel('RS.formbuilder.WysiwygField', {
	defaultHeight: 160,
	columnSize: 4000,
	dbColumnType: ['TextArea', 'WysiwygField'],
	mixins: ['BaseControl', 'FormControl', 'DefaultValueControl', 'StringControl', 'TooltipControl', 'EncryptableControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a ReadOnlyTextField
 * uiType: TextField
 * xtype: displayfield
 * serverEnum: UI_READONLYTEXTFIELD("ReadOnlyTextField")
 */
/*glu.defModel('RS.formbuilder.ReadOnlyTextField', {
	dbColumnType: ['ReadOnlyTextField'],
	mixins: ['BaseControl', 'FormControl', 'DefaultValueControl', 'DatabaseControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});*/

glu.defModel('RS.formbuilder.SpacerField', {
	mixins: ['BaseControl', 'SizeControl', 'DependencyControl', 'SpacerControl']
});
glu.defModel('RS.formbuilder.SpacerControl', {
	id: '',
	inputName: 'spacer',
	inputType: 'INPUT',

	showHorizontalRule: false,

	ruleDisplay$: function() {
		return this.showHorizontalRule ? 'border-bottom:1px solid #999' : 'border-bottom:0px'
	},

	initMixin: function() {
		this.set('inputName', 'spacer' + Ext.id());
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getSpacerControlConversionProperties) : this.serializeFunctions = [this.getSpacerControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseSpacerControlConversionProperties) : this.deserializeFunctions = [this.parseSpacerControlConversionProperties];
	},

	getSpacerControlConversionProperties: function() {
		return {
			sourceType: this.inputType,
			id: this.id,
			name: this.inputName,
			showHorizontalRule: this.showHorizontalRule
		}
	},
	parseSpacerControlConversionProperties: function(config) {
		this.set('inputName', config.name)
		this.set('showHorizontalRule', config.showHorizontalRule)
	}
});

glu.defModel('RS.formbuilder.LabelField', {
	mixins: ['BaseControl', 'SizeControl', 'DependencyControl', 'LabelControl']
});
glu.defModel('RS.formbuilder.LabelControl', {
	inputName: 'label',
	inputType: 'INPUT',

	fieldLabel: '',

	initMixin: function() {
		this.set('inputName', 'label' + Ext.id());
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getLabelControlConversionProperties) : this.serializeFunctions = [this.getLabelControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseLabelControlConversionProperties) : this.deserializeFunctions = [this.parseLabelControlConversionProperties];
	},

	getLabelControlConversionProperties: function() {
		return {
			sourceType: this.inputType,
			id: this.id,
			name: this.inputName,
			displayName: this.fieldLabel
		}
	},
	parseLabelControlConversionProperties: function(config) {
		this.set('inputName', config.name)
		this.set('fieldLabel', config.displayName)
	}
})

glu.defModel('RS.formbuilder.JournalControl', {

	allowJournalEdit: false,

	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getJournalControlConversionProperties) : this.serializeFunctions = [this.getJournalControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseJournalControlConversionProperties) : this.deserializeFunctions = [this.parseJournalControlConversionProperties];
	},

	getJournalControlConversionProperties: function() {
		return {
			allowJournalEdit: this.allowJournalEdit
		}
	},
	parseJournalControlConversionProperties: function(config) {
		this.set('allowJournalEdit', config.allowJournalEdit)
	}
})


/**
 * Model definition for a NumberField
 * uiType: NumberTextField
 * xtype: numberfield
 * serverEnum: UI_NUMBERTEXTFIELD("NumberTextField")
 */
glu.defModel('RS.formbuilder.NumberField', {
	dbColumnType: ['NumberTextField'],
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'NumberControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a DecimalField
 * uiType: DecimalTextField
 * xtype: displayfield
 * serverEnum: UI_DECIMALTEXTFIELD("DecimalTextField")
 */
glu.defModel('RS.formbuilder.DecimalField', {
	dbColumnType: ['DecimalTextField'],
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'DecimalControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a CheckboxField
 * uiType: CheckBox
 * xtype: checkbox
 * serverEnum: UI_CHECKBOX("CheckBox")
 */
glu.defModel('RS.formbuilder.CheckboxField', {
	dbColumnType: ['CheckBox'],
	columnSize: 1000,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'DatabaseControl', 'DefaultBooleanControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a MultipleCheckboxField
 * uiType: MultipleCheckBox
 * xtype: checkboxgroup | checkbox in fieldset
 * serverEnum: UI_MULTIPLECHECKBOX("MultipleCheckBox")
 */
glu.defModel('RS.formbuilder.MultipleCheckboxField', {
	dbColumnType: ['MultipleCheckBox', 'MultiSelectComboBoxField'],
	columnSize: 1000,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'DatabaseControl', 'MultiChoiceControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a RadioButtonField
 * uiType: RadioButton
 * xtype: radiogroup
 * serverEnum: UI_CHOICE("RadioButton")
 */
glu.defModel('RS.formbuilder.RadioButtonField', {
	dbColumnType: ['RadioButton', 'ComboBox'],
	columnSize: 1000,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'DatabaseControl', 'ChoiceControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a ComboBoxField
 * uiType: ComboBox
 * xtype: combobox | selectfield
 * serverEnum: UI_COMBOBOX("ComboBox")
 */
glu.defModel('RS.formbuilder.ComboBoxField', {
	dbColumnType: ['ComboBox', 'RadioButton'],
	isCombobox: true,
	columnSize: 1000,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'AllowInputControl', 'DatabaseControl', 'ChoiceControl', 'DependencyControl', 'SizeControl']
});
/**
 * Model definition for a Multi-Select ComboBoxField
 * uiType: ComboBox
 * xtype: combobox | selectfield
 * serverEnum: UI_COMBOBOX("MultiSelectComboBox")
 */
glu.defModel('RS.formbuilder.MultiSelectComboBoxField', {
	dbColumnType: ['MultipleCheckBox', 'MultiSelectComboBoxField'],
	isCombobox: true,
	columnSize: 1000,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'AllowInputControl', 'DatabaseControl', 'MultiChoiceControl', 'DependencyControl', 'SizeControl']
});
/**
 * Model definition for a NameField
 * uiType:
 * xtype: namefield
 * serverEnum: UI_NAMEFIELD("NameField")
 */
glu.defModel('RS.formbuilder.NameField', {
	dbColumnType: ['NameField'],
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'DatabaseControl', 'SizeControl', 'NameControl', 'DependencyControl'],
	fieldLabel: 'fullName',
	firstName: '',
	middleInitial: '',
	middleName: '',
	lastName: '',
	columnSize: 1000,
	init: function() {
		if (this.fieldLabel == 'fullName') this.set('fieldLabel', this.localize('fullName'));
	}
});
/**
 * Model definition for a AddressField
 * uiType:
 * xtype: addressfield
 * serverEnum: UI_ADDRESS("AddressField")
 */
glu.defModel('RS.formbuilder.AddressField', {
	dbColumnType: ['AddressField'],
	columnSize: 1000,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'DatabaseControl', 'AddressControl', 'SizeControl', 'DependencyControl'],
	defaultHeight: 185
});
/**
 * Model definition for a EmailField
 * uiType:
 * xtype: emailfield
 * serverEnum: UI_EMAIL("EmailField")
 */
glu.defModel('RS.formbuilder.EmailField', {
	dbColumnType: ['EmailField'],
	allowedMaxValue: 333,
	columnSize: 333,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'DefaultValueControl', 'StringControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
glu.defModel('RS.formbuilder.MACAddressField', {
	dbColumnType: ['MACAddressField'],
	allowedMaxValue: 333,
	columnSize: 333,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'DefaultValueControl', 'StringControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
glu.defModel('RS.formbuilder.IPAddressField', {
	dbColumnType: ['IPAddressField'],
	allowedMaxValue: 333,
	columnSize: 333,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'DefaultValueControl', 'StringControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
glu.defModel('RS.formbuilder.CIDRAddressField', {
	dbColumnType: ['CIDRAddressField'],
	allowedMaxValue: 333,
	columnSize: 333,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'DefaultValueControl', 'StringControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a PhoneField
 * uiType:
 * xtype: phonefield
 * serverEnum: UI_PHONE("PhoneField")
 */
glu.defModel('RS.formbuilder.PhoneField', {
	allowedMaxValue: 40,
	columnSize: 40,
	dbColumnType: ['PhoneField'],
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a DateField
 * uiType: Date
 * xtype: datefield
 * serverEnum: UI_DATE("Date")
 */
glu.defModel('RS.formbuilder.DateField', {
	dbColumnType: ['Date'],
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'DateControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a TimeField
 * uiType: Timestamp
 * xtype: timefield
 * serverEnum: UI_TIMESTAMP("Timestamp")
 */
glu.defModel('RS.formbuilder.TimeField', {
	dbColumnType: ['Timestamp'],
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'TimeControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a DateTimeField
 * uiType: DateTime
 * xtype: datetimefield
 * serverEnum: UI_DATETIME("DateTime")
 */
glu.defModel('RS.formbuilder.DateTimeField', {
	dbColumnType: ['DateTime'],
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'DateTimeControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a LinkField
 * uiType: Link
 * xtype: textfield
 * serverEnum: UI_LINK("Link")
 */
glu.defModel('RS.formbuilder.LinkField', {
	dbColumnType: ['Link'],
	defaultValue: 'http://www.example.com',
	columnSize: 1000,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'DefaultValueControl', 'LinkControl', 'StringControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a LikeRTField
 * uiType:
 * xtype: likertfield
 * serverEnum:
 */
// glu.defModel('RS.formbuilder.LikeRTField', {
// 	dbColumnType: 'LikeRTField',
// 	mixins: ['BaseControl', 'FormControl', 'RequireableControl', 'DatabaseControl', 'SizeControl']
// });
/**
 * Model definition for a PasswordField
 * uiType: Password
 * xtype: textfield
 * serverEnum: UI_PASSWORD("Password")
 */
glu.defModel('RS.formbuilder.PasswordField', {
	dbColumnType: ['Password'],
	allowedMaxValue: 333,
	columnSize: 333,
	encrypted: true,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'DefaultValueControl', 'StringControl', 'DatabaseControl', 'SizeControl', 'DependencyControl'],
	encryptedIsEnabled$: function() {
		return false;
	}
});
/**
 * Model definition for a JournalField
 * uiType: Journal
 * xtype: journalfield
 * serverEnum: UI_JOURNALWIDGET("Journal")
 */
glu.defModel('RS.formbuilder.JournalField', {
	dbColumnType: ['Journal'],
	mixins: ['BaseControl', 'FormControl', 'DatabaseControl', 'TooltipControl', 'SizeControl', 'DependencyControl', 'JournalControl'],
	defaultHeight: 300,
	inputType: 'DB',
	columnSize: 4000
});
/**
 * Model definition for a ListField
 * uiType: List
 * xtype: multiselect
 * serverEnum: UI_LIST("List")
 */
// UI_LIST("List"),
glu.defModel('RS.formbuilder.ListField', {
	dbColumnType: ['List'],
	mixins: ['BaseControl', 'FormControl', 'RequireableControl', 'TooltipControl', 'EncryptableControl', 'MultiChoiceControl', 'DatabaseControl', 'SizeControl', 'DependencyControl'],
	defaultHeight: 150,
	columnSize: 1000
});
/**
 * Model definition for a SequenceField
 * uiType: Sequence
 * xtype: displayfield
 * serverEnum: UI_SEQUENCE("Sequence")
 */
glu.defModel('RS.formbuilder.SequenceField', {
	dbColumnType: ['Sequence'],
	allowedMaxValue: 40,
	columnSize: 40,
	inputType: 'DB',
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'SequenceControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a ReferenceField
 * uiType: Reference
 * xtype: displayfield
 * serverEnum: UI_REFERENCE("Reference")
 */
glu.defModel('RS.formbuilder.ReferenceField', {
	dbColumnType: ['Reference'],
	inputType: 'DB',
	columnSize: 40,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'DatabaseControl', 'ReferenceControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a HiddenField
 * uiType: HiddenField
 * xtype: hiddenfield
 * serverEnum: UI_HIDDEN("HiddenField")
 */
glu.defModel('RS.formbuilder.HiddenField', {
	dbColumnType: ['HiddenField'],
	columnSize: 333,
	mixins: ['BaseControl', 'FormControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a TagField
 * uiType: TagField
 * xtype: displayfield
 * serverEnum: UI_TAG("TagField")
 */
/*glu.defModel('RS.formbuilder.TagField', {
	dbColumnType: 'TagField',
	mixins: ['BaseControl', 'FormControl', 'RequireableControl', 'EncryptableControl', 'DefaultValueControl', 'StringControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});*/
/**
 * Model definition for a ButtonField
 * uiType:
 * xtype: button
 * serverEnum:
 */
glu.defModel('RS.formbuilder.ButtonField', {
	mixins: ['ButtonControl']
});
/**
 * Model definition for a UserPickerField
 * uiType:
 * xtype: userpickerfield
 * serverEnum: UI_USERPICKER("UserPickerField")
 */
glu.defModel('RS.formbuilder.UserPickerField', {
	dbColumnType: ['UserPickerField'],
	columnSize: 40,
	mixins: ['BaseControl', 'FormControl', 'RequireableControl', 'TooltipControl', 'EncryptableControl', 'DatabaseControl', 'GroupPickerControl', 'SizeControl', 'DependencyControl']
});

/**
 * Model definition for a TeamPickerField
 * uiType:
 * xtype: teampickerfield
 * serverEnum: UI_TEAMPICKER("TeamPickerField")
 */
glu.defModel('RS.formbuilder.TeamPickerField', {
	dbColumnType: ['TeamPickerField'],
	columnSize: 40,
	mixins: ['BaseControl', 'FormControl', 'RequireableControl', 'TooltipControl', 'EncryptableControl', 'DatabaseControl', 'TeamPickerControl', 'SizeControl', 'DependencyControl']
});

glu.defModel('RS.formbuilder.FileUploadField', {
	mixins: ['BaseControl', 'FileUploadControl', 'TooltipControl', 'SizeControl', 'DependencyControl'],
	defaultHeight: 150
});

glu.defModel('RS.formbuilder.ReferenceTableField', {
	mixins: ['BaseControl', 'ReferenceTableControl', 'TooltipControl', 'SizeControl', 'DependencyControl'],
	defaultHeight: 150
});

glu.defModel('RS.formbuilder.TabContainerField', {
	mixins: ['BaseControl', 'TooltipControl', 'TabContainerControl', 'SizeControl', 'DependencyControl'],
	defaultHeight: 150
});

glu.defModel('RS.formbuilder.SectionContainerField', {
	mixins: ['BaseControl', 'SizeControl', 'DependencyControl', 'SectionContainerControl']
});

glu.defModel('RS.formbuilder.UrlField', {
	mixins: ['BaseControl', 'UrlControl', 'TooltipControl', 'SizeControl', 'DependencyControl'],
	defaultHeight: 150
});
