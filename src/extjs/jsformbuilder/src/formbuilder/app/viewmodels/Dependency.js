glu.defModel('RS.formbuilder.Dependency', {

	initialValue: '',
	initialActionOptions: '',

	id: '',

	action: '',
	actionDisplay$: function() {
		return this.localize(this.action);
	},
	actionOptions$: function() {
		return this.action == 'setBgColor' ? this.color : '';
	},
	color: '',
	colorIsVisible$: function() {
		return this.action == 'setBgColor';
	},
	condition: '',
	conditionDisplay$: function() {
		return this.condition ? this.localize(this.condition) : '';
	},
	target: '',
	value$: function() {
		var val = this.stringValue;//'';
		// switch(this.targetType) {
		// case 'bool':
		// 	val = this.booleanValue;
		// 	break;
		// case 'number':
		// 	val = this.numberValue;
		// 	break;
		// case 'date':
		// 	val = this.dateValue;
		// 	break;
		// case 'string':
		// 	val = this.stringValue;
		// 	break;
		// }
		return val;
	},

	targetType$: function() {
		var target = this.target,
			type = '';
		if(target) {
			var control;
			this.rootVM.mainForm.controls.foreach(function(ctrl) {
				if(ctrl.inputName == target || ctrl.column == target) {
					control = ctrl;
					return false;
				}
			});
			type = this.getControlType(control);
		}

		return type;
	},

	getControlType: function(control) {
		var type = '';
		if(control) {
			switch(control.viewmodelName) {
			case 'CheckboxField':
				type = 'bool';
				break;
			case 'NumberField':
			case 'DecimalField':
				type = 'number';
				break;
			case 'DateField':
			case 'DateTimeField':
			case 'TimeField':
				type = 'date';
				break;
			default:
				type = 'string';
			}
		}
		return type;
	},

	stringValue: '',
	stringValueIsVisible$: function() {
		return true;//this.targetType == 'string';
	},
	// numberValue: '',
	// numberValueIsVisible$: function() {
	// 	return this.targetType == 'number';
	// },
	// dateValue: '',
	// dateValueIsVisible$: function() {
	// 	return this.targetType == 'date';
	// },
	// booleanValue: '',
	// booleanValueIsVisible$: function() {
	// 	return this.targetType == 'bool';
	// },

	stringValueIsEnabled$: function() {
		return this.condition !== "isEmpty" && this.condition !== "notEmpty";
	},

	isEdit: false,
	title$: function() {
		return this.isEdit ? this.localize('editDependencyTitle') : this.localize('addDependencyTitle');
	},

	fields: ['action', 'actionOptions', 'condition', 'target', 'value', 'id'],

	dependencyActionStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},

	dependencyConditionStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		listeners: {
			load: function() {
				var target = this.parentVM.target,
					type = '',
					conditions = [];
				this.removeAll();
				//Add in only the options that make sense for the target.  If there is no target, then display all of them
				var type = this.parentVM.targetType;

				if(target == 'activeTabIndex') isString = false;

				switch(type) {
				case 'number':
					this.add([{
						name: this.parentVM.localize('equals'),
						value: 'equals'
					}, {
						name: this.parentVM.localize('notequals'),
						value: 'notequals'
					}, {
						name: this.parentVM.localize('greaterThan'),
						value: 'greaterThan'
					}, {
						name: this.parentVM.localize('greaterThanOrEqual'),
						value: 'greaterThanOrEqual'
					}, {
						name: this.parentVM.localize('lessThan'),
						value: 'lessThan'
					}, {
						name: this.parentVM.localize('lessThanOrEqual'),
						value: 'lessThanOrEqual'
					}]);
					break;
				case 'date':
					this.add([{
						name: this.parentVM.localize('on'),
						value: 'on'
					}, {
						name: this.parentVM.localize('after'),
						value: 'after'
					}, {
						name: this.parentVM.localize('before'),
						value: 'before'
					}]);
					break;
				case 'bool':
					this.add([{
						name: this.parentVM.localize('equals'),
						value: 'equals'
					}, {
						name: this.parentVM.localize('notequals'),
						value: 'notequals'
					}]);
					break;
				case 'string':
				default:
					this.add([{
						name: this.parentVM.localize('equals'),
						value: 'equals'
					}, {
						name: this.parentVM.localize('notequals'),
						value: 'notequals'
					}, {
						name: this.parentVM.localize('Contains'),
						value: 'contains'
					}, {
						name: this.parentVM.localize('notcontains'),
						value: 'notcontains'
					}]);
					break;
				}

				this.add([{ 
					name: this.parentVM.localize('isEmpty'),
					value: 'isEmpty'
				}, { 
					name: this.parentVM.localize('notEmpty'),
					value: 'notEmpty'
				}]);
			}
		}
	},

	dependencyTargetStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		listeners: {
			load: function() {
				var fieldNames = this.rootVM.getFormFieldNames();
				Ext.each(fieldNames, function(name) {
					this.add({
						name: name,
						value: name
					})
				}, this);
				if(this.rootVM.mainForm.useTabs) this.add({
					name: 'activeTabIndex',
					value: 'activeTabIndex'
				});
				this.sort('name', 'ASC');
			}
		}
	},

	init: function() {
		this.dependencyActionStore.removeAll();
		this.dependencyActionStore.add([{
			name: this.localize('isVisible'),
			value: 'isVisible'
		}, {
			name: this.localize('isEnabled'),
			value: 'isEnabled'
		}, {
			name: this.localize('setBackgroundColor'),
			value: 'setBgColor'
		}]);

		this.dependencyConditionStore.removeAll();
		this.dependencyConditionStore.add([{
			name: this.parentVM.localize('equals'),
			value: 'equals'
		}, {
			name: this.parentVM.localize('notequals'),
			value: 'notequals'
		}, {
			name: this.parentVM.localize('Contains'),
			value: 'contains'
		}, {
			name: this.parentVM.localize('notcontains'),
			value: 'notcontains'
		}, {
			name: this.parentVM.localize('on'),
			value: 'on'
		}, {
			name: this.parentVM.localize('after'),
			value: 'after'
		}, {
			name: this.parentVM.localize('before'),
			value: 'before'
		}, {
			name: this.parentVM.localize('equals'),
			value: 'equals'
		}, {
			name: this.parentVM.localize('notequals'),
			value: 'notequals'
		}, {
			name: this.parentVM.localize('greaterThan'),
			value: 'greaterThan'
		}, {
			name: this.parentVM.localize('greaterThanOrEqual'),
			value: 'greaterThanOrEqual'
		}, {
			name: this.parentVM.localize('lessThan'),
			value: 'lessThan'
		}, {
			name: this.parentVM.localize('lessThanOrEqual'),
			value: 'lessThanOrEqual'
		},{ 
			name: this.parentVM.localize('isEmpty'),
			value: 'isEmpty'
		}, { 
			name: this.parentVM.localize('notEmpty'),
			value: 'notEmpty'
		}]);

		this.dependencyTargetStore.load();

		this.set('color', this.initialActionOptions);

		if(this.initialValue) {
			// switch(this.targetType) {
			// case 'string':
			// 	this.set('stringValue', this.initialValue);
			// 	break;
			// case 'number':
			// 	this.set('numberValue', this.initialValue);
			// 	break;
			// case 'date':
			// 	this.set('dateValue', this.initialValue);
			// 	break;
			// case 'bool':
			// 	this.set('booleanValue', this.initialValue);
			// 	break;
			// }
			this.set('stringValue', this.initialValue);
		}
	},

	applyDependency: function() {
		if(this.isEdit) this.parentVM.reallyEditDependency(this.asObject());
		else this.parentVM.reallyAddDependency(this.asObject());
		this.doClose();
	},
	cancel: function() {
		this.doClose();
	}
});