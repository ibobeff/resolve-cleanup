glu.defModel('RS.formbuilder.Viewer', {

	embed: true,

	activate: function(screen, params) {
		var values = Ext.clone(this.defaultValues)
		Ext.apply(values, params)
		this.loadData(this.defaultValues) //restore all default values

		//now apply values from activate
		Ext.Object.each(values, function(key) {
			if (Ext.isDefined(this[key])) this.set(key, values[key])
		}, this)
		this.correctInputs()
		//don't need to reload the form because the form automatically gets loaded when the formId changes
		// if (this.form && this.form.rendered) this.form.loadForm()
	},

	form: null,

	fields: ['formName',
		'name',
		'view',
		'formId',
		'sys_id',
		'recordId',
		'queryString',
		'query',
		'params',
		'formdefinition'
	],

	defaultValues: {
		formdefinition: false,
		formName: '',
		name: '',
		view: '',
		formId: '',
		sys_id: '',
		recordId: '',
		queryString: '',
		query: '',
		params: ''
	},

	formName: '',
	name: '',
	view: '',

	formId: '',

	sys_id: '',
	recordId: '',

	queryString: '',
	query: '',

	params: '',
	formdefinition: false,

	init: function() {
		this.correctInputs()
	},

	correctInputs: function() {
		this.set('formName', this.formName || this.name || this.view)
		this.set('recordId', this.recordId || this.sys_id)
		this.set('queryString', this.queryString || this.query)
	},

	registerForm: function(form) {
		this.set('form', form)
	},

	setFormRecordId: function(id) {
		this.form.setRecordId(id)
	},

	resetForm: function() {
		this.form.resetForm()
	},

	formLoaded: function() {
		this.fireEvent('formLoaded', this)
	},
	//circulating back to update the record id of the viewer
	actionCompleted: function(recordId, actions) {
		Ext.each(actions, function(action) {
			if (action.crudAction == 'SUBMIT')
				this.set('recordId', recordId);
		}, this);
	}
})