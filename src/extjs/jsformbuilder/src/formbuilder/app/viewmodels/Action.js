/**
 * Model definition for an Action.
 * An Action is contained in a button, and keeps track of the operation and associated value that needs to be executed on the server when the button is clicked
 */
glu.defModel('RS.formbuilder.Action', {
	id: '',
	/**
	 * Tells us whether this action is new or being edited
	 */
	isNew: true,
	/**
	 * Title of the action window 'Add Action' or 'Edit Action'
	 */
	actionTitle$: function() {
		return this.isNew ? this.localize('addAction') : this.localize('editAction');
	},
	/**
	 * The operation to perform on the server
	 */
	operation: '',
	/**
	 * The translated operation to display to the user
	 */
	operationName$: function() {
		return this.localize(this.operation);
	},
	/**
	 * The translated value of the operation (if one is set)
	 */
	value$: function() {
		switch (this.operation) {
			case 'DB':
				return this.localize(this.dbAction);
			case 'SIR':
				return this.localize(this.sirAction);
			case 'RUNBOOK':
				return this.RUNBOOK;
			case 'SCRIPT':
				return this.scriptName || this.SCRIPT;
			case 'EVENT':
				return this.EVENT_EVENTID;
			case 'MAPPING':
				return this.mappingTo;
			case 'ACTIONTASK':
				return this.ACTIONTASK;
			case 'RELOAD':
				return this.reloadDelay;
			case 'GOTO':
				return this.gotoTabName
		}

		return '';
	},
	/**
	 * True to indicate that the user shouldn't be able to select the db operation type because there is already one for the button
	 */
	alreadyHasDBOperation: false,
	/**
	 * The db action to perform if the operation is DB
	 */
	dbAction: 'SUBMIT',
	/**
	 * The sir action to perform if the operation is SIR
	 */
	sirAction: 'saveCaseData',
	/**
	 * The runbook to run if the operation is runbook
	 */
	RUNBOOK: '',
	runbookChange: function(newValue) {
		this.set('RUNBOOK', newValue)
	},
	/**
	 * The script to run if the operation is script
	 */
	SCRIPT: '',
	/**
	 * Formula for getting the script name to display to the user if the operation is script
	 */
	scriptName$: function() {
		var value = this.SCRIPT;
		if (this.getScripStoreCount() > 0) {
			var record = this.getScripStoreRecord(value);
			if (record) {
				return record.data.name;
			}
		}
		return this.SCRIPT;
	},
	getScripStoreCount : function(){
		return this.rootVM.mainForm.scriptStore.getCount();
	},
	getScripStoreRecord : function(value){
		return this.rootVM.mainForm.scriptStore.findRecord('value', value, 0, false, false, true);
	},
	//Runbook related
	WIKI: '',
	WIKIIsVisible$: function() {
		this.operation == 'RUNBOOK';
	},

	PROBLEMID: '',
	//related to both runbook and events
	PROBLEMIDIsVisible$: function() {
		return this.operation == 'RUNBOOK' || this.operation == 'EVENT' || this.operation == 'ACTIONTASK';
	},
	problemIdStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},

	//Event related
	EVENT_EVENTID: '',
	EVENT_EVENTIDIsVisible$: function() {
		return this.operation == 'EVENT'
	},
	EVENT_EVENTIDIsValid$: function() {
		if (this.operation == 'EVENT' && !this.EVENT_EVENTID) return this.localize('eventIdInvalid');
		return true;
	},
	// EVENT_REFERENCE: '',
	// EVENT_REFERENCEIsVisible$: function() {
	// 	return this.operation == 'EVENT'
	// },
	// EVENT_REFERENCEIsValid$: function() {
	// 	if (this.operation == 'EVENT' && this.EVENT_REFERENCE == '') return this.localize('eventReferenceInvalid');
	// 	return true;
	// },
	//Script related
	TIMEOUT: '',	// blank TIMEOUT means use the global.execute.timeout value defined in System Properties.
	usingDefault: false,
	synchronous$: function() {
		return (this.usingDefault || this.TIMEOUT >= 0);
	},
	asynchronous$: function() {
		return this.TIMEOUT < 0;
	},
	wait$: function() {
		return (this.usingDefault || this.TIMEOUT > 0);
	},
	waitSeconds: 0,
	waitSecondsIsEnabled$: function() {
		return (this.synchronous && !this.usingDefault);
	},

	useDefaultIsEnabled$: function() {
		return this.synchronous;
	},
	updateUseDefault: function() {
		this.set('usingDefault', !this.usingDefault);
	},
	timeoutGroup: 0,
	timeoutGroupIsVisible$: function() {
		return this.operation == 'SCRIPT'
	},
	scriptTimeout: '',
	scriptTimeoutIsVisible$: function() {
		return this.operation == 'SCRIPT';
	},

	mappingStore: {
		mtype: 'store',
		fields: ['name']
	},

	mappingFrom: '',
	mappingFromIsVisible$: function() {
		return this.operation == 'MAPPING' || this.operation == 'EVENT' && this.PROBLEMID == 'MAPFROM';
	},
	mappingFromIsValid$: function() {
		if (this.operation == 'MAPPING' && !this.mappingFrom) return this.localize('mappingFromInvalid');
		return true;
	},
	mappingTo: '',
	mappingToIsVisible$: function() {
		return this.operation == 'MAPPING'
	},
	mappingToIsValid$: function() {
		if (this.operation == 'MAPPING' && !this.mappingTo) return this.localize('mappingToInvalid');
		return true;
	},

	redirectTarget: '_blank',
	redirectTargetIsVisible$: function() {
		return this.operation == 'REDIRECT'
	},
	redirectTargetIsValid$: function() {
		if (this.operation == 'REDIRECT' && !this.redirectTarget) return this.localize('redirectTargetInvalid');
		return true;
	},
	redirectStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},
	redirectUrl: '',
	redirectUrlIsVisible$: function() {
		return this.operation == 'REDIRECT'
	},

	ACTIONTASK: '',
	ACTIONTASKIsVisible$: function() {
		return this.operation == 'ACTIONTASK';
	},
	ACTIONTASKIsValid$: function() {
		if (this.operation == 'ACTIONTASK' && !this.ACTIONTASK) return this.localize('actionTaskInvalid');
		return true;
	},

	showConfirmation: true,
	showConfirmationIsVisible$: function() {
		return this.operation == 'DB' && this.dbAction == 'DELETE';
	},

	messageBoxTitle: '',
	messageBoxTitleIsVisible$: function() {
		return this.operation == 'MESSAGEBOX'
	},
	messageBoxMessage: '',
	messageBoxMessageIsVisible$: function() {
		return this.operation == 'MESSAGEBOX'
	},
	messageBoxYesText: '',
	messageBoxYesTextIsVisible$: function() {
		return this.operation == 'MESSAGEBOX'
	},
	messageBoxNoText: '',
	messageBoxNoTextIsVisible$: function() {
		return this.operation == 'MESSAGEBOX'
	},

	reloadDelay: 0,
	reloadDelayIsVisible$: function() {
		return this.operation == 'RELOAD';
	},

	reloadDependencyIsVisible$: function() {
		return this.operation == 'RELOAD';
	},
	reloadTarget: '',
	reloadCondition: '',
	reloadValue: '',

	gotoTabName: '',
	gotoTabNameIsVisible$: function() {
		return this.operation == 'GOTO'
	},

	dependencyConditionStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		listeners: {
			load: function() {
				this.removeAll();
				//Add in only the options that make sense for the target.  If there is no target, then display all of them
				var target = this.parentVM.reloadTarget,
					isString = true;
				if (target) {
					var control;
					this.rootVM.mainForm.controls.foreach(function(ctrl) {
						if (ctrl.inputName == target || ctrl.column == target) {
							control = ctrl;
							return false;
						}
					});
					if (control) {
						switch (control.viewmodelName) {
							case 'NumberField':
							case 'DecimalField':
							case 'DateField':
							case 'DateTimeField':
							case 'TimeField':
								isString = false;
								break;
							default:
								isString = true;
						}
					}
				}

				if (target == 'activeTabIndex') isString = false;

				this.add({
					name: this.parentVM.localize('equals'),
					value: 'equals'
				}, {
					name: this.parentVM.localize('notequals'),
					value: 'notequals'
				});

				if (isString) {
					this.add([{
						name: this.parentVM.localize('contains'),
						value: 'contains'
					}, {
						name: this.parentVM.localize('notcontains'),
						value: 'notcontains'
					}]);
				} else {
					this.add([{
						name: this.parentVM.localize('greaterThan'),
						value: 'greaterThan'
					}, {
						name: this.parentVM.localize('greaterThanOrEqual'),
						value: 'greaterThanOrEqual'
					}, {
						name: this.parentVM.localize('lessThan'),
						value: 'lessThan'
					}, {
						name: this.parentVM.localize('lessThanOrEqual'),
						value: 'lessThanOrEqual'
					}]);
				}
			}
		}
	},

	dependencyTargetStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		listeners: {
			load: function() {
				var fieldNames = this.rootVM.getFormFieldNames();
				Ext.each(fieldNames, function(name) {
					this.add({
						name: name,
						value: name
					})
				}, this);
				if (this.rootVM.mainForm.useTabs) this.add({
					name: 'activeTabIndex',
					value: 'activeTabIndex'
				});
				this.sort('name', 'ASC');
			}
		}
	},


	leaf: true,

	/**
	 * List of important fields that should be used for serializing this action
	 */
	fields: ['operation', 'operationName', 'dbAction', 'RUNBOOK', 'SCRIPT', 'value', 'PROBLEMID', 'EVENT_EVENTID', 'EVENT_REFERENCE', 'TIMEOUT', 'waitSeconds', 'mappingFrom', 'mappingTo', 'redirectTarget', 'redirectUrl', 'ACTIONTASK', 'showConfirmation', 'messageBoxTitle', 'messageBoxMessage', 'messageBoxYesText', 'messageBoxNoText', 'reloadDelay', 'reloadTarget', 'reloadCondition', 'reloadValue', 'gotoTabName'],

	/**
	 * Type of item in the grid, because we're an action this is hard coded to action to indicate that we are an action to the UI
	 */
	type: 'action',

	/**
	 * Store to display different operations
	 */
	operationStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: []
	},


	/**
	 * Store to display different dbActions
	 */
	dbActionStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: []
	},

	/**
	 * Store to display different sirActions
	 */
	sirActionStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: []
	},

	init: function() {
		//populate the combobox stores
		this.operationStore.removeAll();
		this.operationStore.add([{
			name: this.localize('DB'),
			value: 'DB'
		}, {
			name: this.localize('SIR'),
			value: 'SIR'
		}, {
			name: this.localize('RUNBOOK'),
			value: 'RUNBOOK'
		}, {
			name: this.localize('EVENT'),
			value: 'EVENT'
		}, {
			name: this.localize('SCRIPT'),
			value: 'SCRIPT'
		}, {
			name: this.localize('MAPPING'),
			value: 'MAPPING'
		}, {
			name: this.localize('REDIRECT'),
			value: 'REDIRECT'
		}, {
			name: this.localize('CLOSE'),
			value: 'CLOSE'
		}, {
			name: this.localize('RESET'),
			value: 'RESET'
		}, {
			name: this.localize('ACTIONTASK'),
			value: 'ACTIONTASK'
		}, {
			name: this.localize('MESSAGEBOX'),
			value: 'MESSAGEBOX'
		}, {
			name: this.localize('reload'),
			value: 'RELOAD'
		}, {
			name: this.localize('returnText'),
			value: 'RETURN'
		}, {
			name: this.localize('exit'),
			value: 'EXIT'
		}]);
		this.operationStore.sort('name', 'ASC');

		if (this.parentVM.useTabs) {
			this.operationStore.add([{
				name: this.localize('NEXT'),
				value: 'NEXT'
			}, {
				name: this.localize('BACK'),
				value: 'BACK'
			}, {
				name: this.localize('GOTO'),
				value: 'GOTO'
			}]);
		}

		this.dbActionStore.removeAll();
		this.dbActionStore.add([{
			name: this.localize('submitAction'),
			value: 'SUBMIT'
		}, {
			name: this.localize('deleteActionName'),
			value: 'DELETE'
		}]);

		this.sirActionStore.removeAll();
		this.sirActionStore.add([{
			name: this.localize('saveCaseData'),
			value: 'saveCaseData'
		}]);

		this.problemIdStore.removeAll();
		switch (this.operation) {
			case 'RUNBOOK':
			case 'ACTIONTASK':
				this.problemIdStore.removeAll()
				this.problemIdStore.add([{
						name: this.localize('NEW'),
						value: 'NEW'
					}, {
						name: this.localize('LOOKUP'),
						value: 'LOOKUP'
					}, {
						name: this.localize('ACTIVE'),
						value: 'ACTIVE'
					},
					/*, {
					name: this.localize('NONE'),
					value: ''
				}, */
					{
						name: this.localize('MAPFROM'),
						value: 'MAPFROM'
					}
				])
				break;

			case 'EVENT':
				this.set('PROBLEMID', 'LOOKUP')
				this.problemIdStore.removeAll()
				this.problemIdStore.add([
					/*{
					name: this.localize('NEW'),
					value: 'NEW'
				},*/
					{
						name: this.localize('LOOKUP'),
						value: 'LOOKUP'
					},
					/*{
					name: this.localize('ACTIVE'),
					value: 'ACTIVE'
				}, {
					name: this.localize('NONE'),
					value: ''
				}, */
					{
						name: this.localize('MAPFROM'),
						value: 'MAPFROM'
					}
				])
				break;

			case 'SCRIPT':
				if (this.TIMEOUT == '') {
					this.set('usingDefault', true);
				}
				else {
					this.set('usingDefault', false);
				}
				break;
		
			default: 
				break;
		}

		var names = this.rootVM.getFormFieldNames();
		Ext.each(names, function(name) {
			this.mappingStore.add({
				name: name
			});
		}, this);

		this.rootVM.mainForm.hiddenFields.each(function(hiddenField) {
			this.mappingStore.add({
				name: hiddenField.data.name
			});
		}, this);

		this.mappingStore.add({
			name: 'PROBLEMID'
		});

		this.mappingStore.sort('name', 'ASC');

		this.redirectStore.add([{
			name: '_blank'
		}, {
			name: '_self'
		}, {
			name: '_parent'
		}, {
			name: '_top'
		}]);

		if (!this.messageBoxYesText) this.set('messageBoxYesText', this.localize('messageBoxYesText'));
		if (!this.messageBoxNoText) this.set('messageBoxNoText', this.localize('messageBoxNoText'));

		this.dependencyTargetStore.load();
		this.dependencyConditionStore.load();

		//Reload the runbook store because things might have changed
		this.rootVM.mainForm.runbookStore.load()
	},

	/**
	 * Determines if the dbAction combobox should be visible
	 */
	dbActionIsVisible$: function() {
		return this.operation == 'DB';
	},

	/**
	 * Determines if the sirAction combobox should be visible
	 */
	sirActionIsVisible$: function() {
		return this.operation == 'SIR';
	},

	/**
	 * Determines if the runbook combobox should be visible
	 */
	RUNBOOKIsVisible$: function() {
		return this.operation == 'RUNBOOK' || this.operation == 'EVENT';
	},

	/**
	 * Determines if the script combobox should be visible
	 */
	SCRIPTIsVisible$: function() {
		return this.operation == 'SCRIPT';
	},

	/**
	 * Button handler that forwards the addAction event to the parentVM to be handled properly
	 */
	addAction: function() {
		this.doClose();
		this.parentVM.addRealAction(this);
	},
	/**
	 * Button handler that just closes the window
	 */
	cancel: function() {
		this.doClose();
	},

	/**
	 * Determines if the addAction should be enabled because the form is valid
	 */
	addActionIsEnabled$: function() {
		return this.isValid;
	},

	/**
	 * Determines if the editAction should be enabled because the form is valid
	 */
	applyActionIsEnabled$: function() {
		return this.isValid;
	},

	/**
	 * Button handler that forwards the editAction event to the parentVM to be handled properly
	 */
	applyAction: function() {
		if (this.asynchronous === true) {
			this.set('TIMEOUT', -1);
		}
		else if (this.usingDefault) {
			this.set('TIMEOUT', '');
		}
		else {
			this.set('TIMEOUT', this.waitSeconds * 1000); //this.set('TIMEOUT', 0);
		}

		this.doClose();
		this.parentVM.editRealAction(this.asObject());
	},

	/**
	 * Determines if the operation is valid
	 */
	operationIsValid$: function() {
		if (!this.operation) return this.localize('actionOperationInvalid');
		//if(this.operation == 'DB' && this.alreadyHasDBOperation) return this.localize('actionOperationAlreadyDBInvalid');
		return true;
	},
	/**
	 * Determines if the dbAction is valid
	 */
	dbActionIsValid$: function() {
		if (this.operation == 'DB' && !this.dbAction) return this.localize('actionDbActionInvalid');
		return true;
	},
	/**
	 * Determines if the sirAction is valid
	 */
	sirActionIsValid$: function() {
		if (this.operation == 'SIR' && !this.sirAction) return this.localize('actionsirActionInvalid');
		return true;
	},
	/**
	 * Determines if the runbook is valid
	 */
	RUNBOOKIsValid$: function() {
		if ((this.operation == 'RUNBOOK' || this.operation == 'EVENT') && !this.RUNBOOK) return this.localize('actionRunbookInvalid');
		return true;
	},
	/**
	 * Determines if the script is valid
	 */
	SCRIPTIsValid$: function() {
		if (this.operation == 'SCRIPT' && !this.SCRIPT) return this.localize('actionScriptInvalid');
		return true;
	},

	when_operation_changes_default_problemId_and_load_problemId_store: {
		on: ['operationChanged'],
		action: function() {
			if (this.operation == 'RUNBOOK' || this.operation == 'ACTIONTASK') {
				this.set('PROBLEMID', 'NEW')
				this.problemIdStore.removeAll()
				this.problemIdStore.add([{
						name: this.localize('NEW'),
						value: 'NEW'
					}, {
						name: this.localize('LOOKUP'),
						value: 'LOOKUP'
					}, {
						name: this.localize('ACTIVE'),
						value: 'ACTIVE'
					},
					/*, {
				name: this.localize('NONE'),
				value: ''
			}, */
					{
						name: this.localize('MAPFROM'),
						value: 'MAPFROM'
					}
				])
			}
			if (this.operation == 'EVENT') {
				this.set('PROBLEMID', 'LOOKUP')
				this.problemIdStore.removeAll()
				this.problemIdStore.add([
					/*{
				name: this.localize('NEW'),
				value: 'NEW'
			},*/
					{
						name: this.localize('LOOKUP'),
						value: 'LOOKUP'
					},
					/*{
				name: this.localize('ACTIVE'),
				value: 'ACTIVE'
			}, {
				name: this.localize('NONE'),
				value: ''
			}, */
					{
						name: this.localize('MAPFROM'),
						value: 'MAPFROM'
					}
				])
			}
		}
	}
});