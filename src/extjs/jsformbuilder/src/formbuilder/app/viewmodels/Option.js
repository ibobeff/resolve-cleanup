/**
 * Model definition for an Option
 * An Option is stored in a control like a radio button or dropdown box.  It keeps track of the text the user types for the option
 * and also tracks its default value (which should be either checked or selected)
 */
glu.defModel('RS.formbuilder.Option', {
	/**
	 * Id of the option used for keeping things in sync, this is not persisted to the server
	 */
	id: '',
	/**
	 * Text of the option displayed to the user
	 */
	text: '',
	displayText$: function(){
		return this.text.split('=')[0];
	},
	init: function() {
		this.set('id', Ext.id());
	},
	/**
	 * Determines if the option is selected
	 */
	isSelected$: function() {
		return this.id == this.parentVM.defaultValue;
	},
	/**
	 * Sets the selected attribute of the option to true
	 * @param {Object} value
	 */
	setIsSelected: function(value) {
		if (value === true) this.parentVM.set('defaultValue', this.id);
	},
	/**
	 * Button Handler that forwards the event to the parentVM to properly add another option
	 */
	addOption: function() {
		this.parentVM.addOption(this);
	},
	/**
	 * Button Handler that forwards the event to the parentVM to properly remove this option
	 */
	removeOption: function() {
		this.parentVM.removeOption(this);
	},
	when_option_changes_update_store: {
		on: ['textChanged'],
		action: function(){
			this.parentVM.syncStore();
		}
	}
});

/**
 * Model definition for Checkbox Options
 * Checkbox Options are very similar to Options from radio buttons, but they can have multiple selected at the same time
 */
glu.defModel('RS.formbuilder.CheckOption', {
	/**
	 * Id of the option to keep things in sync, this is not persisted to the backend
	 */
	id: '',
	/**
	 * Text of the option displayed to the user
	 */
	text: '',
	displayText$: function(){
		return this.text.split('=')[0];
	},
	/**
	 * Whether the checkbox option is checked or not
	 */
	isChecked: false,
	/**
	 * Reactor to determine when the event for checking the checkbox is fired to forward that to the parentVM
	 */
	forward_events: {
		on: 'isCheckedChanged',
		action: function() {
			this.parentVM.checkControl(this);
		}
	},

	onTextChange_fireChangeInOptionStore: {
		on: 'textChanged',
		action: function() {
			this.parentVM.options.fireEvent('edited', this, this.parentList.indexOf(this));
		}
	},
	init: function() {
		this.set('id', Ext.id());
	},
	/**
	 * Button Handler that forwards the event to the parentVM to properly add another option
	 */
	addOption: function() {
		this.parentVM.addOption(this);
	},
	/**
	 * Button handler that forwards the event to the parentVM to properly remove this option
	 */
	removeOption: function() {
		this.parentVM.removeOption(this);
	}
});