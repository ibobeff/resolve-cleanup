glu.defModel('RS.formbuilder.Forms', {
	mock: false,

	displayName: '',

	state: '',

	stateId: 'formbuilderforms',

	store: null,

	columns: null,

	formsSelections: [],

	activate: function() {
		clientVM.setWindowTitle(this.localize('formsWindowTitle'))
		this.loadForms()
	},

	init: function() {
		if (this.mock) this.backend = RS.formbuilder.createMockBackend(true);
		this.set('displayName', this.localize('formsTitle'));
		this.set('store', Ext.create('Ext.data.Store', {
			fields: ['id', 'uformName', 'udisplayName', 'utableName'].concat(RS.common.grid.getSysFields()),
			remoteSort: true,
			sorters: [{
				property: 'uformName',
				direction: 'ASC'
			}],
			proxy: {
				type: 'ajax',
				url: '/resolve/service/form/list',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		}));
		this.set('columns', [{
			header: '~~uformName~~',
			dataIndex: 'uformName',
			filterable: true,
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~udisplayName~~',
			dataIndex: 'udisplayName',
			filterable: true,
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~utableName~~',
			dataIndex: 'utableName',
			filterable: true,
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		}].concat(RS.common.grid.getSysColumns()));
	},

	loadForms: function() {
		this.set('state', 'wait');
		this.store.load({
			scope: this,
			callback: function(rec, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('ListFormsErrMsg'));
					return;
				}
				this.set('state', 'ready');
			}
		});
	},

	editForm: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.formbuilder.Main',
			params: {
				id: id
			}
		});
	},

	createForm: function() {
		clientVM.handleNavigation({
			modelName: 'RS.formbuilder.Main',
			params: {
				id: ''
			}
		});
	},

	deleteForms: function() {
		this.message({
			title: this.localize('DeleteFormsTitle'),
			msg: this.localize(this.formsSelections.length > 1 ? 'DeleteFormsMsg' : 'DeleteFormMsg', {
				num: this.formsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteForms'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.sendDeleteReq
		});
	},

	sendDeleteReq: function(btn) {
		if (btn != 'yes') {
			this.set('state', 'itemSelected');
			return;
		}

		this.set('state', 'wait');

		var ids = [];

		Ext.each(this.formsSelections, function(r) {
			ids.push(r.get('id'));
		}, this);
		this.ajax({
			url: '/resolve/service/form/delete',
			jsonData: ids,
			scope: this,
			success: this.handleDeleteSucResp,
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	handleDeleteSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('DeleteErrMsg') + '[' + respData.message + ']');
		} else
			clientVM.displaySuccess(this.localize('DeleteSucMsg'));
		this.loadForms();
	},

	viewForm: function() {
		clientVM.handleNavigation({
			modelName: 'RS.formbuilder.Viewer',
			params: {
				name: this.formsSelections[0].get('uformName')
			}
		})
	},

	viewFormIsEnabled$: function() {
		return this.formsSelections.length == 1 && !this.wait;
	},

	createFormIsEnabled$: function() {
		return this.state != 'wait';
	},

	deleteFormsIsEnabled$: function() {
		return this.state === 'itemSelected';
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},

	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	when_formsSelections_changed: {
		on: ['formsSelectionsChanged'],
		action: function() {
			if (this.formsSelections.length > 0)
				this.set('state', 'itemSelected')
			else
				this.set('state', 'ready');
		}
	}
});