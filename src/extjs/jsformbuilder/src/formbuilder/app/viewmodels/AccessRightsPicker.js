glu.defModel('RS.formbuilder.AccessRightsPicker', {

	init: function() {
		this.roles.load({
			callback: this.rolesLoaded,
			scope: this
		});
	},

	rolesLoaded: function(records, operation, success) {
		for (var i = 0; i < this.currentRights.length; i++) {
			var currentRight = this.currentRights[i];
			var role = this.roles.findBy(function(role) {
				if (role.data.value == currentRight.value) return true;
				return false;
			});
			this.roles.removeAt(role);
		}
	},

	currentRights: [],

	roles: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getRoles',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	rolesSelections: [],

	cancel: function() {
		this.doClose();
	},
	addAccessRightsAction: function() {
		this.parentVM.addRealAccessRights(this.rolesSelections);
		this.doClose();
	},
	addAccessRightsActionIsEnabled$: function() {
		return this.rolesSelections.length > 0;
	}
});