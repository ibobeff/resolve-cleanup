glu.defModel('RS.formbuilder.TabPanel', {
	tabs: {
		mtype: 'activatorlist',
		focusProperty: 'selectedTab',
		autoParent: true
	},
	tabStore: {
		mtype: 'store',
		fields: ['title'],
		data: [],
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'tabs'
		}]
	},
	selectedTab: 0,
	lastSelectedTab: 0,
	initialized: false,
	init: function() {
		//Add one tab to get the ball rolling
		this.addTab();
		this.set('selectedTab', this.tabs.getAt(0));
		this.set('lastSelectedTab', this.tabs.getAt(0));
	},
	when_tabsSelections_change_select_newTab: {
		on: ['tabsSelectionsChanged'],
		action: function() {
			if (this.tabsSelections.length == 0 && this.tabStore.getCount() > 0) this.set('tabsSelections', [this.tabStore.getAt(0)]);
			else this.set('selectedTab', this.tabs.getAt(this.tabStore.indexOf(this.tabsSelections[0])));
		}
	},
	addTab: function(config, index) {
		if (config == Ext.EventObject) {
			this.parentVM.addTab();
			return;
		}
		var tab = this.model({
			mtype: 'Tab'
		});
		tab.init();
		for (var key in config) {
			tab.set(key, config[key]);
		}
		return this.tabs.insert(index >= 0 ? index + 1 : this.tabs.length, tab);
	},
	tabsSelections: [],
	editTabIsEnabled$: function() {
		return this.tabsSelections.length == 1
	},
	editTab: function() {
		var tabSelection = this.tabsSelections[0],
			selectedTab, tabSelectionIndex = this.tabStore.indexOf(tabSelection);
		if (tabSelectionIndex > -1) {
			selectedTab = this.tabs.getAt(tabSelectionIndex);
			this.syncCurrentTab();
			this.set('selectedTab', selectedTab);
			this.open(Ext.applyIf({
				mtype: 'Tab'
			}, selectedTab.asObject()), 'detail');
		}
	},
	editRealTab: function(tabConfig) {
		var tabSelection = this.tabsSelections[0],
			tabSelectionIndex = this.tabStore.indexOf(tabSelection),
			selectedTab;
		if (tabSelectionIndex > -1) {
			selectedTab = this.tabs.getAt(tabSelectionIndex);
			selectedTab.loadData(tabConfig);
			selectedTab.parseDependencyControlConversionProperties(tabConfig.dependenciesJson);
			this.tabs.fireEvent('edited', selectedTab, tabSelectionIndex);
		}
	},
	removeTabIsEnabled$: function() {
		return this.tabsSelections.length == 1
	},
	removeTab: function() {
		var tabSelection = this.tabsSelections[0],
			selectedTab, tabSelectionIndex = this.tabStore.indexOf(tabSelection);
		if (tabSelectionIndex > -1) {
			selectedTab = this.tabs.getAt(tabSelectionIndex);
			this.syncCurrentTab();
			this.set('selectedTab', selectedTab);
			if (this.selectedTab.controls.length > 0) {
				this.parentVM.confirmTabRemoval();
			} else this.tabs.remove(this.selectedTab);
		}
	},
	when_selectedTab_changes_store_existing_controls_and_restore_tab_controls: {
		on: ['selectedTabChanged'],
		action: function() {
			//store controls to the lastSelectedTab
			if (this.lastSelectedTab) this.lastSelectedTab.controls = [];
			this.parentVM.controls.foreach(function(control) {
				if (control != this.parentVM.tabPanel) {
					this.lastSelectedTab.controls.push(control);
				}
			}, this)

			//Setup the last selected tab after we've stored off the information
			this.lastSelectedTab = this.selectedTab;

			//Then remove them from the form
			while (this.parentVM.controls.length > 1) {
				this.parentVM.controls.removeAt(1);
			}

			//And finally restore the newly selected tab controls
			if (this.selectedTab) {
				Ext.each(this.selectedTab.controls, function(control) {
					if (control) this.parentVM.controls.add(control)
				}, this);
			}
		}
	},
	syncCurrentTab: function() {
		if (this.selectedTab) {
			this.selectedTab.controls = [];
			this.parentVM.controls.foreach(function(control) {
				if (control && control != this.parentVM.tabPanel) {
					this.selectedTab.controls.push(control);
				}
			}, this);
		}
	},
	clearControls: function() {
		this.tabs.foreach(function(tab) {
			tab.controls = [];
		})
	}
});

glu.defModel('RS.formbuilder.Tab', {
	mixins: ['DependencyControl'],
	id: '',
	title: 'Untitled Tab',
	controls: [],
	fields: ['title'],
	init: function() {
		if (this.title === 'Untitled Tab') this.set('title', this.localize('untitledTab'));
	},
	// closable$: function() {
	// 	return this.parentVM.tabs.length > 1;
	// },
	asObject: function() {
		return {
			title: this.title,
			dependenciesJson: this.getDependencyControlConversionProperties().dependencies
		}
	},
	applyTab: function() {
		this.doClose();
		this.parentVM.editRealTab(this.asObject());
	},
	cancel: function() {
		this.doClose();
	}
});