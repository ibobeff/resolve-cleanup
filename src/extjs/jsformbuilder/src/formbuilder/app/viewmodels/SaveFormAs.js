glu.defModel('RS.formbuilder.SaveFormAs', {
	title: 'UntitledForm',
	name: 'UNTITLEDFORM',
	nameIsValid$: function() {
		if (this.name.length === 0) return this.localize('formNameEmptyInvalidText');
		if (this.name.length > 100) return this.localize('formNameTooLongInvalidText');
		if (/[^A-Z|_|0-9]/g.test(this.name)) return this.localize('formNameInvalidText');
		return this.parentVM.existingFormNames.findRecord('name', this.name, 0, false, false, true) ? this.localize('nameInvalidText', this.name) : true;
	},
	/**
	 * Validation formula to make sure that the title isn't a duplicate
	 */
	titleIsValid$: function() {
		if (this.title.length > 500) return this.localize('titleTooLongInvalidText');
		return true;
	},

	saveFormAs: function() {
		this.parentVM.reallySaveFormAs(this.name, this.title);
		this.doClose();
	},
	saveFormAsIsEnabled$: function() {
		return this.isValid;
	},
	cancel: function() {
		this.doClose();
	}
});