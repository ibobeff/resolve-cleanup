glu.defModel('RS.formbuilder.FormPicker', {

	showCreate: false,

	formPickerTitle: '',
	title$: function() {
		return this.formPickerTitle || this.localize('formPickerTitle')
	},


	searchQuery: '',
	when_searchQueryChanges_update_grid: {
		on: ['searchQueryChanged'],
		action: function() {
			this.forms.load()
		}
	},

	filter: [],
	startsWithBase: '',

	forms: {
		mtype: 'store',
		fields: ['id', 'uformName', 'udisplayName', 'utableName'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'uformName',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	formsColumns: [{
		header: '~~uformName~~',
		dataIndex: 'uformName',
		filterable: true,
		flex: 1,
		renderer: RS.common.grid.plainRenderer()
	}, {
		header: '~~udisplayName~~',
		dataIndex: 'udisplayName',
		filterable: true,
		flex: 1,
		renderer: RS.common.grid.plainRenderer()
	}, {
		header: '~~utableName~~',
		dataIndex: 'utableName',
		filterable: true,
		flex: 1,
		renderer: RS.common.grid.plainRenderer()
	}].concat(RS.common.grid.getSysColumns()),

	formsSelections: [],

	init: function() {
		Ext.Array.forEach(this.filter, function(f) {
			if (f.condition == 'startsWith') {
				this.set('startsWithBase', f.value)
			}
		}, this)

		this.forms.on('beforeload', function(store, operation) {
			operation.params = operation.params || {}
			var filter = this.filter;

			if (this.searchQuery) {
				filter = [{
					field: 'uformName',
					condition: 'startsWith',
					value: this.startsWithBase + this.searchQuery
				}]
			}

			if (filter && filter.length > 0)
				operation.params.filter = Ext.encode(filter)
		}, this)

		this.forms.load()
	},
	dumper: null,
	select: function() {
		if (this.dumper)
			this.dumper(this.formsSelections);
		else
			this.parentVM.set('name', this.formsSelections[0].get('uformName'))
		this.doClose()
	},
	selectIsEnabled$: function() {
		return this.formsSelections.length > 0
	},
	cancel: function() {
		this.doClose()
	},

	createFormIsVisible$: function() {
		return this.showCreate
	},
	createForm: function() {
		this.parentVM.createForm()
		this.doClose()
	}
})