/**
 * Model definition for ImportOption
 * The import option model has all the available options for importing different choices into a choice/select control
 */
glu.defModel('RS.formbuilder.ImportOption', {
	/**
	 * The option text to display to the user (i.e. the text of the actually selected option)
	 */
	optionText: '',
	/**
	 * The selected option that the user has chosen
	 */
	selectedOption: '',

	/**
	 * Reactor for when the selection changes to populate the textarea with the text
	 */
	whenSelectionChanges: {
		on: ['selectedOptionChanged'],
		action: function() {
			var optionName = this.selectedOption[0],
				realOption;
			this.availableOptions.each(function(option) {
				if (option.data.name == optionName) realOption = option;
			});
			if (realOption) {
				this.set('optionText', realOption.data.text);
			}
		}
	},
	/**
	 * List of available options for the user to import
	 */
	availableOptions: {
		mtype: 'store',
		fields: ['name', 'text'],
		data: [{
			name: 'gender',
			text: ['Male', 'Female', 'Prefer Not to Answer'].join('\n')
		}, {
			name: 'age',
			text: ['Under 18', '8-24', '25-34', '35-44', '45-54', '55-64', '65 or Above', 'Prefer Not to Answer'].join('\n')
		}, {
			name: 'employment',
			text: ['Employed Full-Time', 'Employed Part-Time', 'Self-employed', 'Not employed, but looking for work', 'Not employed and not looking for work', 'Homemaker', 'Retired', 'Student', 'Prefer Not to Answer'].join('\n')
		}, {
			name: 'income',
			text: ['Under $20,000', '$20,000 - $30,000', '$30,000 - $40,000', '$40,000 - $50,000', '$50,000 - $75,000', '$75,000 - $100,000', '$100,000 - $150,000', '$150,000 or more', 'Prefer Not to Answer'].join('\n')
		}, {
			name: 'education',
			text: ['Some High School', 'High School Graduate or Equivalent', 'Trade or Vocational Degree', 'Some College', 'Associate Degree', 'Bachelor\'s Degree', 'Graduate of Professional Degree', 'Prefer Not to Answer'].join('\n')
		}, {
			name: 'daysOfTheWeek',
			text: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'].join('\n')
		}, {
			name: 'monthsOfTheYear',
			text: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'].join('\n')
		}, {
			name: 'timezones',
			text: ['(-12:00) International Date Line', '(-11:00) Midway Island, Samoa', '(-10:00) Hawaii', '(-9:00) Alaska', '(-8:00) Pacific', '(-7:00) Mountain', '(-6:00) Central', '(-5:00) Eastern', '(-4:30) Venezuela', '(-4:00) Atlantic', '(-3:30) Newfoundland', '(-3:00) Brasilia, Buenos Aires, Georgetown', '(-2:00) Mid-Atlantic', '(-1:00) Azores, Cape Verde Isles', '(0:00) Dublin, London, Monrovia, Casablanca', '(+1:00) Paris, Madrid, Athens, Berlin, Rome', '(+2:00) Eastern Europe, Harare, Pretoria, Israel', '(+3:00) Moscow, Baghdad, Kuwait, Nairobi', '(+3:30) Tehran', '(+4:00) Abu Dhabi, Muscat, Tbilisi, Kazan', '(+4:30) Kabul', '(+5:00) Islamabad, Karachi, Ekaterinburg', '(+5:30) Bombay, Calcutta, Madras, New Delhi', '(+6:00) Almaty, Dhaka', '(+7:00) Bangkok, Jakarta, Hanoi', '(+8:00) Beijing, Hong Kong, Perth, Singapore', '(+9:00) Tokyo, Osaka, Sapporo, Seoul, Yakutsk', '(+9:30) Adelaide, Darwin', '(+10:00) Sydney, Guam, Port Moresby, Vladivostok', '(+11:00) Magadan, Solomon Isles, New Caledonia', '(+12:00) Fiji, Marshall Isles, Wellington, Auckland'].join('\n')
		}, {
			name: 'states',
			text: ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'].join('\n')
		}, {
			name: 'continents',
			text: ['Africa', 'Antarctica', 'Asia', 'Australia', 'Europe', 'North America', 'South America'].join('\n')
		}, {
			name: 'howOften',
			text: ['Everyday', 'Once a week', '2 to 3 times a month', 'Once a month', 'Less than once a month'].join('\n')
		}, {
			name: 'howLong',
			text: ['Less than a month', '1-6 months', '1-3 years', 'Over 3 Years', 'Never used'].join('\n')
		}, {
			name: 'satisfaction',
			text: ['Very Satisfied', 'Satisfied', 'Neutral', 'Unsatisfied', 'Very Unsatisfied', 'N/A'].join('\n')
		}, {
			name: 'importance',
			text: ['Very Important', 'Important', 'Neutral', 'Somewhat Important', 'Not at all Important', 'N/A'].join('\n')
		}, {
			name: 'agreement',
			text: ['Strongly Agree', 'Agree', 'Neutral', 'Disagree', 'Strongly Disagree', 'N/A'].join('\n')
		}, {
			name: 'comparison',
			text: ['Much Better', 'Somewhat Better', 'About the Same', 'Somewhat Worse', 'Much Worse', 'Don\'t Know'].join('\n')
		}, {
			name: 'wouldYou',
			text: ['Definitely', 'Probably', 'Not Sure', 'Probably Not', 'Definitely Not'].join('\n')
		}, {
			name: 'nflTeams',
			text: ['Arizona Cardinals', 'Atlanta Falcons', 'Baltimore Ravens', 'Buffalo Bills', 'Carolina Panthers', 'Chicago Bears', 'Cincinnati Bengals', 'Cleveland Browns', 'Dallas Cowboys', 'Denver Broncos', 'Detroit Lions', 'Green Bay Packers', 'Houston Texans', 'Indianapolis Colts', 'Jacksonville Jaguars', 'Kansas City Chiefs', 'Miami Dolphins', 'Minnesota Vikings', 'New England Patriots', 'New Orleans Saints', 'New York Giants', 'New York Jets', 'Oakland Raiders', 'Philadelphia Eagles', 'Pittsburgh Steelers', 'San Diego Chargers', 'San Francisco 49ers', 'Seattle Seahawks', 'St. Louis Rams', 'Tampa Bay Buccaneers', 'Tennessee Titans', 'Washington Redskins'].join('\n')
		}]
	},
	/**
	 * Button Handler to close the window
	 */
	cancel: function() {
		this.doClose();
	},
	/**
	 * Button Handler that adds the selected options to the options of the control
	 */
	importOption: function() {
		//import the options from the textarea into the options for the parentVM
		var text = this.optionText;
		var options = text.split('\n');
		Ext.each(options, function(option) {
			if (option) this.parentVM.addOption(null, option);
		}, this);
		this.doClose();
	},

	init: function() {
		this.availableOptions.each(function(option) {
			option.data.name = this.localize(option.data.name);
		}, this);
	}
});