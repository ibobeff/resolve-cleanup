/**
 * Model definition for the PreviewTable
 * The preview table is a table of the changes that will be made to the database on the server side.
 * Its a last stop for users to make sure that what they are creating is what they think they are really creating.
 */
glu.defModel('RS.formbuilder.PreviewTable', {
	/**
	 * The current serialized form to be sent to the server
	 */
	formConfig: {},
	/**
	 * The table name to display in the title of the grid
	 */
	tableName: '',
	/**
	 * The database fields configured in the form and on the server that are displayed to the user
	 */
	tableFields: {
		mtype: 'list',
		autoParent: true
	},

	wikiOption: 'none',

	/**
	 * Name of the wiki document to create with this form when the user saves it
	 */
	wikiName: '',
	wikiNameCreate: '',

	noWiki: false,
	createWiki: false,
	addToWiki: false,

	showCreateWiki: true,

	wikiNameIsEnabled$: function() {
		return this.addToWiki;
	},

	wikiNameCreateIsEnabled$: function() {
		return this.createWiki;
	},

	wikiNameCreateIsValid$: function() {
		if (this.createWiki) {
			if (!this.wikiNameCreate) return this.localize('wikiNameCreateBlankInvalid');
			if (this.wikiNames.findRecord('name', this.wikiNameCreate, 0, false, false, true)) return this.localize('wikiNameCreateInvalid', [this.wikiNameCreate]);
		}
		return true;
	},

	wikiNameIsValid$: function() {
		if (this.addToWiki && !this.wikiName) return this.localize('wikiNameInvalid');
		if (this.addToWiki && !this.wikiNames.findRecord('name', this.wikiName, 0, false, false, true)) return this.localize('wikiNameAddInvalid', [this.wikiNameCreate]);
		return true;
	},

	/**
	 * Store to keep track of the valid value list of wiki names in the system
	 */
	wikiNames: {
		mtype: 'store',
		model: 'RS.formbuilder.model.WikiName'
	},

	/**
	 * Store to display the tableFields to the user in a grid
	 */
	tableStore: {
		mtype: 'store',
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'tableFields'
		}],
		fields: ['columnName', 'action', 'actionState'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		},
		data: []
	},
	/**
	 * Flag to indicate that we are going to create a new table
	 */
	isNewTable: true,

	previewWarning: '',
	schemaUpdate: false,
	previewWarningIsVisible$: function() {
		return this.schemaUpdate
	},

	sizeExceeded: false,
	sizeWarning: '',
	sizeWarningIsVisible$: function() {
		return this.sizeExceeded
	},

	init: function() {
		this.set('previewWarning', this.localize('previewWarning'))
		this.set('sizeWarning', this.localize('sizeWarning'))
		this.wikiNames.load({
			scope: this,
			callback: this.compareWikiNames,
			params: {
				wikiName: this.formConfig.wikiName
			}
		})

		var name = this.formConfig.tableName;
		this.set('tableName', name);

		//get each column to be used for this form
		var fields = [],
			p = 0,
			t = 0,
			c = 0,
			f = 0;
		for (p = 0; p < this.formConfig.panels.length; p++) {
			for (t = 0; t < this.formConfig.panels[p].tabs.length; t++) {
				for (c = 0; c < this.formConfig.panels[p].tabs[t].columns.length; c++) {
					for (f = 0; f < this.formConfig.panels[p].tabs[t].columns[c].fields.length; f++) {
						fields.push(this.formConfig.panels[p].tabs[t].columns[c].fields[f]);
						if (this.formConfig.panels[p].tabs[t].columns[c].fields[f].uiType == 'SectionContainerField') {
							var section = this.formConfig.panels[p].tabs[t].columns[c].fields[f],
								columns = Ext.decode(section.sectionColumns);
							Ext.each(columns, function(column) {
								Ext.each(column.controls, function(control) {
									if (control) fields.push(control);
								})
							})
						}
					}
				}
			}
		}
		this.formConfig.panels[0].tabs[0].columns[0].fields;
		var newTableColumn = this.localize('noChangeTableColumn');
		Ext.each(fields, function(field) {
			if (field.sourceType == 'DB') {
				this.tableFields.add({
					mtype: 'viewmodel',
					fields: ['columnName', 'action', 'actionState', 'size'],
					//for serialization
					columnName: field.dbcolumn,
					action: newTableColumn,
					actionState: 0,
					size: Number(field.stringMaxLength) || 0
				});
			}
		}, this);

		Ext.Ajax.request({
			url: '/resolve/service/form/getTableData',
			params: {
				tableName: name
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					this.compareTable(response.data);
				} else clientVM.displayError(response.message);
			},
			failure: function(r){
				clientVM.displayFailure(r);
			}
		})
	},
	compareWikiNames: function() {
		if (this.wikiNames.getById(this.formConfig.wikiName)) {
			this.set('wikiName', this.formConfig.wikiName);
			this.set('addToWiki', true);
			this.set('showCreateWiki', false);
		} else if (this.formConfig.wikiName) {
			this.set('wikiNameCreate', this.formConfig.wikiName);
			this.set('createWiki', true);
			this.set('showCreateWiki', true);
		} else {
			this.set('noWiki', true);
		}
	},
	/**
	 * Compares the existing form table data with the server table data to determine new, change, or remove status for each column
	 * @param {Object} serverTable
	 */
	compareTable: function(serverTable) {
		this.set('isNewTable', serverTable.newTable);
		if (serverTable.newTable) {
			this.tableFields.foreach(function(tableField, index) {
				tableField.set('action', this.localize('newTableColumn'));
				tableField.set('actionState', 1);
				this.tableFields.fireEvent('edited', tableField.asObject(), index);
			}, this);
		} else {
			var columns = serverTable.fields || [];

			this.tableFields.foreach(function(tableField, index) {
				var found = false;
				Ext.each(columns, function(column) {
					if (column.ucolumnModelName == tableField.columnName) {
						found = true;
						return false;
					}
				});
				if (!found) {
					tableField.set('action', this.localize('newTableColumn'));
					tableField.set('actionState', 1);
					this.tableFields.fireEvent('edited', tableField.asObject(), index);
					this.set('schemaUpdate', true);
				}
			}, this);

			this.tableStore.sort('action', 'ASC');
		}
		//Go through each of the columns and see what the size is, sum it up, and if its > 15000 display size warning
		var sum = 0;
		this.tableFields.foreach(function(tableField) {
			sum += tableField.size;
		})
		if (sum > 15000) this.set('sizeExceeded', true)
	},
	/**
	 * Button Handler to close the window and cancel the saving of the form
	 */
	cancel: function() {
		this.doClose();
	},

	saveAndUpdateIsEnabled$: function() {
		return this.isValid;
	},
	saveAndCreateIsEnabled$: function() {
		return this.isValid;
	},
	/**
	 * Button Handler to save the form to the server
	 */
	saveAndUpdate: function() {
		this.parentVM.confirmSave(this);
		this.doClose();
	},
	/**
	 * Button Handler to save the form to the server
	 */
	saveAndCreate: function() {
		this.parentVM.confirmSave(this);
		this.doClose();
	},

	save: function() {
		this.parentVM.confirmSave(this)
		this.doClose()
	}
});