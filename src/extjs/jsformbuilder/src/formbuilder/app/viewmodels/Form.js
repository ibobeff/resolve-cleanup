/**
 * Model definition for a Form
 * A form is the main component that we are trying to build with the form builder.  It contains all the controls, hiddenFields, and buttons that should comprise the form
 */
glu.defModel('RS.formbuilder.Form', {
    /**
     * Id of the form stored on the server
     */
    id: '',

    isAdmin$: function() {
        return this.parentIsAdmin();
    },
    parentIsAdmin : function(){
        return this.parentVM.isAdmin;
    },
    /**
     * Title of the form to display to the user
     */
    formTitle : 'UntitledForm',
    title: 'UntitledForm',
    name: 'UNTITLEDFORM',
    properName$: function() {
        return this.name.toUpperCase();
    },

    regAllowedCharacters: new RegExp(/[^A-Z|_|0-9]/g),

    nameIsValid$: function() {       
        if (this.name.length > 100) {
            return this.localize('formNameTooLongInvalidText');
        }

        if (this.regAllowedCharacters.test(this.properName)) {
            return this.localize('formNameInvalidText');
        }

        if (this.parentVM.id) {
            return true;
        }

        if (this.parentVM.existingFormNames.findRecord('name', this.properName, 0, false, false, true)) {
            return this.localize('nameInvalidText', this.properName);
        }

        return true;
    },

    when_name_changes_change_title_to_match: {
        on: ['nameChanged'],
        action: function() {
            if (!this.titleIsChanged) {
                this.set('title', this.properName);
            }
        }
    },
   when_title_changes_change_title_to_match: {
        on: ['titleChanged'],
        action: function() {            
            this.set('formTitle', Ext.util.Format.htmlEncode(this.title));           
        }
    },

    titleOnFocus: '',
    titleIsChanged: false,

    focusTitle: function (field) {
        this.titleOnFocus = field.target.value;
    },
    
    blurTitle: function (field) {
        this.titleIsChanged = this.titleIsChanged || this.titleOnFocus !== field.target.value;
    },

    formWindowTitle: '',

    /**
     * Validation formula to make sure that the title isn't a duplicate
     */
    titleIsValid$: function() {
        if (this.title.length > 500) {
            return this.localize('titleTooLongInvalidText');
        }

        return true;
    },
    originalWindowTitle: '',
    windowTitle$: function() {
        var title = this.properName;
        clientVM.setWindowTitle(title)
    },
    /**
     * Default label alignment for controls that are added to the form
     */
    labelAlign: 'left',
    /**
     * The dbTable to either create or update when the user saves the form
     */
    dbTable: '',
    dbTableIsValid$: function() {
        return (/^(?=[a-zA-Z])[a-zA-Z0-9_]{1,30}$/.test(this.dbTable)) || !this.dbTable ? true : this.localize('invalidDBTableName')
    },

    /**
     * Name of the wiki document to create with this form when the user saves it
     */
    wikiName: '',
    wikiNameIsValid$: function() {
        if (!this.wikiName) return true;
        return /[\w]+[.][\w]+/g.test(this.wikiName) ? true : this.localize('wikiNameNamespace');
    },

    showFieldSettings: 0,

    /**
     * Store to keep track of the valid value list of wiki names in the system
     */
    wikiNames: {
        mtype: 'store',
        model: 'RS.formbuilder.model.WikiName',
        proxy: {
            url: '/resolve/service/form/getwikinames',
            type: 'ajax',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },

    /**
     * List of controls that are on the form.  We use activator list here to make sure that when we have focus on a control we can display its detail view to edit the control
     */
    controls: {
        mtype: 'activatorlist',
        focusProperty: 'detailControl',
        autoParent: true
    },

    /**
     * List of buttons that are on the form.
     */
    formButtons: {
        mtype: 'list',
        autoParent: true
    },

    accessRights: {
        mtype: 'store',
        fields: ['name', 'value', {
            name: 'admin',
            type: 'bool'
        }, {
            name: 'edit',
            type: 'bool'
        }, {
            name: 'view',
            type: 'bool'
        }],
        proxy: {
            type: 'memory',
            reader: {
                type: 'json'
            }
        }
    },

    /**
     * Store for the buttons that displays as a tree grid with nested buttons->actions
     */
    treeStore: {
        mtype: 'treestore',
        mixins: [{
            type: 'listtreestoreadapter',
            attachTo: 'formButtons'
        }],
        fields: ['id', {
            name: 'operationName',
            type: 'string'
        }, {
            name: 'value',
            type: 'string'
        }, {
            name: 'type',
            type: 'string'
        }, {
            name: 'iconCls',
            type: 'string'
        }, {
            name: 'leaf',
            type: 'boolean'
        }],
        proxy: {
            type: 'memory',
            reader: {
                type: 'json'
            }
        },
        root: {
            expanded: true,
            operation: 'rootNode'
        }
    },

    /**
     * Store for the hidden fields that are on the form
     */
    hiddenFields: {
        mtype: 'store',
        fields: ['name', 'value'],
        data: []
    },

    useTabs: false,
    isWizard: false,

    tabPanel: {
        mtype: 'TabPanel'
    },

    when_useTabs_changes_add_or_remove_tabpanel: {
        on: ['useTabsChanged'],
        action: function() {
            if (this.useTabs) {
                if (this.controls.getAt(0) != this.tabPanel) {
                    this.tabPanel.clearControls();
                    if (this.tabPanel.tabs.length === 0) {
                        // REG-324 Add one tab to get the ball rolling
                        this.tabPanel.addTab();
                    }
                    this.tabPanel.set('selectedTab', this.tabPanel.tabs.getAt(0));
                    this.controls.insert(0, this.tabPanel);
                    this.tabPanel.clearControls();
                    this.tabPanel.syncCurrentTab();
                }
            } else {
                this.tabPanel.syncCurrentTab();
                var tab1HasControls = false,
                    tab2HasControls = false,
                    i;

                for (i = 0; i < this.tabPanel.tabs.length; i++) {
                    if (this.tabPanel.tabs.getAt(i).controls.length > 0) {
                        if (!tab1HasControls) {
                            tab1HasControls = true;
                        } else {
                            tab2HasControls = true;  
                        } 
                    }
                }

                if (tab1HasControls && tab2HasControls) {
                    this.confirmTabPanelRemoval();
                } else {
                    this.controls.removeAll();
                    //restore controls from the one tab that does have controls
                    for (i = 0; i < this.tabPanel.tabs.length; i++) {
                        if (this.tabPanel.tabs.getAt(i).controls.length > 0) {
                            Ext.each(this.tabPanel.tabs.getAt(i).controls, function(control) {
                                this.controls.add(control);
                            }, this);
                        }
                    }
                }
            }
        }
    },

    confirmTabPanelRemoval: function() {
        this.message({
            title: this.localize('confirmTabPanelRemoveTitle'),
            msg: this.localize('confirmTabPanelRemoveBody'),
            buttons: Ext.MessageBox.YESNOCANCEL,
            buttonText: {
                yes: this.localize('mergeTabPanelControls'),
                no: this.localize('removeTabPanelControls')
            },
            fn: this.confirmTabPanelRemovalResponse,
            scope: this
        });
    },

    confirmTabPanelRemovalResponse: function(btn) {
        if (btn === 'cancel') {
            this.set('useTabs', true);
        } else if (btn === 'yes') {
            this.mergeTabs();
            this.controls.remove(this.tabPanel);
        } else if (btn === 'no') {
            this.clearTabs();
            this.controls.remove(this.tabPanel);
        }        
    },

    mergeTabs: function() {
        this.tabPanel.syncCurrentTab();
        var i, j, tab, controls = [];

        for (i = 0; i < this.tabPanel.tabs.length; i++) {
            tab = this.tabPanel.tabs.getAt(i);
            Ext.each(tab.controls, function(control) {
                controls.push(control);
            });
            tab.controls = [];
        }

        this.controls.removeAll();
        Ext.each(controls, function(control) {
            this.controls.add(control);
        }, this);
    },
    clearTabs: function() {
        this.tabPanel.tabs.foreach(function(tab) {
            tab.controls = []
        });
        this.controls.removeAll();
    },

    addTab: function() {
        var tabSelection = this.tabsSelections[0],
            tabSelectionIndex = this.tabPanel.tabStore.indexOf(tabSelection);
        this.tabPanel.addTab({}, tabSelectionIndex);
        this.tabPanel.set('selectedTab', this.tabPanel.tabs.getAt(this.tabPanel.tabs.length - 1));
    },
    when_tabsSelections_change_select_newTab: {
        on: ['tabsSelectionsChanged'],
        action: function() {
            if (this.tabsSelections.length === 0 && this.tabPanel.tabStore.getCount() > 0) {
                this.set('tabsSelections', [this.tabPanel.tabStore.getAt(0)]);
            } else {
                var tabIndex = this.tabPanel.tabStore.indexOf(this.tabsSelections[0]);
                this.tabPanel.set('selectedTab', this.tabPanel.tabs.getAt(tabIndex));
            }
        }
    },
    tabsSelections: [],
    editTabIsEnabled$: function() {
        return this.tabsSelections.length == 1
    },
    editTab: function() {
        var tabSelection = this.tabsSelections[0],
            selectedTab, tabSelectionIndex = this.tabPanel.tabStore.indexOf(tabSelection);
        if (tabSelectionIndex > -1) {
            selectedTab = this.tabPanel.tabs.getAt(tabSelectionIndex);
            this.tabPanel.syncCurrentTab();
            this.tabPanel.set('selectedTab', selectedTab);
            this.open(Ext.applyIf({
                mtype: 'Tab'
            }, selectedTab.asObject()), 'detail');
        }
    },
    editRealTab: function(tabConfig) {
        var tabSelection = this.tabsSelections[0],
            tabSelectionIndex = this.tabPanel.tabStore.indexOf(tabSelection),
            selectedTab;

        if (tabSelectionIndex > -1) {
            selectedTab = this.tabPanel.tabs.getAt(tabSelectionIndex);
            selectedTab.loadData(tabConfig);
            selectedTab.parseDependencyControlConversionProperties(tabConfig.dependenciesJson);
        }
    },
    removeTabIsEnabled$: function() {
        return this.tabsSelections.length == 1;
    },
    removeTab: function() {
        var tabSelection = this.tabsSelections[0],
            selectedTab, tabSelectionIndex = this.tabPanel.tabStore.indexOf(tabSelection);

        if (tabSelectionIndex > -1) {
            selectedTab = this.tabPanel.tabs.getAt(tabSelectionIndex);
            this.tabPanel.syncCurrentTab();
            this.tabPanel.set('selectedTab', selectedTab);
            if (this.tabPanel.selectedTab.controls.length > 0) {
                this.confirmTabRemoval();
            } else {
                this.tabPanel.tabs.remove(this.tabPanel.selectedTab);
            }
        }
    },
    confirmTabRemoval: function() {
        this.message({
            title: this.localize('confirmTabRemoveTitle'),
            msg: this.localize('confirmTabRemoveBody'),
            buttons: Ext.MessageBox.YESNOCANCEL,
            buttonText: {
                yes: this.localize('mergeTabControls'),
                no: this.localize('removeTabControls')
            },
            fn: this.confirmTabRemovalResponse,
            scope: this
        });
    },

    confirmTabRemovalResponse: function(btn) {
        if (btn === 'yes') {
            this.mergeAndRemoveTab();
        } else if (btn === 'no') {
            this.noMergeRemoveTab();
        }
    },

    noMergeRemoveTab: function() {
        this.tabPanel.tabs.remove(this.tabPanel.selectedTab);
    },

    mergeAndRemoveTab: function() {
        var tab, tabIndex, i, oldSelectedTab = this.tabPanel.selectedTab;
        for (i = 0; i < this.tabPanel.tabs.length; i++) {
            if (this.tabPanel.tabs.getAt(i) != this.tabPanel.selectedTab) {
                tab = this.tabPanel.tabs.getAt(i);
                tabIndex = i;
            }
        }
        var controls = this.tabPanel.selectedTab.controls;
        this.tabPanel.set('selectedTab', tab);
        Ext.each(controls, function(control) {
            this.controls.add(control);
        }, this)

        this.tabPanel.tabs.remove(oldSelectedTab);
    },

    /**
     * Flag internally used to determine if the control should be re-selected because an operation is rebuilding it (bulk)
     */
    reselectControlIndex: -1,
    /**
     * Flag internally used to determine if the control should be re-selected because of an operation rebuilding it (bulk)
     */
    reselectControlCount: 0,

    /**
     * The currently selected control on the form or -1 if no selection
     */
    detailControl: -1,

    /**
     * Determines if the card that says 'please add a field' should be displayed
     */
    showNoFieldsCard$: function() {
        return this.controls.length == 0 ? 0 : 1
    },

    /**
     * Determines if the card that says 'please select a field' should be displayed
     */
    showSelectFieldCard$: function() {
        return this.detailControl === -1 || !Ext.isDefined(this.detailControl) ? 0 : 1
    },

    /**
     * Store for the different label alignment options available to the user (left, right, top)
     */
    labelAlignOptionsList: {
        mtype: 'store',
        fields: ['text', 'value'],
        data: [{
            text: 'Left',
            value: 'left'
        }, {
            text: 'Right',
            value: 'right'
        }, {
            text: 'Top',
            value: 'top'
        }]
    },

    /**
     * Store of the custom tables currently in the system that the user can edit or link this form to
     */
    customTablesList: {
        mtype: 'store',
        model: 'RS.formbuilder.model.CustomTableDTO',
        data: [],
        proxy: {
            type: 'ajax',
            url: '/resolve/service/form/getcustomtables',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },

    /**
     * Store of the selected custom table's columns that the user can pick from to link the form to
     */
    columnStore: {
        mtype: 'store',
        fields: ['name', 'dbcolumnId', 'displayName', 'dbtype', 'uiType', 'integerMinValue', 'integerMaxValue', 'decimalMinValue', 'decimalMaxValue', 'stringMinLength', 'stringMaxLength', 'uiStringMaxLength', 'selectValues', 'choiceValues', 'referenceTable', 'referenceDisplayColumn'],
        data: [],
        proxy: {
            type: 'ajax',
            url: '/resolve/service/form/getcustomtablecolumns',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },
    /**
     * Store to display the runbooks for the user to choose from
     */
    runbookStore: {
        mtype: 'store',
        fields: ['name', 'value'],
        proxy: {
            type: 'ajax',
            url: '/resolve/service/form/getrunbooks',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },
    /**
     * Store to display the scripts for the user to choose from
     */
    scriptStore: {
        mtype: 'store',
        fields: ['name', 'value'],
        proxy: {
            type: 'ajax',
            url: '/resolve/service/form/getscripts',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },

    actionTaskStore: {
        mtype: 'store',
        fields: ['name', 'value'],
        proxy: {
            type: 'ajax',
            url: '/resolve/service/form/getactiontasks',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },

    /**
     * Reactor for when the dbTable choice is changed, then we need to reload the columns store because the columns won't be correct anymore with the new table
     */
    when_dbtable_changes_reload_store_or_fix_name: {
        on: ['dbTableChanged'],
        action: function() {
            var val = this.dbTable,
                found = false;

            this.customTablesList.each(function(table) {
                if (table.data.umodelName == val) {
                    found = true;
                    return false;
                }
            });

            if (found) {
                this.columnStore.load({
                    callback: this.populateModelType,
                    scope: this
                });
            } else {
                this.columnStore.removeAll();
            }
        }
    },

    populateModelType: function(records, operation, success) {
        if (this.detailControl >= 0 || Ext.isObject(this.detailControl)) {
            this.columnStore.insert(0, {
                name: 'newColumn',
                displayName: this.localize('newColumn')
            });
            var self = this;
            this.columnStore.each(function(record) {
                record.data.modelType = function() {
                    if (self.detailControl.uiType === 'SectionContainerField') {
                        return self.detailControl.selectedColumn.detailControl.dbColumnType;
                    }

                    return self.detailControl.dbColumnType;
                }
            }, this)
            this.columnStore.fireEvent('refresh', this.columnStore);
        } else {
            this.columnStore.insert(0, {
                name: 'newColumn',
                displayName: this.localize('newColumn')
            });
        }
        this.fireEvent('afterColumnStoreLoaded');
    },

    init: function() {
        //Make sure the tabPanel is initialized to get translated tabs and at least one tab on the tabPanel
        this.tabPanel.init();

        this.labelAlignOptionsList.each(function(option) {
            option.set('text', this.localize(option.get('text')));
        }, this);

        //this is the workaround for extjs 4.2.0 bugs:http://www.sencha.com/forum/showthread.php?256359-Combobox-with-autoLoad-store-applies-Filter
        this.customTablesList.on('beforeload', function() {
            this.customTablesList.clearFilter();
        }, this);

        //Load the custom tables from the server
        this.customTablesList.load({
            scope: this,
            callback: this.customTablesLoaded
        });

        this.columnStore.on('beforeload', function(store, operation, eOpts) {
            operation.params = operation.params || {};
            Ext.apply(operation.params, {
                tableName: this.dbTable
            });
        }, this);

        this.runbookStore.on('load', function(store, records) {
            store.insert(0, {
                name: this.localize('currentWiki'),
                value: '${CURRENT_WIKI}'
            })
        }, this)

        //Don't need to load this because its not used anywhere now
        this.runbookStore.load();
        this.scriptStore.load();
        this.actionTaskStore.load();
        this.wikiNames.load();

        this.set('name', this.localize('UNTITLEDFORM'));
        this.set('formTitle', this.localize('UntitledForm'));

        if (this.dbTable) this.columnStore.load();
        this.formButtons.on('insertchild', this.rootVM.enforcingSIRContext.bind(this.rootVM));
        this.formButtons.on('editedchild', this.rootVM.enforcingSIRContext.bind(this.rootVM));
        this.formButtons.on('removechild', this.rootVM.enforcingSIRContext.bind(this.rootVM)); 
        this.formButtons.on('buttonremoved', this.rootVM.enforcingSIRContext.bind(this.rootVM)); 


    },

    customTablesLoaded: function() {
        // debugger;
        // this.customTablesDoneLoading = true;
        if (this.dbTable) {
            this.columnStore.load();
        }
    },

    /**
     * The initial control length.  Primarily used to determine dirty-ness from an edit of the form
     */
    initialControlLength: 0,

    /**
     * Handler for the user dropping a control on the form.  Adds the control if a button was dropped, or reorders the fields appropriately
     * @param {Object} dropIndex
     * @param {Object} sourceIndex
     * @param {Object} controlConfig
     */
    dropControl: function(dropIndex, sourceIndex, controlConfig) {
        if (dropIndex >= 0 && sourceIndex >= 0) {
            this.controls.insert(sourceIndex < dropIndex ? dropIndex - 1 : dropIndex, this.controls.removeAt(sourceIndex));
        } else if (dropIndex >= 0 && controlConfig) {
            this.insertControl(dropIndex, controlConfig);
        }
    },

    /**
     * Handler that changes the fieldLabel of the selected control
     * @param {Object} newFieldLabelValue
     */
    editFieldLabel: function(newFieldLabelValue) {
        if (this.detailControl) {
            this.detailControl.set('fieldLabel', newFieldLabelValue);
        }
    },

    /**
     * Handler that changes the title for the form
     * @param {Object} newTitle
     */
    editTitle: function(newTitle) {
        this.set('title', Ext.util.Format.htmlEncode(newTitle));
    },

    /**
     * Inserts a control at the provided index
     * @param {Object} index
     * @param {Object} controlConfig
     */
    insertControl: function(index, controlConfig) {
        var control;

        if (Ext.isString(controlConfig)) {
            control = this.model({
                mtype: controlConfig,
                labelAlign: this.labelAlign
            });
        } else {
            control = this.model(controlConfig);
        }

        //Disable the file upload button if this is a file upload field
        if (control.viewmodelName === 'FileUploadField') {
            this.rootVM.set('fileUploadIsEnabled', false);
        }

        this.controls.insert(index, control);
        this.selectControl(control);
        return control;
    },
    /**
     * Adds a control to the end of the form
     * @param {Object} controlConfig
     */
    addControl: function(controlConfig) {
        var index = this.controls.indexOf(this.detailControl);
        
        if (index > -1) {
            index++;
        } else {
            index = this.controls.length;
        }

        this.insertControl(index, controlConfig);
    },
    addRealControl: function(control) {
        this.controls.insert(this.controls.length, control);
    },
    /**
     * Removes the control from the form
     * @param {Object} ctrl
     */
    removeControl: function(ctrl) {
        var currentIndex = this.controls.indexOf(ctrl);
        
        if (ctrl.viewmodelName === 'FileUploadField') {
            this.rootVM.set('fileUploadIsEnabled', true);
        }

        if (ctrl.viewmodelName === 'TabContainerField') {
            ctrl.referenceTables.foreach(function(table) {
                if (table.rsType === 'FileManager') { 
                    this.rootVM.set('fileUploadIsEnabled', true);
                }
            });
        }

        if (ctrl.viewmodelName === 'SectionContainerField') {
            ctrl.columns.each(function(c) {
                c.controls.each(function(subCtrl) {
                    if (subCtrl.viewmodelName === 'FileUploadField') {
                        this.rootVM.set('fileUploadIsEnabled', true);
                    }
                })
            });
        }

        this.controls.removeAt(currentIndex);
    },
    /**
     * Duplicates the provided control and puts the copy below the existing control
     * @param {Object} ctrl
     */
    duplicateControl: function(ctrl) {
        this.insertControl(this.controls.indexOf(ctrl) + 1, this.getCopy(ctrl));
    },
    /**
     * Rebuilds the control in the event that something has changed that cannot be auto-magically changed due to limitations in the extjs code
     * @param {Object} ctrl
     */
    duplicateAndRemoveControl: function(ctrl) {
        this.duplicateControl(ctrl);
        this.removeControl(ctrl);
    },
    /**
     * Rebuilds the control in the event that something has changed that cannot be auto-magically changed due to limitations in the extjs code
     * @param {Object} ctrl
     */
    rebuildControl: function(ctrl, reselectControl) {
        var me = this;
        var currentIndex = this.controls.indexOf(ctrl);
        var control = this.controls.getAt(currentIndex);
        
        if (control) {
            //Delay to make sure all internal event (ExtJs Events) of this field is complete before remove it.
            setTimeout(function(){
                me.controls.removeAt(currentIndex);
                me.controls.insert(currentIndex, control);
                var isReselect = me.reselectControlIndex === currentIndex || me.reselectControlCount <= 0;
                if (currentIndex !== -1 && isReselect && reselectControl) {
                    me.selectControl(me.controls.getAt(currentIndex));
                    me.reselectControlCount--;
                }
            },50);           
        }
    },
    /**
     * Returns the copied control for duplication purposes
     * @param {Object} ctrl
     */
    getCopy: function(ctrl) {
        var formcontrol = RS.formbuilder.viewmodels.FormControl;
        var config = {};
        this.copyTemplate(formcontrol, ctrl, config);
        //TODO: add additional templates here as they become available
        config.mtype = ctrl.viewmodelName;
        return config;
    },
    /**
     * Copies the control key by key
     * @param {Object} viewmodel
     * @param {Object} ctrl
     * @param {Object} config
     */
    copyTemplate: function(viewmodel, ctrl, config) {
        var key;
        for (key in viewmodel) {
            if (!Ext.isFunction(ctrl[key]) && !Ext.isObject(ctrl[key])) {
                config[key] = Ext.clone(ctrl[key]);
            }
        }
    },
    /**
     * Programatically selects the provided control
     * @param {Object} ctrl
     */
    selectControl: function(ctrl) {
        this.set('detailControl', ctrl);
    },

    when_detailControl_changes_ensure_field_settings_are_shown: {
        on: ['detailControlChanged'],
        action: function() {
            this.set('showFieldSettings', 1);

            if (this.detailControl) {
                this.columnStore.each(function(record) {
                    var self = this;
                    record.data.modelType = function() {
                        var type = self.detailControl.dbColumnType;

                        if (self.detailControl.uiType === 'SectionContainerField') {
                            type = self.detailControl.selectedColumn.detailControl.dbColumnType;
                        }

                        return type;
                    }
                }, this);
            }
        }
    },
    /**
     * Determines if the form is valid
     */
    isValid$: function() {
        return true;
    },
    /**
     * Determines if the form is dirty
     */
    // isDirty$: function() {
    //  return this.controls.length != this.initialControlLength;
    // },
    /**
     * Reactor for when the label alignment changes.  We have to go through and change the existing label alignments on the already existing controls in the form
     */
    whenLabelAlignChanges: {
        on: ['labelAlignChanged'],
        action: function() {
            this.reselectControlIndex = this.controls.indexOf(this.detailControl);
            this.reselectControlCount = this.controls.length;
            this.controls.foreach(function(control) {
                control.set('labelAlign', this.labelAlign);
            }, this);
        }
    },
    /**
     * Array of the currently selected hidden field on the grid
     */
    hiddenFieldsSelections: [],
    /**
     * Determines whether the remove button for the hidden fields is enabled because a selection has been made on the hidden fields
     */
    removeHiddenFieldIsEnabled: {
        on: ['hiddenFieldsSelectionsChanged'],
        formula: function() {
            return this.hiddenFieldsSelections.length > 0;
        }
    },
    /**
     * Adds a hidden field to the form
     */
    addHiddenField: function() {
        this.hiddenFields.add({
            name: this.localize('Untitled'),
            value: ''
        });
    },
    /**
     * Removes the selected hidden field from the form
     */
    removeHiddenField: function() {
        this.hiddenFields.remove(this.hiddenFieldsSelections);
    },

    actionItemDoubleClick: function(record, item, index, e, eOpts) {
        if (record.isLeaf()) {
            this.editAction();
        } else {
            this.editButton();
        }
    },
    /**
     * Adds a button to the form
     */
    addButton: function(type) {
        var buttonIndex = this.getSelectedButtonIndex();
        
        if (buttonIndex === -1) {
            buttonIndex = this.formButtons.length;
        } else {
            buttonIndex++;
        }

        this.formButtons.insert(buttonIndex, this.model({
            mtype: 'ButtonControl',
            type: Ext.isString(type) ? type : 'button'
        }));

        this.set('formButtonsSelections', [this.treeStore.getRootNode().getChildAt(buttonIndex)]);
    },
    addNewButton: function() {
        this.addButton();
    },
    addSeparator: function() {
        this.addButton('separator')
    },
    addSpacer: function() {
        this.addButton('spacer')
    },
    addRightJustify: function() {
        this.addButton('rightJustify')
    },
    editButton: function() {
        var buttonIndex = this.getSelectedButtonIndex();
        var button = this.formButtons.getAt(buttonIndex);
        this.open(Ext.applyIf({
            mtype: 'ButtonControl',
            type: button.type
        }, button.asObject()), 'detail');
    },
    editRealButton: function(buttonProperties) {
        var buttonIndex = this.getSelectedButtonIndex(),
            button = this.formButtons.getAt(buttonIndex);
        button.loadData(buttonProperties);
        button.parseDependencyControlConversionProperties(buttonProperties.dependenciesJson);
        this.formButtons.fireEvent('edited', button, buttonIndex);
    },
    editButtonIsEnabled$: function() {
        return this.buttonSelected;
    },
    /**
     * Deletes the selected button from the form
     */
    removeButton: function() {
        this.formButtons.removeAt(this.treeStore.getRootNode().indexOf(this.formButtonsSelections[0]));
        this.formButtons.fireEvent('buttonremoved');
        this.set('formButtonsSelections', []);
    },

    /**
     * Array of the currently selected button/action in the treegrid
     */
    formButtonsSelections: [],
    /**
     * Determines if a button is currently selected
     */
    buttonSelected$: function() {
        return this.formButtonsSelections.length == 1 && this.formButtonsSelections[0].data.type != 'action';
    },
    /**
     * Determines if the delete button should be enabled
     */
    removeButtonIsEnabled$: function() {
        return this.buttonSelected;
    },
    /**
     * Determines if the add action button should be enabled
     */
    addActionIsEnabled$: function() {
        return this.formButtonsSelections.length == 1 && this.formButtonsSelections[0].data.type != 'rightJustify' && this.formButtonsSelections[0].data.type != 'separator' && this.formButtonsSelections[0].data.type != 'spacer';
    },
    /**
     * Determines if an action in a button is currently selected
     */
    actionSelected$: function() {
        return this.formButtonsSelections.length == 1 && this.formButtonsSelections[0].data.type == 'action';
    },
    /**
     * Determines if the edit action button should be enabled
     */
    editActionIsEnabled$: function() {
        return this.actionSelected;
    },
    /**
     * Determines if the delete action button should be enabled
     */
    removeActionIsEnabled$: function() {
        return this.actionSelected;
    },
    getSelectedButtonIndex: function() {
        return this.getButtonIndex(this.formButtonsSelections[0]);
    },
    getButtonIndex: function(node) {
        if (!node) return -1;
        if (this.formButtonsSelections.length == 1 && node.data.type == 'action') node = node.parentNode;
        var selectionIndex = node.getOwnerTree().getRootNode().indexOf(node);
        return selectionIndex;
    },
    /**
     * Displays a dialog to select the action properties before adding the action
     */
    addAction: function() {
        //determine if there are any db actions already to set on the action
        var buttonIndex = this.getSelectedButtonIndex(),
            alreadyHasDBOperation = false;
        this.formButtons.getAt(buttonIndex).actions.foreach(function(action) {
            if (action.operation.toLowerCase() == 'db') alreadyHasDBOperation = true;
        });
        this.open({
            mtype: 'Action',
            parentVM: this,
            alreadyHasDBOperation: alreadyHasDBOperation,
            id: Ext.id()
        });
    },
    /**
     * Actually adds the configured action to the selected button (unlike addAction which opens the window)
     * @param {Object} action
     */
    addRealAction: function(action) {
        var selection = this.formButtonsSelections[0],
            parent = selection.parentNode,
            childIndex = parent.indexOf(selection),
            buttonIndex = this.getSelectedButtonIndex(),
            insertIndex = childIndex + 1;

        action.unParent();
        
        if (selection.data.type !== 'action') {
            insertIndex = 0;
        }

        this.formButtons.getAt(buttonIndex).actions.insert(insertIndex, action);
        this.formButtons.fireEvent('insertchild', action, buttonIndex, insertIndex);
        this.ensureHiddenActionFields(action);
        this.set('formButtonsSelections', [this.treeStore.getRootNode().getChildAt(buttonIndex).getChildAt(insertIndex)]);
    },
    ensureHiddenActionFields: function(action) {
        //If the action is an runbook or event, make sure the optional hidden fields are automatically added to the hidden fields if they don't already exist
        if (action.operation === 'EVENT') {
            var hiddenParams = ['REFERENCE', 'ALERTID', 'CORRELATIONID', 'EVENT_REFERENCE'];

            for (var i = 0; i < hiddenParams.length; i++) {
                var found = false;

                this.hiddenFields.each(function(hidden) {
                    if (hidden.data.name === hiddenParams[i]) {
                        found = true;
                        return false;
                    }
                })

                if (!found) this.hiddenFields.add({
                    name: hiddenParams[i],
                    value: ''
                });                
            }
        }
    },
    /**
     * Displays a dialog to edit the selected action properties
     */
    editAction: function() {
        var selection = this.formButtonsSelections[0],
            parent = selection.parentNode,
            childIndex = parent.indexOf(selection),
            parentIndex = this.getSelectedButtonIndex(),
            action = this.formButtons.getAt(parentIndex).actions.getAt(childIndex),
            alreadyHasDBOperation = false;
        this.formButtons.getAt(parentIndex).actions.foreach(function(a) {
            if (a != action && a.operation.toLowerCase() == 'db') alreadyHasDBOperation = true;
        });
        this.open(Ext.applyIf({
            mtype: 'Action',
            isNew: false,
            alreadyHasDBOperation: alreadyHasDBOperation
        }, action.asObject()));
    },
    /**
     * Actually edits the selected action (unlike applyAction which opens the window)
     * @param {Object} actionProperties
     */
    editRealAction: function(actionProperties) {
        var selection = this.formButtonsSelections[0],
            parent = selection.parentNode,
            childIndex = parent.indexOf(selection),
            parentIndex = this.getSelectedButtonIndex(),
            action = this.formButtons.getAt(parentIndex).actions.getAt(childIndex);
        action.loadData(actionProperties);
        this.formButtons.fireEvent('editedchild', parentIndex, selection, action);
        this.set('formButtonsSelections', []);
        this.ensureHiddenActionFields(actionProperties);
    },
    /**
     * Deletes the currently selected action from the button
     * @param {Object} action
     */
    removeAction: function(action) {
        var selection = this.formButtonsSelections[0],
            parent = selection.parentNode,
            childIndex = parent.indexOf(selection),
            parentIndex = this.getSelectedButtonIndex();
        this.formButtons.getAt(parentIndex).actions.removeAt(childIndex);
        this.formButtons.fireEvent('removechild', parentIndex, selection);
        this.set('formButtonsSelections', []);
    },

    accessRightsSelections: [],
    addAccessRight: function() {
        var rights = [];
        this.accessRights.each(function(right) {
            rights.push(right.data);
        });
        this.open({
            mtype: 'AccessRightsPicker',
            currentRights: rights
        });
    },
    
    addRealAccessRights: function(accessRights) {
        Ext.each(accessRights, function(accessRight) {
            this.accessRights.add(Ext.applyIf(accessRight.data, {
                view: true,
                edit: true,
                admin: true
            }));
        }, this);
    },
    
    removeAccessRightIsEnabled: {
        on: ['accessRightsSelectionsChanged'],
        formula: function() {
            return this.accessRightsSelections.length > 0;
        }
    },
    
    removeAccessRight: function() {
        for (var i = 0; i < this.accessRightsSelections.length; i++) {
            this.accessRights.remove(this.accessRightsSelections[i]);
        }
    },
    
    dropButton: function(node, data, overModel, dropPosition, eOpts) {
        // console.log('dropPosition is: ' + dropPosition + ' dropnode is ' + data.records[0].data.operationName + ' overnode is ' + overModel.data.operationName);
        var rootNode = this.treeStore.getRootNode(), buttonConfigs = [];

        //First, calculate the order that the form buttons should be in based on the tree's new order after the drop of the button
        rootNode.eachChild(function(child) {
            var index = this.getButtonIndexFromNode(child);
            if (index > -1) buttonConfigs.push(this.formButtons.getAt(index).getButtonControlConversionProperties());
        }, this);

        //then remove all the buttons from the form
        this.formButtons.removeAll();

        //then deserialize all the buttons which will re-add them in the proper order
        for (var i = 1; i < buttonConfigs.length; i++) {			
			buttonConfigs[i].orderNumber = i;
            this.parentVM.deserializeButton(buttonConfigs[i]);
        }
        //Make sure to clear out any existing formButtonsSelections that might have been there
        this.set('formButtonsSelections', []);
    },

    dropAction: function(node, data, overModel, dropPosition, eOpts) {
        // console.log('dropPosition is: ' + dropPosition + ' dropnode is ' + data.records[0].data.operationName + ' overnode is ' + overModel.data.operationName);
        //cases:
        //1. Dropping an action onto a new button
        //2. Dropping an action before or after an existing action
        var dragActionIndex = data.records[0].parentNode.indexOf(data.records[0]),
            overButtonIndex = this.getButtonIndex(overModel),
            dragButtonIndex = this.getButtonIndex(data.records[0]),
            overButton = this.formButtons.getAt(overButtonIndex),
            dragButton = this.formButtons.getAt(dragButtonIndex),
            overActionIndex = overModel.data.type == 'action' ? overModel.parentNode.indexOf(overModel) : -1,
            action = dragButton.actions.removeAt(dragActionIndex);

        action.unParent();

        if (dropPosition == 'append') overButton.actions.add(action);
        else overButton.actions.insert(overActionIndex, action);
    },
   
    getButtonIndexFromNode: function(node) {
        var id = node.internalId;
        var n = this.formButtons._private.objs.length;

        for (var i = 0; i < n; i++) {
            if (this.formButtons._private.objs[i].id === id) {
                return i;
            }
        }

        return -1;
    }
});