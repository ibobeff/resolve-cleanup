/**
 * Model definition for the Main entry point to the form builder
 * Main is the launch point for the form builder and contains the form that is going to be built.  It also displays the detail information
 * for the currently selected control and shows information when there are no fields and no selected fields
 */
glu.defModel('RS.formbuilder.Main', {
	create: false,
	atId: '',
	rbId: '',

	initialState: null,
	activate: function(screen, params) {
		//Change the window title to the current form name
		this.mainForm.windowTitle$()
		this.rootVM.set('fileUploadIsEnabled', true);
		// if (params && params.id) this.set('id', params.id)
		// if (params && params.name) this.set('name', params.name)
		// if (params && params.formName) this.set('formName', params.formName)
		// if (params && params.view) this.set('formName', params.view)
		this.set('id', params && params.id ? params.id : '')
			// this.set('formName', params && params.formName ? params.formName : '')
			// this.set('formName', params && params.view ? params.view : '')
		this.set('formName', params && (params.name || params.formName || params.view) ? params.name || params.formName || params.view : '')

		if (params && params.create === 'true') this.set('create', true)
		this.set('atId', params.atId || '')
		this.set('rbId', params.rbId || '')
		this.mainForm.customTablesList.load({
			scope: this.mainForm,
			callback: this.mainForm.customTablesLoaded
		});
		this.existingFormNames.load();
		this.sirContext = false;
		this.loadForm();
	},
	id: '',
	tabPanelId: '',

	/**
	 *The id of the form to load if one is provided (edit mode)
	 */
	formId: '',

	/**
	 *The name of the form to load if one is provided (edit mode)
	 */
	formName: '',

	/**
	 *Tells the editor whether the user is an admin or not.  Non-admin cannot create tables/columns and must choose from exiting db structures
	 */
	isAdmin: true,

	/**
	 *Name of the wiki document to pass through to the form to know which wiki document to link to, or which wiki document to create
	 */
	wikiName: '',

	/**
	 *List of controls that are available to the user to add to the form
	 */
	availableControls: {
		mtype: 'list',
		autoParent: true
	},

	/**
	 *List of the form names that are currently in the system
	 */
	existingFormNames: {
		mtype: 'store',
		fields: ['name'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getformnames',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	/**
	 *Instance of the main form that we are building
	 */
	mainForm: {
		mtype: 'Form'
	},

	back: function() {
		var current = this.convertToRSForm();
		if (Ext.encode(current).replace(/"[^:]+"\:(""|null),/g, '') != Ext.encode(this.initialState).replace(/"[^:]+"\:(""|null),/g, ''))
			this.message({
				title: this.localize('leaveWithoutSaveTitle'),
				msg: this.localize('leaveWithoutSave'),
				button: Ext.MessageBox.YESNO,
				buttonText: {
					'yes': this.localize('leave'),
					'no': this.localize('cancel')
				},
				scope: this,
				fn: function(btn) {
					if (btn == 'no')
						return;
					clientVM.handleNavigation({
						modelName: 'RS.formbuilder.Forms'
					});
				}
			});
		else
			clientVM.handleNavigation({
				modelName: 'RS.formbuilder.Forms'
			});
	},

	sirContext: false,
	unSupportedFieldsMap: {
		FileUploadField: 'File Upload',
		JournalField: 'Journal',
		SequenceField: 'Sequence',
		ReferenceField: 'Reference',
		UserPickerField: 'User Picker',
		TeamPickerField: 'Team Picker',
		ReferenceTableField: 'Reference Table',
		TabContainerField: 'Tab Container',
		SectionContainerField: 'Section'
	},

	enforcingSIRContext: function() {
		this.sirContext = false;
		this.mainForm.formButtons.forEach(function(btn) {
			btn.actions.forEach(function(action) {
				if (action.operationName === 'SIR'&& action.sirAction === 'saveCaseData') {
					this.sirContext = true;
				}
			}, this);
		}, this);
		this.availableControls.foreach(function(availableControl) {
			switch (availableControl.controlType) {
				case 'FileUploadField':
				case 'JournalField':
				case 'SequenceField':
				case 'ReferenceField':
				case 'UserPickerField':
				case 'TeamPickerField':
				case 'ReferenceTableField':
				case 'TabContainerField':
				case 'SectionContainerField':
					availableControl.set('isDisabled', this.sirContext);
					break;
				default:
					break;
			}
		}, this);
		if (this.sirContext) {
			this.validateSIRFields();
		} else {
			this.mainForm.controls.foreach(function(c) {
				var fieldName = c.viewmodelName;
				if (this.unSupportedFieldsMap[fieldName]) {
					c.set('unsupportedField', false);
				}
			
			}, this);
		}
	},

	resetFormControls: function() {
		this.availableControls.foreach(function(availableControl) {
			availableControl.set('isDisabled', false);
		}, this);
	},

	init: function() {
		//Initialize the available controls from the namespace
		var model;
		for (model in RS.formbuilder.viewmodels) {
			if (model.indexOf('Field') > -1 && model != 'HiddenField' && model != 'ButtonField') this.availableControls.add(this.model({
				mtype: 'viewmodel',
				name: this.localize(model),
				controlType: model,
				isDisabled: false
			}))
		}

		if (this.name) this.set('formName', this.name)
		if (this.view) this.set('formName', this.view)

		// if (this.id || this.formId || this.formName) {
		// 	//Load the form data
		// 	this.loadForm();
		// } else {
		// 	Ext.Ajax.request({
		// 		url: '/resolve/service/form/getDefaultAccessRights',
		// 		scope: this,
		// 		success: this.loadDefaultRights,
		// 		failure: function(r) {
		// 			Ext.Msg.alert('Error', 'An unknown error has occurred.');
		// 		}
		// 	});
		// }

		this.existingFormNames.load();

		//initialize the main form to get the translated strings
		this.mainForm.set('wikiName', this.wikiName);
		this.mainForm.init();

		// generate page token for actiontask/editorstub.jsp
		clientVM.getCSRFToken_ForURI('/resolve/customtable/customtable.jsp');
	},

	loadForm: function() {
		if (this.id || this.formId || this.formName) {
			//Display loading message
			Ext.MessageBox.show({
				msg: this.localize('loadingMessage'),
				progressText: this.localize('loadingProgress'),
				width: 300,
				wait: true,
				waitConfig: {
					interval: 200
				}
				//icon: 'ext-mb-save'
			});

			this.ajax({
				url: '/resolve/service/form/getformfb',
				params: {
					view: this.formName,
					formsysid: this.id || this.formId
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) {
						if (response.data) {
							this.convertFromRSForm(response.data);
							this.mainForm.on('afterColumnStoreLoaded', function() {
								this.set('initialState', this.convertToRSForm());
								return 'discard';
							}, this);
							this.enforcingSIRContext();
						} else clientVM.displayError(this.localize('noDataFromServer'))
					} else {
						Ext.MessageBox.hide()
						if (this.create) {
							this.mainForm.set('name', this.formName)
								//Pull in the default access rights
							this.ajax({
								url: '/resolve/service/form/getDefaultAccessRights',
								success: function(resp) {
									this.loadDefaultRights(resp)
								},
								failure: function(resp) {
									clientVM.displayFailure(resp);
								}
							})

							//If there is a runbook or action task, then load them and pre-populate the form
							if (this.atId) {
								this.ajax({
									url: '/resolve/service/actiontask/get',
									params: {
										id: this.atId,
										name: ''
									},
									success: function(r) {
										var response = RS.common.parsePayload(r)
										if (response.success) {
											//Add the fields for all the inputs from the action task
											if (response.data && response.data.resolveActionInvoc && Ext.isArray(response.data.resolveActionInvoc.resolveActionParameters)) {
												Ext.Array.forEach(response.data.resolveActionInvoc.resolveActionParameters, function(param) {
													if (param.utype.toLowerCase() == 'input') {
														//Add a string field with the information provided
														this.addControl({
															mtype: 'RS.formbuilder.TextField',
															defaultValue: param.udefaultValue,
															encrypted: param.uencrypt,
															mandatory: param.urequired,
															fieldLabel: param.uname,
															inputName: param.uname
														})
													}
												}, this)
											}
											//Add in the execute button to the form with this action task as a step
											this.mainForm.addButton()
											this.mainForm.editRealButton({
												id: this.mainForm.formButtons.getAt(0).id,
												operation: this.localize('execute'),
												operationName: this.localize('execute'),
												dependenciesJson: {}
											})
											this.mainForm.addRealAction(this.mainForm.model({
												mtype: 'RS.formbuilder.Action',
												operation: 'ACTIONTASK',
												PROBLEMID: 'ACTIVE',
												ACTIONTASK: response.data.ufullName
											}))
										} else
											clientVM.displayError(response.message)
									},
									failure: function(r) {
										clientVM.displayFailure(r);
									}
								})
							}
							if (this.rbId) {
								this.ajax({
									url: '/resolve/service/wiki/get',
									params: {
										id: this.rbId,
										name: ''
									},
									success: function(r) {
										var response = RS.common.parsePayload(r)
										if (response.success) {
											try {
												var params = Ext.decode(response.data.uwikiParameters || '[]'),
													mtype;
												//sort by order of the params
												Ext.Array.sort(params, function(a, b) {
													if (a.order < b.order) return -1
													if (a.order > b.order) return 1
													return 0
												})

												// Add the fields for all the inputs from the runbook
												Ext.Array.forEach(params, function(param) {
													switch (param.type) {
														case 'int':
															mtype = 'RS.formbuilder.NumberField'
															break;
														case 'decimal':
															mtype = 'RS.formbuilder.DecimalField'
															break;
														case 'date':
															mtype = 'RS.formbuilder.DateField'
															break;
														case 'string':
														default:
															mtype = 'RS.formbuilder.TextField'
															break;
													}

													//Add a field with the information provided
													this.addControl({
														mtype: mtype,
														defaultValue: param.defaultValue,
														fieldLabel: param.name,
														inputName: param.name
													})
												}, this)
											} catch (e) {
												//Don't worry about this because there could be bad params data from the server
											}

											//Add in the execute button to the form with this action task as a step
											this.mainForm.addButton()
											this.mainForm.editRealButton({
												id: this.mainForm.formButtons.getAt(0).id,
												operation: this.localize('execute'),
												operationName: this.localize('execute'),
												dependenciesJson: {}
											})
											this.mainForm.addRealAction(this.mainForm.model({
												mtype: 'RS.formbuilder.Action',
												operation: 'RUNBOOK',
												PROBLEMID: 'ACTIVE',
												RUNBOOK: response.data.ufullname
											}))
										} else
											clientVM.displayError(response.message)
									},
									failure: function(r) {
										clientVM.displayFailure(r);
									}
								})
							}
						} else {
							clientVM.displayError(response.message)
						}
					}
				},
				failure: function(r) {
					Ext.MessageBox.hide()
					clientVM.displayFailure(r);
				}
			});
		} else {
			this.resetFormControls();
			this.clearExistingForm({
				id: '',
				panels: [{
					id: '',
					tabs: [{
						id: ''
					}]
				}],
				wizard: false,
				formName: this.localize('UNTITLEDFORM'),
				displayName: this.localize('UntitledForm'),
				tableName: ''
			})

			this.ajax({
				url: '/resolve/service/form/getDefaultAccessRights',
				success: function(resp) {
					this.loadDefaultRights(resp);
					this.set('initialState', this.convertToRSForm());
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			});
		}
	},

	loadDefaultRights: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			this.processAccessRights({
				viewRoles: response.data.read,
				editRoles: response.data.edit,
				adminRoles: response.data.admin
			});
		}
	},

	/**
	 *Button Handler for previewing the form
	 */
	preview: function() {
		var form = this.convertToRSForm();
		Ext.create('Ext.window.Window', {
			title: this.localize('preview'),
			modal: true,
			border: false,
			listeners: {
				beforeshow: function() {
					clientVM.setPopupSizeToEightyPercent(this);
				}
			},
			layout: 'fit',
			padding : 15,
			cls : 'rs-modal-popup',
			items: [Ext.create('RS.formbuilder.viewer.desktop.RSForm', {
				formConfiguration: form,
			})]
		}).show();
	},

	/**
	 *Button handler to load the real version of the form stored on the database
	 */
	live: function() {
		Ext.create('Ext.window.Window', {
			title: this.localize('live'),
			modal: true,
			border: false,
			width: 600,
			height: 400,
			layout: 'fit',
			items: {
				xtype: 'rsform'
			}
		}).show();
	},

	refreshForm: function() {
		Ext.MessageBox.show({
			title: this.localize('refreshFormTitle'),
			msg: this.localize('refreshFormBody'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('discardChanges'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.confirmRefreshForm
		});
	},

	confirmRefreshForm: function(btn) {
		if (btn === 'yes') this.loadForm();
	},

	/**
	 *Determines if the save button should be enabled because the form is dirty and valid
	 */
	saveFormIsEnabled$: function() {
		return this.mainForm.isValid // && this.mainForm.isDirty;
	},

	/**
	 *Adds a control to the form
	 */
	addControl: function(controlConfig) {
		this.mainForm.addControl(controlConfig);
	},
	/**
	 *Makes sure the control is valid, if its not it displays a message
	 */
	validateControl: function(control, tab) {
		if (control.isValid === false) {
			if (tab) this.mainForm.tabPanel.set('selectedTab', tab);
			control.selectControl();
			Ext.Msg.alert(this.localize('validationErrorTitle'), this.localize('validationErrorBody', [control.fieldLabel]));
			return false;
		}
		if (control.inputType == 'INPUT' && control.inputName == this.localize('Untitled')) {
			if (tab) this.mainForm.tabPanel.set('selectedTab', tab);
			control.selectControl();
			Ext.Msg.alert(this.localize('untitledFieldNameTitle'), this.localize('untitledFieldNameBody', name));
			return false;
		}
		if (control.inputType == 'DB' && !this.mainForm.dbTable) {
			if (tab) this.mainForm.tabPanel.set('selectedTab', tab);
			control.selectControl();
			Ext.Msg.alert(this.localize('inputFieldDBWithoutTableTitle'), this.localize('inputFieldDBWithoutTableBody', name));
			return false;
		}
		return true;
	},

	getFormFieldNames: function() {
		var fieldNames = [];
		//populate list of field names from controls to determine dependency validations
		if (this.mainForm.useTabs) {
			this.mainForm.tabPanel.syncCurrentTab();
			this.mainForm.tabPanel.tabs.foreach(function(tab) {
				Ext.each(tab.controls, function(control) {
					if (control && control.viewmodelName != 'SpacerField') fieldNames.push((control.inputType == 'INPUT' ? control.inputName : control.column) || control.fieldLabel);
				}, this)
			}, this)
		} else this.mainForm.controls.foreach(function(control) {
			if (control && control.viewmodelName != 'SpacerField') fieldNames.push((control.inputType == 'INPUT' ? control.inputName : control.column) || control.fieldLabel);
		}, this);
		return fieldNames;
	},

	validateFormFieldNames: function() {
		var fieldNames = {},
			name = '',
			valid = true;

		//populate list of field names from controls to determine dependency validations
		if (this.mainForm.useTabs) {
			this.mainForm.tabPanel.syncCurrentTab();
			this.mainForm.tabPanel.tabs.foreach(function(tab) {
				Ext.each(tab.controls, function(control) {
					if (control) {
						name = (control.inputType == 'INPUT' ? control.inputName : control.column) || control.fieldLabel;
						if (name) {
							if (fieldNames[name]) {
								Ext.Msg.alert(this.localize('duplicateFieldNameTitle'), this.localize('duplicateFieldNameBody', name));
								valid = false;
								return false;
							} else {
								fieldNames[name] = 'something';
							}
						}
						if (control.columns && control.columns.length)
							control.columns.foreach(function(col) {
								col.controls.foreach(function(subControl) {
									name = (subControl.inputType == 'INPUT' ? subControl.inputName : subControl.column) || subControl.fieldLabel;
									if (name) {
										if (fieldNames[name]) {
											Ext.Msg.alert(this.localize('duplicateFieldNameTitle'), this.localize('duplicateFieldNameBody', name));
											valid = false;
											return false;
										} else {
											fieldNames[name] = 'something';
										}
									}
								})
							})
					}
				}, this)
			}, this)
		} else this.mainForm.controls.foreach(function(control) {
			if (control) {
				name = (control.inputType == 'INPUT' ? control.inputName : control.column) || control.fieldLabel;
				if (name) {
					if (fieldNames[name]) {
						Ext.Msg.alert(this.localize('duplicateFieldNameTitle'), this.localize('duplicateFieldNameBody', name));
						valid = false;
						return false;
					} else {
						fieldNames[name] = 'something';
					}
				}
				if (typeof control.columns === "object") {
					control.columns.foreach(function(col) {
						col.controls.foreach(function(subControl) {
							name = (subControl.inputType == 'INPUT' ? subControl.inputName : subControl.column) || subControl.fieldLabel;
							if (name) {
								if (fieldNames[name]) {
									Ext.Msg.alert(this.localize('duplicateFieldNameTitle'), this.localize('duplicateFieldNameBody', name));
									valid = false;
									return false;
								} else {
									fieldNames[name] = 'something';
								}
							}
						});
					});
				}
			}
		}, this);

		//Now make sure to check hidden fields too
		this.mainForm.hiddenFields.each(function(field) {
			if (fieldNames[field.data.name]) {
				Ext.Msg.alert(this.localize('duplicateFieldNameTitle'), this.localize('duplicateFieldNameBody', field.data.name));
				valid = false;
				return false;
			} else {
				fieldNames[field.data.name] = 'something';
			}
		}, this);
		return valid;
	},

	deleteForm: function() {
		Ext.MessageBox.show({
			title: this.localize('deleteFormTitle'),
			msg: this.localize('deleteFormBody', [this.mainForm.title, this.mainForm.properName]),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteForm'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteForm
		});
	},

	deleteFormIsEnabled$: function() {
		return this.id;
	},

	reallyDeleteForm: function(button) {
		if (button === 'yes') {
			Ext.Ajax.request({
				url: '/resolve/service/formbuilder/deleteForm',
				params: {
					formId: this.id
				},
				scope: this,
				success: this.deletedForm,
				failure: function(r){
					clientVM.displayFailure(r);
				}
			});
		}
	},

	deletedForm: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			this.clearExistingForm({
				id: '',
				panels: [{
					id: '',
					tabs: [{
						id: ''
					}]
				}],
				wizard: false,
				formName: this.localize('UNTITLEDFORM'),
				displayName: this.localize('UntitledForm'),
				tableName: ''
			});
		} else {
			clientVM.displayError(response.message);
		}
	},

	saveFormAs: function() {
		if (!this.validateSIRFields()) {
			return;
		}
		if (this.validateMainForm()) {
			var rsForm = this.convertToRSForm();
			//check to make sure there are access rights
			if (rsForm.adminRoles == '') {
				Ext.Msg.alert(this.localize('noAccessRightsTitle'), this.localize('noAccessRightsBody'));
				return;
			}
			this.open({
				mtype: 'SaveFormAs',
				name: rsForm.formName + '_' + this.localize('COPY'),
				title: rsForm.displayName + ' ' + this.localize('Copy')
			});
		}
	},
	saveFormAsIsEnabled$: function() {
		return this.id;
	},
	reallySaveFormAs: function(name, title) {
		var rsForm = this.convertToRSForm();
		rsForm.formName = name;
		rsForm.viewName = name;
		rsForm.displayName = title;

		//Wipe out all the id's in the current form because we're trying to save this form as a copy
		rsForm.id = '';
		rsForm.buttonPanel.id = '';
		Ext.each(rsForm.buttonPanel.controls, function(control) {
			control.id = '';
			Ext.each(control.dependencies, function(dep) {
				dep.id = '';
			});
		});
		Ext.each(rsForm.panels, function(panel) {
			panel.id = '';
			Ext.each(panel.tabs, function(tab) {
				tab.id = '';
				Ext.each(tab.columns, function(column) {
					Ext.each(column.fields, function(field) {
						field.id = '';
						Ext.each(field.dependencies, function(dep) {
							dep.id = '';
						});
					})
				});
			});
		});
		this.persistForm(rsForm, false);
	},

	validateMainForm: function() {
		var valid = true,
			fieldNames = this.getFormFieldNames();

		valid = this.validateFormFieldNames();
		if (!valid) return false;

		//make sure there is a formName
		if (!this.mainForm.properName) {
			valid = false;
			this.mainForm.set('showFieldSettings', 0);
			Ext.Msg.alert(this.localize('noFormNameTitle'), this.localize('noFormNameBody'));
			return false;
		}

		//Determine if the controls on the page are all valid
		if (this.mainForm.useTabs) {
			this.mainForm.tabPanel.tabs.foreach(function(tab) {
				Ext.each(tab.controls, function(control) {
					if (control) {
						//TODO: If control is a section, then check each column's controls as well
						if (!this.validateControl(control, tab)) {
							valid = false;
							return false;
						}
						if (control.dependenciesStore) {
							control.dependenciesStore.each(function(dep) {
								if (dep.data.target && fieldNames.indexOf(dep.data.target) == -1) {
									Ext.Msg.alert(this.localize('dependencyTargetInvalidTitle'), this.localize('dependencyTargetInvalidBody', [(control.inputType == 'INPUT' ? control.inputName : control.column) || control.fieldLabel, dep.data.target]));
									valid = false;
									return false;
								}
							}, this)
						}
					}
				}, this)
			}, this)
		} else this.mainForm.controls.foreach(function(control) {
			if (control) {
				//TODO: if control is a section, then check the column's controls too
				if (!this.validateControl(control)) {
					valid = false;
					return false;
				}
				if (control.dependenciesStore) {
					control.dependenciesStore.each(function(dep) {
						if (dep.data.target && fieldNames.indexOf(dep.data.target) == -1) {
							Ext.Msg.alert(this.localize('dependencyTargetInvalidTitle'), this.localize('dependencyTargetInvalidBody', [(control.inputType == 'INPUT' ? control.inputName : control.column) || control.fieldLabel, dep.data.target]));
							valid = false;
							return false;
						}
					}, this)
				}
			}
		}, this);

		//Check duplicate name of form
		if (this.mainForm.titleIsValid !== true) {
			Ext.Msg.alert(this.localize('duplicateNameDetectedTitle'), this.mainForm.titleIsValid);
			return false;
		}

		if (this.mainForm.nameIsValid !== true) {
			Ext.Msg.alert(this.localize('formNameInvalidTitle'), this.mainForm.nameIsValid);
			return false;
		}

		//If there isn't a table associated with this form, and there's a file upload field, that's bad... so throw an error
		if (!this.mainForm.dbTable) {
			if (fieldNames.indexOf('u_fileupload') > -1) {
				Ext.Msg.alert(this.localize('fileUploadNoTableTitle'), this.localize('fileUploadNoTableBody'));
				return false;
			}
		}

		//Check for button dependencies targets existing (buttons should be valid too)
		this.mainForm.formButtons.foreach(function(button) {
			if (button.dependencies) {
				button.dependencies.each(function(dep) {
					if (dep.data.target && fieldNames.indexOf(dep.data.target) == -1) {
						Ext.Msg.alert(this.localize('dependencyTargetInvalidTitle'), this.localize('dependencyTargetInvalidBody', [button.operation, dep.data.target]));
						valid = false;
						return false;
					}
				}, this)
			}
			if (button.type == 'button' && (!button.actions || button.actions.length == 0)) {
				Ext.Msg.alert(this.localize('noButtonActionTitle'), this.localize('noButtonActionBody', [button.operation]));
				valid = false;
				return false;
			}
		}, this);

		return valid;
	},

	/**
	 *Opens a dialog of the changes to be made to the server when the save is done
	 */
	validateSIRFields: function() {
		if (this.sirContext) {
			var unSupportedFields = [];
			var unSupportedControls = [];
			this.mainForm.controls.foreach(function(c) {
				var fieldName = c.viewmodelName;
				if (this.unSupportedFieldsMap[fieldName]) {
					unSupportedFields.push(this.unSupportedFieldsMap[fieldName]);
					unSupportedControls.push(c);
				}
			
			}, this);
			if (unSupportedFields.length) {
				var msg = Ext.String.format('For SIR action, the fields "{0}" must be deleted before the form can be saved.', unSupportedFields.join(', '));
				Ext.Msg.alert('Unsupported Field(s) Found on SIR Action', msg);
				for (var i = 0; i < unSupportedControls.length; i++) {
					unSupportedControls[i].set('unsupportedField', true);
				}
				return false;
			}
		}
		return true;
	},
	saveForm: function() {
		if (!this.validateSIRFields()) {
			return;
		}
		if (this.validateMainForm()) {
			var rsForm = this.convertToRSForm();
			//check to make sure there are access rights
			if (rsForm.adminRoles == '') {
				Ext.Msg.alert(this.localize('noAccessRightsTitle'), this.localize('noAccessRightsBody'));
				return;
			}
			this.open({
				mtype: 'PreviewTable',
				formConfig: rsForm
			});
		}
	},

	newForm: function() {
		window.open((window.location.origin ? window.location.origin : window.location.protocol + '//' + window.location.host) + '/resolve/jsp/rsclient.jsp?' + clientVM.rsclientToken + '&title=Custom%20Form%20View&url=%2Fresolve%2Fformbuilder%2Fformbuilder.jsp%3F');
	},
	/**
	 *Converts the form to server data and saves the form on the server
	 */
	confirmSave: function(previewTableData) {
		var rsForm = this.convertToRSForm(true);

		if (previewTableData.addToWiki) rsForm.wikiName = previewTableData.wikiName;
		if (previewTableData.createWiki) rsForm.wikiName = previewTableData.wikiNameCreate;
		if (previewTableData.noWiki) rsForm.wikiName = '';

		this.persistForm(rsForm);
	},

	persistForm: function(rsForm, preventReload) {
		Ext.MessageBox.show({
			msg: this.localize('savingMessage'),
			progressText: this.localize('savingProgress'),
			width: 300,
			wait: true,
			waitConfig: {
				interval: 200
			}
			//icon: 'ext-mb-save'
		});

		this.ajax({
			url: '/resolve/service/formbuilder/submitfb',
			jsonData: rsForm,
			timeout: 300000,
			scope: this,
			success: function(r) {
				Ext.MessageBox.hide();
				var response = RS.common.parsePayload(r);
				if (response.success) {
					clientVM.displaySuccess(this.localize('formSavedSuccessfully'))
					if (response.data && !preventReload) {
						this.convertFromRSForm(response.data);
						this.mainForm.on('afterColumnStoreLoaded', function() {
							this.set('initialState', this.convertToRSForm());
							return 'discard'
						}, this)
					}
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				Ext.MessageBox.hide()
				clientVM.displayFailure(r);
			}
		});
	},

	/**
	 *Converts the mainForm to a correct server interpretation to be persisted on the db
	 */
	convertToRSForm: function(isSave) {
		isSave = !!isSave;  // Making default to 'false'

		var form = {
				properties: {},
				panels: []
			},
			mainForm = this.mainForm,
			tabs = [];

		//Make sure things are in sync before we start serializing everything
		if (mainForm.useTabs) mainForm.tabPanel.syncCurrentTab();

		//Convert to form here
		form.id = this.id;
		form.displayName = mainForm.title;
		form.formName = mainForm.properName;
		form.viewName = mainForm.properName;
		form.tableName = mainForm.dbTable;
		form.wizard = mainForm.isWizard;
		form.windowTitle = mainForm.formWindowTitle;
		var tableRecord = mainForm.customTablesList.findRecord('umodelName', mainForm.dbTable, 0, false, false, true);
		form.tableDisplayName = tableRecord ? tableRecord.data.udisplayName : mainForm.dbTable;
		form.tableSysId = tableRecord ? tableRecord.data.sys_id : '';
		form.wikiName = mainForm.wikiName;

		if (mainForm.useTabs) {
			mainForm.tabPanel.tabs.foreach(function(tab, index) {
				var formFields = this.serializeControls(tab.controls);
				if (formFields) {
					for (var i=0; i<formFields.length; i++) {
						if (formFields[i].uiType === 'UrlContainerField' && formFields[i].referenceTableUrl) {
							const urlData = formFields[i].referenceTableUrl.split('#');
							const uriData = urlData[0].split('?');
							clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
								var csrftoken = '?' + data[0] + '=' + data[1];
								var tokenizedData = uri + csrftoken;
								if (uriData.length > 1) {
									tokenizedData += '&' + uriData[1];
								}
								if (urlData.length > 1) {
									tokenizedData += '#' + urlData[1];
								}
								formFields[i].referenceTableUrl = tokenizedData;
							});
						}
					}
				}
				var deps = tab.getDependencyControlConversionProperties().dependencies;
				tabs.push({
					id: tab.id,
					name: tab.title,
					displayName: tab.title,
					orderNumber: index + 1,
					cols: 1,
					columns: [{
						fields: formFields
					}],
					dependencies: deps
				});
			}, this);
		} else {
			//add in controls from the form
			var formFields = this.serializeControls(mainForm.controls);
			tabs.push({
				id: mainForm.id,
				cols: 1,
				columns: [{
					fields: formFields
				}]
			});
		}
		//buid structure for backend
		form.panels = [{
			id: this.tabPanelId,
			tabs: tabs
		}];

		//add in hidden fields
		var hiddenFields = this.convertHiddenFields(mainForm.hiddenFields);
		form.panels[0].tabs[0].columns[0].fields = form.panels[0].tabs[0].columns[0].fields.concat(hiddenFields);

		//Add in buttons
		var formButtons = this.serializeButtons(mainForm.formButtons, isSave);
		form.buttonPanel = {
			controls: formButtons
		};

		//Add in access rights
		var viewRoles = [],
			editRoles = [],
			adminRoles = [];
		this.mainForm.accessRights.each(function(accessRight) {
			if (accessRight.data.view) viewRoles.push(accessRight.data.name);
			if (accessRight.data.edit) editRoles.push(accessRight.data.name);
			if (accessRight.data.admin) adminRoles.push(accessRight.data.name);
		});

		form.viewRoles = viewRoles.join(',');
		form.editRoles = editRoles.join(',');
		form.adminRoles = adminRoles.join(',');

		return form;
	},

	clearExistingForm: function(rsForm) {
		this.set('id', rsForm.id);
		this.mainForm.controls.removeAll();
		this.set('tabPanelId', rsForm.panels[0].id);
		this.mainForm.tabPanel.clearControls();
		this.mainForm.set('useTabs', false);
		this.mainForm.hiddenFields.removeAll();
		try {
			this.mainForm.accessRights.removeAll();
		} catch (e) {}
		this.mainForm.formButtons.removeAll();	
		this.mainForm.treeStore.removed = [];
		this.mainForm.set('formButtonsSelections', []);
		this.mainForm.set('isWizard', rsForm.wizard);

		this.mainForm.set('id', rsForm.panels[0].tabs[0].id);
		this.mainForm.set('name', rsForm.formName)
		this.mainForm.set('title', rsForm.displayName || '');
		this.mainForm.set('dbTable', rsForm.tableName || '');
		this.mainForm.set('formWindowTitle', rsForm.windowTitle);
		this.mainForm.set('wikiName', rsForm.wikiName);

		this.mainForm.runbookStore.load();
		this.mainForm.scriptStore.load();
		this.mainForm.actionTaskStore.load();
		this.mainForm.wikiNames.load();
	},
	convertFromRSForm: function(rsForm) {
		if (!rsForm) {
			Ext.MessageBox.hide()
			clientVM.displayError(this.localize('noFormBody'))
			return
		}
		if (!rsForm.panels || rsForm.panels.length == 0) rsForm.panels = [{
			tabs: [{
				columns: [{
					fields: []
				}]
			}]
		}];

		var selectedTabIndex = this.mainForm.tabPanel.tabs.indexOf(this.mainForm.tabPanel.selectedTab);

		rsForm.buttonPanel = rsForm.buttonPanel || {
			controls: []
		};

		this.mainForm.customTablesList.load();
		if (rsForm.tableName) {
			this.mainForm.columnStore.removeAll();
			this.mainForm.columnStore.load();
		}
		this.existingFormNames.load();

		this.clearExistingForm(rsForm);

		var buttonPanel = rsForm.buttonPanel.controls;
		if (rsForm.panels[0].tabs.length > 1 || rsForm.panels[0].tabs[0].name) {
			this.mainForm.set('useTabs', true);

			this.mainForm.tabPanel.tabs.removeAll();
			Ext.Array.forEach(rsForm.panels[0].tabs, function(tab) {
				if (tab.columns.length > 0) {
					var fields = [];
					Ext.each(tab.columns[0].fields, function(fieldConfig) {
						fields.push(this.deserializeControlNoAdd(fieldConfig))
					}, this);
					this.mainForm.tabPanel.addTab({
						id: tab.id,
						title: tab.name,
						controls: fields
					});
					var newTab = this.mainForm.tabPanel.tabs.getAt(this.mainForm.tabPanel.tabs.length - 1);
					newTab.parseDependencyControlConversionProperties(tab);
				}
			}, this);

			if (this.mainForm.tabPanel.tabs.length > 0) this.mainForm.tabPanel.set('selectedTab', this.mainForm.tabPanel.tabs.getAt(selectedTabIndex < this.mainForm.tabPanel.tabs.length ? selectedTabIndex : 0));

		} else {
			if (rsForm.panels[0].tabs[0].columns.length > 0) {
				var configuration = rsForm.panels[0].tabs[0].columns[0].fields
					//convert all the controls to appropriate builder controls
				Ext.each(configuration, this.deserializeControl, this);
			}
		}

		//convert the buttons
		Ext.each(buttonPanel, this.deserializeButton, this);

		//ensure the form settings are displayed after restoring a form
		this.mainForm.set('showFieldSettings', 0);

		//process the access rights
		this.processAccessRights(rsForm);

		Ext.dd.DragDropManager.refreshCache({
			formControls: true,
			columnControls: true
		});

		this.mainForm.set('dbTable', rsForm.tableName || '');

		Ext.defer(function() {
			Ext.MessageBox.hide();
		}, 10)
	},

	processAccessRights: function(rsForm) {
		var viewRoles = rsForm.viewRoles ? rsForm.viewRoles.split(',') : [],
			editRoles = rsForm.editRoles ? rsForm.editRoles.split(',') : [],
			adminRoles = rsForm.adminRoles ? rsForm.adminRoles.split(',') : [],
			accessRights = [],
			i;
		for (i = 0; i < viewRoles.length; i++)
			this.addToAccessRights(accessRights, viewRoles[i], 'view', true);
		for (i = 0; i < editRoles.length; i++)
			this.addToAccessRights(accessRights, editRoles[i], 'edit', true);
		for (i = 0; i < adminRoles.length; i++)
			this.addToAccessRights(accessRights, adminRoles[i], 'admin', true);

		this.mainForm.accessRights.add(accessRights);
	},
	addToAccessRights: function(accessRights, name, right, value) {
		var i, len = accessRights.length;
		for (i = 0; i < len; i++) {
			if (accessRights[i].name == name) {
				accessRights[i][right] = value;
				return;
			}
		}

		var newName = {
			name: name,
			value: name
		};
		newName[right] = value;
		accessRights.push(newName);
	},
	/**
	 *Converts controls from extjs to server form to be peristed on the db
	 */
	serializeControls: function(controls) {
		var ctrls = [];
		if (Ext.isArray(controls)) Ext.each(controls, function(control, index) {
			if (control) ctrls.push(this.serializeControl(control, index + 1));
		}, this)
		else if (controls.foreach) controls.foreach(function(control, index) {
			if (control) ctrls.push(this.serializeControl(control, index + 1));
		}, this);

		return ctrls;
	},
	serializeControl: function(control, index) {
		var tmpControl = {
			orderNumber: index
		};

		Ext.each(control.serializeFunctions, function(serializeFunction) {
			Ext.apply(tmpControl, serializeFunction.call(control));
		}, this);

		switch (control.viewmodelName) {
			case 'TextField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'TextField'
				});
				break;

			case 'TextAreaField':
				Ext.apply(tmpControl, {
					xtype: 'textarea',
					uiType: 'TextArea'
				});
				break;
			case 'RadioButtonField':
				Ext.apply(tmpControl, {
					xtype: 'radiogroup',
					uiType: 'RadioButton'
				});
				break;
			case 'NumberField':
				Ext.apply(tmpControl, {
					xtype: 'numberfield',
					uiType: 'NumberTextField'
				});
				break;
			case 'DecimalField':
				Ext.apply(tmpControl, {
					xtype: 'numberfield',
					uiType: 'DecimalTextField'
				});
				break;
			case 'MultipleCheckboxField':
				Ext.apply(tmpControl, {
					xtype: 'checkboxgroup',
					uiType: 'MultipleCheckBox'
				});
				break;
			case 'ComboBoxField':
				Ext.apply(tmpControl, {
					xtype: 'combo',
					uiType: 'ComboBox'
				});
				break;
			case 'MultiSelectComboBoxField':
				Ext.apply(tmpControl, {
					xtype: 'combo',
					uiType: 'MultiSelectComboBoxField'
				});
				break;
			case 'NameField':
				Ext.apply(tmpControl, {
					xtype: 'namefield',
					uiType: 'NameField'
				});
				break;
			case 'AddressField':
				Ext.apply(tmpControl, {
					xtype: 'addressfield',
					uiType: 'AddressField'
				});
				break;
			case 'EmailField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'EmailField'
				});
				break;

			case 'MACAddressField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'MACAddressField'
				});
				break;

			case 'IPAddressField':
			Ext.apply(tmpControl, {
				xtype: 'textfield',
				uiType: 'IPAddressField'
			});
			break;

			case 'CIDRAddressField':
			Ext.apply(tmpControl, {
				xtype: 'textfield',
				uiType: 'CIDRAddressField'
			});
			break;

			case 'PhoneField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'PhoneField'
				});
				break;
			case 'DateField':
				Ext.apply(tmpControl, {
					xtype: 'datefield',
					uiType: 'Date'
				});
				break;
			case 'TimeField':
				Ext.apply(tmpControl, {
					xtype: 'timefield',
					uiType: 'Timestamp'
				});
				break;
			case 'LinkField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'Link'
				});
				break;
			case 'LikeRTField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'TextField'
				});
				break;
			case 'CheckboxField':
				Ext.apply(tmpControl, {
					xtype: 'checkbox',
					uiType: 'CheckBox'
				});
				break;
			case 'PasswordField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'Password'
				});
				break;
			case 'TagField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'TagField'
				});
				break;
			case 'SequenceField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'Sequence'
				});
				break;
			case 'ListField':
				Ext.apply(tmpControl, {
					xtype: 'multiselect',
					uiType: 'List'
				});
				break;
			case 'ButtonField':
				Ext.apply(tmpControl, {
					xtype: 'button',
					uiType: 'ButtonField'
				});
				break;
			case 'ReferenceField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'Reference'
				});
				break;
			case 'JournalField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'Journal'
				});
				break;
			case 'DateTimeField':
				Ext.apply(tmpControl, {
					xtype: 'datefield',
					uiType: 'DateTime'
				});
				break;

			case 'ReadOnlyTextField':
				Ext.apply(tmpControl, {
					xtype: 'displayfield',
					uiType: 'ReadOnlyTextField'
				})
				break;

			case 'UserPickerField':
				Ext.apply(tmpControl, {
					xtype: 'userpickerfield',
					uiType: 'UserPickerField'
				})
				break;

			case 'TeamPickerField':
				Ext.apply(tmpControl, {
					xtype: 'teampickerfield',
					uiType: 'TeamPickerField'
				})
				break;

			case 'FileUploadField':
				Ext.apply(tmpControl, {
					xtype: 'uploadmanager',
					uiType: 'FileUploadField'
				});
				break;

			case 'ReferenceTableField':
				Ext.apply(tmpControl, {
					xtype: 'referencetable',
					uiType: 'ReferenceTableField'
				})
				break;

			case 'SpacerField':
				Ext.apply(tmpControl, {
					xtype: 'box',
					uiType: 'SpacerField'
				});
				break;

			case 'LabelField':
				Ext.apply(tmpControl, {
					xtype: 'label',
					uiType: 'LabelField'
				})
				break;

			case 'WysiwygField':
				Ext.apply(tmpControl, {
					xtype: 'htmleditor',
					uiType: 'WysiwygField'
				})
				break;

			case 'TabContainerField':
				Ext.apply(tmpControl, {
					xtype: 'tabcontainer',
					uiType: 'TabContainerField'
				})
				break;

			case 'UrlField':
				Ext.apply(tmpControl, {
					xtype: 'urlcontainerfield',
					uiType: 'UrlContainerField'
				});
				break;

			case 'SectionContainerField':
				Ext.apply(tmpControl, {
					xtype: 'panel',
					uiType: 'SectionContainerField'
				});
				break;

			case 'Panel':
				return null;
				break;

			default:
				break;
		}

		return tmpControl;
	},
	deserializeControlNoAdd: function(config) {
		return this.deserializeControl(config, false);
	},
	deserializeControl: function(config, autoAdd, records, parent) {
		config.fieldLabel = config.displayName;

		switch (config.uiType) {
			case 'TextField':
				config.mtype = 'TextField';
				break;
			case 'TextArea':
				config.mtype = 'TextAreaField';
				break;
			case 'NumberTextField':
				config.mtype = 'NumberField';
				break;
			case 'DecimalTextField':
				config.mtype = 'DecimalField';
				break;
			case 'ComboBox':
				config.mtype = 'ComboBoxField';
				break;
			case 'MultipleCheckBox':
				config.mtype = 'MultipleCheckboxField';
				break;
			case 'RadioButton':
				config.mtype = 'RadioButtonField';
				break;
			case 'Date':
				config.mtype = 'DateField';
				break;
			case 'Timestamp':
				config.mtype = 'TimeField';
				break;
			case 'DateTime':
				config.mtype = 'DateTimeField';
				break;
			case 'Link':
				config.mtype = 'LinkField';
				break;
			case 'CheckBox':
				config.mtype = 'CheckboxField';
				break;
			case 'Password':
				config.mtype = 'PasswordField';
				break;
			case 'TagField':
				config.mtype = 'TagField';
				break;
			case 'Sequence':
				config.mtype = 'SequenceField';
				break;
			case 'List':
				config.mtype = 'ListField';
				break;
			case 'ButtonField':
				config.mtype = 'ButtonField';
				break;
			case 'Reference':
				config.mtype = 'ReferenceField';
				break;
			case 'Journal':
				config.mtype = 'JournalField';
				break;
			case 'ReadOnlyTextField':
				config.mtype = 'ReadOnlyTextField';
				break;
			case 'UserPickerField':
				config.mtype = 'UserPickerField';
				break;
			case 'TeamPickerField':
				config.mtype = 'TeamPickerField';
				break;
			case 'EmailField':
				config.mtype = 'EmailField';
				break;
			case 'MACAddressField':
				config.mtype = 'MACAddressField';
				break;
			case 'IPAddressField':
				config.mtype = 'IPAddressField';
				break;
			case 'CIDRAddressField':
				config.mtype = 'CIDRAddressField';
				break;
			case 'PhoneField':
				config.mtype = 'PhoneField';
				break;
			case 'NameField':
				config.mtype = 'NameField';
				break;
			case 'AddressField':
				config.mtype = 'AddressField';
				break;
			case 'HiddenField':
				config.mtype = 'HiddenField';
				config.value = config.hiddenValue;
				break;
			case 'FileUploadField':
				config.mtype = 'FileUploadField';
				//We're restoring a file upload field, which means that we already have one and shouldn't enable the field by default
				this.set('fileUploadIsEnabled', false);
				break;
			case 'ReferenceTableField':
				config.mtype = 'ReferenceTableField';
				break;
			case 'MultiSelectComboBoxField':
				config.mtype = 'MultiSelectComboBoxField';
				break;
			case 'SpacerField':
				config.mtype = 'SpacerField';
				break;
			case 'LabelField':
				config.mtype = 'LabelField';
				break;
			case 'WysiwygField':
				config.mtype = 'WysiwygField';
				break;
			case 'TabContainerField':
				config.mtype = 'TabContainerField';
				break;
			case 'UrlContainerField':
				config.mtype = 'UrlField';
				break;
			case 'SectionContainerField':
				config.mtype = 'SectionContainerField';
				break;
			default:
				break;
		}

		var ctrl = (parent || this.mainForm).model(config);

		Ext.each(ctrl.deserializeFunctions, function(deserializeFunction) {
			deserializeFunction.call(ctrl, config);
		}, this);

		if (ctrl.viewmodelName == 'HiddenField') {
			this.mainForm.hiddenFields.add(config);
		} else {
			if (autoAdd !== false) {
				this.mainForm.addRealControl(ctrl);
			} else {
				return ctrl;
			}
		}
	},
	/**
	 *Converts hidden fields to controls to be persisted on the db
	 * @param {Object} hiddenFields
	 */
	convertHiddenFields: function(hiddenFields) {
		var controls = [];
		hiddenFields.data.each(function(hiddenField) {
			controls.push({
				hiddenValue: hiddenField.data.value,
				name: hiddenField.data.name,
				sourceType: 'INPUT',
				xtype: 'hiddenfield',
				uiType: 'HiddenField'
			});
		})
		return controls;
	},
	/**
	 *Converts buttons to buttonPanel to be persisted on the db
	 * @param {Object} buttons
	 */
	serializeButtons: function(buttons, isSave) {
		var buttonPanel = [];
		buttons.foreach(function(button,index) {
			button.orderNumber = index+1;
			buttonPanel.push(button.getButtonControlConversionProperties(isSave));
		});
		return buttonPanel;
	},
	deserializeButton: function(button) {
		var btn = this.mainForm.model({
			mtype: 'ButtonControl'
		});
		btn.parseButtonControlConversionProperties(button);
		this.mainForm.formButtons.add(btn);
		btn.parseButtonControlConversionProperties(button, true);
	},

	fileUploadIsEnabled: true,
	when_fileUploadIsEnabled_changes_update_the_available_field: {
		on: ['fileUploadIsEnabledChanged'],
		action: function() {
			this.availableControls.foreach(function(availableControl) {
				if (availableControl.controlType == 'FileUploadField') availableControl.set('isDisabled', this.sirContext || !this.fileUploadIsEnabled);
			}, this);
		}
	},

	showFieldSettings: function() {
		this.mainForm.set('showFieldSettings', 1)
	},
	showFieldSettingsIsPressed$: function() {
		return this.mainForm.showFieldSettings == 1
	},
	showFormSettings: function() {
		this.mainForm.set('showFieldSettings', 0)
	},
	showFormSettingsIsPressed$: function() {
		return this.mainForm.showFieldSettings == 0
	}

});
