glu.defModel('RS.formbuilder.ReferenceTable', {
	mixins: ['DependencyControl'],
	fields: ['fieldLabel', 'referenceTable', 'referenceColumn', 'referenceColumns', 'allowReferenceTableAdd', 'allowReferenceTableRemove', 'allowReferenceTableView', 'referenceTableUrl', 'referenceTableViewPopup', 'referenceTableViewPopupWidth', 'referenceTableViewPopupHeight', 'referenceTableViewPopupTitle', 'rsType'],
	rsType: 'ReferenceTable',
	fieldLabel: '',
	referenceTable: '',
	referenceColumn: '',
	referenceColumns: '',
	allowReferenceTableAdd: true,
	allowReferenceTableRemove: true,
	allowReferenceTableView: true,
	referenceTableUrl: '',
	referenceTableViewPopup: false,
	referenceTableViewPopupWidth: 600,
	referenceTableViewPopupHeight: 600,
	referenceTableViewPopupTitle: '',
	referenceColumnStore: {
		mtype: 'store',
		fields: ['name', 'dbcolumnId', 'displayName', 'dbtype', 'uiType', 'integerMinValue', 'integerMaxValue', 'decimalMinValue', 'decimalMaxValue', 'stringMinLength', 'stringMaxLength', 'uiStringMaxLength', 'selectValues', 'choiceValues', 'referenceTable', 'referenceDisplayColumn'],
		data: [],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getcustomtablecolumns',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	}
});