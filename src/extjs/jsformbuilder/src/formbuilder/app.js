/**
 * Main entry point for the Form Builder application.  Builds a viewport to display the form builder and instantiates the Main ViewModel
 */
Ext.onReady(function() {
	Ext.tip.QuickTipManager.init();
	glu.viewport({
		mtype: 'RS.formbuilder.Main',
		formName: formBuilderOptions.formName,
		formId: formBuilderOptions.formId,
		isAdmin: formBuilderOptions.isAdmin,
		wikiName: formBuilderOptions.wikiName
	});
});