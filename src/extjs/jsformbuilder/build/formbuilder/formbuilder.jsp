<!-- ******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
********************************************************************************* -->

<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="org.owasp.esapi.ESAPI" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%
boolean debug = false;
String d = request.getParameter("debug");
if( d != null && d.indexOf("t") > -1){
    debug = true;
}
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title id="page-title">Resolve Form Builder</title>

    <jsp:include page="../client/common.jsp" flush="true"/>
    <jsp:include page="loader.jsp" flush="true"/>

    <script type="text/javascript">
        var formBuilderOptions = {
            formId: '<%=ESAPI.encoder().encodeForJavaScript(request.getParameter("formId") == null ? "" : request.getParameter("formId"))%>',
            formName: '<%=ESAPI.encoder().encodeForJavaScript(request.getParameter("view") == null ? "" : request.getParameter("view").replace("'", "\\\'"))%>',
            isAdmin: <%="true"%>,
            wikiName: '<%=""%>'
        };
    </script>

    <script type="text/javascript">
        Ext.onReady(function() {
            Ext.tip.QuickTipManager.init();
            glu.asyncLayouts = true
            glu.viewport({
                mtype: 'RS.formbuilder.Main',
                formName: formBuilderOptions.formName,
                formId: formBuilderOptions.formId,
                isAdmin: formBuilderOptions.isAdmin,
                wikiName: formBuilderOptions.wikiName
            });
        });
    </script>

</head>
<body>
</body>
</html>

