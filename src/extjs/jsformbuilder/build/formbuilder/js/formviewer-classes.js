/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/
/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.Address', {
	extend: 'Ext.form.FieldContainer',
	alias: 'widget.addressfield',

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	addressSeparator: '|&|',

	initComponent: function() {
		var fieldName = this.name,
			values = this.value ? this.value.split(this.addressSeparator) : [];

		if(!Ext.isArray(this.items)) this.items = [{
			xtype: 'hiddenfield',
			name: this.name
		}];

		this.items = [{
			xtype: 'textfield',
			labelAlign: 'top',
			readOnly: this.readOnly,
			fieldCls: this.fieldCls,
			fieldLabel: RS.formbuilder.locale.Street1,
			value: values.length == 6 ? values[0] : '',
			listeners: {
				change: {
					scope: this,
					fn: this.addressChanged
				}
			}
		}, {
			xtype: 'textfield',
			labelAlign: 'top',
			readOnly: this.readOnly,
			fieldCls: this.fieldCls,
			fieldLabel: RS.formbuilder.locale.Street2,
			value: values.length == 6 ? values[1] : '',
			listeners: {
				change: {
					scope: this,
					fn: this.addressChanged
				}
			}
		}, {
			xtype: 'fieldcontainer',
			hideLabel: true,
			layout: 'hbox',
			items: [{
				xtype: 'textfield',
				labelAlign: 'top',
				readOnly: this.readOnly,
				fieldCls: this.fieldCls,
				flex: 1,
				fieldLabel: RS.formbuilder.locale.City,
				value: values.length == 6 ? values[2] : '',
				listeners: {
					change: {
						scope: this,
						fn: this.addressChanged
					}
				}
			}, {
				xtype: 'textfield',
				labelAlign: 'top',
				readOnly: this.readOnly,
				fieldCls: this.fieldCls,
				margin: '0 0 0 5',
				flex: 1,
				fieldLabel: RS.formbuilder.locale.State,
				value: values.length == 6 ? values[3] : '',
				listeners: {
					change: {
						scope: this,
						fn: this.addressChanged
					}
				}
			}]
		}, {
			xtype: 'fieldcontainer',
			hideLabel: true,
			layout: 'hbox',
			items: [{
				xtype: 'textfield',
				labelAlign: 'top',
				readOnly: this.readOnly,
				fieldCls: this.fieldCls,
				flex: 1,
				fieldLabel: RS.formbuilder.locale.Zip,
				value: values.length == 6 ? values[4] : '',
				listeners: {
					change: {
						scope: this,
						fn: this.addressChanged
					}
				}
			}, {
				xtype: 'combobox',
				displayField: 'name',
				valueField: 'code',
				queryMode: 'local',
				forceSelection: true,
				value: values.length == 6 ? values[5] : '',
				store: {
					data: this.listOfCountries,
					fields: ['name', 'code'],
					proxy: {
						type: 'memory',
						reader: {
							type: 'json'
						}
					}
				},
				labelAlign: 'top',
				readOnly: this.readOnly,
				fieldCls: this.fieldCls,
				margin: '0 0 0 5',
				flex: 1,
				fieldLabel: RS.formbuilder.locale.Country,
				listeners: {
					change: {
						scope: this,
						fn: this.addressChanged
					}
				}
			}]
		}].concat(this.items);
		this.callParent();
	},
	setFieldStyle: function(style){
		var fields = this.query('field');
		Ext.each(fields, function(field){
			if( field.setFieldStyle)field.setFieldStyle(style);
		});
	},
	addressChanged: function() {
		if(!this.rendered) return;
		var address = [],
			fields = this.query('field');
		Ext.each(fields, function(field) {
			if(field.xtype != 'hiddenfield') address.push(field.getValue());
		});
		this.down('hiddenfield').setValue(address.join(this.addressSeparator));
	},
	listOfCountries: [{
		name: 'Afghanistan',
		code: 'AF'
	}, {
		name: 'Åland Islands',
		code: 'AX'
	}, {
		name: 'Albania',
		code: 'AL'
	}, {
		name: 'Algeria',
		code: 'DZ'
	}, {
		name: 'American Samoa',
		code: 'AS'
	}, {
		name: 'AndorrA',
		code: 'AD'
	}, {
		name: 'Angola',
		code: 'AO'
	}, {
		name: 'Anguilla',
		code: 'AI'
	}, {
		name: 'Antarctica',
		code: 'AQ'
	}, {
		name: 'Antigua and Barbuda',
		code: 'AG'
	}, {
		name: 'Argentina',
		code: 'AR'
	}, {
		name: 'Armenia',
		code: 'AM'
	}, {
		name: 'Aruba',
		code: 'AW'
	}, {
		name: 'Australia',
		code: 'AU'
	}, {
		name: 'Austria',
		code: 'AT'
	}, {
		name: 'Azerbaijan',
		code: 'AZ'
	}, {
		name: 'Bahamas',
		code: 'BS'
	}, {
		name: 'Bahrain',
		code: 'BH'
	}, {
		name: 'Bangladesh',
		code: 'BD'
	}, {
		name: 'Barbados',
		code: 'BB'
	}, {
		name: 'Belarus',
		code: 'BY'
	}, {
		name: 'Belgium',
		code: 'BE'
	}, {
		name: 'Belize',
		code: 'BZ'
	}, {
		name: 'Benin',
		code: 'BJ'
	}, {
		name: 'Bermuda',
		code: 'BM'
	}, {
		name: 'Bhutan',
		code: 'BT'
	}, {
		name: 'Bolivia',
		code: 'BO'
	}, {
		name: 'Bosnia and Herzegovina',
		code: 'BA'
	}, {
		name: 'Botswana',
		code: 'BW'
	}, {
		name: 'Bouvet Island',
		code: 'BV'
	}, {
		name: 'Brazil',
		code: 'BR'
	}, {
		name: 'British Indian Ocean Territory',
		code: 'IO'
	}, {
		name: 'Brunei Darussalam',
		code: 'BN'
	}, {
		name: 'Bulgaria',
		code: 'BG'
	}, {
		name: 'Burkina Faso',
		code: 'BF'
	}, {
		name: 'Burundi',
		code: 'BI'
	}, {
		name: 'Cambodia',
		code: 'KH'
	}, {
		name: 'Cameroon',
		code: 'CM'
	}, {
		name: 'Canada',
		code: 'CA'
	}, {
		name: 'Cape Verde',
		code: 'CV'
	}, {
		name: 'Cayman Islands',
		code: 'KY'
	}, {
		name: 'Central African Republic',
		code: 'CF'
	}, {
		name: 'Chad',
		code: 'TD'
	}, {
		name: 'Chile',
		code: 'CL'
	}, {
		name: 'China',
		code: 'CN'
	}, {
		name: 'Christmas Island',
		code: 'CX'
	}, {
		name: 'Cocos (Keeling) Islands',
		code: 'CC'
	}, {
		name: 'Colombia',
		code: 'CO'
	}, {
		name: 'Comoros',
		code: 'KM'
	}, {
		name: 'Congo',
		code: 'CG'
	}, {
		name: 'Congo, The Democratic Republic of the',
		code: 'CD'
	}, {
		name: 'Cook Islands',
		code: 'CK'
	}, {
		name: 'Costa Rica',
		code: 'CR'
	}, {
		name: 'Cote D\'Ivoire',
		code: 'CI'
	}, {
		name: 'Croatia',
		code: 'HR'
	}, {
		name: 'Cuba',
		code: 'CU'
	}, {
		name: 'Cyprus',
		code: 'CY'
	}, {
		name: 'Czech Republic',
		code: 'CZ'
	}, {
		name: 'Denmark',
		code: 'DK'
	}, {
		name: 'Djibouti',
		code: 'DJ'
	}, {
		name: 'Dominica',
		code: 'DM'
	}, {
		name: 'Dominican Republic',
		code: 'DO'
	}, {
		name: 'Ecuador',
		code: 'EC'
	}, {
		name: 'Egypt',
		code: 'EG'
	}, {
		name: 'El Salvador',
		code: 'SV'
	}, {
		name: 'Equatorial Guinea',
		code: 'GQ'
	}, {
		name: 'Eritrea',
		code: 'ER'
	}, {
		name: 'Estonia',
		code: 'EE'
	}, {
		name: 'Ethiopia',
		code: 'ET'
	}, {
		name: 'Falkland Islands (Malvinas)',
		code: 'FK'
	}, {
		name: 'Faroe Islands',
		code: 'FO'
	}, {
		name: 'Fiji',
		code: 'FJ'
	}, {
		name: 'Finland',
		code: 'FI'
	}, {
		name: 'France',
		code: 'FR'
	}, {
		name: 'French Guiana',
		code: 'GF'
	}, {
		name: 'French Polynesia',
		code: 'PF'
	}, {
		name: 'French Southern Territories',
		code: 'TF'
	}, {
		name: 'Gabon',
		code: 'GA'
	}, {
		name: 'Gambia',
		code: 'GM'
	}, {
		name: 'Georgia',
		code: 'GE'
	}, {
		name: 'Germany',
		code: 'DE'
	}, {
		name: 'Ghana',
		code: 'GH'
	}, {
		name: 'Gibraltar',
		code: 'GI'
	}, {
		name: 'Greece',
		code: 'GR'
	}, {
		name: 'Greenland',
		code: 'GL'
	}, {
		name: 'Grenada',
		code: 'GD'
	}, {
		name: 'Guadeloupe',
		code: 'GP'
	}, {
		name: 'Guam',
		code: 'GU'
	}, {
		name: 'Guatemala',
		code: 'GT'
	}, {
		name: 'Guernsey',
		code: 'GG'
	}, {
		name: 'Guinea',
		code: 'GN'
	}, {
		name: 'Guinea-Bissau',
		code: 'GW'
	}, {
		name: 'Guyana',
		code: 'GY'
	}, {
		name: 'Haiti',
		code: 'HT'
	}, {
		name: 'Heard Island and Mcdonald Islands',
		code: 'HM'
	}, {
		name: 'Holy See (Vatican City State)',
		code: 'VA'
	}, {
		name: 'Honduras',
		code: 'HN'
	}, {
		name: 'Hong Kong',
		code: 'HK'
	}, {
		name: 'Hungary',
		code: 'HU'
	}, {
		name: 'Iceland',
		code: 'IS'
	}, {
		name: 'India',
		code: 'IN'
	}, {
		name: 'Indonesia',
		code: 'ID'
	}, {
		name: 'Iran, Islamic Republic Of',
		code: 'IR'
	}, {
		name: 'Iraq',
		code: 'IQ'
	}, {
		name: 'Ireland',
		code: 'IE'
	}, {
		name: 'Isle of Man',
		code: 'IM'
	}, {
		name: 'Israel',
		code: 'IL'
	}, {
		name: 'Italy',
		code: 'IT'
	}, {
		name: 'Jamaica',
		code: 'JM'
	}, {
		name: 'Japan',
		code: 'JP'
	}, {
		name: 'Jersey',
		code: 'JE'
	}, {
		name: 'Jordan',
		code: 'JO'
	}, {
		name: 'Kazakhstan',
		code: 'KZ'
	}, {
		name: 'Kenya',
		code: 'KE'
	}, {
		name: 'Kiribati',
		code: 'KI'
	}, {
		name: 'Korea, Democratic People\'S Republic of',
		code: 'KP'
	}, {
		name: 'Korea, Republic of',
		code: 'KR'
	}, {
		name: 'Kuwait',
		code: 'KW'
	}, {
		name: 'Kyrgyzstan',
		code: 'KG'
	}, {
		name: 'Lao People\'S Democratic Republic',
		code: 'LA'
	}, {
		name: 'Latvia',
		code: 'LV'
	}, {
		name: 'Lebanon',
		code: 'LB'
	}, {
		name: 'Lesotho',
		code: 'LS'
	}, {
		name: 'Liberia',
		code: 'LR'
	}, {
		name: 'Libyan Arab Jamahiriya',
		code: 'LY'
	}, {
		name: 'Liechtenstein',
		code: 'LI'
	}, {
		name: 'Lithuania',
		code: 'LT'
	}, {
		name: 'Luxembourg',
		code: 'LU'
	}, {
		name: 'Macao',
		code: 'MO'
	}, {
		name: 'Macedonia, The Former Yugoslav Republic of',
		code: 'MK'
	}, {
		name: 'Madagascar',
		code: 'MG'
	}, {
		name: 'Malawi',
		code: 'MW'
	}, {
		name: 'Malaysia',
		code: 'MY'
	}, {
		name: 'Maldives',
		code: 'MV'
	}, {
		name: 'Mali',
		code: 'ML'
	}, {
		name: 'Malta',
		code: 'MT'
	}, {
		name: 'Marshall Islands',
		code: 'MH'
	}, {
		name: 'Martinique',
		code: 'MQ'
	}, {
		name: 'Mauritania',
		code: 'MR'
	}, {
		name: 'Mauritius',
		code: 'MU'
	}, {
		name: 'Mayotte',
		code: 'YT'
	}, {
		name: 'Mexico',
		code: 'MX'
	}, {
		name: 'Micronesia, Federated States of',
		code: 'FM'
	}, {
		name: 'Moldova, Republic of',
		code: 'MD'
	}, {
		name: 'Monaco',
		code: 'MC'
	}, {
		name: 'Mongolia',
		code: 'MN'
	}, {
		name: 'Montserrat',
		code: 'MS'
	}, {
		name: 'Morocco',
		code: 'MA'
	}, {
		name: 'Mozambique',
		code: 'MZ'
	}, {
		name: 'Myanmar',
		code: 'MM'
	}, {
		name: 'Namibia',
		code: 'NA'
	}, {
		name: 'Nauru',
		code: 'NR'
	}, {
		name: 'Nepal',
		code: 'NP'
	}, {
		name: 'Netherlands',
		code: 'NL'
	}, {
		name: 'Netherlands Antilles',
		code: 'AN'
	}, {
		name: 'New Caledonia',
		code: 'NC'
	}, {
		name: 'New Zealand',
		code: 'NZ'
	}, {
		name: 'Nicaragua',
		code: 'NI'
	}, {
		name: 'Niger',
		code: 'NE'
	}, {
		name: 'Nigeria',
		code: 'NG'
	}, {
		name: 'Niue',
		code: 'NU'
	}, {
		name: 'Norfolk Island',
		code: 'NF'
	}, {
		name: 'Northern Mariana Islands',
		code: 'MP'
	}, {
		name: 'Norway',
		code: 'NO'
	}, {
		name: 'Oman',
		code: 'OM'
	}, {
		name: 'Pakistan',
		code: 'PK'
	}, {
		name: 'Palau',
		code: 'PW'
	}, {
		name: 'Palestinian Territory, Occupied',
		code: 'PS'
	}, {
		name: 'Panama',
		code: 'PA'
	}, {
		name: 'Papua New Guinea',
		code: 'PG'
	}, {
		name: 'Paraguay',
		code: 'PY'
	}, {
		name: 'Peru',
		code: 'PE'
	}, {
		name: 'Philippines',
		code: 'PH'
	}, {
		name: 'Pitcairn',
		code: 'PN'
	}, {
		name: 'Poland',
		code: 'PL'
	}, {
		name: 'Portugal',
		code: 'PT'
	}, {
		name: 'Puerto Rico',
		code: 'PR'
	}, {
		name: 'Qatar',
		code: 'QA'
	}, {
		name: 'Reunion',
		code: 'RE'
	}, {
		name: 'Romania',
		code: 'RO'
	}, {
		name: 'Russian Federation',
		code: 'RU'
	}, {
		name: 'RWANDA',
		code: 'RW'
	}, {
		name: 'Saint Helena',
		code: 'SH'
	}, {
		name: 'Saint Kitts and Nevis',
		code: 'KN'
	}, {
		name: 'Saint Lucia',
		code: 'LC'
	}, {
		name: 'Saint Pierre and Miquelon',
		code: 'PM'
	}, {
		name: 'Saint Vincent and the Grenadines',
		code: 'VC'
	}, {
		name: 'Samoa',
		code: 'WS'
	}, {
		name: 'San Marino',
		code: 'SM'
	}, {
		name: 'Sao Tome and Principe',
		code: 'ST'
	}, {
		name: 'Saudi Arabia',
		code: 'SA'
	}, {
		name: 'Senegal',
		code: 'SN'
	}, {
		name: 'Serbia and Montenegro',
		code: 'CS'
	}, {
		name: 'Seychelles',
		code: 'SC'
	}, {
		name: 'Sierra Leone',
		code: 'SL'
	}, {
		name: 'Singapore',
		code: 'SG'
	}, {
		name: 'Slovakia',
		code: 'SK'
	}, {
		name: 'Slovenia',
		code: 'SI'
	}, {
		name: 'Solomon Islands',
		code: 'SB'
	}, {
		name: 'Somalia',
		code: 'SO'
	}, {
		name: 'South Africa',
		code: 'ZA'
	}, {
		name: 'South Georgia and the South Sandwich Islands',
		code: 'GS'
	}, {
		name: 'Spain',
		code: 'ES'
	}, {
		name: 'Sri Lanka',
		code: 'LK'
	}, {
		name: 'Sudan',
		code: 'SD'
	}, {
		name: 'Suriname',
		code: 'SR'
	}, {
		name: 'Svalbard and Jan Mayen',
		code: 'SJ'
	}, {
		name: 'Swaziland',
		code: 'SZ'
	}, {
		name: 'Sweden',
		code: 'SE'
	}, {
		name: 'Switzerland',
		code: 'CH'
	}, {
		name: 'Syrian Arab Republic',
		code: 'SY'
	}, {
		name: 'Taiwan, Province of China',
		code: 'TW'
	}, {
		name: 'Tajikistan',
		code: 'TJ'
	}, {
		name: 'Tanzania, United Republic of',
		code: 'TZ'
	}, {
		name: 'Thailand',
		code: 'TH'
	}, {
		name: 'Timor-Leste',
		code: 'TL'
	}, {
		name: 'Togo',
		code: 'TG'
	}, {
		name: 'Tokelau',
		code: 'TK'
	}, {
		name: 'Tonga',
		code: 'TO'
	}, {
		name: 'Trinidad and Tobago',
		code: 'TT'
	}, {
		name: 'Tunisia',
		code: 'TN'
	}, {
		name: 'Turkey',
		code: 'TR'
	}, {
		name: 'Turkmenistan',
		code: 'TM'
	}, {
		name: 'Turks and Caicos Islands',
		code: 'TC'
	}, {
		name: 'Tuvalu',
		code: 'TV'
	}, {
		name: 'Uganda',
		code: 'UG'
	}, {
		name: 'Ukraine',
		code: 'UA'
	}, {
		name: 'United Arab Emirates',
		code: 'AE'
	}, {
		name: 'United Kingdom',
		code: 'GB'
	}, {
		name: 'United States',
		code: 'US'
	}, {
		name: 'United States Minor Outlying Islands',
		code: 'UM'
	}, {
		name: 'Uruguay',
		code: 'UY'
	}, {
		name: 'Uzbekistan',
		code: 'UZ'
	}, {
		name: 'Vanuatu',
		code: 'VU'
	}, {
		name: 'Venezuela',
		code: 'VE'
	}, {
		name: 'Viet Nam',
		code: 'VN'
	}, {
		name: 'Virgin Islands, British',
		code: 'VG'
	}, {
		name: 'Virgin Islands, U.S.',
		code: 'VI'
	}, {
		name: 'Wallis and Futuna',
		code: 'WF'
	}, {
		name: 'Western Sahara',
		code: 'EH'
	}, {
		name: 'Yemen',
		code: 'YE'
	}, {
		name: 'Zambia',
		code: 'ZM'
	}, {
		name: 'Zimbabwe',
		code: 'ZW'
	}]
});
Ext.define('RS.formbuilder.viewer.desktop.fields.ClearableCombobox', {
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.clearablecombobox',

	//Clear Trigger
	clearCls: Ext.baseCSSPrefix + 'form-clear-trigger',
	clearClick: function() {
		//clear the current value and hide the clear button
		this.setValue('');
		this.triggerEl.item(0).parent().setDisplayed('none');
	},

	initComponent: function() {
		this.trigger2Cls = this.triggerCls;
		this.onTrigger2Click = this.onTriggerClick;
		this.trigger1Cls = this.clearCls;
		this.onTrigger1Click = this.clearClick;

		this.callParent();
	},

	onRender: function() {
		this.callParent(arguments);
		if (this.value && !this.readOnly) this.triggerEl.item(0).parent().setDisplayed('table-cell')
		else this.triggerEl.item(0).parent().setDisplayed('none');
	},

	setValue: function(value) {
		this.callParent(arguments);
		if (this.rendered && this.value && !this.mandatory && !this.readOnly) {
			this.triggerEl.item(0).parent().setDisplayed('table-cell');
			this.doComponentLayout();
		}
	}
});
Ext.define('RS.formbuilder.viewer.desktop.fields.ColorPickerCombo', {
	extend: 'Ext.form.field.Trigger',
	alias: 'widget.colorpickercombo',
	onTriggerClick: function() {
		var me = this;
		if (!me.picker)
			me.picker = Ext.create('Ext.picker.Color', {
				pickerField: this,
				ownerCt: this,
				renderTo: document.body,
				floating: true,
				// hidden: true,
				focusOnShow: true,
				style: {
					backgroundColor: "#fff"
				},
				listeners: {
					scope: this,
					select: function(field, value, opts) {
						me.setValue('#' + value);
						me.inputEl.setStyle({
							backgroundColor: value
						});
						me.picker.hide();
						me.picker.destroy();
						me.picker = null;
					},
					show: function(field, opts) {
						field.getEl().monitorMouseLeave(500, field.hide, field);
					}
				}
			})
		me.picker.alignTo(me.inputEl, 'tl-bl?');
		me.picker.show(me.inputEl);
	}
});

glu.regAdapter('colorpickercombo', {
	extend: 'combo'
});
Ext.define('RS.formbuilder.viewer.desktop.fields.FormPickerField', {
	extend: 'Ext.form.field.Trigger',
	alias: 'widget.formpickerfield',

	trigger1Cls: 'x-form-search-trigger',
	trigger2Cls: 'rs-btn-edit',
	trigger3Cls: 'icon-plus',

	allowCreate: false,
	editable: false,

	initComponent: function() {
		var me = this;
		me.callParent()
		me.on('render', function() {
			if (!me.allowCreate)
				me.triggerEl.item(2).parent().setDisplayed('none')
			me.triggerEl.item(2).addCls('x-icon-over icon-large rs-icon')
			me.triggerEl.item(2).parent().setStyle({
				'padding-top': '3px'
			})
			if (me.getValue()) me.triggerEl.item(1).parent().setDisplayed('table-cell')
			else me.triggerEl.item(1).parent().setDisplayed('none')
			me.triggerEl.item(1).addCls('x-icon-over')
			me.inputEl.on('click', function() {
				me.fireEvent('choose')
			})
		})
	},

	setValue: function(value) {
		this.callParent(arguments)
		if (this.rendered) {
			if (value) this.triggerEl.item(1).parent().setDisplayed('table-cell')
			else this.triggerEl.item(1).parent().setDisplayed('none')
		}
	},

	onTrigger1Click: function() {
		this.fireEvent('choose', this)
	},

	onTrigger2Click: function() {
		this.fireEvent('edit', this)
	},

	onTrigger3Click: function() {
		this.fireEvent('create', this)
	}
})
/**
 * InputTextMask script used for mask/regexp operations.
 * Mask Individual Character Usage:
 * 9 - designates only numeric values
 * L - designates only uppercase letter values
 * l - designates only lowercase letter values
 * A - designates only alphanumeric values
 * X - denotes that a custom client script regular expression is specified</li>
 * All other characters are assumed to be "special" characters used to mask the input component.
 * Example 1:
 * (999)999-9999 only numeric values can be entered where the the character
 * position value is 9. Parenthesis and dash are non-editable/mask characters.
 * Example 2:
 * 99L-ll-X[^A-C]X only numeric values for the first two characters,
 * uppercase values for the third character, lowercase letters for the
 * fifth/sixth characters, and the last character X[^A-C]X together counts
 * as the eighth character regular expression that would allow all characters
 * but "A", "B", and "C". Dashes outside the regular expression are non-editable/mask characters.
 * @constructor
 * @param (String) mask The InputTextMask
 * @param (boolean) clearWhenInvalid True to clear the mask when the field blurs and the text is invalid. Optional, default is true.
 */

Ext.define('RS.formbuilder.viewer.desktop.InputTextMask', {
	extend: 'Ext.AbstractPlugin',
	alias: 'ptype.inputtextmask',

	constructor: function(mask, clearWhenInvalid, offset) {
		this.maskOffset = offset || 0;
		if (clearWhenInvalid === undefined) this.clearWhenInvalid = true;
		else this.clearWhenInvalid = clearWhenInvalid;
		this.rawMask = mask;
		this.viewMask = '';
		this.maskArray = new Array();
		var mai = 0;
		var regexp = '';
		for (var i = 0; i < mask.length; i++) {
			if (regexp) {
				if (regexp == 'X') {
					regexp = '';
				}
				if (mask.charAt(i) == 'X') {
					this.maskArray[mai] = regexp;
					mai++;
					regexp = '';
				} else {
					regexp += mask.charAt(i);
				}
			} else if (mask.charAt(i) == 'X') {
				regexp += 'X';
				this.viewMask += '_';
			} else if (mask.charAt(i) == '9' || mask.charAt(i) == 'L' || mask.charAt(i) == 'l' || mask.charAt(i) == 'A') {
				this.viewMask += '_';
				this.maskArray[mai] = mask.charAt(i);
				mai++;
			} else {
				this.viewMask += mask.charAt(i);
				this.maskArray[mai] = RegExp.escape(mask.charAt(i));
				mai++;
			}
		}

		this.specialChars = this.viewMask.replace(/(L|l|9|A|_|X)/g, '');
		return this;
	},

	init: function(field) {
		this.field = field;

		if (field.rendered) {
			this.assignEl();
		} else {
			field.on('render', this.assignEl, this);
		}

		field.on('blur', this.removeValueWhenInvalid, this);
		field.on('focus', this.processMaskFocus, this);
	},

	assignEl: function() {
		this.inputTextElement = this.field.inputEl.dom;
		this.field.inputEl.on('keypress', this.processKeyPress, this);
		this.field.inputEl.on('keydown', this.processKeyDown, this);
		if (Ext.isSafari || Ext.isIE) {
			this.field.inputEl.on('paste', this.startTask, this);
			this.field.inputEl.on('cut', this.startTask, this);
		}
		if (Ext.isGecko || Ext.isOpera) {
			this.field.inputEl.on('mousedown', this.setPreviousValue, this);
		}
		if (Ext.isGecko) {
			this.field.inputEl.on('input', this.onInput, this);
		}
		if (Ext.isOpera) {
			this.field.inputEl.on('input', this.onInputOpera, this);
		}
	},
	onInput: function() {
		this.startTask(false);
	},
	onInputOpera: function() {
		if (!this.prevValueOpera) {
			this.startTask(false);
		} else {
			this.manageBackspaceAndDeleteOpera();
		}
	},

	manageBackspaceAndDeleteOpera: function() {
		this.inputTextElement.value = this.prevValueOpera.cursorPos.previousValue;
		this.manageTheText(this.prevValueOpera.keycode, this.prevValueOpera.cursorPos);
		this.prevValueOpera = null;
	},

	setPreviousValue: function(event) {
		this.oldCursorPos = this.getCursorPosition();
	},

	getValidatedKey: function(keycode, cursorPosition) {
		var maskKey = this.maskArray[cursorPosition.start];
		if (maskKey == '9') {
			return keycode.pressedKey.match(/[0-9]/);
		} else if (maskKey == 'L') {
			return (keycode.pressedKey.match(/[A-Za-z]/)) ? keycode.pressedKey.toUpperCase() : null;
		} else if (maskKey == 'l') {
			return (keycode.pressedKey.match(/[A-Za-z]/)) ? keycode.pressedKey.toLowerCase() : null;
		} else if (maskKey == 'A') {
			return keycode.pressedKey.match(/[A-Za-z0-9]/);
		} else if (maskKey) {
			return (keycode.pressedKey.match(new RegExp(maskKey)));
		}
		return (null);
	},

	removeValueWhenInvalid: function() {
		if (this.clearWhenInvalid && this.inputTextElement.value.indexOf('_') > -1) {
			this.inputTextElement.value = '';
		}
	},

	managePaste: function() {
		if (this.oldCursorPos == null) {
			return;
		}
		var valuePasted = this.inputTextElement.value.substring(this.oldCursorPos.start, this.inputTextElement.value.length - (this.oldCursorPos.previousValue.length - this.oldCursorPos.end));
		if (this.oldCursorPos.start < this.oldCursorPos.end) {
			this.oldCursorPos.previousValue = this.oldCursorPos.previousValue.substring(0, this.oldCursorPos.start) + this.viewMask.substring(this.oldCursorPos.start, this.oldCursorPos.end) + this.oldCursorPos.previousValue.substring(this.oldCursorPos.end, this.oldCursorPos.previousValue.length);
			valuePasted = valuePasted.substr(0, this.oldCursorPos.end - this.oldCursorPos.start);
		}
		this.inputTextElement.value = this.oldCursorPos.previousValue;
		keycode = {
			unicode: '',
			isShiftPressed: false,
			isTab: false,
			isBackspace: false,
			isLeftOrRightArrow: false,
			isDelete: false,
			pressedKey: ''
		}
		var charOk = false;
		for (var i = 0; i < valuePasted.length; i++) {
			keycode.pressedKey = valuePasted.substr(i, 1);
			keycode.unicode = valuePasted.charCodeAt(i);
			this.oldCursorPos = this.skipMaskCharacters(keycode, this.oldCursorPos);
			if (this.oldCursorPos === false) {
				break;
			}
			if (this.injectValue(keycode, this.oldCursorPos)) {
				charOk = true;
				this.moveCursorToPosition(keycode, this.oldCursorPos);
				this.oldCursorPos.previousValue = this.inputTextElement.value;
				this.oldCursorPos.start = this.oldCursorPos.start + 1;
			}
		}
		if (!charOk && this.oldCursorPos !== false) {
			this.moveCursorToPosition(null, this.oldCursorPos);
		}
		this.oldCursorPos = null;
	},

	processKeyDown: function(e) {
		this.processMaskFormatting(e, 'keydown');
	},

	processKeyPress: function(e) {
		this.processMaskFormatting(e, 'keypress');
	},

	startTask: function(setOldCursor) {
		if (this.task == undefined) {
			this.task = new Ext.util.DelayedTask(this.managePaste, this);
		}
		if (setOldCursor !== false) {
			this.oldCursorPos = this.getCursorPosition();
		}
		this.task.delay(0);
	},

	skipMaskCharacters: function(keycode, cursorPos) {
		if (cursorPos.start != cursorPos.end && (keycode.isDelete || keycode.isBackspace)) return (cursorPos);
		while (this.specialChars.match(RegExp.escape(this.viewMask.charAt(((keycode.isBackspace) ? cursorPos.start - 1 : cursorPos.start))))) {
			if (keycode.isBackspace) {
				cursorPos.dec();
			} else {
				cursorPos.inc();
			}
			if (cursorPos.start >= cursorPos.previousValue.length || cursorPos.start < 0) {
				return false;
			}
		}
		return (cursorPos);
	},

	isManagedByKeyDown: function(keycode) {
		if (keycode.isDelete || keycode.isBackspace) {
			return (true);
		}
		return (false);
	},

	processMaskFormatting: function(e, type) {
		this.oldCursorPos = null;
		var cursorPos = this.getCursorPosition();
		var keycode = this.getKeyCode(e, type);
		if (keycode.unicode == 0) { //?? sometimes on Safari
			return;
		}
		if ((keycode.unicode == 67 || keycode.unicode == 99) && e.ctrlKey) { //Ctrl+c, let's the browser manage it!
			return;
		}
		if ((keycode.unicode == 88 || keycode.unicode == 120) && e.ctrlKey) { //Ctrl+x, manage paste
			this.startTask();
			return;
		}
		if ((keycode.unicode == 86 || keycode.unicode == 118) && e.ctrlKey) { //Ctrl+v, manage paste....
			this.startTask();
			return;
		}
		if ((keycode.isBackspace || keycode.isDelete) && Ext.isOpera) {
			this.prevValueOpera = {
				cursorPos: cursorPos,
				keycode: keycode
			};
			return;
		}
		if (type == 'keydown' && !this.isManagedByKeyDown(keycode)) {
			return true;
		}
		if (type == 'keypress' && this.isManagedByKeyDown(keycode)) {
			return true;
		}
		if (this.handleEventBubble(e, keycode, type)) {
			return true;
		}
		return (this.manageTheText(keycode, cursorPos));
	},

	manageTheText: function(keycode, cursorPos) {
		if (this.inputTextElement.value.length === 0) {
			this.inputTextElement.value = this.viewMask;
		}
		cursorPos = this.skipMaskCharacters(keycode, cursorPos);
		if (cursorPos === false) {
			return false;
		}
		if (this.injectValue(keycode, cursorPos)) {
			this.moveCursorToPosition(keycode, cursorPos);
		}
		return (false);
	},

	processMaskFocus: function() {
		if (this.inputTextElement.value.length == 0) {
			var self = this
			var cursorPos = this.getCursorPosition();
			this.inputTextElement.value = this.viewMask;
			setTimeout(function() {
				self.moveCursorToPosition(null, cursorPos);
			}, 1)
		}
	},

	isManagedByBrowser: function(keyEvent, keycode, type) {
		if (((type == 'keypress' && keyEvent.charCode === 0) || type == 'keydown') && (keycode.unicode == Ext.EventObject.TAB || keycode.unicode == Ext.EventObject.RETURN || keycode.unicode == Ext.EventObject.ENTER || keycode.unicode == Ext.EventObject.SHIFT || keycode.unicode == Ext.EventObject.CONTROL || keycode.unicode == Ext.EventObject.ESC || keycode.unicode == Ext.EventObject.PAGEUP || keycode.unicode == Ext.EventObject.PAGEDOWN || keycode.unicode == Ext.EventObject.END || keycode.unicode == Ext.EventObject.HOME || keycode.unicode == Ext.EventObject.LEFT || keycode.unicode == Ext.EventObject.UP || keycode.unicode == Ext.EventObject.RIGHT || keycode.unicode == Ext.EventObject.DOWN)) {
			return (true);
		}
		return (false);
	},

	handleEventBubble: function(keyEvent, keycode, type) {
		try {
			if (keycode && this.isManagedByBrowser(keyEvent, keycode, type)) {
				return true;
			}
			keyEvent.stopEvent();
			return false;
		} catch (e) {
			alert(e.message);
		}
	},

	getCursorPosition: function() {
		var s, e, r;
		if (this.inputTextElement.createTextRange) {
			r = document.selection.createRange().duplicate();
			r.moveEnd('character', this.inputTextElement.value.length);
			if (r.text === '') {
				s = this.inputTextElement.value.length;
			} else {
				s = this.inputTextElement.value.lastIndexOf(r.text);
			}
			r = document.selection.createRange().duplicate();
			r.moveStart('character', -this.inputTextElement.value.length);
			e = r.text.length;
		} else {
			s = this.inputTextElement.selectionStart;
			e = this.inputTextElement.selectionEnd;
		}

		if (s === 0 && e === 0) {
			s = s + this.maskOffset
			e = e + +this.maskOffset
		}
		return this.CursorPosition(s, e, r, this.inputTextElement.value);
	},

	moveCursorToPosition: function(keycode, cursorPosition) {
		var p = (!keycode || (keycode && keycode.isBackspace)) ? cursorPosition.start : cursorPosition.start + 1;
		if (this.inputTextElement.createTextRange) {
			cursorPosition.range.move('character', p);
			cursorPosition.range.select();
		} else {
			this.inputTextElement.selectionStart = p;
			this.inputTextElement.selectionEnd = p;
		}
	},

	injectValue: function(keycode, cursorPosition) {
		if (!keycode.isDelete && keycode.unicode == cursorPosition.previousValue.charCodeAt(cursorPosition.start)) return true;
		var key;
		if (!keycode.isDelete && !keycode.isBackspace) {
			key = this.getValidatedKey(keycode, cursorPosition);
		} else {
			if (cursorPosition.start == cursorPosition.end) {
				key = '_';
				if (keycode.isBackspace) {
					cursorPosition.dec();
				}
			} else {
				key = this.viewMask.substring(cursorPosition.start, cursorPosition.end);
			}
		}
		if (key) {
			this.inputTextElement.value = cursorPosition.previousValue.substring(0, cursorPosition.start) + key + cursorPosition.previousValue.substring(cursorPosition.start + key.length, cursorPosition.previousValue.length);
			return true;
		}
		return false;
	},

	getKeyCode: function(onKeyDownEvent, type) {
		var keycode = {};
		keycode.unicode = onKeyDownEvent.getKey();
		keycode.isShiftPressed = onKeyDownEvent.shiftKey;

		keycode.isDelete = ((onKeyDownEvent.getKey() == Ext.EventObject.DELETE && type == 'keydown') || (type == 'keypress' && onKeyDownEvent.charCode === 0 && onKeyDownEvent.keyCode == Ext.EventObject.DELETE)) ? true : false;
		keycode.isTab = (onKeyDownEvent.getKey() == Ext.EventObject.TAB) ? true : false;
		keycode.isBackspace = (onKeyDownEvent.getKey() == Ext.EventObject.BACKSPACE) ? true : false;
		keycode.isLeftOrRightArrow = (onKeyDownEvent.getKey() == Ext.EventObject.LEFT || onKeyDownEvent.getKey() == Ext.EventObject.RIGHT) ? true : false;
		keycode.pressedKey = String.fromCharCode(keycode.unicode);
		return (keycode);
	},

	CursorPosition: function(start, end, range, previousValue) {
		var cursorPosition = {};
		cursorPosition.start = isNaN(start) ? 0 : start;
		cursorPosition.end = isNaN(end) ? 0 : end;
		cursorPosition.range = range;
		cursorPosition.previousValue = previousValue;
		cursorPosition.inc = function() {
			cursorPosition.start++;
			cursorPosition.end++;
		};
		cursorPosition.dec = function() {
			cursorPosition.start--;
			cursorPosition.end--;
		};
		return (cursorPosition);
	}
});

Ext.applyIf(RegExp, {
	escape: function(str) {
		return new String(str).replace(/([.*+?^=!:${}()|[\]\/\\])/g, '\\$1');
	}
});
/**
 *
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.JournalField', {
	extend: 'Ext.form.FieldContainer',
	alias: 'widget.journalfield',

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	journalSeparator: '|&|',

	userId: '',
	journalEntries: [],

	allowJournalEdit: false,

	disabled: false,

	resizable: true,
	resizeHandles: 's',

	initComponent: function() {
		var me = this;
		me.value = me.value || [];
		if (Ext.isString(me.value)) me.value = Ext.decode(me.value);

		me.height = me.height || 300; //to ensure the grid is displayed
		me.journalEntries = me.value;

		me.journalStore = Ext.create('Ext.data.Store', {
			fields: ['userId', {
				name: 'createDate',
				type: 'date',
				format: 'c',
				dateFormat: 'c'
			}, 'note'],
			data: me.value,
			proxy: {
				type: 'memory',
				reader: {
					type: 'json'
				}
			}
		});

		var editor = Ext.create('Ext.grid.plugin.CellEditing', {});

		if (!Ext.isArray(me.items)) me.items = [{
			xtype: 'hiddenfield',
			name: me.name,
			getValue: function() {
				var temp = me.ownerCt.journalEntries.slice(0),  // me.ownerCt.journalEntries is immutable
					newVal = me.ownerCt.down('textarea').getValue();
				if (newVal) {
					temp.splice(0, 0, {
						userId: me.ownerCt.userId,
						createDate: Ext.Date.format(new Date(), 'c'),
						note: Ext.htmlEncode(newVal).replace(/\n/g, '<br/>')
					});
				}
				return Ext.encode(temp);
			}
		}];

		var journalPlugins = [];
		if (this.allowJournalEdit) journalPlugins.push(editor)

		me.items = me.items.concat([{
			xtype: 'textarea',
			readOnly: me.readOnly,
			hidden: me.readOnly
		}, {
			xtype: 'grid',
			flex: 1,
			plugins: journalPlugins,
			columns: [{
				header: 'Note',
				dataIndex: 'note',
				flex: 1,
				editor: {}
			}, {
				header: 'User',
				width: 100,
				dataIndex: 'userId'
			}, {
				header: 'Date',
				dataIndex: 'createDate',
				width: 200,
				renderer: Ext.util.Format.dateRenderer('D, M d, Y g:i:s A')
			}, {
				xtype: 'actioncolumn',
				hidden: !this.allowJournalEdit,
				hideable: false,
				width: 40,
				items: [{
					icon: '/resolve/images/edittsk_tsk.gif',
					// Use a URL in the icon config
					tooltip: 'Edit',
					handler: function(grid, rowIndex, colIndex) {
						if (journalPlugins.length > 0)
							editor.startEditByPosition({
								row: rowIndex,
								column: 0
							});
					}
				}, {
					icon: '/resolve/images/closex.gif',
					tooltip: 'Delete',
					scope: me,
					handler: function(grid, rowIndex, colIndex) {
						grid.lastRowIndex = rowIndex;
						Ext.MessageBox.show({
							title: 'Confirm Delete',
							msg: 'Are you sure you want to delete the selected journal entry?',
							buttons: Ext.MessageBox.YESNO,
							buttonText: {
								yes: 'Delete',
								no: 'Cancel'
							},
							scope: grid,
							fn: me.removeJournalEntry
						});
					}
				}]
			}],
			listeners: {
				edit: function() {
					me.storeJournalEntries(this);
				}
			},
			store: me.journalStore
		}]);

		me.callParent();
	},
	journalEntryChanged: function(field, newValue, oldValue, eOpts) {
		//serialize the current value for the field into the hidden field
		var temp = [{
			userId: this.userId,
			createDate: Ext.Date.format(new Date(), 'c'),
			note: Ext.htmlEncode(newValue).replace(/\n/g, '<br/>')
		}];
		temp = temp.concat(this.journalEntries);
		this.down('hiddenfield').setValue(Ext.encode(temp));
	},
	removeJournalEntry: function(btn) {
		if (btn === 'yes') {
			this.getStore().removeAt(this.lastRowIndex);
			this.up('fieldcontainer').storeJournalEntries(this);
		}
	},
	storeJournalEntries: function(grid) {
		var records = [];
		grid.getStore().each(function(record) {
			records.push({
				userId: record.get('userId'),
				createDate: record.get('createDate'),
				note: record.get('note')
			});
		});
		this.journalEntries = records;
		var textarea = this.down('textarea'),
			val = textarea.getValue();
		if (val) {
			this.journalEntryChanged(textarea, val);
		}
	},
	setValue: function(value) {
		if (value && Ext.isString(value)) {
			var journals = Ext.decode(value);
			if (Ext.isArray(journals)) {
				this.down('textareafield').setValue('');
				this.journalEntries = journals;
				this.journalStore.removeAll();
				this.journalStore.add(journals);
			}
		}
	},
	setFieldStyle: function(style) {
		var fields = this.query('field');
		Ext.each(fields, function(field) {
			if (field.setFieldStyle) field.setFieldStyle(style);
		});
	}
});
/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.Link', {
	extend: 'Ext.form.Label',
	alias: 'widget.linkfield',

	padding: '10px 0px 10px 0px',
	style: {
		display: 'block'
	},

	initComponent: function() {
		this.callParent();
		this.setValue(this.value || this.defaultValue);

		var me = this;
		this.on('afterrender', function() {
			me.setDisabled(me.disabled)
		})
	},

	setValue: function(value) {
		var link = this.generateLink(value);
		this.setText(link, false)
	},

	generateLink: function(value) {
		//parse the link and return it
		if (!value) return '';

		//We have a document link and we need to look that information up
		Ext.Ajax.request({
			url: '/resolve/service/form/getLink',
			params: {
				wikiLink: value,
				target: this.linkTarget
			},
			scope: this,
			success: this.processServerResponse,
			failure: function(r){
				clientVM.displayFailure(r);
			}
		});
		return value;
	},

	processServerResponse: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			var link = response.data;
			if (this.linkTarget && link.indexOf('target') <= 0) {
				var hrefIndex = link.indexOf('href'),
					linkArray = link.split('');
				linkArray.splice(hrefIndex, 0, Ext.String.format(' target="{0}" ', this.linkTarget));
				link = linkArray.join('');
			}
			if (this.rendered) this.setText(link, false)
			else this.html = link
			// check HTML anchor href links
			var anchors = window.document.querySelectorAll('a');
			for (var i=0; i < anchors.length; i++) {
				var element = anchors[i];
				var link = element.getAttribute('href');
				if (link) {
					clientVM.injectCSRFPageTokenToElement(link, element, 'href');
				}
			}
			this.setDisabled(this.disabled)
		} else {
			clientVM.displayError(response.message);
		}
	},

	setDisabled: function(disabled) {
		this.disabled = disabled;
		if (!this.rendered) return;
		var links = this.getEl().query('a');
		Ext.each(links, function(link) {
			if (disabled) {
				this.oldLink = Ext.fly(link).dom.href;
				Ext.fly(link).dom.href = 'javascript:void(0)';
				Ext.fly(link).setStyle({
					color: 'grey'
				});
			} else {
				if (this.oldLink) Ext.fly(link).dom.href = this.oldLink;
				Ext.fly(link).setStyle({
					color: 'blue'
				});
			}
		}, this)
	}
});
/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.ListField', {
	extend: 'Ext.container.Container',
	alias: 'widget.listfield',

	layout: 'fit',
	border: false,
	choiceSeparator: '|&|',
	choiceValueSeparator: '|=|',

	initComponent: function() {
		var me = this;

		if (!Ext.isArray(this.items)) this.items = [{
			xtype: 'hidden',
			name: this.name
		}];

		this.items = [{
			xtype: 'grid',
			forceFit: true,
			title: this.fieldLabel,
			disableSelection: this.readOnly,
			displayField: 'name',
			valueField: 'value',
			store: Ext.create('Ext.data.Store', {
				data: this.parseOptions(this.choiceValues),
				fields: [{
					name: 'name'
				}, {
					name: 'value'
				}]
			}),
			plugins: [Ext.create('Ext.grid.plugin.CellEditing', {
				pluginId: 'cellEditor',
				listeners: {
					scope: this,
					edit: this.listChanged
				}
			})],
			hideHeaders: true,
			columns: [{
				dataIndex: 'name',
				editor: {}
			}],
			selModel: {
				mode: 'MULTI'
			},
			listeners: {
				selectionchange: {
					scope: this,
					fn: this.listChanged
				}
			},
			tbar: [{
				text: RS.formbuilder.locale.add,
				disabled: this.readOnly,
				scope: this,
				handler: function(button) {
					var grid = this.down('grid'),
						editor = grid.getPlugin('cellEditor');
					grid.store.add({
						name: 'NEW'
					});
					//Automatically start editing the newly added record
					if (editor) {
						editor.startEditByPosition({
							row: grid.store.getCount() - 1,
							column: 0
						});
					}
				}
			}, {
				text: RS.formbuilder.locale.remove,
				itemId: 'removeButton',
				disabled: true,
				scope: this,
				handler: function(button) {
					var grid = this.down('grid'),
						values = grid.getSelectionModel().getSelection(),
						i = 0;
					Ext.each(values, function(value) {
						grid.store.remove(value);
					});
				}
			}]
		}].concat(this.items)

		this.callParent()

		this.on('afterrender', function() {
			me.initializeValues()
		}, this, {
			single: true
		})

		var hidden = this.down('hiddenfield')
		if (hidden) {
			hidden.listSetValue = hidden.setValue
			hidden.setValue = function(value, internal) {
				me.value = value
				if (!internal) me.initializeValues()
				hidden.listSetValue(value)
			}
		}
	},
	initializeValues: function() {
		var values = this.parseValue(this.value || this.defaultValue || '');
		var grid = this.down('grid');
		if (grid) {
			var store = grid.store,
				sm = grid.getSelectionModel();
			sm.deselectAll()
			Ext.each(values, function(value) {
				var index = store.indexOf(store.findRecord('name', value, 0, false, false, true))
				if (index == -1) {
					store.add({
						name: value,
						value: value
					})
					sm.select(store.getCount() - 1, true)
				} else
					sm.select(index, true)
			}, this)
		}
	},
	listChanged: function(model, selected, eOpts) {
		if (!Ext.isArray(selected)) selected = this.getGrid().getSelectionModel().getSelection()
		if (selected.length > 0) {
			this.down('#removeButton').enable()
		} else {
			this.down('#removeButton').disable()
		}
		var selectedNames = [];
		Ext.Array.forEach(selected, function(select) {
			selectedNames.push(select.data.name);
		});

		this.down('hiddenfield').setValue(selectedNames.join(this.choiceSeparator), true)
	},
	getGrid: function() {
		return this.down('grid')
	},
	setFieldStyle: function(style) {
		var fields = this.query('field');
		Ext.each(fields, function(field) {
			if (field.setFieldStyle) field.setFieldStyle(style);
		});
	}
});
/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.NameField', {
	extend: 'Ext.form.FieldContainer',
	alias: 'widget.namefield',

	layout: 'hbox',
	nameSeparator: '|&|',

	initComponent: function() {
		var fieldName = this.name,
			values = this.value ? this.value.split(this.nameSeparator) : [],
			showings = this.selectValues ? this.selectValues.split(this.nameSeparator) : [true, true, false, true],
			i = 0;
		for(; i < showings.length; i++) {
			if(Ext.isString(showings[i])) showings[i] = showings[i] === 'true'
		}

		if(!Ext.isArray(this.items)) this.items = [{
			xtype: 'hiddenfield',
			name: this.name
		}];
		this.fieldLabel =  Ext.String.htmlEncode(this.fieldLabel || '');
		this.items = [{
			xtype: 'textfield',
			flex: 1,
			hideLabel: true,
			allowBlank: !this.mandatory || this.readOnly,
			readOnly: this.readOnly,
			fieldCls: this.fieldCls,
			value: values[0] || '',
			hidden: showings[0],
			disabled: showings[0],
			listeners: {
				change: {
					scope: this,
					fn: this.nameChanged
				}
			}
		}, {
			xtype: 'textfield',
			flex: 1,
			hideLabel: true,
			readOnly: this.readOnly,
			fieldCls: this.fieldCls,
			allowBlank: !this.mandatory || this.readOnly,
			margins: '0 0 0 5',
			value: values[1] || '',
			hidden: showings[1],
			disabled: showings[1],
			listeners: {
				change: {
					scope: this,
					fn: this.nameChanged
				}
			}
		}, {
			xtype: 'textfield',
			width: 30,
			hideLabel: true,
			readOnly: this.readOnly,
			fieldCls: this.fieldCls,
			allowBlank: !this.mandatory || this.readOnly,
			margins: '0 0 0 5',
			value: values[2] || '',
			hidden: showings[2],
			disabled: showings[2],
			listeners: {
				change: {
					scope: this,
					fn: this.nameChanged
				}
			}
		}, {
			xtype: 'textfield',
			flex: 2,
			hideLabel: true,
			readOnly: this.readOnly,
			fieldCls: this.fieldCls,
			allowBlank: !this.mandatory || this.readOnly,
			margins: '0 0 0 5',
			value: values[3] || '',
			hidden: showings[3],
			disabled: showings[3],
			listeners: {
				change: {
					scope: this,
					fn: this.nameChanged
				}
			}
		}].concat(this.items);

		this.callParent();
	},
	nameChanged: function() {
		var name = [];
		this.items.each(function(part) {
			if(part.xtype !== 'hiddenfield') name.push(part.getValue());
		});
		this.items.getAt(4).setValue(name.join(this.nameSeparator));
	},
	setFieldStyle: function(style){
		var fields = this.query('field');
		Ext.each(fields, function(field){
			if( field.setFieldStyle)field.setFieldStyle(style);
		});
	}
});
/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.ReferenceField', {
	extend: 'Ext.form.field.Trigger',
	alias: 'widget.referencefield',

	initComponent: function() {
		this.editable = false;
		this.callParent();
	},
	onRender: function() {
		//call into the server to get information about the columns that are going to come back from a particular table
		Ext.Ajax.request({
			url: '/resolve/service/form/getcustomtablecolumns',
			params: {
				tableName: this.referenceTable
			},
			scope: this,
			success: this.processColumns,
			failure: function(r){
				clientVM.displayFailure(r);
			}
		});
		this.callParent(arguments);
		this.setShowClearTrigger(false);

		var name = this.name || this.el.dom.name;
		// this.el.dom.removeAttribute('name');
		this.hiddenField = this.el.insertSibling({
			tag: 'input',
			type: 'hidden',
			name: name
		});
		this.hiddenName = name;
		// otherwise field is not found by BasicForm::findField
		if (this.value) {
			this.setShowClearTrigger(true);
			Ext.Ajax.request({
				url: '/resolve/service/dbapi/getRecordData',
				params: {
					tableName: this.referenceTable,
					type: 'form',
					start: 0,
					filter: Ext.encode([{
						field: 'sys_id',
						type: 'auto',
						condition: 'equals',
						value: this.value
					}])
				},
				scope: this,
				success: this.processValue,
				failure: function(r){
					clientVM.displayFailure(r);
				}
			})
		}

		//setup tooltips
		Ext.create('Ext.tip.ToolTip', {
			target: this.triggerEl.item(0),
			html: RS.formbuilder.locale.refrenceClear
		});
		Ext.create('Ext.tip.ToolTip', {
			target: this.triggerEl.item(1),
			html: RS.formbuilder.locale.referenceJump
		});
		Ext.create('Ext.tip.ToolTip', {
			target: this.triggerEl.item(2),
			html: RS.formbuilder.locale.referenceSearch
		});

	},

	processColumns: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			if (Ext.isArray(response.records)) {
				this.gridColumns = [];
				this.storeFields = [];
				var storeType = '';
				Ext.each(response.records, function(record) {
					this.gridColumns.push({
						header: record.displayName,
						dataIndex: record.columnModelName,
						filter: true
					});

					switch (record.type) {
						case 'boolean':
							storeType = 'boolean';
							break;

						case 'timestamp':
							storeType = 'date';
							break;

						case 'integer':
						case 'long':
						case 'float':
						case 'double':
							storeType = 'numeric';
							break;

						default:
							storeType = 'string';
							break;
					}
					this.storeFields.push({
						name: record.columnModelName,
						type: storeType,
						dateFormat: 'c'
					});
				}, this);

				this.gridStore = Ext.create('Ext.data.Store', {
					fields: this.storeFields,
					remoteSort: true,
					remoteFilter: true,
					proxy: {
						type: 'ajax',
						url: '/resolve/service/dbapi/getRecordData',
						extraParams: {
							tableName: this.referenceTable,
							type: 'table'
						},
						reader: {
							type: 'json',
							root: 'records'
						},
						listeners: {
							exception: function(e, resp, op) {
								clientVM.displayExceptionError(e, resp, op);
							}
						}
					}
				});
				if (this.referenceTable) this.gridStore.load();
			}
			//go through the records and populate the grid columns and types for the grid store
		} else clientVM.displayError(response.message);
	},

	processValue: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			if (response.data) {
				this.selectReferenceRecord(response);
			}
		} else clientVM.displayError(response.message);
	},

	setValue: function(value, process) {
		this.callParent(arguments);
		if (value && process !== false) {
			this.setShowClearTrigger(true);
			Ext.Ajax.request({
				url: '/resolve/service/dbapi/getRecordData',
				params: {
					tableName: this.referenceTable,
					type: 'form',
					start: 0,
					filter: Ext.encode([{
						field: 'sys_id',
						type: 'auto',
						condition: 'equals',
						value: value
					}])
				},
				scope: this,
				success: this.processValue,
				failure: function(r){
					clientVM.displayFailure(r);
				}
			})
		}

	},

	//Search Trigger
	trigger3Cls: Ext.baseCSSPrefix + 'form-search-trigger',
	onTrigger3Click: function() {
		var me = this;
		//open search dialog window
		var selectionWindow = Ext.create('Ext.window.Window', {
			modal: true,
			width: 600,
			height: 400,
			layout: 'fit',
			title: RS.formbuilder.locale.SelectRecordTitle,
			items: [{
				xtype: 'grid',
				forceFit: false,
				columns: this.gridColumns,
				store: this.gridStore,
				bbar: Ext.create('Ext.PagingToolbar', {
					store: this.gridStore,
					displayInfo: true,
					displayMsg: 'Displaying records {0} - {1} of {2}',
					emptyMsg: "No records to display"
				}),
				listeners: {
					selectionchange: function(grid, selected, eOpts) {
						var selectButton = selectionWindow.down('#selectButton');
						if (selected.length == 1) selectButton.enable()
						else selectButton.disable()
					},
					itemdblclick: function(selectionModel, record, item, index, e, eOpts) {
						me.selectReferenceRecord(record)
						selectionWindow.close()
					}
				}
			}],
			buttonAlign: 'left',
			buttons: [{
				text: RS.formbuilder.locale.select,
				itemId: 'selectButton',
				disabled: true,
				handler: function(button) {
					var win = button.up('window'),
						grid = win.down('grid');
					me.selectReferenceRecord(grid.getSelectionModel().getSelection()[0]);
					win.close();
				}
			}, {
				text: RS.formbuilder.locale.cancel,
				handler: function(button) {
					button.up('window').close();
				}
			}]
		});

		selectionWindow.show();
	},

	selectReferenceRecord: function(record) {
		this.setValue(record.data[this.referenceDisplayColumn], false);
		this.hiddenField.dom.value = record.data.sys_id;
		this.setShowClearTrigger(true);
	},

	//Clear Trigger
	triggerCls: Ext.baseCSSPrefix + 'form-clear-trigger',
	onTriggerClick: function() {
		//clear the current value and hide the clear button
		this.setValue('');
		this.hiddenField.dom.value = '';
		this.setShowClearTrigger(false);
	},

	//Jump Trigger
	trigger2Cls: Ext.baseCSSPrefix + 'form-jump-trigger',
	onTrigger2Click: function() {
		window.open(Ext.String.format(this.referenceTarget, this.hiddenField.dom.value), '_blank');
	},

	setShowClearTrigger: function(display) {
		this.triggerEl.item(0).parent().setDisplayed(display ? 'table-cell' : 'none');
		if (this.referenceTarget) this.triggerEl.item(1).parent().setDisplayed(display ? 'table-cell' : 'none');
		else this.triggerEl.item(1).parent().setDisplayed('none');
		this.doComponentLayout();
	},

	getRawValue: function() {
		return this.rendered ? this.hiddenField.dom.value : '';
	}
});
/**
 *
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.ReferenceTable', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.referencetable',

	layout: 'fit',

	anchor: '-20',
	height: 300,
	border: false,

	resizable: true,
	resizeHandles: 's',
	csrftoken: '',

	initComponent: function() {
		var me = this;

		if (this.referenceTableUrl != '') {
			var url = this.referenceTableUrl.split('?');
			clientVM.getCSRFToken_ForURI(url[0], function(token_pair) {
				this.csrftoken = token_pair[0] + '=' + token_pair[1];
			}.bind(this))
		}

		var displayNames = me.displayName ? me.displayName.split('|') : [],
			referenceTables = me.refGridReferenceByTable ? me.refGridReferenceByTable.split('|') : [],
			refGridReferenceColumnName = me.refGridReferenceColumnName ? me.refGridReferenceColumnName.split('|') : [],
			referenceColumns = me.refGridSelectedReferenceColumns ? me.refGridSelectedReferenceColumns.split('|') : [],
			allowReferenceTableAdd = me.allowReferenceTableAdd ? me.allowReferenceTableAdd.split('|') : [],
			allowReferenceTableRemove = me.allowReferenceTableRemove ? me.allowReferenceTableRemove.split('|') : [],
			allowReferenceTableView = me.allowReferenceTableView ? me.allowReferenceTableView.split('|') : [],
			referenceTableUrl = me.referenceTableUrl ? me.referenceTableUrl.split('|') : [],
			referenceTableViewPopup = me.refGridViewPopup ? me.refGridViewPopup.split('|') : [],
			referenceTableViewPopupWidth = me.refGridViewPopupWidth ? me.refGridViewPopupWidth.split('|') : [],
			referenceTableViewPopupHeight = me.refGridViewPopupHeight ? me.refGridViewPopupHeight.split('|') : [],
			referenceTableViewPopupTitle = me.refGridViewTitle ? me.refGridViewTitle.split('|') : [],
			len = displayNames.length,
			i, tabs = [];

		for (i = 0; i < len; i++) {
			var cols = referenceColumns[i] ? Ext.decode(referenceColumns[i]) : [];
			tabs.push({
				title: displayNames[i],
				referenceTable: referenceTables[i],
				referenceColumn: refGridReferenceColumnName[i],
				referenceColumns: cols,
				allowReferenceTableAdd: allowReferenceTableAdd[i] == 'true',
				allowReferenceTableRemove: allowReferenceTableRemove[i] == 'true',
				allowReferenceTableView: allowReferenceTableView[i] == 'true',
				referenceTableUrl: referenceTableUrl[i],
				referenceTableViewPopup: referenceTableViewPopup[i] == 'true',
				referenceTableViewPopupWidth: Number(referenceTableViewPopupWidth[i]),
				referenceTableViewPopupHeight: Number(referenceTableViewPopupHeight[i]),
				referenceTableViewPopupTitle: referenceTableViewPopupTitle[i]
			});
		}

		//Now render either the tabs or the one grid
		var config = {};
		if (len > 1) {
			config.xtype = 'tabpanel';
			config.plain = true;
			config.items = [];
			Ext.each(tabs, function(tab) {
				config.items.push(this.convertToGrid(tab));
			}, this);
		} else {
			config = this.convertToGrid(tabs[0]);
		}
		me.items = config;

		me.callParent();

		if (this.recordId) me.loadReferences();
	},
	convertToGrid: function(serverConf) {
		var me = this;
		var config = Ext.apply({}, serverConf, {
			xtype: 'grid'
		});
		config.columns = [{
			xtype: 'actioncolumn',
			hideable: false,
			hidden: !serverConf.allowReferenceTableView,
			width: 25,
			items: [{
				icon: '/resolve/images/link.gif',
				// Use a URL in the icon config
				tooltip: RS.formbuilder.locale.viewReference,
				scope: this,
				handler: function(grid, rowIndex, colIndex) {
					var rec = grid.getStore().getAt(rowIndex),
						sys_id = rec.get('sys_id'),
						url = serverConf.referenceTableUrl,
						me = this;
					if (sys_id) {
						//open new window to the url configured for this reference table
						var containsSysIdAlready = url.indexOf('${sys_id}') > -1;

						url = url.replace(/${sys_id}/g, sys_id).replace(/${parent_sys_id}/g, me.recordId);
						var temp = url.match(/\${(.*?)}/g)
						if (temp && temp.length > 0)
							Ext.Array.forEach(temp, function(match) {
								var val = rec.get(match.substring(2, match.length - 1))
								if (val) url = url.replace(match, val)
							})

						if (!containsSysIdAlready) {
							var split = url.split('?'),
								params = {};
							if (split.length > 1) {
								params = Ext.Object.fromQueryString(split[1]);
							}
							url = split[0] + '?' + Ext.urlEncode(Ext.applyIf({
								sys_id: sys_id
							}, params));
						}

						if (url.indexOf('/resolve/') === 0 && url.indexOf('OWASP_CSRFTOKEN') === -1) {
							const urlData = url.split('#');
							const uriData = urlData[0].split('?');
							clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
								var csrftoken = '?' + data[0] + '=' + data[1];
								var tokenizedData = uri + csrftoken;
								if (uriData.length > 1) {
									tokenizedData += '&' + uriData[1];
								}
								if (urlData.length > 1) {
									tokenizedData += '#' + urlData[1];
								}
								url = tokenizedData;
							});
						}

						if (!serverConf.referenceTableViewPopup) window.open(url);
						else {
							Ext.create('Ext.window.Window', {
								modal: true,
								border: false,
								title: serverConf.referenceTableViewPopupTitle || RS.formbuilder.locale.NewReference + serverConf.title,
								width: Number(serverConf.referenceTableViewPopupWidth) || 600,
								height: Number(serverConf.referenceTableViewPopupHeight) || 400,
								layout: 'fit',
								html: Ext.String.format('<iframe class="rs_iframe" src="{0}" height="100%" style="width:100%;border:0" frameborder="0"/>', url),
								listeners: {
									render: function(w) {
										w.getEl().select('iframe').on('load', function(e, t, eOpts) {
											var me = this;
											t.contentWindow.registerForm = function(form) {
												form.on('beforeclose', function() {
													Ext.defer(function() {
														this.close();
													}, 100, this);
													return false;
												}, me);
												form.on('beforeexit', function() {
													Ext.defer(function() {
														this.close();
													}, 100, this);
													return false;
												}, me);
											}
										}, w);
									},
									close: {
										scope: this,
										fn: function() {
											this.loadReferences();
										}
									}
								}
							}).show();
						}
					}
				}
			}]
		}];

		//calculate columns to display
		var storeFields = ['sys_id'];
		Ext.each(serverConf.referenceColumns, function(displayColumn) {
			var type = 'string';
			switch (displayColumn.dbtype) {
				case 'timestamp':
					type = 'date';
					break;
				default:
					type = 'string';
					break;
			}
			var storeCol = {
				name: displayColumn.value,
				type: type
			};
			var col = {
				flex: 1,
				text: displayColumn.name,
				dataIndex: displayColumn.value
			};
			if (type == 'date') {
				storeCol.format = 'time'
				storeCol.dateFormat = 'time'
				col.renderer = Ext.util.Format.dateRenderer('Y-m-d g:i:s A')
			}

			storeFields.push(storeCol);
			config.columns.push(col);
		});

		

		config.store = Ext.create('Ext.data.Store', {
			fields: storeFields,
			proxy: {
				type: 'ajax',
				url: '/resolve/service/dbapi/getRecordData',
				extraParams: {
					sqlQuery: Ext.String.format('select * from {0} where {1} = \'{2}\'', serverConf.referenceTable, serverConf.referenceColumn, this.recordId),
					type: 'table',
					useSql: true
				},
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});

		// config.selModel = Ext.create('Ext.selection.CheckboxModel');
		var generatedId = Ext.id();
		config.dockedItems = [{
			xtype: 'toolbar',
			dock: 'top',
			name: 'actionBar',
			items: [{
				text: RS.formbuilder.locale.referenceTableNew,
				name: 'newReference',
				itemId: 'newButton',
				hidden: !serverConf.allowReferenceTableAdd,
				scope: this,
				handler: function(button) {
					var url = serverConf.referenceTableUrl ? serverConf.referenceTableUrl : '';

					if (url != '' && this.csrftoken != '') {
						var urlData = url.split('?');
						url = urlData[0] + '?' + this.csrftoken;
						if (urlData.length > 1) {
							url += '&' + urlData[1];
						}
					}

					url = url.replace(/\${parent_sys_id}/g, me.recordId);

					//pull fields from current form into the url if needed
					var temp = url.match(/\${(.*?)}/g)
					if (temp && temp.length > 0) {
						var form = this.up('form'),
							values = form.getValues();
						Ext.Array.forEach(temp, function(match) {
							var val = values[match.substring(2, match.length - 1)]
							if (val != null) url = url.replace(match, val)
						}, this)
					}

					if (url.indexOf('/resolve/') === 0 && url.indexOf('OWASP_CSRFTOKEN') === -1) {
						const urlData = url.split('#');
						const uriData = urlData[0].split('?');
						clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
							var csrftoken = '?' + data[0] + '=' + data[1];
							var tokenizedData = uri + csrftoken;
							if (uriData.length > 1) {
								tokenizedData += '&' + uriData[1];
							}
							if (urlData.length > 1) {
								tokenizedData += '#' + urlData[1];
							}
							url = tokenizedData;
						});
					}

					if (!serverConf.referenceTableViewPopup && url) window.open(url);
					else {
						Ext.create('Ext.window.Window', {
							modal: true,
							border: false,
							title: serverConf.referenceTableViewPopupTitle || RS.formbuilder.locale.NewReference + serverConf.title,
							width: Number(serverConf.referenceTableViewPopupWidth) || 600,
							height: Number(serverConf.referenceTableViewPopupHeight) || 400,
							layout: 'fit',
							html: Ext.String.format('<iframe class="rs_iframe" src="{0}" height="100%" style="width:100%;border:0" frameborder="0"/>', url),
							listeners: {
								render: function(w) {
									w.getEl().select('iframe').on('load', function(e, t, eOpts) {
										var me = this;
										t.contentWindow.registerForm = function(form) {
											form.on('beforeclose', function() {
												Ext.defer(function() {
													this.close();
												}, 100, this);
												return false;
											}, me);
											form.on('beforeexit', function() {
												Ext.defer(function() {
													this.close();
												}, 100, this);
												return false;
											}, me);
										}
									}, w);
								},
								close: {
									scope: this,
									fn: function() {
										this.loadReferences();
									}
								}
							}
						}).show();
					}
				}
			}, {
				text: RS.formbuilder.locale.referenceTableDelete,
				disabled: true,
				name: 'removeReference',
				itemId: 'removeButton' + generatedId,
				hidden: !serverConf.allowReferenceTableRemove,
				scope: this,
				handler: function(button) {
					var grid = button.up('grid'),
						selections = grid.getSelectionModel().getSelection();
					Ext.MessageBox.show({
						title: RS.formbuilder.locale.confirmRemove,
						msg: selections.length === 1 ? RS.formbuilder.locale.confirmRemoveReference : Ext.String.format(RS.formbuilder.locale.confirmRemoveReferences, selections.length),
						buttons: Ext.MessageBox.YESNO,
						buttonText: {
							yes: RS.formbuilder.locale.deleteReference,
							no: RS.formbuilder.locale.cancel
						},
						scope: grid,
						fn: this.removeSelectedReferences
					});
				}
			}]
		}];

		config.plugins = [{
			ptype: 'pager'
		}];

		config.listeners = {
			scope: this,
			selectionchange: function(selectionModel, selections, eOpts) {
				if (selections.length > 0) this.down('#removeButton' + generatedId).enable();
				else this.down('#removeButton' + generatedId).disable();
			}
		};

		return config;
	},
	removeSelectedReferences: function(button) {
		if (button === 'yes') {
			var selections = this.getSelectionModel().getSelection(),
				ids = [];
			Ext.each(selections, function(selection) {
				ids.push(selection.get('sys_id'));
			});
			var panel = this.up('referencetable');
			Ext.Ajax.request({
				url: '/resolve/service/dbapi/delete',
				params: {
					tableName: this.referenceTable,
					whereClause: 'sys_id in (\'' + ids.join('\',\'') + '\')',
					type: 'form',
					useSql: true
				},
				scope: panel,
				success: panel.removeDeletedReferences,
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			});
		}
	},
	removeDeletedReferences: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			this.loadReferences();
		} else {
			clientVM.displayError(response.message);
		}
	},
	loadReferences: function() {
		var grids = this.query('grid');
		Ext.each(grids, function(grid) {
			grid.getStore().load();
		}, this);
	}
});
/**
 *
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.TabContainerField', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.tabcontainerfield',

	layout: 'fit',

	anchor: '-20',
	height: 300,
	border: false,

	resizable: true,
	resizeHandles: 's',
	csrftoken: '',

	initComponent: function() {
		var me = this;

		if (this.referenceTableUrl != '') {
			var url = this.referenceTableUrl.split('?');
			clientVM.getCSRFToken_ForURI(url[0], function(token_pair) {
				this.csrftoken = token_pair[0] + '=' + token_pair[1];
			}.bind(this))
		}

		var displayNames = me.displayName ? me.displayName.split('|') : [],
			referenceTables = me.refGridReferenceByTable ? me.refGridReferenceByTable.split('|') : [],
			refGridReferenceColumnName = me.refGridReferenceColumnName ? me.refGridReferenceColumnName.split('|') : [],
			referenceColumns = me.refGridSelectedReferenceColumns ? me.refGridSelectedReferenceColumns.split('|') : [],
			allowReferenceTableAdd = me.allowReferenceTableAdd ? me.allowReferenceTableAdd.split('|') : [],
			allowReferenceTableRemove = me.allowReferenceTableRemove ? me.allowReferenceTableRemove.split('|') : [],
			allowReferenceTableView = me.allowReferenceTableView ? me.allowReferenceTableView.split('|') : [],
			referenceTableUrl = me.referenceTableUrl ? me.referenceTableUrl.split('|') : [],
			referenceTableViewPopup = me.refGridViewPopup ? me.refGridViewPopup.split('|') : [],
			referenceTableViewPopupWidth = me.refGridViewPopupWidth ? me.refGridViewPopupWidth.split('|') : [],
			referenceTableViewPopupHeight = me.refGridViewPopupHeight ? me.refGridViewPopupHeight.split('|') : [],
			referenceTableViewPopupTitle = me.refGridViewTitle ? me.refGridViewTitle.split('|') : [],
			len = displayNames.length,
			i, tabs = [];

		for (i = 0; i < len; i++) {
			var cols = referenceColumns[i] ? Ext.decode(referenceColumns[i]) : [];
			tabs.push({
				title: displayNames[i],
				referenceTable: referenceTables[i],
				referenceColumn: refGridReferenceColumnName[i],
				referenceColumns: cols,
				allowReferenceTableAdd: allowReferenceTableAdd[i] == 'true',
				allowReferenceTableRemove: allowReferenceTableRemove[i] == 'true',
				allowReferenceTableView: allowReferenceTableView[i] == 'true',
				referenceTableUrl: referenceTableUrl[i],
				referenceTableViewPopup: referenceTableViewPopup[i] == 'true',
				referenceTableViewPopupWidth: Number(referenceTableViewPopupWidth[i]),
				referenceTableViewPopupHeight: Number(referenceTableViewPopupHeight[i]),
				referenceTableViewPopupTitle: referenceTableViewPopupTitle[i]
			});
		}

		//Now render either the tabs or the one grid
		var config = {};
		if (len > 1) {
			config.xtype = 'tabpanel';
			config.plain = true;
			config.items = [];
			Ext.each(tabs, function(tab) {
				config.items.push(this.convertToGrid(tab));
			}, this);
		} else {
			config = this.convertToGrid(tabs[0]);
		}
		me.items = config;

		me.callParent();

		if (this.recordId) me.loadReferences();
	},
	convertToGrid: function(serverConf) {
		var me = this;

		if (serverConf.referenceTableViewPopupTitle == 'FileManager') {
			return {
				border: false,
				title: serverConf.title,
				xtype: 'uploadmanager',
				formName: me.formName,
				recordId: me.recordId,
				fileUploadTableName: me.fileUploadTableName,
				referenceColumnName: me.referenceColumnName,
				allowUpload: serverConf.allowReferenceTableAdd,
				allowRemove: serverConf.allowReferenceTableRemove
			};
		}
		if (serverConf.referenceTableViewPopupTitle == 'URL') {
			if (serverConf.referenceTableUrl && serverConf.referenceTableUrl.indexOf('/resolve/') === 0) {
    			const urlData = serverConf.referenceTableUrl.split('#');
    			const uriData = urlData[0].split('?');
    			clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
    				var csrftoken = '?' + data[0] + '=' + data[1];
    				var tokenizedData = uri + csrftoken;
    				if (uriData.length > 1) {
    					tokenizedData += '&' + uriData[1];
    				}
    				if (urlData.length > 1) {
    					tokenizedData += '#' + urlData[1];
    				}
    				serverConf.referenceTableUrl = tokenizedData;
    			});
    		}

			return {
				border: false,
				title: serverConf.title,
				html: '&nbsp;',
				listeners: {
					afterrender: function(panel) {
						panel.update(Ext.String.format('<iframe class="rs_iframe" src="{0}" height="100%" style="width:100%;border:0" frameborder="0"/>', serverConf.referenceTableUrl))
					}
				}
			}
		}


		var config = Ext.apply({}, serverConf, {
			xtype: 'grid'
		});
		config.columns = [{
			xtype: 'actioncolumn',
			hideable: false,
			hidden: !serverConf.allowReferenceTableView,
			width: 25,
			items: [{
				icon: '/resolve/images/link.gif',
				// Use a URL in the icon config
				tooltip: RS.formbuilder.locale.viewReference,
				scope: this,
				handler: function(grid, rowIndex, colIndex) {
					var rec = grid.getStore().getAt(rowIndex),
						sys_id = rec.get('sys_id'),
						url = serverConf.referenceTableUrl,
						me = this;
					if (sys_id) {
						//open new window to the url configured for this reference table
						var containsSysIdAlready = url.indexOf('${sys_id}') > -1;

						url = url.replace(/${sys_id}/g, sys_id).replace(/${parent_sys_id}/g, me.recordId);

						if (!containsSysIdAlready) {
							var split = url.split('?'),
								params = {};
							if (split.length > 1) {
								params = Ext.Object.fromQueryString(split[1]);
							}
							url = split[0] + '?' + Ext.urlEncode(Ext.applyIf({
								sys_id: sys_id
							}, params));
						}

						if (url.indexOf('/resolve/') === 0 && url.indexOf('OWASP_CSRFTOKEN') === -1) {
							const urlData = url.split('#');
							const uriData = urlData[0].split('?');
							clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
								var csrftoken = '?' + data[0] + '=' + data[1];
								var tokenizedData = uri + csrftoken;
								if (uriData.length > 1) {
									tokenizedData += '&' + uriData[1];
								}
								if (urlData.length > 1) {
									tokenizedData += '#' + urlData[1];
								}
								url = tokenizedData;
							});
						}

						if (!serverConf.referenceTableViewPopup) window.open(url);
						else {
							Ext.create('Ext.window.Window', {
								modal: true,
								border: false,
								autoScroll: false,
								title: serverConf.referenceTableViewPopupTitle || RS.formbuilder.locale.EditReference + serverConf.title,
								width: Number(serverConf.referenceTableViewPopupWidth) || 600,
								height: Number(serverConf.referenceTableViewPopupHeight) || 400,
								layout: 'fit',
								html: Ext.String.format('<iframe class="rs_iframe" src="{0}" height="100%" style="width:100%;border:0" frameborder="0"/>', url),
								listeners: {
									render: function(w) {
										w.getEl().select('iframe').on('load', function(e, t, eOpts) {
											var me = this;
											t.contentWindow.registerForm = function(form) {
												form.on('beforeclose', function() {
													Ext.defer(function() {
														this.close();
													}, 100, this);
													return false;
												}, me);
												form.on('beforeexit', function() {
													Ext.defer(function() {
														this.close();
													}, 100, this);
													return false;
												}, me);
												form.on('titleChanged', function(title) {
													this.setTitle(title);
													return false;
												}, me);
											}
										}, w);
									},
									close: function() {
										me.loadReferences();
									}
								}
							}).show();
						}
					}
				}
			}]
		}];

		//calculate columns to display
		var storeFields = ['sys_id'];
		Ext.each(serverConf.referenceColumns, function(displayColumn) {
			var type = 'string';
			switch (displayColumn.dbtype) {
				case 'timestamp':
					type = 'date';
					break;
				default:
					type = 'string';
					break;
			}
			var storeCol = {
				name: displayColumn.value,
				type: type
			};
			var col = {
				flex: 1,
				text: displayColumn.name,
				dataIndex: displayColumn.value
			};
			if (type == 'date') {
				storeCol.format = 'c';
				col.renderer = Ext.util.Format.dateRenderer('Y-m-d g:i:s A');
			}

			storeFields.push(storeCol);
			config.columns.push(col);
		});

		var field = serverConf.referenceColumn;
		if (serverConf.referenceColumn == 'u_content_request' && serverConf.referenceTable != 'content_request') {
			field += '.sys_id';
		}

		config.store = Ext.create('Ext.data.Store', {
			fields: storeFields,
			proxy: {
				type: 'ajax',
				url: '/resolve/service/dbapi/getRecordData',
				extraParams: {
					tableName: serverConf.referenceTable,
					type: 'table',
					start: 0,
					filter: Ext.encode([{
						field: field,
						type: 'auto',
						condition: 'equals',
						value: this.recordId || '__blank'
					}])
				},
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});

		// config.selModel = Ext.create('Ext.selection.CheckboxModel');
		var generatedId = Ext.id();
		config.dockedItems = [{
			xtype: 'toolbar',
			dock: 'top',
			name: 'actionBar',
			items: [{
				text: RS.formbuilder.locale.referenceTableNew,
				name: 'newReference',
				itemId: 'newButton',
				hidden: !serverConf.allowReferenceTableAdd,
				scope: this,
				handler: function(button) {
					var url = serverConf.referenceTableUrl ? serverConf.referenceTableUrl : '';

					if (url != '' && this.csrftoken != '') {
						var urlData = url.split('?');
						url = urlData[0] + '?' + this.csrftoken;
						if (urlData.length > 1) {
							url += '&' + urlData[1];
						}
					}

					url = url.replace(/\${parent_sys_id}/g, me.recordId);

					if (url.indexOf('/resolve/') === 0 && url.indexOf('OWASP_CSRFTOKEN') === -1) {
						const urlData = url.split('#');
						const uriData = urlData[0].split('?');
						clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
							var csrftoken = '?' + data[0] + '=' + data[1];
							var tokenizedData = uri + csrftoken;
							if (uriData.length > 1) {
								tokenizedData += '&' + uriData[1];
							}
							if (urlData.length > 1) {
								tokenizedData += '#' + urlData[1];
							}
							url = tokenizedData;
						});
					}

					if (!serverConf.referenceTableViewPopup && url) window.open(url);
					else {
						Ext.create('Ext.window.Window', {
							modal: true,
							border: false,
							title: serverConf.referenceTableViewPopupTitle || RS.formbuilder.locale.NewReference + serverConf.title,
							width: Number(serverConf.referenceTableViewPopupWidth) || 600,
							height: Number(serverConf.referenceTableViewPopupHeight) || 400,
							layout: 'fit',
							html: Ext.String.format('<iframe class="rs_iframe" src="{0}" height="100%" style="width:100%;border:0" frameborder="0"/>', url),
							listeners: {
								render: function(w) {
									w.getEl().select('iframe').on('load', function(e, t, eOpts) {
										var me = this;
										t.contentWindow.registerForm = function(form) {
											form.on('beforeclose', function() {
												Ext.defer(function() {
													this.close();
												}, 100, this);
												return false;
											}, me);
											form.on('beforeexit', function() {
												Ext.defer(function() {
													this.close();
												}, 100, this);
												return false;
											}, me);
										}
									}, w);
								},
								close: {
									scope: this,
									fn: function() {
										this.loadReferences();
									}
								}
							}
						}).show();
					}
				}
			}, {
				text: RS.formbuilder.locale.referenceTableDelete,
				disabled: true,
				name: 'removeReference',
				itemId: 'removeButton' + generatedId,
				hidden: !serverConf.allowReferenceTableRemove,
				scope: this,
				handler: function(button) {
					var grid = button.up('grid'),
						selections = grid.getSelectionModel().getSelection();
					Ext.MessageBox.show({
						title: RS.formbuilder.locale.confirmRemove,
						msg: selections.length === 1 ? RS.formbuilder.locale.confirmRemoveReference : Ext.String.format(RS.formbuilder.locale.confirmRemoveReferences, selections.length),
						buttons: Ext.MessageBox.YESNO,
						buttonText: {
							yes: RS.formbuilder.locale.deleteReference,
							no: RS.formbuilder.locale.cancel
						},
						scope: grid,
						fn: this.removeSelectedReferences
					});
				}
			}]
		}];

		config.plugins = [{
			ptype: 'pager'
		}];

		config.listeners = {
			scope: this,
			selectionchange: function(selectionModel, selections, eOpts) {
				if (selections.length > 0) this.down('#removeButton' + generatedId).enable();
				else this.down('#removeButton' + generatedId).disable();
			}
		};

		return config;
	},
	removeSelectedReferences: function(button) {
		if (button === 'yes') {
			var selections = this.getSelectionModel().getSelection(),
				ids = [];
			Ext.each(selections, function(selection) {
				ids.push(selection.get('sys_id'));
			});
			var panel = this.up('referencetable');
			Ext.Ajax.request({
				url: '/resolve/service/dbapi/delete',
				params: {
					tableName: this.referenceTable,
					whereClause: 'sys_id in (\'' + ids.join('\',\'') + '\')',
					type: 'form',
					useSql: true
				},
				scope: panel,
				success: panel.removeDeletedReferences,
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			});
		}
	},
	removeDeletedReferences: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			this.loadReferences();
		} else {
			clientVM.displayError(response.message);
		}
	},
	loadReferences: function() {
		var grids = this.query('grid');
		Ext.each(grids, function(grid) {
			grid.getStore().load();
		}, this);
	}
});
/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.TagField', {
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.tagfield',

	displayField: 'name',
	valueField: 'value',
	queryMode: 'local',
	multiSelect: true,

	initComponent: function() {
		this.store = Ext.create('Ext.data.Store', {
			fields: ['name', 'value'],
			proxy: {
				type: 'ajax',
				url: '/resolve/service/form/getTags',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});
		this.store.load({});

		this.callParent();
	}
});
/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.TeamPicker', {
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.teampickerfield',

	displayField: 'name',
	valueField: 'value',
	queryMode: 'local',

	forceSelection: true,

	/**
	 * Pipe separated list of team id's to filter the users with.  For example 'id123|id456'
	 */
	teamsOfTeams: '',
	recurseThroughTeamsList: false,

	//Clear Trigger
	clearCls: Ext.baseCSSPrefix + 'form-clear-trigger',
	clearClick: function() {
		//clear the current value and hide the clear button
		this.setValue('');
		this.triggerEl.item(0).parent().setDisplayed('none');
	},

	initComponent: function() {
		this.trigger2Cls = this.triggerCls;
		this.onTrigger2Click = this.onTriggerClick;
		this.trigger1Cls = this.clearCls;
		this.onTrigger1Click = this.clearClick;

		this.store = Ext.create('Ext.data.Store', {
			fields: ['name', 'value', 'type'],
			proxy: {
				type: 'ajax',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});

		Ext.Ajax.request({
			url: '/resolve/service/form/getTeams',
			params: {
				teamPickerTeamsOfTeams: this.teamPickerTeamsOfTeams,
				recurseTeamsOfTeam: this.recurseTeamsOfTeam
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					this.store.loadData(response.records);
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(r){
				clientVM.displayFailure(r);
			}
		});

		this.callParent();
	},

	onRender: function() {
		this.callParent(arguments);
		this.triggerEl.item(0).parent().setDisplayed('none');
	},

	setValue: function(value) {
		this.callParent(arguments);
		if (this.rendered && this.value && !this.mandatory && !this.readOnly) {
			this.triggerEl.item(0).parent().setDisplayed('table-cell');
			this.doComponentLayout();
		}
	}
});
/**
 *
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.UrlContainerField', {
	extend: 'Ext.Panel',
	alias: 'widget.urlcontainerfield',

	initComponent: function() {
		var me = this;
		me.html = '&nbsp;';
		me.callParent();
		me.on('afterrender', function() {
			me.update(Ext.String.format('<iframe class="rs_iframe" src="{0}" height="100%" style="width:100%;border:0" frameborder="0"/>', me.referenceTableUrl));
		})
	}
});
/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.UserPicker', {
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.userpickerfield',

	displayField: 'name',
	valueField: 'value',
	queryMode: 'local',

	meValue: 'me',

	allowAssignToMe: false,
	autoAssignToMe: false,
	// forceSelection: true,

	//user trigger
	userCls: 'icon-user', //Ext.baseCSSPrefix + 'form-user-trigger',
	userClick: function() {
		this.setValue(this.meValue);
	},

	//Clear Trigger
	clearCls: Ext.baseCSSPrefix + 'form-clear-trigger',
	clearClick: function() {
		//clear the current value and hide the clear button
		this.clearing = true;
		this.setValue('', true);
		this.triggerEl.item(0).parent().setDisplayed('none');
	},

	/**
	 * Pipe separated list of group id's to filter the users with.  For example 'id123|id456'
	 */
	groups: '',
	usersOfTeams: '',
	recurseUsersOfTeam: false,
	teamsOfTeams: '',
	recurseTeamsOfTeam: false,

	stillLoading: false,

	initComponent: function() {
		this.trigger2Cls = this.triggerCls;
		this.onTrigger2Click = this.onTriggerClick;
		this.trigger1Cls = this.clearCls;
		this.onTrigger1Click = this.clearClick;
		this.trigger3Cls = this.userCls;
		this.trigger3OverCls = this.userCls;
		this.onTrigger3Click = this.userClick;

		this.store = Ext.create('Ext.data.Store', {
			fields: ['name', 'value', 'type'],
			proxy: {
				type: 'memory',
				reader: {
					type: 'json',
					root: 'records'
				}
			}
		});

		this.stillLoading = true;
		Ext.Ajax.request({
			url: '/resolve/service/form/getUsers',
			jsonData: {
				groups: this.groups || '',
				usersOfTeams: this.usersOfTeams || '',
				recurseUsersOfTeam: this.recurseUsersOfTeam,
				teamsOfTeams: this.teamsOfTeams,
				recurseTeamsOfTeam: this.recurseTeamsOfTeam
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (!response.success) {
					clientVM.displayError(response.message);
					return;
				}
				this.store.add(response.records);
				this.stillLoading = false;
				this.setValueActual(this.tempValue);
				if (this.autoAssignToMe && !this.getValue()) this.setValue(this.meValue);
				//if there is only one team, then auto assign to that team, overriding the auto-assign-to-me option
				var assignToGroup = '';
				this.store.each(function(record) {
					if (record.data.type == 'team') {
						if (!assignToGroup) assignToGroup = record.data.value;
						else {
							assignToGroup = false;
							return false;
						}
					}
				})
				if (assignToGroup && !this.getValue()) {
					this.setValue(assignToGroup);
				}
			},
			failure: function(r){
				clientVM.displayFailure(r);
			}
		});


		this.callParent();
	},
	onRender: function() {
		this.callParent(arguments);
		if (this.readOnly) {
			this.getEl().select('input').setStyle({
				border: '0px solid transparent',
				backgroundImage: 'none',
				cursor: 'auto'
			});
		}
		var tip = Ext.create('Ext.tip.ToolTip', {
			target: this.triggerEl.item(2),
			html: RS.formbuilder.locale.assignToMe
		});
		this.triggerEl.item(0).parent().setDisplayed('none');
		if (!this.allowAssignToMe) this.triggerEl.item(2).parent().setDisplayed('none');
		else {
			this.triggerEl.item(2).addCls('icon-large rs-icon rs-no-background')
		}
	},
	//snapshot of current value when the control is still loading the field
	tempValue: '',
	setValue: function(value, clear) {
		if (this.stillLoading) {
			this.tempValue = value;
			return;
		}
		this.callParent(arguments);
		if (this.rendered && this.value && !this.mandatory && !this.readOnly) {
			this.triggerEl.item(0).parent().setDisplayed('table-cell');
			this.doComponentLayout();
		}

		if (this.rendered && !this.value && !clear) {
			if (this.clearing) {
				this.clearing = false;
				return;
			}
			//Being passed null because we're being reset.  Run through the default logic again
			if (this.autoAssignToMe === null) {
				Ext.defer(function() {
					this.setValue(this.meValue);
				}, 100, this)
				return;
			}
			//if there is only one team, then auto assign to that team, overriding the auto-assign-to-me option
			var assignToGroup = '';
			this.store.each(function(record) {
				if (record.data.type == 'team') {
					if (!assignToGroup) assignToGroup = record.data.value;
					else {
						assignToGroup = false;
						return false;
					}
				}
			});
			if (assignToGroup && !this.getValue()) {
				this.setValue(assignToGroup);
			}
		}
	}
});
/**
 * Rendering engine for the FormBuilder.  Takes a formConfiguration, formName, or formId to display the user configured controls.
 */
Ext.define('RS.formbuilder.viewer.desktop.RSForm', {
	API : {
		submit : '/resolve/service/form/submit',
		getForm : '/resolve/service/form/getform',
		getRecordData : '/resolve/service/dbapi/getRecordData',
		getDefaultData : '/resolve/service/form/getdefaultdata',
		getATProperty : '/resolve/service/atproperties/list',
		getWSData : '/resolve/service/wsdata/getMap'
	},
	showLoading: true,
	params: '',
	fromViewer: false,
	formdefinition: false,

	/**
	 * Separator used to separate different fields in the choice and select fields
	 */
	choiceSeparator: '|&|',

	/**
	 * Separator used to separate different values in the choice and select fields
	 */
	choiceValueSeparator: '|=|',

	extend: 'Ext.panel.Panel',
	alias: 'widget.rsform',

	border: false,
	layout: 'fit',
	fieldConfiguration: [],
	submitConfiguration: {
		viewName: '',
		formSysId: '',
		tableName: '',
		userId: '',
		timezone: '',
		crudAction: '',
		query: '',
		controlItemSysId: '',
		dbRowData: {},
		sourceRowData: {}
	},

	embed: false,
	hideToolbar: false,

	/**
	 * Id of the form to use when looking up the controls from the server
	 */
	formId: '',
	setFormId: function(value) {
		if (value != this.formId) {
			this.formId = value
			this.fireEvent('reloadForm', this)
		}
	},

	/**
	 * Name of the form to use when looking up the controls from the server
	 */
	formName: '',
	setFormName: function(value) {
		if (value != this.formName) {
			this.formName = value
			this.fireEvent('reloadForm', this)
		}
	},

	/**
	 * The record to load into the grid once the form has actually been rendered (or empty for a new record)
	 */
	recordId: '',
	setRecordId: function(value) {
		if (value != this.recordId) {
			this.recordId = value
			this.fireEvent('reloadForm', this)
		}
	},
	queryString: '',
	setQueryString: function(value) {
		if (value != this.queryString) {
			this.queryString = value
			this.fireEvent('reloadForm', this)
		}
	},

	params: '',
	setParams: function(value) {
		if (value != this.params) {
			this.params = value
			this.fireEvent('reloadForm', this)
		}
	},

	initComponent: function() {
		this.addEvents(
			/**
			 * @event actioncompleted
			 * Fires when the button is clicked and all actions have completed running on the server
			 * @param {RS.formbuilder.RSForm} form The form generating this event
			 * @param {Ext.button.Button} button The button that was clicked
			 */
			'actioncompleted', 'beforeclose', 'beforeexit', 'beforeredirect', 'reloadForm');
		//convert all the items configurations to proper configurations
		var configuration = this.formConfiguration;
		if (configuration) this.convertConfig(configuration);
		else if (!this.fromViewer) this.loadForm()
		if (!this.height) delete this.height;
		this.callParent();

		this.on('render', function() {
			var me = this;
			if (me.autoSize) {
				if (!me.height) {
					var doc = Ext.getDoc(),
						size = doc.getViewSize(),
						windowHeight = size.height,
						windowWidth = size.width;
					Ext.EventManager.onWindowResize(function() {
						var doc = Ext.getDoc(),
							size = doc.getViewSize();
						me.maxHeight = size.height - 1;
						me.setWidth(size.width);
						me.doLayout();
						Ext.each(Ext.dom.Query.select('div[class=x-mask]'), function(el) {
							Ext.fly(el).setStyle('display', 'none');
						});
					});
					doc.select('body').setStyle('overflow', 'hidden');
					me.maxHeight = windowHeight - 1;
					me.setWidth(windowWidth);
					me.doLayout();
				} else {
					//Still correct the width if it changes regardless of height setting
					Ext.EventManager.onWindowResize(function() {
						var doc = Ext.getDoc(),
							size = doc.getViewSize();
						me.setWidth(size.width);
						me.doLayout();
						Ext.each(Ext.dom.Query.select('div[class=x-mask]'), function(el) {
							Ext.fly(el).setStyle('display', 'none');
						});
					});
					var size = Ext.getDoc().getViewSize();
					me.setWidth(size.width);
					me.doLayout();
				}
			}
		}, this, {
			single: true
		});

		this.on('reloadForm', function() {
			if (this.rendered) this.loadForm()
		}, this, {
			buffer: 100
		})

		Ext.defer(function() {
			if (typeof(registerForm) == 'function') {
				registerForm(this);
			}
		}, 100, this);
	},

	loadForm: function() {
		if (this.formName || this.formId || this.recordId) {
			Ext.Ajax.request({
				url: this.API['getForm'],
				params: {
					view: this.formName,
					formsysid: this.formId,
					recordid: this.recordId,
					formdefinition: this.formdefinition
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) {
						if (response.data) {
							this.convertConfig(response.data)
							this.getWSData()
							this.getATProperty()
							// store the form's tablename for use in SIR custom data form
							clientVM.forms = {};
							clientVM.forms[response.data.viewName] = response.data.tableName;
						} else {
							clientVM.displayError(RS.formbuilder.locale.formNotFound)
							this.removeAll()
						}
						return;
					}
					clientVM.displayError(response.message);
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			});
		} else this.processUrlVariables()
	},

	/**
	 * Converts the configuration from the server to real controls to be rendered on the page
	 * @param {Object} config
	 */
	convertConfig: function(config) {
		config.displayName = Ext.String.htmlEncode(config.displayName) || '';
		this.viewmodelSpec = {
			mtype: 'viewmodel',
			ns: 'RS.formbuilder'
		};
		var viewSpec = {},
			configuration;
		var buttonPanel = config.buttonPanel ? config.buttonPanel.controls : [] || [];

		this.submitConfiguration = Ext.apply({}, {
			viewName: config.viewName,
			formSysId: config.id,
			tableName: config.tableName,
			userId: ''
		}, this.submitConfiguration);

		this.userName = config.username;

		if (config.windowTitle) {
			var variables = config.windowTitle.match(/\${[\w]+}/g),
				functionString = Ext.String.format('\nvar title="{0}";\n', Ext.String.htmlEncode(config.windowTitle));
			Ext.each(variables, function(variable, index) {
				functionString += Ext.String.format('var s{0} = this.{1};\ntitle = title.replace(/\\{2}/g, s{0});\n', index, variable.substring(2, variable.length - 1), variable);
			});

			//actually set the window title based on the new title
			functionString += 'if( this[\'formScope\'] && this[\'formScope\'].fireEvent(\'titleChanged\', title) !== false){clientVM.setWindowTitle(title);}return title;';
			this.viewmodelSpec.windowTitle$ = new Function(functionString);
		}

		//look in temp for the real controls to display
		config.panels = config.panels || [];
		if (config.panels.length == 0) config.panels.push({
			tabs: [{
				columns: [{
					fields: []
				}]
			}]
		});
		if (config.panels[0].tabs.length > 1) {
			this.viewmodelSpec.activeTabIndex = 0;
			this.viewmodelSpec.tabCount = config.panels[0].tabs.length;
			var tabItems = [],
				tabs = config.panels[0].tabs;
			Ext.each(tabs, function(tab) {
				var controls = tab.columns[0].fields;
				Ext.each(controls, this.convertControl, this);
				tabItems.push({
					layout: 'anchor',
					bodyPadding: '10px',
					title: tab.name,
					name: tab.name.replace(/[^\w]/g, ''),
					defaults: {
						labelWidth: 125
					},
					items: controls,
					autoScroll: true
				});
				tab.name = tab.name.replace(/[^\w]/g, '');
				this.parseDependencies(tab);
			}, this);

			var temp = {
				xtype: 'tabpanel',
				plain: true,
				activeTab: '@{activeTabIndex}',
				items: tabItems
			};

			if (config.wizard) {
				Ext.apply(temp, {
					tabBar: {
						cls: 'wizard-steps'
					}
				});
			}

			configuration = [temp];
		} else {
			if (config.panels[0].tabs[0].columns.length > 0)
				configuration = config.panels[0].tabs[0].columns[0].fields;
			
			if (configuration) {
				for (var i=0; i<configuration.length; i++) {
					if (configuration[i].uiType === 'UrlContainerField' && configuration[i].referenceTableUrl.indexOf('/resolve/') === 0) {
						const urlData = configuration[i].referenceTableUrl.split('#');
						const uriData = urlData[0].split('?');
						clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
							var csrftoken = '?' + data[0] + '=' + data[1];
							var tokenizedData = uri + csrftoken;
							if (uriData.length > 1) {
								tokenizedData += '&' + uriData[1];
							}
							if (urlData.length > 1) {
								tokenizedData += '#' + urlData[1];
							}
							configuration[i].referenceTableUrl = tokenizedData;
						});
					}
				}
			}

			//convert all the controls to appropriate extjs controls
			Ext.Array.forEach(configuration || [], this.convertControl, this);
		}

		this.fieldConfiguration = config.panels[0].tabs;

		//convert the buttons
		if (!buttonPanel) buttonPanel = [];
		Ext.Array.forEach(buttonPanel, this.convertButton, this);

		var parentWindow = this.up('window')
		if (parentWindow && config.displayName) {
			parentWindow.setTitle(config.displayName)
			config.displayName = ''
		}

		Ext.apply(viewSpec, {
			xtype: 'form',
			border: false,
			autoScroll: true,
			bodyPadding: '10px',
			htmlEncodeTitle : true,
			title: this.embed ? '' : config.displayName,
			buttonAlign: 'left',
			cls: 'rs-custom-button',
			items: Ext.Array.from(configuration),
			defaults: {
				labelWidth: 125
			}
		});

		if (this.embed) Ext.apply(viewSpec, {
			tbar: {
				xtype: 'toolbar',
				cls: 'actionBar actionBar-form',
				items: buttonPanel
			}
		})
		else Ext.apply(viewSpec, {
			buttons: buttonPanel
		})

		//Showing a tab panel instead of just a form, so we need to correct some normal form styling
		if (config.panels[0].tabs.length > 1) {
			viewSpec.layout = 'fit';
			viewSpec.autoScroll = false;
			// delete viewSpec.bodyPadding;
		}

		//load up the model and the view
		this.viewmodelSpec.formScope = '';
		this.vm = glu.model(this.viewmodelSpec);
		this.vm.formScope = this;
		this.viewSpec = viewSpec;
		if (this.vm.windowTitle$) this.vm.windowTitle$();
		var view = glu.viewFromSpec(this.vm, viewSpec);
		if (this.hideToolbar) {
			view.dockedItems.each(function(item) {
				if (item.isXType('toolbar')) item.hide()
			})
		}
		if (this.items) {
			this.removeAll()
			this.add(view);
		} else this.items = [view]

		if (this.recordId || this.queryString) this.setRecordId(this.recordId)
		else this.processUrlVariables();

		this.fireEvent('formLoaded', this, this)
	},

	processDataRequest: function(r, button) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			if (response.data) {
				var data = null,
					tempDate = null;
				if (Ext.isArray(response.data)) {
					var vals = {};
					Ext.each(response.data, function(val) {
						vals[val.fieldName] = val.defaultValue;
					})
					response.data = vals;
				}
				for (var key in response.data) {
					data = response.data[key];
					if (this.vm[key + 'IsDate']) {
						tempDate = Ext.Date.parse(data, 'c');
						if (Ext.isDate(tempDate)) data = tempDate;
						//Parse out the option values
						if (data && Ext.isString(data) && data.indexOf(this.choiceSeparator) > -1) {
							data = this.parseValue(data);
						}
					}
					if (data && Ext.isString(data) && data.indexOf(this.choiceSeparator) > -1) {
						data = this.parseValue(data);
					}
					if (Ext.isDefined(this.vm[key])) {
						if (Ext.isDate(this.vm[key]) && !data) data = new Date();
						this.vm.set(key, data);
					}
				}
				//Special processing for radios because the bindings don't work with extjs's messed up structure, we have to manually set the values
				//checkbox group is fix with a manual event wiring..need to find a better way to deal with field group
				var components = this.query('radiogroup');
				Ext.each(components, function(component) {
					var name = component.name;
					if (!name) name = component.items.getAt(0).name;
					if (Ext.isDefined(response.data[name])) {
						var obj = {};
						obj[name] = response.data[name];
						component.setValue(obj);
					}
				});
				this.recordId = response.data.sys_id;
				//Also make sure to reload the reference grids if there are any
				var referenceGrids = this.query('referencetable');
				Ext.each(referenceGrids, function(referenceGrid) {
					referenceGrid.loadReferences();
				});
				//check if there is a reload target, condition, and value on the button.  If there is then we need to reload if that condition isn't met
				if (button && button.reloadCondition && button.reloadTarget && button.reloadValue) {
					var needsReload = false;
					if (Ext.isDefined(this.vm[button.reloadTarget])) {
						var val = this.vm[button.reloadTarget],
							value = button.reloadValue;
						needsReload = true;

						switch (button.reloadCondition) {
							case 'equals':
								if (val == value) needsReload = false;
								break;
							case 'notequals':
								if (val != value) needsReload = false;
								break;
							case 'greaterThan':
								if (val > value) needsReload = false;
								break;
							case 'greaterThanOrEqual':
								if (val >= value) needsReload = false;
								break;
							case 'lessThan':
								if (val < value) needsReload = false;
								break;
							case 'lessThanOrEqual':
								if (val <= value) needsReload = false;
								break;
							case 'contains':
								if (val.indexOf(value) > -1) needsReload = false;
								break;
							case 'notcontains':
								if (val.indexOf(value) == -1) needsReload = false;
								break;
						}
					}

					if (needsReload && this.recordId) {
						if (response.data && !response.data.u_worksheet) {
							//button.enable();
							button.setIcon('');
							clientVM.displayError(RS.formbuilder.locale.worksheet_not_found);
						} else if (response.data.u_worksheet) {
							this.verifyWS(response.data.u_worksheet, function(){
								Ext.defer(this.setRecordId, button.reloadDelay * 1000, this, [this.recordId, button]);
							}.bind(this), function() {
								//button.enable();
								button.setIcon('');
							});
						} else {
							Ext.defer(this.setRecordId, button.reloadDelay * 1000, this, [this.recordId, button]);
						}
					} else {
						button.enable();
						button.setIcon('');
					}
				}
			}
		} else clientVM.displayError(response.message)
		this.processUrlVariables()
			//Ensure the window isn't scrolled by focusing on fields that aren't visible
		window.scrollTo(0, 0)
	},

	/**
	 * Converts an individual control provided by the server to the correct representation for extjs to display
	 * @param {Object} config
	 */
	convertControl: function(config) {
		config.sectionTitle =  Ext.String.htmlEncode(config.sectionTitle|| '');
		config.tooltip =  Ext.String.htmlEncode(config.tooltip|| '');
		config.fieldLabel = config.displayName;
		// config.value = config.value || config.defaultValue;
		config.allowBlank = !config.mandatory;
		config.labelSeparator = config.mandatory ? ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>' : ':';

		//configure viewmodel and dependencies
		this.viewmodelSpec[config.name] = config.value || config.defaultValue || '';

		this.parseDependencies(config);
		// if (config.mandatory) this.viewmodelSpec[config.name + 'IsValid$'] = new Function('if( !this.' + config.name + ')return false;');
		if (!config.width) delete config.width;
		if (!config.height) delete config.height;

		//if its the default height, the just let the controls be what they want to be
		if (config.height == 22 || config.height == 25) delete config.height;

		if (!config.width) {
			config.anchor = '99.5%';
		} else {
			config.width += 125;
		}
		delete config.id; //this could screw things up since the id's start with numbers
		delete config.value;
		if (!config.hidden) delete config.hidden;

		if (config.readOnly) config.fieldCls = 'x-form-field-readOnly';

		if (config.tooltip) {
			config.listeners = {
				render: function(field) {
					var target = field.getEl().dom.id;
					Ext.tip.QuickTipManager.register({
						target: field.getEl().dom.id,
						text: config.tooltip
					});
				}
			};
		}

		switch (config.uiType) {
			case 'TextField':
				config.xtype = 'textfield';
				config.minLength = config.stringMinLength;
				if (config.stringMaxLength > 0 && config.stringMaxLength != 4001) config.maxLength = config.stringMaxLength;
				break;

			case 'TextArea':
				config.xtype = 'textareafield';
				config.resizable = true;
				config.resizeHandles = 's';
				config.minLength = config.stringMinLength;
				if (config.stringMaxLength > 0 && config.stringMaxLength != 4001) config.maxLength = config.stringMaxLength;
				break;

			case 'NumberTextField':
				config.xtype = 'numberfield';
				config.allowDecimals = false;
				config.minValue = config.integerMinValue;
				if (config.integerMaxValue > 0) config.maxValue = config.integerMaxValue;
				break;

			case 'DecimalTextField':
				config.xtype = 'numberfield';
				config.minValue = config.decimalMinValue;
				if (config.decimalMaxVaue > 0) config.maxValue = config.decimalMaxValue;
				break;

			case 'ComboBox':
				config.xtype = 'clearablecombobox';
				config.displayField = 'name';
				config.valueField = 'value';
				config.editable = config.selectUserInput;
				config.forceSelection = !config.selectUserInput;
				config.store = '@{' + config.name + 'Store' + '}';
				this.viewmodelSpec[config.name] = this.parseValue(config.defaultValue).join('');
				//If the store is configured to be from advanced or custom, then we need to get the data once we have values in the fields
				if (config.choiceOptionSelection > 0) {
					this.viewmodelSpec[config.name] = '';
					config.disabled = '@{!' + config.name + 'IsLoadedAndEnabled}';
					config.pageSize = 50;
					var variableString = this.getVariableString(config);
					this.viewmodelSpec[config.name + 'IsLoadedAndEnabled$'] = function() {
						return true;
					}
					if (variableString) {
						this.viewmodelSpec[config.name + 'loadStoreOn$'] = new Function(Ext.String.format('this.set("{1}".substring(0,"{1}".length-5), "");if( {0} ){var ps = this.{1}.proxy.extraParamsOrig || this.{1}.proxy.extraParams,vars=ps.sqlQuery.match(/\\$\{(.*?)\\}/g);this.{1}.proxy.extraParamsOrig=Ext.clone(ps);Ext.each(vars, function(variable, index) {ps.sqlQuery = ps.sqlQuery.replace(variable, this[variable.substring(2,variable.length-1)])},this);this.{1}.load({params: ps});}', variableString, config.name + 'Store'));
						this.viewmodelSpec[config.name + 'IsLoadedAndEnabled$'] = new Function(Ext.String.format('return {0}', variableString));
					}
					config.queryMode = 'local';
					this.viewmodelSpec[config.name + 'Store'] = {
						mtype: 'store',
						autoLoad: variableString ? false : true,
						fields: ['name', 'value'],
						proxy: {
							type: 'ajax',
							url: this.API['getRecordData'],
							extraParams: {
								useSql: true,
								type: 'table',
								sqlQuery: config.choiceOptionSelection == 2 ? config.choiceSql : Ext.String.format('select {0} as name, {1} as value from {2} {3}', config.choiceDisplayField, config.choiceValueField, config.choiceTable, variableString ? Ext.String.format('where {0} = \'{1}\'', config.choiceWhere, '${' + config.choiceField + '}') : '')
							},
							reader: {
								type: 'json',
								root: 'records'
							},
							listeners: {
								exception: function(e, resp, op) {
									clientVM.displayExceptionError(e, resp, op);
								}
							}
						}
					};
				} else {
					this.viewmodelSpec[config.name + 'Store'] = {
						mtype: 'store',
						data: this.parseOptions(config.choiceValues),
						fields: ['name', 'value']
					}
				}
				break;

			case 'MultiSelectComboBoxField':
				config.xtype = 'combobox';
				config.displayField = 'name';
				config.valueField = 'value';
				config.editable = config.selectUserInput;
				config.multiSelect = true;
				config.delimiter = '; ';
				config.forceSelection = !config.selectUserInput;
				this.viewmodelSpec[config.name] = this.parseValue(config.defaultValue);
				config.store = Ext.create('Ext.data.Store', {
					data: this.parseOptions(config.choiceValues),
					fields: [{
						name: 'name'
					}, {
						name: 'value'
					}]
				});
				break;

			case 'MultipleCheckBox':
				config.xtype = 'checkboxgroup';
				config.columns = config.widgetColumns || 0;
				config.items = this.parseOptions(config.choiceValues, config.defaultValue, 'checkbox');
				config.listeners = Ext.applyIf(config.listeners || {}, {
					scope: this,
					change: function(checkboxgroup, newValue, oldValue, eOpts) {
						this.vm.set(checkboxgroup.configName, newValue[checkboxgroup.configName] || oldValue);
					}
				});
				config.hidden = '@{!' + config.name + 'IsVisible}';
				config.currentValue = '@{' + config.name + 'Compute}'
				config.setCurrentValue = function(value) {
					this.items.each(function(item) {
						Ext.each(value, function(val) {
							if (val == item.boxLabel)
								item.setValue(true);
						});
					});
				}
				if (!this.viewmodelSpec[config.name + 'IsVisible$']) this.viewmodelSpec[config.name + 'IsVisible$'] = function() {
					return true
				}
				this.viewmodelSpec[config.name] = this.parseValue(config.defaultValue);
				this.viewmodelSpec[config.name + 'Compute$'] = new Function(Ext.String.format('return this.{0}', config.name));
				Ext.Array.forEach(config.items, function(item) {
					if (Ext.isString(item.value)) item.value = false;
					Ext.apply(item, {
						name: config.name,
						readOnly: config.readOnly
					});
				});
				delete config.value;
				config.configName = config.name;
				delete config.name;
				break;

			case 'RadioButton':
				config.xtype = 'radiogroup';
				config.columns = config.widgetColumns || 0;
				config.items = this.parseOptions(config.choiceValues, config.defaultValue, 'radio');
				this.viewmodelSpec[config.name] = this.parseValue(config.defaultValue).join('');
				Ext.Array.forEach(config.items, function(item) {
					Ext.apply(item, {
						name: config.name,
						readOnly: config.readOnly
					});
				});
				delete config.value;
				config.configName = config.name;
				delete config.name;
				break;

			case 'Date':
				config.xtype = 'xdatefield';
				this.viewmodelSpec[config.name] = config.value || config.defaultValue || new Date()
				this.viewmodelSpec[config.name + 'IsDate'] = true
				if (!Ext.isDate(this.viewmodelSpec[config.name])) this.viewmodelSpec[config.name] = Ext.Date.parse(this.viewmodelSpec[config.name], 'c')
				break;

			case 'Timestamp':
				config.xtype = 'xtimefield';
				this.viewmodelSpec[config.name] = config.value || config.defaultValue || new Date()
				this.viewmodelSpec[config.name + 'IsDate'] = true
				if (!Ext.isDate(this.viewmodelSpec[config.name])) this.viewmodelSpec[config.name] = Ext.Date.parse(this.viewmodelSpec[config.name], 'c')
				config.store = '';
				break;

			case 'DateTime':
				config.xtype = 'xdatetimefield';
				config.hidden = '@{!' + config.name + 'IsVisible}';
				config.disabled = '@{!' + config.name + 'IsEnabled}';
				if (!this.viewmodelSpec[config.name + 'IsVisible$']) this.viewmodelSpec[config.name + 'IsVisible$'] = function() {
					return true
				}
				if (!this.viewmodelSpec[config.name + 'IsEnabled$']) this.viewmodelSpec[config.name + 'IsEnabled$'] = function() {
					return true
				}
				this.viewmodelSpec[config.name] = config.value || config.defaultValue || new Date()
				this.viewmodelSpec[config.name + 'IsDate'] = true
				if (!Ext.isDate(this.viewmodelSpec[config.name])) this.viewmodelSpec[config.name] = Ext.Date.parse(this.viewmodelSpec[config.name], 'c')
				config.items = [{
					xtype: 'hidden',
					name: config.name,
					listeners: {
						render: function(field) {
							if (Ext.isBoolean(field.disabled)) field.setDisabled(field.disabled);
							if (Ext.isBoolean(field.visible)) field.setVisible(field.visible);
						}
					},
					setVisible: function(visible) {
						this.ownerCt.setVisible(visible);
					},
					setDisabled: function(disabled) {
						this.ownerCt.setDisabled(disabled);
					}
				}];
				break;

			case 'Link':
				config.xtype = 'linkfield';
				config.listeners = Ext.applyIf(config.listeners || {}, {
					afterrender: function(field) {
						if (Ext.isBoolean(field.disabled)) field.setDisabled(field.disabled);
					}
				});
				break;

			case 'CheckBox':
				config.xtype = 'checkbox';
				this.viewmodelSpec[config.name] = config.value || (config.defaultValue === true || config.defaultValue === 'true' ? true : false);
				config.inputValue = 'true';
				delete config.fieldCls;
				break;

			case 'Password':
				config.xtype = 'textfield';
				config.inputType = 'password';
				break;

			case 'TagField':
				config.xtype = 'tagfield';
				break;

			case 'Sequence':
				config.xtype = 'displayfield';
				break;

			case 'List':
				config.xtype = 'listfield';
				config.parseOptions = this.parseOptions;
				config.parseValue = this.parseValue;
				config.items = [{
					xtype: 'hidden',
					name: config.name,
					listeners: {
						render: function(field) {
							if (Ext.isBoolean(field.disabled)) field.setDisabled(field.disabled);
							if (Ext.isBoolean(field.visible)) field.setVisible(field.visible);
						}
					},
					setVisible: function(visible) {
						this.ownerCt.setVisible(visible);
					},
					setDisabled: function(disabled) {
						this.ownerCt.setDisabled(disabled);
					}
				}];
				this.viewmodelSpec[config.name] = this.parseValue(config.defaultValue);
				break;

			case 'ButtonField':
				//deal with actions
				config.xtype = 'button';
				delete config.anchor;
				config.text = config.displayName;
				config.handler = this.handleButtonClick;
				config.scope = this;
				break;

			case 'Reference':
				config.xtype = 'referencefield';
				break;

			case 'Journal':
				config.xtype = 'journalfield';
				config.userId = this.userName;
				config.items = [{
					xtype: 'hiddenfield',
					name: config.name,
					listeners: {
						render: function(field) {
							if (Ext.isBoolean(field.disabled)) field.setDisabled(field.disabled);
							if (Ext.isBoolean(field.visible)) field.setVisible(field.visible);
						}
					},
					getValue: function() {
						var temp = this.ownerCt.journalEntries.slice(0),  // me.ownerCt.journalEntries is immutable
							newVal = this.ownerCt.down('textarea').getValue();
						if (newVal) {
							temp.splice(0, 0, {
								userId: this.ownerCt.userId,
								createDate: Ext.Date.format(new Date(), 'c'),
								note: Ext.htmlEncode(newVal).replace(/\n/g, '<br/>')
							});
						}
						return Ext.encode(temp);
					},
					setVisible: function(visible) {
						this.ownerCt.setVisible(visible);
					},
					setDisabled: function(disabled) {
						this.ownerCt.setDisabled(disabled);
					}
				}];
				break;

			case 'ReadOnlyTextField':
				config.xtype = 'displayfield';
				break;

			case 'UserPickerField':
				config.xtype = 'userpickerfield';
				config.meValue = this.userName;
				config.store = '';
				break;

			case 'TeamPickerField':
				config.xtype = 'teampickerfield';
				config.store = '';
				break;

			case 'EmailField':
				config.xtype = 'textfield';
				config.regex = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
				config.invalidText = 'The value in this field is invalid. It must be an email address like: someone@example.com';
				break;

			case 'MACAddressField':
				config.xtype = 'textfield';
				config.regex = /(?=^[0-9a-f]{2}([\.:-])(?:[0-9a-f]{2}\1){4}[0-9a-f]{2}$|^[0-9a-f]{2}([\.:-])(?:[0-9a-f]{2}\2){6}[0-9a-f]{2}$|^[0-9a-f]{3}([\.:-])(?:[0-9a-f]{3}\3){2}[0-9a-f]{3}$|^[0-9a-f]{4}([\.:-])(?:[0-9a-f]{4}\4){2}[0-9a-f]{4}$)/i;
				config.invalidText = 'The value in this field is invalid. It must follow standard 48 bits or 64 bits MAC Address Format';
				break;

			case 'IPAddressField':
				config.xtype = 'textfield';
				config.regex = /^(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))$/;
				config.invalidText = 'The value in this field is invalid. It must be an IP Address like: 192.168.0.1';
				break;

			case 'CIDRAddressField':
				config.xtype = 'textfield';
				config.regex = /^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2])){1}$/;
				config.invalidText = 'The value in this field is invalid. It must be an CIDR Address like: 192.168.0.1/14';
				break;

			case 'PhoneField':
				config.xtype = 'textfield';
				config.plugins = [new RS.formbuilder.viewer.desktop.InputTextMask('(999) 999 - 9999', undefined, 1)];
				break;

			case 'NameField':
				config.xtype = 'namefield';
				config.choiceSeparator = this.choiceSeparator;
				config.items = [{
					xtype: 'hiddenfield',
					name: config.name,
					listeners: {
						render: function(field) {
							if (Ext.isBoolean(field.disabled)) field.setDisabled(field.disabled);
							if (Ext.isBoolean(field.visible)) field.setVisible(field.visible);
						},
						change: function(panel, newValue, oldValue) {
							var choiceSeparator = this.up().choiceSeparator;
							var values = newValue.split(choiceSeparator);
							var idx = 0;
							var items = panel.ownerCt.items.items;
							for (var i = 0; i < items.length; i++) {
								if ((items[i].xtype === 'textfield' || items[i].xtype === 'combobox') && Ext.isFunction(items[i].setValue)) {
									items[i].setRawValue(values[idx++]);
								}
							}
						}
					},
					setVisible: function(visible) {
						this.ownerCt.setVisible(visible);
					},
					setDisabled: function(disabled) {
						this.ownerCt.setDisabled(disabled);
					}
				}];
				break;

			case 'AddressField':
				config.xtype = 'addressfield';
				config.choiceSeparator = this.choiceSeparator;
				config.items = [{
					xtype: 'hiddenfield',
					name: config.name,
					listeners: {
						render: function(field) {
							if (Ext.isBoolean(field.disabled)) field.setDisabled(field.disabled);
							if (Ext.isBoolean(field.visible)) field.setVisible(field.visible);
						},
						change: function(panel, newValue, oldValue) {
							var choiceSeparator = this.up().choiceSeparator;
							var values = newValue.split(choiceSeparator);
							var idx = 0;
							var items = panel.ownerCt.items.items;
							for (var i = 0; i < items.length; i++) {
								if (items[i].xtype === 'textfield' && Ext.isFunction(items[i].setValue)) {
									items[i].setRawValue(values[idx++]);
								} else if (items[i].xtype === 'fieldcontainer') {
									var subItems = items[i].items.items;
									for (var j = 0; j < subItems.length; j++) {
										if ((subItems[j].xtype === 'textfield' || subItems[j].xtype === 'combobox') && Ext.isFunction(subItems[j].setValue)) {
											subItems[j].setRawValue(values[idx++]);
										}
									}
								}
							}
						}
					},
					setVisible: function(visible) {
						this.ownerCt.setVisible(visible);
					},
					setDisabled: function(disabled) {
						this.ownerCt.setDisabled(disabled);
					}
				}];
				break;

			case 'FileUploadField':
				this.viewmodelSpec[config.name] = [];
				config.xtype = 'uploadmanager';
				config.formName = this.formName;
				config.recordId = this.recordId;
				break;

			case 'ReferenceTableField':
				this.viewmodelSpec[config.name] = [];
				config.xtype = 'referencetable';
				config.recordId = this.recordId;
				break;

			case 'HiddenField':
				config.xtype = 'hiddenfield';
				config.value = config.hiddenValue;
				break;

			case 'SpacerField':
				delete this.viewmodelSpec[config.name];
				config.xtype = 'component';
				if (config.showHorizontalRule) {
					config.html = '<hr/>'
				}
				delete config.name;
				break;

			case 'LabelField':
				config.xtype = 'label'
				config.text = config.displayName
				break;

			case 'WysiwygField':
				config.xtype = 'htmleditor';
				config.defaultValue = config.defaultValue || (Ext.isOpera || Ext.isIE6) ? '&#160;' : '&#8203;'
				break;

			case 'TabContainerField':
				this.viewmodelSpec[config.name] = [];
				config.xtype = 'tabcontainerfield';
				config.formName = this.formName;
				config.recordId = this.recordId;
				break;

			case 'UrlContainerField':
				this.viewmodelSpec[config.name] = [];
				config.xtype = 'urlcontainerfield';
				break;

			case 'SectionContainerField':
				if (config.height == 25) delete config.height;
				config.xtype = config.sectionType;
				config.border = false;
				config.bodyPadding = '10px 0px 10px 0px';
				config.layout = {
					type: 'hbox',
					align: 'stretch'
				};
				config.defaults = {
					flex: 1,
					border: false
				};
				config.collapsible = true;
				config.title = config.sectionTitle;
				var cols = [],
					decoded = Ext.decode(config.sectionColumns);
				Ext.Array.forEach(decoded, function(column) {
					var ctrls = [];
					Ext.Array.forEach(column.controls, function(control) {
						if (control) {
							this.convertControl(control)
							ctrls.push(control);
						}
					}, this);
					cols.push({
						layout: 'anchor',
						items: ctrls
					})
				}, this)
				config.items = cols;
				break;

			default:
				config.xtype = 'textfield'

				break;
		}
	},

	getVariableString: function(config) {
		var variables = '';
		if (config.choiceOptionSelection == 1 && config.choiceField && config.choiceWhere) {
			variables = Ext.String.format('this.{0}', config.choiceField);
		} else {
			config.choiceSql = config.choiceSql || '';
			var vars = config.choiceSql.match(/\$\{(.*?)\}/g)
			Ext.Array.forEach(vars || [], function(variable, index) {
				if (index > 0) variables += ' && ';
				variables += 'this.' + variable.substring(2, variable.length - 1);
			});
		}

		return variables;
	},

	defaultFont: 'Verdana',
	defaultFontSize: '10',
	defaultFontColor: '333333',
	defaultBackgroundColor: 'DDDDDD',

	// Approximate fontsize conversion from pt to px
	fontSizePtMapping : {
		6 : 8,
		8 : 11,
		9 : 12,
		10 : 13,
		12 : 16,
		14 : 19,
		18 : 24,
		24 : 32,
		30 : 40,
		36 : 48,
		48 : 64,
		60 : 80
	},

	/**
	 * Converts a server configuration of a button into a button that can be rendered by extjs
	 * @param {Object} button
	 */
	convertButton: function(button) {
		button.text = button.displayName || "Fix Me!";
		button.handler = this.handleButtonClick;
		button.sysId = button.id;
		button.scope = this;

		if (Ext.firefoxVersion !== 0) {
			// RBA-12536: this is Ext JS 4 bug. We are putting a bandage to get around the issue
			button.cls = 'fixFirefoxIssue';
		}
		switch (button.type) {
			case 'separator':
				button.xtype = 'tbseparator';
				break;
			case 'spacer':
				button.xtype = 'tbspacer';
				break;
			case 'rightJustify':
				button.xtype = 'tbfill';
				break;
			case 'button':
				var fontFamily = button.font+', Tahoma' || this.defaultFont+', Tahoma',
					fontSize,
					color = '#' + (button.fontColor ? button.fontColor : this.defaultFontColor),
					backgroundColor = '#' + (button.backgroundColor ? button.backgroundColor : this.defaultBackgroundColor);

				if (button.fontSize && this.fontSizePtMapping[parseInt(button.fontSize)]) {
					fontSize = parseInt(button.fontSize);
				} else {
					fontSize = parseInt(this.defaultFontSize);
				}

				var style = {
					fontFamily: fontFamily,
					fontSize: this.fontSizePtMapping[fontSize] + 'px',
					color: color,
					backgroundColor: backgroundColor
				}
				Ext.apply(button, {
					style: style,
					height: this.fontSizePtMapping[fontSize] + 10
				});
			default:
				button.xtype = 'button';
				break;
		}

		//determine crud action for db type actions
		button.crudAction = this.getCrudAction(button);

		//make sure we have name for the glu dependencies
		button.name = 'button_' + button.id;
		//setup standard glu dependencies for next/back buttons
		//conflicts with glu bindings
		delete button.hidden;

		//delete id because it screws things up to have an id on extjs controls
		delete button.id;
		this.parseButtonDependencies(button);
	},
	getCrudAction: function(button) {
		var i, actions = button.actions || [],
			len = actions.length,
			action;
		for (i = 0; i < len; i++) {
			action = actions[i];
			if (action.operation == 'DB' || action.operation == 'SIR') return action.crudAction;
		}
	},
	/**
	 * Parses values out of a select or choice control
	 * @param {Object} configValue
	 */
	parseValue: function(configValue) {
		var returnValue = [],
			configValue = configValue || '';
		var values = Ext.isString(configValue) ? configValue.split(this.choiceSeparator) : configValue;
		Ext.each(values, function(value) {
			if (value) {
				var val = value.split(this.choiceValueSeparator);
				var nameVal = val[0].split('=');
				if (nameVal.length == 1) nameVal.push(nameVal[0]);
				returnValue.push(val[1] == 'true' ? nameVal[1] : val[0]);
			}
		}, this);
		return returnValue;
	},
	/**
	 * Parses options out of a select or choice control
	 * @param {Object} optionString
	 * @param {Object} valueString
	 */
	parseOptions: function(optionString, valueString, type) {
		var options = [],
			optionString = optionString || '',
			defaultOptions = {};
		if (type) defaultOptions.xtype = type;
		var split = optionString.split(this.choiceSeparator);
		Ext.each(split, function(option) {
			var nameValue = option.split(this.choiceValueSeparator);
			if (type != 'radio') defaultOptions.value = nameValue[1] && (nameValue[1] != 'true' || nameValue[1] != 'false') ? nameValue[1] : nameValue[0];
			if (nameValue[0]) {
				var nameValCombo = nameValue[0].split('=');
				if (nameValCombo.length == 1) nameValCombo.push(nameValCombo[0]);
				if (!type) {
					defaultOptions.name = nameValCombo[0];
					defaultOptions.value = nameValCombo[1];
				}
				options.push(Ext.apply({
					boxLabel: nameValCombo[0],
					inputValue: nameValCombo[1],
					xtype: type,
					hideLabel: true
				}, defaultOptions))
			}
			delete defaultOptions.value;
			delete defaultOptions.name;
		}, this);
		if (valueString) {
			var values = valueString.split(this.choiceSeparator),
				i, j;
			for (i = 0; i < values.length; i++) {
				var val = values[i].split(this.choiceValueSeparator);
				// if(val[1] == 'true') {
				for (j = 0; j < options.length; j++) {
					var nameVal = val[0].split('=');
					if (nameVal.length == 1) nameVal.push(nameVal[0]);
					if (options[j].boxLabel == nameVal[0]) options[j][type == 'checkbox' ? 'value' : 'checked'] = true;
					else options[j][type == 'checkbox' ? 'value' : 'checked'] = options[j][type == 'checkbox' ? 'value' : 'checked'] == true || false;
				}
				// }
			}
		}
		return options;
	},
	/**
	 * Handles the button clicks to actions relationship and will perform the appropriate posting of the values to be processed by the server.
	 * Deals with reset and close actions client side if everything goes well on the server side.
	 * @param {Object} button
	 */
	handleButtonClick: function(button) {
		//If we're in the middle of uploading, then message the user that they can't perform actions until the upload is done
		var uploadForm = button.up('form'),
			uploader = uploadForm.down('uploadmanager');
		if (uploader && uploader.isUploading()) {
			Ext.Msg.alert(RS.formbuilder.locale.uploadInProgressTitle, RS.formbuilder.locale.uploadInProgressBody);
			return;
		}
		//Handle actions
		var shouldReset = false,
			shouldReload = false,
			reloadDelay = 0,
			reloadTarget = '',
			reloadCondition = '',
			reloadValue = '',
			shouldClose = false,
			shouldExit = false,
			shouldReturn = false,
			submitToServer = false,
			automationExecuted = false,
			actionParams = {},
			displayMessage = false,
			messageBoxTitle = '',
			messageBoxMessage = '',
			messageBoxYesText = '',
			messageBoxNoText = '';
		Ext.each(button.actions, function(action) {
			switch (action.operation) {
				case 'RESET':
					shouldReset = true;
					break;
				case 'CLOSE':
					shouldClose = true;
					break;
				case 'EXIT':
					shouldExit = true;
					break;
				case 'RETURN':
					shouldReturn = true
					break;
				case 'MESSAGEBOX':
					displayMessage = true;
					if (action.additionalParam && Ext.isString(action.additionalParam)) {
						var decode = this.deserializeParams(action.additionalParam);
						messageBoxTitle = Ext.String.htmlEncode(decode.messageBoxTitle);
						messageBoxMessage = decode.messageBoxMessage;
						messageBoxYesText = decode.messageBoxYesText;
						messageBoxNoText = decode.messageBoxNoText;
					}
					break;
				case 'RELOAD':
					shouldReload = true;
					if (action.additionalParam && Ext.isString(action.additionalParam)) {
						var decode = this.deserializeParams(action.additionalParam);
						reloadDelay = decode.reloadDelay;
						reloadTarget = decode.reloadTarget;
						reloadCondition = decode.reloadCondition;
						reloadValue = decode.reloadValue;
					}
					break;
				case 'DB':
					if (action.additionalParam && Ext.isString(action.additionalParam)) {
						var decode = this.deserializeParams(action.additionalParam);
						if (decode.sirAction && decode.sirAction == 'saveCaseData') {
							action.operation = 'SIR';
							button.crudAction = 'saveCaseData';
							action.additionalParam = '';
						}
					}
				case 'SIR':
				case 'EVENT':
				case 'RUNBOOK':
				case 'ACTIONTASK':
				case 'SCRIPT':
				case 'REDIRECT':
				case 'MAPPING':
					automationExecuted = true;
					submitToServer = true;
					if (action.additionalParam && Ext.isString(action.additionalParam)) {
						var decode = this.deserializeParams(action.additionalParam);
						for (var key in decode) {
							if (!decode[key]) delete decode[key];
						}
						Ext.applyIf(actionParams, decode);
						if (actionParams.showConfirmation) {
							displayMessage = true;
							messageBoxTitle = RS.formbuilder.locale.confirmDeleteTitle;
							messageBoxMessage = RS.formbuilder.locale.confirmDeleteMessage;
							messageBoxYesText = RS.formbuilder.locale.confirmDeleteYesText;
							messageBoxNoText = RS.formbuilder.locale.confirmDeleteNoText;
						}
					}
					break;
				case 'NEXT':
					var tabPanel = this.down('tabpanel');
					if (tabPanel) {
						var nextIndex = this.vm.activeTabIndex + 1,
							nextTab = tabPanel.items.getAt(nextIndex);
						while (tabPanel.items.getAt(nextIndex) && (tabPanel.items.getAt(nextIndex).tab.hidden || tabPanel.items.getAt(nextIndex).tab.disabled)) {
							nextIndex += 1;
						}
						if (tabPanel.items.getAt(nextIndex)) this.vm.set('activeTabIndex', nextIndex);
					}
					break;
				case 'BACK':
					var tabPanel = this.down('tabpanel');
					if (tabPanel) {
						var previousIndex = this.vm.activeTabIndex - 1,
							previousTab = tabPanel.items.getAt(previousIndex);
						while (tabPanel.items.getAt(previousIndex) && (tabPanel.items.getAt(previousIndex).tab.hidden || tabPanel.items.getAt(previousIndex).tab.disabled)) {
							previousIndex -= 1;
						}
						if (tabPanel.items.getAt(previousIndex)) this.vm.set('activeTabIndex', previousIndex);
					}
					break;
				case 'GOTO':
					var tabPanel = this.down('tabpanel');
					if (tabPanel) {
						var decode = this.deserializeParams(action.additionalParam),
							index = -1;
						var gotoTabName = decode['gotoTabName']
						gotoTabName = gotoTabName.replace(/[^\w]/g, '')
						tabPanel.items.each(function(tab, idx) {
							if (tab.name === gotoTabName) index = idx
						}, this)
						if (index > -1)
							this.vm.set('activeTabIndex', index)
					}
					break;
				default:
					Ext.Msg.alert(RS.formbuilder.locale.unknownAction, Ext.String.format(RS.formbuilder.locale.unknownActionBody, action.operation));
					break;
			}
		}, this);

		if (shouldReturn) {
			clientVM.handleNavigationBack()
		}

		button.shouldSubmitToServer = submitToServer;
		button.shouldReset = shouldReset;
		button.shouldClose = shouldClose;
		button.shouldExit = shouldExit;
		button.actionParams = actionParams;
		button.shouldReload = shouldReload;
		button.reloadDelay = reloadDelay;
		button.reloadTarget = reloadTarget;
		button.reloadCondition = reloadCondition;
		button.reloadValue = reloadValue;
		button.automationExecuted = automationExecuted;

		if (displayMessage) {
			Ext.MessageBox.show({
				title: messageBoxTitle,
				msg: messageBoxMessage,
				buttons: Ext.MessageBox.YESNO,
				buttonText: {
					yes: messageBoxYesText,
					no: messageBoxNoText
				},
				scope: button,
				fn: this.handleMessage
			});
			return;
		}

		if (submitToServer) {
			this.submitToServer(button);
		} else {
			if (shouldReset) {
				button.up('form').getForm().reset();
				this.getDefaultData();
			}
			if (shouldExit && this.fireEvent('beforeexit') !== false) {
				var win = button.up('window');
				if (win)
					win.close()
				else
					clientVM.handleNavigationBackOrExit()
			}
			if (shouldClose && this.fireEvent('beforeclose') !== false) {
				var win = button.up('window') || getResolveRoot();
				win.close();
			}
			if (shouldReload) {
				if (this.recordId) Ext.defer(this.setRecordId, button.reloadDelay * 1000, this, [this.recordId, button]);
				else Ext.defer(this.getDefaultData, button.reloadDelay * 1000, this, []);
			}
			this.fireEvent('actioncompleted', this, button);
		}
	},
	handleMessage: function(button) {
		if (button === 'yes') {
			var form = this.up('form'),
				rsForm = this.up('rsform');
			if (this.shouldSubmitToServer) rsForm.submitToServer(this);
			else {
				if (this.shouldReset) {
					this.getDefaultData();
				}
				if (this.shouldExit && this.fireEvent('beforeexit') !== false) {
					var win = button.up('window');
					if (win)
						win.close()
					else
						clientVM.handleNavigationBackOrExit()
				}
				if (this.shouldClose && this.fireEvent('beforeclose') !== false) {
					var win = this.up('window') || getResolveRoot();
					win.close();
				}
				this.fireEvent('actioncompleted', this, button);
			}
		}
	},
	getDefaultData: function() {
		Ext.Ajax.request({
			url: this.API['getDefaultData'],
			params: {
				view: this.formName,
				formsysid: this.formId,
				recordid: this.recordId
			},
			scope: this,
			success: function(r) {
				this.processDataRequest(r)
				this.processUrlVariables()
				this.getWSData()
				this.getATProperty()
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		});
	},

	getATProperty: function() {
		var properties = {}
		var filters = []
		Ext.Object.each(this.vm, function(key, value) {
			if (Ext.isString(value) && /\$PROPERTY\{(.*)\}/gi.test(value)) {
				var k = /\$PROPERTY\{(.*)\}/gi.exec(value)[1]
				if (k) {
					properties[k] = key
					filters.push({
						field: 'uname',
						type: 'auto',
						condition: 'equals',
						value: k
					});
				}
			}
		}, this)
		if (filters.length == 0)
			return;
		Ext.Ajax.request({
			url: this.API['getATProperty'],
			scope: this,
			params: {
				limit: -1,
				operator: 'or',
				filter: Ext.encode(filters)
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					//clientVM.displayError(this.localize('getPropertyError', respData.message))
					clientVM.displayError(respData.message);
					return
				}
				Ext.each(respData.records, function(r) {
					if (properties[r.uname])
						this.vm.set(properties[r.uname], r.uvalue);
				}, this)
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})

	},

	getWSData: function() {
		//Now that we have all the default data from the form and the url params processed, we need to apply any WSDATA data
		Ext.Ajax.request({
			url: this.API['getWSData'],
			params: {
				problemId: clientVM.problemId
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					//Process through fields, looking for their default values to be set to something that is in the WSDATA
					//Something like ${test} means test: 'seomthing' must exist in the WSDATA
					Ext.Object.each(this.vm, function(key, value) {
						if (Ext.isString(value) && /\$WSDATA\{(.*)\}/gi.test(value)) {
							var k = /\$WSDATA\{(.*)\}/gi.exec(value)[1]
							if (k && Ext.isDefined(response.data[k])) {
								this.vm.set(key, response.data[k])
							}
							else
								//Default to blank value if couldnt find a match.
								this.vm.set(key, '');
						}
					}, this)
				} else
					clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	resetForm: function() {
		this.down('form').getForm().reset()
	},

	saveForm: function() {
		var saveButton = null;
		this.down('form').dockedItems.each(function(item) {
			if (item.isXType('toolbar')) {
				item.items.each(function(button) {
					if (button.actions && Ext.isArray(button.actions)) {
						Ext.Array.forEach(button.actions, function(action) {
							if (action.crudAction == 'SUBMIT') {
								saveButton = button
							}
						})
					}
					if (button) return false
				})
			}
		})

		if (!saveButton) {
			clientVM.displayError(RS.formbuilder.locale.noSaveButtonBody, 10000, RS.formbuilder.locale.noSaveButtonTitle);
		} else this.handleButtonClick(saveButton)
	},
	submitToServer: function(button, form) {
		if (!form) form = button.up('form')

		if (!form.getForm().isValid()) {
			clientVM.displayError(RS.formbuilder.locale.invalidFormBody, 10000, RS.formbuilder.locale.invalidFormTitle);			
			return;
		}

		if (!button) {
			button = {}
		} else {
			//Change button to show "working" state
			button.disable();
			button.setIcon('/resolve/images/loading.gif');
		}

		var params = form.getForm().getValues(),
			serverParams = this.submitConfiguration,
			key;

		//The server needs to know the id of the button that was clicked to know which actions to perform
		serverParams.controlItemSysId = button.sysId;
		serverParams.crudAction = button.crudAction;

		//Go get the displayField information too
		var displayFields = form.query('displayfield');
		Ext.each(displayFields, function(displayField) {
			params[displayField.getName()] = displayField.getValue();
		});

		//Because they are so different, do the same with journal fields
		var journalFields = form.query('journalfield');
		Ext.each(journalFields, function(journalField) {
			var hidden = journalField.down('hiddenfield');
			if (hidden) {
				var name = hidden.getName(),
					value = hidden.getValue();
				params[name] = value;
				if (Ext.isDefined(this.vm[name])) this.vm.set(name, value);
			}
		}, this);

		//Prepare the params for form submission
		var dbData = {},
			inputData = {},
			paramType, actionParams = button.actionParams;

		//Take the action params, and if the PROBLEMID or EVENT_REFERENCE is a field, then populate them with the field value instead
		if (actionParams && actionParams.PROBLEMID && actionParams.PROBLEMID == 'MAPFROM' && params[actionParams.mappingFrom]) {
			params.PROBLEMID = params[actionParams.mappingFrom];
			params.EVENT_REFERENCE = params[actionParams.mappingFrom];
		}

		for (key in params) {
			paramType = this.getParamType(key);
			if (paramType === 'DB') dbData[key] = Ext.isArray(params[key]) ? this.serializeChoice(params[key]) : params[key]
			else if (paramType === 'INPUT') inputData[key] = Ext.isArray(params[key]) ? this.serializeChoice(params[key]) : params[key];
		}

		// if button action is "saveCaseData" & customDataForm, use formRecord from customDataForm (if exist)
		if (button.crudAction === 'saveCaseData' && clientVM.customDataForm) {
			if (clientVM.customDataForm && clientVM.customDataForm.formNames && clientVM.customDataForm.formNames[serverParams.viewName]) {
				Ext.apply(dbData, {
					sys_id: clientVM.customDataForm.formNames[serverParams.viewName]
				});
			}
		} 
		//Make sure the sys_id is in there if it came from the server
		else if (this.recordId) Ext.apply(dbData, {
			sys_id: this.recordId
		});

		serverParams.dbRowData.data = dbData;
		serverParams.sourceRowData.data = inputData;

		if (clientVM.activeScreen.parentVM && clientVM.activeScreen.parentVM.sirContext) {
			var sirContextVM = clientVM.activeScreen.parentVM;
			if (sirContextVM.ticketInfo && sirContextVM.ticketInfo.problemId) {
				serverParams.sourceRowData.data.PROBLEMID = sirContextVM.ticketInfo.problemId;	// SIR problemId has highest precedence
				serverParams.sourceRowData.data.incidentId = sirContextVM.ticketInfo.id;
				serverParams.sourceRowData.data.phase = sirContextVM.activitiesTab.activitiesView.phase;
				serverParams.sourceRowData.data.activityName = sirContextVM.activitiesTab.activitiesView.activityName;
			}
			if (sirContextVM.activityId) {
				serverParams.sourceRowData.data.activityId = sirContextVM.activityId;
			}
		} else if (clientVM.SIR_SELECTED_ACTIVITY) {
				serverParams.sourceRowData.data.PROBLEMID = clientVM.SIR_PROBLEMID;	// SIR problemId has highest precedence
				serverParams.sourceRowData.data.incidentId = clientVM.SIR_SELECTED_ACTIVITY.incidentId;
				serverParams.sourceRowData.data.phase = clientVM.SIR_SELECTED_ACTIVITY.phase;
				serverParams.sourceRowData.data.activityName = clientVM.SIR_SELECTED_ACTIVITY.activityName;		
				serverParams.sourceRowData.data.activityId = clientVM.SIR_SELECTED_ACTIVITY.id;			
		} else {
			actionParams.PROBLEMID = clientVM.SIR_PROBLEMID || this.problemId || actionParams.PROBLEMID;  // URL problemId has higher precedence
		}

		//Apply any additional parameters from the action that is being performed
		Ext.applyIf(serverParams.sourceRowData.data, actionParams);
		Ext.apply(serverParams.sourceRowData.data, {
			'RESOLVE.ORG_NAME' : clientVM.orgName,
			'RESOLVE.ORG_ID' : clientVM.orgId
		})
		//Apply the current runbook that the form is on
		serverParams.currentDocumentName = window['wikiDocumentName'] || ''

		Ext.Ajax.request({
			url: this.API['submit'],
			jsonData: serverParams,
			scope: this,
			success: function(r) {
				if (!button.shouldReload && button.enable) {
					button.enable();
					button.setIcon('');
				}
				var response = RS.common.parsePayload(r);
				if (response.success) {

					//update problemId and problemNumber on the client if possible
					if (clientVM.SIR_PROBLEMID) {
						(clientVM || getResolveRoot().clientVM).fireEvent('refreshResult');
						// if button action is "saveCaseData" and we have customDataForm, then also submit the data to Custom Form Data
						if (button.crudAction === 'saveCaseData' && clientVM.SIR_INCIDENTID && clientVM.customDataForm) {
							this.submitCustomDataForm(response.data);
						}
					}
					else if (button.actionParams.PROBLEMID == 'ACTIVE') {
						Ext.defer(function() {
							(clientVM || getResolveRoot().clientVM).fireEvent('problemIdChanged');
						}, 500)
					} else if (button.actionParams.PROBLEMID == 'NEW' && response.data.PROBLEMID && getResolveRoot().clientVM) {
						clientVM.updateProblemInfo(response.data.PROBLEMID);
					} else if (button.automationExecuted && (clientVM || getResolveRoot().clientVM)) {
						Ext.defer(function() {
							(clientVM || getResolveRoot().clientVM).fireEvent('worksheetExecuted');

							// trigger refresh result since worksheetExecuted is currently not doing anything
							(clientVM || getResolveRoot().clientVM).fireEvent('refreshResult');
						}, 500)
					}

					if (button.crudAction === 'saveCaseData' && clientVM.customDataForm) {
						// do nothing for custom data form
					} else {
						//Populate sys_id if it comes back from the server so that any further actions are updates until reloaded
						if (response.data.DB_SYS_ID) this.recordId = response.data.DB_SYS_ID;
					}


					//TODO: errorException

					//Redirect the user if they specified a redirect action
					if (response.data.REDIRECT) {
						var redirect = response.data.REDIRECT,
							executeRedirect = true;
						if (response.data.REDIRECT_TARGET == '_self' && this.fireEvent('beforeredirect', response.data.REDIRECT) === false) {
							executeRedirect = false
						}

						if (executeRedirect) {
							if (redirect.indexOf('#') == 0 || redirect.indexOf('RS.') == 0) {
								var splits = redirect.indexOf('#') == 0 ? redirect.substring(1).split('/') : redirect.split('/')
								clientVM.handleNavigation({
									modelName: splits[0],
									target: response.data.REDIRECT_TARGET,
									params: Ext.Object.fromQueryString(splits[1])
								})
							} else if (redirect.indexOf('/') == 0) {
								clientVM.handleNavigation({
									modelName: 'RS.client.URLScreen',
									target: response.data.REDIRECT_TARGET,
									params: {
										location: redirect
									}
								})
							} else {
								window.open(response.data.REDIRECT.replace(/${sys_id}/g, this.recordId), response.data.REDIRECT_TARGET)
							}
						}
					}

					if (button.shouldReset) {
						this.down('form').getForm().reset();
						this.recordId = '';
						this.getDefaultData();
					}
					//display success message
					clientVM.displaySuccess(RS.formbuilder.locale.formSavedSuccessfully, null, RS.formbuilder.locale.success);
					if (button.shouldExit && this.fireEvent('beforeexit') !== false) {
						var win = button.up('window');
						if (win)
							Ext.defer(function() {
								win.close();
							}, 3000);
						else
							Ext.defer(function() {
								clientVM.handleNavigationBackOrExit()
							}, 3000);
					}

					if (button.shouldClose && this.fireEvent('beforeclose') !== false) {
						var win = this.up('window') || getResolveRoot();
						Ext.defer(function() {
							win.close();
						}, 3000);
					}

					var managers = this.query('uploadmanager');
					Ext.each(managers, function(manager) {
						manager.recordId = this.recordId;
						manager.setEnabled(this.recordId);
					}, this);

					if (button.shouldReload && this.recordId) {
						Ext.defer(this.setRecordId, button.reloadDelay * 1000, this, [this.recordId, button]);
					}
					this.fireEvent('actioncompleted', this, button);
				} else {
					//display error message
					clientVM.displayError(response.message);
				}
			},
			failure: function(r) {
				if (button.enable) {
					button.enable();
					button.setIcon('');
				}
				clientVM.displayFailure(r);
			}
		});
	},
	submitCustomDataForm: function(data, onSuccess, onFailure) {
		if (clientVM.SIR_INCIDENTID && clientVM.customDataForm) {
			if (!clientVM.customDataForm.formNames) {
				clientVM.customDataForm.formNames = {};
			}
			if (!clientVM.customDataForm.formNames[this.formName]) {
				clientVM.customDataForm.formNames[this.formName] = data.DB_SYS_ID;
			}
			var formRecord = {};
			formRecord[this.formName] = data.DB_SYS_ID;
			Ext.Ajax.request({
				url: '/resolve/service/playbook/customdata',
				jsonData: {
					incidentId: clientVM.SIR_INCIDENTID,
					wikiId: clientVM.customDataForm.baseWikiId,
					formRecord: formRecord,
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) {
						if (Ext.isFunction(onSuccess)) {
							onSuccess();
						}
					} else {
						if (Ext.isFunction(onFailure)) {
							onFailure();
						}
						clientVM.displayError(response.message);
					}
				},
				failure: function(resp) {
					if (Ext.isFunction(onFailure)) {
						onFailure();
					}
					clientVM.displayFailure(resp);
				}
			});
		}
	},
	verifyWS: function(worksheetId, onSuccess, onFailure) {
		Ext.Ajax.request({
			url: '/resolve/service/worksheet/getWS',
			params: {
				id: worksheetId
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					if (Ext.isFunction(onSuccess)) {
						onSuccess();
					}
				} else {
					if (Ext.isFunction(onFailure)) {
						onFailure();
					}
					clientVM.displayError(response.message);
				}
			},
			failure: function(resp) {
				if (Ext.isFunction(onFailure)) {
					onFailure();
				}
				clientVM.displayFailure(resp);
			}
		});
	},
	deserializeParams: function(paramString) {
		var params = {},
			paramsSplit = paramString.split(this.choiceSeparator);

		Ext.each(paramsSplit, function(split) {
			var val = split.split(this.choiceValueSeparator);
			if (val.length > 1) {
				params[val[0]] = val[1];
			}
		}, this);

		return params;
	},
	serializeChoice: function(paramsArray) {
		var params = [];
		Ext.each(paramsArray, function(param) {
			if (param) params.push(param);
		}, this);

		return params.join(this.choiceSeparator);
	},
	getParamType: function(param) {
		var i, j, tab, column, f, field, returnType;
		for (i = 0; i < this.fieldConfiguration.length; i++) {
			tab = this.fieldConfiguration[i]
			for (j = 0; j < tab.columns.length; j++) {
				column = tab.columns[j]
				if (column) {
					for (f = 0; f < column.fields.length; f++) {
						field = column.fields[f]
						returnType = this.getParamTypeOfField(field, param)
						if (returnType) return returnType
					}
				}
			}
		}

		if (param == 'EVENT_REFERENCE' || param == 'PROBLEMID') return 'INPUT';

		//unknown field so we need to return blank here
		return '';
	},

	getParamTypeOfField: function(field, param) {
		if (field.name == param || field.configName == param) return field.sourceType
		if (field.uiType == 'SectionContainerField') {
			var sc, sectionColumn, sf, sectionField, returnType
			sectionColumns = Ext.decode(field.sectionColumns) || [];
			for (sc = 0; sc < sectionColumns.length; sc++) {
				sectionColumn = sectionColumns[sc]
				for (sf = 0; sf < sectionColumn.controls.length; sf++) {
					sectionField = sectionColumn.controls[sf]
					if (sectionField) {
						returnType = this.getParamTypeOfField(sectionField, param)
						if (returnType) return returnType
					}
				}
			}
		}
		return null
	},

	setRecordId: function(recordId, button) {
		this.recordId = recordId;

		var queryParams = this.queryString ? Ext.Object.fromQueryString(this.queryString) : {};
		if (this.recordId) Ext.apply(queryParams, {
			sys_id: this.recordId
		});

		var filter = [];
		for (var key in queryParams) {
			var field = key;
			if (key == 'u_content_request' && this.submitConfiguration.tableName != 'content_request') {
				field += '.sys_id';
			}

			filter.push({
				field: field,
				type: 'auto',
				condition: 'equals',
				value: queryParams[key]
			});
		}

		if (this.showLoading)
			Ext.MessageBox.show({
				msg: RS.formbuilder.locale.loadingMessage,
				progressText: RS.formbuilder.locale.loadingProgress,
				width: 300,
				wait: true,
				waitConfig: {
					interval: 200
				}
			});

		if (filter.length && this.submitConfiguration.tableName) {
			Ext.Ajax.request({
				url: this.API['getRecordData'],
				params: {
					tableName: this.submitConfiguration.tableName,
					type: 'form',
					start: 0,
					filter: Ext.encode(filter)
				},
				scope: this,
				success: function(r) {
					if (this.showLoading) Ext.MessageBox.hide()
					Ext.each(Ext.dom.Query.select('div[class=x-mask]'), function(el) {
						Ext.fly(el).setStyle('display', 'none');
					});
					this.doLayout();

					Ext.defer(function() {
						Ext.each(Ext.dom.Query.select('div[class=x-mask]'), function(el) {
							Ext.fly(el).setStyle('display', 'none');
						});
						this.doLayout();
					}, 500, this);

					this.processDataRequest(r, button);
				},
				failure: function(r) {
					if (this.showLoading) Ext.MessageBox.hide()
					clientVM.displayFailure(r);
				}
			})
		}
	},

	processUrlVariables: function() {
		if (!this.rendered || !this.vm) return;
		var theRootWnd = window;
		var params = {};
		var hasParams = false;
		if (this.params) {
			hasParams = true;
			params = Ext.Object.fromQueryString(this.params)
		} else {
			try {
				while (theRootWnd.parent != theRootWnd) {
					if (theRootWnd.location.search) {
						hasParams = true;
						Ext.apply(params, Ext.Object.fromQueryString(theRootWnd.location.search));
					}
					theRootWnd = theRootWnd.parent;
				}
				if (theRootWnd.location.search) {
					hasParams = true;
					Ext.apply(params, Ext.Object.fromQueryString(theRootWnd.location.search));
				}
			} catch (e) {
				//cross-site security
			}
		}

		if (hasParams) {
			for (var key in params) {
				if (Ext.isDefined(this.vm[key])) this.vm.set(key, params[key]);
			}
			//Special processing for radios/checkboxes because the bindings don't work with extjs's messed up structure, we have to manually set the values
			var components = this.query('radiogroup, checkboxgroup');
			Ext.each(components, function(component) {
				var name = component.name;
				if (!name) name = component.items.getAt(0).name;
				if (Ext.isDefined(params[name])) {
					var obj = {};
					obj[name] = params[name];
					component.setValue(obj);
				}
			});
			//Special processing for hidden variables
			var hiddenFields = this.query('hiddenfield');
			Ext.each(hiddenFields, function(hiddenField) {
				if (Ext.isDefined(params[hiddenField.name])) {
					hiddenField.setValue(params[hiddenField.name]);
				}
			});
		}
	},
	parseDependencies: function(controlConfig) {
		this.applyDependencies(controlConfig, controlConfig.dependencies);
	},
	parseButtonDependencies: function(buttonConfig) {
		var dependencies = buttonConfig.dependencies || [];
		Ext.each(buttonConfig.actions, function(action) {
			if (action.operation == 'NEXT') dependencies.push({
				target: 'activeTabIndex',
				action: 'isVisible',
				condition: 'lessThan',
				value: 'this.tabCount-1',
				template: '{0} {1} {2}'
			});
			if (action.operation == 'BACK') dependencies.push({
				target: 'activeTabIndex',
				action: 'isVisible',
				condition: 'greaterThan',
				value: '0',
				template: '{0} {1} {2}'
			});
		});
		this.applyDependencies(buttonConfig, dependencies);
	},
	applyDependencies: function(source, dependencies) {
		var actionFunctions = {},
			actionOptions = {};
		Ext.each(dependencies, function(dependency) {
			var values = (dependency.value || '').split(/[\||,]/),
				target = 'this.' + dependency.target,
				targetFormField = this.getFormControl(dependency.target),
				targetFieldType = this.getControlType(targetFormField),
				functionString = '',
				actionKey = '',
				condition = '==',
				template = dependency.template;

			switch (dependency.condition) {
				case 'isEmpty':
				case 'equals':
				case 'on':
					condition = '==';
					break;
				case 'notequals':
				case 'notEmpty':
					condition = '!=';
					break;
				case 'greaterThan':
				case 'after':
					condition = '>';
					break;
				case 'greaterThanOrEqual':
					condition = '>=';
					break;
				case 'lessThan':
				case 'before':
					condition = '<';
					break;
				case 'lessThanOrEqual':
					condition = '<=';
					break;
				case 'contains':
					template = '{0} == null ? false : {0}.toString().indexOf("{2}") > -1';
					condition = dependency.condition;
					break;
				case 'notcontains':
					template = '{0} == null ? false : {0}.toString().indexOf("{2}") == -1';
					condition = dependency.condition;
					break;
			}

			Ext.each(values, function(value, index) {
				if (index > 0) functionString += ' || ';
				var val = Ext.String.trim(value);

				if (dependency.condition === 'isEmpty' || dependency.condition === 'notEmpty') {
					val = "";
					template = '{0} {1} "{2}"';
				} else if (targetFieldType === "date") {
					var dateString = val;
					switch (dateString.toLowerCase()) {
						case 'today':
							dateString = Ext.Date.format(new Date(), 'Y-m-d');
							break;
						case 'yesterday':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, -1), 'Y-m-d');
							break;
						case 'tomorrow':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, 1), 'Y-m-d');
							break;
						case 'last week':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, -7), 'Y-m-d');
							break;
						case 'next week':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, 7), 'Y-m-d');
							break;
						case 'last month':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.MONTH, -1), 'Y-m-d');
							break;
						case 'next month':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.MONTH, 1), 'Y-m-d');
							break;
						case 'last year':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, -1), 'Y-m-d');
							break;
						case 'next year':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, 1), 'Y-m-d');
							break;
						default:
							break;
					}
					var tryDate = this.parseDates(dateString);
					if (Ext.isDate(tryDate.date)) {
						if (Ext.Date.formatContainsHourInfo(tryDate.format) && Ext.Date.formatContainsDateInfo(tryDate.format)) {
							val = Ext.String.format('Ext.Date.parse("{0}", "{1}")', dateString, tryDate.format);
							target = Ext.String.format('Ext.Date.parse({0}, "c")', target);
						} else if (Ext.Date.formatContainsHourInfo(tryDate.format)) {
							//For time we need to assume we're deal with a time field, so we need to clear the date and use the default date that sencha uses
							//initDate: [2008,0,1],
							val = Ext.String.format('Ext.Date.parse("{0}", "{1}").setFullYear(2008,0,1)', dateString, tryDate.format);
						} else if (Ext.Date.formatContainsDateInfo(tryDate.format)) {
							val = Ext.String.format('Ext.Date.clearTime(Ext.Date.parse("{0}", "{1}"))', dateString, tryDate.format);
						}
						template = Ext.Date.formatContainsHourInfo(tryDate.format) ? '{0} {1} {2}' : (condition == '==' ? '(!Ext.isDate({0}) ? false : Ext.Date.clearTime({0}) >= {2} && Ext.Date.clearTime({0}) <= {2})' : '(!Ext.isDate({0}) ? false : Ext.Date.clearTime({0}) {1} {2})');
					}
				} else if (targetFieldType === "bool" && (val.toLowerCase() === 'true' || val.toLowerCase() === 'false')) {
					template = '{0} {1} {2} ' + (dependency.condition == 'notequals' ? '&&' : '||') + ' {0} {1} \"{2}\"';
					val = val.toLowerCase();
				} else if (targetFieldType === "number" && Ext.isNumeric(val) && (Ext.Array.indexOf(['==', '!=', '<', '<=', '>', '>='], condition) > -1)) {
					val = Number(val);
					template = '{0} {1} {2}';
				}

				if (!template) template = '{0} {1} "{2}"';

				functionString += Ext.String.format(template, target, condition, val);
			}, this);

			switch (dependency.action) {
				case 'isEnabled':
					actionKey = 'IsEnabled$';
					break;
				case 'isVisible':
					actionKey = 'IsVisible$';
					break;
				case 'setBgColor':
					actionKey = 'SetBackgroundColor$';
					break;
				default:
					break;
			}

			if (actionKey) {
				if (actionFunctions[actionKey]) {
					if (actionKey == 'SetBackgroundColor$') {
						var count = 1;
						while (actionFunctions[actionKey]) {
							actionKey = Ext.String.format('SetBackgroundColor{0}$', count);
							count++;
						}
						actionFunctions[actionKey] += ' ( ' + functionString + ' ) ';
						actionOptions[actionKey] = dependency.actionOptions;
					} else {
						if (actionFunctions[actionKey].length > 0) actionFunctions[actionKey] += ' && ';
						actionFunctions[actionKey] += ' ( ' + functionString + ' ) ';
						actionOptions[actionKey] = dependency.actionOptions;
					}
				} else {
					actionFunctions[actionKey] = ' ( ' + functionString + ' ) ';
					actionOptions[actionKey] = dependency.actionOptions;
				}
			}
		}, this);

		for (var key in actionFunctions) {
			if (key.indexOf('SetBackgroundColor') > -1) {
				this.viewmodelSpec[source.name + key] = new Function('return (' + actionFunctions[key] + ') ? "' + actionOptions[key] + '" : "transparent"');
				source.fieldStyle = 'background-color:@{' + source.name + key.substring(0, key.length - 1) + '}';
			} else this.viewmodelSpec[source.name + key] = new Function('return ' + actionFunctions[key]);
		}
	},

	getAllFormControls: function(fieldName) {
		var self = this,
			i = 0,
			config = self.formConfiguration,
			controls = [];
		if (config) {
			if (config.panels[0].tabs.length > 1) { //User checked the "use tabs" checkbox
				var tabItems = [],
					tabs = config.panels[0].tabs;
				for (i = tabs.length - 1; i > 0; i--) {
					controls.concat(tabs[i].columns[0].fields); //concatenate each fields array into a single controls array.
				}
			} else if (config.panels[0].tabs[0].columns.length > 0) { //more common use case - no tabs. //check that there's at least one column before proceeidng
				controls = config.panels[0].tabs[0].columns[0].fields; //set the controls = to these fields, as they are all there is in this form.
				/*Ext.each(controls, function(control){
					//testFormControl
				});*/
			}
		}
		return controls;
	},

	getFormControl: function(fieldName) {
		var matchingControl = null,
			i = 0,
			k = 0,
			controls = this.getAllFormControls(),
			control = null;
		loopOverControls: for (i = controls.length; i--;) {
			control = controls[i];
			if (control.uiType === 'SpacerField') { //ignore space fields
				continue loopOverControls;
			} else if (false /*control.columns*/ ) { //if there are formfields nested in this control (i.e. it's a section) iterate through those and check them.
				/*control.columns.foreach(function(col) {
					col.controls.foreach(function(subControl) {}
				}*/
			} else if (control.name === fieldName) { //otherwise check the current field in the iteration to see if the names match. if so return it.
				matchingControl = control;
				break loopOverControls;
			}
		}
		return matchingControl;
	},

	getControlType: function(control) {
		var type = '';
		if (control) {
			switch (control.uiType) {
				case 'CheckBox':
				case 'MultipleCheckBox':
					type = 'bool';
					break;
				case 'NumberTextField':
				case 'DecimalTextField':
					type = 'number';
					break;
				case 'Date':
				case 'DateTime':
					type = 'date';
					break;
				default:
					type = 'string';
					break;
			}
		}
		return type;
	},

	parseDates: function(dateString) {
		var formats = ['Y-m-d', 'g:i a', 'g:i A', 'G:i', 'Y-m-d g:i a', 'Y-m-d g:i A', 'Y-m-d G:i'],
			returnVal = {
				date: '',
				format: ''
			};

		Ext.each(formats, function(format) {
			var date = Ext.Date.parse(dateString, format);
			if (Ext.isDate(date)) {
				returnVal = {
					date: date,
					format: format
				};
				return false;
			}
		});
		return returnVal;
	}
});

