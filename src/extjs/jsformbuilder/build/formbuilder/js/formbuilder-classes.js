/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/
/**
 * DTO for the Custom Table information
 */
Ext.define('RS.formbuilder.model.CustomTableDTO', {
	extend: 'Ext.data.Model',
	idProperty: 'sys_id',
	fields: ['sys_id', 'uname', 'umodelName', 'udisplayName', 'utype', 'usource', 'udestination']
});

Ext.define('RS.formbuilder.model.WikiName', {
	extend: 'Ext.data.Model',
	idProperty: 'name',
	fields: ['name', 'value'],
	proxy: {
			url: '/resolve/service/form/getwikinames',
			type: 'ajax',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
});
/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.Address', {
	extend: 'Ext.form.FieldContainer',
	alias: 'widget.addressfield',

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	addressSeparator: '|&|',

	initComponent: function() {
		var fieldName = this.name,
			values = this.value ? this.value.split(this.addressSeparator) : [];

		if(!Ext.isArray(this.items)) this.items = [{
			xtype: 'hiddenfield',
			name: this.name
		}];

		this.items = [{
			xtype: 'textfield',
			labelAlign: 'top',
			readOnly: this.readOnly,
			fieldCls: this.fieldCls,
			fieldLabel: RS.formbuilder.locale.Street1,
			value: values.length == 6 ? values[0] : '',
			listeners: {
				change: {
					scope: this,
					fn: this.addressChanged
				}
			}
		}, {
			xtype: 'textfield',
			labelAlign: 'top',
			readOnly: this.readOnly,
			fieldCls: this.fieldCls,
			fieldLabel: RS.formbuilder.locale.Street2,
			value: values.length == 6 ? values[1] : '',
			listeners: {
				change: {
					scope: this,
					fn: this.addressChanged
				}
			}
		}, {
			xtype: 'fieldcontainer',
			hideLabel: true,
			layout: 'hbox',
			items: [{
				xtype: 'textfield',
				labelAlign: 'top',
				readOnly: this.readOnly,
				fieldCls: this.fieldCls,
				flex: 1,
				fieldLabel: RS.formbuilder.locale.City,
				value: values.length == 6 ? values[2] : '',
				listeners: {
					change: {
						scope: this,
						fn: this.addressChanged
					}
				}
			}, {
				xtype: 'textfield',
				labelAlign: 'top',
				readOnly: this.readOnly,
				fieldCls: this.fieldCls,
				margin: '0 0 0 5',
				flex: 1,
				fieldLabel: RS.formbuilder.locale.State,
				value: values.length == 6 ? values[3] : '',
				listeners: {
					change: {
						scope: this,
						fn: this.addressChanged
					}
				}
			}]
		}, {
			xtype: 'fieldcontainer',
			hideLabel: true,
			layout: 'hbox',
			items: [{
				xtype: 'textfield',
				labelAlign: 'top',
				readOnly: this.readOnly,
				fieldCls: this.fieldCls,
				flex: 1,
				fieldLabel: RS.formbuilder.locale.Zip,
				value: values.length == 6 ? values[4] : '',
				listeners: {
					change: {
						scope: this,
						fn: this.addressChanged
					}
				}
			}, {
				xtype: 'combobox',
				displayField: 'name',
				valueField: 'code',
				queryMode: 'local',
				forceSelection: true,
				value: values.length == 6 ? values[5] : '',
				store: {
					data: this.listOfCountries,
					fields: ['name', 'code'],
					proxy: {
						type: 'memory',
						reader: {
							type: 'json'
						}
					}
				},
				labelAlign: 'top',
				readOnly: this.readOnly,
				fieldCls: this.fieldCls,
				margin: '0 0 0 5',
				flex: 1,
				fieldLabel: RS.formbuilder.locale.Country,
				listeners: {
					change: {
						scope: this,
						fn: this.addressChanged
					}
				}
			}]
		}].concat(this.items);
		this.callParent();
	},
	setFieldStyle: function(style){
		var fields = this.query('field');
		Ext.each(fields, function(field){
			if( field.setFieldStyle)field.setFieldStyle(style);
		});
	},
	addressChanged: function() {
		if(!this.rendered) return;
		var address = [],
			fields = this.query('field');
		Ext.each(fields, function(field) {
			if(field.xtype != 'hiddenfield') address.push(field.getValue());
		});
		this.down('hiddenfield').setValue(address.join(this.addressSeparator));
	},
	listOfCountries: [{
		name: 'Afghanistan',
		code: 'AF'
	}, {
		name: 'Åland Islands',
		code: 'AX'
	}, {
		name: 'Albania',
		code: 'AL'
	}, {
		name: 'Algeria',
		code: 'DZ'
	}, {
		name: 'American Samoa',
		code: 'AS'
	}, {
		name: 'AndorrA',
		code: 'AD'
	}, {
		name: 'Angola',
		code: 'AO'
	}, {
		name: 'Anguilla',
		code: 'AI'
	}, {
		name: 'Antarctica',
		code: 'AQ'
	}, {
		name: 'Antigua and Barbuda',
		code: 'AG'
	}, {
		name: 'Argentina',
		code: 'AR'
	}, {
		name: 'Armenia',
		code: 'AM'
	}, {
		name: 'Aruba',
		code: 'AW'
	}, {
		name: 'Australia',
		code: 'AU'
	}, {
		name: 'Austria',
		code: 'AT'
	}, {
		name: 'Azerbaijan',
		code: 'AZ'
	}, {
		name: 'Bahamas',
		code: 'BS'
	}, {
		name: 'Bahrain',
		code: 'BH'
	}, {
		name: 'Bangladesh',
		code: 'BD'
	}, {
		name: 'Barbados',
		code: 'BB'
	}, {
		name: 'Belarus',
		code: 'BY'
	}, {
		name: 'Belgium',
		code: 'BE'
	}, {
		name: 'Belize',
		code: 'BZ'
	}, {
		name: 'Benin',
		code: 'BJ'
	}, {
		name: 'Bermuda',
		code: 'BM'
	}, {
		name: 'Bhutan',
		code: 'BT'
	}, {
		name: 'Bolivia',
		code: 'BO'
	}, {
		name: 'Bosnia and Herzegovina',
		code: 'BA'
	}, {
		name: 'Botswana',
		code: 'BW'
	}, {
		name: 'Bouvet Island',
		code: 'BV'
	}, {
		name: 'Brazil',
		code: 'BR'
	}, {
		name: 'British Indian Ocean Territory',
		code: 'IO'
	}, {
		name: 'Brunei Darussalam',
		code: 'BN'
	}, {
		name: 'Bulgaria',
		code: 'BG'
	}, {
		name: 'Burkina Faso',
		code: 'BF'
	}, {
		name: 'Burundi',
		code: 'BI'
	}, {
		name: 'Cambodia',
		code: 'KH'
	}, {
		name: 'Cameroon',
		code: 'CM'
	}, {
		name: 'Canada',
		code: 'CA'
	}, {
		name: 'Cape Verde',
		code: 'CV'
	}, {
		name: 'Cayman Islands',
		code: 'KY'
	}, {
		name: 'Central African Republic',
		code: 'CF'
	}, {
		name: 'Chad',
		code: 'TD'
	}, {
		name: 'Chile',
		code: 'CL'
	}, {
		name: 'China',
		code: 'CN'
	}, {
		name: 'Christmas Island',
		code: 'CX'
	}, {
		name: 'Cocos (Keeling) Islands',
		code: 'CC'
	}, {
		name: 'Colombia',
		code: 'CO'
	}, {
		name: 'Comoros',
		code: 'KM'
	}, {
		name: 'Congo',
		code: 'CG'
	}, {
		name: 'Congo, The Democratic Republic of the',
		code: 'CD'
	}, {
		name: 'Cook Islands',
		code: 'CK'
	}, {
		name: 'Costa Rica',
		code: 'CR'
	}, {
		name: 'Cote D\'Ivoire',
		code: 'CI'
	}, {
		name: 'Croatia',
		code: 'HR'
	}, {
		name: 'Cuba',
		code: 'CU'
	}, {
		name: 'Cyprus',
		code: 'CY'
	}, {
		name: 'Czech Republic',
		code: 'CZ'
	}, {
		name: 'Denmark',
		code: 'DK'
	}, {
		name: 'Djibouti',
		code: 'DJ'
	}, {
		name: 'Dominica',
		code: 'DM'
	}, {
		name: 'Dominican Republic',
		code: 'DO'
	}, {
		name: 'Ecuador',
		code: 'EC'
	}, {
		name: 'Egypt',
		code: 'EG'
	}, {
		name: 'El Salvador',
		code: 'SV'
	}, {
		name: 'Equatorial Guinea',
		code: 'GQ'
	}, {
		name: 'Eritrea',
		code: 'ER'
	}, {
		name: 'Estonia',
		code: 'EE'
	}, {
		name: 'Ethiopia',
		code: 'ET'
	}, {
		name: 'Falkland Islands (Malvinas)',
		code: 'FK'
	}, {
		name: 'Faroe Islands',
		code: 'FO'
	}, {
		name: 'Fiji',
		code: 'FJ'
	}, {
		name: 'Finland',
		code: 'FI'
	}, {
		name: 'France',
		code: 'FR'
	}, {
		name: 'French Guiana',
		code: 'GF'
	}, {
		name: 'French Polynesia',
		code: 'PF'
	}, {
		name: 'French Southern Territories',
		code: 'TF'
	}, {
		name: 'Gabon',
		code: 'GA'
	}, {
		name: 'Gambia',
		code: 'GM'
	}, {
		name: 'Georgia',
		code: 'GE'
	}, {
		name: 'Germany',
		code: 'DE'
	}, {
		name: 'Ghana',
		code: 'GH'
	}, {
		name: 'Gibraltar',
		code: 'GI'
	}, {
		name: 'Greece',
		code: 'GR'
	}, {
		name: 'Greenland',
		code: 'GL'
	}, {
		name: 'Grenada',
		code: 'GD'
	}, {
		name: 'Guadeloupe',
		code: 'GP'
	}, {
		name: 'Guam',
		code: 'GU'
	}, {
		name: 'Guatemala',
		code: 'GT'
	}, {
		name: 'Guernsey',
		code: 'GG'
	}, {
		name: 'Guinea',
		code: 'GN'
	}, {
		name: 'Guinea-Bissau',
		code: 'GW'
	}, {
		name: 'Guyana',
		code: 'GY'
	}, {
		name: 'Haiti',
		code: 'HT'
	}, {
		name: 'Heard Island and Mcdonald Islands',
		code: 'HM'
	}, {
		name: 'Holy See (Vatican City State)',
		code: 'VA'
	}, {
		name: 'Honduras',
		code: 'HN'
	}, {
		name: 'Hong Kong',
		code: 'HK'
	}, {
		name: 'Hungary',
		code: 'HU'
	}, {
		name: 'Iceland',
		code: 'IS'
	}, {
		name: 'India',
		code: 'IN'
	}, {
		name: 'Indonesia',
		code: 'ID'
	}, {
		name: 'Iran, Islamic Republic Of',
		code: 'IR'
	}, {
		name: 'Iraq',
		code: 'IQ'
	}, {
		name: 'Ireland',
		code: 'IE'
	}, {
		name: 'Isle of Man',
		code: 'IM'
	}, {
		name: 'Israel',
		code: 'IL'
	}, {
		name: 'Italy',
		code: 'IT'
	}, {
		name: 'Jamaica',
		code: 'JM'
	}, {
		name: 'Japan',
		code: 'JP'
	}, {
		name: 'Jersey',
		code: 'JE'
	}, {
		name: 'Jordan',
		code: 'JO'
	}, {
		name: 'Kazakhstan',
		code: 'KZ'
	}, {
		name: 'Kenya',
		code: 'KE'
	}, {
		name: 'Kiribati',
		code: 'KI'
	}, {
		name: 'Korea, Democratic People\'S Republic of',
		code: 'KP'
	}, {
		name: 'Korea, Republic of',
		code: 'KR'
	}, {
		name: 'Kuwait',
		code: 'KW'
	}, {
		name: 'Kyrgyzstan',
		code: 'KG'
	}, {
		name: 'Lao People\'S Democratic Republic',
		code: 'LA'
	}, {
		name: 'Latvia',
		code: 'LV'
	}, {
		name: 'Lebanon',
		code: 'LB'
	}, {
		name: 'Lesotho',
		code: 'LS'
	}, {
		name: 'Liberia',
		code: 'LR'
	}, {
		name: 'Libyan Arab Jamahiriya',
		code: 'LY'
	}, {
		name: 'Liechtenstein',
		code: 'LI'
	}, {
		name: 'Lithuania',
		code: 'LT'
	}, {
		name: 'Luxembourg',
		code: 'LU'
	}, {
		name: 'Macao',
		code: 'MO'
	}, {
		name: 'Macedonia, The Former Yugoslav Republic of',
		code: 'MK'
	}, {
		name: 'Madagascar',
		code: 'MG'
	}, {
		name: 'Malawi',
		code: 'MW'
	}, {
		name: 'Malaysia',
		code: 'MY'
	}, {
		name: 'Maldives',
		code: 'MV'
	}, {
		name: 'Mali',
		code: 'ML'
	}, {
		name: 'Malta',
		code: 'MT'
	}, {
		name: 'Marshall Islands',
		code: 'MH'
	}, {
		name: 'Martinique',
		code: 'MQ'
	}, {
		name: 'Mauritania',
		code: 'MR'
	}, {
		name: 'Mauritius',
		code: 'MU'
	}, {
		name: 'Mayotte',
		code: 'YT'
	}, {
		name: 'Mexico',
		code: 'MX'
	}, {
		name: 'Micronesia, Federated States of',
		code: 'FM'
	}, {
		name: 'Moldova, Republic of',
		code: 'MD'
	}, {
		name: 'Monaco',
		code: 'MC'
	}, {
		name: 'Mongolia',
		code: 'MN'
	}, {
		name: 'Montserrat',
		code: 'MS'
	}, {
		name: 'Morocco',
		code: 'MA'
	}, {
		name: 'Mozambique',
		code: 'MZ'
	}, {
		name: 'Myanmar',
		code: 'MM'
	}, {
		name: 'Namibia',
		code: 'NA'
	}, {
		name: 'Nauru',
		code: 'NR'
	}, {
		name: 'Nepal',
		code: 'NP'
	}, {
		name: 'Netherlands',
		code: 'NL'
	}, {
		name: 'Netherlands Antilles',
		code: 'AN'
	}, {
		name: 'New Caledonia',
		code: 'NC'
	}, {
		name: 'New Zealand',
		code: 'NZ'
	}, {
		name: 'Nicaragua',
		code: 'NI'
	}, {
		name: 'Niger',
		code: 'NE'
	}, {
		name: 'Nigeria',
		code: 'NG'
	}, {
		name: 'Niue',
		code: 'NU'
	}, {
		name: 'Norfolk Island',
		code: 'NF'
	}, {
		name: 'Northern Mariana Islands',
		code: 'MP'
	}, {
		name: 'Norway',
		code: 'NO'
	}, {
		name: 'Oman',
		code: 'OM'
	}, {
		name: 'Pakistan',
		code: 'PK'
	}, {
		name: 'Palau',
		code: 'PW'
	}, {
		name: 'Palestinian Territory, Occupied',
		code: 'PS'
	}, {
		name: 'Panama',
		code: 'PA'
	}, {
		name: 'Papua New Guinea',
		code: 'PG'
	}, {
		name: 'Paraguay',
		code: 'PY'
	}, {
		name: 'Peru',
		code: 'PE'
	}, {
		name: 'Philippines',
		code: 'PH'
	}, {
		name: 'Pitcairn',
		code: 'PN'
	}, {
		name: 'Poland',
		code: 'PL'
	}, {
		name: 'Portugal',
		code: 'PT'
	}, {
		name: 'Puerto Rico',
		code: 'PR'
	}, {
		name: 'Qatar',
		code: 'QA'
	}, {
		name: 'Reunion',
		code: 'RE'
	}, {
		name: 'Romania',
		code: 'RO'
	}, {
		name: 'Russian Federation',
		code: 'RU'
	}, {
		name: 'RWANDA',
		code: 'RW'
	}, {
		name: 'Saint Helena',
		code: 'SH'
	}, {
		name: 'Saint Kitts and Nevis',
		code: 'KN'
	}, {
		name: 'Saint Lucia',
		code: 'LC'
	}, {
		name: 'Saint Pierre and Miquelon',
		code: 'PM'
	}, {
		name: 'Saint Vincent and the Grenadines',
		code: 'VC'
	}, {
		name: 'Samoa',
		code: 'WS'
	}, {
		name: 'San Marino',
		code: 'SM'
	}, {
		name: 'Sao Tome and Principe',
		code: 'ST'
	}, {
		name: 'Saudi Arabia',
		code: 'SA'
	}, {
		name: 'Senegal',
		code: 'SN'
	}, {
		name: 'Serbia and Montenegro',
		code: 'CS'
	}, {
		name: 'Seychelles',
		code: 'SC'
	}, {
		name: 'Sierra Leone',
		code: 'SL'
	}, {
		name: 'Singapore',
		code: 'SG'
	}, {
		name: 'Slovakia',
		code: 'SK'
	}, {
		name: 'Slovenia',
		code: 'SI'
	}, {
		name: 'Solomon Islands',
		code: 'SB'
	}, {
		name: 'Somalia',
		code: 'SO'
	}, {
		name: 'South Africa',
		code: 'ZA'
	}, {
		name: 'South Georgia and the South Sandwich Islands',
		code: 'GS'
	}, {
		name: 'Spain',
		code: 'ES'
	}, {
		name: 'Sri Lanka',
		code: 'LK'
	}, {
		name: 'Sudan',
		code: 'SD'
	}, {
		name: 'Suriname',
		code: 'SR'
	}, {
		name: 'Svalbard and Jan Mayen',
		code: 'SJ'
	}, {
		name: 'Swaziland',
		code: 'SZ'
	}, {
		name: 'Sweden',
		code: 'SE'
	}, {
		name: 'Switzerland',
		code: 'CH'
	}, {
		name: 'Syrian Arab Republic',
		code: 'SY'
	}, {
		name: 'Taiwan, Province of China',
		code: 'TW'
	}, {
		name: 'Tajikistan',
		code: 'TJ'
	}, {
		name: 'Tanzania, United Republic of',
		code: 'TZ'
	}, {
		name: 'Thailand',
		code: 'TH'
	}, {
		name: 'Timor-Leste',
		code: 'TL'
	}, {
		name: 'Togo',
		code: 'TG'
	}, {
		name: 'Tokelau',
		code: 'TK'
	}, {
		name: 'Tonga',
		code: 'TO'
	}, {
		name: 'Trinidad and Tobago',
		code: 'TT'
	}, {
		name: 'Tunisia',
		code: 'TN'
	}, {
		name: 'Turkey',
		code: 'TR'
	}, {
		name: 'Turkmenistan',
		code: 'TM'
	}, {
		name: 'Turks and Caicos Islands',
		code: 'TC'
	}, {
		name: 'Tuvalu',
		code: 'TV'
	}, {
		name: 'Uganda',
		code: 'UG'
	}, {
		name: 'Ukraine',
		code: 'UA'
	}, {
		name: 'United Arab Emirates',
		code: 'AE'
	}, {
		name: 'United Kingdom',
		code: 'GB'
	}, {
		name: 'United States',
		code: 'US'
	}, {
		name: 'United States Minor Outlying Islands',
		code: 'UM'
	}, {
		name: 'Uruguay',
		code: 'UY'
	}, {
		name: 'Uzbekistan',
		code: 'UZ'
	}, {
		name: 'Vanuatu',
		code: 'VU'
	}, {
		name: 'Venezuela',
		code: 'VE'
	}, {
		name: 'Viet Nam',
		code: 'VN'
	}, {
		name: 'Virgin Islands, British',
		code: 'VG'
	}, {
		name: 'Virgin Islands, U.S.',
		code: 'VI'
	}, {
		name: 'Wallis and Futuna',
		code: 'WF'
	}, {
		name: 'Western Sahara',
		code: 'EH'
	}, {
		name: 'Yemen',
		code: 'YE'
	}, {
		name: 'Zambia',
		code: 'ZM'
	}, {
		name: 'Zimbabwe',
		code: 'ZW'
	}]
});
Ext.define('RS.formbuilder.viewer.desktop.fields.ClearableCombobox', {
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.clearablecombobox',

	//Clear Trigger
	clearCls: Ext.baseCSSPrefix + 'form-clear-trigger',
	clearClick: function() {
		//clear the current value and hide the clear button
		this.setValue('');
		this.triggerEl.item(0).parent().setDisplayed('none');
	},

	initComponent: function() {
		this.trigger2Cls = this.triggerCls;
		this.onTrigger2Click = this.onTriggerClick;
		this.trigger1Cls = this.clearCls;
		this.onTrigger1Click = this.clearClick;

		this.callParent();
	},

	onRender: function() {
		this.callParent(arguments);
		if (this.value && !this.readOnly) this.triggerEl.item(0).parent().setDisplayed('table-cell')
		else this.triggerEl.item(0).parent().setDisplayed('none');
	},

	setValue: function(value) {
		this.callParent(arguments);
		if (this.rendered && this.value && !this.mandatory && !this.readOnly) {
			this.triggerEl.item(0).parent().setDisplayed('table-cell');
			this.doComponentLayout();
		}
	}
});
Ext.define('RS.formbuilder.viewer.desktop.fields.ColorPickerCombo', {
	extend: 'Ext.form.field.Trigger',
	alias: 'widget.colorpickercombo',
	onTriggerClick: function() {
		var me = this;
		if (!me.picker)
			me.picker = Ext.create('Ext.picker.Color', {
				pickerField: this,
				ownerCt: this,
				renderTo: document.body,
				floating: true,
				// hidden: true,
				focusOnShow: true,
				style: {
					backgroundColor: "#fff"
				},
				listeners: {
					scope: this,
					select: function(field, value, opts) {
						me.setValue('#' + value);
						me.inputEl.setStyle({
							backgroundColor: value
						});
						me.picker.hide();
						me.picker.destroy();
						me.picker = null;
					},
					show: function(field, opts) {
						field.getEl().monitorMouseLeave(500, field.hide, field);
					}
				}
			})
		me.picker.alignTo(me.inputEl, 'tl-bl?');
		me.picker.show(me.inputEl);
	}
});

glu.regAdapter('colorpickercombo', {
	extend: 'combo'
});
Ext.define('RS.formbuilder.viewer.desktop.fields.FormPickerField', {
	extend: 'Ext.form.field.Trigger',
	alias: 'widget.formpickerfield',

	trigger1Cls: 'x-form-search-trigger',
	trigger2Cls: 'rs-btn-edit',
	trigger3Cls: 'icon-plus',

	allowCreate: false,
	editable: false,

	initComponent: function() {
		var me = this;
		me.callParent()
		me.on('render', function() {
			if (!me.allowCreate)
				me.triggerEl.item(2).parent().setDisplayed('none')
			me.triggerEl.item(2).addCls('x-icon-over icon-large rs-icon')
			me.triggerEl.item(2).parent().setStyle({
				'padding-top': '3px'
			})
			if (me.getValue()) me.triggerEl.item(1).parent().setDisplayed('table-cell')
			else me.triggerEl.item(1).parent().setDisplayed('none')
			me.triggerEl.item(1).addCls('x-icon-over')
			me.inputEl.on('click', function() {
				me.fireEvent('choose')
			})
		})
	},

	setValue: function(value) {
		this.callParent(arguments)
		if (this.rendered) {
			if (value) this.triggerEl.item(1).parent().setDisplayed('table-cell')
			else this.triggerEl.item(1).parent().setDisplayed('none')
		}
	},

	onTrigger1Click: function() {
		this.fireEvent('choose', this)
	},

	onTrigger2Click: function() {
		this.fireEvent('edit', this)
	},

	onTrigger3Click: function() {
		this.fireEvent('create', this)
	}
})
/**
 * InputTextMask script used for mask/regexp operations.
 * Mask Individual Character Usage:
 * 9 - designates only numeric values
 * L - designates only uppercase letter values
 * l - designates only lowercase letter values
 * A - designates only alphanumeric values
 * X - denotes that a custom client script regular expression is specified</li>
 * All other characters are assumed to be "special" characters used to mask the input component.
 * Example 1:
 * (999)999-9999 only numeric values can be entered where the the character
 * position value is 9. Parenthesis and dash are non-editable/mask characters.
 * Example 2:
 * 99L-ll-X[^A-C]X only numeric values for the first two characters,
 * uppercase values for the third character, lowercase letters for the
 * fifth/sixth characters, and the last character X[^A-C]X together counts
 * as the eighth character regular expression that would allow all characters
 * but "A", "B", and "C". Dashes outside the regular expression are non-editable/mask characters.
 * @constructor
 * @param (String) mask The InputTextMask
 * @param (boolean) clearWhenInvalid True to clear the mask when the field blurs and the text is invalid. Optional, default is true.
 */

Ext.define('RS.formbuilder.viewer.desktop.InputTextMask', {
	extend: 'Ext.AbstractPlugin',
	alias: 'ptype.inputtextmask',

	constructor: function(mask, clearWhenInvalid, offset) {
		this.maskOffset = offset || 0;
		if (clearWhenInvalid === undefined) this.clearWhenInvalid = true;
		else this.clearWhenInvalid = clearWhenInvalid;
		this.rawMask = mask;
		this.viewMask = '';
		this.maskArray = new Array();
		var mai = 0;
		var regexp = '';
		for (var i = 0; i < mask.length; i++) {
			if (regexp) {
				if (regexp == 'X') {
					regexp = '';
				}
				if (mask.charAt(i) == 'X') {
					this.maskArray[mai] = regexp;
					mai++;
					regexp = '';
				} else {
					regexp += mask.charAt(i);
				}
			} else if (mask.charAt(i) == 'X') {
				regexp += 'X';
				this.viewMask += '_';
			} else if (mask.charAt(i) == '9' || mask.charAt(i) == 'L' || mask.charAt(i) == 'l' || mask.charAt(i) == 'A') {
				this.viewMask += '_';
				this.maskArray[mai] = mask.charAt(i);
				mai++;
			} else {
				this.viewMask += mask.charAt(i);
				this.maskArray[mai] = RegExp.escape(mask.charAt(i));
				mai++;
			}
		}

		this.specialChars = this.viewMask.replace(/(L|l|9|A|_|X)/g, '');
		return this;
	},

	init: function(field) {
		this.field = field;

		if (field.rendered) {
			this.assignEl();
		} else {
			field.on('render', this.assignEl, this);
		}

		field.on('blur', this.removeValueWhenInvalid, this);
		field.on('focus', this.processMaskFocus, this);
	},

	assignEl: function() {
		this.inputTextElement = this.field.inputEl.dom;
		this.field.inputEl.on('keypress', this.processKeyPress, this);
		this.field.inputEl.on('keydown', this.processKeyDown, this);
		if (Ext.isSafari || Ext.isIE) {
			this.field.inputEl.on('paste', this.startTask, this);
			this.field.inputEl.on('cut', this.startTask, this);
		}
		if (Ext.isGecko || Ext.isOpera) {
			this.field.inputEl.on('mousedown', this.setPreviousValue, this);
		}
		if (Ext.isGecko) {
			this.field.inputEl.on('input', this.onInput, this);
		}
		if (Ext.isOpera) {
			this.field.inputEl.on('input', this.onInputOpera, this);
		}
	},
	onInput: function() {
		this.startTask(false);
	},
	onInputOpera: function() {
		if (!this.prevValueOpera) {
			this.startTask(false);
		} else {
			this.manageBackspaceAndDeleteOpera();
		}
	},

	manageBackspaceAndDeleteOpera: function() {
		this.inputTextElement.value = this.prevValueOpera.cursorPos.previousValue;
		this.manageTheText(this.prevValueOpera.keycode, this.prevValueOpera.cursorPos);
		this.prevValueOpera = null;
	},

	setPreviousValue: function(event) {
		this.oldCursorPos = this.getCursorPosition();
	},

	getValidatedKey: function(keycode, cursorPosition) {
		var maskKey = this.maskArray[cursorPosition.start];
		if (maskKey == '9') {
			return keycode.pressedKey.match(/[0-9]/);
		} else if (maskKey == 'L') {
			return (keycode.pressedKey.match(/[A-Za-z]/)) ? keycode.pressedKey.toUpperCase() : null;
		} else if (maskKey == 'l') {
			return (keycode.pressedKey.match(/[A-Za-z]/)) ? keycode.pressedKey.toLowerCase() : null;
		} else if (maskKey == 'A') {
			return keycode.pressedKey.match(/[A-Za-z0-9]/);
		} else if (maskKey) {
			return (keycode.pressedKey.match(new RegExp(maskKey)));
		}
		return (null);
	},

	removeValueWhenInvalid: function() {
		if (this.clearWhenInvalid && this.inputTextElement.value.indexOf('_') > -1) {
			this.inputTextElement.value = '';
		}
	},

	managePaste: function() {
		if (this.oldCursorPos == null) {
			return;
		}
		var valuePasted = this.inputTextElement.value.substring(this.oldCursorPos.start, this.inputTextElement.value.length - (this.oldCursorPos.previousValue.length - this.oldCursorPos.end));
		if (this.oldCursorPos.start < this.oldCursorPos.end) {
			this.oldCursorPos.previousValue = this.oldCursorPos.previousValue.substring(0, this.oldCursorPos.start) + this.viewMask.substring(this.oldCursorPos.start, this.oldCursorPos.end) + this.oldCursorPos.previousValue.substring(this.oldCursorPos.end, this.oldCursorPos.previousValue.length);
			valuePasted = valuePasted.substr(0, this.oldCursorPos.end - this.oldCursorPos.start);
		}
		this.inputTextElement.value = this.oldCursorPos.previousValue;
		keycode = {
			unicode: '',
			isShiftPressed: false,
			isTab: false,
			isBackspace: false,
			isLeftOrRightArrow: false,
			isDelete: false,
			pressedKey: ''
		}
		var charOk = false;
		for (var i = 0; i < valuePasted.length; i++) {
			keycode.pressedKey = valuePasted.substr(i, 1);
			keycode.unicode = valuePasted.charCodeAt(i);
			this.oldCursorPos = this.skipMaskCharacters(keycode, this.oldCursorPos);
			if (this.oldCursorPos === false) {
				break;
			}
			if (this.injectValue(keycode, this.oldCursorPos)) {
				charOk = true;
				this.moveCursorToPosition(keycode, this.oldCursorPos);
				this.oldCursorPos.previousValue = this.inputTextElement.value;
				this.oldCursorPos.start = this.oldCursorPos.start + 1;
			}
		}
		if (!charOk && this.oldCursorPos !== false) {
			this.moveCursorToPosition(null, this.oldCursorPos);
		}
		this.oldCursorPos = null;
	},

	processKeyDown: function(e) {
		this.processMaskFormatting(e, 'keydown');
	},

	processKeyPress: function(e) {
		this.processMaskFormatting(e, 'keypress');
	},

	startTask: function(setOldCursor) {
		if (this.task == undefined) {
			this.task = new Ext.util.DelayedTask(this.managePaste, this);
		}
		if (setOldCursor !== false) {
			this.oldCursorPos = this.getCursorPosition();
		}
		this.task.delay(0);
	},

	skipMaskCharacters: function(keycode, cursorPos) {
		if (cursorPos.start != cursorPos.end && (keycode.isDelete || keycode.isBackspace)) return (cursorPos);
		while (this.specialChars.match(RegExp.escape(this.viewMask.charAt(((keycode.isBackspace) ? cursorPos.start - 1 : cursorPos.start))))) {
			if (keycode.isBackspace) {
				cursorPos.dec();
			} else {
				cursorPos.inc();
			}
			if (cursorPos.start >= cursorPos.previousValue.length || cursorPos.start < 0) {
				return false;
			}
		}
		return (cursorPos);
	},

	isManagedByKeyDown: function(keycode) {
		if (keycode.isDelete || keycode.isBackspace) {
			return (true);
		}
		return (false);
	},

	processMaskFormatting: function(e, type) {
		this.oldCursorPos = null;
		var cursorPos = this.getCursorPosition();
		var keycode = this.getKeyCode(e, type);
		if (keycode.unicode == 0) { //?? sometimes on Safari
			return;
		}
		if ((keycode.unicode == 67 || keycode.unicode == 99) && e.ctrlKey) { //Ctrl+c, let's the browser manage it!
			return;
		}
		if ((keycode.unicode == 88 || keycode.unicode == 120) && e.ctrlKey) { //Ctrl+x, manage paste
			this.startTask();
			return;
		}
		if ((keycode.unicode == 86 || keycode.unicode == 118) && e.ctrlKey) { //Ctrl+v, manage paste....
			this.startTask();
			return;
		}
		if ((keycode.isBackspace || keycode.isDelete) && Ext.isOpera) {
			this.prevValueOpera = {
				cursorPos: cursorPos,
				keycode: keycode
			};
			return;
		}
		if (type == 'keydown' && !this.isManagedByKeyDown(keycode)) {
			return true;
		}
		if (type == 'keypress' && this.isManagedByKeyDown(keycode)) {
			return true;
		}
		if (this.handleEventBubble(e, keycode, type)) {
			return true;
		}
		return (this.manageTheText(keycode, cursorPos));
	},

	manageTheText: function(keycode, cursorPos) {
		if (this.inputTextElement.value.length === 0) {
			this.inputTextElement.value = this.viewMask;
		}
		cursorPos = this.skipMaskCharacters(keycode, cursorPos);
		if (cursorPos === false) {
			return false;
		}
		if (this.injectValue(keycode, cursorPos)) {
			this.moveCursorToPosition(keycode, cursorPos);
		}
		return (false);
	},

	processMaskFocus: function() {
		if (this.inputTextElement.value.length == 0) {
			var self = this
			var cursorPos = this.getCursorPosition();
			this.inputTextElement.value = this.viewMask;
			setTimeout(function() {
				self.moveCursorToPosition(null, cursorPos);
			}, 1)
		}
	},

	isManagedByBrowser: function(keyEvent, keycode, type) {
		if (((type == 'keypress' && keyEvent.charCode === 0) || type == 'keydown') && (keycode.unicode == Ext.EventObject.TAB || keycode.unicode == Ext.EventObject.RETURN || keycode.unicode == Ext.EventObject.ENTER || keycode.unicode == Ext.EventObject.SHIFT || keycode.unicode == Ext.EventObject.CONTROL || keycode.unicode == Ext.EventObject.ESC || keycode.unicode == Ext.EventObject.PAGEUP || keycode.unicode == Ext.EventObject.PAGEDOWN || keycode.unicode == Ext.EventObject.END || keycode.unicode == Ext.EventObject.HOME || keycode.unicode == Ext.EventObject.LEFT || keycode.unicode == Ext.EventObject.UP || keycode.unicode == Ext.EventObject.RIGHT || keycode.unicode == Ext.EventObject.DOWN)) {
			return (true);
		}
		return (false);
	},

	handleEventBubble: function(keyEvent, keycode, type) {
		try {
			if (keycode && this.isManagedByBrowser(keyEvent, keycode, type)) {
				return true;
			}
			keyEvent.stopEvent();
			return false;
		} catch (e) {
			alert(e.message);
		}
	},

	getCursorPosition: function() {
		var s, e, r;
		if (this.inputTextElement.createTextRange) {
			r = document.selection.createRange().duplicate();
			r.moveEnd('character', this.inputTextElement.value.length);
			if (r.text === '') {
				s = this.inputTextElement.value.length;
			} else {
				s = this.inputTextElement.value.lastIndexOf(r.text);
			}
			r = document.selection.createRange().duplicate();
			r.moveStart('character', -this.inputTextElement.value.length);
			e = r.text.length;
		} else {
			s = this.inputTextElement.selectionStart;
			e = this.inputTextElement.selectionEnd;
		}

		if (s === 0 && e === 0) {
			s = s + this.maskOffset
			e = e + +this.maskOffset
		}
		return this.CursorPosition(s, e, r, this.inputTextElement.value);
	},

	moveCursorToPosition: function(keycode, cursorPosition) {
		var p = (!keycode || (keycode && keycode.isBackspace)) ? cursorPosition.start : cursorPosition.start + 1;
		if (this.inputTextElement.createTextRange) {
			cursorPosition.range.move('character', p);
			cursorPosition.range.select();
		} else {
			this.inputTextElement.selectionStart = p;
			this.inputTextElement.selectionEnd = p;
		}
	},

	injectValue: function(keycode, cursorPosition) {
		if (!keycode.isDelete && keycode.unicode == cursorPosition.previousValue.charCodeAt(cursorPosition.start)) return true;
		var key;
		if (!keycode.isDelete && !keycode.isBackspace) {
			key = this.getValidatedKey(keycode, cursorPosition);
		} else {
			if (cursorPosition.start == cursorPosition.end) {
				key = '_';
				if (keycode.isBackspace) {
					cursorPosition.dec();
				}
			} else {
				key = this.viewMask.substring(cursorPosition.start, cursorPosition.end);
			}
		}
		if (key) {
			this.inputTextElement.value = cursorPosition.previousValue.substring(0, cursorPosition.start) + key + cursorPosition.previousValue.substring(cursorPosition.start + key.length, cursorPosition.previousValue.length);
			return true;
		}
		return false;
	},

	getKeyCode: function(onKeyDownEvent, type) {
		var keycode = {};
		keycode.unicode = onKeyDownEvent.getKey();
		keycode.isShiftPressed = onKeyDownEvent.shiftKey;

		keycode.isDelete = ((onKeyDownEvent.getKey() == Ext.EventObject.DELETE && type == 'keydown') || (type == 'keypress' && onKeyDownEvent.charCode === 0 && onKeyDownEvent.keyCode == Ext.EventObject.DELETE)) ? true : false;
		keycode.isTab = (onKeyDownEvent.getKey() == Ext.EventObject.TAB) ? true : false;
		keycode.isBackspace = (onKeyDownEvent.getKey() == Ext.EventObject.BACKSPACE) ? true : false;
		keycode.isLeftOrRightArrow = (onKeyDownEvent.getKey() == Ext.EventObject.LEFT || onKeyDownEvent.getKey() == Ext.EventObject.RIGHT) ? true : false;
		keycode.pressedKey = String.fromCharCode(keycode.unicode);
		return (keycode);
	},

	CursorPosition: function(start, end, range, previousValue) {
		var cursorPosition = {};
		cursorPosition.start = isNaN(start) ? 0 : start;
		cursorPosition.end = isNaN(end) ? 0 : end;
		cursorPosition.range = range;
		cursorPosition.previousValue = previousValue;
		cursorPosition.inc = function() {
			cursorPosition.start++;
			cursorPosition.end++;
		};
		cursorPosition.dec = function() {
			cursorPosition.start--;
			cursorPosition.end--;
		};
		return (cursorPosition);
	}
});

Ext.applyIf(RegExp, {
	escape: function(str) {
		return new String(str).replace(/([.*+?^=!:${}()|[\]\/\\])/g, '\\$1');
	}
});
/**
 *
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.JournalField', {
	extend: 'Ext.form.FieldContainer',
	alias: 'widget.journalfield',

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	journalSeparator: '|&|',

	userId: '',
	journalEntries: [],

	allowJournalEdit: false,

	disabled: false,

	resizable: true,
	resizeHandles: 's',

	initComponent: function() {
		var me = this;
		me.value = me.value || [];
		if (Ext.isString(me.value)) me.value = Ext.decode(me.value);

		me.height = me.height || 300; //to ensure the grid is displayed
		me.journalEntries = me.value;

		me.journalStore = Ext.create('Ext.data.Store', {
			fields: ['userId', {
				name: 'createDate',
				type: 'date',
				format: 'c',
				dateFormat: 'c'
			}, 'note'],
			data: me.value,
			proxy: {
				type: 'memory',
				reader: {
					type: 'json'
				}
			}
		});

		var editor = Ext.create('Ext.grid.plugin.CellEditing', {});

		if (!Ext.isArray(me.items)) me.items = [{
			xtype: 'hiddenfield',
			name: me.name,
			getValue: function() {
				var temp = me.ownerCt.journalEntries.slice(0),  // me.ownerCt.journalEntries is immutable
					newVal = me.ownerCt.down('textarea').getValue();
				if (newVal) {
					temp.splice(0, 0, {
						userId: me.ownerCt.userId,
						createDate: Ext.Date.format(new Date(), 'c'),
						note: Ext.htmlEncode(newVal).replace(/\n/g, '<br/>')
					});
				}
				return Ext.encode(temp);
			}
		}];

		var journalPlugins = [];
		if (this.allowJournalEdit) journalPlugins.push(editor)

		me.items = me.items.concat([{
			xtype: 'textarea',
			readOnly: me.readOnly,
			hidden: me.readOnly
		}, {
			xtype: 'grid',
			flex: 1,
			plugins: journalPlugins,
			columns: [{
				header: 'Note',
				dataIndex: 'note',
				flex: 1,
				editor: {}
			}, {
				header: 'User',
				width: 100,
				dataIndex: 'userId'
			}, {
				header: 'Date',
				dataIndex: 'createDate',
				width: 200,
				renderer: Ext.util.Format.dateRenderer('D, M d, Y g:i:s A')
			}, {
				xtype: 'actioncolumn',
				hidden: !this.allowJournalEdit,
				hideable: false,
				width: 40,
				items: [{
					icon: '/resolve/images/edittsk_tsk.gif',
					// Use a URL in the icon config
					tooltip: 'Edit',
					handler: function(grid, rowIndex, colIndex) {
						if (journalPlugins.length > 0)
							editor.startEditByPosition({
								row: rowIndex,
								column: 0
							});
					}
				}, {
					icon: '/resolve/images/closex.gif',
					tooltip: 'Delete',
					scope: me,
					handler: function(grid, rowIndex, colIndex) {
						grid.lastRowIndex = rowIndex;
						Ext.MessageBox.show({
							title: 'Confirm Delete',
							msg: 'Are you sure you want to delete the selected journal entry?',
							buttons: Ext.MessageBox.YESNO,
							buttonText: {
								yes: 'Delete',
								no: 'Cancel'
							},
							scope: grid,
							fn: me.removeJournalEntry
						});
					}
				}]
			}],
			listeners: {
				edit: function() {
					me.storeJournalEntries(this);
				}
			},
			store: me.journalStore
		}]);

		me.callParent();
	},
	journalEntryChanged: function(field, newValue, oldValue, eOpts) {
		//serialize the current value for the field into the hidden field
		var temp = [{
			userId: this.userId,
			createDate: Ext.Date.format(new Date(), 'c'),
			note: Ext.htmlEncode(newValue).replace(/\n/g, '<br/>')
		}];
		temp = temp.concat(this.journalEntries);
		this.down('hiddenfield').setValue(Ext.encode(temp));
	},
	removeJournalEntry: function(btn) {
		if (btn === 'yes') {
			this.getStore().removeAt(this.lastRowIndex);
			this.up('fieldcontainer').storeJournalEntries(this);
		}
	},
	storeJournalEntries: function(grid) {
		var records = [];
		grid.getStore().each(function(record) {
			records.push({
				userId: record.get('userId'),
				createDate: record.get('createDate'),
				note: record.get('note')
			});
		});
		this.journalEntries = records;
		var textarea = this.down('textarea'),
			val = textarea.getValue();
		if (val) {
			this.journalEntryChanged(textarea, val);
		}
	},
	setValue: function(value) {
		if (value && Ext.isString(value)) {
			var journals = Ext.decode(value);
			if (Ext.isArray(journals)) {
				this.down('textareafield').setValue('');
				this.journalEntries = journals;
				this.journalStore.removeAll();
				this.journalStore.add(journals);
			}
		}
	},
	setFieldStyle: function(style) {
		var fields = this.query('field');
		Ext.each(fields, function(field) {
			if (field.setFieldStyle) field.setFieldStyle(style);
		});
	}
});
/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.Link', {
	extend: 'Ext.form.Label',
	alias: 'widget.linkfield',

	padding: '10px 0px 10px 0px',
	style: {
		display: 'block'
	},

	initComponent: function() {
		this.callParent();
		this.setValue(this.value || this.defaultValue);

		var me = this;
		this.on('afterrender', function() {
			me.setDisabled(me.disabled)
		})
	},

	setValue: function(value) {
		var link = this.generateLink(value);
		this.setText(link, false)
	},

	generateLink: function(value) {
		//parse the link and return it
		if (!value) return '';

		//We have a document link and we need to look that information up
		Ext.Ajax.request({
			url: '/resolve/service/form/getLink',
			params: {
				wikiLink: value,
				target: this.linkTarget
			},
			scope: this,
			success: this.processServerResponse,
			failure: function(r){
				clientVM.displayFailure(r);
			}
		});
		return value;
	},

	processServerResponse: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			var link = response.data;
			if (this.linkTarget && link.indexOf('target') <= 0) {
				var hrefIndex = link.indexOf('href'),
					linkArray = link.split('');
				linkArray.splice(hrefIndex, 0, Ext.String.format(' target="{0}" ', this.linkTarget));
				link = linkArray.join('');
			}
			if (this.rendered) this.setText(link, false)
			else this.html = link
			// check HTML anchor href links
			var anchors = window.document.querySelectorAll('a');
			for (var i=0; i < anchors.length; i++) {
				var element = anchors[i];
				var link = element.getAttribute('href');
				if (link) {
					clientVM.injectCSRFPageTokenToElement(link, element, 'href');
				}
			}
			this.setDisabled(this.disabled)
		} else {
			clientVM.displayError(response.message);
		}
	},

	setDisabled: function(disabled) {
		this.disabled = disabled;
		if (!this.rendered) return;
		var links = this.getEl().query('a');
		Ext.each(links, function(link) {
			if (disabled) {
				this.oldLink = Ext.fly(link).dom.href;
				Ext.fly(link).dom.href = 'javascript:void(0)';
				Ext.fly(link).setStyle({
					color: 'grey'
				});
			} else {
				if (this.oldLink) Ext.fly(link).dom.href = this.oldLink;
				Ext.fly(link).setStyle({
					color: 'blue'
				});
			}
		}, this)
	}
});
/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.ListField', {
	extend: 'Ext.container.Container',
	alias: 'widget.listfield',

	layout: 'fit',
	border: false,
	choiceSeparator: '|&|',
	choiceValueSeparator: '|=|',

	initComponent: function() {
		var me = this;

		if (!Ext.isArray(this.items)) this.items = [{
			xtype: 'hidden',
			name: this.name
		}];

		this.items = [{
			xtype: 'grid',
			forceFit: true,
			title: this.fieldLabel,
			disableSelection: this.readOnly,
			displayField: 'name',
			valueField: 'value',
			store: Ext.create('Ext.data.Store', {
				data: this.parseOptions(this.choiceValues),
				fields: [{
					name: 'name'
				}, {
					name: 'value'
				}]
			}),
			plugins: [Ext.create('Ext.grid.plugin.CellEditing', {
				pluginId: 'cellEditor',
				listeners: {
					scope: this,
					edit: this.listChanged
				}
			})],
			hideHeaders: true,
			columns: [{
				dataIndex: 'name',
				editor: {}
			}],
			selModel: {
				mode: 'MULTI'
			},
			listeners: {
				selectionchange: {
					scope: this,
					fn: this.listChanged
				}
			},
			tbar: [{
				text: RS.formbuilder.locale.add,
				disabled: this.readOnly,
				scope: this,
				handler: function(button) {
					var grid = this.down('grid'),
						editor = grid.getPlugin('cellEditor');
					grid.store.add({
						name: 'NEW'
					});
					//Automatically start editing the newly added record
					if (editor) {
						editor.startEditByPosition({
							row: grid.store.getCount() - 1,
							column: 0
						});
					}
				}
			}, {
				text: RS.formbuilder.locale.remove,
				itemId: 'removeButton',
				disabled: true,
				scope: this,
				handler: function(button) {
					var grid = this.down('grid'),
						values = grid.getSelectionModel().getSelection(),
						i = 0;
					Ext.each(values, function(value) {
						grid.store.remove(value);
					});
				}
			}]
		}].concat(this.items)

		this.callParent()

		this.on('afterrender', function() {
			me.initializeValues()
		}, this, {
			single: true
		})

		var hidden = this.down('hiddenfield')
		if (hidden) {
			hidden.listSetValue = hidden.setValue
			hidden.setValue = function(value, internal) {
				me.value = value
				if (!internal) me.initializeValues()
				hidden.listSetValue(value)
			}
		}
	},
	initializeValues: function() {
		var values = this.parseValue(this.value || this.defaultValue || '');
		var grid = this.down('grid');
		if (grid) {
			var store = grid.store,
				sm = grid.getSelectionModel();
			sm.deselectAll()
			Ext.each(values, function(value) {
				var index = store.indexOf(store.findRecord('name', value, 0, false, false, true))
				if (index == -1) {
					store.add({
						name: value,
						value: value
					})
					sm.select(store.getCount() - 1, true)
				} else
					sm.select(index, true)
			}, this)
		}
	},
	listChanged: function(model, selected, eOpts) {
		if (!Ext.isArray(selected)) selected = this.getGrid().getSelectionModel().getSelection()
		if (selected.length > 0) {
			this.down('#removeButton').enable()
		} else {
			this.down('#removeButton').disable()
		}
		var selectedNames = [];
		Ext.Array.forEach(selected, function(select) {
			selectedNames.push(select.data.name);
		});

		this.down('hiddenfield').setValue(selectedNames.join(this.choiceSeparator), true)
	},
	getGrid: function() {
		return this.down('grid')
	},
	setFieldStyle: function(style) {
		var fields = this.query('field');
		Ext.each(fields, function(field) {
			if (field.setFieldStyle) field.setFieldStyle(style);
		});
	}
});
/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.NameField', {
	extend: 'Ext.form.FieldContainer',
	alias: 'widget.namefield',

	layout: 'hbox',
	nameSeparator: '|&|',

	initComponent: function() {
		var fieldName = this.name,
			values = this.value ? this.value.split(this.nameSeparator) : [],
			showings = this.selectValues ? this.selectValues.split(this.nameSeparator) : [true, true, false, true],
			i = 0;
		for(; i < showings.length; i++) {
			if(Ext.isString(showings[i])) showings[i] = showings[i] === 'true'
		}

		if(!Ext.isArray(this.items)) this.items = [{
			xtype: 'hiddenfield',
			name: this.name
		}];
		this.fieldLabel =  Ext.String.htmlEncode(this.fieldLabel || '');
		this.items = [{
			xtype: 'textfield',
			flex: 1,
			hideLabel: true,
			allowBlank: !this.mandatory || this.readOnly,
			readOnly: this.readOnly,
			fieldCls: this.fieldCls,
			value: values[0] || '',
			hidden: showings[0],
			disabled: showings[0],
			listeners: {
				change: {
					scope: this,
					fn: this.nameChanged
				}
			}
		}, {
			xtype: 'textfield',
			flex: 1,
			hideLabel: true,
			readOnly: this.readOnly,
			fieldCls: this.fieldCls,
			allowBlank: !this.mandatory || this.readOnly,
			margins: '0 0 0 5',
			value: values[1] || '',
			hidden: showings[1],
			disabled: showings[1],
			listeners: {
				change: {
					scope: this,
					fn: this.nameChanged
				}
			}
		}, {
			xtype: 'textfield',
			width: 30,
			hideLabel: true,
			readOnly: this.readOnly,
			fieldCls: this.fieldCls,
			allowBlank: !this.mandatory || this.readOnly,
			margins: '0 0 0 5',
			value: values[2] || '',
			hidden: showings[2],
			disabled: showings[2],
			listeners: {
				change: {
					scope: this,
					fn: this.nameChanged
				}
			}
		}, {
			xtype: 'textfield',
			flex: 2,
			hideLabel: true,
			readOnly: this.readOnly,
			fieldCls: this.fieldCls,
			allowBlank: !this.mandatory || this.readOnly,
			margins: '0 0 0 5',
			value: values[3] || '',
			hidden: showings[3],
			disabled: showings[3],
			listeners: {
				change: {
					scope: this,
					fn: this.nameChanged
				}
			}
		}].concat(this.items);

		this.callParent();
	},
	nameChanged: function() {
		var name = [];
		this.items.each(function(part) {
			if(part.xtype !== 'hiddenfield') name.push(part.getValue());
		});
		this.items.getAt(4).setValue(name.join(this.nameSeparator));
	},
	setFieldStyle: function(style){
		var fields = this.query('field');
		Ext.each(fields, function(field){
			if( field.setFieldStyle)field.setFieldStyle(style);
		});
	}
});
/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.ReferenceField', {
	extend: 'Ext.form.field.Trigger',
	alias: 'widget.referencefield',

	initComponent: function() {
		this.editable = false;
		this.callParent();
	},
	onRender: function() {
		//call into the server to get information about the columns that are going to come back from a particular table
		Ext.Ajax.request({
			url: '/resolve/service/form/getcustomtablecolumns',
			params: {
				tableName: this.referenceTable
			},
			scope: this,
			success: this.processColumns,
			failure: function(r){
				clientVM.displayFailure(r);
			}
		});
		this.callParent(arguments);
		this.setShowClearTrigger(false);

		var name = this.name || this.el.dom.name;
		// this.el.dom.removeAttribute('name');
		this.hiddenField = this.el.insertSibling({
			tag: 'input',
			type: 'hidden',
			name: name
		});
		this.hiddenName = name;
		// otherwise field is not found by BasicForm::findField
		if (this.value) {
			this.setShowClearTrigger(true);
			Ext.Ajax.request({
				url: '/resolve/service/dbapi/getRecordData',
				params: {
					tableName: this.referenceTable,
					type: 'form',
					start: 0,
					filter: Ext.encode([{
						field: 'sys_id',
						type: 'auto',
						condition: 'equals',
						value: this.value
					}])
				},
				scope: this,
				success: this.processValue,
				failure: function(r){
					clientVM.displayFailure(r);
				}
			})
		}

		//setup tooltips
		Ext.create('Ext.tip.ToolTip', {
			target: this.triggerEl.item(0),
			html: RS.formbuilder.locale.refrenceClear
		});
		Ext.create('Ext.tip.ToolTip', {
			target: this.triggerEl.item(1),
			html: RS.formbuilder.locale.referenceJump
		});
		Ext.create('Ext.tip.ToolTip', {
			target: this.triggerEl.item(2),
			html: RS.formbuilder.locale.referenceSearch
		});

	},

	processColumns: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			if (Ext.isArray(response.records)) {
				this.gridColumns = [];
				this.storeFields = [];
				var storeType = '';
				Ext.each(response.records, function(record) {
					this.gridColumns.push({
						header: record.displayName,
						dataIndex: record.columnModelName,
						filter: true
					});

					switch (record.type) {
						case 'boolean':
							storeType = 'boolean';
							break;

						case 'timestamp':
							storeType = 'date';
							break;

						case 'integer':
						case 'long':
						case 'float':
						case 'double':
							storeType = 'numeric';
							break;

						default:
							storeType = 'string';
							break;
					}
					this.storeFields.push({
						name: record.columnModelName,
						type: storeType,
						dateFormat: 'c'
					});
				}, this);

				this.gridStore = Ext.create('Ext.data.Store', {
					fields: this.storeFields,
					remoteSort: true,
					remoteFilter: true,
					proxy: {
						type: 'ajax',
						url: '/resolve/service/dbapi/getRecordData',
						extraParams: {
							tableName: this.referenceTable,
							type: 'table'
						},
						reader: {
							type: 'json',
							root: 'records'
						},
						listeners: {
							exception: function(e, resp, op) {
								clientVM.displayExceptionError(e, resp, op);
							}
						}
					}
				});
				if (this.referenceTable) this.gridStore.load();
			}
			//go through the records and populate the grid columns and types for the grid store
		} else clientVM.displayError(response.message);
	},

	processValue: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			if (response.data) {
				this.selectReferenceRecord(response);
			}
		} else clientVM.displayError(response.message);
	},

	setValue: function(value, process) {
		this.callParent(arguments);
		if (value && process !== false) {
			this.setShowClearTrigger(true);
			Ext.Ajax.request({
				url: '/resolve/service/dbapi/getRecordData',
				params: {
					tableName: this.referenceTable,
					type: 'form',
					start: 0,
					filter: Ext.encode([{
						field: 'sys_id',
						type: 'auto',
						condition: 'equals',
						value: value
					}])
				},
				scope: this,
				success: this.processValue,
				failure: function(r){
					clientVM.displayFailure(r);
				}
			})
		}

	},

	//Search Trigger
	trigger3Cls: Ext.baseCSSPrefix + 'form-search-trigger',
	onTrigger3Click: function() {
		var me = this;
		//open search dialog window
		var selectionWindow = Ext.create('Ext.window.Window', {
			modal: true,
			width: 600,
			height: 400,
			layout: 'fit',
			title: RS.formbuilder.locale.SelectRecordTitle,
			items: [{
				xtype: 'grid',
				forceFit: false,
				columns: this.gridColumns,
				store: this.gridStore,
				bbar: Ext.create('Ext.PagingToolbar', {
					store: this.gridStore,
					displayInfo: true,
					displayMsg: 'Displaying records {0} - {1} of {2}',
					emptyMsg: "No records to display"
				}),
				listeners: {
					selectionchange: function(grid, selected, eOpts) {
						var selectButton = selectionWindow.down('#selectButton');
						if (selected.length == 1) selectButton.enable()
						else selectButton.disable()
					},
					itemdblclick: function(selectionModel, record, item, index, e, eOpts) {
						me.selectReferenceRecord(record)
						selectionWindow.close()
					}
				}
			}],
			buttonAlign: 'left',
			buttons: [{
				text: RS.formbuilder.locale.select,
				itemId: 'selectButton',
				disabled: true,
				handler: function(button) {
					var win = button.up('window'),
						grid = win.down('grid');
					me.selectReferenceRecord(grid.getSelectionModel().getSelection()[0]);
					win.close();
				}
			}, {
				text: RS.formbuilder.locale.cancel,
				handler: function(button) {
					button.up('window').close();
				}
			}]
		});

		selectionWindow.show();
	},

	selectReferenceRecord: function(record) {
		this.setValue(record.data[this.referenceDisplayColumn], false);
		this.hiddenField.dom.value = record.data.sys_id;
		this.setShowClearTrigger(true);
	},

	//Clear Trigger
	triggerCls: Ext.baseCSSPrefix + 'form-clear-trigger',
	onTriggerClick: function() {
		//clear the current value and hide the clear button
		this.setValue('');
		this.hiddenField.dom.value = '';
		this.setShowClearTrigger(false);
	},

	//Jump Trigger
	trigger2Cls: Ext.baseCSSPrefix + 'form-jump-trigger',
	onTrigger2Click: function() {
		window.open(Ext.String.format(this.referenceTarget, this.hiddenField.dom.value), '_blank');
	},

	setShowClearTrigger: function(display) {
		this.triggerEl.item(0).parent().setDisplayed(display ? 'table-cell' : 'none');
		if (this.referenceTarget) this.triggerEl.item(1).parent().setDisplayed(display ? 'table-cell' : 'none');
		else this.triggerEl.item(1).parent().setDisplayed('none');
		this.doComponentLayout();
	},

	getRawValue: function() {
		return this.rendered ? this.hiddenField.dom.value : '';
	}
});
/**
 *
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.ReferenceTable', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.referencetable',

	layout: 'fit',

	anchor: '-20',
	height: 300,
	border: false,

	resizable: true,
	resizeHandles: 's',
	csrftoken: '',

	initComponent: function() {
		var me = this;

		if (this.referenceTableUrl != '') {
			var url = this.referenceTableUrl.split('?');
			clientVM.getCSRFToken_ForURI(url[0], function(token_pair) {
				this.csrftoken = token_pair[0] + '=' + token_pair[1];
			}.bind(this))
		}

		var displayNames = me.displayName ? me.displayName.split('|') : [],
			referenceTables = me.refGridReferenceByTable ? me.refGridReferenceByTable.split('|') : [],
			refGridReferenceColumnName = me.refGridReferenceColumnName ? me.refGridReferenceColumnName.split('|') : [],
			referenceColumns = me.refGridSelectedReferenceColumns ? me.refGridSelectedReferenceColumns.split('|') : [],
			allowReferenceTableAdd = me.allowReferenceTableAdd ? me.allowReferenceTableAdd.split('|') : [],
			allowReferenceTableRemove = me.allowReferenceTableRemove ? me.allowReferenceTableRemove.split('|') : [],
			allowReferenceTableView = me.allowReferenceTableView ? me.allowReferenceTableView.split('|') : [],
			referenceTableUrl = me.referenceTableUrl ? me.referenceTableUrl.split('|') : [],
			referenceTableViewPopup = me.refGridViewPopup ? me.refGridViewPopup.split('|') : [],
			referenceTableViewPopupWidth = me.refGridViewPopupWidth ? me.refGridViewPopupWidth.split('|') : [],
			referenceTableViewPopupHeight = me.refGridViewPopupHeight ? me.refGridViewPopupHeight.split('|') : [],
			referenceTableViewPopupTitle = me.refGridViewTitle ? me.refGridViewTitle.split('|') : [],
			len = displayNames.length,
			i, tabs = [];

		for (i = 0; i < len; i++) {
			var cols = referenceColumns[i] ? Ext.decode(referenceColumns[i]) : [];
			tabs.push({
				title: displayNames[i],
				referenceTable: referenceTables[i],
				referenceColumn: refGridReferenceColumnName[i],
				referenceColumns: cols,
				allowReferenceTableAdd: allowReferenceTableAdd[i] == 'true',
				allowReferenceTableRemove: allowReferenceTableRemove[i] == 'true',
				allowReferenceTableView: allowReferenceTableView[i] == 'true',
				referenceTableUrl: referenceTableUrl[i],
				referenceTableViewPopup: referenceTableViewPopup[i] == 'true',
				referenceTableViewPopupWidth: Number(referenceTableViewPopupWidth[i]),
				referenceTableViewPopupHeight: Number(referenceTableViewPopupHeight[i]),
				referenceTableViewPopupTitle: referenceTableViewPopupTitle[i]
			});
		}

		//Now render either the tabs or the one grid
		var config = {};
		if (len > 1) {
			config.xtype = 'tabpanel';
			config.plain = true;
			config.items = [];
			Ext.each(tabs, function(tab) {
				config.items.push(this.convertToGrid(tab));
			}, this);
		} else {
			config = this.convertToGrid(tabs[0]);
		}
		me.items = config;

		me.callParent();

		if (this.recordId) me.loadReferences();
	},
	convertToGrid: function(serverConf) {
		var me = this;
		var config = Ext.apply({}, serverConf, {
			xtype: 'grid'
		});
		config.columns = [{
			xtype: 'actioncolumn',
			hideable: false,
			hidden: !serverConf.allowReferenceTableView,
			width: 25,
			items: [{
				icon: '/resolve/images/link.gif',
				// Use a URL in the icon config
				tooltip: RS.formbuilder.locale.viewReference,
				scope: this,
				handler: function(grid, rowIndex, colIndex) {
					var rec = grid.getStore().getAt(rowIndex),
						sys_id = rec.get('sys_id'),
						url = serverConf.referenceTableUrl,
						me = this;
					if (sys_id) {
						//open new window to the url configured for this reference table
						var containsSysIdAlready = url.indexOf('${sys_id}') > -1;

						url = url.replace(/${sys_id}/g, sys_id).replace(/${parent_sys_id}/g, me.recordId);
						var temp = url.match(/\${(.*?)}/g)
						if (temp && temp.length > 0)
							Ext.Array.forEach(temp, function(match) {
								var val = rec.get(match.substring(2, match.length - 1))
								if (val) url = url.replace(match, val)
							})

						if (!containsSysIdAlready) {
							var split = url.split('?'),
								params = {};
							if (split.length > 1) {
								params = Ext.Object.fromQueryString(split[1]);
							}
							url = split[0] + '?' + Ext.urlEncode(Ext.applyIf({
								sys_id: sys_id
							}, params));
						}

						if (url.indexOf('/resolve/') === 0 && url.indexOf('OWASP_CSRFTOKEN') === -1) {
							const urlData = url.split('#');
							const uriData = urlData[0].split('?');
							clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
								var csrftoken = '?' + data[0] + '=' + data[1];
								var tokenizedData = uri + csrftoken;
								if (uriData.length > 1) {
									tokenizedData += '&' + uriData[1];
								}
								if (urlData.length > 1) {
									tokenizedData += '#' + urlData[1];
								}
								url = tokenizedData;
							});
						}

						if (!serverConf.referenceTableViewPopup) window.open(url);
						else {
							Ext.create('Ext.window.Window', {
								modal: true,
								border: false,
								title: serverConf.referenceTableViewPopupTitle || RS.formbuilder.locale.NewReference + serverConf.title,
								width: Number(serverConf.referenceTableViewPopupWidth) || 600,
								height: Number(serverConf.referenceTableViewPopupHeight) || 400,
								layout: 'fit',
								html: Ext.String.format('<iframe class="rs_iframe" src="{0}" height="100%" style="width:100%;border:0" frameborder="0"/>', url),
								listeners: {
									render: function(w) {
										w.getEl().select('iframe').on('load', function(e, t, eOpts) {
											var me = this;
											t.contentWindow.registerForm = function(form) {
												form.on('beforeclose', function() {
													Ext.defer(function() {
														this.close();
													}, 100, this);
													return false;
												}, me);
												form.on('beforeexit', function() {
													Ext.defer(function() {
														this.close();
													}, 100, this);
													return false;
												}, me);
											}
										}, w);
									},
									close: {
										scope: this,
										fn: function() {
											this.loadReferences();
										}
									}
								}
							}).show();
						}
					}
				}
			}]
		}];

		//calculate columns to display
		var storeFields = ['sys_id'];
		Ext.each(serverConf.referenceColumns, function(displayColumn) {
			var type = 'string';
			switch (displayColumn.dbtype) {
				case 'timestamp':
					type = 'date';
					break;
				default:
					type = 'string';
					break;
			}
			var storeCol = {
				name: displayColumn.value,
				type: type
			};
			var col = {
				flex: 1,
				text: displayColumn.name,
				dataIndex: displayColumn.value
			};
			if (type == 'date') {
				storeCol.format = 'time'
				storeCol.dateFormat = 'time'
				col.renderer = Ext.util.Format.dateRenderer('Y-m-d g:i:s A')
			}

			storeFields.push(storeCol);
			config.columns.push(col);
		});

		

		config.store = Ext.create('Ext.data.Store', {
			fields: storeFields,
			proxy: {
				type: 'ajax',
				url: '/resolve/service/dbapi/getRecordData',
				extraParams: {
					sqlQuery: Ext.String.format('select * from {0} where {1} = \'{2}\'', serverConf.referenceTable, serverConf.referenceColumn, this.recordId),
					type: 'table',
					useSql: true
				},
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});

		// config.selModel = Ext.create('Ext.selection.CheckboxModel');
		var generatedId = Ext.id();
		config.dockedItems = [{
			xtype: 'toolbar',
			dock: 'top',
			name: 'actionBar',
			items: [{
				text: RS.formbuilder.locale.referenceTableNew,
				name: 'newReference',
				itemId: 'newButton',
				hidden: !serverConf.allowReferenceTableAdd,
				scope: this,
				handler: function(button) {
					var url = serverConf.referenceTableUrl ? serverConf.referenceTableUrl : '';

					if (url != '' && this.csrftoken != '') {
						var urlData = url.split('?');
						url = urlData[0] + '?' + this.csrftoken;
						if (urlData.length > 1) {
							url += '&' + urlData[1];
						}
					}

					url = url.replace(/\${parent_sys_id}/g, me.recordId);

					//pull fields from current form into the url if needed
					var temp = url.match(/\${(.*?)}/g)
					if (temp && temp.length > 0) {
						var form = this.up('form'),
							values = form.getValues();
						Ext.Array.forEach(temp, function(match) {
							var val = values[match.substring(2, match.length - 1)]
							if (val != null) url = url.replace(match, val)
						}, this)
					}

					if (url.indexOf('/resolve/') === 0 && url.indexOf('OWASP_CSRFTOKEN') === -1) {
						const urlData = url.split('#');
						const uriData = urlData[0].split('?');
						clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
							var csrftoken = '?' + data[0] + '=' + data[1];
							var tokenizedData = uri + csrftoken;
							if (uriData.length > 1) {
								tokenizedData += '&' + uriData[1];
							}
							if (urlData.length > 1) {
								tokenizedData += '#' + urlData[1];
							}
							url = tokenizedData;
						});
					}

					if (!serverConf.referenceTableViewPopup && url) window.open(url);
					else {
						Ext.create('Ext.window.Window', {
							modal: true,
							border: false,
							title: serverConf.referenceTableViewPopupTitle || RS.formbuilder.locale.NewReference + serverConf.title,
							width: Number(serverConf.referenceTableViewPopupWidth) || 600,
							height: Number(serverConf.referenceTableViewPopupHeight) || 400,
							layout: 'fit',
							html: Ext.String.format('<iframe class="rs_iframe" src="{0}" height="100%" style="width:100%;border:0" frameborder="0"/>', url),
							listeners: {
								render: function(w) {
									w.getEl().select('iframe').on('load', function(e, t, eOpts) {
										var me = this;
										t.contentWindow.registerForm = function(form) {
											form.on('beforeclose', function() {
												Ext.defer(function() {
													this.close();
												}, 100, this);
												return false;
											}, me);
											form.on('beforeexit', function() {
												Ext.defer(function() {
													this.close();
												}, 100, this);
												return false;
											}, me);
										}
									}, w);
								},
								close: {
									scope: this,
									fn: function() {
										this.loadReferences();
									}
								}
							}
						}).show();
					}
				}
			}, {
				text: RS.formbuilder.locale.referenceTableDelete,
				disabled: true,
				name: 'removeReference',
				itemId: 'removeButton' + generatedId,
				hidden: !serverConf.allowReferenceTableRemove,
				scope: this,
				handler: function(button) {
					var grid = button.up('grid'),
						selections = grid.getSelectionModel().getSelection();
					Ext.MessageBox.show({
						title: RS.formbuilder.locale.confirmRemove,
						msg: selections.length === 1 ? RS.formbuilder.locale.confirmRemoveReference : Ext.String.format(RS.formbuilder.locale.confirmRemoveReferences, selections.length),
						buttons: Ext.MessageBox.YESNO,
						buttonText: {
							yes: RS.formbuilder.locale.deleteReference,
							no: RS.formbuilder.locale.cancel
						},
						scope: grid,
						fn: this.removeSelectedReferences
					});
				}
			}]
		}];

		config.plugins = [{
			ptype: 'pager'
		}];

		config.listeners = {
			scope: this,
			selectionchange: function(selectionModel, selections, eOpts) {
				if (selections.length > 0) this.down('#removeButton' + generatedId).enable();
				else this.down('#removeButton' + generatedId).disable();
			}
		};

		return config;
	},
	removeSelectedReferences: function(button) {
		if (button === 'yes') {
			var selections = this.getSelectionModel().getSelection(),
				ids = [];
			Ext.each(selections, function(selection) {
				ids.push(selection.get('sys_id'));
			});
			var panel = this.up('referencetable');
			Ext.Ajax.request({
				url: '/resolve/service/dbapi/delete',
				params: {
					tableName: this.referenceTable,
					whereClause: 'sys_id in (\'' + ids.join('\',\'') + '\')',
					type: 'form',
					useSql: true
				},
				scope: panel,
				success: panel.removeDeletedReferences,
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			});
		}
	},
	removeDeletedReferences: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			this.loadReferences();
		} else {
			clientVM.displayError(response.message);
		}
	},
	loadReferences: function() {
		var grids = this.query('grid');
		Ext.each(grids, function(grid) {
			grid.getStore().load();
		}, this);
	}
});
/**
 *
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.TabContainerField', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.tabcontainerfield',

	layout: 'fit',

	anchor: '-20',
	height: 300,
	border: false,

	resizable: true,
	resizeHandles: 's',
	csrftoken: '',

	initComponent: function() {
		var me = this;

		if (this.referenceTableUrl != '') {
			var url = this.referenceTableUrl.split('?');
			clientVM.getCSRFToken_ForURI(url[0], function(token_pair) {
				this.csrftoken = token_pair[0] + '=' + token_pair[1];
			}.bind(this))
		}

		var displayNames = me.displayName ? me.displayName.split('|') : [],
			referenceTables = me.refGridReferenceByTable ? me.refGridReferenceByTable.split('|') : [],
			refGridReferenceColumnName = me.refGridReferenceColumnName ? me.refGridReferenceColumnName.split('|') : [],
			referenceColumns = me.refGridSelectedReferenceColumns ? me.refGridSelectedReferenceColumns.split('|') : [],
			allowReferenceTableAdd = me.allowReferenceTableAdd ? me.allowReferenceTableAdd.split('|') : [],
			allowReferenceTableRemove = me.allowReferenceTableRemove ? me.allowReferenceTableRemove.split('|') : [],
			allowReferenceTableView = me.allowReferenceTableView ? me.allowReferenceTableView.split('|') : [],
			referenceTableUrl = me.referenceTableUrl ? me.referenceTableUrl.split('|') : [],
			referenceTableViewPopup = me.refGridViewPopup ? me.refGridViewPopup.split('|') : [],
			referenceTableViewPopupWidth = me.refGridViewPopupWidth ? me.refGridViewPopupWidth.split('|') : [],
			referenceTableViewPopupHeight = me.refGridViewPopupHeight ? me.refGridViewPopupHeight.split('|') : [],
			referenceTableViewPopupTitle = me.refGridViewTitle ? me.refGridViewTitle.split('|') : [],
			len = displayNames.length,
			i, tabs = [];

		for (i = 0; i < len; i++) {
			var cols = referenceColumns[i] ? Ext.decode(referenceColumns[i]) : [];
			tabs.push({
				title: displayNames[i],
				referenceTable: referenceTables[i],
				referenceColumn: refGridReferenceColumnName[i],
				referenceColumns: cols,
				allowReferenceTableAdd: allowReferenceTableAdd[i] == 'true',
				allowReferenceTableRemove: allowReferenceTableRemove[i] == 'true',
				allowReferenceTableView: allowReferenceTableView[i] == 'true',
				referenceTableUrl: referenceTableUrl[i],
				referenceTableViewPopup: referenceTableViewPopup[i] == 'true',
				referenceTableViewPopupWidth: Number(referenceTableViewPopupWidth[i]),
				referenceTableViewPopupHeight: Number(referenceTableViewPopupHeight[i]),
				referenceTableViewPopupTitle: referenceTableViewPopupTitle[i]
			});
		}

		//Now render either the tabs or the one grid
		var config = {};
		if (len > 1) {
			config.xtype = 'tabpanel';
			config.plain = true;
			config.items = [];
			Ext.each(tabs, function(tab) {
				config.items.push(this.convertToGrid(tab));
			}, this);
		} else {
			config = this.convertToGrid(tabs[0]);
		}
		me.items = config;

		me.callParent();

		if (this.recordId) me.loadReferences();
	},
	convertToGrid: function(serverConf) {
		var me = this;

		if (serverConf.referenceTableViewPopupTitle == 'FileManager') {
			return {
				border: false,
				title: serverConf.title,
				xtype: 'uploadmanager',
				formName: me.formName,
				recordId: me.recordId,
				fileUploadTableName: me.fileUploadTableName,
				referenceColumnName: me.referenceColumnName,
				allowUpload: serverConf.allowReferenceTableAdd,
				allowRemove: serverConf.allowReferenceTableRemove
			};
		}
		if (serverConf.referenceTableViewPopupTitle == 'URL') {
			if (serverConf.referenceTableUrl && serverConf.referenceTableUrl.indexOf('/resolve/') === 0) {
    			const urlData = serverConf.referenceTableUrl.split('#');
    			const uriData = urlData[0].split('?');
    			clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
    				var csrftoken = '?' + data[0] + '=' + data[1];
    				var tokenizedData = uri + csrftoken;
    				if (uriData.length > 1) {
    					tokenizedData += '&' + uriData[1];
    				}
    				if (urlData.length > 1) {
    					tokenizedData += '#' + urlData[1];
    				}
    				serverConf.referenceTableUrl = tokenizedData;
    			});
    		}

			return {
				border: false,
				title: serverConf.title,
				html: '&nbsp;',
				listeners: {
					afterrender: function(panel) {
						panel.update(Ext.String.format('<iframe class="rs_iframe" src="{0}" height="100%" style="width:100%;border:0" frameborder="0"/>', serverConf.referenceTableUrl))
					}
				}
			}
		}


		var config = Ext.apply({}, serverConf, {
			xtype: 'grid'
		});
		config.columns = [{
			xtype: 'actioncolumn',
			hideable: false,
			hidden: !serverConf.allowReferenceTableView,
			width: 25,
			items: [{
				icon: '/resolve/images/link.gif',
				// Use a URL in the icon config
				tooltip: RS.formbuilder.locale.viewReference,
				scope: this,
				handler: function(grid, rowIndex, colIndex) {
					var rec = grid.getStore().getAt(rowIndex),
						sys_id = rec.get('sys_id'),
						url = serverConf.referenceTableUrl,
						me = this;
					if (sys_id) {
						//open new window to the url configured for this reference table
						var containsSysIdAlready = url.indexOf('${sys_id}') > -1;

						url = url.replace(/${sys_id}/g, sys_id).replace(/${parent_sys_id}/g, me.recordId);

						if (!containsSysIdAlready) {
							var split = url.split('?'),
								params = {};
							if (split.length > 1) {
								params = Ext.Object.fromQueryString(split[1]);
							}
							url = split[0] + '?' + Ext.urlEncode(Ext.applyIf({
								sys_id: sys_id
							}, params));
						}

						if (url.indexOf('/resolve/') === 0 && url.indexOf('OWASP_CSRFTOKEN') === -1) {
							const urlData = url.split('#');
							const uriData = urlData[0].split('?');
							clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
								var csrftoken = '?' + data[0] + '=' + data[1];
								var tokenizedData = uri + csrftoken;
								if (uriData.length > 1) {
									tokenizedData += '&' + uriData[1];
								}
								if (urlData.length > 1) {
									tokenizedData += '#' + urlData[1];
								}
								url = tokenizedData;
							});
						}

						if (!serverConf.referenceTableViewPopup) window.open(url);
						else {
							Ext.create('Ext.window.Window', {
								modal: true,
								border: false,
								autoScroll: false,
								title: serverConf.referenceTableViewPopupTitle || RS.formbuilder.locale.EditReference + serverConf.title,
								width: Number(serverConf.referenceTableViewPopupWidth) || 600,
								height: Number(serverConf.referenceTableViewPopupHeight) || 400,
								layout: 'fit',
								html: Ext.String.format('<iframe class="rs_iframe" src="{0}" height="100%" style="width:100%;border:0" frameborder="0"/>', url),
								listeners: {
									render: function(w) {
										w.getEl().select('iframe').on('load', function(e, t, eOpts) {
											var me = this;
											t.contentWindow.registerForm = function(form) {
												form.on('beforeclose', function() {
													Ext.defer(function() {
														this.close();
													}, 100, this);
													return false;
												}, me);
												form.on('beforeexit', function() {
													Ext.defer(function() {
														this.close();
													}, 100, this);
													return false;
												}, me);
												form.on('titleChanged', function(title) {
													this.setTitle(title);
													return false;
												}, me);
											}
										}, w);
									},
									close: function() {
										me.loadReferences();
									}
								}
							}).show();
						}
					}
				}
			}]
		}];

		//calculate columns to display
		var storeFields = ['sys_id'];
		Ext.each(serverConf.referenceColumns, function(displayColumn) {
			var type = 'string';
			switch (displayColumn.dbtype) {
				case 'timestamp':
					type = 'date';
					break;
				default:
					type = 'string';
					break;
			}
			var storeCol = {
				name: displayColumn.value,
				type: type
			};
			var col = {
				flex: 1,
				text: displayColumn.name,
				dataIndex: displayColumn.value
			};
			if (type == 'date') {
				storeCol.format = 'c';
				col.renderer = Ext.util.Format.dateRenderer('Y-m-d g:i:s A');
			}

			storeFields.push(storeCol);
			config.columns.push(col);
		});

		var field = serverConf.referenceColumn;
		if (serverConf.referenceColumn == 'u_content_request' && serverConf.referenceTable != 'content_request') {
			field += '.sys_id';
		}

		config.store = Ext.create('Ext.data.Store', {
			fields: storeFields,
			proxy: {
				type: 'ajax',
				url: '/resolve/service/dbapi/getRecordData',
				extraParams: {
					tableName: serverConf.referenceTable,
					type: 'table',
					start: 0,
					filter: Ext.encode([{
						field: field,
						type: 'auto',
						condition: 'equals',
						value: this.recordId || '__blank'
					}])
				},
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});

		// config.selModel = Ext.create('Ext.selection.CheckboxModel');
		var generatedId = Ext.id();
		config.dockedItems = [{
			xtype: 'toolbar',
			dock: 'top',
			name: 'actionBar',
			items: [{
				text: RS.formbuilder.locale.referenceTableNew,
				name: 'newReference',
				itemId: 'newButton',
				hidden: !serverConf.allowReferenceTableAdd,
				scope: this,
				handler: function(button) {
					var url = serverConf.referenceTableUrl ? serverConf.referenceTableUrl : '';

					if (url != '' && this.csrftoken != '') {
						var urlData = url.split('?');
						url = urlData[0] + '?' + this.csrftoken;
						if (urlData.length > 1) {
							url += '&' + urlData[1];
						}
					}

					url = url.replace(/\${parent_sys_id}/g, me.recordId);

					if (url.indexOf('/resolve/') === 0 && url.indexOf('OWASP_CSRFTOKEN') === -1) {
						const urlData = url.split('#');
						const uriData = urlData[0].split('?');
						clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
							var csrftoken = '?' + data[0] + '=' + data[1];
							var tokenizedData = uri + csrftoken;
							if (uriData.length > 1) {
								tokenizedData += '&' + uriData[1];
							}
							if (urlData.length > 1) {
								tokenizedData += '#' + urlData[1];
							}
							url = tokenizedData;
						});
					}

					if (!serverConf.referenceTableViewPopup && url) window.open(url);
					else {
						Ext.create('Ext.window.Window', {
							modal: true,
							border: false,
							title: serverConf.referenceTableViewPopupTitle || RS.formbuilder.locale.NewReference + serverConf.title,
							width: Number(serverConf.referenceTableViewPopupWidth) || 600,
							height: Number(serverConf.referenceTableViewPopupHeight) || 400,
							layout: 'fit',
							html: Ext.String.format('<iframe class="rs_iframe" src="{0}" height="100%" style="width:100%;border:0" frameborder="0"/>', url),
							listeners: {
								render: function(w) {
									w.getEl().select('iframe').on('load', function(e, t, eOpts) {
										var me = this;
										t.contentWindow.registerForm = function(form) {
											form.on('beforeclose', function() {
												Ext.defer(function() {
													this.close();
												}, 100, this);
												return false;
											}, me);
											form.on('beforeexit', function() {
												Ext.defer(function() {
													this.close();
												}, 100, this);
												return false;
											}, me);
										}
									}, w);
								},
								close: {
									scope: this,
									fn: function() {
										this.loadReferences();
									}
								}
							}
						}).show();
					}
				}
			}, {
				text: RS.formbuilder.locale.referenceTableDelete,
				disabled: true,
				name: 'removeReference',
				itemId: 'removeButton' + generatedId,
				hidden: !serverConf.allowReferenceTableRemove,
				scope: this,
				handler: function(button) {
					var grid = button.up('grid'),
						selections = grid.getSelectionModel().getSelection();
					Ext.MessageBox.show({
						title: RS.formbuilder.locale.confirmRemove,
						msg: selections.length === 1 ? RS.formbuilder.locale.confirmRemoveReference : Ext.String.format(RS.formbuilder.locale.confirmRemoveReferences, selections.length),
						buttons: Ext.MessageBox.YESNO,
						buttonText: {
							yes: RS.formbuilder.locale.deleteReference,
							no: RS.formbuilder.locale.cancel
						},
						scope: grid,
						fn: this.removeSelectedReferences
					});
				}
			}]
		}];

		config.plugins = [{
			ptype: 'pager'
		}];

		config.listeners = {
			scope: this,
			selectionchange: function(selectionModel, selections, eOpts) {
				if (selections.length > 0) this.down('#removeButton' + generatedId).enable();
				else this.down('#removeButton' + generatedId).disable();
			}
		};

		return config;
	},
	removeSelectedReferences: function(button) {
		if (button === 'yes') {
			var selections = this.getSelectionModel().getSelection(),
				ids = [];
			Ext.each(selections, function(selection) {
				ids.push(selection.get('sys_id'));
			});
			var panel = this.up('referencetable');
			Ext.Ajax.request({
				url: '/resolve/service/dbapi/delete',
				params: {
					tableName: this.referenceTable,
					whereClause: 'sys_id in (\'' + ids.join('\',\'') + '\')',
					type: 'form',
					useSql: true
				},
				scope: panel,
				success: panel.removeDeletedReferences,
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			});
		}
	},
	removeDeletedReferences: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			this.loadReferences();
		} else {
			clientVM.displayError(response.message);
		}
	},
	loadReferences: function() {
		var grids = this.query('grid');
		Ext.each(grids, function(grid) {
			grid.getStore().load();
		}, this);
	}
});
/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.TagField', {
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.tagfield',

	displayField: 'name',
	valueField: 'value',
	queryMode: 'local',
	multiSelect: true,

	initComponent: function() {
		this.store = Ext.create('Ext.data.Store', {
			fields: ['name', 'value'],
			proxy: {
				type: 'ajax',
				url: '/resolve/service/form/getTags',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});
		this.store.load({});

		this.callParent();
	}
});
/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.TeamPicker', {
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.teampickerfield',

	displayField: 'name',
	valueField: 'value',
	queryMode: 'local',

	forceSelection: true,

	/**
	 * Pipe separated list of team id's to filter the users with.  For example 'id123|id456'
	 */
	teamsOfTeams: '',
	recurseThroughTeamsList: false,

	//Clear Trigger
	clearCls: Ext.baseCSSPrefix + 'form-clear-trigger',
	clearClick: function() {
		//clear the current value and hide the clear button
		this.setValue('');
		this.triggerEl.item(0).parent().setDisplayed('none');
	},

	initComponent: function() {
		this.trigger2Cls = this.triggerCls;
		this.onTrigger2Click = this.onTriggerClick;
		this.trigger1Cls = this.clearCls;
		this.onTrigger1Click = this.clearClick;

		this.store = Ext.create('Ext.data.Store', {
			fields: ['name', 'value', 'type'],
			proxy: {
				type: 'ajax',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		});

		Ext.Ajax.request({
			url: '/resolve/service/form/getTeams',
			params: {
				teamPickerTeamsOfTeams: this.teamPickerTeamsOfTeams,
				recurseTeamsOfTeam: this.recurseTeamsOfTeam
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					this.store.loadData(response.records);
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(r){
				clientVM.displayFailure(r);
			}
		});

		this.callParent();
	},

	onRender: function() {
		this.callParent(arguments);
		this.triggerEl.item(0).parent().setDisplayed('none');
	},

	setValue: function(value) {
		this.callParent(arguments);
		if (this.rendered && this.value && !this.mandatory && !this.readOnly) {
			this.triggerEl.item(0).parent().setDisplayed('table-cell');
			this.doComponentLayout();
		}
	}
});
/**
 *
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.UrlContainerField', {
	extend: 'Ext.Panel',
	alias: 'widget.urlcontainerfield',

	initComponent: function() {
		var me = this;
		me.html = '&nbsp;';
		me.callParent();
		me.on('afterrender', function() {
			me.update(Ext.String.format('<iframe class="rs_iframe" src="{0}" height="100%" style="width:100%;border:0" frameborder="0"/>', me.referenceTableUrl));
		})
	}
});
/**
 * Combobox that allows the user to choose from a list of users in the system.  Its filtered by providing groups of users that should be returned from the server
 */
Ext.define('RS.formbuilder.viewer.desktop.fields.UserPicker', {
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.userpickerfield',

	displayField: 'name',
	valueField: 'value',
	queryMode: 'local',

	meValue: 'me',

	allowAssignToMe: false,
	autoAssignToMe: false,
	// forceSelection: true,

	//user trigger
	userCls: 'icon-user', //Ext.baseCSSPrefix + 'form-user-trigger',
	userClick: function() {
		this.setValue(this.meValue);
	},

	//Clear Trigger
	clearCls: Ext.baseCSSPrefix + 'form-clear-trigger',
	clearClick: function() {
		//clear the current value and hide the clear button
		this.clearing = true;
		this.setValue('', true);
		this.triggerEl.item(0).parent().setDisplayed('none');
	},

	/**
	 * Pipe separated list of group id's to filter the users with.  For example 'id123|id456'
	 */
	groups: '',
	usersOfTeams: '',
	recurseUsersOfTeam: false,
	teamsOfTeams: '',
	recurseTeamsOfTeam: false,

	stillLoading: false,

	initComponent: function() {
		this.trigger2Cls = this.triggerCls;
		this.onTrigger2Click = this.onTriggerClick;
		this.trigger1Cls = this.clearCls;
		this.onTrigger1Click = this.clearClick;
		this.trigger3Cls = this.userCls;
		this.trigger3OverCls = this.userCls;
		this.onTrigger3Click = this.userClick;

		this.store = Ext.create('Ext.data.Store', {
			fields: ['name', 'value', 'type'],
			proxy: {
				type: 'memory',
				reader: {
					type: 'json',
					root: 'records'
				}
			}
		});

		this.stillLoading = true;
		Ext.Ajax.request({
			url: '/resolve/service/form/getUsers',
			jsonData: {
				groups: this.groups || '',
				usersOfTeams: this.usersOfTeams || '',
				recurseUsersOfTeam: this.recurseUsersOfTeam,
				teamsOfTeams: this.teamsOfTeams,
				recurseTeamsOfTeam: this.recurseTeamsOfTeam
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (!response.success) {
					clientVM.displayError(response.message);
					return;
				}
				this.store.add(response.records);
				this.stillLoading = false;
				this.setValueActual(this.tempValue);
				if (this.autoAssignToMe && !this.getValue()) this.setValue(this.meValue);
				//if there is only one team, then auto assign to that team, overriding the auto-assign-to-me option
				var assignToGroup = '';
				this.store.each(function(record) {
					if (record.data.type == 'team') {
						if (!assignToGroup) assignToGroup = record.data.value;
						else {
							assignToGroup = false;
							return false;
						}
					}
				})
				if (assignToGroup && !this.getValue()) {
					this.setValue(assignToGroup);
				}
			},
			failure: function(r){
				clientVM.displayFailure(r);
			}
		});


		this.callParent();
	},
	onRender: function() {
		this.callParent(arguments);
		if (this.readOnly) {
			this.getEl().select('input').setStyle({
				border: '0px solid transparent',
				backgroundImage: 'none',
				cursor: 'auto'
			});
		}
		var tip = Ext.create('Ext.tip.ToolTip', {
			target: this.triggerEl.item(2),
			html: RS.formbuilder.locale.assignToMe
		});
		this.triggerEl.item(0).parent().setDisplayed('none');
		if (!this.allowAssignToMe) this.triggerEl.item(2).parent().setDisplayed('none');
		else {
			this.triggerEl.item(2).addCls('icon-large rs-icon rs-no-background')
		}
	},
	//snapshot of current value when the control is still loading the field
	tempValue: '',
	setValue: function(value, clear) {
		if (this.stillLoading) {
			this.tempValue = value;
			return;
		}
		this.callParent(arguments);
		if (this.rendered && this.value && !this.mandatory && !this.readOnly) {
			this.triggerEl.item(0).parent().setDisplayed('table-cell');
			this.doComponentLayout();
		}

		if (this.rendered && !this.value && !clear) {
			if (this.clearing) {
				this.clearing = false;
				return;
			}
			//Being passed null because we're being reset.  Run through the default logic again
			if (this.autoAssignToMe === null) {
				Ext.defer(function() {
					this.setValue(this.meValue);
				}, 100, this)
				return;
			}
			//if there is only one team, then auto assign to that team, overriding the auto-assign-to-me option
			var assignToGroup = '';
			this.store.each(function(record) {
				if (record.data.type == 'team') {
					if (!assignToGroup) assignToGroup = record.data.value;
					else {
						assignToGroup = false;
						return false;
					}
				}
			});
			if (assignToGroup && !this.getValue()) {
				this.setValue(assignToGroup);
			}
		}
	}
});
/**
 * Rendering engine for the FormBuilder.  Takes a formConfiguration, formName, or formId to display the user configured controls.
 */
Ext.define('RS.formbuilder.viewer.desktop.RSForm', {
	API : {
		submit : '/resolve/service/form/submit',
		getForm : '/resolve/service/form/getform',
		getRecordData : '/resolve/service/dbapi/getRecordData',
		getDefaultData : '/resolve/service/form/getdefaultdata',
		getATProperty : '/resolve/service/atproperties/list',
		getWSData : '/resolve/service/wsdata/getMap'
	},
	showLoading: true,
	params: '',
	fromViewer: false,
	formdefinition: false,

	/**
	 * Separator used to separate different fields in the choice and select fields
	 */
	choiceSeparator: '|&|',

	/**
	 * Separator used to separate different values in the choice and select fields
	 */
	choiceValueSeparator: '|=|',

	extend: 'Ext.panel.Panel',
	alias: 'widget.rsform',

	border: false,
	layout: 'fit',
	fieldConfiguration: [],
	submitConfiguration: {
		viewName: '',
		formSysId: '',
		tableName: '',
		userId: '',
		timezone: '',
		crudAction: '',
		query: '',
		controlItemSysId: '',
		dbRowData: {},
		sourceRowData: {}
	},

	embed: false,
	hideToolbar: false,

	/**
	 * Id of the form to use when looking up the controls from the server
	 */
	formId: '',
	setFormId: function(value) {
		if (value != this.formId) {
			this.formId = value
			this.fireEvent('reloadForm', this)
		}
	},

	/**
	 * Name of the form to use when looking up the controls from the server
	 */
	formName: '',
	setFormName: function(value) {
		if (value != this.formName) {
			this.formName = value
			this.fireEvent('reloadForm', this)
		}
	},

	/**
	 * The record to load into the grid once the form has actually been rendered (or empty for a new record)
	 */
	recordId: '',
	setRecordId: function(value) {
		if (value != this.recordId) {
			this.recordId = value
			this.fireEvent('reloadForm', this)
		}
	},
	queryString: '',
	setQueryString: function(value) {
		if (value != this.queryString) {
			this.queryString = value
			this.fireEvent('reloadForm', this)
		}
	},

	params: '',
	setParams: function(value) {
		if (value != this.params) {
			this.params = value
			this.fireEvent('reloadForm', this)
		}
	},

	initComponent: function() {
		this.addEvents(
			/**
			 * @event actioncompleted
			 * Fires when the button is clicked and all actions have completed running on the server
			 * @param {RS.formbuilder.RSForm} form The form generating this event
			 * @param {Ext.button.Button} button The button that was clicked
			 */
			'actioncompleted', 'beforeclose', 'beforeexit', 'beforeredirect', 'reloadForm');
		//convert all the items configurations to proper configurations
		var configuration = this.formConfiguration;
		if (configuration) this.convertConfig(configuration);
		else if (!this.fromViewer) this.loadForm()
		if (!this.height) delete this.height;
		this.callParent();

		this.on('render', function() {
			var me = this;
			if (me.autoSize) {
				if (!me.height) {
					var doc = Ext.getDoc(),
						size = doc.getViewSize(),
						windowHeight = size.height,
						windowWidth = size.width;
					Ext.EventManager.onWindowResize(function() {
						var doc = Ext.getDoc(),
							size = doc.getViewSize();
						me.maxHeight = size.height - 1;
						me.setWidth(size.width);
						me.doLayout();
						Ext.each(Ext.dom.Query.select('div[class=x-mask]'), function(el) {
							Ext.fly(el).setStyle('display', 'none');
						});
					});
					doc.select('body').setStyle('overflow', 'hidden');
					me.maxHeight = windowHeight - 1;
					me.setWidth(windowWidth);
					me.doLayout();
				} else {
					//Still correct the width if it changes regardless of height setting
					Ext.EventManager.onWindowResize(function() {
						var doc = Ext.getDoc(),
							size = doc.getViewSize();
						me.setWidth(size.width);
						me.doLayout();
						Ext.each(Ext.dom.Query.select('div[class=x-mask]'), function(el) {
							Ext.fly(el).setStyle('display', 'none');
						});
					});
					var size = Ext.getDoc().getViewSize();
					me.setWidth(size.width);
					me.doLayout();
				}
			}
		}, this, {
			single: true
		});

		this.on('reloadForm', function() {
			if (this.rendered) this.loadForm()
		}, this, {
			buffer: 100
		})

		Ext.defer(function() {
			if (typeof(registerForm) == 'function') {
				registerForm(this);
			}
		}, 100, this);
	},

	loadForm: function() {
		if (this.formName || this.formId || this.recordId) {
			Ext.Ajax.request({
				url: this.API['getForm'],
				params: {
					view: this.formName,
					formsysid: this.formId,
					recordid: this.recordId,
					formdefinition: this.formdefinition
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) {
						if (response.data) {
							this.convertConfig(response.data)
							this.getWSData()
							this.getATProperty()
							// store the form's tablename for use in SIR custom data form
							clientVM.forms = {};
							clientVM.forms[response.data.viewName] = response.data.tableName;
						} else {
							clientVM.displayError(RS.formbuilder.locale.formNotFound)
							this.removeAll()
						}
						return;
					}
					clientVM.displayError(response.message);
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			});
		} else this.processUrlVariables()
	},

	/**
	 * Converts the configuration from the server to real controls to be rendered on the page
	 * @param {Object} config
	 */
	convertConfig: function(config) {
		config.displayName = Ext.String.htmlEncode(config.displayName) || '';
		this.viewmodelSpec = {
			mtype: 'viewmodel',
			ns: 'RS.formbuilder'
		};
		var viewSpec = {},
			configuration;
		var buttonPanel = config.buttonPanel ? config.buttonPanel.controls : [] || [];

		this.submitConfiguration = Ext.apply({}, {
			viewName: config.viewName,
			formSysId: config.id,
			tableName: config.tableName,
			userId: ''
		}, this.submitConfiguration);

		this.userName = config.username;

		if (config.windowTitle) {
			var variables = config.windowTitle.match(/\${[\w]+}/g),
				functionString = Ext.String.format('\nvar title="{0}";\n', Ext.String.htmlEncode(config.windowTitle));
			Ext.each(variables, function(variable, index) {
				functionString += Ext.String.format('var s{0} = this.{1};\ntitle = title.replace(/\\{2}/g, s{0});\n', index, variable.substring(2, variable.length - 1), variable);
			});

			//actually set the window title based on the new title
			functionString += 'if( this[\'formScope\'] && this[\'formScope\'].fireEvent(\'titleChanged\', title) !== false){clientVM.setWindowTitle(title);}return title;';
			this.viewmodelSpec.windowTitle$ = new Function(functionString);
		}

		//look in temp for the real controls to display
		config.panels = config.panels || [];
		if (config.panels.length == 0) config.panels.push({
			tabs: [{
				columns: [{
					fields: []
				}]
			}]
		});
		if (config.panels[0].tabs.length > 1) {
			this.viewmodelSpec.activeTabIndex = 0;
			this.viewmodelSpec.tabCount = config.panels[0].tabs.length;
			var tabItems = [],
				tabs = config.panels[0].tabs;
			Ext.each(tabs, function(tab) {
				var controls = tab.columns[0].fields;
				Ext.each(controls, this.convertControl, this);
				tabItems.push({
					layout: 'anchor',
					bodyPadding: '10px',
					title: tab.name,
					name: tab.name.replace(/[^\w]/g, ''),
					defaults: {
						labelWidth: 125
					},
					items: controls,
					autoScroll: true
				});
				tab.name = tab.name.replace(/[^\w]/g, '');
				this.parseDependencies(tab);
			}, this);

			var temp = {
				xtype: 'tabpanel',
				plain: true,
				activeTab: '@{activeTabIndex}',
				items: tabItems
			};

			if (config.wizard) {
				Ext.apply(temp, {
					tabBar: {
						cls: 'wizard-steps'
					}
				});
			}

			configuration = [temp];
		} else {
			if (config.panels[0].tabs[0].columns.length > 0)
				configuration = config.panels[0].tabs[0].columns[0].fields;
			
			if (configuration) {
				for (var i=0; i<configuration.length; i++) {
					if (configuration[i].uiType === 'UrlContainerField' && configuration[i].referenceTableUrl.indexOf('/resolve/') === 0) {
						const urlData = configuration[i].referenceTableUrl.split('#');
						const uriData = urlData[0].split('?');
						clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
							var csrftoken = '?' + data[0] + '=' + data[1];
							var tokenizedData = uri + csrftoken;
							if (uriData.length > 1) {
								tokenizedData += '&' + uriData[1];
							}
							if (urlData.length > 1) {
								tokenizedData += '#' + urlData[1];
							}
							configuration[i].referenceTableUrl = tokenizedData;
						});
					}
				}
			}

			//convert all the controls to appropriate extjs controls
			Ext.Array.forEach(configuration || [], this.convertControl, this);
		}

		this.fieldConfiguration = config.panels[0].tabs;

		//convert the buttons
		if (!buttonPanel) buttonPanel = [];
		Ext.Array.forEach(buttonPanel, this.convertButton, this);

		var parentWindow = this.up('window')
		if (parentWindow && config.displayName) {
			parentWindow.setTitle(config.displayName)
			config.displayName = ''
		}

		Ext.apply(viewSpec, {
			xtype: 'form',
			border: false,
			autoScroll: true,
			bodyPadding: '10px',
			htmlEncodeTitle : true,
			title: this.embed ? '' : config.displayName,
			buttonAlign: 'left',
			cls: 'rs-custom-button',
			items: Ext.Array.from(configuration),
			defaults: {
				labelWidth: 125
			}
		});

		if (this.embed) Ext.apply(viewSpec, {
			tbar: {
				xtype: 'toolbar',
				cls: 'actionBar actionBar-form',
				items: buttonPanel
			}
		})
		else Ext.apply(viewSpec, {
			buttons: buttonPanel
		})

		//Showing a tab panel instead of just a form, so we need to correct some normal form styling
		if (config.panels[0].tabs.length > 1) {
			viewSpec.layout = 'fit';
			viewSpec.autoScroll = false;
			// delete viewSpec.bodyPadding;
		}

		//load up the model and the view
		this.viewmodelSpec.formScope = '';
		this.vm = glu.model(this.viewmodelSpec);
		this.vm.formScope = this;
		this.viewSpec = viewSpec;
		if (this.vm.windowTitle$) this.vm.windowTitle$();
		var view = glu.viewFromSpec(this.vm, viewSpec);
		if (this.hideToolbar) {
			view.dockedItems.each(function(item) {
				if (item.isXType('toolbar')) item.hide()
			})
		}
		if (this.items) {
			this.removeAll()
			this.add(view);
		} else this.items = [view]

		if (this.recordId || this.queryString) this.setRecordId(this.recordId)
		else this.processUrlVariables();

		this.fireEvent('formLoaded', this, this)
	},

	processDataRequest: function(r, button) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			if (response.data) {
				var data = null,
					tempDate = null;
				if (Ext.isArray(response.data)) {
					var vals = {};
					Ext.each(response.data, function(val) {
						vals[val.fieldName] = val.defaultValue;
					})
					response.data = vals;
				}
				for (var key in response.data) {
					data = response.data[key];
					if (this.vm[key + 'IsDate']) {
						tempDate = Ext.Date.parse(data, 'c');
						if (Ext.isDate(tempDate)) data = tempDate;
						//Parse out the option values
						if (data && Ext.isString(data) && data.indexOf(this.choiceSeparator) > -1) {
							data = this.parseValue(data);
						}
					}
					if (data && Ext.isString(data) && data.indexOf(this.choiceSeparator) > -1) {
						data = this.parseValue(data);
					}
					if (Ext.isDefined(this.vm[key])) {
						if (Ext.isDate(this.vm[key]) && !data) data = new Date();
						this.vm.set(key, data);
					}
				}
				//Special processing for radios because the bindings don't work with extjs's messed up structure, we have to manually set the values
				//checkbox group is fix with a manual event wiring..need to find a better way to deal with field group
				var components = this.query('radiogroup');
				Ext.each(components, function(component) {
					var name = component.name;
					if (!name) name = component.items.getAt(0).name;
					if (Ext.isDefined(response.data[name])) {
						var obj = {};
						obj[name] = response.data[name];
						component.setValue(obj);
					}
				});
				this.recordId = response.data.sys_id;
				//Also make sure to reload the reference grids if there are any
				var referenceGrids = this.query('referencetable');
				Ext.each(referenceGrids, function(referenceGrid) {
					referenceGrid.loadReferences();
				});
				//check if there is a reload target, condition, and value on the button.  If there is then we need to reload if that condition isn't met
				if (button && button.reloadCondition && button.reloadTarget && button.reloadValue) {
					var needsReload = false;
					if (Ext.isDefined(this.vm[button.reloadTarget])) {
						var val = this.vm[button.reloadTarget],
							value = button.reloadValue;
						needsReload = true;

						switch (button.reloadCondition) {
							case 'equals':
								if (val == value) needsReload = false;
								break;
							case 'notequals':
								if (val != value) needsReload = false;
								break;
							case 'greaterThan':
								if (val > value) needsReload = false;
								break;
							case 'greaterThanOrEqual':
								if (val >= value) needsReload = false;
								break;
							case 'lessThan':
								if (val < value) needsReload = false;
								break;
							case 'lessThanOrEqual':
								if (val <= value) needsReload = false;
								break;
							case 'contains':
								if (val.indexOf(value) > -1) needsReload = false;
								break;
							case 'notcontains':
								if (val.indexOf(value) == -1) needsReload = false;
								break;
						}
					}

					if (needsReload && this.recordId) {
						if (response.data && !response.data.u_worksheet) {
							//button.enable();
							button.setIcon('');
							clientVM.displayError(RS.formbuilder.locale.worksheet_not_found);
						} else if (response.data.u_worksheet) {
							this.verifyWS(response.data.u_worksheet, function(){
								Ext.defer(this.setRecordId, button.reloadDelay * 1000, this, [this.recordId, button]);
							}.bind(this), function() {
								//button.enable();
								button.setIcon('');
							});
						} else {
							Ext.defer(this.setRecordId, button.reloadDelay * 1000, this, [this.recordId, button]);
						}
					} else {
						button.enable();
						button.setIcon('');
					}
				}
			}
		} else clientVM.displayError(response.message)
		this.processUrlVariables()
			//Ensure the window isn't scrolled by focusing on fields that aren't visible
		window.scrollTo(0, 0)
	},

	/**
	 * Converts an individual control provided by the server to the correct representation for extjs to display
	 * @param {Object} config
	 */
	convertControl: function(config) {
		config.sectionTitle =  Ext.String.htmlEncode(config.sectionTitle|| '');
		config.tooltip =  Ext.String.htmlEncode(config.tooltip|| '');
		config.fieldLabel = config.displayName;
		// config.value = config.value || config.defaultValue;
		config.allowBlank = !config.mandatory;
		config.labelSeparator = config.mandatory ? ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>' : ':';

		//configure viewmodel and dependencies
		this.viewmodelSpec[config.name] = config.value || config.defaultValue || '';

		this.parseDependencies(config);
		// if (config.mandatory) this.viewmodelSpec[config.name + 'IsValid$'] = new Function('if( !this.' + config.name + ')return false;');
		if (!config.width) delete config.width;
		if (!config.height) delete config.height;

		//if its the default height, the just let the controls be what they want to be
		if (config.height == 22 || config.height == 25) delete config.height;

		if (!config.width) {
			config.anchor = '99.5%';
		} else {
			config.width += 125;
		}
		delete config.id; //this could screw things up since the id's start with numbers
		delete config.value;
		if (!config.hidden) delete config.hidden;

		if (config.readOnly) config.fieldCls = 'x-form-field-readOnly';

		if (config.tooltip) {
			config.listeners = {
				render: function(field) {
					var target = field.getEl().dom.id;
					Ext.tip.QuickTipManager.register({
						target: field.getEl().dom.id,
						text: config.tooltip
					});
				}
			};
		}

		switch (config.uiType) {
			case 'TextField':
				config.xtype = 'textfield';
				config.minLength = config.stringMinLength;
				if (config.stringMaxLength > 0 && config.stringMaxLength != 4001) config.maxLength = config.stringMaxLength;
				break;

			case 'TextArea':
				config.xtype = 'textareafield';
				config.resizable = true;
				config.resizeHandles = 's';
				config.minLength = config.stringMinLength;
				if (config.stringMaxLength > 0 && config.stringMaxLength != 4001) config.maxLength = config.stringMaxLength;
				break;

			case 'NumberTextField':
				config.xtype = 'numberfield';
				config.allowDecimals = false;
				config.minValue = config.integerMinValue;
				if (config.integerMaxValue > 0) config.maxValue = config.integerMaxValue;
				break;

			case 'DecimalTextField':
				config.xtype = 'numberfield';
				config.minValue = config.decimalMinValue;
				if (config.decimalMaxVaue > 0) config.maxValue = config.decimalMaxValue;
				break;

			case 'ComboBox':
				config.xtype = 'clearablecombobox';
				config.displayField = 'name';
				config.valueField = 'value';
				config.editable = config.selectUserInput;
				config.forceSelection = !config.selectUserInput;
				config.store = '@{' + config.name + 'Store' + '}';
				this.viewmodelSpec[config.name] = this.parseValue(config.defaultValue).join('');
				//If the store is configured to be from advanced or custom, then we need to get the data once we have values in the fields
				if (config.choiceOptionSelection > 0) {
					this.viewmodelSpec[config.name] = '';
					config.disabled = '@{!' + config.name + 'IsLoadedAndEnabled}';
					config.pageSize = 50;
					var variableString = this.getVariableString(config);
					this.viewmodelSpec[config.name + 'IsLoadedAndEnabled$'] = function() {
						return true;
					}
					if (variableString) {
						this.viewmodelSpec[config.name + 'loadStoreOn$'] = new Function(Ext.String.format('this.set("{1}".substring(0,"{1}".length-5), "");if( {0} ){var ps = this.{1}.proxy.extraParamsOrig || this.{1}.proxy.extraParams,vars=ps.sqlQuery.match(/\\$\{(.*?)\\}/g);this.{1}.proxy.extraParamsOrig=Ext.clone(ps);Ext.each(vars, function(variable, index) {ps.sqlQuery = ps.sqlQuery.replace(variable, this[variable.substring(2,variable.length-1)])},this);this.{1}.load({params: ps});}', variableString, config.name + 'Store'));
						this.viewmodelSpec[config.name + 'IsLoadedAndEnabled$'] = new Function(Ext.String.format('return {0}', variableString));
					}
					config.queryMode = 'local';
					this.viewmodelSpec[config.name + 'Store'] = {
						mtype: 'store',
						autoLoad: variableString ? false : true,
						fields: ['name', 'value'],
						proxy: {
							type: 'ajax',
							url: this.API['getRecordData'],
							extraParams: {
								useSql: true,
								type: 'table',
								sqlQuery: config.choiceOptionSelection == 2 ? config.choiceSql : Ext.String.format('select {0} as name, {1} as value from {2} {3}', config.choiceDisplayField, config.choiceValueField, config.choiceTable, variableString ? Ext.String.format('where {0} = \'{1}\'', config.choiceWhere, '${' + config.choiceField + '}') : '')
							},
							reader: {
								type: 'json',
								root: 'records'
							},
							listeners: {
								exception: function(e, resp, op) {
									clientVM.displayExceptionError(e, resp, op);
								}
							}
						}
					};
				} else {
					this.viewmodelSpec[config.name + 'Store'] = {
						mtype: 'store',
						data: this.parseOptions(config.choiceValues),
						fields: ['name', 'value']
					}
				}
				break;

			case 'MultiSelectComboBoxField':
				config.xtype = 'combobox';
				config.displayField = 'name';
				config.valueField = 'value';
				config.editable = config.selectUserInput;
				config.multiSelect = true;
				config.delimiter = '; ';
				config.forceSelection = !config.selectUserInput;
				this.viewmodelSpec[config.name] = this.parseValue(config.defaultValue);
				config.store = Ext.create('Ext.data.Store', {
					data: this.parseOptions(config.choiceValues),
					fields: [{
						name: 'name'
					}, {
						name: 'value'
					}]
				});
				break;

			case 'MultipleCheckBox':
				config.xtype = 'checkboxgroup';
				config.columns = config.widgetColumns || 0;
				config.items = this.parseOptions(config.choiceValues, config.defaultValue, 'checkbox');
				config.listeners = Ext.applyIf(config.listeners || {}, {
					scope: this,
					change: function(checkboxgroup, newValue, oldValue, eOpts) {
						this.vm.set(checkboxgroup.configName, newValue[checkboxgroup.configName] || oldValue);
					}
				});
				config.hidden = '@{!' + config.name + 'IsVisible}';
				config.currentValue = '@{' + config.name + 'Compute}'
				config.setCurrentValue = function(value) {
					this.items.each(function(item) {
						Ext.each(value, function(val) {
							if (val == item.boxLabel)
								item.setValue(true);
						});
					});
				}
				if (!this.viewmodelSpec[config.name + 'IsVisible$']) this.viewmodelSpec[config.name + 'IsVisible$'] = function() {
					return true
				}
				this.viewmodelSpec[config.name] = this.parseValue(config.defaultValue);
				this.viewmodelSpec[config.name + 'Compute$'] = new Function(Ext.String.format('return this.{0}', config.name));
				Ext.Array.forEach(config.items, function(item) {
					if (Ext.isString(item.value)) item.value = false;
					Ext.apply(item, {
						name: config.name,
						readOnly: config.readOnly
					});
				});
				delete config.value;
				config.configName = config.name;
				delete config.name;
				break;

			case 'RadioButton':
				config.xtype = 'radiogroup';
				config.columns = config.widgetColumns || 0;
				config.items = this.parseOptions(config.choiceValues, config.defaultValue, 'radio');
				this.viewmodelSpec[config.name] = this.parseValue(config.defaultValue).join('');
				Ext.Array.forEach(config.items, function(item) {
					Ext.apply(item, {
						name: config.name,
						readOnly: config.readOnly
					});
				});
				delete config.value;
				config.configName = config.name;
				delete config.name;
				break;

			case 'Date':
				config.xtype = 'xdatefield';
				this.viewmodelSpec[config.name] = config.value || config.defaultValue || new Date()
				this.viewmodelSpec[config.name + 'IsDate'] = true
				if (!Ext.isDate(this.viewmodelSpec[config.name])) this.viewmodelSpec[config.name] = Ext.Date.parse(this.viewmodelSpec[config.name], 'c')
				break;

			case 'Timestamp':
				config.xtype = 'xtimefield';
				this.viewmodelSpec[config.name] = config.value || config.defaultValue || new Date()
				this.viewmodelSpec[config.name + 'IsDate'] = true
				if (!Ext.isDate(this.viewmodelSpec[config.name])) this.viewmodelSpec[config.name] = Ext.Date.parse(this.viewmodelSpec[config.name], 'c')
				config.store = '';
				break;

			case 'DateTime':
				config.xtype = 'xdatetimefield';
				config.hidden = '@{!' + config.name + 'IsVisible}';
				config.disabled = '@{!' + config.name + 'IsEnabled}';
				if (!this.viewmodelSpec[config.name + 'IsVisible$']) this.viewmodelSpec[config.name + 'IsVisible$'] = function() {
					return true
				}
				if (!this.viewmodelSpec[config.name + 'IsEnabled$']) this.viewmodelSpec[config.name + 'IsEnabled$'] = function() {
					return true
				}
				this.viewmodelSpec[config.name] = config.value || config.defaultValue || new Date()
				this.viewmodelSpec[config.name + 'IsDate'] = true
				if (!Ext.isDate(this.viewmodelSpec[config.name])) this.viewmodelSpec[config.name] = Ext.Date.parse(this.viewmodelSpec[config.name], 'c')
				config.items = [{
					xtype: 'hidden',
					name: config.name,
					listeners: {
						render: function(field) {
							if (Ext.isBoolean(field.disabled)) field.setDisabled(field.disabled);
							if (Ext.isBoolean(field.visible)) field.setVisible(field.visible);
						}
					},
					setVisible: function(visible) {
						this.ownerCt.setVisible(visible);
					},
					setDisabled: function(disabled) {
						this.ownerCt.setDisabled(disabled);
					}
				}];
				break;

			case 'Link':
				config.xtype = 'linkfield';
				config.listeners = Ext.applyIf(config.listeners || {}, {
					afterrender: function(field) {
						if (Ext.isBoolean(field.disabled)) field.setDisabled(field.disabled);
					}
				});
				break;

			case 'CheckBox':
				config.xtype = 'checkbox';
				this.viewmodelSpec[config.name] = config.value || (config.defaultValue === true || config.defaultValue === 'true' ? true : false);
				config.inputValue = 'true';
				delete config.fieldCls;
				break;

			case 'Password':
				config.xtype = 'textfield';
				config.inputType = 'password';
				break;

			case 'TagField':
				config.xtype = 'tagfield';
				break;

			case 'Sequence':
				config.xtype = 'displayfield';
				break;

			case 'List':
				config.xtype = 'listfield';
				config.parseOptions = this.parseOptions;
				config.parseValue = this.parseValue;
				config.items = [{
					xtype: 'hidden',
					name: config.name,
					listeners: {
						render: function(field) {
							if (Ext.isBoolean(field.disabled)) field.setDisabled(field.disabled);
							if (Ext.isBoolean(field.visible)) field.setVisible(field.visible);
						}
					},
					setVisible: function(visible) {
						this.ownerCt.setVisible(visible);
					},
					setDisabled: function(disabled) {
						this.ownerCt.setDisabled(disabled);
					}
				}];
				this.viewmodelSpec[config.name] = this.parseValue(config.defaultValue);
				break;

			case 'ButtonField':
				//deal with actions
				config.xtype = 'button';
				delete config.anchor;
				config.text = config.displayName;
				config.handler = this.handleButtonClick;
				config.scope = this;
				break;

			case 'Reference':
				config.xtype = 'referencefield';
				break;

			case 'Journal':
				config.xtype = 'journalfield';
				config.userId = this.userName;
				config.items = [{
					xtype: 'hiddenfield',
					name: config.name,
					listeners: {
						render: function(field) {
							if (Ext.isBoolean(field.disabled)) field.setDisabled(field.disabled);
							if (Ext.isBoolean(field.visible)) field.setVisible(field.visible);
						}
					},
					getValue: function() {
						var temp = this.ownerCt.journalEntries.slice(0),  // me.ownerCt.journalEntries is immutable
							newVal = this.ownerCt.down('textarea').getValue();
						if (newVal) {
							temp.splice(0, 0, {
								userId: this.ownerCt.userId,
								createDate: Ext.Date.format(new Date(), 'c'),
								note: Ext.htmlEncode(newVal).replace(/\n/g, '<br/>')
							});
						}
						return Ext.encode(temp);
					},
					setVisible: function(visible) {
						this.ownerCt.setVisible(visible);
					},
					setDisabled: function(disabled) {
						this.ownerCt.setDisabled(disabled);
					}
				}];
				break;

			case 'ReadOnlyTextField':
				config.xtype = 'displayfield';
				break;

			case 'UserPickerField':
				config.xtype = 'userpickerfield';
				config.meValue = this.userName;
				config.store = '';
				break;

			case 'TeamPickerField':
				config.xtype = 'teampickerfield';
				config.store = '';
				break;

			case 'EmailField':
				config.xtype = 'textfield';
				config.regex = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
				config.invalidText = 'The value in this field is invalid. It must be an email address like: someone@example.com';
				break;

			case 'MACAddressField':
				config.xtype = 'textfield';
				config.regex = /(?=^[0-9a-f]{2}([\.:-])(?:[0-9a-f]{2}\1){4}[0-9a-f]{2}$|^[0-9a-f]{2}([\.:-])(?:[0-9a-f]{2}\2){6}[0-9a-f]{2}$|^[0-9a-f]{3}([\.:-])(?:[0-9a-f]{3}\3){2}[0-9a-f]{3}$|^[0-9a-f]{4}([\.:-])(?:[0-9a-f]{4}\4){2}[0-9a-f]{4}$)/i;
				config.invalidText = 'The value in this field is invalid. It must follow standard 48 bits or 64 bits MAC Address Format';
				break;

			case 'IPAddressField':
				config.xtype = 'textfield';
				config.regex = /^(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))$/;
				config.invalidText = 'The value in this field is invalid. It must be an IP Address like: 192.168.0.1';
				break;

			case 'CIDRAddressField':
				config.xtype = 'textfield';
				config.regex = /^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2])){1}$/;
				config.invalidText = 'The value in this field is invalid. It must be an CIDR Address like: 192.168.0.1/14';
				break;

			case 'PhoneField':
				config.xtype = 'textfield';
				config.plugins = [new RS.formbuilder.viewer.desktop.InputTextMask('(999) 999 - 9999', undefined, 1)];
				break;

			case 'NameField':
				config.xtype = 'namefield';
				config.choiceSeparator = this.choiceSeparator;
				config.items = [{
					xtype: 'hiddenfield',
					name: config.name,
					listeners: {
						render: function(field) {
							if (Ext.isBoolean(field.disabled)) field.setDisabled(field.disabled);
							if (Ext.isBoolean(field.visible)) field.setVisible(field.visible);
						},
						change: function(panel, newValue, oldValue) {
							var choiceSeparator = this.up().choiceSeparator;
							var values = newValue.split(choiceSeparator);
							var idx = 0;
							var items = panel.ownerCt.items.items;
							for (var i = 0; i < items.length; i++) {
								if ((items[i].xtype === 'textfield' || items[i].xtype === 'combobox') && Ext.isFunction(items[i].setValue)) {
									items[i].setRawValue(values[idx++]);
								}
							}
						}
					},
					setVisible: function(visible) {
						this.ownerCt.setVisible(visible);
					},
					setDisabled: function(disabled) {
						this.ownerCt.setDisabled(disabled);
					}
				}];
				break;

			case 'AddressField':
				config.xtype = 'addressfield';
				config.choiceSeparator = this.choiceSeparator;
				config.items = [{
					xtype: 'hiddenfield',
					name: config.name,
					listeners: {
						render: function(field) {
							if (Ext.isBoolean(field.disabled)) field.setDisabled(field.disabled);
							if (Ext.isBoolean(field.visible)) field.setVisible(field.visible);
						},
						change: function(panel, newValue, oldValue) {
							var choiceSeparator = this.up().choiceSeparator;
							var values = newValue.split(choiceSeparator);
							var idx = 0;
							var items = panel.ownerCt.items.items;
							for (var i = 0; i < items.length; i++) {
								if (items[i].xtype === 'textfield' && Ext.isFunction(items[i].setValue)) {
									items[i].setRawValue(values[idx++]);
								} else if (items[i].xtype === 'fieldcontainer') {
									var subItems = items[i].items.items;
									for (var j = 0; j < subItems.length; j++) {
										if ((subItems[j].xtype === 'textfield' || subItems[j].xtype === 'combobox') && Ext.isFunction(subItems[j].setValue)) {
											subItems[j].setRawValue(values[idx++]);
										}
									}
								}
							}
						}
					},
					setVisible: function(visible) {
						this.ownerCt.setVisible(visible);
					},
					setDisabled: function(disabled) {
						this.ownerCt.setDisabled(disabled);
					}
				}];
				break;

			case 'FileUploadField':
				this.viewmodelSpec[config.name] = [];
				config.xtype = 'uploadmanager';
				config.formName = this.formName;
				config.recordId = this.recordId;
				break;

			case 'ReferenceTableField':
				this.viewmodelSpec[config.name] = [];
				config.xtype = 'referencetable';
				config.recordId = this.recordId;
				break;

			case 'HiddenField':
				config.xtype = 'hiddenfield';
				config.value = config.hiddenValue;
				break;

			case 'SpacerField':
				delete this.viewmodelSpec[config.name];
				config.xtype = 'component';
				if (config.showHorizontalRule) {
					config.html = '<hr/>'
				}
				delete config.name;
				break;

			case 'LabelField':
				config.xtype = 'label'
				config.text = config.displayName
				break;

			case 'WysiwygField':
				config.xtype = 'htmleditor';
				config.defaultValue = config.defaultValue || (Ext.isOpera || Ext.isIE6) ? '&#160;' : '&#8203;'
				break;

			case 'TabContainerField':
				this.viewmodelSpec[config.name] = [];
				config.xtype = 'tabcontainerfield';
				config.formName = this.formName;
				config.recordId = this.recordId;
				break;

			case 'UrlContainerField':
				this.viewmodelSpec[config.name] = [];
				config.xtype = 'urlcontainerfield';
				break;

			case 'SectionContainerField':
				if (config.height == 25) delete config.height;
				config.xtype = config.sectionType;
				config.border = false;
				config.bodyPadding = '10px 0px 10px 0px';
				config.layout = {
					type: 'hbox',
					align: 'stretch'
				};
				config.defaults = {
					flex: 1,
					border: false
				};
				config.collapsible = true;
				config.title = config.sectionTitle;
				var cols = [],
					decoded = Ext.decode(config.sectionColumns);
				Ext.Array.forEach(decoded, function(column) {
					var ctrls = [];
					Ext.Array.forEach(column.controls, function(control) {
						if (control) {
							this.convertControl(control)
							ctrls.push(control);
						}
					}, this);
					cols.push({
						layout: 'anchor',
						items: ctrls
					})
				}, this)
				config.items = cols;
				break;

			default:
				config.xtype = 'textfield'

				break;
		}
	},

	getVariableString: function(config) {
		var variables = '';
		if (config.choiceOptionSelection == 1 && config.choiceField && config.choiceWhere) {
			variables = Ext.String.format('this.{0}', config.choiceField);
		} else {
			config.choiceSql = config.choiceSql || '';
			var vars = config.choiceSql.match(/\$\{(.*?)\}/g)
			Ext.Array.forEach(vars || [], function(variable, index) {
				if (index > 0) variables += ' && ';
				variables += 'this.' + variable.substring(2, variable.length - 1);
			});
		}

		return variables;
	},

	defaultFont: 'Verdana',
	defaultFontSize: '10',
	defaultFontColor: '333333',
	defaultBackgroundColor: 'DDDDDD',

	// Approximate fontsize conversion from pt to px
	fontSizePtMapping : {
		6 : 8,
		8 : 11,
		9 : 12,
		10 : 13,
		12 : 16,
		14 : 19,
		18 : 24,
		24 : 32,
		30 : 40,
		36 : 48,
		48 : 64,
		60 : 80
	},

	/**
	 * Converts a server configuration of a button into a button that can be rendered by extjs
	 * @param {Object} button
	 */
	convertButton: function(button) {
		button.text = button.displayName || "Fix Me!";
		button.handler = this.handleButtonClick;
		button.sysId = button.id;
		button.scope = this;

		if (Ext.firefoxVersion !== 0) {
			// RBA-12536: this is Ext JS 4 bug. We are putting a bandage to get around the issue
			button.cls = 'fixFirefoxIssue';
		}
		switch (button.type) {
			case 'separator':
				button.xtype = 'tbseparator';
				break;
			case 'spacer':
				button.xtype = 'tbspacer';
				break;
			case 'rightJustify':
				button.xtype = 'tbfill';
				break;
			case 'button':
				var fontFamily = button.font+', Tahoma' || this.defaultFont+', Tahoma',
					fontSize,
					color = '#' + (button.fontColor ? button.fontColor : this.defaultFontColor),
					backgroundColor = '#' + (button.backgroundColor ? button.backgroundColor : this.defaultBackgroundColor);

				if (button.fontSize && this.fontSizePtMapping[parseInt(button.fontSize)]) {
					fontSize = parseInt(button.fontSize);
				} else {
					fontSize = parseInt(this.defaultFontSize);
				}

				var style = {
					fontFamily: fontFamily,
					fontSize: this.fontSizePtMapping[fontSize] + 'px',
					color: color,
					backgroundColor: backgroundColor
				}
				Ext.apply(button, {
					style: style,
					height: this.fontSizePtMapping[fontSize] + 10
				});
			default:
				button.xtype = 'button';
				break;
		}

		//determine crud action for db type actions
		button.crudAction = this.getCrudAction(button);

		//make sure we have name for the glu dependencies
		button.name = 'button_' + button.id;
		//setup standard glu dependencies for next/back buttons
		//conflicts with glu bindings
		delete button.hidden;

		//delete id because it screws things up to have an id on extjs controls
		delete button.id;
		this.parseButtonDependencies(button);
	},
	getCrudAction: function(button) {
		var i, actions = button.actions || [],
			len = actions.length,
			action;
		for (i = 0; i < len; i++) {
			action = actions[i];
			if (action.operation == 'DB' || action.operation == 'SIR') return action.crudAction;
		}
	},
	/**
	 * Parses values out of a select or choice control
	 * @param {Object} configValue
	 */
	parseValue: function(configValue) {
		var returnValue = [],
			configValue = configValue || '';
		var values = Ext.isString(configValue) ? configValue.split(this.choiceSeparator) : configValue;
		Ext.each(values, function(value) {
			if (value) {
				var val = value.split(this.choiceValueSeparator);
				var nameVal = val[0].split('=');
				if (nameVal.length == 1) nameVal.push(nameVal[0]);
				returnValue.push(val[1] == 'true' ? nameVal[1] : val[0]);
			}
		}, this);
		return returnValue;
	},
	/**
	 * Parses options out of a select or choice control
	 * @param {Object} optionString
	 * @param {Object} valueString
	 */
	parseOptions: function(optionString, valueString, type) {
		var options = [],
			optionString = optionString || '',
			defaultOptions = {};
		if (type) defaultOptions.xtype = type;
		var split = optionString.split(this.choiceSeparator);
		Ext.each(split, function(option) {
			var nameValue = option.split(this.choiceValueSeparator);
			if (type != 'radio') defaultOptions.value = nameValue[1] && (nameValue[1] != 'true' || nameValue[1] != 'false') ? nameValue[1] : nameValue[0];
			if (nameValue[0]) {
				var nameValCombo = nameValue[0].split('=');
				if (nameValCombo.length == 1) nameValCombo.push(nameValCombo[0]);
				if (!type) {
					defaultOptions.name = nameValCombo[0];
					defaultOptions.value = nameValCombo[1];
				}
				options.push(Ext.apply({
					boxLabel: nameValCombo[0],
					inputValue: nameValCombo[1],
					xtype: type,
					hideLabel: true
				}, defaultOptions))
			}
			delete defaultOptions.value;
			delete defaultOptions.name;
		}, this);
		if (valueString) {
			var values = valueString.split(this.choiceSeparator),
				i, j;
			for (i = 0; i < values.length; i++) {
				var val = values[i].split(this.choiceValueSeparator);
				// if(val[1] == 'true') {
				for (j = 0; j < options.length; j++) {
					var nameVal = val[0].split('=');
					if (nameVal.length == 1) nameVal.push(nameVal[0]);
					if (options[j].boxLabel == nameVal[0]) options[j][type == 'checkbox' ? 'value' : 'checked'] = true;
					else options[j][type == 'checkbox' ? 'value' : 'checked'] = options[j][type == 'checkbox' ? 'value' : 'checked'] == true || false;
				}
				// }
			}
		}
		return options;
	},
	/**
	 * Handles the button clicks to actions relationship and will perform the appropriate posting of the values to be processed by the server.
	 * Deals with reset and close actions client side if everything goes well on the server side.
	 * @param {Object} button
	 */
	handleButtonClick: function(button) {
		//If we're in the middle of uploading, then message the user that they can't perform actions until the upload is done
		var uploadForm = button.up('form'),
			uploader = uploadForm.down('uploadmanager');
		if (uploader && uploader.isUploading()) {
			Ext.Msg.alert(RS.formbuilder.locale.uploadInProgressTitle, RS.formbuilder.locale.uploadInProgressBody);
			return;
		}
		//Handle actions
		var shouldReset = false,
			shouldReload = false,
			reloadDelay = 0,
			reloadTarget = '',
			reloadCondition = '',
			reloadValue = '',
			shouldClose = false,
			shouldExit = false,
			shouldReturn = false,
			submitToServer = false,
			automationExecuted = false,
			actionParams = {},
			displayMessage = false,
			messageBoxTitle = '',
			messageBoxMessage = '',
			messageBoxYesText = '',
			messageBoxNoText = '';
		Ext.each(button.actions, function(action) {
			switch (action.operation) {
				case 'RESET':
					shouldReset = true;
					break;
				case 'CLOSE':
					shouldClose = true;
					break;
				case 'EXIT':
					shouldExit = true;
					break;
				case 'RETURN':
					shouldReturn = true
					break;
				case 'MESSAGEBOX':
					displayMessage = true;
					if (action.additionalParam && Ext.isString(action.additionalParam)) {
						var decode = this.deserializeParams(action.additionalParam);
						messageBoxTitle = Ext.String.htmlEncode(decode.messageBoxTitle);
						messageBoxMessage = decode.messageBoxMessage;
						messageBoxYesText = decode.messageBoxYesText;
						messageBoxNoText = decode.messageBoxNoText;
					}
					break;
				case 'RELOAD':
					shouldReload = true;
					if (action.additionalParam && Ext.isString(action.additionalParam)) {
						var decode = this.deserializeParams(action.additionalParam);
						reloadDelay = decode.reloadDelay;
						reloadTarget = decode.reloadTarget;
						reloadCondition = decode.reloadCondition;
						reloadValue = decode.reloadValue;
					}
					break;
				case 'DB':
					if (action.additionalParam && Ext.isString(action.additionalParam)) {
						var decode = this.deserializeParams(action.additionalParam);
						if (decode.sirAction && decode.sirAction == 'saveCaseData') {
							action.operation = 'SIR';
							button.crudAction = 'saveCaseData';
							action.additionalParam = '';
						}
					}
				case 'SIR':
				case 'EVENT':
				case 'RUNBOOK':
				case 'ACTIONTASK':
				case 'SCRIPT':
				case 'REDIRECT':
				case 'MAPPING':
					automationExecuted = true;
					submitToServer = true;
					if (action.additionalParam && Ext.isString(action.additionalParam)) {
						var decode = this.deserializeParams(action.additionalParam);
						for (var key in decode) {
							if (!decode[key]) delete decode[key];
						}
						Ext.applyIf(actionParams, decode);
						if (actionParams.showConfirmation) {
							displayMessage = true;
							messageBoxTitle = RS.formbuilder.locale.confirmDeleteTitle;
							messageBoxMessage = RS.formbuilder.locale.confirmDeleteMessage;
							messageBoxYesText = RS.formbuilder.locale.confirmDeleteYesText;
							messageBoxNoText = RS.formbuilder.locale.confirmDeleteNoText;
						}
					}
					break;
				case 'NEXT':
					var tabPanel = this.down('tabpanel');
					if (tabPanel) {
						var nextIndex = this.vm.activeTabIndex + 1,
							nextTab = tabPanel.items.getAt(nextIndex);
						while (tabPanel.items.getAt(nextIndex) && (tabPanel.items.getAt(nextIndex).tab.hidden || tabPanel.items.getAt(nextIndex).tab.disabled)) {
							nextIndex += 1;
						}
						if (tabPanel.items.getAt(nextIndex)) this.vm.set('activeTabIndex', nextIndex);
					}
					break;
				case 'BACK':
					var tabPanel = this.down('tabpanel');
					if (tabPanel) {
						var previousIndex = this.vm.activeTabIndex - 1,
							previousTab = tabPanel.items.getAt(previousIndex);
						while (tabPanel.items.getAt(previousIndex) && (tabPanel.items.getAt(previousIndex).tab.hidden || tabPanel.items.getAt(previousIndex).tab.disabled)) {
							previousIndex -= 1;
						}
						if (tabPanel.items.getAt(previousIndex)) this.vm.set('activeTabIndex', previousIndex);
					}
					break;
				case 'GOTO':
					var tabPanel = this.down('tabpanel');
					if (tabPanel) {
						var decode = this.deserializeParams(action.additionalParam),
							index = -1;
						var gotoTabName = decode['gotoTabName']
						gotoTabName = gotoTabName.replace(/[^\w]/g, '')
						tabPanel.items.each(function(tab, idx) {
							if (tab.name === gotoTabName) index = idx
						}, this)
						if (index > -1)
							this.vm.set('activeTabIndex', index)
					}
					break;
				default:
					Ext.Msg.alert(RS.formbuilder.locale.unknownAction, Ext.String.format(RS.formbuilder.locale.unknownActionBody, action.operation));
					break;
			}
		}, this);

		if (shouldReturn) {
			clientVM.handleNavigationBack()
		}

		button.shouldSubmitToServer = submitToServer;
		button.shouldReset = shouldReset;
		button.shouldClose = shouldClose;
		button.shouldExit = shouldExit;
		button.actionParams = actionParams;
		button.shouldReload = shouldReload;
		button.reloadDelay = reloadDelay;
		button.reloadTarget = reloadTarget;
		button.reloadCondition = reloadCondition;
		button.reloadValue = reloadValue;
		button.automationExecuted = automationExecuted;

		if (displayMessage) {
			Ext.MessageBox.show({
				title: messageBoxTitle,
				msg: messageBoxMessage,
				buttons: Ext.MessageBox.YESNO,
				buttonText: {
					yes: messageBoxYesText,
					no: messageBoxNoText
				},
				scope: button,
				fn: this.handleMessage
			});
			return;
		}

		if (submitToServer) {
			this.submitToServer(button);
		} else {
			if (shouldReset) {
				button.up('form').getForm().reset();
				this.getDefaultData();
			}
			if (shouldExit && this.fireEvent('beforeexit') !== false) {
				var win = button.up('window');
				if (win)
					win.close()
				else
					clientVM.handleNavigationBackOrExit()
			}
			if (shouldClose && this.fireEvent('beforeclose') !== false) {
				var win = button.up('window') || getResolveRoot();
				win.close();
			}
			if (shouldReload) {
				if (this.recordId) Ext.defer(this.setRecordId, button.reloadDelay * 1000, this, [this.recordId, button]);
				else Ext.defer(this.getDefaultData, button.reloadDelay * 1000, this, []);
			}
			this.fireEvent('actioncompleted', this, button);
		}
	},
	handleMessage: function(button) {
		if (button === 'yes') {
			var form = this.up('form'),
				rsForm = this.up('rsform');
			if (this.shouldSubmitToServer) rsForm.submitToServer(this);
			else {
				if (this.shouldReset) {
					this.getDefaultData();
				}
				if (this.shouldExit && this.fireEvent('beforeexit') !== false) {
					var win = button.up('window');
					if (win)
						win.close()
					else
						clientVM.handleNavigationBackOrExit()
				}
				if (this.shouldClose && this.fireEvent('beforeclose') !== false) {
					var win = this.up('window') || getResolveRoot();
					win.close();
				}
				this.fireEvent('actioncompleted', this, button);
			}
		}
	},
	getDefaultData: function() {
		Ext.Ajax.request({
			url: this.API['getDefaultData'],
			params: {
				view: this.formName,
				formsysid: this.formId,
				recordid: this.recordId
			},
			scope: this,
			success: function(r) {
				this.processDataRequest(r)
				this.processUrlVariables()
				this.getWSData()
				this.getATProperty()
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		});
	},

	getATProperty: function() {
		var properties = {}
		var filters = []
		Ext.Object.each(this.vm, function(key, value) {
			if (Ext.isString(value) && /\$PROPERTY\{(.*)\}/gi.test(value)) {
				var k = /\$PROPERTY\{(.*)\}/gi.exec(value)[1]
				if (k) {
					properties[k] = key
					filters.push({
						field: 'uname',
						type: 'auto',
						condition: 'equals',
						value: k
					});
				}
			}
		}, this)
		if (filters.length == 0)
			return;
		Ext.Ajax.request({
			url: this.API['getATProperty'],
			scope: this,
			params: {
				limit: -1,
				operator: 'or',
				filter: Ext.encode(filters)
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					//clientVM.displayError(this.localize('getPropertyError', respData.message))
					clientVM.displayError(respData.message);
					return
				}
				Ext.each(respData.records, function(r) {
					if (properties[r.uname])
						this.vm.set(properties[r.uname], r.uvalue);
				}, this)
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})

	},

	getWSData: function() {
		//Now that we have all the default data from the form and the url params processed, we need to apply any WSDATA data
		Ext.Ajax.request({
			url: this.API['getWSData'],
			params: {
				problemId: clientVM.problemId
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					//Process through fields, looking for their default values to be set to something that is in the WSDATA
					//Something like ${test} means test: 'seomthing' must exist in the WSDATA
					Ext.Object.each(this.vm, function(key, value) {
						if (Ext.isString(value) && /\$WSDATA\{(.*)\}/gi.test(value)) {
							var k = /\$WSDATA\{(.*)\}/gi.exec(value)[1]
							if (k && Ext.isDefined(response.data[k])) {
								this.vm.set(key, response.data[k])
							}
							else
								//Default to blank value if couldnt find a match.
								this.vm.set(key, '');
						}
					}, this)
				} else
					clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	resetForm: function() {
		this.down('form').getForm().reset()
	},

	saveForm: function() {
		var saveButton = null;
		this.down('form').dockedItems.each(function(item) {
			if (item.isXType('toolbar')) {
				item.items.each(function(button) {
					if (button.actions && Ext.isArray(button.actions)) {
						Ext.Array.forEach(button.actions, function(action) {
							if (action.crudAction == 'SUBMIT') {
								saveButton = button
							}
						})
					}
					if (button) return false
				})
			}
		})

		if (!saveButton) {
			clientVM.displayError(RS.formbuilder.locale.noSaveButtonBody, 10000, RS.formbuilder.locale.noSaveButtonTitle);
		} else this.handleButtonClick(saveButton)
	},
	submitToServer: function(button, form) {
		if (!form) form = button.up('form')

		if (!form.getForm().isValid()) {
			clientVM.displayError(RS.formbuilder.locale.invalidFormBody, 10000, RS.formbuilder.locale.invalidFormTitle);			
			return;
		}

		if (!button) {
			button = {}
		} else {
			//Change button to show "working" state
			button.disable();
			button.setIcon('/resolve/images/loading.gif');
		}

		var params = form.getForm().getValues(),
			serverParams = this.submitConfiguration,
			key;

		//The server needs to know the id of the button that was clicked to know which actions to perform
		serverParams.controlItemSysId = button.sysId;
		serverParams.crudAction = button.crudAction;

		//Go get the displayField information too
		var displayFields = form.query('displayfield');
		Ext.each(displayFields, function(displayField) {
			params[displayField.getName()] = displayField.getValue();
		});

		//Because they are so different, do the same with journal fields
		var journalFields = form.query('journalfield');
		Ext.each(journalFields, function(journalField) {
			var hidden = journalField.down('hiddenfield');
			if (hidden) {
				var name = hidden.getName(),
					value = hidden.getValue();
				params[name] = value;
				if (Ext.isDefined(this.vm[name])) this.vm.set(name, value);
			}
		}, this);

		//Prepare the params for form submission
		var dbData = {},
			inputData = {},
			paramType, actionParams = button.actionParams;

		//Take the action params, and if the PROBLEMID or EVENT_REFERENCE is a field, then populate them with the field value instead
		if (actionParams && actionParams.PROBLEMID && actionParams.PROBLEMID == 'MAPFROM' && params[actionParams.mappingFrom]) {
			params.PROBLEMID = params[actionParams.mappingFrom];
			params.EVENT_REFERENCE = params[actionParams.mappingFrom];
		}

		for (key in params) {
			paramType = this.getParamType(key);
			if (paramType === 'DB') dbData[key] = Ext.isArray(params[key]) ? this.serializeChoice(params[key]) : params[key]
			else if (paramType === 'INPUT') inputData[key] = Ext.isArray(params[key]) ? this.serializeChoice(params[key]) : params[key];
		}

		// if button action is "saveCaseData" & customDataForm, use formRecord from customDataForm (if exist)
		if (button.crudAction === 'saveCaseData' && clientVM.customDataForm) {
			if (clientVM.customDataForm && clientVM.customDataForm.formNames && clientVM.customDataForm.formNames[serverParams.viewName]) {
				Ext.apply(dbData, {
					sys_id: clientVM.customDataForm.formNames[serverParams.viewName]
				});
			}
		} 
		//Make sure the sys_id is in there if it came from the server
		else if (this.recordId) Ext.apply(dbData, {
			sys_id: this.recordId
		});

		serverParams.dbRowData.data = dbData;
		serverParams.sourceRowData.data = inputData;

		if (clientVM.activeScreen.parentVM && clientVM.activeScreen.parentVM.sirContext) {
			var sirContextVM = clientVM.activeScreen.parentVM;
			if (sirContextVM.ticketInfo && sirContextVM.ticketInfo.problemId) {
				serverParams.sourceRowData.data.PROBLEMID = sirContextVM.ticketInfo.problemId;	// SIR problemId has highest precedence
				serverParams.sourceRowData.data.incidentId = sirContextVM.ticketInfo.id;
				serverParams.sourceRowData.data.phase = sirContextVM.activitiesTab.activitiesView.phase;
				serverParams.sourceRowData.data.activityName = sirContextVM.activitiesTab.activitiesView.activityName;
			}
			if (sirContextVM.activityId) {
				serverParams.sourceRowData.data.activityId = sirContextVM.activityId;
			}
		} else if (clientVM.SIR_SELECTED_ACTIVITY) {
				serverParams.sourceRowData.data.PROBLEMID = clientVM.SIR_PROBLEMID;	// SIR problemId has highest precedence
				serverParams.sourceRowData.data.incidentId = clientVM.SIR_SELECTED_ACTIVITY.incidentId;
				serverParams.sourceRowData.data.phase = clientVM.SIR_SELECTED_ACTIVITY.phase;
				serverParams.sourceRowData.data.activityName = clientVM.SIR_SELECTED_ACTIVITY.activityName;		
				serverParams.sourceRowData.data.activityId = clientVM.SIR_SELECTED_ACTIVITY.id;			
		} else {
			actionParams.PROBLEMID = clientVM.SIR_PROBLEMID || this.problemId || actionParams.PROBLEMID;  // URL problemId has higher precedence
		}

		//Apply any additional parameters from the action that is being performed
		Ext.applyIf(serverParams.sourceRowData.data, actionParams);
		Ext.apply(serverParams.sourceRowData.data, {
			'RESOLVE.ORG_NAME' : clientVM.orgName,
			'RESOLVE.ORG_ID' : clientVM.orgId
		})
		//Apply the current runbook that the form is on
		serverParams.currentDocumentName = window['wikiDocumentName'] || ''

		Ext.Ajax.request({
			url: this.API['submit'],
			jsonData: serverParams,
			scope: this,
			success: function(r) {
				if (!button.shouldReload && button.enable) {
					button.enable();
					button.setIcon('');
				}
				var response = RS.common.parsePayload(r);
				if (response.success) {

					//update problemId and problemNumber on the client if possible
					if (clientVM.SIR_PROBLEMID) {
						(clientVM || getResolveRoot().clientVM).fireEvent('refreshResult');
						// if button action is "saveCaseData" and we have customDataForm, then also submit the data to Custom Form Data
						if (button.crudAction === 'saveCaseData' && clientVM.SIR_INCIDENTID && clientVM.customDataForm) {
							this.submitCustomDataForm(response.data);
						}
					}
					else if (button.actionParams.PROBLEMID == 'ACTIVE') {
						Ext.defer(function() {
							(clientVM || getResolveRoot().clientVM).fireEvent('problemIdChanged');
						}, 500)
					} else if (button.actionParams.PROBLEMID == 'NEW' && response.data.PROBLEMID && getResolveRoot().clientVM) {
						clientVM.updateProblemInfo(response.data.PROBLEMID);
					} else if (button.automationExecuted && (clientVM || getResolveRoot().clientVM)) {
						Ext.defer(function() {
							(clientVM || getResolveRoot().clientVM).fireEvent('worksheetExecuted');

							// trigger refresh result since worksheetExecuted is currently not doing anything
							(clientVM || getResolveRoot().clientVM).fireEvent('refreshResult');
						}, 500)
					}

					if (button.crudAction === 'saveCaseData' && clientVM.customDataForm) {
						// do nothing for custom data form
					} else {
						//Populate sys_id if it comes back from the server so that any further actions are updates until reloaded
						if (response.data.DB_SYS_ID) this.recordId = response.data.DB_SYS_ID;
					}


					//TODO: errorException

					//Redirect the user if they specified a redirect action
					if (response.data.REDIRECT) {
						var redirect = response.data.REDIRECT,
							executeRedirect = true;
						if (response.data.REDIRECT_TARGET == '_self' && this.fireEvent('beforeredirect', response.data.REDIRECT) === false) {
							executeRedirect = false
						}

						if (executeRedirect) {
							if (redirect.indexOf('#') == 0 || redirect.indexOf('RS.') == 0) {
								var splits = redirect.indexOf('#') == 0 ? redirect.substring(1).split('/') : redirect.split('/')
								clientVM.handleNavigation({
									modelName: splits[0],
									target: response.data.REDIRECT_TARGET,
									params: Ext.Object.fromQueryString(splits[1])
								})
							} else if (redirect.indexOf('/') == 0) {
								clientVM.handleNavigation({
									modelName: 'RS.client.URLScreen',
									target: response.data.REDIRECT_TARGET,
									params: {
										location: redirect
									}
								})
							} else {
								window.open(response.data.REDIRECT.replace(/${sys_id}/g, this.recordId), response.data.REDIRECT_TARGET)
							}
						}
					}

					if (button.shouldReset) {
						this.down('form').getForm().reset();
						this.recordId = '';
						this.getDefaultData();
					}
					//display success message
					clientVM.displaySuccess(RS.formbuilder.locale.formSavedSuccessfully, null, RS.formbuilder.locale.success);
					if (button.shouldExit && this.fireEvent('beforeexit') !== false) {
						var win = button.up('window');
						if (win)
							Ext.defer(function() {
								win.close();
							}, 3000);
						else
							Ext.defer(function() {
								clientVM.handleNavigationBackOrExit()
							}, 3000);
					}

					if (button.shouldClose && this.fireEvent('beforeclose') !== false) {
						var win = this.up('window') || getResolveRoot();
						Ext.defer(function() {
							win.close();
						}, 3000);
					}

					var managers = this.query('uploadmanager');
					Ext.each(managers, function(manager) {
						manager.recordId = this.recordId;
						manager.setEnabled(this.recordId);
					}, this);

					if (button.shouldReload && this.recordId) {
						Ext.defer(this.setRecordId, button.reloadDelay * 1000, this, [this.recordId, button]);
					}
					this.fireEvent('actioncompleted', this, button);
				} else {
					//display error message
					clientVM.displayError(response.message);
				}
			},
			failure: function(r) {
				if (button.enable) {
					button.enable();
					button.setIcon('');
				}
				clientVM.displayFailure(r);
			}
		});
	},
	submitCustomDataForm: function(data, onSuccess, onFailure) {
		if (clientVM.SIR_INCIDENTID && clientVM.customDataForm) {
			if (!clientVM.customDataForm.formNames) {
				clientVM.customDataForm.formNames = {};
			}
			if (!clientVM.customDataForm.formNames[this.formName]) {
				clientVM.customDataForm.formNames[this.formName] = data.DB_SYS_ID;
			}
			var formRecord = {};
			formRecord[this.formName] = data.DB_SYS_ID;
			Ext.Ajax.request({
				url: '/resolve/service/playbook/customdata',
				jsonData: {
					incidentId: clientVM.SIR_INCIDENTID,
					wikiId: clientVM.customDataForm.baseWikiId,
					formRecord: formRecord,
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) {
						if (Ext.isFunction(onSuccess)) {
							onSuccess();
						}
					} else {
						if (Ext.isFunction(onFailure)) {
							onFailure();
						}
						clientVM.displayError(response.message);
					}
				},
				failure: function(resp) {
					if (Ext.isFunction(onFailure)) {
						onFailure();
					}
					clientVM.displayFailure(resp);
				}
			});
		}
	},
	verifyWS: function(worksheetId, onSuccess, onFailure) {
		Ext.Ajax.request({
			url: '/resolve/service/worksheet/getWS',
			params: {
				id: worksheetId
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					if (Ext.isFunction(onSuccess)) {
						onSuccess();
					}
				} else {
					if (Ext.isFunction(onFailure)) {
						onFailure();
					}
					clientVM.displayError(response.message);
				}
			},
			failure: function(resp) {
				if (Ext.isFunction(onFailure)) {
					onFailure();
				}
				clientVM.displayFailure(resp);
			}
		});
	},
	deserializeParams: function(paramString) {
		var params = {},
			paramsSplit = paramString.split(this.choiceSeparator);

		Ext.each(paramsSplit, function(split) {
			var val = split.split(this.choiceValueSeparator);
			if (val.length > 1) {
				params[val[0]] = val[1];
			}
		}, this);

		return params;
	},
	serializeChoice: function(paramsArray) {
		var params = [];
		Ext.each(paramsArray, function(param) {
			if (param) params.push(param);
		}, this);

		return params.join(this.choiceSeparator);
	},
	getParamType: function(param) {
		var i, j, tab, column, f, field, returnType;
		for (i = 0; i < this.fieldConfiguration.length; i++) {
			tab = this.fieldConfiguration[i]
			for (j = 0; j < tab.columns.length; j++) {
				column = tab.columns[j]
				if (column) {
					for (f = 0; f < column.fields.length; f++) {
						field = column.fields[f]
						returnType = this.getParamTypeOfField(field, param)
						if (returnType) return returnType
					}
				}
			}
		}

		if (param == 'EVENT_REFERENCE' || param == 'PROBLEMID') return 'INPUT';

		//unknown field so we need to return blank here
		return '';
	},

	getParamTypeOfField: function(field, param) {
		if (field.name == param || field.configName == param) return field.sourceType
		if (field.uiType == 'SectionContainerField') {
			var sc, sectionColumn, sf, sectionField, returnType
			sectionColumns = Ext.decode(field.sectionColumns) || [];
			for (sc = 0; sc < sectionColumns.length; sc++) {
				sectionColumn = sectionColumns[sc]
				for (sf = 0; sf < sectionColumn.controls.length; sf++) {
					sectionField = sectionColumn.controls[sf]
					if (sectionField) {
						returnType = this.getParamTypeOfField(sectionField, param)
						if (returnType) return returnType
					}
				}
			}
		}
		return null
	},

	setRecordId: function(recordId, button) {
		this.recordId = recordId;

		var queryParams = this.queryString ? Ext.Object.fromQueryString(this.queryString) : {};
		if (this.recordId) Ext.apply(queryParams, {
			sys_id: this.recordId
		});

		var filter = [];
		for (var key in queryParams) {
			var field = key;
			if (key == 'u_content_request' && this.submitConfiguration.tableName != 'content_request') {
				field += '.sys_id';
			}

			filter.push({
				field: field,
				type: 'auto',
				condition: 'equals',
				value: queryParams[key]
			});
		}

		if (this.showLoading)
			Ext.MessageBox.show({
				msg: RS.formbuilder.locale.loadingMessage,
				progressText: RS.formbuilder.locale.loadingProgress,
				width: 300,
				wait: true,
				waitConfig: {
					interval: 200
				}
			});

		if (filter.length && this.submitConfiguration.tableName) {
			Ext.Ajax.request({
				url: this.API['getRecordData'],
				params: {
					tableName: this.submitConfiguration.tableName,
					type: 'form',
					start: 0,
					filter: Ext.encode(filter)
				},
				scope: this,
				success: function(r) {
					if (this.showLoading) Ext.MessageBox.hide()
					Ext.each(Ext.dom.Query.select('div[class=x-mask]'), function(el) {
						Ext.fly(el).setStyle('display', 'none');
					});
					this.doLayout();

					Ext.defer(function() {
						Ext.each(Ext.dom.Query.select('div[class=x-mask]'), function(el) {
							Ext.fly(el).setStyle('display', 'none');
						});
						this.doLayout();
					}, 500, this);

					this.processDataRequest(r, button);
				},
				failure: function(r) {
					if (this.showLoading) Ext.MessageBox.hide()
					clientVM.displayFailure(r);
				}
			})
		}
	},

	processUrlVariables: function() {
		if (!this.rendered || !this.vm) return;
		var theRootWnd = window;
		var params = {};
		var hasParams = false;
		if (this.params) {
			hasParams = true;
			params = Ext.Object.fromQueryString(this.params)
		} else {
			try {
				while (theRootWnd.parent != theRootWnd) {
					if (theRootWnd.location.search) {
						hasParams = true;
						Ext.apply(params, Ext.Object.fromQueryString(theRootWnd.location.search));
					}
					theRootWnd = theRootWnd.parent;
				}
				if (theRootWnd.location.search) {
					hasParams = true;
					Ext.apply(params, Ext.Object.fromQueryString(theRootWnd.location.search));
				}
			} catch (e) {
				//cross-site security
			}
		}

		if (hasParams) {
			for (var key in params) {
				if (Ext.isDefined(this.vm[key])) this.vm.set(key, params[key]);
			}
			//Special processing for radios/checkboxes because the bindings don't work with extjs's messed up structure, we have to manually set the values
			var components = this.query('radiogroup, checkboxgroup');
			Ext.each(components, function(component) {
				var name = component.name;
				if (!name) name = component.items.getAt(0).name;
				if (Ext.isDefined(params[name])) {
					var obj = {};
					obj[name] = params[name];
					component.setValue(obj);
				}
			});
			//Special processing for hidden variables
			var hiddenFields = this.query('hiddenfield');
			Ext.each(hiddenFields, function(hiddenField) {
				if (Ext.isDefined(params[hiddenField.name])) {
					hiddenField.setValue(params[hiddenField.name]);
				}
			});
		}
	},
	parseDependencies: function(controlConfig) {
		this.applyDependencies(controlConfig, controlConfig.dependencies);
	},
	parseButtonDependencies: function(buttonConfig) {
		var dependencies = buttonConfig.dependencies || [];
		Ext.each(buttonConfig.actions, function(action) {
			if (action.operation == 'NEXT') dependencies.push({
				target: 'activeTabIndex',
				action: 'isVisible',
				condition: 'lessThan',
				value: 'this.tabCount-1',
				template: '{0} {1} {2}'
			});
			if (action.operation == 'BACK') dependencies.push({
				target: 'activeTabIndex',
				action: 'isVisible',
				condition: 'greaterThan',
				value: '0',
				template: '{0} {1} {2}'
			});
		});
		this.applyDependencies(buttonConfig, dependencies);
	},
	applyDependencies: function(source, dependencies) {
		var actionFunctions = {},
			actionOptions = {};
		Ext.each(dependencies, function(dependency) {
			var values = (dependency.value || '').split(/[\||,]/),
				target = 'this.' + dependency.target,
				targetFormField = this.getFormControl(dependency.target),
				targetFieldType = this.getControlType(targetFormField),
				functionString = '',
				actionKey = '',
				condition = '==',
				template = dependency.template;

			switch (dependency.condition) {
				case 'isEmpty':
				case 'equals':
				case 'on':
					condition = '==';
					break;
				case 'notequals':
				case 'notEmpty':
					condition = '!=';
					break;
				case 'greaterThan':
				case 'after':
					condition = '>';
					break;
				case 'greaterThanOrEqual':
					condition = '>=';
					break;
				case 'lessThan':
				case 'before':
					condition = '<';
					break;
				case 'lessThanOrEqual':
					condition = '<=';
					break;
				case 'contains':
					template = '{0} == null ? false : {0}.toString().indexOf("{2}") > -1';
					condition = dependency.condition;
					break;
				case 'notcontains':
					template = '{0} == null ? false : {0}.toString().indexOf("{2}") == -1';
					condition = dependency.condition;
					break;
			}

			Ext.each(values, function(value, index) {
				if (index > 0) functionString += ' || ';
				var val = Ext.String.trim(value);

				if (dependency.condition === 'isEmpty' || dependency.condition === 'notEmpty') {
					val = "";
					template = '{0} {1} "{2}"';
				} else if (targetFieldType === "date") {
					var dateString = val;
					switch (dateString.toLowerCase()) {
						case 'today':
							dateString = Ext.Date.format(new Date(), 'Y-m-d');
							break;
						case 'yesterday':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, -1), 'Y-m-d');
							break;
						case 'tomorrow':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, 1), 'Y-m-d');
							break;
						case 'last week':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, -7), 'Y-m-d');
							break;
						case 'next week':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, 7), 'Y-m-d');
							break;
						case 'last month':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.MONTH, -1), 'Y-m-d');
							break;
						case 'next month':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.MONTH, 1), 'Y-m-d');
							break;
						case 'last year':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, -1), 'Y-m-d');
							break;
						case 'next year':
							dateString = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, 1), 'Y-m-d');
							break;
						default:
							break;
					}
					var tryDate = this.parseDates(dateString);
					if (Ext.isDate(tryDate.date)) {
						if (Ext.Date.formatContainsHourInfo(tryDate.format) && Ext.Date.formatContainsDateInfo(tryDate.format)) {
							val = Ext.String.format('Ext.Date.parse("{0}", "{1}")', dateString, tryDate.format);
							target = Ext.String.format('Ext.Date.parse({0}, "c")', target);
						} else if (Ext.Date.formatContainsHourInfo(tryDate.format)) {
							//For time we need to assume we're deal with a time field, so we need to clear the date and use the default date that sencha uses
							//initDate: [2008,0,1],
							val = Ext.String.format('Ext.Date.parse("{0}", "{1}").setFullYear(2008,0,1)', dateString, tryDate.format);
						} else if (Ext.Date.formatContainsDateInfo(tryDate.format)) {
							val = Ext.String.format('Ext.Date.clearTime(Ext.Date.parse("{0}", "{1}"))', dateString, tryDate.format);
						}
						template = Ext.Date.formatContainsHourInfo(tryDate.format) ? '{0} {1} {2}' : (condition == '==' ? '(!Ext.isDate({0}) ? false : Ext.Date.clearTime({0}) >= {2} && Ext.Date.clearTime({0}) <= {2})' : '(!Ext.isDate({0}) ? false : Ext.Date.clearTime({0}) {1} {2})');
					}
				} else if (targetFieldType === "bool" && (val.toLowerCase() === 'true' || val.toLowerCase() === 'false')) {
					template = '{0} {1} {2} ' + (dependency.condition == 'notequals' ? '&&' : '||') + ' {0} {1} \"{2}\"';
					val = val.toLowerCase();
				} else if (targetFieldType === "number" && Ext.isNumeric(val) && (Ext.Array.indexOf(['==', '!=', '<', '<=', '>', '>='], condition) > -1)) {
					val = Number(val);
					template = '{0} {1} {2}';
				}

				if (!template) template = '{0} {1} "{2}"';

				functionString += Ext.String.format(template, target, condition, val);
			}, this);

			switch (dependency.action) {
				case 'isEnabled':
					actionKey = 'IsEnabled$';
					break;
				case 'isVisible':
					actionKey = 'IsVisible$';
					break;
				case 'setBgColor':
					actionKey = 'SetBackgroundColor$';
					break;
				default:
					break;
			}

			if (actionKey) {
				if (actionFunctions[actionKey]) {
					if (actionKey == 'SetBackgroundColor$') {
						var count = 1;
						while (actionFunctions[actionKey]) {
							actionKey = Ext.String.format('SetBackgroundColor{0}$', count);
							count++;
						}
						actionFunctions[actionKey] += ' ( ' + functionString + ' ) ';
						actionOptions[actionKey] = dependency.actionOptions;
					} else {
						if (actionFunctions[actionKey].length > 0) actionFunctions[actionKey] += ' && ';
						actionFunctions[actionKey] += ' ( ' + functionString + ' ) ';
						actionOptions[actionKey] = dependency.actionOptions;
					}
				} else {
					actionFunctions[actionKey] = ' ( ' + functionString + ' ) ';
					actionOptions[actionKey] = dependency.actionOptions;
				}
			}
		}, this);

		for (var key in actionFunctions) {
			if (key.indexOf('SetBackgroundColor') > -1) {
				this.viewmodelSpec[source.name + key] = new Function('return (' + actionFunctions[key] + ') ? "' + actionOptions[key] + '" : "transparent"');
				source.fieldStyle = 'background-color:@{' + source.name + key.substring(0, key.length - 1) + '}';
			} else this.viewmodelSpec[source.name + key] = new Function('return ' + actionFunctions[key]);
		}
	},

	getAllFormControls: function(fieldName) {
		var self = this,
			i = 0,
			config = self.formConfiguration,
			controls = [];
		if (config) {
			if (config.panels[0].tabs.length > 1) { //User checked the "use tabs" checkbox
				var tabItems = [],
					tabs = config.panels[0].tabs;
				for (i = tabs.length - 1; i > 0; i--) {
					controls.concat(tabs[i].columns[0].fields); //concatenate each fields array into a single controls array.
				}
			} else if (config.panels[0].tabs[0].columns.length > 0) { //more common use case - no tabs. //check that there's at least one column before proceeidng
				controls = config.panels[0].tabs[0].columns[0].fields; //set the controls = to these fields, as they are all there is in this form.
				/*Ext.each(controls, function(control){
					//testFormControl
				});*/
			}
		}
		return controls;
	},

	getFormControl: function(fieldName) {
		var matchingControl = null,
			i = 0,
			k = 0,
			controls = this.getAllFormControls(),
			control = null;
		loopOverControls: for (i = controls.length; i--;) {
			control = controls[i];
			if (control.uiType === 'SpacerField') { //ignore space fields
				continue loopOverControls;
			} else if (false /*control.columns*/ ) { //if there are formfields nested in this control (i.e. it's a section) iterate through those and check them.
				/*control.columns.foreach(function(col) {
					col.controls.foreach(function(subControl) {}
				}*/
			} else if (control.name === fieldName) { //otherwise check the current field in the iteration to see if the names match. if so return it.
				matchingControl = control;
				break loopOverControls;
			}
		}
		return matchingControl;
	},

	getControlType: function(control) {
		var type = '';
		if (control) {
			switch (control.uiType) {
				case 'CheckBox':
				case 'MultipleCheckBox':
					type = 'bool';
					break;
				case 'NumberTextField':
				case 'DecimalTextField':
					type = 'number';
					break;
				case 'Date':
				case 'DateTime':
					type = 'date';
					break;
				default:
					type = 'string';
					break;
			}
		}
		return type;
	},

	parseDates: function(dateString) {
		var formats = ['Y-m-d', 'g:i a', 'g:i A', 'G:i', 'Y-m-d g:i a', 'Y-m-d g:i A', 'Y-m-d G:i'],
			returnVal = {
				date: '',
				format: ''
			};

		Ext.each(formats, function(format) {
			var date = Ext.Date.parse(dateString, format);
			if (Ext.isDate(date)) {
				returnVal = {
					date: date,
					format: format
				};
				return false;
			}
		});
		return returnVal;
	}
});

/**
 * @author ryan.smith
 */
Ext.define('RS.formbuilder.viewer.mobile.RSForm', {
	choiceSeparator: '|&|',
	choiceValueSeparator: '|=|',

	extend: 'Ext.Panel',
	alias: 'widget.rsform_mobile',

	fullscreen: true,
	border: false,

	fieldConfiguration: [],
	submitConfiguration: {
		viewName: '',
		formSysId: '',
		tableName: '',
		userId: '',
		timezone: '',
		crudAction: '',
		query: '',
		controlItemSysId: '',
		dbRowData: {},
		sourceRowData: {}
	},

	/**
	 * Id of the form to use when looking up the controls from the server
	 */
	formId: '',

	/**
	 * Name of the form to use when looking up the controls from the server
	 */
	formName: '',

	/**
	 * The record to load into the grid once the form has actually been rendered (or empty for a new record)
	 */
	recordId: '',

	initialize: function() {
		//convert all the items configurations to proper configurations
		var configuration = this.formConfiguration;
		if (configuration) this.convertConfig(configuration);
		else Ext.Ajax.request({
			url: '/resolve/service/form/getform',
			params: {
				formname: this.initialConfig.formName,
				formsysid: this.initialConfig.formId,
				recordid: this.initialConfig.recordId
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					if (response.data) this.convertConfig(response.data);
					else clientVM.displayError('Form not found');
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		});
		this.callParent();
	},

	convertConfig: function(config) {
		var form = {},
			configuration;
		var buttonPanel = config.buttonPanel.controls;

		Ext.apply(this.submitConfiguration, {
			viewName: config.viewName,
			formSysId: config.id,
			tableName: config.tableName,
			userId: ''
		});

		//look in temp for the real controls to display
		if (config.panels[0].tabs.length > 1) {
			var tabItems = [],
				tabs = config.panels[0].tabs;
			Ext.each(tabs, function(tab) {
				var controls = tab.columns[0].fields;
				Ext.each(controls, this.convertControl, this);
				tabItems.push({
					layout: 'anchor',
					title: tab.name,
					items: controls
				});
			}, this);
			configuration = {
				xtype: 'tabpanel',
				items: tabItems
			};
		} else {
			configuration = config.panels[0].tabs[0].columns[0].fields;
			this.fieldConfiguration = config.panels[0].tabs;

			//convert all the controls to appropriate extjs controls
			//TODO: Pass the userId to the user picker field to have an "assign to me" feature
			Ext.each(configuration, this.convertControl, this);
		}

		//convert the buttons
		Ext.each(buttonPanel, this.convertButton, this);
		var buttonToolbar = {
			xtype: 'toolbar',
			ui: 'light',
			docked: 'bottom',
			layout: {
				pack: 'left'
			},
			items: buttonPanel
		};
		configuration.push(buttonToolbar);

		Ext.apply(form, {
			// bodyPadding: 10,
			autoScroll: true,
			xtype: 'formpanel',
			title: config.displayName,
			items: configuration
		});

		//Showing a tab panel instead of just a form, so we need to correct some normal form styling
		if (config.panels[0].tabs.length > 1) {
			form.layout = 'fit';
			delete form.bodyPadding;
		}

		if (this.rendered) {
			this.add(form);
			this.processUrlVariables();
		} else this.items = form;
	},

	/**
	 * Converts an individual control provided by the server to the correct representation for extjs to display
	 * @param {Object} config
	 */
	convertControl: function(config) {
		config.label = config.displayName;
		config.value = config.value || config.defaultValue;

		config.required = config.mandatory;

		if (!config.width) delete config.width;
		if (!config.height) delete config.height;

		if (!config.width) config.anchor = '-20';

		switch (config.uiType) {
			case 'TextField':
				config.xtype = 'textfield';
				config.minLength = config.stringMinLength;
				config.maxLength = config.stringMaxLength;
				break;

			case 'TextArea':
				config.xtype = 'textareafield';
				config.minLength = config.stringMinLength;
				config.maxLength = config.stringMaxLength;
				break;

			case 'NumberTextField':
				config.xtype = 'numberfield';
				config.allowDecimals = false;
				config.minValue = config.integerMinValue;
				config.maxValue = config.integerMaxValue;
				break;

			case 'DecimalTextField':
				config.xtype = 'numberfield';
				config.minValue = config.decimalMinValue;
				config.maxValue = config.decimalMaxValue;
				break;

			case 'ComboBox':
				config.xtype = 'combobox';
				config.displayField = 'name';
				config.valueField = 'value';
				config.value = this.parseValue(config.value).join('');
				config.store = Ext.create('Ext.data.Store', {
					data: this.parseOptions(config.selectValues),
					fields: [{
						name: 'name'
					}, {
						name: 'value'
					}]
				});
				break;
			case 'MultipleCheckBox':
				config.xtype = 'checkboxgroup';
				config.columns = 1;
				config.items = this.parseOptions(config.checkboxValues, config.value);
				Ext.each(config.items, function(item) {
					Ext.apply(item, {
						name: config.name
					});
				});
				delete config.value;
				break;

			case 'RadioButton':
				config.xtype = 'radiogroup';
				config.columns = 1;
				config.items = this.parseOptions(config.choiceValues, config.value);
				Ext.each(config.items, function(item) {
					Ext.apply(item, {
						name: config.name
					});
				});
				delete config.value;
				break;

			case 'Date':
				config.xtype = 'xdatefield';
				break;

			case 'Timestamp':
				config.xtype = 'xtimefield';
				break;

			case 'DateTime':
				config.xtype = 'xdatetimefield';
				break;

			case 'Link':
				config.xtype = 'linkfield';
				break;

			case 'CheckBox':
				config.xtype = 'checkbox';
				config.checked = config.value;
				config.inputValue = 'true';
				break;

			case 'Password':
				config.xtype = 'textfield';
				config.inputType = 'password';
				break;

			case 'TagField':
				config.xtype = 'tagfield';
				break;

			case 'Sequence':
				config.xtype = 'displayfield';
				break;

			case 'List':
				config.xtype = 'listfield';
				config.parseOptions = this.parseOptions;
				break;

			case 'ButtonField':
				//deal with actions
				config.xtype = 'button';
				delete config.anchor;
				config.text = config.displayName;
				config.handler = this.handleButtonClick;
				config.scope = this;
				break;

			case 'Reference':
				config.xtype = 'textfield';
				//TODO: might need to be a custom control
				break;

			case 'Journal':
				config.xtype = 'journalfield';
				break;

			case 'ReadOnlyTextField':
				config.xtype = 'displayfield';
				break;

			case 'UserPickerField':
				config.xtype = 'userpickerfield';
				break;

			case 'TeamPickerField':
				config.xtype = 'teampickerfield';
				break;

			case 'EmailField':
				config.xtype = 'textfield';
				config.regex = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
				config.invalidText = 'The value in this field is invalid.  It must be an email address like: someone@example.com';
				break;
			
			case 'MACAddressField':
				config.xtype = 'textfield';
				config.regex = /^[0-9a-f]{1,2}([\.:-])(?:[0-9a-f]{1,2}\1){4}[0-9a-f]{1,2}$/;
				config.invalidText = 'The value in this field is invalid.  It must be an MAC address like: 00-11-22-33-44-55';
				break;

			case 'IPAddressField':
				config.xtype = 'textfield';
				config.regex = /^(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))$/;
				config.invalidText = 'The value in this field is invalid.  It must be an IP Address like: 192.168.0.1';
				break;

			case 'CIDRAddressField':
				config.xtype = 'textfield';
				config.regex = /^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2]))?$/;
				config.invalidText = 'The value in this field is invalid.  It must be an CIDR Address like: 192.168.0.1/14';
				break;

			case 'PhoneField':
				config.xtype = 'textfield';
				//config.plugins = [new RS.formbuilder.viewer.desktop.InputTextMask('(999) 999 - 9999')];
				break;

			case 'NameField':
				config.xtype = 'namefield';
				break;

			case 'AddressField':
				// config.xtype = 'addressfield';
				config.xtype = 'textfield';
				break;

			case 'HiddenField':
				config.xtype = 'hiddenfield';
				config.value = config.hiddenValue;
				break;

			default:
				// debugger;
				break;
		}
	},

	/**
	 * Converts a server configuration of a button into a button that can be rendered by extjs
	 * @param {Object} button
	 */
	convertButton: function(button) {
		button.text = button.displayName || "Fix Me!";
		button.handler = this.handleButtonClick;
		button.sysId = button.id;
		button.scope = this;
		button.xtype = 'button';
		//determine crud action for db type actions
		button.crudAction = this.getCrudAction(button);
	},
	getCrudAction: function(button) {
		var i, actions = button.actions,
			len = actions.length,
			action;
		for (i = 0; i < len; i++) {
			action = actions[i];
			if (action.operation == 'DB') return action.crudAction;
		}
	},
	/**
	 * Parses values out of a select or choice control
	 * @param {Object} configValue
	 */
	parseValue: function(configValue) {
		var returnValue = [],
			configValue = configValue || '';
		var values = configValue.split(this.choiceSeparator);
		Ext.each(values, function(value) {
			var name = '';
			var val = value.split(this.choiceValueSeparator);
			returnValue.push(val[1] == 'true' ? val[0] : val[1]);
		}, this);
		return returnValue;
	},
	/**
	 * Parses options out of a select or choice control
	 * @param {Object} optionString
	 * @param {Object} valueString
	 */
	parseOptions: function(optionString, valueString) {
		var options = [],
			optionString = optionString || '';
		var split = optionString.split(this.choiceSeparator);
		Ext.each(split, function(option) {
			var nameValue = option.split(this.choiceValueSeparator);
			if (nameValue[0]) options.push({
				boxLabel: nameValue[0],
				name: nameValue[0],
				inputValue: nameValue[0],
				value: nameValue[1] && (nameValue[1] != 'true' || nameValue[1] != 'false') ? nameValue[1] : nameValue[0]
			})
		}, this);

		if (valueString) {
			var values = valueString.split(this.choiceSeparator),
				i, j;
			for (i = 0; i < values.length; i++) {
				var val = values[i].split(this.choiceValueSeparator);
				if (val[1] == 'true') {
					for (j = 0; j < options.length; j++) {
						if (options[j].boxLabel == val[0]) options[j].checked = true;
					}
				}
			}
		}
		return options;
	},
	/**
	 * Handles the button clicks to actions relationship and will perform the appropriate posting of the values to be processed by the server.
	 * Deals with reset and close actions client side if everything goes well on the server side.
	 * @param {Object} button
	 */
	handleButtonClick: function(button) {
		//Change button to show "working" state
		button.disable();
		button.setIcon('/resolve/images/loading.gif');

		//Handle actions
		var shouldReset = false,
			shouldClose = false,
			submitToServer = false;
		Ext.each(button.actions, function(action) {
			switch (action.operation.toLowerCase()) {
				case 'reset':
					shouldReset = true;
					break;
				case 'close':
					shouldClose = true;
					break;
				case 'db':
				case 'event':
				case 'runbook':
				case 'script':
					submitToServer = true;
					break;
				default:
					Ext.Msg.alert('Unknown action', 'The form you have loaded has an action that is unknown to this viewer.')
					break;
			}
		});
		if (submitToServer) {
			var params = button.up('form').getForm().getValues(),
				serverParams = this.submitConfiguration,
				key;

			//The server needs to know the id of the button that was clicked to know which actions to perform
			serverParams.controlItemSysId = button.sysId;
			serverParams.crudAction = button.crudAction;

			//Prepare the params for form submission
			var dbData = {},
				inputData = {};
			for (key in params) {
				if (this.getParamType(key) === 'DB') dbData[key] = Ext.isArray(params[key]) ? this.serializeChoice(params[key]) : params[key]
				else inputData[key] = Ext.isArray(params[key]) ? this.serializeChoice(params[key]) : params[key];
			}
			serverParams.dbRowData.data = dbData;
			serverParams.sourceRowData.data = inputData;

			Ext.Ajax.request({
				url: '/resolve/service/form/submit',
				jsonData: serverParams,
				scope: this,
				success: function(r) {
					button.enable();
					button.setIcon('');
					var response = RS.common.parsePayload(r);
					if (response.success) {
						if (shouldReset) {
							this.getForm().reset();
						}
						//display success message
						clientVM.displaySuccess('Form saved successfully', null, 'Success');						

						if (shouldClose) {
							var win = this.up('window') || window;
							Ext.defer(function() {
								win.close();
							}, 3000);
						}
						this.fireEvent('actioncompleted', this, button);
					} else {
						//display error message
						RS.UserMessage.msg({
							success: false,
							title: 'Failure',
							msg: response.message
						});
					}
				},
				failure: function(r) {
					button.enable();
					button.setIcon('');
					//Ext.Msg.alert('Error', 'An unknown error has occured');
					clientVM.displayFailure(r);
				}
			});
		} else {
			button.enable();
			button.setIcon('');
			if (shouldReset) this.getForm().reset();
			if (shouldClose) {
				var win = button.up('window') || window;
				win.close();
			}
		}
	},
	serializeChoice: function(paramsArray) {
		var params = [];
		Ext.each(paramsArray, function(param) {
			params.push(param + this.choiceValueSeparator + 'true');
		}, this);

		return params.join(this.choiceSeparator);
	},
	getParamType: function(param) {
		var i, j, tab, column, field;
		for (i = 0; i < this.fieldConfiguration.length; i++) {
			tab = this.fieldConfiguration[i];
			for (j = 0; j < tab.columns.length; j++) {
				column = tab.columns[i];
				for (f = 0; f < column.fields.length; f++) {
					field = column.fields[f];
					if (field.name == param) return field.sourceType;
				}
			}
		}
		return 'INPUT';
	},
	setRecordId: function(recordId) {
		this.recordId = recordId;
		Ext.Ajax.request({
			url: '/resolve/service/form/getRecordData',
			params: {
				formId: this.id,
				recordId: this.recordId
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					// debugger;
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(r){
				clientVM.displayFailure(r);
			}
		});
	},
	processUrlVariables: function() {
		var search = window.location.search;
		if (search) {
			var params = Ext.Object.fromQueryString(search.substring(1));
			this.down('form').getForm().setValues(params);
		}
	}
});
glu.defModel('RS.formbuilder.AccessRightsPicker', {

	init: function() {
		this.roles.load({
			callback: this.rolesLoaded,
			scope: this
		});
	},

	rolesLoaded: function(records, operation, success) {
		for (var i = 0; i < this.currentRights.length; i++) {
			var currentRight = this.currentRights[i];
			var role = this.roles.findBy(function(role) {
				if (role.data.value == currentRight.value) return true;
				return false;
			});
			this.roles.removeAt(role);
		}
	},

	currentRights: [],

	roles: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getRoles',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	rolesSelections: [],

	cancel: function() {
		this.doClose();
	},
	addAccessRightsAction: function() {
		this.parentVM.addRealAccessRights(this.rolesSelections);
		this.doClose();
	},
	addAccessRightsActionIsEnabled$: function() {
		return this.rolesSelections.length > 0;
	}
});
/**
 * Model definition for an Action.
 * An Action is contained in a button, and keeps track of the operation and associated value that needs to be executed on the server when the button is clicked
 */
glu.defModel('RS.formbuilder.Action', {
	id: '',
	/**
	 * Tells us whether this action is new or being edited
	 */
	isNew: true,
	/**
	 * Title of the action window 'Add Action' or 'Edit Action'
	 */
	actionTitle$: function() {
		return this.isNew ? this.localize('addAction') : this.localize('editAction');
	},
	/**
	 * The operation to perform on the server
	 */
	operation: '',
	/**
	 * The translated operation to display to the user
	 */
	operationName$: function() {
		return this.localize(this.operation);
	},
	/**
	 * The translated value of the operation (if one is set)
	 */
	value$: function() {
		switch (this.operation) {
			case 'DB':
				return this.localize(this.dbAction);
			case 'SIR':
				return this.localize(this.sirAction);
			case 'RUNBOOK':
				return this.RUNBOOK;
			case 'SCRIPT':
				return this.scriptName || this.SCRIPT;
			case 'EVENT':
				return this.EVENT_EVENTID;
			case 'MAPPING':
				return this.mappingTo;
			case 'ACTIONTASK':
				return this.ACTIONTASK;
			case 'RELOAD':
				return this.reloadDelay;
			case 'GOTO':
				return this.gotoTabName
		}

		return '';
	},
	/**
	 * True to indicate that the user shouldn't be able to select the db operation type because there is already one for the button
	 */
	alreadyHasDBOperation: false,
	/**
	 * The db action to perform if the operation is DB
	 */
	dbAction: 'SUBMIT',
	/**
	 * The sir action to perform if the operation is SIR
	 */
	sirAction: 'saveCaseData',
	/**
	 * The runbook to run if the operation is runbook
	 */
	RUNBOOK: '',
	runbookChange: function(newValue) {
		this.set('RUNBOOK', newValue)
	},
	/**
	 * The script to run if the operation is script
	 */
	SCRIPT: '',
	/**
	 * Formula for getting the script name to display to the user if the operation is script
	 */
	scriptName$: function() {
		var value = this.SCRIPT;
		if (this.getScripStoreCount() > 0) {
			var record = this.getScripStoreRecord(value);
			if (record) {
				return record.data.name;
			}
		}
		return this.SCRIPT;
	},
	getScripStoreCount : function(){
		return this.rootVM.mainForm.scriptStore.getCount();
	},
	getScripStoreRecord : function(value){
		return this.rootVM.mainForm.scriptStore.findRecord('value', value, 0, false, false, true);
	},
	//Runbook related
	WIKI: '',
	WIKIIsVisible$: function() {
		this.operation == 'RUNBOOK';
	},

	PROBLEMID: '',
	//related to both runbook and events
	PROBLEMIDIsVisible$: function() {
		return this.operation == 'RUNBOOK' || this.operation == 'EVENT' || this.operation == 'ACTIONTASK';
	},
	problemIdStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},

	//Event related
	EVENT_EVENTID: '',
	EVENT_EVENTIDIsVisible$: function() {
		return this.operation == 'EVENT'
	},
	EVENT_EVENTIDIsValid$: function() {
		if (this.operation == 'EVENT' && !this.EVENT_EVENTID) return this.localize('eventIdInvalid');
		return true;
	},
	// EVENT_REFERENCE: '',
	// EVENT_REFERENCEIsVisible$: function() {
	// 	return this.operation == 'EVENT'
	// },
	// EVENT_REFERENCEIsValid$: function() {
	// 	if (this.operation == 'EVENT' && this.EVENT_REFERENCE == '') return this.localize('eventReferenceInvalid');
	// 	return true;
	// },
	//Script related
	TIMEOUT: '',	// blank TIMEOUT means use the global.execute.timeout value defined in System Properties.
	usingDefault: false,
	synchronous$: function() {
		return (this.usingDefault || this.TIMEOUT >= 0);
	},
	asynchronous$: function() {
		return this.TIMEOUT < 0;
	},
	wait$: function() {
		return (this.usingDefault || this.TIMEOUT > 0);
	},
	waitSeconds: 0,
	waitSecondsIsEnabled$: function() {
		return (this.synchronous && !this.usingDefault);
	},

	useDefaultIsEnabled$: function() {
		return this.synchronous;
	},
	updateUseDefault: function() {
		this.set('usingDefault', !this.usingDefault);
	},
	timeoutGroup: 0,
	timeoutGroupIsVisible$: function() {
		return this.operation == 'SCRIPT'
	},
	scriptTimeout: '',
	scriptTimeoutIsVisible$: function() {
		return this.operation == 'SCRIPT';
	},

	mappingStore: {
		mtype: 'store',
		fields: ['name']
	},

	mappingFrom: '',
	mappingFromIsVisible$: function() {
		return this.operation == 'MAPPING' || this.operation == 'EVENT' && this.PROBLEMID == 'MAPFROM';
	},
	mappingFromIsValid$: function() {
		if (this.operation == 'MAPPING' && !this.mappingFrom) return this.localize('mappingFromInvalid');
		return true;
	},
	mappingTo: '',
	mappingToIsVisible$: function() {
		return this.operation == 'MAPPING'
	},
	mappingToIsValid$: function() {
		if (this.operation == 'MAPPING' && !this.mappingTo) return this.localize('mappingToInvalid');
		return true;
	},

	redirectTarget: '_blank',
	redirectTargetIsVisible$: function() {
		return this.operation == 'REDIRECT'
	},
	redirectTargetIsValid$: function() {
		if (this.operation == 'REDIRECT' && !this.redirectTarget) return this.localize('redirectTargetInvalid');
		return true;
	},
	redirectStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},
	redirectUrl: '',
	redirectUrlIsVisible$: function() {
		return this.operation == 'REDIRECT'
	},

	ACTIONTASK: '',
	ACTIONTASKIsVisible$: function() {
		return this.operation == 'ACTIONTASK';
	},
	ACTIONTASKIsValid$: function() {
		if (this.operation == 'ACTIONTASK' && !this.ACTIONTASK) return this.localize('actionTaskInvalid');
		return true;
	},

	showConfirmation: true,
	showConfirmationIsVisible$: function() {
		return this.operation == 'DB' && this.dbAction == 'DELETE';
	},

	messageBoxTitle: '',
	messageBoxTitleIsVisible$: function() {
		return this.operation == 'MESSAGEBOX'
	},
	messageBoxMessage: '',
	messageBoxMessageIsVisible$: function() {
		return this.operation == 'MESSAGEBOX'
	},
	messageBoxYesText: '',
	messageBoxYesTextIsVisible$: function() {
		return this.operation == 'MESSAGEBOX'
	},
	messageBoxNoText: '',
	messageBoxNoTextIsVisible$: function() {
		return this.operation == 'MESSAGEBOX'
	},

	reloadDelay: 0,
	reloadDelayIsVisible$: function() {
		return this.operation == 'RELOAD';
	},

	reloadDependencyIsVisible$: function() {
		return this.operation == 'RELOAD';
	},
	reloadTarget: '',
	reloadCondition: '',
	reloadValue: '',

	gotoTabName: '',
	gotoTabNameIsVisible$: function() {
		return this.operation == 'GOTO'
	},

	dependencyConditionStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		listeners: {
			load: function() {
				this.removeAll();
				//Add in only the options that make sense for the target.  If there is no target, then display all of them
				var target = this.parentVM.reloadTarget,
					isString = true;
				if (target) {
					var control;
					this.rootVM.mainForm.controls.foreach(function(ctrl) {
						if (ctrl.inputName == target || ctrl.column == target) {
							control = ctrl;
							return false;
						}
					});
					if (control) {
						switch (control.viewmodelName) {
							case 'NumberField':
							case 'DecimalField':
							case 'DateField':
							case 'DateTimeField':
							case 'TimeField':
								isString = false;
								break;
							default:
								isString = true;
						}
					}
				}

				if (target == 'activeTabIndex') isString = false;

				this.add({
					name: this.parentVM.localize('equals'),
					value: 'equals'
				}, {
					name: this.parentVM.localize('notequals'),
					value: 'notequals'
				});

				if (isString) {
					this.add([{
						name: this.parentVM.localize('contains'),
						value: 'contains'
					}, {
						name: this.parentVM.localize('notcontains'),
						value: 'notcontains'
					}]);
				} else {
					this.add([{
						name: this.parentVM.localize('greaterThan'),
						value: 'greaterThan'
					}, {
						name: this.parentVM.localize('greaterThanOrEqual'),
						value: 'greaterThanOrEqual'
					}, {
						name: this.parentVM.localize('lessThan'),
						value: 'lessThan'
					}, {
						name: this.parentVM.localize('lessThanOrEqual'),
						value: 'lessThanOrEqual'
					}]);
				}
			}
		}
	},

	dependencyTargetStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		listeners: {
			load: function() {
				var fieldNames = this.rootVM.getFormFieldNames();
				Ext.each(fieldNames, function(name) {
					this.add({
						name: name,
						value: name
					})
				}, this);
				if (this.rootVM.mainForm.useTabs) this.add({
					name: 'activeTabIndex',
					value: 'activeTabIndex'
				});
				this.sort('name', 'ASC');
			}
		}
	},


	leaf: true,

	/**
	 * List of important fields that should be used for serializing this action
	 */
	fields: ['operation', 'operationName', 'dbAction', 'RUNBOOK', 'SCRIPT', 'value', 'PROBLEMID', 'EVENT_EVENTID', 'EVENT_REFERENCE', 'TIMEOUT', 'waitSeconds', 'mappingFrom', 'mappingTo', 'redirectTarget', 'redirectUrl', 'ACTIONTASK', 'showConfirmation', 'messageBoxTitle', 'messageBoxMessage', 'messageBoxYesText', 'messageBoxNoText', 'reloadDelay', 'reloadTarget', 'reloadCondition', 'reloadValue', 'gotoTabName'],

	/**
	 * Type of item in the grid, because we're an action this is hard coded to action to indicate that we are an action to the UI
	 */
	type: 'action',

	/**
	 * Store to display different operations
	 */
	operationStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: []
	},


	/**
	 * Store to display different dbActions
	 */
	dbActionStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: []
	},

	/**
	 * Store to display different sirActions
	 */
	sirActionStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: []
	},

	init: function() {
		//populate the combobox stores
		this.operationStore.removeAll();
		this.operationStore.add([{
			name: this.localize('DB'),
			value: 'DB'
		}, {
			name: this.localize('SIR'),
			value: 'SIR'
		}, {
			name: this.localize('RUNBOOK'),
			value: 'RUNBOOK'
		}, {
			name: this.localize('EVENT'),
			value: 'EVENT'
		}, {
			name: this.localize('SCRIPT'),
			value: 'SCRIPT'
		}, {
			name: this.localize('MAPPING'),
			value: 'MAPPING'
		}, {
			name: this.localize('REDIRECT'),
			value: 'REDIRECT'
		}, {
			name: this.localize('CLOSE'),
			value: 'CLOSE'
		}, {
			name: this.localize('RESET'),
			value: 'RESET'
		}, {
			name: this.localize('ACTIONTASK'),
			value: 'ACTIONTASK'
		}, {
			name: this.localize('MESSAGEBOX'),
			value: 'MESSAGEBOX'
		}, {
			name: this.localize('reload'),
			value: 'RELOAD'
		}, {
			name: this.localize('returnText'),
			value: 'RETURN'
		}, {
			name: this.localize('exit'),
			value: 'EXIT'
		}]);
		this.operationStore.sort('name', 'ASC');

		if (this.parentVM.useTabs) {
			this.operationStore.add([{
				name: this.localize('NEXT'),
				value: 'NEXT'
			}, {
				name: this.localize('BACK'),
				value: 'BACK'
			}, {
				name: this.localize('GOTO'),
				value: 'GOTO'
			}]);
		}

		this.dbActionStore.removeAll();
		this.dbActionStore.add([{
			name: this.localize('submitAction'),
			value: 'SUBMIT'
		}, {
			name: this.localize('deleteActionName'),
			value: 'DELETE'
		}]);

		this.sirActionStore.removeAll();
		this.sirActionStore.add([{
			name: this.localize('saveCaseData'),
			value: 'saveCaseData'
		}]);

		this.problemIdStore.removeAll();
		switch (this.operation) {
			case 'RUNBOOK':
			case 'ACTIONTASK':
				this.problemIdStore.removeAll()
				this.problemIdStore.add([{
						name: this.localize('NEW'),
						value: 'NEW'
					}, {
						name: this.localize('LOOKUP'),
						value: 'LOOKUP'
					}, {
						name: this.localize('ACTIVE'),
						value: 'ACTIVE'
					},
					/*, {
					name: this.localize('NONE'),
					value: ''
				}, */
					{
						name: this.localize('MAPFROM'),
						value: 'MAPFROM'
					}
				])
				break;

			case 'EVENT':
				this.set('PROBLEMID', 'LOOKUP')
				this.problemIdStore.removeAll()
				this.problemIdStore.add([
					/*{
					name: this.localize('NEW'),
					value: 'NEW'
				},*/
					{
						name: this.localize('LOOKUP'),
						value: 'LOOKUP'
					},
					/*{
					name: this.localize('ACTIVE'),
					value: 'ACTIVE'
				}, {
					name: this.localize('NONE'),
					value: ''
				}, */
					{
						name: this.localize('MAPFROM'),
						value: 'MAPFROM'
					}
				])
				break;

			case 'SCRIPT':
				if (this.TIMEOUT == '') {
					this.set('usingDefault', true);
				}
				else {
					this.set('usingDefault', false);
				}
				break;
		
			default: 
				break;
		}

		var names = this.rootVM.getFormFieldNames();
		Ext.each(names, function(name) {
			this.mappingStore.add({
				name: name
			});
		}, this);

		this.rootVM.mainForm.hiddenFields.each(function(hiddenField) {
			this.mappingStore.add({
				name: hiddenField.data.name
			});
		}, this);

		this.mappingStore.add({
			name: 'PROBLEMID'
		});

		this.mappingStore.sort('name', 'ASC');

		this.redirectStore.add([{
			name: '_blank'
		}, {
			name: '_self'
		}, {
			name: '_parent'
		}, {
			name: '_top'
		}]);

		if (!this.messageBoxYesText) this.set('messageBoxYesText', this.localize('messageBoxYesText'));
		if (!this.messageBoxNoText) this.set('messageBoxNoText', this.localize('messageBoxNoText'));

		this.dependencyTargetStore.load();
		this.dependencyConditionStore.load();

		//Reload the runbook store because things might have changed
		this.rootVM.mainForm.runbookStore.load()
	},

	/**
	 * Determines if the dbAction combobox should be visible
	 */
	dbActionIsVisible$: function() {
		return this.operation == 'DB';
	},

	/**
	 * Determines if the sirAction combobox should be visible
	 */
	sirActionIsVisible$: function() {
		return this.operation == 'SIR';
	},

	/**
	 * Determines if the runbook combobox should be visible
	 */
	RUNBOOKIsVisible$: function() {
		return this.operation == 'RUNBOOK' || this.operation == 'EVENT';
	},

	/**
	 * Determines if the script combobox should be visible
	 */
	SCRIPTIsVisible$: function() {
		return this.operation == 'SCRIPT';
	},

	/**
	 * Button handler that forwards the addAction event to the parentVM to be handled properly
	 */
	addAction: function() {
		this.doClose();
		this.parentVM.addRealAction(this);
	},
	/**
	 * Button handler that just closes the window
	 */
	cancel: function() {
		this.doClose();
	},

	/**
	 * Determines if the addAction should be enabled because the form is valid
	 */
	addActionIsEnabled$: function() {
		return this.isValid;
	},

	/**
	 * Determines if the editAction should be enabled because the form is valid
	 */
	applyActionIsEnabled$: function() {
		return this.isValid;
	},

	/**
	 * Button handler that forwards the editAction event to the parentVM to be handled properly
	 */
	applyAction: function() {
		if (this.asynchronous === true) {
			this.set('TIMEOUT', -1);
		}
		else if (this.usingDefault) {
			this.set('TIMEOUT', '');
		}
		else {
			this.set('TIMEOUT', this.waitSeconds * 1000); //this.set('TIMEOUT', 0);
		}

		this.doClose();
		this.parentVM.editRealAction(this.asObject());
	},

	/**
	 * Determines if the operation is valid
	 */
	operationIsValid$: function() {
		if (!this.operation) return this.localize('actionOperationInvalid');
		//if(this.operation == 'DB' && this.alreadyHasDBOperation) return this.localize('actionOperationAlreadyDBInvalid');
		return true;
	},
	/**
	 * Determines if the dbAction is valid
	 */
	dbActionIsValid$: function() {
		if (this.operation == 'DB' && !this.dbAction) return this.localize('actionDbActionInvalid');
		return true;
	},
	/**
	 * Determines if the sirAction is valid
	 */
	sirActionIsValid$: function() {
		if (this.operation == 'SIR' && !this.sirAction) return this.localize('actionsirActionInvalid');
		return true;
	},
	/**
	 * Determines if the runbook is valid
	 */
	RUNBOOKIsValid$: function() {
		if ((this.operation == 'RUNBOOK' || this.operation == 'EVENT') && !this.RUNBOOK) return this.localize('actionRunbookInvalid');
		return true;
	},
	/**
	 * Determines if the script is valid
	 */
	SCRIPTIsValid$: function() {
		if (this.operation == 'SCRIPT' && !this.SCRIPT) return this.localize('actionScriptInvalid');
		return true;
	},

	when_operation_changes_default_problemId_and_load_problemId_store: {
		on: ['operationChanged'],
		action: function() {
			if (this.operation == 'RUNBOOK' || this.operation == 'ACTIONTASK') {
				this.set('PROBLEMID', 'NEW')
				this.problemIdStore.removeAll()
				this.problemIdStore.add([{
						name: this.localize('NEW'),
						value: 'NEW'
					}, {
						name: this.localize('LOOKUP'),
						value: 'LOOKUP'
					}, {
						name: this.localize('ACTIVE'),
						value: 'ACTIVE'
					},
					/*, {
				name: this.localize('NONE'),
				value: ''
			}, */
					{
						name: this.localize('MAPFROM'),
						value: 'MAPFROM'
					}
				])
			}
			if (this.operation == 'EVENT') {
				this.set('PROBLEMID', 'LOOKUP')
				this.problemIdStore.removeAll()
				this.problemIdStore.add([
					/*{
				name: this.localize('NEW'),
				value: 'NEW'
			},*/
					{
						name: this.localize('LOOKUP'),
						value: 'LOOKUP'
					},
					/*{
				name: this.localize('ACTIVE'),
				value: 'ACTIVE'
			}, {
				name: this.localize('NONE'),
				value: ''
			}, */
					{
						name: this.localize('MAPFROM'),
						value: 'MAPFROM'
					}
				])
			}
		}
	}
});
glu.defModel('RS.formbuilder.Dependency', {

	initialValue: '',
	initialActionOptions: '',

	id: '',

	action: '',
	actionDisplay$: function() {
		return this.localize(this.action);
	},
	actionOptions$: function() {
		return this.action == 'setBgColor' ? this.color : '';
	},
	color: '',
	colorIsVisible$: function() {
		return this.action == 'setBgColor';
	},
	condition: '',
	conditionDisplay$: function() {
		return this.localize(this.condition);
	},
	target: '',
	value$: function() {
		var val = this.stringValue;//'';
		// switch(this.targetType) {
		// case 'bool':
		// 	val = this.booleanValue;
		// 	break;
		// case 'number':
		// 	val = this.numberValue;
		// 	break;
		// case 'date':
		// 	val = this.dateValue;
		// 	break;
		// case 'string':
		// 	val = this.stringValue;
		// 	break;
		// }
		return val;
	},

	targetType$: function() {
		var target = this.target,
			type = '';
		if(target) {
			var control;
			this.rootVM.mainForm.controls.foreach(function(ctrl) {
				if(ctrl.inputName == target || ctrl.column == target) {
					control = ctrl;
					return false;
				}
			});
			type = this.getControlType(control);
		}

		return type;
	},

	getControlType: function(control) {
		var type = '';
		if(control) {
			switch(control.viewmodelName) {
			case 'CheckboxField':
				type = 'bool';
				break;
			case 'NumberField':
			case 'DecimalField':
				type = 'number';
				break;
			case 'DateField':
			case 'DateTimeField':
			case 'TimeField':
				type = 'date';
				break;
			default:
				type = 'string';
			}
		}
		return type;
	},

	stringValue: '',
	stringValueIsVisible$: function() {
		return true;//this.targetType == 'string';
	},
	// numberValue: '',
	// numberValueIsVisible$: function() {
	// 	return this.targetType == 'number';
	// },
	// dateValue: '',
	// dateValueIsVisible$: function() {
	// 	return this.targetType == 'date';
	// },
	// booleanValue: '',
	// booleanValueIsVisible$: function() {
	// 	return this.targetType == 'bool';
	// },

	stringValueIsEnabled$: function() {
		return this.condition !== "isEmpty" && this.condition !== "notEmpty";
	},

	isEdit: false,
	title$: function() {
		return this.isEdit ? this.localize('editDependencyTitle') : this.localize('addDependencyTitle');
	},

	fields: ['action', 'actionOptions', 'condition', 'target', 'value', 'id'],

	dependencyActionStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},

	dependencyConditionStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		listeners: {
			load: function() {
				var target = this.parentVM.target,
					type = '',
					conditions = [];
				this.removeAll();
				//Add in only the options that make sense for the target.  If there is no target, then display all of them
				var type = this.parentVM.targetType;

				if(target == 'activeTabIndex') isString = false;

				switch(type) {
				case 'number':
					this.add([{
						name: this.parentVM.localize('equals'),
						value: 'equals'
					}, {
						name: this.parentVM.localize('notequals'),
						value: 'notequals'
					}, {
						name: this.parentVM.localize('greaterThan'),
						value: 'greaterThan'
					}, {
						name: this.parentVM.localize('greaterThanOrEqual'),
						value: 'greaterThanOrEqual'
					}, {
						name: this.parentVM.localize('lessThan'),
						value: 'lessThan'
					}, {
						name: this.parentVM.localize('lessThanOrEqual'),
						value: 'lessThanOrEqual'
					}]);
					break;
				case 'date':
					this.add([{
						name: this.parentVM.localize('on'),
						value: 'on'
					}, {
						name: this.parentVM.localize('after'),
						value: 'after'
					}, {
						name: this.parentVM.localize('before'),
						value: 'before'
					}]);
					break;
				case 'bool':
					this.add([{
						name: this.parentVM.localize('equals'),
						value: 'equals'
					}, {
						name: this.parentVM.localize('notequals'),
						value: 'notequals'
					}]);
					break;
				case 'string':
				default:
					this.add([{
						name: this.parentVM.localize('equals'),
						value: 'equals'
					}, {
						name: this.parentVM.localize('notequals'),
						value: 'notequals'
					}, {
						name: this.parentVM.localize('Contains'),
						value: 'contains'
					}, {
						name: this.parentVM.localize('notcontains'),
						value: 'notcontains'
					}]);
					break;
				}

				this.add([{ 
					name: this.parentVM.localize('isEmpty'),
					value: 'isEmpty'
				}, { 
					name: this.parentVM.localize('notEmpty'),
					value: 'notEmpty'
				}]);
			}
		}
	},

	dependencyTargetStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		listeners: {
			load: function() {
				var fieldNames = this.rootVM.getFormFieldNames();
				Ext.each(fieldNames, function(name) {
					this.add({
						name: name,
						value: name
					})
				}, this);
				if(this.rootVM.mainForm.useTabs) this.add({
					name: 'activeTabIndex',
					value: 'activeTabIndex'
				});
				this.sort('name', 'ASC');
			}
		}
	},

	init: function() {
		this.dependencyActionStore.removeAll();
		this.dependencyActionStore.add([{
			name: this.localize('isVisible'),
			value: 'isVisible'
		}, {
			name: this.localize('isEnabled'),
			value: 'isEnabled'
		}, {
			name: this.localize('setBackgroundColor'),
			value: 'setBgColor'
		}]);

		this.dependencyConditionStore.removeAll();
		this.dependencyConditionStore.add([{
			name: this.parentVM.localize('equals'),
			value: 'equals'
		}, {
			name: this.parentVM.localize('notequals'),
			value: 'notequals'
		}, {
			name: this.parentVM.localize('Contains'),
			value: 'contains'
		}, {
			name: this.parentVM.localize('notcontains'),
			value: 'notcontains'
		}, {
			name: this.parentVM.localize('on'),
			value: 'on'
		}, {
			name: this.parentVM.localize('after'),
			value: 'after'
		}, {
			name: this.parentVM.localize('before'),
			value: 'before'
		}, {
			name: this.parentVM.localize('equals'),
			value: 'equals'
		}, {
			name: this.parentVM.localize('notequals'),
			value: 'notequals'
		}, {
			name: this.parentVM.localize('greaterThan'),
			value: 'greaterThan'
		}, {
			name: this.parentVM.localize('greaterThanOrEqual'),
			value: 'greaterThanOrEqual'
		}, {
			name: this.parentVM.localize('lessThan'),
			value: 'lessThan'
		}, {
			name: this.parentVM.localize('lessThanOrEqual'),
			value: 'lessThanOrEqual'
		},{ 
			name: this.parentVM.localize('isEmpty'),
			value: 'isEmpty'
		}, { 
			name: this.parentVM.localize('notEmpty'),
			value: 'notEmpty'
		}]);

		this.dependencyTargetStore.load();

		this.set('color', this.initialActionOptions);

		if(this.initialValue) {
			// switch(this.targetType) {
			// case 'string':
			// 	this.set('stringValue', this.initialValue);
			// 	break;
			// case 'number':
			// 	this.set('numberValue', this.initialValue);
			// 	break;
			// case 'date':
			// 	this.set('dateValue', this.initialValue);
			// 	break;
			// case 'bool':
			// 	this.set('booleanValue', this.initialValue);
			// 	break;
			// }
			this.set('stringValue', this.initialValue);
		}
	},

	applyDependency: function() {
		if(this.isEdit) this.parentVM.reallyEditDependency(this.asObject());
		else this.parentVM.reallyAddDependency(this.asObject());
		this.doClose();
	},
	cancel: function() {
		this.doClose();
	}
});
/**
 * Model definition for a Form
 * A form is the main component that we are trying to build with the form builder.  It contains all the controls, hiddenFields, and buttons that should comprise the form
 */
glu.defModel('RS.formbuilder.Form', {
    /**
     * Id of the form stored on the server
     */
    id: '',

    isAdmin$: function() {
        return this.parentIsAdmin();
    },
    parentIsAdmin : function(){
        return this.parentVM.isAdmin;
    },
    /**
     * Title of the form to display to the user
     */
    formTitle : 'UntitledForm',
    title: 'UntitledForm',
    name: 'UNTITLEDFORM',
    properName$: function() {
        return this.name.toUpperCase();
    },

    regAllowedCharacters: new RegExp(/[^A-Z|_|0-9]/g),

    nameIsValid$: function() {       
        if (this.name.length > 100) {
            return this.localize('formNameTooLongInvalidText');
        }

        if (this.regAllowedCharacters.test(this.properName)) {
            return this.localize('formNameInvalidText');
        }

        if (this.parentVM.id) {
            return true;
        }

        if (this.parentVM.existingFormNames.findRecord('name', this.properName, 0, false, false, true)) {
            return this.localize('nameInvalidText', this.properName);
        }

        return true;
    },

    when_name_changes_change_title_to_match: {
        on: ['nameChanged'],
        action: function() {
            if (!this.titleIsChanged) {
                this.set('title', this.properName);
            }
        }
    },
   when_title_changes_change_title_to_match: {
        on: ['titleChanged'],
        action: function() {            
            this.set('formTitle', Ext.util.Format.htmlEncode(this.title));           
        }
    },

    titleOnFocus: '',
    titleIsChanged: false,

    focusTitle: function (field) {
        this.titleOnFocus = field.target.value;
    },
    
    blurTitle: function (field) {
        this.titleIsChanged = this.titleIsChanged || this.titleOnFocus !== field.target.value;
    },

    formWindowTitle: '',

    /**
     * Validation formula to make sure that the title isn't a duplicate
     */
    titleIsValid$: function() {
        if (this.title.length > 500) {
            return this.localize('titleTooLongInvalidText');
        }

        return true;
    },
    originalWindowTitle: '',
    windowTitle$: function() {
        var title = this.properName;
        clientVM.setWindowTitle(title)
    },
    /**
     * Default label alignment for controls that are added to the form
     */
    labelAlign: 'left',
    /**
     * The dbTable to either create or update when the user saves the form
     */
    dbTable: '',
    dbTableIsValid$: function() {
        return (/^(?=[a-zA-Z])[a-zA-Z0-9_]{1,30}$/.test(this.dbTable)) || !this.dbTable ? true : this.localize('invalidDBTableName')
    },

    /**
     * Name of the wiki document to create with this form when the user saves it
     */
    wikiName: '',
    wikiNameIsValid$: function() {
        if (!this.wikiName) return true;
        return /[\w]+[.][\w]+/g.test(this.wikiName) ? true : this.localize('wikiNameNamespace');
    },

    showFieldSettings: 0,

    /**
     * Store to keep track of the valid value list of wiki names in the system
     */
    wikiNames: {
        mtype: 'store',
        model: 'RS.formbuilder.model.WikiName',
        proxy: {
            url: '/resolve/service/form/getwikinames',
            type: 'ajax',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },

    /**
     * List of controls that are on the form.  We use activator list here to make sure that when we have focus on a control we can display its detail view to edit the control
     */
    controls: {
        mtype: 'activatorlist',
        focusProperty: 'detailControl',
        autoParent: true
    },

    /**
     * List of buttons that are on the form.
     */
    formButtons: {
        mtype: 'list',
        autoParent: true
    },

    accessRights: {
        mtype: 'store',
        fields: ['name', 'value', {
            name: 'admin',
            type: 'bool'
        }, {
            name: 'edit',
            type: 'bool'
        }, {
            name: 'view',
            type: 'bool'
        }],
        proxy: {
            type: 'memory',
            reader: {
                type: 'json'
            }
        }
    },

    /**
     * Store for the buttons that displays as a tree grid with nested buttons->actions
     */
    treeStore: {
        mtype: 'treestore',
        mixins: [{
            type: 'listtreestoreadapter',
            attachTo: 'formButtons'
        }],
        fields: ['id', {
            name: 'operationName',
            type: 'string'
        }, {
            name: 'value',
            type: 'string'
        }, {
            name: 'type',
            type: 'string'
        }, {
            name: 'iconCls',
            type: 'string'
        }, {
            name: 'leaf',
            type: 'boolean'
        }],
        proxy: {
            type: 'memory',
            reader: {
                type: 'json'
            }
        },
        root: {
            expanded: true,
            operation: 'rootNode'
        }
    },

    /**
     * Store for the hidden fields that are on the form
     */
    hiddenFields: {
        mtype: 'store',
        fields: ['name', 'value'],
        data: []
    },

    useTabs: false,
    isWizard: false,

    tabPanel: {
        mtype: 'TabPanel'
    },

    when_useTabs_changes_add_or_remove_tabpanel: {
        on: ['useTabsChanged'],
        action: function() {
            if (this.useTabs) {
                if (this.controls.getAt(0) != this.tabPanel) {
                    this.tabPanel.clearControls();
                    if (this.tabPanel.tabs.length === 0) {
                        // REG-324 Add one tab to get the ball rolling
                        this.tabPanel.addTab();
                    }
                    this.tabPanel.set('selectedTab', this.tabPanel.tabs.getAt(0));
                    this.controls.insert(0, this.tabPanel);
                    this.tabPanel.clearControls();
                    this.tabPanel.syncCurrentTab();
                }
            } else {
                this.tabPanel.syncCurrentTab();
                var tab1HasControls = false,
                    tab2HasControls = false,
                    i;

                for (i = 0; i < this.tabPanel.tabs.length; i++) {
                    if (this.tabPanel.tabs.getAt(i).controls.length > 0) {
                        if (!tab1HasControls) {
                            tab1HasControls = true;
                        } else {
                            tab2HasControls = true;  
                        } 
                    }
                }

                if (tab1HasControls && tab2HasControls) {
                    this.confirmTabPanelRemoval();
                } else {
                    this.controls.removeAll();
                    //restore controls from the one tab that does have controls
                    for (i = 0; i < this.tabPanel.tabs.length; i++) {
                        if (this.tabPanel.tabs.getAt(i).controls.length > 0) {
                            Ext.each(this.tabPanel.tabs.getAt(i).controls, function(control) {
                                this.controls.add(control);
                            }, this);
                        }
                    }
                }
            }
        }
    },

    confirmTabPanelRemoval: function() {
        this.message({
            title: this.localize('confirmTabPanelRemoveTitle'),
            msg: this.localize('confirmTabPanelRemoveBody'),
            buttons: Ext.MessageBox.YESNOCANCEL,
            buttonText: {
                yes: this.localize('mergeTabPanelControls'),
                no: this.localize('removeTabPanelControls')
            },
            fn: this.confirmTabPanelRemovalResponse,
            scope: this
        });
    },

    confirmTabPanelRemovalResponse: function(btn) {
        if (btn === 'cancel') {
            this.set('useTabs', true);
        } else if (btn === 'yes') {
            this.mergeTabs();
            this.controls.remove(this.tabPanel);
        } else if (btn === 'no') {
            this.clearTabs();
            this.controls.remove(this.tabPanel);
        }        
    },

    mergeTabs: function() {
        this.tabPanel.syncCurrentTab();
        var i, j, tab, controls = [];

        for (i = 0; i < this.tabPanel.tabs.length; i++) {
            tab = this.tabPanel.tabs.getAt(i);
            Ext.each(tab.controls, function(control) {
                controls.push(control);
            });
            tab.controls = [];
        }

        this.controls.removeAll();
        Ext.each(controls, function(control) {
            this.controls.add(control);
        }, this);
    },
    clearTabs: function() {
        this.tabPanel.tabs.foreach(function(tab) {
            tab.controls = []
        });
        this.controls.removeAll();
    },

    addTab: function() {
        var tabSelection = this.tabsSelections[0],
            tabSelectionIndex = this.tabPanel.tabStore.indexOf(tabSelection);
        this.tabPanel.addTab({}, tabSelectionIndex);
        this.tabPanel.set('selectedTab', this.tabPanel.tabs.getAt(this.tabPanel.tabs.length - 1));
    },
    when_tabsSelections_change_select_newTab: {
        on: ['tabsSelectionsChanged'],
        action: function() {
            if (this.tabsSelections.length === 0 && this.tabPanel.tabStore.getCount() > 0) {
                this.set('tabsSelections', [this.tabPanel.tabStore.getAt(0)]);
            } else {
                var tabIndex = this.tabPanel.tabStore.indexOf(this.tabsSelections[0]);
                this.tabPanel.set('selectedTab', this.tabPanel.tabs.getAt(tabIndex));
            }
        }
    },
    tabsSelections: [],
    editTabIsEnabled$: function() {
        return this.tabsSelections.length == 1
    },
    editTab: function() {
        var tabSelection = this.tabsSelections[0],
            selectedTab, tabSelectionIndex = this.tabPanel.tabStore.indexOf(tabSelection);
        if (tabSelectionIndex > -1) {
            selectedTab = this.tabPanel.tabs.getAt(tabSelectionIndex);
            this.tabPanel.syncCurrentTab();
            this.tabPanel.set('selectedTab', selectedTab);
            this.open(Ext.applyIf({
                mtype: 'Tab'
            }, selectedTab.asObject()), 'detail');
        }
    },
    editRealTab: function(tabConfig) {
        var tabSelection = this.tabsSelections[0],
            tabSelectionIndex = this.tabPanel.tabStore.indexOf(tabSelection),
            selectedTab;

        if (tabSelectionIndex > -1) {
            selectedTab = this.tabPanel.tabs.getAt(tabSelectionIndex);
            selectedTab.loadData(tabConfig);
            selectedTab.parseDependencyControlConversionProperties(tabConfig.dependenciesJson);
        }
    },
    removeTabIsEnabled$: function() {
        return this.tabsSelections.length == 1;
    },
    removeTab: function() {
        var tabSelection = this.tabsSelections[0],
            selectedTab, tabSelectionIndex = this.tabPanel.tabStore.indexOf(tabSelection);

        if (tabSelectionIndex > -1) {
            selectedTab = this.tabPanel.tabs.getAt(tabSelectionIndex);
            this.tabPanel.syncCurrentTab();
            this.tabPanel.set('selectedTab', selectedTab);
            if (this.tabPanel.selectedTab.controls.length > 0) {
                this.confirmTabRemoval();
            } else {
                this.tabPanel.tabs.remove(this.tabPanel.selectedTab);
            }
        }
    },
    confirmTabRemoval: function() {
        this.message({
            title: this.localize('confirmTabRemoveTitle'),
            msg: this.localize('confirmTabRemoveBody'),
            buttons: Ext.MessageBox.YESNOCANCEL,
            buttonText: {
                yes: this.localize('mergeTabControls'),
                no: this.localize('removeTabControls')
            },
            fn: this.confirmTabRemovalResponse,
            scope: this
        });
    },

    confirmTabRemovalResponse: function(btn) {
        if (btn === 'yes') {
            this.mergeAndRemoveTab();
        } else if (btn === 'no') {
            this.noMergeRemoveTab();
        }
    },

    noMergeRemoveTab: function() {
        this.tabPanel.tabs.remove(this.tabPanel.selectedTab);
    },

    mergeAndRemoveTab: function() {
        var tab, tabIndex, i, oldSelectedTab = this.tabPanel.selectedTab;
        for (i = 0; i < this.tabPanel.tabs.length; i++) {
            if (this.tabPanel.tabs.getAt(i) != this.tabPanel.selectedTab) {
                tab = this.tabPanel.tabs.getAt(i);
                tabIndex = i;
            }
        }
        var controls = this.tabPanel.selectedTab.controls;
        this.tabPanel.set('selectedTab', tab);
        Ext.each(controls, function(control) {
            this.controls.add(control);
        }, this)

        this.tabPanel.tabs.remove(oldSelectedTab);
    },

    /**
     * Flag internally used to determine if the control should be re-selected because an operation is rebuilding it (bulk)
     */
    reselectControlIndex: -1,
    /**
     * Flag internally used to determine if the control should be re-selected because of an operation rebuilding it (bulk)
     */
    reselectControlCount: 0,

    /**
     * The currently selected control on the form or -1 if no selection
     */
    detailControl: -1,

    /**
     * Determines if the card that says 'please add a field' should be displayed
     */
    showNoFieldsCard$: function() {
        return this.controls.length == 0 ? 0 : 1
    },

    /**
     * Determines if the card that says 'please select a field' should be displayed
     */
    showSelectFieldCard$: function() {
        return this.detailControl === -1 || !Ext.isDefined(this.detailControl) ? 0 : 1
    },

    /**
     * Store for the different label alignment options available to the user (left, right, top)
     */
    labelAlignOptionsList: {
        mtype: 'store',
        fields: ['text', 'value'],
        data: [{
            text: 'Left',
            value: 'left'
        }, {
            text: 'Right',
            value: 'right'
        }, {
            text: 'Top',
            value: 'top'
        }]
    },

    /**
     * Store of the custom tables currently in the system that the user can edit or link this form to
     */
    customTablesList: {
        mtype: 'store',
        model: 'RS.formbuilder.model.CustomTableDTO',
        data: [],
        proxy: {
            type: 'ajax',
            url: '/resolve/service/form/getcustomtables',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },

    /**
     * Store of the selected custom table's columns that the user can pick from to link the form to
     */
    columnStore: {
        mtype: 'store',
        fields: ['name', 'dbcolumnId', 'displayName', 'dbtype', 'uiType', 'integerMinValue', 'integerMaxValue', 'decimalMinValue', 'decimalMaxValue', 'stringMinLength', 'stringMaxLength', 'uiStringMaxLength', 'selectValues', 'choiceValues', 'referenceTable', 'referenceDisplayColumn'],
        data: [],
        proxy: {
            type: 'ajax',
            url: '/resolve/service/form/getcustomtablecolumns',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },
    /**
     * Store to display the runbooks for the user to choose from
     */
    runbookStore: {
        mtype: 'store',
        fields: ['name', 'value'],
        proxy: {
            type: 'ajax',
            url: '/resolve/service/form/getrunbooks',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },
    /**
     * Store to display the scripts for the user to choose from
     */
    scriptStore: {
        mtype: 'store',
        fields: ['name', 'value'],
        proxy: {
            type: 'ajax',
            url: '/resolve/service/form/getscripts',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },

    actionTaskStore: {
        mtype: 'store',
        fields: ['name', 'value'],
        proxy: {
            type: 'ajax',
            url: '/resolve/service/form/getactiontasks',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },

    /**
     * Reactor for when the dbTable choice is changed, then we need to reload the columns store because the columns won't be correct anymore with the new table
     */
    when_dbtable_changes_reload_store_or_fix_name: {
        on: ['dbTableChanged'],
        action: function() {
            var val = this.dbTable,
                found = false;

            this.customTablesList.each(function(table) {
                if (table.data.umodelName == val) {
                    found = true;
                    return false;
                }
            });

            if (found) {
                this.columnStore.load({
                    callback: this.populateModelType,
                    scope: this
                });
            } else {
                this.columnStore.removeAll();
            }
        }
    },

    populateModelType: function(records, operation, success) {
        if (this.detailControl >= 0 || Ext.isObject(this.detailControl)) {
            this.columnStore.insert(0, {
                name: 'newColumn',
                displayName: this.localize('newColumn')
            });
            var self = this;
            this.columnStore.each(function(record) {
                record.data.modelType = function() {
                    if (self.detailControl.uiType === 'SectionContainerField') {
                        return self.detailControl.selectedColumn.detailControl.dbColumnType;
                    }

                    return self.detailControl.dbColumnType;
                }
            }, this)
            this.columnStore.fireEvent('refresh', this.columnStore);
        } else {
            this.columnStore.insert(0, {
                name: 'newColumn',
                displayName: this.localize('newColumn')
            });
        }
        this.fireEvent('afterColumnStoreLoaded');
    },

    init: function() {
        //Make sure the tabPanel is initialized to get translated tabs and at least one tab on the tabPanel
        this.tabPanel.init();

        this.labelAlignOptionsList.each(function(option) {
            option.set('text', this.localize(option.get('text')));
        }, this);

        //this is the workaround for extjs 4.2.0 bugs:http://www.sencha.com/forum/showthread.php?256359-Combobox-with-autoLoad-store-applies-Filter
        this.customTablesList.on('beforeload', function() {
            this.customTablesList.clearFilter();
        }, this);

        //Load the custom tables from the server
        this.customTablesList.load({
            scope: this,
            callback: this.customTablesLoaded
        });

        this.columnStore.on('beforeload', function(store, operation, eOpts) {
            operation.params = operation.params || {};
            Ext.apply(operation.params, {
                tableName: this.dbTable
            });
        }, this);

        this.runbookStore.on('load', function(store, records) {
            store.insert(0, {
                name: this.localize('currentWiki'),
                value: '${CURRENT_WIKI}'
            })
        }, this)

        //Don't need to load this because its not used anywhere now
        this.runbookStore.load();
        this.scriptStore.load();
        this.actionTaskStore.load();
        this.wikiNames.load();

        this.set('name', this.localize('UNTITLEDFORM'));
        this.set('formTitle', this.localize('UntitledForm'));

        if (this.dbTable) this.columnStore.load();
        this.formButtons.on('insertchild', this.rootVM.enforcingSIRContext.bind(this.rootVM));
        this.formButtons.on('editedchild', this.rootVM.enforcingSIRContext.bind(this.rootVM));
        this.formButtons.on('removechild', this.rootVM.enforcingSIRContext.bind(this.rootVM)); 
        this.formButtons.on('buttonremoved', this.rootVM.enforcingSIRContext.bind(this.rootVM)); 


    },

    customTablesLoaded: function() {
        // debugger;
        // this.customTablesDoneLoading = true;
        if (this.dbTable) {
            this.columnStore.load();
        }
    },

    /**
     * The initial control length.  Primarily used to determine dirty-ness from an edit of the form
     */
    initialControlLength: 0,

    /**
     * Handler for the user dropping a control on the form.  Adds the control if a button was dropped, or reorders the fields appropriately
     * @param {Object} dropIndex
     * @param {Object} sourceIndex
     * @param {Object} controlConfig
     */
    dropControl: function(dropIndex, sourceIndex, controlConfig) {
        if (dropIndex >= 0 && sourceIndex >= 0) {
            this.controls.insert(sourceIndex < dropIndex ? dropIndex - 1 : dropIndex, this.controls.removeAt(sourceIndex));
        } else if (dropIndex >= 0 && controlConfig) {
            this.insertControl(dropIndex, controlConfig);
        }
    },

    /**
     * Handler that changes the fieldLabel of the selected control
     * @param {Object} newFieldLabelValue
     */
    editFieldLabel: function(newFieldLabelValue) {
        if (this.detailControl) {
            this.detailControl.set('fieldLabel', newFieldLabelValue);
        }
    },

    /**
     * Handler that changes the title for the form
     * @param {Object} newTitle
     */
    editTitle: function(newTitle) {
        this.set('title', Ext.util.Format.htmlEncode(newTitle));
    },

    /**
     * Inserts a control at the provided index
     * @param {Object} index
     * @param {Object} controlConfig
     */
    insertControl: function(index, controlConfig) {
        var control;

        if (Ext.isString(controlConfig)) {
            control = this.model({
                mtype: controlConfig,
                labelAlign: this.labelAlign
            });
        } else {
            control = this.model(controlConfig);
        }

        //Disable the file upload button if this is a file upload field
        if (control.viewmodelName === 'FileUploadField') {
            this.rootVM.set('fileUploadIsEnabled', false);
        }

        this.controls.insert(index, control);
        this.selectControl(control);
        return control;
    },
    /**
     * Adds a control to the end of the form
     * @param {Object} controlConfig
     */
    addControl: function(controlConfig) {
        var index = this.controls.indexOf(this.detailControl);
        
        if (index > -1) {
            index++;
        } else {
            index = this.controls.length;
        }

        this.insertControl(index, controlConfig);
    },
    addRealControl: function(control) {
        this.controls.insert(this.controls.length, control);
    },
    /**
     * Removes the control from the form
     * @param {Object} ctrl
     */
    removeControl: function(ctrl) {
        var currentIndex = this.controls.indexOf(ctrl);
        
        if (ctrl.viewmodelName === 'FileUploadField') {
            this.rootVM.set('fileUploadIsEnabled', true);
        }

        if (ctrl.viewmodelName === 'TabContainerField') {
            ctrl.referenceTables.foreach(function(table) {
                if (table.rsType === 'FileManager') { 
                    this.rootVM.set('fileUploadIsEnabled', true);
                }
            });
        }

        if (ctrl.viewmodelName === 'SectionContainerField') {
            ctrl.columns.each(function(c) {
                c.controls.each(function(subCtrl) {
                    if (subCtrl.viewmodelName === 'FileUploadField') {
                        this.rootVM.set('fileUploadIsEnabled', true);
                    }
                })
            });
        }

        this.controls.removeAt(currentIndex);
    },
    /**
     * Duplicates the provided control and puts the copy below the existing control
     * @param {Object} ctrl
     */
    duplicateControl: function(ctrl) {
        this.insertControl(this.controls.indexOf(ctrl) + 1, this.getCopy(ctrl));
    },
    /**
     * Rebuilds the control in the event that something has changed that cannot be auto-magically changed due to limitations in the extjs code
     * @param {Object} ctrl
     */
    duplicateAndRemoveControl: function(ctrl) {
        this.duplicateControl(ctrl);
        this.removeControl(ctrl);
    },
    /**
     * Rebuilds the control in the event that something has changed that cannot be auto-magically changed due to limitations in the extjs code
     * @param {Object} ctrl
     */
    rebuildControl: function(ctrl, reselectControl) {
        var me = this;
        var currentIndex = this.controls.indexOf(ctrl);
        var control = this.controls.getAt(currentIndex);
        
        if (control) {
            //Delay to make sure all internal event (ExtJs Events) of this field is complete before remove it.
            setTimeout(function(){
                me.controls.removeAt(currentIndex);
                me.controls.insert(currentIndex, control);
                var isReselect = me.reselectControlIndex === currentIndex || me.reselectControlCount <= 0;
                if (currentIndex !== -1 && isReselect && reselectControl) {
                    me.selectControl(me.controls.getAt(currentIndex));
                    me.reselectControlCount--;
                }
            },50);           
        }
    },
    /**
     * Returns the copied control for duplication purposes
     * @param {Object} ctrl
     */
    getCopy: function(ctrl) {
        var formcontrol = RS.formbuilder.viewmodels.FormControl;
        var config = {};
        this.copyTemplate(formcontrol, ctrl, config);
        //TODO: add additional templates here as they become available
        config.mtype = ctrl.viewmodelName;
        return config;
    },
    /**
     * Copies the control key by key
     * @param {Object} viewmodel
     * @param {Object} ctrl
     * @param {Object} config
     */
    copyTemplate: function(viewmodel, ctrl, config) {
        var key;
        for (key in viewmodel) {
            if (!Ext.isFunction(ctrl[key]) && !Ext.isObject(ctrl[key])) {
                config[key] = Ext.clone(ctrl[key]);
            }
        }
    },
    /**
     * Programatically selects the provided control
     * @param {Object} ctrl
     */
    selectControl: function(ctrl) {
        this.set('detailControl', ctrl);
    },

    when_detailControl_changes_ensure_field_settings_are_shown: {
        on: ['detailControlChanged'],
        action: function() {
            this.set('showFieldSettings', 1);

            if (this.detailControl) {
                this.columnStore.each(function(record) {
                    var self = this;
                    record.data.modelType = function() {
                        var type = self.detailControl.dbColumnType;

                        if (self.detailControl.uiType === 'SectionContainerField') {
                            type = self.detailControl.selectedColumn.detailControl.dbColumnType;
                        }

                        return type;
                    }
                }, this);
            }
        }
    },
    /**
     * Determines if the form is valid
     */
    isValid$: function() {
        return true;
    },
    /**
     * Determines if the form is dirty
     */
    // isDirty$: function() {
    //  return this.controls.length != this.initialControlLength;
    // },
    /**
     * Reactor for when the label alignment changes.  We have to go through and change the existing label alignments on the already existing controls in the form
     */
    whenLabelAlignChanges: {
        on: ['labelAlignChanged'],
        action: function() {
            this.reselectControlIndex = this.controls.indexOf(this.detailControl);
            this.reselectControlCount = this.controls.length;
            this.controls.foreach(function(control) {
                control.set('labelAlign', this.labelAlign);
            }, this);
        }
    },
    /**
     * Array of the currently selected hidden field on the grid
     */
    hiddenFieldsSelections: [],
    /**
     * Determines whether the remove button for the hidden fields is enabled because a selection has been made on the hidden fields
     */
    removeHiddenFieldIsEnabled: {
        on: ['hiddenFieldsSelectionsChanged'],
        formula: function() {
            return this.hiddenFieldsSelections.length > 0;
        }
    },
    /**
     * Adds a hidden field to the form
     */
    addHiddenField: function() {
        this.hiddenFields.add({
            name: this.localize('Untitled'),
            value: ''
        });
    },
    /**
     * Removes the selected hidden field from the form
     */
    removeHiddenField: function() {
        this.hiddenFields.remove(this.hiddenFieldsSelections);
    },

    actionItemDoubleClick: function(record, item, index, e, eOpts) {
        if (record.isLeaf()) {
            this.editAction();
        } else {
            this.editButton();
        }
    },
    /**
     * Adds a button to the form
     */
    addButton: function(type) {
        var buttonIndex = this.getSelectedButtonIndex();
        
        if (buttonIndex === -1) {
            buttonIndex = this.formButtons.length;
        } else {
            buttonIndex++;
        }

        this.formButtons.insert(buttonIndex, this.model({
            mtype: 'ButtonControl',
            type: Ext.isString(type) ? type : 'button'
        }));

        this.set('formButtonsSelections', [this.treeStore.getRootNode().getChildAt(buttonIndex)]);
    },
    addNewButton: function() {
        this.addButton();
    },
    addSeparator: function() {
        this.addButton('separator')
    },
    addSpacer: function() {
        this.addButton('spacer')
    },
    addRightJustify: function() {
        this.addButton('rightJustify')
    },
    editButton: function() {
        var buttonIndex = this.getSelectedButtonIndex();
        var button = this.formButtons.getAt(buttonIndex);
        this.open(Ext.applyIf({
            mtype: 'ButtonControl',
            type: button.type
        }, button.asObject()), 'detail');
    },
    editRealButton: function(buttonProperties) {
        var buttonIndex = this.getSelectedButtonIndex(),
            button = this.formButtons.getAt(buttonIndex);
        button.loadData(buttonProperties);
        button.parseDependencyControlConversionProperties(buttonProperties.dependenciesJson);
        this.formButtons.fireEvent('edited', button, buttonIndex);
    },
    editButtonIsEnabled$: function() {
        return this.buttonSelected;
    },
    /**
     * Deletes the selected button from the form
     */
    removeButton: function() {
        this.formButtons.removeAt(this.treeStore.getRootNode().indexOf(this.formButtonsSelections[0]));
        this.formButtons.fireEvent('buttonremoved');
        this.set('formButtonsSelections', []);
    },

    /**
     * Array of the currently selected button/action in the treegrid
     */
    formButtonsSelections: [],
    /**
     * Determines if a button is currently selected
     */
    buttonSelected$: function() {
        return this.formButtonsSelections.length == 1 && this.formButtonsSelections[0].data.type != 'action';
    },
    /**
     * Determines if the delete button should be enabled
     */
    removeButtonIsEnabled$: function() {
        return this.buttonSelected;
    },
    /**
     * Determines if the add action button should be enabled
     */
    addActionIsEnabled$: function() {
        return this.formButtonsSelections.length == 1 && this.formButtonsSelections[0].data.type != 'rightJustify' && this.formButtonsSelections[0].data.type != 'separator' && this.formButtonsSelections[0].data.type != 'spacer';
    },
    /**
     * Determines if an action in a button is currently selected
     */
    actionSelected$: function() {
        return this.formButtonsSelections.length == 1 && this.formButtonsSelections[0].data.type == 'action';
    },
    /**
     * Determines if the edit action button should be enabled
     */
    editActionIsEnabled$: function() {
        return this.actionSelected;
    },
    /**
     * Determines if the delete action button should be enabled
     */
    removeActionIsEnabled$: function() {
        return this.actionSelected;
    },
    getSelectedButtonIndex: function() {
        return this.getButtonIndex(this.formButtonsSelections[0]);
    },
    getButtonIndex: function(node) {
        if (!node) return -1;
        if (this.formButtonsSelections.length == 1 && node.data.type == 'action') node = node.parentNode;
        var selectionIndex = node.getOwnerTree().getRootNode().indexOf(node);
        return selectionIndex;
    },
    /**
     * Displays a dialog to select the action properties before adding the action
     */
    addAction: function() {
        //determine if there are any db actions already to set on the action
        var buttonIndex = this.getSelectedButtonIndex(),
            alreadyHasDBOperation = false;
        this.formButtons.getAt(buttonIndex).actions.foreach(function(action) {
            if (action.operation.toLowerCase() == 'db') alreadyHasDBOperation = true;
        });
        this.open({
            mtype: 'Action',
            parentVM: this,
            alreadyHasDBOperation: alreadyHasDBOperation,
            id: Ext.id()
        });
    },
    /**
     * Actually adds the configured action to the selected button (unlike addAction which opens the window)
     * @param {Object} action
     */
    addRealAction: function(action) {
        var selection = this.formButtonsSelections[0],
            parent = selection.parentNode,
            childIndex = parent.indexOf(selection),
            buttonIndex = this.getSelectedButtonIndex(),
            insertIndex = childIndex + 1;

        action.unParent();
        
        if (selection.data.type !== 'action') {
            insertIndex = 0;
        }

        this.formButtons.getAt(buttonIndex).actions.insert(insertIndex, action);
        this.formButtons.fireEvent('insertchild', action, buttonIndex, insertIndex);
        this.ensureHiddenActionFields(action);
        this.set('formButtonsSelections', [this.treeStore.getRootNode().getChildAt(buttonIndex).getChildAt(insertIndex)]);
    },
    ensureHiddenActionFields: function(action) {
        //If the action is an runbook or event, make sure the optional hidden fields are automatically added to the hidden fields if they don't already exist
        if (action.operation === 'EVENT') {
            var hiddenParams = ['REFERENCE', 'ALERTID', 'CORRELATIONID', 'EVENT_REFERENCE'];

            for (var i = 0; i < hiddenParams.length; i++) {
                var found = false;

                this.hiddenFields.each(function(hidden) {
                    if (hidden.data.name === hiddenParams[i]) {
                        found = true;
                        return false;
                    }
                })

                if (!found) this.hiddenFields.add({
                    name: hiddenParams[i],
                    value: ''
                });                
            }
        }
    },
    /**
     * Displays a dialog to edit the selected action properties
     */
    editAction: function() {
        var selection = this.formButtonsSelections[0],
            parent = selection.parentNode,
            childIndex = parent.indexOf(selection),
            parentIndex = this.getSelectedButtonIndex(),
            action = this.formButtons.getAt(parentIndex).actions.getAt(childIndex),
            alreadyHasDBOperation = false;
        this.formButtons.getAt(parentIndex).actions.foreach(function(a) {
            if (a != action && a.operation.toLowerCase() == 'db') alreadyHasDBOperation = true;
        });
        this.open(Ext.applyIf({
            mtype: 'Action',
            isNew: false,
            alreadyHasDBOperation: alreadyHasDBOperation
        }, action.asObject()));
    },
    /**
     * Actually edits the selected action (unlike applyAction which opens the window)
     * @param {Object} actionProperties
     */
    editRealAction: function(actionProperties) {
        var selection = this.formButtonsSelections[0],
            parent = selection.parentNode,
            childIndex = parent.indexOf(selection),
            parentIndex = this.getSelectedButtonIndex(),
            action = this.formButtons.getAt(parentIndex).actions.getAt(childIndex);
        action.loadData(actionProperties);
        this.formButtons.fireEvent('editedchild', parentIndex, selection, action);
        this.set('formButtonsSelections', []);
        this.ensureHiddenActionFields(actionProperties);
    },
    /**
     * Deletes the currently selected action from the button
     * @param {Object} action
     */
    removeAction: function(action) {
        var selection = this.formButtonsSelections[0],
            parent = selection.parentNode,
            childIndex = parent.indexOf(selection),
            parentIndex = this.getSelectedButtonIndex();
        this.formButtons.getAt(parentIndex).actions.removeAt(childIndex);
        this.formButtons.fireEvent('removechild', parentIndex, selection);
        this.set('formButtonsSelections', []);
    },

    accessRightsSelections: [],
    addAccessRight: function() {
        var rights = [];
        this.accessRights.each(function(right) {
            rights.push(right.data);
        });
        this.open({
            mtype: 'AccessRightsPicker',
            currentRights: rights
        });
    },
    
    addRealAccessRights: function(accessRights) {
        Ext.each(accessRights, function(accessRight) {
            this.accessRights.add(Ext.applyIf(accessRight.data, {
                view: true,
                edit: true,
                admin: true
            }));
        }, this);
    },
    
    removeAccessRightIsEnabled: {
        on: ['accessRightsSelectionsChanged'],
        formula: function() {
            return this.accessRightsSelections.length > 0;
        }
    },
    
    removeAccessRight: function() {
        for (var i = 0; i < this.accessRightsSelections.length; i++) {
            this.accessRights.remove(this.accessRightsSelections[i]);
        }
    },
    
    dropButton: function(node, data, overModel, dropPosition, eOpts) {
        // console.log('dropPosition is: ' + dropPosition + ' dropnode is ' + data.records[0].data.operationName + ' overnode is ' + overModel.data.operationName);
        var rootNode = this.treeStore.getRootNode(), buttonConfigs = [];

        //First, calculate the order that the form buttons should be in based on the tree's new order after the drop of the button
        rootNode.eachChild(function(child) {
            var index = this.getButtonIndexFromNode(child);
            if (index > -1) buttonConfigs.push(this.formButtons.getAt(index).getButtonControlConversionProperties());
        }, this);

        //then remove all the buttons from the form
        this.formButtons.removeAll();

        //then deserialize all the buttons which will re-add them in the proper order
        for (var i = 1; i < buttonConfigs.length; i++) {			
			buttonConfigs[i].orderNumber = i;
            this.parentVM.deserializeButton(buttonConfigs[i]);
        }
        //Make sure to clear out any existing formButtonsSelections that might have been there
        this.set('formButtonsSelections', []);
    },

    dropAction: function(node, data, overModel, dropPosition, eOpts) {
        // console.log('dropPosition is: ' + dropPosition + ' dropnode is ' + data.records[0].data.operationName + ' overnode is ' + overModel.data.operationName);
        //cases:
        //1. Dropping an action onto a new button
        //2. Dropping an action before or after an existing action
        var dragActionIndex = data.records[0].parentNode.indexOf(data.records[0]),
            overButtonIndex = this.getButtonIndex(overModel),
            dragButtonIndex = this.getButtonIndex(data.records[0]),
            overButton = this.formButtons.getAt(overButtonIndex),
            dragButton = this.formButtons.getAt(dragButtonIndex),
            overActionIndex = overModel.data.type == 'action' ? overModel.parentNode.indexOf(overModel) : -1,
            action = dragButton.actions.removeAt(dragActionIndex);

        action.unParent();

        if (dropPosition == 'append') overButton.actions.add(action);
        else overButton.actions.insert(overActionIndex, action);
    },
   
    getButtonIndexFromNode: function(node) {
        var id = node.internalId;
        var n = this.formButtons._private.objs.length;

        for (var i = 0; i < n; i++) {
            if (this.formButtons._private.objs[i].id === id) {
                return i;
            }
        }

        return -1;
    }
});
glu.defModel('RS.formbuilder.BaseControl', {
	unsupportedField: false,
	viewmodelTranslatedName$: function() {
		return this.localize(this.viewmodelName);
	},

	/**
	 * Button Handler that forwards the event to the parentVM and removes this control from the form
	 */
	removeControl: function() {
		this.parentVM.removeControl(this);
	},
	/**
	 * Button Handler that forwards the event to the parentVM and duplicates this control on the form
	 */
	duplicateControl: function() {
		this.parentVM.duplicateControl(this);
	},
	/**
	 * Event Handler that forwards the event to the parentVM and selects this control on the form
	 */
	selectControl: function() {
		this.parentVM.selectControl(this);
	}
});
/**
 * Mixin definition for a FormControl
 * A form control is the base of all the controls.  It is a mixin, and as such its attributes will get added to all controls
 * It provides basic functionality for each field like fieldLabel, required, defaultValues, etc.
 */
glu.defModel('RS.formbuilder.FormControl', {
	/**
	 * Internal id of the control, this is not persisted to the backend
	 */
	id: '',
	/**
	 * The default value to show to the user when the control initially loads
	 */
	defaultValueInput: '',
	defaultValue: '',
	when_default_value_change : {
		on : ['defaultValueInputChanged'],
		action : function(val){
			this.set('defaultValue',/^[a-zA-Z0-9_\-](\s{0,1}[a-zA-Z0-9])*$/.test(val) ? val : '');			
		}
	},
	allowedMaxValue: 999999999,
	/**
	 * Field label of the control to display to the user, otherwise known as displayName
	 */
	fieldLabel: 'Untitled',
	/**
	 * Formula for determining the label separator of a control.  If its a required field, then we give it a red star to indicate that it is a required field
	 */
	mandatoryLabelSeparator$: function() {
		return this.mandatory ? ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>' : ':'
	},
	/**
	 * Label alignment (top, left, right) of the field label for the control
	 */
	labelAlign: 'left',
	/**
	 * List of label alignment options, configures a store with left, right, and top options for the user to choose from
	 */
	labelAlignOptionsList: {
		mtype: 'store',
		fields: ['text', 'value'],
		data: [{
			text: 'Left',
			value: 'left'
		}, {
			text: 'Right',
			value: 'right'
		}, {
			text: 'Top',
			value: 'top'
		}]
	},

	/**
	 * Indicates whether the user should be able to change this field's value
	 */
	readOnly: false,
	/**
	 * Indicates whether the control should be shown to the user or not
	 */
	hidden: false,

	fieldLabelOnFocus: 'Untitled',
    fieldLabelIsChanged: false,

    focusFieldLabel: function (field) {
        this.fieldLabelOnFocus = field.target.value;
    },
    
    blurFieldLabel: function (field) {
		this.fieldLabelIsChanged = this.fieldLabelIsChanged || this.fieldLabelOnFocus !== field.target.value;
    },

	when_hidden_changes_update_required: {
		on: ['hiddenChanged'],
		action: function() {
			if (Ext.isDefined(this.requiredShouldBeEnabled)) this.set('requiredShouldBeEnabled', !this.hidden);
			if (this.hidden && Ext.isDefined(this.mandatory)) this.set('mandatory', false);
		}
	},

	initMixin: function() {
		this.labelAlignOptionsList.each(function(option) {
			option.set('text', this.localize(option.get('text')));
		}, this);

		if (this.fieldLabel === 'Untitled') this.set('fieldLabel', this.localize(this.fieldLabel));

		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getFormControlConversionProperties) : this.serializeFunctions = [this.getFormControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseFormControlConversionProperties) : this.deserializeFunctions = [this.parseFormControlConversionProperties];
	},

	/**
	 * Reactor listening for events that cannot be automatically updated by the framework because extjs doesn't support it, so we need to rebuild the control
	 */
	whenControlNeedsRebuilding: {
		on: ['labelAlignChanged', 'mandatoryChanged'],
		action: function() {
			//Rebuild just the live view if at all possible
			this.parentVM.rebuildControl(this, true);
		}
	},
	/**
	 * Conversion method that turns a form control into server fields to be persisted in the db
	 */
	getFormControlConversionProperties: function() {
		return {
			id: this.id,
			name: (this.inputType === 'INPUT' ? this.inputName : this.column) || this.fieldLabel,
			displayName: this.fieldLabel,
			hidden: this.hidden,
			readOnly: this.readOnly,
			labelAlign: this.labelAlign
		};
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseFormControlConversionProperties: function(config) {
		this.set('id', config.id);
		this.set('fieldLabel', config.displayName);
		this.set('inputName', config.name);
		this.set('readOnly', config.readOnly);
		this.set('hidden', config.hidden);
		this.set('labelAlign', config.labelAlign || this.parentVM.labelAlign);
		this.fieldLabelIsChanged = this.fieldLabelIsChanged || this.fieldLabelOnFocus !== config.displayName;
	}
});

/**
 * Mixin definition for a SizeControl
 * A size control is a mixin for storing whether the field can be resized by the user.
 * It keeps track of things like width and height for the control
 */
glu.defModel('RS.formbuilder.SizeControl', {
	/**
	 * Width of the control in pixels
	 */
	width: 300,
	/**
	 * Height of the control in pixels
	 */
	height: 25,
	/**
	 * Default height of controls
	 */
	defaultHeight: 25,
	/**
	 * Whether the section should be collapsed (hidden)
	 */
	collapsed: true,
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getSizeControlConversionProperties) : this.serializeFunctions = [this.getSizeControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseSizeControlConversionProperties) : this.deserializeFunctions = [this.parseSizeControlConversionProperties];
	},
	/**
	 * Conversion method that turns a size control into server fields to be persisted in the db
	 */
	getSizeControlConversionProperties: function() {
		return {
			height: this.collapsed ? this.defaultHeight : this.height,
			width: this.actualWidth
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseSizeControlConversionProperties: function(config) {
		this.set('height', config.height || this.height);
		this.set('width', config.width || this.width);

		//old default height was 22, in that case up it to new default height
		if (this.height == 22) this.set('height', this.defaultHeight)

		if (config.width || (config.height > 0 && config.height != this.defaultHeight)) {
			this.set('collapsed', false);
		}
	},
	actualWidth$: function() {
		return this.collapsed ? 0 : this.width;
	},
	/**
	 * The actual height to be used for the control based on whether the size fieldset is collapsed or not
	 */
	actualHeight$: function() {
		var dh = this.defaultHeight,
			h = this.height;
		if (this.labelAlign == 'top') {
			dh += 20;
			h += 20;
		}
		return this.collapsed ? dh : h;
	}
});
/**
 * Mixin definition for a StringControl
 * A string control has a min and max length associated with it
 */
glu.defModel('RS.formbuilder.StringControl', {
	/**
	 * Minimum length of the string to make the control valid
	 */
	minLength: 0,
	minLengthIsValid$: function() {
		if (this.minLength < 0) return this.localize('minValueInvalid', [0]);
		if (this.minLength > this.allowedMaxValue) return this.localize('maxValueInvalid', [this.allowedMaxValue]);
		return true;
	},
	/**
	 * Maximum length of the string to make the control valid
	 */
	maxLength: 100,
	maxLengthIsValid$: function() {
		if (this.maxLength < 0) return this.localize('minValueInvalid', [0]);
		if (this.maxLength > this.allowedMaxValue) return this.localize('maxValueInvalid', [this.allowedMaxValue]);
		return true;
	},
	columnSize: 333,
	columnSizeStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [{
			name: '40',
			value: 40
		}, {
			name: '333',
			value: 333
		}, {
			name: '1000',
			value: 1000
		}, {
			name: '4000',
			value: 4000
		}, {
			name: '4000+',
			value: 4001
		}]
	},

	columnSizeIsVisible$: function() {
		return this.inputType == 'DB';
	},
	columnSizeIsReadOnly$: function() {
		return this.id || (this.column && this.parentVM.columnStore.findRecord('name', this.column, 0, false, false, true));
	},
	when_column_size_changes_update_allowedMaxValue: {
		on: ['columnSizeChanged'],
		action: function() {
			if (this.columnSize <= 4000) this.set('allowedMaxValue', this.columnSize);
			else this.set('allowedMaxValue', 999999999);
		}
	},

	initMixin: function() {
		this.set('maxLength', this.allowedMaxValue);
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getStringControlConversionProperties) : this.serializeFunctions = [this.getStringControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseStringControlConversionProperties) : this.deserializeFunctions = [this.parseStringControlConversionProperties];
	},
	/**
	 * Conversion method that turns a string control into server fields to be persisted in the db
	 */
	getStringControlConversionProperties: function() {
		return {
			stringMinLength: this.minLength,
			uiStringMaxLength: this.inputType == 'DB' ? (Math.min(this.maxLength <= 0 ? 999999999 : this.maxLength, this.columnSize)) : this.maxLength
		};
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseStringControlConversionProperties: function(config) {
		this.set('maxLength', config.uiStringMaxLength <= 4000 ? config.uiStringMaxLength : 999999999);
		this.set('minLength', config.stringMinLength);
	}
});
/**
 * Mixin definition for a DefaultValueControl
 * Keeps track of the default value for the control
 */
glu.defModel('RS.formbuilder.DefaultValueControl', {
	/**
	 * The default value of the control to pre-populate for the user
	 */
	defaultValue: '',
	defaultValueIsEnabled: true,
	when_field_label_starts_with_sys_disable_default_value: {
		on: ['dbFieldNameChanged'],
		action: function() {
			if (this.dbFieldName && this.dbFieldName.indexOf('sys_') == 0) {
				this.set('defaultValue', '');
				this.set('defaultValueIsEnabled', false);
			} else this.set('defaultValueIsEnabled', true);
		}
	},
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getDefaultValueControlConversionProperties) : this.serializeFunctions = [this.getDefaultValueControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseDefaultValueControlConversionProperties) : this.deserializeFunctions = [this.parseDefaultValueControlConversionProperties];
	},
	/**
	 * Conversion method that turns a default value control into server fields to be persisted in the db
	 */
	getDefaultValueControlConversionProperties: function() {
		return {
			defaultValue: this.defaultValue
		};
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseDefaultValueControlConversionProperties: function(config) {
		this.set('defaultValue', config.defaultValue || '');
	}
});
glu.defModel('RS.formbuilder.DefaultBooleanControl', {
	defaultValue: false,

	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getDefaultBooleanValueControlConversionProperties) : this.serializeFunctions = [this.getDefaultBooleanValueControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseDefaultBooleanValueControlConversionProperties) : this.deserializeFunctions = [this.parseDefaultBooleanValueControlConversionProperties];
	},
	/**
	 * Conversion method that turns a default value control into server fields to be persisted in the db
	 */
	getDefaultBooleanValueControlConversionProperties: function() {
		return {
			defaultValue: this.defaultValue
		};
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseDefaultBooleanValueControlConversionProperties: function(config) {
		this.set('defaultValue', config.defaultValue === 'true' || false);
	}
});

/**
 * Mixin definition for a DateControl
 * Keeps track of a date default value instead of a string
 */
glu.defModel('RS.formbuilder.DateControl', {
	/**
	 * The default value of the control to pre-populate for the user
	 */
	defaultValue: '',
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getDateControlConversionProperties) : this.serializeFunctions = [this.getDateControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseDateControlConversionProperties) : this.deserializeFunctions = [this.parseDateControlConversionProperties];
	},
	/**
	 * Conversion method that turns a date control into server fields to be persisted in the db
	 */
	getDateControlConversionProperties: function() {
		return {
			defaultValue: Ext.Date.format(this.defaultValue, 'c')
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseDateControlConversionProperties: function(config) {
		this.set('defaultValue', Ext.Date.parse(config.defaultValue, 'c'));
	}
});
/**
 * Mixin definition for a TimeControl
 * Keeps track of a time default value instead of a string
 */
glu.defModel('RS.formbuilder.TimeControl', {
	/**
	 * The default value of the control to pre-populate for the user
	 */
	defaultValue: '',
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getTimeControlConversionProperties) : this.serializeFunctions = [this.getTimeControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseTimeControlConversionProperties) : this.deserializeFunctions = [this.parseTimeControlConversionProperties];
	},
	/**
	 * Conversion method that turns a time control into server fields to be persisted in the db
	 */
	getTimeControlConversionProperties: function() {
		return {
			defaultValue: Ext.Date.format(this.defaultValue, 'c')
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseTimeControlConversionProperties: function(config) {
		this.set('defaultValue', Ext.Date.parse(config.defaultValue, 'c'));
	}
});
/**
 * Mixin definition for a DateTimeControl
 * Keeps track of a date and a time and merges the two together for the initial value of the control
 */
glu.defModel('RS.formbuilder.DateTimeControl', {
	/**
	 * Internal date to store from the date control to be merged with the time
	 */
	date: '',
	/**
	 * Internal time to store from the time control to be merged with the date
	 */
	time: '',
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getDateTimeControlConversionProperties) : this.serializeFunctions = [this.getDateTimeControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseDateTimeControlConversionProperties) : this.deserializeFunctions = [this.parseDateTimeControlConversionProperties];
	},
	/**
	 * Conversion method that turns a date time control into server fields to be persisted in the db
	 */
	getDateTimeControlConversionProperties: function() {
		var myDate = this.date,
			myTime = this.time,
			datetime = new Date();

		if (Ext.isDate(myDate) && Ext.isDate(myTime)) datetime = new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), myTime.getHours(), myTime.getMinutes(), myTime.getSeconds());
		else if (Ext.isDate(myDate)) datetime = myDate;
		else if (Ext.isDate(myTime)) datetime = myTime;
		else return {
			defaultValue: ''
		};

		return {
			defaultValue: Ext.Date.format(datetime, 'c')
		};

	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseDateTimeControlConversionProperties: function(config) {
		this.set('date', Ext.Date.parse(config.defaultValue, 'c'));
		this.set('time', Ext.Date.parse(config.defaultValue, 'c'));
	}
});
/**
 * Mixin definition for a RequireableControl
 * Determines whether this field is required before submitting to the server
 */
glu.defModel('RS.formbuilder.RequireableControl', {
	/**
	 * Whether the control is required to be filled out or not
	 */
	mandatory: false,
	requiredShouldBeEnabled: true,
	mandatoryIsEnabled$: function() {
		return this.requiredShouldBeEnabled;
	},
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getRequireableControlConversionProperties) : this.serializeFunctions = [this.getRequireableControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseRequireableControlConversionProperties) : this.deserializeFunctions = [this.parseRequireableControlConversionProperties];
	},
	/**
	 * Conversion method that turns a requireable control into server fields to be persisted in the db
	 */
	getRequireableControlConversionProperties: function() {
		return {
			mandatory: this.mandatory
		};
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseRequireableControlConversionProperties: function(config) {
		this.set('mandatory', config.mandatory);
	}
});
/**
 * Mixin definition for an Encryptable Control
 * Determines whether this field is encrypted before submitting to the database
 */
glu.defModel('RS.formbuilder.EncryptableControl', {
	/**
	 * Whether the control is required to be filled out or not
	 */
	encrypted: false,
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getEncryptableControlConversionProperties) : this.serializeFunctions = [this.getEncryptableControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseEncryptableControlConversionProperties) : this.deserializeFunctions = [this.parseEncryptableControlConversionProperties];
	},
	/**
	 * Conversion method that turns a requireable control into server fields to be persisted in the db
	 */
	getEncryptableControlConversionProperties: function() {
		return {
			encrypted: this.encrypted
		};
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseEncryptableControlConversionProperties: function(config) {
		this.set('encrypted', config.encrypted);
	}
});
/**
 * Mixin definition for a SequenceControl
 * Keeps track of a sequence prefix for the user
 */
glu.defModel('RS.formbuilder.SequenceControl', {
	/**
	 * User defined sequence prefix
	 */
	prefix: '',
	prefixIsValid$: function() {
		if (!this.prefix) return this.localize('prefixRequired');
		return true;
	},
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getPrefixControlConversionProperties) : this.serializeFunctions = [this.getPrefixControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parsePrefixControlConversionProperties) : this.deserializeFunctions = [this.parsePrefixControlConversionProperties];
	},
	/**
	 * Conversion method that turns a sequence control into server fields to be persisted in the db
	 */
	getPrefixControlConversionProperties: function() {
		return {
			sequencePrefix: this.prefix
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parsePrefixControlConversionProperties: function(config) {
		this.set('prefix', config.sequencePrefix);
	}
});
/**
 * Mixin definition for a NumberControl
 * Keeps track of min and max VALUE instead of length like in a string control
 */
glu.defModel('RS.formbuilder.NumberControl', {
	/**
	 * The default value to show to the user when the control initially loads
	 */
	defaultValue: '',
	/**
	 * Min value for the number
	 */
	minValue: 0,
	/**
	 * Max value for the number
	 */
	maxValue: 999999999,
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getNumberControlConversionProperties) : this.serializeFunctions = [this.getNumberControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseNumberControlConversionProperties) : this.deserializeFunctions = [this.parseNumberControlConversionProperties];
	},
	/**
	 * Conversion method that turns a number control into server fields to be persisted in the db
	 */
	getNumberControlConversionProperties: function() {
		return {
			integerMinValue: this.minValue,
			integerMaxValue: this.maxValue <= 0 ? 999999999 : this.maxValue,
			defaultValue: this.defaultValue
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseNumberControlConversionProperties: function(config) {
		this.set('minValue', config.integerMinValue);
		this.set('maxValue', config.integerMaxValue);
		this.set('defaultValue', config.defaultValue);
	}
});
/**
 * Mixin definition for a DecimalControl
 * Keeps track of min and max VALUE instead of length like in a string control
 */
glu.defModel('RS.formbuilder.DecimalControl', {
	/**
	 * The default value to show to the user when the control initially loads
	 */
	defaultValue: '',
	/**
	 * Min value for the decimal
	 */
	minValue: 0,
	/**
	 * Max value for the decimal
	 */
	maxValue: 999999999,
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getDecimalControlConversionProperties) : this.serializeFunctions = [this.getDecimalControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseDecimalControlConversionProperties) : this.deserializeFunctions = [this.parseDecimalControlConversionProperties];
	},
	/**
	 * Conversion method that turns a decimal control into server fields to be persisted in the db
	 */
	getDecimalControlConversionProperties: function() {
		return {
			decimalMinValue: this.minValue,
			decimalMaxValue: this.maxValue <= 0 ? 999999999 : this.maxValue,
			defaultValue: this.defaultValue
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseDecimalControlConversionProperties: function(config) {
		this.set('minValue', config.decimalMinValue);
		this.set('maxValue', config.decimalMaxValue);
		this.set('defaultValue', config.defaultValue);
	}
});

glu.defModel('RS.formbuilder.AllowInputControl', {

	allowUserInput: false,
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getAllowInputControlConversionProperties) : this.serializeFunctions = [this.getAllowInputControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseAllowInputControlConversionProperties) : this.deserializeFunctions = [this.parseAllowInputControlConversionProperties];
	},

	getAllowInputControlConversionProperties: function() {
		return {
			selectUserInput: this.allowUserInput
		}
	},
	parseAllowInputControlConversionProperties: function(config) {
		this.set('allowUserInput', config.selectUserInput);
	}
});
/**
 * Mixin defintion for a ChoiceControl
 * Choice control is used for dropdown and radiobuttons, and defines a list of available options for the user to choose from
 */
glu.defModel('RS.formbuilder.ChoiceControl', {
	/**
	 * The separator for serializing the multiple options
	 */
	choiceSeparator: '|&|',
	/**
	 * The separator for serializing the option to its value (checked)
	 */
	choiceValueSeparator: '|=|',
	/**
	 * List of options provided by the user
	 */
	options: {
		mtype: 'list',
		autoParent: true
	},

	columns: 0,
	columnsIsVisible$: function() {
		return !this.isCombobox;
	},
	when_columns_count_changes_rebuild_control: {
		on: ['columnsChanged'],
		action: function() {
			this.parentVM.rebuildControl(this, true);
		}
	},

	/**
	 * The actual height to be used for the control based on whether the size fieldset is collapsed or not
	 */
	actualHeight$: function() {
		var dh = this.defaultHeight,
			h = this.height;

		if (this.columns > 0) {
			dh += (this.optionsStore.getCount() / this.columns) * 20
		}
		if (this.labelAlign == 'top') {
			dh += 20;
			h += 20;
		}
		return this.collapsed ? dh : h;
	},

	/**
	 * Store linked to the options to display the options in a store if we need to
	 */
	optionsStore: {
		mtype: 'store',
		fields: ['text', 'id', 'displayText'],
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'options'
		}]
	},
	syncStore: function() {
		this.optionsStore.removeAll();
		this.options.foreach(function(option) {
			this.optionsStore.add(option)
		}, this);
	},
	/**
	 * The default value for the control to present to the user once the control loads
	 */
	defaultValue: '',

	/**
	 * Determines whether the options list is greater than one in order to not be allowed to remove the last option (otherwise you would loose the ability to add another option)
	 */
	optionsGreaterThanOne$: function() {
		return this.options.length > 1;
	},
	/**
	 * Determines which option is selected by default based on the defaultValue
	 */
	calculateDefaultValue$: function() {
		var optionValue = '',
			val = this.defaultValue;
		this.options.foreach(function(option) {
			if (option.id == val) optionValue = option.id;
		});
		return optionValue;
	},
	setCalculateDefaultValue: function(value) {
		this.options.foreach(function(option) {
			if (option.id == value) option.set('isSelected', true);
		}, this);
	},
	tempId: '',
	initMixin: function() {
		this.set('tempId', Ext.id());

		var i = 0;
		for (; i < 3; i++) {
			this.addOption(null, this.localize('Option') + ' ' + (this.options.length + 1));
		}

		this.tableColumnStore.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				tableName: this.dropdownChoiceTable
			});
		}, this);

		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getChoiceControlConversionProperties) : this.serializeFunctions = [this.getChoiceControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseChoiceControlConversionProperties) : this.deserializeFunctions = [this.parseChoiceControlConversionProperties];
	},
	/**
	 * Adds an option to the control with the provided text
	 * @param {Object} ctrl
	 * @param {Object} text
	 */
	addOption: function(ctrl, text) {
		var index = this.options.indexOf(ctrl)
		if (index == -1) index = this.options.length;

		var model = this.model({
			mtype: 'Option',
			text: text || ' '
		});
		model.init();

		this.options.insert(index + 1, model);
		if (this.options.length == 1) {
			this.set('defaultValue', this.options.getAt(0).id);
		}
		if (model.text == ' ') model.set('text', '');
	},
	/**
	 * Removes the provided option from the list of options
	 * @param {Object} ctrl
	 */
	removeOption: function(ctrl) {
		if (ctrl.id == this.defaultValue) this.set('defaultValue', '');
		this.options.remove(ctrl);
	},
	/**
	 * Conversion method that turns a choice control into server fields to be persisted in the db
	 */
	getChoiceControlConversionProperties: function() {
		var choices = [],
			choiceValue = '',
			values = [];
		this.options.foreach(function(option) {
			choices.push(option.text);
			if (option.isSelected) values.push(option.text);
		}, this);

		var choicesString = choices.join(this.choiceSeparator);
		var valuesString = values.join(this.choiceSeparator);

		return {
			choiceValues: choicesString,
			defaultValue: valuesString,
			widgetColumns: this.columns,

			choiceOptionSelection: this.activeOption,
			choiceTable: this.dropdownChoiceTable,
			choiceDisplayField: this.displayField,
			choiceValueField: this.valueField,
			choiceWhere: this.where,
			choiceField: this.parentField,
			choiceSql: this.activeOption == 2 ? this.sqlQuery : ''
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseChoiceControlConversionProperties: function(config) {
		this.set('columns', config.widgetColumns || 0);


		this.set('simple', false);
		this.set('advanced', false);
		this.set('custom', false);
		switch (config.choiceOptionSelection) {
			case 1:
				this.set('advanced', true);
				this.set('dropdownChoiceTable', config.choiceTable);
				this.set('displayField', config.choiceDisplayField);
				this.set('valueField', config.choiceValueField);
				this.set('where', config.choiceWhere);
				this.fieldStore.add({
					name: config.choiceField,
					value: config.choiceField
				})
				this.set('parentField', config.choiceField);
				break;
			case 2:
				this.set('custom', true);
				if (config.choiceSql) this.set('sqlQuery', config.choiceSql);
				break;
			case 0:
			default:
				this.set('simple', true);
				var choices = config.choiceValues ? config.choiceValues.split(this.choiceSeparator) : '';
				var values = config.defaultValue ? config.defaultValue.split(this.choiceSeparator) : '';
				var i = 0;
				for (; i < values.length; i++) {
					values[i] = values[i].split(this.choiceValueSeparator)[0];
				}
				this.options.removeAll();
				Ext.each(choices, function(choice) {
					this.options.add(this.model({
						mtype: 'Option',
						text: choice || ' ',
						id: Ext.id()
					}));
					this.options.getAt(this.options.length - 1).set('isSelected', Ext.Array.indexOf(values, choice) > -1);
				}, this);
				break;
		}

		if (config.choiceOptionSelection == 0) {
			//Do nothing
		} else {
			while (this.options.length > 1) this.options.removeAt(1);
			this.options.getAt(0).set('text', this.localize(config.choiceOptionSelection == 1 ? 'advanced' : 'custom'))
		}
	},
	/**
	 * Button Handler to open the ImportOption dialog box
	 */
	importOptions: function() {
		this.open({
			mtype: 'ImportOption'
		});
	},

	importOptionsIsVisible$: function() {
		return this.simple;
	},

	activeOption$: function() {
		if (this.custom) return 2;
		if (this.advanced) return 1;
		return 0;
	},

	simple: true,
	advanced: false,
	custom: false,

	displayOptionsSelections$: function() {
		return this.viewmodelName == 'ComboBoxField'
	},

	sqlQuery: 'select f1 as name, f2 as value from table where f3=\'${fieldName}\'',
	sqlQueryIsVisible$: function() {
		return this.custom
	},

	tableColumnStore: {
		mtype: 'store',
		fields: ['name', 'dbcolumnId', 'displayName', 'dbtype', 'uiType', 'integerMinValue', 'integerMaxValue', 'decimalMinValue', 'decimalMaxValue', 'stringMinLength', 'stringMaxLength', 'uiStringMaxLength', 'selectValues', 'choiceValues', 'referenceTable', 'referenceDisplayColumn'],
		data: [],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getcustomtablecolumns',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	dropdownChoiceTable: '',
	when_dropdown_choiceTable_changes_update_columns: {
		on: 'dropdownChoiceTableChanged',
		action: function() {
			this.tableColumnStore.load();
		}
	},
	displayField: '',
	displayFieldIsEnabled$: function() {
		return this.dropdownChoiceTable
	},
	valueField: '',
	valueFieldIsEnabled$: function() {
		return this.dropdownChoiceTable
	},
	where: '',
	whereIsEnabled$: function() {
		return this.dropdownChoiceTable
	},
	parentField: '',
	parentFieldIsEnabled$: function() {
		return this.where
	},

	fieldStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		listeners: {
			load: function() {
				var fieldNames = this.rootVM.getFormFieldNames();
				Ext.each(fieldNames, function(name) {
					this.add({
						name: name,
						value: name
					})
				}, this);
				this.sort('name', 'ASC');
			}
		}
	}


});
/**
 * Mixin definition for the MultiChoiceControl
 * Very similar to the choice control except that options can be multi-selected
 */
glu.defModel('RS.formbuilder.MultiChoiceControl', {
	/**
	 * The actual height to be used for the control based on whether the size fieldset is collapsed or not
	 */
	actualHeight$: function() {
		var dh = this.defaultHeight,
			h = this.height;

		if (this.columns > 0) {
			dh += (this.optionsStore.getCount() / this.columns) * 20
		}
		if (this.labelAlign == 'top') {
			dh += 20;
			h += 20;
		}
		return this.collapsed ? null : h;
	},
	/**
	 * The separator for serializing the multiple options
	 */
	choiceSeparator: '|&|',
	/**
	 * The separator for serializing the option to its value (checked)
	 */
	choiceValueSeparator: '|=|',
	/**
	 * List of options provided by the user
	 */
	options: {
		mtype: 'list',
		autoParent: true
	},
	/**
	 * Store linked to the options to display the options in a store if we need to
	 */
	optionsStore: {
		mtype: 'store',
		fields: ['text', 'id', 'displayText'],
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'options'
		}]
	},
	syncStore: function() {
		this.optionsStore.removeAll();
		this.options.foreach(function(option) {
			this.optionsStore.add(option)
		}, this);
	},

	columns: 0,
	columnsIsVisible$: function() {
		return !this.isCombobox;
	},
	when_columns_count_changes_rebuild_control: {
		on: ['columnsChanged'],
		action: function() {
			this.parentVM.rebuildControl(this, true);
		}
	},

	/**
	 * Determines whether there is more than one option in the list to make sure you cannot remove the last option (becaue the add button is on the option)
	 */
	optionsGreaterThanOne$: function() {
		return this.options.length > 1;
	},
	tempId: '',
	initMixin: function() {
		this.set('tempId', Ext.id());
		var i = 0;
		for (; i < 3; i++)
			this.addOption(null, this.localize('Option') + ' ' + (this.options.length + 1));
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getMultiChoiceControlConversionProperties) : this.serializeFunctions = [this.getMultiChoiceControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseMultiChoiceControlConversionProperties) : this.deserializeFunctions = [this.parseMultiChoiceControlConversionProperties];
	},
	/**
	 * Determines the default value based on which options are selected
	 */
	defaultValue$: function() {
		var values = [];
		for (var i = 0; i < this.options.length; i++) {
			if (this.options.getAt(i).isChecked) values.push(this.options.getAt(i).id);
		}
		return values;
	},
	/**
	 * Determines which option is selected by default based on the defaultValue
	 */
	calculateDefaultValue$: function() {
		return this.defaultValue;
	},
	setCalculateDefaultValue: function(val) {
		var i = 0,
			j, found;
		for (; i < this.options.length; i++) {
			found = false;

			for (j = 0; j < val.length; j++) {
				if (this.options.getAt(i).id == val[j]) {
					found = true;
				}
			}

			this.options.getAt(i).set('isChecked', found);
		}
	},
	/**
	 * Fires an event to indicate the options have changed when the checked state of an option is changed
	 * @param {Object} ctrl
	 */
	checkControl: function(ctrl) {
		this.fireEvent('optionschanged');
	},
	/**
	 * Adds an option to the list of options with the provided text
	 * @param {Object} ctrl
	 * @param {Object} text
	 */
	addOption: function(ctrl, text) {
		var index = this.options.indexOf(ctrl)
		if (index == -1) index = this.options.length - 1;

		var model = this.model({
			mtype: 'CheckOption',
			text: text || ' '
		});
		model.init();
		this.options.insert(index + 1, model);
		if (model.text == ' ') model.set('text', '');
	},
	/**
	 * Removes the provided option from the list of options
	 * @param {Object} ctrl
	 */
	removeOption: function(ctrl) {
		this.options.remove(ctrl);
	},
	/**
	 * Conversion method that turns a multi-choice control into server fields to be persisted in the db
	 */
	getMultiChoiceControlConversionProperties: function() {
		var choices = [],
			choiceValue = '',
			values = [];
		this.options.foreach(function(option) {
			choices.push(option.text);
			if (option.isChecked) values.push(option.text + this.choiceValueSeparator + 'true');
		}, this);

		var choicesString = choices.join(this.choiceSeparator);
		var valuesString = values.join(this.choiceSeparator);

		return {
			choiceValues: choicesString,
			defaultValue: valuesString,
			widgetColumns: this.columns
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseMultiChoiceControlConversionProperties: function(config) {
		this.set('columns', config.widgetColumns || 0);
		var choices = config.choiceValues ? config.choiceValues.split(this.choiceSeparator) : '';
		var values = config.defaultValue ? config.defaultValue.split(this.choiceSeparator) : '';
		var i = 0;
		for (; i < values.length; i++) {
			values[i] = values[i].split(this.choiceValueSeparator)[0];
		}
		this.options.removeAll();
		Ext.each(choices, function(choice) {
			this.options.add(this.model({
				mtype: 'Option',
				text: choice || ' ',
				isChecked: Ext.Array.indexOf(values, choice) > -1
			}));
		}, this);
	},
	/**
	 * Button Handler to open the ImportOption dialog
	 */
	importOptions: function() {
		this.open({
			mtype: 'ImportOption'
		});
	}
});

/**
 * Mixin definition for a ButtonControl
 * A button is not like a form control at all, it doesn't have a field label, but has text to display on the button and a list of actions to perform when the button is clicked
 */
glu.defModel('RS.formbuilder.ButtonControl', {
	mixins: ['DependencyControl'],
	fields: ['operation', 'operationName', 'customTableDisplay', 'font', 'fontSize', 'fontColor', 'backgroundColor'],
	asObject: function() {
		var buttonControl = {
			operation: this.operation,
			operationName: this.operationName,
			customTableDisplay: this.customTableDisplay,
			dependenciesJson: this.getDependencyControlConversionProperties().dependencies,
			id: this.id
		}

		if (this.type == 'button') {
			Ext.apply(buttonControl, {
				font: this.font,
				fontSize: this.fontSize,
				fontColor: this.fontColor,
				backgroundColor: this.backgroundColor
			});
		}

		return buttonControl;
	},
	id: '',
	/**
	 * Operation to perform when the button is clicked by the user
	 */
	operation: 'New Button',
	/**
	 * Translated operation name to display to the user
	 */
	operationName$: function() {
		return this.operation;
	},
	/**
	 * Sets the operation to the provided value
	 * @param {Object} val
	 */
	setOperationName: function(val) {
		this.set('operation', val);
		var idx = this.parentList.indexOf(this);
		this.parentVM.set('formButtonsSelections', [this.parentVM.treeStore.getRootNode().getChildAt(idx)]);
	},

	customTableDisplay: false,
	/**
	 * For view purposes we need to define this object as a button to separate it from Actions
	 */
	type: 'button',
	buttonXType: 'button',
	leaf: false,
	/**
	 * A list of actions to be performed when the button is clicked
	 */
	actions: {
		mtype: 'list',
		autoParent: true
	},
	/**
	 * Store to display the list of actions to the user in a grid
	 */
	actionsStore: {
		mtype: 'store',
		fields: ['operationName', 'value'],
		data: [],
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'actions'
		}]
	},

	defaultFont: 'Verdana',
	defaultFontSize: '10',
	defaultFontColor: '333333',
	defaultBackgroundColor: 'DDDDDD',

	editButton: false,

	init: function() {
		this.set('editButton', this.type == 'button');
		if (!this.id) this.set('id', Ext.id());
		if (this.operation == 'New Button') this.set('operation', this.localize('newButton'));
		if (this.type == 'button') {
			if (!this.font) {
				this.resetButtonStyle();
			}
		} else {
			this.set('operation', this.localize(this.type));
			this.set('leaf', true);
		}
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getButtonControlConversionProperties) : this.serializeFunctions = [this.getButtonControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseButtonControlConversionProperties) : this.deserializeFunctions = [this.parseButtonControlConversionProperties];
	},
	/**
	 * Button Handler that forwards this event to the parentVM to remove this control from the form
	 */
	removeControl: function() {
		this.parentVM.removeControl(this);
	},
	/**
	 * Conversion method that turns a button control into a server buttonPanel to be persisted in the db
	 */
	getButtonControlConversionProperties: function(isSave) {
		var convertedActions = [];
		this.actions.foreach(function(action, index) {
			var params = {};
			switch (action.operation) {
				case 'MAPPING':
					if (action.mappingFrom && action.mappingTo) {
						params[action.mappingTo] = action.mappingFrom;
					}
					break;
	
				case 'SCRIPT':
					Ext.apply(params, {
						TIMEOUT: action.TIMEOUT
					});
					break;
	
				case 'EVENT':
					Ext.apply(params, {
						EVENT_EVENTID: action.EVENT_EVENTID,
						PROBLEMID: action.PROBLEMID,
						EVENT_REFERENCE: action.PROBLEMID,
						mappingFrom: action.PROBLEMID == 'MAPFROM' ? action.mappingFrom : ''
					});
					break;
	
				case 'RUNBOOK':
					Ext.apply(params, {
						PROBLEMID: action.PROBLEMID
					});
					break;
	
				case 'ACTIONTASK':
					Ext.apply(params, {
						PROBLEMID: action.PROBLEMID
					});
					break;
	
				case 'DB':
					if (action.dbAction == 'DELETE') {
						Ext.apply(params, {
							showConfirmation: action.showConfirmation
						});
					}
					break;

				case 'SIR':
					if (isSave && action.sirAction === 'saveCaseData') {
						action.dbAction = 'SUBMIT';
						action.operation = 'DB';
						Ext.apply(params, {
							sirAction: action.sirAction
						});
					} else {
						action.dbAction = action.sirAction; // since sirAction is not supported in BE, just pass sirAction to the dbAction
					}
					break;
	
				case 'MESSAGEBOX':
					Ext.apply(params, {
						messageBoxTitle: action.messageBoxTitle,
						messageBoxMessage: action.messageBoxMessage,
						messageBoxYesText: action.messageBoxYesText,
						messageBoxNoText: action.messageBoxNoText
					});
					break;
	
				case 'RELOAD':
					Ext.apply(params, {
						reloadDelay: action.reloadDelay,
						reloadTarget: action.reloadTarget,
						reloadCondition: action.reloadCondition,
						reloadValue: action.reloadValue
					});
					break;
	
				case 'GOTO':
					Ext.apply(params, {
						gotoTabName: action.gotoTabName
					});
					break;
	
				default:
					break;
			}

			convertedActions.push({
				id: action.id,
				crudAction: action.dbAction,
				operation: action.operation,
				runbook: action.RUNBOOK,
				actionTask: action.ACTIONTASK,
				script: action.SCRIPT,
				orderNumber: index,
				redirectTarget: action.redirectTarget,
				redirectUrl: action.redirectUrl,
				additionalParam: this.serializeParams(params)
			});
		}, this);

		var deps = this.getDependencyControlConversionProperties().dependencies;
		var buttonControl = {
			id: this.id.indexOf('ext') > -1 ? '' : this.id,
			displayName: this.operation,
			customTableDisplay: this.customTableDisplay,
			name: 'CUSTOM',
			type: this.type,
			orderNumber: this.orderNumber,
			actions: convertedActions,
			dependencies: deps,
		};

		if (this.type == 'button') {
			Ext.apply(buttonControl, {
				font: this.font,
				fontSize: this.fontSize,
				fontColor: this.fontColor,
				backgroundColor: this.backgroundColor
			});
		}

		return buttonControl;
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseButtonControlConversionProperties: function(config, actionsOnly) {
		this.set('operation', config.displayName);
		this.set('id', config.id);
		this.set('type', config.type);
		this.set('customTableDisplay', config.customTableDisplay);
		if (config.type == 'button') {
			this.set('font', config.font || this.defaultFont);
			this.set('fontSize', config.fontSize || this.defaultFontSize);
			this.set('fontColor', config.fontColor || this.defaultFontColor);
			this.set('backgroundColor', config.backgroundColor || this.defaultBackgroundColor);
		} else {
			// this.set('operation', this.localize(config.type));
			this.set('leaf', true);
		}
		this.parseDependencyControlConversionProperties(config);
		if (actionsOnly) {
			this.actions.removeAll();
			Ext.each(config.actions, function(actionConfig, index) {
				var action = this.model({
					mtype: 'Action'
				});
				action.set('id', actionConfig.id || '')
				action.set('dbAction', actionConfig.crudAction);
				action.set('operation', actionConfig.operation || '');
				action.set('RUNBOOK', actionConfig.runbook || '');
				action.set('ACTIONTASK', actionConfig.actionTask || '');
				action.set('SCRIPT', actionConfig.script || '');
				action.set('redirectTarget', actionConfig.redirectTarget || '');
				action.set('redirectUrl', actionConfig.redirectUrl || '');
				if (actionConfig.additionalParam && Ext.isString(actionConfig.additionalParam)) {
					var additionalParam = this.deserializeParams(actionConfig.additionalParam);
					switch (action.operation) {
						case 'EVENT':
							action.set('PROBLEMID', additionalParam.PROBLEMID || '');
							action.set('EVENT_EVENTID', additionalParam.EVENT_EVENTID || '');
							action.set('EVENT_REFERENCE', additionalParam.EVENT_REFERENCE || '');
							action.set('mappingFrom', additionalParam.mappingFrom || '');
							break;
	
						case 'RUNBOOK':
							action.set('PROBLEMID', additionalParam.PROBLEMID || '');
							break;
	
						case 'ACTIONTASK':
							action.set('PROBLEMID', additionalParam.PROBLEMID || '');
							break;
	
						case 'SCRIPT':
							action.set('TIMEOUT', additionalParam.TIMEOUT);
							if (additionalParam.TIMEOUT == '') {
								action.set('waitSeconds', 0);
							}
							else if (additionalParam.TIMEOUT > 0) {
								action.set('waitSeconds', additionalParam.TIMEOUT / 1000);
							}
							break;
	
						case 'MAPPING':
							for (var key in additionalParam) {
								action.set('mappingFrom', additionalParam[key]);
								action.set('mappingTo', key);
							}
							break;
	
						case 'DB':
							if (action.dbAction == 'DELETE') {
								action.set('showConfirmation', additionalParam.showConfirmation);
							} else if (action.dbAction == 'SUBMIT' && additionalParam.sirAction == 'saveCaseData') {
								action.set('sirAction', additionalParam.sirAction);
								action.set('operation', 'SIR');
							}
							break;
	
						case 'MESSAGEBOX':
							action.set('messageBoxTitle', additionalParam.messageBoxTitle);
							action.set('messageBoxMessage', additionalParam.messageBoxMessage);
							action.set('messageBoxYesText', additionalParam.messageBoxYesText);
							action.set('messageBoxNoText', additionalParam.messageBoxNoText);
							break;
	
						case 'RELOAD':
							action.set('reloadDelay', additionalParam.reloadDelay);
							action.set('reloadTarget', additionalParam.reloadTarget);
							action.set('reloadCondition', additionalParam.reloadCondition);
							action.set('reloadValue', additionalParam.reloadValue);
							break;
	
						case 'GOTO':
							action.set('gotoTabName', additionalParam.gotoTabName)
							break;
						default:
							break;
					}
				}
				this.addRealAction(action);
				this.parentList.fireEvent('appendchild', action, this.parentList.indexOf(this));
			}, this);
		}
	},
	/**
	 * The separator for serializing the multiple options
	 */
	choiceSeparator: '|&|',
	/**
	 * The separator for serializing the option to its value (checked)
	 */
	choiceValueSeparator: '|=|',
	serializeParams: function(params) {
		var serialized = '';
		Ext.Object.each(params, function(param) {
			if (serialized) serialized += this.choiceSeparator;
			serialized += param + this.choiceValueSeparator + params[param];
		}, this);
		return serialized;
	},
	deserializeParams: function(paramString) {
		var params = {},
			paramsSplit = paramString.split(this.choiceSeparator);

		Ext.each(paramsSplit, function(split) {
			var val = split.split(this.choiceValueSeparator);
			if (val.length > 1) {
				params[val[0]] = val[1];
			}
		}, this);

		return params;
	},
	/**
	 * Array of selections in the grid to determine which action is selected by the user
	 */
	actionsSelections: [],
	/**
	 * Button Handler to add an action that opens a dialog box with the action properties
	 */
	addAction: function() {
		this.open({
			mtype: 'Action'
		});
	},
	/**
	 * Event Handler to actually add the action to the button with the provided action object (unlike addAction that raises the dialog)
	 * @param {Object} action
	 */
	addRealAction: function(action) {
		action.set('isNew', false);
		this.actions.add(action);
	},
	/**
	 * Button Handler to edit an action that opens a dialog box with the action properties
	 */
	editAction: function() {
		var action = this.actions.getAt(this.actionsStore.indexOf(this.actionsSelections[0]));
		this.open(Ext.applyIf({
			mtype: 'Action',
			isNew: false
		}, action.asObject()));
	},
	/**
	 * Event Handler to actually edit the action in the button with the provided action properties (unlike editAction that raises the dialog)
	 * @param {Object} actionProperties
	 */
	editRealAction: function(actionProperties) {
		var index = this.actionsStore.indexOf(this.actionsSelections[0]),
			action = this.actions.getAt(index);
		action.loadData(actionProperties);
		this.actions.fireEvent('edited', actionProperties, index);
	},
	/**
	 * Determines whether the edit action should be enabled based on the selection of the action
	 */
	editActionIsEnabled: {
		on: ['actionsSelectionsChanged'],
		formula: function() {
			return this.actionsSelections.length == 1;
		}
	},
	/**
	 * Deletes the selected action from the button
	 */
	removeAction: function() {
		for (var i = 0; i < this.actionsSelections.length; i++) {
			this.actions.removeAt(this.actionsStore.indexOf(this.actionsSelections[i]));
		}
	},
	/**
	 * Determines if an action is selected and enables the delete button if an action is selected
	 */
	removeActionIsEnabled: {
		on: ['actionsSelectionsChanged'],
		formula: function() {
			return this.actionsSelections.length == 1;
		}
	},

	fontStore: {
		mtype: 'store',
		fields: [{
			name: 'font',
			convert: function(v, r) {
				return r.raw
			}
		}],
		data: ['Helvetica', 'Verdana', 'Times New Roman', 'Garamond', 'Courier New']
	},

	fontSizeStore: {
		mtype: 'store',
		fields: [{
			name: 'fontsize',
			convert: function(v, r) {
				return r.raw
			}
		}],
		data: [6, 8, 9, 10, 12, 14, 18, 24, 30, 36, 48, 60]
	},

	font: '',
	fontSize: '',
	fontColor: '',
	backgroundColor: '',

	fontFamily$: function() {
		return this.font;
	},

	fontColorIcon$: function() {
		return 'rs-fontcolor-icon hex'+this.fontColor;
	},

	fillColorIcon$: function() {
		return 'rs-fillcolor-icon hex'+this.backgroundColor;
	},

	updateButtonFont: function(font) {
		this.set('font', font);
	},

	updateButtonFontSize: function(fontSize) {
		this.set('fontSize', fontSize);
	},

	updateButtonFontColor: function(color) {
		this.set('fontColor', color);
	},

	updateButtonBackgroundColor: function(color) {
		this.set('backgroundColor', color);
	},

	resetButtonStyle: function() {
		this.set('font', this.defaultFont);
		this.set('fontSize', this.defaultFontSize);
		this.set('fontColor', this.defaultFontColor);
		this.set('backgroundColor', this.defaultBackgroundColor);
	},

	applyButton: function() {
		this.doClose();
		this.parentVM.editRealButton(this.asObject());
	},
	cancel: function() {
		this.doClose();
	}
});
/**
 * Mixin definition for a DatabaseControl
 * A database control is persisted in the database or can be an input field on the form, but this keeps track of that information
 */
glu.defModel('RS.formbuilder.DatabaseControl', {
	/**
	 * Table to store the control in
	 */
	table: '',
	/**
	 * Column to store the control in
	 */
	column: '',
	columnIsValid$: function() {
		if (!this.column || this.inputType == 'INPUT') return true;
		//determine the column type from the column name (if this isn't a new column) and check its type with the type of this control to make sure they match
		var record = this.parentVM.columnStore.findRecord('name', this.column, 0, false, false, true);
		if (record && record.data.uiType && this.dbColumnType.indexOf(record.data.uiType) === -1) {
			return this.localize('columnInvalidText', [this.dbColumnType, record.data.uiType]);
		}
		//if no record, then its a new column
		if (this.column.length > 30) return this.localize('columnTooLongInvalidText');
		if (/[^A-Z|a-z|_|0-9]/g.test(this.column)) return this.localize('columnSpecialCharactersInvalidText');
		return true;
	},
	when_column_changes_update_field_label: {
		on: ['columnChanged'],
		action: function() {
			var record = this.parentVM.columnStore.findRecord('name', this.column, 0, false, false, true)
			if (record) {
				this.set('fieldLabel', record.data.displayName);
				//also update the min/max length/value as well as any options
				for (var key in record.data) {
					var realKey = '',
						isMax = false;
					switch (key) {
						case 'stringMaxLength':
							realKey = 'allowedMaxValue';
							break;
						case 'uiStringMaxLength':
							realKey = 'maxLength';
							break;
						case 'stringMinLength':
							realKey = 'minLength';
							break;
						case 'integerMaxValue':
						case 'decimalMaxValue':
							realKey = 'maxValue';
							if (!Ext.isDefined(this[realKey])) {
								realKey = 'maxLength';
							}
							isMax = true;
							break;
						case 'integerMinValue':
						case 'decimalMinValue':
							realKey = 'minValue';
							break;
						case 'referenceTable':
							if (Ext.isDefined(this.referenceTable)) this.set(key, record.data[key]);
							break;
						case 'referenceDisplayColumn':
							if (Ext.isDefined(this.referenceDisplayColumn)) this.set(key, record.data[key]);
							break;
					}
					if (realKey && Ext.isDefined(this[realKey]) && record.data[key]) {
						this.set(realKey, record.data[key]);
						if (realKey === 'allowedMaxValue' && Ext.isDefined(this.columnSize)) {
							this.set('columnSize', record.data[key]);
						}
						if (isMax) {
							if (Ext.isDefined(this.columnSize)) {
								this.set('columnSize', record.data[key]);
							}
							this.set('allowedMaxValue', record.data[key] == 4001 ? 999999999 : record.data[key]);
						}
					}

					//options
					if (Ext.isDefined(this.options) && (key == 'selectValues' || key == 'choiceValues')) {
						//parse the options and fill them up
						var values = record.data[key];
						if (values) {
							if (this.parseChoiceControlConversionProperties) this.parseChoiceControlConversionProperties({
								choiceValues: values
							});
							else if (this.parseMultiChoiceControlConversionProperties) this.parseMultiChoiceControlConversionProperties({
								choiceValues: values
							})
						}
					}
				}
			}
		}
	},
	columnIsEditable$: function() {
		var isAdmin = this.rootVM.isAdmin,
			isNew = false;
		var record = this.parentVM.columnStore.findRecord('name', this.column, 0, false, false, true);
		if (record) {
			if (record.data.name == 'newColumn') isNew = true;
		} else isNew = true;

		return isAdmin && isNew;
	},
	/**
	 * Name to pass the value of this control to when submitting a form
	 */
	inputName: '',
	
	regWord: /[^\w]+/, 

	regNumber: /^[0-9]+/,

	inputNameIsValid$: function() {
		var result = true;

		if (this.regWord.test(this.inputName)) {
			result = this.localize('fieldNameInvalid');
		}

		if (result && this.regNumber.test(this.inputName)) {
			result = this.localize('fieldNameStartInvalid');
		}

		return result;
	},

	when_fieldName_changes_on_input_field_update_fieldLabel_automatically: {
		on: ['inputNameChanged'],
		action: function() {			
			if (this.inputType === 'INPUT' && this.inputName !== null) {
				var fieldLabelIsTracked = typeof this.fieldLabelIsChanged !== 'undefined';

				if(fieldLabelIsTracked) {
					if(!this.fieldLabelIsChanged) {
						this.set('fieldLabel', this.inputName);
					}
				} else {
					this.set('fieldLabel', this.inputName);
				}				
			}
		}
	},

	blurDatabaseFieldName: function() {
		if (this.inputType === 'DB') {
			if (this.column.indexOf('sys_') === 0) {
				this.set('column', this.column.toLowerCase());	
			} else if (this.column.indexOf('u_') !== 0) {
				this.parentVM.columnStore.clearFilter()
				this.set('column', 'u_' + this.column.toLowerCase());
			}

			//replace spaces automatically with _'s
			this.set('column', this.column.replace(/ /g, '_'));
		}		
	},
	/**
	 * Input type of the control.  Either 'DB' or 'INPUT'
	 */
	inputType: 'INPUT',

	/**
	 * Tracker to determine if the input radiobutton is selected
	 */
	inputTypeSelected$: function() {
		return this.inputType == 'INPUT'
	},
	inputTypeIsEnabled$: function() {
		return Ext.Array.indexOf(['ReferenceField', 'SequenceField', 'JournalField'], this.viewmodelName) == -1;
	},
	dbTypeIsEnabled$: function() {
		return this.parentVM.dbTable;
	},
	/**
	 * Tracker to determine if the db radiobutton is selected
	 */
	dbTypeSelected$: function() {
		return this.inputType == 'DB'
	},

	dbFieldName$: function() {
		var fieldName = this.inputType == 'INPUT' ? this.inputName : this.column;
		if (!fieldName) fieldName = this.localize('untitledField');
		return fieldName;
	},
	/**
	 * Formula for setting the inputType properly based on the change of
	 */
	whenTypeSelectionChanges_UpdateInputType$: {
		on: ['inputTypeSelectedChanged', 'dbTypeSelectedChanged'],
		action: function() {
			if (this.inputTypeSelected) this.set('inputType', 'INPUT');
			else this.set('inputType', 'DB');
		}
	},
	/**
	 * Determines whether the user has indicated if the input type is a db or input type
	 */
	hideColumnCombobox$: function() {
		return this.inputType == 'INPUT';
	},

	initMixin: function() {
		this.set('inputName', this.fieldLabel);
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getDatabaseControlConversionProperties) : this.serializeFunctions = [this.getDatabaseControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseDatabaseControlConversionProperties) : this.deserializeFunctions = [this.parseDatabaseControlConversionProperties];
	},

	/**
	 * Conversion method that turns a database control into server fields to be persisted in the db
	 */
	getDatabaseControlConversionProperties: function() {
		if (this.inputType == 'DB' && this.column.indexOf('sys_') !== 0 && this.column.indexOf('u_') !== 0) this.set('column', 'u_' + this.column);
		this.parentVM.columnStore.clearFilter();
		//when the exsiting field gets renamed..we need to use dbcolumnId to find it in the store
		var record = this.parentVM.columnStore.findRecord('dbcolumnId', this.dbcolumnId, 0, false, false, true);
		//when a new field gets created...and we want to use the existing dbtable.. then we need the db column name to map the field...
		if (!record)
			record = this.parentVM.columnStore.findRecord('name', this.column, 0, false, false, true);
		return {
			dbtable: this.table || this.parentVM.dbTable,
			dbcolumn: this.inputType == 'INPUT' ? '' : this.column,
			columnModelName: this.inputType == 'INPUT' ? '' : this.column,
			dbcolumnId: this.inputType == 'INPUT' ? '' : record ? record.data.dbcolumnId : '',
			sourceType: this.inputType,
			stringMaxLength: this.inputType == 'INPUT' ? this.maxLength : this.columnSize
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseDatabaseControlConversionProperties: function(config) {
		this.set('table', config.dbtable);
		this.set('inputType', config.sourceType);
		this.set('inputName', config.name);
		this.set('fieldLabel', config.fieldLabel);
		// this.set('inputTypeSelected', config.sourceType == 'INPUT');
		// this.set('dbTypeSelected', config.sourceType == 'DB');
		this.set('column', config.dbcolumn);
		if (this.inputType == 'DB' && config.stringMaxLength) {
			if (Ext.isDefined(this.columnSize)) this.set('columnSize', config.stringMaxLength);
			this.set('allowedMaxValue', config.stringMaxLength == 4001 ? 999999999 : config.stringMaxLength);
		}
		if (this.inputType == 'INPUT') {

		}
	}
});

glu.defModel('RS.formbuilder.ReferenceControl', {
	referenceTable: '',
	referenceDisplayColumn: '',
	referenceTarget: '',

	customTablesList: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getcustomtablesforref',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	referenceTableIsValid$: function() {
		if (!this.referenceTable) return this.localize('referenceTableInvalid');
		return true;
	},
	referenceDisplayColumnIsValid$: function() {
		if (!this.referenceDisplayColumn) return this.localize('referenceColumnInvalid');
		return true;
	},

	referenceColumnStore: {
		mtype: 'store',
		fields: ['name', 'dbcolumnId', 'displayName', 'dbtype', 'uiType', 'integerMinValue', 'integerMaxValue', 'decimalMinValue', 'decimalMaxValue', 'stringMinLength', 'stringMaxLength', 'uiStringMaxLength', 'selectValues', 'choiceValues', 'referenceTable', 'referenceDisplayColumn'],
		data: [],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getcustomtablecolumns',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	when_table_changes_update_columns: {
		on: ['referenceTableChanged'],
		action: function() {
			this.referenceColumnStore.load();
		}
	},

	initMixin: function() {
		this.customTablesList.load({
			scope: this,
			callback: this.customTablesLoaded
		});

		this.referenceColumnStore.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				tableName: this.referenceTable
			});
		}, this);

		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getReferenceControlConversionProperties) : this.serializeFunctions = [this.getReferenceControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseReferenceControlConversionProperties) : this.deserializeFunctions = [this.parseReferenceControlConversionProperties];
	},

	customTablesLoaded: function(records, operation, success) {
		Ext.each(records, function(record, index) {
			if (record.data.name.indexOf('_fu') != -1 && record.data.name.indexOf('_fu') == record.data.name.length - 3) {
				this.customTablesList.remove(record)
			}
		}, this)
	},

	getReferenceControlConversionProperties: function() {
		return {
			referenceTable: this.referenceTable,
			referenceDisplayColumn: this.referenceDisplayColumn,
			referenceTarget: this.referenceTarget
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseReferenceControlConversionProperties: function(config) {
		this.set('referenceTarget', config.referenceTarget);
		this.set('referenceTable', config.referenceTable);
		this.set('referenceDisplayColumn', config.referenceDisplayColumn);
		this.referenceColumnStore.load();
	}
});

glu.defModel('RS.formbuilder.ReferenceTableControl', {
	id: '',
	fieldLabel: '',
	fieldLabelIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceTable: '',
	referenceTableIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceColumn: '',
	referenceColumnIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceColumns: '',
	referenceColumnsIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceColumnsIsValid$: function() {
		return this.referenceColumns.length > 0 ? true : this.localize('invalidReferenceColumns');
	},
	allowReferenceTableAdd: true,
	allowReferenceTableAddIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	allowReferenceTableRemove: true,
	allowReferenceTableRemoveIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	allowReferenceTableView: true,
	allowReferenceTableViewIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceTableUrl: '',
	referenceTableUrlIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceTableViewPopup: false,
	referenceTableViewPopupIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceTableViewPopupWidth: 600,
	referenceTableViewPopupWidthIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1 && this.referenceTableViewPopup;
	},
	referenceTableViewPopupHeight: 600,
	referenceTableViewPopupHeightIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1 && this.referenceTableViewPopup;
	},
	referenceTableViewPopupTitle: '',
	referenceTableViewPopupTitleIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1 && this.referenceTableViewPopup;
	},

	referenceTableUrlNewTab: true,

	fields: ['fieldLabel', 'referenceTable', 'referenceColumn', 'referenceColumns', 'allowReferenceTableAdd', 'allowReferenceTableRemove', 'allowReferenceTableView', 'referenceTableUrl', 'referenceTableViewPopup', 'referenceTableViewPopupWidth', 'referenceTableViewPopupHeight', 'referenceTableViewPopupTitle'],

	when_field_changes_update_current_reference: {
		on: ['fieldLabelChanged', 'referenceTableChanged', 'referenceColumnChanged', 'referenceColumnsChanged', 'allowReferenceTableAddChanged', 'allowReferenceTableRemoveChanged', 'allowReferenceTableViewChanged', 'referenceTableUrlChanged', 'referenceTableViewPopupChanged', 'referenceTableViewPopupWidthChanged', 'referenceTableViewPopupHeightChanged', 'referenceTableViewPopupTitleChanged'],
		action: function() {
			//persist to current reference
			if (Ext.isObject(this.currentReference)) {
				this.currentReference.referenceColumnStore.removeAll();
				this.referenceColumnStore.each(function(record) {
					this.currentReference.referenceColumnStore.add(record);
				}, this);
				Ext.Object.each(this.currentReference.asObject(), function(attribute) {
					this.currentReference.set(attribute, this[attribute]);
				}, this);
			}
		}
	},
	when_reference_table_changes_update_reference_column_too: {
		on: ['referenceTableChanged'],
		action: function() {
			var record = this.referenceStore.findRecord('value', this.referenceTable, 0, false, false, true)
			if (record) {
				this.set('referenceColumn', record.data.name);
			}
		}
	},

	referenceTables: {
		mtype: 'list',
		autoParent: true
	},

	referenceTablesSelections: [],
	currentReference: 0,
	when_current_reference_changes_update_selection_in_grid: {
		on: 'currentReferenceChanged',
		action: function() {
			if (Ext.isObject(this.currentReference)) {
				this.changeReferenceTablesSelections([this.referenceTablesStore.getAt(this.referenceTables.indexOf(this.currentReference))]);
			}
		}
	},
	when_selection_changes_update_fields: {
		on: ['referenceTablesSelectionsChanged'],
		action: function() {
			var sel = this.referenceTablesSelections[0];
			if (sel) {
				var selection = this.referenceTables.getAt(this.referenceTablesStore.indexOf(sel));
				if (selection) {
					//Persist dependencies
					// if(Ext.isObject(this.currentReference)) {
					// 	var deps = this.getDependencyControlConversionProperties();
					// 	this.currentReference.parseDependencyControlConversionProperties(deps);
					// }
					var temp = this.currentReference;
					this.currentReference = null;
					Ext.Object.each(selection.asObject(), function(attribute) {
						if (Ext.isDefined(this[attribute]) && attribute != 'id') this.set(attribute, selection[attribute]);
					}, this);
					// this.parseDependencyControlConversionProperties(selection.getDependencyControlConversionProperties());
					this.currentReference = temp;
					this.set('currentReference', selection);
				}
			}
		}
	},

	referenceTablesStore: {
		mtype: 'store',
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'referenceTables'
		}],
		fields: ['fieldLabel']
	},

	referenceDisplayStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},

	referenceStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getReferenceTables',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	referenceColumnStore: {
		mtype: 'store',
		fields: ['name', 'dbcolumnId', 'displayName', 'dbtype', 'uiType', 'integerMinValue', 'integerMaxValue', 'decimalMinValue', 'decimalMaxValue', 'stringMinLength', 'stringMaxLength', 'uiStringMaxLength', 'selectValues', 'choiceValues', 'referenceTable', 'referenceDisplayColumn'],
		data: [],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getcustomtablecolumns',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	when_referenceTable_changes_update_reference_columns: {
		on: ['referenceTableChanged'],
		action: function() {
			this.set('referenceColumns', '');
			this.referenceColumnStore.load();
		}
	},

	referenceColumnsIsEnabled$: function() {
		return this.referenceTable;
	},

	referenceTableIsValid$: function() {
		if (!this.referenceTable) return this.localize('referenceTableInvalid')
		return true;
	},

	referenceTableUrlIsValid$: function() {
		if ((this.allowReferenceTableAdd || this.allowReferenceTableView) && !this.referenceTableUrl) return this.localize('referenceTableUrlInvalid');
		return true;
	},

	when_referenceTableUrl_changed_check_match_to_wiki: {
		on: ['referenceTableUrlChanged'],
		action: function() {
			if ((this.parentVM.wikiNames && this.parentVM.wikiNames.getById(this.referenceTableUrl)) || (this.parentVM.parentVM.wikiNames && this.parentVM.parentVM.wikiNames.getById(this.referenceTableUrl))) {
				Ext.defer(function() {
					this.set('referenceTableUrl', '/resolve/service/wiki/view/' + this.referenceTableUrl.split('.').join('/'));
				}, 100, this);
			}
		}
	},

	initMixin: function() {
		this.referenceStore.load({
			params: {
				tableName: this.parentVM.dbTable
			}
		});
		this.referenceColumnStore.on('beforeload', function(store, operation, eOpts) {
			if (!this.referenceTable) return false
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				tableName: this.referenceTable
			});
		}, this);

		//Initialize first reference
		this.referenceTables.add(this.model({
			mtype: 'ReferenceTable',
			fieldLabel: this.localize('references')
		}));
		//Initially select the only record so that all the fields are initialized properly
		this.set('currentReference', this.referenceTables.getAt(0));
		this.changeReferenceTablesSelections([this.referenceTablesStore.getAt(0)]);

		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getReferenceTableControlConversionProperties) : this.serializeFunctions = [this.getReferenceTableControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseReferenceTableControlConversionProperties) : this.deserializeFunctions = [this.parseReferenceTableControlConversionProperties];
	},

	changeReferenceTablesSelections: function(selections) {
		this.set('referenceTablesSelections', selections);
		this.referenceTablesStore.fireEvent('cellSelectionVMChanged', this, selections);
	},

	addReference: function() {
		var selectedIndex = -1;
		var sel = this.referenceTablesSelections[0];
		if (sel) selectedIndex = this.referenceTablesStore.indexOf(sel);
		if (selectedIndex == -1) selectedIndex = this.referenceTables.length;
		else selectedIndex++;
		this.referenceTables.insert(selectedIndex, this.model({
			mtype: 'ReferenceTable',
			fieldLabel: this.localize('unknownReference')
		}));

		this.set('referenceTablesSelections', [this.referenceTablesStore.getAt(selectedIndex)]);
	},
	removeReference: function() {
		var sel = this.referenceTablesSelections[0];
		if (sel) {
			var selection = this.referenceTables.getAt(this.referenceTablesStore.indexOf(sel));
			if (selection) {
				this.referenceTables.remove(selection);
				this.changeReferenceTablesSelections([this.referenceTablesStore.getAt(0)]);
			}
		}
	},
	removeReferenceIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},

	getReferenceTableControlConversionProperties: function() {

		//Go through each of the ReferenceTables and combine them to persist on the backend
		var displayName = '',
			refGridReferenceByTable = '',
			refGridReferenceColumnName = '',
			refGridSelectedReferenceColumns = '',
			allowReferenceTableAdd = '',
			allowReferenceTableRemove = '',
			allowReferenceTableView = '',
			referenceTableUrl = '',
			referenceTableViewPopup = '',
			referenceTableViewPopupWidth = '',
			referenceTableViewPopupHeight = '',
			referenceTableViewPopupTitle = '';

		this.referenceTables.foreach(function(referenceTable, index) {
			displayName += (index > 0 ? '|' : '') + referenceTable.fieldLabel;
			refGridReferenceByTable += (index > 0 ? '|' : '') + referenceTable.referenceTable;
			refGridReferenceColumnName += (index > 0 ? '|' : '') + referenceTable.referenceColumn;
			allowReferenceTableAdd += (index > 0 ? '|' : '') + referenceTable.allowReferenceTableAdd;
			allowReferenceTableRemove += (index > 0 ? '|' : '') + referenceTable.allowReferenceTableRemove;
			allowReferenceTableView += (index > 0 ? '|' : '') + referenceTable.allowReferenceTableView;
			referenceTableUrl += (index > 0 ? '|' : '') + referenceTable.referenceTableUrl;
			referenceTableViewPopup += (index > 0 ? '|' : '') + referenceTable.referenceTableViewPopup;
			referenceTableViewPopupWidth += (index > 0 ? '|' : '') + referenceTable.referenceTableViewPopupWidth;
			referenceTableViewPopupHeight += (index > 0 ? '|' : '') + referenceTable.referenceTableViewPopupHeight;
			referenceTableViewPopupTitle += (index > 0 ? '|' : '') + referenceTable.referenceTableViewPopupTitle;

			var displayColumns = [];

			Ext.each(referenceTable.referenceColumns, function(refColumn) {
				var rec = referenceTable.referenceColumnStore.findRecord('name', refColumn, 0, false, false, true);
				if (rec) {
					displayColumns.push({
						name: rec.data.displayName,
						value: refColumn,
						dbtype: rec.data.dbtype
					});
				}
			}, this);
			refGridSelectedReferenceColumns += (index > 0 ? '|' : '') + (displayColumns.length > 0 ? Ext.encode(displayColumns) : '');
		}, this);

		return {
			id: this.id,
			sourceType: 'INPUT',
			name: 'u_referenceTable_',
			displayName: displayName,
			refGridReferenceByTable: refGridReferenceByTable,
			refGridReferenceColumnName: refGridReferenceColumnName,
			refGridSelectedReferenceColumns: refGridSelectedReferenceColumns,
			allowReferenceTableAdd: allowReferenceTableAdd,
			allowReferenceTableRemove: allowReferenceTableRemove,
			allowReferenceTableView: allowReferenceTableView,
			referenceTableUrl: referenceTableUrl,
			refGridViewPopup: referenceTableViewPopup,
			refGridViewPopupWidth: referenceTableViewPopupWidth,
			refGridViewPopupHeight: referenceTableViewPopupHeight,
			refGridViewTitle: referenceTableViewPopupTitle
		}
	},

	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseReferenceTableControlConversionProperties: function(config) {
		this.set('id', config.id);

		//Parse out each Reference Table from each of the fields
		var displayNames = config.displayName ? config.displayName.split('|') : [],
			referenceTables = config.refGridReferenceByTable ? config.refGridReferenceByTable.split('|') : [],
			refGridReferenceColumnName = config.refGridReferenceColumnName ? config.refGridReferenceColumnName.split('|') : [],
			referenceColumns = config.refGridSelectedReferenceColumns ? config.refGridSelectedReferenceColumns.split('|') : [],
			allowReferenceTableAdd = config.allowReferenceTableAdd ? config.allowReferenceTableAdd.split('|') : [],
			allowReferenceTableRemove = config.allowReferenceTableRemove ? config.allowReferenceTableRemove.split('|') : [],
			allowReferenceTableView = config.allowReferenceTableView ? config.allowReferenceTableView.split('|') : [],
			referenceTableUrl = config.referenceTableUrl ? config.referenceTableUrl.split('|') : [],
			referenceTableViewPopup = config.refGridViewPopup ? config.refGridViewPopup.split('|') : [],
			referenceTableViewPopupWidth = config.refGridViewPopupWidth ? config.refGridViewPopupWidth.split('|') : [],
			referenceTableViewPopupHeight = config.refGridViewPopupHeight ? config.refGridViewPopupHeight.split('|') : [],
			referenceTableViewPopupTitle = config.refGridViewTitle ? config.refGridViewTitle.split('|') : [],
			len = displayNames.length,
			i;

		for (i = 0; i < len; i++) {
			var cols = referenceColumns[i] ? Ext.decode(referenceColumns[i]) : [],
				displayColumns = [];

			Ext.each(cols, function(col) {
				displayColumns.push(col.value);
			})

			var refTable = this.model({
				mtype: 'ReferenceTable',
				fieldLabel: displayNames[i],
				referenceTable: referenceTables[i],
				referenceColumn: refGridReferenceColumnName[i],
				referenceColumns: displayColumns,
				allowReferenceTableAdd: allowReferenceTableAdd[i] == 'true',
				allowReferenceTableRemove: allowReferenceTableRemove[i] == 'true',
				allowReferenceTableView: allowReferenceTableView[i] == 'true',
				referenceTableUrl: referenceTableUrl[i],
				referenceTableViewPopup: referenceTableViewPopup[i] == 'true',
				referenceTableViewPopupWidth: Number(referenceTableViewPopupWidth[i]),
				referenceTableViewPopupHeight: Number(referenceTableViewPopupHeight[i]),
				referenceTableViewPopupTitle: referenceTableViewPopupTitle[i]
			});

			refTable.referenceColumnStore.load({
				params: {
					tableName: refTable.referenceTable
				}
			})
			this.referenceTables.add(refTable)
		}
		if (len > 0) {
			this.referenceTables.removeAt(0)
			this.set('currentReference', this.referenceTables.getAt(0))
		}
	}
});

/**
 * Mixin defiinition for a GroupPickerControl
 * Used in the user picker detail view, and keeps track of the groups to filter when displaying the form to the user
 */
glu.defModel('RS.formbuilder.GroupPickerControl', {
	/**
	 * Pipe ( | ) delimited list of groups to get users from to display to the user when displaying the user picker control
	 */
	groups: '',
	/**
	 * Store for the dropdown that allows the user to choose which groups the user should be allowed to pick from
	 */
	groupStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getUserGroups',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	teams: '',
	recurseThroughTeams: false,
	teamStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getAllTeams',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	teamList: '',
	recurseThroughTeamsList: false,

	allowAssignToMe: false,
	autoAssignToMe: false,

	initMixin: function() {
		this.groupStore.load();
		this.teamStore.load();
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getGroupsControlConversionProperties) : this.serializeFunctions = [this.getGroupsControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseGroupsControlConversionProperties) : this.deserializeFunctions = [this.parseGroupsControlConversionProperties];
	},
	/**
	 * Conversion method that turns a group picker control into server fields to be persisted in the db
	 */
	getGroupsControlConversionProperties: function() {
		return {
			groups: Ext.isArray(this.groups) ? this.groups.join('|') : this.groups,
			usersOfTeams: Ext.isArray(this.teams) ? this.teams.join('|') : this.teams,
			teamsOfTeams: Ext.isArray(this.teamList) ? this.teamList.join('|') : this.teamList,
			recurseUsersOfTeam: this.recurseThroughTeams,
			recurseTeamsOfTeam: this.recurseThroughTeamsList,
			allowAssignToMe: this.allowAssignToMe,
			autoAssignToMe: this.autoAssignToMe
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseGroupsControlConversionProperties: function(config) {
		this.set('groups', config.groups ? config.groups.split('|') : '');
		this.set('teams', config.usersOfTeams ? config.usersOfTeams.split('|') : '');
		this.set('teamList', config.teamsOfTeams ? config.teamsOfTeams.split('|') : '');
		this.set('recurseThroughTeams', config.recurseUsersOfTeam);
		this.set('recurseThroughTeamsList', config.recurseTeamsOfTeam);
		this.set('allowAssignToMe', config.allowAssignToMe);
		this.set('autoAssignToMe', config.autoAssignToMe)
	}
});

/**
 * Mixin defiinition for a TeamPickerControl
 * Used in the team picker detail view, and keeps track of the teams to filter when displaying the form to the user
 */
glu.defModel('RS.formbuilder.TeamPickerControl', {

	teamPickerTeamsOfTeams: '',
	recurseThroughTeamsList: false,
	teamStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getAllTeams',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	initMixin: function() {
		this.teamStore.load();
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getTeamControlConversionProperties) : this.serializeFunctions = [this.getTeamControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseTeamControlConversionProperties) : this.deserializeFunctions = [this.parseTeamControlConversionProperties];
	},
	/**
	 * Conversion method that turns a group picker control into server fields to be persisted in the db
	 */
	getTeamControlConversionProperties: function() {
		return {
			teamPickerTeamsOfTeams: Ext.isArray(this.teamPickerTeamsOfTeams) ? this.teamPickerTeamsOfTeams.join('|') : this.teamPickerTeamsOfTeams,
			recurseTeamsOfTeam: this.recurseThroughTeamsList
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseTeamControlConversionProperties: function(config) {
		if (!config.teamPickerTeamsOfTeams) config.teamPickerTeamsOfTeams = '';
		this.set('teamPickerTeamsOfTeams', config.teamPickerTeamsOfTeams ? config.teamPickerTeamsOfTeams.split('|') : '');
		this.set('recurseThroughTeamsList', config.recurseTeamsOfTeam);
	}
});

glu.defModel('RS.formbuilder.NameControl', {
	nameSeparator: '|&|',

	showFirstName: true,
	showMiddleInitial: true,
	showMiddleName: false,
	showLastName: true,

	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getNameControlConversionProperties) : this.serializeFunctions = [this.getNameControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseNameControlConversionProperties) : this.deserializeFunctions = [this.parseNameControlConversionProperties];
	},

	/**
	 * Conversion method that turns a name control into server fields to be persisted in the db
	 */
	getNameControlConversionProperties: function() {
		return {
			selectValues: [!this.showFirstName, !this.showMiddleName, !this.showMiddleInitial, !this.showLastName].join(this.nameSeparator)
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseNameControlConversionProperties: function(config) {
		var vals = config.selectValues.split(this.nameSeparator);
		this.set('showFirstName', vals[0]);
		this.set('showMiddleName', vals[1]);
		this.set('showMiddleInitial', vals[2]);
		this.set('showLastName', vals[3]);
	}
});

glu.defModel('RS.formbuilder.LinkControl', {
	linkTarget: '_blank',
	linkTargetStore: {
		mtype: 'store',
		fields: ['name'],
		data: [{
			name: '_blank'
		}, {
			name: '_self'
		}, {
			name: '_parent'
		}, {
			name: '_top'
		}]
	},

	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getLinkControlConversionProperties) : this.serializeFunctions = [this.getLinkControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseLinkControlConversionProperties) : this.deserializeFunctions = [this.parseLinkControlConversionProperties];
	},

	getLinkControlConversionProperties: function() {
		return {
			linkTarget: this.linkTarget
		}
	},
	parseLinkControlConversionProperties: function(config) {
		this.set('linkTarget', config.linkTarget);
	}
});

glu.defModel('RS.formbuilder.FileUploadControl', {
	id: '',
	fieldLabel: '',
	allowUpload: true,
	allowRemove: true,
	allowDownload: true,
	allowedFileTypes: '*',
	referenceColumnName: '',

	// fileTypes: {
	// 	mtype: 'store',
	// 	fields: ['name', 'value']
	// },
	tableName: '',

	fileStore: {
		mtype: 'store',
		fields: ['name']
	},

	inputType: 'INPUT',
	inputName: 'u_fileupload',

	initMixin: function() {
		this.set('fieldLabel', this.localize('attachments'));
		//Add in suggestions for file types
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getFileUploadControlConversionProperties) : this.serializeFunctions = [this.getFileUploadControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseFileUploadControlConversionProperties) : this.deserializeFunctions = [this.parseFileUploadControlConversionProperties];
	},

	getFileUploadControlConversionProperties: function() {
		return {
			id: this.tableName == Ext.String.format('{0}_fu', this.parentVM.dbTable) ? this.id : '',
			fileUploadTableName: Ext.String.format('{0}_fu', this.parentVM.dbTable),
			allowUpload: this.allowUpload,
			allowRemove: this.allowRemove,
			allowDownload: this.allowDownload,
			displayName: this.fieldLabel,
			allowedFileTypes: this.allowedFileTypes,
			sourceType: this.inputType,
			name: this.inputName,
			referenceColumnName: this.referenceColumnName
		}
	},
	parseFileUploadControlConversionProperties: function(config) {
		this.set('id', config.id);
		this.set('fieldLabel', config.displayName);
		this.set('tableName', config.fileUploadTableName || '');
		this.set('allowUpload', config.allowUpload);
		this.set('allowRemove', config.allowRemove);
		this.set('allowDownload', config.allowDownload);
		this.set('allowedFileTypes', config.allowedFileTypes);
		this.set('referenceColumnName', config.referenceColumnName);
	}
});

glu.defModel('RS.formbuilder.DependencyControl', {
	dependenciesStore: {
		mtype: 'store',
		fields: ['id', 'action', 'actionDisplay', 'target', 'condition', 'conditionDisplay', 'value', 'actionOptions', 'actionOptionsDisplay']
	},

	dependenciesJson: '',

	depDoubleClick: function(record, item, index, e, eOpts) {
		this.set('dependenciesSelections', [record]);
		this.editDependency();
	},

	initMixin: function() {
		this.parseDependencyControlConversionProperties(this.dependenciesJson);
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getDependencyControlConversionProperties) : this.serializeFunctions = [this.getDependencyControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseDependencyControlConversionProperties) : this.deserializeFunctions = [this.parseDependencyControlConversionProperties];
	},
	translateDependency: function(dep) {
		return Ext.applyIf(dep, {
			actionDisplay: this.localize(dep.action),
			conditionDisplay: this.localize(dep.condition)
		});
	},
	addDependency: function() {
		//open a window to display the dependency options
		//on close of the window, then you can really add the dependency
		this.open({
			mtype: 'Dependency'
		});
	},
	reallyAddDependency: function(dep) {
		this.dependenciesStore.add(this.translateDependency(dep));
	},
	editDependency: function() {
		var current = this.dependenciesSelections[0];
		this.open(Ext.apply({
			mtype: 'Dependency',
			isEdit: true,
			initialValue: current.data.value,
			initialActionOptions: current.data.actionOptions
		}, current.data));
	},
	editDependencyIsEnabled$: function() {
		return this.dependenciesSelections.length == 1;
	},
	reallyEditDependency: function(dep) {
		var current = this.dependenciesSelections[0],
			translated = this.translateDependency(dep);
		for (var k in translated) {
			current.set(k, dep[k]);
		}
	},
	dependenciesSelections: [],
	removeDependency: function() {
		this.dependenciesStore.remove(this.dependenciesSelections[0]);
	},
	removeDependencyIsEnabled$: function() {
		return this.dependenciesSelections.length == 1;
	},
	getDependencyControlConversionProperties: function() {
		var deps = [];
		this.dependenciesStore.each(function(record) {
			// var actionName = record.data.action,
			// 	actionRecord = this.dependencyActionStore.findRecord('name', actionName, 0, false, false, true);
			// if(actionRecord) actionName = actionRecord.data.value;
			// var conditionName = record.data.condition,
			// 	conditionRecord = this.dependencyConditionStore.findRecord('name', conditionName, 0, false, false, true);
			// if(conditionRecord) conditionName = conditionRecord.data.value;
			if (record.data.target) {
				deps.push({
					id: record.data.id,
					action: record.data.action,
					condition: record.data.condition,
					target: record.data.target,
					actionOptions: record.data.actionOptions,
					value: record.data.value
				});
				// deps.push(Ext.apply(record.data, {
				// 	action: actionName,
				// 	condition: conditionName
				// }));
			}
		}, this);
		return {
			dependencies: deps
		};
	},


	parseDependencyControlConversionProperties: function(config) {
		var deps = config;
		if (!Ext.isArray(config) && config.dependencies) {
			deps = config.dependencies;
		}

		if (Ext.isArray(deps)) {
			this.dependenciesStore.removeAll();
			Ext.each(deps, function(item) {
				this.dependenciesStore.add(this.translateDependency(item));
			}, this);
		}
	}
});

glu.defModel('RS.formbuilder.AddressControl', {
	country: 'US',
	state: '',

	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getAddressControlConversionProperties) : this.serializeFunctions = [this.getAddressControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseAddressControlConversionProperties) : this.deserializeFunctions = [this.parseAddressControlConversionProperties];
	},
	getAddressControlConversionProperties: function() {
		return {
			defaultValue: Ext.String.format('|&||&||&|{0}|&||&|{1}', this.state, this.country)
		}
	},
	parseAddressControlConversionProperties: function(config) {
		var vals = config.defaultValue ? config.defaultValue.split('|&|') : '';
		if (vals.length == 6) {
			this.set('state', vals[3]);
			this.set('country', vals[5]);
		}
	},
	countryStore: {
		mtype: 'store',
		fields: ['name', 'code'],
		data: [{
			name: 'Afghanistan',
			code: 'AF'
		}, {
			name: 'Åland Islands',
			code: 'AX'
		}, {
			name: 'Albania',
			code: 'AL'
		}, {
			name: 'Algeria',
			code: 'DZ'
		}, {
			name: 'American Samoa',
			code: 'AS'
		}, {
			name: 'AndorrA',
			code: 'AD'
		}, {
			name: 'Angola',
			code: 'AO'
		}, {
			name: 'Anguilla',
			code: 'AI'
		}, {
			name: 'Antarctica',
			code: 'AQ'
		}, {
			name: 'Antigua and Barbuda',
			code: 'AG'
		}, {
			name: 'Argentina',
			code: 'AR'
		}, {
			name: 'Armenia',
			code: 'AM'
		}, {
			name: 'Aruba',
			code: 'AW'
		}, {
			name: 'Australia',
			code: 'AU'
		}, {
			name: 'Austria',
			code: 'AT'
		}, {
			name: 'Azerbaijan',
			code: 'AZ'
		}, {
			name: 'Bahamas',
			code: 'BS'
		}, {
			name: 'Bahrain',
			code: 'BH'
		}, {
			name: 'Bangladesh',
			code: 'BD'
		}, {
			name: 'Barbados',
			code: 'BB'
		}, {
			name: 'Belarus',
			code: 'BY'
		}, {
			name: 'Belgium',
			code: 'BE'
		}, {
			name: 'Belize',
			code: 'BZ'
		}, {
			name: 'Benin',
			code: 'BJ'
		}, {
			name: 'Bermuda',
			code: 'BM'
		}, {
			name: 'Bhutan',
			code: 'BT'
		}, {
			name: 'Bolivia',
			code: 'BO'
		}, {
			name: 'Bosnia and Herzegovina',
			code: 'BA'
		}, {
			name: 'Botswana',
			code: 'BW'
		}, {
			name: 'Bouvet Island',
			code: 'BV'
		}, {
			name: 'Brazil',
			code: 'BR'
		}, {
			name: 'British Indian Ocean Territory',
			code: 'IO'
		}, {
			name: 'Brunei Darussalam',
			code: 'BN'
		}, {
			name: 'Bulgaria',
			code: 'BG'
		}, {
			name: 'Burkina Faso',
			code: 'BF'
		}, {
			name: 'Burundi',
			code: 'BI'
		}, {
			name: 'Cambodia',
			code: 'KH'
		}, {
			name: 'Cameroon',
			code: 'CM'
		}, {
			name: 'Canada',
			code: 'CA'
		}, {
			name: 'Cape Verde',
			code: 'CV'
		}, {
			name: 'Cayman Islands',
			code: 'KY'
		}, {
			name: 'Central African Republic',
			code: 'CF'
		}, {
			name: 'Chad',
			code: 'TD'
		}, {
			name: 'Chile',
			code: 'CL'
		}, {
			name: 'China',
			code: 'CN'
		}, {
			name: 'Christmas Island',
			code: 'CX'
		}, {
			name: 'Cocos (Keeling) Islands',
			code: 'CC'
		}, {
			name: 'Colombia',
			code: 'CO'
		}, {
			name: 'Comoros',
			code: 'KM'
		}, {
			name: 'Congo',
			code: 'CG'
		}, {
			name: 'Congo, The Democratic Republic of the',
			code: 'CD'
		}, {
			name: 'Cook Islands',
			code: 'CK'
		}, {
			name: 'Costa Rica',
			code: 'CR'
		}, {
			name: 'Cote D\'Ivoire',
			code: 'CI'
		}, {
			name: 'Croatia',
			code: 'HR'
		}, {
			name: 'Cuba',
			code: 'CU'
		}, {
			name: 'Cyprus',
			code: 'CY'
		}, {
			name: 'Czech Republic',
			code: 'CZ'
		}, {
			name: 'Denmark',
			code: 'DK'
		}, {
			name: 'Djibouti',
			code: 'DJ'
		}, {
			name: 'Dominica',
			code: 'DM'
		}, {
			name: 'Dominican Republic',
			code: 'DO'
		}, {
			name: 'Ecuador',
			code: 'EC'
		}, {
			name: 'Egypt',
			code: 'EG'
		}, {
			name: 'El Salvador',
			code: 'SV'
		}, {
			name: 'Equatorial Guinea',
			code: 'GQ'
		}, {
			name: 'Eritrea',
			code: 'ER'
		}, {
			name: 'Estonia',
			code: 'EE'
		}, {
			name: 'Ethiopia',
			code: 'ET'
		}, {
			name: 'Falkland Islands (Malvinas)',
			code: 'FK'
		}, {
			name: 'Faroe Islands',
			code: 'FO'
		}, {
			name: 'Fiji',
			code: 'FJ'
		}, {
			name: 'Finland',
			code: 'FI'
		}, {
			name: 'France',
			code: 'FR'
		}, {
			name: 'French Guiana',
			code: 'GF'
		}, {
			name: 'French Polynesia',
			code: 'PF'
		}, {
			name: 'French Southern Territories',
			code: 'TF'
		}, {
			name: 'Gabon',
			code: 'GA'
		}, {
			name: 'Gambia',
			code: 'GM'
		}, {
			name: 'Georgia',
			code: 'GE'
		}, {
			name: 'Germany',
			code: 'DE'
		}, {
			name: 'Ghana',
			code: 'GH'
		}, {
			name: 'Gibraltar',
			code: 'GI'
		}, {
			name: 'Greece',
			code: 'GR'
		}, {
			name: 'Greenland',
			code: 'GL'
		}, {
			name: 'Grenada',
			code: 'GD'
		}, {
			name: 'Guadeloupe',
			code: 'GP'
		}, {
			name: 'Guam',
			code: 'GU'
		}, {
			name: 'Guatemala',
			code: 'GT'
		}, {
			name: 'Guernsey',
			code: 'GG'
		}, {
			name: 'Guinea',
			code: 'GN'
		}, {
			name: 'Guinea-Bissau',
			code: 'GW'
		}, {
			name: 'Guyana',
			code: 'GY'
		}, {
			name: 'Haiti',
			code: 'HT'
		}, {
			name: 'Heard Island and Mcdonald Islands',
			code: 'HM'
		}, {
			name: 'Holy See (Vatican City State)',
			code: 'VA'
		}, {
			name: 'Honduras',
			code: 'HN'
		}, {
			name: 'Hong Kong',
			code: 'HK'
		}, {
			name: 'Hungary',
			code: 'HU'
		}, {
			name: 'Iceland',
			code: 'IS'
		}, {
			name: 'India',
			code: 'IN'
		}, {
			name: 'Indonesia',
			code: 'ID'
		}, {
			name: 'Iran, Islamic Republic Of',
			code: 'IR'
		}, {
			name: 'Iraq',
			code: 'IQ'
		}, {
			name: 'Ireland',
			code: 'IE'
		}, {
			name: 'Isle of Man',
			code: 'IM'
		}, {
			name: 'Israel',
			code: 'IL'
		}, {
			name: 'Italy',
			code: 'IT'
		}, {
			name: 'Jamaica',
			code: 'JM'
		}, {
			name: 'Japan',
			code: 'JP'
		}, {
			name: 'Jersey',
			code: 'JE'
		}, {
			name: 'Jordan',
			code: 'JO'
		}, {
			name: 'Kazakhstan',
			code: 'KZ'
		}, {
			name: 'Kenya',
			code: 'KE'
		}, {
			name: 'Kiribati',
			code: 'KI'
		}, {
			name: 'Korea, Democratic People\'S Republic of',
			code: 'KP'
		}, {
			name: 'Korea, Republic of',
			code: 'KR'
		}, {
			name: 'Kuwait',
			code: 'KW'
		}, {
			name: 'Kyrgyzstan',
			code: 'KG'
		}, {
			name: 'Lao People\'S Democratic Republic',
			code: 'LA'
		}, {
			name: 'Latvia',
			code: 'LV'
		}, {
			name: 'Lebanon',
			code: 'LB'
		}, {
			name: 'Lesotho',
			code: 'LS'
		}, {
			name: 'Liberia',
			code: 'LR'
		}, {
			name: 'Libyan Arab Jamahiriya',
			code: 'LY'
		}, {
			name: 'Liechtenstein',
			code: 'LI'
		}, {
			name: 'Lithuania',
			code: 'LT'
		}, {
			name: 'Luxembourg',
			code: 'LU'
		}, {
			name: 'Macao',
			code: 'MO'
		}, {
			name: 'Macedonia, The Former Yugoslav Republic of',
			code: 'MK'
		}, {
			name: 'Madagascar',
			code: 'MG'
		}, {
			name: 'Malawi',
			code: 'MW'
		}, {
			name: 'Malaysia',
			code: 'MY'
		}, {
			name: 'Maldives',
			code: 'MV'
		}, {
			name: 'Mali',
			code: 'ML'
		}, {
			name: 'Malta',
			code: 'MT'
		}, {
			name: 'Marshall Islands',
			code: 'MH'
		}, {
			name: 'Martinique',
			code: 'MQ'
		}, {
			name: 'Mauritania',
			code: 'MR'
		}, {
			name: 'Mauritius',
			code: 'MU'
		}, {
			name: 'Mayotte',
			code: 'YT'
		}, {
			name: 'Mexico',
			code: 'MX'
		}, {
			name: 'Micronesia, Federated States of',
			code: 'FM'
		}, {
			name: 'Moldova, Republic of',
			code: 'MD'
		}, {
			name: 'Monaco',
			code: 'MC'
		}, {
			name: 'Mongolia',
			code: 'MN'
		}, {
			name: 'Montserrat',
			code: 'MS'
		}, {
			name: 'Morocco',
			code: 'MA'
		}, {
			name: 'Mozambique',
			code: 'MZ'
		}, {
			name: 'Myanmar',
			code: 'MM'
		}, {
			name: 'Namibia',
			code: 'NA'
		}, {
			name: 'Nauru',
			code: 'NR'
		}, {
			name: 'Nepal',
			code: 'NP'
		}, {
			name: 'Netherlands',
			code: 'NL'
		}, {
			name: 'Netherlands Antilles',
			code: 'AN'
		}, {
			name: 'New Caledonia',
			code: 'NC'
		}, {
			name: 'New Zealand',
			code: 'NZ'
		}, {
			name: 'Nicaragua',
			code: 'NI'
		}, {
			name: 'Niger',
			code: 'NE'
		}, {
			name: 'Nigeria',
			code: 'NG'
		}, {
			name: 'Niue',
			code: 'NU'
		}, {
			name: 'Norfolk Island',
			code: 'NF'
		}, {
			name: 'Northern Mariana Islands',
			code: 'MP'
		}, {
			name: 'Norway',
			code: 'NO'
		}, {
			name: 'Oman',
			code: 'OM'
		}, {
			name: 'Pakistan',
			code: 'PK'
		}, {
			name: 'Palau',
			code: 'PW'
		}, {
			name: 'Palestinian Territory, Occupied',
			code: 'PS'
		}, {
			name: 'Panama',
			code: 'PA'
		}, {
			name: 'Papua New Guinea',
			code: 'PG'
		}, {
			name: 'Paraguay',
			code: 'PY'
		}, {
			name: 'Peru',
			code: 'PE'
		}, {
			name: 'Philippines',
			code: 'PH'
		}, {
			name: 'Pitcairn',
			code: 'PN'
		}, {
			name: 'Poland',
			code: 'PL'
		}, {
			name: 'Portugal',
			code: 'PT'
		}, {
			name: 'Puerto Rico',
			code: 'PR'
		}, {
			name: 'Qatar',
			code: 'QA'
		}, {
			name: 'Reunion',
			code: 'RE'
		}, {
			name: 'Romania',
			code: 'RO'
		}, {
			name: 'Russian Federation',
			code: 'RU'
		}, {
			name: 'RWANDA',
			code: 'RW'
		}, {
			name: 'Saint Helena',
			code: 'SH'
		}, {
			name: 'Saint Kitts and Nevis',
			code: 'KN'
		}, {
			name: 'Saint Lucia',
			code: 'LC'
		}, {
			name: 'Saint Pierre and Miquelon',
			code: 'PM'
		}, {
			name: 'Saint Vincent and the Grenadines',
			code: 'VC'
		}, {
			name: 'Samoa',
			code: 'WS'
		}, {
			name: 'San Marino',
			code: 'SM'
		}, {
			name: 'Sao Tome and Principe',
			code: 'ST'
		}, {
			name: 'Saudi Arabia',
			code: 'SA'
		}, {
			name: 'Senegal',
			code: 'SN'
		}, {
			name: 'Serbia and Montenegro',
			code: 'CS'
		}, {
			name: 'Seychelles',
			code: 'SC'
		}, {
			name: 'Sierra Leone',
			code: 'SL'
		}, {
			name: 'Singapore',
			code: 'SG'
		}, {
			name: 'Slovakia',
			code: 'SK'
		}, {
			name: 'Slovenia',
			code: 'SI'
		}, {
			name: 'Solomon Islands',
			code: 'SB'
		}, {
			name: 'Somalia',
			code: 'SO'
		}, {
			name: 'South Africa',
			code: 'ZA'
		}, {
			name: 'South Georgia and the South Sandwich Islands',
			code: 'GS'
		}, {
			name: 'Spain',
			code: 'ES'
		}, {
			name: 'Sri Lanka',
			code: 'LK'
		}, {
			name: 'Sudan',
			code: 'SD'
		}, {
			name: 'Suriname',
			code: 'SR'
		}, {
			name: 'Svalbard and Jan Mayen',
			code: 'SJ'
		}, {
			name: 'Swaziland',
			code: 'SZ'
		}, {
			name: 'Sweden',
			code: 'SE'
		}, {
			name: 'Switzerland',
			code: 'CH'
		}, {
			name: 'Syrian Arab Republic',
			code: 'SY'
		}, {
			name: 'Taiwan, Province of China',
			code: 'TW'
		}, {
			name: 'Tajikistan',
			code: 'TJ'
		}, {
			name: 'Tanzania, United Republic of',
			code: 'TZ'
		}, {
			name: 'Thailand',
			code: 'TH'
		}, {
			name: 'Timor-Leste',
			code: 'TL'
		}, {
			name: 'Togo',
			code: 'TG'
		}, {
			name: 'Tokelau',
			code: 'TK'
		}, {
			name: 'Tonga',
			code: 'TO'
		}, {
			name: 'Trinidad and Tobago',
			code: 'TT'
		}, {
			name: 'Tunisia',
			code: 'TN'
		}, {
			name: 'Turkey',
			code: 'TR'
		}, {
			name: 'Turkmenistan',
			code: 'TM'
		}, {
			name: 'Turks and Caicos Islands',
			code: 'TC'
		}, {
			name: 'Tuvalu',
			code: 'TV'
		}, {
			name: 'Uganda',
			code: 'UG'
		}, {
			name: 'Ukraine',
			code: 'UA'
		}, {
			name: 'United Arab Emirates',
			code: 'AE'
		}, {
			name: 'United Kingdom',
			code: 'GB'
		}, {
			name: 'United States',
			code: 'US'
		}, {
			name: 'United States Minor Outlying Islands',
			code: 'UM'
		}, {
			name: 'Uruguay',
			code: 'UY'
		}, {
			name: 'Uzbekistan',
			code: 'UZ'
		}, {
			name: 'Vanuatu',
			code: 'VU'
		}, {
			name: 'Venezuela',
			code: 'VE'
		}, {
			name: 'Viet Nam',
			code: 'VN'
		}, {
			name: 'Virgin Islands, British',
			code: 'VG'
		}, {
			name: 'Virgin Islands, U.S.',
			code: 'VI'
		}, {
			name: 'Wallis and Futuna',
			code: 'WF'
		}, {
			name: 'Western Sahara',
			code: 'EH'
		}, {
			name: 'Yemen',
			code: 'YE'
		}, {
			name: 'Zambia',
			code: 'ZM'
		}, {
			name: 'Zimbabwe',
			code: 'ZW'
		}]
	}
});

glu.defModel('RS.formbuilder.TooltipControl', {
	tooltip: '',
	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getTooltipControlConversionProperties) : this.serializeFunctions = [this.getTooltipControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseTooltipControlConversionProperties) : this.deserializeFunctions = [this.parseTooltipControlConversionProperties];
	},
	/**
	 * Conversion method that turns a time control into server fields to be persisted in the db
	 */
	getTooltipControlConversionProperties: function() {
		return {
			tooltip: this.tooltip
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseTooltipControlConversionProperties: function(config) {
		this.set('tooltip', config.tooltip);
	}
});

glu.defModel('RS.formbuilder.UrlControl', {
	inputName: 'u_urlcontrol',
	inputType: 'INPUT',

	referenceTableUrl: '',

	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getUrlControlConversionProperties) : this.serializeFunctions = [this.getUrlControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseUrlControlConversionProperties) : this.deserializeFunctions = [this.parseUrlControlConversionProperties];
		this.set('inputName', this.inputName + Ext.id())
	},
	/**
	 * Conversion method that turns a time control into server fields to be persisted in the db
	 */
	getUrlControlConversionProperties: function() {
		return {
			sourceType: this.inputType,
			id: this.id,
			name: this.inputName,
			referenceTableUrl: this.referenceTableUrl
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseUrlControlConversionProperties: function(config) {
		this.set('referenceTableUrl', config.referenceTableUrl);
	}
});

glu.defModel('RS.formbuilder.SectionContainerControl', {
	sectionType: 'panel',
	sectionTypeStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},

	title: '',

	dbTable$: function() {
		return this.parentVM.dbTable;
	},
	columnStore$: function() {
		return this.parentVM.columnStore
	},

	layout: 'column',
	layoutStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},

	columnCount: 1,
	columnCountIsVisible$: function() {
		return this.layout == 'column'
	},
	when_columns_count_changes_rebuild_control: {
		on: ['columnCountChanged'],
		action: function() {
			if (this.columnCount != -1) {
				while (this.columns.length > this.columnCount) this.columns.removeAt(this.columns.length - 1)
				while (this.columns.length < this.columnCount) this.columns.add(this.model({
					mtype: 'Column'
				}))
			}
		}
	},

	columns: {
		mtype: 'activatorlist',
		focusProperty: 'selectedColumn',
		autoParent: true
	},
	selectedColumn: -1,
	when_selected_column_changes_update_selectedColumnIndex: {
		on: ['selectedColumnChanged'],
		action: function() {
			if (this.selectedColumn) this.set('selectedColumnIndex', this.columns.indexOf(this.selectedColumn))
		}
	},
	selectedColumnIndex: -1,

	init: function() {
		this.sectionTypeStore.add([{
			name: this.localize('panel'),
			value: 'panel'
		}, {
			name: this.localize('fieldset'),
			value: 'fieldset'
		}]);

		this.layoutStore.add([{
			name: this.localize('auto'),
			value: 'auto'
		}, {
			name: this.localize('columnLayout'),
			value: 'column'
		}]);

		this.columns.add({
			mtype: 'Column'
		})

		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getSectionContainerControlConversionProperties) : this.serializeFunctions = [this.getSectionContainerControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseSectionContainerControlConversionProperties) : this.deserializeFunctions = [this.parseSectionContainerControlConversionProperties];
	},

	getSectionContainerControlConversionProperties: function() {
		//Go through each column and serialize the controls
		//Then encode the resulting array and store as the sectionColumns
		var encodeColumns = []
		this.columns.foreach(function(column) {
			encodeColumns.push({
				controls: this.rootVM.serializeControls(column.controls)
			})
		}, this)
		return {
			id: this.id,
			sourceType: 'INPUT',
			name: 'u_sectionContainer',
			sectionLayout: this.layout,
			sectionTitle: this.title,
			sectionType: this.sectionType,
			sectionNumberOfColumns: this.columnCount,
			sectionColumns: Ext.encode(encodeColumns)
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseSectionContainerControlConversionProperties: function(config) {
		this.set('id', config.id)
		this.set('layout', config.sectionLayout)
		this.set('title', config.sectionTitle)
		this.set('sectionType', config.sectionType)
		this.set('columnCount', -1)
		this.columns.removeAll()
		var decodeColumns = Ext.decode(config.sectionColumns)
		if (decodeColumns && Ext.isArray(decodeColumns)) {
			Ext.each(decodeColumns, function(column) {
				var c = this.model({
					mtype: 'Column'
				})
				this.columns.add(c)
				Ext.each(column.controls || [], function(control) {
					if (control && control.uiType) {
						var ctrl = this.rootVM.deserializeControl(control, false, null, c)
						ctrl.isInSection = true
						c.controls.add(ctrl)
					}
				}, this)
			}, this)
		}
		this.set('columnCount', config.sectionNumberOfColumns)
		if (this.columns.length == 0) {
			this.columns.add({
				mtype: 'Column'
			})
			this.set('columnCount', 1)
		}
		this.set('selectedColumn', 0)
	}
});
/**
 * TODO: look into adding mixin RS.formbuilder.Form. Uses many similar methods. Would reduce duplication - HR
 */
glu.defModel('RS.formbuilder.Column', {
	mixins: [],
	controls: {
		mtype: 'activatorlist',
		focusProperty: 'detailControl',
		autoParent: true
	},
	detailControl: -1,
	/* The currently selected control on the form or -1 if no selection */
	reselectControlIndex: -1,
	/* Flag internally used to determine if the control should be re-selected because an operation is rebuilding it (bulk)  */
	reselectControlCount: 0,
	when_detail_control_changes_make_sure_column_has_focus_on_section: {
		on: ['detailControlChanged', 'activeitemchangerequest'],
		action: function() {
			this.parentVM.set('selectedColumn', this);
		}
	},

	dbTable$: function() {
		return this.parentVM.dbTable
	},
	columnStore$: function() {
		return this.parentVM.columnStore
	},

	columnClicked: function() {
		this.parentVM.set('selectedColumn', this);
	},

	init: function() {
		this.controls.add({
			mtype: 'Panel'
		})
	},
	removeControl: function(control) {
		this.controls.remove(control);
		if (control.viewmodelName == 'FileUploadField') {
			this.rootVM.set('fileUploadIsEnabled', true);
		}
	},
	dropControl: function(dropIndex, sourceIndex, controlConfig, columnIndex) {
		if (dropIndex >= 0 && sourceIndex >= 0) {
			this.controls.insert(sourceIndex < dropIndex ? dropIndex - 1 : dropIndex, this.controls.removeAt(sourceIndex));
		} else if (dropIndex >= 0 && controlConfig) {
			this.insertControl(dropIndex, controlConfig);
		}
	},
	insertControl: function(index, controlConfig) {
		var control;

		if (controlConfig == 'SectionContainerField') {
			this.message({
				title: this.localize('sectionInSectionTitle'),
				msg: this.localize('sectionInSectionMessage'),
				buttons: Ext.MessageBox.OK
			})
			return null
		}

		if (Ext.isString(controlConfig)) control = this.model({
			mtype: controlConfig,
			isInSection: true,
			labelAlign: this.labelAlign
		});
		else {
			control = this.model(Ext.apply(controlConfig, {
				isInSection: true
			}));
		}

		//Disable the file upload button if this is a file upload field
		if (control.viewmodelName == 'FileUploadField') {
			this.rootVM.set('fileUploadIsEnabled', false);
		}

		this.controls.insert(index, control);
		this.selectControl(control);
		return control;
	},
	selectControl: function(control) {
		this.set('detailControl', control)
	},
	rebuildControl: function(ctrl, reselectControl) {
		var currentIndex = this.controls.indexOf(ctrl);
		var control = this.controls.removeAt(currentIndex);
		if (control) this.controls.insert(currentIndex, control);
		if (currentIndex != -1 && (this.reselectControlIndex == currentIndex || this.reselectControlCount <= 0) && reselectControl) {
			this.selectControl(this.controls.getAt(currentIndex));
			this.reselectControlCount--;
		}
	}
});

glu.defModel('RS.formbuilder.Panel', {})

glu.defModel('RS.formbuilder.TabContainerControl', {

	id: '',
	fieldLabel: '',
	fieldLabelIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceTable: '',
	referenceTableIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceColumn: '',
	referenceColumnIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceColumns: '',
	referenceColumnsIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	buttonSetIsHidden$ : function(){
		return Ext.Array.indexOf(['ReferenceTable', 'FileManager'], this.currentReference.rsType) == -1;
 	},
	allowReferenceTableAdd: true,
	allowReferenceTableAddIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	allowReferenceTableAddText$: function() {
		if (this.currentReference.rsType == 'FileManager') return this.localize('allowUpload');
		return this.localize('allowReferenceTableAdd');
	},
	allowReferenceTableRemove: true,
	allowReferenceTableRemoveIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	allowReferenceTableRemoveText$: function() {
		if (this.currentReference.rsType == 'FileManager') return this.localize('allowRemove');
		return this.localize('allowReferenceTableRemove');
	},
	allowReferenceTableView: true,
	allowReferenceTableViewIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	allowReferenceTableViewText$: function() {
		if (this.currentReference.rsType == 'FileManager') return this.localize('allowDownload');
		return this.localize('allowReferenceTableView');
	},
	referenceTableUrl: '',
	referenceTableUrlIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	referenceTableViewPopup: false,
	referenceTableViewPopupIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},
	popupFieldSetIsHidden$ : function(){
		return Ext.Array.indexOf(['ReferenceTable'], this.currentReference.rsType) == -1;
	},
	referenceTableViewPopupWidth: 600,
	referenceTableViewPopupHeight: 600,
	referenceTableViewPopupTitle: '',
	currentReferenceNew$: function() {
		if (this.currentReference.rsType == 'FileManager') return this.localize('upload');
		return this.localize('referenceTableNew');
	},

	referenceTableUrlNewTab: true,
	rsType: 'ReferenceTable',

	fields: ['fieldLabel', 'referenceTable', 'referenceColumn', 'referenceColumns', 'allowReferenceTableAdd', 'allowReferenceTableRemove', 'allowReferenceTableView', 'referenceTableUrl', 'referenceTableViewPopup', 'referenceTableViewPopupWidth', 'referenceTableViewPopupHeight', 'referenceTableViewPopupTitle'],

	when_field_changes_update_current_reference: {
		on: ['fieldLabelChanged', 'referenceTableChanged', 'referenceColumnChanged', 'referenceColumnsChanged', 'allowReferenceTableAddChanged', 'allowReferenceTableRemoveChanged', 'allowReferenceTableViewChanged', 'referenceTableUrlChanged', 'referenceTableViewPopupChanged', 'referenceTableViewPopupWidthChanged', 'referenceTableViewPopupHeightChanged', 'referenceTableViewPopupTitleChanged'],
		action: function() {
			//persist to current reference
			if (Ext.isObject(this.currentReference)) {
				Ext.Object.each(this.currentReference.asObject(), function(attribute) {
					this.currentReference.set(attribute, this[attribute]);
				}, this);
				this.currentReference.referenceColumnStore.removeAll();
				this.referenceColumnStore.each(function(record) {
					this.currentReference.referenceColumnStore.add(record);
				}, this);
			}
		}
	},
	when_reference_table_changes_update_reference_column_too: {
		on: ['referenceTableChanged'],
		action: function() {
			var record = this.referenceStore.findRecord('value', this.referenceTable, 0, false, false, true)
			if (record) {
				this.set('referenceColumn', record.data.name);
			}
		}
	},

	referenceTables: {
		mtype: 'list',
		autoParent: true
	},

	referenceTablesSelections: [],
	currentReference: 0,
	when_current_reference_changes_update_selection_in_grid: {
		on: 'currentReferenceChanged',
		action: function() {
			if (Ext.isObject(this.currentReference)) {
				this.changeReferenceTablesSelections([this.referenceTablesStore.getAt(this.referenceTables.indexOf(this.currentReference))]);
			}
		}
	},
	when_selection_changes_update_fields: {
		on: ['referenceTablesSelectionsChanged'],
		action: function() {
			var sel = this.referenceTablesSelections[0];
			if (sel) {
				var selection = this.referenceTables.getAt(this.referenceTablesStore.indexOf(sel));
				if (selection) {
					//Persist dependencies
					// if(Ext.isObject(this.currentReference)) {
					// 	var deps = this.getDependencyControlConversionProperties();
					// 	this.currentReference.parseDependencyControlConversionProperties(deps);
					// }
					var temp = this.currentReference;
					this.currentReference = null;
					Ext.Object.each(selection.asObject(), function(attribute) {
						if (Ext.isDefined(this[attribute])) this.set(attribute, selection[attribute]);
					}, this);
					// this.parseDependencyControlConversionProperties(selection.getDependencyControlConversionProperties());
					this.currentReference = temp;
					this.set('currentReference', selection);
				}
			}
		}
	},

	referenceTablesStore: {
		mtype: 'store',
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'referenceTables'
		}],
		fields: ['fieldLabel']
	},

	referenceDisplayStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},

	referenceStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getReferenceTables',
			// url: '/resolve/service/form/getcustomtablesforref',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	referenceColumnStore: {
		mtype: 'store',
		fields: ['name', 'dbcolumnId', 'displayName', 'dbtype', 'uiType', 'integerMinValue', 'integerMaxValue', 'decimalMinValue', 'decimalMaxValue', 'stringMinLength', 'stringMaxLength', 'uiStringMaxLength', 'selectValues', 'choiceValues', 'referenceTable', 'referenceDisplayColumn'],
		data: [],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getcustomtablecolumns',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	when_referenceTable_changes_update_reference_columns: {
		on: ['referenceTableChanged'],
		action: function() {
			this.set('referenceColumns', '');
			this.referenceColumnStore.load();
		}
	},

	referenceColumnsIsEnabled$: function() {
		return this.referenceTable;
	},
	referenceColumnsIsVisible$: function() {
		return Ext.Array.indexOf(['ReferenceTable'], this.currentReference.rsType) > -1;
	},

	referenceTableIsValid$: function() {
		if (this.currentReference) {
			if (this.currentReference.rsType == 'ReferenceTable' && !this.referenceTable) return this.localize('referenceTableInvalid')
		}
		return true;
	},
	referenceTableIsVisible$: function() {
		return Ext.Array.indexOf(['ReferenceTable'], this.currentReference.rsType) > -1;
	},

	referenceTableUrlText$: function() {
		if (this.currentReference.rsType == 'FileManager') return this.localize('allowedFileTypes');
		return this.localize('referenceTableUrl');
	},

	referenceTableUrlIsValid$: function() {
		if (this.currentReference) {
			switch (this.currentReference.rsType) {
				case 'ReferenceTable':
					if ((this.allowReferenceTableAdd || this.allowReferenceTableView) && !this.referenceTableUrl) return this.localize('referenceTableUrlInvalid');
					break;
				case 'FileManager':
					return true;
					break;
				case 'URL':
					if (!this.referenceTableUrl) return this.localize('urlTabInvalid');
					break;
			}
		}
		return true;
	},

	when_referenceTableUrl_changed_check_match_to_wiki: {
		on: ['referenceTableUrlChanged'],
		action: function() {
			if ((this.parentVM.wikiNames && this.parentVM.wikiNames.getById(this.referenceTableUrl)) || (this.parentVM.parentVM.wikiNames && this.parentVM.parentVM.wikiNames.getById(this.referenceTableUrl))) {
				Ext.defer(function() {
					this.set('referenceTableUrl', '/resolve/service/wiki/view/' + this.referenceTableUrl.split('.').join('/'));
				}, 100, this);
			}
		}
	},

	changeReferenceTablesSelections: function(selections) {
		this.set('referenceTablesSelections', selections);
		this.referenceTablesStore.fireEvent('cellSelectionVMChanged', this, selections);
	},

	addReferenceTable: function() {
		var selectedIndex = -1;
		var sel = this.referenceTablesSelections[0];
		if (sel) selectedIndex = this.referenceTablesStore.indexOf(sel);
		if (selectedIndex == -1) selectedIndex = this.referenceTables.length;
		else selectedIndex++;
		this.referenceTables.insert(selectedIndex, this.model({
			mtype: 'ReferenceTable',
			rsType: 'ReferenceTable',
			fieldLabel: this.localize('unknownReference')
		}));

		this.set('referenceTablesSelections', [this.referenceTablesStore.getAt(selectedIndex)]);
	},

	addFileManager: function() {
		var selectedIndex = -1;
		var sel = this.referenceTablesSelections[0];
		if (sel) selectedIndex = this.referenceTablesStore.indexOf(sel);
		if (selectedIndex == -1) selectedIndex = this.referenceTables.length;
		else selectedIndex++;
		this.referenceTables.insert(selectedIndex, this.model({
			mtype: 'ReferenceTable',
			rsType: 'FileManager',
			referenceTableUrl: '*',
			fieldLabel: this.localize('attachments')
		}));

		//disable the adding of more file managers on the page
		this.rootVM.set('fileUploadIsEnabled', false);

		this.set('referenceTablesSelections', [this.referenceTablesStore.getAt(selectedIndex)]);
	},

	addFileManagerIsEnabled$: function() {
		return this.rootVM.fileUploadIsEnabled;
	},

	addURL: function() {
		var selectedIndex = -1;
		var sel = this.referenceTablesSelections[0];
		if (sel) selectedIndex = this.referenceTablesStore.indexOf(sel);
		if (selectedIndex == -1) selectedIndex = this.referenceTables.length;
		else selectedIndex++;
		this.referenceTables.insert(selectedIndex, this.model({
			mtype: 'ReferenceTable',
			rsType: 'URL',
			allowReferenceTableAdd: false,
			allowReferenceTableRemove: false,
			fieldLabel: this.localize('urlTab')
		}));

		this.set('referenceTablesSelections', [this.referenceTablesStore.getAt(selectedIndex)]);
	},
	removeReference: function() {
		var sel = this.referenceTablesSelections[0];
		if (sel) {
			var selection = this.referenceTables.getAt(this.referenceTablesStore.indexOf(sel));
			if (selection) {
				if (this.currentReference.rsType == 'FileManager') this.rootVM.set('fileUploadIsEnabled', true);
				this.referenceTables.remove(selection);
				this.changeReferenceTablesSelections([this.referenceTablesStore.getAt(0)]);
			}
		}
	},
	removeReferenceIsEnabled$: function() {
		return this.referenceTablesSelections.length == 1;
	},

	initMixin: function() {
		this.referenceStore.load({
			params: {
				tableName: this.parentVM.dbTable
			}
		});
		this.referenceColumnStore.on('beforeload', function(store, operation, eOpts) {
			if (!this.referenceTable) return false
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				tableName: this.referenceTable
			});
		}, this);

		//Initialize first reference
		this.referenceTables.add(this.model({
			mtype: 'ReferenceTable',
			fieldLabel: this.localize('references')
		}));
		//Initially select the only record so that all the fields are initialized properly
		this.set('currentReference', this.referenceTables.getAt(0));
		this.changeReferenceTablesSelections([this.referenceTablesStore.getAt(0)]);

		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getTabContainerControlConversionProperties) : this.serializeFunctions = [this.getTabContainerControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseTabContainerControlConversionProperties) : this.deserializeFunctions = [this.parseTabContainerControlConversionProperties];
	},

	getTabContainerControlConversionProperties: function() {

		//Go through each of the ReferenceTables and combine them to persist on the backend
		var displayName = '',
			refGridReferenceByTable = '',
			refGridReferenceColumnName = '',
			refGridSelectedReferenceColumns = '',
			allowReferenceTableAdd = '',
			allowReferenceTableRemove = '',
			allowReferenceTableView = '',
			referenceTableUrl = '',
			referenceTableViewPopup = '',
			referenceTableViewPopupWidth = '',
			referenceTableViewPopupHeight = '',
			referenceTableViewPopupTitle = '';

		this.referenceTables.foreach(function(referenceTable, index) {
			displayName += (index > 0 ? '|' : '') + referenceTable.fieldLabel;
			refGridReferenceByTable += (index > 0 ? '|' : '') + referenceTable.referenceTable;
			refGridReferenceColumnName += (index > 0 ? '|' : '') + referenceTable.referenceColumn;
			allowReferenceTableAdd += (index > 0 ? '|' : '') + referenceTable.allowReferenceTableAdd;
			allowReferenceTableRemove += (index > 0 ? '|' : '') + referenceTable.allowReferenceTableRemove;
			allowReferenceTableView += (index > 0 ? '|' : '') + referenceTable.allowReferenceTableView;
			referenceTableUrl += (index > 0 ? '|' : '') + referenceTable.referenceTableUrl;
			referenceTableViewPopup += (index > 0 ? '|' : '') + referenceTable.referenceTableViewPopup;
			referenceTableViewPopupWidth += (index > 0 ? '|' : '') + referenceTable.referenceTableViewPopupWidth;
			referenceTableViewPopupHeight += (index > 0 ? '|' : '') + referenceTable.referenceTableViewPopupHeight;
			referenceTableViewPopupTitle += (index > 0 ? '|' : '') + (referenceTable.referenceTableViewPopupTitle || referenceTable.rsType);

			var displayColumns = [];

			Ext.each(referenceTable.referenceColumns, function(refColumn) {
				var rec = referenceTable.referenceColumnStore.findRecord('name', refColumn, 0, false, false, true);
				if (rec) {
					displayColumns.push({
						name: rec.data.displayName,
						value: refColumn,
						dbtype: rec.data.dbtype
					});
				}
			}, this);
			refGridSelectedReferenceColumns += (index > 0 ? '|' : '') + (displayColumns.length > 0 ? Ext.encode(displayColumns) : '');
		}, this);

		return {
			id: this.id,
			sourceType: 'INPUT',
			name: 'u_referenceTable_',
			fileUploadTableName: Ext.String.format('{0}_fu', this.parentVM.dbTable),
			displayName: displayName,
			refGridReferenceByTable: refGridReferenceByTable,
			refGridReferenceColumnName: refGridReferenceColumnName,
			refGridSelectedReferenceColumns: refGridSelectedReferenceColumns,
			allowReferenceTableAdd: allowReferenceTableAdd,
			allowReferenceTableRemove: allowReferenceTableRemove,
			allowReferenceTableView: allowReferenceTableView,
			referenceTableUrl: referenceTableUrl,
			refGridViewPopup: referenceTableViewPopup,
			refGridViewPopupWidth: referenceTableViewPopupWidth,
			refGridViewPopupHeight: referenceTableViewPopupHeight,
			refGridViewTitle: referenceTableViewPopupTitle
		}
	},
	/**
	 * Conversion method that turns a server field to a form control
	 */
	parseTabContainerControlConversionProperties: function(config) {
		this.set('id', config.id);

		//Parse out each Reference Table from each of the fields
		var displayNames = config.displayName ? config.displayName.split('|') : [],
			referenceTables = config.refGridReferenceByTable ? config.refGridReferenceByTable.split('|') : [],
			refGridReferenceColumnName = config.refGridReferenceColumnName ? config.refGridReferenceColumnName.split('|') : [],
			referenceColumns = config.refGridSelectedReferenceColumns ? config.refGridSelectedReferenceColumns.split('|') : [],
			allowReferenceTableAdd = config.allowReferenceTableAdd ? config.allowReferenceTableAdd.split('|') : [],
			allowReferenceTableRemove = config.allowReferenceTableRemove ? config.allowReferenceTableRemove.split('|') : [],
			allowReferenceTableView = config.allowReferenceTableView ? config.allowReferenceTableView.split('|') : [],
			referenceTableUrl = config.referenceTableUrl ? config.referenceTableUrl.split('|') : [],
			referenceTableViewPopup = config.refGridViewPopup ? config.refGridViewPopup.split('|') : [],
			referenceTableViewPopupWidth = config.refGridViewPopupWidth ? config.refGridViewPopupWidth.split('|') : [],
			referenceTableViewPopupHeight = config.refGridViewPopupHeight ? config.refGridViewPopupHeight.split('|') : [],
			referenceTableViewPopupTitle = config.refGridViewTitle ? config.refGridViewTitle.split('|') : [],
			len = displayNames.length,
			i;

		for (i = 0; i < len; i++) {
			var cols = referenceColumns[i] ? Ext.decode(referenceColumns[i]) : [],
				displayColumns = [];
			Ext.each(cols, function(col) {
				displayColumns.push(col.value);
			});
			var refTable = this.model({
				mtype: 'ReferenceTable',
				fieldLabel: displayNames[i],
				referenceTable: referenceTables[i],
				referenceColumn: refGridReferenceColumnName[i],
				referenceColumns: displayColumns,
				allowReferenceTableAdd: allowReferenceTableAdd[i] == 'true',
				allowReferenceTableRemove: allowReferenceTableRemove[i] == 'true',
				allowReferenceTableView: allowReferenceTableView[i] == 'true',
				referenceTableUrl: referenceTableUrl[i],
				referenceTableViewPopup: referenceTableViewPopup[i] == 'true',
				referenceTableViewPopupWidth: Number(referenceTableViewPopupWidth[i]),
				referenceTableViewPopupHeight: Number(referenceTableViewPopupHeight[i]),
				referenceTableViewPopupTitle: referenceTableViewPopupTitle[i]
			});
			if (Ext.Array.indexOf(['ReferenceTable', 'FileManager', 'URL'], refTable.referenceTableViewPopupTitle) > -1) {
				refTable.set('rsType', refTable.referenceTableViewPopupTitle);
				if (refTable.rsType == 'FileManager') this.rootVM.set('fileUploadIsEnabled', false);
			}
			refTable.referenceColumnStore.load({
				params: {
					tableName: refTable.referenceTable
				}
			});
			this.referenceTables.add(refTable);
		}

		if (len > 0) {
			this.referenceTables.removeAt(0);
			this.set('currentReference', this.referenceTables.getAt(0));
		}
	}
});

// STANDARD CONTROLS
/**
 * Model Definition for a TextField
 * uiType: TextField
 * xtype: textfield
 * serverEnum: UI_TEXTFIELD("TextField")
 */
glu.defModel('RS.formbuilder.TextField', {
	dbColumnType: ['TextField'],
	allowedMaxValue: 333,
	mixins: ['BaseControl', 'FormControl', 'DefaultValueControl', 'StringControl', 'RequireableControl', 'TooltipControl', 'EncryptableControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a TextAreaField
 * uiType: TextArea
 * xtype: textareafield
 * serverEnum: UI_TEXTAREA("TextArea")
 */
glu.defModel('RS.formbuilder.TextAreaField', {
	defaultHeight: 60,
	dbColumnType: ['TextArea'],
	mixins: ['BaseControl', 'FormControl', 'DefaultValueControl', 'StringControl', 'RequireableControl', 'TooltipControl', 'EncryptableControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});

glu.defModel('RS.formbuilder.WysiwygField', {
	defaultHeight: 160,
	columnSize: 4000,
	dbColumnType: ['TextArea', 'WysiwygField'],
	mixins: ['BaseControl', 'FormControl', 'DefaultValueControl', 'StringControl', 'TooltipControl', 'EncryptableControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a ReadOnlyTextField
 * uiType: TextField
 * xtype: displayfield
 * serverEnum: UI_READONLYTEXTFIELD("ReadOnlyTextField")
 */
/*glu.defModel('RS.formbuilder.ReadOnlyTextField', {
	dbColumnType: ['ReadOnlyTextField'],
	mixins: ['BaseControl', 'FormControl', 'DefaultValueControl', 'DatabaseControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});*/

glu.defModel('RS.formbuilder.SpacerField', {
	mixins: ['BaseControl', 'SizeControl', 'DependencyControl', 'SpacerControl']
});
glu.defModel('RS.formbuilder.SpacerControl', {
	id: '',
	inputName: 'spacer',
	inputType: 'INPUT',

	showHorizontalRule: false,

	ruleDisplay$: function() {
		return this.showHorizontalRule ? 'border-bottom:1px solid #999' : 'border-bottom:0px'
	},

	initMixin: function() {
		this.set('inputName', 'spacer' + Ext.id());
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getSpacerControlConversionProperties) : this.serializeFunctions = [this.getSpacerControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseSpacerControlConversionProperties) : this.deserializeFunctions = [this.parseSpacerControlConversionProperties];
	},

	getSpacerControlConversionProperties: function() {
		return {
			sourceType: this.inputType,
			id: this.id,
			name: this.inputName,
			showHorizontalRule: this.showHorizontalRule
		}
	},
	parseSpacerControlConversionProperties: function(config) {
		this.set('inputName', config.name)
		this.set('showHorizontalRule', config.showHorizontalRule)
	}
});

glu.defModel('RS.formbuilder.LabelField', {
	mixins: ['BaseControl', 'SizeControl', 'DependencyControl', 'LabelControl']
});
glu.defModel('RS.formbuilder.LabelControl', {
	inputName: 'label',
	inputType: 'INPUT',

	fieldLabel: '',

	initMixin: function() {
		this.set('inputName', 'label' + Ext.id());
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getLabelControlConversionProperties) : this.serializeFunctions = [this.getLabelControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseLabelControlConversionProperties) : this.deserializeFunctions = [this.parseLabelControlConversionProperties];
	},

	getLabelControlConversionProperties: function() {
		return {
			sourceType: this.inputType,
			id: this.id,
			name: this.inputName,
			displayName: this.fieldLabel
		}
	},
	parseLabelControlConversionProperties: function(config) {
		this.set('inputName', config.name)
		this.set('fieldLabel', config.displayName)
	}
})

glu.defModel('RS.formbuilder.JournalControl', {

	allowJournalEdit: false,

	initMixin: function() {
		Ext.isDefined(this.serializeFunctions) ? this.serializeFunctions.push(this.getJournalControlConversionProperties) : this.serializeFunctions = [this.getJournalControlConversionProperties];
		Ext.isDefined(this.deserializeFunctions) ? this.deserializeFunctions.push(this.parseJournalControlConversionProperties) : this.deserializeFunctions = [this.parseJournalControlConversionProperties];
	},

	getJournalControlConversionProperties: function() {
		return {
			allowJournalEdit: this.allowJournalEdit
		}
	},
	parseJournalControlConversionProperties: function(config) {
		this.set('allowJournalEdit', config.allowJournalEdit)
	}
})


/**
 * Model definition for a NumberField
 * uiType: NumberTextField
 * xtype: numberfield
 * serverEnum: UI_NUMBERTEXTFIELD("NumberTextField")
 */
glu.defModel('RS.formbuilder.NumberField', {
	dbColumnType: ['NumberTextField'],
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'NumberControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a DecimalField
 * uiType: DecimalTextField
 * xtype: displayfield
 * serverEnum: UI_DECIMALTEXTFIELD("DecimalTextField")
 */
glu.defModel('RS.formbuilder.DecimalField', {
	dbColumnType: ['DecimalTextField'],
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'DecimalControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a CheckboxField
 * uiType: CheckBox
 * xtype: checkbox
 * serverEnum: UI_CHECKBOX("CheckBox")
 */
glu.defModel('RS.formbuilder.CheckboxField', {
	dbColumnType: ['CheckBox'],
	columnSize: 1000,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'DatabaseControl', 'DefaultBooleanControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a MultipleCheckboxField
 * uiType: MultipleCheckBox
 * xtype: checkboxgroup | checkbox in fieldset
 * serverEnum: UI_MULTIPLECHECKBOX("MultipleCheckBox")
 */
glu.defModel('RS.formbuilder.MultipleCheckboxField', {
	dbColumnType: ['MultipleCheckBox', 'MultiSelectComboBoxField'],
	columnSize: 1000,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'DatabaseControl', 'MultiChoiceControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a RadioButtonField
 * uiType: RadioButton
 * xtype: radiogroup
 * serverEnum: UI_CHOICE("RadioButton")
 */
glu.defModel('RS.formbuilder.RadioButtonField', {
	dbColumnType: ['RadioButton', 'ComboBox'],
	columnSize: 1000,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'DatabaseControl', 'ChoiceControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a ComboBoxField
 * uiType: ComboBox
 * xtype: combobox | selectfield
 * serverEnum: UI_COMBOBOX("ComboBox")
 */
glu.defModel('RS.formbuilder.ComboBoxField', {
	dbColumnType: ['ComboBox', 'RadioButton'],
	isCombobox: true,
	columnSize: 1000,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'AllowInputControl', 'DatabaseControl', 'ChoiceControl', 'DependencyControl', 'SizeControl']
});
/**
 * Model definition for a Multi-Select ComboBoxField
 * uiType: ComboBox
 * xtype: combobox | selectfield
 * serverEnum: UI_COMBOBOX("MultiSelectComboBox")
 */
glu.defModel('RS.formbuilder.MultiSelectComboBoxField', {
	dbColumnType: ['MultipleCheckBox', 'MultiSelectComboBoxField'],
	isCombobox: true,
	columnSize: 1000,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'AllowInputControl', 'DatabaseControl', 'MultiChoiceControl', 'DependencyControl', 'SizeControl']
});
/**
 * Model definition for a NameField
 * uiType:
 * xtype: namefield
 * serverEnum: UI_NAMEFIELD("NameField")
 */
glu.defModel('RS.formbuilder.NameField', {
	dbColumnType: ['NameField'],
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'DatabaseControl', 'SizeControl', 'NameControl', 'DependencyControl'],
	fieldLabel: 'fullName',
	firstName: '',
	middleInitial: '',
	middleName: '',
	lastName: '',
	columnSize: 1000,
	init: function() {
		if (this.fieldLabel == 'fullName') this.set('fieldLabel', this.localize('fullName'));
	}
});
/**
 * Model definition for a AddressField
 * uiType:
 * xtype: addressfield
 * serverEnum: UI_ADDRESS("AddressField")
 */
glu.defModel('RS.formbuilder.AddressField', {
	dbColumnType: ['AddressField'],
	columnSize: 1000,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'DatabaseControl', 'AddressControl', 'SizeControl', 'DependencyControl'],
	defaultHeight: 185
});
/**
 * Model definition for a EmailField
 * uiType:
 * xtype: emailfield
 * serverEnum: UI_EMAIL("EmailField")
 */
glu.defModel('RS.formbuilder.EmailField', {
	dbColumnType: ['EmailField'],
	allowedMaxValue: 333,
	columnSize: 333,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'DefaultValueControl', 'StringControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
glu.defModel('RS.formbuilder.MACAddressField', {
	dbColumnType: ['MACAddressField'],
	allowedMaxValue: 333,
	columnSize: 333,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'DefaultValueControl', 'StringControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
glu.defModel('RS.formbuilder.IPAddressField', {
	dbColumnType: ['IPAddressField'],
	allowedMaxValue: 333,
	columnSize: 333,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'DefaultValueControl', 'StringControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
glu.defModel('RS.formbuilder.CIDRAddressField', {
	dbColumnType: ['CIDRAddressField'],
	allowedMaxValue: 333,
	columnSize: 333,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'DefaultValueControl', 'StringControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a PhoneField
 * uiType:
 * xtype: phonefield
 * serverEnum: UI_PHONE("PhoneField")
 */
glu.defModel('RS.formbuilder.PhoneField', {
	allowedMaxValue: 40,
	columnSize: 40,
	dbColumnType: ['PhoneField'],
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a DateField
 * uiType: Date
 * xtype: datefield
 * serverEnum: UI_DATE("Date")
 */
glu.defModel('RS.formbuilder.DateField', {
	dbColumnType: ['Date'],
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'DateControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a TimeField
 * uiType: Timestamp
 * xtype: timefield
 * serverEnum: UI_TIMESTAMP("Timestamp")
 */
glu.defModel('RS.formbuilder.TimeField', {
	dbColumnType: ['Timestamp'],
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'TimeControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a DateTimeField
 * uiType: DateTime
 * xtype: datetimefield
 * serverEnum: UI_DATETIME("DateTime")
 */
glu.defModel('RS.formbuilder.DateTimeField', {
	dbColumnType: ['DateTime'],
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'DateTimeControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a LinkField
 * uiType: Link
 * xtype: textfield
 * serverEnum: UI_LINK("Link")
 */
glu.defModel('RS.formbuilder.LinkField', {
	dbColumnType: ['Link'],
	defaultValue: 'http://www.example.com',
	columnSize: 1000,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'DefaultValueControl', 'LinkControl', 'StringControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a LikeRTField
 * uiType:
 * xtype: likertfield
 * serverEnum:
 */
// glu.defModel('RS.formbuilder.LikeRTField', {
// 	dbColumnType: 'LikeRTField',
// 	mixins: ['BaseControl', 'FormControl', 'RequireableControl', 'DatabaseControl', 'SizeControl']
// });
/**
 * Model definition for a PasswordField
 * uiType: Password
 * xtype: textfield
 * serverEnum: UI_PASSWORD("Password")
 */
glu.defModel('RS.formbuilder.PasswordField', {
	dbColumnType: ['Password'],
	allowedMaxValue: 333,
	columnSize: 333,
	encrypted: true,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'DefaultValueControl', 'StringControl', 'DatabaseControl', 'SizeControl', 'DependencyControl'],
	encryptedIsEnabled$: function() {
		return false;
	}
});
/**
 * Model definition for a JournalField
 * uiType: Journal
 * xtype: journalfield
 * serverEnum: UI_JOURNALWIDGET("Journal")
 */
glu.defModel('RS.formbuilder.JournalField', {
	dbColumnType: ['Journal'],
	mixins: ['BaseControl', 'FormControl', 'DatabaseControl', 'TooltipControl', 'SizeControl', 'DependencyControl', 'JournalControl'],
	defaultHeight: 300,
	inputType: 'DB',
	columnSize: 4000
});
/**
 * Model definition for a ListField
 * uiType: List
 * xtype: multiselect
 * serverEnum: UI_LIST("List")
 */
// UI_LIST("List"),
glu.defModel('RS.formbuilder.ListField', {
	dbColumnType: ['List'],
	mixins: ['BaseControl', 'FormControl', 'RequireableControl', 'TooltipControl', 'EncryptableControl', 'MultiChoiceControl', 'DatabaseControl', 'SizeControl', 'DependencyControl'],
	defaultHeight: 150,
	columnSize: 1000
});
/**
 * Model definition for a SequenceField
 * uiType: Sequence
 * xtype: displayfield
 * serverEnum: UI_SEQUENCE("Sequence")
 */
glu.defModel('RS.formbuilder.SequenceField', {
	dbColumnType: ['Sequence'],
	allowedMaxValue: 40,
	columnSize: 40,
	inputType: 'DB',
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'RequireableControl', 'EncryptableControl', 'SequenceControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a ReferenceField
 * uiType: Reference
 * xtype: displayfield
 * serverEnum: UI_REFERENCE("Reference")
 */
glu.defModel('RS.formbuilder.ReferenceField', {
	dbColumnType: ['Reference'],
	inputType: 'DB',
	columnSize: 40,
	mixins: ['BaseControl', 'FormControl', 'TooltipControl', 'DatabaseControl', 'ReferenceControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a HiddenField
 * uiType: HiddenField
 * xtype: hiddenfield
 * serverEnum: UI_HIDDEN("HiddenField")
 */
glu.defModel('RS.formbuilder.HiddenField', {
	dbColumnType: ['HiddenField'],
	columnSize: 333,
	mixins: ['BaseControl', 'FormControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});
/**
 * Model definition for a TagField
 * uiType: TagField
 * xtype: displayfield
 * serverEnum: UI_TAG("TagField")
 */
/*glu.defModel('RS.formbuilder.TagField', {
	dbColumnType: 'TagField',
	mixins: ['BaseControl', 'FormControl', 'RequireableControl', 'EncryptableControl', 'DefaultValueControl', 'StringControl', 'DatabaseControl', 'SizeControl', 'DependencyControl']
});*/
/**
 * Model definition for a ButtonField
 * uiType:
 * xtype: button
 * serverEnum:
 */
glu.defModel('RS.formbuilder.ButtonField', {
	mixins: ['ButtonControl']
});
/**
 * Model definition for a UserPickerField
 * uiType:
 * xtype: userpickerfield
 * serverEnum: UI_USERPICKER("UserPickerField")
 */
glu.defModel('RS.formbuilder.UserPickerField', {
	dbColumnType: ['UserPickerField'],
	columnSize: 40,
	mixins: ['BaseControl', 'FormControl', 'RequireableControl', 'TooltipControl', 'EncryptableControl', 'DatabaseControl', 'GroupPickerControl', 'SizeControl', 'DependencyControl']
});

/**
 * Model definition for a TeamPickerField
 * uiType:
 * xtype: teampickerfield
 * serverEnum: UI_TEAMPICKER("TeamPickerField")
 */
glu.defModel('RS.formbuilder.TeamPickerField', {
	dbColumnType: ['TeamPickerField'],
	columnSize: 40,
	mixins: ['BaseControl', 'FormControl', 'RequireableControl', 'TooltipControl', 'EncryptableControl', 'DatabaseControl', 'TeamPickerControl', 'SizeControl', 'DependencyControl']
});

glu.defModel('RS.formbuilder.FileUploadField', {
	mixins: ['BaseControl', 'FileUploadControl', 'TooltipControl', 'SizeControl', 'DependencyControl'],
	defaultHeight: 150
});

glu.defModel('RS.formbuilder.ReferenceTableField', {
	mixins: ['BaseControl', 'ReferenceTableControl', 'TooltipControl', 'SizeControl', 'DependencyControl'],
	defaultHeight: 150
});

glu.defModel('RS.formbuilder.TabContainerField', {
	mixins: ['BaseControl', 'TooltipControl', 'TabContainerControl', 'SizeControl', 'DependencyControl'],
	defaultHeight: 150
});

glu.defModel('RS.formbuilder.SectionContainerField', {
	mixins: ['BaseControl', 'SizeControl', 'DependencyControl', 'SectionContainerControl']
});

glu.defModel('RS.formbuilder.UrlField', {
	mixins: ['BaseControl', 'UrlControl', 'TooltipControl', 'SizeControl', 'DependencyControl'],
	defaultHeight: 150
});

glu.defModel('RS.formbuilder.FormPicker', {

	showCreate: false,

	formPickerTitle: '',
	title$: function() {
		return this.formPickerTitle || this.localize('formPickerTitle')
	},


	searchQuery: '',
	when_searchQueryChanges_update_grid: {
		on: ['searchQueryChanged'],
		action: function() {
			this.forms.load()
		}
	},

	filter: [],
	startsWithBase: '',

	forms: {
		mtype: 'store',
		fields: ['id', 'uformName', 'udisplayName', 'utableName'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'uformName',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	formsColumns: [{
		header: '~~uformName~~',
		dataIndex: 'uformName',
		filterable: true,
		flex: 1,
		renderer: RS.common.grid.plainRenderer()
	}, {
		header: '~~udisplayName~~',
		dataIndex: 'udisplayName',
		filterable: true,
		flex: 1,
		renderer: RS.common.grid.plainRenderer()
	}, {
		header: '~~utableName~~',
		dataIndex: 'utableName',
		filterable: true,
		flex: 1,
		renderer: RS.common.grid.plainRenderer()
	}].concat(RS.common.grid.getSysColumns()),

	formsSelections: [],

	init: function() {
		Ext.Array.forEach(this.filter, function(f) {
			if (f.condition == 'startsWith') {
				this.set('startsWithBase', f.value)
			}
		}, this)

		this.forms.on('beforeload', function(store, operation) {
			operation.params = operation.params || {}
			var filter = this.filter;

			if (this.searchQuery) {
				filter = [{
					field: 'uformName',
					condition: 'startsWith',
					value: this.startsWithBase + this.searchQuery
				}]
			}

			if (filter && filter.length > 0)
				operation.params.filter = Ext.encode(filter)
		}, this)

		this.forms.load()
	},
	dumper: null,
	select: function() {
		if (this.dumper)
			this.dumper(this.formsSelections);
		else
			this.parentVM.set('name', this.formsSelections[0].get('uformName'))
		this.doClose()
	},
	selectIsEnabled$: function() {
		return this.formsSelections.length > 0
	},
	cancel: function() {
		this.doClose()
	},

	createFormIsVisible$: function() {
		return this.showCreate
	},
	createForm: function() {
		this.parentVM.createForm()
		this.doClose()
	}
})
glu.defModel('RS.formbuilder.Forms', {
	mock: false,

	displayName: '',

	state: '',

	stateId: 'formbuilderforms',

	store: null,

	columns: null,

	formsSelections: [],

	activate: function() {
		clientVM.setWindowTitle(this.localize('formsWindowTitle'))
		this.loadForms()
	},

	init: function() {
		if (this.mock) this.backend = RS.formbuilder.createMockBackend(true);
		this.set('displayName', this.localize('formsTitle'));
		this.set('store', Ext.create('Ext.data.Store', {
			fields: ['id', 'uformName', 'udisplayName', 'utableName'].concat(RS.common.grid.getSysFields()),
			remoteSort: true,
			sorters: [{
				property: 'uformName',
				direction: 'ASC'
			}],
			proxy: {
				type: 'ajax',
				url: '/resolve/service/form/list',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			}
		}));
		this.set('columns', [{
			header: '~~uformName~~',
			dataIndex: 'uformName',
			filterable: true,
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~udisplayName~~',
			dataIndex: 'udisplayName',
			filterable: true,
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~utableName~~',
			dataIndex: 'utableName',
			filterable: true,
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		}].concat(RS.common.grid.getSysColumns()));
	},

	loadForms: function() {
		this.set('state', 'wait');
		this.store.load({
			scope: this,
			callback: function(rec, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('ListFormsErrMsg'));
					return;
				}
				this.set('state', 'ready');
			}
		});
	},

	editForm: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.formbuilder.Main',
			params: {
				id: id
			}
		});
	},

	createForm: function() {
		clientVM.handleNavigation({
			modelName: 'RS.formbuilder.Main',
			params: {
				id: ''
			}
		});
	},

	deleteForms: function() {
		this.message({
			title: this.localize('DeleteFormsTitle'),
			msg: this.localize(this.formsSelections.length > 1 ? 'DeleteFormsMsg' : 'DeleteFormMsg', {
				num: this.formsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteForms'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.sendDeleteReq
		});
	},

	sendDeleteReq: function(btn) {
		if (btn != 'yes') {
			this.set('state', 'itemSelected');
			return;
		}

		this.set('state', 'wait');

		var ids = [];

		Ext.each(this.formsSelections, function(r) {
			ids.push(r.get('id'));
		}, this);
		this.ajax({
			url: '/resolve/service/form/delete',
			jsonData: ids,
			scope: this,
			success: this.handleDeleteSucResp,
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	handleDeleteSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('DeleteErrMsg') + '[' + respData.message + ']');
		} else
			clientVM.displaySuccess(this.localize('DeleteSucMsg'));
		this.loadForms();
	},

	viewForm: function() {
		clientVM.handleNavigation({
			modelName: 'RS.formbuilder.Viewer',
			params: {
				name: this.formsSelections[0].get('uformName')
			}
		})
	},

	viewFormIsEnabled$: function() {
		return this.formsSelections.length == 1 && !this.wait;
	},

	createFormIsEnabled$: function() {
		return this.state != 'wait';
	},

	deleteFormsIsEnabled$: function() {
		return this.state === 'itemSelected';
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},

	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	when_formsSelections_changed: {
		on: ['formsSelectionsChanged'],
		action: function() {
			if (this.formsSelections.length > 0)
				this.set('state', 'itemSelected')
			else
				this.set('state', 'ready');
		}
	}
});
/**
 * Model definition for ImportOption
 * The import option model has all the available options for importing different choices into a choice/select control
 */
glu.defModel('RS.formbuilder.ImportOption', {
	/**
	 * The option text to display to the user (i.e. the text of the actually selected option)
	 */
	optionText: '',
	/**
	 * The selected option that the user has chosen
	 */
	selectedOption: '',

	/**
	 * Reactor for when the selection changes to populate the textarea with the text
	 */
	whenSelectionChanges: {
		on: ['selectedOptionChanged'],
		action: function() {
			var optionName = this.selectedOption[0],
				realOption;
			this.availableOptions.each(function(option) {
				if (option.data.name == optionName) realOption = option;
			});
			if (realOption) {
				this.set('optionText', realOption.data.text);
			}
		}
	},
	/**
	 * List of available options for the user to import
	 */
	availableOptions: {
		mtype: 'store',
		fields: ['name', 'text'],
		data: [{
			name: 'gender',
			text: ['Male', 'Female', 'Prefer Not to Answer'].join('\n')
		}, {
			name: 'age',
			text: ['Under 18', '8-24', '25-34', '35-44', '45-54', '55-64', '65 or Above', 'Prefer Not to Answer'].join('\n')
		}, {
			name: 'employment',
			text: ['Employed Full-Time', 'Employed Part-Time', 'Self-employed', 'Not employed, but looking for work', 'Not employed and not looking for work', 'Homemaker', 'Retired', 'Student', 'Prefer Not to Answer'].join('\n')
		}, {
			name: 'income',
			text: ['Under $20,000', '$20,000 - $30,000', '$30,000 - $40,000', '$40,000 - $50,000', '$50,000 - $75,000', '$75,000 - $100,000', '$100,000 - $150,000', '$150,000 or more', 'Prefer Not to Answer'].join('\n')
		}, {
			name: 'education',
			text: ['Some High School', 'High School Graduate or Equivalent', 'Trade or Vocational Degree', 'Some College', 'Associate Degree', 'Bachelor\'s Degree', 'Graduate of Professional Degree', 'Prefer Not to Answer'].join('\n')
		}, {
			name: 'daysOfTheWeek',
			text: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'].join('\n')
		}, {
			name: 'monthsOfTheYear',
			text: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'].join('\n')
		}, {
			name: 'timezones',
			text: ['(-12:00) International Date Line', '(-11:00) Midway Island, Samoa', '(-10:00) Hawaii', '(-9:00) Alaska', '(-8:00) Pacific', '(-7:00) Mountain', '(-6:00) Central', '(-5:00) Eastern', '(-4:30) Venezuela', '(-4:00) Atlantic', '(-3:30) Newfoundland', '(-3:00) Brasilia, Buenos Aires, Georgetown', '(-2:00) Mid-Atlantic', '(-1:00) Azores, Cape Verde Isles', '(0:00) Dublin, London, Monrovia, Casablanca', '(+1:00) Paris, Madrid, Athens, Berlin, Rome', '(+2:00) Eastern Europe, Harare, Pretoria, Israel', '(+3:00) Moscow, Baghdad, Kuwait, Nairobi', '(+3:30) Tehran', '(+4:00) Abu Dhabi, Muscat, Tbilisi, Kazan', '(+4:30) Kabul', '(+5:00) Islamabad, Karachi, Ekaterinburg', '(+5:30) Bombay, Calcutta, Madras, New Delhi', '(+6:00) Almaty, Dhaka', '(+7:00) Bangkok, Jakarta, Hanoi', '(+8:00) Beijing, Hong Kong, Perth, Singapore', '(+9:00) Tokyo, Osaka, Sapporo, Seoul, Yakutsk', '(+9:30) Adelaide, Darwin', '(+10:00) Sydney, Guam, Port Moresby, Vladivostok', '(+11:00) Magadan, Solomon Isles, New Caledonia', '(+12:00) Fiji, Marshall Isles, Wellington, Auckland'].join('\n')
		}, {
			name: 'states',
			text: ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'].join('\n')
		}, {
			name: 'continents',
			text: ['Africa', 'Antarctica', 'Asia', 'Australia', 'Europe', 'North America', 'South America'].join('\n')
		}, {
			name: 'howOften',
			text: ['Everyday', 'Once a week', '2 to 3 times a month', 'Once a month', 'Less than once a month'].join('\n')
		}, {
			name: 'howLong',
			text: ['Less than a month', '1-6 months', '1-3 years', 'Over 3 Years', 'Never used'].join('\n')
		}, {
			name: 'satisfaction',
			text: ['Very Satisfied', 'Satisfied', 'Neutral', 'Unsatisfied', 'Very Unsatisfied', 'N/A'].join('\n')
		}, {
			name: 'importance',
			text: ['Very Important', 'Important', 'Neutral', 'Somewhat Important', 'Not at all Important', 'N/A'].join('\n')
		}, {
			name: 'agreement',
			text: ['Strongly Agree', 'Agree', 'Neutral', 'Disagree', 'Strongly Disagree', 'N/A'].join('\n')
		}, {
			name: 'comparison',
			text: ['Much Better', 'Somewhat Better', 'About the Same', 'Somewhat Worse', 'Much Worse', 'Don\'t Know'].join('\n')
		}, {
			name: 'wouldYou',
			text: ['Definitely', 'Probably', 'Not Sure', 'Probably Not', 'Definitely Not'].join('\n')
		}, {
			name: 'nflTeams',
			text: ['Arizona Cardinals', 'Atlanta Falcons', 'Baltimore Ravens', 'Buffalo Bills', 'Carolina Panthers', 'Chicago Bears', 'Cincinnati Bengals', 'Cleveland Browns', 'Dallas Cowboys', 'Denver Broncos', 'Detroit Lions', 'Green Bay Packers', 'Houston Texans', 'Indianapolis Colts', 'Jacksonville Jaguars', 'Kansas City Chiefs', 'Miami Dolphins', 'Minnesota Vikings', 'New England Patriots', 'New Orleans Saints', 'New York Giants', 'New York Jets', 'Oakland Raiders', 'Philadelphia Eagles', 'Pittsburgh Steelers', 'San Diego Chargers', 'San Francisco 49ers', 'Seattle Seahawks', 'St. Louis Rams', 'Tampa Bay Buccaneers', 'Tennessee Titans', 'Washington Redskins'].join('\n')
		}]
	},
	/**
	 * Button Handler to close the window
	 */
	cancel: function() {
		this.doClose();
	},
	/**
	 * Button Handler that adds the selected options to the options of the control
	 */
	importOption: function() {
		//import the options from the textarea into the options for the parentVM
		var text = this.optionText;
		var options = text.split('\n');
		Ext.each(options, function(option) {
			if (option) this.parentVM.addOption(null, option);
		}, this);
		this.doClose();
	},

	init: function() {
		this.availableOptions.each(function(option) {
			option.data.name = this.localize(option.data.name);
		}, this);
	}
});
/**
 * Model definition for the Main entry point to the form builder
 * Main is the launch point for the form builder and contains the form that is going to be built.  It also displays the detail information
 * for the currently selected control and shows information when there are no fields and no selected fields
 */
glu.defModel('RS.formbuilder.Main', {
	create: false,
	atId: '',
	rbId: '',

	initialState: null,
	activate: function(screen, params) {
		//Change the window title to the current form name
		this.mainForm.windowTitle$()
		this.rootVM.set('fileUploadIsEnabled', true);
		// if (params && params.id) this.set('id', params.id)
		// if (params && params.name) this.set('name', params.name)
		// if (params && params.formName) this.set('formName', params.formName)
		// if (params && params.view) this.set('formName', params.view)
		this.set('id', params && params.id ? params.id : '')
			// this.set('formName', params && params.formName ? params.formName : '')
			// this.set('formName', params && params.view ? params.view : '')
		this.set('formName', params && (params.name || params.formName || params.view) ? params.name || params.formName || params.view : '')

		if (params && params.create === 'true') this.set('create', true)
		this.set('atId', params.atId || '')
		this.set('rbId', params.rbId || '')
		this.mainForm.customTablesList.load({
			scope: this.mainForm,
			callback: this.mainForm.customTablesLoaded
		});
		this.existingFormNames.load();
		this.sirContext = false;
		this.loadForm();
	},
	id: '',
	tabPanelId: '',

	/**
	 *The id of the form to load if one is provided (edit mode)
	 */
	formId: '',

	/**
	 *The name of the form to load if one is provided (edit mode)
	 */
	formName: '',

	/**
	 *Tells the editor whether the user is an admin or not.  Non-admin cannot create tables/columns and must choose from exiting db structures
	 */
	isAdmin: true,

	/**
	 *Name of the wiki document to pass through to the form to know which wiki document to link to, or which wiki document to create
	 */
	wikiName: '',

	/**
	 *List of controls that are available to the user to add to the form
	 */
	availableControls: {
		mtype: 'list',
		autoParent: true
	},

	/**
	 *List of the form names that are currently in the system
	 */
	existingFormNames: {
		mtype: 'store',
		fields: ['name'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getformnames',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	/**
	 *Instance of the main form that we are building
	 */
	mainForm: {
		mtype: 'Form'
	},

	back: function() {
		var current = this.convertToRSForm();
		if (Ext.encode(current).replace(/"[^:]+"\:(""|null),/g, '') != Ext.encode(this.initialState).replace(/"[^:]+"\:(""|null),/g, ''))
			this.message({
				title: this.localize('leaveWithoutSaveTitle'),
				msg: this.localize('leaveWithoutSave'),
				button: Ext.MessageBox.YESNO,
				buttonText: {
					'yes': this.localize('leave'),
					'no': this.localize('cancel')
				},
				scope: this,
				fn: function(btn) {
					if (btn == 'no')
						return;
					clientVM.handleNavigation({
						modelName: 'RS.formbuilder.Forms'
					});
				}
			});
		else
			clientVM.handleNavigation({
				modelName: 'RS.formbuilder.Forms'
			});
	},

	sirContext: false,
	unSupportedFieldsMap: {
		FileUploadField: 'File Upload',
		JournalField: 'Journal',
		SequenceField: 'Sequence',
		ReferenceField: 'Reference',
		UserPickerField: 'User Picker',
		TeamPickerField: 'Team Picker',
		ReferenceTableField: 'Reference Table',
		TabContainerField: 'Tab Container',
		SectionContainerField: 'Section'
	},

	enforcingSIRContext: function() {
		this.sirContext = false;
		this.mainForm.formButtons.forEach(function(btn) {
			btn.actions.forEach(function(action) {
				if (action.operationName === 'SIR'&& action.sirAction === 'saveCaseData') {
					this.sirContext = true;
				}
			}, this);
		}, this);
		this.availableControls.foreach(function(availableControl) {
			switch (availableControl.controlType) {
				case 'FileUploadField':
				case 'JournalField':
				case 'SequenceField':
				case 'ReferenceField':
				case 'UserPickerField':
				case 'TeamPickerField':
				case 'ReferenceTableField':
				case 'TabContainerField':
				case 'SectionContainerField':
					availableControl.set('isDisabled', this.sirContext);
					break;
				default:
					break;
			}
		}, this);
		if (this.sirContext) {
			this.validateSIRFields();
		} else {
			this.mainForm.controls.foreach(function(c) {
				var fieldName = c.viewmodelName;
				if (this.unSupportedFieldsMap[fieldName]) {
					c.set('unsupportedField', false);
				}
			
			}, this);
		}
	},

	resetFormControls: function() {
		this.availableControls.foreach(function(availableControl) {
			availableControl.set('isDisabled', false);
		}, this);
	},

	init: function() {
		//Initialize the available controls from the namespace
		var model;
		for (model in RS.formbuilder.viewmodels) {
			if (model.indexOf('Field') > -1 && model != 'HiddenField' && model != 'ButtonField') this.availableControls.add(this.model({
				mtype: 'viewmodel',
				name: this.localize(model),
				controlType: model,
				isDisabled: false
			}))
		}

		if (this.name) this.set('formName', this.name)
		if (this.view) this.set('formName', this.view)

		// if (this.id || this.formId || this.formName) {
		// 	//Load the form data
		// 	this.loadForm();
		// } else {
		// 	Ext.Ajax.request({
		// 		url: '/resolve/service/form/getDefaultAccessRights',
		// 		scope: this,
		// 		success: this.loadDefaultRights,
		// 		failure: function(r) {
		// 			Ext.Msg.alert('Error', 'An unknown error has occurred.');
		// 		}
		// 	});
		// }

		this.existingFormNames.load();

		//initialize the main form to get the translated strings
		this.mainForm.set('wikiName', this.wikiName);
		this.mainForm.init();

		// generate page token for actiontask/editorstub.jsp
		clientVM.getCSRFToken_ForURI('/resolve/customtable/customtable.jsp');
	},

	loadForm: function() {
		if (this.id || this.formId || this.formName) {
			//Display loading message
			Ext.MessageBox.show({
				msg: this.localize('loadingMessage'),
				progressText: this.localize('loadingProgress'),
				width: 300,
				wait: true,
				waitConfig: {
					interval: 200
				}
				//icon: 'ext-mb-save'
			});

			this.ajax({
				url: '/resolve/service/form/getformfb',
				params: {
					view: this.formName,
					formsysid: this.id || this.formId
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) {
						if (response.data) {
							this.convertFromRSForm(response.data);
							this.mainForm.on('afterColumnStoreLoaded', function() {
								this.set('initialState', this.convertToRSForm());
								return 'discard';
							}, this);
							this.enforcingSIRContext();
						} else clientVM.displayError(this.localize('noDataFromServer'))
					} else {
						Ext.MessageBox.hide()
						if (this.create) {
							this.mainForm.set('name', this.formName)
								//Pull in the default access rights
							this.ajax({
								url: '/resolve/service/form/getDefaultAccessRights',
								success: function(resp) {
									this.loadDefaultRights(resp)
								},
								failure: function(resp) {
									clientVM.displayFailure(resp);
								}
							})

							//If there is a runbook or action task, then load them and pre-populate the form
							if (this.atId) {
								this.ajax({
									url: '/resolve/service/actiontask/get',
									params: {
										id: this.atId,
										name: ''
									},
									success: function(r) {
										var response = RS.common.parsePayload(r)
										if (response.success) {
											//Add the fields for all the inputs from the action task
											if (response.data && response.data.resolveActionInvoc && Ext.isArray(response.data.resolveActionInvoc.resolveActionParameters)) {
												Ext.Array.forEach(response.data.resolveActionInvoc.resolveActionParameters, function(param) {
													if (param.utype.toLowerCase() == 'input') {
														//Add a string field with the information provided
														this.addControl({
															mtype: 'RS.formbuilder.TextField',
															defaultValue: param.udefaultValue,
															encrypted: param.uencrypt,
															mandatory: param.urequired,
															fieldLabel: param.uname,
															inputName: param.uname
														})
													}
												}, this)
											}
											//Add in the execute button to the form with this action task as a step
											this.mainForm.addButton()
											this.mainForm.editRealButton({
												id: this.mainForm.formButtons.getAt(0).id,
												operation: this.localize('execute'),
												operationName: this.localize('execute'),
												dependenciesJson: {}
											})
											this.mainForm.addRealAction(this.mainForm.model({
												mtype: 'RS.formbuilder.Action',
												operation: 'ACTIONTASK',
												PROBLEMID: 'ACTIVE',
												ACTIONTASK: response.data.ufullName
											}))
										} else
											clientVM.displayError(response.message)
									},
									failure: function(r) {
										clientVM.displayFailure(r);
									}
								})
							}
							if (this.rbId) {
								this.ajax({
									url: '/resolve/service/wiki/get',
									params: {
										id: this.rbId,
										name: ''
									},
									success: function(r) {
										var response = RS.common.parsePayload(r)
										if (response.success) {
											try {
												var params = Ext.decode(response.data.uwikiParameters || '[]'),
													mtype;
												//sort by order of the params
												Ext.Array.sort(params, function(a, b) {
													if (a.order < b.order) return -1
													if (a.order > b.order) return 1
													return 0
												})

												// Add the fields for all the inputs from the runbook
												Ext.Array.forEach(params, function(param) {
													switch (param.type) {
														case 'int':
															mtype = 'RS.formbuilder.NumberField'
															break;
														case 'decimal':
															mtype = 'RS.formbuilder.DecimalField'
															break;
														case 'date':
															mtype = 'RS.formbuilder.DateField'
															break;
														case 'string':
														default:
															mtype = 'RS.formbuilder.TextField'
															break;
													}

													//Add a field with the information provided
													this.addControl({
														mtype: mtype,
														defaultValue: param.defaultValue,
														fieldLabel: param.name,
														inputName: param.name
													})
												}, this)
											} catch (e) {
												//Don't worry about this because there could be bad params data from the server
											}

											//Add in the execute button to the form with this action task as a step
											this.mainForm.addButton()
											this.mainForm.editRealButton({
												id: this.mainForm.formButtons.getAt(0).id,
												operation: this.localize('execute'),
												operationName: this.localize('execute'),
												dependenciesJson: {}
											})
											this.mainForm.addRealAction(this.mainForm.model({
												mtype: 'RS.formbuilder.Action',
												operation: 'RUNBOOK',
												PROBLEMID: 'ACTIVE',
												RUNBOOK: response.data.ufullname
											}))
										} else
											clientVM.displayError(response.message)
									},
									failure: function(r) {
										clientVM.displayFailure(r);
									}
								})
							}
						} else {
							clientVM.displayError(response.message)
						}
					}
				},
				failure: function(r) {
					Ext.MessageBox.hide()
					clientVM.displayFailure(r);
				}
			});
		} else {
			this.resetFormControls();
			this.clearExistingForm({
				id: '',
				panels: [{
					id: '',
					tabs: [{
						id: ''
					}]
				}],
				wizard: false,
				formName: this.localize('UNTITLEDFORM'),
				displayName: this.localize('UntitledForm'),
				tableName: ''
			})

			this.ajax({
				url: '/resolve/service/form/getDefaultAccessRights',
				success: function(resp) {
					this.loadDefaultRights(resp);
					this.set('initialState', this.convertToRSForm());
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			});
		}
	},

	loadDefaultRights: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			this.processAccessRights({
				viewRoles: response.data.read,
				editRoles: response.data.edit,
				adminRoles: response.data.admin
			});
		}
	},

	/**
	 *Button Handler for previewing the form
	 */
	preview: function() {
		var form = this.convertToRSForm();
		Ext.create('Ext.window.Window', {
			title: this.localize('preview'),
			modal: true,
			border: false,
			listeners: {
				beforeshow: function() {
					clientVM.setPopupSizeToEightyPercent(this);
				}
			},
			layout: 'fit',
			padding : 15,
			cls : 'rs-modal-popup',
			items: [Ext.create('RS.formbuilder.viewer.desktop.RSForm', {
				formConfiguration: form,
			})]
		}).show();
	},

	/**
	 *Button handler to load the real version of the form stored on the database
	 */
	live: function() {
		Ext.create('Ext.window.Window', {
			title: this.localize('live'),
			modal: true,
			border: false,
			width: 600,
			height: 400,
			layout: 'fit',
			items: {
				xtype: 'rsform'
			}
		}).show();
	},

	refreshForm: function() {
		Ext.MessageBox.show({
			title: this.localize('refreshFormTitle'),
			msg: this.localize('refreshFormBody'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('discardChanges'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.confirmRefreshForm
		});
	},

	confirmRefreshForm: function(btn) {
		if (btn === 'yes') this.loadForm();
	},

	/**
	 *Determines if the save button should be enabled because the form is dirty and valid
	 */
	saveFormIsEnabled$: function() {
		return this.mainForm.isValid // && this.mainForm.isDirty;
	},

	/**
	 *Adds a control to the form
	 */
	addControl: function(controlConfig) {
		this.mainForm.addControl(controlConfig);
	},
	/**
	 *Makes sure the control is valid, if its not it displays a message
	 */
	validateControl: function(control, tab) {
		if (control.isValid === false) {
			if (tab) this.mainForm.tabPanel.set('selectedTab', tab);
			control.selectControl();
			Ext.Msg.alert(this.localize('validationErrorTitle'), this.localize('validationErrorBody', [control.fieldLabel]));
			return false;
		}
		if (control.inputType == 'INPUT' && control.inputName == this.localize('Untitled')) {
			if (tab) this.mainForm.tabPanel.set('selectedTab', tab);
			control.selectControl();
			Ext.Msg.alert(this.localize('untitledFieldNameTitle'), this.localize('untitledFieldNameBody', name));
			return false;
		}
		if (control.inputType == 'DB' && !this.mainForm.dbTable) {
			if (tab) this.mainForm.tabPanel.set('selectedTab', tab);
			control.selectControl();
			Ext.Msg.alert(this.localize('inputFieldDBWithoutTableTitle'), this.localize('inputFieldDBWithoutTableBody', name));
			return false;
		}
		return true;
	},

	getFormFieldNames: function() {
		var fieldNames = [];
		//populate list of field names from controls to determine dependency validations
		if (this.mainForm.useTabs) {
			this.mainForm.tabPanel.syncCurrentTab();
			this.mainForm.tabPanel.tabs.foreach(function(tab) {
				Ext.each(tab.controls, function(control) {
					if (control && control.viewmodelName != 'SpacerField') fieldNames.push((control.inputType == 'INPUT' ? control.inputName : control.column) || control.fieldLabel);
				}, this)
			}, this)
		} else this.mainForm.controls.foreach(function(control) {
			if (control && control.viewmodelName != 'SpacerField') fieldNames.push((control.inputType == 'INPUT' ? control.inputName : control.column) || control.fieldLabel);
		}, this);
		return fieldNames;
	},

	validateFormFieldNames: function() {
		var fieldNames = {},
			name = '',
			valid = true;

		//populate list of field names from controls to determine dependency validations
		if (this.mainForm.useTabs) {
			this.mainForm.tabPanel.syncCurrentTab();
			this.mainForm.tabPanel.tabs.foreach(function(tab) {
				Ext.each(tab.controls, function(control) {
					if (control) {
						name = (control.inputType == 'INPUT' ? control.inputName : control.column) || control.fieldLabel;
						if (name) {
							if (fieldNames[name]) {
								Ext.Msg.alert(this.localize('duplicateFieldNameTitle'), this.localize('duplicateFieldNameBody', name));
								valid = false;
								return false;
							} else {
								fieldNames[name] = 'something';
							}
						}
						if (control.columns && control.columns.length)
							control.columns.foreach(function(col) {
								col.controls.foreach(function(subControl) {
									name = (subControl.inputType == 'INPUT' ? subControl.inputName : subControl.column) || subControl.fieldLabel;
									if (name) {
										if (fieldNames[name]) {
											Ext.Msg.alert(this.localize('duplicateFieldNameTitle'), this.localize('duplicateFieldNameBody', name));
											valid = false;
											return false;
										} else {
											fieldNames[name] = 'something';
										}
									}
								})
							})
					}
				}, this)
			}, this)
		} else this.mainForm.controls.foreach(function(control) {
			if (control) {
				name = (control.inputType == 'INPUT' ? control.inputName : control.column) || control.fieldLabel;
				if (name) {
					if (fieldNames[name]) {
						Ext.Msg.alert(this.localize('duplicateFieldNameTitle'), this.localize('duplicateFieldNameBody', name));
						valid = false;
						return false;
					} else {
						fieldNames[name] = 'something';
					}
				}
				if (typeof control.columns === "object") {
					control.columns.foreach(function(col) {
						col.controls.foreach(function(subControl) {
							name = (subControl.inputType == 'INPUT' ? subControl.inputName : subControl.column) || subControl.fieldLabel;
							if (name) {
								if (fieldNames[name]) {
									Ext.Msg.alert(this.localize('duplicateFieldNameTitle'), this.localize('duplicateFieldNameBody', name));
									valid = false;
									return false;
								} else {
									fieldNames[name] = 'something';
								}
							}
						});
					});
				}
			}
		}, this);

		//Now make sure to check hidden fields too
		this.mainForm.hiddenFields.each(function(field) {
			if (fieldNames[field.data.name]) {
				Ext.Msg.alert(this.localize('duplicateFieldNameTitle'), this.localize('duplicateFieldNameBody', field.data.name));
				valid = false;
				return false;
			} else {
				fieldNames[field.data.name] = 'something';
			}
		}, this);
		return valid;
	},

	deleteForm: function() {
		Ext.MessageBox.show({
			title: this.localize('deleteFormTitle'),
			msg: this.localize('deleteFormBody', [this.mainForm.title, this.mainForm.properName]),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteForm'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteForm
		});
	},

	deleteFormIsEnabled$: function() {
		return this.id;
	},

	reallyDeleteForm: function(button) {
		if (button === 'yes') {
			Ext.Ajax.request({
				url: '/resolve/service/formbuilder/deleteForm',
				params: {
					formId: this.id
				},
				scope: this,
				success: this.deletedForm,
				failure: function(r){
					clientVM.displayFailure(r);
				}
			});
		}
	},

	deletedForm: function(r) {
		var response = RS.common.parsePayload(r);
		if (response.success) {
			this.clearExistingForm({
				id: '',
				panels: [{
					id: '',
					tabs: [{
						id: ''
					}]
				}],
				wizard: false,
				formName: this.localize('UNTITLEDFORM'),
				displayName: this.localize('UntitledForm'),
				tableName: ''
			});
		} else {
			clientVM.displayError(response.message);
		}
	},

	saveFormAs: function() {
		if (!this.validateSIRFields()) {
			return;
		}
		if (this.validateMainForm()) {
			var rsForm = this.convertToRSForm();
			//check to make sure there are access rights
			if (rsForm.adminRoles == '') {
				Ext.Msg.alert(this.localize('noAccessRightsTitle'), this.localize('noAccessRightsBody'));
				return;
			}
			this.open({
				mtype: 'SaveFormAs',
				name: rsForm.formName + '_' + this.localize('COPY'),
				title: rsForm.displayName + ' ' + this.localize('Copy')
			});
		}
	},
	saveFormAsIsEnabled$: function() {
		return this.id;
	},
	reallySaveFormAs: function(name, title) {
		var rsForm = this.convertToRSForm();
		rsForm.formName = name;
		rsForm.viewName = name;
		rsForm.displayName = title;

		//Wipe out all the id's in the current form because we're trying to save this form as a copy
		rsForm.id = '';
		rsForm.buttonPanel.id = '';
		Ext.each(rsForm.buttonPanel.controls, function(control) {
			control.id = '';
			Ext.each(control.dependencies, function(dep) {
				dep.id = '';
			});
		});
		Ext.each(rsForm.panels, function(panel) {
			panel.id = '';
			Ext.each(panel.tabs, function(tab) {
				tab.id = '';
				Ext.each(tab.columns, function(column) {
					Ext.each(column.fields, function(field) {
						field.id = '';
						Ext.each(field.dependencies, function(dep) {
							dep.id = '';
						});
					})
				});
			});
		});
		this.persistForm(rsForm, false);
	},

	validateMainForm: function() {
		var valid = true,
			fieldNames = this.getFormFieldNames();

		valid = this.validateFormFieldNames();
		if (!valid) return false;

		//make sure there is a formName
		if (!this.mainForm.properName) {
			valid = false;
			this.mainForm.set('showFieldSettings', 0);
			Ext.Msg.alert(this.localize('noFormNameTitle'), this.localize('noFormNameBody'));
			return false;
		}

		//Determine if the controls on the page are all valid
		if (this.mainForm.useTabs) {
			this.mainForm.tabPanel.tabs.foreach(function(tab) {
				Ext.each(tab.controls, function(control) {
					if (control) {
						//TODO: If control is a section, then check each column's controls as well
						if (!this.validateControl(control, tab)) {
							valid = false;
							return false;
						}
						if (control.dependenciesStore) {
							control.dependenciesStore.each(function(dep) {
								if (dep.data.target && fieldNames.indexOf(dep.data.target) == -1) {
									Ext.Msg.alert(this.localize('dependencyTargetInvalidTitle'), this.localize('dependencyTargetInvalidBody', [(control.inputType == 'INPUT' ? control.inputName : control.column) || control.fieldLabel, dep.data.target]));
									valid = false;
									return false;
								}
							}, this)
						}
					}
				}, this)
			}, this)
		} else this.mainForm.controls.foreach(function(control) {
			if (control) {
				//TODO: if control is a section, then check the column's controls too
				if (!this.validateControl(control)) {
					valid = false;
					return false;
				}
				if (control.dependenciesStore) {
					control.dependenciesStore.each(function(dep) {
						if (dep.data.target && fieldNames.indexOf(dep.data.target) == -1) {
							Ext.Msg.alert(this.localize('dependencyTargetInvalidTitle'), this.localize('dependencyTargetInvalidBody', [(control.inputType == 'INPUT' ? control.inputName : control.column) || control.fieldLabel, dep.data.target]));
							valid = false;
							return false;
						}
					}, this)
				}
			}
		}, this);

		//Check duplicate name of form
		if (this.mainForm.titleIsValid !== true) {
			Ext.Msg.alert(this.localize('duplicateNameDetectedTitle'), this.mainForm.titleIsValid);
			return false;
		}

		if (this.mainForm.nameIsValid !== true) {
			Ext.Msg.alert(this.localize('formNameInvalidTitle'), this.mainForm.nameIsValid);
			return false;
		}

		//If there isn't a table associated with this form, and there's a file upload field, that's bad... so throw an error
		if (!this.mainForm.dbTable) {
			if (fieldNames.indexOf('u_fileupload') > -1) {
				Ext.Msg.alert(this.localize('fileUploadNoTableTitle'), this.localize('fileUploadNoTableBody'));
				return false;
			}
		}

		//Check for button dependencies targets existing (buttons should be valid too)
		this.mainForm.formButtons.foreach(function(button) {
			if (button.dependencies) {
				button.dependencies.each(function(dep) {
					if (dep.data.target && fieldNames.indexOf(dep.data.target) == -1) {
						Ext.Msg.alert(this.localize('dependencyTargetInvalidTitle'), this.localize('dependencyTargetInvalidBody', [button.operation, dep.data.target]));
						valid = false;
						return false;
					}
				}, this)
			}
			if (button.type == 'button' && (!button.actions || button.actions.length == 0)) {
				Ext.Msg.alert(this.localize('noButtonActionTitle'), this.localize('noButtonActionBody', [button.operation]));
				valid = false;
				return false;
			}
		}, this);

		return valid;
	},

	/**
	 *Opens a dialog of the changes to be made to the server when the save is done
	 */
	validateSIRFields: function() {
		if (this.sirContext) {
			var unSupportedFields = [];
			var unSupportedControls = [];
			this.mainForm.controls.foreach(function(c) {
				var fieldName = c.viewmodelName;
				if (this.unSupportedFieldsMap[fieldName]) {
					unSupportedFields.push(this.unSupportedFieldsMap[fieldName]);
					unSupportedControls.push(c);
				}
			
			}, this);
			if (unSupportedFields.length) {
				var msg = Ext.String.format('For SIR action, the fields "{0}" must be deleted before the form can be saved.', unSupportedFields.join(', '));
				Ext.Msg.alert('Unsupported Field(s) Found on SIR Action', msg);
				for (var i = 0; i < unSupportedControls.length; i++) {
					unSupportedControls[i].set('unsupportedField', true);
				}
				return false;
			}
		}
		return true;
	},
	saveForm: function() {
		if (!this.validateSIRFields()) {
			return;
		}
		if (this.validateMainForm()) {
			var rsForm = this.convertToRSForm();
			//check to make sure there are access rights
			if (rsForm.adminRoles == '') {
				Ext.Msg.alert(this.localize('noAccessRightsTitle'), this.localize('noAccessRightsBody'));
				return;
			}
			this.open({
				mtype: 'PreviewTable',
				formConfig: rsForm
			});
		}
	},

	newForm: function() {
		window.open((window.location.origin ? window.location.origin : window.location.protocol + '//' + window.location.host) + '/resolve/jsp/rsclient.jsp?' + clientVM.rsclientToken + '&title=Custom%20Form%20View&url=%2Fresolve%2Fformbuilder%2Fformbuilder.jsp%3F');
	},
	/**
	 *Converts the form to server data and saves the form on the server
	 */
	confirmSave: function(previewTableData) {
		var rsForm = this.convertToRSForm(true);

		if (previewTableData.addToWiki) rsForm.wikiName = previewTableData.wikiName;
		if (previewTableData.createWiki) rsForm.wikiName = previewTableData.wikiNameCreate;
		if (previewTableData.noWiki) rsForm.wikiName = '';

		this.persistForm(rsForm);
	},

	persistForm: function(rsForm, preventReload) {
		Ext.MessageBox.show({
			msg: this.localize('savingMessage'),
			progressText: this.localize('savingProgress'),
			width: 300,
			wait: true,
			waitConfig: {
				interval: 200
			}
			//icon: 'ext-mb-save'
		});

		this.ajax({
			url: '/resolve/service/formbuilder/submitfb',
			jsonData: rsForm,
			timeout: 300000,
			scope: this,
			success: function(r) {
				Ext.MessageBox.hide();
				var response = RS.common.parsePayload(r);
				if (response.success) {
					clientVM.displaySuccess(this.localize('formSavedSuccessfully'))
					if (response.data && !preventReload) {
						this.convertFromRSForm(response.data);
						this.mainForm.on('afterColumnStoreLoaded', function() {
							this.set('initialState', this.convertToRSForm());
							return 'discard'
						}, this)
					}
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				Ext.MessageBox.hide()
				clientVM.displayFailure(r);
			}
		});
	},

	/**
	 *Converts the mainForm to a correct server interpretation to be persisted on the db
	 */
	convertToRSForm: function(isSave) {
		isSave = !!isSave;  // Making default to 'false'

		var form = {
				properties: {},
				panels: []
			},
			mainForm = this.mainForm,
			tabs = [];

		//Make sure things are in sync before we start serializing everything
		if (mainForm.useTabs) mainForm.tabPanel.syncCurrentTab();

		//Convert to form here
		form.id = this.id;
		form.displayName = mainForm.title;
		form.formName = mainForm.properName;
		form.viewName = mainForm.properName;
		form.tableName = mainForm.dbTable;
		form.wizard = mainForm.isWizard;
		form.windowTitle = mainForm.formWindowTitle;
		var tableRecord = mainForm.customTablesList.findRecord('umodelName', mainForm.dbTable, 0, false, false, true);
		form.tableDisplayName = tableRecord ? tableRecord.data.udisplayName : mainForm.dbTable;
		form.tableSysId = tableRecord ? tableRecord.data.sys_id : '';
		form.wikiName = mainForm.wikiName;

		if (mainForm.useTabs) {
			mainForm.tabPanel.tabs.foreach(function(tab, index) {
				var formFields = this.serializeControls(tab.controls);
				if (formFields) {
					for (var i=0; i<formFields.length; i++) {
						if (formFields[i].uiType === 'UrlContainerField' && formFields[i].referenceTableUrl) {
							const urlData = formFields[i].referenceTableUrl.split('#');
							const uriData = urlData[0].split('?');
							clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
								var csrftoken = '?' + data[0] + '=' + data[1];
								var tokenizedData = uri + csrftoken;
								if (uriData.length > 1) {
									tokenizedData += '&' + uriData[1];
								}
								if (urlData.length > 1) {
									tokenizedData += '#' + urlData[1];
								}
								formFields[i].referenceTableUrl = tokenizedData;
							});
						}
					}
				}
				var deps = tab.getDependencyControlConversionProperties().dependencies;
				tabs.push({
					id: tab.id,
					name: tab.title,
					displayName: tab.title,
					orderNumber: index + 1,
					cols: 1,
					columns: [{
						fields: formFields
					}],
					dependencies: deps
				});
			}, this);
		} else {
			//add in controls from the form
			var formFields = this.serializeControls(mainForm.controls);
			tabs.push({
				id: mainForm.id,
				cols: 1,
				columns: [{
					fields: formFields
				}]
			});
		}
		//buid structure for backend
		form.panels = [{
			id: this.tabPanelId,
			tabs: tabs
		}];

		//add in hidden fields
		var hiddenFields = this.convertHiddenFields(mainForm.hiddenFields);
		form.panels[0].tabs[0].columns[0].fields = form.panels[0].tabs[0].columns[0].fields.concat(hiddenFields);

		//Add in buttons
		var formButtons = this.serializeButtons(mainForm.formButtons, isSave);
		form.buttonPanel = {
			controls: formButtons
		};

		//Add in access rights
		var viewRoles = [],
			editRoles = [],
			adminRoles = [];
		this.mainForm.accessRights.each(function(accessRight) {
			if (accessRight.data.view) viewRoles.push(accessRight.data.name);
			if (accessRight.data.edit) editRoles.push(accessRight.data.name);
			if (accessRight.data.admin) adminRoles.push(accessRight.data.name);
		});

		form.viewRoles = viewRoles.join(',');
		form.editRoles = editRoles.join(',');
		form.adminRoles = adminRoles.join(',');

		return form;
	},

	clearExistingForm: function(rsForm) {
		this.set('id', rsForm.id);
		this.mainForm.controls.removeAll();
		this.set('tabPanelId', rsForm.panels[0].id);
		this.mainForm.tabPanel.clearControls();
		this.mainForm.set('useTabs', false);
		this.mainForm.hiddenFields.removeAll();
		try {
			this.mainForm.accessRights.removeAll();
		} catch (e) {}
		this.mainForm.formButtons.removeAll();	
		this.mainForm.treeStore.removed = [];
		this.mainForm.set('formButtonsSelections', []);
		this.mainForm.set('isWizard', rsForm.wizard);

		this.mainForm.set('id', rsForm.panels[0].tabs[0].id);
		this.mainForm.set('name', rsForm.formName)
		this.mainForm.set('title', rsForm.displayName || '');
		this.mainForm.set('dbTable', rsForm.tableName || '');
		this.mainForm.set('formWindowTitle', rsForm.windowTitle);
		this.mainForm.set('wikiName', rsForm.wikiName);

		this.mainForm.runbookStore.load();
		this.mainForm.scriptStore.load();
		this.mainForm.actionTaskStore.load();
		this.mainForm.wikiNames.load();
	},
	convertFromRSForm: function(rsForm) {
		if (!rsForm) {
			Ext.MessageBox.hide()
			clientVM.displayError(this.localize('noFormBody'))
			return
		}
		if (!rsForm.panels || rsForm.panels.length == 0) rsForm.panels = [{
			tabs: [{
				columns: [{
					fields: []
				}]
			}]
		}];

		var selectedTabIndex = this.mainForm.tabPanel.tabs.indexOf(this.mainForm.tabPanel.selectedTab);

		rsForm.buttonPanel = rsForm.buttonPanel || {
			controls: []
		};

		this.mainForm.customTablesList.load();
		if (rsForm.tableName) {
			this.mainForm.columnStore.removeAll();
			this.mainForm.columnStore.load();
		}
		this.existingFormNames.load();

		this.clearExistingForm(rsForm);

		var buttonPanel = rsForm.buttonPanel.controls;
		if (rsForm.panels[0].tabs.length > 1 || rsForm.panels[0].tabs[0].name) {
			this.mainForm.set('useTabs', true);

			this.mainForm.tabPanel.tabs.removeAll();
			Ext.Array.forEach(rsForm.panels[0].tabs, function(tab) {
				if (tab.columns.length > 0) {
					var fields = [];
					Ext.each(tab.columns[0].fields, function(fieldConfig) {
						fields.push(this.deserializeControlNoAdd(fieldConfig))
					}, this);
					this.mainForm.tabPanel.addTab({
						id: tab.id,
						title: tab.name,
						controls: fields
					});
					var newTab = this.mainForm.tabPanel.tabs.getAt(this.mainForm.tabPanel.tabs.length - 1);
					newTab.parseDependencyControlConversionProperties(tab);
				}
			}, this);

			if (this.mainForm.tabPanel.tabs.length > 0) this.mainForm.tabPanel.set('selectedTab', this.mainForm.tabPanel.tabs.getAt(selectedTabIndex < this.mainForm.tabPanel.tabs.length ? selectedTabIndex : 0));

		} else {
			if (rsForm.panels[0].tabs[0].columns.length > 0) {
				var configuration = rsForm.panels[0].tabs[0].columns[0].fields
					//convert all the controls to appropriate builder controls
				Ext.each(configuration, this.deserializeControl, this);
			}
		}

		//convert the buttons
		Ext.each(buttonPanel, this.deserializeButton, this);

		//ensure the form settings are displayed after restoring a form
		this.mainForm.set('showFieldSettings', 0);

		//process the access rights
		this.processAccessRights(rsForm);

		Ext.dd.DragDropManager.refreshCache({
			formControls: true,
			columnControls: true
		});

		this.mainForm.set('dbTable', rsForm.tableName || '');

		Ext.defer(function() {
			Ext.MessageBox.hide();
		}, 10)
	},

	processAccessRights: function(rsForm) {
		var viewRoles = rsForm.viewRoles ? rsForm.viewRoles.split(',') : [],
			editRoles = rsForm.editRoles ? rsForm.editRoles.split(',') : [],
			adminRoles = rsForm.adminRoles ? rsForm.adminRoles.split(',') : [],
			accessRights = [],
			i;
		for (i = 0; i < viewRoles.length; i++)
			this.addToAccessRights(accessRights, viewRoles[i], 'view', true);
		for (i = 0; i < editRoles.length; i++)
			this.addToAccessRights(accessRights, editRoles[i], 'edit', true);
		for (i = 0; i < adminRoles.length; i++)
			this.addToAccessRights(accessRights, adminRoles[i], 'admin', true);

		this.mainForm.accessRights.add(accessRights);
	},
	addToAccessRights: function(accessRights, name, right, value) {
		var i, len = accessRights.length;
		for (i = 0; i < len; i++) {
			if (accessRights[i].name == name) {
				accessRights[i][right] = value;
				return;
			}
		}

		var newName = {
			name: name,
			value: name
		};
		newName[right] = value;
		accessRights.push(newName);
	},
	/**
	 *Converts controls from extjs to server form to be peristed on the db
	 */
	serializeControls: function(controls) {
		var ctrls = [];
		if (Ext.isArray(controls)) Ext.each(controls, function(control, index) {
			if (control) ctrls.push(this.serializeControl(control, index + 1));
		}, this)
		else if (controls.foreach) controls.foreach(function(control, index) {
			if (control) ctrls.push(this.serializeControl(control, index + 1));
		}, this);

		return ctrls;
	},
	serializeControl: function(control, index) {
		var tmpControl = {
			orderNumber: index
		};

		Ext.each(control.serializeFunctions, function(serializeFunction) {
			Ext.apply(tmpControl, serializeFunction.call(control));
		}, this);

		switch (control.viewmodelName) {
			case 'TextField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'TextField'
				});
				break;

			case 'TextAreaField':
				Ext.apply(tmpControl, {
					xtype: 'textarea',
					uiType: 'TextArea'
				});
				break;
			case 'RadioButtonField':
				Ext.apply(tmpControl, {
					xtype: 'radiogroup',
					uiType: 'RadioButton'
				});
				break;
			case 'NumberField':
				Ext.apply(tmpControl, {
					xtype: 'numberfield',
					uiType: 'NumberTextField'
				});
				break;
			case 'DecimalField':
				Ext.apply(tmpControl, {
					xtype: 'numberfield',
					uiType: 'DecimalTextField'
				});
				break;
			case 'MultipleCheckboxField':
				Ext.apply(tmpControl, {
					xtype: 'checkboxgroup',
					uiType: 'MultipleCheckBox'
				});
				break;
			case 'ComboBoxField':
				Ext.apply(tmpControl, {
					xtype: 'combo',
					uiType: 'ComboBox'
				});
				break;
			case 'MultiSelectComboBoxField':
				Ext.apply(tmpControl, {
					xtype: 'combo',
					uiType: 'MultiSelectComboBoxField'
				});
				break;
			case 'NameField':
				Ext.apply(tmpControl, {
					xtype: 'namefield',
					uiType: 'NameField'
				});
				break;
			case 'AddressField':
				Ext.apply(tmpControl, {
					xtype: 'addressfield',
					uiType: 'AddressField'
				});
				break;
			case 'EmailField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'EmailField'
				});
				break;

			case 'MACAddressField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'MACAddressField'
				});
				break;

			case 'IPAddressField':
			Ext.apply(tmpControl, {
				xtype: 'textfield',
				uiType: 'IPAddressField'
			});
			break;

			case 'CIDRAddressField':
			Ext.apply(tmpControl, {
				xtype: 'textfield',
				uiType: 'CIDRAddressField'
			});
			break;

			case 'PhoneField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'PhoneField'
				});
				break;
			case 'DateField':
				Ext.apply(tmpControl, {
					xtype: 'datefield',
					uiType: 'Date'
				});
				break;
			case 'TimeField':
				Ext.apply(tmpControl, {
					xtype: 'timefield',
					uiType: 'Timestamp'
				});
				break;
			case 'LinkField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'Link'
				});
				break;
			case 'LikeRTField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'TextField'
				});
				break;
			case 'CheckboxField':
				Ext.apply(tmpControl, {
					xtype: 'checkbox',
					uiType: 'CheckBox'
				});
				break;
			case 'PasswordField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'Password'
				});
				break;
			case 'TagField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'TagField'
				});
				break;
			case 'SequenceField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'Sequence'
				});
				break;
			case 'ListField':
				Ext.apply(tmpControl, {
					xtype: 'multiselect',
					uiType: 'List'
				});
				break;
			case 'ButtonField':
				Ext.apply(tmpControl, {
					xtype: 'button',
					uiType: 'ButtonField'
				});
				break;
			case 'ReferenceField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'Reference'
				});
				break;
			case 'JournalField':
				Ext.apply(tmpControl, {
					xtype: 'textfield',
					uiType: 'Journal'
				});
				break;
			case 'DateTimeField':
				Ext.apply(tmpControl, {
					xtype: 'datefield',
					uiType: 'DateTime'
				});
				break;

			case 'ReadOnlyTextField':
				Ext.apply(tmpControl, {
					xtype: 'displayfield',
					uiType: 'ReadOnlyTextField'
				})
				break;

			case 'UserPickerField':
				Ext.apply(tmpControl, {
					xtype: 'userpickerfield',
					uiType: 'UserPickerField'
				})
				break;

			case 'TeamPickerField':
				Ext.apply(tmpControl, {
					xtype: 'teampickerfield',
					uiType: 'TeamPickerField'
				})
				break;

			case 'FileUploadField':
				Ext.apply(tmpControl, {
					xtype: 'uploadmanager',
					uiType: 'FileUploadField'
				});
				break;

			case 'ReferenceTableField':
				Ext.apply(tmpControl, {
					xtype: 'referencetable',
					uiType: 'ReferenceTableField'
				})
				break;

			case 'SpacerField':
				Ext.apply(tmpControl, {
					xtype: 'box',
					uiType: 'SpacerField'
				});
				break;

			case 'LabelField':
				Ext.apply(tmpControl, {
					xtype: 'label',
					uiType: 'LabelField'
				})
				break;

			case 'WysiwygField':
				Ext.apply(tmpControl, {
					xtype: 'htmleditor',
					uiType: 'WysiwygField'
				})
				break;

			case 'TabContainerField':
				Ext.apply(tmpControl, {
					xtype: 'tabcontainer',
					uiType: 'TabContainerField'
				})
				break;

			case 'UrlField':
				Ext.apply(tmpControl, {
					xtype: 'urlcontainerfield',
					uiType: 'UrlContainerField'
				});
				break;

			case 'SectionContainerField':
				Ext.apply(tmpControl, {
					xtype: 'panel',
					uiType: 'SectionContainerField'
				});
				break;

			case 'Panel':
				return null;
				break;

			default:
				break;
		}

		return tmpControl;
	},
	deserializeControlNoAdd: function(config) {
		return this.deserializeControl(config, false);
	},
	deserializeControl: function(config, autoAdd, records, parent) {
		config.fieldLabel = config.displayName;

		switch (config.uiType) {
			case 'TextField':
				config.mtype = 'TextField';
				break;
			case 'TextArea':
				config.mtype = 'TextAreaField';
				break;
			case 'NumberTextField':
				config.mtype = 'NumberField';
				break;
			case 'DecimalTextField':
				config.mtype = 'DecimalField';
				break;
			case 'ComboBox':
				config.mtype = 'ComboBoxField';
				break;
			case 'MultipleCheckBox':
				config.mtype = 'MultipleCheckboxField';
				break;
			case 'RadioButton':
				config.mtype = 'RadioButtonField';
				break;
			case 'Date':
				config.mtype = 'DateField';
				break;
			case 'Timestamp':
				config.mtype = 'TimeField';
				break;
			case 'DateTime':
				config.mtype = 'DateTimeField';
				break;
			case 'Link':
				config.mtype = 'LinkField';
				break;
			case 'CheckBox':
				config.mtype = 'CheckboxField';
				break;
			case 'Password':
				config.mtype = 'PasswordField';
				break;
			case 'TagField':
				config.mtype = 'TagField';
				break;
			case 'Sequence':
				config.mtype = 'SequenceField';
				break;
			case 'List':
				config.mtype = 'ListField';
				break;
			case 'ButtonField':
				config.mtype = 'ButtonField';
				break;
			case 'Reference':
				config.mtype = 'ReferenceField';
				break;
			case 'Journal':
				config.mtype = 'JournalField';
				break;
			case 'ReadOnlyTextField':
				config.mtype = 'ReadOnlyTextField';
				break;
			case 'UserPickerField':
				config.mtype = 'UserPickerField';
				break;
			case 'TeamPickerField':
				config.mtype = 'TeamPickerField';
				break;
			case 'EmailField':
				config.mtype = 'EmailField';
				break;
			case 'MACAddressField':
				config.mtype = 'MACAddressField';
				break;
			case 'IPAddressField':
				config.mtype = 'IPAddressField';
				break;
			case 'CIDRAddressField':
				config.mtype = 'CIDRAddressField';
				break;
			case 'PhoneField':
				config.mtype = 'PhoneField';
				break;
			case 'NameField':
				config.mtype = 'NameField';
				break;
			case 'AddressField':
				config.mtype = 'AddressField';
				break;
			case 'HiddenField':
				config.mtype = 'HiddenField';
				config.value = config.hiddenValue;
				break;
			case 'FileUploadField':
				config.mtype = 'FileUploadField';
				//We're restoring a file upload field, which means that we already have one and shouldn't enable the field by default
				this.set('fileUploadIsEnabled', false);
				break;
			case 'ReferenceTableField':
				config.mtype = 'ReferenceTableField';
				break;
			case 'MultiSelectComboBoxField':
				config.mtype = 'MultiSelectComboBoxField';
				break;
			case 'SpacerField':
				config.mtype = 'SpacerField';
				break;
			case 'LabelField':
				config.mtype = 'LabelField';
				break;
			case 'WysiwygField':
				config.mtype = 'WysiwygField';
				break;
			case 'TabContainerField':
				config.mtype = 'TabContainerField';
				break;
			case 'UrlContainerField':
				config.mtype = 'UrlField';
				break;
			case 'SectionContainerField':
				config.mtype = 'SectionContainerField';
				break;
			default:
				break;
		}

		var ctrl = (parent || this.mainForm).model(config);

		Ext.each(ctrl.deserializeFunctions, function(deserializeFunction) {
			deserializeFunction.call(ctrl, config);
		}, this);

		if (ctrl.viewmodelName == 'HiddenField') {
			this.mainForm.hiddenFields.add(config);
		} else {
			if (autoAdd !== false) {
				this.mainForm.addRealControl(ctrl);
			} else {
				return ctrl;
			}
		}
	},
	/**
	 *Converts hidden fields to controls to be persisted on the db
	 * @param {Object} hiddenFields
	 */
	convertHiddenFields: function(hiddenFields) {
		var controls = [];
		hiddenFields.data.each(function(hiddenField) {
			controls.push({
				hiddenValue: hiddenField.data.value,
				name: hiddenField.data.name,
				sourceType: 'INPUT',
				xtype: 'hiddenfield',
				uiType: 'HiddenField'
			});
		})
		return controls;
	},
	/**
	 *Converts buttons to buttonPanel to be persisted on the db
	 * @param {Object} buttons
	 */
	serializeButtons: function(buttons, isSave) {
		var buttonPanel = [];
		buttons.foreach(function(button,index) {
			button.orderNumber = index+1;
			buttonPanel.push(button.getButtonControlConversionProperties(isSave));
		});
		return buttonPanel;
	},
	deserializeButton: function(button) {
		var btn = this.mainForm.model({
			mtype: 'ButtonControl'
		});
		btn.parseButtonControlConversionProperties(button);
		this.mainForm.formButtons.add(btn);
		btn.parseButtonControlConversionProperties(button, true);
	},

	fileUploadIsEnabled: true,
	when_fileUploadIsEnabled_changes_update_the_available_field: {
		on: ['fileUploadIsEnabledChanged'],
		action: function() {
			this.availableControls.foreach(function(availableControl) {
				if (availableControl.controlType == 'FileUploadField') availableControl.set('isDisabled', this.sirContext || !this.fileUploadIsEnabled);
			}, this);
		}
	},

	showFieldSettings: function() {
		this.mainForm.set('showFieldSettings', 1)
	},
	showFieldSettingsIsPressed$: function() {
		return this.mainForm.showFieldSettings == 1
	},
	showFormSettings: function() {
		this.mainForm.set('showFieldSettings', 0)
	},
	showFormSettingsIsPressed$: function() {
		return this.mainForm.showFieldSettings == 0
	}

});

/**
 * Model definition for an Option
 * An Option is stored in a control like a radio button or dropdown box.  It keeps track of the text the user types for the option
 * and also tracks its default value (which should be either checked or selected)
 */
glu.defModel('RS.formbuilder.Option', {
	/**
	 * Id of the option used for keeping things in sync, this is not persisted to the server
	 */
	id: '',
	/**
	 * Text of the option displayed to the user
	 */
	text: '',
	displayText$: function(){
		return this.text.split('=')[0];
	},
	init: function() {
		this.set('id', Ext.id());
	},
	/**
	 * Determines if the option is selected
	 */
	isSelected$: function() {
		return this.id == this.parentVM.defaultValue;
	},
	/**
	 * Sets the selected attribute of the option to true
	 * @param {Object} value
	 */
	setIsSelected: function(value) {
		if (value === true) this.parentVM.set('defaultValue', this.id);
	},
	/**
	 * Button Handler that forwards the event to the parentVM to properly add another option
	 */
	addOption: function() {
		this.parentVM.addOption(this);
	},
	/**
	 * Button Handler that forwards the event to the parentVM to properly remove this option
	 */
	removeOption: function() {
		this.parentVM.removeOption(this);
	},
	when_option_changes_update_store: {
		on: ['textChanged'],
		action: function(){
			this.parentVM.syncStore();
		}
	}
});

/**
 * Model definition for Checkbox Options
 * Checkbox Options are very similar to Options from radio buttons, but they can have multiple selected at the same time
 */
glu.defModel('RS.formbuilder.CheckOption', {
	/**
	 * Id of the option to keep things in sync, this is not persisted to the backend
	 */
	id: '',
	/**
	 * Text of the option displayed to the user
	 */
	text: '',
	displayText$: function(){
		return this.text.split('=')[0];
	},
	/**
	 * Whether the checkbox option is checked or not
	 */
	isChecked: false,
	/**
	 * Reactor to determine when the event for checking the checkbox is fired to forward that to the parentVM
	 */
	forward_events: {
		on: 'isCheckedChanged',
		action: function() {
			this.parentVM.checkControl(this);
		}
	},

	onTextChange_fireChangeInOptionStore: {
		on: 'textChanged',
		action: function() {
			this.parentVM.options.fireEvent('edited', this, this.parentList.indexOf(this));
		}
	},
	init: function() {
		this.set('id', Ext.id());
	},
	/**
	 * Button Handler that forwards the event to the parentVM to properly add another option
	 */
	addOption: function() {
		this.parentVM.addOption(this);
	},
	/**
	 * Button handler that forwards the event to the parentVM to properly remove this option
	 */
	removeOption: function() {
		this.parentVM.removeOption(this);
	}
});
/**
 * Model definition for the PreviewTable
 * The preview table is a table of the changes that will be made to the database on the server side.
 * Its a last stop for users to make sure that what they are creating is what they think they are really creating.
 */
glu.defModel('RS.formbuilder.PreviewTable', {
	/**
	 * The current serialized form to be sent to the server
	 */
	formConfig: {},
	/**
	 * The table name to display in the title of the grid
	 */
	tableName: '',
	/**
	 * The database fields configured in the form and on the server that are displayed to the user
	 */
	tableFields: {
		mtype: 'list',
		autoParent: true
	},

	wikiOption: 'none',

	/**
	 * Name of the wiki document to create with this form when the user saves it
	 */
	wikiName: '',
	wikiNameCreate: '',

	noWiki: false,
	createWiki: false,
	addToWiki: false,

	showCreateWiki: true,

	wikiNameIsEnabled$: function() {
		return this.addToWiki;
	},

	wikiNameCreateIsEnabled$: function() {
		return this.createWiki;
	},

	wikiNameCreateIsValid$: function() {
		if (this.createWiki) {
			if (!this.wikiNameCreate) return this.localize('wikiNameCreateBlankInvalid');
			if (this.wikiNames.findRecord('name', this.wikiNameCreate, 0, false, false, true)) return this.localize('wikiNameCreateInvalid', [this.wikiNameCreate]);
		}
		return true;
	},

	wikiNameIsValid$: function() {
		if (this.addToWiki && !this.wikiName) return this.localize('wikiNameInvalid');
		if (this.addToWiki && !this.wikiNames.findRecord('name', this.wikiName, 0, false, false, true)) return this.localize('wikiNameAddInvalid', [this.wikiNameCreate]);
		return true;
	},

	/**
	 * Store to keep track of the valid value list of wiki names in the system
	 */
	wikiNames: {
		mtype: 'store',
		model: 'RS.formbuilder.model.WikiName'
	},

	/**
	 * Store to display the tableFields to the user in a grid
	 */
	tableStore: {
		mtype: 'store',
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'tableFields'
		}],
		fields: ['columnName', 'action', 'actionState'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		},
		data: []
	},
	/**
	 * Flag to indicate that we are going to create a new table
	 */
	isNewTable: true,

	previewWarning: '',
	schemaUpdate: false,
	previewWarningIsVisible$: function() {
		return this.schemaUpdate
	},

	sizeExceeded: false,
	sizeWarning: '',
	sizeWarningIsVisible$: function() {
		return this.sizeExceeded
	},

	init: function() {
		this.set('previewWarning', this.localize('previewWarning'))
		this.set('sizeWarning', this.localize('sizeWarning'))
		this.wikiNames.load({
			scope: this,
			callback: this.compareWikiNames,
			params: {
				wikiName: this.formConfig.wikiName
			}
		})

		var name = this.formConfig.tableName;
		this.set('tableName', name);

		//get each column to be used for this form
		var fields = [],
			p = 0,
			t = 0,
			c = 0,
			f = 0;
		for (p = 0; p < this.formConfig.panels.length; p++) {
			for (t = 0; t < this.formConfig.panels[p].tabs.length; t++) {
				for (c = 0; c < this.formConfig.panels[p].tabs[t].columns.length; c++) {
					for (f = 0; f < this.formConfig.panels[p].tabs[t].columns[c].fields.length; f++) {
						fields.push(this.formConfig.panels[p].tabs[t].columns[c].fields[f]);
						if (this.formConfig.panels[p].tabs[t].columns[c].fields[f].uiType == 'SectionContainerField') {
							var section = this.formConfig.panels[p].tabs[t].columns[c].fields[f],
								columns = Ext.decode(section.sectionColumns);
							Ext.each(columns, function(column) {
								Ext.each(column.controls, function(control) {
									if (control) fields.push(control);
								})
							})
						}
					}
				}
			}
		}
		this.formConfig.panels[0].tabs[0].columns[0].fields;
		var newTableColumn = this.localize('noChangeTableColumn');
		Ext.each(fields, function(field) {
			if (field.sourceType == 'DB') {
				this.tableFields.add({
					mtype: 'viewmodel',
					fields: ['columnName', 'action', 'actionState', 'size'],
					//for serialization
					columnName: field.dbcolumn,
					action: newTableColumn,
					actionState: 0,
					size: Number(field.stringMaxLength) || 0
				});
			}
		}, this);

		Ext.Ajax.request({
			url: '/resolve/service/form/getTableData',
			params: {
				tableName: name
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					this.compareTable(response.data);
				} else clientVM.displayError(response.message);
			},
			failure: function(r){
				clientVM.displayFailure(r);
			}
		})
	},
	compareWikiNames: function() {
		if (this.wikiNames.getById(this.formConfig.wikiName)) {
			this.set('wikiName', this.formConfig.wikiName);
			this.set('addToWiki', true);
			this.set('showCreateWiki', false);
		} else if (this.formConfig.wikiName) {
			this.set('wikiNameCreate', this.formConfig.wikiName);
			this.set('createWiki', true);
			this.set('showCreateWiki', true);
		} else {
			this.set('noWiki', true);
		}
	},
	/**
	 * Compares the existing form table data with the server table data to determine new, change, or remove status for each column
	 * @param {Object} serverTable
	 */
	compareTable: function(serverTable) {
		this.set('isNewTable', serverTable.newTable);
		if (serverTable.newTable) {
			this.tableFields.foreach(function(tableField, index) {
				tableField.set('action', this.localize('newTableColumn'));
				tableField.set('actionState', 1);
				this.tableFields.fireEvent('edited', tableField.asObject(), index);
			}, this);
		} else {
			var columns = serverTable.fields || [];

			this.tableFields.foreach(function(tableField, index) {
				var found = false;
				Ext.each(columns, function(column) {
					if (column.ucolumnModelName == tableField.columnName) {
						found = true;
						return false;
					}
				});
				if (!found) {
					tableField.set('action', this.localize('newTableColumn'));
					tableField.set('actionState', 1);
					this.tableFields.fireEvent('edited', tableField.asObject(), index);
					this.set('schemaUpdate', true);
				}
			}, this);

			this.tableStore.sort('action', 'ASC');
		}
		//Go through each of the columns and see what the size is, sum it up, and if its > 15000 display size warning
		var sum = 0;
		this.tableFields.foreach(function(tableField) {
			sum += tableField.size;
		})
		if (sum > 15000) this.set('sizeExceeded', true)
	},
	/**
	 * Button Handler to close the window and cancel the saving of the form
	 */
	cancel: function() {
		this.doClose();
	},

	saveAndUpdateIsEnabled$: function() {
		return this.isValid;
	},
	saveAndCreateIsEnabled$: function() {
		return this.isValid;
	},
	/**
	 * Button Handler to save the form to the server
	 */
	saveAndUpdate: function() {
		this.parentVM.confirmSave(this);
		this.doClose();
	},
	/**
	 * Button Handler to save the form to the server
	 */
	saveAndCreate: function() {
		this.parentVM.confirmSave(this);
		this.doClose();
	},

	save: function() {
		this.parentVM.confirmSave(this)
		this.doClose()
	}
});
glu.defModel('RS.formbuilder.ReferenceTable', {
	mixins: ['DependencyControl'],
	fields: ['fieldLabel', 'referenceTable', 'referenceColumn', 'referenceColumns', 'allowReferenceTableAdd', 'allowReferenceTableRemove', 'allowReferenceTableView', 'referenceTableUrl', 'referenceTableViewPopup', 'referenceTableViewPopupWidth', 'referenceTableViewPopupHeight', 'referenceTableViewPopupTitle', 'rsType'],
	rsType: 'ReferenceTable',
	fieldLabel: '',
	referenceTable: '',
	referenceColumn: '',
	referenceColumns: '',
	allowReferenceTableAdd: true,
	allowReferenceTableRemove: true,
	allowReferenceTableView: true,
	referenceTableUrl: '',
	referenceTableViewPopup: false,
	referenceTableViewPopupWidth: 600,
	referenceTableViewPopupHeight: 600,
	referenceTableViewPopupTitle: '',
	referenceColumnStore: {
		mtype: 'store',
		fields: ['name', 'dbcolumnId', 'displayName', 'dbtype', 'uiType', 'integerMinValue', 'integerMaxValue', 'decimalMinValue', 'decimalMaxValue', 'stringMinLength', 'stringMaxLength', 'uiStringMaxLength', 'selectValues', 'choiceValues', 'referenceTable', 'referenceDisplayColumn'],
		data: [],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getcustomtablecolumns',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	}
});
glu.defModel('RS.formbuilder.SaveFormAs', {
	title: 'UntitledForm',
	name: 'UNTITLEDFORM',
	nameIsValid$: function() {
		if (this.name.length === 0) return this.localize('formNameEmptyInvalidText');
		if (this.name.length > 100) return this.localize('formNameTooLongInvalidText');
		if (/[^A-Z|_|0-9]/g.test(this.name)) return this.localize('formNameInvalidText');
		return this.parentVM.existingFormNames.findRecord('name', this.name, 0, false, false, true) ? this.localize('nameInvalidText', this.name) : true;
	},
	/**
	 * Validation formula to make sure that the title isn't a duplicate
	 */
	titleIsValid$: function() {
		if (this.title.length > 500) return this.localize('titleTooLongInvalidText');
		return true;
	},

	saveFormAs: function() {
		this.parentVM.reallySaveFormAs(this.name, this.title);
		this.doClose();
	},
	saveFormAsIsEnabled$: function() {
		return this.isValid;
	},
	cancel: function() {
		this.doClose();
	}
});
glu.defModel('RS.formbuilder.TabPanel', {
	tabs: {
		mtype: 'activatorlist',
		focusProperty: 'selectedTab',
		autoParent: true
	},
	tabStore: {
		mtype: 'store',
		fields: ['title'],
		data: [],
		mixins: [{
			type: 'liststoreadapter',
			attachTo: 'tabs'
		}]
	},
	selectedTab: 0,
	lastSelectedTab: 0,
	initialized: false,
	init: function() {
		//Add one tab to get the ball rolling
		this.addTab();
		this.set('selectedTab', this.tabs.getAt(0));
		this.set('lastSelectedTab', this.tabs.getAt(0));
	},
	when_tabsSelections_change_select_newTab: {
		on: ['tabsSelectionsChanged'],
		action: function() {
			if (this.tabsSelections.length == 0 && this.tabStore.getCount() > 0) this.set('tabsSelections', [this.tabStore.getAt(0)]);
			else this.set('selectedTab', this.tabs.getAt(this.tabStore.indexOf(this.tabsSelections[0])));
		}
	},
	addTab: function(config, index) {
		if (config == Ext.EventObject) {
			this.parentVM.addTab();
			return;
		}
		var tab = this.model({
			mtype: 'Tab'
		});
		tab.init();
		for (var key in config) {
			tab.set(key, config[key]);
		}
		return this.tabs.insert(index >= 0 ? index + 1 : this.tabs.length, tab);
	},
	tabsSelections: [],
	editTabIsEnabled$: function() {
		return this.tabsSelections.length == 1
	},
	editTab: function() {
		var tabSelection = this.tabsSelections[0],
			selectedTab, tabSelectionIndex = this.tabStore.indexOf(tabSelection);
		if (tabSelectionIndex > -1) {
			selectedTab = this.tabs.getAt(tabSelectionIndex);
			this.syncCurrentTab();
			this.set('selectedTab', selectedTab);
			this.open(Ext.applyIf({
				mtype: 'Tab'
			}, selectedTab.asObject()), 'detail');
		}
	},
	editRealTab: function(tabConfig) {
		var tabSelection = this.tabsSelections[0],
			tabSelectionIndex = this.tabStore.indexOf(tabSelection),
			selectedTab;
		if (tabSelectionIndex > -1) {
			selectedTab = this.tabs.getAt(tabSelectionIndex);
			selectedTab.loadData(tabConfig);
			selectedTab.parseDependencyControlConversionProperties(tabConfig.dependenciesJson);
			this.tabs.fireEvent('edited', selectedTab, tabSelectionIndex);
		}
	},
	removeTabIsEnabled$: function() {
		return this.tabsSelections.length == 1
	},
	removeTab: function() {
		var tabSelection = this.tabsSelections[0],
			selectedTab, tabSelectionIndex = this.tabStore.indexOf(tabSelection);
		if (tabSelectionIndex > -1) {
			selectedTab = this.tabs.getAt(tabSelectionIndex);
			this.syncCurrentTab();
			this.set('selectedTab', selectedTab);
			if (this.selectedTab.controls.length > 0) {
				this.parentVM.confirmTabRemoval();
			} else this.tabs.remove(this.selectedTab);
		}
	},
	when_selectedTab_changes_store_existing_controls_and_restore_tab_controls: {
		on: ['selectedTabChanged'],
		action: function() {
			//store controls to the lastSelectedTab
			if (this.lastSelectedTab) this.lastSelectedTab.controls = [];
			this.parentVM.controls.foreach(function(control) {
				if (control != this.parentVM.tabPanel) {
					this.lastSelectedTab.controls.push(control);
				}
			}, this)

			//Setup the last selected tab after we've stored off the information
			this.lastSelectedTab = this.selectedTab;

			//Then remove them from the form
			while (this.parentVM.controls.length > 1) {
				this.parentVM.controls.removeAt(1);
			}

			//And finally restore the newly selected tab controls
			if (this.selectedTab) {
				Ext.each(this.selectedTab.controls, function(control) {
					if (control) this.parentVM.controls.add(control)
				}, this);
			}
		}
	},
	syncCurrentTab: function() {
		if (this.selectedTab) {
			this.selectedTab.controls = [];
			this.parentVM.controls.foreach(function(control) {
				if (control && control != this.parentVM.tabPanel) {
					this.selectedTab.controls.push(control);
				}
			}, this);
		}
	},
	clearControls: function() {
		this.tabs.foreach(function(tab) {
			tab.controls = [];
		})
	}
});

glu.defModel('RS.formbuilder.Tab', {
	mixins: ['DependencyControl'],
	id: '',
	title: 'Untitled Tab',
	controls: [],
	fields: ['title'],
	init: function() {
		if (this.title === 'Untitled Tab') this.set('title', this.localize('untitledTab'));
	},
	// closable$: function() {
	// 	return this.parentVM.tabs.length > 1;
	// },
	asObject: function() {
		return {
			title: this.title,
			dependenciesJson: this.getDependencyControlConversionProperties().dependencies
		}
	},
	applyTab: function() {
		this.doClose();
		this.parentVM.editRealTab(this.asObject());
	},
	cancel: function() {
		this.doClose();
	}
});
glu.defModel('RS.formbuilder.Viewer', {

	embed: true,

	activate: function(screen, params) {
		var values = Ext.clone(this.defaultValues)
		Ext.apply(values, params)
		this.loadData(this.defaultValues) //restore all default values

		//now apply values from activate
		Ext.Object.each(values, function(key) {
			if (Ext.isDefined(this[key])) this.set(key, values[key])
		}, this)
		this.correctInputs()
		//don't need to reload the form because the form automatically gets loaded when the formId changes
		// if (this.form && this.form.rendered) this.form.loadForm()
	},

	form: null,

	fields: ['formName',
		'name',
		'view',
		'formId',
		'sys_id',
		'recordId',
		'queryString',
		'query',
		'params',
		'formdefinition'
	],

	defaultValues: {
		formdefinition: false,
		formName: '',
		name: '',
		view: '',
		formId: '',
		sys_id: '',
		recordId: '',
		queryString: '',
		query: '',
		params: ''
	},

	formName: '',
	name: '',
	view: '',

	formId: '',

	sys_id: '',
	recordId: '',

	queryString: '',
	query: '',

	params: '',
	formdefinition: false,

	init: function() {
		this.correctInputs()
	},

	correctInputs: function() {
		this.set('formName', this.formName || this.name || this.view)
		this.set('recordId', this.recordId || this.sys_id)
		this.set('queryString', this.queryString || this.query)
	},

	registerForm: function(form) {
		this.set('form', form)
	},

	setFormRecordId: function(id) {
		this.form.setRecordId(id)
	},

	resetForm: function() {
		this.form.resetForm()
	},

	formLoaded: function() {
		this.fireEvent('formLoaded', this)
	},
	//circulating back to update the record id of the viewer
	actionCompleted: function(recordId, actions) {
		Ext.each(actions, function(action) {
			if (action.crudAction == 'SUBMIT')
				this.set('recordId', recordId);
		}, this);
	}
})
/**
 * @author ryan.smith
 */
glu.defView('RS.formbuilder.AccessRightsPicker', {
	height: 300,
	width: 600,
	cls : 'rs-modal-popup',
	modal : true,
	padding : 15,
	title: '~~addAccessRights~~',
	layout: 'fit',
	items: [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		forceFit: true,
		name: 'roles',
		columns: [{
			header: '~~Name~~',
			dataIndex: 'name'
		}],
		margin : '0 0 10 0'
	}],	
	buttons: [{
		cls : 'rs-med-btn rs-btn-dark',
		name: 'addAccessRightsAction'
	},{
		cls : 'rs-med-btn rs-btn-light',
		name: 'cancel'
	}]
});
/**
 * View definition for an Action viewmodel.  Shows an operation (required) and if additional
 * information (like runbook or script) is required based on the operation.  Hides the comboboxes
 * if they aren't needed for the selected operation.
 */
glu.defView('RS.formbuilder.Action', {
	minHeight: 250,
	width: 600,
	padding : 15,
	cls : 'rs-modal-popup',	
	title: '@{actionTitle}',	
	modal: true,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	items: [{
		xtype: 'combobox',
		store: '@{operationStore}',
		displayField: 'name',
		valueField: 'value',
		editable: false,
		forceSelection: true,
		queryMode: 'local',
		name: 'operation'
	}, {
		xtype: 'textfield',
		name: 'EVENT_EVENTID'
	}, {
		xtype: 'combobox',
		store: '@{dbActionStore}',
		displayField: 'name',
		valueField: 'value',
		editable: false,
		forceSelection: true,
		queryMode: 'local',
		name: 'dbAction'
	}, {
		xtype: 'combobox',
		store: '@{sirActionStore}',
		displayField: 'name',
		valueField: 'value',
		editable: false,
		forceSelection: true,
		queryMode: 'local',
		name: 'sirAction'
	}, {
		xtype: 'combobox',
		store: '@{rootVM.mainForm.runbookStore}',
		displayField: 'name',
		valueField: 'value',
		forceSelection: false,
		typeAhead: true,
		queryMode: 'local',
		// name: 'RUNBOOK',
		fieldLabel: '~~RUNBOOK~~',
		hidden: '@{!RUNBOOKIsVisible}',
		listeners: {
			render: function(combo) {
				combo.setValue(combo._vm.RUNBOOK)
			},
			change: '@{runbookChange}'
		}
	}, {
		xtype: 'combobox',
		store: '@{rootVM.mainForm.scriptStore}',
		displayField: 'name',
		valueField: 'value',
		editable: false,
		forceSelection: true,
		queryMode: 'local',
		name: 'SCRIPT'
	}, {
		xtype: 'combobox',
		store: '@{rootVM.mainForm.actionTaskStore}',
		displayField: 'name',
		valueField: 'value',
		editable: true,
		forceSelection: true,
		queryMode: 'local',
		name: 'ACTIONTASK'
	}, {
		xtype: 'combobox',
		store: '@{problemIdStore}',
		displayField: 'name',
		valueField: 'value',
		editable: true,
		queryMode: 'local',
		// hideTrigger: true,
		name: 'PROBLEMID'
	},
	/* {
	xtype: 'textfield',
	name: 'EVENT_REFERENCE'
}, */
	{
		xtype: 'radiogroup',
		fieldLabel: '~~execution~~',
		name: 'scriptTimeout',
		columns: 1,
		items: [{
			layout: 'hbox',
			border: false,
			items: [{
				xtype: 'radio',
				name: 'timeout',
				boxLabel: '~~synchronous~~',
				inputValue: 0,
				value: '@{synchronous}'
			}, {
				xtype: 'numberfield',
				flex: 1,
				padding: '0 0 0 10',
				hideLabel: true,
				minValue: 0,
				name: 'waitSeconds'
			},
			{
				xtype : 'checkboxfield',
				padding: '0 0 0 10',
				boxLabel: '~~useDefault~~',
				disabled: '@{!useDefaultIsEnabled}',
				checked : '@{usingDefault}',
				tooltip: '~~useDefaultTooltip~~',
				submitValue: false,
				listeners: {
					render: function(checkbox) {
						if (checkbox.tooltip) {
							Ext.create('Ext.tip.ToolTip', {
								target: checkbox.getEl(),
								html: checkbox.tooltip
							})
						}
					},
					change: '@{updateUseDefault}'
				}
			}]
		}, {
			xtype: 'radio',
			name: 'timeout',
			boxLabel: '~~asynchronous~~',
			inputValue: -1,
			value: '@{asynchronous}'
		}]
	}, {
		name: 'redirectTarget',
		xtype: 'combobox',
		store: '@{redirectStore}',
		editable: true,
		displayField: 'name',
		valueField: 'name',
		queryMode: 'local'
	}, {
		name: 'redirectUrl',
		xtype: 'textfield'
	}, {
		name: 'mappingFrom',
		xtype: 'combobox',
		store: '@{mappingStore}',
		editable: true,
		displayField: 'name',
		valueField: 'name',
		queryMode: 'local'
	}, {
		name: 'mappingTo',
		xtype: 'combobox',
		store: '@{mappingStore}',
		editable: true,
		displayField: 'name',
		valueField: 'name',
		queryMode: 'local'
	}, {
		name: 'showConfirmation',
		xtype: 'checkbox'
	}, {
		name: 'messageBoxTitle',
		xtype: 'textfield'
	}, {
		name: 'messageBoxMessage',
		xtype: 'textfield'
	}, {
		name: 'messageBoxYesText',
		xtype: 'textfield'
	}, {
		name: 'messageBoxNoText',
		xtype: 'textfield'
	}, {
		name: 'reloadDelay',
		xtype: 'numberfield',
		minValue: 0
	}, {
		name: 'gotoTabName',
		xtype: 'textfield'
	}, {
		xtype: 'fieldcontainer',
		fieldLabel: '~~StopWhen~~',
		hidden: '@{!reloadDependencyIsVisible}',
		layout: 'hbox',
		items: [{
			xtype: 'combobox',
			name: 'reloadTarget',
			store: '@{dependencyTargetStore}',
			displayField: 'name',
			valueField: 'value',
			forceSelection: true,
			editable: false,
			selectOnTab: true,
			triggerAction: 'all',
			queryMode: 'local',
			listConfig: {
				minWidth: 150
			},
			listeners: {
				expand: function(field) {
					field.store.load()
				}
			},
			hideLabel: true,
			flex: 1
		}, {
			xtype: 'combobox',
			name: 'reloadCondition',
			store: '@{dependencyConditionStore}',
			displayField: 'name',
			valueField: 'value',
			forceSelection: true,
			editable: false,
			selectOnTab: true,
			triggerAction: 'all',
			queryMode: 'local',
			listConfig: {
				minWidth: 150
			},
			listeners: {
				expand: function(field) {
					field.store.load()
				}
			},
			hideLabel: true,
			padding: '0 0 0 5',
			flex: 1
		}, {
			xtype: 'textfield',
			name: 'reloadValue',
			hideLabel: true,
			padding: '0 0 0 5',
			flex: 1
		}]
	}],
	buttons: [{
		name: 'addAction',
		hidden: '@{!isNew}',
		cls : 'rs-med-btn rs-btn-dark'
	}, {
		name: 'applyAction',
		hidden: '@{isNew}',
		cls : 'rs-med-btn rs-btn-dark'
	}, {
		name: 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}],
});
/**
 * View definition for a Button Control.  Renders a button in the buttons bar of the form
 */
glu.defView('RS.formbuilder.ButtonControl', {
	xtype: 'autobutton',
	text: '@{operation}'
});

glu.defView('RS.formbuilder.ButtonControl', 'detail', {
	title: '~~editButtonTitle~~',
	height: 400,
	width: 600,
	padding : 15,
	layout: 'fit',
	border: false,
	cls : 'rs-modal-popup',
	modal: true,
	items: [{
		xtype: 'form',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		border: false,	
		items: [{
			xtype: 'textfield',
			name: 'operation',
			fieldLabel: '~~name~~'
		}, {
			xtype: 'panel',
			layout: 'hbox',
			padding: '5 0 5 0',
			hidden: '@{!..editButton}',
			items: [{
				xtype: 'combo',
				displayField: 'font',
				valueField: 'font',
				fieldLabel: '~~font~~',
				editable: false,
				width: 270,
				value: '@{font}',
				store: '@{fontStore}',
				listeners: {
					select: function(combo, records) {
						var r = records[0],
							font = r.get('font');

						this.fireEvent('updatebuttonfont', this, font);
					},
					updatebuttonfont: '@{updateButtonFont}'
				}
			}, {
				xtype: 'combo',
				displayField: 'fontsize',
				valueField: 'fontsize',
				width: 65,
				value: '@{fontSize}',
				padding: '0 0 0 10',
				editable: false,
				displayTpl: Ext.create('Ext.XTemplate',
					'<tpl for=".">',
					'{fontsize}pt',
					'</tpl>'
				),
				store: '@{fontSizeStore}',
				listeners: {
					select: function(combo, records) {
						var r = records[0],
							fontSize = parseInt(r.get('fontsize'));

						this.fireEvent('updatebuttonfontsize', this, fontSize);
					},
					updatebuttonfontsize: '@{updateButtonFontSize}'
				}
			}, {
				xtype: 'toolbar',
				itemId: 'buttonColorId',
				items: [{
					iconCls: '@{fontColorIcon}',
					width: 45,
					tooltip: '~~fgColorTooltip~~',
					menu: {
						xtype: 'resolvecolormenu',
						handler: function(cm, color) {
							if (typeof(color) == "string") {
								this.up('#buttonColorId').fireEvent('updatebuttonfontcolor', this, color);
								this.clear();
							}
						}
					}
				}, {
					iconCls: '@{fillColorIcon}',
					width: 45,
					tooltip: '~~bgColorTooltip~~',
					menu: {
						xtype: 'resolvecolormenu',
						handler: function(cm, color) {
							if (typeof(color) == "string") {
								this.up('#buttonColorId').fireEvent('updatebuttonbackgroundcolor', this, color);
								this.clear();
							}
						}
					}
				}],
				listeners: {
					updatebuttonfontcolor: '@{updateButtonFontColor}',
					updatebuttonbackgroundcolor: '@{updateButtonBackgroundColor}',
				}
			}]
		}, {
			xtype: 'checkbox',
			name: 'customTableDisplay',
			hideLabel: true,
			boxLabel: '~~customTableDisplay~~',
			padding: '-2 0 0 104',
			tooltip: '~~customTableDisplayTooltip~~',
			listeners: {
				render: function(field) {
					Ext.create('Ext.tip.ToolTip', {
						target: field.getEl(),
						html: field.tooltip
					})
				}
			}
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',		
			title: '~~dependencies~~',
			name: 'dependencies',
			flex: 1,
			store: '@{dependenciesStore}',
			forceFit: true,
			selModel: {
				selType: 'rowmodel'
			},
			/*plugins: [{
		ptype: 'cellediting',
		clicksToEdit: 1
	}],*/
			dockedItems :[{
				xtype : 'toolbar',
				cls : 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items : ['addDependency','editDependency','removeDependency']
			}] ,
			columns: [{
				dataIndex: 'actionDisplay',
				header: '~~dependencyAction~~'
				/*editor: {
			xtype: 'combobox',
			store: '@{dependencyActionStore}',
			displayField: 'name',
			valueField: 'name',
			forceSelection: true,
			editable: false,
			selectOnTab: true,
			triggerAction: 'all',
			queryMode: 'local'
		}*/
			}, {
				dataIndex: 'target',
				header: '~~target~~'
				/*editor: {
			xtype: 'combobox',
			store: '@{dependencyTargetStore}',
			displayField: 'name',
			valueField: 'value',
			forceSelection: true,
			editable: false,
			selectOnTab: true,
			triggerAction: 'all',
			queryMode: 'local',
			listeners: {
				expand: function(field) {
					field.store.load();
				}
			}
		}*/
			}, {
				dataIndex: 'conditionDisplay',
				header: '~~condition~~'
				/*editor: {
			xtype: 'combobox',
			store: '@{dependencyConditionStore}',
			displayField: 'name',
			valueField: 'name',
			forceSelection: true,
			editable: false,
			selectOnTab: true,
			triggerAction: 'all',
			queryMode: 'local',
			listConfig: {
				minWidth: 150
			},
			listeners: {
				expand: function(field) {
					field.store.load()
				}
			}
		}*/
			}, {
				dataIndex: 'value',
				header: '~~value~~',
				// editor: {},
				renderer: function(value) {
					return Ext.util.Format.htmlEncode(value);
				}
			}],
			listeners: {
				itemdblclick: '@{depDoubleClick}'
			}
		}]
	}],
	buttons: [{
		name: 'applyButton',
		cls : 'rs-med-btn rs-btn-dark'
	}, {
		name: 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});

glu.regAdapter('autobutton', {
	beforeCollect: (function() {
		return function(config, dataModel) {
			var xtype = 'button';
			switch (dataModel.type) {
				case 'separator':
					xtype = 'tbseparator';
					break;
				case 'spacer':
					xtype = 'tbspacer';
					break;
				case 'rightJustify':
					xtype = 'tbfill';
					break;
			}
			config.xtype = xtype;
		}
	})()

});
glu.defView('RS.formbuilder.Dependency', {
	title: '@{title}',
	modal: true,
	width: 600,
	height: 250,
	padding : 15,
	cls : 'rs-modal-popup',	
	border: false,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	items: [{
		xtype: 'combobox',
		name: 'action',
		store: '@{dependencyActionStore}',
		displayField: 'name',
		valueField: 'value',
		forceSelection: true,
		editable: false,
		selectOnTab: true,
		triggerAction: 'all',
		queryMode: 'local'
	}, {
		xtype: 'colorpickercombo',
		name: 'color'
	}, {
		xtype: 'combobox',
		name: 'target',
		store: '@{dependencyTargetStore}',
		displayField: 'name',
		valueField: 'value',
		forceSelection: true,
		editable: false,
		selectOnTab: true,
		triggerAction: 'all',
		queryMode: 'local'
	}, {
		xtype: 'combobox',
		name: 'condition',
		store: '@{dependencyConditionStore}',
		displayField: 'name',
		valueField: 'value',
		forceSelection: true,
		editable: false,
		selectOnTab: true,
		triggerAction: 'all',
		queryMode: 'local',
		listConfig: {
			minWidth: 150
		},
		listeners: {
			expand: function(field) {
				field.store.load()
			}
		}
	}, {
		xtype: 'textfield',
		name: 'stringValue'
	}/*, {
		xtype: 'numberfield',
		name: 'numberValue'
	}, {
		xtype: 'datefield',
		name: 'dateValue'
	}, {
		xtype: 'textfield',
		name: 'booleanValue'
	}*/],
	buttons: [{
		cls : 'rs-med-btn rs-btn-dark',
		name: 'applyDependency'
	}, {
		cls : 'rs-med-btn rs-btn-light',
		name: 'cancel'
	}]	
});
/**
 * View definition for a Form viewmodel.  Displays the preview of a Form with each of the controls
 * If no controls are added, then a card flips to show the user a helpful text indicating that they should add controls to the form
 */
glu.defView('RS.formbuilder.Form', {
	border: false,
	title: {
		value: '@{formTitle}',
		trigger: 'dblclick',
		field: {
			xtype: 'textfield'
		}
	},
	layout: 'card',
	activeItem: '@{showNoFieldsCard}',
	plugins: [{
		ptype: 'panelfielddragzone'
	}, {
		ptype: 'panelfielddroptarget'
	}],
	buttonAlign: 'left',
	buttons: '@{formButtons}',
	items: [{
		border: false,
		bodyPadding: '10px',
		html: '<div class="notice"><h2>Please add a field.</h2><p>This is a <strong>live preview</strong> of your form. Currently, <strong>you don\'t have any fields</strong>. Use the buttons on the right to create inputs for your form. Click on the fields to change their properties.</p></div>'
	}, {
		xtype: 'form',
		border: false,
		autoScroll: true,
		activeItem: '@{detailControl}',
		items: '@{controls}',
		plugins: [{
			ptype: 'activeitemfieldselector'
		}]
	}],
	cls: 'formControlsContainer',
	listeners: {
		itemDropped: '@{dropControl}',
		fieldlabeledited: '@{editFieldLabel}',
		titleedited: '@{editTitle}'
	}
});

/**
 * View (Detail) definition for a Form viewmodel.  This displays the options for the form like buttons, hidden fields, and global settings for the form
 */
glu.defView('RS.formbuilder.Form', 'detail', {
	autoScroll: true,
	bodyPadding: '10px',
	border: false,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		labelWidth: 125
	},
	items: [{
			xtype: 'textfield',
			allowBlank: false,
			name: 'name',
			fieldStyle: 'text-transform:uppercase',
			enableKeyEvents: true
		}, {
			xtype: 'textfield',
			name: 'title',
			listeners: {
				focus: '@{focusTitle}',
				blur: '@{blurTitle}'
			}
		}
		, {
			xtype: 'combobox',
			name: 'labelAlign',
			editable: false,
			forceSelection: true,
			queryMode: 'local',
			displayField: 'text',
			valueField: 'value',
			store: '@{labelAlignOptionsList}'
		}, {
			xtype: 'combo',
			name: 'dbTable',
			editable: '@{isAdmin}',
			forceSelection: '@{!isAdmin}',
			queryMode: 'local',
			displayField: 'uname',
			typeAhead: false,
			valueField: 'umodelName',
			store: '@{customTablesList}',
			listeners: {
				valuechanged: function() {
					var actual = this.getActionEl().dom.value;
					if (this.getValue() != this.getActionEl().dom.value)
						this.setValue(actual);
				}
			}
		}, {
			xtype: 'textfield',
			name: 'formWindowTitle'
		}, {
			xtype: 'fieldset',
			padding : '0 10 10 10',
			title: '~~useTabs~~',
			checkboxToggle: true,
			collapsed: '@{!useTabs}',
			items: [{
				xtype: 'checkbox',
				name: 'isWizard',
				boxLabel : '~~isWizard~~',
				hideLabel : true
			}, {
				xtype: 'grid',
				cls : 'rs-grid-dark',
				store: '@{tabPanel.tabStore}',
				title: '~~tabs~~',
				forceFit: true,
				name: 'tabs',
				dockedItems : [{
					xtype : 'toolbar',
					cls : 'rs-dockedtoolbar',
					defaults : {
						cls : 'rs-small-btn rs-btn-light'
					},
					items :  ['addTab','editTab','removeTab']
				}],
				selModel: {
					selType: 'cellmodel'
				},
				plugins: [{
					ptype: 'cellediting',
					clicksToEdit: 2
				}],
				columns: [{
					header: '~~Name~~',
					dataIndex: 'title',
					hideable: false,
					sortable: false,
					editor: {
						allowBlank: false,
						maxLength: 100
					}
				}]
			}]
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			title: '~~HiddenFields~~',
			name: 'hiddenFields',
			forceFit: true,
			padding: '0 0 10 0',
			dockedtoolbar : [{
				xtype : 'toolbar',
				cls : 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items : ['addHiddenField','removeHiddenField']
			}],
			selModel: {
				selType: 'cellmodel'
			},
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 2
			}],
			columns: [{
				header: '~~Name~~',
				dataIndex: 'name',
				hideable: false,
				editor: {
					allowBlank: false
				}
			}, {
				header: '~~Value~~',
				dataIndex: 'value',
				hideable: false,
				editor: {
					allowBlank: false
				}
			}]
		}, {
			title: '~~Buttons~~',
			dockedItems : [{
				xtype : 'toolbar',
				cls : 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items : [{
					name: 'addButton',
					xtype: 'splitbutton',
					menu: {
						xtype: 'menu',
						items: ['addNewButton','addSeparator','addSpacer','addRightJustify']
					}
				},'editButton','removeButton',{
					xtype : 'button',
					text : 'Action',
					menu : {
						xtype : 'menu',
						items : ['addAction','editAction','removeAction']
					}
				}]
			}],
			xtype: 'treepanel',
			cls : 'rs-grid-dark',
			padding: '0 0 10 0',
			useArrows: true,
			rootVisible: false,
			selected: '@{formButtonsSelections}',
			store: '@{treeStore}',
			scroll: false,
			viewConfig: {
				plugins: {
					ptype: 'treeviewdragdrop'
				},
				listeners: {
					beforeDrop: function(node, data, overModel, dropPosition, dropHandler, eOpts) {
						//Can only drop a button to another button position
						var returnVal = false,
							eventName;
						if (overModel.data.type != 'action') {
							if (dropPosition == 'append' && data.records[0].data.type == 'action') {
								eventName = 'dropAction';
								returnVal = true;
							} //appending an action to a button is ok
							if (data.records[0].data.type != 'action' && (dropPosition == 'before' || dropPosition == 'after')) {
								eventName = 'dropButton';
								returnVal = true;
							} //moving a button before or after another button is ok
						}
						if (data.records[0].data.type == 'action' && overModel.data.type == 'action' && (dropPosition == 'before' || dropPosition == 'after')) {
							eventName = 'dropAction';
							returnVal = true;
						} //action before or after another action is ok
						if (eventName == 'dropAction') this.ownerCt.fireEvent(eventName, this.ownerCt, node, data, overModel, dropPosition, eOpts);
						return returnVal;
					},
					drop: function(node, data, overModel, dropPosition, eOpts) {
						var eventName = 'dropButton';

						if (overModel.data.type != 'action') {
							if (dropPosition == 'append' && data.records[0].data.type == 'action') eventName = 'dropAction'; //appending an action to a button is ok
							if (data.records[0].data.type != 'action' && (dropPosition == 'before' || dropPosition == 'after')) eventName = 'dropButton'; //moving a button before or after another button is ok
						}
						if (data.records[0].data.type == 'action' && overModel.data.type == 'action' && (dropPosition == 'before' || dropPosition == 'after')) eventName = 'dropAction'; //action before or after another action is ok
						if (eventName == 'dropButton') this.ownerCt.fireEvent(eventName, this.ownerCt, node, data, overModel, dropPosition, eOpts);
					}
				}
			},
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}],
			columns: [{
				xtype: 'treecolumn',
				//this is so we know which column will show the tree
				header: '~~operation~~',
				flex: 1,
				dataIndex: 'operationName',
				sortable: false,
				editor: {}
			}, {
				header: '~~value~~',
				flex: 1,
				dataIndex: 'value',
				sortable: false,
				editor: {}
			}],
			listeners: {
				beforeEdit: function(editor, e, eOpts) {
					if (e.record.data.type === 'button' && e.colIdx === 1) return false;
					if (e.record.data.type === 'action') return false;
					if (e.record.data.type === 'separator') return false;
					if (e.record.data.type === 'spacer') return false;
					if (e.record.data.type === 'rightJustify') return false;
				},
				dropButton: '@{dropButton}',
				dropAction: '@{dropAction}',
				itemappend: function() {
					this.expandAll();
				},
				itemdblclick: '@{actionItemDoubleClick}'
			}
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			padding: '0 0 10 0',
			title: '~~AccessRights~~',
			name: 'accessRights',
			dockedItems : [{
				xtype : 'toolbar',
				cls : 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items :  ['addAccessRight','removeAccessRight']
			}],
			selModel: {
				selType: 'cellmodel'
			},
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 2
			}],
			columns: [{
					header: '~~Name~~',
					dataIndex: 'name',
					hideable: false,
					flex: 1,
					editor: {
						allowBlank: false
					}
				}, {
					xtype: 'checkcolumn',
					stopSelection: false,
					header: '~~adminRight~~',
					dataIndex: 'admin',
					hideable: false,
					width: 80
				},
				{
					xtype: 'checkcolumn',
					stopSelection: false,
					header: '~~viewRight~~',
					dataIndex: 'view',
					hideable: false,
					width: 80
				}
			]
		}
	]
});
Ext.define('RS.formbuilder.Control.Panel', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.formfieldcontrolpanel',
	config: {
		unsupportedField: false
	},
	setUnsupportedField: function(unsupported) {
		if (unsupported) {
			this.addCls('unsupported_formfieldcontrolpanel');
		} else {
			this.removeCls('unsupported_formfieldcontrolpanel');
		}
	}
});

/**
 * Factory definition for Form Controls to render as a preview in the form
 * Adds a remove button and overall wrapper template to each control so that we can drag-n-drop the controls as well as remove them
 */
RS.formbuilder.views.FormControlLayoutFactory = function(subView) {
	var config = {}

	if (Ext.Array.indexOf(subView.vm.mixins, 'FormControl') >= 0) {
		glu.applyIf(config, {
			flex: 1,
			labelWidth: 125,
			fieldLabel: {
				value: '@{fieldLabel}',
				trigger: 'dblclick',
				field: {
					xtype: 'textfield'								
				}				
			},
			readOnly: '@{readOnly}',
			disabled: '@{hidden}',
			labelSeparator: '@{mandatoryLabelSeparator}',
			value: '@{defaultValue}',
			labelAlign: '@{labelAlign}'			
		});
	}

	if (Ext.Array.indexOf(subView.vm.mixins, 'SizeControl') >= 0) {
		glu.applyIf(config, {
			height: '@{actualHeight}',
			width: '@{width}'
		});
	}	

	glu.applyIf(subView, config);
	var items = [subView, {
		xtype: 'button',
		icon: '/resolve/images/remove.png',
		hidden: true,
		name: 'removeControl'
	}];

	if (Ext.Array.indexOf(subView.vm.mixins, 'ButtonControl') >= 0) {
		items.splice(1, 0, {
			xtype: 'displayfield',
			flex: 1
		});
	}

	return {
		xtype: 'formfieldcontrolpanel',
		cls: subView.vm.isInSection ? 'formControlSectionContainer' : 'formControlContainer',
		unsupportedField: '@{unsupportedField}',
		bodyCls: 'formControlContainerBody',
		padding: '5px',
		layout: {
			type: 'hbox',
			align: 'middle'
		},
		items: items,
		border: false
	}
};

/**
 * Template definition for the FormControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.formControlTemplate = [{
	xtype: 'textfield',
	name: 'fieldLabel',
	listeners: {
		focus: '@{focusFieldLabel}',
		blur: '@{blurFieldLabel}'
	}	
}, {
	xtype: 'combobox',
	name: 'labelAlign',
	editable: false,
	forceSelection: true,
	queryMode: 'local',
	displayField: 'text',
	valueField: 'value',
	store: '@{labelAlignOptionsList}'
}, {
	xtype: 'checkbox',
	name: 'readOnly',
	hideLabel : true,
	boxLabel : '~~readOnly~~',
	margin : '0 0 0 130'
}, {
	xtype: 'checkbox',
	name: 'hidden',
	hideLabel : true,
	boxLabel : '~~hidden~~',
	margin : '0 0 0 130'
}];

/**
 * Template definition for the SizeControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.formControlSizeTemplate = [{
	xtype: 'fieldset',
	padding : '0 10 5 10',
	defaults: {
		labelWidth: 115
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	checkboxToggle: true,
	title: '~~Size~~',
	collapsed: '@{collapsed}',
	collapsible: true,
	items: [{
		xtype: 'numberfield',
		minValue: 125,
		maxValue: 9999999,
		minValue: 0,
		name: 'width'
	}, {
		xtype: 'numberfield',
		minValue: 22,
		maxValue: 9999999,
		minValue: 0,
		name: 'height'
	}]
}];

/**
 * Template definition for the RequireableControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.requireableControlTemplate = [{
	xtype: 'checkbox',
	name: 'mandatory',
	hideLabel : true,
	boxLabel : '~~mandatory~~',
	margin : '0 0 0 130'
}];

RS.formbuilder.tooltipControlTemplate = [{
	xtype: 'textfield',
	name: 'tooltip',
	maxLength: 500
}];

/**
 * Template definition for the RequireableControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.encryptableControlTemplate = [{
	xtype: 'checkbox',
	name: 'encrypted',
	hideLabel : true,
	boxLabel : '~~encrypted~~',
	margin : '0 0 0 130'
}];

/**
 * Template definition for the DefaultValueControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.defaultValueTemplate = [{
	xtype: 'textfield',
	name: 'defaultValue',
	maxLength: 1000
}];

RS.formbuilder.defaultValueBooleanTemplate = [{
	xtype: 'checkbox',
	name: 'defaultValue',
	hideLabel : true,
	boxLabel : '~~defaultValue~~',
	margin : '0 0 0 130'
}];

/**
 * Template definition for the StringControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.stringRangeTemplate = [{
	xtype: 'fieldset',
	defaults: {
		labelWidth: 115
	},
	title: '~~dbTypeParams~~',
	hidden: '@{!columnSizeIsVisible}',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'combobox',
		name: 'columnSize',
		store: '@{columnSizeStore}',
		readOnly: '@{columnSizeIsReadOnly}',
		displayField: 'name',
		valueField: 'value',
		editable: false,
		forceSelection: true
	}]
}, {
	xtype: 'fieldset',
	padding : '0 10 5 10',
	defaults: {
		labelWidth: 115
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	title: '~~Range~~',
	items: [{
		xtype: 'numberfield',
		name: 'minLength'
	}, {
		xtype: 'numberfield',
		name: 'maxLength'
	}]
}];
/**
 * Template definition for the NumberControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.numberRangeTemplate = [{
	xtype: 'numberfield',
	allowDecimals: false,
	name: 'defaultValue'
}, {
	xtype: 'fieldset',
	padding : '0 10 5 10',
	defaults: {
		labelWidth: 115
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	title: '~~Range~~',
	items: [{
		xtype: 'numberfield',
		allowDecimals: false,
		name: 'minValue'
	}, {
		xtype: 'numberfield',
		allowDecimals: false,
		name: 'maxValue',
		maxValue: '@{allowedMaxValue}'
	}]
}];

/**
 * Template definition for the DecimalControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.decimalRangeTemplate = [{
	xtype: 'numberfield',
	allowDecimals: true,
	decimalPrecision: 5,
	name: 'defaultValue'
}, {
	xtype: 'fieldset',
	defaults: {
		labelWidth: 117
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	title: '~~Range~~',
	items: [{
		xtype: 'numberfield',
		allowDecimals: true,
		name: 'minValue'
	}, {
		xtype: 'numberfield',
		allowDecimals: true,
		name: 'maxValue',
		maxValue: '@{allowedMaxValue}'
	}]
}];
/**
 * Template definition for the ChoiceControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.choiceRadioTemplate = [{
	xtype: 'numberfield',
	name: 'columns',
	maxValue: 10,
	minValue: 0
}, {
	xtype: 'radiogroup',
	fieldLabel: '~~optionType~~',
	labelWidth : 125,
	hidden: '@{!displayOptionsSelections}',
	items: [{
		xtype: 'radio',
		name: 'optionType',
		value: '@{simple}',
		boxLabel: '~~simple~~'
	}, {
		xtype: 'radio',
		name: 'optionType',
		value: '@{advanced}',
		boxLabel: '~~advanced~~'
	}, {
		xtype: 'radio',
		name: 'optionType',
		value: '@{custom}',
		boxLabel: '~~custom~~'
	}]
}, {
	layout: 'card',
	border: false,
	activeItem: '@{activeOption}',
	items: [{
		xtype: 'fieldset',		
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		title: '~~Options~~',
		items: '@{options}',
		itemTemplate: {
			border: false,
			layout: 'hbox',
			bodyPadding: '3px',
			defaults : {
				margin : '0 5 0 0'
			},
			items: [{
				xtype: 'radio',
				name: '@{..tempId}',
				inputValue: '@{id}',
				checked: '@{isSelected}',			
			}, {
				flex: 1,
				xtype: 'textfield',
				value: '@{text}',
				maxLength: 60
			}, {
				xtype: 'button',
				name: 'addOption',
				cls : 'rs-small-btn rs-btn-dark',
			}, {
				xtype: 'button',
				margin :0,
				hidden: '@{!..optionsGreaterThanOne}',
				name: 'removeOption',
				cls : 'rs-small-btn rs-btn-light',
			}]
		}
	}, {
		xtype: 'panel',
		border: false,
		layout: 'anchor',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaults : {
			labelWidth : 125
		},
		items: [{
			xtype: 'combobox',
			name: 'dropdownChoiceTable',
			queryMode: 'local',
			store: '@{..customTablesList}',
			displayField: 'uname',
			valueField: 'umodelName'
		}, {
			xtype: 'combobox',
			name: 'displayField',
			store: '@{tableColumnStore}',
			queryMode: 'local',
			displayField: 'displayName',
			valueField: 'name'
		}, {
			xtype: 'combobox',
			name: 'valueField',
			store: '@{tableColumnStore}',
			queryMode: 'local',
			displayField: 'displayName',
			valueField: 'name'
		}, {
			xtype: 'clearablecombobox',
			name: 'where',
			store: '@{tableColumnStore}',
			queryMode: 'local',
			displayField: 'displayName',
			valueField: 'name'
		}, {
			xtype: 'clearablecombobox',
			name: 'parentField',
			store: '@{fieldStore}',
			queryMode: 'local',
			displayField: 'name',
			valueField: 'value',
			listeners: {
				expand: function(combo) {
					combo.getStore().load();
				}
			}
		}]
	}, {
		xtype: 'textarea',
		labelWidth : 125,
		name: 'sqlQuery'
	}]
}, {
	layout: {
		type: 'hbox',
		pack: 'center',
		align: 'middle'
	},
	padding: '0 0 10 0',
	border: false,
	items: [
		{
			xtype: 'button',
			name: 'importOptions',
			cls : 'rs-small-btn rs-btn-dark'
		}
	]
}];

/**
 * Template definition for the MultiChoiceControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.choiceCheckTemplate = [{
	xtype: 'numberfield',
	name: 'columns',
	maxValue: 10
}, {
	xtype: 'fieldset',	
	padding : '0 10 5 10',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	title: '~~Options~~',
	items: '@{options}',
	itemTemplate: {
		border: false,
		layout: 'hbox',
		bodyPadding: '3px',
		defaults: {		
			margin: '0 5 0 0'
		},
		items: [{
			xtype: 'checkbox',
			value: '@{isChecked}'
		}, {
			flex: 1,
			xtype: 'textfield',
			value: '@{text}',
			maxLength: 60			
		}, {
			xtype: 'button',
			name: 'addOption',
			cls : 'rs-small-btn rs-btn-dark'		
		}, {
			xtype: 'button',
			margin : 0,
			hidden: '@{!..optionsGreaterThanOne}',
			cls : 'rs-small-btn rs-btn-light',
			name: 'removeOption'
		}]
	}
}, {
	layout: {
		type: 'hbox',
		pack: 'center',
		align: 'middle'
	},
	border: false,
	items: [{
		xtype: 'button',
		name: 'importOptions',
		cls : 'rs-small-btn rs-btn-dark',
	}]
}];
/**
 * Template definition for the DateControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.dateTemplate = [{
	xtype: 'datefield',
	name: 'defaultValue'
}];
/**
 * Template definition for the TimeControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.timeTemplate = [{
	xtype: 'timefield',
	name: 'defaultValue',
	store: ''
}];
/**
 * Template definition for the DateTimeControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.dateTimeTemplate = [{
	xtype: 'datefield',
	name: 'date'
}, {
	xtype: 'timefield',
	name: 'time',
	store: ''
}];
/**
 * Template definition for the ButtonControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.buttonTemplate = [{
	xtype: 'textfield',
	fieldLabel: '~~text~~',
	name: 'operation'
}, {
	xtype: 'grid',
	forceFit: true,
	store: '@{actionsStore}',
	name: 'actions',
	tbar: [{
		name: 'addAction'
	}, {
		name: 'editAction'
	}, {
		name: 'removeAction'
	}],
	columns: [{
		dataIndex: 'operationName',
		header: '~~operation~~'
	}, {
		dataIndex: 'value',
		header: '~~value~~'
	}]
}]
/**
 * Template definition for the SequenceControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.sequenceTemplate = [{
	xtype: 'textfield',
	name: 'prefix'
}]
/**
 * Template definition for the DatabaseControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.databaseTemplate = [{
		xtype: 'radiogroup',
		fieldLabel: '~~type~~',
		items: [{
			xtype: 'radio',
			name: 'inputType',
			inputValue: 'INPUT',
			boxLabel: '~~input~~',
			value: '@{inputTypeSelected}',
			disabled: '@{!inputTypeIsEnabled}'
		}, {
			xtype: 'radio',
			name: 'inputType',
			inputValue: 'DB',
			boxLabel: '~~db~~',
			value: '@{dbTypeSelected}',
			disabled: '@{!dbTypeIsEnabled}'
		}]
	}, {
		xtype: 'combo',
		name: 'column',
		editable: '@{columnIsEditable}',
		queryMode: 'local',
		displayField: 'name',
		valueField: 'name',
		keyDelay: 1,
		store: '@{..columnStore}',
		hidden: '@{hideColumnCombobox}',
		displayTpl: '<tpl for=".">{[typeof values === "string" ? values : values["name"] === "newColumn" ? "u_" : ((values["name"] ? values["name"] : values["displayName"]))]}</tpl>',
		tpl: new Ext.XTemplate('<ul><tpl for=".">', '<li role="option" class="x-boundlist-item" <tpl if="this.showDisabled(values)">style="color:#888888" </tpl> >', '{[values["name"] === "newColumn" ? "**" + values["displayName"] + "**" : (values["name"] + " [" + values["displayName"] + "]")]}</li>', '</tpl></ul>', {
			showDisabled: function(values) {
				if (values.name === 'newColumn') return false;
				// console.log('ui type is ' + values.uiType + ' and model type is ' + values.modelType );
				return values.modelType().indexOf(values.uiType) == -1;
			}
		}),
		listeners: {
			blur: '@{blurDatabaseFieldName}'
		}
	}, {
		xtype: 'textfield',
		name: 'inputName',
		keyDelay: 1,
		hidden: '@{!hideColumnCombobox}'
	}
];
/**
 * Template definition for the UserPickerControl DETAIL to render and edit the field's properties
 */
RS.formbuilder.groupPickerTemplate = [{
	xtype: 'checkbox',
	name: 'allowAssignToMe',
	boxLabel : '~~allowAssignToMe~~',
	margin : '0 0 0 130',
	hideLabel : true
}, {
	xtype: 'checkbox',
	name: 'autoAssignToMe',
	boxLabel : '~~autoAssignToMe~~',
	margin : '0 0 0 130',
	hideLabel : true
}, {
	xtype: 'fieldset',
	defaults: {
		labelWidth: 115
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	title: '~~userFilter~~',
	items: [{
		xtype: 'combo',
		editable: false,
		forceSelection: true,
		queryMode: 'local',
		displayField: 'name',
		valueField: 'value',
		multiSelect: true,
		name: 'groups',
		store: '@{groupStore}'
	}, {
		xtype: 'combo',
		editable: false,
		forceSelection: true,
		queryMode: 'local',
		displayField: 'name',
		valueField: 'value',
		multiSelect: true,
		name: 'teams',
		store: '@{teamStore}'
	}, {
		xtype: 'checkbox',
		name: 'recurseThroughTeams',
		hideLabel: true,
		margin: '0 0 0 120',
		boxLabel: '~~recurseThroughTeams~~'
	}]
}, {
	xtype: 'fieldset',
	padding : '0 10 5 10',
	defaults: {
		labelWidth: 115
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	title: '~~teamFilter~~',
	items: [{
			xtype: 'combo',
			editable: false,
			forceSelection: true,
			queryMode: 'local',
			displayField: 'name',
			valueField: 'value',
			multiSelect: true,
			name: 'teamList',
			store: '@{teamStore}'
		}
	]
}];

RS.formbuilder.teamPickerTemplate = [{
	xtype: 'fieldset',
	defaults: {
		labelWidth: 115
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	title: '~~teamFilter~~',
	items: [{
		xtype: 'combo',
		editable: false,
		forceSelection: true,
		queryMode: 'local',
		displayField: 'name',
		valueField: 'value',
		multiSelect: true,
		name: 'teamPickerTeamsOfTeams',
		store: '@{teamStore}'
	}, {
		xtype: 'checkbox',
		name: 'recurseThroughTeamsList',
		hideLabel: true,
		padding: '0 0 0 120',
		boxLabel: '~~recurseThroughTeams~~'
	}]
}];

RS.formbuilder.nameControlTemplate = [{
	xtype: 'fieldset',
	defaults: {
		labelWidth: 125
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	title: '~~nameOptions~~',
	defaults : {
		hideLabel : true,
	},
	items: [{
		xtype: 'checkbox',
		name: 'showFirstName',
		boxLabel : '~~showFirstName~~'
	}, {
		xtype: 'checkbox',
		name: 'showMiddleInitial',
		boxLabel : '~~showMiddleInitial~~'
	}, {
		xtype: 'checkbox',
		name: 'showMiddleName',
		boxLabel : '~~showMiddleName~~'
	}, {
		xtype: 'checkbox',
		name: 'showLastName',
		boxLabel : '~~showLastName~~'
	}]
}];

RS.formbuilder.dependencyControlTemplate = [{
	xtype: 'grid',
	cls : 'rs-grid-dark',
	title: '~~dependencies~~',
	name: 'dependencies',
	flex: 1,
	minHeight: 250,
	autoScroll : true,
	store: '@{dependenciesStore}',
	forceFit: true,
	selModel: {
		selType: 'rowmodel'
	},
	dockedItems : [{
		xtype : 'toolbar',
		cls : 'rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items :  ['addDependency','editDependency','removeDependency']	
	}],
	columns: [{
		dataIndex: 'actionDisplay',
		header: '~~dependencyAction~~'
	}, {
		dataIndex: 'target',
		header: '~~target~~'
	}, {
		dataIndex: 'conditionDisplay',
		header: '~~condition~~'
	}, {
		dataIndex: 'value',
		header: '~~value~~',
		renderer: function(value) {
			return Ext.util.Format.htmlEncode(value);
		}
	}],
	listeners: {
		itemdblclick: '@{depDoubleClick}'
	}
}];

RS.formbuilder.addressControlTemplate = [{
	name: 'country',
	xtype: 'combobox',
	store: '@{countryStore}',
	queryMode: 'local',
	displayField: 'name',
	valueField: 'code',
	forceSelection: true,
	editable: false
}, {
	name: 'state',
	xtype: 'textfield'
}];

RS.formbuilder.allowInputControlTemplate = [{
	xtype: 'checkbox',
	name: 'allowUserInput',
	hideLabel : true,
	boxLabel : '~~allowUserInput~~',
	margin : '0 0 0 130'
}];

RS.formbuilder.referenceControlTemplate = [{
	xtype: 'combobox',
	name: 'referenceTable',
	store: '@{customTablesList}',
	queryMode: 'local',
	displayField: 'name',
	valueField: 'value',
	forceSelection: true,
	editable: false,
	listeners: {
		expand: function(combo) {
			combo.store.clearFilter();
		}
	}
}, {
	xtype: 'combobox',
	name: 'referenceDisplayColumn',
	store: '@{referenceColumnStore}',
	queryMode: 'local',
	displayField: 'displayName',
	valueField: 'name',
	forceSelection: true,
	editable: false
}, {
	xtype: 'textfield',
	name: 'referenceTarget',
	listeners: {
		render: function(field) {
			Ext.create('Ext.tip.ToolTip', {
				target: field.getEl(),
				html: RS.formbuilder.locale.referenceTargetTooltip
			});
		}
	}
}];

RS.formbuilder.linkTemplate = [{
	xtype: 'combobox',
	name: 'linkTarget',
	store: '@{linkTargetStore}',
	queryMode: 'local',
	displayField: 'name',
	valueField: 'name',
	editable: true
}];

RS.formbuilder.fileUploadControlTemplate = [{
	xtype: 'textfield',
	name: 'fieldLabel',
	labelWidth: 125
}, {
	xtype: 'checkbox',
	name: 'allowUpload',
	hideLabel : 'true',
	margin : '0 0 0 130',
	boxLabel : '~~allowUpload~~'
}, {
	xtype: 'checkbox',
	name: 'allowRemove',
	hideLabel : 'true',
	margin : '0 0 0 130',
	boxLabel : '~~allowRemove~~'
}, {
	xtype: 'checkbox',
	name: 'allowDownload',
	hideLabel : 'true',
	margin : '0 0 0 130',
	boxLabel : '~~allowDownload~~'
}, {
	xtype: 'textfield',
	name: 'allowedFileTypes'	
}];

RS.formbuilder.referenceTableControlTemplate = [{
	xtype: 'grid',
	cls : 'rs-grid-dark',
	title: '~~referenceTabs~~',
	store: '@{referenceTablesStore}',
	forceFit: true,
	name: 'referenceTables',
	padding: '0 0 10 0',
	dockedItems :[{
		xtype : 'toolbar',
		cls : 'rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items :  ['addReference','removeReference']
	}],
	listeners: {
		render: function(grid) {
			grid.store.on('cellSelectionVMChanged', function(vm, selections) {
				var editor = this.getPlugin('cellediting');
				if (editor && selections.length == 1) {
					var r = this.store.indexOf(selections[0]);
					editor.cancelEdit();
					editor.startEditByPosition({
						row: r,
						column: 0
					});
				}
			}, grid);
		}
	},
	selModel: {
		selType: 'cellmodel'
	},
	plugins: [{
		ptype: 'cellediting',
		pluginId: 'cellediting',
		clicksToEdit: 1
	}],
	columns: [{
		header: '~~referenceTabTitle~~',
		dataIndex: 'fieldLabel',
		hideable: false,
		editor: {
			allowBlank: false
		}
	}]
}, {
	xtype : 'fieldset',
	title : '~~buttonSet~~',
	padding : '0 10 5 10',
	layout : {
		type : 'hbox'
	},
	defaults : {
		hideLabel : true,
		margin : '0 15 0 0'
	},
	items : [{
		xtype: 'checkbox',
		name: 'allowReferenceTableAdd',
		boxLabel : '~~allowReferenceTableAdd~~'		
	}, {
		xtype: 'checkbox',
		name: 'allowReferenceTableRemove',
		boxLabel : '~~allowReferenceTableRemove~~'
	}, {
		xtype: 'checkbox',
		name: 'allowReferenceTableView',
		boxLabel : '~~allowReferenceTableView~~'
	}]
}, {
	xtype: 'combobox',
	name: 'referenceTable',
	store: '@{referenceStore}',
	queryMode: 'local',
	displayField: 'value',
	valueField: 'value',
	forceSelection: true,
	editable: false
}, {
	xtype: 'combobox',
	name: 'referenceColumns',
	multiSelect: true,
	store: '@{referenceColumnStore}',
	queryMode: 'local',
	displayField: 'displayName',
	valueField: 'name',
	forceSelection: true,
	editable: false
}, {
	xtype: 'textfield',
	name: 'referenceTableUrl'
},{
	xtype : 'fieldset',
	padding : '0 10 5 10',
	checkboxToggle : true,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	collapsed : '@{!referenceTableViewPopup}',
	collapsible : true,
	title : '~~referenceTableViewPopup~~',
	defaults : {
		labelWidth : 115
	},
	items : [{
		xtype: 'textfield',
		name: 'referenceTableViewPopupTitle'
	}, {
		xtype: 'numberfield',
		minValue: 0,
		maxValue: 4000,
		name: 'referenceTableViewPopupWidth'
	}, {
		xtype: 'numberfield',
		minValue: 0,
		maxValue: 4000,
		name: 'referenceTableViewPopupHeight'
	}]
}];

RS.formbuilder.tabContainerControlTemplate = [{
	xtype: 'grid',
	cls : 'rs-grid-dark',
	title: '~~referenceTabs~~',
	store: '@{referenceTablesStore}',
	forceFit: true,
	name: 'referenceTables',
	padding: '0 0 5 0',
	dockedItems: [{
		xtype : 'toolbar',
		cls : 'rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items : [{
			text: '~~addReference~~',
			menu: {
				items: [{
					name: 'addReferenceTable'
				}, {
					name: 'addFileManager'
				}, {
					name: 'addURL'
				}]
			}
		}, {
			name: 'removeReference'
		}]
	}],
	listeners: {
		render: function(grid) {
			grid.store.on('cellSelectionVMChanged', function(vm, selections) {
				var editor = this.getPlugin('cellediting');
				if (editor && selections.length == 1) {
					var r = this.store.indexOf(selections[0]);
					editor.cancelEdit();
					editor.startEditByPosition({
						row: r,
						column: 0
					});
				}
			}, grid);
		}
	},
	selModel: {
		selType: 'cellmodel'
	},
	plugins: [{
		ptype: 'cellediting',
		pluginId: 'cellediting',
		clicksToEdit: 1
	}],
	columns: [{
		header: '~~referenceTabTitle~~',
		dataIndex: 'fieldLabel',
		hideable: false,
		editor: {
			allowBlank: false
		}
	}]
}, {
	xtype : 'fieldset',
	hidden : '@{buttonSetIsHidden}',
	title : '~~buttonSet~~',
	padding : '0 10 5 10',
	layout : {
		type : 'hbox'
	},
	defaults : {
		hideLabel : true,
		margin : '0 15 0 0'
	},
	items : [{
		xtype: 'checkbox',
		name: 'allowReferenceTableAdd',
		boxLabel : '~~allowReferenceTableAdd~~'		
	}, {
		xtype: 'checkbox',
		name: 'allowReferenceTableRemove',
		boxLabel : '~~allowReferenceTableRemove~~'
	}, {
		xtype: 'checkbox',
		name: 'allowReferenceTableView',
		boxLabel : '~~allowReferenceTableView~~'
	}]
},{
	xtype: 'combobox',
	labelWidth: 125,
	name: 'referenceTable',
	store: '@{referenceStore}',
	queryMode: 'local',
	displayField: 'value',
	valueField: 'value',
	forceSelection: true,
	editable: false
}, {
	xtype: 'combobox',
	labelWidth: 125,
	name: 'referenceColumns',
	multiSelect: true,
	store: '@{referenceColumnStore}',
	queryMode: 'local',
	displayField: 'displayName',
	valueField: 'name',
	forceSelection: true,
	editable: false
}, {
	xtype: 'textfield',
	labelWidth: 125,
	name: 'referenceTableUrl',
	fieldLabel: '@{referenceTableUrlText}'
},{
	xtype : 'fieldset',
	hidden : '@{popupFieldSetIsHidden}',
	checkboxToggle : true,
	collapsible : true,
	title : '~~referenceTableViewPopup~~',
	collapsed : '@{!referenceTableViewPopup}',
	padding : '0 10 5 10',
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		labelWidth : 115
	},
	items : [{
		xtype: 'textfield',		
		name: 'referenceTableViewPopupTitle'
	}, {
		xtype: 'numberfield',	
		minValue: 0,
		maxValue: 4000,
		name: 'referenceTableViewPopupWidth'
	}, {
		xtype: 'numberfield',	
		minValue: 0,
		maxValue: 4000,
		name: 'referenceTableViewPopupHeight'
	}]
}];

RS.formbuilder.urlControlTemplate = [{
	xtype: 'textfield',
	name: 'referenceTableUrl'
}];

RS.formbuilder.catalogControlTemplate = [{
	xtype: 'grid',
	store: '@{comboboxesStore}',
	selected: '@{catalogSelections}',
	columns: [{
		header: '~~fieldLabel~~',
		flex: 1,
		dataIndex: 'fieldLabel',
		sortable: false
	}],
	tbar: [{
		name: 'addCombobox'
	}, {
		name: 'editCombobox'
	}, {
		name: 'removeCombobox'
	}]
}];

RS.formbuilder.sectionContainerControlTemplate = [{
	xtype: 'combobox',
	name: 'sectionType',
	store: '@{sectionTypeStore}',
	displayField: 'name',
	valueField: 'value',
	queryMode: 'local',
	padding: '10 0 0 0'
}, {
	xtype: 'textfield',
	name: 'title'
}, {
	xtype: 'combobox',
	name: 'layout',
	store: '@{layoutStore}',
	displayField: 'name',
	valueField: 'value',
	queryMode: 'local'
}, {
	xtype: 'numberfield',
	name: 'columnCount',
	minValue: 1,
	maxValue: 4
}, {
	flex: 1,
	layout: 'card',
	activeItem: '@{selectedColumnIndex}',
	items: '@{columns}',
	defaults: {
		viewMode: 'detail'
	}
}];

RS.formbuilder.spacerControlTemplate = [{
	xtype: 'checkbox',	
	name: 'showHorizontalRule',
	hideLabel : true,
	boxLabel : '~~showHorizontalRule~~'
}];

RS.formbuilder.labelControlTemplate = [{
	xtype: 'textfield',
	name: 'fieldLabel',
	fieldLabel: '~~labelDisplayText~~'
}];

RS.formbuilder.journalControlTemplate = [{
	xtype: 'checkbox',	
	name: 'allowJournalEdit',
	hideLabel : true,
	boxLabel : '~~allowJournalEdit~~',
	margin : '0 0 0 130'
}];

/**
 * Adds the template obj to the provided config before it gets rendered in the detail form view
 * @param {Object} config
 * @param {Object} obj
 */

function addTemplateToFactory(config, obj) {
	if (Ext.isArray(obj)) {
		config = config.concat(obj);
	} else {
		config.push(obj);
	}

	return config;
}

/**
 * Factory definition for Form Controls DETAIL to render controls to edit the properties of the form's controls
 * Concatinates all the templates to create one FieldSettings card for the control based on the mixins of the control
 */
RS.formbuilder.views.FormDetailControlLayoutFactory = function(subView) {
	var config = [];

	if (Ext.Array.indexOf(subView.vm.mixins, 'DatabaseControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.databaseTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'FormControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.formControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'RequireableControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.requireableControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'EncryptableControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.encryptableControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'AllowInputControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.allowInputControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'CatalogControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.catalogControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'TooltipControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.tooltipControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'UrlControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.urlControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'DefaultValueControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.defaultValueTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'LinkControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.linkTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'DefaultBooleanControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.defaultValueBooleanTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'NumberControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.numberRangeTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'DecimalControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.decimalRangeTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'DateControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.dateTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'TimeControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.timeTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'SequenceControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.sequenceTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'NameControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.nameControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'StringControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.stringRangeTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'DateTimeControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.dateTimeTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'ButtonControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.buttonTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'ChoiceControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.choiceRadioTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'GroupPickerControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.groupPickerTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'TeamPickerControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.teamPickerTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'MultiChoiceControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.choiceCheckTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'AddressControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.addressControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'ReferenceControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.referenceControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'FileUploadControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.fileUploadControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'ReferenceTableControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.referenceTableControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'TabContainerControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.tabContainerControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'SpacerControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.spacerControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'LabelControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.labelControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'JournalControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.journalControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'SizeControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.formControlSizeTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'DependencyControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.dependencyControlTemplate));

	if (Ext.Array.indexOf(subView.vm.mixins, 'SectionContainerControl') >= 0) config = addTemplateToFactory(config, Ext.clone(RS.formbuilder.sectionContainerControlTemplate));

	return {
		xtype: 'form',
		border: false,
		autoScroll: true,
		title: '@{viewmodelTranslatedName}',
		bodyPadding: '10px',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaults: {
			labelWidth: 125
		},
		items: subView.items ? config.concat(subView.items) : config,
		/*listeners: {
			activate: function(panel) {
				if (panel.body) panel.body.highlight('#d6e8ff', {
					duration: 1000
				});
			}
		}*/
	}
};

/**
 * View definition for a TextField viewmodel
 * serverEnum: UI_TEXTFIELD("TextField")
 */
glu.defView('RS.formbuilder.TextField', {
	xtype: 'textfield',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a TextField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.TextField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a TextAreaField viewmodel
 * serverEnum: UI_TEXTAREA("TextArea")
 */
glu.defView('RS.formbuilder.TextAreaField', {
	xtype: 'textarea',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a TextAreaField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.TextAreaField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a WysiwygField viewmodel
 * serverEnum: UI_TEXTAREA("TextArea")
 */
glu.defView('RS.formbuilder.WysiwygField', {
	xtype: 'htmleditor',
	parentLayout: 'FormControlLayout',
	enableSourceEdit: false
});
/**
 * View DETAIL definition for a WysiwygField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.WysiwygField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a ReadOnlyTextField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.ReadOnlyTextField', {
	xtype: 'box',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a ReadOnlyTextField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.ReadOnlyTextField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a SpacerField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.SpacerField', {
	xtype: 'component',
	html: '~~SpacerField~~',
	style: '@{ruleDisplay}',
	flex: 1,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a SpacerField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.SpacerField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a LabelField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.LabelField', {
	xtype: 'label',
	text: '@{fieldLabel}',
	flex: 1,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a LabelField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.LabelField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a RadioButtonField viewmodel
 * serverEnum: UI_CHOICE("RadioButton")
 */
glu.defView('RS.formbuilder.RadioButtonField', {
	xtype: 'radiogroup',
	parentLayout: 'FormControlLayout',
	items: '@{options}',
	columns: '@{columns}',
	itemTemplate: {
		boxLabel: {
			value: '@{displayText}',
			trigger: 'dblclick',
			field: {
				xtype: 'textfield'
			}
		},
		inputValue: '@{id}',
		checked: '@{isSelected}',
		name: '@{..id}',
		xtype: 'radio'
	}
});
/**
 * View DETAIL definition for a RadioButtonField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.RadioButtonField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a NumberField viewmodel
 * serverEnum: UI_NUMBERTEXTFIELD("NumberTextField")
 */
glu.defView('RS.formbuilder.NumberField', {
	xtype: 'numberfield',
	allowDecimals: false,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a NumberField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.NumberField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a DecimalField viewmodel
 * serverEnum: UI_DECIMALTEXTFIELD("DecimalTextField")
 */
glu.defView('RS.formbuilder.DecimalField', {
	xtype: 'numberfield',
	allowDecimals: true,
	decimalPrecision: 5,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a DecimalField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.DecimalField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a MultipleCheckboxField viewmodel
 * serverEnum: UI_MULTIPLECHECKBOX("MultipleCheckBox")
 */
glu.defView('RS.formbuilder.MultipleCheckboxField', {
	xtype: 'checkboxgroup',
	parentLayout: 'FormControlLayout',
	items: '@{options}',
	columns: '@{columns}',
	itemTemplate: {
		boxLabel: {
			value: '@{displayText}',
			trigger: 'dblclick',
			field: {
				xtype: 'textfield'
			}
		},
		xtype: 'checkbox',
		inputId: '@{id}',
		value: '@{isChecked}'
	}
});
/**
 * View DETAIL definition for a MultipleCheckboxField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.MultipleCheckboxField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a ComboBoxField viewmodel
 * serverEnum: UI_COMBOBOX("ComboBox")
 */
glu.defView('RS.formbuilder.ComboBoxField', {
	xtype: 'combobox',
	value: '@{calculateDefaultValue}',
	store: '@{optionsStore}',
	displayField: 'displayText',
	valueField: 'id',
	queryMode: 'local',
	editable: false,
	forceSelection: true,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a ComboBoxField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.ComboBoxField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a MultiSelectComboBoxField viewmodel
 * serverEnum: UI_COMBOBOX("ComboBox")
 */
glu.defView('RS.formbuilder.MultiSelectComboBoxField', {
	xtype: 'combobox',
	value: '@{calculateDefaultValue}',
	store: '@{optionsStore}',
	displayField: 'displayText',
	valueField: 'id',
	queryMode: 'local',
	delimiter: '; ',
	editable: false,
	forceSelection: true,
	multiSelect: true,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a MultiSelectComboBoxField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.MultiSelectComboBoxField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a NameField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.NameField', {
	xtype: 'fieldcontainer',
	parentLayout: 'FormControlLayout',
	layout: 'hbox',
	defaultType: 'textfield',
	items: [{
		flex: 1,
		name: 'firstName',
		hidden: '@{!showFirstName}',
		hideLabel: true
	}, {
		width: 30,
		name: 'middleInitial',
		hidden: '@{!showMiddleInitial}',
		hideLabel: true,
		margins: '0 0 0 5'
	}, {
		flex: 1,
		name: 'middleName',
		hidden: '@{!showMiddleName}',
		hideLabel: true,
		margins: '0 0 0 5'
	}, {
		flex: 2,
		name: 'lastName',
		hidden: '@{!showLastName}',
		hideLabel: true,
		allowBlank: false,
		margins: '0 0 0 5'
	}]
});
/**
 * View DETAIL definition for a NameField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.NameField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a AddressField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.AddressField', {
	xtype: 'fieldcontainer',
	parentLayout: 'FormControlLayout',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'textfield',
		labelAlign: 'top',
		fieldLabel: '~~Street1~~'
	}, {
		xtype: 'textfield',
		labelAlign: 'top',
		fieldLabel: '~~Street2~~'
	}, {
		xtype: 'fieldcontainer',
		hideLabel: true,
		layout: 'hbox',
		items: [{
			xtype: 'textfield',
			labelAlign: 'top',
			flex: 1,
			fieldLabel: '~~City~~'
		}, {
			xtype: 'textfield',
			name: 'state',
			labelAlign: 'top',
			margin: '0 0 0 5',
			flex: 1
		}]
	}, {
		xtype: 'fieldcontainer',
		hideLabel: true,
		layout: 'hbox',
		items: [{
			xtype: 'textfield',
			labelAlign: 'top',
			flex: 1,
			fieldLabel: '~~Zip~~'
		}, {
			xtype: 'combobox',
			name: 'country',
			labelAlign: 'top',
			margin: '0 0 0 5',
			flex: 1,
			store: '@{countryStore}',
			queryMode: 'local',
			displayField: 'name',
			valueField: 'code',
			forceSelection: true,
			editable: false
		}]
	}]
});
/**
 * View DETAIL definition for a AddressField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.AddressField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a EmailField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.EmailField', {
	xtype: 'textfield',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a EmailField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.EmailField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});


glu.defView('RS.formbuilder.MACAddressField', {
	xtype: 'textfield',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a MACAddressField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.MACAddressField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

glu.defView('RS.formbuilder.IPAddressField', {
	xtype: 'textfield',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a MACAddressField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.IPAddressField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

glu.defView('RS.formbuilder.CIDRAddressField', {
	xtype: 'textfield',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a EmailField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.CIDRAddressField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a PhoneField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.PhoneField', {
	xtype: 'textfield',
	parentLayout: 'FormControlLayout',
	readOnly: true,
	plugins: [new RS.formbuilder.viewer.desktop.InputTextMask('(999) 999 - 9999')]
});
/**
 * View DETAIL definition for a PhoneField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.PhoneField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a DateField viewmodel
 * serverEnum: UI_DATE("Date")
 */
glu.defView('RS.formbuilder.DateField', {
	xtype: 'datefield',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a DateField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.DateField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a TimeField viewmodel
 * serverEnum: UI_TIMESTAMP("Timestamp")
 */
glu.defView('RS.formbuilder.TimeField', {
	xtype: 'timefield',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a TimeField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.TimeField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a LinkField viewmodel
 * serverEnum: UI_LINK("Link")
 */
glu.defView('RS.formbuilder.LinkField', {
	xtype: 'textfield',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a LinkField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.LinkField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a LikeRTField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.LikeRTField', {
	xtype: 'textfield',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a LikeRTField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.LikeRTField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a CheckboxField viewmodel
 * serverEnum: UI_CHECKBOX("CheckBox")
 */
glu.defView('RS.formbuilder.CheckboxField', {
	xtype: 'checkbox',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a CheckboxField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.CheckboxField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a PasswordField viewmodel
 * serverEnum: UI_PASSWORD("Password")
 */
glu.defView('RS.formbuilder.PasswordField', {
	xtype: 'textfield',
	inputType: 'password',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a PasswordField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.PasswordField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a JournalField viewmodel
 * serverEnum: UI_JOURNALWIDGET("Journal")
 */
glu.defView('RS.formbuilder.JournalField', {
	xtype: 'journalfield',
	readOnly: true,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a JournalField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.JournalField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a DateTimeField viewmodel
 * serverEnum: UI_DATETIME("DateTime")
 */
glu.defView('RS.formbuilder.DateTimeField', {
	xtype: 'fieldcontainer',
	layout: 'hbox',
	items: [{
		xtype: 'datefield',
		flex: 1,
		value: '@{date}'
	}, {
		xtype: 'timefield',
		flex: 1,
		margin: '0 0 0 5',
		value: '@{time}'
	}],
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a DateTimeField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.DateTimeField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a ListField viewmodel
 * serverEnum: UI_LIST("List")
 */
glu.defView('RS.formbuilder.ListField', {
	xtype: 'grid',
	disableSelection: true,
	viewConfig: {
		trackOver: false
	},
	title: {
		value: '@{fieldLabel}',
		trigger: 'dblclick',
		field: {
			xtype: 'textfield'
		}
	},
	value: '@{calculateDefaultValue}',
	store: '@{optionsStore}',
	tbar: [{
		text: '~~add~~'
	}, {
		text: '~~remove~~'
	}],
	columns: [{
		dataIndex: 'text'
	}],
	hideHeaders: true,
	forceFit: true,
	selModel: {
		mode: 'MULTI'
	},
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a ListField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.ListField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a SequenceField viewmodel
 * serverEnum: UI_SEQUENCE("Sequence")
 */
glu.defView('RS.formbuilder.SequenceField', {
	xtype: 'textfield',
	value: '@{prefix}',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a SequenceField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.SequenceField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a ReferenceField viewmodel
 * serverEnum: UI_REFERENCE("Reference")
 */
glu.defView('RS.formbuilder.ReferenceField', {
	xtype: 'referencefield',
	readOnly: true,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a ReferenceField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.ReferenceField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a HiddenField viewmodel
 * serverEnum: UI_HIDDEN("HiddenField")
 */
glu.defView('RS.formbuilder.HiddenField', {
	xtype: 'textfield',
	parentLayout: 'FormControlLayout'
});

/**
 * View definition for a TagField viewmodel
 * serverEnum: UI_TAG("TagField")
 */
glu.defView('RS.formbuilder.TagField', {
	xtype: 'tagfield',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a TagField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.TagField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a ButtonField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.ButtonField', {
	xtype: 'button',
	text: '@{operationName}',
	minWidth: 50,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a ButtonField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.ButtonField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a UserPickerField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.UserPickerField', {
	xtype: 'combobox',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a UserPickerField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.UserPickerField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View definition for a TeamPickerField viewmodel
 * serverEnum:
 */
glu.defView('RS.formbuilder.TeamPickerField', {
	xtype: 'combobox',
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a TeamPickerField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.TeamPickerField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

glu.defView('RS.formbuilder.FileUploadField', {
	xtype: 'grid',
	flex: 1,
	disableSelection: true,
	title: {
		value: '@{fieldLabel}',
		trigger: 'dblclick',
		field: {
			xtype: 'textfield'
		}
	},
	store: '@{fileStore}',
	tbar: [{
		text: '~~upload~~',
		disabled: true
	}, {
		text: '~~removeFile~~',
		disabled: true
	}],
	columns: [{
		header: '~~uploadColumnName~~',
		dataIndex: 'name'
	}],
	forceFit: true,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a FileUploadField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.FileUploadField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

glu.defView('RS.formbuilder.ReferenceTableField', {
	xtype: 'tabpanel',
	plain: true,
	items: '@{referenceTables}',
	activeTab: '@{currentReference}',
	flex: 1,
	itemTemplate: {
		title: '@{fieldLabel}',
		xtype: 'grid',
		disableSelection: true,
		store: '@{..referenceDisplayStore}',
		tbar: [{
			text: '~~referenceTableNew~~',
			hidden: '@{!allowReferenceTableAdd}',
			disabled: true
		}, {
			text: '~~referenceTableDelete~~',
			hidden: '@{!allowReferenceTableRemove}',
			disabled: true
		}],
		columns: [{
			header: '~~referenceTableColumnName~~',
			dataIndex: 'name'
		}],
		forceFit: true
	},
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a ReferenceTableField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.ReferenceTableField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

glu.defView('RS.formbuilder.ReferenceTableField', {
	xtype: 'tabpanel',
	plain: true,
	items: '@{referenceTables}',
	activeTab: '@{currentReference}',
	flex: 1,
	itemTemplate: {
		title: '@{fieldLabel}',
		xtype: 'grid',
		disableSelection: true,
		store: '@{..referenceDisplayStore}',
		tbar: [{
			text: '~~referenceTableNew~~',
			hidden: '@{!allowReferenceTableAdd}',
			disabled: true
		}, {
			text: '~~referenceTableDelete~~',
			hidden: '@{!allowReferenceTableRemove}',
			disabled: true
		}],
		columns: [{
			header: '~~referenceTableColumnName~~',
			dataIndex: 'name'
		}],
		forceFit: true
	},
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a TabContainerField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.TabContainerField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

glu.defView('RS.formbuilder.TabContainerField', {
	xtype: 'tabpanel',
	plain: true,
	items: '@{referenceTables}',
	activeTab: '@{currentReference}',
	flex: 1,
	itemTemplate: {
		title: '@{fieldLabel}',
		xtype: 'grid',
		disableSelection: true,
		store: '@{..referenceDisplayStore}',
		tbar: [{
			text: '@{..currentReferenceNew}',
			hidden: '@{!allowReferenceTableAdd}',
			disabled: true
		}, {
			text: '~~referenceTableDelete~~',
			hidden: '@{!allowReferenceTableRemove}',
			disabled: true
		}],
		columns: [{
			header: '~~referenceTableColumnName~~',
			dataIndex: 'name'
		}],
		forceFit: true
	},
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a TabContainerField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.TabContainerField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

/**
 * View DETAIL definition for a UrlField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.UrlField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

glu.defView('RS.formbuilder.UrlField', {
	title: '~~UrlField~~',
	html: '~~urlFieldHtmlText~~',
	flex: 1,
	parentLayout: 'FormControlLayout'
});
/**
 * View DETAIL definition for a UrlField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.UrlField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

glu.defView('RS.formbuilder.SectionContainerField', {
	title: '@{title}',
	flex: 1,
	layout: 'hbox',
	items: '@{columns}',
	height: null,
	parentLayout: 'FormControlLayout'
});

glu.defView('RS.formbuilder.Column', {
	flex: 1,
	items: '@{controls}',
	activeItem: '@{detailControl}',
	layout: 'anchor',
	minHeight: 300,
	defaults: {
		anchor: '100%'
	},
	cls: 'formControlSectionContainer',
	bodyCls: 'formControlSectionContainerBody',
	plugins: [{
		ptype: 'activeitemfieldsectionselector'
	}, {
		ptype: 'panelfielddragzone',
		ddGroup: 'columnControls',
		targetCls: '.formControlSectionContainer'
	}, {
		ptype: 'panelfielddroptarget',
		ddGroup: 'columnControls',
		targetCls: 'formControlSectionContainer'
	}],
	listeners: {
		itemDropped: '@{dropControl}',
		clicked: '@{columnClicked}',
		render: function(col) {
			col.getEl().on('click', function() {
				col.fireEvent('clicked');
			});
		}
	}
});

glu.defView('RS.formbuilder.Column', 'detail', {
	items: '@{controls}',
	layout: 'card',
	activeItem: '@{detailControl}',
	defaults: {
		viewMode: 'detail'
	}
});

glu.defView('RS.formbuilder.Panel', {
	hidden: true,
	border: false
})
glu.defView('RS.formbuilder.Panel', 'detail', {
	hidden: true,
	border: false
})

/**
 * View DETAIL definition for a TabContainerField viewmodel - renders with templates from the detail control layout factory
 */
glu.defView('RS.formbuilder.SectionContainerField', 'detail', {
	parentLayout: 'FormDetailControlLayout'
});

glu.defView('RS.formbuilder.FormPicker', {
	asWindow: {
		title: '@{title}',
		cls : 'rs-modal-popup',
		padding : 15,
		modal: true,
		listeners: {
			beforeshow: function() {
				clientVM.setPopupSizeToEightyPercent(this);
			}
		}
	},
	layout: 'fit',
	buttons: [{
		name :'select',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'createForm',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}],
	items: [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'forms',
		columns: '@{formsColumns}',
		plugins: [{
			ptype: 'pager'
		}],
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',			
			items: [{
				xtype: 'textfield',
				name: 'searchQuery',			
				width : 400				
			}]
		}]
	}]
})
glu.defView('RS.formbuilder.Forms', {
	xtype: 'grid',
	cls : 'rs-grid-dark',
	stateId: '@{stateId}',
	stateful: true,
	padding : 15,
	layout: 'fit',
	displayName: '@{displayName}',
	name: 'forms',
	store: '@{store}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['createForm', 'viewForm', 'deleteForms']
	}],
	columns: '@{columns}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},

	listeners: {
		editAction: '@{editForm}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
});
/**
 * View definition for the ImportOption viewmodel.  Displays a window with a listbox on the left to choose from a list of
 * categories and then displays the actual options on the right for the group chosen.
 */
glu.defView('RS.formbuilder.ImportOption', {
	width: 600,
	height: 300,
	modal: true,
	cls : 'rs-modal-popup',
	title: '~~importOptions~~',
	border: false,
	layout: 'fit',
	items: [{
		layout: {
			type: 'hbox',
			align: 'stretch',
			padding: 10
		},
		items: [{
			flex: 1,
			hideLabel: true,
			xtype: 'multiselect',
			multiSelect: false,
			name: 'selectedOption',
			valueField: 'name',
			displayField: 'name',
			store: '@{availableOptions}'
		}, {
			flex: 1,
			xtype: 'textarea',
			hideLabel: true,
			name: 'optionText',
			padding: '0px 5px 0px 10px'
		}]
	}],
	buttons: [{
		name: 'importOption',
		cls : 'rs-med-btn rs-btn-dark'
	}, {
		name: 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});
/**
 * View definition for the main view of the form builder.  This component can be placed anywhere in the system and will render the form builder inside of a border layout
 * It provides an overal container for the available controls, the preview form, and detailed information about the form
 */
glu.defView('RS.formbuilder.Main', {	
	padding: 15,
	layout: 'border',
	defaults: {
		border: false
	},
	dockedItems: [{
		xtype: 'toolbar',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls: 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '~~formBuilderTitle~~'
		}]
	},{
		xtype : 'toolbar',
		cls : 'rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items :  [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',		
		}, {
			name: 'saveForm',
			listeners: {
				render: function(button) {
					// Insert the save button into clientVM
					clientVM.updateSaveButtons(button);
				}
			}

		}, 'saveFormAs', 'preview', '->', {
			iconCls: 'x-tbar-loading',
			handler: '@{refreshForm}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	items: [{
		region: 'center',
		defaults: {
			border: false
		},
		layout: 'border',
		items: [{
			title: '~~availableFields~~',
			bodyCls: 'availableFields',
			region: 'east',
			width: 200,
			maxWidth: 200,
			split: true,
			overflowY: 'scroll',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaultType: 'button',
			items: '@{availableControls}',
			itemTemplate: {
				ui: 'form-builder-button-small-toolbar',
				xtype: 'button',
				text: '@{name}',
				name: 'addControl',
				value: '@{controlType}',
				cls: 'formControlButton',
				padding: '3px',
				margin: '2px 5px 2px 5px',
				disabled: '@{isDisabled}'
			},
			plugins: [{
				ptype: 'panelavailablecontroldragzone'
			}]
		}, {
			region: 'center',
			xtype: '@{mainForm}'
		}],
		listeners: {
			afterrender: function() {
				var splitters = this.query('bordersplitter')
				Ext.Array.forEach(splitters, function(splitter) {
					splitter.setSize(1, 1)
					splitter.getEl().setStyle({
						backgroundColor: '#fbfbfb'
					})
				})
			}
		}
	}, {
		region: 'east',
		width: 525,
		layout: 'card',
		plain: true,
		deferredRender: false,
		activeItem: '@{mainForm.showFieldSettings}',
		tbar: {
			xtype: 'toolbar',
			cls: 'actionBar actionBar-form',
			style: 'padding-bottom: 6px',
			ui: 'display-toolbar',
			defaultButtonUI: 'display-toolbar-button',
			items: [{
				name: 'showFormSettings',
				pressed: '@{showFormSettingsIsPressed}'
			}, {
				name: 'showFieldSettings',
				pressed: '@{showFieldSettingsIsPressed}'
			}]
		},
		bodyStyle: 'padding-top:4px',
		items: [{
			xtype: '@{mainForm}',
			viewMode: 'detail'
		}, {			
			layout: 'card',
			border: false,
			activeItem: '@{mainForm.showSelectFieldCard}',
			items: [{
				border: false,
				html: '<div class="notice-small"><h3>Please select a field.</h3></div>'
			}, {
				layout: 'card',
				border: false,
				activeItem: '@{mainForm.detailControl}',
				items: '@{mainForm.controls}',
				defaults: {
					viewMode: 'detail'
				}
			}]
		}]
	}],	
	listeners: {
		afterrender: function() {
			var splitters = this.query('bordersplitter')
			Ext.Array.forEach(splitters, function(splitter) {
				splitter.setSize(1, 1)
				splitter.getEl().setStyle({
					backgroundColor: '#fbfbfb'
				})
			})
		}
	}
});

/**
 * View definition for a PreviewTable which is a window with a grid in it that displays the changes to be made to the database so users
 * can confirm that the changes are what they are expecting to be made
 */
glu.defView('RS.formbuilder.PreviewTable', {
	height: 500,
	width: 700,
	padding : 15,
	cls : 'rs-modal-popup',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	title: '~~previewOfChanges~~',
	border: false,
	modal: true,

	items: [{
			border: false,
			bodyPadding: '10px',
			html: '@{previewWarning}',
			hidden: '@{!previewWarningIsVisible}'
		}, {
			border: false,
			bodyPadding: '10px',
			html: '@{sizeWarning}',
			hidden: '@{!sizeWarningIsVisible}'
		},
		/*{
		xtype: 'form',
		items: [{
			xtype: 'fieldset',
			title: 'Wiki Options',
			items: [{
				xtype: 'fieldcontainer',
				layout: 'hbox',
				hidden: '@{showCreateWiki}',
				items: [{
					xtype: 'radio',
					name: 'wikiOption',
					inputValue: 'add',
					checked: '@{addToWiki}',
					boxLabel: '~~addToWiki~~'
				}, {
					xtype: 'displayfield',
					name: 'wikiName',
					flex: 1,
					hideLabel: true,
					padding: '0 0 0 5'
				}]
			}, {
				xtype: 'fieldcontainer',
				layout: 'hbox',
				hidden: '@{!showCreateWiki}',
				items: [{
					xtype: 'radio',
					name: 'wikiOption',
					inputValue: 'create',
					checked: '@{createWiki}',
					boxLabel: '~~createWiki~~'
				}, {
					xtype: 'textfield',
					flex: 1,
					hideLabel: true,
					name: 'wikiNameCreate',
					padding: '0 0 0 10'
				}]
			}, {
				xtype: 'radio',
				name: 'wikiOption',
				inputValue: 'none',
				checked: '@{noWiki}',
				boxLabel: '~~noWiki~~'
			}]
		}]
	},*/
		{
			xtype: 'grid',
			flex: 1,
			cls : 'rs-grid-dark',
			store: '@{tableStore}',
			title: '@{tableName}',
			border: false,
			forceFit: true,
			emptyText: '~~noColumnChanges~~',
			viewConfig: {
				deferEmptyText: false
			},
			columns: [{
					header: '~~columnName~~',
					dataIndex: 'columnName'
				}, {
					header: '~~action~~',
					dataIndex: 'action',
					renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
						if (record.data.actionState === '1') metaData.style = 'background-color: #dc4b4b; color: white';
						else metaData.style = '';
						return value;
					}
				}
				/*, {
		 header : '~~dataType~~',
		 dataIndex : 'type'
		 }*/
			]
		}
	],	
	buttons: [{
		name :'save',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});
/**
 * A custom drag proxy implementation specific to {@link Ext.panel.Panel}s. This class
 * is primarily used internally for the Panel's drag drop implementation, and
 * should never need to be created directly.
 * @private
 */
Ext.define('RS.panel.Proxy', {

	/**
	 * @cfg {Boolean} [moveOnDrag=true]
	 * True to move the panel to the dragged position when dropped
	 */
	moveOnDrag: true,

	/**
	 * Creates new panel proxy.
	 * @param {Ext.panel.Panel} panel The {@link Ext.panel.Panel} to proxy for
	 * @param {Object} [config] Config object
	 */
	constructor: function(panel, config) {
		var me = this;

		/**
		 * @property panel
		 * @type Ext.panel.Panel
		 */
		me.panel = panel;
		me.id = me.panel.id + '-ddproxy';
		Ext.apply(me, config);
	},

	/**
	 * @cfg {Boolean} insertProxy
	 * True to insert a placeholder proxy element while dragging the panel, false to drag with no proxy.
	 * Most Panels are not absolute positioned and therefore we need to reserve this space.
	 */
	insertProxy: true,

	// private overrides
	setStatus: Ext.emptyFn,
	reset: Ext.emptyFn,
	update: Ext.emptyFn,
	stop: Ext.emptyFn,
	sync: Ext.emptyFn,

	/**
	 * Gets the proxy's element
	 * @return {Ext.Element} The proxy's element
	 */
	getEl: function() {
		return this.ghost.el;
	},

	/**
	 * Gets the proxy's ghost Panel
	 * @return {Ext.panel.Panel} The proxy's ghost Panel
	 */
	getGhost: function() {
		return this.ghost;
	},

	/**
	 * Gets the proxy element. This is the element that represents where the
	 * Panel was before we started the drag operation.
	 * @return {Ext.Element} The proxy's element
	 */
	getProxy: function() {
		return this.proxy;
	},

	/**
	 * Hides the proxy
	 */
	hide: function() {
		var me = this;

		if (me.ghost) {
			if (me.proxy) {
				me.proxy.remove();
				delete me.proxy;
			}

			// Unghost the Panel, do not move the Panel to where the ghost was
			if (me.panel.unghost) me.panel.unghost(null, me.moveOnDrag);
			delete me.ghost;
		}
	},

	/**
	 * Shows the proxy
	 */
	show: function() {
		var me = this,
			panelSize;

		if (!me.ghost) {
			panelSize = me.panel.getSize();
			me.panel.el.setVisibilityMode(Ext.Element.DISPLAY);
			// debugger;
			if (me.panel.ghost) {
				me.ghost = me.panel.ghost();
			} else {
				me.ghost = {};
			}
			if (me.insertProxy) {
				// bc Panels aren't absolute positioned we need to take up the space
				// of where the panel previously was
				me.proxy = me.panel.el.insertSibling({
					cls: Ext.baseCSSPrefix + 'panel-dd-spacer'
				});
				me.proxy.setSize(panelSize);
			}
		}
	},

	// private
	repair: function(xy, callback, scope) {
		this.hide();
		Ext.callback(callback, scope || this);
	},

	/**
	 * Moves the proxy to a different position in the DOM.  This is typically
	 * called while dragging the Panel to keep the proxy sync'd to the Panel's
	 * location.
	 * @param {HTMLElement} parentNode The proxy's parent DOM node
	 * @param {HTMLElement} [before] The sibling node before which the
	 * proxy should be inserted. Defaults to the parent's last child if not
	 * specified.
	 */
	moveProxy: function(parentNode, before) {
		if (this.proxy) {
			parentNode.insertBefore(this.proxy.dom, before);
		}
	}
});
glu.defView('RS.formbuilder.SaveFormAs', {
	height: 230,
	width: 500,
	padding : 15,
	cls : 'rs-modal-popup',
	layout: 'fit',
	title: '~~saveFormAsTitle~~',
	modal: true,
	items: [{
		xtype: 'form',	
		items: [{
			name: 'name',
			xtype: 'textfield',
			anchor: '-20'
		}, {
			name: 'title',
			xtype: 'textfield',
			anchor: '-20'
		}]
	}],	
	buttons: [{
		name: 'saveFormAs',
		cls : 'rs-med-btn rs-btn-dark'
	}, {
		name: 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});
glu.defView('RS.formbuilder.TabPanel', {
	xtype: 'tabpanel',
	plain: true,
	items: '@{tabs}',
	activeTab: '@{selectedTab}',
	height: 23
});

glu.defView('RS.formbuilder.TabPanel', 'detail', {
	xtype: 'panel',
	bodyPadding: '10px',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'grid',
		title: '~~tabs~~',
		store: '@{tabStore}',
		forceFit: true,
		name: 'tabs',
		tbar: [{
			name: 'addTab'
		}, {
			name: 'editTab'
		}, {
			name: 'removeTab'
		}],
		selModel: {
			selType: 'cellmodel'
		},
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		columns: [{
			header: '~~Name~~',
			dataIndex: 'title',
			hideable: false,
			sortable: false,
			editor: {
				allowBlank: false
			}
		}]
	}]
});

glu.defView('RS.formbuilder.Tab', {
	title: '@{title}'
	// closable : '@{closable}'
});

glu.defView('RS.formbuilder.Tab', 'detail', {
	title: '~~editTabTitle~~',
	height: 300,
	width: 600,
	layout: 'fit',
	border: false,
	modal: true,
	items: [{
		xtype: 'form',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		border: false,
		bodyPadding: '10px',
		items: [{
			xtype: 'textfield',
			name: 'title'
		}, {
			xtype: 'grid',
			title: '~~dependencies~~',
			name: 'dependencies',
			flex: 1,
			store: '@{dependenciesStore}',
			forceFit: true,
			selModel: {
				selType: 'rowmodel'
			},
			/*plugins: [{
		ptype: 'cellediting',
		clicksToEdit: 1
	}],*/
			tbar: [{
				name: 'addDependency'
			}, {
				name: 'editDependency'
			}, {
				name: 'removeDependency'
			}],
			columns: [{
				dataIndex: 'actionDisplay',
				header: '~~dependencyAction~~'
				/*editor: {
			xtype: 'combobox',
			store: '@{dependencyActionStore}',
			displayField: 'name',
			valueField: 'name',
			forceSelection: true,
			editable: false,
			selectOnTab: true,
			triggerAction: 'all',
			queryMode: 'local'
		}*/
			}, {
				dataIndex: 'target',
				header: '~~target~~'
				/*editor: {
			xtype: 'combobox',
			store: '@{dependencyTargetStore}',
			displayField: 'name',
			valueField: 'value',
			forceSelection: true,
			editable: false,
			selectOnTab: true,
			triggerAction: 'all',
			queryMode: 'local',
			listeners: {
				expand: function(field) {
					field.store.load();
				}
			}
		}*/
			}, {
				dataIndex: 'conditionDisplay',
				header: '~~condition~~'
				/*editor: {
			xtype: 'combobox',
			store: '@{dependencyConditionStore}',
			displayField: 'name',
			valueField: 'name',
			forceSelection: true,
			editable: false,
			selectOnTab: true,
			triggerAction: 'all',
			queryMode: 'local',
			listConfig: {
				minWidth: 150
			},
			listeners: {
				expand: function(field) {
					field.store.load()
				}
			}
		}*/
			}, {
				dataIndex: 'value',
				header: '~~value~~',
				// editor: {},
				renderer: function(value) {
					return Ext.util.Format.htmlEncode(value);
				}
			}],
			listeners: {
				itemdblclick: '@{depDoubleClick}'
			}
		}]
	}],
	buttonAlign: 'left',
	buttons: [{
		name: 'applyTab'
	}, {
		name: 'cancel'
	}]
});
glu.defView('RS.formbuilder.Viewer', {
	layout: 'fit',
	items: [{
		xtype: 'rsform',
		showLoading: false,
		formName: '@{formName}',
		formId: '@{formId}',
		recordId: '@{recordId}',
		queryString: '@{queryString}',
		params: '@{params}',
		embed: '@{embed}',
		formdefinition: '@{formdefinition}',
		fromViewer: true,
		items: [],
		listeners: {
			afterrender: function(form) {
				form.fireEvent('registerWithViewmodel', form, form)
			},
			registerWithViewmodel: '@{registerForm}',
			beforeclose: function() {
				this.up('panel').fireEvent('beforeclose')
			},
			//circulating back to update the record id of the viewer
			actioncompleted: function(rsform, actionBtn) {
				this.up('panel').fireEvent('actioncompleted', this, rsform.recordId, actionBtn.actions);
			},
			beforeredirect: function() {
				this.up('panel').fireEvent('beforeredirect')
			},
			reloadForm: function() {
				this.up('panel').fireEvent('reloadForm')
			},
			formLoaded: '@{formLoaded}'
		}
	}],
	listeners: {
		//circulating back to update the record id of the viewer
		actioncompleted: '@{actionCompleted}'
	}
})
/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.formbuilder').locale = {
    formBuilderTitle : 'Form Builder',
    Untitled: 'Untitled',
    UntitledForm: 'Untitled Form',
    UNTITLEDFORM: 'UNTITLED_FORM',
    untitledField: 'Untitled Field',
    dbFieldName: 'Field Name',

    wikiName: 'Wiki Name',
    addToWiki: 'Add to existing wiki:',
    createWiki: 'Create new wiki named:',
    noWiki: 'Do Nothing',

    formWindowTitle: 'Window Title',

    Size: 'Size',
    width: 'Width',
    height: 'Height',

    saveForm: 'Save',
    saveFormAs: 'Save As',
    saveFormAsTitle: 'Save As',
    saveFormAsBody: 'Please enter new form name',
    saveFormAsTitle: 'Save Form As',
    COPY: 'COPY',
    Copy: 'Copy',
    savingMessage: 'Saving your form...',
    loadingMessage: 'Loading your form',
    loadingProgress: 'Loading...',
    newForm: 'New Form',
    preview: 'Preview',
    live: 'Live',
    refreshForm: 'Refresh',
    refreshFormTitle: 'Are you sure you want to refresh?',
    refreshFormBody: 'Any changes you have made to the form will be lost, are you sure you want to refresh the form?',
    discardChanges: 'Discard Changes',
    duplicateFieldNameTitle: 'Duplicate controls with same field name',
    duplicateFieldNameBody: 'Multiple controls are referencing the field name <b><i>{0}</i></b>.  Field names need to be unique to each control.  Please correct the errors on the form before saving.',
    formNameInvalidText: 'Form name must be all uppercase letters, numbers, or underscore.',
    formNameInvalidTitle: 'Form name is invalid',
    fieldNameInvalid: 'Field name can only contain letters without spaces',
    fieldNameStartInvalid: 'Field name cannot start with number',
    formNotFound: 'Form not found',
    wikiNameNamespace: 'Wiki name must indicate a namespace',

    inputFieldDBWithoutTableTitle: 'DB Field selected without table',
    inputFieldDBWithoutTableBody: 'You have a field that is trying to store data in a table, but no table has been defined for the form.  Please change the field type to Input or select a table on form settings.',

    cancel: 'Cancel',
    addAction: 'Add Action',
    editAction: 'Edit Action',
    applyAction: 'Apply',
    removeAction: 'Remove Action',

    dbTable: 'Table Name',
    dbColumn: 'Column Name',

    Buttons: 'Buttons',
    newButton: 'New Button',
    addButton: 'Add',
    addNewButton: 'Button',
    addSeparator: 'Separator',
    separator: 'Separator',
    addSpacer: 'Spacer',
    spacer: 'Spacer',
    addRightJustify: 'Right Justify',
    rightJustify: 'Right Justify',
    editButton: 'Edit',
    editButtonTitle: 'Edit Button',
    applyButton: 'Apply',
    removeButton: 'Remove',

    availableFields: 'Available Fields',
    form_settings: 'Form Settings',
    field_settings: 'Field Settings',
    showFormSettings: 'Form Settings',
    showFieldSettings: 'Field Settings',

    duplicateControl: 'Duplicate',
    removeControl: 'Remove',
    title: 'Title',
    name: 'Name',
    labelAlign: 'Label Alignment',
    mandatory: 'Required',
    encrypted: 'Encrypted',

    Left: 'Left',
    Right: 'Right',
    Top: 'Top',

    fieldLabel: 'Field Label',
    defaultValue: 'Default Value',

    minLength: 'Min Length',
    maxLength: 'Max Length',

    dbTypeParams: 'DB Type Parameters',
    columnSize: 'Column Size',

    minValue: 'Min Value',
    maxValue: 'Max Value',

    Range: 'Range',
    Options: 'Options',
    columns: 'Columns',

    allowUserInput: 'Allow user input',

    addOption: 'Add',
    removeOption: 'Remove',
    Option: 'Option',

    HiddenFields: 'Hidden Fields',
    addHiddenField: 'Add',
    removeHiddenField: 'Remove',

    Name: 'Name',
    Value: 'Value',

    text: 'Text',
    untitledButton: 'Untitled Button',

    date: 'Date',
    time: 'Time',

    prefix: 'Prefix',

    column: 'Field Name',
    inputName: 'Field Name',
    table: 'Table Name',
    type: 'Type',
    newColumn: 'New Column',

    value: 'Value',
    operation: 'Operation',
    EVENT: 'Event',
    CLOSE: 'Close',
    RESET: 'Reset',
    CANCEL: 'Cancel',
    returnText: 'Return',
    RETURN: 'Return',
    EXIT: 'Exit',
    exit: 'Exit',

    customTableDisplay: 'Display on custom table',
    customTableDisplayTooltip: 'If checked, this button will appear in the custom table grid and you will be able to execute the button actions from the custom table grid without having to load the form',

	font: 'Font Style',
	fgColorTooltip: 'Font color of button',
	bgColorTooltip: 'Background color of button',

    dbAction: 'DB Action',
    submitAction: 'Submit',
    deleteActionName: 'Delete',
    RUNBOOK: 'Runbook',
    SCRIPT: 'Script',
    PROBLEMID: 'Problem Id',
    NONE: 'NONE',
    EVENT_EVENTID: 'Event Id',
    EVENT_REFERENCE: 'Event Reference',
    eventIdInvalid: 'You must provide an event Id',
    eventReferenceInvalid: 'You must provide an event reference',

	SIR: 'SIR',
	sirAction: 'SIR Action',
	saveCaseData: 'Save Case Data',
	actionsirActionInvalid: 'You must select an sir action to perform',

    NEW: 'NEW',
    LOOKUP: 'LOOKUP',
    ACTIVE: 'ACTIVE',

    ACTIONTASK: 'Action Task',
    MESSAGEBOX: 'Message Box',

    reload: 'Reload',
    RELOAD: 'Reload',
    reloadDelay: 'Delay (in sec)',
    StopWhen: 'Stop when',

    showConfirmation: 'Show Confirmation',
    messageBoxTitle: 'Title',
    messageBoxMessage: 'Message',
    messageBoxYesText: 'Ok',
    messageBoxNoText: 'Cancel',

    confirmDeleteTitle: 'Confirm Delete',
    confirmDeleteMessage: 'Are you sure you want to delete this record?',
    confirmDeleteYesText: 'Delete',
    confirmDeleteNoText: 'Cancel',

    DB: 'DB',
    SUBMIT: 'Submit',
    RUNBOOK: 'Runbook',
    SCRIPT: 'Script',
    CLOSE: 'Close',
    DELETE: 'Delete',

    execution: 'Execution',
    asynchronous: 'Asynchronous',
    synchronous: 'Synchronous (in seconds)',
    timeout: 'Timeout (in seconds)',
	useDefault: 'Use Default',
	useDefaultTooltip: 'Use global.execute.timeout value defined in System Properties.',

    input: 'Input',
    db: 'DB',

    importOptions: 'Import Predefined Options',
    importOption: 'Add Options to Field',

    readOnly: 'Read Only',
    hidden: 'Hidden',

    //Action error messages
    leaveWithoutSave: 'Are you sure you want to leave without saving the current content?',
    leaveWithoutSaveTitle: 'Leave without save',
    leave: 'Leave',
    actionOperationInvalid: 'You must choose an operation',
    actionDbActionInvalid: 'You must select a db action to perform',
    actionRunbookInvalid: 'You must select a runbook to run',
    actionScriptInvalid: 'You must select a script to execute',
    actionTaskInvalid: 'You must select an action task to execute',
    actionOperationAlreadyDBInvalid: 'You can only have one DB operation per button. Either edit that action, or remove it before adding another DB operation.',

    noAccessRightsTitle: 'No admin access rights defined',
    noAccessRightsBody: 'You must add at least one admin access right to the form before you can save it',

    columnInvalidText: 'The column you have chosen is a "{0}" where the control requires a "{1}" to properly store its data.  Please select another column, or create a new one.',
    columnTooLongInvalidText: 'The column name you have entered is too long, please limit it to 30 characters',
    columnSpecialCharactersInvalidText: 'The column name you have entered is invalid, please use only letters, numbers, and underscores',
    validationErrorTitle: 'Validation Error',
    validationErrorBody: 'The field <b><i>{0}</i></b> is invalid.  Please correct the errors before saving the form',

    duplicateNameDetectedTitle: 'Duplicate Form Name',
    duplicateNameDetectedBody: 'You have entered a form name <b><i>{0}</i></b> which is already in the system.  Please use another name before saving the form.',
    nameInvalidText: 'A form with the name <b><i>{0}</i></b> already exists.  Please use another name before saving the form.',

    wikiNameCreateBlankInvalid: 'Please provide a wiki name to create',
    wikiNameCreateInvalid: 'A wiki document with the name <b><i>{0}</i></b> already exists in the system.  Please use another name before saving',
    wikiNameInvalid: 'Please choose a wiki document before saving',
    wikiNameAddInvalid: 'You have entered a document name that does not exist.  Please enter a name that exists in the system, or choose to create a new wiki with this name',

    titleTooLongInvalidText: 'The title of the form is too long, please limit it to 500 characters',
    formNameTooLongInvalidText: 'The name of the form is too long, please limit it to 100 characters',
    formNameEmptyInvalidText: 'The name of the form cannot be empty',


    minValueInvalid: 'The minimum value for this field is {0}',
    maxValueInvalid: 'The maximum value for this field is {0}',

    userFilter: 'User Filter',
    groups: 'Users of Groups',
    teams: 'Users of Teams',
    recurseThroughTeams: 'Recurse through teams',
    teamFilter: 'Team Filter',
    teamList: 'Owner of Teams',
    teamPickerTeamsOfTeams: 'List of teams',
    allowAssignToMe: 'Allow assign to me option',
    autoAssignToMe: 'Auto-assign to the current user',
    assignToMe: 'Assign to me',

    FirstMiddleLast: 'First, Middle, Last',

    Street1: 'Street Address',
    Street2: 'Address Line 2',
    City: 'City',
    State: 'State/Province/Region',
    Zip: 'Postal/Zip Code',
    Country: 'Country',
    country: 'Country',
    state: 'State',

    showHorizontalRule: 'Show Horizontal Line',
    labelDisplayText: 'Label Display',
    allowJournalEdit: 'Allow edit of journal entries',
    currentWiki: '$CURRENT_WIKI',

    nameOptions: 'Name Options',
    showFirstName: 'First Name',
    showMiddleInitial: 'Middle Initial',
    showMiddleName: 'Middle Name',
    showLastName: 'Last Name',
    fullName: 'Full Name',

    previewOfChanges: 'Preview of Changes',
    noColumnChanges: 'No columns changed...',
    columnName: 'Column Name',
    action: 'Action',
    dataType: 'Data Type',
    newTableColumn: 'Create',
    editTableColumn: 'Change',
    deleteTableColumn: 'Remove',
    noChangeTableColumn: 'No Change',
    saveAndUpdate: 'Save & Update',
    saveAndCreate: 'Save & Create',
    save: 'Save',

    //CONTROLS
    TextField: 'Single Line Text',
    TextAreaField: 'Paragraph Text',
    RadioButtonField: 'Radio Button',
    NumberField: 'Number',
    DecimalField: 'Decimal',
    MultipleCheckboxField: 'Multi-Checkbox',
    ComboBoxField: 'Dropdown',
    NameField: 'Name',
    AddressField: 'Address',
    EmailField: 'Email',
    MACAddressField: 'MAC Address',
    IPAddressField: 'IP Address (IP4)',
    CIDRAddressField: 'CIDR Address',
    PhoneField: 'Phone',
    DateField: 'Date',
    TimeField: 'Time',
    LinkField: 'Link',
    LikeRTField: 'Like RT',
    CheckboxField: 'Checkbox',
    PasswordField: 'Password',
    TagField: 'Tag',
    SequenceField: 'Sequence',
    ListField: 'List',
    ButtonField: 'Button',
    ReferenceField: 'Reference',
    JournalField: 'Journal',
    DateTimeField: 'Date Time',
    ReadOnlyTextField: 'Read Only Text',
    UserPickerField: 'User Picker',
    TeamPickerField: 'Team Picker',
    FileUploadField: 'File Manager',
    MultiSelectComboBoxField: 'Multi-Select Dropdown',
    ReferenceTableField: 'Reference Table',
    SpacerField: 'Spacer',
    WysiwygField: 'WYSIWYG Editor',
    TabContainerField: 'Tab Container',
    UrlField: 'Url Container',
    CatalogField: 'Catalog',
    SectionContainerField: 'Section',
    LabelField: 'Label',

    //IMPORT OPTIONS
    gender: 'Gender',
    age: 'Age',
    employment: 'Employment',
    income: 'Income',
    education: 'Education',
    daysOfTheWeek: 'Days of the Week',
    monthsOfTheYear: 'Months of the Year',
    timezones: 'Time Zones',
    states: 'States',
    continents: 'Continents',
    howOften: 'How Often',
    howLong: 'How Long',
    satisfaction: 'Satisfaction',
    importance: 'Importance',
    agreement: 'Agreement',
    comparison: 'Comparison',
    wouldYou: 'Would You',
    nflTeams: 'NFL Teams',

    AccessRights: 'Access Rights',
    addAccessRights: 'Add Access Rights',
    addAccessRightsAction: 'Add',
    addAccessRight: 'Add',
    removeAccessRight: 'Remove',
    viewRight: 'View',
    adminRight: 'Admin',
    editRight: 'Edit',

    useTabs: 'Use Tabs',
    untitledTab: 'Untitled Tab',
    tabs: 'Tabs',
    addTab: 'Add',
    editTab: 'Edit',
    removeTab: 'Remove',
    applyTab: 'Apply',
    editTabTitle: 'Edit Tab',
    confirmTabPanelRemoveTitle: 'Controls exist on multiple tabs',
    confirmTabPanelRemoveBody: 'You have multiple tabs with controls on them, what do you want to do with the controls?',
    mergeTabPanelControls: 'Merge',
    removeTabPanelControls: 'Remove All',

    confirmTabRemoveTitle: 'Controls exist on the current tab',
    confirmTabRemoveBody: 'The tab you are removing has controls on it, what would you like to do with the controls?',
    mergeTabControls: 'Merge',
    removeTabControls: 'Remove',

    success: 'Success',
    formSavedSuccessfully: 'Form Submitted Successfully',
    error: 'Error',
    communicationError: 'There was an error communicating with the server',
    noRights: 'You do not have access rights to view this form',
    noDataFromServer: 'Either you don\'t have access rights to view this form, or a problem has occurred on the server and no data was returned.  If you know you have access to this form, please check the error logs.',

    isWizard: 'Style as wizard',
    GOTO: 'Go to',
    gotoTabName: 'Tab name',
    NEXT: 'Next',
    BACK: 'Back',
    MAPPING: 'Mapping',
    mappingFrom: 'From',
    MAPFROM: 'Map From Field',
    mappingFromInvalid: 'Please choose a field to map from',
    mappingTo: 'To',
    mappingToInvalid: 'Please choose a field to map to',
    REDIRECT: 'Redirect',
    redirectTarget: 'Target',
    redirectTargetInvalid: 'Please choose a target to redirect to',
    redirectUrl: 'Url',

    //Dependency information
    name: 'Name',
    dependencies: 'Dependencies',
    dependencyAction: 'Action',
    target: 'Target',
    condition: 'Condition',
    addDependency: 'Add',
    addDependencyTitle: 'Add Dependency',
    editDependency: 'Edit',
    editDependencyTitle: 'Edit Dependency',
    color: 'Color',
    setBackgroundColor: 'Set Background Color',
    setBgColor: 'Set Background',
    stringValue: 'Value',
    numberValue: 'Value',
    dateValue: 'Value',
    booleanValue: 'Value',
    applyDependency: 'Apply',
    removeDependency: 'Remove',
    isVisible: 'Is Visible',
    isEnabled: 'Is Enabled',
    dependencyTargetInvalidTitle: 'Dependency Target Invalid',
    dependencyTargetInvalidBody: 'The control "{0}" has a dependecny on "{1}" but it does not exist on the form, please select a target that exists on the form',
    isEmpty: 'Is Empty',
    notEmpty: 'Not Empty',
    equals: 'Equals',
    notequals: 'Not Equal',
    contains: 'Contains',
    Contains: 'Contains',
    notcontains: 'Does Not Contain',
    lessThan: 'Less than',
    lessThanOrEqual: 'Less than or equal to',
    greaterThan: 'Greater than',
    greaterThanOrEqual: 'Greater than or equal to',
    on: 'On',
    after: 'After',
    before: 'Before',

    noFormNameTitle: 'No Form Name',
    noFormNameBody: 'Please provide a form name before saving this form',

    noFormTitle: 'No form from server',
    noFormBody: 'Please make sure you have rights to view this form',

    noButtonActionTitle: 'No action on button',
    noButtonActionBody: 'The button <b><i>{0}</i></b> doesn\'t have any actions attached. Please add an action to the button before saving.',

    add: 'Add',
    remove: 'Remove',

    referenceTable: 'Table',
    referenceDisplayColumn: 'Display Column',
    referenceTarget: 'Jump Target URL',
    referenceClear: 'Click to clear current value',
    referenceJump: 'Click to show in new window',
    referenceSearch: 'Click to lookup a record',
    referenceTargetTooltip: 'Provide a URL for the system to navigate to.  Use "{0}" for the location of the sys_id selected',

    select: 'Select',
    SelectRecordTitle: 'Select a record',

    linkTarget: 'Target',


    dragDropEnabledEmptyText: 'Click upload or drag files here to upload',
    dragDropDisabledEmptyText: 'Click upload to upload files',
    noRecordIdForUpload: 'Please save this form before uploading files',

    uploadColumnName: 'Name',
    uploadColumnSize: 'Size',
    uploadColumnChange: 'Change',
    uploadColumnState: 'State',
    uploadColumnStatus: 'Status',
	uploadColumnUploadedBy: 'Uploaded By',


    statusQueuedText: 'Ready to upload',
    statusUploadingText: 'Uploading ({0}%)',
    statusFailedText: 'Error',
    statusDoneText: 'Complete',
    statusInvalidSizeText: 'File too large',
    statusInvalidExtensionText: 'Invalid file type',

    upload: 'Upload',
    uploadTooltip: 'Drag files into the grid to upload',
    removeFile: 'Delete',
    ready: 'Ready',
    updateProgressText: 'Upload {0}% ({1} of {2})',
    sysUploadedOn: 'Uploaded On',

    confirmRemove: 'Confirm file delete',
    confirmRemoveFile: 'Are you sure you want to delete the selected file? This operation cannot be undone!',
    confirmRemoveFiles: 'Are you sure you want to delete the {0} selected files? This operation cannot be undone!',
    removeFile: 'Delete',
    attachments: 'Attachments',
    download: 'Download',
    files: 'Files',

    allowUpload: 'Allow Upload',
    allowRemove: 'Allow Delete',
    allowDownload: 'Allow Download',
    allowedFileTypes: 'File Types',

    noDownloadTitle: 'Upload not done',
    noDownloadBody: 'Please wait for the upload to complete before downloading this file',

    emptyUploadTitle: 'Cannot upload empty file',
    emptyUploadBody: 'This file is 0 bytes, so it will not be attached.',

    buttonSet : 'Button Set',
    references: 'References',
    referenceTable: 'Table',
    referenceColumns: 'Display Columns',
    allowReferenceTableAdd: 'New',
    allowReferenceTableRemove: 'Delete',
    allowReferenceTableView: 'View',
    referenceTableUrl: 'URL',

    referenceTableColumnName: 'Name',
    referenceTableNew: 'New',
    referenceTableDelete: 'Delete',

    referenceTableInvalid: 'You must provide a table to reference',
    referenceTableUrlInvalid: 'You must provide a url or wiki document name if you allow the user to create or view a record in the reference table',
    referenceColumnInvalid: 'You must provide a column to reference',
    invalidReferenceColumns: 'The Display Columns shouldn\'t be empty.',

    prefixRequired: 'Please provide a prefix',

    deleteFormTitle: 'Delete Form',
    deleteFormBody: 'Are you sure you want to delete the form <b><i>{0} [{1}]</i></b>?  This operation cannot be undone!',
    deleteForm: 'Delete',
    deleteReference: 'Delete',
    viewReference: 'View Reference',

    confirmRemoveReference: 'Are you sure you want to delete the selected reference? This operation cannot be undone!',
    confirmRemoveReferences: 'Are you sure you want to delete the {0} selected references? This operation cannot be undone!',

    fileUploadNoTableTitle: 'File Upload Field detected without a table',
    fileUploadNoTableBody: 'You cannot have a file upload field without a database table.  Please remove the file upload field or select a table for the form to store data.',

    useReferenceTabs: 'Use Tabs',
    addReference: 'Add',
    addReferenceTable: 'Add Reference Table',
    addFileManager: 'Add File Manager',
    fileManager: 'File Manager',
    addURL: 'Add URL',
    urlTab: 'Blank URL',
    urlTabInvalid: 'Please provide a url to load in this tab',
    removeReference: 'Remove',
    unknownReference: 'Unknown Reference',
    referenceTabs: 'Reference Tabs',
    referenceTabTitle: 'Title',
    referenceTableUrlNewTab: 'Display in new tab',
    referenceTableViewPopup: 'Display in popup',
    referenceTableViewPopupWidth: 'Popup Width',
    referenceTableViewPopupHeight: 'Popup Height',
    referenceTableViewPopupTitle: 'Popup Title',
    EditReference: 'Edit - ',
    NewReference: 'New - ',

    untitledFieldNameTitle: 'Field is Untitled',
    untitledFieldNameBody: 'There is an untitled field on the form, please give it a name',

    invalidFormTitle: 'Form is invalid',
    invalidFormBody: 'Please correct the errors on the form before submitting',

    invalidDBTableName: 'Table Name must be alphanumeric, with underscores allowed',

    previewWarning: '<font style="color:red">Warning: This action may take some time while we re-initialize the database sessions.  The system will not be useable during this time.</font>',
    sizeWarning: '<font style="color:red">Warning: This table is getting to be very large.  There are known issues with creating tables with a large number of columns when using MySQL.  Please be sure to check the table in your database to make sure each column was properly created.</font>',

    uploadInProgressTitle: 'Upload in progress',
    uploadInProgressBody: 'An upload is currently in progress, please wait for the upload to finish before performing an action.',

    tooltip: 'Tooltip',

    urlFieldHtmlText: 'Your page displayed here',

    item: 'Item',
    untitledCombo: 'Untitled',
    addCombobox: 'Add',
    applyCombobox: 'Apply',
    editCombobox: 'Edit',
    removeCombobox: 'Remove',
    addCatalogItem: 'Add Item',
    removeCatalogItem: 'Remove',
    displayField: 'Display Field',
    valueField: 'Value Field',
    where: 'Where',
    parentField: 'Field',
    editCatalogOption: 'Edit Option',
    addCatalogOption: 'Add Option',
    removeCatalogOption: 'Remove Option',
    applyCatalogOption: 'Apply',
    display: 'Display',
    addCatalogCombobox: 'Add Combobox',
    editCatalogCombobox: 'Edit Combobox',
    optionType: 'Option Type',
    simple: 'Simple',
    advanced: 'Advanced',
    custom: 'Custom',
    sqlQuery: 'Sql Query',
    dropdownChoiceTable: 'Table',

    panel: 'Panel',
    fieldset: 'Field Set',
    columnCount: 'Columns',
    columnLayout: 'Column',
    auto: 'Auto',
    sectionType: 'Section type',
    layout: 'Layout',
    sectionInSectionTitle: 'Cannot add Section',
    sectionInSectionMessage: 'You cannot add a Section to another Section.',

    //Forms
    formsTitle: 'Forms',
    uformName: 'Name',
    udisplayName: 'Display Name',
    utableName: 'Table Name',
    createForm: 'New',
    deleteForms: 'Delete',
    sysUpdatedOn: 'Updated On',
    sysCreatedOn: 'Created On',
    sysCreatedBy: 'Created By',
    sysUpdatedBy: 'Updated By',
    sysId: 'Sys ID',
    sysOrganizationName: 'Organization',
    formsWindowTitle: 'Forms',
    back: 'Back',
    viewForm: 'View',
    DeleteFormsTitle: 'Delete Forms',
    DeleteFormsMsg: 'Are you sure you want to delete the selected records({num} items selected)?',
    DeleteFormMsg: 'Are you sure you want to delete the selected item?',
    confirmDelete: 'OK',
    DeleteSucMsg: 'The selected records have been deleted.',
    DeleteErrMsg: 'Cannot delete the selected records.',
    ListFormsErrMsg: 'Cannot get forms from the server.',

    formPickerTitle: 'Select a Form',
    searchQuery: 'Search',
    createForm: 'Create',

    execute: 'Execute',

    unknownAction: 'Unknown action',
    unknownActionBody: 'The form you have loaded has an action that is unknown to this viewer. Action was {0}',

	worksheet_not_found: 'Worksheet not found'
}
/**
 * Plugin that highlights the selected field (whether it was done by the user on cilck or programatically)
 */
Ext.define('RS.formbuilder.plugins.ActiveItemFieldSectionSelector', {
	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.activeitemfieldsectionselector',

	init: function(panel) {
		panel.on('activeitemchanged', this.selectField, this, {
			delay: 1,
			buffer: 50
		});
	},

	selectField: function(form, control) {
		//focus on the input field of the control so the user can edit the control
		if (control.items.first() && control.items.first().focus) control.items.first().focus();

		//if the control isn't already selected, then we need to select it and highlight it
		var highlightColor = 'efefef';
		// if (control.getEl().getColor('background-color', '#ffffff') !== '#' + highlightColor.toLowerCase()) {
		//display a background highlight on the item when its selected
		var panel = control.up('panel');
		if (panel) {
			panel.items.each(function(item) {
				var buttons = item.query('button');
				Ext.each(buttons, function(button) {
					if (button.icon) button.hide();
				});
				var el = item.getEl();
				if (el.getColor('background-color', '#ffffff') !== '#ffffff') {
					item.getEl().animate({
						duration: 100,
						easing: 'ease-in',
						from: {
							backgroundColor: highlightColor
						},
						to: {
							backgroundColor: 'ffffff'
						}
					});
				}
			});

			//display the duplicate and remove buttons
			var buttons = control.query('button');
			Ext.each(buttons, function(button) {
				Ext.defer(function() {
					button.show();
				}, 10);
			});

			control.getEl().stopAnimation();
			control.getEl().animate({
				duration: 250,
				easing: 'ease-in',
				from: {
					backgroundColor: 'ffffff'
				},
				to: {
					backgroundColor: highlightColor
				}
			});
		}

		// }
	}
});
/**
 * Plugin that highlights the selected field (whether it was done by the user on cilck or programatically)
 */
Ext.define('RS.formbuilder.plugins.ActiveItemFieldSelector', {
	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.activeitemfieldselector',

	init: function(panel) {
		panel.on('activeitemchanged', this.selectField, this, {
			delay: 1,
			buffer: 50
		});
	},

	selectField: function(form, control) {
		//focus on the input field of the control so the user can edit the control
		if (control.items.first() && control.items.first().focus) control.items.first().focus();

		//if the control isn't already selected, then we need to select it and highlight it
		var highlightColor = 'efefef';
		// if (control.getEl().getColor('background-color', '#ffffff') !== '#' + highlightColor.toLowerCase()) {
		//display a background highlight on the item when its selected
		var form = control.up('form');
		if (form) {
			form.items.each(function(item) {
				var buttons = item.query('button');
				Ext.each(buttons, function(button) {
					if (button.ownerCt == item && button.icon) button.hide();
				});
				var el = item.getEl();
				if (el.getColor('background-color', '#ffffff') !== '#ffffff') {
					item.getEl().animate({
						duration: 100,
						easing: 'ease-in',
						from: {
							backgroundColor: highlightColor
						},
						to: {
							backgroundColor: 'ffffff'
						}
					});
				}
			});

			//display the duplicate and remove buttons
			var buttons = control.query('button');
			Ext.each(buttons, function(button) {
				if (button.ownerCt == control) button.show();
			});

			control.getEl().stopAnimation();
			control.getEl().animate({
				duration: 250,
				easing: 'ease-in',
				from: {
					backgroundColor: 'ffffff'
				},
				to: {
					backgroundColor: highlightColor
				}
			});
		}

		// }
	}
});
/**
 * Plugin that makes Buttons from the Avaialable Controls dragable onto the form
 */
Ext.define('RS.formbuilder.plugins.PanelAvailableControlDragZone', {

	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.panelavailablecontroldragzone',

	init: function(panel) {
		panel.on('render', this.onPanelRender, this, {
			single: true
		});
	},

	onPanelRender: function(panel) {
		var temp = new Ext.dd.DragZone(panel.getEl(), {
			ddGroup: 'columnControls',

			//configures the different drag targets based on a css selector and returns the item data for dragging
			getDragData: function(e) {
				var sourceEl = e.getTarget('.formControlButton');
				if (sourceEl) {
					var d = sourceEl.cloneNode(true);
					d.id = Ext.id();
					return {
						ddel: d,
						sourceEl: sourceEl,
						repairXY: Ext.fly(sourceEl).getXY(),
						proxyPanel: new Ext.panel.Proxy(Ext.getCmp(sourceEl.id), {})
					}
				}
			},

			//Provide coordinates for the proxy to slide back to on failed drag.
			//This is the original XY coordinates of the draggable element captured
			//in the getDragData method.
			getRepairXY: function() {
				return this.dragData.repairXY;
			}
		});
		temp.addToGroup('formControls');
	}
});
/**
 * Plugin that makes Fields within a Panel dragable
 * Any component that has the css class formControlButton (like all the fields have) will be reorderable in the form
 */
Ext.define('RS.formbuilder.plugins.PanelFieldDragZone', {

	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.panelfielddragzone',

	ddGroup: 'formControls',
	targetCls: '.formControlContainer',

	init: function(panel) {
		panel.on('render', this.onPanelRender, this, {
			single: true
		});
	},

	onPanelRender: function(panel) {
		var me = this;
		var proxyPanel;
		new Ext.dd.DragZone(panel.getEl(), {
			ddGroup: me.ddGroup,

			//Gets the drag data information by finding the drag targets based on their css class
			getDragData: function(e) {
				var sourceEl = e.getTarget(me.targetCls, 9);
				if (sourceEl && !Ext.fly(sourceEl).child('.formControlSectionContainerBody')) {
					var d = sourceEl.cloneNode(true);
					d.id = Ext.id();
					proxyPanel = new Ext.panel.Proxy(Ext.getCmp(sourceEl.id), {});
					return {
						ddel: d,
						sourceEl: sourceEl,
						proxyPanel: proxyPanel,
						repairXY: Ext.fly(sourceEl).getXY()
					}
				}
			},

			//Provide coordinates for the proxy to slide back to on failed drag.
			//This is the original XY coordinates of the draggable element captured
			//in the getDragData method.
			getRepairXY: function() {
				return this.dragData.repairXY;
			},
			afterInvalidDrop: function() {
				proxyPanel.hide();
				return false;
			}
		});
	}
});
/**
 * Plugin that sets up a DropZone for dragged items to be dropped on
 * Provides a mask to indicate where the component will be added when dropped on the panel
 */
Ext.define('RS.formbuilder.plugins.PanelFieldDropTarget', {

	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.panelfielddroptarget',

	ddGroup: 'formControls',
	targetCls: 'formControlContainer',

	init: function(panel) {
		panel.on('render', this.onPanelRender, this, {
			single: true
		});
	},

	onPanelRender: function(panel) {
		var me = this;
		new Ext.dd.DropTarget(panel.getEl(), {
			ddGroup: me.ddGroup,

			//Gets called continuously as the mouse moves, so as we get to a new node, we move the proxy to that new position
			notifyOver: function(dd, e, data) {
				var node = e.getTarget('div[class*=' + me.targetCls + ']');
				if (node) {
					try {
						data.proxyPanel.show();
					} catch (e) {}
					data.proxyPanel.moveProxy(node.parentNode, node.nextSibling);
					data.nextSibling = node.nextSibling;
				}
				return Ext.dd.DropZone.prototype.dropAllowed;
			},

			//Gets called when the item is dropped.  Calculates the indexes for start, end, or a new item and passes that to the
			//viewmodel for processing by firing the itemdropped event that the viewmodel is hooked into
			notifyDrop: function(source, e, data) {
				var i, controlsPanel = panel.items.getAt(1),
					dropIndex = controlsPanel ? controlsPanel.items.length : 0,
					itemLen = dropIndex,
					sourceIndex = -1,
					controlConfig;

				if (me.targetCls == 'formControlSectionContainer') {
					controlsPanel = panel
					dropIndex = panel.items.length
					itemLen = dropIndex
				}

				for (i = 0; i < itemLen; i++) {
					if (controlsPanel.items.items[i].id == data.sourceEl.id) sourceIndex = i;
					if (data.nextSibling && data.nextSibling.nextSibling && data.nextSibling.nextSibling.id == controlsPanel.items.items[i].id) {
						dropIndex = i;
					}
				}

				data.proxyPanel.hide();

				if (sourceIndex == -1) {
					controlConfig = Ext.getCmp(data.sourceEl.id).value;
				}

				if ((sourceIndex >= 0 || controlConfig) && dropIndex >= 0) panel.fireEvent('itemDropped', panel, dropIndex, sourceIndex, controlConfig);

				return true;
			}
		});
	}
});
