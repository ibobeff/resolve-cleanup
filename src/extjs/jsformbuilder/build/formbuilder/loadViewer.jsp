<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.resolve.util.JspUtils" %>
<%@page import="org.owasp.esapi.ESAPI"%>
<%
    String ver = ESAPI.encoder().encodeForURL(JspUtils.getResolveVersion());

    boolean debugForm = false;
    String d = request.getParameter("debugForm");
    if( d != null && d.indexOf("t") > -1){
        debugForm = true;
    }

    boolean mock = false;
    String m = request.getParameter("mock");
    if( m != null && m.indexOf("t") > -1){
        mock = true;
    }
%>

<%
    if( debugForm ){
%>
    <script type="text/javascript" src="/resolve/formbuilder/locale/locale_en.js?_v=<%=ver%>"></script>
    <script type="text/javascript" src="/resolve/formbuilder/js/formviewer-classes.js?_v=<%=ver%>"></script>
<%
    }else{
%>
    <script type="text/javascript" src="/resolve/formbuilder/locale/locale_en.js?_v=<%=ver%>"></script>
    <script type="text/javascript" src="/resolve/formbuilder/js/formviewer-classes.js?_v=<%=ver%>"></script>
<%
    }
%>