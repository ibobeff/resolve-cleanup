/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.syslog').locale = {
    windowTitle: 'SysLog Project',
    component: 'Component',
    severity: 'Severity',
    message: 'Message',
    type: 'Type',
    source: 'Source',
    userName: 'User Name',
    sysCreatedOn: 'Created On',
    sysCreatedBy: 'Created By',
    sysUpdatedOn: 'Updated On',
    sysUpdatedBy: 'Updated By',
    organizationName: 'Organization',
    sysId: 'Sys ID',
    logSummary: 'Log Summary',
    gateway: 'Gateway',
    ServerErr: 'Server Error',
    cancel: 'Back',
    AlertLog: {
        alertLogTitle: 'Alert Logs',
        deleteAlertLogRecs: 'Delete',
        displayName: 'Alert Log',
        deleteAlertMsg: 'Are you sure you want to delete the selected alert log',
        deleteAction: 'Delete',
        deleteTitle: 'Delete Alert Log Records',
        deletesAlertMsg: 'Are you sure you want to delete the selected alert log records?({len} records selected)',
        deleteFailureMsg: 'Cannot delete the alert log.'
    },
    AlertLogException: {
        alertLogTitle: 'Alert Log',
        back : 'Back'
    },
    AuditLog: {
        auditLogTitle: 'Audit Logs',
        getAuditLogError: 'Audit Log Load Error',
        getAuditLogErrMsg: 'Cannot get audit log',
        displayName: 'Audit Log',
        deleteAuditLogRecs: 'Delete',
        confirmLoadFailure: 'OK',
        deleteAuditLogRecsActionTitle: 'Delete Audit Logs',
        deleteAuditLogRecMsg: 'Are you sure you want to delete the selected item?',
        deleteAuditLogRecsMsg: 'Are you sure you want to delete the selected items?({len} items selected',
        deleteAction: 'Delete',
        deleteFailureMsg: 'Cannot delete audit log',
        cancelDelete: 'Cancel'
    },
    AuditLogMsg: {
        auditLogTitle: 'Audit Log',
        GetAuditLogErr: 'Cannot get audit log',
        back : 'Back'
    },
    ImpexLog: {
        impexLogTitle: 'Import/Export Logs',
        displayName: 'Import/Export Log',
        deleteImpexLogRecs: 'Delete',
        getImpexLogError: 'Load Import Export Log Error',
        getImpexLogErrMsg: 'Cannot get Import Export Log from server',
        confirmLoadFailure: 'OK',
        deletesImpexLogRecs: 'Are you sure you want to delete the selected items?({num} itmes selected)',
        deleteImpexLogRec: 'Are you sure you want to delete the selected item?',
        deleteTitle: 'Delete Import Export Log Records',
        deleteAction: 'Delete',
        deleteImpexLogRecsFailureMsg: 'Cannot delete implex log'
    },
    ImpexLogHistory: {
        impexLogTitle: 'Import/Export Log',
        GetImplexLogErr: 'Cannot get impex log.',
        back : 'Back'
    },
    GatewayLog: {
        gatewayLogTitle: 'Gateway Logs',
        getLogFailureTitle: 'Can Not Get Gateway Log',
        getLogFailureMsg: 'Can not get Gateway log from the server',
        confirmGetLogFailure: 'OK',
        deleteTitle: 'Delete Gateway Log Records',
        deleteRec: 'Are you sure you want to delete the selected record?',
        deleteRecs: 'Are you sure you want to delete the selected records? ({num} records selected)',
        deleteAction: 'Delete',
        deleteGatewayLogRecs: 'Delete',
        displayName: 'Gateway Log',
        deleteFailureMsg: 'Cannot delete gateway log',
        cancelDelete: 'Cancel'
    },
    GatewayLogMsg: {
        gatewayLogTitle: 'Gateway Log',
        GetGatewayLogErr: 'Cannot get gateway log',
        back : 'Back'
    }
}