/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/
glu.defModel('RS.syslog.AlertLog', {

	mock: false,
	state: 'init',
	displayName: '',
	stateId: 'syslogAlertLog',
	alertMsgs: {
		mtype: 'store',
		fields: ['ucomponent', 'useverity', 'umessage', 'utype'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'sysCreatedOn',
			direction: 'DESC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/alertlog/getAlertLog',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	columns: null,
	alertMsgsSelections: [],
	activate: function() {
		clientVM.setWindowTitle(this.localize('alertLogTitle'))
		this.alertMsgs.load({
			scope: this,
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	init: function() {
		if (this.mock) this.backend = RS.syslog.createMockBackend(true);
		var cols = RS.common.grid.getSysColumns();
		Ext.each(cols, function(col) {
			if (col.dataIndex == 'sysCreatedOn') {
				col.hidden = false;
				col.initialShow = true;
				col.width = 150;
			}			
		});
		this.set('columns', [{
			header: '~~component~~',
			dataIndex: 'ucomponent',
			filterable: true,
			width: 150
		}, {
			header: '~~severity~~',
			dataIndex: 'useverity',
			filterable: true,
			width: 150
		}, {
			header: '~~message~~',
			dataIndex: 'umessage',
			filterable: true,
			flex: 1,
			renderer: RS.common.grid.logRenderer(4)
		}, {
			header: '~~type~~',
			dataIndex: 'utype',
			filterable: true,
			width: 150
		}].concat(cols));
		this.set('state', 'waitGetAlertLogResponse');
		this.set('displayName', this.localize('displayName'));
	},

	deleteAlertLogRecs: function() {
		this.set('state', 'confirmDelete');
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.alertMsgsSelections.length == 1 ? 'deleteAlertMsg' : 'deletesAlertMsg', {
				len: this.alertMsgsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.sendDeleteRequest
		});
	},

	sendDeleteRequest: function(btn) {
		if (btn === 'no') {
			this.set('state', 'itemSelected');
			return;
		}
		var ids = '';
		for (var i = 0; i < this.alertMsgsSelections.length; i++) {
			ids += this.alertMsgsSelections[i].data.id + ',';
		}
		this.ajax({
			url: '/resolve/service/alertlog/deleteAlertLogRecs',
			params: {
				ids: ids
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					this.set('state', 'deleteFailure');
					clientVM.displayError(this.localize('deleteFailureMsg') + '[' + respData.message + ']');
				}
				this.alertMsgs.load({
					scope: this,
					callback: function() {
						this.set('state', 'ready');
					}
				});
				this.set('state', 'waitGetAlertLogResponse');
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
		this.set('state', 'waitDeleteResponse');
	},

	readExp: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.syslog.AlertLogException',
			params: {
				sys_id: id
			}

		});
		this.set('state', 'showExp');
	},

	deleteAlertLogRecsIsEnabled$: function() {
		return this.state === 'itemSelected'
	},

	when_state_changed: {
		on: ['stateChanged'],
		action: function() {
			if (this.state === 'ready' || this.state === 'deleteFailure') {
				this.set('alertMsgsSelections', []);
			}
		}
	},

	when_alertMsgSelection_changed: {
		on: ['alertMsgsSelectionsChanged'],
		action: function() {
			if (this.alertMsgsSelections.length > 0 && this.state != 'showExp')
				this.set('state', 'itemSelected');
			if (this.alertMsgsSelections.length == 0)
				if (this.state === 'itemSelected')
					this.set('state', 'ready');
		}
	}
});
glu.defModel('RS.syslog.AlertLogException', {
	state: '',
	alertLogTitle: '',

	userDateFormat: null,
	umessage: '',
	sys_id: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	organizationName: '',

	defaultData: {
		umessage: '',
		sys_id: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		organizationName: ''
	},

	fields: ['umessage', 'sys_id', 'sysCreatedOn', 'sysCreatedBy', 'sysUpdatedOn', 'sysUpdatedBy', 'organizationName'],

	activate: function(screen, params) {
		window.document.title = this.localize('windowTitle')
		this.resetForm();
		this.set('state', 'ready');
		var sys_id = this.sys_id || '';
		this.set('sys_id', params && params.sys_id ? params.sys_id : sys_id);
		this.set('state', 'ready');
		if (this.sys_id != '') {
			this.loadRec();
			return;
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.syslog.createMockBackend(true);
		this.set('userDateFormat', clientVM.getUserDefaultDateFormat());
		this.set('alertLogTitle', this.localize('alertLogTitle'));
	},

	loadRec: function() {
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/alertlog/getAlertLogRec',
			params: {
				id: this.sys_id
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (respData.success)
					this.setData(respData.data);
				else
					clientVM.displayError(this.localize('GetAlertLogErr') + '[' + resp.status + ']', respData.message);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.syslog.AlertLog'
		});
	},

	resetForm: function() {
		this.loadData(this.defaultData);
	},

	refresh: function() {
		this.loadRec();
	},

	setData: function(data) {
		this.loadData(data);
	},

	refreshIsEnabled$: function() {
		return this.state != 'wait';
	}
});
glu.defModel('RS.syslog.AuditLog', {
	state: 'init',
	displayName: '',
	stateId: 'syslogAuditLog',
	auditLogRecs: {
		mtype: 'store',
		fields: ['usource', 'umessage', 'uuserName'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'sysCreatedOn',
			direction: 'DESC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/auditlog/getAuditLog',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	columns: null,
	auditLogRecsSelections: [],
	activate: function() {
		clientVM.setWindowTitle(this.localize('auditLogTitle'))
		this.loadRecs();
	},
	init: function() {
		if (this.mock) this.backend = RS.syslog.createMockBackend(true);
		var cols = RS.common.grid.getSysColumns();
		Ext.each(cols, function(col) {
			col.hidden = !(col.dataIndex == 'sysCreatedOn');
		});
		this.set('displayName', this.localize('displayName'));
		this.set('columns', [{
			header: '~~source~~',
			dataIndex: 'usource',
			filterable: true,
			width: 150
		}, {
			header: '~~message~~',
			dataIndex: 'umessage',
			filterable: true,
			flex: 1,
			renderer: RS.common.grid.logRenderer(4, true)
		}, {
			header: '~~userName~~',
			dataIndex: 'uuserName',
			filterable: true,
			width: 150
		}].concat(cols));
	},

	loadRecs: function() {
		this.auditLogRecs.load({
			scope: this,
			callback: function(records, op, success) {
				if (!success) {
					//clientVM.displayError(this.localize('getAuditLogErrMsg'));
					return;
				}
				this.set('state', 'ready');
				this.set('auditLogRecsSelections', []);

			}
		});

		this.set('state', 'waitGetAuditLogResp');
	},

	deleteAuditLogRecsIsEnabled$: function() {
		return this.state === 'itemSelected';
	},

	showMsg: function(id) {
		this.set('state', 'showMsg');
		clientVM.handleNavigation({
			modelName: 'RS.syslog.AuditLogMsg',
			params: {
				id: id
			}
		});
	},

	deleteAuditLogRecs: function() {
		this.set('state', 'confirmDelete');
		this.message({
			title: this.localize('deleteAuditLogRecsActionTitle'),
			msg: this.localize(this.auditLogRecsSelections.length == 1 ? 'deleteAuditLogRecMsg' : 'deleteAuditLogRecsMsg', {
				len: this.auditLogRecsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancelDelete')
			},
			scope: this,
			fn: this.sendDeleteReq
		});
	},

	sendDeleteReq: function(btn) {
		if (btn === 'no') {
			this.set('state', 'itemSelected');
			return;
		}
		this.set('state', 'waitDeleteResp');
		var ids = [];
		this.auditLogRecsSelections.forEach(function(log){
			ids.push(log.data.id);
		}) 

		var strIds = ids.join(',');
		this.ajax({
			url: '/resolve/service/auditlog/deleteAuditLogRecs',
			params: {
				ids: strIds
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('deleteFailureMsg') + '[' + respData.message + ']');
				}
				this.loadRecs();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},
	when_item_selected: {
		on: ['auditLogRecsSelectionsChanged'],
		action: function() {
			if (this.auditLogRecsSelections.length > 0)
				this.set('state', 'itemSelected');
			else
				this.set('state', 'ready');
		}
	}
});
glu.defModel('RS.syslog.AuditLogMsg', {
	auditLogTitle: '',
	state: '',

	id: '',
	umessage: '',
	sys_id: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	organizationName: '',
	userDateFormat: null,

	defaultData: {
		id: '',
		umessage: '',
		sys_id: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		organizationName: ''
	},
	fields: ['umessage', 'sys_id', 'sysCreatedOn', 'sysCreatedBy', 'sysUpdatedOn', 'sysUpdatedBy', 'organizationName'],
	activate: function(screen, params) {
		window.document.title = this.localize('windowTitle')
		this.resetForm();
		this.set('state', 'ready');
		var id = this.id || '';
		this.set('id', params && params.id ? params.id : id);
		this.set('state', 'ready');
		if (this.id != '') {
			this.loadRec();
			return;
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.syslog.createMockBackend(true);
		this.set('userDateFormat', clientVM.getUserDefaultDateFormat());
		this.set('auditLogTitle', this.localize('auditLogTitle'));
	},

	loadRec: function() {
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/auditlog/getAuditLogRec',
			params: {
				id: this.id
			},
			scope: this,
			success: function(resp) {
				this.set('state', 'ready');
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('GetAuditLogErr') + '[' + respData.message + ']');
					return;
				}
				var data = respData.data;
				this.loadData(data);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.syslog.AuditLog'
		});
	},

	resetForm: function() {
		this.loadData(this.defaultData);
	},

	refresh: function() {
		this.loadRec();
	},

	refreshIsEnabled$: function() {
		return this.state != 'wait';
	}
});
glu.defModel('RS.syslog.GatewayLog', {
	state: 'init',
	displayName: '',
	stateId: 'syslogGatewayLog',
	gatewayLogRecs: {
		mtype: 'store',
		fields: ['useverity', 'ucomponent', 'ugateway', 'utype', 'umessage'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'sysCreatedOn',
			direction: 'DESC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/gatewaylog/getGatewayLog',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	gatewayLogRecsSelections: [],

	columns: null,

	activate: function() {
		clientVM.setWindowTitle(this.localize('gatewayLogTitle'))
	},

	init: function() {
		if (this.mock) this.backend = RS.syslog.createMockBackend(true);
		this.loadRecs();
		var cols = RS.common.grid.getSysColumns();
		Ext.each(cols, function(col) {
			col.hidden = !(col.dataIndex == 'sysCreatedOn');
		});
		this.set('displayName', this.localize('displayName'));
		this.set('columns', [{
			header: '~~component~~',
			dataIndex: 'ucomponent',
			filterable: true,
			flex: 1
		}, {
			header: '~~gateway~~',
			dataIndex: 'ugateway',
			filterable: true,
			flex: 1
		}, {
			header: '~~severity~~',
			dataIndex: 'useverity',
			filterable: true,
			flex: 1
		}, {
			header: '~~message~~',
			dataIndex: 'umessage',
			filterable: 1,
			flex: 5,
			renderer: RS.common.grid.logRenderer(4)
		}, {
			header: '~~type~~',
			dataIndex: 'utype',
			filterable: true,
			flex: 1
		}].concat(cols));
	},

	loadRecs: function() {
		this.set('state', 'waitGetGatewayLogResp');
		this.gatewayLogRecs.load({
			scope: this,
			callback: function(records, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('getLogFailureMsg'));
					return;
				}

				this.set('gatewayLogRecsSelections', []);
				this.set('state', 'ready');
			}
		})
	},

	showMsg: function(id) {
		this.set('state', 'showMsg');
		clientVM.handleNavigation({
			modelName: 'RS.syslog.GatewayLogMsg',
			params: {
				id: id
			}
		});
	},

	deleteGatewayLogRecs: function() {
		this.set('state', 'confirmDelete');
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.gatewayLogRecsSelections.length > 1 ? 'deleteRec' : 'deleteRecs', {
				num: this.gatewayLogRecsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancelDelete')
			},
			scope: this,
			fn: this.sendDeleteGatewayLogRecsReq
		});

	},

	sendDeleteGatewayLogRecsReq: function(btn) {
		if (btn === 'no') {
			this.set('state', 'itemSelected');
			return;
		}
		this.set('state', 'waitDeleteResp');
		var ids = [];
		this.gatewayLogRecsSelections.forEach(function(log){
			ids.push(log.data.id);
		})

		var strIds = ids.join(',');

		this.ajax({
			url: '/resolve/service/gatewaylog/deleteGatewayLogRecs',
			params: {
				ids: strIds
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('deleteFailureMsg') + '[' + respData.message + ']');
				}

				this.loadRecs();
			},

			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	deleteGatewayLogRecsIsEnabled$: function() {
		return this.state === 'itemSelected';
	},

	when_gatewayLogRecsSelection_changed: {
		on: ['gatewayLogRecsSelectionsChanged'],
		action: function() {
			if (this.gatewayLogRecsSelections.length > 0)
				this.set('state', 'itemSelected');
			else
				this.set('state', 'ready');
		}
	}
});
glu.defModel('RS.syslog.GatewayLogMsg', {
	gatewayLogTitle: '',
	state: '',

	id: '',
	umessage: '',
	sys_id: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	organizationName: '',
	userDateFormat: null,

	defaultData: {
		id: '',
		umessage: '',
		sys_id: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		organizationName: ''
	},
	fields: ['umessage', 'sys_id', 'sysCreatedOn', 'sysCreatedBy', 'sysUpdatedOn', 'sysUpdatedBy', 'organizationName'],

	activate: function(screen, params) {
		window.document.title = this.localize('windowTitle')
		this.resetForm();
		this.set('state', 'ready');
		var id = this.id || '';
		this.set('id', params && params.id ? params.id : id);
		this.set('state', 'ready');
		if (this.id != '') {
			this.loadRec();
			return;
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.syslog.createMockBackend(true);
		this.set('userDateFormat', clientVM.getUserDefaultDateFormat());
		this.set('gatewayLogTitle', this.localize('gatewayLogTitle'));

	},

	loadRec: function() {
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/gatewaylog/getGatewayLogRec',
			params: {
				id: this.id
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('GetGatewayLogErr') + '[' + respData.message + ']');
					return;
				}
				var data = respData.data;
				this.loadData(data);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.syslog.GatewayLog'
		});
	},

	resetForm: function() {
		this.loadData(this.defaultData);
	},

	refresh: function() {
		this.loadRec();
	},

	refreshIsEnabled$: function() {
		return this.state != 'wait';
	}
});
glu.defModel('RS.syslog.ImpexLog', {
    stateId: 'syslogImpexLog',
    state: 'init',
    displayName: '',
    impexLogRecs: {
        mtype: 'store',
        fields: ['utype', 'ulogSummary'].concat(RS.common.grid.getSysFields()),
        remoteSort: true,
        sorters: [{
            property: 'sysCreatedOn',
            direction: 'DESC'
        }],
        proxy: {
            type: 'ajax',
            url: '/resolve/service/impexlog/getImpexLog',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },

    columns: null,

    impexLogRecsSelections: [],

    activate: function() {
        clientVM.setWindowTitle(this.localize('impexLogTitle'))
        this.loadRecs();
    },

    init: function() {
        if (this.mock) this.backend = RS.syslog.createMockBackend(true);
        var cols = RS.common.grid.getSysColumns();
        Ext.each(cols, function(col) {
            col.hidden = !(col.dataIndex == 'sysCreatedOn');
        });
        this.set('displayName', this.localize('displayName'));
        this.set('columns', [{
            header: '~~type~~',
            dataIndex: 'utype',
            filterable: true,
            width: 70
        }, {
            header: '~~logSummary~~',
            dataIndex: 'ulogSummary',
            filterable: true,
            flex: 3,
            renderer: RS.common.grid.logRenderer(4)
        }].concat(cols));
    },

    loadRecs: function() {
        this.impexLogRecs.load({
            scope: this,
            callback: function(records, op, success) {
                if (!success) {
                    //clientVM.displayError(this.localize('getImpexLogErrMsg'));
                    return;
                }
                this.set('state', 'ready');
                this.set('impexLogRecsSelections', []);
            }
        });

        this.set('state', 'waitGetImpexLogResp');
    },

    deleteImpexLogRecsIsEnabled$: function() {
        return this.state === 'itemSelected';
    },

    showMsg: function(id) {
        this.set('state', 'showMsg');
        clientVM.handleNavigation({
            modelName: 'RS.syslog.ImpexLogHistory',
            params: {
                id: id
            }
        });
    },

    deleteImpexLogRecs: function() {
        this.set('state', 'confirmDelete');
        this.message({
            title: this.localize('deleteTitle'),
            msg: this.localize(this.impexLogRecsSelections.length == 1 ? 'deleteImpexLogRec' : 'deletesImpexLogRecs', {
                num: this.impexLogRecsSelections.length
            }),
            buttons: Ext.MessageBox.YESNO,
            buttonText: {
                yes: this.localize('deleteAction'),
                no: this.localize('cancel')
            },
            scope: this,
            fn: this.sendDeleteRequest
        })
    },

    sendDeleteRequest: function(btn) {
        if (btn === 'no') {
            this.set('state', 'itemSelected');
            return;
        }
        var ids = null;
        if (this.impexLogRecsSelections.length > 0)
            ids = this.impexLogRecsSelections[0].data.id;
        for (var i = 1; i < this.impexLogRecsSelections.length; i++) {
            ids += ',' + this.impexLogRecsSelections[i].data.id;
        }

        this.set('state', 'waitDeleteResp');

        this.ajax({
            url: '/resolve/service/impexlog/deleteImpexLogRecs',
            params: {
                ids: ids
            },
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    clientVM.displayError(this.localize('deleteImpexLogRecsFailureMsg') + '[' + respData.message + ']');

                }

                this.loadRecs();

            },

            failure: function(resp) {
                clientVM.displayFailure(resp);
            },
			callback: function() {
				this.set('state', 'ready');
			}
        })
    },

    when_implexLogRecsSelection_changed: {
        on: ['impexLogRecsSelectionsChanged'],
        action: function() {
            if (this.impexLogRecsSelections.length > 0)
                this.set('state', 'itemSelected');
            else
                this.set('state', 'ready');
        }
    }
});
glu.defModel('RS.syslog.ImpexLogHistory', {
	impexLogTitle: '',
	state: '',

	id: '',
	ulogHistory: '',
	sys_id: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	organizationName: '',
	userDateFormat: null,

	defaultData: {
		id: '',
		ulogHistory: '',
		sys_id: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		organizationName: ''
	},
	fields: ['ulogHistory', 'sys_id', 'sysCreatedOn', 'sysCreatedBy', 'sysUpdatedOn', 'sysUpdatedBy', 'organizationName'],

	activate: function(screen, params) {
		window.document.title = this.localize('windowTitle')
		this.resetForm();
		this.set('state', 'ready');
		var id = this.id || '';
		this.set('id', params && params.id ? params.id : id);
		this.set('state', 'ready');
		if (this.id != '') {
			this.loadRec();
			return;
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.syslog.createMockBackend(true);
		this.set('userDateFormat', clientVM.getUserDefaultDateFormat());
		this.set('impexLogTitle', this.localize('impexLogTitle'));
	},

	loadRec: function() {
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/impexlog/getImpexLogRec',
			params: {
				id: this.id
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('GetImplexLogErr') + '[' + respData.message + ']');
					return;
				}
				this.loadData(respData.data);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.syslog.ImpexLog'
		});
	},

	resetForm: function() {
		this.loadData(this.defaultData);
	},

	refresh: function() {
		this.loadRec();
	},

	refreshIsEnabled$: function() {
		return this.state != 'wait';
	}
});
glu.defView('RS.syslog.AlertLog', {
	padding: 15,
	xtype: 'grid',
	cls : 'rs-grid-dark',
	name: 'alertMsgs',
	stateId: '@{stateId}',
	displayName: '@{displayName}',
	columns: '@{columns}',
	store: '@{alertMsgs}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['deleteAlertLogRecs']
	}],
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',	
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},
	listeners: {
		editAction: '@{readExp}'
	}
});
glu.defView('RS.syslog.AlertLogException', {
	padding: 15,
	layout: 'fit',
	dockedItems: [{
		xtype: 'toolbar',		
		ui: 'display-toolbar',
		cls: 'actionBar rs-dockedtoolbar',
		margin : '0 0 15 0',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{alertLogTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',	
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',
			cls : 'rs-small-btn rs-btn-light'	
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{sys_id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{organizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	items: [{
		xtype: 'textarea',
		readOnly: true,
		value: '@{umessage}'
	}]
});
glu.defView('RS.syslog.AuditLog', {
	padding: 15,
	xtype: 'grid',
	cls : 'rs-grid-dark',
	name: 'auditLogRecs',
	columns: '@{columns}',
	stateId: '@{stateId}',
	store: '@{auditLogRecs}',
	displayName: '@{displayName}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['deleteAuditLogRecs']
	}],
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},
	listeners: {
		editAction: '@{showMsg}'
	}
});
glu.defView('RS.syslog.AuditLogMsg', {
	padding: 15,
	layout: 'fit',
	dockedItems: [{
		xtype: 'toolbar',	
		ui: 'display-toolbar',
		cls: 'actionBar rs-dockedtoolbar',
		margin : '0 0 15 0',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{auditLogTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',		
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',
			cls : 'rs-small-btn rs-btn-light'	
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{sys_id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{organizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	items: [{
		xtype: 'textarea',
		readOnly: true,
		value: '@{umessage}'
	}]
});
glu.defView('RS.syslog.GatewayLog', {
	padding: 15,
	cls : 'rs-grid-dark',
	xtype: 'grid',
	layout: 'fit',
	name: 'gatewayLogRecs',
	stateId: '@{stateId}',
	displayName: '@{displayName}',
	store: '@{gatewayLogRecs}',
	columns: '@{columns}',
	dockedItems: [{
		xtype: 'toolbar',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['deleteGatewayLogRecs']
	}],
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.editColumnTooltip,
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},
	listeners: {
		editAction: '@{showMsg}'
	}
});
glu.defView('RS.syslog.GatewayLogMsg', {
	padding: 15,
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls :'rs-dockedtoolbar',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{gatewayLogTitle}'
		}]
	}, {
		xtype: 'toolbar',	
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'		
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{sys_id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{organizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],

	layout: 'fit',

	items: [{
		xtype: 'textarea',
		readOnly: true,
		value: '@{umessage}'
	}]
});
glu.defView('RS.syslog.ImpexLog', {
	xtype: 'grid',
	padding: 15,
	cls : 'rs-grid-dark',
	displayName: '@{displayName}',
	name: 'impexLogRecs',
	stateId: '@{stateId}',
	columns: '@{columns}',
	store: '@{impexLogRecs}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['deleteImpexLogRecs']
	}],
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',	
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},

	listeners: {
		editAction: '@{showMsg}'
	}
});
glu.defView('RS.syslog.ImpexLogHistory', {
	padding: 15,
	layout: 'fit',
	dockedItems: [{
		xtype: 'toolbar',
		cls: 'actionBar rs-dockedtoolbar',
		margin : '0 0 15 0',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{impexLogTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'		
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{sys_id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{organizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	items: [{
		autoScroll: true,
		style : 'border-top:1px solid silver',
		padding : '8 0',
		html: '@{ulogHistory}',
		listeners: {
			afterrender: function(panel, eOpt) {
				clientVM.makeCtrlASelectAllOnView(panel);
			}
		}
	}]
});
/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.syslog').locale = {
    windowTitle: 'SysLog Project',
    component: 'Component',
    severity: 'Severity',
    message: 'Message',
    type: 'Type',
    source: 'Source',
    userName: 'User Name',
    sysCreatedOn: 'Created On',
    sysCreatedBy: 'Created By',
    sysUpdatedOn: 'Updated On',
    sysUpdatedBy: 'Updated By',
    organizationName: 'Organization',
    sysId: 'Sys ID',
    logSummary: 'Log Summary',
    gateway: 'Gateway',
    ServerErr: 'Server Error',
    cancel: 'Back',
    AlertLog: {
        alertLogTitle: 'Alert Logs',
        deleteAlertLogRecs: 'Delete',
        displayName: 'Alert Log',
        deleteAlertMsg: 'Are you sure you want to delete the selected alert log',
        deleteAction: 'Delete',
        deleteTitle: 'Delete Alert Log Records',
        deletesAlertMsg: 'Are you sure you want to delete the selected alert log records?({len} records selected)',
        deleteFailureMsg: 'Cannot delete the alert log.'
    },
    AlertLogException: {
        alertLogTitle: 'Alert Log',
        back : 'Back'
    },
    AuditLog: {
        auditLogTitle: 'Audit Logs',
        getAuditLogError: 'Audit Log Load Error',
        getAuditLogErrMsg: 'Cannot get audit log',
        displayName: 'Audit Log',
        deleteAuditLogRecs: 'Delete',
        confirmLoadFailure: 'OK',
        deleteAuditLogRecsActionTitle: 'Delete Audit Logs',
        deleteAuditLogRecMsg: 'Are you sure you want to delete the selected item?',
        deleteAuditLogRecsMsg: 'Are you sure you want to delete the selected items?({len} items selected',
        deleteAction: 'Delete',
        deleteFailureMsg: 'Cannot delete audit log',
        cancelDelete: 'Cancel'
    },
    AuditLogMsg: {
        auditLogTitle: 'Audit Log',
        GetAuditLogErr: 'Cannot get audit log',
        back : 'Back'
    },
    ImpexLog: {
        impexLogTitle: 'Import/Export Logs',
        displayName: 'Import/Export Log',
        deleteImpexLogRecs: 'Delete',
        getImpexLogError: 'Load Import Export Log Error',
        getImpexLogErrMsg: 'Cannot get Import Export Log from server',
        confirmLoadFailure: 'OK',
        deletesImpexLogRecs: 'Are you sure you want to delete the selected items?({num} itmes selected)',
        deleteImpexLogRec: 'Are you sure you want to delete the selected item?',
        deleteTitle: 'Delete Import Export Log Records',
        deleteAction: 'Delete',
        deleteImpexLogRecsFailureMsg: 'Cannot delete implex log'
    },
    ImpexLogHistory: {
        impexLogTitle: 'Import/Export Log',
        GetImplexLogErr: 'Cannot get impex log.',
        back : 'Back'
    },
    GatewayLog: {
        gatewayLogTitle: 'Gateway Logs',
        getLogFailureTitle: 'Can Not Get Gateway Log',
        getLogFailureMsg: 'Can not get Gateway log from the server',
        confirmGetLogFailure: 'OK',
        deleteTitle: 'Delete Gateway Log Records',
        deleteRec: 'Are you sure you want to delete the selected record?',
        deleteRecs: 'Are you sure you want to delete the selected records? ({num} records selected)',
        deleteAction: 'Delete',
        deleteGatewayLogRecs: 'Delete',
        displayName: 'Gateway Log',
        deleteFailureMsg: 'Cannot delete gateway log',
        cancelDelete: 'Cancel'
    },
    GatewayLogMsg: {
        gatewayLogTitle: 'Gateway Log',
        GetGatewayLogErr: 'Cannot get gateway log',
        back : 'Back'
    }
}
