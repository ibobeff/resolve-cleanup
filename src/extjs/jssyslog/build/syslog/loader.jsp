<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.resolve.util.JspUtils" %>
<%@page import="org.owasp.esapi.ESAPI"%>
<%
    String ver = ESAPI.encoder().encodeForURL(JspUtils.getResolveVersion());

    boolean debug = false;
    String d = request.getParameter("debug");
    if( d != null && d.indexOf("t") > -1){
        debug = true;
    }

    boolean debugSysLog = false;
    String dT = request.getParameter("debugSysLog");
    if( dT != null && dT.indexOf("t") > -1){
        debugSysLog = true;
    }

    boolean mock = false;
    String m = request.getParameter("mock");
    if( m != null && m.indexOf("t") > -1){
        mock = true;
    }
%>

<%
    if( debug || debugSysLog ){
%>
<script type="text/javascript" src="/resolve/syslog/js/syslog-classes.js?_v=<%=ver%>"></script>
<%
    }else{
%>
<script type="text/javascript" src="/resolve/syslog/js/syslog-classes.js?_v=<%=ver%>"></script>
<%
    }
%>

