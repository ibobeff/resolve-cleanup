glu.defModel('RS.syslog.AlertLogException', {
	state: '',
	alertLogTitle: '',

	userDateFormat: null,
	umessage: '',
	sys_id: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	organizationName: '',

	defaultData: {
		umessage: '',
		sys_id: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		organizationName: ''
	},

	fields: ['umessage', 'sys_id', 'sysCreatedOn', 'sysCreatedBy', 'sysUpdatedOn', 'sysUpdatedBy', 'organizationName'],

	activate: function(screen, params) {
		window.document.title = this.localize('windowTitle')
		this.resetForm();
		this.set('state', 'ready');
		var sys_id = this.sys_id || '';
		this.set('sys_id', params && params.sys_id ? params.sys_id : sys_id);
		this.set('state', 'ready');
		if (this.sys_id != '') {
			this.loadRec();
			return;
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.syslog.createMockBackend(true);
		this.set('userDateFormat', clientVM.getUserDefaultDateFormat());
		this.set('alertLogTitle', this.localize('alertLogTitle'));
	},

	loadRec: function() {
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/alertlog/getAlertLogRec',
			params: {
				id: this.sys_id
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (respData.success)
					this.setData(respData.data);
				else
					clientVM.displayError(this.localize('GetAlertLogErr') + '[' + resp.status + ']', respData.message);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.syslog.AlertLog'
		});
	},

	resetForm: function() {
		this.loadData(this.defaultData);
	},

	refresh: function() {
		this.loadRec();
	},

	setData: function(data) {
		this.loadData(data);
	},

	refreshIsEnabled$: function() {
		return this.state != 'wait';
	}
});