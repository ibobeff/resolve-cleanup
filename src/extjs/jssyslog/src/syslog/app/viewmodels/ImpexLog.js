glu.defModel('RS.syslog.ImpexLog', {
    stateId: 'syslogImpexLog',
    state: 'init',
    displayName: '',
    impexLogRecs: {
        mtype: 'store',
        fields: ['utype', 'ulogSummary'].concat(RS.common.grid.getSysFields()),
        remoteSort: true,
        sorters: [{
            property: 'sysCreatedOn',
            direction: 'DESC'
        }],
        proxy: {
            type: 'ajax',
            url: '/resolve/service/impexlog/getImpexLog',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },

    columns: null,

    impexLogRecsSelections: [],

    activate: function() {
        clientVM.setWindowTitle(this.localize('impexLogTitle'))
        this.loadRecs();
    },

    init: function() {
        if (this.mock) this.backend = RS.syslog.createMockBackend(true);
        var cols = RS.common.grid.getSysColumns();
        Ext.each(cols, function(col) {
            col.hidden = !(col.dataIndex == 'sysCreatedOn');
        });
        this.set('displayName', this.localize('displayName'));
        this.set('columns', [{
            header: '~~type~~',
            dataIndex: 'utype',
            filterable: true,
            width: 70
        }, {
            header: '~~logSummary~~',
            dataIndex: 'ulogSummary',
            filterable: true,
            flex: 3,
            renderer: RS.common.grid.logRenderer(4)
        }].concat(cols));
    },

    loadRecs: function() {
        this.impexLogRecs.load({
            scope: this,
            callback: function(records, op, success) {
                if (!success) {
                    //clientVM.displayError(this.localize('getImpexLogErrMsg'));
                    return;
                }
                this.set('state', 'ready');
                this.set('impexLogRecsSelections', []);
            }
        });

        this.set('state', 'waitGetImpexLogResp');
    },

    deleteImpexLogRecsIsEnabled$: function() {
        return this.state === 'itemSelected';
    },

    showMsg: function(id) {
        this.set('state', 'showMsg');
        clientVM.handleNavigation({
            modelName: 'RS.syslog.ImpexLogHistory',
            params: {
                id: id
            }
        });
    },

    deleteImpexLogRecs: function() {
        this.set('state', 'confirmDelete');
        this.message({
            title: this.localize('deleteTitle'),
            msg: this.localize(this.impexLogRecsSelections.length == 1 ? 'deleteImpexLogRec' : 'deletesImpexLogRecs', {
                num: this.impexLogRecsSelections.length
            }),
            buttons: Ext.MessageBox.YESNO,
            buttonText: {
                yes: this.localize('deleteAction'),
                no: this.localize('cancel')
            },
            scope: this,
            fn: this.sendDeleteRequest
        })
    },

    sendDeleteRequest: function(btn) {
        if (btn === 'no') {
            this.set('state', 'itemSelected');
            return;
        }
        var ids = null;
        if (this.impexLogRecsSelections.length > 0)
            ids = this.impexLogRecsSelections[0].data.id;
        for (var i = 1; i < this.impexLogRecsSelections.length; i++) {
            ids += ',' + this.impexLogRecsSelections[i].data.id;
        }

        this.set('state', 'waitDeleteResp');

        this.ajax({
            url: '/resolve/service/impexlog/deleteImpexLogRecs',
            params: {
                ids: ids
            },
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    clientVM.displayError(this.localize('deleteImpexLogRecsFailureMsg') + '[' + respData.message + ']');

                }

                this.loadRecs();

            },

            failure: function(resp) {
                clientVM.displayFailure(resp);
            },
			callback: function() {
				this.set('state', 'ready');
			}
        })
    },

    when_implexLogRecsSelection_changed: {
        on: ['impexLogRecsSelectionsChanged'],
        action: function() {
            if (this.impexLogRecsSelections.length > 0)
                this.set('state', 'itemSelected');
            else
                this.set('state', 'ready');
        }
    }
});