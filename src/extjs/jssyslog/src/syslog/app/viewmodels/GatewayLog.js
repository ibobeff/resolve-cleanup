glu.defModel('RS.syslog.GatewayLog', {
	state: 'init',
	displayName: '',
	stateId: 'syslogGatewayLog',
	gatewayLogRecs: {
		mtype: 'store',
		fields: ['useverity', 'ucomponent', 'ugateway', 'utype', 'umessage'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'sysCreatedOn',
			direction: 'DESC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/gatewaylog/getGatewayLog',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	gatewayLogRecsSelections: [],

	columns: null,

	activate: function() {
		clientVM.setWindowTitle(this.localize('gatewayLogTitle'))
	},

	init: function() {
		if (this.mock) this.backend = RS.syslog.createMockBackend(true);
		this.loadRecs();
		var cols = RS.common.grid.getSysColumns();
		Ext.each(cols, function(col) {
			col.hidden = !(col.dataIndex == 'sysCreatedOn');
		});
		this.set('displayName', this.localize('displayName'));
		this.set('columns', [{
			header: '~~component~~',
			dataIndex: 'ucomponent',
			filterable: true,
			flex: 1
		}, {
			header: '~~gateway~~',
			dataIndex: 'ugateway',
			filterable: true,
			flex: 1
		}, {
			header: '~~severity~~',
			dataIndex: 'useverity',
			filterable: true,
			flex: 1
		}, {
			header: '~~message~~',
			dataIndex: 'umessage',
			filterable: 1,
			flex: 5,
			renderer: RS.common.grid.logRenderer(4)
		}, {
			header: '~~type~~',
			dataIndex: 'utype',
			filterable: true,
			flex: 1
		}].concat(cols));
	},

	loadRecs: function() {
		this.set('state', 'waitGetGatewayLogResp');
		this.gatewayLogRecs.load({
			scope: this,
			callback: function(records, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('getLogFailureMsg'));
					return;
				}

				this.set('gatewayLogRecsSelections', []);
				this.set('state', 'ready');
			}
		})
	},

	showMsg: function(id) {
		this.set('state', 'showMsg');
		clientVM.handleNavigation({
			modelName: 'RS.syslog.GatewayLogMsg',
			params: {
				id: id
			}
		});
	},

	deleteGatewayLogRecs: function() {
		this.set('state', 'confirmDelete');
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.gatewayLogRecsSelections.length > 1 ? 'deleteRec' : 'deleteRecs', {
				num: this.gatewayLogRecsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancelDelete')
			},
			scope: this,
			fn: this.sendDeleteGatewayLogRecsReq
		});

	},

	sendDeleteGatewayLogRecsReq: function(btn) {
		if (btn === 'no') {
			this.set('state', 'itemSelected');
			return;
		}
		this.set('state', 'waitDeleteResp');
		var ids = [];
		this.gatewayLogRecsSelections.forEach(function(log){
			ids.push(log.data.id);
		})

		var strIds = ids.join(',');

		this.ajax({
			url: '/resolve/service/gatewaylog/deleteGatewayLogRecs',
			params: {
				ids: strIds
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('deleteFailureMsg') + '[' + respData.message + ']');
				}

				this.loadRecs();
			},

			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	deleteGatewayLogRecsIsEnabled$: function() {
		return this.state === 'itemSelected';
	},

	when_gatewayLogRecsSelection_changed: {
		on: ['gatewayLogRecsSelectionsChanged'],
		action: function() {
			if (this.gatewayLogRecsSelections.length > 0)
				this.set('state', 'itemSelected');
			else
				this.set('state', 'ready');
		}
	}
});