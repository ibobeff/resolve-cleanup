glu.defModel('RS.syslog.AuditLog', {
	state: 'init',
	displayName: '',
	stateId: 'syslogAuditLog',
	auditLogRecs: {
		mtype: 'store',
		fields: ['usource', 'umessage', 'uuserName'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'sysCreatedOn',
			direction: 'DESC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/auditlog/getAuditLog',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	columns: null,
	auditLogRecsSelections: [],
	activate: function() {
		clientVM.setWindowTitle(this.localize('auditLogTitle'))
		this.loadRecs();
	},
	init: function() {
		if (this.mock) this.backend = RS.syslog.createMockBackend(true);
		var cols = RS.common.grid.getSysColumns();
		Ext.each(cols, function(col) {
			col.hidden = !(col.dataIndex == 'sysCreatedOn');
		});
		this.set('displayName', this.localize('displayName'));
		this.set('columns', [{
			header: '~~source~~',
			dataIndex: 'usource',
			filterable: true,
			width: 150
		}, {
			header: '~~message~~',
			dataIndex: 'umessage',
			filterable: true,
			flex: 1,
			renderer: RS.common.grid.logRenderer(4, true)
		}, {
			header: '~~userName~~',
			dataIndex: 'uuserName',
			filterable: true,
			width: 150
		}].concat(cols));
	},

	loadRecs: function() {
		this.auditLogRecs.load({
			scope: this,
			callback: function(records, op, success) {
				if (!success) {
					//clientVM.displayError(this.localize('getAuditLogErrMsg'));
					return;
				}
				this.set('state', 'ready');
				this.set('auditLogRecsSelections', []);

			}
		});

		this.set('state', 'waitGetAuditLogResp');
	},

	deleteAuditLogRecsIsEnabled$: function() {
		return this.state === 'itemSelected';
	},

	showMsg: function(id) {
		this.set('state', 'showMsg');
		clientVM.handleNavigation({
			modelName: 'RS.syslog.AuditLogMsg',
			params: {
				id: id
			}
		});
	},

	deleteAuditLogRecs: function() {
		this.set('state', 'confirmDelete');
		this.message({
			title: this.localize('deleteAuditLogRecsActionTitle'),
			msg: this.localize(this.auditLogRecsSelections.length == 1 ? 'deleteAuditLogRecMsg' : 'deleteAuditLogRecsMsg', {
				len: this.auditLogRecsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancelDelete')
			},
			scope: this,
			fn: this.sendDeleteReq
		});
	},

	sendDeleteReq: function(btn) {
		if (btn === 'no') {
			this.set('state', 'itemSelected');
			return;
		}
		this.set('state', 'waitDeleteResp');
		var ids = [];
		this.auditLogRecsSelections.forEach(function(log){
			ids.push(log.data.id);
		}) 

		var strIds = ids.join(',');
		this.ajax({
			url: '/resolve/service/auditlog/deleteAuditLogRecs',
			params: {
				ids: strIds
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('deleteFailureMsg') + '[' + respData.message + ']');
				}
				this.loadRecs();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},
	when_item_selected: {
		on: ['auditLogRecsSelectionsChanged'],
		action: function() {
			if (this.auditLogRecsSelections.length > 0)
				this.set('state', 'itemSelected');
			else
				this.set('state', 'ready');
		}
	}
});