glu.defModel('RS.syslog.AlertLog', {

	mock: false,
	state: 'init',
	displayName: '',
	stateId: 'syslogAlertLog',
	alertMsgs: {
		mtype: 'store',
		fields: ['ucomponent', 'useverity', 'umessage', 'utype'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'sysCreatedOn',
			direction: 'DESC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/alertlog/getAlertLog',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	columns: null,
	alertMsgsSelections: [],
	activate: function() {
		clientVM.setWindowTitle(this.localize('alertLogTitle'))
		this.alertMsgs.load({
			scope: this,
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	init: function() {
		if (this.mock) this.backend = RS.syslog.createMockBackend(true);
		var cols = RS.common.grid.getSysColumns();
		Ext.each(cols, function(col) {
			if (col.dataIndex == 'sysCreatedOn') {
				col.hidden = false;
				col.initialShow = true;
				col.width = 150;
			}			
		});
		this.set('columns', [{
			header: '~~component~~',
			dataIndex: 'ucomponent',
			filterable: true,
			width: 150
		}, {
			header: '~~severity~~',
			dataIndex: 'useverity',
			filterable: true,
			width: 150
		}, {
			header: '~~message~~',
			dataIndex: 'umessage',
			filterable: true,
			flex: 1,
			renderer: RS.common.grid.logRenderer(4)
		}, {
			header: '~~type~~',
			dataIndex: 'utype',
			filterable: true,
			width: 150
		}].concat(cols));
		this.set('state', 'waitGetAlertLogResponse');
		this.set('displayName', this.localize('displayName'));
	},

	deleteAlertLogRecs: function() {
		this.set('state', 'confirmDelete');
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.alertMsgsSelections.length == 1 ? 'deleteAlertMsg' : 'deletesAlertMsg', {
				len: this.alertMsgsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.sendDeleteRequest
		});
	},

	sendDeleteRequest: function(btn) {
		if (btn === 'no') {
			this.set('state', 'itemSelected');
			return;
		}
		var ids = '';
		for (var i = 0; i < this.alertMsgsSelections.length; i++) {
			ids += this.alertMsgsSelections[i].data.id + ',';
		}
		this.ajax({
			url: '/resolve/service/alertlog/deleteAlertLogRecs',
			params: {
				ids: ids
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					this.set('state', 'deleteFailure');
					clientVM.displayError(this.localize('deleteFailureMsg') + '[' + respData.message + ']');
				}
				this.alertMsgs.load({
					scope: this,
					callback: function() {
						this.set('state', 'ready');
					}
				});
				this.set('state', 'waitGetAlertLogResponse');
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
		this.set('state', 'waitDeleteResponse');
	},

	readExp: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.syslog.AlertLogException',
			params: {
				sys_id: id
			}

		});
		this.set('state', 'showExp');
	},

	deleteAlertLogRecsIsEnabled$: function() {
		return this.state === 'itemSelected'
	},

	when_state_changed: {
		on: ['stateChanged'],
		action: function() {
			if (this.state === 'ready' || this.state === 'deleteFailure') {
				this.set('alertMsgsSelections', []);
			}
		}
	},

	when_alertMsgSelection_changed: {
		on: ['alertMsgsSelectionsChanged'],
		action: function() {
			if (this.alertMsgsSelections.length > 0 && this.state != 'showExp')
				this.set('state', 'itemSelected');
			if (this.alertMsgsSelections.length == 0)
				if (this.state === 'itemSelected')
					this.set('state', 'ready');
		}
	}
});