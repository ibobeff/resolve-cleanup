glu.defModel('RS.syslog.ImpexLogHistory', {
	impexLogTitle: '',
	state: '',

	id: '',
	ulogHistory: '',
	sys_id: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	organizationName: '',
	userDateFormat: null,

	defaultData: {
		id: '',
		ulogHistory: '',
		sys_id: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		organizationName: ''
	},
	fields: ['ulogHistory', 'sys_id', 'sysCreatedOn', 'sysCreatedBy', 'sysUpdatedOn', 'sysUpdatedBy', 'organizationName'],

	activate: function(screen, params) {
		window.document.title = this.localize('windowTitle')
		this.resetForm();
		this.set('state', 'ready');
		var id = this.id || '';
		this.set('id', params && params.id ? params.id : id);
		this.set('state', 'ready');
		if (this.id != '') {
			this.loadRec();
			return;
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.syslog.createMockBackend(true);
		this.set('userDateFormat', clientVM.getUserDefaultDateFormat());
		this.set('impexLogTitle', this.localize('impexLogTitle'));
	},

	loadRec: function() {
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/impexlog/getImpexLogRec',
			params: {
				id: this.id
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('GetImplexLogErr') + '[' + respData.message + ']');
					return;
				}
				this.loadData(respData.data);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.syslog.ImpexLog'
		});
	},

	resetForm: function() {
		this.loadData(this.defaultData);
	},

	refresh: function() {
		this.loadRec();
	},

	refreshIsEnabled$: function() {
		return this.state != 'wait';
	}
});