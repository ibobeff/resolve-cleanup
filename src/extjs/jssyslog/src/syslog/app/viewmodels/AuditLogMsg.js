glu.defModel('RS.syslog.AuditLogMsg', {
	auditLogTitle: '',
	state: '',

	id: '',
	umessage: '',
	sys_id: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	organizationName: '',
	userDateFormat: null,

	defaultData: {
		id: '',
		umessage: '',
		sys_id: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		organizationName: ''
	},
	fields: ['umessage', 'sys_id', 'sysCreatedOn', 'sysCreatedBy', 'sysUpdatedOn', 'sysUpdatedBy', 'organizationName'],
	activate: function(screen, params) {
		window.document.title = this.localize('windowTitle')
		this.resetForm();
		this.set('state', 'ready');
		var id = this.id || '';
		this.set('id', params && params.id ? params.id : id);
		this.set('state', 'ready');
		if (this.id != '') {
			this.loadRec();
			return;
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.syslog.createMockBackend(true);
		this.set('userDateFormat', clientVM.getUserDefaultDateFormat());
		this.set('auditLogTitle', this.localize('auditLogTitle'));
	},

	loadRec: function() {
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/auditlog/getAuditLogRec',
			params: {
				id: this.id
			},
			scope: this,
			success: function(resp) {
				this.set('state', 'ready');
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('GetAuditLogErr') + '[' + respData.message + ']');
					return;
				}
				var data = respData.data;
				this.loadData(data);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.syslog.AuditLog'
		});
	},

	resetForm: function() {
		this.loadData(this.defaultData);
	},

	refresh: function() {
		this.loadRec();
	},

	refreshIsEnabled$: function() {
		return this.state != 'wait';
	}
});