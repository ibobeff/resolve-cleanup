glu.defView('RS.syslog.ImpexLog', {
	xtype: 'grid',
	padding: 15,
	cls : 'rs-grid-dark',
	displayName: '@{displayName}',
	name: 'impexLogRecs',
	stateId: '@{stateId}',
	columns: '@{columns}',
	store: '@{impexLogRecs}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['deleteImpexLogRecs']
	}],
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',	
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},

	listeners: {
		editAction: '@{showMsg}'
	}
});