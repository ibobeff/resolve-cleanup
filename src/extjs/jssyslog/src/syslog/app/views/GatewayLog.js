glu.defView('RS.syslog.GatewayLog', {
	padding: 15,
	cls : 'rs-grid-dark',
	xtype: 'grid',
	layout: 'fit',
	name: 'gatewayLogRecs',
	stateId: '@{stateId}',
	displayName: '@{displayName}',
	store: '@{gatewayLogRecs}',
	columns: '@{columns}',
	dockedItems: [{
		xtype: 'toolbar',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['deleteGatewayLogRecs']
	}],
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.editColumnTooltip,
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},
	listeners: {
		editAction: '@{showMsg}'
	}
});