glu.defView('RS.syslog.ImpexLogHistory', {
	padding: 15,
	layout: 'fit',
	dockedItems: [{
		xtype: 'toolbar',
		cls: 'actionBar rs-dockedtoolbar',
		margin : '0 0 15 0',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{impexLogTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'		
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{sys_id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{organizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	items: [{
		autoScroll: true,
		style : 'border-top:1px solid silver',
		padding : '8 0',
		html: '@{ulogHistory}',
		listeners: {
			afterrender: function(panel, eOpt) {
				clientVM.makeCtrlASelectAllOnView(panel);
			}
		}
	}]
});