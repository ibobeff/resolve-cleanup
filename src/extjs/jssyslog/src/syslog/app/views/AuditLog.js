glu.defView('RS.syslog.AuditLog', {
	padding: 15,
	xtype: 'grid',
	cls : 'rs-grid-dark',
	name: 'auditLogRecs',
	columns: '@{columns}',
	stateId: '@{stateId}',
	store: '@{auditLogRecs}',
	displayName: '@{displayName}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['deleteAuditLogRecs']
	}],
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},
	listeners: {
		editAction: '@{showMsg}'
	}
});