glu.defView('RS.syslog.AlertLog', {
	padding: 15,
	xtype: 'grid',
	cls : 'rs-grid-dark',
	name: 'alertMsgs',
	stateId: '@{stateId}',
	displayName: '@{displayName}',
	columns: '@{columns}',
	store: '@{alertMsgs}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['deleteAlertLogRecs']
	}],
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',	
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},
	listeners: {
		editAction: '@{readExp}'
	}
});