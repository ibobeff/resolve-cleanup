glu.defView('RS.syslog.AuditLogMsg', {
	padding: 15,
	layout: 'fit',
	dockedItems: [{
		xtype: 'toolbar',	
		ui: 'display-toolbar',
		cls: 'actionBar rs-dockedtoolbar',
		margin : '0 0 15 0',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{auditLogTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',		
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',
			cls : 'rs-small-btn rs-btn-light'	
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{sys_id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{organizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	items: [{
		xtype: 'textarea',
		readOnly: true,
		value: '@{umessage}'
	}]
});