/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.metric').locale = {
	windowTitle: 'Metric Project',

	//Fields
	uname: 'Name',
	ugroup: 'Group',
	utype: 'Type',
	ualertStatus: 'Alert Level',
	uhigh: 'High',
	ulow: 'Low',
	ucomponenttype: 'Component',
	uipaddress: 'Address',
	definitionsDisplayName: 'Metric Definitions',
	select: 'Select',
	/* Server Component Grid */
	uipaddress: 'Address',
	ustatus: 'Status',
	uversion: 'Resolve Version',
	uupdatetime: 'Active Since',
	uguid: 'GUID',

	Definitions: {

		//Actions
		createDefinition: 'New',
		deleteDefinition: 'Delete',
		deleteTitle: 'Delete Metric Defintions',
		deletesMessage: 'Are you sure you want to delete the selected definitions?<center>({len} properties selected)</center>',
		deleteMessage: 'Are you sure you want to delete the selected definition?',
		cancel: 'Cancel',

		Default: 'default',
		DecisionTree: 'decision tree',

		save: 'Save'
	},

	Definition: {
		editDefinition: 'Edit Definition',
		newDefinition: 'New Definition',

		//Actions
		save: 'Save',
		saveAndDeploy: 'Save & Deploy',
		cancel: 'Cancel',
		saveSuccess: 'Metric definition has been saved.',
		saveFailure: 'Failed to save the metric definition',
		saveTitle: 'Save Metric Definition',
		ok: 'OK',
		saveError: 'Error saving the metric definition',

		//Validations
		valueIsInvalid: 'Please provide a value between 0 and 1000000',
		typeIsInvalid: 'Please provide a type from the drop down box',
		nameInvalid: 'Please provide a name for the definition'

	},

	Servers: {
		serversDisplayName: 'Metric Servers',
		deployToServer: 'Deploy',
		defaultThresholds: 'Default Thresholds'
	},

	Server: {
		//Actions
		serverDeployMetricsTitle: 'Deploy Metrics',
		serverDeployStatus: 'Deploy Status',
		serverDeployStatusMessage: 'Deployed to server successfully.',
		serverDeployError: 'Deploy Status',
		serverDeployErrorMessage: 'There are no records selected to deploy.',

		deploy: 'Deploy',
		cancel: 'Cancel',

		//Validations
		deployInvalid: 'Please select a metric to be deployed',
		ajaxErrorTitle: 'Backend Server Error',
		confirm: 'OK'
	},

	ServerComponent: {
		//Actions
		deploy: 'Deploy',
		cancel: 'Cancel',
		componentsDisplayName: 'Server Components',

		serverDeployStatus: 'Deploy Status',
		serverDeploySuccessMessage: 'Deployed to server successfully.',
		serverDeployErrorMessage: 'There are no records selected to deploy.',
		serverDeployMetricsTitle: 'Deploy Metrics',

		// Error messages when server cannot respond with a lidt of active components(RSCONTROL, RSVIEW)
		componentErrorTitle: 'Server Component Error',
		componentErrorMsg: 'The server currently cannot handle the request.',
		confirm: 'OK',

		//Validations
		deployInvalid: 'Please select a metric to be deployed'
	}
}