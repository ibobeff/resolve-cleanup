/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/
Ext.define('RS.metric.model.Definitions', {
	extend: 'Ext.data.Model',
	idProperty: 'id',
	fields: ['id', 'uname', 'ugroup', 'utype', 'ucomponenttype', 'uipaddress', 'uguid', 'uhigh', 'ulow', 'ualertStatus'],
	proxy: {
		type: 'ajax',
		url: '/resolve/service/metric/list',
		reader: {
			type: 'json',
			root: 'records'
		},
		listeners: {
			exception: function(e, resp, op) {
				clientVM.displayExceptionError(e, resp, op);
			}
		}
	}
});
glu.defModel('RS.metric.Definition', {

	mock: false,

	waitingResponse: false,

	fields: ['id', 'uname', 'ugroup', 'utype', 'uhigh', 'ulow', 'uguid', 'ualertStatus'],
	id: '',
	uname: '',
	unameIsValid$: function() {
		return this.uname ? true : this.localize('nameInvalid')
	},
	ugroup: '',
	ugroupIsValid$: function() {
		return this.ugroup ? true : this.localize('groupInvalid')
	},
	utype: '',
	uhigh: 100,
	ulow: 0,
	uguid: '',
	percent$: function() {
		return this.uhigh + this.ulow
	},

	windowTitle$: function() {
		return this.id ? this.localize('editDefinition') : this.localize('newDefinition')
	},

	init: function() {
		if (this.mock) this.backend = RS.metric.createMockBackend(true)

		if (this.id) this.ajax({
			url: '/resolve/service/metric/get',
			params: {
				id: this.id
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) this.loadData(response.data)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	save: function() {
		var definitionToSave = {
			sys_id: this.id,
			UName: this.uname,
			UGroup: this.ugroup,
			UType: this.utype,
			UHigh: this.uhigh,
			ULow: this.ulow,
			UGuid: this.uguid,
			UAlertStatus: this.ualertStatus
		}
		this.set('waitingResponse', true);
		this.ajax({
			url: '/resolve/service/metric/save',
			jsonData: definitionToSave,
			scope: this,
			success: function(resp, opt) {
				this.showSaveMessage(resp.responseText)
				this.parentVM.definitions.load()
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('waitingResponse', false)
			}
		});
		this.doClose();
	},


	showSaveMessage: function(msg) {
		var state = 'saveSuccess';
		var responseObj = Ext.JSON.decode(msg);
		if (responseObj.message != null && responseObj.message.contains('saveError'))
			state = 'saveFailure';
		this.message({
			title: this.localize('saveTitle'),
			msg: this.localize(state),
			buttons: Ext.MessageBox.YES,
			buttonText: {
				yes: this.localize('ok')
			},
			scope: this
		})
	},

	getResponseData: function(response) {
		try {
			var data = Ext.decode(response.responseText);
		} catch (ex) {
			Ext.Error.raise({
				response: response,
				json: response.responseText,
				parseError: ex,
				msg: 'Unable to parse the JSON returned by the server: ' + ex.toString()
			});
		}

		return data;
	},

	saveIsEnabled$: function() {
		return this.isValid
	},

	saveAndDeploy: function() {
		//first save the record and the deploy it to the list of servers
		var definitionToSave = {
			sys_id: this.id,
			UName: this.uname,
			UGroup: this.ugroup,
			UType: this.utype,
			UHigh: this.uhigh,
			ULow: this.ulow,
			UGuid: this.uguid,
			UAlertStatus: this.ualertStatus
		}
		this.ajax({
			url: '/resolve/service/metric/save',
			jsonData: definitionToSave,
			success: function(resp, opt) {
				this.set('waitingResponse', false)
				this.parentVM.definitions.load()
				this.set('waitingResponse', false)
				this.doClose()
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
		this.set('waitingResponse', true);
		//call the deploying servers
		this.open({
			mtype: 'RS.metric.ServerComponent',
			definitionId: this.id
		})
	},

	saveAndDeployIsVisible$: function() {
		return this.id
	},

	saveAndDeployIsEnabled$: function() {
		return this.isValid
	},

	cancel: function() {
		this.doClose()
	},

	cancelIsEnabled$: function() {
		return true
	},

	ulowIsValid$: function() {
		if (this.ulow < 0) return this.localize('valueIsInvalid')
		if (this.ulow > 1000000) return this.localize('valueIsInvalid')
		if (!this.ulow && this.ulow !== 0) return this.localize('valueIsInvalid')
		return true
	},

	uhighIsValid$: function() {
		if (this.uhigh < 0) return this.localize('valueIsInvalid')
		if (this.uhigh > 1000000) return this.localize('valueIsInvalid')
		if (!this.uhigh && this.uhigh !== 0) return this.localize('valueIsInvalid')
		return true
	},

	utypeIsValid$: function() {
		if (!this.utype) return this.localize('typeIsInvalid')
		return true
	}
})
glu.defModel('RS.metric.Definitions', {

	mock: false,

	stateId: 'DefinitionsGrid',

	activate: function() {
		window.document.title = this.localize('windowTitle')
	},

	waitResponse: false,

	definitions: {
		mtype: 'store',
		model: 'RS.metric.model.Definitions'
	},

	definitionsSelections: [],

	typeStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},

	displayName: '~~definitionsDisplayName~~',

	columns: [{
		header: '~~uname~~',
		dataIndex: 'uname',
		flex: 1,
		filterable: true
	}, {
		header: '~~ugroup~~',
		dataIndex: 'ugroup',
		flex: 1,
		filterable: true
	}, {
		header: '~~uhigh~~',
		dataIndex: 'uhigh',
		flex: 1,
		filterable: true,
		editor: {
			xtype: 'numberfield',
			allowBlank: false
		}
	}, {
		header: '~~ulow~~',
		dataIndex: 'ulow',
		flex: 1,
		filterable: true,
		editor: {
			xtype: 'numberfield',
			allowBlank: false
		}
	}, {
		header: '~~utype~~',
		dataIndex: 'utype',
		flex: 1,
		filterable: true
	}, {
		header: '~~ualertStatus~~',
		dataIndex: 'ualertStatus',
		flex: 1,
		filterable: true
	}],

	init: function() {
		if (this.mock) this.backend = RS.metric.createMockBackend(true)
		this.set('displayName', this.localize('definitionsDisplayName'))
		this.definitions.load()


		//Load up the types
		this.typeStore.add([{
			name: this.localize('Default'),
			value: 'default'
		}, {
			name: this.localize('DecisionTree'),
			value: 'decisiontree'
		}])
	},

	//Common grid stuff
	allSelected: false,
	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	//Buttons
	editDefinition: function(id) {
		this.open({
			mtype: 'RS.metric.Definition',
			id: id
		})
	},

	deleteDefinition: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.definitionsSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', {
				len: this.definitionsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteDefinition'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteDefinition
		})
	},

	reallyDeleteDefinition: function(btn) {
		if (btn == 'yes') {
			var ids = [];
			for (var i = 0; i < this.definitionsSelections.length; i++) {
				ids.push(this.definitionsSelections[i].data.id);
			}
			strIds = ids.join(',')
			this.ajax({
				url: '/resolve/service/metric/delete',
				params: {
					ids: strIds
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				},
				callback: function(resp, opt) {
					this.set('definitionsSelections', [])
					var self = this;
					this.definitions.load({
						callback: function() {
							self.set('waitResponse', false)
						}
					});
				}
			})
			this.set('waitResponse', true)
		}
	},

	deleteDefinitionIsEnabled$: function() {
		return (this.definitionsSelections.length > 0 && !this.waitResponse)
	},

	save: function() {
		var changes, data;
		this.definitions.each(function(definition) {
			changes = definition.getChanges()
			if (!Ext.Object.isEmpty(changes)) {
				data = definition.getData()
				this.ajax({
					url: '/resolve/service/metric/save',
					jsonData: {
						sys_id: data.id,
						UName: data.uname,
						UGroup: data.ugroup,
						UType: data.utype,
						UHigh: data.uhigh,
						ULow: data.ulow,
						UGuid: data.uguid,
						UAlertStatus: data.ualertStatus
					},
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (!response.success) clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}, this)
		this.definitions.commitChanges()
	}
})
glu.defModel('RS.metric.Server', {
	mock: false,
	buttonVisibility: false,
	serversIds: '',

	//Common grid stuff
	allSelected: false,
	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	definitionsStore: {
		mtype: 'store',
		model: 'RS.metric.model.Definitions',
		remoteSort: true,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/metric/list',
			reader: {
				type: 'json',
				root: 'records'
			}
			/* handled below in handleLoadComponentsFailure()
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
			*/
		}
	},

	definitionsSelections: [],

	columns: [{
		header: '~~uname~~',
		dataIndex: 'uname',
		flex: 1,
		filterable: true
	}, {
		header: '~~ugroup~~',
		dataIndex: 'ugroup',
		flex: 1,
		filterable: true
	}, {
		header: '~~uhigh~~',
		dataIndex: 'uhigh',
		filterable: true,
		editor: {
			xtype: 'textfield'
			//allowBlank: false
		}
	}, {
		header: '~~ulow~~',
		dataIndex: 'ulow',
		filterable: true,
		editor: {
			xtype: 'textfield'
			//allowBlank: false
		}
	}, {
		header: '~~utype~~',
		dataIndex: 'utype',
		filterable: true
	}, {
		header: '~~ualertStatus~~',
		dataIndex: 'ualertStatus',
		flex: 1,
		filterable: true
	}],

	displayName: '~~definitionsDisplayName~~',
	windowTitle: '~~serverDeployMetricsTitle~~',

	init: function() {
		if (this.mock) this.backend = RS.metric.createMockBackend(true)
		this.set('displayName', this.localize('definitionsDisplayName'))
		this.set('windowTitle', this.localize('serverDeployMetricsTitle'))
		this.set('deployStatusName', this.localize('serverDeployStatus'))
		this.set('deployStatusError', this.localize('serverDeployError'))
		this.set('deployStatusMessage', this.localize('serverDeployStatusMessage'))
		this.set('deployErrorMessage', this.localize('serverDeployErrorMessage'))

		//Load the definitions from the server
		this.definitionsStore.load({
			scope: this,
			callback: function(records, op, suc) {
				if (!suc) {
					this.handleLoadComponentsFailure(op)
				}
			}
		})
	},

	handleLoadComponentsFailure: function(op) {
		this.message({
			title: this.localize('componentErrorTitle'),
			msg: op.error,
			buttons: Ext.MessageBox.YES,
			buttonText: {
				yes: this.localize('confirm')
			}
		})
	},

	cancel: function() {
		this.doClose()
	},

	cancelIsEnabled$: function() {
		return true
	},

	cancelIsVisible$: function() {
		return !this.buttonVisibility
	},

	persisting: false,

	deployStatusName: '~~serverDeployStatus~~',
	deployStatusError: '~~serverDeployError~~',
	deployStatusMessage: '~~serverDeployStatusMessage~~',
	deployErrorMessage: '~~serverDeployErrorMessage~~',

	serversSelections: [],

	deploy: function() { // deploy to the following components

		//Check if records are selected
		if (this.definitionsSelections.length > 0) //selections from the grid
		{
			/*
				//TEST : print the data you get of the selection 
				for(var i = 0; i < this.definitionsSelections.length; i++)
				{
					 alert(this.definitionsSelections[i].data["id"]);
				}
			*/
			/* Array of server components to deploy on */
			var guids = [];
			for (var i = 0; i < this.serversIds.length; i++) {
				guids.push(this.serversIds[i])
			}

			/* sys_ids of metric definition from Server */
			var metricids = [];
			var highs = [];
			var lows = [];
			for (var i = 0; i < this.definitionsSelections.length; i++) {
				metricids.push(this.definitionsSelections[i].data.id)
				highs.push(this.definitionsSelections[i].data.uhigh)
				lows.push(this.definitionsSelections[i].data.ulow)
			}

			this.ajax({
				url: '/resolve/service/metric/deploy', //call to the server to deploy records
				params: {
					metricDefSysIds: metricids,
					guids: guids,
					highs: highs,
					lows: lows
				},
				scope: this,
				success: function(serverReply) {
					var response = RS.common.parsePayload(serverReply)
					if (response.success) {
						// alert message that it has been deployed
						this.message({
							title: this.localize('serverDeployStatus'),
							msg: this.deployStatusMessage,
							buttons: Ext.MessageBox.YES,
							buttonText: {
								yes: this.localize('confirm')
							}
						})
						//Ext.Msg.alert(this.deployStatusName, this.deployStatusMessage);
						this.parentVM.updateDeployedServers()
					} else {
						clientVM.displayError(response.message) // message comes from server
					}
				},
				failure : function(resp){
					clientVM.displayFailure(resp);
				},
				callback: function(records, op, suc) {
					this.set('persisting', false)
					if (!suc) {
						this.handleLoadDeployListFailure(op)
					}
				}
			})

			this.set('persisting', true)
			/*
	    	this.ajax({
	    		url: '/resolve/service/metric/deployed/list', //call to the server to get a list of deployed sys_ids
	    		params: {
	    			sys_ids:this.serversIds
	    		},
	    		success: function(serverReply) {
	    			var response = RS.common.parsePayload(serverReply)
					if (response.success) {
						Ext.Array.forEach(response.records, function(record) {
							//Create an array of guids to be sent as parameter
							guids.push(record.uguid);
						})
						
						// sys_ids of metric definition from Server 
				    	var metricids = [];
				    	for (var i = 0; i < this.definitionsSelections.length; i++) {
							metricids.push(this.definitionsSelections[i].data.id)
				    	}
				    	
						this.ajax({
							url: '/resolve/service/metric/deploy', //call to the server to deploy records
					    	params: {
								metricDefSysIds: metricids,
								guids: guids
							},
							success: function(serverReply) {
								this.set('persisting', false)
								var response = RS.common.parsePayload(serverReply)
								if (response.success) {
									// alert message that it has been deployed
									this.message({			
										title: this.localize('serverDeployStatus'),
										msg: this.deployStatusMessage,
										buttons:Ext.MessageBox.YES,
										buttonText:{
											yes: this.localize('confirm')
										}
									})
									//Ext.Msg.alert(this.deployStatusName, this.deployStatusMessage);
									this.parentVM.updateDeployedServers()
								} 
								else 
								{ 
									clientVM.displayError(response.message) // message comes from server
								}
							}	
						})
						this.set('persisting', true)
					} 
				},
				
				
				callback: function(records, op, suc){
					if(!suc){ 
						this.handleLoadDeployListFailure(op)
					}
				}
	    	}) //end of ajax call
	    	*/

		} else {
			Ext.Msg.alert(this.deployStatusError, this.deployErrorMessage);
			this.set('persisting', false)
		}


		this.doClose()

	},

	handleLoadDeployListFailure: function(op) {
		this.message({
			title: this.localize('ajaxErrorTitle'),
			msg: op.error,
			buttons: Ext.MessageBox.YES,
			buttonText: {
				yes: this.localize('confirm')
			}
		})
	},

	deployIsEnabled$: function() {
		//disable deploy button when no records are selected
		return (this.definitionsSelections.length > 0) ? true : false
	},

	deployIsVisible$: function() {
		return !this.buttonVisibility
	}
})
glu.defModel('RS.metric.ServerComponent', {

	mock: false,
	name: 'components',
	displayName: '~~componentsDisplayName~~',
	windowTitle: '~~serverDeployMetricsTitle~~',

	definitionId: '',

	activate: function() {
		window.document.title = this.localize('windowTitle')
	},

	//Common grid stuff
	allSelected: false,
	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	componentStore: {
		mtype: 'store',
		remoteSort: true,
		fields: ['id', 'uipaddress', 'ustatus', 'utype', 'uversion', 'uguid', {
			name: 'uupdatetime',
			type: 'date',
			format: 'time',
			dateFormat: 'time'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/metric/component/list',
			reader: {
				type: 'json',
				root: 'records',
				messageProperty: 'message'
			}
			/* hanlded below in handleLoadComponentsFailure()
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
			*/
		}
	},

	componentsSelections: [], // the grid selection, the name should be same as the name of grid

	columns: [],

	init: function() {
		if (this.mock) {
			this.backend = RS.metric.createMockBackend(true)
		}
		this.set('displayName', this.localize('componentsDisplayName'))
		this.set('windowTitle', this.localize('serverDeployMetricsTitle'))
		this.set('deployStatusName', this.localize('serverDeployStatus'))
		this.set('deploySuccessMessage', this.localize('serverDeploySuccessMessage'))
		this.set('deployErrorMessage', this.localize('serverDeployErrorMessage'))

		/* Setting Columns here because outside the init() the this.clientVM is not instantiated yet */
		this.set('columns', [{
			header: '~~uipaddress~~',
			dataIndex: 'uipaddress',
			flex: 1,
			filterable: true
		}, {
			header: '~~ustatus~~',
			dataIndex: 'ustatus',
			flex: 1,
			filterable: true
		}, {
			header: '~~utype~~',
			dataIndex: 'utype',
			flex: 1,
			filterable: true
		}, {
			header: '~~uversion~~',
			dataIndex: 'uversion',
			filterable: true
		}, {
			header: '~~uupdatetime~~',
			dataIndex: 'uupdatetime',
			sortable: true,
			width: 200,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}])

		//Load the server components from the server
		this.componentStore.load({
			scope: this,
			callback: function(records, op, suc) {
				if (!suc) {
					this.handleLoadComponentsFailure(op)
				}
			}
		})
	},

	handleLoadComponentsFailure: function(op) {
		this.message({
			title: this.localize('componentErrorTitle'),
			msg: op.error,
			/*
			{
				var message = this.getResponseData(op.response).message
				if (message) {
					var messageDescription = 'componentErrorTitle';
					Ext.MessageBox.alert(messageDescription, message);
				}
				//this.localize(op.response.responseText),//'componentErrorMsg'),
			},	
			*/
			buttons: Ext.MessageBox.YES,
			buttonText: {
				yes: this.localize('confirm')
			}
		})
	},

	getResponseData: function(response) {
		try {
			var data = Ext.decode(response.responseText);
		} catch (ex) {
			Ext.Error.raise({
				response: response,
				json: response.responseText,
				parseError: ex,
				msg: 'Unable to parse the JSON returned by the server: ' + ex.toString()
			});
		}

		return data;
	},

	cancel: function() {
		this.doClose()
	},

	persisting: false,
	deployStatusName: '~~serverDeployStatus~~',
	deploySuccessMessage: '~~serverDeploySuccessMessage~~',
	deployErrorMessage: '~~serverDeployErrorMessage~~',


	deploy: function() {
		//Check if records are selected
		if (this.componentsSelections.length > 0) //selections from the grid
		{
			/*
				//TEST : print the data you get of the selection 
				for(var i = 0; i < this.componentsSelections.length; i++)
				{
					 alert(this.componentsSelections[i].data["id"]);
				}
			*/
			/* Array of server components to deploy on */
			var guids = [];
			for (var i = 0; i < this.componentsSelections.length; i++) {
				guids.push(this.componentsSelections[i].data.uguid);
			}

			/* sys_id of metric definition from Definition */
			var metricids = [];
			metricids.push(this.definitionId);

			//ajax call to deploy the metric definition
			this.ajax({
				url: '/resolve/service/metric/deploy', //call to the server to deploy records				
				params: {
					metricDefSysIds: metricids,
					guids: guids
				},
				scope: this,
				success: function(serverReply) {
					var response = RS.common.parsePayload(serverReply)
					if (response.success) {
						// alert message that it has been deployed
						//Ext.Msg.alert(this.deployStatusName, this.deploySuccessMessage);
						this.message({
							title: this.localize(this.deployStatusName),
							msg: this.deploySuccessMessage,
							buttons: Ext.MessageBox.YES,
							buttonText: {
								yes: this.localize('confirm')
							}
						})
					} else {
						clientVM.displayError(response.message) // this message comes from AjaxMetric's /metric/deploy
					}
				},
				failure : function(resp){
					clientVM.displayFailure(resp);
				},
				callback : function(){
					this.set('persisting', false)
				}
			})
			this.set('persisting', true)
		} else {
			Ext.Msg.alert(this.deployStatusError, this.deployErrorMessage);
			this.set('persisting', false)
		}
		this.doClose()
	},

	deployIsEnabled$: function() {
		//disable deploy button when no records are selected
		return (this.componentsSelections.length > 0) ? true : false
	},

	buttonVisibility: false,
	deployIsVisible$: function() {
		return !this.buttonVisibility
	}

})
glu.defModel('RS.metric.Servers', {

	mock: false,

	stateId: 'ServersGrid',

	waitResponse: false,

	activate: function() {
		window.document.title = this.localize('windowTitle')
	},

	displayName: '~~serversDisplayName~~',

	servers: {
		mtype: 'store',
		groupField: 'uipaddress',
		remoteSort: true,
		fields: ['id', 'uipaddress', 'ustatus', 'utype', 'uversion', 'uguid', {
			name: 'uupdatetime',
			type: 'date',
			format: 'time',
			dateFormat: 'time'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/metric/component/list',
			reader: {
				type: 'json',
				root: 'records',
				messageProperty: 'message'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	serversSelections: [],

	columns: [{
		header: '~~utype~~',
		dataIndex: 'utype',
		filterable: true,
		flex: 1
	}, {
		header: '~~uipaddress~~',
		dataIndex: 'uipaddress',
		filterable: true,
		flex: 1
	}, {
		header: '~~uguid~~',
		dataIndex: 'uguid',
		filterable: true,
		flex: 1
	}],

	init: function() {
		if (this.mock) this.backend = RS.metric.createMockBackend(true)
		this.set('displayName', this.localize('serversDisplayName'))
		this.servers.load()
	},

	//Common grid stuff
	allSelected: false,
	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	//Buttons
	deployToServer: function() {
		var serverIds = [];
		for (var i = 0; i < this.serversSelections.length; i++) {
			serverIds.push(this.serversSelections[i].data.uguid); // send a list of GUIDs that are available
		}
		this.open({
			mtype: 'RS.metric.Server',
			serversIds: serverIds
		})
	},

	defaultThresholds: function() {
		clientVM.handleNavigation({
			modelName: 'RS.metric.Definitions'
		})
	},

	deployToServerIsEnabled$: function() {
		return this.serversSelections.length > 0
	},

	updateDeployedServers: function() {
		this.servers.load({
			scope: this,
			callback: function() {
				this.set('waitResponse', false)
			}
		})
		this.set('waitResponse', true)
	},
	expandGridRow: function(expander, record, body, rowIndex) {
		debugger
		alert("expanding row");
	},
	beforeExpand: function(record, body, rowIndex) {
		var record = this.view.getRecord(this.view.getNode(rowIdx));
		var guid = record.get('uguid');
		if (this.fireEvent('beforeexpand', this, record, body, rowIndex) !== false) {
			body.innerHTML = this.getBodyContent(record, rowIndex);
			return true;
		} else {
			return false;
		}
	},
	getBodyContent: function() {
		var body = '<div id="tmp">Loading…</div>';
		this.ajax({
			url: '/resolve/service/metric/guid/list', //call to the server to deploy records				
			params: {
				guid: guid
			},
			success: function(response, options) {
				Ext.getDom("articleReportsPreview" + options.objId).innerHTML = Ext.htmlEncode(response.responseText);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})

		return body;
	}
})
glu.defView('RS.metric.Definition', {
	layout: 'fit',
	items: [{
		xtype: 'form',
		autoScroll: true,
		bodyPadding: '10px',
		defaults: {
			anchor: '-20',
			allowBlank: false
		},

		items: [{
			xtype: 'displayfield',
			name: 'uname',
			enforceMaxLength: 100
		}, {
			xtype: 'displayfield',
			name: 'ugroup',
			enforceMaxLength: 100
		}, {
			xtype: 'textfield',
			name: 'uhigh',
			enforceMaxLength: 20
		}, {
			xtype: 'textfield',
			name: 'ulow',
			enforceMaxLength: 20
		}, {
			xtype: 'displayfield',
			name: 'utype',
			enforceMaxLength: 100
		}, {
			xtype: 'textfield',
			name: 'ualertStatus',
			enforceMaxLength: 40
		}]
	}],
	buttonAlign: 'left',
	buttons: ['save', 'cancel'],
	asWindow: {
		width: 600,
		height: 290,
		modal: true,
		title: '@{windowTitle}'
	}
})
glu.defView('RS.metric.Definitions', {
	padding: '10px',
	xtype: 'grid',
	name: 'definitions',
	columns: '@{columns}',
	border: false,
	stateId: '@{stateId}',
	stateful: true,
	displayName: '@{displayName}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager',
		showSysInfo: false
	}, {
		ptype: 'cellediting',
		clicksToEdit: 1
	}],

	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.editColumnTooltip,
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},
	columns: '@{columns}',
	viewConfig: {
		enableTextSelection: true
	},

	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar',
		name: 'actionBar',
		items: ['save']
	}],
	listeners: {
		editAction: '@{editDefinition}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
});
glu.defView('RS.metric.Server', {
	//html: 'Server view',
	displayName: '@{displayName}',
	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'definitions',
		border: false,
		displayName: '@{displayName}',

		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}, {
			ptype: 'cellediting',
			clicksToEdit: 1
		}],

		store: '@{definitionsStore}',

		columns: '@{columns}',

		verticalScroller: {
			xtype: 'paginggridscroller',
			activePrefetch: false
		},
		selType: 'checkboxmodel',
		viewConfig: {
			enableTextSelection: false
		},
		//disableSelection: true,
		invalidateScrollerOnRefresh: false,
		listeners: {
			//bind the row element to checkbox in the 
			select: function(model, record, index) {
				id = record.get('id');
				//To test if returns the correct id
				//alert(id);
			}
			// },
			// deselect: function(model, record, index) {
			// //Nothing for now
			// }
		}
	}],

	//Buttons
	buttonAlign: 'left',
	buttons: ['deploy', 'cancel'],
	asWindow: {
		width: 800,
		height: 600,
		title: '@{windowTitle}'
	},

	listeners: {
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}

});
/**
 * @author alokika.dash
 */
glu.defView('RS.metric.ServerComponent', {
	//html: 'Server view',
	displayName: '@{displayName}',
	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'components',
		border: false,
		displayName: '@{displayName}',

		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}],

		store: '@{componentStore}',

		columns: '@{columns}',

		verticalScroller: {
			xtype: 'paginggridscroller',
			activePrefetch: false
		},

		selType: 'checkboxmodel',

		viewConfig: {
			enableTextSelection: false
		},

		invalidateScrollerOnRefresh: false,

		listeners: {
			//bind the row element to checkbox in the 
			select: function(model, record, index) {
				id = record.get('id');
				//To test if returns the correct id
				//alert(id);
			}
		}
	}],

	//Buttons
	buttonAlign: 'left',
	buttons: ['deploy', 'cancel'],

	asWindow: {
		width: 800,
		height: 600,
		title: '@{windowTitle}'
	},

	listeners: {
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}

})
glu.defView('RS.metric.Servers', {
	padding: '10px',
	xtype: 'grid',
	name: 'servers',
	columns: '@{columns}',
	border: false,
	stateId: '@{stateId}',
	stateful: true,
	displayName: '@{displayName}',

	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager',
		showSysInfo: false
	}, {
		ptype: 'rowexpander',
		expandOnDblClick: false,
		selectRowOnExpand: true,
		rowBodyTpl: '<b></b>'
	}],

	features: {
		ftype: 'grouping',
		enableGroupingMenu: false,
		startCollapsed: true // start all groups collapsed
	},

	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar',
		name: 'actionBar',
		items: ['deployToServer', 'defaultThresholds']
	}],

	viewConfig: {
		enableTextSelection: true,
		listeners: {
			expandbody: function(rowNode, record, expandRow, eOpts) {
				Ext.Ajax.request({
					url: '/resolve/service/metric/guid/list', //call to the server to deploy records
					params: {
						guid: record.get('uguid')
					},
					success: function(response, options) {
						var respData = RS.common.parsePayload(response);
						if (respData.success){
							var matrixTpl = new Ext.XTemplate(
								'<style type="text/css">',
								'table.imagetable {',
								'font-family: verdana,arial,sans-serif;',
								'font-size:11px;',
								'color:#333333;',
								'border-width: 1px;',
								'border-color: #999999;',
								'border-collapse: collapse;',
	
								'}',
								'table.imagetable th {',
								'background:silver;',
								'border-width: 2px;',
								'padding: 8px;',
								'border-style: solid;',
								'border-color: #999999;',
								'font-size: 12px;',
								'}',
								'table.imagetable td {',
								'background:#ebf3fd;',
								'border-width: 1px;',
								'padding: 8px;',
								'border-style: solid;',
								'border-color: #999999;',
								'font-style:normal;',
								'}',
								'</style>',
								'<table class="imagetable" width="40%">',
								'<!-- Table Header -->',
								'<tr>',
								'<th>Name</th>',
								'<th>Group</th>',
								'<th>High</th>',
								'<th>Low</th>',
								'</tr>',
	
								'<tpl for=".">',
								'<tr>',
								'<td align="center" >{uname}</td>',
								'<td align="center">{ugroup}</td>',
								'<td align="center">{uhigh}</td>',
								'<td align="center">{ulow}</td>',
								'</tr>',
								'</tpl>',
								// '</tpl>',
	
								'</table>'
	
								//{disableFormats: true}
							)
							// expandRow.innerHTML = expandRow.innerHTML.replace('<b></b>', matrixTpl)
							matrixTpl.overwrite(Ext.fly(expandRow).query('b')[0], respData.records)
						} else {
							clientVM.displayError(respData.message);
						}
					},
					failure: function(response) {
						//alert(DWRUtil.toDescriptiveString(error, 3));
						clientVM.displayFailure(response);
					}
				})
			}
		}
	},

	listeners: {
		deployAction: '@{deployToServer}'
	}
})
/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.metric').locale = {
	windowTitle: 'Metric Project',

	//Fields
	uname: 'Name',
	ugroup: 'Group',
	utype: 'Type',
	ualertStatus: 'Alert Level',
	uhigh: 'High',
	ulow: 'Low',
	ucomponenttype: 'Component',
	uipaddress: 'Address',
	definitionsDisplayName: 'Metric Definitions',
	select: 'Select',
	/* Server Component Grid */
	uipaddress: 'Address',
	ustatus: 'Status',
	uversion: 'Resolve Version',
	uupdatetime: 'Active Since',
	uguid: 'GUID',

	Definitions: {

		//Actions
		createDefinition: 'New',
		deleteDefinition: 'Delete',
		deleteTitle: 'Delete Metric Defintions',
		deletesMessage: 'Are you sure you want to delete the selected definitions?<center>({len} properties selected)</center>',
		deleteMessage: 'Are you sure you want to delete the selected definition?',
		cancel: 'Cancel',

		Default: 'default',
		DecisionTree: 'decision tree',

		save: 'Save'
	},

	Definition: {
		editDefinition: 'Edit Definition',
		newDefinition: 'New Definition',

		//Actions
		save: 'Save',
		saveAndDeploy: 'Save & Deploy',
		cancel: 'Cancel',
		saveSuccess: 'Metric definition has been saved.',
		saveFailure: 'Failed to save the metric definition',
		saveTitle: 'Save Metric Definition',
		ok: 'OK',
		saveError: 'Error saving the metric definition',

		//Validations
		valueIsInvalid: 'Please provide a value between 0 and 1000000',
		typeIsInvalid: 'Please provide a type from the drop down box',
		nameInvalid: 'Please provide a name for the definition'

	},

	Servers: {
		serversDisplayName: 'Metric Servers',
		deployToServer: 'Deploy',
		defaultThresholds: 'Default Thresholds'
	},

	Server: {
		//Actions
		serverDeployMetricsTitle: 'Deploy Metrics',
		serverDeployStatus: 'Deploy Status',
		serverDeployStatusMessage: 'Deployed to server successfully.',
		serverDeployError: 'Deploy Status',
		serverDeployErrorMessage: 'There are no records selected to deploy.',

		deploy: 'Deploy',
		cancel: 'Cancel',

		//Validations
		deployInvalid: 'Please select a metric to be deployed',
		ajaxErrorTitle: 'Backend Server Error',
		confirm: 'OK'
	},

	ServerComponent: {
		//Actions
		deploy: 'Deploy',
		cancel: 'Cancel',
		componentsDisplayName: 'Server Components',

		serverDeployStatus: 'Deploy Status',
		serverDeploySuccessMessage: 'Deployed to server successfully.',
		serverDeployErrorMessage: 'There are no records selected to deploy.',
		serverDeployMetricsTitle: 'Deploy Metrics',

		// Error messages when server cannot respond with a lidt of active components(RSCONTROL, RSVIEW)
		componentErrorTitle: 'Server Component Error',
		componentErrorMsg: 'The server currently cannot handle the request.',
		confirm: 'OK',

		//Validations
		deployInvalid: 'Please select a metric to be deployed'
	}
}
