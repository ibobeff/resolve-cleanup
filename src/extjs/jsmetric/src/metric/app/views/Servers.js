glu.defView('RS.metric.Servers', {
	padding: '10px',
	xtype: 'grid',
	name: 'servers',
	columns: '@{columns}',
	border: false,
	stateId: '@{stateId}',
	stateful: true,
	displayName: '@{displayName}',

	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager',
		showSysInfo: false
	}, {
		ptype: 'rowexpander',
		expandOnDblClick: false,
		selectRowOnExpand: true,
		rowBodyTpl: '<b></b>'
	}],

	features: {
		ftype: 'grouping',
		enableGroupingMenu: false,
		startCollapsed: true // start all groups collapsed
	},

	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar',
		name: 'actionBar',
		items: ['deployToServer', 'defaultThresholds']
	}],

	viewConfig: {
		enableTextSelection: true,
		listeners: {
			expandbody: function(rowNode, record, expandRow, eOpts) {
				Ext.Ajax.request({
					url: '/resolve/service/metric/guid/list', //call to the server to deploy records
					params: {
						guid: record.get('uguid')
					},
					success: function(response, options) {
						var respData = RS.common.parsePayload(response);
						if (respData.success){
							var matrixTpl = new Ext.XTemplate(
								'<style type="text/css">',
								'table.imagetable {',
								'font-family: verdana,arial,sans-serif;',
								'font-size:11px;',
								'color:#333333;',
								'border-width: 1px;',
								'border-color: #999999;',
								'border-collapse: collapse;',
	
								'}',
								'table.imagetable th {',
								'background:silver;',
								'border-width: 2px;',
								'padding: 8px;',
								'border-style: solid;',
								'border-color: #999999;',
								'font-size: 12px;',
								'}',
								'table.imagetable td {',
								'background:#ebf3fd;',
								'border-width: 1px;',
								'padding: 8px;',
								'border-style: solid;',
								'border-color: #999999;',
								'font-style:normal;',
								'}',
								'</style>',
								'<table class="imagetable" width="40%">',
								'<!-- Table Header -->',
								'<tr>',
								'<th>Name</th>',
								'<th>Group</th>',
								'<th>High</th>',
								'<th>Low</th>',
								'</tr>',
	
								'<tpl for=".">',
								'<tr>',
								'<td align="center" >{uname}</td>',
								'<td align="center">{ugroup}</td>',
								'<td align="center">{uhigh}</td>',
								'<td align="center">{ulow}</td>',
								'</tr>',
								'</tpl>',
								// '</tpl>',
	
								'</table>'
	
								//{disableFormats: true}
							)
							// expandRow.innerHTML = expandRow.innerHTML.replace('<b></b>', matrixTpl)
							matrixTpl.overwrite(Ext.fly(expandRow).query('b')[0], respData.records)
						} else {
							clientVM.displayError(respData.message);
						}
					},
					failure: function(response) {
						//alert(DWRUtil.toDescriptiveString(error, 3));
						clientVM.displayFailure(response);
					}
				})
			}
		}
	},

	listeners: {
		deployAction: '@{deployToServer}'
	}
})