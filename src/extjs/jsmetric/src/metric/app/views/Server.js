glu.defView('RS.metric.Server', {
	//html: 'Server view',
	displayName: '@{displayName}',
	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'definitions',
		border: false,
		displayName: '@{displayName}',

		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}, {
			ptype: 'cellediting',
			clicksToEdit: 1
		}],

		store: '@{definitionsStore}',

		columns: '@{columns}',

		verticalScroller: {
			xtype: 'paginggridscroller',
			activePrefetch: false
		},
		selType: 'checkboxmodel',
		viewConfig: {
			enableTextSelection: false
		},
		//disableSelection: true,
		invalidateScrollerOnRefresh: false,
		listeners: {
			//bind the row element to checkbox in the 
			select: function(model, record, index) {
				id = record.get('id');
				//To test if returns the correct id
				//alert(id);
			}
			// },
			// deselect: function(model, record, index) {
			// //Nothing for now
			// }
		}
	}],

	//Buttons
	buttonAlign: 'left',
	buttons: ['deploy', 'cancel'],
	asWindow: {
		width: 800,
		height: 600,
		title: '@{windowTitle}'
	},

	listeners: {
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}

});