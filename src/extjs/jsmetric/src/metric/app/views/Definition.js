glu.defView('RS.metric.Definition', {
	layout: 'fit',
	items: [{
		xtype: 'form',
		autoScroll: true,
		bodyPadding: '10px',
		defaults: {
			anchor: '-20',
			allowBlank: false
		},

		items: [{
			xtype: 'displayfield',
			name: 'uname',
			enforceMaxLength: 100
		}, {
			xtype: 'displayfield',
			name: 'ugroup',
			enforceMaxLength: 100
		}, {
			xtype: 'textfield',
			name: 'uhigh',
			enforceMaxLength: 20
		}, {
			xtype: 'textfield',
			name: 'ulow',
			enforceMaxLength: 20
		}, {
			xtype: 'displayfield',
			name: 'utype',
			enforceMaxLength: 100
		}, {
			xtype: 'textfield',
			name: 'ualertStatus',
			enforceMaxLength: 40
		}]
	}],
	buttonAlign: 'left',
	buttons: ['save', 'cancel'],
	asWindow: {
		width: 600,
		height: 290,
		modal: true,
		title: '@{windowTitle}'
	}
})