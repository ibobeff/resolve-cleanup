glu.defView('RS.metric.Definitions', {
	padding: '10px',
	xtype: 'grid',
	name: 'definitions',
	columns: '@{columns}',
	border: false,
	stateId: '@{stateId}',
	stateful: true,
	displayName: '@{displayName}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager',
		showSysInfo: false
	}, {
		ptype: 'cellediting',
		clicksToEdit: 1
	}],

	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.editColumnTooltip,
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},
	columns: '@{columns}',
	viewConfig: {
		enableTextSelection: true
	},

	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar',
		name: 'actionBar',
		items: ['save']
	}],
	listeners: {
		editAction: '@{editDefinition}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
});