/**
 * @author alokika.dash
 */
glu.defView('RS.metric.ServerComponent', {
	//html: 'Server view',
	displayName: '@{displayName}',
	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'components',
		border: false,
		displayName: '@{displayName}',

		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}],

		store: '@{componentStore}',

		columns: '@{columns}',

		verticalScroller: {
			xtype: 'paginggridscroller',
			activePrefetch: false
		},

		selType: 'checkboxmodel',

		viewConfig: {
			enableTextSelection: false
		},

		invalidateScrollerOnRefresh: false,

		listeners: {
			//bind the row element to checkbox in the 
			select: function(model, record, index) {
				id = record.get('id');
				//To test if returns the correct id
				//alert(id);
			}
		}
	}],

	//Buttons
	buttonAlign: 'left',
	buttons: ['deploy', 'cancel'],

	asWindow: {
		width: 800,
		height: 600,
		title: '@{windowTitle}'
	},

	listeners: {
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}

})