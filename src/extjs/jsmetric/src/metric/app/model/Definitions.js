Ext.define('RS.metric.model.Definitions', {
	extend: 'Ext.data.Model',
	idProperty: 'id',
	fields: ['id', 'uname', 'ugroup', 'utype', 'ucomponenttype', 'uipaddress', 'uguid', 'uhigh', 'ulow', 'ualertStatus'],
	proxy: {
		type: 'ajax',
		url: '/resolve/service/metric/list',
		reader: {
			type: 'json',
			root: 'records'
		},
		listeners: {
			exception: function(e, resp, op) {
				clientVM.displayExceptionError(e, resp, op);
			}
		}
	}
});