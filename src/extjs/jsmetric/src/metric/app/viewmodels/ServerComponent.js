glu.defModel('RS.metric.ServerComponent', {

	mock: false,
	name: 'components',
	displayName: '~~componentsDisplayName~~',
	windowTitle: '~~serverDeployMetricsTitle~~',

	definitionId: '',

	activate: function() {
		window.document.title = this.localize('windowTitle')
	},

	//Common grid stuff
	allSelected: false,
	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	componentStore: {
		mtype: 'store',
		remoteSort: true,
		fields: ['id', 'uipaddress', 'ustatus', 'utype', 'uversion', 'uguid', {
			name: 'uupdatetime',
			type: 'date',
			format: 'time',
			dateFormat: 'time'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/metric/component/list',
			reader: {
				type: 'json',
				root: 'records',
				messageProperty: 'message'
			}
			/* hanlded below in handleLoadComponentsFailure()
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
			*/
		}
	},

	componentsSelections: [], // the grid selection, the name should be same as the name of grid

	columns: [],

	init: function() {
		if (this.mock) {
			this.backend = RS.metric.createMockBackend(true)
		}
		this.set('displayName', this.localize('componentsDisplayName'))
		this.set('windowTitle', this.localize('serverDeployMetricsTitle'))
		this.set('deployStatusName', this.localize('serverDeployStatus'))
		this.set('deploySuccessMessage', this.localize('serverDeploySuccessMessage'))
		this.set('deployErrorMessage', this.localize('serverDeployErrorMessage'))

		/* Setting Columns here because outside the init() the this.clientVM is not instantiated yet */
		this.set('columns', [{
			header: '~~uipaddress~~',
			dataIndex: 'uipaddress',
			flex: 1,
			filterable: true
		}, {
			header: '~~ustatus~~',
			dataIndex: 'ustatus',
			flex: 1,
			filterable: true
		}, {
			header: '~~utype~~',
			dataIndex: 'utype',
			flex: 1,
			filterable: true
		}, {
			header: '~~uversion~~',
			dataIndex: 'uversion',
			filterable: true
		}, {
			header: '~~uupdatetime~~',
			dataIndex: 'uupdatetime',
			sortable: true,
			width: 200,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}])

		//Load the server components from the server
		this.componentStore.load({
			scope: this,
			callback: function(records, op, suc) {
				if (!suc) {
					this.handleLoadComponentsFailure(op)
				}
			}
		})
	},

	handleLoadComponentsFailure: function(op) {
		this.message({
			title: this.localize('componentErrorTitle'),
			msg: op.error,
			/*
			{
				var message = this.getResponseData(op.response).message
				if (message) {
					var messageDescription = 'componentErrorTitle';
					Ext.MessageBox.alert(messageDescription, message);
				}
				//this.localize(op.response.responseText),//'componentErrorMsg'),
			},	
			*/
			buttons: Ext.MessageBox.YES,
			buttonText: {
				yes: this.localize('confirm')
			}
		})
	},

	getResponseData: function(response) {
		try {
			var data = Ext.decode(response.responseText);
		} catch (ex) {
			Ext.Error.raise({
				response: response,
				json: response.responseText,
				parseError: ex,
				msg: 'Unable to parse the JSON returned by the server: ' + ex.toString()
			});
		}

		return data;
	},

	cancel: function() {
		this.doClose()
	},

	persisting: false,
	deployStatusName: '~~serverDeployStatus~~',
	deploySuccessMessage: '~~serverDeploySuccessMessage~~',
	deployErrorMessage: '~~serverDeployErrorMessage~~',


	deploy: function() {
		//Check if records are selected
		if (this.componentsSelections.length > 0) //selections from the grid
		{
			/*
				//TEST : print the data you get of the selection 
				for(var i = 0; i < this.componentsSelections.length; i++)
				{
					 alert(this.componentsSelections[i].data["id"]);
				}
			*/
			/* Array of server components to deploy on */
			var guids = [];
			for (var i = 0; i < this.componentsSelections.length; i++) {
				guids.push(this.componentsSelections[i].data.uguid);
			}

			/* sys_id of metric definition from Definition */
			var metricids = [];
			metricids.push(this.definitionId);

			//ajax call to deploy the metric definition
			this.ajax({
				url: '/resolve/service/metric/deploy', //call to the server to deploy records				
				params: {
					metricDefSysIds: metricids,
					guids: guids
				},
				scope: this,
				success: function(serverReply) {
					var response = RS.common.parsePayload(serverReply)
					if (response.success) {
						// alert message that it has been deployed
						//Ext.Msg.alert(this.deployStatusName, this.deploySuccessMessage);
						this.message({
							title: this.localize(this.deployStatusName),
							msg: this.deploySuccessMessage,
							buttons: Ext.MessageBox.YES,
							buttonText: {
								yes: this.localize('confirm')
							}
						})
					} else {
						clientVM.displayError(response.message) // this message comes from AjaxMetric's /metric/deploy
					}
				},
				failure : function(resp){
					clientVM.displayFailure(resp);
				},
				callback : function(){
					this.set('persisting', false)
				}
			})
			this.set('persisting', true)
		} else {
			Ext.Msg.alert(this.deployStatusError, this.deployErrorMessage);
			this.set('persisting', false)
		}
		this.doClose()
	},

	deployIsEnabled$: function() {
		//disable deploy button when no records are selected
		return (this.componentsSelections.length > 0) ? true : false
	},

	buttonVisibility: false,
	deployIsVisible$: function() {
		return !this.buttonVisibility
	}

})