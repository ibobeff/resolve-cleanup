glu.defModel('RS.metric.Definitions', {

	mock: false,

	stateId: 'DefinitionsGrid',

	activate: function() {
		window.document.title = this.localize('windowTitle')
	},

	waitResponse: false,

	definitions: {
		mtype: 'store',
		model: 'RS.metric.model.Definitions'
	},

	definitionsSelections: [],

	typeStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},

	displayName: '~~definitionsDisplayName~~',

	columns: [{
		header: '~~uname~~',
		dataIndex: 'uname',
		flex: 1,
		filterable: true
	}, {
		header: '~~ugroup~~',
		dataIndex: 'ugroup',
		flex: 1,
		filterable: true
	}, {
		header: '~~uhigh~~',
		dataIndex: 'uhigh',
		flex: 1,
		filterable: true,
		editor: {
			xtype: 'numberfield',
			allowBlank: false
		}
	}, {
		header: '~~ulow~~',
		dataIndex: 'ulow',
		flex: 1,
		filterable: true,
		editor: {
			xtype: 'numberfield',
			allowBlank: false
		}
	}, {
		header: '~~utype~~',
		dataIndex: 'utype',
		flex: 1,
		filterable: true
	}, {
		header: '~~ualertStatus~~',
		dataIndex: 'ualertStatus',
		flex: 1,
		filterable: true
	}],

	init: function() {
		if (this.mock) this.backend = RS.metric.createMockBackend(true)
		this.set('displayName', this.localize('definitionsDisplayName'))
		this.definitions.load()


		//Load up the types
		this.typeStore.add([{
			name: this.localize('Default'),
			value: 'default'
		}, {
			name: this.localize('DecisionTree'),
			value: 'decisiontree'
		}])
	},

	//Common grid stuff
	allSelected: false,
	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	//Buttons
	editDefinition: function(id) {
		this.open({
			mtype: 'RS.metric.Definition',
			id: id
		})
	},

	deleteDefinition: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.definitionsSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', {
				len: this.definitionsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteDefinition'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteDefinition
		})
	},

	reallyDeleteDefinition: function(btn) {
		if (btn == 'yes') {
			var ids = [];
			for (var i = 0; i < this.definitionsSelections.length; i++) {
				ids.push(this.definitionsSelections[i].data.id);
			}
			strIds = ids.join(',')
			this.ajax({
				url: '/resolve/service/metric/delete',
				params: {
					ids: strIds
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				},
				callback: function(resp, opt) {
					this.set('definitionsSelections', [])
					var self = this;
					this.definitions.load({
						callback: function() {
							self.set('waitResponse', false)
						}
					});
				}
			})
			this.set('waitResponse', true)
		}
	},

	deleteDefinitionIsEnabled$: function() {
		return (this.definitionsSelections.length > 0 && !this.waitResponse)
	},

	save: function() {
		var changes, data;
		this.definitions.each(function(definition) {
			changes = definition.getChanges()
			if (!Ext.Object.isEmpty(changes)) {
				data = definition.getData()
				this.ajax({
					url: '/resolve/service/metric/save',
					jsonData: {
						sys_id: data.id,
						UName: data.uname,
						UGroup: data.ugroup,
						UType: data.utype,
						UHigh: data.uhigh,
						ULow: data.ulow,
						UGuid: data.uguid,
						UAlertStatus: data.ualertStatus
					},
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (!response.success) clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}, this)
		this.definitions.commitChanges()
	}
})