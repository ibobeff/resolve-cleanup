glu.defModel('RS.metric.Definition', {

	mock: false,

	waitingResponse: false,

	fields: ['id', 'uname', 'ugroup', 'utype', 'uhigh', 'ulow', 'uguid', 'ualertStatus'],
	id: '',
	uname: '',
	unameIsValid$: function() {
		return this.uname ? true : this.localize('nameInvalid')
	},
	ugroup: '',
	ugroupIsValid$: function() {
		return this.ugroup ? true : this.localize('groupInvalid')
	},
	utype: '',
	uhigh: 100,
	ulow: 0,
	uguid: '',
	percent$: function() {
		return this.uhigh + this.ulow
	},

	windowTitle$: function() {
		return this.id ? this.localize('editDefinition') : this.localize('newDefinition')
	},

	init: function() {
		if (this.mock) this.backend = RS.metric.createMockBackend(true)

		if (this.id) this.ajax({
			url: '/resolve/service/metric/get',
			params: {
				id: this.id
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) this.loadData(response.data)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	save: function() {
		var definitionToSave = {
			sys_id: this.id,
			UName: this.uname,
			UGroup: this.ugroup,
			UType: this.utype,
			UHigh: this.uhigh,
			ULow: this.ulow,
			UGuid: this.uguid,
			UAlertStatus: this.ualertStatus
		}
		this.set('waitingResponse', true);
		this.ajax({
			url: '/resolve/service/metric/save',
			jsonData: definitionToSave,
			scope: this,
			success: function(resp, opt) {
				this.showSaveMessage(resp.responseText)
				this.parentVM.definitions.load()
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('waitingResponse', false)
			}
		});
		this.doClose();
	},


	showSaveMessage: function(msg) {
		var state = 'saveSuccess';
		var responseObj = Ext.JSON.decode(msg);
		if (responseObj.message != null && responseObj.message.contains('saveError'))
			state = 'saveFailure';
		this.message({
			title: this.localize('saveTitle'),
			msg: this.localize(state),
			buttons: Ext.MessageBox.YES,
			buttonText: {
				yes: this.localize('ok')
			},
			scope: this
		})
	},

	getResponseData: function(response) {
		try {
			var data = Ext.decode(response.responseText);
		} catch (ex) {
			Ext.Error.raise({
				response: response,
				json: response.responseText,
				parseError: ex,
				msg: 'Unable to parse the JSON returned by the server: ' + ex.toString()
			});
		}

		return data;
	},

	saveIsEnabled$: function() {
		return this.isValid
	},

	saveAndDeploy: function() {
		//first save the record and the deploy it to the list of servers
		var definitionToSave = {
			sys_id: this.id,
			UName: this.uname,
			UGroup: this.ugroup,
			UType: this.utype,
			UHigh: this.uhigh,
			ULow: this.ulow,
			UGuid: this.uguid,
			UAlertStatus: this.ualertStatus
		}
		this.ajax({
			url: '/resolve/service/metric/save',
			jsonData: definitionToSave,
			success: function(resp, opt) {
				this.set('waitingResponse', false)
				this.parentVM.definitions.load()
				this.set('waitingResponse', false)
				this.doClose()
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
		this.set('waitingResponse', true);
		//call the deploying servers
		this.open({
			mtype: 'RS.metric.ServerComponent',
			definitionId: this.id
		})
	},

	saveAndDeployIsVisible$: function() {
		return this.id
	},

	saveAndDeployIsEnabled$: function() {
		return this.isValid
	},

	cancel: function() {
		this.doClose()
	},

	cancelIsEnabled$: function() {
		return true
	},

	ulowIsValid$: function() {
		if (this.ulow < 0) return this.localize('valueIsInvalid')
		if (this.ulow > 1000000) return this.localize('valueIsInvalid')
		if (!this.ulow && this.ulow !== 0) return this.localize('valueIsInvalid')
		return true
	},

	uhighIsValid$: function() {
		if (this.uhigh < 0) return this.localize('valueIsInvalid')
		if (this.uhigh > 1000000) return this.localize('valueIsInvalid')
		if (!this.uhigh && this.uhigh !== 0) return this.localize('valueIsInvalid')
		return true
	},

	utypeIsValid$: function() {
		if (!this.utype) return this.localize('typeIsInvalid')
		return true
	}
})