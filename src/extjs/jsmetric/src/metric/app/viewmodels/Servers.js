glu.defModel('RS.metric.Servers', {

	mock: false,

	stateId: 'ServersGrid',

	waitResponse: false,

	activate: function() {
		window.document.title = this.localize('windowTitle')
	},

	displayName: '~~serversDisplayName~~',

	servers: {
		mtype: 'store',
		groupField: 'uipaddress',
		remoteSort: true,
		fields: ['id', 'uipaddress', 'ustatus', 'utype', 'uversion', 'uguid', {
			name: 'uupdatetime',
			type: 'date',
			format: 'time',
			dateFormat: 'time'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/metric/component/list',
			reader: {
				type: 'json',
				root: 'records',
				messageProperty: 'message'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	serversSelections: [],

	columns: [{
		header: '~~utype~~',
		dataIndex: 'utype',
		filterable: true,
		flex: 1
	}, {
		header: '~~uipaddress~~',
		dataIndex: 'uipaddress',
		filterable: true,
		flex: 1
	}, {
		header: '~~uguid~~',
		dataIndex: 'uguid',
		filterable: true,
		flex: 1
	}],

	init: function() {
		if (this.mock) this.backend = RS.metric.createMockBackend(true)
		this.set('displayName', this.localize('serversDisplayName'))
		this.servers.load()
	},

	//Common grid stuff
	allSelected: false,
	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	//Buttons
	deployToServer: function() {
		var serverIds = [];
		for (var i = 0; i < this.serversSelections.length; i++) {
			serverIds.push(this.serversSelections[i].data.uguid); // send a list of GUIDs that are available
		}
		this.open({
			mtype: 'RS.metric.Server',
			serversIds: serverIds
		})
	},

	defaultThresholds: function() {
		clientVM.handleNavigation({
			modelName: 'RS.metric.Definitions'
		})
	},

	deployToServerIsEnabled$: function() {
		return this.serversSelections.length > 0
	},

	updateDeployedServers: function() {
		this.servers.load({
			scope: this,
			callback: function() {
				this.set('waitResponse', false)
			}
		})
		this.set('waitResponse', true)
	},
	expandGridRow: function(expander, record, body, rowIndex) {
		debugger
		alert("expanding row");
	},
	beforeExpand: function(record, body, rowIndex) {
		var record = this.view.getRecord(this.view.getNode(rowIdx));
		var guid = record.get('uguid');
		if (this.fireEvent('beforeexpand', this, record, body, rowIndex) !== false) {
			body.innerHTML = this.getBodyContent(record, rowIndex);
			return true;
		} else {
			return false;
		}
	},
	getBodyContent: function() {
		var body = '<div id="tmp">Loading…</div>';
		this.ajax({
			url: '/resolve/service/metric/guid/list', //call to the server to deploy records				
			params: {
				guid: guid
			},
			success: function(response, options) {
				Ext.getDom("articleReportsPreview" + options.objId).innerHTML = Ext.htmlEncode(response.responseText);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})

		return body;
	}
})