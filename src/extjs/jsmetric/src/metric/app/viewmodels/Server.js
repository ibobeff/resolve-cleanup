glu.defModel('RS.metric.Server', {
	mock: false,
	buttonVisibility: false,
	serversIds: '',

	//Common grid stuff
	allSelected: false,
	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	definitionsStore: {
		mtype: 'store',
		model: 'RS.metric.model.Definitions',
		remoteSort: true,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/metric/list',
			reader: {
				type: 'json',
				root: 'records'
			}
			/* handled below in handleLoadComponentsFailure()
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
			*/
		}
	},

	definitionsSelections: [],

	columns: [{
		header: '~~uname~~',
		dataIndex: 'uname',
		flex: 1,
		filterable: true
	}, {
		header: '~~ugroup~~',
		dataIndex: 'ugroup',
		flex: 1,
		filterable: true
	}, {
		header: '~~uhigh~~',
		dataIndex: 'uhigh',
		filterable: true,
		editor: {
			xtype: 'textfield'
			//allowBlank: false
		}
	}, {
		header: '~~ulow~~',
		dataIndex: 'ulow',
		filterable: true,
		editor: {
			xtype: 'textfield'
			//allowBlank: false
		}
	}, {
		header: '~~utype~~',
		dataIndex: 'utype',
		filterable: true
	}, {
		header: '~~ualertStatus~~',
		dataIndex: 'ualertStatus',
		flex: 1,
		filterable: true
	}],

	displayName: '~~definitionsDisplayName~~',
	windowTitle: '~~serverDeployMetricsTitle~~',

	init: function() {
		if (this.mock) this.backend = RS.metric.createMockBackend(true)
		this.set('displayName', this.localize('definitionsDisplayName'))
		this.set('windowTitle', this.localize('serverDeployMetricsTitle'))
		this.set('deployStatusName', this.localize('serverDeployStatus'))
		this.set('deployStatusError', this.localize('serverDeployError'))
		this.set('deployStatusMessage', this.localize('serverDeployStatusMessage'))
		this.set('deployErrorMessage', this.localize('serverDeployErrorMessage'))

		//Load the definitions from the server
		this.definitionsStore.load({
			scope: this,
			callback: function(records, op, suc) {
				if (!suc) {
					this.handleLoadComponentsFailure(op)
				}
			}
		})
	},

	handleLoadComponentsFailure: function(op) {
		this.message({
			title: this.localize('componentErrorTitle'),
			msg: op.error,
			buttons: Ext.MessageBox.YES,
			buttonText: {
				yes: this.localize('confirm')
			}
		})
	},

	cancel: function() {
		this.doClose()
	},

	cancelIsEnabled$: function() {
		return true
	},

	cancelIsVisible$: function() {
		return !this.buttonVisibility
	},

	persisting: false,

	deployStatusName: '~~serverDeployStatus~~',
	deployStatusError: '~~serverDeployError~~',
	deployStatusMessage: '~~serverDeployStatusMessage~~',
	deployErrorMessage: '~~serverDeployErrorMessage~~',

	serversSelections: [],

	deploy: function() { // deploy to the following components

		//Check if records are selected
		if (this.definitionsSelections.length > 0) //selections from the grid
		{
			/*
				//TEST : print the data you get of the selection 
				for(var i = 0; i < this.definitionsSelections.length; i++)
				{
					 alert(this.definitionsSelections[i].data["id"]);
				}
			*/
			/* Array of server components to deploy on */
			var guids = [];
			for (var i = 0; i < this.serversIds.length; i++) {
				guids.push(this.serversIds[i])
			}

			/* sys_ids of metric definition from Server */
			var metricids = [];
			var highs = [];
			var lows = [];
			for (var i = 0; i < this.definitionsSelections.length; i++) {
				metricids.push(this.definitionsSelections[i].data.id)
				highs.push(this.definitionsSelections[i].data.uhigh)
				lows.push(this.definitionsSelections[i].data.ulow)
			}

			this.ajax({
				url: '/resolve/service/metric/deploy', //call to the server to deploy records
				params: {
					metricDefSysIds: metricids,
					guids: guids,
					highs: highs,
					lows: lows
				},
				scope: this,
				success: function(serverReply) {
					var response = RS.common.parsePayload(serverReply)
					if (response.success) {
						// alert message that it has been deployed
						this.message({
							title: this.localize('serverDeployStatus'),
							msg: this.deployStatusMessage,
							buttons: Ext.MessageBox.YES,
							buttonText: {
								yes: this.localize('confirm')
							}
						})
						//Ext.Msg.alert(this.deployStatusName, this.deployStatusMessage);
						this.parentVM.updateDeployedServers()
					} else {
						clientVM.displayError(response.message) // message comes from server
					}
				},
				failure : function(resp){
					clientVM.displayFailure(resp);
				},
				callback: function(records, op, suc) {
					this.set('persisting', false)
					if (!suc) {
						this.handleLoadDeployListFailure(op)
					}
				}
			})

			this.set('persisting', true)
			/*
	    	this.ajax({
	    		url: '/resolve/service/metric/deployed/list', //call to the server to get a list of deployed sys_ids
	    		params: {
	    			sys_ids:this.serversIds
	    		},
	    		success: function(serverReply) {
	    			var response = RS.common.parsePayload(serverReply)
					if (response.success) {
						Ext.Array.forEach(response.records, function(record) {
							//Create an array of guids to be sent as parameter
							guids.push(record.uguid);
						})
						
						// sys_ids of metric definition from Server 
				    	var metricids = [];
				    	for (var i = 0; i < this.definitionsSelections.length; i++) {
							metricids.push(this.definitionsSelections[i].data.id)
				    	}
				    	
						this.ajax({
							url: '/resolve/service/metric/deploy', //call to the server to deploy records
					    	params: {
								metricDefSysIds: metricids,
								guids: guids
							},
							success: function(serverReply) {
								this.set('persisting', false)
								var response = RS.common.parsePayload(serverReply)
								if (response.success) {
									// alert message that it has been deployed
									this.message({			
										title: this.localize('serverDeployStatus'),
										msg: this.deployStatusMessage,
										buttons:Ext.MessageBox.YES,
										buttonText:{
											yes: this.localize('confirm')
										}
									})
									//Ext.Msg.alert(this.deployStatusName, this.deployStatusMessage);
									this.parentVM.updateDeployedServers()
								} 
								else 
								{ 
									clientVM.displayError(response.message) // message comes from server
								}
							}	
						})
						this.set('persisting', true)
					} 
				},
				
				
				callback: function(records, op, suc){
					if(!suc){ 
						this.handleLoadDeployListFailure(op)
					}
				}
	    	}) //end of ajax call
	    	*/

		} else {
			Ext.Msg.alert(this.deployStatusError, this.deployErrorMessage);
			this.set('persisting', false)
		}


		this.doClose()

	},

	handleLoadDeployListFailure: function(op) {
		this.message({
			title: this.localize('ajaxErrorTitle'),
			msg: op.error,
			buttons: Ext.MessageBox.YES,
			buttonText: {
				yes: this.localize('confirm')
			}
		})
	},

	deployIsEnabled$: function() {
		//disable deploy button when no records are selected
		return (this.definitionsSelections.length > 0) ? true : false
	},

	deployIsVisible$: function() {
		return !this.buttonVisibility
	}
})