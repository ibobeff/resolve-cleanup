/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/
glu.defModel('RS.license.AddLicense',{
	mixins: ['FileUploadManager'],
	api: {
		list: '/resolve/service/license/licenses',
		upload: '/resolve/service/license/upload',
	},

	intro: '',
	encrypt: '',
	activeItem: 1,
	wait: false,

	allowedExtensions: ['lic'],
	fineUploaderTemplateHidden: false,
	checkForDuplicates: false,
	allowMultipleFiles: false,

	uploaderAdditionalClass: 'license',

	init: function() {
		this.set('wait', false);
		this.set('dragHereText', this.localize('dragHere'));
		this.set('intro', this.activeItem == 0 ? this.localize('pasteIntro') : this.localize(Ext.isIE8m ? 'noDndUploadIntro' : 'uploadIntro'));
	},

	fileOnUploadCallback: function(manager, filename) {
		this.set('wait', true);
	},

	fileOnCompleteCallback: function(manager, filename, response, xhr) {
        this.set('wait', false);
		if (response.success) {
			this.open({
				mtype: 'RS.license.License',
				license: response.data,
				isModal: true
			});
			this.doClose();
		}
	},

	filesOnAllCompleteCallback: function(manager) {
	},

	activeItemChanged$: function() {
		this.set('intro', this.activeItem == 0 ? this.localize('pasteIntro') : this.localize(Ext.isIE8m ? 'noDndUploadIntro' : 'uploadIntro'));
	},

	uploadLicense: function() {
		this.set('activeItem', 1);
	},
	uploadLicenseIsPressed$: function() {
		return this.activeItem == 1;
	},
	pasteLicense: function() {
		this.set('activeItem', 0);
	},
	pasteLicenseIsPressed$: function() {
		return this.activeItem == 0;
	},

	submitText: function() {
		this.set('wait', true);
		this.set('intro', this.localize('sendingData'));
		this.ajax({
			url: '/resolve/service/license/submit',
			params: {
				data: this.encrypt
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('submitLicenseErr', respData.message));
					return;
				}
				this.open({
					mtype: 'RS.license.License',
					license: respData.data,
					isModal: true
				});
				this.doClose();
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},
	submitTextIsVisible$: function() {
		return this.activeItem == 0;
	},
	submitTextIsEnabled$: function() {
		return !this.wait && !! this.encrypt;
	},
	browse: function() {
	},
	browseIsVisible$: function() {
		return this.activeItem == 1;
	},
	browseIsEnabled$: function() {
		return !this.wait;
	}
});

glu.defModel('RS.license.FileUploadManagerFactory', function(cfg) {
	Ext.applyIf(cfg, RS.common.viewmodels.FileUploadManager);
	return cfg;
});

/*
glu.defModel('RS.license.AddLicense', {
	intro: '',
	encrypt: '',
	activeItem: 1,
	wait: false,
	upload: function() {
		this.set('activeItem', 1);
	},
	uploadIsPressed$: function() {
		return this.activeItem == 1;
	},
	paste: function() {
		this.set('activeItem', 0);
	},
	pasteIsPressed$: function() {
		return this.activeItem == 0;
	},
	submitText: function() {
		this.set('wait', true);
		this.set('intro', this.localize('sendingData'));
		this.ajax({
			url: '/resolve/service/license/submit',
			params: {
				data: this.encrypt
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('submitLicenseErr', respData.message));
					return;
				}
				this.open({
					mtype: 'RS.license.License',
					license: respData.data
				});
				this.doClose();
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},
	submitTextIsVisible$: function() {
		return this.activeItem == 0;
	},
	submitTextIsEnabled$: function() {
		return !this.wait && !! this.encrypt;
	},
	browse: function() {

	},
	browseIsVisible$: function() {
		return this.activeItem == 1;
	},
	browseIsEnabled$: function() {
		return !this.wait;
	},

	activeItemChanged$: function() {
		this.set('intro', this.activeItem == 0 ? this.localize('pasteIntro') : this.localize(Ext.isIE8m ? 'noDndUploadIntro' : 'uploadIntro'));
	},
	dndIsVisible$: function() {
		return this.activeItem == 1 && !Ext.isIE8m && !this.wait;
	},

	filesAdded: function(uploader) {
		this.set('wait', true);
		this.set('intro', this.localize('sendingData'));
	},

	uploadComplete: function(uploader, response) {
		this.set('wait', false);
		if (!response.success) {
			clientVM.displayError(this.localize('submitLicenseErr', response.message));
			return;
		}
		this.open({
			mtype: 'RS.license.License',
			license: response.data
		});
		this.close();
	},
	cancel: function() {
		this.doClose();
	}
});
*/

glu.defModel('RS.license.License', {
	license: null,
	dockedItemsIsVisible$: function() {
		return this.license == null;
	},
	key: '',
	type: '',
	customer: '',
	cores: '',
	coresDisplay$: function() {
		return this.cores == 0 ? 'Unlimited' : this.cores;
	},
	endUserCount: '',
	endUserCountDisplay$: function() {
		return this.endUserCount == 0 ? 'Unlimited' : this.endUserCount;
	},
	adminUserCount: '',
	adminUserCountDisplay$: function() {
		return this.adminUserCount == 0 ? 'Unlimited' : this.adminUserCount;
	},
	startDate: '',
	startedOn$: function() {
		return this.getParsedDate(this.startDate);
	},
	expirationDate: '',
	expiredOn$: function() {
		return this.getParsedDate(this.expirationDate);
	},
	createDate: '',
	createdOn$: function() {
		return this.getParsedDate(this.createDate);
	},
	uploadDate: '',
	uploadedOn$: function() {
		return this.getParsedDate(this.uploadDate);
	},
	evaluation: false,
	expired: false,
	color$: function() {
		this.expired ? 'red' : 'black';
	},
	isModal: false,

	ha: false,
	fields: ['key', 'type', 'customer', 'cores', 'duration', 'endUserCount', 'adminUserCount', 'expirationDate', 'deleteDate', {
		name: 'evaluation',
		type: 'bool'
	}, {
		name: 'expired',
		type: 'bool'
	}, {
		name: 'createDate',
		type: 'date',
		dateFormat: 'time'
	}, {
		name: 'deleteDate',
		type: 'date',
		dateFormat: 'time'
	}, {
		name: 'uploadDate',
		type: 'date',
		dateFormat: 'time'
	}, {
		name: 'expirationDate',
		type: 'date',
		dateFormat: 'time'
	}, {
		name: 'ha',
		type: 'bool'
	}],
	licenseTitle: '',
	defaultData: {

	},
	ipStore: {
		mtype: 'store',
		fields: ['ipAddress'],
		proxy: {
			type: 'memory'
		}
	},
	memStore: {
		mtype: 'store',
		fields: ['component', 'maxMemory'],
		proxy: {
			type: 'memory'
		}
	},
	gatewayStore: {
		mtype: 'store',
		fields: ['gateway', 'quantity'],
		proxy: {
			type: 'memory'
		}
	},
	getParsedDate: function(date) {
		var d = date,
			formats = ['time', 'c', 'Y-m-d G:i:s'];

		if (!Ext.isDate(d)) {
			Ext.Array.each(formats, function(format) {
				d = Ext.Date.parse(d, format)
				return !Ext.isDate(d)
			})
		}

		if (Ext.isDate(d))
			return Ext.Date.format(d, 'Y-m-d G:i:s');

		return RS.common.locale.unknownDate
	},
	activate: function(screen, params) {
		this.set('key', params && params.key ? params.key : '');
		if (this.key)
			this.loadRecord();
	},
	padding: '10px',
	init: function() {
		if (this.isModal) {
			this.set('padding', '0');
		}
		this.set('licenseTitle', this.localize('licenseTitle'));
		if (this.license)
			this.populate(this.license);
	},
	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.license.Main'
		})
	},
	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveScript, this, [exitAfterSave])
		this.task.delay(500)
		this.set('state', 'wait')
	},
	saveIsEnabled$: function() {
		return !this.state != 'wait' && this.isValkey && this.isDirty;
	},
	saveAndExit: function() {
		if (!this.saveIsEnabled)
			return;
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},

	refresh: function() {
		if (this.key)
			this.loadRecord();
	},

	populate: function(license) {
		this.loadData(license);
		this.ipStore.removeAll();
		Ext.Object.each(license.ipAddresses, function(key, value) {
			this.ipStore.add({
				ipAddress: key
			});
		}, this);
		this.memStore.removeAll();
		Ext.Object.each(license.maxMemory, function(key, value) {
			this.memStore.add({
				component: key,
				maxMemory: value
			});
		}, this);
		this.gatewayStore.removeAll();
		Ext.Object.each(license.gateway, function(key, value) {
			this.gatewayStore.add({
				gateway: key,
				quantity: value.instanceCount
			})
		}, this);
	},

	loadRecord: function() {
		this.ajax({
			url: '/resolve/service/license/get',
			params: {
				key: this.key
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					//clientVM.displayError(this.localize('getLicenseErr', respData.message));
					clientVM.displayError(respData.message);
				}
				if (respData.data)
					this.populate(respData.data);
			},

			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	apply: function() {
		this.ajax({
			url: '/resolve/service/license/apply',
			params: {
				data: this.license.rawLicense
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					//clientVM.displayError(this.localize('applyErr', respData.message));
					clientVM.displayError(respData.message);
					return;
				}
				this.parentVM.parentVM.refresh();
				clientVM.updateLicenseBanner();
				this.doClose();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	applyIsEnabled$: function() {
		return this.license && this.license.rawLicense;
	},

	cancel: function() {
		this.doClose();
	}
});
glu.defModel('RS.license.Main', {

	mock: false,
	stateLog: 'init',
	activate: function() {
		window.document.title = this.localize('windowTitle')
		this.loadSummary();
		this.license.load();
		this.alertLog.load({
			scope: this,
			callback: function() {
				this.set('stateLog', 'ready');
			}
		});
	},

	refresh: function() {
		this.loadSummary();
		this.license.load();
		this.alertLog.load({
			scope: this,
			callback: function() {
				this.set('stateLog', 'ready');
			}
		});
	},

	displayName: '~~License Grid~~',
	stateId: 'rslicense',
	stateIdLog: 'rslicenselog',
	reqCount: 0,
	logReqCount: 0,
	cores: 0,
	ha: false,
	endUserCount: 0,
	adminUserCount: 0,
	fields: ['cores', 'ha', 'endUserCount', 'adminUserCount'],

	gatewayFields: {
		mtype: 'list'
	},
	memoryFields: {
		mtype: 'list'
	},
	license: {
		mtype: 'store',
		pageable: false,
		fields: ['key', 'type', 'duration', {
			name: 'expirationDate',
			type: 'date',
			dateFormat: 'time'
		}, {
			name: 'deleteDate',
			type: 'date',
			dateFormat: 'time'
		}, {
			name: 'uploadDate',
			type: 'date',
			dateFormat: 'time'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/license/licenses',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	alertLog: {
		mtype: 'store',
		fields: ['useverity', 'umessage', 'ucomponent', 'utype'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		pageSize: 10,
		sorters: [{
			property: 'sysCreatedOn',
			direction: 'DESC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/alertlog/getLicenseLog',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	columns: null,
	alertLogColumns: null,

	allLicenseSelected: false,
	licenseSelections: [],

	allLogsSelected: false,
	alertLogSelections: [],

	init: function() {
		//Called when mocking the backend to test the UI
		if (this.mock) this.backend = RS.license.createMockBackend(true)

		this.set('displayName', this.localize('license'))
		var cols = [{
			header: '~~key~~',
			dataIndex: 'key',
			filterable: true,
			sortable: true,
			flex: 2
		}, {
			header: '~~type~~',
			dataIndex: 'type',
			filterable: true,
			sortable: true,
			width: 100
		}, {
			header: '~~duration~~',
			dataIndex: 'duration',
			filterable: true,
			sortable: true,
			width: 100
		}, {
			header: '~~uploadDate~~',
			dataIndex: 'uploadDate',
			filterable: true,
			sortable: true,
			flex: 1,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}, {
			header: '~~expirationDate~~',
			dataIndex: 'expirationDate',
			filterable: true,
			sortable: true,
			flex: 1,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}]
		Ext.each(cols, function(c) {
			var r = c.renderer;
			c.renderer = function(value, meta, record) {
				var v = value;
				if (record.get('expired'))
					meta.style = 'background-color:red !important';
				if (r)
					v = r(value);
				return v;
			}
		});
		this.set('columns', cols);
		this.license.on('beforeload', function() {
			this.set('reqCount', this.reqCount + 1);
		}, this);
		this.license.on('load', function(store, op, success) {
			this.set('reqCount', this.reqCount - 1);
			if (!success)
				clientVM.displayError(this.localize('listLicenseErr'));
		}, this);

		//Alert log columns (actually sysLog)
		if (this.mock) this.backend = RS.syslog.createMockBackend(true);
		var alertLogCols = RS.common.grid.getSysColumns();
		Ext.each(alertLogCols, function(col) {
			col.hidden = !(col.dataIndex == 'sysCreatedOn');
		});
		this.set('alertLogColumns', [{
			header: '~~message~~',
			dataIndex: 'umessage',
			filterable: true,
			flex: 1,
			renderer: RS.common.grid.logRenderer(4)
		}, {
			header: '~~component~~',
			dataIndex: 'ucomponent',
			filterable: true,
			width: 330
			/*		}, {
			header: '~~severity~~',
			dataIndex: 'useverity',
			filterable: true,
			width: 150 */
		}, {
			header: '~~type~~',
			dataIndex: 'utype',
			filterable: true,
			width: 75
		}].concat(alertLogCols));
		this.set('stateLog', 'waitGetAlertLogResponse');
		this.set('displayName', this.localize('displayName'));
	},

	selectAllLicenseAcrossPages: function(selectAll) {
		this.set('allLicenseSelected', selectAll)
	},
	selectionChangedLicense: function() {

		this.selectAllLicenseAcrossPages(false)
	},

	selectAllLogsAcrossPages: function(selectAll) {
		this.set('allLogsSelected', selectAll)
	},

	selectionChangedLog: function() {
		this.selectAllLicenseAcrossPages(false)
	},

	add: function() {
		this.open({
			mtype: 'RS.license.AddLicense'
		});
	},

	addIsEnabled$: function() {
		return this.reqCount == 0;
	},

	editRecord: function(key) {
		clientVM.handleNavigation({
			modelName: 'RS.license.License',
			params: {
				key: key
			}
		});
	},

	loadSummary: function() {
		this.set('reqCount', this.reqCount + 1);
		this.ajax({
			url: '/resolve/service/license/summary',
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('getSummaryErr', respData.message));
					return;
				}

				Ext.Object.each(respData.data, function(k, v) {
					respData.data[k] = (v === 0 ? 'Unlimited' : v);
				});
				this.loadData(respData.data);
				this.gatewayFields.removeAll();
				Ext.Object.each(respData.data.gateway, function(k, v) {
					this.gatewayFields.add(glu.model({
						mtype: 'RS.license.SummaryField',
						name: k,
						value: v.instanceCount == 0 ? 'Unlimited' : v.instanceCount
					}));
				}, this);
				this.memoryFields.removeAll();
				Ext.Object.each(respData.data.maxMemory, function(k, v) {
					this.memoryFields.add(glu.model({
						mtype: 'RS.license.SummaryField',
						name: k,
						value: v == 0 ? 'Unlimited' : v
					}));
				}, this);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
		});
	},

	singleRecordSelected$: function() {
		return this.licenseSelections.length == 1
	},
	editRecordIsEnabled$: function() {
		return this.singleRecordSelected
	},

	deleteRecordIsEnabled$: function() {
		return this.licenseSelections.length > 0 && this.reqCount == 0;
	},
	deleteRecord: function() {
		//Delete the selected record(s), but first confirm the action
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.licenseSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.licenseSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			this.set('reqCount', this.reqCount + 1);
			if (this.allLicenseSelected) {
				this.ajax({
					url: '/resolve/service/license/delete',
					params: {
						keys: '',
						whereClause: this.catalogs.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.refresh();
							clientVM.updateLicenseBanner();
							clientVM.displaySuccess(this.localize('deleteSuccess'))
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					},
					callback: function() {
						this.set('reqCount', this.reqCount - 1);
					}
				})
			} else {
				var ids = []
				Ext.each(this.licenseSelections, function(selection) {
					ids.push(selection.get('key'))
				})
				this.ajax({
					url: '/resolve/service/license/delete',
					params: {
						keys: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.refresh();
							clientVM.updateLicenseBanner();
							clientVM.displaySuccess(this.localize('deleteSuccess'))
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					},
					callback: function() {
						this.set('reqCount', this.reqCount - 1);
					}
				})
			}
		}
	},

	singleLogSelected$: function() {
		return this.alertLogSelections.length == 1
	},
	deleteLogIsEnabled$: function() {
		return this.alertLogSelections.length > 0 && this.logReqCount == 0;
	},
	editLog: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.syslog.AlertLogException',
			target: '_blank',
			params: {
				sys_id: id
			}
		});
	},
	deleteLog: function() {
		//Delete the selected record(s), but first confirm the action
		this.set('stateLog', 'confirmDelete');
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.alertLogSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.alertLogSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteLogs
		})
	},
	reallyDeleteLogs: function(btn) {
		if (btn === 'no') {
			this.set('stateLog', 'itemSelected');
			return;
		}
		var ids = '';
		for (var i = 0; i < this.alertLogSelections.length; i++) {
			ids += this.alertLogSelections[i].data.id + ',';
		}
		this.ajax({
			url: '/resolve/service/alertlog/deleteAlertLogRecs',
			params: {
				ids: ids
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					this.set('stateLog', 'deleteFailure');
					clientVM.displayError(this.localize('deleteFailureMsg') + '[' + respData.message + ']');
				}
				this.alertLog.load({
					scope: this,
					callback: function() {
						this.set('stateLog', 'ready');
					}
				});
				this.set('stateLog', 'waitGetAlertLogResponse');
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
		this.set('stateLog', 'waitDeleteResponse');
	}
});

glu.defModel('RS.license.SummaryField', {
	name: '',
	value: ''
});
glu.defView('RS.license.AddLicense', {
	asWindow: {
		title: '~~addLicense~~',
		cls : 'rs-modal-popup',
		height: 500,
		width: 800,
		modal: true,
		padding : 15,
		closable: true
	},

	dockedItems: [{
		xtype: 'toolbar',
		ui: 'display-toolbar',
		cls : 'rs-dockedtoolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			name: 'uploadLicense',
			handler: function() {
				this.fireEvent('uploadLicense');
			},
			listeners: {
				uploadLicense: '@{uploadLicense}'
			}
		}, {
			name: 'pasteLicense',
			handler: function() {
				this.fireEvent('pasteLicense');
			},
			listeners: {
				pasteLicense: '@{pasteLicense}'
			}
		}]
	}],

	layout: 'card',
	defaultType: 'panel',
	defaults: {
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		padding: '10 0 0 0',
	},
	activeItem: '@{activeItem}',
	items: [{
		items: [{
			html: '@{intro}'
		}, {
			xtype: 'textarea',
			padding: '10 0 0 0',
			flex: 1,
			hideLabel: true,
			name: 'encrypt'
		}]
	}, {
		defaultType: 'panel',
		items: [{
			html: '@{intro}',
			margin: '0 0 10 0'
		}, {
			itemId: 'fineUploaderTemplate',
			html: '@{fineUploaderTemplate}',
			height: '@{fineUploaderTemplateHeight}',
			hidden: '@{fineUploaderTemplateHidden}',
		}, {
			id: 'attachments_grid',
			hidden: true
		}]
	}],
	
	listeners: {
		uploadComplete: function() {
			var fineuploader = this.down('#fineUploaderTemplate');
			fineuploader.hide();
		}
	},

	buttons: [{
        name: 'browse',
		id: 'upload_button',
        cls : 'rs-med-btn rs-btn-dark',
		preventDefault: false,
    },{
        name: 'submitText',
        cls : 'rs-med-btn rs-btn-dark'
	},{
        name: 'close',
		text: '~~cancel~~',
        cls : 'rs-med-btn rs-btn-light'
	}]
});

/*
glu.defView('RS.license.AddLicense', {
	bodyPadding: '10px',
	dockedItems: [{
		xtype: 'toolbar',
		items: [{
			name: 'upload',
			handler: function() {
				this.fireEvent('upload');
				var file = Ext.query('input[type="file"]')[0];
				Ext.fly(file.parentNode).setStyle('z-index', '0');
			},
			listeners: {
				upload: '@{upload}'
			}
		}, {
			name: 'paste',
			handler: function() {
				this.fireEvent('paste');
				var file = Ext.query('input[type="file"]')[0];
				Ext.fly(file.parentNode).setStyle('z-index', '-1');
			},
			listeners: {
				paste: '@{paste}'
			}
		}]
	}],
	layout: 'card',
	defaultType: 'panel',
	defaults: {
		layout: {
			type: 'vbox',
			align: 'stretch'
		}
	},
	activeItem: '@{activeItem}',
	items: [{
		items: [{
			html: '@{intro}'
		}, {
			xtype: 'textarea',
			padding: '10 0 0 0',
			flex: 1,
			hideLabel: true,
			name: 'encrypt'
		}]
	}, {
		defaultType: 'panel',
		items: [{
			html: '@{intro}'
		}, {
			flex: 1,
			itemId: 'dropzone',
			hidden: '@{!dndIsVisible}',
			layout: {
				type: 'vbox',
				pack: 'center',
				align: 'center'
			},
			style: 'border: 2px dashed #ccc;margin: 10px 0px 0px 0px',
			items: [{
				xtype: 'label',
				text: '~~dragHere~~',
				style: {
					'font-size': '30px',
					'color': '#CCCCCC'
				}
			}]
		}]
	}],

	getConfig: function() {
		return {
			autoStart: true,
			url: '/resolve/service/license/upload',
			filters: [{
				title: "License Files",
				extensions: "lic"
			}],
			browse_button: this.getBrowseButton(),
			drop_element: this.getDropArea(),
			container: this.getContainer(),
			chunk_size: null,
			runtimes: "html5,flash,silverlight,html4",
			flash_swf_url: '/resolve/js/plupload/Moxie.swf',
			unique_names: true,
			multipart: true,
			multipart_params: {},
			multi_selection: false,
			required_features: null,
			ownerCt: this
		}
	},
	getBrowseButton: function() {
		return this.down('#browse').btnEl.dom.id;
	},
	getDropArea: function() {
		return this.down('#dropzone').getEl().dom.id;
	},
	getContainer: function() {
		var btn = this.down('#browse');
		return btn.ownerCt.id;
	},
	uploaderListeners: {
		filesadded: function(uploader, data) {
			this.owner.fireEvent('filesAdded', this.owner, uploader, data);
		},
		uploadprogress: function(uploader, file, name, size, percent) {
			this.owner.percent = file.percent;
		},
		fileuploaded: function(uploader, file, response) {
			this.owner.fireEvent('fileUploaded', this.owner, uploader, response);
		}
	},

	listeners: {
		filesAdded: '@{filesAdded}',
		fileUploaded: '@{uploadComplete}',
		afterrender: function() {
			this.myUploader = Ext.create('RS.common.SinglePlupUpload', this);
			this.uploader = this.getConfig();
			this.myUploader.initialize(this);
		}
	},
	buttonAlign: 'left',
	buttons: [{
		name: 'browse',
		itemId: 'browse'
	}, 'submitText', 'cancel'],
	asWindow: {
		title: '~~addLicense~~',
		modal: true,
		height: 400,
		width: 500
	}
})
*/
glu.defView('RS.license.License', {
	layout: 'border',
	padding : '@{padding}',
	xtype: 'panel',
	autoScroll: true,
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		hidden: '@{isModal}',
		cls: 'actionBar actionBar-form',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{licenseTitle}'
		}]
	}, {
		xtype: 'toolbar',
		hidden: '@{isModal}',
		cls: 'actionBar actionBar-form',
		//defaultButtonUI: 'display-toolbar-button',
		padding: '10',
		items: [{
			name: 'back',
			cls : 'rs-small-btn rs-btn-light',
			iconCls: 'icon-large icon-reply-all rs-icon'
		}, '->', {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			tooltip: '~~refresh~~'
		}]
	}, {
		xtype: 'toolbar',
		hidden: '@{!isModal}',
		dock: 'top',
		cls: 'actionBar actionBar-form',
		items: ['->', {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}'
		}] 
	}],

	layout: {
		type: 'hbox',
		align: 'stretch',
		pack: 'start'
	},

	items: [{
		flex: 1,
		autoScroll: true,
		minWidth: 700,
		padding: '10px',
		xtype: 'panel',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},

		defaultType: 'textfield',
		items: [{
			xtype: 'panel',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			defaults: {
				flex: 1,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaults : {
					margin : '4 0'
				},
				defaultType: 'displayfield'
			},		
			items: [{							
				items: ['key', 'customer', 'ha', 'coresDisplay', 'endUserCountDisplay' /*, 'adminUserCountDisplay'*/ ]
			}, {				
				items: ['type', 'createdOn', 'uploadedOn', 'duration', /*'startedOn'*/ {
					name: 'expiredOn',
					fieldStyle: 'color:@{color} !important'
				}]
			}]
		}, {
			xtype: 'panel',
			minHeight: 300,
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			defaults: {
				flex: 1,
				cls : 'rs-grid-dark',
				margin : '0 15 0 0',
				layout: {
					type: 'vbox',
					align: 'stretch'
				}
			},
			defaultType: 'grid',
			items: [{
				title: '~~ipAddress~~',
				name: 'ipAddress',
				store: '@{ipStore}',				
				columns: [{
					header: '~~ipAddress~~',
					dataIndex: 'ipAddress',
					flex: 1
				}]
			}, {
				title: '~~maxMemory~~',
				name: 'maxMemory',
				store: '@{memStore}',			
				columns: [{
					header: '~~component~~',
					dataIndex: 'component',
					flex: 1
				}, {
					header: '~~maxMemory~~',
					dataIndex: 'maxMemory',
					flex: 1
				}]
			}, {
				title: '~~gateway~~',
				name: 'gateway',
				store: '@{gatewayStore}',
				margin : 0,
				columns: [{
					header: '~~gateway~~',
					dataIndex: 'gateway',
					flex: 1
				}, {
					header: '~~quantity~~',
					dataIndex: 'quantity',
					flex: 1
				}]
			}]
		}]
	}],

	asWindow: {
		title: '~~licenseTitle~~',
		modal: true,
		width: 800,
		cls : 'rs-modal-popup',
		padding : 15,
		closable: true,
		buttons: [{
			name: 'apply',
			cls : 'rs-med-btn rs-btn-dark'
		}, {
			name: 'cancel',
			cls : 'rs-med-btn rs-btn-light'
		}],
	}
});
glu.defView('RS.license.Main', {
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	defaults: {
		flex: 1
	},
	padding : 15,
	items: [{
		region: 'west',
		split: true,
		minWidth: 400,
		xtype: 'panel',
		padding: '0 5 0 0',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},

		items: [{
			xtype: 'panel',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				title: '~~generalEntitlement~~',
				xtype: 'panel',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				defaultType: 'panel',
				defaults: {
					flex: 1
				},
				items: [{
					defaultType: 'displayfield',
					items: ['cores', 'ha', 'endUserCount' /*, 'adminUserCount' */ ]
				}]
			}, {
				xtype: 'panel',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaults: {
					flex: 1
				},
				items: [{
					title: '~~maxMemory~~',
					padding: '0 0 0 5px',
					xtype: 'panel',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: '@{memoryFields}'
				}, {
					title: '~~connectors~~',
					padding: '0 5px 0 0',
					xtype: 'panel',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: '@{gatewayFields}'
				}]
			}]
		}]

	}, {
		region: 'center',
		xtype: 'panel',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		minWidth: 730,
		padding: '0 0 0 5',
		items: [{
			xtype: 'grid',
			cls : 'rs-grid-dark',
			name: 'license',
			title: '~~license~~',
			border: false,
			stateId: '@{stateId}',
			displayName: '@{displayName}',
			stateful: true,
			plugins: [{
				ptype: 'pager',
				pageable: true,
				showSysInfo: false
			}],
			selModel: {
				selType: 'resolvecheckboxmodel',
				columnTooltip: RS.common.locale.editColumnTooltip,
				columnTarget: '_self',
				columnEventName: 'editAction',
				columnIdField: 'key'
			},
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar rs-dockedtoolbar',
				name: 'actionBar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items: ['add', 'deleteRecord']
			}],
			columns: '@{columns}',
			viewConfig: {
				enableTextSelection: true
			},
			listeners: {
				editAction: '@{editRecord}',
				selectAllAcrossPages: '@{selectAllLicenseAcrossPages}',
				selectionChange: '@{selectionChangedLicense}',
				refresh: '@{refresh}',
				render: function(license) {
					var btn = this.down('#refresh');
					btn.on('click', function() {
						license.fireEvent('refresh');
					});
				}
			}
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			name: 'alertLog',
			title: '~~alertLog~~',
			border: false,
			stateId: '@{stateIdLog}',
			displayName: '@{displayName}',
			stateful: true,
			plugins: [{
				ptype: 'pager',
				pageable: true,
				pageSize: 25,
				showSysInfo: false
			}],
			selModel: {
				selType: 'resolvecheckboxmodel',
				columnTooltip: RS.common.locale.editColumnTooltip,
				columnTarget: '_self',
				columnEventName: 'editAction',
				columnIdField: 'id'
			},
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar rs-dockedtoolbar',
				name: 'actionBar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items: ['deleteLog']
			}],
			columns: '@{alertLogColumns}',
			viewConfig: {
				enableTextSelection: true
			},
			listeners: {
				editAction: '@{editLog}',
				selectAllAcrossPages: '@{selectAllLogsAcrossPages}',
				selectionChange: '@{selectionChangedLog}',
				refresh: '@{refresh}',
				render: function(alertLog) {
					var btn = this.down('#refresh');
					btn.on('click', function() {
						alertLog.fireEvent('refresh');
					});
				}
			}
		}]
	}]
});

glu.defView('RS.license.SummaryField', {
	xtype: 'displayfield',
	fieldLabel: '@{name}',
	value: '@{value}'
});
/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.license').locale = {
	windowTitle: 'License Project',
	type: 'Type',
	key: 'Key',
	duration: 'Duration',
	uploadDate: 'Upload Date',
	expirationDate: 'Expiration Date',
	deleteDate: 'Delete Date',
	ipAddress: 'IP Address',
	maxMemory: 'Maximum Memory',
	component: 'Component',
	message: 'Message',
	gateway: 'Gateway',
	quantity: 'Quantity',
	Main: {
		license: 'License',
		alertLog: 'Alert Log',
		add: 'Add',
		deleteRecord: 'Delete',
		deleteLog: 'Delete',
		deleteTitle: 'Delete',
		deleteAction: 'Delete',
		deleteMessage: 'Are you sure you want to delete the selected record?',
		deletesMessage: 'Are you sure you want to delete the selected records({0} selected)?',
		deleteSuccess: 'The selected records are deleted.',
		cancel: 'Cancel',
		cores: 'vCPU',
		ha: 'HA',
		endUserCount: 'Users',
		adminUserCount: 'Admins',
		generalEntitlement: 'General Entitlement',
		connectors: 'Connectors',
		maxMemory: 'Max Memory',
		listLicenseErr: 'Cannot get Licenses from the server!',
		getSummaryErr: 'Cannot get the summary from the server.[{0}]'
	},

	License: {
		licenseTitle: 'License',
		save: 'Save',
		customer: 'Customer',
		expired: 'Expired',
		ipAddress: 'IP Address',
		coresDisplay: 'vCPUs',
		endUserCountDisplay: 'End User',
		adminUserCountDisplay: 'Admin User',
		maxMemory: 'Maximum Memory',
		createdOn: 'Created On',
		uploadedOn: 'Uploaded On',
		startedOn: 'Started On',
		expiredOn: 'Expires On',
		ha: 'HA',
		gateway: 'Gateways',
		apply: 'Apply',
		cancel: 'Cancel',
		refresh: 'Refresh',
		back: 'Back'
	},

	AddLicense: {
		cancel: 'Cancel',
		addLicense: 'Add License',
		uploadLicense: 'Upload License File',
		browse: 'Browse',
		uploadIntro: 'Please click Browse button to select a file for uploading or drag the license file into the area below.',
		pasteIntro: 'Please paste the encrpyted license data into the textarea below.',
		noDndUploadIntro: 'Please click Browse button to select a file for uploading.',
		dragHere: 'Drop your license file here',
		pasteLicense: 'Paste Encrypt License Data',
		submitText: 'Submit',
		sendingData: 'Sending license data to server...',
		submitLicenseErr: 'Cannot send license data to the server.[{0}]'
	}
}
