glu.defView('RS.license.Main', {
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	defaults: {
		flex: 1
	},
	padding : 15,
	items: [{
		region: 'west',
		split: true,
		minWidth: 400,
		xtype: 'panel',
		padding: '0 5 0 0',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},

		items: [{
			xtype: 'panel',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				title: '~~generalEntitlement~~',
				xtype: 'panel',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				defaultType: 'panel',
				defaults: {
					flex: 1
				},
				items: [{
					defaultType: 'displayfield',
					items: ['cores', 'ha', 'endUserCount' /*, 'adminUserCount' */ ]
				}]
			}, {
				xtype: 'panel',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaults: {
					flex: 1
				},
				items: [{
					title: '~~maxMemory~~',
					padding: '0 0 0 5px',
					xtype: 'panel',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: '@{memoryFields}'
				}, {
					title: '~~connectors~~',
					padding: '0 5px 0 0',
					xtype: 'panel',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: '@{gatewayFields}'
				}]
			}]
		}]

	}, {
		region: 'center',
		xtype: 'panel',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		minWidth: 730,
		padding: '0 0 0 5',
		items: [{
			xtype: 'grid',
			cls : 'rs-grid-dark',
			name: 'license',
			title: '~~license~~',
			border: false,
			stateId: '@{stateId}',
			displayName: '@{displayName}',
			stateful: true,
			plugins: [{
				ptype: 'pager',
				pageable: true,
				showSysInfo: false
			}],
			selModel: {
				selType: 'resolvecheckboxmodel',
				columnTooltip: RS.common.locale.editColumnTooltip,
				columnTarget: '_self',
				columnEventName: 'editAction',
				columnIdField: 'key'
			},
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar rs-dockedtoolbar',
				name: 'actionBar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items: ['add', 'deleteRecord']
			}],
			columns: '@{columns}',
			viewConfig: {
				enableTextSelection: true
			},
			listeners: {
				editAction: '@{editRecord}',
				selectAllAcrossPages: '@{selectAllLicenseAcrossPages}',
				selectionChange: '@{selectionChangedLicense}',
				refresh: '@{refresh}',
				render: function(license) {
					var btn = this.down('#refresh');
					btn.on('click', function() {
						license.fireEvent('refresh');
					});
				}
			}
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			name: 'alertLog',
			title: '~~alertLog~~',
			border: false,
			stateId: '@{stateIdLog}',
			displayName: '@{displayName}',
			stateful: true,
			plugins: [{
				ptype: 'pager',
				pageable: true,
				pageSize: 25,
				showSysInfo: false
			}],
			selModel: {
				selType: 'resolvecheckboxmodel',
				columnTooltip: RS.common.locale.editColumnTooltip,
				columnTarget: '_self',
				columnEventName: 'editAction',
				columnIdField: 'id'
			},
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar rs-dockedtoolbar',
				name: 'actionBar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items: ['deleteLog']
			}],
			columns: '@{alertLogColumns}',
			viewConfig: {
				enableTextSelection: true
			},
			listeners: {
				editAction: '@{editLog}',
				selectAllAcrossPages: '@{selectAllLogsAcrossPages}',
				selectionChange: '@{selectionChangedLog}',
				refresh: '@{refresh}',
				render: function(alertLog) {
					var btn = this.down('#refresh');
					btn.on('click', function() {
						alertLog.fireEvent('refresh');
					});
				}
			}
		}]
	}]
});

glu.defView('RS.license.SummaryField', {
	xtype: 'displayfield',
	fieldLabel: '@{name}',
	value: '@{value}'
});