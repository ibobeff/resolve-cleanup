glu.defView('RS.license.License', {
	layout: 'border',
	padding : '@{padding}',
	xtype: 'panel',
	autoScroll: true,
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		hidden: '@{isModal}',
		cls: 'actionBar actionBar-form',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{licenseTitle}'
		}]
	}, {
		xtype: 'toolbar',
		hidden: '@{isModal}',
		cls: 'actionBar actionBar-form',
		//defaultButtonUI: 'display-toolbar-button',
		padding: '10',
		items: [{
			name: 'back',
			cls : 'rs-small-btn rs-btn-light',
			iconCls: 'icon-large icon-reply-all rs-icon'
		}, '->', {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			tooltip: '~~refresh~~'
		}]
	}, {
		xtype: 'toolbar',
		hidden: '@{!isModal}',
		dock: 'top',
		cls: 'actionBar actionBar-form',
		items: ['->', {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}'
		}] 
	}],

	layout: {
		type: 'hbox',
		align: 'stretch',
		pack: 'start'
	},

	items: [{
		flex: 1,
		autoScroll: true,
		minWidth: 700,
		padding: '10px',
		xtype: 'panel',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},

		defaultType: 'textfield',
		items: [{
			xtype: 'panel',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			defaults: {
				flex: 1,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaults : {
					margin : '4 0'
				},
				defaultType: 'displayfield'
			},		
			items: [{							
				items: ['key', 'customer', 'ha', 'coresDisplay', 'endUserCountDisplay' /*, 'adminUserCountDisplay'*/ ]
			}, {				
				items: ['type', 'createdOn', 'uploadedOn', 'duration', /*'startedOn'*/ {
					name: 'expiredOn',
					fieldStyle: 'color:@{color} !important'
				}]
			}]
		}, {
			xtype: 'panel',
			minHeight: 300,
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			defaults: {
				flex: 1,
				cls : 'rs-grid-dark',
				margin : '0 15 0 0',
				layout: {
					type: 'vbox',
					align: 'stretch'
				}
			},
			defaultType: 'grid',
			items: [{
				title: '~~ipAddress~~',
				name: 'ipAddress',
				store: '@{ipStore}',				
				columns: [{
					header: '~~ipAddress~~',
					dataIndex: 'ipAddress',
					flex: 1
				}]
			}, {
				title: '~~maxMemory~~',
				name: 'maxMemory',
				store: '@{memStore}',			
				columns: [{
					header: '~~component~~',
					dataIndex: 'component',
					flex: 1
				}, {
					header: '~~maxMemory~~',
					dataIndex: 'maxMemory',
					flex: 1
				}]
			}, {
				title: '~~gateway~~',
				name: 'gateway',
				store: '@{gatewayStore}',
				margin : 0,
				columns: [{
					header: '~~gateway~~',
					dataIndex: 'gateway',
					flex: 1
				}, {
					header: '~~quantity~~',
					dataIndex: 'quantity',
					flex: 1
				}]
			}]
		}]
	}],

	asWindow: {
		title: '~~licenseTitle~~',
		modal: true,
		width: 800,
		cls : 'rs-modal-popup',
		padding : 15,
		closable: true,
		buttons: [{
			name: 'apply',
			cls : 'rs-med-btn rs-btn-dark'
		}, {
			name: 'cancel',
			cls : 'rs-med-btn rs-btn-light'
		}],
	}
});