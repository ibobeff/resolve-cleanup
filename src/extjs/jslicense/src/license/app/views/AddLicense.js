glu.defView('RS.license.AddLicense', {
	asWindow: {
		title: '~~addLicense~~',
		cls : 'rs-modal-popup',
		height: 500,
		width: 800,
		modal: true,
		padding : 15,
		closable: true
	},

	dockedItems: [{
		xtype: 'toolbar',
		ui: 'display-toolbar',
		cls : 'rs-dockedtoolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			name: 'uploadLicense',
			handler: function() {
				this.fireEvent('uploadLicense');
			},
			listeners: {
				uploadLicense: '@{uploadLicense}'
			}
		}, {
			name: 'pasteLicense',
			handler: function() {
				this.fireEvent('pasteLicense');
			},
			listeners: {
				pasteLicense: '@{pasteLicense}'
			}
		}]
	}],

	layout: 'card',
	defaultType: 'panel',
	defaults: {
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		padding: '10 0 0 0',
	},
	activeItem: '@{activeItem}',
	items: [{
		items: [{
			html: '@{intro}'
		}, {
			xtype: 'textarea',
			padding: '10 0 0 0',
			flex: 1,
			hideLabel: true,
			name: 'encrypt'
		}]
	}, {
		defaultType: 'panel',
		items: [{
			html: '@{intro}',
			margin: '0 0 10 0'
		}, {
			itemId: 'fineUploaderTemplate',
			html: '@{fineUploaderTemplate}',
			height: '@{fineUploaderTemplateHeight}',
			hidden: '@{fineUploaderTemplateHidden}',
		}, {
			id: 'attachments_grid',
			hidden: true
		}]
	}],
	
	listeners: {
		uploadComplete: function() {
			var fineuploader = this.down('#fineUploaderTemplate');
			fineuploader.hide();
		}
	},

	buttons: [{
        name: 'browse',
		id: 'upload_button',
        cls : 'rs-med-btn rs-btn-dark',
		preventDefault: false,
    },{
        name: 'submitText',
        cls : 'rs-med-btn rs-btn-dark'
	},{
        name: 'close',
		text: '~~cancel~~',
        cls : 'rs-med-btn rs-btn-light'
	}]
});

/*
glu.defView('RS.license.AddLicense', {
	bodyPadding: '10px',
	dockedItems: [{
		xtype: 'toolbar',
		items: [{
			name: 'upload',
			handler: function() {
				this.fireEvent('upload');
				var file = Ext.query('input[type="file"]')[0];
				Ext.fly(file.parentNode).setStyle('z-index', '0');
			},
			listeners: {
				upload: '@{upload}'
			}
		}, {
			name: 'paste',
			handler: function() {
				this.fireEvent('paste');
				var file = Ext.query('input[type="file"]')[0];
				Ext.fly(file.parentNode).setStyle('z-index', '-1');
			},
			listeners: {
				paste: '@{paste}'
			}
		}]
	}],
	layout: 'card',
	defaultType: 'panel',
	defaults: {
		layout: {
			type: 'vbox',
			align: 'stretch'
		}
	},
	activeItem: '@{activeItem}',
	items: [{
		items: [{
			html: '@{intro}'
		}, {
			xtype: 'textarea',
			padding: '10 0 0 0',
			flex: 1,
			hideLabel: true,
			name: 'encrypt'
		}]
	}, {
		defaultType: 'panel',
		items: [{
			html: '@{intro}'
		}, {
			flex: 1,
			itemId: 'dropzone',
			hidden: '@{!dndIsVisible}',
			layout: {
				type: 'vbox',
				pack: 'center',
				align: 'center'
			},
			style: 'border: 2px dashed #ccc;margin: 10px 0px 0px 0px',
			items: [{
				xtype: 'label',
				text: '~~dragHere~~',
				style: {
					'font-size': '30px',
					'color': '#CCCCCC'
				}
			}]
		}]
	}],

	getConfig: function() {
		return {
			autoStart: true,
			url: '/resolve/service/license/upload',
			filters: [{
				title: "License Files",
				extensions: "lic"
			}],
			browse_button: this.getBrowseButton(),
			drop_element: this.getDropArea(),
			container: this.getContainer(),
			chunk_size: null,
			runtimes: "html5,flash,silverlight,html4",
			flash_swf_url: '/resolve/js/plupload/Moxie.swf',
			unique_names: true,
			multipart: true,
			multipart_params: {},
			multi_selection: false,
			required_features: null,
			ownerCt: this
		}
	},
	getBrowseButton: function() {
		return this.down('#browse').btnEl.dom.id;
	},
	getDropArea: function() {
		return this.down('#dropzone').getEl().dom.id;
	},
	getContainer: function() {
		var btn = this.down('#browse');
		return btn.ownerCt.id;
	},
	uploaderListeners: {
		filesadded: function(uploader, data) {
			this.owner.fireEvent('filesAdded', this.owner, uploader, data);
		},
		uploadprogress: function(uploader, file, name, size, percent) {
			this.owner.percent = file.percent;
		},
		fileuploaded: function(uploader, file, response) {
			this.owner.fireEvent('fileUploaded', this.owner, uploader, response);
		}
	},

	listeners: {
		filesAdded: '@{filesAdded}',
		fileUploaded: '@{uploadComplete}',
		afterrender: function() {
			this.myUploader = Ext.create('RS.common.SinglePlupUpload', this);
			this.uploader = this.getConfig();
			this.myUploader.initialize(this);
		}
	},
	buttonAlign: 'left',
	buttons: [{
		name: 'browse',
		itemId: 'browse'
	}, 'submitText', 'cancel'],
	asWindow: {
		title: '~~addLicense~~',
		modal: true,
		height: 400,
		width: 500
	}
})
*/