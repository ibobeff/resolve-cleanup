glu.defModel('RS.license.Main', {

	mock: false,
	stateLog: 'init',
	activate: function() {
		window.document.title = this.localize('windowTitle')
		this.loadSummary();
		this.license.load();
		this.alertLog.load({
			scope: this,
			callback: function() {
				this.set('stateLog', 'ready');
			}
		});
	},

	refresh: function() {
		this.loadSummary();
		this.license.load();
		this.alertLog.load({
			scope: this,
			callback: function() {
				this.set('stateLog', 'ready');
			}
		});
	},

	displayName: '~~License Grid~~',
	stateId: 'rslicense',
	stateIdLog: 'rslicenselog',
	reqCount: 0,
	logReqCount: 0,
	cores: 0,
	ha: false,
	endUserCount: 0,
	adminUserCount: 0,
	fields: ['cores', 'ha', 'endUserCount', 'adminUserCount'],

	gatewayFields: {
		mtype: 'list'
	},
	memoryFields: {
		mtype: 'list'
	},
	license: {
		mtype: 'store',
		pageable: false,
		fields: ['key', 'type', 'duration', {
			name: 'expirationDate',
			type: 'date',
			dateFormat: 'time'
		}, {
			name: 'deleteDate',
			type: 'date',
			dateFormat: 'time'
		}, {
			name: 'uploadDate',
			type: 'date',
			dateFormat: 'time'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/license/licenses',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	alertLog: {
		mtype: 'store',
		fields: ['useverity', 'umessage', 'ucomponent', 'utype'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		pageSize: 10,
		sorters: [{
			property: 'sysCreatedOn',
			direction: 'DESC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/alertlog/getLicenseLog',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	columns: null,
	alertLogColumns: null,

	allLicenseSelected: false,
	licenseSelections: [],

	allLogsSelected: false,
	alertLogSelections: [],

	init: function() {
		//Called when mocking the backend to test the UI
		if (this.mock) this.backend = RS.license.createMockBackend(true)

		this.set('displayName', this.localize('license'))
		var cols = [{
			header: '~~key~~',
			dataIndex: 'key',
			filterable: true,
			sortable: true,
			flex: 2
		}, {
			header: '~~type~~',
			dataIndex: 'type',
			filterable: true,
			sortable: true,
			width: 100
		}, {
			header: '~~duration~~',
			dataIndex: 'duration',
			filterable: true,
			sortable: true,
			width: 100
		}, {
			header: '~~uploadDate~~',
			dataIndex: 'uploadDate',
			filterable: true,
			sortable: true,
			flex: 1,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}, {
			header: '~~expirationDate~~',
			dataIndex: 'expirationDate',
			filterable: true,
			sortable: true,
			flex: 1,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}]
		Ext.each(cols, function(c) {
			var r = c.renderer;
			c.renderer = function(value, meta, record) {
				var v = value;
				if (record.get('expired'))
					meta.style = 'background-color:red !important';
				if (r)
					v = r(value);
				return v;
			}
		});
		this.set('columns', cols);
		this.license.on('beforeload', function() {
			this.set('reqCount', this.reqCount + 1);
		}, this);
		this.license.on('load', function(store, op, success) {
			this.set('reqCount', this.reqCount - 1);
			if (!success)
				clientVM.displayError(this.localize('listLicenseErr'));
		}, this);

		//Alert log columns (actually sysLog)
		if (this.mock) this.backend = RS.syslog.createMockBackend(true);
		var alertLogCols = RS.common.grid.getSysColumns();
		Ext.each(alertLogCols, function(col) {
			col.hidden = !(col.dataIndex == 'sysCreatedOn');
		});
		this.set('alertLogColumns', [{
			header: '~~message~~',
			dataIndex: 'umessage',
			filterable: true,
			flex: 1,
			renderer: RS.common.grid.logRenderer(4)
		}, {
			header: '~~component~~',
			dataIndex: 'ucomponent',
			filterable: true,
			width: 330
			/*		}, {
			header: '~~severity~~',
			dataIndex: 'useverity',
			filterable: true,
			width: 150 */
		}, {
			header: '~~type~~',
			dataIndex: 'utype',
			filterable: true,
			width: 75
		}].concat(alertLogCols));
		this.set('stateLog', 'waitGetAlertLogResponse');
		this.set('displayName', this.localize('displayName'));
	},

	selectAllLicenseAcrossPages: function(selectAll) {
		this.set('allLicenseSelected', selectAll)
	},
	selectionChangedLicense: function() {

		this.selectAllLicenseAcrossPages(false)
	},

	selectAllLogsAcrossPages: function(selectAll) {
		this.set('allLogsSelected', selectAll)
	},

	selectionChangedLog: function() {
		this.selectAllLicenseAcrossPages(false)
	},

	add: function() {
		this.open({
			mtype: 'RS.license.AddLicense'
		});
	},

	addIsEnabled$: function() {
		return this.reqCount == 0;
	},

	editRecord: function(key) {
		clientVM.handleNavigation({
			modelName: 'RS.license.License',
			params: {
				key: key
			}
		});
	},

	loadSummary: function() {
		this.set('reqCount', this.reqCount + 1);
		this.ajax({
			url: '/resolve/service/license/summary',
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('getSummaryErr', respData.message));
					return;
				}

				Ext.Object.each(respData.data, function(k, v) {
					respData.data[k] = (v === 0 ? 'Unlimited' : v);
				});
				this.loadData(respData.data);
				this.gatewayFields.removeAll();
				Ext.Object.each(respData.data.gateway, function(k, v) {
					this.gatewayFields.add(glu.model({
						mtype: 'RS.license.SummaryField',
						name: k,
						value: v.instanceCount == 0 ? 'Unlimited' : v.instanceCount
					}));
				}, this);
				this.memoryFields.removeAll();
				Ext.Object.each(respData.data.maxMemory, function(k, v) {
					this.memoryFields.add(glu.model({
						mtype: 'RS.license.SummaryField',
						name: k,
						value: v == 0 ? 'Unlimited' : v
					}));
				}, this);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('reqCount', this.reqCount - 1);
			}
		});
	},

	singleRecordSelected$: function() {
		return this.licenseSelections.length == 1
	},
	editRecordIsEnabled$: function() {
		return this.singleRecordSelected
	},

	deleteRecordIsEnabled$: function() {
		return this.licenseSelections.length > 0 && this.reqCount == 0;
	},
	deleteRecord: function() {
		//Delete the selected record(s), but first confirm the action
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.licenseSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.licenseSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			this.set('reqCount', this.reqCount + 1);
			if (this.allLicenseSelected) {
				this.ajax({
					url: '/resolve/service/license/delete',
					params: {
						keys: '',
						whereClause: this.catalogs.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.refresh();
							clientVM.updateLicenseBanner();
							clientVM.displaySuccess(this.localize('deleteSuccess'))
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					},
					callback: function() {
						this.set('reqCount', this.reqCount - 1);
					}
				})
			} else {
				var ids = []
				Ext.each(this.licenseSelections, function(selection) {
					ids.push(selection.get('key'))
				})
				this.ajax({
					url: '/resolve/service/license/delete',
					params: {
						keys: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.refresh();
							clientVM.updateLicenseBanner();
							clientVM.displaySuccess(this.localize('deleteSuccess'))
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					},
					callback: function() {
						this.set('reqCount', this.reqCount - 1);
					}
				})
			}
		}
	},

	singleLogSelected$: function() {
		return this.alertLogSelections.length == 1
	},
	deleteLogIsEnabled$: function() {
		return this.alertLogSelections.length > 0 && this.logReqCount == 0;
	},
	editLog: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.syslog.AlertLogException',
			target: '_blank',
			params: {
				sys_id: id
			}
		});
	},
	deleteLog: function() {
		//Delete the selected record(s), but first confirm the action
		this.set('stateLog', 'confirmDelete');
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.alertLogSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.alertLogSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteLogs
		})
	},
	reallyDeleteLogs: function(btn) {
		if (btn === 'no') {
			this.set('stateLog', 'itemSelected');
			return;
		}
		var ids = '';
		for (var i = 0; i < this.alertLogSelections.length; i++) {
			ids += this.alertLogSelections[i].data.id + ',';
		}
		this.ajax({
			url: '/resolve/service/alertlog/deleteAlertLogRecs',
			params: {
				ids: ids
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					this.set('stateLog', 'deleteFailure');
					clientVM.displayError(this.localize('deleteFailureMsg') + '[' + respData.message + ']');
				}
				this.alertLog.load({
					scope: this,
					callback: function() {
						this.set('stateLog', 'ready');
					}
				});
				this.set('stateLog', 'waitGetAlertLogResponse');
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
		this.set('stateLog', 'waitDeleteResponse');
	}
});

glu.defModel('RS.license.SummaryField', {
	name: '',
	value: ''
});