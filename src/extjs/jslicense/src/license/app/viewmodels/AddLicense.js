glu.defModel('RS.license.AddLicense',{
	mixins: ['FileUploadManager'],
	api: {
		list: '/resolve/service/license/licenses',
		upload: '/resolve/service/license/upload',
	},

	intro: '',
	encrypt: '',
	activeItem: 1,
	wait: false,

	allowedExtensions: ['lic'],
	fineUploaderTemplateHidden: false,
	checkForDuplicates: false,
	allowMultipleFiles: false,

	uploaderAdditionalClass: 'license',

	init: function() {
		this.set('wait', false);
		this.set('dragHereText', this.localize('dragHere'));
		this.set('intro', this.activeItem == 0 ? this.localize('pasteIntro') : this.localize(Ext.isIE8m ? 'noDndUploadIntro' : 'uploadIntro'));
	},

	fileOnUploadCallback: function(manager, filename) {
		this.set('wait', true);
	},

	fileOnCompleteCallback: function(manager, filename, response, xhr) {
        this.set('wait', false);
		if (response.success) {
			this.open({
				mtype: 'RS.license.License',
				license: response.data,
				isModal: true
			});
			this.doClose();
		}
	},

	filesOnAllCompleteCallback: function(manager) {
	},

	activeItemChanged$: function() {
		this.set('intro', this.activeItem == 0 ? this.localize('pasteIntro') : this.localize(Ext.isIE8m ? 'noDndUploadIntro' : 'uploadIntro'));
	},

	uploadLicense: function() {
		this.set('activeItem', 1);
	},
	uploadLicenseIsPressed$: function() {
		return this.activeItem == 1;
	},
	pasteLicense: function() {
		this.set('activeItem', 0);
	},
	pasteLicenseIsPressed$: function() {
		return this.activeItem == 0;
	},

	submitText: function() {
		this.set('wait', true);
		this.set('intro', this.localize('sendingData'));
		this.ajax({
			url: '/resolve/service/license/submit',
			params: {
				data: this.encrypt
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('submitLicenseErr', respData.message));
					return;
				}
				this.open({
					mtype: 'RS.license.License',
					license: respData.data,
					isModal: true
				});
				this.doClose();
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},
	submitTextIsVisible$: function() {
		return this.activeItem == 0;
	},
	submitTextIsEnabled$: function() {
		return !this.wait && !! this.encrypt;
	},
	browse: function() {
	},
	browseIsVisible$: function() {
		return this.activeItem == 1;
	},
	browseIsEnabled$: function() {
		return !this.wait;
	}
});

glu.defModel('RS.license.FileUploadManagerFactory', function(cfg) {
	Ext.applyIf(cfg, RS.common.viewmodels.FileUploadManager);
	return cfg;
});

/*
glu.defModel('RS.license.AddLicense', {
	intro: '',
	encrypt: '',
	activeItem: 1,
	wait: false,
	upload: function() {
		this.set('activeItem', 1);
	},
	uploadIsPressed$: function() {
		return this.activeItem == 1;
	},
	paste: function() {
		this.set('activeItem', 0);
	},
	pasteIsPressed$: function() {
		return this.activeItem == 0;
	},
	submitText: function() {
		this.set('wait', true);
		this.set('intro', this.localize('sendingData'));
		this.ajax({
			url: '/resolve/service/license/submit',
			params: {
				data: this.encrypt
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('submitLicenseErr', respData.message));
					return;
				}
				this.open({
					mtype: 'RS.license.License',
					license: respData.data
				});
				this.doClose();
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},
	submitTextIsVisible$: function() {
		return this.activeItem == 0;
	},
	submitTextIsEnabled$: function() {
		return !this.wait && !! this.encrypt;
	},
	browse: function() {

	},
	browseIsVisible$: function() {
		return this.activeItem == 1;
	},
	browseIsEnabled$: function() {
		return !this.wait;
	},

	activeItemChanged$: function() {
		this.set('intro', this.activeItem == 0 ? this.localize('pasteIntro') : this.localize(Ext.isIE8m ? 'noDndUploadIntro' : 'uploadIntro'));
	},
	dndIsVisible$: function() {
		return this.activeItem == 1 && !Ext.isIE8m && !this.wait;
	},

	filesAdded: function(uploader) {
		this.set('wait', true);
		this.set('intro', this.localize('sendingData'));
	},

	uploadComplete: function(uploader, response) {
		this.set('wait', false);
		if (!response.success) {
			clientVM.displayError(this.localize('submitLicenseErr', response.message));
			return;
		}
		this.open({
			mtype: 'RS.license.License',
			license: response.data
		});
		this.close();
	},
	cancel: function() {
		this.doClose();
	}
});
*/
