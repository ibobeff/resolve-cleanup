glu.defModel('RS.license.License', {
	license: null,
	dockedItemsIsVisible$: function() {
		return this.license == null;
	},
	key: '',
	type: '',
	customer: '',
	cores: '',
	coresDisplay$: function() {
		return this.cores == 0 ? 'Unlimited' : this.cores;
	},
	endUserCount: '',
	endUserCountDisplay$: function() {
		return this.endUserCount == 0 ? 'Unlimited' : this.endUserCount;
	},
	adminUserCount: '',
	adminUserCountDisplay$: function() {
		return this.adminUserCount == 0 ? 'Unlimited' : this.adminUserCount;
	},
	startDate: '',
	startedOn$: function() {
		return this.getParsedDate(this.startDate);
	},
	expirationDate: '',
	expiredOn$: function() {
		return this.getParsedDate(this.expirationDate);
	},
	createDate: '',
	createdOn$: function() {
		return this.getParsedDate(this.createDate);
	},
	uploadDate: '',
	uploadedOn$: function() {
		return this.getParsedDate(this.uploadDate);
	},
	evaluation: false,
	expired: false,
	color$: function() {
		this.expired ? 'red' : 'black';
	},
	isModal: false,

	ha: false,
	fields: ['key', 'type', 'customer', 'cores', 'duration', 'endUserCount', 'adminUserCount', 'expirationDate', 'deleteDate', {
		name: 'evaluation',
		type: 'bool'
	}, {
		name: 'expired',
		type: 'bool'
	}, {
		name: 'createDate',
		type: 'date',
		dateFormat: 'time'
	}, {
		name: 'deleteDate',
		type: 'date',
		dateFormat: 'time'
	}, {
		name: 'uploadDate',
		type: 'date',
		dateFormat: 'time'
	}, {
		name: 'expirationDate',
		type: 'date',
		dateFormat: 'time'
	}, {
		name: 'ha',
		type: 'bool'
	}],
	licenseTitle: '',
	defaultData: {

	},
	ipStore: {
		mtype: 'store',
		fields: ['ipAddress'],
		proxy: {
			type: 'memory'
		}
	},
	memStore: {
		mtype: 'store',
		fields: ['component', 'maxMemory'],
		proxy: {
			type: 'memory'
		}
	},
	gatewayStore: {
		mtype: 'store',
		fields: ['gateway', 'quantity'],
		proxy: {
			type: 'memory'
		}
	},
	getParsedDate: function(date) {
		var d = date,
			formats = ['time', 'c', 'Y-m-d G:i:s'];

		if (!Ext.isDate(d)) {
			Ext.Array.each(formats, function(format) {
				d = Ext.Date.parse(d, format)
				return !Ext.isDate(d)
			})
		}

		if (Ext.isDate(d))
			return Ext.Date.format(d, 'Y-m-d G:i:s');

		return RS.common.locale.unknownDate
	},
	activate: function(screen, params) {
		this.set('key', params && params.key ? params.key : '');
		if (this.key)
			this.loadRecord();
	},
	padding: '10px',
	init: function() {
		if (this.isModal) {
			this.set('padding', '0');
		}
		this.set('licenseTitle', this.localize('licenseTitle'));
		if (this.license)
			this.populate(this.license);
	},
	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.license.Main'
		})
	},
	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveScript, this, [exitAfterSave])
		this.task.delay(500)
		this.set('state', 'wait')
	},
	saveIsEnabled$: function() {
		return !this.state != 'wait' && this.isValkey && this.isDirty;
	},
	saveAndExit: function() {
		if (!this.saveIsEnabled)
			return;
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},

	refresh: function() {
		if (this.key)
			this.loadRecord();
	},

	populate: function(license) {
		this.loadData(license);
		this.ipStore.removeAll();
		Ext.Object.each(license.ipAddresses, function(key, value) {
			this.ipStore.add({
				ipAddress: key
			});
		}, this);
		this.memStore.removeAll();
		Ext.Object.each(license.maxMemory, function(key, value) {
			this.memStore.add({
				component: key,
				maxMemory: value
			});
		}, this);
		this.gatewayStore.removeAll();
		Ext.Object.each(license.gateway, function(key, value) {
			this.gatewayStore.add({
				gateway: key,
				quantity: value.instanceCount
			})
		}, this);
	},

	loadRecord: function() {
		this.ajax({
			url: '/resolve/service/license/get',
			params: {
				key: this.key
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					//clientVM.displayError(this.localize('getLicenseErr', respData.message));
					clientVM.displayError(respData.message);
				}
				if (respData.data)
					this.populate(respData.data);
			},

			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	apply: function() {
		this.ajax({
			url: '/resolve/service/license/apply',
			params: {
				data: this.license.rawLicense
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					//clientVM.displayError(this.localize('applyErr', respData.message));
					clientVM.displayError(respData.message);
					return;
				}
				this.parentVM.parentVM.refresh();
				clientVM.updateLicenseBanner();
				this.doClose();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	applyIsEnabled$: function() {
		return this.license && this.license.rawLicense;
	},

	cancel: function() {
		this.doClose();
	}
});