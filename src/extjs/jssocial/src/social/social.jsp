<!-- ******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
********************************************************************************* -->

<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@
    page import="java.util.regex.Pattern,java.util.regex.Matcher"
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="com.resolve.util.JspUtils" %>
<%@page import="org.owasp.esapi.ESAPI"%>
<%
    String ver = ESAPI.encoder().encodeForURL(JspUtils.getResolveVersion());

    boolean debug = false;
    String d = request.getParameter("debug");
    if( d != null && d.indexOf("t") > -1){
        debug = true;
    }

    boolean mock = false;
    String m = request.getParameter("mock");
    if( m != null && m.indexOf("t") > -1){
        mock = true;
    }
%>

<%!
    boolean isMobile = false;
    
    /*
     The is_mobile function is adapted from this version that was originally written in php found at http://www.000webhost.com/forum/scripts-code-snippets/27404-php-extremely-simple-way-check-if-mobile-browser.html
     protected function _is_mobile($useragent){
        if(strpos($useragent, 'mobile') !== false || strpos($useragent, 'android') !== false){
          return true;
        }else if(preg_match('/android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/',$useragent)){
          return true;
        }else if(preg_match('/(bolt\/[0-9]{1}\.[0-9]{3})|nexian(\s|\-)?nx|(e|k)touch|micromax|obigo|kddi\-|;foma;|netfront/',$useragent)){
          return true;
        }else if(preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/',substr($useragent,0,4))){
          return true;
        }
        return false;
      }
     */
    public boolean is_mobile(String userAgent){
        Pattern phase1 = Pattern.compile("/android.+mobile|avantgo|bada\\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\\/|plucker|pocket|psp|symbian|treo|up\\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/");
        Pattern phase2 = Pattern.compile("/(bolt\\/[0-9]{1}\\.[0-9]{3})|nexian(\\s|\\-)?nx|(e|k)touch|micromax|obigo|kddi\\-|;foma;|netfront/");
        Pattern phase3 = Pattern.compile("/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55\\/|capi|ccwa|cdm\\-|cell|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds(12|\\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|i230|iac( |\\-|\\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\\/)|klon|kpt |kwc\\-|kyo(c|k)|le(no|xi)|lg( g|\\/(k|l|u)|50|54|e\\-|e\\/|\\-[a-w])|libw|lynx|m1\\-w|m3ga|m50\\/|ma(te|ui|xo)|mc(01|21|ca)|m\\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk\\/|se(c(\\-|0|1)|47|mc|nd|ri)|sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\\-|tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\\-|2|g)|yas\\-|your|zeto|zte\\-/");
        if(userAgent.indexOf("mobile") > -1 || userAgent.indexOf("android") > -1){
            return true;
        }
        else if( phase1.matcher(userAgent).matches() )
            return true;
        else if( phase2.matcher(userAgent).matches() )
            return true;
        else if( phase3.matcher(userAgent.substring(0,4)).matches() )
            return true;        
        return false;
    }
%>
<%
    String userAgent = request.getHeader("user-agent");
    String version = request.getParameter("v");
    if( version == null ){
        //calculate whether we're on a mobile or not
        isMobile = is_mobile(userAgent.toLowerCase());
    }
    else if( version.indexOf("f") > -1){
        isMobile = false;
    }
    else if( version.indexOf("m") > -1){
        isMobile = true;
    }
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title id="page-title">Resolve Social</title>

    <jsp:include page="../client/common.jsp" flush="true" />
    <jsp:include page="loader.jsp" flush="true" />
    <script type="text/javascript" src="../client/js/client-classes.js?_v=<%=ver%>"></script>

    <%
        if( isMobile ){
    %>
    <script type="text/javascript">
        Ext.onReady(function() {
            //Initialize State Management
            if (Ext.supports.LocalStorage) Ext.state.Manager.setProvider(new Ext.state.LocalStorageProvider())
            else Ext.state.Manager.setProvider(new Ext.state.CookieProvider())

            //Initialize extjs tooltips
            Ext.tip.QuickTipManager.init()

            //Configure glu for optimal performance
            glu.asyncLayouts = true

            glu.viewport({
                mtype : 'RS.social.Main',
                mock: <%=mock%>
            },'mini')
        });
    </script>
    <%
        }
        else{
    %>
    <script type="text/javascript">
        Ext.onReady(function() {
            //Initialize State Management
            if (Ext.supports.LocalStorage) Ext.state.Manager.setProvider(new Ext.state.LocalStorageProvider())
            else Ext.state.Manager.setProvider(new Ext.state.CookieProvider())

            //Initialize extjs tooltips
            Ext.tip.QuickTipManager.init()

            //Configure glu for optimal performance
            glu.asyncLayouts = true
            
            glu.viewport({
                mtype : 'RS.social.Main',
                mock: <%=mock%>
            })
        });
    </script>
    <%
        }
    %>
    
</head>
    <body>
    </body>
</html>
