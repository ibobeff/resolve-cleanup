/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.social').locale = {

	success: 'Success',
	error: 'Error',
	failure: 'An error has occurred on the server, please contact your administrator for assistance',

	windowTitle: 'Resolve Social',

	postYourMessage: 'Post your message…',
	postYourMessageDisplay: 'Post your message to {0} …',
	addAComment: 'Add a comment…',
	messageSent: 'Message Sent',

	//Actions
	post: 'Post',
	newPost: 'New Post',
	comment: 'Comment',
	cancel: 'Cancel',
	cancelMini: 'Cancel',
	switchToPostDisplay: 'Back',

	notificationSettingChanged: 'Notification setting changed',
	applyDigest: 'Send a daily email digest',
	unapplyDigest: 'Stop sending daily email digest',
	applyEmail: 'Forward messages to email',
	unapplyEmail: 'Stop forwarding messages to email',

	postMessageTooltip: 'Post your message…',
	filterTooltip: 'Filter posts',
	refreshPostsTooltip: 'Refresh Posts',
	autoRefreshPosts: 'Auto Refresh',
	pollInterval: 'Interval',
	followStreamTooltip: 'Click to follow this stream',

	nextPage: 'Next Page',
	previousPage: 'Previous Page',

	notAuthenticated: 'You are no longer authenticated.  Please login from the main window.',

	//Fields
	to: 'To',
	invalidTo: 'Please provide a stream to post to',
	invalidToSystem: 'You cannot post to other streams when you are posting to the system stream.  Please remove system or remove the other streams.',
	subject: 'Subject',
	content: 'Content',
	author: 'Author',

	//Filter
	filter: 'Filter your posts…',

	//Grid columns
	name: 'Name',
	type: 'Type',

	// Component Types
	defaultText: 'Default',
	user: 'User',
	document: 'Document',

	//User Default hard coded streams
	all: 'All',
	inbox: 'Inbox',
	starred: 'Starred',
	blog: 'Blog',
	sent: 'Sent',
	notification: 'System',
	activity: 'Activity',

	//Streams
	streams: 'Streams',
	showAll: 'Show all streams that you are following',
	showActive: 'Show only the active streams that you are following',
	refreshStreams: 'Refresh Streams',

	//Stream Group Actions
	followStream: 'Follow a stream',

	//Stream Actions
	edit: 'Edit',
	view: 'View',
	settings: 'Settings',
	configure: 'Configure',
	markAllRead: 'Mark all as read',
	markedStreamRead: 'Marked all posts as read',
	markAllPostsReadTooltip: 'Mark all as read',
	markAllPostsRead: 'All',
	markPostsOlderThanOneDayRead: 'Older than 1 day',
	markPostsOlderThanOneWeekRead: 'Older than 1 week',
	filterMenuHeader: 'Filter',
	markAsRead: 'Mark As Read',
	unreadOnly: 'New Only',
	oneDayOld: '1 day old',
	oneWeekOld: '1 week old',
	unpin: 'Unpin',
	pin: 'Pin',
	lock: 'Lock',
	unlock: 'Unlock',
	unfollow: 'Unfollow',
	follow: 'Follow',
	unfollow: 'Unfollow',
	popout: 'Popout',
	followUser: 'Follow User',
	unfollowUser: 'Unfollow User',

	//Post Actions
	share: 'Share',
	like: 'Like',
	move: 'Move',
	deletePost: 'Delete',
	deleteComment: 'Delete',
	read: 'Mark Read',
	unread: 'Mark Unread',

	//Time
	seconds: 'seconds',
	minute: 'minute',
	minutes: 'minutes',
	hour: 'hour',
	hours: 'hours',
	yesterday: 'Yesterday',
	days: 'days',
	ago: 'ago',

	//Post Editor
	createLink: 'Create Link',
	attach: 'Attach Files',

	//Notifications
	forward: 'Forward to Email',
	digest: 'Send Digest Email',
	configureNotifications: 'Configure System Messages',

	//Unknown Component Navigation
	unknownComponentTitle: 'Unknown Component',
	unknownComponentMessage: 'The component type "{0}" is not registered with the Social Component Map',

	number: 'Number',
	reference: 'Reference',

	StreamPicker: {
		addressToDialogWindowTitle: 'Address your post',
		followDialogWindowTitle: 'Follow a stream',
		addFollowersWindowTitle: 'Add Followers',
		type: 'Filter By…',
		searchQuery: 'Search…',
		select: 'Select',
		follow: 'Follow',
		addFollowers: 'Add Followers',
		createComponent: 'Create',

		searchWorksheet: 'Search worksheets...',

		showMyWorksheets: 'Click to show my worksheets',
		showAllWorksheets: 'Click to show all worksheets'
	},

	CreateLink: {
		createLinkTitle: 'Create Link',
		createLink: 'Create Link',
		display: 'Text to display',
		linkTo: 'Link to',

		url: 'URL',
		urlLabel: 'To what url should this link go?',
		docLabel: 'To what document should this link go?',
		actionTaskLabel: 'To what action task should this link go?',
		worksheetLabel: 'To what worksheet should this link go?',
		forumLabel: 'To what forum should this link go?',
		rssLabel: 'To what rss should this link go?',

		searchQuery: 'Search…'
	},

	//Common commponent types
	component: 'Component',
	doc: 'Document',
	actionTask: 'Action Task',
	forum: 'Forum',
	rss: 'RSS',
	process: 'Process',
	team: 'Team',
	actiontask: 'Action Task',
	runbook: 'Runbook',
	worksheet: 'Worksheet',
	decisiontree: 'Decision Tree',
	decisiontrees: 'Decision Tree',
	namespace: 'Namespace',
	system: 'System',

	AttachFile: {
		attachFileWindowTitle: 'Attach File',
		attach: 'Attach'
	},

	Likes: {
		peopleWhoLikeThis: 'People who like this',
		you: 'You'
	},

	Post: {
		expandComments: 'Show {0} comments',
		expandComment: 'Show 1 comment',
		collapseComments: 'Hide {0} comments',
		collapseComment: 'Hide 1 comment',

		confirmPostDeleteTitle: 'Confirm Post Deletion',
		confirmPostDeleteBody: 'Are you sure you want to delete this post?',
		confirmCommentDeleteTitle: 'Confirm Comment Deletion',
		confirmCommentDeleteBody: 'Are you sure you want to delete this comment?',
		//postDeleted: 'Post deleted, <a onclick="Ext.Ajax.request({url: \'/resolve/service/social/undoDelete\', params: {id: \'{0}\'}});Ext.get(\'msg-div\').select(\'div\').fadeOut({duration: 200,remove: true});" style="cursor:pointer;cursor:hand;">click to undo</a>'
		postDeleted: 'Post deleted',
		commentDeleted: 'Comment deleted',

		likeTooltip: 'Click to like',
		unlikeTooltip: 'Click to un-like',
		starredTooltip: 'Click to star',
		unstarredTooltip: 'Click to un-star',
		readTooltip: 'Click to mark read',
		unreadTooltip: 'Click to mark un-read',

		answered: 'Answered',
		unanswered: 'Unanswered',
		answer: 'Answer',
		isAnswer: 'Is Answer?',
		response: 'response',
		responses: 'responses',

		target: 'Target:',
		targets: 'Targets:',

		postFrom: 'Post from:'
	},

	MovePost: {
		movePost: 'Move Post',
		invalidTo: 'Please provide a stream to move the post to'
	},

	Component: {
		email: 'Email',
		phone: 'Phone',
		mobile: 'Mobile',
		description: 'Description'
	},

	Followers: {
		followers: 'Followers',
		close: 'Close',
		addMoreFollowers: 'Add Followers',
		noFollowers: 'No followers at this time',
		you: 'You',
		remove: 'Remove'
	},

	ConfigureNotifications: {
		configureNotificationsTitle: 'Configure System Messages',
		save: 'Save',
		cancel: 'Cancel',
		notficationsSaved: 'System Messages preferences saved',
		selectAllProcess: 'Select All',
		PROCESS_CREATE: 'Create',
		PROCESS_UPDATE: 'Update',
		PROCESS_PURGED: 'Purged',
		PROCESS_RUNBOOK_ADDED: 'Runbook added',
		PROCESS_RUNBOOK_REMOVED: 'Runbook removed',
		PROCESS_DOCUMENT_ADDED: 'Document added',
		PROCESS_DOCUMENT_REMOVED: 'Document removed',
		PROCESS_ACTIONTASK_ADDED: 'ActionTask added',
		PROCESS_ACTIONTASK_REMOVED: 'ActionTask removed',
		PROCESS_RSS_ADDED: 'Rss added',
		PROCESS_RSS_REMOVED: 'Rss removed',
		PROCESS_FORUM_ADDED: 'Forum added',
		PROCESS_FORUM_REMOVED: 'Forum removed',
		PROCESS_TEAM_ADDED: 'Team added',
		PROCESS_TEAM_REMOVED: 'Team removed',
		PROCESS_USER_ADDED: 'User added',
		PROCESS_USER_REMOVED: 'User removed',
		PROCESS_DIGEST_EMAIL: 'Digest Email',
		PROCESS_FORWARD_EMAIL: 'Forward Email',
		selectAllTeam: 'Select All',
		TEAM_CREATE: 'Create',
		TEAM_UPDATE: 'Update',
		TEAM_PURGED: 'Purged',
		TEAM_ADDED_TO_PROCESS: 'Added to Process',
		TEAM_REMOVED_FROM_PROCESS: 'Removed from Process',
		TEAM_ADDED_TO_TEAM: 'Added to Team',
		TEAM_REMOVED_FROM_TEAM: 'Removed from Team',
		TEAM_USER_ADDED: 'User added',
		TEAM_USER_REMOVED: 'User removed',
		TEAM_DIGEST_EMAIL: 'Digest Email',
		TEAM_FORWARD_EMAIL: 'Forward Email',
		selectAllForum: 'Select All',
		FORUM_CREATE: 'Create',
		FORUM_UPDATE: 'Update',
		FORUM_PURGED: 'Purged',
		FORUM_ADDED_TO_PROCESS: 'Added to Process',
		FORUM_REMOVED_FROM_PROCESS: 'Removed from Process',
		FORUM_USER_ADDED: 'User added',
		FORUM_USER_REMOVED: 'User removed',
		FORUM_DIGEST_EMAIL: 'Digest Email',
		FORUM_FORWARD_EMAIL: 'Forward Email',
		selectAllUser: 'Select All',
		USER_ADDED_TO_PROCESS: 'Added to Process',
		USER_ADDED_TO_TEAM: 'Added to Team',
		USER_FOLLOW_ME: 'User following me',
		USER_REMOVED_FROM_PROCESS: 'Removed from Process',
		USER_REMOVED_FROM_TEAM: 'Removed from Team',
		USER_UNFOLLOW_ME: 'User unfollow me',
		USER_ADDED_TO_FORUM: 'Added to Forum',
		USER_REMOVED_FROM_FORUM: 'Removed from Forum',
		USER_PROFILE_CHANGE: 'User profile change',
		USER_DIGEST_EMAIL: 'Digest Email',
		USER_FORWARD_EMAIL: 'Inbox Forward Email',
		selectAllActionTask: 'Select All',
		ACTIONTASK_CREATE: 'Create',
		ACTIONTASK_UPDATE: 'Update',
		ACTIONTASK_PURGED: 'Purged',
		ACTIONTASK_ADDED_TO_PROCESS: 'Added to Process',
		ACTIONTASK_REMOVED_FROM_PROCESS: 'Removed from Process',
		ACTIONTASK_DIGEST_EMAIL: 'Digest Email',
		ACTIONTASK_FORWARD_EMAIL: 'Forward Email',
		selectAllRunbook: 'Select All',
		RUNBOOK_UPDATE: 'Update',
		RUNBOOK_ADDED_TO_PROCESS: 'Added to Process',
		RUNBOOK_REMOVED_FROM_PROCESS: 'Removed from Process',
		RUNBOOK_COMMIT: 'Commit',
		RUNBOOK_DIGEST_EMAIL: 'Digest Email',
		RUNBOOK_FORWARD_EMAIL: 'Forward Email',
		selectAllDocument: 'Select All',
		DOCUMENT_CREATE: 'Create',
		DOCUMENT_UPDATE: 'Update',
		DOCUMENT_DELETE: 'Delete',
		DOCUMENT_PURGED: 'Purged',
		DOCUMENT_ADDED_TO_PROCESS: 'Added to Process',
		DOCUMENT_REMOVED_FROM_PROCESS: 'Removed from Process',
		DOCUMENT_COMMIT: 'Commit',
		DOCUMENT_DIGEST_EMAIL: 'Digest Email',
		DOCUMENT_FORWARD_EMAIL: 'Forward Email',
		selectAllRSS: 'Select All',
		RSS_CREATE: 'Create',
		RSS_UPDATE: 'Update',
		RSS_PURGED: 'Purged',
		RSS_ADDED_TO_PROCESS: 'Added to Process',
		RSS_REMOVED_FROM_PROCESS: 'Removed from Process',
		RSS_DIGEST_EMAIL: 'Digest Email',
		RSS_FORWARD_EMAIL: 'Forward Email',

		PROCESS: 'Process',
		TEAM: 'Team',
		FORUM: 'Forum',
		USER: 'User',
		ACTIONTASK: 'Actiontask',
		RUNBOOK: 'Runbook',
		DOCUMENT: 'Document',
		DECISIONTREES: 'Decision Tree',
		RSS: 'RSS'
	}
}