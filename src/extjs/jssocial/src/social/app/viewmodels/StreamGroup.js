glu.defModel('RS.social.StreamGroup', {
	id: '',
	name: '',
	type: '',

	display: '',

	streams: {
		mtype: 'list'
	},

	store: undefined,

	showAll: false,

	isMe: false,

	hidePagingToolbar$: function () {
		// Currently only Document streams have pagination supported provided that total count is more than
		// default items per page.
		return !((this.type === 'DOCUMENT' && this.parentVM.documentStreamStore.getTotalCount() > this.parentVM.defaultItemsPerPage)
			|| (this.type === 'ACTIONTASK' && this.parentVM.actionTaskStreamStore.getTotalCount() > this.parentVM.defaultItemsPerPage));
	},

	canCollapse$: function() {
		return this.streams.indexOf(this.parentVM.activeStream) == -1
	},

	init: function() {
		this.streams.add(this.model({
			mtype: 'Blank'
		}));
	},

	toggleCollapse: function() {
		var currentlyShown = false;
		this.streams.forEach(function(s) {
			if (Ext.isDefined(s.initialShow)) {
				currentlyShown = currentlyShown || s.initialShow
				s.set('initialShow', false);
			}
		})
		if (currentlyShown)
			this.set('showAll', currentlyShown);
		this.set('showAll', !this.showAll)
	},

	refreshDisplay: function() {
		var count = 0;
		this.streams.forEach(function(stream) {
			count += Number(stream.unReadCount)
		})
		this.set('display', this.name + (count > 0 ? ' (' + count + ')' : ''))
	},

	joinComponent: function(tool) {
		this.open({
			mtype: 'RS.social.StreamPicker',
			type: this.type ? this.type.toLowerCase() : '',
			pickerType: 1
		})
	},

	openComponentSettings: function(tool) {
		debugger;
	},

	registerTool: function(tool) {
		if (this.type == 'Default') tool.hide()
	},

	selectStream: function(stream) {
		this.parentVM.selectStream(stream)
	},

	sendDigest: false,
	digestChanged: function(checked) {
		debugger;
	},
	sendEmail: false,
	emailForwardChanged: function(checked) {
		debugger;
	},

	digestApplied: function() {
		if (this.type == 'NAMESPACE') return
		var streams = [];
		this.streams.forEach(function(stream) {
			streams.push(stream.sys_id)
		})
		this.ajax({
			url: '/resolve/service/social/specific/notify/updateType',
			params: {
				streamIds: streams,
				selectedType: Ext.String.format('{0}_{1}_{2}', this.type, 'DIGEST', 'EMAIL'),
				value: true
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.parentVM.refreshStreams()
					clientVM.displaySuccess(this.localize('notificationSettingChanged'))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	unapplyDigest: function() {
		if (this.type == 'NAMESPACE') return
		var streams = [];
		this.streams.forEach(function(stream) {
			streams.push(stream.sys_id)
		})
		this.ajax({
			url: '/resolve/service/social/specific/notify/updateType',
			params: {
				streamIds: streams,
				selectedType: Ext.String.format('{0}_{1}_{2}', this.type, 'DIGEST', 'EMAIL'),
				value: false
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.parentVM.refreshStreams()
					clientVM.displaySuccess(this.localize('notificationSettingChanged'))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	emailApplied: function() {
		if (this.type == 'NAMESPACE') return
		var streams = [];
		this.streams.forEach(function(stream) {
			streams.push(stream.sys_id)
		})
		this.ajax({
			url: '/resolve/service/social/specific/notify/updateType',
			params: {
				streamIds: streams,
				selectedType: Ext.String.format('{0}_{1}_{2}', this.type, 'FORWARD', 'EMAIL'),
				value: true
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.parentVM.refreshStreams()
					clientVM.displaySuccess(this.localize('notificationSettingChanged'))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	unapplyEmail: function() {
		if (this.type == 'NAMESPACE') return
		var streams = [];
		this.streams.forEach(function(stream) {
			streams.push(stream.sys_id)
		})
		this.ajax({
			url: '/resolve/service/social/specific/notify/updateType',
			params: {
				streamIds: streams,
				selectedType: Ext.String.format('{0}_{1}_{2}', this.type, 'FORWARD', 'EMAIL'),
				value: false
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.parentVM.refreshStreams()
					clientVM.displaySuccess(this.localize('notificationSettingChanged'))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	supportPagination: function() {
		return (this.type === 'DOCUMENT' || this.type === 'ACTIONTASK');
	},

	//
	// This method is called after the streams are loaded.
	//
	handleStoreLoaded: function(records) {
		// Remove existing streams from document StreamGroup
		this.streams.removeAll();
		// Add new streams into document StreamGroup
		this.updateStreams(records);
	},

	//
	// This method gets data from the store, converts them into streams.
	//
	priorUnreadRecords: null,
	updateStreams: function(records) {
		if (records) {
			for(var i=0; i<records.length; i++) {
				var stream = records[i];
				if (stream.data.id) {
					this.streams.add(this.model(Ext.apply({
						mtype: 'RS.social.Stream'
					}, stream.data)));
	
				} else {
					// Update unread records by 
					// 1. subtracting what was added
					// 2. adding current unread record of this group
					var priorUnreadRecords = Ext.apply({}, stream.raw);
					if (this.parentVM.unreadRecords) {
						for (key in this.parentVM.unreadRecords) {
							stream.raw[key] = 
								this.parentVM.unreadRecords[key] + stream.raw[key] - (!this.priorUnreadRecords? 0: this.priorUnreadRecords[key]);
						}
					}
					this.parentVM.set('unreadRecords', stream.raw);
					// Update prior unread records
					this.priorUnreadRecords  =  Ext.apply({}, priorUnreadRecords);
				}
			}
		}
	}
});

glu.defModel('RS.social.Blank', {
	selected: false
})