glu.defModel('RS.social.Likes', {
	target: null,
	csrftoken: '',

	likes: {
		mtype: 'list',
		autoParent: true
	},

	init: function() {
		this.initToken();
		if (this.parentVM.id) this.ajax({
			url: '/resolve/service/social/listLikes',
			params: {
				id: this.parentVM.id
			},
			success: function(r) {
				var response = RS.common.parsePayload(r),
					streamStore = this.parentVM.parentVM.streamStore || this.parentVM.parentVM.parentVM.streamStore;
				if (response.success) Ext.Array.forEach(response.records, function(record) {
					if (record.username == this.parentVM.username) record.currentUser = true
					record.following = false
					streamStore.each(function(stream) {
						if (stream.get('username') == record.username) {
							record.following = true
							return false
						}
					})
					this.likes.add(Ext.apply({
						mtype: 'viewmodel',
						followText$: function() {
							return this.currentUser ? this.parentVM.localize('you') : Ext.String.format('<span class="{0} like-follow-text"></span><span class="like-follow-text">{1}</span>', this.followIcon, this.following ? this.localize('unfollow') : this.localize('follow'))
						},
						followIcon$: function() {
							return this.following ? 'icon-large icon-reply like-following' : 'icon-large icon-plus like-follow'
						},
						likeProfileImage$: function() {
							return this.csrftoken ? '/resolve/service/user/downloadProfile?username=' + this.username + '&' + this.csrftoken : '';
						},
						displayUser: function() {
							clientVM.handleNavigation({
								modelName: 'RS.user.User',
								target: '_blank',
								params: {
									username: this.username
								}
							})
						},
						toggleFollowing: function() {
							this.set('following', !this.following)
							this.ajax({
								url: '/resolve/service/social/setFollowStreams',
								params: {
									ids: [this.id],
									follow: this.following
								},
								success: function(r) {
									var response = RS.common.parsePayload(r)
									if (response.success) this.rootVM.refreshStreams()
									else clientVM.displayError(response.message)
								}
							})
						}
					}, record))
				}, this)
				else clientVM.displayError(response.message)
			}
		})
	},

	activate: function() {
		this.initToken();
	},

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/user/downloadProfile', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this));
	},

	close: function() {
		this.parentVM.set('likesDisplayed', false)
		this.doClose()
	}
})