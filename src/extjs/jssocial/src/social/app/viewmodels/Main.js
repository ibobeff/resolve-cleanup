glu.defModel('RS.social.Main', {

	mock: false,

	active: false,

	postId: '',

	displayPostProfilePictures: true,
	displayCommentProfilePictures: true,

	miniMode: false,

	mode: '',
	id: '',

	defaultItemsPerPage: 10,

	isNews$: function() {
		return this.mode == 'news'
	},

	isEmbed$: function() {
		return this.mode == 'embed'
	},

	showStreams$: function() {
		return !this.isNews && !this.isEmbed
	},

	activate: function(screen, params) {
		this.set('active', true)
		this.set('mode', params ? params.mode : '')
		if (params && params.streamId) this.setInitialStream(params.streamId)
		if (params && params.streamParent) this.set('streamParent', params.streamParent)
		this.updateScreen()
	},

	deactivate: function() {
		this.set('active', false)
	},

	checkNavigateAway: function() {
		this.set('active', false)
	},



	groups: {
		mtype: 'list',
		add: function(streamGroup) {
			// We display DOCUMENT and ACTIONTASK at the end
			if(streamGroup.type === 'DOCUMENT') {
				(this.length > 0 && this.getAt(this.length-1).type === 'ACTIONTASK')
					? this.insert(this.length-1, streamGroup)
					: this.insert(this.length, streamGroup);
			} else if(streamGroup.type === 'ACTIONTASK') {
				// Always insert at the end
				this.insert(this.length, streamGroup);
			} else {
				// Insert before Document or ActionTask. If none exists, insert at the end
				var i;
				for (i=0; i<this.length; i++) {
					var type = this.getAt(i).type;
					if (type === 'DOCUMENT' || type == 'ACTIONTASK') {
						break;
					}
				}
				this.insert(i, streamGroup);
			}
		}
	},

	streamToStore: {
		mtype: 'store',
		fields: ['id', 'name', 'type', 'displayName'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	streamStore: {
		mtype: 'store',
		model: 'RS.social.UserStream',
		proxy: {
			type: 'ajax',
			url: '/resolve/service/social/listUserStreams',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	documentStreamStore : {
		mtype: 'store',
		model: 'RS.social.UserStream',
		proxy: {
			type: 'ajax',
			url: '/resolve/service/social/listDocumentUserStreams',
			reader: {
				type: 'json',
				root: 'records',
				totalProperty : 'total'
			}
		},
		supportPaging: true
	},

	actionTaskStreamStore : {
		mtype: 'store',
		model: 'RS.social.UserStream',
		proxy: {
			type: 'ajax',
			url: '/resolve/service/social/listActionTaskUserStreams',
			reader: {
				type: 'json',
				root: 'records',
				totalProperty : 'total'
			}
		},
		supportPaging: true
	},

	pageSize: 10,
	pageNumber: 1,
	when_pageNumber_changes_reload_posts: {
		on: ['pageNumberChanged'],
		action: function() {
			this.postFilterStore.load()
		}
	},
	totalCount: 0,

	postFilterStore: {
		remoteSort: true,
		mtype: 'store',
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		fields: [
			'id',
			'content',
			'title',
			'author',
			'sysUpdatedOn'
		],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/social/listPosts',
			reader: {
				type: 'json',
				root: 'records',
				totalProperty: 'total'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
				
			}
		}
	},
	posts: {
		mtype: 'list'
	},

	switchingToPost: false,

	postYourMessageDisplay$: function() {
		if (this.streamParent) return this.localize('postYourMessageDisplay', this.streamParent)
		if (this.activeStream) {
			if (this.streamToStore.findExact('id', this.activeStream.sys_id) > -1)
				return this.localize('postYourMessageDisplay', this.activeStream.name)
			return this.localize('postYourMessageDisplay', this.localize('blog'))
		}
		return this.localize('postYourMessage')
	},

	activeStream: null,
	when_active_stream_changes_update_posts: {
		on: ['activeStreamChanged'],
		action: function() {
			//clear out any initial stream id too
			this.set('initialStreamId', '')
			this.posts.removeAll()
			this.set('pageNumber', 1)
			this.set('postId', '')
			this.postFilterStore.load()
			this.updateWindowTitle()
			this.updateMini()
			if (this.activeStream.lock) this.switchToFilter()
			else if (!this.switchingToPost) {
				this.set('activePostCard', 0)
			}
			this.set('switchingToPost', false)
			this.switchToPostDisplay()
		}
	},
	activePostCard: 0,

	userNameText: '',

	poller : null,
	pollInterval: 30,
	initialStreamId: '',
	initialStreamType: '',

	clientVM: null,
	authenticated: false,

	unreadOnly: false,
	when_unreadOnly_changes_update_posts: {
		on: 'unreadOnlyChanged',
		action: function() {
			this.postFilterStore.load()
		}
	},

	oneDayOld: false,
	oneWeekOld: false,

	when_postOld_changes_update_posts: {
		on: ['oneDayOldChanged'],
		action: function() {
			if (this.oneDayOld) {
				this.set('oneWeekOld', false)
			}
			this.postFilterStore.load()
		}
	},

	when_oneWeekOld_changes: {
		on: ['oneWeekOldChanged'],
		action: function() {
			if (this.oneWeekOld) {
				this.set('oneDayOld', false)
			}
			this.postFilterStore.load()
		}
	},

	postFilterStoreLoadingCount: 0,

	startPolling: function() {
		if (this.poller)
			this.poller.destroy();
		if (this.pollInterval < 5)
			this.set('pollInterval', 5);
		var resultRunner = new Ext.util.TaskRunner();	
		var task = resultRunner.newTask({
			run: function() {
				if (this.autoRefreshPosts)
					this.updateScreen();
			},
			scope: this,
			interval: this.pollInterval * 1000
		});
		this.set('poller', task);
		this.poller.start();
	},
	cleanGroup: function() {
		var wipedOut = [];
		this.groups.foreach(function(g) {
			if (g.streams.getCount() == 1 && g.streams.getAt(0).viewmodelName == 'Blank')
				wipedOut.push(g);
		});
		Ext.each(wipedOut, function(g) {
			this.groups.remove(g)
		}, this)
	},

	init: function() {
		var me = this;

		this.setDefaultPageSize();
		this.on('streamsUpdated', this.streamsUpdated);

		if (this.mock) this.backend = RS.social.createMockBackend(true)

		this.streamStore.on('load', function() {
			this.initUnreadRecords(),
			this.updateWindowTitle()
			this.updateStreams()
			this.updateShowFollow()
			this.cleanGroup();
		}, this)
		
		this.postFilterStore.on('beforeload', function(store, operation, eOpts) {
			if (!this.activeStream && !this.initialStreamId && !this.postId) return false
			if (this.mode == 'embed' && !this.initialStreamId) return false

			operation.params = operation.params || {}

			var newerThanDate = '';
			if (this.oneDayOld) newerThanDate = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, -1), 'c')
			if (this.oneWeekOld) newerThanDate = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, -7), 'c')

			Ext.apply(operation.params, {
				streamId: this.initialStreamId || (this.activeStream ? this.activeStream.id : ''),
				streamType: this.initialStreamType || (this.activeStream ? this.activeStream.type : ''),
				page: this.pageNumber,
				start: (this.pageNumber - 1) * this.pageSize,
				limit: this.pageSize,
				unreadOnly: this.unreadOnly
			})

			if (newerThanDate)
				Ext.apply(operation.params, {
					filter: Ext.encode([{
						field: 'sysUpdatedOn',
						condition: 'greaterThan',
						type: 'date',
						value: newerThanDate
					}])
				})

			if (this.postId) {
				Ext.apply(operation.params, {
					postId: this.postId
				})
			}
			this.set('postFilterStoreLoadingCount', this.postFilterStoreLoadingCount + 1)
		}, this)

		this.streamStore.load();

		this.postFilterStore.on('load', function(store, records, successfull, eOpts) {
			this.set('postFilterStoreLoadingCount', this.postFilterStoreLoadingCount - 1)
			//this.streamStore.load();
			if (this.postFilterStoreLoadingCount == 0) this.updatePosts()
		}, this)

		if (this.id) this.set('initialStreamId', this.id)
		delete this.id

		//Start auto-timer to refresh posts automatically	
		this.startPolling();

		if (!window['clientVM']) { //&& window.opener && window.opener.clientVM) {
			this.set('miniMode', true)
			this.set('clientVM', this.getMiniClientVM())
			window['clientVM'] = this.clientVM
			this.set('authenticated', clientVM.authenticated)
			clientVM.on('authenticatedChanged', function() {
				me.set('authenticated', clientVM.authenticated)
			})
			this.set('active', true)
			this.updateScreen() //manually kick updatescreen from mini-mode
		}

		//Go get the user's name from the client
		if (clientVM.user) this.set('userNameText', clientVM.user.fullName)
		else {
			clientVM.on('userChanged', function() {
			if (clientVM.user) this.set('userNameText', clientVM.user.fullName)
		}, this)
		}

		if (this.postId) this.postFilterStore.load()

		var winParams = {};
		try {
			Ext.Object.fromQueryString(window.location.search)
		} catch (e) {
			clientVM.displayError(e)
		}
		if (winParams.streamId) {
			this.set('activeCard', 1)
			this.set('initialStreamId', winParams.streamId)
		}
		if (this.postId) this.set('activeCard', 2)
		if (this.isNews) this.set('activePostCard', 3)
	},
	beforeDestroyComponent : function(){
		if(this.poller)			
			this.poller.destroy();
	},
	loadPosts: function() {
		this.postFilterStore.load()
	},

	getMiniClientVM: function() {
		var vm = this;
		var model = glu.model({
			mtype: 'viewmodel',
			authenticated: true,

			setWindowTitle: function(title) {
				window.document.title = title
			},
			displayError: function(message) {
				RS.UserMessage.msg({
					success: false,
					title: vm.localize('error'),
					msg: message,
					closeable: true
				})
			},

			displayFailure: function() {
				RS.UserMessage.msg({
					success: false,
					title: vm.localize('error'),
					msg: vm.localize('failure'),
					closeable: true
				})
			},

			displaySuccess: function(message) {
				RS.UserMessage.msg({
					success: true,
					title: vm.localize('success'),
					msg: message,
					duration: 2000,
					closeable: true
				})
			},

			user: null,

			init: function() {
				this.ajax({
					url: '/resolve/service/client/getUser',
					params: {
						notificationsSince: Math.max(Ext.Date.format(Ext.Date.subtract(new Date(), Ext.Date.HOUR, 1), 'time'), Ext.state.Manager.get('notificationIgnore', 0))
					},
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.set('user', response.data)
					}
				})
			}
		});
		model.init()
		return model
	},

	navigateToProfile: function(username) {
		clientVM.handleNavigation({
			modelName: 'RS.user.User',
			params: {
				username: username
			}
		})
	},

	unreadRecords: null,
	when_unreadrecords_changed_update_counts: {
		on: ['unreadRecordsChanged'],
		action: function() {
			if (this.groups.length > 0)
				this.groups.getAt(0).streams.forEach(function(stream) {
					if (Ext.isDefined(this.unreadRecords[stream.id]))
						stream.set('unReadCount', this.unreadRecords[stream.id])
				}, this)
		}
	},

	updateExisting: function(streamStore, stream) {
		return (!!!streamStore.supportPaging && !stream.supportPaging());
	},

	updateStreams: function() {
		//go through each of the streams in the store and update the streams list appropriately by either:
		//showing, hiding, updating the count, adding, removing, or whatever else might have changed since the last refresh
		this.streamToStore.removeAll()

		var index = -1,
			group, i = 0,
			len, streams, lookupId, streamId, newStreamsForStreamStore = [];

		this.streamStore.sort([{
			property: 'name',
			direction: 'ASC'
		}])

		this.streamStore.each(function(stream) {
			if (stream.data.id) {
				index = -1
				if (!group || stream.data.type != group.type || stream.data.currentUser != group.isMe) {
					group = this.getStreamGroup(stream)
				}

				if (!group.isMe) {
					if (stream.get('type').toLowerCase() != 'namespace')
						newStreamsForStreamStore.push({
							name: stream.get('name'),
							type: stream.get('type'),
							id: stream.get('sys_id'),
							displayName: stream.get('displayName')
						})

					len = group.streams.length
					streams = group.streams
					lookupId = stream.data.id

					for (i = 0; i < len && index == -1; i++) {
						if (lookupId == streams.getAt(i).id)
							index = i
					}

					//Add anything that wasn't found
					if (index == -1)
						streams.add(group.model(Ext.apply({
							mtype: 'RS.social.Stream'
						}, stream.data)))
					else streams.getAt(index).updateStream(stream.data) //Update existing records
				} else
					this.updateUserStreams(group, stream)
			} else {
				//we don't have a stream, we have the aggregate of unread records.
				// Accumulate unread records with the current on.
				var unreadDocuments = this.getDocumentGroup(),
				    unreadActionTasks = this.getActionTaskGroup();
				for (key in stream.raw) {
					stream.raw[key] = stream.raw[key] + (this.unreadRecords ? this.unreadRecords[key]: 0) + 
		                  (unreadDocuments && unreadDocuments.priorUnreadRecords ? unreadDocuments.priorUnreadRecords[key]: 0) + 
		                  (unreadActionTasks && unreadActionTasks.priorUnreadRecords ? unreadActionTasks.priorUnreadRecords[key]: 0);					
				}
				this.set('unreadRecords', stream.raw);
			}
		}, this)

		//Remove any extra items that aren't there anymore
		for (var g = 0; g < this.groups.length; g++) {
			var group = this.groups.getAt(g)
			for (var i = 0; i < group.streams.length; i++) {
				var mStream = group.streams.getAt(i),
					found = this.streamStore.getById(mStream.id) != null;

				if (!found && Ext.Array.indexOf(['all', 'inbox', 'starred', 'blog', 'sent', 'system', 'activity'], mStream.id) == -1) {
					if (this.updateExisting(this.streamStore, mStream)) {
						group.streams.removeAt(i)
						i--;
					}
				}
			}
			if (group.streams.length == 0) {
				this.groups.removeAt(g)
				g--;
			} else group.refreshDisplay()
		}

		this.streamToStore.add(newStreamsForStreamStore)
		this.streamToStore.sort('name', 'ASC')

		//Sort each group's streams
		this.groups.forEach(function(group) {
			if (!group.isMe && group.type !== 'DOCUMENT' && group.type !== 'ACTIONTASK') {
				var sortStreams = []
				group.streams.forEach(function(stream) {
					sortStreams.push(stream)
				})
				sortStreams.sort(function(a, b) {
					var aTemp = a.name ? a.name.toLowerCase() : a.name,
						bTemp = b.name ? b.name.toLowerCase() : b.name;

					if (aTemp > bTemp) return 1
					if (aTemp < bTemp) return -1
					return 0
				})

				Ext.Array.forEach(sortStreams, function(sortStream, index) {
					if (group.streams.getAt(index) != sortStream)
						group.streams.transferFrom(group.streams, sortStream, index)
				})
			}
		})

		if ((!this.activeStream && !this.postId) || this.initialStreamChanged) {
			this.set('initialStreamChanged', false)
			if (this.initialStreamId) this.selectStreamById(this.initialStreamId)
			else this.selectFirstStream()
		}
		this.fireEvent('streamsUpdated');
	},

	setDefaultPageSize: function() {
		// Default configurable page size is 10 items
		this.documentStreamStore.pageSize = this.defaultItemsPerPage;
		this.actionTaskStreamStore.pageSize = this.defaultItemsPerPage;
	},

	getStreamGroup: function(stream) {
		var g = null,
			type = stream.data.type,
			isCurrentUser = stream.data.currentUser,
			i = 0,
			len = this.groups.length,
			temp;

		for (; i < len && !g; i++) {
			temp = this.groups.getAt(i)
			if (temp.type == type && temp.isMe == isCurrentUser) g = temp
		}

		if (!g) {
			var name;
			if (stream.data.currentUser) name = this.localize('defaultText')
			else name = this.localize((type || '').toLowerCase())

			g = this.model({
				mtype: 'RS.social.StreamGroup',
				id: Ext.id(),
				type: type,
				name: name,
				isMe: stream.data.currentUser
			})
			if (stream.data.currentUser) this.groups.insert(0, g)
			else this.groups.add(g)
		}
		return g;
	},

	updateUserStreams: function(group, stream) {
		this.streamToStore.add({
			name: this.localize('blog'),
			id: stream.get('id'),
			displayName: this.localize('blog')
		})

		if (hasPermission('admin,social_admin'))
			this.streamToStore.add({
				name: this.localize('system'),
				id: 'system',
				displayName: this.localize('system')
			})
			//All, Inbox, Starred, Blog, Sent, system
		var all = inbox = starred = blog = sent = system = activity = false;
		group.streams.forEach(function(s) {
			switch (s.id) {
				case 'all':
					all = true
					break;
				case 'inbox':
					inbox = true
					break;
				case 'starred':
					starred = true
					break;
				case 'blog':
					blog = true
					break;
				case 'sent':
					sent = true
					break;
				case 'system':
					system = true
					break;
				case 'activity':
					activity = true
					break;
			}
		})

		if (!all)
			group.streams.add(group.model({
				mtype: 'RS.social.Stream',
				id: 'all',
				sys_id: 'all',
				name: this.localize('all'),
				isMe: true,
				unReadCount: stream.raw.unReadCountOfAll,
				type: 'USER_ALL'
			}))

		if (!activity) group.streams.add(group.model({
			mtype: 'RS.social.Stream',
			id: 'activity',
			name: this.localize('activity'),
			isMe: true,
			unReadCount: stream.raw.unReadCountOfActivity,
			showDigest: false,
			showEmail: false
		}))

		if (!inbox) group.streams.add(group.model({
			mtype: 'RS.social.Stream',
			id: 'inbox',
			sys_id: 'inbox',
			name: this.localize('inbox'),
			isMe: true,
			unReadCount: stream.raw.unReadCountOfInbox,
			type: 'USER'
		}))

		if (!starred) group.streams.add(group.model({
			mtype: 'RS.social.Stream',
			id: 'starred',
			name: this.localize('starred'),
			isMe: true,
			unReadCount: stream.raw.unReadCountOfStarred,
			showDigest: false,
			showEmail: false
		}))

		if (!blog) group.streams.add(group.model({
			mtype: 'RS.social.Stream',
			id: 'blog',
			name: this.localize('blog'),
			isMe: true,
			showDigest: false,
			showEmail: false
		}))

		if (!sent) group.streams.add(group.model({
			mtype: 'RS.social.Stream',
			id: 'sent',
			name: this.localize('sent'),
			isMe: true,
			showDigest: false,
			showEmail: false
		}))

		if (!system) group.streams.add(group.model({
			mtype: 'RS.social.Stream',
			id: 'system',
			sys_id: 'system',
			// lock: true,
			name: this.localize('system'),
			isMe: true,
			type: 'system'
		}))
	},

	initUnreadRecords: function() {
		// Reset unreadRecords before every load.
		this.unreadRecords = null;
	},

	getGroup: function(name) {
		for (var i=0; i<this.groups.length; i++) {
			if (this.groups.getAt(i).type === name) {
				return this.groups.getAt(i);
			}
		}
		return null;
	},

	getDocumentGroup: function() {
		return this.getGroup('DOCUMENT');
	},

	getActionTaskGroup: function() {
		return this.getGroup('ACTIONTASK');
	},

	getStreamGroupWithPagination: function(type) {
		var streamGroup = null,
			i = 0;
		for(i=0; i<this.groups.length; i++) {
			var curStreamGroup = this.groups.getAt(i);
			if (curStreamGroup.type === type) {
				streamGroup = curStreamGroup;
				break;
			}
		}
		if(!streamGroup) {
			streamGroup = this.model({
				mtype: 'RS.social.StreamGroup',
				id: Ext.id(),
				type: type,
				name: this.localize((type || '').toLowerCase()),
				isMe: false
			});
			if (type === 'DOCUMENT') {
				streamGroup.set('store', this.documentStreamStore);
			} else if (type === 'ACTIONTASK') {
				streamGroup.set('store', this.actionTaskStreamStore);
			}
			this.groups.add(streamGroup);
		}
		return streamGroup;
	},

	updatesLocalsTreamStore: function(streams) {
		if (streams) {
			for (var i=0; i<streams.length; i++) {
				var curStream = streams[i];
				var curStreamToStoreRecord = this.streamToStore.getById(curStream.id);
				if (!!curStreamToStoreRecord) {
					this.streamToStore.remove(curStreamToStoreRecord);
				}
				this.streamToStore.add({
					name: curStream.get('name'),
					type: curStream.get('type'),
					id: curStream.get('sys_id'),
					displayName: curStream.get('displayName')
				});
			}
		}
	},

	removeStreamsFromLocalsTreamStore: function(streams) {
		if (streams) {
			for (var i=0; i<streams.length; i++) {
				var curStream = streams.getAt(i);
				var curStreamToStoreRecord = this.streamToStore.getById(curStream.id);
				if (!!curStreamToStoreRecord) {
					this.streamToStore.remove(curStreamToStoreRecord);
				}
			}
		}
	},

	updatePagination: function(type) {
		if(type === 'DOCUMENT') {
			this.documentStreamStore.reload();
		} else if (type === 'ACTIONTASK') {
			this.actionTaskStreamStore.reload();
		}
	},

	postYourMessageBoxReady: false,
	streamsUpdated: function() {
		this.updatesLocalsTreamStore(this.documentStreamStore.getRange());
		this.updatesLocalsTreamStore(this.actionTaskStreamStore.getRange());
		this.set('postYourMessageBoxReady', true);
	},

	// @param records: DOCUMENT stream records
	updateStreamGroup: function(records, type) {
		var streamGroup = this.getStreamGroupWithPagination(type);

		// Remove streamGroup's streams from 'streamToStore'
		this.removeStreamsFromLocalsTreamStore(streamGroup.streams);

		streamGroup.handleStoreLoaded(records);
		this.updatesLocalsTreamStore(records);
	},

	updateDocumentStreamGroupOnloaded: function(store, records, successful) {
		if (successful) {
			this.updateStreamGroup(records, 'DOCUMENT');
		}
	},

	updateActionTaskStreamGroupOnloaded: function(store, records, successful) {
		if (successful) {
			this.updateStreamGroup(records, 'ACTIONTASK');
		}
	},

	loadStreamGroupWithPagination: function() {
		//
		// Go through each store, load it and hook up pagination.
		//

		// DOCUMENT
		this.documentStreamStore.load({
			params: {
				start: (this.documentStreamStore.currentPage-1)*this.documentStreamStore.pageSize,
				limit: this.documentStreamStore.pageSize
			},
			scope: this,
			callback: function(records, operation, success) {
				this.updateStreamGroup(records, 'DOCUMENT');
				// Intercept Document's store load so that we can update the
				// Document StreamGroup every time a new document page is loaded.
				this.documentStreamStore.on('load', this.updateDocumentStreamGroupOnloaded, this);
			}
		});

		// ACTIONTASK
		this.actionTaskStreamStore.load({
			params: {
				start: (this.actionTaskStreamStore.currentPage-1)*this.actionTaskStreamStore.pageSize,
				limit: this.actionTaskStreamStore.pageSize
			},
			scope: this,
			callback: function(records, operation, success) {
				this.updateStreamGroup(records, 'ACTIONTASK');
				// Intercept Document's store load so that we can update the
				// Document StreamGroup every time a new document page is loaded.
				this.actionTaskStreamStore.on('load', this.updateActionTaskStreamGroupOnloaded, this);
			}
		});
	},

	updateScreen: function(config) {
		if (this.active) {
			//Update the window title immediately even though it will also be done after the store is loaded
			this.updateWindowTitle()

			//Update streams
			if (!this.miniMode || this.activeCard == 0)
				this.streamStore.load()

			//Update selected stream's posts
			if (this.activeStream)
				this.postFilterStore.load()

			// Load stream groups that support pagination.
			if (!this.miniMode || this.activeCard == 0) {
				this.loadStreamGroupWithPagination();
			}
		}
	},

	updatePosts: function() {
		this.set('totalCount', this.postFilterStore.totalCount);

		//Remove any old posts that weren't in the latest page of results
		for (var i = 0; i < this.posts.length; i++) {
			if (!this.postFilterStore.getById(this.posts.getAt(i).id)) {
				this.posts.removeAt(i)
				i--;
			}
		}

		var posts = this.postFilterStore.data.items;

		for(var i = posts.length - 1; i >= 0; i--) {
			var post = posts[i];
			var postIndex = -1;

			for(var j = 0; j < this.posts.length; j++) {
				if(this.posts.getAt(j).id === post.data.id) {
					postIndex = j;
					break;
				}
			}

			if(postIndex === -1) {
				var newPost = this.model(Ext.apply({
					mtype: 'RS.social.Post'
					}, post.raw));
				this.posts.insert(0,newPost);
			} else {
				var match = this.posts.getAt(postIndex);
				match.updatePost(post);

				if (this.activePost && this.activePost.id === match.id) {
					this.activePost.syncPostProperties(match);
				}
			}
		}
	},

	updateWindowTitle: function() {
		//Update window title with the proper count of unread social posts if there are any
		if (this.rootVM == this) {
			var winTitle = this.localize('windowTitle');
			if (this.activeStream) {
				winTitle = this.activeStream.name + ' - ' + winTitle
				if (this.activeStream.count) winTitle = '(' + this.activeStream.count + ') ' + winTitle
			}

			if (clientVM && clientVM.setWindowTitle) clientVM.setWindowTitle(winTitle)
			else window.document.title = winTitle
		}
	},


	to: [],
	toIsValid$: function() {
		if (Ext.Array.indexOf(this.to, 'system') != -1 && this.to.length > 1) return this.localize('invalidToSystem')
		return this.to.length > 0 ? true : this.localize('invalidTo')
	},
	toHidden: [],
	subject: '',
	content: '',
	contentIsValid$: function() {
		return this.content.length > 0
	},

	showAdvancedTo: function() {
		this.open({
			mtype: 'RS.social.StreamPicker'
		})
	},

	streamParent: '',

	switchToPostMessage: function() {
		this.set('activePostCard', 1);

		this.posts.forEach(function(post) {
			post.cancel();
		});

		this.set('toHidden', '');
		this.set('switchingToPost', true);

		if (this.initialStreamId && this.streamParent) {
			this.set('toHidden', [this.streamToStore.createModel({
				id: this.initialStreamId,
				name: this.initialStreamId == (clientVM.user ? clientVM.user.id : '') ? this.localize('blog') : this.streamParent,
				displayName: this.initialStreamId == (clientVM.user ? clientVM.user.id : '') ? this.localize('blog') : this.streamParent
			})]);
		} else if (this.activeStream && this.streamToStore.findExact('id', this.activeStream.sys_id) > -1) {
			this.set('toHidden', [this.streamToStore.getAt(this.streamToStore.findExact('id', this.activeStream.sys_id))]);
		} else if (this.activeStream && this.activeStream.id == 'system') {
			var systemStreamIndex = this.streamToStore.findExact('name', this.localize('system'));

			if (systemStreamIndex > -1) {
				this.set('toHidden', [this.streamToStore.getAt(systemStreamIndex)]);
			}
		} else {
			//default to Blog
			var blogStreamIndex = this.streamToStore.findExact('name', this.localize('blog'));

			if (blogStreamIndex > -1) {
				this.set('toHidden', [this.streamToStore.getAt(blogStreamIndex)]);
			}

			//Also, switch to the blog stream because Duke said to
			this.selectStreamById('blog');
		}
	},

	switchToFilter: function() {
		this.set('activePostCard', 2)
	},

	showPostToolbarButtons$: function() {
		return this.activePostCard != 1 && this.postDisplayItem != 1
	},

	showPostToolbarButtonsRefresh$: function() {
		return this.activePostCard != 1
	},

	showPostToolbarActions$: function() {
		return this.activePostCard != 1 && !this.isNews && this.postDisplayItem != 1
	},

	showPostActionToolbarActions$: function() {
		return this.activePostCard != 1 && !this.isNews && ((this.activeStream && !this.activeStream.lock && this.activeStream.type.toLowerCase() != 'namespace') || this.initialStreamId) && this.postDisplayItem != 1
	},

	showPostToolbarButton$: function() {
		return this.showPostToolbarButtons && this.activeStream && !this.activeStream.lock
	},

	post: function(form, post) {
		var p = this;
		if (post && post.id) p = post
		p.set('posting', true)
		var content = p.commentContent || p.content;
		content = content.replace('<span class="suggest"></span>', '');

		this.ajax({
			url: '/resolve/service/social/submit',
			params: {
				streams: p.to,
				subject: p.subject,
				content: content,
				postId: p.id,
				streamType: this.initialStreamType || (this.activeStream ? this.activeStream.type : '')
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.set('pageNumber', 1)
					this.postFilterStore.load()
					if (form && form.fireEvent) form.fireEvent('resetPostForm')
					p.resetPost()
					if (this.miniMode) this.set('activeCard', 1)
					if (!this.miniMode) clientVM.displaySuccess(this.localize('messageSent'))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('posting', false);
			}
		})
	},
	posting: false,
	postIsEnabled$: function() {
		return this.isValid && !this.posting && this.content.indexOf('/resolve/js/extjs4/resources/ext-theme-neptune/images/loadmask/loading.gif') == -1
	},

	comment: function(form, post) {
		this.post(form, post)
	},
	commentIsVisible$: function() {
		return false
	},

	cancel: function() {
		this.resetPost()
	},

	resetPost: function() {
		this.set('activePostCard', 0)
		this.set('to', '')
		this.set('subject', '')
		this.set('content', '')
	},

	/* Deprecated feature
	createLink: function(editor) {
		this.open({
			mtype: 'RS.social.CreateLink',
			editor: editor
		})
	}, 
	*/ 

	/* Deprecated feature
	attachFile: function(editor) {
		this.open({
			mtype: 'RS.social.AttachFile',
			editor: editor
		})
	},
	*/

	markAllPostsRead: function() {
		if (this.activeStream)
			this.activeStream.markStreamRead(new Date())
		if (this.mode == 'embed')
			this.ajax({
				url: '/resolve/service/social/markStreamRead',
				params: {
					id: this.initialStreamId,
					streamType: this.initialStreamType,
					olderThanDate: Ext.Date.format(new Date(), 'c')
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					Ext.defer(this.refreshPosts, 800, this);
					// this.refreshPosts();
					if (response.success) clientVM.displaySuccess(this.localize('markedStreamRead'))
					else clientVM.displayError(response.message)
				}
			})
	},
	markPostsOlderThanOneDayRead: function() {
		if (this.activeStream) this.activeStream.markStreamRead(Ext.Date.add(new Date(), Ext.Date.DAY, -1))
	},
	markPostsOlderThanOneWeekRead: function() {
		if (this.activeStream) this.activeStream.markStreamRead(Ext.Date.add(new Date(), Ext.Date.DAY, -7))
	},
	markReadScroll: function(index) {
		if (index < this.posts.length && !this.posts.getAt(index).read) this.posts.getAt(index).toggleReadPost()
	},

	refreshPosts: function() {
		this.postFilterStore.load()
	},
	autoRefreshPosts: false,	
	refreshStreams: function() {
		this.streamStore.load();
	},
	toggleStreams: function(expand) {
		this.groups.forEach(function(group) {
			group.set('showAll', expand)
		})
	},
	showToggleStreams$: function() {
		return this.activeCard == 0
	},

	selectStream: function(stream) {
		this.groups.forEach(function(group) {
			group.streams.forEach(function(s) {
				if (Ext.isDefined(s.selected))
					s.set('selected', s == stream)
			})
		})
		this.switchToPostDisplay()
		if (this.miniMode) this.set('activeCard', 1)
	},

	findStream: function(stream) {
		var rStream = null;
		this.groups.forEach(function(group) {
			group.streams.forEach(function(s) {
				if (s == stream) rStream = stream
			})
		})
		return rStream
	},

	selectStreamById: function(streamId) {
		var found = false;
		this.groups.forEach(function(group) {
			group.streams.forEach(function(s) {
				if (s.id == streamId) found = true
				s.set('selected', s.id == streamId)
			})
		})
		if (this.miniMode) {
			if (found)
				this.set('activeCard', 1)
			else
				this.ajax({
					url: '/resolve/service/social/getStream',
					params: {
						id: streamId
					},
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							if (response.data) {
								this.set('activeStream', this.model(Ext.apply({
									mtype: 'RS.social.Stream'
								}, response.data)))
							}
						} else clientVM.displayError(response.message)
					}
				})
		}
	},

	selectFirstStream: function() {
		if (this.groups.length > 0) {
			var group = this.groups.getAt(0)
			if (group.streams.length > 0) this.selectStream(group.streams.getAt(1))
		}
	},

	hideLikes: function() {
		this.posts.forEach(function(p) {
			p.closePopups()
		})
	},

	removePost: function(post) {
		this.posts.remove(post)
	},

	joinComponent: function() {
		this.open({
			mtype: 'RS.social.StreamPicker',
			type: 'user',
			pickerType: 1
		})
	},

	/*Mini components*/
	activeCard: 0, //0 streams, 1 stream, 2 post, 3 post/comment

	when_activeCard_changes_update_data: {
		on: ['activeCardChanged'],
		action: function() {
			this.updateTitleText()
			this.updateBackText()
			this.updateShowFollow()
			this.refresh()
		}
	},

	initialStreamChanged: false,
	setInitialStream: function(id) {
		this.set('initialStreamChanged', true)
		this.set('initialStreamId', id)
		this.refreshPosts()
	},

	when_initialStreamId_changes_update_posts: {
		on: ['initialStreamIdChanged'],
		action: function() {
			if (this.initialStreamId) {
				this.set('pageNumber', 1)
				this.postFilterStore.load()
			}
		}
	},

	backText: '',
	updateBackText: function() {
		switch (this.activeCard) {
			case 0:
				this.set('backText', '')
				break;
			case 1:
				this.set('backText', this.localize('streams'))
				break;
			case 2:
				this.set('backText', this.activeStream ? this.activeStream.titleDisplay : '')
				break;
			case 3:
				this.set('backText', this.activeStream ? this.activeStream.titleDisplay : '')
				break;
			case 4:
				this.set('backText', this.activeStream ? this.activeStream.titleDisplay : '')
				break;
		}
	},

	titleText: '',
	titleTextTooltip: '',
	updateTitleText: function() {
		switch (this.activeCard) {
			case 0:
				this.set('titleText', this.localize('streams'))
				this.set('titleTextTooltip', this.localize('streams'))
				break;
			case 1:
				this.set('titleText', this.activeStream ? this.activeStream.titleDisplay : '')
				this.set('titleTextTooltip', this.activeStream ? this.activeStream.titleFullDisplay : '')
				break;
			case 2:
				this.set('titleText', this.activeStream ? this.activeStream.titleDisplay : '')
				this.set('titleTextTooltip', this.activeStream ? this.activeStream.titleFullDisplay : '')
				break;
			case 3:
				this.set('titleText', this.localize('newPost'))
				this.set('titleTextTooltip', this.localize('newPost'))
				break;
		}
	},


	refreshIsVisible$: function() {
		return this.activeCard < 3
	},

	showRefreshButton$: function() {
		return this.activeCard < 3
	},
	showPostButton$: function() {
		var stream = this.activeStream
		return stream && !stream.lock && (!stream.isMe || stream.id == 'blog') && (this.activeCard == 1 || this.activeCard == 2)
	},
	switchToPost: function() {
		var id = this.activeStream.id
		if (id == 'blog') {
			var index = this.streamToStore.findExact('name', this.localize('blog'))
			if (index > -1) {
				var blogStream = this.streamToStore.getAt(index)
				id = blogStream.data.id
			}
		}
		this.set('to', [id])
		this.set('activeCard', 3)
	},
	switchToForum: function() {
		this.set('activeCard', 4)
	},

	refresh: function() {
		switch (this.activeCard) {
			case 0:
				this.streamStore.load()
				break;
			case 1:
				this.postFilterStore.load()
				break;
			case 2:
				break;
		}
	},
	backIsVisible$: function() {
		return this.activeCard > 0
	},
	back: function() {
		if (this.activeCard == 3 || this.activeCard == 4) this.set('activeCard', 1)
		else if (this.activeCard > 0) this.set('activeCard', this.activeCard - 1)
	},

	updateMini: function() {
		this.updateBackText()
		this.updateTitleText()
		this.set('activeCard', 1)
	},

	cancelMini: function() {
		this.set('to', '')
		this.set('subject', '')
		this.set('content', '')
		this.set('activeCard', 1)
	},

	showPagingButtons$: function() {
		return this.activeCard == 1
	},

	updateShowFollow: function() {
		var temp = this.activeStream,
			stream = this.findStream(temp);
		if (temp)
			this.set('showFollow', stream == null && this.activeCard == 1)
	},

	showFollow: false,

	followStream: function() {
		this.ajax({
			url: '/resolve/service/social/setFollowStreams',
			params: {
				ids: [this.activeStream.id],
				follow: true
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) this.refreshStreams()
				else clientVM.displayError(response.message)
			}
		})
	},

	authenticateText$: function() {
		return this.authenticated ? false : this.localize('notAuthenticated')
	},

	previousPage: function() {
		this.set('pageNumber', this.pageNumber - 1)
	},
	previousPageIsEnabled$: function() {
		return this.pageNumber > 1
	},
	nextPage: function() {
		this.set('pageNumber', this.pageNumber + 1)
	},
	nextPageIsEnabled$: function() {
		return this.pageNumber * this.pageSize < this.totalCount
	},

	postDisplayItem: 0,
	activePost: {
		mtype: 'Post',
		viewMode: 'forum'
	},

	switchToForumDisplay: function(post) {
		this.activePost.syncPostProperties(post)
		if (this.miniMode) {
			this.switchToForum()
		} else {
			this.set('postDisplayItem', 1)
		}
	},

	switchToPostDisplay: function() {
		this.activePost.cancel()
		this.set('postDisplayItem', 0)
	},

	switchToPostDisplayIsVisible$: function() {
		return this.postDisplayItem == 1
	}
})