glu.defModel('RS.social.ConfigureNotifications', {

	activeItem: 0,

	selectAllProcess: false,
	PROCESS_CREATE: false,
	PROCESS_UPDATE: false,
	PROCESS_PURGED: false,
	PROCESS_RUNBOOK_ADDED: false,
	PROCESS_RUNBOOK_REMOVED: false,
	PROCESS_DOCUMENT_ADDED: false,
	PROCESS_DOCUMENT_REMOVED: false,
	PROCESS_ACTIONTASK_ADDED: false,
	PROCESS_ACTIONTASK_REMOVED: false,
	PROCESS_RSS_ADDED: false,
	PROCESS_RSS_REMOVED: false,
	PROCESS_FORUM_ADDED: false,
	PROCESS_FORUM_REMOVED: false,
	PROCESS_TEAM_ADDED: false,
	PROCESS_TEAM_REMOVED: false,
	PROCESS_USER_ADDED: false,
	PROCESS_USER_REMOVED: false,
	PROCESS_DIGEST_EMAIL: false,
	PROCESS_FORWARD_EMAIL: false,
	when_selectAllProcess_changes_to_true_select_all_process: {
		on: ['selectAllProcessChanged'],
		action: function() {
			if (this.selectAllProcess) {
				this.set('PROCESS_CREATE', true)
				this.set('PROCESS_UPDATE', true)
				this.set('PROCESS_PURGED', true)
				this.set('PROCESS_RUNBOOK_ADDED', true)
				this.set('PROCESS_RUNBOOK_REMOVED', true)
				this.set('PROCESS_DOCUMENT_ADDED', true)
				this.set('PROCESS_DOCUMENT_REMOVED', true)
				this.set('PROCESS_ACTIONTASK_ADDED', true)
				this.set('PROCESS_ACTIONTASK_REMOVED', true)
				this.set('PROCESS_RSS_ADDED', true)
				this.set('PROCESS_RSS_REMOVED', true)
				this.set('PROCESS_FORUM_ADDED', true)
				this.set('PROCESS_FORUM_REMOVED', true)
				this.set('PROCESS_TEAM_ADDED', true)
				this.set('PROCESS_TEAM_REMOVED', true)
				this.set('PROCESS_USER_ADDED', true)
				this.set('PROCESS_USER_REMOVED', true)
				this.set('PROCESS_DIGEST_EMAIL', true)
				this.set('PROCESS_FORWARD_EMAIL', true)
			}
		}
	},
	when_process_unselected_uncheck_select_all_box: {
		on: [
			'PROCESS_CREATEChanged',
			'PROCESS_UPDATEChanged',
			'PROCESS_PURGEDChanged',
			'PROCESS_RUNBOOK_ADDEDChanged',
			'PROCESS_RUNBOOK_REMOVEDChanged',
			'PROCESS_DOCUMENT_ADDEDChanged',
			'PROCESS_DOCUMENT_REMOVEDChanged',
			'PROCESS_ACTIONTASK_ADDEDChanged',
			'PROCESS_ACTIONTASK_REMOVEDChanged',
			'PROCESS_RSS_ADDEDChanged',
			'PROCESS_RSS_REMOVEDChanged',
			'PROCESS_FORUM_ADDEDChanged',
			'PROCESS_FORUM_REMOVEDChanged',
			'PROCESS_TEAM_ADDEDChanged',
			'PROCESS_TEAM_REMOVEDChanged',
			'PROCESS_USER_ADDEDChanged',
			'PROCESS_USER_REMOVEDChanged',
			'PROCESS_DIGEST_EMAILChanged',
			'PROCESS_FORWARD_EMAILChanged'
		],
		action: function() {
			if (this.PROCESS_CREATE &&
				this.PROCESS_UPDATE &&
				this.PROCESS_PURGED &&
				this.PROCESS_RUNBOOK_ADDED &&
				this.PROCESS_RUNBOOK_REMOVED &&
				this.PROCESS_DOCUMENT_ADDED &&
				this.PROCESS_DOCUMENT_REMOVED &&
				this.PROCESS_ACTIONTASK_ADDED &&
				this.PROCESS_ACTIONTASK_REMOVED &&
				this.PROCESS_RSS_ADDED &&
				this.PROCESS_RSS_REMOVED &&
				this.PROCESS_FORUM_ADDED &&
				this.PROCESS_FORUM_REMOVED &&
				this.PROCESS_TEAM_ADDED &&
				this.PROCESS_TEAM_REMOVED &&
				this.PROCESS_USER_ADDED &&
				this.PROCESS_USER_REMOVED &&
				this.PROCESS_DIGEST_EMAIL &&
				this.PROCESS_FORWARD_EMAIL) this.set('selectAllProcess', true)
			else {
				this.set('selectAllProcess', false)
			}
		}
	},

	selectAllTeam: false,
	TEAM_CREATE: false,
	TEAM_UPDATE: false,
	TEAM_PURGED: false,
	TEAM_ADDED_TO_PROCESS: false,
	TEAM_REMOVED_FROM_PROCESS: false,
	TEAM_ADDED_TO_TEAM: false,
	TEAM_REMOVED_FROM_TEAM: false,
	TEAM_USER_ADDED: false,
	TEAM_USER_REMOVED: false,
	TEAM_DIGEST_EMAIL: false,
	TEAM_FORWARD_EMAIL: false,

	when_selectAllTeam_changes_to_true_select_all_Team: {
		on: ['selectAllTeamChanged'],
		action: function() {
			if (this.selectAllTeam) {
				this.set('TEAM_CREATE', true)
				this.set('TEAM_UPDATE', true)
				this.set('TEAM_PURGED', true)
				this.set('TEAM_ADDED_TO_PROCESS', true)
				this.set('TEAM_REMOVED_FROM_PROCESS', true)
				this.set('TEAM_ADDED_TO_TEAM', true)
				this.set('TEAM_REMOVED_FROM_TEAM', true)
				this.set('TEAM_USER_ADDED', true)
				this.set('TEAM_USER_REMOVED', true)
				this.set('TEAM_DIGEST_EMAIL', true)
				this.set('TEAM_FORWARD_EMAIL', true)
			}
		}
	},
	when_team_unselected_uncheck_select_all_box: {
		on: [
			'TEAM_CREATEChanged',
			'TEAM_UPDATEChanged',
			'TEAM_PURGEDChanged',
			'TEAM_ADDED_TO_PROCESSChanged',
			'TEAM_REMOVED_FROM_PROCESSChanged',
			'TEAM_ADDED_TO_TEAMChanged',
			'TEAM_REMOVED_FROM_TEAMChanged',
			'TEAM_USER_ADDEDChanged',
			'TEAM_USER_REMOVEDChanged',
			'TEAM_DIGEST_EMAILChanged',
			'TEAM_FORWARD_EMAILChanged'
		],
		action: function() {
			if (this.TEAM_CREATE &&
				this.TEAM_UPDATE &&
				this.TEAM_PURGED &&
				this.TEAM_ADDED_TO_PROCESS &&
				this.TEAM_REMOVED_FROM_PROCESS &&
				this.TEAM_ADDED_TO_TEAM &&
				this.TEAM_REMOVED_FROM_TEAM &&
				this.TEAM_USER_ADDED &&
				this.TEAM_USER_REMOVED &&
				this.TEAM_DIGEST_EMAIL &&
				this.TEAM_FORWARD_EMAIL) this.set('selectAllTeam', true)
			else {
				this.set('selectAllTeam', false)
			}
		}
	},

	selectAllForum: false,
	FORUM_CREATE: false,
	FORUM_UPDATE: false,
	FORUM_PURGED: false,
	FORUM_ADDED_TO_PROCESS: false,
	FORUM_REMOVED_FROM_PROCESS: false,
	FORUM_USER_ADDED: false,
	FORUM_USER_REMOVED: false,
	FORUM_DIGEST_EMAIL: false,
	FORUM_FORWARD_EMAIL: false,

	when_selectAllForum_changes_to_true_select_all_Forum: {
		on: ['selectAllForumChanged'],
		action: function() {
			if (this.selectAllForum) {
				this.set('FORUM_CREATE', true)
				this.set('FORUM_UPDATE', true)
				this.set('FORUM_PURGED', true)
				this.set('FORUM_ADDED_TO_PROCESS', true)
				this.set('FORUM_REMOVED_FROM_PROCESS', true)
				this.set('FORUM_USER_ADDED', true)
				this.set('FORUM_USER_REMOVED', true)
				this.set('FORUM_DIGEST_EMAIL', true)
				this.set('FORUM_FORWARD_EMAIL', true)
			}
		}
	},
	when_forum_unselected_uncheck_select_all_box: {
		on: [
			'FORUM_CREATEChanged',
			'FORUM_UPDATEChanged',
			'FORUM_PURGEDChanged',
			'FORUM_ADDED_TO_PROCESSChanged',
			'FORUM_REMOVED_FROM_PROCESSChanged',
			'FORUM_USER_ADDEDChanged',
			'FORUM_USER_REMOVEDChanged',
			'FORUM_DIGEST_EMAILChanged',
			'FORUM_FORWARD_EMAILChanged'
		],
		action: function() {
			if (this.FORUM_CREATE &&
				this.FORUM_UPDATE &&
				this.FORUM_PURGED &&
				this.FORUM_ADDED_TO_PROCESS &&
				this.FORUM_REMOVED_FROM_PROCESS &&
				this.FORUM_USER_ADDED &&
				this.FORUM_USER_REMOVED &&
				this.FORUM_DIGEST_EMAIL &&
				this.FORUM_FORWARD_EMAIL) this.set('selectAllForum', true)
			else {
				this.set('selectAllForum', false)
			}
		}
	},

	selectAllUser: false,
	USER_ADDED_TO_PROCESS: false,
	USER_ADDED_TO_TEAM: false,
	USER_FOLLOW_ME: false,
	USER_REMOVED_FROM_PROCESS: false,
	USER_REMOVED_FROM_TEAM: false,
	USER_UNFOLLOW_ME: false,
	USER_ADDED_TO_FORUM: false,
	USER_REMOVED_FROM_FORUM: false,
	USER_PROFILE_CHANGE: false,
	USER_DIGEST_EMAIL: false,
	USER_FORWARD_EMAIL: false,

	when_selectAllUser_changes_to_true_select_all_User: {
		on: ['selectAllUserChanged'],
		action: function() {
			if (this.selectAllUser) {
				this.set('USER_ADDED_TO_PROCESS', true)
				this.set('USER_ADDED_TO_TEAM', true)
				this.set('USER_FOLLOW_ME', true)
				this.set('USER_REMOVED_FROM_PROCESS', true)
				this.set('USER_REMOVED_FROM_TEAM', true)
				this.set('USER_UNFOLLOW_ME', true)
				this.set('USER_ADDED_TO_FORUM', true)
				this.set('USER_REMOVED_FROM_FORUM', true)
				this.set('USER_PROFILE_CHANGE', true)
				this.set('USER_DIGEST_EMAIL', true)
				this.set('USER_FORWARD_EMAIL', true)
			}
		}
	},
	when_user_unselected_uncheck_select_all_box: {
		on: [
			'USER_ADDED_TO_PROCESSChanged',
			'USER_ADDED_TO_TEAMChanged',
			'USER_FOLLOW_MEChanged',
			'USER_REMOVED_FROM_PROCESSChanged',
			'USER_REMOVED_FROM_TEAMChanged',
			'USER_UNFOLLOW_MEChanged',
			'USER_ADDED_TO_FORUMChanged',
			'USER_REMOVED_FROM_FORUMChanged',
			'USER_PROFILE_CHANGEChanged',
			'USER_DIGEST_EMAILChanged',
			'USER_FORWARD_EMAILChanged'
		],
		action: function() {
			if (this.USER_ADDED_TO_PROCESS &&
				this.USER_ADDED_TO_TEAM &&
				this.USER_FOLLOW_ME &&
				this.USER_REMOVED_FROM_PROCESS &&
				this.USER_REMOVED_FROM_TEAM &&
				this.USER_UNFOLLOW_ME &&
				this.USER_ADDED_TO_FORUM &&
				this.USER_REMOVED_FROM_FORUM &&
				this.USER_PROFILE_CHANGE &&
				this.USER_DIGEST_EMAIL &&
				this.USER_FORWARD_EMAIL) this.set('selectAllUser', true)
			else {
				this.set('selectAllUser', false)
			}
		}
	},

	selectAllActionTask: false,
	ACTIONTASK_CREATE: false,
	ACTIONTASK_UPDATE: false,
	ACTIONTASK_PURGED: false,
	ACTIONTASK_ADDED_TO_PROCESS: false,
	ACTIONTASK_REMOVED_FROM_PROCESS: false,
	ACTIONTASK_DIGEST_EMAIL: false,
	ACTIONTASK_FORWARD_EMAIL: false,

	when_selectAllActionTask_changes_to_true_select_all_ActionTask: {
		on: ['selectAllActionTaskChanged'],
		action: function() {
			if (this.selectAllActionTask) {
				this.set('ACTIONTASK_CREATE', true)
				this.set('ACTIONTASK_UPDATE', true)
				this.set('ACTIONTASK_PURGED', true)
				this.set('ACTIONTASK_ADDED_TO_PROCESS', true)
				this.set('ACTIONTASK_REMOVED_FROM_PROCESS', true)
				this.set('ACTIONTASK_DIGEST_EMAIL', true)
				this.set('ACTIONTASK_FORWARD_EMAIL', true)
			}
		}
	},
	when_actiontask_unselected_uncheck_select_all_box: {
		on: [
			'ACTIONTASK_CREATEChanged',
			'ACTIONTASK_UPDATEChanged',
			'ACTIONTASK_PURGEDChanged',
			'ACTIONTASK_ADDED_TO_PROCESSChanged',
			'ACTIONTASK_REMOVED_FROM_PROCESSChanged',
			'ACTIONTASK_DIGEST_EMAILChanged',
			'ACTIONTASK_FORWARD_EMAILChanged'
		],
		action: function() {
			if (this.ACTIONTASK_CREATE &&
				this.ACTIONTASK_UPDATE &&
				this.ACTIONTASK_PURGED &&
				this.ACTIONTASK_ADDED_TO_PROCESS &&
				this.ACTIONTASK_REMOVED_FROM_PROCESS &&
				this.ACTIONTASK_DIGEST_EMAIL &&
				this.ACTIONTASK_FORWARD_EMAIL) this.set('selectAllActionTask', true)
			else {
				this.set('selectAllActionTask', false)
			}
		}
	},

	selectAllRunbook: false,
	RUNBOOK_UPDATE: false,
	RUNBOOK_ADDED_TO_PROCESS: false,
	RUNBOOK_REMOVED_FROM_PROCESS: false,
	RUNBOOK_COMMIT: false,
	RUNBOOK_DIGEST_EMAIL: false,
	RUNBOOK_FORWARD_EMAIL: false,

	when_selectAllRunbook_changes_to_true_select_all_Runbook: {
		on: ['selectAllRunbookChanged'],
		action: function() {
			if (this.selectAllRunbook) {
				this.set('RUNBOOK_UPDATE', true)
				this.set('RUNBOOK_ADDED_TO_PROCESS', true)
				this.set('RUNBOOK_REMOVED_FROM_PROCESS', true)
				this.set('RUNBOOK_COMMIT', true)
				this.set('RUNBOOK_DIGEST_EMAIL', true)
				this.set('RUNBOOK_FORWARD_EMAIL', true)
			}
		}
	},
	when_runbook_unselected_uncheck_select_all_box: {
		on: [
			'RUNBOOK_UPDATEChanged',
			'RUNBOOK_ADDED_TO_PROCESSChanged',
			'RUNBOOK_REMOVED_FROM_PROCESSChanged',
			'RUNBOOK_COMMITChanged',
			'RUNBOOK_DIGEST_EMAILChanged',
			'RUNBOOK_FORWARD_EMAILChanged'
		],
		action: function() {
			if (this.RUNBOOK_UPDATE &&
				this.RUNBOOK_ADDED_TO_PROCESS &&
				this.RUNBOOK_REMOVED_FROM_PROCESS &&
				this.RUNBOOK_COMMIT &&
				this.RUNBOOK_DIGEST_EMAIL &&
				this.RUNBOOK_FORWARD_EMAIL) this.set('selectAllRunbook', true)
			else {
				this.set('selectAllRunbook', false)
			}
		}
	},

	selectAllDocument: false,
	DOCUMENT_CREATE: false,
	DOCUMENT_UPDATE: false,
	DOCUMENT_DELETE: false,
	DOCUMENT_PURGED: false,
	DOCUMENT_ADDED_TO_PROCESS: false,
	DOCUMENT_REMOVED_FROM_PROCESS: false,
	DOCUMENT_COMMIT: false,
	DOCUMENT_DIGEST_EMAIL: false,
	DOCUMENT_FORWARD_EMAIL: false,

	when_selectAllDocument_changes_to_true_select_all_Document: {
		on: ['selectAllDocumentChanged'],
		action: function() {
			if (this.selectAllDocument) {
				this.set('DOCUMENT_CREATE', true)
				this.set('DOCUMENT_UPDATE', true)
				this.set('DOCUMENT_DELETE', true)
				this.set('DOCUMENT_PURGED', true)
				this.set('DOCUMENT_ADDED_TO_PROCESS', true)
				this.set('DOCUMENT_REMOVED_FROM_PROCESS', true)
				this.set('DOCUMENT_COMMIT', true)
				this.set('DOCUMENT_DIGEST_EMAIL', true)
				this.set('DOCUMENT_FORWARD_EMAIL', true)
			}
		}
	},
	when_document_unselected_uncheck_select_all_box: {
		on: [
			'DOCUMENT_CREATEChanged',
			'DOCUMENT_UPDATEChanged',
			'DOCUMENT_DELETEChanged',
			'DOCUMENT_PURGEDChanged',
			'DOCUMENT_ADDED_TO_PROCESSChanged',
			'DOCUMENT_REMOVED_FROM_PROCESSChanged',
			'DOCUMENT_COMMITChanged',
			'DOCUMENT_DIGEST_EMAILChanged',
			'DOCUMENT_FORWARD_EMAILChanged'
		],
		action: function() {
			if (this.DOCUMENT_CREATE &&
				this.DOCUMENT_UPDATE &&
				this.DOCUMENT_DELETE &&
				this.DOCUMENT_PURGED &&
				this.DOCUMENT_ADDED_TO_PROCESS &&
				this.DOCUMENT_REMOVED_FROM_PROCESS &&
				this.DOCUMENT_COMMIT &&
				this.DOCUMENT_DIGEST_EMAIL &&
				this.DOCUMENT_FORWARD_EMAIL) this.set('selectAllDocument', true)
			else {
				this.set('selectAllDocument', false)
			}
		}
	},

	selectAllRSS: false,
	RSS_CREATE: false,
	RSS_UPDATE: false,
	RSS_PURGED: false,
	RSS_ADDED_TO_PROCESS: false,
	RSS_REMOVED_FROM_PROCESS: false,
	RSS_DIGEST_EMAIL: false,
	RSS_FORWARD_EMAIL: false,

	when_selectAllRSS_changes_to_true_select_all_RSS: {
		on: ['selectAllRSSChanged'],
		action: function() {
			if (this.selectAllRSS) {
				this.set('RSS_CREATE', true)
				this.set('RSS_UPDATE', true)
				this.set('RSS_PURGED', true)
				this.set('RSS_ADDED_TO_PROCESS', true)
				this.set('RSS_REMOVED_FROM_PROCESS', true)
				this.set('RSS_DIGEST_EMAIL', true)
				this.set('RSS_FORWARD_EMAIL', true)
			}
		}
	},
	when_rss_unselected_uncheck_select_all_box: {
		on: [
			'RSS_CREATEChanged',
			'RSS_UPDATEChanged',
			'RSS_PURGEDChanged',
			'RSS_ADDED_TO_PROCESSChanged',
			'RSS_REMOVED_FROM_PROCESSChanged',
			'RSS_DIGEST_EMAILChanged',
			'RSS_FORWARD_EMAILChanged'
		],
		action: function() {
			if (this.RSS_CREATE &&
				this.RSS_UPDATE &&
				this.RSS_PURGED &&
				this.RSS_ADDED_TO_PROCESS &&
				this.RSS_REMOVED_FROM_PROCESS &&
				this.RSS_DIGEST_EMAIL &&
				this.RSS_FORWARD_EMAIL) this.set('selectAllRSS', true)
			else {
				this.set('selectAllRSS', false)
			}
		}
	},

	fields: [
		'PROCESS_CREATE',
		'PROCESS_UPDATE',
		'PROCESS_PURGED',
		'PROCESS_RUNBOOK_ADDED',
		'PROCESS_RUNBOOK_REMOVED',
		'PROCESS_DOCUMENT_ADDED',
		'PROCESS_DOCUMENT_REMOVED',
		'PROCESS_ACTIONTASK_ADDED',
		'PROCESS_ACTIONTASK_REMOVED',
		'PROCESS_RSS_ADDED',
		'PROCESS_RSS_REMOVED',
		'PROCESS_FORUM_ADDED',
		'PROCESS_FORUM_REMOVED',
		'PROCESS_TEAM_ADDED',
		'PROCESS_TEAM_REMOVED',
		'PROCESS_USER_ADDED',
		'PROCESS_USER_REMOVED',
		'PROCESS_DIGEST_EMAIL',
		'PROCESS_FORWARD_EMAIL',
		'TEAM_CREATE',
		'TEAM_UPDATE',
		'TEAM_PURGED',
		'TEAM_ADDED_TO_PROCESS',
		'TEAM_REMOVED_FROM_PROCESS',
		'TEAM_ADDED_TO_TEAM',
		'TEAM_REMOVED_FROM_TEAM',
		'TEAM_USER_ADDED',
		'TEAM_USER_REMOVED',
		'TEAM_DIGEST_EMAIL',
		'TEAM_FORWARD_EMAIL',
		'FORUM_CREATE',
		'FORUM_UPDATE',
		'FORUM_PURGED',
		'FORUM_ADDED_TO_PROCESS',
		'FORUM_REMOVED_FROM_PROCESS',
		'FORUM_USER_ADDED',
		'FORUM_USER_REMOVED',
		'FORUM_DIGEST_EMAIL',
		'FORUM_FORWARD_EMAIL',
		'USER_ADDED_TO_PROCESS',
		'USER_ADDED_TO_TEAM',
		'USER_FOLLOW_ME',
		'USER_REMOVED_FROM_PROCESS',
		'USER_REMOVED_FROM_TEAM',
		'USER_UNFOLLOW_ME',
		'USER_ADDED_TO_FORUM',
		'USER_REMOVED_FROM_FORUM',
		'USER_PROFILE_CHANGE',
		'USER_DIGEST_EMAIL',
		'USER_FORWARD_EMAIL',
		'ACTIONTASK_CREATE',
		'ACTIONTASK_UPDATE',
		'ACTIONTASK_PURGED',
		'ACTIONTASK_ADDED_TO_PROCESS',
		'ACTIONTASK_REMOVED_FROM_PROCESS',
		'ACTIONTASK_DIGEST_EMAIL',
		'ACTIONTASK_FORWARD_EMAIL',
		'RUNBOOK_UPDATE',
		'RUNBOOK_ADDED_TO_PROCESS',
		'RUNBOOK_REMOVED_FROM_PROCESS',
		'RUNBOOK_COMMIT',
		'RUNBOOK_DIGEST_EMAIL',
		'RUNBOOK_FORWARD_EMAIL',
		'DOCUMENT_CREATE',
		'DOCUMENT_UPDATE',
		'DOCUMENT_DELETE',
		'DOCUMENT_PURGED',
		'DOCUMENT_ADDED_TO_PROCESS',
		'DOCUMENT_REMOVED_FROM_PROCESS',
		'DOCUMENT_COMMIT',
		'DOCUMENT_DIGEST_EMAIL',
		'DOCUMENT_FORWARD_EMAIL',
		'RSS_CREATE',
		'RSS_UPDATE',
		'RSS_PURGED',
		'RSS_ADDED_TO_PROCESS',
		'RSS_REMOVED_FROM_PROCESS',
		'RSS_DIGEST_EMAIL',
		'RSS_FORWARD_EMAIL'
	],

	typeStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	type: '',

	when_type_changes_update_active_card: {
		on: ['typeChanged'],
		action: function() {
			switch (this.type) {
				case 'PROCESS':
					this.set('activeItem', 0)
					break;
				case 'TEAM':
					this.set('activeItem', 1)
					break;
				case 'FORUM':
					this.set('activeItem', 2)
					break;
				case 'USER':
					this.set('activeItem', 3)
					break;
				case 'ACTIONTASK':
					this.set('activeItem', 4)
					break;
				case 'RUNBOOK':
					this.set('activeItem', 5)
					break;
				case 'DOCUMENT':
					this.set('activeItem', 6)
					break;
				case 'DECISIONTREES':
					this.set('activeItem', 7)
					break;
				case 'RSS':
					this.set('activeItem', 8)
					break;
			}
		}
	},

	init: function() {
		this.loadNotifications()
	},

	loadNotifications: function() {
		this.ajax({
			url: '/resolve/service/social/global/notify/get',
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					Ext.Array.forEach(response.records || [], function(record) {
						if (record.notifyTypes.length > 0)
							this.typeStore.add({
								name: this.localize(record.compType),
								value: record.compType
							})
						Ext.Array.forEach(record.notifyTypes || [], function(type) {
							if (Ext.isDefined(this[type.type]))
								this.set(type.type, type.value === 'true' || type.value === true)
						}, this)
					}, this)

					if (this.typeStore.getCount() > 0) this.set('type', this.typeStore.getAt(0).get('value'))
				}
			}
		})
	},

	save: function() {
		var params = this.asObject(),
			selectedTypes = ['blank'];
		Ext.Object.each(params, function(param) {
			if (params[param] == 'true') selectedTypes.push(param)
		})

		this.ajax({
			url: '/resolve/service/social/global/notify/update',
			params: {
				selectedTypes: selectedTypes
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					displaySuccess(this.localize('notficationsSaved'))
					this.doClose()
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	cancel: function() {
		this.doClose()
	}
})