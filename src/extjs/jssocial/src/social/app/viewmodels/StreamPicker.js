glu.defModel('RS.social.StreamPicker', {

	windowTitle$: function() {
		switch (this.pickerType) {
			case 0:
				return this.localize('addressToDialogWindowTitle')
				break;
			case 1:
				return this.localize('followDialogWindowTitle')
				break;
			case 2:
				return this.localize('addFollowersWindowTitle')
				break;
		}
	},

	activeItem$: function() {
		return this.type == 'worksheet' ? 1 : 0
	},

	/**
		0 - Follow Dialog
		1 - Address Dialog
		2 - Add Followers Dialog
	*/
	pickerType: 0,

	streamId: '',

	toGrid: {
		mtype: 'store',
		sorters: ['displayName'],
		fields: ['id', 'name', 'type', 'displayName'],
		remoteSort: true,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/social/listAllStreams',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	toGridSelections: [],

	toGridColumns: [{
			dataIndex: 'displayName',
			header: '~~name~~',
			flex: 1
		}
		/*, {
		dataIndex: 'type',
		header: '~~type~~'
	}*/
	],

	type: '',
	typeIsVisible: true,
	typeStore: {
		mtype: 'store',
		fields: ['name', {
			name: 'value',
			convert: function(v) {
				return (v || '').toLowerCase()
			}
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/social/listComponentTypes',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	searchQuery: '',

	worksheets: {
		mtype: 'store',
		remoteSort: true,
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		fields: ['id', 'sysId', 'number', 'reference', 'correlationId', 'alertId', 'wsData_dt_dtList'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/worksheet/list',			
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	worksheetsSelections: [],
	worksheetsColumns: [],

	myWorksheetsIsPressed: false,
	myWorksheets: function() {
		this.set('myWorksheetsIsPressed', !this.myWorksheetsIsPressed)
	},
	myWorksheetsPressedCls$: function() {
		return this.myWorksheetsIsPressed ? 'rs-social-button-pressed' : ''
	},
	myWorksheetsTooltip$: function() {
		return this.myWorksheetsIsPressed ? this.localize('showAllWorksheets') : this.localize('showMyWorksheets')
	},

	searchText: '',
	when_searchText_or_my_worksheets_changes_update_worksheet_grid: {
		on: ['searchTextChanged', 'myWorksheetsIsPressedChanged'],
		action: function() {
			this.worksheets.load()
		}
	},

	init: function() {
		if (this.pickerType == 2) {
			this.type = 'user'
			this.set('typeIsVisible', false)
		}

		this.toGrid.on('beforeload', function(store, operation, eOpts) {
			var filters = [];

			if (this.type) filters.push({
				field: 'type',
				condition: 'equals',
				value: this.type
			})

			if (this.searchQuery) filters.push({
				field: 'name',
				condition: 'startsWith',
				value: this.searchQuery
			})

			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				filter: Ext.encode(filters)
			})
		}, this)

		this.typeStore.on('load', function(store, records) {
			Ext.Array.forEach(records || [], function(record) {
				record.set('name', this.localize(record.get('name').toLowerCase()))
			}, this)

			//Reorder the components so that our groups are on top
			for (var i = 0; i < records.length; i++) {
				if (records[i].get('value').toLowerCase() == 'forum')
					records.splice(0, 0, records.splice(i, 1)[0])
				if (records[i].get('value').toLowerCase() == 'process')
					records.splice(1, 0, records.splice(i, 1)[0])
				if (records[i].get('value').toLowerCase() == 'team')
					records.splice(2, 0, records.splice(i, 1)[0])
			}
			store.loadData(records)
		}, this)


		this.typeStore.load()

		this.set('worksheetsColumns', [{
			dataIndex: 'number',
			text: '~~number~~',
			filterable: false,
			sortable: true,
			flex: 1
		}, {
			dataIndex: 'reference',
			text: '~~reference~~',
			filterable: false,
			sortable: false,
			flex: 1,
			renderer: function(value, metaData, record) {
				return value || record.get('correlationId') || record.get('alertId') || ''
			}
		}, {
			text: RS.common.locale.sysUpdated,
			dataIndex: 'sysUpdatedOn',
			filterable: false,
			sortable: true,
			width: 200,
			renderer: Ext.util.Format.dateRenderer('M j Y, h:i:s.u')
		}])

		this.worksheets.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {}

			var filter = [];
			if (this.searchText) {
				filter = [{
					field: 'alertId',
					type: 'auto',
					condition: 'contains',
					value: this.searchText
				}, {
					field: 'correlationId',
					type: 'auto',
					condition: 'contains',
					value: this.searchText
				}, {
					field: 'number',
					type: 'auto',
					condition: 'contains',
					value: this.searchText
				}, {
					field: 'reference',
					type: 'auto',
					condition: 'contains',
					value: this.searchText
				}]
			}

			if (this.myWorksheetsIsPressed) {
				filter.push({
					field: 'assignedToName',
					type: 'auto',
					condition: 'equals',
					value: clientVM.user.name
				})
			}

			Ext.apply(operation.params, {
				operator: 'or',
				filter: Ext.encode(filter),
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			})
		}, this)

		this.worksheets.load()

		if (!this.type)
			this.typeStore.on('load', function(store, records) {
				if (records.length > 0) {
					Ext.Array.forEach(records || [], function(record) {
							if (record.get('value').toLowerCase() == 'team')
								this.set('type', record.get('value'))
						}, this)
						// this.set('type', records[0].get('value'))
				}
			}, this, {
				single: true
			})
		else
			this.toGrid.load()
	},

	when_filter_changes_reload_grid: {
		on: ['typeChanged', 'searchQueryChanged'],
		action: function() {
			this.toGrid.loadPage(1)
		}
	},

	search: function() {
		this.toGrid.load()
	},

	select: function() {
		var selections = [];
		if (this.parentVM.to)
			Ext.Array.forEach(this.parentVM.to, function(to) {
				selections.push(to)
			}, this)
		Ext.Array.forEach(this.type == 'worksheet' ? this.worksheetsSelections : this.toGridSelections, function(selection) {
			var contains = false
			Ext.Array.forEach(selections, function(sel) {
				if (sel == selection || (sel.get && sel.get('id') == selection)) contains = true
			})
			if (!contains) {
				if (this.type == 'worksheet') Ext.apply(selection.data, {
					name: selection.get('number'),
					displayName: selection.get('number'),
					type: 'worksheet'
				})
				selections.push(selection)
			}
		}, this)
		this.parentVM.set('toHidden', selections)
		this.doClose()
	},
	selectIsVisible$: function() {
		return this.pickerType == 0
	},
	selectIsEnabled$: function() {
		return this.type == 'worksheet' ? this.worksheetsSelections.length > 0 : this.toGridSelections.length > 0
	},

	follow: function() {
		//Call in to the server to add these streams to the user
		var selections = [];
		Ext.Array.forEach(this.type == 'worksheet' ? this.worksheetsSelections : this.toGridSelections, function(selection) {
			selections.push(selection.get('id'))
		})
		this.ajax({
			url: '/resolve/service/social/setFollowStreams',
			params: {
				ids: selections,
				follow: true,
				streamType: this.type
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.rootVM.refreshStreams()
					this.doClose()
				} else clientVM.displayError(response.message)
			}
		})
	},
	followIsVisible$: function() {
		return this.pickerType == 1
	},
	followIsEnabled$: function() {
		return this.selectIsEnabled
	},

	addFollowers: function() {
		//Call in to the server to add these streams to the user
		var selections = [];
		Ext.Array.forEach(this.type == 'worksheet' ? this.worksheetsSelections : this.toGridSelections, function(selection) {
			selections.push(selection.get('id'))
		})
		this.ajax({
			url: '/resolve/service/social/setFollowers',
			params: {
				streamIds: [this.streamId],
				streamType: this.streamType,
				ids: selections,
				follow: true
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.doClose()
				}
			}
		})
	},
	addFollowersIsVisible$: function() {
		return this.pickerType == 2
	},
	addFollowersIsEnabled$: function() {
		return this.selectIsEnabled
	},

	cancel: function() {
		this.doClose()
	},

	createComponent: function() {
		this.parentVM.open({
			mtype: 'RS.socialadmin.CreateComponent',
			type: this.type
		})
		this.doClose()
	},
	createComponentIsVisible$: function() {
		return this.followIsVisible && (hasPermission('admin') || hasPermission('socialadmin'));
	}
})