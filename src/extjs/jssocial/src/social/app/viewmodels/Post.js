glu.defModel('RS.social.Post', {
	id: '',
	authorUsername: '',
	authorDisplayName: '',
	authorSysId: '',
	commentAuthorUsername: '',
	commentAuthorDisplayName: '',
	authorId: '',
	title: '',
	uri: '',
	subject$: function() {
		return Ext.htmlEncode(this.title);
	},
	viewMode: '',
	component: '',
	componentType: '',
	content: '',
	csrftoken: '',
	contentDisplay$: function() {
		//hashtag ee.log(this.content);xtraction to auto-link
		var content = this.content,
			hashtags;
		//Weak logic to rely on string generated from backend to decide if this is our custom HTML content.
		var isRSContent = content.indexOf('<!-- HTML_BEGIN -->') != -1 || content.indexOf('Searched Wiki Documents') != -1;
		content = content.replace(/<span class="suggest"><\/span>/g, '').replace(/&lt;span class=&quot;suggest&quot;&gt;&lt;\/span&gt;/g, '')

		hashtags = content.match(/(^|\s)#([\w\d]*[\w\d])\s+/g)

		if (hashtags) {
			Ext.Array.forEach(hashtags, function(rawTag) {
				var tag = Ext.String.trim(rawTag);
				if (tag.indexOf('&quot;') == -1)
					content = content.replace(tag, tag.substring(0, tag.indexOf('#')) + Ext.String.format('<a href="#RS.search.Main/tag={0}">{1}</a>', tag.substring(tag.indexOf('#') + 1), tag.substring(tag.indexOf('#'))))
			})
		}

		if ((content.indexOf('localhost') != -1) || (content.indexOf(window.location.origin) != -1) && (content.indexOf('resolve/jsp/rsclient.jsp?') != -1)) {
			const urlData = content.split('href=');
			var tmpContent = '';
			if (urlData.length > 1) {
				for (var i=0; i < urlData.length; i++) {
					if ((urlData[i].indexOf('localhost') != -1) || (urlData[i].indexOf(window.location.origin) != -1) && (urlData[i].indexOf('resolve/jsp/rsclient.jsp?') != -1)) {
						var subUrlPart = urlData[i].split('resolve/jsp/rsclient.jsp?');
						if (subUrlPart.length > 1) {
							tmpContent += unescape('href=\'/resolve/jsp/rsclient.jsp?' + clientVM.rsclientToken + '%26' + subUrlPart[1]);
						} else {
							tmpContent += subUrlPart[0];
						}
					} else {
						tmpContent += urlData[i];
					}
				}
				if (tmpContent) {
					content = tmpContent;
				}
			}
		}

		return isRSContent ? Ext.util.Format.nl2br(content) : Ext.util.Format.nl2br(Ext.String.htmlEncode(content));
	},
	starred: false,
	numberOfLikes: 0,
	numberOfComments: 0,
	liked: false,
	sysCreatedOn: new Date(),
	sysUpdatedOn: new Date(),
	now: new Date(),
	comments: [],
	targets: [],
	beforeDestroyComponent : function(){
		if(this.poller)
			this.poller.destroy();
		if(this.runner)
			this.runner.destroy();
	},
	changeInterestedTarget: function(target) {
		this.set('component', target.name);
	},
	shouldBeLocked$: function() {
		var interestedTarget = null;
		Ext.each(this.targets, function(target) {
			if (target.name == this.component)
				interestedTarget = target;
		}, this);
		return interestedTarget && interestedTarget.locked || this.locked;
	},
	locked: false,
	read: true,
	hasDeleteRights: false,

	answer: false,

	commentsList: {
		mtype: 'list'
	},

	targetList: {
		mtype: 'list'
	},

	to$: function() {
		return this.id
	},

	commentContent: '',
	activePostCard: 0,
	switchToPostMessage: function() {
		if (this.getRootVM().cancel && this.getRootVM().posts) {
			this.getRootVM().cancel()
			this.getRootVM().posts.forEach(function(post) {
				post.cancel()
			})
		}
		this.set('activePostCard', 1)
	},

	cancel: function() {
		this.resetPost()
	},

	resetPost: function() {
		this.set('activePostCard', 0)
		this.set('commentContent', '')
	},
	comment: function(form) {
		this.getParentVM().comment(form, this)
	},
	commentIsEnabled$: function() {
		return this.postIsEnabled
	},
	posting: false,
	postIsEnabled$: function() {
		return !this.posting && this.commentContent.indexOf('/resolve/js/extjs4/resources/ext-theme-neptune/images/loadmask/loading.gif') == -1 && this.commentContent.length > 0
	},

	/* Deprecated feature
	createLink: function(editor) {
		this.open({
			mtype: 'RS.social.CreateLink',
			editor: editor
		})
	},
	*/

	/* Deprecated feature
	attachFile: function(editor) {
		this.open({
			mtype: 'RS.social.AttachFile',
			editor: editor
		})
	},
	*/

	isPost$: function() {
		return this.getParentVM().viewmodelName != 'Post'
	},

	imageSize$: function() {
		return this.isPost && !this.getRootVM().miniMode ? 46 : 28
	},

	imagePadding$: function() {
		return this.isPost ? '0px 10px 0px 0px' : '10px 10px 0px 0px'
	},

	imageAlignment$: function() {
		return this.isPost && this.getParentVM().displayPostProfilePictures ? 'middle' : 'top'
	},

	showAuthorSubject$: function() {
		return !this.subject
	},

	subjectDisplay$: function() {
		return this.getRootVM().miniMode ? Ext.util.Format.ellipsis(this.subject, 20) : Ext.util.Format.ellipsis(this.subject, 75)
	},

	subjectTooltip$: function() {
		return this.getRootVM().miniMode ? this.subject : ''
	},

	dateTooltip$: function() {
		return this.getRootVM().miniMode ? this.dateText : ''
	},

	contentPadding$: function() {
		return this.isPost && this.getParentVM().displayPostProfilePictures ? '10px 0px 0px 0px' : '10px 0px 0px 5px'
	},

	showStar$: function() {
		return true //this.type != 'system'
	},

	username: '',

	showCaret$: function() {
		return this.isPost && !this.getRootVM().miniMode && this.showAuthor && this.type != 'system' && this.type != 'manual'
	},

	showAuthor$: function() {
		var hasNonRSS = false
			/*
			 * To remove displaying system user
			 * from post uncomment the following
			 * // commented out lines
			 */
			//if(this.authorUsername.toLowerCase() != 'system'){
		Ext.Array.each(this.targets, function(t) {
				if (t.type != 'RSS') {
					hasNonRSS = true
					return false
				}
			})
			//}

		return hasNonRSS
	},

	displayComponent$: function() {
		return !this.getRootVM().miniMode && this.type != 'system' && this.type != 'manual'
	},

	displayAddComment$: function() {
		return !this.shouldBeLocked && (this.viewMode == 'forum' || !this.isForum) && (this.getParentVM().activeStream && !this.getParentVM().activeStream.lock) && this.type != 'system' && this.type != 'manual'
	},

	targetsLocked$: function() {
		var hasNonLocked = false
		Ext.Array.each(this.targets, function(t) {
			if (!t.lock) {
				hasNonLocked = true
				return false
			}
		})
		return !hasNonLocked
	},

	init: function() {
		this.initToken();
		this.set('now', new Date())
		if (!Ext.isDate(this.sysCreatedOn)) this.set('sysCreatedOn', Ext.Date.parse(this.sysCreatedOn, 'time'))
		if (!Ext.isNumber(this.numberOfLikes)) this.set('numberOfLikes', Number(this.numberOfLikes))

		if (!this.runner) this.runner = new Ext.util.TaskRunner()
		if (!this.poller) this.poller = this.runner.newTask({
			run: this.updateNow,
			scope: this,
			interval: 15000 //15 seconds
		})
		this.poller.start()

		this.targets = this.targets || []

		Ext.Array.forEach(this.targets, function(target) {
			if (target.sysId == this.authorSysId) target.name = this.localize('blog')
			if (this.getParentVM().activeStream && this.getParentVM().activeStream.id == 'system') target.name = this.localize('system')
			this.targetList.add(this.model(Ext.apply({
				mtype: 'viewmodel'
			}, target)))
			if (target.type && target.type.toLowerCase() == 'rss') this.set('componentType', 'rss')
			if (target.type && target.type.toLowerCase() == 'forum') this.set('componentType', 'forum')
		}, this)

		if (this.targetList.length > 0) {
			//determine best target to display
			var set = false
			this.targetList.forEach(function(target) {
				if (this.getParentVM().activeStream && target.name == this.getParentVM().activeStream.name) {
					this.set('component', target.type == 'USER' ? target.displayName : target.name)
					set = true
				}
			}, this)
			if (!set) this.set('component', this.targetList.getAt(0).type == 'USER' ? this.targetList.getAt(0).displayName : this.targetList.getAt(0).name)
		} else this.set('component', this.localize('blog'))

		if (this.isPost) {
			this.comments = this.comments || []
			Ext.Array.forEach(this.comments, function(child) {
				this.commentsList.add(this.model(Ext.apply({
					mtype: 'RS.social.Post'
				}, child)))

				if (child.answer) {
					this.set('answered', true)
					this.answersList.add(this.model(Ext.apply({
						mtype: 'RS.social.Post'
					}, child)))
				}
			}, this)
		} else {
			//determine delete rights
			this.set('hasDeleteRights', this.getParentVM().username == this.commentAuthorUsername || hasPermission('admin'))
		}
		this.determineFollowing()
	},
	activate: function() {
		this.initToken();
	},
	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/user/downloadProfile', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this));
	},
	getRootVM : function(){
		return this.rootVM;
	},
	getParentVM : function(){
		return this.parentVM;
	},
	updateNow: function() {
		this.set('now', new Date())
	},
	starCls$: function() {
		return 'post-star icon-large ' + (this.starred ? 'icon-star post-starred' : 'icon-star-empty');
	},
	starTooltip$: function() {
		return this.starred ? this.localize('starTooltip') : this.localize('starredTooltip')
	},

	numberOfLikesText$: function() {
		return Ext.String.format('({0})', this.numberOfLikes)
	},

	showImage$: function() {
		return this.isPost ? this.getParentVM().displayPostProfilePictures : this.getParentVM().parentVM.displayCommentProfilePictures
	},
	userProfilePicture$: function() {
		return this.csrftoken !== '' ? Ext.String.format('/resolve/service/user/downloadProfile?username={0}&{1}', this.commentAuthorUsername || this.authorUsername, this.csrftoken) : '';
	},
	collapsed: false,
	collapseTitle$: function() {
		var count = this.commentsList.length
		return this.collapsed ? this.localize(count == 1 ? 'expandComment' : 'expandComments', [count]) : this.localize(count == 1 ? 'collapseComment' : 'collapseComments', [count])
	},
	collapseCls$: function() {
		return this.collapsed ? 'icon-caret-down' : 'icon-caret-up'
	},
	displayComments$: function() {
		return this.commentsList.length > 0 && (this.viewMode == 'forum' || !this.isForum)
	},
	displayContent$: function() {
		return !this.isForum || this.viewMode == 'forum'
	},
	toggleCollapseComments: function() {
		this.set('collapsed', !this.collapsed)
	},

	toggleStarred: function() {
		this.set('starred', !this.starred)
		this.syncMainPostProperties()
		this.ajax({
			url: '/resolve/service/social/setStarred',
			params: {
				id: this.id,
				parentId: this.getParentVM().id,
				starred: this.starred
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (!response.success) {
					this.set('starred', !this.starred)
					this.syncMainPostProperties()
					clientVM.displayError(response.message)
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	toggleLike: function() {
		this.set('liked', !this.liked)
		this.set('numberOfLikes', Number(this.numberOfLikes) + (this.liked ? 1 : -1))
		this.syncMainPostProperties()
		this.ajax({
			url: '/resolve/service/social/setLike',
			params: {
				id: this.id,
				parentId: this.getParentVM().id,
				like: this.liked
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (!response.success) {
					this.set('like', !this.like)
					this.set('numberOfLikes', this.numberOfLikes + (this.liked ? 1 : -1))
					this.syncMainPostProperties()
					clientVM.displayError(response.message)
				} else {
					//set the Post to be 'read' if user Likes it.
					this.set('read', true)
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	likeCls$: function() {
		return 'post-hand icon-large ' + (this.liked ? 'icon-thumbs-up post-like-hand' : 'icon-thumbs-up-alt post-like-hand')
	},

	displayTargets$: function() {
		return this.targetList.length > 1 && !this.getRootVM().miniMode && this.type != 'system' && this.type != 'manual'
	},

	authorTooltip$: function() {
		if (this.getRootVM().miniMode && this.targetList.length > 0) {
			var list = [];
			this.targetList.foreach(function(target) {
				list.push(target.name)
			})
			return (list.length == 1 ? this.localize('target') : this.localize('targets')) + ' ' + list.join(', ')
		}
		return ''
	},

	dateText$: function() {
		var now = this.now,
			date = this.sysCreatedOn,
			diff;

		if (!Ext.isDate(date)) date = Ext.Date.parse(date, 'time')

		diff = (now - date) / 1000

		if (diff < 60) //1 minute
			return Ext.String.format('{0} {1} {2}', Math.max(Math.round(diff), 0), this.localize('seconds'), this.localize('ago'))
		if (60 < diff && diff < 60 * 2) // > 1 minute but < 2 minutes
			return Ext.String.format('{0} {1} {2}', 1, this.localize('minute'), this.localize('ago'))
		if (60 * 2 < diff && diff < 60 * 60) // > 2 minutes but < 60 minutes
			return Ext.String.format('{0} {1} {2}', Math.round(diff / 60), this.localize('minutes'), this.localize('ago'))
		if (60 * 60 < diff && diff < 2 * 60 * 60) // > 1 hour but < 2 hours
			return Ext.String.format('{0} {1} {2}', 1, this.localize('hour'), this.localize('ago'))
		if (2 * 60 * 60 < diff && diff < 24 * 60 * 60) // > 2 hours but < 1 day
			return Ext.String.format('{0} {1} {2}', Math.round(diff / (60 * 60)), this.localize('hours'), this.localize('ago'))
		if (24 * 60 * 60 < diff && diff < 2 * 24 * 60 * 60) // > 24 hours but < 48 hours
			return Ext.String.format('{0}', this.localize('yesterday'))
		if (2 * 24 * 60 * 60 < diff && diff < 7 * 24 * 60 * 60)
			return Ext.String.format('{0} {1} {2}', Math.round(diff / (24 * 60 * 60)), this.localize('days'), this.localize('ago'))

		return Ext.Date.format(date, clientVM.userDefaultDateFormat || Ext.state.Manager.get('userDefaultDateFormat', 'c'))
	},

	likesDisplayed: false,
	likesModel: null,
	toggleLikesDisplay: function(tbtext) {
		if (!this.getRootVM().miniMode) {
			if (this.likesDisplayed)
				this.likesModel.doClose()
			else {
				if (this.numberOfLikes > 0) {
					this.likesModel = this.open({
						mtype: 'RS.social.Likes',
						target: tbtext
					})
					this.set('likesDisplayed', !this.likesDisplayed)
				}
			}
		}
	},
	closeLikes: function() {
		if (!this.getRootVM().miniMode) {
			if (this.likesDisplayed) {
				this.likesModel.doClose()
				this.set('likesDisplayed', false)
			}

			if (this.authorDisplayed) {
				this.authorModel.doClose()
				this.set('authorDisplayed', false)
			}
		}
	},

	authorDisplayed: false,
	authorModel: null,
	toggleAuthorDisplay: function(tbtext) {
		if (!this.getRootVM().miniMode) {
			if (this.authorDisplayed)
				this.authorModel.doClose()
			else {
				this.closeComponent()
				this.authorModel = this.open({
					mtype: 'RS.social.Component',
					type: 'user',
					name: this.commentAuthorDisplayName || this.authorDisplayName,
					username: this.commentAuthorUsername || this.authorUsername,
					componentTarget: {
						sysId: this.authorSysId,
						type: 'user'
					},
					target: tbtext,
					currentUser: (this.commentAuthorUsername || this.authorUsername) == this.username,
					following: this.following
				})
			}
			this.set('authorDisplayed', !this.authorDisplayed)
		}
	},
	closeAuthor: function() {
		if (this.authorDisplayed) {
			this.authorModel.doClose()
			this.set('authorDisplayed', false)
		}
	},

	componentDisplayed: false,
	componentModel: null,
	toggleComponentDisplay: function(tbtext) {
		if (!this.getRootVM().miniMode) {
			if (this.componentDisplayed && this.componentModel)
				this.componentModel.doClose()
			else {
				this.closeAuthor()
				if (this.targetList.length > 0) {
					var following = false,
						currentUser = false;
					if (this.isPost) {
						var id = this.targetList.getAt(0).sysId;
						this.getParentVM().streamStore.each(function(stream) {
							if (stream.get('sys_id') == id) {
								if (stream.get('username') == this.username) currentUser = true
								following = true
								return false
							}
						}, this)
					}
					this.componentModel = this.open({
						mtype: 'RS.social.Component',
						type: this.componentType,
						following: following,
						currentUser: currentUser,
						name: this.targetList.getAt(0).name,
						username: this.targetList.getAt(0).name,
						componentTarget: this.targetList.getAt(0),
						target: tbtext
					})
					this.set('componentDisplayed', !this.componentDisplayed)
				}
			}
		}
	},
	closeComponent: function() {
		if (this.componentDisplayed) {
			this.componentModel.doClose()
			this.set('componentDisplayed', false)
		}
	},

	closePopups: function() {
		this.closeLikes()
		this.closeAuthor()
		this.closeComponent()

		this.commentsList.forEach(function(c) {
			c.closePopups()
		})
	},

	sharePost: function() {
		this.getParentVM().switchToPostMessage()
		this.getParentVM().set('content', Ext.String.format('{2} <a href="#RS.social.Main/postId={0}" target="_blank">{1}</a>', this.id, this.subject || Ext.util.Format.htmlEncode(Ext.String.format('<a href="#RS.social.Main/postId={0}" target="_blank">{1}</a>', this.id, this.authorUsername)), this.localize('postFrom')))
		this.getParentVM().set('to', [])
		this.getParentVM().set('toHidden', [])
	},

	showShareLink$: function() {
		return this.isPost && !this.getRootVM().miniMode
	},

	movePost: function() {
		this.open({
			mtype: 'RS.social.MovePost'
		})
	},
	deletePost: function() {
		this.message({
			title: this.localize('confirmPostDeleteTitle'),
			msg: this.localize('confirmPostDeleteBody'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deletePost'),
				no: this.localize('cancel')
			},
			fn: this.confirmDeletePost,
			scope: this
		})
	},
	confirmDeletePost: function(btn) {
		if (btn == 'yes') {
			this.ajax({
				url: '/resolve/service/social/delete',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						clientVM.displaySuccess(this.localize('postDeleted', [this.id]))
						this.getParentVM().removePost(this)
					} else clientVM.displayError(response.message)
				}
			})
		}
	},
	deleteComment: function() {
		this.message({
			title: this.localize('confirmCommentDeleteTitle'),
			msg: this.localize('confirmCommentDeleteBody'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteComment'),
				no: this.localize('cancel')
			},
			fn: this.confirmDeleteComment,
			scope: this
		})
	},
	confirmDeleteComment: function(btn) {
		if (btn == 'yes') {
			this.ajax({
				url: '/resolve/service/social/deleteComment',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						clientVM.displaySuccess(this.localize('commentDeleted', [this.id]))
						this.getParentVM().removePost(this)
					} else clientVM.displayError(response.message)
				}
			})
		}
	},
	toggleReadPost: function() {
		this.set('read', !this.read)
		this.syncMainPostProperties()
		this.ajax({
			url: '/resolve/service/social/setRead',
			params: {
				id: this.id,
				parentId: this.getParentVM().id,
				read: this.read
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (!response.success) {
					this.set('read', !this.read)
					this.syncMainPostProperties()
					clientVM.displayError(response.message)
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	removePost: function(post) {
		this.commentsList.remove(post)
	},

	toggleLockPost: function() {
		this.set('locked', !this.shouldBeLocked)
		this.syncMainPostProperties()
		this.ajax({
			url: '/resolve/service/social/setLockPost',
			params: {
				id: this.id,
				parentId: this.getParentVM().id,
				lock: this.shouldBeLocked
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) this.getRootVM().refreshPosts()
				else {
					this.set('locked', !this.shouldBeLocked)
					this.syncMainPostProperties()
					clientVM.displayError(response.message)
				}
			}
		})
	},

	showMove$: function() {
		return !this.getRootVM().miniMode && this.hasDeleteRights && this.type != 'rss' && this.type != 'system' && this.getParentVM().streamStore && this.getParentVM().streamStore.getCount() > 0;
	},

	showLocked$: function() {
		return true //this.type != 'system'
	},

	currentUser$: function() {
		return this.authorUsername == this.username
	},

	following: false,
	toggleFollowAuthor: function() {
		this.ajax({
			url: '/resolve/service/user/getUser',
			params: {
				username: this.authorUsername
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.ajax({
						url: '/resolve/service/social/setFollowStreams',
						params: {
							ids: response.data.id,
							follow: !this.following
						},
						success: function(r) {
							var response = RS.common.parsePayload(r)
							if (response.success) {
								this.set('following', !this.following)
								this.getParentVM().streamStore.load()
							} else clientVM.displayError(response.message)
						},
						failure: function(r) {
							clientVM.displayFailure(r);
						}
					})
				}
			}
		})
	},

	readCls$: function() {
		return 'icon-large post-read ' + (this.read ? 'icon-ok' : 'icon-envelope')
	},
	toggleRead: function() {
		this.set('read', !this.read)
		this.syncMainPostProperties()
		this.ajax({
			url: '/resolve/service/social/setRead',
			params: {
				id: this.id,
				parentId: this.getParentVM().id,
				read: this.read
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (!response.success) {
					this.set('read', !this.read)
					this.syncMainPostProperties()
					clientVM.displayError(response.message)
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	likeTooltip$: function() {
		return this.liked ? this.localize('unlikeTooltip') : this.localize('likeTooltip')
	},
	starredTooltip$: function() {
		return this.starred ? this.localize('unstarredTooltip') : this.localize('starredTooltip')
	},
	readTooltip$: function() {
		return this.read ? this.localize('unreadTooltip') : this.localize('readTooltip')
	},

	updatePost: function(post) {
		var realPost = post.raw ? post.raw : post,
			comments = realPost.comments || [],
			contains = false;

		this.set('liked', realPost.liked)
		this.set('read', realPost.read)
		this.set('starred', realPost.starred)
		this.set('numberOfLikes', realPost.numberOfLikes)
		this.set('answer', realPost.answer)

		this.set('targets', realPost.targets)
		this.targetList.removeAll()
		Ext.Array.forEach(this.targets || [], function(target) {
			if (target.sysId == this.authorSysId) target.name = this.localize('blog')
			if (this.getParentVM().activeStream && this.getParentVM().activeStream.id == 'system') target.name = this.localize('system')
			this.targetList.add(this.model(Ext.apply({
				mtype: 'viewmodel'
			}, target)))
			if (target.type && target.type.toLowerCase() == 'forum') this.set('componentType', 'forum')
		}, this)

		if (this.targetList.length > 0) {
			//determine best target to display
			var set = false
			this.targetList.forEach(function(target) {
				if (this.getParentVM().activeStream && target.name == this.getParentVM().activeStream.name) {
					this.set('component', target.type == 'USER' ? target.displayName : target.name)
					set = true
				}
			}, this)
			if (!set) this.set('component', this.targetList.getAt(0).type == 'USER' ? this.targetList.getAt(0).displayName : this.targetList.getAt(0).name)
		} else this.set('component', this.localize('blog'))

		Ext.Array.forEach(comments, function(comment) {
			contains = false
			this.commentsList.forEach(function(c) {
				if (c.id == comment.id) {
					contains = true
					c.updatePost(comment)
					return false
				}
			})

			if (!contains) {
				this.commentsList.add(this.model(Ext.apply({
					mtype: 'RS.social.Post'
				}, comment)))
				if (comment.answer) {
					this.set('answered', true)
					this.answersList.add(this.model(Ext.apply({
						mtype: 'RS.social.Post'
					}, comment)))
				}

			}
		}, this)

		var answered = false;
		this.commentsList.forEach(function(comment) {
			if (comment.answer) answered = true
		})
		this.set('answered', answered)
		this.determineFollowing()
	},

	determineFollowing: function() {
		if (!this.getParentVM().streamStore)
			return;
		var following = false
		if (this.isPost)
			this.getParentVM().streamStore.each(function(stream) {
				if (stream.get('username') == this.authorUsername) {
					following = true
					return false
				}
			}, this)
		this.set('following', following)
	},

	syncMainPostProperties: function() {
		if (this.getParentVM().posts)
			this.getParentVM().posts.forEach(function(post) {
				if (post.id == this.id)
					post.updatePost(this)
			}, this)
	},

	syncPostProperties: function(post) {
		this.set('id', post.id)
		this.set('authorUsername', post.authorUsername)
		this.set('username', post.username)
		this.set('title', post.title)
		this.set('content', post.content)
		this.set('starred', post.starred)
		this.set('numberOfLikes', post.numberOfLikes)
		this.set('liked', post.liked)
		this.set('sysCreatedOn', post.sysCreatedOn)
		this.set('componentType', post.componentType)

		this.targetList.removeAll()
		post.targetList.forEach(function(target) {
			this.targetList.add(target)
		}, this)

		if (this.targetList.length > 0) this.set('component', this.targetList.getAt(0).name)
		else this.set('component', this.localize('blog'))

		var answered = false;
		this.commentsList.removeAll()
		post.commentsList.forEach(function(comment) {
			this.commentsList.add(comment)
			if (comment.answer) answered = true
		}, this)
		this.set('answered', answered)

		this.answersList.removeAll()
		post.answersList.forEach(function(comment) {
			this.answersList.add(comment)
		}, this)
	},

	/*FORUM DISPLAY*/

	isForum$: function() {
		return this.getParentVM().activeStream && this.getParentVM().activeStream.type && this.getParentVM().activeStream.type.toLowerCase() == 'forum' //componentType == 'forum' //&& this.type != 'system'
	},

	showAnswer$: function() {
		return this.isForum
	},

	answered: false,

	answerText$: function() {
		return this.answered ? this.localize('answered') : this.localize('unanswered')
	},

	answerCls$: function() {
		return this.answered ? 'post-forum-answered' : 'post-forum-unanswered'
	},

	subjectClicked: function() {
		if (this.isForum && this.viewMode != 'forum')
			this.getRootVM().switchToForumDisplay(this)
		else
			this.getRootVM().switchToPostDisplay()

		if (this.componentType == 'rss' && this.uri)
			window.open(this.uri, '_blank')
	},

	postForumSubjectCls$: function() {
		return this.isForum || this.componentType == 'rss' ? 'post-forum-subject' : ''
	},

	isForumComment$: function() {
		return !this.isPost && this.getParentVM().isForum && this.getParentVM().currentUser
	},

	answerTextComment$: function() {
		return this.answer ? this.localize('answer') : this.localize('isAnswer')
	},

	commentAnswerCls$: function() {
		return this.answer ? 'post-forum-answered' : ''
	},

	calculateAnswer: function() {
		this.set('answered', false)
		this.answersList.removeAll()
		this.commentsList.forEach(function(comment) {
			if (comment.answer) {
				this.set('answered', true)
				this.answersList.add(comment)
			}
		}, this)
		if (this.getParentVM().activePost && this.getParentVM().activePost.id == this.id) this.getParentVM().activePost.syncPostProperties(this)
	},

	toggleAnswer: function() {
		this.set('answer', !this.answer)
		this.getParentVM().calculateAnswer()
		this.ajax({
			url: '/resolve/service/social/setAnswer',
			params: {
				id: this.id,
				parentId: this.getParentVM().id,
				answer: this.answer
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (!response.success) {
					this.set('answer', !this.answer)
					clientVM.displayError(response.message)
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	answersList: {
		mtype: 'list'
	},
	displayAnswers$: function() {
		return this.viewMode == 'forum' && this.answered && this.answersList.length > 0
	},

	forumCountText$: function() {
		if (this.isForum) {
			return Ext.String.format('({0} {1})', this.commentsList.length, this.commentsList.length === 1 ? this.localize('response') : this.localize('responses'))
		}
		return ''
	},

	forumBorderCls$: function() {
		return this.isForum ? 'post-forum-border' : ''
	},

	navigateToProfile: function(username) {
		if (this.getParentVM().navigateToProfile) this.getParentVM().navigateToProfile(username)
		else if (this.getParentVM().parentVM.navigateToProfile) this.getParentVM().parentVM.navigateToProfile(username)
	},

	miniMode$: function() {
		return this.getParentVM().miniMode
	}
})