glu.defModel('RS.social.Stream', {
	id: '',
	name: '',
	displayName: '',
	type: '',
	unReadCount: 0,
	pinned: false,
	isMe: false,
	sysUpdatedOn: new Date(),
	lock: false,
	following: true,

	showDigest: true,
	showEmail: true,

	showTool: false,
	selected: false,
	when_selected_changes_update_root: {
		on: ['selectedChanged'],
		action: function() {
			if (this.selected) this.parentVM.parentVM.set('activeStream', this)
		}
	},
	allowUnfollow$: function() {
		return this.following
	},
	displayTool$: function() {
		return this.showTool || this.selected
	},

	init: function() {
		if (this.parentVM.parentVM.unreadRecords && this.parentVM.parentVM.unreadRecords[this.id])
			this.set('unReadCount', this.parentVM.parentVM.unreadRecords[this.id])

		if (this.componentNotification) {
			Ext.Array.forEach(this.componentNotification.notifyTypes, function(notifyType) {
				if (notifyType.type.indexOf('DIGEST_EMAIL') > -1) this.set('sendDigest', notifyType.value == 'true' || notifyType.value == 'True')
				if (notifyType.type.indexOf('FORWARD_EMAIL') > -1) this.set('sendEmail', notifyType.value == 'true' || notifyType.value == 'True')
			}, this)
		}
		//if (this.sysUpdatedOn > Ext.Date.add(new Date(), Ext.Date.DAY, -14) || this.sysCreatedOn > Ext.Date.add(new Date(), Ext.Date.DAY, -14)) //within the last 2 weeks)
			this.set('initialShow', true)
	},
	getNotification: function(nextAction) {
		if (this.type)
			this.ajax({
				url: '/resolve/service/social/specific/notify/get',
				params: {
					streamId: this.sys_id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						if (response.data) {
							Ext.Array.forEach(response.data.notifyTypes || [], function(notifyType) {
								if (notifyType.type.indexOf(this.type == 'system' ? 'USER_SYSTEM' : this.type) == 0) {
									if (this.type == 'USER') {
										if (notifyType.type.indexOf('USER_ALL') == -1) {
											if (notifyType.type.indexOf('DIGEST_EMAIL') > -1) this.set('sendDigest', notifyType.value == 'true' || notifyType.value == 'True')
											if (notifyType.type.indexOf('FORWARD_EMAIL') > -1) this.set('sendEmail', notifyType.value == 'true' || notifyType.value == 'True')
										}
									} else {
										if (notifyType.type.indexOf('DIGEST_EMAIL') > -1) this.set('sendDigest', notifyType.value == 'true' || notifyType.value == 'True')
										if (notifyType.type.indexOf('FORWARD_EMAIL') > -1) this.set('sendEmail', notifyType.value == 'true' || notifyType.value == 'True')
									}
								}
							}, this)
						}
					} else {
						clientVM.displayError(response.message);
					}

					nextAction();
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		else
			nextAction();
	},
	initialShow: false,
	selectedCss$: function() {
		return this.selected ? 'selected' : 'unselected'
	},
	displayStream$: function() {
		return this.parentVM.showAll || this.pinned || this.isMe || this.initialShow
	},

	displayView$: function() {
		return !this.isMe && this.type && Ext.Array.indexOf(['process', 'team', 'forum', 'rss'], this.type.toLowerCase()) == -1 && !this.parentVM.parentVM.miniMode
	},

	displaySettings$: function() {
		return !this.isMe && this.type && Ext.Array.indexOf(['process', 'team', 'forum', 'rss'], this.type.toLowerCase()) > -1 && !this.parentVM.parentVM.miniMode
	},

	display$: function() {
		var display = this.unReadCount > 0 ? '<b>{0}</b>' : '{0}'
		if (this.type.toLowerCase() == 'user' && this.id != 'inbox') {
			return Ext.String.format(display, Ext.util.Format.ellipsis(this.displayName, this.parentVM.parentVM.miniMode ? 42 : 28, false) + (this.unReadCount > 0 ? ' (' + this.unReadCount + ')' : ''))
		} else {
			return Ext.String.format(display, Ext.util.Format.ellipsis(this.name, this.parentVM.parentVM.miniMode ? 42 : 28, false) + (this.unReadCount > 0 ? ' (' + this.unReadCount + ')' : ''))
		}
	},

	titleDisplay$: function() {
		var display = this.unReadCount > 0 && !this.parentVM.parentVM.miniMode ? '<b>{0}</b>' : '{0}'
		if (this.type.toLowerCase() == 'user' && this.id != 'inbox') {
			return Ext.String.format(display, Ext.util.Format.ellipsis(this.displayName, 15, false) + (this.unReadCount > 0 ? ' (' + this.unReadCount + ')' : ''))
		} else {
			return Ext.String.format(display, Ext.util.Format.ellipsis(this.name, 15, false) + (this.unReadCount > 0 ? ' (' + this.unReadCount + ')' : ''))
		}
	},

	titleFullDisplay$: function() {
		var display = this.unReadCount > 0 ? '<b>{0}</b>' : '{0}'
		if (this.type.toLowerCase() == 'user' && this.id != 'inbox') {
			return Ext.String.format(display, this.displayName + (this.unReadCount > 0 ? ' (' + this.unReadCount + ')' : ''))
		} else {
			return Ext.String.format(display, this.name + (this.unReadCount > 0 ? ' (' + this.unReadCount + ')' : ''))
		}

	},

	isSystem$: function() {
		return this.id == 'system'
	},

	configureNotifications: function() {
		this.open({
			mtype: 'RS.social.ConfigureNotifications'
		})
	},

	viewStream: function() {
		var modelName = RS.social.ComponentMap[this.type.toLowerCase()]
		if (modelName) {
			if (modelName == 'RS.wiki.WikiAdmin') {
				clientVM.handleNavigation({
					modelName: modelName,
					params: {
						ufullname: 'startsWith~' + this.sys_id,
						social: true
					}
				})
			} else if (modelName == 'RS.user.User') {
				clientVM.handleNavigation({
					modelName: modelName,
					params: {
						username: this.username
					}
				})
			} else
				clientVM.handleNavigation({
					modelName: modelName,
					params: {
						id: this.sys_id,
						social: true
					}
				})
		} else
			this.message({
				title: this.localize('unknownComponentTitle'),
				msg: this.localize('unknownComponentMessage', [this.type.toLowerCase()]),
				buttons: Ext.MessageBox.OK
			})
	},
	configureStream: function() {
		var streamTypes = {
			rss: 'rss',
			team: 'teams',
			process: 'process',
			forum: 'forums'
		};
		clientVM.handleNavigation({
			modelName: 'RS.socialadmin.Component',
			params: {
				id: this.id,
				name: streamTypes[this.type.toLowerCase()]
			}
		})
	},
	togglePinned: function() {
		this.ajax({
			url: '/resolve/service/social/setPinned',
			params: {
				id: this.id,
				pinned: !this.pinned
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success)
					this.set('pinned', !this.pinned)
				else
					clientVM.displayError(response.message)
			}
		})
	},
	toggleLocked: function() {
		this.ajax({
			url: '/resolve/service/social/setLockStream',
			params: {
				id: this.id,
				lock: !this.lock
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.set('lock', !this.lock)
					this.parentVM.parentVM.fireEvent('activeStreamChanged')
				} else
					clientVM.displayError(response.message)
			}
		})
	},
	unfollowStream: function() {
		this.ajax({
			url: '/resolve/service/social/setFollowStreams',
			params: {
				ids: [this.id],
				follow: false
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				this.parentVM.parentVM.selectFirstStream()
				this.parentVM.parentVM.refreshStreams()
				if (!response.success) {
					clientVM.displayError(response.message);
				} else {
					this.parentVM.parentVM.updatePagination(this.type);
				}
			}
		})
	},
	markStreamRead: function(date) {
		this.ajax({
			url: '/resolve/service/social/markStreamRead',
			params: {
				id: this.id,
				streamType: this.type,
				olderThanDate: Ext.Date.format(date || new Date(), 'c')
			},
			success: function(r) {
				var response = RS.common.parsePayload(r);
				this.parentVM.parentVM.updateScreen();
				if (response.success) clientVM.displaySuccess(this.localize('markedStreamRead'))
				else clientVM.displayError(response.message)
			}
		})
	},

	updateStream: function(stream) {
		Ext.Object.each(stream, function(key) {
			this.set(key, stream[key])
		}, this)

		if (this.componentNotification) {
			Ext.Array.forEach(this.componentNotification.notifyTypes, function(notifyType) {
				if (notifyType.type.indexOf('DIGEST_EMAIL') > -1) this.set('sendDigest', notifyType.value == 'true' || notifyType.value == 'True')
				if (notifyType.type.indexOf('FORWARD_EMAIL') > -1) this.set('sendEmail', notifyType.value == 'true' || notifyType.value == 'True')
			}, this)
		}
	},

	mouseOver: function() {
		this.set('showTool', true)
	},
	mouseOut: function() {
		this.set('showTool', false)
	},
	select: function() {
		this.parentVM.selectStream(this)
	},
	doubleClick: function() {
		if (this.displayView) {
			this.viewStream()
		}
	},
	popout: function() {
		var params = Ext.Object.fromQueryString(window.location.search),
			debugParam = '';
		if (params.debug || params.debugSocial) debugParam = '&debug=true'
		var child = window.open(Ext.String.format('/resolve/social/social.jsp?v=m&streamId={0}{1}', this.id, debugParam), '_blank', 'height=325,width=400,menubar=no,status=no,toolbar=no');
		// clientVM.childWindows.push(child)
	},
	showPopout$: function() {
		return false;
		// return !this.parentVM.parentVM.miniMode
	},

	sendDigest: false,
	digestChanged: function(checked) {
		if (checked != this.sendDigest) {
			this.ajax({
				url: '/resolve/service/social/specific/notify/updateType',
				params: {
					streamIds: [this.sys_id],
					selectedType: Ext.String.format('{0}_{1}_{2}', this.type == 'system' ? 'USER_SYSTEM' : this.type, 'DIGEST', 'EMAIL'),
					value: checked
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.set('sendDigest', checked)
						clientVM.displaySuccess(this.localize('notificationSettingChanged'))
					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},
	sendEmail: false,
	emailForwardChanged: function(checked) {
		if (checked != this.sendEmail) {
			this.ajax({
				url: '/resolve/service/social/specific/notify/updateType',
				params: {
					streamIds: [this.sys_id],
					selectedType: Ext.String.format('{0}_{1}_{2}', this.type == 'system' ? 'USER_SYSTEM' : this.type, 'FORWARD', 'EMAIL'),
					value: checked
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.set('sendEmail', checked)
						clientVM.displaySuccess(this.localize('notificationSettingChanged'))
					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},
	supportPaging: function() {
		return (this.type === 'DOCUMENT' || this.type === 'ACTIONTASK');
	}
})
