glu.defModel('RS.social.Component', {
	target: null,
	type: '',
	following: false,
	name: '',
	username: '',
	componentId: '',
	currentUser: false,

	isAuthor$: function() {
		return this.type.toLowerCase() == 'user'
	},

	description: '',
	uemail: '',
	uhomePhone: '',
	umobilePhone: '',
	csrftoken: '',
	fields: ['description', 'uemail', 'uhomePhone', 'umobilePhone'],

	viewText$: function() {
		return '<i class="icon-large icon-play stream-menu-icon" style="text-decoration:none"></i> ' + this.localize('view')
	},

	init: function() {
		this.initToken();
		if (this.isAuthor) {
			this.ajax({
				url: '/resolve/service/user/getUser',
				params: {
					username: this.name
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.loadData(response.data || {})
						this.set('componentDescription', this.componentDescription$())
					}
				}
			})
		} else {

		}
	},

	activate: function() {
		this.initToken();
	},

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/user/downloadProfile', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this));
	},

	close: function() {
		if (this.parentVM.componentDisplayed) this.parentVM.set('componentDisplayed', false)
		if (this.parentVM.authorDisplayed) this.parentVM.set('authorDisplayed', false)
		this.doClose()
	},

	followText$: function() {
		return Ext.String.format('<span class="{0} like-follow-text" style="background:transparent !important;"></span><span class="like-follow-text rs-link" style="background:transparent !important;">{1}</span>', this.followIcon, this.following ? this.localize('unfollow') : this.localize('follow'))
	},
	followIcon$: function() {
		return this.following ? 'icon-large icon-reply like-following' : 'icon-large icon-plus like-follow'
	},
	toggleFollowing: function() {
		this.set('following', !this.following)
		this.ajax({
			url: '/resolve/service/social/setFollowStreams',
			params: {
				ids: [this.componentTarget.sysId],
				follow: this.following
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) this.parentVM.parentVM.refreshStreams()
				else clientVM.displayError(response.message)
			}
		})
	},

	componentDescription$: function() {
		if (this.isAuthor) {
			var description = Ext.String.format('<font cls="rs-menu-icon">{0}</font>', this.description || ''),
			image = Ext.String.format('<img style="width:46px;height:46px" src="/resolve/service/user/downloadProfile?username={0}&{1}"/>', this.username, this.csrftoken),
			email = Ext.String.format('{0}: {1}', this.localize('email'), this.uemail || ''),
			phone = Ext.String.format('{0}: {1}', this.localize('phone'), this.uhomePhone || ''),
			mobile = Ext.String.format('{0}: {1}', this.localize('mobile'), this.umobilePhone || '');
			return Ext.String.format('<table><tr><td colspan=2>{0}</td></tr><tr><td>{1}</td><td style="padding-left:10px">{2}</td></tr></table>', description, image, [email, phone, mobile].join('<br/>'))
		} else {
			this.getComponentDescription()
		}
	},

	getComponentDescription: function() {
		var name = Ext.String.format('{0}: {1}', this.localize('name'), this.componentTarget.name || ''),
			type = Ext.String.format('{0}: {1}', this.localize('type'), this.localize(this.componentTarget.type.toLowerCase()) || ''),
			description = Ext.String.format('{0}: {1}', this.localize('description'), this.componentTarget.description || '');
		return Ext.String.format('<table><tr><td colspan=2>{0}</td></tr><tr><td>{1}</td></tr><tr><td>{2}</td></tr></table>', name, type, description)
	},

	view: function() {
		var modelName = RS.social.ComponentMap[(this.componentTarget.type || 'user').toLowerCase()]
		if (modelName) {
			if (modelName == 'RS.wiki.WikiAdmin') {
				clientVM.handleNavigation({
					modelName: modelName,
					params: {
						ufullname: 'startsWith~' + this.sys_id,
						social: true
					}
				})
			} else if (modelName == 'RS.user.User') {
				clientVM.handleNavigation({
					modelName: modelName,
					params: {
						username: this.username
					}
				})
			} else
				clientVM.handleNavigation({
					modelName: modelName,
					params: {
						id: this.componentTarget.sysId,
						social: true
					}
				})
		} else
			this.message({
				title: this.localize('unknownComponentTitle'),
				msg: this.localize('unknownComponentMessage', [this.type.toLowerCase()]),
				buttons: Ext.MessageBox.OK
			});
		this.doClose();
	}
})