/* Deprecated feature
glu.defModel('RS.social.CreateLink', {

	editor: null,

	link: 'url',
	display: '',

	height$: function() {
		return this.link == 'component' ? 500 : 225
	},

	when_input_changes_update_display: {
		on: ['urlChanged'],
		action: function(value) {
			if (Ext.String.startsWith(value, this.display, true)) this.set('display', value)
		}
	},

	url: 'http://',
	urlIsVisible$: function() {
		return this.link == 'url'
	},

	searchQuery: '',
	search: function() {
		this.toGrid.load()
	},
	type: '',
	typeStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/social/listComponentTypes',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	toGridIsVisible$: function() {
		return this.link == 'component'
	},
	toGrid: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['name'],
		fields: ['id', 'name', 'type', 'username'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/social/listAllStreams',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	when_filter_changes_reload_grid: {
		on: ['typeChanged', 'searchQueryChanged'],
		action: function() {
			this.toGrid.load()
		}
	},

	toGridSelections: [],

	lastSelections: [],
	when_selection_changes_update_text: {
		on: ['toGridSelectionsChanged'],
		action: function() {
			if (!this.display || this.lastSelections.length > 0 && (this.display == this.lastSelections[0].get('name'))) this.set('display', this.toGridSelections[0].get('name'))
			this.set('lastSelections', this.toGridSelections)
		}
	},

	toGridColumns: [{
		dataIndex: 'name',
		header: '~~name~~',
		flex: 1
	}, {
		dataIndex: 'type',
		header: '~~type~~'
	}],

	componentTypes: {
		mtype: 'list',
		autoParent: true
	},

	init: function() {
		this.componentTypes.add({
			mtype: 'RS.social.ComponentType',
			name: this.localize('url'),
			inputValue: 'url',
			value: true
		})
		this.componentTypes.add({
			mtype: 'RS.social.ComponentType',
			name: this.localize('component'),
			inputValue: 'component'
		})

		this.toGrid.on('beforeload', function(store, operation, eOpts) {
			var filters = [];

			if (this.type) filters.push({
				field: 'type',
				condition: 'equals',
				value: this.type
			})

			if (this.search) filters.push({
				field: 'name',
				condition: 'startsWith',
				value: this.searchQuery
			})

			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				filter: Ext.encode(filters)
			})
		}, this)


		this.typeStore.load()

		if (!this.type) this.typeStore.on('load', function(store, records) {
			if (records.length > 0) this.set('type', records[0].get('value'))
		}, this, {
			single: true
		})
		else this.toGrid.load()
	},

	hrefLink$: function() {
		if (this.link == 'url') return this.url
		if (this.link == 'component' && this.toGridSelections.length > 0) {
			var type = this.toGridSelections[0].get('type').toLowerCase();
			return Ext.String.format('#{0}/{1}={2}', (RS.social.ComponentMap[type.toLowerCase()] || ''), type == 'user' ? 'username' : 'id', type == 'user' ? this.toGridSelections[0].get('username') : this.toGridSelections[0].get('id'))
		}
	},

	createLink: function() {
		this.editor.insertAtCursor(Ext.String.format('<a href="{0}" target="_blank">{1}</a>', this.hrefLink, this.display))
		this.doClose()
	},

	createLinkIsEnabled$: function() {
		return this.display && (this.link == 'component' ? this.toGridSelections.length == 1 : this.url)
	},

	cancel: function() {
		this.doClose()
	}

})

glu.defModel('RS.social.ComponentType', {
	name: '',
	inputValue: '',
	value: false,
	when_value_changes_update_parent: {
		on: ['valueChanged'],
		action: function() {
			if (this.value) this.parentVM.set('link', this.inputValue)
		}
	}
})
*/
