glu.defModel('RS.social.MovePost', {

	to: [],
	toIsValid$: function() {
		return this.to.length > 0 ? true : this.localize('invalidTo')
	},
	toHidden: [],

	streamToStore: {
		mtype: 'store',
		fields: ['id', 'name', 'type', 'displayName'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	init: function() {
		this.parentVM.parentVM.streamToStore.each(function(record) {
			this.streamToStore.add(record)
		}, this)
		var tos = []
		if (this.parentVM && this.parentVM.target) {
			Ext.Array.forEach(this.parentVM.target, function(target) {
				if (target.id) tos.push(target.id)
			})

			if (tos.length == 0) {
				var blogStreamIndex = this.rootVM.streamToStore.findExact('name', this.localize('blog'))
				if (blogStreamIndex > -1)
					tos.push(this.rootVM.streamToStore.getAt(blogStreamIndex))
			}
			this.set('to', tos)
		}
	},

	movePost: function() {
		this.ajax({
			url: '/resolve/service/social/move',
			params: {
				postId: this.parentVM.id,
				targets: this.to
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.rootVM.refreshPosts()
					this.doClose()
				} else
					clientVM.displayError(response.message)
			}
		})
	},

	movePostIsEnabled$: function() {
		return this.isValid
	},

	cancel: function() {
		this.doClose()
	}
})