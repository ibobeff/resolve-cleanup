glu.defModel('RS.social.Followers', {
	target: null,

	streamId: '',

	removeFollowers: true,
	addFollowers: false,
	buttonWindow: false,
	csrftoken: '',
	baseCls$: function() {
		return this.buttonWindow && !Ext.isIE8m ? 'x-panel' : 'x-window'
	},
	ui$: function() {
		return this.buttonWindow && !Ext.isIE8m ? 'sysinfo-dialog' : 'default'
	},
	draggable$: function() {
		return !this.buttonWindow
	},

	followers: {
		mtype: 'list',
		autoParent: true
	},

	containsMe: false,

	init: function() {
		this.initToken();
		this.loadFollowers()
	},

	activate: function() {
		this.initToken();
	},

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/user/downloadProfile', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this))
	},

	loadFollowers: function() {
		if (this.streamId || this.parentVM.id) {
			this.followers.removeAll();
			var followersModel = this;
			this.ajax({
				url: '/resolve/service/social/listFollowers',
				params: {
					streamId: this.streamId || this.parentVM.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.set('containsMe', false)
						Ext.Array.forEach(response.records || [], function(record) {
							if (record.currentUser || record.id == clientVM.user.id) this.set('containsMe', true)
							var me = this;
							this.followers.add(Ext.apply({
								mtype: 'viewmodel',
								followText$: function() {
									if (me.removeFollowers)
										return Ext.String.format('<span style="color:red" class="icon-large icon-remove like-follow-text"></span>&nbsp;<span class="like-follow-text">{0}</span>', this.parentVM.localize('remove'))
									return this.currentUser || this.id == clientVM.user.id ? this.parentVM.localize('you') : Ext.String.format('<span class="{0} like-follow-text"></span><span class="like-follow-text">{1}</span>', this.followIcon, this.following ? this.localize('unfollow') : this.localize('follow'))
								},
								followIcon$: function() {
									return this.following ? 'icon-large icon-reply like-following' : 'icon-large icon-plus like-follow'
								},
								followerProfileImage$: function() {
									return followersModel.csrftoken ? '/resolve/service/user/downloadProfile?username=' + this.username + '&' + followersModel.csrftoken : '';
								},
								toggleFollowing: function() {
									if (me.removeFollowers) {
										this.ajax({
											url: '/resolve/service/social/setFollowers',
											params: {
												streamIds: [this.parentVM.streamId || this.parentVM.parentVM.id],
												ids: [this.id],
												follow: false
											},
											success: function(r) {
												var response = RS.common.parsePayload(r)
												if (response.success) {
													this.parentVM.loadFollowers()
													if (this.rootVM.refreshStreams) this.rootVM.refreshStreams()
												} else clientVM.displayError(response.message)
											}
										})
									} else {
										this.set('following', !this.following)
										this.ajax({
											url: '/resolve/service/social/setFollowStreams',
											params: {
												ids: [this.id],
												follow: this.following
											},
											success: function(r) {
												var response = RS.common.parsePayload(r)
												if (response.success) {
													this.parentVM.loadFollowers()
													if (this.rootVM.refreshStreams) this.rootVM.refreshStreams()
												} else clientVM.displayError(response.message)
											}
										})
									}
								}
							}, record))
						}, this)

						if (this.followers.length == 0) {
							this.followers.add({
								mtype: 'viewmodel',
								name: this.localize('noFollowers'),
								followText$: function() {
									return ''
								},
								followIcon$: function() {
									return ''
								},
								followerProfileImage$: function() {
									return ''
								},
								toggleFollowing: function() {}
							})
						}
					} else clientVM.displayError(response.message)
				}
			})
		}
	},

	follow: function() {
		this.ajax({
			url: '/resolve/service/social/setFollowStreams',
			params: {
				ids: [this.streamId || this.parentVM.id],
				streamType: this.streamType || this.parentVM.streamType,
				follow: true
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				this.loadFollowers()
			}
		})
	},
	followIsVisible$: function() {
		return this.addFollowers && !this.containsMe
	},
	unfollow: function() {
		this.ajax({
			url: '/resolve/service/social/setFollowStreams',
			params: {
				ids: [this.streamId || this.parentVM.id],
				follow: false
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				this.loadFollowers()
			}
		})
	},
	unfollowIsVisible$: function() {
		return this.addFollowers && this.containsMe
	},
	addMoreFollowers: function() {
		var model = glu.model({
			mtype: 'RS.social.StreamPicker',
			pickerType: 2,
			streamId: this.streamId || this.parentVM.id,
			streamType: this.streamType || this.parentVM.streamType
		})
		model.init()
		glu.openWindow(model)
		this.doClose()
	},
	addMoreFollowersIsVisible$: function() {
		return this.addFollowers
	}
})