Ext.define('RS.social.UserStream', {
    extend: 'Ext.data.Model',
	fields: [
		'id', 'sys_id', 'name', 'type', 'username', 'componentNotification', 'displayName',
		{
			name: 'following',
			type: 'bool'
		}, {
			name: 'lock',
			type: 'bool'
		}, {
			name: 'currentUser',
			type: 'bool'
		}, {
			name: 'unReadCount',
			type: 'numeric'
		}, {
			name: 'isMe',
			type: 'bool'
		}, {
			name: 'pinned',
			type: 'bool'
		}, {
			name: 'sysUpdatedOn',
			type: 'date',
			format: 'time',
			dateFormat: 'time'
		}]
});