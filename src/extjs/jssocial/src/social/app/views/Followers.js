glu.defView('RS.social.Followers', {
	minHeight: 250,
	width: 600,
	modal : true,
	padding : 15,
	title: '~~followers~~',
	ui: '@{ui}',
	baseCls: '@{baseCls}',
	draggable: '@{draggable}',
	cls : 'rs-modal-popup',	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	autoScroll: true,
	items: '@{followers}',
	bodyPadding: '10px',
	itemTemplate: {
		layout: {
			type: 'hbox',
			align: 'middle'
		},
		padding: '0px 0px 10px 0px',
		items: [{
			xtype: 'image',
			width: 46,
			height: 46,
			margin: '0px 10px 0px 0px',
			src: '@{followerProfileImage}'
		}, {
			xtype: 'label',
			cls: 'like-username',
			text: '@{name}',
			flex: 1
		}, {
			html: '@{followText}',
			listeners: {
				render: function(item) {
					item.getEl().on('click', function() {
						item.fireEvent('toggleFollowing', item)
					})
				},
				toggleFollowing: '@{toggleFollowing}'
			}
		}]
	},	
	buttons: [/*'follow', 'unfollow', */
	{
		name :'addMoreFollowers',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		cls : 'rs-med-btn rs-btn-light',
		name : 'close'
	}]
})