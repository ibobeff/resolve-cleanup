/* Deprecated feature
glu.defView('RS.social.AttachFile', {

	asWindow: {
		width: 600,
		height: 400,
		title: '~~attachFileWindowTitle~~'
	},

	layout: 'fit',
	items: [{
		xtype: 'uploadmanager',
		allowUpload: true,
		resizable: false,
		fileServiceListUrl: '/resolve/service/social/attachmentList',
		fileServiceUploadUrl: '/resolve/service/social/upload',
		fileUploadTableName: 'SocialAttachment',
		selType: 'rowmodel',
		preventLoading: true,
		selModel: {
			mode: 'MULTI'
		},
		recordId: true,
		uploader: {
			url: '/resolve/service/social/upload',
			uploadpath: 'dev',
			autoStart: true,
			max_file_size: '2020mb',
			flash_swf_url: '/resolve/js/plupload/Moxie.swf',
			urlstream_upload: true
		},
		listeners: {
			selectionchange: '@{selectionChanged}',
			itemdblclick: '@{selectRecord}',
			uploadcomplete: function(uploader, files) {
				if (files.length > 0) this.getSelectionModel().select(this.getStore().getAt(this.getStore().find('sys_id', files[files.length - 1].sys_id)))
			}
		}
	}],

	buttonAlign: 'left',
	buttons: ['attach', 'cancel']
})
*/
