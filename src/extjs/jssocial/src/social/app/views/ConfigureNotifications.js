glu.defView('RS.social.ConfigureNotifications', {
	asWindow: {
		title: '~~configureNotificationsTitle~~',
		width: 500,
		height: 400,
		modal: true
	},
	buttonAlign: 'left',
	buttons: ['save', 'cancel'],
	bodyPadding: '10px',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'combo',
		name: 'type',
		store: '@{typeStore}',
		displayField: 'name',
		valueField: 'value',
		queryMode: 'local'
	}, {
		layout: 'card',
		activeItem: '@{activeItem}',
		flex: 1,
		items: [{
			title: '~~PROCESS~~',
			defaultType: 'checkbox',
			autoScroll: true,
			bodyPadding: '10px',
			defaults: {
				hideLabel: true
			},
			items: [{
					name: 'selectAllProcess',
					boxLabel: '~~selectAllProcess~~'
				}, {
					name: 'PROCESS_CREATE',
					boxLabel: '~~PROCESS_CREATE~~'
				}, {
					name: 'PROCESS_UPDATE',
					boxLabel: '~~PROCESS_UPDATE~~'
				}, {
					name: 'PROCESS_PURGED',
					boxLabel: '~~PROCESS_PURGED~~'
				}, {
					name: 'PROCESS_DOCUMENT_ADDED',
					boxLabel: '~~PROCESS_DOCUMENT_ADDED~~'
				}, {
					name: 'PROCESS_DOCUMENT_REMOVED',
					boxLabel: '~~PROCESS_DOCUMENT_REMOVED~~'
				}, {
					name: 'PROCESS_ACTIONTASK_ADDED',
					boxLabel: '~~PROCESS_ACTIONTASK_ADDED~~'
				}, {
					name: 'PROCESS_ACTIONTASK_REMOVED',
					boxLabel: '~~PROCESS_ACTIONTASK_REMOVED~~'
				}, {
					name: 'PROCESS_RSS_ADDED',
					boxLabel: '~~PROCESS_RSS_ADDED~~'
				}, {
					name: 'PROCESS_RSS_REMOVED',
					boxLabel: '~~PROCESS_RSS_REMOVED~~'
				}, {
					name: 'PROCESS_FORUM_ADDED',
					boxLabel: '~~PROCESS_FORUM_ADDED~~'
				}, {
					name: 'PROCESS_FORUM_REMOVED',
					boxLabel: '~~PROCESS_FORUM_REMOVED~~'
				}, {
					name: 'PROCESS_TEAM_ADDED',
					boxLabel: '~~PROCESS_TEAM_ADDED~~'
				}, {
					name: 'PROCESS_TEAM_REMOVED',
					boxLabel: '~~PROCESS_TEAM_REMOVED~~'
				}, {
					name: 'PROCESS_USER_ADDED',
					boxLabel: '~~PROCESS_USER_ADDED~~'
				}, {
					name: 'PROCESS_USER_REMOVED',
					boxLabel: '~~PROCESS_USER_REMOVED~~'
				}
				/*, {
				name: 'PROCESS_DIGEST_EMAIL',
				boxLabel: '~~PROCESS_DIGEST_EMAIL~~'
			}, {
				name: 'PROCESS_FORWARD_EMAIL',
				boxLabel: '~~PROCESS_FORWARD_EMAIL~~'
			}*/
			]
		}, {
			title: '~~TEAM~~',
			defaultType: 'checkbox',
			autoScroll: true,
			bodyPadding: '10px',
			defaults: {
				hideLabel: true
			},
			items: [{
					name: 'selectAllTeam',
					boxLabel: '~~selectAllTeam~~'
				}, {
					name: 'TEAM_CREATE',
					boxLabel: '~~TEAM_CREATE~~'
				}, {
					name: 'TEAM_UPDATE',
					boxLabel: '~~TEAM_UPDATE~~'
				}, {
					name: 'TEAM_PURGED',
					boxLabel: '~~TEAM_PURGED~~'
				}, {
					name: 'TEAM_ADDED_TO_PROCESS',
					boxLabel: '~~TEAM_ADDED_TO_PROCESS~~'
				}, {
					name: 'TEAM_REMOVED_FROM_PROCESS',
					boxLabel: '~~TEAM_REMOVED_FROM_PROCESS~~'
				}, {
					name: 'TEAM_ADDED_TO_TEAM',
					boxLabel: '~~TEAM_ADDED_TO_TEAM~~'
				}, {
					name: 'TEAM_REMOVED_FROM_TEAM',
					boxLabel: '~~TEAM_REMOVED_FROM_TEAM~~'
				}, {
					name: 'TEAM_USER_ADDED',
					boxLabel: '~~TEAM_USER_ADDED~~'
				}, {
					name: 'TEAM_USER_REMOVED',
					boxLabel: '~~TEAM_USER_REMOVED~~'
				}
				/*, {
				name: 'TEAM_DIGEST_EMAIL',
				boxLabel: '~~TEAM_DIGEST_EMAIL~~'
			}, {
				name: 'TEAM_FORWARD_EMAIL',
				boxLabel: '~~TEAM_FORWARD_EMAIL~~'
			}*/
			]
		}, {
			title: '~~FORUM~~',
			defaultType: 'checkbox',
			autoScroll: true,
			bodyPadding: '10px',
			defaults: {
				hideLabel: true
			},
			items: [{
					name: 'selectAllForum',
					boxLabel: '~~selectAllForum~~'
				}, {
					name: 'FORUM_CREATE',
					boxLabel: '~~FORUM_CREATE~~'
				}, {
					name: 'FORUM_UPDATE',
					boxLabel: '~~FORUM_UPDATE~~'
				}, {
					name: 'FORUM_PURGED',
					boxLabel: '~~FORUM_PURGED~~'
				}, {
					name: 'FORUM_ADDED_TO_PROCESS',
					boxLabel: '~~FORUM_ADDED_TO_PROCESS~~'
				}, {
					name: 'FORUM_REMOVED_FROM_PROCESS',
					boxLabel: '~~FORUM_REMOVED_FROM_PROCESS~~'
				}, {
					name: 'FORUM_USER_ADDED',
					boxLabel: '~~FORUM_USER_ADDED~~'
				}, {
					name: 'FORUM_USER_REMOVED',
					boxLabel: '~~FORUM_USER_REMOVED~~'
				}
				/*, {
				name: 'FORUM_DIGEST_EMAIL',
				boxLabel: '~~FORUM_DIGEST_EMAIL~~'
			}, {
				name: 'FORUM_FORWARD_EMAIL',
				boxLabel: '~~FORUM_FORWARD_EMAIL~~'
			}*/
			]
		}, {
			title: '~~USER~~',
			defaultType: 'checkbox',
			autoScroll: true,
			bodyPadding: '10px',
			defaults: {
				hideLabel: true
			},
			items: [{
					name: 'selectAllUser',
					boxLabel: '~~selectAllUser~~'
				}, {
					name: 'USER_ADDED_TO_PROCESS',
					boxLabel: '~~USER_ADDED_TO_PROCESS~~'
				}, {
					name: 'USER_ADDED_TO_TEAM',
					boxLabel: '~~USER_ADDED_TO_TEAM~~'
				}, {
					name: 'USER_FOLLOW_ME',
					boxLabel: '~~USER_FOLLOW_ME~~'
				}, {
					name: 'USER_REMOVED_FROM_PROCESS',
					boxLabel: '~~USER_REMOVED_FROM_PROCESS~~'
				}, {
					name: 'USER_REMOVED_FROM_TEAM',
					boxLabel: '~~USER_REMOVED_FROM_TEAM~~'
				}, {
					name: 'USER_UNFOLLOW_ME',
					boxLabel: '~~USER_UNFOLLOW_ME~~'
				}, {
					name: 'USER_ADDED_TO_FORUM',
					boxLabel: '~~USER_ADDED_TO_FORUM~~'
				}, {
					name: 'USER_REMOVED_FROM_FORUM',
					boxLabel: '~~USER_REMOVED_FROM_FORUM~~'
				}, {
					name: 'USER_PROFILE_CHANGE',
					boxLabel: '~~USER_PROFILE_CHANGE~~'
				}
				/*, {
				name: 'USER_DIGEST_EMAIL',
				boxLabel: '~~USER_DIGEST_EMAIL~~'
			}, {
				name: 'USER_FORWARD_EMAIL',
				boxLabel: '~~USER_FORWARD_EMAIL~~'
			}*/
			]
		}, {
			title: '~~ACTIONTASK~~',
			defaultType: 'checkbox',
			autoScroll: true,
			bodyPadding: '10px',
			defaults: {
				hideLabel: true
			},
			items: [{
					name: 'selectAllActionTask',
					boxLabel: '~~selectAllActionTask~~'
				}, {
					name: 'ACTIONTASK_CREATE',
					boxLabel: '~~ACTIONTASK_CREATE~~'
				}, {
					name: 'ACTIONTASK_UPDATE',
					boxLabel: '~~ACTIONTASK_UPDATE~~'
				}, {
					name: 'ACTIONTASK_PURGED',
					boxLabel: '~~ACTIONTASK_PURGED~~'
				}, {
					name: 'ACTIONTASK_ADDED_TO_PROCESS',
					boxLabel: '~~ACTIONTASK_ADDED_TO_PROCESS~~'
				}, {
					name: 'ACTIONTASK_REMOVED_FROM_PROCESS',
					boxLabel: '~~ACTIONTASK_REMOVED_FROM_PROCESS~~'
				}
				/*, {
				name: 'ACTIONTASK_DIGEST_EMAIL',
				boxLabel: '~~ACTIONTASK_DIGEST_EMAIL~~'
			}, {
				name: 'ACTIONTASK_FORWARD_EMAIL',
				boxLabel: '~~ACTIONTASK_FORWARD_EMAIL~~'
			}*/
			]
		}, {
			title: '~~RUNBOOK~~',
			defaultType: 'checkbox',
			autoScroll: true,
			bodyPadding: '10px',
			defaults: {
				hideLabel: true
			},
			items: [{
					name: 'selectAllRunbook',
					boxLabel: '~~selectAllRunbook~~'
				}, {
					name: 'RUNBOOK_UPDATE',
					boxLabel: '~~RUNBOOK_UPDATE~~'
				}, {
					name: 'RUNBOOK_ADDED_TO_PROCESS',
					boxLabel: '~~RUNBOOK_ADDED_TO_PROCESS~~'
				}, {
					name: 'RUNBOOK_REMOVED_FROM_PROCESS',
					boxLabel: '~~RUNBOOK_REMOVED_FROM_PROCESS~~'
				}, {
					name: 'RUNBOOK_COMMIT',
					boxLabel: '~~RUNBOOK_COMMIT~~'
				}
				/*, {
				name: 'RUNBOOK_DIGEST_EMAIL',
				boxLabel: '~~RUNBOOK_DIGEST_EMAIL~~'
			}, {
				name: 'RUNBOOK_FORWARD_EMAIL',
				boxLabel: '~~RUNBOOK_FORWARD_EMAIL~~'
			}*/
			]
		}, {
			title: '~~DOCUMENT~~',
			defaultType: 'checkbox',
			autoScroll: true,
			bodyPadding: '10px',
			defaults: {
				hideLabel: true
			},
			items: [{
					name: 'selectAllDocument',
					boxLabel: '~~selectAllDocument~~'
				}, {
					name: 'DOCUMENT_CREATE',
					boxLabel: '~~DOCUMENT_CREATE~~'
				}, {
					name: 'DOCUMENT_UPDATE',
					boxLabel: '~~DOCUMENT_UPDATE~~'
				}, {
					name: 'DOCUMENT_DELETE',
					boxLabel: '~~DOCUMENT_DELETE~~'
				}, {
					name: 'DOCUMENT_PURGED',
					boxLabel: '~~DOCUMENT_PURGED~~'
				}, {
					name: 'DOCUMENT_ADDED_TO_PROCESS',
					boxLabel: '~~DOCUMENT_ADDED_TO_PROCESS~~'
				}, {
					name: 'DOCUMENT_REMOVED_FROM_PROCESS',
					boxLabel: '~~DOCUMENT_REMOVED_FROM_PROCESS~~'
				}, {
					name: 'DOCUMENT_COMMIT',
					boxLabel: '~~DOCUMENT_COMMIT~~'
				}
				/*, {
				name: 'DOCUMENT_DIGEST_EMAIL',
				boxLabel: '~~DOCUMENT_DIGEST_EMAIL~~'
			}, {
				name: 'DOCUMENT_FORWARD_EMAIL',
				boxLabel: '~~DOCUMENT_FORWARD_EMAIL~~'
			}*/
			]
		}, {
			title: '~~DECISIONTREES~~',
			defaultType: 'checkbox',
			autoScroll: true,
			bodyPadding: '10px',
			defaults: {
				hideLabel: true
			},
			items: []
		}, {
			title: '~~RSS~~',
			defaultType: 'checkbox',
			autoScroll: true,
			bodyPadding: '10px',
			defaults: {
				hideLabel: true
			},
			items: [{
					name: 'selectAllRSS',
					boxLabel: '~~selectAllRSS~~'
				}, {
					name: 'RSS_CREATE',
					boxLabel: '~~RSS_CREATE~~'
				}, {
					name: 'RSS_UPDATE',
					boxLabel: '~~RSS_UPDATE~~'
				}, {
					name: 'RSS_PURGED',
					boxLabel: '~~RSS_PURGED~~'
				}, {
					name: 'RSS_ADDED_TO_PROCESS',
					boxLabel: '~~RSS_ADDED_TO_PROCESS~~'
				}, {
					name: 'RSS_REMOVED_FROM_PROCESS',
					boxLabel: '~~RSS_REMOVED_FROM_PROCESS~~'
				}
				/*, {
				name: 'RSS_DIGEST_EMAIL',
				boxLabel: '~~RSS_DIGEST_EMAIL~~'
			}, {
				name: 'RSS_FORWARD_EMAIL',
				boxLabel: '~~RSS_FORWARD_EMAIL~~'
			}*/
			]
		}]
	}]
})