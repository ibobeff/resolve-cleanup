glu.defView('RS.social.Stream', {
	xtype: 'container',
	hidden: '@{!displayStream}',
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	ui: 'social-navigation-stream',
	cls: 'stream-@{selectedCss}',
	overCls: 'stream-over',
	bodyStyle: 'background:transparent',
	height: 22,
	items: [{
		ui: 'social-navigation-stream',
		html: '@{display}',
		tooltip: '@{displayName}',
		bodyStyle: 'background:transparent',
		flex: 1,
		listeners: {
			render: function(panel) {
				var tip = Ext.create('Ext.tip.ToolTip', {
					target: panel.getEl(),
					html: panel.tooltip
				});
			}
		}
	}, {
		xtype: 'tool',
		hidden: '@{!displayTool}',
		pinned: '@{pinned}',
		locked: '@{lock}',
		showPopout: '@{showPopout}',
		type: 'toggle-collapsed',
		displayView: '@{displayView}',
		displaySettings: '@{displaySettings}',
		style: 'margin-right: 10px',
		isMe: '@{isMe}',
		isSystem: '@{isSystem}',
		allowUnfollow: '@{allowUnfollow}',
		sendDigest: '@{sendDigest}',
		sendEmail: '@{sendEmail}',
		showDigest: '@{showDigest}',
		showEmail: '@{showEmail}',
		handler: function() {
			var me = this;
			if (!me.menu)
				me._vm.getNotification(function() {
					me.menu = Ext.create('Ext.menu.Menu', {
						items: [{
							text: RS.social.locale.view,
							iconCls: 'icon-play icon-large stream-menu-icon',
							hidden: !me.displayView,
							handler: function() {
								me.fireEvent('view', me)
							}
						}, {
							text: RS.social.locale.settings,
							iconCls: 'icon-cog icon-large stream-menu-icon',
							hidden: !me.displaySettings,
							handler: function() {
								me.fireEvent('configure', me)
							}
						}, {
							text: RS.social.locale.configureNotifications,
							hidden: !me.isSystem,
							iconCls: 'icon-large icon-cog stream-menu-icon',
							handler: function() {
								me.fireEvent('configureNotifications', me)
							}
						}, {
							itemId: 'pinMenu',
							text: '',
							iconCls: 'icon-pushpin icon-large stream-menu-icon',
							handler: function() {
								me.fireEvent('togglePinned', me)
							}
						}, {
							text: RS.social.locale.markAllRead,
							iconCls: 'icon-ok icon-large stream-menu-icon',
							handler: function() {
								me.fireEvent('markStreamRead', me)
							}
						}, {
							text: RS.social.locale.unfollow,
							iconCls: 'icon-reply icon-large stream-menu-icon',
							hidden: me.isMe || !me.allowUnfollow,
							handler: function() {
								me.fireEvent('unfollowStream', me)
							}
						}, {
							itemId: 'lockMenu',
							hidden: me.isMe,
							text: '',
							iconCls: 'icon-large',
							handler: function() {
								me.fireEvent('toggleLocked', me)
							}
						}, {
							text: RS.social.locale.popout,
							hidden: !me.showPopout,
							iconCls: 'icon-external-link icon-large stream-menu-icon',
							handler: function() {
								me.fireEvent('popout', me)
							}
						}, '-', {
							itemId: 'digest',
							hidden: !me.showDigest,
							text: RS.social.locale.digest,
							checked: me.sendDigest,
							listeners: {
								checkchange: function(item, checked) {
									me.fireEvent('digestChanged', item, checked)
								}
							}
						}, {
							itemId: 'email',
							hidden: !me.showEmail,
							text: RS.social.locale.forward,
							checked: me.sendEmail,
							listeners: {
								checkchange: function(item, checked) {
									me.fireEvent('emailForwardChanged', item, checked)
								}
							}
						}],

						listeners: {
							beforehide: function() {
								if (Ext.isIE8m)
									Ext.each(this.query('menuitem[iconCls*="stream-menu-icon"]'), function(item) {
										if (!item.isVisible())
											return;
										var icons = item.getEl().query('*[id*="iconEl"]')
										Ext.each(icons, function(icon) {
											// alert(Ext.fly(icon).getVisibilityMode())
											// Ext.fly(icon).hide();
											icon.style.display = 'none';
										});
									});
							},
							show: function() {
								if (Ext.isIE8m)
									Ext.each(this.query('menuitem[iconCls*="stream-menu-icon"]'), function(item) {
										if (!item.isVisible())
											return;
										var icons = item.getEl().query('*[id*="iconEl"]')
										Ext.each(icons, function(icon) {
											icon.style.display = 'block';
										});
									});
							},
							afterLayout: function() {
								this.alignTo(me.getEl(), 'br');
							}
						}
					})
					me.menu.down('#pinMenu').setIconCls(me.pinned ? 'icon-pushpin icon-large stream-menu-icon icon-rotate-90' : 'icon-pushpin icon-large stream-menu-icon')
					me.menu.down('#pinMenu').setText(me.pinned ? RS.social.locale.unpin : RS.social.locale.pin)
					me.menu.down('#lockMenu').setIconCls(me.locked ? 'icon-lock icon-large stream-menu-icon' : 'icon-unlock icon-large stream-menu-icon')
					me.menu.down('#lockMenu').setText(me.locked ? RS.social.locale.unlock : RS.social.locale.lock)
					me.menu.down('#digest').setChecked(me.sendDigest)
					me.menu.down('#email').setChecked(me.sendEmail)
					me.menu.show();
				});
			else
				me.menu.showBy(me)
		},
		setPinned: function(value) {
			this.pinned = value
		},
		setLocked: function(value) {
			this.locked = value
		},
		setShowPopout: function(value) {
			this.showPopout = value
		},
		setSendDigest: function(value) {
			this.sendDigest = value
		},
		setSendEmail: function(value) {
			this.sendEmail = value
		},
		listeners: {
			view: '@{viewStream}',
			configure: '@{configureStream}',
			togglePinned: '@{togglePinned}',
			markStreamRead: '@{markStreamRead}',
			unfollowStream: '@{unfollowStream}',
			toggleLocked: '@{toggleLocked}',
			popout: '@{popout}',
			digestChanged: '@{digestChanged}',
			emailForwardChanged: '@{emailForwardChanged}',
			configureNotifications: '@{configureNotifications}'
		}
	}],
	listeners: {
		afterrender: function(panel) {
			panel.getEl().on('mouseover', function() {
				panel.fireEvent('mouseOver', panel)
			})
			panel.getEl().on('mouseout', function(e) {
				if (!e.within(panel.getEl(), true))
					panel.fireEvent('mouseOut', panel)
			})
			panel.getEl().on('click', function() {
				panel.fireEvent('select', panel, panel)
			})
			panel.getEl().on('dblclick', function() {
				panel.fireEvent('doubleClick', panel, panel)
			})
		},
		mouseOver: '@{mouseOver}',
		mouseOut: '@{mouseOut}',
		select: '@{select}',
		doubleClick: '@{doubleClick}'
	}
})