glu.defView('RS.social.StreamGroup', {
	title: '@{display}',
	ui: 'social-navigation-group',
	titleCollapse: true,
	collapsible: true,
	hideCollapseTool: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	isMe: '@{isMe}',
	items: '@{streams}',
	headerOverCls: 'stream-over',
	canCollapse: '@{canCollapse}',
	sendDigest: '@{sendDigest}',
	sendEmail: '@{sendEmail}',
	setCanCollapse: function(value) {
		this.canCollapse = value
	},

	dockedItems: [{
		xtype: 'pagingtoolbar',
		dock: 'bottom',
		store: '@{store}',
		hidden: '@{hidePagingToolbar}',
		displayInfo: true,
		items: {
			xtype: 'numberfield',
			width: 180,
			name: 'itemsPerPage',
			fieldLabel: 'Items per page',
			allowDecimals: false,
			minValue: 1,
			maxValue: 40,
			value: 10,
			step: 5,
			listeners: {
				specialkey: function(field, e) {
					if (e.getKey() == e.ENTER) {
						var	value = field.getValue();
						if (value > 40) {
							return;
						}
						var store = this.up('pagingtoolbar').store;
						store.pageSize = value;
						store.load();
					}
				}
			}
		}
	}],

	tools: [{
		xtype: 'tool',
		type: 'plus',
		cls: 'social-follow icon-share-alt',
		style: 'top:0px !important',
		handler: function() {
			this.up('panel').fireEvent('followClicked', this, this)
		},
		listeners: {
			render: function(tool) {
				Ext.create('Ext.tip.ToolTip', {
					html: RS.social.locale.followStream,
					target: tool.getEl()
				})
				tool.up('panel').fireEvent('registerTool', tool, tool)
			}
		}
	}, {
		xtype: 'tool',
		type: 'toggle-collapsed',
		cls: 'social-group-follow',
		handler: function() {
			var me = this;
			if (!me.menu) me.menu = Ext.create('Ext.menu.Menu', {
				items: [
					/*{
					itemId: 'digest',
					text: RS.social.locale.digest,
					checked: me.up('panel').sendDigest,
					listeners: {
						checkchange: function(item, checked) {
							me.up('panel').fireEvent('digestChanged', item, checked)
						}
					}
				},
					{
						itemId: 'email',
						text: RS.social.locale.forward,
						checked: me.up('panel').sendEmail,
						listeners: {
							checkchange: function(item, checked) {
								me.up('panel').fireEvent('digestChanged', item, checked)
							}
						}
					}, */
					{
						text: RS.social.locale.applyDigest,
						handler: function(item) {
							me.up('panel').fireEvent('digestApplied', item)
						}
					}, {
						text: RS.social.locale.unapplyDigest,
						handler: function() {
							me.up('panel').fireEvent('unapplyDigest')
						}
					}, {
						text: RS.social.locale.applyEmail,
						handler: function(item) {
							me.up('panel').fireEvent('emailApplied', item)
						}
					}, {
						text: RS.social.locale.unapplyEmail,
						handler: function(item) {
							me.up('panel').fireEvent('unapplyEmail', item)
						}
					}
				]
			})
			// me.menu.down('#digest').setChecked(me.up('panel').sendDigest)
			// me.menu.down('#email').setChecked(me.up('panel').sendEmail)
			me.menu.showBy(me)
		}
	}],
	listeners: {
		followClicked: '@{joinComponent}',
		gearClicked: '@{openComponentSettings}',
		registerTool: '@{registerTool}',
		digestChanged: '@{digestChanged}',
		emailForwardChanged: '@{emailForwardChanged}',
		digestApplied: '@{digestApplied}',
		unapplyDigest: '@{unapplyDigest}',
		emailApplied: '@{emailApplied}',
		unapplyEmail: '@{unapplyEmail}',
		toggleCollapse: '@{toggleCollapse}',
		beforeCollapse: function(panel) {
			if (panel.canCollapse)
				panel.fireEvent('toggleCollapse')
				// return panel.canCollapse
			return false
		},
		afterrender: function( panel, eOpts ) {
			// NOTE
			// To get around the issue with glupagingtoolbar that registers 'beforechange' with
			// a method returning 'false' that prevents the paging actions from working.
			//
			panel.down('pagingtoolbar').hasListeners['beforechange'] = 0;
		}
	}
});

glu.defView('RS.social.Blank', {
	html: '&nbsp;',
	height: 1
});