glu.defView('RS.social.Component', {
	asWindow: {
		height: 162,
		width: 350,
		title: '@{name}',
		draggable: false,
		baseCls: Ext.isIE8m ? 'x-window' : 'x-panel',
		ui: Ext.isIE8m ? 'default' : 'social-component-dialog',
		displayTarget: '@{target}',
		floatable: false,
		listeners: {
			afterrender: function(panel) {
				panel.getEl().on('mouseout', function(e) {
					Ext.defer(function() {
						if (!Ext.EventObject.within(panel.getEl()))
							panel.fireEvent('mouseClose', panel)
					}, 1000, panel)
				})
			},
			show: function(panel) {
				if (panel.displayTarget.getY() <= panel.getHeight())
					panel.alignTo(panel.displayTarget, 'tl-bl')
				else panel.alignTo(panel.displayTarget, 'bl-tl')
			},
			mouseClose: '@{close}'
		}
	},
	bodyPadding: '10px',
	bodyCls: 'component-description',
	html: '@{componentDescription}',
	buttons: {
		xtype: 'toolbar',
		cls: 'component-toolbar-footer',
		hidden: '@{currentUser}',
		items: ['->', {
			xtype: 'tbtext',
			text: '@{viewText}',
			cls: 'rs-link',
			listeners: {
				render: function(item) {
					item.getEl().on('click', function() {
						item.fireEvent('view', item)
					})
				},
				view: '@{view}'
			}
		}, {
			xtype: 'panel',
			html: '@{followText}',
			width: 90,
			style: 'background:transparent !important',
			bodyStyle: 'background:transparent !important',
			listeners: {
				render: function(item) {
					item.getEl().on('click', function() {
						item.fireEvent('toggleFollowing', item)
					})
				},
				toggleFollowing: '@{toggleFollowing}'
			}
		}]
	}
})