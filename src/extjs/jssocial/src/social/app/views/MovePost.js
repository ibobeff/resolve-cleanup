glu.defView('RS.social.MovePost', {
	asWindow: {
		height: 200,
		width: 600,
		title: '~~movePost~~'
	},

	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	bodyPadding: '10px',
	items: [{
		xtype: 'comboboxselect',
		forceSelection: false,
		hideTrigger: true,
		itemId: 'toField',
		store: '@{streamToStore}',
		displayField: 'displayName',
		valueField: 'id',
		name: 'to',
		queryMode: 'local',
		typeAhead: true,
		multiSelect: true,
		tpl: '<tpl for="."><div class="x-boundlist-item">{displayName} {[values["type"] ? "(" + values["type"] + ")" : ""]}</div></tpl>'
	}, {
		xtype: 'hidden',
		name: 'toHidden',
		setValue: function(value) {
			var form = this.up('window');
			if (form) form.down('#toField').setValue(value)
		}
	}],
	buttonAlign: 'left',
	buttons: ['movePost', 'cancel']
})