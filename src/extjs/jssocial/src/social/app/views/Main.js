glu.defView('RS.social.Main', {
	layout: 'border',
	itemId: 'RSsocialMain',
	screenName: 'RS.social.Main',
	items: [{
		xtype: 'container',
		hidden: '@{!showStreams}',
		region: 'west',
		width: 275,
		split: true,
		layout: {
			type: 'vbox',
			align: 'center'
		},
		items: [{
			flex: 1,
			width: '100%',
			ui: 'social-navigation-header',
			title: '~~streams~~',
			items: '@{groups}',
			bodyPadding: '0px 0px 0px 5px',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			autoScroll: true,
			tools: [{
				type: 'up',
				style: 'top: 0px !important',
				handler: function() {
					this.getEl().select('img').toggleCls('x-tool-up')
					this.getEl().select('img').toggleCls('x-tool-down')
					this.up('panel').fireEvent('toggleStreams', this, !this.isUp)
					this.getEl().set({
						'data-qtip': this.isUp ? RS.social.locale.showAll : RS.social.locale.showActive
					})
					this.isUp = !this.isUp
				},
				listeners: {
					afterrender: function() {
						this.getEl().set({
							'data-qtip': RS.social.locale.showAll
						})
					}
				}
			}, {
				type: 'plus',
				cls: 'social-main-follow icon-share-alt',
				style: 'top:0px !important',
				handler: function() {
					this.up('panel').fireEvent('followClicked', this, this)
				},
				listeners: {
					render: function(tool) {
						Ext.create('Ext.tip.ToolTip', {
							html: RS.social.locale.followStream,
							target: tool.getEl()
						})
						// tool.up('panel').fireEvent('registerTool', tool, tool)
					}
				}
			}, {
				type: 'refresh',
				style: 'top: 0px !important',
				handler: function() {
					this.up('panel').fireEvent('refreshStreams', this)
				},
				listeners: {
					afterrender: function(button) {
						this.getEl().set({
							'data-qtip': RS.social.locale.refreshStreams
						});
						var view = this.up('#RSsocialMain');
						// Pass the screen name to fix social is rendered when wiki main is rendered.
						// This is the only place found so far having this issue.
						clientVM.updateRefreshButtons(button, view.screenName);
					}
				}
			}],
			listeners: {
				refreshStreams: '@{refreshStreams}',
				toggleStreams: '@{toggleStreams}',
				followClicked: '@{joinComponent}'
			}
		}]
	}, {
		xtype: 'container',
		region: 'center',
		itemId: 'centerRegion',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		autoScroll: true,
		minHeight: 200,
		items: [{
			xtype: 'container',

			layout: {
				type: 'hbox',
				align: 'top'
			},
			items: [{
				layout: {
					type: 'card',
					deferredRender: true
				},
				flex: 1,
				activeItem: '@{activePostCard}',
				items: [{
					xtype: 'container',
					padding: '10px',
					layout: {
						type: 'hbox'
					},
					height: 35,
					listeners: {
						activate: function(panel) {
							if (panel.ownerCt.originalHeight) panel.ownerCt.setHeight(panel.ownerCt.originalHeight)
						}
					},
					items: [{
						xtype: 'toolbar',
						items: [{
							xtype: 'button',
							iconCls: 'icon-large icon-eject rs-icon',
							name: 'switchToPostDisplay'
						}]
					}, {
						xtype: 'textfield',
						flex: 1,
						itemId: 'postYourMessageBox',
						emptyText: '@{postYourMessageDisplay}',
						setEmptyText: function(value) {
							this.emptyText = value
							this.applyEmptyText()
						},
						hidden: '@{!showPostActionToolbarActions}',
						disabled: '@{!postYourMessageBoxReady}',
						inputCls: 'post-message',
						resizeForPost: function() {
							this.ownerCt.ownerCt.originalHeight = this.ownerCt.ownerCt.getHeight()
							this.ownerCt.ownerCt.setHeight(Ext.getBody().getHeight() * 0.5)
						},
						listeners: {
							postMessage: '@{switchToPostMessage}',
							render: function(field) {
								field.getEl().on('click', function() {
									field.fireEvent('postMessage', field)
									// field.ownerCt.ownerCt.animate({
									// 	to: {
									// 		height: Ext.getBody().getHeight() * 0.5 //50% of the document's height
									// 	},
									// 	duration: 500,
									// 	easing: 'backOut',
									// 	dynamic: true
									// })
								})
							}
						}
					}]
				}, {
					xtype: 'form',
					itemId: 'postForm',
					bodyPadding: '10px',
					defaults: {
						anchor: '100%'
					},
					items: [{
						xtype: 'comboboxselect',
						forceSelection: false,
						hideTrigger: true,
						itemId: 'toField',
						store: '@{streamToStore}',
						displayField: 'displayName',
						valueField: 'id',
						name: 'to',
						queryMode: 'local',
						typeAhead: true,
						typeAheadDelay: 500,
						multiSelect: true,
						tpl: '<tpl for="."><div class="x-boundlist-item">{displayName} {[values["type"] ? "(" + values["type"] + ")" : ""]}</div></tpl>',
						listeners: {
							render: function(field) {
								field.labelEl.on({
									scope: field,
									mouseover: function() {
										this.labelEl.addCls('toLabelOver')
									},
									mouseout: function() {
										this.labelEl.removeCls('toLabelOver')
									},
									click: function() {
										field.fireEvent('showAdvancedTo', field)
									}
								})
							},
							select: function() {
								this.collapse()
							},
							showAdvancedTo: '@{showAdvancedTo}'
						}
					}, {
						xtype: 'hidden',
						name: 'toHidden',
						setValue: function(value) {
							var form = this.up('form');
							if (form) form.down('#toField').setValue(value)
						}
					}, {
						xtype: 'textfield',
						itemId: 'subject',
						name: 'subject'
					}, {
						xtype: 'posteditor',
						anchor: '100% -50',
						name: 'content',
						itemId: 'content',
						listeners: {
							//createLink: '@{createLink}',
							//attachFile: '@{attachFile}'
						}
					}],
					buttonAlign: 'left',
					buttons: {
						xtype: 'toolbar',
						cls: 'post-toolbar-footer',
						items: [{
							name: 'post',
							handler: function(button) {
								this.up().up().down('posteditor').closeMenu();
								button.up('form').fireEvent('post', button.up('form'), button.up('form'))
							}
						}, {
							name: 'comment',
							handler: function(button) {
								this.up().up().down('posteditor').closeMenu();
								button.up('form').fireEvent('comment', button.up('form'), button.up('form'))
							}
						}, {
							text: '~~cancel~~',
							handler: function(button) {
								this.up().up().down('posteditor').closeMenu();
								button.up('form').fireEvent('resetPostForm')
								button.up('form').fireEvent('cancel')
							}
						}]
					},
					listeners: {
						show: function(form) {
							Ext.defer(function() {
								form.down('#content').focus()
								var toField = form.down('#toField'),
									models = toField.valueModels;
								Ext.Array.forEach(models, function(model) {
									if (model.get('type').toLowerCase() == 'forum') {
										form.down('#subject').focus()
									}
								})
							}, 10)
						},
						resetPostForm: function() {
							this.down('#toField').clearValue() //manually clear the value because glu bindings are a little weak
						},
						cancel: '@{cancel}',
						post: '@{post}',
						comment: '@{comment}'
					}
				}, {
					bodyPadding: '10px',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: [{
						xtype: 'combobox',
						itemId: 'filter',
						emptyText: '~~filter~~',
						inputCls: 'post-message',
						filterStore: '@{postFilterStore}',					
						displayField: 'name',
						valueField: 'value',
						columns: [{
							dataIndex: 'content',
							filterable: true,
							text: '~~content~~'
						}, {
							dataIndex: 'authorUsername',
							filterable: true,
							text: 'Author'
						}, {
							dataIndex: 'title',
							filterable: true,
							text: 'Subject'
						}],
						/*plugins: [{
							ptype: 'searchfieldfilter',
							showComparisons: false,
							filterBarItemId: 'filterBar'
						}],*/
						listeners: {
							beforerender: function(combo) {
								Ext.Array.forEach(combo.columns, function(column) {
									switch (column.text) {
										case '~~content~~':
											column.text = RS.social.locale.content
											break;
										case '~~subject~~':
											column.text = RS.social.locale.subject
											break;
										case '~~author~~':
											column.text = RS.social.locale.author
											break;
									}
								})
							}
						}
					}, {
						xtype: 'filterbar',
						itemId: 'filterBar',
						flex: 1,
						allowPersistFilter: false,
						listeners: {
							add: {
								fn: function() {
									this.ownerCt.down('#filter').getPlugin().filterChanged()
								},
								buffer: 10
							},
							remove: {
								fn: function() {
									this.ownerCt.down('#filter').getPlugin().filterChanged()
								},
								buffer: 10
							},
							clearFilter: function() {
								this.ownerCt.down('#filter').clearValue()
							}
						}
					}],
					listeners: {
						activate: function(panel) {
							panel.down('#filter').focus()
						}
					}
				}, {
					html: '&nbsp;'
				}]
			}, {
				xtype: 'toolbar',
				padding: '10px 10px 0px 0px',
				items: [{
					name: 'switchToPostMessage',
					hidden: '@{!showPostActionToolbarActions}',
					text: '',
					iconCls: 'icon-comment-alt social-post-toolbar',
					tooltip: '~~postMessageTooltip~~'
				}, {
					name: 'switchToFilter',
					hidden: '@{!showPostToolbarActions}',
					text: '',
					iconCls: 'icon-search social-post-toolbar',
					tooltip: '~~filterTooltip~~'
				}, {
					xtype: 'splitbutton',
					name: 'markAllPostsRead',
					hidden: '@{!showPostToolbarActions}',
					text: '',
					iconCls: 'icon-ok social-post-toolbar',
					tooltip: '~~markAllPostsReadTooltip~~',
					menu: [{
							xtype: 'panel',
							plain: true,
							//style: 'font-weight: bold',
							cls: 'x-menu-item-header',
							html: '~~markAsRead~~'
						}, {
							name: 'markAllPostsRead'
						}, {
							name: 'markPostsOlderThanOneDayRead'
						}, {
							name: 'markPostsOlderThanOneWeekRead'
						}, {
							xtype: 'panel',
							plain: true,
							//style: 'font-weight: bold',
							cls: 'x-menu-item-header',
							html: '~~filterMenuHeader~~'
						}, {
							xtype: 'menucheckitem',
							checked: '@{unreadOnly}',
							text: '~~unreadOnly~~'
						}
						/*, {
						xtype: 'menucheckitem',
						checked: '@{oneDayOld}',
						text: '~~oneDayOld~~'
					}, {
						xtype: 'menucheckitem',
						checked: '@{oneWeekOld}',
						text: '~~oneWeekOld~~'
					}*/
					]
				}, {
					xtype: 'tbseparator',
					hidden: '@{!showPostToolbarActions}',
					cls: 'social-post-toolbar'
				}, {
					iconCls: 'icon-chevron-left social-post-toolbar-page',
					hidden: '@{!showPostToolbarButtons}',
					name: 'previousPage',
					tooltip: '~~previousPage~~',
					text: ''
				}, {
					iconCls: 'icon-chevron-right social-post-toolbar-page',
					hidden: '@{!showPostToolbarButtons}',
					name: 'nextPage',
					tooltip: '~~nextPage~~',
					text: ''
				}, {
					xtype: 'tbseparator',
					hidden: '@{!showPostToolbarButtons}',
					cls: 'social-post-toolbar'
				}, {
					xtype: 'splitbutton',
					name: 'refreshPosts',
					text: '',
					hidden: '@{!showPostToolbarButtonsRefresh}',
					iconCls: 'icon-repeat social-post-toolbar',
					tooltip: '~~refreshPostsTooltip~~',
					menu: {
						cls: 'auto-refresh-container',
						width: 200,
						plain: true,
						items: [{
							xtype: 'checkboxfield',
							name: 'autoRefreshPosts',
							hideLabel: true,
							boxLabel: '~~autoRefreshPosts~~'
						}, {
							xtype: 'fieldcontainer',
							hidden: '@{!autoRefreshPosts}',
							layout: {
								type: 'hbox',
								align: 'stretch'
							},
							items: [{
								xtype: 'numberfield',
								labelWidth: 60,
								name: 'pollInterval',
								minValue: 5,
								flex: 1								
							}, {
								xtype: 'tbtext',
								style: {
									marginTop: '2px',
									lineHeight: '20px'
								},
								text: '(sec.)'
							}]
						}],
						listeners: {
							hide: '@{startPolling}'
						}
					}
				}]
			}]
		}, {
			flex: 1,
			itemId: 'postPanel',
			layout: 'card',
			activeItem: '@{postDisplayItem}',
			items: [{
				bodyPadding: '10px',
				autoScroll: true,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: '@{posts}',
				listeners: {
					render: function(panel) {
						panel.body.on('scroll', function(e, el) {
							panel.fireEvent('hideLikes', panel)

							var thisHeight = [],
								prevHeight = [],
								total = 0,
								scroll = el.scrollTop,
								i = 0;
							panel.items.each(function(item, index) {
								total = total + item.getHeight()
								thisHeight[index] = item.getHeight()
								prevHeight[index] = total - item.getHeight()
							})

							for (; i < panel.items.length; i++) {
								if (prevHeight[i] < (scroll + panel.getHeight() / 4) && prevHeight[i + 1] >= (scroll + panel.getHeight() / 4)) {
									panel.prevItem = panel.currentItem

									panel.currentItem = i
									if (panel.currentItem != panel.prevItem) {
										panel.fireEvent('markReadScroll', panel, panel.prevItem)
									}
								}
							}
						})
					},
					add: function(panel, component, index) {
						if (index % 10 == 9)
							component.on('render', function() {
								panel.body.scrollTo('top', 0);
							});
					},
					hideLikes: '@{hideLikes}',
					markReadScroll: '@{markReadScroll}'
				}
			}, {
				bodyPadding: '10px',
				autoScroll: true,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [{
					xtype: '@{activePost}'
				}],
				listeners: {
					render: function(panel) {
						panel.body.on('scroll', function(e, el) {
							panel.fireEvent('hideLikes', panel, e, el, panel)
						})
					},
					hideLikes: '@{hideLikes}'
				}
			}]
		}]
	}],
	listeners: {
		afterrender: function() {
			var splitters = this.query('bordersplitter')
			Ext.Array.forEach(splitters, function(splitter) {
				splitter.setSize(1, 1)
				splitter.getEl().setStyle({
					backgroundColor: '#fbfbfb'
				})
			})
		},
		beforedestroy : '@{beforeDestroyComponent}'
	}
});