glu.defView('RS.social.Likes', {
	asWindow: {
		baseCls: 'x-panel',
		ui: 'social-likes-dialog',
		height: 215,
		width: 325,
		title: '~~peopleWhoLikeThis~~',
		draggable: false,
		displayTarget: '@{target}',
		floatable: false,
		listeners: {
			afterrender: function(panel) {
				panel.getEl().on('mouseout', function(e) {
					Ext.defer(function() {
						if (!Ext.EventObject.within(panel.getEl()))
							panel.fireEvent('mouseClose', panel)
					}, 1000, panel)
				})
			},
			show: function(panel) {
				panel.alignTo(panel.displayTarget, 'tr-br')
			},
			mouseClose: '@{close}'
		}
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	autoScroll: true,
	items: '@{likes}',
	bodyPadding: '10px',
	itemTemplate: {
		layout: {
			type: 'hbox',
			align: 'middle'
		},
		padding: '0px 0px 10px 0px',
		items: [{
			xtype: 'image',
			width: 46,
			height: 46,
			margin: '0px 10px 0px 0px',
			src: '@{likeProfileImage}'
		}, {
			xtype: 'label',
			cls: 'like-username',
			text: '@{displayName}',
			flex: 1,
			listeners: {
				render: function(label) {
					label.getEl().on('click', function() {
						label.fireEvent('displayUser', label)
					})
				},
				displayUser: '@{displayUser}'
			}
		}, {
			html: '@{followText}',
			minWidth: 85,
			listeners: {
				render: function(item) {
					item.getEl().on('click', function() {
						item.fireEvent('toggleFollowing', item)
					})
				},
				toggleFollowing: '@{toggleFollowing}'
			}
		}]
	}
})