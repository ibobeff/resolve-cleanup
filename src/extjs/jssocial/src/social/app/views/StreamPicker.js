glu.defView('RS.social.StreamPicker', {
	padding : 15,
	title: '@{windowTitle}',
	modal: true,
	width: 900,
	height: 500,
	layout: 'card',
	activeItem: '@{activeItem}',
	items: [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'toGrid',
		columns: '@{toGridColumns}',
		selType: 'checkboxmodel',
		dockedItems: [{
			name: 'actionBar',
			xtype: 'toolbar',
			cls : 'rs-dockedtoolbar',
			items: [{
				xtype: 'combobox',
				name: 'type',
				emptyText: '~~type~~',
				hideLabel: true,
				displayField: 'name',
				valueField: 'value',
				queryMode: 'local',
				editable: false,
				listConfig: {
					tpl: '<tpl for="."><div class="x-boundlist-item">{name}</div><tpl if="xindex == 3"><hr /></tpl></tpl>'
				},
				listeners: {
					render: function(combo) {
						combo.store.on('load', function(store, records) {
							if (combo.getValue())
								combo.select(combo.store.getAt(combo.store.findExact('value', combo.getValue())))
						}, combo, {
							single: true
						})
					}
				}
			}, {
				xtype: 'textfield',
				name: 'searchQuery',
				hideLabel: true,
				minWidth: 300,
				flex: 1,
				emptyText: '~~searchQuery~~'
			}, {
				xtype: 'button',
				handler: '@{search}',
				iconCls: 'icon-search rs-icon-button'
			}]
		}],
		plugins: [{
			ptype: 'pager',
			showSysInfo: false
		}]
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'worksheets',
		flex: 1,
		columns: '@{worksheetsColumns}',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: [{
				xtype: 'combobox',
				name: 'type',
				emptyText: '~~type~~',
				hideLabel: true,
				displayField: 'name',
				valueField: 'value',
				queryMode: 'local',
				listeners: {
					render: function(combo) {
						combo.store.on('load', function(store, records) {
							if (combo.getValue())
								combo.select(combo.store.getAt(combo.store.findExact('value', combo.getValue())))
						}, combo, {
							single: true
						})
					}
				}
			}, {
				xtype: 'textfield',
				name: 'searchText',
				hideLabel: true,
				width: 250,
				emptyText: '~~searchWorksheet~~'
			}, {
				iconCls: 'icon-large icon-user rs-icon @{myWorksheetsPressedCls}',
				name: 'myWorksheets',
				text: '',
				tooltip: '@{myWorksheetsTooltip}'
			}]
		}],
		plugins: [{
			ptype: 'pager',
			showSysInfo: false,
			allowAutoRefresh: false
		}]
	}],	
	buttons: [{
		name :'select', 
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name :'follow', 
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name :'addFollowers', 
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name: 'createComponent',
		cls : 'rs-med-btn rs-btn-dark',
		roleHide: true,
		roles: 'social_admin'
	},{
		name :'cancel', 
		cls : 'rs-med-btn rs-btn-light'
	}]
})