glu.defView('RS.social.Post', {
	padding: '15px 0px',
	cls: '@{forumBorderCls}',
	items: [{
		xtype: 'container',
		layout: {
			type: 'hbox',
			align: 'middle'
		},
		items: [{
			xtype: 'image',
			cls: 'profile-pic',
			src: '@{userProfilePicture}',
			authorUsername: '@{authorUsername}',
			setAuthorUsername: function(authorUsername) {
				this.authorUsername = authorUsername;
			},
			hidden: '@{!showImage}',
			width: '@{imageSize}',
			height: '@{imageSize}',
			margin: '@{imagePadding}',
			listeners: {
				render: function(panel) {
					panel.getEl().on('click', function() {
						panel.fireEvent('navigateToProfile', panel, panel.authorUsername)
					})
				},
				navigateToProfile: '@{navigateToProfile}'
			}
		}, {
			flex: 1,
			xtype: 'container',
			items: [{
				xtype: 'toolbar',
				padding: '0px',
				items: [{
					xtype: 'tbtext',
					text: '@{subjectDisplay}',
					tooltip: '@{subjectTooltip}',
					hidden: '@{showAuthorSubject}',
					cls: 'post-subject @{postForumSubjectCls}',
					listeners: {
						render: function(tbtext) {
							if (tbtext.tooltip)
								tbtext.tooltipControl = Ext.create('Ext.tip.ToolTip', {
									target: tbtext.getEl(),
									html: tbtext.tooltip
								})

							tbtext.getEl().on('click', function() {
								tbtext.fireEvent('subjectClicked', tbtext)
							})
						},
						subjectClicked: '@{subjectClicked}'
					}
				}, {
					xtype: 'tbtext',
					hidden: '@{!showAuthorSubject}',
					cls: 'post-author-subject',
					text: '@{authorDisplayName}',
					listeners: {
						render: function(tbtext) {
							if (tbtext.tooltip)
								tbtext.tooltipControl = Ext.create('Ext.tip.ToolTip', {
									target: tbtext.getEl(),
									html: tbtext.tooltip
								})

							tbtext.getEl().on('click', function() {
								tbtext.fireEvent('subjectClicked', tbtext)
							})
						},
						subjectClicked: '@{subjectClicked}'
					}
				}, '->', {
					xtype: 'tbtext',
					text: '@{answerText}',
					cls: '@{answerCls}',
					hidden: '@{!showAnswer}'
				}, {
					xtype: 'tbseparator',
					hidden: '@{!showAnswer}'
				}, {
					xtype: 'tbtext',
					cls: 'post-link',
					text: '~~share~~',
					hidden: '@{!showShareLink}',
					listeners: {
						render: function(tbtext) {
							tbtext.getEl().on('click', function() {
								tbtext.fireEvent('sharePost', tbtext, tbtext)
							})
						},
						sharePost: '@{sharePost}'
					}
				}, {
					xtype: 'tbseparator',
					hidden: '@{!isPost}'
				}, {
					xtype: 'tbtext',
					cls: '@{readCls}',
					tooltip: '@{readTooltip}',
					setTooltip: function(value) {
						if (this.tooltipControl) this.tooltipControl.update(value)
					},
					listeners: {
						afterrender: function(tbtext) {
							tbtext.getEl().on('click', function() {
								tbtext.fireEvent('toggleRead', tbtext)
							})
							tbtext.tooltipControl = Ext.create('Ext.tip.ToolTip', {
								html: tbtext.tooltip,
								target: tbtext.getEl()
							})
						},
						toggleRead: '@{toggleRead}'
					}
				}, {
					xtype: 'tbtext',
					cls: '@{starCls}',
					tooltip: '@{starredTooltip}',
					hidden: '@{!showStar}',
					setTooltip: function(value) {
						if (this.tooltipControl) this.tooltipControl.update(value)
					},
					listeners: {
						afterrender: function(tbtext) {
							tbtext.getEl().on('click', function() {
								tbtext.fireEvent('toggleStarred', tbtext)
							})
							tbtext.tooltipControl = Ext.create('Ext.tip.ToolTip', {
								html: tbtext.tooltip,
								target: tbtext.getEl()
							})
						},
						toggleStarred: '@{toggleStarred}'
					}
				}, {
					xtype: 'tbtext',
					cls: '@{likeCls}',
					tooltip: '@{likeTooltip}',
					setTooltip: function(value) {
						if (this.tooltipControl) this.tooltipControl.update(value)
					},
					listeners: {
						afterrender: function(tbtext) {
							tbtext.getEl().on('click', function() {
								tbtext.fireEvent('toggleLike', tbtext)
							})
							tbtext.tooltipControl = Ext.create('Ext.tip.ToolTip', {
								html: tbtext.tooltip,
								target: tbtext.getEl()
							})
						},
						toggleLike: '@{toggleLike}'
					}
				}, {
					xtype: 'tbtext',
					cls: 'post-link',
					text: '@{numberOfLikesText}',
					listeners: {
						render: function(tbtext) {
							tbtext.getEl().on('click', function() {
								tbtext.fireEvent('toggleLikesDisplay', tbtext, tbtext)
							})
							// tbtext.getEl().on('mouseover', function() {
							// 	Ext.defer(function() {
							// 		if (Ext.EventObject.within(tbtext.getEl())) tbtext.fireEvent('toggleLikesDisplay', tbtext, tbtext)
							// 	}, 1000)
							// })
						},
						toggleLikesDisplay: '@{toggleLikesDisplay}'
					}
				}, {
					xtype: 'tbtext',
					cls: 'icon-large icon-caret-down post-hand',
					read: '@{read}',
					setRead: function(value) {
						this.read = value
					},
					locked: '@{shouldBeLocked}',
					showLocked: '@{showLocked}',
					showMove: '@{showMove}',
					following: '@{following}',
					currentUser: '@{currentUser}',
					hasDeleteRights: '@{hasDeleteRights}',
					listeners: {
						render: function(tbtext) {
							tbtext.getEl().on('click', function() {
								tbtext.fireEvent('showMenu', tbtext)
							})
						},
						showMenu: function(tbtext) {
							if (!tbtext.menu) tbtext.menu = Ext.create('Ext.menu.Menu', {
								items: [{
									text: RS.social.locale.move,
									hidden: !tbtext.showMove,
									iconCls: 'icon-large icon-exchange post-menu-icon',
									handler: function() {
										tbtext.fireEvent('movePost')
									}
								}, {
									text: RS.social.locale.deletePost,
									hidden: !tbtext.hasDeleteRights,
									iconCls: 'icon-large icon-remove post-menu-icon',
									handler: function() {
										tbtext.fireEvent('deletePost')
									}
								}, {
									itemId: 'readButton',
									text: tbtext.read ? RS.social.locale.unread : RS.social.locale.read,
									iconCls: tbtext.read ? 'icon-large icon-envelope post-menu-icon' : 'icon-large icon-ok post-menu-icon',
									handler: function() {
										tbtext.read = !tbtext.read
										this.setText(tbtext.read ? RS.social.locale.unread : RS.social.locale.read)
										this.setIconCls(tbtext.read ? 'icon-large icon-ok post-menu-icon' : 'icon-large icon-envelope post-menu-icon')
										tbtext.fireEvent('toggleReadPost')
									}
								}, {
									text: tbtext.locked ? RS.social.locale.unlock : RS.social.locale.lock,
									iconCls: tbtext.locked ? 'icon-large icon-lock post-menu-icon' : 'icon-large icon-unlock post-menu-icon',
									hidden: !tbtext.showLocked,
									handler: function() {
										tbtext.locked = !tbtext.locked
										this.setText(tbtext.locked ? RS.social.locale.unlock : RS.social.locale.lock)
										this.setIconCls(tbtext.locked ? 'icon-large icon-lock post-menu-icon' : 'icon-large icon-unlock post-menu-icon')
										tbtext.fireEvent('toggleLockPost')
									}
								}, {
									text: tbtext.following ? RS.social.locale.unfollowUser : RS.social.locale.followUser,
									iconCls: tbtext.following ? 'icon-large icon-reply post-menu-icon' : 'icon-large icon-share-alt post-menu-icon',
									hidden: tbtext.currentUser,
									handler: function() {
										tbtext.following = !tbtext.following
										this.setText(tbtext.following ? RS.social.locale.unfollowUser : RS.social.locale.followUser)
										this.setIconCls(tbtext.following ? 'icon-large icon-reply post-menu-icon' : 'icon-large icon-share-alt post-menu-icon')
										tbtext.fireEvent('toggleFollowAuthor')
									}
								}]
							})

							tbtext.menu.down('#readButton').setText(tbtext.read ? RS.social.locale.unread : RS.social.locale.read)
							tbtext.menu.down('#readButton').setIconCls(tbtext.read ? 'icon-large icon-envelope post-menu-icon' : 'icon-large icon-ok post-menu-icon')
							tbtext.menu.showBy(tbtext)
						},
						movePost: '@{movePost}',
						deletePost: '@{deletePost}',
						toggleReadPost: '@{toggleReadPost}',
						toggleLockPost: '@{toggleLockPost}',
						toggleFollowAuthor: '@{toggleFollowAuthor}'
					}
				}]
			}, {
				xtype: 'toolbar',
				padding: '0px',
				items: [{
					xtype: 'tbtext',
					cls: 'post-author',
					text: '@{authorDisplayName}',
					tooltip: '@{authorTooltip}',
					hidden: '@{!showAuthor}',
					setTooltip: function(value) {
						if (this.tooltipCtrl) this.tooltipCtrl.update(value)
						else this.tooltipCtrl = Ext.create('Ext.tip.ToolTip', {
							html: value,
							target: this.getEl()
						})
					},
					listeners: {
						render: function(tbtext) {
							tbtext.getEl().on('click', function() {
								tbtext.fireEvent('toggleAuthorDisplay', tbtext, tbtext)
							})
							// tbtext.getEl().on('mouseover', function() {
							// 	Ext.defer(function() {
							// 		if (Ext.EventObject.within(tbtext.getEl())) tbtext.fireEvent('toggleAuthorDisplay', tbtext, tbtext)
							// 	}, 1000)
							// })
							if (tbtext.tooltip) {
								tbtext.tooltipCtrl = Ext.create('Ext.tip.ToolTip', {
									html: tbtext.tooltip,
									target: tbtext.getEl()
								})
							}
						},
						toggleAuthorDisplay: '@{toggleAuthorDisplay}'
					}
				}, {
					xtype: 'tbtext',
					hidden: '@{!showCaret}',
					cls: 'icon-angle-right icon-large post-arrow'
				}, {
					xtype: 'tbtext',
					cls: 'post-component',
					text: '@{component}',
					hidden: '@{!displayComponent}',
					listeners: {
						render: function(tbtext) {
							tbtext.getEl().on('click', function() {
								tbtext.fireEvent('toggleComponentDisplay', tbtext, tbtext)
							})
						},
						toggleComponentDisplay: '@{toggleComponentDisplay}'
					}
				}, {
					xtype: 'tbtext',
					hidden: '@{!displayTargets}',
					targets: '@{targetList}',
					cls: 'icon-caret-down post-hand',
					listeners: {
						render: function(tbtext) {
							var me = this;
							tbtext.getEl().on('click', function() {
								if (!tbtext.menu) {
									var targetItems = [];
									tbtext.targets.forEach(function(target) {
										targetItems.push({
											text: target.name,
											socialTarget: target,
											listeners: {
												click: function() {
													me.fireEvent('changeInterestedTarget', this, this.socialTarget);
												}
											}
										})
									})
									tbtext.menu = Ext.create('Ext.menu.Menu', {
										items: targetItems
									})
								}
								tbtext.menu.showBy(tbtext)
							})
						},
						changeInterestedTarget: '@{changeInterestedTarget}'
					}
				}, '->', {
					xtype: 'tbtext',
					text: '@{forumCountText}',
					cls: 'post-date'
				}, {
					xtype: 'tbtext',
					cls: 'post-date',
					text: '@{dateText}'
				}]
			}]
		}]
	}, {
		xtype: 'component',
		padding: '@{contentPadding}',
		hidden: '@{!displayContent}',
		style: 'word-wrap:break-word',
		html: '@{contentDisplay}',
		isMini: '@{miniMode}',
		setHtml: function(value) {
			this.update(value)
		},
		tooltip: '@{dateTooltip}',
		listeners: {
			render: function(panel) {
				if (panel.tooltip) panel.tooltipControl = Ext.create('Ext.tip.ToolTip', {
					html: panel.tooltip,
					target: panel.getEl()
				})

				var links = panel.getEl().query('a'),
					images = panel.getEl().query('img');

				Ext.Array.forEach(links, function(link) {
					var data = link.getAttribute('fullData'),
						dataType = link.getAttribute('dataType'),
						currentLocation = window.location.protocol + '//' + window.location.host + window.location.pathname;
					if (data) {
						if (dataType == 'pdf') {
							link.target = '_blank'
							link.href = data
						} else
							Ext.fly(link).on('click', function(e) {
								e.stopEvent()
								downloadFile(data)
							})
					} else if (link.href && link.href.indexOf(currentLocation) == -1) {
						link.target = '_blank'
					}

					if (link.href.indexOf('social.jsp') > -1) {
						var temp = link.href.split('#')[1];
						link.href = '/resolve/jsp/rsclient.jsp' + (temp ? '#' + temp : '')
					}

					//Correct legacy links to be displayed properly
					if (link.href.indexOf('/showentity?') > -1) {
						//grab type
						var params = Ext.Object.fromQueryString(link.href.split('?')[1])
						if (params.type) {
							switch (params.type.toLowerCase()) {
								case 'document':
									link.href = '#RS.wiki.Main/name=' + params.name
									break;
								case 'actiontask':
									link.href = '#RS.actiontask.ActionTask/id=' + params.sysId
									break;
								case 'worksheet':
									link.href = '#RS.worksheet.Worksheet/id=' + params.sysId
									break;
								case 'forum':
									link.href = '#RS.socialadmin.Component/name=forum&id=' + params.sysId
									break;
								case 'rss':
									link.href = '#RS.socialadmin.Component/name=rss&id=' + params.sysId
									break;
								case 'process':
									link.href = '#RS.socialadmin.Component/name=process&id=' + params.sysId
									break;
								case 'team':
									link.href = '#RS.socialadmin.Component/name=team&id=' + params.sysId
									break;
								case 'user':
									link.href = '#RS.user.User/username=' + params.username
									break;
							}
						}
					}

					if (link.href.indexOf('/post/download') > -1 || Ext.isFunction(link.onclick)) {
						var img = Ext.fly(link).query('img')[0];
						var href = link.href;
						if (img) href = img.src;
						if (href.indexOf('?') > -1) {
							var params = Ext.Object.fromQueryString(href.split('?')[1])
							if (params.fileid) {
								link.onclick = null
								Ext.fly(link).on('click', function(e) {
									e.stopEvent()
									downloadFile('/resolve/service/social/download?id=' + params.fileid)
								})
							}
						}
					}

					if (panel.isMini) {
						link.target = '_blank'
						if (link.href.indexOf('social.jsp') > -1) link.href = '/resolve/jsp/rsclient.jsp' + link.hash
					}
				})

				Ext.Array.forEach(images, function(image) {
					var image = Ext.get(image),
						fullData = image.getAttribute('fullData');

					//convert legacy images
					var imageSrc = Ext.fly(image).dom.src;
					if (imageSrc.indexOf('/post/download') > -1) {
						var params = Ext.Object.fromQueryString(imageSrc.split('?')[1])
						if (params.fileid) {
							var fileid = params.fileid
							if (Ext.String.endsWith(fileid, '"')) fileid = fileid.substring(0, fileid.length - 1)
							Ext.fly(image).dom.src = '/resolve/service/social/download?preview=true&id=' + fileid
							fullData = '/resolve/service/social/download?id=' + fileid
						}
					}

					if (fullData) image.setWidth(200) //force only internal images to 200px (rss images should appear as they are+)
					image.on('click', function() {
						displayLargeImage(fullData || image.src, '')
					})
					image.on('load', function() {
						//force refresh of parent container because this image will load an change the size of the content after being loaded
						panel.ownerCt.doLayout()
					})

				})
			}
		}
	}, {
		hidden: '@{!displayAnswers}',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		padding: '20px 0px 30px 18px',
		items: '@{answersList}',
		defaults: {
			viewMode: 'comment'
		}
	}, {
		xtype: 'container',
		hidden: '@{!displayComments}',
		layout: {
			type: 'hbox',
			align: 'middle'
		},
		items: [{
			xtype: 'label',
			cls: 'post-display-comments',
			text: '@{collapseTitle}',
			listeners: {
				render: function(label) {
					label.getEl().on('click', function() {
						label.fireEvent('toggleCollapseComments')
					})
				},
				toggleCollapseComments: '@{toggleCollapseComments}'
			}
		}, {
			xtype: 'label',
			cls: '@{collapseCls} icon-large post-display-comments-caret',
			listeners: {
				render: function(label) {
					label.getEl().on('click', function() {
						label.fireEvent('toggleCollapseComments')
					})
				},
				toggleCollapseComments: '@{toggleCollapseComments}'
			}
		}]
	}, {
		xtype: 'panel',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		hidden: '@{!displayComments}',
		collapsible: true,
		hideCollapseTool: true,
		collapsed: '@{collapsed}',
		header: false,
		padding: '0px 0px 0px 18px',
		items: '@{commentsList}',
		defaults: {
			viewMode: 'comment'
		}
	}, {
		hidden: '@{!displayAddComment}',
		layout: {
			type: 'card',
			deferredRender: true
		},
		activeItem: '@{activePostCard}',
		items: [{
			bodyPadding: '10px',
			layout: 'hbox',
			height: 35,
			listeners: {
				activate: function(panel) {
					if (this.ownerCt.originalHeight) this.ownerCt.setHeight(this.ownerCt.originalHeight)
				}
			},
			items: [{
				xtype: 'textfield',
				flex: 1,
				emptyText: '~~addAComment~~',
				inputCls: 'comment-message',
				listeners: {
					postMessage: '@{switchToPostMessage}',
					render: function(field) {
						field.getEl().on('click', function() {
							field.ownerCt.ownerCt.originalHeight = field.ownerCt.ownerCt.getHeight()
							field.fireEvent('postMessage', field)
							// field.ownerCt.ownerCt.animate({
							// 	to: {
							// 		height: 200
							// 	},
							// 	duration: 750,
							// 	easing: 'backOut',
							// 	dynamic: true
							// })
							field.ownerCt.ownerCt.setHeight(200)
							var editor = field.up('panel').up('panel').down('posteditor').ownerCt
							Ext.defer(function() {
								if (Ext.getBody().getHeight() - editor.getY() < 200)
									field.up('#postPanel').scrollBy({
										x: 0,
										y: 200
									}, true)
							}, 100)
						})
					}
				}
			}]
		}, {
			xtype: 'form',
			bodyPadding: '10px',
			defaults: {
				anchor: '100%'
			},
			items: [{
				xtype: 'posteditor',
				displayLinkAndUpload: '@{!miniMode}',
				padding: '10 0 0 0',
				height: 140,
				name: 'commentContent',
				itemId: 'commentContent',
				hideLabel: true,
				listeners: {
					//createLink: '@{createLink}',
					//attachFile: '@{attachFile}',
					render: function(ctrl) {
						ctrl.focus()
					}
				}
			}],
			buttonAlign: 'left',
			buttons: {
				xtype: 'toolbar',
				cls: 'post-toolbar-footer',
				items: [{
					name: 'comment',
					handler: function(button) {
						button.up('form').fireEvent('comment', button.up('form'), button.up('form'))
					}
				}, {
					text: '~~cancel~~',
					handler: function(button) {
						button.up('form').fireEvent('resetPostForm')
						button.up('form').fireEvent('cancel')
					}
				}]
			},
			listeners: {
				render: function() {},
				show: function(form) {
					var ctrl = form.down('#commentContent')
					if (ctrl) ctrl.focus()
				},
				resetPostForm: function() {
					// this.ownerCt.animate({
					// 	to: {
					// 		height: this.ownerCt.originalHeight
					// 	},
					// 	duration: 250,
					// 	dynamic: true
					// })
					this.ownerCt.setHeight(this.ownerCt.originalHeight)
				},
				cancel: '@{cancel}',
				comment: '@{comment}',
				//createLink: '@{createLink}',
				//attachFile: '@{attachFile}'
			}
		}]
	}],
	listeners : {
		beforedestroy : '@{beforeDestroyComponent}'
	}
})

glu.defView('RS.social.Post', 'comment', {
	xtype: 'container',
	anchor: '-20',
	padding: '0px 0px',
	layout: {
		type: 'hbox',
		align: 'top'
	},
	items: [{
		xtype: 'image',
		cls: 'profile-pic',
		src: '@{userProfilePicture}',
		authorUsername: '@{commentAuthorUsername}',
		hidden: '@{!showImage}',
		width: '@{imageSize}',
		height: '@{imageSize}',
		margin: '@{imagePadding}',
		listeners: {
			render: function(panel) {
				panel.getEl().on('click', function() {
					panel.fireEvent('navigateToProfile', panel, panel.authorUsername)
				})
			},
			navigateToProfile: '@{navigateToProfile}'
		}
	}, {
		xtype: 'container',
		flex: 1,
		items: [{
			xtype: 'toolbar',
			padding: '0px',
			items: ['->', {
				xtype: 'tbtext',
				hidden: '@{!isForumComment}',
				cls: '@{commentAnswerCls} post-forum-answer',
				text: '@{answerTextComment}',
				listeners: {
					afterrender: function(tbtext) {
						tbtext.getEl().on('click', function() {
							tbtext.fireEvent('toggleAnswer', tbtext)
						})
					},
					toggleAnswer: '@{toggleAnswer}'
				}
			}, {
				xtype: 'tbseparator',
				hidden: '@{!isForumComment}'
			}, {
				xtype: 'tbtext',
				cls: '@{likeCls}',
				tooltip: '@{likeTooltip}',
				setTooltip: function(value) {
					if (this.tooltipControl) this.tooltipControl.update(value)
				},
				listeners: {
					afterrender: function(tbtext) {
						tbtext.getEl().on('click', function() {
							tbtext.fireEvent('toggleLike', tbtext)
						})
						tbtext.tooltipControl = Ext.create('Ext.tip.ToolTip', {
							html: tbtext.tooltip,
							target: tbtext.getEl()
						})
					},
					toggleLike: '@{toggleLike}'
				}
			}, {
				xtype: 'tbtext',
				cls: 'post-link',
				text: '@{numberOfLikesText}',
				listeners: {
					render: function(tbtext) {
						tbtext.getEl().on('click', function() {
							tbtext.fireEvent('toggleLikesDisplay', tbtext, tbtext)
						})
						// tbtext.getEl().on('mouseover', function() {
						// 	Ext.defer(function() {
						// 		if (Ext.EventObject.within(tbtext.getEl())) tbtext.fireEvent('toggleLikesDisplay', tbtext, tbtext)
						// 	}, 1000)
						// })
					},
					toggleLikesDisplay: '@{toggleLikesDisplay}'
				}
			}, {
				xtype: 'tbtext',
				cls: 'icon-large icon-caret-down post-hand',
				hidden: '@{!hasDeleteRights}',
				listeners: {
					render: function(tbtext) {
						tbtext.getEl().on('click', function() {
							tbtext.fireEvent('showMenu', tbtext)
						})
					},
					showMenu: function(tbtext) {
						if (!tbtext.menu) tbtext.menu = Ext.create('Ext.menu.Menu', {
							items: [{
								text: RS.social.locale.deletePost,
								iconCls: 'icon-large icon-remove post-menu-icon',
								handler: function() {
									tbtext.fireEvent('deleteComment')
								}
							}]
						})

						tbtext.menu.showBy(tbtext)
					},
					deleteComment: '@{deleteComment}'
				}
			}]
		}, {
			xtype: 'toolbar',
			padding: '0px',
			items: [{
				xtype: 'tbtext',
				cls: 'comment-author',
				text: '@{commentAuthorDisplayName}',
				listeners: {
					render: function(tbtext) {
						tbtext.getEl().on('click', function() {
							tbtext.fireEvent('toggleAuthorDisplay', tbtext, tbtext)
						})
					},
					toggleAuthorDisplay: '@{toggleAuthorDisplay}'
				}
			}, '->', {
				xtype: 'tbtext',
				cls: 'post-date',
				text: '@{dateText}'
			}]
		}, {
			xtype: 'container',
			padding: '@{contentPadding}',
			style: 'word-wrap:break-word',
			html: '@{contentDisplay}',
			isMini: '@{miniMode}',
			listeners: {
				render: function(panel) {
					var links = panel.getEl().query('a'),
						images = panel.getEl().query('img');

					Ext.Array.forEach(links, function(link) {
						var data = link.getAttribute('fullData'),
							dataType = link.getAttribute('dataType'),
							currentLocation = window.location.protocol + '//' + window.location.host + window.location.pathname;
						if (data) {
							if (dataType == 'pdf') {
								link.target = '_blank'
								link.href = data
							} else
								Ext.fly(link).on('click', function(e) {
									e.stopEvent()
									downloadFile(data)
								})
						} else if (link.href && link.href.indexOf(currentLocation) == -1) {
							link.target = '_blank'
						}

						if (link.href.indexOf('social.jsp') > -1) {
							var temp = link.href.split('#')[1];
							link.href = '/resolve/jsp/rsclient.jsp' + (temp ? '#' + temp : '')
						}

						//Correct legacy links to be displayed properly
						if (link.href.indexOf('/showentity?') > -1) {
							//grab type
							var params = Ext.Object.fromQueryString(link.href.split('?')[1])
							if (params.type) {
								switch (params.type.toLowerCase()) {
									case 'document':
										link.href = '#RS.wiki.Main/name=' + params.name
										break;
									case 'actiontask':
										link.href = '#RS.actiontask.ActionTask/id=' + params.sysId
										break;
									case 'worksheet':
										link.href = '#RS.worksheet.Worksheet/id=' + params.sysId
										break;
									case 'forum':
										link.href = '#RS.socialadmin.Component/name=forum&id=' + params.sysId
										break;
									case 'rss':
										link.href = '#RS.socialadmin.Component/name=rss&id=' + params.sysId
										break;
									case 'process':
										link.href = '#RS.socialadmin.Component/name=process&id=' + params.sysId
										break;
									case 'team':
										link.href = '#RS.socialadmin.Component/name=team&id=' + params.sysId
										break;
									case 'user':
										link.href = '#RS.user.User/username=' + params.username
										break;
								}
							}
						}

						if (link.href.indexOf('/post/download') > -1 || Ext.isFunction(link.onclick)) {
							var img = Ext.fly(link).query('img')[0];
							var href = link.href;
							if (img) href = img.src;
							if (href.indexOf('?') > -1) {
								var params = Ext.Object.fromQueryString(href.split('?')[1])
								if (params.fileid) {
									link.onclick = null
									Ext.fly(link).on('click', function(e) {
										e.stopEvent()
										downloadFile('/resolve/service/social/download?id=' + params.fileid)
									})
								}
							}
						}

						if (panel.isMini) {
							link.target = '_blank'
							if (link.href.indexOf('social.jsp') > -1) link.href = '/resolve/jsp/rsclient.jsp' + link.hash
						}
					})

					Ext.Array.forEach(images, function(image) {
						var image = Ext.get(image),
							fullData = image.getAttribute('fullData');

						//convert legacy images
						var imageSrc = Ext.fly(image).dom.src;
						if (imageSrc.indexOf('/post/download') > -1) {
							var params = Ext.Object.fromQueryString(imageSrc.split('?')[1])
							if (params.fileid) {
								var fileid = params.fileid
								if (Ext.String.endsWith(fileid, '"')) fileid = fileid.substring(0, fileid.length - 1)
								Ext.fly(image).dom.src = '/resolve/service/social/download?preview=true&id=' + fileid
								fullData = '/resolve/service/social/download?id=' + fileid
							}
						}

						if (fullData) image.setWidth(200) //force only internal images to 200px (rss images should appear as they are+)

						image.on('click', function() {
							displayLargeImage(fullData || image.src, '')
						})

						image.on('load', function() {
							//force refresh of parent container because this image will load an change the size of the content after being loaded
							panel.ownerCt.doLayout()
						})
					})
				}
			}
		}]
	}]
})