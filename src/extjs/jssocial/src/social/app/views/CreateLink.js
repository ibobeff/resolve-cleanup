/* Deprecated feature
glu.defView('RS.social.CreateLink', {
	asWindow: {
		title: '~~createLinkTitle~~',
		width: 850,
		height: '@{height}',
		modal: true,
		listeners: {
			resize: function(win) {
				win.center()
			}
		}
	},

	layout: 'fit',
	items: [{
		xtype: 'form',
		bodyPadding: '10px',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'textfield',
			name: 'display',
			labelWidth: 125
		}, {
			labelWidth: 125,
			xtype: 'fieldcontainer',
			fieldLabel: '~~linkTo~~',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: '@{componentTypes}'
		}, {
			xtype: 'textfield',
			labelAlign: 'top',
			name: 'url',
			fieldLabel: '~~urlLabel~~',
			labelSeparator: '',
			keyDelay: 1
		}, {
			xtype: 'grid',
			name: 'toGrid',
			scroll: true,
			flex: 1,
			columns: '@{toGridColumns}',
			selType: 'rowmodel',
			dockedItems: [{
				name: 'actionBar',
				xtype: 'toolbar',
				items: [{
					xtype: 'combobox',
					name: 'type',
					emptyText: '~~type~~',
					hideLabel: true,
					displayField: 'name',
					valueField: 'value',
					queryMode: 'local'
				}, {
					xtype: 'textfield',
					name: 'searchQuery',
					hideLabel: true,
					flex: 1,
					minWidth: 250,
					emptyText: '~~searchQuery~~'
				}, {
					xtype: 'button',
					handler: '@{search}',
					iconCls: 'icon-search rs-icon-button'
				}]
			}],
			plugins: [{
				ptype: 'pager',
				showSysInfo: false
			}]
		}]
	}],
	buttonAlign: 'left',
	buttons: ['createLink', 'cancel']
})

glu.defView('RS.social.ComponentType', {
	xtype: 'radio',
	name: 'type',
	boxLabel: '@{name}',
	padding: '0px 20px 0px 0px',
	inputValue: '@{inputValue}',
	value: '@{value}'
})
*/

