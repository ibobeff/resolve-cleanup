glu.defView('RS.social.Main', 'mini', {
	activeItem: '@{activeCard}',
	layout: 'card',
	displayMask: '@{authenticateText}',
	setDisplayMask: function(value) {
		this.setLoading(value)
	},
	dockedItems: [{
		dock: 'top',
		xtype: 'toolbar',
		cls: 'social-mini-toolbar-header',
		defaultButtonUI: 'social-mini-toolbar-button-small',
		items: [{
			text: '@{backText}',
			iconCls: 'icon-large icon-chevron-left',
			name: 'back'
		}, {
			xtype: 'tbtext',
			cls: 'social-mini-post-title',
			text: '@{titleText}',
			tooltip: '@{titleTextTooltip}',
			setTooltip: function(value) {
				this.tooltip = value
				if (this.tooltipControl) this.tooltipControl.update(value)
			},
			listeners: {
				render: function(tbtext) {
					tbtext.tooltipControl = Ext.create('Ext.tip.ToolTip', {
						html: tbtext.tooltip,
						target: tbtext.getEl()
					})
				}
			}
		}, {
			xtype: 'tbfill'
		}, {
			iconCls: 'icon-large icon-plus',
			hidden: '@{!showFollow}',
			handler: '@{followStream}',
			tooltip: '~~followStreamTooltip~~'
		}, {
			iconCls: 'icon-chevron-left',
			hidden: '@{!showPagingButtons}',
			name: 'previousPage',
			tooltip: '~~previousPage~~',
			text: ''
		}, {
			iconCls: 'icon-chevron-right',
			hidden: '@{!showPagingButtons}',
			name: 'nextPage',
			tooltip: '~~nextPage~~',
			text: ''
		}, {
			xtype: 'tbseparator',
			hidden: '@{!showPagingButtons}'
		}, {
			iconCls: 'icon-large icon-chevron-sign-down',
			hidden: '@{!showToggleStreams}',
			handler: function(button) {
				button.fireEvent('toggleStreams', button, !button.isUp)
				button.isUp = !button.isUp
				button.setIconCls('icon-large ' + (button.isUp ? 'icon-chevron-sign-up' : 'icon-chevron-sign-down'))
			},
			listeners: {
				toggleStreams: '@{toggleStreams}'
			}
		}, {
			iconCls: 'icon-large icon-repeat',
			hidden: '@{!showRefreshButton}',
			handler: '@{refresh}',
			tooltip: '~~refreshPostsTooltip~~',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	defaults: {
		listeners: {
			activate: function(panel) {
				panel.up('panel').dockedItems.each(function(docked) {
					docked.items.each(function(item) {
						item.doComponentLayout()
					})
					docked.doLayout()
				})
			}
		}
	},
	items: [{
		autoScroll: true,
		items: '@{groups}'
	}, {
		autoScroll: true,
		padding: '0px 0px 0px 10px',
		items: '@{posts}'
	}, {
		html: 'comments'
	}, {
		xtype: 'form',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		buttonAlign: 'left',
		buttons: ['post', 'cancelMini'],
		bodyPadding: '5px',
		items: [{
			xtype: 'textfield',
			emptyText: '~~subject~~',
			hideLabel: true,
			name: 'subject'
		}, {
			xtype: 'posteditor',
			displayLinkAndUpload: false,
			flex: 1,
			name: 'content',
			listeners: {
				//createLink: '@{createLink}',
				//attachFile: '@{attachFile}'
			}
		}]
	}, {
		xtype: '@{activePost}'
	}],
	bbar: {
		xtype: 'toolbar',
		hidden: '@{!showPostButton}',
		cls: 'social-mini-toolbar-footer',
		defaultButtonUI: 'social-mini-toolbar-button-small',
		items: [{
			xtype: 'textfield',
			inputCls: 'post-message',
			emptyText: '~~postYourMessage~~',
			readOnly: true,
			flex: 1,
			listeners: {
				render: function(field) {
					field.getEl().on('click', function() {
						field.fireEvent('switchToPost', field)
					})
				},
				switchToPost: '@{switchToPost}'
			}
		}, {
			iconCls: 'icon-large icon-comment-alt',
			hidden: '@{!showPostButton}',
			handler: '@{switchToPost}'
		}]
	},
	listeners: {
		render: function(panel) {
			if (panel.displayMask !== false)
				panel.setLoading(panel.displayMask)
		}
	}
})