Ext.define('RS.social.PostEditor', {
	extend: 'RS.common.HtmlLintEditor',
	alias: 'widget.posteditor',
	displayLinkAndUpload: true,
	searchString: '',
	cursorIndex: 0,
	hideLabel: true,
	name: 'content',
	enableAlignments: false,
	enableFont: false,
	enableFontSize: false,
	enableFormat: false,
	enableLists: false,
	enableSourceEdit: false,
	enableColors: false,
	enableLinks: false,
	enableUpload: false,

	getDocMarkup: function() {
		//WARNING: This is a copy from extjs to override the style sheet loaded in the htmleditor font.
		var me = this,
			h = me.iframeEl.getHeight() - me.iframePad * 2,
			oldIE = Ext.isIE8m;

		// - IE9+ require a strict doctype otherwise text outside visible area can't be selected.
		// - Opera inserts <P> tags on Return key, so P margins must be removed to avoid double line-height.
		// - On browsers other than IE, the font is not inherited by the IFRAME so it must be specified.
		return Ext.String.format((oldIE ? '' : '<!DOCTYPE html>') + '<html><head><style type="text/css">' + (Ext.isOpera ? 'p{margin:0}' : '') + 'body{border:0;margin:0;padding:{0}px;direction:' + (me.rtl ? 'rtl;' : 'ltr;') + (oldIE ? Ext.emptyString : 'min-') + 'height:{1}px;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:text;background-color:white;' + (Ext.isIE ? '' : 'font-size:12px;font-family:{2}') + '}</style><link rel="stylesheet" type="text/css" href="/resolve/client/css/client-all.css"/></head><body></body></html>', me.iframePad, h, me.defaultFont);
	},

	getSelection: function() {
		var me = this,
			val = me.getValue(),
			end = val.indexOf('<span class="suggest"></span>'),
			temp = val.substring(0, val.indexOf('<span class="suggest"></span>')),
			start = temp.lastIndexOf(me.menu.magicKey),
			selection = val.substring(start, end);
		return selection;
	},

	click: function(e) {
		this.closeMenu();
	},

	closeMenu: function() {
		if (this.menu) {
			this.menu.close();
			this.focus();
			this.menu = null;
			this.store = null;
		}
	},

	removeText: function(el, text, magicKey) {
		var nodes = this.getTextNodesIn(el),
			textString = '',
			start = -1,
			end = -1,
			smallText = text.toLowerCase().substring(1),
			node;

		for (var i = 0; i < nodes.length; i++) {
			node = nodes[i];
			textString += (node.textContent || node.nodeValue || '');

			if (node.mark) {
				start = textString.lastIndexOf(magicKey);
				end = textString.length;
			}
		}

		// console.info(nodes)
		if (start > -1) {
			this.setSelectionRange(el, start, end);
			this.execCmd('delete');

			if (Ext.isGecko) {
				this.execCmd('delete');
			}
		}
	},

	getTextNodesIn: function(node) {
		var textNodes = [];

		if (node.nodeType == 3) {
			textNodes.push(node);
		} else {
			var children = node.childNodes;

			for (var i = 0; i < children.length; i++) {
				var current = children[i];

				if (Ext.fly(current).hasCls('suggest')) {
					textNodes.push({
						mark: 'fromHere'
					});
				}

				textNodes.push.apply(textNodes, this.getTextNodesIn(current));
			}
		}

		return textNodes;
	},

	setSelectionRange: function(el, start, end) {
		var doc = this.getDoc() || document;
		var win = this.getWin();
		var hasCreateFn = typeof doc.createRange === "function";
		var hasGetSelectionFn = win && typeof win.getSelection === "function";

		if (hasCreateFn && hasGetSelectionFn) {
			var range = doc.createRange();
			range.selectNodeContents(el);
			var textNodes = this.getTextNodesIn(el);
			var foundStart = false;
			var charCount = 0,
				endCharCount;

			for (var i = 0; i < textNodes.length; i++) {
				var textNode = textNodes[i++];

				if (textNode.mark) {
					continue;
				}

				endCharCount = charCount + textNode.length;
				range.selectNodeContents(textNode);
				var hasMoreChars = start >= charCount && start <= endCharCount;

				if (!foundStart && hasMoreChars) {
					range.setStart(textNode, start - charCount);
					foundStart = true;
				}

				if (foundStart && end <= endCharCount) {
					range.setEnd(textNode, end - charCount);
					break;
				}

				charCount = endCharCount;
			}

			var sel = this.getWin().getSelection();
			sel.removeAllRanges();
			sel.addRange(range);
		} else if (document.selection && document.body.createTextRange) {
			var textRange = document.body.createTextRange();
			textRange.moveToElementText(el);
			textRange.collapse(true);
			textRange.moveEnd("character", end);
			textRange.moveStart("character", start);
			textRange.select();
		}
	}
});