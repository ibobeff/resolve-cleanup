Ext.namespace('RS.social.ComponentMap')
Ext.applyIf(RS.social.ComponentMap, {
	actiontask: 'RS.actiontask.ActionTask',
	decisiontree: 'RS.wiki.Main',
	document: 'RS.wiki.Main',
	forum: 'RS.social.Main',
	namespace: 'RS.wiki.WikiAdmin',
	process: 'RS.social.Main',
	rss: 'RS.social.Main',
	runbook: 'RS.wiki.Main',
	team: 'RS.social.Main',
	user: 'RS.user.User',
	worksheet: 'RS.worksheet.Worksheet'

})